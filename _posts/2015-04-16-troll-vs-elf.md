---
layout: post
title: "Troll vs. elf"
date: 2015-04-16
---

Game on. Our brave team is exploring some caves.
Suddenly, our scout notices a huuuge cavern.
Sadly, it is occupied by old, war-grizzled troll.

The Fight starts.
What now?
How should a game master deal with it?

A light, agile elf should easily avoid troll's attacks.
A troll, however, should he manage to hit an elf then he will easily turn him into an elven jelly.

Logical, isn't it? Sadly, most RPG systems see fight as a fight, regardless of what it "really" is.

Let us look at the following situation:

Troll: attack 4, damage 10, defense 4, absorption 5
Elf: attack 8, damage 2, defense 8, absorption 0

If the players are using a single 6-sided die (1d6), an elf will hit a troll virtually every time, damaging him for 0 to 3 damage per hit. Troll has a very low chance to actually hit that pesky elf (he has to roll 6 on d6), however, if he does hit, he will deal on average 13 damage (10 + d6), most likely killing an elf on the spot.

You could say that this is how it should be. And this particular case is kind of exaggerated. Additionally, the way skills are described here makes player's declarations kind of meaningless.

Let us extend our example.
This time an elf and a troll are fighting on an old, rickety rope bridge.
And this time elf's player approaches this topic differently, declaring an action:
"I want to use enemy momentum to make him fall off the bridge."
What now?
In theory, elf will of course hit a troll. But the mechanism shown above does not provide a proper way to tell if the 'trip the troll' action was successful. Or if it was even possible. Attack vs... defense? absorption? The mechanics doesn't say how to resolve it.

For us, this kind of mechanics is broken - it does not support our playstyle.

We are using something different.
Our character description says what a character can do and what is he / she like.
For example, sticking to a fantasy setting, we can say that an elf has a "wardancer" (4) ability while a troll has a "berserker" (4) ability. Elf is "agile" (4), troll has "stone skin" (4)

Whenever elf uses his attack speed, reflexes, dodging ability etc in combat, he is using his full power - reaching 8.
Whenever troll is soaking hits or tries to smash enemy, he is at his 8.

Going back to the bridge example.
On this terrain an elf has an advantage. A troll can't fully use his tankiness and his size works against him. In addition, he has no skill to compensate for the hostile environment.
If that elf declares "I am trying to cut one of the ropes, so that the bridge tilts and I try to grab a rope to stay on it while the troll falls down", he will be using his "agile" trait. (he cannot use "wardancer" because that was not a directly offensive action). Troll has no skill that would help him, resulting in elf having 4 and troll having 0. If elf succeeds, fight has just finished, which does not necessarily mean that the troll is dead.
If an elf wanted to maximize his numbers, he would need to change his declaration to e.g. "I feign an attack and dance past the troll to provoke the troll to do an attack that will result in him breaking the rope he's standing on with his club while (...)"

However still, if the context is negligible, that whole fight boils down to one lucky dice roll and "I... yyyy... cut him!". An agile elf wardancer is '8' and a stone troll berserker is '8'. Without the context, this battle is boring.

So we are back to spot one.

Problem gets even bigger if we start to thing about non-combat conflicts.

Hektor is a tough inqusitor, he can interrogate anyone and anything. On the other hand, Sabina is a cute mascot. She can charm anyone and anything if she likes.
Hektor will always be able to interrogate Sabina, who will confess everything she knows and even more. On the other hand, Hektor will not be able to refuse her if she asks really nicely.

Combat is easy in a way. It deals with something tangible - physical state. Social conflicts, however, can be approached in many different ways. Sometimes a move in one direction will make other options unavailable.
For example, if Hektor wanted to interrogate Sabina, she could appeal to his better side he did not even know he had and no interrogation would happen.
However, if Hektor was properly suspicious about her, his inqusitor nature would win, leaving her womanly wiles useless against him.

Possibly, a good solution would be something between "attack, defense, damage..." and "agile wardancer". Maybe we should use (borrowed from MMO) idea of "tanking" and "evasion".
If we do that, agile wardancer could give 6 to evasion and 6 to hit, while stone berserker would give 6 to damage and 6 to absorption.

In no context fight those mechanisms work similarily. 
However someone having skills of both berserker and wardancer would be able to defend from the enemy where they both are strong (damage vs absorption) and attack where enemy is weak (precision critical hit vs... nothing).

Of course, this is a somewhat simplified example. 
Normally, a barbarian would ensure for example +1 to damage, +1 to hit (no defense), +1 courage. If a character can have 6 skills and 6 traits describing it, the set and combination of those skills and traits would create a profile of that character.

This profile would reward a player for doing things that fit the character, and particular skills would determine an exact way of how character's goals are achieved - therefore modifying the difficulty by the context of the game world.

Unfortunately, breaking all character skills and traits to a long list of statistics slows down the game.

Thus, another approach to this problem.

You could roughly (this requires deeper thought) divide skills into 3 categories:
combat, social and "science"
Each of the categories can have skills that are subtle (wardancer, everybody's mascot) and those that are rough (barbarian, inquisitor)
If we agree that defending against an attack from "the other" category is more difficult, elf vs troll issue becomes much smaller.

The biggest difference is in the story: elf will be able to trick the troll, but can't really hurt it, while troll will simply be unable to hit the elf. Or, in different implementation, elf can critically hit the troll ignoring the absorption (direct hit into an eye) while troll will be able to smash an elf ignoring the evasion. The exact implementation depends on the goals of the mechanics, but you see the symmetry.

This also causes one more effect. With limited number of skills, characters are starting to specialize. It is no longer possible to create a true one-man army - your character will always have a soft belly somewhere. Even if you did manage to cover all bases, you would be much weaker compared to someone who has fewer skills, but much more specialized; on the other hand, you might be able to use those weaker skills to hit an opponent where he is weak.

Which solution is the best depends on needs of players and play style. We plan to experiment until we find something that will work for us.

Regardless of what we choose, "an elf and a troll problem" is a good acceptance test for RPG mechanics. One of many we are utilizing and one of those every mechanics needs to answer somehow.