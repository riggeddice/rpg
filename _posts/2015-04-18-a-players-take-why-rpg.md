---
layout: post
title: "A player's take: why RPG"
date: 2015-04-18
---
# Bad opinion

"Those other people", who don't play RPGs usually don't think highly of role playing games. "Their" opinion starts at "not harmful, let them play", goes through "waste of time, sitting in a group and babbling about nothing" and sometimes gets to "dangerous, satanic sect, better closed in a far away, secluded place so that they can't harm anyone. Ah, and harmful for women."

At the same time the players (including women) think it is one of the most interesting types of entertainment.

What is very bad is the fact there is very little understanding between the two groups, which makes life of RPGers harder.
Even worse, often there is no will to achieve understanding on either side. Usually, attempts to explain what RPG is to "regular folk" scares them away, even if you don't go into the weirder areas.
This leads to us, 'weirdos' hiding away and pretending to be 'normal' among others.

Why is opinion about roleplayers so bad?

There are many stereotypes concerning RPG players. Stinky, unshaven geeks, who hiss at the sight of sunlight and are dumbfounded in presence of women - this is how people often see you when you say you play RPGs.

It is also difficult to properly explain what RPG is about due to lack of agreement in the role playing community. Fight between 'story tellers' and 'dice rollers', or quarrels which mechanics is better can be discouraging, even if you are already playing, lest when you are not 'in'.

# Why bother?

So... what is RPG?
Typical Joe would ask a friendly uncle (usually called Google) and get pointed to wiki, which says:

"A role-playing game (RPG and sometimes roleplaying game) is a game in which players assume the roles of characters in a fictional setting. Players take responsibility for acting out these roles within a narrative, either through literal acting or through a process of structured decision-making or character development. Actions taken within many games succeed or fail according to a formal system of rules and guidelines."

Not bad, eh? And even does not sound very evil...
But try to convince your aunt that it's not as bad as she thought with this definition.

When I am asked what RPG is, the shortest answer I can come up with is "fun".
But that's cheating. There is so much more to it that it's hard to choose a place to start.

# Creativity

RPG can teach you to come up with unexpected solutions to problems. Even if during a game you solve problems you would never see in real life, it still teaches you to search for solutions, to come up with them and to persevere against odds, sometimes under pressure - sessions can get quite intense in this good sense.

# Community

RPG helps people bond. After all, a group of players is usually more or less constant and people get to know each other, even outside of the imaginary world they play in. And defeating a very powerful boss together leaves people with a sense of accomplishment and - in some weird way - brotherhood of arms. And it does not matter those arms were imaginary. Some people have shared memories of memorable travels and trips, RPG players have shared memories of accomplishments and successes which really changed the "world" they played in, with real emotions (even if the world was a fiction). Like reading an amazing book, squared.

RPG also makes the players understand others better. A player needs to take the point of view of someone else, sometimes someone very different to them. It forces a perspective change. It forces thinking "how would that other person act and think". This transfers to real world; very often players see, that different people have different goals, needs and perspectives. Therefore, RPG players can learn cooperation and getting to consensus.

# Growing up

RPG helps people learn to deal with defeat. Sometimes you will fail to do what you wanted and you know what? It's okay. Because you can try again. And as life is a sequence of victories and defeats, RPGs give the players the abilities to function better in the world.

RPG could also be a sandbox for children to learn to deal with some situations normally not happening to us every day. A very good example could be a Trollbabe system, where a father acted as game master for his daughter, who was about 6 at that time. During a session, after her decision, her character saved a village, but lost a dear friend... He had his doubts, but carried out the situation. He thought she'd be discouraged. Yet, a few days later she asked him to continue...
Don't use that example in discussion with grumpy aunt though ;-)

But you don't have to torment your children with such difficult situations. You can let them explore, define their own world, nurture their imagination, so that they don't forget how it is to have a very flexible mind while growing up.

# Fun

It's also a way to tell a great story, one where nobody knows how it will end.
Or just destress yourself with old good hack-and-slash type of session.
Or make a difficult tactical scenario with multiple stakes and resources allocation.

In short, RPG can be whatever we choose to make it.
Sandbox, playground, it's the best movie theather ever, because you're right in the very middle of action and you are in control!

So go forth, fellow RPGers, and play!