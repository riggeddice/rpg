---
layout: post
title: "Your character would never do that!"
date: 2015-04-19
---

# The problem

One of the most common complaints towards a player (especially a newish one) would be something like "your character does not act according to their personality". For a layman, this means that the player (person sitting at the table) is acting in the way their character (that is, an avatar in Shared Fictional World / Shared Imaginary Space) would never act.

For example, can you imagine Disney's Little Mermaid actively trying to drown random sailors for loot and experience points? Or can you imagine Han Solo begging imperial stormtroopers for his life? Yeah... slightly unnatural. A character does not act how they "should". The deed is not consistent with their personality.

This situation - a very common one - is usually seen as player's "fault". In fact, it is a symptom of the broken **mechanics** of the game. 


# An example

Aurora is a mermaid princess (Disney' Little Mermaid clone). She and her whole tribe is endangered by an Evil CEO of Toxic Factories Industry, Derek Handsome because of his companies spilling dangerous chemicals on top of her tribe's underwater home. Derek is fighting political battles with his sister, Anna, who wanted to think of the environment (but Derek is in charge so she can't do much). Humans don't know about mermaid-folk existing.

The game master (person responsible for processing the reality and - simplified - for the creation of the main stories' opportunities) has put Derek Handsome and his sister on the damaged ship which is being hit by a storm. The ship is creaking and will soon fall apart. Aurora is in the vicinity (how convenient and unexpected!), therefore the game master asks the player controlling Aurora "so, what do you do"?

A GM hopes for a resolution more or less like this: Aurora shows herself to the sailors and she will guide the ship to the land, summoning the help from her tribe (and various magical fish). During this scene she will negotiate with Derek to make a deal: his life (and Anna's) for not polluting her home anymore. GM also secretly hopes for the player to step up and make Aurora charmed by Derek's beauty... this would lead to a potentially twisted and interesting story... perhaps even a romance? Oooh, the opportunities!

And a player? What does a player think?
Ok, so if the ship gets help it may not fall apart, but Aurora's tribe will suffer; nobody knows whether Anna convinces Derek to stop polluting and the man was described to have a heart of stone. On the other hand, if Derek dies here, Anna takes over the company. Sure, Anna will not be happy (her brother just died), but whatever, Anna cannot prove Aurora did something to Derek (especially as Aurora is quite a sweet-talker). 
So Aurora's actions should go as follows: do her best to ruin the ship even more, make sure Derek Handsome dies (send the thing closest to a fish assasin after him) and save Anna - and ONLY Anna (well, perhaps some sailors too; they are irrelevant). Bonus points for crying in front of Anna as Aurora couldn't save everyone including Anna's dear brother. Tell Anna "I saved your life, now you stop killing my people". Logical, rational, uses Aurora's strengths and pretty foolproof.

The game master is shocked and horrified. Aurora acted so... cold-blooded and calculating. She is supposed to be a sweet and cute mermaid! And... and... ROMANCE! GM tells a player "This is not what Aurora would do". Player disagrees. In heated discussion they look at the character sheet (a document which shows the important information about a character; kind of a CV).

And what do they see on the character sheet?

Strength = 10, Dexterity = 6, Vitality = 5, Charisma = 8, skills: "swimming, fish summoning, charming people, singing..."

Well, I do see our mermaid is very strong and is quite a smooth-talker. But I do not see "sweet and cute mermaid" anywhere. Neither do I see "cold-blooded Derek killer".


# What went wrong

Miscommunication and differences in expectations.

See, GM sees Aurora as a cute Disney Mermaid archetype while the player sees her as an underwater guardian of her tribe, warrior princess, "I did what I had to do" type of a mermaid. Sure, GM told the player what campaign they are playing (it was 3 months ago and the player had a sick rabbit) and the player told GM what Aurora is supposed to be (but it was 2 months ago and noone was really sober at that time)...

This should be on a character sheet. As in, this is too important of an information to NOT be included in the single document visible for both a GM and a player which is a communication tool between the player and a game master about an in-fiction character! 
If you play a tactical RPG, you want balanced stats and profiles, as this is important. But if you play a RPG with stories and choices over tactical challenges it is important to know what the character is, what are their hopes and dreams. 

THIS is important. The character sheet should have different information according to the type of a game you are playing!


# The commonly used solution

Some game masters started requiring the players to write the "historical information" on character sheets. What did you do in the past? How did you resolve stuff? What are your hopes and dreams? What are you to the world? That way a game master is able to **infer** what a player will do with the character.
Infer. Not know. And the player still needs to remember what was written there.

Because of the difficulty of sessions, intensity, emotions and the fact a player does not "live" their character's life, the character's personality will eventually drift from what was written to the player's personality. I mean, imagine a very simple situation: your character has a manner. Every time it should say 's' it says 'z'. You will remember it first 4 sessions (one month). Then, the deeper the player is invested in the world, the more happens around them, the more they need to remember... the player will eventually forget because they are overwhelmed.

And we are talking only about a simple mannerism. Not about the entire way of acting and resolving issues which everyone does differently.

This is why in my opinion the commonly accepted solution to this problem does not correct it, hiding it instead. It gives one more thing to think about for the player, which only increases the mental burden on an already difficult form of entertainment.

# Mechanics as a solution

Ok, so how should this work in an ideal world?

In an ideal world the player should not have to remember this stuff at all. The player's character would simply promote particular actions (consistent with the personality and history) over the actions disjoint with the character. The player is in control, but the character steers the player, showing "hey, I would do this". And player is like "okay, let's do it like this then" or "not this time, I want the other way".

And this is exactly what the mechanics can do for you.
Let us modify the character sheet without touching any other components of the mechanics.

Why not replace the Strength, Dexterity... etc (statistics) from the character sheet with descriptions (tags)? For example, new character sheet would look like this:

from:
stats: Strength = 10, Dexterity = 6, Vitality = 5, Charisma = 8, 
skills: "swimming, fish summoning, charming people, singing..."

to:
stats: Disney's Mermaid = 3, Sweet and Cute = 5, Warrior Princess = 8, "My tribe is worth every sacrifice" = 10 
skills: "swimming, fish summoning, charming people, singing..."

What has changed?

The stat-based character sheet shows that Aurora is a very strong mermaid who is also good with people and convincing them. She is on the delicate side and not too agile. Her ways of resolving the problems will be "attack first, talk later, avoid being hit or attacked". 
The focus here is what Aurora **does**.

The tag-based character sheet shows that Aurora cares for her tribe and she will act mercilessly. She is still a sweet and a cute mermaid, but her primary focus is her tribe and her main way of acting is with "warrior princess" ways. 
The focus here is what Aurora **is**.

So the above helps with resolving miscommunications. But how to make the steering part work?

Humans are pretty rational agents - they will usually try to maximize their chances to win. If Aurora's player is able to, they will always use "My tribe is worth every sacrifice", simply because that tag gives them the highest result. Seeing this character sheet it is **obvious** Derek has no chance to survive, as Aurora can get 3 trying to use her Disney nature or 8 using Warrior Princess. 

Oopsie.

Now, the funny thing - the player WILL act according to the character's personality, simply because this is the most efficient way. The way which maximizes the chances to win. Sure, the player can do it differently, but then the player will not maximize their efficiency (which reduces the chance of conflict resolution to go the player's way, thus, the player will probably do it the optimal way which is - still - the way consistent with Aurora's personality). And the player does not have to remember anything at all.

Short example:

Aurora is trying to convince Anna to stop polluting the water. She can either go with her soft mermaid nature ("Sweet and Cute") or go threatening and intimidating Anna ("warrior princess"). But Anna is a person hardened in political fights with her brother (+2 to defense against intimidation) and is grateful to Aurora for saving her (-2 to defense against persuasion).

Sweet and Cute: base 5 + Anna grateful for being saved -> 7
Warrior Princess: base 8 + Anna being hardened -> 6

From the player's point of view, the most efficient way (and the more probable result of success) is to use the softer side of Aurora's nature. Even if, a moment ago, the same Aurora murdered half of the ship's crew and sent an assassin after her brother.

...I am starting to like that character...

The differences in fiction between the two approaches and potential next moves for all the agents are outside the scope of this article, but they are the desired side effects of this type of mechanics.


# The closing

Mechanics is so much more than "I hit him, he hits me" which is often one of the few things portrayed in RPG games and a proper use of the mechanics is what can really help the players and a game master achieve the common goal - to have fun.

What - I hope - I managed to achieve is show you why the "you are not playing according to your character's personality" problem is possible to resolve using mechanics and this is one of the clear advantages of using a structured mechanics in RPG games.