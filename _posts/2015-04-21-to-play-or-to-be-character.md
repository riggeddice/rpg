---
layout: post
title: "To play a character or to be a character?"
date: 2015-04-21
---

This question has always been a basis for a very heated discussion in gaming world.

# Be a character

I have heard many players saying that without "becoming" your character you can't really, truly have fun.
For them it never is "my character does X", it is always "I do X".

They immerse in their character so much that the border between the two is blurred during the game. They do not CREATE the story, they BECOME a part of the story and of the world.
The best fun they have when they are "in character", acting and doing only those things that their character would / could do.

I would like to look closer at "becoming a character", because this play style is in my eyes in some extent responsible for the bad opinion RPG has among older generations.

When player "is a character", he or she becomes very invested in the character and very involved in the game. After all, it affects "the player", not "the character".
Is it bad? Not necessarily, as long as they are mature enough to be able to step out of the world of fiction at any time.

If this in not the case, you could hear following exchange:

- "You robbed me!"
- "Because you insulted me!"
- "How could you, I thought we were on the same side!"

And again, it's fine, even with raised voices, as long as animosity does not continue beyond the in-game world.

If people involved are not mature enough, sometimes situations like this can even lead to broken friendships. 

Also, being this invested is not easily understandable for people from the outside of gaming world.

What is the best way to get more space in the public transportation for yourself?
Just start discussing your latest gaming session. I guarantee you that the moment you start saying "...and then I took my +6 axe of dragon slaying and severed its head and chopped the wings off the carcass..." you will mysteriously get plenty of space around. Even if it's crowded.
But beware, you might be taken for someone questionable or get questioned by the police as to where did you hide the body. ;-)

There are also potential problems with 'becoming' a character. Some players want that immersion so much, that they will not hear whatever advice is given to them unless given 'in game'. Also, getting them into 'out-of-game' state can be a tad difficult - it will interfere with their whole experience and - understandably - make them a bit cranky.
Plus, there is always a question of "what your character can do", but this is a topic for another post.

In a mixed group of players with differing play styles, this could be disturbing and tiring.

# Play a character

There are also players on the other end of the spectrum. Those players treat their characters more like pawns in chess. 
For them, fun lies in actively affecting the story by planning their strategies, in detailing a complex plan to solve the problem at hand, in playing the whole world, making things happen their way. Their character is just a tool they can use to influence the system.

This kind of players is less noticeable. Their discussions in public transportation are not as disturbing to general public, even if a bit weird.

However, this type of players stands at risk of over-strategizing and taking decisions that their character would never do. It does not sound that bad, until you see a holy paladin mercilessly killing a villain, only because nobody will know and this is the best way he can ensure a particular village will be safe.

# Problems

As a game master, your problem with the "be a character" type of player will show the very moment you will want to split the team, or show something that is "not here".
Their reaction will be "Wait, wait, wait, my character is not there, I can not see it!"
To accommodate the players "not there" will need to leave the room, part of your group will start to get bored, because they must wait until second part does their thing.
Also, player sharing an idea with another player is unthinkable. Reasoning here goes "how can a bard advice a warrior as to how to fight?" or "How can a barbarian solve a riddle better than a wizard?"
There is also a good side: usually the story is quite dynamic with this type of players.

On the other hand, with the "play a character" type of player you risk action slowing down almost to a stop as they tweak and perfect their little plan of taking over the world, even when this particular part of it is absolutely not significant. Or, different issue, sometimes your RPG session might start to feel like a board game...
Good sides: very easy cooperation between players and you have no problems with peeking at "another place, another time".

There is also a third problem.
Players of both types are not very compatible with the opposite kind.
If you are unlucky enough to have an extreme case (of either side), this player will try to impose his playing style on others. Because, you know, this is the TRUE role playing! 

# Solution

Is there a way to protect yourself from those issues?
Yes. 
Your best weapon here is a carefully designed and simple social contract (what are we going to play, how, universum, rules of conduct are very important parts of it) 

And - unfortunately - your players must be mature enough.