
# System
System stworzony jest po to, aby wspólnie tworzyć opowieści.
Konflikty stawiane są o to, jak potoczy się opowiadana historia.
Mniej istotne są konkretne akcje postaci (na przykład "robię trzy kroki w prawo") a efekt pożądany przez grających ("chcę się dowiedzieć, co tu się mogło stać").

System jest systemem pulowym.
Nie korzysta jednak z kostek a z żetonów.
Żetony mogą mieć trzy oznaczenia (X, V , O) oraz różne kolory. 
Żetony "X" reprezentują porażkę.
Żetony "V" reprezentują sukces.
Żetony "O" to żetony specjalne.


# Mechanika

## Poziomy trudności

Poziom trudności konfliktu określony jest przez pulę początkową.
Pula budowana jest przez dodanie do puli początkowej sukcesów wynikających z karty postaci, bonusów i umiejętności specjalnych.
Istnieją 4 poziomy trudności:

Typowy: 5 żetonów porażki
Trudny: 8 żetonów porażki, -3 żetony sukcesu (najczęściej występujący)
Ekstremalny: 11 żetonów porażki, -6 żetonów sukcesu
Heroiczny: 15 żetonów porażki, -9 żetonów sukcesu

## Karta postaci

Umiejętności postaci podzielone są na trzy kategorie.
Każda kategoria może dodać do puli konfliktu do trzech żetonów sukcesu (w sumie maksymalnie do 9).


## Umiejętności specjalne

Niezależnie od settingu, system umożliwia wykorzystanie jednego rodzaju umiejętności "specjalnej"

Przykłady umiejętności specjalnych:
- magia
- moce wampirów
- black technology
- moce Chaosu
- ...

Wykorzystanie umiejętności specjalnej powoduje:
- dodanie do puli trzech niebieskich żetonów sukcesu
- podmienienie jednego żetonu porażki na niebieski
- dodanie do puli od 1 do 5 niebieskich żetonów specjalnych

Im więcej żetonów specjalnych, tym bardziej niestabilne są umiejętności specjalne. Rekomendowana ilość żetonów specjalnych to 3.


## Bonus sytuacyjny

Mistrz gry może przyznać do 4 dodatkowych żetonów sukcesu. Zależeć to będzie od okoliczności, podejścia i opisu graczy, przygotowania do akcji i tak dalej.
Żetony bonusu sytuacyjnego zwykle są czerwone.


## Zasoby

W zależności od sytuacji w konflikcie gracz może wykorzystać zasoby.
Wykorzystanie zasobu powoduje dodanie do puli trzech żółtych żetonów sukcesu i wymianę jednego zwykłego żetonu porażki na żółty.

## Budowanie puli
Gracz zaczyna z pulą początkową, którą następnie uzupełnia o żetony sukcesu wynikające z 
- karty postaci, 
- zasobów, 
- umiejętności specjalnych,
- bonusu sytuacyjnego

Jeśli konieczne jest odjęcie sukcesów za poziom trudności, najpierw usuwa się zwykłe żetony sukcesu.

## Wynik

Aby uzyskać odpowiedź, jak rozwiązał się konflikt, należy pociągnąć jeden żeton. Jego rodzaj i kolor określa wynik i jego zabarwienie.





# Konflikt

Konflikt jest narzędziem zarówno dla mistrza gry jak i gracza. Pozwala on usztywnić elementy opowiadana.
Wszystko, co nie jest potwierdzone konfliktem, może się zmienić w trakcie wspólnego tworzenia opowieści.


## Deklarowanie konfliktów

## Kiedy deklarować konflikt?

## Jak deklarować konflikt?

## Jak zinterpretować konflikt?



