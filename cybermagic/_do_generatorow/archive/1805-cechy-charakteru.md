---
layout: inwazja-konspekt
title: "Cechy charakteru, obserwowalne"
---

# {{ page.title }}

Agresywny, dążący do starcia
Aktywny, wiecznie coś robi
Altruistyczny, skupiony na dobru innych ludzi
Ambitny, chętny, by się wykazać
Amoralny, jedyną zasadą jest korzyść
Anarchistyczny, nie ufa nikomu u władzy
Apatyczny, niewiele tą osobę motywuje
Apodyktyczny, władczy
Ascetyczny, unikający luksusów
Aspołeczny, nie dba o innych
Atletyczny, dobrze zbudowany
Atrakcyjny, przyciąga uwagę
Autentyczny, nie zakłada masek
Bardzo łatwy do zniechęcenia
Bardzo uzdolniony, specjalista w swoim fachu
Barwny, z niesamowicie ciekawymi opowieściami
Bazuje na intuicji jako podstawie działania
Bez cienia manier, nie potrafi się zachować
Bezbarwny, przezroczysty, praktycznie niewidzialny
Bezduszny i twardy
Bezkompromisowy, nieustępliwy, niemożliwy do zatrzymania
Beznamiętny, o minie z kamienia
Bezpośredni, mówi jak myśli
Beztroski i swobodny; żyje według własnych przekonań
Będzie jak ma być; fatalistyczny
Bigoteryjny, świętoszkowaty
Błazeński i śmieszkowaty
Błyskotliwy, świetny w zagadkach
Boi się konkretnej rzeczy i unika tej rzeczy za wszelką cenę
Brawurowy i ze skłonnościami do nadmiernego ryzyka
Brutalny, bezwzględny, grubiański
Bystry, szybko łączący fakty
Cechuje się arogancją i poczuciem wyższości
Charyzmatyczny
Chętny do eksperymentowania i testowania nowych rzeczy
Chętny przygód i czegoś nowego
Chłodny, nie okazuje uczuć
Chwalipięta, czego to nie zrobił...
Cichy i nieśmiały; bojaźliwy
Cichy, nie mówi dużo
Ciekawski, wszystko chce wiedzieć i wszędzie wsadzić nos
Ciepły, otwarty wobec ludzi
Cierpliwy, jak pająk w sieci
Cyniczny, patrzy na świat z goryczą
Dekadent, lubi przepych i luksus
Delikatny i o cienkiej skórze
Dobroduszny, miły w obyciu
Dobrze wychowany; zachowuje się zgodnie z zasadami
Dogmatyczny i ze skłonnościami do potępiania
Dojrzały, zachowuje się profesjonalnie i "dorośle"
Domator, osoba rodzinna
Dowcipny, Błyskotliwy, o ciętym języku
Drapieżny, wiecznie zdobywa coraz więcej
Dyskretny, sekrety weń są jak w grób
Dziecinny, niepoważny
Dziwaczny i troszkę śmieszny
Egocentryczny, jest pępkiem świata
Egzaltowany, w przejaskrawiony sposób okazuje uczucia
Ekstrawagancki oraz ekspresywny
Elegancki, noszący się z klasą
Elokwenty, zna też świetnie słownictwo
Empatyczny, zwracający uwagę na uczucia i potrzeby innych
Entuzjastyczny, skłonny do bycia porwanym idei
Fanatyczny; idea jest obsesją i życiem
Figlarny i wesoły
Formalny, niechętnie przechodzi na poziom koleżeński
Hedonistyczny, szukający przyjemności
Hojny, dzielący się z innymi
Honorowy, zawsze wie jak winien się zachować
Idealistyczny, skupiony na lepszej przyszłości
Impulsywny, nie do końca kontroluje emocje
Indywidualistyczny, skupiony na jednostkach
Innowacyjny, szuka nowych rozwiązań i podejścia
Introspektywny, skupiony na głębokim wnętrzu
Intrygancki, lubi sieci intryg i politykę
Jadowity, napędzany nienawiścią i możliwością zaszkodzenia
Kapryśny i marzycielski, buja w obłokach
Kapryśny, zmienny i niestały
Kieruje się silnymi zasadami i regułami
Kłótliwy, lubi spory dla sporów
Kocha zabawę, wszędzie szuka rozrywki
Kompleks paladyna; bierze za dużo na siebie
Konfliktowy; dążący do starcia
Konformistyczny, unikający kłopotów
Kontemplacyjny, refleksyjny
Kreatywny, tworzący wiecznie coś nowego
Krewki i drażliwy, łatwo wyprowadzić z równowagi
Krytyczny, szuka dziury w całym
Kulturalny i obyty w świecie
Lekceważący i zuchwały
Lekceważy sobie obowiązki i nie doprowadza niczego do końca
Leniwy, marnuje czas
Litościwy, wybacza oraz zawsze skupia się na słabszych
Lojalny i oddany przyjaciołom / grupie
Lubi podróże i non stop zmieniać miejsce zamieszkania
Lubi wyzwania i rywalizację; wszystko traktuje jako rywalizację
Łagodny, próbuje nikogo nie urazić i nie skrzywdzić
Łatwo adaptuje się do sytuacji
Łatwo nawiązuje kontakt, otwarty
Łatwo wywrzeć na tej osobie wrażenie
Ma fatalny styl; zupełnie bez gustu
Ma skłonności do gniewu i wybuchania
Ma wyraźne problemy zdrowotne
Małostkowy; przywiązuje wagę do rzeczy nieistotnych
Maruda, mnóstwo narzeka i wysysa radość
Marzycielski, nie do końca jest w tym świecie
Masochistyczny, czuje przyjemność z cierpienia
Mądry, dużo wie i wykorzystuje tą wiedzę
Melancholijny, patrzy często w przeszłość
Metodyczny, stopniowo rozwiązujący problemy
Mimik, dopasowuje się do stylu osoby, z którą rozmawia
Młodzieńczy, zachowuje się młodziej niż wypadałoby z wieku
Myśli swobodnie, nie ogranicza się do doktryn czy dogmatów
Nadęty i napuszony, lubi jak się schlebia
Nadużywa różnych używek (narkotyki, alkohol)
Naiwny, łatwo wszystko wmówić
Namiętny i żarliwy; płomienny
Napięty i znerwicowany
Narcyz; skupiony na swoim odbiciu i zapatrzony w siebie
Nie dba o potrzeby innych
Nie kłania się nikomu, z podniesioną głową
Nie ma absolutnie żadnych barier czy granicy osobistej
Nie planuje długoterminowo, żyje chwilą
Niechlujny, nie dba o higienę i porządek
Niecierpliwy, chce TERAZ
Niefrasobliwy, beztroski i nieco naiwny
Niekłopotliwy, uległy i posłuszny
Niekompetentny; niezdolny do rywalizacji
Nielojalny, rzuci sojuszników pod autobus
Nienawistny i złowrogi; źle chce dla innych
Nieobliczany, nie wiadomo co zrobi
Nieodpowiedzialny; nie można niczego powierzyć
Niepokojący, jest w tej osobie coś dziwnego
Nierozważny i nieroztropny
Niespokojny, nerwowy, rozbiegany
Nieszczęśliwy i raczej nieudacznik.
Nieśmiały, skrępowany i często zażenowany
Nietolerancyjny, musi być tak jak uważa
Nieuczciwy, z tendencjami do oszukiwania
Niewdzięczny, uważa, że wszystko się należało
Niezależny i nie akceptujący zależności od innych
Niezdyscyplinowany, marnuje czas i energię
Niezgrabny, niezdarny
Nigdy nie wiadomo, co jest prawdą a co maską
Nihilistyczny, nie widzi żadnej nadziei
Niszczycielski, destrukcyjny
Nowoczesny, o stylu bardzo 'metrowarszawskim'
Obojętny, nie wykazuje inicjatywy, robi co każą
Obrażalski, zawsze znajdzie coś za co może się obrazić
Obserwator; raczej stoi z boku i patrzy niż działa bezpośrednio
Oczytany, ceni wiedzę dla wiedzy
Oddany komuś lub czemuś; ogniskuje swoją tożsamość wobec tej idei
Oddany swojemu zadaniu / celowi
Odpowiedzialny, przyjmuje odpowiedzialność
Odważny, rzuca się na problemy jak lew
Odważny, śmiały i Przedsiębiorczy
Ognisty, pełny pasji
Ogromną uwagę przykłada do stylu i ubrania
Okrutny; nie zna litości
Olśniewający i pełen przepychu
Opanowany; niewiele po sobie pokazuje
Opętany szaleństwem; nie działa normalnie
Opiekuńczy, skłonny do ochrony innych
Oportunista; skupia się na korzyściach dla siebie
Opryskliwy, bezceremonialny i obcesowy
Osoba pełna sprzeczności
Osoba starej daty, trochę z poprzedniej epoki
Ostrożny i powściągliwy
Oszczędny, dusigrosz
Otwarty na nowe wrażenia i doznania
Pacyfistyczny, pokojowo nastawiony
Paranoiczny; wszędzie widzi zagrożenie
Patriotycznie nastawiony
Patrzący w dal, skupiony na długoterminowych działaniach
Pedantyczny, z ogromną dbałością o szczegóły
Pełen wdzięczności, raduje się i docenia
Pełny energii, żywy wulkan
Pełny godności, nieco staromodny
Pełny gracji i wytworny
Pełny nienawiści; nieufny i niechętny innym
Pełny szacunku i uszanowania wobec innych
Pełny werwy i animuszu
Pełny wewnętrznej dumy z czegoś
Pełny życia, chce żyć pełnią życia
Perfekcjonista; ważniejszy jest wynik pracy niż skutek / efekt
Perwersyjny, uwielbia szokować i robić skandale
Pewny siebie
Płochliwy, niestały i zmienny
Płytki; zwraca uwagę tylko na to co widać na pierwszy rzut oka
Pochlebczy i obłudny
Pochłonięty niezdrową fascynacją śmiercią i okultyzmem
Poddańczy, ulega sile czy charyzmie
Podejrzliwy i skłonny do szukania pułapek
Polega tylko na sobie; nie wierzy w innych
Pomocny, starający się wesprzeć innych
Ponury, wszędzie widzi ciemność i kracze
Popularny, rozpoznawany i szeroko znany
Posiada określone rytuały i tiki nerwowe
Posłuszny i oddany
Poważny, unika żartów
Powściągliwy, skryty i wyważony
Pozornie nieśmiertelny; sprawia wrażenie nietykalnego
Pracowity, skupiający się na pracy jako na wartości
Praktyczny, skupiony na tym co może zrobić
Prawi wszystkim morał, mówi jak mają żyć
Praworządny, uznaje prawo za najwyższą wartość
Pretensjonalny, sztuczny, chce udawać kogoś innego
Profesorski, mówi mądrze, ale niekoniecznie wobec targetu
Prostacki, prymitywny
Prostolinijny i otwarty; podstępy nie są tu siłą
Prowokacyjny i wyzywający
Prowokacyjny, lubi wywoływać reakcję
Przebiegły i wnikliwy; sprytny
Przebiegły, podstępny, mówi nie wprost
Przebiegły, pomysłowy i zręczny
Przedsiębiorczy i pomysłowy; wszystko załatwi
Przesądny, wierzy w "dziwną" przyczynowo-skutkowość
Przewidujący, trudny do zaskoczenia
Przewidywalny i schematyczny
Przezorny, ostrożny, rozważny, roztropny
Przyjacielski, łatwo nawiązujący znajomości
Przymilny, taki trochę lizus
Przyziemny, stąpa twardo po ziemi
Punktualny i nie tolerujący spóźnień
Purytański, nie toleruje odchyleń od "normalności"
Racjonalny, szuka wyjaśnień
Radosny, cieszy się sam i daje radość
Religijny
Rodzinny, skupiony na życiu rodzinnym
Romantyczny, szukający uniesień i emocji
Roztargniony i nieco zagubiony
Sadystyczny, lubi patrzeć jak inni cierpią
Samokrytyczny, wbija sobie nóż w plecy
Samolubny, skupiony na sobie
Samoniszczący, dążący do straty tego, co ma
Samotnik z natury
Samowystarczalny, trochę pod McGyvera
Sarkastyczny i złośliwy
Sceptyczny wobec prawie wszystkiego
Sentymentalny, podatny na nostalgię
Serdeczny i szczery, momentami rubaszny
Skłonny do manipulacji i podstępów; podstępny i kłamliwy
Skłonny do refleksji i szukania odpowiedzi w przeszłości
Skłonny do szukania odpowiedzi w teorii
Skrępowany i pełny zahamowań
Skromny i unikający pokus ciała
Skromny, pokorny
Skrupulatny i pracowity
Skryty; zachowujący swoją prywatność dla siebie
Skupia się na własnej reputacji jako najważniejszej rzeczy
Skupiony na logice jako głównym narzędziu decyzyjnym
Skupiony na współpracowaniu i sytuacjach win-win
Skupiony przede wszystkim na pieniądzach i korzyściach materialnych
Słodki, uśmiechnięty i do rany przyłóż; wdzięczny; lekko naiwny
Solidny, twardy i niewzruszony w swoich przekonaniach i działaniu
Spokojny, bez pośpiechu; flegmatyczny
Spokojny, trudny do wytrącenia z równowagi
Spolegliwy; niechętnie się kłóci
Spontaniczny, działa pod wpływem emocji
Spostrzegawczy, zauważa rzeczy które innym unikają
Sprawny (wydajny), skupiony na szybkim wykonywaniu działań i działaniach operacyjnych
Stabilny, opoka niemożliwa do ruszenia
Stanowczy i silny, zdecydowany
Stoicki, przyjmuje los i rzeczywistość ze spokojem
Subtelny, zostawia podpowiedzi zamiast mówić wprost
Surowy, wymagający
Swojski, prosty
Szczególnie rozsądny i poczytalny
Szczególnie właściwie traktuje płeć przeciwną
Szczery i nie ukrywający nic przed innymi
Szczery i uczciwy
Szlachetny, skupia się na czynieniu dobra
Sztywny, jak z kijem w... plecach
Świetnie zorganizowany; zawsze wie co, gdzie i kiedy
Tajemniczy, skrywa sekrety pod uśmiechem
Taktowny, liczy się z innymi i ich poglądami
Tchórzliwy, unika ryzyka i kłopotów
Tendencje do podkradania różnych drobiazgów
Tępy i nudny
Tolerancyjny, skłonny do akceptowania wszystkiego, co nie robi bezpośredniej krzywdy
Towarzyski i przyjacielski
Trwożliwy; boi się własnego cienia
Trzeźwo myślący, o jasnym umyśle
Twardy jak podeszwa
Uczciwy i dążący do balansu oraz zadowolenia wszystkich
Uduchowiony, skupiony na tematach mistycznych
Ufny i momentami naiwny
Ugodowy, unika konfliktów - woli przegrać
Ujmujący i sympatyczny
Uległy i posłuszny
Unikający obrażania kogokolwiek i konfliktów
Uparty jak osioł
Uprzedzony wobec konkretnej grupy / klasy
Uroczy, miły w obejściu
Uroczysty i poważny
Uszczypliwy i zgryźliwy
Uważny, zwraca uwagę na szczegóły
W ogóle nie zwraca uwagę na praktyczność, tylko inne aspekty
Wiarygodny; można nań polegać
Wiecznie głodny większej mocy, potęgi i chwały
Wiecznie niepewny, kwestionuje swoje decyzje
Wiecznie zagubiony, działa zgodnie z błędnym modelem
Wielkoduszny, wzniosły. Kieruje się zasadami.
Wierny, skupiający się na swoich przekonaniach
Wnikliwy i odkrywczy
Wrażliwy, łatwy do zranienia
Wszechstronny; człowiek renesansu
Wszędzie musi wsadzić nochal, wścibski
Wulgarny, czy to w mowie czy w stroju
Wybaczający, nie dba o przeszłe rany. Ufny.
Wykształcony, ze szczególną wiedzą w jakiejś dziedzinie
Wymagający, żąda maksimum wysiłku od innych
Wyrachowany, szuka własnego interesu
Wyraźnie czegoś żałuje i okazuje skruchę
Wyznawca teorii spiskowych
Z dobrego domu, trochę arystokratyczny
Z natury odrzuca wszystkie pomysły, trzeba powoli przekonywać
Z natury optymistyczny, ze skłonnościami do "jakoś się uda"
Z poczuciem humoru, lubi żartować
Zadaje mnóstwo pytań, troszkę inkwizycyjny
Zadbany, skupiony na wyglądzie i działaniu
Zadowolony z siebie, beztroski
Zagubiony, rozpaczliwie szuka kogoś, kto da kierunek życia
Zakłóca spokój; nie toleruje spokoju
Zapominalski; non stop trzeba coś przypomnieć
Zatroskany i zaniepokojony
Zawłaszcza osoby i przedmioty
Zawsze bardzo zajęty, wypełnia sobie każdą sekundę, wiecznie w pośpiechu
Zawsze zgodny z najnowszą modą, nowoczesny
Zazdrosny i zawistny
Zbyt pewny siebie, zarozumiały
Zdecydowany, szybko podejmujący decyzje i działający
Zdesperowany, nie cofnie się przed niczym
Zdradliwy; do sukcesu przez nóż w plecach sojusznika
Zdyscyplinowany, skupiony na osiąganiu sukcesów
Zdystansowany, powściągliwy i wyniosły
Ze skłonnościami do dramatyzmu; drama queen
Ze skłonnościami do wymyślania i ubarwiania wszystkiego
Zjadliwy i ostry jak żyleta
Zjadł wszystkie rozumy; zarozumiały
Zmysłowy i bardzo skupiony na ciele i uczuciach
Zniechęca innych, demotywujący
Zniewieściałość dla mężczyzn, męskość dla kobiet
Zrzędliwy i kłótliwy
Zwodniczy i złośliwy przy tym; trickster
Zwracający ogromną uwagę na estetykę
Żarliwy i skłonny do wydania ogromnej ilości energii
Życzliwy, próbuje pomóc i zostawić świat lepszym
