---
layout: inwazja-konspekt
title: "Cechy wyglądu"
---

# {{ page.title }}

; Źródło: http://wizerunekkobiety.pl/wizerunek-aparycja/biogram-opis-siebie/charakterystyczne-cechy-wygladu-zewnetrznego-czlowieka-opis-osoby/

## Wzrost

wysoki, niewysoki, niski, średniego wzrostu, mały, duży

## Sylwetka

szczupła, chuda, smukła, wychudzona, wiotka, delikatna, zgrabna, niezgrabna, wyprostowana, zgarbiona, gruba, tęga, przy kości, krępa, przysadzista, masywna, wysportowana, sportowa

## Twarz

okrągła, pociągła, o wydatnych kościach policzkowych, kwadratowa, pryszczata, piękna, brzydka, nijaka, pospolita

## Głos

niski, wysoki, chrapliwy, cichy, donośny, ciepły, chłodny, miękki, twardy

## Czoło

wysokie, niskie, gładkie, wypukłe, wklęsłe, alabastrowe, gniewne, pomarszczone, błyszczące

## Nos

mały, duży, orli, rzymski, lekko zakrzywiony, krzywy, perkaty, zadarty, kartoflany, wąski, długi, garbaty, wydatny

## Uszy

małe, duże, drobne, wielkie, odstające, wydatne, pokaźne, zarośnięte

## Oczy

duże, małe, wyłupiaste, szeroko rozstawione, wąsko rozstawione, zielone, czarne, piwne, niebieskie, szare, jasnoniebieskie, jasnoszare, jasnozielone, mądre, sarnie, piękne, wyraziste

## Spojrzenie

śmiejące się, radosne, pogodne, smutne, melancholijne, powłóczyste, zamglone, zalotne, frywolne, zdecydowane, surowe, agresywne, spode łba, tępe, bystre, mądre, sarnie, chmurne, ciepłe, zimne, lodowate, przyjazne, nieprzyjazne, błędne, rozbiegane, myślące, bezmyślne, wyraziste

## Usta

pełne, szerokie, wąskie, blade, czerwone, seksowne, wydatne, zacięte, drobne, duże, małe, zaróżowione, wydęte, zaciśnięte, przygryzione

## Brwi 

szerokie, wąskie, wydepilowane, gęste, czarne, ciemne, jasne, siwe, krzaczaste, zrośnięte, bujne, delikatne, delikatnie zarysowana linia brwi, proste, zaokrąglone

## Cera

opalona, śniada, świeża, porcelanowa, ładna, zdrowa, rześka, wypielęgnowana, ogorzała, jasna, ciemna, rumiana, piegowata, blada, ziemista, zniszczona, brzydka, pomarszczona, przesuszona, tłusta, świecąca, zaczerwieniona, plamista, trądzikowa, obwisła, wiotka

## Włosy

długie (do pasa, do ramion), półdługie, krótkie, gęste, słabe, delikatne, mocne, proste, kręcone, podkręcane na końcach, falujące, rozczochrane, uczesane, gładkie, przylizane, spięte, związane, upięte, rozpuszczone, rozwichrzone, bujne, przerzedzone, równo przycięte, lśniące, matowe, elektryzujące się, czyste, brudne, tłuste, przetłuszczone, z łupieżem, nieświeże, elegancko uczesane, utapirowane, polakierowane, zmierzwione, jasne, ciemne, czarne, ciemnobrązowe (brunetka), brązowe, blond (blondynka), jasnobrązowe, lekko szare (szatynka), rude, siwe, farbowane, naturalne, z odrostami

## Fryzura

bujna, rozwichrzona, zmierzwiona, tradycyjna, awangardowa, wymyślna, dziwaczna, szałowa, grzeczna, łysy, z baczkami, z bokobrodami, z zakolami, z dredami, z grzywką, bez grzywki

## Szyja

długa, krótka, łabędzia, smukła, gruba

## Ramiona

szerokie, barczyste, wąskie, umięśnione, drobne

## Ręce (dłonie)

szczupłe, pulchne, szerokie, wąskie, z długimi/krótkimi palcami, spracowane, wypielęgnowane, gładkie, szorstkie, zaczerwienione, delikatne, duże, małe, pomarszczone, chude, smukłe, alabastrowe

## Brzuch

duży, mały, płaski, wypukły, zaokrąglony, obwisły, opalony, blady, ładny

## Pośladki

małe, duże, jędrne, obwisłe, płaskie, okrągłe, zaokrąglone, zgrabne

## Biodra

szerokie, wąskie, rozłożyste

## Talia

wąska, szeroka, wiotka, szczupła, talia osy

## Nogi

długie, krótkie, szczupłe, opalone, blade, masywne, umięśnione, zgrabne, niezgrabne, proste, krzywe, grube, szczupłe, chude, patykowate, pałąkowate

## Uda

grube, szczupłe, zgrabne, masywne, umięśnione, jędrne

## Łydki

umięśnione, chude, grube, jędrne

## Stopy

szerokie, wąskie, drobne, duże, małe, pokrzywione, zadbane, zaniedbane

## Skóra (ciało)

opalona, śniada, jasna, ciemna, jędrna, wiotka, przyjemna w dotyku, aksamitna, lśniąca, delikatna, miękka, blada, pomarszczona, szorstka, wysuszona, tłusta, gruba, popękana, cienka, zwiotczała, sucha