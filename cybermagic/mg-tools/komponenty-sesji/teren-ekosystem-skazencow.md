# Komponent sesji
## Metadane

* Nazwa: Teren - Ekosystem Skażeńców
* Shortdesc: zbiór różnorodnych Skażeńców na podstawie roślin i owadów, kłopotliwi i niebezpieczni

## Koncept wysokopoziomowy

* Zbiór różnorodnych Skażeńców na podstawie roślin i owadów; stanowią problem, ale nie są super-morderczy
* Zwykle występują w Skażonym terenie i stanowią zagrożenie dla pojedynczych osób czy mniejszych grup
* Przede wszystkim istoty organiczne, dostosowane do danego terenu

Odpowiedz na pytania:
    
* Skąd tu się wzięli Skażeńcy tego typu - co jest źródłem Skażenia?
* Jakie są ich dominujące bioformy i z czym przede wszystkim mamy do czynienia?

## Mroczne serce

* "Oczy są wszędzie. Czasem wystają z traw. Tu nie ma większego planu, to jakaś chora magiczna mutacja normalnej fauny i flory"
* "Uważaj na nich, bo część z nich jest mięsożerna a część idzie zgodnie z echem emocjonalnym. I nie wiesz co jest grane."
* **Stan aktualny**: w tym terenie występują grupy i przestrzenie Skażeńców, którzy tworzą własny dziwny ekosystem
* **Stan oczekiwany**: cały ten teren stanowi ekosystem Skażeńców, nie zostało nic 'normalnego' a kto wejdzie, dołączy do ekosystemu
* **Dylemat**: Skażeńcy stanowią jedzenie? Przejść przez niebezpieczny teren czy go ominąć? 
* **Agenda**
    * poluj na wszystkich wchodzących na obszar; może uda się kogoś zaatakować i zjeść z zaskoczenia
    * prezentuj nowy, obcy ekosystem i pokaż, że to już nie jest świat ludzi
    * próbuj dodać każdą biomasę do ekosystemu by powiększyć obszar Skażeniowy
    * zarażaj, Skaź i ukarz każdego kto próbuje wejść lub skrzywdzić teren Skażeńców
    * prezentuj okazje - rzadkie anomalie z których można skorzystać jeśli zaryzykuje się wejście w gniazdo Skażeńców 

## Zasoby i Aspekty

* "_to ich teren_" - mogą być wszędzie, nie wiadomo gdzie się znajdują i działają z zaskoczenia
* "_ślisko, obleśnie i gęsto_" - teren jest zapleśniały, przegniły i łatwo się uszkodzić lub splugawić
* "_gniazda paskudztw_" - potencjalne purchawki emitujące owady lub inne formy, różnego rodzaju gniazda wężopnączy
* "_dziwne mutanty_" - bazowane na ludzkich i zwierzęcych formach, poruszają się i polują
* "_chore trucizny i zarazy_" - rany czy nawet oddychanie przez dłuższy czas Skażonym powietrzem prowadzi do problemów zdrowotnych

## Pokusy i Typowe Porażki

* Ktoś wpakował się prosto w gniazdo Skażeńców, którzy natychmiast oplątują swoją ofiarę
* Skażeniec atakuje z zaskoczenia, porywa / kaleczy kogoś i znika z nim
* Skażeńcy mają coś bardzo wartościowego w jednym z gniazd
* Ktoś panikuje lub robi ruch pod wpływem strachu przez coś przerażającego w Skażonym ekosystemie
* Interesująca anomalia o własnościach które da się wykorzystać
* Bioforma która kiedyś była najpewniej człowiekiem, która ma swój dawny sprzęt

