# Komponent sesji
## Metadane

* Nazwa: Teren - miejsce w ruinie
* Shortdesc: Niestabilne strukturalnie miejsce, które się rozpada ale stanowi okazję do czegoś wartościowego

## Koncept wysokopoziomowy

* Miejsce, w którym się znajdujemy jest w bardzo złym stanie. Jest strukturalnie niestabilne, potencjalnie może coś się zawalić, kable nie mają izolacji itp.
* Trzeba uważać na każdy ruch by się nie zapaść. Samo miejsce jest jedną wielką pułapką.

Odpowiedz na pytania:
    
* Co sprawiło, że struktura budynku / jednostki jest w tak złym stanie?
* Czy są bardziej uszkodzone i mniej uszkodzone fragmenty?

## Mroczne serce

* "Tam są jeszcze wartościowe rzeczy, ale uważałbym - nie wiem co tam żyje a sama ruina grozi zawaleniem"
* **Stan aktualny**: rozpadające się miejsce, które może pełnić rolę schronienia czy źródła czegoś wartościowego
* **Stan oczekiwany**: entropia wygrywa - surowce są niesprawne, w ścianach coś żyje a teren się rozpada i rani przybyszów
* **Dylemat**: Spróbować coś pozyskać i wykorzystać Ruinę ryzykując zdrowiem?
* **Agenda**
    * utrudniaj przejście przez Ruinę. Osuwiska, zawalone rzeczy, ostre krawędzie. Spowalniaj ich.
    * prezentuj okazje z wysokim ryzykiem; czy to 'platformówki' czy konieczność inwestycji zasobów własnych
    * daj możliwość określenia co się tu działo, pokaż logi i osoby które ucierpiały (ranne i martwe)
    * prezentuj potencjalnych nowych mieszkańców terenu, niebezpiecznych i scavengerów, ludzi i istoty
    * prezentuj bardzo niebezpieczne miejsca (uszkodzony reaktor, biolab który wyciekł) jako przeszkody czy okazje

## Zasoby i Aspekty

* "_ten teren jest niestabilny_" - pozornie teren wygląda na normalny, ale się kruszy. Nie spadnij.
* "_cicho, bo coś się zawali_" - wszystko jest kruche i delikatne, trzeba uważać z hałasami.
* "_te ściany da się przebić_" - drzwi zbutwiały. Ściany przerdzewiały. Bariery nie są trwałe.
* "_wszystko zgniło..._" - pojawia się biom charakterystyczny dla gnijących miejsc. Okazja na choroby i pleśnie? Jest ślisko.
* "_uwaga na wyładowania_" - elektryczność iskruje, grożąc ogromnymi zniszczeniami
* "_to wygląda upiornie_" - kiedyś ludzkie dzieło, dziś wygląda upiornie i bez nadziei.

## Pokusy i Typowe Porażki

* Ostre krawędzie rozpadającego się poszycia uszkadzają KOMUŚ skafander
* Niestabilny grunt sprawia, że KTOŚ się częściowo zapada
* Tu powinno być przejście - a jest zawał. KTOŚ nie przejdzie.
* W migoczących światłach awaryjnych COŚ/KTOŚ się ukrywa.
* KTOŚ traci nadzieję, że przejdziecie przez to bezpiecznie.
* KTOŚ odkrył uszkodzony mechanizm, który da się naprawić / wykorzystać jego części.
* Przez tą pozornie trwałą ścianę da się przejść.
* COŚ jest zalane wodą, gdy rury puściły.
* Miejsce składowania odpadów wygląda jak biohazard trzeciego stopnia. A tam jest to czego szukamy.
