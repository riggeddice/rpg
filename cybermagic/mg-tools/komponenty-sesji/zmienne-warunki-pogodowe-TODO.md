### Zmienne Warunki Pogodowe

* **Koncept wysokopoziomowy**
    * Nagłe zmiany warunków pogodowych - zwłaszcza burze czy wiatry - mogą stanowić duże wyzwanie dla podróżujących
* **Agenda** 
    * **Głód**: "Na tym terenie nie jesteś u siebie. Zmiażdżę Cię pogodą."
    * **Dylemat**: Czy kontynuować mimo ryzyka, czy szukać schronienia i czekać na lepsze warunki.
    * Wykorzystaj warunki pogodowe do tworzenia nieprzewidywalnych i trudnych sytuacji.
    * Przedstaw problemy typu: znalezienie schronienia czy kontynuowanie podróży pomimo niebezpieczeństw.
* **Zasoby i Aspekty**
    * "_nagła burza_" - niespodziewana burza może zmusić graczy do szukania schronienia lub ryzykowania w burzy.
    * "_gęsta mgła_" - mgła utrudnia widoczność i orientację, co może prowadzić do zgubienia drogi.
    * "_mordercza ulewa_" - usuwa ślady, usuwa widoczność
* **Pokusy i Typowe Porażki** 
    * W gęstej mgle ktoś może zgubić drogę i oddalić się od reszty grupy; da się rozdzielić.
    * Nagła burza - mokrzy i zmarznięci lub uszkodzenie sprzętu.
    * Ograniczony dostęp do magii i technologii, fantomy na radarach i w komunikatorach
    * Ambush - drapieżnik albo inna grupa
    * Znajdują schronienie, ale jest już zajęte
    * Zmiana terenu z uwagi na problem z pogodą
