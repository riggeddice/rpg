# Komponent sesji
## Metadane

* Nazwa: Frakcja - scavengerzy
* Shortdesc: Grupa złomiarzy z zaawansowanym sprzętem próbujących rozłożyć coś na kawałki i rozkraść co się da

## Koncept wysokopoziomowy

* Grupa złomiarzy z zaawansowanym sprzętem próbujących rozłożyć coś na kawałki i rozkraść co się da
* Agresywnie bonią swojego łupu, ale nie są doskonałymi wojownikami 

Odpowiedz na pytania:
    
* Co próbują rozłożyć i rozkraść scavengerzy?
* Jak są wyposażeni, by móc to osiągnąć? Co daje im główne przewagi?

## Mroczne serce

* "Jeśli coś da się rozkraść, rozkradną. Jeśli kogoś da się sprzedać, sprzedadzą. Niebezpieczni, ale to nie jest oddział wojskowy."
* **Stan aktualny**: grupa salvagerów która już część zasobów zdobyła, dostosowanych do terenu, niedaleko miejsca salvagowanego 
* **Stan oczekiwany**: miejsce salvagowane nie posiada niczego wartościowego, salvagerzy przesuwają się w inne miejsce jak szarańcza
* **Dylemat**: handlować z nimi czy ich okraść? próbować coś wydobyć samemu z wraku mimo ryzyka?
* **Agenda**
    * blokuj innym możliwość salvagowania miejsca; łup należy do nich
    * handluj wszystkim, co salvagerzy posiadają, na bardzo korzystnych dla nich warunkach
    * jeśli widoczna jest słabość, rozważ wymuszenie lub zniewolenie słabych - lub pomoc
    * prezentuj wiedzę o otoczeniu i o specyfice lokacji; salvagerzy są świetnie dostosowani do tego co mają

## Zasoby i Aspekty

* "_znają się na rzeczy_" - salvagerzy wiedzą jak rozłożyć i ocenić każdy komponent, jak wejść i wyjść bezpiecznie
* "_żyjemy dzięki ostrożności_" - działają powoli i metodycznie, mając kręgi alarmów i przymusowych ochotników
* "_cost-efficiency_" - więcej wyciągają niż wkładają, wezmą tylko to co jest wartościowe
* "_sprzęt ciężki_" - nawet jeśli makeshiftowy, to ich sprzęt nadaje się do działania w warunkach ruin

## Pokusy i Typowe Porażki

* Scavengerzy znajdują słabość u kogoś, którą są w stanie wykorzystać
* Scavengerzy pozyskali coś, co jest komuś bardzo potrzebne
* Ktoś wpakował się w typową pułapkę / perymetr obronny scavengerów
* Działamy w obszarze przeznaczonym do rozpadu przez scavengerów
* Możliwość kupienia lub sprzedania czegoś co jest szczególnie przydatne komuś lub scavengerom
* Scavengerzy potrzebują wsparcia w pokonaniu jakiegoś zagrożenia
* Scavengerzy mają mapę lub lokalizację niebezpiecznych rzeczy
