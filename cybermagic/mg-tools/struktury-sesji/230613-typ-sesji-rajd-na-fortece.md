**Nazwa**: Typ Sesji: RAJD NA FORTECĘ

### Koncept wysokopoziomowy

* Infiltracja i eksfiltracja. Wpadamy, bierzemy Target z Ufortyfikowanej Placówki i uciekamy zanim Przeciwnik zdąży zareagować.
* Przykłady:
    * Obóz wojskowy (placówka), z jeńcami wojennymi (Target), których Gracze próbują wydostać przed egzekucją.
    * Laboratorium naukowe w wysokim wieżowcu (placówka), z blueprintami szczepionki (Target), które Gracze próbują wydostać by produkować tańsze generyki w Indiach.
    * Szkoła dla niesfornych nastolatków (placówka), z dzieciakami do 'reedukacji' (Target). Gracze próbują zrobić zdjęcia i przekazać je prasie by zamknąć placówkę.

**Odpowiedz na pytania by doprecyzować:**

* Czym jest Ufortyfikowana Placówka? Jakie są główne przeszkody do pokonania?
    * Jaki System wspiera Placówkę (np. system prawny, mafia...)?
* Czym jest Target, co próbują Gracze (dalej zwani: Rajderami) wydostać?
    * Dlaczego Rajderom na tym zależy? Co z Targetem zrobią? Co dzięki temu się stanie?
* Co sprawia, że Rajderzy są w stanie w ogóle dostać się do Ufortyfikowanej Placówki?

### Sterowanie sesją
    
* **Mroczne serce Sesji**
    * **Głód**: "Status quo jest królem. Zadanie Placówki zostanie wykonane, nieważne jaką ceną."
        * Odeprzeć intruzów, odepchnąć ich lub przemielić Systemem i wpasować w działanie Placówki.
        * Chronić Target przed Rajderami i wszystkimi innymi.
    * **Ofiara**: 
        * Człowieczeństwo: system jako Moloch jest bezduszny. Odbiera człowieczeństwo swoim podwładnych i zmienia ich w trybiki maszyny.
    * **Rola Graczy**: Rajderzy - ci, którzy atakują, wpadają i uciekają z łupem.
        * mimo niesamowitej przewagi Przeciwników prędkość i chaos pozwalają Rajderom na adaptację i działanie.
        * wykorzystywać cechy Placówki i poszczególnych agentów Placówki przeciwko niej samej.
* **Agenda - co chcesz osiągnąć**: 
    * Pokaż siłę i przewagę Placówki i Przeciwnika. Przygotowanie, metodyczność, bezduszność, inevitability.
    * Pokaż, jak ludzie wykonują polecenia Przeciwnika niezależnie od swoich marzeń. System złamał ich i ich system moralny.
    * Odizoluj Rajderów, uwięź ich, przesłuchaj i wykorzystaj potęgę Systemu przeciwko nim.
* **Dark Future - wizja maksymalnej porażki Graczy**:
    * Target i Placówka stały się nieosiągalne, dowody i dane zniszczone. Placówka zabezpieczyła się przed następnym podobnym atakiem.
    * Rajderzy zostali złapani, ich zasoby odebrane.
    * To, co Rajderzy próbują osiągnąć stało się interesujące dla Przeciwników, którzy zrobią kontratak.
* **Przykładowe fazy sesji**
    * (1) Znając słaby punkt Placówki, zdobywanie Zasobów lub wzmocnienie Słabego Punktu
        * _sukces_: mamy sposób wejścia do Placówki
        * _dark future_: Konieczność znalezienia innego słabego punktu plus Przeciwnik wie o operacji Rajderów.
        * _pokusa_: znalezienie sojusznika w środku, poznanie planów Placówki, wzmocnienie słaby punkt, awaryjna droga ucieczki, spowolnienie czasu reakcji Przeciwnika, zdobycie sojuszników
        * _porażka_: Przeciwnik się spieszy, straty w sprzęcie, atak sił Przeciwnika 
    * (2) Infiltracja Placówki i zlokalizowanie Targetu
        * _sukces_: udane uniknięcie wykrycia, zlokalizowanie Targetu
        * _dark future_: Nie ma dobrego dojścia do Targetu. Trzeba będzie przebić się siłowo i liczyć na umiejętność adaptacji.
        * _pokusa_: okazja zdobycia czegoś jeszcze (spełniającego cele), tymczasowe bezpieczne miejsce (przyczółek), uszkodzenie Zasobów Przeciwnika, ułatwienie wycofania się.
        * _porażka_: Target jest w trudnym miejscu, ekstra zabezpieczenia, Przeciwnik lokalizuje Rajderów, Elita poluje na Rajderów, brak bezpiecznego miejsca
    * (3) Pozyskanie Targetu
        * _sukces_: przebicie się przez zabezpieczenia i pozyskanie Targetu. Uruchamia się 'tor alokacji środków Przeciwnika'.
        * _dark future_: Target jest pułapką Przeciwnika i jest już przeniesiony. Elita interweniuje.
        * _pokusa_: przekierowanie uwagi Przeciwnika gdzieś indziej (spowolnienie dywersją), Target jest w dużo lepszym stanie, pozyskanie sojusznika "w środku".
        * _porażka_: uszkodzenie lub utrata Targetu, wzmocnienie zabezpieczeń przez Przeciwnika, ostry limit czasowy, utrata zasobów, krzywdzenie niewinnych
    * (4) Ewakuacja z Placówki
        * _sukces_: opuszczenie Placówki z Targetem
        * _dark future_: Wszystkie drogi ewakuacji są zablokowane i pełne pułapek. Placówka konfrontuje się z Rajderami pełnymi siłami.
        * _pokusa_: dywersja spowolniająca Przeciwnika, uszkodzenie Placówki i jej głównych celów, przez ruchy Przeciwnika pojawia się nieoczekiwana droga
        * _porażka_: odcięcie drogi Rajderom, atak Elit, uszkodzenie lub utrata Targetu, krzywdzenie niewinnych, uwięzienie Rajderów, ostra utrata zasobów, konieczność planu B
    * (5) Uniknięcie kontrataku
        * _sukces_: zatarcie śladów lub fałszywe ślady
        * _dark future_: Przeciwnik natychmiast przystępuje do kontrataku, z pełną wiedzą o miejscu docelowym Rajderów i z zamiarem odzyskania Targetu.
        * _pokusa_: skierowanie Przeciwnika na innego przeciwnika
        * _porażka_: Przeciwnik poluje na Rajderów i/lub ich cel, konieczność wyścigu, uszkodzenie lub utrata Targetu, 
* **Fazy alternatywne, wspierające**
    * Rekrutacja wewnętrznej pomocy
        * _sukces_: zdobycie sojusznika wewnątrz Placówki, który może pomóc w misji
        * _dark future_: Potencjalny sojusznik jest agentem Przeciwnika. Operacja jest bardzo trudna a Przeciwnik przygotowany.
        * _pokusa_: informacje na temat innych celów lub tajemnic Placówki
        * _porażka_: niepowodzenie w rekrutacji, alarmowanie Przeciwnika
    * Sabotowanie działania Placówki
        * _sukces_: utrudnienie Przeciwnikowi reakcji na działania Rajderów
        * _dark future_: Placówka jest przygotowana na sabotaż. Rajderzy są narażeni na większe ryzyko wykrycia i ran. Ostre podniesienie toru Alokacji Środków Przeciwnika.
        * _pokusa_: spowodowanie większych szkód dla Przeciwnika, dodatkowe zasoby
        * _porażka_: alarmowanie Przeciwnika, utrata czasu i zasobów
    * Zakłócanie pościgu
        * _sukces_: utrudnienie Przeciwnikowi śledzenia Rajderów i złapanie ich
        * _dark future_: Przeciwnik dogania Rajderów; nie uniknie się konfrontacji.
        * _pokusa_: dodatkowe zasoby, informacje o ruchach Przeciwnika, przez _fog of war_ Przeciwnik krzywdzi swoje własne zasoby
        * _porażka_: straty w zasobach i sprzęcie, straty w czasie, rany i uszkodzenia, zapędzenie w kozi róg, pomniejsze starcia z pomniejszymi siłami i pułapkami

### Pomoc dla MG

* **Pokusy - co Gracze mogą dostać więcej jeśli ryzykują**
    * Zdobycie dodatkowych informacji lub zasobów z Placówki które pomogą w uzyskaniu Efektu ("czemu robimy rajd")
    * Pozyskanie sojusznika w Placówce, który fundamentalnie zgadza się z Efektem
    * Wywołanie większych szkód dla Przeciwnika niż planowano
    * Target jest w lepszym stanie / coś, co pomoże w ewakuacji Targetu
    * Zdobycie nazwisk lub kontaktów na przyszłość, na analogiczne operacje
    * Znalezienie dowodów umożliwiających zdyskredytowanie / zbudowanie wsparcia
    * Zbudowanie szumu medialnego przeciwko Placówce w okolicy
* **Typowe eskalacje sukcesów**
    * Znalezienie wzoru działań Przeciwnika, który można wykorzystać. Nowa słabość.
    * Zdobycie dodatkowego zasobu, czegoś, co może się przydać
    * Wykorzystanie chaosu do ukrycia swoich działań lub zmylenia przeciwnika
    * Pozyskanie sojusznika w Placówce, nawet nieświadomego / tymczasowo
* **Typowe porażki**
    * Wykrycie przez Przeciwnika, przyspieszenie ich planów lub alarmowanie o obecności Rajderów
    * Wprowadzenie nazwanego członka Elity, któremu szczególnie zależy na tej sprawie (awans? personal stuff?)
    * Wprowadzenie osób niewinnych / niepowiązanych, którym stanie się krzywda jeśli będziemy procedować
    * Czegoś nie wiedzieliśmy. Odcięcie planu; potrzebny jest plan alternatywny
    * Utrata jakichś ważnych zasobów, sprzętu lub konieczność czekania (utrata czasu)
    * Przeciwnik jest lepiej przygotowany do kontrataku - napotkanie niespodziewanych sił
    * Wykonanie akcji jest powiązane z czymś co robi dużą szkodę
    * Potrzebne jest COŚ JESZCZE bo sam Target jest niewystarczający
* **Typowe tory na sesji**
    * _Wykrycie ze strony Przeciwnika_: im wyżej, tym bardziej Przeciwnik wie o obecności Postaci. 
        * Skala [nieświadomy - ma ich dossier].
    * _Alokacja środków Przeciwnika_: im wyżej, tym więcej środków Przeciwnik alokuje do zatrzymania Postaci. 
        * Skala [nie zakłóca działania Placówki - akceptuje uszkodzenie Placówki by zatrzymać Rajderów]
    * _Uszkodzenie Targetu_: im wyżej, tym bardziej Target jest uszkodzony.
        * Skala [Target jest w perfekcyjnym stanie - Target jest zniszczony]

