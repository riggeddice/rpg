**Nazwa**: Typ Sesji: INTEGRACJA ROZBITEJ FRAKCJI

## Koncept wysokopoziomowy

* Mamy Grupę - na przykład: (gang, grupę naukowców, firmę, oddział). Sztab dowodzący tej grupy został zniszczony.
    * Grupa się zatomizowała i pojawiają się starcia wewnętrzne. Każdy próbuje zdobyć coś "dla siebie"
* Zadaniem Graczy jest skonsolidowanie grupy pod swoją kontrolą.
    * Gracze pełną rolę Integratorów (ci, którzy scalają i zmieniają rozbite w całość)
    * Ci, którzy mogą się przydać i są chętni, niech wrócą do Grupy i oddadzą się pod dowodzenie Graczy.
    * Ci, z którymi nie da się współpracować, niech odejdą i zostawią sprzęt i zasoby jeśli się da.
* Na końcu sesji Gracze mają sprawną, skuteczną, acz niepełną Grupę zdolną do skutecznego działania.
* Przykłady:
    * Postacie wchodzą na statek kosmiczny, na którym nie ma oficerów. Ludzie podzielili się na frakcje i kłócą się co robić dalej. Postacie scalają statek do kupy i doprowadzają go do działania.
    * Młody arystokrata spoza planety musi zjednoczyć skłócone plemiona na pustyni, by stworzyć siłę zdolną do obalenia tyranii Imperium mieszkające w mieście.

## Jaka jest rola Graczy podczas tej sesji? Agenda graczy?

* Wizja
    * Głód: "Ta zrujnowana Grupa jest jak pęknięty miecz. Możemy go odbudować i przekształcić w wartościowe narzędzie, ale to wymaga pracy."
    * Stan Aktualny: Grupa jest zatomizowana, różne subfrakcje się zwalczają, każdy ciągnie w swoją stronę, walka o kluczowe zasoby, Grupa jest osłabiona a jej cel - stracony.
    * Stan Porażki: Grupa się rozpadła, subfrakcje przejęły działania, zasoby rozkradzione, jednostki zwalczone i pokonane. "Bankructwo".
    * Stan Pożądany: Grupa jest silna, pod kontrolą Integratorów, większość zasobów jest przejęta, potencjalni zdrajcy wyplenieni i wszystko co ważne zostało w grupie.
* Strategie
    * Identyfikacja subfrakcji od której zacząć integrację i przejąć kontrolę
    * Zdobywanie zaufania poszczególnych subfrakcji i integrowanie ich ze swoją
        * Usuwanie kluczowych członków sprzecznych / szkodliwych subfrakcji
    * Umacnianie swojej subfrakcji i zwalczanie innych subfrakcji
        * Zwalczanie dezintegrujących Grupę subfrakcji lub subfrakcji o sprzecznych celach z Graczami
        * Walka o kluczowe surowce, zasoby i sojuszników
        * Motywowanie i dawanie wspólnego kierunku i celu Grupie i subfrakcjom
    * Zmniejszanie ilości konfliktów
        * Mediacja i negocjacje w celu zakończenia konfliktów i sprzeczek między subfrakcjami
        * Tworzenie warunków do współpracy, demonstracja obszarów potencjalnej współpracy
    * Ochrona i zabezpieczanie Grupy przed zewnętrznymi wrogami
    * Budowanie nowej kultury, nowa identyfikacji, nowego celu. Stara Grupa umarła, nowa jest feniksem.

## Sterowanie sesją
### Mroczne serce Sesji
#### Agenda - co chcesz osiągnąć?

* Wizja
    * Głód: "Ta zrujnowana Grupa ma mnóstwo wartościowych zasobów i subfrakcji. Zakończmy tą farsę, poodrywajmy co się da i zostawmy to miejsce gorszym i słabszym."
    * Stan Aktualny: Grupa jest zatomizowana, różne subfrakcje się zwalczają, każdy ciągnie w swoją stronę, walka o kluczowe zasoby, Grupa jest osłabiona a jej cel - stracony.
    * Stan Docelowy: Po Grupie zostało wspomnienie - subfrakcje mają wojnę domową, siły wewnętrzne rozebrały wszystko co się dało, zewnętrzne rozkradły i nie zostało nic o wartości.
    * Konsekwencje: ofiarą staje się szersze Bezpieczeństwo i Stabilność. Brak Grupy prowadzi do utraty pewnych możliwości i utraty tego, co Grupa wypracowała.
* Strategie
    * Sabotaże, zabójstwa i działania dezintegracyjne
    * Gra na konfliktach między subfrakcjami
    * Gra na chciwości jednostek i walka o zasoby
    * Gdzie dwóch się bije tam trzeci korzysta. Gdzie dwóch chce się integrować, tam trzeba zrobić intrygę.
    * Sojusze z siłami zewnętrznymi
* Zasoby
    * Ambitne subfrakcje o różnych celach - każdy ciągnie do swojego celu
    * Subfrakcja dążąca do integracji Grupy pod zupełnie nowym celem i banderą
    * Osoby, które utraciły wiarę w cel Grupy i próbują ukraść co się da i uciec
    * Konflikty wewnętrzne i resentymenty, jakie naturalnie pojawiają się między ludźmi wspólnie pracującymi
    * Siły zewnętrzne, widzące słabość Grupy próbujące wyciągnąć kluczowych członków, napaść lub rozkraść

#### Dark Future

* Grupa jest zniszczona, pozostaje po niej jedynie wspomnienie. Odbudowała się subfrakcja twierdząca, że są nimi dalej, ale cel jest inny i nie stanowi nawet części siły Grupy.
* Wszystkie subfrakcje są w stanie wojny domowej, walcząc o ochłapy. Siły zewnętrzne i oportuniści wyszarpują co się da. Wyższy cel jest nieosiągalny.
* Integratorzy coś wyszarpali, ale nie udało im się tego ani zintegrować ani przejąć kontroli nad niczym szczególnie ważnym.

### Fazy sesji
#### Główne fazy

**(1). Znalezienie i pozyskanie dobrej subfrakcji od której można zacząć Integrację**

* _działania_
    * szukanie najlepszej subfrakcji, analiza subfrakcji, negocjacje - czemu Integratorzy są najlepszą opcją, budowanie sojuszu
* _sukces_
    * Integratorzy są w sztabie subfrakcji od której można zacząć integrację. Subfrakcja oddała się pod dowodzenie Integratorów, lub z nimi współpracuje
* _dark future_ 
    * znaleziona subfrakcja jest bardzo słaba i rozbita, jej siły są nieufne Integratorom i inna silniejsza frakcja próbuje integrować pod swoją banderą.
* _pokusa_: 
    * subfrakcja ma szczególnie cenny zasób, subfrakcja ma reputację, subfrakcja ufa Integratorom, subfrakcja ma szczególnie dobre kontakty z kimś
* _porażka_: 
    * subfrakcja jest osłabiona, Integratorzy nie mają zaufania subfrakcji, subfrakcja ma szczególnych wrogów lub kiepski teren

**(2). Obrona swojej subfrakcji przed innymi subfrakcjami (oraz siłami zewnętrznymi), 'stop the bleeding', ustabilizowanie swojej roli dowódczej**

* _działania_
    * fortyfikacja słabych punktów, walka ze zdrajcami, ochrona zasobów, zastawianie pułapek, motywowanie i budowanie kultury
* _sukces_ 
    * Integratorzy mają stabilną i bezpieczną subfrakcję, wzmocnioną przez wchłonięcie pomniejszych płotek i pozbycie się jednostek problematycznych
* _dark future_ 
    * Integratorzy musieli opuścić subfrakcję; nie tylko została rozbita ale nie pełni już roli możliwej integracji
* _pokusa_: 
    * pułapka by dodać cenne osoby do subfrakcji, pokazanie siły by zniechęcić innych do ataku, sukces mający motywować i integrować
* _porażka_: 
    * subfrakcja ma słaby punkt, mamy w środku zdrajców i sabotażystów, straciliśmy cenne zasoby

**(3). "Fire and motion" - zajmowanie innych subfrakcji i ważnych zasobów zanim zrobią to inni; wojna o subfrakcje**

* _działania_
    * identyfikacja kluczowych celów, szybka integracja, budowanie sojuszy i negocjacje, ataki, pułapki i obrony ważnych zasobów, osłabianie przeciwników
* _sukces_ 
    * Integratorzy wydobyli kluczowe zasoby i skonsolidowali słabsze frakcje. Powstają 2-3 stabilne frakcje, Integratorzy są jedną z najsilniejszych.
* _dark future_ 
    * Integratorzy mają małą i nikczemną subfrakcję, nie są w stanie nią integrować. Muszą się oderwać zanim coś większego ich pożre.
* _pokusa_: 
    * zdobycie czegoś szczególnie wartego, skłonienie silnej subfrakcji do oddania czegoś, budowanie sojuszu zewnętrznego, zbudowanie odpowiedniej reputacji wśród kogoś, znalezienie tajnego spotkania innych stron
* _porażka_: 
    * ostry konflikt z inną subfrakcją, utrata czegoś ważnego, strata ważnej grupki do innej subfrakcji, brak zasobów do utrzymania wszystkiego

**(4). Konfrontacja z głównymi rywalami o kluczowe zasoby**

* _działania_
    * ostra walka o kluczowe zasoby, zastraszanie i przekonywanie że Integratorzy są silniejsi, eksploatacja słabych punktów wroga, polityka i zabójstwa
* _sukces_
    * Integratorzy mają najsilniejszą frakcję. Pozostałe frakcje nie są w stanie złamać zintegrowanej i nie stanowią dość przeszkód. Integratorzy mają co potrzebowali.
* _dark future_
    * Integratorzy mają stabilną frakcję, ale są silniejsze. Nie ma kluczowych zasobów, ale to co jest jest zabezpieczone. Utracone zaufanie. Trzeba się wycofać.
* _pokusa_
    * napuszczenie rywali na siebie, zrzucenie z siebie uwagi trzeciej strony, zdobycie podwójnego agenta u przeciwnika
* _porażka_
    * utrata kluczowych sił do rywali lub trzeciej strony, wyniszczenie dwustronne, utrata terytorium i zaufania

**(5). Ostateczna Konsolidacja - które zasoby zdążymy utrzymać, które subfrakcje zintegrować? Co zatrzymamy zanim zabezpieczymy okno kradzieży i ucieczki?**

* _działania_
    * decyzja co chronimy pod kątem CELU, wysyłanie oddziałów i fortyfikowanie, motywowanie i osobiste konfrontacje, utrzymanie morale, zabezpieczenie kluczowych zasobów
* _sukces_
    * zachowaliśmy większość siły oryginalnej Grupy i mamy lojalną Grupę do naszego celu. Grupa jest słabsza niż oryginalnie, ale jest sprawna i zmotywowana
* _dark future_
    * docelowo, nasza Grupa straciła większość swojej siły i cennych zasobów. Musimy odbudowywać z czegoś małego i słabego z groźnymi zewnętrznymi siłami. Grupa niestabilna i zdemoralizowana.
* _pokusa_
    * szczególnie wartościowe zasoby, nowe kontakty chcące współpracować z Nową Grupą, wprowadziliśmy przeciwników w błąd
* _porażka_
    * straciliśmy coś szczególnie cennego, polityczne reperkusje, utrzymaliśmy 'za dużo' i przez to mamy niestabilność w Grupie, trudne moralnie i politycznie decyzje

#### Alternatywne fazy

(5). Wykonanie zadania - po Konfrontacji jeśli nie trzeba Konsolidacji to możemy wykonać główne zadanie Grupy przed rozpadem.

### Przygotowanie przed sesją

Odpowiedz na pytania:

* Czym jest Grupa? Czemu jej istnieje jest wartościowe?
* Dlaczego Grupa została bez głowy / dowodzenia?
* Jakie są 3-4 Kluczowe Zasoby należące do Grupy?
    * o to toczy się walka
* Jakie są 2-3 siły z różnymi Agendami w ramach Grupy?
    * to są przeciwne znaczące frakcje
* Jakie są 4-5 pomniejsze siły w ramach Grupy?
    * jedna z nich jest potencjalnie do przejęcia przez Graczy

### Dodatkowe narzędzia
#### Pokusy - co Gracze mogą dostać więcej jeśli ryzykują

?

#### Typowe eskalacje sukcesów

?

#### Typowe porażki

?

#### Typowe tory na sesji

?
