**Nazwa**: Typ Sesji: RATUNKOWA

### Koncept wysokopoziomowy

* Film katastroficzny z perspektywy ratowników.
* "Iria" / "Aliens 2" - mamy normalne miejsce (baza / statek kosmiczny) gdzie pojawia się Niepowstrzymana Katastrofa (potwór) i musimy uratować kogo się da.
* "Titanic" - luksusowy statek wycieczkowy zderzył się z górą lodową. Niepowstrzymaną Katastrofą jest zatonięcie statku. Ratujemy kogo się da wśród różnorodnych frakcji i agend.

**Odpowiedz na pytania by doprecyzować:**

* czym jest Niepowstrzymana Katastrofa? Czemu jest Niepowstrzymana?
* czym jest Normalne Miejsce gdzie doszło do Katastrofy?
* co będzie stać na drodze Ratowników? Jaki typ Hazardów i problemów?

### Sterowanie sesją
    
* **Mroczne serce Sesji**
    * **Głód**: "Człowiek i cywilizacja są NICZYM w obliczu Katastrofy"
        * zniszczenie wszystkiego co osiągnął człowiek przy użyciu Katastrofy.
        * pokazanie mrocznej natury człowieka, Katastrofa jest pretekstem.
    * **Ofiara**: człowieczeństwo i nadzieja
        * ludzie walczą między sobą o przetrwanie, kradzieże nad pomoc bliźnim.
    * **Rola Graczy**: nadzieja i lepsze jutro
        * mimo potworności Katastrofy, są jednostki zdolne i chętne do pomocy.
        * pomóżcie ludziom, wyciągnijcie co się da, dajcie im nadzieję na jutro.
* **Agenda - co chcesz osiągnąć**: 
    * Pokaż nikłość i bezradność ludzi w obliczu Katastrofy. Ich pomysły są niczym w obliczu Katastrofy.
    * Pokaż jak ludzie walczą o przetrwanie i o "swoje" w obliczu Katastrofy, robiąc sobie krzywdę.
    * Zniszcz jak najwięcej ludzi i dóbr. Nic nie zostanie.
* **Dark Future - wizja maksymalnej porażki Graczy**:
    * Wszyscy zginęli, jeden po drugim. Nie mieli żadnych szans.
    * Nikt nie dowiedział się, skąd Katastrofa się wzięła.
    * Ratownicy tracą sprzęt, są poranieni i sami potrzebują pomocy. Część zginęła.
    * Teren jest uszkodzony lub stracony.
* **Przykładowe fazy sesji**
    * (1) Bezpieczne wejście do Problematycznego Miejsca i zrobienie przyczółka
        * _sukces:_ Istnieje dobry, bezpieczny Przyczółek będący wejściem do Problematycznego miejsca.
        * _dark future:_ Nie ma bezpiecznego Przyczółka i miejsca, ale operacja trwa. Zespół jest odizolowany od bazy. Utrata większości zasobów.
        * _pokusa:_ dodatkowe informacje o Miejscu lub Problemie, uratowanie kogoś wcześniej, budowa zaufania wśród ratowanych.
        * _porażka:_ przyczółek jest niebezpieczny, utrata części zasobów, konieczność szybkiego działania, niestabilność Miejsca
    * (2) Plan ratowania ludzi
        * _sukces_: Zespół zlokalizował ludzi do uratowania. Zespół wie jaka Katastrofa jest w Miejscu i mniej więcej wie z czym ma do czynienia.
        * _dark future_: Konieczność rozdzielenia się, działania pod wpływem stresu w dużym tempie. Nie ma zasobów. Sporo ludzi ginie. Konieczność konfrontacji z Katastrofą. Ratowani biegają jak kurczaki bez głowy i mają dziwne agendy.
        * _pokusa_: możliwość zdobycia informacji o Katastrofie lub Miejscu, możliwość uratowanie jeszcze kogoś, budowa zaufania wśród ratowanych
        * _porażka_: konieczność rozdzielenia się, dodatkowe komplikacje, konieczność szukania ludzi, ludzie są w niefortunnych miejscach, konieczność eksploracji Miejsca, ratowani ludzie mają swoje agendy
    * (3) zabezpieczenie drogi do ludzi przed Katastrofą
        * _sukces_: Zbudowanie bezpiecznej od Katastrofy drogi, inżynieria w Miejscu, zbudowanie dostępu do ludzi
        * _dark future_: Katastrofa pożera Miejsce, nie ma dostępu do ludzi. Miejsce się rozpada. Konieczność ewakuacji Ratowników.
        * _pokusa_: j.w. PLUS możliwość pozyskania zasobów ze struktury Miejsca, zabezpieczenie fragmentu Miejsca, deeskalacja konfliktów wśród Ratowanych
        * _porażka_: Katastrofa się rozprzestrzenia, coś ważnego się rozpada, dodatkowe VIPy z problemami, "zadanie ważniejsze niż ludzie", ratowani ludzie mają swoje agendy, chaos i nic nie wiadomo, utrata sprzętu
    * (4) uratowanie ludzi przed Katastrofą
        * _sukces_: Udało się uratować wszystkiego i wszystko co dało się uzyskać.
        * _dark future_: Katastrofa pożera Miejsce, nie ma dostępu do ludzi. Miejsce się rozpada. Konieczność ewakuacji Ratowników.
        * _pokusa_: j.w.
        * _porażka_: Katastrofa się rozprzestrzenia, coś ważnego się rozpada, ludzie obracają się przeciw ludziom, wszyscy walczą o "swoje", ludzie umierają
    * (5) wycofanie się na bezpieczną odległość zanim Katastrofa "wygra"
        * _sukces_: Zespołowi udaje się wycofać na bezpieczną odległość i zabezpieczyć uratowanych.
        * _dark future_: Ratownicy widzą, jak wszystko czego nie udało im się uratować jest pochłonięte przez Katastrofę. Dalej nie wiadomo skąd Katastrofa się wzięła. Ratownicy poranienie, nie wszyscy przetrwali.
        * _pokusa_: coś ważnego na pokładzie do wyciągnięcia, zatrzymanie Katastrofy przed overloadem
        * _porażka_: j.w.

### Pomoc dla MG

* **Pokusy - co Gracze mogą dostać więcej jeśli ryzykują**
    * Pomóż ludziom uratować coś cennego - więcej niż tylko życie
    * Dowiedz się co tu się stało i z czego wynika ta katastrofa, zbierz dowody
    * Zapewnij zasoby dla zespołu ratowników na przyszłość
    * Spraw, by ten typ Katastrof nie pojawił się w przyszłości
* **Typowe eskalacje sukcesów**
    * Udało się uratować kogoś więcej
    * Gracze dowiadują się coś o tym, co tu się stało
    * Graczom udało się pozyskać surowce które można dalej wykorzystać przy operacji ratunkowej
    * Graczom udało nam się zdobyć zaufanie i pochwałę przełożonych lub prasy
    * Gracze dostają dostęp do obszaru, który przedtem był odcięty
* **Typowe porażki**
    * Pojawia się presja czasowa - ktoś kogo możemy uratować jest zagrożony
    * Katastrofa się rozprzestrzenia i obejmuje następny obszar
    * Pojawia się dodatkowa sub-Katastrofa powiązana
    * Wyposażenie ratowników zostaje uszkodzone LUB brakuje im zasobów
    * Ratownicy sami wpadają w kłopoty
    * Nowe okoliczności - Ratownicy muszą zmienić plan bo coś jest inaczej niż wyglądało
    * Coś ważnego wybucha, coś ulega uszkodzeniu
    * Ratowani działają zgodnie ze swoją agendą, szkodząc innym Ratowanym lub Ratownikom
