**Nazwa**: Typ Sesji: ZATRZYMAĆ FAILURE CASCADE

## Koncept wysokopoziomowy

* Mamy Złożony System - na przykład: (elektrownia atomowa, statek kosmiczny, sieć komputerowa, miasto). 
    * Jakaś kluczowa część tego Systemu uległa awarii.
    * W wyniku tej awarii, zaczyna następować lawina innych awarii i problemów. Każdy uszkodzony element prowadzi do uszkodzenia kolejnego.
* Zadaniem Graczy jest zatrzymanie lawiny awarii, naprawa Systemu i przywrócenie go do stanu sprawności.
    * Gracze pełnią rolę Mechaników - inżynierów, naukowców, techników, społeczników, którzy mają za zadanie zatrzymać awarię i zredukować zniszczenia.
    * Gracze balansują pomiędzy: 
        * atakowaniem źródła problemu, by rozwiązać to i nie dopuścić do niekontrolowanej kaskady
        * ratowaniem 'celów pobocznych', by zapobiec stratom w ludziach i zasobach
        * ograniczaniem i wzmacnianiem jeszcze działających podsystemów, by mieć bezpieczne przyczółki i coś na czym można polegać
* Na końcu sesji, Gracze powinni być w stanie przywrócić Złożony System do działania (lub go wygasić), choć będą konsekwencje długoterminowe.
* Przykłady:
    * Na statku kosmicznym awaria jednego z modułów prowadzi do lawiny problemów w innych systemach. Mechanicy muszą zidentyfikować i naprawić źródło problemu, a następnie zająć się skutkami awarii.
    * Po tym, jak Antek zdradził Basię, zaczęły pojawiać się plotki i cały złożony system zaczął się sypać - ostracyzm, 'nie gadam z Tobą' itp. Mechanicy muszą raz na zawsze przeciąć podchody i doprowadzić do współpracy wystarczającej masy krytycznej.

## Jaka jest rola Graczy podczas tej sesji? Agenda graczy?

* Wizja
    * Głód: "Złożony System się rozpada. Z jedną uszkodzoną częścią zaraz kolejna się rozsypuje. Musimy zatrzymać lawinę awarii zanim wszystko się rozsypie w sposób nieodwracalny."
    * Stan Aktualny: Złożony System już jest uszkodzony, co prowadzi do dalszej kaskady zniszczeń. Skutki pierwszej awarii są coraz bardziej odczuwalne. Zagrożone są kolejne części systemu.
    * Stan Porażki: _Failure Cascade_ nie została zatrzymana, doszło do najmocniejszej możliwej erupcji, są straty w ludziach i mieniu i System nie będzie już nigdy działał jak zawsze.
    * Stan Pożądany: _Failure Cascade_ została zatrzymana, acz część rzeczy wymaga wymiany. System można uruchomić ponownie, skutki awarii są odwracalne.
* Strategie
    * Mechaniczne radzenie sobie z awarią
        * Identyfikowanie objawów awarii i rozwiązanie problemu lub leczenie objawowe.
        * Przewidywanie kolejnych awarii i reagowanie na nie z wyprzedzeniem.
        * Naprawa lub zastąpienie kluczowych uszkodzonych elementów Systemu.
        * Zrozumienie praprzyczyny awarii i skonstruowanie rozwiązania mającego zatrzymać _Failure Cascade_
    * Zarządzanie morale i strachem ludzi, kierowanie zasobów w odpowiednie miejsce
        * Zarządzanie kryzysem, utrzymanie porządku i zapewnienie bezpieczeństwa.
        * Ewakuowanie zagrożonych osób, 
    * Zapewnienie głównych funkcjonalności Systemu mimo kaskadującej awarii
        * Budowa rozwiązań tymczasowych, przyczółka do dalszej pracy    
        * Przywracanie funkcji Systemu i minimalizowanie skutków awarii.
    * Wyciąganie wniosków i lekcji z kryzysu, poprawa Systemu w celu uniknięcia powtórzenia sytuacji.

## Sterowanie sesją
### Mroczne serce Sesji
#### Agenda - co chcesz osiągnąć?

* Wizja
    * Głód: ""
    * Stan Aktualny: 
    * Stan Docelowy: 
    * Konsekwencje: 
* Strategie
    * 
    * 
    * 
    * 
    * 
* Zasoby
    * 
    * 
    * 
    * 
    * 

#### Dark Future

* 

### Fazy sesji
#### Główne fazy

**(1). **

* _działania_
    * 
* _sukces_
    * 
* _dark future_ 
    * 
* _pokusa_: 
    * 
* _porażka_: 
    * 

**(2). **

* _działania_
    * 
* _sukces_
    * 
* _dark future_ 
    * 
* _pokusa_: 
    * 
* _porażka_: 
    * 

**(3). **

* _działania_
    * 
* _sukces_
    * 
* _dark future_ 
    * 
* _pokusa_: 
    * 
* _porażka_: 
    * 

**(4). **

* _działania_
    * 
* _sukces_
    * 
* _dark future_ 
    * 
* _pokusa_: 
    * 
* _porażka_: 
    * 

**(5). **

* _działania_
    * 
* _sukces_
    * 
* _dark future_ 
    * 
* _pokusa_: 
    * 
* _porażka_: 
    * 

#### Alternatywne fazy

.

### Przygotowanie przed sesją

Odpowiedz na pytania:

* .

### Dodatkowe narzędzia
#### Pokusy - co Gracze mogą dostać więcej jeśli ryzykują

?

#### Typowe eskalacje sukcesów

?

#### Typowe porażki

?

#### Typowe tory na sesji

?
