**Nazwa**: Typ Sesji: ODKRYCIE SEKRETU

### Koncept wysokopoziomowy

* Jest to wariant sesji detektywistycznej, który nie jest morderstwem. Mamy Sekret który ukrywa [Ofiarę / Potencjalną Korzyść].
    * Grzebanie w książkach, gazetach, dokumentach. Rozmowa z ludźmi. Zebranie faktów do kupy i zrozumienie co się tu dzieje.
* Zadaniem Graczy jest poznanie Sekretu i decyzja - ujawniamy lub nie. Co robimy z tym _dalej_.
* Przykłady:
    * Początkujący dziennikarz zniknął (Ofiara), gdy próbował dowiedzieć się kto sprzedaje ziarna do silosa w Karlowinie (Sekret).
    * Od lat mówi się o kolekcji rzadkich monet pana Wielokołowca (Korzyść). Ale po jego śmierci 140 lat temu nikt jej już nie widział - gdzie są (Sekret)?
    * Dwa Pokolenia temu dziadek umarł w tajemniczych okolicznościach (Ofiara) a w testamencie przepisał wszystko Kultowi (Strażnik Sekretu). Dziś czas dowiedzieć się jak umarł dziadek (Sekret).
    * książka "Killing Floor" z Jackiem Reacherem, autor: Lee Child. Jest tam sekret (konspiracja), prawie wszyscy są jakoś tym dotknięci i Reacher rozwiązuje Sekret.

**Odpowiedz na pytania by doprecyzować:**

* Czym jest Ofiara? Kto / co cierpi przez to, że Sekret nie wyszedł na jaw?
* Co się zmieni, jeśli Sekret wyjdzie na jaw? Kto straci a kto zyska?
    * To daje nam zainteresowane frakcje: Strażnicy Sekretu oraz Beneficjenci Prawdy
        * Kim są Strażnicy i kim Beneficjenci?
* Czym jest Sekret? Dlaczego nikomu wcześniej nie udało się go odkryć? A jeśli tak - czemu go nie zdradzili?

### Sterowanie sesją
    
* **Mroczne serce Sesji**
    * **Głód**: "Niektóre Sekrety dlatego są sekretami, bo ich ujawnienie zniszczy za wiele"
        * Ukrywaj Sekret - Sekret nie powinien nigdy wyjść na światło dzienne. Zbyt wiele zmieni i zrani.
        * Pokaż, jak Sekret (i jego ujawnienie) niszczy tych, którzy go znają i jak zmienił życie ludzi dookoła.
    * **Ofiara**: Uczciwość, prawość i sprawiedliwość
        * Strażnicy Sekretu żyją w kłamstwie i mają krew na rękach. Beneficjenci cierpią przez Sekret.
    * **Rola Graczy**: Poszukiwacze Prawdy, decydenci odnośnie przyszłości
        * Poszukiwacze Prawdy, którzy muszą przeciwstawić się Strażnikom Sekretu i połączyć fakty w logiczną całość
        * Po poznaniu prawdy, czeka ich decyzja - Sekret zostaje zachowany czy Prawda ujawniona. Co dalej z Beneficjentami i Strażnikami.
* **Agenda - co chcesz osiągnąć**: 
    * Sekret musi pozostać ukryty i Strażnicy posuną się bardzo daleko by tak pozostało.
    * Konsekwencje ujawnienia Sekretu zniszczą życie winnych i niewinnych. Teraz i Gracze są uwikłani w wielki Sekret.
    * Pokaż skutki Sekretu, uwikłanie niewinnych ludzi w okolicy. Pokaż konspirację i konsekwencje. 
    * Pokaż jak daleko niektóre osoby mogą posunąć się, aby ukryć prawdę. Niszcz świadków, wprowadzaj fałszywe ślady.
* **Dark Future - wizja maksymalnej porażki Graczy**:
    * Sekret pozostaje poza zasięgiem Poszukiwaczy; nie dowiedzieli się co się stało
    * Sojusznicy Poszukiwaczy zostali zastraszeni i złamani. Gracze zostają zdemaskowani, ich reputacja jest zniszczona lub są w inny sposób ukarani. Lub dołączyli do Strażników.
    * Nic się nie zmieniło - Sekret pozostaje ukryty, a Ofiara nadal cierpi. Strażnicy Sekretu kontrolują sytuację jeszcze lepiej.
* **Przykładowe fazy sesji**
    * (1) Poszukiwacze dowiadują się, że jest tu Sekret. Impuls do rozpoczęcia śledztwa.
        * _sukces_: Poszukiwacze identyfikują problem. Widzą konsekwencję Sekretu. Mają następny krok jak odkryć co się tu dzieje.
        * _dark future_: Strażnicy dowiadują się o Poszukiwaczach. Poszukiwacze nie mają następnego kroku - muszą wywalczyć go od Strażników nie wiedząc o ich obecności. "Coś tu jest bardzo nie tak".
        * _pokusa_: więcej powiązanych faktów, ślady obecności Strażników, zaufanie Beneficjentów
        * _porażka_: więcej fałszywych tropów, dezinformacji, brak zaufania potencjalnych sojuszników, sojusznik powiązany ze Strażnikami
    * (2) Tysiące tropów - fałszywe i prawdziwe. Zbieranie informacji.
        * _sukces_: Poszukiwacze odróżnili prawdziwe tropy od fałszywych, zaczynają rozumieć, co się tu dzieje. Wiedzą o istnieniu Sekretu, Strażnika i Beneficjentów. Strażnicy wiedzą o Poszukiwaczach.
        * _dark future_: Poszukiwacze wyciągają złe wnioski i idą za złymi tropami. Zmarnowali mnóstwo czasu, mają Strażników jako sojuszników i uważają Beneficjentów i Ofiary za problem.
        * _pokusa_: odkrycie prawdziwej przesłanki, odkrycie czegoś o Strażnikach, szansa na nowych sojuszników, zmuszenie Strażników do innych planów
        * _porażka_: demonstracja konsekwencji ujawnienia Sekretu, demonstracja siły Strażników, zastraszenie sojuszników, sojusznicy też nie są kryształowi, odkrycie fałszywej przesłanki, sojusznik czegoś nie zrobi ze strachu
    * (3) Hipoteza prawdy - niekompletny pierwszy obraz Sekretu.
        * _sukces_: Poszukiwacze mają pierwszy obraz Sekretu, rozumieją konsekwencje jego ujawnienia - dla Beneficjentów, Ofiar i dla Strażników. Czas odkryć to, co Sekret ukrywa / znaleźć dowody.
        * _dark future_: Poszukiwacze przez błędne przesłanki i tropy stają po stronie Strażników. Strażnicy pomagają Poszukiwaczom zniszczyć Ofiary i Beneficjentów. 
        * _pokusa_: potwierdzenie jakichś faktów jako prawdziwych, uszkodzenie zasobów/sił Strażników, przekonanie sojuszników, wyciągnięcie ze Strażników zdrajcy, poznanie faktów przeszłości
        * _porażka_: Strażnicy usuwają sojusznika, Strażnicy robią bezpośredni atak, utrata zaufania sojuszników, jakiś subfakt jest wadliwy, Poszukiwacze idą w złą stronę
    * (4) Konfrontacja z Sekretem
        * _sukces_: Poszukiwacze wiedzą już, co jest ukryte przez Sekret i mają na to dowody. Żadna ze stron nie jest już w stanie powstrzymać Poszukiwaczy.
        * _dark future_: Poszukiwacze może mają wiedzę, ale nie mają dowodów i są zmuszeni do ewakuacji - przeważające siły Strażników na nich polują. Lub: "nigdy nie znajdą dowodów".
        * _pokusa_: neutralizacja działania Strażników, zabezpieczenie Beneficjentów, pozyskanie dodatkowych Zasobów
        * _porażka_: utrata części dowodów, Strażnicy usuwają sojusznika, Strażnicy robią bezpośredni atak, utrata zaufania sojuszników
    * (5) Decyzja - co Poszukiwacze zrobią z Sekretem? Jak się skończy ta historia?
        * _sukces_: Sekret jest utrzymany albo ujawniony. Beneficjenci, Ofiary i Strażnicy dostają taką przyszłość, jakiej oczekują Gracze.
        * _dark future_: Mimo decyzji Poszukiwaczy, inne strony skutecznie zawłaszczyły dobra / zasoby i przyszłość nie potoczyła się tak jak Graczy chcieli
        * _pokusa_: zwiększenie harmonii i zmniejszenie cierpienia interesariuszy, więcej dla Poszukiwaczy, redukcja cierpienia z ujawnienia Sekretu
        * _porażka_: pomoc jednym krzywdzi innych, niektóre przeszłe czyny są niewybaczalne, część zasobów ukradziono, kłótnie między Beneficjentami
* **Fazy alternatywne, wspierające**
    * Sojusznik w niebezpieczeństwie - ktoś, kto pomógł Poszukiwaczom jest teraz bardzo zagrożony przez Strażników i Poszukiwacze o tym wiedzą
        * _sukces_: Poszukiwacze ocalają sojusznika, uszkadzają bezpośrednie siły Strażników i zwiększają lojalność sojusznika. Dowiadują się o czymś przydatnym.
        * _dark future_: Sojusznik ginie z rąk Strażników, a Poszukiwacze tracą cenne wsparcie i zaufanie innych potencjalnych sojuszników.
        * _pokusa_: dodatkowe uszkodzenie sił Strażników, dodatkowe pozyskanie zasobów, poznanie faktów, zwiększenie odwagi innych sojuszników, destrukcja morale Strażników
        * _porażka_: Strażnicy zastraszają innych sojuszników, Strażnicy dostają wsparcie siłowe, Strażnicy kogoś porywają
    * Zdrajca w Zespole - wśród Poszukiwaczy jest zdrajca, udało się już znaleźć na to przesłanki i dowody. Ale kto to jest?
        * _sukces_: Poszukiwacze odkrywają tożsamość zdrajcy, neutralizują go i wprowadzają fałszywe dane Strażnikom
        * _dark future_: Poszukiwacze usuwają NIEWŁAŚCIWEGO "zdrajcę", ufając prawdziwemu zdrajcy. Strażnicy kontrolują i znają każdy ruch Poszukiwaczy.
        * _pokusa_: przekaż Strażnikom fałszywe dane, obróć zdrajcę na swoją stronę (podwójny agent), pozyskaj zasoby Strażników
        * _porażka_: przekaż Strażnikom prawdziwe dane, zniechęć sojuszników, zwiększ atmosferę paranoi
    * Wojna domowa - między Sojusznikami jest ogromny konflikt agend. Poszukiwacze muszą wybrać - albo zdeeskalować problem.
        * _sukces_: Poszukiwacze umacniają swoje relacje z sojusznikami i utrzymują integralność grupy. Sojusznicy są zdeterminowani i trudni do zastraszenia.
        * _dark future_: Poszukiwacze zostają sami i patrzą, jak sojusznicy się skutecznie zwalczają nie dbając o Sekret. Część sojuszników dołącza do Strażników.
        * _pokusa_: nowi sojusznicy, podżeganie sojuszników do większego zaangażowania w sprawę, ktoś powie coś bardzo przydatnego
        * _porażka_: straty wśród sojuszników, trudne decyzje Poszukiwaczy - kogo wybieramy, marnowanie czasu i ruchy Strażników
    * Śladami detektywa - ktoś kiedyś próbował odkryć Sekret. Nie udało się to (a może?), ale idąc tamtym tropem można dojść do wartościowych wyników.
        * _sukces_: Poszukiwacze mają dramatyczne przyspieszenie badania tropu, nieznane wcześniej informacje.
        * _dark future_: Tak jak detektyw, tak Poszukiwacze wpadają w tą samą pułapkę Strażników.
        * _pokusa_: sojusznicy detektywa, notatki detektywa, dowody ujawniające coś wartościowego
        * _porażka_: detektyw też błądził, Strażnicy znają następne ruchy Poszukiwaczy, uruchomienie wrogów detektywa, problemy detektywa stają się problemami Poszukiwaczy
    * Atak na bazę Strażników - Poszukiwacze dowiadują się gdzie jest baza Strażników i kim są i decydują się rozwiązać problem bezpośrednio.
        * _sukces_: Udany atak na bazę, dezorganizując Strażników i niszcząc ich zasoby. Poszukiwacze dostają cenne informacje znane tylko Strażnikom.
        * _dark future_: Atak kończy się niepowodzeniem. Strażnicy są silniejsi, zdeterminowani a Poszukiwacze mają straszne straty. Są złapani i usunięci.
        * _pokusa_: więcej informacji, więcej zasobów, część Strażników staje się sojusznikami, Strażnicy są złamani
        * _porażka_: straty wśród sojuszników, straty wśród Poszukiwaczy, postawienie Poszukiwaczy w złym świetle

### Pomoc dla MG

* **Pokusy - co Gracze mogą dostać więcej jeśli ryzykują**
    * Eskalacja działań ze strony Strażników, którzy przez to się odkrywają
    * Informacja o lokalizacji lub osobie, która może dać cenne informacje
    * Zdobycie nieoczekiwanej przewagi nad Strażnikami
    * Nowe tropy i ścieżki, które mogą prowadzić do czegoś powiązanego acz nie do końca na temat
* **Typowe eskalacje sukcesów**
    * Zdobycie zaufania i wsparcia Sojuszników
    * Zdobycie zasobów od Beneficjentów
    * Ślady wskazujące na kolejne ruchy Strażników
    * Zdobywanie autorytetu i zaufania
* **Typowe porażki**
    * Odkrycie mrocznych sekretów sojuszników. W tej sprawie wszyscy są w coś umoczeni...
    * Zdrada lub zaniechanie ze strony sojuszników (szantaż, apatia lub niewiara).
    * Działania Poszukiwaczy prowadzą do niezamierzonych negatywnych konsekwencji wobec osób trzecich
    * Im bardziej odkrywamy Sekret, tym więcej problemów i tym więcej niezadowolonych osób
    * Przyciągnięcie uwagę innych sił - niekoniecznie przyjaznych
    * Sekret staje się coraz bardziej skomplikowany i niebezpieczny
* **Typowe tory na sesji**
    * _Zastraszenie sojuszników_: im wyżej, tym bardziej Strażnicy kontrolują miejsce
        * Skala [współpracują z Poszukiwaczami - pełne zniewolenie przez Poszukiwaczy]



