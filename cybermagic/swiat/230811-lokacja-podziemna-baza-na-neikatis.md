# Notatka

## 1. Podziemna baza na Neikatis
### 1.1. Materiały

* https://www.humanmars.net/search/label/Underground%20colony
* https://www.humanmars.net/2019/11/mars-colony-for-1000-people-by-innspace.html

### 1.2. Rozkład

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Cognitio Nexus
                        1. Poziom zero
                            1. System defensywny
                            1. Stacja przeładunkowa
                            1. Starport
                        1. Poziom (-1) Mieszkalny
                            1. Szyb centralny
                            1. NW
                                1. Magazyny wewnętrzne
                                1. System obronny
                                1. Sprzęt ciężki, kopalniany
                            1. NE
                                1. Medical
                                1. Rebreathing
                                1. Administracja
                            1. S
                                1. Sektor mieszkalny
                                    1. Przedszkole
                                    1. Stołówki
                                1. Centrum Rozrywki
                                1. Park
                        1. Poziom (-2) Industrialny
                            1. NW
                                1. Inżynieria
                                1. Fabrykacja
                                1. Reprocesor Śmieci
                                1. Magazyny
                                1. Reaktor
                            1. NE
                                1. Centrum Dowodzenia
                                1. Rdzeń AI
                                1. Podtrzymywanie życia
                            1. S    
                                1. Szklarnie podziemne
                                1. Stacje badawcze
                        1. Poziom (-3) Badań Niebezpiecznych
                            1. Śluza defensywna
                            1. Biolab
                            1. Magitech
                            1. Anomaliczne
