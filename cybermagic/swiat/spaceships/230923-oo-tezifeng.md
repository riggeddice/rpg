## Statek

* Nazwa: OO Tezifeng
* Załoga: 20
* Typ: Korweta patrolowa

.

* Przednia Część:
    * Mostek Sterowniczy: Miejsce komendy statku z konsolami nawigacyjnymi, systemami obronnymi i komunikacyjnymi.
    * Centrum Komunikacyjne: Z antenami i urządzeniami do komunikacji dalekiego zasięgu.
    * Strefa Relaksu: Miejsce wypoczynku z systemem rozrywki.
    * Kuchnia i Jadalnia: Przystosowana do gotowania i spożywania posiłków w warunkach mikrograwitacji.
* Środkowa Część:
    * Moduły Mieszkalne:
    * Indywidualne kajuty dla załogi.
    * Kabina kapitana.
    * Magazyny z żywnością: Z zaopatrzeniem, które jest hermetycznie zamknięte i łatwe do przechowywania.
    * Służba Medyczna: Wyposażona w sprzęt do leczenia urazów związanych z kosmicznymi warunkami.
    * Sala Gimnastyczna: Przystosowana do ćwiczeń w mikrograwitacji.
    * Dokowanie dla Szybkich Łodzi Kosmicznych: Mniejsze pojazdy używane do misji zbliżeniowych na planetach lub asteroidach.
    * Magazyn Uzbrojenia: Przechowywanie broni przystosowanej do kosmicznych warunków.
    * Cele Więzienne/Areszt: Jeśli korweta ma obowiązek zatrzymywania kosmicznych przestępców.
    * Łazienki: Przystosowane do warunków mikrograwitacji.
    * Śluza główna
* Tylna Część:
    * Segment Napędowy: Główne silniki statku oraz systemy wsparcia życia.
    * Sala Techniczna: Dla napraw i konserwacji sprzętu w warunkach kosmicznych.
    * Pomieszczenie Awaryjne: Z kapsułami ratunkowymi i innym sprzętem awaryjnym.



## Załoga

?
