
Endurance > Speed > Utility > Armour > Guns

duża korweta transportowa, 30 osób

* Przednia Część:
    * Mostek
    * Centrum Komunikacyjne
    * Strefa Relaksu
    * Kuchnia i Jadalnia
    * Działko laserowe (z górnej strony)
    * AI Core
    * Magazyny
* Środkowa Część:
    * Jawna część
        * Moduły Mieszkalne
            * Kajuty
            * Łazienka soniczna
            * Sala Gimnastyczna
        * Kabina kapitana.
        * Żywność
        * Medical
        * Śluza główna
        * Life Support
    * Tajna część
        * Miejsca przemytu i transportu ludzi
        * Gaz
        * Ładunki wybuchowe
* Tylna Część:
    * Magazyny
    * Zapasy wody, paliwa...
    * Maszynownia
    * Główne silniki statku
    * Inżynieria
    * Doki Serwisowe

Co ma:

* 1 Prom Ratunkowy (napęd chemiczny, szybki, małe delta-v)
* 1 'chaff'; kosmiczne śmieci z poukrywanymi 3 minami-rakietami
* 1 'pop-up mine'