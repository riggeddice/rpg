## Statek

* Nazwa: ON Arcadalian
* Załoga: 140
* Typ: Supply Ship

.

1. Część przednia
    1. Mostek, Dowodzenie
        * Systemy nawigacyjne, sensory i komunikacyjne.
        * Command&Control – centralne miejsce kontroli zaawansowanych technologii statku.
    2. AI Core
        * Miejsce, w którym mieści się główna sztuczna inteligencja statku, zarządzająca i monitorująca wszystkie systemy.
    3. Pokład Życiowy
        * System Podtrzymywania Życia – utrzymujący odpowiednie warunki do życia na pokładzie.
        * Kwatery Załogi – miejsca do spania, relaksu i rekreacji dla załogi.
2. Część środkowa
    1. Sekcja Tankowania
        * Doki Tankujące – dla przekazywania paliwa innym statkom.
        * Magazyny Paliw – zawierające różne rodzaje paliwa potrzebne do wsparcia innych jednostek.
    2. Pokład Zaopatrzenia
        * Magazyny Zaopatrzenia – przechowujące zapasy, części zamienne i inne niezbędne materiały.
        * Dropping Bay – sekcja do przesyłania zaopatrzenia na inne statki lub na powierzchnię planet.
    3. Komora Badań
        * Laboratoria Badań – do przeprowadzania eksperymentów i badań naukowych.
        * Obserwatorium – do obserwacji i badania kosmosu.
        * Medical Bay – z zaawansowanymi możliwościami leczenia.
2. Część tylna
    1. Pokład Inżynieryjny
        * Advanced Engineering Suite – miejsce, w którym przeprowadzane są zaawansowane prace inżynieryjne i naprawy.
        * Systemy Kontroli Energetycznej – miejsce kontrolowania i dystrybucji energii do wszystkich systemów statku.
    2. Silniki i reaktory
    3. Dźwig Ładunkowy
        * Służy do przekazywania ładunków pomiędzy pokładami oraz innymi jednostkami.
    4. Doki Serwisowe
        * Dla przeprowadzania zewnętrznych napraw i utrzymania.
        * Support Drones – drony przeznaczone do wsparcia i napraw na zewnątrz statku.




## Załoga

Oczywiście, oto zmodyfikowane propozycje dla załogi statku "Aeternum":

1. **Kapitan**
    * Livia Sertiano
    * Była naukowcem, która postanowiła pójść za wiedzą i zrezygnowała z akademickiej kariery, by dowodzić statkiem wsparcia.
    * Mimo że rzadko uczestniczy w bezpośrednich starciach, posiada dogłębną wiedzę taktyczną.
2. **Pierwszy Oficer / Zastępca Kapitana**
    * Marcus Valperius
    * Były inżynier z głęboką fascynacją kosmosem; najlepiej wie co i jak
    * Jego pragmatyczne podejście i analityczny umysł są nieocenione w sytuacjach kryzysowych.
3. **Inżynier Główny**: 
    * Octavia Ingelico
    * Jej zaawansowana wiedza inżynieryjna jest kluczowa dla funkcjonowania wszystkich systemów statku.
    * Często spędza długie godziny przy pracy, by zapewnić, że statek jest zawsze gotowy do działania.
4. **Oficer Naukowy / Technologiczny / Komunikacji**: 
    * Drusus Arilesis
    * Jego wszechstronna wiedza pozwala mu na prowadzenie badań i analiz w różnych dziedzinach nauki.
    * Nieustannie poszukuje nowych możliwości technologicznych, które mogą poprawić funkcjonowanie statku.
5. **Oficer Bezpieczeństwa**:  
    * Valeria Malartin
    * Odpowiada za zapewnienie bezpieczeństwa załogi i utrzymanie porządku na statku.
    * Jej doświadczenie i intuicja pozwalają jej na szybkie identyfikowanie potencjalnych zagrożeń.

