## Statek

* Nazwa: OO Castigator
* Załoga: 420
* Typ: Krążownik Anihilacyjny

.

1. **Część przednia (Głowica)**
    1. **Mostek, Dowodzenie**
        * Systemy nawigacyjne, sensory i komunikacyjne.
        * Command&Control – centralne miejsce kontroli zaawansowanych technologii statku.
    2. **AI Core**
        * Miejsce, w którym mieści się główna sztuczna inteligencja statku.
    3. **Pokład Życiowy 1**
        * System Podtrzymywania Życia.
        * Kwatery Załogi.
        * Rozrywka, mesa...
    4. **Czujniki i Systemy Obserwacyjne**
    5. **System komunikacji**
    6. **Systemy defensywne przednie**
    7. **Ekrany Ochronne**
2. **Pokład przedni dolny** 
    1. **Głowica działa strumieniowego**
    2. **Skrzydła - radiatory**
3. **Część środkowa (Korpus)**
    1. **Pokład Inżynieryjny**
    2. **Medical Treatment**
    3. **Pokład Życiowy 2**
        * System Podtrzymywania Życia.
        * Kwatery Załogi.
        * Rozrywka, mesa...
    4. **Ekrany Ochronne**
    5. **Hangar ŁZ**
        * Fabrykatory ŁZ
    6. **Hangar statków i pinas**
4. **Część środkowa (dolna)**
    1. **Pokład Generatora**
    2. **Pokład Chłodzenia**
    3. **Skrzydła - radiatory**
    4. **Pokład Zaopatrzenia Działa Strumieniowego**
        1. Akcelerator działa strumieniowego
    5. **Amunicja i wkłady Działa Strumieniowego**
    6. **Pokład Inżynieryjny**
    7. **Ekrany Ochronne**
    8. **Hangary wymiany elementów Castigatora**
4. **Część tylna (Sekcja napędowa)**
    1. **Silniki i reaktory**
    2. **Doki Serwisowe**
    3. **Systemy defensywne**
    4. **Pokład Zaopatrzenia**


## Załoga

?
