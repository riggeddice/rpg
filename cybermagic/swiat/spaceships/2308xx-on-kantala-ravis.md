

* Rozkład jednostki Kantala Ravis (ok. 200 osób)
    * Statek podłużny, w formie cylindra
    * Transport w formie 'pociągów'
    * Przednia część
        * silniki
        * silniki manewrowne
        * harvesters, drony
        * magazyny
        * fabrykatory
        * detektory
        * magazyny wody
        * broń
        * siłownia / rozrywka
    * Środkowa część
        * AI Core
        * Life support
        * Kwatery mieszkalne
        * mesa, żywność
        * Medical
    * Tylna część
        * silniki manewrowne
        * silniki
        * reaktor główny
        * inżynieria
        * kociarnia pacyfikatorów
        * śluzy, ładownia, magazyny