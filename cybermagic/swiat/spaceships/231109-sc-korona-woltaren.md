
Endurance > Speed > Utility > Armour > Guns

korweta dalekosiężna, 20 osób (oczekujesz: 40)

* Przednia Część:
    * Mostek: Miejsce komendy statku z konsolami nawigacyjnymi, systemami obronnymi i komunikacyjnymi.
    * Centrum Komunikacyjne: Z antenami i urządzeniami do komunikacji dalekiego zasięgu.
    * Strefa Relaksu: Miejsce wypoczynku z systemem rozrywki.
    * Kuchnia i Jadalnia: Przystosowana do gotowania i spożywania posiłków w warunkach mikrograwitacji.
    * Działko laserowe
    * AI Core: Semla. Kiepska.
    * Magazyny
* Środkowa Część:
    * Moduły Mieszkalne
    * Indywidualne kajuty dla załogi.
    * Łazienki
    * Kabina kapitana.
    * Żywność
    * Medical
    * Sala Gimnastyczna
    * Magazyn Uzbrojenia
    * Śluza główna
    * Orchard / "Sad": hydroponika; unikalna cecha
    * Life Support
* Tylna Część:
    * Magazyny
    * Zapasy wody, paliwa...
    * Maszynownia
    * Segment Napędowy: Główne silniki statku oraz systemy wsparcia życia.
    * Sala Techniczna: Dla napraw i konserwacji sprzętu w warunkach kosmicznych.
    * Pomieszczenie Awaryjne: Z kapsułami ratunkowymi i innym sprzętem awaryjnym.
    * Doki Serwisowe
