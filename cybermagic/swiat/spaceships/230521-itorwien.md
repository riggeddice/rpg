
**Statek "Itorwien"**

1. **Pokład Główny/Komora Odzysku**: Jest to obszar, w którym zbierane są kosmiczne odpady. Za pomocą systemów magnetycznych oraz grawitacyjnych odpady są zgarniane i przekazywane do dalszej obróbki.
2. **Sekcja Maszyn**: Tutaj znajdują się główne silniki i systemy napędowe statku, a także systemy napędzające urządzenia do zbierania odpadów.
3. **Komora Przetwarzania**: Miejsce, w którym zgromadzone odpady są segregowane, kompresowane i przetwarzane na materiały wtórne lub energię.
4. **Mostek**: Tutaj kapitan i sternicy prowadzą statek i nadzorują operacje. Znajdują się tu systemy nawigacyjne, komunikacyjne oraz kontroli systemów statku.
5. **Kwatery Załogi**: Miejsca do spania i relaksu dla załogi. Mogą istnieć osobne kabiny dla różnych członków załogi lub wspólne pomieszczenie.
6. **Kantyna**: Miejsce, gdzie załoga przygotowuje i spożywa posiłki. Jest tu także miejsce na odpoczynek i rekreację.
7. **Magazyn**: Miejsce, gdzie przechowywane są zapasowe części, narzędzia, dodatkowe systemy zbierania odpadów oraz inne niezbędne materiały.
8. **Dźwig Ładunkowy**: Używany do przenoszenia skompresowanych odpadów lub przetworzonych materiałów do innych statków lub stacji.
9. **Doki Serwisowe**: Miejsce, gdzie mogą być przeprowadzane naprawy zewnętrzne, utrzymanie systemów zbierających odpady i inne prace serwisowe na zewnątrz statku.
10. **Life Support**
11. **AI Core**