## Statek

* Nazwa: ON Anitikaplan
* Załoga: 376
* Typ: Krążownik Generacyjny (family unit) + Drone Carrier + AI Generator
* Size:
    * 400k ton
    * 500 m długości
    * 10 pokładów

Struktura jednostki (to NIE jest jednostka bojowa!):

* Część Przednia (Głowica)
    * Mostek, Dowodzenie
        * Systemy nawigacyjne, sensory i komunikacyjne.
        * Command&Control – centralne miejsce kontroli zaawansowanych technologii statku.
    * AI Core
        * Miejsce, gdzie mieści się główna sztuczna inteligencja statku, zarządzająca wszystkimi systemami.
    * Pokład Życiowy 1
        * System Podtrzymywania Życia.
        * Kwatery Załogi.
        * Rozrywka, mesa, udogodnienia życiowe.
        * Panorama.
    * Czujniki i Systemy Obserwacyjne
        * Zaawansowane systemy do monitorowania otoczenia statku.
    * System Komunikacji
        * Możliwości dalekiego zasięgu i precyzyjnego śledzenia.
    * Systemy Defensywne Przednie
        * Ekrany Ochronne i inne systemy obronne.
* Pokład Przedni Dolny
    * Radiatory
        * Skrzydła - radiatory dla zarządzania ciepłem.
* Część Środkowa (Korpus)
    * Pokład Inżynieryjny
        * Zaawansowane warsztaty i pomieszczenia do napraw.
    * Medical
        * Medical Bay z zaawansowanymi technologiami medycznymi.
    * Pokład Życiowy 2
        * Dodatkowy system podtrzymywania życia i kwatery załogi.
        * Rozrywka, mesa i inne udogodnienia.
    * Hangar Dron
        * Fabrykatory Dron i miejsce do przechowywania mniejszych pojazdów.
    * Hangar Statków i Pinas
        * Przestrzeń dla dodatkowych statków i pinas.
* Część Środkowa (Dolna)
    * Pokład Generatora
        * Główne źródło zasilania statku.
    * Pokład Chłodzenia
        * Systemy zarządzania temperaturą i radiatory.
    * Pokład Zaopatrzenia
        * Zaopatrzenie inżynieryjne
    * Krypta Przodków
    * Systemy Stazy
* Część Tylna (Sekcja Napędowa)
    * Silniki i Reaktory
        * Potężne silniki dla długich podróży międzygwiezdnych.
        * Reaktory jądrowe i alternatywne źródła energii.
    * Doki Serwisowe
        * Przestrzeń dla przeprowadzania zewnętrznych napraw i utrzymania.
        * Support Drones – drony przeznaczone do wsparcia i napraw na zewnątrz statku.
    * Systemy Defensywne
        * Tylne ekrany ochronne i inne systemy obronne.
    * Pokład Zaopatrzenia
        * Magazyny i systemy logistyczne dla zaopatrzenia statku.
