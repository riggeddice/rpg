Endurance > Speed > Utility > Armour > Guns

Superdalekosiężna ekonomiczna fregata transportu hibernacyjnego, 15 osób aktywnych, 360 zahibernowanych

1. Część przednia
    1. Mostek, Dowodzenie
        * Systemy nawigacyjne, sensory i komunikacyjne.
        * Command&Control – centralne miejsce kontroli zaawansowanych technologii statku.
    2. AI Core
        * Miejsce, w którym mieści się główna sztuczna inteligencja statku, zarządzająca i monitorująca wszystkie systemy.
    3. Pokład Życiowy
        * System Podtrzymywania Życia – utrzymujący odpowiednie warunki do życia na pokładzie.
        * Kwatery Załogi – miejsca do spania, relaksu i rekreacji dla załogi.
        * Medical - leczenie itp.
    4. Reaktor (zapasowy)
2. Część środkowa
    1. Pokład Zaopatrzenia
        * Magazyny Zaopatrzenia – przechowujące zapasy, części zamienne i inne niezbędne materiały.
        * Dropping Bay – sekcja do przesyłania zaopatrzenia na inne statki lub na powierzchnię planet.
    2. Pokład Hibernacyjny 1
        * 3 Węzły po 60 osób
    3. Pokład Hibernacyjny 2
        * 3 Węzły po 60 osób
    4. Pokład Ratunkowy
        * Medical (awaryjny)
        * System Podtrzymywania Życia, zapasowy
3. Część tylna
    1. Pokład Inżynieryjny
        * Advanced Engineering Suite – miejsce, w którym przeprowadzane są zaawansowane prace inżynieryjne i naprawy.
        * Systemy Kontroli Energetycznej – miejsce kontrolowania i dystrybucji energii do wszystkich systemów statku.
    2. Silniki
    3. Reaktory
    4. Doki Serwisowe
        * Dla przeprowadzania zewnętrznych napraw i utrzymania.
        * Support Drones – drony przeznaczone do wsparcia i napraw na zewnątrz statku.
        * Hangar Pintki
