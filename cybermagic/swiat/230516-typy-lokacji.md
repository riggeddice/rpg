# Rody Aurum

## 1. Typy lokacji

Typy lokacji:

* 

Aspekty lokalizacji:

* teren
    * zagrożenia / problemy wynikające z przestrzeni terenowej
* 
* 

## 2. Lokacje

### 2.1. KODOWANIE

C (ciało), U (umysł / intelekt / psychotronika), E (emocje / morale / kontrola magiczna), S (społeczne)

### 2.2. Typowe Wzgórza Verlenlandu

* populacja: "dzikie zwierzęta", "potwory Verlenlandu", "samotni traperzy"
* teren: "stromy i zdradliwy", "krótkie i zdradliwe zbocza", "surowy klimat", "dzika pierwotna puszcza"
* hazard: "kamienne lawiny", "potencjalnie niestabilny teren - jaskinie i groty", "gejzery", "Niemożliwe do zauważenia przepaście"
* mutatory: "mnóstwo miejsc gdzie można się schować", "bardzo niebezpiecznie zejść ze szlaku"

### 2.3. Pole Kwiatowe Samszarów

* populacja: "duchy opiekuńcze", "osoby szukające ukojenia"
* teren: "nieskończony kwiatowy raj", "menhir centrujący energię magiczną"
* hazard: "niewidoczny teren pod nogami", "pomniejsze magiczne efemerydy"
* mutatory: "ogromna ilość otwartego terenu", "miejsce magicznych rytuałów"

Opis lokalizacji:

Ekologiczny przystanek dla wielu istot. Setki gatunków motyli, pszczoły i innych owadów spłatają kwiaty w poszukiwaniu nektaru, zapewniając niezbędne zapylanie. W tym samym czasie, duchy opiekuńcze, które są widoczne tylko dla najbardziej wrażliwych, troszczą się o równowagę pomiędzy naturalnym światem a technologicznym, niewidzialną dla większości.

Kluczowy punkt dla społeczności. Kwiaty, które rosną tu obficie, są zbierane do produkcji naturalnych leków, perfum, a nawet do wykorzystania w kulinarii. Dodatkowo, pszczoły i inne owady zapylające stanowią źródło miodu i wosku. Menhir sam w sobie jest również atrakcją turystyczną, przyciągającą zarówno turystów, jak i naukowców zainteresowanych badaniem związku między magią a technologią.

Miejsce ukojenia, regeneracji, wypoczynku i piękna. Wiele rzadkich roślin, często wykorzystywanych do różnych magicznych rytuałów.

Miejsce bardzo bezpieczne.

## 2.4. Niewielki hotel rodzinny Samszarów

* populacja: "rodzina właścicieli", "goście z różnych obszarów"
* teren: "malownicza okolica", "starannie utrzymane ogrody", "niewielka ilość pokoi"
* hazard: "wścibskie lokalne duchy", "plotki i ploteczki"
* mutatory: "kursy i warsztaty robione przez właścicieli", "właściciele dbają o gości jak o rodzinę", "sezonowe wydarzenia"


