## 1. Lud: Drakolici
### 1.1. Notka

* Jak o nich mówimy: drakolita, drakolitka, drakolici
* Klasa: saityci, indywidualiści w grupach celowych, uniwersaliści, heterogeniczni w ideach, heterogeniczni w formach, "pakty z demonami"

### 1.2. Fiszka

1. Metapsychology (tenets and values of psyche, art, culture)
    1. Universalism, Achievement -> Znaleźć najlepszą możliwą bioformę, zaadaptować się, szukać nowych rzeczy i odkrywać to, co jeszcze nieodkryte. Każdy ma prawo do przyszłości.
    2. Wieczna ewolucja ludzkiej formy -> jesteśmy tylko w danym punkcie naturalnej ewolucji. Czas, byśmy wzięli formy które pasują do sytuacji i lepiej nas odzwierciedlają.
    3. Precyzyjna komunikacja i skupienie na zasadach -> przy tak różnych bioformach i umysłach i kulturach tylko czysta i precyzyjna komunikacja może nam pomóc
    4. Indywidualiści szukający różnorodności -> każdy ma prawo do innego znalezienia idealnego miejsca i idealnej formy. 
    5. Wyższość homo superior -> inni są ograniczeni swoim ciałem i małym spojrzeniem. My nie boimy się zmian tego co najważniejsze - naszej bioformy, naszego potencjału.
2. Sześć fundamentów wartości (w skali [0, 5]; suma 15)
    1. Care:        (1): niech każdy osiągnie najwyższy poziom swojego potencjału, unikanie zadawania cierpienia. Niestabilność oznacza, że każda bioforma spotka kiedyś swój kres.
    2. Fairness:    (3): jeśli komuś pomogę, oczekuję tego samego. Jeśli zabiorę Ci kota, oczekuję podobnej odpowiedzi. Grupy są tylko wygodnymi konstruktami, sprawy są między jednostkami.
    3. Loyalty:     (1): jedyna reguła: tit-for-tat; jak zachowujesz się wobec drakolity tak odpowie. BETRAYAL to zaskoczenie regułami. Ogólnie: nie ma "nas", jestem tylko "ja".
    4. Authority:   (4): zasady i reguły są BARDZO ważne, nawet, jeśli nie są w pełni poprawne. Trzymają się najgłupszych reguł, by nie było łowów na czarownice. Acz robią to inteligentnie.
    5. Sanctity:    (3): ludzka forma musi być zaadaptowana do najlepszej jej formy. Następny krok w ewolucji ludzkości. Pod kątem tego co robisz oraz glorii istot żywych.
    6. Liberty:     (3): każdy ma prawo dążyć do doskonałości - swego ciała, swych osiągnięć - i przekazać geny dalej. Silna indywidualizacja w ramach istniejących praw. Prawo do odejścia.
3. Osie
    1. Archy - Kratos:                  ambivalent archy, anti kratos       (nie wolno wymuszać siłą zachowań innych, ale czemu stać na drodze tego kto jak patrzy na hierarchię?)
    2. Personal - Economic Freedoms:    high personal, low economic         (everything goes, dowolna adaptacja, ALE nie ma możliwości zrobić kontraktów czy blokad anty-odejściowych)
    3. high/low trust:                  low trust                           (inni nie postrzegają nas jak swoich a i część naszych może mieć niesprawne bioformy)
4. What do they value the most
    1. perfekcyjna adaptacja do otoczenia i doskonała mimikra - nie jesteś uznany za "obcego", mimo, że jesteś
    2. bardzo precyzyjna i dobra komunikacja, jasne i czytelne zasady mające podparcie w wartości dla grupy
5. Ingroup - Outgroup
    1. IN: ja i niewielka grupka których nazywam przyjaciółmi
    2. OUT: wszyscy inni
    3. ENEMY: puryści ludzkiej formy i antynaukowcy
6. Organization
    1. structure: niewielkie grupki z autokratą. Wysoka autonomia i niezależność, ale jak długo trzymasz się reguł. W czym masz prawo opuścić miejsca gdzie reguły Ci nie pasują.
    2. allocation: zgodnie z możliwościami i zainteresowaniami. Możliwość nadpisania przez monarchę, acz to wiąże się z ryzykiem zmiany monarchy. Organizacje kapitalistyczne.
    3. archy / rank: hierarchia zgodna z daną strukturą, na szczycie monarcha absolutny
    4. political power: zwykle drakolici mają systemy typu monarchia absolutna + wybór następnego monarchy zgodnie z zasadami grupy
    5. rozwiązywanie konfliktów: dyskusja > pakt z demonem > właściwa instancja sądownicza
    6. spirituality: perfekcyjna bioforma, następny krok ewolucji ludzkości. Pozostanie w tej samej bioformie do końca świata jest marnowaniem ludzkiego potencjału.
7. Symbolism and Aesthetics
    1. BARDZO różnorodna. Z uwagi na silny indywidualizm i różne zmysły różnych bioform sztuka drakolitów jest całkowicie niezrozumiała dla wielu nawet w ich obszarze
    2. Dominującym tematem jest ewolucja i wieczna zmiana. Ale to bardzo zależy od tego jak wygląda i działa dany drakolita i co go interesuje.
    3. Dominującym kierunkiem jest sztuka w formie samego ciała. Piękno ciała, piękno zmodyfikowanego ciała. Walki gladiatorów, pokaz modeli.
    4. Drakolici czerpią ze wszystkich istniejących kultur i bardzo je remiksują.
8. Taboo
    1. Wzbudzanie strachu u innych ludzi tak, by wpłynęło to na jednostkę lub grupę. Są zbyt zmienieni by móc pozwolić sobie na panikę w społeczeństwach niedrakolickich.
    2. Złamanie kontraktu celowego typu "Pakt z demonem". Ogólnie, łamanie prawa i zasad (znanych wcześniej) niekoniecznie jest tabu, ale podpada niebezpiecznie blisko tego miejsca.
    3. Utrata kontroli nad sobą i stanie się prawdziwym potworem.
    4. Niewolnictwo.

### 1.3. Wspólna historia i doświadczenia, wspólne problemy i traumy -> adaptacje

* Zawsze istnieli ludzie, którzy chcieli móc pokazać swoją odmienną naturę i się wyróżnić - czy przez tatuaże, czy przez inne modyfikacje biologiczne. Też istnieli ludzie, którzy wierzyli, że ludzka forma jest jedynie wynikiem ewolucji i trzeba zrobić kolejny krok - wziąć ewolucję w swoje ręce.
* Po Pęknięciu Rzeczywistości okazało się, że pojawiły się nowe, lepsze możliwości zmiany ludzkiej formy. I różne warianty ludzkiej formy są efektywne w różnych miejscach. Tak powstali drakolici - heteromorfy wierzące, że do każdej rzeczywistości i sytuacji da się zaadaptować inną bioformą.
* Z definicji silni indywidualiści, ale nagle zorientowali się, że w nowym świecie są traktowani jako 'potencjalne potwory' a nie jako inni ludzie. To zmusiło ich do budowania grup i drakońskich zasad.
    * --> indywidualistyczne podejście wymusza dużą współpracę grupową; same zmiany bioform wymagają dużo technologii i magitechu
    * --> self-governing, silne skupienie pod kątem autokracji i "zasady nie wzbudzania strachu u innych ludzi"
* W pewnym momencie drakolici zaczęli pojawiać się też w innych społeczeństwach. To sprawiło, że zostali _tymi obcymi_. Jeśli ktoś zawinił? Drakolita. Ktoś zrobił coś złego? Drakolita. Jednocześnie, jako _homo superior_ okazali się po prostu LEPSI od innych. Dodajmy zasadę Paradygmatu - przy polu magicznym i niezwykłej nieufności wobec drakolitów, część drakolickich bioform została złamana przez Paradygmat i faktycznie stali się potworami.
    * --> pojawienie się zasady 'trzymaj się reguł miejsca w którym jesteś'
    * --> ogromna nieufność drakolitów wobec innych drakolitów i nie-drakolitów
    * --> podstawową zasadą społeczną są kontrakty celowe, zwane przez niektórych nie-drakolitów "pakt z demonem".
    * --> zmodyfikowana bioforma + cyborgizacja + neurosprzężenie nie zawsze działa bardzo dobrze. Lepiej skupić się na czystej bioformie, acz nie odrzucimy kogoś zintegrowanego.
* Widząc problemy związane z TAI, drakolici pierwsi zauważyli, że różnica między TAI wysokiego stopnia i ludźmi jest możliwa do przekroczenia. Widząc kralothy, drakolici uznali, że może istnieć też nie-ludzki umysł który jest osobnym bytem i cywilizacją. To wprowadziło ich w jeszcze większą izolację od humanocentrycznej natury sektora Astoriańskiego.
    * --> tendencje do nie odrzucania konceptów takich jak Saitaer, sojusz z kralothami czy wolne TAI. Każdy ma prawo do lepszej przyszłości a oni zaadaptują się do zwycięzcy.
    * --> sojusze i grupy nie tylko składające się z samych drakolitów
    * --> tym mocniejszy wpływ zasad i tym mniejszy 'reliance' na rzeczy miękkie takie jak 'lojalność'
* Im trudniejsza sytuacja, tym szybsza ewolucja i tym silniejsza adaptacja. To sprawia, że drakolici w pełni akceptują trudne sytuacje z fatalizmem. Dogmat ewolucyjny sprawia, że podejście i próba ewolucji jest na linii _religijnej_ a nie tylko _z perspektywy efektywności_
    * --> akceptacja strat jednostek. Tylko najsilniejsi - najlepiej zaadaptowani - przetrwają. Sprawia to, że wyglądają na bardzo zimnych i nieczułych.
    * --> jak długo żyjesz, możesz wyprowadzić sytuację na lepsze.
    * --> Twoje ciało jest Twoją bronią. Używaj wszystkiego co Twoja bioforma zapewnia.
* Drakolici są przede wszystkim tam, gdzie inne plemiona sobie nie radzą. Daleko w asteroidach, gdzie jest mało energii i musisz długo hibernować. W zbyt zimnych lub zbyt gorących miejscach. Tam, gdzie zmiana bioformy jest najlepszym rozwiązaniem.
    * --> możesz żyć wszędzie. Nie jesteś tym, co oni. Nie musisz z nimi konkurować. Jesteś najdoskonalszym pionierem.

### 1.4. Główne stereotypy i powiedzonka

* "TAI, kraloth, Saitaer, człowiek. Niech zwycięży najlepszy a my zaadaptujemy się do tego co jest ostateczną bioformą."
* "Gdyby człowiek nie musiał, z drzewa by nie schodził. Problemy i presja zmuszają nas do ewolucji."
* "Nieważne w jakim stanie żyjesz - jeśli żyjesz, pojawi się okazja."
* Trzech bandytów napadło drakolitę. Drakolita zabił wszystkich, nie było świadków, zdążyłby uciec - ale poszedł oddać się w ręce prawa i został zastrzelony. Wiedział, co go czeka, a poszedł.

### 1.5. Pytania dodatkowe

1. What is one major stereotype about your culture that you want to dispel right now?

Nie jesteśmy "zdradzieckimi demonami które da się kontrolować jedynie używając umiejętnie zawartych paktów". Po pierwsze, nigdy nie obiecywałem nikomu że mu pomogę lub że jestem po jego stronie - jak mogę go zatem zdradzić? To nie moja sprawa, co sobie myślał; po prostu podróżowaliśmy po tej samej ścieżce bo było nam wygodnie. Po drugie, "PAKT Z DEMONEM"?! To są normalne kontrakty zgodne z systemem prawnym. Jak często zdarza nam się łamać zasady Twojego miejsca czy ingerować w Twój świat? Nie oczekujemy niczego innego - nie mów mi jak mam żyć w moim świecie jeśli nie ma na to jasnej reguły. Ile razy zrobiłem komuś krzywdę, a ile razy pomogłem - mimo, że nie musiałem?

2. What are the things that you consider acceptable that may not be accepted in other cultures?

* dramatyczna rekonstrukcja ludzkiej formy, nie zawsze bezpieczna - ale zawsze prawidłowa z perspektywy celu i otoczenia.
* niska lojalność i pozorne "tylko trzyma się zasad". Zasady są ważne, to one determinują co i jak. Nie możesz nie mieć zasad przy tak heterogenicznym świecie.
* skrajny indywidualizm
* homo superior - fizyczna odmienność i "lepszość"
* akceptacja wszystkich form życia i egzystencji, chęć współpracy z nimi i brania z nich optymalnych cech biologicznych

3. What is one characteristic that describes your culture as a whole?

Modyfikujący się genetycznie 'prawnicy' bardziej zwracający uwagę na zasady niż kontakty międzyludzkie, uważający ewolucję bioformy ludzkiej za imperatyw moralny.

5. Who do you consider to be your family? How FAMILY looks like (one home, head of family, responsibilities)

Nie ma grup, są tylko jednostki. Acz jednostki mogą tworzyć grupy; większość drakolitów jednak ma zasady powiązane z krwią (rodzice, dzieci, kuzynostwo...)

6. What links your culture / group together

Zagrożenie zewnętrzne - drakolici są unikalni, więc większość osób mimo wszystko nie chce być blisko nienaturalnych bioform. Wiara w konieczność ewolucji ludzkiej formy i 'wspólne dzielenie się nauką' - najlepsze formy powstają przez kooperację. Fakt, że wszyscy jesteśmy _homo superior_ a inni nie i jeszcze tam nie doszli.

7. When do you consider a person to be an adult?
8. What are the traits that are considered to be positive?
9. Who do you think are the heroes of society?
10. What are the cultural values that children are exposed to immediately?
11. What kinds of work are considered the be prestigious?
12. What are the normal food products that are eaten?
13. Are there some colors / symbols that are being used for certain events?
14. Are there some beliefs that are associated with XXX
15. How will you describe a typical day in your life?

