## 1. Sybrianie
### 1.1. Notka

* Jak o nich mówimy: sybrianin, sybrianka, sybrianie
* Klasa: gaianie, indywidualiści, biopuryści

### 1.2. Omówienie ogólnie

Okrutny teren. Duże stężenie anomalii. Półnagie siłowanie się z niedźwiedziem w okolicach lodowych krain. Niezależność, twarda dusza pioniera i - przede wszystkim - mnóstwo śmiechu i niezgody nie niegodziwy świat. Sybrianie - pionierzy, szaleńcy, idący o jeden krok za daleko.

Pionierzy, którzy dla wolności uciekli najdalej jak tylko się dało od cywilizacji. Nie mieli wiele, ale mieli siebie i byli szczęśliwi. Przed pęknięciem rzeczywistości byli gdzieś na uboczu cywilizacji - brali trudne rzeczy, których nikt nie chciał. Akceptowali ryzyko i potencjalnie niską dochodowość, bo życie nie na tym polega by zarabiać a by wygrywać walki godne zwyciężania. Gdy rzeczywistość Pękła, byli najlepiej przystosowani do nowej rzeczywistości. Ot, kolejne ryzyko, kolejny problem do pokonania. WRESZCIE godny przeciwnik.

### 1.3. Gdzie ich spotkamy

Astoria -> Aurum: Verlenland, Astoria -> poza Zjedoczeniem, na obrzeżach. Neikatis -> niektóre arkologie.

### 1.4. Fiszka

1. Metapsychology (tenets and values of psyche, art, culture)
    1. Stimulation, Achievement -> dobrze się bawić, wysoka adrenalina, być ciągle coraz lepszym
    2. Nic nie jest niemożliwe -> poradzimy sobie w każdych okolicznościach, po prostu czasem trzeba kilka podejść
    3. Śmiech -> gdy Ci źle, śmiej się. Gdy nie dajesz rady, śmiej się. Gdy wygrywasz, śmiej się. Nie bądź cichy! Radość i walka są sprzężone.
    4. Wyzwania i ryzyko -> nie zostawaj w jednym miejscu, idź dalej. Owszem, możesz zapłacić, możesz dużo stracić. Ale kto nie ryzykuje, nie wygrywa i naprawdę _nie żyje_.
2. Symbolism and Aesthetics
    1. Sława bohaterom -> kult tych, którzy wzięli duże ryzyko i im się udało... lub nie
    2. Kinetyczna sztuka -> taniec, żarty i dowcipy, walki gladiatorów, contests, arena
    3. Rzeczy bardzo efektowne ale nadal praktyczne i funkcjonalne, krzykliwe kolory
    4. Gry ryzykowne
3. What do they value the most
    1. Efektowne Czyny. Sukcesy i porażki. Ale tylko Efektowne Czyny które miały sens.
4. Taboo
    1. Marnowanie życia na rzeczy nieważne.
    2. Ryzykowanie życiem innych bez postawienia swojego życia na szali

### 1.5. Główne stereotypy i powiedzonka

* To o nich opowiada się małym potworom historie o strasznych ludziach.
* "Wszyscy kiedyś umrzemy - grunt, by umrzeć DOBRZE."

### 1.6. Pytania dodatkowe

1. What is one major stereotype about your culture that you want to dispel right now?

Nie jesteśmy "irytującymi, niezdyscyplinowanymi głośnymi wiecznie zaczepnymi psycholami którzy chcą się tylko bić". Tylko Arkadia taka jest ;-). A na serio - zaczepiamy i atakujemy tylko osoby które SZANUJEMY. Jak chcesz się testować, jak chcesz sprawdzić jak dobry jesteś jeśli tylko to mówisz? Popatrz, tam gdzie inni wyślą na front swoich podwładnych, tam my idziemy z przodu. Mamy dużą dyscyplinę i cierpliwość - trudno nie mieć jeśli ostatnie dwa dni siedzisz nago w rowie i czekasz jak ten cholerny niedźwiedź podejdzie.

2. What are the things that you consider acceptable that may not be accepted in other cultures?

* Wieczna gra z ryzykiem -> niekoniecznie wybierzesz najbezpieczniejsze rozwiązanie; zwykle wybierzesz to o większym zwrocie i ryzyku
* Narażanie nawet najmłodszych na ryzyko -> nie tylko dorośli, dzieci też. Trzeba stawiać życie na szali od najmniejszego
* Wieczne poszukiwanie dobrego przeciwnika -> wszyscy umrzemy, grunt, by znaleźć WŁAŚCIWEGO przeciwnika i WŁAŚCIWE wyzwanie
* Ciągłe konfrontowanie się i atakowanie -> jeśli nie będziesz testować siebie i członków rodziny, jak staniesz się silniejszy?

3. What is one characteristic that describes your culture as a whole?

zaczepni, odważni, szaleni, pierwsi do bitki

5. Who do you consider to be your family? How FAMILY looks like (one home, head of family, responsibilities)

.

6. When do you consider a person to be an adult?

po samodzielnym pokonaniu odpowiedniej klasy "niedźwiedzia" który był problemem dla grupy

8. What are the traits that are considered to be positive?

odwaga, waleczność, twardość, zaradność

9. Who do you think are the heroes of society?

?

10. What are the cultural values that children are exposed to immediately?

"Życie jest ciężkie i dostaniesz wpierdol. Kwestia taka, że musisz przetrwać i oddać."

11. What kinds of work are considered the be prestigious?

łowca potworów, wojownik, komandos, advancer