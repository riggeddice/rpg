## 1. Sarderyci
### 1.1. Notka

* Jak o nich mówimy: sarderyta, sarderytka, sarderyci
* Klasa: skrajni saityci, uniwersaliści, nomadzi deathworld, resourceful
* Występowanie: Neikatis (pustynia), Astoria (nieużytki)
* Pochodzenie: miks noktiańsko - saitaerski

### 1.2. Omówienie ogólnie



### 1.3. Gdzie ich spotkamy



### 1.4. Fiszka

1. Metapsychology (tenets and values of psyche, art, culture)
    1. Adaptation, Self-direction: you can become anything you wish and nothing can ever stop you
    2. Wyciągnij rękę, ale zdrady nie wybaczaj nigdy
    3. Zawsze istnieje forma która jest lepiej zaadaptowana. Nigdy nie zatrzymuj się póki jej nie osiągniesz.
    4. Nigdy się nie poddawaj - czegoś jeszcze nie wiesz
2. Symbolism and Aesthetics
    1. Bardzo bogata sztuka, zwłaszcza muzyka i oratorska. Nic "dużego". Zdobienia jednostek.
    2. Sztuka i opowieści są bardzo wysoko cenione -> one sprawiają, że przy takiej różnorodności nadal jesteśmy razem.
3. What do they value the most
    1. adaptacja, pomaganie innym, wytrwałość
4. Taboo
    1. Become complacent -> musisz się adaptować i zmieniać cały czas

### 1.5. Główne stereotypy i powiedzonka

* Unholy scary posthuman monsters... brudne technokaraluchy ludzkości
* Mimo życia w absolutnym brudzie i zerowych warunkach zawsze są uśmiechnięci
* Unikają konfliktu za wszelką cenę, wolą handlować i się posunąć. Ale jak już wejdą do walki...
* "Skażony obszar? Super, zajmijcie te ziemie a my przejdziemy na tamte i się posuniemy"
* "Maciek umarł? Oki, Ty dostajesz jego nerki, Ty mechaniczną rękę a resztę przetworzymy w jedzenie"

### 1.6. Pytania dodatkowe

1. What is one major stereotype about your culture that you want to dispel right now?

"Unholy posthuman monsters"? Sarderyci robią wszystko co mogą by móc przetrwać, ale nie sprawia to, że przestają być ludźmi. Dzięki szerokości bioform i technoform nie ma dwóch podobnych sarderytów. A jednak każdy skłonny współpracować z sarderytami otrzyma wikt i opierunek. Sarderyci są bardzo szczerzy i pomocni. Acz zrobią wszystko by przetrwać. Nie tworzą państw, ale pomogą też poza swoim klanem. "Przetrwamy niezależnie od okoliczności."

2. What are the things that you consider acceptable that may not be accepted in other cultures?

* Rekonstrukcja i readaptacja bioform. Nie znajdziesz dwóch podobnych do siebie sarderytów.
* Ciała to jedynie źródło materiałów biologicznych i mechanicznych. 
* Istoty niemożliwe do bycia przydatnymi zostaną zabite z szacunkiem. Np. za stare.
* Masz obowiązek poświęcić WSZYSTKO by być lepszym. Sentymentalne skupienie się na ciele jest grzechem.
* ...i wszystko w formie deathworld nomads

3. What is one characteristic that describes your culture as a whole?

Różnorodność bioform, modyfikacje biologiczne i technologiczne. Karaluchy wśród ludzi.

5. Who do you consider to be your family? How FAMILY looks like (one home, head of family, responsibilities)

Klan. Moja grupa nomadów. Pomagam wszystkim wszędzie, ale moja rodzina to moja grupa / mój crawler / moi przyjaciele.

6. When do you consider a person to be an adult?
8. What are the traits that are considered to be positive?
9. Who do you think are the heroes of society?
10. What are the cultural values that children are exposed to immediately?
11. What kinds of work are considered the be prestigious?
12. What are the normal food products that are eaten?
13. Are there some colors / symbols that are being used for certain events?
14. Are there some beliefs that are associated with XXX
15. How will you describe a typical day in your life?