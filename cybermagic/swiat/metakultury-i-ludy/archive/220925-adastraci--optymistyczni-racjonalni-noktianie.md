## 1. Adastraci
### 1.1. Notka

* Jak o nich mówimy: adastrata, adastratka, adastraci
* Klasa: próżniowcy (noktianie), terragenofile, racjonaliści, heterogeniczni
* Występowanie: Noctis -> Orbiter, Cieniaszczyt, ród Perikas z Aurum.
* Pochodzenie: Noctis

### 1.2. Omówienie ogólnie

Adastraci pochodzą z Noctis. Człowiek ujarzmił planetę, uniknął apokalipsy, uciekł w kosmos i się rozprzestrzenił. Duma z osiągnięć technicznych i dowód na to, że może istnieć lepsze jutro sformułowało cały nurt adastracki.

Szeroko współpracujący ze sobą adastraci są optymistami i pionierami z natury. Jeden z pierwszych nurtów noktiańskich który dotarł do Sektora Astoriańskiego jeszcze przed Pęknięciem Rzeczywistości. Wierzą, że wszystko da się rozwiązać używając technologii i niezłomnej ludzkiej natury. Co prawda są próżniowcami, ale skutecznie adaptują się też do planetarnego życia. Gardzą religiami, metafizyką itp - "jesteśmy lepsi niż to". Wszystko da się zrozumieć używająć technologii.

Adastraci mają wbudowaną w podstawową kulturę usprawnienie ludzkiego życia. Nie przeszkadza im transhumanizm ani współpraca z innymi terragenami. To, czego jednak się boją - że ludzkość doprowadzi do samozagłady (zbyt dużo takich rzeczy działo się w przeszłości). Z tego wynikają ich tabu.

### 1.3. Gdzie ich spotkamy

Adastraci to jeden z głównych nurtów noktiańskich, który dotarł do Astorii dawno temu. Tak więc adastratów spotkamy dość sporo w różnych miejscach, zwłaszcza, że adastraci się dobrze integrują i raczej nie trzymają się innych adastratów. 

W Aurum - ród Perikas jest rodem czysto adastrackim.
Na Orbiterze występuje duża populacja adastratów. Tak samo w Cieniaszczycie.

### 1.4. Fiszka

1. Metapsychology (tenets and values of psyche, art, culture)
    1. Stimulation (nowe, dalej, lepiej) + Achievement (zostaw coś po sobie, spętaj naturę)
    2. efektowne konstrukcje, lepsze jutro, megastruktury
    3. wszechstronność i bogactwo myśli, sztuka i nauka
    4. SKRAJNA racjonalność i logika -> wszystko jest możliwe do pokonania, wszystko da się opanować, ludzkość ZAWSZE wygra
2. Symbolism and Aesthetics
    1. bardzo bogata i różnorodna sztuka, która często nie posiada walorów poza estetycznymi
    2. sztuka jest w większości optymistyczna i zachęcająca do działania
    3. duże skupienie na grach i zagadkach logicznych
    4. większe i bardziej efektowne oznacza lepsze. Triumf ludzkości.
3. What do they value the most
    1. optymizm, racjonalność, konstrukcja -> wszystko da się rozwiązać używając nauki, wszystko uda nam się osiągnąć razem
4. Taboo
    1. religia, metafizyka, fanatyzm
    2. systemy autoreplikujące
    3. plagi, plagi memetyczne

### 1.5. Główne stereotypy i powiedzonka

* "Idź do przodu, nie patrz za siebie"
* "Wszystko da się rozwiązać używając logicznego podejścia i technologii"
* "Jeśli sami się nie zniszczymy, jesteśmy skazani na sukces"
* S: Nierealistyczni, nie patrzą na to co jest możliwe, żyją z głową w chmurach
* S: Traktują swoje TAI jak przyjaciół, ale powiedz tylko o Seilii i zjeżają się jak koty...
* S: Ta ich niesamowita młodzieńczość sprawia, że już czuję się stary. Strasznie to irytujące, takie bezpodstawne przekonanie że mają rację.

### 1.6. Pytania dodatkowe

Pytania dodatkowe:

1. What is one major stereotype about your culture that you want to dispel right now?

"Prędzej rozwalą pół planety niż zamieszkają koło nieznanej okresowej efemerydy"? Na pewno nie zniszczymy POŁOWY PLANETY - nasze eksperymenty i działania są zawsze bezpieczne; akurat unikamy robienia krzywdy innym bardziej niż próbujemy 'wysadzać planety'. A jak chodzi o okresowe efemerydy? Okresowość oznacza, że jest tam jakaś regularność -> więc da się do tego dojść, zbadać i okiełznać. A jak nie wiesz, co ta efemeryda robi i czemu się pojawia... nie sądzisz że to bardziej niebezpieczne niż jej ujarzmienie jako źródło energii? Popatrz co już ludzkość zrobiła! Taka efemeryda to pikuś.

2. What are the things that you consider acceptable that may not be accepted in other cultures?

* Bioinżynieria, konstrukcja TAI itp. Uplifting, BO MOŻEMY. Traktowanie wszystkich Terragenów "po równo".
* Odrzucanie wszelkiej metafizyki i religii i przechodzenie w tryb smerfa Ważniaka i próba zracjonalizowania tego technologią.
* Można posunąć się za daleko - to straty konieczne. Kto nie próbuje, nie wygrywa.
* Irytująca naiwność wobec ludzkiej natury - adastraci zakładają, że ludzie są fundamentalnie dobrzy.

3. What is one characteristic that describes your culture as a whole?

Optymizm, umiłowanie racjonalizmu (i technologii), próba przekraczania granic

4. Who do you consider to be your family? How FAMILY looks like (one home, head of family, responsibilities)

Niskie więzi rodzinne, raczej luźna asocjacja pod kątem zainteresowań i tego jak możemy sobie pomóc.

5. When do you consider a person to be an adult?

Wiek biologiczny - 18+.

6. What are the traits that are considered to be positive?

Kreatywność, optymistyczne podejście, energia i chęć do działania

7. Who do you think are the heroes of society?
8. What are the cultural values that children are exposed to immediately?
9. What kinds of work are considered the be prestigious?
10. What are the normal food products that are eaten?
11. Are there some colors / symbols that are being used for certain events?
12. Are there some beliefs that are associated with XXX
13. How will you describe a typical day in your life?
