## 1. Faerilowie
### 1.1. Notka

* Jak o nich mówimy: faeril, faerilka, faerile / faerilowie
* Klasa: gaianie, technologiczni, otwarci klanowcy, terragenofile, heterogeniczni, builderzy > pionierzy
* Występowanie: Neikatis, Syndykat Aureliona
* Pochodzenie: korporacyjne supersystemy

### 1.2. Omówienie ogólnie

Mniej zamieszkałe planety, kolonie górnicze, kolonie karne. Ludzie, którzy trafili do megakorporacji patrzących tylko na pieniądze. Korporacje, które patrzą na ludzi jak na surowce. Dzieci urodzone z długiem dziedziczonym od rodziców (bez opcji odrzucenia), z obowiązkową edukacją (płatną) i które nigdy nie wyjdą z niewoli. Lepiej rzucić więcej ludzi na problem niż zapłacić trochę więcej i dać im lepszy sprzęt.

A potem rzeczywistość Pękła.

Megakorporacje upadły. Pesymizm i rozpacz przegrały jako dominanty memetyczne. I faerilowie zostali sami - z niskim poziomem technologicznym, ale bez długu i z ogromną determinacją by przetrwać.

Faerilowie zostali poważnie zmienieni przez przeszłość przed Pęknięciem. Nadal do dzisiaj niewolnictwo u nich obowiązuje, do dzisiaj mają skupienie na ochronie swoich rodzin i swojego klanu oraz doceniają skuteczne społeczeństwo kastowe (wynikające z klanów i tego że różni ludzie pełnili różne role). Ale dzięki doświadczeniom związanym z technologią (i jej brakiem) pozytywnie podchodzą do nowinek czy wszczepów. Byle ludzie zostali ludźmi.

Oathbound Synthesis.

### 1.3. Gdzie ich spotkamy

Neikatis, K1, Valentina, cywilny sektor Orbitera. Ale też - Syndykat Aureliona, który jest główną hiperkorporacją która przetrwała.

### 1.4. Fiszka

1. Metapsychology (tenets and values of psyche, art, culture)
    1. Family, Security, Humility -> "masz tylko swoich ludzi / klan i nie wiesz co Cię czeka. Ale obowiązkiem i trudem osiągniecie lepsze jutro."
    2. Technologia, nowinki -> "co prawda mięśnie mogą wszystko, ale możesz polegać na technologii - kupuje lepsze jutro."
    3. Kastowość -> "twoje urodzenie detminuje Twój skillsystem i Twoją przyszłość. Możesz wykupić się do innej kasty, co jest dowodem Twojej niezwykłości"
    4. Terragen -> "nieważne czy ludzie, TAI czy coś innego. Wszystko co pochodzi ze rdzenia Ludzkości się nadaje."
    5. Mistrzostwo -> achievement + humility; nieważne CZEGO nie robisz, rób to maksymalnie dobrze. Nie chodzi o rywalizację; bycie perfekcyjnym trybikiem maszyny.
    6. Skłonności do religijności / duchowości.
2. Symbolism and Aesthetics
    1. Bardzo silny nacisk na technologię, hightech i różnego rodzaju efekty tego typu
    2. Perfekcja, mastery. Bardzo duże skupienie na szczegółach.
3. What do they value the most
    1. Pełnienie swojej roli we WŁAŚCIWY sposób we WŁAŚCIWYM miejscu i dzięki temu zapewnienie swojemu klanowi przychodów i bezpieczeństwa
4. Taboo
    1. Zdrada swojego klanu lub swojej kasty. Próba bycia w innym miejscu niż powinno się być.
    2. Całkowicie bezduszne traktowanie innych ludzi czy terragenów.
    3. Złamanie przysięgi lub kontraktu.

### 1.5. Główne stereotypy i powiedzonka

* "Nieważne gdzie jestem przypisany, wykonam zadanie bo na tym polega ta praca."
* Praktycznie każdy faeril to kultysta; pytanie CZEGO. Gdzie trzech faerilów tam cztery kulty...
* Każda z rodzin ma co najmniej jednego mechanicznego psiaka który opiekuje się dziećmi i się z nimi bawi...
* Po cholerę im to niewolnictwo, skoro mają te wszystkie maszyny?! SERIO chodzi o to że taniej?! Na pewno mają mroczne religijne cele.
* Nie miej długu wobec faerila; nie zapomni i będziesz musiał spłacić. Oddasz pierwsze dziecko lub je nazwiesz imieniem ulubionego napoju po 15 latach...

### 1.6. Pytania dodatkowe

1. What is one major stereotype about your culture that you want to dispel right now?

To nie jest tak, że jesteśmy "szalonymi kultystami kochającymi niewolnictwo - zarówno zewnętrzne (obcy) jak i wewnętrzne (system kastowy) z przyczyn religijnych". Dzięki systemowi kastowemu każdy wie, gdzie należy i czego ma się uczyć. Jest STRUKTURA, wiesz jak żyć, nie miotasz się bez sensu. Jeśli ktoś jest wyjątkowo dobry w działaniach innej kasty, cały klan jest w stanie złożyć się by przenieść tę osobę to tamtej kasty - ale to wymaga talentu oraz determinacji, nic darmo. A jeśli chodzi o osoby z zewnątrz... jak możesz zaufać komuś kogo nie znasz jeśli nie przeszedł przez stadium niewolnika?

2. What are the things that you consider acceptable that may not be accepted in other cultures?

* Niewolnictwo - zwłaszcza, że często niewolnicy są po prostu tańsi niż automatyzacja.
* Predestynacja - każdy ma swoje miejsce i swoje talenty i ma je optymalizować i się ich uczyć. Prawda, ogranicza - ale jednocześnie nadaje wizję i fokusuje.
* Duchowość - bez religii czy jakichkolwiek wierzeń ludzki umysł nie przetrwałby tego świata. Sama rodzina to za mało. Musi być też nadzieja na lepsze jutro.
* Terragen - AI mają pełne prawa, tak jak upliftowane zwierzęta. Wszystkie terrageny.
* Zdrada klanu karana jest śmiercią a wszelkie przysięgi są traktowane śmiertelnie poważnie.

3. What is one characteristic that describes your culture as a whole?

Uduchowione społeczeństwo klanowo-kastowe bardzo mocno wierzące w technologię i postęp dzięki niej.

5. Who do you consider to be your family? How FAMILY looks like (one home, head of family, responsibilities)

Klan, łącznie z niewolnikami i terragenami. Na różnych prawach, ale zawsze. Są starsi klanu (tych się słuchamy), niewolnicy (tych traktujemy dobrze) i inni członkowie klanu. Co ciekawe, terrageny są traktowane na równi z ludźmi.

6. When do you consider a person to be an adult?

Gdy jest w stanie pełnić rolę pracownika zależnego w swoim obszarze; gdy może stać się Czeladnikiem w działaniach swojej Kasty.

8. What are the traits that are considered to be positive?

pracowitość, humility, spryt (rekombinowanie tego co mamy w coś lepszego)

9. Who do you think are the heroes of society?

Osoby, które miały wybór i jednak zrobiły to co powinny. Osoby, które poświęciły się w imię swojego klanu. Osoby, które osiągnęły mistrzostwo w swojej dziedzinie.

10. What are the cultural values that children are exposed to immediately?

"Każdy ma swoje miejsce w egzystencji i dobre trzymanie się tego miejsca prowadzi do szczęścia i sukcesów całego klanu".

11. What kinds of work are considered the be prestigious?

W ich wypadku - każda. Jak długo wykonujesz pracę do której jesteś predestynowany. Nie ma szczególnej chęci do zmiany kast.
