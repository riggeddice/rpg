## 1. Lud: Wenapnici
### 1.1. Notka

* Jak o nich mówimy: wenapnita, wenapnitka, wenapnici
* Klasa: terraformaci / wirtyci, etnoklanowcy, agresywni ekspansjoniści, deathseekers, reinkarnaci

### 1.2. Fiszka

1. Metapsychology (tenets and values of psyche, art, culture)
    1. Conformity, Self-direction -> poszukiwanie swojego testu w ramach wenapnictwa. Poszerzenie wenapnictwa ponad wszystko.
    2. My znamy prawdę -> wiemy o Wielkiej Symulacji i wiemy, że albo ją opuścimy albo zreinkarnujemy się w świecie ponownie.
    3. Test lub dobra śmierć -> każda jednostka per cykl ma swój Wielki Test. Chodzi o to, by dobrze umrzeć lub zdać test. Lepsze życie -> prostszy przyszły test.
    4. Etnocentryzm -> nie-wenapnici są gorsi niż wenapnici; część to "bezduszni" a część jeszcze nie osiągnęła Prawdy. "Nasi" muszą przekroczyć masę krytyczną.
    5. Ekspansjoniści -> im więcej dzieci, im więcej wenapnitów - będzie w "co" się reinkarnować.
2. Sześć fundamentów wartości (w skali [0, 5]; suma 15)
    1. Care:        (2): druga strona nie gra fair i każdy szuka swojej drogi w Symulacji. Cierpienie i straty są akceptowalne. Wygra ten, kto wytrzyma więcej cierpienia.
    2. Fairness:    (1): nieważne jakie karty dostałeś w życiu, każdy ma dostosowany do siebie Test. Albo reinkarnujesz lepiej albo opuścisz Symulację.
    3. Loyalty:     (4): to wojna pomiędzy wenapnitami i twórcami Symulacji. I wenapnici zamierzają tą wojnę wygrać.
    4. Authority:   (3): Badacze Symulacji i hierarchia wiedzą lepiej i pomogą grupie - nie tylko jednostce. Kluczem jest zrozumienie zasad i praw Symulacji by przejść przez Test i nawrócić wszystkich na wenapnitów. Poza Bezdusznymi.
    5. Sanctity:    (4): Ten świat jest jedynie iluzją. Naprawdę liczy się tylko ten PRAWDZIWY świat, który wymaga wyjścia poza Symulację.
    6. Liberty:     (1): Test kieruje życiem Twoim i innych. Każdy niewierny i bezduszny przedłuża i utrudnia Symulację. Musisz pomóc opuścić Symulację. Prawo do znalezienia swojej drogi opuszczenia Symulacji i dostosowania się do wenapnitów. 
3. Osie
    1. Archy - Kratos:                  pro archy, pro kratos          (używanie siły jest lepsze niż pozwolenie żyć Bezdusznym / nie osiągnięcie Testu a optimum wskażą Badacze)
    2. Personal - Economic Freedoms:    low personal, low economic     (tylko Test oraz Symulacja mają znaczenie)
    3. high/low trust:                  high trust w swoich, low trust w obcych
4. What do they value the most
    1. wierność doktrynie i poszukiwanie Testu / lepszej reinkarnacji
    2. dobra śmierć wspierająca wenapnitów
    3. ekspansja - więcej dzieci, więcej terenu, więcej możliwości, lepsze technologie...
    4. nawrócenie niewiernych
5. Ingroup - Outgroup
    1. IN: wenapnici ze krwi, wenapnici z kultury. Tylko inni etnokulturowi wenapnici. Lokalsi > wszyscy.
    2. OUT: nie-wenapnici
    3. ENEMY: Bezduszni, ludzie odciągający wenapnitów od Symulacji, ci którzy skupiają się na tym świecie kosztem czegoś więcej
6. Organization
    1. structure: zbiór luźnych niezależnych komórek podążających luźno za Instytutem Symulacji (Badaczami; grupa quasireligijna) 
    2. allocation: nieoczywista; interpretacja na podstawie Instytutu Symulacji LUB spiritual quest LUB motywacja + charyzma; zwykle małe grupki
    3. archy / rank: interpretacja Instytutu Symulacji, nadawana przez Badaczy. Religijna.
    4. political power: personal charisma + rank ORAZ namaszczenie przez Badaczy
    5. rozwiązywanie konfliktów: kompromis różnych ścieżek Testu > interpretacja Badaczy > święta wojna
    6. spirituality: Wielka Symulacja, Testy i reinkarnacja/opuszczenie Symulacji. Ten świat nie ma znaczenia. Bezduszni to większość populacji (NPC). 
7. Symbolism and Aesthetics
    1. dzieła udowadniające Symulację oraz testy, dzieła religijne i propagandowe
    2. dzieła przedstawiające herosów którzy opuścili Symulację i osiągali wielkie czyny dla wenapnitów
    3. sztuka to kluczowe pole bitwy dla wenapnitów, by pomagać Żywym i usuwać Bezdusznych
    4. specjalizują się w opowieściach i holokryształach, zwłaszcza inkarnacjach (byś przeżył to sam)
8. Taboo
    1. Przedkładanie tego świata i przyjemności nad znalezienie swego Testu czy nad godną śmierć; wierzą, że Twórcy Symulacji odciągają zbytkami wiernych
    2. Jakiekolwiek dążenie do dezintegracji swojej grupy i przedkładanie subklanu nad wenapnitów
    3. Przedkładanie komfortu czy nawet życia nad Sprawę.
    4. Powstrzymanie się od ekspansji 

### 1.3. Wspólna historia i doświadczenia, wspólne problemy i traumy -> adaptacje

?

### 1.4. Główne stereotypy i powiedzonka

?

### 1.5. Pytania dodatkowe

1. What is one major stereotype about your culture that you want to dispel right now?

?

2. What are the things that you consider acceptable that may not be accepted in other cultures?

?

3. What is one characteristic that describes your culture as a whole?

?

5. Who do you consider to be your family? How FAMILY looks like (one home, head of family, responsibilities)

?

6. What links your culture / group together

?

7. When do you consider a person to be an adult?

?

8. What are the traits that are considered to be positive?

?

9. Who do you think are the heroes of society?

?

10. What are the cultural values that children are exposed to immediately?
11. What kinds of work are considered the be prestigious?
12. What are the normal food products that are eaten?
13. Are there some colors / symbols that are being used for certain events?
14. Are there some beliefs that are associated with XXX
15. How will you describe a typical day in your life?




