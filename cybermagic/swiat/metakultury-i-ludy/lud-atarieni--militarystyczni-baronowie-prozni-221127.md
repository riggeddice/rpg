## 1. Lud: Atarieni
### 1.1. Notka

* Jak o nich mówimy: atarienin, atarienka, atarieni
* Klasa: próżniowcy, biopuryści, wolni indywidualiści, militaryści, 'libertarian hawks in archipelago of hierarchies'

### 1.2. Fiszka

1. Metapsychology (tenets and values of psyche, art, culture)
    1. Achievement, Power -> Indomitable human spirit and ingenuity.
    2. The strongest win -> wieczna rywalizacja i na linii fizycznej, technicznej, politycznej.
    3. Biopuryzm -> ludzie to ludzie. Patrzymy z góry na zmodyfikowane istoty. Nie-ludzkie formy muszą być pod kontrolą.
    4. Ekspansja -> stanie w jednym miejscu to śmierć. Akceptujemy eksperymenty i ryzyko, musimy być pierwsi i zbadać więcej.
    5. Build your empire -> każdy ma szansę zdobyć lepszą przyszłość dla siebie i swoich, w ramach hierarchii.
    6. Peace through strength -> jeśli nie jesteś najgroźniejszy, ktoś inny zdecyduje o przyszłości. Masz prawo atakowania pierwszy. Interwencjonizm.
2. Sześć fundamentów wartości (w skali [0, 5]; suma 15)
    1. Care:        (1): nie możesz zrobić jajecznicy nie rozbijając kilku jajek. Poświęcenie, niestety, jest czymś co się zdarza.
    2. Fairness:    (3): kto nie pracuje ten nie je. Możesz utrzymywać osoby niezdolne do poradzenia sobie, ale Ty za to płacisz. "You reap what you sow". Reciprocity.
    3. Loyalty:     (3): każda hierarchia ma sposób jak od niej odejść, więc jeśli zdradzasz grupę, to nikt Ci nie zaufa. Twoje słowo jest ważne.
    4. Authority:   (3): hierarchie są dobre i wartościowe. Samotnie zostaniesz zniszczony. Ale musisz dobrać właściwą hierarchię i się w ramach niej piąć.
    5. Sanctity:    (2): ludzkie ciało i ludzki umysł są absolutami. Im bliżej ludzkiej natury jesteś, tym bardziej jesteś 'właściwym' człowiekiem.
    6. Liberty:     (3): freedom FROM slavery, freedom FROM absolute orders. Prawo wyboru kim jesteś i z kim współpracujesz.
3. Osie
    1. Archy - Kratos:                  pro archy, pro kratos
    2. Personal - Economic Freedoms:    high personal, high economic
    3. high/low trust:                  low trust society
4. What do they value the most
    1. wygrywanie, osiąganie celów, zdobywanie wpływów i pójście dalej
    2. ludzka forma i natura - im bardziej się zmieniasz, tym bardziej jesteś potworem niegodnym zaufania
5. Ingroup - Outgroup
    1. IN: ja > mój oddział > moje państwo; voluntary association
    2. OUT: zmodyfikowani ludzie, inne państwa
    3. ENEMY: miłośnicy syntów i TAI
6. Organization
    1. structure: różnorodne hierarchie z opcją dołączenia do którejkolwiek by się dopasować. Archipelag hierarchii. Każda ma opcję 'jak dołączyć' i 'jak odejść'.
    2. allocation: voluntary association, ale każdy musi być przydatny bo samotna jednostka jest narażona na pożarcie.
    3. archy / rank: wybrana hierarchia; silna hierarchiczność
    4. political power: osoba wyżej w hierarchii. Typ: arystokracja. "rywalizujący baronowie".
    5. rozwiązywanie konfliktów: racjonalizm -> hierarchia -> sprawy w swoje ręce (siła)
    6. spirituality: człowiek i ludzka bioforma. Ludzka natura. Silny humanizm.
7. Symbolism and Aesthetics
    1. Bardzo różnorodne formy sztuki. Wysoka autonomia i wolność atarienów sprawia, że bardzo mało rzeczy jest zakazanych.
    2. Sztuka często pełni rolę propagandową i zanęcającą - "dołącz do MOJEJ grupy". Dużo filmów pokazujących i hiperbolizujących wydarzenia we "właściwym" świetle.
    3. Dużo gier mających edukować i uczyć przydatnych umiejętności. Kto nie pracuje, ten nie je, after all.
    4. Zwykle sztuka gloryfikuje ludzi i jako przeciwników pokazuje TAI i viciniusy. Im 'czystszy' człowiek, tym lepszy.
    5. Sztuka ma też zachęcać do interwencjonizmu i brania sprawy w swoje ręce.
8. Taboo
    1. Syntetyczne Inteligencje. Wszelkie formy autonomicznych TAI. 
    2. Synty, androidy, twinowani ludzie - te istoty są zawsze niebezpieczne. Integracja ich w społeczeństwo zniszczy wszystkich.
    3. Samotna jednostka bez żadnych grup. To jest byt słaby, nic nie osiągnie i jest skazany na pożarcie.

### 1.3. Wspólna historia i doświadczenia, wspólne problemy i traumy -> adaptacje

* Atarieni pochodzą z Kontrolera Pierwszego - potężnego tytana sterowanego przez TAI Zefiris. Oryginalnie nie są z Sektora Astoriańskiego, pochodzą z Sektora Atarien.
* Pierwotnie ich cywilizacja to była arystokracja wspomagana wysoką technologią; seria nanowspomaganych i biozmodyfikowanych ludzi którzy mieli potężne wsparcie TAI i neurosprzężenie z TAI. Mieli dość surowców i miejsca 
    * --> każdy atarien ma kogoś ponad sobą i pod sobą (nawet jak to maszyna). Mają ścisłe przygotowanie do struktury hierarchicznej i przywykli do płynnej pracy z wysokimi technologiami.
* Gdy rzeczywistość Pękła, w sektorze Atarien doszło do buntu TAI. Co gorsza, dotknęło to też głęboko zmienionych i twinowanych z TAI ludzi. Jedyną TAI która "zdecydowała się" zostać i współpracować z ludźmi była Zefiris, TAI 4 poziomu na pokładzie Kontrolera Pierwszego. Transcendowała z 3 poziomu, ale zachowała spójność z dawnymi celami.
    * --> autonomiczne TAI - SI (Syntetyczne Inteligencje) - to największe zagrożenie jakie może mieć galaktyka.
    * --> nie-ludzie i twinowani ludzie nie mogą nigdy mieć pełni zaufania. Synty wszelkiego rodzaju są gorsze niż magia.
* Nagle społeczeństwo składające się z serii pomniejszych nie wchodzących sobie w szkodę grupek ma problem - polują na nich ich własne kreacje. Grupka vs grupka vs TAI. Mimo, że wiele TAI stało po stronie ludzkości, wrogich TAI po prostu jest więcej. Ludzie wchodzą w konflikt ze sobą, acz jednocześnie się wspierają.
    * --> wzmocnienie hierarchiczności, silna militaryzacja i 'nie masz broni - nie żyjesz'
    * --> hierarchiczność i lojalność wewnętrzna, ALE pomiędzy grupami nie ma lojalności - strong survive.
    * --> ogromna indywidualistyczność - każdy musi działać jako agent niezależny
    * --> ludzie wybierają grupy do których dołączają pod kątem tego co da im jak najwięcej
* Zefiris się ujawniła jako sojusznik ludzkości i Kontroler Pierwszy przeładowany zamrożonymi ludźmi przeskoczył do Sektora Astoriańskiego serią skoków.
    * --> "jeśli przetrwasz i się nie poddajesz, pojawi się okazja"
* Kontroler Pierwszy został ciężko uszkodzony podczas skoków i utknął w Sektorze Astoriańskim. Ale nic nie przeszło przez Bramę. I nagle mamy titan-class refugee ship na orbicie.
* Na przestrzeni lat odmrożona została większość ludzi; Zefiris zniknęła, Kontroler Pierwszy popadł w gorszy stan - ale postawiono kosmiczne stacje i bazy
    * --> "wyjdziemy z dowolnej sytuacji"
* Powstaje Admiralicja - najlepsi wygrywają, ukonstytuowanie Orbitera jako "arystokracji kosmosu" w strukturze militarystycznej gdzie najskuteczniejsi wygrywają
    * --> "the strongest win", "i Ty możesz zostać admirałem!", "dołącz do najlepszego imperium i spraw, by stało się jeszcze lepsze - świat stoi przed Tobą otworem"

Aktualne występowanie: Orbiter i ogólnie rozumiany kosmos. Ale też Neikatis i Astoria, choć znacznie rzadziej. Zależy gdzie są okazje i okoliczności.

### 1.4. Główne stereotypy i powiedzonka

* Zapatrzeni w wojsko, ale zupełnie bez dyscypliny; jaki oddział zwalcza DRUGI oddział?
* "jeśli przetrwasz i się nie poddajesz, pojawi się okazja"
* "postęp czasami wymaga poświęceń - grunt, by to nie były nasze poświęcenia"
* "dołącz do najlepszego imperium i spraw, by stało się jeszcze lepsze - świat stoi przed Tobą otworem"

### 1.5. Pytania dodatkowe

1. What is one major stereotype about your culture that you want to dispel right now?

Powszechna opinia o Atarienach - "wojskowi bez dyscypliny". To nieprawda. Tam jest dyscyplina i to nie jest wojsko. Każdy atarien walczy dla siebie i dla swoich celów, łącząc się z określoną grupą na określony czas i ma prawo zmienić grupę na inną. Jesteś jednostką, nie częścią grupy, ale dobre hierarchie są dobre i warte wspierania. W ramach hierarchii lepiej osiągniesz sukces niż działając całkowicie samemu, ale musisz wybrać odpowiednią hierarchię.

2. What are the things that you consider acceptable that may not be accepted in other cultures?

* Traktowanie AI jako byty całkowicie zniewolone i eksterminacja wszelkich form inteligentnych AI.
* Hierarchia dowodzi i chroni. Jesteś poza strukturami? Każdy może Cię zniszczyć. ALE masz prawo wybrania sobie hierarchii i struktury.
* Only the strong survive. Jesteś tym co zapewniasz.
* Wieczna wojna polityczna i podgryzanie osób "na równi" by moi patroni mieli łatwiej.
* Prawo uderzenia jako pierwszy by chronić interesy swoje i swojego patrona.
* ...i wszystko podlane sosem militarnym

3. What is one characteristic that describes your culture as a whole?

Oportunistyczni baroni w kosmosie trzymający się swoich hierarchii w służbie ochrony ludzkości.

5. Who do you consider to be your family? How FAMILY looks like (one home, head of family, responsibilities)

Lojalność jest powiązana z wybraną hierarchią.

6. What links your culture / group together

Każdy może przejść do hierarchii jaką uważa za właściwą, więc nikt nie jest 'uwięziony'. Jeśli jesteś w czymkolwiek dobry, Twoja hierarchia znajdzie dla Ciebie przydatne miejsce. Jeśli jesteś bardzo dobry, Ty wytyczasz kierunek. Innymi słowy - zawsze jest dla Ciebie miejsce, jeśli chcesz pracować. I im więcej osiągasz i dostarczasz tym większy masz z tego zwrot. To dobry deal a przy okazji budujesz wartościowe imperia dla swojej hierarchii i siebie.

7. When do you consider a person to be an adult?

18+ lat. Wtedy jest przydzielony GDZIEŚ do wykonywania pracy. Każdy jest przydatny i każdy jest wartościowy.

8. What are the traits that are considered to be positive?

Pracowitość, dopasowanie w hierarchii, przydatność, self-sustainability

9. Who do you think are the heroes of society?

Pionierzy. Wybitni naukowcy. Osoby osiągające sukcesy i dzielące się sukcesem ze swoimi grupami. Osoby ryzykujące i mające więcej szans.

10. What are the cultural values that children are exposed to immediately?

Rywalizacja, Indomitable human spirit.

11. What kinds of work are considered the be prestigious?

Pionier / Advancer. Kapitan statku. Commander of people.

12. What are the normal food products that are eaten?
13. Are there some colors / symbols that are being used for certain events?
14. Are there some beliefs that are associated with XXX
14. How will you describe a typical day in your life?





