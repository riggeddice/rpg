## 1. Lud: Sarderyci
### 1.1. Notka

* Jak o nich mówimy: sarderyta, sarderytka, sarderyci
* Klasa: skrajni saityci, uniwersaliści, nomadzi deathworld, resourceful

### 1.2. Fiszka

1. Metapsychology (tenets and values of psyche, art, culture)
    1. Adaptation, Self-direction: you can become anything you wish and nothing can ever stop you
    2. Wyciągnij rękę, ale zdrady nie wybaczaj nigdy
    3. Zawsze istnieje forma która jest lepiej zaadaptowana. Nigdy nie zatrzymuj się póki jej nie osiągniesz.
    4. Nigdy się nie poddawaj - czegoś jeszcze nie wiesz
2. Sześć fundamentów wartości (w skali [0, 5]; suma 15)
    1. Care:        (5|3): Wobec swojej grupy - absolutna. Wobec innych - wysoka.
    2. Fairness:    (1): Nie ma takiego znaczenia jak osiągasz swoje cele, oszustwem czy współpracą i czy to proporcjonalny wynik. Wbudowane "wedle potrzeb".
    3. Loyalty:     (5|1): Wobec swojej grupy - absolutna. Wobec outgroup - niska.
    4. Authority:   (2|1): Wobec swojej grupy - wysoka. Wobec outgroup - niska.
    5. Sanctity:    (1): Historia crawlera. Przodkowie (duchowo), ci którzy byli wcześniej, ci których części posłużyły do budowy. Decyzje i wyniki.
    6. Liberty:     (1): wolność od ograniczeń bioformy, konieczność adaptacji do potrzeb grupy
3. Osie
    1. Archy - Kratos:                  pro archy, anti kratos
    2. Personal - Economic Freedoms:    low personal, low economic
    3. high/low trust:                  very high trust internal, very low trust outgroup
4. What do they value the most
    1. adaptacja, pomaganie innym, wytrwałość
5. Ingroup - Outgroup
    1. IN: Sarderyci z mojego crawlera
    2. OUT: Wszyscy inni
    3. ENEMY: farighanie, wyznawcy saitaera, zdrajcy MOJEJ grupy
6. Organization
    1. structure: małe grupki, klany skupione dookoła wioski / statku kosmicznego / crawlera
    2. allocation: w zależności od adaptacji ludzie są przypisani do prac, ludzie się adaptują gdy jest to potrzebne
    3. archy / rank: oparte na eksperckości; kompetentne osoby dowodzą w swoich obszarach
    4. political power: oligarchia merytokratyczna
    5. rozwiązywanie konfliktów: merytokracja -> decyzja dowodzenia
    6. spirituality: historia crawlera, przodkowie
7. Symbolism and Aesthetics
    1. Bardzo bogata sztuka, zwłaszcza muzyka i oratorska. Nic "dużego fizycznie". Raczej subtelne.
    2. Sztuka i opowieści są bardzo wysoko cenione -> one sprawiają, że przy takiej różnorodności nadal jesteśmy razem.
    3. Każdy crawler ma własne formy sztuki (zależy od kompetencji) opisującej historię crawlera
    4. Zapamiętywanie tych, którzy byli wcześniej; zapamiętywanie decyzji. Osiągnięcia przeszłości.
8. Taboo
    1. Become complacent -> musisz się adaptować i zmieniać cały czas
    2. Wiara w Saitaera
    3. Jakakolwiek forma zdrady SWOICH

### 1.3. Wspólna historia i doświadczenia, wspólne problemy i traumy -> adaptacje

* Gdy rzeczywistość pękła: w większości na Neikatis, w okolicach Asimear, nieużytki Astorii czy inne miejsca klasyfikujące jako 'deathworld'
* Żaden człowiek nie jest w stanie przetrwać na deathworldzie. A jednostka bez grupy nie jest w stanie sobie poradzić.
    * -> nie ma świętości, wszystko by przetrwać
    * "to się jeszcze okaże" -> wieczny, nieskończony optymizm. Wszystko jest potencjalnym surowcem. Zaadaptujemy się do wszystkiego.
    * -> bardzo duże znaczenie ścisłej grupy na której można polegać
* Nie da się przeżyć w obecności magii i wszystkich zmianach. Większość wymiera - idziemy w zaawansowane techniki cybernetyczne i biotechniczne
    * -> Organizujemy się w grupach - klanach - dookoła wspólnych bytów typu 'crawler'. Ścisła grupka / rodzina.
    * -> Można poświęcić wszystko, by tylko przetrwać.
    * -> Homogeniczność KULTUROWA i MEMETYCZNA w grupkach, ale heterogeniczność formy i ciała.
    * -> Nieważne: ludzie, TAI, viciniusy... jeśli można Ci ufać, jesteś jednym z nas.
    * -> Nie zabijajmy się. Unikajmy konfliktów. Przyjdzie kolejna katastrofa.
* Żadna grupa nie znająca się na deathworldzie nie ma jak sobie poradzić
    * -> wszystkim pomagamy - niech nikt nie musi żyć tak jak my kiedyś
    * -> jeśli to wroga grupa, możemy ich wprowadzić w pułapkę i wykorzystać na części
* Spotkania z innymi osobami "z zewnątrz", nie będących sarderytami wiążą się z szokiem. Sarderyci nie są uważani za ludzi.
    * -> potężna dychotomia nasi / obcy. Jak nas traktujesz, tak potraktujemy Ciebie
* Bardzo łatwo "zatracić siebie" w obliczu magii i deathworlda. Tak powstają farighanie. Z sarderytów.
    * -> Farighanie nie są już ludźmi. Skrajna deemfaza outgrupy. Ostracyzm == śmierć. Zdrada == śmierć.
    * -> Jeszcze silniejszy nacisk na potężny link wewnątrz swojej grupy.
* Trauma Pęknięcia Rzeczywistości i ciągłych konieczności adaptacji z deathworlda
    * -> Zawsze zachowuj optymizm. Nigdy nie przestań się uśmiechać.
    * -> Nigdy nie spoczywaj na laurach - nie wiesz jaka katastrofa pojawi się zaraz.
* Przybycie Saitaera, który reprezentuje wszystko czym sarderyci są acz stanowi zło. I reakcja ludzi na sarderytów - "Wy terrorformy".
    * -> Zrozumienie z czego to wynika - więcej uśmiechu, więcej łagodności i niegroźności, więcej pomagania
    * -> Jeszcze wyższa izolacja i nerwowa reakcja na imię Saitaera
    * -> Część osób dyskretnie czci Saitaera, acz oni są tępieni przez większość populacji

### 1.4. Główne stereotypy i powiedzonka

* Unholy scary posthuman monsters... brudne technokaraluchy ludzkości
* Mimo życia w absolutnym brudzie i zerowych warunkach zawsze są uśmiechnięci
* Unikają konfliktu za wszelką cenę, wolą handlować i się posunąć. Ale jak już wejdą do walki...
* Możesz ich obrażać do woli i nic z tym nie zrobią XD. Tylko dalej się głupio uśmiechają.
* "Skażony obszar? Super, zajmijcie te ziemie a my przejdziemy na tamte i się posuniemy"
* "Maciek umarł? Oki, Ty dostajesz jego nerki, Ty mechaniczną rękę a resztę przetworzymy w jedzenie"

### 1.5. Pytania dodatkowe

1. What is one major stereotype about your culture that you want to dispel right now?

"Unholy posthuman monsters"? Sarderyci robią wszystko co mogą by móc przetrwać, ale nie sprawia to, że przestają być ludźmi. Dzięki szerokości bioform i technoform nie ma dwóch podobnych sarderytów. A jednak każdy skłonny współpracować z sarderytami otrzyma wikt i opierunek. Sarderyci są bardzo szczerzy i pomocni. Acz zrobią wszystko by przetrwać. Nie tworzą państw, ale pomogą też poza swoim klanem. "Przetrwamy niezależnie od okoliczności."

2. What are the things that you consider acceptable that may not be accepted in other cultures?

* Rekonstrukcja i readaptacja bioform. Nie znajdziesz dwóch podobnych do siebie sarderytów.
* Ciała to jedynie źródło materiałów biologicznych i mechanicznych. 
* Istoty niemożliwe do bycia przydatnymi zostaną zabite z szacunkiem. Np. za stare.
* Masz obowiązek poświęcić WSZYSTKO by być lepszym. Sentymentalne skupienie się na ciele jest grzechem.
* ...i wszystko w formie deathworld nomads

3. What is one characteristic that describes your culture as a whole?

Różnorodność bioform, modyfikacje biologiczne i technologiczne. Karaluchy wśród ludzi.

5. Who do you consider to be your family? How FAMILY looks like (one home, head of family, responsibilities)

Klan. Moja grupa nomadów. Pomagam wszystkim wszędzie, ale moja rodzina to moja grupa / mój crawler / moi przyjaciele. I moi przodkowie.

6. What links your culture / group together

Wspólni przodkowie. Wspólna historia i przeżycia. Mamy unikalną historię tylko dla naszego crawlera i ci, którzy coś dokonają wpiszą się w historię nas wszystkich. Jesteśmy częściami większej całości - czasem dosłownie, oddając części ciała.

7. When do you consider a person to be an adult?
8. What are the traits that are considered to be positive?
9. Who do you think are the heroes of society?
10. What are the cultural values that children are exposed to immediately?
11. What kinds of work are considered the be prestigious?
12. What are the normal food products that are eaten?
13. Are there some colors / symbols that are being used for certain events?
14. Are there some beliefs that are associated with XXX
15. How will you describe a typical day in your life?




