## 1. Lud: Savaranie
### 1.1. Notka

* Jak o nich mówimy: savaranin, savaranka, savaranie
* Klasa: próżniowcy (noctis), silni klanowcy, biopuryści + 'swoje' ai

### 1.2. Fiszka

1. Metapsychology (tenets and values of psyche, art, culture)
    1. Tradition (stare techniki działają), Conservation (zachowanie i oszczędność), Family (klan, "moi ludzie")
    2. Bardzo duża nieufność - wobec urządzeń (stare i kiepskie), obcych... ale jak się przekonają, 'blood clan for life'
    3. Jesteś częścią czegoś większego. Musisz zaakceptować śmierć dla dobra reszty Rodziny / Klanu.
    4. Niewygoda i cierpienie hartuje ducha i ciało. Hart ciała i ducha są najważniejsze.
2. Sześć fundamentów wartości (w skali [0, 5]; suma 15)
    1. Care:        (4): cały ekosystem, pełne sustainability jest pod opieką. Śmierć jednego modułu może zabić statek. Pełen recykling. Cała natura. Każda osoba.
    2. Fairness:    (1): nerka nie otrzymuje więcej tlenu niż wątroba. To co jest, to jest. Czasem robisz wszystko co w Twojej mocy i umierasz dla reszty systemu.
    3. Loyalty:     (4): wszyscy jesteśmy tylko kończynami żywego organizmu. Każdy wkłada energię i się stara a nieefektywność części systemu zabije cały system.
    4. Authority:   (3): mechanizmy chroniące system są sprawne i żywe. Należy je wspierać i o nie dbać. A nowotwory wypala się białymi krwinkami.
    5. Sanctity:    (2): system jest całym światem. System jest czymś dużo większym niż tylko suma bytów. Jesteśmy jednym systemem i jednym bytem.
    6. Liberty:     (1): freedom FROM pointless harm, freedom TO perform actions optimal to the system
3. Osie
    1. Archy - Kratos:                  ambivalent archy, pro kratos
    2. Personal - Economic Freedoms:    low personal, low economic
    3. high/low trust:                  high trust internal, low trust outgroup
4. What do they value the most
    1. silence + stoicism + conservation of energy
    2. recycling, self-sufficiency, jack of all trades
    3. sacrifice for your people
5. Ingroup - Outgroup
    1. IN:      wszystko - absolutnie wszystko - w "swoim" ekosystemie
    2. OUT:     wszystko poza "swoim" ekosystemem
    3. ENEMY:   wszystko zagrażające naszemu ekosystemowi
6. Organization
    1. structure: brak jawnej i jasnej struktury; każdy odpowiada za to co jest potrzebne i co umie + koordynacja
    2. allocation: pod kątem tego co jest najbardziej potrzebne i kto jest dostępny. Lub przekierowanie przez dowódcę.
    3. archy / rank: brak. Każdy ma jakieś silne strony.
    4. political power: osoba desygnowana do dowodzenia / kolektywna rada 
    5. rozwiązywanie konfliktów: osoba desygnowana do dowodzenia lub rozwiązania tego konfliktu, autokratycznie
    6. spirituality: ekosystem jest żywą istotą. Wszyscy optymalizują zdrowie i stan tej "istoty". Przetrwamy dłużej niż inni. Żywy statek, żywy ekosystem. Obserwator, minimalna ingerencja.
7. Symbolism and Aesthetics
    1. bardzo utylitarne podejście do wszystkiego
    2. sztuka abstrakcyjna, wzory i patterny
    3. duży nacisk na ciszę, medytację i introspekcję
    4. minimalizm; jest duże piękno w niewielkich, subtelnych ruchach
    5. wiecznie powtarzający się motyw Pustki jako piękna
8. Taboo
    1. marnotrawstwo wszelakie, dążenie do indywidualizmu > zdrowie ekosystemu
    2. wybór siebie nad swoją Rodzinę (ale NIE odejście od savaran; na to jest mechanizm)

### 1.3. Wspólna historia i doświadczenia, wspólne problemy i traumy -> adaptacje

* Pochodzą z miejsc o minimalnej ilości surowców. Daleko od energii i cywilizacji. Statki kosmiczne lecące w cieniu. I minimalna ilość surowców. Zwykle z wyniszczonych / biednych obszarów
    * -> To ich zmusiło do maksymalnej oszczędności energii i kalorii oraz do silnego zaufania innym osobom ze 'swojego statku'
* Statki - ich "ekosystemy" - często umierają w bardzo dziwny, nietypowy sposób a oni są zmuszeni do bilansować minimalną ilością surowców nie wiedząc co stanie się jutro
    * -> Traktowanie statku jako żywej istoty i wszystkich i wszystkiego jako części ciała tej istoty
    * -> Akceptacja tego, że czasem kot typu pacyfikator może być bardziej wartościowy dla statku niż dwóch załogantów i odpowiednie działanie
    * -> "Nie jesteśmy lepsi od TAI czy viciniusów. To jest to, co jest teraz najbardziej potrzebne do przetrwania Domu."
* Ich jednostki są zwykle traktowane z podejrzliwością i pogardą przez innych. Stary sprzęt, często słaby. Narażony.
    * -> Tendencje do izolacjonizmu i działania poza szerszym ekosystemem
    * -> Unikanie NADMIARU komponentów; to powoduje, że ich jednostki nie są bardzo narażone
* Niektórym nie pasuje życie tak skrajne jak lubią Savaranie, zwłaszcza jak wejdziemy w inne, "lepsze" potencjalnie życie
    * -> zasada "możesz odejść, jeśli nie chcesz żyć z nami"
    * -> skupienie na wewnętrznych mechanizmach siły - grupa Cię zmusi do współpracy
    * -> skupienie na wspólnej duchowości i rodzinie

### 1.4. Główne stereotypy i powiedzonka

* S: Oni NIGDY niczego nie wyrzucają - żyją w wiecznej graciarni i syfie
* S: Nigdy nie wiesz - na tym wraku nikt by nie przetrwał, więc na pewno znajdziesz tam kilku savaran...
* S: Na pytanie 'czy mnie kochasz' powiedział 'tak'. To więcej, niż w ostatnim tygodniu od niego usłyszałam
* S: Tylko Savaranin na pytanie 'czy masz klucz francuski' znajdzie jeden w garniturze ORAZ potrafi go użyć do czegoś dziwnego. Będąc na weselu. Swoim.

### 1.5. Pytania dodatkowe

1. What is one major stereotype about your culture that you want to dispel right now?

"Cisi, nudni i paranoicy" - to nie tak. Nie potrzebują ogromnej stymulacji by móc podziwiać piękno rzeczywistości. Cisza jest formą muzyki - możesz odpocząć sam z myślami w swojej głowie. A jak chodzi o paranoję... nasze statki i tak ledwo trzymały się kupy, trzeba zrobić wszystko, by przetrwać - nawet usłyszeć zmianę dźwięku jakichś maszyn.

2. What are the things that you consider acceptable that may not be accepted in other cultures?

* Kanibalizm. Tzn, nie bezpośredni - raczej skrajny recykling absolutnie wszystkiego.
* Skrajna oszczędność materii, energii i narzędzi. Nawet, jak możesz sobie na to pozwolić.
* Skrajna nieufność wobec innych i pełne zaufanie wobec swoich. Zawsze szukasz dróg ucieczki, co gdzie jest itp.
* Poświęcanie słabszych członków Rodziny jeśli ktoś musi zginąć. 
* Eutanazja jednostek które nie są "przydatne" i akceptacja tego w wypadku swoich ran.
* Praktycznie ślepe posłuszeństwo starszyźnie Rodziny. Zawahanie na umierającym statku kosmicznym może zabić wszystkich.

3. What is one characteristic that describes your culture as a whole?

* Zaradność, Cisza, Nieufność

4. Who do you consider to be your family? How FAMILY looks like (one home, head of family, responsibilities)

Rodzina to osoby z tego samego statku / tej samej krwi. Ewentualnie byty ściśle sprzymierzone, które przejdą przez Rytuał Przyjęcia Do Rodziny i podlegają tym samym prawom i obowiązkom.

6. What links your culture / group together

Po pierwsze, możesz odejść. Nie jesteś uwięziony w Systemie. Ale jeśli decydujesz się zostać?

7. When do you consider a person to be an adult?

Rytuał Dojrzałości w wieku lat 16-25. Alternatywą jest opuszczenie Rodziny i udanie się gdzieś indziej.

8. What are the traits that are considered to be positive?
9. Who do you think are the heroes of society?
10. What are the cultural values that children are exposed to immediately?
11. What kinds of work are considered the be prestigious?
12. What are the normal food products that are eaten?
13. Are there some colors / symbols that are being used for certain events?
14. Are there some beliefs that are associated with XXX
15. How will you describe a typical day in your life?




