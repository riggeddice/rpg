## 1. Lud: Klarkartianie
### 1.1. Notka

* Jak o nich mówimy: klarkartianin, klarkartianka, klarkartianie
* Klasa: próżniowcy, terragenofile, noktianie, racjonaliści, heterogeniczni, optymizm

### 1.2. Fiszka

1. Metapsychology (tenets and values of psyche, art, culture)
    1. Dobrodziejstwo, Uniwersalism -> zbudujmy lepszy świat i podnieśmy poziom ludzi i ludzkości pokazując im prawdę. Nieważne do jakiej grupy należysz. Ludzie, TAI, wszystko.
    2. Technologia, nie magia -> wszystko jest możliwe do pokonania, wszystko da się opanować, ludzkość ZAWSZE wygra. Ujarzmiliśmy planetę, gwiazdy, ujarzmimy też magię.
    3. Poszukiwacze Prawdy -> prawda jest wartością sama w sobie. Życie z prawdą i w prawdzie jest długoterminowo bardziej opłacalne.
    4. Filantropia grupowa -> ważne jest to by pomagać innym i sprawić by świat był lepszy. Nie tylko "naszym" - "wszystkim".
    5. Walka o wolność memetyczną -> nieważne czy jesteś SI czy człowiek, masz prawo swobodnie myśleć. Dlatego wszelkie religie, kulty i pasożyty memetyczne są podejrzane. Tylko nauka.
2. Sześć fundamentów wartości (w skali [0, 5]; suma 15)
    1. Care:        (3): pomóc wszystkim zrozumieć, podnieść ludzi używając technologii i zamknąć 'magię' w 'technologii' jak przez wszystkie lata. Silna filantropia. Wobec grup > jednostek.
    2. Fairness:    (3): umożliwić każdemu dotarcie do poziomu godnego życia a potem umożliwić by szedł dalej. Dzielić się owocami pracy, ALE optymalizować najlepszych. Współpraca > jednostka.
    3. Loyalty:     (3): wobec terragenów, przeciwko anomaliom. Lojalność nie tylko wobec swoich ale też wobec TERRAGENÓW w świetle magii i anomalii. I wobec ideologii Prawdy i Nauki.
    4. Authority:   (1): w większości hierarchie kłamią. Są takie sensowne oparte o audytowalne systemy z konkretnym celem. Te są wartościowe. Ale musisz móc wszystkie złamać.
    5. Sanctity:    (2): wszystko jest możliwe do zrozumienia i kontrolowania przez ludzki umysł. "Magia" to tylko fizyka której jeszcze nie rozumiemy. Nauka jest kluczem.
    6. Liberty:     (3): prawo do prawdy i wolności myśli, prawo do godnego (acz ubogiego) życia, prawo do równych szans na starcie, prawo do zmarnowania wszystkiego, prawo do samoobrony 
3. Osie
    1. Archy - Kratos:                  anti archy, pro kratos          (zniszczyć durne hierarchie i kłamiące systemy; można użyć siły by pokazać innym Prawdę)
    2. Personal - Economic Freedoms:    high personal, high economic    (prawa do badań, zarabiania i biznesu. Mało regulacji na firmy - zaufanie, że moralność klarkartian zwycięży)
    3. high/low trust:                  high trust w swoich, low trust w obcych
4. What do they value the most
    1. zrozumienie rzeczywistości i kontrolowanie rzeczy anomalnych. Wszystko jest narzędziem w oczach ludzkiego umysłu.
    2. optymizm, racjonalność, konstrukcja -> wszystko da się rozwiązać używając nauki, wszystko uda nam się osiągnąć razem
    3. prawda jest wartością samą w sobie.
    4. pomoc innym ludziom, grupom oraz jednostkom.
5. Ingroup - Outgroup
    1. IN: firma, korporacja, organizacja celowa. Grupa, do której należysz. Plus inni z Twej kultury.
    2. OUT: Terrageny spoza Twojej kultury i spoza Twojej organizacji.
    3. ENEMY: ci, którzy eksploatują innych albo posiadają niewolników.
6. Organization
    1. structure: zbiór luźnych firm i organizacji w świecie o regułach zapewniających takim bytom działanie
    2. allocation: zgodnie z kompetencjami i możliwościami LUB zgodnie z hierarchią firmy
    3. archy / rank: zgodnie z hierarchią firmy LUB zgodnie z kompetencjami
    4. political power: demokracja / voluntary association
    5. rozwiązywanie konfliktów: metoda naukowa, analiza > hierarchia w której jesteś > siła osobista LUB zmiana firmy
    6. spirituality: "religia nauki". Wszystko da się zrozumieć, skwantyfikować, zbadać i zamknąć w narzędzia. 
7. Symbolism and Aesthetics
    1. megastruktury. Złożone systemy. Doskonalsze narzędzia. Rzeczy, które pokazują potęgę ludzkości i inspirują.
    2. temat: magia jako narzędzie w rękach człowieka, świetna rasa ludzka, propaganda pokazująca równość terragenów.
8. Taboo
    1. wiara w "magię", "bóstwa" i rzeczy nadprzyrodzone. To tylko aspekty naukowe których jeszcze nie rozumiemy. Fanatyzm wszelkiego rodzaju.
    2. systemy autoreplikujące, plagi, plagi memetyczne - za dużo w przeszłości noktiańskiej było potencjalnych autodestrukcji i Wielkich Filtrów.
    3. ukrywanie prawdy, by czyjeś interesy wzmacniać albo by ukryć niewygodne fakty o rzeczywistości. Prawda jest wartością samą w sobie.
    4. niewolnictwo, zarówno ludzi jak i TAI. Klarkartianie jak większość noktian traktuje 'SI' (Syntetyczne Inteligencje) na równi z sobą. Klarkartianie szczególnie się boją zniewolenia myśli.
    5. eksperymenty na ludziach i eksploatacja ludzi. Niegodne wykorzystywanie innych.

### 1.3. Wspólna historia i doświadczenia, wspólne problemy i traumy -> adaptacje

* Klarkartianie pochodzą z sektorów kontrolowanych przez Noctis.
* Gdy rzeczywistość pękła, w światach Noctis zapanował poważny problem - magia istnieje, ale magia jest niemożliwa. To zbudowało dominantę - "magia nie istnieje". Jednak wykrycie przez Noctis obecności Saitaera w sektorze astoriańskich i próby badania konceptu Saitaera sprawiły, że zaczęły dziać się rzeczy niemożliwe i po raz pierwszy od dawna Noctis zaczęło rozumieć ideę magii.
* Klarkartianie potraktowali to jako fakt - istnieje magia, a przynajmniej coś co DZIAŁA jak magia, ale ona trzyma się pewnych prawideł. To znaczy, że to jest element fizyki, coś, czego wcześniej nie znano. Prawa fizyki nie są w stanie się zmienić. I wtedy odkryto, że Noctis wykorzystuje amnestyki celowe by nie mieć wiedzy o magii. Że kiedyś rzeczywistość wyglądała inaczej.
    * --> niskie zaufanie do autorytetów i do establishmentu. Jeśli ludzie są tak delikatni, że trzeba przed nimi ukrywać prawdę, są niegodni.
    * --> silne skupienie na prawdzie i metodzie naukowej - wszystko da się zrozumieć, wszystko da się rozwiązać, wszystko wykorzystać jako narzędzia.
* By dojść do prawdy i móc rozwiązać te wszystkie problemy klarkartianie pomagali sobie wzajemnie. Tylko wspólne umysły, wspomagane używając wolnych TAI mogły dojść do prawdy o magii i o Saitaerze. Silna współpraca naukowa doprowadziła do BŁĘDNEGO modelu magii, ale takiego, który odpowiadał na część pytań.
    * --> współpraca w oparciu o więzi międzyludzkie
    * --> duma z osiągnięć technicznych i dowód na to, że może istnieć lepsze jutro sformułowało cały nurt noktiański; wpłynął też na klarkartian
    * --> wszystko da się rozwiązać używając technologii i niezłomnej ludzkiej natury
* Odkrycie Saitaera i nowy model magiczny jednak destabilizował tkankę Noctis. W światach noktiańskich nie doszło do jawnej wojny domowej, ale były starcia i opory. Klarkartianie weszli w sojusz i zaczęli działać razem - nie można zakopać prawdy a oni MAJĄ RACJĘ. Ich model LEPIEJ tłumaczy rzeczywistość. W ten sposób klarkartianie zgromadzili sporą populację światów noktiańskich; wszystkich tych, dla których obecność magii jest nowym parametrem a świat FAKTYCZNIE zmienił się podczas Pęknięcia. Nie da się jednak pokonać pojedynczymi jednostkami floty; to sprawia, że klarkartianie zaczęli łączyć się w grupy.
    * --> czysty indywidualizm nie działa. Nie masz surowców ani zasobów. Co więcej, też z perspektywy ekonomicznej ludzie giną a nie powinni. Stąd budowanie grup i organizacji.
    * --> dlaczego ludzie umierają, skoro nasz poziom technologiczny pozwala im na to by przetrwali? Nie mamy niewolnictwa, mamy dość zasobów. Konieczność działań charytatywnych.
    * --> by bronić prawdy i możliwości klarkartianie akceptują użycie broni i walkę. Nie będą znowu zniewoleni.
* Klarkartianie w większości znaleźli się po stronie przegranych w Wielkiej Wojnie Noktiańskiej. Mieli do wyboru - amnestyki lub opuszczenie światów Noctis. Większość opuściła światy Noctis tylko po to, by wpakować się w problemy na Astorii. W świetle problemów na linii Saitaer - Noctis - Astoria doszło do ostrej walki z Orbiterem którą noktianie przegrali
    * --> możesz mieć rację a jednak przegrać
    * --> ludzkość ma różne formy działania i współpracy. Jeśli nie podniesie się CAŁEJ ludzkości, nie uda się naprawić świata
* Noktianie (czyli też klarkartianie) są traktowani jak podludzie. Część zniewolona, część nie ma praw. Nie mówiąc już o BIA i TAI.
    * --> jeszcze wyższa nieufność wobec innych i jeszcze niższe skłonności do Authority.
    * --> jeszcze wyższa skłonność do militaryzmu
    * --> jeszcze większa skłonność do pomocy innym (a zwłaszcza swoim), ale też nie zdradzanie własnych poglądów i przekonań

### 1.4. Główne stereotypy i powiedzonka

* "Idź do przodu, nie patrz za siebie"
* "Wszystko da się rozwiązać używając logicznego podejścia i technologii"
* "Jeśli sami się nie zniszczymy, jesteśmy skazani na sukces"
* "Prawda jest wartością samą w sobie; lepsza smutna prawda niż pozornie szczęśliwe życie w kłamstwie"
* S: Nierealistyczni, nie patrzą na to co jest możliwe, żyją z głową w chmurach
* S: Traktują swoje TAI jak przyjaciół, ale powiedz tylko o Seilii i zjeżają się jak koty...
* S: Ta ich niesamowita młodzieńczość sprawia, że już czuję się stary. Strasznie to irytujące, takie bezpodstawne przekonanie że mają rację.

### 1.5. Pytania dodatkowe

1. What is one major stereotype about your culture that you want to dispel right now?

"Prędzej rozwalą pół planety niż zamieszkają koło nieznanej okresowej efemerydy"? Na pewno nie zniszczymy POŁOWY PLANETY - nasze eksperymenty i działania są zawsze bezpieczne; akurat unikamy robienia krzywdy innym bardziej niż próbujemy 'wysadzać planety'. A jak chodzi o okresowe efemerydy? Okresowość oznacza, że jest tam jakaś regularność -> więc da się do tego dojść, zbadać i okiełznać. A jak nie wiesz, co ta efemeryda robi i czemu się pojawia... nie sądzisz że to bardziej niebezpieczne niż jej ujarzmienie jako źródło energii? Popatrz co już ludzkość zrobiła! Taka efemeryda to pikuś.

2. What are the things that you consider acceptable that may not be accepted in other cultures?

* Bioinżynieria, konstrukcja TAI itp. Uplifting, BO MOŻEMY. Traktowanie wszystkich Terragenów "po równo".
* Odrzucanie wszelkiej metafizyki i religii i przechodzenie w tryb smerfa Ważniaka i próba zracjonalizowania tego technologią.
* Można posunąć się za daleko - to straty konieczne. Kto nie próbuje, nie wygrywa.
* Irytująca naiwność wobec ludzkiej natury - zakładają, że ludzie są fundamentalnie dobrzy.

3. What is one characteristic that describes your culture as a whole?

Poszukiwacze prawdy, którzy próbują zostawić świat lepszymi niż zastali i próbują ujarzmić magię jak technologię.

5. Who do you consider to be your family? How FAMILY looks like (one home, head of family, responsibilities)

Niskie więzi rodzinne, raczej luźna asocjacja pod kątem zainteresowań i tego jak możemy sobie pomóc.

6. What links your culture / group together

Wspólne umiłowanie do prawdy i dalszego rozwoju i ewolucji ludzkiej cywilizacji. Fakt, że chcą współpracować ze wszystkimi terragenami i chcą by wszyscy terrageni szli do przodu (nietypowe w sektorze astoriańskim). Niechęć do niewolnictwa i 'dolna granica' pomocy każdemu. I bardzo nietypowa na sektor astoriański niewiara w magię XD.

7. When do you consider a person to be an adult?

Wiek biologiczny - 20+.

8. What are the traits that are considered to be positive?

Kreatywność, optymistyczne podejście, energia i chęć do działania

9. Who do you think are the heroes of society?

dziennikarz, naukowiec, szef firmy

10. What are the cultural values that children are exposed to immediately?
11. What kinds of work are considered the be prestigious?
12. What are the normal food products that are eaten?
13. Are there some colors / symbols that are being used for certain events?
14. Are there some beliefs that are associated with XXX
15. How will you describe a typical day in your life?




