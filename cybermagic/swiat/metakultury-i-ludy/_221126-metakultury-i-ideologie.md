---
layout: mechanika
title: "Notatka konstrukcji świata, 221126 - metakultury i ideologie"
---

# {{ page.title }}

## Autorzy

* Autor: Żółw
* Projektanci: Żółw

## 1. Purpose

Ten dokument ma pokazać jak konstruować grupy i jak na to patrzeć

## 2. Definicje i na czym pracujemy

* Metakultura / lud
    * DEFINICJA: cechy wspólne dla grupy ludzi działających w tej samej przestrzeni, o tych samych doświadczeniach itp.
    * stereotypy: "Niemcy nie mają poczucia humoru i są punktualni", "Japończycy są honorowi i mają kij w tyłku"
    * wspólna historia i doświadczenia: "wszyscy byliśmy pod panowaniem rzymskim", "wielki głód"
    * wspólne problemy: "żyjemy w cieniu Rosji która chce nas zniszczyć", "nie ma wody na pustyni -> zakaz zatruwania wody"
    * wspólna przestrzeń: dziki zachód / seria małych wysp / pustynia
    * wspólna kultura: inni dookoła tak robią, więc ja też --> w odpowiedzi na wszystko powyższe
* Ideologia
    * DEFINICJA: wspólne wierzenia co do tego gdzie rzeczywistość ma iść i czym jest "dobre życie"
    * co optymalizują
    * przed czym się chronią
    * utopia: jak wygląda świat idealny jak wygra
    * przeciwnik / dystopia: co jest anatemą, potworem i dlaczego; przeciwko czemu ideologia jest skierowana 
    * hierarchia / governance: "im bardziej pokrzywdzony tym wyżej" / "im bardziej uznany przez Hierarchię tym wyżej"
    * transgresje: jakie czyny wypychają jednostkę poza grupę społeczną, co jest "grzechem"
    * wybaczanie / zadośćuczynienie: jak jednostka może się zrehabilitować
    * przykazania: "dekalog"
    * mechanizmy kary: w jaki sposób jest akceptowalne karanie członków ideologii
    * czym jest raj:
    * co jest dla nich święte / ważne?
    * na jaką grupę patrzą w ich 'sphere of influence'?
* Frakcja
    * wpływ przestrzeni i unikalnych doświadczeń grupy
    * synteza metakultury i ideologii -> potem unikalne doświadczenia grupy
* Jednostka
    * unikalny zbiór wartości PONAD wartości frakcji
    * własna ideologia, alternatywna lub w kontrze do frakcyjnej 

## 3. Linki

* https://en.wikipedia.org/wiki/Nolan_Chart -> oś 'freedom'
    * **oś: personal - economic freedoms**
    * **authoritarianism**: low personal freedom, low economic freedom
        * use of strong central power to preserve the political status quo, and reductions in the rule of law, separation of powers, and democratic voting
        * rejection of political plurality
        * Minimal political mobilization, Ill-defined executive powers, often vague and shifting extends the power of the executive
        * Highly concentrated and centralized government power maintained by political repression and the exclusion of potential challengers. It uses political parties and mass organizations to mobilize people around the goals of the regime
    * **USA liberal left**: low economic freedom, high personal freedom.
    * **USA conservative right**: high economic freedom, low personal freedom.
    * **USA libertarian**: high economic freedom, high personal freedom.
* https://en.wikipedia.org/wiki/Brian_Patrick_Mitchell -> oś w kategorii USA dla prawicy
    * **oś: archy - kratos**
        * archy: (defined as the recognition of rank)
        * kratos: (defined as the use of force)
    * **communitarian**: ambivalent toward archy, pro kratos
        * central concepts: Civil society, Political particularism (my group, not everyone), Positive rights, Social capital, Value pluralism
        * reaction to excessive individualism, understood as an undue emphasis on individual rights, leading people to become selfish or egocentric
        * person's social identity and personality are largely molded by community relationships
        * Although the community might be a family, communitarianism usually is understood, in the wider, philosophical sense, as a collection of interactions
    * **progressive**: anti archy, pro kratos (democratic progressivism)
        * USUALLY:  it is possible through political action for human societies to improve over time. As a political movement, progressivism purports to advance the human condition through social reform based on advancements
        * began as an intellectual rebellion against the political philosophy of Constitutionalism[26] as expressed by John Locke and the founders of the American Republic, whereby the authority of government depends on observing limitations on its just powers.
    * **radical**: anti archy, ambivalent toward kratos
        * intent to transform or replace the fundamental principles of a society or political system, often through social change, structural change
    * **individualist**: anti archy, anti kratos (libertarian individualism)
        * promote the exercise of one's goals and desires 
        * value independence and self-reliance
        * advocate that interests of the individual should achieve precedence over the state or a social group 
        * oppose external interference upon one's own interests by society or institutions such as the government
    * **paleolibertarian**: ambivalent toward archy, anti kratos
        * Anti-imperialism, Civil libertarianism, Decentralization, Economic freedom, Free market, Free migration, Free trade, Free will, Homestead principle Individuality, Limited government, Localism, Marriage privatization, Non-aggression principle, Non-interventionism Polycentric law, Private defense agency, Private property, Public choice theory, Rugged Individualism, Self-ownership, Single tax, Spontaneous order, Stateless society, Voluntary association, Voluntary society
        * developed in opposition to the link between social avant-garde and libertarianism as if they were indivisible issues (...) charged mainstream libertarians with "hatred of Western culture". He argued that "pornographic photography, 'free'-thinking, chaotic painting, (...) and modernist films have nothing in common with the libertarian political agenda—no matter how much individual libertarians may revel in them
    * **paleoconservative**: pro archy, anti kratos (republican constitutionalism)
        * paleoconservatives press for restrictions on immigration, a rollback of multicultural programs and large-scale demographic change, the decentralization of federal policy, the restoration of controls upon free trade, a greater emphasis upon economic nationalism and non-interventionism in the conduct of American foreign policy
        * tend to oppose abortion, gay marriage, and LGBTQ rights.
        * believe that tradition is a form of reason, rather than a competing force
        * ethic is based in a culture of families, linked by friendship, common enemies, and common projects
    * **theoconservative**: pro archy, ambivalent toward kratos
        * Complementarianism (different roles men/women), Cultural heritage, Culture of life, Familialism, Family values, Natural law, Natural order, Public morality, Tradition, Traditional authority
        * Christian political factions characterized by their strong support of socially conservative and traditionalist policies
    * **neoconservative**: pro archy, pro kratos (plutocratic nationalism)
        * typically advocate the promotion of democracy and interventionism in international affairs, including peace through strength, and are known for espousing disdain for communism and political radicalism
        * endorsed the civil rights movement, racial integration and Martin Luther King Jr.[20] From the 1950s to the 1960s, liberals generally endorsed military action in order to prevent a communist victory
        * places emphasis on traditional power structures over social pluralism. They organize in favor of duty, traditional values and social institutions
* http://www.atlas101.ca/pm/concepts/haidts-6-innate-moral-foundations/  (Haidt, "Righteous Mind")
    * **Care / Harm**: raise children and keep them from harm
        * virtues of kindness, gentleness, and nurturance
    * **Fairness / Cheating**: work with others for mutual benefit | equity, proportionality, tit-for-that :: reciprocity
        * justice, rights, and autonomy
    * **Loyalty / betrayal**: my group; adaptive challenge of forming cohesive coalitions
        * virtues of patriotism and self-sacrifice for the group
    * **Authority / subversion**: respect hierarchies, social rules, maintain order and justice
        * virtues of leadership and followership, including deference to legitimate authority and respect for traditions.
    * **Sanctity / degradation**: avoid parasites and diseases, coordinate stronger in group
        * striving to live in an elevated, less carnal, more noble way
    * **Liberty / oppression**: from government / from oppression / to pursue self-defined happiness :: domination / tyranny
        * reactance and resentment people feel toward those who dominate them and restrict their liberty
