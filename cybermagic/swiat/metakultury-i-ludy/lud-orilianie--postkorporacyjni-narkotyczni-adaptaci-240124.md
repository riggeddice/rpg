## 1. Lud: Orilianie
### 1.1. Notka

* Jak o nich mówimy: orilianin, orilianka, orilianie
* Klasa: saityci, klanowcy, proterrageni, uniwersaliści, hedoniści

### 1.2. Fiszka

1. Metapsychology (tenets and values of psyche, art, culture)
    1. Hedonism, Humility -> Zostać zaadaptowanym do pełnienia potrzebnej roli w społeczeństwie i czuć maksymalną radość z tego przez adaptację.
    2. Kolektywna Kontrola -> Jeżeli grupa czegoś potrzebuje, jednostka zostanie tam umieszczona. Jeśli pozyska się człowieka, należy on do grupy.
    3. Efektywność nad Etykę -> Jeśli wszystko to sygnały biochemiczne, dla osiągnięcia celu można wszystko. Grunt, by druga strona TEŻ została szczęśliwa.
    4. Ekspansywna Adaptacja -> Potrzebna jest maksymalizacja wykorzystania środków i dostępu do środków. W nowym świecie używamy wszystkich technik.
    5. Absolutna Równość -> Jesteśmy umieszczeni w odpowiednich miejscach zgodnie z adaptacją i skillsetem. Hierarchie, pochodzenie... nic nie ma znaczenia.
2. Sześć fundamentów wartości (w skali [0, 5]; suma 15)
    1. Care:        (2): szczęście i radość dla wszystkich, każdego umieścić tam gdzie jego miejsce i pełna radość. Poza tym - człowiek jest (szczęśliwym) zasobem
    2. Fairness:    (1): życie daje nieuczciwe karty, ale da się każdego prawidłowo umieścić i zaadaptować - czy tego chce czy nie; autonomia nie istnieje
    3. Loyalty:     (4): jedyne co ma znaczenie - Twoja grupa, wspólne działanie, kohezja społeczna.
    4. Authority:   (3): każdego należy umieścić tam gdzie czerpie maksymalną radość i jest najbardziej użyteczny. Błędy w tym wymiarze to niestabilność hormonalna i błądy systemu.
    5. Sanctity:    (4): radość, szczęście, pracowitość i człowieczeństwo jako perfekcja. Człowiek oraz Grupa są czymś WIĘKSZYM i należy to optymalizować.
    6. Liberty:     (1): każdy ma jedno miejsce gdzie się przyda lub gdzie się da kogoś umocować. Ta osoba winna być tam umieszczona.
3. Osie
    1. Archy - Kratos:                  anti archy, pro kratos        (usprawnimy społeczeństwa ignorując istniejące hierarchie siłą i radością)
    2. Personal - Economic Freedoms:    low personal, high economic   (nawet jeśli trzeba zniewolić ludzi ekonomicznie, System działa najlepiej przy wysokiej efektywności i radości)
    3. high/low trust:                  low trust                     (każdy wykorzysta każdego i tak być powinno; grunt, by ludzie byli szczęśliwi)
4. What do they value the most
    1. radość i szczęście dla każdego człowieka; będziesz szczęśliwy robiąc to gdzie jesteś potrzebny
    2. perfekcyjna adaptacja człowieka do systemu; będziesz maksymalnie efektywny
    3. umieszczenie każdego terragena i każdej anomalii w perfekcyjnym systemie; pokonamy Entropię pracą
5. Ingroup - Outgroup
    1. IN: racjonaliści skupiający się na efektywności; ci, którzy poszerzają możliwości Ludzkości
    2. OUT: zwolennicy 'głupich ideologii'; ekspansja, adaptacja, efektywność i (przymusowa) radość są jedyną opcją Ludzkości.
    3. ENEMY: osoby chętne do marnotrawienia surowców by mieć władzę nad innymi. Przecież można chemią.
6. Organization
    1. structure: możliwe do sterowania grupy / klany, działające jako grupy celowe
    2. allocation: zgodnie z umiejętnościami plus adaptacja do maksymalnego szczęścia wykonując pracę. Praca to hobby.
    3. archy / rank: brak; każdy jest umieszczony zgodnie z potrzebami grupy
    4. political power: odpowiednio umieszczone osoby w odpowiednich miejscach
    5. rozwiązywanie konfliktów: odwołanie się do dobra grupy > odwołanie się do Sędziów > opuszczenie grupy
    6. spirituality: każdy ma swoje miejsce w Wielkiej Maszynie, szczęście z bycia oddanym pracy jest cnotą każdej istoty
7. Symbolism and Aesthetics
    1. sztuka perswazyjna: demonstracja osobom spoza kultury jak wygląda pełna radość z bycia częścią Wielkiej Maszyny, czy przez narkotyki, virt czy symulacje.
    2. bardzo duże skupienie na doświadczeniach immersyjnych, pracowitości i różnych formach przeżywania dumy, radości i szczęścia. 
    3. codzienne narzędzia, sprzęt i elementy są dobrze wykonane i piękne. Rzemieślnicy są dumni ze swych dzieł. Efektywność nie oznacza brzydoty.
    4. nawet sztuka pełni rolę, tu: wzmacnianie kohezji społecznej oraz pokazywanie innych form działań
8. Taboo
    1. Czyn indywidualistyczny, odrzucający efektywność oraz powodujący marnotrawstwo zasobów
    2. Nieakceptowanie wyznaczonej roli oraz odrywanie innych od wyznaczonych ról
    3. Obniżanie poziomu szczęścia innej osoby (chyba, że potem się go podniesie)
    4. Skupienie się na hierarchiach innych niż czysta 'merytokracja oparta na adaptacji'. 
    5. Bezsensowny wandalizm i destrukcja.

### 1.3. Wspólna historia i doświadczenia, wspólne problemy i traumy -> adaptacje

Jeszcze przed Pęknięciem Rzeczywistości istniały grupy i firmy, które skupiały się na maksymalizacji szczęścia swojej populacji. „Zadowolony pracownik pracuje lepiej”. Wprowadzenie odpowiednich wszczepów oraz wspomaganie biochemicznych pozwalało zapewnić pracownikowi odpowiednie zadowolenie.

Po Pęknięciu Rzeczywistości okazało się, że szczęśliwe grupy ludzi które chcą działać na rzecz ludzkości są całkiem dobrze zaadaptowane do nowej sytuacji. Bez większego problemu zaadaptowali się do pojawienia się magii i stwierdzili, że ekspansja, nauka, maksymalizacja wykorzystania nowego świata (w tym magii i anomalii) oraz znalezienie właściwego miejsca zarówno dla Bytów pochodnych ludziom jak i nowych, pochodnych magii jest ludzką odpowiedzialnością.

Bardzo optymistyczna metakultura, wierzą, że przez maksymalizację wykorzystania wszystkiego i maksymalizację radości wszystkich istot są w stanie pokonać Entropię.

Kolektywnie uważają indywidualizm za archaizm. Widząc nieszczęście ludzi dookoła niech rozumieją czemu tamci koniecznie pragną zachować to co mają. Uważają rajdy i dostosowanie innych ludzi do swojej metakultury za odpowiednik ratowania tonących którzy nie wiedzą, że toną.

Szczególnie rozpowszechnieni w okolicy Syndykatu Aureliona.

### 1.4. Główne stereotypy i powiedzonka

* "Praca to jest hobby, hobby to jest praca"; jeśli nad czymś pracujesz, powinno się Cię Zaadaptować by to było Twe hobby. Inaczej to nieludzkie.
* "Kto się cieszy, pracuje lepiej"

### 1.5. Pytania dodatkowe

1. What is one major stereotype about your culture that you want to dispel right now?

?

2. What are the things that you consider acceptable that may not be accepted in other cultures?

?

3. What is one characteristic that describes your culture as a whole?

?

5. Who do you consider to be your family? How FAMILY looks like (one home, head of family, responsibilities)

?

6. What links your culture / group together

?

7. When do you consider a person to be an adult?

?

8. What are the traits that are considered to be positive?

?

9. Who do you think are the heroes of society?

?

10. What are the cultural values that children are exposed to immediately?
11. What kinds of work are considered the be prestigious?
12. What are the normal food products that are eaten?
13. Are there some colors / symbols that are being used for certain events?
14. Are there some beliefs that are associated with XXX
15. How will you describe a typical day in your life?




