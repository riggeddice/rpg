## 1. Lud: Kordylici
### 1.1. Notka

* Jak o nich mówimy: kordylita, kordylitka, kordylici
* Klasa: gaianie, biofile, heterogeniczni

### 1.2. Fiszka

1. Metapsychology (tenets and values of psyche, art, culture)
    1. Hedonism, Security -> musimy ciężko pracować, ale potem musimy się też bawić. Praca służy życiu, nie na odwrót
    2. Stabilność -> Niezależnie od okoliczności, świat JAKOŚ się toczy. JAKOŚ to jest. COŚ się dzieje i będzie działo.
    3. Prawo do zmiany -> Eksperymentuj, baw się, próbuj, szukaj swojej drogi, potykaj się i upadaj. Nieefektywność i próbowanie to cechy ludzkie.
    4. Odpoczynek -> Ludzkość może ma inną sytuację niż kiedyś, ale jest prawo do odpoczynku, lenistwa, sztuki i wszystkiego czemu warto żyć. Więcej niż praca i zadania.
2. Sześć fundamentów wartości (w skali [0, 5]; suma 15)
    1. Care:        (4): z uwagi na to jak wygląda rzeczywistość, pomóż innym i daj wiele szans
    2. Fairness:    (1): życie nie jest sprawiedliwe i nie jest tak, że prawidłowo dostajemy to co powinniśmy. Oszukiwanie innych jest akceptowalne.
    3. Loyalty:     (3): jeśli masz swoich ludzi i swoją grupę, należy się jej trzymać i wspierać. W końcu jest to to co masz i co jest ważne
    4. Authority:   (2): każdy musi znaleźć własną drogę, acz jak kogoś już szanujesz to warto rozważyć co mówi
    5. Sanctity:    (2): człowiek to nie 'maszyna z mięsa' a Energie są dowodem na istnienie czegoś więcej. Należy prawidłowo traktować zasady i rytuały nawet jak ich nie rozumiemy
    6. Liberty:     (3): bez możliwości eksperymentowania i robienia błędów nie ma możliwości eksplorowania i szukania czegoś więcej
3. Osie
    1. Archy - Kratos:                  pro archy, pro kratos                   (da się usprawnić społeczeństwa przez prawidłowe użycie siły i prawidłowe wykorzystywanie hierarchii, zwłaszcza wśród zasłużonych)
    2. Personal - Economic Freedoms:    high personal, low economic             (każdy powinien mieć możliwości i szerokość działania, ale nie można pozwolić na monopol i redukcję szerokości)
    3. high/low trust:                  low trust                               (każdy mimo wszystko dba o siebie, więc nie można liczyć na to, że Cię nie oszuka)
4. What do they value the most
    1. Znalezienie własnej drogi, zrobienie czegoś odmiennego, nowy sprawny _strain_ w istniejącej rzeczywistości
    2. Zaradność w różnych, nieoczekiwanych sytuacjach
5. Ingroup - Outgroup
    1. IN: Ty > Rodzina > Poszerzona Rodzina == Wybrana Organizacja > Firma > Inni
    2. OUT: osoby które są ZBYT POWAŻNE i chcą 'żyć dla pracy / zasad', zbyt skrajni indywidualiści
    3. ENEMY: wszyscy, co próbują ograniczać możliwości i zamykać świat w niezmiennych kajdanach i strukturach, wszyscy którzy próbują zniszczyć 'wszystko czemu warto żyć'
6. Organization
    1. structure: macierzowa; z jednej stronie Poszerzona Rodzina, z drugiej Wybrana Organizacja. Każdy należy do jakiegoś ekosystemu. Wybraną Organizację da się zmienić, Rodzinę też.
    2. allocation: zgodnie z kompetencjami i możliwościami LUB zgodnie z hierarchią firmy
    3. archy / rank: zgodnie z hierarchią firmy LUB zgodnie z kompetencjami
    4. political power: demokracja / voluntary association
    5. rozwiązywanie konfliktów: kontrakt > hierarchia > siła / oddalenie się
    6. spirituality: bardzo indywidualistyczne; zwykle powiązane ze sztuką, Energią czy dowolną religią. Jeden z głównych obszarów różnorodności.
7. Symbolism and Aesthetics
    1. Festiwale, tańce, zabawy grupowe -> grupowe budowanie linków, zabawy intensywne i pełne radości
    2. 'Marnotrawstwo energii' -> rzeczy efektowne, efemeryczne i skupione na doświadczeniach i czymś co można opisywać 'byłem wtedy gdy...'
    3. Motywy kontrastowe, pokazujące balans egzystencji
8. Taboo
    1. Pójście za dominującym działaniem, nie próbując znaleźć własnej drogi.
    2. "Kopanie leżącego", nadużywanie możliwości wobec słabszych od siebie, niewolenie innych.
    2. Eliminowanie 'czasu dla siebie' i odpoczynku. Usuwanie tego balansu w życiu.

### 1.3. Wspólna historia i doświadczenia, wspólne problemy i traumy -> adaptacje

* Dominujące grupy i kultury astoriańskie pochodzące od pierwszych kolonistów.
    * A dokładniej od statku Kordylius, skąd nazwa 'Kordylita'
    * Część z nich do dzisiaj nazywa Astorię 'Kordylią Trzy'

?

### 1.4. Główne stereotypy i powiedzonka

?

### 1.5. Pytania dodatkowe

1. What is one major stereotype about your culture that you want to dispel right now?

?

2. What are the things that you consider acceptable that may not be accepted in other cultures?

?

3. What is one characteristic that describes your culture as a whole?

?

5. Who do you consider to be your family? How FAMILY looks like (one home, head of family, responsibilities)

?

6. What links your culture / group together

?

7. When do you consider a person to be an adult?

?

8. What are the traits that are considered to be positive?
9. Who do you think are the heroes of society?
10. What are the cultural values that children are exposed to immediately?
11. What kinds of work are considered the be prestigious?
12. What are the normal food products that are eaten?
13. Are there some colors / symbols that are being used for certain events?
14. Are there some beliefs that are associated with XXX
15. How will you describe a typical day in your life?




