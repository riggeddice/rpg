## 1. Lud: XXXXXXXXXXXXXXXXXXXX
### 1.1. Notka

* Jak o nich mówimy: on, ona, oni
* Klasa: 

### 1.2. Fiszka

1. Metapsychology (tenets and values of psyche, art, culture)
    1. VALUES
    2. 
    3. 
    4. 
2. Sześć fundamentów wartości (w skali [0, 5]; suma 15)
    1. Care:        (x|y): 
    2. Fairness:    (x): 
    3. Loyalty:     (): 
    4. Authority:   (): 
    5. Sanctity:    (): 
    6. Liberty:     (): 
3. Osie
    1. Archy - Kratos:                  pro archy, anti kratos          ??
    2. Personal - Economic Freedoms:    low personal, high economic     ??
    3. high/low trust:                  high trust internal, low trust outgroup     ??
4. What do they value the most
    1. 
5. Ingroup - Outgroup
    1. IN: 
    2. OUT: 
    3. ENEMY: 
6. Organization
    1. structure: 
    2. allocation: 
    3. archy / rank: 
    4. political power: 
    5. rozwiązywanie konfliktów: 
    6. spirituality: 
7. Symbolism and Aesthetics
    1. 
    2. 
    3. 
8. Taboo
    1. 
    2. 

### 1.3. Wspólna historia i doświadczenia, wspólne problemy i traumy -> adaptacje

* Gdy rzeczywistość pękła: xxx
    * -> ADAPTACJA

### 1.4. Główne stereotypy i powiedzonka

* .

### 1.5. Pytania dodatkowe

1. What is one major stereotype about your culture that you want to dispel right now?

2. What are the things that you consider acceptable that may not be accepted in other cultures?

3. What is one characteristic that describes your culture as a whole?

5. Who do you consider to be your family? How FAMILY looks like (one home, head of family, responsibilities)

6. What links your culture / group together

7. When do you consider a person to be an adult?
8. What are the traits that are considered to be positive?
9. Who do you think are the heroes of society?
10. What are the cultural values that children are exposed to immediately?
11. What kinds of work are considered the be prestigious?
12. What are the normal food products that are eaten?
13. Are there some colors / symbols that are being used for certain events?
14. Are there some beliefs that are associated with XXX
15. How will you describe a typical day in your life?




