# Materiałoznawstwo magitech

## 1. Opis kategorii


## 2. Jakie nietypowe stopy i minerały mamy

### 2.1. Irianium

Irianium jest jakimś stopem litu. Baterie. Wysoka gęstość energetyczna.

### 2.2. Astinian

Pełni rolę komputronium. Materia o bardzo wysokim potencjale tego typu. Rzadki, występuje w okolicach Asimear.

### 2.3. Morfelin

Silnie adaptowalna materia pod kątem konstrukcji i kształtowania z możliwością zmiany swojej struktury. Stopy morfelinu są wykorzystywane m.in. w fabrykatorach a morfistal jest pancerzem statku kosmicznego.

### 2.4. Kryształy ixiackie / Ixiat

Powiązane z ixionem, kryształy Przeniesienia. Pozwalają na syntezę rzeczy niemożliwych. Niezbędne do budowania TAI 3 stopnia.
