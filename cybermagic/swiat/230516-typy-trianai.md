# Rody Aurum

## 1. Trianai - whatis


Bioformy Trianai:

* Ainshker - swarm hunter
* Tentalid - corruptor
* Klarvath - antitank
* Klarvachnid - immobilizing trapper
* Ursator - ciężka jednostka przełamania
* Skorpivath - szturmowa jednostka kwasowa
* Skorpilit - szturmowa jednostka elektryczna
* Siraiada - lurer of fools, mind attacker; syrena + driada
* Drolmot - lieutenant, fungus-claimed by trianai
* Amalgamoid - commander, adapter

## 2. Konkretne bioformy

### 2.1. KODOWANIE

C (ciało), U (umysł / intelekt / psychotronika), E (emocje / morale / kontrola magiczna), S (społeczne)

### 2.2. Ainshker

* .Ainshker
    * akcje: "jestem nieruchomy i niewidoczny", "atakuję z zaskoczenia", "szybko szarżuję", "przesuwam się w niemożliwe miejsce", "przesuwam się na plecy wroga", "atakuję słaby punkt"
    * siły: "niezwykle szybki", "wszędzie wlezie", "przyczepny jak bluszcz", "szpony i zęby"
    * defensywy: "niezwykle szybki", "nieruchomy jest niewidoczny", "gdzie jeden tam wiele", "wizualny kameleon"
    * słabości: "uderzenie nieprzeciwpancerne", "wrażliwy na eksplozje"
    * zachowania: "strike & flee", "swarm"

Opis: podstawowa bioforma trianai, wczesny hunter & gatherer. Poluje na nie-ciężko-opancerzone ofiary i próbuje je skutecznie usunąć. Wilkokształtne z aspektami roślinnymi

### 2.3. Tentalid

* .Tentalid
    * akcje: "jestem nieruchomy i niewidoczny", "atakuję z zaskoczenia", "oplatam mackami", "wstrzykuję Ziarno", "rozdzieram mackami / wtargnięcie"
    * siły: "ogromna siła i przyczepność", "ogromna odporność na magię i ataki mentalne"
    * defensywy: "elastyczne ciało pochłania uderzenia", "odporny na mały kaliber"
    * słabości: "uderzenie nieprzeciwpancerne", "niezbyt szybki", "wrażliwy na elektryczność"
    * zachowania: "atak z zaskoczenia", "w ukryciu jak długo się da", "łap ofiarę i ją wciągnij"

Opis: ośmiornicopodobna istota wstrzykująca Ziarno w ofiarę, przekształcając ją w agenta Trianai. Pasożytnicza istota wykorzystująca agenty biologiczne.

### 2.4. Klarvath, Klarvachnid

* .Klarvath
    * akcje: "rozpuszczam pancerz śluzem", "pluję śluzem", "eksploduję żrącymi substancjami", "śluz zastyga i unieruchamia"
    * siły: "silny i żrący śluz", "atak przeciwpancerny", "śluz zastyga i unieruchamia"
    * defensywy: "ekstremalna odporność toksyczna"
    * słabości: "wrażliwy na przebicie", "wrażliwy na wysokie temperatury", "wrażliwy na silne wibracje", "dość wolny", "wszystko jest łatwopalne i wybucha"
    * zachowania: "polowanie z ukrycia", "wykorzystanie śluzu do pułapek", "unieruchamianie i rozpuszczanie ofiar", "artyleria"
* .Klarvachnid
    * akcje: "wstrzykuję śluz", "rozkładam sieć", "punktowo niszczę pancerz śluzem"
    * siły: "silny i żrący śluz", "niezwykle wytrzymała sieć", "wszędzie wlezie", "przyczepny jak bluszcz"
    * defensywy: "ekstremalna odporność toksyczna", "szybka zmiana środka masy", "nieruchomy jest niewidoczny"
    * słabości: "wrażliwy na wysokie temperatury", "wszystko jest łatwopalne i wybucha"
    * zachowania: "rozpinanie pułapek", "unieruchamianie ofiar"

Opis:

Klarvath to larwopodobna istota zdolna do opluwania celu śluzem - albo zabijając na miejscu żrącym śluzem, albo paraliżując i unieruchamiając śluzem zastygającym.
Klarvachnid natopiast wykorzystuje pajęcze aspekty by unieruchamiać servary.
Oba służą do tego, by rozpuszczać biomasę celu i zanieść ją tam gdzie jest potrzebna. Klarvath jest "przydatniejszy", Klarvarachnid jest po to by odcinać fragmenty terenu.

### 2.5. Ursator

* .Ursator
    * akcje: "rozrywam na kawałki", "szarżuję i dewastuję"
    * siły: "upiorny pęd", "upiornie silny", "biega 30 km/h", "berserker"
    * defensywy: "redundancja organów", "berserker", "pancerz deflekcyjny"
    * słabości: "krótki czas działania"
    * zachowania: "dewastator"

Opis:

Ursator to dewastujący trianai przełamania, zdolny do przerzucenia 500 kg (a w szale berserkerskim więcej). Krzyżówka niedźwiedzia z pancernikiem z niezwykle ostrymi szponami, metalizowany (przez). Raz uruchomiony ursator jest strasznie trudny do zatrzymania.

### 2.6. Skorpivath, Skorpilit

* .Skorpivath
    * akcje: "szybki atak szczypcami", "unieruchomienie szczypcami", "przebijające uderzenie ogonem", "bardzo daleko skaczę"
    * siły: "skoczny jak cholera", "silne szczypce", "ogon-metalowy-przebijak", "korodujący śluz", "eksploduje pod wpływem ognia"
    * defensywy: "metalizowany pancerz", "zdolność do szybkiego przemieszczania się", "świetna orientacja powietrzna"
    * słabości: "wrażliwy na ogień", "krótki czas działania", "wrażliwe czułki"
    * zachowania: "atak z zaskoczenia", "szybkie i bezlitosne ataki"
* .Skorpilit
    * akcje: "szybki atak szczypcami", "unieruchomienie szczypcami", "przebijające uderzenie ogonem", "bardzo daleko skaczę", "emituję łuk elektryczny"
    * siły: "skoczny jak cholera", "silne szczypce", "ogon-metalowy-przebijak", "emisja wyładowań elektrycznych"
    * defensywy: "metalizowany pancerz", "zdolność do szybkiego przemieszczania się", "świetna orientacja powietrzna"
    * słabości: "krótki czas działania", "wrażliwe czułki"
    * zachowania: "atak z zaskoczenia", "szybkie i bezlitosne ataki"

Opis:

Bipedalny skorpionoid będący odpowiedzią trianai na Lancery. Szybkie, opancerzone, ze strasznymi szczypcami. 

### 2.7. Siraiada

* .Siraiada
    * akcje: "przyzywam mentalnie do zguby", "fałszuję Twoją rzeczywistość", "spełniam Twoje marzenia", "oplatam Cię i wchłonę", "zmieniam emocje Trianai", "bezpośrednio atakuję szokiem emocjonalnym"
    * siły: "halucynacje i emisje emocji", "silna magicznie", "przekształca energię Pryzmatu", "uderza traumami i marzeniami"
    * defensywy: "zanurzam się w wodzie", "nie mam spójnego miejsca do trafienia", "odporność na ogień przez płyny", "porusza się powoli"
    * słabości: "wrażliwość na eksplozje i rozrywanie"
    * zachowania: "dyskretna i niewidoczna", "nie zwraca na siebie uwagi"

Opis: 

Wyjątkowo dziwny mix rośliny w stylu rosiczki z pnączokształtnym hominidem. Atakuje mentalnie, wysyłając fale przyzywające i rozrywające rzeczywistość dla ofiary.

### 2.8. Drolmot

* .Drolmot
    * akcje: 
    * siły: 
    * defensywy: 
    * słabości: 
    * zachowania: 

Opis: 

Agent Trianai, gdy Trianai ulojalnią osobę zmieniając jej impulsy (hormony, funkcja nagrody itp) grzybkiem. Prawdziwy obraz to śluzogrzyb wpełzający przez ucho.

### 2.9. Amalgamoid

* .Amalgamoid
    * akcje: "asymilacja materii", "zmiana kształtu", "rozerwanie mackami", "używam narzędzi i haseł jak moje ofiary"
    * siły: "rozpuszcza wszystko", "asymilacja pamięci i ciała", "pełna zmiennokształtność glutowata", "alien mind", "spawn secondary amalgamoids"
    * defensywy: "nieprawdopodobnie żywotny" (3), "brak obszaru trafialnego", "upiorna regeneracja", "imitacja otoczenia", "odporny na warunki ekstremalne"
    * słabości: "ogień", "czyste niszczenie biomasy", "konieczność zachowania ciągłości masy"
    * zachowania: "lure and consume"

Opis: 

Commander Trianai, pełni rolę koordynatora używając siraid. Nadal celem jest powiększanie biomasy i adaptacja trianai do potrzeb. Potrafi się rozdzielić, acz nie może nigdy spaść poniżej pewnego poziomu biomasy.
