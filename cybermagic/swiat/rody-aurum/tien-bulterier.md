## Opis Rodu

* Nazwa: Bulterier
* Serce
    * Metakultura wiodąca
        * Sybrianie + Dekadianie
            * -> S: 'Jesteśmy najlepsi, jesteśmy wybitnymi istotami legend. Niezależnie od wyzwania, poradzimy sobie ze wszystkim.'
            * -> D: 'Jesteśmy najlepiej wyszkoleni, jesteśmy honorowi i lojalni i jesteśmy ostatnimi którzy mogą zrobić to co należy.'
            * -> DS: 'Reprezentujemy najbardziej niszczycielską siłę w egzystencji kontrolowaną przez najdoskonalszych strażników Harmonii.'
        * MODYFIKACJA kulturowa
            * 'Prawdziwym wrogiem jest wróg wewnętrzny. Energia, która jest w nas. Pytanie to nie "czy możemy coś zniszczyć", ale "czy się odważymy"?'
            * 'Balans, harmonia i minimalne ruchy. W ten sposób zachowamy kontrolę w obliczu tego wszystkiego.'
    * Energia wiodąca
        * Alteris, Interis
        * -> 'jeśli znaczenia się ciągle zmieniają, Pustka nie zdąży ich pochłonąć. Jeśli Pustka pochłania znaczenia, NieRzeczywistość nie zdąży się wylać'
        * -> 'zachowywanie harmonii między _tym-co-nie-może-istnieć_ a _tym-co-zaniknęło_ sprawi, że Energie będą się zwalczać i nie zniszczą wszystkiego poza obszarem Rodu'
        * -> 'jesteśmy strażnikami negacji _tego-co-naprawdę-istnieje_, potrafimy zmienić i wymazać wszystko. Nasza magia jest prawdziwie apokaliptyczna. Czarujemy gdy MUSIMY.'
        * -> 'nie jesteśmy w stanie wygrać z własną magią; nawet nie wiemy czy powinniśmy reagować i czy czegoś nie zniszczymy. Ale ktoś musi coś zrobić i tylko my możemy.'
            * UWAGA: tu widać Skażenie sentisiecią. Tendencje do Rozpaczy oraz Zagubienia -> lepiej nie robić nic tylko przetrwać bo NIC NIE WIADOMO I WSZYSTKO MOŻNA ZNISZCZYĆ.
            * UWAGA: Bulterierzy ogólnie boją się swojej magii i rozpaczliwie balansują między Pustką (tym-czego-nie-ma) i NieRzeczywistością (tym-co-nie-powinno-być)
        * -> 'gdy już musimy zacząć walczyć, zostanie tylko jedna osoba na polu bitwy. Najlepiej, bym to był ja.'
        * -> 'Praktycznie NIC NIE CZUJĘ a jeśli już... to zwykle tego nie rozumiem, to obce uczucia. Potrzeba mi więcej stymulacji i INNEJ stymulacji niż normalnym magom.'
            * UWAGA: Bulterierzy mają podniesiony próg bólu, obniżony emocji itp. Co więcej, mają zniekształcenia przez sentisieć Alteris. To sprawia, że są dziwni i reagują dziwnie.
                * czują mniej i inaczej
        * -> 'Niszczenie przynosi ulgę i jest najdoskonalszą formą sztuki.'
        * affinity (magia bojowa, magia negacji, Alteris i Interis)
    * Wartości dominujące
        * Security, Benevolence
        * -> 'Nieważne co się stanie, obronimy WSZYSTKICH. Obronimy ludzkość. Obronimy przed magią, atakami, czymkolwiek. Mamy siłę ognia i ją wykorzystamy.'
        * -> 'Bulterier. Pies, który się nie boi. Pies, który walczy do końca. Pies, który w obronie swoich przyjaciół rzuci się na WSZYSTKO.'
        * -> 'Jestem tienem, więc jestem rycerzem ostatniej szansy. Jeśli jestem proszony o ingerencję, jest źle. Więc nie ma co się ograniczać.'
        * -> 'Jeśli działam, nie ma czegoś takiego jak "przerwijmy", więc... nie zaczynaj czego nie chcesz doprowadzić do końca.'
* Specyfika
    * Wyróżniki szczególne
        * Bulterier ma tendencję do maksymalnie niszczycielskiej magii bojowej, zwykle zniekształconej przez zarówno Alteris jak i Interis.
        * Przeciętny Bulterier mało mówi i mało ingeruje. Jest osobą raczej cichą i ostrożną w słowach i czynach, unikający konfliktów. Nie chce stracić kontroli.
        * Bulterier jest DZIWNY. Zmieniony przez Alteris (przez co jego zmysły dają dziwne efekty) oraz przez Interis (przez co czuje mniej), więc nie zawsze reaguje czy mówi jak inni magowie
        * Fizycznie, Bulterier jest zwykle wątły jak na sybrianina, ale nadal silniejszy i w lepszej formie niż przeciętny człowiek
        * Bulterier ceni sobie Harmonię i Balans bardziej niż cokolwiek innego. Często robi rzeczy 'złe', by równoważyć 'dobre' i vice versa, czego zwykle nikt nie rozumie.
    * Specyfika, jedną frazą
        * Silnie Skażeni Druidzi Negacji-Tego-Co-Jest, którzy walczą o zachowanie balansu by nie doszło do sformowania Dzikiej Sentisieci lub nie doszło do rozlania się Alteris / Interis.
    * Konflikty wewnętrzne
        * Różne interpretacje na temat tego czym jest balans, kiedy można ingerować i co należy zrobić w tej konkretnej sytuacji.
    * Konflikty zewnętrzne
        * Zła interpretacja działań ze strony Bulterierów lub innych tienów
        * Użycie nadmiernej siły ognia lub niewłaściwych działań ze strony Bulterierów
        * Coś opuściło ziemie Bulterierów lub tam weszło. Lub Bulterierzy tak uważają. Lub stało się to przez zakrzywienie przestrzeni Alteris.
    * Co powinni robić i co jest tabu
        * DOBRE: 'Zachowuj balans za wszelką cenę, nie pozwól by jakakolwiek Energia osiągnęła przewagę - nieważne jak się nie maskuje.'
        * DOBRE: 'Jeśli potrzebujesz coś zrobić by uzyskać balans, zrób to. Jest to mniejszym złem niż alternatywa.'
        * DOBRE: 'Pomóż innym jeśli jesteś w stanie, zwłaszcza jeśli jesteś w stanie coś zniszczyć i sobie ulżyć.'
        * DOBRE: 'Jeśli ktoś wkręca Cię w intrygę wbrew Twej woli, ukarz go srogo. Nie wie z jakimi siłami ma do czynienia i jak to groźne.'
        * TABU: 'Czarowanie bez powodu, używanie swoich Energii czy możliwości. Bulterier winien zachować balans i rozsądek.'
        * TABU: 'Ingerencje w działania innych, spiski, uczestniczenie w intrygach z własnej woli. Nie wiesz, czy Twoje działania czegoś nie pogrążą i nie zdestabilizują.'
* Bezpośredni sąsiedzi (aktualnie; to się zmieniało przez własności Alteris)
    * Kazitan
    * Lemurczak
    * ?

## Historia i Kontekst
### Przed Pęknięciem Rzeczywistości

* Utraciliśmy wiedzę jak wyglądał teren Bulterierów przed Pęknięciem Rzeczywistości. Interis oraz Alteris sprawiły, że nic na ten temat nie wiemy.
    * Po prawdzie, to nie ma znaczenia.

### Gdy Rzeczywistość Pękła

* Ziemie, które będą ziemiami Bulterierów stały się czymś _niewłaściwym_. Pomiędzy Alteris i Interis pojawiały się nieprawidłowe manifestacje. Ludzie zostawali Skażeni. Te ziemie promieniowały problem dalej a sentisieć próbowała pasożytniczo pochłonąć jak najwięcej istot, dołączając ich do _negacji-tego-co-jest_
    * Nawet nie potrafimy określić populacji ani typów manifestacji z tego okresu. Znaczenia się pozmieniały i zostały utracone...
    * WIEMY, że wielu magów próbowało coś z tym zrobić, ale jedynie dołączyli do legionów _tego-czego-nie-ma-i-nie-powinno-być_
* Te ziemie stanowiły skrajnie niebezpieczny Spawner, generujący problemy na przylegających terenach i powoli się rozprzestrzeniający. Energie Alteris i Interis z jakiegoś powodu były w stanie rozprzestrzeniać sentisieć też na szersze ziemie i nowo powstałe Aurum stanęło przed poważnym i niebezpiecznym problemem.
* Sowińscy i Myrczkowie zaprojektowali plan stworzenia tienów dla tej sentisieci. Wybrano ochotników, najbardziej optymistycznych i lojalnych spośród sybrian i dekadian. Przeprowadzono mnóstwo eksperymentów by sprząc ich z sentisiecią Bulterierów.
    * -> Zdecydowana większość ochotników stała się Manifestacjami
        * Utraciliśmy dużo informacji z tego okresu. Nadal mamy problem Znaczeń.
    * -> W ciągu dwóch tygodni(?) lub dwóch lat(?) udało się stworzyć pierwszych tienów rodu Bulterier
* Bulterierzy nie byli zdolni do opanowania sentisieci. Naturalna Rozpacz i Zagubienie były silniejsze od dowolnej indoktrynacji czy wsparcia. Sytuacja wyglądała beznadziejnie.
    * -> Bulterierzy zrozumieli, że nie są w stanie pokonać ani opanować własnej magii. To silniejsze od nich. Wielu popełniło samobójstwo, nie chcąc stać się zagrożeniem.
* Bulterierom udało się w końcu znaleźć rozwiązanie - obrócenie Alteris przeciwko Interis i vice versa. Ciągłe karmienie Interis _NieRzeczywistością_ i wzmacnianie _NieRzeczywistości_ by Interis nie dało rady pożreć wszystkiego. To sprawiło, że ekspansja sentisieci została zatrzymana i Bulterierzy zaczęli prawidłowo pełnić rolę Strażników
    * -> Bulterierzy zaczynają stanowić Ród. Tien Bulterier zaczyna się zmieniać fizycznie i mentalnie.
    * -> Duma i poczucie, że prawidłowo pełnią rolę tienów Aurum przeciwko temu z czym mają do czynienia. Ludzkość nadal wygrywa.
* Bulterierzy są wspierani przez całość Aurum pod kontrolą Sowińskich.

### Teraz

Ziemie Bulterierów nadają się do zamieszkania, acz są bardzo niebezpieczne i nie możesz ufać ani swoim zmysłom ani pamięci. Poza niektórymi miejscami, ziemie te się ciągle zmieniają, poszerzają i przestają istnieć. Fakt, że znajdują się tam niesamowicie cenne minerały i materiały, więc ludzie poruszają się flotami pojazdów nigdy nie zatrzymującymi się w jednym miejscu poza niektórymi osadami określonymi jako bezpieczne miejsca przez tienów.

Bulterierowie są Druidami Negacji-Tego-Co-Jest; wzmacniając Alteris i Interis w odpowiednich miejscach i odpowiedni sposób, by prądy walczących ze sobą energii nie zniszczyły ludzi i by żadna z energii nie wydostała się poza dany teren. Dzięki sentisieci stali się elementami tej dziwnej rzeczywistości, przez co 'normalny świat' nie jest dla nich do końca normalny. Ich siła ognia (magii) jest niesamowita; mają nadspodziewanie wielu szczerych wyznawców Seilii w swoich szeregach.

Jeśli Bulterierowie czegoś potrzebują, Aurum im to zapewni. Ku niechęci wielu młodych magów z innych rodów, nie rozumiejących co się tu stało.
