## Opis Rodu

* Nazwa: Diakon
* Serce
    * Metakultura wiodąca
        * Drakolici
            * -> różnorodność i dążenie do wyższej perfekcji są podstawą udanego świata i życia
            * -> działanie zgodne z bardzo autonomicznymi grupami celowymi
        * MODYFIKACJA kulturowa
            * 'w świecie magii nie da się zakładać, że kontrakt pozostaje niezmienny; słuszne działania są ważniejsze'
            * 'piękno, elegancja, natura i zmysłowość są tym co sprawia że warto żyć mimo niestabilności rzeczywistości'
            * 'jeśli będziemy piękni, mili i słodziutcy, nie będą się nas bali i stworzymy raj na ziemi'
    * Energia wiodąca
        * Ixion, Alucis
        * -> 'moralnym obowiązkiem jest ewolucja do najlepszej formy i postaci'
        * -> 'piękno jest bardzo ważne - rzeczy piękne po prostu są lepsze'
        * -> 'naszym obowiązkiem i możliwością jest tworzenie utopii - raju na ziemi dla wszystkich obecnych'
        * -> affinity (plants, plant evolution)
    * Wartości dominujące
        * Hedonism, Universalism
        * -> 'jeśli nie masz po co żyć, do czego aspirować i możliwości zakosztowania nowych doznań, Nihilus już zwyciężył'
        * -> 'jeśli nie robisz innym krzywdy, możesz robić wszystko co uważasz za słuszne'
        * -> 'krzywda jest tym co druga strona uważa za krzywdę; jeśli nie uzna tego za krzywdę, działaj'
        * -> 'nieważne czy jesteś osobą, syntem czy anomalią. Życie to życie.'
* Specyfika
    * Wyróżniki szczególne
        * Bardzo silne skupienie na morale, pięknie, hedonizmie i roślinach / naturze
        * Często uważani za nierozsądnych sybarytów, w praktyce inteligentni agenci Uśmiechu i Szczęścia
        * Imiona pochodzą od funkcji, są trzyczłonowe. Pierwsze NADAJĘ SOBIE, drugie NADAJĄ MNIE, trzecie DOPRECYZOWUJE
            * np. "Radośniczka Dewastacja Artystka Diakon"... jest dość jednoznacznym przykładem
        * Nie ma dwóch podobnych do siebie Diakonów; są to też tieni dość indywidualistyczni i rzadko działający w stałych grupach
        * Mimo uroczego i słodkiego wyglądu, DALEJ są homo superior. Są odporni na Skażenie biologiczne i trudny teren.
    * Specyfika, jedną frazą
        * "Nie wystarczy przetrwać! Bez piękna i nadziei tracimy człowieczeństwo!"
        * Skrajnie różnorodni artyści i sybaryci bez cienia moralności i umiaru, którzy pragną wszystkim poprawić humor dążąc do piękna.
    * Konflikty wewnętrzne
        * Jak traktować ludzi / tych konkretnych ludzi? Wolna wola vs Ukojenie
        * Konkretnie, w którą stronę wyewoluować ten byt / obszar? Co będzie piękniejsze?
    * Konflikty zewnętrzne
        * Szok kulturowy / skandal
        * Niewłaściwe traktowanie ludzi / vipów przez randomowego Diakona
    * Co powinni robić i co jest tabu
        * DOBRE: 'Wszystko co istnieje powinno być piękne; wprowadź artyzm i elegancję do każdej najmniejszej śrubki. Po to warto żyć.'
        * DOBRE: 'Nie wystarczy przetrwać; by pokonać mrok i wprowadzić optymizm i nadzieję musisz dać im coś PO CO warto żyć i przetrwać.'
        * DOBRE: 'Jeśli ktoś czegoś pragnie, ale protestuje - przekształć rzeczywistość tej osoby by mogła to otrzymać.'
        * DOBRE: 'Jeśli ktoś cierpi, ukoj jego cierpienie. Daj trochę radości i optymizmu w tym trudnym świecie.'
        * DOBRE: 'Szanuj piękno i naturę. Acz nie wahaj się usprawnić tego, co masz koło siebie - perfekcja jest nieosiągalna.'
        * TABU: 'pozostawienie bez pomocy kogoś kto się męczy lub nie ma niczego pięknego koło siebie'
        * TABU: 'krzywdzenie innych (gdzie krzywda definiowana jest jako ODBIÓR krzywdy przez osobę)'
* Bezpośredni sąsiedzi
    * Ród Blakenbauer
    * ?
    * ?

## Historia i Kontekst
### Przed Pęknięciem Rzeczywistości

* Ziemie które dziś należą do rodu Diakon to był park narodowy i rezerwat; mokradła oraz teren 'mało zaludniony'
* Teren Diakonów (i Blakenbauerów) był mało zaludnionym miejscem, dość nieprzystępnym i specjalnie pozostawionym jako dziki

### Gdy Rzeczywistość Pękła

* Tereny rodu Diakon stały się dzikie i bardzo niebezpieczne, zaczęły zagrażać ludziom dookoła i się 'rozlewać'
* Pierwsi magowie rodu Diakon to byli drakolici którzy chcieli opanować zagrożenie. Ku ich zdziwieniu, doszło do senti-infuzji.
    * -> tien oznacza 'niezależnie od okoliczności biorę temat na siebie, jestem homo superior'
    * -> Diakoni wyglądają słodziutko i poczciwie, ale są nieprawdopodobnie twardzi i radzą sobie ze Skażeniem biologicznym i brakami
    * -> teren rodu Diakon do dzisiaj ma miejsca bardzo niebezpieczne, ale za to jakie piękne i halucynogenne ;-)
* Z biegiem czasu coraz więcej ludzi (nie tylko drakolitów) zaczęło migrować na tereny rodu Diakon
    * -> Diakoni zawsze mają założenie 'żyj i daj żyć innym' oraz 'pomóż, ogranicz cierpienie'
* Diakoni od początku nie walczyli z terenem, próbowali dostosować siebie do niego i teren do siebie, uwypuklać różnorodność
    * -> Nawet w niebezpiecznych roślinach i koszmarach jest piękno, które warto uwypuklić
* Ludzie żyjący na terenie Diakonów często cierpieli i mieli problemy; Diakoni nie mają problemów by ich Ewoluować czy zmienić im sygnały cierpienia w sygnały szczęścia
    * -> niepotrzebe cierpienie jest grzechem

### Teraz

Włości Diakonów są jak 'teren z krainy druidów' - wyhodowane pomieszczenia, domy, różnorodne kwiaty i owady. Jest dostęp do darmowych środków kojących i sprawiających przyjemność. Różnego rodzaju substancje psychoaktywne nie tylko są dozwolone ale są też zachęcane. Jeśli ktoś cierpi, istnieje sposób ukojenia czy naprawienia tego cierpienia.

Tieni są bardzo indywidualistyczni, skupiając się na różnych aspektach swojej domeny. Jedyne co ich łączy to cel - uśmierzenie, piękno, radość, dbanie o dobrobyt ludzi i innych w swoich włościach. Z perspektywy tienów, inni ludzie w ich włościach są potencjalnymi chętnymi zasobami - a jeśli ktoś nie jest chętny, można użyć różne techniki by byli chętni (tylko w oparciu na radość i przyjemność, nigdy na cierpienie).

Wieczne upiększania, przekształcenia i budowanie na konceptach innych sprawiają, że tereny Diakonów są naprawdę interesujące i godne odwiedzenia. Acz trzeba uważać, by nie zostać na zawsze, zauroczeni pięknym miejscem.

Zwykle Diakoni udają mniej inteligentnych i mniej kompetentnych niż są. Po pierwsze, inni czują się lepiej w towarzystwie 'niezbyt rozsądnej, uroczej osoby'. Po drugie, to daje Diakonom więcej możliwości wpływu. NADAL są homo superior, o czym łatwo zapomnieć. I do tego mają magię ORAZ sentisieć.

Diakoni bardzo lubią szokować. Moda Diakonów często dotyczy nie tylko ubrań ale też różnego rodzaju przekształceń czy bodypaintingu, często opartych na motywach roślinnych.

Zwykle Diakoni wykorzystują narzędzia i stroje w oparciu o rośliny.
