# Rody Aurum

## 1. Lista rodów z krótkimi danymi

* 1. Arłacz             : Unumens, Ixion        : "perfekcyjny rój - każdy jest szczęśliwy w swoim miejscu, zaadaptowany do perfekcji"
* 2. Bankierz           : Fidetis, Esuriit      : "arystokracja rządzi się swoimi prawami"
* 3. Barandis           : Anteclis, Fidetis     : "zawsze 'jakoś było', więc szukajmy maksymalnie szeroko i róbmy swoje jak najlepiej, nie trzeba się spinać"
* 4. Blakenbauer        : Praecis, Anteclis     : "przekroczyliśmy człowieczeństwo i zbudujemy świat od zera"
* 5. Bulterier          : Interis, Alteris      : "druidzi Pustki i NieŚwiata, maksymalna siła ognia"
* 6. Burgacz            : Praecis, Sempitus     : "pragmatycznie budujemy rzeczy które przetrwają samą wieczność"
* 7. Diakon             : Ixion, Alucis         : "sama słodycz i ukojenie. Po co się nas bać jak możesz kochać?"
* 8. Gwozdnik           : Unumens, Fidetis      : "pomocni strażnicy, militarystyczni filantropowie ostatniej szansy"
* 9. Kazitan            : Fidetis, Interis      : "liście na wietrze przeznaczenia, pionki w rękach większych sił"
* 10. Keksik            : Alucis, Exemplis      : "artyści skupieni na reputacji, nieustraszeni"
* 11. Lemurczak         : Alteris, Esuriit      : "silni pożerają słabych; zmienimy świat na godny naszej siły"
* 12. Maus              : Unumens, Sempitus     : "jednostka nieistotna, grupa przetrwa"
* 13. Myrczek           : Alucis, Esuriit       : "nieżyciowi obsesyjni marzyciele dążący do JEDNEGO sukcesu"
* 14. Mysiokornik       : Ixion, Sempitus       : "jak karaluchy - niezniszczalni i wszechobecni"
* 15. Perikas           : Unumens, Exemplis     : "magia to tylko narzędzie. Magia dynamiczna zostanie usunięta."
* 16. Samszar           : Alucis, Alteris       : "nowy świat wymaga harmonii pomiędzy ludzkością duchami i tym, co zastaliśmy"
* 17. Sowiński          : Anteclis, Sempitus    : "przetrwamy działając szybciej, mądrzej i ekstensywniej. Wielość strainów > jednostka."
* 18. Sylver (upadły)   : Interis, Esuriit      : "wszystko stanie się nami. WSZYSTKO nami. ZAWSZE nami. NIGDY nami."
* 19. Terienak          : Praecis, Interis      : "technologią zbudujemy lepszy świat. Co się nie podda kontroli, zniszczymy."
* 20. Tessalon          : Exemplis, Alteris     : "nie ma granic, wszystkie przekroczymy - i zrozumiemy niezrozumiałe"
* 21. Torszecki         : Ixion, Alteris        : "wszystko przekształcimy, czego się nie da - dostosujemy się do tego"
* 22. Ursus             : Anteclis, Alucis      : "rzeczywistość jest wtórna; virt, memetyka i konstrukty mentalne"
* 23. Verlen            : Praecis, Exemplis     : "Im większe kły ma niedźwiedź, tym większa blizna i większa chwała"


## 2. Macierz Energia - Energia

            | Praecis  | Anteclis    | Unumens  |  Ixion   | Sempitus    | Fidetis  | Exemplis |  Alucis  | Interis  |  Alteris  | Esuriit   |
-----------------------------------------------------------------------------------------------------------------------------------------
Praecis     |  -XXX-   | Blakenbauer |          |          | Burgacz     |          | Verlen   |          | Terienak |           |           |
Anteclis    |  -XXX-   |  -XXX-      |          |          | Sowiński    | Barandis |          | Ursus    |          |           |           |
Unumens     |  -XXX-   |  -XXX-      |  -XXX-   | Arłacz   | Maus        | Gwozdnik | Perikas  |          |          |           |           |
Ixion       |  -XXX-   |  -XXX-      |  -XXX-   |  -XXX-   | Mysiokornik |          |          | Diakon   |          | Torszecki |           |
Sempitus    |  -XXX-   |  -XXX-      |  -XXX-   |  -XXX-   |  -XXX-      |          |          |          |          |           |           |
Fidetis     |  -XXX-   |  -XXX-      |  -XXX-   |  -XXX-   |  -XXX-      |  -XXX-   |          |          | Kazitan  |           | Bankierz  |
Exemplis    |  -XXX-   |  -XXX-      |  -XXX-   |  -XXX-   |  -XXX-      |  -XXX-   |  -XXX-   | Keksik   |          | Tessalon  |           |
Alucis      |  -XXX-   |  -XXX-      |  -XXX-   |  -XXX-   |  -XXX-      |  -XXX-   |  -XXX-   |  -XXX-   |          | Samszar   | Myrczek   |
Interis     |  -XXX-   |  -XXX-      |  -XXX-   |  -XXX-   |  -XXX-      |  -XXX-   |  -XXX-   |  -XXX-   |  -XXX-   | Bulterier | Sylver    |
Alteris     |  -XXX-   |  -XXX-      |  -XXX-   |  -XXX-   |  -XXX-      |  -XXX-   |  -XXX-   |  -XXX-   |  -XXX-   |  -XXX-    | Lemurczak |
Esuriit     |  -XXX-   |  -XXX-      |  -XXX-   |  -XXX-   |  -XXX-      |  -XXX-   |  -XXX-   |  -XXX-   |  -XXX-   |  -XXX-    |  -XXX-    |

========================

## 3. Dokładniejszy opis rodów
### 2.11. Lemurczak

* metakultura: różnorodne 
* modyfikacja kulturowa: "silni pożerają słabych", "nikt Ci nigdy nie pomoże"
* specjalizacja: czarny ród specjalizujący się w niewolnictwie i twistowaniu innych do swojej woli

Przed Pęknięciem w Aurum działały elementy Syndykatu Aureliona (jako mafia). Udało im się sprząc z wierchuszką pewnego terenu. Przywrócili dyskretnie niewolnictwo i doprowadzili do struktury typu "klasa panów" i "plebs". I wiele osób z owego "plebsu" znalazło się między młotem (system prawny, wierchuszka) i kowadłem (mafia, Syndykat).

A sentisieć sprzęgła się z osobami o najsilniejszych uczucia i największy resentyment - w różnorodnych niewolników.

W ten sposób powstał ród Lemurczak - ród zrodzony z resentymentów oraz chęci zemsty.

* KONFLIKTY WEWNĘTRZNE: Lemurczaki współpracują, ale każdy próbuje być tym "największym", nie mają wiele wspólnego. Nie pomagają sobie aktywnie, acz połączą siły przeciw każdemu z zewnątrz.
* KONFLIKTY ZEWNĘTRZNE: Lemurczaki to drapieżny ród działający zgodnie z zasadą "might makes right". Bardzo nielubiani.

### 2.15. Samszar

* metakultura: różnorodne, z dominantą faerilów (uduchowienie, długoterminowe podejście, kastowość)
* modyfikacja kulturowa: "harmonia nad technologię i grupowość", "piękno ma duże znaczenie"
* specjalizacja: ród specjalizujący się w sferze duchów i harmonii z naturą, ogólnie druidzi / szamani

Cierpliwość, piękno i dążenie do harmonicznej konstrukcji lepszej rzeczywistości. Poszanowanie wszelkiego życia, historii i chęć posunięcia się trochę na bok. Unikanie zbędnych hierarchii. To są naturalne cechy rodu Samszar, które jedynie zostały wzmocnione przez Pęknięcie Rzeczywistości.

Samszarowie specjalizują się w niższym poziomie ingerencji i w koegzystencji w harmonii ze światem, czy to magiczno-anomalnym czy naturalnym. Dbają o sferę astralną, duchy, dobrą atmosferę i ekologię. Ich tereny maksymalizują 'sustainability'.

* KONFLIKTY WEWNĘTRZNE: Brak silnych struktur i za duże przywiązanie do harmonii, tradycji i 'nie wchodzenia innym w szkodę' powoduje dużo wyższe napięcia wewnętrzne niż się wydaje. Każda rodzina arystokratyczna sama sobie sterem, żeglarzem i okrętem.
* KONFLIKTY ZEWNĘTRZNE: Samszarowie chcą się adaptować. Wiele rodów chce podbić lub okiełznać rzeczywistość. Samszarowie mają cierpliwość i działają 


