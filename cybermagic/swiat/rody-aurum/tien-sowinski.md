## Opis Rodu

* Nazwa: Sowiński
* Serce
    * Metakultura wiodąca
        * Kordylici + Faerile
            * -> K: 'Kluczem jest znalezienie własnej drogi w ciągle zmieniającym się świecie; bez eksperymentowania i ryzyka nie ma lepszego jutra.'
            * -> K: 'Sama praca bez zabawy i eksperymentów nie ma sensu. Musimy móc eksperymentować i się bawić - bez tego nie znajdziemy najlepszych pomysłów.'
            * -> F: 'Każdy ma swoje miejsce w egzystencji. Naszym miejscem, jak się okazało, jest rządzenie i doprowadzenie Ludzkości do nowego, lepszego jutra.'
            * -> KF: 'Jeżeli masz możliwość zrobienia tego co należy i masz ku temu możliwości i kompetencje i tego NIE zrobisz, nie jesteś godny lepszego jutra.'
        * MODYFIKACJA kulturowa
            * 'Duchowość jest kluczowa, ale inaczej niż ONI myśleli, bo to my stanowimy "boską arystokrację". Jesteśmy predestynowani by przeprowadzić wszystkich do Lepszego Jutra.'
    * Energia wiodąca
        * Anteclysm, Sempitus
        * -> 'Ktoś musi zapewnić nieśmiertelność ludzkości i ideałom Aurum. To jesteśmy my. Jednostki umierają na ołtarzu Ludzkości, ale Ludzkość przetrwa.'
        * -> 'Musimy umożliwić jednostkom na robienie różnych rzeczy nawet jak przegrają, a Sowińscy zaadaptują się do tego co działa najlepiej i jest potwierdzone.'
        * -> 'Nieskończona ekspansja we wszystkich możliwych kierunkach. W ten sposób znajdziemy rozwiązanie wszystkich problemów.'
        * -> 'Musimy promować WSZYSTKIE możliwe nurty i ścieżki które nie są terminalne. Musimy być panami własnego losu.'
        * -> 'Technologia, wiedza, zasoby wymagają reinwestowania. Budujemy na tym co mieliśmy. Majątek który leży - nie pracuje'
    * Wartości dominujące
        * Power, Achievement
        * -> 'Naszym miejscem w egzystencji jest doprowadzenie do zjednoczenia Ludzkości i wspólne przeprowadzenie Ludzkości do Lepszego Jutra.'
        * -> 'Każdy Sowiński winien dążyć do własnej ścieżki znalezienia Lepszego Jutra. Większość zawiedzie. Jednemu się uda.'
        * -> 'Sowińscy powinni być jak najlepsi - gdyby nasi poprzednicy nie byli dość dobrzy, sentisieć by ich nie wybrała.' 
        * -> 'Nikt nie może nas ograniczyć, nikt nie może nas uwięzić. 
* Specyfika
    * Wyróżniki szczególne
        * Doskonali administratorzy, biurokraci, naukowcy i eksploratorzy - nexus kontroli i wiedzy; całość tego wszystkiego wraca do Rodu do Wielkich Bibliotek.
        * Nie boją się bardzo interesujących, zaawansowanych i niebezpiecznych eksperymentów. Nie boją się szkód i cierpienia swoich podwładnych czy członków Rodu.
        * Mają swoją wizję Zjednoczonej Ludzkości i konsekwentnie do niej dążą wszelkimi metodami - od przekupstwa, przez magię i eksperymenty aż do zabójstw
        * Bardzo bogaci i wpływowi, można powiedzieć 'szare eminencje Aurum'. To oni stworzyli Aurum a potem Zjednoczenie Letejskie.
    * Specyfika, jedną frazą
        * Bogaci unifikatorzy Ludzkości wierzący w to, że to ich przeznaczenie by Ludzkość się zjednoczyła pod sztandarem Sowińskich
    * Konflikty wewnętrzne
        * Starcia pomiędzy Sowińskimi o zasoby, honor czy 'kto ma rację' są dość częste - jak Ci zależy, bronisz swego zdania. Jak nie, nie powinieneś go trzymać.
        * Podejście 'pojedynczy Sowiński nie ma znaczenie, liczy się wynik' kontra nowsze 'jestem arystokratą, jaki sens nie korzystać?'
    * Konflikty zewnętrzne
        * Sowiński WIE, że to właśnie ON doprowadzi Ludzkość do Zjednoczenia. Ta arogancja i szarogęsienie STRASZNIE irytuje.
        * Sowińscy w służbie Zjednoczenia Ludzkości są skłonni posunąć się bardzo daleko, robiąc rzeczy nieakceptowalne.
        * Sowiński może wesprzeć innych Sowińskich, zwłaszcza w sytuacjach skrajnych.
        * Sowiński działa, jeśli uważa że powinien zadziałać i nikt na jego miejscu niczego nie zrobi lepiej (lub nie robi).
        * Zdaniem Sowińskiego, cel uświęca środki.
    * Co powinni robić i co jest tabu
        * DOBRE: 'Cel uświęca środki, ale patrz też na efekty uboczne. Zjednoczenie Ludzkości jest niemożliwe, jeśli wszyscy nas nienawidzą. Jeśli jednak nikt nie widzi złych czynów i są one konieczne...'
        * DOBRE: 'Buduj współpracę pomiędzy różnymi ludźmi i frakcjami. Współpraca jest ważniejsza niż godność jednej osoby czy zachowywanie sekretów.'
        * DOBRE: 'Dziel się tym co wiesz i w czym możesz pomóc, ale jeśli ktoś uzna Twój prymat. Jesteś Sowińskim. Choć handel by uzyskać coś czego chcesz jest więcej niż OK'
        * DOBRE: 'Eksperymentuj, szukaj swojej drogi, przegrywaj i przyjmuj pomoc. Jesteś tylko ziarnkiem piasku w większym ekosystemie.'
        * TABU: 'Bezsensowne nadużywanie władzy i pomiatanie innymi. W ten sposób nie zjednoczymy Ludzkości.'
        * TABU: 'Ograniczanie mocy, możliwości i wpływów Sowińskich. Sprawianie, by Sowińscy byli uznawani za bardzo szkodliwych czy problematycznych.'
* Bezpośredni sąsiedzi
    * Gwozdnik
    * ?
    * ?

## Historia i Kontekst
### Przed Pęknięciem Rzeczywistości

* Teren Sowińskich był bogatym terenem, zarówno pod kątem finansowym jak i pod kątem industrialno-technologicznym
    * Zaludnione ziemie, bogatsze niż przeciętnie, o bardzo rozbudowanej sieci nanomaszyn i dość silnych mechanizmach kontroli i monitorowania
* Ci, którzy stali się Sowińskimi byli administratorami i inżynierami. Normalna 'klasa administracyjna / pracownicza', nie najbogatsi ale skuteczni

### Gdy Rzeczywistość Pękła

* Sentisieć wyrwała się spod kontroli, ale Sowińscy się nie przestraszyli. Uznali to za poważny zbiór awarii, coś katastrofalnego ale normalnego
    * ...i magia odpowiedziała.
    * Połączenie biurokracji, zasad, pewności siebie, stosunkowo łagodnego Pęknięcia i procedur sprawiły, że teren nie ucierpiał aż tak jak 'powinien'
    * -> Nagle Sowińscy zostali tienami. Przyjęli nazwę Rodu 'Sowiński', od 'Sowy', symbolu ich ochronnych nanitek i ich ogólnej mądrości
* Bardzo szybko Sowińscy przejęli władzę; z jednej strony nikt nie był w stanie ich powstrzymać, z drugiej strony potężna seria awarii, problemów i manifestacji zarówno na ich terenach jak i poza nimi wymagała, by ktoś się tym zajął. I Sowińscy się tym zajęli.
    * -> Sowińscy przyjęli rolę tienów; nie wiedząc do końca _czym mogą być_ poszli w arystokrację starej daty. I tak zostało XD.
    * -> Dzięki rzutkości Sowińskich, ich umiejętnościom adaptacji, procedurom i zimnej krwi udało im się zredukować zniszczenia i utrzymać teren
        * TAK, umiejętności administracji, biurokracji i zarządzania są tu kluczem.
* Sowińscy bardzo szybko zaczęli pomagać innym terenom i łączyć inne Rody pod egidę 'Aurum', czyli Zjednoczenia Rodów. Wprowadzili pojęcie Arystokracji oraz pokazali jak tego używać i co ogólnie może to dać tienom. Wprowadzili zasady, prawa i pokazali tienom jak czerpać określone przywileje. Potem podzielili się surowcami i skupili na współpracy pod kątem nowego lepszego świata.
    * -> Inne rody Aurum uznały prymat Sowińskich, przynajmniej pozornie
    * -> Szybko Sowińscy mieli przewagę i trudno było się z nimi nie połączyć
* To Sowińscy doprowadzili do powstania Sojuszu Letejskiego i Aurum.
* Sowińscy do dzisiaj zbierają technologię, wiedzę itp.

### Teraz

Ziemie Sowińskich są futurystycznymi zaawansowanymi terenami, pełnymi festiwali, zaawansowanych fabrykatorów, technologii itp. Sowińscy do dzisiaj trzymają się zasad naukowych i wysokich technologii. Ich eksploratorzy wielkim kosztem personalnym pozyskują wiedzę o rzeczach nieznanych innym rodom. Ich zaawansowane programy (m.in. Program Kosmiczny Aurum) z rozmachem prowadzą do wyższych poziomów technologicznych niż kiedykolwiek. Sowińscy dzielą się owocami swoich działań i pracy - czemu nie - jeśli, oczywiście, uznasz ich prymat.

'Tien Sowiński' to wielka odpowiedzialność. Nie wobec poszczególnych ludzi, a wobec ogólnie rozumianej Ludzkości. Twoim obowiązkiem jest robić coś, co zwiększy szansę na Zjednoczenie Ludzkości i jeszcze silniejsze zwiększenie możliwości Sowińskich, Aurum, Astorii i Całej Ludzkości (w tej kolejności). Ile jajek trzeba rozbić by zrobić omlet, to jest wtórne. Im mniej tym lepiej, ale.

Sowińscy rządzą Aurum - jako administratorzy, wsparcie prawne, najbogatsi i najlepsi managerowie - a Aurum rządzi Sojuszem Letejskim. Zwykle tego nie nadużywają i większość wiedzy, zasobów itp. reinwestują w jeszcze większe i ambitniejsze programy oraz bardziej zaawansowane mechanizmy.

Bardzo trudno zrozumieć działanie Sowińskich jeśli nie weźmie się pod uwagę podejścia metakultury faerilów - _indywidualnie żaden Sowiński nie ma znaczenia_. Liczy się wynik działań _Rodu Sowińskich i ludzi współpracujących_. Acz to się też rozmyło z biegiem czasu.
