## Opis Rodu

* Nazwa: Gwozdnik
* Serce
    * Metakultura wiodąca: 
        * Dekadianie > Klarkartianie
            * -> D: 'jeśli jesteś w stanie pomóc swoim, powinieneś im pomóc; każdy musi mieć umiejętności samoobrony'
            * -> K: 'to co robimy ma budować lepszy świat dla wszystkich; nie skupiajmy się tylko na bezpieczeństwie ale też na dobrobycie'
        * MODYFIKACJA kulturowa: 
            * 'przez magię każdy ma swoje miejsce w Rodzie Gwozdnik; tien będzie wyżej niż mag czy człowiek'
    * Energia wiodąca: Fidetis, Unumens
        * -> 'tien to zaszczyt oraz obowiązek'
        * -> 'robiąc to co należy zbudujemy bezpieczny i silny świat z supremacją ludzkości nad magią'
        * -> 'działając zgodnie z zasadami i ku dobru wspólnemu '
    * Wartości dominujące
        * Universalism, Benevolence
        * -> 'nieważne kim jesteś, ważne, że pełnisz odpowiednią rolę'
    * Specyfika, jedną frazą
        * 'militaryści współpracujący z Orbiterem, trzymający się swoich i robiący dobrą robotę'
* Specyfika
    * Wyróżniki szczególne
        * kultura raczej militarystyczna, dość filantropijna ('wędka nie ryba')
        * duty and cooperation; każdy raczej ma swoje miejsce
        * silne skupienie na przydatności, pragmatyzmie i walce z anomaliami dla ochrony ludzi
        * raczej trzymają się razem, niska ekspansja, brak chwalenia się
        * silna współpraca z Orbiterem i innymi, w walce z anomaliami i by chronić ludzi
        * "Śmierć jest naszą przyjaciółką. Dzięki niej nie skończymy Skażeni i Splugawieni"
    * Konflikty wewnętrzne
        * starcia o współdzielone zasoby w ramach różnych hierarchii - wszyscy chcą 'dobrze', każdy inaczej to interpretuje
        * silna hierarchia rodzin i konieczność działania razem vs umiejętności naturalne poszczególnych ludzi
        * stosunek do magii: narzędzie czy zagrożenie? Jak bardzo można bezpiecznie tego używać?
        * 'jesteś tienem, jesteś winny ludziom ochrony i musisz walczyć z anomaliami'
        * dekadiańska 'ochrona, bezpieczeństwo, poświęcenie' vs klarkartiańska 'szybka budowa lepszego świata dla wszystkich'
    * Konflikty zewnętrzne
        * wszystko co zagraża Aurum lub porządkowi
        * wszystko co zagraża ich ludziom i ich cywilizacji
    * Co powinni robić i co jest tabu
        * DOBRE: filantropia i pomoc innym zgodnie ze swoimi możliwościami
        * DOBRE: służba w wojsku, zwalczanie anomalii, należenie do silnej struktury
        * TABU: ucieczka przed poświęceniem i wykonywaniem właściwych działań
        * TABU: robienie rzeczy 'niewartych robienia', 'nieważnych'
        * TABU: rozczarowanie Rodu i Rodziców
        * TABU: wspieranie Skażenia i Skażanie się
* Bezpośredni sąsiedzi
    * Sowińscy
    * ?
    * ?

## Historia i Kontekst
### Przed Pęknięciem Rzeczywistości

* Ludy astoriańskie nabierające metakultury klarkartiańskiej i niewielka ilość dekadiańskiej.
* Teren 'zrównoważony', świetnie zintegrowany kulturowo. "Nasi astoriańscy noktianie". Astorianie i noktianie się wymieszali.
* Wysoka technologia, podejście "pomóżmy innym", ogólnie dobra współpraca i niezły dobrobyt danego terenu.

### Gdy Rzeczywistość Pękła

* Gdy Rzeczywistość pękła i magia się wylała na tereny Gwozdników, ludzie stanęli razem walcząc przeciwko temu co się działo. Nieważne - mag, człowiek, biznesmen czy żołnierz. 
    * Ta jedność charakterystyczna dla kultury dekadiańskiej i te poświęcenia - silniejszy ratował wielu słabszych, strażak oddawał życie za osoby chronione - ukształtowały pierwotne spojrzenie na Ród Gwozdnik. Sentisieć skupiła się na dekadianach, mimo, że ich było mniej.
        * -> W odróżnieniu od wielu innych Rodów, Gwozdnikowie uważają, że bycie tienem to zaszczyt i odpowiedzialność. 
        * -> To nadaje im konkretną rolę i są zobowiązani ją godnie pełnić. I - tak - poświęcić się dla ludzi, jeśli będzie to potrzebne.
* Z biegiem czasu ludzie zaczęli przejmować bardziej dekadiańską kulturę, dodając do niej aspekty klarkartiańskiej
    * Pierwsze spory na linii 'nie idziecie tak daleko jak powinniście' vs 'za dużo walki, za mało jutra'

### Teraz

Gwozdnikowie nie traktują magii jako coś, co 'jest i było zawsze'. Nadal trzymają się podejścia, że ludzka wola i technologia są ważniejsze. Jednocześnie magia stanowi kolejne narzędzie do którego trzeba się zaadaptować i które należy wykorzystać. Bardziej klarkartiańskie grupki nadal odrzucają magię, preferując używanie przede wszystkim technologię i technomagię nad czystą energię. To sprawia, że często jedynym magiem który się na danym terenie pojawia jest... tien.

Dekadiańska część kultury przesuwa tienów w rolę strażników. Bardzo industrialna i militarystyczna kultura, pierwsza lub ostatnia linia obrony, wspierana przez żołnierzy. Tienowie rządzą terenem Gwozdników, ale słuchają ekspertów.

Klarkartiańska część kultury przesuwa tienów w rolę filantropów. "Wędka a nie ryba", współpracując z ekspertami, dbając nie tylko o bezpieczeństwo ale też o 'wartość i jakość życia'.

