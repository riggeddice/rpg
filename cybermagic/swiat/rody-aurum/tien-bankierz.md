## Opis Rodu

* Nazwa: Bankierz
* Serce
    * Metakultura wiodąca
        * Sybrianie + Faerile + Kordylici (ci nie wpłynęli na kulturę)
            * -> S: 'Żyj intensywnie, śmiej się i walcz. Masz potencjał tworzyć własną legendę, więc twórz ją.'
            * -> F: 'Technologia, bogactwo, środki i magia są ważne. Więcej niż tylko ludzka wola, ciała i mięśnie.'
            * -> SF: 'Zasady NASZE są ważne, ICH nie są ważne. Wywalczasz dla swojej grupy miejsce w egzystencji.'
        * MODYFIKACJA kulturowa
            * 'Z uwagi na Esuriit istnieje zbiór zasad których MUSIMY się trzymać. Zasady Bankierzy. Te i tylko te są nienaruszalne.'
    * Energia wiodąca
        * Fidetis, Esuriit
        * -> 'niepohamowana ambicja, głód bycia najlepszym i posiadania jak najwięcej - ale walczymy z tym by zmienić świat na lepsze'
        * -> 'każdy Bankierz ma potencjał apokaliptycznego zniszczenia i nieskończonej nienawiści. Ale to tylko potencjał. Jesteśmy LEPSI.'
        * -> 'Bankierz potrafi pracować, śmiać się, kochać i walczyć MOCNIEJ niż ktoś inny. Ale umiar jest trudny do złapania.'
        * -> 'reguły, hierarchie, ścisłe zasady, rytuały - to oddziela nas od zwierząt i oddala od nas Esuriit'
        * -> 'My się poświęcamy kontrolując nasze żądze i nasze przeznaczenie by INNYM było lepiej. Dlatego ONI są nam winni.'
    * Wartości dominujące
        * Power, Benevolence
        * -> 'Najlepsi indywidualnie, na każdym polu: czy to militarnym czy to pojedynkowym czy to gromadzenia bogactwa.'
        * -> 'Ponieważ możemy być najlepsi i możemy osiągnąć co chcemy, możemy wykazać się wielkodusznością, mecenatem i zmienić świat na lepszy'
        * -> 'Najlepszym ustrojem politycznym jest arystokracja - możemy więcej, jesteśmy lepsi, wiemy jak ich pokierować by było im lepiej.'
        * -> 'Masz wielką moc. Nie krzywdź innych, skanalizuj ją by zostawić po sobie pomnik. Bankierz winien być niebiański i szanowany.'
* Specyfika
    * Wyróżniki szczególne
        * Nienaturalne i krótkotrwałe akty niezwykłych umiejętności i działań zasilanych czystą energią Ambicji i Głodu.
        * Najlepsi pojedynkowicze w Aurum, też 'najbardziej elegancka i trzymająca się zasad arystokracja Aurum'
        * Bankierze są bardzo agresywni i ambitni. Sybriański aspekt zasilany Esuriit. Ale Bankierz nie wbije Ci noża w plecy.
            * -> Jeśli Bankierz jest Twoim wrogiem, wiesz o tym.
            * -> Bankierz jest honorowy i oczekuje honorowego zachowania. Jeśli go nie wykażesz, odpowie odpowiednio.
        * Zwykle młodzi Bankierze ukierunkowują energię w działania bezpośrednie i pojedynki, starsi akumulują surowce i możliwości.
        * Bankierz uważa, że Bankierzowi wolno więcej.
    * Specyfika, jedną frazą
        * Bogata i agresywna Arystokracja, pragnąca władzy i pozostawienia po sobie pomnika.
    * Konflikty wewnętrzne
        * Pojedynki między Bankierzami zdarzają się BARDZO często.
        * Echo Wielkiego Czyszczenia, podczas którego wielu niewinnych Bankierzy ucierpiało i potraciło rodziny.
    * Konflikty zewnętrzne
        * Jeżeli Bankierz czegoś pragnie, ma prawo to zabrać jeśli może to utrzymać. To mocno konfliktuje Bankierzy z innymi rodami.
            * inni arystokraci Aurum nie są dla Bankierzy 'potencjalnymi ofiarami', ale nie-arystokracja-Aurum (np. ludzie) to fair game. 
        * Honor i zasady Bankierzy.
        * Wielkie Plany Bankierzy o pozyskanie władzy, zasobów itp.
        * vs Sowiński: 'ci szczęśliwcy zajęli naszą pozycję i nasze prawowite miejsce'
        * vs Lemurczak: 'ten plebs zniszczył lokalną arystokrację, część naszych nawet i oddał się plugawemu mrokowi i Esuriit'
    * Co powinni robić i co jest tabu
        * DOBRE: 'Jeżeli czegoś pragniesz, weź to i zrób to. Jesteś arystokratą. By "ich" chronić, cały czas więzisz Esuriit'
        * DOBRE: 'Trzymaj się zasad i reguł swojego Rodu. Niekoniecznie innych - inni nie rozumieją co robisz.'
        * DOBRE: 'Jesteś tienem - wykazuj się wielkodusznością, pomagaj innym, ale też żądaj szacunku i pokłonu.'
        * DOBRE: 'Każdy może upaść. Bądź silny, zdecydowany, bezlitosny. Więc nie odchodź od reguł bo zostaniesz celem.'
        * DOBRE: 'Bankierz zachowuje kontrolę nad sobą niezależnie od okoliczności. Elegancja i kontrola.'
        * TABU: 'Uleganie swoim mrocznym instynktom, m.in. Esuriit; ogólnie, wykorzystywaie Esuriit.'
        * TABU: 'Uleganie nienawiści poza ściśle wyznaczonymi zasadami i kanałami; stąd nacisk na pojedynki.'
        * TABU: 'Odmawianie pojedynku w sytuacji w której powinien ów pojedynek mieć miejsce (honor, emocje, zasoby)'
        * TABU: 'Postęp i bogactwo muszą rosnąć - więc nie wolno Ci niszczyć dóbr, dewastować rzeczy wartościowych i pięknych. Ulegasz Esuriit?'
* Bezpośredni sąsiedzi
    * Lemurczak (te same latyfundia, ale gorsze rodziny)
    * Myrczek (niższy poziom jakości terenu, nie ma już latyfundiów, ten sam model)
    * ?

## Historia i Kontekst
### Przed Pęknięciem Rzeczywistości

* Teren Bankierzy był bogatym terenem, ośrodkiem centralnym ponad teren Sowińskich (myśl: Mazowieckie > Małopolskie)
    * Teren ten jest industrialny, rolniczy, zaludniony, zawiera latyfundia i stare industrialne rodziny
    * Sybrianie są w większości na szczycie, Faerilowie w większości na dole. Teren ten był jednym z wartościowych centrów ekonomicznych.
* Ziemie Bankierzy były uważane za wzór tego jak model może działać i wyglądać. Silnie stechnicyzowany, ludzie żyją w dobrobycie.

### Gdy Rzeczywistość Pękła

* Bankierze są w ośrodku działania energii Esuriit. Stare Rodziny naturalnie przejęły kontrolę nad sentisiecią i wielu z nich oszalało.
* Ci, którzy zostali Dotknięci Esuriit ale nie oszaleli zrobili wspólny front, wprowadzili zasady i reguły (nieznane dla sybrian normalnie) i doprowadzili do zbrojnego przejęcia rzeczy od 'tych innych' Starych Rodzin które upadły
    * -> Każdy może upaść. Bądź silny, zdecydowany, bezlitosny.
    * -> Gdy Bankierz zaczyna ulegać Esuriit, odchodzić od reguł, staje się celem innych Bankierzy. Pomoc lub _coup de grace_.
* Ci, którzy oszaleli zostali zabici. Ci, którzy NIE oszaleli, zostali adoptowani. Z faerilską subkulturą Bankierzowie wypracowali reguły arystokracji.
    * -> W ramach Wielkiego Czyszczenia (eufemizm) doszło do błędów i przejęć majątków. Część Bankierzy to pamięta i nie jest fanami niektórych innych Bankierzy (eufemizm)
* Bankierze stają się bardzo agresywnym i ambitnym rodem. Ale nie da się odróżnić agresji i ambicji Esuriit od 'zdrowej wersji'. Stąd nacisk na pojedynki, gry i sformalizowane działąnia. Osoba zdominowana przez Esuriit najpewniej nie utrzyma kontroli i Wielkiej Gry.
* Bankierze byli zajęci sobą dość długi czas. Potem zaczęli rozbudowywać ośrodek władzy w Aurum. Ale Sowińscy, którzy "mieli łatwo" mieli już przewagę. I gdy Sowińscy przyszli, Bankierze mieli do wyboru: krwawą wojnę albo integrację w Aurum z Sowińskimi. Bankierze wybrali integrację, choć wielu do dziś tego nie zaakceptowało
    * -> Bankierze uważają Sowińskich za 'tych co mieli łatwo' i 'fuksiarzy'. Bankierze uważają, że to ONI powinni rządzić Aurum.
    * -> Bankierze nadal wspierają Aurum. Acz celują w jak najwięcej jak najwyższych stanowisk. 
* W sprawie Programu Kosmicznego Aurum i wielu innych wielkich planów Sowińskich, Bankierze ich wiernie wspierają.

### Teraz

Ziemie Bankierzy to w większości bardzo zaawansowane latyfundia, super-potężne technologicznie w których żyje się bardzo dobrze. Wysoka wydajność, wysoka wygoda, ale konsekwencja - tien Bankierz jeśli czegoś chce to może to zabrać. A inny tien Bankierz może wyzwać go na pojedynek o wspóldzielone zasoby (zasady co można wyzwać, o co można, jak to działa - są ściśle określone; inaczej nie byłoby możliwości budowania niczego trwale, patrz TABU 'nie niszcz').

'Tien Bankierz' winien być prawdziwym arystokratą. Ma ogromne uprawnienia, ale ma też odpowiedzialności - powinien kanalizować swoją ambicję i pragnienie zniszczenia przeciwko anomaliom i napastnikom. Powinien zachować kontrolę nad sobą i budować rzeczy większe niż on sam. By to osiągnąć, ma dostęp do wszystkich zasobów jakie potrafi utrzymać lub zniewolić (niewola jest OK, pod warunkiem, że nie ulegasz swojej mrocznej naturze przeciw niewolnikom).

Bankierze są jednym z najbardziej wpływowych rodów Aurum. Znajdziecie ich na większości wysokich stanowisk. Mają ogromną siłę militarną, zawsze zbrojąc się do walki z następnym przeciwnikiem. Dzięki Bankierzom i ich sile ognia udało się odeprzeć Inwazję Noctis i siły Saitaera. Bankierze jednak nie obracają swojej siły przeciwko Aurum - dzika sentisieć przeraża nawet ich.

To, co różni Bankierzy od Sowińskich - Bankierze nie mają 'Wielkiego Planu' na Zjednoczenie Ludzkości. Bankierze pragną rozprzestrzenić Arystokrację jako najlepszy system zarządczy na świecie.
