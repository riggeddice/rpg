## Opis Rodu

* Nazwa: Blakenbauer
* Serce
    * Metakultura wiodąca
        * Drakolici
            * -> 'ten świat jest niebezpieczny i nikt nam nie ufa; wykrójmy własną część świata gdzie możemy robić co uważamy za słuszne'
            * -> 'żadnych granic - każdy ma prawo znaleźć ideał i nikt nie ma prawa nikogo zatrzymać przed dążeniem do perfekcji'
        * MODYFIKACJA kulturowa
            * 'kontrakty nie są potrzebne jeśli jesteś na szczycie ewolucji, ale słowo pozostaje święte'
            * 'magia jest kolejnym narzędziem ewolucji - przekształćmy cały świat pod naszym kątem, nie tylko nasze ciała'
            * 'wszelkie biologiczne istoty nadają się do ewolucji, istnieje idealna forma dla każdej istoty pod kątem konkretnego celu. Z tienem na szczycie.'
    * Energia wiodąca
        * Praecis, Anteclis
        * -> 'celem nie jest przekształcenie tego co jest; celem jest konstrukcja idealnych form pasujących do nowego świata'
        * -> 'rekonstrukcja rzeczywistości na podstawie głębokiego zrozumienia i odpowiednich form'
        * -> 'koniec uciekania. Koniec ukrywania naszej prawdziwej natury. Czas zbudować odpowiedni świat, oparty na nauce i bioformacji.'
        * -> affinity (biokonstrukcja / płaszczki)
    * Wartości dominujące
        * Self-Direction, Achievement
        * -> 'nikt nie powie Blakenbauerowi co należy robić i czego robić nie należy. Nie możesz nas kontrolować. Wiedza i ewolucja idą naprzód.'
        * -> 'to ja będę tym, który osiągnie kolejny poziom ewolucji i rozwiążę odpowiedni problem'
        * -> 'moralność innych jest czymś, co nie powinno zatrzymać przed następnym poziomem ewolucji. Dobrze, mogą mnie potępiać - ale użyją mojej wiedzy'c
* Specyfika
    * Wyróżniki szczególne
        * Blakenbauerowie grają często rolę 'strasznych potworów', nawet jak nimi nie są.
            * Trzeba im oddać: _drakolicki homo superior_, Klątwa, nie uważanie się do końca za człowieka i wszechobecne płaszczki nie pomagają XD
        * W czymkolwiek Blakenbauer się nie specjalizuje, przeciętny Blakenbauer będzie lepszy w tym co robi niż nie-Blakenbauer
            * ...pomaga fakt, że robił eksperymenty na ludziach niezależnie od ich zdania w tym temacie.
            * przeciętny Blakenbauer jest dużo bardziej amoralny niż ktoś inny
        * 'Klątwa Blakenbauerów', czyli specyficzny wpływ na ich _visiat_
            * mężczyźni mają formę bojową; bardzo niebezpieczną transformację w bestię. Tracą wtedy kontrolę i ich magia skupia się na walce i zniszczeniu.
            * kobiety są ultra-adaptywne. Bardzo łatwo mogą zmieniać wygląd, kształt itp. Ale to wymaga zewnętrznej magii i działań.
        * Blakenbauerom zwykle towarzyszą 'płaszczki', czyli potwory słuchający swoich twórców
        * Blakenbauerzy zwykle wyglądają 'dość normalnie', mając niewielką tylko adaptację do swojego terenu.
    * Specyfika, jedną frazą
        * "Opracujemy lepsze jutro używając bioformacji i terraformacji. Lepsze jutro dla wszystkich. Magia to doskonałe narzędzie."
        * "Gdzie diabeł (lub Aurum) nie może, tam Blakenbauera pośle"
        * Nieludzcy naukowcy specjalizujący się w pracy nad perfekcyjnym życiem, pragnący zbudować perfekcyjny świat używając bioformacji.
    * Konflikty wewnętrzne
        * Czy dopuścić do traktowania ludzi w taki sposób lub do tak mrocznych eksperymentów? 'Indywidualizm' vs 'Lepszy Świat'
        * ?
    * Konflikty zewnętrzne
        * z Diakonami: 'piękno, wzbogacanie innych i ukrywanie swojej natury' vs 'inżynieria nowego, lepszego, replikującego się świata'
        * propozycja świata Blakenbauerów jest uważana za dość nieludzką i niebezpieczną
        * 'płaszczki' są potencjalnie bardzo niebezpieczne bez swoich kontrolerów i są zdolne do replikacji. De facto - monster spawners.
        * Blakenbauer pod Klątwą stanowi śmiertelne zagrożenie
    * Co powinni robić i co jest tabu
        * DOBRE: 'Twórz płaszczki, eksperymentuj z życiem i terraformuj rzeczywistość w coś lepszego niż to co mamy domyślnie'
        * DOBRE: 'Pomagaj ludziom na swoich włościach i nie tylko. Są cennym zasobem.'
        * DOBRE: 'Pozyskuj wiedzę i ją poszerzaj. Nigdy nie wiadomo co się przyda.'
        * DOBRE: 'Daj szansę każdemu kto Ciebie personalnie nie skrzywdził. Nie wszyscy nasi przodkowie byli święci. Jeśli Ciebie skrzywdził, cóż...'
        * TABU: 'Krzywdzenie innych na darmo. Jeśli ktoś musi cierpieć, niech będzie to wartościowe dla nauki lub świata'
        * TABU: 'Trwała krzywda innych ludzi czy magów. Takie rzeczy jedynie sprawiają, że wszystkim Blakenbauerom jest trudniej w budowie lepszego świata.'
* Bezpośredni sąsiedzi
    * Ród Diakon
    * Ród Verlen
    * Ród Samszar

## Historia i Kontekst
### Przed Pęknięciem Rzeczywistości

* Ziemie które dziś należą do rodu Blakenbauerów to był park narodowy i rezerwat; mokradła oraz teren 'mało zaludniony'
* Teren Blakenbauerów (i Diakonów) był mało zaludnionym miejscem, dość nieprzystępnym i specjalnie pozostawionym jako dziki
* Część prześladowanych drakolitów ukryło się wśród innych drakolitów, na terenie 'parku narodowego'.

### Gdy Rzeczywistość Pękła

* Tereny rodu Blakenbauer zaczęły się rozwijać w formie bardzo niebezpiecznej sentisieci jako PIERWSZE w Aurum, co jedynie wzmocniło efekt 'ci źli drakolici coś zrobili'
    * -> Blakenbauerowie, już wcześniej prześladowani, nadal są atakowani i to coraz bardziej. Acz sentisieć ich chroni.
* Blakenbauerowie musieli uciekać głębiej w niebezpieczne tereny i liczyć na to, że magia ich nie zniszczy.
    * -> tu pojawia się Klątwa Blakenbauerów; Blakenbauer ma drugą, niebezpieczną formę.
* Starcia między Diakonami i Blakenbauerami, które jednak szybko się skończyły gdy oba Rody poszły w inną stronę. Dziś do tego tematu nie wracają.
* Blakenbauerowie, samotni w świecie magii, zaczęli ściągać innych drakolitów i 'wyrzutków' oferując im nowy lepszy świat.
* Starcia między Blakenbauerami o to jak procedować w nowym świecie i o to jak rządzić się przy założeniu tak silnego indywidualizmu.
    * -> 'możesz robić co uważasz za słuszne jeśli inny Blakenbauer przez to nie ucierpi, też pośrednio. Nie niszcz reputacji, ale nie musisz o nią dbać.'
    * -> tien oznacza 'tego, który próbuje zbudować nową, lepszą rzeczywistość dla wszystkich i poszerzać swoją wiedzę i możliwości'
* Ludzie są świadomi, że tereny Blakenbauerów niekoniecznie są dla nich najlepsze, ale wiedzą, że KAŻDY dostanie nową szansę na tym terenie.
* Blakenbauerowie skupili się na płaszczkach i bardzo silnie grają tematem 'jesteśmy nie-ludźmi w nowym świecie'
    * -> 'jeśli się nas boją i nie wiedzą co zrobimy, dadzą nam spokój'

### Teraz

Blakenbauerowie są postrzegani jako jeden z mroczniejszych rodów Aurum. W praktyce, to pracowicie kultywowana reputacja - inni zostawią ich w spokoju.

Blakenbauerowie są dość amoralni, lecz nie robią nikomu krzywdy bez powodu. W odróżnieniu od Diakonów, nie skupiają się w ogóle na ludziach. Oni próbują skupić się na jak najlepszym stworzeniu lepszego organicznego świata i kawałka rzeczywistości. Jako, że ludzie stanowią zasilanie magiczne, Blakenbauerowie dbają o swoją populację i próbują zapewnić im zdrowie, bezpieczeństwo i wszystko czego oni potrzebują. Ale ważne jest to, że Blakenbauerowie fundamentalnie nie uważają się za część świata. Oni są czymś odmiennym od 'normalnych' ludzi i magów, nawet drakolitów.

Z uwagi na historię Blakenbauerów i ich prześladowanie przez większość 'normalnych ludzi', Blakenbauerowie dają szansę większości ludzi. Ziemie Blakenbauerów stanowią zawsze azyl dla ludzi którzy tam dotrą, nieważne czego nie zrobili w przeszłości. Acz jeśli zrobią coś niewłaściwego na terenach Blakenbauerów, biada im. Zawsze przydadzą się ludzie do przeprowadzenia bardziej zaawansowanych badań.

Ziemie Blakenbauerów nie wyglądają 'normalnie'. Tak jak ziemie Diakonów, są odmienne od wszystkiego co znamy. Ziemie Blakenbauerów to zaawansowane interakcje biologiczne poza technologiami, istoty z zaawansowanymi łańcuchami ekologicznymi i nienaturalne istoty i rośliny.

Blakenbauerowie mają tendencję do skupiania się na tematach organicznych i ewolucji. Mają tendencję do pójścia bardzo daleko i bardzo dokładnie. Mają dużą ilość mrocznych eksperymentów i badań i z uwagi na ich indywidualizm i brak silnej kontroli kończy się to często tym że faktycznie ktoś ucierpi.

Blakenbauerowie są _homo superior_, są _senti-tienami_ i są wzmocnieni przez Klątwę. Używają wszystkich tych assetów bez cienia zawahania - jeśli ktoś podpadnie Blakenbauerowi, to Blakenbauer jest w stanie zrobić mu dużo większą krzywdę niż mag z większości innych rodów. Żadnych granic, żadnych zasad moralnych. Tylko czysta konsekwencja stanięcia na drodze doskonałemu drapieżnikowi.

Jeśli Blakenbauer da komuś słowo to go dotrzyma. Słowo Blakenbauera pozostaje święte.
