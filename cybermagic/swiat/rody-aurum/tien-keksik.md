## Opis Rodu

* Nazwa: Keksik
* Serce
    * Metakultura wiodąca
        * Kordylici + Wenapnici
            * -> Ko: 'Szukaj piękna i radości w każdych okolicznościach, zabawa jest tym czemu warto żyć'
            * -> We: 'W życiu chodzi o to by dobrze umrzeć lub zdać Wielki Test'
            * -> KW: 'Oddaj się w pełni szalonej kreacji, wybierz Cel i zdaj Wielki Test. Uda się, bo Test jest tym co robisz!'
        * MODYFIKACJA kulturowa
            * 'Gdyby Wielki Test był czymś innym, Keksik by się tym nie zainteresował. Więc Wielki Test musi być tym na czym Keksik się skupia.'
    * Energia wiodąca
        * Alucis, Exemplis
        * -> 'Piękno, elegancja, sztuka, kolekcjonowanie, zmysłowość - gdzieś tam znajdziesz Test którego szukasz'
        * -> 'Keksik winien być przykładem tiena. Czysty, w dobrej formie, pierwszy na ochotnika.'
        * -> 'Keksik jest niemożliwy do złamania i zawsze z wdziękiem wspiera sojuszników.'
        * -> 'Balans między czynem i sztuką jest istotny. Szanuj wszelkie wysokie osiągnięcia i gardź brakiem kontroli i widoczną słabością.'
        * -> 'Każdy czyn powinien być piękny. Jesteś tym czym MIAŁEŚ być. Zachowaj swoją czystość i inspiruj do lepszego jutra.'
        * -> 'Bądź lojalnym, wiernym przyjacielem i przeprowadź wszystkich przez trudny Test.'
    * Wartości dominujące
        * Face, Benevolence
        * -> 'Bądź nieustraszony - jesteś ostatnią linią obrony w świecie pełnym rozpaczy. Pokaż sens życia. Odpędź demony uśmiechem.'
        * -> 'Twoja śmierć nie jest najgorszym co Cię może spotkać. Możesz okazać się niegodny wobec Rodu oraz Wielkiego Testu.'
        * -> 'Musisz być wystarczająco dobry, niekoniecznie najlepszy. Ważne, by ukierunkować energię by pomóc innym.'
        * -> 'Reputacja i szacunek są kluczowe. Szanuj i wspieraj wszystkich, poza tymi robiącymi czyny niegodne. Bądź źródłem światła, by niegodni zmienili zachowanie.'
* Specyfika
    * Wyróżniki szczególne
        * Nieustraszeni. Pomiędzy 'Wielkim Testem' oraz Alucis+Exemplis, Keksik panikuje ostatni. Skłonności do heroizmu.
        * Działania z rozmachem. Nie chodzi o to, by zostawić świat lepszym - chodzi o to, by WIDOCZNIE zmienić świat. Zdać Test.
        * Umiejętności artystyczne. Każdy Keksik uczy się co najmniej jednej dziedziny artystycznej, by móc zrobić coś pięknego.
        * Obsesyjne skupienie na wizerunku. To, co jest 'postrzegane' jest nie mniej ważne niż to co jest 'prawdziwe'.
        * Testowanie. Wieczne testowanie swego ciała i umysłu, zwłaszcza skupiając się na zachowaniu kontroli i nieskazitelnej reputacji.
        * Inspirowanie. Nie są 'najlepsi', ale próbują zawsze podnieść innych. Z uśmiechem biorą trudne i brudne prace oraz stają naprzeciw Złu niezależnie od kosztu. 
    * Specyfika, jedną frazą
        * Nieustraszeni artyści skupieni na nieskazitelnej reputacji, działający z dużym rozmachem.
    * Konflikty wewnętrzne
        * Linia Alucis-Exemplis. Ukojenie i Przyjemność vs Czystość i Perfekcja. świat-jaki-powinien-być vs świat-jaki-jest .
        * Linia Reputacja-Rozmach. Próba wyglądania jak najlepiej vs próba osiągnięcia wielkości i prezentowanie pozycji.
        * Nikt nigdy nie jest w stanie dorównać swoim wyobrażeniom. Trudno wrócić do poziomu Reputacji po porażce i upadku.
    * Konflikty zewnętrzne
        * Keksik często jest odbierany jako snob i elegancik, zwłaszcza wśród osób o niskiej reputacji.
        * (nie ma wielu konfliktów; Keksik często wejdzie w rolę wspierającą / supportową)
    * Co powinni robić i co jest tabu
        * DOBRE: 'Jeśli nikt nie chce czegoś zrobić a jesteś w stanie, zrób to. Nawet, jeśli to brudne czy nieprzyjemne.'
        * DOBRE: 'Nie bój się śmierci czy cierpienia. Jesteś elementem Wielkiej Symulacji. To może być Wielki Test.'
        * DOBRE: 'Szanuj tych, którzy potrafią robić wielkie rzeczy i coś osiągnęli. Aspiruj do bycia jak oni - i skieruj energię by pomóc grupie.'
        * DOBRE: 'Stwórz coś pięknego, coś, czego nie było. Coś, co sprawi że inni zobaczą dlaczego warto. Coś, co przyniesie światło w ciemności.'
        * TABU: 'Utrata twarzy z jakiegokolwiek powodu oznacza, że jesteś osobą niską i potencjalnie niebezpieczną z perspektywy Rodu.'
        * TABU: 'Zdrada przyjaciół albo swojej grupy. To sprawia, że nie tylko jesteś źle postrzegany, ale też uniemożliwia Ci prowadzenie innych.'
        * TABU: 'Poddanie się używkom, magii itp. Używanie jest OK, ale całkowita utrata kontroli jest niewłaściwe.'
* Bezpośredni sąsiedzi
    * ?
    * ?
    * ?

## Historia i Kontekst
### Przed Pęknięciem Rzeczywistości

* Na tym terenie znajdują się zarówno dominujący powszechnie Kordylici jak i niewielkie grupki Wenapnitów. Ci drudzy uważani powszechnie dzisiaj za problematycznych nie wpadli jeszcze w obsesję Symulacji (Rzeczywistość nie Pękła). Podstawowe cech ich kultury jednak skonfrontowały ich skrajnie przeciwko dominującej grupie Kordylitów.
* ?

### Gdy Rzeczywistość Pękła

* Teren, na którym występował ten Ród ma wyjątkowo duże Skażenie Pryzmatyczne. Co gorsza, pierwotnie posiada kolor Alteris i Alucis.
    * Niestety, ludzkie koszmary stają się prawdą. Rzeczywistość przekształca się w najgorszy możliwy sposób z perspektywy przeciętnej populacji tego miejsca.
    * Ludzie tak są skupieni na przetrwaniu, że aż zapominają, dlaczego próbują przetrwać. Pojawia się pętla - im bardziej ludzie próbują przetrwać, tym bardziej teren staje się problematyczny.
    * W tym miejscu pojawia się ród Keksik. Oni wyraźniej nie akceptują tego, że przetrwanie teoretycznie staje się wszystkim. Decydują się walczyć z Pryzmatem bezpośrednio, niekoniecznie zdając sobie w pełni sprawę z tego co robią. Po prostu tacy są.
    * -> W miejscu o skrajnie dużym Skażeniu Pryzmatycznym reputacja i fasada stają się kluczem. Nie tylko trzeba być w stanie poradzić sobie z problemami - trzeba jeszcze wyglądać w taki sposób by inni wierzyli że to rozwiąże problem.
    * -> Już od samego początku Keksikowie napotykają największy dylemat – Ukojenie i Piękno kontra Czystość i Wybitność. Ale na tym etapie okazuje się, że strategię wygrywają co okazuje się być Wybitne Piękno i inspiracja ku lepszemu jutru.
* ?

### Teraz

Ziemie rodu Keksik są bardzo stabilne. Tienowie aktywnie opiekują się terenem oraz populacją ludzką na tym terenie, zwracając uwagę nie tylko na zdrowie fizyczne, ale też na to by było po co żyć. Tienowie dbają o zrównoważony rozwój swoich ludzi, oczekując za to jednak posłuszeństwa.

Populacja ludzka na tym terenie żyje stosunkowo normalne, ale bezpieczne życie. Keksikowie dają pełną swobodę memetyczną, ufając, że poradzą sobie z ewentualnymi anomaliami i problemami.

Tien Keksik próbuje być pomocnym dla swojego społeczeństwa i próbuje dbać o swoich ludzi. Alucis + Exemplis przekształcają się w wybitny dzieła sztuki, ale także w ogromną cierpliwość i niekończącą się kontrolę swoich czynów. Tienowie nie są powodem do strachu; ludzie lubią swoich opiekunów.

Jednocześnie populacja ludzi jest stosunkowo ‘słaba’ z perspektywy pozostałych rodów. Tienowie się nimi opiekują, przez to ich trochę upupiając. Ludzie mieszkający na tych terenach bardzo rzadko opuszczają te ziemie. Większość tego co potrzebują zapewniają im tieni.
