## Opis Rodu

* Nazwa: Perikas
* Serce
    * Metakultura wiodąca
        * Atarieni + Faerile
            * -> A: 'Kto śpi pozwalając magii na rozrastanie się i działanie po swojemu, ten przegrał. Trzeba wiecznie być czujnym'
            * -> F: 'Nie wolno spoczywać na laurach, trzeba znaleźć nowe metody radzenia sobie z magią i anomaliami'
            * -> AF: 'Głęboka wiara w to, że każda jednostka może coś zmienić i każda osoba i decyzja ma znaczenie'
        * MODYFIKACJA kulturowa
            * 'Magia jest zawsze niebezpieczna i należy jej unikać. Zamykanie magii w narzędzia, jej kontrola i reglamentacja jest kluczem'
    * Energia wiodąca
        * Exemplis, Unumens
        * -> 'zjednoczenie przeciw anomalnej rzeczywistości pozwoli zmienić świat w coś co działa i jest piękne. Razem - możemy.'
        * -> 'Anomalie, Skażenie, niekontrolowana magia - musi być monitorowana, kontrolowana i unieszkodliwiana'
        * -> 'nadmierne poleganie na magii i oddalenie się od ideału ludzkości jest głupotą i zagrożeniem'
        * -> 'wolność TAK. Ale poza obszarem magii. Magowie winni być rejestrowani i reglamentowani.'
        * -> affinity (puryfikacja, antymagia)
    * Wartości dominujące
        * Conformity, Security
        * -> 'to hierarchie i systemy dają prawdziwe możliwości zmiany świata'
        * -> 'pojedyncza decyzja i magia potrafią zmienić rzeczywistość, stąd tak ważne są zasady i reguły'
        * -> 'odpowiedzialnością jest piąć się jak najwyżej szczytu by redukować zależność od magii i zamykać moc w narzędziach' 
* Specyfika
    * Wyróżniki szczególne
        * Ród nieufny magii i zwalczający magię i energię magiczną poza zakresem narzędzi
        * Specjalizują się w puryfikacji i usuwaniem magii w każdej formie
        * Są wierni hierarchiom i lojalni, ale jak się przeleje czarę to robią sabotaż magicznych obszarów
        * Każda decyzja, każde działanie ma znaczenie - więc każda decyzja musi być świadoma i poprawna
    * Specyfika, jedną frazą
        * "Możemy naprawić świat, nie polegać na magii i udowodnić supremację ludzkości - ale musimy iść razem"
        * Zdyscyplinowani cisi puryfikatorzy wierni hierarchiom i zasadom, usuwający wszelkie formy magii dynamicznej.
    * Konflikty wewnętrzne
        * ile magii jest konieczne do używania by rozwiązać problem, jak duże ryzyko bierzemy
        * więcej wolności, czy więcej pomagania w niszczeniu anomalii? W końcu jesteśmy tylko my...
        * więcej dobrobytu dla swoich ludzi, czy więcej zwalczania anomalii też poza swoim terenem?
    * Konflikty zewnętrzne
        * czy stanąć przeciwko zasadom i hierarchiom czy zaakceptować użycie magii tym razem?
        * ktoś wykorzystuje naturalną anomalię / magię, która dla Perikasa jest nieakceptowalna i winna być dezaktywowana
        * vs Samszar, Blakenbauer: oni wykorzystują energię magiczną 'do lepszego świata' w sposób niebezpieczny i nieakceptowalny
    * Co powinni robić i co jest tabu
        * DOBRE: 'usuwaj anomalie magiczne i problemy z magią; niekontrolowana magia winna być usunięta i zniszczona'
        * DOBRE: 'promuj wykorzystywanie technologii w miejsce magii, uznawaj wszelkie elementy magiczne za bardzo duże ryzyko'
        * DOBRE: 'opracowuj i rozdawaj wiedzę jak radzić sobie z magią i jak redukować zależności od poszczególnych Energii'
        * TABU: 'zostaw po sobie bałagan w formie anomalnej i magicznej'
        * TABU: 'możesz coś magicznego unieszkodliwić i nie kosztuje Cię to za dużo a jednak tego nie robisz'
        * TABU: 'przez Twoje zaniechania niezależnie od okoliczności ludzie / inni magowie uciekli się do używania magii'
        * TABU: 'jako, że jesteś tienem to nadużywasz swoje uprawnienia i pomiatasz ludźmi. Magowie są zawsze Skażeni, ale ludzie to ludzie.'
* Bezpośredni sąsiedzi
    * Ród Verlen
    * ?
    * ?

## Historia i Kontekst
### Przed Pęknięciem Rzeczywistości

* Ziemie które dziś należą do rodu Perikas były różnorodnym terenem, od dużych miast przez wioski do terenu niezamieszkałego.
    * Na tych ziemiach znajdowało się największe miasto, stolica regionu - Karmalewin 
* Dzisiejsi Perikasowie byli bardzo różnorodną grupą ludzi z różnych klas społecznych. Pozornie nic ich nie łączyło.

### Gdy Rzeczywistość Pękła

* Karmalewin jako miasto zmieniło się w Wielką Manifestację Unumens, zmieniając się w 'Wielkie Mrowisko'. Anomalia jest zbyt potężna by ją kiedykolwiek wyczyścić czy puryfikować.
    * -> Perikasowie do dzisiaj próbują coś zrobić z Anomalią Karmalewin. Do dzisiaj im się nie udało.
    * -> Pierwsze spotkanie z magią dla większości na tym terenie jest skrajnie traumatyczne i niebezpieczne.
* Doszło do prób ratowania ludzi i oddalania się od Anomalii Karmalewin. Wielu zginęło, wiele terenu zostało ciężko Skażone. Większość miast upadła. Bardzo ciężka sprawa i sytuacja
* Pierwsi magowie wcale nie radzili sobie najlepiej.
* Pierwsi Perikasowie mieli wsparcie sentisieci; ich połączyło 'braterstwo krwi' i chęć wspólnego ratowania ludzi i swojego terenu. To były prawdziwe starcia ludzi i magów przeciw Anomaliom i Efemerydom, wspieranym przez Perikasów
    * -> musimy działać razem przeciw magii
    * -> magowie prędzej czy później upadną; Unumens ich przejmie i obróci przeciwko populacji którą chronili (wyjątek: Perikasowie)
* Na przestrzeni czasu udało się zapewnić pewne bezpieczeństwo dla ludzi i odbijać część terenu dla ludzkości, zwłaszcza polegając na technologii i nowo odkrytych własnościach lapisu a nie na czystej magii.
* Magowie niestety prędzej czy później ulegali energii Unumens, z wyjątkiem Perikasów
    * -> magowie wymagają rejestracji oraz nie można polegać na magii tak naprawdę.

### Teraz

Ród Perikas jest niesamowicie sceptyczny wobec magii; też dlatego, bo właśnie ta energia która przysparzała im tak wiele problemów jest jedną z ich energii wiodących. Wolą tworzyć zaawansowane narzędzia magitech które ujarzmiają magię niż polegać na potencjalnie niebezpiecznych  zaklęciach z potencjalnymi Paradoksami. Magowie rodu Perikas raczej nie czarują jak mogą tego uniknąć.

Anomalia Karmalewin do dzisiaj jest niebezpieczną Anomalią Macierzystą Unumens, do dzisiaj jest cierniem w sercu każdego Perikasa i do dzisiaj jest dowodem, że jeśli pozostawi się magię niekontrolowaną to może dojść do czegoś strasznego.

'Tien Perikas' jest trochę jak strażak - zajmuje się usuwaniem anomalii i efektów magicznych oraz ochroną ludzi przed magią. Uważają się za 'strażaków' a nie 'zarządców' danego terenu. Jako 'siły specjalne antymagiczne' mają poszerzone uprawnienia i możliwość wymuszenia współpracy od różnych ludzi, ale w bardzo złym stylu jest nadużywanie swoich uprawnień.

Włości Perikasów są maksymalnie 'normalne i nie-Aurum' ze wszystkich terenów Aurum. Unika się używania magii i specjalnego statusu tienów, skupienie jest na niszczeniu zagrożeń dla ludzkości (dostarczanych przez sentisieć wbrew Perikasom). 

Perikasowie z przyjemnością przyjdą pomóc przeciwko anomaliom magicznym nawet poza swoim terenem. Jest to coś co uważają za swój obowiązek a Karmalewin za wieczny pomnik i przypomnienie co się stanie jak zawiodą.