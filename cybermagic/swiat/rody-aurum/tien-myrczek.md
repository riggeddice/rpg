## Opis Rodu

* Nazwa: Myrczek
* Serce
    * Metakultura wiodąca
        * Kordylici + Klarkartianie
            * -> Ko: 'Najważniejsze jest to, by nie dać się zwariować i pamiętać o odpoczynku i hobby. Nie można żyć by pracować.'
            * -> Kl: 'Da się odkryć jak rzeczywistość działa. Magia jest kolejną rzeczą którą da się odkryć, zrozumieć, wykorzystać.'
            * -> KK: 'Magię da się docelowo wykorzystać i jest świetną okazją - ale musimy mieć czas i podejść do tego spokojnie'
        * MODYFIKACJA kulturowa
            * 'Z uwagi na to czym magia jest i jak jest niebezpieczna, trzeba wpierw się ustabilizować wewnętrznie i zrobić własny konstrukt - świat wewnętrzny'
    * Energia wiodąca
        * Alucis, Esuriit
        * -> 'Magia, jakkolwiek niebezpieczna, daje nam niesamowite możliwości i należy wejść w głąb, acz ostrożnie. To okazja!'
        * -> 'Medytacja i skupienie na wewnętrznym świecie umożliwi nam stabilizację by radzić sobie z największymi problemami.'
        * -> 'Wejście odpowiednio głęboko w swoje hobby i marzenia wymaga oddania się im do poziomu absolutnej obsesji'
        * -> 'Wpierw stwórz własny świat mentalny (medytacja, hobby, skupienie), POTEM skanalizuj wolę by robić rzeczy niebezpieczne'
    * Wartości dominujące
        * Self-Direction, Humility
        * -> 'Każdy znajdzie swoje miejsce i musi owo miejsce znaleźć i wypracować. Nie wiemy co się przyda, idź za sercem.'
        * -> 'Bogactwo i złożoność rzeczywistości jest niesamowita i każdą rzecz można studiować setkami lat.'
        * -> 'Każdy szuka swojej drogi w trudnym świecie. Nie przeszkadzaj im, nie kwestionuj. Rób swoje i rób to dobrze.'
        * -> 'Możesz musieć podejść do problemu tysiąc razy, ale każdy problem da się rozwiązać - samemu lub z przyjaciółmi.'
        * -> 'Jak ktoś potrzebuje pomocy, pomóż mu. Nigdy nie wiesz kiedy Ty będziesz pomocy potrzebować.'
* Specyfika
    * Wyróżniki szczególne
        * Głęboki optymizm. Myrczek zachowuje spokój i optymizm niezależnie od sytuacji. WIEDZĄ że da się sobie poradzić.
        * Jedna bardzo głęboka specjalizacja. Hobby Myrczka jest światem tego Myrczka, wchodzi nieskończenie głęboko w temat.
        * Trochę nieżyciowi. Trochę żyją we własnym świecie. Takie 'nic ich nie rusza i nie dotyka'.
        * Myrczek bardzo szybko potrafi się re-centrować i wrócić do wewnętrznego świata
    * Specyfika, jedną frazą
        * Nieco nieżyciowi marzyciele skupieni na swoim hobby, którzy pomogą każdemu kto poprosi
    * Konflikty wewnętrzne
        * Niektórzy bardziej dotknięci przez Esuriit (albo o to podejrzani) wchodzą za głęboko w obsesję i stają się niebezpieczni
        * Gdy różni Myrczkowie mają różne wizje odnośnie tych samych zasobów współdzielonych
    * Konflikty zewnętrzne
        * Myrczek ze swoją nieżyciowością, niską prędkością, niepewnością że zrobią co trzeba i swoim skupieniem na wewnętrznym świecie jest SKRAJNIE IRYTUJĄCY.
        * Wysoki optymizm Myrczka w obszarze magii wraz z nastawieniem długoterminowym sprawia, że ktoś chcący naprawić sytuację tu i teraz ma często silny konflikt z Myrczkiem.
        * Myrczek może źle zinterpretować rzeczywistość przez swoje wizje (Alucis) i działać nieracjonalnie i błędnie.
        * Jeśli ludzie nie chcą czegoś robić, Myrczek ich nie zmusi. Jeśli Myrczek nie chce czegoś robić, najpewniej nie zrobi.
            * Trudno zaufać, że Myrczek szybko i dobrze coś przeprowadzi, chyba że on chce.
        * Myrczek pracuje nad czymś co jest potencjalnie niebezpieczne. Ktoś mu to niszczy. Myrczek eskaluje w bardziej niebezpieczny projekt...
    * Co powinni robić i co jest tabu
        * DOBRE: 'Weź sobie bardzo trudny problem do rozwiązania i go rozwiąż w ciągu następnych 10 lat.'
        * DOBRE: 'Nie zapominaj o odpoczynku i konieczności zachowania równowagi emocjonalnej, bo magia Cię pożre.'
        * DOBRE: 'Długoterminowe działanie, działanie w skali pokoleń a nie jednostek. Jednostka może cierpieć, jeśli uda się zrobić coś długoterminowo.'
        * DOBRE: 'Zapewniaj piękno, radość, spokój, ukojenie. Spraw, by wszyscy czuli się lepiej i by wszystko działało lepiej.'
        * DOBRE: 'Znajdź jak najlepsze zastosowanie dla magii. Nie bój się jej. Magia jest okazją i korzyścią. Przesuwa nas do przodu.'
        * TABU: 'Działanie gdy jest się zdecentrowanym i nie ma się równowagi mentalnej jest niebezpieczne. Wpierw odnajdź wewnętrzne skupienie.'
        * TABU: 'Zmuszanie innych do działania poza standardowy balans lub wytrącając kogoś ze stabilnego rdzenia mentalnego; presja/krzyczenie.'
        * TABU: 'Niszczenie czegoś nad czym ktoś ciężko pracował, nawet, jeśli to jest potencjalnie niebezpieczne'
* Bezpośredni sąsiedzi
    * Bankierz (latyfundia i bogactwo, ten teren modelował po terenie Bankierzy)
    * Lemurczak (latyfundia, wpływy Aureliona)
    * Kazitan

## Historia i Kontekst
### Przed Pęknięciem Rzeczywistości

* Teren Myrczków nie był szczególnie bogaty ani zaludniony. Teren rolniczy, znajdujący się w obszarze wpływów latyfundiów Bankierzy.
* Niższy poziom bogactwa czy technologiczny niż ziemie Bankierzy, ale ta sama strefa kulturowa.
* Tereny 'publiczne', nie należące do wielkich rodzin czy firm. Są tu pewne wpływy Syndykatu Aureliona, ale nie tak duże.

### Gdy Rzeczywistość Pękła

* Bankierze myśleli, że uda się przejąć kontrolę nad tym terenem bo ma podobne cechy. Nie udało się. Nie ma kompatybilności sentisieci.
    * Wielkie starcia i walki pomiędzy sentisieciami.
    * Bankierze wysyłają siły, robią badania naukowe... nic nie działa. Sentisieć po prostu nie jest kompatybilna.
    * -> Ludzie w coraz gorszym stanie. Potrzebują pomocy i nadziei. Ale nie ma jej skąd wziąć.
* Pojawiają się bohaterowie, lekarze, badacze... ludzie, którzy z optymizmem i cierpliwością próbują ujarzmić sentisieć.
    * -> Wielu z nich zostaje pożartych przez Esuriit + Alucis, ale najmocniej zcentrowani dają sobie radę
    * -> Pojawia się ród Myrczek - część z Bankierzy, część z zewnątrz. Ludzie, którzy są szczególnie ustabilizowani, optymistyczni i zdolni do spokojnego i cierpliwego działania i rozwiązania problemu stopniowo.
* Teren ulega stopniowej stabilizacji; Myrczkowie nie próbują OPANOWAĆ sentisieci a się z nią POŁĄCZYĆ i WYKORZYSTAĆ. Stopniowa cierpliwa praca i próba obsesyjnego zrozumienia i określenia
    * -> Tylko optymistyczni marzyciele długoterminowo nie zostają pożarci przez Esuriit.
* Teren ma bardzo niebezpieczne anomalie, zwłaszcza typu Esuriit (plaga nienawiści), choroby, zniszczenia i elementy. Myrczkowie konsekwentnie i powoli rozbierają problem, jeden po drugim, recentrując się i skupiając na tym co i kiedy da się zrobić. Ci, którzy się zdecentrowali zostali pożarci przez Esuriit i obrócili się przeciw reszcie.
    * -> Zwiększenie przekonania, że KAŻDY problem da się rozwiązać jeśli tylko działa się powoli i metodycznie
    * -> Stabilny rdzeń Alucis jest kluczem by poradzić sobie z Esuriit.
* W końcu teren Myrczków się powoli stabilizuje, choć nigdy nie jest w pełni bezpieczny.

### Teraz

Ziemie Myrczków nie są w pełni bezpieczne. Istnieją poszczególne manifestacje Alucis i Esuriit, istnieją mniej i bardziej bezpieczne miejsca, ale są wystarczająco dobrze określone i istnieją bezpieczne drogi pomiędzy głównymi ośrodkami.

Tereny Myrczków są niesamowicie zaawansowane technomagicznie. Ich mistrzostwo energii magicznych i opanowanie energii jest nieporównywalne z większością rodów a potencjalne awarie i problemy są wpisane w koszt tych technologii - ważniejszy jest postęp i długoterminowe działanie niż to by nikomu nigdy nie stała się krzywda.

Myrczkowie się specjalizują w zupełnie różnych rzeczach, wędrując i rozwiązując różnorodne problemy tam gdzie to konieczne. 'Tien Myrczek' ma pojedyncze hobby/obsesję, zasilane przez Esuriit i stabilizowane przez Alucis. Rolą tien Myrczka jest doprowadzić do najlepszego zrozumienia materii i pomocy ludziom - magia jest pomocna i jest okazją by świat był lepszy i Myrczkowie chcą to udowodnić. 

W kontekście radzenia sobie z Esuriit i innymi problematycznymi energiami Myrczkowie mają radę, biblioteki i zbiory rad mających na celu pomocy młodym adeptom. Ich techniki medytacyjne i wchodzenie w głąb Alucis daje im więcej możliwości niż normalmym magom się wydaje. Acz wcale nie są mniej nieżyciowi ;-).
