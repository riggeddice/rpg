# Bioformacja
## Metadane

* Nazwa: Furia Mataris
* Shortdesc: noktiańska infiltratorka homo superior zdolna do samodzielnego operowania; (child agent)-derived

## Dostosowania

Ogólnie: Furia Mataris jest szybsza i silniejsza od większości mężczyzn, ale nadal jest człowiekiem i nie ma nadludzkich zdolności.

* Elementy wychowania savarańskiego
    * minimalne potrzeby, zużycie, ślad
    * robisz wszystko co potrzebne, we are parts of the System
* Odporność na promieniowanie, Skażenie, trucizny
* Wzmocnione kości i gęstość mięśni - są silniejsze i szybsze
* Wyszkolenie w używaniu większości broni
* Umiejętność wytrzymania dużego bólu i presji
* Po użyciu odpowiednich środków, zmiana tonu skóry

## Koszty

* Specjalne serum raz tygodniowo, przez strzykawkę (można overloadować do 3 tygodni, ale ze stratą surowców i chorą Furią)
* Biologicznie, niekompatybilna z normalnymi ludźmi (transfuzja, dzieci itp.)
* Wymaga najpóźniej 11-latki do biomorforwania; procedura transformacji zajmuje koło 6 lat (równoczesne ze szkoleniem itp).
* Success rate: 70%, fail w 80% kończy się sprawną osobą używaną gdzieś indziej
* Furie są tylko dziewczynami z uwagi na kilka rzeczy
    * U chłopców success rate jest rzędu 30%. Nie warto.
    * Chłopcy mają testosteron itp. To zmniejsza success rate, ale nie uniemożliwia. Testosteron miesza na tyle, że powstają berserkerzy.
    * Dziewczyny są naturalnie lepszymi infiltratorkami, bo są słodsze, nikt się nie spodziewa.

## Wykorzystanie

* W warunkach w których człowiek bez pancerza by sobie nie poradził, na jednostkach kosmicznych
* Infiltracja - wchodzi do nieznanej populacji, cuteness++, scout, eksfiltracja
* Asasynka
* Bardzo rzadko - na froncie. Po prostu nieeefektywna kosztowo. Chyba że jako bodyguard VIPa.

## Opis

* Pochodzenie: jednostka savarańska, infiltratorzy polujący piratów i pobierający rzeczy z nie-savarańskich baz i jednostek
* Rekrutacja
    * Sieroty, 'osoby nadmiarowe', czasem: jakaś forma religii.
    * Bardzo rzadko ochotnik (dziecko nie może być ochotnikiem prawnie, choć savaranie tak na to nie patrzą)
