## Energia

### 1. Skrótowo

* **nazwa**: Interis
* **tron**: Nihilus
* **ofiara / Skażeniec**: Interient
* **slogan**: NULL AND VOID
* **alternatywne nazwy**: 
    * Wielkie Wymazanie
    * Pustka wszelkich znaczeń
    * Harmonia wiecznej nicości
    * Kres istnienia i znaczenia
* **idea**: Wymazanie. Negacja. Unicestwienie. Pustka. Zapomnienie. Usunięcie znaczenia. Niczego nie ma, niczego nie było, niczego nie będzie i nic nie ma znaczenia.
* **wizja**: Perfekcyjna równowaga. Nie ma niczego. Nic nie istniało, nic istnieć nie będzie. Koniec bez początku.
* **symbolika**: czarny punkt zasysający wszystko co istnieje

### 2. Lokalizacja w Panteonie

* "Możesz Wygrać z Nihilusem"
* "Możesz Spowolnić Nihilusa"
* "Możesz Zaakceptować Nihilusa"
* Kolory inne
    * Interis, pustka wszelkich znaczeń

### 3. Wyjaśnienie energii

* **Tron**: Nihilus
* **Dominujący kolor**: Entropia, koniec wszystkiego
* **Kolory wspomagające**
    * Bezsilność: "Nic co zrobimy nie ma znaczenia. Nikt nas nie zapamięta. Nie możesz wygrać, zremisować, wycofać się z gry. Nie będzie niczego i nic na to nie możemy poradzić."
    * Ogrom: "Wszechświat jest niezmiernie wielki, w atomie jest więcej pustki niż materii, historia trwa 'wieczność'. Nic nie jesteśmy w stanie zrobić ani osiągnąć."

Po nocy przychodzi dzień. Śmierć jednej istoty daje życie innym. Ale nie w wypadku Interis. Prawdziwa pustka pochłania coraz więcej, nie pozostawiając niczego. Wymazuje rzeczy z egzystencji sprawiając, że nigdy nie istniały. Nie ma energii. Nie ma życia. Nie ma radości, bólu ani cierpienia. Nie ma _niczego_. Jest jedynie perfekcyjna równowaga, harmonia nicości, pustka wszelkich znaczeń. Ostateczne zwycięstwo entropii, energii Interis i **Nihilusa**.

Interis jest jak wieczna otchłań, w której każdy promień światła, każda myśl, każdy dźwięk zostają pochłonięte, nigdy nie mogąc się narodzić, jakby nigdy ich nie było. Gdy galaktyki w końcu zgasną, i nawet czarne dziury zgasną i zabraknie energii we wszechświecie - Interis doprowadzi do Harmonii Nicości.

Interis to jest to, co jest POMIĘDZY rzeczywistością, POMIĘDZY nierzeczywistością, perfekcyjny dekompozytor, absolutna symetria. Interis wymazuje byt z rzeczywistości, nie produkując energii. Wymazanie. Negacja. Unicestwienie. W pustce Interis nic nie ma już znaczenia. Entropia zawsze zwycięża i nic co zrobimy nie jest w stanie jej zatrzymać. Nasze czyny nie mają znaczenia. To jak próbować walczyć z tsunami lub z wybuchem wulkanu. Nie wiemy jak świat się zaczął, ale Nihilus go zakończy.

Nihilus, bóg Entropii przy Ogromie i Bezsilności, nie dba o nikogo i o nic. Nihilus nie zauważa niczego. Jest reprezentacją śmierci cieplnej wszechświata i końca rzeczywistości. Usunięcia 'znaczenia' i marzeń. Jest niepowstrzymany, nieubłagany, nie da się z nim negocjować. Nihilus to nie tylko kres wszystkiego, ale także świadectwo naszej świadomości bezsilności wobec nieuniknionego. Reprezentuje apogeum końca, ostateczną granica egzystencji i Wielkie Wymazanie.

Co ważne, Interis i Nihilus się nie spieszą. To najbardziej 'cierpliwa' energia. I tak wszystko się skończy.

### 4. Wizja Świata z dominacją tej Energii

Przy pełnej dominacji Interis nie będzie _niczego_. Ale pozwolę sobie pokazać rzeczywistość, gdzie jest jeszcze coś więcej niż tylko Interis, by pokazać w którą stronę to idzie.

Rzeczywistość stała się pustą, bezkresną otchłanią, gdzie każdy dźwięk, każda myśl, każdy kolor zostają wchłoniętę przez absolutną ciszę i ciemność. Wszystko, co kiedykolwiek znaliśmy - emocje, wspomnienia, pragnienia - zostaje wyssane. Zamiast tego pozostaje jedynie nicość, rozciągająca się w nieskończoność.

Najsilniej odbite miasta w Eterze Nieskończonym stają się fragmentami pustych cieni, rozciągających się w Ostatecznej Pustce. Nie ma dźwięków, zapachów ani ruchu. Nie ma marnowania energii - w ogóle nie ma energii. Co więcej, nie ma też znaczeń, nie istnieją żadne pojęcia. 'Przyjaźń', 'złość' - wszystko zaniknęło i zostało usunięte nawet z komunikacji czy z serc cieni, które kiedyś były ludźmi.

Wszystko, co kiedykolwiek miało znaczenie, teraz znika w absolutnej pustce Interis.

Wracając do ludzi, ci najmocniej odbici w Eterze stracili swoją tożsamość i sens egzystencji. Stracili pamięć. Ich emocje, wspomnienia i myśli zaniknęły, a z nimi wszelkie pojęcie o tym, kim kiedykolwiek byli. Nie ma rodzin, więzów, ludzkich dzieł. Co więcej, nie ma też żalu czy tęsknoty za tym, co utraciliśmy. Jest tylko Pustka.

Sam koncept "istnienia" i "nieistnienia" zanika w nicości Interis.

Świat Interis jest miejscem, gdzie wszelkie znaczenie i kontekst ginie w absolutnej pustce.

### 5. Typowe manifestacje tej energii

Anomalie / efekty

* Usunięcie Czynności: ktoś nie jest w stanie czegoś zrobić (np. ruszać uszami, powiedzieć czegoś, popełnić samobójstwa) - nawet nie umie WYOBRAZIĆ sobie robienia tego
* Wymazanie Uczuć i Pamięci: Patrzysz na zdjęcie osoby ukochanej i jej nie rozpoznajesz (lub rozpoznajesz, ale nic nie czujesz)
* Wysysanie Motywacji: to nic nie zmieni, to jest bez znaczenia, to nie ma sensu, nic nie zrobimy, jesteśmy za 'mali'
* Absolutne Zniszczenie: Interis to najbardziej destruktywna forma energii
* Zrozumienie Prawdy: "Jedyne, co nam zostaje to samobójstwo. Nic nie ma sensu."
* Rozpięcie z Rzeczywistością: "Nic nie istnieje, nic nie ma znaczenia, nic nie ma sensu". Katatonia, pusty śmiech, TERROR.
* Entropia postępuje. Rzeczy rdzewieją i się psują.
* Morale zanika. Nic nie cieszy. Brak woli życia. Leżysz i patrzysz.
* TERROR w obliczu Wielkości Nihilusa i Entropii.
* "Nic nie czuję!": nie czuje ciała, nie czuje bólu, nie czuje uczuć i pragnie poczuć ZANIM PRZESTANIE PRAGNĄĆ...
* Wymazanie czegoś z rzeczywistości: więcej niż tylko zniszczenie; nawet nie pamiętamy, że to kiedyś istniało
* Destrukcja magii / efektu magicznego: Interis zawsze wygrywa.

Przeciwnicy

* "Entrofera": 'pies pustki', istota złożona z Entropii, której dotyk rdzewieje i postarza, która wysysa i poluje
* "Exnihilor": mag, rozprzestrzeniający Pustkę, który nie posiada już niczego 'w środku'. Niczego nie pragnie. Oddał się Nihilusowi.
* "Anioł Harmonii": perfekcyjna czarna kula poruszająca się i wysysająca i usuwająca wszystko z egzystencji. Niszczy i prowadzi do zrównania.
* "Atinaspes": 'Ćma Braku Nadziei', ćmokształtna istota ze świetlistymi mackami widoczna (tylko bez światła) przez żywe istoty, której oczy zabierają wszystko co wartościowe i która buduje Gniazdo z wyssanych żyć, niewoląc ofiary.

### 6. Reprezentacja muzyczna

TRIGGER WARNING: Entropy, Decay.

* Delain "Creatures"
    * "Our cities have turned to ash | All our stories have been told | Our final melody is slowly | Dying with the light"
    * "You try, you stall, you're forced to give it all" - "But when your back's against the wall, who's gonna catch you when you fall"
    * to reprezentuje, że czegokolwiek robimy, idzie wszystko w jedną stronę i tracimy coraz więcej i walczymy ale w końcu nam się nie uda i Nihilus w nieunikniony sposób wygrywa
* Devouring Stars "Whispers in the Void"
    * posłuchaj melodii. Usłyszysz zanikającą rzeczywistość.
* wariant Linkin Park "In The End"
    * https://www.youtube.com/watch?v=1KzlrT4gYSA  (Warhammer 40.000 Tribute - Death - Linkin Park (Cover))
    * "One thing, I don't know why | It doesn't even matter how hard you try"
    * "I tried so hard and got so far | But in the end, it doesn't even matter | I had to fall to lose it all | But in the end, it doesn't even matter"
    * aspekt beznadziejności i 'nic nie możemy zrobić'.
* Infected Rain "Dying Light"
    * "Like leaves falling from the tree | We crumble and fall inevitably" ... "Like a candle in the wind | Flickering, fading in the end"
    * "Wrinkles on our face (To see the other side of the moon) A sign of our grace (Becoming part of our tomb) Holding on to our past so tight (Losing its bloom) Like a fading dying light (Our impending doom)"
    * doskonale oddaje drogę w kierunku Entropii
* White Dragon "Nebula"
    * "Will I remember when it was alright? | When you could love us and we were together" ... "I keep wondering if you understand what I say | If you know who I am, if you know who we are"
    * "Watch me, hear me, I'm still here | Are you proud of what I've become? | Touch me, hug me, feel my soul | Although I know you won't understand my song"
    * bardzo smutna piosenka, pokazująca zniszczenia dokonywane w umyśle człowieka przez postępującą Entropię


### 7. Reprezentacja graficzna

![Interis, rysunek](mats/interis/interis-1.webp)

![Interis, rysunek](mats/interis/interis-2.webp)
