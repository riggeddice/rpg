## Energia

### 1. Skrótowo

* **nazwa**: Ixion
* **tron**: Saitaer
* **ofiara / Skażeniec**: Ixient
* **slogan**: ???
* **alternatywne nazwy**: 
    * ?
* **idea**: maksymalna egzystencja gdziekolwiek: odrzuć wszystko co nie jest niezbędne
* **wizja**: jak długo jest choć cień energii, jakaś forma ludzkości się zaadaptowała i prosperuje dalej
* **symbolika**: morski; ???

### 2. Lokalizacja w Panteonie

.

* "Możesz Wygrać z Nihilusem"
* "Możesz Spowolnić Nihilusa"
* "Możesz Zaakceptować Nihilusa"
* Kolory inne
    * Interis, pustka wszelkich znaczeń

### 3. Wyjaśnienie energii

* **Tron**: Saitaer
* **Dominujący kolor**: Adaptacja / Przekształcenie
* **Kolory wspomagające**
    * Poświęcenie: "Wszystko co nie jest niezbędne musi być usunięte by móc ewoluować i się rozwijać dalej w istotnych kierunkach. Odrzuć niezbędne i odrzuć niepotrzebny smutek."
    * Transorganika: "Mechanizmy się psują. Biologia jest delikatna i nie poradzi sobie w próżni. Ale połączenie obu - transorganika - jest uosobieniem perfekcji w świecie magii."

"Ixion" to najbliższa fonetycznie nazwa energii pochodzącej ze świata **Saitaera**. Saitaer jest pasożytniczym bóstwem, który w roku 79 wdarł się na Primus i objął tron Adaptacji, dodając do niej aspekt Transorganiki. Stąd słowotwórcza nazwa energii jest inna niż większości innych energii.






Po nocy przychodzi dzień. Śmierć jednej istoty daje życie innym. Ale nie w wypadku Interis. Prawdziwa pustka pochłania coraz więcej, nie pozostawiając niczego. Wymazuje rzeczy z egzystencji sprawiając, że nigdy nie istniały. Nie ma energii. Nie ma życia. Nie ma radości, bólu ani cierpienia. Nie ma _niczego_. Jest jedynie perfekcyjna równowaga, harmonia nicości, pustka wszelkich znaczeń. Ostateczne zwycięstwo entropii, energii Interis i **Nihilusa**.

Interis jest jak wieczna otchłań, w której każdy promień światła, każda myśl, każdy dźwięk zostają pochłonięte, nigdy nie mogąc się narodzić, jakby nigdy ich nie było. Gdy galaktyki w końcu zgasną, i nawet czarne dziury zgasną i zabraknie energii we wszechświecie - Interis doprowadzi do Harmonii Nicości.

Interis to jest to, co jest POMIĘDZY rzeczywistością, POMIĘDZY nierzeczywistością, perfekcyjny dekompozytor, absolutna symetria. Interis wymazuje byt z rzeczywistości, nie produkując energii. Wymazanie. Negacja. Unicestwienie. W pustce Interis nic nie ma już znaczenia. Entropia zawsze zwycięża i nic co zrobimy nie jest w stanie jej zatrzymać. Nasze czyny nie mają znaczenia. To jak próbować walczyć z tsunami lub z wybuchem wulkanu. Nie wiemy jak świat się zaczął, ale Nihilus go zakończy.

Nihilus, bóg Entropii przy Ogromie i Bezsilności, nie dba o nikogo i o nic. Nihilus nie zauważa niczego. Jest reprezentacją śmierci cieplnej wszechświata i końca rzeczywistości. Usunięcia 'znaczenia' i marzeń. Jest niepowstrzymany, nieubłagany, nie da się z nim negocjować. Nihilus to nie tylko kres wszystkiego, ale także świadectwo naszej świadomości bezsilności wobec nieuniknionego. Reprezentuje apogeum końca, ostateczną granica egzystencji i Wielkie Wymazanie.

Co ważne, Interis i Nihilus się nie spieszą. To najbardziej 'cierpliwa' energia. I tak wszystko się skończy.

### 4. Wizja Świata z dominacją tej Energii

.

Przy pełnej dominacji Interis nie będzie _niczego_. Ale pozwolę sobie pokazać rzeczywistość, gdzie jest jeszcze coś więcej niż tylko Interis, by pokazać w którą stronę to idzie.

Rzeczywistość stała się pustą, bezkresną otchłanią, gdzie każdy dźwięk, każda myśl, każdy kolor zostają wchłoniętę przez absolutną ciszę i ciemność. Wszystko, co kiedykolwiek znaliśmy - emocje, wspomnienia, pragnienia - zostaje wyssane. Zamiast tego pozostaje jedynie nicość, rozciągająca się w nieskończoność.

Najsilniej odbite miasta w Eterze Nieskończonym stają się fragmentami pustych cieni, rozciągających się w Ostatecznej Pustce. Nie ma dźwięków, zapachów ani ruchu. Nie ma marnowania energii - w ogóle nie ma energii. Co więcej, nie ma też znaczeń, nie istnieją żadne pojęcia. 'Przyjaźń', 'złość' - wszystko zaniknęło i zostało usunięte nawet z komunikacji czy z serc cieni, które kiedyś były ludźmi.

Wszystko, co kiedykolwiek miało znaczenie, teraz znika w absolutnej pustce Interis.

Wracając do ludzi, ci najmocniej odbici w Eterze stracili swoją tożsamość i sens egzystencji. Stracili pamięć. Ich emocje, wspomnienia i myśli zaniknęły, a z nimi wszelkie pojęcie o tym, kim kiedykolwiek byli. Nie ma rodzin, więzów, ludzkich dzieł. Co więcej, nie ma też żalu czy tęsknoty za tym, co utraciliśmy. Jest tylko Pustka.

Sam koncept "istnienia" i "nieistnienia" zanika w nicości Interis.

Świat Interis jest miejscem, gdzie wszelkie znaczenie i kontekst ginie w absolutnej pustce.

### 5. Typowe manifestacje tej energii

.

Anomalie / efekty

* Usunięcie Czynności: ktoś nie jest w stanie czegoś zrobić (np. ruszać uszami, powiedzieć czegoś, popełnić samobójstwa) - nawet nie umie WYOBRAZIĆ sobie robienia tego
* Wymazanie Uczuć i Pamięci: Patrzysz na zdjęcie osoby ukochanej i jej nie rozpoznajesz (lub rozpoznajesz, ale nic nie czujesz)
* Wysysanie Motywacji: to nic nie zmieni, to jest bez znaczenia, to nie ma sensu, nic nie zrobimy, jesteśmy za 'mali'
* Absolutne Zniszczenie: Interis to najbardziej destruktywna forma energii
* Zrozumienie Prawdy: "Jedyne, co nam zostaje to samobójstwo. Nic nie ma sensu."
* Rozpięcie z Rzeczywistością: "Nic nie istnieje, nic nie ma znaczenia, nic nie ma sensu". Katatonia, pusty śmiech, TERROR.
* Entropia postępuje. Rzeczy rdzewieją i się psują.
* Morale zanika. Nic nie cieszy. Brak woli życia. Leżysz i patrzysz.
* TERROR w obliczu Wielkości Nihilusa i Entropii.
* "Nic nie czuję!": nie czuje ciała, nie czuje bólu, nie czuje uczuć i pragnie poczuć ZANIM PRZESTANIE PRAGNĄĆ...
* Wymazanie czegoś z rzeczywistości: więcej niż tylko zniszczenie; nawet nie pamiętamy, że to kiedyś istniało
* Destrukcja magii / efektu magicznego: Interis zawsze wygrywa.

Przeciwnicy

* Terrorform: Ixient na podstawie człowieka albo zwierzaka, transorganizowany i zaprojektowany pod kątem walki. Dwa Terrorformy nie są do siebie podobne, acz zawsze są świetnie dostosowane do swojego otoczenia i celu.
* Prefektiss: 'doskonałość', pasożytniczy czerw transorganiczny wbijający się do ciała i przejmujący kontrolę nad mechanizmami i/lub ludźmi. Wbija się w centralny układ nerwowy. Prefektiss to KIEDYŚ był człowiek. Bardzo odporny.
* Nanofar: gra na 'nenufar', jest to bazowany na roślinach ixioński 'fabrykator / adapter / rekonstruktor' zdolny do samomaskowania i przekształcania rzeczy 'w sobie' w to czym powinny być z perspektywy ixiońskiej i sytuacyjnej.

### 6. Reprezentacja muzyczna

* Grendel "New Flesh"
    * "Cortex combined | The systems connected | Neural floodgates | The melding proceeds | Senses entwined | The merging of vision | Neural floodgates | The melding proceeds"
    * "The notion that nature can be calculated | Inevitably leads to the conclusion that | Humans too can be reduced to basic mechanical parts"
    * "(Devoid of a soul) | (Beginning of the new flesh) | (Unyielding corpse)"
    * Piękny obraz procesu Adaptacji. Z perspektywy ludzi, istoty Przekształcone mogą "nie mieć duszy", bo usunięto im rzeczy niepotrzebne by zastąpić je przydatnymi.
    * Z perspektywy Adaptacji oraz Oszczędności - człowiek jest tylko zbiorem komponentów, które Terrorform wybiera pod kątem celu. 
* Muse "Survival"
    * "Life's a race | And I am gonna win | Yes, I am gonna win | And I'll light the fuse | And I'll never lose | And I choose to survive | Whatever it takes"
    * "And I choose to survive | Whatever it takes | You won't pull ahead | 'Cause I'll keep up the pace | And I'll reveal my strength | To the whole human race"
    * Oddaje to myśl pod spodem ixionu - wieczna wojna, wieczna adaptacja, ZAWSZE najlepszy i zwycięski
* Dethklok: "Murmaider 2: The Water God"
    * "A master of the art of murder | Mermaids weep the blackened tears | So you swim to a sunken ship | Invited by a soul who bleeds from the lips | The prophet, who beckons you, wades in the dark" | "Speaks an ancient language, this language is of sharks"
    * "Your eyes have gone black |  You'll never look back | You'll never stop swimming | You'll always be tracked | Your life has transformed ..." "
    * Sama piosenka oddaje pięknie odrzucenie wszystkiego dla celu i transformację, nieważne jak okrutnie nie będziesz wyglądać potem i co się z Tobą nie stanie. Ixion nie zna piękna a silny pożera słabszego.

### 7. Reprezentacja graficzna

* [Furi, walka z drugim bossem](https://www.youtube.com/watch?v=_PRx_Xk98EE)
    * Da się USŁYSZEĆ w cudowny sposób ból w głosie krzyczącej istoty.
    * Jest tylko wola przetrwania i nienawiść - motywatory, które posuwają przeciwnika do przodu
    * Piękne połączenie mechanizmów i organiki
    * There is no humanity left. Just a purpose. Echa 'what had I become!' zanikają zdominowane przez CEL, którym jest powstrzymanie protagonisty.

![Ixion, rysunek](mats/ixion/ixion-1.png)

![Ixion, rysunek](mats/ixion/ixion-2.png)

![Ixion, rysunek](mats/ixion/ixion-3.png)
