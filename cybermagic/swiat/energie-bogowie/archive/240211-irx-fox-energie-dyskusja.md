* Fox: wyobraź sobie, agresywny gatunek, który porasta wszystko, co wejdzie w jego zakres temperaturowy i jeśli masz na łodzi masę krytyczną to się nie wynurzysz, póki ich nie zrzucisz z siebie
* Fox: i tak polują np na wieloryby
* Zolw: i masz nową sesję XD
* Fox: porastają, ściągają na dno i zjadają
* Zolw: 'sos z łodzi podwodnej, coś ich porwało' 'lecimy ich ratować' 'OMG MAMY GRZYBY NA KADŁUBIE WTF'
* Fox: tak xD
* Zolw: sesja typu GRZYB-ORROR
* Fox: najgorzej, bo nurek wychodząc też jest porastany
* Zolw: yup
* Zolw: "poddaj się potędze jedynej godnej inkarnacji plechy - Mrocznej Grzybni!"
* Fox: co to by było? Grzybowa inkarnacja esuriit?
* Zolw: niekoniecznie. Ta forma ekspresji niekoniecznie jest powiązana z głodem i nienawiścią tbh. Pierwsze skojarzenie - ixion (adaptacja do środowiska, modyfikacja własnej struktury). Drugie - anteclis (ucieczka-przed-extinction, eksploracja, szybsze przesunięcie i przekształcenie sposobu działania). Trzecie - alteris. Nawet exemplis by podeszło - grzyby to perfekcyjne mechanizmy do rozkładania na kawałki. Nie pasują tylko: praecis (władza nad otoczeniem, mistrzostwo > konstrukcja), fidetis, unumens, alucis, interis. Da się znaleźć wyjaśnienie dla sempitus i esuriit, ale to nie mój pierwszy wybór 
* Fox: Może interis
* Fox: Interis jest głodem bez emocji
* Zolw: każda istota ma w sobie elementy wszystkich Energii. Energie mają za zadanie spowodować wypaczenie - posunąć coś "do przodu", dalej niż powinno iść.
* Zolw: Interis jest "brakiem wszystkiego", nie jest "czymś"
* Fox: "Osiągnę masę krytyczną i finalnie i tak cię zjem"
* Zolw: hm, jak to pokazać...
* Zolw: to co mówisz to Ixion/Sempitus
* Zolw: a NAWET element Fidetis
* Zolw: popatrz: Ixion: "działam w otoczeniu w którym nie mogę, cokolwiek się nie dzieje to się dostosuję" Sempitus: "przetrwam wszystko, poszerzę, osiągnę masę krytyczną i cokolwiek nie zrobisz to Cię zjem (aspekt ekspansji)" Fidetis: "to nieuniknione, bo tak było i być powinno. Jesteś na tym terenie, zarażona przez nasze Grzyby i już wiemy jak to się skończy - zgodnie z naturalnym porządkiem Grzybni"
* Fox: kontra - działam w bardzo ograniczonym otoczeniu, nie mogę adaptować poza strefę, zmiana otoczenia mnie zabije
* Fox: ale wszystko w mojej strefie wpływów zostanie zjedzone
* Fox: wszystko co tu wpłynie finalnie zniknie
* Fox: okej, może fidetis by pasowało
* Fox: nieubłagany porządek natury
* Zolw: adaptacja do tego otoczenia. Ixion nie ma aspektu ekspansji a nie każda manifestacja Sempitus musi skupiać się na przestrzennej ekspansji. Jeśli chcesz to ograniczyć 'przestrzennie', zakoloruj to przez Fidetis właśnie
* Fox: dobra, widzę, ma sens
* Fox: ty, to z tego jest śmieszny wniosek
* Fox: naturalny predator to fidetis
* Fox: porządek natury i przeznaczenie jedzący i jedzoy
* Fox: każdy je i każdy zostanie zjedzony xd
* Zolw: tak, jak w Magic:the Gathering kolor Zielony (Nature, Harmony) oznacza 'naturalny porządek' i ma najgroźniejsze potwory
* Fox: mamy naszego boga natury
* Fox: tru
* Fox: łał xd
* Zolw: "Green already embraces the predator/prey relationships found in nature. Eat or be eaten. "
* Fox: wiesz żółw, ja NIE MIAŁAM POJĘCIA że to zmieści sie akurat w fidetis
* Fox: więc mam teraz moment olśnienia xd
* Zolw: ja też nie miałem pojęcia, bo zwyczajnie o tym nie myślałem ^^. Ale to pasuje. Fidetis (prawidłowe miejsce w egzystencji) lub Exemplis (amplifikacja naturalnych tendencji).
* Zolw: po prostu intuicyjnie "tu to pasuje"
* Fox: tak, najwyraźniej
* Zolw: zauważ że nie ma tu 'obojętności i pustki' Interis ("nic co robisz nie ma znaczenia, żadna łódź podwodna, żadne starania. In the end everything turns to rust and nothing will remember you have ever existed") ani nienawistnego głodu Esuriit
* Zolw: to po prostu 'naturalne drapieżniki', nietypowe grzyby, które PASUJĄ w tym miejscu bo ekosystem POTRZEBOWAŁ tego w tej niszy i w odpowiedzi Fidetis zamanifestowało ten typ padlino-rozkładającego drapieżnika
* Fox: yhym, dobra, ma sens
* Fox: To śmieszne, bo finalnie energie magiczne tak jak cały system działają na intencji nie sposobie
* Zolw: /bow
