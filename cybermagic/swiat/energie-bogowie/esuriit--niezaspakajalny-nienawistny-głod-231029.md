## Energia

### 1. Skrótowo

* **nazwa**: Esuriit
* **tron**: Omninemesis
* **ofiara / Skażeniec**: Esurient
* **slogan**: HUNGER UNENDING ("Głód bez końca")
* **alternatywne nazwy**:
    * nienawistny głód
    * nienasycalny ból
    * obsesja pożerająca wszystko
    * uzależniająca ambicja
* **idea**: nieskończony głód: niepohamowana ambicja korodująca duszę. Niekończąca się eksploatacja. Uzależnienie. Wypaczenie, monomania, obsesja, głód pożerający wszystko aż zostaje tylko większy głód.
* **wizja**: Omninemesis integruje w siebie Rzeczywistość w amalgamat świadomości sprzężonych w wiecznym pożeraniu siebie nawzajem, cierpiących i GŁODNYCH, niemożliwych do nasycenia, z nadzieją JUŻ PRAWIE, lecz nie do osiągnięcia
* **symbolika**: czerwień; ouroboros nienawiści i głodu

### 2. Lokalizacja w Panteonie

* "Możesz Wygrać z Nihilusem"
* "Możesz Spowolnić Nihilusa"
* "Możesz Zaakceptować Nihilusa"
* Kolory inne
    * Esuriit, niezaspakajalny nienawistny głód

### 3. Wyjaśnienie energii

* **Tron**: Omninemesis
* **Dominujący kolor**: Nieskończony Głód, Ambicja Bez Granic
* **Kolory wspomagające**
    * Nienawiść: To, co inni mają a ja nie mogę. Ukojenie, którego nigdy nie otrzymam. Ból i cierpienie którego nie mogę ugasić, ciągle pchający mnie ku pożeraniu. Oni TEŻ mają cierpieć! Jak ja!
    * Uzależnienie: Im więcej mam, tym więcej potrzebuje. Im więcej posiadam, tym bardziej muszę mieć więcej. W którymś momencie tracę wszystko próbując zdobyć KOLEJNĄ dawkę.

Esuriit jest inną energią niż pozostałe. Gdy Rzeczywistość Pękła, większość ludzkości (i nie tylko) wymarła. Wszystkie nie-optymistyczne grupy zostały zniszczone przez magię. Wszystkie kulty śmierci zostały pożarte. Wszelkie nadmiary populacji ludzkiej wpadło 'na żywca' w Eter Nieskończony. To sprawiło, że naturalna ludzka natura - ambicja - została Zmieniona. Tak na Primus promieniuje Esuriit - Wieczny, Niezaspokojony Głód w połączeniu z Nienawiścią, Cierpieniem i Żądzą Ekspansji. To sprawiło też, że sympatia powiązana z krwią i ogólnie rozumiana Magia Krwi stała się nieodwracalnie Zmieniona i stała się częścią Esuriit.

Czym zatem jest Esuriit? 

Wyobraź sobie dziecko, które nigdy nie znało miłości, ale widziało miłość dookoła. Nigdy nie otrzymało pomocnej dłoni. W dziecku budzi się głód - pragnienie tego, czego nie zaznało oraz nienawiść - czemu ono nie dostało tego co mają wszyscy inni, którzy się cieszą i śmieją w świetle. Nałóż na to jeszcze ból, zarówno fizyczny jak i 'duszy'. To jest zaczyn Esuriit. Esuriit to CIERPIENIE. Głód, niemożliwy do zaspokojenia. Nienawiść, niemożliwa do ukojenia. Cierpienie, które nigdy się nie skończy. Esuriit to wiecznie drgające, raniące siebie i wszystko inne siły, organiczne i drapieżne, raniące by zmniejszyć cierpienie własnej egzystencji.

To, co odróżnia Esuriit od wszelkich innych form energii - Esuriit sama w sobie nas NIENAWIDZI. Anti-life. Anti-world. Corruption beyond understanding. Ta energia pragnie zniszczyć wszystko co kochamy i połączyć nas w amalgamat głodu i cierpienia, niepohamowanej ambicji w ramach której wszyscy będą cierpieli tak samo mocno, ale każdy inaczej, w swój własny unikalny sposób. Esuriit, która została przekolorowana 'wpadnięciem sporej części ludzkości prosto w Eter Nieskończony' w coś potwornego posiada echo tego wszystkiego - ambicji, wielkości, niepohamowania, nienawiści. To sprawia, że jakkolwiek Esuriit nie jest _świadoma_, ale ma _agendę_.

Bogiem na tronie Esuriit jest **Omninemesis**. Niekształtna amalgamacja węży, głów i pochłoniętych istot, która pragnie wedrzeć się na Primus i wszystko połączyć z Esuriit.

Trzeba zrozumieć, że Esuriit nie jest tylko głodem _fizycznym_ ale też _duchowym_. Esurient żywi się nie tylko ciałem ofiar ale też - a nawet zwłaszcza - cierpieniem swojej ofiary. 

Wyobraźmy sobie ukochaną dotkniętą przez Esuriit. Najpierw zaczyna mieć podszepty i głód którego nie rozumie, próbuje z tym walczyć. Powoli impulsy ulegają przepisaniu. Zaczyna krzywdzić i ranić swojego partnera. Nie chodzi tylko o ból fizyczny, karmi się zwłaszcza bólem psychicznym i im bardziej się karmi tym bardziej jest głodna i tym bardziej wpada w pętlę Esuriit i tym bardziej boli i przypomina sobie kim była ale jest za daleko i za bardzo boli i nie ma tego jak zatrzymać i pojawia się nienawiść - na świat, na siebie, na wszystko...

Esuriit jest powiązana z Magią Krwi - "tanie i potężne" źródło energii które jednak zawsze kończy się w ten sam sposób. Istoty Primusa są wrażliwe na Esuriit, do poziomu "Esuriit zawsze wygrywa z magami i ludźmi".

### 4. Wizja Świata z dominacją tej Energii

Ostateczne zwycięstwo Esuriit zakończy się tym, że Omninemesis wleje się na Primus i połączy ze sobą wszystko co żyje, w jedną wielką pulsującą masę nigdy nie kończącego się głodu i nienawiści. Jednak zanim do tego dojdzie, świat będzie mieć obraz 'pośredni'.

Rzeczywistość Esuriit _nienawidzi_ ludzkości. Materia, energia, wszystko _nienawidzi_ ludzkości i pragnie naszego cierpienia. Nie śmierci - ale integracji z Esuriit. Śmierć jest prostą drogą ucieczki.

Wszelkie formy głodu - czy to ambicja, pragnienie, głód fizyczny czy uczucia - są zwielokrotnione i niemożliwe do zaspokojenia. I jednocześnie ofiara widzi, że TA INNA OSOBA ma to czego ofiara potrzebuje. Wieczne nienasycenie, wieczne cierpienie. To sprawia, że ludzie posuwają się do ekstremów z desperacji, a ta utrata człowieczeństwa i upodlenie jedynie 'cieszy' Esuriit i nasila głód i pożądanie. Nie ma ukojenia. Jest tylko głód, cierpienie i narastające poczucie krzywdy przerodzone w nienawiść.

Wszelkie formy życia uległy przekształceniu w terrorformy odzwierciedlające najmroczniejsze instynkty ich oryginalnych form. Zaniknęły wszelkie formy altruizmu i uczuć innych niż te pod kontrolą Esuriit. Wszystko jest niesamowicie agresywne i brutalne, w wiecznym _ruchu i głodzie_. Wszystko jest tak niesamowicie _organiczne i cielesne_, niewyobrażalnie _wilgotne i lepkie_. Jest tak strasznie _duszno_. I tak niewiarygodnie _BOLI_. A _ONI_ mają wszystko czego pragniesz!

Wszelkie ludzkie konstrukcje i dzieła stoją nieużywane, wykorzystywane jedynie jako prymitywne narzędzia do zadawania bólu i zniszczenia swego bliźniego.

Gdzieś 'pod spodem' terrorformów są uwięzione resztki ludzkiej natury orientującej się co się stało z naszym pięknym światem i pragnący krzyczeć w przerażeniu i z bólu. Ale nie ma możliwości wydania dźwięku. Nikt nie jest w stanie pomóc. Nie ma ukojenia.

Jest tylko maksymalizacja cierpienia, głodu i nienawiści.

### 5. Typowe manifestacje tej energii

Uwaga! Esuriit najłatwiej się Skazić czerpiąc energię magiczną z Dotkniętego terenu lub przez Sympatię Krwi ze wszystkich Energii.

Anomalie / efekty

* Dark Amplification: uzyskanie szczególnie dużej mocy / rozwiązania problemu, ale wprowadzenie Uzależniającej Ambicji do swojego ciała i sposobu działania, która zaczyna zmieniać Twoją percepcję i to czego pragniesz
* Obsesja, monomania: najczęściej pojawiające się efekty Esuriit. Agresywna fiksacja na czymś negatywnym / na pragnieniu, do którego dążysz za wszelką cenę.
* Głód Bez Końca: Niezależnie od tego, ile jest spożywane, głód nigdy nie jest zaspokajany. Ofiara idzie coraz bardziej ekstremalnie, pragnąć najmniejszego ukojenia, które w końcu otrzymuje - zadając większe cierpienie innym.
* Zniewolenie: ofiara jest aż uzależniona od swojej obsesji / obiektu, całkowite przepisanie cierpienia (daleko od obiektu) oraz monomanii (przy obiekcie). Agresywna monomania.
* Nienawistna Percepcja: zaczynają pojawiać się fałszywe wspomnienia czy postrzeganie rzeczywistości się zmienia. ONI mają to czego chcesz i się z Ciebie śmieją i Cię zdradzili i Ci to zabrali...
* Transfuzja Esuriit: praktycznie każdy byt Esuriit na podstawie ludzkiej jest w stanie zarazić inną osobę przez transfuzję odpowiedniej ilości krwi oraz uruchomienie żądz, cierpienia i nienawiści.
* Krwioczerwie: wnikają w ciało ofiary, namnażają się powodując ogromne cierpienie i przejmując wyższe funkcje po czym próbują wykluć się zostawiając ofiarę w złym stanie i wpełzają przez skórę do kolejnej ofiary.
* Klątwożyt Krwi: Co prawda klątwożyty dotykają magów z różnych energii, ale te z Esuriit są "najczęstsze". Zaraźliwe po polu magicznym.

Przeciwnicy

* Wendigo: niemożliwy do nasycenia głód oparty na kanibalizmie, obsesyjna koncentracja na sobie i swoich potrzebach, utracenie resztek człowieczeństwa, niezwykłe wzmocnienie cech fizycznych i karmienie się cierpieniem.
* Defiler: mag dotknięty Esuriit, który karmi się cierpieniem swoich ofiar i utracił już większość swoich ludzkich własności.
* Revenant: Esurient tak bardzo nienawidzący swojego mordercy, że rozerwał ciało i _powrócił_. Jest to byt "żywy", bardzo organiczny acz już nie ludzki. Mono-obsesja na punkcie ofiary i osób z jego krwi, z aurą nienawiści dookoła.
* Mandragora: wyrastająca na krwi umierających (lub martwych) ludzi, pragnie zemsty. Wrzask mandragory sprowadza ogromne cierpienie i kieruje ofiary ku szaleńczej obsesji.
* Vastifer: dewastator; straszliwy potwór wspomagany przez Esuriit, 'superniedźwiedź' pragnący zniszczyć wszystko i nienawidzący wszystkiego.
* Fambratus: kiedyś zwierzątko domowe lub dziecko, teraz wypaczone i przekształcone w eidolona nienawiści i głodu. "Pozostawione na śmierć i źle traktowane" mimo, że to się nigdy nie wydarzyło. Pragnie ukojenia.

### 6. Reprezentacja muzyczna

* Umbra Et Imago "Endorphin"
    * "Caught in a dark and sterile world | Life fading like a withered leaf" ... "And I see my own heart getting harder an colder | Endorphin deadens my senses, more and more"
    * "My blood boils with rage, full of euphoria | I feel this power in the air" ... "Ecstasy is spreading | I can't control my mind"
    * "My fingers are stained with blood | The guilt stains red on my hands | The shouts of the father | So yelling and agonizing | Probe into my brain | I'd like to change | But Endorphin deadens my senses"
    * Jedna z _najlepszych_ imo reprezentacji mroku Esuriit. Zaczyna się łagodniej, przechodzi w mrok zarówno tekstem jak i melodią.
* Iced Earth "Dracula"
    * "True love may come | Only once in a thousand lifetimes | I too have loved, they took her from me | I prayed for her soul | I prayed for her peace"
    * "Is this my reward | For serving God's own war? | The blood I've spilled up | For faith fulfilled | To damn her, a disgrace | You spit back in my face"
    * "There are far worse things | Awaiting man than death | Come taste what I have seen | I'm spreading my disease"
    * Ballada metalowa. Miłość przerodzona w korozję duszy. Prawdziwa nienawiść pochodzi ze zdrady a Esuriit doprowadza do pomieszania pamięci i utraty korzyści

### 7. Reprezentacja graficzna

Cytując coś podobnego z PoE: "horrific mass of organic beings melded together, and every one of those beings is pulling and clawing away eternally, trying to escape the pain and pursue even the smallest pleasure or consumption to distract themselves from the agony of their horrible existence". (https://www.pathofexile.com/forum/view-thread/3245186 )

![Esuriit, rysunek](mats/esuriit/esuriit-1.png)

![Esuriit, rysunek](mats/esuriit/esuriit-2.jpg)

![Esuriit, rysunek](mats/esuriit/esuriit-3.png)

![Esuriit, rysunek](mats/esuriit/esuriit-4.png)
