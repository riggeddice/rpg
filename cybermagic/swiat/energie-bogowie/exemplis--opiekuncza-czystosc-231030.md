## Energia

### 1. Skrótowo

* **nazwa**: Exemplis
* **tron**: Seilia
* **ofiara / Skażeniec**: Exemplient
* **slogan**: PROTECTION THROUGH PURITY
* **alternatywne nazwy**: 
    * opiekuńcza czystość
    * światło oczyszczenia
    * jesteś-tym-czym-zawsze-miałeś-być
    * intensywny blask ochrony
* **idea**: Intensyfikacja perfekcji ludzkości. Zachowanie idealnej idei i wypalenie wszystkiego nie pasującego światłem czystości. Opieka i ochrona.
* **wizja**: Ludzkość kroczy ku sukcesom, chroniona i bezpieczna przez swe dzieła. Zachowuje czystość formy i ducha w upiornym świecie magii. A wszystko co stoi na drodze ludzkości jest wypalone białym światłem.
* **symbolika**: biel; białe oczyszczające światło

### 2. Lokalizacja w Panteonie

* "Możesz Wygrać z Nihilusem"
* "Możesz Spowolnić Nihilusa"
* "Możesz Zaakceptować Nihilusa"
    * Exemplis, opiekuńcza czystość
* Kolory inne

### 3. Wyjaśnienie energii

* **Tron**: Seilia
* **Dominujący kolor**: Ochrona
* **Kolory wspomagające**
    * Oczyszczenie: "Zachowajmy to czym jesteśmy - czysta i perfekcyjna ludzkość. Usuńmy zbędne Skażenia i niewłaściwe wpływy. Zejdźmy do ludzkiej perfekcji."
    * Intensyfikacja: "Stać nas na więcej. Statek kosmiczny JESZCZE wytrzyma. Człowiek WYTRZYMA w polu magicznym. Jesteś w stanie osiągnąć więcej niż Ci się wydaje - będąc dokładnie tym czym jesteś."

Dzieci często boją się ciemności i uciekają do światła i pod opiekę rodziców. Tych rodziców, którzy są pierwszymi bohaterami dziecka i którym dzieci próbują dorównać. A z drugiej strony, rodzice patrząc na dzieci widzą nieskończony potencjał i perfekcję człowieczeństwa, pragnąć je ochronić i wypalić wszelkie 'złe' wpływy, pomóc im osiągnąć swój docelowy los. 

Ten właśnie koncept stoi za energią Exemplis. Ochrona, puryfikacja oraz intensyfikacja.

Świat jest strasznym miejscem, zwłaszcza po Pęknięciu Rzeczywistości. Anomalie i potwory żyją w mroku i przeciętny człowiek sam nie ma szans z upiornym wszechświatem. Exemplis reprezentuje światło Ochrony przez Intensyfikację (wzmocnienie naturalnych cech i sił: "Ty dasz sobie radę!") jak i Oczyszczenie (usuwanie negatywnych wpływów: "wypalam Cię światłem, wrócisz do normalności"). Energia puryfikacji, odpierania wpływów, czyszczenia i wzmacniania naturalnych elementów, metaforycznie reprezentuje dążenie do "igrzysk olimpijskich bez dopingu".

Technicznie, Exemplis jest energią wrogą wszelkim formom zmian podstawowego Wzoru czy formy (cyborgizacja, Skażenie) i służy przede wszystkim wzmocnieniu bytów zgodnie z ich 'naturalnym stożkiem rozwoju'. Np. karabin dotknięty Exemplis będzie mniej awaryjny, celniejszy itp. Jednocześnie Exemplis jest energią skupiającą się na (Ochronie - Zachowaniu - Konserwacji) tego co jest, więc jest wrogie wszelkiej formie "zmiany".

Boginią powiązaną z Exemplis jest **Seilia**, jeden z nielicznych przypadków w których udało się stworzyć _ideę_ bogini która pragnie chronić ludzkość i naturę tego co to znaczy "być człowiekiem" czy "chronić kreacje stworzone przez ludzi".

### 4. Wizja Świata z dominacją tej Energii

Rzeczywistość jest wypełniona białym światłem, które nie oślepia, lecz oczyszcza. Wszelkie formy anomalii są usunięte, wszelkie zakłócenia ludzkości i terragenów zostają usunięte. Ludzkość jest chroniona przez białe światło, maszyny działają tak dobrze jak to możliwe i ludzie są w stanie osiągnąć swój potencjał - _i nic innego i nic inaczej_. Zmiany toczą się w formie organicznej, zgodnie z 'naturalną' ewolucją techniki i ludzkości. Nie jesteśmy w stanie 'przekroczyć' naszego potencjału czy możliwości - białe światło chroni, oczyszcza, ale też ogranicza.

Ludzkość jest wszech-chroniona przez angelnet, zarówno przed bytami magicznymi jak i problemami naturalnymi. Nie zbliżysz się do klifu, bo angelnet postawi między Tobą a klifem barierkę ochronną. Nie jesteś w stanie się upić czy narkotyzować, bo białe światło wyczyści Cię z bycia 'pod wpływem'. Ochrona dominuje nad wolną wolą. Możesz czegoś pragnąć, wola nie jest Ci odebrana - ale nie możesz tego osiągnąć, bo Światło Oczyszczenia uniemożliwi Ci zrobienie sobie niewłaściwej krzywdy.

Miasta posiadają klasyczną różnorodność ludzkiej natury, ale są dostosowane do maksymalizacji ochrony i intensyfikacji swojej natury i swoich właścicieli. Wszelkie byty są bardzo mocno Odbite w Pryzmacie, acz ze skierowaniem pod kątem bezpieczeństwa. Mniej istotna jest efektywność, bardziej doskonałość esencji. 

Ludzie są "lepsi". Z jednej strony dążą do doskonałości i czystości, z drugiej - odebrane są im okazje do zatruwania swoich organizmów i "pójścia na skróty". W pewien sposób stają się najlepszą wersją siebie a konflikty między sobą mogą rozwiązywać w najbardziej okrutny sposób, bo angelnet i tak ich powstrzyma. Ciemną stroną tego świata jest: doskonałość bez celu, ochrona bez ukojenia, czystość bez powodu.

Dokładnie to samo dotyczy też natury. Najpiękniejsze kwiaty, najzdrowsze rośliny i najlepsze zwierzęta.

Zaznaczyć też warto, że rzeczy naturalne jak np. "ból" są też przeżywane intensywniej i mocniej, gdzie painkillery nie działają. Nigdzie w OCHRONIE nie ma słowa o UKOJENIU a przecież "cierpienie uszlachetnia".

Wszelkie odchylenia od świata Intensywnego Blasku Ochrony są wypalone przez Białe Światło Seilii.

WAŻNE:

Seilia jest wyjątkowo narażona na zmiany, związane ze światopoglądem wyznających ją wiernych, ale też ludzkości jako całość. 

W wypadku radykalizacji wiernych Seilii jej przykazania zradykalizują się wraz z nimi, wzmacniając aspekty puryfikacji i brak tolerancji dla wszelkich odchyłów od normy.

Jednak w wypadku rozszerzenia w świadomości ogółu społeczeństwa frazy "czysty człowiek" - człowiek bez skażenia, orginalna forma i wzór człowieczeństwa, również domena Seilii może się rozszerzyć, biorąc pod opiekę wszystkich, nazywanych tym terminem.

### 5. Typowe manifestacje tej energii

Anomalie / efekty

* Angelnet: infrastruktura bezpieczeństwa, zawierająca sensory, magitech ochronny itp. Nadzór oraz ochrona. Wzmocnienie możliwości ale też blokada niepożądanych akcji.
* Amplifikacja Pryzmatyczna: "ten statek kosmiczny ma jeszcze dość, poradzi sobie!". Czyli: coś, co nie powinno być w stanie czegoś osiągnąć (lub to zbyt ryzykowne) jest jeszcze w stanie to zrobić.
* Oczyszczające Światło: usunięcie efektów Skażenia i/lub elementów magicznych i ranienie istot głęboko magicznych.
* Intensyfikacja jednego wymiaru: (np. konkretnego uczucia, siły / odporności). Jest to wzmocnienie naturalnej cechy, ale jednej i niekoniecznie kontekstowo poprawne.
* Kokon Ochrony: perfekcyjna ochrona, ofiara jest zamknięta w środku i ulega ciągłej intensyfikacji i puryfikacji. A sam kokon jest Pryzmatycznie perfekcyjnym polem ochronnym.

Przeciwnicy

* Luminator: kot-Pacyfikator dotknięty Exemplis, który samoistnie poluje na anomalie i je czyści. Doskonały łowca, przesunięty do apogeum kociości i bardzo problematyczny dla magów.
* Cień Poświetlny: anomalia lub istota wypalona przez Seilię, reprezentująca to 'co zostało'. Szkieletowa konstrukcja, częściowo rzeczywista a częściowo w świecie idei, jako 'Żołnierz Światła'. 
* Anioł Światła: manifestacja angelnetu, usuwający wszystko co jest anomalne i co stoi przeciwko ideałom ludzkości. SOCOM-class Peacemaker. Puryfikacja wszystkiego.

### 6. Reprezentacja muzyczna

Nie jestem szczególnie zadowolony, ale nie potrafię znaleźć niczego _idealnego_.

* Thomas Bergersen "Children of the Sun"
    * "The fire in our eyes | The passion never dies | We're the chosen ones | The children of the sun"
    * "Flying too close to sun | As if we're invincible | Cannot dictate our faith | On this earth that we're living on | We are the children of the sun | The love for everyone | Always on our own"
    * Dzieci Seilii są samotne w świecie chronionym przez ich Strażniczkę. Są doskonałe, są niezwykle potężne i mogą osiągnąć bardzo wiele. Pasja, ochrona, usunięcie wszystkiego co nie jest nimi.
* Amaranthe "Archangel"
    * "We brood in a land that is struck by disease | A visage so vivid I see, but it's just so unreal" <-- reprezentuje PRZED pojawieniem się Seilii i Exemplis
    * "Archangel rise, the trinity has synchronized | A remedy for humankind" (...) From hell on earth to paradise" <-- reprezentuje bezwzględność Oczyszczenia
    * Dopóki choć jedna istota jest Nieczysta, Seilia działa dalej. Nieważne jak ciężka nie jest walka, Seilia. Działa. Dalej.

### 7. Reprezentacja graficzna

![Exemplis, rysunek](mats/exemplis/exemplis-1.png)

![Exemplis, rysunek](mats/exemplis/exemplis-2.png)

![Exemplis, rysunek](mats/exemplis/exemplis-3.png)
