## Energia

### 1. Skrótowo

* **nazwa**: Alucis
* **tron**: Arazille
* **ofiara / Skażeniec**: Alucient
* **slogan**: BEAUTIFUL DREAM ("Najpiękniejszy Sen")
* **alternatywne nazwy**:
    * ukojenie przez ułudę
    * najpiękniejszy sen
    * odbicie piękniejszego świata
    * kraina marzeń
* **idea**: Ucieczka od rzeczywistości do miejsca, gdzie wszystko jest idealne, lecz nic nie jest prawdziwe. Zaniknięcie pojęcia 'prawdziwość' czy 'cierpienie'. Jest tylko perfekcyjny sen.
* **wizja**: Nie ma cierpienia. Nie ma braku nadziei. Jest tylko przyjemny sen, wieczna iluzja i szczęście. Nawet jeśli Nihilus wszystko pochłonie, niczego nie poczujesz. Jest tylko Ukojenie, do końca.
* **symbolika**: jasny błękit; lustro odbijające wszystko co najpiękniejsze

### 2. Lokalizacja w Panteonie

* "Możesz Wygrać z Nihilusem"
* "Możesz Spowolnić Nihilusa"
* "Możesz Zaakceptować Nihilusa"
    * Alucis, najpiękniejszy sen
* Kolory inne

### 3. Wyjaśnienie energii

* **Tron**: Arazille
* **Dominujący kolor**: Ukojenie / Odpoczynek / 'Bliss'
* **Kolory wspomagające**
    * Sen / Marzenie: Ból i niedoskonałość rzeczywistości mogą rozpłynąć się w perfekcji snu dopasowego idealnie do Ciebie i Twoich potrzeb. Ból i cierpienie nie istnieją.
    * Piękno: Obezwładniające piękno, łzy płynące z oczu w obliczu nieogarnialnej perfekcji, doskonałość wizji niemożliwej do replikacji w 'świecie zewnętrznym'.
    * Wolność: Oddalenie od cierpienia, niemożność zmuszenia do czegokolwiek, nieskończona wszechstronność możliwości w przyjemnym, fikcyjnym świecie. 'Emigracja wewnętrzna'.

Ukojenie przez Ułudę, energia pięknego spokoju i odpoczynku, miejsce bezpieczeństwa i bycie oddalonym od tego co straszne i bolesne. Ucieczka od wszystkiego co złe.

Wyobraź sobie, że umiera bliska Ci osoba. Czujesz ogromne cierpienie. To jest _niesprawiedliwe_ i tak nie powinno być. Alucis jest odpowiedzią na ten bunt - przenosi Cię (mentalnie) do świata, w którym _tak nie jest_. Nie czujesz bólu, żalu, masz możliwość porozmawiać jeszcze raz... lub wiele razy z tymi, których Ci tak bardzo brakuje.

Alucis jest jak piękny sen, który nigdy się nie kończy. Stanowi manifestację ukojenia, pasywności i 'dreamy bliss'. Służy też do tworzenia bezpiecznych amnestyków, tworzenia fałszywych wspomnień i kuracji mentalnej i duszy. Kraina Marzeń nakładająca się na rzeczywistość. Pozwala też na budowanie fałszywych wspomnień i w dużym stopniu odpowiada za działanie magii mentalnej.

Jeżeli nic nie da się Ci odebrać, jeśli nie da się Cię skrzywdzić, nie da się Cię do niczego zmusić. To sprawia, że Alucis jest też energią uwalniającą niewolników - jest jedną z nielicznych rzeczy zdolnych do ukojenia Skażenia Esuriit. Nieważne jak okrutny nadzorca niewolników by nie był, Alucis sprawi, że okrucieństwo nadawane na ciało i układ nerwowy nigdy nie dotkną ofiary, żyjącej w wiecznym śnie piękna i ukojenia.

Alucis nie tylko usuwa cierpienie, ale również przynosi prawdziwą wolność w Krainie Marzeń. Ludzie są w stanie robić to, co kochają, i realizować swoje pasje bez żadnych przeszkód. Czują się niezwykle wolni, spełnieni i naprawdę żyją, chociaż fizycznie mogą być zanurzeni w głębokim śnie.

Boginią powiązaną z Alucis jest **Arazille**, kiedyś Harmonia Azalia Iluminacja Diakon. Bardzo popularna i bardzo szlachetna arcymag, robiąca wszystko by ratować szeroką populację ludzi. Długo zapewniała ochronę przed Skażeniem, długo ratowała ludzi i chroniła ich przed cierpieniem. Wiara ludzi sprawiła, że nieświadomie Harmonia Diakon objęła Tron Alucis i stała się Arazille, przestając być Harmonią Diakon.

### 4. Wizja Świata z dominacją tej Energii

Rzeczywistość zanika w śnie. Wszystko co żyje, 'zasnęło'. Ciała poruszają się automatycznie, ale umysły żyją w 'swoich światach', odrywając to, co odczuwają od tego, co się dzieje naprawdę. W rzeczywistości 'zanikają' instynkty, wszystko porusza się maksymalnie automatycznie. Za to w świecie snów każda istota żyje w "swojej fikcyjnej rzeczywistości".

W tym fikcyjnym świecie nie ma miejsca na ból, cierpienie czy niesprawiedliwość. Wszystko jest takie, jak być powinno, jednakże każdy widzi inny obraz – swój własny ideał.

Rzeka czasu płynie inaczej w świecie Alucis. Można by powiedzieć, że każdy ma swoje tempo, swoją ścieżkę, którą podąża. W wielu przypadkach ludzie przemieszczają się po świecie jak w transie. Ich ciała są tu i teraz, ale ich umysły są zanurzone w cudownych, indywidualnych światach. Przechodząc obok siebie, nie zauważają innych, ponieważ są zbyt pochłonięci własnym pięknym snem.

Miasta, które kiedyś były pełne zgiełku i chaosu, teraz są miejscami ciszy i spokoju. Budynki, choć wydają się opuszczone, są w rzeczywistości pełne ludzi, którzy snują się w swoich marzeniach. Część z nich doświadcza chwil ze swojej przeszłości (wyidealizowanych), podczas gdy inni odkrywają nieznane dotąd światy, które nigdy by nie mogły istnieć w fizycznej rzeczywistości.

Flora i fauna tego świata również doświadczają magii Alucis. Drzewa są pełne kolorów, które migoczą i zmieniają się w zależności od nastrojów przyrody. Zwierzęta, które kiedyś żyły w strachu przed drapieżnikami, teraz biegają razem, śpiąc w jednym legowisku. Wszystko jest możliwe w tym śnie.

Nie ma tu miejsca na katastrofy, wypadki czy nieprzewidziane tragedie. Wszystko, co jest niebezpieczne lub bolesne, zostaje odfiltrowane, zastąpione przez delikatne wspomnienie lub przyjemne doznanie. Gdy ktoś miałby potknąć się na ulicy, w rzeczywistości doświadcza uczucia unoszenia się w powietrzu. Śmierć i choroby są pojęciami nieznanymi. Wszelki dyskomfort, ból i negatywne emocje są natychmiast zastępowane uczuciem ukojenia i spokoju.

Ciemniejsza strona tego świata to fakt, że prawdziwe życie, z jego rzeczywistymi wyzwaniami, uczuciami i doświadczeniami, zostało zastąpione wiecznym, ale pustym snem.

### 5. Typowe manifestacje tej energii

Anomalie / efekty

* Łzy Szczęścia: w obliczu czegoś przepięknego lub anomalnego delikwent wpada w absolutny trans emocjonalny, 'krainę Arazille'. Zwykle połączone ze sztuką lub lustrem.
* Nieprzenikalna Tarcza Mentalna: cel "żyje w swoim świecie". Nie da się zmotywować, skrzywdzić, zranić, zabrać. Absolutny filtr. 
* Uwolnienie Z Kajdanów: usunięcie negatywnych efektów (uzależnienie, cierpienie, zaprogramowanie...) z ofiary, gdyż ofiara zaczyna żyć w swoim świecie i to wszystko nie ma znaczenia...
* Fikcyjna Rzeczywistość: postrzegasz rzeczywistość inaczej niż to czym ona naprawdę jest. Jako coś piękniejszego i lepszego.
* Najpiękniejsza Sztuka: w obliczu dzieła sztuki bariera między rzeczywistością a Krainą Snów jest bardzo niewielka.
* Echo Utraconych: bardzo kochane osoby mogą się zamanifestować jako perfekcyjne zjawy, jako halucynacje, działając jak byśmy się po nich spodziewali

Przeciwnicy

* Kuratorzy: roboty próbujące pojmać wszystkich ludzi i wsadzić ich w Aleksandrię, by spełniać ich marzenia i się nimi opiekować.
* Marzeniak: humanoid wyglądający jak człowiek, zwany też jako 'fabokl', otoczony aurą zakłócenia rzeczywistości i pozytywnych snów. Sprawia, że czujesz się radosny, błogi i uspokojony cokolwiek się nie dzieje. Piękny śpiew. Forma syreny?
* Pięknościarka: forma 'pielęgniarki' przyciągającej uwagę. Kusi ukojeniem i pięknym śpiewem, sprowadzając osoby do miejsc, gdzie mogą się położyć i odejść na zawsze w Krainę Snów. Emituje fale Ukojenia, co negatywnie wpływa na walkę z nimi.
* Lustro Pryzmatyczne: w wyniku działania sztuki, tak dużo energii odbija się w eterze że dochodzi do manifestacji Efemerydy odzwierciedlającej to, co pojawiło się w Krainie Snów.
* Poszukiwacze Ukojenia: ludzie, którzy zostali Dotknięci przez Arazille i pragną powrócić do Krainy Snów, na zawsze.

### 6. Reprezentacja muzyczna

* Noir "Secret Game"
    * "Come to me | We'll never be apart | The sun you see - it's me"
    * "No more pain | No memories remain | Now you can play with me"
    * "So love me now | You are the one | I'll give you all the stars I see | The rain is gone | No pain is here"
    * Najpotężniejsza reprezentacja Alucis. Świat bez cierpienia, wieczny sen pełen radości, zadowolenia i piękna.
* Hungry Lucy "Balloon Girl"
    * "Somewhere So far away She heard A little voice say | There's no reason To laugh or to sing There's no reason For anything"
    * "You have everything  You could ever want You have everything that you need They adore you They will follow you They will happily bow at your feet"
    * Niczego nie MUSISZ robić. Masz WSZYSTKO czego potrzebujesz. WYGRAŁAŚ. Jest tylko piękno i radość.
* My Little Pony "Come Little Children"?
    * https://www.youtube.com/watch?app=desktop&v=yxOcZ9gVLq8
    * "Come little children, l'll take thee away | Into the land of enchantment | Come little children, the time's come to play | In my garden of shadows"
    * "Follow, sweet children, I'll show thee the way | Through all the pain and the sorrows"
    * "Hush now dear children, it must be this way | Too weary of life and deceptions"
    * Alucis ZABIERA Cię i daje TOBIE ukojenie, pozostawiając po Tobie pustkę w świecie rzeczywistym. To fałszywa rzeczywistość.

### 7. Reprezentacja graficzna

![Alucis, rysunek](mats/alucis/alucis-1.jpg)

![Alucis, rysunek](mats/alucis/alucis-2.jpg)

![Alucis, rysunek](mats/alucis/alucis-3.png)

![Alucis, rysunek](mats/alucis/alucis-4.png)
