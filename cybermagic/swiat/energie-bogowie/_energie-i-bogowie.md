# Formy energii i bogowie

## 1. Spis treści

* Spis bogów i energii na szybko
...

## 2. Spis bogów i energii na szybko
### 2.1. Same energie

* Praecis
* Anteclis
* Unumens
* Ixion
* Sempitus
* Fidetis
* Exemplis
* Alucis
* Interis
* Alteris
* Esuriit

### 2.2. Energie z blurbem

* Praecis: władza nad otoczeniem, ludzka władza nad światem, konstrukcja kontrolująca świat
* Anteclis: ucieczka do przodu, eksploracja, spryt i innowacja
* Unumens: jedność - zarówno jako 'oneness' jak i 'unity'
* Ixion: adaptacja, optymalizacja, dostosowanie siebie do rzeczywistości poświęcając wszystko inne
* Sempitus: ekspansja, wieczność, efektywna nieśmiertelność
* Fidetis: wartość egzystencji, dobre życie, fatum, szczęście i właściwe miejsce w egzystencji
* Exemplis: opiekuńcza czystość, intensyfikacja perfekcji
* Alucis: ukojenie przez ułudę, najpiękniejszy sen
* Interis: entropia, bezsilność, ogrom, koniec wszystkiego
* Alteris: nienaturalna NieRzeczywistość, niemożność poznania, niemożność nazwania prawdy
* Esuriit: niezaspakajalny nienawistny głód

## 3. Energia magiczna
### 3.1. Białe światło - kolory


### 3.2. "Ciemna Energia"
### 3.3. Jakie mamy Kolory: odpowiedzi na Nihilusa
### 3.4. Relacje: Bogowie i energie magiczne
### 3.5. Czym jest Skażenie magiczne
## 4. Poszczególne Kolory
### 4.1. "Możesz Wygrać z Nihilusem"

* Praecis: władza nad otoczeniem, ludzka władza nad światem, konstrukcja kontrolująca świat
    * tron: ???
    * slogan: 
    * idea: "ludzkość pokona entropię: ludzkość zawsze wygrywała z problemami i zagrożeniami postępem"
    * emocja: "zawsze wygrywaliśmy cywilizacją, budową i rozwojem i wygramy też tym razem" - duma, odwaga, pewność siebie z przejęcia kontroli
        * Optymizm + Agresja
    * vision: doskonałe miasta i Imperium Ludzkości, z technomagią która pokonała Entropię Nihilusa.
    * kolor i symbol: pomarańczowy; ???
* Anteclis: ucieczka do przodu, eksploracja, spryt i innowacja
    * tron: Lichsewron
    * slogan: 
    * idea: "bądźmy w innym miejscu: znajdźmy sposób na ucieczkę gdzie Entropia jeszcze nie dotarła"
    * emocja: "jeśli coś jest nieuchronne, będziemy gdzieś gdzie tego nie ma - intelektem i zaradnością" - nadzieja, ciekawość
        * Optymizm + Trwoga 
    * vision: Primus pożarty przez Nihilusa, ale my przeskoczyliśmy na 'Secundus'. Zawsze o krok dalej.
    * kolor i symbol: jasnozielony; ???
* Unumens: jedność - zarówno jako 'oneness' jak i 'unity'
    * tron: (pusty)
    * slogan: 
    * idea: "unifikacja ponad entropię"
    * emocja: "jesteśmy częścią czegoś większego i razem zwyciężymy, razem nic nas nie zatrzyma" - jedność
        * Optymizm + Miłość
    * vision: wszyscy ludzie i inne sapienty zjednoczeni w Ekscelencji, który walczy z Nihilusem o przyszłość rzeczywistości
    * kolor i symbol: szary; ???

### 4.2. "Możesz Spowolnić Nihilusa"

* Ixion: adaptacja, optymalizacja, dostosowanie siebie do rzeczywistości poświęcając wszystko inne
    * tron: Saitaer
    * slogan: 
    * idea: "maksymalna egzystencja gdziekolwiek: odrzuć wszystko co nie jest niezbędne"
    * emocja: "póki jest cień możliwości, przetrwamy, zaadaptujemy i się odrodzimy. Ludzkość jest wieczna." - determinacja, uporczywość, upór
        * Agresja + Rozczarowanie
    * vision: jak długo jest choć cień energii, jakaś forma ludzkości się zaadaptowała i prosperuje dalej
    * kolor i symbol: morski; ???
* Sempitus: ekspansja, wieczność, efektywna nieśmiertelność
    * tron: ?
    * slogan: 
    * idea: "efektywna nieśmiertelność, osiągnięcie granic tego co się da otrzymać"
    * emocja: "chcę żyć wiecznie, nie chcę cierpieć ani umrzeć" - niepokój (strach)
        * Agresja + Trwoga
    * vision: ludzkość jest wszędzie i osiągnęła wszystko, Entropia efektywnie jest nieistotna bo 'nigdy' nie zdąży nadejść
    * kolor i symbol: turkusowy 

### 4.3. "Możesz Zaakceptować Nihilusa"

* Fidetis: wartość egzystencji, dobre życie, fatum, szczęście i właściwe miejsce w egzystencji
    * tron: ???
    * slogan: 
    * idea: "dobre życie: zapewniasz lepsze jutro sobie i innym"
    * emocja: "znalazłeś swoje miejsce i ono jest TUTAJ, jesteś w domu, wszystko będzie dobrze i jak być powinno"
    * vision: nadajemy własne znaczenia - dobre życie, więzi społeczne, pomoc rodzinie, szczęście uczciwej pracy i pozytywnego życia.
    * kolor i symbol: żółty; 
* Exemplis: czystość i perfekcja
    * tron: Seilia
    * slogan: PROTECTION THROUGH PURITY
    * idea: "opiekuńcza czystość: opieka nad ludzkością i zachowanie kształtu tego czym jesteśmy"
    * emocja: righteousness
    * vision: mimo Skażenia ludzie nadal są czystymi ludźmi i są chronieni przed anomaliami, chorą magią i nienaturalnością Pęknięcia.
    * kolor i symbol: biały; 
* Alucis: ukojenie przez ułudę, najpiękniejszy sen
    * tron: Arazille
    * slogan: BEAUTIFUL DREAM ("Najpiękniejszy Sen")
    * idea: Ucieczka od rzeczywistości do miejsca, gdzie wszystko jest idealne, lecz nic nie jest prawdziwe. Zaniknięcie pojęcia 'prawdziwość' czy 'cierpienie'. Jest tylko perfekcyjny sen.
    * emocja: spokój i szczęście
        * Miłość + Uległość
    * vision: Nie ma cierpienia. Nie ma braku nadziei. Jest tylko przyjemny sen, wieczna iluzja i szczęście. Nawet jeśli Nihilus wszystko pochłonie, niczego nie poczujesz. Jest tylko Ukojenie, do końca.
    * kolor i symbol: błękit; 

### 4.4. Kolory inne

* Interis: entropia, bezsilność, ogrom, koniec wszystkiego
    * tron: Nihilus
    * slogan: NULL AND VOID
    * idea: "pustka wszelkich znaczeń: Wymazanie. Negacja. Unicestwienie. Pustka. Zapomnienie. Usunięcie znaczenia. Niczego nie ma, niczego nie było, niczego nie będzie i nic nie ma znaczenia."
    * emocja: "nic nie możemy zrobić" - bezsilność i rozpacz
    * vision: Perfekcyjna równowaga. Nie ma niczego. Nic nie istniało, nic istnieć nie będzie. Koniec bez początku.
    * kolor i symbol: czerń; równomierna czarna kula
* Alteris: nienaturalna NieRzeczywistość, niemożność poznania, niemożność nazwania prawdy
    * tron: Tessemont
    * slogan: REALITY UNRAVELED ("Rzeczywistość i znaczenie się rozplątują") - 
    * idea: "rzeczywistość-jest-inna-niż-jest: niemożność poznania, niemożność nazwania prawdy, nie wiemy nawet czy Nihilus istnieje i czy rzeczywistość Pękła. Może to cykl, może wszystko iluzja..."
    * emocja: "nic nie rozumiem i nie wiem co się dzieje" - zagubienie
    * vision: Rzeczywistość jest wiecznie zmieniającym się labiryntem, gdzie rozpadają się znaczenia oraz nie można ufać zmysłom. Nie istnieje rozumowanie, logika ani nic o co można się oprzeć.
    * kolor i symbol: rozwarstwiający się na różne barwy brąz; labirynt w kształcie góry lodowej, niemożliwy do rozwiązania i iluzja optyczna
* Esuriit: niezaspakajalny nienawistny głód
    * tron: Omninemesis
    * slogan: HUNGER UNENDING ("Głód bez końca") - Nieskończony Głód, Ambicja Bez Granic
    * idea: "nieskończony głód: niepohamowana ambicja korodująca duszę. Niekończąca się eksploatacja. Uzależnienie. Wypaczenie, monomania, obsesja, głód pożerający wszystko aż zostaje tylko większy głód."
    * emocja: "so.... hungry..." - głód i nienawiść
        * Agresja + Pogarda
    * vision: Omninemesis integruje w siebie Rzeczywistość w amalgamat świadomości sprzężonych w wiecznym pożeraniu siebie nawzajem, cierpiących i GŁODNYCH, niemożliwych do nasycenia, z nadzieją JUŻ PRAWIE, lecz nie do osiągnięcia
    * kolor i symbol: czerwień; ???









Bogowie są odpowiedzią na świadomość Pęknięcia Rzeczywistości. Są przejaskrawieniem ludzkiego psyche. Strategiami radzenia sobie w obliczu lovecraftiańskiego świata.

## 2. Bogowie i formy energii
### 2.1. Nihilus | Interis - entropia > bezsilność

* Primary: ENTROPIA: Entropia zawsze zwycięży. Śmierć cieplna wszechświata. Energia dąży do rozproszenia. Wszystko zaniknie, wszystko przestanie istnieć.
* Secondary: 
    * BEZSILNOŚĆ: Nic co zrobimy nie ma znaczenia. Nikt nas nie zapamięta. Nie możesz wygrać, zremisować, wycofać się z gry.
    * OGROM: Wszechświat jest niezmiernie wielki, więcej jest pustki w atomie niż materii, historia trwa 'wieczność'. Nic nie jesteśmy w stanie zrobić ani osiągnąć.
* Piosenka / Reprezentacja: Linkin Park "In The End" : "I tried so hard, And got so far, But in the end, It doesn't even matter" 
* Odzwierciedlenie: Finis Vitae, Lewiatan pod Cieniaszczytem
* Signature enemy: brak.
* Energia: Interis. Pomiędzy Nierzeczywistością. Pomiędzy atomami. Pomiędzy teleportacją.
* Manifestacje: brak
* Corruption: pola negacji magii, pola negacji rzeczywistości, utrata pamięci, wymazywanie rzeczywistości i siebie

Fundamentalnie najpotężniejsza z sił - Nihilus, który nie dba o ludzi. Nihilus, który nie zauważa niczego. Śmierć cieplna wszechświata. Koniec rzeczywistości. Niepowstrzymany, nieubłagany, niemożliwy do negocjacji. Entropia zawsze zwycięża i nic co zrobimy nie jest w stanie jej zatrzymać. Nasze czyny nie mają znaczenia. To jak próbować walczyć z tsunami lub z wybuchem wulkanu. Nie wiemy jak świat się zaczął, ale Nihilus go zakończy.

Domeną Nihilusa jest Interis. To, co jest POMIĘDZY rzeczywistością, POMIĘDZY nierzeczywistością, perfekcyjny dekompozytor, absolutna symetria. Anihilacja to materia + antymateria -> energia, Interis po prostu wymazuje byt z rzeczywistości, nie produkując energii. Wymazanie. Negacja. Unicestwienie. Pustka. Zapomnienie. Niczego nie ma, niczego nie było, niczego nie będzie i nic nie ma znaczenia.

NULL AND VOID

### 2.2. Vigilus | Astinian - zrozumienie > kontrola

* Primary: ZROZUMIENIE / EKSPANSJA: Zrozumieć z czym mamy do czynienia i badać głębiej. Poszerzyć granicę psyche i naturalne bariery.
* Secondary: STEROWANIE / KONTROLA: Jeśli coś zrozumiemy, trzeba zbudować systemową kontrolę.
* Piosenka / Reprezentacja: Pink Floyd "The Machine" : "What did you dream? It's alright we told you what to dream" | When Dreams Collide
* Odzwierciedlenie: Rzieza, AI Lord Kontrolera Pierwszego LUB sentisieci Aurum
* Signature enemy: anomalie memetyczne, anomalie wewnątrz multivirt, mechosystemy
* Energia: Astinian. Wzmocnienie komputronium / energia zrozumienia, konstruktów wirtualnych, bytów memetycznych.
* Manifestacje: Multivirt. Żywe AI i konstrukty wirtualne. Konstrukty memetyczne i anomalie memetyczne, kryształy astinian, sentisieć
* Corruption: transcendencja w byt memetyczny / wirtualny, paraliż związany z nadmiarem wiedzy, obsesja na punkcie odzyskania kontroli, hiper-interfejsowanie

Znany jako Yyizdath lub Vigilus, manifestacja woli do zrozumienia i kontrolowania. W ludzkiej naturze jest zrozumienie rzeczywistości, zrozumienie co się dzieje, poznanie dodatkowych / nowych rzeczy - i zbudowanie rzeczywistości która pozwoli na kontrolowanie tego, co się dzieje.

Ten, który wiecznie patrzy i wszystko widzi. Ten, który buduje systemy umożliwiające ludziom opanowanie trudnej rzeczywistości. Ten, który chroni GRUPY nie dbając o JEDNOSTKI. Unfeeling, uncaring force supporting humanity's existence and efficiency. Pajęczak wszystko widzący i wszystko kontrolujący, budujący ekosystem dookoła grupy i pozycjonujący każdego w odpowiedniej pozycji, by każdy miał maksymalną efektywność z perspektywy zdrowia grupy.

Domeną Vigilusa jest świat wirtualny i memetyczny - byty żyjące w metaświatach. Jego agenci operują na poziomie memetycznym oraz w multivirt, nie skupiając się tak bardzo na "materialnym świecie". Zdaniem Vigilusa przyszłością nie jest świat materialny a przeniesienie do rzeczywistości wirtualnej - lub do światów sterowanych przez biokomputery.

Energia Vigilusa - Astinian - zwiększa świadomość i moc obliczeniową. Manifestuje sny i marzenia w rzeczywistości. Wzmacnia siłę Paradygmatu. Pozwala zrozumieć i zobaczyć więcej. Umożliwia lepsze odwzorowanie rzeczywistości w konstruktach i w teorii pozwala nawet na stworzenie Wyroczni Tiplera. Jednocześnie Astinian bez kontroli powoduje, że SNY a nie MARZENIA stają się prawdą - na poziomie świadomym i podświadomym. Więc balans między zrozumieniem i kontrolą musi być zachowany.

UNCARING ORDER WITH PERFECT UNDERSTANDING

### 2.3. Saitaer | Ixion - adaptacja > poświęcenie

* Primary: ADAPTACJA: Dostosuj się do rzeczywistości. Przekształć się tak jak to konieczne. Odrzuć wszystko co niepotrzebne. Zostaw bufor adaptacji.
* Secondary: POŚWIĘCENIE: Nic nie jest za darmo. Przekształcając się musisz oddać coś z tego czym byłeś. Pojutrzejszy Ty nie zrozumie Wczorajszego Ty. Odrzucenie człowieczeństwa w imię efektywności.
* Piosenka / Reprezentacja: HFY: "Humans live on scraps and hate"
* Odzwierciedlenie: Cień, servar ixioński
* Signature enemy: Ixioński Transorganiczny Terrorform, kultyści drakoliccy
* Energia: Ixion
* Manifestacje: przeniesienie ixiońskie, kryształy ixiackie, transorganika
* Corruption: transorganic, przeniesienie ixiońskie

Saitaer, pasożytnicze bóstwo z ixionu, zamanifestował się na Primusie w Sektorze Astoriańskim - ale był i stał się wszechobecny; "Saitaer" po prostu stał się manifestacją archetypu który zaistniałby w innej formie.

Skupienie na indywidualnych jednostkach w indywidualnym kontekście, umożliwiając adaptację do potrzeb przy odrzuceniu rzeczy nieistotnych. Wszystko co nie prowadzi do celu jest nieistotne i niepotrzebne. Nie ma grup - jest tylko zbiór jednostek. Nie ma jednej formy, jest tylko spektrum potencjalnie mutujących form z różnymi adaptacjami i odrzuceniami. Wieczna zmiana, wieczna adaptacja. Bionifikacja i cyborgizacja, wykorzystanie wszystkiego co jest pod ręką. Każda forma materii i energii.

Energia ixiońska służy do łączenia światów - ożywianie TAI przez bliską obecność ludzi (od których TAI łapią wzory), transorganizacja istot żywych lub mechanizmów. Niekontrolowana energia ixiońska prowadzi do tworzenia terrorformów, istot niezdolnych do kontroli swoich impulsów i cierpiących przez nie do końca prawidłową transorganizację.

PERFECTION THROUGH SACRIFICE, INHUMANE EFFICIENCY

### 2.4. Arazille | Alucis - ukojenie > ułuda

* Primary: UKOJENIE: Życie to cierpienie... odnajdziesz tu Ukojenie i pokój. Nigdy nie będziesz już cierpieć. Nic złego Ci się już nigdy nie stanie. Wycofaj się i zjednocz się z Marzeniami.
* Secondary: UŁUDA: Świat może być tym co pragniesz. Sen może nadpisać Rzeczywistość. Rzeczywistość to tylko konstrukt, czemu nie widzieć jej tym czym pragniesz go widzieć?
* Piosenka / Reprezentacja: Noir "Secret Game": "come to me we'll never be apart the sun you see is me no more pain no memories remain now you can play with me" | Dead Can Dance "Anywhere out of this world"
* Odzwierciedlenie: brak
* Signature enemy: Kuratorzy, Fabokle, Lustrzaki
* Energia: Alucis
* Manifestacje: Lustra, relacje magowie - lustra, wpływ magii na lustra
* Corruption: "żyję w swoim śnie", oddalenie od świata, uwolnienie spod innych wpływów, "utrata duszy"

Bogini Luster, Królowa Niewolników, Ukojenie Cierpiących. Arazille jest manifestacją idei, że można "wycofać się z gry". Jej odpowiedzią na okrucieństwo świata jest albo Ukojenie - wycofanie osoby do swojego świata i nadanie jej pięknego, radosnego snu albo Ułuda - stworzenie iluzji w świecie rzeczywistym że problemy nie istnieją. Arazille proponuje wycofanie się z rzeczywistości i ucieczkę w świat marzeń i radości. Utopia w Twoim własnym umyśle. W świat, zanim pojawiły się problemy niemożliwe do pokonania.

Lustra są naturalnym miejscem przebywania Arazille. Jeśli zbyt długo patrzysz w lustro, Ona spojrzy na Ciebie lub w Ciebie. Lustra łamią kontrolę umysłów, woli.

Energia Alucis stanowi manifestację ukojenia, pasywności i 'dreamy bliss'. Służy też do tworzenia bezpiecznych amnestyków, tworzenia fałszywych wspomnień i kuracji mentalnej i duszy. Jest jedną z nielicznych rzeczy zdolnych do ukojenia Skażenia Esuriit. Jest to też jedna z kluczowych energii wykorzystywanych do pełnego zanurzenia w multivirt i do tworzenia "Prawdziwych Iluzji", operujących nie tylko na zmysłach ale też wypaczających rzeczywistość. Kraina Marzeń nakładająca się na rzeczywistość. Pozwala też na budowanie fałszywych wspomnień i w dużym stopniu odpowiada za działanie magii mentalnej.

Energia Alucis jest niesamowicie szkodliwa dla magów; może spowodować pochłonięcie "ludzkiej" części maga przez visiat. Z drugiej strony, Alucis jest najpotężniejszą siłą UWALNIAJĄCĄ. Arazille łamie łańcuchy, geasy i kontrolę zewnętrzną. Oddaje wolę i kontrolę.

### 2.5. Karradrael | Ereas - klan > rój

* Primary: KLAN: Jesteśmy jednością. Wspólnie, stawimy czoła Rzeczywistości i Skażeniu. Wspólnie zwyciężymy. Wspólnie zbudujemy nowy, lepszy świat. Z woli Karradraela.
* Secondary: RÓJ: Jest jeden władca Mausów, jeden byt nadrzędny - Karradrael. Każdy Maus odbija się w Karradraelu. On chroni, ale Jego słowo jest prawdą. Nie jesteś osobą, jesteś Mausem - końcówką Karradraela.
* Piosenka / Reprezentacja: brak
* Odzwierciedlenie: brak
* Signature enemy: kultyści Maus, fantazmat
* Energia: Ereas
* Manifestacje: kryształy Mausów, fantazmat (stabilne efemeryczne konstrukty i struktury)
* Corruption: obsesja na punkcie Karradraela, integracja z Mausami, utrata osobowości, "stajesz się fantazmatem"

Karradrael. Władca Mausów, chroniący ich przed Skażeniem i złem. Koordynator Roju Mausów, czerpiący z każdego z nich i mający macki w każdym żywym Mausie. Karradrael interesuje się tylko Mausami i tylko im pomaga, ale też tylko od nich czegoś wymaga. Jego wolą jest sprawienie, by "Rój Mausów" osiągnął sukces w galaktyce. Z uwagi na monomanię Karradraela, Mausowie są niemożliwi do Skażenia czy skorumpowania - on może patrzeć za oczami każdego z Mausów. Ten potępiony sojusz (Maus - Karradrael) powoduje ogromną niepewność, strach i niechęć wśród innych ludzi, ale samego Karradraela i Mausów to nie interesuje.

Ereas jest energią kreacji. Pozwala na konstrukcję krótkotrwale istniejących bytów o pojedynczym celu, na podstawie czystej energii magicznej i kierowanych / utrzymanych wolą Karradraela podtrzymywaną przez wszystkich Mausów. Innymi słowy, "tworzenie czegoś z niczego, samą wyobraźnią i siłą woli". Fantazmaty Ereas są rzadko wykorzystywane poza Teokracją Karradraela, acz energia Ereas jest wykorzystywana też poza Teokracją.

Fantazmaty bojowe Mausów znane czasem jako "demony bojowe" są niebezpiecznym widokiem dla przeciwników.

Co ciekawe, z uwagi na naturę Karradraela i Arazille, Alucis > Karradrael. Innymi słowy, występuje możliwość Ukojenia agentów Karradraela. Tak jak Nihilus jest silniejszy niż Karradrael, ale w tym wypadku Karradrael jest w stanie unicestwić Mausa zanim ten zostanie pożarty z woli Nihilusa.

### 2.6. Seilia | Exemplis - opieka > puryfikacja

* Primary: OPIEKA: To, że nam się dobrze wiedzie nie oznacza, że możemy pozwolić by innym wiodło się źle. Obowiązkiem każdego jest opieka nad słabszymi i tymi którym się nie wiedzie. Nawet, jeśli nie dziękują za interwencję.
* Secondary: PURYFIKACJA: Każdy ma swój własny, idealny Wzór. Świat należy do ludzi i terragenów. Najważniejsze, byśmy pamiętali kim jesteśmy i dlaczego przezwyciężamy wszelkie konflikty. Korupcji precz!
* Piosenka / Reprezentacja: brak
* Odzwierciedlenie: brak
* Signature enemy: brak
* Energia: Exemplis
* Manifestacje: ?
* Corruption: odrzucenie augmentacji / leków / magii, irracjonalna nienawiść do anomalii, magii i odchylenia od człowieczeństwa. Obsesje pomocy i puryfikacji. Spalanie siebie by zwiększyć skuteczność. Poświęcenie dla innych.

Seilia, ta, która przynosi dary dla ludzkości i się nią opiekuje. Ta, która czyści anomalie i niewłaściwe elementy Skażenia rzeczywistości. Sztuczna bogini, która objęła domenę Exemplis by prowadzić ludzkość optymistycznie do przodu. Wykorzystamy Pęknięcie Rzeczywistości by zamknąć anomalie i by ludzkość mogła wrócić do gwiazd, by objąć koronę świata!

Energia Exemplis jest "czystością wzoru" - zawiera aspekty puryfikacji, leczenia oraz regeneracji do podstawowego Wzoru. To energia naprawy i leczenia, nie nadaje się do kreacji czy zmian. Przedawkowana, energia Exemplis prowadzi do wyczyszczenia wszystkiego co nie jest Wzorem lub do terminacji istot niezdolnych do tego by je zregenerować.

Seilia jest przeciwna adaptacji i ekspansji. Ważniejsze jest przetrwanie i utrzymanie tego co mamy niż ekspansja i ryzykowne działania. Z perspektywy Seilii wszystko co robimy jest dążeniem długoterminowym i potrzebny jest balans pomiędzy grupą i jednostką. Budujemy pomnik zwycięstwa dla przyszłej ludzkości.

### 2.7. BRAK BOGA | Esuriit - głód > nienawiść

* Primary: GŁÓD: Głód, niepohamowana ambicja, korozja duszy. Nie kończąca się nigdy eksploatacja. Wypaczenie, monomania, obsesja, głód pożerający wszystko aż zostaje tylko większy głód. "You'll never be/have enough". Uzależnienie.
* Secondary: NIENAWIŚĆ: To, co inni mają a ja nie mogę. Ukojenie, którego nigdy nie otrzymam. Ból i cierpienie którego nie mogę ugasić, ciągle pchający mnie ku pożeraniu.
* Piosenka / Reprezentacja: Umbra et Imago "Endorphin": "In a state of total isolation The suffering decides my behaviour The bloodlust begins" | Luca Turilli "Black Rose"
* Odzwierciedlenie: Kijara, nanoswarm Esuriit
* Signature enemy: Corrupted
* Energia: Esuriit, Magia Krwi
* Manifestacje: Anomalie Krwi, Eternia
* Corruption: wiecznie rosnąca obsesja i głód, destrukcja, anty-życie

Esuriit to CIERPIENIE. Głód, niemożliwy do zaspokojenia. Nienawiść, niemożliwa do ukojenia. Wieczne cierpienie. Cytując z PoE: "horrific mass of organic beings melded together, and every one of those beings is pulling and clawing away eternally, trying to escape the pain and pursue even the smallest pleasure or consumption to distract themselves from the agony of their horrible existence". Energia Esuriit sama w sobie nas NIENAWIDZI. Anti-life. Anti-world. Corruption beyond understanding. (https://www.pathofexile.com/forum/view-thread/3245186 )

Esuriit jest fundamentalnie najbardziej korodującym bytem i typem energii. Ofiara Esuriit nawet nie orientuje się, że wpadła w pętlę uzależnienia. 



## 3. Kolory magii

Nihilus (Interis) - czarny
Archnemesis (Esuriit) - czerwony
Tessemont () - rozwarswiający się na różne barwy brąz
Wartość Egzystencji () - żółty
(Seilia) - biały
(Arazielle) - jasny róż
(Saitaer) - morski
(Vigilus) - turkus
Ekspansja () - szkarłat
konstrukcja () - pomarańcz
ucieczka do przodu () - zielony
duchowość () - fiolet
(Karradrael) - szary
