## Energia

### 1. Skrótowo

* **nazwa**: Alteris
* **tron**: Tessemont
* **ofiara / Skażeniec**: Alterient
* **slogan**: REALITY UNRAVELED ("Rzeczywistość i znaczenie się rozplątują")
* **alternatywne nazwy**: 
    * rzeczywistość-nie-jest-tym-czym-jest
    * Dotyk, którego nie poczujesz
    * Labirynt nie do przejścia
    * Zniekształcona zasłona
* **idea**: niemożność poznania, niemożność nazwania prawdy, nie wiemy nawet czy Nihilus istnieje i czy rzeczywistość Pękła. Może to cykl, może wszystko iluzja...
* **wizja**: Rzeczywistość jest wiecznie zmieniającym się labiryntem, gdzie rozpadają się znaczenia oraz nie można ufać zmysłom. Nie istnieje rozumowanie, logika ani nic o co można się oprzeć.
* **symbolika**: rozwarstwiający się na różne barwy brąz; labirynt w kształcie góry lodowej, niemożliwy do rozwiązania i iluzja optyczna

### 2. Lokalizacja w Panteonie

* "Możesz Wygrać z Nihilusem"
* "Możesz Spowolnić Nihilusa"
* "Możesz Zaakceptować Nihilusa"
* Kolory inne
    * Alteris, zniekształcenie rzeczywistości i poznania

### 3. Wyjaśnienie energii

* **Tron**: Tessemont
* **Dominujący kolor**: Zniekształcenie / NieRzeczywistość. 
* **Kolory wspomagające**
    * Kłamstwo: "Nie ma znaczenia, czy świat jest błędny czy prawdziwy, jeśli i tak nie jesteś w stanie odróżnić prawdy od fałszu."
    * Znaczenia: "Jesteśmy ograniczeni przez to, co potrafimy zrozumieć i przez koncepty w naszej głowie."

Widzimy ścianę, ale czy naprawdę jest ścianą? Może to tylko iluzja (lub mimik). A może to prawdziwa ściana. To my nadajemy znaczenia, na przykład tajnym przejściem odblokowanym przez przesunięcie fikcyjnej książki w biblioteczce. A dla kogoś, kto nie zna konceptu tajnego przejścia odblokowanego fikcyjną książką? Nigdy nie jest w stanie znaleźć przejścia.

Rzeczywistość jest niemożliwa do prawdziwego odkrycia - a nawet, gdyby była, nie mając odpowiedniej sieci Znaczeń nie jesteś w stanie pojąć tego, co odkryłeś. A Alteris reprezentuje wiecznie zmieniającą się rzeczywistość, to czym _ona nie jest i nie może być a jednak jest_, śpiew pomidora, który jest lalką, acz po spojrzeniu pod innym kątem jest spiralą zakrzywioną w czwartym wymiarze.

Alteris reprezentuje esencję wiecznie zmieniającej się rzeczywistości, jest tym, czym _rzeczywistość nie jest i nie może być a jednak jest_. Jest ostatecznym Zniekształceniem, zarówno materii, energii jak i na poziomie poznawczym. Tworzy byty niemożliwe i pozbawione sensu, acz trzymające się swoich groteskowych zasad. Najlepiej zrozumieć Alteris przez zrozumienie trójwymiarowej siatki hipersześcianu, lub jak z dwuwymiarowej siatki sześcianu powstaje sześcian. Tam jest logika, ale nie trzymająca się naszych prawideł i niemożliwa przez nas do zinterpretowania.

Bogiem powiązanym z Alteris jest **Tessemont**, czterowymiarowa góra lodowa będąca niemożliwym do rozwiązania labiryntem, niemożliwym do zrozumienia i pojęcia.

### 4. Wizja Świata z dominacją tej Energii

Rzeczywistość stała się skomplikowanym labiryntem zniekształceń i fałszu. Wszystko, co kiedyś uważaliśmy za pewne - fizyka, materia, koncepty takie jak 'miłość' czy 'wrogość' - teraz są niestabilne i niemożliwe do przewidzenia. Co gorsza, wszystko się cały czas zmienia i dla każdego wygląda _inaczej_. Co gorsza, w zależności od kąta spojrzenia rzeczywistość przekształca się jeszcze raz.

Miasta stają się wiecznie zmieniającymi się labiryntami o niemożliwych kształtach i kolorach, zwykle w kształcie spiralnym. Wielopoziomowe autostrady prowadzą donikąd lub kończą się w niemożliwych miejscach. Anomalie przestrzenne są na porządku dziennym.

Jeśli chodzi o ludzi; z uwagi na niestabilność zmysłów oraz pojęć nie wiesz, czy podchodząca do Ciebie osoba chce Cię skrzywdzić czy Ci pomóc. Niby 'decyzja nadal jest Twoja', ale nie masz żadnych wiarygodnych inputów. Ludzie mają odmienne pojęcia - 'ekspres do kawy' dla jednej osoby staje się znaczeniowo podobny do 'zwierzątko' a dla drugiej do 'arcywroga'. Dla jednej osoby ból jest 'cierpieniem', dla drugiej 'czymś śmiesznym'. I po 5 sekundach się to może zmienić. Lub nie. Powoduje to całkowity rozpad komunikacji.

Sam koncept 'prawdy' i 'kłamstwa' przestaje mieć znaczenie w wiecznie zmieniającej się rzeczywistości, z mutującymi Znaczeniami, z fałszywymi zmysłami.

Rzeczywistość staje się płynna, niemożliwa do przewidzenia i pełna niespodzianek. W tym świecie nic nie jest pewne, a granice rzeczywistości są nieuchwytne.

### 5. Typowe manifestacje tej energii

Anomalie / efekty

* Anomalia przestrzenna: zakrzywiająca i zniekształcająca przestrzeń, odległości itp.
* Używasz niewłaściwych słów i byty są 'znaczeniowo' podobne do czegoś innego (np. 'kot' jest koło 'ekspres do kawy') i zachowujesz się wobec tych bytów niewłaściwie i nie rozumiesz czemu inni robią inaczej
* Iluzje i kłamstwa, zakłócenie postrzegania
* To nie moja ręka: wrażenie obcości czy nienaturalności czegoś, co jest Twoją naturalną częścią
* Znane Przedmioty zachowują się w Nieznany sposób
* Coś pojawia się gdy na to nie patrzysz, widzisz to kątem oka, ale jak patrzysz - niczego nie ma
* Ogromne radiatory statku kosmicznego są jednocześnie pajęczą siecią a jak popatrzysz pod trochę innym kątem mają niemożliwą geometrię (efekt czwartego wymiaru)

Przeciwnicy

* Mimik: istota fałszująca to, czym jest i prezentująca się jako coś innego
* NieXXX: np. NiePies, czy NiePrzemek. Istota podobna do podstawowej, ale w jakiś sposób zdecydowanie _odmienna_ i nienaturalna

### 6. Reprezentacja muzyczna

* Avalanches "Frontier Psychiatrist"
    * https://www.youtube.com/watch?v=qLrnkK2YEcE 
        * nie muszę tłumaczyć czemu.
    * ważne: there is no MALICE in Alteris. Alteris nie próbuje nas ZNISZCZYĆ. Alteris po prostu JEST. Stąd 'klimat' tego teledysku / piosenki pasuje.
* Emilie Autumn "Time for Tea"
    * z uwagi na trudne do przewidzenia 'co będzie dalej' z perspektywy melodii
    * z uwagi na narastającą energię
    * "There was a little girl, who had a little curl | Right in the middle of her forehead | And when she was good, she was very, very good | But when she was bad, she was homicidal!"
    * "Hatchet... Check! | Scalpel... Check!" ... "It’s time for war! | It’s time for blood! | It’s time for TEA!"
* BATTLE BEAST - Master Of Illusion
    * https://www.youtube.com/watch?v=6sLSyTEXfCI 
    * "I'm the master of illusion | Welcome to my mad delusion | I'm the one who makes the rules | For the heroes and the fools | I can tear down angels from the sky | And make Mona Lisa cry"
    * "Reflections of the real fade | The stranger in the mirror remains | Can you hear the weeping violins | And cut the strings before the end begins?"
    * obejrzyjcie teledysk, zobaczycie bardziej agresywny wpływ Alteris - od 'faux-compel' po 'corruption of wish'; po prostu potraktujcie to dosłownie


### 7. Reprezentacja graficzna

![Alteris, rysunek](mats/alteris/alteris-1.webp)

![Alteris, rysunek](mats/alteris/alteris-2.webp)
