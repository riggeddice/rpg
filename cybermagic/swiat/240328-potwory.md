# Rody Aurum

## 1. Potwory




## 2. Konkretne potwory

### 2.1. KODOWANIE

C (ciało), U (umysł / intelekt / psychotronika), E (emocje / morale / kontrola magiczna), S (społeczne)

### 2.2. Glukszwajn

* .Glukszwajn
    * akcje: "psotna, przeszkadzajka", "teleportuje się", "poluje na magię", "eksploduje i ranteleportuje pod wpływem stresu"
    * siły: "destabilizator magii", "ranteleportacja"
    * defensywy: "niezwykle antymagiczna", "ranteleportacja"
    * słabości: "to tylko świnia"
    * zachowania: "szuka magii i ją pożera", "ciekawska, szczurzy"

Opis: świnka pożerająca magię, destabilizująca energię magiczną.

### 2.3. Verlenland

* .Podły Pajęczak Syrenowy
    * akcje: "wstrzykuję śluz", "rozkładam sieć", "punktowo niszczę pancerz śluzem", "przyzywanie ofiary śpiewem"
    * siły: "silny i żrący śluz", "niezwykle wytrzymała sieć", "labirynt korytarzy"
    * defensywy: "ekstremalna odporność toksyczna", "labirynt korytarzy", "nieruchomy jest niewidoczny"
    * słabości: "wrażliwy na wysokie temperatury", "wszystko jest łatwopalne i wybucha"
    * zachowania: "rozpinanie pułapek", "unieruchamianie ofiar", "przyzywanie ofiary śpiewem"
    * komentarze
        * wzorowany na Trianai::Klarvath
* .Spawner pod kontrolą Leniperii
    * akcje: "wystrzeliwuję Sieć Rozpaczy", "zamykam ofiarę w kokonie", "atakuję pnączomacką", "Oczy Terroru", "Chowam się w ścianie", "spawnuję Wojownika"
    * siły: "atak z tysiąca stron", "corrupting touch", "nieprawdopodobnie silny"
    * defensywy: "macki zasłaniają skałą", "nieciągły byt roślinny", "Interis chroni przed żywiołami"
    * słabości: "wolny", "nieciągły i ma jądro"
    * zachowania: "otacza przeciwnika", "corruption over destruction", "atakuje spod ziemi"
    * komentarze
        * wzorowany na Dweller From The Depths, Transformers G1
        * lokalizacja: sesja z młodą Arianną i Mścizębem
* .Plantspawned Earth Elemental
    * akcje: "wolno przeładowujący się strzał", "ranię szponem", "udaję kamień"
    * siły: "niewidoczny bez ruchu", "bardzo silny", "spojrzenie Koszmaru"
    * defensywy: "bardzo ciężki pancerz"
    * słabości: "wolny i toporny", "niezgrabny", "wolno strzela", "mało celny"
    * zachowania: "atakuję frontalnie", "atakuję z zaskoczenia"
    * komentarze
        * wzorowany na Dweller From The Depths zombies, Transformers G1
        * lokalizacja: sesja z młodą Arianną i Mścizębem
* .Strasznołabędź: Verlenowy potwór, który uległ Paradoksalizacji Uli
    * akcje: 'Jego cień paraliżuje', 'jego głos przeraża', 'emisja światła', 'wężoszyja z superdziobem', 'iluzje repozycjonujące', 'ppanc atak'
    * defensywy: 'pole deflekcji' (życie) (3), iluzje unikowe, szybki, zwrotny
    * słabości: 'brak AoE', 'głód mięsa i strachu'
* .Śluzowce
    * akcje: "kamufluje się w cieniu", "zaskakuje ofiarę", "pluje olejem", "przyjmuje formę zainfekowanego", "zmień formę"
    * siły: "mocny jak stal", "umiejętność kamuflażu", "zdolność przekształcania ofiar w zombie", "infekujący olej"
    * defensywy: "byt oparty na oleju", "wygląda jak ofiara", "zombifikowane ofiary jako osłona"
    * słabości: "krew jaszczurów tunelowych", "rozpuszczone ciało"
    * zachowania: "zaskakuj i zombifikuj", "korzystaj z osłony zombifikowanych", "wykorzystuje zombifikowanych jako pionki"
    * komentarze
        * sesja: ekologia jaszczurów w arachnoziemie
* .Crystaller
    * akcje: "imituję dowolny obiekt", "atakuję z zaskoczenia", "kryształowa trumna", "tworzę barierę", "wykorzystuję słabe kinetyczne punkty ofiary"
    * siły: "może naśladować dowolny kształt i formę", "ostry jak brzytwa", "chłonie krew ofiary"
    * defensywy: "kryształowa twardość", "niewykrywalny w formie obiektu", "przezroczysty i niemal niewidoczny"
    * słabości: "narażony na silne uderzenia"
    * zachowania: "atak z zaskoczenia", "chłonie krew ofiary"
    * komentarze
        * sesja: ekologia jaszczurów w arachnoziemie
* .myceliański rój
    * akcje: "rosnę niewidocznie", "znienacka zarastam", "rosnę w poprzek drogi", "przenikam przez najdrobniejsze szczeliny", "rosnę na plecach wroga", "korzeniuję w słaby punkt"
    * siły: "ekstremalnie szybkie wzrost", "może rosnąć w każdym miejscu", "przyczepność na poziomie komórkowym", "ostre jak brzytwa strzępki"
    * defensywy: "ekstremalnie szybkie wzrost", "byt nieciągły", "kamuflaż"
    * słabości: "narażony na działanie wysokich temperatur", "wrażliwość na suszę"
    * zachowania: "converge and amass"
    * komentarze
        * sesja: ekologia jaszczurów w arachnoziemie

### 2.3. Przeszkody / teren

* Labirynt Korytarzy
    * akcje: "zagubienie", "nie da się manewrować", "za ciasno by przejść", "atak od tyłu", "szczelina"
    * siły: "ryzyko zawału", "odcięcie od sojuszników", "spowolnienie ruchów bo ciasno", "fałszywe sygnały sensorów"
    * słabości: "stałe punkty orientacyjne"

