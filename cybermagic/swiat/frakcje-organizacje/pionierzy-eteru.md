---
layout: default
categories: faction
title: "Pionierzy Eteru"
---
# {{ page.title }}

## Lokalizacja

* Polska, tzw. Frontier (miejsca silnie Skażone lub styczne z epicentrami Eteru)

## Omówienie frakcji i zasobów

Ta frakcja jest bardzo luźną frakcją. Można nawet nazwać ich "luźną gildią", czyli grupą osób skupionych we wspólnym celu. To powoduje, że nie można jednoznacznie określić ich faktycznej siły czy zasobów.

Zidentyfikowano około 200 osób identyfikujących się z tą frakcją, z czego 50% stanowią viciniusy i inni transludzie.

* Przystanie
    * bezpieczne miejsca na Frontierach, ufortyfikowane jak cholera
    * zawierają ogłoszenia które są bezpieczniejsze niż 

## Model biznesowy

Pionierzy Eteru są raczej samowystarczalni. Handlują między sobą i sobie pomagają. Odpowiada im takie życie. Zwykle handlują artefaktami Eteru oraz informacją, zwłaszcza za żywność czy narzędzia. Pionierzy stanowią bufor między niestabilną nienormalnością Eteru i Skażenia a normalnym ludzkim światem.

## Główne powiązania

* Większość Rodów Magicznych wspiera Pionierów Eteru.
* Najsilniejszym sojusznikiem Pionierów jest Yyizdathcorp, który często wysyła nadmierne siły militarne mające wesprzeć Pionierów.

## Konstrukcja

### Koncepcja

#### Przyczyna Istnienia (czemu tu jesteśmy)

Eksploracja, Wolność, Samodzielność, Ochrona przed Eterem

#### Tożsamość (kim chcemy być)

* Swobodna grupa luźnych agentów robiąca to, co należy.
* Działamy stricte na Frontierze Eteru. Nie dla nas cywilizacja.
* Potrafimy poradzić sobie ze wszystkim co z Eteru przyjdzie - eksterianami, Skażeńcami itp.
* Wiemy, że jesteśmy ochroną ludzkości i wiemy, że ludzkość nas wspiera.

#### Długoterminowa Intencja (gdzie chcemy dotrzeć)

* Primus jest całkowicie odizolowany od zagrożeń ze strony Eteru; kontrolujemy chaos.
* Mamy przyczółki w Eterze Nieskończonym i na innych światach czy fazach do których prowadzi Eter.
* Dzięki nam doszło do ekspansji ludzkości przez świat - zawsze jest jakiś Frontier i my zawsze tam jesteśmy.
* Nikt nam nie rozkazuje, nikt nas nie kontroluje. My im pomagamy, oni zostawiają nas w spokoju.

### Natura

#### Kultura (jak robimy to, co robimy)

* Musimy sobie wszyscy pomagać, nie ma miejsca na zdradę czy politykę. Eter nie wybaczy.
* Każde z nas ma prawo do własnych decyzji. Musimy sobie pomagać, ale nie będziemy się kontrolować. Nikt nie jest królem.
* Szanujemy Eter. Wiemy, że nie jesteśmy w stanie go pokonać. Szanujemy jego zwyczaje i nie jesteśmy głupi. Są tu potwory jak mało.
* Ludzkość jest święta - nie zapominajmy, że robimy to dla naszych braci i sióstr.

#### Struktura (jak zorganizowaliśmy pracę)

* Grupa luźnych agentów bez żadnej władzy czy kontroli.
* Jeśli ktoś kontroluje jakiś teren, to ma nadrzędne prawo głosu nad wszystkimi.

### Wizja

#### Cele (Goals)

* Zapewnienie odpowiednich schronisk i placówek na Frontierze, by zawsze była bezpieczna przystań.
* Wzmacnianie systemu obronnego między Frontierem a cywilizacją. To, że jesteśmy po stronie Frontieru nic nie znaczy.
* Budowanie szlaków handlowych i przerzutowych z cywilizacją.

### Strategia (jak osiągamy Wizję i Naturę)

Ogólnie ciężko o strategię jak nie ma dowodzenia. Ale Przystanie pomagają:

* Komunikacja, bezpieczeństwo, handel - w każdej Przystani. Przystanie współpracują ze sobą.
    * Osoby samolubne, niechętne do pomocy innym nie otrzymają pomocy. Zasada lustra - jak Ty mnie, tak ja Tobie. Ogłoszenia w każdej Przystani.
    * Absolutny zakaz używania niektórych form magii (np. mentalna) na innych magów na obszarze Frontieru.
* Zasada nagród. Nagroda za zniszczenie Skażeńca, za handel, za przekazanie wiadomości...

### Kluczowe siły

* Magowie tej frakcji to twarda ekipa, bardzo trudna do pokonania czy zniewolenia.
* Dostęp do dziwnych kuriozów pochodzących ze Skażenia i Eteru jako pierwsi.
* Pionierzy traktowani są z sympatią ze strony innych magów - poza tymi chętnymi do kontroli.

### Kluczowe słabości

* To nie jest FRAKCJA technicznie rzecz biorąc. Grupa indywidualności.
* Brak dostępu do najsilniejszych laboratoriów czy zaawansowanych magitechów. Nie wyprodukują sami.
* Na Frontierze niektóre elementy magii zachowują się _inaczej_, co ogranicza możliwe magitechnologie.
* Są na pierwszej linii, więc mogą spotkać się nawet z Tytanem czy być wchłonięci przez Eter Nieskończony.

## Historia
