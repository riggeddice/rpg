---
layout: default
categories: faction
title: "Jakryju"
---
# {{ page.title }}

## Lokalizacja

* Polska, z uwagi na specyfikę firmy ich siedziba jest ruchoma.

## Omówienie frakcji i zasobów

Firma ma 30-40 osób, w czym ~5 magów (1 permanentnie na "Wielkiej Bajce").

* Sklepik Kuriozów
    * siedziba główna
    * potrafi inkarnować się w dowolnym miejscu 'for sale' na okres 2-3 dni
    * służy do sprzedaży artefaktów i bytów Eteru i kupowaniu opowieści i legend
* Biblioteki Legend i Opowieści
    * kontrolowana przez ród Weinerów; dostęp przez Krystiana
    * służy do sprawdzenia jaką siłę Pryzmatu mają zebrane opowieści by się nie wpakować
* Okręt "Wielka Bajka" w Eterze
    * zwykle w Porcie Mgieł, na Granicy Bałtyckiej
    * służy do inkarnacji Opowieści w artefakty i holokostki pamięci
    * służy do przenoszenia ekipy do Eteru
* Hiperkostka Pamięci
    * kontrolowana przez Julianę Maus; w Sklepiku Kuriozów
    * służy do budowania holokostek pamięci; magitechnika Mausów
* Dostęp do wsparcia ze strony trzech Rodów: Maus, Weiner, Zajcew
    * inwestorzy Zajcewów oraz "Wielka Bajka" w leasingu

## Model biznesowy

1. Sklepik Kuriozów pojawia się w losowym mieście i sprzedaje Artefakty Eteru i inne rzeczy za cenne rzeczy lub opowieści
2. Kupione opowieści są weryfikowane w Bibliotekach Opowieści Weinerów czy nie są zbyt Skażone Pryzmatem; jeśli są korzystne lub neutralne Pryzmatycznie to:
3. Opowieść jest przerabiana przez bardów na coś, co da się zmonetyzować.
4. Okręt "Wielka Bajka" płynie przez Eter, by wywołać tą Opowieść.
    1. "Wielka Bajka" broni się przed zagrożeniami ze strony Eteru
    2. Harvestowane są artefakty i cenne rzeczy powstałe przez odbicie w Eterze
    3. Przez odpowiednie punkty widzenia wydarzenia są przeżywane i obserwowane
5. Już w porcie, artefakty są dostosowywane do sprzedaży i przeżycia są przekształcane w holokostki pamięci
6. To, co się uda wykorzystać czy uratować trafia do Sklepika Kuriozów...

## Główne powiązania

* Janusz Zajcew
* Krystian Weiner
* Juliana Maus

## Konstrukcja

### Koncepcja

#### Przyczyna Istnienia (czemu tu jesteśmy)

Eksploracja, przygoda, dostęp do dziwów tego świata.

#### Tożsamość (kim chcemy być)

* Niewielka grupka przyjaciół robiąca biznes po swojemu.
* Na Frontierze Eteru, nie nudna i bezpieczna posadka.
* Prawdziwe życie, eksploracja i dostęp do dziwnych ciekawych rzeczy.

#### Długoterminowa Intencja (gdzie chcemy dotrzeć)

* "Wielka Bajka" stanie się pancernikiem eterowym zdolnym do podjęcia walki z Lewiatanem
* Mamy dostęp do bardzo ciekawych artefaktów i dziwów; jesteśmy głównym dostawcą dla ważnych kolekcjonerów i nauki
* Do nas trafiają najciekawsze opowieści z nadzieją, że wygenerujemy z nich prawdziwe korzyści

### Natura

#### Kultura (jak robimy to, co robimy)

* Przyjaźń, współpraca, pomagamy sobie - Eter nie wybaczy zdrady czy złych emocji
* Pracujemy bo to dochodowe oraz korzystne. Nie każde zlecenie przyjmujemy - musimy w nie wierzyć.
* Jesteśmy dobrymi fachowcami i takiego zachowania oczekujemy od klientów.

#### Struktura (jak zorganizowaliśmy pracę)

* Dowodzi triumwirat: Janusz, Krystian, Juliana.
* Zasobami Rodów zarządza osoba z danego rodu.
* Okrętem dowodzi Janusz, bo tylko Zajcewowie byli na tyle szaleni by zainwestować.

### Wizja

#### Cele (Goals)

* Rozbudowa i wzmocnienie "Wielkiej Bajki" o coraz to nowsze moduły i defensywę.
* Rozbudowa "Sklepiku Kuriozów" by mógł bezpiecznie poruszać się po Polsce mimo posiadanych kuriozów.
* Dotarcie do odpowiedniej ilości potencjalnych klientów (naukowcy, kolekcjonerzy).
* Zapewnienie lepszego przerobu dziwnych artefaktów z Eteru, Opowieści itp.
* Zbudowanie rozpoznawalności marki Jakryju dla lepszych opowieści, szacunku, kontaktu itp.

### Strategia (jak osiągamy Wizję i Naturę)

* Większość przychodów reinwestujemy w "Wielką Bajkę" i paliwo dla "Sklepiku Kuriozów"
* Zapewnienie pozwoleń dla "Sklepiku Kuriozów" by mógł inkarnować się w odpowiednich miejscach.
* Opłacamy odpowiednie siły w Rodach magów oraz wśród odpowiednich zarządców terenu.
* Gdy inkarnujemy się przypadkowo w niewłaściwym miejscu (nie kontrolujemy tego), uciekamy lub się opłacamy.
* Stymulujemy powstające opowieści przez robienie konkursów zwłaszcza celowane do najmłodszych.

### Kluczowe siły

* Dostęp do magów z trzech różnych Rodów które są zasadniczo przyjazne tej inicjatywie
* Połapany model biznesowy oraz główne artefakty które faktycznie działają
* "Wielka Bajka", okręt bojowy Eteru.

### Kluczowe słabości

* Każde ogniwo tego łańcucha jest potencjalnie niezastąpione.
* Pozwolenia i aspekty prawne tego biznesu są lekko problematyczne.
* Brak kluczowej kompetencji wyceniania artefaktów Eteru w ramach Jakryju.
* "Wielka Bajka" znajduje się na Granicy Bałtyckiej między Eterem a Bałtykiem. Jest narażona.
* Niezadowolenie ze strony potężnych Zajcewów może odebrać "Wielką Bajkę".

## Historia
