---
layout: post
categories: bloodline
title: "Perikas - kontrola chaosu narzędziami"
---

# {{ page.title }}

## Skrót kultury

### Ogólnie

Eksploratorzy, odkrywcy, nastawieni na ryzyko twórcy narzędzi i pragmatyczni budowniczowie przyszłości. Ród Perikas został zaprojektowany by Ludzkość mogła przetrwać w świecie dotkniętym przez Eter. By móc zachować człowieczeństwo i wykorzystywać narzędzia do kontroli chaosu.

Pionierzy, Eksploratorzy, osoby zdolne do radzenia sobie w warunkach niekorzystnych i budujące cywilizację w świecie chaosu.

### Główne wartości

Wartości:

* **Perikas** odważnie eksploruje Eter i eksploatuje go dla dobra Ludzkości.
* **Perikas** wykorzystuje Eter jako narzędzie do postępu Ludzkości. Eter przeciwko Eterowi.
* **Perikas** stoi po stronie Ludzi, nigdy po stronie Eteru, Eksterian czy anomalii.

Zwycięstwo i porażka:

* **Perikas** zwycięża, jeśli Ludzkość zdobywa przyczółek w Eterze i jeśli idziemy o krok dalej technomagicznie.
* **Perikas** przegrywa, jeśli Ludzkość musi zaadaptować się do Eteru by móc funkcjonować w tym świecie

### Sztuka

Sztuka rodu Perikas to najczęściej upiększanie narzędzi, "porządna krasnoludzka robota". Z uwagi na unikanie tematów okołoPryzmatycznych, skupiają się na dobrej robocie i pięknie dobrej roboty a nie na sztuce dla sztuki.

Nie zmienia to faktu, że magowie Perikas lubią obcować z pięknem - ale jeszcze bardziej lubią być bezpieczni. Ich konstrukcje są bardzo utylitarne i surowe, ale mają tą mistrzowską elegancję i dyskretne zdobienia.

Trochę jak programiści dumni ze swojego kodu, czy mistrzowie ze swoich dzieł.

## Ulubiony teren

Ród Perikas zazwyczaj czuje się najlepiej w pobliżu wody - rzeki, mórz oraz jezior. Miejsca, w którym jest wysoki poziom Skażenia magicznego. Miejsca, w których silnie przebija się Eter.

Po osiągnięciu takiego terenu, magowie rodu Perikas próbują go ujarzmić i kontrolować.

## Dominanty

### Umiejętności

#### Preferowane

* Perikas najczęściej ma co najmniej jedną umiejętność "survivalową" lub "crafterską" - Perikas to taki "Lone Ranger", jest założenie że sobie poradzi w terenach niecywilizowanych - nawet skrajnych.
* Perikas ma silne skierowanie na wykorzystywanie technologii ludzkiej i na radzenie sobie bez użycia magii. Nigdy nie wiadomo, co się stanie gdy uderzy kolejne Zaćmienie / mamy niekorzystny Pryzmat.
* Bardzo często mag rodu Perikas ma umiejętności bojowe (samoobrony). W świecie Eterycznych drapieżników, nie chce być ofiarą.

#### Wykluczenia

* Perikas bardzo rzadko ma umiejętności związane ze sztuką mogącą generować silny Pryzmat. Pryzmat pochodzący ze sztuki może być generowany w niekorzystnym miejscu, dla niekorzystnych osób i może dojść do niepożądanego Skażenia
* Tak samo, Perikas niechętnie korzysta z Anomalii czy Skażeńców.
* Bardzo rzadko Perikas ma zaawansowane umiejętności społeczne. Na froncie Faz czy Eteru i tak wszyscy sobie muszą pomagać, ale z uwagi na niektóre działania magiczne (np. mentalka) nie można ufać nikomu bezgranicznie

### Magia

#### Preferowane

* Dominująca szkoła magii to Technomancja i Kataliza, zwłaszcza w zakresie wiązania energii i budowanie z niej narzędzi
* Druga dominująca szkoła magii to Puryfikacja oraz Ochrona Przed Skażeniem, by móc radzić sobie w warunkach wysokoEterycznych
* Trzecia dominująca szkoła magiczna, acz rzadsza, to "Survivalówka". Czy to budowanie schronienia, czy to fortyfikowanie się, czy to czyszczenie i puryfikowanie jedzenia.

#### Wykluczenia

* Praktycznie nie jest spotykana magia związana z kontrolą Pryzmatu, najwyżej z wygaszaniem Pryzmatu.
* Praktycznie nie jest spotykana magia mentalna czy kontroli innych. Zwyczajnie to jest zbyt niebezpieczne w związku z ich typowym obszarem.

## Historia

Ród Perikas - tak jak ród Zajcew - pochodzi z okolic Syberii.
