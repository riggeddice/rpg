* Fox: czy w EZ są dusze?
* Fox: I co się dzieje po śmierci?
* Żółw: nie mam żadnej odpowiedzi na to pytanie bo się nie pojawiło
* Żółw: WIEM o odbiciu symbolicznym w Eterze (think: Athamarein, Arazille)
* Żółw: WIEM o duchach jako echach Eteru, niezdolnych do uczenia się i zapętlonych
* Fox: tak, są różne echa, też o tym myślałam, ale co Z TOBĄ
* Żółw: ale nie było niczego o "postaci po śmierci"
* Żółw: właśnie
* Fox: tu jest śmieszna rzecz, bo ludzie prawie zawsze wierzą, że coś po śmierci jest, czyli nawet jeśli nie było to po pęknięciu musiało powstać
* Żółw: (Fidetis, Interis, Anteclis -> różne warianty przyszłości: od 'coś jest' do 'nie ma niczego')
* Fox: to znaczy, że ludzie EZ wyznają Nihilusa bardziej niż im się wydaje?
* Fox: W sensie, średnio połowa populacji będzie wierzyć, że po śmierci nastaje pustka?
* Fox: to jest nagle zaskakująco... pesymistyczny? aspekt
* Fox: szczególnie, gdy Nihilus jest złem, z którym walczymy
* Żółw: ludzie WIERZĄ w życie po śmierci, dusze itp
* Żółw: Alimarisa "próbuje" to wykorzystać
* Żółw: by "skoczyć nad Nihilusa"
* Żółw: ale JEŚLI Nihilus wygrywa, nie ma niczego
* Fox: okej, czyli mamy zmaganie dwóch najgłębiej zakorzenionych wierzeń ludzi
* Żółw: tak
* Żółw: dlatego nie mam odpowiedzi
* Żółw: bo JEŚLI powiem "jest życie po śmierci", to Alimarisa ma rację
* Żółw: jeśli powiem "nie ma niczego", to Alimarisa jest kolejną nadzieją ludzkości, pożeraną przez entropię jak wszystko 😉
* Fox: o łał
* Fox: czyli ludzkość nie wie, co więcej głęboka wiara zdefiniuje co na prawdę jest
* Żółw: entropia > wiara. Może tak, jest powód, czemu aspektem Interis jest "ogrom".
* Fox: yhym
* Fox: wiara się kończy gdy nic nie ma
* Żółw: więc jeśli JEST COŚ, to to coś musi być "z fizyki świata"
* Żółw: tak jak w "fizyce" jest termodynamika której manifestacją jest Nihilus
* Żółw: i dlatego NIE WIEMY czy Alimarisa / Spiconcis jest udaną strategią
* Fox: chociaż nawet jeśli nie wiemy, informacja czy dusze po śmierci są w stanie trafić do "jej realmu" powinna być czymś ustalonym
* Fox: nawet jeśli jej realm może zostać pożarty przez nihilusa
* Żółw: a skąd ludzie w świecie będą o tym wiedzieć jeśli to droga w jedną stronę? 😉
* Fox: fair point
* Żółw: nie wiemy czy "przodek pomógł", czy to pryzmat (bo wierzę że pomógł) czy szczęście
* Żółw: więc my nie musimy wiedzieć
* Fox: dobra, teraz widzę^^
* Fox: nice
* Żółw: wprowadzenie panteonu bardzo nam utrudnia i ułatwia życie XD
* Fox: zrobiło coś dziwnego
* Fox: mamy zaświaty schrodingera
* Fox: cudowne
