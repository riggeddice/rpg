---
layout: inwazja-vicinius
title: "Skażony Skorpioped"
---

# {{ page.title }}

## Mechanika

### Moc vicinusa

**Trwałość** : 3

| Obszar działania wroga    | Konflikt   |
|---------------------------|------------|
| Atak fizyczny (siła)      | Trudny     |
| Atak fizyczny (szybkość)  | Trudny     |
| Pancerz fizyczny          | Trudny     |
| Pancerz mentalny          | Heroiczny  |
| Pancerz społeczny         | Niemożliwy |
| Pancerz magiczny          | Trudny     |
| Zręczność                 | Trudny     |
| Obserwacja                | Typowy     |
| Intelekt                  | Łatwy      |
| Zasadzka                  | Łatwy      |

### Koncept

Skorpionokształtna, pancerna, dwunożna istota głodna mięsa i zemsty. Skorpioformy występujące zwykle pod ziemią. Szybkie, wybuchowe i śmiertelnie niebezpieczne w zwarciu.

### Otoczenie

Pod ziemią, w ciasnych tunelach i pomieszczeniach. Niedaleko silnych źródeł energii magicznych i innych form Skażenia i zanieczyszczeń.

### Misja

Zapewnić miejsce lęgowe niedaleko Skażonego obszaru, pozyskać i zapewnić inne istoty zdolne stać się skorpioformami, upolować mięso i zbudować legowisko. Dodatkowo, zlokalizować przyczynę ich powstania (źródło Skażenia Pryzmatycznego / "grzech") i owo źródło usunąć nie dbając o ewentualne ofiary konieczne.

### Siły

* Zabity przez silny wstrząs lub broń energetyczną wybucha ze straszliwą siłą (Heroiczny w epicentrum, Trudny na odległość)
* Potrafi wystrzelić pancerną głowę do przodu by pożreć ofiarę lub opluć ją kwasem (Trudny, zaskoczenie, zasięgowy)
* Niesamowicie szybki jak na tak pancerną istotę; potrafi wleźć praktycznie w każde miejsce

### Słabości

* Jego głównym słabym punktem jest szyja. Trudno ją trafić (Trudny), lecz jeśli się uda - umrze nie wybuchając.
* Jest wrażliwy na wysoką temperaturę i ogólnie rozumiany ogień. Pancerz tylko Łatwy.
* Wykrywalny magicznie z uwagi na Skażenie. Test Typowy by dostać na niego "radar".

### Zachowania

* Przede wszystkim chroni swojego miejsca lęgowego ze źródłem Skażenia.
* Poluje na ludzi i inne humanoidalne istoty, które jest w stanie docelowo przekształcić w inne skorpiopedy (okres 2 tygodni).
* Tendencje do berserkerskiego atakowania, korzystając ze swojej siły, pancerza i prędkości.
* Raczej przekopuje się przez ziemię niż wychodzi na otwartą przestrzeń.
* Intelekt zwierzęcy, acz zostaje echo "inteligentnej istoty" jak chodzi o taktykę. Zbyt dużo jednak emocji, za mało planowania.

## Opis

### Jak to wygląda

Żółto-zielony, trzymetrowy humanoid przypominający skorpiona. Zgarbiony. Potężna chitynokształtna, metalizowana łuska. Nie jest to efemeryda; musi powstać albo z ludzkiego nosiciela albo z istot które zwyczajnie miały predylekcje stać się taką istotą.

### Ekonomia

Pojawia się w okolicach Skażenia. Bardzo szybko populacja Skorpiopedów może wymknąć się spod kontroli i to jest powodem, dla którego na terenach Skażenia podziemnego zwykle wprowadza się jakieś magiczne pompy czy terminusów ochronnych. Z uwagi na ich tendencje do wybuchania, walka z skorpiopedami kończy się tragicznie z perspektywy ekonomii. Szczęśliwie, często wystarczy jeden zapalający dron by mieć "napalm w tunelu".

Głowy skorpiopedów są cenne - wykorzystuje się je do wzmacniania zaklęć w podobny sposób jak kryształy vitium, a są jednak stabilniejsze. Pancerze skorpiopedów często są stosowane w niektórych pancerzach magitechowych - jak długo terminus nie zakłada wysokiej temperatury.

### Wykorzystanie

Zgodnie z Porozumieniami Pustogorskimi Skorpiopedy nie są w żaden sposób wykorzystywane ekonomicznie.
