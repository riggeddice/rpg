---
layout: cybermagic-przeciwnicy
title: "AK Nocna Krypta"
---

## Koncept

Nocna Krypta - anomalny statek medyczny, niemożliwy do trwałego zniszczenia.

## Historia

* Ten statek kosmiczny zaczął jako "Nocne Ostrze" - niewielki, pancerny carrier ŁeZ.
* Niestety, ten model nie był przystosowany do długoterminowych działań jako carrier ŁeZ. Przerobiono go na anty-anomalny statek medyczny: 
    * ŁZy pozwalały Nocnej Krypcie się chronić przed anomaliami i do rozcinania / pozyskiwania co jest potrzebne.
    * W miejsce poszerzonych hangarów wstawiono biolaby, biovaty i systemy stazy.
    * "Nocne Ostrze" zostało przemianowane na "Nocną Ostoję"
    * Dowódca: Oliwia Kurint
* Gdy "Nocna Ostoja" złapało straszliwą plagę, dowódca medyczny - Helena Mirkowicz - poświęciła się w krwawym rytuale by kupić czas Ostoi.
* "Ostoja" była martwą jednostką, nazwano ją "Nocną Kryptą".
* Arianna Verlen ją reaktywowała i zintegrowała - Helena przejęła kontrolę nad Kryptą nadpisując BIA Klath
* Nocna Krypta stała się "statkiem-widmo", szukającym ludzi by ich ratować w Eterze Nieskończonym i Nierzeczywistości.

## Mechanika

### Klasa przeciwnika

* Przeciwnik **elitarny**
* Zewnętrzna część Krypty
    * Rój anomalnych ŁeZ - celują w ekstrakcję każdej żywej istoty z pojazdu i transport tych istot na pokład Krypty
    * Superciężki, regenerujący pancerz
    * Ekran ochronny - odpychający i niszczący wszystko co stara się zbliżyć do Krypty
    * Anomalne wyładowania magiczne; z uwagi na bardzo ciężkie napromieniowanie samej Krypty
* Krypta, front
    * Anomalne działo strumieniowe; Krypta nie ma mocy na działo strumieniowe. Ale ma XD.
* Krypta, front-boki
    * Pociski klasy MIRV, hipersyntetyzowane. 10/5 sekund.
* W środku Krypty, zawsze
    * Echa, Halucynacje - odbicia z przeszłości osób na Krypcie, z uwagi na potężną wolę Heleny i potężne Skażenie mentalne. Nie wiesz co prawdą, co fikcją.
    * Roboty - jednostki medyczne i wspierające mające odnaleźć i pomagać.
    * Pacjenci - osoby, które zostały "naprawione" przez Kryptę i nie mają pojęcia co się dzieje (często nawet nie wiedzą kim są przez Halucynacje!)
    * Korozja Nano-Wirusowa - osoby na pokładzie Krypty 
* Odkąd pojawiła się inkarnacja "Heleny"
    * Graviton blasts - potężne pola grawitacyjne, jakby nie dało się 
    * Trauma blasts - potężne pole mentalne generujące strach i traumę w swoich ofiarach
* Reaktor
    * Bardzo anomalne miejsce; sprzężone energetycznie z Anomalią Kolapsu
* Supercore
    * Wola Heleny staje się tu rzeczywistością.

### Struktura Nocnej Krypty

* Podwójny pokład górny
    * System komunikacji
    * Ciężkie działo strumieniowe
    * Hangar ŁZ
    * Syntetyzatory ŁZ
    * Syntetyzatory MIRV
    * 20 wyrzutni MIRV
    * Zaawansowany Engineering suite
    * Surgery Suites
* Dysk pod pokładem górnym
    * Mostek z ołtarzem Heleny
    * Nawigacja, sensory
    * System podtrzymywania życia
    * Reaktor
    * Projektor ekranu ochronnego
    * Dwa silniki awaryjne
    * Kabiny dowództwa
* Pokład centralny, za dyskiem
    * Cztery silniki
    * Kabiny załogi
    * Kabiny personelu medycznego
    * Engineering
    * System podtrzymywania życia, dla nie-dysku
    * Medical Treatment: 50 osób
    * Recovery Ward: 20 osób
    * Railway system (transport po Krypcie)
    * Droid / Angel Bay
    * Syntetyzator Aniołów
* Pokład tylny
    * Właz
    * Intensive Care Units
    * Hibernation Suites
    * 2* Biolab
    * 2* Containment Chamber
    * 4* Quarantine System
    * 2* Magazyn
    * Kostnica

### Otoczenie

Kosmos.

### Misja

* Uratować wszystkich.
* Wskrzesić kapitan Oliwię używając Arcymaga.

### Siły

* .

### Słabości

* psychika Heleny jest przewidywalna - chce ratować (nawet wbrew osobie ratowanej) i odzyskać kapitan Kurint.

### Zachowania

* .

## Opis

### Jak to wygląda

* Pokład górny
    * Hangar ŁZ
    * System komunikacji
    * Mostek z ołtarzem Heleny
    * Nawigacja, sensory
    * System podtrzymywania życia
* Pokład centralny
    * Reaktor
    * Dwa silniki
    * Projektor ekranu ochronnego
    * Kabiny załogi
    * Engineering, Fabrykatory
    * System podtrzymywania życia
* Pokład dolny
    * Właz
    * 2* Biolab
    * 2* Containment Chamber
    * 2* Magazyn

### Ekonomia

Jedno ze źródeł pozyskiwania ludzi przez Orbiter nad Astorią. Niebezpieczne, ale skuteczne.

### Wykorzystanie

.
