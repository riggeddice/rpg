---
layout: default
title: Miasto Pustogor
---

# {{ page.title }}

## Kluczowe aspekty

* **Przestrzeń**: Szczelina; Góry Puste; górskie miasto; sztuczne jezioro
* **Anomalie**: port morski w górach bez morza; wieczny deszcz i mgła; forteca
* **Ekonomia**: handel egzotyczny; dalekosiężne portalisko; górnictwo; polowanie na anomalie
* **Wartości Główne**: Order, Courage, Strength, Creativity, Community
* **Wartości Słabe**: Feelings, Spirituality
* **Kultura**: tarcia grup, duma, biurokracja, zamordyzm, bezpieczeństwo ponad prawo, pomaganie sobie, zniechęcanie do magii
* **Populacja**: bezpieczna ilość ludzi, magowie, dużo osób w tranzycie, mało viciniusów, sporo konstruminusów

## Dominujące sublokalizacje

### 1. Port Eteryczny

* na Primusie wygląda to śmiesznie - wielki port na niewielkim sztucznym jeziorze wiecznie okrytym mgłą
* ogromne baterie dział; większość siły ognia Pustogoru skierowane jest na jezioro graniczne
* duży, dużo się dzieje - ludzie mają zwykle zakaz zbliżania się do "bazy wojskowej". Obiekt kpin ludzi.
* bogate magazyny portowe; nie są największe, ale bardzo dużo cennych różności. Miejsce drogie jak cholera.

### 2. Jezioro Graniczne

* ma bezpośrednie przejście na Eter Nieskończony i Granicę Pustogorską
* jedno ze źródeł Skażenia i energii magicznej dla Pustogoru
* dno jeziora wiecznie się zmienia i też prowadzi do Eteru Nieskończonego - ranportale

### 3. Generatory Deszczu

* wysokie wieże zapewniające właściwy brak widoczności i redukcję emisji magicznej w Pustogorze
* brak funkcjonowania Generatorów sprawiłby zniszczenie Pustogoru narastającą energią magiczną i powiększenie Szczeliny

### 4. Ciężkie Baterie Dział

* potężne działa, zdolne do spustoszenia Pustogoru i okolicy. Siła ognia zdolna do dewastacji autowara czy Lewiatana.
* mieszanka najnowszych i starszych. Ogromny koszt utrzymaniowy; część nie jest napędzana magią.
* zawierają ekran ochronny i pola siłowe; Pustogor jest fortecą dużej mocy.

### 5. Miasteczko

* takie slumsowate miejsce, acz nie jest tak niebezpieczne jak się wydaje. Przemyt, wentyl bezpieczeństwa, anarchia.
* w większości znajdują się tam osoby które nie chcą być w centrum kontrolowanym przez potężne siły militarne
* prawo jest tam zdecydowanie bardziej luźne i większość rzeczy nie jest raportowane. A terminusi się nie wtrącają.

### 6. Wielki Bazar

* da się tam kupić praktycznie każdą anomalię czy dziwność. Najdziwniejsi kupcy, najbardziej zaskakujące plotki. Bardziej detal niż hurt.
* bardzo bezpieczne miejsce patrolowane regularnie przez konstruminusów, ale zawsze jest za mało konstruminusów na zbyt wielki bazar.
* ulubione miejsce spotkań nieco podejrzanych jednostek

### 7. Barbakan

* Centralna kwatera militarna w Pustogorze, miejsce dowodzenia terminusów i główne kwatery konstruminusów
* Potężny zbiór informacji na temat okolicy, przeciwników, viciniusów, anomalii itp. Jeśli ktoś to ma, jest to w Barbakanie.
* Forteca w fortecy. Najpotężniejsze miejsce do obrony w całym Pustogorze, najlepiej chronione i monitorowane. Nie ma tam prywatności.

### 8. Terminuski Szpital

* Jeden z najlepszych szpitali na Astorii, specjalizujący się w dekontaminacji i naprawie do Wzoru
* Specjalizuje się tylko w magach i ludziach, ale za to jest obiektem najwyższej klasy

### 9. Dzielnica Uciechy

* Wbrew nazwie, jest to po prostu normalne miejsce na standardy większości miast. Ot, sklepy, gabinety kosmetyczne, rozrywka...
* Jak w każdym mieście portowym, dużo tam się dzieje - ale to w miarę nudna uciecha.
* Znajdują się tam liczne szkoły walki i miejsca do potrenowania.

## Dominujące frakcje

### 1. Czerwone Myszy

* Grupka "młodych wilczków" - połączenie terminusów, łowców nagród i cokolwiek się znajdzie w nadziei na glorię i chwałę
* Nie są "gangiem", ale nie przejmują się legalnością - chcą jak największej masy krytycznej i mocy. Im więcej, tym lepiej.
* Potrafią dobrze zapłacić, ale niekoniecznie płaca jest powiązana z faktyczną trudnością zadania.
* Wartości: Strength, Achievement, Enjoyment

### 2. Orbitalna Gwardia

* Siła dążąca do ujednolicenia planów Orbitera i Pustogoru. Tu jest siła ognia, tam jest technologia.
* Bardzo promilitarni, zwolennicy porządku i współpracowania. Chcą bezpieczeństwa Astorii za wszelką cenę.
* Wartości: Order, Strength, Community

### 3. Miasteczkowcy

* Anarchiści, łowcy nagród, zwolennicy eksperymentowania i mniej zamordystycznej kontroli. Często współpracują z kupcami.
* Mniej zależy im na samym Pustogorze, raczej na tym, co Pustogor sobą reprezentuje i co ma do zaoferowania.
* Wartości: Creativity, Freedom, Achievement

## Dominujące osobistości

### 1. Karla Mrozik

* Aktualnie przełożona terminusów Pustogorskich i przełożona frakcji Orbitalnej Gwardii. Jakkolwiek to Rada Terminusów podejmuje decyzje w Pustogorze, ona jest Głosem tej Rady.
* Bardzo niebezpieczna i wpływowa postać - o jej klasie niech świadczy to, że nie będąc magiem Rodu dowodzi Pustogorem.

### 2. Lucjusz Blakenbauer

* Bardzo oddany swoim pacjentom lekarz Terminuskiego Szpitala. Święcie wierzy w "nie czynienie zła"
* Powiązany z niejedną mafią i pomagający niejednemu "złemu" magowi dla czystej idei. Uznany za nietykalnego i nieprzekupnego

### 3. Alan Bartozol

* Terminus frakcji Czerwonych Myszy. Cyngiel, bardzo kompetentny i niebezpieczny. Bardziej egzekutor niż zwiadowca.
* Przede wszystkim terminus - co prawda jest najemnikiem, ale zawsze zastrzega, że problemy Szczeliny i Pustogoru idą jako pierwsze.

## Niestabilności i dynamizm

### 1. Miasteczkowcy - Barbakan

Bezpieczeństwo kontra Wolność

Miasteczkowcy to anarchiści, zwolennicy wolności. Barbakan pilnuje bezpieczeństwa i trzyma wszystko pod kontrolą żelazną pięścią. Miasteczkowcy ciągle próbują odrobinę przekraczać kontrolę Barbakanu, Barbakan ciągle nie pozwala na to by struktura Pustogoru została zakłócona.

## Opis

### Pierwsze wrażenie

Okiem człowieka:

Ciągle pada deszcz. Gdy jest ładna pogoda, to jest mżawka. Ciągle jest jakaś mgła. Zawsze jest ograniczona widoczność i zawsze jest dość ponuro.

Pustogor jest miastem zbudowanym w górach, ze wszystkimi wzniesienami i obniżeniami jakich można się spodziewać - tak więc jeśli masz samochód, to ciągle jedziesz pod górkę lub zjeżdżasz z górki. Człowiek się nieźle namęczy zanim gdzieś dojdzie. To nie jest miasto dla starszych osób - ani dla samochodów.

Budynki są raczej niskie i przysadziste. Im bliżej centrum tym budynki mogą być wyższe, co daje wrażenie, że da się z centrum Pustogoru mieć oko na całe miasto. Oczywiście, gdyby nie ten deszcz czy mgła.

Pustogor sprawia wrażenie fortecy, które to wrażenie jedynie jest wzmocnione przez znajdujące się w różnych miejscach (z pewnością nieaktywne, bo po co) działa i wyrzutnie rakiet. Często da się zauważyć miłośników militariów robiących sobie zdjęcia z tymi działami.

Jakby mało było absurdów, w południowo-wschodniej części Pustogora znajduje się wielkie, sztuczne jezioro wraz z ogromnym portem. Ten port wygląda na aktywny... to jest, wyglądałby, gdyby było coś widać. Od razu rzucają się w oczy potężne baterie dział celujących w jezioro - podobno król Szczepan Szalony grał "w statki" używając dział oraz statków na jeziorze. Nikogo nie zdziwiłby taki idiotyzm w tak idiotycznym miejscu.

W południowej części Pustogoru znajduje się coś co wygląda jak miasteczko slumsów. Tamtejsze budynki nie są zbudowane zbyt solidnie i znajduje się tam Element Społeczeństwa - dziwni, często agresywnie wyglądający ludzie mówiący dziwnymi słowami i milczący, gdy przechodzi się niedaleko. Czasami dało się zauważyć, że nawet mają broń. Oczywiście, policja nic sobie z tego nie robi.

Ale za to w północno-zachodniej części Pustogoru jest ogromny bazar. Czego tam nie ma! Owszem, czasami są tam niesamowicie drogie rzeczy, ale da się tam znaleźć perełki jakich nie da się znaleźć nigdzie indziej. Pojawia się tylko jedno pytanie - w jaki sposób to się tam znalazło?!

Co więcej, wiecznie jest tu coś budowane. Jakieś mieszkania, domki, jakieś budynki - kto by chciał tu mieszkać?!


### Co tu grać

- członek mafii chcący zrobić coś za plecami lokalnych sił porządkowych (terminusi)
- lokalne siły porządkowe (grupa terminusów) polujące na potwora z pobliskiego Trzęsawiska, walczące z jedną z lokalnych mafii... policjanci i złodzieje. 
- płaszcz i szpada: szpieg próbujący pozyskać zastrzeżone dane z fortecy terminusów
- lokalny przedsiębiorca podpadający pomniejszemu mafioso

### Przeszłość

W roku 1943 świat pękł - doszło do Eterycznego Zaćmienia. W górach pojawiła się ogromna Szczelina, która zaczęła się jedynie powiększać. Po ciężkiej pracy i poświęceniach wielu naukowców i magów udało się w okolicach roku 1983 ustabilizować sytuację. Trzeba było jednak wbudować odpowiedniej klasy odpromienniki Skażenia i zabezpieczenia przed dalszym postępem Skażenia.

Tak powstało miasto Pustogor - forteca mająca chronić przed Lewiatanami, czy czymkolwiek mogącym wypełznąć z Wielkiej Szczeliny Pustogorskiej. Miejsce mające wbudowane odpromienniki i zabezpieczenia. Forteca chroniąca Pierwokraj przed anomaliami i efemerydami.

Pustogor jest miastem stosunkowo młodym - w chwili obecnej ma niecałe 50 lat. Jest budowane wieloetapowo. Na początku miało być jedynie niewielką stacją garnizonową, ale po upływie czasu okazało się, że Szczelina może być dochodowa na kilka sposobów:

* źródło kryształów _vitium_ i innych cennych minerałów i anomalii mineralnych (górnictwo)
* źródło anomalii magicznych (eksploracja Szczeliny, polowanie na anomalie)
* bardzo tanie źródła energii magicznej przez obecność Szczeliny (magitrownie)

Powyższe sprawiło, że potrzeby militarne Pustogoru zostały nieco przesunięte na drugi plan. W okolicy Pustogoru zaczęli pojawiać się łowcy nagród, łowcy anomalii, naukowcy i inżynierowie magitrowni.

Za tym poszła rozbudowa miasta - już nie tylko jako fortecy ale też jako miejsca w którym można mieszkać i godnie żyć, i to mimo niefortunnej sytuacji pogodowej.

W roku 2004 odkryto, że obecność Wielkiej Szczeliny umożliwia dalekosiężne połączenia przez Eter. Nie mówimy tu o "zwykłych" portalach - mówimy tu o bardzo dalekosiężnych i długodystansowych portalach, łącznie z niestabilnymi portalami do innych kontynentów - a nawet innych światów.

W związku z powyższym zbudowano ogromne sztuczne jezioro mające być rezerwuarem energii magicznej ze Szczeliny (po stronie południowo-wschodniej), po czym ufortyfikowano południowo-wschodnią część Pustogoru jeszcze mocniej, na wypadek, gdyby coś miało wyleźć z Eteru Nieskończonego lub ze Szczeliny.

Niestety, w roku 2015 stężenie osób nieświadomych magii i nieskażonych energią magiczną spadło poniżej bezpiecznej wartości krytycznej. To sprawiło, że Pustogor stał się "widoczny" z perspektywy istot mieszkających w Eterze Nieskończonym.

W 2015 roku Pustogor odwiedził jeden z Lewiatanów.

W wyniku starcia z Lewiatanem około 33% miasta zostało zniszczone. Zginęło kilka tysięcy ludzi i co najmniej pół tysiąca magów. Najprawdopodobniej - nikt nie wie tego na pewno - sam Lewiatan został zniszczony a jego ciało upadło w głąb Wielkiej Szczeliny. Żaden ze śmiałków próbujących pobrać próbki Lewiatana nie przeżył.

W tej chwili minęło ponad 30 lat od starcia z Lewiatanem. Pustogor jest odbudowany, silnie ufortyfikowany oraz ma stały garnizon 5 skrzydeł terminusów (z możliwością sprowadzenia 15 kolejnych skrzydeł w ciągu godziny).

Z uwagi na konieczność zachowania bezpieczeństwa magii w Pustogorze zrobiono następujące rzeczy:

* Stałe generatory deszczu i mgieł: jako, że tak blisko Szczeliny magia unosi się w powietrzu, deszcz prowadzi do zmniejszenia stężenia magicznego w okolicy.
* Stworzono opowieść o królu Szczepanie Szalonym, który chciał mieć port morski w górach i bał się najazdu górali - to zmieniło potężne baterie dział Pustogorskich w atrakcję turystyczną
* Wprowadzono prawa szczególnie zakazujące używania magii w jakimkolwiek stopniu na ludzi. Jednocześnie, ujawnienie magii wobec ludzi oznacza, że mag albo płaci ogromną grzywnę, albo jest odpowiedzialny za sprowadzenie na ten teren innych ludzi (by stężenie osób świadomych magii nie wzrosło)

### Populacja

Pustogor ma około 25000 osób w okolicy, z czego około 500 magów.

Z uwagi na swoje umiejscowienie niedaleko Szczeliny, Pustogor jest miejscem, w którym jest bardzo wysokie stężenie populacji magów. Terminusi Pustogorscy konsekwentnie próbują utrzymać stosunek 50:1, więc aktywnie odsyłają zainteresowanych magów i przyciągają nieświadomych magii ludzi.

Populację Pustogoru da się podzielić na kilka grup:

* Normalni ludzie mieszkający w okolicy: ogólnie, zabezpieczenie przed Lewiatanem i innymi potworami z Eteru i Szczeliny
* Turyści: przybyli dla militariów czy legendy o królu Szczepanie Szalonym. Czy pochodzić po górach, ku utrapieniu miejscowych magów
* Łowcy: czy to łowcy nagród, anomalii czy czegoś innego - nieistotne. Część z nich zamieszkała na stałe, część jest przelotem.
* Kupcy: powiązane silnie z tym, że Pustogor jest dalekosiężnym, niestabilnym Portaliskiem
* Naukowcy: badacze Szczeliny, artefaktorzy... zwykle na bardziej zewnętrznym kręgu Pustogora.
* Wojskowi: terminusi, magowie bojowi, zaawansowany sprzęt militarny. To oni dowodzą Pustogorem.

Najsilniejsze tarcia są między wojskowymi chcącymi zachować bezpieczeństwo Pustogoru a przyjezdnymi chcącymi dobrze zarobić i zachować wolność - zwykle pochodzą z grup łowców, naukowców czy kupców. To sprawia, że ci magowie często przenoszą się do Miasteczka (slumsów), by być jak najdalej oczu wojskowych.

### Różnice

#### Różnice w prawie

* **Absolutny zakaz używania magii pogodowej**: generatory mgieł i deszczu muszą wiecznie działać dla bezpieczeństwa miasta - inaczej energia magiczna z samego przebywania niedaleko Szczeliny doprowadzi do Skażenia ludzi lub powstawania Efemeryd.
* **Absolutny zakaz dotykania magią systemów obronnych**: Średnio raz w tygodniu dochodzi do pojawienia się niewielkiego ataku z Eteru, więc systemy obronne są kluczowe. Dotknięcie magią systemów obronnych może je uszkodzić - więc jest nakaz zgłoszenia celem naprawy (i zapłacenia grzywny). Nie trzeba było czarować.
* **Absolutny zakaz dotykania ludzi magią i uświadamiania ich o magii**: bezpieczeństwo Pustogoru wymaga odpowiedniej ilości nie Dotkniętych magią ludzi. Im większy jest Pustogor, tym więcej ludzi musi nie być Dotkniętych magią i tym więcej musi nie wiedzieć o magii. Skażenie magiczne ludzi wymaga zatem sprowadzenia ludzi spoza Pustogoru...
* **Nakaz rejestrowania zejść do Szczeliny**: działania dookoła Pustogoru są silnie zbiurokratyzowane. Komendant Pustogoru musi wiedzieć, co się dzieje w okolicy, co zostało odnalezione i jak wygląda sytuacja - by sprawa nie wymknęła mu się spod kontroli.
* **Nakaz informowania o przybyciu do Pustogoru**: jeśli ilość istot świadomych magii lub dotkniętych magią przekroczy stosunek 1:25 wobec istot nieświadomych, Pustogor znowu stanie się miejscem widocznym w Eterze i może przyciągnąć kolejnego Lewiatana.
* **Nakaz wykonywania poleceń terminusów Pustogorskich**: Pustogor jest bezpiecznym miastem, bo wszystkie polecenia terminusów Pustogorskich muszą być wykonywane - od nakazu opuszczenia miasta (bo stosunek niebezpiecznie spadł) po rekwizycję przedmiotów czy sprzętu.
* **Zakaz wykorzystywania dronów, przyzwanych istot itp**: Nie wysyłajcie niczego w powietrze. Nie wykorzystywajcie agentów. Nigdy nie wiadomo co się stanie. W ogóle, najlepiej nie wzbijajcie się w powietrze.

#### Różnice w zwyczajach

* **Człowiek to skarb**: z perspektywy mieszkańców Pustogoru, przyprowadzenie turystów czy ludzi nieświadomych magii i nie Dotkniętych magią to jedna z najlepszych dla okolicy rzeczy jaką można zrobić.
* **Jesteśmy na tej samej łodzi**: Pustogor jest miastem, w którym ludzie i magowie chętnie sobie pomagają, acz bez nadmiaru słów. Oczekuje się od wszystkich działania ku dobru wspólnemu. Za blisko Szczeliny i zbyt niebezpieczne miejsce by mogło być inaczej.
* **Magia bywa groźna**: Z uwagi na straszne konsekwencje magii w Pustogorze, raczej nie ma tendencji do czarowania poza określonymi desygnowanymi strefami. To miejsce musi być tak bezpieczne jak to tylko możliwe.

#### Różnice w działaniu miejsca

* W Pustogorze można kupić lub pozyskać sporo rzeczy naprawdę dziwnych. Można też je sprzedać. Oczywiście, wszystko musi być rejestrowane przez wojskowych.
* Efekty Skażenia i utrata kontroli magicznej w Pustogorze ma dużo silniejszy wpływ niż zwykle - obecność Szczeliny.

### Ekonomia

Pustogor jest doskonale połączony z egzotycznymi miejscami, ale przy użyciu nieco niestabilnych połączeń przez Eter. Niestety, z uwagi na samą lokalizację i strukturę Pustogoru, nie jest w stanie transportować wielkich partii materiałów (gdzie by to składować?). To powoduje, że Pustogor raczej zajmuje się handlem egzotycznym i dalekosiężnym a nie hurtowym dalekosiężnym.

Z uwagi na pobliską Szczelinę w Pustogorze energia magiczna jest nadzwyczaj tania. Pustogor zarabia eksportując energię magiczną z magitrowni na północ, do Pierwokraju.

Szczelina przyciąga też naukowców, łowców oraz kupców. Dodatkowo, są stałe działania górników zbierających nietypowe minerały i materiały. To wszystko powoduje, że Pustogor jest żywym miastem, przede wszystkim jest traktowane jako Generator (coś, co tworzy: minerały, anomalie, badania, egzotyczne rzeczy) i przesyła je dalej.

Słabym punktem Pustogoru jest ograniczone skomunikowanie z północą Pierwokraju. Ma linie kolejowe, ale z uwagi na specyfikę Pustogoru pociągi raczej będą jechały nocą (by ludzie na trasie nie zadawali dziwnych pytań). Ma bardzo szybką linię komunikacyjną przez wirzec, ale wykorzystywana jest tylko do transportu niestabilnych kryształów _vitium_.

Pustogor bardzo potrzebuje ludzi nieświadomych magii oraz nie dotkniętych magią. Ciągle toczą się jakieś budowy mieszkań i "niepotrzebnych prac" tylko po to, by ludzie mieli pracę i chcieli tu przyjechać. Niestety, sam Pustogor jest mało atrakcyjnym miejscem do mieszkania - wieczny deszcz, mała widoczność i ogólnie niefajnie.

## Lokalizacja

1. Świat
    1. Primus
        1. Astoria
            1. Pierwokraj
                1. Szczeliniec
                    1. Powiat Pustogorski
                        1. Pustogor
