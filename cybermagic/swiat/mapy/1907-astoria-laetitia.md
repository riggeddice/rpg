---
layout: inwazja-general
categories: general
title: "Kultura: Astoria: Laetitia"
---

# {{ page.title }}

## 1. Mapa wartości kultury

![Mapa wartości kultury Astorii::Laetitii](mats/1907-astoria-laetitia.jpg)

## 1. Aspekty kultury

### Główny: Laetitia

**Laetitia** od której powstała nazwa zjednoczenia miast i frakcji jest dominującym nurtem kulturowym.

1. Miej nadzieję, patrz z optymizmem.
2. Ciesz się życiem, życie ma sens i znaczenie.
3. Radość i optymizm dla każdego.

### Główny: Bezpieczeństwo

Astoriański sektor dostał porządny cios zarówno od Noctis jak i od Pęknięcia Rzeczywistości. Stąd pojawiły się trzy dominujące aspekty:

1. Przede wszystkim przetrwać
2. Pragmatyzm nad sympatię
3. Prymat terminusów; militaryzm

Między innymi dzięki temu skupieniu na Bezpieczeństwie do "Astorii" dołączył Orbiter.

### Główny: Niezależność

Z uwagi na to, że Astoria to zbiór państw-miast i konglomerat różnych, nie do końca pasujących do siebie kawałków Niezależność jest jednym z dominujących nurtów:

1. Każdy ma prawo do własnej moralności
2. Podejście silnie indywidualistyczne
3. Wolność do- oraz od- są wysoko cenione

Dzięki temu różne byty mogące wchodzić w skład Astorii mogą jednocześnie współpracować ze sobą do momentu naruszenia Bezpieczeństwa.

### Wspomagający: Adaptacja i Skuteczność


### Wspomagający: Czystość i Konserwacja


### Wspomagający: Auxilium


## 2. Implikacje

## 3. Występowanie

## 4. Wyjaśnienie historyczne
