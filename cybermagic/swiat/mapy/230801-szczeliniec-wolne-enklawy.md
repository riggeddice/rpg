## 1. Przestrzeń
## 1.1. Lokalizacja i biomy

Patrzymy na teren Enklaw Zachodnich:

![Mapa highlevel](mats/230801-enklawy/230801-mapa-highlv.png)

I teraz mamy dominujący teren:

![Ukształtowanie Geograficzne](mats/230801-enklawy/230801-poludnie-szczelinca-struct.png)

(baza: Ulytau_District, Kazachstan)

## 2. Krótki opis przestrzeni
### 2.1. Wysoki poziom

* Teren poza kontrolą Szczelińca, wysoko Skażony o ogromnej ilości Hazardów
* Teren w którym żyją przede wszystkim nomadzi i wyrzutki, osoby nie chcące się poddać dekadiańskiej kulturze
* Teren rządzony przez Skażeńce, na który wpływa Pacyfika i Wieczna Maszyna
* Teren przez który podróżują Lewiatany, wygląda post-apo
* Z perspektywy Szczelińca, teren buforowy przed chorym światem magii

### 2.2. Przestrzeń, wizualnie

Keyword: połoniny, wyżyna, step półpustynny

![Wizualnie](mats/230801-enklawy/230801-przestrzen-enklaw.png)

Inne dobre linki:

* https://www.google.com/maps/place/Ulytau+District,+100000,+Kazakhstan/@48.6528998,67.0077074,3a,75y,210.66h/data=!3m8!1e2!3m6!1sAF1QipOw97IABsrfJgaBvuvlju7VJ5WhYCz5iWQ7s3ia!2e10!3e12!6shttps:%2F%2Flh5.googleusercontent.com%2Fp%2FAF1QipOw97IABsrfJgaBvuvlju7VJ5WhYCz5iWQ7s3ia%3Dw203-h114-k-no!7i5760!8i3240!4m7!3m6!1s0x4222ebd0ebf2e49d:0x7b773d92b3959da0!8m2!3d47.8613436!4d66.3487124!10e5!16s%2Fm%2F041597c!5m1!1e4?entry=ttu#
* https://baronphotography.eu/najpiekniejsze-poloniny-w-bieszczadach/


### 2.2. Warunki terenowe
#### 2.2.1. Klimat

* Zakres termiczny: [-10, +20] stopni Celsjusza
* klimat kontynentalny
* Roślinność jest typowa dla surowych środowisk i składa się głównie z traw i niewielkiej ilości drzew, takich jak rosnące na stepach saksauły
* Granice
    * SE: Góry Puste łącznie z Wielką Szczeliną
    * N: Sojusz Letejski, Martelis
    * NE: Sojusz Letejski, Szczeliniec

#### 2.2.2. Typowe ukształtowanie i problemy

* Stepy pełne skał i jaskiń
* Skażone piaski
* Anomaliczne minerały wystające spod skał
* Zrujnowane miasteczka i tereny kiedyś ludzkie
* Toksyczne strumienie płynące niedaleko Pacyfiki
* Ścieżki, które prowadzą donikąd
* Anomalie zwłaszcza przestrzenne i mentalne
* MORDERCZE anomalne zmiany pogody i potencjalne burze magiczne
* Zupełnie nie działają drony czy niższego poziomu TAI
    * szybko tracimy nad nimi kontrolę i dołączają do mechosystemu

#### 2.2.3. Naturalni mieszkańcy

* Dominanta: rośliny
    * Rośliny z korzeniami adaptującymi się do ofiar
    * Bardzo niebezpieczne formy roślin i grzybów, też infekujące zwierzęta
    * 'plantaformy' - syntezy roślinnych istot i istot zwierzęcych / ludzkich
    * rośliny wpływające mentalnie (Śpiewający Lotos) lub zarastający (Symlotos)
* Dominanta: Owady symbiotyczne z roślinami
* Dominanta: Mechosystem
    * 'Żywe' maszyny, animowane anomalną energię
* Psoniedźwiedzie, polujące w sforach
* Scintiluxianie, istoty szukające 'ludzkiego umysłu' po przekroczeniu pewnej populacji
* Lewiatany, przekształcające swoją obecnością rzeczywistość

#### 2.2.3. Przetrwanie

* Schronienie: jaskinie, wzgórza, ruiny, enklawy
* Woda: 
    * rekomendacja to woda deszczowa, lapisowana
    * bardzo trudno o puryfikację większej ilości wody na tym terenie
    * są niektóre rzadkie głębokie źródła nieSkażonej wody, zwykle tam pojawiają się Enklawy
* Jedzenie: 
    * odkażone niektóre formy roślin, owadów oraz zwierząt
    * możliwość hodowli niektórych wyjątkowo stabilnych odmian (glukszwajnów zwłaszcza)
* LifeSupport:
    * medical: używanie tego co jest + import
    * podejście silnie nomadyczne

### 2.3. Polityka

* Szczeliniec 
    * akceptuje to co się dzieje w Enklawach i z nimi handluje
    * nie toleruje działań przeciwko Szczelińcowi; linią demarkacyjną jest Bariera
    * korzysta z możliwości badań i pozyskiwania rzeczy z anomalnych terenów
* Wolny Uśmiech Szczelińca
    * handluje i pozyskuje ludzi
    * zależy im na jak największej autonomii i niezależności Enklaw
* Nocne Niebo Szczelińca
    * pomaga noktianom i dostarcza sprzęt by przetrwać
    * ma tam swoje bazy i stashe
* Wolne Ptaki żyją w _love hate_ ze Szczelińcem
* Enklawy i nomadzi _raczej_ ze sobą współpracują, choć nie przeszkadza im rajd od czasu do czasu
* Wszyscy unikają zbyt dużych populacji by nie napotkać Scintiluxian ani Lewiatanów

### 2.4. Potrzeby

* LAPIS!!!
* Racje żywnościowe i wodne
* Zaawansowana technologia (servar, broń, puryfikacje, medykamenty)
* Dobra cywilizacji

### 2.5. Okazje

* Clarketech / Black Technology
* Anomalie i anomalne minerały
* Zakazany sprzęt z rozbitych jednostek i z czasów wojny
* Nietypowe bioformy, zioła i rzeczy nieznane nauce
* Możliwość eksperymentowania, niedostępna na terenie Szczelińca
* Output Wiecznej Maszyny
* Wydzieliny Lewiatanów i dziwnych bioform

## 3. Strony Aktywne i Zasoby
### 3.1. Mutatory (teren)

* Technologia wspomagana niską psychotroniką dołącza do mechosystemu (drony, słabe TAI)
* Bardzo mocno zmieniająca się pogoda w bardzo niebezpieczny sposób
* Zbyt duże skupiska ludzkie ściągają Scintiluxian
* Występują Skażone Piaski i Skażony Teren, nie zawsze łatwe do wykrycia
* Magia ma dodatkowe (+2Og); Og reprezentuje Skażenie zaklęcia przez dominujące siły (Pacyfika, Maszyna) i Lewiatany

### 3.2. Frakcje
#### 3.2.1. Wolne Ptaki

* **Koncept**
    * Luźno rozproszona i skłócona wewnętrznie frakcja indywidualistów działająca wśród nomadów oraz Miasteczka Pustogoru.
    * Zdecydowanie obróceni przeciwko 'tyranii terminusów' oraz Pustogorowi, chcą bardziej wolnego i niezależnego życia.
    * Nie zgadzają się na to, by Pustogor decydował jak żyć. Bardzo pionierskie podejście. "Dziki Zachód".
    * "Wolność i niezależność. Wy miejcie tą 'cywilizację', my działamy po naszemu - zdrowiej."
* **Serce**
    * **Agenda**
        * ""
        * **Jest**
            * 
        * **Ma być**
            * 
    * **Tory**: 
        * v1: 
* **Zasoby i Aspekty**
    * 
* **Import**:
    * 
* **Eksport**:
    * 

#### 3.2.2. Forteca Symlotosu

* **Koncept**
    * Sprzężeni z kwiatami Symlotosu ludzie z różnych frakcji, zainfekowani przez Symlotos
    * Ogólnie łagodni i pomocni, ale żądają wzmacniania ekosystemu w którym Kwiaty mają priorytet i mogą się rozwijać.
    * "Niech Kwiat oplecie świat. Zero wojen. Zero cierpienia. Jedna harmonia."
* **Serce**
    * **Agenda**
        * "Niech Kwiat oplecie świat. Zero wojen. Zero cierpienia. Jedna harmonia."
        * **Jest**
            * Ludzie marnują energię i się kłócą między sobą szukając 'niezależności'
            * Świat jest niebezpieczny i takie działania nie pomagają
        * **Ma być**
            * Kwiat połączy wszystkich harmonijną siecią, korzystając z talentów każdego
            * Każdy ma perfekcyjną rolę pod kątem swoich umiejętności i możliwości
            * Żyjesz jak długo jesteś przydatny, potem dołączysz do biomasy
    * **Tory**: 
        * v1: Szukają ludzi w potrzebie i robią działania humanitarne, by dołączyć ich do Symlotosu
        * v2: Budują karawany handlowe by powiększyć swoje wpływy i możliwości
        * v3: Ekspansja Symlotosu na teren tymczasowo niezamieszkały przez ludzi; nowa placówka
        * v4: Pozyskanie kluczowych zasobów; wysłanie raid party by zdobyć coś ważnego
* **Zasoby i Aspekty**
    * _superciężka forteca_: doskonale ufortyfikowana i z jakiegoś powodu Lewiatany tam się nie zbliżają
    * _perfekcyjna harmonia_: wiele umysłów połączonych Kwiatem; brak kłótni; jednostka autonomiczna, acz pragnie dobra kolektywu
    * _współpraca ludzi i zwierząt_: jeśli Kwiat może coś zainfekować to będzie to elementem Harmonii
* **Import**:
    * sprzęt ciężki i mechanizmy, broń
    * ludzie, zwierzęta... wszystko co można Symlotosować
    * "bezpieczeństwo", czyli usługi najemnicze itp.
    * woda, nieważne czy Skażona czy nie, w ogromnych ilościach
    * wyspecjalizowane umiejętności; Symlotos osłabia 'górną granicę skilli'
* **Eksport**:
    * mroczna nadzieja: "po co się męczyć, dołącz do nas i bądź na zawsze szczęśliwy"
    * żywność i medykamenty z Symlotosem (lub bez)
    * podstawowy sprzęt do przetrwania, wyprodukowany przez siebie
    * wszelkie namiastki luksusu: im nie są do niczego potrzebne, mają Kwiat.
    * Sanktuarium: możesz się tam schować i znaleźć bezpieczne miejsce na jakiś czas.
    * szczęśliwa siła robocza; Symlotos może wysłać część swoich sił by coś za to otrzymać
* **Powiązania**:
    * Pozytywne ostrożne relacje z większością Enklaw
    * Skrajnie negatywne relacje z Pustogorem i Wolnymi Ptakami na linii "wolność - szczęście"
    * Negatywne relacje z Czarnymi Czaszkami, którzy wykorzystują Symlotos jako źródło jedzenia

#### 3.2.3. Czarne Czaszki

* **Koncept**
    * Łowcy nagród i nomadzi, 'roaming bands' działający na terenach gdzie żaden człowiek nie powinien działać. Salvagerzy zagłady.
    * Uzbrojeni i zaadaptowani do tego świata, doskonale sobie radzą niewidoczni, cisi i niewykrywalni.
    * Odrzucili nadzieję, że da się osiągnąć coś cywilizacją i wracają do klasy hunter-gatherer.
    * "Silni będą silni. Słabi będą służyć lub posłużą jako pożywienie."
* **Serce**
    * **Agenda**
        * "Silni będą silni. Słabi będą służyć lub posłużą jako pożywienie."
        * **Jest**
            * Żałosne próby odbudowania cywilizacji lub zatracenia siebie nie mają sensu - to nowy świat.
            * Próbują tworzyć jakieś prawa i mechanizmy, ale nie da się opanować dzikości Szczeliny.
        * **Ma być**
            * Nie tylko przetrwamy ale też będziemy prosperować w spartańskim świecie zbieractwa
            * Wywalczymy sobie wszystko czego potrzebujemy sprytem i potęgą.
            * Centralna baza i grupy nomadów. A słabi będą służyć silnym.
            * Nowy Człowiek będzie lepszy niż dekadencki słaby człowiek kiedyś.
    * **Tory**: 
        * v1: Rytuał Przejścia: Czarne Czaszki robią test komuś w swoich (lub nie) szeregach by sprawdzić, czy się nadaje.
        * v2: Pozyskanie kluczowych zasobów na niegościnnych, anomalnych terenach lub od innych frakcji.
        * v3: Podgrupka wykonuje misję dla innej frakcji za odpowiednie zasoby.
        * v4: Podgrupka poluje na potencjalnych rekrutów albo żywność
        * v5: Przejęcie transportu do / z Enklawy Ralnik (tamte są super wartościowe)
* **Zasoby i Aspekty**
    * _Niewidoczność_: Są mistrzami kamuflażu i niewykrywalności, dzięki czemu mogą poruszać się po nieprzyjaznych terenach też wobec Anomalii.
    * _Silna hierarchia_: Mocno zdefiniowana hierarchia zasady "prawo dżungli", gdzie silniejszy dominuje nad słabszym.
    * _Adaptacja_: Przystosowani do przetrwania w najbardziej nieprzyjaznych warunkach i dostosowania się do sprzętu anomalnego.
    * _Ludzki duch, nie ciało_: Akceptują zmiany i adaptacje by się lepiej przystosować
    * _Naturalni nomadzi_: Rzadko zostają dłużej w tym samym miejscu
* **Import**:
    * Silni rekruci, dobrej klasy sprzęt
    * Towary luksusowe działające dla nomadów
    * Pojazdy i broń
* **Eksport**:
    * Anomalny sprzęt, rzeczy których już nie używają
    * Pozyskana żywność i woda
    * Pozyskane rzeczy z niebezpiecznych miejsc które są im niepotrzebne
    * Usługi łowców nagród czy przewodników
    * Informacje o terenie; najwięcej wiedzą i widzą
* **Powiązania**:
    * Szanują się z Wolnymi Ptakami i z Mafią; czasem handlują
    * Większość Enklaw traktują jako potencjalne ofiary

#### 3.2.4. Enklawa Ralnik

* **Koncept**
    * Grupa bardzo doświadczonych post-Szczelinian, nomadzka, która ma małe stado glukszwajnów
    * Zajmują się rolnictwem i Puryfikacją terenu na którym są a glukszwajny ratują im łuski
    * Jedna z silniejszych Enklaw, z mobilnym fortem na bazie starej jednostki bojowej
    * "Rozwiązaniem na problemy z inżynierią jest szybsza inżynieria i więcej viciniusów."
* **Serce**
    * **Agenda**
        * "Nie spuryfikujemy świata, ale zbudujemy coś z czym da się pracować"
        * **Jest**
            * Niewielka ruchoma Enklawa z uprawami i glukszwajnami, często się przesuwająca
            * Dobrze chronieni, ale wrażliwi na atak masowy lub superciężkich istot
        * **Ma być**
            * Wykrojone fragmenty terenu zgodne ze wzorem, na które da się przesunąć tymczasową Enklawę i rozpocząć żniwa z glukszwajnami
            * Odkryjemy jak współistnieć z tym terenem używając technologii i cywilizacji
    * **Tory**: 
        * v1: Eksperymenty z nowymi przestrzeniami, terenami i roślinami
        * v2: Próby pozyskania większej ilości substratów i komponentów do fabrykacji
        * v3: Eksploracja i badania by znaleźć lepsze tereny do zajęcia i puryfikacji
        * v4: Eksterminacja niebezpiecznego gniazda Skażeńców
        * v5: Atak wyprzedzający lub przekierowanie przeciwników gdzieś indziej
* **Zasoby i Aspekty**
    * _Hovertank Carcharein 'Słoneczny Płomień'_: jedyny aktywny hovertank inżynieryjny z AI _Maia_.
    * _Drony Inżynieryjne Carchareina_: silnie kontrolowane przez Maię by ich nie stracić
    * _Małe stadko glukszwajnów_: Puryfikują teren i umożliwiają Enklawie przetrwanie.
    * _Kultura rolniczo-saperska_: astoriańscy saperzy i pionierzy zdolni do błyskawicznej stabilizacji
* **Import**:
    * Woda. Dużo wody. Może być Skażona
    * Usługi militarne i najemnicze
    * Medykamenty, elementy medyczne itp.
    * Informacje o ruchach Lewiatanów i groźnych potworów
    * Anomalne rzeczy z okolicznego terenu
    * Paliwo i baterie dla hovertanka _Carcharein class_ (zwłaszcza Szczeliniec)
    * Substrat do fabrykatorów dron inżynieryjnych 'Płomienia'.
    * Części i materiały do konserwacji i naprawy sprzętu i pojazdów (niefabrykowalne)
    * Hub handlowy dla Enklaw jeszcze bardziej oddalonych od Szczelińca i Martelisa
* **Eksport**:
    * Usługi Puryfikacji i glukszwajnowe; też konstrukcja i fortyfikacja
    * Ciężki sprzęt, pojazdy, rzeczy jakie są w stanie sfabrykować i naprawić
    * Żywność, zwłaszcza roślinna
    * Duża wiedza techniczna, inżynieryjna, mapy
    * Reselling rzeczy importowanych na czym im szczególnie nie zależy
* **Powiązania**:
    * Wrodzy noktianom (najeźdźcy), Czarnym Czaszkom (piraci), Symlotosowi (nieludzie)
    * Ogólnie przyjaźni innym frakcjom

### 3.3. Nazwane Hazardy
#### 3.3.1. Pacyfika (!)

* **Koncept**
    * Wymarła arkologia zbyt blisko Szczeliny, kiedyś supernowoczesna
    * Opanowana przez Anomalie Przestrzenne, echo świata który MÓGŁ być
    * Żywy dowód, że ludzkość nigdy nie opanuje terenu magicznego
    * Co ważne, to nie jest spawner. Ale to perfekcyjny **disruptor**.
* **Serce**
    * **Agenda**
        * "Przestrzeń jest tylko złudzeniem. Nic nie jest prawdziwe."
        * zdestabilizuj rzeczywistość: technologię, przestrzeń, magię
        * pokaż chaos rzeczywistości i konsekwencje Kolapsu
    * **Dark Future**: 
        * Miejsce łączące światy; mikro-Anomalia Kolapsu. Nikt nie przychodzi. Grobowiec tego, co być mogło.
        * Kto tu wejdzie, ten nie wróci, stanie się częścią Legionu
    * **Umocowanie**: blisko Szczeliny, amplifikuje ludzkie dusze, załamanie przestrzeni
    * **Tory**: 
        * v1: brak. Pacyfika jest stroną z agendą izolacjonistyczną. Hazard nie Aktor.
        * v2: Pacyfika przekształca przestrzeń i otworzy się / zamknie się przejście GDZIEŚ.
        * v3: Legion wyruszy poza Pacyfikę by osiągnąć COŚ.
* **Zasoby i Aspekty**
    * _nienaturalna topologia_: iluzje optyczne stają się prawdą; Escher
    * _kryjówki na skarby_: Przemytnicy zostawiają tam skarby do wzięcia 'potem'
    * _rzeczywistość jest nieciągła_: nigdy nie wiesz gdzie wyjdziesz; ranteleporter
    * _anomalie przestrzenne_: potencjalne pułapki i pętle, idziesz w jedną stronę a wyjdziesz inną
    * _Legion Pacyfiki_: efemerydy zrodzone ze strachu oraz ci, którzy nie umieli wyjść; emocjonalne echa
* **Import**:
    * pozyskuje energię z Wielkiej Szczeliny
    * pozyskuje materię z Kolapsu
    * pozyskuje formę i wolę z przybyszów
    * nie ma niczego czego pragnie od innych frakcji
* **Eksport**:
    * skarby ze Szczeliny, porozkładane w dziwnych miejscach Pacyfiki
    * ofiara; ktoś złapany przez Pacyfikę kogo się udało uratować
    * Clarketech / Black Technology
* **Pokusy i typowe Porażki**
    * zwrócenie uwagi Legionu Pacyfiki; przekierowanie części Legionu GDZIEŚ.
    * przekształcona przestrzeń - KTOŚ wychodzi gdzieś gdzie nie chciał
    * odnajdujesz coś, co ktoś w Pacyfice chciał ukryć
    * coś, co ktoś próbował zrobić przestaje działać; Pacyfika to _disruptowała_
    * zagubieni w Pacyfice

#### 3.3.2. Wieczna Maszyna (!)

#### 3.3.3. Las Pusty

* **Koncept**
    * Kiedyś to była farma hydroponiczna, Skażenie przekształciło to miejsce w Krwawy Las
    * Żywi się krwią swoich ofiar i Skażeniem; miejsce gdzie można się odżywić i je odżywić
    * "Symbiotyczna" struktura dostarczająca jedzenia i wody w zamian za ofiary
* **Serce**
    * **Agenda**:
        * "Jesteś częścią ekosystemu. Zjeść albo zostać zjedzonym. Co wsadzisz, otrzymasz."
        * rozprzestrzeniaj wpływ Lasu agentami, porywaj biomasę i dostarczaj ją do Lasu
        * daj okazję do posilenia się oraz do zasilenia Lasu ;-)
    * **Dark Future**: 
        * "Cały teren współpracuje z Lasem albo jest częścią Lasu. Las jest wszystkim"
        * jedyne, co nie jest pokryte Lasem to tereny Pacyfiki i podróże Lewiatanów
        * Las przekroczył masę krytyczną i stał się czymś nowym, lepszym i straszniejszym
    * **Umocowanie**: 
        * generator żywności i wody oraz puryfikator
        * kultywowany przez Enklawy ale też zwalczany, by się nie rozprzestrzenił za bardzo
    * **Tory**: 
        * v1: jakaś frakcja wykorzystuje Las Pusty jako generator żywności ściągając inne istoty lub karmiąc ją ludźmi
        * v2: ktoś jest w głębi Lasu Pustego i trzeba go uratować, zanim zostanie zjedzony
        * v3: Las Pusty się rozprzestrzenia, zajmując coraz to nowsze obszary przez agentów
* **Zasoby i Aspekty**
    * _źródło jedzenia i wody_: odpowiednio wykorzystując 'owoce' lasu da się mieć bezpieczną żywność
    * _naturalny Puryfikator_: w naturalny sposób destabilizuje i wysysa magię, anomalie i artefakty
    * _symbiotyczne owady_: bronią, atakują, dostarczają biomasę, rozprzestrzeniają
    * _kultyści i sojusznicy_: Las ma ludzi którzy go wielbią i karmią; nie dadzą go zniszczyć
    * _natura zawsze znajdzie drogę_: nie wiadomo jak, ale potrafi wypełznąć z każdego _containmentu_
* **Import**:
    * pozyskuje energię Puryfikując teren
    * pozyskuje biomasę wabiąc oraz wysyłając agentów
    * potrzebuje więcej biomasy, wody, dobrej gleby - zwłaszcza gdy musi coś Puryfikować
* **Eksport**:
    * puryfikowany sprzęt, żywność, woda itp.
    * potencjalne sadzonki na mikro-puryfikację, jeśli się uda
    * rzadkie rośliny lub owady należące do ekosystemu Lasu
* **Pokusy i typowe Porażki**
    * coś ważnego lub ktoś ważny znajduje się w Pustym Lesie i da się to pozyskać
    * Pusty Las jest potencjalnie dobrą pułapką lub kryjówką
    * okazja do pozyskania rzadkich cennych roślin, kosztem krwawej ofiary
    * ktoś lub coś jakoś zostaje pochłonięte przez Las
    * kultyści pomagają lub zdradzają kogoś

#### 3.3.4. Wzgórza Wiecznego Echa

#### 3.3.5. Ruina Floty Noctis

### 3.4. Generyczne klocki

#### 3.4.1. Zrujnowane miasteczko

* **Koncept wysokopoziomowy**
    * Kiedyś tętniące życiem miasteczko, teraz puste i opuszczone z powodu katastrofy.
* **Agenda** 
    * **Głód**: "Jeśli wejdziesz, zostaniesz tu. Jeśli nie, nie oddam Ci moich skarbów"
    * Spraw, aby gracze czuli ciężar przeszłości i nadzieję, że może coś się da zdobyć - dużo ryzykując.
    * Daj im możliwość pozyskania czegoś cennego, ale niemałym kosztem
* **Dylemat**: Czy pozyskać coś teraz i ryzykować, czy poszukać gdzieś indziej?
* **Zasoby i Aspekty**
    * "_widmo przeszłości_" - popękane ulice, zniszczone budynki i porzucone przedmioty codziennego użytku przypominają o lepszych czasach.
    * "_siedliska mutantów_" - skrzywione budynki i podziemne kanały stanowią doskonałe siedliska dla mutantów.
    * "_toxic waste_" - skażenie z powodu którego miasto zostało opuszczone jest nadal obecne, stwarzając dodatkowe zagrożenie.
    * "_zachowane skarby_" - choć większość miasteczka jest w ruinie, mogą tam być jeszcze jakieś wartościowe przedmioty.
* **Pokusy i Typowe Porażki** 
    * Przypadkowe natknięcie się na mutantów.
    * Wybuch gazu lub innej zgromadzonej substancji.
    * Próbując dostać się do schronu, gracz aktywuje starą, jeszcze działającą obronną pułapkę.
    * Gracz znajduje starą, zapomnianą przez czas mapę miasteczka, która może prowadzić do cennych zasobów.
    * Gracze natykają się na ślady innej grupy ocalałych, co może prowadzić do konfliktu lub ewentualnej współpracy.
    * Odnalezienie skarbu, który może przynieść wiele korzyści, ale także przyciągnąć niechciane uwagi.
    * Spotkanie z mutantem, który jest mniej wrogi i chętny do wymiany informacji za zasoby.
    * Gracz doznaje choroby z promieniowania lub skażenia toksycznymi odpadami.

#### 3.4.2. Lokalna potworyzowana fauna

* **Koncept wysokopoziomowy**
    * Lokalna niebezpieczna fauna i drapieżniki
* **Agenda** 
    * **Głód**: "Nie jesteś na swoim terenie. Zostaniesz zjedzony i zasilisz ekosystem swoją biomasą."
    * Przypomnij graczom, że są intruzami w tym środowisku i że muszą szanować jego mieszkańców.
    * Zagrażaj graczom poprzez niekontrolowane reakcje zwierząt.
* **Dylemat**: Muszą poruszać się ostrożnie, aby nie aktywować zwierząt, a jednocześnie szybko, aby nie zostać złapani.
* **Zasoby i Aspekty**
    * "_zakłócone zwierzęta_" - miejscowe zwierzęta są niespokojne i mogą zaatakować w każdej chwili.
    * "_ślady zwierząt_" - można je używać do nawigacji, ale mogą prowadzić do niebezpiecznych miejsc.
    * "_zbyt groźny potwór_" - coś, co jest potencjalnym super-hazardem lub maskowaniem.
* **Pokusy i Typowe Porażki** 
    * Obecność groźnego potwora, którego można na coś napuścić
    * Możliwość schronienia się w miejscu wyczyszczonym przez potwora
    * Zbocze góry, po którym gracze próbują przejść, okazuje się miejscem gniazdowania ptaków drapieżnych, które zaatakują, aby chronić swoje młode.
    * Potencjalna żywność

#### 3.4.3. Zmienne Warunki Pogodowe

* **Koncept wysokopoziomowy**
    * Nagłe zmiany warunków pogodowych - zwłaszcza burze czy wiatry - mogą stanowić duże wyzwanie dla podróżujących
* **Agenda** 
    * **Głód**: "Na tym terenie nie jesteś u siebie. Zmiażdżę Cię pogodą."
    * Wykorzystaj warunki pogodowe do tworzenia nieprzewidywalnych i trudnych sytuacji.
    * Przedstaw graczy trudnymi decyzjami związanymi z pogodą, takimi jak znalezienie schronienia czy kontynuowanie podróży pomimo niebezpieczeństw.
* **Dylemat**: Gracze muszą zdecydować, czy kontynuować mimo ryzyka, czy szukać schronienia i czekać na lepsze warunki.
* **Zasoby i Aspekty**
    * "_nagła burza_" - niespodziewana burza może zmusić graczy do szukania schronienia lub ryzykowania w burzy.
    * "_gęsta mgła_" - mgła utrudnia widoczność i orientację, co może prowadzić do zgubienia drogi.
    * "_mordercza ulewa_" - usuwa ślady, usuwa widoczność
* **Pokusy i Typowe Porażki** 
    * W gęstej mgle ktoś może zgubić drogę i oddalić się od reszty grupy; da się rozdzielić.
    * Nagła burza - mokrzy i zmarznięci lub uszkodzenie sprzętu.
    * Ograniczony dostęp do magii i technologii, fantomy na radarach i w komunikatorach
    * Ambush - drapieżnik albo inna grupa
    * Znajdują schronienie, ale jest już zajęte
    * Zmiana terenu z uwagi na problem z pogodą

#### 3.4.4. Niebezpieczne Ścieżki

* **Koncept wysokopoziomowy**
    * Ścieżki są pełne pułapek i niebezpieczeństw, naturalnych lub anomalnych
* **Agenda** 
    * **Głód**: "Nie znasz nawet zagrożenia i nie wiesz co będzie Twoją zgubą"
    * Pokaż niebezpieczeństwo nieznanego i niebezpieczeństwa płynące z poruszania się po niewygodnym, potencjalnie anomalnym terenie.
    * Kuś możliwością lepszego przejścia, ale kosztem poświęcenia zdrowia, zasobów lub niefortunnego encountera
    * Stwórz sytuacje, w których gracze muszą zdecydować, czy ryzykować, idąc nieznaną ścieżką, czy szukać innej drogi.
* **Dylemat**
    * Konflikt: Czy ryzykować, idąc niebezpieczną ścieżką, czy szukać bezpieczniejszej drogi?
* **Zasoby i Aspekty**
    * "_ścieżka pełna pułapek_" - niebezpieczne pułapki (też anomaliczne lub głodne rośliny) mogą zagrażać bezpieczeństwu graczy.
    * "_ścieżka przez urwisko_" - jedyne droga prowadzi przez stromy klif, który zagraża upadkiem.
    * "_zdradliwe podłoże_" - ścieżka jest pełna kamieni, które mogą spowodować potknięcia lub zwichnięcia.
    * "_błotne osuwisko_" - podłoże się rwie i może zabrać Cię ze sobą
* **Pokusy i Typowe Porażki** 
    * Przejście dalej wiąże się z koniecznością pozbycia się części sprzętu, bo nie uniesiesz
    * Przejście dalej ma niebezpieczne rośliny i owady, mogące Cię zatruć
    * Okazja pozyskania czegoś wartościowego z ciała kogoś kto nie dał rady się przedostać
    * "No przecież NIKT by tam nie poszedł, nikt nie jest tak głupi!"
    * Ślady bardzo niebezpiecznego potwora - a jesteśmy na fatalnym terenie
    * Próbując przejść ktoś hałasuje i daje znać o swojej obecności innym lub potworowi

