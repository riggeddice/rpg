---
layout: default
title: Trzęsawisko Zjawosztup
---

# {{ page.title }}

## Kluczowe aspekty

* **Przestrzeń**: trzęsawisko, Skażenie magiczne, ślisko, zdradliwe kroki
* **Anomalie**: Duchy Zjawosztup, groźna roślinność, łatwo się zgubić
* **Ekonomia**: cenne Anomalie, cenne viciniusy, miejsce azylu

## Dominujące sublokalizacje

### 1. Laboratorium W Drzewie

* wiecznie się przesuwające
* wskrzeszane przez Zjawosztup
* biolab i wielka biobiblioteka Sataraila

### 2. Toń Pustki

* hipnotyczna i wciągająca
* pokazuje obrazy z innych miejsc
* potencjalny atak węży

### 3. Mglista Ścieżka

* prowadzi w inne miejsca
* często jednostronna
* plącze energię magiczną

### 4. Wir Szczeliny

* Emisja energii Szczeliny
* serce Trzęsawiska
* pulsujące serce Skażenia
* centralne jeziorko

### 5. Głodna Ziemia

* zsyła iluzje i koszmary
* źle nadepniesz - idziesz na dno
* dom Skażonych pająków

### 6. Polana Nadziei

* miejsce azylu
* leczy i chroni
* uczucie piękna i łagodności

### 7. Azyl Odrzuconych

* wioska, gdzie żyje do 100 osób
* miejsce bezpieczne na Trzęsawisku
* osoby nie mające miejsca w normalnym świecie
* hodowla rzadkich, magicznych bytów

## Dominujące osobistości

### 1. Wiktor Satarail

* wybitnej klasy biomanta i naukowiec
* bardzo lubi sztukę - a zwłaszcza muzykę
* strażnik viciniusów i ludzi przeciwko magom

## Nieprzyjemne wydarzenia

* Nic nie da się zobaczyć. Łatwo się zgubić; Trzęsawisko się zmienia
* Trzęsawisko powołało Strażnika by zatrzymać ofiarę na swoim terenie
* Niemożność rozpalenia ognia, wiecznie wilgotno
* Magiczne choroby osłabiające odporność ludzi czy magów
* Możliwość zapadnięcia się w śliski, pozornie bezpieczny grunt
* Skażone rośliny czy zwierzęta, wiecznie regenerujące
* polana pełna Lis'ha'orr. Niestety, zamaskowanych...

## Pierwsze wrażenie

## Przeszłość

## Populacja

## Różnice

### Różnice w prawie


### Różnice w zwyczajach


### Różnice w działaniu miejsca

## Ekonomia

## Points of Interest

## Lokalizacja

1. Świat
    1. Primus
        1. Astoria
            1. Pierwokraj
                1. Szczeliniec
                    1. Powiat Pustogorski
                        1. Trzęsawisko Zjawosztup
