## Układ arkologii Nativis

* Aspekty:
    * Bardzo dużo roślin, drzew itp. Piękne roślinne miejsce.
    * Bardzo spokojna arkologia, silnie stechnicyzowana i sprawna.
    * Dominanta: faeril > drakolici, ale próba balansowania. Niskie stężenie AI.
    * BIA jako centralna jednostka dowodząca, imieniem "Prometeus"
* Struktura:
    * Wielkość: 4.74 km kwadratowych, trzy poziomy
    * Populacja: 11900 (Stroszek)
        * Operations & Administration: 1100
        * Extraction & Manufacturing: 3213
        * Medical: 355
        * Science: 360
        * Maintenance: 700
        * Leisure: 1000
        * Social Services: 240
        * Transportation: 800
        * Training: 229
        * Life Support: 500
        * Computer & TAI & Communications: 500
        * Engineering: 500
        * Supply / Food: 1200
        * Security: 300
    * Bogactwo: wysokie. Technologia + piękna roślinność.
    * Eksport: żywność (przede wszystkim), komponenty konstrukcyjne, hightech, terraformacja
    * Import: luxuries, prime materials, water, terraforming devices, weapons
    * Manufacturing: jedzenie, konstrukcja, terraformacja
    * Kultura: 
        * dominanta: piękno, duma z tego co osiągnęli, duma z tego że nie ma wojny domowej faeril/drakolici
        * wyjątek: brak.
    * Przestrzenie
        * rozrywki: parki, natura, zdrowe ciało - zdrowy duch
        * inne: hotele, laboratoria terraformacyjne, restauracje...
    * Zasilanie
        * solar + microfusion
    * Żywność
        * różnorodna; to miejsce jest źródłem żywności
        * przede wszystkim roślinna, ale też zawiera elementy zwierzęce (te najczęściej z importu)
    * Life support => terraformacja + filtracja
    * Comms => biomechaniczne okablowanie + normalne sieci
    * TAI => BIA wysokiego stopnia imieniem Prometeus
    * Starport => large
    * Infrastructure => high-tech, dużo półautonomicznych TAI, wszystko nadrzędne przez Prometeusa. Świetny transport publiczny. 
    * Access & Security => arkologia z silnymi blast walls, silnymi liniami dostępowymi.

Historia: 

* w trakcie wojny domowej sprzedawała żywność na 2 fronty co spowodowało znaczne bogactwo ale też częściową niechęć innych arkologii
* obecnie prowadzi ekspansje na struktury zrujnowane/opuszczone po wojnie żeby całkowicie się uniezależnić od dostaw z poza planety

## Kto
### Arkologia Nativis
#### Kidironowie - centrala

* Lycoris Kidiron, 57 lat (ze wspomaganiami drakolickimi dającymi jej efektywność 3x-latki) (objęta przez Wiki)
    * Wartości
        * TAK: Bezpieczeństwo, Osiągnięcia ("dzięki mnie ta arkologia będzie bezpieczna i osiągniemy najwyższy poziom eksportu żywności")
        * NIE: Konformizm ("nikt, kto próbował się dopasować do innych nie doprowadził do postępu")
    * Ocean: ENCAO: +-0+0 (optymistyczna, nieustraszona, lubi się dogadywać z innymi, nie patrzy na ryzyko)
    * Silniki:
        * TAK: Kapitan Ahab: polowanie na białego wieloryba do końca świata i jeszcze dalej, nieważne co przy tym stracimy. (tu: "zrobię najlepszą arkologię, lepszą dla ludzi niż planeta macierzysta")
    * Rola:
        * ekspert od bioinżynierii i terraformacji, pionier (zdolna do przeżycia w trudnym terenie)
* Rafał Kidiron
    * Wartości
        * TAK: Achievement, Power("wraz z sukcesami przychodzi władza, wraz z władzą musisz osiągać więcej sukcesów")
    * Ocean
        * ENCAO: 00+-0 (precyzyjny i dobrze wszystko ma zaplanowane, lubi się kłócić i nie zgadzać z innymi)
    * Silniki:
        * TAK: Duma i monument: to NASZE sukcesy są najlepsze i to co MY osiągnęliśmy jest najlepsze. Musimy wspierać NASZE dzieła i propagować ich chwałę.
    * Rola:
        * kuzyn Lycoris, security lord, w wiecznym konflikcie z Gurdaczem (security & money - beauty)

#### Kidironowie / "władza" - support

* Tymon Korkoran (kuzyn Eustachego)
    * Ocean: ENCAO:  +--00
        * Jeżeli czegoś chce, weźmie to dla siebie; zazdrosny i zawistny
        * Niecierpliwy, chce TERAZ i nie będzie czekać
        * Egzaltowany, w przejaskrawiony sposób okazuje uczucia
    * Wartości:
        * TAK: Universalism, TAK: Stimulation, TAK: Tradition, NIE: Face, NIE: Humility
    * Silnik:
        * TAK: Być jak idol: iść w ślady swojego Idola i Mentora. Stać się tym czym idol się stał.
        * TAK: Wrócić do ukochanej: byłem głupi i ją straciłem. Zrobię wszystko, by móc do niej wrócić.
* Laurencjusz Kidiron (młody Kidiron, chce być ZNACZĄCY)
    * Ocean: ENCAO:  +0---
        * Nie planuje długoterminowo, żyje chwilą
        * Nudny. Nie ma nic ciekawego do powiedzenia i niezbyt się czymkolwiek interesuje.
    * Wartości:
        * TAK: Face, TAK: Power, TAK: Hedonism, NIE: Achievement
    * Silnik:
        * TAK: Przejąć władzę nad ogranizacją: ten ród / ta firma będą MOJE. Podbiję je, zmanipuluję lub doprowadzę do tego pokojowo.
* Karol Lertys (dziadek)
    * Ocean: ENCAO:  -00-+
        * Zrzędliwy i kłótliwy
        * Skryty; zachowujący swoją prywatność dla siebie
        * Chętny do eksperymentowania i testowania nowych rzeczy
    * Wartości:
        * TAK: Achievement, TAK: Family, TAK: Security, NIE: Stimulation, NIE: Self-direction
    * Silnik: (wszystko by tylko pomóc swoim wnukom; KIEDYŚ przemytnik potem inżynier arkologii i zna tajne skrytki)
        * TAK: Herold Baltazara: zwiększać wpływ i moc kogoś innego, powiększać ich wpływy.
        * NIE: Do the right thing: niezależnie od tego co jest wygodne czy tanie, własne zasady i własny kompas moralny są najważniejsze. By spojrzeć w lustro.
* Jarlow Gurdacz
    * Wartości
        * TAK: Hedonism, Face NIE: Conformism ("najpiękniejsza arkologia, klejnot Neikatis - i to nasza!")
    * Ocean
        * ENCAO: 000++ (kocha piękno i ma mnóstwo świetnych pomysłów)
    * Silniki:
        * TAK: Artystyczna dusza: Stworzyć piękno, pokazać je całemu światu lub wręcz przeciwnie, zachować dla godnej publiczności. Być docenionym.
    * Rola:
        * ekspert od roślinności, architektury i zdrowia arkologii, w wiecznym konflikcie z Rafałem Kidironem (security & money - beauty)


### Infernia

* Bartłomiej Korkoran, wuj i twarda łapka rządząca Infernią.
    * Ocean: ENCAO:  +-000
        * Bezkompromisowy, nieustępliwy, niemożliwy do zatrzymania
        * Ciekawski, wszystko chce wiedzieć i wszędzie wsadzić nos
    * Wartości:
        * TAK: Family, TAK: Self-direction, TAK: Achievement, NIE: Tradition, NIE: Humility
    * Silnik:
        * TAK: Inkwizytor: niszczenie fake news, publiczne pokazywanie hipokryzji, ujawnianie bolesnych prawd
        * TAK: Kolekcjoner: kolekcjonuje coś rzadkiego ()
    * Rola: wuj, kapitan Inferni
* Celina Lertys, podkochująca się w Eustachym 
    * Ocean: ENCAO:  --+-0
        * Stanowczy i silny, zdecydowany
        * Skryty; zachowujący swoją prywatność dla siebie
        * Nigdy nie wiadomo, co jest prawdą a co maską
    * Wartości:
        * TAK: Universalism, TAK: Tradition, TAK: Security, NIE: Power, NIE: Humility
    * Silnik:
        * TAK: Harmonia naturalna: balans między technologią i naturą, utrzymanie cudów natury. Lasy Amazonii.
        * NIE: Awans za wszelką cenę: albo lepsza firma albo wojna o stanowisko z innymi. To stanowisko będzie moje.
    * Rola: science / bio officer + medical. Młoda podkochująca się w Eustachym :D. Złota, płonąca skóra z błękitnymi włosami, może robić wyładowania
* Jan Lertys
    * Ocean: ENCAO: -0-0+
        * Zapominalski; non stop trzeba coś przypomnieć
        * Obserwator; raczej stoi z boku i patrzy niż działa bezpośrednio
        * Łatwo wywrzeć na tej osobie wrażenie
    * Wartości:
        * TAK: Humility, TAK: Universalism, TAK: Self-direction, NIE: Achievement, NIE: Security
    * Silnik:
        * TAK: Pokój: Kapuleti i Monteki MUSZĄ żyć w pokoju. Unikajmy starć, idźmy w pacyfizm. Wygaszanie konfliktów, harmonia.
        * TAK: Ratunek przed chorą miłością: TYLKO NIE Celina -> Eustachy. Pls.
    * Rola: First Mate (siłowy / ładownia); chce zejść do bazy bo tu pracuje jego ukochana Daria
* Emilia d'Infernia
    * "żona" Bartłomieja. Żywy arsenał. Egzekutor Bartłomieja. (Emilia -> "Egzekucja MILItarnA")


### CES Purdont

* Wiktor Turkalis
    * Ocean: ENCAO:  0+0-0
        * Prostacki, prymitywny, nie dba co myślą inni
        * Nieśmiały, skrępowany i często zażenowany
    * Wartości:
        * TAK: Humility, TAK: Family, TAK: Face, NIE: Achievement, NIE: Conformity
    * Silnik:
        * TAK: Do the right thing: niezależnie od tego co jest wygodne czy tanie, własne zasady i własny kompas moralny są najważniejsze. By spojrzeć w lustro.
        * TAK: Papa Smerf: zawsze doradza, daje dobre rady. Chce być pomocny i dać rady które naprawdę pomogą. Chce, by za jego radami KTOŚ poszedł i by było mu lepiej.
        * NIE: Tępić debili: nienawidzę głupoty do poziomu NUKLEARNEGO. Niech cierpią. Sama przyjemność. I zemsta.
    * Rola: przełożony 
* Kamil Wraczok
    * Ocean: ENCAO:  +0---
        * Nigdy się nie nudzi, lubi rutynę i powtarzalne rzeczy
        * Nie dba o potrzeby innych, tylko o swoje
        * Ryzykant, zawsze rzuca się w ogień
    * Wartości:
        * TAK: Self-direction, TAK: Face, TAK: Stimulation, NIE: Security, NIE: Conformity
    * Silnik:
        * TAK: Wędrowny mistrz Ryu: chcę być coraz lepszy w Czymś, chcę trudnych wyzwań, chcę się wykazać. There is always someone stronger.
        * TAK: Prowokator: podkręcanie atmosfery, by inni działali ostro pod stresem. Obserwowanie ich prawdziwej natury.
    * Rola: P.O. bazy, próbuje wszystko ukryć i naprawić sprawę samemu.
* Daria Triazis
    * Ocean: ENCAO:  -0+00
        * Polega tylko na sobie; nie wierzy w innych
        * Szczególnie rozsądny i poczytalny
    * Wartości:
        * TAK: Conformity, TAK: Hedonism, TAK: Self-direction, NIE: Universalism, NIE: Humility
    * Silnik:
        * TAK: Odnaleźć dziecko: znaleźć i uratować zaginioną osobę, która jest poza zasięgiem.
        * TAK: Wyplątać się z demonicznych długów: muszę jeść by przeżyć i wpakowałem się w ostre kłopoty z siłami, które się nie patyczkują. Spłacić lub uciec.
    * Rola: pracuje jako inżynier terraformacji; atm zarażona. Lertys jest w niej zakochany.
* Joachim Puriur
    * Ocean: ENCAO:  0--+-
        * Poważny, unika żartów
        * Jeżeli czegoś chce, weźmie to dla siebie; zazdrosny i zawistny
        * Szlachetny, skupia się na czynieniu dobra
    * Wartości:
        * TAK: Security, TAK: Tradition, TAK: Face, NIE: Humility, NIE: Hedonism
    * Silnik:
        * TAK: Utopia Star Trek: znajdowanie porozumienia i dobra nawet w osobach bardzo różnych od nas. Współpraca i koegzystencja.
* Kordian Olgator
    * Ocean: ENCAO:  --+00
        * Polega tylko na sobie; nie wierzy w innych
        * Stabilny, opoka niemożliwa do ruszenia, niemożliwy do wyprowadzenia z równowagi
        * Przewidujący, trudny do zaskoczenia - na wszystko ma plan awaryjny
    * Wartości:
        * TAK: Power, TAK: Universalism, TAK: Stimulation, NIE: Conformity, NIE: Family
    * Silnik:
        * TAK: Preserver: "this planet is dying..." - TO jest ważne i piękne i niewidoczne! Muszę mieć pomoc byśmy wszyscy TEGO nie stracili! GM w umierającym MMO.
        * TAK: Red Queen Race: jeśli stoisz w miejscu,

### Infernia

* Bartłomiej Korkoran, wuj i twarda łapka rządząca Infernią.
    * Ocean: ENCAO:  +-000
        * Bezkompromisowy, nieustępliwy, niemożliwy do zatrzymania
        * Ciekawski, wszystko chce wiedzieć i wszędzie wsadzić nos
    * Wartości:
        * TAK: Family, TAK: Self-direction, TAK: Achievement, NIE: Tradition, NIE: Humility
    * Silnik:
        * TAK: Inkwizytor: niszczenie fake news, publiczne pokazywanie hipokryzji, ujawnianie bolesnych prawd
        * TAK: Kolekcjoner: kolekcjonuje coś rzadkiego ()
    * Rola: wuj, kapitan Inferni
* Celina Lertys, podkochująca się w Eustachym 
    * Ocean: ENCAO:  --+-0
        * Stanowczy i silny, zdecydowany
        * Skryty; zachowujący swoją prywatność dla siebie
        * Nigdy nie wiadomo, co jest prawdą a co maską
    * Wartości:
        * TAK: Universalism, TAK: Tradition, TAK: Security, NIE: Power, NIE: Humility
    * Silnik:
        * TAK: Harmonia naturalna: balans między technologią i naturą, utrzymanie cudów natury. Lasy Amazonii.
        * NIE: Awans za wszelką cenę: albo lepsza firma albo wojna o stanowisko z innymi. To stanowisko będzie moje.
    * Rola: science / bio officer + medical. Młoda podkochująca się w Eustachym :D. Złota, płonąca skóra z błękitnymi włosami, może robić wyładowania
* Jan Lertys
    * Ocean: ENCAO: -0-0+
        * Zapominalski; non stop trzeba coś przypomnieć
        * Obserwator; raczej stoi z boku i patrzy niż działa bezpośrednio
        * Łatwo wywrzeć na tej osobie wrażenie
    * Wartości:
        * TAK: Humility, TAK: Universalism, TAK: Self-direction, NIE: Achievement, NIE: Security
    * Silnik:
        * TAK: Pokój: Kapuleti i Monteki MUSZĄ żyć w pokoju. Unikajmy starć, idźmy w pacyfizm. Wygaszanie konfliktów, harmonia.
        * TAK: Ratunek przed chorą miłością: TYLKO NIE Celina -> Eustachy. Pls.
    * Rola: First Mate (siłowy / ładownia); chce zejść do bazy bo tu pracuje jego ukochana Daria
* Emilia d'Infernia
    * "żona" Bartłomieja. Żywy arsenał. Egzekutor Bartłomieja. (Emilia -> "Egzekucja MILItarnA")

