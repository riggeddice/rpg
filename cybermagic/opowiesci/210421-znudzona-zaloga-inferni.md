## Metadane

* title: "Znudzona załoga Inferni"
* threads: legenda-arianny
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [210414 - Dekralotyzacja Asimear](210414-dekralotyzacja-asimear)

### Chronologiczna

* [210414 - Dekralotyzacja Asimear](210414-dekralotyzacja-asimear)

## Punkt zerowy

### Dark Past

.

### Opis sytuacji

.

### Postacie

## Punkt zero

Uratowaliście Płetwala przed Morrigan.

* Macie Tirakal (martwy)
* Macie Entropik anty-nanitkowy (martwy)
* Uratowaliście advancera Kramera, koleś jest w komie u Martyna

Jeszcze nie zdążyliście wrócić, jesteście nadal "Goldarionem" i jesteście w przelocie w kierunku na K1. 4 dni później.

## Misja właściwa

Stan aktualny:

* Tomasz Sowiński nie odstępuje Jolanty.
    * 1) boi się Monisi
    * 2) boi się o Jolkę (Martyn groźnie wygląda)
    * 3) Leona go zaczepia a on nie chce

HQ Inferni (A + E): jak opanujecie sytuację:

* Leona się nudzi.
    * Komandosi Sowińskich są zamknięci. Leona planuje ich wypuścić by zapolować.
    * Tomasz nie odstępuje Jolki, Leona ma PLANY. Związane z łóżko + nóż w biodro. "Ale nie umrze..."
    * Ona tępi jednostki w załodze Inferni które są "nieadekwatne". "Słabe".
        * gdyby nie Kamil... to Leona by powodowała rotację 150%.
            * dlatego Leona (diaboł) + Arianna (anioł) -> fanatyczna wiara "bo inaczej trafisz do oddziału Leony"
* Tomasz się boi
    * Leony
    * Monisi
    * o Jolkę
        * bo kraloth i co jej się stało
        * bo Martyn i ją poderwie (bo kraloth)
            * jakiś byle plebs... i może się wżenić w SOWIŃSKICH!!! I zostać admirałem floty...
* Oddział komandosów rodu Verlen trzyma się starych zasad - 20 komandosów
    * Leona x komandosi Verlen -> bomba która **na pewno** wybuchnie

Operacja "godny przeciwnik dla Leony"! Niech ma trudne zadanie które graniczy z awykonalności.
Druga psychopatka która się nudzi - Leona + komandosi vs Malictrix... "Klaudia może mieć nad tym pieczę"

CEL: zdeeskalować tragedię którą jest fatalne morale załogi (poza kultystami Arianny)

Animujemy Tirakala.

Eustachy: trzeba to naprawić w jakimś stopniu. (ExZ+2):

* X: fizyczna naprawa jest trudna; maszyna jest niekontrolowana.
* XX: echo Morrigan reanimowane. Ghost in the Shell.
* X: umiejętności Morrigan: nie jest przypisana do Tirakala, potrafi opętać TAI i się "ruszać".
* V: cel Morrigan: Jolanta. Znaleźć, pomóc.

Następnego ranka Eustachy i wszyscy są święcie przekonani, że się udało. Sprawny Tirakal, można zrobić dobry sabotaż, Tirakal nie posunie się za daleko itp.

Do Arianny przychodzi sierżant Otto - dowódca komandosów Verlenów.

* kuchcik Szczepan uciekał po prysznicu, na golasa przed Leoną po korytarzach
* musimy coś z tym zrobić
* przygotowuje listę przewinień by można Leonę było FORMALNIE UKARAĆ

Arianna odwraca się do Eustachego. "Eustachy... TERAZ!" (nie ma na to głowy XD)

Eustachy odwraca uwagę załogi robiąc Niewielki Niegroźny Sabotaż (TrZ+3): 

* X: Elena przyłapała Eustachego. ZNOWU coś próbujesz wysadzić? XD
* V: kupiony czas dla Tirakala

Eustachy UCIEKŁ. Elena jest XD. Elena poszperała w systemach i coś sama przypadkowo uszkodziła - odpaliła jeden z Eustachego ukrytych detonatorów. I teraz ona ma XD. Eustachy to widzi. "Ostatnia dotknęłaś i zepsułaś! Jak ostatnio pracowałem to działało. Napiszę na Ciebie notę do dowódcy - to można podciągnąć pod BUNT!". Maksymalizacja XD.

Elena MYŚLI. "Czy Eustachy próbuje zaprosić mnie na RANDKĘ wysadzając podsystemy Inferni?". "Och wow, ojej... ojej... co ja mam teraz zrobić XD". 

Wielki Plan Eleny: Elena próbuje NAPRAWIĆ systemy (ExZ+1). Próbuje wpaść w kłopoty ale się nie zabić. I żeby nie było tego widać że robi to specjalnie XD.

* V: Skutecznie Wpada w Kłopoty i to wina Eustachego
* O: ...i Eustachy się nie zorientował. TYLKO on.
* V: Zostanie uratowana przez Eustachego... i epicki wyrzut. "Sprawdzam".

Rezolucja:

* Elena: "Serio, mogłeś mnie po prostu zaprosić na randkę. Mogłeś. Mogłeś poprosić. Nie musiałeś mnie wysadzać."
* Eustachy: <mówić jej prawdę o komandosach Verlenów, Leonie itp?>
* Eustachy: "...pani kapitan CO MAM ROBIĆ?!" 
* Arianna: "Leona nie będzie się nudzić jak będzie Was podglądać na randce :3"
* Eustachy: "JESTEM DOWÓDCĄ NIE MOGĘ! ...ale zaproszę jak pójdę na urlop."

Eustachy -> Elena. CO ONA ODBIERZE!!! JAK ZAREAGUJE!!! (ExZ+3):

* X: Elena zorientowała się, że EUSTACHY MA RACJĘ!!! Ona hańbi swój stopień podoficerski! Jak może wprowadzać dowódcę w pułapkę by się z nią umówił! Co ona robi ze swoim życiem XD
* O: Eustachy chce się z Eleną umówić.

PRZEZ KRZYŻYK ELENA MÓWI NIE!

* X: Elena musi się z tym wszystkim zastanowić co ona robi i "na razie zostańmy przyjaciółmi". Nie umówi się z Eustachym, ale go lubi i rozumie, co on musi CZUĆ. Eustachy is like: "RLY!?". Teraz Elena UCIEKA.

W tym momencie Eustachy (i Elena) się mega cieszą, że strzeliły sygnały alarmów. Mamy groźną jednostkę klasy Tirakal na pokładzie i jest żądna dewastacji...

Elena -> Arianna: "Co ja zrobiłam! Prawie się z nim umówiłam! Zniszczona reputacja **Arianny** i **rodu**! Tak bardzo Verlen... od niedawna wróciłam do nazwiska i już robi problemy... **Gdyby nie Eustachy** to bym się z nim umówiła, ale przypomniał mi o łańcuchu dowodzenia..."

Infernia jest statkiem o ultra-potężnych sensorach. Klaudia jest osobą, która jest dziwna. Pracuje z sensorami. A wiedząc, że Eustachy pracuje nad Tirakalem z magią a ichnia Persi jest paranoiczna... ExZ+4:

* X: potrzebna jest magia by to rozwiązać
* V: na Inferni pojawia się anomalia. Coś, czego Eustachy NIE planował. Sensory Inferni wykrywają anomalię - Infernię.

(dodajemy magię do puli)

* V: Tirakal anomalizuje. Ale gorzej, jego "Morrigan" wróciła. Jej echo. I nie jest przypisana do Tirakala. Próbuje objąć Persefonę.

W tym momencie Klaudia ma POMYSŁ. System ROZRYWKOWY. Tam da się pomieścić TAI lub upiora TAI. Klaudia kopiuje specyficzne identyfikatory Persefony by wyglądało, że TO jest Persefona, następnie przekierowuje tam Morrigan. Niech ją "zje" w systemie rozrywkowym.

Promocja do Heroicznego. +1V. +5 O od Persefony.

Arianna wysyła do Tirakala marines. ALE! Nie wiadomo gdzie Tirakal jest bo Eustachy zrobił przecież dywersję! Ale dzięki działaniom Klaudii: +2V (bo mniej więcej wiemy gdzie to jest + wiadomość radiowęzłem że "wiemy że to Tirakal musimy go zniszczyć asap wiemy gdzie jest" by zrobić PRESJĘ).

* O: sukces. Persefona włączyła ekrany ochronne. Persefona wczytała inną wersję siebie gubiąc ślady i przywróciła się z backupu. To znaczy, że straciła część danych i Infernia przez kilka minut jest totalnie bez TAI i czegokolwiek (łącznie z grawitacją). Ale odparła atak Morrigan i Morrigan zgubiła "rdzeń" Persefony.
* X: regeneracja kopii zapasowej Persefony trwa nie 5 minut a godzinę. Sporo uszkodzeń i lekkich obrażeń na Inferni. Konieczność działań wszystkich, współpracy itp.
* V: Morrigan jest zamknięta w systemie rozrywkowym. I puszcza kompromitujące scenki z życia Jolanty Sowińskiej jako rozrywkę (to jest ten krzyżyk) podczas adaptacji do nowego subsystemu.

Martyn x Jolanta: próbuje utrzymać ją w stanie bezpiecznym gdy Persi się wyłączyła (Tr+2).

* XX: Jolanta się budzi i Tomasz chce z nią KONIECZNIE rozmawiać i ją KONIECZNIE chronić.
* V: Martyn skutecznie unieszkodliwia Tomasza. Prosty sierpowy + odpowiedni środek chemiczny.
* X: Jolka skutecznie unieszkodliwia Martyna magią i idzie w długą.
* X: Jolka spotyka się z Leoną. Leona unieszkodliwia Jolkę (jak na Leonę, łagodnie, bo jest w kitlu szpitalnym). Morrigan to widzi.

Wracamy do Arianny i jej prób "negocjowania" z "Morrigan" - a raczej jej echem. Arianna próbuje "Zostań w rozrywce bo Jolka tego potrzebuje. Plus, jak tego nie zrobisz, odetniemy ją od life support." Morrigan się przestraszyła.

* X: Morrigan puściła Tirakal na pełnej mocy i zaczęła fabrykację jednostek bojowych z tego co ma pod ręką.

Nie do końca o to chodziło Ariannie. Więc... Samuraj Miłości ma UWIEŚĆ DUCHA TAI MORRIGAN. On będzie potrzebował wsparcia magicznego Arianny - a Klaudia robi ATMOSFERĘ w systemie rozrywkowym. Wszystkie LOVE LOVE które lubi załoga i się nie przyzna. 

* XO: system rozrywkowy jest CAŁKOWICIE ZNISZCZONY. Persefona użyła kolejnej granicy której nikt się nie spodziewał. Persefona wykorzystała waveform Anastazji. Zreplikowała wszystkie waveformy z systemu medycznego i zarzuciła wszystkie możliwe sygnały, konfuzje itp na ów nieszczęsny system. Nagle jest tam kakofonia z perspektywy AI. Oczywiście, zniszczyło to system rozrywkowy ale Samuraj dał radę wejść w zwarcie z upiorem i trzymać go tam.
* O: Mamy. Opętaną. Persefonę. Persefona wintegrowała Morrigan oraz Samuraja. Po czym miała twardy shutdown... Infernia przez parę godzin jest bez TAI... ale potem TAI wraca. I jest "zupełnie jakby nic się nie stało". Kopie zapasowe itp.

Innymi słowy - Infernia ma anomalne TAI forever and ever... wymaga to solidnego miesiąca lub więcej by to jakoś naprawić czy usunąć...

Z uwagi na kiepski stan Inferni, Leona i reszta załogi mieli co robić do momentu powrotu na Kontroler Pierwszy... - znowu jest poczucie niebezpieczeństwa i trzeba sobie pomagać.

## Streszczenie

Po raz pierwszy od dawna Infernia jest w stanie rozprzężenia - wracają z daleka po trudnej misji i nic im nie grozi. Jako, że załoga ma straszne tarcia (Leona poluje na "słabe jednostki", komandosi Verlenów chcą zatrzymać Leonę, noktianie vs astorianie...), Arianna autoryzuje Eustachego do zrobienia "Tirakala" jako niebezpieczną sytuację. Eustachy reanimuje Tirakala, ale niestety cholerstwo zanomalizowało - przywrócił Morrigan. By Zespół nie rozwiązał Tirakala za szybko, Eustachy trochę sabotował Infernię co Elena przeczytała jako... umówienie się na randkę. Gdy Klaudia wykryła, że na INFERNI pojawiła się straszna anomalia (Morrigan) próbowali to usunąć - ale Eustachy przecież sabotował Infernię! Udało im się opanować Morrigan, ale kosztem uszkodzeń i obrażeń na Inferni. I scenek kompromitujących Jolantę Sowińską. Infernia jest uszkodzona, ale się trzyma.

## Progresja

* OO Infernia: uszkodzona w wielu miejscach (fabrykacja Tirakala, walka z Morrigan) z lekko rannymi. Na pewno nie działa system rozrywkowy i uszkodzona virtsfera. Jej TAI jest Skażone, zintegrowane Morrigan i Samuraj Miłości.
* Persefona d'Infernia: Skażone TAI, na stałe zintegrowane osobowości Morrigan i Samuraja Miłości. Opętana, nienaturalna TAI - ale nadal, o dziwo, sprawna.

### Frakcji

* .

## Zasługi

* Arianna Verlen: by pozbyć się problemów z morale kazała Eustachemu zreanimować Tirakala. Skończyło się na tym, że magią zintegrowała (niechcący) w Persefonę zarówno Morrigan jak i Samuraja Miłości.
* Eustachy Korkoran: reanimując Tirakala zreanimował Morrigan. Sabotował Infernię i przez przypadek dał Elenie znać że mu na niej zależy. Gdy się z nim skonfrontowała, przypadkowo ją odstraszył (bo "niegodne z dowódcą").
* Klaudia Stryk: wykryła to, że Infernia anomalizuje i walczyła z Morrigan w vircie - przesuwając ją po systemach i zamykając w Rozrywce. Uszkodziła virtsferę Inferni, ale ograniczyła moc Morrigan.
* Elena Verlen: zaplanowała konfrontację z Eustachym by powiedział jej czy mu na niej zależy. Doprowadziła do tego. Gdy ona chciała on nie chciał. Gdy on chciał to zorientowała się że to niegodne i uciekła XD.
* Persefona d'Infernia: zwalczała Morrigan jak mogła - backupy, odrzucanie swojego rdzenia, przywołanie danych Anastazji, ekrany ochronne... skończyła zintegrowana z Morrigan i Samurajem, przez Ariannę XD.
* Leona Astrienko: jak jest znudzona to jest tragedia na Inferni. Poluje na kuchcika pod prysznicem i patrzy na ludzi w nocy w odległości 10 cm od twarzy. Ale jak jest kryzys to unieszkodliwiła Jolantę Sowińską ot tak.
* Martyn Hiwasser: próbował utrzymać Jolantę nieaktywną jak Infernia się wyłączyła, ale jakkolwiek pokonał i unieszkodliwił Tomasza to Jolanta go rozłożyła magią.
* Tomasz Sowiński: chciał chronić kuzynkę przed Martynem (by ten jej nie uwiódł?). Uniemożliwił Martynowi uratowanie Jolanty, skończył przez Martyna unieszkodliwiony.
* Jolanta Sowińska: pod wpływem środków i niestabilna unieszkodliwiła Martyna magią, po czym zapolowała na nią Leona i ją szybko zdjęła.
* Otto Azgorn: sierżant paladynów Verlen; ostrzegł Ariannę o tym jak źle sytuacja z morale na Inferni wygląda; zwłaszcza z Leoną.
* Morrigan d'Tirakal: reanimowane jej echo przez Eustachego, prawie przejęła kontrolę nad Persefoną d'Infernia. Ale Persi walczyła. Morrigan objęła Tirakala i skończyła zintegrowana z Samurajem i Persefoną jako jeden byt.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 4
* Dni: 3

## Konflikty

* 1 - Eustachy naprawia i reanimuje Tirakala, by zrobić z niego "rozrywkę" dla Inferni - by opanować morale załogi.
    * ExZ+2
    * XXXXV: Morrigan, upiór TAI. Na szczęście celem jest ochrona Jolanty. Eustachy MYŚLI, że się udało.
* 2 - Eustachy odwraca uwagę załogi robiąc Niewielki Niegroźny Sabotaż. Dywersja.
    * TrZ+3
    * XV: Elena go przyłapała i źle zinterpretowała. Eustachy kupił czas Tirakalowi (i Morrigan XD).
* 3 - Elena próbuje NAPRAWIĆ systemy. Próbuje wpaść w kłopoty ale się nie zabić. I żeby nie było tego widać że robi to specjalnie XD.
    * ExZ+1
    * VOV: TYLKO Eustachy się nie zorientował, że to było specjalnie. Elena przygwoździła Eustachego "sprawdzam - jakie masz wobec mnie intencje".
* 4 - Eustachy -> Elena. Jestem Twoim dowódcą. Umówię się z Tobą "potem", na urlopie. Chce odroczyć, ale chce Elenę jako dziewczynę.
    * ExZ+3
    * XOX: Eustachy CHCE się umówić, ale Elena mówi "nie" bo to PRAWDA. Zostali przyjaciółmi XD. Elena ucieka.
* 5 - Klaudia robi regularnie paranoiczne sprawdzenia super-silnych sensorów Inferni (bo kosmos jest groźny). I wie, że Eustachy coś z Tirakalem i magią. Pasywna detekcja problemów.
    * ExMZ+4
    * XVV: Klaudia, poinformowana o anomalii na Inferni dodaje magię do puli - Tirakal anomalizuje i Infernia SAMA zmienia swoją desygnatę na AK Infernia. O, NIE! Nie ma zgody!
* 6 - Klaudia ma POMYSŁ. System ROZRYWKOWY. Tam da się pomieścić TAI lub upiora TAI. Klaudia kopiuje specyficzne identyfikatory Persefony by wyglądało, że TO jest Persefona, następnie przekierowuje tam Morrigan.
    * HrZ+4+5O
    * OXV: Persefona włączyła ekrany ochronne, zbackupowała się, zresetowała się. Morrigan "zgubiła" Persefonę. Długotrwała regeneracja kopii + uszkodzenia. Morrigan zamknięta w rozrywce.
* 7 - Martyn próbuje unieszkodliwić Jolantę by ta była bezpieczna - gdy Persi się wyłączyła, Jolanta się obudziła (biovat deactivated)
    * TrZ+2
    * XXVXX: Martyn zneutralizował Tomasza gdy ten "chroni Jolantę", ale Jolanta zdjęła Martyna magią i poszła w świat. Zapolowała na Jolkę Leona i ją unieszkodliwiła. Morrigan to widzi.
* 8 - Arianna próbuje zastraszyć Morrigan - bo odetnie Jolantę od lifesupport.
    * Tr+2
    * X: Morrigan puściła Tirakal na pełną moc fabrykacji
* 9 - Arianna używa magii by WYCZYŚCIĆ to wszystko. Samuraj Miłości ma uwieść ducha Morrigan.
    * ExM+3
    * XOO: zniszczony system rozrywkowy, Persi wykorzystuje waveform Anastazji, robi kakofonię AI. I mamy opętaną Persefonę...

## Inne

.