## Metadane

* title: "Kapitan Verlen i koniec przygody na Królowej"
* threads: kosmiczna-chwala-arianny
* motives: aurum-program-kosmiczny, rekonstrukcja-ruiny
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [221019 - Kapitan Verlen i pierwszy ruch statku](221019-kapitan-verlen-i-pierwszy-ruch-statku)

### Chronologiczna

* [221019 - Kapitan Verlen i pierwszy ruch statku](221019-kapitan-verlen-i-pierwszy-ruch-statku)

## Plan sesji
### Theme & Vision

* Ciężka praca i małe sukcesy odbudowują bezużyteczny statek kosmiczny
* Starcia pomiędzy frakcjami są dewastujące i letalne -> dlatego Orbiter jest taki jaki jest, struktura "hierarchiczno-militarna"
    * Jednak pojedyncze jednostki Orbitera próbują coś z tym zrobić i naprawić

### Fiszki

Kto jest na statku (55 osób max)

#### Dowodzenie

* Arianna Verlen: kapitan Królowej Kosmicznej Chwały
* Daria Czarnewik: engineering officer, (4 inż pod nią)
* Alezja Dumorin: eks-kapitan, Orbiter
    * ENCAO:  +0--0 |Amoralna, skuteczna| VALS: Hedonism, Face, Power| DRIVE: Wędrowny mistrz Ryu
* Władawiec Diakon: pierwszy oficer, tien, (p.o. Stefana) 
    * ENCAO:  +-0-0 |Intrygancki;;Żywy wulkan| VALS: Hedonism, Self-direction| DRIVE: Follow My Dreams, Korupcja anioła
* Klaudiusz Terienak: medical officer, tien, (2 med pod nim) 
    * ENCAO:  +-000 |Bezkompromisowy, nieustępliwy| VALS: Power, Stimulation| DRIVE: Przejąć władzę
* Grażyna Burgacz: logistyka i sprzęt (officer), tien (3 osoby pod nią) 
    * ENCAO:  -0+-- |Powściągliwa i 'nudna';;Ascetyczna| VALS: Humility, Tradition| DRIVE: Starszy Brat
* Arnulf Perikas: fabrykacja i produkcja (officer), tien (2 inż pod nim) 
    * ENCAO:  0-0-- |Hardheaded;;Napuszony| VALS: Tradition, Family| DRIVE: Wzbudzenie zachwytu
* Maja Samszar: comms officer, tien (p.o. Klarysy jak K. nie może) 
    * ENCAO:  0+-0- |Moralizatorska| VALS: Stimulation, Conformity >> Power| DRIVE: Długi

#### Operacja

* Klarysa Jirnik: artillery, (p.o. Mai jak Maja nie może) 
    * ENCAO:  00+-- |Wymagający;;Małostkowy| VALS: Tradition, Conformity|DRIVE: Sprawiedliwość
* Stefan Torkil: pilot, K1
    * ENCAO:  -+--0 |Nie lubi ryzyka;;Spontaniczny| VALS: Benevolence >> Stimulation| DRIVE: Odbudować reputację
* Tomasz Ruppok: starszy mat (13 osób + 2 starszych)
    * ENCAO:  +0--- |Mało punktualny;;Kłótliwy;;W przejaskrawiony sposób okazuje uczucia| VALS: Hedonism >> Achievement, Family| DRIVE: Apokalipta
* Rufus Warkoczyk: starszy mat (13 osób + 2 starszych)
    * ENCAO:  0-+-0 |Zarozumiały;;Bezkompromisowy, niemożliwy do zatrzymania;;Zorganizowany| VALS: Self-direction, Power| DRIVE: Odkrycie konspiracji ("ktoś nas sabotuje")
* Marcelina Trzęsiel: inżynier syntezy (pod Arnulfem), pod opieką tien Terienaka
    * ENCAO:  0+0-+ |Małostkowa;;Dramatic shifts in mood;;Różnorodność form| VALS: Hedonism, Achievement >> Security| DRIVE: "Co za tą górą"

#### Infiltracja / Starcie

* Erwin Pies: kapral marine, Orbiter (w sumie z nim - 4 marines)
    * ENCAO:  -0-+- |Skryty;;Wiecznie zagubiony;;Praktyczny| VALS: Tradition, Stimulation >> Power| DRIVE: Ochraniać słabszych
* Leona Astrienko: marine, Orbiter
* Szymon Wanad: marine, Orbiter
* Tomasz Dojnicz: marine, Orbiter
    * ENCAO:  --0+0 |Nie znosi być w centrum uwagi;;Dusza towarzystwa| VALS: Conformity, Hedonism >> Power| DRIVE: Arlekin Maytag
* Szczepan Myrczek: advancer, tien, seilita,
    * ENCAO:  --+0- |Stabilny emocjonalnie;;Zawsze bardzo zajęty| VALS: Face, Achievement, Hedonism >> Self-direction| DRIVE: Zasady są święte
* Mariusz Bulterier: advancer, tien, seilita, sybrianin 
    * ENCAO:  0-+-- |Prostolinijny i otwarty;;Nie kłania się nikomu;;Uroczysty i poważny| VALS: Humility, Tradition| DRIVE: Nawracanie
* Hubert Kerwelenios: advancer, sarderyta
    * ENCAO:  -0+-0 |Kontemplacyjny, refleksyjny;;Kompleks paladyna| VALS: Humility, Tradition >> Stimulation| DRIVE: Odbudowa i odnowa

### Co się wydarzyło KIEDYŚ

* .

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

* .

### Co się stanie (what will happen)

* High_level: dolecieć do Karsztarina i wprowadzić advancera na pokład
    * jak się uda, morale++
    * pilot (dolecieć, stabilizacja), synteza, dwaj advancerzy
* S1: Arnulf: 
    * znajduje z Myrczkiem zapasy narkotyków i alkoholu należącego do nie-Aurum (gniew Ruppoka); Klaudiusz im też zapewnia medical.
    * Grażyna jest za tym, by jednak pozwolić i nie niszczyć
    * => Aurum vs nie-Aurum. Dyscyplina vs morale.
* S2: Arnulf: 
    * chce pozyskać brakujące komponenty z Karsztarina; dodatkowy subcel; ma mało xarimatów ale bardzo mało rzadkich substancji (req: Arianna -> Orbiter)
    * chce by Daria pomogła mu z kalibracją i korektą syntezatora
    * niech Arianna pomoże mu w tym ZANIM przyleci skiff z "Tucznika Drugiego". <-- już załatwił by się udało
    * => Aurum vs Orbiter. Wsparcie sojusznika (Arnulf, face, surowce) czy zgodnie z zasadami?
* S3: Klaudiusz:
    * chce ukryć fakt istnienia Marceliny, który jednak zaczyna być "słyszany"
    * Arianna ma problem w załodze <- medical officer sucks? He wants girls?
* S4: Pies: 
    * chce wyprodukować siedem skafandrów by zrobić ćwiczenia marines i advancerów

.

* Pierwszy oficer - Władawiec Diakon - podrywa na lewo i prawo bawiąc się hormonami.
    * W medycznym jest tien Klaudiusz Terienak który chce zdobyć uznanie Władawca, ale boi się Leony.
* Aurum podzieliło się na frakcje i próbują wygrywać między sobą.
* STRASZNA nieufność Orbitera do Aurum

### Sukces graczy (when you win)

* .

## Sesja właściwa
### Wydarzenia przedsesjowe

.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Arianna chce przegrzać silniki. Chce zrobić próbny przelot i próbne manewry. Ten statek dawno nie latał, przed ćwiczeniami trzeba go rozruszać. Przy okazji to jest mniejsza presja na wszystkich. "To nie ich wina to wina statku". Daria patrzy z wyrzutem.

Daria uruchamia statek do pełnego działania - diagnostyka, komponenty, paliwo itp (dyskretnie kapral Pies rozkłada marines przy rzeczach możliwych do sabotażu). Daria ma Semlę, ma inżynierów, jest gotowa. Zrobiła wielokrotne testy. WIE WSZYSTKO. Rozstawia inżynierów w kluczowych punktach.

TpZ (rozstawienie, testy itp.) +4 +3Or:

* X: lot statku spowoduje, że niektóre strukturalne elementy będą wymagały naprawy. Ale - zadziała. Jakiś dzień. To będzie GŁOŚNE i STRASZNE ale nic ważnego. Patchwork.
* V: statek jest 'cleared'. Będzie działał. Będzie wydawał dźwięki jak zarzynana metalowa świnia, mogą być jakieś mikro-efemerydy, ale będzie działać.
    * Ten statek pochodzi z Anomalii Kolapsu, to jest patchwork kilku statków z AK. Dzięki temu jest dość tani, jest sprawny, ale wiesz.
        * Chyba nikt nie zakładał, że on naprawdę będzie robił jakieś poważne rzeczy
* Vr: statek przetestowany, wszystko przeszło. Lot jest konfliktem TYPOWYM.
* X: niestety, statek jeszcze będzie wymagał testów diagnostyki. Zbyt posiekany, zbyt patchworkowy. Jeszcze miesiąc i Daria go opanuje.

Arianna kazała Stefanowi zrobić kilka kółek Królową. Nic wyczynowego, 30% mocy silników, trochę pomanewrowania, trochę poskanowania - minimalne użycie wszystkich podsystemów. Co działa, co nie działa, ale też -> żeby się udało. Happy path. I jak coś nie działa to powtarzamy aż zacznie.

TpZ (Daria zrobiła diagnostykę + załoga przećwiczona) +4:

* V: faktycznie, statek przeleciał kilka kółek. Udało się.
* V: poprawione morale załogi. Coś z tego jest. Jakoś to funkcjonuje.
* V: Daria wie gdzie są potencjalne problemy.
* V: to, że Królowa zaczyna działać, acz powoli, zostało Ariannie zauważone w Orbiterze.
* V: Arianna kazała podnieść podsystemy, elementy itp, trudność akrobacji. MIMO WSZYSTKO statek działa. Na 50%. I się nie rozpadł. I po tym jak NAJPIERW strzeliły te komponenty, nic nowego nie strzeliło. Większość takich "uładnień z Aurum" poszła.

Załoga jest naprawdę podbudowana. Ten statek zaczyna latać. Ten statek zaczyna nie być strusiem ani kiwi. Mimo stresu i odpadających rzeczy - statek jest "sprawny". Wszystkie systemy są zielone. Załoga poradziła sobie w bezstresowej sytuacji. Jakieś niekrytyczne systemy są żółte, Daria to naprawi z inżynierami w 1 dzień.

Arianna jest przez MOMENT szczęśliwa. A potem... zatrzymaliście się. Po ćwiczeniach. Załoga sobie gratuluje, biją brawo. Całkowicie nieprofesjonalnie, ale szczerze. A Daria na sam koniec jak wszystko okrzepnęło - gratuluje inżynierom. Szczerze i uczciwie. Zasłużyli. Nawet ten co do Wanada przegrał koleżankę w karty.

Godzinę później, do Arianny przyszedł Arnulf. Advancer Myrczek znalazł ALKOHOL i FABRYKATOR NARKOTYKÓW / ALKOHOLU i oprócz tego KART i GIER KARCIANYCH, ŻETONÓW itp. Innymi słowy, Myrczek znalazł to, z czego Ruppok robi problemy. Arnulf tak patrzy na Ariannę. "To ile będzie batów?"

Arianna "nie będziemy batożyć." -> one używały też elementów bio syntezatorów Królowej. ORAZ elementów z medical. Mało istotnych, ale zawsze. Tylko Arnulf i Myrczek wiedzą o tym alkoholu i narkotykach. I tak, użyte były elementy bio z fabrykatorów.

Arianna wie, że ludzie tu są spoza Aurum; traktują to jako przywilej. Arianna idzie do Klaudiusza i "dodaj leki na przeczyszczenie, nic mocnego, ale nieprzyjemne ma być". Klaudiusz ma podejrzanie nerwową minę. "Pani kapitan, czy to dobry pomysł? Oni teraz z panią współpracują." Arianna "trzeba postawić granice". Klaudiusz chciał coś dodać, ale zmienił zdanie. Coś grzeczny się zrobił.

DZIEŃ 3: 

Kible się nie zamykają. Ludzie cierpią. Klaudiusz się zna na rzeczy. Arianna się uśmiecha.

Arianna zleca Klaudiuszowi żeby pomógł w objawach tym najbardziej cierpiącym, potem zaczęła śledztwo - zleca to Psu. Pies ma się dowiedzieć skąd to się wzięło. Pies dotarł do źródła informacji - Arianna kilka godzin później wiedziała, że przyczyną jest alkohol i narkotyki. Ruppok spieprzył partię. Arianna ma imię producenta i to inni nadali na niego. Czyli Arianna ma możliwości punitywne.

Arianna wzięła Ruppoka na poważną rozmowę. On jest za kasę, czemu to zrobił, co ma na swoją obronę. "Pani kapitan, wszystko jest winą Arnulfa lub Klaudiusza. Mają za stare lub niesprawne fabrykatory. Żadna inna partia nie miała kłopotów." - i tak SZCZERZE - "Rozumie pani kapitan, że to nie moja wina."

Nie o to Ariannie chodziło. "Nie widzisz żadnego problemu z alkoholem na jednostce kosmicznej podczas aktywnej służby?" - "Pani kapitan, pani PILOT jest na dragach ale nie jak pilotuje. Nie widzę problemu, to normalne. To jest dobry pilot i moi ludzie też są dobrzy." - "Rozumiem Twój punkt widzenia, ale powinniśmy zejść ze wszelkich używek". Jego mina jest jakbyś powiedziała "zjedz swojego ukochanego psa". "Pani kapitan, no ale bądź pani człowiekiem, tak się nie da. Na tych jednostkach jest z dupy."

* Ruppok: "Pani kapitan, ta jednostka NIGDY nie będzie wezwana na akcję" /śmiech
* Arianna: "Nie powiedziałam JEŚLI a KIEDY pójdzie na akcję. Wykonała wczoraj swoje pierwsze manewry po miesiącu mojej pracy. A nie wybieram się stąd szybko. Po kolejnym miesiącu kolejne ćwiczenia. Potem akcja. W rok - ta jednostka będzie aktywnie działająca i może uczestniczyć w akcjach."
* Ruppok: /w jego oku mignęło coś na kształt litości. Aż otworzył usta. "Tak jest." Nieszczerze, ale czegoś Ariannie nie mówi.
* Arianna: "Czemu uważasz że ta jednostka nigdy nie będzie brała udziału w akcjach? Powiedz teraz."

Arianna chce się dowiedzieć. Zaskoczyło ją, że jest mu jej żal. Pokazuje mu plan na jednostkę i z entuzjazmem. Bardziej ryzykownie by poczuł presję.

TrZ (Verlen...) +3:

* Vr: 
    * Ruppok: "pani kapitan, na tej jednostce nie ma Orbiterowców. Poza panią i kapitan EEEE oficer Durmont. Orbiter trzyma się Orbitera. Nigdy nie będzie pani OKAZJI na coś sensownego. To nie to, że pani robi coś źle. To to, że Orbiter pani nie da."
    * Arianna: "Aurum ma swoje programy"
    * Ruppok: "<śmieje> A Noctis ma naukowców. Wierzy pani w program kosmiczny Aurum? To pani nie jest tu zesłana za karę? Jak ja?"
    * Ruppok: "Dobrze pani poszło. Dobrze. Ale to koniec drogi. Dalej nic nie ma. Nie ma nic. Tylko dragi. Jak Alezja Durmont."
    * Arianna: "Jak tak do tego podejdziemy to faktycznie nie będzie nic."
    * Ruppok: "Niech nam pani dragów nie zabiera, co? Zrobimy dobrze. I tak nic z tego nie będzie. Niektórzy oficerowie to rozumieją."
    * Arianna: "Za co pana tu przysłali panie Ruppok?"
    * Ruppok: "Byłem z Orbitera. Miałem uratować statek i poświęcić przyjaciół. Byłem inżynierem. Stwierdziłem, że uratuję przyjaciół. To była głupia misja od samego początku. Statku nie dało się uratować. Nic mi się nie udało. Ale jeden kolega przeżył. Było dochodzenie. Kto jest winny? Ja. Bo nie wykonałem polecenia. Mimo, że nie dało się nic zrobić a przynajmniej kogoś uratowałem. Wszystkie statki są z dupy. Pytanie jak daleko od czarnej dziury jesteś."
    * Arianna: "Nie chcesz kogoś uratować, jedyne na kim możesz polegać to Ty? Jesteśmy jednostką wsparcia z dobrą obsadą i dobrymi ludźmi. Możemy komuś pomagać. Ktoś teraz nie ma pomocy bo my tu stoimy. Orbiter ma małe siły. Nie może być wszędzie. Każda jednostka może ratować kogoś. Funkcjonalna, w pełni obsadzona jednostka stoi i pozwalamy ludziom ginąć."
    * Ruppok: "Ale pani kapitan ma załogę z dupy."
    * Arianna: "Będę pracować aż wypchnę ich z tej dupy." /Ruppok się zaśmiał.
    * Ruppok: "Nie będę pani kapitan przeszkadzał, ale nie oddam tego co jest piękne w życiu."
* V: Ruppok to łyknął. Zejdzie z najcięższych narkotyków dla WSZYSTKICH i będzie fabrykował takie... "normalne".

OFICJALNA wersja? Oduzależniamy załogę stopniowo. Dostali karę, teraz odwyk, stopniowo. Odcięcie naraz od wszystkiego to terapia szokowa. (Arianna +dane medyczne od Klaudiusza +2Vg)

* Vz: Klaudiusz wyciągnął serię danych, dowodów, obserwacji - przyłożył się. Serio się przyłożył. Arianna aż jest zaskoczona.

Arianna jest w wystarczająco dobrym humorze.

DZIEŃ 4: 

Arnulf przyszedł do Arianny z POMYSŁEM ROZWIĄZANIA. "Pani kapitan, musimy zdobyć część substratów dość szybko do fabrykatorów. Mamy dość protomat, mamy mniej biomat niż potrzebujemy, ale najbardziej brakuje nam rzadkich materiałów. Musimy zdobyć to w 1-2 tygodnie - będzie leciał na planetę Tucznik Trzeci. Tam jeśli przekażemy odpowiednie rzeczy, to dostaniemy dużo brakujących materiałów i pomocy. Ale potrzebujemy im wyprodukować niektóre sprawy. Do Aurum."

Arnulf robi taki mały przemyt - to, co Orbiter ma a Aurum nie ma za to, co ma Aurum a ta jednostka potrzebuje. DLATEGO Arnulf trzyma to do kupy. Dlatego Darii nie zgadzały się na początku liczby... a jak Arnulf zdobywał te surowce? "Mafia na Orbiterze". Znaczy: Arnulf podnajmował im tą jednostkę i robił rzeczy "nielegalne które ukrywamy przed Orbiterem" i za to dostawał te rzadkie surowce.

Arianna chce, by Tucznik przywiózł dobre jedzenie, rzeczy które nadają się dla tej załogi. By ta załoga dobrze szła do przodu. By budować morale. I dopłacimy im w przyszłości. A Arianna chce się umówić z Kurzminem - niech on zbuduje link kontaktów dzięki Darii.

Arianna MA HANDLOWAĆ z Tucznikiem by załoga coś dostała by wzmocnić morale i pozytywny odbiór że z Tucznikiem i z planetą. Łączy to ludzi z Aurum i ludzi nie z Aurum. I do tego wszystkiego buduje link Kurzmin - Stacja Nonarion Nadziei.

* Kurzmin się zgodzi, bo OBIECAŁ że się zgodzi i faktycznie może na tym zyskać.
    * Obiekcja: "to nie zadziała, nie znam ich, nie będą chcieli"
        * Złamanie obiekcji: "Daria"
    * Kurzmin ma nowe kontakty
* Tucznik się zgodzi, bo na tym więcej zyskuje.
* Załoga się zgodzi
* Morale idzie do góry

I to stanowi pewien problem dla Sił Specjalnych. Celem Królowej od samego początku była REDUKCJA SZANSY na to, by Aurum mogło się udać w kosmos. A tu taka jedna Arianna nie tylko dała radę zintegrować zespół oraz załogę, ale też powiększyć link pomiędzy nie-Orbiterową częścią kosmosu.

Arianną zainteresował się osobiście nowo wschodzący admirał, Antoni Kramer. Więc usunięcie Arianny nie wchodzi w grę.

Zatem Orbiter wezwał Ariannę i zaproponował jej coś innego - ma Darię, która zna okolice Anomalii Kolapsu (poza bezpośrednim sterowaniem Orbitera). Ma całkiem sensowną załogę. Dostanie nową jednostkę, lepszą, może wziąć kogo chce z tej załogi i będzie mogła spełniać pożyteczne funkcje jako kapitan jednostki kosmicznej a nie na jednostce treningowej.

Dzięki temu Orbiter ugrał to co chciał:

* Program kosmiczny Aurum jest zakłócony - Arianna i kompetentni oficerowie lecą DALEKO od planety
* Arianna i reszta zespołu będzie sensowna i będzie pod wpływem Orbitera
* Relacje Orbiter - Nonarion (stacja przy Anomalii) idzie do góry
* Arianna i Aurum nie wiedzą nic o sabotażu programu kosmicznego Aurum.

Arianna się oczywiście ucieszyła i zgodziła. Ale zażądała małych zmian personalnych - nie chce Klaudiusza Terienaka ani Stefana jako pilota. Orbiter wyraził zgodę.

Sytuacja opanowana i wszyscy są zadowoleni.

## Streszczenie

Po długiej i ciężkiej pracy Arianna i Daria doprowadziły Królową do 50% sprawności nominalnej. Radość i wysokie morale załogi. Gdy Arnulf przyszedł do Arianny mówiąc o fabrykacji narkotyków i alkoholu, ona kazała to 'zatruć'. Zmotywowała Ruppoka, by dał jej szansę i znalazła sposób jak częściowo odzyskać materiały do fabrykatora po rozmowie z Arnulfem - sojusz z bliską Anomalii Kolapsu stacją Nonarion; poprosiła Kurzmina by to załatwił. TAK PRZESTRASZYŁA siły specjalne Orbitera, że Ariannę i załogę przenieśli bliżej Nonariona by sabotować program kosmiczny Aurum. Koniec przygody na Królowej :-).

## Progresja

* Arianna Verlen: zainteresowanie nowo wschodzącego admirała Antoniego Kramera z uwagi na jej nadspodziewany sukces
* Arianna Verlen: przeniesiona z Królowej Kosmicznej Chwały na dużo lepszą jednostkę, Astralną Flarę.
* Daria Czarnewik: przeniesiona z Królowej Kosmicznej Chwały na dużo lepszą jednostkę, Astralną Flarę.
* OO Królowa Kosmicznej Chwały: doprowadzona do 50% funkcjonowania wszystkich systemów; jeszcze 1-2 miesiące w dokach i będzie w pełni sprawna.
* OO Królowa Kosmicznej Chwały: całkowicie poprzerzucana załoga, by nie była zbyt skuteczna. Arianna i większość załogi -> Astralna Flara.

### Frakcji

* .

## Zasługi

* Arianna Verlen: doprowadziła załogę do wiary w swoje umiejętności i Królową; kazała zatruć nielegalny alkohol by uniknąć konfrontacji ale wyjść na swoim. Przekonała Ruppoka, by ten dał szansę jej oraz Królowej oraz znalazła sposób jak odzyskać surowce - handel przez Tucznika Trzeciego z planetą i Nonarionem.
* Daria Czarnewik: uruchomiła statek do pełnego działania. Po długiej pracy Królowa jest kontrolowana i będzie działać. Więc - przesuną ją gdzieś indziej XD.
* Stefan Torkil: zrobił kilka kółek Królową, nic wyczynowego, ale dał radę. Potwierdził że statek działa.
* Arnulf Perikas: znalazł alkohol i shunt fabrykatora; znalazł też potencjalne rozwiązanie dla Arianny - 'nielegalny' handel z planetą. Przemyt.
* Klaudiusz Terienak: jest grzeczniutki bo się boi po tym jak spieprzył z Marceliną; zatruł syntezator alkoholu dla Arianny by było przeczyszczenie i wejście do Ruppoka.
* Tomasz Ruppok: stoi za syntezą alkoholu i narkotyków na Królowej; niby spieprzył partię (zatruta przez Terienaka). Niesamowicie cyniczny - uważa, że Królowa nigdy nie pójdzie na akcję a oni są 'the lost ones'. Kiedyś był inżynierem na Orbiterze, nie wykonał rozkazu i DZIĘKI TEMU uratował jedną osobę - ale poleciał bo ktoś musiał. 
* Leszek Kurzmin: współpracuje z Arianną. Ona zapewnia linię 'Aurum - Tucznik Trzeci', on linię 'Tucznik Trzeci - Nonarion'. I tak powstanie udany handel na którym wszyscy wygrają.
* Erwin Pies: dyskretnie porozstawiał marines w miejscach które mogą być sabotowane; cicho zapewnił, że wszystko bezbłędnie działa z tą załogą.
* Antoni Kramer: nowo wschodzący admirał Orbitera; zainteresował się Arianną Verlen widząc jej skuteczność i powodzenie. To osłoniło ją przed działaniami Sił Specjalnych.
* OO Tucznik Trzeci: transport pomiędzy Aurum a Orbiterem, służy do przemytu na korzyść obu stron. Arnulf Perikas ma tam bliskie kontakty.
* OO Królowa Kosmicznej Chwały: wreszcie zaczęła działać, choć jest jeszcze brzydsza niż kiedykolwiek. Faktycznie statek patchworkowy.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 4
* Dni: 7

## Konflikty

* 1 - Daria uruchamia statek do pełnego działania
    * TpZ (rozstawienie, testy itp.) +4 +3Or
    * XVVrX: pomniejsze naprawy potrzebne, głośne i patchworkowe; miesiąc pracy Darii i inżynierów. Statek jest 'cleared', lot jest konfliktem TYPOWYM.
* 2 - Stefan Torkil zrobił kilka kółek Królową, nic wyczynowego, ale potwierdził skuteczność jednostki
    * TpZ (Daria zrobiła diagnostykę + załoga przećwiczona) +4:
    * VVVVV: kilka kółek udanych, morale jest, Daria widzi kłopoty, Orbiter widzi że Arianna to zrobiła, statek działa na 50%.
* 3 - Arianna chce się dowiedzieć o co chodzi z Ruppokiem. Zaskoczyło ją, że jest mu jej żal. Pokazuje mu plan na jednostkę i z entuzjazmem.
    * TrZ (Verlen...) +3
    * VV: Ruppok powiedział kim był kiedyś - inżynierem Orbitera i wyleciał za niesubordynację. Arianna przekonała go by dał jej szansę i zszedł z ostrych narkotyków dla załogi (tylko normalne)
* 4 - 
    * 
    * 
* 5 - 
    * 
    * 
* 6 - 
    * 
    * 
* 7 - 
    * 
    * 
* 8 - 
    * 
    * 
* 9 - 
    * 
    * 
* 10 -
    * 
    * 
