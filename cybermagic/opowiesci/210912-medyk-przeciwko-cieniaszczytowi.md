## Metadane

* title: "Medyk przeciwko Cieniaszczytowi"
* threads: młodosc-martyna
* gm: kić
* players: żółw

## Kontynuacja
### Kampanijna

* [210904 - Broszka dla eternianki](210904-broszka-dla-eternianki)

### Chronologiczna

* [210904 - Broszka dla eternianki](210904-broszka-dla-eternianki)

### Plan sesji
#### Co się wydarzyło

.

#### Sukces graczy

* ?

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Martyn ma 24 lata. Jest stosunkowo wysokim oficerem dość sporego gangu, Rozpruwaczy. Nie chciał iść na sam szczyt, bo dużo pracy i mało kobiet. Działa jako augmenter i medyk. Plus, jako social ops - wcześnie się na nim tu poznali. Nie ma stałej grupy podwładnych; wolny elektron który dużo może. Przy okazji - HR tej bandy. Ludzie przychodzą do niego z problemami. Do grupy wciągnął go Paweł Pieprz. 

MY (czytelnicy) WIEMY, że wojna wybuchnie mniej więcej za niecały rok.

Do Martyna przyszedł strapiony Paweł - siostra mu zniknęła. Nie przyszła na obiad. Martyn jest zaniepokojony - siostra znika w Cieniaszczycie to cholernie zła opcja. Chłopaka dawno rzuciła, nie ma wrogów. Paweł gadał z siostrą "przedwczoraj", informacje z kliniki że są nie takie wyniki i musi podejść. Miała być wczoraj, Paweł nie dzwonił do kliniki. Ma klucze do mieszkania Kasi (siostry).

Martyn natychmiast dzwoni do kliniki lekarskiej. Informuje, że ma przyjaciółkę którą czasem się zajmuje - Kasię Pieprz. Chce tylko jednej wiadomości - czy BYŁA w klinice. Mówię, że zniknęła - czy mogą mu pomóc.

Tr+3:

* V: Tak, była tu wczoraj. Ma godzinę wyjścia. 
* V: Informacja, że Kasia była u Franciszka Tocza. 

Martyn szuka też po bandzie i po hipernecie na szybko informacji o tym, czy Franciszek Tocz i ta klinika nie mają jakiegoś powiązania z Niewłaściwymi Czynami Kralotycznymi itp.

ExZ+2:

* V: Martynowi znaleźli rzeczy wskazujące na to, że dookoła Tocza działy się dziwne rzeczy. Plotki. Dobry lekarz, robi co powinien. Ale są plotki:
    * bardzo negocjowalna moralność
    * lekarz, który raczej Ci pomoże chyba że ktoś mu zapłaci
    * ma reputację, że zajmuje się leczeniem szemranych spraw
* ZAWIESZONE

Kim był były Kasi? Po rozmowie z Pawłem - raczej nie jest w to zamieszany. Mięsień, należy do jakiejś rywalizującej grupy. Mniejszej. Raczej jest zasada "don't touch the family", ale dla Martyna jest to potencjalny zasób...

Martyn idzie odwiedzić mieszkanie Kasi z Pawłem. Do Mrowiska. Wejście bez problemu. 

Martyn składa zaklęcie - chce widzieć "warstwami naskórka i płynów ustrojowych" kto tu był, gdzie był, co robili i co to za płyny w ciągu ostatnich 2 tygodni. Też powinno to oznaczyć ubrania.

TrM+3:

* Vm: Kasia nie jest święta. Lata po imprezach, gzi się troszkę. Nie jest pustelnikiem ale nie jest prostytutką. Martyn ma pattern rzeczy i miejsc gdzie ostatnio była
    * mieszkanie nie jest wielkie, sporo zaznaczona jest półka z papierami + informacje z kliniki. Niepokojące wyniki badań, prosimy o wizytę.
    * po opuszczeniu kliniki nie dotarła do mieszkania. Klinika - mieszkanie raczej pojazd nie piechotą.
    * jest tu nietykana od dawna torba podróżna, nie ma torebki. 

Dalsze działania:

* Martyn kazał Pawłowi poszukać hasła Kasi, odnaleźć czym się martwiła. Co chciała badać w tej klinice. Na papiery Kasi i na jej śmieci Martyn rzuca gangowych ekspertów od who-is-who.
* Martyn poprosił swoich gangmanów o to, by popytali żebraków i na mieście kto i gdzie widział Kasię PO WYJŚCIU z kliniki. Martyn nie zakłada, że to była Kasia - jeśli w klinice coś się jej stało, cóż, to Cieniaszczyt. To niekoniecznie ona.

ODWIESZONY EKSTREMALNY - jakie słabości (chłopcy, dziewczynki...) ma Franciszek i gdzie można go zapułapkować:

* X: Wiedzą, że KTOŚ grzebie
* X: Wiedzą, że MARTYN grzebie
* X: Franciszek i klinika jest na baczności
* (darmowe): lubi hazard ponad swoje fundusze i nie ma problemów finansowych

Tamten cel jest niemożliwy - więc Martyn szuka informacji odnośnie tego kto w tej klinice mógłby być słabym punktem - niezadowolony z gościa, czuje się z tym _źle_.

* X: Nie ma takiej osoby. W klinice nic nie wiedzą o Franciszku złego i uważają go za dobrego lekarza. Nie ma tam słabych punktów tego typu.

To jest moment, w którym Martyn potrzebuje ochrony. To nie może być laska z Rozpruwaczy, bo koleś może zrobić skan na temat grupy w której obraca się Martyn. Więc Martyn dzwoni do byłego Kasi. Adrian. Martyn powiedział, że Kasia zniknęła tak jak wielu innych. Adrian może być pierwszy w kolejności zemsty - bo rodzin się nie atakuje. ALE. Martyn BĘDZIE zaatakowany - potrzebuje prześlicznej lasencji do ochrony. Taką, której nie uwierzą że jest jego ochroniarzem a dziewczyną. I jak Martyna zaatakują, to ona go ochroni, złapią delikwentów i przesłuchają.

Martyn powiedział szefowi co zamierza. WIE, że jeśli coś się rozwali to padnie na niego - akceptuje to. Ale Martyn zwykle ma rację.

Szperacz gangu ma znaleźć Martynowi terminuskę. Taką, która interesowała się tym tematem - znikających dziewczyn. Najlepiej, jeśli ma personalne powody i jest zablokowana / sfrustrowana.

TrZ+3:

* Vz: interesuje się tematem
* V: Martyn ma terminuskę
* V: sfrustrowana, zablokowana
* Vz: personalnie zainteresowana tematem

Martyn wie gdzie Irena (terminuska) przebywa w czasie wolnym, mniej więcej. Chce odwiedzić ją w barze. Jednak po drodze musiał tam dojechać pociągiem - tam Adela podeszła do niego niezauważenie i nóż w żebra by pokazać że MOŻE. Jest bardzo w typie Martyna - ale 1) bro code 2) nie śpi się z ochroniarzami. Dał jej stymulanty bojowe na wszelki wypadek (nieufnie wzięła). I udaje, że jest jego dziewczyną. Adela wdzięcznie gra tę rolę.

Terminuska Irena w Szkarłatnym Szepcie. Martyn podbija z Adelą pod ramieniem. Siadają. Martyn zaprasza do tańca - oczarowuje, pokazuje co wie, mówi co zrobił i dlaczego. Co chce osiągnąć - czego nie wie o Franciszku? Wsparcie formalne np. do wyników Kasi. I oczarować/zaintrygować - na końcu sesji Martyn chce z Ireną iść do łóżka. Plus, Martyn ma REPUTACJĘ - zawsze chroni kobiety. I co chce osiągnąć? Niech kobiety będą bezpieczne przed tym predatorem.

TrZ+4: 

* V: powie co wie o Franciszku
* X: jest zainteresowana, ale KTOŚ zwróci na nią uwagę. Ale nie w kontekście tej sesji. Potem ma wpierdol.
* Xz: Martyn is TOO resourceful. Za duże zainteresowane wzbudza. HE NEEDS TO GO. Nie może tu zostać długoterminowo bo ktoś go sprzątnie.
* V: wsparcie formalne odnośnie wyników itp.
* V: chęć wsparcia tam gdzie to konieczne. Nie "Martyn dowodzi" a "współpracują". Martyn jest skłonny słuchać jej poleceń.
* V: wystarczająco oczarowana i ufa w intencje Martyna

Irena: koleś to płotka, nie tylko on. Irena działa tu parę lat. Jedna z jej pierwszych spraw to było zaginięcie - nie udało Irenie się tej osobie znaleźć. Ślady doskonale zatarte. Potem wiele innych spraw. Nie tylko dziewczyny znikają. Co jakiś czas znikają ludzie. Nie magowie. Łączy tych ludzi tylko spotkania z lekarzami - nie zawsze jeden lekarz. Acz ten jeden lekarz jest bardzo prominentny. Irena nie miała podstaw, by któregoś z lekarzy przycisnąć. Dostała polecenie, by nie drążyć tematu.

Martyn zaproponował, że jakby złapał Franciszka, to ona może stać w cieniu i zadawać pytania. Ona powiedziała, że on STRASZNIE ucierpi jak to zrobi. Martyn powiedział, że jeśli to sprawi, że jego przygoda w Cieniaszczycie się skończy, to się skończy. Tu zrobił wiele fajnych rzeczy i ma super historie. Ale ona (Irena) potrzebuje jego pomocy, Kasia i inne dziewczyny też. A on może pojechać dalej. A jak odejść... to z wybuchem.

* V: Zrobił PIEKIELNIE dobre wrażenie.

Martyn poprosił Irenę, by ta zaczęła pracę nad nakazem. On sam ma zamiar odwiedzić Franciszka i go sprowokować. Plus, może czegoś się dowie. Zdaniem ADELI jest to idiotyczny pomysł. Zdaniem Ireny - nierozsądnie. Zdaniem Martyna - zadziała.

Martyn ma wsparcie Ireny, żebraków, innych "szczurów". Ok - to Cieniaszczyt. Ok - Cieniaszczyt ma czarne sektory - tam nikt nie pójdzie. Martyn też nie. Ale Martyn próbuje mapować z pomocą Ireny i swoich ludzi gdzie i jak poruszała się Kasia po wyjściu z kliniki: gdzie zniknęła i czy poszła do czarnego sektora z własnej woli.

ExZ+3:

* X: szefostwo Ireny "zostaw ten temat, laska". Ma ogon.
* X: wszystko wróciło do Martyna - to ON rozpuścił informację. On mąci wodę.
* Xz: już wiadomo i widać, że Martyn współpracuje z Ireną
* X: wskazówka na jeden z potężniejszych czy groźniejszych gangów cieniaszczyckich "Blacktech" - weszła do innej kliniki powiązanej z tamtym gangiem. Większość "gangu" NIE CHCE więcej z Martynem w kwestii Kasi. Poważny cios w surowce. 
* (darmowo) nietypowa droga, Kasia nie jechała jak zwykle. Jechała w inne miejsca niż zwykle.

Klinika. Spotkanie z Franciszkiem. Zakres rozmowy:

* "Nazywam się Martyn Hiwasser i szukałem informacji na pana temat. Jestem lekarzem Kasi Pieprz."
* Martyn pozycjonuje się na głównego przeciwnika. Mastermind. On znajdzie Kasię. Zdobędzie nakaz - co jest w papierach. Efekty uboczne leków.
    * jeżeli "oskarżam lekarza", ja muszę mieć dowody. Ale "efekty uboczne leków po których dziwnie w edytowanych rekordach" -> wygląda jakby chowali malpractice
* Martyn chce swoją przyjaciółkę odzyskać.

Ex+3:

* V: Franciszek puts up a face, ale się boi. Czegoś. I niczego z tego co Martyn powiedział. Boi się, że jak coś powie to ma wpierdol. Ale to znaczy, że COŚ WIE.
    * nie boi się terminusów, gangów, kliniki, reputacji, opinii publicznej. Czego się boi?
* (+2Vg) Martyn zmienia taktykę - podkreśla bravado. Chce, by Franciszek spodziewał się specops i sigint - ale nie bezpośredniego napadu.
    * V: Franciszek TOTALNIE nie spodziewa się napadu. Wszystko ale nie tego.
    * XX: Kasia jest stracona dla Martyna. Nie da rady jej uratować.
* V: Martyn zauważa, że co jakiś czas (nie zdając sobie sprawy) Franciszek spogląda na konkretną szafkę, w konkretne miejsce.
* V: Martyn rozbija ampułkę z gazem (na który ma antidotum) i korzystając z szoku Franciszka otwiera szafkę. Plus - dym by wywołać alarm przeciwpożarowy.

Szafka jest wypełniona różnego rodzaju akcesoriami medycznymi. Jest kilka fiolek, które zwracają uwagę Martyna. Franciszek jest skulony i kaszle (pięść w brzuch pomogła) a Martyn składa zaklęcie - co jest w tych fiolkach. Jaki to ma efekt. I hotlinia do Ireny.

ExZM+2+3O:

* Vz: to nie jest coś co ma wpływ. To jest badanie. Te fiolki badają krew. Ale nie jest to normalne badanie. To osobny, dziwny test.
* Vm: to nie jest forma magii krwi, ale jest jakaś forma sympatii z energią z jaką Martyn się dotąd nie zetknął i nie dotyczy magów. Ktoś szuka pokrewieństwa ludzi z _czymś_, jakąś formą magii.

Mocodawcy Franciszka ani Franciszek nie spodziewali się bezpośredniego ataku. Nikt nie jest tak bezbrzeżnie głupi. Martyn wie, że Adela jest w poczekalni. Martyn zwija fiolki... i nowy czar. Stymulacja SIEBIE. Martyn musi uciec. Używa stymulantów, rzeczy które mogą pomóc z szafki. Jak trzeba - Paradoks przekieruje się we Franciszka. Cel - przy użyciu magii uzyskać dość dużo siły i mocy, by móc z Adelą opuścić klinikę i się rozpłynąć. A Irena niech Martyna aresztuje.

"Wysyłam to czego się dowiedziałem. Mam dla Ciebie próbki. Aresztuj mnie ZANIM mnie zabiją.": Martyn --> Irena.

ExMZ+3:

* Xm: Franciszek jest źródłem energii. Szok go zabił. Martyn nie chciał - ale tak się przeraził tym co znalazł że stracił kontrolę.
* O: Adela jest śmiertelnie zakochana w Martynie (od jutra). A stymulanty Martyna pożarły energię osób w klinice - trzeba poczyścić i poleczyć.
* X: Przez to co Martyn zrobił ONI przyspieszyli i uznali, że trzeba dokończyć. Kasia jest stracona przez Martyna. Gdyby nie to, może dałoby się ją uratować.
* Vm: Martyn i Adela uciekli z kliniki w chaosie. Adela upewnia się, że Martyn trafia do Ireny i ZNIKA. To przekroczyło... wszystko.
* X: It's LOUD. Poszło w wiadomościach. Martyn jako przestępca, mastermind, okrutny bezduszny itp.

Martyn eskaluje - wie, że jego miejsce w Cieniaszczycie się skończyło. Wie, że jego reputacja jest w ruinach. Ale jednocześnie to co może zrobić - powiedzieć wszystkim o co chodzi. Przekazać tym gangom. Niech powiedzą "swoim siostrom". Niech informacja idzie. Niech ci, co za tym stoją mają bardzo trudno i ich mocodawcy mają z czego się tłumaczyć i to jest mniej efektywne finansowo. I to jest połączone z poziomem akcji terminusów ratujących kogo się da i rozwalających co tu się dzieje.

Bo terminusi nie mogą tego zignorować.

ExZ+4:

* X: terminusi ponieśli straty. Część gangów, która próbowała im pomóc też. Druga Strona była gotowa.
* Vz: operacja terminusów się udała + od Ireny Martyn dowiedział się szczegółów ("Wiesz, że nie żyjesz. Powiem Ci, że to nie było na marne.")
    * test jaki Martyn wygrzebał - szukał kompatybilności człowieka z odkryta w laboratorium pod Cieniaszczytem niedaleko leylina krew lewiatanów
        * piekielnie drogi i nielegalny składnik, badania nad nadawaniem magii ludziom
        * Kasia nie przeżyła. Potencjalnie na swoje szczęście.
* X: Propaganda Drugiej Strony zadziałała. Martyn został sam. Wyrzekli się Martyna a wiele osób (Paweł, Adrian) stało się jego wrogami.
* V: Martyn osiągnął sukces - ujawnił gangom i "dziennikarzom", że to były lab BlackTech, że to oni robili te rzeczy. A niektórych rzeczy nie robisz nawet w Cieniaszczycie.
* X: Martyn miał zginąć w więzieniu. Basically, he was dead. Ale uratował go Orbiter. Porwali i na jego miejsce wsadzili "klona" (szkoda miragenta) przy współpracy Ireny - która uznała, że szkoda Martyna i miała kontakty w Orbiterze. Nie jest jego przyjaciółką - on jest zbyt niebezpieczny - ale warty uratowania. Irena zaproponowała go do sił specjalnych...

(AUTHOR'S NOTE: Wiemy już, czemu pod Cieniaszczytem udało się zwabić Finis Vitae. Tam jest leyline - energia. Pytanie skąd ta krew? Ale to inny kontekst.)

(Second note: mimo wygranego konfliktu, Irena x Martyn nie skończyli w łóżku. Nie byli w nastroju.)

## Streszczenie

Sprawa zaginionej siostry kumpla doprowadziła do odkrycia, że BlackTech robi eksperymenty na porywanych ludziach. BlackTech jest tak wysoko osadzone, że nikt nie chciał przeciw nim stanąć. Martyn Hiwasser, którego ruchy ratujące siostrę przyjaciela były neutralizowane zaatakował ich "frontalnie" i ujawnił co zrobili. Za to jest nagroda za jego głowę i terminuska-sojuszniczka-na-szybko wysłała go do sił specjalnych Orbitera. By go ratować.

## Progresja

* Martyn Hiwasser: nieskończona wręcz nienawiść wobec cieniaszczyckiego gangu BlackTech i działań mających krzywdzić niewinnych ludzi - skazując ich na eksperymenty celem nadawania magii.
* Martyn Hiwasser: odrzucony przez gangi i grupy cieniaszczyckie, ale też uwielbiany przez te grupy które utraciły w Cieniaszczycie bliskich do BlackTech.
* Martyn Hiwasser: wróg bardzo wysoko w Cieniaszczycie, w BlackTech (i pochodnych), Paweł (były przyjaciel), Adrian (były były Kasi i były Adeli) i wśród wielu osób w Cieniaszczycie. Jest... źle. SERIO ciężko. Nagroda za głowę.
* Martyn Hiwasser: Opinia mavericka. Śmiertelnie niebezpiecznego, inteligentnego i szybko działającego maga, który jednak na poziomie strategicznym nie umie zarządzać ryzykiem w żaden sposób.
* Irena Czakram: odblokowana przez przełożonych; po działaniach Martyna już nie było po co chronić BlackTech.
* Adela Kołczan: przez Paradoks Martyna Hiwassera śmiertelnie się w nim zakochała. To zniszczyło jej związek z Adrianem Koziołem i doprowadziło do dalszych komplikacji, bo Martyn nie chciał jej wykorzystać.

### Frakcji

* BlackTech Cieniaszczytu: oniegdaj najpotężniejszy gang zajmujący się blacktech i magitech, którego machinacje wyszły na jaw przez Martyna Hiwassera. Docelowo upada; reputacja w Cieniaszczycie ma znaczenie.
* BlackTech Cieniaszczytu: krwawa omerta w kierunku na Martyna Hiwassera. To on stoi za zniszczeniem wszystkiego nad czym BlackTech pracował.

## Zasługi

* Martyn Hiwasser: 24 lata. Chcąc uratować siostrę przyjaciela, zniszczył najgroźniejszy gang w Cieniaszczycie zmieniając strategie co pięć sekund i skazując się na śmierć. Uratowany przez Orbiter. Musiał odejść, ale odszedł z HUKIEM.Pierwszy raz kogoś zabił (Franciszek Tocz), doprowadził do śmierci siostry przyjaciela (Kasia Pieprz) i skrzywdził swoją ochroniarkę, która się w nim śmiertelnie zakochała przez magię (Adela Kołczan).
* Paweł Pieprz: wciągnął Martyna Hiwassera do Rozpruwaczy; przyszedł do Martyna by ten pomógł mu uratować siostrę. Pomógł Martynowi z hasłami siostry i ogólnymi działaniami - ale jego siostra zginęła. Wini Martyna. Od teraz - śmiertelny wróg Martyna.
* Kasia Pieprz: siostra Pawła; KIA. Sympatyczna dziewczyna bez szczególnych cech, która była kompatybilna z krwią Lewiatana nad którą pracowali BlackTech. Porwana, przeszła proces adaptacji nieco za wcześnie (przez działania Martyna) i zgineła.
* Franciszek Tocz: lekarz o negocjowalnej moralności. Znajdował osoby pasujące do profilu BlackTech i wysyłał je dalej. Martyn uznał, że jest zamieszany. Zginął do Paradoksu Martyna. HE DIDN'T KNOW!
* Adrian Kozioł: były Kasi; Martyn do niego zadzwonił; potrzebny mu był ochroniarz celem wymanewrowania BlackTech i Adrian polecił swoją dziewczynę. Martyn nie złamał bro code, ale w wyniku jego Paradoksu Adela się w nim śmiertelnie zakochała. Od teraz - śmiertelny wróg Martyna.
* Irena Czakram: terminuska Cieniaszczycka; od lat rozpracowywała eksperymenty BlackTech w Cieniaszczycie. Zablokowana przez przełożonych. Martyn Hiwasser wszedł z nią w sojusz i ją odblokował. Z wdzięczności dała cynk Orbiterowi, by nie zabili Martyna w celi. Ostrożni przyjaciele, acz trzyma Martyna na odległość kija bo maverick.
* Adela Kołczan: dziewczyna Adriana i ochroniarz Martyna; ostra żyleta, która pomogła Martynowi na prośbę chłopaka i przez Paradoks Martyna się w nim śmiertelnie zakochała. NADAL nie chce go chronić. Nie da się. HE'S FUCKING CRAZY.

### Frakcji

* BlackTech Cieniaszczytu: gang; prowadzili badania nad nadawaniem magii ludziom przy użyciu Leyline pod Cieniaszczytem i współpracując z szemranymi osobami. Mocno osadzeni gdzieś wysoko w Cieniaszczycie, dobre plecy. Ostra konfrontacja z terminusami i innymi gangami przez porywanie ludzi do tych badań (dzięki, Martyn Hiwasser). To jest początek ich upadku.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Przelotyk Wschodni
                            1. Cieniaszczyt
                                1. Mrowisko: jest tam mieszkanie Kasi Pieprz; dokładnie przetrzepane magią Martyna i szperaczami gangu Rozpruwaczy.
                                1. Knajpka Szkarłatny Szept: ulubione miejsce terminuski Ireny; Martyn i Irena tańczyli i przekazywali sobie info odnośnie planów vs Toczowi.
                                1. Kliniki Czarnego Światła: zbiór porozrzucanych klinik świadczących tanie i porządne usługi nie współpracujących z kralothami ani BlackTech. Franciszek Tocz jednak współpracował z BlackTech i Martyn się tu z nim skonfrontował ku ogromnemu zdziwieniu wszystkich. Nikt nie atakuje Czarnego Światła - po co?
                            1. Cieniaszczyt, podziemia
                                1. Leyline: miejsce mocy wykorzystywane przez BlackTech do integracji krwi Lewiatana z ludźmi celem syntetyzowania magów na ich żądanie.

## Czas

* Chronologia: Inwazja Noctis
* Opóźnienie: -370
* Dni: 3

## Inne

.

## Konflikty

* 1 - Martyn natychmiast dzwoni do kliniki lekarskiej. Chce tylko jednej wiadomości - czy BYŁA w klinice. Mówię, że zniknęła - czy mogą mu pomóc.
    * Tr+3
    * VV: Informacja o godzinie wyjścia + pointer na Franciszka Tocza.
* 2 - Martyn szuka też po bandzie i po hipernecie na szybko informacji o tym, czy Franciszek Tocz i ta klinika nie mają jakiegoś powiązania z Niewłaściwymi Czynami Kralotycznymi itp.
    * ExZ+2
    * V: Informacje o Toczu
    * (ZAWIESZONY)
* 3 - Martyn składa zaklęcie - chce widzieć "warstwami naskórka i płynów ustrojowych" kto tu był, gdzie był, co robili i co to za płyny w ciągu ostatnich 2 tygodni. Też powinno to oznaczyć ubrania.
    * TrM+3
    * Vm: informacje o Kasi. Nie jest święta; lata po imprezach, zdobycie patternu. Informacje że nie wróciła do domu po wyjściu z kliniki. Papiery z kliniki.
* 4 - ODWIESZONY EKSTREMALNY - jakie słabości (chłopcy, dziewczynki...) ma Franciszek i gdzie można go zapułapkować:
    * ExZ+2 bez 1V
    * XXX: Wiedzą, że KTOŚ - MARTYN grzebie i Franciszek + klinika na baczności. Ale Martyn wie że Franciszek lubi hazard i ma "nadmiar" kasy.
    * X: (jako że tamten cel niemożliwy, Martyn szuka kreta w klinice) Nie ma takiej osoby. W klinice nic nie wiedzą o Franciszku złego.
* 5 - Szperacz gangu ma znaleźć Martynowi terminuskę. Taką, która interesowała się tym tematem - znikających dziewczyn. Najlepiej, jeśli ma personalne powody i jest zablokowana / sfrustrowana.
    * TrZ+3
    * VzVVVz: ma terminuskę, interesuje się tematem, sfrustrowana i zablokowana, personalnie zainteresowana tematem
* 6 - Martyn zaprasza terminuskę do tańca. Oczarowuje, pokazuje co wie, mówi co zrobił i dlaczego. Cel: czego on nie wie? Chce jej wsparcia formalnego. Plus na końcu sesji chce ją do łóżka.
    * TrZ+4
    * VXXzVVV: terminuska powie co wie o Franciszku, wsparcie formalne i gdzie konieczne, współpracują. I tak, oczarowana i pójdzie do łóżka. ALE Martyna ktoś sprzątnie jak zostanie w Cieniaszczycie a terminuska -> kłopoty.
    * V: +zrobił PIEKIELNIE dobre wrażenie na terminusce
* 7 - Martyn próbuje mapować z pomocą Ireny i swoich ludzi gdzie i jak poruszała się Kasia po wyjściu z kliniki
    * ExZ+3
    * XXXzX: szefostwo Ireny "zostaw temat", wszystko wróciło do Martyna, jawna współpraca, po ujawnieniu że to BlackTech mało kto chce współpracować z Martynem.
* 8 - Więc Martyn poszedł na bezpośrednie spotkanie z Franciszkiem do kliniki. W paszczę lwa. Sprowokować i dowiedzieć się jak najwięcej.
    * Ex+3
    * V: Franciszek CZEGOŚ się boi, czegoś bardzo innego.
    * (+2Vg) Martyn zmienia taktykę - chce, by Franciszek spodziewał się specops i sigint - ale nie bezpośredniego napadu.
        * VXX: Franciszek TOTALNIE nie spodziewa się napadu. Wszystko ale nie tego. Kasia jest stracona dla Martyna. Nie da rady jej uratować.
    * VV: Martyn widzi że coś jest w szafce, rozbija ampułkę i wyłącza Franciszka + bierze szafkę.
* 9 - Martyn składa zaklęcie - co jest w tych fiolkach z szafki. Jaki to ma efekt. I hotlinia do Ireny.
    * ExZM+2+3O
    * VzVm: to jest system badawczy krwi, dopasowania do CZEGOŚ, sympatii. 
* 10 - Martyn musi uciec. Używa stymulantów, rzeczy które mogą pomóc z szafki. Jak trzeba - Paradoks przekieruje się we Franciszka. Cel - przy użyciu magii uzyskać dość dużo siły i mocy, by móc z Adelą opuścić klinikę i się rozpłynąć.
    * ExMZ+3
    * XmOXVmX: Franciszek źródło energii KIA, Adela śmiertelnie zakochana + ludzie poosłabiani, ONI przyspieszyli i Kasia nie żyje przez Martyna, Martyn i Adela zwiali, TOO LOUD i destrukcja reputacji Martyna.
* 11 - Martyn eskaluje - wie, że jego miejsce w Cieniaszczycie się skończyło. Chce powiedzieć wszystkim o co chodzi. Przekazać tym gangom. Niech powiedzą "swoim siostrom". Niech ci, co za tym stoją mają bardzo trudno.
    * ExZ+4
    * XVzXVX: terminusi mają straty, gangi też, operacja udana, propaganda wroga działa ale Martyna też. Martyn miał zginąć w więzieniu, uratowany przez Orbiter.
