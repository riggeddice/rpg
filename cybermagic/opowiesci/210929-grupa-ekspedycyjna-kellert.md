## Metadane

* title: "Grupa Ekspedycyjna Kellert"
* threads: legenda-arianny, niestabilna-brama, sektor-mevilig
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [210922 - Ostatnia akcja bohaterki](210922-ostatnia-akcja-bohaterki)

### Chronologiczna

* [210922 - Ostatnia akcja bohaterki](210922-ostatnia-akcja-bohaterki)

### Plan sesji
#### Co się wydarzyło

* Odkryto nową "lokalizację" mapy Eterycznej przy niestabilnej bramie.
* Termia wysłała jednostki zwiadowcze. Brak jednoznacznych odpowiedzi, ale wszystko wskazuje na to, że wpadły w kłopoty - SOS.
* Termia wysłała Grupę Ekspedycyjną Kellert. Kellert została anihilowana. Nie wiadomo o co chodzi. Termia zasealowała dalsze działania.
* Do Bramy Eterycznej przeniesiono superpancernik "Omega Septimus". TAI klasy Amaranth.
* Admirał Kramer wysyła perfekcyjnie zamaskowaną Infernię by odkryła co się stało z Grupą Ekspedycyjną Kellert. Uratujcie co się da. Co jest po drugiej stronie?
    * Infernia ani super-szybka ani super-zwrotna ani pancerna. Ale mało widoczna.

#### Sukces graczy

* 

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Eustachy. Dwa tygodnie byłeś na cholernym kongresie. Tłumaczyłeś arystokratkom odnośnie funduszy. Wiadomość od Leony - jest w szpitalu i chce się z nim widzieć.

Szpital na K1. Leona jest w stanie fatalnym. Wygnała wszystkich. Ostrzegła Eustachego, że Martyn to lord eternijski. Władca niewolników. I Eustachy musi chronić Infernię. Niech Eustachy uważa na niego, bo nikt inny nie chce Leony wziąć na poważnie...

Arianna <-- Kramer

Kramer wyjaśnił sytuację:

* Noviter, automatyczni zwiadowcy: OK, potwierdzili Noviter (wysokie p(x)). Nie ma z nimi kontakt.
* Wysłała Termia jednostkę załogową - stracony kontakt po 5 min
* Wysłała grupę ekspedycyjną po 5 min zniknęła
* Powód Termii? "Bo ich stracimy". Za wysoka regularność.

Infernia powinna poradzić sobie wrócić poniżej 5 minut. TO KONKRETNE PRZEJŚCIE trwa ~1h.

Arianna -> Klaudia. Niech Klaudia sprawdzi poprawność z sektorem Noviter. Klaudia robi analizę danych z sektora Noviter, używając K1 jako komponent. Wszystkie dane z K1. A Kramer autoryzował. Klaudia prosi o wsparcie różne AI stacji. Porównuje z sektorem, zebrać co jest w stanie zebrać, poczyścić, odszumić itp. Znane sektory, interpolacje itp. PLUS zmiany czasowe sektora. Jakby był time dilation.

TrZ+4+3Og:

* V: Marszałek Grzmotoszpon Trzeci, TAI na K1, WSZYSTKIE zapisy o Noviter pochodzą z K1. To jest manipulacja na danych zrobionych przez zwiadowców. Duża moc obliczeniowa przeciwnika.
* V: Odszumienie, korekta itp. "pierwsze 5 minut". Udało się odzyskać jakiś obraz. Gwiazdę przyćmioną chmurą. Dyson Swarm, farma Amat. Nowy sektor. Sektor o którym nie wiedzieliśmy. Bardzo dużo wraków.
* (->Ex) V: Tło elektromagnetyczne. Atakowane są TAI. Innymi słowy, TAI zaczynają szaleć. Na planecie są jakieś sygnały - planeta żyje.
* Vz: Sygnał jest _dziwny_ że próbuje uwolnić, złamać bariery ale umożliwić podpięcie do większej sieci i uzyskanie większej mocy. Corrupted Transcendence.
* XXz: Persi będzie zmieniać "osobowości dominujące". Ok, to jest system defensywny, ale część z osobowości jest niewskazana.

K1 wbudował w Persefonę Inferni mnóstwo dodatkowych zabezpieczeń, elementów, samozniszczenie, wszystko co lubimy najbardziej. Psychotronicy K1 nie mają pojęcia z czym mają do czynienia - zorientowali się że jest w bardzo złym stanie, ale Arianna zawetowała czysty reset. No i statek musi lecieć natychmiast.

Klaudia chce zabrać ze sobą "pudełko". TAI Elainka - "magnetofon". System sensoryczny skierowany na AI, na komunikację. Klaudia chce zebrać jak najwięcej materiałów będąc tam. Arianna wzięła zapasową TAI na pokład by ją poświęcić XD. "Oops, sygnał co miał nas położyć nas położył". Plus dryfowanie. Elainka w sandboxie która myśli, że jest rzeczywista.

Na pokład Inferni tymczasowo ADAM PSYCHOTRONIK. Ktoś, kto grzebie w Persi. Klaudia dała mu dość uprawnień by nie musiał się ubiegać o uprawnienia itp.

Termia chce się widzieć z Arianną. Czy Arianna chce Emulatorkę? Tak. Moduł Emulatorki - zamontowany. Plus - Infernia holuje koloidową szalupę. Infernia ma też zamontowane dwie torpedy anihilacyjne. Tylko dwie. Wolno się aktywują, wolne, kiepskie sterowanie, ale jak walną... Plus, Arianna autoryzowała Eustachego do użyciu emitera anihilacji...

Infernia przechodzi przez Bramę. Ma "kotwicę". Infernia przechodzi przez Bramę.

Infernia z szalupą się przedostały. Niezauważone. Odłączona szalupa i... co dalej?

Klaudia szuka "naszych" jednostek po sensorach pasywnych. Które statki niedawno, które dawno, trajektoria jednostek, rozłożenie złomu... gdzie są nasze jednostki, gdzie mogły się dostać itp. Plus sygnały "naszych" zwiadowców. Co było czego nie było.

TrZ+3:

* V: Klaudii udało się zlokalizować za daleko jedną z jednostek, OO Savera. Była jednostką naukową - da się odratować. Uszkodzenia od zębów.
* Vz: Infernia ma super czujniki - są ślady życia na statku. Ktoś tam żyje. Savera nadaje. Dziwne mamrotanie...
* V: Pasywnie wykryty sygnał innej jednostki. Jest przytłumiony, automatyczne SOS. Ze środka wielkiej planetoidy.
* X: Trudno dolecieć do Savery nie dotykając czegoś. Arianna pilotuje.

Eustachy zdecydował się odpalić torpedę. Taką, z sygnałami i aktywnym sonarem. Niech ona ściągnie ogień i pokaże co tu się dzieje - i Infernia ma informacje. Zewnętrznie SOS, w tym ukryty sygnał kodem Orbitera "jesteśmy tu, nie traćcie nadziei".

TrZ+3+3O: 

* X: Planeta wysyła szeroką wiązkę sygnału plugawiącego AI. 
* V: Torpeda opuściła Infernię. Włączyła "sygnał". 

Eustachy chce użyć większej ilości torped - sprawdzić czy siła sygnału, czy jednostka. Czy wystarczą torpedy, czy potrzebna nam jest pintka. Manewrować w tym paskudztwie. Plus jedna torpeda jako kosmiczny złom by przywaliła, poocierała - czy to wywoła reakcję. Czyli jak to działa.

* (+1V) V: Pirania nie reaguje na dotyk torpedy. Kolejne torpedy wysłane są w stanie utorować drogę Inferni. "Smaczniejszy cel" powoduje większe zainteresowanie. Potem wracają - powoli - na swoje miejsce
* V: Utorowana droga do Savery.

Lecimy pasywnie na Saverę. TrZ+4+1O.

* Vz: Infernia jest statkiem świetnej klasy. Dolecieliście do Savery. Da się dojrzeć w środku. Są ślady firefighta. Są ciała ludzi. Jakiś koleś w "szmacianym skafandrze" (ze złomowiska) - ciało.

Wysyłamy Elenę samą - z zestawem środków medycznych od Martyna i ładunków wybuchowych Eustachego. By wysłać "niepotrzebną część statku" w kosmos by zrobić tło. Emulatorka gotowa na ratunek. 

Elena próbuje wejść jak najbliżej mostka, by się podpiąć do sensorów, logów itp. Przez kwatery oficerów. Nie ma reaktorów, TAI i ludzie bełkoczą "po swojemu". Ręczne otwarcie śluzy (panic button itp).

TrZ+2+3O:

* Vz: Elena dostała się na statek. Koleś przebity ze ściany kolcem. Nie ma twarzy. Twarz jest zdarta.
* (+2O) V: Elena ma detekcję statku. Widzi kamerami i systemami statku.
* X: Kapitan patrzy przez interkom na Elenę. SZUKA jej. Czuje też posmak Esuriit.
* X: Elena próbuje zamaskować twarze na systemie. Ale CZUJE OBECNOŚĆ. DETEKCJA: 1.
* (+2Vg) X: Elena znajduje żywego nieszczęśnika. Wpakowuje w niego środki Martyna by go pobudzić i rozrzuca miny. CZERW wyskoczył ze ściany ale nie trafił w Elenę. MORALE: 1.
* V: Elena zbudowała dyskretny link komunikacyjny między statkiem i Infernią. Używając przenośnych. W końcu zgubiła prześladowcę (-2O). Ale jest zestresowana. Na wszystkich monitorach statku pojawiła się twarz Eidolona. Głosy stały się głośniejsze a TAI mówi w lockstepie z ludźmi.
* X: Eidolon zaczął zachowywać się powoli problematycznie. Elena zaczyna kontrolować go po neurosprzężeniu. Overridować. Żąda ekstrakcji ASAP.
* V: Elenie udało się bezpiecznie wrócić na Infernię. Eidolon - natychmiast do kontroli psychotronicznej, Elena - też pod kontrolę Martyna. I psychotronika.

Na bazie obserwacji da się uratować część załogi. Ale tam COŚ jest. Elena smutno powiedziała Martynowi, że rozumie te słowa. Mówi o potworze, który jest "niszczycielem i zbawicielem".

Cel - "uratować załogę tego statku, bo część się da ale mostek stracony więc będzie odstraszał piranie".

Eustachy używa torped m.in "torped z piłami tarczowymi" by odseperować statek na 2 kawałki. Statek podziurawiony jak ser szwajcarski.

TrZ+4+5O:

* V: Udało się odseparować statek na kluczowe kawałki. PLUS nadajemy naszą mantrę "modlitwy do Arianny". 
* X: "Piranie" zbliżają się w kierunku na ten "kawałek z załogą". Ale krążą dookoła, boją się zbliżyć. Czujnie patrzą.
* (+Vg) X: Obie części statku nadają sygnał. Infernia łapie go pasywnie. Sygnał --> sandbox. Sygnał to ten sygnał Orbitera. Ale odszyfrowany. "jesteśmy tu, nie traćcie nadziei". Bardzo mocny sygnał. 
* Vz: Udało się wydobyć tylko tą część by "bóstwo" miało ograniczoną moc. Mało czasu, mało możliwości. Nie ma "statku" tylko "załoganci".

Potrzebujemy kogoś, kto tam wejdzie i ich wyłączy. I zamknie na pintce. Martyn robi spacer kosmiczny w zwykłym skafandrze - by zmniejszyć jego sygnaturę. A Eustachy wystrzeliwuje torpedę, która ma odrzutem przesunąć Skażony statek. A Martyn -> w szczelinę między czystym statkiem.

* V: Eustachy przesunął odpowiednio statek.

Martyn robi spacer itp:

Tr+3+5O:

* X: Na pokładzie niestety część osób się nie wybudzi.
* O: "Bóstwo" poczuło obecność Martyna. DETEKCJA +1.
* X: "Przednia" część statku odleci za szybko. Martyn nie zdąży ewakuać pacjentów - jakoś inaczej z piraniami.
* V: Wyłączenie pacjentów. Martynowi udało się "wyłączyć" bóstwo z tego równania i sektora. | To wygląda jakby było zrobione przez ludzi. To stworzone z ofiar. To ma chronić.
* X: Bóstwo manifestuje się tam. Kolokacja.
* (+M 9O) X: Martyn próbuje przepiąć się na "ja jestem elementem tego co trzeba chronić" i zasłania pacjentów. PIRANIE ODLECIAŁY widząc bóstwo w tej części.
* (+3Vg na zachętę) O: Arianna robi najazd komandosów i Emulatorki. Szturm na to miejsce. Ratujemy kogo się da. DETEKCJA +1.
* Vm: Magiczny szturm - Martyn spowolnił to cholerstwo. Skonfundował.

Udało się uratować SIEDEM osób. Nie ma strat po naszej stronie. Ewakuacja na Infernię, pełna koma - brain death - na osoby które się udało uratować. Zdarte twarze.

Jeden z elementów złomu nie ruszających się - zamaskowany statek. Zniszczony. Lokalny. Ale to ZNACZY, ŻE TU GDZIEŚ SĄ LUDZIE. Drugi sygnał - lekkie echo hipernetowe z oddali. Ktoś włączył hipernet, myśląc, że sygnał Arianny (a potwora). I zgasł. Więcej "naszych" tu żyje.

Nie ma problemu z powrotem - plan jest bardzo dobry. Lecą w cieniu Skażonej jednostki do Bramy, aż przeciągną ją z kotwicy...

Bardzo. Dobry. Debriefing.

MORALE: 1/6
DETEKCJA: 3/6

## Streszczenie

Termia wysłała siły do sektora "Noviter", ale to był inny sektor (Mevilig). Straciła bezzałogowce, potem grupę ekspedycyjną Kellert. Kramer wysłał Infernię zakoloidowaną; okazało się, że ten sektor ma "strażnika Esuriit" zrobionego przez ludzi i grupę Piranii, sterowanych przez TAI 4 poziomu. Inferni udało się zebrać dane i wrócić z siedmioma uratowanymi członkami OO Savera, gdzie większość załogi została corruptowana przez Vigilusa...

## Progresja

* .

### Frakcji

* .

## Zasługi

* Arianna Verlen: zdecydowała się na skrajnie niebezpieczną misję, by ratować ludzi Termii. W kluczowym momencie - czy Martyn uratuje ludzi czy nie, sam na sam z Vigilusem - kazała szturmować korzystając z tego że Piranie się oddaliły. Uratowała Martyna i siedem osób.
* Eustachy Korkoran: sterował Piraniami używając "torped z sonarem", robiąc przejście dla Inferni. Potem manipulował lokalizacją fragmentu statku z Vigilusem by Martyn mógł ratować ludzi.
* Klaudia Stryk: analizując dane z jednostek zwiadowczych na K1 odkryła, że przeciwnik wpływa na TAI. Pasywnie czujnikami znalazła Saverę w sektorze Mevilig. Zapewniła psychotronika Adama na pewien czas na Infernię.
* TAI Marszałek Grzmotoszpon Trzeci: Jedno z TAI na K1, które lubi podróżować. Ale nie może, więc ogląda filmy i się tam "wkleja". Wykrył, że dane ze zwiadowców to było 100% sfabrykowany footage z danych już dostępnych na K1.
* Aleksandra Termia: przeprowadziła operację w sektorze "Noviter", nie wiedząc, że to sektor Mevilig. Straciła 6 okrętów i 200 osób. Nie chciała tracić więcej - ale Ariannie dodała Emulatorkę.
* Antoni Kramer: nie zgadza się na to, że Termia nie ma jak uratować 6 statków i 200 osób. Zakoloidował Infernię, autoryzował sprzęt i wysłał Infernię przez Bramę.
* Olena Orion: Emulatorka dostarczona przez Termię na Infernię. Walczyła przeciwko Vigilusowi z Sektora Mevilig wraz z paladynami Verlenów. 
* Otto Azgorn: dowodził grupą szturmową mającą uratować Martyna od Vigilusa. Udało im się wyciągnąć Martyna i siedem osób, zdzierając im twarze. 
* Martyn Hiwasser: kosmiczny spacer do odstrzelonego fragmentu OO Savera; wyłączył kogo się dało z Dotkniętych przez Vigilusa. Po manifestacji Vigilusa doszedł co to jest - próbował ich ochronić sympatią Esuriit, ale nie udało mu się. Gdyby nie szybka reakcja Arianny i rozkaz ataku do Otto i Emulatorki, byłoby po nim. A tak uratowali 7 osób. Z 36 z załogi.
* Elena Verlen: zinfiltrowała Skażoną OO Savera. Dała się Zobaczyć Vigilusowi, ale skutecznie odkryła położenie wszystkiego na Saverze i zdołała uciec zanim została rozsiekana przez krwawe "bóstwo".
* Leona Astrienko: leży w szpitalu; ostrzegła Eustachego, że Martyn jest eternijskim lordem, miał niewolników i ogólnie zniszczy Infernię. Eustachy jest lekko sceptyczny ale ok - będzie uważał.
* Vigilus Mevilig: stworzony przez ludzi byt Esuriit mający na celu chronić (?), manifestujący się jako zbiór za dużej ilości kończyn wychodzących z mechanizmów. Bóstwo? Widzi twarze. Kolokuje.
* Adam Nerawol: psychotronik Orbitera, dość dobry, znaleziony przez Klaudię by zająć się Persefoną na Inferni na czas super niebezpiecznej operacji.
* OO Omega Septius: superpancernik Orbitera; przesunięty przez Termię w okolice Bramy Kariańskiej na wypadek poważnych problemów.
* OO Infernia: zakoloidowana, z dwoma torpedami anihilacyjnymi, wysłana przez Bramę do nieznanego sektora by uratować Grupę Ekspedycyjną Kellert. Sukces - Infernia + siedem osób wróciło żywe.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Brama Kariańska: naenergetyzowana, ma jedno nowe, nietypowe połączenie. To sektor Mevilig (acz wygląda na detektorach jak Noviter). Do pilnowania Bramy przesunięto superpancernik Omega Septius.
        1. Sektor Mevilig: nowo odkryty przez Astorię sektor, bardzo niegościnny. Grupa Ekspedycyjna Kellert Astorii straciła tu 6 statków i 200 osób, 5 min po wejściu.
            1. Chmura Piranii: niedaleko Bramy, kosmiczny złom składający się m.in. z Piranii polujących na dowolną jednostkę przechodzącą przez Bramę. Jest tam też OO Savera.
        1. Sektor Noviter: Termia myślała, że TAM prowadzi Brama Eteryczna; niestety, myliła się. Tym razem Brama prowadzi do Mevilig...


## Czas

* Opóźnienie: 4
* Dni: 3

## Konflikty

* 1 - Klaudia robi analizę danych z sektora Noviter, używając K1 jako komponent.
    * TrZ+4+3Og:
    * VVVzXXz: Persi zmienia osobowości defensywnie; info o tym jak sektor wygląda i że przeciwnik koroduje TAI w 5 minut.
* 2 - Klaudia szuka "naszych" jednostek po sensorach pasywnych. Które statki niedawno, które dawno, trajektoria jednostek, rozłożenie złomu...
    * TrZ+3
    * VVzVX: Savera jest daleko, wymaga manewrów; Klaudia ma dowód że na Saverze ktoś żyje i wie gdzie ona jest. Jest jeszcze jedna duża jednostka z SOS.
* 3 - Eustachy zdecydował się odpalić torpedę. Taką, z sygnałami i aktywnym sonarem. Niech ona ściągnie ogień i pokaże co tu się dzieje - i Infernia ma informacje.
    * TrZ+3+3O
    * XV: Planeta WIE, że wiązka plugawiąca nie działa; torpeda pokazała jak działają Piranie; utorowana droga do Savery.
* 4 - Lecimy pasywnie na Saverę.
    * TrZ+4+1O
    * Vz: Infernia + Arianna dały radę. Lokacja: Savera.
* 5 - Wysyłamy Elenę samą - z zestawem środków medycznych od Martyna i ładunków wybuchowych Eustachego. By wysłać "niepotrzebną część statku" w kosmos by zrobić tło.
    * TrZ+2+3O
    * VzV: Elena na statku; widzi kamerami i systemami statku
    * (+2O) XX: "kapitan" jej SZUKA i ona czuje Vigilusa
    * (+2Vg) XVXV: Elena gubi Vigilusa, który włączył czerwie. Zabiła jakiegoś nieszczęśnika środkami Martyna. Eidolon gaśnie; udało jej się ewakuować. Dała potrzebne info.
* 6 - Eustachy używa torped m.in "torped z piłami tarczowymi" by odseperować statek na 2 kawałki.
    * TrZ+4+5O
    * VX: separacja udana, ale bardzo zainteresowane Piranie
    * (+Vg) XVzV: Vigilus odszyfrował Orbiter i tauntuje, zmusił innych Orbiterowców do pokazania się. Eustachy świetnie odseparował części i "przesunął" by Martyn mógł wpełznąć.
* 7 - Martyn -> w szczelinę między czystym statkiem. Uratować tych 17 ludzi.
    * Tr+3+5O
    * XOXVX: Martyn nie zdąży ewakuować na czas; Piranie blisko, bóstwo się pojawiło
    * (+M =9O) X: Martyn próbuje przepiąć na "chroń nas też", ale bóstwo masakruje "ofiary"
    * (+3Vg zachęta) OVm: szturm Arianny (Otto, paladyni, Emulatorka). Ratujemy kogo się da -> 7 osób

## Inne
### Projekt sesji
#### Narzędzia
##### Koncept

1. Odebranie kontroli (mechanika?), nie wiesz co się stanie, nie przewidzisz konsekwencji
2. Coś nieznanego, niedotykalnego, "overwhelming"
3. Disturbing visuals
4. Wysokie stawki
5. Ciągła, rosnąca presja (ale tor ma być niewidoczny!)
6. Elementy uwolnienia napięcia, komediowe, śmiech
7. Sukces horroru - nigdy nie będzie jak było. Porażka - koniec lokalnego świata postaci.
8. Trudne, niejednoznaczne moralnie decyzje pod presją
9. SIS musi być tożsamy między graczami i MG - to jest ważniejsze niż kiedykolwiek. Konsekwencje muszą być zrozumiałe. Nie ZNANE a ZROZUMIAŁE.

##### Implementacja

1. Odebranie kontroli (mechanika?), nie wiesz co się stanie, nie przewidzisz konsekwencji
    * Kultystyczny terrorform "reaguje" na magię i głośne akcje (1-10 entropicznych; zwykle 3.)
        * Możliwość manifestacji terrorforma. Echo Esuriit, ale jedyne w okolicy.
    * Nie mogą używać swoich głównych silnych stron.
    * Dwa specjalne tory: MORALE (załogi) i DETEKCJA (Terrorform, Pożeracze). Len: 6.
2. Coś nieznanego, niedotykalnego, "overwhelming"
    * Kultystyczny terrorform -> pojawia się "znikąd", zasilany ludźmi; "wampiryczny kult". Esuriit > all.
    * Pożeracze -> nie wiadomo które elementy nieożywione to Pożeracze
3. Disturbing visuals
    * Gwiazda przytłumiona (swarm Dysona)
    * Komunikacja na kanałach mówiąca wiersze i słowa o Pożeraczach i Terrorformie i "grzechu" -> TAI które oszalały
    * Na wszystkich obrazach. We wszystkich rzeźbach. Wszystkie ryciny - twarze zostały usunięte.
    * Wszyscy noszą dziwne maski by nie było widać ich twarzy
    * TAI Grupy Ekspedycyjnej Kellert dołączyły do Pożeraczy.
    * Korytarze, strasznie długie korytarze.
    * Część postaci zaczyna panikować. Nie rozumieją, nie radzą sobie.
    * System orbitalny dookoła planety. Tam - farma ludzi.
4. Wysokie stawki
    * Ludzka baza, około 5000-6000 ludzi.
    * Uratowanie załogi 5-6 statków (~300 osób; z tego uratujemy max. 80; z tego 30 nakarmiło terrorforma)
    * Pomniejsze ludzkie stateczki; Raman Torvis, który wie, że Semarakein trafił do ich bazy. Chce uratowania rodziny.
5. Ciągła, rosnąca presja
    * Infernia sukcesywnie ma swój tor "ukrycia". Jak przekroczymy, musimy UCIEKAĆ. Len: 6.
    * Ujawnić kult terrorforma? Jeśli tak... ci ludzie nie mają szans.
        * Pożeracze uniemożliwią komukolwiek i w jakikolwiek sposób wysłać tu floty. "Nie tacy próbowali".
6. Elementy uwolnienia napięcia, komediowe, śmiech
    * Śmieszne maski, Raman który jest może niezłym pilotem ale uważa Infernię za 'luksusowy statek'.
    * Elena i Eustachy.
7. Sukces horroru - nigdy nie będzie jak było. Porażka - koniec lokalnego świata postaci.
    * Mamy jedną akcję. Potem raczej nikt już nie autoryzuje operacji w tym sektorze.
    * Każde działanie graczy coś zmieni i zniszczy.
8. Trudne, niejednoznaczne moralnie decyzje
    * Co z kultystami? Oni poświęcają ludzi i tworzą strasznego terrorforma; "nasz potwór jest większy niż ich"
    * Czy zostawimy tych ludzi tutaj? Infernia nie ma jak ich ewakuować. Oni się biją kto może wejść na pokład Inferni.
9. SIS musi być tożsamy między graczami i MG - to jest ważniejsze niż kiedykolwiek. Konsekwencje muszą być zrozumiałe. Nie ZNANE a ZROZUMIAŁE.
    * Wyjaśnienie przez MG przed sesją, że sesja jest śmiertelnie niebezpieczna. Można stracić część załogi, raczej nie nazwane osoby, ale ten. Proceed with caution.
    * Straciliśmy 6 statków. 2 krążowniki, 2 niszczyciele, science, swarmer. Nie wiemy do czego. Zero sygnału.

#### Przeciwnik

1. esuriit-Skażony mechaniczny terrorform
2. Wielka planetoida Kalarfam
3. Ciągły niedobór energii i konieczność chowania się przed Pożeraczami... i terrorformami
4. kultyści w bazie.
5. Sam teren bazy i dotarcie do niej, dookoła planetoidy Kalarfam.

#### Sceny

Podczas całej sesji - 3 entropiczne esuriit. W wypadku magii - 5 entropicznych esuriit a nie 3 magiczne.

1. Infernia szuka zaginionych jednostek. Zbliża się do niej złom kosmiczny. Złom kosmiczny to nie złom. To Pożeracze - zmierzają w kierunku Inferni. Jest ich za dużo by wszystkie zniszczyć... jak sharkticony. A użycie głośnych działań - ściągnie terrorforma (o którym nie wiedzą).
    * KONFLIKTY: czy Inferni uda się odlecieć?
1. Planetoida Kalarfam; Infernia za sygnałami SOS itp. wlatuje do środka i szuka źródła sygnału. Ostrzeliwuje ją mały stateczek który próbuje Infernię wyprowadzić na bezpieczną przestrzeń. To budzi terrorforma w planetoidzie, który poluje na Infernię i ów stateczek.
    * POKAZUJE: zagrożenie z "prostych rzeczy". Pułapka wynikająca z ixiońskiego terrorforma polującego na byty.
    * AKTORZY: planetoida, terrorform, stateczek
    * KONFLIKTY: czy stateczek przetrwa? czy Infernia ucieknie terrorformowi bez szkód?
1. Wejście na martwy statek 'OO Savera'. Próba odnalezienia map / sektora / where the hell are we. Wrogowie: Tech-Mawurm, Necroguard, Tech-Rockmorph, Soulmorph. I szepty. Szepty o terrorformie. I jeszcze gasnący ludzie, należący już częściowo do szalonej TAI.
    * POKAZUJE: tu się strasznie coś złego stało...
    * KONFLIKTY: dyskretne dostanie się do danych, wycofanie się bez złamania sygnału, COUP DE GRACE, czy dojdą do info o "szalonych TAI"?
1. Ludzka baza; reaktor, pole niewidoczności, kultyści esuriit trzymający bazę przy życiu składając ofiary. Ten odcinek Netflixa ze statkiem kosmicznym. A dotrzeć tam... Infernia jest duża.
    * KONFLIKTY: czy Infernia będzie w dobrym stanie / nie uszkodzi maskowania? Czy połapią się w rozkładzie bazy? Czy dojdą do tego czemu wszyscy mają maski? Czemu nikt nie mówi gdzie są ludzie z Grupy Ekspedycyjnej Kellert? Odnajdą ich w tunelach NIE ściągając Pożeraczy / terrorforma?
1. Ludzie w bazie ROZPACZLIWIE chcą dostać się na pokład Inferni. Ewakuacja? Ratunek? Dzięki Xardasowi! Rozpaczliwe błagania, dadzą wszystko. Uciec z tego piekła. Ambrozja -> ludzie padają na ziemię chłeptać krew.
    * KONFLIKTY: czy Infernia... odleci? Czy będzie wojna Infernia (200) vs większość populacji? Czy kogokolwiek uda się uratować? Wezwać terrorforma przypadkiem? Uratować "swoich"?
1. Terrorform
    * (loop)"...za dużo ścięgien, kości, za długie, tak duży, tak dużo, za dużo oczu, za dużo kończyn, kończyny wszędzie, on jest wszędzie, krew wszędzie, zbawiciel, niszczyciel, szepty, tak dużo szeptów, każda twarz szepcze, tak wiele twarzy, każda twarz jest jego, żyje w każdej twarzy, ostrej, przekłuwające, tnące, mechanizmy, ścięgna..."

#### Wsparcie graficzne (rysunki)

"Pożeracze": 

https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/471e1f8b-52a2-4674-b387-62664840ab6b/db1v3d9-5bd60e16-2a6f-41c2-b91e-4fd1583d4b5a.png/v1/fill/w_1024,h_576,q_75,strp/planet_eater_by_iblackrow-db1v3d9.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwic3ViIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl0sIm9iaiI6W1t7InBhdGgiOiIvZi80NzFlMWY4Yi01MmEyLTQ2NzQtYjM4Ny02MjY2NDg0MGFiNmIvZGIxdjNkOS01YmQ2MGUxNi0yYTZmLTQxYzItYjkxZS00ZmQxNTgzZDRiNWEucG5nIiwid2lkdGgiOiI8PTEwMjQiLCJoZWlnaHQiOiI8PTU3NiJ9XV19.fArgrlSjuB6p6YOI0TCJ_lEmxh71CFp3xPBjtHbmYTk

https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/2cd89210-7688-4043-bb99-a82eac25198d/dlszf3-bfb766c0-1737-46e1-a29e-ce8166eae501.jpg/v1/fill/w_737,h_1000,q_75,strp/nuclear_planet_eater_by_graysapphire.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwic3ViIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl0sIm9iaiI6W1t7InBhdGgiOiIvZi8yY2Q4OTIxMC03Njg4LTQwNDMtYmI5OS1hODJlYWMyNTE5OGQvZGxzemYzLWJmYjc2NmMwLTE3MzctNDZlMS1hMjllLWNlODE2NmVhZTUwMS5qcGciLCJ3aWR0aCI6Ijw9NzM3IiwiaGVpZ2h0IjoiPD0xMDAwIn1dXX0.506sa_NzNIQA5zyNeF6Qg1gOSqIAySOPpzuK7cANPXc

Jak to wygląda:

https://vignette.wikia.nocookie.net/deadspace/images/f/f5/12001069_1492495657739704_5654452977.jpg/revision/latest/scale-to-width-down/2000?cb=20161004163147

https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/e4269b07-4c51-4846-a24a-2dfc1580a5f6/d9dy40v-d62e5a0b-28e4-45ea-aefe-936de16e1cc0.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwic3ViIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsImF1ZCI6WyJ1cm46c2VydmljZTpmaWxlLmRvd25sb2FkIl0sIm9iaiI6W1t7InBhdGgiOiIvZi9lNDI2OWIwNy00YzUxLTQ4NDYtYTI0YS0yZGZjMTU4MGE1ZjYvZDlkeTQwdi1kNjJlNWEwYi0yOGU0LTQ1ZWEtYWVmZS05MzZkZTE2ZTFjYzAuanBnIn1dXX0.SACJvt9kKjlSmqve-Bk3FqU4eaZIms4Ks3XQJ1FsVcM

Statek:

https://i.pinimg.com/originals/7d/c1/df/7dc1df119e21eee0a084fc3021cc6fce.jpg

Zintegrowany kapitan:

https://qph.fs.quoracdn.net/main-qimg-e3679007d55fa452e0f020451e59322d-c

Terrorform (closest):

https://i.pinimg.com/originals/49/34/54/493454d7a33815d4e9a997cb6931b553.png
