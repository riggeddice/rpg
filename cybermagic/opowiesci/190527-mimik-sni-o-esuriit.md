## Metadane

* title: "Mimik śni o Esuriit"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [190524 - Córka Mimika](190524-corka-mimika)

### Chronologiczna

* [190524 - Córka Mimika](190524-corka-mimika)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. Czy Pięknotka opanuje viciniusy pomagając Wiktorowi?
2. Czy Pięknotce uda się uzyskać odpowiednie środki z Senetis?

### Wizja

* Istoty się Wiktorowi rozchodzą; Mirela zniknęła

### Tory

* .

Sceny:

* Scena 1: .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena 1: Uciekająca Mirela**

Pięknotka jest na Trzęsawisku, u Wiktora. Przyniosła mu kolejne zapasy. Nie dlatego jednak Wiktor ją poprosił o pomoc - dlatego, że zgubił Mirelę. Mirela uciekła na bagno, jest zbyt głodna i zbyt przerażona. Użyła Ścieżek i przeniosła się gdzieś w promieniu Pustogoru. Wiktor poprosił Pięknotkę, by ona poszła znaleźć Mirelę - w innym wypadku on osobiście spali wszystko na drodze do Mireli. Wiktor dał Pięknotce trochę krwi Mireli.

Adela Kirys. Pięknotka po raz pierwszy od dawna się z nią spotkała. Adela powiedziała, że Mirela miała sny. Adela poprosiła Pięknotkę o to, by ona weszła w jej umysł. Może coś znajdzie w tym, co Mirela mówiła. Pięknotka jest zdziwiona, ale Adela naprawdę chce Pięknotce pomóc uratować Mirelę. (TpM: S). Mirela skarżyła się na koszmary senne, koszmary o nieskończonym głodzie. W miasteczku o łodzi w kształcie buta. I tam zapewne chciała się dostać. O dziwo, Wiktor powiedział, że Mirela płynnie kontroluje Ścieżki - ona potrafi się nimi poruszać. On nie ma pojęcia dlaczego.

Adela oddała Pięknotce swoje kosmetyki. Naładowane pozytywnymi uczuciami Adeli będą stanowić dowód dla Mireli, że Pięknotka pochodzi od Adeli. Wiktor odeskortował Pięknotkę do granicy Trzęsawiska. Pięknotka udała sie więc do Czółenka - zdążyć zanim Mirela coś narozrabia.

**Scena 2: Mimik w Czółenku**

Czółenko to niewielka miejscowość. Słynie z dziwnego posągu i z wielkiej studni na rynku. W Czółenku jest jeden konstruminus. Pięknotka nie zamierza się z nim kontaktować. Rzuciła silne zaklęcie bazowane m.in. na krwi Mireli - chce znaleźć mimiczkę. Magiczny sukces - udało się, ale jej smak i ból (chce być kosmetyczką, nie terminuską) został wysłany i do Mireli i do Esuriit. Pięknotka wie już - Mirela znajduje się w lokalnej tancbudzie.

Pięknotka poszła odwiedzić mimiczkę. Zamknięta impreza. Pięknotka - śliczna Diakonka - bez większego problemu przechodzi przez bramkarza (Tp:P). By Mirela rozpoznała w Pięknotce potencjalnego sojusznika - Pięknotka użyła kosmetyków Adeli. I Pięknotka zobaczyła - Mirela siedzi koło DJa i karmi się energią ich wszystkich. Zasila się wszystkimi i rośnie w moc. Pięknotka podeszła bliżej by Mirela mogła wyczuć perfumy Adeli i by Pięknotka mogła porozmawiać.

Pięknotka pokazała Adeli, że DJ i ludzie są już mocno wydrenowani. Spytała, czemu mimiczka żre tyle energii - ona chce "nakarmić dziecko". Czyżby Mirela miała umiejętności oceny sytuacji godne Adeli? Mirela spróbowała odciąć pożeranie, ale nie umie. Pięknotka kazała jej iść do ubikacji i tam ją przemalowała - niech nikt mimiczki nie pozna (TpZ: SS). Mirela nie umie wyłączyć asymilacji energii przez obecne Esuriit; Pięknotka jej nie dotyka. Nikt nie może jej dotknąć.

Wyszły bezpiecznie z tancbudy. Pięknotka spróbowała przekonać Mirelę, by ta nie robiła niczego z Esuriit, ale to jak groch o ścianę. Mirela chce pomóc "dziecku" - jest zbyt naiwna i za mało wie, by spodziewać się potencjalnego ataku. Nie wie czym jest Esuriit i jak jest kosmicznie groźne. Niekończąca się, złowroga wola pełna głodu. Pięknotka chce Erwina do pomocy. Mirela się nie zgodziła. Pięknotka poprosiła Adelę o mediację. Adela serio przekonywała Mirelę, by ta się zgodziła - ale stanęło na tym, że Adela też się musi pojawić. Czarodziejka się ucieszyła. Pięknotka potrzebuje alkoholu.

**Scena 3: Esuriit w Czółenku**

Pięknotka, Erwin, Adela i Mirela są w Czółenku. Ten biedny, pojedynczy konstruminus dalej nic nie wie. Może i lepiej. Pięknotka nie umie przeżyć tego, że nie dała rady przekonać naiwnego mimika...

Erwin spróbował troszkę rozbawić Mirelę, ale mu nie wyszło. Adela ją przytuliła, po czym odskoczyła z piskiem. Pięknotka przesłała Erwinowi po hipernecie informację, że mają do czynienia z Esuriit. Ten użył magicznego wykrywania by to znaleźć; wykorzystuje moc Mireli by znaleźć to źródło (TrM+2=12,3,6=S). Erwin znalazł cel. Zbladł - ma mały węzełek Esuriit, nic jeszcze wielkiego, ale ukryty za lustrami. I tu jest kłopot; Esuriit + lustra. Erwin poprosił Pięknotkę, by przekonała Mirelę i Adelę oraz by ONA je ewakuowała. On rozwiąże problem tego węzełka, wezwie wsparcie.

Pięknotka przekonała Adelę, że Esuriit jest śmiertelnie niebezpieczne dla Mireli. Niech pomoże przekonać Mirelę. Udało się. (Tr:9,3,7=SS). Pięknotce i Adeli udało się przekonać Mirelę. Czas powrotu na bagna. Mirela jest załamanym mimikiem - Adela naruszyła swoją przyjaźń by tylko ratować Mirelę. Pięknotka, Adela i Mirela oddaliły się w kierunku Trzęsawiska, a Erwin wziął tematy z Esuriit na siebie...

**Sprawdzenie Torów** ():

* Zdrowie Mireli: 3: .
* Moc Mireli: 3: .
* Kłopoty w Czółenku: 5: 3
* Sny Esuriit: 3: 2

**Epilog**:

* Tak, było tam dziecko. I siłom Pustogoru udało się to dziecko uratować.

## Streszczenie

Mirela uciekła Wiktorowi Satarailowi z Trzęsawiska, więc ów poprosił o pomoc Pięknotkę. Mirela wyczuła Esuriit w Czółenku i chciała nakarmić głód; Pięknotka jednak dała radę ją znaleźć i nie dopuścić do Pożarcia Mireli przez Esuriit; zamiast tego ściągnęła oddział terminusów którzy sami rozwiązali problem. No i wycofała Mirelę z powrotem do Wiktora.

## Progresja

* Mirela Satarail: potrafi kontrolować Ścieżki Trzęsawiska Zjawosztup. Potrafi przenieść się tam, gdzie chce.
* Mirela Satarail: bardzo wyczulona na energię Esuriit i zdolna do wykrywania jej z dalekiej odległości.

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: pozyskała krew Mireli i kosmetyki Adeli by znaleźć i wkupić się w łaski Mireli, po czym nie dopuściła do spotkania Mireli z Esuriit i wezwała grupę terminusów do rozwiązania problemu.
* Wiktor Satarail: zmartwiony ucieczką Mireli; poprosił Pięknotkę (alternatywą jest jego osobiste działanie). Dał Pięknotkce trochę krwi Mireli by ta zrobiła detektor.
* Adela Kirys: chce być taka jak Wiktor, ale ów ją zbywa; dała Pięknotce trochę swoich kosmetyków by Mirela się jej nie bała. Aha, zaprzyjaźniła się z Mirelą.
* Mirela Satarail: wyczuła Esuriit z oddali, uciekła z Trzęsawiska by ratować ludzi dotkniętych Esuriit. Ma bardzo złą ocenę sytuacji; uratowała ją Pięknotka zanim Esuriit jej nie Pożarło.
* Erwin Galilien: najlepszy katalista w okolicy, wpadł pomóc Pięknotce znaleźć Mirelę a znalazł faktyczne Esuriit.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Czółenko: niewielka miejscowość pod Podwiertem, niedaleko Trzęsawiska Zjawosztup, raczej na terenie korporacyjnym
                                1. Tancbuda: miejsce, gdzie Mirela wzmacnia DJa pełnią mocy i żywi się wszystkimi by nasycić Esuriit by nie było głodne (co jest niemożliwe).
                                1. Bunkry: Mirela i Erwin znaleźli ten bunkier, w którym Alicja miała Paniczne Skażenie. Tam jest niewielki Węzeł Esuriit; terminusi go wyczyszczą.
                        1. Trzęsawisko Zjawosztup

## Czas

* Opóźnienie: 14
* Dni: 2
