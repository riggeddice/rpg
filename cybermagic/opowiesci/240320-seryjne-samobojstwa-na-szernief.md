## Metadane

* title: "Seryjne samobójstwa na Szernief"
* threads: problemy-con-szernief
* motives: the-man-in-black, the-collector, the-experimenter, local-team-decent, deathwish, energia-interis, bardzo-grozne-potwory, grade-of-success, presja-czasu, policja-odwraca-oczy
* gm: żółw
* players: magda_zaba, zygfryd, seba, hela

## Kontynuacja
### Kampanijna

* [240214 - Relikwia z androida?!](240214-relikwia-z-androida)

### Chronologiczna

* [240214 - Relikwia z androida?!](240214-relikwia-z-androida)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * Inspiracje
        * Alien in the Darkness, efekt spotkania z Flair ^^
    * Mortal Love "I Want to Die"
        * "I'm too tired of this life | - I just wanna die - | All I need is my big sleep | - I just wanna die -" && "I want to die | But really, I am already dead "
        * Ofiary Pożeracza Szczęścia (atinaspes interis) nie chcą już żyć. Funkcjonują, ale nie nazwałbym tego "życiem".
* Opowieść o (Theme and vision):
    * "Kalista i Felina próbują rozwiązać anomalny problem własnymi siłami, nie licząc na Agencję."
    * "Stacja - wysoko naenergetyzowana magicznie - jest idealnym miejscem na eksperyment z potworem Interis z perspektywy mrocznego agenta... lub ofiary tego potwora"
        * Ireneusz Kralmik, badacz egzotycznych istot, napotkał Ćmę Pożerającą Wszystko Co Ma Wartość. Ona odebrała mu moralność i blokady i dała mu swoje 'dziecko'.
        * Ireneusz znalazł tą stację i wprowadził Ćmę na jej pokład. Mimo, że sam jest amoralnym socjopatą, nie był taki póki Ćma nie odebrała mu wszystkiego co cenił.
        * Istoty z którymi mamy do czynienia są koszmarne. Zła osoba w złym miejscu w złym czasie - i koniec. 
    * Lokalizacja: Stacja CON Szernief
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * the-man-in-black: Kalista (XD). Musi ukryć fakt istnienia magii i obecności potwora. Jest to nie mniej ważne niż usunięcie samego potwora, by Agencja się nie pojawiła. (tor wiedzy)
    * the-collector: Ćmokształtny Pożeracz Życia (atinaspes interis). Akumuluje jak najwięcej energii i wspomnień w odpowiedniej kolejności, tworząc Gniazdo, by przejść do kolejnego punktu Cyklu
    * the-experimenter: naukowiec (z zewnątrz), który wprowadził Potwora na pokład Szernief, by zobaczyć jak on zadziała. Nie jest świadomy, że on sam jest Dotknięty Interis.
    * local-team-decent: Zespół to grupa lokalsów na Szernief wspieranych z cienia przez Kalistę
    * deathwish: konsekwencje napotkania (atinaspes interis). Pragnienie śmierci, samobójstwa i brak nadziei. Pustka w sercu. Nieważne, kim ta osoba wcześniej nie była - od tej pory jest Pusta w środku i pragnie śmierci.
    * energia-interis: odebranie wszelkiej nadziei, szansy, pragnienia istnienia przez Ćmostwora. Utrata wszystkiego. Są tylko Jego Puste Oczy, które zabiorą Ci wszystko...
    * bardzo-grozne-potwory: atinaspes interis, ćmostwór wysysający nadzieję i wszystko co piękne, którego oczy niewolą i prowadzą do samobójstw; tu - zakłada Gniazdo.
    * grade-of-success: strukturyzacja sesji pod kątem (wykrywalność) oraz (ludzkie ofiary) oraz (konieczność wezwania Agencji)
    * presja-czasu: IRL co 30 minut atinaspes dodaje +1 osobę do Gniazda. Po przekroczeniu masy krytycznej (czas skończony), Agencja MUSI być wezwana - Zespół nie jest w stanie wygrać bez strat.
    * policja-odwraca-oczy: Felina jest skłonna zaryzykować; niech lokalne siły Szernief spróbują rozwiązać problem bez wzywania Agencji
* O co grają Gracze?
    * Sukces: zniszczenie Ćmy (atinaspes interis)
    * Porażka:
        * tor paniki - coś się dzieje na Stacji, utrata ludzi. Duże straty.
        * tor wiedzy o magii i wpływu magii - to jest bardzo niebezpieczne.
        * tor Gniazda atinaspes.
        * niewykrycie Ireneusza. Dalej jest na Stacji. Dalej jego pusty umysł może budować i działać.
        * wezwanie Agencji
* O co gra MG?
    * Highlevel
        * Setting jest "STORY-FIRST". Niski stopień trudności, dużo ludzi, kiepskie warunki.
        * zniszczą Ćmę. Tego jestem prawie pewny.
        * Chcę, by sami znaleźli sposób na zniszczenie Ćmy - jak?
        * Chcę sprawdzić czy są w stanie poradzić sobie z lokalnymi postaciami w settingu.
    * Co uzyskać fabularnie
        * Ćma zmieni jedną Nazwaną postać. Którą - zależy od działań Graczy. Default: Kalista.
        * Chcę, by powstało Gniazdo atinaspes. Wtedy trzeba coś zrobić, by je zneutralizować zanim atinaspes się nie rozmnoży i nie pójdzie dalej.
        * Niewiele chcę dostać. Mam tory, będę je uczciwie prowadził. Za niski poziom trudności.
        * Kalista musi użyć magii by zwalczać atinaspes (Ćmę)?
* Default Future
    * Ćma jest zniszczona, ale Ireneusz jest tutaj dalej. Agencja zostanie wezwana (nie gram o to, ale to jest w default future).
    * Gniazdo Atinaspes istnieje i może pozwolić naszej małej atinaspes stać się dużą groźną atinaspes.
    * Ćma Spojrzy W Oczy Aeriny, Mawira, Feliny albo Kalisty. Spowoduje to poważną zmianę przyszłości.
* Dilemma: Ireneusz - ofiara, potwór? Jak mu pomóc? Da się?
* Crisis source: 
    * HEART: "Ireneusz napotkał Ćmę. Ireneusz zaczął rozprzestrzeniać Ćmę."
    * VISIBLE: "Seryjne samobójstwa ludzi"
        
### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

KONTEKST:

* Na pokładzie Szernief znajduje się Ćmokształtny Pożeracz Szczęścia (Interis) (atinaspes interis).
    * niewidoczny w świetle (więc przez elektronikę, w normalny sposób tak)
    * jeśli na niego spojrzysz, zabiera Ci wszystko i zadaje straszne cierpienie emocjonalne; "to wszystko Twoja wina" i nie da się zapomnieć
    * chce stworzyć odpowiednią tkaninę życia i emocji używając emocji swoich ofiar
    * ćmokształtny potwór składający się ze świetlistych macek, składający Gniazdo z różnych emocji
    * ofiary prowadzą do Pożeracza kolejne ofiary na Żywienie, by odzyskać Cokolwiek. One wiedzą. Ale nie umieją powiedzieć...
* Ireneusz Kralmik, naukowiec, wprowadził [atinaspes interis] na pokład Szernief, by zobaczyć jak Ćma Pożerająca Wartość Życia działa i móc ją wykorzystać
* Ireneusz Kralmik nie ma pojęcia, że on sam jest ofiarą Ćmy - stracił granice i bariery. Działa tylko zgodnie z mechanizmami rozprzestrzeniania Ćmy.

CHRONOLOGIA:

* rok temu
    * Ireneusz Kralmik, badający dziwne istoty napotkał [atinaspes interis], czyli [Ćmokształtny Pożeracz Szczęścia]. Zafascynował się tą istotą.
    * Gdzieś na przestrzeni tego czasu stracił moralność i blokady; zostały one pożarte przez atinaspes.
    * Od tej pory Ireneusz próbuje zapewnić bezpieczeństwo atinaspes i ją żywić. I rozmnożyć.
* 2 miesiące temu
    * Ireneusz Kralmik, naukowiec, wprowadził [atinaspes interis], czyli [Ćmokształtny Pożeracz Szczęścia] na Szernief.
        * Pozyskał "żonę", która była nośnikiem atinaspes i z nią przybył. Potem z "żony" wypełzł larwo-kokon.
            * Teatr Symboliczny w Sektorze Mieszkalnym Gamma
        * "Żona" była pierwszą ofiarą. Poszła z kochankiem w kosmos, patrzeć jak pracuje się nad tarczą i wyszła aktywnie poza tarczę solarną. Nie było dość biomasy, ale nikt nie zwrócił na to uwagi.
            * Przedstawiała się jako Amanda Ferres, studentka Ireneusza Kralmika
* niedawno
    * Atinaspes się wykluła. Ireneusz utrzymuje lokalizację - Teatr Symboliczny w Gamma
    * Atinaspes zaczęła się przemieszczać i polować na ludzi, odbierając im za dużo i prowadząc do ich samobójstw.
        * Ludzie zaczynają sami prowadzić innych ludzi do atinaspes by powstało właściwe Gniazdo
        * -> wpierw ludzie pod kątem PRZESTRZENI potem pod kątem ZNAJOMOŚCI
    * Kalista wykryła anomalie wśród samobójstw i uruchomiła Agentów Szernief. Nasz Zespół.

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

* F0: TUTORIAL
    * Zespół jest wezwany jako Grupa Ochrony; nauczyciel zabił ucznia (który się nie bronił O_O) i teraz próbuje się dostać do innej ofiary, której nie było w szkole, do Teatru Symbolicznego.
    * Cel nauczyciela: zabić drugiego ucznia, by on nie mógł rozprzestrzenić Obrazu Interis dalej.
* F1: Pozyskiwanie Energii
    * PROXIMITY: Teatr Symboliczny (mieszkalna strefa Gamma)
    * Kalista skompilowała listę ofiar: 
        * (Amanda (żona), uczucie: BRAK, wyklucie się) - "wypadek, wyszła w kosmos" 
        * Ewin Parg (górnik w kopalni), uczucie: ekstatyczna radość bo powiedziała TAK (14 dni)
        * Matala Parg (górnik w kopalni), uczucie: rozpacz bo go zabrakło (8 dni)
        * Sergan Ordik kochanek Amandy pain (7 dni)
        * Eia-16 (savaranin na siłowni) duma z ciężkiej pracy (5 dni)
        * Maja (influencerka) radość z poznania wielkich ludzi (4 dni) 
        * Franz Kravis (nauczyciel) (1 dzień)
    * Dzień 1: Ofiara: she-Mawirowiec z Przetwarzania (przyprowadzi medyka). "GDZIE! MOJE! SERCE!!!" (Sirus). Ma pręt i chce się bić.
    * Dzień 2: Ofiara: Medyk, który wycina sobie oczy. "Nie sprowadzę zła na dom". "Zabij mnie."
    * Konieczność ewakuacji Gamma?
    * Konsekwencje dla populacji turystów?
* F2: Konstrukcja Gniazda
    * Teatr Symboliczny jest gotowy. Potrzebna jest Odpowiednia Ofiara.
    * Zorganizowanie spektaklu w Teatrze
* F3: Endgame
    * 21:00 czasu rzeczywistego INIT
    * Aerina idzie na spektakl, wraz z kilkunastoma osobami
    * atinaspes zaczyna operację INFEST Aerina
* Overall
    * chains
        * tor paniki: (cisza - nerwowość - redukcja przybyć - evac - PANIC!)
        * tor zniszczeń: (brak - niewielkie straty - spore straty - strukturalna destrukcja)
        * tor wiedzy o magii i wpływu magii: (cisza - niektórzy - sporo - problem)
        * tor Gniazda Atinaspes: (brak - (nieaktywne) - aktywne - rozmnoży się)
    * stakes
        * stan Aeriny - zdrowa czy Dotknięta Ćmą?
        * stan Kalisty / Mawira - jak przetrwają tą sesję?
        * czy trzeba wezwać Agencję?
        * czy Ireneusz może działać dalej?
    * opponent
        * Ćma atinaspes interis
        * Ireneusz, Sługa Atinaspes
    * problem
        * .

## Sesja - analiza
### Fiszki

.

### Scena Zero - impl
#### Jak sesja została opisana ludziom

Pełna życia i radości turystka aspirująca do bycia influencerką popełniła samobójstwo. 

Niedługo potem skontaktowała się z Wami czarodziejka i poprosiła Was o pomoc. Podobno to nie jest pierwsze dziwne samobójstwo na Stacji Kosmicznej i czarodziejka podejrzewa działanie magii. Choć czemu właśnie ta influencerka – czarodziejka nie wie.

Do tego momentu nie mieliście pojęcia o istnieniu magii. Z tego co się dowiedzieliście, prawie nikt o magii nie wie i wiedzieć nie może, bo dojdzie do katastrofy. Nie wiedzieliście też, że na tej stacji ukrywa się od lat lokalna czarodziejka, której na Stacji - na której mieszkacie od lat - zależy.

Nie spodziewaliście się, że Wy – inżynier paneli słonecznych, ochroniarz, księgowy... - będziecie mieć do czynienia z czymś nienaturalnym czy magicznym. Jak czarodziejka wyjaśniła, zanim pomoc się pojawi to miną dwa tygodnie i sporo osób może zginąć.

Z dobrych wieści – macie przyjaciół na stacji, macie różne role i funkcje i jest sporo osób chętnych Wam pomóc, nawet, jeśli do końca nie wiedzą o co Wam chodzi. Ze złych wieści, część z tych osób może zginąć.

#### Implementacja właściwa

Grupa agentów ochrony dostała wiadomość, że Franz - nauczyciel pracujący w jednej ze szkół - zabił swojego ucznia (nożem) oraz próbuje się gdzieś przedostać. Ich zadaniem było zatrzymać Franza. Pierwszy pomysł agentów polegał na tym, by szybko ewakuować następny segment i zablokować śluzy.

Tp +2:

* V: ludzie zostali ewakuowani, śluzy zablokowane

Udało się – Franz został uwięziony w jednej ze śluz. Komunikacja z nim przez komunikatory i kamery (na wypadek gdyby to było zaraźliwe) - dlaczego to zrobił? O co chodziło?

Ex +3:

* V: nie chce więcej zabijać, ale musiał to zrobić
    * oczy. Widział oczy.
* (+Z) otwarcie drzwi w taki sposób by się nie spodziewał że tam będzie teraquidowiec ze strzałką: V: Franz złapany żywcem

Franz jest bardzo pusty w środku. Uśmiecha się, ale jest niekompletny. Próbował dostać się do kolejnego miejsca żeby zabić kolejną osobę. Nie chciał zabijać nikogo, ale to było jedyne wyjście - jego zdaniem. Agenci ochrony postanowili, że unieszkodliwią go strzałką usypiającą. Sukces.

### Sesja Właściwa - impl

Biosyntka Medyczna szybko sprawdziła dane medyczne - ma do nich dostęp. Okazuje się, że dane wskazują na to, że Franz jest zdrowy. To nie powinno być zaraźliwe, w ogóle nie powinno nic z nim być nie tak. Coś jest nie tak z jego psychiką, więc uznano to za atak psychotyczny i wsadzili go na leki. Niespecjalnie ktoś się nim przejmował.

Inspektor Celny stwierdził że kluczową informacją jest to, co łączy influencerkę która popełniła samobójstwo i Franza, który wpierw zabił ucznia a potem chciał popełnić samobójstwo. Czy się spotkali? Gdzie?

Tr +2:

* V: .
    * Franz i dzieciak nie mieli kontaktu z influencerką
    * Franz i dzieciak kochali sztukę. Nawet taką na tej stacji.
* V: Maja jako influencerka była bardzo zainteresowana kulturą.
* V: (z pomocą Naukowca) - jest coś co łączy Maję i Franza - Teatr Symboliczny. Jedyne miejsce, gdzie byli razem. Ale nie na tym samym przedstawieniu.

Wynik okazał się być bardzo interesujący - Franz oraz influencerka Maja nigdy się nie spotkali. Czyli cokolwiek to jest, to nie przechodzi przez transmisji osobistą. Może wspólne miejsce? I tak - Maja została ściągnięta propagować Stację. To była młoda dziewczyna, która dopiero próbowała się wybić. Popularyzator na tej stacji (Yarlov) zainwestował w nią i ma teraz problem czy mowę wydać te nagrania czy to będzie źerowanie na jej śmierci.

Co ich połączyło – Teatr Symboliczny, zarówno nauczyciel ze swoim uczniem często tam przebywał jak i influencerka odwiedziła to miejsce by zobaczyć unikalną sztukę savarańsko-drakolicko-szerniefską. Ale nigdy nie byli na tym samym spektaklu. Czyli nie chodzi o sam spektakl tylko coś w tamtym miejscu? Teorie krążyły dookoła nietypowych fal dźwiękowych czy pola magicznego; za mało danych.

Tymczasem Biosyntka zdecydowała się przesłuchać nauczyciela. Do tego celu zdjęła go z medykamentów. Chodzi o to, by dało się go dobrze podłączyć do aparatury i rozmawiać z nim jak nie jest na lekach. To pozwoli lepiej ocenić jego stan i jak działa.

Tp +2:

* X: "JA CI TO WYJAŚNIĘ"
* V: (działanie Naukowca) powołanie się na to, że to eksperymentalna procedura medyczna.

Jeden z lekarzy na miejscu zauważył co robi Biosyntka i - nie traktując jej jako osobę mającą prawa - zaczął jej wyjaśniać że mogłaby zrobić krzywdę Franzowi, bo on będzie cierpiał, więc Biosyntka nie może go odpiąć od leków. Biosyntka zauważyła, że tak naprawdę ona ma uprawnienia do leczenia eksperymentalnego.

Faktycznie, Naukowiec zrobiła dla Biosyntki odpowiednie uprawnienia. Lekarz, widząc te wszystkie parametry i górę biurokracji która go czeka jeśli będzie przeszkadzał, szybko się oddalił. Biosyntka zdecydowała się przesłuchać Franza a Szuler udał się do Teatru Symbolicznego by zebrać dźwięki ze spektaklu i samego miejsca. Może to pomoże Franzowi coś sobie przypomnieć lub zdobyć jakieś informacje.

Biosyntka przesłuchuje Franza pod aparaturą, która działa jak wykrywacz kłamstw. On rozpoznał dźwięki z teatru. Uśmiechnął się. Chciałby ich nigdy nie rozpoznać. Żałuje, że tam kiedykolwiek poszedł.

TrZ+3:

* X: (następna Faza - następna ofiara)
* V: Franz cierpi coraz mocniej; Interis go pożera
* (-> Tp -Z) V, bo Biosyntka obiecała mu, że go zabije i cierpienie się skończy:
    * to zabiera mu wartość życia, wszystko zanika
    * zabił chłopca, bo nie chciał go zaprowadzić (pokusa)
    * chciał zabić Tarę, żeby JEJ nie zabrano. A skąd powiązanie? Miał z nią romans.
    * Franz poszedł na zaplecze z Tarą. Tam TO się stało. Nie pamięta detali


Franz cierpi. Biosyntka - rozmawiając z nim – zauważyła, że on nieprawidłowo działa pod wykrywaczem kłamstw. Ma uszkodzoną pamięć, ma nieprawidłowy profil emocjonalny. On niewiele czuje; każda minuta zadaje mu ból. Nie pamięta niczego pięknego z czasów życia z rodziną. Nie pamięta co dokładnie stało się w teatrze. Jest w ruinie psychicznej.

Biosyntka obiecała mu, że jeśli on będzie współpracował to ona pozwoli mu umrzeć. To sprawiło we zrobił wszystko co w jego mocy by odpowiedzieć na jej pytania. Dlaczego zabił chłopca? Bo stracił zbyt wiele. Za dużo zostało mu odebrane przez te OCZY. Nie pamięta jakie oczy, pamięta tylko że tam były. I jakby doprowadził chłopca, to odzyskałby coś z tego co stracił.

A dlaczego został złapany? Próbował dostać się do Teatru Symbolicznego, zabić jedną techniczkę, Tarę. Chciał ją zabić, żeby jej nie stało się to co jemu, a potem chciał siebie zabić. Czemu ją? Przyznał się (całkowicie bezdusznie i bez jakichkolwiek uczuć) że miał z nią romans; zdradzał żonę.

Szuler znajduje się w Teatrze; bez problemu zlokalizował Tarę. Nie chciała z nim rozmawiać (nic dziwnego; miała romans z człowiekiem podejrzewanym o morderstwo i nie chciała być na czołówkach gazet), ale Szuler jest powiązany z mawirowcami; to nie jest drakolita któremu się odmawia. Plus wygląda nieprawdopodobnie dobrze.

TrZ+3:

* V: Franz się zmienił, faktycznie.
* V: (Naukowiec) po opisie zidentyfikował energię Interis, współpracując z Czarodziejką.


Tara pamięta, że mieli romans na zapleczu. Zaplecze zwykle jest dobrze oświetlone i zwykle są tam ludzie, więc musieli poczekać na odpowiedni moment. Zupełnie nie zauważyła faktu, że on się zmienił. Jej pamięć też jest uszkodzona. Też jest niekompletna, ale w inny sposób. Ona jest bardzo „sobą”, ale jej percepcja tego jak normalnie zachowują się ludzie została usunięta.

To pozwoliło Naukowcowi zrobić hipotezę na temat tego jaka tu jest Energia, z którą walczą (z pomocą Czarodziejki). Interis – Koniec Wszystkiego, Ostateczna Pustka i Zwycięstwo Entropii. Jeżeli jest tu coś, co wykorzystuje energię magiczną i oni nie mają jak się przed tym bronić… to bardzo trudno będzie sobie poradzić.

Dzięki temu, że Czarodziejka patrzyła, jak Agencja działa, doszła do tego jak działa lapis. Ale skąd pozyskać lapis na stacji? 

Szuler wie - pośród niektórych bogatych turystów czasami zdarzają się nietypowe ozdoby. Wydawałoby się, że ludzie skupią się na platynie, złocie, rzeczach wartościowych. Niektórzy jednak mają biżuterię lapisową. Teraz to ma sens. Nawet jeżeli ludzie nie wiedzą o magii, to część osób, które wiedzą o magii mogą zabezpieczyć swoich krewnych i znajomych zdobywając dla nich lapisową biżuterię.

To znaczy, że trzeba ją tylko ukraść. Ktoś wcześniej na tej stacji już to zrobił. A Szuler zna większość kryjówek, ma znajomości i wie jak to zrobić by móc pozyskać coś ważnego

ExZ+3:

* V: Mają godną porcję lapisu

Faktycznie – Szulerowi udało się pozyskać lapis do konfrontacji z potworem. Mają lapis, wiedzą, gdzie potwór się znajduje. Nie mają wprawy z walki z potworami, ale mają nadzieję, że sobie poradzą.

Wpierw Inspektor Celny skutecznie wygnał ludzi z terenu Teatru, wspierany przez Szulera. I potem będą w stanie tam wejść.

Tp +3:

* X: Ofiara działa
* V: Teatr zamknięty; skoro zarówno mafia (Szuler) jak i prawo (Inspektor) chcą, to nie ma szans, będzie ewakuacja.

Rozpoczęcie procedury ewakuacji zostało częściowo przerwane - dziwne hałasy „z dołu” (Przetwarzanie, pod Teatrem Symbolicznym); coś na kształt krzyków, dźwięk człowieka zabijanego przez człowieka i dźwięk uderzania metalu o rury. Szuler oraz Celnik szybko tam pobiegli a Biosyntka i Naukowiec zabezpieczali Teatr, by zapewnić, że ludzi tu nie będzie i nic nie wyjdzie tą stroną.

Zespół zapalił światło. Na dole – drakolitka, z wyrwanymi przez siebie własnymi oczami, z łomem, atakująca byty w powietrzu których nie ma i od czasu do czasu uderzająca w rury. Wyraźnie też jest coś z nią nie tak. Zabiła 2 ludzi. Złapała jednego i zaczęła pożerać jego zwłoki; czując smak krwi rozdarła się rozpaczą i wściekłością. Niestety, to Adarva - przyjaciółka Inspektora.

* X: To przyjaciółka i szefowa kultu Inspektora
* V: Ginie z ręki (noża) Szulera

Jedyne utrudnienie – Adarva jest osobą, która nigdy w życiu nie poszłaby do Teatru; nigdy nie socjalizowałaby się z niewiernymi. Ale Inspektor słusznie zauważył – Teatr (zaplecze) jest połączony rurami z Przetwarzaniem, gdzie zginęła Adarva. Tak więc jeżeli przeciwnik jest ruchomy, to nadal wszystko się zgadza.

Dobrze. Czyli czas wejść na zaplecze teatru.

Zaplecze Teatru wygląda dosyć normalnie. Bardzo spartańskie, jak to savarańska konstrukcja. Wyraźnie robią to co są w stanie tym co mają pod ręką. Nie ma tu nic dziwnego i wszystko wygląda w miarę w porządku. 

Naukowiec, wiedząc o energii Interis, wzięła zbiór różnych roślin, bytów biologicznych… kopiując dokładnie to, co robiła Agencja przed nimi. Może da się znaleźć w jakiś sposób dowód na obecność jakiejś Energii.

Ex Z+3:

* X: zostało 2 savaran
* V: mamy sprawny sposób wykrycia potwora
* X: potwór tam jest

Bardzo niepokojąca była obecność 2 savaran. Nic nie robili, zajmują się jakąś formą pracy przy kurtynach. Są aż tak bardzo spustoszeni, że nawet nie zauważają obecności Zespołu. Biosyntka ich uśpiła używając strzykawki z czymś; w końcu praca w skrzydle medycznym zobowiązuje.

Zdecydowali się złożyć ze sprzętu, który znajduje się tutaj taki snop światła który prowadzi z góry prosto na jedno miejsce, jak na scenę. I wyłączyć wszystkie inne światła. Zrobili to.

Świat się zmienił. Tam, gdzie niczego nie było - przy ścianie znajdują się kokony pulsujące w różnych kolorach. Na suficie COŚ jest, choć nikt na to wolał nie patrzeć. Uśpieni savaranie są połączeni jakimiś dziwnymi odnogami owadzimi z kokonami. I kokonów jest 7. Ogólnie zapanował terror – poza Biosyntką. Ona nic nie widzi. Nic. Dla niej nic się nie zmieniło. Widząc reakcję pozostałych, podeszła do przełącznika i włączyła światła. Koszmarny świat zniknął.

* (+3V) snop światła i kokony V: są bezpiecznie w świetle
* (-3X) Biosyntka zgasi światło


Ok - sytuacja zasadniczo jest dziwna. Wszystko wskazuje na to, że „tamtych rzeczy” po prostu nie ma, dopóki pali się światło. Jak mogą sobie z tym poradzić? Czarodziejka zasugerowała, że może użyć magii i spróbować to „usunąć” z rzeczywistości. Zapytana jak bardzo jest pewna że sobie poradzi, powiedziała, że jakieś 25%. Nigdy nie robiła czegoś takiego i nie wie nawet jak zacząć.

Inspektor Celny oraz Szuler zdecydowali się pójść pozyskać dodatkową broń i osłaniać Teatr Symboliczny ze wszystkich możliwych stron, ściągając wsparcie bojowe. A Biosyntka i Naukowiec postanowiły spróbować pokonać potwora i zbadać te kokony i co one oznaczają znajdując się w strumieniu światła. W końcu – jeżeli Biosyntka niczego nie widzi, to najpewniej jej też nie widać, prawda?

* V: (skalpel laserowy) niszczymy kokon

Wykonali więc ten plan - Naukowiec stoi w środku snopu światła a Biosyntka podeszła z laserowym skalpelem zobaczyć, czy jest w stanie wejść w interakcję z kokonem. Skalpel jest podczerwony, czyste ciepło, Naukowiec powiedziała, że tak - kokon ulega zniszczeniu. Naukowiec zobaczyła szybki ruch ze strony czegoś na suficie w stronę Biosyntki, ale Biosyntka zignorowała potencjalne ostrzeżenie ze strony Naukowiec.

* V: Zniszczone kokony

Potężne uderzenie ze strony Ćmy Interis rozszarpało część struktury Biosyntki. Zniszczony bok, utrata kontroli w jednej ręce, czerwone sygnały alarmowe i uszkodzone systemy zasilające. Biosyntka NADAL nie widzi co ją zaatakowało, ale kontynuuje operację - rozpaczliwie atakuje wszystkie kokony o których wie, próbując uniknąć ataku ze strony Ćmy.

* X: Interis pożera Naukowca

Naukowiec nie pozwoli na to, by jej sojuszniczka zginęła w taki sposób. Rzuciła się w stronę na Ćmę Interis – i spojrzała na wzory na jej skrzydłach. Spojrzała na OCZY. Poczuła, jak wszystko co pięknie zaczyna w niej zanikać. Zaczyna nie być niczego.

* X: Ćma unieruchomiona, Biosyntka zniszczona

Biosyntka, na granicy terminalnego zniszczenia, wykorzystała ciężkie dekoracje znajdujące się na jednej ze ścian oraz ekstrapolując, gdzie Ćma powinna się znajdować złapała dekorację i zrzuciła je na Ćmę. Niestety, nie tylko nie zgadła prawidłowo, gdzie Ćma się znajduje, ale też z uwagi na uszkodzenia nie była w stanie uniknąć spadającej dekoracji. Biosyntka, zmiażdżona ciężkimi dekoracjami, została nieodwracalnie zniszczona.

* V: Atak chemiczny, Naukowiec i Ćma zniszczeni

Śmierć sojuszniczki obudziła COŚ w Naukowiec. Ostatnie wspomnienia. Ostatnie uczucia. Naukowiec wzięła najostrzejsze chemikalia jakie posiadała - dekombinatory biologiczne, które miały posłużyć do niszczenia niebezpiecznych eksperymentów – i rzuciła się na Ćmę by zniszczyć jej strukturę biologiczną. Ćma nie była w stanie odskoczyć ani uniknąć ataku – Biosyntka ją skutecznie unieruchomiła, przytrzaskując jej jedno skrzydło.

Zarówno Naukowiec jak i Ćma Interis stopiły się w ogniu morderczych chemikaliów. Naukowiec na tym etapie niewiele już czuła.

Potwór został zniszczony, Stacja uratowana.

## Streszczenie

Atinaspes Interis został wprowadzony na Szernief przez Kralmika. Zespół miejscowych, wspierany z cienia przez Czarodziejkę, rozpoczął śledztwo po serii samobójstw, odkrywając punkt wspólny - Teatr Symboliczny. W toku śledztwa Zespół odkrywa, że Atinaspes tworzy Gniazdo, by się móc rozprzestrzenić. Atinaspes zostaje zniszczona przez Zespół, acz kosztem dwóch członków Zespołu. Udało się wygrać bez interwencji Agencji. Czarodziejka zrobiła co w jej mocy by odpowiednie osoby wiedziały o bohaterkach Szernief.

## Progresja

* .

## Zasługi

* Klasa Biosynt Medyczny: i: 'Weterynarz'; rozwiązała zagadkę stanu psychicznego Franza i dzięki temu doszła do działania Atinaspes Interis. Walcząc z Ćmą, poświęciła się by ją unieruchomić. KIA.
* Klasa Drakolita Szuler: i: 'Bob'; pozyskał lapis dla Zespołu, przesłuchał Tarę i skutecznie zabił nożem oślepioną szaloną drakolitkę Dotkniętą przez Atinaspes Interis. Uroczy i morderczy.
* Klasa Savaranin Celnik: i: 'Du-42'; znalazł połączenie pomiędzy Franzem i Mają, potem wygnał ludzi z Teatru Symbolicznego. Osłaniał Biosyntkę i Naukowiec na wypadek gdyby Ćma przetrwała i się wydostała.
* Klasa Savaranin Naukowiec: i: 'San-62'; doszła do energii Interis, zdobyła uprawnienia dla Biosyntki i odkryła gdzie jest Atinaspes. Zniszczyła Ćmę dekombinatorami biologicznymi, niszcząc siebie w procesie. KIA.
* Kalista Surilik: czarodziejka i inicjatorka Anomalistów Szernief; działa z cienia i pomogła w zlokalizowaniu danych prowadzących do zagrożenia Atinaspes. Organizowała i działała z cienia, manewrując danymi by nikt nic nie widział.
* Ireneusz Kralmik: naukowiec w służbie atinaspes; zafascynowany Atinaspes Interis, POŻARTY przez atinaspes i roznoszący Ziarno Ćmy. Wprowadził ją na Szernief, ale zawiódł; opuścił Szernief szukając kolejnej Atinaspes.
* Markus Yarlow: popularyzator; on ściągnął Maję (influencerkę) na Stację, przyjaciel Naukowiec. Załamany, że Maja nie żyje. Przygotował dla niej wszystkie atrakcje i chciał wybić stację wśród turystów.
* Tara Fezinis: techniczka z Teatru Symbolicznego, pozbawiona strachu i umiejętności rozpoznania że z ludźmi coś jest nie tak przez Atinaspes Interis. Miała romans z Franzem (nauczycielem).

## Frakcje

* Anomaliści Szernief: Stworzenie frakcji. Niestety, 2 osoby (Biosyntka Medyczna i Naukowiec Pasjonat) zginęli w walce z Anomalią Interis.
* Anomaliści Szernief: Dostęp do porcji lapisu, pozyskanej z przemytu przez jednego z członków. Opracowanie przez Czarodziejkę, jak najlepiej Lapisu użyć.

## Fakty Lokalizacji

* CON Szernief: Anomaliści Szernief zostali stworzeni i po raz pierwszy poradzili sobie z potworem; Atinaspes Interis. Stacja nie ucierpiała za mocno i nie musiała czekać na przybycie Agencji.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Kalmantis
            1. Sebirialis, orbita
                1. CON Szernief
                    1. Powłoka Wewnętrzna
                        1. Poziom Minus Jeden
                            1. Obszar Mieszkalny Gamma: mieszany, między savaranami i drakolitami. Ale tylko lokalsi, bez jakichś zewnętrznych.
                                1. Pomieszczenia mieszkalne
                                1. Awaryjny system podtrzymywania życia
                                1. System recyklingu
                                1. Łazienka i higiena
                                1. Siłownia
                                1. Systemy bezpieczeństwa
                                1. Teatr Symboliczny: bezpośrednio nad systemami Przetwarzania; to daje możliwość robienia efektów specjalnych. Tam na zapleczu znajduje się Atinaspes Interis.

## Czas

* Opóźnienie: 97
* Dni: 2
