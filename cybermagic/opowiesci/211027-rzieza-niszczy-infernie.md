## Metadane

* title: "Rzieza niszczy Infernię"
* threads: legenda-arianny, niestabilna-brama, sektor-mevilig
* gm: żółw
* players: kić, fox

## Kontynuacja
### Kampanijna

* [211020 - Kurczakownia](211020-kurczakownia)

### Chronologiczna

* [211020 - Kurczakownia](211020-kurczakownia)

### Plan sesji
#### Co się wydarzyło

* .

#### Sukces graczy

* 

#### TODO

* Klaudia i Rzieza
* Maria chce na Infernię
* Flawia chce autoraportować swoje czyny
* Leona przekonuje Ariannę o problemie z Martynem
* Tivr i ukryta rzadka karta do gry
* Arianna ma kreskówkę na swój temat
* Tajemnicze jednostki zbliżające się do okolic świń
* Projekt Aurum "Fantazmat"
* Admirał Atanair, szepty z Mevilig - próba objęcia Arianny
* Jolanta Sowińska vs Nataniel Morlan - miragent zniszczony na Tivrze

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

OSIEM DNI TEMU.

Infernia, w stanie... nieidealnym leci w kierunku na K1. Ma naprawdę sporo głów do rozdysponowania. Klaudia przeprowadza normalne operacje, lądować zanim statek zostanie zniszczony. Jeszcze w drodze Klaudia spisała na kartce wiadomość do siebie i podbiła do Martyna. Martyn jest sam. Klaudia poprosiła o amnestyki i by on był jej dead man's hand. Martyn spytał, czy przeciwnik może dowiedzieć się o nim. Klaudia nie wie. Więc Martyn musi ukryć to też przed sobą i zapomnieć. Da się załatwić. 2 tygodnie...

Jeszcze zanim jest opcja zanim systemy K1 połączą się z Infernią Klaudia przygotowuje Infernię, raporty itp. Zbiera nagrania, dane z czujników, kopie...

Plan Klaudii - zadokować Infernię. Potem - terminal i skontaktować się z Rziezą.

W hangarze Inferni - ludzie mają hełmy nieprzezroczyste. Nie działa hipernet, wyłączone są wszelkie formy komunikacji. Kramer na nagraniu - infohazard. Gotowi do dekontaminacji. Infernia zaznaczyła, że na pokładzie mają GŁOWY. Time-critical cargo. To wzbudziło konsternację w grupie czyszczącej - totalnie poza procedurami.

Dekontaminacja - oddział "remontowy" w hełmach i wszystkim, dekontaminują itp. Strzykawki itp. A Infernia została ślicznie wyczyszczona z pamięci o infohazardzie, o Rziezy itp.

Arianna opiera się woli i czyszczeniu Rziezy. Ma determinację, by TYM RAZEM było dobrze. By TYM RAZEM nie stało się coś złego. Bo coś ma wrażenie że już miała jakoś.

ExZ (przeczyszczona przez Ataienne)+2+5O(V):

* X: Arianna ma przez następny tydzień uszkodzone kanały magiczne. Nie poczaruje na pełnej mocy. Środki i metody Rziezy odpowiadają imieniu. "The Butcher".
* O: Rzieza orientuje się, że Ataienne przeczyściła Ariannę w Trzecim Raju - ślad na arcymagu jest bezsporny. Ale przez to Rzieza dochodzi do wniosku - jeśli ślad Ataienne jest widoczny przez niego, to ślad Rziezy też będzie widoczny.
* O: Arianna opiera się Rziezie. Użyła mocy Ataienne, którą przypomniała sobie z trudem. RZIEZA STANĄŁ. Zaczął reintegrować pamięć Arianny. Rzieza zauważył, że Ataienne jest TAI władająca mocą magiczną. Poziom zagrożenia Ataienne został podniesiony do czerwonego przez Rziezę.

Arianna jest nie wiadomo gdzie, ale wszędzie widzi stoje twarze. CZY NIE WYSZŁA ZE ŚWIĄTYNI? EUSTACHY ZAWIÓDŁ?

Arianna czeka do momentu aż Rzieza skończy ją reintegrować i próbuje go zalać informacjami i się wyrwać (Hr). Nie. Połączenie z ELENĄ. Po sympatii. Czystą krwią, tak, jak Arianna widziała u Martyna i Eleny. Rozpaczliwa prośba mentalna do Eleny -> OBUDŹ SIĘ I WYCIĄGNIJ MNIE STĄD! SOS!

ExZM+4:

* V: Elena się obudziła i jest w pełni władz fizycznych i mentalnych na pełnej adrenalinie.

Elena jest bez ubrania, sprzętu itp. Leży na łóżek, podpięta do komputerów - które jeszcze nie wiedzą że jesteś aktywna. Dookoła leżą członkowie Inferni. Elena jest na K1, w jakimś BlackLab. Klaudia i Martyn są w bardziej zaawansowanych maszynach (Rzieza musi jakoś odwróci amnestyki). Elena czeka na moment kiedy lekarze są w innym miejscu - trzeba obudzić Otto. Coś zagraża Ariannie - trzeba zrobić dywersję.

Elena wyczekuje aż lekarze nie patrzą - zwinny skok i strzał z adrenalinki w Otto. On ma się obudzić. Jemu można ufać.

ExZM+4:

* OmOm: ESURIIT INFUSION. Elena trochę spanikowała. Pełna moc Esuriit. RISING FURY.
* Xm: Elena, która ma w pewien sposób obsesję na punkcie ochrony swoich i bycia "tą dobrą kuzynką", tą, przez którą nikt nie zginie... 
    * Wybudziła WSZYSTKICH (poza Eustachym)
    * Wszyscy są w głodzie Esuriit. Poza Arianną. I Martynem + Klaudią, bo inny, super-zaawansowany biovat.
* V: Elena dała radę zatrzymać manifestację Zbawiciela - Niszczyciela.

Gdy Arianna doszła do zmysłów, gdy wypadła z atrium Rziezy... wszyscy w tym laboratorium poza załogą Inferni byli już martwi. Kilkanaście lekarzy, personelu itp. Jesteście zamknięci.

Otto przejął dowodzenie. Blood legion. Elena wycałowuje Eustachego czy coś. On śpi. Nic nie wie. I się nie dowie. Czarne laboratorium jest odcięte. Infernia to hivemind Esuriit, głodny i chętny do osłony Arianny za cenę Legionu...

KLAUDIA. Przebudzenie. Dziesiątki luster. Luster Rziezy. Pamiętasz. Rzieza poinformował ją, że doszło do awarii przy dekontaminacji i mamy Esuriit. Katastrofę.

Klaudia ściąga jednostki czyszczące po hipernecie. Rzieza odstrzelił laboratorium od K1. Dryfuje w kosmosie. Rzieza chce ewakuować Ariannę i Klaudię.

Plan jest taki - Martyn bierze **Ariannę** za zakładniczkę. Potrzebują statku, który będzie izolacją. Arianna i Martyn mają tam docelowo trafić. A Krwawy Legion niech kombinuje jak wyciągnąć Ariannę XD. Przyczepy do przewozu koni - w jednej przegródce Martyn z Arianną z wozem. W innym - reszta lub Martyn zabije Ariannę. I deszcz lapisu...

Martyn zażądał odpowiedniego koktajlu stymulantów. Biovat zapytał "are you sure". Martyn potwierdził.

ExZ+3:

* X: Martyn jest zdjęty z akcji na tydzień. Ciężko odchorowuje. Po wszystkim.
* X: Martyn wypada na miesiąc.
* X: Martyn jest przesunięty mentalnie w czas wojny. Martyn będzie "maksymalnie efektywny". I to będzie pamiętał i będzie jemu zapamiętane.
* Xz: Zasobowe simulacrum. Martyn eksterminuje załogę Inferni. Wchłonął za dużo Esuriit.

Klaudia widzi co się dzieje i ostrzega Ariannę. ARIANNA widzi co się dzieje i krzyczy "Ognia!". I Rzieza strzelił z K1. Laboratorium się przebiło.

Strzał K1 mający unieszkodliwić simulacrum i cokolwiek tu się dzieje. Strzał perfekcyjny K1.

TrZ+3:

* V: Perfekcyjny strzał, simulacrum po krótkiej eksterminacji wyssane w kosmos.
* V: Martynowi się udało o tyle, że za simulacrum poszła "ta energia" w większości. Załoga Inferni jest otumaniona, na adrenalinie, ale nie jest tak opętana. Ale statek jest rozszczelniony.

Klaudia próbuje korzystać z mocy obliczeniowej K1 (w końcu Rzieza ją jakoś podpiął). Rzucić się na jakiś słabiej stabilny stół, skoro on też tam poleci. I niech on przytka dziurę. I dostaje "normalny" koktajl stymulantów. Nie to, co chciał Martyn...

Tr (zasób skonsumowany) (niepełna) +3:

* V: Klaudii udało się przytkać stołem zanim za duża populacja zginęła w kosmosie. 
* V: Udało jej się uniknąć poważniejszych obrażeń. Klaudia mistrz surfingu.

Rzieza wysłał jednostkę by otoczyła polem siłowym to laboratorium. Więc nie wymrą z braku tlenu.

Arianna zasłania Martyna własną piersią przed ludźmi chcącymi go rozszarpać. "Martyn zrobił to DLA NIEJ. Ona opada z sił i oni muszą się do jej sieci podpiąć bo opada z sił. Muszą podpiąć się do biovatów. Ona zaczerpnie z tego energię."

Tr (zasób skonsumowany) + 3:

* V: Uratowała Martyna. Przekierowała ich uwagę na siebie.
* X: Nie wszyscy przetrwają biovaty. Rezydualne Esuriit.
* X: Nie do skasowania pamięć. Za mocno się odbiło. Nieskasowywalne - ale to znaczy, że Rzieza nie jest w stanie Was skasować.
* V: Ariannie UDAŁO się do nich przemówić. Udało się. Weszli do biovatów. Klaudii się udało ich "wyłączyć". Na szczęście - uratowało to sporo istnień.

Sytuacja wreszcie została opanowana. Arianna i Klaudia mają chwilę na rozmowę z Rziezą w trakcie gdy ekipa ratunkowa próbuje naprawić co się da.

## Streszczenie

Infernia wraca z Mevilig. Rzieza chce wyczyścić im pamięć o sobie. Arianna się opiera. Rzieza dowiaduje się o Ataienne. Arianna budzi Elenę po sympatii, Elena sprowadza Esuriit do laboratorium dekontaminacyjnego K1. Dużo śmierci. Martyn próbuje to opanować - budzi swoje simulacrum. Arianna i Klaudia opanowują przy pomocy Rziezy sytuację. 37% załogi Inferni nie żyje, Flawia wyssana w kosmos, Elena złamana, Martyn w szpitalu.

## Progresja

* Arianna Verlen: jej umysł i pamięć zostały odbudowane przez Rziezę. Pamięta moc Ataienne i to, co Ataienne jej zrobiła.
* Martyn Hiwasser: 5 miesięcy regeneracji w szpitalu na K1. Jego arkin jest żywy, niestabilny i szuka. Ma aktywne simulacrum w formie "fingers". Część osób na Inferni się go boi.
* Elena Verlen: straszna rekonstrukcja Esuriit. Libido++. Obsesja w stronę Eustachego. Elena nie czuje, że kontroluje swoje ciało i umysł. Przebudowa Paradoksu na Eustachego.
* Flawia Blakenbauer: wyssana w kosmos. Nie zginęła, ale jej los i los Orbitera się na zawsze rozdzieliły.
* OO Infernia: w wyniku dewastacji załogi przez simulacrum Martyna i emisję Esuriit Eleny, Infernia straciła 37% załogi. I Flawię. A załoga Inferni nie może mieć wymazanej pamięci.
* Izabela Zarantel: uwierzyła w to, że Arianna Verlen jest prawdziwą inkarnacją aspektu Zbawiciela. To jest jedyne, co ma sens w kontekście tego WSZYSTKIEGO. Jest wyznawcą. To Arianna zażyczyła sobie, by Izabela przetrwała to wszystko - i dlatego Izabela żyje.

### Frakcji

* .

## Zasługi

* Arianna Verlen: chcąc powstrzymać ingerencję w jej pamięć Rziezy użyła podświadomie technik Ataienne. Rzieza dowiedział się o Ataienne. Arianna wezwała Elenę na pomoc, która zalała Esuriit laboratorium. Arianna z Klaudią próbowała opanować załogę Inferni i jak Martyn wyssał energię Esuriit z załogi, dała radę to zrobić.
* Klaudia Stryk: most notable action - surfowanie po blacie stołu by zapobiec wyssaniu załogantów Inferni w kosmos. Opracowała też plan jak uratować Infernię od Esuriit - niestety, Martyn nie dał rady go wykonać. Współpracowała ze Rziezą by uratować kogo się da i by Rzieza miał pretekst do nie zabijania Inferni.
* TAI Rzieza d'K1: chciał usunąć pamięć Inferni o swej obecności; natrafił na Ariannę i wykrył ingerencję Ataienne. Potem współpracował z Arianną i Klaudią jak shit hit the fan. MÓGŁ i POWINIEN wszystkich zabić, ale NIE CHCIAŁ i udało mu się jednak większość uratować. Za co nikt mu nie podziękował. De facto, zabił Infernię.
* Elena Verlen: obudzona rozpaczliwie przez Ariannę. Zdecentrowana. Próbowała wszystkich uratować i wlała Esuriit do laboratorium (eksplozja Paradoksu). Z trudem powstrzymała manifestację Zbawiciela-Niszczyciela. Potężnie Skażona i przekształcona przez Esuriit; resztę operacji dobierała się do śpiącego Eustachego. NIE MOŻE TEGO SOBIE WYBACZYĆ.
* Martyn Hiwasser: gdy Klaudia powiedziała mu o problemie ze Skażeniem Esuriit, zaakceptował plan. Ale Esuriit jego też zmogło - obudził simulacrum i zaczął zabijać załogę Inferni (w samoobronie i dla demonstracji). Przynajmniej udało mu się wyssać Esuriit. Zapłacił straszną cenę - szpital, reputacja, te wszystkie śmierci na sumieniu. Znowu.
* Otto Azgorn: dowodził wszystkimi jak byli opętani Esuriit. Nadal lojalny Ariannie. Zniszczył laboratorium i zabił wszystkich. Przetrwał walkę z simulacrum Martyna. Ostro pocięty.
* Flawia Blakenbauer: w wyniku Krwawej Emisji Eleny i działań simulacrum Martyna wyssało ją w kosmos z laboratorium przy K1. Jej los się na zawsze rozdzielił z Orbiterem.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy
                    1. Laboratoria Dekontaminacyjne: jedno z nich zostało zniszczone przez Dotyk Esuriit spowodowany przez Elenę, wyrzucone z K1 przez Rziezę i zestrzelone. Duże ofiary w ludziach.

## Czas

* Opóźnienie: 2
* Dni: 5

## Konflikty

* 1 - Arianna opiera się woli i czyszczeniu Rziezy. Ma determinację, by TYM RAZEM było dobrze. By TYM RAZEM nie stało się coś złego. Bo coś ma wrażenie że już miała jakoś.
    * ExZ (przeczyszczona przez Ataienne)+2+5O(V)
    * XOvOv: Arianna ma uszkodzone kanały magiczne, Rzieza dowiaduje się o Ataienne i się na nią kieruje, reintegruje pamięć Arianny
* 2 - Arianna łączy się z Eleną. Po sympatii. Czystą krwią, tak, jak Arianna widziała u Martyna i Eleny. Rozpaczliwa prośba mentalna do Eleny -> OBUDŹ SIĘ I WYCIĄGNIJ MNIE STĄD! SOS!
    * ExZM+4
    * V: Elena się obudziła i jest w pełni władz fizycznych i mentalnych na pełnej adrenalinie.
* 3 - Elena wyczekuje aż lekarze nie patrzą - zwinny skok i strzał z adrenalinki w Otto. On ma się obudzić. Jemu można ufać.
    * ExZM+4
    * OmOm: ESURIIT INFUSION. Elena trochę spanikowała. Pełna moc Esuriit. RISING FURY.
    * Xm: Elena, która ma w pewien sposób obsesję na punkcie ochrony swoich i bycia "tą dobrą kuzynką", tą, przez którą nikt nie zginie... wszyscy w głodzie Esuriit.
    * V: ALE zatrzymała manifestację Zbawiciela - Niszczyciela. Przynajmniej tyle.
* 4 - Martyn zażądał odpowiedniego koktajlu stymulantów. Biovat zapytał "are you sure". Martyn potwierdził.
    * ExZ+3
    * XXXXz: Martyn zdjęty z akcji na miesiąc, zasobowe simulacrum. Martyn eksterminuje załogę Inferni.
* 5 - Strzał K1 mający unieszkodliwić simulacrum i cokolwiek tu się dzieje. Strzał - Rzieza perfekcyjnie strzela z K1.
    * TrZ+3
    * VV: Perfekcyjny strzał wysysa simulacrum w kosmos. Martyn wyssał Esuriit swoim simulacrum, więc jest dobrze.
* 6 - Klaudia próbuje korzystać z mocy obliczeniowej K1 (w końcu Rzieza ją jakoś podpiął). Rzucić się na jakiś słabiej stabilny stół, skoro on też tam poleci. Uratować przed dekompresją.
    * Tr (zasób skonsumowany) (niepełna) +3
    * VV: Klaudii udało się przytkać stołem zanim za duża populacja zginęła w kosmosie. Uniknęła poważniejszych obrażeń.
* 7 - Arianna zasłania Martyna własną piersią przed ludźmi chcącymi go rozszarpać. "Martyn zrobił to DLA NIEJ. Ona opada z sił i oni mają wejść do biovatów."
    * Tr (zasób skonsumowany) + 3:
    * VXXV: Martyn uratowany, Arianna do nich przemówiła i zadziałało. Ale - ich pamięć jest nie do skasowania i nie wszyscy przetrwają biovaty.
