## Metadane

* title: "Magitrownia Finis Vitae"
* threads: narodziny-skorpiona
* gm: żółw
* players: onyks

## Kontynuacja
### Kampanijna

* [190704 - Magitrownia Finis Vitae](190704-magitrownia-finis-vitae)

### Chronologiczna

* [190704 - Magitrownia Finis Vitae](190704-magitrownia-finis-vitae)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. 

### Wizja

* .

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Grupa pięciu magów - Druid, Detektyw, Scrapper, Lekarz i Reporter byli, poza jedną osobą w magitrowni, jedynymi magami na tym niewielkim terenie. I pewnego dnia doszło do katastrofy - magitrownia połączyła się z Finis Vitae i wszyscy na pokładzie magitrowni zginęli. Udało się jedynie dwóch techników uratować przez wsadzenie ich do skafandrów i zamknięcie w zbiorniku na odpady niebezpieczne. Magitrownia tymczasem zaczęła coraz bardziej destabilizować otoczenie...

Następnego dnia - do Lekarza przyszedł chłopak, Piotrek, skarżąc się że jego ukochany pies go ugryzł. Zapytany, powiedział, że Burek go odpędzał z jakiegoś miejsca. A w końcu go ugryzł i Piotrek musiał od Burka uciec. Pies tam został. Sama rana Piotrka jest magiczna; po wyleczeniu jej przez Lekarza okazało się, że miała aspekty lecznicze. To było dziwne.

Druid tymczasem dotarł na miejsce gdzie był Burek. Pies nie żyje - ale na warcie stoi echo psa. W sztucznym gaiku znajduje się Skażeniec - wąż, bardzo jadowity i niebezpieczny. Chce pożerać i rosnąć. Jest też echo psa, nie pozwalające nikomu zbliżyć się do węża. Druid wykorzystał wsparcie echa psa i pojmał węża, acz duch psa został ukąszony i zgasł.

Po pokazaniu węża reszcie zespołu, Detektyw od razu pomyślał o hodowli viciniusów użytkowych. Poszedł odwiedzić Bartka Urczana i znalazł ślady wskazujące na to, że faktycznie wąż uciekł z jego hodowli. Urczan powiedział, że od wczoraj ma problemy z anomaliami - STRASZNE problemy. Ekrany ochronne przestały działać. Viciniusy się teleportują. Ogólnie, chaos. Czyli... coś jest dziwnego nie tylko w lesie; ogólnie z energią magiczną.

No dobrze - tak więc Detektyw w połączeniu ze Scrapperem dali radę wykrozystać jednego z glukszwajnów hodowli viciniusów i złożyli prowizoryczny detektor chorej energii magicznej (po ekstrakcji z węża). Detektor chrumkając poszedł szukać energii; znalazł Skażonego chochoła i uciekł, ale Zespół ma jednoznaczną odpowiedź - coś stało się w magitrowni...

Reporter zaczął dostawać dziwne sygnały i dźwięki ze swoich anten i komunikacji. Ściągnął Scrappera i przesterowali antenę. Dzięki temu Reporter odszyfrował sygnał - to było SOS i pętla; byli świadkami ostatnich chwil ludzi i czarodziejki z magitrowni. Reporter natychmiast połączył się z technikami z magitrowni; dowiedział się, że nie są w stanie niczego z tym zrobić i są zamknięci. Wyjaśnili jak działa magitrownia - potrzebuje wody z rzeki do generowania energii, ale jednocześnie podziemne zapasowe zbiorniki przebiły się i połączyły z CZYMŚ. I to zniszczyło magitrownię.

Od razu pojawił się pomysł - zbiornik retencyjny. Wyłączmy prąd w magitrowni. Reporter pojechał i przekonał strażnika do użycia zbiornika i wyłączenia magitrowni. Wkręcił go. To kupiło zespołowi czas na zrobienie czegoś z magitrownią.

Jednak Skażenie dalej postępowało - w szkole pojawiła się Efemeryda Nienawiści - hate plague. Jak to cholerstwo wyczyścić? Detektyw przekonał Urczana do pożyczenia Druidowi stada glukszwajnów i Druid wprowadził armię glukszwajnów do szkoły - dzięki temu Efemeryda skończyła w brzuszku świń.

Detektyw przeprowadził Scrappera przez anomalie przestrzenne magitrowni i udało im się uwolnić techników; po wyprowadzeniu dwóch ocalałych nieszczęśników, Lekarz ich odkaził. Pozostała jeszcze jedna ekspedycja - Detektyw, Druid i Scrapper poszli wyłączyć samą magitrownię. Drogę zastąpiły im duchy, ale Druid przekonał je, że pomoże ich rodzinom. Muszą wyłączyć magitrownię, inaczej nie ma nadziei na przyszłość. Duchy się zgodziły i odeszły w niepamięć...

Magitrownia zgasła. Połączenie z Finis Vitae zostało wyłączone...

**Sprawdzenie Torów** ():

* skomplikowane.

**Epilog**:

* .

## Streszczenie

Magitrownia Maczkowiecka połączyła się przypadkowo z Finis Vitae i wszyscy (prawie) na pokładzie zginęli. Lokalny zespół magów dał radę opanować większość efemeryd i anomalii, by w końcu wyłączyć samą magitrownię i rozerwać połączenie między budynkiem i autowarem.

## Progresja

* Patrycja Radniak: dziewięciolatka; straciła matkę w katastrofie magitrowni. Magowie pomogli jej i jej ojcu, jak obiecali matce Patrycji w formie ducha.

### Frakcji

* .

## Zasługi

* Bartek Urczan: chytry hodowca potworów; dobrze żyje z innymi, ale biznes przede wszystkim. Pomógł magom użyczając stado glukszwajnów i współpracował; nie chciał tragedii dla miasteczka.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Maczkowiec
                            1. Hodowla viciniusów: systemy zabezpieczeń, płoty itp przestały działać przez szalejącą magitrownię; sporo anomalicznych problemów
                            1. Magitrownia Stu Maków: przypadkowo podczas pracy połączyła się z Finis Vitae, prawie niszcząc całe miasteczko anomaliami
                            1. Lasek Czerwony: miejsce pojawienia się Skażonego węża leczniczego; pies uratował pana przed wężem kosztem swego życia
                            1. Szkoła: miejsce pojawienia się efemerydy plagi nienawiści; stado glukszwajnów ją zeżarło
                        1. Maczkowiec, obrzeża:
                            1. Zbiornik retencyjny: wykorzystany do tymczasowego pozbawienia magitrowni energii by móc uratować wszystkich w środku

## Czas

* Chronologia: Aktualna chronologia
* Opóźnienie: -1811
* Dni: 2
