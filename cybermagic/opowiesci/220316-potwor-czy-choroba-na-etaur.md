## Metadane

* title: "Potwór czy choroba na EtAur?"
* threads: legenda-arianny, etaur-zwycieska
* gm: żółw
* players: fox, kapsel, kić

## Kontynuacja
### Kampanijna

* [220309 - Upadek Eleny](220309-upadek-eleny)
* [220323 - Zatruta furia gaulronów](220323-zatruta-furia-gaulronow)

### Chronologiczna

* [220309 - Upadek Eleny](220309-upadek-eleny)

## Plan sesji
### Co się wydarzyło

* Koordynator-Antoni-Persefona ukrył obecność mordercy (Daniela Puszanta)
    * Antoni był kiedyś faerilem; powiązany z Danielem przez Arkologię Dekantis
* W Eternijskiej bazie jest w biovatach rodzina mordercy (Daniela Puszanta)
* W Eternijskiej bazie 
    * tien Melania Akacja (tienka Eternijska): zgłosiła problem
    * poprzedni koordynator magazynowni, Rafał Szlędak, zaprzyjaźnił się z TAI Nephthys (zaawansowana Persefona).
        * Nephthys jest zbyt błędna; więc jest wpakowana do "pudełka".
* Baza nazywa się EtAur Zwycięska.
    * tien Maciej Parczak: Eternia; tymczasowo dowodzi stacją (dotknięty Chorobą); faza: Irytka.
    * tien Dominika Perikas: Aurum; druga w dowodzeniu. Przejęła po Rafale. ???
    * Erwin Mumurnik: szef inżynierii; paranoik, trochę zajmuje się szmuglem z lokalnymi piratami
    * tien Feliks Ketran: Eternia; lekarz (dotknięty Chorobą); faza: Irytka. 
    * Suwan Chankar: były komandos, chroni interesy Eterni. Szef ochrony. Nie lubi Orbitera, melancholik i stoik
    * Tymoteusz Czerw: oficer łącznościowy Eterni. Chipper. Hero worship.

Lokalizacja:

+ pomieszczenia mieszkalne
+ podtrzymywanie życia
+ odzyskiwanie wody
+ panele słoneczne i baterniownia 
+ szklarnie
+ stacja przeładunkowa
+ magazyny:
  - płynne
  - stałe 
+ przedszkole
+ stołówki
+ centrum rozrywkowe
+ serwerownia
+ ochrona
+ administracja
+ punkt celny
+ serwisownia
+ atraktory lodowych śmieci - stacja grazerów 
+ stacja pozyskiwania wody

### Co się stanie

* .

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

* .

### Scena Właściwa - impl

Dyskretny komunikat do Arianny. Kody: Eternijskie. tien Melania Akacja. 

* Coś się stało przedszkolance (Marika), przez arkin poszedł chory sygnał; Marika jest szpiegiem Melanii
* Baza jest połączonymi siłami Aurum i Eterni, gdzie Eternia jest dominująca
* 3 tygodnie temu TAI bazy została wyłączona - Nephthys.
* Tydzień temu doszło do przełamania ochrony bazy - pojawił się "potwór". Skanery nie zidentyfikowały TEGO jako człowieka.
    * Aktualnie P.O. TAI jest Antoni. Został sprzęgnięty z AI Core przez Arkin.
    * W bazie są nieudane TAI, eksperymenty, klony... stacja przeładunkowa. Handel Eterni z odrobiną Aurum.
* Melania poprosiła Ariannę o rozwiązanie sprawy. Poważnie podejrzewa Orbiter. Więc - Arianna jako jej nadzieja.

Eustachy - masz spotkanie z Eleną. Przechwyciła go. Uśmiech.

* Elena: "Kiedy zamierzasz mi się oświadczyć?"
* Eustachy: patrzy na nią XD. "Nie zrozumiałem..."
* Elena: uśmiech, lekko szalony, bardzo głodny. "Pytałam, kiedy mi się oświadczysz. JA nie jestem taka jak inne panienki. Dla Ciebie odzyskałam urodę. Teraz NIC nie stoi na drodze temu, żebyś mi się oświadczył."
* Eustachy: "Chyba nie jestem jeszcze gotowy na tak poważny krok..."
* Elena: "kiedy będziesz?"
* Eustachy: "na pewno po służbie. Związek małżeński - muszę być zdrowy na ciele i umyśle"
* Elena: "zastanów się. Jeśli Ty nie będziesz facetem w tym związku - ja Cię do tego zmuszę."

Eustachy uniknął oświadczyn. Chwilowo.

Arianna chce Raoula, Marię (zagrożenie biologiczne; niby potwór). Nie chcemy Leony. Elena na detoksyfikację w jakiejś pomocniczej jednostce (Kladia zrobiła pomniejszy stabilizator Eleny).

Tivr jest sprawny. Lecimy Tivrem. Tymoteusz od łączności lubi Zespół. Chce Ariannę na kawę. Po 10 minutach dał zezwolenie od przełożonego (tien Parczaka). Da się spokojnie wylądować. 

Maria zaproponowała rozkochanie wszystkich w Eustachym +superviagra.

Wylądowaliście bez kłopotu. Na miejscu 10 uzbrojonych. Chankar oddelegował Lamię do zajęcia się załogą Inferni. Lamia poprosiła o przerwę 1h i przekazała Was do kwater. 

Tymoteusz pojawił się, zdziwiony że nie ma Lamii. Z radością do Eustachego i Arianny. 

Klaudia szuka po bazie i danych. Wyciąga podstawowe ogłoszenia. Widzi, że 70% ogłoszeń została usunięta.

OFICJALNIE - nowa TAI dla Inferni. Faktycznie co się spieprzyło z Nephthys. Tymoteusz przyznał, że TAI mu się zgubiła i chciałby ją znaleźć dla Dominiki. Tymoteusz zaprowadził Was do Klaudii i Klaudię do sensownej konsoli. Konsola jest w jednej dodatkowej placówce łączności.

Klaudia ma dostęp do konsoli. Mamy uwalone Nephthys. Czy ona działa? Czy jest odcięta? Czy coś robi...

TrZ+3:

* X: pojawi się Chankar
* V: Klaudia ma informacje. Wbiła się w system
    * Klaudia ma echo Nephthys. Jest tam i działa. Ma ograniczony dostęp, działa z magazynów, jest stamtąd podpięta i pomaga Antoniemu z ukrycia. Jest przeniesiona. Pełni swoją rolę mimo że poza swoimi parametrami.
* Xz: Chankar OPIERDALA Tymoteusza. Inconsolable.
* V: Klaudia ma serię informacji dodatkowych
    * Nephthys - wyłączona, 3 dni później - aktywna ponownie
    * Nephtys
        * Melania powiedziała, że Nephthys nie można było ufać, bo robiła błędy w inwentaryzacji itp.
        * Tymoteusz to potwierdza. Nephthys jest uszkodzonym modelem.
        * W raportach wyraźnie Klaudia widzi, że Nephtys kasowała część rekordów. Aktywnie próbowała być mniej skuteczna.
        * Klaudia wie, że logika Nephthys - priorytetyzacja ochrona bazy i ludzi. Oraz "przyjaciół"
* V: Klaudia ma wystarczający backdoor. Jak długo konsola jest podpięta ma możliwość wbicia się.

Chankar + 2 kolesi. OPIERDOLIŁ Tymoteusza i chce aresztować. Po chwili - zmienił minę na twarzy. Oddalił się. Dowiedział się od Antoniego, że Lamia jest zdjęta. Chankar "ona miała być bezpieczna!!!". Głównie Erwin Mumurnik (główny inżynier) zajmował się Nephthys. Drugą osobą jest Antoni.

Arianna -> Dominika. Ona wie sporo.

* Dominika nic nie wie o obecności Arianny
* Dominika powiedziała o pladze
    * Podobno magia lekarza nie pomogła, może się zaraziła.
* Dominika prosi o wsparcie Arianny
* Lamia jest ważna nie dlatego że JEST ważna ale jest krewną kogoś bardzo ważnego w Eterni

Przy mostku nie ma nikogo. Nie ma strażników. A powinni być. COŚ się stało na stacji. COŚ dziwnego przyszło. Albo ktoś miał Paradoksy albo coś przyszło. To stacja przeładunkowo - coś przyszło w którejś paczce...

## Streszczenie

Na stacji EtAur pojawiły się poważne problemy - potwór? Choroba? Suwan próbuje ustabilizować co się da, ale nic nie może zrobić. Arianna poproszona o pomoc przybyła, ale nikt nie chce z nią praktycznie współpracować. Klaudia doszła do przyczyny problemów - choroba, ale potwór jest wektorem. Arianna doszła do transmisji po kanałach magicznych. Świetnie.

## Progresja

* Elena Verlen: contained. Znajduje się w specjalnym biovacie gdzie jest regenerowana i reanimowana do formy 'kontrolowanego viciniusa'.

### Frakcji

* .

## Zasługi

* Arianna Verlen: koordynacja; do niej odzywa się Eternia i ona jest tą, z którą wszyscy rozmawiają. Doszła do tego, że choroba może rozprzestrzeniać się po kanałach magicznych.
* Eustachy Korkoran: uniknął aresztowania przez siły na EtAur, podzielił siły i szuka potwora? Choroby? Aha, uniknął oświadczyn Eleny.
* Klaudia Stryk: włamuje się do roboczego TAI (nie Nephthys) i zakłada backdoor; dowiaduje się o przeszłości tej bazy.
* Elena Verlen: pyta Eustachego, kiedy ten zamierza jej się oświadczyć. Jej szaleństwo rośnie. Wyłączona, wsadzona do biovatu na regenerację.
* Maria Naavas: zawsze ma "najlepsze" pomysły. Jako, że w EtAur nikt nie lubi Zespołu, zaproponowała rozkochać wszystkich feromonami Eustachego. WTF. Oczywiście - zablokowano ten plan.
* Melania Akacja: eternijska tienka wysokiej klasy, z sił 'kosmicznych', która potrzebuje wsparcia w EtAur. Ale nie z Orbitera - Ariannę osobiście. Jej córka na EtAur jest zagrożona i baza może upaść.
* Suwan Chankar: nie jest w stanie opanować EtAur; nie wie czy walczyć z chorobą czy z potworem. Oddelegował Lamię do Zespołu, ale Lamia zawiodła i dała się zarazić XD. 
* Dominika Perikas: oficjalnie poprosiła Ariannę o pomoc, bo widzi, że z sytuacją można sobie nie poradzić...
* Tymoteusz Czerw: oficer łącznościowy Eterni. Chipper. Hero worship wobec EUSTACHEGO. Chce być jak on. Wpadł w kłopoty u Chankara, bo złamał protokoły by dać dostępy BOHATEROM (Eustachemu et al).

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Elang, księżyc Astorii
                1. EtAur Zwycięska: najgorzej nazwana baza w historii kosmosu; pierwsza baza księżycowa Eterni z Aurum. Coś złego się tam dzieje - potwór? Choroba? Potworna choroba???
 
## Czas

* Opóźnienie: 15
* Dni: 2

## Konflikty

* 1 - Klaudia ma dostęp do konsoli. Mamy uwalone Nephthys. Czy ona działa? Czy jest odcięta? Czy coś robi...
    * TrZ+3
    * XVXzVV: Klaudia ma backdoor i echo Nephthys. Informacja co i jak.
