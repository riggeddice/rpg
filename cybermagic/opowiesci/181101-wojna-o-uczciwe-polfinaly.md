## Metadane

* title: "Wojna o uczciwe półfinały"
* threads: progildie-zaczestwa, alan-opiekun-ucisnionych, deprecated
* motives: gra-supreme-missionforce, kara-nieproporcjonalna, audyt-infiltracyjny, katastrofalny-paradoks, anomalia-enchanter
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [181030 - Zaczęstwiacy czy Karolina?](181030-zaczestwiacy-czy-karolina)

### Chronologiczna

* [181030 - Zaczęstwiacy czy Karolina?](181030-zaczestwiacy-czy-karolina)

## Projektowanie sesji

### Struktura sesji: Wyścig

* **Strona, potrzeba, sukces, porażka, sposób, silnik, breakpointy**
    * Ambitne Orzeszki
    * "niech uczciwie się toczy gra": dać szansę Zaczęstwiakom, ale usunąć problem magii
    * Zaczęstwiacy wejdą do półfinału (uczciwie)
    * Zaczęstwiacy będą mieli problem z magami i zajmie im chwilę zanim się zrekonstruują
    * działania osobiste - Marlena i Kirył
    * 1k6-1 / 5, skok na 5
    * Tor:
        * 5: -
        * 10: -
        * 15: Usunięcie problemów z energią magiczną.
        * 20: Reputacja Zaczęstwiaków uratowana i wszystko naprawione; usunięcie Yyizdisa
* **Strona, potrzeba, sukces, porażka, sposób, silnik, breakpointy**
    * Terminusi Pustogorscy
    * "problemy z energią magiczną muszą zostać wyczyszczone i rozwiązane"
    * Wszelkie problemy z energią magiczną i Maskaradą zostaną usunięte
    * Informacja nadal będzie w internetach PLUS energia magiczna będzie dalej Skażała
    * działania terminusa - Alan
    * 1k6-1 / 5, skok na 5
    * Tor:
        * 5: -
        * 10: Pięknotka podpięta pod ten teren
        * 15: Maskarada usunięta kosztem reputacji Michała; Pięknotka podpięta pod Zaczęstwiaków
        * 20: Energia magiczna usunięta dużym kosztem Zaczęstwiaków i wpiernicz innym zespołom
    * TRWAŁOŚĆ: 15 lub Maskarada & Wyczyszczenie
* **Strona, potrzeba, sukces, porażka, sposób, silnik, breakpointy**
    * Yyizdis
    * "wszelkie formy oszukiwania muszą zostać ukarane"
    * Zaczęstwiacy zostaną ukarani; nie będą NAPRAWDĘ grali w półfinale a w symulacji
    * Zaczęstwiacy będą grali w prawdziwych półfinałach
    * Yyizdis i "policja internetowa"
    * 1k6-1 / 5, skok na 5
    * Tor:
        * 5: -
        * 10: Zaczęstwiacy nie będą grać w tym turnieju
        * 15: Zaczęstwiacy nie będą grać przez dłuższy czas a Damian po nim w ogóle, corrupted
    * TRWAŁOŚĆ: 10
* **Czemu gracze muszą w to wejść?**
    * To WCIĄŻ kontynuacja tych problemów z Ateną...
* **Trigger?**
    * Z Grubej Rury
* **Okrążenia**
    * Brak. Ich funkcją są tory kierowane przez Graczy.

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) 
* Ż: (element niepasujący) 
* Ż: (przeszłość, kontekst) 

### Dark Future

1. Zaczęstwiacy nie zagrają w półfinałach
2. Terminusi Pustogorscy nieco uszkodzą kohezję Zaczęstwiaków, bo muszą chować anomalie
3. Pięknotka jest uznana za częściowo odpowiedzialną za te problemy

Pogoda, otoczenie:

* Ładna i ciepła pogoda

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

* Wiewiórka przybyła do Zaczęstwa, bo coś bardzo nie było w porządku z grą Ognioterroru. I faktycznie, cheatował... nieświadomie.
* Ognioterrorem interesuje się Yyizdis. Wiewiórka próbuje określić, co należy zrobić, by problem zniknął. Dodatkowo, inny gracz streamował Skażenie.
* Wiewiórka musi zatrzeć ślady zanim Yyizdis się pojawi.

## Misja właściwa

**Scena**: (20:21)

Cyberszkoła. Popołudnie; Pięknotka jedzie tam w odpowiedzi na podłe Skażenie magiczne... nie cieszy jej to.

Odpalony fire alarm. Ewakuacja, wszystko działa. Skażenie dochodzi z jednej sali na pierwszym piętrze. Pięknotka zaczęła wszystkich wyganiach, wbijając się do środka. Nie ma problemu - po prostu dyrektor jest jeszcze w środku. (Tp:7-3-4=SS). Pięknotka znalazła dyrektora Kruszawieckiego - jest częściowo sprzężony ze szkołą (cyborg); kontroluje zraszaczami i wali do jednego z pomieszczeń krzycząc "nie zniszczycie mojej szkoły".

(Tp:9-3-3=S). Pięknotka zauważyła, że w środku ktoś jest. Wiewiórka próbuje opanować sytuację. Ale zanim Pięknotka zdążyła zareagować, do Cyberszkoły zaczął wbiegać terminus Alan. A w środku jest niezarejestrowany mag, co Pięknotki nie cieszy. Pięknotka przygotowuje zaklęcie i wali do środka potężną błyskawicą (magia). (Tp:12-3-2=S). Potężna błyskawica zmiotła wszystko co było w środku, niszcząc Skażenie. Jeszcze zanim Alan dotarł, Pięknotka weszła do środka. Dyrektor obrócił się do niej...

...Alan go ogłuszył. Po krótkim sporze z Pięknotką. ONA prychnęła i weszła pierwsza. Szybko dojrzała przy suficie przerażoną Wiewiórkę z błagalnymi oczami. Zanim Alan wszedł, Pięknotka ładnie poprosiła (słodki uśmiech) Alana o ogarnięcie straży (Ł:7-2-3=S). Alan prychnął, zauważył, że to jej teren i poszedł. Pięknotka ma go z głowy na razie (ale pod ręką jakby był potrzebny).

Pięknotka kazała Wiewiórce zeskoczyć. Ta powiedziała, że spowodowała Skażenie przez próbę badania komputera pod kątem złamania Maskarady przez ludzi. Streaming. Pięknotka zna procedury - da się to wyczyścić. Powinna normalnie to zgłosić. Nie zgłosiła, bo nie chciała by ci ludzie ucierpieli ani normalnie ani reputacyjnie. Dodatkowo Wiewiórka zapytała niewinnie co się stanie jeśli w tej Skażającej zaklęcia szkole pojawi się... Yyizdis?

Pięknotka wezwała wsparcie i zabrała ze sobą Wiewiórkę. Do kwatery terminusów... (Kić: punkt wpływu - nowa postać. Terminus. FORMIDABLE. Katalista i spec od przesłuchań).

* tor_mml: 0 + 2 = 2
* tor_aln: 0 + 4 = 4
* tor_yiz: 0 + 2 = 2

Wpływ:

* Żółw: 5
* Kić: 1 (1)

**Scena**: (20:57)

Kwatera Terminusów. Jedna bardzo nieszczęśliwa Marlenka, przerażająco wyglądający Tymon i przyjazna i empatyczna Pięknotka. Marlena powiedziała całą prawdę - ktoś tu oszukiwał, ona przyszła dowiedzieć się co i jak a to byli ludzie którzy JAKOŚ użyli magii. No i był streaming. No i pojawi się Yyizdis - i ona się boi że coś mu się może stać w tej dziwnej szkole. A w ogóle tu w Zaczęstwie jest wielki sponsor Supreme Missionforce. I ta lokalna drużyna jest niezła. (Ł:8-3-2=S). A tak w ogóle to jeden członek jej gildii jej pomaga.

...gildii?

Marlena powiedziała, że jest kapitanem Ambitnych Orzeszków. I jej kolega z gildii tu jest. Pięknotka poprosiła Tymona, by ten poszedł sprawdzić w Cyberszkole co się dzieje; tylko musi ściągnąć jakiegoś technomantę do pomocy (1 Zasób). No i Pięknotka z Marleną zostały same. Marlena powiedziała, że to bardzo ważne - niech Zaczęstwiacy mogą walczyć w półfinale. Zasłużyli na to. Dlatego trzeba zatrzymać Yyizdisa by nie zabanował Damiana ("Ognioterrora"). Pięknotce aż ręce opadły...

Pięknotka kazała Marlenie się zarejestrować - musi wszystko zapisać i zarejestrować, sama zaczęła się zastanawiać jak się z tego wyplątać - i jak osłonić Yyizdisa.

Pięknotka wzięła pustogorskiego technomantę - terminusa (1 zasób) i postawiła "czerwoną linię policyjną" by odepchnąć Yyizdisa. Nie ma prawa wejść. To powinno pomóc w ewentualnym Skażeniu tej istoty (Tr:9-3-5=S). 3 obrażeń w Yyizdisa. Ma się skontaktować a nie wchodzić z marszu. Yyizdis will comply.

Pięknotka odetchnęła z ulgą po informacji od Tymona - nic się nie stało złego, dyrektora wyleczą a Trzęsawisko jest nieporuszone. Ogólnie, nie doszło do żadnej katastrofy. Poza tym, że Marlena złamała prawo i się nie zarejestrowała, ale to da się naprawić jakąś grzywną ;-). Cóż, trzeba skupić się na ochronie tyłka Pięknotki. Już jest nietragicznie - (1) za Yyizdisa, (1) za działania cyberszkolne.

Marlena zapytana jak by rozwiązała tą sprawę z Maskaradą powiedziała - testy AB, reklama żelków. Zrobiłaby to jako kilka filmików i uploadowała. Pięknotka kazała jej użyć swojego komputera i to zrobić; ona sama się tym zajmie by Michał pamiętał, że uploadował te filmiki i dostał nagrodę za reklamę żelków. I - Pięknotka amendowała - wydał już kasę. Sama nadal jest załamana całym problemem ze streamowanie. To powinno ochronić tyłek Pięknotki (Tp+3:8,3,4=P). Niestety, mimo, że plan Marleny zadziałał, Alan udowodni że proceduralnie niekoniecznie Maskarada jest bezpieczna (+1 tor Alana).

Prawdziwy problem Pięknotki jest podwójny:

* bruździ jej Alan i chce ją pod to podpiąć
* jak ludzie mieli dostęp do magii; nie przyszła z Cyberszkoły a Marlena tego nie wie

Skupmy się wpierw na Alanie - czemu jest w tej sprawie? Co on tu robił? Pięknotka właśnie wpadła na coś genialnego - tak bardzo terminus, nie znalazł Wiewiórki. Nie znalazł cywila. Pięknotka zdecydowała się obrobić Alanowi dupsko: niech będzie bardziej zajęty sobą niż Pięknotką. (Tr+3:10,3,5=SS). W ramach SS (i 5 Wpływu) Pięknotka jest odpowiedzialna za teren Zaczęstwa. (2 Wpływu Kić: i dostanie kogoś do pomocy kto jest technomantą).

Pięknotka jest bardzo nieszczęśliwa z tego powodu; musi się jakoś wyplątać...

* tor_mml: 2 +0 = 2
* tor_aln: 5 +1 = 6
* tor_yiz: 2 +2 = 4
* trwałość_yiz: 7 (10)
* ochrona_tyłka: 5 (10)

Wpływ:

* Żółw: 5 (10)
* Kić: 1 (4)

**Scena**: (21:51)

W ramach dupochronu - Pięknotka wszystko dokumentuje. Wszystko. Dobrze - jak ci ludzie używali magii, jak to się stało? Komunikacja Pięknotka - Tymon. To trzeba rozwiązać.

Przeszli się więc do Zaczęstwiaków i bez większych problemów trafili na Damiana Podpalnika. Damian jest w swoim mieszkaniu i pracuje nad jakąś grafiką. Pięknotka powiedziała, że polecony a Tymon rzucił zaklęcie katalityczne. Gdzie znajduje się Skażenie? Ano, źródło Skażenia jest na Nieużytkach Staszka. Tymon wie dokładnie gdzie.

Na Nieużytkach dotarli do "studni życzeń". Miejsce, gdzie Damian z dziewczyną - Karoliną - składali obietnice itp. To sprawiło, że magia się ich nauczyła. To miejsce jest źródłem Skażenia, które prowadzi do problemów. Ta energia zawsze idzie za Damianem.

Czy da się to rozwiązać / rozpiąć? Oczywiście, że tak. Jeśli uda się ściągnąć Damiana i Karolinę, będzie to stosunkowo łatwe do rozwiązania - będzie sympatia i wszystko.

Co zatem zrobić? Pięknotka poszła do Damiana i dała mu magiczną sugestię - niech on weźmie Karolinę na randkę. Jedna mała magiczna sugestia. Powinno być trywialne, ale ku ogromnemu zaskoczeniu Pięknotki NIE JEST - Karolina nie chciała iść na randkę, bo była zajęta grą w SupMis z Lią. To wymagało tyci bardziej przekonywującego zaklęcia... (Tp:12-3-2=SS). Skonfliktowanie polega na tym, że zarówno Karolina jak i Lia poszły na testową randkę z Damianem - a wyglądają tak rewelacyjnie, że przyciągają spojrzenia i innych zainteresowanych...

Pięknotka ma lekkiego facepalma, ale robi dobrą minę do złej gry.

Na miejscu nieco niespokojny Tymon. Koło studni jest 8 osób. Damian, Lia, Karolina i 5 randomów. Tymon lekko opierniczył Pięknotkę; czas jednak by rzucić zaklęcie odpinające magię od Damiana i Karoliny. (Tp:12-3-2=S). Magiczny sukces Tymona:

1. Karolina zostaje tak piękna jak teraz. Ona po prostu będzie epicko wyglądać.
2. Karolina stanie się naturalnym arcymistrzem gry w Supreme Missionforce, jak Wiewiórka.
3. -3 ochrona tyłka Pięknotki
4. Damian i Karolina są odcięci od studni; ten problem został rozwiązany.

Tymon ma wielkie oczy, Pięknotka lekko chichocze.

* tor_mml: 2 +5
* tor_aln: 6 +0
* tor_yiz: 4 +1
* trwałość_yiz: 7 (10)
* ochrona_tyłka: 2 (10)

Wpływ:

* Żółw: 11 (16)
* Kić: 2 (5)

**Scena**: (22:20)

Z powrotem w kwaterze terminusów. Marlenie udało się zatrzeć ślady po Maskaradzie, Pięknotce - usunąć problem magicznego sprzężenia. Ostatnie co zostało to Yyizdis, który właśnie się do nich odezwał.

Marlena zaczęła protestować przed audytem (przed Pięknotką) - audyt wykaże, że Zaczęstwiacy oszukiwali, ale to nie jest ich wina; to czysta magia. Pięknotka spytała, czy Marlena ma zespół jakiego nie lubi. Marlena zaprotestowała - fair play. Pięknotka poszła sprytem - to wina ALANA. Jego sygnatura magiczna jest tam wszędzie. Zebrała i przedstawiła Yyizdisowi sygnaturę i dowody, również poszlakowe (+1). Użyła też zasobu. (Tr+1:9-3-5=SS).

PLOT TWIST! Alan grał w gry jak nikt nie patrzył; Yyizdath go zabanował. Alan nie wie za co. Będzie się odwoływał i się dowie. Potem będzie winił Pięknotkę, ale nie będzie dało się udowodnić, że to specjalnie. Ona serio nie wiedziała, że on grał...

* tor_mml: 7
* tor_aln: 6
* tor_yiz: 5
* trwałość_yiz: 7 (10)
* ochrona_tyłka: 0 (10)

Wpływ:

* Żółw: 13 (18)
* Kić: 4 (7)

**Epilog**: (22:35)

* Zaczęstwiacy zagrają w półfinale. Nie są w stanie z osłabionym (bez magii) Damianem pokonać Orzeszków, ale to była uczciwa walka.
* Karolina obudziła w sobie miłość do SupMis. Jej uroda sprawiła, że powoli staje się legendarną graczką. Kto wie, może zacznie streamować...
* Pięknotka tymczasowo odpowiada za Zaczęstwo z pomocą Tymona. Nie cieszy jej to. Tymona też nie - nikt nie chce Zaczęstwa.
* Alana zabanowali za niewinność

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |   13    |    18       |
| Kić           |    2    |     7       |

Czyli:

* (K): (2) Taśma ostrzegawcza w internecie. Żaden Yyizdis nigdy nie wejdzie do Cyberszkoły bez zaanonsowania się. A hakerzy są ostrzeżeni.
* (Ż): .

## Streszczenie

Marlena przybyła do Zaczęstwa sprawdzić, czy Zaczęstwiacy oszukują w SupMis. Okazało się, że to ludzie - nie wiedzą o magii i tak, magia im pomaga. Marlena zaczęła próbować im pomóc i wyplątać ich z magii zanim yyizdis zabanuje tą drużynę (konkurencję Marleny). Spowodowała Efekt Skażenia i musiała ją uratować Pięknotka, która zaczęła się zastanawiać jak wplątała się w tą sprawę. Koniec końców się udało - ale Pięknotka dostała w odpowiedzialność zarządzanie Zaczęstwem jako terminuska...

## Progresja

* Pięknotka Diakon: dostała odpowiedzialność za teren Zaczęstwa, co jej wyjątkowo nie pasuje. Tymon jej będzie pomagał.
* Karolina Erenit: utraciła połączenie ze Studnią Życzeń; nie działają w jej okolicy już dziwne magiczne efekty
* Karolina Erenit: ma nadnaturalne umiejętności grania w Supreme Missionforce; do tego jest naprawdę piękną dziewczyną przez zrost mocy Pięknotki i Tymona
* Damian Podpalnik: utracił połączenie ze Studnią Życzeń; nie działają w jego okolicy już dziwne magiczne efekty
* Alan Bartozol: skrycie miłośnik gry 'Unreal Tremor'. Zabanowany przez Yyizdatha i nie wie czemu...
* Michał Krutkiwąs: ma w historii skuteczną reklamę żelków połączoną z patostreamowaniem. Epicko dziwne.
* Pięknotka Diakon: chce zrzucić z siebie Zaczęstwo i dostać jakiś sensowny teren. JAKIKOLWIEK. Ale nie Zaczęstwo - nie jest technomantką ani katalistką.

## Zasługi

* Pięknotka Diakon: knuje, obrabia tyłek Alanowi i trochę wbrew sobie pomaga Marlenie w tych dziwnych grach online, choć zupełnie jej nie rozumie
* Tadeusz Kruszawiecki: dyrektor został tymczasowo zcyborgizowany. Po 3 dniach wróci do normy. Nadal chroni szkołę całym sercem.
* Alan Bartozol: terminus Czerwonych Myszy zajmujący się sprawą Skażenia z ramienia Pustogoru. Dostał za niewinność; no dobra, chciał wrobić Pięknotkę w ten teren.
* Marlena Maja Leszczyńska: próbowała sama rozwiązać problem Skażenia i yyizdisa, ale szczęśliwie dla siebie miała Skażenie i sprawą zajęła się Pięknotka której wyszło.
* Tymon Grubosz: przerażający terminus-katalista o gołębim sercu. Dobry przyjaciel Pięknotki. Miał niefortunnie piękny efekt Skażenia rozrywając Karolinę przy studni.
* Damian Podpalnik: dziewczyna przedkłada grę nad niego, stąd Pięknotka potrzebowała silniejszego czaru by poszła z nim na randkę. Rozerwany ze studnią życzeń.
* Karolina Erenit: rozerwana magicznie ze studnią życzeń, podskoczyła jej uroda drastycznie plus w ciągu miesiąca będzie wybitnym graczem SupMis.

## Frakcje

* Zaczęstwiacy Multivirtu: w sumie, Damian staje się słabym graczem. Ale Karolina staje się graczką wybitną. Zmiana gracza na lewym skrzydle.
* EliSquid Supremus: szefa ich gildii, Alana Bartozola, zabanowano za niewinność

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo:
                                1. Cyberszkoła: miejsce gdzie Marlena miała Skażenie i gdzie zaczęło się wszystko. Niebezpieczne dla Yyizdisa.
                                1. Nieużytki Staszka: znajduje się tam Studnia Życzeń którą Pięknotka i Tymon rozmontowali raz na zawsze.
                                1. Osiedle Ptasie: mieszka tam Damian (i w sumie Karolina); tam doszło do zaczarowania Damiana by wziął na randkę Karolinę.
                            1. Pustogor
                                1. Barbakan: kwatery terminusów, silnie ufortyfikowane. Tam przesłuchiwali Marlenę.

## Czas

* Opóźnienie: 1
* Dni: 2

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

Start z Grubej Rury 2

## Wykorzystana mechanika

1810
