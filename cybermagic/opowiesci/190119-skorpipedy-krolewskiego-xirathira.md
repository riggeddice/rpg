## Metadane

* title: "Skorpipedy królewskiego xirathira"
* threads: olga-regentka-czarnopalca, wojna-o-dusze-szczelinca, alan-opiekun-ucisnionych
* motives: subtelna-ingerencja-maga, wyscig-zbrojen, ukryty-aniol-stroz
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [190116 - Wypalenie Saitaera z Trzęsawiska](190116-wypalenie-saitaera-z-trzesawiska)

### Chronologiczna

* [190116 - Wypalenie Saitaera z Trzęsawiska](190116-wypalenie-saitaera-z-trzesawiska)

## Projektowanie sesji

### Pytania sesji

* 

### Struktura sesji: Frakcje

* PodŻarnia: Taunter, Adapter, Ośmieszacz
* Czerwone Myszy: Eskalacja
* Adela: coraz większa desperacja
* Heroine: Zniszczenie Myszy

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. ...

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (18:07)

Pięć dni później, Pięknotka wyszła ze szpitala, sprawna jak zawsze. Acz z coraz nowszymi doświadczeniami - lekarz powiedział jej, że tak zaawansowana transformacja w tak krótkim czasie nie powinna być możliwa. Erwin odprowadził ją do domu. I Erwin ma Nutkę! Odbudował ją z kopii zapasowej; oryginalna Nutka nie była możliwa do odzyskania. Pięknotka wie, że Saitaer by ją oddał - ale prosić bogów o łaskę..?

Pięknotka dowiedziała się, że próbował się z nią skontaktować Waldemar Mózg. Dyskretnie. Ale Erwin go wywalił - Pięknotka nie jest w stanie. Erwin chroni Pięknotkę - ale ona chce się dowiedzieć o co chodziło. Nie chciał żadnego innego terminusa - tylko ją.

Pięknotka zatem skontaktowała się z Waldemarem. O co chodzi.

Mózg przyszedł do Pięknotki. Nie wygląda już tak bogato jak kiedyś. I wygląda na zmęczonego i nieszczęśliwego. Powiedział, że Myszy się rozpadają i on próbuje trzymać co się da. Niestety, pod Żarnią pojawiła się grupa toksycznych skorpipedów. Adela próbowała je zatruć, ale bez większego powodzenia - pogorszyła tylko sprawę! (to Pięknotkę zdziwiło). Kolejna dziwiąca Pięknotkę rzecz to to, że skorpipedy ujawniają się nocą ludziom - i ludzie się stresują, nie chcą chodzić w niektórych miejscach itp. Ogólnie, poziom traumy i stresu wzrósł wśród ludzi. Pryzmat stresu i ludzi rośnie u ludzi na ich terenie...

Waldemar powiedział, że boi się, że to przez Adelę. Nie planuje się jej pozbywać - chyba, że będzie musiał zapłacić sporą karę przez nią. Ale boi się, że sprawa ze skorpipedami eskaluje. One mogą coś osłaniać tam pod ziemią...

Pięknotka powiedziała, że spojrzy na to. Może coś jej się uda zrobić. Waldemar zaprosił ją do centrum dowodzenia Myszy. Bunkier Dowodzenia w Podwiercie. Kiedyś centralna baza Pięknotki.

_Podwiert, Bunkier Rezydenta_

Pięknotka sprawdza w War Roomie zestawienia informacji itp. Jest bezpośrednia implikacja - parę dni po próbach otrucia skorpipedów przez Adelę one zaczęły pryzmatycznie wpływać na ludzi. No i widać wyraźnie, że Myszy mają coś do Olgi - albo Olga do Myszy. Zgodnie ze wzorem wszystko wskazuje na to, że te skorpipedy działają INACZEJ niż zwykle - jakby się żywiły Pryzmatem Paranoi. Nie robią nikomu szczególnej krzywdy, za jednym wyjątkiem - mag Myszy niedaleko Czarnopalca.

Pięknotka zadała proste pytanie - skąd wiemy, że jest JEDNO gniazdo? To może być kilka. Waldemar się przeraził. Pięknotka pojechała spotkać się z zaatakowanym magiem Myszy.

_Pustogor, Szpital Terminuski_

Sławomir Muczarek w szpitalu. Nie wygląda jak ofiara skorpipeda. Skorpipedy się często nie powstrzymują. Pięknotka chce od niego wiedzieć co on tam robił i co się stało - widzi, że jego odpowiedzi są niekoherentne i coś mu wlazło na pamięć. (Tp+3: 12,3,2=S). Powiedział, o co chodziło. Był tam by wraz z innymi magami Myszy (na zmianę) doprowadzić do tego, by Olga jednak chciała współfinansować ochronę okolicy. Jedną z metod było podburzanie ludzi. Żeby oni mogli potem uratować sprawę i pokazać Oldze co mogą zrobić. Ale coś poszło cholernie nie tak - chyba Olga straciła kontrolę? Potem był ten skorpiped...

Pięknotka poszła negocjować użycie neuronauty z Lucjuszem Blakenbauerem... ten ma domyślne podejście wrogie. Gdy Pięknotka powiedziała o pamięci, Lucjusz zauważył, że pacjent dostał silną dozę środków torturujących i halucynogennych. Skorpiped terroru? Nie wie, czy taki wariant istnieje. Tak czy inaczej, Lucjusz się zgodził na wsparcie neuronauty.

(TrZ:10,3,5=S). Pięknotka dostała Prawdę. Taką, jaką pamięta Sławomir. Pamiętał, że Olga obróciła się przeciwko tym ludziom. Sławomir miał już pomóc, ale wtedy został zaatakowany. Widział - wyraźnie widział - dwie istoty. Skorpiped, który go zranił. A drugą istotą był Królewski Xirathir. Wiktor Satarail. I Sławomir widział go tylko z oddali. Gdyby nie neuronauta, NIGDY by nie dało się zobaczyć tego Xirathira.

W czym Myszy nic nie zgłosiły. Nikt nic nie zgłosił.

Pięknotka ma teraz do myślenia...

_Pustogor, Barbakan_

Pięknotka jedzie po bibliotece z pomocą bibliotekarzy. Wszystko o skorpipedach i środkach o skorpipedach. Też o xirathirach. Próbuje to scrossować z danymi o środkach które otrzymał Muczarek. To jest Pięknotkowa podstawa - co będzie jej potrzebne później. Nie szuka czego konkretnego, próbuje zrozumieć. Znalazła coś ciekawego - młody Wiktor Satarail opracowywał formę zaawansowanego xirathira. To on stał za projektem "xirathir Królewski". To on zbudował tą formę i to serum zanim stał się magiem rodu Satarail. Ważna rzecz - królewski xirathir wymaga hosta który jest magiem. Czyli Pięknotka może wykluczyć jakieś viciniusy z bagien. To raczej on sam.

_Podwiert, Bunkier Rezydenta_

Pięknotka rozmawia z naburmuszoną Adelą. Adela dała Pięknotce próbkę skorpipeda jaką ma od Alana - to typowa próbka skorpipeda. Powiedziała Alanowi, że chce kombinować z perfumami i ładnie poprosiła, to jej dostarczył - Alan nie wie, że Myszy mają problem ze skorpipedami bo nie jest już magiem Myszy (nie zaskakuje Pięknotki). Niestety, zdaniem Pięknotki, Adela zawaliła - nie dostała dokładnej informacji o skorpipedach PONOWNIE i PONOWNIE mogło coś się stać. Ale żaden z oryginalnych wzorów skorpipedów o jakich czytała nie wskazuje na to, że mogło się "pogorszyć", najwyżej "nie zadziałać".

Z drugiej strony obecność Wiktora Sataraila mogła sprawić, że on zbuduje kontrę. Ale po co? Czemu Wiktor miałby tępić ADELĘ?

Pięknotka uświadamia Adelę, że jej działania najpewniej coś pogorszyły. Przez to, że nie patrzyła, najpewniej naraża niewinnych ludzi i magów. Niech ona jest jeszcze bardzej wytrącona z równowagi i pewności siebie (Tp+3:9,3,3=S). Adela jest naprawdę niepewna siebie i boi się robić coś poza standardowymi tematami powiązanymi z urodą.

Jeszcze jedno spotkanie z Waldemarem Mózgiem - na osobności powiedziała mu, że pójdzie porozmawiać z Olgą. Coś musi jej obiecać. Zakaz - absolutny zakaz - harrassowania Olgi. Niech ta czarodziejka żyje sama na uboczu, niech nikt się do niej nie zbliża, niech ma spokój. A jeśli będą ochraniać, to będzie pomagać. (Tr+1:8,3,6=P). Waldemar obiecał, przy całej swojej wierze że to się stanie. (ale to się nie stanie, P).

**Scena**: (19:48)

_Czarnopalec, Pusta Wieś_

Pięknotka zapowiedziała się przed przyjściem. Została zaproszona. I faktycznie, Olga czeka w Pustej Wsi. Jest zestresowana i zmęczona. Powiedziała, że ostatnio na szczęście mniej ludzi się pojawia. Pięknotka powiedziała, że interesuje ją sprawa skorpipedów. Olga powiedziała, że może pomóc. Pięknotka wyjaśnia POWÓD przybycia - chce rozwiązać problem skorpipedów. Ale wpierw chce się dowiedzieć - to nie Olga stoi za skorpipedami?

Olga próbuje nie powiedzieć prawdy (Tp+1:10,3,3=S). Oczywiście, nie miała szans. Powiedziała, że przyszedł do niej Wiktor Satarail. Powiedział, że chce jej pomóc i pomóc jej viciniusom. Na początku płakała ze strachu, by jej nie zabijał - ale Wiktor był eleganckim dżentelmenem. Pomógł jej i zagnieździł skorpipedy. Powiedział, jak je odgnieździć. Powiedział też, że jeśli ona komukolwiek powie lub spróbuje go zatrzymać, to on zamorduje wszystkich którzy spróbują skrzywdzić jej malutkie księstewko.

Pięknotka jeszcze wróciła do Lucjusza Blakenbauera i pobrała jakieś leki, coś, co może Wiktorowi pomóc i uśmierzyć jego ból. Zostawiła to u Olgi - niech Wiktor sobie to odbierze. (TrZ+1:11,3,5=S). Relacje nie są ciepłe, ale nie są już wrogie.

Wpływ:

* Ż: 9
* K: 2

Skuteczne Akcje: .

**Epilog** (20:11)

* Poproszona Olga oczywiście "wykorzystała swoją wiedzę" i pozbyła się skorpipedów.
* Przez pewien czas żadna Mysz nie ruszyła ogonkiem by robić coś z Olgą.
* Wszyscy magowie wydobrzeli i nikomu nie stała się żadna trwała krzywda.
* Wiktor dalej chodzi na herbatkę do Olgi.
* Pięknotka wróciła do siebie z głupkowatym uśmiechem - Olga x Wiktor.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |    9    |      9      |
| Kić           |    4    |      4      |

Czyli:

* (K): .
* (Ż): .

## Streszczenie

Czerwone Myszy poprosiły Pięknotkę o pomoc - pojawiły się na ich terenie niebezpieczne skorpipedy i działania Adeli jedynie pogarszają sprawę. Okazało się, że to działania Wiktora Sataraila, który chce pomóc Oldze Myszeczce w utrzymaniu terenu i odparciu harrasserów z Myszy. Pięknotka odkręciła sprawę i trochę poprawiła relację z Wiktorem. Acz jest sceptyczna wobec samych Myszy...

## Progresja

* Pięknotka Diakon: poprawiła swoje relacje z Wiktorem Satarailem - nie jakoś bardzo, ale wrócili do neutralnego chłodnego.
* Olga Myszeczka: Wiktor Satarail jej pomógł w odparciu harrasserów z Czerwonych Myszy. Wiktor jest jej cichym sojusznikiem, kimś, kto jej pomaga.
* Adela Kirys: jej poczucie własnej wartości i umiejętności biomantycznych jest już naprawdę niskie.
* Wiktor Satarail: chce pomóc Oldze w jej świecie, z jej viciniusami i jej problemami. Niech Olga ma spokojne, fajne życie.
* Alan Bartozol: pomógł trochę Adeli Kirys z sympatii. Opuścił Czerwone Myszy. Nie mają nic dla niego, lepiej mu żyć samotnie.

## Zasługi

* Pięknotka Diakon: zdecydowała się uratować Myszy przed nimi samymi - odkryła, że Olga przyjaźni się z Wiktorem i że Wiktor zdecydował się skorpipedami zrobić problemy harrassującym Olgę Myszom.
* Waldemar Mózg: przejął kontrolę nad rozpadającymi się Czerwonymi Myszami i próbuje je utrzymać w dobrym stanie. Poprosił Pięknotkę o rozwiązanie problemu ze skorpipedami.
* Sławomir Muczarek: troszkę atakował Olgę zwykłymi ludźmi; za to zmodyfikowany skorpiped zrobił mu poważną ranę i dopiero Wiktor Satarail zatrzymał skorpipeda przed egzekucją Sławka.
* Wiktor Satarail: zdecydował się pomóc Oldze (chyba z nią flirtuje). Stworzył dziwne skorpipedy i odwiedza Olgę regularnie w jej Pustej Wsi. Chce jej pomóc.
* Adela Kirys: lekko nadwyrężyła zaufanie Alana i poprosiła o próbkę skorpipeda by zrobić przeciw niemu truciznę. Niestety, Wiktor jest o kilka klas lepszy od Adeli.
* Olga Myszeczka: nie radzi sobie z magami i ludźmi którzy non stop rozbijają jej spokojną Pustą Wieś. Powiedziała całą prawdę Pięknotce - o wsparciu Wiktora Sataraila.

## Frakcje

* Czerwone Myszy: straciły Alana Bartozola jako agenta i sojusznika. Waldemar Mózg utrzymuje co jest w stanie, ale jego subfrakcja gaśnie.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Żarnia
                                1. Żernia: pod którą pojawiła się wylęgarnia dziwnych toksycznych skorpipedów, których nie dało się usunąć (dopiero Olga dała radę)
                            1. Podwiert
                                1. Bunkier Rezydenta: centrum dowodzenia maga rezydenta Trójkąta; pod kontrolą Czerwonych Myszy. Nic nie zmienili...
                            1. Czarnopalec
                                1. Pusta Wieś: gdzie Pięknotka dowiedziała się prawdy o skorpipedach od Olgi oraz którą czasami odwiedza Wiktor Satarail

## Czas

* Opóźnienie: 5
* Dni: 1

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* nic

## Wykorzystana mechanika

1811
