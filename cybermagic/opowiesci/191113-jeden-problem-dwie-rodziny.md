## Metadane

* title: "Jeden problem, dwie rodziny"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [191108 - Ukojenie Aleksandrii](191108-ukojenie-aleksandrii)

### Chronologiczna

* [191108 - Ukojenie Aleksandrii](191108-ukojenie-aleksandrii)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

brak

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

Dominujące uczucie:

* "Miałeś cierpieć tak, jak ja cierpiałem"
* "Krwawa Aleksandria zniszczyła moje życie, ja zniszczę całość Twojego świata"

Pytania:

* "Kto destabilizuje Kruczaniec?"
* "Jak daleko dojdzie do skażenia Kruczańca?"

## Misja właściwa

### Prolog

Trzy tygodnie temu.

Potwór zbliżył się do Mordowni, w Upiornym Lesie. Tam jest impreza, ścigaczowcy się dobrze bawią. Potwór jednak nie jest tam dla nich - jest tam dla Sabiny. Ona ma ostatni brakujący element do jego ewolucji - adaptogen kralotyczny. Potwór wysłał sugestię do jednego z chłopaków na zewnątrz, że zbliża się rywalizujący gang z Wroniewa. Ścigaczowcy wypadli na zewnątrz, kobiety zostały w środku a potwór w płaszczu iluzji wszedł do środka pomieszczenia.

Bez problemu skierował się do kobiecej toalety, gdzie Sabina właśnie oddaje swój posiłek po nadmiarze alkoholu, wspierana przez Anastazję Gęś - lokalną sprzedawczynię w fabryce mebli. Jak dziewczyny zobaczyły potwora, przeraziły się. Potwór zażądał od Sabiny adaptogen - i go otrzymał. Sabina nie mogła uwierzyć, "że to się stało". Potwór z uśmiechem opuścił teren, nie krzywdząc żadnej z nich. Nie miał po co - jego celem jest coś zupełnie innego...

### Sesja właściwa

Trzy tygodnie później.

W zupełnie innym miejscu, Kamil Lemurczak jest niezadowolony. Na teren Kruczańca przypałętał mu się reporter i zaczyna węszyć. Niech oni coś zrobią i rozwiążą problem z tym reporterem. Mówiąc "oni" ma oczywiście na myśli zaufanego Karola Kszatniaka (katai, manipulator) i niegodną uwagi Teresę Marszalnik. Na miejscu jest jakaś "Kornwalia" (Amelia) i ona wkupiła się w łaski Kamila przez faktyczne robienie modelu biznesowego na tym terenie który działa - ściąganie turystów, maksymalizowanie kiczu... ogólnie, niech to "gotyckie" miasteczko faktycznie będzie gotyckie.

Kamil powiedział Karolowi i Teresie, że "Kornwalia" jest zaufana. Ale ma ich słuchać. I na miejscu jest też pisarz (Leszek) i on też jest zaufany. Niech to rozwiążą, od czegoś są...

Teresa nie cierpi Kamila, Karol i Kamil sobie ufają. Jednak nikt nie jest w stanie powiedzieć "nie" Kamilowi Lemurczakowi - nie na tym terenie, nie jeśli biorą od niego pieniądze (lub nie zabiera im rzeczy). Tak więc Zespół poszedł i szybko się zreintegrował - Kamil, Amelia i Teresa.

W Kruczańcu, pierwsze czym zajął się Karol to poszukiwanie informacji na temat tego reportera. Ów reporter porusza się po nizinach społecznych zamiast przyjść do zamku pogadać z pisarzem jak człowiek, więc Karol niechętnie musiał iść gadać z nizinami społecznymi. I tam:

* dowiedział się o tym, że jest jakiś potwór w okolicy
* dowiedział się, że osobą która na ten temat najwięcej wie jest niejaka Anastazja Gęś z fabryki mebli
* niestety, reporter dowiedział się o obecności Karola; i reporter szuka informacji o potworze
* dowiedział się, że coś jakoś arystokratka w okolicy. Jakieś 3 tygodnie temu.

Arystokratka? Amelia sięgnęła pamięcią wstecz. Pamięta sprawę - to była niejaka Sabina Kazitan, degeneratka jakich mało; przyjechała się "nawalić" i miała ze sobą robota bojowego klasy Serratus. Ale od pewnego momentu nie miała ze sobą już Serratusa; odesłała go (co dla Amelii było dziwne). Amelia i Sabina nie wchodziły w interakcję; Sabina płaciła i sama stanowiła atrakcję turystyczną a Amelia nie widziała przeszkód by odseparować Sabinę od jej pieniędzy.

"Sabina" - to dla odmianie podpowiedziało coś Teresie. Teresa sięgnęła po informacje do swoich przyjaciółek i "ale nikomu nie mów" arystokratów. Wyciągnęła kilka zacnych plotek:

* Sabina to ulubiona zabawka Kamila Lemurczaka. Lemurczak uwielbia doprowadzać Sabinę do płaczu i ją upokarzać, zwłaszcza publicznie.
* Sabina swego czasu wpadała w taką desperację, że aż szukała jakichś rytuałów Krwi, ale nigdy nie miała dość odwagi i szaleństwa by za tym pójść dalej.
* Sabina ma tendencje do narkotyzowania się i znieczulania się od życia, zwłaszcza po dłuższych kontaktach z Lemurczakiem.
* Sabina nie tak dawno kombinowała, jak ściągnąć adaptogen kralotyczny z Cieniaszczytu. Podobno jej się udało.

Niepokojące. Mając te informacje, Amelia wykorzystała swój zaawansowany system - po przejęciu kontroli nad tym terenem, wraz z Zenek & Zenek wymieniła wszystkie kolektory taumiczne na odpowiednie, Zenkowe, i podobudowywała do nich detektory przekazujące informacje do niej, do jej systemów detekcyjnych znajdujących się w zamku Leszka. Połączyła informacje z wielu kolektorów i zaczęła zbierać do kupy dalsze informacje:

* Potwór powstał z połączenia trzech energii - Magii Krwi, Esuriit i Mentalnej. NIEFORTUNNE!
* Potwór powstał jakieś 2-3 dni przed tym, jak wszedł w kontakt z Sabiną i Anastazją. Powstał w Fabryce Mebli; tam zginęła jakaś osoba.
* Zabicie tej osoby nie było powiązane z samym głodem i nienawiścią. To były inne uczucia - ból, rozpacz, głód, miłość, poczucie straty.

Tak więc Amelia doszła do niepokojącego wniosku - potwór powstał przez zabicie osoby sobie bliskiej. To jest czyn miłości, nie nienawiści. To jest groźniejsze i bardziej nieprzewidywalne niż się wydaje. Nie do końca wiadomo czego potwór chce, ale jest on straszniejszy niż się wydaje. No i najpewniej ma to jakieś powiązania z Sabiną.

Tymczasem, w fabryce mebli... Karol dyskutuje z Anastazją Gęś i zdobywa dalsze informacje. Poza zdobyciem wszystkiego co Anastazję widziała (ze sceny zero), Anastazja dorzuciła jeszcze kilka cennych informacji po odpowiednim dopytaniu przez Karola:

* Sabina przekazała potworowi adaptogen kralotyczny. Wie, bo Sabina i potwór użyli tego terminu.
* Sabina WIEDZIAŁA, kim jest potwór. Nie spodziewała się tego zupełnie, ale zupełnie nie była zaskoczona.
* Tydzień przed pojawieniem się potwora, Sabina była w przychodni z jakimś chłopakiem od ścigacza. Nie była trzeźwa. A potem odesłała swojego Serratusa. I od tej pory piła mocniej, i zawsze na smutno.

Czyli najpewniej Sabina coś zbroiła. Nic dziwnego, zdaniem Karola. Sabina to jest degeneratka... i wtedy usłyszeli straszliwy krzyk agonii połączony z gulgotaniem. Karol błyskawicznie kazał Anastazji uciekać i sam schronił się za jedną z maszyn. Co się stało: potwór złapał jednego z facetów w fabryce i w okrutny sposób wsadził go w maszynę. Delikwent powinien nie żyć, ale Esuriit trzyma go przy życiu i maksymalizuje jego cierpienie. Ludzie widzący to czują Grozę; potwór się tym karmi i wzmacnia.

Karol natychmiast włączył spryskiwacze przeciwpożarowe. "Czar prysnął" i ludzie zaczęli uciekać; Karol nadał sygnał SOS. Co na to Teresa i Amelia? Zaczęły mu kibicować. Ale terminus Paweł rzucił się na pomoc.

Karol nie jest szczęśliwy, ale musi jakoś... coś... na szczęście potwór nie jest zainteresowany walką z Karolem. Karol zapytał potwora o co chodzi. Potwór, o dziwo, odpowiedział. Zabrali jego rodzinę. Ci z zamku zabrali jego rodzinę. I oni zapłacą. Zostaną zniszczeni.

Aha. To gorzej, bo "ci z zamku" to między innymi też Karol (ale potwór o tym nie wie). A już na pewno Amelia. Szczęśliwie na to wpadł Paweł. Terminus kupił czas Karolowi by ten się wycofał, ale sam terminus nie miał siły pokonać potwora. Potwór uciekł, dalej karmić się grozą i destrukcją.

Mając wszystkie informacje, zespół poskładał historię do kupy używając rejestru ludności, danych z aury, krwi i tożsamości zabitego gościa w fabryce mebli (Konrad, taki "gość od brudnej roboty" dla Leszka).

* w przychodni był lekarz, Adam. On miał chorego syna, ale dało się go wyleczyć. Acz cierpiał chroniczny ból.
* na początku swojej kariery Leszek nie do końca czuł potrzeby Aleksandrii. MUSIAŁ ją nakarmić. Użył owego syna (Romana).
* Adam z żoną, Luizą, szukali dziecka. Luiza poszła do prasy.
* Pojawili się "faceci od brudnej roboty". Porwali Luizę, odpowiednie narkotyki, szpital psychiatryczny. Zniszczyli jej mózg.
* Luiza w szpitalu psychiatrycznym
* Adam odzyskał "kontrolę prawną" nad Luizą, opiekuje się nią w domu
* Adam spotyka się z Sabiną. Ona widzi kogoś kto cierpi, chlapnęła współczuciem, Adam pytał ją jako czarodziejkę. Wygadała się z czymś.
* Po tym wygadaniu, Sabina odesłała swojego Serratusa, by na pewno nikt się nie dowiedział co zrobiła.
* Adam użył rytuału krwi i stał się potworem. Esuriit ZAWSZE wygrywa; on już jest martwy (ale jeszcze jest echo jego osobowości).
* Sabina dopakowała go adaptogenem.
* Potem Sabina chwilę była w okolicy by się upewnić, że nic złego się nie dzieje, potem odjechała.

Innymi słowy, ten sam problem co miał Leszek. Leszek stracił Klarę - pozyskał Aleksandrię. Adam stracił Romana i Luizę - nie miał co pozyskać, stał się potworem...

Adam jest GROŹNY. Praktycznie nie do ruszenia przez ich zespół. Co gorsza, może odpiąć Pawła od Aleksandrii i zrobić zeń drugą istotę Esuriit... tak więc, najsensowniejszym wyjściem byłoby wezwanie terminusów. Ale nie mogą tego zrobić, bo... jest Aleksandria pod ziemią. I mają bardzo przechlapane...

Co ta cholerna Sabina zrobiła? Diabli wiedzą - trzeba dowiedzieć się od niej. I tu do akcji weszła Teresa.

Teresa trochę zna Sabinę. Nie są przyjaciółkami - Sabina Kazitan nie ma przyjaciół, jest najbardziej wartą Kamila Lemurczaka osobą jaka jest - ale powinno dać się coś z tym wszystkim zrobić. Teresa porozmiawiała z Sabiną po hipernecie i powiedziała jej, że jest źle. Potrzebna jest pomoc Sabiny. Ta oczywiście zachowała plausible deniability, ale powiedziała jak to rozmontować - jako, że Adam faktycznie użył rytuału Krwi, to jest w tym rytuale ukryte powiązanie. Tether.

To sprawia, że są trzy sposoby na usunięcie tego potwora - zniszczenie Aleksandrii (Sabina nie wie o Aleksandrii), poświęcenie Romana w tak samo krwawym rytuale jak ten który zabił Luizę, lub by potwór zabił Romana. Teresa nalegała na obecność Sabiny na miejscu; arystokratka niechętnie, ale się zgodziła. W końcu to przez nią...

Nadszedł czas na kolację. Teresa, Amelia i rodzina Leszka usiedli do kolacji; Karol się spóźnia. I na to nagle Klara zaczyna mówić "Tatusiu, jestem głodna". Nieumarła Klara Skaziła sie energią Esuriit - potwór rośnie w siłę i chce wszystko zabrać Leszkowi. Leszek nic nie postrzega; Aleksandria wpływa na jego detekcję i jego umysł. Amelia błyskawicznie wykonuje zamach patelnią - strąciła połowę zastawy i przewróciła samego pisarza. Teresa krzyknęła przeszywająco. Jej ból i strach w głosie naprowadził na nią Klarę; przerażona Teresa wyjęła broń dostarczoną jej kiedyś przez Białego Rycerza i strzeliła. Broń - mentalna - dokarmiła upiora. Klara zaczęła się karmić strachem i koszmarami z działka mentalnego...

...ale to kupiło Amelii czas na kontakt z Aleksandrią i wysłanie do niej sygnału "Klara cierpi". Aleksandria zrobiła "atak troskliwych misiów" i wypaliła Esuriit z Klary. Sytuacja wróciła do bezpiecznego stanu... ale trzeba coś z tym zrobić.

Coś.

Teresa i Amelia zaczęły konspirować. Praprzyczyną problemu jest Kamil Lemurczak. Jeśli udało się zdominować terminusa Pawła, to może da się zdominować też Kamila Lemurczaka..? Pokrzepione tą myślą, dziewczyny zabrały się do pracy.

* Teresa zauważyła przy Karolu, że to wszystko wina Sabiny i Kamil chciałby obejrzeć jak ona się płaszczy po tym wszystkim.
* Karol wezwał Kamila; powiedział mu, że przecież to okazja obejrzenia jak Sabina spieprzyła i teraz się nie wywinie. Kamil to łyknął. Weźmie nawet kamerę.
* Teresa jednocześnie urabiała Sabinę, która nie ma pojęcia o Lemurczaku. Sabina ma pomóc pokonać potwora. Zgodziła się, niechętnie (ale z perspektywy potwora ONA będzie celem).
* No i Amelia doprowadziła do wyjęcia Romana z Aleksandrii, kosztem pewnej manipulacji tego poczciwego urządzenia.

Idą w plan "niech potwór zabije swojego syna". Najmniej okrutny dla ich psychik.

Tak więc w dzień zero, Amelia i Karol zmontowali pułapkę w opuszczonym Supermarkecie i cały zespół tam się udał wraz z Romanem, wysyłając sygnął tak, by potwór to poczuł. Tam jest jego syn. Zanim potwór się zmaterializował, pojawił się Lemurczak i opierniczył Sabinę. Powiedział, że teraz się nie wywinie, że zabierze jej rodzinie wszystko, że ona nie ma nic. Takie rytuały ludziom. Sabina jest w absolutnej desperacji, ale jeszcze nie błaga. Ma inny plan...

Gdy pojawił się potwór, ruszył w kierunku na Sabinę a nie zamaskowanego usmacznionego Romana. A Sabina nie walczy - czarodziejka po prostu zamknęła oczy i czekała na śmierć. Karol do tego nie dopuści - szybko wstrzyknął sobie adrenalinę i narkotyki bojowe, po czym skorzystał z pędu potwora Esuriit i wepchnął biednego Romana pod ogień. Ale sam kontakt z istotą Esuriit Karola osłabił; gdy potwór zabił swojego syna i padł na ziemię jako zgniłe mięso, Karol też padł. Narkotyki plus Esuriit - za dużo.

Przez to nikt się nie zorientował, że Teresa i Amelia z pomocą terminusa Pawła wsadziły Kamila Lemurczaka do Aleksandrii by zrobić z niego niewolnika Aleksandrii - a dokładniej, to Amelii. Teresie pasuje a Sabina nic nie wie. Ale żyje.

Tor: 14

* (05) - Adam niszczy Konrada.
* (10) - Częściowa manifestacja Esuriit; Klara oraz Emil.
* (15) - "Korozja ludzkich ciał"
* (20) - Pełna manifestacja Esuriit
* (25) - Wojna domowa

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

By zasilić Aleksandrię, czasami trzeba było kogoś zdigitalizować. Zabrano ciężko chorego syna lekarza. Gdy żona lekarza udała się do prasy, wsadzili ją do szpitala psychiatrycznego i narkotykami zniszczyli jej mózg. Lekarz, wspierany przez Sabinę (która chlapnęła pijana) stał się potworem Krwi i Esuriit. Zespół - Amelia i arystokraci - zniszczyli potwora, ale korzystając z okazji sprzęgli Kamila Lemurczaka z Aleksandrią.

## Progresja

* Amelia Mirzant: największy zwycięzca. Kontroluje Kamila Lemurczaka, pozbyła się zagrożenia na tym terenie i jeszcze nikt nie wie o jej Aleksandrii.
* Teresa Marszalnik: uwolniona od Kamila Lemurczaka. To, że dalej ma władzę nic nie zmienia - Aleksandria nie zrobi jej krzywdy.
* Kamil Lemurczak: zniewolony przez Aleksandrię, stał się wiecznie szczęśliwym agentem Amelii Mirzant
* Sabina Kazitan: przeżyła. Jej zrozumienie Krwawych Rytuałów i Esuriit okazało się naprawdę duże i dość subtelne, co jest niepokojące.

### Frakcji

* .

## Zasługi

* Amelia Mirzant: katalistka, która przejęła kontrolę nad całym terenem i biznesowo wszystkim steruje. Dogaduje się z Aleksandrią, nie jest jej częścią. Pośrednio steruje Lemurczakiem.
* Karol Kszatniak: lojalny Lemurczakowi, katai i manipulator. Skutecznie dawał odpór Potworowi i doprowadził do jego śmierci, zbierał też informacje o wszystkim w okolicy.
* Teresa Marszalnik: manipulatorka najwyższej klasy. To ona doprowadziła do tego, że Sabina przybyła, Karol ściągnął Lemurczaka a Amelia chciała Lemurczaka uwięzić Aleksandrią.
* Leszek Baszcz: całkowicie nieświadomy rzeczywistości; żyje w swoim świecie, świecie Aleksandrii. Prawie pożarła go nieumarła córka Dotknięta przez Esuriit a on nic nie wiedział.
* Klara Baszcz: nieumarła; przejęta przez moc Esuriit prawie pożarła ojca - zatrzymały ją Amelia i Teresa i finalnie wypaliło się z niej wszystko Aleksandrią.
* Paweł Kukułnik: stoczył kilka bitew z Potworem. Szczególnie wrażliwy na energię Esuriit; czyżby była sprzeczna z technologią Kuratorów?
* Kamil Lemurczak: przybył na wezwanie swojego oddanego Karola znęcać się nad Sabiną Kazitan. Prawie mu się udało - tyle, że skończył w Aleksandrii.
* Sabina Kazitan: degeneratka, która wpierw wypaplała o krwawym rytuale człowiekowi a potem wróciła by pomóc. Panicznie się boi Kamila Lemurczaka i go nienawidzi do poziomu, że wybiera śmierć. Ma ogromną wiedzę o rytuałach, magii krwi i rzeczach bardzo ezoterycznych i niebezpiecznych.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Lemurski
                            1. Kruczaniec
                                1. Zamek Pisarza: starcie Klary-Esuriit z ojcem, Amelią i Teresą; też miejsce, gdzie Zespół spokojnie mieszkał jako namiestnicy Lemurczaka.
                                1. Upiorny Las
                                    1. Mordownia: niegodne miejsce picia i zabawy dla młodzieży, nie tylko lokalnej. Potwór tam pozyskał adaptogen kralotyczny od pijanej Sabiny.
                                1. Supermarket Złotko: miejsce pułapki na Potwora przez Zespół. Tam Sabina prawie zginęła, gdyby nie uratował jej Karol.
                                1. Fabryka Mebli Larmat: miejsce krwawego rytuału w którym powstał Potwór i miejsce, gdzie Potwór wzbudział grozę wśród ludzi.

## Czas

* Opóźnienie: 72
* Dni: 5
