## Metadane

* title: "Romans dzięki Esuriit"
* threads: legenda-arianny
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [211027 - Rzieza niszczy Infernię](211027-rzieza-niszczy-infernie)

### Chronologiczna

* [211027 - Rzieza niszczy Infernię](211027-rzieza-niszczy-infernie)

## Plan sesji
### Co się wydarzyło

* Załoga Inferni
    * Elena: jej Korupcja jest strasznie silna. Unika wszystkich. Pożera ją sumienie. Unika Eustachego i czarowania. Ucieka w formalizmy i do swojej kajuty. Nie mówi ani słowa - lub atakuje. "I am the monster".
    * Maria: przybywa na Infernię z prośby Martyna (dead man's hand --> Klaudia). Broni honoru Martyna jak potrafi.
    * Leona: wypisała się ze szpitala. Słaba jak cholera, ale sprawna. Nie pozwoli, by jej statek umarł "przez Eternię".
    * Verlenowie: trzymają się silnie. 1 komandos + Otto. Otto wymusza twarde podejście na pokładzie.
    * Kamil: zapalił fanatyzm części ludzi jeszcze silniej.
    * Młodsza część załogi Inferni którzy nie uciekli w religię: morale złamane.
    * Noktianie: chcą doprowadzić Tivr (Aries Tal) do dawnej wielkości. Widzą to jako szansę na SWOJĄ jednostkę pod dowództwem Arianny. Morale podniesione. Straty... cóż, noktianie przywykli.
* Co się wydarzy
    * Nikt nie zauważył zniknięcia Flawii. Nobody cared. Za to Martyna odwiedzają eternianie.
    * Elena znika w czeluściach K1. Nie pojawia się, nie odzywa się. Ale się pojawi jak jest potrzebna. Nie umie stawić czoła Inferni - a już zwłaszcza Eustachemu.
    * Eustachy ma Leonę na pokładzie. Leona jest słaba... za słaba. Ale będzie walczyć dla Inferni. Tivra?
    * ( Tivr ma zniszczonego miragenta na pokładzie. Pokłosie Sowińska - Morlan. Wygląda jak martwa Arianna. )
    * Maria wchodzi na pokład, z prośby zdalnej Martyna. Starcie Leona - Maria. ("Jestem tylko lekarzem" + brak strachu). NIKT jej nie ufa XD.
    * Starcia między "kultystami", "noktianami" i złamanymi.
        * Rafał Kot (kultysta), Franciszek Muchacz (kultysta), Janina Leszcz (kultysta)
    * Szepty Seweryna Atanaira.
    * Dziennikarz z pytaniem o wiadomość od pirata. "Krwawy Jonasz Pudelek".

### Sukces graczy

* 

### TODO

* ?

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Jedenaście dni później, Eustachy się obudził w zupełnie nowym świecie. I Leona z pełną dezaprobatą. Chce, by Eustachy pomógł jej dostać się na Infernię. "Nie jesteś ani szczególnie silny ani bystry, ale masz reputację kochanka roku. Więc mnie bałamuć, weź pod ramię."

Wpada dwóch pielęgniarzy, bysiów. Chcą Leonę - ona nie ma siły. Eustachy się postawił, chce Leonę.

Tp (niepełny) +2+3O:

* V: dobrze, idź pani z nim.
* O: plotki, że się zadajesz z Leoną. Jesteście parą. Dowód -> przyszła do Eustachego, a on ją czułą opieką. Tak, puścili ją z Eustachym.

Leona się obwinia, że jeden raz jej nie było i od razu doszło do katastrofy.

TYMCZASEM ARIANNA.

* Nikt nie upomniał się o Flawię. Jej zniknięcie "obyło się bez echa".
* Arianna dostała od Klaudii serię listów napisanych "z żalem zawiadamiam że". Dla każdego (łącznie z Flawią) dopisała kilka słów i wysłała. --> Arianna woli wysłać niż siedzieć cicho.
* Maria -> tymczasowe dołączenie do załogi. Arianna się z nią kontaktuje. Chce dołączyć tymczasowo.
* Karol Reichard wiadomość do Arianny -> nie ma Eleny. CZY ROBI GRY WOJENNE BEZ ZEZWOLENIA! Arianna z kamienną miną "SĄ ZEZWOLENIA".

--> Arianna do Klaudii: napraw. Gry wojenne. Aha, gdzie jest Elena?

Eustachy -> Klaudia. Na prośbę Leony... trzeba porwać medyka..? Ściągnąć medyka na pokład Inferni? A Eustachy serio nie ogarnia co tu się dzieje. Eustachy dostarcza cieknący worek (ranną Leonę) na Infernię...

Klaudia robi gry wojenne. "Schował się advancer i go szukamy". I odzywa się do Olgierda - "Arianna schowała advancerkę. Myślisz, że ją znajdziecie?"

TrZ+2:

* XX: W tym samym czasie i miejscu toczą się INNE gry wojenne. Reichard ma opierdol. Nie sprawdził, że dwie gry wojenne w tym samym miejscu. SUPER-UWAŻNY wobec Inferni.
* V: Klaudia je poszerza. 
* V: Zrobimy zasadzkę na ich ludzi. Poradzić sobie w kryzysowej sytuacji przeciw zaskoczeniu.

Mamy zwiększone siły polujące na Elenę. Poszukiwania Eustachego - Eustachy chce znaleźć Elenę PIERWSZY, przed wszystkimi innymi.

ExZ+3:

* Vz: Eustachy za dobrze zna Elenę. Wiedział, jakie są jej wzory poruszania się. Plus nie ma wyłączonego transpondera.

Eustachy -> Elena. Znalazł ją. Wraz z dwoma inżynierami poszedł w kierunku na najbardziej plugawe, Skażone sektory K1. Elena sama wyszła by ich poratować - nie ma SZANS że technicy są w stanie tam się poruszać i nie wymrzeć. Eustachy zresztą też. Elena spytała, po co tam są - bo wszyscy się o nią martwią. Aha. Ok, to wróci. Elena próbuje tak lekko, udaje, że nic się nie dzieje. 

A Eustachy naciska na Elenę. Chce, by powiedziała, jak się czuje, co jej jest. Tak trochę nie wprost.

TrZ+2:

* V: Elena wybuchła i powiedziała prawdę:
    * ma DOŚĆ tego, że ludzie przez nią umierają. Gdyby nie ona, Esuriit nie wlałoby się na K1. Ona jest tego winna. Ona zniszczyła ich wszystkich.
    * to nie jest pierwszy raz, pierwsze osoby zabiła jak była nastolatką.
    * Eustachy na to, że im bardziej chcemy pomóc im bardziej się staramy tym bardziej zło wyrządzamy często. Ona musi z tym żyć. Nie podoba mu się, że jej zdaniem byłoby lepiej gdyby nie istniała.
* V: "Esuriit to jest głód, to obsesja. Zgadnij co jest moją." - Elena, zrozpaczona, do Eustachego.
    * Przyznała się, że ON jest jej obsesją. Obsesyjnie go kocha. Tak, powiedziała mu to. Że nie umie myśleć o niczym innym niż on.
    * Eustachy: "To nie nasza przeszłość determinuje a nasza teraźniejszość".

Eustachy wpadł na pomysł. Zdehermetyzował kombinezon i wystawił się na promieniowanie z otwartych, toksycznych systemów K1 sektora 49. 

Elena w panice odpala moc magiczną. Wszystko. Przytula Eustachego i się z nim portuje!

TrZM+3+5O:

* Vz: Elena przytula, ratuje...

I Elena natychmiast wzywa kogoś do pomocy. Znajdują ich inni żołnierze którzy szukają advancera, nie z Inferni. ZNALEŹLIŚMY ICH! MAMY ADVANCERA!

Maria i antyradiacyjne -> Eustachy.

Arianna wysyła do Olgierda wiadomość - Eustachy go uprzedził.

Arianna i Elena. Elena w absolutnej desperacji. Leona. Eustachy wybrał LEONĘ a nie ją. Elena wypłakuje oczy. Nie jest stabilna, nie wierzy w swoje możliwości, w swój Wzór, nie wierzy że nadal jest sobą i że może to przetrwać. Arianna - niech Elena walczy.

## Streszczenie

Infernia nie ma dość załogi, by móc działać, więc Termia przekazuje Ariannie Tivr. Leona nie może sobie wybaczyć, że jej nie było i to się stało, więc się wypisuje z pomocą Eustachego (co powoduje plotki Leona x Eustachy). Elena ucieka do Sektora 49 na K1 by być samotna; Arianna organizuje ostre poszukiwania. Eustachy znajduje Elenę by ją pocieszyć. Elena w końcu przyznaje Eustachemu, że go kocha. Maria Naavas z woli Martyna tymczasowo dołącza do załogi Arianny.

## Progresja

* Elena Verlen: załamana i zrozpaczona. Eustachy wybrał LEONĘ nad nią. Ze wszystkich osób. LEONĘ!
* Elena Verlen: jej zdaniem byłoby lepiej, gdyby NIE ISTNIAŁA. Zabiła tyle niewinnych osób. Esuriit ją pożera. Chce odejść w noc... ale nie może i nie chce. Eustachy lub śmierć.
* Eustachy Korkoran: plotki, że zadaje się z Leoną na Inferni. Że ona jest jego stałą partnerką. Kinky pair of doom.
* Karol Reichard: bardzo ostrożny we wszelkich kontaktach z Infernią. WIE, że coś jest nie tak, ale nie ma jak tego udowodnić lub się bronić.
* OO Tivr: przechodzi pod dowództwo Arianny Verlen, z daru admirał Termii
* Arianna Verlen: dostaje Tivr jako swoją drugą jednostkę

### Frakcji

* .

## Zasługi

* Arianna Verlen: działania administracyjne; akceptacja listów o zgonie załogantów (+ pisze coś od siebie), NIE UKRYWA Flawii mimo że mogłaby uniknąć potencjalnych reperkusji, organizuje gry wojenne by ukryć, że Elena sama uciekła do czarnych sektorów K1.
* Eustachy Korkoran: wydostał Leonę ze szpitala na jej prośbę. Potem jednak przekierował się na Elenę, która uciekła i potrzebowała pomocy. Próbował ją uspokoić i wyjaśnić, że będzie dobrze. Faktycznie, uspokoił ją trochę. Pokazał że jej ufa. Acz się napromieniował (co jest nieprzyjemne).
* Klaudia Stryk: Napisała serię listów o śmierci załogantów Inferni by Arianna nie musiała. Potem zorganizowała gry wojenne, by ukryć fakt, że Elena zniknęła w czeluściach K1. Poinformowała Marię o sytuacji z Martynem (bo była dead man's hand Martyna - jakby coś mu było, Klaudia ma powiedzieć Marii). Aha, znalazła Innego Medyka Na Infernię.
* Leona Astrienko: w stanie niesprawnym; wyszła ze szpitala, bo Infernia jej potrzebuje. Sama się wypisała z pomocą Eustachego. 20% pełnej mocy.
* Elena Verlen: złamana, nie może się pogodzić z tym co zrobiła (Esuriit) i co się z nią stało (Esuriit). Ucieka do Sektora 49. Arianna organizuje grę wojenną by ją znaleźć. W końcu Elena przyznaje Eustachemu, że go kocha. Ale nie usłyszała tego z powrotem, więc wypłakuje oczy, że Eustachy wybrał LEONĘ nad nią.
* Karol Reichard: upewnia się, że Arianna nie robi GIER WOJENNYCH BEZ ZEZWOLENIA! Za to dostaje wpiernicz (Klaudia antydatowała gry wojenne). 
* Maria Naavas: tymczasowo dołączyła do Inferni z woli Martyna (który jest nieaktywny). Podała Eustachemu leki antyradiacyjne po jego wybryku. Zajmie się Eustachy x Elena ;-).
* Wawrzyn Rewemis: 72-letni weteran, medyk, 36% zmechanizowania, maska na twarzy. Klaudia ściągnęła go na Infernię do zajmowania się Leoną. Nie jest wybitny, ale może pokierować młodych.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy
                    1. Sektor 49: bardzo zniszczony, niestabilny sektor K1. Wiecznie gorejące, z trudem autoreperowane reaktory. Anomalie. Tam się schowała Elena przed światem.

## Czas

* Opóźnienie: 11
* Dni: 1

## Konflikty

* 1 - Wpada dwóch pielęgniarzy, bysiów. Chcą Leonę - ona nie ma siły walczyć. Eustachy się postawił, chce Leonę wypisać (bo ona chce).
    * Tp (niepełny) +2+3O:
    * VO: dobra, udało się - ale są plotki, że Eustachy i Leona to PRAWDZIWA para. 
* 2 - Klaudia robi gry wojenne. "Schował się advancer i go szukamy". I odzywa się do Olgierda - "Arianna schowała advancerkę. Myślisz, że ją znajdziecie?"
    * TrZ+2
    * XXVV: Są w tym czasie inne gry wojenne i Reichard (niewinny) ma wpiernicz za niewinność. Klaudia poszerza grę i inni komodorowie są "za".
* 3 - Mamy zwiększone siły polujące na Elenę. Poszukiwania Eustachego - Eustachy chce znaleźć Elenę PIERWSZY, przed wszystkimi innymi.
    * ExZ+3
    * Vz: Eustachy za dobrze zna Elenę. Wiedział, jakie są jej wzory poruszania się. Plus Elena nie ma wyłączonego transpondera.
* 4 - Eustachy naciska na Elenę. Chce, by powiedziała, jak się czuje, co jej jest. Tak trochę nie wprost.
    * TrZ+2
    * VV: Elena powiedziała prawdę Eustachemu. PRAWIE całą prawdę - kocha go (tak), nienawidzi tego czym się stała i że ludzie przez nią umierają (tak).
* 5 - Elena w panice odpala moc magiczną. Wszystko. Przytula Eustachego i się z nim portuje!
    * TrZM+3+5O
    * Vz: Elena uratowała Eustachego portując się z nim poza niebezpieczne sektory K1. Nawet nie wie jak to zrobiła i jak to dziwne, że jej się udało (bo nie miała namiaru).
