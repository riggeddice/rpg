## Metadane

* title: "Potworność powojenna Trzykwiatu"
* threads: brak
* motives: the-addicted, the-protector, energia-esuriit, teraz-jest-inaczej-niz-kiedys, deathwish, maskowanie-silnych-uczuc, lost-my-way, complete-monster
* gm: żółw
* players: fox, kapsel, kić

## Kontynuacja
### Kampanijna

* [240306 - Potworność powojenna Trzykwiatu](240306-potwornosc-powojenna-trzykwiatu)

### Chronologiczna

* [231228 - Księżniczka Arianna ratuje dzieci w lesie](231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * Inspiracje
        * Nimona "przyjaciółki", Kerrigan vs Narud "lost all", Zeiram "spawner, skillset", Izaro "pity the emperor sitting lonely on a throne"
        * Samildanach/Morrigan: "Czym się staliśmy, gdzie się podział ten młody piękny rycerz..."
    * Lombard - "Gołębi Puch"
        * "Każdym dniem zabijam to | Deszczem obcych rąk | Zmywam ślad | Zabijam to | Każda noc przynosi znów | Bezsensowny ból"
* Opowieść o (Theme and vision):
    * "Świeżo po wojnie, w trudnych sytuacjach, w absolutnej rozpaczy - miasteczko radzi sobie tak jak może i oficer Sił Astorii podejmuje trudne decyzje gubiąc swoją drogę. They become Monsters."
        * Ulrich, drużynowy mag i medyk, poświęcił się by uratować miasteczko przed Potworem -> stał się Potworem i "chroni Miasteczko" przed złem i potworami
        * Latisza, porucznik dowodząca siłami, przejęła kontrolę nad miasteczkiem -> karmi Potwora nie zauważając, że świat się zmienił i sama jest przyczyną koszmaru
    * Lokalizacja: niewielkie miasteczko przy Podwiercie, okolice (las), przeszłość
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * the-addicted: Ulrich, kiedyś bohater, który użył mocy by ratować wszystkich - to poświęcenie zabrało mu wszystko, ale go (niestety) nie zabiło, zmieniając go w Potwora. Pragnie nadziei, spawnuje Syreny.
    * the-protector: Latisza, kiedyś bohaterka, która zrobiła WSZYSTKO by ratować miasteczko, łącznie z dostarczeniem Ulrichowi ofiar które mają go zasilać i odsunąć jego Głód od miasteczka.
    * energia-esuriit: nieskończony głód i pożądanie nadziei, energii zmieniły Ulricha w potwora. Esurient i tak powróci. A jego Syreny ściągną nowe ofiary. Pomniejszy dotyk Esuriit wpływa też na Latiszę, która bierze coraz mocniejsze narkotyki by NIE CZUĆ głodu i pustki.
    * teraz-jest-inaczej-niz-kiedys: kiedyś 'karmienie esurienta' to była jedyna opcja, ale sytuacja się zmieniła. Teraz da się to zrobić inaczej. Są magowie, jest większa siła itp.
    * deathwish: zarówno Ulrich (potwór, którego resztka chce umrzeć) jak i Latisza (która nie może spać przez to co zrobiła)
    * maskowanie-silnych-uczuc: Latisza maskuje rozpacz i poczucie bycia potworem przez dekadencję i autorytarne rządzenie się i pławienie się w luksusie i zabawie.
    * lost-my-way: Latisza, która zmieniła doświadczonych i zaufanych przyjaciół z sił astoriańskich w łowców młodych ludzi, młodych dziewcząt by karmić Ulricha dla ochrony przed Eternią.
    * complete-monster: Zofia, która z przyjemnością przeprowadzi 'farmy ludzi' by tylko dostarczać żywe osoby Ulrichowi i chronić społeczność. I corruptor Latiszy.
* O co gra MG?
    * Highlevel
        * Postacie Graczy stanowią źródło destabilizacji Miasteczka i ich działanie niszczy delikatną tkankę.
        * Serio, w tej sesji jestem przede wszystkim ciekaw jakie decyzje podejmą i jak podejdą do problemu Latiszy, Pustogoru, tego co trzeba było zrobić itp.
        * Żadnych granic. Żadnych kompromisów. Zabij, skrzywdź i zdewastuj. Mechanikę podciągnij na maksimum.
            * Rany, utrata sprzętu, utrata przyjaciół, Marked By Death...
    * Co uzyskać fabularnie
        * Latisza powinna umrzeć - albo jako nowy Potwór, albo jako ofiara. Ale jej złapanie, oskarżenie i publiczna pogarda też są OK.
        * Postacie graczy powinny zostać potrzaskane, poranione i odrzucone przez społeczność. Nagroda za próbę naprawy sprawy.
        * Jeśli wezwane będą siły Pustogoru, powinny uznać to miasteczko za plugawe miejsce godne oczyszczenia. Heavy purge. Sporo osób które wiedziały zostanie oskarżonych i aresztowanych.
            * To miasteczko dostanie regenta z Pustogoru; "nie są zdolni do samosterowania".
        * Miasteczko traci część populacji; ludzie umierają przez Potwora.
* Default Future
    * Latisza coraz bardziej karmi potwora ludźmi (niewolnikami, porwanymi).
    * Ulrich coraz bardziej traci człowieczeństwo
    * Docelowo, wymienimy Ulricha na innego maga.
* Dilemma: Cała sesja jest jednym wielkim chędożonym dylematem XD. Latisza: zniszczyć / zostawić? A jej legenda? Ulrich i jego legenda? Ryzykować zniszczenie Potwora i otwarcie na świat?
* Crisis source: 
    * HEART: "Latisza i Ulrich wybrali chronienie swoich ludzi ponad robienie tego co właściwe. Niekoniecznie mieli wybór, ale tak zrobili."
    * VISIBLE: "Powstanie Drapieżców NeoVim, Potwór (Ulrich), znikający ludzie, 'znaleźli kogoś nie wiadomo skąd', The Siren"
        
### Co się stało i co wiemy

* Oddział milicji astoriańskiej 'Neokwiat' do walki z Noctis działał w okolicach miasteczka Trzykwiat, prześladowane przez Potwora
    * Neokwiat zniszczył Potwora, Ulrich się poświęcił walcząc z tym cholerstwem.
    * Neokwiat dostał za zadanie chronić Trzykwiat przed potworami i Noctis. 
    * ALE było tego za dużo i było to za trudne
* Latisza zaczęła karmić Ulricha (już potwora) ludźmi - nagle Trzykwiat skutecznie się broni - przed bandami Noctis, przed potworami z lasu itp.

### Co się stanie (what will happen)

* F0: TUTORIAL
    * Sandra, Latisza, Ulrich vs 8 noktian z ludźmi zamkniętymi w zepsutym pojeździe
* F1: 
    * Kacper: sierżant. Fox: dewastator, Kić: operator dron i inżynier
    * Znikają ludzie (moja kuzynka zniknęła: Ksenia -> (Kacper))
    * Działa ZeiriWiła (Ksenia do lasu)
    * Latisza pije
* F2: 
    * Znalezienie człowieka bez języka, Latisza go przejmie
    * Bunkier 
    * Siły Juliana idą do lasu szukać ludzi -> Atak Ulricha; Julian zginie
* F3: Decyzja i konfrontacja
* Overall
    * chains
        * .
    * stakes
        * .
    * opponent
        * .
    * problem
        * .

## Sesja - analiza
### Fiszki

* Natan Wierzbitowiec
    * OCEAN: N+O+ | niezwykle lojalny, cichy, wykonuje rozkazy bez kwestionowania; bezwzględny - Soundwave, TF:Prime.
    * VALS: Tradition > Self-direction | Powodzenie Trzykwiatu, zachowanie status quo. Latisza nade wszystko.
* Petra Łomniczajew
    * OCEAN: A-O+ | skrajnie logiczna i oddana Natanowi, najlepszy winien rządzić, coś trzeba z tym zrobić, kiepska w intrygach - Shockwave, TF:Prime
    * VALS: Achievement, Family | Zdrowie i radość Natana, Trzykwiat jako najlepsza miejscowość i oaza spokoju, zrozumienie świata nauką
* Zofia Skorupniczek
    * OCEAN: N+C+ | niezwykle intensywna i amoralna, charyzmatyczna i przekonywująca - Bombshell, IDW
    * VALS: Achievement, Hedonism | sadystka i niezwykle precyzyjna badaczka magii; zrobi wszystko by pomóc Latiszy i Trzykwiatowi

### Scena Zero - impl

brak

### Sesja Właściwa - impl

Ksenia odwiedza Jakuba. Majsterkowanie, sprzęt tego typu rzeczy. Jakub zwykle usprawnia swój dom. Majsterkuje itp. Ale Ksenia napatoczyła.

* Ksenia: Mam problem, nie wiem jak rozwiązać, coś jest nie w porządku.
* Kuba: Jaki problem?
* Ksenia: Moja kuzynka zniknęła. Jechała stąd do innych miejsc i nie dotarła. Wszyscy którzy jechali nie dotarli. 8 osób. A _pani porucznik_ odmówiła wysłania pomocy.
    * (Latisza była świetnym oficerem - popada w 'młodzi chłopcy, fajne używki i ogólnie jest dobrze nie przeszkadzać')
    * (Mniej potworów niż w okolicznych miejscowościach, nie jest to blisko Kurhanu Stali)

Jakub: Ksenia miauczy i trzeba zrobić robotę. Latisza nic z tym nie robi. Szukamy zaginionych. Nie powinni być przy kurhanie.

Zbierasz zespół, ale łapie Cię Arek.

* Arek: Kuba, gdzie idziesz?
* Kuba: (naświetla)
* Arek: Porucznik Latisza sobie tego nie życzyła. Coś się dzieje, potrzebne są wszystkie siły.
* Kuba: Zaginęło już 10 osób, i to blisko - na naszym podwórku.
* Arek: Latisza wysłała oddział 20-osobowy. Elitarny oddział z Natanem. Znajdą ich. Wy musicie zostać. Powiedziała Ksenii, by to zostawiła.

Jakub -> Latisza

* Latisza: Kto tam? Jestem zajęta (chichot w tle)
* Kuba: Zaginęło 10 osób i wysyłasz oddział ogniem i mieczem, nie idź tam. Może... co wiesz? O co chodzi? Ekipa szturmowa?
* Latisza: Wiesz, jest tam znowu grupa noktian. Albo lokalnych bandytów. Podejrzewam, że poszli na handel.
* Kuba: Prościej będzie ogniem i mieczem i mieć wywalone czy nie?
* Latisza: Wolę ich uratować, Natan jest kompetetnym...
* Kuba: Zobaczą ich z kilku kilometrów. Jak żyją...
* Latisza: Co proponujesz? Idą... 
* Kuba: Trzymaj w odwodzie, wezmę swoich, jako zatroskani obywatele, zrobimy przynętę i jak się rzucą to spacyfikujemy. A jak źle to na białym koniu. Zapodziali i nie umieją wyjść, na pełnej kurwie jest kiepskim pomysłem. Maksymalizujemy niebezpieczeństwo. A YOU ARE SNEAKY to podnosi szansę.
* Latisza: Tak, ale... (ona coś wie)
* Kuba: Jakie ALE? Jak coś wiesz, powiedz.

Tp +3 +3Oy:

* Vr: (bo Ulrich) Latisza ma wyraźnie wściekły głos (zdenerwowałeś, nie wiesz czemu). "Ulricha w to nie mieszaj. Tak, nie żyją, ok? WIEM że nie żyją. Trzeba... trzeba to rozwalić."
    * Kuba: To skoro wiesz to trzeba powiedzieć a nie odpierdalać jakieś demokratyczne...
    * Latisza: Nie ma demokracji, to Ksenia przyszła. 
    * Kuba: Ty odpowiadasz za bezpieczeństwo wszystkich, jeśli zgłasza że coś nie tak... co rozwalić.
    * Latisza: Nie ogarnąłbyś tego. Zostaw to Natanowi. Zatrzymaj Ksenię czy ją pociesz czy coś. Dało się ich uratować. (wymsknęło)
    * Kuba: Powiedz więcej
    * Latisza: Nie, nic nie muszę Ci mówić. Miasto działa i jest bezpieczne. Mówiłam Ksenii, żeby kuzynka się nie ruszała. Teraz zadbaj by KSENIA została tutaj. Bo też zginie.\
    * Kuba: To w obrębie miasta
    * Latisza: Poza działkami. Fortifarmy i miasta są bezpieczni
    * Kuba: Wszyscy obywatele są bezpieczni?
    * Latisza: TYLKO CI KTÓRZY SIĘ SŁUCHAJĄ.
    * Latisza: Bądź usłuchanym żołnierzem, pilnuj Ksenii i zostaw mnie w spokoju, jestem zajęta. Pedicure samo się nie zrobi.

Petra potrzebowała różnych rzeczy. A Zuza dostarczała jej tych rzeczy. Petra lubi Zuzę. A Petra to dziewczyna Natana.

Petra siedzi w swoim baraku, robiąc badania CZEGOŚ. Jak zobaczyła Zuzę, odłożyła na bok. 

* Petra: jestem zajęta (z uśmiechem)
* Zuza: 5 minut?
* Petra: Znajdę.
* Zuza: niezła impreza, coś odzyskiwać.
* Petra: Impreza? (blink blink)
* Zuza: Walka
* Zuza: (zasypuje pytaniami by Petra nie miała czasu myśleć)

Tr+3:

* V: 
    * Petra: zauważyłaś, że Latisza troszeczkę...
    * Zuza: opuszcza się w obowiązkach
    * Petra: Tak. I Natan dowodzi, tak naprawdę.
    * Zuza: o.
    * Petra: Zauważyłaś, prawda?
    * Zuza: TAAAAK, skoro Latisza z głową w łóżku
    * Petra: Może... może czas na zmianę przełożonego?
    * Zuza: Nie jestem przywiązana do tego kto dowodzi, póki działa jak trzeba, może dowodzić nawet Jakub
    * Petra: (zaśmiała) Nie jest kompetentny, to tylko Jakub
    * Zuza: Raczej nie, ale...
    * Petra: Natan został wysłany na KOLEJNĄ akcję mającą ukryć jak jest źle. Szukałam. Szukałam w danych.
        * (wyliczenia): miasta i miejsca w okolicy mają 30-60% więcej ataków niż Wasze
    * Petra: Powiedz mi że to nie jest anomalia statystyczna.
    * Zuza: Może jesteśmy w lepszej pozycji albo Latisza ma kontrakty? Albo potwór?
    * Petra: Natan... Popatrz (pokazała to co bada) - to kawałek metalu z energią magiczną.
        * (wyliczenia): tego jest więcej. Co ciekawe, z grup DĄŻĄCYCH do nas i DĄŻĄCYCH OD NAS jest o 200% większa letalność niż zwykłych.
    * Zuza: Teren groźny dla ludzi a my w oku cyklonu?
    * Petra: Tak. A Latisza coś wie. I wysyła Natana. Komuś coś powie

Natalia dostaje dane od reszty Zespołu. Wysyła stealthy drone do Latiszy. Niech ta ją podsłuchuje. Plus zbereźne nagrania do rozpowszechnienia itp.

Z ciekawostek - drony pobierasz ze Zbrojowni. Zbrojownia ma za mało sprzętu. DUŻO mniej niż powinna mieć. Powiedzmy - ma 20% tego co powinno być. Mnóstwo sprzętu zniknęło. (info do Zespołu). Jest tu zaopatrzeniowiec. Co szczególnie wkurzyło Natkę - większość HUNTER-KILLER dron też zniknęła. Radosław Zientek.

* Radek: Co?
* Natalia: No co tak pusto?
* Radek: (westchnął) Latisza. Jej rozkazy.
* Natalia: ZEŻARLIŚCIE TO?
* Radek: (uśmiech) Nie, nie. Nie, Nati, chyba... na pewno wydałem to Natanowi. Natan miał jakąś sekretną misję. (brak na 60-70 os)
* Natalia: Za dużo, jak muły
* Radek: Nie, mówisz o tym teraz - a ja mówię ogólnie. To było wcześniej.
* Natalia: Nie zdają sprzętu?
* Radek: Zdają. Ale Latisza powiedziała, że ten sprzęt nie wróci. Ona dowodzi. I miałam nikomu nie mówić. 
* Natalia: Ok... mogę... czego potrzebujesz się dowiedzieć? Czego chcesz?
* Radek: Ja nic nie chcę. Ja jestem lojalnym obywatelem.
* Natalia: Jak my wszyscy.
* Radek: Poza Petrą. Jej nie nazwiemy "lojalną obywatelką".
* Natalia: No cóż...
* Radek: Manifesty były zatrzymane w fortifarmach, odebrane przez Natana
* Natalia: Co tu się dzieje?
* Radek: Nie wiem. Latisza mówi, że to zapewnia nam bezpieczeństwo. Ja wierzę Latiszy. Tak czy inaczej - demilitaryzujemy się i nie mamy pełni siły fabrykatorów
* Natalia: Dotąd nie dała nam powodu by nie wierzyć, nie?
* Radek: Tak... (z lekkim brakiem przekonania)

Nati ma dane które fortifarmy itp. Bierze drony i idzie dalej. CIEKAWE - to zawsze te same 3 fortifarmy. Mniej ufortyfikowane, bogatsze, z zaufanymi ludźmi Latiszy.

Jakub zamknął Ksenię w piwnicy, kajdankami do kaloryfera i się nie ma jak uwolnić sensownie.

Co mamy:

* zaginięta grupa ludzi - są martwi?
* 80% sprzętu wywiezione z bazy (Radek ukrywał)
* oko cyklonu - wszystko ginie, niedaleko Kurhan Stali. Który Wam nic nie robi.

Teoria Zuzy - Latisza jest dotknięta Esuriit, przez Ulricha. Jeśli tak, Zofia będzie wiedzieć. Może dlatego się Zofia i Latisza zbliżyły do siebie?

Jakub i Zuza idą do Petry. Znowu, pogadać o Latiszy i Zofii.

* Petra: Znowu Wy? Jestem zajęta.
* Kuba: Sprawę mam do Ciebie. 
* Petra: Narkotyki dla Latiszy? Hm, nie wiedziałam że są dla Latiszy (zdziwiona). Tak, wiem więcej.
* Petra: Większość składników jest robiona lokalnie, w jednej z fortifarm. Są też eksportowane. Wiem, bo pomagałam liczyć ceny.
* Kuba: Zbrojownia jest prawie pusta
* Petra: CO! (zapluła się). Ona jest niegodna. Zdradziła!
* Kuba: Nie wiemy, coś wisi w powietrzu. Wyprztykała się ze sprzętu, też z tego z którego się nie wyprztykuje.
* Petra: Chodźcie ze mną. Dostałam coś od Natana

(Petra pokazała lodówkę. A w środku - głowa. "Syrena", która jest wyhodowana na brance / człowieku). Obserwacja: WSZYSCY którzy znali Ulricha bliżej, nic im się nie działo. Pierwszą osobą jest kuzynka Ksenii. Może... może Ulrich traci nad sobą panowanie?

* Natalia: Obalamy rząd?
* Kuba: Niech rząd się obroni.

Zespół idzie do Latiszy. Latisza ma dwóch strażników (których znacie). Nati ma podsłuch który przyklei. No i poproszono Was byście poczekali. Latisza w końcu się pojawiła, w nieco rozchełstanym mundurze i nieco błędnym obliczu.

* Latisza: No?
* Kuba: My zadajemy pytania.
* Latisza: Nie bo ja dowodzę. Ja jestem królową wiesz?
* Zofia: (zza kotary wychynęła Zosia). (Podała coś Latiszy. Ta to wypija)
* Latisza: Nadal jestem przewodniczącą... porucznikiem. Ty nie. Czemu tu jesteście?
* Kuba: Defraudujesz środki.
* Latisza: Nie, nie defraduję środków.
* Kuba: Jesteś poćpana jak messerschmidt!
* Latisza: Mmmoże! Nadal... nadal jestem sensowniejsza niż Ty. WSZYSTKO dla tego miasta robię, wszystko. A Ty nie doceniasz, tylko kwestionujesz.
* Natalia: Np. co?
* Kuba: Pozbycie się replikatorów?
* Latisza: MMoooże. Nie wiesz co tam było.
* Zofia: Pani porucznik chciała powiedzieć, że replikatory były uszkodzone.
* Kuba: Wszystkie?
* Zofia: Nie, tylko te, które wyeksportowała w swojej wielkiej mądrości.
* Kuba: Kto chciałby nabyć uszkodzone replikatory?
* Zofia: (spojrzała z niedowierzaniem) A kto im powie?
* Kuba: Robicie deal, wysokokosztowy, i potencjalny kupiec dostanie wadliwy produkt. Zero problemu?
* Latisza: Ja widzę problem. Widzę. Ulrich by nie chciał. Ale musimy chronić miasto.
* Natalia: Przed czym? Przed Ulrichem?
* Latisza: NIE ON NIGDY BY NIC NIE ZROBIŁ MIASTU.
* Kuba: Działasz na szkodę miasta?
* Latisza: (coś powiedzieć)
* Zofia: Pani porucznik zapewnia, że miasto jest wieczne i bezpieczne.
* Natalia: Nie ma sprzętu, nie obronimy się, ta tu (Latisza) jest oćpana z dupy... nie klei się. Coś się dzieje, o co chodzi?

Tr +3:

* X: Czyny Latiszy dalej są nieakceptowalne dla większości jej dawnego oddziału. To jest złe i błędne.
* V: Zośka powie Wam sporo.
    * Zofia: Obronimy się. Te Fabrykatory były Skażone. Trzeba było się ich pozbyć. Jeśli za nie dostaniemy pieniądze, materiał... to tym lepiej.
    * Zofia: Sprzęt... to bardziej skomplikowane. Musimy mieć... Latisza, jak to nazwałaś?
    * Latisza: Zespoły polujące. Łapacze.
    * Natalia: Co mają łapać?
    * Zofia: Ludzi, naturalnie.
    * Natalia: I co chcesz z nimi robić?
    * Latisza: Żeby Ulrich miał więcej sił. Żeby mógł nas chronić. Pamiętacie.

Latisza się przyznała. Mają polowanie na ludzi. Te dwie zrobiły niezły przemysł przeciwko ludzkości - jak to Zofia zaczęła "wiecie, jeśli potrzebujemy 10 osób na miesiąc a jedna kobieta może mieć jedno dziecko raz na dziesięć miesięcy...". Dla Latiszy to smutne, ukrywa twarz w dłoniach. Zofia się cieszy. Taki charakter, ona to lubi.

Zofia ma plany dodatkowe - jej modele pokazują, że bez straty ważnych osób mają 5-10 lat co najmniej a warto poszukać magów.

* Latisza: Powiedz co ty byś zrobiła, powiedz co byś zrobiła na moim miejscu. Nic nie było. Nic nie ma. Nie ma magów, nie ma kontaktu, nie ma maglev. Nic. Ale Ulrich został.
* Latisza: (płacz) On nie żył, ale go przywróciłam. Przywróciłam go. Użyłam go jako strażnika. Nigdy by mi nie wybaczył.

Nati: "jesteśmy w stanie zdobyć maga który będzie chciał tu przyjść, za moc siłę itp. Tam jest źle i tym bardziej łatwo tu go ściągnąć." Nati próbuje pokazać, że są w stanie zdobyć maga - one lepiej niż Natan. Natan jest zajęty tutaj, musi zdobywać ludzi.

Ex Z +4:

* Vz: Zdaniem Latiszy oraz Zosi to jest dobry plan. Możecie iść. ALE proponują, byście poczekały na Natana by dał wam obstawę. (Ksenia jest za darmo)
* Vr: Dostaniecie obstawę INNYCH żołnierzy których sami weźmiecie. Dzięki temu macie bezpieczniejsze przejście. No i macie pojazdy.

Zespół zabrał od Petry TO CO DOSTAŁ NATAN.

* Petra: Czyli przekonaliście Zosię, żeby jednak Natan dowodził?
* Zuza: Nie, ale w konsekwencji tak się najpewniej stanie.
* Petra: (uśmiech) Przewrót. Dobrze, pomogę Wam.

Petra wyposażyła ich we wszystko co będzie potrzebne. Zostawiła ich z uśmiechem.

Macie sprzęt militarny. APC i cztery ścigacze. 10 osób. Nati wybiera trasę z daleka od kurhanu i cisną możliwie najkrótszą drogą do celu. Najlepiej tam gdzie Natan przechodził w odpowiednich momentach - powinno być oczyszczone. Zespół ma przesądy - lokalne przesądy - i biorą lokalną "króliczą łapkę na szczęście". One powinny działać. PLUS drony dookoła karawany i jak coś się stanie - to rzucamy zwierzę hodowlane na pożarcie (pomaziane ludzką krwią) i wiejemy dalej

Tp +4:

* Vr: Na pewno uda się uniknąć Natana
* V: Udało się uniknąć esurientów. Sukces.
* V: Używamy naszego sprzętu. Dojedziemy do Pustogoru w ciągu kilku godzin

SELEKCJA ŻOŁNIERZY: dobre dusze, ci co nie wiedzieli co się dzieje i co będą oburzeni. Ci, którzy stanęliby za Wami.

Pustogor. Zaczyna się ściemniać.

Barbakan.

Zespół bardzo prosi, by Latiszy pomóc a nie zabić. Ona naprawdę nie miała wyboru, zmanipulowana. Przez Zosię? A Natan wykonuje rozkazy.

Terminusi podejmują decyzję, wielkie spotkanie Zespołu z Terminusami Pustogorskimi. 

Ex+3+5Og (reprezentują ZAUFANIE Pustogoru do Was):

* X: Terminusi wejdą TWARDO. Przejmują kontrolę nad miasteczkiem i obejmują regencję. Tam będzie stacjonarny terminus (dwóch). Wy jesteście w oczach swoich zdrajcami.
* Vr: Terminusi dadzą spokój Natanowi i Petrze. Oni wykonują rozkazy i Natan zostaje administratorem, ale nigdy nie może dowodzić.
* X: Natan uruchomił plan 'Zefir'. Udało się mu ewakuować najbardziej umoczonych ludzi by Pustogor ich nie dostał. Ewakuował też Zosia.
* X: Latisza miała publiczny proces, terminusi pustogorscy pokazali to jako przykład "w każdym raju jest wąż" - tylko dlatego to miejsce mogło działać. Dzięki temu przyspieszona jest asymilacja innych miejsc.

Jakub zostaje regentem tego miejsca, jako ktoś kogo inni znają. Mimo, że uważają go za zdrajcę, ale jednak rozumieją. Natan - który przecież był problemem - też zostaje jako administrator Jakuba. A Natalia i Zuza mogą wrócić albo dostać inne wartościowe pozycje w innych miejscach.

## Streszczenie

Trzykwiat jest niedaleko Esurienta z Kurhanu, którym okazuje się, że Ulrich - mag i mąż Latiszy - zginął broniąc miasteczka przed Esurientem. Niestety, z uwagi na ciężką sytuację Latisza przywraca go do życia Esuriit i składa mu ludzi w ofierze z pomocą Zofii, co niszczy jej psychikę, ale chroni miasto. Gdy znika przyjaciółka ze starego oddziału grupka weteranów zaczyna śledztwo które odkrywa sekret Trzykwiatu. Bohaterowie wymanewrowali Latiszę i Zofię i zwrócili się do Pustogoru o pomoc. Pustogor przejął kontrolę, esurient będzie opanowany a weterani niestety skończą jako zdrajcy swoich.

## Progresja

* Jakub Kurbeczko: uznany za zdrajcę Trzykwiata, został regentem Trzykwiata z ramienia Pustogoru. Do końca wspiera Trzykwiat i przyjaciół, nawet pod terminusami.
* Zuzanna Szagbin: uznana za zdrajcę Trzykwiata, wyniosła się do innego miejsca niż Trzykwiat wspierana przez Pustogor.
* Natalia Pszaruk: uznana za zdrajcę Trzykwiata, wyniosła się do innego miejsca niż Trzykwiat wspierana przez Pustogor.
* Natan Wierzbitowiec: mimo, że jest mastermindem stojącym za Trzykwiatem, Cierniami Latiszy oraz esurientem, ma pełen pardon ze strony Pustogoru. Zastępca Jakuba.

## Zasługi

* Jakub Kurbeczko: sierżant; chciał szukać zaginionych ale doszedł do tego że Latisza coś wie. Lubiany, skonfrontował się z Latiszą - nie jest tą samą osobą co kiedyś. Zamknął Ksenię w piwnicy by czegoś głupiego nie zrobiła i postanowił z Zuzą i Natalią zniszczyć esurienta oddając kontrolę Pustogorowi.
* Zuzanna Szagbin: dewastator i infiltrator; zaprzyjaźniona z Petrą, dowiedziała się że Latisza jest mniej popularna niż się wydawało i poznała sekret Zofii - narkotyki dla Latiszy. Opracowała ewakuację i wybrała 10 osób z którymi należy udać się do Pustogoru.
* Natalia Pszaruk: drony i inżynieria; dobrze dogaduje się z Radkiem i skutecznie wykorzystuje drony by uniknąć esurientów; przekonuje Zofię, że mogą pozyskać maga do kontroli esurienta.
* Latisza Warkolnicz: bohaterska porucznik, która by bronić Trzykwiata 'wskrzesiła' ukochanego jako esurienta i szła coraz głębiej jak chodzi o mroczne decyzje. Skończyła jako 'marionetka' uzależniona od silnych dragów by zabić sumienie. Ostatecznie - osądzona przez Pustogor i stracona. Śmierć była dla niej wybawieniem.
* Ulrich Warkolnicz: zginął walcząc z Esurientem z Kurhanu; 'wskrzeszony' przez Latiszę by był strażnikiem, coraz bardziej esurient coraz mniej osoba. Chronił Trzykwiata, ale kosztem krwawych ludzkich ofiar. Do pokonania go wezwano Pustogor.
* Natan Wierzbitowiec: kompetentny administrator i świetny żołnierz, outer patrol i łapanie ludzi by karmić esurienta. To on naprawdę dowodzi w miejscu Latiszy. Nadal zostaje w Trzykwiacie, ale nie musi już nikogo zabijać i nie musi łapać ludzi by wzmacniać esurienta. Ewakuował Zofię i najbardziej umoczonych, tworząc Ciernie Latiszy.
* Petra Łomniczajew: niezbyt sympatyczna naukowiec lojalna Natanowi; urażona niekompetencją Latiszy. Nie do końca wie co się tu dzieje, ale bada Syrenę i doszła do wielu ciekawych śladów. Pomaga Zespołowi bo myśli, że pomoże Natanowi przejąć władzę.
* Zofia Skorupniczek: Amoralna i sadystyczna (acz lojalna) naukowiec Esuriit w służbie Latiszy; ona zaprojektowała 'farmy ludzi' by dostarczać esurientowi posiłek, ona przekonała Latiszę o konieczności pójścia głębiej w Esuriit. A najsmutniejsze, że działała na niepełnych danych, bo dało się to zmienić.
* Radosław Zientarmik: zarządzał zbrojownią i dostarczał sprzęt na tajne misje; pokazał Natalii, że mnóstwo wartościowych rzeczy została eksportowana i są tajne transporty na fortifarmy. Nadal lojalny Latiszy, ale serce go boli i nie wie co myśleć.
* Ksenia Byczajnik: jej kuzynka nie dogadywała się z Latiszą i zniknęła. Ksenia zainicjowała poszukiwania i poszła do Jakuba, co zastartowało wszystko. Lojalna kuzynce, chciała jej szukać, więc musiała zostać zamknięta w piwnicy. Teraz - dołączyła do Pustogoru.

## Frakcje

* Drapieżcy Neovim: Bardzo wzmocnieni przez Latiszę i Trzykwiat; sprzętem i kontaktami. De facto, Trzykwiat zbudował im tu kluczowe kontakty i linie.
* Trzy Ciernie Latiszy: Oddział stworzony przez Natana, ewakuowali Zofię. Bardzo niebezpieczna grupa. Czują się zdradzeni i odrzuceni, pustka w miejscu gdzie ratowali wszystkich ludzi. To moment ich powstania. Ewakuowali się poza zasięg Pustogoru.
* Terminuscy Strażnicy Szczeliny: współpracując z zespołem z Trzykwiata którzy powiedzieli o mrocznym sekrecie Trzykwiata weszli do Trzykwiata, przygotowali się do zniszczenia Esurienta, przejęli kontrolę nad Trzykwiatem i zainstalowali Jakuba jako regenta z Natanem na pierwszym oficerze. Ich reputacja i znaczenia poszło w górę i poszerzają swoje imperium.

## Fakty Lokalizacji

* Trzykwiat: słynie z wyjątkowo żyznej gleby i doskonałych plonów rolnych; oaza Astorii w Lasie Trzęsawnym, 20-30k populacji. Ma pozytywną imigrację, dobre miejsce po wojnie.
* Trzykwiat: otoczony fortifarmami i posiada dworzec maglev; ważne: okolice są wyjątkowo niebezpieczne (z uwagi na chroniącego Esurienta z Kurhanu i bandy łapaczy).
* Trzykwiat: został przejęty przez Pustogor i zainstalowano nowego regenta (Jakuba) - okazało się, że działał kosztem niewinnych i karmiono tu esurienta...

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Trzykwiat: słynie z wyjątkowo żyznej gleby i doskonałych plonów rolnych; oaza Astorii w Lasie Trzęsawnym, 20-30k populacji, TERAZ poza kontrolą Pustogoru
                                1. Centrum
                                    1. Bazar
                                    1. Ratusz
                                    1. Zbrojownia
                                1. Północny Pierścień
                                    1. Bastion
                                    1. Służby Miejskie
                                    1. Osiedle Północne
                                    1. Dworzec Maglev
                                    1. Wieże zewnętrzne
                                1. Południowy Pierścień
                                    1. Sady i Szklarnie
                                    1. Osiedle Południowe
                                    1. Wieże zewnętrzne
                            1. Trzykwiat, okolice
                                1. Las Trzęsawny
                                    1. Fortifarmy Trzykwiata
                                    1. Kurhan Metalu: miejsce, w którym kiedyś doszło do samobójczego ataku dwóch jednostek w ogniu Esuriit; jest tam Ruina Ulricha

## Czas

* Opóźnienie: -287
* Dni: 5
