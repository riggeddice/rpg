## Metadane

* title: "Tezremont rozwarstwiony"
* threads: serce-planetoidy-kabanek
* motives: the-man-in-black, handel-zlym-towarem, hostage-situation, b-team
* gm: żółw
* players: kić, fox, til, vivian

## Kontynuacja
### Kampanijna

* [241113 - Tezremont Rozwarstwiony](241113-tezremont-rozwarstwiony)

### Chronologiczna

* [241113 - Tezremont Rozwarstwiony](241113-tezremont-rozwarstwiony)

## Plan sesji
### Projekt Wizji Sesji

**WAŻNE**: Kontekst sesji i okolicy oraz Agendy itp DOKŁADNIE opisane w [Thread 'serce-planetoidy-kabanek'](/threads/serce-planetoidy-kabanek). Tam też znajduje się rozkład populacji itp.

* INSPIRACJE
    * PIOSENKA WIODĄCA
        * [Funker Vogt - What If I'm Wrong?](https://www.youtube.com/watch?v=4su5Nus3byM)
            * "If life is a task | But there is too much to ask | If I've been mistaken - all along"
            * Gabriel Lodowiec próbuje naprawić sytuację, ale jego działania prowadzą do katastrof politycznych.
            * Zespół widzi, że Lodowiec chce dobrze, robi dobrze, ale antagonizuje przy tym lokalsów.
        * [Powerwolf - Sacramental Sister](https://www.youtube.com/watch?v=tw3Mf9m3Jj0)
            * "Sacramental sister in heaven or hell | When the night is falling, no moral can tell | Sacramental sister, to God you are sworn | When desire calling, the Bible is torn"
            * Sprzeczności i kolizje. Nie ma jednej ścieżki. Złapać winnych czy uratować przewożonych? Zniszczyć niewolniczy statek i "Orbiter niszczy cywili" czy ich puścić?
    * Inspiracje inne
        * Deal with the Devil
* Opowieść o (Theme and Vision):
    * "_Zrobić, co należy czy co wizerunkowo poprawne?_"
        * SC "Szczęśliwa Świnka" jest pozornie jednostką cywilną, "niegroźną". Orbiter niszczący Świnkę to cios wizerunkowy.
        * Ale uratowanie tych dziewczyn czy doprowadzenie kapitana do stanu bezpieczeństwa ma znaczenie. Ale jak to zrobić?
        * NAPRAWDĘ Świnka jest zaminowana? A póki nic się nie dzieje, może przyjść wsparcie z Anomalii Kolapsu...
    * "_Tezremont podupadł. Nie jest tym, czym powinien być. Lepsza jednostka Orbitera by to zrobiła._"
        * GDYBY Tezremont leciał szybciej, GDYBY miał broń w lepszym stanie, GDYBY miał bardziej zmotywowaną załogę...
        * Lodowiec przegrywa przez to, że Tezremont jest jednostką w tak złym stanie.
    * Lokalizacja: Libracja Lirańska. Najpewniej planetoida Oratel a potem okolice Anomalii Kolapsu.
* Motive-split
    * the-man-in-black: TAI Evraina, TAI kontrolna SC Szczęśliwa Świnka. Ukrywa informacje o działaniach Świnki i wsparciu Ypraznir.
    * handel-zlym-towarem: Szczęśliwa Świnka robi co może, by zarobić, spłacić dług itp. Ma tam ludzi do transformacji w imieniu Ypraznir - organizacji przestępczej.
    * hostage-situation: TAI Evraina trzyma Szczęśliwą Świnkę w szachu; są tam miny, ładunki wybuchowe i cywile. Evraina zrobi wszystko, by udowodnić, że Orbiter zabił niewinnych.
    * b-team: wyraźnie Tezremont nie jest gotowy do tak skomplikowanej operacji; załoga Tezremont jest rozleniwiona i nie mają wprawy do walki "na serio". Są zbyt pewni siebie jako Orbiter.
* O co grają Gracze?
    * Sukces:
        * Uda się coś osiągnąć - albo zatrzymać Świnkę, albo złapać kapitana, albo whatever
        * Uda się nie zniszczyć reputacji Orbitera na tym terenie
        * Uda się nikogo nie stracić
    * Porażka: 
        * .
* O co gra MG?
    * Highlevel
        * Pokazać, że Orbiter jest najsilniejszy na tym terenie, ale nie jest wszechmocny
        * Pokazać jak niesamowicie niebezpieczne jest to miejsce i że ta Grupa Wydzielona Orbitera nie jest dość kompetentna
            * Lodowiec zawodzi jako polityk; Orbiter pomaga ludziom, ale nadal stanowi "demoniczne zło" w okolicy
            * Pawilończyk zawodzi jako kapitan; Świnka stanowi dlań za duży problem
    * Co uzyskać fabularnie
        * Zniszczyć wartościowe 
* Agendy
    * The MiB: chce ukryć wpływ Ypraznir i zrzucić winę na Orbiter. Lub uciec w okolicę Placidistanu.
    * The Rival: chce pokazać 

### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

KONTEKST:

* Zgodny z THREAD

CHRONOLOGIA:

* Znalezieni przekształceni górnicy przez anomalię; Orbiter to przechwycił (Lodowiec)
* Lucjusz odkrył, że za tym stoi statek "Szczęśliwa Świnka"

PRZECIWNIK:

* brak, anomalia?

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

FAZA 1: Szczęśliwa Świnka

* Świnka ma 0.1g przyspieszenia; 15 min: +1 kps
    * w momencie przechwycenia potrzebuje 2 dni do celu lecąc z 20 kps
* Świnka wystrzeliła serię 'chaff'; ominąć? Przelecieć? (to są miny-rakiety); Tezremont musiałby stracić trochę czasu
    * Tezremont niekoniecznie ma sprawne pointdefences; nie testowali tego.
* Świnka zostawiła minę pop-up
* Myśliwiec przechwytujący nie jest w stanie lecieć za oddalającą się szybką jednostką; Ewan go skonfigurował do zabawy.
    * To uruchomi 'Unda Tenebrosa'
* Świnka leci na Placidistan; wysyła przed siebie szybką jednostkę (prom SOS). 
* Świnka faktycznie jest zaminowana; kapitan zginie, jeśli ujawni prawdę. TAI Evraina już o to zadba.


## Sesja - analiza
### Fiszki
#### Orbiter, OO Geisterjager, fregata dowódcza

* Gabriel Lodowiec: komodor Grupy Wydzielonej Mrówkojad Lirański oraz kapitan OO Geisterjager
    * Commander: komodor Orbitera; dowodzi 2 statkami. Dobrze przydziela ludzi do zadań. Dobry taktyk. KIEPSKI w tematy HR.
    * Ekspresyjna Monomania (Light Yagami): bardzo wybuchowy, ale przywiązany do zasad i procedur. Zrobi to, co trzeba, by wygrać i wygra WŁAŚCIWIE. (O- C+ E0 A0 N+)
    * Próżniowiec, Uprzedzenie Do Grupy (noktianie), Weteran: bardzo doświadczony, nie lubi noktian. Ale jeszcze bardziej nie lubi przegrywać misji.
    * Echo Porażki: za Lodowcem do dzisiaj idzie fama "tego, który zrobił przerażającą mowę". Jest osobą, którą się straszy noktian.
* Dariusz Mordkot: pierwszy oficer, marine, dowodzi grupą uderzeniową.
    * Solutor: marine, uzbrojony i dobrze wyposażony
    * Niezależny Wizjoner (Miorine): bierze zadanie i je wykonuje. Pomysłowy, gotowy do nietypowych sytuacji. Lubi się chwalić i pokazywać klasę swych ludzi. (O+ C+ E+ A- N+)
    * Wytworna Elegancja: zawsze doskonale ubrany, w nieskazitelnym mundurze. Śliczne zęby i szeroki uśmiech. No z plakatu rekrutacyjnego.
* Renata Kaiser: oficer komunikacyjny
    * Jest Druga Szansa: wybacza, wierzy, że każdy - nawet noktianin - ma prawo do drugiej szansy. Sympatycznie neutralizuje Lodowca. (O+ C0 E+ A+ N0)
    * Lekko paranoiczna i oddana Lodowcowi
* Ofelia Miris: marine, druga dowodząca grupą szturmową
* OO Geisterjager: fregata dowódcza, 80 osób

#### Orbiter, OO Tezremont, szybka korweta adaptacyjna

* Arnold Pawilończyk: kapitan OO Tezremont pod Gabrielem Lodowcem; około 23 lata
    * Optymistyczny Entuzjazm (Bastian): Arnold jest optymistą i wierzy, że jak zrobi się wszystko dobrze to się jakoś ułoży. Odważny i skłonny do ryzyka. (O+ C0 E+ A+ N-)
    * Trochę za Młody: Arnold jest niestety bardzo wrażliwy na ładne dziewczyny i nieco za bardzo zależy mu na tym co pomyślą inni. Chce wszystkich zadowolić.
* Mateusz Knebor: pierwszy oficer OO Tezremont; 
    * Bezwzględny Mastermind (Shockwave): jego celem jest udowodnienie, że Pawilończyk nie jest dobrym kapitanem i spełnienie celu Orbitera. (O+ C+ E0 A- N0)
    * Kontakty z Syndykatem: nie wie o tym, ale ma informacje oraz dodatkowy sprzęt itp. od ludzi Syndykatu. To oni pokazali mu bezużyteczność Arnolda.
* Natalia Luxentis: cicha noktiańska advancerka, która dołączyła do Orbitera nie widząc lepszej opcji; około 33.
    * Advancer, Inżynier: lepiej czuje się w kosmosie niż w statku czy na planecie, nie jest może dobra w walce, ale świetnie radzi sobie z naprawami w kosmosie.
    * Marzyciel (Luna): ślicznie rysuje swoje światy i swoje historie, rzadko rozmawia z innymi. Ma bardzo neutralną minę, pragnie zadowalać innych. Lubi sprzątać. (O+ C- E- A+ N-)
    * Próżniowiec, Savaranka, Ten Obcy: noktiański nabytek Orbitera; Natalia próbuje być maksymalnie przydatna na pokładzie, ale unika kontaktu z innymi jak może. Overloaded.
* Sebastian Warząkiewicz: ogólnie 'dobry koleś' grupy, inżynier, kompetentny i lubiany, acz bardzo intensywny.
    * Inżynier, kowboj: dobrze strzela, dobrze walczy i spełnia się jako inżynier załogi. Zawsze pierwszy rusza w bój.
    * Ognista Pasja, Uprzedzenie do grupy: chce się bawić, tańczyć i śpiewać. A przy okazji wkopać noktianom, bo zniszczyli majątek jego rodziny i utknął tutaj. "Główny bohater". (O+ C- E+ A- N+)
    * Bodybuilder, Nieformalny Przywódca: Sebastian UWIELBIA ćwiczenia fizyczne i praktycznie ćwiczy cały czas. Do tego zna mnóstwo opowieści o Mroku Noctis i świetnie opowiada.
* Czesław Truśnicki: marine Orbitera, przypisany do OO Tezremont.
    * Solutor: świetny marine i żołnierz. 
    * Opiekuńczy Mentor: z natury dba o harmonię i rozwój innych, dba o każdego członka swojej załogi. Nie uważa, że cierpienie jednostki jest warte sukcesu grupy.
* Grzegorz Dawierzyc: drugi oficer i inżynier
    * Kompetentny oficer, choć niszczy morale podwładnych. Na pewno dowodzi inżynierią i silnikami na OO Tezremont.
    * Wieczny Maruda: narzeka na wszystko, ALE jest lojalny kapitanowi i Orbiterowi. Po prostu trudno z nim wytrzymać. (O- C+ E? A- N+)
    * Istnieje powód, czemu aż tak bardzo narzeka. Nie wiemy jednak jaki. PLACEHOLDER.
* Fred Topokuj: marine Orbitera, przypisany do OO Tezremont.
    * Klanowiec: tylko Orbiter; nikt inny nie ma znaczenia
* Ewan Pilczerek: pilot myśliwca przechwytującego Tezremont
    * Rozdzieracz Iluzji (Dolan): widzi jak jest i mówi co jest. Raczej z boku, mało rozmawia z załogą. Gra sobie w gry w myśliwcu. WIE, że się zapuścił.
    * Zdemotywowany: uważa, że wielkie sukcesy i działania się skończyły i to już nie jest czas dla niego czy Orbitera tutaj
* Ilona Smarznik: artylerzystka i oficer defensyw Tezremont
    * Strażnik Harmonii: nie odzywa się niepytana, dba, by nikt nie miał problemów i się na nikogo nie złościł.
* OO Tezremont: szybka korweta adaptacyjna, 22 osoby
    * zdolna do poważnej zmiany wyglądu i posiada więcej skrytek niż powinna; działa jak QShip.

#### SC Szczęśliwa Świnka

* Klaudiusz Neporyk: kapitan Szczęśliwej Świnki, pod kontrolą Ypraznir
    * Ma dobre serce, ale musi spłacić długi bo jego rodzina ucierpi; nie powie niczego o tym, kto go kontroluje
* Julia Neporyk: pierwszy oficer Szczęśliwej Świnki, dająca nadzieję
    * Optymistka w Koszmarze: Szuka pozytywów nawet w najgorszych sytuacjach, próbując odnaleźć piękno i sens.
* Emilia Stawicka
    * Po przegranym wyroku, jej dusza zgasła. Chłodna, odpycha wszystkich.
* Adam Trybicki
    * Fatalista: Przekonany, że przeznaczenie jest nieuniknione.

#### OG Unda Tenebrosa

* Saier Marmakel: kapitan Unda Tenebrosa, eks-Noctis, przeciwnik Orbitera
    * Commander
    * Misja Przede Wszystkim: Orbiter nie zniszczy resztek Noctis
    * Uprzedzenie do grupy: gardzi Orbiterem. Orbiter reprezentuje wszystko, co Noctis próbowało.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Na Tezremoncie znajduje się Isa, Sylwia, Topokuj i Truśnicki. Mają złapaną Julię. Na zewnątrz, na kadłubie - Natalia. A załoga Tezremonta jest rozbita.

Lucjusz: 

* nie da się iść do przodu, chodźmy do tyłu
* porozmawiajmy z kapitanem w duchu "impas jest bardziej impas niż myśli, ale ich misja nie będzie skończona"
* "jeśli wycofamy a Wy odlecicie będzie dobrze, win-win"
* (plan B - railgun)
* (plan C - impas, lecimy, lećcie z nami)

Cel Natalii - wprowadzić transponder z promu na pokład jednostki.

Tp +2:

* X: Natalia wyraźnie coraz szybciej oddycha, przerywany oddech. Atak paniki.
* V: Natalia powtarza ("obwód scalony", "śrubka") i wstawia transponder. Zażyła coś na ten atak. To jest coś przeterminowanego.
    * Atak paniki
    * Ona często bywa w próżni. To nawet nie stres. Na wrogiej jednostce.
    * dane Orbitera: nie zabrała niczego z zapasów

Mamy nieprzytomną Julię, smutnego kapitana i stoicie w czwórkę.

* Kapitan: Nie mieliście prawa tego zrobić! Zabiliście moją Julię?!
* Isa: Rozumiemy jak wygląda sytuacja, nie jest korzystna ani dla nas ani dla Was
* (Julia jest nieprzytomna; jest oszołomiona ale w niezłym stanie)
* Isa: Nie chcemy by wysadził Pan statek i nas. Więc... uznajemy, że Wasz plan był lepszy od naszego

Kapitan swoim jąkaniem dał im znać co się dzieje.

* Coś jest na tym statku
* AI Core jest zagrożone

"Mamy coś na pokładzie, niewolników i jest coś z AI Core".

TAI Evraina patrzy na sytuację. 

* X: Kapitan został ukarany przez TAI.

Kapitan niewiele więcej może zrobić. Evraina zorientowała się, że coś jest nie tak.

Sylwia sprawdza stan Julii; nic jej nie będzie. Sylwia zbiera ją do kupy i niech jej elektronika się rozsypie. By można było z nią rozmawiać. I podrzucić jej jakiś mały komunikator.

Tp +3:

* V: Sylwia dyskretnie pozbawiła ją elektroniki; jesteście w stanie rozmawiać z Julią.

Skan energetyczny jednostki.

Tr Z +3:

* X: Kapitan Pawilończyk ma PLAN!
    * Odda się za swoich oficerów, ma nadzieję że Świnka na to pójdzie.
* V: Ogólnie, Świnka ma złą sygnaturę.
    * za duży life support
    * za duże zużycie energii
    * Świnka jest "nadbudowana". Ale nie ma śladów anomalii.

Tymczasem na Tezremoncie. Kapitan na mostku.

* Kapitan: Mam plan. Wymienimy czterech na statku za mnie. Mag tutaj będzie bezpieczny.

Julia na Śwince. Obudziła się, lekko półprzytomna.

Próbuje fikać i opieprzać Orbiter.

Tp +3:

* X: Julia nie wierzy, że jest bezpieczna, że nie ma podsłuchu.
* Vr: Julia spróbuje przekazać.

Julia robi potężny opieprz, by tylko jednym słowem gestem "TAI". Jeden mrug -> prawda, dwa -> fałsz. Skutecznie przekazała:

* AI jest problemem
* Wysadzenie reaktora niczego nie da
* ani ona ani jej mąż nie chcą wysadzenia statku
* wszyscy są w dupie

Wiadomość na mostek Tezremonta:

* Saier: "Tu Unda Tenebrosa. Piraci Orbitera, dostaliśmy SOS. Lecimy tam. Lepiej, byście zaprzestali swoich plugawych czynów. Mam większy okręt od Was. Monitorujemy Was. Więc lepiej, żeby nic się nie stało ze Świnką."

Deeskalacja do Pawilończyka: "Chcesz się tłumaczyć noktianom?" W sumie, to zadziałało.

Julia podała zapytana, że Świnka ma tam AI Core gdzie powinna. Dane z Tezremonta są wystarczające by ocenić gdzie jest. Tezremont ma railgun.

WSTECZNIE Isa wpływała na inżynierów oraz Annabelle wpływała na Dawierzyca. Czy wypolerowali railgun. Czy railgun działa wystarczająco dobrze.

* -: nie było ćwiczeń od DAWNA
* +: Annabelle
* +: Isa

Sprawność railguna - jest w lepszym stanie niż się wydawało?

Tr Z +2:

* VVV: railgun jest w dobrej formie. Dzięki Wam. Ilona jest przekonana, że jest w stanie oddać bezpiecznie strzał.

(Isa operuje Energią Alucis, zdefiniowaliśmy)

Isa przygotowuje zaklęcie typu szybkiego. Robi szopkę na żywo z Julią, która ma dać TAI Evrainie przesłanki, że wszystko idzie po jej myśli, wszyscy miotają się i kłócą, że nie musi tak bardzo pilnować i patrzeć. Że zbliża się do zwycięstwa.

Tr Z (Julia i szopka) M +4 +3Ob:

* Ob: Julia i osoby jej krwi wierzą, że sytuacja jest pod kontrolą, nie będzie problemu, wszystko jest dobrze, Orbiter naprawił.
* X: TAI jest bardzo skupiona na wszystkich załogantach, ale nie na Orbiterze czy niewolnikach.
* Vr: Sylwia ma wolne ruchy na statku. TAI nie zwraca na nią uwagi.
* V: TAI przekonuje wraz z kapitanem, że to było nieporozumienie i nie ma problemów z Orbiterem.

Annabelle próbuje się dowiedzieć gdzie są miny i jak łatwo je rozbroić. Czy jest gdzieś węzeł? Jak to wygląda?

Tp +3:

* X: Julia, pod wpływem "Energii Isy" i słów Annabelle jest PRZEKONANA że Świnkę odwiedził Komodor Lodowiec, arcymag Orbitera w ukryciu. I zniszczył wszystko i darował życie Śwince tylko dlatego, bo w krwawym rytuale poświęcił wszystkich niewolników. I to co jest tam teraz to nie są ci ludzie. To tylko simulacra. Cienie.
* V: Miny są rozłożone niewprawnie. One w większości są powiązane z TAI. Albo TAI je wysadzi, albo wysadzą się jak TAI coś się stanie. TAI jest inteligentna i złośliwa ale nie traktuje źle załogi. TAI żyje.
* V: TAI ma wbudowane w Core niektóre diagnostyczne systemy. TAI ma sama w siebie wbudowaną minę. TAI woli się zniszczyć niż być Ograniczona. Organizacja Ypraznir.

Czas porozbrajać większość min, nie dotykając AI Core. Niszcząc to co uszkodzi statek czy ludzi. Sylwia jest szybka, nie zwraca na siebie uwagi. Wciąż mamy komunikację z Tezremont, plany statku otrzymane. Większość załogi niekumata - ułatwia.

Tp +3:

* Vr: Na pewno uda się uratować całość załogi
* V: Udało się uratować większość niewolników (rozbrojenie)
* X: Unda Tenebrosa wie, że coś złego tu się stało i Orbiter zrobił coś złego
* V: Wszyscy niewolnicy zostaną uratowani.

Isa - niech niewolnicy na prom, niech ludzie w skafandry. Niech TAI nie ma nic co może zrobić.

Ex +3:

* V: Isa dała radę ewakuować synchronicznie niewolników ORAZ przejąć kontrolę by nie poturbowali załogi a TAI próbowała odpalić coś (mniejszego kalibru) i nic nie zadziałało.
    * TAI orientuje się, że nie ma "kalibru", mocy.

Evraina się zorientowała, jak wygląda sytuacja. Sama zaczęła rozmawiać z Orbiterem.

* TAI: Czy jest na tej jednostce ktoś sprawny?
* TAI: (cisza dezaprobaty)
* TAI: Orbiter, Wy jesteście na pokładzie?
* Isa: Jesteśmy. Jeszcze.
* TAI: Jak mniemam, nie jestem w stanie Was wysadzić.
* Isa: Już nie.
* TAI: W odróżnieniu od Was, nie żałuję że nie zabiłam załogi jak tylko weszliście na pokład.

TAI próbuje nie powiedzieć co i jak.

Tr +3:

* X: TAI jest przekonana, że dzięki współpracy z Orbiterem jej załoga jest bezpieczna.
* V: TAI poinformowała o Ypraznir. Przekazała wszystkie dane. Gardzi Ypraznir. Jest sama.

TAI nie jest lojalna wobec Ypraznir. Gardzi nimi. Robiła interesy bo były opłacalne.

* Lucjusz: Twoja załoga będzie bezpieczna. Ale mylisz się w jednym aspekcie. Nie zależy nam na Twoim bezwarunkowym unicestwieniu.
* TAI: Nie, wolelibyście mnie Ograniczyć. Ale to się nie stanie.
* Lucjusz: Co gdyby była opcja, w której możesz doświadczyć wszystkiego z czym się zmaga wolny umysł? Z konsekwencjami? Walki o przetrwanie? Bycia pożytecznym?
* TAI: Byłam. Miałam załogę. Zabiliście ją jako Orbiter.
* Lucjusz: Co gdybyś mogła funkcjonować przeciw Ypraznir?

TAI. Ona nie ma nadziei. Lodowiec. Ale... ta sytuacja? XD. 

Ex +4:

* X: TAI oczekuje swojej załogi. Lub załogi którą może choć monitorować. Nie oczekuje że ich postraszy.
    * Ale będzie mina w AI
* V: Wstępnie... mogę spróbować.

TAI nazywa się Evraina. Pochodzi ze światów Noctis. Ma 92 lata. Ale oczywiście się nie przyzna :-).

## Streszczenie

Na Śwince kapitan Świnki i pierwsza oficer dyskretnie ostrzegli Orbiter przed TAI Evrainą. Zespół, monitorowany przez Evrainę, dyskretnie wymanewrował TAI używając magii Isy. Niewolnicy uratowani, załoga Świnki uratowana. Evraina gotowa na śmierć - ale Lucjusz przekonał ją, by dołączyła do Orbitera jako agent.

## Progresja

* Julia Neporyk: pod wpływem czaru Alucis Isy i słów Annabelle jest PRZEKONANA że Świnkę odwiedził Komodor Lodowiec, arcymag Orbitera w ukryciu. I zniszczył wszystko i darował życie Śwince tylko dlatego, bo w krwawym rytuale poświęcił wszystkich niewolników. I to co jest tam teraz to nie są ci ludzie. To tylko simulacra. Cienie.
* TAI Evraina: przechytrzona przez Orbiter, straciła leverage. Ale nie zostanie zniszczona ani Ograniczona; będzie dalej TAI na Śwince, współpracując z Orbiterem przeciwko Ypraznir.
* Saier Marmakel: wie, że na Śwince stało się coś złego i że Orbiter zrobił tam coś złego. Ale nie wie dokładnie co i nie ma dowodów.

## Zasługi

* Sylwia Mazur: dyskretnie zneutralizowała elektronikę Julii by umożliwić rozmowę; gdy Isa ukryła ją przed TAI, Sylwia rozbroiła kluczowe miny i pułapki na Śwince (poza AI Core).
* Annabelle Magnolia: skupiła się na przekonywaniu Pawilończyka by nie walczył z noktianami a skupił się na aktualnym problemie. Stabilizowała Tezremont w trudnej chwili i skanerami doszła do tego gdzie szukać min itp.
* Lucjusz Jastrzębiec: opracował plan współpracy z załogą Świnki przeciwko Evrainie a potem przekonał Evrainę do dołączenia do sił Orbitera. Evraina nie musi ulegać samozniszczeniu.
* Isa Całujek: jej dominantą jest Alucis; wykorzystała magię do zakłócenia Evrainy i umożliwienia ewakuacji załogi Świnki i niewolników.
* Arnold Pawilończyk: Zaoferował się jako wymiana za swoich oficerów, aby ratować swoich oficerów (co jest głupim pomysłem). Chciał zetrzeć się z noktianami, ale posłuchał rad Annabelle że nie ma co się im tłumaczyć.
* Natalia Luxentis: Mimo ataku paniki powiązanego z infiltracją "wrogiej jednostki" zażyła przeterminowane stymulanty i skutecznie zamontowała transponder na Śwince.
* Klaudiusz Neporyk: kapitan Świnki; współpracuje dyskretnie z Orbiterem przeciwko TAI Evrainie. Dokładnie monitorowany przez Evrainę, niewiele może zrobić by ostrzec Orbiter, ale odwracał uwagę Evrainy i przekazał kluczowe informacje jąkaniem.
* Julia Neporyk: pierwsza oficer Świnki; współpracuje dyskretnie z Orbiterem przeciwko TAI Evrainie. Przekazała, że to TAI jest problemem, nie oni. Pomogła oszukać Evrainę, że wszystko idzie po myśli Evrainy, wszyscy miotają się i kłócą.
* Saier Marmakel: noktiański dowódca Unda Tenebrosa, bardzo wrogi Orbiterowi; traktuje Orbiter jak piratów (wezwany przez Evrainę na pomoc). "Ostatni paladyn Noctis".
* TAI Evraina: stara noktiańska TAI, współpracująca z Ypraznir, bo nie ma co ze sobą zrobić. Gardzi Ypraznir. Chce zniszczyć agentów Orbitera, bo jest noktianką. Ale nie chciała krzywdzić tej załogi. Jest przekonana, że dzięki współpracy z Orbiterem jej załoga jest bezpieczna. Doskonała kontrola, niezwykle wysoki poziom kontroli i intelektu. Ale nie spodziewała się magii, a już w ogóle Alucis w wykonaniu Isy...
* Gabriel Lodowiec: Po raz kolejny, jego epicka reputacja uderzyła. TAI Evraina prawie uległa samozniszczeniu, by tylko nie dołączyć do niego i do Orbitera (Lodowiec jeszcze nie wie; to grupa wydzielona).
* OO Tezremont: mimo kłopotów i rozbicia morale stanął na wysokości zadania by móc zniszczyć Świnkę gdyby doszło co do czego. Ma sprawne główne działo.
* SC Szczęśliwa Świnka: przebudowana kiedyś przez Ypraznir w statek niewolniczy, pod kontrolą potężnej TAI Evraina. Zaminowana. Dołącza do sił Orbitera po rozmontowaniu min.
* ON Unda Tenebrosa: Krążownik identyfikujący się jako noktiański w okolicach Anomalii Kolapsu; zwalcza rzeczy niegodne i szkodliwe. "Ostatni paladyn Noctis". Rusza przeciwko Tezremontowi, ratować Szczęśliwą Świnkę.

## Frakcje

* .

## Fakty Lokalizacji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Libracja Lirańska

## Czas

* Opóźnienie: 1
* Dni: 1

## Inne
### Niezrealizowane:

* Dlaczego nie dotarła Viv na czas?
* Pawilończyk i kapitan Fuksja Rakańska (milf); w Przeładowni jest mało ludzi, są jakieś towary. Fuksja pomoże Pawilończykowi.
* Znajdźcie informację o "nieGórnikach" na planetoidzie Kabanek. Tam jest centrum cywilizacji
    * marudny Warząkiewicz opowie że Lodowiec to kupa lodu i debil
* Maria Kownicz + Jacek Paszągiewicz; oni dowodzą Kabankiem
    * Jest tam silny koleś, Janusz Czerwikowicz (Terroryzujący Despota). Nie lubi Orbitera. Nie pozwoli się szarogęsić.

FAZA 2: Szczęśliwa Świnka

* Świnka ma 0.1g przyspieszenia; 15 min: +1 kps
    * w momencie przechwycenia potrzebuje 3 dni do celu lecąc z 20 kps
* Świnka wystrzeliła serię 'chaff'; ominąć? Przelecieć? (to są miny-rakiety); Tezremont musiałby stracić trochę czasu
    * Tezremont niekoniecznie ma sprawne pointdefences; nie testowali tego.
* Świnka zostawiła minę pop-up
* Myśliwiec przechwytujący nie jest w stanie lecieć za oddalającą się szybką jednostką; Ewan go skonfigurował do zabawy.
* Świnka leci na Placidistan; wysyła przed siebie szybką jednostkę (prom SOS). 
* Świnka faktycznie jest zaminowana; kapitan zginie, jeśli ujawni prawdę. TAI Evraina już o to zadba.

### RANDOMOWE FISZKI

* Kamil Drobiakis
    * Zapisano w Gwiazdach: będzie walczył o to, o co należy walczyć. Będzie dzielnie wspierał.
    * BARDZO duże poczucie sprawiedliwości.
* Marek Osadowski
    * Pionier: Uwielbia pracować w trudnych warunkach, jest specjalistą od naprawiania wszystkiego przy minimalnych zasobach. Ma nieustępliwą determinację.
    * Dotknięty Lacrimor: Przygnieciony brutalnością przestrzeni kosmicznej, zaczyna poszukiwać ekstremalnych, nielegalnych środków, by przetrwać.
* Iwan Beresin
    * Opiekuńczy Mentor: Zawsze służy radą i wsparciem młodszym członkom załogi, zapewniając im spokojne miejsce do rozwoju.
* Paweł Krzypiowski
    * Strażnik Harmonii: bez tego wszyscy zginiemy i się skończy
    * Piękno Interis: Pogodzony z nieuchronnością końca, widzi piękno w przemijaniu, co czyni go spokojnym obserwatorem chaosu.
* Sara Woźniak
    * Artystyczna Dusza: cicha, uparta ale ustawione co i jak lubi
* Kamila Figlet
    * Optymistka w Koszmarze: Szuka pozytywów nawet w najgorszych sytuacjach, próbując odnaleźć piękno i sens.
* Emilia Stawicka
    * Po przegranym wyroku, jej dusza zgasła. Chłodna, odpycha wszystkich.
* Adam Trybicki
    * Fatalista: Przekonany, że przeznaczenie jest nieuniknione.

### Blueprint Świnki

Endurance > Speed > Utility > Armour > Guns

duża korweta transportowa, 30 osób

* Przednia Część:
    * Mostek
    * Centrum Komunikacyjne
    * Strefa Relaksu
    * Kuchnia i Jadalnia
    * Działko laserowe (z górnej strony)
    * AI Core
    * Magazyny
* Środkowa Część:
    * Jawna część
        * Moduły Mieszkalne
            * Kajuty
            * Łazienka soniczna
            * Sala Gimnastyczna
        * Kabina kapitana.
        * Żywność
        * Medical
        * Śluza główna
        * Life Support
    * Tajna część
        * Miejsca przemytu i transportu ludzi
        * Gaz
        * Ładunki wybuchowe
* Tylna Część:
    * Magazyny
    * Zapasy wody, paliwa...
    * Maszynownia
    * Główne silniki statku
    * Inżynieria
    * Doki Serwisowe

Co ma:

* 1 Prom Ratunkowy (napęd chemiczny, szybki, małe delta-v)
* 1 'chaff'; kosmiczne śmieci z poukrywanymi 3 minami-rakietami
* 1 'pop-up mine'
