## Metadane

* title: "Córka Mimika"
* threads: nemesis-pieknotki
* gm: żółw
* players: sabrina, markus, onyks, kamila_13, mila

## Kontynuacja
### Kampanijna

* [190522 - Inwazja mimików](190522-inwazja-mimikow)

### Chronologiczna

* [190522 - Inwazja mimików](190522-inwazja-mimikow)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

* .

### Tory

* niżej

Sceny:

* Scena 1: .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

W Sensoplex nadal znajdują się cenne rzeczy. Luxuritias próbuje tam wejść, ale ogólnie próbują zrobić to legalnie - z pozwoleniami, uprawnieniami i wszystkim. Tymczasem Senetis jest zainteresowane danymi z Sensus więc wysłali swoich agentów by szperali i poszukiwali. Często materializujące się Koloseum jednak nie pomaga.

Oddział Szarych Szakali, niewielka grupa najemników, chciała pomóc Luxuritias i się im podlizać. Tak więc próbowali odepchnąć Senetis - niech Luxuritias spokojnie to może obserwować i badać. Zaczęli zastawiać pułapki, destabilizować Sensoplex i ogólnie dbać o to, by jednak nie doszło do jakichś badań.

Zespół stwierdził, że tak być nie może. Zbyt wiele ludzi może ucierpieć w rozgrywkach korporacyjnych a jak dojdzie do wojny to już w ogóle. Tak więc zeszli przejrzeć te pułapki, unieczynnić je i pozbierać dowody. To powinno uszkodzić Szakale i zatrzymać niewłaściwość.

Tyle, że Zespół wpakował się na jedną z pułapek. Niejaki Dariusz Puszczak, sojusznik Zespołu, wpadł do dziury. Prawie zginął, ale wpadł na nieznajomą kobietę; o dziwo, nie zrobił jej szczególnej krzywdy mimo, że stracił przytomność.

Kobieta była nieufna i przy pierwszej okazji próbowała zwiać. Zespół ją złapał i zaniósł do siedziby jednego ze swoich agentów. Zamknęli ją w piwnicy i rozpuścili wici - kim ona jest, komu na niej zależy. Tymczasem okazało się, że nie mają śladów Szakali. Nie mają niczego. Tak więc wysłali agentów ludzkich by udawali podsłuchiwanie Szakali i wysłali agenta eterycznego, by ów faktycznie podsłuchał plany Szakali. Udało się - doszli do tego, że Szakale chcą wprowadzić tam brudną bombę. Coś, co Skazi teren biologicznie. W ten sposób Senetis nie będzie zainteresowane wejściem na ten teren przez pewien czas, co kupi czas Luxuritias.

Tymczasem do rezydencji Zespołu przybył detektyw Żuwaczka. Powiedział, że kobieta w piwnicy jest anomalną formą mimika symbiotycznego. Jest bardzo niebezpieczna i ktoś ją na pewno kupi. Zespół jednak nie jest przekonany; nie robiła nic złego. Żuwaczka smętnie odszedł (sprzedał informacje o mimiku Senetis).

Zespół zbadał córkę mimika; udało się im dojść do tego, że owa mimiczka nie do końca wie, czym jest, ma własności wampiryczne i amplifikacyjne, nie ma złej woli i ogólnie jest całkiem sympatyczna. Ale Senetis pokroi ją na plasterki na stole operacyjnym.

Pojawiły się dwa dominujące wątki, oba nierozwiązywalne: jak zatrzymać Szakale i jak zatrzymać Senetis, którzy będą chcieli przechwycić viciniuskę bez praw.

W odpowiedzi przyszedł im do głowy chory plan. Wiktor Satarail. Jedyna istota zdolna do zaopiekowania się viciniuską - i jednocześnie za to może pomóc im obronić teren. Jak jednak przekonać Wiktora? Tu z pomocą przyszedł im fakt, że w ich zespole znajdował się jeden vicinius, w formie latającego gada (smoka). Wysłali tego viciniusa na Trzęsawisko Zjawosztup na negocjacje z Wiktorem.

Trzęsawisko nie było delikatne. Smoczyca prawie utonęła, ale Wiktor ją uratował. Gdy smoczyca wyjaśniła, że mają viciniuskę i chcieliby by Wiktor mógł ją uratować, Wiktor zapewnił smoczycę, że trafiła w dobre miejsce. Smoczyca poprosiła o jakieś rzeczy umożliwiające zatrzymanie Szakali - by nie podłożyli brudnej bomby. Coś, co sprawi, że Sensoplex nie stanie się polem bitwy. Wiktor się zgodził. Dostarczył odpowiednie roślinki i powiedział smoczycy, że jak to się skończy - niech viciniuska trafi do niego.

Tymczasem do bazy Zespołu wpadli magowie z Senetis chcący odkupić viciniuskę. Lokalny magnat ich oszukał - powiedział, że viciniuskę już ludzie z Senetis kupili i ona poszła z nimi. Agenci Senetis byli zaskoczeni - poprosili o możliwość przeprowadzenia badań. Magnat się zgodził.

Gdy Szakale weszły do Sensoplex, czekała na nich pułapka Sataraila. Zostali spętani i unieszkodliwieni. Zespół - kosztem kilku swoich lojalnych agentów - zrobił rajd na biura Szakali by zdobyć niepodważalne dowody by Szakale na pewno się nie wykręciły z całej tej sprawy.

Cały Zespół potem próbował ukryć aurę w rezydencji. Udało im się zamaskować aurę jako aurę Wiktora Sataraila. W ten sposób gdy Senetis przyszli, to "wykryli" że to Satarail podał się za nich i odkupił viciniuskę. Automatycznie przestali szukać, nie miało to sensu.

Smoczyca pod osłoną nocy przeniosła viciniuskę do Sataraila.

**Sprawdzenie Torów** ():

* .

**Epilog**:

* Rozprzestrzeniła się plotka, że w Sensoplex jest skarb, coś super ważnego - dlatego Senetis i Satarail o to rywalizują
* Szakale zostały rozwiązane - pojawiły się liczniejsze grupki niewielkich najemników. Jeszcze gorzej.
* W kopalni Coś Znaleziono, coś powiązanego z Saitaerem.

## Streszczenie

Córka Mimika okazała się być stosunkowo niegroźną kobietą amplifikatorem. Znalazła się w Sensoplex, ale została wydobyta stamtąd przez Zespół próbujący nie dopuścić do uciemiężenia agentów Senetis przez Szakale. Córka Mimika trafiła pod opiekę Wiktora Sataraila, Podwiert stał się grobem dla Szakali (rozwiązało ich) oraz Sensoplex jest okryty sławą miejsca tajemniczych skarbów.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Wiktor Satarail: pomógł Córce Mimika i dostał ją pod swoją opiekę; dodatkowo wzbudził grozę w Podwiercie (bo co tym razem knuje)
* Dariusz Puszczak: aktywista antywojenny i antykorporacyjny; chciał ratować agentów Senetis, złamał nogę wpadając na Córkę Mimika i skończył nieprzytomny przez cała sesję
* Antoni Żuwaczka: konsekwentnie szuka Córki Mimika, znalazł ją i sprzedał Senetis.
* Mirela Satarail: Córka Mimika; wampir energetyczno-amplifikacyjny. Zagubiona i o dobrych intencjach, trafiła pod opiekę Wiktora Sataraila.

## Plany

* .

### Frakcji

* Luxuritias: próbują przejąć Sensoplex w Podwiercie sposobami politycznymi i legalnymi
* Senetis: próbują zbadać Sensoplex w Podwiercie

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Sensoplex: miejsce zastawiania pułapek i innych takich.
                                1. Dolina Biurowa: sporo firm, biur itp. Tam siedzibę miały Szakale. Tam się je podsłuchiwało i infiltrowało.
                                1. Osiedle Sosen: miejsce, gdzie Zespół ma rezydencję i centralę dowodzenia. Bogatsze i lepsze osiedle niż Leszczynowe.
                        1. Trzęsawisko Zjawosztup: miejsce, gdzie ostatecznie trafiła Córka Mimika, pod osłonę Wiktora Sataraila

## Czas

* Opóźnienie: 10
* Dni: 3
