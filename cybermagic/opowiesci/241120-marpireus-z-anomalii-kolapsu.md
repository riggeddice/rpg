## Metadane

* title: "Marpireus z Anomalii Kolapsu"
* threads: iluzja-kontroli-anomalii-kolapsu
* motives: energia-alteris, energia-lacrimor, the-protector, the-doomed, arogancja-mlodosci, anomalia-rzadka, dom-w-plomieniach, dziwna-zmiana-zachowania, niekompetentna-pomoc
* gm: żółw
* players: random_ala_241120, random_aga_241120, random_tomek_241120

## Kontynuacja
### Kampanijna

* [241120 - Marpireus z Anomalii Kolapsu](241120-marpireus-z-anomalii-kolapsu)

### Chronologiczna

* [241113 - Tezremont, rozwarstwiony](241113-tezremont-rozwarstwiony)

## Plan sesji
### Projekt Wizji Sesji

* INSPIRACJE
    * PIOSENKA WIODĄCA
        * [Emilie Autumn - Take the pill](https://www.youtube.com/watch?v=PWheyBQVQyQ)
        * "We are not happy with your progress | You're not yet considered 'sane' | If these pills are not effective | We'll electroshock your brain"
        * "Be careful what you say... | Today could be your day... | You no longer rule your body | You no longer own those rights"
        * Transformacja z biegiem czasu, która wpływa na załogę Marpireusa
    * Inspiracje inne
        * Alien in the Darkness, ponownie. Tym razem - szaleństwo które pojawiało się po działaniach Fleur.
* Opowieść o (Theme and Vision):
    * "_Anomalia Kolapsu jest po prostu zbyt niebezpieczna by móc z niej korzystać bezpiecznie_"
        * Od pewnego czasu Marpireus, salvager dobrej klasy wykorzystywał Anomalię Kolapsu; niestety, ten jeden raz trafił na coś skrajnie niebepiecznego.
        * Anomalia wpływa na umysły i zmienia logikę agentów Marpireusa. Daje im inny, obcy świat. Ludzie nie są w stanie niczego zrobić przeciw tej manifestacji.
        * Kapitan prowadzi Marpireus w pustkę, cały czas przyspieszając w kierunku na nicość
    * "_Jak daleko się posuniesz by uratować wszystkich?_"
        * Tylko dzieci są w stanie zachować odporność na Anomalię magiczną. Tylko one widzą świat takim-jaki-jest.
        * JAK BARDZO dzieci spróbują uratować wszystkich? Zabiją kogoś? Unikną?
    * "_Orbiter pomoże każdemu, nawet ogromnym kosztem dla siebie._"
        * Lodowiec pomoże. Nieważne, że kosztem swoich surowców. Zaryzykuje.
    * Lokalizacja: Okolice Anomalii Kolapsu
* Motive-split
    * the-protector: zbiorowa agenda większości NPC na pokładzie Marpireusa; próbują uratować statek i chronić dzieci przed zagrożeniami.
    * the-doomed: zbiorowa agenda SC Marpireus jako statku oraz Anomalii. Próba walki z Ruiną, ale jednocześnie zbliżająca się Ruina jako anomalizacja statku.
    * energia-alteris: ludzie i świat zaczynają się przekształcać pod kosztem Eternal Nightmare (lacrimor-alteris).
    * energia-lacrimor: ludzie zaczynają "widzieć świat" jako koszmarny i siebie jako bohaterów; 'reality becomes nightmare'.
    * arogancja-mlodosci: 3-4 młode dzieciaki poza postaciami graczy mają przekonanie, że są w stanie wygrać tą sesję używając "potęgi miłości" czy swoich skilli. Kończy się tragicznie.
    * anomalia-rzadka: anomalne rzadkie przedmioty Alteris-Lacrimor, które wpływają na całą załogę i zmieniają ich życie, pamięć, przeszłość.
    * dom-w-plomieniach: SC Marpireus, doskonałej klasy statek salvagerski, który podczas salvagingu zdobył nieświadomie potężną Anomalię wypaczającą umysły członków załogi.
    * dziwna-zmiana-zachowania: stopniowa zmiana zachowania załogi Marpireusa, którzy działają RACJONALNIE na bazie tego co pamiętają i co widzą. Ale niestety widzą inne rzeczy niż były.
    * niekompetentna-pomoc: jedyne postacie dostępne na sesji są albo częściowo pod wpływem Anomalii (i nie wiedzą co się dzieje) albo są dziećmi i nie wiedzą jak działać.
* O co grają Gracze?
    * Sukces:
        * Uratować Marpireus
            * Nie dopuścić, by odleciał za daleko w przestrzeń
            * Przekonać TAI, że sytuacja jest poza kontrolą, że jest bardzo źle
                * TAI nie ma uprawnień, by nadpisywać rozkazy załogi
            * Jakoś doprowadzić do tego, by ktoś go uratował (Orbiter?)
        * Pozbyć się Anomalii z ładowni
            * Zanim dojdzie do za dużego Skażenia
            * Zanim dorośli zaczną polować na dzieci i niePotwory
    * Porażka: 
        * Za dużo martwych (dzieci i dorosłych). 
        * Anomalia pożera statek - powstaje AK Marpireus.
* O co gra MG?
    * Highlevel
        * Pokazać Anomalię Kolapsu i jak bardzo nie-Orbiter mają przechlapane w kontekście magii.
        * ?
    * Co uzyskać fabularnie
        * Niech dzieciaki wezwą Orbiter na pomoc; z siłami Lodowca.
        * Niech 
* Agendy
    * The Protector: chronić dzieci, chronić statek. Mimo, że są pod wpływem Anomalii i tak naprawdę to trzeba chronić przed nimi.
    * The Doomed: statek dąży do anomalizacji siebie. Ostatnią resztką woli kapitan przeznaczył AK Marpireus do 'twisting nether'.

### Co zostało zgłoszone na sesję

Marpireus jest statkiem kosmicznym zamieszkałym przez 55 osób, w czym 7 nastolatków. Jest to statek handlowy oraz ‘złomiarski', pozyskujący wartościowe części z ruin wraków w okolicy magicznej Anomalii Kolapsu.

Jako lokalne dzieciaki Marpireusa jesteście przyuczani do zawodów. W kosmosie szybko się dorasta. Dorośli są lepsi od Was, ale jak trzeba, dajecie sobie radę.

Niestety, podczas aktualnego lotu zaczyna się dziać coś dziwnego. Nie tylko w tajemniczych okolicznościach zmieniliście kurs, ale podobno statek ma nieokreślone problemy. A dorośli – którzy normalnie tłumaczą Wam wszystko – nabierają wody w usta i zaczynają się dziwnie zachowywać.

Czy jesteście zaatakowani? Czy na pokład przedostał się dziwny przybysz? Czy – jak mówi syn kapitana – na pokładzie jest potwór i dorośli próbują Was chronić? Ani Wy, ani inne dzieciaki nie zauważyliście bezpośrednich śladów, ale coś faktycznie się tu dzieje...

Czy odkryjecie co dziwnego dzieje się na Marpireusie? Czy uda się Wam – słabszym, mniej kompetentnym niż załoga Marpireusa – rozwiązać problem i uratować dorosłych? Jeśli nie, dom Wasz, Waszych przyjaciół i rodziców zostanie zniszczony, łącznie z Wami.

Powodzenia.

### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

KONTEKST:

* Zgodny z CHRONOLOGIĄ

CHRONOLOGIA:

* Marpireus pozyskał z Anomalii Kolapsu cenne materiały i części, które są Skażone
* Lacrimor + Alteris zaczyna negatywnie wpływać na Marpireus i załogę. Tylko na dorosłych.
* Kapitan, nie do końca przytomny, celuje Marpireus w pustkę i przyspiesza maksymalnie (G = 0.1)

PRZECIWNIK:

* Anomalny Ładunek na pokładzie Marpireusa
* Skażona załoga; dorośli, którzy 'widzą swój świat'

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)
#### Faza 0 (18:00 - 18:30): Orbiter kontra Marpireus

* Tonalnie
    * Optymistyczna, ciężka praca, która daje radę.
* Wiedza
    * Pokazać załogę i jak się zachowują gdy jest fajnie.
    * Pokazać problemy z Orbiterem, czemu są problemem i trzeba ich omijać.
    * Marpireus robi porządną robotę i jest jednostką dającą sobie radę z zagrożeniami.
* Kontekst postaci i problemu
    * Dzieci są kompetentne
    * TAI Merila jest przyjazna i pomocna
    * Dorośli traktują dzieci z sympatią i szacunkiem
    * Dorośli dają radę z zadaniami, osłaniają młodych
* Implementacja
    * Statek nie ma obowiązku zatrzymać się do kontroli, ale Geisterjager ma większą siłę ognia.
    * Orbiter wchodzi na Marpireus, tam są przemycane Skażone rzeczy.
    * Pokazanie Pacyfikatora, który kiepsko szuka magii; 'T-4' wyśmiewa tego kota. Julka płacze.
    * Orbiter konfiskuje jakieś pomniejsze Skażone rzeczy.

#### Faza 1 (18:30 - 19:30): COŚ NA POKŁADZIE

| Kto                 | 18:30 - 19:30          | 19:00 - 19:30              |
|---------------------|------------------------|----------------------------|
| **The Doomed**      | NIC                    | "kierunek: nicość", Gawin w kosmosie |
| **The Protector**   | "zabezpieczcie zespół" | "toczy się walka"          |
| **TAI Merila**      | NIC                    | "polujemy na potwora"      |
| **Anomaly**         | NIC                    | NIC                        |

* "atak od silników": podobno coś atakuje od strony silników, fortyfikacja i ewakuacja młodych
* "zabezpieczcie zespół": zbieramy. Julka z kotem | Marek chroni 6, 8 | Ela monitoruje systemy i nie wie co się dzieje.
* "pokażcie mi potwora": podobno jest gdzieś w okolicy korytarza, niech ktoś go ściągnie a Merila zamknie śluzę. No monster.
* "kierunek: nicość": zmiana kursu. Kapitan kieruje jednostkę w stronę pustki
* Gawin w kosmosie: wyszedł na zewnątrz jednostki i patrzy w gwiazdy.

#### Faza 2 (19:15 - 20:15): PSYCHOZA

| Kto                 | 18:30 - 19:30          | 19:00 - 19:30              |
|---------------------|------------------------|----------------------------|
| **The Doomed**      | Ela umiera, Johann klucz | Kapitan zabija. TAI Shutdown. |
| **The Protector**   | Szczepan osłania       | Gawin do Zespołu           |
| **TAI Merila**      | osłania jak może; blokady | "POMOCY!"               |
| **Anomaly**         | NIC                    | Mechaniczny Wąż Z Kabli    |

* Ela umiera: Ela próbuje dotrzeć do rodziców. Jest duszona jako "potwór".
* Johann szuka klucza (w ciele Natalii)
* Szczepan chroni dzieci: widzi rzeczy, jest uzbrojony, nie da nikomu zostać skrzywdzonym
* Wiadomość od Gawina: coś jest nie w porządku, czy ktoś mnie słyszy
* Osłona Meliry: śluzy, informacje. Ale nie może nic zrobić przeciw ludziom.
* SOS Meliry: "nie mogę... zatrzymać... ludzkich rozkazów."
* DZIECI ZACZYNAJĄ PANIKOWAĆ. Statek zaczyna się ZMIENIAĆ.

#### Faza 3 (20:15 - 21:15): APOKALIPSA

Eskalacja

## Sesja - analiza
### Fiszki

.

### Scena Zero - impl

Geisterjager, jak zwykle, czepia się. Marpireus wraca z Anomalii Kolapsu z łupami a Geisterjager żąda prawa inspekcji, bo "coś niebezpiecznego może być na pokładzie".

Tr +2:

* X: Plan przechytrzenia Geisterjager nie zadziała. Geisterjager jest zainteresowany inspekcją i ją przeprowadzi.
* V: Inspekcja, wspierana przez Marpireusa, zadziała szybko.
* X: Płacz Julki niestety nikogo nie wzruszył. Nie udało się przekonać Orbitera.
* X: Z uwagi na procedury Orbiter nikogo nie akceptuje podejścia Zespołu.
* V: Orbiter zaakceptował i przeprowadził operację odkażania wodą. To kilka godzin, skomplikowane i kosztowne opóźnienia, ale przynajmniej Orbiter się odwali.


### Sesja Właściwa - impl

2 miesiące później.

Wiadomość na komunikatorze od lekarki: "Coś jest nie tak, coś jest na kadłubie!" Tymczasem w innym miejscu, wiadomość "piraci!" Dlatego dzieci mają być zamknięte i być ostrożne. Mają przeprowadzić operację ewakuacji i chowania młodszych dzieciaków.

Julka próbuje złapać przestraszonego kota (który PODOBNO jest Pacyfikatorem, ale... taaak... był tani). Mięsień przechwycił Julkę i kota:

Tp +3:

* X: Zajęło to dużo czasu. Polowanie na kota i Julkę.
* Vr: Udało się złapać kota i ewakuować Julkę. Nie widać ryzyk, ale kot czegoś się boi.

Słodycz próbuje połączyć się z dorosłymi. Znaleźć osoby, które wiedzą co się dzieje. Czekać na instrukcję. Co robić!

Tr Z +3:

* X: Dorośli są zdenerwowani, dzieją się tu straszne rzeczy. Natychmiast wracać do kwater!
* X: Kapitan deklamuje jakąś dziwną księgę; chyba coś z nim nie tak. Nie ma połączenia.
* X: Gawin, advancer, jest w kosmosie. Poza jednostką. Coś robi. Nie ma z nim kontaktu.

Nerd skupia się na systemach Marpireusa. Co tam jest, czego można się dowiedzieć?

Tr Z +3:

* V: Interesujące informacje
    * Wszystko jest na oko w porządku. Systemy pokazują, że... wszystko działa. Psychoza ludzi? Masowa? O_o
    * Aha, lecimy prosto w głęboki kosmos, ciągle przyspieszając. Oddalamy się od kursu, trajektorii, oddalamy się...

Jeden z innych dzieciaków to Marian, syn kapitana, uczący się na nawigatora. Zespół (dokładniej: Słodycz) przekonał Mariana, że trzeba sprawdzić co się tu dzieje.

Tp +3:

* Vr: Marian ma dowód, że lecą w złym kierunku.
    * Marian idzie na mostek. Idzie naprawić sytuację i porozmawiać z ojcem.
        * (niestety, kapitan, w okowach magii, zabije swoje dziecko)

Tymczasem Mięsień się zorientował, że MOŻE tu być coś magicznego. W końcu psychoza. Zwłaszcza, gdy Nerd przekazał Mięśniowi informację że ten kot **jest** Pacyfikatorem (w 10%). To nie tylko mała Julka. Więc - można biec po statku z kotem i spróbować zlokalizować GDZIE może być Anomalia Magiczna.

Tr Z+3:

* Vr: gdzieś na tylnej części statku
* Vr: znając tajne przejścia, był w stanie zlokalizować że Anomalia jest w ładowni. Czyli coś co wysalvagowali z Anomalii Kolapsu jest przyczyną...

Strzelanina. Ludzie się ostrzeliwują. Niedługo potem informacja od TAI - "sytuacja jest pod kontrolą...". Czyli sytuacja idzie w coraz gorszą stronę. Jest coraz gorzej...

Udało się skontaktować z Gawinem.

Tr Z +3:

* X: Gawin jeszcze jest Skażony, ale powoli poziom Energii Magicznej z niego schodzi. Nie do końca kontaktuje, ale kontaktuje LEPIEJ.
* V: Gawin jest przekonany przez dzieciaki, by zostać na zewnątrz. Że tu się dzieje coś złego.
* Vr: Gawin pomoże dzieciom - mają kogoś DOROSŁEGO kogo TAI posłucha!

Więc Gawin i dzieciaki przekonują TAI, że sytuacja jest anomalna i problematyczna. TAI też chce pomóc, ale jest ograniczona w możliwości, nie może działać wbrew załodze. Ale... próbuje.

* VV: TAI zaakceptowała. Puściła. Dała dzieciom dowodzenie jednostką.
    * Kapitan zabił Mariana.
    * Są akty kanibalizmu na pokładzie.
    * Ogólnie problem XD

Mięsień widzi, jak jego przyjaciel i opiekun zabija i pożera swoją żonę. Mięsień próbuje ją uratować - atakuje Johanna.

Tr+3:

* X: Mięsień ciężko rani Johanna z zaskoczenia.
* Or: Mięsień jest ranny, niesprawna ręka.
* V: Ale Johann wyjdzie z tego i Mięsień też. Udało się powstrzymać atak.

Widząc jak jest źle, dzieciaki zdecydowały się wezwać pomoc. Orbiter. Orbiter ma dość prędkości i siły by ich dogonić zanim dojdzie do tragedii. Więc - wzywamy SOS.

Tp +2:

* V: Orbiter na początku jest bardzo zimny ("is it a prank"), ale jak dzieci się pokazały i powiedziały że mają problemy z Anomalią Magiczną i dorośli są niesprawni, Geisterjager im pomoże.
* V: Orbiter rozkazał TAI oddać kontrolę dzieciom pod groźbą zniszczenia. TAI mając "wyjście" zrobiła to z przyjemnością.

Mając kontrolę nad statkiem, TAI mogła zaprowadzić pewne porządki, neutralizację przez system podtrzymywania życia (zmienić stosunek atmosfery). A Mięsień i Gawin wyrzucili Anomalię z ładowni na zewnątrz. Gawin Z TRUDEM dał radę uciec. Prosto do śluzy. O KROK przed Anomalią, która została na kadłubie.

A Słodycz, ucząca się na medyczkę, zabrała się za medyczną pomoc wszystkim komu mogła...

Wynik:

Tr +4:

* V: Dobry stan załogi
* XXX: Słodycz nie jest dość dobra, ma ranę psychiczną i nigdy już nie będzie medykiem. Ma koszmary.
* VV: Udało się uratować 39/55 członków załogi Marpireusa.

Dzieci uratowały statek. No, Orbiter, ale bez dzieci nie byłoby szans.

## Streszczenie

Podczas rutynowej operacji salvagerskiej, SN Marpireus staje się ofiarą Anomalii Magicznej wyniesionej ze statku na Anomalii Kolapsu. Dorośli zaczynają popadać w psychozę a kapitan ostatnim ruchem rozpaczliwie kieruje statek w nicość. Dzieci na pokładzie muszą przejąć inicjatywę. Dzięki ich działaniom oraz wsparciu częściowo skażonego Advancera udaje się zlokalizować źródło problemu - Anomalię w ładowni - i zapobiec całkowitej katastrofie. Wezwany na pomoc Orbiter (OO Geisterjager) porządkuje sytuację, ale to dzieci były bohaterami. Choć zapłaciły wysoką cenę za swoje bohaterstwo.

## Progresja

* .

## Zasługi

* Klasa Sama Słodycz: Młoda adeptka medycyny, która pomimo braku doświadczenia starała się pomóc każdemu rannemu członkowi załogi. Choć nie wszystkie jej działania przyniosły sukces, jej determinacja i odwaga w obliczu chaosu pozwoliły na stabilizację wielu rannych. Niestety, ogromny ciężar psychiczny i niepowodzenia pozostawiły na niej trwały ślad, przekreślając jej przyszłość jako medyczki.
* Klasa Kosmiczny Nerd: Odpowiedzialny za analizę systemów statku i wykrycie kluczowych anomalii, które wskazały, że Marpireus dryfuje ku nicości. Jego szybkie i logiczne działania w chaosie umożliwiły dzieciom odkrycie lokalizacji Anomalii w ładowni oraz nawiązanie kontaktu z Gawinem, co odegrało kluczową rolę w ocaleniu statku.
* Klasa Mięsień Z Ładowni: Zdeterminowany i lojalny, nie zawahał się podjąć walki z Johannem, widząc jego atak na żonę. Pomimo odniesienia poważnych obrażeń, jego heroiczne działania uratowały życie zaatakowanej kobiety. Później wraz z Gawainem, usunął Anomalię z ładowni, ryzykując własne życie.
* Gawain Marczerik: Dorosły Advancer, który częściowo uległ wpływowi Anomalii, ale dzięki determinacji dzieci odzyskał kontrolę nad sobą. Jego obecność jako jedynego dorosłego zdolnego do działania dała TAI impuls do oddania dzieciom kontroli nad statkiem.
* Renata Kaiser: Oficer komunikacyjny Geisterjager; wykazała się dużą łagodnością i determinacją w komunikacji z dziećmi. Mimo, że wszyscy spodziewali się surowości i okrucieństwa, była łagodna i pomocnie przeprowadziła dzieciaki, przecząc mrocznej opinii Geisterjager.
* Gabriel Lodowiec: Dowódca Geisterjager; osobiście podjął decyzję o pomocy Marpireusowi, mimo ryzyka i kosztów. Reprezentuje nurt "Orbiter pomoże jak jest w stanie", niezależnie od jego opinii.
* SN Marpireus: Stał się ofiarą własnego sukcesu. Anomalia Kolapsu okazała się zbyt niebezpieczna; wyciągnął stamtąd coś ZŁEGO. Niemal przekształcił się w śmiertelną pułapkę na swoich pasażerów. Mimo to, dzięki odwadze dzieci i pomocy Orbitera, Marpireus przetrwał.
* OO Geisterjager: Dowiódł, że nawet statek znany z surowości i brutalności może być symbolem nadziei. Mimo kosztów, Geisterjager pomógł Marpireusowi.

## Frakcje

* brak

## Fakty Lokalizacji

* brak

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Libracja Lirańska
## Czas

* Opóźnienie: -6
* Dni: 6

## Inne

?