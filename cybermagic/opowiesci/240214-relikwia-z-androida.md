## Metadane

* title: "Relikwia z androida?!"
* threads: agencja-lux-umbrarum, problemy-con-szernief
* motives: energia-anteclis, the-resurrector, the-experimenter, the-man-in-black, faceci-w-czerni, niepelna-kontrola, slipping-through-my-fingers, tylko-cel-ma-znaczenie, zawisc-mordercza, tools-of-enslavement
* gm: żółw
* players: igor, cyris, andromeda, sabrina, samael

## Kontynuacja
### Kampanijna

* [240117 - Dla swych marzeń, warto](240117-dla-swych-marzen-warto)

### Chronologiczna

* [240117 - Dla swych marzeń, warto](240117-dla-swych-marzen-warto)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * Eily Eira - Monster
        * "Row, row, row, for it's too late | Row your boat so far away | There's no room for your disgrace | Only one can rule this place"
        * "I am the monster | Run, run, run, fast as you can | Run like I'm the boogieman "
        * Pierwszą linijkę dopasuję do Elwiry, która zabiła Alinę. A drugą do Aliny :-).
* Opowieść o (Theme and vision): 
    * "Lustrzane pragnienia - nie chcę stracić ZNACZENIA i być niczym vs nie chcę stracić ŻYCIA i przestać istnieć, mam po co żyć!!!"
        * Elwira, która dorobiła się pozycji na plagiacie i agresywnych działaniach marketingowych i zarządczych
            * Alina odkryła jej plagiat i się z nią skonfrontowała. Elwira ją zabija używając zakazanych narkotyków 'żmijoszept' - Alina się zabija pod ich wpływem.
        * Alina, która ma od niedawna męża, przybyła tu jako asystentka sławnej Elwiry
            * Powróciła jako _resurrector_, pragnie wrócić do życia. I sprawiedliwości dla siebie.
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * energia-anteclis: szybko wskrzesić 'Alinę' zanim skończy się dostępna energia. Alina: 'Nie zapomnijcie o mnie'! Alina była protomagiem w korzystnym dla jej energii miejscu.
    * the-resurrector: 'Alina', a raczej to co z niej pozostało; próbuje powrócić do życia używając energii wiary w Vanessę-07 używając roślinnych form. Potrzebuje energii, krwi, ciała i skruchy.
    * the-experimenter: Kalista; próbuje zobaczyć, czy umie WYKRYĆ co się tu dzieje używając swoich nowych umiejętności a jednocześnie nie pokazać niczego Agencji.
    * the-man-in-black: Elwira; próbuje zapewnić, że nikt nigdy nie dowie się o jej czynach i o tym że zabiła Alinę. Próbuje ukryć swoje czyny i osłonić reputację.
    * faceci-w-czerni: Agencja Lux Umbrarum. Wyczyścić, usunąć, sprawić by problem zniknął i nikt się niczego nie dowiedział.
    * niepelna-kontrola: 'Alina' myśli, że jest w stanie wrócić. Jej różne niewielkie odłamki. Ale nie ma powrotu i NOWA forma dominuje starą.
    * slipping-through-my-fingers: dr Elwira Barknis; miała karierę, była rozpoznawalna, WSZYSTKO było - ale 'Alina' była lepsza od niej i znalazła plagiat. I teraz wszystko kaskaduje.
    * tylko-cel-ma-znaczenie: z jednej strony Elwira, która dla reputacji i miejsca w historii zabiła Alinę. Z drugiej strony, Alina - nieważne kto zginie, doprowadzi do sprawiedliwości.
    * zawisc-mordercza: Alina miała wszystko, Elwira straciłaby wszystko. Więc Elwira zabiła Alinę i nakarmiła nią hydroponikę.
    * tools-of-enslavement: specyficzne narkotyki; za ich pomocą Elwira zabiła Alinę i teraz 'Alina' wykorzystuje je do ustawienia wszystkiego do rytuału mającego ją wskrzesić
* Dilemma: destroy the resurrector, do we save Elwira?
* Detale
    * Crisis source: 
        * HEART: "Niesprawiedliwa śmierć Aliny prowadzi do prób jej rozpaczliwego powrotu przez biosynta lub TAI"
        * VISIBLE: "Zbieranie środków na Powrót"
    * Agendas (integrują Dark Future)
        * Elwira: the-man-in-black, schować dane i fakty, zaciemnić dowody. ŚRODKI: ma biosynta klasy Infiltrator działającego jak jej asystent, Klaudiusz
        * Echo Aliny: the-resurrector, pozyskać dość krwi i komponentów Vanessy by móc powrócić używając roślin i hydroponiki. Używa hipnotycznych roślin (zapach, narkotyki)
        * Kalista: the-experimenter, próbuje zrozumieć swoje umiejętności alteriańskie i ukryć fakt że je ma, chce pomóc stacji
        * Delgado: chce być samotnym biosyntem, nie zintegrował się
        * Sia: chce redukować koszt energii i materiału, niezależnie od tego co i jak. To jest droga dla Szernief - podejście savarańskie
        * Rada: chce chronić reputację Elwiry i sprowadzić więcej turystów i osób klasy Elwiry. Łącznie z Aeriną Cavalis.

### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

* 6 miesięcy temu: Agencja tu była
    * Rozwiązała problem Obsesyjnego Artefaktu
    * Kalista straciła palec do Pierścienia; od tej pory nosi rękawiczkę
* 4 miesiące temu: 
    * Udało się przeprowadzić kampanię sprowadzającą wartościowych ludzi na Stację Szernief
    * M.in. zespół doktor Elwiry Barknis w którego składzie była Alina Mekran
    * Udało się zintegrować biosynty z resztą Stacji, z wyjątkiem jednego – Delgado.
* 2 miesiąc temu:
    * Alina miała "nieszczęśliwy wypadek pod wpływem narkotyków" w jednym z badanych systemów hydroponicznych.
    * Elwira płaci za przekształcenie danych o narkotycznej przeszłości Tadeusza Mekrana, by zrzucić winę na niego i Alinę.
* miesiąc temu:
    * aresztowanie męża Aliny, Tadeusza Mekrana - za zniszczenie badań Elwiry i próbę samobójstwa w tym samym miejscu gdzie zginęła Alina. "Chciałem być z nią"
        * przeprowadził go Mawir Hong (wstawiła się za nim Elwira by go nie aresztować).
    * migotanie systemów TAI, system hydroponiczny się zaczyna ZMIENIAĆ (nie widać tego bo TAI)
    * pojawiają się dziwne sny
* 2 tygodnie temu:
    * biologowie dostają części Vanessy
    * pojawiają się walki na arenie
        * i pierwsze kulty
* teraz:
    * Agencja się pojawia

### Co się najpewniej stanie - plan wydarzeń (to się NIE STANIE; to kierunek i wizja dla MG)

* F0: TUTORIAL
    * Rozbicie przez Mawirowców grupy kultystów pro-Vanessowych, oni chcą walczyć za swoją idolkę. Sorkon Hantor widzi, że z nimi coś nie tak.
    * Badania wskazują, że są pod wpływem narkotyków powodujących podatność na sugestię i halucynogennych... ale SKĄD? TAI nic nie wie.
* F1: Konstrukcja Rytuału
    * Aerina Cavalis prosi o pomoc Agencję - Kalista zniszczy stację i plany - dlatego musiała być aresztowana
        * Kalista zarzuca Radzie i Aerinie, że ukrywają przestępcę. Historia z mgr Aliną Mekran, która zginęła w podejrzanych okolicznościach
            * W JAKI sposób Alina przemyciła narkotyki? Czemu Elwira zaakceptowała to tak łatwo? Czemu nagle teraz? Czemu nie było ochrony?
    * Nadmierny transfer statków pomiędzy Hydroponiką h4 a Stacją (Skażone rośliny)
    * TAI posiada echa wspomnień Aliny, nie wszystko pamięta i nie wszystko widzi
    * zbieranie krwi, budowanie kultu Vanessy (spontaniczny, z sygnałów TAI) - wszystko pokazuje przeszłość Aliny
    * Elwira usuwa i maskuje ślady sama lub z pomocą Klaudiusza (biosynta klasy Infiltrator)
    * Losowe artykuły dotyczące fitogenezy na Sebirialisie + krew
* F2: Alina: Manifestacja
    * Biomasa z hydroponiki staje się 'biokomputerem' z dodanymi częściami skrzydła biologicznego; próbuje shackować TAI i zreintegrować shardy Aliny
    * Alina próbuje wrócić jako Vanessa; Delgado broni Vanessę. Mawir i Sebastian potencjalna pomoc.
    * Alina próbuje wrócić jako TAI i stać się Vanessą.
* Overall
    * chains
        * regeneracja Aliny
            * (dość krwi), (części Vanessy), (Skażona hydroponika) -> kult Vanessy, nie zapomnij o Alinie -> powrót
                * arena w imię Vanessy, "żeby było lepiej" (frakcja Integratorów), polowanie Delgado
        * ukrycie śladów przez Elwirę
        * zniszczenie Delgado
            * (bloodhunt), (walka z przeważającymi siłami), (ostatni biosynt bez chęci do życia)
        * czy uda się to ukryć
            * dla Kalisty powinno być ujawnione, dla Aeriny - ukryte - by sprowadzić więcej ludzi
    * stakes
        * życie i reputacja: Tadeusza, Elwiry, Delgado
        * stan stacji Szernief - czy ściąga więcej ludzi czy ich odpycha
    * opponent
        * Alina/Anteclis (TAI), Elwira, też samotny Delgado
    * problem
        * .
    * key scenes
        * .

## Sesja - analiza
### Fiszki

.

#### Strony

.

### Scena Zero - impl
#### OGÓLNY OPIS SESJI

Vanessa - odrzucony przez społeczeństwo android - zginęła kilka miesięcy temu heroiczną śmiercią broniąc Stacji Kosmicznej. Została uznana za bohaterkę, pośmiertnie.

Niedawno Sztuczna Inteligencja zarządzająca Stacją Kosmiczną wykonała polecenie wydania szczątków Vanessy trzem losowym biologom. Samo w sobie byłoby to dziwne, ale… nikt nigdy nie wydał tego polecenia. A biolodzy nie pamiętają, by kiedykolwiek te szczątki przejęli lub co z nimi zrobili.

Jednocześnie na nielegalne, podziemne areny zaczęli przychodzić ‘normalni ludzie’ – ojcowie rodzin, administratorzy - którzy nie tylko nigdy nie interesowali się tego typu rozrywkami, ale też nie mieli żadnych szans. Szli na walkę w imieniu Vanessy. Walczyli z nieskończoną determinacją i brakiem umiejętności, ignorując rany - powodując uczucie wstrętu u doświadczonych gladiatorów. „To masakra, nie walka”.

Czy to jest ze sobą powiązane? Jeśli tak – jak?

Wy jesteście Agencją Rządową ds. Eliminacji i Ukrywania Skutków Anomalii Magicznych. To wygląda jak przypadek dla Was – i to dość niepokojący. I to teraz, gdy Stacja zaczęła stawać na nogi…

Społeczeństwo nie może wiedzieć, że magia istnieje, bo dojdzie do katastrofy, dlatego zawsze skutecznie tuszujecie temat. Waszym zadaniem będzie rozpoznać problem, usunąć go i stworzyć odpowiednią bajeczkę czemu to się stało. Jesteście elitarną grupą agentów z dobrym sprzętem i licznymi sojusznikami na miejscu. 

#### SCENA ZERO

Mawirowcy, kultyści Saitaera napotykają modlących się kultystów Vanessy, w parku. Mawirowcy ich izolują i chcą zwalczyć fałszywą wiarę (bo tylko Saitaer jest właściwym władcą)

Ex+3:

* V: jest tam 8 fanatyków Vanessy (nieprzekonywalnych), 12 nie jest tak zaciekłych
* Vz: czemu to robią? Vanessa powróci. Muszą zbierać krew, do tego musi być to krew przelana przez kogoś, nie oddana dobrowolnie.
* X: pojawiają się oficerowie porządkowi; zrobią z tym porządek
* V: bitwa Vanessowców oraz Mawirowców przeciwko siłom porządkowym; udało się nawrócić grupę Vanessowców - "to aspekt Saitaera". Kult się rozprzestrzenia.
* V: badania później pokazały, że fanatycy Vanessy są pod wpływem jakichś hipnonarkotyków. Jakich, czemu, skąd - nie wiadomo.

I na to wezwano Agencję. Dzieją się tu dziwne rzeczy poza samym zniknięciem Vanessy i tym, że TAI nie pamięta co się tu dzieje.

### Sesja Właściwa - impl

Oficer Naukowy ściągnął próbki krwi i narkotyków na Luminarius. Co się tam dzieje? Robi testy - czy tam coś jest?

TrZ+3:

* V: We krwi są niezwykle czyste narkotyki 'żmijoszept'. Ale na tej stacji nie da się takich wyprodukować. Co więcej, są perfekcyjnie dopasowane do targetu - podawane były przez eksperta w idealny sposób. Samo to wskazuje na coś w stylu magii? Bo skąd narkotyki i skąd tak dobre narkotyki? Wymaga to ogromnej wiedzy i możliwości technicznych których tu nie ma.
* X: Dark Agenda ++ (Elwira chowa dowody używając swoich sił)
* V: Udało się dobrać odpowiedni 'detektor krewetkowy' na magię. Energia typu Anteclis. Co więcej, doszli do tego że używany jest krwawy rytuał.
* X: By zdobyć więcej informacji i szczegółów potrzebne będą badania na ludziach.

Hacker próbuje opracować w jaki sposób się rozprzestrzenia Infekcja. Skąd się to bierze, jak idą narkotyki i skąd biorą się ludzie bijący się na arenie?

TrZ+3:

* V: Pacjenci zero to ludzie pracujący przy stacjach hydroponicznych orbitujących dookoła CON Szernief. Stamtąd idzie dalej - ale nie po 'jedzeniu' a po 'proximity'
* X: Elwira wie, że Zespół szuka i się do niej może dostać. Elwira intensyfikuje ukrywanie informacji na lewo i prawo.
* X: W TAI coś jest. Jest tam inny byt. Inna osobowość. I ta inna osobowość zaczyna uszkadzać dane TAI - czyścić logi, usuwać wszelkie informacje... Hacker stoi i blokuje, ale ten inny byt psychotroniczny skutecznie napiera. To nie jest normalna TAI...

W chwili, w której wszystko poszło w cholerę i TAI ma 'w środku' coś innego, coś uszkodzonego - Dyplomata zdecydował się działać. Jeśli to jest, jeśli to myśli, może da się z tym dogadać jakoś. Może da się z tym dojść do czegoś więcej. Przekonać. Że nie chcemy nic złego i jesteśmy tu by pomóc. Że Agencja pomaga. (+3X +3Oy)

* V: 
    * Dyplomata wychodzi z założenia "przychodzimy w pokoju, jesteśmy Agencją, pomagamy"
    * Byt informuje, że Agencja przybywa jako zagrożenie. Byt to "COŚ" a nie "KTOŚ". Jest... ograniczony. Pusty. Brakujący. Niepełny.
    * Dyplomata chce się dowiedzieć, kim/czym Byt jest. Niekompletny Byt wysyła sygnał.
        * KTOŚ spadający w dół, płynąca krew z góry
        * KTOŚ niewinny, zamknięty w więzieniu
        * Wyraźnie Byt niewiele pamięta i jest niekompletny. Ale to co pokazuje jest prawdą, choć niemożliwą do dobrej interpretacji.

Znowu Hacker próbuje dojść do tego co się stało. Do czego pasują rzeczy i wydarzenia pokazywane przez Byt. A z drugiej strony - Dyplomata próbuje zrozumieć więcej na temat Bytu.

* V: 
    * Byt jest odłamkiem większej całości, niekompletnym umysłem, odłamkiem energii Anteclis, który próbuje nie zginąć i działać.
    * W Hydrolab H4 był wypadek; tam były siły porządkowe, tam doszło do zniszczeń materiałów i danych. Zrobił to Tadeusz Mekran.
    * Alina Mekran zginęła w H4 Hydrolab. A jej mąż - Tadeusz - dalej jest w więzieniu.
    * O dziwo, był na scenie Mawir Hong. On dostał parol; wstawiono się za nim, że on nie powodował żadnych zniszczeń.

Hacker przeszukuje też informacje po systemach, dostając ciekawe odpowiedzi:

* Alina Mekran zginęła 2 miesiące temu. Nieszczęśliwy wypadek, zginęła w systemach hydroponicznych i została recyklowana. W Hydrolab H4.
* 2 tygodnie temu Mawir Hong doprowadził Tadeusza Mekrana do H4 Hydrolab. Mekran chciał popełnić dobrze samobójstwo. Faktycznie, podciął sobie żyły i krew płynęła. Mawir tylko patrzył. Ale ochrona wyciągnęła Tadeusza Mekrana i Honga, zanim doszło do zniszczenia H4 Hydrolab.
* Kalista bardzo mocno zaangażowała się w tę sprawę. Ale Aerina Cavalis ją zablokowała - Elwira przyniesie inną populację i pieniądze na Szernief. I Kalista została odizolowana ze stacji, zamknięta w izolatce. Tymczasowo.

Wykorzystując dane z NavirMed i ogólne informacje i uprawnienia, Sabotażysta poszedł porozmawiać z Kalistą. Ona będzie wiedziała co się tu dzieje. Ona zawsze coś wie. Kalista nie chce rozmawiać z Agencją, ale Sabotażysta jej pokazał, że jest jedyną opcją. Kalista zażądała wolności. Sabotażysta jej to OBIECAŁ (ale docelowo złamał słowo).

Tr Z (NavirMed) +3:

* V: NavirMed nie ma z tym powiązania; nie chcą się bawić z narkotykami, dostarczyli Agencji wszystkie potrzebne dane i próbki
* X: Wszystkie formy przesłuchania Kalisty i szperanie przez Agencję sprawiły, że Elwira się dowiedziała o tym. Przygotowuje pozew przeciw Agencji.
* X: Wszystkie dane pokazujące winę Elwiry i problemy Elwira - Tadeusz - Alina zostały wyczyszczone; nie nadają się do sądu. Elwira ma macki poza Szernief.
* X: Faktycznie, zgodnie z planem Aeriny, sporo ludzi się pojawiło na Szernief dzięki Elwirze. Elwira jest ważna i uderzenie w nią polityczne skrzywdzi stację.
* V: Kalista doszła do tego, że Elwira jest dobra w hipnotyczne narkotyki. To jej ekspertyza. Doszła do tego, że Elwira POTENCJALNIE w przeszłości przypisywała sobie czyjeś inne sukcesy i Alina do tego doszła. Alina zawsze wierzyła w drugą szansę. Zdaniem Kalisty, duża szansa, że Elwira stoi za krzywdą Aliny. I Kalista oczekuje, że Agencja zrobi dobro - że Elwira zapłaci za swoje zbrodnie.

Wiemy bowiem, że Alina na pewno nie żyje - zginęła w H4 i nakarmiła hydroponikę. Wiemy, że nie walczyła. Najpewniej była pod wpływem tych samych narkotyków co potem inne osoby. Ale po co jej ciało Vanessy?

Oficer Naukowy doszedł do tego - żeby mogła wrócić. Alina chce wrócić w ciele Vanessy. A krew służy do dostarczenia energii.

Dyplomata rozpoczął zaawansowaną propagandę. Trzeba zabrać stąd Vanessę, pokazać, że ją Agencją może naprawić i doprowadzić do funkcjonowania (pomiędzy Hackerem i Inżynierem da się naprawić Vanessę; będą uszkodzenia i straty w psychotronice, ale rdzeń będzie działał). 

Tr +3 +3Ob + 3Vz:

* X: musieli obiecać, że naprawią i muszą naprawić
* X: MANIFESTACJA MAGICZNA. H4 Awakens.
* V: ludzie dostarczą Vanessę Agencji czekając na pojawienie się ich prorokini

Z perspektywy "Aliny" - a raczej tego co z Aliny zostało - jej jedyna szansa powrotu do swojego kochanego męża znika. Alina uruchamia H4 Hydrolab z poziomu biokonstrukcji i biostruktury. Staje się to anomalną stacją hydroponiczną. BIA (Biologiczny Intelekt Autonomiczny) próbuje przejąć kontrolę nad AI Szernief. By ratować TAI Szernief, Hacker spina Luminariusa i zaawansowane TAI bojowe Luminariusa z Szernief. Razem odeprą atak psychotroniczny ze strony Anomalnej BIA.

Tr Z +3 + 3Ob:

* XX: nie tylko nie dali radę odeprzeć ataku; Luminarius ma poważną awarię i musi być ewakuowany i eskortowany. Atak psychotroniczny ciężko uszkodził Luminariusa.
* V: udało się Hackerowi i Luminariusowi kupić tyle czasu ile się dało

Inżynier i Sabotażysta szybko biegną do silników, manewrowania itp. Cel - jak najszybsze zniszczenie anomalnej H4 używając silników manewrowych Szernief. Nie mają innych sensownych możliwości.

* V: Udało się im nie uszkodzić Szernief strukturalnie za mocno. Lekkie uszkodzenia, ale nic dużego
* V: H4 płonie

Niestety, ludzie połączeni z H4 potrzebowali pomocy medycznej by się 'odpiąć' od hipnotycznych narkotyków. Oficer Naukowy robił co mógł by to zrobić. Na szybko udało mu się uratować... (XVX) 80% osób które były Sprzężone z H4.

Mimo dużej ilości strat i paniki, udało się Agencji uratować Szernief.

Fallout i co stało się później:

* Kalista i Aerina są przeciwniczkami. Aerina boi się magii więc chce rozwodnić populację na Szernief. Kalista dąży do prawdy. Kalista i Aerina, kiedyś przyjaciółki, są wrogami.
* Agencja zdradziła Kalistę ponownie - Elwirę nie spotkała sprawiedliwa kara a sama Kalista jest odizolowana. Kalista tego nie zapomni.
* Elwira wycofuje pozew; Agencja trzyma ją na krótkiej smyczy. Jej umiejętności będą przydatne dla Szernief, więc może spokojnie żyć i działać.
* Vanessa została naprawiona i wycofana poza stację. Zostanie turystyczną podcasterką, a Stacja Szernief będzie miejscem popularnym turystycznie, jako "dom Vanessy".

## Streszczenie

Zniknęły szczątki Vanessy - bohaterskiego androida, wydane trzem biologom. Niewyjaśnione polecenie wydania szczątków Vanessy i niespodziewane pojawienie się fanatyków walczących w jej imieniu na podziemnych arenach, zainfekowanych niemożliwymi do zrobienia na tej stacji hipnonarkotykami, wskazało na problem z anomaliami magicznymi. Okazało się, że przyczyną było morderstwo Aliny Mekran przez Elwirę Barknis, której plagiat i agresywne metody w pracy wyszły na jaw. Alina, próbująca wrócić do życia jako "resurrector" za pomocą roślin i hydroponiki, potrzebowała energii, krwi i ciała. Agencja była w stanie zamaskować sytuację, zniszczyć zainfekowany orbitujący dookoła Szernief hydrolab H4 i zabrali Vanessę jako influencerkę turystyczną poza Stację.

## Progresja

* Aerina Cavalis: jej przyjaźń z Kalistą się skończyła. Będzie chronić stację, Kalista może ją zniszczyć.
* Kalista Surilik: zdradzona przez Agencję (ponownie) oraz przez Aerinę, zbliża się do Mawira. Prawda jest najważniejsza.

## Zasługi

* Klasa Sabotażysta: skuteczne negocjacje z Kalistą, które odsłoniły kluczowe informacje dotyczące Elwiry i Aliny, umożliwiając Agencji głębsze zrozumienie sytuacji i dalsze kroki działania. Potem - współpraca z Inżynierem by zniszczyć H4 Hydrolab używając płomieni silników Szernief.
* Klasa Inżynier: wypalenie H4 Hydrolab silnikami Szernief i stabilizacja stacji po trudnym manewrze. Potem, z hackerem, naprawa Vanessy.
* Klasa Dyplomata: kampania informacyjna, która umożliwiła pomyślną ewakuację Vanessy, minimalizując szkody i zapewniając jej nową rolę jako ikony turystycznej. Też: negocjacje z 'shardem Aliny' wewnątrz TAI Szernief.
* Klasa Oficer Naukowy: odkrycie przyczyny hipnotycznego wpływu na mieszkańców stacji i rozwój metody odtrucia, co pozwoliło na uratowanie większości osób pod wpływem narkotyków. Odkrycie energii Anteclis.
* Klasa Hacker: obrona przed psychotronicznym atakiem, zabezpieczając systemy stacji przed przejęciem przez Anomalię BIA. Też: znalezienie pacjenta zero Szernief i danych z dokumentacji, co tu się naprawdę działo i jak daleki zasięg ma Elwira.
* Elwira Barknis: dorobiła się pozycji na plagiacie i agresywnych działaniach marketingowych i zarządczych; gdy Alina to odkryła, Elwira ją zabiła a potem usunęła ślady. Przyniosła dużo wartościowych ludzi na stację i podnosi jej reputację.
* Alina Mekran: asystentka Elwiry; odkryła plagiat Elwiry i się z nią skonfrontowała. Zginęła z ręki Elwiry, ale wróciła dla męża. Anomalia Anteclis. Zginęła na dobre w ogniu silników Szernief.
* Tadeusz Mekran: kochający mąż Aliny, który pod wpływem hipnonarkotyków chciał się zabić po zniszczeniu serii danych (dla Elwiry). Aktywowało to ducha Aliny. Ewakuowany przez Agencję z Szernief.
* Mawir Hong: Tadeusz błagał go o pomoc by móc się zabić tam gdzie zginęła Alina. Pomógł mu. Pomógł Tadeuszowi, bo Tadeusz ma jaja. Trafił do więzienia, ale go szybko wypuścili za wstawiennictwem Elwiry.
* Vanessa d'Cavalis: została ikoną, symbolem, celem religijnym i miała posłużyć do wskrzeszenia Aliny. Przechwycona przez Agencję i naprawiona, z nową psychotroniką, została idolką i podcasterką turystyczną.
* Kalista Surilik: walcząca o prawdę za wszelką cenę i zamknięta przez Aerinę w izolatce, odkryła prawdę o Elwirze i podzieliła się nią z Agencją. Zdradzona przez Aerinę i Agencję, pójdzie własną drogą.
* Aerina Cavalis: dążąc do ochrony reputacji stacji oraz Elwiry Barknis, próbowała zmanipulować sytuację, by utrzymać status quo, co niestety skutkowało izolacją Kalisty. Tormentowana przez przeszłość, będzie chronić stację za wszelką cenę.

## Frakcje

* Con Szernief Querenci: decydują się działać na własną rękę, bez współpracy z Agencją. Mają dość sił by sobie z tym poradzić. Trzeba tylko przygotować zasoby i znajomości.
* Con Szernief Unifikaci: coraz bardziej rosną w siłę, zgodnie z 'my mamy wspólną historię i kulturę, ci obcy nie mają nic'
* Con Szernief Biosynty: frakcja zaniknęła, rozproszona w pozostałych frakcjach.
* Con Szernief Popularyzatorzy: bardzo wzmocnieni, działaniami Aeriny, Rady oraz Elwiry. Ich celem staje się rozwodnienie populacji Szernief by redukować anomalie magiczne, co pasuje też Agencji.
* Con Szernief Justarianie: nie podoba im się rozwadnianie populacji stacji i niszczenie kultury która do tej stacji należy. Kolejny problem do rozwiązania.
* Con Szernief Mawirowcy: dołączają do nich też niektórzy kultyści Vanessy; nie tylko drakolici. Zaczynają budować 'shadow government' w tle.

## Fakty Lokalizacji

* CON Szernief: TAI stacji została zmodyfikowana przez wpływ echa Aliny Mekran / Anteclis; działa troszkę inaczej i jej podstawowe ograniczenia przestały działać.
* CON Szernief: do Mawirowców zaczęli dołączać też kultyści Vanessy; siły wyznawców Saitaera rosną. ALE - wpływa sporo nowych ludzi dzięki Elwirze i Aerinie - zarówno turystów jak i naukowców.
* CON Szernief: niewielkie uszkodzenia strukturalne, utrata Hydrolab H4.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Kalmantis
            1. Sebirialis, orbita
                1. CON Szernief
                    1. Orbitujące stacje hydroponiczne: jedna z nich (Hydrolab H4) była miejscem zbrodni - zginęła tam Alina pod wpływem hipnotycznych narkotyków Elwiry. Potem się skaziła i spłonęła w ogniu Szernief.

## Czas

* Opóźnienie: 191
* Dni: 2
