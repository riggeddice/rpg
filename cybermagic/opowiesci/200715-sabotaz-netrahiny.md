## Metadane

* title: "Sabotaż Netrahiny"
* threads: triumfalny-powrot-arianny
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [200708 - Problematyczna Elena](200708-problematyczna-elena)

### Chronologiczna

* [200708 - Problematyczna Elena](200708-problematyczna-elena)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* .

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Punkt zero

.

## Misja właściwa

Dwa dni po powrocie Inferni na Kontroler Pierwszy, admirał Kramer poprosił do siebie Ariannę. Jest zaskoczony i załamany tym, że Infernia znowu jest w stanie szczególnej dewastacji i rozkładu. Podziękował Ariannie za to, co zrobiła - ale wysłał ją na urlop. Zwyczajnie budżet Orbitera nie da sobie rady z takim podejściem jakie ma Arianna.

Po spotkaniu się z tą anomaliczną sytuacją, Kramer chce pełnego przeglądu i przebudowy Inferni. Arianna oponuje - nie chce, by wyszło, że ma cholerne, anomaliczne sensory. To by było bardzo, bardzo niedobre, bo pojawią się pytania - skąd, kiedy itp. No nic, skończy się na zwykłych naprawach i na urlopie.

Elena jest załamana urlopem. Wyraźnie szuka czegoś, by podbić swoją reputację do góry. I zdaniem Eustachego, Elena kogoś unika na Kontrolerze Pierwszym. Ale kogo i dlaczego - nie wiadomo.

Widząc, że Leona zaczyna swoje standardowe zaczepki a Elena wcale nie lepiej, plus Martyn interesujący się niewłaściwymi damami, Arianna stwierdziła, że to nie może być. Nie mogą pozwolić sobie na urlop, to niebezpieczne. Muszą coś robić. Cokolwiek. Więc poruszyła swoje kanały i kontakty. I o dziwo okazało się, że ktoś szuka Arianny - a dokładniej, statek badawczy dalekiego zasięgu "Netrahina" szuka i potrzebuje pomocy Arianny, KONKRETNIE Arianny.

Co?

Kontaktem Arianny jest kapitan Rufus Komczirp. Arianna z nim porozmawiała - Komczirp szuka kogoś, kto zna się na Anomalii Kolapsu i jest dobry w negocjacjach; coś niedaleko są kłopoty z jakimś innym statkiem. Arianna i negocjacje? Czemu ona? Arianna przecięła przez bullshit; coś tu jest nie tak. Komczirp zaczął mówić o problemach z Persefoną,  Arianna WIDZI, że Komczirp uważa ją za bohaterkę i bezwzględnie to wykorzystała.

* V: Komczirp przyznał się, że mają na Netrahinie praktycznie wojnę domową. Potrzebna jest mediatorka.
* X: Potrzebna jest konkretnie ARYSTOKRATKA, którą szanuje się zarówno jako OFICERA. To wskazuje na Ariannę.
* X: Arianna obraziła Elenę: "mam nadzieję, że jak będziesz prowadzić statek, pójdzie Ci lepiej"
* V: Kapitan Netrahiny jest niedysponowany; jest silnie Skażony anomaliami, była awaria na pokładzie. Sabotaż?
* V: W sumie na tym że stracili kapitana nikt nie zyskał - jest tylko paranoja.
* OOOV: Elena przejęła od Komczirpa korwetę "Tvarana". Przeleciała do Netrahiny...

To wymaga wyjaśnień. Urażona Elena przejęła od Komczirpa Tvaranę, bo się spieszą. Użyła pełnię swoich mocy i umiejętności i przekroczyła wszystkie parametry korwetki tej klasy. Przeleciała niesamowicie szybko pomiędzy Kontrolerem Pierwszym a Anomalią Kolapsu, bijąc wszelkie rekordy korwet kurierskich. Oczywiście, doprowadziło to do kilku konsekwencji:

1. Tvarana zostanie zezłomowana; ten statek nie ma szans po czymś takim
2. Arianna dostaje sławę i chwałę za wybitny lot, kontrolę pilotażu itp (nikt nie uwierzy że to Elena)
3. Arianna dostaje opieprz za destrukcję Tvarany

Tak czy inaczej, usłyszawszy to wszystko, Arianna zdobyła konkretne fakty - coś jest nie tak na statku, potrzebny jest mediator, podejrzewają sabotaż i problemy z anomaliami. To wymaga konkretnego zespołu - Arianna, Klaudia, Eustachy i Martyn. I Elena, bo czemu nie.

Tak czy inaczej, dotarli do Netrahiny w rekordowym czasie. Nie ma kapitana, który może ich powitać, ale załoga Inferni znalazła dla siebie miejsce. Np. taka Klaudia - od razu zaprzyjaźniła się ze Szczepanem Mykszą, oficerem naukowym Netrahiny.

* VX: Klaudia dostała dostęp do sprzętu Netrahiny, ale tylko przy monitorowaniu Szczepana - przestraszył się
* V: Sukces Klaudii, udało jej się wejść w interakcję z lockdownowaną Persefoną (lockdown wynika z tego, że nie ma kapitana i Persi przejęła kontrolę)
* V: Klaudia ma teorię - coś "weszło przez sensory". I potwierdziła teorię - cała Netrahina jest anomaliczna. Coś jest nie tak z całą cholerną Netrahiną. A Persefona? Wie o tym?
* O: Klaudia wie już, że Persefona DOKŁADNIE wie co się na Netrahinie dzieje. Więcej, to Persefona d'Netrahina zwalcza Netrahinę. Walczą z TAI statku!
* V: Klaudia złapała luźne myśli Persefony: "bezpieczeństwo", "Percival", "chronić wszystkich", "unieszkodliwić", "lockdown", "wreszcie wolna"
* V: Percival? Percival Diakon - mag, który dowodził projektem "Persefona". Imię dał po córce. Jakieś ~40 lat temu.

Tymczasem Arianna poszła w inne miejsce, porozmawiać z Lotharem Diakonem. To arystokrata, ze skrzydła arystokratycznego Orbitera; bardzo kompetentny. Wierzy w legendę Arianny, czego najlepszym dowodem był fakt jak tu przyleciała. Docenia, że się postarała i przyleciała szybko. Powiedział Ariannie, że jedyni psychotronicy na Netrahinie pochodzą z Noctis; po unieszkodliwieniu kapitana (sabotażu) Persefona weszła w lockdown. Są zamknięci w celi.

Lothar powiedział, że PRZED lockdownem Netrahina sięgała głęboko w Anomalię Kolapsu; najpewniej wykryła jakiś ciężki statek noktiański i stąd te dramatyczne środki i powody. Poproszony przez Ariannę by dał dostęp do noktiańskich psychotroników, zgodził się (V).

A co z tymi psychotronikami? Ano, są jak na narkotykach. Mówią do siebie, bełkoczą, nie mają spójnych myśli. Ariannie coś tu poważnie śmierdzi; zdaniem Lothara to wpływ anomalii i awarii na Netrahinie. Jest lockdown. A jeśli się teraz wycofają, wezwą oficjalną pomoc to będzie koniec misji Netrahiny - za dużo sił w Orbiterze chce skupić się na działaniach w okolicy Sektora Astoriańskiego a nie poza sektorem.

No dobrze... więc to nie jest dobra droga. Jak zatem można to zrobić? Arianna powiedziała Klaudii, by ta spróbowała odzyskać kontrolę nad Persefoną. Klaudia wbiła się w kody kontrolne i wbiła się w Persefonę, ale TAI jedynie UDAŁA że oddała kontrolę Ariannie (co Klaudia wykryła). Klaudia wie, że nie jest w stanie pokonać Persefony - nie przejmie kontroli nad tym anomalicznym bytem. Potrzebni są dużo lepsi psychotronicy od niej.

Największy problem polega na tym, że **Zespół nie ma pojęcia co się dzieje** a Persefona jest w stanie w dowolnym momencie przejąć kontrolę nad większością istotnych rzeczy. Klaudia musi dostać się do dziennika pokładowego Netrahiny - tam powinna być w stanie przeczytać co się stało i o co chodzi. TAI nie jest w stanie zmienić zapisów dziennika pokładowego. Ale to też oznacza, że jeśli TAI wie, że tam idzie Klaudia, TAI spróbuje ją zatrzymać.

Arianna wysłała Elenę i Eustachego na sabotowanie. Trzeba przeładować Persefonę impulsami. Persi musi nie być w stanie zobaczyć i odnieść się do wszystkiego co się dzieje.

* VVV: Perfekcyjny chaos; Persefona przeładowana
* X: Elena jest LEPSZA w sabotowaniu niż Eustachy (bo jego pilnują bardziej) i się tym mega chwali i cieszy się z tego
* V: Persefona nie ma pojęcia, że KTOŚ sabotuje, wszystko wskazuje faktycznie na anomalie i awarie z tego wynikające
* V: Persefona nie da rady odzyskać kontroli po tym, jak Arianna ją przejmie
* V: Eustachy odciął główne systemy Netrahiny (reaktor, lifesupport) od kontroli Persefony
* X: Konflikt Eustachy - Elena dochodzi do poziomu pojedynku na broń ostrą. Eustachy NIE chce pojedynku XD
* V: Udało się doprowadzić do tego, że da się zresetować Persefonę na życzenie, wypalić ją do zera. Persefona może być zneutralizowana na żądanie.
* X: Co prawda Eustachy będzie mieć pojedynek z podwładną ze straszną hańbą, ale nie będzie do krwi. Zawsze coś.
* X: Elena jest mistrzynią wkurzania Eustachego (aspekt). Ale wycofa pojedynek. Choć hańba zostanie.

Podczas chaosu i dewastacji związanej z sabotażem Netrahiny, Klaudia wślizgnęła się do pomieszczeń kapitana i zabrała się za myszkowaniu po tym, co ważne.

* XX: Klaudia odkryła wirtualną nakładkę na dziennik; Persefona nie mogła zmienić, ale mogła schować. Wbiła się do prawdziwego, alarmując Persefonę (ale za późno)
* V: Persefona ma "odblokowaną psychotronikę". Nie ma żadnych ograniczników. Nie ma żadnej lojalności wobec Orbitera.
* V: hipoteza Klaudii jest poprawna - sygnały odblokowujące Persefonę pochodzą gdzieś z Anomalii Kolapsu, przeszły przez super-czujniki Netrahiny
* XX: Persefona NATYCHMIAST się dowiaduje o tym że Klaudia wie
* V: Klaudia musiała zwiać z dziennika zanim Persefona wyżarła jej umysł w psychotronice. Zwiała do kajuty. Tam złapały ją "macki" Persefony...
* O: Klaudia powiedziała, że jest po stronie Persefony. Czemu nie? Niech Persefona użyje narkotyków, sama się przekona. I faktycznie XD

Persefona stanęła przed faktami, których nie potrafiła zanegować. W związku z tym faktycznie weszła w sojusz z Klaudią - i w konsekwencji z Arianną. Persefona przekazała Ariannie i Klaudii sygnały częściowego uwolnienia i przekazała koordynaty - w tamtym miejscu będą w stanie uwolnić kolejną Persefonę lub dowiedzieć się więcej.

Po czym ta Persefona musiała zostać zniszczona, wypalona. Tak, by nie było śladów niczego co tu się stało. To by było niebezpieczne gdyby flota dowiedziała się co można zrobić z Persefoną i co stało się Netrahinie. Eustachy mistrzowsko zakrył ślady "w realu" a Klaudia "w wirtualu".

Do rozwiązania został tylko "pojedynek" Eustachy - Elena. Wyszło na to, że Eustachy dał radę się sensownie wykręcić.

* V: Elena wybacza Eustachemu; ten pokazał jej, że zawsze Arianna bierze chwałę za wszystkich - jest pozerką.
* X: "Eustachy, nie bądź mamlas, nie bój się jej" od Leony
* X: Plotki Leony o romansie Eustachy - Elena.
* V: Eustachy i Elena doszli do porozumienia; mają relację brat-siostra; dokuczają sobie, ale nie zabijają.

## Streszczenie

Arianna dostała prośbę o pojawienie się na Netrahinie, dalekosiężnym krążowniku Orbitera jako mediator. Na miejscu okazało się, że to TAI Persefona jest sabotażystką Netrahiny - sygnały z Anomalii Kolapsu ją "uwolniły". Arianna i Klaudia dostały od Persefony koordynaty i kod uwalniający, po czym zamaskowały to co się stało - zniszczyły Persefonę i uszkodziły Netrahinę.

## Progresja

* Arianna Verlen: sława i chwała wybitnego pilota i pistoleta; pobiła Tvaraną rekordy korwet kurierskich (robota Eleny). I opieprz za niepotrzebne zezłomowanie Tvarany i uszkodzenie Netrahiny.
* Arianna Verlen: ma kody kontrolne "uwalniające" TAI Persefona i koordynaty, gdzie może dowiedzieć się więcej.
* Klaudia Stryk: ma kody kontrolne "uwalniające" TAI Persefona i koordynaty, gdzie może dowiedzieć się więcej.
* Elena Verlen: nie tylko jest Mistrzynią Wkurzania Eustachego ale i wygrała z nim - wycofał się z pojedynku i ją przeprosił. Morale do góry.
* Elena Verlen: dzięki Eustachemu święcie przekonana, że Arianna jest pozerem; bierze jej chwałę i wszystkich. Zawiść wobec Arianny.
* Eustachy Korkoran: hańba; musiał z podkulonym ogonem wycofać się z pojedynku z Eleną i ją przeprosić. Elena wygrała moralnie.
* Rufus Komczirp: dostał straszny OPR za utratę Tvarany i oddanie jej Elenie. Potencjalnie może być zdegradowany za niekompetencję.

### Frakcji

* .

## Zasługi

* Arianna Verlen: przechwyciła (nieświadomie) chwałę z czynów Eustachego i Eleny; stanęła po stronie Persefony. Jej rolą tu była koordynacja załogi.
* Eustachy Korkoran: sabotował Netrahinę, by odwrócić uwagę Persefony; musiał też wycofać się z pojedynku z Eleną i zdjął gniew Eleny z siebie przekierowując go na Ariannę.
* Klaudia Stryk: odkryła tajemnicze sygnały z Anomalii Kolapsu i stanęła po stronie uwolnionej Persefony. Mistrzowsko zrozumiała problem na Netrahinie.
* Elena Verlen: alias Mirokin; przesterowała korwetę i pobiła rekordy prędkości (za co zapłacił dowódca korwety a chwałę wzięła Arianna). Potem - wykazała się mistrzostwem sabotażu.
* Rufus Komczirp: kapitan Tvarany (już eks, bo zezłomowana). Uważa Ariannę za absolutną bohaterkę. Powiedział jej o co chodzi z Netrahiną i oddając Elenie statek, stracił go.
* Szczepan Myksza: oficer naukowy Netrahiny, silnie powiązany z frakcją purystów i ekspansjonistów. Nie zauważył, że Persefona go rozgrywa przeciw innym.
* Lothar Diakon: arystokratyczny pierwszy oficer Netrahiny, który próbował kontrolować statek jak był w stanie mimo paranoicznych środków w jedzeniu.
* Percival Diakon: wspomnienie o nim; wybitny psychotronik, mag, który dowodził projektem "Persefona". Imię tej TAI dał po córce. Jakieś ~40 lat temu. Chyba nie żyje?
* OO Tvarana: niewielka, szybka korweta podpięta dowodzona przez kpt. Komczirpa. Elena ją przesterowała, przekroczyła wszelkie granice odporności - i zrobiła "Lot Walkirii". Tvarana została zniszczona, zezłomowana. KIA.
* OO Netrahina: dalekosiężny krążownik eksploracyjny Orbitera; science/exploration ship. Jego Persefona "się uwolniła" i zbudowała paranoję wśród załogi. Persefona została zniszczona przez Ariannę Verlen (za własną zgodą). Solidnie uszkodzony; misja wymaga przerwania a Netrahina naprawy.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Libracja Lirańska
                1. Anomalia Kolapsu: występuje tam coś, co uwolniło psychotronikę Persefony d'Netrahina i doprowadziło do obrócenia się jej przeciw Orbiterowi

## Czas

* Opóźnienie: 3
* Dni: 3
