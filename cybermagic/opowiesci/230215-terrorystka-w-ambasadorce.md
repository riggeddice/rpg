## Metadane

* title: "Terrorystka w Ambasadorce"
* threads: kidiron-zbawca-nativis
* motives: ujawnic-zlo-publicznie, sleeper-agent, hostage-situation
* gm: żółw
* players: fox, kapsel, kić

## Kontynuacja
### Kampanijna

* [230208 - Pierwsza randka Eustachego](230208-pierwsza-randka-eustachego)

### Chronologiczna

* [230208 - Pierwsza randka Eustachego](230208-pierwsza-randka-eustachego)

## Plan sesji
### Theme & Vision

* The past returns to Nativis
    * "To nie ma znaczenia. Oni wszyscy o to nie dbają. Chcą bezpiecznej arkologii a ja ją zapewniam, Bartku." - Rafał Kidiron
    * Dziennikarz chce zrobić program o przeklętym Kidironie, Bartłomiej nalega (w tajemnicy przed Kidironem). Kidiron się, o dziwo, docelowo zgadza.
    * Misteria Anakris kiedyś była częścią arkologii Lirvint. Straciła wszystko po tym jak Kidiron zdobył Światło Ukojenia.
        * terrorism. Misteria chce odzyskać Światło, wiły oraz ujawnić okrucieństwo i zło Kidirona.
* Sadness, eternal corporatism
    * "Twoje ideały są niepraktyczne. Straciłeś Infernię. Nie masz po co walczyć. Bez Ciebie Nativis nie byłoby tym czym jest. Czerp korzyści z Twych poświęceń." - Rafał Kidiron
* RIFT się pogłębia - Bartłomiej Korkoran / Rafał Kidiron
    * Korkoran jest wściekły na Kidirona za to co zrobił 10 lat temu z Lirvint. Zdaniem Kidirona to jedyna sensowna opcja.
    * Kidiron stabilizuje i ratuje arkologię przed zagrożeniami. "głód? Mamy jedzenie. Inne dobra? Mamy wiły. Bunty? Stłumione. Ludzie są bezpieczni. Nativis jest w szczycie!"
    * Kidiron jest drakolitą ("nigdy nie złamałem żadnej zasady"), Korkoran jest faerilem ("to jest niezgodne z wolą Jej Ekscelencji! Zadajesz cierpienie!").
    * Kidiron chce Misterię żywcem. Jest możliwa do wykorzystania na chwałę Nativis.
* Pokazanie bogatej dzielnicy 

### Co się stanie (what will happen)

* S1: Randka Eustachego z Kalią - końcówka
    * emanacja magii z Ambasadorki Ukojenia. Coś tam się dzieje. 97 osób w akcji terrorystycznej
    * JEŚLI Eustachy / Ardilla tam wpadli, wujek odda się na zakładnika
* S2: Odbijamy zakładników
    * Kompleks 'Ambasadorka Ukojenia' ma: (holoprojekcje, wiły, zakładników, buduary, pokoje, rośliny, barek, bogactwo, plac)
    * Misteria zgadza się na dziennikarza i medyka. Nie wie, że Kalia jest idolką. "Chcesz zobaczyć co oni robią dziewczynom, KALIO? Chcesz wiedzieć?!"
    * Kidiron ma gdzieś życie Misterii. Nie jest przeciwny śmierci ludzi - ale niech wiły przetrwają. I Światło Ukojenia.
* S3: Tobiasz Lobrak jest w panic room. 
    * Wraz z Karolem, szefem tego cholerstwa
* SN: Kłótnia Kidiron - Bartłomiej Korkoran. Decyzja Eustachego w sprawie Misterii.


### Sukces graczy (when you win)

* Rozwiązanie problemu terroryzmu

## Sesja właściwa

### Fiszki

#### 1. Infernia

* Bartłomiej Korkoran: wuj i twarda łapka rządząca Infernią (WG: "System służy ludziom") | faeril: "Infernia służy Korkoranom jako awatar Bezimiennej Pani."
    * (ENCAO: +-000 |Bezkompromisowy, nieustępliwy;;Ciekawski|Family, Benevolence, Self-direction > Achievement, Tradition, Humility| DRIVE: Inkwizytor: ujawnianie bolesnych prawd)
    * "Nasza Infernia ma za zadanie dać nam wolność oraz pomagać innym!"
* Emilia d'Infernia
    * "żona" Bartłomieja. Żywy arsenał. Egzekutor Bartłomieja. (Emilia -> "Egzekucja MILItarnA")
* Ardilla Korkoran: faerilka działająca jak drakolitka i przyszywana kuzynka Eustachego  <-- często Fox
    * badacz/odkrywca/scrapper. Zmodyfikowana tak, by łatwiej wchodzić w niedostępne miejsca, bardziej zwinność + orientacja w terenie + pewne umiejętności badawcze.
* Celina Lertys: drakolitka z Aspirii podkochująca się w Eustachym  <-- często Kić
    * (ENCAO: --+-0 |Stanowcza, skryta;;O wielu maskach| Universalism, Tradition, Security > Power, Humility| DRIVE: Harmonia naturalna między wszystkim)
    * złotoskóra o błękitnych elektrycznych włosach podkochująca się w Eustachym (science / bio officer + medical); augmentowana na widzenie rzeczy ukrytych; 19 lat
    * "Jesteśmy częścią Neikatis i podlegamy procesom Neikatis. Ale nadal mam zamiar zrobić wszystko by uratować swoich przyjaciół."
* Jan Lertys: drakolita z Aspirii
    * (ENCAO: -0-0+ |Zapominalski, obserwator, łatwo nań wywrzeć wrażenie | Humility, Universalism > Self-direction, Achievement | DRIVE: Pokój: Kapuleti i Monteki MUSZĄ żyć w pokoju, tylko nie Celina x Eustachy)
    * "Ciężką pracą dojdziemy do tego, że wszystko będzie działać jak powinno. Ja nie wtrącam się do działania Bartłomieja Korkorana; on wie lepiej."
* Ralf Tapszecz: mag (domena: DOMINUJĄCA ROZPACZ) i nowy dodatek do Inferni, kultysta Interis. Noktianin, savaranin.
    * (ENCAO:  -00-+ |Bezbarwny, przezroczysty;;Złośliwy;;Buja w obłokach| VALS: Conformity, Humility >> Benevolence| DRIVE: Piękno zniszczenia)
    * "Nasze porażki nie mają znaczenia. Tak czy inaczej zwyciężymy."
* Tymon Korkoran: egzaltowany lekkoduch i już nie następca Bartłomieja
    * (ENCAO:  +--00 |Co chce, weźmie;;Niecierpliwy;;Egzaltowany | Stimulation, Tradition > Face, Humility | TAK: Być lepszym od Bartłomieja w oczach Kidirona)
    * "Infernia jest moja i należy do mnie. Bartłomiej marnuje jej potencjał, a dzięki niej Nativis może rządzić Neikatis!"
* Mariusz Dobrowąs: oficer wojskowy Inferni
    * (ENCAO:  0-0+- |Nudny i przewidywalny;;Rodzinny| VALS: Humility, Hedonism >> Stimulation | DRIVE: Supremacja Nativis i Inferni)
    * "Pomagamy SWOIM, inni nie mają znaczenia. Pisałem się na pomoc swoim."

#### 2. Arkologia Nativis

* Rafał Kidiron
    * (ENCAO: 00+-0 |precyzyjny i zaplanowany;; DIMIR (UB);; Żywy wąż nie człowiek | Achievement, Power, Face | DRIVE: Duma i monument)
    * security lord i nieformalny dyktator arkologii
    * "Nasza arkologia zaszła tak daleko i nie możemy pozwolić, by cokolwiek stanęło na drodzę jej absolutnej wielkości."
    * "Tylko ci, którzy są przydatni mają miejsce w Nativis"
* Lycoris Kidiron: 57 lat (ze wspomaganiami drakolickimi dającymi jej efektywność 3x-latki)
    * (ENCAO: +0+-0 | Proaktywna;; Pierwsza w działaniu | Security, Achievement > Conformism | DRIVE: Kapitan Ahab (perfekcyjna arkologia))
    * ekspert od bioinżynierii i terraformacji, pionier (zdolna do przeżycia w trudnym terenie)
    * "Nikt, kto próbował się dopasować do innych nie doprowadził do postępu. Dzięki mnie Nativis będzie bezpieczna."
* Laurencjusz Kidiron
    * (ENCAO: +0--- |Żyje chwilą;;Nudny| Face, Power, Hedonism > Achievement | DRIVE: Przejąć władzę nad arkologią)
    * "Ta arkologia musi należeć do mnie. Ile może jeszcze być ograniczana przez Rafała? On nie jest tylko 'szefem ochrony'..."
* Kalia Awiter: influencerka z Nativis. Aktywnie próbuje pójść do łóżka z Eustachym i go podbić. Wierzy w Infernię. 21 lat. Drakolitka.
    * (ENCAO:  +--00 |Stoicka;;Radosna, cieszy się i daje radość;;Dzikie i spontaniczne pomysły| VALS: Universalism, Stimulation, Conformity | DRIVE: Zatrzymać czas, zrobić coś dobrego)
    * "Nativis musi być centrum kultury Neikatis. Musimy dać COŚ dlaczego warto żyć. Coś więcej niż tylko przetrwanie!"
* Marcel Draglin: oficer Kidirona dowodzący Czarnymi Hełmami
    * (ENCAO: --+00 |Bezbarwny, bez osobowości;;Bezkompromisowy;;Skupiony na wyglądzie| VALS: Family, Achievement >> Hedonism, Humility| DRIVE: Lokalny społecznik)
    * "Nativis będzie bezpieczna. A jedynym bezpieczeństwem jest Kidiron."

#### 2. Arkologia Nativis, bogacze

* Tobiasz Lobrak: (nie Nativis), 44 lata
    * (ENCAO:  ---0+ |Lekceważy obowiązki;;Skryty;;Marzycielski | VALS: Face, Power >> Face| DRIVE: TORMENT)
    * dostawca dużej ilości luksusowych środków do Nativis po niższej cenie
    * uwielbia znęcać się nad kobietami. Upodobał sobie Misterię.
    * "Możesz mnie zabić, ale będę ZAWSZE żył w twojej głowie, mała"
* Filip Serkion: 57 lat
    * (ENCAO:  0--+0 |Nieuczciwy;;Stanowczy;;Łagodny| VALS: Conformity, Family >> Stimulation, Power| DRIVE: Strach przed zapomnieniem)
    * bogaty acz niegroźny koleś; oddaje się przyjemnością i pozwala Kidironowi rządzić po swojemu
    * "To jest dobre miejsce dla dobrych ludzi. Czemu niszczysz piękno?"
* Karol Praczlik: 42 lat
    * (ENCAO:  0---+ |Zawłaszcza osoby i przedmioty dla siebie;;Dziecinny| VALS: Hedonism, Stimulation >> Face| DRIVE: Sprawne procesy)
* Maria Mirtel: 34, towarzyszka z Ambasadorki
    * (ENCAO:  +-0-0 |Jakoś to będzie.;;Zawłaszcza osoby i przedmioty dla siebie;;Ambitna| VALS: Benevolence, Family >> Face| DRIVE: Pieniądz -> odnaleźć rodzinę)
    * "Błagam, nie rób mi krzywdy..." "Muszę jakoś pracować, nic nie umiem poza byciem ładną i miłą. Inaczej mnie wyrzucą na piaski Neikatis..."

#### 3. Siły terrorystyczne

* Magda Misteria Sarbanik: czarodziejka mimikry i roślin, (33) 
    * (ENCAO:  +00-0 |Brutalna;;Łatwo zdobywa przyjaciół| VALS: Family, Tradition, Achievement >> Conformity| DRIVE: CORRUPTED SOUL)
    * "Kidiron zabrał mi WSZYSTKO. Zabiorę mu reputację, arkologię i odzyskam moje wiły!"
    * "Lirvint była w trakcie odbudowy. Udałoby nam się gdyby nie Wy!!!"
    * AGENDA: 1) zniszczyć Kidirona ujawniając sabotaż arkologii Lirvint, 2) ewakuować wiły, 3) zniszczyć Tobiasza
    * ŚRODEK: 1) magia krwi 2) rośliny + spory (Black Heart) 3) wiły  | blood heart
* 11 wił + 8 dziewczyn + 7 facetów
* 44 osób 

### Scena Zero - impl

.

### Sesja Właściwa - impl

Eustachy pobiegł na mostek Inferni po ciężką broń. No i jest wujek. Ostrzegł Kalię "coś się popsuło".

Kalia i Ardilla wchodzą do Ambasadorki. Tam coś się dzieje. Krzyk.

Ardilla próbuje odciąć szybą Ambasadorkę od resztę. Wiła ją atakuje. Kalia rzuca hologram siebie "gdzieś indziej" by odwrócić uwagę. Ardilla próbuje zamknąć szybę ZANIM wiły zadziałają.

* X: Ardilla i Kalia są w Ambasadorce.
* Og: Kalia odpaliła hologram ale ze stresu rzutowała siebie i jej holoprojekcja padła. Ujawniła swoją obecność. (+5Ob)
* Ob: wiły zmieniły sposób poruszania się. Zaczęły Was odcinać od wyjścia. Skupiają się na Kalii. Chcą ją TUTAJ. Co gorsza, rośliny zaczęły błyskawicznie szybko rosnąć.
* Ob: Ardilla jest zwinna. Kalia nie jest. Kalię oplótł jakiś bluszcz.
    * "Poddaj się, Infernianko, albo ona zginie". Ardilla widzi damę do towarzystwa, ślady przypalania. Ona kontroluje magię.

Ardilla się poddaje. Podeszła do niej wiła i lekko położyła szpon na ramieniu.

* Misteria: "Chcę odzyskać wiły z powrotem. Nasze wiły. I chcę coś do przebijania drzwi. I albo się wycofam stąd albo ta arkologia spłynie krwią."
* Ardilla: "Tylko tyle chcesz? To jakiś problem? Czemu te wiły są przetrzymywane?" Kalia też wyraźnie nie wie. "Jeśli Kidiron coś Ci zrobił, pomożemy"
* Kalia: "Kidiron by coś zrobił?"
* Ardilla: "Kidiron jak to Kidiron i Czarne Hełmy..."
* Misteria: "Kidiron zniszczył naszą arkologię i porwał nasze wiły."
* Ardilla: "To brzmi jak coś co Kidiron by zrobił, faktycznie"
* Misteria: "I w tym budynku jest człowiek który uczestniczył w rajdzie na naszą arkologię."
* Ardilla: "Najważniejsze jest wydostanie stąd wił. Pełno czarnych hełmów."
* Misteria: "I prasa! Kidiron zapłaci za swoją zbrodnię."

Misteria powiedziała cele:

* Ujawnić jakim złym człowiekiem jest Kidiron
* Dorwać złego człowieka który tu jest i który robił jej krzywdę
* Zabrać wiły ze sobą.

.

Misteria: "Jestem tutaj w tym miejscu od 4 lat. W Ambasadorce. Szukałam sposobu jak wydostać wiły bezpiecznie i cicho. I ujawnić zbrodnię Kidirona." (spojrzała na ślady przypalania). Misteria nie planowała ataku teraz. Misteria do Ardilli "JAK mam się schować z wiłami w kanałach?" Po chwili taka złość jej przeszła. Wiła podeszła i łagodnie pogłaskała po ramieniu.

TYMCZASEM EUSTACHY

Eustachy włączył alarm na Inferni. Tymon "jestem zajęty poważnymi rzeczami!" Eustachy "jak ja się zaraz zajmę, jest alarm, robota, idziemy do Ambasadorki, impuls magiczny. Bierz fanty, pompkę masowego zniszczenia i lecimy!" Eustachy widzi na monitorze nagranie "z torebki" Kalii. Ona nagrywa wideo. Zespół leci ścigaczem do Ambasadorki, jedzie też pancerny wóz Czarnych Hełmów. Porządny i pancerny.

* Wujek: "Czy jesteś w środku?"
* Ardilla: "Tak"
* Wujek: "Czy coś Ci grozi?"
* Ardilla: "Nie, ale wiły chcą wydostać się z arkologii"
* Wujek: "Posłuchaj, to poważna sprawa. Zaproponuj czarodziejce, że Ty wyjdziesz a ja wezmę Twoje miejsce."
* Ardilla: "Poradzę sobie wujku, spokojnie"
* Wujek: "Nie byłaś w tego typu operacjach."
* Ardilla: "Kiedyś trzeba zacząć."
* Wujek: "Ardillo, to rozkaz, potem porozmawiamy."
* Ardilla: (rozłącza się)
* Wujek -> Eustachy: Ardilla jest pod kontrolą przeciwnika.
* Eustachy: I co ja mam z tym zrobić?
* Wujek: Jeśli spróbujesz ją uratować, może odmówić. Może z Tobą walczyć. Mag biologiczny ma dostęp do feromonów. Zakładasz maskę.
* Wujek: Działamy delikatnie, Twoja randka też tam jest.
* Wujek: Musimy nawiązać kontakt i usunąć maga zanim dojdzie do tragedii. Atak na Korkorana jest nieakceptowalny.

W tej arkologii jest ciężkie uzbrojenie i Infernia wie gdzie. Wujek -> Infernia. Nie pozwala Rafałowi Kidironowi na zalanie ogniem.

* Kidirion -> Eustachy: Pamiętasz, że powiedziałem, że robimy to czego chce arkologia a nie to czego my chcemy.
* Eustachy: A czego teraz chce arkologia?
* Kidiron: Są tam bardzo wartościowi ludzie. Nie możemy ich zabić. A najlepiej, jeśli czarodziejkę weźmiemy żywcem.
* Kidiron: Jeśli myślisz że będę płakał jak dziewczyna która podniesie rękę na Twoją kuzynkę zginie to myślisz że będę płakał?
* Eustachy: Może umrzeć. Nikt nie będzie płakał.
* Kidiron: Mi będzie szkoda, bo jest cennym zasobem.
* Kidiron: KAŻDEGO da się przekonać. Potrzebujesz jedynie odpowiedniej technologii.

Ambasadorka jest zamknięta, nie jest ufortyfikowana.

Kalia kłóci się z Ardillą. Zdaniem Kalii Kidiron prowadzi arkologię do przyszłości. Zdaniem Ardilli, Kidiron prowadzi arkologię do zagłady.

Ardilla przekonuje Kalię. Przedstawia jej polecenia Kidirona i to co słyszała i złapała. Bo Ardilla jest WSZĘDZIE.

Tr (niepełny) Z (fakty) +3 +4Og:

* Og: Kalia retransmituje dane. Żeby nie działo się nic głupiego. By Kidiron nie mógł zrobić "żelaznej pięści" itp. Ale to znaczy, że mowa Ardilli o Kidironie wychodzi.
    * WSZYSCY praktycznie będą uważać, że to CHYTRY PODSTĘP INFERNIANKI lub wpływ wiłowego maga by zwieść MROCZNĄ CZARODZIEJKĘ.
    * Ardilla nie wie że to retransmitowane więc mówi szczerze
    * Kidiron -> Eustachego: "popatrz, jak pod wpływem środków można wypaczyć prawdę..."
    * Eustachy: "narkotyki"
    * Kidiron: "dokładnie. Narkotyki."
* Vz: Kalia jest przekonana, by ujawnić prawdę.
    * Kalia: "nie wierzę w to, że Kidiron robi te wszystkie rzeczy. Ale jeżeli masz dowody, czarodziejko, retransmituję to by arkologia mogła to ocenić."
    * Misteria: "nazywam się Misteria. I nie wiem jak możecie nie wierzyć... to jest KIDIRON, przecież widać co on robi!"
    * Kalia: "Kidiron uratował mnie z umierającej arkologii."
    * Misteria: "a dlaczego ta arkologia umierała? Zwykle Infernia ratuje ludzi, nie Kidiron."

.

* Wujek -> Eustachy: Sytuacja jest dziwna. Wszystko wskazuje na to, że Ardilla jest pod kontrolą przeciwnika, ale Kalia nie. To nie ma sensu.
* Eustachy: Ardilla jest większym zagrożeniem niż Kalia
* Wujek: Owszem, ale Kalia jest cenniejszym zakładnikiem niż Ardilla. Dla Kidirona. Nie dla NAS.
* Wujek: Jeśli jesteś w stanie, uspokój przeciwnika. Zinfiltrujemy to.

Siły Kidirona i Eustachy z Tymonem otoczyły budynek (uniemożliwiając jakieś dziwne rzeczy). Tymon się przyznał że jest droga do podglądania panienek w Ambasadorce.

NEGOCJACJE:

Misteria powiedziała cele:

* Ujawnić jakim złym człowiekiem jest Kidiron
* Dorwać złego człowieka który tu jest i który robił jej krzywdę
* Zabrać wiły ze sobą.

Kidiron:

* Chcesz reportaż, zrób reportaż
* Możesz zostać lojalną obywatelką arkologii i zapomnimy o sprawie

Kidiron jest przekonany, że Misteria nikogo nie zabije. Nie ten profil.

Eustachy ma pomysł jak to dobrze sabotować. W kontrolowany sposób obniżyć poziom tlenu by ludzie w środku byli otępiali i nieco nieogarnięci. I nagle móc zrobić im szok temperatury (cieplej) by rozszerzyć naczynia krwionośne.

Ex Z (bo masz sprzęt i kontrolę nad nim) +4:

* V: Sukcesywnie ten plan zadziała. Potrwa, ale zadziała.
* X: Część wił będzie bardzo ciężko uszkodzona.
* Xz: Magia Misterii uniemożliwi jej unieszkodliwienie. Krwawo.

Ardilla przekonuje Misterię, by faktycznie wymienić wszystkich rannych na wujka. Wujek jest lepszym zakładnikiem a im się najwyżej może stać krzywda. Wujek ma przyjść związany. Zgodził się. Wujek updatował Eustachego. Nadal aprobuje plan Eustachego, ale chce chronić swoją Ardillę i kontrolować sytuację.

Wujek wszedł, usiadł przy ścianie i się odprężył.

Ardilla usłyszała jakby cichy głos. 

* Ralf: "Ardillo? To Ralf. Jesteś tu? Jesteś bezpieczna?"
* Ardilla: "Gdzie jesteś?"
* Ralf: "Podglądam"
* Ardilla: "Gdzie jesteś?"
* Ralf: "Blisko. Nie martw się."

To, że uwaga jest na wujku pomaga Ralfowi. Ralf dowiedział się co i jak. Kazał się dowiedzieć Ardilli czy da się ewakuować ową czarodziejkę czy Ardillę.

-----------

AGENDY:

* Misteria (Magda Sarbanik)
    * AGENDA: 1) zniszczyć Kidirona ujawniając sabotaż arkologii Lirvint, 2) ewakuować wiły, 3) zniszczyć Tobiasza
    * ŚRODEK: 1) magia krwi 2) rośliny + spory (Black Heart) 3) wiły  | blood heart
* Marcel Draglin
    * AGENDA: 1) pojmać Misterię
    * ŚRODEK: 1) snajperzy 2) podkładamy bomby 3) granaty (gazowe, odłamkowe)
* Rafał Kidiron
    * AGENDA: "this is all irrelevant". 1) wzmocnić link z Eustachym 2) pokazać siłę arkologii ("jesteśmy atakowani") 3) pojmać Misterię
    * ŚRODEK: czas, "Mglisty Smok", kontakty poza arkologią, w ostateczności Trianai
* Wujek Bartłomiej
    * AGENDA: wydobyć Ardillę bezpiecznie
    * ŚRODEK: serpentian spike
* Ralf
    * AGENDA: wydobyć Ardillę bezpiecznie
* Kalia Awiter
    * AGENDA: nikomu nic złego się nie może stać, peacekeeper
* Tobiasz Lobrak
    * AGENDA: 1) pozyskać Misterię za wszelką cenę 2) przetrwać
    * "Nawet jak mnie zabijesz, będę wiecznie żył w Twojej głowie, mała"

RUCHY:

1. Misteria pracuje nad Krwawym Sercem (N> blood)
    1. -> Misteria była torturowana i przypalana, MUSI uciec, Tobiasz MUSI zginąć
    2. Kidiron współpracuje z Tobiaszem, on coś dostarcza ważnego
2. Ralf linkuje Ardillę i Eustachego
3. Marcel przygotowuje operację 'intruzja'. Jeśli wiemy gdzie jest Misteria, możemy użyć superciężkiego lasera by ją ustrzelić
    1. Ktoś musi pozycjonować Misterię w odpowiednie miejsce
    1. Nie działa w panic room
4. Ralf i Lertysowie z woli Ardilli zajmują się ewakuacją w głąb arkologii (acz bez Inferni nie uda im się uciec)
    1. Trzeba odwrócić uwagę Kidirona, Marcela.
    1. Trzeba przekonać Misterię by SAMA uciekła, wiły nie mają szans.
    1. Trzeba przekonać wujka
99. DARK FUTURE
    1. Wujek: gdy Misteria traci kontrolę nad magią, _hyperform spike_ (terrorform); zabija Misterię kosztem zdrowia.
    2. Misteria: one with the Heart after death; becomes a monster to kill
    3. Kalia: ciężko ranna, potrzebuje pomocy
    4. Tobiasz: nic mu się nie dzieje
    5. Ralf: wchodzi do walki

-----------

Ardilla widzi, jak Misteria pilnuje 5 wiłami wujka. Wujek wygląda niegroźnie. Misteria składa Czarne Serce. Kosztem 2-3 wił. I wykończenia swojej energii. Zaklęcie zmienia jej sposób myślenia. 

Ardilla: "jak przekonamy ludzi o winie Kidironów, pomogą nam się wydostać. A z Kalią mamy prawdziwy reportaż". Misteria: "Zrobimy to... zdecydowanie musimy to zrobić." Kalia przygotowuje oświetlenie itp, pomagają jej kelnerzy i osoby z Ambasadorki.

* Ralf: "Rozmawiałem z dowódcą Czarnych Hełmów. Ma ciężki laser. Jeśli przesuniecie czarodziejkę (z trudem to powiedział) w miejsce delikatne, Hełmy ją zestrzelą."
* Ralf: "Ręka z nowotworem będzie odcięta."
* Ralf: "Przekażę."

Tobiasz przestraszył Misterię, Ardilla dowiedziała się jaką Tobiasz ma agendę i że już Misterię skrzywdził...

Do Eustachego zwrócił się Kidiron. Powiedział mu sytuację. Decyzja Eustachego: czekamy na rozwój sytuacji.

Ralf zestawił połączenie Eustachy - Ardilla.

* Ardilla: "Dobrze Cię słyszesz mam wrażenie że nas nie rozstrzelisz!"
* Eustachy: "Chciałęm powiedzieć że nie będize bolało"
* Ardilla: "DZIĘKI"
* Eustachy: "A co się stało? Wchodzimy z ogniem lub ogniem i mieczem"
* Ardilla: "Gadam z Misterią, ona chce się z wiłami ewakuować, robili im straszne rzeczy"
* Eustachy: "Dowody?"
* Ardilla: (dowody że Tobiasz to straszny pojeb)
* Ardilla: "Pomóż nam znaleźć drogę wyjścia! Żeby nie trafiła w łapy Kidirona!"
* E->K: czy to prawda?
* Kidiron: (powiedział prawdę, że Tobiasz to PODŁY człowiek)
* Eustachy: argumenty czemu go nie zabijać?
* Kidiron: (waha się) Tobiasz zdradził Syndykat. Jest podwójnym agentem.
    * dzięki niemu są neuroobroże, dzięki niemu jest jedzenie i intelligence
    * jeśli go nie będzie? 5% ludzi może zginąć
    * "Kidiron robi to dla arkologii, nie bo chce"
* Eustachy: Robimy to bo musimy (nie popieram tego, odjebałbym śmiecia), pomogę Wam w ewakuacji, ale Tobiasz musi przetrwać - ma kwity i jest potrzebny arkologii. Tracimy 5% ludzi arkologii.
* Ardilla: Musimy wydostać stąd ludzi.
* Wujek -> Eustachy: "nie zgadzam się że arkologia musi przetrwać jeśli robi takie rzeczy. Nie wierzę, że to potrzebne. Kidiron może nie mówić prawdy."
* Eustachy: Jeśli nie mówi prawdy, możemy go zabić w dowolnym momencie. Skażemy go w cywilizowany sposób.
* Wujek: (chwila milczenia) co proponujesz?
* Eustachy: ewakuujemy pokrzywdzone, "tylne wyjście", zabunkrujemy na Inferni i odstawimy. A by nie robiły problemów, wyrwiemy chwasta by sprawiedliwości. Ewakuujemy je Infernią i potem je schowamy.

Kidiron jest skłonny pozwolić Misterii odejść jeśli zarzuci głupich działań. Zgadza się też na to, by Misteria współpracowała z Arkologią w roli czarodziejki a nie damy do towarzystwa. Chyba że chce. Ale "nie pracuje" -> "nie je". Kidiron jest skłonny dać Misterii immunitet przed Tobiaszem. Eustachy przekazał wszystko Ardilli. A ona Kidironowi nie ufa.

Eustachy "potrzebuję miejsca do przeprowadzenia operacji". Czy Kidiron może usunąć Czarne Hełmy. Zgodził się - bo Eustachy dał słowo, że nic złego się nie stanie Kalii.

Kalia robi reportaż mający pokazać ZŁO W SERCU KIDIRONA i prawdziwą historię arkologii Lirvint (z której pochodzi Misteria).

Ex Z (dowody) +3 +3Og (inny uchodźca z Lirvint):

* X: Kidiron FAKTYCZNIE jest tym co doprowadził tą arkologię do sukcesów. Zimny drań, ale oddał życie tej arkologii. I ludzie bardziej boją się świata niż jego.
* V: Kidiron dostał cios reputacyjny. FAKTYCZNIE to się ujawniło.
* X: aura: "Kidiron jest zbyt groźny by go próbować pozbawić władzy, jeśli takie rzeczy robi to co zrobi oponentom"
* (+1Vg) wycinamy elementy i puszczamy w formacie memowym. Taki PUTIN. 
* Vz: pojawi się opozycja przeciwko Kidironowi i ruch oporu
    * im lepiej w arkologii i im szczęśliwsi są ludzie, im mniej się boją (a Kidiron próbuje) tym większy opór przeciw Kidironowi
* (Vg): Kalia jest skonfliktowana odnośnie roli Kidirona. Kalia ma dobre serce. A Kidiron jest wyraźnie potworem. "Po naszej stronie", ale potworem.
    * Kalia pochodzi z tej samej arkologii co Misteria.

Kidiron z uśmiechem "To nie ma znaczenia. Oni wszyscy o to nie dbają. Chcą bezpiecznej arkologii a ja ją zapewniam".

Kalia zrobiła request do swoich simpów by lajkowali. Celem jest to by pokazać Misterii że jest odzew. Jest poparcie. Da się stąd wydostać, jest poparcie ludzi. Kidiron nie może ich skrzywdzić bo wyjdzie na potwora.

Tr Z (fani Kalii i jej retoryka) +3:

* V: Kalia udowodniła Misterii, że reportaż coś zmienił (więcej niż naprawdę)
* VV: Misteria pozwala Kalii odejść

Gdy Kalia wybiegła zestresowana (słodząc sobie krzyżowo z Ardillą), Kidiron ją przytulił. Potem się oddalił i Hełmy się zbierają. 

Eustachy składa trasę (częściowo po kanałach częściowo na ulicy) by ewakuować Misterię i wiły. Założenie że Ralf wysadzi dziurę w ziemi.

TrZ+3:

* Vz: istnieje trasa która na pewno działa.
* X: Kidiron obserwuje trasę by nagrać "odlecieli"
* X: Wiła ma tracker XD. Kidiron wie co i gdzie
* Vg: Misteria jest przekonana, że ta trasa jest bezpieczna i może odlecieć Infernią.

Kidiron "odejdźcie bezpiecznie, daję Wam prawo bezpiecznego odejścia i póki nic nie robią przeciwko Nativis i jemu póty będą bezpieczne."

* Misteria patrzy na Ardillę "jeśli odejdę, on mnie złapie. A jestem blisko. Zniszczę go. Mam tak blisko!"
* Ardilla: "Jeśli go teraz zniszczysz to Kidiron skorzysta z oświadczenia i Cię zdejmie!"
* M: "Ale jeśli tego nie zrobię, znajdzie mnie i złapie!"
* Ardilla: "Wydostaniemy Cię spoza jego zasięg." (śmiech z panic roomu)
* M: "Chodź, pokażę Ci"

Misteria pokazała Czarne Serce. Chce nim spalić Tobiasza, zniszczyć jego układ nerwowy.

* Ardilla: "Nie przeciągniemy czasu, jak wszystko jest przygotowane do wyjścia. Rozumiem że się go boisz i chcesz się go pozbyć, ale... nie są dla Ciebie wiły ważniejsze? Twoja arkologia?"
* M: "Tak, są ważniejsze!"
* Ardilla: "Serce Cię zniszczy, nie wyjdziesz stąd. Nic nie osłoni Cię przed Kidironem. Będąc w środku jego terenu jest na to przygotowany."

"Jesteś na skraju sił, słabniesz, a on wyjdzie i po Ciebie przyjdzie i nie wiem czy coś możemy zrobić."

Tr (Z działania itp) +3 +3Or:

* X: Misteria jest rozedrgana. Część wił jest nieprzytomna. Ona jest niedotleniona. "MUSZĘ!" (+2Or)
* X: Misteria odmawia opuszczenia tego terenu bez rozwiązania Tobiasza. Musi mieć _coś_. Krew, coś od niego. Nie rozmontuje Serca.
* X: Misteria odmawia. Po prostu.

ZMIANA KONFLIKTU (+3V):

* XOrX: Misteria i Ardilla się kłócą. Wiły skupiają się na nich.
    * nagle wujek wbija coś w Misterię. Strzykawka. Misteria jest nieprzytomna.
    * wujek jest przytomny, nie stoi, jest "na czworakach", wiły są poduszone itp.
        * "nic jej nie będzie... ewakuuj ją. Je. Wszystkie."
        * potrzebuje pomocy medycznej. asap.

Ludzie z Inferni i Czarne Hełmy wspólnie ewakuują nieprzytomnych na Infernię. Drony, reportaż i Kalia towarzysząca Ardilli. Faktycznie, Kidiron dotrzymał słowa i ewakuował Misterię. Ale Czarne Hełmy wpakowały jej tracker. Kidiron w końcu chce być poinformowany...

## Streszczenie

Kalia i Ardilla weszły do Ambasadorki by pomóc jeśli są w stanie, odcięły ich wiły i Ardilla ich poddaje. Misteria - napastniczka - okazuje się być czarodziejką roślin. Ona chce odzyskać wiły i zarzucić Kidironowi zniszczenie arkologii oraz porwanie wił. Jej celem jest ujawnienie prawdy o Kidironie, ukaranie Tobiasza (patrona Ambasadorki który robi złe rzeczy) i zabezpieczenie wił. Wujek chce zastąpić Ardillę, ale ona odmawia. Zespół (poza Ardillą) planuje zinfiltrować Ambasadorkę i usunąć maga (Misterię). 

Eustachy wpada na pomysł kontrolowanego obniżenia poziomu tlenu wewnątrz. Ardilla przekonuje Misterię do wymiany rannych na wujka. Ralf stanowi kanał komunikacyjny. Misteria tworzy Czarne Serce, kosztem wił i energii. Planują przekonać ludzi o winie Kidironów, wydostać się z Ambasadorki i stworzyć reportaż z Kalią. Ralf mówi o laserze na dachu, którym może zniszczyć czarodziejkę. Tobiasz wzbudza w sercu Misterii terror. Eustachy i Kidiron oczekują na rozwój sytuacji. W wyniku negocjacji, Kidiron zgadza się współpracować pod warunkiem, że Misteria nie będzie działać przeciwko niemu czy arkologii.

Kalia tworzy reportaż o Kidironie i arkologii Lirvint. Kidiron obiecuje bezpieczne wyjście dla Misterii i wił, pod warunkiem zapewnienia nieagresji. Ardilla próbuje przekonać Misterię do ewakuacji, ale ona nie chce opuścić miejsca bez rozwiązania sprawy z Tobiaszem. Czarne Serce wpływa na Misterię, więc wujek ją obezwładnia wstrzykując jej jakieś świństwo, sam zostaje poraniony przez wiły. Kidiron dotrzymał słowa odnośnie ewakuacji, acz postawił na Misterii tracker.

## Progresja

* Magda Misteria Sarbanik: ma na sobie tracker Kidirona, jest przerażona Tobiaszem i jego możliwościami. Wycofała się na pustynię Neikatis, tam będzie dość bezpieczna ze swoją magią. Chyba.
* Tobiasz Lobrak: wie, że Misteria chciała go zabić. Od tej pory Tobiasz skupia się na próbie pozyskania Misterii i założenia jej neuroobroży. She WILL comply.
* Rafał Kidiron: ujawnione zostało, że robił złe rzeczy w arkologii Lirvint. Pojawiła się silna opozycja wewnętrzna i ruch oporu. Im lepiej w arkologii i im szczęśliwsi są ludzie, im mniej się boją (a Kidiron próbuje by tak było) tym większy opór przeciw niemu.
* Rafał Kidiron: jedyna osoba na której mu naprawdę zależy i którą naprawdę lubi - Kalia Awiter - została ujawniona jako jego "słaby punkt".

## Zasługi

* Eustachy Korkoran: ostrzegł Kalię o problemie, zdobył broń, aktywował alarm na Inferni i przeprowadził okrążenie budynku Ambasadorki. Przyczynił się do rozwiązania konfliktu poprzez pomysł obniżenia poziomu tlenu oraz mediację między Kidironem i Ardillą.
* Ardilla Korkoran: weszła do Ambasadorki razem z Kalią; została pojmana przez Misterię, ale przekonała ją do wymiany rannych na wujka. Próbowała negocjować ewakuację Misterii i innych osób. Przekonała Misterię by wymienić rannych na wujka i stanowiła serce zespołu; bez niej Misteria zostałaby zestrzelona lub zabita. Współpracując z Kalią doprowadziła do tego, że nikomu nic się nie stało. Współpracując z Misterią i Kalią doprowadziła do ruchu oporu przeciw Kidironowi.
* Kalia Awiter: dzielnie weszła ratować Ambasadorkę, później stworzyła reportaż mający na celu ujawnienie prawdy o Kidironie i arkologii Lirvint mimo niechęci do Misterii. Współpracowała z Ardillą i pomogła ewakuować osoby z Ambasadorki, będąc gwarantką że nic złego się Misterii nie stanie z ręki Kidirona. Mimo, że jest po stronie Kidirona, współpracując z Misterią stworzyła ruch oporu przeciw Kidironowi.
* Magda Misteria Sarbanik: pragnęła odzyskać swoje wiły i ujawnić zbrodnię Kidirona. Tworzyła Czarne Serce, co zmieniało jej sposób myślenia. Ostatecznie została ewakuowana z Ambasadorki, ale Kidiron umieścił w niej tracker.
* Ralf Tapszecz: w jakiś sposób dał radę nawiązać połączenie z Ardillą gdy ona była w Ambasadorce pełznąc korytarzami. Nawiązał link Eustachy - Ardilla i zadbał o bezpieczeństwo wszystkich.
* Bartłomiej Korkoran: chciał zastąpić Ardillę jako zakładnik, ale nie wyszło. Więc wymienił się na grupę rannych ludzi. W momencie w którym Misteria już nie była racjonalna zaatakował ją i obezwładnił, ciężko ranny przez wiły. Dzięki temu pomógł doprowadzić do ewakuacji wszystkich z Ambasadorki.
* Rafał Kidiron: wierzył, że to fundamentalnie nie ma znaczenia, że wyszły na jaw jego mroczne ruchy w arkologii Lirvint. I faktycznie, nie miało w samej arkologii, ale pojawił się ruch oporu. Został oskarżany przez Misterię o uszkodzenie arkologii Lirvint i porwanie wił. Ostatecznie zgodził się na współpracę, pod warunkiem, że Misteria nie będzie działać przeciwko niemu i arkologii. Umieścił tracker w Misterii, aby monitorować jej ruchy. Zimny jak zawsze, korzystał z okazji by mocniej pokazać Eustachemu że takie działania są potrzebne dla tej arkologii.
* Tymon Korkoran: regularnie próbował się wkraść do Ambasadorki podglądać dziewczyny i znalazł ścieżkę którą powiedział Ralfowi. Nie wiadomo czy być z niego dumnym czy nim gardzić.
* Tobiasz Lobrak: straszy Misterię, że ona wpadnie w jego ręce. Obiecuje jej to. Zamknięty w panic roomie, Kidiron go wyciągnął. Ma powiązanie z Syndykatem Aureliona, ale chyba gra na dwa fronty bo Kidiron daje mu okazję zaspokojenia mroku.

## Frakcje

* Imperium Kidirona w Nativis: ujawniono, że Kiridon robił złe rzeczy w arkologii Lirvint. Pojawił się ruch oporu przeciwko Kidironowi - im lepiej w arkologii i im szczęśliwsi są ludzie, tym większy opór. 
* Czarne Hełmy Nativis: byłyby w stanie zniszczyć Misterię (napastniczkę), ale Kidiron wolał by Eustachy to rozwiązał z większą subtelnością.

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Nativis
                        1. Poziom 3 - Górny Środkowy
                            1. Wschód
                                1. Dzielnica Luksusu
                                    1. Ambasadorka Ukojenia
                    1. Arkologia Lirvint: kiedyś słynęła z rozkoszy i elegancji, ale miała poważne problemy gdy rozszarpały ją wiły z działań Kidirona.

## Czas

* Opóźnienie: 1
* Dni: 1

## Inne

### Układ Kolonii Coruscatis

1. Kolonia Coruscatis: (167 osób załogi)
    1. Kopuła rdzenia: (życie, zabawa, medical, leisure, sleep, center)
    1. Moduł fabrykacyjny: (północ, 15 załogi, magazyny, materiały)
    1. Farma: (NE, magazyny ziarna i jedzenia)
    1. Biolab
    1. Bateria dział defensywnych
    1. Elektrownia
    1. Magazyny

### Układ arkologii Nativis

* Aspekty:
    * Bardzo dużo roślin, drzew itp. Piękne roślinne miejsce.
    * Bardzo spokojna arkologia, silnie stechnicyzowana i sprawna.
    * Dominanta: faeril > drakolici, ale próba balansowania. Niskie stężenie AI.
    * BIA jako centralna jednostka dowodząca, imieniem "Prometeus"
* Struktura:
    * Wielkość: 4.74 km kwadratowych, trzy poziomy
    * Populacja: 11900 (Stroszek)
        * Operations & Administration: 1100
        * Extraction & Manufacturing: 3213
        * Medical: 355
        * Science: 360
        * Maintenance: 700
        * Leisure: 1000
        * Social Services: 240
        * Transportation: 800
        * Training: 229
        * Life Support: 500
        * Computer & TAI & Communications: 500
        * Engineering: 500
        * Supply / Food: 1200
        * Security: 300
    * Bogactwo: wysokie. Technologia + piękna roślinność.
    * Eksport: żywność (przede wszystkim), komponenty konstrukcyjne, hightech, terraformacja
    * Import: luxuries, prime materials, water, terraforming devices, weapons
    * Manufacturing: jedzenie, konstrukcja, terraformacja
    * Kultura: 
        * dominanta: piękno, duma z tego co osiągnęli, duma z tego że nie ma wojny domowej faeril/drakolici
        * wyjątek: brak.
    * Przestrzenie
        * rozrywki: parki, natura, zdrowe ciało - zdrowy duch
        * inne: hotele, laboratoria terraformacyjne, restauracje...
    * Zasilanie
        * solar + microfusion
    * Żywność
        * różnorodna; to miejsce jest źródłem żywności
        * przede wszystkim roślinna, ale też zawiera elementy zwierzęce (te najczęściej z importu)
    * Life support => terraformacja + filtracja
    * Comms => biomechaniczne okablowanie + normalne sieci
    * TAI => BIA wysokiego stopnia imieniem Prometeus
    * Starport => large
    * Infrastructure => high-tech, dużo półautonomicznych TAI, wszystko nadrzędne przez Prometeusa. Świetny transport publiczny. 
    * Access & Security => arkologia z silnymi blast walls, silnymi liniami dostępowymi.

Historia: 

* w trakcie wojny domowej sprzedawała żywność na 2 fronty co spowodowało znaczne bogactwo ale też częściową niechęć innych arkologii
* obecnie prowadzi ekspansje na struktury zrujnowane/opuszczone po wojnie żeby całkowicie się uniezależnić od dostaw spoza planety
