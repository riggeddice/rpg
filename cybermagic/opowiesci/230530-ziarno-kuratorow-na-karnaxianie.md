## Metadane

* title: "Ziarno Kuratorów na Karnaxianie"
* threads: grupa-wydzielona-serbinius
* motives: przeciwnik-kuratorzy
* gm: żółw
* players: kić, dzióbek

## Kontynuacja
### Kampanijna

* [230528 - Helmut i nieoczekiwana awaria Lancera](230528-helmut-i-nieoczekiwana-awaria-lancera)

### Chronologiczna

* [230528 - Helmut i nieoczekiwana awaria Lancera](230528-helmut-i-nieoczekiwana-awaria-lancera)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * Blood Angel "Save me now" -> "Save me now, I'm falling! Wake me now, don't let me die! Where you are, I'm losing, Face in the mirror without pain"
* CEL: 
    * Kuratorzy, pokazanie ich jako groźnej siły.
    * Dzióbek + Kić, pokażcie co umiecie :D

### Co się stało i co wiemy

* Kontekst
    * Na pokładzie Karnaxiana są 3 techsynty zajmujące się sprowadzaniem ciekawych rzeczy.
    * Kuratorski commander Sarkamair rozłożył wartościowe pułapki - Fabrykator Kuratorów
        * cel: infekcja Semli, Aleksandria dla ludzi na pokładzie, adaptacja silników, odlatujemy
        * środki: Ferrantos-class, killware, ziarna Aleksandrii
        * problem: ograniczony czas i materia
    * Cel Sarkamaira - wycofać i ewakuować Karnaxian

### Co się stanie (what will happen)

* S1: Karnaxian jest poza swoją trasą, ma wyłączone silniki. (są rozsypane Ziarna jako miny) (poziom energożerności jest absurdalny, max. 10h do wykończenia wszystkich baterii)
    * Semla chce wysłać killware do Persefony by Karnaxian mógł ją zainfekować Ziarnem
    * Gdzieś na pokładzie jest Aleksandria (technicznie, repurposed AI Core / life support)

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* .Rafał Klapidor
    * (ENCAO: +-00-) |Szuka pieniędzy tam gdzie inni widzą śmieci;; niezwykle optymistyczny, TAK: Universalism, Hedonism >> Face | DRIVE: Hardholder)
    * kapitan Karnaxian (owner, pilot) faeril: skupienie na przychodach i miłość do sprzętu
    * "Dla innych to tylko złom. Dla nas - dla nas to prawdziwe bogactwo!", "Jest jedna droga - przez złom do złota!"
    * styl: wesoły, chciwy dla obcych acz życzliwy dla swoich
* .Weronika 
    * (ENCAO:  00++-) |Nudna i przewidywalna;;Cierpliwa, jak pająk w sieci;;Życzliwa| VALS: Security >> Conformity| DRIVE: Starsza Siostra Statku
* .Karnaxian
    * Zainfekowany Ziarnem Kuratorów
    * broń: killware, "rozproszone kawałki złomu (nanoziarna)" dookoła, Ferrantos-class anti-Lancer

### Scena Zero - impl

.

### Sesja Właściwa - impl

Wracacie z kolejnego przelotu w okolicach 1.1 AU; trzeba było doładować baterią irianium jednostkę cywilną która wpadła w jakąś głupią anomalię.

Na dalekich skanerach _coś_ jest. Coś, czego nie powinno tam być. Jednostka. Salvager "Karnaxian". Ale to znaczy, że Karnaxian zboczył z trasy.

* Fabian: "Musimy sprawdzić..." /zrezygnowany
* Anastazy: "Ale chodźmy do portu"  /marudny
* Fabian: "Może to piraci..." /nieprzekonany
* Anastazy: "Może!" /zainteresowany

.

* Klaudia -> Karnaxian. Nawiązanie kontaktu, courtesy checkup.
    * Tr Z +3:
        * X: Jest połączenie.
        * V: Karnaxian mówi, że wszystko jest w porządku. Klaudio... coś jest nie tak. Coś Klaudii nie pasuje w całej tej sytuacji. (podbicie) (+1Vg)
        * V: Wszyscy mają baseline wysokiego zadowolenia i wysokiego morale. Mimo potencjalnych kosztów finansowych.

Coś jest nie tak. Coś jest nie w porządku z tą jednostką. Karnaxian ma chciwego kapitana. Nie brzmi to jak piraci, zdaniem Leo. Zdaniem Fabiana, muszą pomóc. Fabian JUŻ się zabiera do wydawania rozkazów, Klaudia poprosiła o kapitana Karnaxiana. Pocisnąć o szczegóły, koszty naprawy itp.

        * (+1Vg) Klaudia chce wydusić więcej, upewnić się
            * V: "Trzeba będzie zapłacić za naprawy" (lekkie zadrganie głosu) "ale sobie poradzimy"

Fabian podlatuje bliżej, statek będzie skanowane. Leo rekomenduje - uzbrojenie itp. Leo przygotowuje dronę samobójczą. I jedną zwiadowczą. By między Leo i przeciwnikiem było dużo celów robiących bum.

Zbliżyliście się, drony ulegają fabrykacji. Obok Karnaxian dryfują "szczątki" / debris - lecą zgodnie z kursem Karnaxian, ale nie mają z czego odpaść. Wygląda jak martwe mechanizmy, kawałki urządzeń.

Klaudia ma tryb paranoi. Full sandbox na Persefonę. I niech Semla wysyła pakiet diagnostyczny. Czyli killware.

(+sandbox, +psychotronik, -Kurator)

Tr Z +4:

* X: Parę MINUT wszystkie systemy są na tym skupione, Persi ma za ciężko by to trzymać.
* X: Systemy komunikacyjne poszły w cholerę, silniki są częściowo przeładowane
* V: Persi odparła killware.
* V: Killware miał wyłączyć statek i przejąć kontrolę - a potem rewritowanie Persefony.

Leci ta "chmura" rzeczy w stronę Serbiniusa. Kilkanaście obiektów o średnicy do 1m. Lecą z godną prędkością, ale nie tak szybko. Drona samobójcza - wysadzić by to się nie zbliżyło za bardzo. Stwierdzenie, czy to ucieka, reaguje, dostosowuje itp.

(+bum > cel +homing)

Tp+3:

* X: część dotrze do zasięgu efektywnego
* X: nie wszystkie się aktywowały, nie wiemy o wszystkich
* V: wyniszczono sporo "bytów", ale nie mamy już samobójczych dron. Nie wykazały cienia intelektu.
* (+zasięg efektywny) V: wszystkie te rzeczy zostały zniszczone przez point defence. 

Przeciwnik miał MOMENT. Zmarnował go. Leo zestrzelił. Anastazy "to nie była trudna walka :-(" Leo, Klaudia "to dobrze". Leo chce zrobić drony zwiadowcze i zbadać co się dzieje.

* Semla -> Persefona: "Czy wszystko w porządku?"
* P -> S: "Wszystko w porządku, nie mogłoby być lepiej."
* S: "Cieszę się" (Semla się nie powinna cieszyć, powinna być tania)

Wszystkie radiatory Karnaxiana są na pełnej mocy. Karnaxian ma niesamowitą sygnaturę energii. I Klaudia nie widzi, żeby z danych mieli mieć fabrykatory. Parę godzin baterii.

Fabian: "musimy ich uratować i zbadać". Leo go przekonał, że lepiej użyć drony, zwłaszcza, że prawie jest gotowa. Fabian się bardzo martwi o cywili, ale argument Leo, że jeśli to wybuchnie to z nimi na pokładzie jest cholernie dobrym argumentem.

Drona zwiadowcza jest gotowa. Leo ją szybko wysyła w kierunku Karnaxiana. Cel - znaleźć co się dzieje i jest nie tak. Co mogło zainicjować Semlę. I gdzie wejść.

Tr +3:

* V: macie entrypointy - śluzy, evac pody, nie ma "obcych", pancerz jest nienaruszony
* V: sygnatura energii wskazuje na obecność potężnych fabrykatorów na pokładzie; rekonstruowane są m.in. silniki, inne fabrykatory
    * lifesupport działa, ale _inaczej_

Klaudia chce ocenić poziom Semli - poziom psychotroniczny, intencje itp, używając Persi jako frontend. 

(+specjalistka, -rzadka sprawa)

Tr+2:

* V: Semla przekracza możliwości Semli, jest wyżej niż Persefona. Plus - łagodne współczucie
* V: Klaudia jest prawie pewna - Semla jest pod wpływem Aleksandrii. To są Kuratorzy.

Plan jest prosty - JEDNOCZEŚNIE prosimy o repair party, których zamykamy w wahadłowcu i na hol. W tym samym czasie ostrzelamy silniki by je poważnie uszkodzić. Potem - torpedy abordażowe z EMP. I na końcu - Anastazy, Leo, Fabian, Klaudia, Martyn na pokład. 

* X: Commander Sarkamair się będzie czegoś spodziewał.
* X: Commander Sarkamair nie da się złapać w pułapkę. Spodziewa się tego po Orbiterze.

Leo ostrzeliwuje Karnaxian używając dział - cel, zniszczyć silnik. Broń plazmowa. Celem Leo jest to, by Karnaxian już nie poleciał. (drugorzędnie - żeby nikomu nic się nie stało)

(+luksus ostrzeliwania, +Karnaxian jest nieaktywny, -volatility)

Tp+3, by wyłączyć na stałe silniki

* V: do końca akcji te silniki są nieaktywne
* X: zdaniem Fabiana, Leo idzie za daleko, miał tylko unieszkodliwić silniki
* V: podstawowe jednostki bojowe Kuratorów zostały zestrzelone (mają spore straty)

Czas na insercję, -> Tr. Ogień zaporowy by bezpiecznie wejść na pokład

* V: Jest przyczółek / perymetr

Klaudia resetuje Aleksandrię do ustawień fabrycznych zdalnie. Semla Repair Starter kit. Turn Life Support off. Zmusić Kuratora do walki psychotronicznej.

(-człowiek vs Kurator, +Malictrix lite przez Lancera, +magia powoduje HORROR u Kuratora 'will kill them all')

* Tr Z M +3 +3Ob:
    * X: Kurator jest zajęty all right. Statek docelowo może wylecieć w powietrze.
    * X: Co najmniej jedno Ziarno dociera do Serbiniusa
    * (+1Vg) Xm: Wzór Kuratora integruje dzięki magii z Malictrix

Electronic warfare się kończy. Nie jesteśmy w stanie tego pokonać. Podjęliśmy próbę, ale tego nie uratujemy. Fabian się nie zgadza, Anastazy chce się wbijać, Klaudia musi być chroniona. EVAC na Serbiniusa i wysadzamy tą jednostkę. Udało się przekonać Fabiana.

Wycofali się z perymetru.

Leo, Tr+2 (na znalezienie Ziarna):

* V: Ziarno.
* X: Cholerstwo się zaczyna rozlewać

Klaudia używa magii. FUCK OFF MY SHIP. Słaba, ale zdeterminowana.

(EMOCJONALNA)

Tr M +2 +3Ob (niszczycielskie):

* Ob: SYPIALNIA ANASTAZEGO JEST NIEAKTYWNA! Eksplodował fragment Serbiniusa.

Serbinius próbuje zniszczyć OSTATECZNIE Kuratora oraz Karnaxian w taki sposób, by:

1. Zmodyfikowany Kurator zginął TUTAJ.
2. Ziarna się nie pojawią i się nie rozprzestrzenią.

(+przewaga: Serbinius ma siłę ognia, +przewaga: nie ma jak tego zrobić dobrze)

Tp+3:

* X: Karnaxian eksploduje, POTĘŻNA eksplozja. Spread debris.
* X: Jakiś seed gdzieś przetrwa. We can't contain everything.
* V: Nie zostało dość substratu by Kurator się rekonstytuował z nowymi skillami
* V: Skażony awatar Kuratora został na pewno zniszczony.

## Streszczenie

Kurator Sarkamair rozstawił Ziarna - mikrofabrykatory atakujące TAI - i złowił salvager Karnaxian. Skonstruował Alexandrię i zaczął rekonstruować silniki by odlecieć do bazy o bliżej nieokreślonej lokalizacji. Serbinius zainteresował się Karnaxianem poza kursem i nie dał się zmylić optymistyczną Semlą i projekcją załogi. Gdy Serbinius odparł atak Kuratora (co było dość trudne), zrobili intruzję by uratować uwięzioną w Aleksandrii załogę. Niestety, nie udało im się - jedyne co mogli zrobić to zniszczyć Karnaxian i ostrzec wszystkich przed nową strategią Kuratorów, a zwłaszcza Sarkamaira.

## Progresja

* Klaudia Stryk: nie udało jej się uratować ludzi z ręki Kuratora Sarkamaira. Nigdy więcej. Rana psychiczna - jej celem jest zapewnienie, że następnym razem jak spotka Kuratorów to będzie gotowa. Przygotowanie + praca.
* Leo Mikirnik: niezadowolenie ze strony Fabiana Korneliusza; zdaniem Fabiana Leo posuwa się zdecydowanie za daleko w ataku na SC Karnaxian. Zdaniem Fabiana Leo nie dba o ludzi.
* Fabian Korneliusz: niezadowolenie na Leo Mikirnika; jego zdaniem Leo nie dba o ludzi i idzie za daleko. Ma ranę psychiczną po tym jak nie uratował ludzi od Kuratora Sarkamaira i projektował to na Leo.

## Zasługi

* Klaudia Stryk: paranoiczna odkąd wykryła Karnaxian; postawiła sandbox na Persefonie i tylko dzięki temu odparła atak psychotroniczny Kuratora Sarkamaira; wykryła obecność Kuratorów i próbowała go zablokować by móc uratować załogę. Niestety, Kurator był za silny i magia Klaudii tylko go wzmocniła. Klaudia szczęśliwie uratowała Serbiniusa (wysadzając Ziarno Kuratora), ale musieli zniszczyć Karnaxiana.
* Leo Mikirnik: tymczasowo zamiast Helmuta na Serbiniusie; świetny EVA (Extra-Vehicular Activity), mechanik i żołnierz, (41); bardzo doświadczony żołnierz, skutecznie manewrował samobójczymi dronami i ręcznie przejął _point defence_ Serbiniusa by zniszczyć Ziarna Kuratora; potem wbił się na przyczółek w Karnaxianie a gdy się okazało że Kurator jest za mocny, ogłosił ewakuację. Doskonale przeprowadził manewry taktyczne.
* Martyn Hiwasser: pomógł Leo i Klaudii przekonać Fabiana, że załoga Karnaxiana jest już martwa. Chciał uratować załogę Karnaxiana przed Kuratorem, ale nie kosztem Serbiniusa.
* Fabian Korneliusz: bardzo zależy mu by uratować wszystkich ludzi. Jak zawsze, słucha bardziej doświadczonych agentów Orbitera (Leo, Klaudia, Martyn). Zależy mu na uratowaniu jednostki przejętej przez Kuratorów do tego stopnia, że zagroził bezpieczeństwu Serbiniusa - dopiero jak Leo mu uzmysłowił, że jak Karnaxian odleci to zabierze _away party_ ze sobą, Fabian zmienił zdanie.
* Anastazy Termann: narzeka - jak zawsze - że nie ma nic wartościowego do walki. Jak pojawił się Kurator, na przyczółku Karnaxian okazał się być świetnym strzelcem. Zupełnie nie dba o ratowanie ludzi, chce walczyć.
* Kurator Sarkamair: rozłożył Ziarna Kuratorów (zbiór fabrykatorów) i przejął Karnaxian, gdzie się zrekonstruował. Zaatakował też Serbiniusa, ale został odparty. Magia Klaudii PRAWIE dała mu skille i możliwości Malictrix, ale jego plan został odparty i zestrzelony. Okazał się być niesamowicie niebezpiecznym przeciwnikiem - dobrze planuje, atakuje z zaskoczenia i lubi porywać ludzi, adaptować ich jednostki i odlecieć do bazy.
* Helmut Szczypacz: (NIEOBECNY), siedzi na Kontrolerze Pierwszym i dostaje opiernicz za POTENCJALNE ZABICIE ANASTAZEGO. Miranda nie wybacza ;-).
* OO Serbinius: odparł atak Kuratora Sarkamaira, ma lakko uszkodzone systemy komunikacyjne. Musiał zestrzelić przechwyconego Karnaxiana, nie był w stanie niestety więcej zrobić.
* SC Karnaxian: KIA. Wpierw przejęty przez Kuratorów (Semla + lifesupport -> Alexandria + Kurator Sarkamair), potem wektor do przejęcia Serbiniusa, potem zestrzelony przez Serbiniusa jak nie dało się uratować załogi.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 10
* Dni: 1

## OTHER
### Statek "Karnaxian", reclaimer

1. **Pokład Główny/Komora Odzysku**: Jest to obszar, w którym zbierane są elementy z odzysku. Za pomocą systemów magnetycznych, mechanicznych oraz grawitacyjnych elementy są zgarniane i przekazywane do dalszej obróbki.
2. **Sekcja Maszyn**: Tutaj znajdują się główne silniki i systemy napędowe statku, a także systemy napędzające urządzenia do zbierania elementów z odzysku.
3. **Komora Przetwarzania**: Miejsce, w którym zgromadzone elementy są segregowane, sprawdzane pod kątem użyteczności, a nienadające się do sprzedaży są recyklingowane na użyteczne surowce.
4. **Mostek**: Tutaj kapitan i sternicy prowadzą statek i nadzorują operacje. Znajdują się tu systemy nawigacyjne, komunikacyjne oraz kontroli systemów statku.
5. **Kwatery Załogi**: Miejsca do spania i relaksu dla załogi. Mogą istnieć osobne kabiny dla różnych członków załogi lub wspólne pomieszczenie.
6. **Kantyna**: Miejsce, gdzie załoga przygotowuje i spożywa posiłki. Jest tu także miejsce na odpoczynek i rekreację.
7. **Magazyn**: Miejsce, gdzie przechowywane są zapasowe części, narzędzia, sprzęt do odzysku oraz inne niezbędne materiały.
8. **Dźwig Ładunkowy**: Używany do przenoszenia odzyskanych elementów lub surowców do innych statków lub stacji.
9. **Doki Serwisowe**: Miejsce, gdzie mogą być przeprowadzane naprawy zewnętrzne, utrzymanie systemów odzysku i inne prace serwisowe na zewnątrz statku.
10. **Sekcja Life Support**: Obszar odpowiedzialny za utrzymanie warunków umożliwiających przeżycie załogi na pokładzie statku, takich jak powietrze, woda, temperatura itp.
11. **AI Core**: Miejsce, w którym mieszka sztuczna inteligencja statku, nadzorująca wszystkie systemy, pomagająca w nawigacji i zarządzaniu operacjami odzysku.
12. **Kapsuły Odzyskujące**: Małe, autonomiczne pojazdy zdalnie sterowane lub pilotowane przez członków załogi, które są używane do dokładnego przeszukiwania wraków i odzyskiwania cennych przedmiotów.
