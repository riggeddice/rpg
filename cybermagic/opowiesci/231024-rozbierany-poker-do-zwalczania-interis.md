## Metadane

* title: "Rozbierany poker do zwalczania Interis"
* threads: salvagerzy-lohalian, rehabilitacja-eleny-samszar
* motives: fish-out-of-water, fish-in-water, casual-cruelty, karne-podle-zadanie, konflikt-wewnatrzzespolowy, energia-interis
* gm: żółw
* players: anadia, kić

## Kontynuacja
### Kampanijna

* [230905 - Druga tienka przybywa na pomoc](230905-druga-tienka-przybywa-na-pomoc)

### Chronologiczna

* [230905 - Druga tienka przybywa na pomoc](230905-druga-tienka-przybywa-na-pomoc)

## Plan sesji
## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA: 
    * Asking Alexandria - When Everyday's The Weekend
        * "I can't see straight anymore, just got kicked out the fucking door! | It's all easy, it's all easy! | That's how we fucking roll!"
* Opowieść o (Theme and vision): 
    * "Orbiter, normalnie dumny i pełen energii stracił motywację. Dowodzenie idzie od góry a tu dowodzenie nie idzie od góry."
    * Nikt nie wierzy w te misje i w te działania. Z jakiegoś powodu, ta grupa Orbitera zgasła. Ale misja musi być wykonana.
    * Desperacka zabawa by ukryć strach, brak nadziei i "czemu tu jesteśmy"
    * SUKCES
        * zbudowanie zespołu, identyfikacja problemu
* Motive-split
    * fish-out-of-water: (-> Elena). Co można? Czego nie można? Co jest akceptowalne? Jak daleko się posunąć? Jak nie wypaść?
    * fish-in-water: (-> Estella). Jak nawigować TEN zespół, dokąd się posunąć? Jak ich pozyskać? Jak zintegrować Elenę?
    * casual-cruelty: (Jarek: traktowanie Maurycego i tienek, bo MOŻNA), (Tomasz: ignorowanie martwych noktian), (dowodzenie: żołnierze mają robić robotę mimo morale i strachu)
    * karne-podle-zadanie: infiltracja zniszczonej jednostki i jej wyczyszczenie
    * konflikt-wewnatrzzespolowy: (Jarek, Tomasz) mają cele, Paweł trzyma w kupie śmiechem, Maurycy skupia się na sekretach Harmonii
* Detale
    * Crisis source: 
        * HEART: "niezdolność do akceptacji tienek, niechęć do bycia w tym miejscu, to wszystko jest bez sensu i niebezpieczne" 
        * VISIBLE: "niska motywacja, niska energia, niechęć do robienia czegokolwiek, chęć do zabawy, narzekanie na dowództwo"
    * Delta future
        * DARK FUTURE: "zespół się rozpada, nie będą działać razem, pocięci i w złej formie"
        * LIGHT FUTURE: "zespół się ukonstytuował"
    * Dilemma: "How far to go in order to make that stuff work"

### Co się stało i co wiemy

* Zamierzchła Przeszłość
    * irrelevant
* Przeszłość
    * Maurycy przegrał zakład, ma zaprosić tienkę
    * Odblokowali jednostkę noktiańską, nazywa się 'Osculi Vespae'. Nie ma tam nic interesującego. Trzeba wyczyścić. Nie jest groźna
        * ZNOWU nie działa komunikacja
        * ZNOWU burza śnieżna
* Teraźniejszość
    * .
* Frakcje
    * tym razem każda jednostka reprezentuje własną frakcję; patrz na osoby
* w okolicy bardzo dużo jednostek Orbitera i Noctis jest rozbitych
* grupa czyszcząca z ramienia Orbitera, z Astralną Flarą - mają podczyścić teren; baza to Lohalian

### Co się stanie (what will happen)

* Faza 0: _zaproszenie_
    * Maurycy prosi Elenę o przyjście na grę w karty
    * (wstecznie) Estella integruje się z zespołem dzięki Afagrelowi
* Faza 1: _nocne gry karciane_
    * stakes:
        * jaką opinię mają żołnierze o obu tienkach
            * jak daleko tienki się posuną (Maurycy ma jedną rozebrać w grze w karty, Pawełek będzie oszukiwał)
            * jak skończy się pojedynek z Cyprianem. Jarek bierze to na siebie, Cyprian nic nie ma do niego.
        * czy sprawa zaeskaluje wyżej?
    * opponent: 
        * karciarze: Jarek (brutalna niechęć do tienek), Maurycy (nieśmiały przegrał zakład)
    * problem:
        * brak nadziei, chęć gry tym co mamy, impreza, demoralizacja. Nie chcą być żołnierzami na tym terenie.
    * koniec fazy:
        * gra karciana się kończy
* Faza 2: _misja sprzątania Osculi Vespae_
    * stakes:
        * żołnierze czy przyjaciele?
        * demonstracja kompetencji i przydatności
    * opponent: 
        * burza, złe warunki, fatalny teren, jednostka w złym stanie
        * Tomasz chce pieniądz
    * problem:
        * to samo co wyżej.
        * kto podłożył te zwłoki?
    * koniec fazy:
        * czysta jednostka.
* SN: spotkanie z inżynierem - nic nie działa, wykryli jednostkę w zimnie. Może tam coś jest.
* SN: wejście na uszkodzoną noktiańską jednostkę, szabrujemy

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki
#### 1. Grupa wydzielona Lohalian

SZTAB

* Mikołaj Larnecjat: dowódca grupy wydzielonej sprzątającej Przelotyk Północny; stracił bliskich podczas wojny
    * OCEAN: (E-O-): zamknięty w sobie, rzetelny, spokojny i dobry w powtarzalnych operacjach. "Nie powinienem był przetrwać wojny. Jeszcze Orbiterowi jednak się przydam."
    * VALS: (Conformism(Duty), Humility): jak coś ma zrobić, zrobi i dostarczy. "Nieważne czy mi się podoba, czy nie. Jestem elementem maszyny Orbitera."
    * Core Wound - Lie: "Wszystko, co kochałem zginęło i zostało zniszczone. Jestem sam." - "Nie przywiązuj się. Jest tylko misja."
    * Styl: Spokojny, absolutnie nie inspirujący i cichy miłośnik muzyki klasycznej pochłonięty swoimi myślami, ale wykonujący każde polecenie rzetelnie i sensownie.
    * metakultura: Atarien: "dowódca każe, ja wykonuję. Na tym polega hierarchia. Tak się buduje coś większego niż my sami."
* Rachela Brześniak: czarodziejka Orbitera (katalistka i technomantka), dołączona do pomocy w dekontaminacji terenu; na uboczu, bo jest "dziwna"
    * OCEAN: (N+C+): niezwykle ostrożna z czarowaniem i anomaliami, niewiele mówi i raczej trzyma się sama ze sobą.
    * VALS: (Achievement, Universalism): Skupia się dokładnie na zadaniu, by je wykonać może pracować z każdym. "Dekontaminacja to moje zadanie. Zajmijcie się resztą."
    * Core Wound - Lie: "Współpracowała z żywymi TAI i na jej oczach Rzieza zniszczył jej przyjaciół; myśli że to jej wina" - "Magia MUSI być kontrolowana. Wszystko co anomalne musi być reglamentowane."
    * Styl: Cicha, pełna wewnętrznego ognia. Skupia się na dekontaminacji z żarliwością godną faerilki.
    * metakultura: Faeril: "ciężką pracą da się rozwiązać każdy problem - to tylko kwestia cierpliwości i determinacji"
* Irek Korniczew: główny inżynier wysłany przez Flarę by zająć się obsługą bazy tymczasowej
    * OCEAN: (C+O+): tysiące drobnych pomysłów i usprawnień, poszukiwanie nowego i wszystko przy poszanowaniu istniejącej maszyny. "Wszystko da się usprawnić i zrobić lepiej. Dojdziemy do tego jak."
    * VALS: (Stimulation, Achievement): coś nowego, coś ciekawego, coś rozwijającego. A nie gówno na patyku. "Te drobne powtarzające się błędy powinna rozwiązać TAI."
    * Core Wound - Lie: "Zawsze inni dostawali wszystko co ciekawe a ja tylko maintenance, mimo że to moja specjalizacja" - "Te wszystkie tematy maintenance to misja karna; nie mogę brać na siebie"
    * Styl: Wiecznie zirytowany, ale pomocny. Narzeka na sprzęt i zadanie. Jednocześnie - jeden z lepszych inżynierów.
    * metakultura: Atarien: "jeśli nie dam rady się wykazać, rozpłynę się w tłumie szarych twarzy"
* Antoni Paklinos: chorąży odpowiedzialny za integrację z ramienia grupy wydzielonej Salvagerów Lohalian
    * OCEAN: (A-C+): niezwykle cierpliwy i nieźle współpracuje z ludźmi, jednocześnie dość agresywny; lubi dawać ludziom szansę i ich testować. "Paklinos to swój chłop; nieco zmierzły i testujący, ale daje radę"
    * VALS: (Security, Benevolence): niezależnie czego nie dostaje i czym się nie zajmuje, kluczowa dla niego jest pomoc innym i zadbanie o ochronę. "On nawet do kibla wchodzi z nożem"
    * Core Wound - Lie: "Zrobiłem to, co należało zrobić i zostałem wygnany na planetę" - "Nic co robimy nie ma znaczenia, ale możemy pomagać w najbliższej okolicy. Od myślenia jest dowództwo."
    * Styl: Dość zaczepny ale pozytywny facet; dużo testuje i zawsze jest 'in your face'. Zaufany i daje radę.
    * metakultura: Atarien: "Oddział jest tyle warty ile jego wytrenowanie i spójność z rozkazami. Co nie jest testowane i szkolone, to się nie dzieje."

OPERACYJKA

* Tomasz Afagrel: kapral grupy wydzielonej pod dowództwem Larnecjata; były przemytnik
    * OCEAN: (E+O+): nie boi się ryzyka i zawsze pierwszy do szydery czy okazji. "Jeśli jest zysk, warto spróbować."
    * VALS: (Hedonism, Power): pierwszy szuka okazji do dorobienia czy dobrej zabawy. Straszny hustler. "Nikt nie dostanie za darmo niczego. Trzeba chcieć i działać."
    * Core Wound - Lie: "Może jestem z bogatego domu, ale wojna zniszczyła wszystko co mieliśmy" - "KAŻDĄ okazję trzeba wykorzystać, WSZYSTKIEGO spróbować - by nie żałować."
    * Styl: Głośny, pełen energii, uśmiech drwiący i szyderczy. Zawsze szuka sposobu na zabawę czy okazję. Lojalny wobec "swoich". Kolekcjonuje fanty.
    * metakultura: Atarien: "moja rodzina miała wszystko i wszystko straciła. Moja kolej odbudowania potęgi rodziny."
* Jarosław Mirelski: szeregowy, ekspert od walki w zwarciu; wygląda jak kupa mięśni o świńskich oczkach
    * OCEAN: (E+C-): skłonny do ryzyka, łatwo się denerwuje, impulsywny, ale też bardzo lojalny. "Z Jarkiem lepiej nie wchodzić w konflikt, chyba że masz ochotę na siniaka."
    * VALS: (Achievement, Tradition): duma z własnych umiejętności w walce. Tradycyjnie patrzy na honor i wyzwania. "Prawdziwi faceci rozwiązują problemy walką, a ich przełożeni ich uruchamiają"
    * Core Wound - Lie: "W wyniku scamu moja rodzina została bez niczego i tylko wojsko było dla mnie nadzieją" - "Pokaż, że warto na ciebie stawiać, bądź świetnym żołnierzem."
    * Styl: Krótki temperament, zawsze gotów do wyzwania. Przeciwników mierzy wzrokiem, ale też ma w sobie pewien urok, który przyciąga innych. Przyjazny dla "plebsu".
    * metakultura: Atarien: "Ludzie są ludźmi. Trzymamy się razem. Każdemu normalsowi warto pomóc."
* Paweł Lawarczak: szeregowy, znany z niezwykłej zręczności i brawurowej gry w karty; advancer
    * OCEAN: (E+N-): Charyzmatyczny, gadatliwy i uwielbia być w centrum uwagi. Działa zanim pomyśli, ryzykant. "Paweł zawsze ma jakąś sztuczkę, żart czy pomysł. Nieważne jak źle jest, Paweł rozchmurzy."
    * VALS: (Hedonism, Security): Żyje dla chwili, uwielbia emocje i ryzyko, ale dba by nie spotkała go krzywda. Małe oszustwa to kolejny sposób na świetną zabawę. "Najlepszy do zabawy, ale go pilnuj..."
    * Core Wound - Lie: "Jako dziecko byłem złodziejem i akrobatą; jak pili to bili" - "Jeśli wszyscy się śmieją i postrzegają mnie za niegroźnego, jestem bezpieczny"
    * Styl: Wiecznie uśmiechnięty, zwinny jak kot, zawsze ma gotowy dowcip na ustach. Ma sztuczki karciane, nic nie bierze na poważnie i ogólnie co się nie stanie - jest radość.
    * metakultura: Atarien: "To są moi ludzie. Tylko mnie wolno ich oszukiwać."
* Maurycy Derwisz: szeregowy, technik i miłośnik historii Harmonii Diakon
    * OCEAN: (N+C+): Nieśmiały, skoncentrowany na badaniach i technologii, cierpliwy cichy i dokładny. "Zna się na rzeczach, o których inni nawet nie słyszeli. Ale musisz się dobrze przyjrzeć, żeby go zauważyć."
    * VALS: (Achievement, Self-Direction): Zbiera wiedzę o Harmonii Diakon; nieskończenie układający 'puzzle' jej życia. "Tajemnice Harmonii to wyzwanie które przezwyciężę."
    * Core Wound - Lie: "Jego rodzina musiała uciekać z Aurum z uwagi na powiązanie z Harmonią Diakon" - "Jeśli zrozumiem o co chodziło z Harmonią, zrozumiem sekret mojej rodziny"
    * Styl: Praktyczne ubrania, do swobodnej pracy z różnymi urządzeniami i dyskrecji. Mimo nieśmiałości wobec kobiet, jeden z lepszych strzelców i ekspertów od sprzętu. Przystojny chłopaczek.

#### 2. Siły Samszarów

* Cyprian Mirisztalnik: chorąży Aurum, kompetentny w walce i wierny towarzysz Eleny w świecie Orbitera
    * OCEAN (E+ A-): pełen życiowej energii i charyzmy, opiekuńczy ALE arogancki i roszczeniowy. "Kto ma moc, ma obowiązek. Kto ma moc, tego słuchają."
    * VALS: (Conformity, Tradition): głęboko wierzy w znaczenie hierarchii i lojalności. Wierzy też w odpowiednie wyglądanie i zachowywanie się. "Krew mówi sama za siebie."
    * Core Wound - Lie: "Nie będąc arystokratą, wszyscy mnie ignorowali" - "Zapewnię odpowiednią opiekę i kontrolę nad innymi, dzięki czemu mnie nie odrzucą i nie zostawią."
    * Styl: Opiekuńczy i dominujący jednocześnie, kłótliwy ale elegancki. Patrzy na świat przez pryzmat hierarchii i formalnej kontroli.
* Adelajda Kalmiris
    * Core Wound - Lie: "tylko mnie udało się uciec i tylko mnie udało się rodowi Eleny uratować." - "ród Samszar jest inny - to jedyny szlachetny, dobry ród; oni nie pozwalają na zło"

### Scena Zero - impl

.

### Sesja Właściwa - impl

Estella i Elena poznały się na obieraniu ziemniaków. Jak to stwierdził Paklinos "jesteście dziewczynami więc się znacie na ziemniakach". Cyprian bardzo protestował. Paklinos jego też przesunął na ziemniaki. 

* Cyprian: "Rachela jest kobietą a nie musi obierać ziemniaków!" (Elena słyszy)
* Elena: "Cyprian, CHODŹ, ziemniaki nikogo nie zabiły. Chodź, możesz obierać ze mną"
* Cyprian: "Ale tien porucznik, to się godzi, Ty nie możesz obierać ziemniaków!"
* Elena: "CYPRIANIE ZIEMNIAKI NAS NIE ZABIJĄ! CHODŹ!"
* Cyprian: "A Adelajda?"
* Paklinos: "Może być niesprawiedliwym, że pani Adelajda Kalmiris nie obiera ziemniaków..."
* Cyprian: "To niech ona a nie tien porucznik!"
* Elena: "ALBO idziesz ze mną obierać ziemniaki albo chorąży Paklinos zleci Ci coś innego. Wykonajmy bez zbędnego gadania."
* Cyprian: "Tak jest, tien porucznik!"
* Elena: "...wybierz jedno..."
* Paklinos się uśmiecha, ale nic nie mówi.

Estella obrała już z pół kilo ziemniaków. Kucharz Jaromir patrzy na te ziemniaki Estelli... "chorąża, z całym szacunkiem, z dupy są te ziemniaki". Estella "Kucharzu, może nie są piękne, ale jak w żołądku?"

* Jaromir: "Dobra, będziesz miała jakąś pomoc. O" - ucieszył się widząc smutnego mega eleganckiego Cypriana i Elenę.
* Estella: "Przyszłyście pomóc?"
* Elena: "Witaj, jestem Elena Samszar."
* Es: Estella Gwozdnik
* El: (cicho) Mogę podpatrzeć jak z tymi ziemniakami?
* Es: (cicho) Jasne.

Cyprian próbuje obierać ziemniaki LEPIEJ niż Estella. Nie wychodzi mu. Estella nie zauważa konkurencji. Elena patrzy na Cypriana, wywija oczami, nic nie mówi. JAKOŚ obiera ziemniaki.

Tymczasem w powietrzu jest fetor gotowanej puszki, konserwy, wojskowej racji żywnościowej z rybą... Cyprian stracił kilka punktów morale. Co ciekawe, z ziemniaków trzeba dużo wykrawać. Są podgnite. Są w złym stanie.

Jaromir powiedział, że były odbierane tydzień temu. Dobrze przechowywane. I były lepsze. Elena "można zobaczyć?" Jaromir się obruszył "tien porucznik, POTRAFIĘ przechowywać ziemniaki." Elena "nie o to mi chodzi, jestem magiem. Wyczuwam rzeczy których ty nie czujesz. Tam może coś być czego nie powinno być. Nie zobaczysz tego. Widać że taki kucharz jak Ty się dobrze zajmuje towarem. Może się uda znaleźć rozwiązanie."

* Jaromir: "jedyne co się psuje to te konserwy. I inne konserwy też są w gorszym stanie."
* Cyprian: "(cicho) widać, że ten kucharz się dobrze zajmuje wszystkim, na przykład nami..."

Jaromir bierze Elenę na bok. Cyprian staje, Jaromir "gdzie, ziemniak czeka". Estella się uśmiecha.

Jaromir bierze Elenę do chłodnego miejsca. Elena, idąc, czuje się... _nie tak_. Coś jest nie w porządku w całym statku, nie tylko tu. Strasznie delikatne. Dotarli do magazynu. Gdy Elena wzięła ziemniaka i zaczęła go macać by rzucić czar, PRZYPOMNIAŁA SOBIE że jest zakaz czarowania na pokładzie Lohalian. Więc Elena... ignoruje i czaruje. Magią Samszarów.

Tr M +2 +3Ob:

* V: (p) Elena ma DOWÓD, że to pole magiczne powoduje degenerację ziemniaków i konserw. Ten dowód zrozumie każda sensowna osoba, nawet nie mag. 
* V: (m) 
    * Elena poczuła energię i SMAK tej energii. Zobaczyła cienie, echa duchów, PUSTYCH, wyżartych ze wspomnień. To są echa które nie pamiętają swojej funkcji.
    * Elena zna ten typ energii. Entropia. Śmierć cieplna wszechświata. Koniec wszystkiego. Pustka znaczeń. Interis. Pole energii Interis. Cały statek jest dotknięty Interis.
* Vm: (e)
    * Elena WIE jak zaizolować spiżarnię - trzeba PO PIERWSZE "wystarczająco wierzyć" że jedzenie przetrwa, PO DRUGIE zgromadzić odpowiednią ilość rzeczy które dają ludziom tu dobre myśli. Te "totemy" rozwieszone w spiżarni dadzą izolację od Interis.
    * Elena POCZUŁA pustkę. Nieskończoną pustkę. ŚWIADOMOŚĆ źródła Interis. Potężne źródło GDZIEŚ pod ziemią. WSZYSTKO jest napromieniowane przez Interis. Cały teren.

Cały teren jest pod wpływem Interis. I potężne źródło energii Interis, promieniujące, jest GDZIEŚ pod ziemią. I to jest za duże do zatrzymania. I tylko Elena wie.

* Elena: "Wiem co jest nie tak i rozwiążę problem, ale nie w tej chwili. Nie mogę się rządzić sama na Lohalianie. Problem wymaga bardziej wysublimowanego podejścia i szeroko zakrojonej akcji. Muszę przedyskutować z kapitanem. Ale - bądź dobrej myśli." (potapała kucharza po ramieniu). "Ziemniaki - i reszta żywności - będą uratowane".
* Kucharz: (spojrzał ponuro) "Dobrze. Ziemniak czeka."

Elena wróciła do ziemniaków. Estella jest ciekawa, obiera dalej. Estella czeka aż Jaromir zajmie się zupą z konserw.

* Es: (hipernet) "Coś ciekawego?"
* El: (hipernet) (mówi wszystko)
* Es: "Ok... paskudnie? Trzeba powiedzieć kapitanowi. Musi wiedzieć."
* El: "Jest potrzebny zwłaszcza do prewencji."
* Es: "Jesteś w stanie jako Samszar zrobić coś nie tylko magią? By poprawić jakość tego co mamy?"
* El: "Żarcia?"
* Es: "Tak..."

ELENA MA TEST NA ZIEMNIAKI

Tr (niepełna) +3:

* X: Elena się skaleczyła obierając ziemniaki
* X: Elena nie będzie wysłana na ziemniaki. Ziemniaki są w złym stanie. Elena wycięła je ŹLE.
    * Elena ma opinię NAJGORSZEGO ZIEMNIACZAKA EVER.

Elena odwiedza kapitana.

Kapitan Larnecjat siedzi w swoim biurze, nad mapami i dokumentami, analizuje rzeczy.

* L: tien Samszar.
* E: panie kapitanie, mamy problem!
* L: (uniósł brew)
* E: panie kapitanie, melduję, że znalazłam na tym terenie źródło Interis. DUŻE źródło Interis. Lewiatana albo Węzeł, który jest odpowiedzialny za zniszczenie wszystkiego, od duchów po ziemniaki. A nawet przez statki które tu spadały.
* E: (tłumaczy bardziej naukowo)
* L: (słucha i nie przerywa)
* E: rozwiązanie by osłonić jedzenie... (tłumaczy) a docelowo proponowałabym większą ekipę magów...
* L: Interis, tien Samszar. Skąd ta pewność?
* E: (WŁAŚNIE POWIEDZIAŁAM!!!) (tłumaczy dowody jak krowie na rowie)

Tr Z +3:

* X: Kapitan ma swoje rozkazy i wykona swoje rozkazy. Będzie czyścił statki.
    * Elena: (przypomina co się dzieje przy Interis, morale, żarcie, ludzie zaczną gasnąć, samobójstwa, może i kapitan dotknięty Interis...) +1Vg
* Vg: Kapitan wierzy Elenie. 
    * L: To niedobra wiadomość. Nasza misja zrobiła się trudniejsza. Faktycznie, chorąża Gwozdnik miała rację - byłaś tu potrzebna. Twoje najbliższe rekomendacje?
    * El: (pozytywne wzmocnienie żarcia, zabezpieczyć jedzenie, morale się podniesie)
* Vr: Kapitan idzie za planem Eleny i ma o niej dobrą opinię jak chodzi o energie. Jest otwarty na jej pomysły.
    * L: Dobrze. Na pewno wydam odpowiednie polecenia. Jeśli możesz, ściągnij coś smacznego ze swojego rodu. Zostajesz... opiekunką morale.
    * Kapitan gromadzi potrzebne elementy do Rytuału.
* V: (Elena dyskretnie załatwiła w rodzie ściągnięcie dobrego jedzenia i dobrych zasobów do morale dla Orbitera tutaj. NAWET Ród jest zadowolony z pochwały kapitana wobec Eleny.)
    * (Elena dostała pochwałę od kapitana do Samszarów)

WIECZÓR.

Elena jest w "swojej kwaterze", którą dzieli z Adelajdą. Pukanie. Cichy głos Maurycego.

Elena wychodzi. W mundurze. Widzisz zakłopotanego ślicznego młodzieńca. W mundurze. I wyraźnie jest zawstydzony.

* El: W czym mogę Ci pomóc, szeregowy? Czemu jesteście tak zawstydzeni, szeregowy?
* M: (koleś odwrócił wzrok) Pani porucznik, chciałem panią... zaprosić. Dyskretnie. W IMIENIU ZESPOŁU! (orientując się co właśnie powiedział)
* M: Gramy w karty, nikt nie wie, będziemy w pani zespole. To jest, pani i chorożej Gwozdnik.
* El: Estella też gra?
* M: Tak. Też.
* El -> Es: "EJ GRASZ W KARTY?"
* Es -> El: "No ba, Ty nie?"
* El -> Es: "że teraz?"
* Es -> El: "No zaprasza."
* El -> Es: "Że od nich nie od Ciebie?"
* Es -> El: "Tak"
* El: Co zabieramy?
* M: Nic, pani porucznik. Nie musi pani mieć niczego!
* El: Niczego?
* M: To znaczy, pani porucznik... (PANIC!)

.

* Elena -> Adelajda: "chcesz iść pograć w karty?"
* A: pani porucznik, mogę pójść. Wiem, że Cyprian zawsze jest zainteresowany.

.

* El: Mogę kogoś ze sobą zabrać?
* M: pani porucznik, to znaczy... (myśli) chyba... tak.

Adelajda i Elena trafiły w labiryncie zniszczonego Lohalian do jednego z pomieszczeń nie do końca wyczyszczonego. Tam jest sztuczne ciepło, jest trochę posprzątane, ale jest złowroga aura Interis, bardzo delikatna. Siedzi tam kilka osób.

* Estella
* Tomasz Afagrel
* Jarosław Mirelski (kupa mięśni o świńskich oczach)
* Paweł Lawarczak
* Maurycy Derwisz
* Adelajda
* Elena

Na widok Adelajdy Jarek oblizał dłoń i przeczesał włosy. Ogólnie, Adelajda zrobiła wrażenie.

Kilka pierwszych rund gry w karty były o kamyczki, żetony... po prostu zabawa, integracja, tego typu rzeczy.

Tr +3:

* XVr: ogólnie, gra jest dość wyrównana. Trochę wygrywacie, trochę przegrywacie, raczej wygrywacie.
    * (Tr+2: V: Estella widzi, że KTOŚ daje im wygrać, gra nie jest w pełni losowa, ale np. Maurycy nie oszukuje, nie jest w tym dobry)

.

* Maurycy: "może... zrobimy grę... ciekawszą?" (prawie schował głowę w ręce)
* Tomasz: "co masz na myśli, Maurycy?"
* Maurycy: ...
* Paweł: "nie wiem co on myślał, ale może podniesiemy stawki?"
* Tomasz: "nie mamy pieniędzy, nie takie, o jakie warto grać"
* Maurycy: ... (za cicho)
* Es -> El (hipernet): wiesz że oni dawali nam wygrać? Wiesz że mają nadzieję na... trochę inny rodzaj stawek?
* El -> Es (h): a jakby im dać nauczkę?
* Es -> El (h): nie jestem specem od kart
* El -> Es (h): ale karty są papierowe. Oszukiwać kartami też można. (sprzedaje plan podmiany karcianej Estelli)
* Es -> El (h): czemu nie... (oszukiwanie w karty ma długie wojskowe tradycje)
* Tomasz: "no Maurycy powiedz o co Ci chodzi"
* Maurycy: (czerwony jak piwonia) ...żebyśmy grali albo... o coś... powiedzieć prawdę... albo pozbyć się części ubrania...
* Tomasz: "OHOHO, Maurycy, nie spodziewałem się po Tobie takiego pomysłu! Co za pies z Ciebie"
* Tomasz: "Ale to może być niekomfortowe dla naszych dam..."
* Elena: "Nie będziecie się czuć komfortowo jak przegracie?" /bibliotekowy głos
* Tomasz: "Pani tien porucznik, nagość nie jest mi obca, chodzę pod prysznic" /śmiech z sali
* (jedyny koleś co się nawet nie uśmiechnął to ten świńskooki Jarosław)
* Elena: "ja o kartach w książkach, jestem zaskoczona że rundy, ale wysoka stawka, obawiam się... czy możemy liczyć na kolczyki, inne takie, taryfa ulgowa, jesteśmy nowicjuszkami...

Tr Z +3:

* X: 
    * Tomasz: "no dobrze, pani porucznik, zrozumiałe. Więc niech na przykład... my mamy 6 warstw do zdjęcia a Wy po 10. Pasuje?"
* V: zaakceptowali warunki.
* Vr: nie wierzą, że zostali wkręceni. Wierzą, że ONI wygrali negocjacje z odpowiednio hedonistycznymi tienkami które są znudzone. "jak w książkach o tienkach".

Gramy w karty lekko oszukując (co ja mówię, OSTRO oszukując). Łańcuchy:

* karty: WYGRAŁYŚMY, NIKT NIC NIE WIE
* pozycja: ustawiona - pełen szacunek
* zespół: zintegrowany
* dowiedzieć się więcej: o zespole, o sytuacji...
* (-) resentymenty: obecne, wysokie
* (-) ilość ciałka: trochę, Adelajda, oops

Złożenie zaklęcia i gra z oszustwem wykorzystuje Adelajdę i Estellę jako odwrócenie uwagi. Czyli: Elena czaruje, ale Adelajda pomaga poprawić coś Estelli przy staniku pod strojem. Chłopcy są skupieni. Mają nadzieję, że COŚ zobaczą, plus, no widzą łapki. Ogólnie, Adelajda idzie w 'tę grę'. To jest silny Zasób.

Tp M +4 +3Ob:

* Vm: Elena skupiała się na czarze jakby od tego zależało jej życie. Wykupione: WYGRAŁYŚMY + USTAWIONA POZYCJA
    * karty są perfekcyjnie sprawne. Oszust myśli, że kontroluje sytuację, ale w praktyce to Elena kontroluje. A Estella hipernetem mówi jej JAK to robić
* V: INTEGRACJA ZESPOŁU.
    * żarty podczas gry, wszyscy poza Jarosławem wyraźnie dobrze się bawią. Maurycy się troszeczkę otworzył, nie jest to takie straszne. Inni mu TROCHĘ dokuczają, ale nie kocą.
* X: Adelajda kończy w bieliźnie. Jarosław jest zadowolony.
* V: Coś więcej o zespole
    * Tomasz: "dał radę sprzedać ciężarówkę należącą do kumpla i wyjechali z miasta a potem zrobił wielką imprezę i CI INNI zostali bez niczego"
    * Jarosław: "była sytuacja z anomalią. I Jarosław wracał DWA RAZY napromieniowany, ranny, w złej formie - ale wyjął ludzi których nawet nie znał. Widać, że nie lubi bogaczy."
    * Paweł: "niejednokrotnie wkradał się i zmieniał polecenia, by nigdy nie siedzieć w jakichś kiepskich zadaniach. Zgrywus jak cholera."
    * Maurycy: "kolekcjonuje wiedzę o tajemniczej Harmonii Diakon. Potężnej czarodziejce, która kiedyś ratowała ludzi. Niewiele więcej o niej wie. 'Jego wirtualna dziewczyna'"
* Vr: Ten zespół ma wysokie morale. Ci ludzie (poza Jarosławem) mają chęć działania i "być może ta misja będzie fajna i ma sens"
* V: PEŁEN SZACUNEK zespołu. Będą chętnie współpracować z tienkami. Nawet Jarosław ma szacunek.

Elena przyznaje się do oszukiwania, pokazuje, że mogła iść dalej - i oni TEŻ oszukiwali. To nie był przypadek. Skończmy zatem w tym miejscu i zobaczcie, że dostaliście miły prezencik ;-). Rozchodzimy się i nic nie mówimy oficerom. Pasuje? ;-).

Jarosław ma burzę gradową na twarzy, ale Tomasz położył mu rękę na ramieniu "stary, nie wyszło. Tym razem nie wyszło." Paweł pokręcił głową "teraz rozumiem, czemu te karty tak dziwnie działały...". Maurycy bardzo próbuje udawać, że nic nie wie i w ogóle go tu nie ma i nie patrzy na Adelajdę (ALE PATRZY).

Ogólnie - spoko poszło. Tylko rano - wszyscy niewyspani :-). I Elena nie idzie na ziemniaki. Ma zakaz.

## Streszczenie

Tienki powoli aklimatyzują się z bazą (m.in. zbierając ziemniaki). Okazuje się, że rozpad i gnicie ziemniaków są powiązane z energią Nihilusa; gdzieś tu jest potężne źródło energii Interis. Elena nie tylko powiedziała o tym kapitanowi Larnecjatowi ale dodatkowo ściągnęła świeże jedzenie od Samszarów, co zdecydowanie podniesie morale. Tienki spotkały się ze swoim zespołem i jakkolwiek oni oszukiwali tienki w karty, ale Elena oszukiwała bardziej (magią) - udało im się zachować ubrania a nawet zaskarbić sobie pewien szacunek.

## Progresja

* Elena Samszar: pochwała od kapitana Larnecjata, skierowana do rodu Samszar odnośnie samej Eleny
* Elena Samszar: zaakceptowana przez żołnierzy; traktowana jako 'swoja tien porucznik'. Ale - ma zakaz obierania ziemniaków i się z niej tu podśmiewują
* Estella Gwozdnik: traktowana jako 'swoja tien chorąża', z zaakceptowanym tytułem

## Zasługi

* Elena Samszar: bardzo kiepsko obiera ziemniaki (aż się zacięła); wykryła magicznie źródło Interis. Potem oszukiwała żołnierzy w karty, ale wprowadziła do gry Adelajdę, żeby nie byli zbyt wściekli. Pozyskała jedzenie dla Orbitera od Samszarów.
* Estella Gwozdnik: kompetentna w obieraniu ziemniaków (i umie się odgryźć kucharzowi), pomaga Elenie w oszukiwaniu żołnierzy w grę w karty jako element dywersji
* Tomasz Afagrel: zaprojektował plan jak obejrzeć nieubrane tienki - gra w strip poker, Maurycy wprowadzi sprawę, Paweł będzie oszukiwał. Tylko, że Elena oszukiwała lepiej (magią). Potraktował sprawę z humorem.
* Jarosław Mirelski: nie lubi tienek, bardziej lubi oglądać Adelajdę. Niechętnie mówi, nie wchodzi w interakcję szczególnie. Grał w karty i swoją obecnością psuł nastrój przy stole ;-).
* Paweł Lawarczak: świetnie oszukuje w karty (acz Elena lepiej) i robi 'sztuczki magiczne'. Najweselszy z zespołu. Przyjął przegraną z pokorą i szerokim uśmiechem.
* Maurycy Derwisz: prześliczny chłopiec, wstydzi się rozmowy z Eleną. Wciągnął ją do gry w karty na polecenie Tomasza. Też na polecenie zaproponował grę w strip poker. 
* Cyprian Mirisztalnik: narzekał, że Elena musi obierać ziemniaki a Rachela nie. I że on musi. Próbował obierać ziemniaki LEPIEJ niż Estella, co mu nie wychodziło; Estella ma większą wprawę.
* Adelajda Kalmiris: wykorzystana przez Elenę jako dywersja dla tienek grających w karty. Skończyła w bieliźnie w rozbieranego pokera, co sprawiło, że żołnierze nie poczuli się całkowicie oszukani. Ona nie ma wstydu, więc bawiła się nieźle.
* Jaromir Gaburon: kucharz Lohaliana; ponury i zrezygnowany wielki brodaty facet. Wysyła Elenę, Cypriana i Elenę do obierania ziemniaków, pokazał Elenie składzik ziemniaków i już NIGDY nie da Elenie ziemniaka do obierania.
* Mikołaj Larnecjat: zrezygnowany kapitan, który jednak potraktował Interis poważnie, wysępił od Eleny wsparcie żywnościowe od Samszarów i odpowiednio ją pochwalił w nagrodę. Oboje zyskali. Nadal dostarcza zero morale, ale jest udanym administratorem.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, W
                    1. Góry Hallarmeng
                        1. Przyprzelotyk
                            1. Korony Cmentarne
                                1. Ruina Lohalian
                                1. Dolina Krosadasza

## Czas

* Opóźnienie: 1
* Dni: 2
