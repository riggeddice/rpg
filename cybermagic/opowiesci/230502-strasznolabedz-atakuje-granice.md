## Metadane

* title: "Strasznołabędź atakuje granicę"
* threads: brak
* motives: bardzo-grozne-potwory, wasnie-przygraniczne, katastrofalny-paradoks
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [230412 - Dźwiedzie polują na ser](230412-dzwiedzie-poluja-na-ser)

### Chronologiczna

* [230412 - Dźwiedzie polują na ser](230412-dzwiedzie-poluja-na-ser)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * .
* CEL: 
    * Link Elena - Viorika, pokazać, że Elena była dobrze traktowana
    * Przetestować mechanikę aspektową i plansze

### Co się stało i co wiemy

* Kontekst
    * Ula Blakenbauer miała Paradoks. Łabędź odleciał w kierunku na Samszarów.
    * Marcinozaur poprosił Viorikę o rozwiązanie tego problemu, bo on nie może. I chroni Ulę.

### Co się stanie (what will happen)

* S1: Marcinozaur przedstawia problem Viorice. I brak zaawansowanego sprzętu. W pobliżu jest tylko Elena.
* S2: Fort Tawalizer

### Sukces graczy (when you win)

* Łabędź zniszczony
* Łabędź nie dokona za wielu zniszczeń w Samszarskim forcie Tawalizer

## Sesja - analiza

### Fiszki

* Marcinozaur Verlen: mag, skrajny sybrianin, mag iluzji i katai
    * (ENCAO: +-+00 | Don't worry, be happy;;Żarliwy, skłonny do wydania ogromnej ilości energii;;Punktualny| VALS: Face, Stimulation >> Humility| DRIVE: Nawracanie na VERLENIZM)
    * styl: Alex Louis Armstrong x Volo Guide To Monsters.
    * mistrz szpiegów Verlenlandu. Apex szpiegator.
    * Sam sobie zmienił imię. Miał "Marcin". Był JEDYNYM Verlenem z normalnym imieniem, ale chciał pójść jeszcze efektowniej.
    * typowa reakcja: "nazywasz się Maciuś, jak brzuszek, bo tylko jesz. CHODŹ BIEGAĆ PO LESIE!" (do kogoś kto nazywa się inaczej niż Maciek)
* 
* .Strasznołabędź: Verlenowy potwór, który uległ Paradoksalizacji Uli
    * akcje: 'Jego cień paraliżuje', 'jego głos przeraża', 'emisja światła', 'wężoszyja z superdziobem', 'iluzje repozycjonujące', 'ppanc atak'
    * defensywy: 'pole deflekcji' (życie) (3), iluzje unikowe, szybki, zwrotny
    * słabości: 'brak AoE', 'głód mięsa i strachu'
* .Fort Tawalizer
    * aspekty: 

### Scena Zero - impl

.

### Sesja Właściwa - impl

Viorika robi swoje Viorikowe tematy podźwiedziowe. I dostała wiadomość od Marcinozaura. Prośbę dość pilnego kontaktu. Odwrócona uwaga Vioriki - plaskacz od dźwiedzia, rozczarowanego, że V zaczęła tak kiepsko walczyć. V szybko kończy siłowanie i łączy się z Marcinozaurem. Marcinozaur wygląda na zrozpaczonego. To mina typu "złamałem paznokieć" lub "znalazłem Finis Vitae" u niego. Szpiegator...

* M: Wirku, łabędź mi uciekł
* V: kota wyślij?
* M: co ja Blakenbauerem jestem... (po sekundzie) nie, jej nie chcę angażować, nie tu nie do tego.
* M: dopakowany strasznołabędź. Leci na Samszarów. I jest naprawdę... niewygodny.
* V: to znaczy?
* M: kojarzysz Fort Tawalizer?
* V: mhm
* M: stawiam na łabędzia.
* V: mówisz... jak go zgubiłeś? Łabędzica Ci uciekła czy co?
* M: to naprawdę nieważne (Viorika już wie, że za tym jest historia). Ale trzeba go załatwić.
* V: ogarnę Ci ptaka... żebym ja musiała Ci ptaka kontrolować...
* M: Wirku, nie przechwycił go oddział Verlenów. 20 agentów i ptak uciekł. Bez strat, ale...
* V: daj mi raport z akcji gdzie zwiał...
* M: nie ma problemu, wiedziałem, że mogę na Ciebie liczyć!
* V: wisisz mi co najmniej prawdziwą opowieść
* M: Wirku, to nie jest... tego typu opowieść.
* V: still. Nic nie zmienia.
* M: łabędź nie jest tak mocny. Ja po prostu nie mam magów w okolicy. Jest Elena, ale...
* V: jak się nudzi, może się zabrać...

30 minut później.

* E: tien Verlen, na operację, zawsze! (oczy pałają optymizmem i Elena bardzo próbuje nie pokazać entuzjazmu)

Viorika dostaje raport od Marcinozaura i chce go przeanalizować pod kątem ptaka.

aspekty raport: (+dobry raport, -nieznany potwór, -interferencja Marcinozaura, -krótkie starcie)

Viorika podchodzi do problemu raportu analitycznie i dokładnie. Musi wiedzieć jaki oddział zebrać i ściągnąć na Samszarów. Ma do pomocy verlenlandzkiego doświadczonego łowcę potworów, który z nią długo pracował - Szymona. Szymona Kapcia (on niweluje aspekt 'nieznany potwór').

Tr Z (Szymon Kapeć) +3:

* X: Szymon zauważył, że Marcinozaur ukrył jakieś dane. Coś jest nie tak. Ten raport bez tego nie daje pełni pola.
* Vz: Szymon zauważył kilka podstawowych cech łabędzia:
    * ma pole deflekcji; dlatego nie udało się go zestrzelić. Ma też iluzje. Jest szybki. I na pewno nie ma AoE.
        * aktualny plan Vioriki? MEGA DUŻA SIEĆ.
* V: Viorika wykryła dodatkowe rzeczy
    * stwór wyraźnie dąży do emocji i uczuć - dlatego Samszarowie
        * ale _coś_ steruje stwora by tam poleciał. Sam by nie poleciał. Nie z Verlenlandu tam. Ale oprócz tego Viorika ma pewność - "mięso i silne uczucia"
    * Viorika widzi (i Szymon potwierdza) - łabędź jest ZBYT SILNY jak na tą bioformę. Ten jest anomalnie silny. Coś się dziwnego stało. To nie jest "zwykły potwór".
        * Viorika facepalmuje. On. Kryje. Ulę.
            * Ten moment, gdy Verlen kryje Blakenbauerkę...

Viorika łączy się z Marcinozaurem.

* V: "KRYJESZ ULĘ!"
* M: "(próbuje wyglądać niewinnie) (nie wychodzi mu)"
* V: "Widziałam bardziej niewinnie wyglądających rekrutów po zjedzeniu oddziałowego deseru!"
* M: "Powiem Ci jak zachowam sekret. Sekret jest ważny."
* V: "Dobrze... nie powiem oddziałowi, że..."
* M: "Nie rozumiesz, nikt nie może wpaść na to. Nie wiem jak na to wpadłaś."
* V: (uraza) (milczenie)
* M: "Paradoks..."
* V: "Na Samszarów? XD"
* M: "Paradoks wynikał z tego, że rozmawialiśmy o Tobie. A dokładniej, o Karolinusie."
* V: "Nie łączcie mnie z tym."
* M: "Nie spodziewałem się tak żywiołowej reakcji z jej strony. Podczas walki z łabędziem ona miała Paradoks i musiałem się nią zająć. A łabędź odleciał. Mścić się za Ciebie. Nie wiem czemu tak zareagowała." (totalnie zaskoczony) "Ale nie chcę kolejnych problemów z Samszarami ani... żeby ona miała kolejne problemy, nie tak, nie takie."
* V: "Mogłeś tak od razu... czego jeszcze mi nie powiedziałeś?"
* M: "Ona nie może się do niego zbliżyć. Ula. Nie atakował, unikał, więc nie wiem co umie."
* V: "A co umiał przedtem?"
* M: "Krzyk. Łabędzi śpiew. Śpiew Terroru. Wpierw śpiewa potem je. I wężoszyja."
* V: "Potrzebujesz tego łabędzia?"
* M: "Nie. Ale nie chcę, by Samszarowie zobaczyli... infuzję Uli w potworze atakującym z Verlenlandu. Nie przeżyjemy hańby współpracy z Blakenbauerami by zaatakować mały fort."
* V: (dopiero teraz dotarł do niej PRAWDZIWY PROBLEM)

Marcinozaur spytał, czy Viorika gada z Apollem by załatwił to negocjacyjnie. Viorika powiedziała, że zanim Apollo to zrobi... ona po prostu wbije się z oddziałem na teren Samszarów. Dźwiedź i reszta.

Dwie godziny później:

Jeden oddział Vioriki ("Nie-dźwiedzie" w Lancerach), jedna Elena w mikro-Lancerze i jedna Viorika zbliżają się do Fortu Tawalizer.

* E: "Czyli, jak rozumiem, wpadamy, zabijamy potwora i wypadamy?"
* V: "Preferowana wersja"
* V: "Plan - zastawienie pułapki, złapać łabędzia w sieć i tyle."

Viorika chce dowiedzieć się, gdzie jest łabędź i co zrobił. Oraz czy jest Samszar i czy stanowi zagrożenie. Oddział Vioriki rozstawił polową stację do walki radioelektronicznej. Zdobyć info co wiedzą Samszarowie i czy ktoś widział ptasiora.

(aspekty sygnałów: +stacja przechwytująca, +nikt się nie spodziewa, +ogólnie lax security, -sentisieć interesująca się wszystkim)

Tr Z (stacja) +3, podniesiona kategoria sukcesów:

* V:
    * Maks Samszar jest przekonany, że to złośliwy ruch Verlenów. Bo czemu on tu jest!
    * Łabędź nie zeżarł żadnej kozy, ale polował na rolników - i tylko baterie oddaliły łabędzia od ofiar.
    * Łabędź skutecznie przeraził nobli wypoczywających nad jeziorem i to wina Maksa.
    * Łabędź zatopił jacht. Zanurkował, przebił poszycie od dołu. 
        * Łabędź ma atak ppanc. Nice.
* X: Systemy detekcji detektorów zamontowane u Samszarów zadziałały; grupka Verlenów będzie odkryta.
* X: Jednak Viorika została wykryta przez Samszarów. Ale Viorika o tym WIE (bo jej ludzie powiedzieli że zostali wykryci)
    * Viorika postanowiła jednak iść na współpracę jawnie a nie robić _mischief_. Użyjemy Eleny jako +1 do cuteness.

Viorika na bezczela bierze Elenę, oddział dookoła jeziora pilnować łabędzia (bo powinien tam być, tylko zanurkował kiedyś i śpi pod wodą czy coś XD) i na bezczela prosto jak strzelił na Maksa. Viorika i Elena korzystają ze swoich skilli manewrowania w Lancerach, ominąć żołnierzy by wlecieć prosto do HQ.

(aspekty: +zaskoczenie, +nikt nie chce zaczynać z Verlenami, -odległość, -zawodowy oddział, -alert level, +lancery w konfiguracji AGILE, ==nie ma deklaracji walki (próba złapania w przelocie ), -sentisieć Maksa, +Viorika do Maksa na głośnikach że to rozmowa)

Tr +3:

* X: Żołnierze Samszarów okazali się dobrze bronić HQ. Viorika NIE CHCE się przebijać, więc się odbiła od "pajęczych sieci" wszędzie. Musiała z Eleną lądować i zostały otoczone.
    * Viorika znalazła dowódcę i rozkaz "prowadź mnie do Samszara".
* X: Dowódca chciałby, naprawdę chciałby. Ale "tien Verlen, mam rozkazy. Proszę zdać broń i Was zaprowadzę."
    * Elena: "nie ma czasu, tam jest potwór."
    * Viorika: "jak już ukradliście nam potwora miejcie tyle przyzwoitości by zająć się nim na tym terenie. Prowadź do dowódcy!"
* X: Jednak muszą oddać sprzęt...
    * Viorika: "Elena, pilnujesz sprzętu"
    * Viorika opuszcza servar i idzie z podoficerem do Maksa.
    * Elena groźnie patrzy na żołnierzy a oni patrzą na nią z lekkimi uśmiechami.
    * ŁABĘDŹ MA RUCH.

Viorika ma zawsze komunikator do oddziału. Maksymilian siedzi na "krześle szefa". To młody, 22-letni mag. Patrzy na Viorikę z triumfem.

* M: "Tu Verleńskie podejście nie zadziała. Tu jest cywilizacja. Mogłaś przyjść i poprosić o spotkanie bez tych 'theatrics', tien Verlen."
* V: "tien Samszar, nie ma czasu. Nie wiecie co nam zarąbaliście. Rozumiem, łąbądź niczego sobie, można się ponapieprzać, ale to nie wasza klasa."
* M: "Co? (szok)"
* V: "po co ukradliście nam łabędzia? mogliśmy wysłać Wam potwora na miarę Waszych możliwości"
* oddział->V: "tien Verlen, łąbędź się rusza! Przygotowujemy przechwycenie! Ale Samszarowie interferują!!!"
* V->M: "moi ludzie mają ptaka na celowniku, odwołaj swoich"
* M: "poradzimy sobie sami, tien Verlen, to NASZ łabędź. Do nas uciekł. (nieprzekonanym głosem, myśli)"
* V: "nie chcesz tego łabędzia. Naprawdę go nie chcesz. Zapłaciliśmy Blakenbauerom by go wzmocnić. Musieliśmy zrobić coś by nasze dzieciaki miały wyzwanie, po to przywiozłam Elenę. To jej cel."
* M: "CO?! (szok)"

(aspekty: +Verlenowie słyną z walki z potworami, -Maks nie lubi Verlenów TERAZ, +Maks w sumie nie chce tego łabędzia, +opowieść Vioriki ma sens, +Maks może to przekuć na swój sukces)

Tp Z +3:

* V: Maks się zgadza na plan Vioriki i daje jej swobodę działania
* X: Maks zadziałał szybko. Ale jego oddział, niekoniecznie elitarny, zadziałał za wolno.
* X: Viorika wysłała Elenę przodem, ale Elena nie zdąży. Nie przez żołnierzy itp. Komunikacja SZYBKA DYNAMICZNA kuleje. Procedury Samszarowie mają tu dobre, ale dynamicznie nie. Elena startuje i leci. Ale nie dotrze na początek walki, nie na czas.

Czas na starcie dwóch oddziałów z Łabędziem. Bez Eleny i bez Vioriki. Łabędź ma pęd. Aktywna strona to oddziały.

oddział Vioriki: (+ma sieć, -sieć nie jest na jeziorze, +przygotowani, +servary, +monster hunters, +mięso armatnie z Samszarów, +enemy is pinned)

Tr +3:

* V: Oddział Vioriki zorientował się że łabędź startuje i przygotował serię zaporowego ognia. To uniemożliwiło łabędziowi wylecenie z wody. Dziób do góry - dziób na dół.
* X: Wężowy łeb łabędzia złapał nieszczęsnego żołnierza i ściągnął go do wody. Samszarowi żołnierze "nie strzelać!!!". Łabędź zmultiplikował iluzjami nieszczęśnika i siebie. Buduje zaporę.
* V: Nanokrystaliczna sieć została rozpostarta w jeziorze. Łabędź nie ma jak się wydostać. Nieszczęśnik nie żyje.
* X: Łabędź wyemitował ostre światło głęboko pod wodą, pod sobą, cień światła padł na wielu żołnierzy. Poważna większość obecnych nie może się ruszyć.
* X: Łabędź zaczyna przegryzać nanokrystaliczną sieć; ma dość silny dziób by być w stanie, ale sieć jest bardzo odporna (2)
    * Na to wlatuje Elena.
* (+M) X: Łabędź wyrwał się na wolność i Krzykiem uderzył w większość sparaliżowanego oddziału. Niedobitki Verlenów wzmacniają Elenę. Elena używa pełni mocy magicznej a oddział próbuje trzymać Łabędzia nisko.
* Ob: Elena próbowała opleść łabędzia siecią. Niestety, zintegrowała łabędzia z siecią. Sentiinfuzja Uli zaadaptowała sieć. Mamy łąbędzia ze zintegrowaną siecią.
    * Łabędź, skonfliktowany różnymi energiami, odlatuje na wschód. Głębiej do Samszarów.
    * Viorika dała radę założyć power suit. Dopiero teraz. Jeszcze jej tam nie ma.
* V: Elena nie chce dać łabędziowi uciec. Po prostu NIE. Widząc, że jej magia nie zadziałała jak chciała włączyła dopalacz i walnęła w łąbędzia z całej mocy. Zaplątała się w łabędzia (sieć) i zderzyła się z jakimś budynkiem z ogromnym hukiem. Łabędź jest ranny. Elena lekko oszołomiona. Ale Elena ma pęd.
* Vm: Oddział Vioriki rzucił się ratować Elenę i kotwiczyć łabędzia.
    * TAK, są ludzie podziobani. Żołnierze Vioriki swoim ciałem bronili Eleny (dziecko).
    * Elena zdekombinowała łabędzia. Wszystkie aspekty łabędzia zaczęły same walczyć ze sobą.
    * Łabędź się zerwał i wzbił w powietrze, ale jest w nim Skażenie, rak, który go pożera.

Na to wchodzi Viorika. Viorika ma działo. Ciężkie działo. Ustawiła się, przycelowała i artillery strike. Z ziemi. Przewidziała, gdzie łabędź będzie.

* V: Po tym jak Elena zneutralizowała pole siłowe łabędzia (anty-integracją), pojedynczy strzał Vioriki z ppanc zestrzelił łabędzia. Gore opadło na miasto. Niewielkie gore bo nieduży łabędź.

Z dobrych wieści, osoby których dotknął łabędzia cień zaczęły wstawać. Są bardzo osłabione, ale żywe. Czyli jest grupa rannych, ale jedyna śmierć to ten nieszczęśnik co podszedł za blisko jeziora.

Maks przybiegł najszybciej jak był w stanie, w Lancerze. Zanim się zebrał Viorika już zdążyła zestrzelić strasznołabędzia.

Trzeba lokalnym siłom szybkiego reagowania oddać, że oni sprawnie zajmują się medical itp. Viorika im pomaga, ale oni wiedzą co i jak. Elena stoi dumna koło Vioriki. Zmęczona, ale nie jest ranna. Tu Viorika jest dumna ze swoich ludzi, nie z Eleny. Choć trzeba jej oddać, że zrobiła dobrą robotę. "Good job" od Vioriki. I do swoich ludzi. Oni rozumieją. Oni WIEDZĄ. Oni też są dumni.

Maks patrzy na zniszczenia. 

* M: "To był dobry żołnierz. Głupio zginął."
* (dźwiedź na boku): "Stał za blisko. Nie był dobry. Ale chciał dobrze."
* M: (robi się purpurowy)
* V: (odsyła dźwiedzia do noszenia rannych) "Masz dobrych ludzi, nie mają wyszkolenia. Nie na to."
* M: "Czemu to tu przyszło? Z takimi rzeczami macie na co dzień do czynienia?"
* V: "Mniej więcej"
* M: "Skontaktuję się z Tobą, pomożesz mi z procedurami obronnymi tego fortu."
* V: "Nie wolisz wysłać ludzi do nas na szkolenie? To że wymyślę procedury nie znaczy że to zrobią dobrze. Zapomną."
* M: "Bez urazy, ale nie chcę, by mi poginęli."
* V: "To nie jest stwór szkoleniowy."

Maks przekazał Viorice nagrania z walki. Viorika odwdzięczyła się dźwiedziem łącznikowym na miesiąc. Dźwiedź pomoże. Maks BĘDZIE żałował. Ale będzie to wszystko działać lepiej.

## Streszczenie

Marcinozaurowi uciekł łabędź (Ula miała Paradoks, Marcinozaur ją kryje). Viorika z Eleną ruszyły go przechwycić w samszarskim Forcie Tawalizer. Chciały wbić się dyskretnie, ale Maks Samszar ma za dobrych ludzi. Gdy rozbroili Viorikę, łabędź się ruszył. Zła współpraca między verlenlandczykami i samszarowcami doprowadziła do tego, że łabądź zabił jednego z samszarowców i PRAWIE uciekł - Elena go jednak zatrzymała uszkadzając poważnie Lancera (i kończąc ranna). Cholernie ryzykowny ruch. Viorika zestrzeliła łabędzia i potem współpracuje z Maksem by problem się nie pojawił.

## Progresja

* .

## Zasługi

* Viorika Verlen: wiedząc, że Marcinozaurowi uciekł łabędź, zdecydowała się mu pomóc i wziąć Elenę (ćwiczenia). Nie udało jej się tego zrobić dyskretnie. Używa 'cuteness' Eleny jako zasobu do przekonania Maksa. Gdy musiała oddać sprzęt, zaufała swoim ludziom (planom) i Elenie. Współpracuje z Maksem po wszystkim, daje mu dźwiedzia konsultanta bezpieczeństwa.
* Marcinozaur Verlen: Paradoks Uli sprawił, że dopakowany strasznołabędź uciekł do Fortu Tawalizer. Poprosił o pomoc Viorikę i kryje Ulę. Nie naraził V. na ryzyko, acz powiedział tylko to co musiał. 
* Ula Blakenbauer: miała Paradoks i dopakowała strasznołabędzia do absurdalnej mocy. Marcinozaur ją kryje.
* Elena Verlen: pała entuzjazmem do KAŻDEJ operacji Vioriki, też polowanie na łabędzia. Służy jako zasób 'cuteness' do przekonania Maksa. Jako jedyny mag wspiera oddział Verlenów walczący z łabędziem, ledwo sobie radzi i robi SKRAJNIE ryzykowne manewry. Ale się udało.
* Szymon Kapeć: znajomy ekspert od potworów Vioriki; doszedł do głównych cech łabędzia (emocje, deflekcja). Dobrze doradził Viorice co zrobić i jak.
* Maks Samszar: rozstawił proceduralnie Fort Tawalizer i nie dał Viorice się cicho wślizgnąć; dobrze to zrobił. 22-letni mag przyzwyczajony do pewnej siły potworów; łabędź był silniejszy. Dał się przekonać Viorice, ale przez głupie proceduralne podejście i ego V. nie uczestniczyła w walce od początku - więcej rannych. Dał się jej przekonać do wzięcie dźwiedzia konsultanta bezpieczeństwa.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Fort Tawalizer: niewielkie miasteczko na granicach z Verlenlandem (na północy)
                                1. Wzgórza Potworów (N)
                                    1. Obserwatoria potworów
                                    1. Baterie defensywne
                                    1. Farmy kóz: z funkcją odwracania uwagi potworów
                                1. Centrum Miasta (C)
                                    1. Luksusowa dzielnica mieszkalna
                                        1. Jezioro Fortowe: niezbyt może uzbrojone ale śliczne. Tam siedział łabędź i tam z łabędziem toczyła się walka.
                                1. Obserwatorium Potworów i Fort (E)
                                    1. Koszary garnizonu
                                1. Dzielnica mieszkaniowa (S)
                                    1. Domy mieszkalne
                                    1. Sklepy i centra handlowe
                                    1. Szkoły i placówki oświatowe

## Czas

* Opóźnienie: 3
* Dni: 2

## OTHER
### Fakt Lokalizacji
#### Miasto Fort Tawalizer

Dane:

* Nazwa: Fort Tawalizer
* Lokalizacja: 

Fakt:



1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Fort Tawalizer: niewielkie miasteczko na granicach z Verlenlandem (na północy)
                                1. Wzgórza Potworów (N)
                                    1. Obserwatoria potworów
                                    1. Baterie defensywne
                                    1. Farmy kóz: z funkcją odwracania uwagi potworów
                                1. Centrum Miasta (C)
                                    1. Ratusz
                                    1. Park miejski
                                    1. Bazar rękodzieła
                                    1. Luksusowa dzielnica mieszkalna
                                        1. Rezydencje
                                        1. Ekskluzywne restauracje
                                        1. Spa i centrum relaksu
                                        1. Jezioro Fortowe
                                1. Obserwatorium Potworów i Fort (E)
                                    1. Laboratoria badawcze
                                    1. Biura klubu potworologów
                                    1. Koszary garnizonu
                                1. Dzielnica mieszkaniowa (S)
                                    1. Domy mieszkalne
                                    1. Sklepy i centra handlowe
                                    1. Szkoły i placówki oświatowe
                                1. Dzielnica przemysłowa i rolnicza (W)
                                    1. Magazyny i warsztaty
                                    1. Farmy i pola uprawne
                                    1. Młyny i zakłady przetwórcze
