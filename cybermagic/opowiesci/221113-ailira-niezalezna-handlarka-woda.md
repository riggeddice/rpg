## Metadane

* title: "Ailira, niezależna handlarka wodą"
* threads: historia_darii, salvagerzy-anomalii-kolapsu, syndykat-aureliona-w-anomalii-kolapsu
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [221111 - Niebezpieczna woda na Hiyori](221111-niebezpieczna-woda-na-hiyori)

### Chronologiczna

* [221111 - Niebezpieczna woda na Hiyori](221111-niebezpieczna-woda-na-hiyori)

## Plan sesji
### Theme & Vision

* Stacja 'Nonarion Nadziei' nie zawsze ma dla Ciebie najlepszy sprzęt i czasem masz pecha
    * Życie na stacji Nonarion. Okrutne.
    * Hiyori i Nonarion. Reakcje kilku postaci - Iga oraz Kaspian. I Nastia.
* Ailira zrobiła kiedyś błąd, skończyło się fatalnie

### Co się wydarzyło KIEDYŚ

* .

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

.

### Co się stanie (what will happen)

* S01: Rywale wyśmiewający Hiyori; neikatianie eks-przemytnicy. Ale zapewniają dla Nastii coś czym ta może dorobić, coś militarnego. She's in.
    * Filip Szukurkor (ENCAO: 0-0+- |Purytański;;Bezkompromisowy| VALS: Achievement, Face >> Self-direction| DRIVE: Wygrać w rywalizacji); kiedyś Orbiterowiec, 44 lat
* S01: Daria - lepszy sprzęt czy gorszy, ale od nowego podwykonawcy który ma poważny kłopot - faza 1. 
* S02: Daria - możliwość audytu innej jednostki. Zrobi DOBRZE (i się narazi) czy zaakceptuje problem. Tracker.
* S03: Safira - proorbiterowcy chcą założyć na nią ograniczniki. Plus, chcą wsadzić tracker ;-).
* S04: Polowanie na Kaspiana - trzech kolesi (kiedyś z Orbitera) uważa, że on był zabójcą i zabił ich przyjaciela podczas wojny.
    *  Julia Karnit (ENCAO:  +0--0 |Nie dba o to co inni czują;;Śmiała i przedsiębiorcza| VALS: Security, Face >> Achievement| DRIVE: Zatrzymać czas); poluje na Kaspiana, kiedyś Orbiter, 36 lat
* S05: Iga - zrozpaczona. Zbankrutujemy! Poszła do Ailiry ją opieprzyć. Jak ona mogła! Plus plotki ze stacji.
* S06: Ailira - praktycznie traci wszystko na stacji. Zaufanie, straciła jednostkę itp.
    * trzy jednostki nie wróciły, 1 uszkodzona i udało się SOS, jeszcze szukamy
    * zabrali jej grazer. Nie ma nic.
    * sprzeda części sarderytom i przekaże ofiarom. ==> Moduł "Remedianin" (medical part)
* S07: Daria - znaleźć wszystko co ukryto w Hiyori po naprawie + czy pomoże podwykonawcy? (faza 2)

### Fiszki

#### Nonarion Nadziei (stacja cywilna)

* Leo Kasztop: sprzedawca sekretów na Nonarionie (atarienin)
    * ENCAO:  0-+00 | Intrygancki, polityka;;Nie odracza| VALS: Self-direction >> Stimulation, Tradition| DRIVE: Wygrać w rywalizacji)
* Ailira Niiris: tanio sprzedaje surowce na Nonarionie (adastratka) (34 lata)
    * ENCAO:  0-0+- | Stoicka;;Mówi jak jest;;Dobrze wychowana| VALS: Face >> Humility| DRIVE: Odbudowa i odnowa
* .Franciszek: enforcer Nonariona (atarienin)
    * ENCAO:  +00-- | Lubi rutynę;;Uszczypliwy i zgryźliwy;;Lubi wyzwania | VALS: Achievement >> Security| DRIVE: Komfortowe życie
* .Wojciech: agent Aureliona (faeril)
    * ENCAO:  ---+0 |Bezbarwny, przezroczysty;;Niemożliwy do ruszenia| VALS: Power, Humility >> Tradition | DRIVE: Supremacja Aureliona

#### Hiyori (salvager)

* .Daria jako salvager (inżynier)
* Ogden Barbatov: 44 lata, kapitan + marine + salvager (atarienin)
    * ENCAO:  +-00- |Osoba starej daty;;Jeżeli czegoś chce, weźmie| VALS: Achievement, Power >> Conformity| DRIVE: Uwolnić niewolników
* Nastia Barbatov: 43 lata, pilot + marine + salvager (atarienka)
    * ENCAO:  +0--- |Anarchistyczna, nie ufa władzy;;Lubi żartować| VALS: Universalism, Stimulation >> Face| DRIVE: Utopia Star Trek
* Kaspian Certisarius: 37 lat, advancer, noktianin (adastranin); próbuje być daleko od wszystkich
    * ENCAO:  -+-00 |Skryty;;Bardzo Ostrożny;;Nie widzi nadziei| VALS: Tradition, Family, Hedonism| DRIVE: Corrupted contagion (coś na K1)
* Jakub Uprzężnik: 29 lat, advancer-in-training (atarienin)
    * ENCAO:  0+0-0 |Zdradliwy;;Podejrzliwy| VALS: Stimulation, Security >> Achievement| DRIVE: Rana ego
* Iga Mikikot: 29 lat, medyk + PR, (altinianka); próbuje wszystkich nawrócić
    * ENCAO:  0--+- |Dogmatyczna (Seillia), potępia;;Przyjacielska| VALS: Hedonism, Face >> Power, Humility| DRIVE: Femme Fatale
* Patryk Lapszyn: 28 lat, puryfikator + scientist; nie mag (sybrianin); próbuje wejść Idze lub Darii do majtek
    * ENCAO:  00-+- |Tendencje do podkradania;;Pomaga innym| VALS: Self-direction, Stimulation >> Security| DRIVE: Wolność od innych
* .Adam + Ewa d'Hiyori: androidy wsparcia pod kontrolą Safiry i z opcją inkarnacji Safiry.

### Sukces graczy (when you win)

* .

## Sesja właściwa
### Przed sesją - kontekst

Zasoby: (FINANSE, WPŁYWY, SUPPLIES, SPECIALIST), (WODA, VOLATILES, METAL, RARE, FISSILES, EXOTICS, BIO, VISIMAT)

### Scena Zero - impl

.

### Sesja Właściwa - impl

Dzień 1:

Nastia zebrała zespół na naprawianej Hiyori. Wyłączony reaktor, Safira na awaryjnym (zapasy ze stacji). Nastia zaczęła dość ponuro. "Nie mamy środków. Nie mamy pieniędzy." Wszyscy spojrzeli na siebie, ale Nastia zawsze coś wymyśli. Nastia "ale pieniądze będą - Ogden da radę rozwiązać _tamten_ problem. Plus mamy pewne zapasy, sęk w tym, że nie na Nonarionie." Nastia podsumowała sprawę tak "Za kilkanaście dni Hiyori wróci do działania, będzie nas stać na kolejną operację. Ale jeśli macie jakieś zaskórniaki... nie mamy aktywnego statku. Shore leave. Nie chciałam tego, nie spodziewałam się, ale nie mam wyjścia." Iga na to "ale tego nie planowaliśmy!" Nastia "Iga, wiem jak to jest. Nikt tego nie planował. Jeśli potrzebujesz pieniędzy, pożyczę Ci. Ale sama nie mam dużo - muszę doprowadzić Hiyori do działania."

Nastia wydała podstawowe polecenia:

* Daria -> nadzoruj naprawy. Sprzęt, części, popatrz czy czegoś nie da się zrobić. Ty nie masz shore leave, acz nie wierzę, że zajmie Ci to 100% czasu.
* Jakub -> skup się na uprawnieniach advancera. Masz teraz jakiś tydzień.
    * J: to wymaga pewnych łapówek.
    * N: can't help it. Nie mam kasy.
    * I: może zdaj je sam?
    * J: Igusia, tak to nie działa. Tutaj jak nie masz kasy, nie masz uprawnień. Przypomnij mi, Twoje uprawnienia medyczne?
    * I: (colored red) ...nie mam, mam studenckie...
    * Nastia: spokój. 
* Kaspian: "przejrzę kadłub z zewnątrz."
* I: "ale po co? Nic nie uległo uszkodzeniu?!"
* K: "przejrzę kadłub z zewnątrz."

Usłyszeliście klaskanie. Nieznajomy wchodzi na pokład jednostki. 

* F: "Nastia, Nastia, Nastia... i to Ci zostało?" /pogarda
* N: "Filip."
* F: "Słuchajcie, jeśli ktoś chce zmienić jednostkę na sprawną, zapłacę dwa razy tyle co Nastia"
* /wszyscy patrzą na siebie tak... wtf. Tylko jeden Kaspian ma oczy ryby.
* N: "Joke's on you. Mało płacę. Nie wierzą w to, że można podwoić. Dwa razy zero i te sprawy."
* J: "Ogden nam płaci, nie Nastia. Więc pańska oferta... odrzucona."
* F: /śmiech. "Warto było zostawać salvagerem? Wiesz, ile przed Tobą potencjału w walkach kosmicznych?"
* N: "Jak powiedziałam, dawno. I za nami."
* F: "A gdybym Ci obiecał jedną LEGALNĄ akcję gdzie potrzebuję marine oraz advancera? I dobrze płacę?"
* N: /wyraźnie nad tym myśli
* N: "Wyszłam za Ogdena, nie za Ciebie." 
* F: "I dlatego chcę Ci pomóc. On spieprzył Wasze życie, nie chcę, by Wasza załoga też cierpiała." /poważnie "Prosta operacja, anomalny noktiański shit."
* N: "Masz załogę?"
* F: "Weterani."
* N: /wahanie "Dam Ci znać za godzinę."
* F: "Jedna marine, jeden advancer. Nie mam więcej slotów. Nie potrzebuję Was, ale jeśli płacą /shrug. Mogę uzupełnić roster Wami. Aha - zero Ogdenów. Na gówno nie mam slotów."
* D: "Kto płaci?"
* F: "Orbiter. Jakiś pomniejszy kapitan. Anomalna jednostka, potencjalni noktianie, potencjalne anomalie. Jemu nie wolno, ale jak potwierdzimy - będzie wolno."
* N: "Czyli jesteś najemnikiem."
* F: "Lepiej płacą niż salvagerowi. Ale bez obaw, od noktian bym nie brał."
* N: "Dobra, zostaw nas w spokoju, powiem Ci za godzinę."
* F: "Ciao, bambino."

Nastia się aż wzdrygnęła jak poszedł. "Nie cierpię tego człowieka". Po chwili zastanowienia - "Dario, dowodzisz inżynierią..." Daria "idziesz w to?" Nastia "nie mamy wyjścia. Jedna gęba mniej, kasa do przodu. Acz uważam, że to będzie cholernie niebezpieczne." Jakub "słyszałem o pieniądzach. Też chcę." Nastia spojrzała na Kaspiana, który ma rybie oczy jak zawsze i tylko wzruszył ramionami. Nastia, z ociąganiem "ok, wchodzisz. Ale 10% kasy idzie na Hiyori." Jakub się skrzywił, ale nic nie powiedział.

Nastia opuściła jednostkę ze słowami "nie zgińcie tu beze mnie. Safira - dowodzisz jak mnie nie ma." KU WIELKIEMU PROTESTOWI WSZYSTKICH (poza Kaspianem, który ma wszystko gdzieś).

* I: "Safira?! TAI?!"
* D: "Safira, słońce, nic do Ciebie nie mam, ale... nie."
* P: "Nastio, chcesz powiedzieć, że Safira w trybie oszczędzania energii ma być... jest... niż my?! Czemu nie Kaspian?!"
* N: "Właśnie, czemu nie Kaspian" /z przekąsem
* K: /shrugs
* Safira: "Nastio, to najgorszy pomysł jaki znam. Jestem nieodpowiednia do dowodzenia. Mój pattern nie pochodzi od osoby dowódczej. Będę w trybie oszczędzania energii. Co więcej, NIKT nie będzie słuchał moich rozkazów."
* D: "Nawet gorzej, nie wiem czy któreś z napraw nie wymagają wyłączenia."
* N: "Ochotnicy do dowodzenia?"
* D: "Proponuję Kaspiana."
* I: "To dobry pomysł. Kaspian się zna na rzeczy."
* P: "Aha, Kaspian."
* S: "Zgadzam się z przedmówcami."
* K: /mina człowieka który naprawdę nie chce tu być + WTF happened.
* N: "Dobrze, Kaspian, jesteś głównodowodzącym. Utrzymaj ich przy życiu. Masz dostęp do kasy i możesz podpisywać dokumenty. Zgodnie z danymi od Filipa, wrócimy za 6-10 dni."
* K: "Dobrze, zaopiekuję się nimi jak moimi dronami."
* P: "Ostatnio tracisz sporo dron!"
* K: /lodowaty uśmiech, który szybko przerodził się w taką minę zrezygnowanego człowieka.

Nastia i Jakub opuścili Hiyori. Kaspian spojrzał na wszystkie panele, dane itp. 

* K -> D: "Dario, dostawcy." "Co dostawcy? Mamy sporo nagranych." "Różne części, różni dostawcy. Tańsi, lepsze, sprawdź na co nas stać." "Ahead of you there." "Sprawdź jeszcze raz - są nowi dostawcy."
* K -> S: "Safiro, COKOLWIEK dziwnego się dzieje - daj znać. Załóż, że jesteś w środku wojny i szukasz sabotażystów" "Kaspian, najpewniej nie byliśmy celem." "Najpewniej."
    * D: "Masz podstawy podejrzenia, że byliśmy celem?" K: "Nie." D: "Safira ma pewne ograniczenia. Są tu technicy itp." K: "To nie będziemy spać tylko dostaniemy sporo niepotrzebnych wiadomości." D: "A jak bardzo lubisz mieć silnik podłączony dobrym końcem?" K: "Safiro, przekazuj wszystkie wiadomości mnie. Nie budź Darii. Jak będzie trzeba, ja jej przekażę."
* K -> P: "Pracuj nad Leo. Może coś wiedzieć. Nowa jednostka, okazja biznesowa..."
* D -> I: "Dowiedz się coś o Ailirze, jej wodzie itp."
* K -> D,I: "Niebezpieczne"
* I -> K: "Przesadzasz. Prawie rozwaliła nam jednostkę. Jak NIE BĘDZIEMY PYTAĆ to wszyscy będą zdziwieni."
* K: /shrug
* P -> K: "Kurczę, jesteś najbardziej... miękkim dowódcą jakiego widziałem. Taki flak z Ciebie."
* K: /shrug
* P -> K: "A Ty co zrobisz?"
* K: "przejrzę kadłub z zewnątrz."
* S: "podejrzewasz bombę."
* K: "bombę, tracker, (pauza) coś. Coś, czego nie wykryjesz."
* I: "Ale kto? Po co?"
* K: /shrug

Kaspian chce iść sam na spacer kosmiczny. Daria "To łamie wszelkie zasady bezpieczeństwa. Nie chcemy płacić służbom portowym za łowienie advancera.". Kaspian "to kto idzie ze mną? Wszyscy są zajęci. Mam Safirę.". Daria "ja idę z Tobą, mam coś do sprawdzenia." Daria zobaczyła coś w oczach Kaspiana czego się NIE spodziewała. Irytację. Ale /shrug i próbował nie pokazać. ALE CZEMU? To nie ma sensu.

Kaspian wziął BATERIĘ NARZĘDZI ze sobą. Zdaniem Darii - 75% z nich nie przyda się mu do jego celu:

* ręczny komunikator laserowy dalekiego zasięgu
* niewielkie ładunki wybuchowe
* magnetyczny przytwierdzacz (snajperować z kadłuba)
* ...

Innymi słowy, PO CO? Ale zapytany, Kaspian daje bardzo sensowną odpowiedź która jest na 100% nieprawdziwa.

Tak czy inaczej - Daria ogląda statek z zewnątrz, robi pobieżne analizy, zdjęcia i wszystko wskazuje na to że nic złego się nie dzieje. Wysyła zdjęcia bezpośrednio do Safiry, niech ona coś wykryje. Coś dziwnego i niewłaściwego. A kątem oka patrzy z zaskoczeniem jak Kaspian np. używa lasera komunikacyjnego wysyłając sygnał w kadłub. Nie ma szans działać. On robi... dziwne rzeczy.

* D: "Co próbujesz osiągnąć?"
* K: "Patrzę, co się stanie."
* D: "Ale co masz nadzieję, że się stanie lub nie. Po coś to robisz."
* K: "(chwila pauzy) Nie wiem jaki wpływ miała ta woda. Aplikuję WSZYSTKO co mam, by zobaczyć, czy nic się nie zmieniło." /paranoia mode
* K: "(chwila ciszy) Plus, mogę. Mam czas."

Daria uzupełnia informacje ze zdjęć. Szukają nietypowych, dziwnych rzeczy. Ciężki dzień pracy, ale nie udało się znaleźć nic "złego". Czyli dobrze. Wszystko jest w NORMIE. Przy okazji wyjaśnia Kaspianowi jak użyć czegoś lepszego niż laser komunikacyjny do sprawdzania rzeczy. Kaspian przyjął.

Nie zmienia to faktu, że zdaniem Darii Kaspian zachowuje się DZIWNIE. Tzn. zawsze zachowuje się dziwnie a to jest dowód na dziwność. "Too long in space" itp.

Dzień 2:

Spotkanie poranne. Kaspian zaczyna spotkanie jak zawsze - pokazuje gestem, by Patryk zaczął.

Patryk próbował załatwić u Leo co i jak. Ale próbował też się dowiedzieć więcej odnośnie sprawy z Ailirą od Leo. Ku dezaprobacie Kaspiana.

Tr +2:

* V: Patryk ma informacje o nowych dostawcach dla Darii: 
    * Jest nowa firma na rynku. "Monarchix". Naprawdę. Mają jednostkę wsparcia i blueprinty. Chcą wejść na rynek sprzętu dla Nonariona. 
    * Jest też niewielki dostawca, nowy, "Trójbracia". Dosłownie trzech braci. Sprzęt tani, niezbyt solidny. Skupują od salvagerów by refitować. Nieźle refitują. Znają się na rzeczy.
    * Obie grupy trafią do Darii docelowo - są w jej kontaktach które musi rozważyć tego dnia. Obecność Monarchix sprawia, że Trójbracia są bardziej zdesperowani.
    * K -> I: "Dziś pomożesz Darii w negocjacjach z oboma podmiotami"
    * I: "Kto mówi 'podmiot' nieironicznie?"
    * D: "Kaspian"
    * K: /shrug
* V: Patryk dowiedział się też o Ailirze i jej stanie
    * Ailira straciła swoją jedyną jednostkę. Grazer. Został skonfiskowany przez kapitanat.
    * Z sześciu jednostek które miały niebezpieczną wodę udało się uratować dwie, cztery jeszcze szukają. Hiyori jest jedną z tych dwóch.
    * Zdaniem Leo Ailira "is radioactive, don't talk, don't touch". -> nie wyciśnie się kasy z nędzarza.
* V: Leo ma dobry statek dla Hiyori. Siedzi na nim. Nie sprzedaje, nie mówi. Czeka, aż Hiyori wydobrzeje. Wisi to Ogdenowi.
    * Tu jest jakaś historia. Patryk podejrzewa coś smacznego, bo Ogden jest eks-Orbiterowcem a Leo to pełną gębą kryminalista.

Iga powiedziała, że ona mówi następna. Ona próbowała się dopytać Ailiry o jak najwięcej. Złapała ją sam na sam, gdy Ailira była gdzieś przy oknie. Ailira wyraźnie została przez kogoś pobita. Ale nie tak by skrzywdzić a tak by bolało.

Ex Z (prawdziwe współczucie i chęć pomocy) +3:

* Vz: Ailira powiedziała, że bardzo, bardzo przeprasza. Powiedziała, że nie miała wyboru. Nie wie o co chodzi. Sama zrobiła skany. Nic nie wskazywało na to. Ale i tak by to musiała zrobić. I nienawidzi się za to.
    * Zdaniem Igi, she is broken.
    * Zdaniem Kaspiana, to jest moment, w którym Iga przestaje dotykać tego tematu. "Ty tego nie dotykasz. Nastia? Może. Ogden? Może. Ty? Nie."
    * Iga: "Ailira nie ma pieniędzy nawet na... swój biznes."
    * Kaspian: "I kto od niej kupi?"
    * Iga: "Hiyori."
    * Kaspian tylko SPOJRZAŁ. "She's compromised."
    * Patryk: "Znajdzie jakiś sposób żeby z tego wyjść. Zawsze jest jakiś sposób. Może dołączyć jako salvager lub jako operatorka grazera. Jest niezła. Może stąd wyjechać. Lub któryś z flot grazerskich tutaj."

.

* Daria -> Kaspian: "Możesz sprawdzić, że wnętrze zbiorników jest czyste? Nic nie złożyło jaj itp?"
* Kaspian: "Sprawdzę. Gdy dekontaminatorzy skończą."  /Daria zrozumiała wszystkie implikacje jego stwierdzenia.

Kaspian podsumował zadania:

* Daria -> dostawcy
* Iga -> pomoc Darii w negocjacjach
* Patryk -> znaleźć sposób na detekcję czy dekontaminatorzy nam czegoś nie zostawili
* Kaspian -> będzie śledził dekontaminatorów i działał na zewnątrz. On nie chce wchodzić na stację.

Daria zabiera się za przeglądanie ofert. Obie są nadspodziewanie ciekawe i Daria ma możliwość i sposobność porozmawiać z Trójbraćmi.

TRÓJBRAT: Ludwik Trójkadur || ENCAO: +0-+0 | Pomysłowy;;Idealistyczny| VALS: Power, Family, Security | DRIVE: Impress the superior (sybrianin)

Jesteście we fragmencie Nonariona o nazwie ExpanLuminis. Ten noktiański statek kiedyś miał ważną funkcję - teraz wszystko wycięto ze środka i został wielki targ + seria pokoi do prywatnych rozmów. Daria spotkała się z Ludwikiem, z nią Iga. Iga jest super ubrana - dekolcik i wszystko. Daria jest Darią. Ludwik patrzy na Igę. Daria jest szczęśliwa. Daria jest mózgiem, Iga jest głosem.

Ludwik pokazał Darii próbkę tego co ma do zaoferowania na podstawie publicznego manifestu informacji o Hiyori. Daria to przejrzała. Składniki są dość tanie, ale średniej lub kiepskiej jakości. Nie jest to coś, z czym Daria sobie nie poradzi - ale w niektórych przypadkach może być niebezpieczne. Nie są gotowe na miarę. Wymagają nieco innej pracy. Można powiedzieć, 40% ceny, 66% zaufania. A Daria nie lubi 66% zaufania. Zwłaszcza nie po ostatnich. Daria zażądała części wyższej jakości. Ludwik "sami zbieramy jednostki i próbujemy je puryfikować i wykorzystać ponownie. Nie mamy fabrykatorów, nie takich. Możemy współpracując z Wami zrobić lepsze części, ale nie przekroczymy bariery 80%."

Ludwik ma 'passionate plea':

* L: Dopiero zaczynamy. Dopiero nasze techniki i metody wchodzą w grę. Mamy ograniczone źródła materiałów i substratów, statków 'startowych'.
* L: Jeśli Wy nam pomożecie, my pomożemy Wam. Nie myślcie o tym jak o częściach, być może możemy pomóc sobie bardziej.
* L: Nigdy nie mamy super części - nie mamy jak. Ale możemy próbować coś z tym zrobić razem.
* I: Przy częściach niższej klasy możemy nie być w stanie wrócić.
* L: Nie zarekomendowałbym naszych usług jeśli jesteście jednostką wojskową. Ale w Anomalii powinno wystarczyć.
* Daria widzi, że oni mają dobrej klasy testy i dobrej klasy usługi. To nie to - te rzeczy się trzymają kupy.
* Daria pamięta, że Ogden zawsze wybierał rzecz, która jest bezpieczniejsza.

Daria podziękowała i spojrzała na innych dostawców. Monarchix ma DZIWNIE tani sprzęt patrząc na jakość, ale nie ma wszystkich potrzebnych blueprintów. Daria rozumie ten mechanizm - dopiero wchodzą, więc mają ceny promocyjne. Da się skorzystać. Normalnie warto byłoby kupić więcej, ale nie stać + nie ma gdzie przechowywać.

Daria zdecydowała się na następującą strategię:

* Balans WARTOŚĆ | KOSZT, gdzie jeśli mały przyrost wartości, COST jest ważniejszy
* Wszystkie kluczowe elementy mają WARTOŚĆ > KOSZT
* Wybieramy dostawców "etycznych", takich, co nie robią innym krzywdy
* Jeśli się da, priorytetyzujemy Trójbraci. Ale nie kosztem bezpieczeństwa jednostki.
* Redukcja finansowa kosztu ogólnego.

A Iga agresywnie dla Darii negocjuje wszystkie ceny.

Tr Z (negocjacje Igi) +3:

* V: Kluczowe elementy mają WARTOŚĆ > KOSZT i są w budżecie
* V: Balans WARTOŚĆ | KOSZT by redukować koszty tam gdzie to nie jest kluczowe by się wyrobić (-20% kosztu)
* V: Daria dała radę znaleźć dostawców etycznych, wykluczyła tych "innych".
* V: Darii udało się priorytetyzować Trójbraci by weszli na rynek i wiedzieli, że Hiyori pomogła.

Dwa bardzo ciężkie dni Darii i Igi.

Dzień 4:

Daria idzie przez ExpanLuminis szukać poszczególnych części, gdy ją ktoś zaczepił. 

* J: "Przepraszam Cię, Ty jesteś P.O. dowódcy Hiyori?" - pyta twarda marine. 
* D: "Zależy kto pyta". 
* J: "Nazywam się Julia Karnit; tak dowiedziałam się z raportu z kapitanatu. Czy Ty jesteś Daria Czarnewik?"
* D: "Jestem, o co chodzi."
* J: "Młoda jesteś na P.O."
* I: "Widziałaś mnie lub innych?"
* J: "Touche. Podobno na pokładzie macie advancera, Kaspiana. Gdzie teraz jest?"
* D: "Czego od niego chcesz?"
* J: "Jeśli to ten którego szukam, jest noktianinem i odpowiada za śmierć mojego dowódcy."
* I: "NASZ Kaspian NA PEWNO nie jest noktianinem!" /oburzenie "Nie pracowałabym z NOKTIANINEM!" /podniosła głos
* J: /shrug
* I: "To nie mógł być nasz Kaspian, nasz nawet nie umie walczyć. Dostał kiedyś w twarz w barze i się nie podniósł."
* J: "Noktianin którego szukam wykorzystał drony do wyciągnięcia nas poza pozycję i eksterminował jednego po drugim w sadystyczny sposób. Nie MUSIAŁ. 1v6."
* D: "I wygrał?!"
* J: "Dlatego go szukam. This one is a menace."
* D: "I co z nim zrobisz, nawet zakładając że go znajdziesz? Sam pokonał szóstkę. Jesteś sama?"
* J: "Jestem starsza i lepsza. A on został ciężko ranny i miał implanty które mają go utrzymać przy życiu. Zabił. Mojego. Brata."
* I: "Kaspian zwykle siedzi w mesie; mogę Cię zaprowadzić. Wątpię, że go znajdziesz. Jak nie siedzi w mesie Hiyori to siedzi w którymś z ukrytych barów na Nonarionie." 
* J: "Ukrytych barów?"
* I: "Tak mi mówi"

Iga próbuje przekonać Julię, by ta z nią poszła itp. Iga będzie słodka i sympatyczna - na pewno nie znajdzie Kaspiana.

Tr Z (Julia nie ma nadziei że to on) +2:

* Vz: Iga skutecznie przekonała Julię, że nie ma go teraz na statku i jest w jednym z ukrytych barów Nonariona.
* Vz: Iga skutecznie odwróciła uwagę Julii od TEGO Kaspiana. Julia będzie szukać i chce sprawdzić, ale nie priorytetyzuje tego
    * Iga: słodka, antynoktiańska, głupkowata
    * Kaspian: zupełnie nie tak wygląda i nie tak się zachowuje
    * Ogden & Nastia: oni są eks-Orbiter

Iga AUTENTYCZNIE nie wierzy, że to "nasz Kaspian". Julia mówiła, że on jest skutecznym eksterminatorem. Nasz Kaspian nawet nie umie walczyć. Iga nawet nie wierzy, że nasz Kaspian jest noktianinem. Bo inaczej Ogden i Nastia nigdy by go nie wzięli.

Daria zobaczyła REQUEST KOMUNIKACJI OD LEO.

* L: Jest możliwość dorobienia. Potencjalnie niemoralne. Jak wszystko. Ale w Twoim skillsecie.
* D: Rozwiń.
* L: Jest zlecenie z jednej jednostki na sprawdzenie części statku. Wiesz, roboty zrobionej przez mechaników.
* D: A niemoralne ponieważ?
* L: Jesteśmy na stacji Nonarion..? Nie wiem. Zawsze podejrzewam coś takiego. Nie wiem czemu tyle płacą.
* D: Kto?
* L: Classified jeśli nie weźmiesz.
* D: Kogo konkretnie szukają?
* L: Inżynier z jednostki która nie jest powiązana z żadnym z karteli Nonariona. Ani żadną dużą organizacją. Ani niczym 'nieetycznym'. Dlatego podejrzewam niemoralne lub lekko niebezpieczne.
* D: Czy Ogden by mnie do tego zarekomendował?
* L: Nie. Ogden Was ochrania od tego typu fuch i rzeczy.
* D: To zrezygnuję.
* L: Dobrze. Szukam dalej.

Dzień 5:

Na spotkaniu, Iga podzieliła się ze wszystkimi "OMG MARINE POLOWAŁA NA KASPIANA MYŚLAŁA ŻE TO NOKTIAŃSKI MORDERCA"

* Daria obserwuje Kaspiana, szuka reakcji.
* K: Przedstawiła się?
* I: Julia Karnit, z Orbitera. Kiedyś. Szuka zemsty za brata i oddział 1v6!
* K: /uśmiech . Zdecydowanie, to ja. /uśmiech
* I: /śmiech Nie wiedziałam, że masz poczucie humoru.
* K: Zwykle zostawiam mój humor dla Safiry.
* P: Ale to nie Ty, prawda?
* K: /spojrzenie niedowierzania . Gdybym był noktiańskim mordercą, byłbym tutaj? Czy zabijałbym za pieniądze?

Nikt nie uwierzył, że Kaspian jest mordercą. W sensie, MOŻE. Ale... ale nie. Nie taki 'low energy Kaspian'. Po spotkaniu, wszyscy się rozeszli. Daria poprosiła Kaspiana by chwilę został. Został.

* D: To Ty mnie wpisałeś jako P.O.?
* K: Tak.
* D: Po co?
* K: A kogo miałem wpisać?
* D: Siebie?
* K: Może.
* D: Byłaby to prawda?
* K: Może.
* D: No nie może, zostałeś wybrany.
* K: /apatyczny shrug
* D: Następnym razem chociaż mi powiedz, co?
* K: Protestowałabyś?
* D: Oczywiście że tak, ale bym wiedziała
* K: /apatyczny shrug
* D: Nie rób mi tak, po prostu
* K: Dobrze.
* D: Przynajmniej nie bez ostrzeżenia
* K: Dobrze. /z naciskiem
* D: Bo co jak Patryk się spije i muszę go wyciągnąć z kicia?
* K: /apatyczny shrug
* K: Wróć do pracy.
* D: Aye aye, cap'n
* K: /przetarł oczy

Safira dostaje od Darii czujnik czy dwa przy wejściu. Na wszelki wypadek.

Dzień 6:

Spotkanie. Iga jest ponura. Bardzo ponura.

* D: Co się stało?
* I: Na nasze konto wpłynęły pieniądze. Nie bardzo dużo, ale dostaliśmy.
* D: To... źle?
* I: Ailira nie żyje.
* D: O cholera... ok?
* I: Kojarzysz moduł Remedianin na stacji? Tam gdzie jest medycyna? Tam są sarderyci. Ailira sprzedała swoje ciało na części. Zabiła się by zapłacić ofiarom...
* P: Cholera...
* D: To jest...
* K: To. Nas. Nie. Dotyczy. Szkoda dziewczyny. To. Był. Jej. Sposób. Ucieczki. Zastanówcie się co to znaczy i ZAPOMNIJCIE. O. TEJ. SPRAWIE.
* P: Ale kto? Kto mógłby? Jak?
* K: Wystarczy, że zagrozisz rodzinie. To jest skuteczna ucieczka. Drop it.
* D: (opowiada o ofercie od Leo i całej rozmowie). Nie bierzcie dziwnych ekstra rzeczy, nie bierzcie nic, ani tanich pożyczek. Coś tu śmierdzi.
* I, P: (wzięli to na serio)
* K: Nic Wam nie grozi. Jeśli słuchacie rady Darii. Ogden i Nastia wiedzą co robią.
* D: Przeżyjemy parę najbliższych dni na pewno...
* K: Hiyori wróci do działania i opuścimy tą przeklętą stację. /najwięcej emocji ever.

## Streszczenie

Hiyori kończą się pieniądze. Nastia i Jakub biorą robotę najemniczą u byłego eks Nastii. Kaspian jest P.O. Daria składa Hiyori z różnych dostawców i możliwie tanich a dobrych komponentów. Na Kaspiana poluje niejaka Julia uważając go za komandosa advancera mordercę. Okazuje się też, że z 6 statków co miały wodę Ailiry 4 nie wróciły. Ailira została pobita, straciła grazera i sama sprzedała się sarderytom na części. Gdy Kaspian o tym usłyszał, absolutnie zakazał Hiyori coś z tym robić. Czekamy na Ogdena i Nastię. Jednak stacja Nonarion to zło.

## Progresja

* Ludwik Trójkadur: wie, jak bardzo Daria pomogła im w rozkręceniu biznesu przez wzięcie części do Hiyori i dobre przeliczenie na co można sobie pozwolić a na co nie.

### Frakcji

* .

## Zasługi

* Daria Czarnewik: wrobiła Kaspiana w dowodzenie załogą w zastępstwie Nastii. Złożyła z serii dostawców najtańszy akceptowalny model Hiyori. Dużo zajmowała się rzeczami technicznymi i szpiegowała Kaspiana. Iga za nią negocjowała. 
* Kaspian Certisarius: został tymczasowym P.O. kapitana ku swej wielkiej żałości. Daria go wrobiła. Dowodził bardzo delikatnie sugestiami ale 'do whatever'. Siedział na zewnątrz Hiyori większość czasu, może, bo Julia na niego polowała a może, bo robił coś innego - wynosił dziwny sprzęt. Gdy dowiedział się o śmierci Ailiry ZAKAZAŁ zgłębiania tematu i był wyjątkowo zdecydowany jak na niego. Czyżby faktycznie był komandosem - advancerem - mordercą?
* Nastia Barbatov: zdecydowała się na drastyczny ruch by utrzymać Hiyori po ostatniej operacji - poszła z Filipem (swoim eks chłopakiem) na operację najemniczą jako marine i dała Safirę jako P.O. Safira odmówiła, więc Kaspiana.
* Jakub Uprzężnik: poszedł wraz z Nastią na operację najemniczą Filipa Szukurkora by sobie dorobić. Lekko złośliwy, ale ogólnie chce pomóc.
* Patryk Lapszyn: nie miał wiele roboty; asystował Leo w sprawach dobrej jednostki dla Hiyori i wkręcał się w łaski Leo.
* Safira d'Hiyori: low powered mode, a jednak Nastia chciała zrobić z niej P.O. dowódcy. Wszyscy - łącznie z Safirą - zaprotestowali.
* Iga Mikikot: świetna negocjatorka i bardzo empatyczna; pomogła Darii znaleźć lepsze deale i dotarła do prawdy o Ailirze. Kaspian nie pozwolił jej zgłębiać tego tematu głębiej. Nieco naiwna i nieco zbyt optymistyczna.
* Leo Kasztop: kryminalista mający coś pozytywnego w przeszłości z Ogdenem; wyraźnie próbuje pomóc Hiyori odpychając ich od Ailiry i znajdując subzlecenia. Całkowicie amoralny, tylko chrupki się liczą. Ale ostrzegł Darię przed zleceniem.
* Ailira Niiris: KIA. Pobita; przyznała Idze, że nie miała wyboru dając im i innym jednostkom ten lód. Nie poradziła sobie psychicznie z tym co zrobiła i z potencjalnym zniszczeniem serii statków kosmicznych z jej winy. Popełniła samobójstwo sprzedając swoje ciało sarderytom z Modułu Remedianin i przekazując pieniądze ofiarom i rodzinie. 
* Julia Karnit: (ENCAO: +0--0 |Nie dba o to co inni czują;;Śmiała i przedsiębiorcza| VALS: Security, Face >> Achievement| DRIVE: Zatrzymać czas) (poluje na Kaspiana, kiedyś Orbiter, 36 lat) szuka morderczego advancera noktiańskiego który wygrał 1v6. Pokonał jej oddział i zabił jej brata. Podejrzewa, że szuka Kaspiana Certisariusa. Iga ją przekonała tymczasowo, że teraz go nie znajdzie i że to nie on.
* Filip Szukurkor: (ENCAO: 0-0+- |Purytański;;Bezkompromisowy| VALS: Achievement, Face >> Self-direction| DRIVE: Wygrać w rywalizacji) (kiedyś Orbiterowiec, 44 lat); przyszedł wyśmiać Nastię z jej nieszczęścia i ze stanu Hiyori; ma jakąś przeszłość (wybrała Ogdena a nie jego). Jako najemnik wziął Nastię i Jakuba na operację na anomalnym statku noktiańskim dla jakiegoś kapitana Orbitera. Chce wyśmiać ale też chce pomóc.
* Ludwik Trójkadur: (ENCAO: +0-+0 | Pomysłowy;;Idealistyczny| VALS: Power, Family, Security | DRIVE: Impress the superior) (sybrianin). Jeden z Trójbraci, którzy próbują wejść na rynek części z Anomalii Kolapsu po odświeżeniu. Bardzo tanie części (40-50%), ale max. 66-80% pewności i ufności. Daria chciała im pomóc i część ich elementów dodała do Hiyori po uczciwej niskiej cenie.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Libracja Lirańska
                1. Anomalia Kolapsu, orbita
                    1. SC Nonarion Nadziei: 
                        1. Moduł Remedianin: pod kontrolą sarderytów, mistrzowie medycyny, recyklingu i wymiany elementów.
                        1. Moduł ExpanLuminis: wszystko wycięto ze środka statku i został wielki targ + seria pokoi do prywatnych rozmów
                
## Czas

* Opóźnienie: 1
* Dni: 6

## Konflikty

* 1 - 
    * 
    * 
* 2 - 
    * 
    * 
* 3 - 
    * 
    * 
* 4 - 
    * 
    * 
* 5 - 
    * 
    * 
* 6 - 
    * 
    * 
* 7 - 
    * 
    * 
* 8 - 
    * 
    * 
* 9 - 
    * 
    * 
* 10 -
    * 
    * 
