## Metadane

* title: "Bohaterska śmierć Wężomysza"
* threads: brak
* motives: the-facade, the-aspirant, the-soother, body-horror, hartowanie-verlenow, koniecznosc-pelnej-dyskrecji, zrzucenie-winy-na-innych
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [240327 - Syrenopajęczak politycznie skomplikowany](240327-syrenopajeczak-politycznie-skomplikowany)

### Chronologiczna

* [240327 - Syrenopajęczak politycznie skomplikowany](240327-syrenopajeczak-politycznie-skomplikowany)

## Plan sesji
### Projekt Wizji Sesji

* Opowieść o:
    * Inspiracja:
        * Rykard "Togethaa! Familyyy!" (satisfied snake noises)
            * Młody Verlen dał się zniszczyć potworowi, bo nie jest w stanie pokonać największego demona - oczekiwań swoich rodziców
            * Potwór, nie osoba. Jest echo Verlena, nie ma tam w środku prawdziwego Verlena.
        * Mortal Love - "Existence"
            * "It's over it's finished, I am dead inside | I'm not gonna feel anymore | I am dead, please bury me, I am dead"
            * Młody Verlen () (19 lat) stracił CHĘĆ do życia. Nie chce walczyć. Sentisieć odpowiedziała i abominacja go pożarła.
        * Mortal Love - "Sanity"
            * "This is the sin | For I have been loved | This is the sin | (It's my betrayal)"
            * Młoda Verlenka () (14 lat) chce odzyskać brata i NIE WIERZY że nic się nie da zrobić - wzywa pomoc 
    * Theme & Message: 
        * "Dlaczego jest więcej tienek wśród Verlenów niż tienów?"
            * _Oni próbują i umierają; często nie dają sobie rady. One mają nieco większą pomoc lub nieco mniejsze ego_
        * "Thin blue line"
            * _Jesteś po TEJ stronie niebieskiej linii; rodzina zwiera szeregi by nikt niczego się nie dowiedział_
        * "Tienowie MIMO WSZYSTKO stają po jednej stronie"
            * _Lepszy najgorszy Blakenbauer niż prawda o niekompetentnym Verlenie którego pożarła abominacja_
            * ŚLAD: bardzo agresywna Verlenka nie szuka z całych sił Blakenbauera, który za tym stoi.
        * "Siostra nie odrzuci swojego brata; kochające rodzeństwo trzyma się razem"
            * _On był najlepszy! On nauczył mnie wszystkiego! Dał mi WSZYSTKO! Nie możemy go zostawić, nie możemy po prostu go zostawić! ZNISZCZMY tego Blakenbauera!_
    * Jakie emocje?
        * FAZA 1: "Verlen zginął? BLAKENBAUER zabił Verlena?!"
        * FAZA 2: "Co tu się dzieje, coś jest nie tak"
        * FAZA 3: "Co robimy z dziewczyną, co robimy z potworem?"
    * Lokalizacja: Verlenland, Hold Adelitus
* Po co ta sesja?
    * Jak wzbogaca świat
        * Pokaże tragedię Verlenów. Czemu nie są tak dobrzy. Co się dzieje z jednostkami.
        * Pokaże współpracę rodów.
    * Czemu jest ciekawa?
        * l1: Śledztwo: jak zginął Verlen?
        * l2: Jak działają rody tienów
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * the-facade: Adelicja Verlen; pragnie dać "prawidłową historię" Wężomyszowi. Niech umrze w godności i chwale, skoro już nie żyje i nie jest możliwy do uratowania. Mniej ważne jaki był - ważna legenda.
    * the-aspirant: Piszczałka Verlen; pragnie pokazać matce, że ona się myli. Pragnie pokazać, że Verlen może wygrać stojąc jak Wężomysz. She challenges the current order. I pragnie brata z powrotem.
    * the-soother: Pomorzec Blakenbauer; pragnie ukoić cierpienie i niestabilność Wężomysza Verlena oraz pomóc Adelicji zachować pozory. Cel - minimalizacja cierpienia i zbudowanie tego co się da.
    * body-horror: Wężomysz Verlen się zintegrował z Abominacją z własnej woli i z uwagi na problemy z depresją i bycia nie dość dobrym.
    * hartowanie-verlenow: jeśli Verlenowie nie są dość zahartowani, sytuacja się kończy tak jak w wypadku śmierci Wężomysza. Hartuj się albo giń.
    * koniecznosc-pelnej-dyskrecji: Adelicja robi wszystko co może, by nikt nie dowiedział się jaka była prawda o śmierci Wężomysza. NIKT. NIGDY.
    * zrzucenie-winy-na-innych: Adelicja zrzuca winę na ogólnie rozumianych Blakenbauerów (za zgodą Pomorca i ogólnie Blakenbauerów)
* O co grają Gracze?
    * Sukces:
        * Gracze dowiadują się co się tu stało
        * Gracze podejmują JAKĄŚ decyzję
    * Porażka: 
        * Gracze nawet nie dowiedzą się co tu się działo
* O co gra MG?
    * Highlevel
        * Nie mają wpływu na Pomorca ani na Adelicję
        * Nie mają dowodów ani możliwości działania
        * Są niemymi świadkami
    * Co chcę uzyskać fabularnie
        * Pomorzec transformuje Abominację
        * Piszczałka jest chora i śpi
        * Adelicja niszczy Abominację w efektownej, ostrej walce
* Default Future
    * To co Default Future
* Dilemma: brak
* Crisis source: 
    * HEART: "Kultura Verlenów"
    * VISIBLE: "Piszczałka daje znać Ariannie (bo Elena)"
        
### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

.

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

* Łańcuch Adelicji i agentów Adelicji:
    * Legenda Wężomysza
        * uratował swoich ludzi (poszedł sam)
            * <- ratował małą grupkę z oddziałem, kazał się wycofać (świadkowie, Piszczałka) (3 + 3 - Agenci)
        * przed morderczym potworem (Abominacja była dość słaba)
            * <- dowody siły Abominacji (3)
            * <- plotki o mroku Abominacji (5 - Agenci)
        * odważnie wyruszył do działania, zdeterminaowany (depresja) (nie był dobry w walce)
            * <- umiał dobrze walczyć (5)
            * <- był zdeterminowany i odważny (3)
    * Zły Blakenbauer tu był
        * ukryć jego tożsamość (2)
        * pomóc w ewakuacji
            * zrobić drogę (3)
            * zabezpieczyć drogę (3 - Agenci)
        * pokazać jaki jest groźny (3 - Agenci)
    * Sentisieć Pamięta (5)
    * Sprzęt dla Blakenbauera
        * dywersja (2 - Agenci)
        * pozyskać (2 - Agenci)
        * schować (3 - Agenci)
    * Atak na Piszczałkę (2)
* Łańcuch Blakenbauera: 
    * Potężny Potwór
        * senti-amplifikacja (2)
        * wzmocnienie zasobami (3)
        * rytuał (5)
        * aktywacja (2)
    * Ewakuacja
        * przedostać się (3)
    * Spawn Antypiszczałka
        * bats (3)
        * guide (1)
    * Ukojony Wężomysz (3)

## Sesja - analiza
### Mapa Frakcji

.

### Potwory i blokady

.

### Fiszki

.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Arianna, W NOCY, śpiąca, dostała wiadomość na sentisieci z high priority. Od kilkunastoletniej Verlenki. 15?

* Piszczałka: "Tien Arianna Verlen, światło trzeciej... czwartej gwiazdy imperium?" /wahanie
* Arianna: "Słucham? Księżyc w nowiu."
* Piszczałka: "Przepraszam. Więc, tien Arianno Verlen, księżycu w nowiu, oficjalnie proszę Cię o pomoc..."
* Arianna: "O tej porze?"
* Piszczałka: "(zmieszana) Księżycu w Nowiu..."
* Arianna: O co chodzi?
* Piszczałka: "(przestraszona) Blakenbauer zabił mojego brata."
* Arianna: "Jeszcze raz od początku"
* Piszczałka: "Blakenbauer zabił mojego brata. A mama, wielka wojowniczka nic nie robi. Królowa Pustego Legionu. Mama zablokowała mi nawet sentisieć. Ale Ty - Księżycu - byłaś dostępna!"
* Arianna: (pamiętasz tak troszkę jak Marcinozaur się przechwalał że się da z Arianną skontaktować; ktoś się z kimś założył a Marcinozaur nie był zbyt trzeźwy)
 
Piszczałka przyznała się do chronologii:

1. Wieczorem COŚ się stało. Coś strasznie ją bolało. I jej brat zniknął z sentisieci.
2. Piszczałka doszła do tego że na terenie musi być Blakenbauer. Wysłała serię hatemaili do Blakenbauerów.
3. Adelicja ją odcięła i zamknęła.
4. To miało miejsce 10h temu. Piszczałka szukała Arianny.
5. Piszczałka ZDĄŻYŁA wcześniej pozakładać alerty na sentisieć. I alert potwierdził, że na terenie jest Blakenbauer.
    * Alert mówi, że Blakenbauer pojawił się 2h temu a nie 10h temu

Co działo się wcześniej? Zdaniem Piszczałki wszystko było normalnie. Piszczałka była STRASZNIE zazdrosna o brata (i jego dziewczynę)

* Arianna: "Gdy księżyc jest w odpowiedniej fazie moje kanały komunikacyjne są otwarte na osoby w potrzebie"

Arianna i Viorika TEŻ nie czują Wężomysza na sentisieci. OFC - mogła Adelicja go odciąć. Np. Piszczałkę odcięła. Ale to dziwne. Tak czy inaczej, Arianna i Viorika jadą tam szybko, używając pociągu verlenowego. Jest tam dobry szybki pociąg, nie będzie dużo opóźnień.

Gdy się zbliżają do Hold Adelitus, Viorika przygotowuje zapytanie do sentisieci - ostatnia lokalizacja itp. Okazuje się, że Adelicja BARDZO mocno poblokowała dane powiązane z Wężomyszem. Piszczałka poda Ariannie hasła i styl działania Adelicji PLUS wyciąga info o operacjach specjalnych itp. A Viorika robi query.

Ex -> Tr +3:

* V: 
    * Chronologia o dziwo zgadza się z tym co mówi Piszczałka; ona dobrze wyczaiła:
        * Wężomysz znika ~11h temu, Piszczałka panikuje, Adelicja odcina WTEDY informacje o synu (emergency blockades)
        * Piszczałka robi hatemaile i WTEDY Adelicja odcina ją do końca
    * Wężomysz zniknął poza Holdem Adelitus. Był niedaleko, w tzw. Dolinie Węży. (tam są potwory)
    * To do tej pory jest ostatnie miejsce gdzie sentisieć zarejestrowała Wężomysza
* (+1Vg) V: Viorika szuka ANOMALNYCH uczuć - co się tam stało?
    * Wężomysz poszedł walczyć z potworem SAM. Nie wziął oddziału. Złamał wszystkie reguły. I nie miał POWODU tam iść.
    * Wężomysz miał taki stan emocjonalny, że sentisieć defensywnie została wyłączona. Nawet Adelicja nie została poinformowana przed czasem.
    * Wężomysz miał głęboki stan rozpaczy i rezygnacji.
    * W TEJ CHWILI w okolicy Doliny Węży sentisieć daje _minimalny sygnał Wężomysza_. Oraz wykrywa w tamtej okolicy Blakenbauera.

Arianna przepytuje Piszczałkę o w sumie stan psychiczny Wężomysza. Jak się czuje, jak jest, w jakim był, jak się zachowywał... więc Arianna jest w stanie filtrować niekompetentną Piszczałkę. Piszczałka wychwyci linię związku PLUS ona jest z bratem CAŁY CZAS prawie. To taka "ulubiona młodsza siorka". Są bardzo blisko. Ona jest wyczulona. On pomagał w jej wychowaniu. Adelicja mniej.

I Arianna dostała linka na dziewczynę Wężomysza. Ta się nazywa Katia Dzwoneczek - nie jest magiem. Katia zajmowała się obsługą broni (rusznikarz, zbrojeniowa itp - inżynier). Co ciekawe - kontakt do niej jest zablokowany.

Tr +3:

* Vr: 
    * Piszczałka mówi że nic się nie zmieniło.
    * Ona MYŚLI że wszystko jest OK, ale opisuje delikwenta, który ZAWSZE jest zdeptany i zrezygnowany. 
    * On ekranował Piszczałkę przed Adelicją jak potrafił.
    * Adelicja żądała bardzo wiele i od Piszczałki i od Wężomysza. Ale Wężomysz był łagodniejszy od Piszczałki.
    * Wężomysz najbardziej lubił opiekować się siostrą

Arianna i Viorika dotarły na miejsce, do Holdu Adelitus. Wyszły z pociągu z walizkami i sprzętem do polowania na potwory. Co ciekawe - nikt niczego szczególnego od A+V nie chce. Adelicja musi być zajęta. Piszczałka cicho na sentisieci dopinguje Ariannie by ta ją uwolniła.

Viorika szuka gdzie jest ADELICJA: 

* X: po rozmowie z Arianną Piszczałka jest przekonana, że ONA musi ratować brata. To jest jej odpowiedzialność. MUSI go ratować bo on jej pomagał.
* X: Adelicja jest zbyt zajęta by monitorować Viorikę i Ariannę, bo jest na finiszu jednej ze swoich operacji. (SENTISIEĆ-WIDZI)

Wypożyczenie skutera... było trudniejsze niż zwykle. Arianna musiała znaleźć dowód osobisty. Ale się zgodził - w procedurach nie ma czegoś takiego. Arianna i Viorika wsiadają na ścigacz i lecą na Dolinę Węży. Zasięg - 20 minut.

Po 5 minutach:

* Jonasz: "Tu centrala, Jonasz Plerk z tej strony, zawracajcie."
* Viorika: "Procedura SPIERDALAJ, nie masz prawa rozkazywać Verlenom :-)"
* Jonasz: "Tien Verlen, mówię z mandatu Królowej Pustego Legionu."
* Viorika: "A ja mówię z mandatu księżniczki Verlen."
* Jonasz: "...(zatkało go)... ale tien Verlen, NIE MOŻECiE tam lecieć. Nie zmuszajcie mnie do zdalnego wyłączenia Waszego ścigacza." 
* Viorika: "Jesteśmy na tropie KONKRETNEGO potwora, nie ucieknie nam i jak trzeba to zrobimy żywy skuter. Przeszkódź nam i to NADAL zrobimy."

Tr +4:

* X: Sprawa BĘDZIE eskalowana do Adelicji wraz ze skargą o niegrzeczności tych dwóch Verlenek
* V: Jonasz... nie wie co robić. Serio. To powoduje, że daje Wam spokój - nie będzie overridował.

.

* Jonasz: Tien Verlen, jedziecie do niebezpiecznego obszaru
* Viorika: Wiemy. Reklamujesz czy zniechęcasz?
* Jonasz: Tien Verlen, tam się dzieje coś złego. Tam jest mroczny Blakenbauer.
* Viorika: Sugerujesz że nie poradzimy sobie z jednym?
* Jonasz: Tien Adelicja się tym zajmuje...
* Arianna: Nie wejdziemy jej w drogę.
* Jonasz: (cicho, do kogoś innego) przez te siksy stracę robotę (do Verlenek) bądźcie bardzo ostrożne. Bardzo. Tam jest bardzo, BARDZO groźny Blakenbauer.

Ścigacz zbliża się do Doliny Węży. To faktycznie dolina jest. Na dnie doliny są jakieś wyjścia, przejścia itp. Teren jest bardzo zalesiony, mało widać. (JESTEŚMY AT 4) Viorika ustawia alerty na potwory i krótkozasięgowe na sentisieć. 

Tr Z +3:

* V: są poustawiane alerty na ruch, na RZECZY itp.

Gdy się zbliżyliście do Doliny Węży odpaliły wszystkie alerty Vioriki. Dużo ruchu, metaliczne, strzelają do Was. Klasyczne działka PLot celują i strzelają. Arianna NIE JEST zaskoczona, więc Ex. Obcy ścigacz ALE gatlingi w lesie. Co gorsza, alerty sentisieci pokazują że to nie jest wszystko co tu jest.

Viorika przygotowuje SZYBKIE zaklęcie by ścigacz miał ZERO ciepła i ZERO namierzania ze strony gatlinga.

* STEP 1: ustawienie stanu - TrZ+2
    * X: źródło energii zaklęcia - ścigacz; ścigacz będzie odbiciem w Eterze. Viorika nie kontroluje zaklęcia w pełni.
    * V: Viorika prawidłowo ustawiła zaklęcie; całkowite zamaskowanie przed ciepłem i detektorami tego typu. 
    * V: "błędne przekierowanie energii" - Paradoks pójdzie TAM lub TAM.
* STEP 2: dodajemy magię.  (+3Vb, +3Ob, +1Xb)
    * V: INTENCJA POTWIERDZONA - nienamierzalne na ciepło (ścigacz nie ma kłopotu)
        * Z perspektywy zaklęcia - dodaliśmy smokescreen (ziemia, materia w powietrzu, same pociski it)

Arianna manewruje by bezpiecznie wyprowadzić ścigacz; spektakularne manewry: Ex+3 -> Tr Z+3:

* V: (tak dobrze manewry i nikt nie patrzy); ścigacz się ODERWAŁ. Jesteś bezpieczna. 

Skuter ląduje poza dolinką by działka nie miały dobrego namiaru. Sukces. Arianna i Viorika są gotowe do dalszych działań. Sentisieć DALEJ informuje o ruchach w dolince i że tam jest więcej bytów. Nie wychodzą poza dolinkę, rozstawiają ochronę. Ale jeden byt się przesuwa poza - nie w Waszą stronę, w stronę snajperską.

Arianna proponuje osunięcie się zbocza dolinki by zniszczyć gatlingi.

Viorika vs Adelicja o kontrolę nad terenem w miejscu zabezpieczonym. Arianna dodatkowo bombarduje teren ładunkami wybuchowymi by spulchnić i zmiękkczyć glebę.

Ex Z +3:

* X: Adelicja wie co tu się dzieje; musi ingerować.
* Vr: Lawina. Gatlingi zostają zniszczone.

Wiadomość na sentisieci. Adelicja do A+V.

* Adelicja:  Tien Arianno Verlen. Wkraczasz na mój teren wraz z tien Vioriką Verlen. NATYCHMIAST stawcie się przed moim obliczem w mieście.
* Arianna: Polowałyśmy na tego potwora od dłuższego czasu, kontynuujemy polowanie, nie mamy złej woli.
* Adelicja: Nie powiedziałam, że nie możecie polować. Powiedziałam, że żądam byście się stawiły przed moje oblicze TERAZ.
* Arianna: Stawimy się jak skończymy polowanie. Potwór nam ucieknie jak zrezygnujemy.
* Adelicja: Nie ucieknie, bo ustawię sentisieć by go zatrzymać.
* Arianna: Ścigacz jest uszkodzony, wrócimy ale zajmie to czas.
* Adelicja: Oddalcie się z tego terenu jak tylko jesteście w stanie bo tam działa tien Blakenbauer. I mam zamiar go zbombardować. I nie będę czekać. Napalm wszystko wyleczy.
* Arianna: Nie widziałam żadnego ostrzału. Nie jechałybyśmy w ostrzeliwany teren.
* Adelicja: Spodziewam się, moja droga. Ale nie chciałam ostrzegać Blakenbauera że cokolwiek wiem. To miało być jedno czyste uderzenie. Co mi się nie uda, bo weszłyście w paradę.
* Viorika: Do pokonania Blakenbauera napalm niekoniecznie jest najlepszym rozwiązaniem
* Adelicja:  Napalm pozwoli mi zapewnić że Wy nie będziecie szły do Blakenbauera. To nie jest przeciwnik dla Was.

Arianna ORAZ Viorika próbują zagadać Adelicję - zagadać, rozwiązać serię problemów Blakenbauera a SAME schodzą w dół doliny. Jak poleci napalm, chcą być zamknięte w tunelu z Blakenbauerem. Arianna pisze do Piszczałki - są w zasięgu Blakenbauera i potrzebują dywersji. Piszczałka odpala "Projekt Armageddon" (seria wybuchów w mieście). To odwraca uwagę Adelicji...

Tr+4:

* X: niezależnie od wyniku, Adelicja wysyła Pusty Legion w tą stronę. Chce Was pojmać. "Odprowadzić pod strażą by nic się stało".
* V: Adelicja jest zdekoncentrowana; Viorika może użyć sentisieci.
* V: A+V są niewidoczne pod sentisiecią dla Adelicji; fantomy pokazują, że się oddalają od dolinki. Incydentalnie to znaczy że poleci napalm.
* V: Adelicja jest OPÓŹNIONA. A+V mają trochę więcej czasu niż napalm pójdzie. Ona naprawdę nie chce trafić A+V.

Arianna ma ścigacz - wysyła go, a sama się wysuwa ze snajperką i strzela do snajpera (cybersynt).

Tp +3:

* V: Snajper zlokalizowany i zlikwidowany. To był jakiś cyborg.

Viorika ostrzeliwuje wszystkie cele, by nie mogły strzelać do Arianny. Bierze na siebie ogień i ogień zaporowy.

TrZ +3:

* V: Arianna ma zdjęty z siebie ogień
* V: Udało się ogniem zaporowym ściągnąć ogień i nie zostać rannym.

Arianna rzuca zaklęcie - dłuższe - by unieruchomić przeciwników korzeniami. I jest traktowana jak dziecko więc jest sfrustrowana.

TrZ +3:

* V: Arianna się wyciszyła i ustawiła poziom emocjonalny.

+MAGIA

* Vz: przeciwnicy są odpowiednio unieruchomieni. Nie są w stanie wiele zrobić. 

A+V biegną do tunelu przed napalmem. SUKCES. Wejdą do tunelu z Blakenbauerem i cieniem Wężomysza.

===

!! 15 iteracji zanim pojawi się Adelicja !!

* Miny + Toperze z eksplozjami w wąskich korytarzach, unknown corridors
* Blakenbauer: atak serią pocisków (tentacle fight), IMPALE wężoostrzem, tele-blink, Toperze assault, TOXIC GAS
* Wężomysz Verlen: szarża skolopendry zagłady, EMBRACE EMBRACE REFORM, SURROUND STRIKE

===

Tunel. Napalm z góry. Pusty Legion zmierza w tą stronę.

CEL 1: spowolnić Adelicję by Arianna mogła czarować na szybko.

* Adelicja: "Viorika, Arianna, gdzie jesteście?" (jeszcze nie ma VISUALS)
* Viorika: "(przybliżona okolica) - znalazłyśmy ślady potwora, lecimy za nim"
* Adelicja: "Tien Vioriko Verlen. Jesteście na moim terenie. Moim. ŻĄDAM waszego stawiennictwa się."
* Viorika: "Jesteśmy odcięte przez napalm! Próbujemy, ale musimy się wydostać (z lekkim zakłopotaniem), szłyśmy za potworem."

Ex -> Tr (Adelicja UWAŻA Was za młode siksy) +P (zdaniem Adelicji to jest możliwe) +4 (wycieknięcie emocji i pokazanie jak sytuacja wygląda, niedoświadczone, opis bitwy)

* V: Adelicja łyknęła opowiastkę
    * Adelicja: "(z niedowierzaniem) Jesteście niemożliwe... wchodzicie na mój teren i wykazujecie się SKRAJNĄ niekompetencją."
    * Viorika: "... (nie mówi nic)"
    * Adelicja: "wezmę Waszą prawidłową pozycję i wyślę Wam RESCUE PARTY... (całkowicie oburzona i pełna niedowierzania - CO ZA NIEKOMPETENCJA)"
    * Viorika: "żadnego ratunku! Poradzimy sobie same!!!"
    * Adelicja: "nie zasługujecie na tą szansę. Siedźcie tam (nie ukrywa niezadowolenia)"
    * Arianna: (przejmując rozmowę) "Ale jesteśmy blisko! Widzimy przejście! Przejdziemy same, nie potrzebujemy pomocy!"
    * Adelicja: "Księżniczko Arianno Verlen. Wątpię czy dacie rady przebić się same widząc poziom niekompetencji który wykazałyście. Nic nie róbcie - to rozkaz tiena."
    * Arianna: "Dobrze! Idziemy! (udaje opowiadając że skręcacie w najgłupszy kierunek możliwy bo tam są potwory, miny itp - SAMA je podłożyła)"
    * Adelicja: "Nie idźcie tam, tam są miny i zasadzki! To... idźcie inną drogą (koordynaty)"
    * Arianna: "Viorika określi gdzie są miny i wprowadzimy potwora w większy klaster min (nawija)"
    * Adelicja: "NIE! (lekka panika) Viorika NIC nie zrobi, nie jesteś... w stanie tego zrobić! Siedź tam!"
* (+1Vg) Vz: Arianna kupiła Viorice dość czasu

Viorika chce zmylić Adelicję. Chce zamieszać ślady by Adelicja była przekonana że jest tam gdzie szuka ale by nie umiała znaleźć. Koncept - nowe odczyty na stare odczyty. Gdzie wychodzą z pola minowego ale wracają "spacerkiem". Dać jej odczyty i spowolnić by wiedziała dość, ale nie precyzyjnie, 'plausible'.

BAZA: ekstremalna (Verlen nie może zginąć!) -> Tr (nie macie dość sprytu) -> +P (macie przeszłe sygnały) -> -P (zbyt niekompetentne by mieć autonomii) -> +P (ona jest BARDZO zajęta wszystkim innym)

Viorika, Tr Z +3 (+P ekspertka sentisieci, -P jej teren jej kontrola jej oddziały, +P stan emocjonalny Adelicji) -> Tp +3:

* V: Adelicja jest zdekoncentrowana i faktycznie idzie za tym planem; udało się kupić TROCHĘ czasu, do momentu aż Legion by dotarł.
* V: Viorika i Arianna ZNIKNĄ z sentisieci i Adelicja nie uzna tego za schowanie się. Uzna, że weszły na minę. I ma panikę.
* Vr: Po wszystkim, Adelicja dostanie SZACUNEK wobec Arianny i Vioriki nieważne czy je lubi czy nienawidzi. To są prawidłowe taktycznie Verlenki.

Jesteście w tunelach i MNIEJ WIĘCEJ wiecie, gdzie (w którą stronę) jest sygnatura Wężomysza - a raczej to co z niej zostało. I jest niedaleko ten Blakenbauer.

Pytanie - zakradamy się w Lancerach czy idziemy jawnie? Tu wchodzi drona Vioriki - wysyłamy dronę. KTÓRY korytarz prowadzi do Blakenbauera? I czego się spodziewać? (3 cele: mniej więcej kierunek, mniej więcej zagrożenia, niewykrycie)

* baza ekstremalna: niedopasowany sprzęt (podziemia), działanie na terenie z Blakenbauerem który się ufortyfikował, "wrogi teren"
    * +P : przygotowanie oznacza, że drona jest zdolna do dobrego działania pod ziemią (Verlenland) ale jest tu kompetentny Blakenbauer
    * +P : nic nie miało prawa tu wejść, nic nie ma prawa tu zadziałać
    * nie mamy przewagi za metodyczny czas; idziemy szybko

Tr +3:

* X: Viorice zajęła synchronizacja z sentisiecią delikatnie i jej przekonywanie by siedziała cicho ZA DUŻO czasu. Ta sentisieć jest BARDZO pobudzona...
* V: sentisieć odpowiada na żądanie Vioriki i bardzo pomaga jej w skanowaniu, pomocy dronie, znajdowaniu kłopotów itp. (+V +3Vp za sentisieć)
    * Adelicja bardzo skomplikowane zapytania, instrukcje itp. na tej sentisieci.
* V: z pomocą sentisieci dronie udało się zamapować drogę. Blakenbauer COŚ robi magicznego z... amalgamatem, który ma część ludzkich cech Wężomysza. To skolopendra; insektoidalna hybryda potwora i Verlena.
    * Blakenbauer jest zdeformowany; homo superior, otoczony przez pole gazu; on pracuje nad amalgamatem skolopendrowym Wężomysza
* V: drona pozostała niewykryta, MIMO dużej ilości toperzy. Mamy toperze, drona dała je ominąć - tylko dzięki sentisieci. One są aktywne. One nasłuchują.
* Vz: dzięki sentisieci udało się wykryć MINY. OPRÓCZ TEGO cały teren jest zaminowany.

NIE MA SZANS że Blakenbauer zrobił to bez Adelicji. Viorika czuje też lekkie senti-powiązanie skolopendry z sentisiecią. To jest to co zostało z brata. COŚ zostało. Niekoniecznie cały, ale COŚ tam jest.

...nowa potencjalna teoria A+V - Adelicja poświęciła kiepskiego syna by mieć świetną skolopendrę. Ale ona nie jest przecież aż tak zła...

Viorika próbuje dojść do tego co się dzieje - WIDZIAŁYŚCIE i potwory, i ich rozstawienie, i reakcję Adelicji i siły Blakenbauera i siły.

Tr Z +3:

* Vr: to jest NAPRAWDĘ ciekawe
    * siły które broniły doliny wyglądały jak "przejęte siły Adelicji" - bardziej jak Legion poza jej kontrolą, ALE tu w jaskiniach są rzeczy które bardziej pasowałyby TAM a mniej tu
    * miny, toperze itp. są rozstawione PERFEKCYJNIE przeciwko Pustemu Legionowi lub Verlenom. To jest naprawdę dobrze zrobione taktycznie wiedząc jak działają Verlenowie
    * Adelicja ma POWÓD czemu nie rozwiązała problemu na swoim terenie
    * napalm nie miał ŻADNEGO sensu. 0/10. Był przeciw Verlenkom a nie Blakenbauerowi. Ale pytanie - czy by ratować je przed Blakenbauerem czy Blakenbauera przed nimi?

A+V POWINNY mieć kontakt do Piszczałki, ale nie mają. Piszczałka jest nieaktywna w sentisieci, ale żywa. (śpi, nieprzytomna...). Została "wyłączona".

Nasze bohaterki - nie do końca wiedzą KTO za co odpowiada, nie do końca wiedzą jaki ruch zrobić ale muszą się spieszyć.

=== TORY ===

* Łańcuch Adelicji i agentów Adelicji:
    * Legenda Wężomysza
        * uratował swoich ludzi (poszedł sam)           **!!DONE!!**
            * <- ratował małą grupkę z oddziałem, kazał się wycofać (świadkowie, Piszczałka) (3 + 3 - Agenci)
        * przed morderczym potworem (Abominacja była dość słaba)
            * <- dowody siły Abominacji (3)
            * <- plotki o mroku Abominacji (5 - Agenci)
        * odważnie wyruszył do działania, zdeterminaowany (depresja) (nie był dobry w walce)
            * <- umiał dobrze walczyć (5)
            * <- był zdeterminowany i odważny (3)
    * Zły Blakenbauer tu był
        * ukryć jego tożsamość (2)
        * pomóc w ewakuacji
            * zrobić drogę (3)
            * zabezpieczyć drogę (3 - Agenci)
        * pokazać jaki jest groźny (3 - Agenci)
    * Sentisieć Pamięta (5)
    * Sprzęt dla Blakenbauera
        * dywersja (2 - Agenci)
        * pozyskać (2 - Agenci)
        * schować (3 - Agenci)
    * Atak na Piszczałkę (2)
* Łańcuch Blakenbauera: 
    * Potężny Potwór
        * senti-amplifikacja (2)
        * wzmocnienie zasobami (3)
        * rytuał (5)
        * aktywacja (2)
    * Ewakuacja
        * przedostać się (3)
    * Spawn Antypiszczałka                              **!!DONE!!**
        * bats (3)
        * guide (1)
    * Ukojony Wężomysz (3)                              **!!DONE!!**

=== /TORY ===

Arianna ma pomysł - rzucić potwora Verlenlandu na Blakenbauera. JAK to jednak zrobić? Macie przewagę dyskrecji itp. I to dużo lekkich potworów - bo tam są miny. Arianna i Viorika sprawdzały jakie potwory są tu wcześniej, trzeba znaleźć odpowiednie lekkie. Więc - czas znaleźć i przekierować potwory.

Mimo, że Viorika jest w tym lepsza, ale przez czas Arianna się tym zajmuje. Viorika próbuje nawiązać kontakt z Lucjuszem.

Ex -> Tr (Adelicja jest rozkojarzona), +P (research na potworach), -P (chcemy by to było szybkie) +4

* KONTEKST: (_+2 jednostki czasu, ale potem Blakenbauer będzie zajęty_); chcemy je ZNALEŹĆ a potem odgłosami z sentisieci SKIEROWAĆ na Blakenbauera
* V: Mamy potwory. Mamy gniazdo mrówkoidy z szczypcami, dość szybkie i ATV. Mamy kilka takich gniazd - one się karmią energią z sentisieci "leylines". Mrówkoidy uprawiają grzyby.
* Arianna przegłusza węzeł i feromonami z sentisieci kieruje je na Blakenbauera (+1Vg): V: Mrówkoidy zmierzają w tamtą stronę.
* Vr: BLAKENBAUER overwhelmed. Mrówkoidy ze wszystkich stron. Blakenbauer jest zmuszony do przekierowania defensyw w mrówkoidy, przez co nie monitoruje terenu. I czyścimy miny. KTÓRE ROBIĄ EKSPLOZJE.
    * Tunele się sukcesywnie zawalają przez miny
    * Wężomysz się "uruchomił", rozgląda się, szuka; Blakenbauer go uspokaja ręką.
    * Płaszcz Blakenbauera się wygina w niewłaściwy sposób; bardziej humanoid niż człowiek.

Blakenbauer jest skupiony na walce. On steruje wszystkim co tu jest (z niewiadomą dotyczącą Wężomysza). Jest zajęty. Nie skupia się na niczym innym. Jest za dużo sygnałów - jest przeładowany.

Viorika szybko strzela do Lucjusza - czy on coś wie na ten temat.

* Viorika: (z grubsza co widzi, jest taki, czy COŚ kojarzy, COKOLWIEK)
* Lucjusz: Kochanie, nie do końca widzę... nie znam tej sprawy (z lekkim wahaniem)
* Viorika: Jak to chory Blakenbauer to co, mam go spróbować zdjąć?
* Lucjusz: Blakenbauer rzadko bywa... _chory_. Są... (myśli) powiedzmy, że warto potraktować go jak maga. Acz nie _powinien_ być w Verlenlandzie... (z lekkim wahaniem)
* Viorika: Nie brakuje Wam jakiegoś kuzyna?
* Lucjusz: Proponowałbym zostawić ten temat.
* Viorika: Nie mogę.
* Lucjusz: Potraktuj go jak maga. To wewnętrzna sprawa Verlenlandu, jeśli mam rację.
* Viorika: Czyli jak będzie miał wypadek to nikt nie będzie płakał?
* Lucjusz: (lekka załamka) Jeśli to Ty... może być to uznane za zemstę za Elenę... o to Ci chodzi?
* Viorika: Nieproszony gość. Jeśli zdecydujesz się coś powiedzieć, to powiedz.
* Lucjusz: Miłego polowania. Uważam że robisz błąd, ale to Twój błąd do zrobienia.

.

* Arianna: Jaka była odpowiedź na pytanie "rozmawiać czy zabijać"?
* Viorika: Że robię błąd...
* Arianna: Że robisz błąd uczestnicząc w akcjach Verlenów?
* Viorika: ...

Lucjusz sugeruje, żeby Verlenki się wycofały. Ale Viorika ma fajny pomysł - młody TAK BARDZO chciał się pozbyć matki że zrobił _forced transfusion_ i poinformował Blakenbauera. Blakenbauer przybył tu go RATOWAĆ a Adelicja straciła sytuację spod kontroli. 

FAKT: Wzór Wężomysza jest zniszczony. Ma ECHO Verlena, ale nie jest już Verlenem. Jest... echo. Resztka. Nie wiemy nawet w jakim stopniu Wężomysz jest jeszcze osobą. Malformacja / abominacja.

Priorytet, korzystając z okazji - dostać się do Blakenbauera nieinwazyjnie, by WężomyszMalformation nie zainterweniował. Czarodziejki wysyłają dronę. Drona jest signowana przez Viorikę - Blakenbauerowie postrzegają ją jako "no-fun person z którą da się dogadać, ale to jest jak ból zębów".

Drona dyskretnie przelatuje do Blakenbauera. I się ujawnia.

* Viorika: Tien Blakenbauer, Viorika Verlen. Chcę porozmawiać.
* B: (blink blink): Przybyłaś do mojej mrocznej jaskini. Nie powinnaś była tu przychodzić. Mądrze, że przyprowadziłaś dronę. Ale czas na Twoją zgubę, tien Vioriko Verlen.
* Viorika: Możesz odpuścić dramę i pogadać jak mag?
* B: (blink blink): Co tu jest do rozmawiania? Ty zostaniesz zniszczona potęgą mojego potwora. Ten Verlen... 
* Viorika: Prawdopodobnie był głupi, ale to szczegół.
* B: (chwila ciszy): Jego los będzie niczym w porównaniu z Twoim!
* Viorika: Bla bla bla. Jesteś na terenie Verlenów. Podejrzewam, że jedyne co Cię chroni to Adelicja. Podejrzewam, że wiem czemu.
* B: Nie, moja droga! ZNISZCZĘ Adelicję! Zniszczę Ciebie!
* Viorika: Uważaj, żeby Cię mój potwór w tyłek nie ugryzł. Słuchaj - chcesz mu pomóc czy chcesz mu zaszkodzić?
* B: (zamilkł, przez moment nic nie mówi) Ten młody Verlen dzielnie walczył. Myślisz, że skończysz inaczej, jeśli się ze mną zmierzysz?
* Viorika: Pytam, czy walczył.
* B: (znowu cicho) Nie porozmawiamy, tien Verlen. Nie rozmawiam z robotami. (ogromna macka z pancernym kolcem spod płaszcza atakuje dronę, rzuca się w stronę drony, SZYBCIEJ niż powinien). Następna będziesz Ty! Zapraszam do mrocznej jaskini!!!

Co ciekawe, Blakenbauer ZASŁONIŁ Wężomysza przed droną, jakby się spodziewał ataku kamikaze.

Ex -> Tr (spray glue grenade) +3:

* V: Drona bezpiecznie się ewakuowała; Blakenbauer wrócił do jaskini, do Wężomysza. Wystrzelił w tunel gdzie uciekła drona rakietę. Zniszczył tunel, ale drona uciekła

Zaskakujące jest to, że Blakenbauer próbował udawać ZŁEGO BLAKENBAUERA???

(Blakenbauer -> Adelicja, sygnał)

PRIORITY OVERRIDE sentiseci w stronę Vioriki, command signal. Adelicja. DUŻO z siebie daje. Viorika nie próbuje się chować; właśnie rozmawiały z Blakenbauerem. Viorika JAWNIE staje naprzeciw Adelicji - niech Adelicja myśli że wymusiła ale Viorika zachowuje część kontroli, np. fałszywą lokalizację siebie i Arianny.

Ex Z (nie spodziewa się subtelności) +3:

* X: ta walka TRWA. Musi trwać, by Adelicję zmylić. Trwa dużo dłużej niż Viorika by chciała, ale Adelicja i Viorika są w starciu sentisieci.
* Vz: Adelicja jest przekonana że wygrała, ale Viorika ma niektóre okoliczności pod kontrolą

TYMCZASEM Arianna. Blakenbauer wraz z Wężomyszem się przesuwają - w głąb Verlenlandu, pod ziemię. Chcą się oddalić. Ale straci sprzęt i okopaną pozycję. Za to - będzie mieć lepszą pozycję (trudniej się dostać). Za to jest WRAŻLIWSZY na sentisieć (gdyby nie Wężomysz).

Arianna przygotowuje się na zaklęcie. Nie chce by wszystko poszło na marne. Chce psa gończego. JESTEŚ MOIM PSEM! Dogoń ich! To polowanie nie skończy się moją porażką! Niech drona odpowie na to żądanie i stanie się perfekcyjnym psem gończym.

* stan emocjonalny: pasuje, zsynchronizowany
* stan sentisieci: to jest bardzo Verleńskie. Sentisieć aprobuje. Otoczenie chce się zaaptować do Twojej woli.
* drona (ośrodek): to pasuje, i jako wygięcie energii (enchant) i dlatego, że ta drona jest "heroiczna" - przetrwała Blakenbauera.

Arianna się skupia - niech otoczenie to sprawi, TO JEST HEROICZNA DRONA KTÓRA ZNAJDZIE BLAKENBAUERA! Przygotować Pryzmat miejscowy.

Tr Z +3:

* X: Drona zostanie "poświęcona", to już nie będzie drona. To będzie "potwór - hunter", WYKLUJE się z drony. Verlenland w okolicy może go spawnować.
* V: Pryzmat jest zapewniony. Heroiczny potwór anty-Blakenbauerowy, dodany do Pryzmatu miejsca.
* (+M) Vm: z "pióropusza Vioriki" drona pękła i zaczęła się reformować. Przekształciła się w czworonoga - "scifi biały plastik", perfekcyjna powłoka i holograficzne maskowanie. Elegancki, nie tylko taktyczny. Raz wywęszy ofiarę, nigdy jej nie zostawi. Uncanny valley jak się pojawi.
    * Arianna WIE gdzie jest Blakenbauer.
    * Blakenbauer NIE JEST W STANIE pozbyć się tego. Jest szybszy i unika. I się nie zbliża. Ale zawsze informuje, że jest.
    * Zawsze na granicy detekcji
    * Wężomysz też nie jest w stanie tego złapać
    * Życiowym celem potwora jest NĘKANIE tego Blakenbauera.

Adelicja atakuje słownie Viorikę.

* Adelicja: Viorika Verlen. Żyjesz.
* Viorika: Jak mi przykro.
* Adelicja: Albo przestaniecie wchodzić mi w paradę albo Was zamknę w sentisieci. Ten Blakenbauer Was zabije.
* Viorika: Jesteś skłonna zabić swojego Verlena by ukryć swoją niekompetencję? Wpuszczenie na teren Blakenbauera, jak sama twierdzisz "niebezpiecznego".
* Adelicja: Nie zamierzam z Tobą rozmawiać na ten temat, nie jesteś na tym poziomie. Siad i służ. (całkowicie straciła kontrolę)
* Viorika: (coś w stylu) Od czasu jak tu jestem, nie kontrolujesz i jesteś coraz bardziej zdesperowana. Zamiast skorzystać z członka rodu i użyć jako zasobu po wprowadzeniu, niszczysz sobie opcje. Nawet największy generał nie kontroluje całego pola bitwy. Nie kontrolujesz 100% terenu ani osób na tym terenie. Może jestem młoda, ale miałam wystarczająco dużo szkolenia i doświadczenia by rozpoznać pewne sygnały. (PRÓBA wyciągnięcia ją z obsesji, wybicia jej z tego). Córkę TEŻ chcesz stracić?

Tr +4 (fundamentalnie, Viorika ma rację a Adelicja jest osobą wysokiej klasy) +3Oy (ENTROPICZNE jako sukces):

* V: Adelicja odzyskała kontrolę nad sobą. Przez MOMENT mignęła cała presja i stres. Nieprawdopodobne wykończenie i stres. Ona nie poświęciła syna. Ale wróciła jej maska.
    * Viorika: Nie chcę Cię stąd wyrzucić. Nie chcę tego miejsca. Ale jestem Verlenką i chcę, by cały Verlenland dobrze funkcjonował.
    * Adelicja: Wężomysz walczył z potworem. Poszedł tam by ratować ludzi. I przegrał. (furious resignation) (SPRÓBUJ tylko podważyć że jej syn był bohaterem)
    * Viorika: ...
    * Adelicja: I teraz znajdziemy i zniszczymy złego Blakenbauera, lub maga rodu Sylver, który zrobił potwora który był pułapką!

Jest NOWY problem. Łowca irytator do końca życia, ciemiężący Blakenbauera.

Arianna dowiedziała się: Blakenbauer nazywa się Pomorzec Blakenbauer...

* Viorika: Co zrobiłaś z córką?
* Adelicja: Blakenbauer ją unieszkodliwił. Na szczęście, mamy dobrych medyków. Niestety, śpi. Ale nie jest zatruta. Nic jej nie będzie.

Czyli Zespół pomoże Adelicji zachować _legacy_ Wężomysza a Pomorzec Blakenbauer dostanie problem - nemesis, który nań poluje XD.

## Streszczenie

Piszczałka dociera na sentisieci do Arianny - podobno jej brat zginął od Blakenbauera. Na miejscu, w terenie kontrolowanym przez Adelicję Verlen, okazuje się że brat (Wężomysz) najpewniej popełnił samobójstwo dając się zjeść potworowi. Adelicja robi wszystko, by Verlenki nie doszły do prawdy i by chronić honor swojego syna. Arianna i Viorika jednak docierają do Blakenbauera w Dolinie Węży i zmuszają go do ucieczki, po czym poznają prawdę. Decydują się pomóc Adelicji w ochronie honoru Wężomysza a przy okazji Arianna nakłada Klątwę Nemesis na Blakenbauera który tylko próbował pomóc XD.

...i dlatego - przez presję - chłopcy Verlenów nie zawsze dożywają 19-tki...

## Progresja

* Pomorzec Blakenbauer: dzięki Ariannie dostaje Nemesis; kiedyś drona, teraz czworonóg - "scifi biały plastik", perfekcyjna powłoka i holograficzne maskowanie. Elegancki, nie tylko taktyczny. Raz wywęszy ofiarę, nigdy jej nie zostawi.

## Zasługi

* Arianna Verlen: odpowiedziała na wezwanie Piszczałki (choć nie musiała), uniknęła blokady ze strony Adelicji, zneutralizowała obronę Doliny Węży i stworzyła magicznego Łowcę-Nemesis dla nieszczęsnego Blakenbauera który próbował pomóc XD
* Viorika Verlen: wykorzystała swoje umiejętności w zakresie sentisieci, aby zebrać kluczowe informacje i zmylić Adelicję, zyskując cenny czas na przeprowadzenie misji w podziemiach, niedaleko Tajemniczego Blakenbauera i ciała Wężomysza; dowiedziała się od Lucjusza, że tam się dzieje coś innego niż się wydaje. No-nonsense negotiator, co całkowicie zaskoczyło Pomorca Blakenbauera (oczekującego efektownych mów). Przekonała Adelicję, że razem lepiej utrzymają pamięć o bohaterstwie Wężomysza i schowają prawdę przed Piszczałką.
* Adelicja Verlen: Królowa Pustego Legionu i matka chłopca, który popełnił samobójstwo. Chroniła terytorium Verlenów i pamięć o swoim synu jako bohaterze, tworząc fałszywą narrację. Bardzo surowa, nie pozwoli, by Piszczałka (córka) poznała prawdę. Pozyskała pomoc Blakenbauera jako kozła ofiarnego, który za to zapłacił przez Ariannę (przypadek). Miała na celu ochronę rodziny i zachowanie honoru rodu.
* Wężomysz Verlen: 17-latek; nie wytrzymał presji ze strony Rodu oraz matki. Dał się zjeść potworowi. Stał się katalizatorem wszystkich problemów, bo Adelicja próbuje to zamaskować a Zespół odkryć. KIA.
* Piszczałka Verlen: 15-latka; zdołała skontaktować się z Arianną i Vioriką, informując je o zniknięciu brata i pojawieniem się Blakenbauera. Bardzo waleczna, anty-Blakenbauerowa i robi dywersję przeciw matce by tylko poznać prawdę i pomóc Ariannie.
* Pomorzec Blakenbauer: Blakenbauer z dobrym sercem; współpracując z Adelicją ufortyfikował Dolinę Węży i przekształcał ciało Wężomysza by zrobić z niego "strasznego potwora", by Wężomysz zginął w honorze. Gdy Viorika doń zagadała, poszedł w 'ha hahaha ha' Dark Blakenbauer, by ratować reputację Wężomysza i Adelicji. Zwiał, ale dostał 'nemesis' jako przyjaciela, dzięki Ariannie.
* Lucjusz Blakenbauer: udzielił Viorice cennych wskazówek i informacji na temat sytuacji w Dolinie Węży; nie mógł powiedzieć WPROST co tam się dzieje, ale oględnie powiedział, że Blakenbauer nie jest problemem i tam coś się dzieje.

## Frakcje

* .

## Fakty Lokalizacji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. Hold Adelitus

## Czas

* Opóźnienie: 15
* Dni: 2

## Inne

