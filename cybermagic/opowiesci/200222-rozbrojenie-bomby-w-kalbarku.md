## Metadane

* title: "Rozbrojenie bomby w Kalbarku"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [200202 - Krucjata Chevaleresse](200202-krucjata-chevaleresse)

### Chronologiczna

* [200202 - Krucjata Chevaleresse](200202-krucjata-chevaleresse)

## Budowa sesji

### Stan aktualny

* Kardamacz pragnie zbudować Homunkulusa - potrzebuje więcej próbek, więcej mocy - i pozbyć się Ataienne.
* Chevaleresse pragnie siły i pokonania mordercy Alana; wpadła pod indoktrynację Kardamacza.
* Ataienne widzi zagrożenie. Chroni Liberatis swoimi siłami i przygotowuje się do odbicia Chevaleresse.
* Część sił Grzymościa stanęła po stronie Kardamacza.
* Wszystkim nie pasuje Pięknotka i zainteresowanie terminusów.

### Dark Future

* Liberatis Shall Fall (as slaves)
* Chevaleresse Shall Fall (go too far)
* Ofiary śmiertelne

### Pytania

1. 

### Dominujące uczucia

* To wymyka się spod kontroli.

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Pięknotka ma dobry, godny jej plan. Porwać Chevaleresse. A potem się pomyśli.

Pięknotka podbiła do Aleksandry Szklarskiej. Lokalna terminuska się zdziwiła - terminuska ciężkiej ligi? Z Pustogoru? Pięknotka umówiła się z Aleksandrą w Barbakanie - miejscu, które nie jest takie jak wszystkie inne Barbakany tylko lipniejsze. Pięknotka poszła.

Pięknotka po wejściu do Barbakanu poczuła, że coś jest Nie Tak. Nie wie czemu, to jej intuicja. Ale ona przywykła do ufania swojej intuicji, przesterowanej przez Cienia czy Saitaera. Po prostu Pięknotka poczuła się na wrogim terenie... ale na oko wszystko jest w porządku. Zeszła do niej Aleksandra. Pięknotka zauważa, że nie ma w okolicy żadnych konstruminusów. Skupiła uwagę; jakkolwiek to wszystko jest bardzo subtelne, ale jednak chce dojść do tego co się dzieje.

(Tr(8,8)): XVXV:

* Pięknotka ma wrażenie, że Aleksandra jest... pod wpływem jakichś chemikaliów. W ogóle, atmosfera w tym Barbakanie jest nie taka. Biochemia.
* Aleksandra zauważyła, że Pięknotka zauważyła. Przeciwnik jest gdzieś niedaleko - czeka na potencjalny atak.
* Cień jest gotowy do walki i zabijania.

Aleksandra lekko się uśmiechnęła widząc minę Pięknotki. Nie wykonuje żadnych ruchów przeciwko Pięknotce. Zaproponowała jej wejście wgłąb Barbakanu, ale Pięknotka odmówiła; nie ufa jej. Aleksandra wzruszyła ramionami. Pięknotka chce dokładnie wiedzieć co tu się dzieje. Zdecydowała się jednak dać się zaatakować - idzie za Aleksandrą. Aleksandra z Pięknotką przy biurku; jak Aleksandra może Pięknotce pomóc? Terminuska działa inaczej - chce znaleźć co tu jest prawdziwym problemem. Bezpośrednio spytała co jest grane.

* Konstruminusy były częściowo nieaktywne, nie działały; Aleksandra z Bartłomiejem je wyłączyli, by nie przeszkadzały.
* Jej syn jest w Liberatis i ona chce mu pomóc i go uratować.
* Nie mając większych sił, skupili się na wspomaganiu magichemicznym.

Pięknotka chce zbadać te środki; Aleksandra powiedziała, że nie zgadza się na to rozwiązanie - ma wsparcie z _proprietary_ źródeł. Pięknotka jest w prawie, ale Aleksandra zaznacza, że Pięknotka tu nie jest na co dzień - Aleksandra i Bartłomiej tak. Ich wspólnym wrogiem jest Liberatis. Pięknotka powiedziała, że musi to zrobić i zaczęła rzucać zaklęcie. Odczekała aż Aleksandra spróbuje jej przerwać i walnęła ją mocno. Zaczęła walkę wręcz. Aha, podkręciła się zasobowo stymulantami. (V: pokona Aleksandrę, V: wyciągnie Wroga, V: nic jej nie będzie)

VVX: Aleksandra jest na ziemi, ranna i lekko ogłuszona; Pięknotka ranna; krew poszła. Poczuła ruch za sobą - leci w jej kierunku płaszczkostwór z jadowym kolcem i paszczą. Pięknotka robi unik połączony z rzuceniem zszywaczem; by odzyskać równowagę i repozycjonować przeciwnika. XV - Pięknotka chlapie krwią na lewo i prawo, ale ten manewr sprawił, że ma przewage nad stworem; ma plecy potwora. Rzuca czar - chce zawrzeć i zamknąć potwora w bardzo ciasnej klatce by móc go bezpiecznie przebadać. Dodatkowo neutralizuje to troszkę używając swoich chemikaliów.

* Hindered, Contained
* Neutralized

Przeciwnik padł na ziemię w mikroklatce, wijąc się i nie mogąc się ruszyć. Pręty neutralizują ewentualne sygnały biochemiczne jakie może chcieć wysyłać. Pięknotka usłyszała buty biegnącego człowieka/maga do góry, w jej okolice. Krzyknęła - "szybko, pomóż" by ściągnąć Bartłomieja a sama przygotowała się do ataku. Stymulowana Pięknotka jest w stanie sporo zdziałać ;-). VVX - Pięknotce udało się z zaskoczenia rozwalić Bartłomieja; jej uderzenia ze stymulantami są bardzo potężne (a Cień wpływający na jej umysł jedynie wzmacnia siłę). 

Zarówno potwór, Aleksandra jak i Bartłomiej są w rękach Pięknotki. A lekko poszarpana terminuska drapie się po głowie - co tu się właśnie odmiauczało. Normalnie wezwałaby wsparcie - ale nie może, bo Chevaleresse.

Pięknotka skupiła się na tym stworze. Wygląda jak skonstruowana sztucznie bioforma. Ale jest to dziwna struktura; przebadała to. Pięknotka z zasobami (mini-Barbakan) i magią (biomantka) wraz ze swoją unikalną wiedzą o Trzęsawisku i dziwnych bioformach zabrała się do badań.

* V: ta istota jest bioformą wyprodukowaną na bazie człowieka lub maga; to jest, zawiera DNA takiego kogoś. Ale nie powstała całościowo. Dark arts.
* V: ta istota powstała dzięki adaptogenowi kralotycznemu, i to dość zanieczyszczonemu. To nie jest cieniaszczycki adaptogen. To nie robota Grzymościa.
* V: ta istota wzmacnia terminusów. Jego jad płynie w ich żyłach, dając im wspomaganie. Jednocześnie, jak wszystko ze słowiem 'kraloth', uzależnia i zniewala. Pięknotka wie jak wyjść z czegoś takiego - to nie jest czysty i pełny adaptogen, tu nie ma rzeczy tak silnych jak w wypadku zwykłego kralotha. Ale dla Pięknotki to jest _pure evil_ - zwłaszcza, że miała nieprzyjemne doświadczenia z kralothami.
* X: bioforma została zniszczona podczas badań; nie ma dowodów, ale Pięknotka ma dość, by móc sformować antidotum.
* X: przeciwnik wie - monitorował co tu się działo. Wie kim jest Pięknotka, co robiła i jak działała.

Zajęło to trochę czasu, ale Pięknotka dowiedziała się jak wygląda sytuacja. I wcale się nie ucieszyła. Nie spodziewała się, że tak ta sytuacja będzie wyglądała... zaczyna pracę nad antidotum. Musi pomóc pozostałym terminusom - potrzebne jej jest wsparcie.

Ale wpierw - Ataienne. Trzeba ją usadzić.

Pięknotka zażądała od Ataienne, by ta wyjęła macki z konstruminusów. Ataienne zimno powiedziała, że nie kontroluje konstruminusów w Kalbarku. Ataienne spytała Pięknotkę, skąd ta wrogość do niej - przecież Ataienne nigdy nic nie zrobiła przeciw niej a tylko pomaga. Niech Pięknotka autoryzuje Ataienne, ona z przyjemnością pomoże. Pięknotka ma problem z Ataienne - Ataienne nie jest Pustogoru, ona jest bytem Orbitera. No i zachowuje się dziwnie, bardzo dziwnie i niebezpiecznie.

Przepytana, Ataienne powiedziała:

* pomogła Chevaleresse złamać kody Alana, ale nie przekazała nikomu broni. Ona jest AI. To Chevaleresse zrobiła.
* nie chce krzywdy Chevaleresse. Będzie ją chronić za wszelką cenę.
* ma imperatyw ochrony temrinusów. Chwilowo jej celem jest egzekucja maga, który skrzywdził Alana. I jej programowanie to wspiera.

Pięknotka autoryzowała Ataienne na tydzień - lub do rozwiązania sprawy (co przyjdzie pierwsze). Podpięła Ataienne do Barbakanu. Ataienne objęła nową centralę dowodzenia; powiedziała, że jest nieco za ciasna; nie może użyć pełni swojej mocy (naprawdę to może - opanowała fizycznie pół miasta). Zapytana gdzie jest Chevaleresse, Ataienne zaczęła szukać. A w tym czasie Pięknotka skupiła się na antidotum. Aha, dodatkowe zadanie dla Ataienne - niech znajdzie działania Mateusza Kardamacza i Liberatis. AHA, niech dodatkowo naprawi te konstruminusy - najpewniej ktoś nimi manipulował i coś im zrobił. Ataienne przyjęła - zrobi to wszystko.

A antidotum + banki danych Ataienne (XtMZ+2:10,11): XXX. Niestety, Pięknotka ich nie postawi. Kralotycznie dostali bardzo ciężko. Przy braku środków takich jak Blakenbauer, czy Pustogor Pięknotka ich nie postawi; żaden normalny temrinus nie da rady. Innymi słowy, Pięknotka ma Ataienne, ma konstruminusy i absolutny brak jakiejkolwiek wiedzy lokalnej...

Pięknotce zajęło dłuższą chwilę próba postawienia tego antidotum. Kupiło to czas Ataienne na działania związane z Liberatis; Ataienne nie chce ich krzywdy ani zguby. Woli postawić Pięknotkę przeciwko Kardamaczowi.

Po zamknięciu biednych terminusów (a zgodnie z badaniami przeszłych raportów itp. z Barbakanu Pięknotka podejrzewa, że oni sami się nie zaćpali; ktoś im to zrobił), Pięknotka wraz z Ataienne skupiły się na dalszym działaniu.

Ataienne ma dla Pięknotki dobre wieści:

* Znalazła Chevaleresse. Młoda czarodziejka nie jest wśród Liberatis. Ona gawędzi z Kardamaczem, w tym jego dojo. Ataienne wygląda na zmartwioną.
* Konstruminusy - wszystkie 4 - są aktywne i na pełnej mocy.
* Ataienne powiązała bioformy z Kardamaczem. Nie ma dowodów na kralotyczne środki, ale ma dowody na to, że Kardamacz używał różnych bioform.
* Ataienne zaproponowała Pięknotce, że ONA zajmie się Liberatis. Rozpędzi ich na kawałki i odzyska broń dla Pięknotki - konstruminusami przyniesie ją do Barbakanu. Ataienne nie wie jak długo zadziała to rozpędzenie, ale powinno zdeeskalować sytuację. Pięknotka się zgadza - ale jeden konstruminus jest wsparciem Pięknotki.

Dla Ataienne to faktycznie żaden problem; gdy nie ma Leszka, ona dowodzi Liberatis...

A tymczasem Pięknotka - martwi się o Chevaleresse, bo martwi się, że ona chce wciągnąć Kardamacza w pułapkę - a to on wciągnie ją w pułapkę. Kardamacz zawsze był taki... śliski. Ale świetnie walczył. I zupełnie nie miał moralności - gdy przegrywali z kralothem, zostawił ją. Miał pecha bo kraloth poszedł ZA NIM a nie ZA NIĄ.

Pięknotka po prostu poszła do autoklubu piękna, który też pełni funkcję dojo. Z jednym konstruminusem. Wchodzi, minęła strażnika - acz sam strażnik sprawił, że miała ciarki. Coś... biomagicznego. I dotarła do Mateusza i Chevaleresse.

Mateusz... coś z nim jest _bardzo nie tak_. Pięknotka czuje się Skażona w jego towarzystwie. To znaczy że jego biologiczna struktura jest bardzo, bardzo nie w porządku. Ale zachowywał się uprzejmie i zimno wobec Pięknotki. Gdy Pięknotka aresztowała Chevaleresse (by ją wyciągnąć w prawie), ta zaprotestowała - ale Mateusz kazał jej iść. I Chevaleresse poszła.

Chevaleresse zaczęła ostro protestować Pięknotce - problemem jest Ataienne. Ataienne dowodzi Liberatis, zgodnie z tym co mówi Chevaleresse. Ale jednocześnie... to Chevaleresse zaniosła tam broń. Chevaleresse uważa, że Mateusz ma rację - trzeba chronić astoriańską kulturę a Ataienne jest zagrożeniem.

Pięknotka odwiozła Chevaleresse do szpitala z Lucjuszem Blakenbauerem. Wyjaśniła jej - skoro odeszła z Liberatis, mogli coś jej zrobić. A na serio - uważa, że to Mateusz coś jej zrobił, coś bardzo poważnego. Bo Chevaleresse obracająca się przeciw Ataienne? A Lucjusz zatrzyma Chevaleresse i jej pomoże.

Zostawiła Chevaleresse z Lucjuszem i pojechała do Tymona. Po drodze skontaktowała się z Pięknotką Ataienne. Spytała co z Chevaleresse - czy jej coś się stało, czy jest mentalnie uszkodzona. Pięknotka powiedziała, że Chevaleresse jest na badaniach. Ataienne poprosiła o autoryzację egzekucji Mateusza. Pięknotka odmówiła - jeszcze nie ma dowodów. Niech Ataienne kontynuuje plan rozbijania Liberatis i ich rozbrajania. Ataienne przytaknęła i się wyłączyła.

Pięknotka poszła do Tymona. Wyjaśniła mu sytuację i praktycznie powiedziała wszystko. Z Tymonem nie ma co ukrywać, musi wiedzieć w co wchodzi, zrobi wiele dla Alana i Chevaleresse... plus, można mu ufać. Tymon załatwi zastępstwo - pojedzie tam następnego dnia. Ataienne nie będzie sama (czyli nie będzie mogła działać za plecami wszystkich).

Następnego dnia Pięknotka poszła do szpitala, odwiedzić Chevaleresse. A tak na serio - odwiedzić Lucjusza. Lekarz siedział nad wynikami i był oschły jak zawsze.

Lucjusz powiedział co było nie tak - Chevaleresse miała podany eliksir, który zwiększał jej receptywność. Czyli łykała jak młody pelikan. Było to na bazie kralotycznej - ale podane sprawnie, bez zmiany Wzoru czy innych efektów ubocznych. Nie podoba mu się to wszystko. Pięknotka opowiedziała mu o sytuacji - dwóch umierających terminusów, dyskrecja, dziwny bioformowy eks-terminus... poprosiła o pomoc. Lucjusz się zgodził. Pomoże tej dwójce. Pojadą za parę godzin, jak zbierze szkieletowy sprzęt.

Tyle dobrego, że Ataienne zaraportowała - Liberitas zostali rozbici, rozproszeni i udało jej się odzyskać broń i Alana i Grzymościa. Są w Barbakanie. Duży sukces.

## Streszczenie

Kalbark okazał się jedną wielką bombą. Liberatis, silnie uzbrojeni. Kardamacz, kontroluje całe miasto. Terminusi, skażeni kralotycznie. I Chevaleresse na którą wszystko spadnie jeśli przyjdzie wsparcie. Pięknotka rozmontowała tą bombę, ściągnęła Ataienne (oficjalnie) i Zespół dał radę rozproszyć Liberatis, wycofać Chevaleresse i odzyskać kontrolę nad Barbakanem. Pięknotka zebrała wsparcie i na polu bitwy został na serio tylko Kardamacz.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: pokonała dwóch terminusów i bioformę bez Cienia. Potem zdobyła wsparcie Ataienne, wydobyła Chevaleresse i zapewniła siły w Kalbarku na potem.
* Aleksandra Szklarska: terminuska; Skażona kralotycznym wspomaganiem i pokonana przez Pięknotkę. Bardzo ciężki stan.
* Bartłomiej Małczarek: terminus; Skażony kralotycznym wspomaganiem i pokonany przez Pięknotkę. Bardzo ciężki stan.
* Ataienne: autoryzowana przez Pięknotkę do pomocy w Barbakanie, pokazuje bezwzględność wobec Małmałaza i Kardamacza. Rozmontowała Liberatis i odzyskała broń.
* Diana Tevalier: przyczyna wojny Liberatis - Kardamacz, zmanipulowana kralotycznie przez Kardamacza i doń dołączyła. Skończyła przez Pięknotkę w szpitalu.
* Mateusz Kardamacz: Pięknotka wyczuwa w nim bardzo chorą biostrukturę. Chyba przejął kontrolę nad Kalbarkiem. Zwalcza Liberatis za wszelką cenę.
* Lucjusz Blakenbauer: znalazł chorobę Chevaleresse; nie lubi kralotycznych środków jak to co jej podali. Pomoże Pięknotce w ratowaniu terminusów z Kalbarka.
* Tymon Grubosz: znalazł zastępstwo na Zaczęstwo i pojedzie pomóc Pięknotce i Ataienne rozwiązać problemy w Kalbarku. Oczywiście, dyskretnie i na urlopie.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Jastrzębski
                            1. Kalbark
                                1. Mini Barbakan: skażony kralotycznie przez Kardamacza i jego bioformy; miejsce walki Pięknotki z terminusami i bioformą.
                                1. Autoklub Piękna: pełni też rolę dojo. Kardamacz podbił tam Chevaleresse a Pięknotka tam Chevaleresse aresztowała. Miejsce... dziwne.

## Czas

* Opóźnienie: 0
* Dni: 5
