## Metadane

* title: "Operacja: mag dla Symlotosu"
* threads: pronoktianska-mafia-kajrata, furia-mataris-agentka-mafii
* motives: teren-morderczy, echa-inwazji-noctis, sprzet-kiepskiej-jakosci, dont-mess-with-my-people, infiltracja-dyskretna
* gm: żółw
* players: kić, fox

## Kontynuacja
### Kampanijna

* [230806 - Zwiad w Iliminar Caos](230806-zwiad-w-iliminar-caos)

### Chronologiczna

* [230806 - Zwiad w Iliminar Caos](230806-zwiad-w-iliminar-caos)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ?
* Struktura: 
    * ?
* CEL: 
    * MG
    * Drużynowy
* THEME
    * Kajrat mówi 'never again' - będzie walczył o noktian

### Co się stało i co wiemy

* Dane
    * Kajrat wie, kto stoi za porwaniem Furii - Grzymość
    * Kajrat chce pozyskać maga, w końcu, by odzyskać Aynę
    * KONKLUZJA: porywamy maga

### Co się stanie (what will happen)

* F1: Kajrat wyjaśnia plan
    * Furie PLUS Quispis mają porwać maga
        * wydobyć, przechwycić
            * może z Akademii Magicznej? (acz tam zobaczą, że noktianie mają pomoc)
            * Grzymość ma kasyno i miejsce holowirtualne; ma dużo sił w Zaczęstwie
    * Spotkanie z Caelią; ma środki
* F2: Quispis, droga przez tereny niebezpieczne
    * Unikanie patrolu Czaszek ORAZ dron Pustogoru
        * Czaszki nie jadą tam
            * Leon chce strzelać do pnączy, Lestral uważa, że przelecą Quispisem 
            * 
    * Przejście przez mur, tam jest patrol i systemy defensywne
* F3: Transport przez Lasy Podwierckie do Zaczęstwa
    * Dmitri czeka, przewiezie je do Podwiertu
    * superpancerny pociąg; same mają problem z przejściem do Zaczęstwa. Ale mogą próbować.
    * shelter 'Cicha Gwiazdka'

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Xavera Sirtas
    * OCEAN: (C+A-): żywy ogień, jej dusza jest pełna pasji, a jej słowa ostrzem. "Kto siedzi cicho, tego spotyka wszystko co najgorsze"
    * VALS: (Security, Tradition): wszystko odnosi do tego co już widziała i co było, działa dość tradycyjnie. "Wszystko już było i się stało"
    * Core Wound - Lie: "Przeze to, że dużo mówiłam i szperałam, moi rodzice się rozwiedli" - "Będę walczyć i ognistą walką udowodnię, że warto być razem!"
    * Styl: Zawsze gotowa do walki. Agresywna i asertywna. Dobrze wybiera walki. Jej upór często wpędza ją w kłopoty.
* Isaura Velaska
    * OCEAN: (E+A+): uspokajający głos w zespole, zawsze znajduje chwile na uśmiech i dowcip. "Nieważne co się dzieje, trzymaj fason i się uśmiechaj"
    * VALS: (Harmony, Benevolence): "Zespół to jak rodzina. Musimy być dla siebie wsparciem."
    * Core Wound - Lie: "Rodzina mnie porzuciła, ale Furie adoptowały" - "Jeśli zrobię dla Was rodzinę, wszyscy zawsze będziemy razem bez kłótni i sporów"
    * Styl: Ostoja spokoju wśród chaosu, rozładowuje napięcia i tworzy poczucie wspólnoty i jedności; dystyngowana, zachowuje się jak arystokratka
* Caelia Calaris: savarańska lekarka Kajrata
    * OCEAN: (E-N+A+): wyjątkowo nieruchoma i ostrożna społecznie NAWET jak na savarankę, ale empatyczna i przyjacielska. Przytulna. "Oszczędzajmy wszystko."
    * VALS: (Harmony, Achievement): robi co może tym co ma i dba o morale jak umie. "Razem sobie poradzimy."
    * Core Wound - Lie: "Calaris, macierzysta jednostka umarła przez brak zasobów" - "Jeśli będziemy oszczędzać na wszystkim, przetrwamy! Nie kosztem załogi, nigdy!"
    * Styl: Całkowicie nieruchoma, unika bycia pod słońcem, mało mówi i bardzo ciężko pracuje. Wrażliwa, widać ślady łez. Słaba fizycznie z nieprzystosowania planetarnego.
* Lestral Kirmanik: pilot Quispis
    * OCEAN: (E+A+C-): Uwielbia przygody i często działa impulsywnie. Emocje nosi na wierzchu i łatwo się dostosowuje. "Melduję, że wysadziliśmy ten most, bo była impreza!"
    * VALS: (Stimulation, Face): Wesołek ekipy który dba o to, by być postrzegany jako wesołek. Lubi jak coś się dzieje. "Panie sierżancie, melduję, że da się zrobić z tej sytuacji coś zabawnego."
    * Core Wound - Lie: "Wychowałem się na ulicach, nikt mi nie pomógł" - "Jeśli się nie będę pokazywał i nie będę widoczny, znowu mnie zostawią; marketing, baby".
    * Styl: Bezceremonialny, głośny i pełen energii. Ubiera się niekonwencjonalnie. Robi dowcipy, ale ogólnie jest lubiany i dba o swoich.
* Leon Varkas: żołnierz p.d. Kajrata
    * OCEAN: (E+N-O+): Zbyt pewny siebie. Agresywny wobec przeciwników i ma agresywne plany. Mało czym się przejmuje.
    * VALS: (Hedonism, Achievement): Doświadczony, mnóstwo trenuje i próbuje być lepszy od innych. "Jestem najlepszy - w łóżku i na polu bitwy".
    * Styl: Dobrze ubrany elegancik wizualnie, ale jak otworzy usta - sprośny. Świetny infiltrator, doskonale robi nożem i w walce wręcz

### Scena Zero - impl

.

### Sesja Właściwa - impl

Kajrat zażądał odprawy. Dwie Furie się stawiły. O dziwo - nie ma Isaury.

* X: gdzie Isaura?
* K: Furia Isaura jest zajęta czymś innym. Nie mogę powiedzieć Ci czym, bo idziesz na teren wroga

Kajrat wyjaśnił:

* Ayna jest porwana przez Symlotos. I jest tam jeszcze jedna Wasza siostra. Dogadałem się, że za maga otrzymamy obie Furie.
* Jesteście w stanie pozyskać tego maga. I jakkolwiek nie lubię oddawać anomaliom różnego rodzaju magów, ale tego docenicie. 
    * WIEM kto na Was polował. Lokalna mafia, Wolny Uśmiech.
        * Innymi słowy, Furie - noktianki - są wykorzystywane jako niewolnice dla lokalnych bonzów.
        * Więc porwanie maga od Grzymościa i oddanie go Symlotosowi będzie właściwe. (zimny uśmiech)
            * Amanda: popieram. Co wiemy?
                * Kajrat: niewiele. To Wasze zadanie. Co mi się udało dowiedzieć:
                    * Grzymość operuje z Zaczęstwa i Podwiertu - dwa miasta
                    * Ogólnie jako mafia współpracuje z Cieniaszczytem
                    * Grzymość nie jest głupi - nie niszczy lokalnej infrastruktury, nawet pomaga w odbudowie po wojnie
                    * Jego najważniejszym węzłem jest kasyno w Zaczęstwie. Tam są magowie. I Akademia Magiczna, ale ona nie jest pod jego kontrolą
            * Wasze zadanie:
                * Dotrzeć do Zaczęstwa. Rozeznać teren. Zbudować drogę ucieczki. Porwać maga. Przynieść mi maga.
* Z dobrych wiadomości - mamy agenta po drugiej stronie. Dmitri Karpov. Nienawidzi mafii tak, że współpracuje z nami. /lekki uśmiech
    * Macie leki dla jego żony. To jeszcze bardziej zachęci go do współpracy.
* Quispis dostarczy Was do muru. Powinno się wszystko udać. Ale nie mam jak zapewnić Wam drogi powrotnej, nie wiem ile Wam zajmie.

.

* A: Do muru transport, potem na piechotę. Jak Dmitri?
* K: Dmitri Was znajdzie. Będzie wiedział jak będziecie blisko. Leśnicza jednostka szukająca zagrożeń, min itp. Dmitri Was nie wyda.
    * Potencjalnie, Dragan może pomóc Wam w przedostaniu się przez mur. A Caelia dostarczy Wam odpowiedni środek.
* K: NIe musicie się zabijać jeśli wpadniecie w ręce wroga. Odzyskam Was. Jakoś.

Schodzicie do bardziej uszkodzonej części statku; po prawej jest ciemno i smutno. Tam jest medical. Ale po lewej - praca wre. Słychać maszyny. I śpiew po noktiańsku. To oczywiście Dragan. Dragan strasznie fałszuje, gdy naprawia jakieś urządzenia ze swoimi asystentami. To jakieś biesiadne piosenki noktiańskie. I jest czysto a capella. Ucieszył się widząc Furie.

* Dragan: 
    * przedstawiam zmodyfikowany Culicyd. Ten ma 15% czas baterii dłuższy. Trudniej go wykryć. Jest troszkę słabszy. Dalej zapewnia _targeting computer_ i autonomię jednostki. Można go wyłączyć. I... jak go stracimy, łatwo go odbudować.
    * główna modyfikacja: wiemy, że mur jest bardzo silnie zabezpieczony elektronicznie. Detektory, skanery, miny, wszystko co lubicie. Te Culicydy mogą w stan samozniszczenia emitując odpowiedni _scrambler_. Część rzeczy pozakłócacie
        * komendant Kajrat kazał zrobić dywersję w kilku miejscach, np. Isaura dowodzi operacją ściągnięcia...
* X: NIE MÓW NAM TEGO! Nie chcę wiedzieć!
* D: Ah. 
* X: Mamy dywersję.
* D: Ok. A, bo to niejawne?
* A: Mhm.
* D: No popatrz... /zamyślenie.
* X: Nie mów nam co robią inni!

Dragan ma kosę z Caelią po sprawie z migreną - JEGO bolał łeb a ona powiedziała że nie dość.

Czas na spacer do skrzydła medycznego...

Medyczny jest niesamowicie czysty. Nie jest też tak ciemny - mało widać, ale widać DOŚĆ. Acz nie, jeśli idziesz szybko. Na pewno jest ponuro. KĄTEM OKA widać Caelię. Ona się nie ruszyła. Siedzi przed komputerem. Na niskim poziomie oświetlenia. Coś pisze.

* A: Caelio?
* C: (przestała pisać)
* A: Wiesz, po co jesteśmy
* C: nie ma innej możliwości?
* A: niż zdobyć maga?
* C: niż oddać leki Dmitriemu. Te leki, ta dawka, to 20 żołnierzy.
* A: potem porozmawiasz z komendantem
* X: może jest inny sposób, przyniesiemy jak się uda ominąć...
* C: nie przyniesiecie. Nikt nigdy nie przynosi. /fakt /wyjęła z kieszeni dwa dozowniki tabletek, wstała i podeszła
* X: gdzie jest dzieciak który powinien z Tobą siedzieć?
* C: przestraszył się mnie. Nie jestem groźna /lekki uśmiech
* X: czegoś musiał się wystraszyć...
* C: dałam mu do ekstrakcji odpowiednie środki ze zwłok. Nie wrócił. To jest proste zadanie... uciekł, gdy zobaczył twarz siedemnastolatki. Ale ona już nie żyła /wyjaśniając
* X: bądź ostrożniejsza przy dzieciach, wiem że to potrzebne i nie jest bardzo trudne, ale młodzi nie wytrzymują widoku trupów... miał wykształcenie medyczne?
* C: lepiej, by teraz się nauczył, niż gdy jego matka wjedzie na stół operacyjny /cień empatii
* C: jeszcze boi się duchów. Przejdzie mu. Nie wie jeszcze, czym go karmię.

.

* C: każdy środek dla Was to pięć dawek puryfikacji
* X: coś do alkoholu czy wody?
* A: jeśli tam będzie...
* A: czy jesteś w stanie dać coś co możemy same zmienić tego formę?
* C: nie... mam różne środki. Każdy to pięć dawek puryfikacji
* A: jedna dawka środka 'na dzika' (wstrzykiwany / wstrzeliwany), jedna dawka czegoś co dosypujesz do wody lub alkoholu
* C: dostarczę Wam to
* A: dzięki

Xavera zauważyła, co Caelia ma na pulpicie. Analizę ile % zmniejszenia światła spowoduje spowolnienie ruchów -> oszczędność energii -> mniej kalorii -> mniejsze zużycie. I stosunek tego do psychiki żołnierzy.

Quispis - hoverAPC - wygląda fatalnie. Jak zawsze.

* Leon: komendant robi błąd. Lestral, masz dość paliwa?
* Lestral: nie powiedziałbym, że robi błąd. Paliwo by się przydało
* Leon: po co mamy lecieć do ASTORIAN. Zbudujmy tu imperium.
* Lestral: widzisz, Leon, dlatego komendant Kajrat jest komendantem. To nie na mój rozum. Ja tylko wożę Furie.
* Leon: ciekawe, jakie są dobre w walce.
* Lestral: ciekawe, jakie są dobre w łóżku.
* Leon: niedługo będę wiedział...

Leon oficjalnie dowodzi Lestralem (ku wielkiej żałości Lestrala), ale Furie mają wyższy poziom starszeństwa. Kajrat wysłał bardzo mały oddział - inni robią dywersję. W wielu punktach. Bardzo zaawansowana operacja.

Quispis rusza. Macie wygodnie. Leon i Lestral się przerzucają, ale... grzeczniej, bo są Furie. W czym Leonowi przechodzi grzeczność (ten elegancki).

* Le: Xavera, tak?
* X: Zgadza się.
* Le: Czy to prawda, że Furie są dobre w łóżku?
* X: Tak
* X: A Amanda w ogóle jest najlepsza z nas wszystkich, jakbyś widział jakie cuda potrafi zrobić, nie uwierzyłbyś...
* A: On już nie wierzy. Jest niewierzący.
* Le: To prawda, że miałyście to w akademii? Uczyłyście się tego?
* X: A Wy nie mieliście?
* Le: No, nie, po co nam? Znaczy... że jak? /skonfundowany
* Les: Leon, to jakby Cię złapały te, roślinostwory. Przyda Ci się wtedy ta umiejętność. Zaspokoisz je i...
* Le: Lestral, zamknij mordę.
* Les: Ok, ok, dobrze, 'poruczniku'.
* Le: To czemu Was tego uczą?
* A: Bo jesteśmy delikatnymi słabiutkimi kobietami
* Les: Ja już widzę ten pojedynek Amandy z Leonem. Po jednej stronie słabość, po drugiej libido, Leon będzie chodził takimi szerokimi łukami. Nie będzie siedział przez tydzień.
* Le: Ja pierdolę, Lestral, zamknij cholerną mordę.
* X: Długa droga przed nami, macie pytania to pytajcie, będzie nudno
* Les: (odwrócił się do Xavery z zaskoczeniem) Jesteś z tego samego miotu co Amanda? Sory, nie wiem jak to się nazywa.
* X: Amanda ma specjalny talent do prezentowania atutów w działaniu, ja lubię pogadać
* Les: czyli Ty jesteś gawędziarką erotomanką a ona jest... praktykiem?
* X: No ba.
* Le: Amando, udzielisz mi lekcji czy dwóch?
* A: Jeśli naprawdę potrzebujesz edukacji...
* L: Bardzo
* A: Xavera będzie wystarczającym nauczycielem
* X: Nie bądź taka skromna, zawsze jako prymuska przekazujesz wiedzę...
* A: Dlatego czas byś wiedzę przekazała dalej. Jak umiesz kogoś nauczyć, znasz się na tym.
* X: To jak jedzenie czegoś co ktoś zwymiotował. Za pierwszym razem smaczne i pożywne, za drugim...
* Les: Xavero, nie jesteś jak jedzenie czegoś co coś zwymiotował. Jesteś w moim guście. Tak powiem.
* X: Wydawało mi się że bardziej Amanda?
* Les: Czemu Amanda?
* X: Bo Caelia. Spokojna, nie mówi za dużo, nie rusza się za dużo...
* Les: Zmieniam zdanie, jesteś w moim typie.
* Le: Ty patrz na drogę. Droga jest tam. Jesteś pilotem.

Nagle Lestral ostro dał po hamulcach. Wyłączył wszystko. Przeszedł w tryb "kupa metalu". Leon od razu spoważniał i włączył taktyczny komputer. Niedaleko przelatuje powietrzny patrol ze Szczelińca. Nie atakują niczego, tylko monitorują. Amanda patrzy na ich wzorce - to patrol, dobry patrol, ale szukają problemów a nie czegoś. Odkąd Kajrat rozpoczął działania - odkąd Grzymość porwał pierwszą Furię która słyszała głos Kajrata, Pustogor nasilił patrole.

Trzy potencjalne poważne problemy:

* Skradanie się w cieniu patroli pustogorskich
* Przebicie się przez obóz Czarnych Czaszek
* Pozornie bezpieczny teren. Ma takie pnącza wystające z ziemi. Czaszki to unikają szerokim łukiem.

.

* Lestral: "Panienki? Leon? Coś tam pełznie."

Duża gąsienica pełznie w tą stronę. A jesteśmy w cieniu patrolu. I Quispis jest nieruchomy. Gąsienicoglut. Pełznie do ostatniego ruchu (Quispis).

Amanda przycelowuje i strzela do grzmotodrzew. Cel - rozpalenie grzmotodrzew by one się uruchomiły i by patrol uznał, że ten teren warto ominąć - zrobienie 'okna' przez które prześlizgnie się Quispis. By drzewa były jak najbliżej patrolu (wtórnie: uszkodzić patrol).

Ex (+P: nie spodziewają się ALE nadal to jest normalne i możliwe, +P: mistrzostwo Amandy) -> Tr +3: {niewykrycie, odegnanie, okno dla Q.}

* Vr: niewykrycie; patrol nie wie o Quispisie i działaniach Amandy, "to normalna rzecz"
* Vr: okno dla Quispisa; Quispis może się prześlizgnąć

Xavera tymczasem zajmuje się gąsienicostworem. Przebiegnie się Culicydem, ściągając gąsienicostwora a potem wróci silnikami skokowymi.

Tr Z +3:

* V: Xaverze udało się odciągnąc stwora ukrywając się przed patrolem
    * Xavera zwala dużą grupę obiektów by gąsienicy uwagę zwrócić (+1Vg)
* V: Xavera szybkim skokiem wróciła na Quispis, który już był w locie, lecą dalej korzystając z okna.

15 minut później, udało się ominąć daleki patrol. Komputery wykryły coś ciekawego.

* Gniazdo... czegoś. Jakieś insekty. Duże i obleśne. Latające karaluchy? A tam - różnego rodzaju interesujące rzeczy. To, co ludzie w okolicy mogli mieć.
    * To, co tam jest - tam jest interesujący noktiański synt. Był biosyntem, ale 'bio' zostało zjedzone.

Zespół decyduje się lecieć dalej. Nie ma opcji. Lestral "ale Amando, to jest nasz synt!". Amanda: "To RESZTKI naszego synta". Lestral: "Mówisz jak Caelia...". Zbliżają się do zewnętrznego systemu defensywnego.

Przed murem, w okolicach sensorów itp w niektórych miejscach jest taka DZIWNA trawa. Szczególnie jest gęsta tam gdzie zniszczone ciała potworów. Nie porasta zwłok, ale rośnie blisko i jest blisko kości.

(obrona przed zewem mentalnym)

Tp +2:

* X: Leon i Lestral
* V: Amanda i Xavera

Quispis się zatrzymał, poza zasięgiem sensorów. Poczekaliście przez moment aż LOKALNY patrol przeleci. Jedno dobre - nie ma TAI. Bo nie działają dość dobrze na tym terenie.

* Leon: (uruchamia Culicyd, przygotowuje się do wyjścia)
* Amanda: Ty dokąd?
* L: idę Ci pomóc.
* Amanda: chyba Cię pogięło. Dbacie o pojazd.
* Lestral: Furio, w czwórkę mamy większe szanse wykonania misji.
* A: MIsją jest przejście NASZEJ dwójki przez ten mur. WASZEJ dwójki - dostarczenie tutaj. Chcecie ewentualnie ułatwić? Możecie udawać wabik. Ale odradzam, ryzykujecie jeden z nielicznych quispisów. Z dwóch.
* L: pomożemy Wam przejść przez mur i odjedziemy.
* A: POradzimy sobie z przejściem
* L: Może. Nie chcę, by coś Wam się stało zanim pójdziemy do łóżka.
* Les: On ma rację, możemy Wam rozwiązać problem min.
* A: Niszcząc Quispisa?
* L: Nie, striangulujemy lokalizację z Wami.

Xavera zauważa, że zwierzęta się POŁOŻYŁY przy trawie. 

* Amanda: "ogłuszamy i autopilot?"
* X: Dwie Furie, większe szanse. /ogłusza
* A: Wiesz, skoro nie chcesz by nam coś się stanie zanim będzie seks... (kuszące bioderka) /ogłusza

Związani Leon i Lestral, pozbawieni Culicydów, po oddaleniu się Quispisa. Lestral obudzony.

* Les: Ale... co się stało?
* A: Potrzebowaliście odrobiny odpoczynku.
* Les: Przespałem się z Furią i nic nie pamiętam? /głębokie zranione rozczarowanie
* A: Może tak, może nie, ale możesz spytać Isaury?
* Les: CO? Co ma Isaura do tego?
* A: Chciałeś wiedzieć jak to jest z Furią. My musimy iść dalej ale Isaura jest w bazie.
* Les: I czemu Isaura miałaby być zainteresowana?
* A: A jaki ma wybór? Kogo ma do wyboru?
* Les: No... komendanta Kajrata. Przecież to oczywiste.
* A: A komendant?
* Les: No ale plotki mówią, że Kajrat konkretnie ratuje Furie, bo mu się podobają. Na sekretne orgie Furii. /cała wielka teoria o Wielkim Zakonie Furii
* A: (TAK TEORIE SĄ PRAWDZIWE I TRZEBA MIEĆ ZAPROSZENIE NA ORGIE!)
* X: (CO NAJMNIEJ TRZY FURIE NA FRAGMENTY I TERAZ ISAURA!)

.

* (+Z) V: Lestral nadal jest pod wpływem, ale on... on poleci. On się zajmie. On na pewno sobie poradzi. On porozmawia z Isaurą. On dostanie wpierdol.
* V: Nie tylko Lestral wróci, ale wróci ostrożnie, prawidłowo... i jeszcze doprowadzi do miejsca Xaverę i Amandę.

Xavera i Amanda stoją przed Wielkim Murem Pustogorskim. Wygląda imponująco. Kajrat odpalił kilkanaście dywersji w jednym czasie. To sprawia, że nawet jeśli A+X zostawią ślady, to nie są jedyne.

* F1: Dostać się do samego muru, przez miny i czujniki
* F2: Wdrapać się na mur i nie dać się zestrzelić przez autodziałka
* F3: Pozostać niezauważone i wylądować po drugiej stronie
* F4: By nikt nie wiedział że tu byliście

Miny i czujniki da się namierzyć detektorami Dragana. Używając silników skokowych dostajemy się do samego muru Pustogorskiego. I świeże trupy są bezpieczne, bo miny nie zdążyły się zregenerować - nikt nie odbudował defensyw.

Ex (RED: Culicydy z silnikami skokowymi, detektory Dragana oraz koordynacja Furii, +P: wykorzystanie świeżych trupów, +P: macie obraz jak tu się poruszać) -> Tp+3:

* X: zajmuje więcej czasu niż chcemy; siatka jest gęsta, są autoregenerowane defensywy itp. Kajrat nie uwzględnił paranoi Pustogoru.
* V: udało się dostać, ALE jeszcze zapewniamy wykrycie.
* X: systemy Pustogoru wykryły. Wiedzą, że jest próba intruzji.
* V: NADAL da się doprowadzić do tego, że uwierzą, że intruzja skończyła się na trawce.

Wdrapać się na mur i nie dać się zestrzelić przez autodziałka. Amanda poświęca Culicyd jako EMP; a Xavera w swoim ją przeprowadzi i silniki skokowe by przejść przez okna. (EMP do budowania okna, przedostanie, niezauważenie)

Tp+4:

* V: Amanda się wdrapała w jakąś szczelinę i odpaliła EMP z Culicyda. Skoczyła a Xavera ją złapała. Xavera wskoczyła korzystając z okna i rzuciła zniszczonym Culicydem na trawkę mięsożerną.
* V: Xavera, trzymając Amandę, orientując się w sytuacji i idąc za śladami echa, wskoczyła na szczyt muru Pustogorskiego, po czym zaczęła się ześlizgiwać.
* X: Niestety, trzeba poświęcić drugiego Culicyda. Amanda i Xavera są bez servarów.
* V: Oficjalnie, Xavera i Amanda przedostały się nie zostawiając śladów obecności. Czyli wydawało się, że intruzja jest odparta.
* Vr: Furie oderwały się od Muru i uciekły w las...

Czas, by Dmitri zrobił to co powinien. Ale Furie są po stronie Szczelińca.

## Streszczenie

Kajrat odkrył, że to Wolny Uśmiech stoi za porwaniem Furii. Wysyła Xaverę i Amandę do Zaczęstwa, by pozyskały maga i zrobiły kanał przerzutowy. Furiom w Quispisie udało się dotrzeć do Wielkiego Muru Pustogorskiego i go przekroczyć, acz skończyły bez servarów. Mają jednak po drugiej stronie ukryte zapasy oraz kontakt do agenta po stronie Szczelińca...

## Progresja

* .

## Zasługi

* Amanda Kajrat: wysłana przez Kajrata do Zaczęstwa; zero poczucia humoru na Quispisie. Ale robi z grzmotodrzew dywersję dla patrolu i poświęca inteligentnie Culicyd by przejść przez Mur Pustogorski z Xaverą.
* Xavera Sirtas: wysłana przez Kajrata do Zaczęstwa; żartownisia i flirciara, dokucza Amandzie. Szybko wymanewrowała gąsienicostwora i wymyśliła plotkę o orgiach Furii.
* Ernest Kajrat: poznał los Furii i nie zamierza tego tak zostawić. Chce odzyskać od Grzymościa Furie; wysłał Amandę i Xaverę, by one porwały maga i zbudowały kanał przerzutowy.
* Caelia Calaris: savarańska lekarka nie jest fanką dzielenia się medykamentami, bo siły Kajrata mają ich po prostu za mało. Ale wykonuje polecenie.
* Dragan Halatis: nie do końca ogarnia koncept 'dyskrecji', ale prawidłowo zmienił Culicydy pod kątem potrzeb Xavery i Amandy. Świetny inżynier.
* Leon Varkas: bardzo zainteresowany tematem Furii i łóżek; flirtuje z Amandą i łyka od Xavery o Furiach jak młody pelikan. Nie w pełni wierzy w plan Kajrata. Pod wpływem kwiatów niedaleko Muru wpadł w rycerskość i chciał pomóc Furiom, ale się oddalił za ich poleceniem, bo uwierzył w wielką orgię Furii Mataris.
* Lestral Kirmanik: jawnie flirtuje z Xaverą, która jest w jego typie. Dokucza Leonowi. Pod wpływem Kwiatów prawie naraził Quispis, ale wrócił do bazy zachęcony Orgią z Furiami.

## Frakcje

* Nocne Niebo: rozpoczyna operację odzyskania Furii od Symlotosu. Cel - mag od Wolnego Uśmiechu. Środek - Amanda i Xavera. A przy okazji Kajrat miał specjalne zadanie dla Isaury...
* Wolny Uśmiech: Kajrat wie, że oni stali za porwaniami Furii. Kajrat planuje pozyskać od nich maga i odzyskać swoje Furie.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, SW
                    1. Granica Anomalii
                        1. Ruina miasteczka Kalterweiser
                    1. Wielki Mur Pustogorski

## Czas

* Opóźnienie: 2
* Dni: 2

## Specjalne

* .

## OTHER

.