## Metadane

* title: "Tchórzliwy Leonidas na Niedźwiedziowisku"
* threads: brak
* motives: hartowanie-verlenow, rytual-doroslosci, katastrofalny-paradoks
* gm: fox
* players: żółw, kić

## Kontynuacja
### Kampanijna

* [230517 - Tchórzliwy Leonidas na Niedźwiedziowisku](230517-tchorzliwy-leonidas-na-niedzwiedziowisku)

### Chronologiczna

* [230419 - Karolinka, nieokiełznana świnka](230419-karolinka-nieokielznana-swinka)

## Plan sesji
### Theme & Vision & Dilemma

.

### Co się stało i co wiemy

* Kontekst
    * .

### Co się stanie (what will happen)

* S0: 
* S1: 
* S2: 
* S3: 

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

.

### Scena Zero - impl

Dwa dźwiedzie. "Lekka Stopa" i "Palisadowspinacz". Dwóch dźwiedzich komandosów. Proste zadanie. Czwarte podejście do Leonidasa - ma pójść do lasu, pobić się z niedźwiedziem i wrócić. Poprzednie trzy razy nie poszły mu najlepiej. Wrócił jak się ściemniło za pierwszym razem. Tym razem jest przygotowany. Ostatniej nocy się nie przespaliśmy - nie możemy się w pełni skupić. Nie może się dowiedzieć Leonidas - że jesteśmy dźwiedziami. To magia dzieciństwa Verlenów (Święty Mikołaj).

Jesteśmy w lesie, jest ciemno. Jak.

Leonidas wyruszył ze skraju koszar. Nasz cel - niech Leonidas się potknie, straci plecak i "niedźwiedzie" zapolują i zjedzą mu jedzenie a on smutno patrzy bo nie poradzi sobie z dwoma i musi sobie poradzić

Tr (bo nie ma się zorientować) Z (bo weterani) +3 +Og:

* V: "Rustle rustle", Leonidas się przewrócił i stracił plecak (jedno z nas wyłazi do plecaka i z bezczelnością i satysfakcją wyżera, BEZCZELNIE) +1Vg
* V: Operacja udana, zasoby stracone. Leonidas się wycofuje.

"Palisadowspinacz". Wchodzę na drzewo i aserukuję by Leonidasowi nic się nie stało. Teren jest wybrany i przygotowany, nic szczególnego się nie dzieje. Udaje głosy strasznego lasu. A "Lekka Stopa" robi szelesty tu i ówdzie. (+2Og)

* Vr: Leonidas przestraszony, ale zbraceował się - nie zrezygnuje tym razem! Pozytywnie nastawiony do tego że się mu uda.

Młody próbuje się ucharakteryzować by zniknąć. Trochę mu zajmie maskowanie, nie zwraca uwagi na otoczenie praktycznie. Palisadowspinacz będzie udawał że młodego nie znajdzie jak zadziała, Lekka Stopa patroluje i pilnuje. Udało się - młody ma podniesione morale. Rozstawia pułapki, zamaskował się świetnie, nawet trudno go wykryć jak się porusza, przyłożył się do nauki. Ustrzelił ptaszka, nasmarował go miodem z opakowania i położył go w dość widocznym pieńku w lesie.

Tak, zastawia pułapkę na niedźwiedzia XD.

Palisadowspinacz chce znać rozłożenie pułapek. By wpaść tylko w te nieważne.

Tr Z (obserwuję cały czas) +3:

* V: Palisadowspinacz wie gdzie są pułapki i w które wpaść by zjeść rybkę bez większej szkody.

Jest wieczór / noc. Lekka Stopa szuka wersji nietoperza-wampira. Nic super groźnego, ale postraszy Leonidasa; taki "latający pająk we włosach". Te nietoperze plączące się we włosy. Lekka Stopa ściąga nietoperza peruką. Po co? By młody nie miał za łatwo za szybko. (+1Vg)

--> konflikt się zmienia na "zjadam rybkę i uczę czegoś młodego". (+5Og - chcemy go przestraszyć)

* Og: Lekka Stopa pozyskuje nietoperz. Leonidas był przygotowany na wszystko ale nie na NIETOPERZA PLĄCZĄCEGO SIĘ WE WŁOSACH!!! Każdy normalny się przestraszy, nie on. Tarza się, skutecznie zajęty.
* Vz: Palisadowspinacz przeszedł przez pułapki, zjadł rybkę i poszedł. Chłopakowi udało się wyplątać z nietoperza
* X: Chłopak zaczyna mieć pierwsze podejrzenia że za dużo dziwnych rzeczy. (to znaczy że dramatyczno-teatralne plany odpadają).
* X: Chłopak się zranił wpadając w pułapkę swoją walcząc z nietoperzem. Hańba i ból.
* Xz: Palisadowspinacz tak się przejął Leonidasem że wpadł w pułapkę i się zranił. Nie wyjdzie bez pomocy XD.
    * Lekka Stopa idzie ratować Palisadowspinacza, ale zostawia pułapkę że w pułapkę wpadło coś WIĘKSZEGO niż niedźwiedź. Prawdziwy Potwór Verlenlandu.
        * Cokolwiek jest na stopie wśród opowieści dziecięcych, "zaginiony wyvern Blakenbauerów - pół płaszczka pół hydra pół smok - cały Blakenbauer". 
* Vz: Palisadowspinacz uratowany (acz ranny), chłopak odpędzony.
* Og: Chłopak złapał nietoperza, wcisnął w pułapkę jeszcze raz, uszkodzona przez COŚ DUŻEGO. PRAWDZIWY BLAKENBAUER.

Chłopak się bardzo boi, ale ma dwa aspekty że osiągnął sukces. I Palisadowspinacz jest ranny.

Dźwiedzie przechodzą do trybu obserwacji. A Palisadowspinacz liże łapę. Leonidas robi sobie obozowisko, szałas w korzeniach zwalonego drzewa by mieć dystans. Tam się maskuje. A dźwiedzie na lookoucie - by nic mu się nie stało. Leonidas zauważył ślady rannego niedźwiedzia i znaleźć stan niedźwiedzia. Niewidoczny Leonidas się maskuje. Ma dzidę z patyka z nożem na końcu. Palisadowspinacz się ukrywa - jako niedźwiedź a nie dźwiedzi komandos. Młody ma przewagę (krew + maskowanie + możliwość zaskoczenia) co sprawia że Palisadowspinacz muszę się kontrolować (zaskoczenie).

Tr Z (szkolenie, to nie pierwsze niedźwiedziowisko) +2 +5Og:

* Vr: Leonidas widzi że zaskoczył (ale nie zaskoczył)
* X: Palisadowspinacz jest ranny, nie obronił się jak chciał - rana nie bardzo głęboka ale KOLEJNA
* Og: Palisadowspinacz jest ranny WIĘC na moment 500 kg niedźwiedzia chce rozszarpać dzieciaka.
    * -> +Og

W tym momencie Leonidas skoczył do tyłu i użył magii. Gniew w Palisadowspinaczu zaczyna się wzmagać, oba dźwiedzie morphują w POTWORY. Skrzydła. Dodatkowe głowy. DWUCHIMERIAN BLAKENBAUERÓW!!! Leonidas wieje w las...

(potwór na 3 entropiczne)

### Sesja Właściwa - impl

* instruktor szkolący w kamuflażu i skradaniu -> Kić (ulubiony instruktor Leonidasa, który jest mistrzem skradania, stresował się niedźwiedziowiskiem, też była zestresowana) (Strużenka)
* kuzyn Leonidasa -> Żółw (specjalnie przyjechał by wesprzeć młodszego kuzyna, generalnie czekali aż wróci z dźwiedziowiska w kantynie, drzemka, rano światło słoneczne i cisza); Wędziwój (karmi ludzi).

Strużenka Verlen + Wędziwój Verlen.

Powinno być trąbienie na pobudkę. Nic. W kantynie nikogo nie ma, powinna być cisza. Mniejsze miasteczko z koszarami gdzie śpią dzieciaki, jest biuro pani sierżant. Z tego co Strużenka wie, ostatnio wszyscy się nie wysypiali. Wszyscy zaspali - anomalne, ale były takie problemy i nie wiadomo z czego wynika. Wtf?

Strużenka -> sierżant (ona jest dźwiedziem) i powinna mieć imię. Ciężka Łapa. Zawsze w biurze pani sierżant jest taka ciężka atmosfera (jakby coś przeskrobać) a słychać chrapanie. Nigdy nie było widać pani sierżant śpiącą - teraz widać. Marszczy brwi, śni jej się coś złego. Strużenka puka i wchodzi, nie zatrzymując się. Obudzi się? Poderwała łeb, ale przysypia z powrotem. Strużenka drze ryja "baczność".

Tp+3:

* VV: sierżant podskoczyła, "TAKJEST!", zorientowała się że nie jest w Koszarowie Chłopięcym, "plutonowa co jest!" (jest nieprzytomna)
    * S: pani sierżant co się tu dzieje? (szuka czegoś co da info o sytuacji) WSZYSCY śpią tylko nie ja i jeden mag.
    * Ł: to trzeba obudzić, trąbić w trąbkę!
    * S: TRĘBACZE TEŻ ŚPIĄ!
* X: Łapa wychodzi i próbuje obudzić wszystkich rutyną. W wolnym rytmie, jest spowolniona.
    * Mamrotała o Blakenbauerach i płaszczkach...
    * Spytała co z Leonidasem i wróciła do obowiązków.

Wędziwój idzie do dzieciaków. Kilka dzieciaków jest obudzonych. Bardziej zmęczeni niż W+S, ale obudzeni. Korzystają że nikt nie budził więc leżą.

* W: "czy tak zachowują się obywatele Verlenlandu! Wstajecie! Nie bądźcie Verlenlandrynkami jak mówią Blakenbauerowie!"

Wędziwój "specjalne kiełbasy na specjalne umiejętności". 

Tr +3:

* V: poderwani, wykonują podstawowe polecenia. Kilka osób zamarudziło, pani sierżant zrobiłaby to inaczej, ale wzięli się do roboty.
    * "KTO TU JEST MAGIEM! RUCHY RUCHY!"
    * (nie ma patternu, acz dużo osób z jednego domku - z domku Leonidasa)
* +Z (instruktorka) Vr: dzieciaki nie robią kłopotów, wierzą w operację specjalną.
* X: generalnie (użycie młodych jako "sieć neuronową" - co odróżnia tą sytuację od innych, co ich łączy)
    * jest kilka dzieciaków z domku Leonidasa, inni dobrze go znają. Czyli -> pattern jak blisko znają Leonidasa.
        * Leonidas nie jest specjalnie utalentowany, świetny w ukrywaniu się. Maskowaniu.

Ci ludzie przebudzeni stoją, da się zrobić by wykonywali co zawsze, ale to takie słabe.
Strużenka szuka Leonidasa. Ani jego ani dźwiedzi nie ma.

Strużenka wbija się w papiery i szuka planu dźwiedzi jak tępić Leonidasa. I tak planu nikt się nie trzyma, ale Palisadowspinacz i Lekka Stopa miały szkieletowy plan i gdzie miał się odbyć, na którym terenie. Wiemy mniej więcej co i jak.

Gdy przeszukiwane były papiery, autowieżyczki strażnicze zaczęły się uruchamiać. Coś zaryczało. Lecimy tam. Seria z autowieżyczek przeleciała po masywnej łapie zakończonej którą się zasłonił trzygłowy niedźwiedź. I chowa się w lesie. Dzieciaki widząc go zaczynają szeptać. Była legenda opowiadana przez dzieciaki przy ognisku o wielkim potworze grasującym i porywającym tych co się nie przykładają.

Biegamy dookoła, Wędziwój z koszulką Leonidasa (lub innymi artefaktami) i dzieciaki dookoła - szukamy czego szuka potwór

Tr Z +3:

* Vr: potwór reaguje na rzeczy silnie powiązane emocjonalnie z Leonidasem, szczególnie mocno na Wędziwoja i Strużenkę i inne dzieciaki.
* V: im bardziej Wędziwój linkuje z Leonidasem tym mocniej. Ale są rzeczy na jakie potwór nie reaguje to nie widzi. Np. kocyk. ZWYKŁY WOJSKOWY KOC LEONIDASA. Lub sproszkowany szczęśliwy nietoperz. I łapacz snów Leonidasa.
    * V: czyli rzeczy, które Leonidas uznał za swoje i służą do odpędzania zła / schowania przed złem

Wędziwój - mowa przekonywującą dzieciaki że to wszystko jest pod kontrolą, żadna płaszczka nie wygra a potem to samo z wężem ogrodowym i śpiącymi by ich oblać, zmyć z nich Paradoks (i zmienić pryzmat, osłabić siłę Paradoksu i wiążącej magii)

* V: sukces. Udało się osłabić stwora i Paradygmat.

Strużenka idzie szukać Leonidasa. Na pewno znajdzie dzieciaka. A Strużenka zawija się w kocyk, zawija się i wychodzi - potwór jej nie widzi. Bez kłopotu znajduje Leonidasa. Pytanie, czy dość dobrze i szybko by nie złamać mu morale. Strużenka sprawdza miejsca które zna, jak dobrze się schował. Ocenić jak się schował. (zmienił maskowanie, schował się w jakąś rozpadlinę między skałami). I siedzi z dzidą. Strużenka nie idzie głośno ale chce by Leonidas ją zobaczył i się nie wystraszył. Zatrzymuje się przed kryjówką. 

* S: "Leonidas?" 
* L: "pani Strużenka?" - wychyla się
* S: "dobra robota, schowałeś się całkiem skutecznie. Wyłaź."
* L: "nie ma go tu?"
* S: "nie ma w pobliżu niczego"
* L: (dźga by sprawdzić czy prawdziwa ale reaguje bo Verlen nie da się dźgać - wyjmuje kijek z ręki)
* S: EJ!
* L: "to naprawdę pani!" (przytula) "WIELKI POTWÓR BLAKENBAUEROWY! Musimy uciekać, opuścić teren, wszystkich nas zabije!"
* S: "mamy tylko jeden problem, nie możemy, bo nie możemy zostawić innych samym sobie"
* L: "ale on mnie pożre, będzie gonił i nie ucieknę potknę i zje jak zawsze to robi!"
* S: "za każdym razem? Nic Cię nie zjadło" (szturcha go)
* L: "za każdym razem we śnie, to coś..." "jest las... niedźwiedziowisko i to mnie goni, prawie widzę Koszarów, potykam się, dopada mnie... zasypiam i dzieje się to samo"
* S: "słuchaj, schowałeś się. Skutecznie. Nie znalazł Cię. Za to znalazł innych którzy nie umieją lub nie wiedzą że trzeba. Potrzebują pomocy."
* L: "oni potrzebują pomocy a jak ja prosiłem o pomoc to nie chcieli..."
* S: "to znaczy, że nie zasłużyli na wsparcie od VERLENA?"
* S: "oni się z Ciebie nabijali, ale będą Ci teraz wdzięczni."

.

Tr Z +4:

* V: "dobrze będzie widzieć jak inni są mi winni i widzą moją siłę"
* V: zauważył własne osiągnięcie, co osiągnął na niedźwiedziowisku. Czas na pep talk.
    * S: łączy "można się przestraszyć ale widzisz że możesz!", poprawia "odwaga to nie jest brak strachu!" i wprowadza "dźwiedzie próbowały pokonać potwora ale zostały zaskoczone - Blakenbauer przewidział dźwiedzie i dźwiedzie i koledzy potrzebują jego pomocy!", "i tak naprawdę Ty jesteś na zewnątrz a potwór jest w środku"
* X: będzie się chełpił nad innymi że ON ZROBIŁ A ONI GO POMOGLI. Zepsute relacje ale będzie mu lepiej.
* Vz: on to zrobi i będzie Prawdziwym Verlenem! "może nie pokonałeś niedźwiedzia, ale pokonałeś płaszczkę Blakenbauerów"

Wędziwój UWIERZCIE W CHAMPIONA! (nie mówi kto jest championem) SENTISIEĆ ODPOWIE.

* Vr: dzieciaki uwierzyły. Sentisieć / Pryzmat pomogą. Efekt potem będzie piorunujący.
    * (gdyby Wędziwój powiedział KTO jest czempionem, byłby ekstremalny XD)

Wędziwój ma przynętę - coś wartościowego dla Leonidasa. Niewidzialność kocyka - pod spodem przynęta. Położyć przynętę. Potwór się zbliży. Schować przynętę pod niewidzialność.

Tp:

* V: tylko fuksem potwór mógłby sobie poradzić. Mistrz wędlin Wędziwój pokazał czemu Fantazjusz go toleruje XD
    * Wędziwój doprowadził potwora do wodospadu

Strużenka widzi, jak Leonidas ściąga koszulkę (brak klaty). Kamuflaż sztucznej klaty. I czas na wodospad. PRAWDZIWY VERLEN POKONA POTWORA POD WODOSPADEM!!! Plan jest prosty - użyć wodospadu (wprowadzenie "nowej magii" wodą + zmycie "starej magii") + energia magiczna + sentisieć + to że to jego Paradoks + potencjalnie Twoja energia magiczna (nawet bez rzucenia zaklęcia) powinna zadziałać.

Z jednej strony Leonidas, z drugiej potwór. Leonidas nie może zobaczyć jak wielki ten potwór. Nie przez wodospad.

Tr (motywacja) Z (wierzą wszyscy) +3Vv +4 (ulubiona instruktorka):

* Vr: sukces - chłopak wkroczył widzi wielki cień jak narasta. Ale stoi twardo!
* V: wytrzymał wycie potwora, zanurzając głowę przez wodospad, woda opływa i w miarę wycia patrzą w oczy z dzieciakiem i potwór staje się coraz mniejszy
* V: NIE DAM SIĘ TOBIE! NIE POKONASZ! TY PRZEDE MNĄ UCIEKNIESZ! Dzieciak ryczy na potwora. Dwa dźwiedzie wpadają do wody. DALEJ RYCZY!!!

Dzieciak zabrany (trzeba było uspokoić, przezwyciężył największy lęk) i młody Leonidas wejdzie dumny że pokonał potwora. Ksywa "mokroklates". Mokroklates Verlen... nie jest z tego dumny, ale to stało się jego imieniem.

## Streszczenie

Kryzys pod Koszarowem, klasa: standard. Młody Leonidas miał swoje niedźwiedziowisko, trzecie podejście. Dźwiedzi komandosi robią dobrą robotę, ale panikujący młodzik Paradoksem tworzy Legendarną Blakenbestię. Dwaj Verlenowie w pobliżu - Wędziwój i instruktorka Strużenka - zajmują się tematem budując morale w Potworcu, próbując zrozumieć co się tu dzieje i ostatecznie konfrontując odnalezionego Leonidasa z Blakenbestią pod wodospadem, bo prawdziwy Verlen wchodzi pod wodospad walczyć z potworem.

## Progresja

* Leonidas Verlen: NOT a graceful winner. Chełpi się tym, że wszystkich uratował przez Blakenbestią. Ku żałości wszystkich dzieciaków, został tymczasowym bohaterem.

## Zasługi

* Wędziwój Verlen: ekspert od robienia wędlin z potworów, Verlen drugiej klasy; kuzyn Leonidasa i znajomy Fantazjusza. Gdy pojawiła się "Blakenbestia" sformowana Paradoksem Leonidasa, zebrał dzieciaki do kupy i dał im zajęcie, potem przekonał młodych by uwierzyli w "czempiona" (nie mówiąc że to Leonidas) i korzystając z "niewidzialności" spokojnie doprowadził "Blakenbestię" pod wodospad.
* Strużenka Verlen: instruktorka skradania na niedźwiedziowisku, Verlen drugiej klasy. Doszła do tego o co chodzi od sierżant Ciężkiej Łapy, znalazła Leonidasa i przekonała go, że da radę pokonać "Blakenbestię". Doprowadziła do konfrontacji Leonidasa z potworem pod wodospadem ("Verlen kontra potwór, półnago pod wodospadem").
* Leonidas Verlen: (12) trzeci raz na niedźwiedziowisku, dobry w chowaniu się, gdy się mocno przestraszył do jego Paradoks sformował "Blakenbestię" z dźwiedzich komandosów. Gdy się schował, instruktorka go znalazła i przekonała do skonfrontowana się z potworem pod wodospadem. Pokonał niedźwiedziowisko i został wreszcie dorosłym Verlenem. Rodzice chcieli wojownika a dostali, cóż, jego.
* Dźwiedź Palisadowspinacz: komandos na niedźwiedziowisku; asekuruje Leonidasa, jest "desygnowanym niedźwiedziem do pokonania", ale po wpadnięciu w pułapkę na tyle przestraszył Leonidasa, że Paradoks zmergował go z Lekką Stopą by zmienić ich w Blakenbestię.
* Dźwiedź Lekka Stopa: komandos na niedźwiedziowisku; złośliwa i bezczelna, skutecznie zastrasza Leonidasa nietoperzem i ratuje Palisadowspinacza po tym jak on wdepnął w pułapkę Leonidasa. Paradoks Leonidasa zmergował ją z Palidasowspinaczem w Blakenbestię.
* Dźwiedź Ciężka Łapa: pani sierżant w Potworzyku Strasznym; dotknięta Paradoksem Leonidasa, przysypia. Normalnie dowodzi młodymi Verlenami i Verlenlandczykami. Tym razem - przysypia.

### Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. Potworzyk Straszny: miejscowość niedaleko Koszarowa Chłopięcego, służące do niedźwiedziowisk i straszenia dzieci
                                1. Niedźwiedziowisko: ma wszystko co potrzebne - kontrolowane potwory, las, nietoperze...
                                    1. Wodospad Bohaterów: wodospad z wysokim przepływem wody, dość niebezpieczny (ale w warunkach kontrolowanych)
                                1. Namiotowisko: tam śpią młodzi ludzie, są wieżyczki i baraki dla dźwiedzi.

## Czas

* Opóźnienie: -121
* Dni: 2

## OTHER

.
