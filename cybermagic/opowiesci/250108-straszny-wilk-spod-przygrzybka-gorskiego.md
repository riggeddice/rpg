## Metadane

* title: "Straszny Wilk spod Przygrzybka Górskiego"
* threads: brak
* motives: the-protector, aurum-dylematy-i-konflikty, too-far-gone, dobry-uczynek-sie-msci, lojalny-do-konca, magowie-pomagaja-ludziom, szczur-zapedzony-do-kata, corrupting-parasite, kult-mroczny, energia-ixion, energia-interis
* gm: żółw
* players: kić, fox, scholla

## Kontynuacja
### Kampanijna

* [240815 - Weterynarz i Verlen w Przelotyku](240815-weterynarz-i-verlen-w-przelotyku)

### Chronologiczna

* [240815 - Weterynarz i Verlen w Przelotyku](240815-weterynarz-i-verlen-w-przelotyku)

## Plan sesji
### Projekt Wizji Sesji

* INSPIRACJE
    * PIOSENKA WIODĄCA
        * [Blutengel - Our Souls Will Never Die](https://www.youtube.com/watch?v=kipz75KvYvY)
            * "We walk into the fire | But we don't feel the flames | We fight against our Demons | We won't surrendеr to this pain"
            * "But you're afraid and you feel paralyzed | You are not alone | I'm here to show you the way"
            * "We cry we love | Our souls will never die we fight | We cry we love | Our souls will never die"
            * Nieskończona lojalność Wilka. Jego Blakenbauer nigdy nie umrze i nie stanie się elementem Kultu Saitaera.
    * Inspiracje inne
        * Denma, początek z lojalnym cyberpsem
        * "BRONIĆ STUDNI!"
        * Dog in the fog
        * MTG: "Selfless Savior" flavor text: "She raised him from an orphaned pup and gave him a life of love. With his last act, he thanked her."
* Konwencja
    * Książki z cyklu "Wiedźmin" Sapkowskiego. 
        * Przybywamy na miejsce, gdzie jest potwór, ale okazuje się, że dzieje się tam więcej niż się wydaje.
        * Triggery: cierpienie ludzi (włączając nieletnich), śmierć ludzi (włączając nieletnich), body horror (z niskim opisem)
        * Postacie Graczy: raczej bezpieczne, chyba, że zrobią coś nierozsądnego; bardzo silne.
* Opowieść o (Theme and Vision):
    * "_Czasami ci, których próbujemy chronić stanowią największe zagrożenie._"
        * To ludzie należący do Kultu Saitaera są zagrożeniem dla magów którzy próbują im pomóc w tym niegościnnym terenie.
    * "_Miłość. Oddanie. Pies nigdy nie zdradzi swojego właściciela. Pies wykonuje zadanie do końca. Blakenbauer miał rację._"
        * Tomfoer, czarno-srebrny entropiczny wilk, chroni swojego Blakenbauera i zrobi wszystko by mu pomóc.
        * Mimo, że Ixion zaczyna przejmować nad nim kontrolę, Interis utrzymuje Ixion z dala od rdzenia Tomfoera.
    * "_Możesz zaufać potworowi i zachowa on lojalność, ale pamiętaj, że nadal jest potworem._"
        * Tomfoer jest istotą Interis. Jest niesamowicie niebezpieczny i nie dba o niczyje życie.
        * Jednocześnie Tomfoer dba o swojego Blakenbauera i robi wszystko by go uratować.
    * Lokalizacja: Przelotyk
* Motive-split
    * energia-ixion: Kult Saitaera sprawia, że prefektiss był w stanie zapewnić transorganiczne dary dla pewnej części wioski. Ludzie trwają dłużej i bezpieczniej. Umierają mniej i są bezpieczniejsi.
    * energia-interis: Pole energii Interis jest w okolicy Przelotyka; protezy się psują, elektronika ledwo działa, ludzie chorują i są słabsi. Właśnie to sprawia, że ludzie kierują się ku ixionowi by przetrwać i to sprawia, że Szałwiusz Blakenbauer (próbujący neutralizować Interis by pomóc ludziom) działa na tym terenie.
    * kult-mroczny: Kult Saitaera; część populacji Przygrzybka oddała się kultowi Jego Ekscelencji, by nie umrzeć w morderczych warunkach.
    * corrupting-parasite: zaprojektowany podczas Inwazji Saitaera prefektiss przejmujący kontrolę nad magiem. Gdyby nie Tomfoer, Szałwiusz Blakenbauer stałby się elementem Kultu Saitaera.
    * szczur-zapedzony-do-kata: Szałwiusz Blakenbauer i Tomfoer. Nie mają jak uciec, nie mają jak sensownie z tego wyjść. Tomfoer zrobił wszystko, by tylko ściągnąć innych magów i uratować swojego Szałwiusza.
    * magowie-pomagaja-ludziom: Szałwiusz (próbował usprawnić lokalne miasteczko) a potem Verlenowie, którzy przybywają uratować ludzi przed Tomfoerem. Ogólnie, magowie to pozytywna siła.
    * lojalny-do-konca: Tomfoer d'Blakenbauer. Mógł odejść. Mógł zostawić. Miał dość wolnej woli. Mógł też zniszczyć wioskę. Ale został wierząc, że da się uratować Szałwiusza Blakenbauera.
    * dobry-uczynek-sie-msci: Szałwiusz przybył tu pomóc lokalnym ludziom, nie wiedząc o kulcie. Oni nie wiedzieli o Tomfoerze - a dokładniej, co Tomfoer potrafi.
    * too-far-gone: zdaniem większości, Tomfoer. Skażony ixiońsko, częściowo interient. Nie pozostało w nim wiele więcej niż pragnienie obrony swojego Blakenbauera (Szałwiusza). A jednak..?
    * the-protector: Tomfoer d'Blakenbauer; Skażony ixiońsko wilk entropiczny. Chroni swojego Blakenbauera, który został Skażony ixiońsko przez prefektissa.
    * aurum-dylematy-i-konflikty: Co powinni zrobić Verlenowie? Tomfoer zachował się właściwie, zrobił co powinien. A jednak jest potworem, niekontrolowanym, zabijającym też niewinnych ludzi. Plus, problem z Mariolą, która jest ixientem, ale wyraźnie pomaga ludziom. Więc... zniszczyć?
* O co grają Gracze?
    * Sukces:
        * Zniszczą Kult Saitaera lub go odpowiednio zneutralizują. Jak? Zależy od nich. Też: casus Marioli.
        * Uratują Blakenbauera.
        * Rozwiążą problem Wilka. Jak?
    * Porażka: 
        * Verlenki wyjdą na nie-Verlenowate. Wyjdą na takie, które nie pełnią Verlenowej roli.
        * Ixienty są strasznie niebezpieczne. Jakiś ixient zdolny do infekcji pozostanie.
* O co gra MG?
    * Highlevel
        * Pokazać Scholli jak to wszystko działa. Dużo Stereotypów, klocków i komponentów EZ.
        * Pokazać, czemu Blakenbauerowie są tak potężnym i niebezpiecznym rodem. Tomfoer jest KOSZMARNY.
        * Pokazać, że między Rodami - nawet wrogimi - jest możliwość dogadania się. Pomóżmy ludziom za wszelką cenę.
        * Pokazać dylematy międzyrodowe. Dlatego m.in. wprowadzam Smoczkogryza Verlena. Twardego łowcę potworów i obrońcę ludzi, dość nieugiętego w zasadach.
    * Co uzyskać fabularnie
        * Niech teren będzie uznany za "Skażony i niebezpieczny".
        * Niech Tomfoer zostanie zabity a Blakenbauer przeżyje. I niech to zrani jego serce wobec przyjaciółki z Verlenlandu.
        * Niech rozerwie się link między Szałwiuszem Blakenbauerem i Ragną Verlen.
* Agendy
    * .

### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

KONTEKST:

* Przygrzybek Górski jest miejscowością w Przelotyku, w okolicy Verlenlandu, dość blisko obszaru wiecznych mrozów.
* Przygrzybek Górski miał stałe problemy powiązane z plagami, zarazami i brakiem energii. To wynika z pola Interis.
* W Przygrzybku Górskim pojawił się Kapłan Saitaera, który zaczął konwertować przygrzybkowców na Kult Saitaera. To dostarcza nowej Energii - Ixion.
* Jednocześnie na terenie działa Szałwiusz Blakenbauer, który szuka roślin i sposobów wchłaniających Interis. Który pomaga wielu miejscowościom.
    * Szałwiusz znalazł i oswoił Tomfoera lata temu - bardzo niebezpieczną istotę. Tomfoer umierał, ale Szałwiusz uratował mu życie, dużym kosztem personalnym. Po raz kolejny, arogancja Blakenbauera wygrała.
* W końcu Szałwiusz dotarł do Przygrzybka. Zaczął pomagać temu miejscu.
* Za którymś razem, Szałwiusz został zaatakowany przez odpowiednio działającego prefektissa. Pierwsze co przestało działać to hipernet.
* Tomfoer zniszczył prefektissa i wycofał Szałwiusza. Ale szkoda się stała. Szałwiusz został zainfekowany ixiońsko.
* Tomfoer żywi się ludźmi, wysysając z nich Energię i zasila nią Szałwiusza. Ale to sprawia, że Ixion wpływa też na niego (sympatia). Interis pożera Tomfoera z drugiej strony. Tomfoer jest coraz mniej sobą, coraz bardziej interientem.
* Tomfoer pamięta. Chronić swojego przyjaciela. Selfless Savior.

CHRONOLOGIA:

* jak powyżej

PRZECIWNIK:

* Kult Saitaera w Przygrzybku Górskim, pragnący zainfekować i przejąć kontrolę nad magami
* Tomfoer, entropiczny srebrno-czarny wilk Blakenbauerów
* Smoczkogryz Verlen, sympatyczny ale twardy i osadzony w zasadach Verlenów wojownik

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

* Tomfoer docelowo stanie się potworem Interis. Zabraknie reszty osobowości. Odda wszystko by chronić swojego Blakenbauera.
* Smoczkogryz zabije Tomfoera.
* Pojawi się inny Blakenbauer i uratuje umierającego Szałwiusza, ale Szałwiusz będzie miał kilka miesięcy do odkażenia. I nie będzie już taki sam.
* Przygrzybek Górny zostanie powoli konwertowany dalej przez Kult Saitaera

### Fazy sesji

FAZA 0: Prezentacja Tomfoera i magii

* Tonalnie
    * Akcja, kombinowanie oraz groza z działania Tomfoera.
    * Nie każdy Verlen i Blakenbauer muszą walczyć.
* Wiedza
    * Jak działa magia
    * Tomfoer i działania z mgły
    * Wenapnici - raiderzy
    * Szałwiusz, który robi eksperymenty z roślinkami
* Kontekst postaci i problemu
    * Siła ognia Verlenki, umiejętności skradania się.
    * Jak skuteczni i potężni są magowie.
* Implementacja
    * Grupa ludzi ma Blakenbauera na celowniku, ten wygląda na dość nieuzbrojonego.
    * Blakenbauer proponuje im, żeby się wycofali.
    * Są tam roślinki na których są wyjątkowo obrzydliwe tłuste larwy, które Blakenbauer wykorzystuje by wesprzeć roślinki.

FAZY: Atak Tomfoera -> Przybycie Smoczkogryza -> Konfrontacja

| **Frakcja**          | **19:45 - 20:45**                    | **21:45 - 22:45**                   | **22:45 - 23:45**             |
|----------------------|--------------------------------------|-------------------------------------|-------------------------------|
| Kult Saitaera        | przygotowanie do obrony              | uciszanie normalsów i budowa kultu  | lokalny rytuał ochronny       |
| Normalsi Przygrzybka | panika                               | podniesiona presja; próby -> Ragna  | Rozpacz wygrywa; Wenapnici próbują pomóc. |
| Tomfoer              | eksterminacja dziecka i karmienie B. | brak                                | Tomfoer leży i osłania Blakenbauera. Ale interient staje przeciw Smoczkogryzowi. |
| Smoczkogryz          | w obszarze karawany, szuka śladów    | wraca, pyta o smoka                 | Smoczkogryz konfrontuje z Tomfoerem. |

.

* PRZESZŁOŚĆ: Niedawno Tomfoer zrzucił lawinę na pancerną karawanę i zabił jedną osobę w chaosie. Stąd wezwanie Ragny, Arianny, Vioriki.
* PANIKA NORMALSÓW: "Przecież tu nie zaatakuje, prawda? Nie ma jak, prawda?"
* Zabite dziecko było ixientem, które było ciężko chore (jakaś lokalna choroba). Ixient je zasilił. Tomfoer uniknął drugiego dziecka, rodzica by zabić dziecko.
* .

OGÓLNIE:

* Przygrzybek posiada grupę wenapnitów, którzy zostali zniewoleni i pokonani przez ixienty.
* Tomfoer sprowadza lawiny i atakuje skutecznie jeśli postacie idą w tamtą stronę.
* Tomfoer ma elementy mechaniczne, obwody w lewym oku. A to na pewno jest istota Interis-Anteclis.
* Farma Grzybów Jaskiniowych jest w świetnym stanie. Są tam larwy Blakenbauera. Czyli był tu.
    * Korina: "Nagle Tomfoer zaatakował, w nocy. Zaatakował Blakenbauera."

## Sesja - analiza
### Fiszki
#### Normalsi, których zna Ragna

* Korina Serniczkow: starsza lekarka z protezami i wszczepami, która NAPRAWDĘ powinna unikać zimna; walczy nieustępliwie o każde życie tym, do czego ma dostęp.
* Walery Oprychowski: prospektor i zwiadowca; bada teren, zastawia pułapki i monitoruje zarówno ruchy wenapnitów jak i lokalnej groźnej fauny.
* Natalia Rosomicka: burmistrzyni, świetna ekonomistka i administratorka, która dba o Przygrzybek. Odkąd przybyła tu z Aurum, nigdy nie opuściła Przygrzybka.
* Aktein Karr: wyglądający jak szkielet, wiecznie opatulony w futra muzyk oraz bajarz. Jest tu od zawsze i zna mnóstwo ciekawych opowieści spoza planety.

#### Kultyści, których zna Ragna

* Kamil Orpaczek: farmer grzybów, bardzo pracowity i pomocny ojciec Mariusza i Zosi, który zrobi wszystko by dać dobrobyt dzieciom (a Mariusz jest na jakąś lokalną chorobę przewlekle chory).
* Jan Krzyczawiec: potężnie zbudowany mechanik, ma dostęp do monitoringu i systemu obronnego. Niezbyt ufny i niezbyt rozmowny, ale dla Przygrzybka zrobi bardzo wiele.
* Mariola Glewisen: charyzmatyczna kiedyś-oficer, teraz dowodzi obroną Przygrzybka. Doskonale się trzyma jak na swój wiek. Trudna do wyprowadzenia z równowagi.

#### Normalsi, których nie zna Ragna

* Arnulf Burczomił: farmer grzybów; pragnie opuścić ten teren, boi się magii i Rozpaczy a teraz jeszcze potwora; coś się tu dzieje i on nie wie co.
* Renata Szeleśnik: młoda inżynierka, która nie wie co się tu dzieje ale uważa, że dzieje się coś bardzo złego.
* Erwin Limirin: wenapnicki bandyta, który został zmuszony do dołączenia do Przygrzybka

#### Kultyści, których nie zna Ragna

* Rodencja Bemyszcz: młoda barmanka, która zrobi wszystko by się stąd wyrwać. Buzia jej się nie zamyka. Nie jest Skażona.

#### Magowie

* Smoczkogryz Verlen: Verlen łowca potworów, w ciemnym płaszczu i Chevalierze.
    * Solutor Monster Hunter, specjalizujący się w walce z potworami i kontroli terenu przez Praecis.
    * Mroczny Samotnik: O+ C+ E- A- N+; (Anime Dark Protagonist, bardzo przywiązany do zasad Verlenów i do zabijania potworów; sympatyczny, ale wyobcowany).
* Szałwiusz Blakenbauer: zdystansowany ale dość sympatyczny badacz Energii, Anteclis + Interis (co nietypowe). Uratował kiedyś życie Tomfoera.
    * Badacz i implementer bioform w miejscach ogarniętych Interis przy Przelotyku. Specjalizuje się we wszystkim co pełza i dotyka Interis.
    * Ciekawski Eksplorator: O+ C+ E+ A- N-; (Blurr). Ma dobre serce, chce pomóc ludziom. Szybko mówi i niechętnie walczy. Kocha każde życie. Ekspresyjny.

***
***

### Scena Zero - impl

Teren wzgórzysty, trawy, jaskinie, zimno. Ragna jest w Przelotyku i słyszy głośną rozmowę dość daleko w miejscu, gdzie nie powinno być ludzi. Próbuje zajść ludzi tak, żeby jej nie zauważono. Zachodzi od boku.

Ex+4:

* X: Coś monitoruje.
* V: (+2Vg) Ragna coś widzi. A dokładniej - widzi dziwnego, niemal niewidzialnego "wilka" (imieniem Tomfoer). Wilk wyraźnie poluje na tych ludzi i ją obserwuje.

Stwór widzi, że ona go widzi, inteligentnie się przesuwa. TAK, tam rozmawiają ludzie. 

Trójka ludzi (na oko, lokalnych) grozi czwartemu (na oko, magowi - tienowi Aurum), który się nie boi. Ewidentny napad rabunkowy. Mag nie boi się ich. Więcej, wręcz ostrzega ich, że nie mają pojęcia, co się tu dzieje. I gra na czas.

Ragna ujawnia się grupie. "Rabowany" ją rozpoznaje jako Verlenkę, ona jego po chwili jako Blakenbauera. Ragna podchodzi do trzech bandziorów i rekomenduje im wycofanie się, gdy zaczęła zapadać mgła. Napastnicy orientują się, że wybrali sobie na ofiarę maga i właśnie napatoczył się drugi. Więc - się wycofali się bardzo szybko. Ragna natychmiast przestaje celować w Blakenbauera. Mgła - bardzo powoli - zaczyna gasnąć. Blakenbauer skupia się na swojej roślince.

Gdy Ragna zaczyna się interesować roślinką i tym co Blakenbauer robi, Blakenbauer jest PRZEKONANY że Ragna się z niego nabija. Ragna przykuca przy roślince i jej się przygląda. Absolutnie normalna roślina - W LEPSZYM stanie niż zwykle. Blakenbauer powiedział, że próbuje poradzić sobie z Energią Interis i do tego celu wykorzystuje specjalnie przygotowane larwy. O dziwo (z jego punktu widzenia), Ragna zamiast niszczyć życie - interesuje się badaniami.

Więc Szałwiusz Blakenbauer rzucił interesujący tekst:

* "Interesujesz się larwami?" - naiwnie, z rozpalonymi oczami
* "Od teraz chyba tak!" - nieco zagubiona, ale zaciekawiona Ragna Verlen

### Sesja Właściwa - impl

Arianna, Viorika i Ragna zbliżają się do Przygrzybka Górskiego, maleńkiej miejscowości w Przelotyku, za to świetnie ufortyfikowanej i częściowo wbudowanej w skałę.

Podobno Blakenbauer zaatakował to miejce, a jego potwór zabił już nawet ludzi. Srebrny Wilk zaatakował karawanę i porwał człowieka. Były nagrania z ataku srebrnego wilka na karawanę. Na tym terenie jest już Smoczkogryz Verlen; poszedł zbadać obszar karawany i problemów w tamtym obszarze. A Arianna, Viorika i Ragna skupiają się na miejscowości. Arianna i Viorika, bo trzeba zniszczyć potwora. A Ragna, bo doskonale zna ten teren i chce pomóc przyjacielowi (Szałwiuszowi Blakenbauerowi).

Na wejściu postanowiły udać się do pani burmistrz dowiedzieć się, o co chodzi. Natalia Rosomicka - burmistrzyni - wyszła do nich z nimi porozmawiać. Jest nieco przestraszona (TRZY Verlenki???), ale obecność Ragny ją troszkę uspokoiła.

Natalia wyjaśniła co mają i co się stało:

* Blakenbauer spał spokojnie w Barze Górskim; Rodencja jest dowodem.
* Nagle pojawiła się mgła. Bardzo szybko.
* (Na kamerach nagrane) we mgle pojawia się wilk, celuje w randoma z farmy grzybów (Alfreda) i go ciężko gryzie. Potem przechodzi przez mgłę.
* (Podobno, z opowieści Rodencji) pojawia się wilk w barze, atakuje Blakenbauera i przechodzi przez mgłę. Wpierw na plac, z nieprzytomnym Blakenbauerem. Potem zniknął gdzieś we mgle.

Wilk zaatakował Alfreda w tchawicę. Co się stało z ciałem? Ciało zostało spalone z rozkazu Koriny (lekarki).

Viorika pyta Natalię, co Blakenbauer tu robił. Ale naprawdę to Viorika szuka punktu stresu. Czy coś jest takiego że jest niekomfortowe dla Natalii? Czy Natalia coś ukrywa? Coś wie?

Tp+3:

* V: W oczach Natalii: "Szałwiusz próbował pomóc, badał larwy ale coś było nie tak i wilk oszalał."
    * Natalia się martwi czy te larwy mogą zrobić krzywdę i czy z nimi jest coś nie tak.
        * Verlenki nie mają pomysłu jak to wygląda i co jest.
    * Natalia jest zestresowana, bo chwali Blakenbauera przed Verlenami - a przecież Verlenowie i Blakenbauerowie są w stałym konflikcie.

Ragna zdecydowała się na szybkie połączenie hipernetem z Szałwiuszem. Nie udało się, niestety - sygnał jest uszkodzony. To znaczy, że coś z Szałwiuszem jest nie tak. Ale żyje i funkcjonuje. To jednak zdecydowanie podnosi presję i ryzyko sytuacji. To sprawiło, że Ragna poszła z Arianną by przesłuchać Rodencję. A raczej - by poszukać krwi w barze. Jeśli Wilk zaatakował Blakenbauera, muszą być tam ślady krwi. Na pewno tego idealnie nie wyczyścili.

A Viorika w tym czasie próbuje dowiedzieć się kim był Alfred od ludzi losowych, jego krewnych i znajomych. Czemu Wilk zaatakował właśnie Alfreda?

Rodencja, która spotkała Ariannę ma gwiazdki w oczach. Błaga Ariannę, by ta wzięła stąd Rodencję. Rodencja jest przerażona tym wszystkim co się stało. Podczas rozmowy ogólnie potwierdziła co się tam stało:

* Blakenbauer spał, O TAM.
* Zaatakował go Wilk. Rozerwał Blakenbauerowi tchawicę, po czym go wziął i się teleportował przez mgłę. Była szamotanina.

Tymczasem Viorika odpytuje ludzi o Alfreda:

Tp +3:

* Vr: Alfred był człowiekiem, który najbardziej współpracował z Blakenbauerem i najbliżej był larw. Blakenbauer jego przygotowywał do roli "opiekuna larw".
    * Wilk celował w Alfreda. Były różne osoby. Ale Alfred był jego celem. Wilk omijał innych ludzi.
* V: Alfred był dobrym człowiekiem, bardzo ciekawski. Był dość blisko Blakenbauera.
    * Ogólnie, ludzie boją się iść do grzybów bo tam są te larwy i boją się, że Wilk wróci.
    * Alfred dostawał od Blakenbauera środki - inhibitory Skażenia. Bo mógł się Skazić przy użyciu larw.
    * Pani doktor przeprowadziła autopsję a potem kazała spalić ciało.

Ragna tymczasem szuka krwi Blakenbauera. Ale jak? Najpierw - podłoga. Pewnie podłoga nie jest wypucowana sterylnie. Sprzęt Ragny ma dobre detektory śladów biologicznych, zaczyna więc od tego. Wspiera się poważnie opowieścią Rodencji - gdzie spał Blakenbauer, gdzie powinny być ślady biologiczne.

Tr Z +3:

* X: NIE MA śladów biologicznych. W ogóle. To nie jest możliwe.
    * Okazuje się, że to się stało na materacu. NAJLEPSZYM materacu.
        * Rodencja: "Przecież mag nie będzie spał na niewygodnej ziemi!"
        * Verlenki: "...ok?" (one sypiają na ziemi XD)
    * Więc Rodencja zapewniła dostęp do EPICKIEGO WYGODNEGO MATERACA na którym spał Blakenbauer i w którym powinna być jego krew.
* X: Materac został DOBRZE wyczyszczony antymagicznie. To jest poziom "znam się na rzeczy, usuwam ślady magiczne".
    * Rodencja nie mogła być osobą, która to wyczyściła. Ona nie ma wiedzy. Jest zbyt... normalną barmanką.

Ale Ragna miała dużo do czynienia z Blakenbauerem. Robili dużo rzeczy razem. Ragna dobrze zna Szałwiusza. Więc Ragna, wiedząc, że nie da się idealnie wyczyścić krwi z materaca - zdecydowała się użyć potęgi swojej magii. Dojść do próbki krwi Blakenbauera.

Ragna wydobywa z torby proszek, posypuje materac i w momencie gdy działa to wyłaniają się krople krwi nad materacem. Reaktor serwopancerza jako źródło.

* Ob: Na końcu Konfliktu zaczyna pojawiać się mgła. Wilk ma portal do przejścia.
* Vz: W materacu nie było już krwi. ALE nie ma to znaczenia - Ty wydobyłaś sygnaturę. Sympatia zadziałała.
* Ob: To już nie jest tylko mgła. Wilk ma pierwszy ruch. Mgła dotyczy całego miejsca. Ona jest wszędzie w mieście.
* Vr: Tam jest nie tylko krew Blakenbauera. Tam są inne sygnały i sygnatury. Energia Ixiońska. Ale skąd?! IXION?!

Wilk się pojawia. Niedaleko Vioriki, na placu. Wilk jest w gorszym stanie niż był na kamerach; kieruje się wyraźnie w stronę jednej uciekającej rodziny. Uniknął dziecka, ale próbuje skoczyć w stronę INNEGO dziecka, mimo, że jego ojciec próbuje desperacko to dziecko zasłonić.

Viorika przygotowuje serię z broni maszynowej. NIE STANIE SIĘ TO. Nie przy niej w pobliżu.

Ragna wykorzystuje magię; chce przejść przez Mgłę Wilka, chce spotkać się z Tomfoerem. Przecież ona zna tego Wilka. Jest szansa, że go powstrzyma lub że zobaczy co się dzieje. (+2Vg).

* Xb: Ragna weszła w mgłę. Prosto przez Mgły Interis. Nie tylko jest w SZOKU ale też jej serwopancerz się rozpadł; Entropia go pochłonęła. Więc Ragna jest nieuzbrojona, lekko ubrana, w szoku i na linii ruchu Wilka.

Viorika nie pozwoli na to. Nie pozwoli na to za żadne skarby. Przycelowuje i strzela serią w Wilka. Musi odwrócić uwagę Wilka od Ragny...

Ex Z +4:

* V: potężna seria z broni maszynowej Vioriki by odwrócić uwagę od Ragny; mimo Mgły i ogólnej nieciągłości istoty Interis.
    * Wilk dostał serią broni maszynowej. Ból go otrząsnął. W oczach Wilka pojawiło się zrozumienie.
    * Ragna zobaczyła w oczach Wilka ślady ixionu. Czyżby Wilk był też zainfekowany ixionem?!

Arianna używa swojej magii. Chce podnieść istniejące zaklęcie Ragny i zdecydowanie wzmocnić sygnał. Niech Wilk stanie się sobą. Niech Sygnał dotknie Blakenbauera.

* X: Arianna poczuła straszne zimno, wysysa ją to energetycznie, praktycznie pada półprzytomna, ale poczuła... ZADRĘ. Coś w niej jest. Coś jest w Wilku, pasożyt. Na szczęście, Entropia zawsze wygra (Wilk jest istotą Interis).
* X: Arianna jest zdjęta z akcji; zaklęcie się odwróciło. Teraz to energia Arianny zasila Wilka; zdecydowanie nie jest to przyjemne.

Ragna zneutralizowana. Arianna zneutralizowana. Viorika jest ostatnim, co stoi pomiędzy Wilkiem a tamtym dzieciakiem. Krzyczy "Zły pies!" i korzysta z okazji - przecina kabel zasilający, by móc porazić prądem Wilka; powinno zadziałać skuteczniej niż broń kinetyczna. (+1Vg)

* X: Viorika skutecznie przecięła kabel. Niestety - Przygrzybek jest miejscem z aurą Interis, zwłaszcza w aktualnej formie. Viorika zrobiła wszystko prawidłowo, ale kabel niegroźnie uderzył Wilka. Nie ma prądu. Ogólnie, nie ma prądu w części Przygrzybka przez nadmierną energię Interis. Coś poszło nie tak. 

Ragna używa mocy magicznej by użyć krwi Blakenbauera by wzmocnić Tomfoera. Niech stanie się sobą.

* X: Tomfoer - MIMO działań Ragny - nadal skupił się na młodym Mariuszku. Wyssał energię z dzieciaka i zniknął we mgle.
    * Tomfoer postarzał dziecko o ponad 100 lat jednym ugryzieniem.
* Vr: Ragna wzięła krew i wysłała prosto w Tomfoera. POZNAŁ ją.

Ogólnie - PANIKA w mieście. Z ich perspektywy 3 Verlenki zostały pokonane przez Wilka. To bardzo, bardzo źle.

Viorika ma trochę krwi wilka. W końcu go trafiła karabinem maszynowym.

STAN AKTUALNY - WILK ROZPOZNAŁ RAGNĘ ALE NIE ZMIENIŁ DZIAŁAŃ. 

* Coś zainfekowało wilka ixionem?
* Blakenbauer nie kontrolował Wilka wcześniej. Co się stało z tym Wilkiem? Czemu tak się zachowuje?
* Medyczka - zamieszana w maskowanie tego co było?
* Była ofiara tego wydarzenia. Ofiara to Mariusz.

Pytanie - czy Blakenbauerowie nie byliby zainteresowani uratowaniem pobratymca? Byliby, ale jest za daleko.

Dobra. Nie można pozwolić na to, by pojawiła się panika, mimo, że lokalne siły próbują opanować sytuację. Viorika przejmuje dowodzenie. Wydaje polecenia. Zająć się rodziną która została poszkodowana. By Rodencja podała kocyki - pomoc psychologiczną.

Ragna skupia się na ciele Mariusza. Jest duża raz gryziona próbka. Ragna ma hipotezę - jeśli w Wilku jest Skażenie ixiońskie... to może w dziecku też jest ixion? Więc na tym się skupiła.

Tp +4:

* Vr: Ragna weszła w próbkę tego, co było Mariuszem. I znalazła ixionformę w stanie atrofii. 
    * Mariusz był chory. Bardzo słaby przez interis. Stopniowo jego organy były wyłączane; nie dożyłby 20 roku życia.
    * Jego ciało zostało częściowo zastąpione prez transorganikę ixiońską. Dzięki temu byłby w stanie żyć dłużej. Ale... wzmocnienie jest pasożytem ixiońskim.
* V: Jednocześnie udało się Ragnie zauważyć, że mózg Mariusza był częściowo pod kontrolą. Mógł zostać przejęty.
    * Ten byt miał wbudowane mechanizmy maskowania.

Arianna rozmawia z Rodencją, znowu. Barmanka - 100% terroru. ZABIERZ MNIE STĄD!!! Rodencja pyta, czy jest jakakolwiek nadzieja? Czemu Wilk tu jest? Czemu atakuje? Czy da się go powstrzymać? Czemu tu atakuje? Arianna - nie wiemy ale się dowiemy. Zajmiemy się wilkiem. I Arianna dokładnie przepytuje Rodencję o wszystko, co widziała i co tam się działo. Kto kazał wyczyścić materac? Kto to zrobił? Rodencja odpowiedziała - z rozkazu Marioli (przełożonej sił ochrony). Ariannie jednak kilka faktów się nie zgadza, więc dokładniej przepytuje Rodencję co się stało w nocy, gdy Wilk zaatakował Blakenbauera.

Tp +3:

* V: Rodencja jest w całkowitej panice. Nie chce powiedzieć Ariannie, bo nie wierzy, że dobrze widziała, bo to nie ma sensu co widziała.
    * Wpierw, Blakenbauer się zaczął krztusić. Ale jak? On spał. Przecież nic do niego nie przypełzło i nie weszło mu w usta, prawda?!?!?!
    * "On się krztusił". Blakenbauer coś połknął. Krztusił się. Walczył z tym - ona pobiegła mu pomóc, ale on ją odepchnął. WTEDY pojawiła się mgła.
    * Pojawił się Wilk. Ona się schowała, ale widziała, jak Wilk zaatakował gardło Blakenbauera.

Arianna wierzy w to co się tu stało. Jakkolwiek trudno jest uwierzyć, że dało się potencjalnie zainfekować czymś _Blakenbauera_, ale istnieją takie pasożyty ixiońskie. Rodencja sama by tego nie wymyśliła, a to tłumaczy zachowanie Wilka. To też tłumaczy, czemu Rodencja jest aż tak spanikowana - między Wilkiem a tajemniczym pasożytem ixiońskim.

Dobrze. Arianna skupia się na komunikacji z Smoczkogryzem. Dobrze mieć tu kompetentnego Kuzyna. Smoczkogryz z sympatią przywitał Ariannę. Powiedział, że jakkolwiek jest w stanie pokonać Wilka Interis, to jednak nie wszystko jeszcze rozumie. Zbadał karawanę i ruchy Wilka w okolicy.

Arianna powiedziała, że Wilk uderzył w Przygrzybek. Smoczkogryz się zdziwił, że to było możliwe. Arianna zauważyła pole Interis w okolicy. I powiedziała o obecności ixientów. TERAZ Smoczkogryz się serio zmartwił. Hipoteza Arianny - Wilk Interis atakuje istoty ixiońskie by chronić ludzi w imieniu swojego Blakenbauera. Ale Smoczkogryz powiedział, że nie znalazł śladów ixiońskich w karawanie zniszczonej przez Wilka, znalazł zasilanie się energią życiową ludzi. Ale zabezpieczy zwłoki, zapewni siły bojowe i szybko wróci do Przygrzybka. Ixienty atakujące miasto z ludźmi mają najwyższy priorytet.

Viorika i Ragna odwiedzają lekarkę, Korinę. Korina, starsza osoba z licznymi protezami, ciepło przywitała smutną Ragnę (która jest przekonana, że Korina jest ixientką). Ragna poprosiła o informacje czemu Korina kazała spalić ciało Alfreda. Korina zrobiła bardzo dokładną autopsję i wykryła ciągłe wysysanie energii życiowej przez Interis; kazała zniszczyć ciało, by to się nie rozprzestrzeniło. Nie wie wiele o tym jak dokładnie działa magia, więc robi co może.

Ragna dokładnie przeanalizowała autopsję którą zrobiła Korina. Dobra robota. Faktycznie, nic nie wskazuje by Alfred był zainfekowany ixiońsko... co pokazuje, że Wilk miał inną agendę atakując go. Czyli atak był z innego powodu niż na Mariusza. Potwierdza się hipoteza Smoczkogryza, który uważa Wilka za niebezpieczne wściekłe zwierzę..?

Tak czy inaczej, Ragna ma problem. Chce przebadać Korinę i innych, ale potrzebny jej detektor ixioński. Ale jej serwopancerz przestał istnieć po tym, jak Mgła Interis go zniszczyła. Więc - używa magii (używając resztek Mariusza jako substratu wraz ze sprzętem Koriny) by stworzyć sprawny detektor.

Tr M +3 +3Ob: 

* X: Skaner będzie jedynie stacjonarny. W tym pomieszczeniu.
* Vr: Mamy skaner. Działa. Robi co powinien.

Ku uldze Ragny, Korina jest czysta. Nie jest Skażona. Ale na pewno, NA PEWNO cierpi przez kiepskie, skorodowane przez Interis protezy.

Korzystając z okazji, Viorika sprawdziła też ciągle łażącą za Arianną Rodencję. Ta też jest czysta.

Viorika ściąga więc oddział. Przejmuje kontrolę siłą. Ale wpierw bierze Mariolę do przebadania. Mariola nie chce iść, bo jest potrzebna swoim ludziom. A Viorika zmusza ją do przyjścia. Bo nie może im zaufać i nie może powiedzieć Marioli dlaczego ta ma przyjść publicznie.

Tr Z +4:

* V: Mariola przekazuje dowodzenie dalej, podnosząc morale ludzi. Po czym szybko idzie z Vioriką by mieć to za sobą.

Detektor pokazał, że Mariola jest bardzo, BARDZO zaawansowanym ixientem. Bardzo niebezpiecznym i zaawansowanym. Ale jednocześnie w zamkniętym pomieszczeniu, a koło Marioli są dwie Verlenki w serwopancerzach - Arianna i Viorika. Mariola jednak nie zachowuje się tak jak by się można po ixiencie spodziewać. Nie walczy. Zaczyna rozmawiać.

Mariola rozpina opowieść. Mówi, że: 

* Pochodzi jeszcze z czasów Inwazji Saitaera, walczyła po stronie ludzi. 
* Umierała, ale została wtedy zainfekowana ixionem. Ixion uratował jej życie. 
* Reprezentuje _strain_, który nie próbuje się rozprzestrzeniać, ale próbuje powstrzymywać lokalne pole Interis.
* Pokazała wszystko to, co zrobiła. Ratowała to miejsce. Bez niej i bez ixionu to miasteczko zostanie poważnie uszkodzone jak nie zniszczone.
* Nie chciała infekować Blakenbauera. Owszem, przygotowała awaryjnie jednostkę do tego zdolną (miała jedną po wojnie), ale jej nie uruchomiła.
    * Larwy. Larwy Blakenbauera wchodzące w interakcję z Interis weszły też w interakcję z Ixionem... i to uruchomiło infektora
* Mariola ostrzegła, że zniszczenie jej doprowadzi do czegoś jeszcze - INNE okoliczne strainy ixiońskie uznają, że ten strain nie zadziałał. Czyli nie ma co próbować się ograniczać, trzeba atakować.

Wszystko, co Mariola powiedziała brzmi wiarygodnie. Co gorsza, ona w to wierzy. Co jeszcze gorsze, Skażenie Ixiońskie - perfekcyjna adaptacja - sprawia, że ciało ixienta wydzieli odpowiednie środki, elementy, hormony, przekształci pamięć. Mariola będzie święcie wierzyć w to co mówi, bo jest ixientem. Adaptacja przede wszystkim, adaptacja za wszelką cenę.

Verlenki się zawahały. Ale nie. Trzeba ją zniszczyć. Viorika otwiera ogień do Marioli, korzystając z tego, że Mariola jest bardzo przekonywująca.

Tp +3:

* V: Mariola była AUTENTYCZNIE zaskoczona że Viorika strzeliła. Nawet nie wchodziła w tryb bojowy.
    * wstępna analiza Ragny pokazała, że Mariola musiała być młodsza niż z czasów wojny.
* V: Między Vioriką i Arianną udało się zestrzelić Mariolę.

Pojawia się w końcu Smoczkogryz. Przybył do Przygrzybka z oddziałem i karawaną.

* W karawanie - te ciała zabite przez Wilka - oni nie byli zainfekowani.
    * Smoczkogryz sprawdził, ale na wszelki wypadek przyciągnął je do sprawdzenia przez Ragnę.
    * Ragna potwierdziła; nie ma śladu ixionu. Więc hipoteza "interient robi to co robi interient" jest przewidywalna.

Oki - ale co zrobić ze zniszczoną Mariolą? Mariola była lokalną bohaterką i naprawdę była światłem w ciemności. Jeśli wyjdzie na to, że od początku była ixientem, Rozpacz się podniesie i Interis jeszcze mocniej zacznie wpływać na Przygrzybek. (okazało się po wstępnych badaniach, że Mariola nie była tu od 70 lat; ta część słów ixienta nie była prawdą. A inne? Nie wiadomo...)

Po krótkiej rozmowie i serii planów, Smoczkogryz ma pomysł. Niech "prawdziwa, oryginalna Mariola" wezwała Ragne na pomoc, bo odkryła, że tu coś się dzieje. Zły ixient. I obroniła miasto, ale przegrała i została zastąpiona przez terrorforma. W ten sposób:

* Mariola jaką znają zostaje bohaterką
* Mariola jaką zniszczyły Verlenki to ixient, potwór który Mariolę zastąpił
* Nikt nie wie, że ixient od początku im pomagał
* Morale będzie podniesione

Ariannie plan się podoba. Robi mowę i sprzedaje to ludziom. 

"Jesteście wszyscy idiotami! Żadne z Was nie zauważyło! Mariola bohatersko odkryła że to coś jest w okolicy! I udała się sama by Was nie narażać! Jedyne co zrobiła to wiadomość do Ragny z prośbą o pomóc, na wszelki wypadek. I gdy przybyliśmy, nikt nam nie powiedział że z Mariolą coś jest nie tak. Myśleliśmy, że ona działa, że ma jakiś plan. A ona już była martwa, poświęciła się za was wszystkich byśmy mogli dokończyć sprawę. Dziś wytropiłyśmy ixienta który przybrał postać Marioli i go zniszczyłyśmy, potwór był ciężko ranny i ukryty. Mariola Was uratowała."

Tp +4:

* V: Łyknęli to.

To miejsce da się ustabilizować by usunąć zagrożenie ixiońskie. Będzie to wymagało wiele pieniędzy, wiele sprzętu, ale Przygrzybek może przetrwać i zostać oczyszczony od Energii Ixiońskiej. Dostał solidnie w kość: część generatorów nie działa, siły medyczne nie funkcjonują, ale ogólnie populacja jest silna i miasteczko jest jako całość zdrowe.

Ale wpierw trzeba uratować Blakenbauera. Ragna użyje połączenia z Blakenbauerem i krew wilka (pozyskaną przez Viorikę). Użyje magii, by odnaleźć gdzie znajduje się Wilk... i Blakenbauer.

Tr Z M +3 +3Ob:

* Ob: Ragna i Szałwiusz czują swój ból i tylko ból gdy jedno cierpi mocniej. Czyli są połączeni emocjonalnie przez Interis przez emocje Interis - na jakieś kilka miesięcy.
* Vr: Ragna wie, w której jaskini w okolicy kryje się Wilk i Blakenbauer.
    * Czuje też, że wilk DUŻO zapłacił. Stracił dużo pamięci. Stał się mniej-sobą. Interis pożarło niemało z samego Wilka.

Trzy Verlenki wybierają się na ekspedycję by ratować Blakenbauera. Smoczkogryz chciał iść z nimi, ale przekonany przez kuzynki zdecydował się zostać w miasteczku. Nie chce kraść im chwały. 

Gdy zbliżyły się do jaskini w której znajduje się Wilk, Ragna (w personalnym pancerzu, ze sprzętem medycznym i ratunkowym pożyczonym od Koriny) i Viorika (w serwopancerzu) weszły do środka. Arianna przygotowała gniazdo snajperskie 2 kilometry dalej; cierpliwie czeka z nadzieją, że Viorika wypłoszy Wilka z jaskini. Jakkolwiek Ragna jest skonfliktowana czy Wilka należy zabić czy nie, Viorika i Arianna uważają, że potwór - zwłaszcza interient - musi zginąć.

Po przejściu przez jaskinię, w pieczarze pojawiła się ciemność niemożliwa do złamania światłem latarki. Leżący, ranny i nieprzytomny Blakenbauer z głową na Wilku. Wilk, który promieniuje Entropią uniemożliwiając ixioński postęp. Wilk, który patrzy na Verlenki i pokazuje kły. Nie atakuje ALE.

Ragna używa sieci sympatii (wynikającej z Efektu Skażenia wcześniej), by pokazać niepokój ze straty Blakenbauera. Że Ragna próbuje mu pomóc. W końcu przecież Wilk pamięta Ragnę.

Tp +3:

* Vr: Wilk **zrozumiał** że Ragna nie jest zagrożeniem dla Blakenbauera. Acz niekoniecznie że nie jest zagrożeniem dla niego.
* X: Wilk warczy na Viorikę. Niech Viorika wyjdzie. Viorika się cofnęła poza zasięg, ale nadal może strzelać, choć z trudem. Wilk jako wilk nie rozumie konceptu "strzelania".
* V: Wilk został lekko uspokojony, Viorika się cofnęła (ale w zasięgu), Ragna może działać i pomóc Blakenbauerowi.

Gdy Wilk się przesunął, okazuje się, że Blakenbauer leży na "miękkiej poduszce" z jakiegoś z porwanych ludzi. Wilk stopił jego kości; została "miękka masa", by Blakenbauerowi było miękko. Ragna nie skupia się na tym na czym leży jej przyjaciel (choć to kolejny dowód, że Wilk będąc inteligentnym jest zwierzęciem oraz interientem) i zabiera się za łatanie Blakenbauera.

Tr Z +3:

* Vz: Ragna naprawiła fuszerkę zrobioną przez Wilka. Wycięła atroficzne tkanki. Usunęła resztkę ziarna ixiońskiego. Blakenbauerowi nie grozi śmierć.
* V: Ragna doprowadziła Blakenbauera do stanu wystarczającego do ewakuacji.

Ragna powoli się cofa z Blakenbauerem, wyciągając go na "mięsnym materacu". Viorika cofa się z odległości, ale monitoruje Wilka. Wilk idzie pół kroku za Blakenbauerem, gotowy do ataku w dowolnym momencie, ale pozwalając Ragnie wyciągnąć Blakenbauera.

Wyszli poza jaskinię. Na światło. Arianna na pozycji snajperskiej ma możliwość strzelania, jeśli Ragna się odsunie. Ragna robi pół kroku w prawo. Arianna strzela z odpowiednio dopasowanej amunicji.

Tp +3:

* V: Wilk nawet nie poczuł gdy zginął. Ragna straciła jeden sygnał w swojej sieci sympatii.

Blakenbauer uratowany. Interient zniszczony. Ixienty zneutralizowane. Przygrzybek poważnie uszkodzony, ale możliwy do uratowania.

Ragna zdecydowała się zostać jakiś miesiąc w Przygrzybku, doprowadzić to miejsce do jakiegoś działania. W końcu ma krewnych-i-znajomych w Verlenlandzie, dostanie jakiś sprzęt i komponenty, uda się jej doprowadić Przygrzybek do stanu funkcjonowania.

***
***

## Streszczenie

Ragna, Arianna i Viorika wyruszają do Przygrzybka Górskiego, gdzie Entropiczny Wilk atakuje mieszkańców i karawany. Na miejscu odkrywają, że Wilk chroni rannego Blakenbauera (Szałwiusza), jednocześnie popadając w Skażenie Interis i Ixion. Drużyna demaskuje lokalną ixienkę Mariolę, która w imię "dobrych chęci" chciała utrzymać miasteczko przy życiu, ale w istocie była groźnym pasożytem. Verlenki zabijają Mariolę, opanowują panikę wśród ludzi i podążają w górską jaskinię po Szałwiusza. Ragna ratuje maga, a Tomfoer został zastrzelony snajpersko, by nie zagrażał więcej mieszkańcom. Przygrzybek otrząsa się po ciężkich ciosach Skażenia i uszkodzeń i z pomocą Ragny ma szansę odbudować się od nowa.

## Progresja

* Ragna Verlen: zostaje około miesiąca w Przygrzybku Górskim; pomaga w odbudowie i odkażeniu miasteczka.
* Ragna Verlen: kilka miesięcy link emocjonalny przez Interis z Szałwiuszem Blakenbauerem.
* Szałwiusz Blakenbauer: ciężko ranny; potrzebuje około 2 miesięcy regeneracji w biolabie. Stracił Entropicznego Wilka (Tomfoera).
* Szałwiusz Blakenbauer: kilka miesięcy link emocjonalny przez Interis z Ragną Verlen.

## Zasługi

* Arianna Verlen: Przybyła do Przygrzybka wspólnie z Vioriką i Ragną, by powstrzymać Wilka i zabezpieczyć miasteczko przed Skażeniem; próbowała wzmocnić zaklęcie Ragny wobec Wilka, ale sama została osłabiona przez Mgłę Interis i zasilała go energią; ostatecznie, wspólnie z Vioriką, zabiła ixientkę Mariolę, a potem wystąpiła przed mieszkańcami, by opanować panikę i wyjaśnić sprawę "potwora zastępującego Mariolę". Zastrzeliła Wilka snajperką.
* Viorika Verlen: Wraz z Arianną i Ragną przybyła, by zneutralizować Wilka. Gdy Wilk atakował cywili, ostrzeliwała go, pozyskując jego krew. Rozmawiała z ixientką Mariolą i podjęła decyzję o jej zabiciu; zestrzeliła ją. Osłaniała Ragnę podczas wyciągania rannego Szałwiusza z jaskini.
* Ragna Verlen: Zdeterminowana, by uratować Szałwiusza, z którym ma osobistą relację; w trakcie przechodzenia przez Mgłę Interis magią straciła swój serwopancerz. Odkryła Skażenie Marioli dzięki własnoręcznie skonstruowanemu stacjonarnemu skanerowi ixiońskiemu. Korzystając z magii i relacji z Wilkiem znalazła Blakenbauera. Potem w jaskini opatrzyła i ewakuowała półprzytomnego Blakenbauera i wystawiła Wilka na strzał.
* Smoczkogryz Verlen: Badał ataki Wilka w okolicach karawan i przybył do miasteczka z posiłkami; odkrył, że część zabitych nie miała aspektów ixiońskich. Gdy nie było wiadomo jak zamaskować działania Marioli, zaproponował "bohaterską legendę Marioli" która się poświęciła. Niechętnie, ale dał Verlenkom działać samotnie; nie chciał kraść im chwały, mimo, że mu zależało.
* Szałwiusz Blakenbauer: Sprzężony z Entropicznym Wilkiem, porusza się po Przelotyku by pomagać ludziom. Zainstalował się w Przygrzybku, by pomagać ludziom przeciw Interis dziwnymi larwami. Niestety, prefektiss go zainfekowany ixionem a Wilk ewakuował. Uratowany przez Ragnę, wymagał długiej rehabilitacji po śmierci Tomfoera.
* Korina Serniczkow: Starsza lekarka z licznymi protezami, silnie dotknięta przez Interis; przeprowadzała autopsję na ciele ofiary Wilka i kazała je spalić, by zapobiec rozprzestrzenianiu się Skażenia; udostępniła Ragnie sprzęt medyczny do stworzenia skanera ixiońskiego. Nie jest Skażona, acz jest w złej formie. Troszczy się o rannych i wspiera odbudowę.
* Natalia Rosomicka: Burmistrzyni Przygrzybka, która z lękiem przyjmowała wizytę aż trzech Verlenek; nadal broniła dobrego imienia Blakenbauera. Opowiedziała o atakach Wilka i współpracowała, by zapobiec panice. Zapewniła spokój, gdy odkryto, że liderka ochrony (Mariola) była ixientką.
* Kamil Orpaczek: Farmer grzybów i świeży wyznawca Saitaera, gotów na wszystko, by ratować chorego syna Mariusza za pomocą ixiońskiej transorganizacji; przeżył wstrząs, kiedy Wilk zagryzł i "wyssał" z chłopca energię, na jego oczach. Po śmierci Marioli i załamaniu kultu został pozostawiony z silnym żalem i nieufnością wobec magów.
* Mariola Glewisen: Charyzmatyczna przywódczyni obrony miasteczka, będąca zaawansowanym ixientem; przez lata wspierała ludzi, lecz też fortyfikowała Kult Saitaera; zdemaskowana dzięki skanerowi ixiońskiemu Ragny i natychmiast zastrzelona przez Ariannę oraz Viorikę; jej śmierć zatuszowano legendą "prawdziwej" bohaterskiej Marioli, by uniknąć paniki.
* Rodencja Bemyszcz: Barmanka z Baru Górskiego, naoczny świadek infekcji Ixiońskiej Blakenbauera a potem ataku Wilka na Blakenbauera. Na rozkaz Marioli zatarła wszelkie ślady; błagała Verlenki, by ją stąd zabrały i była tak pomocna jak potrafiła. Ma złamane morale.

## Frakcje

* .

## Fakty Lokalizacji

* Przygrzybek Górski: przelotycka wioska w skale, z działkami obronnymi i minami; ma farmę grzybów, niewielką elektrownię geotermiczną i ogólnie jest dość niezależna.
* Przygrzybek Górski: dość wysoki poziom techniczny, wysoki inżynieryjny. Część sprzętu pochodzi z czasów noktiańskich, część to lokalny sprzęt astoriański.
* Przygrzybek Górski: poważne zniszczenia; spustoszone Centrum Medyczne, uszkodzone generatory, mało prądu. Do tego śmierć Marioli i grupa ixientów. Szczęśliwie, pomoc Verlenów by to odbudować.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Pasmo Przyverleńskie
                            1. Przygrzybek Górski
                                1. Część zewnętrzna
                                    1. Pueblo
                                    1. Działka obronne i miny
                                    1. Garaże i Magazyny
                                    1. Pompy i transportery
                                    1. Zewnętrzny Plac Wspólny
                                1. Część wewnętrzna, NW
                                    1. Kopalnia
                                    1. Elektrownia geotermiczna
                                    1. Inżynieria
                                1. Część wewnętrzna, NE
                                    1. Farmy grzybów
                                    1. Uzdatnianie wody
                                    1. Centrum Medyczne
                                    1. Nory mieszkalne
                                    1. Centrum Dowodzenia
                                1. Część wewnętrzna, Centrum
                                    1. Wewnętrzny Plac Wspólny
                                        1. Bar Górski
                                    1. Bazar

## Czas

* Opóźnienie: 6
* Dni: 3

## Inne
### Przekazane Scholli

Informacje podstawowe (nie musisz tego pamiętać, wystarczy raz przeczytać)

* Twoja postać jest Verlenką (czarodziejka z rodu Verlen). Pozostałe postacie Graczy też są Verlenkami.
* **"tien"** to tytuł arystokraty magicznego (jak "lord"). Jesteś więc tien XXX Verlen, gdzie XXX to imię.
    * Verlenowie mają inny wzór imion. Czasem normalne, czasem takie "na pośmiewisko".
        * np. "Smoczkogryz Verlen" dla młodego walecznego tiena, który jako dziecko zagryzał smoczki XD.
        * np. "Arianna Verlen" jest po postaci z bajki dla dzieci.
        * ALE możesz mieć normalne imię.
* Twoja postać przyjaźni się z **tien Szałwiuszem Blakenbauerem**. Dojdziemy do tego jak to się stało potem.
    * Blakenbauerowie i Verlenowie się zwalczają, ale nikt nigdy nie ginie. 
        * Myśl: Capuletti i Monteki z Romeo i Julii, ale bez pocałunków i bez zabijania.
* Twoja postać umie walczyć. KAŻDY Verlen umie walczyć. To bardzo... wojowniczy ród. Masz:
    * **Serwopancerz**: ciężki pancerz, 80 kilogramów, ze wspomaganiem. Osłona środowiskowa, udźwig 50 kg/ręka, moduły wspomagające.
    * **Karabin maszynowy**: jeśli przeciwnik jest szybki i szybko się rusza, jeśli przeciwników jest sporo.
    * **Granaty**: jeśli przeciwnicy to masa, jeśli chcesz ich porozrzucać i obezwładnić.
    * **Działko przeciwpancerne**: jeśli przeciwnik jest ciężko opancerzony, jeśli musisz przebić się przez pancerz, mało strzałów ale duża siła ognia.
    * **Wibroostrza**: do walki bezpośredniej, z bliska, gdy jest ciasno i nie masz możliwości manewru.
    * **Pistolet**: do walki gdy masz ograniczony ruch, do przeciwników z bliska.

Unikalność Twojej postaci - kilka wyborów. Wszystkie mają sens i się przydadzą. Jak nic z tego Ci nie pasuje, daj znać, zmienimy.

* Twoja postać ma imię. JAKIE?!
* Twoja postać jest INNA niż typowy Verlen. Jesteś troszkę na uboczu rodu. Co Cię charakteryzuje?
    * **A**: Specjalizujesz się w **Energii Interis** - Energii Entropii, Końca Wszystkiego, Rozpadu i Rozpaczy. Jesteś odporniejsza na tą Energię, lepiej ją rozumiesz i umiesz jej używać.
    * **B**: **Bywałaś w tym miasteczku wielokrotnie**, współpracując z Szałwiuszem Blakenbauerem. Znasz miejsca, kryjówki, niektórych ludzi i rozumiesz tą miejscowość lepiej niż ktokolwiek inny.
    * **C**: Masz **Siatkę Dron** - posiadasz serię dron obserwacyjnych i bojowych. Pod wieloma względami "możesz być wszędzie i wszystko zobaczyć"
* Twoja postać ma też określone umiejętności poza bojowymi. Jakie?
    * **A**: Mechanik. Potrafisz tworzyć, przekształcać i dostosować sprzęt i mechanizmy do swoich potrzeb.
    * **B**: Badacz. Potrafisz skupiać się na Energiach magicznych, śladach, rzeczach nie pasujących oraz dojść do rozwiązań które innym unikają.
    * **C**: Azalitka. Ludzie Ci ufają i potrafisz ich czytać i rozumieć. Nie porywasz tłumów, ale potrafisz sprawić, że każda osoba z którą dłużej obcujesz Ci powie swoje sekrety.

Fakty, które zna TYLKO Ragna:

FAKTY OGÓLNE:

* Dowolna istota (mag, potwór...) może zawierać **TYLKO dwie Energie Magiczne naraz**. Zero, jedną lub dwie. Nigdy więcej - to destabilizuje Wzór istoty. (_to wiedzą wszyscy, Ty też musisz_)
* W okolicy Przelotyka (gdzie jesteście) jest podniesione pole **Energii Interis** (Koniec Wszystkiego, Pustka, Entropia, Rozpacz). (_to wiedzą wszyscy, Ty też musisz_)
    * Z tym wiąże się  (_to wiesz tylko Ty_)

FAKTY O BLAKENBAUERZE:

* **Szałwiusz Blakenbauer**, nietypowy przyjaciel Ragny
    * poświęcił 3 lata swojego życia na pomaganie różnym osadom w Przelotyku by neutralizować pole Interis.
    * oddał część swojego życia i zdrowia by zasilić energią umierającego Wilka Entropii. Ta istota z nim została, by być mu lojalna. Nazywa się Tomfoer.
    * jest osobą, która nie robi chorych ani groźnych eksperymentów. Jest fundamentalnie dobrym człowiekiem, choć jest Blakenbauerem i jest dość... trudny.
    * zwykle działa w obszarze uzdatniania żywności i uzdatniania wody. Jeśli go gdzieś szukać, to gdzieś tam.
* **Tomfoer**, Mglisty Wilk Entropii Szałwiusza Blakenbauera. 
    * Jest niesamowicie lojalny swojemu właścicielowi, ale jest zwierzęciem; nie drażni się dzikich zwierząt. Myśli jak zwierzę.
    * Nie jest agresywny, ale jeśli coś zagrozi jego właścicielowi - zniszczy to bezwzględnie.
    * Jest istotą dwóch Energii: **Interis** (rozpacz, pustka, nicość, entropia) oraz **Anteclis** (ucieczka do przodu, innowacja, ścieżki możliwe do wybrania).
    * Tomfoer zna Ragnę. Więc _raczej_ nie zrobi jej krzywdy. Chyba, że Ragna zagrozi Szałwiuszowi w jego oczach...

FAKTY O MIASTECZKU:

* Z uwagi na **dużą ilość Energii Interis w okolicy** (czym zajmował się Szałwiusz Blakenbauer), jest tu większa słabość i więcej chorób lokalnych ludzi, wiele nieuleczalnych. Plus sprzęt się psuje.
* **Aktein Karr** najpewniej kiedyś był komandosem lub czymś innym śmiertelnie niebezpiecznym. I to po stronie WROGIEJ do naszej planety. Ale nie ma w nim już woli krzywdzenia innych, jest na emeryturze.
* **Mariusz, syn Kamila** cierpi na przewlekłą chorobę powiązaną z Interis. Jest słaby fizycznie i szybko się męczy. Nie dożyje do 20 roku życia. Tutaj nie da się mu pomóc.

### Tomfoer, Entropiczny MgłoWilk

Tomfoer

| **WHAT**      | **Values**                                        |
|---------------|---------------------------------------------------|
| Atak          | szczęki (1), lodowy podmuch (2)                   |
| Obrona        | interient (2), atak z mgły (2)                    |
| Utils         | mgła (3), mgłoportacja (3), niezwykle szybki, ATV |
