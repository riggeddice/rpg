## Metadane

* title: "Igrzyska przegrywów"
* gm: kić
* players: pati, lucek

## Kontynuacja

### Kampanijna

* [240320 - Seryjne samobójstwa na Szernief](240320-seryjne-samobojstwa-na-szernief)

### Chronologiczna

* [240320 - Seryjne samobójstwa na Szernief](240320-seryjne-samobojstwa-na-szernief)


## Projekt sesji

„Drużyna normalnych ludzi, którzy próbują przetrwać we wrogim środowisku i być grupą która przetrwa najdłużej”
Drużyna + survival + kombinowanie jak użyć otoczenia + próba przetrwania i pokonania innych zespołów
Opis sesji:
Przegrywy. Menele. Drobne złodziejaszki. Ludzie bez perspektyw. Ci, którzy stoczyli się na dno...
To Wy. Ale los dał Wam szansę.
Pewnego ranka budzicie się na zboczu góry, otoczeni wam podobnymi. Porwani z ulic i zaułków, zostaliście wrzuceni w igrzyska, które może wygrać tylko jeden zespół. Zwycięzcy dostaną wszystko, o co tylko poproszą. Los przegranych nikogo nie obchodzi. Weteran Wam powiedział, że publiczność nagradza dobre przedstawienie.
Czy przetrwacie? Czy uda się wam wygrać? Jak daleko się posuniecie, by dostać drugą szansę od losu? To zależy tylko od was.



postacie: Kate (medyk) Misterio(bokser)

### Scena Zero - impl

grupa spełniaczy marzeń otrzymuje po Igrzyskach zlecenie spełnienia marzeń Tomasza.
Po zrealizowaniu jego dziwnych ale możliwych pomysłów (na przykłąd lodówka z AI, które zapewnia idealne jej wyposażenie i dożywotnia subskrypcja w sklepie), nie zostało im zbyt wiele budżetu.
A tu dotąd rozsądny Tomasz zażyczył sobie trzypiętrowej zjeżdżalni w piętrowym budynku... Kiedy próbowali go przekonać, że to nie jest możliwe, do domu weszła kobieta... Tien Keksik.
"Odmawiacie spełnienia jego marzeń?" Po odpowiedniej ilości podlizywania się, tien Keksik uznała, że im pomoże... w końcu trzeba pomagać maluczkim.

#### Sesja właściwa

"No to zakłady przeciwko nam właśnie zyskały na prawdopodobieństwie... Jonatan, były dziennikarz, stwierdził, patrząc na spadającego w przepaść czwartego członka ich zespołu.
Zmuszeni byli uciekać z górskiej półki, na której się ocknęli, gdy ta zaczęła się kruszyć. Kiedy schodzili po grani, nagle pod stopami byłego żołnierza skruszyła się skała.
Nie mieli szans zareagować.

Nie mając nic innego co mogą zrobić, zeszli ze zboczy w dolinę i zaraz rozbili obóz. Kate znalazła wygodną, płytką jaskinię (przynajmniej nikt ich nie zajdzie od drugiego wejścia).
Podczas gdy Misterio z Jonatanem przygotowali łóżka, by spało im się w miarę wygodnie, Kate postanowiła poszukać przydatnych (głównie trujących) roślin.
Zrezygnowali z ognia. Choć dałby Kate szansę na ugotowanie jakiejś trucizny, nie chcieli ogłaszać swojej obecności.
Jonatan opowiedział im to, co wiedział o tym, w jakiej sytuacji się znaleźli. To Igrzyska, tylko jeden zespół może wygrać (ale wygrać można na wiele sposobów). Kiedy poprzednio brał w nich udział (jego zespół nie wygrał), teren ułożony był w siatkę o boku 3x3, każde pole siatki było innym terenem. Jonatan nie wie, ile zespołów jest "na planszy", ale wie, że jeśli ich działania spodobają się publiczności, mogą dostać coś pomocnego.

Pierwszej nocy założyli, że będą raczej bezpieczni. Założyli, że inne zespoły będą próbowały iść przez noc, ale nie powinny do nich dotrzeć. Na wszelki wypadek rozstawili warty. Noc upłynęła spokojnie.
Kończą im się zasoby. Po zwinięciu obozu losowo wybrali kierunek i postanowili zapolować na inny zespół zgodnie z założeniem, że tamci będą zmęczeni po nocy. Znaleźli strumień i podążali za nim, ale w taki sposób, aby nie dać się wykryć. 
Udało im się i rzeczywiście, przy brzegu strumienia czwórka ludzi uzupełniała manierki. Wycofali się w las dookoła, po czym rozdzielili. 
Plan jest taki: Kate podkradnie się i zwinie ich plecaki, w tym czasie Misterio będzie odwracał uwagę. Misterio uznał, że będzie udawał odgłosy jelenia na rykowisku. Kilka krzyżyków później niestety, jedyne, co udało mu się osiągnąć to zwiększenie czujności zespołu przeciwników. Zaszarżował więc przez strumień, i udało mu się ściągnąć za sobą największego (i pewnie najsilniejszego) członka przeciwnego zespołu. W tym czasie, Kate niestety została dostrzeżona i choć udało się jej zwinąć plecak, to dostała dość mocny cios, w wyniku którego miała mało czasu zanim padnie. Na szczęście Jonatan przyszedł jej z pomocą i razem udało im się uciec pościgowi. W tym czasie Misterio wyprzedził pościg i schował się na drzewie. Zeskakując z góry na przeciwnika, rzucił mu piaskiem w oczy i wyprowadził pierwszy cios. Przeciwnik choć silny, nie potrafił jednak pokonać Misterio. W efekcie paker został w gaciach a Misterio zabrał wszystko, łącznie z ciuchami i nożem.  Misterio wygrał w tak widowiskowy sposób, że dostali "punkt dobroci publiczności".
Udało im się ponownie połączyć. Kate, gdy już się ocknęła, dała radę iść. Postanowili kontynuować podróż, by dodać odległości od okradzionego zespołu.

Dalej trzymali się mniej więcej strumienia, jednak w którymś momencie zobaczyli, że na przestrzeni kilku metrów górski krajobraz zmienia się w pustynię. Cofnęli się (pustynia fuj!) i poszli w lewo.
Znaleźli ścieżkę, którą mogli dość wygodnie podążać, postanowili jednak iść trochę ostrożniej i dbać, by nie być zbyt widocznym.
Robiło się już późno, gdy (będąc wyżej) zauważyli cztery osoby podążające ścieżką w ich stronę. 
Schowali się i śledzili ten zespół do czasu aż ci postanowili się zatrzymać na noc. Widać po nich było, że też nie spali zbyt wiele pierwszej nocy. Udało im się znaleźć jaskinię podobną do tej, z której skorzystał Zespół, ale oni rozpalili ognisko i zaczęli gotować potrawkę. Oni mają kociołek (!!!) i pewnie krzesiwo!
Ustalili plan. Kate podkradła się do kociołka i nawrzucała tam nazbieranych przez siebie trujących roślin. Dała nawet radę zadbać, by smak nie był zbyt dziwny. Zespół przeciwników zabrał się za kolację. Niestety (wynik krzyżyka) jedna z osób nie zjadła potrawki. 
Misterio szybko jednak rozprawił się z próbującym się opiekować kolegami strażnikiem. 
Zabrali wszystkie ich plecaki i kociołek oraz krzesiwo, po czym pocisnęli dalej. 

Było już późno i czuli się dość zmęczni, gdy dotarli do lasu namorzynowego. Opadły ich owady i od razu całkowicie przemokli, ale udało im się znaleźć punkt startowy tego terenu. Rozpalili ognisko, rozstawili warty i poszli spać. Pierwsza warta (Kate) upłynęła spokojnie. Obudziła Jonatana i się położyła. Jakiś czas później obudził ich plusk wody pod platformą, na której spali. Na wpół przytomni, ledwo zobaczyli cień kształtu. Ktos ukradł jeden z ich plecaków! Łapać złodzieja! Nie mając jak śledzić czy ścigać zwinnego złodzieja w nocy, zaapelowali do publiczności, aby ta oznaczyła ściganą przez nich osobę. O dziwo, zadziałało.
Po krótkim, acz intensywnym pościgu złapali... dziewczynkę. Wyższe naście lat, cała przerażona. "Nie rób mi krzywdy!" Powstrzymana przez Misterio, Kate schowała nóż, którego prawie użyła.
Przesłuchali Izabelle. Ona straciła zespół, kiedy inny zespół ich zaatakował. Jej udało się schować w ciasnym miejscu i tamci uznali, że nie warto na nią polować. I ona po prostu próbuje przetrwać... A oni mają tyle plecaków, nie potrzebują ich wszystkich... Kate i Misterio odegrali role złego i gorszego gliny (Kate to ten gorszy).  Kate początkowo optowała za zabiciem Izabelle, ale okazało się, że dziewczyna ma coś bardzo pomocnego - mapę terenu. W końcu uznali, że ją "zaadoptują" do zespołu. Oczywiście jej nie ufają, więc warty wciąż są trzy... 

Spać już nie poszli, więc wszyscy byli nieco zmęczeni, ale kiedy przyjrzeli się mapie i policzyli zespoły, uznali, że mogą spróbować wygrać rzucając wszystkim wyzwanie. 
Z mapy wynikało, że pustynia jest w samym centrum. Kate i Misterio założyli, że nawet, jeśli jakiś zespół zaczął na pustyni, to jak najszybciej ją opuścił. Jest szansa, że będą w stanie przygotować pułapkę i wygrać w ten sposób. Chcą to zrobić tak, żeby rzucić wyzwanie innym zespołom, pozwolić im powalczyć między sobą i wykończyć tych, co zostaną na polu walki. 
Korzystając z tego, że są w lesie namorzynowym, Kate zebrała jeszcze więcej trujących roślin (kontaktowo). Pustynia pomaga w szybkim suszeniu.
Znaleźli dobre miejsce na pułapkę. Tak, aby Jonatan mógł z bezpiecznego miejsca dawać publiczności komentarz na żywo (w końcu trzeba się dobrze podlizywać). Znaleźli miejsce z ruchomymi piaskami otaczającymi krąg z kamienia. W środku kręgu Misterio początkowo się ukrywający, potem służący za przynętę. 
Na zewnątrz, poza ruchomymi piaskami Kate z Izabelle. Po przeciwnej stronie, ukrywający się i prowadzący stały komentarz Jonatan (w którymś momencie jego głos zaczął rozbrzmiewać po całej arenie - V na wpływie na publiczność).
Następnie Misterio rzucił wyzwanie wszystkim, po czym się schował.

Ich plan początkowo zadziałał i pozostałe zespoły skupiły się na sobie zamiast natychmiast atakować ich. Niestety, wmieszali się organizatorzy, i kiedy walka się rozpędziła, wywołali potężną burzę piaskową. 
Zanim burza rozpętała się na dobre, Kate z Izabelle udało się zdjąć co najmniej jednego przeciwnika z dystansu. Kilku ugrzęzło w ruchomych piaskach, ale wciąż wielu pozostało. 
Pod Misterio zaczęła drżeć ziemia, zupełnie, jakby coś wielkiego się pod nim poruszało. Nie zastanawiając się długo, wskoczył na otaczające go kamienie. Może i potencjalnie nieco się odsłania, ale przynajmniej nic go nie zje od dołu... Ogromny czerw udał się polować na innych, a Misterio pokazał się w centrum licząc, że któryś z przeciwników się na niego rzuci.
Kate wyczaiła, że zbliża się do niej i Izabelle jeden przeciwnik - udało się jej skutecznie go unieszkodliwić swoim swędząco-trującym proszkiem.
Organizatorzy uznali, że presja jest wciąż zbyt mała i pustynia nagle zmieniła się w tajgę... A burza z piaskowej stała się śnieżną. Oślepiony i zszokowany, zespół zaapelował do swoich patronów o oznaczenie przeciwników. Dostali to, o co prosili, ale nie do końca tak, jak prosili - nagle wszyscy na polu walki wiedzieli, gdzie są wszyscy inni...

Jonatan, dotąd dość bezpieczny i nie zwracający na siebie szczególnej uwagi wrogów stał się celem. Kate i Izabelle, jak również Jonatan pobiegli w stronę centralnej wyspy z kamieni (oni wiedzieli, gdzie są pułapki)
Udało im się wciągnąć w pułapkę jeszcze kilku przeciwników, jednak w szalonym biegu Izabelle padła (X, gracze mieli wybór który NPC przetrwa)

Nagle, burza śniegowa zamieniła się w sztorm, a dookoła powoli zaczęła podnosić się woda... Organizatorzy zdjęli rękawiczki, mógł być tylko jeden zwycięzca.
Zespół zwarł szerego i wspólnie wyończyli ostatnich przeciwników.
W chwili, gdy padł ostatni, grunt pod nimi zaczął się podnosić, jakby stali na platformie.
Wygrali...


(Niestety, nie udało mi się zapisać konfliktów, było bardziej epicko niż to wynika z opisu, ale nie do końca wszystko zapamiętałam)



