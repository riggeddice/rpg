## Metadane

* title: "Anomalne awarie Athamarein?"
* threads: legenda-arianny
* motives: ku-pokrzepieniu-serc, aurum-wasn-miedzyrodowa, bitter-vet, integracja-rozbitej-frakcji, pracownik-nalesnik, przepascie-miedzy-klasami, sabotaz-niespodziewany, skazenie-magiczne-maszynerii, spotlight-na-postac
* gm: żółw
* players: fox, kapsel, kić

## Kontynuacja
### Kampanijna

* [210519 - Osiemnaście Oczu](210519-osiemnascie-oczu)

### Chronologiczna

* [210519 - Osiemnaście Oczu](210519-osiemnascie-oczu)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA: 
    * VNV Nation "Honor"
        * "Such pride remains unbroken | Such words remain unspoken" && "Stand your ground, this is what we are fighting for | For our spirit and laws and ways"
* Opowieść o (Theme and vision): 
    * "Athamarein, anomalny statek-weteran, nie dopuści by jej załoga odleciała w tym stanie. Poratuje i pomoże, nawet sabotując swoje subsystemy."
    * "Duma i cicha demonstracja tradycji i tego wszystkiego o co walczy Orbiter w 'osobie' Athamarein i poprzednich dowódców tej jednostki."
    * "Naprawimy Athamarein, dodamy pewności siebie ludziom na Athamarein i doprowadzimy wszystko do działania - a przy okazji zobaczymy o co warto walczyć."
    * SUKCES
        * odkrycie: co się stało z Athamarein? Dlaczego Athamarein nie działa prawidłowo? 
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * ku-pokrzepieniu-serc: (-> Arianna). Arianna jest szanowana, jest źródłem światła, młodzi tieni patrzą na nią z nadzieją i faktycznie może pomóc ludziom.
    * aurum-wasn-miedzyrodowa: (drp Eustachego, potem kojenie Arianny) młody Sowiński dowodzący jednostką, młody Gwozdnik jako pierwszy oficer.
    * integracja-rozbitej-frakcji: (drp wszyscy) Załoga Athamarein jest skłócona i wszyscy winią wszystkich. Zespół musi to zidentyfikować i ponaprawiać co się tam dzieje.
    * bitter-vet: (drp Eustachego) weteran Remigiusz chciałby na inną jednostkę, ma dość "niekompetentnych tienowatych", wini wszystkich za to co się stało
    * pracownik-nalesnik: (drp Eustachy) tien Uśmiechniczka Diakon, śliczna, chce być influencerką i dobrze wyglądać i zapewniać morale ('ozdoba' statku)
    * przepascie-miedzy-klasami: (drp Arianna/Eustachy) młody Samszar bał się powiedzieć, że to STATEK ich ratował jego zdaniem
    * sabotaz-niespodziewany: (drp Klaudia) jak tylko Athamarein ma coś zrobić (ćwiczenia/działania), po prostu nie działa. Coś się znowu psuje.
    * skazenie-magiczne-maszynerii: (drp wszyscy) Athamarein nie jest może jednostką mocno Skażoną, ale jest jednostką anomalną magicznie. Pragnie chronić swoją załogę.
    * spotlight-na-postac: (-> Athamarein) heroiczna historia Athamarein - od starej korwety która była kupiona przez cywila i przebudowana jako statek anty-piracki, przez serię heroicznych dokonań kapitanów, do nobliwej jednostki "teraz"
* Detale
    * Crisis source: 
        * HEART: "brak pewności siebie załogi, wątpliwości załogi" 
        * VISIBLE: "Athamarein odmawia współpracy a wszystko teoretycznie powinno działać"
    * Delta future
        * DARK FUTURE: "Athamarein idzie na złom, załoga się rozprasza na różne jednostki"
        * LIGHT FUTURE: "Athamarein zostaje naprawiona, załoga podchodzi do kolejnych prób naprawy jednostki"
    * Dilemma: "Co robimy z Athamarein"

### Co się stało i co wiemy

* Zamierzchła Przeszłość
    * Athamarein to bardzo stara jednostka; widziała jeszcze sektor Atarien, jako korweta rakietowa
        * Zwalczała Piranie i przeładowywała VonNeumanny samą czystą siłą ognia, pomagając ludzkości uciec przed maszynami
    * Athamarein miała być wysłana na przetopienie, ale została odkupiona przez Juliusza Karweniana, który chciał uhonorować swego syna służącego na Athamarein
        * Sebastian Karwenian poświęcił oficerów Athamarein, ale zniszczył większą jednostkę w trakcie przejmowania przez Maszyny
    * Athamarein służyła jako jednostka patrolowa (vigilante), odpędzająca piratów; duża prędkość i przeważająca siła ognia. Działała niedaleko Valentiny.
        * Zasadzka na grupę piratów z Athamarein jako QShip (lekki patrolowiec); przestraszeni przeciwnicy bali się siły ognia Athamarein
    * Podczas Inwazji Noctis Athamarein uratowała większą jednostkę; cała załoga Athamarein zginęła, ale statek nadawał się do odbudowy.
        * Athamarein wykorzystała swą prędkość i manewrowość, by przyciągnąć uwagę wroga, pozwalając "Mieczowi Światła" się wycofać
    * Athamarein nigdy nie wróciła do pełnej formy; od tej pory zawsze podróżuje z jednostkami wsparcia.
        * Mimo wieku i tego, że jest średniakiem, Athamarein ma stosunkowo mało problemów
* Przeszłość
    * Athamarein została objęta przez kompetentnych, acz niedoświadczonych tienów jako korweta patrolowa
    * Athamarein napotkali statek przejęty przez Syndykat Aureliona miesiąc temu; Sowiński chciał ratować, statek strzelił do Athamarein i musiał być zniszczony. Zabici i ranni.
* Teraźniejszość
    * Athamarein, lekko anomalny średniak floty, ratuje swoją załogę przed śmiercią i konfliktami odmawiając bycia naprawioną
    * Audyt nic nie wykazał. Athamarein jest po prostu zbyt zniszczona i nadaje się tylko na złom
    * Kircznik -> Arianna -> Oroginiec -> Arianna
* Frakcje
    * tym razem każda jednostka reprezentuje własną frakcję; patrz na osoby

### Co się stanie (what will happen)

* Opowieść o: 
    * "Athamarein, anomalny statek-weteran, nie dopuści by jej załoga odleciała w tym stanie. Poratuje i pomoże, nawet sabotując swoje subsystemy."
    * "Duma i cicha demonstracja tradycji i tego wszystkiego o co walczy Orbiter w 'osobie' Athamarein i poprzednich dowódców tej jednostki."
    * **SUKCES**: naprawienie załogi i decyzja o Athamarein
* Faza 0: _Wprowadzenie_
    * Leona wkręca Eustachego w walkę z młodą marine (T) z Inferni ("uratuj mnie o szlachetny Eustachy!") | młoda kręci głową NIE NIE NIE | Marine się podkochuje w Eustachym, Leona 'teehee'
    * Kircznik -> Arianna o Athamarein, akceptacja: komodor Oroginiec
        * Athamarein miała spotkanie z jednostką cywilną przejętą przez Syndykat i musiała ją zestrzelić, ale nie było rozkazu i A. ucierpiała. Są zabici i ranni. Od tej pory A. powinna działać a nie działa.
    * Alicja vs Eustachy. Wiedza o sabotowaniu i/lub walka. Alicja chce przegrać. Leona ją skroi XD.
* "Dowody sabotażu Athamarein"
    * problemy z łącznością i komunikacją, autozagłuszanie siebie samej
    * zacięcie się broni / ryzyko, że strzeli sama
    * fałszywe alarmy: od wycieku paliwa po możliwe kolizje
    * systemy nawigacyjne pokazują niemożliwe i fałszywe lokalizacje
    * przegrzewanie silników, błędne odczyty reaktora
    * "coś jest nie tak z life support"
    * fluktuacje mocy reaktora
    * błędne dane z sieci sensorów
    * POSĄDZENI
        * Remigiusz, weteran, który chciał iść na pole bitwy a nie być mentorem
        * Aleksandra, weteran Orbitera która bardzo nie lubi Aurum
* Faza 1: _Arianna na Athamarein_
    * ?
* Faza 2: _Natura problemu_
    * stakes:
        * los i morale załogi, chwała Arianny, przyszłość Athamarein
    * opponent: 
        * Źle prowadzony dziennik danych. Athamarein jest dziwną anomalią; działania są dyskretne.
        * Duże resentymenty pomiędzy załogantami: Gwozdnik vs Sowiński, Remigiusz vs niekompetencja, miłośnicy Sandry vs przeciwnicy Sandry
    * problem:
        * nie wiemy skąd bierze się problem na Athamarein? Dlaczego korweta rakietowa nie działa?
            * Athamarein 'anomalnie' psuje swoje działania, by korweta nie mogła wrócić do działań polowych (co mogłoby zintegrować załogę, ale Athamarein tego nie rozumie jako statek kosmiczny)
        * załoganci mają wady
            * Sowiński nie ma pewności siebie i uważa, że zawiódł w ostatniej akcji (chciał ratować życia)
            * Gwozdnik jest zbyt arogancki i uważa, że on byłby lepszym kapitanem (jest zbyt zadaniowy i załoga go nie lubi)
            * Remigiusz uważa wszystkich za niekompetentne dzieciaki i chce przeniesienia (może być mentorem i pomocą, ale nie chce)
            * Uśmiechniczka chce żyć 'glamourous life' a nie ciężko pracować ku lepszemu
            * Aleksandra nie znosi Aurum i uważa, że Athamarein ze swoją historią zasłużyła na coś dużo lepszego
            * Większość załogi chce przeniesienia z Athamarein na inną jednostkę; 'inni na statku są kiepscy' i 'to stara korweta rakietowa'
        * Athamarein nie jest zadbaną jednostką
            * część zaopatrzeniowców 'robi na lewo' z uwagi na niechęć do tienów
            * Uśmiechniczka powinna zarządzać mechaniką, ale tego nie robi. To sprawia, że niektóre mało ważne podsystemy się same uruchamiają (światła awaryjne, mocna wentylacja, głośne AI Core, głośne drzwi)
    * koniec fazy:
        * Klaudia odkrywa, że to Athamarein jako statek kosmiczny powoduje anomalizację
        * Eustachy pokazuje, jak naprawiać kilka kluczowych rzeczy
        * Arianna i Eustachy odkrywają przyczynę resentymentów załogi
    * WAŻNE POSZLAKI
        * Dowód, że Athamarein się trochę przesunęła przy ataku i dlatego ludzie nie ucierpieli na pokładzie Athamarein; trafienie Athamarein jest niekompatybilne z logami rozkazów. SKĄD TEN RUCH? -> Athamarein sama ratuje
        * Dowód, że nikt nie był w stanie przeprowadzić wszystkich sabotaży jednocześnie -> albo dużo sabotażystów albo nikt
        * Postacie
            * Wianuszek miłośników Uśmiechniczki i inżynierowie odmawiający pracy bo influencerka
            * Moment - czemu nie ma zasobów i są tak słabej jakości? -> przemyt, bo nie będziemy pomagać tienom (Andrzej, Hiacynt)
            * Duża ilość podań o przeniesienie i skarg (Remigiusz, Aleksandra jako generator i podżegacz)
            * Dowódca nie jest szanowany, bo Remigiusz podważa jego autorytet. Ale załoga uważa go za 'spoko'. (Andrzej)
            * Remigiusz może stabilizować, ale nie chce. Szanuje Aleksandrę bardziej. Trzecie podanie o przeniesienie odrzucone

## Sesja - analiza
### Fiszki

Centrala:

* Jakub Oroginiec: komodor, pod którym jest Athamarein; chce pomóc młodym tienom bo uważa ich za godnych uwagi
    * OCEAN: E-C- | Tajemniczy, sekrety za uśmiechem;; Silnie intuicyjny;; Życzliwy i taktowny | VALS: Benevolence, Self-Direction | DRIVE: Carmen Sandiego (naprawić szkody)
* Kajetan Kircznik: medical officer (czarny, afro, paw, augmentacje bio)
    * ENCAO:  +-0+0 |Anarchistyczny;;Stabilny emocjonalnie;;Z poczuciem humoru| VALS: Self-direction >> Power| DRIVE: Jestem unikalny
    
Infernia, dodatkowe postacie:

* Alicja Szadawir: młoda neuromarine z Inferni, podkochuje się w Eustachym, ma neurosprzężenie i komputer w głowie, (nie cirrus). Strasznie nieśmiała, ale groźna. Ekspert od materiałów wybuchowych. Niepozorna, bardzo umięśniona.
    * OCEAN: N+C+ | ma problemy ze startowaniem rozmowy;; nieśmiała jak cholera;; straszny kujon | VALS: Humility, Conformity | DRIVE: Uwolnić niewolników
* Kamil Lyraczek
    * OCEAN: (E+O-): Charyzmatyczny lider, który zawsze ma wytyczony kierunek i przekonuje do niego innych. "To jest nasza droga, musimy iść naprzód!"
    * VALS: (Face, Universalism, Tradition): Przekonuje wszystkich do swojego widzenia świata - optymizm, dobro i porządek. "Dla dobra wszystkich, dla lepszej przyszłości!"
    * Styl: Inspirujący lider z ogromnym sercem, który działa z cienia, zawsze gotów poświęcić się dla innych. "Nie dla chwały, ale dla przyszłości. Dla dobra wszystkich."
* Horst Kluskow: chorąży, szef ochrony / komandosów na Inferni, kompetentny i stary wiarus Orbitera, wręcz paranoiczny
    * OCEAN: (E-O+C+): Cichy, nieufny, kładzie nacisk na dyscyplinę i odpowiedzialność. "Zasady są po to, by je przestrzegać. Bezpieczeństwo przede wszystkim!"
    * VALS: (Security, Conformity): Prawdziwa siła tkwi w jedności i wspólnym działaniu ku innowacji. "Porządek, jedność i pomysłowość to klucz do przetrwania."
    * Core Wound - Lie: "Opętany szałem zniszczyłem oddział" - "Kontrola, żadnych używek i jedność. A jak trzeba - nóż."
    * Styl: Opanowany, zawsze gotowy do działania, bezlitośnie uczciwy. "Jeśli nie możesz się dostosować, nie nadajesz się do tej załogi."
* Lars Kidironus: asystent oficera naukowego, wierzy w Kidirona i jego wielkość i jest lojalny Eustachemu. Wszędzie widzi spiski. "Czemu amnestyki, hm?"
    * OCEAN: (N+C+): bardzo metodyczny i precyzyjny, wszystko kataloguje, mistrzowski archiwista. "Anomalia w danych jest znakiem potencjalnego zagrożenia"
    * VALS: (Stimulation, Conformity): dopasuje się do grupy i będzie udawał, że wszystko jest idealnie i w porządku. Ale _patrzy_. "Czemu mnie o to pytasz? Co chcesz usłyszeć?"
    * Core Wound - Lie: "wiedziałem i mnie wymazali, nie wiem CO wiedziałem" - "wszystko zapiszę, zobaczę, będę daleko i dojdę do PRAWDY"
    * Styl: coś między Starscreamem i Cyclonusem. Ostrożny, zawsze na baczności, analizujący każdy ruch i słowo innych. "Dlaczego pytasz? Co chcesz ukryć?"


Athamarein:

* Błażej Sowiński: kapitan Athamarein, zależy mu na ratowaniu każdego ludzkiego życia
    * OCEAN: E-A+ | Introspektywny i bardzo analityczny;; Niechętnie się kłóci;; | VALS: Benevolence, Humility | DRIVE: Papa Smerf
* Andrzej Gwozdnik: pierwszy oficer Athamarein, świetny artylerzysta i doskonały w pojedynkach, doskonale ubrany
    * OCEAN: E-A- | Surowy, wymagający;; Nie socjalizuje się;; Niezwykle skuteczny acz brutalny| VALS: Achievement, Power| DRIVE: Tajemnica Wszechświata
* Uśmiechniczka Konstrukcjonistka Aurora Diakon: świetny inżynier, 'engineering can be very sexy!', nadspodziewanie dobra w walce wręcz
    * OCEAN: E+C+O+ | Zero barier i nieobliczalna;; Niezwykle konsekwentna i długoterminowa w celach;; Potrafi wiele wytrzymać | VALS: Face, Stimulation | Drive: Zostanę influencerką
* Remigiusz Alkarenit: oficer taktyczny i komunikacji i stary weteran Athamarein jeszcze z czasów Lodowca; noktianin; wtedy był marine
    * OCEAN: C+A- | Cichy, wypowiada się gdy musi;; Brawurowy i szybko podejmuje decyzje;; z boku załogi | VALS: Hedonism, Security | Drive: Przysięga (doprowadzić Orbiter do wielkości)
* Hiacynt Samszar: młodszy oficer ds zaopatrzenia, bardzo wyczulony na świat duchów
    * OCEAN: N+A+ | Ufny i wiecznie niepewny;; pragnie być lubiany;; nieśmiały | VALS: Universalism, Conformity | DRIVE: Zapewnić ludzkości jutro
* Aleksandra Tasmalis: 
    * OCEAN: N-C+ | Ciężka praca;; Przywiązuje ogromną wagę do podejścia;; Uparta i wszystko planująca | VALS: Tradition, Achievement | DRIVE: Usunąć szczury z piwnicy 

### Scena Zero - impl

.

### Sesja Właściwa - impl

Eustachy siedzi na K1 i popija w barze (dla plebsu). I dobrze się bawi. Nagle - widzi najbardziej niepokojący widok. Uciekająca Leona. Biegnie w stronę Eustachego. Goni ją inna dziewczyna. Ta jest większa, goni Leonę z desperacją. A Leona nie chce dać się złapać. I leci do Eustachego. "RATUJ MNIE BOHATERZE! EUSTACHY! RATUNKU ONA CHCE MNIE ZABIĆ!" i śmieje się.

* E: "Sama się możesz uratować."
* L: "Jesteś moim bohaterem!"
* E: "No chyba bardzo nie... chlałaś"
* L: "Goni mnie straszna marine, niebezpieczna!"
* E: "Ogarnijcie się same, przyszedłem się wychillować a nie... dziwne..."
* L: Nazywa się ALICJA i ma TWOJE ZDJĘCIE POD PODUSZKĄ!
* (dziewczyny na siebie polują)
* (Eustachy wzdycha i idzie w kąt)
* L: "A tak w ogóle ona może pokonać Cię w każdym aspekcie, łącznie z sabotażem"
* A: "Nie mów tak!!!"
* (ludzie są zainteresowani)
* Eustachy zna problem. LEONA SIĘ NUDZI.
* E: "Daj mi spokój, chiller po służbie, ganiasz się, było cicho, spokojnie... dumali ludzie, pudełko nicości i ganiasz się z inną laską... zawołamy barmana, kisiel i róbcie rzeczy, jak chcecie się tak ganiać, 2 sekcje dalej możecie się szarpać w kiślu"

Tr +3 +3Og (Alicja):

* Vr: "a przynajmniej na tym zarobicie"
    * Leona dokucza NIE Eustachemu a Alicji: "widzisz, chce Cię zobaczyć w kiślu! :D"
    * Alicja zmienia kolor i atakuje.
    * "nie musicie się o mnie bić Eustachego starczy dla wszystkich"
* Vr: Leona z szaleńczym śmiechem rzuciła w Eustachego stanikiem. I zwiała. A Alicja - za nią. Sam stanik jest nienoszony.
* "Składam go, kładę na barze, piwko. One nie uciekną"
    * Eustachy dostaje +1 plotkę do kolekcji. Eustachy ma to gdzieś...

Arianno. Wiadomość od oficerów porządkowych K1.

* K1: "Komodor Verlen?"
* A: "Co się stało?"
* K1: "Wybryki nieobyczajne..."
* A: "Co konkretnie? Trzeba interweniować?"
* K1: "Leona jest w areszcie. W celi obok jest Alicja. Obie są Twoimi marine."
    * Alicja rzuciła w Leonę vending machine i prawie trafiła w komodora Orogińca. Po czym pobiegła za Leoną
    * Leona też została aresztowana i protestowała że nic nie zrobiła i to wszystko wina dyskryminowania Inferni...
* Leona jest wsadzona do celi sama, Alicja - do celi z nieprzyjemnymi kolesiami. I Leona może patrzeć. Tydzień zamknięcia...

Arianno - masz wiadomość od Kajetana Kircznika. Request na spotkanie ("ale bez zabijania komodorów"). Nic nie może Arianna obiecać. A Kircznik jest na K1.

* K: Przyszedłem z pytaniem. Athamarein, kojarzysz. Kurzmin dał nam kilku młodych tienów. Są całkiem sensowni. Ale spieprzyli na Athamarein i Athamarein... nie działa.
* A: Potrzebujecie pomocy na Athamarein?
* K: To... nie jest łatwe. Athamarein przechodzi testy. Ale nie działa. Przez miesiąc nie działa. Od czasu awarii.

Komodor Oroginiec.

* O: Komodor Arianna Verlen?
* A: Komodor Oroginiec? Twoja podopieczna daleko zaszła.
* O: (rozpromienił) To wielka przyjemność, oglądam Sekrety Orbitera regularnie.
* A: Widzi pan jak źle pójdzie, będzie pan bohaterem kolejnego odcinka
* O: mi jest dobrze na uboczu...
* A: Ma pan problem z Athamarein?
* O: nie wiedziałem, że natkną się na Syndykat (nie zauważył że Arianna nie wie i kontynuuje). Syndykat przejął statek cywilny i Athamarein próbował go powstrzymać. Nie chcieli zabić cywilów i prawie zginęli. Gdy statek cywilny trafił troszkę inaczej, Athamarein by wybuchła. No i wysadzili ten statek. Ale Athamarein nie była taka sama i nie działa dobrze. Tak samo jak załoga. Widać resentymenty. A walczyłem by to powstrzymać... (pakiet próśb o przeniesienie)
    * "stanie w porcie im nie służy, chciałem ich zintegrować na akcję"
* (komodor przypadkowo powiedział Ariannie coś czego nie powinien)

Arianna robi spotkanie z oficerami Inferni i komodor Oroginiec.

* Są wszystkie dane o Athamarein dla Klaudii
* Athamarein była prawidłowo naprawiona i wszystkie audyty przeszły. Ale jak przeszła atesty, załoga wróciła... zaczęło się dziać.
    * gdy Athamarein była pilotowana przez stoczniowców - zero problemów. Działa jak staruszek którym jest.

Lista uszkodzeń, niesprawności itp:

* problemy z łącznością i komunikacją, autozagłuszanie siebie samej
* zacięcie się broni / ryzyko, że strzeli sama
* fałszywe alarmy: od wycieku paliwa po możliwe kolizje
* systemy nawigacyjne pokazują niemożliwe i fałszywe lokalizacje
* przegrzewanie silników, błędne odczyty reaktora

"Coś wysyłane z zewnątrz do aktywacji systemów?" -> może.

Arianna szuka anomalii na statku, na bazie danych. Tr Z +2:

* X: nie do końca można ufać danym z Athamarein. A nuż system raportowania danych nie działa?
* X: nic nie wyciągniesz z samych danych. Bez "powąchania" statku będzie ciężko
    * do tej akcji Athamarein ma nienaturalnie mało niesprawności jak na ten wiek jednostki
    * Athamarein nie działa "w inny sposób" niż powinna, nie jak Królowa. Nie działa tylko jak 

Klaudia proponuje, by ekipa z Inferni wzięła Athamarein na przejażdżkę. Jeśli nic dziwnego się nie stanie i w rękach stoczni nic się nie działo, to to kwestia połączenia Athamarein z załogą. Można wziąć i zrobić test. "Tylko nasza załoga" a potem test "nasza załoga steruje ale ich oficerowie na pokładzie". A potem "ich oficerowie sterują ale my na pokładzie" itp... Zawęzić problem i postawić jakieś hipotezy.

Następnego dnia ekipa z Inferni (łącznie z bardzo smutnymi członkami Inferni którzy już nie mają wolnego) objęli Athamarein. 

* Kamil: "komodor Verlen, jesteśmy na pokładzie ruiny. Tylko Ty możesz doprowadzić ją do działania." (absolutne przekonanie)

Eustachy patrzy na Athamarein okiem fachowca:

* wentylatory za głośne, niewyregulowane
* niewielkie problemy i błędy
* (książeczka serwisowa: powinno być OK. Logi Persefony: nikt tego nie robił)

Eustachy patrzy na harmonogramy pracy. Dziennik konserwacji. Czy ktoś nie sabotował, przyklej plasterek itp.

Tr Z +3:

* X: trudno o jednoznaczność, a przynajmniej - warto to połączyć ze zdaniem inżyniera
* Vr: STARE logi i NOWE logi (przed wypadkiem - po wypadku) się różnią
    * przed wypadkiem załoga Athamarein dbała o Athamarein
        * nigdy nie był zadbany ale nie było kłopotów
    * po wypadku przestali dbać o Athamarein
* Vz: (punkt zaczepienia, po wypadku przestali dbać - szersza analiza, manifesty okrętowe, plany podróży... zrozumieć sieć)
    * wiek Athamarein. Praktycznie nie są fabrykowane do niej części.
    * Athamarein od czasów wypadku NIGDY nie wystartowała "na dłużej". Nic nie sprowadzono, nic się nie działo...
        * były transporty różnych rzeczy na pokład na statek, nie zgadzają się z manifestem
            * jedzenie dla tienów jest najtańsze i najpodlejsze
            * tienowie NIE WIEDZĄ o tym co jedzą MYŚLĄ że orbiter daje takie żarcie. To jest od zawsze.
                * (tu Lars komentuje: "...czy my na Inferni jemy to co powinniśmy?")
        * Athamarein, zgodnie z raportami ma 130% standardowego pola magicznego
    * Athamarein ma... nie wszystkie ruchy Athamarein są kompatybilne z działaniami Athamarein.

Arianna robi ćwiczenia, w granicach rozsądku, jak statek powinien dać radę. A Klaudia rozstawia odpowiednie czujniki - mechaniczne, magiczne itp.

Tp +3:

* X: Załoga Inferni nie przywykła do jednostki o stanie Athamarein. Zwłaszcza z Eleną za sterami. Elena uszkodziła Athamarein. Za ostry zwrot. ALE - Athamarein jest w "lepszym" stanie niż powinna być.
    * Athamarein jest w lepszej formie

Arianna mówi załodze, że się sprzęga ze statkiem. "Jest zajebisty". Czy zadziała lepiej?

* V: Athamarein w rękach Arianny prowadzi "dobrze".
* Vr: Athamarein cudów nie zrobi, daje znak o sygnałach alarmowych, ale... dobra jednostka. Energia przeszła przez nowo wymienioną jednostkę.
* V: (Elena)
    * Elena się rozłączyła ze statku, z niepokojem: "komodor Verlen?"
    * A: Eleno?
    * E: Athamarein ze mną walczy.
    * Klaudia: ?
    * E: Robię ostry manewr. Athamarein robi... nieco inny manewr. Łagodniejszy. O podobnym wyniku. Mniej obciążający. (nie umie tego wyjaśnić; Elena się integruje)
    * A: Jednostka skorygowała manewr, ale nie robi nic poza tym czego chcesz?
    * Persefona: wszystkie polecenia są wykonane prawidłowo, nie rozumiem
    * E: wiem. Ja to robię inaczej. To nie to o co mi chodziło.

Klaudia dociąża AI, dorzuca jej stałą analizę zasobów - czy nie ma wycieków. (NIE MA. AI jest całkowicie... "poza tym", nic nie pasożytuje.)

Czas na oficerów. Oficerowie używają załogi Inferni, jako pasażerowie. I Arianna robi test.

Pasażerowie (oficerowie): 

* Błażej Sowiński: elegancki, starstruck, TAK BARDZO PRÓBUJE WYGLĄDAĆ JAK KAPITAN. Monitorował stan swoich oficerów.
* Andrzej Gwozdnik: "jeden więcej dzień"
* Uśmiechniczka Konstrukcjonistka Aurora Diakon: słitfocia z Arianną. 
    * A Arianna zachęca całą załogę.
* Hiacynt Samszar: młodszy oficer ds zaopatrzenia. (17-18 lat). Mega speszony, nieśmiały. 
* Remigiusz Alkarenit: stoi z boku, z neutralną miną.

(Remigiusz próbował przenieść się kilka razy)

Arianna próbuje zrobić test jednostki. Na "pełnym obciążeniu" o którym wie że sobie poradzi - od dołu w górę i w górę. Wyraźnie oficerowie Athamarein są niepewni. Remigiusz chciał się odezwać, ale kapitan był pierwszy.

* B: "Komodor Verlen, z całym szacunkiem, ja wiem, że wiesz co robisz, ale Athamarein nie da rady. Jestem to winny mojej załodze."
* A: "Czemu ma nie dać rady? Dobrze się trzyma jak na swój wiek, dobra jednostka."
* B: "Z całym szacunkiem, ma pewne problemy mimo swojej legendarnej heroiczności."

Klaudia ma pomysł. Czy jest że Athamarein ma jakąś kadencję oczyszczania z załogi? Jest wzór w byciu zniszczonym? Bo to za dużo razy.

Tr Z +3:

* V: na przestrzeni WSZYSTKICH starć Athamarein, populacja Athamarein kończy "bardziej ranna" niż statystycznej jednostki ale dużo mniej zabitych.
    * patrząc na dane, "Athamarein po prostu ma szczęście"

Arianna pokazuje kapitanowi, że można robić manewry i to działa. "Ja wam pokażę".

Tp +3:

* V: Oficerowie nie wierzą. Nie wierzą. Athamarein w rękach Eleny śpiewa. 
* V: Mimo że oni uwierzyli, MIMO tego wszystkiego, Athamarein nie działa lepiej ani gorzej.
    * Gdy oni dowodzą Athamarein, Athamarein dalej działa, nie ma awarii, nie ma nic.
    * Athamarein działa, jak wszyscy tu są.
        * Błażej Sowiński czuje HONOR tej jednostki. On nie chce lecieć na inną. On ma sympatię do Athamarein. To świetna jednostka.

Arianna spotyka się z Remigiuszem. "Co tu się dzieje".

* R: Komodor Verlen, co ma się dziać. Aurum.
* A: Kapitan Sowiński nie wyglądał na kiepskiego dowódcę, widywałam gorszych.
* R: Ja też. Ale kapitan Sowiński nie nadaje się na front.
* A: Dlaczego?
* R: Pani Komodor, nie jest w mojej gestii krytykować zwłaszcza wystarcząco kompetentnego kapitana.
* A: W mojej gestii nie jest wnioskować do kogoś wyżej o zmianie załogi. Muszę zrozumieć problem. Uważasz, że nie jest odpowiedni na front - czemu? Luźniej jest oskarżeniem.
* R: (uśmiech) Ja i moja gadatliwość...
* R: On nie ufa w swoje umiejętności i za bardzo ufa swojej załodze. Za bardzo chce wszystkich uratować i nie chce być twardym kapitanem, bo "Sowiński"
* R: Athamarein zasłużyła na lepszą załogę
* A: Są problemy?
* R: Zaczynając, że jemy kocią karmę? Puszki z sardynkami?
* R: Pierwszy oficer nie pełni roli XO. Nie zajmuje się ludźmi. Nie ma "serca". Kapitan pełni rolę XO. Gwozdnik uważa, że on powinien być kapitanem.
* R: Uśmiechniczka chce żyć jak Arianna. Nie jest inżynierem, jest inżynierem z "tiktoka". Robi rzeczy, jest dobra, ale nie skupia się na inżynierii a na popularności i chłopcach.
* R: Hiacynt jest... dzieckiem. Nie chce podpaść, chce być lubiany, nie zna życia, rozmawia ze statkiem...
* R: Załoga... nie traktuje ich jak oficerów a jak tienów, zwłaszcza przez Uśmiechniczkę oraz Gwozdnika.

Zdaniem Remigiusza, jeśli ten statek wyleci na front - wszyscy zginą. A dotąd same bezpieczne zadania. "I jedna sytuacja niebezpieczna, z randomowym piratem - i prawie wszyscy zginęli. Nie wiem jak przeżyliśmy. Nie powinniśmy być żywi. Byłem jedyną osobą, która cokolwiek monitorowała aktywnie a nawet mnie zaskoczył ten atak. Nie wiem jak ten pirat spudłował."

Remigiusz nie chce być mentorem. Komodor chciał dać go na mentora. "Athamarein zasłużyła na więcej, nie bycie jednostką treningową. Nie tak zasłużyła jako legenda". Arianna: "częsty los legend" patrząc Remigiuszowi w oczy.

Seria nowych testów.

* Ich oficerowie, załoga Inferni - Athamarein bez zarzutu
* Ich oficerowie, ich załoga - Athamarein bez zarzutu (zawsze)

Hipoteza Klaudii - kombinacja cech załoga - statek, ale jak debugowanie to naprawiło? XD

"Wszystko działa, wszystko w porządku, możecie lecieć" - i nagle awaria. Morale wysokie, ale awaria. Rakieta się zaklinowała i trzeba było to naprawić. Nagle - morale nisko. "Organizm statku odbija organizm ludzki". Remigiusz 'dzięki Seilii, nie przeżylibyśmy tego gdyby był poważny problem...'

Ćwiczenia z inną jednostką - też awaria. W chwili, w której coś może bardzo pójść nie tak, z inną jednostką, Athamarein ma awarię. Awaria jest powiązana ze spikiem energii magicznej. Athamarein sama się uszkadza. Athamarein "nie chce" by załoga się narażała.

TA SCENA JAK PRZY K1 ZAŁOGA INFERNI ROBI AKCJĘ CELNĄ I SIĘ UDAJE.

Kurs w stronę Anomalii Kolapsu. Athamarein słucha poleceń. Athamarein akceptuje każdą załogę i grupę - poza tą konkretną grupą.

Czyli Athamarein chroni swoją załogę.

Athamarein chroni do końca.

Athamarein jest bohaterką.

Teraz tylko wyjaśnić to komodorowi ;-).

## Streszczenie

Athamarein - bardzo stara korweta rakietowa, jeszcze sprzed czasu Astorii - miała spotkanie z rajderami Syndykatu. Sama obróciła się, by Syndykat nie zabił jej załogi (Persefona nic nie wie). Jednak załoga Athamarein jest skłócona, straumatyzowana i niekompetentna - nie poradziłaby sobie na akcji, więc Athamarein zaczęła się sama 'sabotować'. W rozpaczy, ściągnęli załogę Inferni zanim Athamarein pójdzie na żyletki - niech Infernianie odkryją i wyjaśnią, co się stało z Athamarein. W wyniku, Athamarein została oddelegowana jako jednostka szkoleniowa. Czas na emeryturę.

## Progresja

* OO Athamarein: przeznaczona od teraz do misji szkoleniowych. Jej ochrona załogi, jej odporność i niskokosztowość są idealne w tej jakże przecież ważnej roli. Czas na emeryturę.

## Zasługi

* Arianna Verlen: namówiona przez Kircznika, wzięła ekipę Inferni by spojrzeli na stan Athamarein. Diagnozuje załogę Athamarein, rozmawia z nimi, identyfikuje problemy i dochodzi do tego, że to Athamarein odrzuca załogę. Dla Klaudii za mała szansa na to wydarzenie, dla Arianny - znajomości 'ludzkiej natury' - to było prostsze do zaakceptowania. Nie mechanizm a serce korwety.
* Eustachy Korkoran: całkowicie zignorował zaczepki Leony (i pośrednio Alicji), wykazał, że Athamarein jest bardzo starą jednostką i ma anomalnie sprawne systemy - aż do niedawna, kiedy są anomalnie NIESPRAWNE.
* Klaudia Stryk: Athamarein okazała się być dla niej wyzwaniem. Prawidłowo naprawiana, znane logi, ale ANOMALNIE dobrze sobie radzi i nie ma strat. Ale czemu? Ustawia TAI, zmienia załogę, projektuje testy. Nie wpadła na 'serce korwety', bo to brzmi jak anomalia blokowana przez Memoriam, ale Athamarein faktycznie odbiła się w Eterze. Pierwszy raz Klaudia jest zadowolona z tego, że źle zinterpretowała dane.
* Elena Verlen: doskonały pilot, integruje się z Athamarein i powiedziała, że coś jest nie tak - Athamarein "sama" koryguje niektóre rzeczy. Pilotuje Athamarein bezbłędnie gdy zaakceptowała jej specyfikę, choć najpierw ją lekko uszkodziła.
* Leona Astrienko: dokucza Alicji Szadawir, chcąc ją skłonić do walki. Mówi, że Alicja się w nim podkochuje i rzuca weń stanikiem (czystym). Robi siarę Eustachemu, chcąc go wciągnąć w grę - a on po prostu chce mieć spokój. Siedzi w areszcie i Arianna jej nie ratuje :-(.
* Alicja Szadawir: poluje na Leonę, bo nie chce się przed Eustachym zdradzić z podkochiwania. Wściekła, rzuca w Leonę automatem do wydawania chrupków, prawie trafiając w komodora Orogińca. Kończy w areszcie, klatka obok Leony.
* Jakub Oroginiec: komodor pod którym jest tymczasowo Athamarein; prawie został zmiażdżony przez Alicję rzuconym automatem do sprzedaży drinków. Patrzy na Ariannę z TAKIMI oczkami i z radością dał jej Athamarein do przebadania. Nie jest to najlepszy komodor w sensie samodzielności i wyzwań, ale zna swoje ograniczenia i dba o swoich ludzi - tu robi dobrą robotę. Kiepsko trzyma język za zębami (wygadał Ariannie o Syndykacie)
* Kajetan Kircznik: niezmiennie szalony afro-czarny-paw, doradził Orogińcowi kontakt z Arianną; nic się nie zmienił. Nadal chodzi w "przebraniach" (fake mustache), nadal dba o ludzi przy sobie, nadal robi głupie dowcipy i lubi Ariannę :-).
* Błażej Sowiński: kapitan, zna historię Athamarein i nauczył się wszystkiego na jej temat czego był w stanie. Niezbyt pewny siebie i nieco za lekko trzyma ludzi za mordę, ale ma potencjał. Stanął naprzeciw Arianny chroniąc załogę i Athamarein.
* Andrzej Gwozdnik: pierwszy oficer, osoba 'bezduszna'. Arogancki; uważa, że byłby lepszym kapitanem. Całkowicie omija aspekt ludzki - po ostatniej katastrofie, tym bardziej pewny że to jedyna droga. 
* Uśmiechniczka Konstrukcjonistka Aurora Diakon: 'Ula', robi sobie selfie z Arianną i ogólnie nie przeszkadza jej stan Athamarein. Jest dobra, ale skupia się na sławie a nie Athamarein, generując sporo problemów (załoga vs oficerowie). 
* Hiacynt Samszar: inżynier, zapatrzony w Uśmiechniczkę; jako jedyny podejrzewa, że to Athamarein jest problemem sama w sobie (Samszar), nikomu oczywiście słowa nie powie, bo się zbyt boi i wstydzi i nie jest pewny że ma rację.
* Remigiusz Alkarenit: chciałby być na akcjach, chciałby być PRZYDATNY. Nie chce szkolić i mentorować, a do tego kom.Oroginiec go przeznaczył. Chciałby na inną jednostkę, ma dość "niekompetentnych tienowatych". Mało mówi. Za poleceniem Arianny, spróbuje pomóc tym tienom.
* OO Athamarein: bardzo stara już korweta, która Odbiła się w Eterze. Traci nadspodziewanie mało załogi. SAMA zrobiła manewr unikający działa Syndykatu. W chwili, w której załoga Athamarein nie jest zdolna do pełnienia misji, Athamarein zaczęła sama się sabotować, uniemożliwiając im iść na akcję gdzie mogliby zginąć. Pozytywna jednostka o podstawowych cechach Anomalii Kosmicznej, ale nastawionej pro-ludzko.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy

## Czas

* Opóźnienie: 4
* Dni: 5

## OTHER
### Status Skażony

* bardziej wrażliwy na energię magiczną i silniejsza magia, ale mniej kontrolowana
* ZAWSZE: +1X, +1Ob
* DODATKOWO JEŚLI MAGIA: 1V -> 1Vb, 1X -> 1Xb, +1Ob

### Status Rozproszony

* pod wpływem silnych emocji / myśli o czymś innym
* -1V, 3X -> 3Xg, +3Og
* Og reprezentuje "skupiony na swoich myślach i swoim działaniu" lub "działanie automatyczne"
* można wymienić Og na Or - sukces ALE zadajesz sobie ranę by się skupić

### Status Solidnie Ranny

* osłabiony i mniej zdolny do działania
* -1V, 2V -> 2Or (sukces ale dalsza rana); możesz usunąć Or
