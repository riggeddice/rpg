## Metadane

* title: "Turyści na Trzęsawisku"
* threads: olga-regentka-czarnopalca
* motives: teren-morderczy, teren-gdzie-rzadzi-magia, turysci-niesforni, fish-in-water, pojedynek-rywali, escort-quest, biznesowa-okazja, sojusznik-bardzo-dziwny, przesladowany-wyrzutek
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [190102 - Stalker i Czerwone Myszy](190102-stalker-i-czerwone-myszy)

### Chronologiczna

* [190102 - Stalker i Czerwone Myszy](190102-stalker-i-czerwone-myszy)

## Projektowanie sesji

### Pytania sesji

* Czy Pięknotka da radę zdobyć posłuch u Myszy?
* Czy Alan da radę przebić się jako zwycięzca tej operacji?
* Czy nikomu nic złego się nie stanie?

### Struktura sesji: Eksperymentalna eksploracja / generacja

* **Scena, Konsekwencja, Niestabilność, Opozycja, Trigger, Pytania**
* mag u Pięknotki
* 3 Myszy + 5 turystów poszło na Trzęsawisko
* 3 infestowanych turystów w Sferze Pyłu
* 1 mag u Accadoriana

* **Tor Tragedii (skok 1k3/3)**
    * 10: efekty uboczne lub trwałe zmiany
    * 15: śmierć LUB zmiany Pięknotki
    * 20: konieczność natychmiastowej ewakuacji

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. ...

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (20:14)

_Pustogor, gabinet Pięknotki_

Do Pięknotki przyszedł mag. Jeden z łowców nagród z Miasteczka. Onufry Perkolat. Powiedział, że są turyści i dobrze płacą. Pięknotka aż nie wierzy, ale NAPRAWDĘ chodzi tylko o pieniądze. Tu nie ma żadnej mrocznej agendy. Są turyści spoza Pustogoru i chcą obejrzeć Pustogor i Trzęsawisko. Podobno jest groźne. Pięknotka próbuje go przekonać, że to naprawdę zły pomysł niezależnie od kasy (T: SS). Pięknotka zrobiła co mogła, przekonała Onufrego, ale wycieczka wzięła kogoś innego...

Tego samego dnia Pięknotka zgłasza zakusy turystyczne po Trzęsawisku. Standardowy formularz numer osiem.

_Pustogor, gabinet Pięknotki, dzień później_

Komunikat do Pięknotki z Barbakanu. Turyści... a Alan już podobno wchodzi na teren Trzęsawiska. Pięknotka dostanie podwózkę awianem. I faktycznie, dotarła na Trzęsawisko w ciągu 20 minut. I Pięknotka dostała 'in charge'. Ona ma dowodzić, bo Alan może pójść za daleko ze swoim upodobaniem do broni ciężkiej. Zwłaszcza, że on posłucha tego rozkazu.

_Trzęsawisko Zjawosztup, wejście_

Alan przekazał sygnał Pięknotce - jest na Trzęsawisku, w obszarze prowadzącym na Mglistą Ścieżkę. Ma jednego maga, zaatakował go sojusznik z Myszy - więc Alan użył działa i go unieszkodliwić. Poprosił Pięknotkę o wzięcie medikitów. Pięknotka to zrobiła. (Tp:S). Pięknotka dotarła do Alana a komunikując się po hipernecie udało jej się dostać informacje od dwóch z nich. Jeden jest pożerany przez Accadoriana, drugi oszalał.

Pięknotka przechwyciła nieszczęsną łowczynię nagród Myszy zestrzeloną przez Alana i odleciała z nią do punktu kontrolnego. Po drodze ją stymulowała (TpZ:S). Łowczyni nagród się obudziła i w lekkiej malignie zaczęła odpowiadać:

* Poszła z kolegą i koleżanką oraz 5 turystami. Faktycznie, doszli do polany zarodników, ale przedtem lub potem mieli jakieś starcia, niektórzy mieli pouszkadzane pancerze. Ona miała przebicie.
* Jeden kolega wcześniej się odłączył, by ściągnąć na siebie ogień. Pięknotka poznała tego, którego złapał Accadorian.
* Nie wie co się stało z resztą, ale na pewno ktoś został na polanie. Tam gdzie są zarodniki.
* Jest przekonana, że dostała sygnał SOS z okolicy Toni.

Pięknotka szybko wróciła do Alana po oddaniu jej do punktu medycznego. On się przebija przez Trzęsawisko, łącznie z pnączoszponami. Ale Pięknotka wie, że bagno jest dziś nienaturalnie spokojne.

Na polanie Pięknotka widzi trzy osoby. Hipernet potwierdza je jako magów. Są w czymś pomiędzy bardzo brutalną orgią lub walką. Alan patrzy nieco zafascynowany. Przygotowuje sieć unieruchamiającą - (9,3,3=S). Udało mu się unieruchomić całą trójkę. Pięknotka nie rozpoznaje w nich inteligentnych istot, są pochłonięci zarodnikami. Alan zauważył, że tylko on poradzi sobie z transportem tej trójki. Niech Pięknotka idzie na Toń, on potem pójdzie na Accadoriana.

ASSAULT: (3T)

Wtem, Trzęsawisko zaatakowało. Samo. Mnóstwo macek uderzyło w kierunku magów. Alan kazał Pięknotce paść i zamknąć oczy, po czym zdetonował _vitium_. Uszkodził swój power suit, ale otworzył przejście i poleciał, ostrzeliwując się, w kierunku punktu medycznego. Pięknotka została sama - jej zadaniem jest lecieć do Toni. Macki próbują złapać Pięknotkę, ona używa natychmiast chemicznej dywersji by się ich pozbyć (TZ:10,3,5=S). Macki ją zgubiły, Pięknotka szybko poleciała w przeciwną stronę.

Tam, wpadła prosto na pnączoszpona. Ten nie lata. Szybko w niego walnęła by lekko ogłuszyć i wykorzystała snajperkę do usunięcia zagrożenia, niezgodnie z przeznaczeniem. Niestety, karabin się uszkodził w szponach potwora, ale potwór został usunięty. Zawsze coś. Poleciała szybko w kierunku na Toń, ale po drodze zauważyła, że wleciała w chmurę wybuchowych zarodników. Natychmiast wyłączyła silnik i wycofała się na piechotę. Wrogowie nie wiedzą gdzie Pięknotka jest - najbezpieczniej będzie przejść ostrożnie przez tą chmurę, poprzeskakiwać sadzawki i pomanewrować po drzewach. (Tp: S). Udało jej się; bez większych przeszkód dotarła do Toni.

Przy Toni Pięknotka widzi dwóch magów. Jeden mag się wpatruje, drugi jest w Toni. Wszedł do wody. Pięknotka natychmiast ogłusza wpatrującego się w Toń. Drugi mag, natomiast, spojrzał na Pięknotkę. Zaczerpnął energię z wody, z Toni, z energii Trzęsawiska. Pięknotka przeleciała nad Tonią by złapać maga i walnąć nim o drzewo. (Tp:SS). Pięknotce się udało, ale spojrzała w Toń. Przez to ją wciągnęło, nie wymanewrowała i walnęła w drzewo. Ją otrzeźwiło, drugi mag stracił przytomność.

Pięknotka liczy. Ma dwóch. Czterech jest uratowanych. Jeden jest u Accadoriana. O jednym nie wiedzą.

Pięknotka potrzebuje pomocy. Wykorzystała swoje kosmetyki by wezwać znajome sobie wiły. Kiedyś już z nimi rozmawiała i nie mają do siebie wrogości. Musi je przyzwać - używa więc kosmetyków ORAZ swojej magii. (TpM:12,3,2=S). Magia Pięknotki wezwała wiły. Ale jednocześnie, zanim się pojawią, zwabiła inne rzeczy.

ASSAULT: (3T)

Magia krążąca w Toni się przebudziła. Mentalny atak ze strony Toni na Pięknotkę. Terminuska wykorzystała awaryjnego bionta o profilu mentalnym, coś na takie przypadki co ze sobą próbuje nosić. (TpZ:P)...

Pięknotka się obudziła. Naga, wrzuciła ubrania w Toń. Power suit na ziemi. No i... Wiktor Satarail właśnie skończył wstrzykiwać jej jakąś substancję by ją z tego wyrwać. Tańczyła z wiłami i się świetnie bawiła... Wiktor powiedział, że próbował uciszyć bagno i przyniósł jej ostatniego zagubionego maga.

Pięknotka poprosiła wiły o pomoc w transporcie trzech magów. Niech tylko nie pokazują się przy stanowisku lekarzy, bo mogą być rozstrzelane. I chce wywołać efekt wow. (Tr:10-3-5=S). Udało jej się to zrobić. Medycy patrzyli z ogromnymi oczami jak Pięknotka wleciała z kilkoma wiłami. Mniej więcej w tym momencie jej power suit odmówił posłuszeństwa i się wyłączył.

Cóż, dostał magicznie. Na to wpadł Alan. Wiły na niego zasyczały i pouciekały. Pięknotka jako Diakonka się nie przejęła. Pięknotka jest podrapana i lekko pokąsana, ale nie jest ranna. Alan wyraźnie jest ranny, i to nie tak lekko. Ale przyniósł maga. Uratowali wszystkich.

Pięnotka powiedziała, że Alan i ranni powinni jak najszybciej wrócić do Pustogoru awianem. Alan powiedział, że Pięknotka leci z nim - ma zadrapania. Pięknotka się nie zgodziła. Po krótkiej kłótni, Alan złapał Pięknotkę i chciał ją pociągnąć za sobą. Pięknotka wyłączyła suit i włączyła by ją puścił. Alan się przewrócił. Stracił przytomność.

Wszyscy szybko się ewakuowali do Pustogoru.

TOR 12: Efekty uboczne / trwałe zmiany. Zaaplikowane.

**Scena**: (21:49)

_Pustogor, gabinet Pięknotki_

Ktoś wpakował Pięknotce do gabinetu bombę katalityczną gdy jej nie było. Jej wszystkie magiczne komponenty, materiały, wszystko - są porujnowane. Poniszczone. Duże straty. Ludzie, którzy pracowali nic nie wiedzą. Najgłupsze jest to, że to powinno się stosunkowo łatwo dać wykryć.

Ktoś nie zrozumiał lekcji. Pięknotka się naprawdę wkurzyła. Zgłosiła to oficjalnie do Barbakanu, zaznaczając, że to się stało gdy była na akcji. Poprosiła o oficjalne sprawdzenie i adekwatne ukaranie napastników. Barbakan wziął to jako wysokopriorytetowy temat - atak na terminuskę na służbie...

_Pustogor, Miasteczko_

Pięknotka otworzyła oczy. Doszła do siebie. Skończyła ujeżdżać Alana, któremu podstawiła afrodyzjak pod nos. Wpływ tych wszystkich wydarzeń i ataku mentalnego Toni oraz wił, plus stres, obudził w niej pewną anomalię. Zachowała się dokładnie jak wiła. Poszła, odwiedziła i zabrała co chciała. Sęk w tym, że on nie chciał. I oni nawet z Pięknotką się nie lubią...

Pięknotka nie wie jak to rozegrać. Pójść czy zostać. Czy go porwać. Jednak zostanie i poczeka aż Alan się obudzi. Nawet zrobiła mu śniadanie.

Alan się obudził rano. Zdziwił go widok Pięknotki i to, co się stało w nocy. Pięknotka go bardzo przeprosiła - musiała użyć wił a do tego jeszcze zadziałała na nią Toń... i detoks Szpitala widać nie wszystko wykrył, lub nie wszystkiemu był w stanie zapobiec. (Tp:SS). Alan przyjął jej przeprosiny, ale powiedział, że wspiera Adelę - bo bez wsparcia Myszy Adela nie ma żadnej drogi ani żadnych możliwości. Na pytanie czy coś Adeli grozi Alan odparł, że interesują się nią interesy mafijne - bo jest słaba i zdesperowana. Ale co on się przejmuje, Adela nie ma w końcu żadnego znaczenia.

Alan zapytał Pięknotkę, czy wszystkie wiły są jej przyjaciółkami. Powiedziała, że wiele wił jest jej przyjaznych. Powiedział, że zawsze zabijał wszystkie - kill on sight. Czemu mu nie powiedziała? Pięknotka powiedziała, że nie słuchałby jej. Alan uśmiechnął się - zastanawia się kto z nich jest większym potworem, on (zabijający wiły) czy ona (nie prosząca go o to).

Wpływ:

* Ż: 1 (12)
* K: 3 (5)

**Epilog** (22:17)

* Cała ósemka magów z Trzęsawiska zostanie opierniczona i płacą ogromne kary
* Pięknotka wie, że Wiktor Satarail nie jest potworem. Przynajmniej tyle, dla tego było warto przyjść
* Pięknotka po raz pierwszy ujawniła swoje zaprzyjaźnione wiły. Alan powiedział, że nie będzie do nich strzelał jak nie będą doń podchodzić.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |         |             |
| Kić           |         |             |

Czyli:

* (K): 
* (Ż): 

## Streszczenie

Grupa turystów spoza Pustogoru odwiedziała Trzęsawisko wraz z trzema łowcami nagród. Pięknotka i Alan poszli ich ratować. W samą porę - sygnał SOS pokazał, że jest już późno. Udało się wszystkich uratować, za co... Pięknotce ktoś zdemolował gabinet. Pięknotka i Alan doszli do pewnego porozumienia w sprawie Adeli - znają jej imię. Acz chyba Alan się o nią martwi.

## Progresja

* Pięknotka Diakon: ma grupę zaprzyjaźnionych wił na Trzęsawisku Zjawosztup.
* Adela Kirys: jest nią zainteresowana mafia i ciemniejsze interesy.

## Zasługi

* Pięknotka Diakon: uratowała grupę turystów z Alanem, pokazała swoje zaprzyjaźnione wiły i skończyła przelatując Alana. O tym nie rozmawiają i nikomu nie mówią.
* Alan Bartozol: pokazał, jak artyleryjski servar poradzi sobie na Trzęsawisku. Świetny terminus, acz skończył nieprzytomny mimo stymulantów. Uratował wielu.
* Wiktor Satarail: nie jest Skażonym potworem i chyba nie jest sterowany przez Saitaera. Pomógł Pięknotce - a mógł zrobić cokolwiek. Chroni Trzęsawisko i powstrzymywał jego działanie.

## Frakcje

* Czerwone Myszy Pustogorskie: ponieśli koszty za głupie działania trzech członków, którzy wzięli turystów

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Gabinet Pięknotki: gdy Pięknotka była na akcji, bomba katalityczna zniszczyła jej część materiałów. Barbakan prowadzi śledztwo.
                        1. Trzęsawisko Zjawosztup:
                            1. Toń Pustki: starcie pomiędzy dwoma magami, Pięknotką, Tonią i grupą wił. I gościnna wizyta Wiktora Sataraila.
                            1. Sfera Pyłu: obszar, gdzie są różnego rodzaju pyły, zarodniki i gazy. Trzeba było trzech magów stamtąd ratować.
                            1. Accadorian: potężna, upiorna "polana" o nieprawdopodobnej sile ognia. Alan odbił stamtąd maga, acz prawie zginął.

## Czas

* Opóźnienie: 1
* Dni: 2

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* nic

## Wykorzystana mechanika

1811
