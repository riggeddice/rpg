## Metadane

* title: "Wojna w Złotym Cieniu"
* threads: pronoktianska-mafia-kajrata
* motives: wojna-domowa, integracja-rozbitej-frakcji
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [230704 - Maja chciała być dorosła](230704-maja-chciala-byc-dorosla)

### Chronologiczna

* [230704 - Maja chciała być dorosła](230704-maja-chciala-byc-dorosla)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ?
* Struktura: 
    * integracja-rozbitej-frakcji
* CEL: 
    * MG
    * Drużynowy
        * Przejąć kontrolę nad Złotym Cieniem dla Kajrata

### Co się stało i co wiemy

* Czym jest Grupa? Czemu jej istnieje jest wartościowe?
    * Złoty Cień Karmazynowego Świtu - przestępcza grupa concierge pomagająca dominującemu rodowi Samszarów w tym czego chcą w tym luksusowym miejscu
* Dlaczego Grupa została bez głowy / dowodzenia?
    * Szefostwo zostało wyrżnięte po aferze z Mają.
* Jakie są 3-4 Kluczowe Zasoby należące do Grupy?
    * Lista Samszarów z którymi da się współpracować i którzy są zainteresowani tego typu rzeczami, też znajomości
    * Legalne biznesy wspierające ZCKŚ finansowo
    * Klinika bioformacji Oteriiel w niedalekim Płomienniku
    * Magazyny dóbr wszelakich, też egzotycznych
    * Grupy wyspecjalizowane w pozyskiwaniu odpowiednich osób i przerzucie ludzi
* Jakie są 2-3 siły z różnymi Agendami w ramach Grupy?
    * **Lojaliści**: chcą utrzymywać współpracę z Samszarami jak zawsze
        * impuls: integracja
        * mają: biznesy, magazyny, większość siły ognia, morale, Samszarów
        * słabość: chaos, brak dowodzenia
    * **Bioformaci**: chcą się skupić na Oteriiel i stanowić niezależny byt, acz dalej współpracujący
        * impuls: fortyfikacja i negocjacja
        * mają: morale, wiedzę, Płomiennik
        * słabość: łańcuch dostaw, mało pieniędzy
    * **Secesjoniści**: mają dość tego że są zabawką w rękach Samszarów i ich dowodzenie może umrzeć i to nie z jej winy
        * impuls: grab and run
        * mają: część biznesów, siłę ognia
        * słabość: niska konsolidacja, oportunizm
* Jakie są 3-5 pomniejsze siły w ramach Grupy?
    * **Łowcy**: distant agents; pozyskują wszystko czego Samszarowie sobie życzą. Teraz nie wiedzą czy nie lepiej znaleźć lepszy sojusz inaczej
        * impuls: find allies, mercenary
        * mają: specjalistyczne grupy
        * słabość: rozproszeni, daleko
    * **Przemytnicy**: czemu nie zarobić WIĘCEJ?
        * impuls: pieniądze
        * mają: magazyny, środki, siłę ognia, tajne aukcje
        * słabość: oportunizm
    * **Ekstatycy**: eksperci od przyjemności i luksusu, z przyjemnością znajdą lepsze warunki gdzieś indziej
        * impuls: pieniądze, okazje do badań
        * mają: wiedzę, kompetencję, część winorośli, narkotyki
        * słabość: niska siła ognia
    
### Co się stanie (what will happen)

* F1: Znalezienie i pozyskanie dobrej subfrakcji od której można zacząć Integrację
* F2: Obrona swojej subfrakcji przed innymi subfrakcjami (oraz siłami zewnętrznymi), 'stop the bleeding', ustabilizowanie swojej roli dowódczej
* F3: "Fire and motion" - zajmowanie innych subfrakcji i ważnych zasobów zanim zrobią to inni; wojna o subfrakcje
* F4: Konfrontacja z głównymi rywalami o kluczowe zasoby
* F5: Ostateczna Konsolidacja - które zasoby zdążymy utrzymać, które subfrakcje zintegrować? Co zatrzymamy zanim zabezpieczymy okno kradzieży i ucieczki?
* INNE: nielegalne wyścigi, podziemne walki, czarny rynek, orgie

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* .

### Scena Zero - impl

.

### Sesja Właściwa - impl

Złoty Cień został złamany. Doszło do dezintegracji. A w tym wszystkim - Amanda Kajrat, pełniąca rolę egzekutora Ernesta Kajrata. Amanda siedzi z uśmiechem w Gwiezdnej Rozkoszy. WIEMY, że doszło do dezintegracji Cienia. Wiemy, że się porozpadał na frakcje i najsilniejsi są Lojaliści, którzy chcą by wszystko wróciło do normy pod Samszarami. Ale są też inne subfrakcje.

Amanda dalej jest ukryta w sentisieci i dalej jest oficjalnie powiązana z Karolinusem i Eleną. A dzięki Karolinusowi Amanda ma kilka kontaktów na "urzędników" Złotego Cienia - ostatnio przecież szukał i sprawdzał. Miło z jego strony.

Nadia, pomniejsza agentka Cienia która rozmawiała z Karolinusem. Idealna jako pierwszy punkt zaczepienia. Dzięki Karolinusowi Amanda wie, że Nadia pracuje jako "organizatorka wydarzeń kulturalnych" w prywatnej firmie, niedaleko za Karmazynowym Świtem. Niewielki biurowiec, okiem Amandy mający lepsze zabezpieczenia niż by się spodziewała. Ale nadal nie dość na nią. A Amanda ma swój ścigacz. Na oko - kilkadziesiąt osób w biurowcu. Co ciekawe, mają wzmożone defensywy. Są aktywni. To wskazuje na problemy.

Amanda bierze żakiecik, sporo papierów i przechodzi z budynku do budynku, udając, że ma problemy. Zauważa, że strażnicy są uważni ale szukają PROBLEMÓW a nie INFILTRATORÓW. Amanda ogólnie nie wygląda szczególnie groźnie, więc się dopina do jakiejś innej osoby wchodzącej do budynku.

Tr Z +3:

* Xz: Amanda widzi, że ten plan nie zadziała - strażnicy dokładnie przyglądają się każdej osobie. To jest ciekawe. Nietypowe na taką organizację. Coś się musi dziać. Więc Amanda anulowała i się oderwała.

Oki - infiltracja na 'ja tu pasuję' nie zadziała. Ale są dostawy, poczta, kurier itp. Amanda wejdzie w rolę kuriera. "Ty tutaj poczekasz a my za Ciebie to dostarczymy".
 
* V: ten plan działa. Kurier widząc minę Amandy i jej uzbrojenie nawet nie próbuje. Jest skonfundowany. "Ale przecież..." i nie pyta. Nie będzie robił kłopotów. Amanda kazała mu się napić kawy i poczekać.

Amanda przechodzi przez perymetr. Strażnicy nie są bardzo wyczuleni - Amanda ma właściwy strój plus ma niewielką paczkę. ZNOWU wygląda na to, że oni obawiają się _czegoś innego_. Z zewnątrz, ale raczej większych sił? Najazdu? 

* X: Amanda przeszła przez perymetr, ale nie mogła wziąć broni ze sobą. Ma plastikowy nóż. Doszła do odpowiedniego miejsca gdzie paczka została przeskanowana i zostanie odebrana.
* X: DYWERSJA Amandy, pozostawiona maszyna piekielna zadziałała - wyciągnęła strażników, ale koleś na monitorze widział, jak Amanda się wślizgnęła. Włączył wewnętrzny alarm.
* V: Amanda, przechodząc przez ubikację, zmieniła strój szybko by "pasować" i przez krótki czas jest zamaskowana.

Amanda zmienia plan. Jako, że strażnicy myślą, że ona jest w środku, przeszła na portiernię - _wychodzi_ ale oficjalnie. Widzi, jak strażnik (ostatni) rozpaczliwie szuka Amandy na monitorach i nie zwraca uwagi na pojedynczą elegancką bizneswoman. Amanda doń podchodzi i zagroziła mu nożem z szerokim uśmiechem: "jeśli ktoś nas zobaczy, miałeś oko".

* V: Strażnik się osunął na krzesło przestraszony.

.

* Amanda: "Dobry wybór. Dla kogo pracujesz?"
* Strażnik: "Cień! Cholera, nie zabijaj mnie, jesteśmy razem! Co Ci odbiło!" (krzyczoszept)
* Amanda: "Czyli dla KOGO pracujesz?"
* Strażnik: "Rufus Bilgemener, dowodzi tym budynkiem. A Ty dla kogo? Czemu..." (zbliża się nóż, strażnik się zamyka)
* Amanda: "Dokładnie tego człowieka szukałam. Doskonale. Zaprowadź mnie do niego i nie próbuj głupot. Nie chcesz by Twoi koledzy zginęli, prawda? Nie wydurniaj się a nikomu nic się nie stanie. Łącznie z Bilgemenerem."

Strażnik patrzy nieufnie na Amandę, ale nie ma większego wyboru. Idzie z nią. Prowadzi ją bez heroizmu. DALEJ myśli że ma do czynienia z kimś z Cienia.

Lepiej wyglądające biuro. Strażnik patrzy na Amandę wyczekująco. "Mam zapukać czy... szef nie lubi gdy mu się przeszkadza". Amanda "cóż, będzie musiał jakoś przeżyć. Zapukaj i wchodź od razu."

* Vr: Strażnik zapukał i wszedł. Amanda ZASKOCZYŁA Bilgemenera rozmawiającego z dwoma innymi osobami. Zamknęła za sobą drzwi. Ona, Bilgemener i dwie inne osoby. Tylko Amanda jest groźna. Zanim ktoś coś zdążył zrobić, Amanda 'pod ścianę, wszyscy, żadnych cichych klawiszy'. I ma wszystkich tak jak chciała.

.

* Bilgemener: "nie wiem kim jesteś, młoda damo, ale popełniasz straszny błąd. NIKT nie napada Złotego Cienia."
* Amanda: "Widzisz, Bilgemener, cień ma to do siebie że jest tylko echem. Mało substancji. Echo tego co go rzuciło."
* Bilgemener: "kim Ty do cholery jesteś?" /niewiara
* Amanda: "Jestem kimś kto tu wszedł mimo podniesionego stanu alertu. Co więcej, NIKOMU z Twoich ludzi nie stała się jak na razie krzywda."
* Bilgemener: "chodzi Ci o pieniądze, chłopców... przecież jak tylko stąd wyjdziesz, zginiesz"
* Amanda: (powolny uśmiech)
* Bilgemener: "sprawa... osobista?" (nie rozumie) "młoda damo, jeśli szukasz kogoś w Cieniu kto mógł Ci zrobić krzywdę, to nie ja... my zajmujemy się księgowością, fałszerstwami i logistyką."
* Amanda: "dziwka tu, dziwka tam..."
* Bilgemener: "Samszar żąda, Samszar dostaje. Tak działa świat. Nie wiem, łowcy dorwali siostrę? Córkę?" (zbladł) "jeśli jesteś z Samszarów i chodzi o tą młodą, nie mieliśmy z tym NIC wspólnego. NIC!"
* Amanda: "nie. Ale mogę zapewnić, że do Ciebie nie dojdą."
* Bilgemener: "ale my NAPRAWDĘ nic nie zrobiliśmy, ta sprawa była poza nami!"
* Amanda: "myślisz, że jej ojciec w to uwierzy jak chociaż się przewiniecie?"
* Bilgemener: (myśli) "tu mnie masz, tienowie wykonali dość... niesubtelne ruchy. Ja jestem za nisko. Ale sprawdziliśmy - ona naprawdę nie wyszła od nas."
* Amanda: "odwołaj alarm. Porozmawiamy o tym, jak Cię ochronić przed fałszywymi oskarżeniami Samszarów. Masz szczęście, że mi się nic nie stało, bo pojawiłyby się odpowiednie dokumenty wskazujące na Twoją pomniejszą jednostkę."

Bilgemener odwołał alarm, strażnik wypuszczony i zaprosili Amandę do stołu. Bilgemener zaznaczył, że ci ludzie to jego analitycy... robił rozeznanie w sytuacji. Amanda dopuszcza ich obecność.

* Amanda: "nie wierzę, że ta impreza na moją cześć. Skąd ta gotowość? Nie powiecie mi, że to standard - mielibyście kompetentnych ludzi gdyby tak było"
* Bilgemener: "damo... jak się do Ciebie zwracać?"
* Amanda: "możesz mi mówić 'Amando'"
* Bilgemener: "Amando, po... (myśli) oświeconym ruchu tienów Cień jest w pewnej rozsypce. Grupa odszczepieńców napada, kradnie i chce się odłączyć. Każdy ma pomysł na inne jutro. Są oczywiście też lojaliści, którzy uważają, że nic się nie stało i oświecony ruch tienów był prawidłowy i wskazany" (w głosie jest cień niezadowolenia z ruchu Samszarów)
* Amanda: "jesteś w nietypowej sytuacji. Jest niebezpieczna, ale rodzi okazje. Są Samszarowie, którzy... chcieliby, bym była agentem zmiany. Niezależnie od tego, mam pewne zasoby. Mogę pomóc Ci ustabilizować sytuację tak jak tego zechcesz. Przeszedłeś test. Nie jesteś głupi i potrafisz podejmować decyzje w stresie."
* Bilgemener: "rozumiem, że ostatnie ruchy Samszarów były bardziej pozornie impulsywne niż się wydawało?"
* Amanda: "Cień potrzebował pewnego wyczyszczenia. A teraz wymaga poskładania. Nie zależy mi na współpracy KONKRETNIE z Tobą, ale wydajesz się sensowny."
* Bilgemener: "i nie zdradzamy Samszarów?"
* Amanda: "nie widzę najmniejszego powodu."

.

* X: Bilgemener żąda dowodów. Amanda mówi o bazie hiperpsychotroników jako dowodzie współpracy z Samszarami i daje dokładny, step-by-step plan jak zniszczyć biurowiec Bilgemenera bez cienia śladów wskazujących na nią. Broń chemiczna, łatwa do złożenia ze zwykłych nawozów i elementów w Siewczynie.
* Vr: Bilgemener jest przekonany. Będzie współpracował z Amandą.
* Vz: Bilgemener bardziej boi się Amandy i tego co ona reprezentuje niż Samszarów. Lepiej mieć Amandę po swojej stronie.

Bilgemener zrobił Amandzie prezentację:

* Złoty Cień Karmazynowego Świtu podzielił się na sześć subfrakcji
    * Lojaliści: chcą utrzymywać współpracę z Samszarami jak zawsze, chcą integrować i zmusić innych do współpracy
        * Bilgemener był jednym z nich, ale nie był zelotą. Jest otwarty na inne Samszarskie dowodzenie (on jeszcze nie wie)
    * Secesjoniści: mają dość tego że są zabawką w rękach Samszarów i ich dowodzenie może umrzeć i to nie z ich winy. Grab and run.
    * Bioformaci: niedaleko, w Płomienniku jest klinika Oteriiel. Tam się dostosowuje ludzi, ale też są najlepsze formy leczenia (tam trafiła Maja). Oni chcą współpracować ale być niezależni.
    * Frakcje mniejsze
        * Łowcy: distant agents; pozyskują wszystko czego Samszarowie sobie życzą. Teraz szukają sojuszników i chcą najemniczyć.
        * Przemytnicy: oni przerzucają i zajmują się "tym wszystkim". Chcą dorobić bardziej.
        * Ekstatycy: oni budują link z Cieniaszczytem. Wiedza i umiejętności. Jeśli Samszar coś chce, Samszar to dostanie. Oni nie mają twardej agendy - chcą być i zarabiać.
* Lojaliści docelowo wygrają a Secesjoniści się oderwą.
    * Bilgemener chciałby nie płacić za cudze błędy. Nie chce być zdradzony. Maja nie była ich winą - 0/10. Oni byli kozłem ofiarnym.
* Bilgemener ma ten budynek, zna kilka nazwisk Samszarów i ich zainteresowania, ma centrum dyspozytorni, FATALNE defensywy i kończące się pieniądze. Ale ma wpływy, jest znany przez Samszarów i ma sporo wiedzy o tym co i jak.

Złoty Cień ma kilka bardzo wartościowych rzeczy w okolicy:

* Lista Samszarów z którymi da się współpracować i którzy są zainteresowani tego typu rzeczami, też znajomości
* Legalne biznesy wspierające ZCKŚ finansowo
* Klinika bioformacji Oteriiel w niedalekim Płomienniku
* Magazyny dóbr wszelakich, też egzotycznych
* Grupy wyspecjalizowane w pozyskiwaniu odpowiednich osób i przerzucie ludzi
* Narkotyki, środki psychoaktywne itp
* Podziemne walki, rozrywki, aukcje...

Grupa Bilgemenera nie jest uważana za ważną czy groźną. Czy odporną. Nie do końca jest po co atakować. Są bardziej łupem niż aktorem z perspektywy wszystkich. Zdaniem Amandy? Czas by rybka pokazała ząbki. Amanda wie, że Kajrat nie ma problemu z zainwestowaniem w akcję - zrobiła _request_ do Kajrata. Chce dostać 10 solidnych noktiańskich agentów. Do obrony i docelowo ataku. Będą w ciągu 3 dni.

Amanda chce:

* rozpuścić plotki by być zaatakowanym gdy siły Kajrata będą już w pobliżu
* zacząć uderzać w pomniejsze frakcje. Pozyskać DANE o Łowcach i Przemytnikach, by Kajrat mógł swoimi siłami w nich uderzyć i przejąć
* trzecia linia - budować plotki i pokazywać Secesjonistów jako największe zagrożenie dla Lojalistów i celów Samszarów. A drugą linią - pokazać Secesjonistom, że Lojaliści chcą ich zniszczyć, pokazowo.

Wpierw działania Bilgemenera. Plotki i pozyskiwanie danych.

Tr Z +3:

* V: plotki, że Bilgemener ma kasę i sprzęt godny zdobycia przy niskich defensywach (za kilka dni); przyciągnie jakiś plebs.
* Vz: (podbicie) Bilgemener dał radę dowiedzieć się o niektórych szlakach i osobach możliwych do przechwycenia przez Kajrata z Łowców i Przemytników
* X: (podbicie) Bilgemener jest uważany za Gracza. Za frakcję a nie tylko zasób.
* X: (manifest) Bilgemener dostał ostrzeżenie - poważne ostrzeżenie od Lojalistów by się nie mieszał.
* V: Amanda dostaje informacje gdzie można spotkać znanego i lubianego przez Secesjonistów Secesjonistę. Koleś ma ksywę 'czacha', jest łysy, silny i kocha życie. Taki "opiekun" ale on najgłośniej mówi, że Samszarowie zachowali się nie fair. Idealny cel dla Amandy.

Na tym się zatrzymujemy; przekazujemy dane Kajratowi. Niech złapie niektóre, więcej na razie nie możemy.

Amanda wie, że w nocy 'Czacha' będzie oglądać nielegalne wyścigi niedaleko Karmazynowego Świtu, najpewniej z co najmniej jedną panienką.

Bilgemener wprowadza jedną ze swoich panienek. Nadię. Nadia nie chciała wracać do biznesu. Nie ma wyjścia. Zadaniem Nadii jest przechwycenie panienek 'Czachy' i ich wycofanie, bo ktoś ze Złotego Cienia chce poznać Czachę bliżej i one NIE CHCĄ tu być w tym czasie.

Tp +3:

* V: (podbicie) Nadia pozbyła się lasek
* V: (manifest) Nadia nie została wykryta
* V: Amanda może podejść do 'Czachy' gdy ten nie ma strażników

Amanda poszła na full cuteness - ucharakteryzowana, co złośliwie, troszkę na Maję. Nie da się zrobić z Amandy Mai, ale dla Amandy był to niezły dowcip. Gdy Czacha szukał swoich dziewczyn, Amanda go zaczepiła. Drapieżnie uwodzi Czachę i chce, by on z nią poszedł nie dbając o okoliczności i potencjalne zagrożenie.

Tr Z +3:

* Vr: (podbicie) Czacha weźmie Amandę do domu, ale będzie miał się trochę na baczności.
* V: (manifest) Czacha będzie CAŁKOWICIE zaskoczony Amandą i jej działaniami.
* V: (eskalacja) To wyglądało na działanie czarodziejki lub viciniusa. He went to easy too fast. Nie ma śladów po Amandzie (sentisieć).

W pokoju, Czacha i Amanda. Poszli do łóżka. I jak doszło co do czego, Amanda zrobiła perfekcyjny cios nożem. Egzekucja na miejscu. Potem posprzątała po sobie (chlor rozwiązuje większość problemów) i zostawiła karteczkę "TAK się odchodzi od Samszarów". A Bilgemener rozpuszcza plotki, że Lojaliści chcą pozabijać i wyrżnąć Secesjonistów.

Ex Z (Czacha) +4:

* Vz: Obie frakcje są skupione przede wszystkim na sobie. Zimna wojna między Lojalistami i Secesjonistami. Nie skupiają się na Kajracie.
    * Secesjoniści chcą się wycofać i nakraść jak najwięcej
    * Lojaliści chcą ochronić Złoty Cień i utrzymać go dla Samszarów
    * A Amanda? Amanda ma wreszcie otwarte pole manewru.

3 dni później przybyło wsparcie od Kajrata...

## Streszczenie

Amanda Kajrat zdobywa Złoty Cień dla Nocnego Nieba. Znalazła niewielką subfrakcję Bilgemenera i pokazała mu, że Samszarowie szukają często kozłów ofiarnych. Amanda wezwała wsparcie z Nieba i destabilizuje Lojalistów przeciw Secesjonistów by kupić czas, przygotowując pułapki na mniejsze grupki by ich przechwycić. A wszystko to pod patronatem Samszarów (którzy niekoniecznie o tym wiedzą).

## Progresja

* Amanda Kajrat: dalej wykrywana jako powiązana z Karolinusem i Eleną, dalej ukryta w sentisieci i pasywnie niewidoczna

## Zasługi

* Amanda Kajrat: inteligentnie zinfiltrowała bazę Złotego Cienia Bilgemenera, po czym obiecała mu, że osłoni go przed gniewem Samszarów. Ściągnęła wsparcie od Kajrata by na bazie Bilgemenera odbudować Złoty Cień w nowej formie. By kupić czas, rozpaliła konflikt Secesjoniści - Lojaliści przez zabicie Secesjonisty udając Lojalistkę.
* Rufus Bilgemener: biurokrata, który całkowitym przypadkiem został 'donem mafijnym' przy Amandzie Kajrat, bo tylko na jego bazę (przez Nadię) Amanda miała namiar. Wyjątkowo rozsądny jak na średniej klasy managera Złotego Cienia; nieszczęśliwy, bo Samszarowie w jego oczach zdradzili lojalnych agentów Cienia. Bo to nie oni skrzywdzili Maję Samszar.
* Nadia Obiris: bardzo niechętnie, ale wprowadzona ponownie do działań operacyjnych. Przekonała dziewczyny 'Czachy' z Secesjonistów, że ktoś ze Złotego Cienia chce się spotkać z 'Czachą'.

## Frakcje

* Złoty Cień Karmazynowego Świtu: ulega dezintegracji; podzielił się na 3 główne frakcje (Lojaliści, Secesjoniści, Bioformaci) i kilka mniejszych. Amanda Kajrat wyszarpuje zeń grupy by przekazać je Nocnemu Niebu.
* Nocne Niebo: na terenie Karmazynowego Świtu (Samszarowie) pozyskali grupę Bilgemenera, choć on o tym jeszcze nie wie. Też pomniejsze grupki 'far scout' Złotego Cienia.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Karmazynowy Świt: miasto w okowach wojny domowej różnych subfrakcji Złotego Cienia
                                1. Centralny Park Harmonii
                                    1. Gwiezdna Rozkosz
                            1. Karmazynowy Świt, okolice: lokalizacja w okowach wojny domowej różnych subfrakcji Złotego Cienia
                                1. Strefa ekonomiczna
                                    1. Biurowce: tu znajduje się niewielka baza Bilgemenera, gdzie Złoty Cień ma dyspozytornię, call center i pracę biurową. Objęte przez Amandę Kajrat.
                                1. Stadion Mistrzów: miejsce nielegalnych wyścigów, gdzie pozwalają na to Samszarowie by Złoty Cień też miał gdzie się wyszaleć

## Czas

* Opóźnienie: 3
* Dni: 4

## Specjalne

* .

## OTHER
### Fakt Lokalizacji
#### Karmazynowy Świt

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Karmazynowy Świt: luksusowe miasteczko Samszarów; ekonomia oparta na usługach, ze szczególnym naciskiem na turystykę, wellness i rozwój osobisty
                                1. Centralny Park Harmonii
                                    1. Eko-spa
                                    1. Sztuczne Plaże
                                    1. Ogrody medytacyjne
                                    1. Pałac Harmonii: luksusowe miejsce odpoczynku i epicki hotel
                                    1. Gwiezdna Rozkosz: mniejszy love hotel
                                1. Rezydencje
                                    1. Szkoły i sale treningowe
                                    1. Biblioteka
                                1. Zewnętrzny pierścień wygaszający: zapewnia ciszę i spokój
                                1. Kraina Winorośli: winnice, winiarnie, restauracje
                            1. Karmazynowy Świt, okolice
                                1. Miasteczko Płomiennik
                                    1. Klinika Oteriiel: zajmują się wszystkim co jest potrzebne odnośnie modyfikacji bioformy, korupcji i dostarczania młodych dam i lordów do towarzystwa
                                    1. Domy
                                    1. Komenda Straży
                                    1. Tereny Rolne
                                