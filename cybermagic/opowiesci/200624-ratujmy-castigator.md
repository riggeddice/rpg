## Metadane

* title: "Ratujmy Castigator"
* threads: triumfalny-powrot-arianny
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [200415 - Sabotaż Miecza Światła](200415-sabotaz-miecza-swiatla)
* [200623 - Adaptacja Azalii](200623-adaptacja-azalii)

### Chronologiczna

* [200415 - Sabotaż Miecza Światła](200415-sabotaz-miecza-swiatla)

// też musi być do 2 tygodnie po: [200623 - Adaptacja Azalii](200623-adaptacja-azalii)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* .

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

* .

### Opis sytuacji

* .

## Punkt zero

* .

## Misja właściwa

Wszystko zaczyna się od Leszka Kurzmina - starego przyjaciela "od zawsze" Arianny, z którym kiedyś Arianna studiowała razem na Akademii Orbitera (jedno z większych miejsc na tytanie). Leszek podkochiwał się w Ariannie, ale był dżentelmenem - nigdy do niczego nie doszło. On z linii militarnej, ona arystokratka z rodu Verlen. Arianna była popularna w Akademii, Leszek był bardziej nerdem. No i Arianna skończyła jako komodor, Leszek jako dowódca OO Castigator.

I wszystko zaczęło iść Nie Tak.

Castigator jest dość ważną jednostką. Jest to w końcu potężne działo artyleryjskie na orbicie Astorii. To jednak spowodowało, że mnóstwo arystokratów Aurum uznało, że jest to jednostka Ważna i Prestiżowa - a co ważniejsze, nie ma dużego ryzyka że kiedykolwiek Castigator pójdzie na pole bitwy; wojny w końcu nie ma. Noctis już nie walczy przeciwko Astorii.

Leszek patrzy w rozpaczy, jak jego ukochany Castigator traci morale i wydajność. A oficerowie i w ogóle jednostki na pokładzie Castigatora osiągają rekordowo niską wydajność - poniżej 25% efektywności (dla porównania, Infernia ma bardzo wysokie 85%). Tak więc Leszek odezwał się do starej przyjaciółki, Arianny - niech Arianna przyjedzie ze swoim najlepszym sabotażystą (Eustachym) i pomoże ocalić Castigator.

Arianna bardzo zdziwiła się tą prośbą. Fakt, MUSI wziąć Leonę z Kontrolera; ona po prostu pakuje się w kłopoty hobbystycznie, więc prośba Leszka jej pasuje - ale jak to nie opanował Castigatora? Przycisnęła go; może mu pomóc, ale musi wiedzieć w czym. I Leszek zaczął sypać:

* możni idą do góry
* on ściągnął "kwiat" arystokracji, zwłaszcza frakcji antywojennej na Castigator
* on chce zniszczyć tą frakcję raz na zawsze
* chce doprowadzić do tego, by ich podegradowali i przesunęli na normalnych ŻP. Niech się uczą, niech są ŻOŁNIERZAMI a nie ARYSTOKRATAMI UDAJĄCYMI WOJSKO. To groźne. Orbiter na to nie stać.

Cóż - Arianna docenia plan Leszka. Nie jest to coś co by sama zrobiła, ale sama jest z arystokracji. Sama nie cierpi tych pasożytów (w odróżnieniu od normalnych arystokratów). Tak więc wzięła Eustachego, Klaudię i Leonę. To dobra ekipa do demolki. Przy spotkaniu z kapitanem Leszkiem Kurzminem, Eustachy pokazał że dokładnie rozumie o co chodzi i naprawdę mu zaimponował. Leszek dał Eustachemu zielone światło i swoje zaufanie.

Arianna wpadła na prosty, sympatyczny pomysł jak zrealizować plan Leszka. Niech Leona mająca zamiłowania do hazardu sprowokuje arystokratów do zabawy rojem ŁeZ chroniących Castigator. Eustachy zrobi mały sabotażyk i arystokraci zniszczą jakiś efektowny fragment Castigatora. Będzie na nich, będą winni, będzie dało się wprowadzić drakońskie metody.

Plan jest super, ale nie uwzględnił Leony.

* Leszek, w szoku: "Kogo ty tu ściągnęłaś?!"
* Arianna, wzruszając ramionami: "Człowieka"

Na wyścigach holograficznych psów Leona - zgodnie z planem - postawiła niewielką sumę pieniędzy. I od razu wykryła, że jeden delikwent tam oszukuje... Leona jak to Leona - (X) nie powstrzymała się, wklupała mu i zamiotła nim podłogę. Dodała, że takie wyścigi holograficznych psów to są dla plebsu, nudne i oszustwa. Nie to co latania ŁZami. Ale to jest coś na co nie mają jaj, Arianna też zresztą jej nie pozwala.

Leona nie spodziewała się, że pięciu facetów będzie chciało dać jej nauczkę. Gdy była sama w korytarzu, jeden mag rzucił na nią czar paraliżujący. Leona jest odporna na magię dzięki wszczepom, ale cierpliwie czekała. Jak facet zaczął nożem odkrawać z jej munduru fragment, Leona uruchomiła implanty. (XXX). MORZE KRWI. Leona poszła czystą wściekłością, czystym niekontrolowanym sadyzmem.

* Leona, z niewinnym uśmiechem, zdająca raport Ariannie: "Oni pokroili moją bluzeczkę. Ja pokroiłam mu jądra na plasterki. Bez znieczulenia."

Cała piątka z bardzo ciężkimi ranami i straszliwymi traumami trafiła do skrzydła medycznego gdy Leona z nimi skończyła. Klaudia wszystko nagrywała, choć nie na wszystko mogła patrzeć (VV). Leona doprowadziła do maksymalnego TERRORU. A Klaudia puściła to na max(upokorzenie) - Leona jest tylko człowiekiem, napadło ją pięciu magów. Czy TO jest stan Castigatora? (Ex->VO).

Sukces. Absolutny sukces. Castigator wziął się w garść, doszło do małego buntu na pokładzie, postawili się naleśniczej arystokracji i Castigator zaczął działać prawidłowo - kosztem ciężkich obrażeń u wszystkich naleśniczych agentów szlachty.

Leszek powiedział Ariannie, że ta jest jak anioł. Maksymalny friendzone. Arianna jest aż zdziwiona takim friendzone...

Tymczasem, pół dnia później, jak jeszcze wszystkie systemy działały na wyższych obrotach, Klaudia zauważyła że coś jest nie tak. Castigator jest zagłuszany i to w bardzo skuteczny sposób. Klaudia się szczególnie nie przejmowała - wzięła jeden z anomalicznych rezonatorów i wbiła go w konsolę Castigatora.

* V: Wykryła powód skutecznego zagłuszania. To nieludzki intelekt, TAI co najmniej 3 kategorii. Samo w sobie jest to groźne i dziwne.
* O: Rezonator Klaudii Skaził konsoletę, wymaga wymiany. Jest to poświęcenie na które Klaudia jest gotowa. Ale Klaudia wie co to - OO Alkaris.
* V: Alkaris nie wie, że jest namierzony przez Klaudię.
* V: Klaudia wykryła sygnały z Alkaris, rozmowy tam i próby nadania sygnału. Organiczna PO-"TAI", Rozalia, jest uszkodzona. Chce zniszczyć Castigator.
* X: Rezonator Klaudii niestety rozwalił konsolę do końca, eksplodował. No trudno.

Klaudia natychmiast ostrzegła o sytuacji Ariannę. Eustachy zaproponował, żeby wysłać SOS do Kontrolera Pierwszego głównym działem - wystrzelić tak, by Alkaris się nie zorientował łatwo - a przynajmniej, by nie miał czasu na reakcję. Zrobił to.

Arianna przekonała Leszka, że ona pomoże magią - ale że Castigator może być lekko uszkodzony po tym wszystkim, bo anomalną czarodziejkę trzeba uratować. Leszek pomaga swoją mocą oraz otwierając reaktor Castigatora dla Arianny.

* V: Arianna dała radę magią kupić trochę czasu Eustachemu.
* XX: Leszek padł, w drgawkach. Za duża moc Arianny.
* XX: Niestety, cała ta sprawa mocno uderzyła też w napięcia na linii Aurum - Orbiter. Zwłaszcza, że kosztem są też nieszczęśni arystokraci i ich zdrowie.
* V: Arianna dała radę osłabić nanoroje Alkarisa - dzięki temu Castigator nie może być zniszczony przez Alkaris z zewnątrz. Tak więc Rozalia musi wejść na pokład.

I tu wchodzi Eustachy.

* X: Wpierw spróbował ułożyć ŁZ jako żywe tarcze; Alkaris po prostu jest zbyt skuteczny i ominął wszystkie, niszcząc je lub pożerając nanorojami (acz nie może przeprogramować)
* Xm: Eustachy zdecydował się użyć działek i min w hangarach; Alkaris po prostu nanitkami wszystko rozłożył i zdewastował hangary. Rozalia jest na pokładzie.
* VO: Eustachy się wściekł. Zaczął wysadzać SEKCJE Castigatora, by tylko Rozalia była zawsze w próżni. Udało mu się, ale Rozalia używała nanitek by wrócić na pokład i osłonić się przed eksplozją.
* VVVV: SUKCES. Eustachy kupił dość czasu eksplozjami i odrzucaniem części Castigatora, by siły Rozalii słabły pod naporem magii Arianny i ostrzału ŁZ Castigatora.
* X: Ale Rozalia dotarła do Persefony...
* VV: Tyle, że to nie była Persefona. Eustachy zrobił taki "odkurzacz" kanałem od fałszywej Persi. Wystrzelił Rozalię w kosmos z ogromnym odrzutem, gdy ta była słabsza.

Resztę rozwiązali medycy Orbitera po przechwyceniu Alkaris i Rozalii...

Castigator - uszkodzony, wymaga solidnych napraw (w kosmosie), nie może przez pewien czas używać głównego działa i się bronić dobrze, ale Eustachy osiągnął duży sukces uniemożliwiając Rozalii przejęcie / zniszczenie nanitkami Castigatora.

## Streszczenie

Skażona przez Wiktora Sataraila Rozalia ruszyła zniszczyć Castigator. Tymczasem Arianna poproszona przez przyjaciela (kapitana Castigatora) doprowadziła terrorem Castigator do prawidłowego działania i strasznie zwiększyła napięcia między Aurum i Orbiterem. Gdy Rozalia i Alkaris uderzyli, Castigator był jakoś gotowy. Castigator odparł Alkaris i wszystkich uratowano przez stopniowe wysadzanie fragmentów Castigatora i kupowanie czasu.

## Progresja

* Leona Astrienko: pakuje się w kłopoty hobbystycznie jak jest na Kontrolerze Pierwszym. Wniosek - NIGDY nie może być po prostu na Kontrolerze Pierwszym bez nadzoru...
* Leona Astrienko: po tym jak urządziła arystokratów na Castigatorze ma opinię Anioła Krwi I Śmierci. Królowa Terroru. Plus, ma co najmniej jedną wendettę przeciwko sobie.
* Eustachy Korkoran: zaimponował Leszkowi Kurzminowi swoim wyczuciem sytuacji i tym, że kapitan im pomoże. A potem - pokonując Rozalię / Azalię d'Alkaris skażoną ixionem.

### Frakcji

* Arystokracja Aurum: wzrost napięć i zamieszek przeciwko Orbiterowi
* Orbiter: wzrost napięć i defensywa przeciwko Aurum (zwłaszcza ich arystokracji)

## Zasługi

* Arianna Verlen: ma świetne plany jak prowokować arystokratów na Castigatorze, ale nie przewidziała brutalnej skuteczności Leony. Potem używając magii powstrzymywała moc Rozalii z Alkarisa.
* Eustachy Korkoran: miał sabotować Castigator przed arystokratami, a faktycznie niszczył go stopniowo by powstrzymać anomaliczną Rozalię z Alkarisa. Wielki sukces - uratował Castigator wybuchami.
* Klaudia Stryk: rozprzestrzeniła filmik z Leoną by Castigator "wziął się w garść" terrorem; potem dała radę odkryć atak maskującego się Alkarisa anomalizując sensory Castigatora.
* Leszek Kurzmin: kapitan Castigatora i przyjaciel Arianny; poprosił ją o pomoc w "oczyszczeniu" Castigatora z naleśniczej arystokracji. Mocno jej ufa... i ją friendzonował.
* Leona Astrienko: miała tylko sprowokować arystokratów do głupiego hazardu a zrobiła Krwawą Noc pięciu magom którzy ją napadli. Najlepsza zabawa ever - stała się postrachem a filmiki poszły...
* Rozalia Wączak: PO TAI d'Alkaris. Skażona przez Sataraila celem zniszczenia Castigatora; działa jak TAI 3 kategorii. Nie dała rady nawet nanitkami - Eustachy niszczył Castigator wywalając ją w kosmos a Arianna dewastowała jej nanoroje.
* OO Castigator: potężny statek artyleryjski na orbicie Astorii. Opanowany przez naleśniczych arystokratów z Aurum. Po wizycie Arianny (i Leony) - wrócił do formy. Uszkodzony przez Rozalię (tak naprawdę Eustachego), po tym jak Wiktor Satarail Skaził ixionem Azalię d'Alkaris.
* OO Alkaris: nanorojowy statek naprawczo-pozyskujący należący do NeoMil w Orbiterze; pod kontrolą Rozalii Wączak jako PO TAI. Tu: opętany przez Wiktora Sataraila; atakuje Castigator by go zniszczyć.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy
                    1. Akademia Orbitera


## Czas

* Opóźnienie: 5
* Dni: 4
