## Metadane

* title: "Porwana Foodfluencerka"
* threads: esuriit-w-czolenku
* gm: żółw
* players: kić, ken

## Kontynuacja
### Kampanijna

* [190419 - Osopokalipsa Wiktora](190419-osopokalipsa-wiktora)

### Chronologiczna

* [190419 - Osopokalipsa Wiktora](190419-osopokalipsa-wiktora)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. Czy porywacze zginą?
2. Czy życie Alicji zostanie zrujnowane?
3. Czy rodzina pozostanie rodziną?

### Wizja

* .

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena 1: Opowieść o twardym Jacku Brzytwie**

An informant told Jack that an acquaintance of his, Mike, has stolen the plans to one of the buildings in the Bureau Settlement. But there is no reason for Mike to ever steal something like that unless he is working for someone. Jack wanted to find the big fish, not the small fry. Therefore Jack decided to break into Mike’s hideout and steal the plans back.

This may not be the best thing to do if you are a Polish man, but Jack was special – he was born in this city, definitely lower-class, more at home with the dirty tricks then with law and order.

Jack took several people and they broke into the apartment where Mike had the plans and replaced those plans for forged ones. That way Jack was able to monitor every move anyone makes using Mike’s operations. That way Jack knows exactly what happens, when happens and how is it going to happen.

Of course, his boss realized that happened. Jack’s boss – a by the book person – chewed him for that action. Mike got sent for two weeks vacation.

TYDZIEŃ PÓŹNIEJ

And then there was the kidnapping. The building where the plans were stolen had a small company called Visiotronic. There is a company specializing in doing streams in the Internet using some kind of influencers and stars. One of those stars, Alice, got kidnapped by the van while she was entering the building. But Jack knew the truth. He placed cameras strategically and he was prepared to monitor everything which happens and so he noticed that the person who was kidnapped was not Alice.

So Alice was kidnapped, but Alice was not the person which was kidnapped this time. So Alice was kidnapped earlier. He put that information to the boss and the boss decided, reluctantly, to let Jack work on this case. He assigned Molly to him – an excellent forensic in the police department.

Using excellent tools in Podwiert’s forensics lab and investigation systems given to police department by Senetis Laboratory, Molly is able to find who was the person who got kidnapped – it was Eliza. Eliza was a friend of Alice; from reading the messages between the girls Molly was able to decipher the communication stream.

Alice wanted to go on vacations; the stress was too much for her. Eliza wanted to help Alice so she pretended to be Alice and then arranged herself to be kidnapped. But in the same time Alice was really missing - reading Eliza’s messages proved to Molly that Eliza contacted Alice’s fanclub. Some of fans were dying to meet Alice in person and Eliza didn’t think anything bad of it; she told them where Alice was heading. And they were the ones who actually kidnapped Alice, with Eliza knowing nothing.

Molly managed to identify the fun talking with Eliza – his name was Gilbert. By tracing his friends and electronics they have managed to pinpoint the location - Gilbert was hidden in a small mansion about 15 km from Pustogor, in Czółenko.

Jack and Molly went there having a proper amount of drones, weapons and being prepared for everything.

**Scena 2: Spokojne Czółenko i fani Alicji**

They entered the mansion using good old „pizza” excuse. Then, they assaulted Gilbert and started interrogating him. Gilbert wasn’t really willing to protect himself. He was talking:

* Currently, Alice is located in an unused silo. She is unharmed; they just want her to taste food they made for her. They are her fans; a bit less stable than usual group.
* Why did they capture her? They are her friends, they just wanted to be with their idol. There are some Alice fanatics, but most of them are not scary.
* There are 11 more people out there; 4 of them are hard-core fanatics but others are normal.

Jack decided that Gilbert needs to be taught a lesson. He skirt him with the knife and told him that if Gilbert ever gets closer to Alice again he will come at Gilbert while he is sleeping and will harm his family. It was very convincing; Gilbert will never get close to Alice again in his life.

Okay – time to sneak into the silo. That was very difficult but inside the silo they have noticed that there are seven beaten up guys and no Alice inside. They said they are from the police and the fans started talking: the fanatics wanted to go deeper with the relationship with Alice, to dress like her… Overall, creepy stuff. As most of the fans were not really that deep into it, there was a fight. During the fight, fanatics beaten people up and five of them took Alice and drove her somewhere.

Wow, so the trail is lost. Nope. Molly used local investigation tools like cameras everywhere and located the van which took Alice. Then she summoned K9 (canine) unit so they can find Alice - and local beaten up friends gave them smell for the canine. So off they went into the abandoned bunkers.

In the meantime, panicky Alice started losing control of her magic. The fear, the humiliation, the hunger for freedom, the hatred – those feelings created a poltergeist effect. So when our police officers entered the fray they did not deal with the hard-core Alice Army; they dealt with a bunch of panicked people who did not understand what is happening.

Police took down all the assailants and the canine expert managed to calm Alice down to the point the poltergeist effect disappeared. To save Alice and her future the did not allow information on that to go outside these walls. Alice is too young to have her life joined by being some kind of para-magical creature.

And the canine expert says nothing because she wasn’t really sure what she so. Also, she owed one Jack.

**Sprawdzenie Torów** ():

* .

**Epilog**:

* Komisariat policji w Podwiercie ma najnowocześniejszy i najlepszy sprzęt podarowany mu przez Laboratorium Senetis.
* Przyjaźń Alicji i Elizy się rozsypała; Alicja nienawidzi Elizy za to, co się stało.
* Nikt nadal niczego nie wie - życie Alicji się nie zniszczyło.

## Streszczenie

Czarodziejka i foodfluencerka Alicja chciała mieć kilka dni spokoju i uciekła, aranżując swoje porwanie. Tyle, że jej creepy fani porwali ją NAPRAWDĘ. Siły policyjne Podwiertu dały radę rozebrać intrygę i odbić Alicję w Czółenku. Alicja straciła kontrolę nad magią, ale została uspokojona przez policję. Nikt nic nie wie a Alicja została pośmiewiskiem.

## Progresja

* Alicja Kiermacz: została pośmiewiskiem - czarodziejka porwana przez FANÓW i uratowana przez POLICJĘ. Plus, sama sobie to zrobiła (bo chciała uciec z bycia gwiazdą).
* Alicja Kiermacz: zagnieździło się w niej Ziarno Esuriit; ma koszmary senne i jest w niej Mrok Esuriit.

### Frakcji

* Visiotronic: niewielka firma dostała 'publicity' i wzbudziła zainteresowanie osób takich jak Kasjopea Maus.

## Zasługi

* Jacek Brzytwa: podwiercki policjant pochodzący z nizin; niekoniecznie idzie 'by-the-book', raczej preferuje zastraszanie, fałszerstwa i działanie bezpośrednie.
* Małgorzata Wisus: podwiercka policjantka i ekspert od forensics. Specjalizuje się w obróbce materiału wideo i dronach.
* Alicja Kiermacz: czarodziejka i foodfluencerka; porwana przez swoich fanów była bezradna i zasiało się w niej ziarno Esuriit ze strachu i z rozpaczy. Uratowana przez ludzkich policjantów.
* Eliza Farnorz: ludzka przyjaciółka Alicji, początkująca aktorka. Niezbyt rozsądna; zdradziła Alicję jej najlojalniejszym fanom, przez co Alicja została porwana. Zaaranżowała swoje porwanie.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Dolina Biurowa: siedziba Visiotronic została zinfiltrowana przez Mike'a, potem stamtąd Eliza się sama porwała przy ich pomocy udając Alicję.
                                1. Komisariat: ma najlepszy sprzęt forensics w okolicy; dostarczony przez Laboratorium Senetis
                            1. Czółenko: niedaleko Czółenka jest sporo pozakopywanch bunkrów i bieda-szybów.
                                1. Opuszczony Silos: miejsce, które nie do końca pełni swoją podstawową funkcję, ale tam fani przetrzymywali Alicję.
                                1. Bunkry: w jednym z nich Alicja miała Paniczne Skażenie które spowodowało Efekt Poltergeista. Tam też zagnieździło się Esuriit.

## Czas

* Opóźnienie: 5
* Dni: 1
