## Metadane

* title: "Spóźniona wojna Elizy"
* threads: nemesis-pieknotki
* gm: żółw
* players: matpass, olo, nix, zefir, kakonel, strus, onyks

## Kontynuacja
### Kampanijna

* [190402 - Eksperymentalny Power Suit](190402-eksperymentalny-power-suit)

### Chronologiczna

* [190402 - Eksperymentalny Power Suit](190402-eksperymentalny-power-suit)

## Punkt zerowy

### Postacie

* Archie, włamywacz o szerokiej wiedzy m.in. z komputerami
* Augustus, kiedyś oddany sprawie a dzisiaj charytatywna fundacja
* Nix, kiedyś neuropilot; dziś służby bezpieczeństwa
* Vanessa, ryzykantka kochająca pustkowia
* Larry, wesoły pragnący uwielbienia
* Kander, kameleon pragnący prawdy
* Net, włamywacz specjalizujący się w informacjach

### Opis sytuacji

Pytania sesji:

* Czy Eliza otrzyma Tucznika i grupę Lojalistów?
* Czy propaganda Elizy obróci brata przeciwko bratu?
* Czy Eliza rozmontuje Trzeci Raj?

Tory:

patrz "1904-opoznione-zamieszki.docx"

Sceny:

1. dropship: eks-psychopata który w sumie nie chce zabijać ale to zrobi

## Misja właściwa

Zespół znajduje się na dworcu kolejowym, gdy nagle dociera do nich, że... pamiętają. Pamiętają co stało się przed Inwazją. Pamiętają zarówno Astorię jak i Noctis. I orientują się, że inni tak samo będą wszystko pamiętali. Tu natychmiast przypomnieli sobie o Damianie - były brutalny morderca, skazaniec po Asymilacji stał się takim fajnym, słodkim misiem. Ogólnie dobry i lubiany sąsiad. A teraz jak się zachowa?

Jako, że sieć komunikacyjna przestała działać przez Elizę natychmiast odzyskali kontrolę i zaczęli monitorować kamerami sytuację. Damian jest w ratuszu i wziął zakładników, ale jeszcze nikt o tym nie wie. Pojechali tam z piskiem opon by zdążyć przed policją i Damianem; udało im się. Wpadli szybko do ratusza przed policją i wpadli na Damiana. Ten z radością się spytał, czy chcą zabić kogoś pierwsi - wyraźnie Damian nie jest zainteresowany zabijaniem.

Kombinacja pokazania siły i autorytetu ("my mamy wyższy stopień") i ciepła ("to życie jest lepsze, nie") pomogła. Damian się uspokoił. Ale uważa, że nie ma opcji powrotu, nie teraz, jak wziął tych zakładników i prawie ich wymordował. Nix jednak był w służbach bezpieczeństwa - wyszedł do policji i im wyjaśnił sprawę; że odzyskali pamięć ale nie chcą ognia w Trzecim Raju, są na tym samym wózku. Chcą naprawić sytuację. I tak trafili do Roberta Garwena.

Garwen zdecydowanie z nimi zaczął współpracować. Powiedział, że problemem jest Eliza Ira i że kontakt z nią jest możliwy tylko przez stację transmisyjną. Eliza coś zrobiła; najpierw doszło do uszkodzenia Ataienne a teraz Eliza zablokowała komunikację i doprowadziła do zamieszek. Coraz większy jest problem w okolicy a jego siły nie chcą strzelać do "swoich" - to Robert dawno temu podjął decyzję by dać szansę ludziom z Noctis a nie zabić wszystkich. Zespół stwierdził, że oni porozmawiają z Elizą. Poszli do wieży transmisyjnej i się z nią połączyli.

Eliza jest... nieco nie taka jak pamiętają. Tamta Eliza była bardziej ludzka, miała ludzkie odruchy. Ta była bardziej inkarnacją. Żałowała, że te lata temu nie dokonała eksterminacji Astorian. Powiedziała, że Saitaer jest w akcji i ona potrzebuje wsparcia by go zatrzymać. Wpierw oddział, potem plan. Wyczuli, że Eliza nie jest już do końca człowiekiem - kryształy zmieniły ją w arcymaginię. W inkarnację swej własnej woli.

Zespół zaczął Elizę przekonywać - uważają, że podjęła wtedy dobrą decyzję. Saitaera da się pokonać inaczej, Astoria nie musi płonąć. Niech ona da im rozwiązać ten problem, niech nie niszczy Astorian. Eliza się zgodziła (konflikt heroiczny). Królowa Kryształów zablokowała swoją aktualną operację. Wyłączyła atak na Trzeci Raj - ale oczekuje, że jednak Zespół uwolni magów Noctis.

Zespół szybko wrócił do Roberta i dowiedział się, że inny mag Inwazji Noctis też słyszał głos Elizy. Wojmił. Wojmił Trusz stracił rodzinę, bo zostali na Noctis. Posłuchał rozkazów Elizy i zdobył dla niej Tucznika, obezwładniając pilota. Zespół stwierdził, że jest większa szansa że Orbiter zbombarduje Tucznika niż pozwoli mu odlecieć; trzeba to jakoś sprytnie rozebrać.

Jak takie coś zrobić? Ano, podeszli do Tucznika i użyli magii - modulacja odpowiedniego sygnału przez głośniki Tucznika i nieszczęsny Wojmił padł nieprzytomny.

Potem już tylko pozostało wyjść z Grzegorzem i ogłosić, że rozwiązują sprawę. Polityk z Noctis oraz polityk z Astorii. W ten sposób zamieszki wygasły zanim doszło do apokalipsy i spłonięcia Trzeciego Raju - mimo mrocznego planu Elizy.

**Sprawdzenie Torów** ():

2 Wpływu -> 50% +1 do toru. Tory:

* nieistotne. Dark future niemożliwe.

**Epilog**:

* Eliza, Arcymagini Kryształów, będzie stopniowo odtajać. Nie ma krystalizacji nienawiści.
* Eliza staje się kluczem do adaptacji Saitaera
* Saitaer jest możliwy do adaptacji do Primusa; nie musi być tylko pasożytem.
* Alternatywnie, Saitaer może utracić zasady boskie i stać się potężnym viciniusem.
* Pojawiają się ruchy mówiące o tym, że niekoniecznie ludzi trzeba kontrolować w pełni - można dać im szansę
* Pojawia się sekta Saitaera, ta "dawna" - adaptacja typu perfekcja dla perfekcji, zgodnie z Ixionem
* Augustus zostanie reprezentantem Inwazji w Orbiterze
* Pojawia się w przyszłości możliwość pozytywnych kontaktów Astoria - świat Inwazji (Noctis)

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
|               |         |             |

Czyli:

* (K): .

## Streszczenie

Po 10 latach od Inwazji z Noctis Eliza wyczuła, że Saitaer się rusza. Uderzyła w Trzeci Raj by odzyskać oddział. Jednak zasymilowane z Astorią siły Noctis stanęły w większości po stronie Astorii i przeciw Elizie. Udało się powstrzymać zamieszki a Zespół przekonał Elizę, że niekoniecznie tędy droga; nie trzeba niszczyć Astorii by uratować świat przed Saitaerem. Eliza uznała, że da im szansę. Zespół pomógł Astorianom ustabilizować sytuację, po czym Ataienne odebrała wszystkim - poza Zespołem - pamięć ponownie. Eliza powoli może odtajać z nienawiści.

## Progresja

* Eliza Ira: arcymagini kryształów odtajała. Nie jest napędzana jedynie skrystalizowaną nienawiścią do Astorii i dawnej Elizy.

### Frakcji

* .

## Zasługi

* Eliza Ira: arcymagini kryształów, która odparła Saitaera i Ataienne krystalizując swą nienawiść. Po 10 latach uderzyła w Trzeci Raj by odzyskać swój oddział po uprzednim wyłączeniu z akcji Ataienne. Zespół ją odtajał i kupił szansę Trzeciemu Rajowi.
* Damian Drób: brutalny osiłek-skazaniec, który po Asymilacji został łagodnym gigantem. Przekonany przez Zespół, że nowe życie może być lepsze niż to stare.
* Robert Garwen: szeryf z Barbakanu Trzeciego Raju; to on obstawiał by nikogo nie zabijać a dać szansę, więc go zesłali do Trzeciego Raju
* Wojmił Trusz, oficer, rodzina została na Noctis. Chciał porwać Tucznika dla Elizy, ale Zespół go unieczynnił. Stracił pamięć - ponownie.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, NW
                    1. Ruiniec
                        1. Trzeci Raj
                            1. Ratusz: miejsce, gdzie Damian wziął zakładników i Zespół skutecznie przekonał go że nie tędy droga
                            1. Barbakan: miejsce centralnego dowodzenia Roberta Garwena; współpracował z Zespołem by uporządkować zamieszki
                            1. Stacja Nadawcza: miejsce komunikacji Trzeciego Raju z Elizą Irą. A dokładniej, Zespołu z Elizą.
                            1. Mały Kosmoport: tam Zespół odbił Tucznika z rąk Wojmiła bez krzywdzenia nikogo na pokładzie

## Czas

* Opóźnienie: 3
* Dni: 2
