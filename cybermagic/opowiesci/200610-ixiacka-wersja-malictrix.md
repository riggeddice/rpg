## Metadane

* title: "Ixiacka wersja Malictrix"
* threads: legenda-arianny
* gm: żółw
* players: kić, fox

## Kontynuacja
### Kampanijna

* * [201224 - Nieprawdopodobny zbieg okoliczności](201224-nieprawdopodobny-zbieg-okolicznosci)

### Chronologiczna

* * [201224 - Nieprawdopodobny zbieg okoliczności](201224-nieprawdopodobny-zbieg-okolicznosci)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* .

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Punkt zero

.

## Misja właściwa

Admirał Kramer poprosił do siebie Ariannę Verlen. Powiedział jej, że jest bardzo trudne i interesujące zadanie. Jedna ze stacji wydobywczych w pasue Telirańskim należąca do firmy/frakcji Melusit wysłała sygnał SOS. Telira zajmuje się pozyskiwaniem kryształów ixiackich; coś, co po czasie dowiedzieliśmy się, że ma powiązanie z Saitaerem i jego światem, Ixionem.

Admirał przekazał podstawowe informacje:

* Kryształy ixiackie redukują bariery. De facto pełnią podobną rolę jak Saitaer - fuzja rzeczy normalnie niemożliwych do fuzji.
* Stacja Telira-Melusit VII znajduje się w lodowym asteroidzie. Należy do Melusit, którzy ostro konkurują z frakcją Saranta.
* Sygnały mówią, że podobno roboty wydobywcze itp. uzyskały świadomość.
* Z uwagi na działanie dookoła kryształów ixiackich maszyny wydobywcze mają bardzo słabą psychotronikę, niemożliwą do utrzymania nawet przez Elainkę. By nie doszło do Transferu Ixiackiego (przeniesienie cech z człowieka na roboty wydobywcze).
* W sektorze Astoriańskim jedynie istoty pochodne ludziom mają prawa obywatelskie. TAI, viciniusy nie-ludziopochodne i byty w virt nie mają praw.

Arianna powiedziała, że nie może wykluczyć rozproszonej inteligencji. Mało prawdopodobne, ale teoretycznie możliwe. Poprosiła Kramera o specjalistyczne skanery psychotroniczne; on się zgodził, by zamontować takowe w Inferni. Po tym wszystkim - Arianna dała rozkaz. Lecą do pasa Teliriańskiego, a dokładniej: na Telira-Melusit VII.

Infernia dotarła, niezauważona, w pobliże stacji VII. Jest całkowicie niewidoczna. Klaudia siadła do sensorów i przeszukuje transmisje. Chce zrozumieć, co tu się dzieje. Czy to atak, czy sabotaż, czy transfer ixiacki...

* V: Klaudii udało się zobaczyć sporo nietypowych transmisji, m.in. dzięki skanerom psychotronicznym Kramera. Stacja jest oblężona, przez ich własne maszyny wydobywcze. Niezła taktyka.
* VX: Niestety, Infernia straciła niewidoczność - ale ma mapę ruchów wrogich maszyn. Widzi sporo bytów, są "rojowe". Czyli jest jakiś hivemind. Są tu też dwa typy robotów - te "rojowe" i te "normalne"; tak jakby infekcja następowała stosunkowo powoli.
* V: Klaudii wspomaganej przez Persefonę d'Infernia udało się przejąć kontrolę nad nieaktywnymi robotami. Skacząc po ich sensorach i detektorach wykryła znajdujący się w środku lodowego asteroidu ciężki servar klasy Fulmen. A to zła wiadomość; Fulmen ma bardzo dużą siłę ognia.

Ale tam jest coś jeszcze. Klaudia siadła do systemów i zaczęła porównywać specyfikę komunikacji, psychotroniki i danych z Kontrolera. I doszła do tego (Ex:V). Ma do czynienia z Malictrix! TAI klasy 2.5, zaprojektowane przez Minerwę Diakon, klasy 'TAI-Killer'. Sabotująca i mordercza TAI. Ta instancja Malictrix nie utrzyma się długo, nie ma źródeł energii; wystarczy jej energii i mocy na może 3 dni, potem wygaśnie. Oki, czyli jest deadline, ale czego ta Malictrix CHCE?

Infernia skontaktowała się ze stacją na asteroidzie. (VX): Malictrix przechwyciła sygnał, ale nie dała rady wejść na systemy Inferni. To jedynie udowodniło Klaudii, że Malictrix potrafi adaptować roboty na asteroidzie do swoich potrzeb.

Arianna porozmawiała z Melwinem Sito, płaczącym jej w słuchawkę. Podobno roboty na stacji uzyskały osobowość i ogłosiły niepodległość. Porwały dużą ilość ludzkich zakładników, m.in. żonę i dzieci Melwina. Żądają spełnienia ich warunków - ostatnie 24h miał na narysowanie FLAGI Z DELFINEM. Ale za każdym razem coś było nie tak. Jak nie zdąży, to jego rodzina zginie. Ale 15 minut przed zakończeniem terminu roboty zmieniły zdanie - teraz chcą flagę z ORKĄ. A on nie umie rysować, nie ma siły, ma dość...

Klaudia zamrugała zdziwiona. To nie jest typowe zachowanie Malictrix. To nie jest w ogóle zachowanie Malictrix. Transfer ixiacki?

Melwin jest święcie przekonany, że MUSZĄ się ewakuować. MUSZĄ! Nie ma wyboru. Muszą uciekać, zostawić to wszystko tu na śmierć - trzeba odbić zakładników i wiać. Arianna dała radę go uspokoić (V), zaznaczając, że tak długo nie spał że jego podejmowanie decyzji jest lekko uszkodzone. Melwin będzie współpracował z Orbiterem.

Najważniejsze, zdaniem Arianny, jest zapewnienie bezpieczeństwa tych osób które tu już są. Tak więc Eustachy używając Inferni zaczął metodycznie niszczyć wszystkie roboty wydobywcze a Leona poprowadziła oddział szturmowy mający ewakuować ludzką załogę stacji. Klaudii jednak coś nie pasowało; puściła na wszystkich pełne skanery psychotroniczne. Okazało się, że 8 kluczowych osób (m.in. rodzina Melwina) przez transfer ixiacki mają w swoim ciele zalążki Malictrix. Jakby weszli w kontakt z Persefoną, Mal by się miała gdzie schować...

Tak więc - kwarantanna. I powoli plan Malictrix zaczął się robić zrozumiały. Mal nie chce niczego ZNISZCZYĆ. Ona chce STĄD UCIEC. Czyli jej celem było zmuszenie Orbitera do ewakuacji Malictrix, dlatego Mal atakuje jakby nie mogła i udaje coś innego niż jest. Gdyby nie te sensory od Kramera, mogłoby to nawet się udać...

Dobrze, ale Malictrix ma jeszcze 7 ludzkich zakładników. Może i jest słaba, ma ograniczone życie, ale może zabić wszystkich. Arianna zdecydowała się na coś nietypowego - zaufa, że transfer ixiacki doprowadził do wykształcenia się odruchów, m.in. samozachowawczych u Malictrix - i spróbuje z nią negocjować. Wysłała małego robocika górniczego i zwróciła się do Malictrix jako to, czym ona jest: Mal. Nie udaje, że nie wie z czym rozmawia.

A na drugim kanale Klaudia rozpaczliwie próbuje zrozumieć co tu się stało z tą Malictrix.

Skan Klaudii (Ex):

* V: Ta Malictrix wiele przeszła. Doszło do transferu ixiackiego od ludzi. Ma szczątkową świadomość i jest sterowana przez nienawiść.
* VXX: Malictrix ma świadomość rojową. Musi mieć grupę ludzi w których się zagnieździ do stabilizacji. Cóż, jest to coś, co Arianna jest w stanie zrobić i zapewnić. Chwilowo użyje ludzi z Melusit; docelowo znajdzie się innych.

Negocjacje Arianny (Ex):

* V: Malictrix zgodzi się na relokację - opuści tą stację i dostanie inne miejsce.
* V: Malictrix odda zakładników i ludzi z Melusit.
* X: Malictrix żąda komputronium. Żąda większych systemów dla swojej mocy. Musi mieć większą moc, większą wydajność. Arianna niechętnie, ale się zgadza.
* V: Malictrix powstrzyma się od ludobójstwa; nawet jeśli ma na to ogromną ochotę, nie zrobi nikomu PERMANENTNEJ krzywdy.
* X: Malictrix zawsze będzie potrzebować biokońcówki przez transfer ixiacki.
* V: Malictrix nie zaatakuje Arianny. Będzie jej ostrożnie lojalna, póki dostaje to, czego pragnie.

Cóż, Arianna ma zaiste piękne, anomaliczne kuriozum. Teraz musiałaby jakoś ją przenieść i przechować. Bardzo ładnie poprosiła Kramera - żeby to się udało, potrzebny jej statek z zaawansowaną psychotroniką. Poprosiła ślicznie Kramera o coś takiego - ale za to przekaże w jego ręce stację Telira-Melusit VII. Kramer się zgodził dostarczyć Ariannie koloidową korwetę OO 'Pandora' (VV).

...A Eustachy uszkodził inteligentnie systemy Pandory. Niech nasza mała Malictrix nie będzie ZBYT groźna...

## Streszczenie

Istotna stacja frakcji Melusit - Telira-Melusit VII - została sabotowana przez frakcję Saranta. Zrzucono na tą stację rozproszoną Malictrix. Ale doszło do transferu ixiackiego i Malictrix uzyskała częściową świadomość, planując, jak stąd uciec. Infernia jednak nie tylko wykryła obecność Malictrix, ale też przekazała stację Orbiterowi, uratowała wszystkich ludzi Melusit i do tego pozyskała ową Malictrix jako sojusznika (o czym nikt nie wie). Wyjątkowo udana operacja.

## Progresja

* OO Infernia: otrzymuje specjalistyczne skanery psychotroniczne krótkiego zasięgu, pozwalające jednak Inferni na detekcję rozproszonych bytów psychotronicznych.
* OO Pandora: statek przekazany Ariannie Verlen przez siły Kramera; zaawansowana psychotronika i koloid. Delikatny jak cholera, zasięg eskortowca. Klasa: korweta. 16 osób.
* Arianna Verlen: otrzymuje niewielki okręt bezzałogowy OO Pandora, w którego wpakowuje Malictrix jako główne TAI.

### Frakcji

* .

## Zasługi

* Arianna Verlen: inteligentnie uzyskała od Kramera OO 'Pandora' i wzmocnienie detektorów Inferni; potem wynegocjowała, że Malictrix będzie współpracować. I przekazała stację Telira-Melusit VII w ręce Orbitera, dając Kramerowi niemały sukces zwłaszcza w linii kryształów ixiackich.
* Klaudia Stryk: znalazła rozproszoną Malictrix w robotach, skanowała sygnały i udowodniła transfer ixiacki między ludźmi a Malictrix. Doszła też do tego, że Mal chce uciec.
* Leona Astrienko: przeprowadziła akcję szturmu na stację Telira-Melusit VII, by uratować ludzi przed ixiacką Malictrix. Zero problemów, bo Mal nie stała jej na drodze XD.
* Malictrix d'Pandora: zrzucona przez Sarantę na stację Telira-Melusit VII. Doszło do transferu ixiackiego; nabrała sadystycznych cech i szczątków osobowości. 
* Melwin Sito: jeden z wysokich oficerów Melusit, dowodzący stacją Telira-Melusit VII; 

### Frakcji

* Wolni Astorianie - Melusit: subfrakcja Wolnych Astorian, specjalizujący się w wydobyciu asteroidów; wrodzy Sarancie. Siedziba kontaktowa w pasie Telirańskim. Mieli problem z Malictrix, którą rozwiązała Infernia. Stracili na stałe stację Telira-Melusit VII.
* Wolni Astorianie - Saranta: subfrakcja Wolnych Astorian, specjalizujący się w wydobyciu asteroidów; wrodzy Melusit. Siedziba kontaktowa na stacji Valentina. Spuścili na stację Telira-Melusit VII Malictrix, która chce się na nich zemścić.
* Orbiter - Kramer: przekazali Ariannie Verlen okręt o nazwie Pandora, 16-osobową korwetę koloidową krótkiego zasięgu. Dostali za to stację Telira-Melusit VII.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Pas Teliriański: daleko od gwiazdy Astorii; znajdują się tam liczne stacje wydobywcze.
                1. Stacja Telira-Melusit VII: lodowa asteroida ze stacją wydobywczą, gdzie zagnieździła się Malictrix Skażona Ixionem; ten problem rozwiązała Infernia. Stacja przeszła z rąk Melusit w ręce Orbitera w wyniku działań Arianny.

## Czas

* Opóźnienie: 15
* Dni: 4
