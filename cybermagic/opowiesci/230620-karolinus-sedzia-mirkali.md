## Metadane

* title: "Karolinus, sędzia Mirkali"
* threads: brak
* motives: tien-na-wlosciach, stare-kontra-nowe, starcia-kultur
* gm: żółw
* players: kamilinux, kić

## Kontynuacja
### Kampanijna

* [230620 - Karolinus, sędzia Mirkali](230620-karolinus-sedzia-mirkali)

### Chronologiczna

* [230425 - Kłótnie sąsiadów w Wańczarku](230425-klotnie-sasiadow-w-wanczarku)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ?
* CEL: 
    * Działania
        * .

### Co się stało i co wiemy

* Kontekst
    * .
* Wydarzenia
    * .

### Co się stanie (what will happen)

struktura sesji: "Moloch" (narzędzia-mg)

* Nazwa miasteczka: Mirkala
* frakcja: "Domy"
    * ekologiczne, wspomagane przez duchy, robione przy użyciu
    * potencjał na rozwój Mirkali i więcej pieniędzy w całym społeczeństwie.
    * sojusznicy: biznesmeni, pionierzy, ludzie 
    * victim: poprzedni tryb życia, ludzie którzy zaufali tracą wszystko
    * siły: kapitał, rozpoznawalność
* frakcja: "Zielarstwo"
    * ekologiczne, zawierają specjalne rośliny i komponenty
    * tak jak jest jest dobrze. Konserwatyści.
    * sojusznicy: cała stabilna lokalna społeczność co żyje jak pączki w maśle, część magów rodu
    * victim: młodzi którzy chcą czegoś innego w życiu, innowacje
    * siły: tradycja, lokalna społeczność, sfałszowane dokumenty
* KONFLIKT:
    * Domy wymagają innego typu szamańskich duchów, osłabiających niektóre Zioła
    * Zioła wymagają ciszy i pewnego typu aury w otoczeniu, którą niszczy chciwość i innowacyjność

Rozpiszmy na fazy:

* Faza 0: Krzywda
    * Na polach Zielarskich pojawiły się uciążliwe insekty niszczące cenne zioła
    * Okazuje się, że te insekty pochodzą od Zielarzy. Zielarze chcieli uszkodzić nimi Domy, ale ktoś z Domów obrócił je przeciw Zielarzom
    * Nie chcąc płacić za cudze winy, Juanita (z Domów) wezwała sędziego z rodu Samszar (postać gracza).
* Faza 1: Decyzja w niewielkiej sprawie (po to ich wezwali)
    * Sędziowie muszą zdecydować, kto jest odpowiedzialny za pojawienie się szkodników na polach Zielarzy.
        * Marcin z Domów, który obrócił szkodniki przeciw Zielarzom?
        * Adrian z Zielarzy, który ściągnął szkodniki jako "zapylacze wartościowych Ziół"?
    * EKSTRA: Młodzież Zielarzy protestuje. Młodzież Domów kontrprotestuje. "Ściągnięci" ludzie Domów wspierają. Burda?
* Faza 2: Tu się dzieje coś więcej - rośnie ilość konfliktów w sposób niekontrolowany
    * vs DOMY: 
        * dramatyczne podniesienie zużycia zasobów - wody, 
        * hałas i zakłócenia dobiegający z terenów Domów, który przeszkadza im w odpoczynku i pracy.
        * zanieczyszczenia, działania operacyjne
        * krajobraz, zniszczenie tego co było. Prawa wysokości płotów i zagospodarowania przestrzeni.
    * vs ZIELARZE:
        * zamordyzm - "albo po naszemu się zachowujecie albo spadajcie"
        * kontaminacja roślin + prawa do ziemi. Wywalenie z terenu tych, którzy mieli prawo tu być bo tylko TE rośliny tu dobrze żyją
        * nie ma możliwości rozwoju, racjonowanie zasobów. Młodzi nie mają jutra.
        * zapachy, alergie itp.
* Faza 3: Eskalacja konfliktu - dzieje się coś poważnego
* Faza 4: Kryzys
* Faza 5: Decyzja

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Juanita Derwisz: prowodyrka "Domów Mirkali" i bizneswoman. Prawniczka.
    * styl: WR. "Mirkala jest warta więcej niż tylko te zioła. Samszarowie nie są dyktatorami."
    * Pozyskała inwestora. Ma wsparcie Diakonów.
* .Marcin Ukraptin: specjalista od duchów i badacz. Frakcja "Domy Mirkali".
    * styl: BGR. "Wszystko zgnije i się rozpadnie, jeśli nie poświęcimy tej gnijącej dziury!"
    * Pracowity, obawia się jutra. Patriota Mirkalański.
* .Adrian Ukraptin: zasłużony "Zielarz", który w warunkach kryzysu ZAWSZE obronił Mirkalę
    * styl: GW. "Harmonia i znajomość swego miejsca jest ważna. My WIEMY jakie jest nasze miejsce."
    * Uratował kiedyś Mirkalę przed katastrofą. 

### Scena Zero - impl

.

### Sesja Właściwa - impl

Co może robić młody ambitny arystokrata rodu Samszar? Nie wiemy, ale został wezwany do Mirkali jako SĘDZIA. Bo coś się stało, niejaka Juanita została oskarżona o "sprowadzenie stonki niszczącej cenne zioła". Sama Juanita nie zgodziła z wyrokiem sądu i ONA zażądała maga rodu Samszar. Jako, że to "mało ważna" sprawa - wysłali Karolinusa.

Oczekiwania szefostwa "klepnij to, Juanita robi kłopoty"... Juanita musiała zapłacić - nawet niemało - by dostać prawo do Samszara. Więc... cóż.

Mirkala słynie "od zawsze" przede wszystkim z cennych upraw. Zioła, eliksiry... Mirkala jest takim... typowym miejscem. Mówi się nawet 'nalewka z Mirkali'. Juanita nie miała bardzo poważnych oskarżeń - miała zapłacić grzywnę (10000) ale wolała wydać (20000) by wezwać Karolinusa.

Władze Mirkali nie oponowały. Nie były przeciwne wezwaniu Samszara. Nawet się ucieszyły. Ale Aleksander Samszar był zajęty i trafiło na Karolinusa. Władze nawet zapłaciły 1k z tych 10k. Są pewne zwycięstwa.

Sama Juanita nazywa się Derwisz, co implikuje, że pochodzi z rodu Diakonów ale nie ma magii. Czyli nie jest stąd.

Mirkala. Na pierwszy rzut oka widać konflikt między niskozasobowym terenem nizinnym i wzgórzami z fabrykacją domów.

Powitano Karolinusa w Ratuszu. Sołtys FILIP Klirpin. Gorąco przywitał Karolinusa. Co myśli o Juanicie:

* Przybłęda z kasą, która chce zniszczyć Mirkalę.
* Filip AUTENTYCZNIE nie rozumie co Juanita chce uzyskać.
    * ona próbuje rozbudować "Domy Mirkali" używając jakichś Diakońskich technologii ze wsparciem duchów
    * ALE! potrzebne są zasoby. Prąd. Jest głośno. To negatywnie wpływa na duchy i delikatne zioła.
    * Mirkala OD ZAWSZE zajmowała się ziołami. TYMI ziołami.
    * "Tien Samszar, PROSZĘ ją zatrzymać... niech domy buduje gdzieś indziej."

Juanita mieszka na Wzgórzu. Sama. 40-50 lat. Piękna.

* Juanita: "Stonka, tien Samszar, to pretekst. Dlatego poprosiłam o wsparcie."
* Juanita: "Ja chcę zrewitalizować ten teren. Oni chcą zachować go w średniowieczu."
* Karolinus: "Co masz na myśli?"
* Juanita: "Wizja. Młodzi są nieszczęśliwi. Muszą się 'zachowywać', by nie stresować ziół."

Juanita wyraźnie wyjaśniła - tu jest lokalna klika, która trzyma władzę i trzyma wszystkich "w średniowieczu". Nie można mieć generatorów MIMO że to legalne, bo zioła. Mimo, że ona zrobiła wszystko poprawnie, to jest teraz prześladowana.

Juanita dodała, że młodzi cierpią najbardziej i ona ma największe poparcie wśród młodych.

Juanita zaznaczyła, że ona NIC nie ma wspólnego ze stonką.

Karolinus ma pomysł - NOWA wioska. Młodzi niech są tam i żyją tam i tylko niektórzy niech tu przyjeżdżają i uprawiają. A młodzi mogą tam się wyluzować. Juanita spojrzała na Karolinusa z uznaniem.

* Juanita: "Odwołuję moje wątpliwości odnośnie Twojego doświadczenia."
* Juanita: "Biznes, który chcę zrobić MUSI być na tych wzgórzach tak samo jak tamte zioła MUSZĄ być w tamtych dolinach. Mogę oczywiście zrobić inny biznes. A oni inne zioła. Tak naprawdę, pomysł pomoże młodym, ale kto za to zapłaci?"
* Karolinus: Ekrany domków między osiedlem a polami by się nie przebijało pole elmag, hałas...
* Juanita: To jest coś co mogę zrobić. Mogę potraktować to jako koszt prowadzenia biznesu. Zwłaszcza jak będę mogła odliczyć to od podatku.

Karolinus kombinuje, jak przywieźć emerytów i rencistów tu na turnusy rehabilitacyjne, coś co będzie działać "na terenie" oraz "duchosanatoria". Juanita zainwestuje w Mirkalę i wszyscy czerpią korzyści.

Karolinus -> Strzała: czy coś jest w tych wzgórzach co pokazuje CZEMU Juanita działa tutaj?

Tp Z (na tym terenie NIE MA TAI) +3:

* Vr: Strzała doszła do tego, że WIĘKSZOŚĆ dokumentów na tym terenie jest sfałszowana. Mirkala operuje własnym obiegiem dokumentów. To co jest w papierach a rzeczywistość to dwie różne rzeczy.
* Vz: Strzała strzeliła do dokumentów Rodu i skanów sentisieci - na terenie "Juanity" nie ma NIC wartościowego oprócz "specyficznej aury". Juanita pracowała z eko-Domami od dawna, od ponad 15 lat. I weszła we współpracę z lokalsami z Mirkali by wiedzę Diakonów przenieść tu.

Karolinus - czemu tu? Juanita skłamała wprost "okazja biznesowa - tu są młodzi, nie ma przemysłu i teren jest prawidłowy". TAK, to są predyktory, ale to nie jest powód.

* Karolinus: Chcę być neutralny ale jak będziesz kłamać się źle skończy. Żadne prawo Cię nie obroni.
* Juanita: W odróżnieniu od Lemurczaków, Samszarowie nie są dyktatorami. Nie masz prawa do mojego umysłu. Nie jestem zobowiązana powiedzieć Ci czemu to robię. Sprawa osobista.
* Karolinus: To Twój osobisty biznes a ta wioska jest w naszym osobistym zainteresowaniu.
* Juanita: Nie chcę zrobić krzywdy LUDZIOM w tym miejscu. Na tym staniemy.

Sołtys Filip.

* Filip: Tien Samszar, jaka to radość. Udało Ci się podjąć słuszną decyzję? /dla niego to oczywista sprawa
* Karolinus: Jakie macie dowody że to ona podrzuciła stonkę?
* Filip: (seria śladów wskazujących na ogromną konspirację)

Filip pokazał Karolinusowi, że W PAPIERACH winna jest Juanita bo 


Sekretarka Agnieszka z przyjemnością przekazała wszystkie dokumenty Karolinusowi (czy pomóc w dekodowaniu). Dała dokumenty drugiego obiegu. Agnieszka "tien Samszar, to Wasze dokumenty. Oczywiście, że macie do nich prawo." Wyjaśnia Karolinusowi (i Strzale o czym nie wie) jak są dokumenty kodowane i jak to ogólnie działa.

Dla Agnieszki Karolinus jest celebrytą.

Strzała analizuje dokumenty.

Tr Z (+dalej nie ma TAI) +3:

* X: (podbicie) wyraźnie dokumenty drugiego obiegu są zaprojektowane przy pomocy magów Samszarów.
* Vz: (podbicie) NIC, autentycznie, NIC nie ma na tych cholernych Wzgórzach. Tu nie chodzi o Wzgórza. SERIO problem jest w tym, że uruchomienie tu biznesu uszkodzi niektóre zioła i niektóre biznesy lokalne.
    * Filip NAPRAWDĘ chroni tego terenu.
* X: (manifest) Samszarowie chronią lokalny teren i zależy im na niezmienności Mirkali.
    * -> zioła, działania, hodowanie duchów itp.
* V: (manifest) Lokalna klika robiła ZŁE RZECZY ludziom, którzy chcieli zmieniać to wszystko.
    * wynajmowano zbirów
    * niszczono karierę i życie, plotki, Pakt...

Podsumowując:

* Młodzi autentycznie nie mają tu przyszłości. Są "utrzymywani siłą".
    * populacja jest szantażowana i jest zastraszona, większość akceptuje to życie.
* System Mirkali TAK JAK JEST nie nadaje się do działania długoterminowego
    * kiedyś było trochę inaczej. Był przepływ ludzi.
    * teraz - wszystko jest zoptymalizowane pod kątem najdelikatniejszych ziół i PRZEDE WSZYSTKIM tych delikatnych ziół. Kiedyś było inaczej, nie było tak ostro
        * ale dostawali pochwały, awanse, pieniądze -> optymalizowali pod tym kątem
    * Krótkoterminowo Mirkala jest opłacalna, ale długoterminowo wymrze. (20 lat)

Karolinus idzie do Filipa i pokazuje to wszystko, wyjaśnia, chce mu uświadomić że jak się nic tu nie zmieni to Mirkala upadnie. Podsuwa pomysł z ekranami - niech młodzi mają miejsce na wyszalenie i zabawę. Niech mają zaawansowaną szkołę, gdzie mogą szukać / uczyć się czegoś więcej. Domki u Juanity z disco i innymi rzeczami.

Filip jest nieszczęśliwy. Karolinus chce znać prawdę.

Tp +3:

* X: Na tym etapie większość "starszyzny" jest strasznie zantagonizowana z Juanitą. Strasznie. Oni jej nie przyjmą łatwo. To musi być TWARDY rozkaz.
* V: Juanita nikogo nie słucha. Nie ma możliwości jej kontrolować - ma pieniądze z zewnątrz, nie da się jej zaszantażować, nie ma niczego co można jej zabrać. Oni tu nie chcą z nią pracować, bo ona jest innym ośrodkiem siły.
    * Cała Mirkala była pod stałą kontrolą Starszyzny
    * Juanita jest całkowicie poza tą kontrolą. Jest JEDYNYM bytem całkowicie poza kontrolą.
    * Juanita sponsoruje młodych, by mogli bezpiecznie wyjechać, "strażnicy konwojów". Uciekinierzy jadą jej ciężarówkami, ładują je i nie wracają.
    * Juanita bardzo przyspiesza apokalipsę Mirkali. (do 10 lat)
        * wyzwala młodych z tego regionu, spoza Mirkalę
            * sam Filip jest oburzony, bo ona na tym traci kasę. CZEMU!

Karolinus naciska na Filipa - przymusowe trzymanie młodych nie ma sensu. Trzeba ich zachęcić. To co oni zrobili i do czego dążą to kołchoz. Plus Mirkala umrze.

Tr Z (bo Samszar) +4:

* V: (podbicie) Filip niechętnie ale stoi po stronie Karolinusa. 
* X: (podbicie) Juanita nie odpuści starszyźnie. Juanita się NIE dogada ze starszyzną.
* Vr: (manifest) Starszyzna i Filip pójdą za planem Karolinusa.
* V: (eskalacja) Starszyzna i Filip aktywnie będą rozwijać plan Karolinusa
    * 1. najdelikatniejsze zioła -> ekranowane itp
    * 2. dać ujście i wentyl dla młodzieży oraz ściągać emerytów i osoby chcące spokoju
    * 3. płacić więcej, by ludzie chcieli zajmować się tymi cholernymi ziołami
    * 4. niech Juanita traci pozwolenie na budowę u Samszarów jeśli się nie dogadają
    * co ważne, NIC z tego nie wymaga pomocy Juanity. Mirkala jest w stanie naprawić się sama
        * -> Samszarowie którzy "za tym" stoją mają niższe przychody ale Mirkala przetrwa. Znika zegar apokalipsy.

Karolinus wraca do Juanity. 

* Karolinus: Mirkala się będzie zmieniać a ona się ma dogadać. W innym wypadku wszelkie pozwolenia zostaną Ci cofnięte.
* Juanita: Tien Samszar... jestem prawniczką. Moja firma zna się na PR. Jeśli wycofacie LEGALNE pozwolenia po to, bym nie mogła kontynuować biznesu, zapłacicie za to więcej niż chcecie.
* Karolinus: Nie przedłużymy
* Juanita: Mam swoje sposoby. W tej chwili nie macie nic co możecie zrobi zanim minie 5 lat. A ja zdążę zbudować nawet głośne i niefortunne z perspektywy aury budynki. Chcecie współpracować.
* Karolinus: Czy to szantaż w stronę Samszarów?
* Juanita: Nie. Nie w stronę Samszarów. Do Was nic nie mam.
* Karolinus: To nasza wioska.
* Juanita: Wymieńcie władze.
* Karolinus: Jedna osoba dla własnego widzimisię wymieni władzę?
* Juanita: Tien Samszar, nie byłby to pierwszy ani ostatni raz. Oskarżyli mnie o stonkę której nie zrobiłam (skłamała że to jest powód). Pofałszowane księgi, pofałszowane śledztwo - naprawdę akceptujesz coś takiego?
* Juanita: Jak można żyć na takim terenie? Wiele zniszczonych żyć.
* Karolinus: Młodymi już się zająłem, nie martw się o ich przyszłość. Natomiast jeśli nadal chcesz tu żyć, naucz się współpracować z lokalnymi władzami i zwyczajami.
* Juanita: Chciałabym wierzyć, że dojdziemy do porozumienia. Ale nie, jeśli... (myśli)... tien Samszar...
* Karolinus: Jeżeli nadal będą problemy, odezwij się i przyjrzę się temu. Ale w zamian obiecaj, że spróbujesz.
* Juanita: Z innej beczki, hipotetycznie, co oni musieliby zrobić byś ich wymienił?
* Karolinus: Nie w mojej kompetencji, mogę to tylko zgłosić.
* Juanita: Czyli nie jesteś Samszarem na którego liczyłam.
* Karolinus: Nie jestem tym na którego liczyłaś.

Karolinus jest tienem Samszarów. Używa sentisieci by znaleźć jakieś brudy i argumenty na Juanitę.

Tr Z M +3Ob +2Vr:

* Vz: (podbicie) Masz przebłyski informacji o Juanicie. Jest jakaś korelacja między jakimiś ludźmi stąd i nią.
* X: Juanita jest wyraźnie "z zewnątrz". Mirkala vs Juanita. Po jej stronie z lokalnych są tylko młodzi. Ale ma inwestorów i silne obudowanie prawne. Jest ZA silne. Ona wiedziała, że idzie na wojnę. I w to weszła.
* V: (manifestacja): 
    * Juanita ma w pokoju dokumenty, poszyfrowane itp. Ale jak je czyta i odszyfrowuje to senti- to łapie.
        * Jest osoba. Dominik. Jej prawa ręka. Najbliższy sojusznik. Nie żyje. Był stąd. Uciekł. 
        * I Juanita szukała informacji na temat jego rodziny.
    * Łączymy z danymi Strzały
        * Dominik był z rodziny, która tu przybyła a potem nastał "nowy porządek", oni byli knąbrni.
            * Tylko Dominik przeżył. Starszyzna stoi za zabiciem jego rodziny.
            * To nie tak, że to były aniołki.
    * To JEST osobiste. Juanita chce ich pomścić.
        * Ona chce ZNISZCZYĆ władze. Ma na to pieniądze. I wszystko co robi ma:
            1. wydobyć młodych, uratować innych "Dominików"
            2. sprawić, by Starszyzna zapłaciła za to, co zrobili

.

* Karolinus: Wiem, co chcesz zrobić. Ale Ci w tym nie pomogę. (zastrasza)
* Juanita: Jeśli wiesz, co chcę zrobić, wiesz, że mnie nie masz jak zatrzymać.
* Karolinus: Wiem jak.
* Juanita: Słucham.
* Karolinus: Nie powiem Ci. 
* Juanita: Powodzenia, tien Samszar. Przyda Ci się. (zimny uśmiech). Jeśli WIESZ co chcę zrobić i czemu, jesteś nie lepszy niż oni.
* Karolinus: Ja przekazuję informacje dalej.
* Juanita: Doceń proszę, że zamiast użyć siły używam ekonomii.
* Karolinus: Używasz sił, ale innych.
* Juanita: Tien Samszar, co ja Twoim zdaniem próbuję zrobić? Dlaczego? Jestem ciekawa, do czego doszedłeś.
* Karolinus: Ta rozmowa jest ekscytująca, ale niestety muszę ją zakończyć.
* Juanita: Jeśli wiem, że Samszarowie mi nie pomogą, to chyba faktycznie nic nie mogę zrobić /sarkazm
* Karolinus: Jeśli wiesz, to nic tu po mnie.
* Juanita: Znajdź proszę kto podłożył stonkę. Zobaczysz, że nie ja... /rezygnacja
* Karolinus: Przyjechałem rozwiązać problem, ale rozwiązałem inny.
* Juanita: I ja za to zapłaciłam?
* Karolinus: Jeszcze nie. Ale jeśli będziesz nachalna, zapłacisz.
* Juanita: To zatrzymaj mój biznes. Ja - nie przestanę.
* Karolinus: Desperacko chcesz wiedzieć co zrobię, ale niestety.

.

* V: Karolinus używając sentisieci, wiedzy Strzały oraz dostępu do sztabu prawników ORAZ dokumentów Juanity znalazł słaby punkt w jej prawnym umocowaniu. Jednak Samszarowie są w stanie powołać się na pewne zasady które sprawią, że ona nie może zrobić tego biznesu na wzgórzu. Jest to... brudna sztuczka, będą pewne retorsje wobec Samszarów, będzie trzeba trochę kasy oddać, ale fundamentalnie to wytrąca Juanicie broń z ręki. (Samszarowie tracą 100, Juanita 1000).
    * "Ze względów bezpieczeństwa wzgórzu grozi lawina, info z sentisieci, konieczność zrobienia rezerwatu"

.

* Juanita: Zdajesz sobie sprawę, że to uniemożliwia mi działania pokojowe.
* Karolinus: Ale słuchaj, chodzi o bezpieczeństwo!
* Juanita: (patrzy na Karolinusa i myśli) Wiem, kiedy przegrałam. (cisza). Dobrze, wycofam swój biznes z tego terenu.
* Karolinus: Jeśli chcesz pomóc młodym, dogadaj się ze Starszyzną.
* Juanita: Nie rozmawiam z mordercami.

Strzała ma dostępy. Ma dokumenty drugiego obiegu. Dane od świadków, dowody - rodzina Dominika to byli podli ludzie. Sam Dominik był z tych "dobrych". Nie znaczy to, że jego rodzina zasłużyła na śmierć, ale nie był to lincz bez sensu. Nie jest to czarno-białe.

Tr Z (+fakty i prawda +info o Juanicie i jej historii +jak akcje wpływają na ten teren i jak ruchy Karolinusa naprawiają dla PRZYSZŁYCH a ona niszczy dla wszystkich i przyszłość) +4:

* V: (podbicie) Juanita widzi, że jej działania nic nie zmienią. Nie naprawią. "Broken".
* Vr: (manifestacja) Juanita widzi, że ta sprawa nie jest oczywista. Nie będzie atakować tego terenu. "Completely broken". Całkowicie się wycofa.

## Streszczenie

Mirkala - miasteczko, gdzie stare (zioła) zwalcza się z nowym (Juanita pragnie budować ekologiczne prefab-domy na wzgórzach). Karolinus przekonał Starszyznę, że muszą dać młodym odskocznię (i znaleźć pieniądze) by Mirkala nie była całkowitym kołchozem, a potem z pomocą Strzały odepchnął Juanitę z tego terenu; niech nie mści się na Starszyźnie za śmierć rodziny jej przyjaciela.

## Progresja

* Karolinus Samszar: bardzo mile widziany w Mirkali, uważany za bohatera - pozbył się Juanity i pomógł młodym. Duży bonus zarówno w subfrakcji Samszarów jak i w samym miasteczku.
* Juanita Derwisz: broken; opuszcza teren Samszarów na stałe

## Zasługi

* Karolinus Samszar: ŚWIETNIE zapowiadający się 'rozsądca Samszarów'. W Mirkali znalazł świetne sposoby na to jak odwrócić pewną śmierć miasta i przyciągać młodych i emerytów oraz poszerzyć wartość Mirkali o coś więcj niż tylko zioła. Z pomocą Strzały odepchnął Juanitę, która chciała pomścić przyjaciela którego rodzinę zniszczyła Starszyzna Mirkali.
* AJA Szybka Strzała: odkryła, że wszystkie dokumenty Mirkali są całkowicie fikcyjne. Potem przygotowała hiper-paczkę wiadomości odnośnie rodziny przyjaciela Juanity i złamała morale Juanity. Juanita przestała walczyć.
* Juanita Derwisz: 40-50 lat, bizneswoman i prawniczka. Jej przyjaciel pochodzi z Mirkali i przyszła pomóc jego (kiepskiej) rodzinie, ale Starszyzna Mirkali ich zabiła. Juanita zdecydowała się uwolnić młodych z Mirkali (która jest trochę kołchozem zielarstwa) i zniszczyć Starszyznę ekonomicznie. Karolinus i Strzała ją Złamali i pokazali jej że to jest trudniejsza sprawa. Juanita opuszcza teren Samszarów z inwestycjami i w ogóle.
* Filip Klirpin: burmistrz Mirkali; należy do Starszyzny rządzącej Mirkalą i robiącą drugi, nielegalny obieg. Współpracuje z Karolinusem. Chce się pozbyć Juanity Derwisz, bo nie da się jej kontrolować. Słucha głosu rozsądku Karolinusa i pracuje nad naprawą miasta.
* Agnieszka Klirpin: sekretarka burmistrza Mirkali; wielka fanka Samszarów. Z przyjemnością przekazała Karolinusowi wszystkie dokumenty drugiego obiegu i pokazała jak je dekodować.

## Frakcje

* Tien Samszar: często na swoim terenie dedykują konkretne miejsca nawet wbrew ludziom - Mirkala jest dedykowana do rzadkich i istotnych ziół, nawet kosztem morale i lokalnych ludzi. Niestety, nie zadbali o Mirkalę i Karolinus jako arbiter musiał przybyć naprawić sprawę.
* Mirkalanie Zielarze: Obywatele Mirkali. 'tak jak jest jest dobrze.' Konserwatyści. Zielarstwo, pro-ekologiczni. Prowadzą Mirkalę by żyła we własnym świecie, dusząc miasteczko.
* Mirkalanie Zielarze: Sojusznicy: cała stabilna lokalna społeczność co żyje jak pączki w maśle, część magów rodu. Victim: młodzi którzy chcą czegoś innego w życiu, innowacje.
* Mirkalanie Budowniczy: Obywatele Mirkali. 'potencjał na rozwój Mirkali i więcej pieniędzy dla wszystkich.' Chcą zmodernizować Mirkalę, nawet wbrew Samszarom.
* Mirkalanie Budowniczy: Sojusznicy: biznesmeni, pionierzy, młodzi, Victim: poprzedni tryb życia.

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Mirkala: małe miasto z własnymi dziwnymi zasadami i kliką; słynie z bardzo rzadkich ziół. Umiera, ale Karolinus swoimi dekretami odwrócił śmierć miasta.
                                1. Wielka Zielarnia (E)
                                1. Wzgórza Bezduszne (W)
                                    1. Technopark: bardzo dumna nazwa na wielką chatę zbudowaną przy użyciu Fabryki Domów
                                    1. Fabryka Domów: infusowane duchami ekologiczne domy chroniące mieszkańców, ekologiczne

## Czas

* Opóźnienie: -34
* Dni: 3

## OTHER
### Fakt Lokalizacji
#### Miasto Mirkala

Dane:

* Nazwa: Mirkala

Fakt:

Mirkala jest małym miastem, z populacją około 30k mieszkańców, ale nie brakuje tu dynamizmu. Mieszkańcy przemieszczają się pieszo, na rowerach lub korzystając z publicznego transportu - niewielkich, elektrycznych pojazdów, które kursują regularnie pomiędzy różnymi dzielnicami miasta. Między starożytnymi lasami a nowoczesnymi technologiami, Mirkala jest miejscem, gdzie tradycja spotyka się z innowacją, tworząc niepowtarzalną atmosferę pełną możliwości i konfliktów.

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Mirkala
                                1. Centrum (Centrum)
                                    1. Plac Handlowy: miejsce, gdzie kupisz wszystko, od najprostszych produktów spożywczych po skomplikowane artefakty magiczne.
                                    1. Karczma Pod Zielonym Liściem
                                1. Dzielnica Zielarzy (E)
                                    1. Wielka Zielarnia
                                    1. Ogrody zmysłów
                                1. Wzgórza Bezduszne (W)
                                    1. Technopark: bardzo dumna nazwa na wielką chatę zbudowaną przy użyciu Fabryki Domów
                                    1. Fabryka Domów


