## Metadane

* title: "Odklątwianie Ateny"
* threads: pieknotka-kaplanka-saitaera, do-kogo-nalezy-epirjon, pieknotka-w-cieniaszczycie, saitaer-bog-primusa
* motives: dotyk-saitaera, klatwozyt, opetanie-przez-istote, trucizna-opozniona, dark-temptation, bardzo-irytujacy-reporter, strach-uzasadniony
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [181114 - Neutralizacja artylerii koszmarów](181114-neutralizacja-artylerii-koszmarow)

### Chronologiczna

* [181114 - Neutralizacja artylerii koszmarów](181114-neutralizacja-artylerii-koszmarow)

## Projektowanie sesji

### Struktura sesji: Wyścig

* **Strona, potrzeba, sukces, porażka, sposób, silnik, breakpointy**
    * Druga Strona, usunięcie Ateny
    * zapewnienie Stacji Epirjon we właściwych rękach
    * Atena nie może wrócić na stację Epirjon
    * Nic już nie zatrzyma Ateny przed powrotem na stację niedługo
    * Upiór Krwi
    * 1k3 / 4, skok na 5
    * Tor:
        * 5: przeciwnik się ukrył
        * 10: Atena nie może wrócić na Epirjon przez dwa tygodnie; przeciwnik ma full deniability
        * 15: Atena nie może wrócić na Epirjon przez miesiąc; przeciwnik jest niewidzialny
        * 20: Atena nie będzie w stanie wrócić na Epirjon; klątwa się sprzęgła ze wzorem katalistki

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) Upiór Ateny atakujący... Pięknotkę?
* Ż: (przeszłość, kontekst) Ataki na Atenę nie zostały zatrzymane

### Dark Future

1. Atena nie będzie w stanie wrócić na Epirjon; klątwa się sprzęgła ze wzorem katalistki

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: Upiór Ateny w szpitalu (20:45)

Pięknotka w szpitalu. Może niedługo wyjść. Przysypia i kątem oka zobaczyła... upiora Ateny. Ale jego nie powinno tu być. Upiór wycelował w Pięknotkę z pistoletu. Ona stoczyła się z łóżka i upiór strzelił. Pięknotka otworzyła oczy - poduszka jest uszkodzona, ale ona jest na ziemi. Wpadł lekarz, Pięknotka ukryła swoim ciałem przypaloną poduszkę i się promiennie uśmiechnęła że nic takiego. Lekarz uspokojony wyszedł.

Pięknotka jest w dobrym stanie; nie jest przypięta do żadnych czujników. Przysłoniła poduszkę i poszła do Ateny. Atena spytała Pięknotkę widząc jej aurę czy ona coś do niej czuje. Widząc minę Pięknotki, zauważyła - wpierw Pięknotka robi z niej modelkę, potem przyzywa jej efemerydę na poziomie Krwi, a teraz... jej efemeryda próbuje Pięknotkę zabić? Gdy Pięknotka powiedziała, że tak nie jest, Atena stwierdziła że ma to gdzieś, rzuca czar.

Zaklęcie jest Skonfliktowane Magicznie. Atena zrobiła sobie dużą krzywdę, ale Pięknotka zobaczyła coś ciekawego:

* Atena i Erwin oraz Terrorform stali się sprzężeni lekkim echem krwi z Pięknotką; dlatego ich wezwała.
* Atena jest pod wpływem Krwawego Klątwożyta i cholera wie skąd się wziął. Erwin najpewniej go nie ma.
* Klątwożyt połączy się z jej defensywami po pewnym czasie; trudno to znaleźć.

Wpadł Lucjusz Blakenbauer i wywalił Pięknotkę i opierniczył Atenę od góry na dół. Pięknotka wie jedno - tego Klątwożyta bardzo trudno będzie znaleźć. Ale ma echo Ateny gdzieś w pobliżu... Pięknotka zażądała wypisania na żądanie i je dostała. To wymaga działań wolnych. Jednym z najciekawszych źródeł wiedzy dla Pięknotki będzie najpewniej terrorform, ale... to straszne źródło jest.

**Scena**: Terrorform imieniem Saitaer (21:08)

Pięknotka posła porozmawiać z terrorformem do chaty Erwina. Tam Erwin i Minerwa kłócą się o przyszłość - Erwin twierdzi, że Minerwy nigdy nie zostawi, ona twierdzi, że Erwin nie musi jej zostawiać ale ma żyć z dziewczynami jak facet. Ona jest power suitem. Pięknotka zrobiła trochę hałasu by ją usłyszeli i weszła. Tam powiedziała jaki problem ma Atena - muszą się dowiedzieć co się dzieje. Pięknotka poprosiła Erwina i Minerwę by doszli do tego CZYM jest terrorform i dlaczego Pięknotka uzyskała takie umiejętności.

Pięknotka przekonuje Minerwę, by ta przyznała że terrorform tam jest i by pomogła - Atena nie może być powiązana z terrorformem będąc na CHOLERNEJ STACJI EPIRJON. Minerwa nie chce przyznać, że terrorform tam jest, bo boi się stracić kontrolę i iść na żyletki. Ona chce umrzeć, ale "not like this". Skonfliktowany Sukces - terrorform awaken.

Minerwa powiedziała, że w poprzedniej iteracji pracy nad rytuałem krwi pod wpływem energii Nojrepów doszło do przyzwania, przyciągnięcia czegoś co stało się terrorformem. Terrorform - jak mówi Minerwa - nie pragnie zniszczenia. On podróżuje by wskrzesić swój świat i do tego potrzebuje energii. Gdy to powiedziała, Pięknotka zauważyła, że rozmawia z terrorformem.

Pięknotka dowiedziała się od terrorforma, że on zawsze znajdzie Pięknotkę, Atenę i Erwina. On chce wskrzesić swój świat a do tego potrzebuje energii. Niestety, jest to energia Krwi. Sama Astoria to za mało by wskrzesić świat istoty takiej jak ten terroform. Terroform żąda krwi i cierpienia - jest w stanie pomóc Atenie. Pięknotka nie chce zgodzić się na to, żeby dać mu Krew; próbuje wyciągnąć z niego coś za darmo. Niech pomoże Atenie. (Tp:7,3,4=P). Terrorform pomoże, ale Skazi krwią Atenę, sam przeprowadzi rytuał.

Terrorform powiedział Pięknotce, że coś zrobione z krwi Ateny znajduje się w okolicach Wielkiej Szczeliny. Minerwie zostawił koordynaty. Znalazł to, łącząc się z Pięknotką i wysysając z niej trochę krwi by znaleźć Wzór Ateny. Powiedział, że Pięknotka musi odprawić rytuał krwi nad tym czymś w okolicach Szczeliny - bez tego nie uda się pomóc Atenie. Powiedział też, że Pięknotka ma rację - on nie pozwoli nikomu obcemu zniszczyć ani JEGO Ateny, ani JEGO Pięknotki ani JEGO Erwina.

Saitaer (terrorform) powiedział, że komuś zależy na odcięciu Ateny od Epirjona. Niech lepiej Pięknotka się pospieszy. I niech odda sprawcę jemu - on go dobrze wykorzysta.

Saitaer zniknął i wróciła Minerwa - w panice. Odpaliła działo plazmowe, ale zanim zdążyło wystrzelić, Pięknotka ją uspokoiła (Typowy: S). Minerwa ukryła wszystko co się stało i powiedziała, że wróciła. Saitaer jest potężny, nie doceniała jak potężny. Pięknotka potwierdziła ze smutkiem.

Lokalizacja "tego czegoś" jest w Szczelinie. Minerwa może tam wejść i tam się dostać, ale nie może wziąć Erwina ze sobą. Zbyt Skażony. Pięknotka..?

Cóż, Pięknotka zauważyła, że Saitaer grał na to, by Minerwa weszła w Szczelinę. Więc Pięknotka zdecydowała się na inny ruch. Całkowicie za plecami Minerwy (by Saitaer nie wiedział) porozmawiać z Ateną - niech zamiast tego wykorzystane będą kralothy.

**Scena**: Plan K. K jak kraloth. (22:22)

Pięknotka zdecydowała się skomunikować z Ateną przez hipernet. Pięknotka powiedziała Atenie, że ma klątwożyta krwi, którego nie da się łatwo wykryć i który jest powiązany z bytem w Szczelinie. Zagrała na paranoi Ateny - może nie wykrycie nie jest przypadkiem. Atena jest zaskoczona - skąd Pięknotka to wie? Pięknotka powiedziała, że zaklęcie Ateny plus ekspert. Atena mylnie zrozumiała, że chodzi o kralothy.

* "Odkąd zaczęłam z Tobą współpracować, moje życie idzie w coraz gorszą stronę" - bardzo załamana Atena o pomyśle Pięknotki z kralothami mającymi ją wyleczyć

Pięknotka użyła najcięższej artylerii - ktoś próbuje to ukryć. Ktoś próbuje zrobić z Ateny niekompetentną idiotkę i zabrać jej dostęp do stacji Epirjon. Albo Atena podejmie kroki drastyczne i nietypowe albo przeciwnik wygra. (Trudny+3:10-3-5=S). Atena, załamana, zgodziła się. Muszą znaleźć tego przeciwnika i jej pomóc. Atena się wypisze na żądanie i wraz z Pięknotką spróbują "zniknąć".

(22:37)
(16:24)

Atena spróbowała się wypisać ze szpitala. Lucjusz Blakenbauer powiedział jej, że nie ma takiej opcji - jeśli ona spróbuje się wypisać ze szpitala, on ma zamiar zrobić wszystkie obstrukcje i utrudnienia by ona tego nie mogła zrobić. Na pewno nie da jej tego zrobić dyskretnie - ostatnie działania Ateny nie świadczą o jej zdrowiu psychicznym. A Atena nie chce mu powiedzieć, gdzie idzie i co będzie robić...

Atena poszła więc do Lucjusza Blakenbauera. Musi z nim porozmawiać na te tematy. Lucjusz przyjął ją chłodno a jak dowiedział się w jakiej sprawie przyszła, lodowato zimno. Jest lekarzem i chce dobrze dla swojej pacjentki - w klinice jest bezpieczna. Pięknotka nie umie go przekonać - albo musi przedstawić mu dowody (które Blakenbauer musi potwierdzić i będzie grzebał w Atenie by je znaleźć, szkodząc jej) albo musi pokonać go formalnie - sprowadzić innego medyka lub użyć Adama Szarjana jako naciski.

Cóż, Pięknotka nie ma wyjścia. Komunikuje się z Adamem Szarjanem i prosi o wsparcie - potrzebna jej opinia medyka by wydobyć Atenę ze szpitala nie mówiąc o terrorformie. Adam powiedział, że będzie załatwione - niech Pięknotka spokojnie działa. No i Pięknotka wróciła do lekarza wiedząc, że Atena wyjdzie. Lucjusz przyjął Pięknotkę bardzo zimno. Przekazał jej leki które Atena ma brać. Powiedział, że jeśli coś jej się stanie - Pięknotka jest w jego oczach osobiście odpowiedzialna.

Pięknotka próbuje ratować relację. Mówi o klątwożycie i że potrzebna jest kuracja magią krwi: (Tr:9-3-5=R,R,S). Blakenbauer powiedział, że sprawdzi. Ale ostrożnie jest skłonny dać wiarę Pięknotce.

Atena i Pięknotka opuściły Pustogor, w drodze na Cieniaszczyt. Lucjusz powiedział, że Atena jest silnie Skażona (Ż: -5 Wpływ), zdewastowała się w szpitalu. Niech unika energii magicznych i czarowania, najlepiej za wszelką cenę.

Wynik:

* Tor Porażki: 5

Wpływ:

* Żółw: 6 (11)
* Kić: 4

**Scena**: Wejście do Cieniaszczytu (17:10)

Bramy Cieniaszczytu są wielkie. I ponure. Wymagają rejestracji. Zanim jednak się tam zbliżyli, Pięknotka zdecydowała się zamaskować Atenę Sowińską przy użyciu gabinetu kosmetycznego jako ktokolwiek inny niż Atena Sowińska. Tak, by nie poznał jej nawet Cezary Zwierz. (Tr:10-3-5=R,SS). Skonfliktowanie: i tak wiadomo, że tam była, acz jej nie rozpoznają.

(-5 Wpływ: paranoja Ateny odpaliła). Podszedł do czarodziejek Cezary Zwierz by zrobić z nimi wywiad. Atena dyskretnie wycofała się do sklepu a Pięknotka ściągnęła na siebie uwagę Zwierza - powiedziała mu, że to kwestia zakładu - przyszła przelecieć kralotha i zrobić z niego wiernego peta mocą miłości. Cezary ma gdzieś w okolicy kolan szczękę, tego by się nie spodziewał. (Tp:S). Cezary zapomniał o Atenie, zebrał soczysty scoop i poleciał dalej. Pięknotka odetchnęła z ulgą i poszła za Ateną...

Atena ostrożnie zbliża się do wyjścia z miasta. Bez kłopotu Pięknotka ją dogoniła. WTF. Dotarło do Ateny - gdzie produkuje się najwyższej klasy klątwożyty tego typu? No właśnie w miejscu takim jak to. Najpewniej jej klątwożyt powstał w Cieniaszczycie i ona będąc tutaj stanie się łatwą ofiarą napastnika. Atena wyraźnie jest rozpalona - jak powiedział Lucjusz, nie powinna działać w obszarze silnie magicznym. A to jest obszar silnie magiczny...

Nie ma czasu, ale trudno. Pięknotka próbuje przekonać Atenę że trzeba. (Tr+2:SS). (Atena się zgodziła pójść z Pięknotką, ale spotka Amadeusza.) Pięknotka nie pieprzyła się - bierze taksówkę. Wszystko co może iść źle idzie źle i lepiej nie marnować czasu na spacery z Ateną w tym stanie.

Dotarły do Nukleonu. Atena idzie zbita jak pies koło Pięknotki. Rejestracja. Kwalifikuje Atenę Amadeusz - Atena prawie zaatakowała magią, więc Amadeusz się wycofał. Wprowadził na to miejsce inną Diakonkę; ona zbadała Atenę i powiedziała Amadeuszowi, że da się zlokalizować źródło tego wszystkiego co jest złe. Twórcę klątwożyta, zwłaszcza z materiałami jakie oni mają w Cieniaszczycie.

Amadeuszowi nie podobają się konsekwencje dla Ateny, ale zobaczą.

Pięknotka poszła porozmawiać z Ateną - ta udaje spokojną, ale jest przerażona. Amadeusz jest w jej oczach "zdradzieckim wujkiem" który odrzucił Astorię i służy kralothom. Atena poprosiła Pięknotkę o pusty kryształ Mausów. Ta jej nie może dostarczyć kryształu... bo to Skazi Atenę jeszcze bardziej. Atena jest przygnębiona ale rozumie to o co Pięknotce chodzi. Nie chce iść się odprężyć. Poczeka tutaj i poczyta o stacjach kosmicznych.

Amadeusz i Pięknotka. Amadeusz powiedział, że nie jest już magiem; jest viciniusem i odrzucił człowieczeństwo. Ona się boi tego czym on się stał. Dlatego między innymi Atena nie chce być na Astorii. Ona pragnie dążyć do gwiazd. Chciała być medykiem na statku kosmicznym.

Pięknotka opowiada Amadeuszowi o terrorformie Saitaerze i o tym, że tego krwawego brandingu nie można ruszać; to niebezpieczne - i dla Astorii i dla Ateny. Cóż... na razie Amadeusz nic z tym nie zrobi. Za to Amadeusz powiedział Pięknotce, że jest możliwość by Atena znalazła tego kto stoi za klątwożytem - ale to wymaga rekonstrukcji kralotycznej w Pałacu Szkarłatnego Światła. Jest szansa że to ją zmieni, acz niewielka. W odróżnieniu od usuwania krwawego klątwożyta - tam nie ma ryzyk.

Pięknotka poszła do Ateny, ponownie. Ta nie ma gorączki ani omamów. Pięknotka powiedziała Atenie, że ona nie jest pierwszą która tak ucierpiała i że da się znaleźć autora tego klątwożyta - ale to ma ryzyko że Atenie stanie się coś strasznego, że ona się zrekonstruowała. Pięknotka przedstawia fakty a Kić silnie ciśnie ;-). (Tp+1:9-3-3) Wspierana wspólną historią i poczuciem obowiązku Ateny, kapitan stacji Epirjon, Pięknotka zdecydowała się na działanie.

Skonfliktowany Sukces: Atena będzie miała wątpliwości czy się zmieniła czy nie (jeśli nie).

**Scena**: Rekonstrukcja Ateny (18:25)

Następny dzień.

Atena jest bledziutka. Gdy wyszły z Nukleonu jeszcze nadrabiała miną. Gdy poszły w kierunku na Pałac Szkarłatnego Światła straciła panowanie i zaczęła krzyczeć i płakać. Rozdzierająco prosiła Pięknotkę, by ta ją uratowała. Diakonka współpracująca z Amadeuszem zażądała od Pięknotki, by ta sobie poszła i Pięknotka odeszła - w oczach Ateny zostawiła ją samą.

Czas sprawdzić skalę transformacji Ateny: 5k20 ilość jedynek: ZERO. Atena nie uległa żadnej zmianie (ale w to nie wierzy).

Wynik:

* Tor Porażki: 11

Wpływ:

* Żółw: 8 (18)
* Kić: 5

**Epilog**: (18:33)

* Atena nie wierzy w to, że się nie zmieniła.
* Atena będzie mogła wrócić na Epirjon, po 2 tygodniach najwcześniej. Przeciwnik ma _plausible deniability_.
* Sygnał Klątwożyta jest dalej badany, ale znaleziono mniej więcej źródło (wymaga czasu) - Orbiter Pierwszy. Ktoś stamtąd.
* Przez te dwa tygodnie Atena i Pięknotka tu zostaną. By zabezpieczyć Atenę przed dalszymi atakami. A potem - Epirjon.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |   8     |    18       |
| Kić           |   9     |     9       |

Czyli:

* (K): 
* (K): -

## Streszczenie

Atenie się nie poprawia a jej echo atakuje Pięknotkę. By zrozumieć co się dzieje, Pięknotka rozmawia z "terrorformem" (Saitaerem) i dowiaduje się o krwawym klątwożycie. Następnie używając wpływów Adama Szarjana wyrywa Atenę spod opieki lekarza i transportuje ją do Cieniaszczytu, by tam można było jej pomóc. By jej pomóc i odkryć napastnika potrzeba jest moc kralotyczna - Atena się nie zgadza, lecz Pięknotka ją przekonała. Atena ulega rekonstrukcji kralotycznej a Amadeusz ma mniej więcej namiar na twórcę klątwożyta.

## Progresja

* Atena Sowińska: ujawniło się, że jest przerażona kralothami i rzeczami kralotycznymi; najbliższe fobii co ma
* Atena Sowińska: nie dogaduje się z wujem Amadeuszem Sowińskim i mu zdecydowanie nie ufa
* Atena Sowińska: została Zrekonstruowana kralotycznie - wyleczona i wyczyszczona, oraz znaleziono sygnał osób chcących ją skrzywdzić
* Atena Sowińska: jeszcze 2 tygodnie nie może dostać się na stację Epirjon i musi działać w okolicach Cieniaszczytu
* Atena Sowińska: jest święcie przekonana, że została zmieniona i nie wie jak (nie została zmieniona)

### Frakcji

* Ostrze Szkarłatu Cieniaszczytu: frakcja Amadeusza Sowińskiego; mają namiar na maga który stoi za klątwożytem Ateny

## Zasługi

* Pięknotka Diakon: skłonna uratować Atenę za wszelką cenę - podpadając lekarzom, współpracując z kralothami, nawet z terrorformem. Przechytrzyła wszystkich, wygrała zdrowie Ateny.
* Atena Sowińska: coraz bardziej chora i Skażona klątwożytem krwi. Zaczynała wpadać w paranoję. Okazuje się, że zawsze kochała kosmos. Zrekonstruowana przez kralothy by ją wyleczyć, co ciężko przeżyła.
* Lucjusz Blakenbauer: lekarz dbający twardo o swoich pacjentów. Starł się z Pięknotką; dopiero wpływy i złoto Adama Szarjana zmusiły go do oddania Ateny jako pacjentki.
* Minerwa Diakon: przerażona, zorientowała się, że Saitaer ukryty w jej power suicie jest dużo groźniejszy niż się komukolwiek wydawało - i że ona nie w pełni kontroluje sytuację.
* Saitaer: zamieszkujący ciało terrorforma corruptor oraz władca magii krwi. Okrutny. Chce wskrzesić swój świat i Naznaczył Atenę, Pięknotkę i Erwina. Klasyfikacja: demon? Bóstwo?
* Amadeusz Sowiński: lord terminus Cieniaszczytu silnie współpracujący z kralothami. Chce znaleźć i usunąć tych co atakują Atenę. Zrekonstruował Atenę mocą kralothów Cieniaszczytu
* Cezary Zwierz: zrobił wywiad z interesującą Diakonką, która chciała przelecieć kralotha na śmierć. To była Pięknotka w przebraniu.

## Plany

* Amadeusz Sowiński: znaleźć i usunąć maga, który skrzywdził Atenę i który chce zatrzymać jej działania powiązane ze stacją Epirjon

### Frakcji

* 

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Przelotyk Wschodni
                            1. Cieniaszczyt
                                1. Kompleks Nukleon: miejsce, w którym Atena została kwalifikowana i gdzie zaprowadziła ją Pięknotka. Niestety, Atena potrzebowała kralotycznej pomocy
                                1. Pałac Szkarłatnego Światła: doszło tam do pełnej rekonstrukcji Ateny Sowińskiej by ją naprawić i uratować, przez kralothy i Amadeusza
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Rdzeń
                                    1. Szpital Terminuski: miejsce pełnej opieki nad Ateną gdzie grasuje Lucjusz Blakenbauer - mag który będzie dbał o swoich.
                                1. Eksterior
                                    1. Miasteczko: w domu Erwina Galiliena Pięknotka i Saitaer odbyli bardzo interesującą rozmowę o bogach i umierających światach.

## Czas

* Opóźnienie: 2
* Dni: 3

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* Start z Grubej Rury 4
* Sesja detektywistyczna zamaskowana jako wyścig z 1 torem

## Wykorzystana mechanika

1810
