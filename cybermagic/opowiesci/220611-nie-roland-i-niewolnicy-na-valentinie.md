## Metadane

* title: "Nie-Roland i niewolnicy na Valentinie"
* threads: historia-rolanda
* gm: kić
* players: żółw

## Kontynuacja
### Kampanijna

* [211229 - Szpieg szpiegowi szpiegiem](211229-szpieg-szpiegowi-szpiegiem)

### Chronologiczna

* [211229 - Szpieg szpiegowi szpiegiem](211229-szpieg-szpiegowi-szpiegiem)

## Plan sesji
### Co się wydarzyło

* .

### Co się stanie

* .

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

Roland został wysłany na stację Valentina. Ewelina Sowińska, lokalna "ambasador" Sowińskich poprosiła w zawoalowany i dyskretny sposób o pomoc. Nie powiedziała dokładnie o co chodzi, ale obawia się o swoje życie. Subtelnie. I niekoniecznie. Ale jednak. Pikanterii dodaje fakt, że w oczach Eweliny Roland jest protektorem, byłym paladynem.

Oczywiście, Roland poszedł w masce. Jego aktualna maska:

* Sowiński, który wypadł z centrum uwagi a-tien Anastazego i rozpaczliwie pragnie powrócić
* Nieco narwany, dobry z pięściami, niezbyt bystry
* Przerzuca mało legalny ale mało niebezpieczny towar - sproszkowane błękitne żaby. Uważa przez to, że jest Wielkim Gangsterem.
* Szuka kanału przerzutowego i czegoś, dzięki czemu będzie w stanie POKONAĆ ORBITER. Dla Sowińskich.

Roland wykorzystuje jako wsparcie historii mało legalny mały transportowiec "Wąż w Puszczy". Wpierw zrobił jakąś operację z nimi, dał się oskubać, potem trafił na Valentinę pomóc Ewelinie...

Jako, że Roland ma przygotowany aparat szpiegowski i wsparcie oraz swoich lojalnych agentów, zajęło mu to tylko 10 dni by faktycznie przygotować wszystko i umieścić się na właściwym miejscu, sfabrykować przeszłość itp. I pozyskać sproszkowaną błękitną żabę.

### Scena Właściwa - impl

Na Valentinę Roland trafił z podbitym okiem.

Roland idzie do podłej speluny na Valentinie. Speluna która WYGLĄDA jak speluna. "Smutny Rajder" (stoły i krzesła przymocowane do podłogi). NIE, nawet Roland nie bierze całości kasy - ale bierze więcej niż powinien by wpaść w kłopoty i by było widać że to naiwniak. I tam próbuje w Rajderze znaleźć tajniaka. Najlepiej takiego co WIDAĆ że jest tajniakiem - niedoświadczony albo specjalnie by odstraszać od podłych interesów.

I tego kolesia traktuje jak prawdziwego szmuglera.

* Roland -> tajniak: "Hej, masz wjazd do syndykatu? Mam dla nich NAJLEPSZY TOWAR EVER."
* tajniak: "Mówisz? Skąd wiesz że jest zainteresowany?"
* Roland -> tajniak: "Robi show w Aurum. Wszyscy go kochają. Mam próbkę."

Tajniak wziął Rolanda "do syndykatu". A na serio - do kicia. Narkotyk (sproszkowana żaba) JEST nielegalny ale ma niską szkodliwość. I przede wszystkim jest nielegalny na planecie; na Valentinie z takich rzeczy się śmieją, tu są PRAWDZIWIE nielegalne rzeczy...

Roland ma prosty cel - wywołać odpowiednio żałosne wrażenie absolutnie niekompetentnego LORDA PRZESTĘPCY AURUM.

Tr+3:

* Vr: Roland jest uważany za "Jasia Fasolę". Niezbyt kompetentny i niezbyt rozgarnięty.
* V: To EWELINA ratuje Rolanda a nie Roland pomaga Ewelinie. W cokolwiek nie wpadła, Druga Strona nie widzi zmiany sytuacji.

NASTĘPNEGO DNIA Ewelina wyciąga Rolanda, który jest lekko skopany w kiciu. Roland się bronił, ale niewprawnie. Wyzywał na pojedynek itp. Nie pokazuje co umie. Wytargali go z celi i zaprowadzili do Eweliny w kajdankach.

* Roland: "Kuzynko, jakie barbarzyństwo! Nie dali mi się wykąpać i godnie ODZIAĆ!"
* Ewelina: <lekko zdegustowane spojrzenie przy strażnikach> <płaci kaucję>
* Roland mruczy: "Powinienem płacić więcej bo jestem więcej warty..."

Ewelina wygląda nie tak jak Roland sobie wyobrażał (zdjęcie itp). Prowadzi przez stację nie nawiązując rozmowy czy dyskusji. Przywykła, że jej polecenia są realizowane. Roland zachowuje się jak "upadły arystokrata który próbuje się odezwać". Sprawdza, czy ona JEST SOBĄ czy nie. Nie wykluczy miragenta, ale...

TrZ+2:

* X: Roland jednak Ewelinę zirytował. Jest zła na niego.
* Xz: Jest ROZCZAROWANA Rolandem. Nie będzie mu ufać. Nie chce współpracować. Roland NATYCHMIAST wplątuje info o tym co o niej wie by pokazać "more than meets the eye". (+1Vg)
* X: Ewelina NIE WIERZY że jest Rolandem. Wie, że jest Sowińskim, ale nie tym. To tylko jakiś Sowiński frajer - kto jej podkłada świnię. Roland natychmiast "z uwagi na specyfikę sytuacji Wielki Paladyn Roland mnie wysłał - jestem agentem. Pomogę. Roland dobrał mnie do specyfiki sytuacji OSOBIŚCIE." +1Vg
* Vr: ona jest przekonana że Roland nie jest Rolandem i ma do Rolanda czynny żal. ALE będzie współpracować.

Roland się nasłuchał w kabinie. Ale przynajmiej potwierdził, że Ewelina jest sobą. Fakt, że Ewelina patrzy na niego z góry i z pogardą całkowicie zignorował - nie powiązane z misją. Daje Ewelinie podpisany przez Rolanda osobiście list - jest jego agentem. Jest to agent "nie wygląda na nic ciekawego, ale jest wystarczająco kompetentny do tematów cienia i siły ognia i w odróżnieniu ode mnie ZNA TE KLIMATY" +1Vg

* V: będzie współpracować.

Ewelina -> Roland "znikają moi ludzie. Część - ciężko obita, część nie żyje.". Naturalnie Roland pyta co ona powinna robić. Ewelina - o kilku osobach blisko niej w jej świcie. Np. straciła szambelana, zginął. W ostatnim tygodniu. Zniknął na parę dni, potem pojawił się martwy i zmasakrowany. MAULED. Ktoś inny odnaleziony wpadnięty w przestrzeń gdzie sczezł bo nikt nie znalazł go szybko. Wśród jej świty od jakiegoś czasu więcej obrażeń - "potknął się i złamał nogę". I nic nie mówią.

Co ciekawe - nie ma ani jednej cute girl która by ucierpiała. Ale są młode wilczki które ucierpiały. To co ich łączy - są blisko Eweliny.

Hipotezy i działania:

* ktoś nowy w zespole koroduje zespół.
* arena, chcą dorobić albo są zmuszeni do dorabiania
* ktoś chce się ożenić z Eweliną i próbują ją odizolować
* działania szpiegowskie / do wykurzenia Eweliny

Działania:

* Roland szuka informacji w HR Eweliny - czy są pokrzywdzone jakiekolwiek osoby które NIE CHCIAŁYBY iść na arenę. => TAK są takie osoby. Arena odpada. 
    * Jeden z martwych jest lekarz Eweliny, noncombatant
* Czy ludzie którzy są u Eweliny są z Aurum czy stąd, czy mieszane? -> mixed
* Czy cierpią ludzie którzy mają konkretny profil pochodzenia, rasy... -> nie ucierpiał NIKT Z AURUM w jej świcie
* Statystyczne zwiększenie zaczęło się około miesiąca temu
* Czy ludzie którzy cierpią mają coś wspólnego na linii ZAWODU, MIEJSCA, TYP TASKA? -> sporo z trafionych to "odmiana strażnika". Mięśnie. A oni przeżywają obrażenia.
* Czy Ewelina cokolwiek z tym robiła? -> gdy opowiadała, nie wyglądało jakby jej na tym zależało. Raczej irytacja.

Roland serią pytań i bramek logicznych szuka informacji czy Ewelina coś wie. Zasób - zaskoczenie. Roland coś jeszcze dodaje - pokazuje przeszłą sytuację w której zadziałał MEGA PUBLICZNIE I GŁOŚNO i pytał laski czy ma kochanka i nie ma kochanka a jednak miała kochanka i była siara. "Jestem szczurem Rolanda od brudnej roboty".

ExZ+4

* V: Ewelina nie chce by to było publiczne. Roland idzie za ciosem - "Roland nie musi wiedzieć, chcę Ci pomóc, on mnie toleruje". +1Vg
* X: COMPEL: nikt nie może się niczego dowiedzieć, nieważne czy Ewelina powie czy Roland sam znajdzie. 
* X: Ewelina is into unsavory business herself. Zaufa jak Roland wykona dla niej prostą rzecz - odbierze transport.
    * Roland ostrzega Ewelinę: "wiesz, że jeśli robisz coś szkodliwego dla rodu Sowińskich to Roland może być tu wysłany celem anioła śmierci, nie?"
    * Ewelina wie, rozumie, przyjmuje. She means no harm to family. Roland akceptuje. Pomoże jej.

Transport. Roland wraz z grupą mięśniów udaje się w konkretne miejsce. Rusty, rundown spot, not often maintained. Jako broń - cattle prod. Podstawiony pojazd, operacja "przeładunek". Rampa. Z niej spędzani są ludzie w łańcuchach. W sporej części niewolnicy dobrze zbudowani, część chuchrowatych.

Roland wykonuje polecenia. W 100% współpracuje, nie pokazuje co o tym myśli itp. W pełni pomocny, nie przejmuje się losem niewolników. W rzeczywistości - wie już że Ewelina wraca do Aurum. Nie wie jeszcze JAK.

Ale wie, że pyta na lewo i prawo "wow, niezła operacja. Ale byłaby lepsza jakbyście jeszcze zarabiali więcej. Np. proszkiem z suszonej błękitnej żaby. Zróbmy kanał spedycyjny, zmieści się kilka beczek na statku". HUSTLER GONNA HUSTLE. I Roland chce, by Ewelina wiedziała co robi.

TrZ+3:

* X: WSZYSCY wiedzą co próbuje zrobić. Loud, obnoxious, stupid. 
    * "Seriously, Ewelina wysłała nam TO"
* V: Ewelina jako głównodowodząca operacją a wysłała TAKIEGO idiotę? Podgrzewa kocioł pod Eweliną.
    * Plus widzi slave pits.
* V: Roland buduje LINK. Będzie przerzucać ten cholerny proszek z żaby. Jego celem nie jest uderzenie w rynek - a w pozyskiwanie niewolników.
    * Jest tu też normalny, regularny rynek - handel niewolnikami, fighting pits...
* Vz: Roland jest bardziej okrutny z niewolnikami - pokazuje jak działa proszek na jednym z niewolników - prowokuje Drugą Stronę.

Gdy już Roland demonstruje działanie proszku na niewolników i ten odpływa w ekstazę Roland wyczuwa drgnięcie energii magicznej. Coś go obserwuje. Więc Roland zrobił to co powinien - wziął gorzałkę, otworzył, udał że łyknął, podał jednemu ze strażników i poszedł się odlać na bok. Gotowy ze stymulantami bojowymi.

Roland jest zaatakowany wręcz. Paker i dwóch pomniejszych. Roland... ucieka. Do pierwszego miejsca które wygląda jak 1v1 i odwraca się i atakuje na PEŁNEJ MOCY. Cel - unieszkodliwić a nie zabić. 

ExZ+3:

* V: Roland zatrzymał pakera z zaskoczenia; unfazed, keeps on attacking. Wyizolowany: 1v1. Koleś patrzy na Rolanda z czystą, żywą nienawiścią. "Na razie zbieram informacje". +Vg
* Vg: Koleś ma poświatę magiczną; wzrok niesfokusowany. 'Not entirely here'. Roland znowu ucieka, rozsypuje koraliki by ten się poślizgnął potem - nawrót. 
* Vz: Byś jest wykopsnięty i przewrócony; dwójka zachowują się jak na ciężkim kacu. 'the fuck is going on'. 

Roland predator wraca i zmienia się w Jasia Fasolę. Rzucił na ziemię torebkę z żabą i wrócił do reszty. Krzyczy głośno! "POMOCY! BIJĄ! KRADNĄ!" Przyszli ostrożnie na pomoc i widzą Rolanda który patrzy w siną dal i wygraża pięścią. Po czym "OKRADLI MNIE!". I Roland ich naprowadza - niech powiedzą mu co wiedzą. W końcu jego tylko okradli a innych ostro biją.

TrZ+2:

* V: ktoś wyśmiewa "to kto Cię napadł", Roland opisuje bysia ale tak żeby nie dało się go jednoznacznie rozpoznać. (+Vg)
    * "najkrótszy czas od wejścia do biznesu do oberwania"

Roland poszedł z nimi do baru i postawił wszystkim kolejkę. "Za nowy lepszy biznes".

* Vr: któryś z nich, włączył mu się 'fatherly drunk'. "Ty żółtodziobie, gówno a nie widziałeś". "Gdzie Ci tam do Felicjana! A patrz jak skończył!" (jeden z mięśniów co zginął). "Zagryziony przez szczury! NADŻARŁY GO! WYŻARŁY MU OCZY OBGRYZŁY MU NOS I PIĘTY." Wygląda, jakby koleś Felicjan był targetowany przez szczury. Jak był na boku to go dorwały.
* X: burda. Roland oberwie w burdzie barowej, jest zalany alkoholem i ogólnie wygląda jak ostatni plebs.
* X: areszt. Znowu. Zaś Ewelina będzie wyciągać.
* Vz: ta historia wywołała burdę. Roman. On zawsze miał pecha. Potknął, fiolka... coś w stylu "szedł, śluza, rozszczelnienie, POP i nie ma". Co jest szczególne - te dwa wypadki, nikt nigdy nie widział momentu śmierci. Zawsze był odosobniony.
    * większość śmierci w okolicy unsavory locations, specyficznie dookoła slave pits - po rozłożeniu.

NASTĘPNEGO DNIA Rolanda wyciągnęła Ewelina z celi. 

* Ewelina: "Miałeś się zachowywać. Nie tak."
* Roland: "Co mogę powiedzieć, szef mnie toleruje. Burda mi nie wyszła, ALE SOBIE DOROBIĘ."
* Ewelina: "Jak długo nie zakłócisz moich operacji?"
* Roland: "Jakich?"
* Ewelina: "A co robiłeś?"
* Roland: "Taka porządna tienka i tego typu operacja? Czy jesteś bezdennie głupia? Gdyby tu był, wziąłby Cię, przeciorał nago po stacji i publicznie ogłosił Twoją hańbę. A Ty chciałaś go tutaj OSOBIŚCIE?"
* Ewelina: <it dawns on her a little bit> prosiłam o wsparcie a nie o paladyna...
* Roland: HELLO JEST W IMIENIU? ROLAND PALADYN? Jakie Ty masz szczęście że ja przyszedłem laska... Ty wiesz że jak był młody prawie porwali go łowcy niewolników?
* Ewelina: Ale się wybronił
* Roland: Ale oni nie.
* Ewelina: Ale Roland nie musi nic wiedzieć
* Roland: Ja mu niczego nie powiem. Ale musisz zacząć współpracować ze mną. Bo musisz pomóc mi zrobić historię gdy zażąda raportu. 

TrZ (epicka opowieść o Rolandzie) +2:

* X: her gains are hers alone, her network is hers alone, can't take it.
* V: opowieść
    * jest smutna Sowińska, mało ważna, siedzi w Aurum, patriarcha nawet o niej nie słyszał. Jest nikim. I to piętnasta córka czy coś. Nie ma nic.
    * otwiera się pozycja ambasadora Aurum. Zgadza się objąć pozycję i ląduje TUTAJ. Almost raped first few days, and not once.
    * ale zafascynowała się miejscem, najpierw była jak maska Rolanda ale - fascynujące, tyle możliwości. A z trzeciej strony - ostro napadali bo laska i niegroźna i tu.
    * buduje reputację - tu thugi, tu coś, niewolnicy są efektem tego, że "ale co ja za to kupię - waciki?" Pensja wydzielona przez Sowińskich była jej zdaniem za mała.
    * coś osiągnęła, ustawiła ród Sowińskich na Valentinie. Coś się dzieje. Nie mają statku ale mogą pozyskiwać zasoby. Ale potem więcej i więcej. A fundusze te same.
    * niewolnicy i walki niewolników bardzo lukratywne. A jak znajdzie niewolnika ze skillem - takiego da się opchnąć. Ona nigdy nie była w arenach. Nie wie jak to wygląda.

Roland pokazuje jej: (+1Vg)

* zdjęcie kolesia pożartego przez szczury
* to, że agenci umierają dookoła slave pens ale skutecznie giną - druga strona ma świetnych agentów
* że on był zaatakowany i informuję, że przeciwnik był berserkerem. Czyli gdyby nie był dość dobry, kaput
* WIĘC przeciwnik eskaluje. Ostrzeżenia nie zadziałały, więc teraz ona jest zagrożona.
* Have you EVER mistreated a slave? (osobiście nigdy). Roland ostrzega - NIGDY tego nie rób. To jest wyzwalacz. Wygląda na jakąś formę klątwy.

więc:

* Vg: przyjęła to maksymalnie poważnie (I do not mistreat my people and I do not go to slave pens)

Roland pokazuje jej jakie konsekwencje będzie miało jak wyjdzie o niewolnikach. I pokazał jej że musi powiedzieć "stop" bo inaczej jej wejdą na głowę. (+1Vg)

* X: jak powie "nie", jej handler na nią zapoluje (ze strony Sowińskich) i ona o tym wie. Próbowała.
    * Roland: "możemy wykorzystać Rolanda. Powiemy mu że handler skłania Cię do niemoralnych czynów. Podniesienia sukienki. Wiesz co on mu zrobi?" (+1Vg)
* Vz: Ewelina docelowo zejdzie z tego niewolnictwa.
    * Ewelina jest niemoralna, nie jest empatyczna, ma gdzieś ludzi itp. Ale Roland przekonał ją z perspektywy utylitaryzmu. And she's open for experiments.
        * burdel forever.

Roland osiągnął swój pierwszy cel. Teraz tylko rozwiązać problem kto poluje na tych ludzi... ale zna wyzwalacz.

Roland, jak to Roland, zdecydował się na pójście konfrontacyjne. Poszedł do Speluny zobaczyć, czy ten silny koleś co go napadł go rozpozna. Nie ma tam byka; Roland trochę próbuje się dowiedzieć gdzie jest koleś i kim jest.

Koleś jest relatywnie nowy, robi swoją robotę, nie jest szczególnie okrutny ale nie zawaha się przywalić. Nie bije dla przyjemności ale nie ma większego oporu. Na imię ma Tomasz. Jest chwilowo w szpitalu. Roland natychmiast kupuje efektowny bukiet kwiatów i czekoladki i idzie odwiedzić Tomasza w szpitalu.

Tomasz jest potargany; wygląda jakby wpadł w coś mordą. KTOŚ mu coś zrobił. To nie Roland. Roland powiedział, że przychodzi od pięknej Dorotei i przynosi kwiaty i czekoladki. Wyraźnie koleś nie wie o co chodzi i Rolanda nie poznaje. "Ciemny zaułek... rury... a piękna Dorotea zrobi krzywdę tym którzy Ci to zrobili. To znaczy nie wiem jak się nazywa, tak piękna dama musiała mieć na imię Dorotea."

TrZ (bo nie wie co jest grane a Roland dużo wie na jego temat)+3:

* V: "Słuchaj, nie wiem o jakiej lasce mówisz, nie mam tu żadnej laski... NIE WIEM kto mi to zrobił."
    * Roland: "Co pamiętasz ostatnio?"
    * Tomasz: "Byłem w robocie, poszedłem się odlać..." - mówi o sektorze 'niewolniczym' choć tak go nie nazwie.
    * Roland: "Z kim pracujesz? Nie chcesz wiedzieć co Ci się stało? Nie chcesz zemsty? KTOŚ coś Ci zrobił. KTOŚ Cię w coś wrabia. Coś takiego spotkało mojego przyjaciela." (+Vg)
* X: Tomasz obsesyjnie szuka "Dorotei". Whoever she is.
* Vz: 
    * Tomasz: "Nie wiem co się stało. Robię swoją robotę, przemieszczam materiał (niewolników), jak każdy facet - poszedłem się odlać, potem nie wiem.". Ma sporo obrażeń.
    * Roland: "Nie powiedziałeś mi wszystkiego. Pamiętasz... szczury? Może nie ukrywaj. Nie pracuję dla Twego szefa. Ja przynoszę bombonierki <słodki okrutny uśmiech>" +1Vg
* V: Boi się. Bardzo. Nie pamięta co się działo, nie ma pojęcia. Wie, że w tym czasie gdy nie wie się strasznie bał i chyba rzuci tą robotę. Nie chyba - na pewno. Nie wróci
    * Roland szepcze: "Dobra decyzja. Wiesz, szczury. Jeden z tych co nie żyje to był mój przyjaciel. Rozumiesz?"
    * Roland: "Coś widziałeś? Błysk światła. Nietypowa myśl. Ruch." - Tomasz się zastanawia.
    * Tomasz jest człowiekiem ale opowiedział coś w stylu ataku mentalnego.

Roland przy wszystkich swoich zaletach ma jedną słabość. Jedną. Nie umie się skradać. Like, w ogóle. Zero. A tu by się przydał ten skill. Nowa myśl Rolanda "I hope SHE (rogue mage) is cute." Plus przybliżone miejsce w którym pracował.

CZYLI MAMY:

* magia mentalna (ten tutaj, Tomasz)
* magia dotykająca zwierząt (szczury)
* magia typu technomancja potencjalnie (śluza)

Roland robi research trupów. Inne trupy i opowieści - czy jest potencjalnie inna szkoła magii. Czy ktokolwiek zginął w sposób niekompatybilny z tymi trzema. W ZASADZIE TO JEST WSZYSTKO CO MOŻE ZABIĆ NA STACJI. Ale ogląda rany Tomasza - czy rany zadane urządzeniem czy magią? Są na nim resztki energii magicznej, ale wygląda na to, że rany są zadane kinetycznie. Normalnie.

Roland ma PLAN.

Znajduje 2 kolesi którzy są raczej podli (gwałciciele, krzywdzą ludzi) i płaci im za to by zachowywali się zastraszająco wobec jednej dziewczyny na transporcie. "Poznacie która, to będzie ta pierwsza. Kwestia honorowa." Nic nie może jej się stać, ale ma się naprawdę przestraszyć. Ani włosek z głowy. Tymczasem Roland rozmieszcza jakieś kamery automatyczne (biedadrony) w różnych miejscach i sam będzie wyczulony szukać maga. Znajdzie cutaśną czarodziejkę i wejdą w sojusz skończony w łóżku. Czy coś.

Najpierw jednak Roland przechodzi na spacer po slave pens. Chce znaleźć pasywnie maga. Ma srebrny sygnet, chodzi po niewolnikach i "ALE CZY TA OSOBA PASUJE DO TEGO SYGNETU". A na sygnecie obraz takiej starej brzydkiej kobiety. Wszyscy patrzą na Rolanda wtf z tej operacji i niewolnicy też gardzą.

Zajmuje to chwilę. Sygnet nie reaguje na żadnego z niewolników. Ale rozgrzewa się trochę w ręce w obszarze kilku cel.

Roland idzie grać z jednym ze strażników w kości i odpala 'the trap', będąc w obszarze tych klatek. Chce wyczuć / wykryć. Aha, przegrywa w kości i głośno narzeka.

Gdy dwójka strażników się wyżywają na biednej dziewczynie (and they enjoy it), w którymś momencie dziewczyna spanikowana, oni szczęśliwi. Roland czuje flarę z jednego z pomieszczeń. Flara dotyka tej dwójki. Nie ma "czaru". Nie widać maga. Jest namiar na pomieszczenie i świeże echo flary. Tamci dwaj zostali Oznaczeni przez flarę...

Czyli KONKRETNA KLATKA jest problemem. I da się wyczuć magię krwi.

Roland chce kupić klatkę (po upewnieniu się że ludzie w środku nie mają ran krwi). Więc... kupuje klatkę. DRENAŻ a nie aktywne użycie przez ludzi. Roland mówi, że ta klatka ma eleganckie zdobienia i na pewno jest warta MILIONY MONET na targu w Aurum. "Wrócę i pokażę Anastazemu Sowińskiemu tę klatkę i wrócę do chwały!"

Potem poszedł do Eweliny. Powiedział że POTENCJALNIE rozwiązał problem. Jeszcze 2 osoby mogą zginąć, poza tym spoko. Powiedział, że to był problem Magii Krwi. Ewelina zbladła. Roland nie ma zamiaru jej mówić szczegółów - niech żyje patrząc za siebie i nie dotyka tematów z niewolnikami. Dał parę przykładów i opowiedział jak został zaatakowany. Niech Ewelina się boi niektórych branż (i skupi np. na domu publicznym).

TrZ (Esuriit wtf) +3:

* Vr: Ewelina jest odpowiednio przestraszona. Cokolwiek związanego z Esuriit i magią krwi? NOPE.

Roland wyjaśnił Ewelinie dwie rzeczy:

* Ma zamiar wzmocnić to co tu się działo (she has wtf). Dzięki temu osoby które czynią zło będą osądzone. "Mój szef gdyby wiedział to mógłby docenić. It's twisted evil shit. Nie doceniłby."
* Niech Ewelina będzie informantką. A on pomoże jej wbić się na rynek. They both win.

Roland, w końcu, wziął skrzynię i wrócił na Orbiter. Tam - przebadać, przerobić, dramatycznie wzmocnić, poukrywać elementy, niech jest śliczna - i dać w prezencie komuś absolutnie nieistotnemu, nieświadomemu i niegroźnemu. Komuś, kogo nigdy to nie ugryzie. Ze źródeł powiązanych z bardziej szlachetną frakcją Orbitera.

A co wyszło z badań klatki?

* Slavers were grabbing people wherever they could.
* Dopadli dwoje braci, wyruszyli z Eterni zwiedzać świat.
* Eternianie dobrze identyfikuje magów, ale "ktoś ma potencjał not worth it"
* Potencjał przytłumił, albo nie budzą
* Jeden z braci był słabym protomagiem
* Jeden zginął na arenie, drugi w tym momencie (protomag) poświęcił się dla Klątwy. Klatka ma "filakterium" - wtopiony amulet eternijski w bazie
* Skupione na niewolnikach, ale zareaguje na wszelki rodzaj 'abuse and torment' - za mało mocy przy tej ilości rzeczy

Artefakt celował w Ewelinę, ale nie dał rady - nigdy nie weszła w promień. Zwykle atakowało solo.

Roland skutecznie wzmocnił - by artefakt dalej pełnił swoją rolę. Działał jeszcze 2-3 miesiące - potem Valentina się wkurzyła, znaleźli i usunęli. Ale co Roland pomścił to pomścił. Eternijski brat byłby dumny. Roland jeszcze przekazał anonimowo (przez agenta) informację do Eterni odnośnie tej dwójki do rodziny. By wiedzieli, że doszło do zemsty i że ci bracia nie wrócą.

## Streszczenie

Roland został poproszony przez Ewelinę Sowińską o pomoc z dyskretną sprawą na Valentinie. Udał się tam jako swój agent, odkrył, że Ewelina weszła w handel niewolnikami i osoby które krzywdzą niewolników są załatwione 'klątwą'. Roland odkrył że problemem była klatka w Slave Pens - więc ją wzmocnił i odłożył na Valentinę by mocniej uciemiężyć łowców niewolników itp. (za skrzywdzenie tienów eternijskich). Po czym zdjął Ewelinę z niewolnictwa i przepiął ją na zarządzanie domu publicznego na Valentinie. Aha, założył kanał spedycyjny przerzutu lekkiego narkotyku XD.

## Progresja

* Roland Sowiński: na tym etapie ma już zaawansowany aparat szpiegów i fałszywych Masek - tożsamości, przedmiotów, niewielkich jednostek gdzie może być kimś innym.
* Roland Sowiński: modus operandi: wchodzi jako jeden ze swoich agentów. Raczej unika powiedzenia że jest Rolandem.
* Roland Sowiński: kanał spedycyjny proszku z błękitnej żaby Aurum, znajomości na Valentinie i Ewelina która z nim współpracuje.
* Ewelina Sowińska: czuje ulgę wobec Rolanda Sowińskiego, że nie przybył osobiście i że jej wysłał szemranego agenta (spoiler: to był on w przebraniu). BOI SIĘ ROLANDA STRASZNIE.

### Frakcji

* .

## Zasługi

* Roland Sowiński: udając swojego agenta wzbudził strach w tien Ewelinie Sowińskiej na Valentinie i odstraszył ją od handlu niewolnikami, przepinając na domy publiczne. Wykrył Anomalię Krwi która mściła się za krzywdę niewolników, po czym ją wzmocnił i odłożył na Valentinę - by wrobić szlachetne części Orbitera że to oni. Nikt nie wie że tam był osobiście.
* Ewelina Sowińska: mało ważna kilkunasta córka, trafiła na ambasadorkę Aurum na Valentinie. Źle jej było ale coś osiągnęła - ustawiła ród Sowińskich na Valentinie. Weszła w handel niewolnikami bo handler ze strony Sowińskich chciał za dużo. Wezwała Rolanda do pomocy z tajemniczą krzywdą jej ludzi. Zeszła z niewolnictwa na dom publiczny i postawiła się handlerowi.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Stacja Valentina: 
                1. Speluna Smutny Rajder: mordownia gdzie są tajniacy i ludzie z różnych stron i frakcji. Podłe miejsce. Roland tam dał się aresztować za głupotę.
                1. Slave Pens: miejsce przechowywania i przerzucania niewolników w ciemnych, brudnych częściach Valentiny. Działała tam Anomalia Krwi którą kupił Roland (XD).
                1. Krwawa Arena: arena gdzie toczą się walki niewolników i niektóre krwawe eventy. Ogólnie, mało przyjemne miejsce. Zginął tu tien eternijski jakiś czas temu.

## Czas

* Opóźnienie: 62
* Dni: 2

## Konflikty

* 1 - Roland ma prosty cel - wywołać odpowiednio żałosne wrażenie absolutnie niekompetentnego LORDA PRZESTĘPCY AURUM
    * Tr+3
    * VVr: odpowiednio niekompetentny obraz Rolanda
* 2 - Ewelina wygląda nie tak jak Roland sobie wyobrażał (zdjęcie itp). Sprawdza, czy ona JEST SOBĄ czy nie. Nie wykluczy miragenta, ale...
    * TrZ+2
    * XXzXVrV: ona jest przekonana że Roland nie jest Rolandem (XD), jest rozczarowana Rolandem że wysłał agenta. Negatywne uczucia. Ale Ewelina będzie współpracować
* 3 - Roland serią pytań i bramek logicznych szuka informacji czy Ewelina coś wie. Zasób - zaskoczenie. 
    * ExZ+4
    * VXX: nie może być publiczne, nikt nie może się nic dowiedzieć, Roland zrobi dla niej jakąś robótkę
* 4 - Roland wykonuje polecenia przerzutu niewolników. "Zróbmy kanał spedycyjny proszku z suszonej błękitnej żaby."
    * TrZ+3
    * XVVVz: wszyscy wkurzeni że Ewelina wysłała 'to', podgrzewa kocioł pod Eweliną, Roland buduje kanał spedycyjny i prowokuje Drugą Stronę.
* 5 - Roland jest zaatakowany wręcz. Paker i dwóch pomniejszych. Roland... ucieka. Do pierwszego miejsca które wygląda jak 1v1 i odwraca się i atakuje na PEŁNEJ MOCY. Cel - unieszkodliwić.
    * ExZ+3
    * VVgVz: Roland zatrzymał z zaskoczenia, wymanewrował, zneutralizował i wrócił do reszty zespołu
* 6 - Roland predator wraca i zmienia się w Jasia Fasolę. I Roland ich naprowadza - niech powiedzą mu co wiedzą. W końcu jego tylko okradli a innych ostro biją.
    * TrZ+2
    * VVrXXVz: Roland nic nie powiedział, wyciągnął inne opowieści o rannych i problematycznych
* 7 - Roland -> Ewelina: "Ja Rolandowi niczego nie powiem. Ale musisz zacząć współpracować ze mną. Bo musisz pomóc mi zrobić historię gdy zażąda raportu."
    * TrZ (epicka opowieść o Rolandzie) +2
    * XVVgXVz: her gains and network are hers alone, ale Roland zna historię Eweliny i wie o walkach niewolnikach. Zdjął ją z niewolników i pomoże pozbyć się handlera. 
* 8 - Tomasz (bysiu) jest potargany i w złym stanie. Roland przepytuje go co wie. Plus, "don't you want revenge?"
    * TrZ (bo nie wie co jest grane a Roland dużo wie na jego temat) +3
    * VXVzV: Roland wie gdzie był Tomasz, Tomasz opowiedział jak wyglądało to z jego POV, Tomasz rzuci robotę - acz obsesyjnie szuka 'Dorotei' która wysłała Rolanda.
* 9 - Roland poszedł do Eweliny. Powiedział, że to był problem Magii Krwi. Niech Ewelina się boi niektórych branż (i skupi np. na domu publicznym).
    * TrZ (Esuriit wtf) +3
    * Vr: Ewelina jest odpowiednio przestraszona. Cokolwiek związanego z Esuriit i magią krwi? NOPE.

