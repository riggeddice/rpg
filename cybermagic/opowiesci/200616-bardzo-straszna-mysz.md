## Metadane

* title: "Bardzo straszna mysz"
* threads: unknown
* gm: żółw
* players: kić, darken

## Kontynuacja
### Kampanijna

* [200524 - Dom dla Melindy](200524-dom-dla-melindy)

### Chronologiczna

* [200524 - Dom dla Melindy](200524-dom-dla-melindy)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* .

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

* Darken obejmuje Gabriela Ursusa
* Kić obejmuje Laurę Tesinik.
* Tukan jest w okolicach Kalbarka

### Opis sytuacji

* Kilku pachołów Aurum skupiło się na zaimponowaniu Gerwazemu.
* Diana chce go uszkodzić, dyskretnie działa z tła by wzmocnić pętlę dewastacji.
* Diana wykorzystuje servar klasy Eidolon by zaszkodzić.
* Pojawiają się esuriańskie anomalie.
* Diana chce by to wyszło, sygnał trafia do terminusów.
* JAKOŚ przechodzi sygnał na postacie graczy
* Ksenia Kirallen łapie sygnał.
* RACE: Ksenia uderza w Aurum i Zespół LUB Zespół poradzi sobie sam.

## Punkt zero

Budowa kontekstu sesji pytaniami generatywnymi:

* Q: Dlaczego Gabriel ma w to wejść?
* A: Rekin poprosił go bezpośrednio i powiedział mu, że będzie źle.
* Q: Dlaczego Laura ma w to wejść?
* A: Kwestia echa przeszłości, coś się stało. Ważne, by Centrala nic się nie dowiedziała, bo coś z Tukanem.
* Q: Dlaczego Rekin wiedział, że coś się dzieje? Skąd się dowiedział?
* A: Dowiedział się od Lilii i tak sygnał trafił do Gabriela. Lilia -> Rekin -> Gabriel -> Laura.
* Q: Jaki jest główny atut anty-Kseniowy jaki macie?
* A: Rekiny da się poświęcić. Lepsi "swoi" bandyci niż "jacyś inni".
* Q: Dlaczego od dwóch dni ziemia drży?
* A: Eksperymenty krążą po bunkrach.
* Q: Co sprawia że Esuriit się nie wyleje mimo tego wszystkiego?
* A: Jest chwilowo dwóch Strażników. To duża moc.

## Misja właściwa

Franek Bulterier - Rekin z Podwiertu - przyszedł do Gabriela Ursusa i powiedział mu, że jest poważniejszy problem. Grupa Rekinów wpada na jakiś poważny problem i coś im może silnie zaszkodzić. On by chciał, by Gabriel na to spojrzał i się tym zajął, jako jeszcze-nie-w-pełni terminus. Na pytanie czemu on Franek dał prostą odpowiedź - to sprawa Aurum, nie sprawa Pustogoru. Ale dzieją się rzeczy tyci za duże.

Czemu Frankowi w ogóle zależy? Czemu się w to angażuje? Franek odpowiedział prosto: (V): te Rekiny to są świnie, ale nie potwory a to co tam się zaczyna dziać go niepokoi. (VV): boi się, że jego przyjaciółka, Diana Lauris, jest w to zamieszana i dlatego szuka dyskrecji. (X): Franek zmusił Gabriela, by zapewnił, że Dianie nie stanie się żadna krzywda. No dobrze, niechętnie bo niechętnie, ale Gabriel w tym pomoże. Wie też, że na tamtym terenie coś jest powiązanego z działaniami Tukana; więc chętnie lub niechętnie, ale dał znać Laurze że potrzebuje jej wsparcia.

Laura nie chciała w to się mieszać, bo niby czemu. Ale Tukana nie ma a obecność potencjalna KSENII KIRALLEN to absolutna katastrofa dla Tukana. To, czego nie wie Gabriel ale wie Laura to to, że w okolicach Czółenka znajduje się Krystalizator Esuriit; a wskazówki jakie pokazał Rekin wskazują na to, że ów Krystalizator może zacząć szwankować. A jakby Ksenia to dorwała... tak więc Laura w to wchodzi, chce lub nie chce. Nie jest w stanie się z Tukanem skontaktować, bo ten jest zajęty problemem jakiegoś mistrza walki eks-terminusa.

Siłą Laury nigdy nie była duża moc magiczna, ale zawsze było pisanie strasznych dokumentów. Tak więc napisała groźny dokument i wezwała przed swoje oblicze Henryka Wkrąża, Rekina, który jest potencjalnym problemem na tamtym terenie. Henryk przyszedł (VO) do Laury i Gabriela; chciał jej powiedzieć wszystko, bo przestraszył się tego co ona napisała (i tego, że Ksenia może się w to zaangażować).

Przesłuchanie Henryka odbyło się magiczno-mentalne; chodzi o czas i o to, że Laura i Gabriel zrozumieją rzeczy jakich Henryk nie wie by pokazać czy powiedzieć.

* V: Henryk i ekipa chcieli wkraść się w łaski Gerwazego Lemurczaka. Chcieli więc w Czółenku przygotować sobie potwora do straszenia i psucia biznesu Diany.
* XX: Diana nie wybacza. Nigdy. Diana wyeskaluje w nieskończoność, ale będzie mieć swoją zemstę.
* V: Niestety... potwór im uciekł. A dokładniej: zagrozili Melindzie, by Diana była grzeczna. Diana się bardzo przestraszyła. A potem potwór im uciekł. Kupili klatkę z "Fantasmagorii" i poszli polować na potwora. A co jest podstawą potwora? Mysz polna. SRS?!

Cóż. Nie zostało nic innego - Laura i Gabriel są przekonani, że to nie jest tak prosta i przyjemna sprawa. Szukają myszy polnej, którą mogło dotknąć Esuriit... i jeśli nie zdążą przed Ksenią, będzie katastrofa. Gabriel skontaktował się z Pięknotką Diakon, swoją przełożoną. Czy ona by mogła go przypisać do tej akcji? To ważne. I to z Laurą. Gabriel wyjaśnił Pięknotce, że chodzi o mysz polną i ma jako wsparcie Laurę i Tukan ją przypisał. Pięknotka OFICJALNIE PRZYPISAŁA Gabriela do tej misji i dodała mu Laurę (V). Ksenia czytając to miała efekt WTF (XO). Przecież... uczniowie terminusów nie powinni zajmować się potworami. Jeszcze nie.

Tak więc Ksenia zrobiła to, w czym jest najlepsza. Skontaktowała się z Laurą i zażądała wyjaśnień. Laura próbowała odeprzeć zainteresowanie koszmarnej terminuski-inkwizytorki, ale to nie takie proste. (V): przekonała Ksenię, że poradzą sobie we dwójkę, ale (XXX): dostała 2 konstruminusy jako niańkę ORAZ Tukan dostał SOLIDNY WPIERDOL od Ksenii w papierach za takie manewry i luźne przypisywanie Laury. Dla Laury i Gabriela to katastrofa, więc eskalowali do Pięknotki (V): Pięknotka wzięła opierdol Ksenii na siebie, powiedziała, że ona dowodzi operacją i Ksenia niech się wycofa. Ksenia wycofała konstruminusy. Pięknotka powiedziała Gabrielowi, że jeśli zawiedzie tu jej zaufanie, to będzie bardzo, bardzo zła.

Podzielili się odpowiedzialnościami: Laura pójdzie do Krystalizatora Esuriit i spróbuje go wykorzystać do zwabienia Myszy. Franek pójdzie do stodoły, zamontuje klatkę i będzie jej pilnował. Gabriel natomiast dowie się wszystkiego od pani burmistrz.

Tymczasem w terenie działa _Diana Lauris_, ukryta w swoim stealth-servarze klasy 'Eidolon'. Ona zbiera dowody, filmy itp. by móc pogrążyć i zniszczyć te Rekiny, które ośmieliły się stanąć naprzeciw niej...

Gabriel odwiedził Matyldę Sęk, burmistrz Czółenka. Matylda potwierdziła - jest w okolicy straszny potwór, wiele osób go widziało. To potencjalnie koszmar. Gabriel jest lekko skonfundowany, bo przecież podobno to było wzorowane na myszy, ale... co poradzić.

Laura tymczasem pokazała ząbki. Poszła do Krystalizatora ukrytego w jednym z bunkrów i weszła w interakcję z Elainką d'Krystalizator. Elainka uznała, że Laura jest niegodna używania tego narzędzia, Laura pokazała kody Tukana. (V): dostała dostęp do Krystalizatora i zsyntetyzowała najlepszą możliwą przynętę na Mysz Esuriit. (X): Elainka naskarżyła Tukanowi, że Laura bawi się energiami jakich miała nie dotykać.

(V): Dodatkowo, Laura dowiedziała się, że _ktoś_ (Diana) tu ingeruje i bawi się tymi energiami. Ktoś ewoluuje potwora świadomie. Ktoś może mieć niedługo krew na rękach. Ale (O) Druga Strona wie o Laurze i Krystalizatorze oraz o przynęcie Esuriit na mysz. Innymi słowy, Diana doszła do tego, że uczniowie terminusa próbują ją zatrzymać. Cóż, nie tacy próbowali.

Dobrze, czas iść do Stodoły i przygotować się na znalezienie tej cholernej myszy. Gabriel wykrył obecność dron nagrywających (VX): dał radę ekranować to, co robią, by nikt nie miał dowodów ani informacji. Co więcej, udało się też Gabrielowi dowiedzieć z czym ma do czynienia z pomocą Laury: to HORROROMYSZ. Istota, która wywołuje koszmary i wygląda na groźną, ale sama w sobie nie stanowi szczegółnego zagrożenia. Jeszcze. Co więcej, ma aspekty kreta. Gabriel szybko usprawnił klatkę; niech mysz nie zdąży się przebić. A Laura wsadziła tam przynętę z Krystalizatora.

Summon Mysz: (VV): Mysz zgodnie z planem + ukrycie energii Esuriit przed osobami z zewnątrz (m.in. przed Dianą), (XX): podczas operacji Zespół został poważnie Skażony; jakieś 3 dni nie mogą w sumie niczego konstruktywnego robić. Ale (V): Mysz jest złapana i powstrzymana.

Na resztkach energii Laura szuka osoby która ich śledzi: (V): to servar klasy Eidolon; infiltrator/zwiadowca, drogi i rzadki. (XX): nie ma żadnych dowodów. Wie tylko o tym, ale nie może nic z tym szczególnego zrobić w chwili obecnej. Więc poluje dalej. (V): udało jej się dotknąć umysłu - to Diana Lemurczak, która tylko dzięki swojemu _twinowaniu z TAI_ może sterować czymś takim jak Eidolon. Ale (X): Eidolon się oderwał, przeszedł w stealth i Laura tyle go widziała...

## Streszczenie

Kilka Rekinów chciało się podlizać Gerwazemu Lemurczakowi i postraszyć Dianę i Melindę. Diana w odpowiedzi sprawiła, że ich eksperymentalna 'mysz Esuriit' uciekła i zaczęła stanowić prawdziwe zagrożenie. W REAKCJI NA TO Laura i Gabriel rozbroili sytuację, wykryli obecność Diany i zatarli wszelkie ślady. Diana jednak nie wybacza i nie zapomina a Henryk też chce iść w temat dalej...

## Progresja

* Pięknotka Diakon: Dostała solidny opiernicz od Ksenii za to, że puszcza Gabriela na akcje z potworami w terenie Esuriit. Poszło do papierów.
* Tomasz Tukan: Dostał LEGENDARNY opiernicz od Ksenii za to, że puszcza Laurę na akcje z potworami w terenie Esuriit I NAWET O TYM NIE WIE. Poszło do papierów.

### Frakcji

* .

## Zasługi

* Laura Tesinik: ratowała reputację Tukana jak mogła; integrowała się z Krystalizatorem i wykryła, że twórcą myszy Esuriit jest Henryk ale w cieniu stoi Diana.
* Gabriel Ursus: arystokrata, który bardzo próbował rozbroić mysz Esuriit zanim ktokolwiek ucierpi. Ściągnął Laurę i katalitycznie chronił klatkę. Laura nad nim dominuje na akcji.
* Franek Bulterier: Rekin który chciał pomóc Dianie i nie pozwolić, by inne osoby z Aurum wpadły w kłopoty. Więc poszedł po linii arystokratycznej do Gabriela Ursusa.
* Diana Lauris: gdy grupka Rekinów chciała skrzywdzić ją i Melindę, pozwoliła im kontynuować plan - ale swoim Eidolonem doprowadziła do ucieczki myszy Esuriit i do jej ewolucji. Rozpalona przez krew Lemurczaków, została zatrzymana przez Laurę i Gabriela i zmuszona do ucieczki jak niepyszna.
* Ksenia Kirallen: wzięła operację 'potwór w okolicach terenu Esuriit', ale Pięknotka odepchnęła ją z tego przypisując Gabriela i Laurę. Napisała odpowiednie rzeczy do papierów.
* Henryk Wkrąż: Rekin, który chciał z kumplami wkraść się w łaski Gerwazego Lemurczaka; zaatakował Dianę Lauris pośrednio i wyhodował mysz Esuriit. Diana go sabotowała i Laura + Gabriel go uratowali przed absolutną hańbą i problemami.
* Matylda Sęk: burmistrz Czółenka, człowiek. Widziała mysz Esuriit i owa mysz pokazywała się jako straszliwy niepokonywalny potwór. Wezwała SOS z Pustogoru.

## Plany

* Tomasz Tukan: musi ukarać Laurę Tesinik za używanie Krystalizatora Esuriit i jego kodów. Nie wolno jej, nie powinna, nie rozumie, nie umie.

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Osiedle Leszczynowe
                                    1. Sklep z reliktami Fantasmagoria: miejsce, w którym Rekiny kupiły klatkę do łapania i kontrolowania Myszy Esuriit do dokuczania Dianie.
                            1. Czółenko
                                1. Bunkry: w jednym z nich znajduje się Krystalizator Esuriit Tukana; tam z nim się integrowała Laura by wyprodukować przynętę.
                                1. Pola Północne: w jednym z gospodarstw, w stodole znajdowała się klatka na Skażoną Straszną Mysz Esuriit. Tam doszło do "epickiej" konfrontacji.

## Czas

* Opóźnienie: 2
* Dni: 3
