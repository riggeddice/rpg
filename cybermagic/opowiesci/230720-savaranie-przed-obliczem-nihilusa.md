## Metadane

* title: "Savaranie przed obliczem Nihilusa"
* threads: brak
* motives: dotyk-nihilusa, one-by-one-they-die, dobry-uczynek-sie-msci, anomalia-memetyczna, break-the-cutie
* gm: żółw
* players: kić, flamerog, kamilinux

## Kontynuacja
### Kampanijna

* [230720 - Savaranie przed obliczem Nihilusa](230720-savaranie-przed-obliczem-nihilusa)

### Chronologiczna

* [230201 - Wyłączone generatory memoriam Inferni](230201-wylaczone-generatory-memoriam-inferni)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * Delain "Creatures"
        * "You try, you stall, you're forced to give it all" - "But when your back's against the wall, who's gonna catch you when you fall"
* CEL: 
    * Highlv
        * Ralf, jak się to stało? Zabijemy go?
    * Tactical
        * ?

### Co się stało i co wiemy

* Przeszłość
    * Ralf i Arnold uratowani przez Kanta Ravis, 10 dni temu
    * Lekarka (Ralena) zbadała Ralfa (zdrowy), Arnolda (nie żyje). Ralena zobaczyła Nihilusa i UDAJE ŻE NIE WIDZI.
    * Główny Inżynier Ryman znalazł niesprawne generatory memoriam; zobaczył Nihilusa i zrozumiał.
    * TAI Ikaria próbuje zatrzymać Rymana, ale widząc beznadziejność się sama odcięła by nie propagować.
        * spawnowała uproszczony submoduł Ikaria-2..3..14: by się upewnić, że wszystko działa.
    * Anomalia gnieździ się w Przedniej Części, tam, gdzie była przechwycona ratunkowa kapsuła Ralfa
        * Rdza
        * Shadow, hypnotic, VISION
        * Ofiary mają chęć samobójstw
        * Mechanizm memetyczny - im więcej WIESZ o Anomalii tym mocniej na Ciebie działa
    * Ralf i Arnold próbują ochronić wszystko. "To z nimi przyszło".
* Frakcje
    * Savaranie (default)
        * Ich nie dotknęło Spojrzenie Nihilusa, oni robią co mogą.
        * impuls: przetrwać, naprawić swoją jednostkę, współpracować z agentami ratunkowymi
    * Maereanie (mourners)
        * Dotyk Nihilusa sprawił, że stracili nadzieję. All is lost.
        * impuls: ZAPOMNIEĆ, ZNISZCZYĆ!
    * Ludifikaci (delusioners)
        * Dotyk Nihilusa sprawił, że uciekli w swoje złudzenia.
        * impuls: RZECZYWISTOŚĆ NIE ISTNIEJE!
* Rozkład jednostki Kantala Ravis (ok. 200 osób)
    * Statek podłużny, w formie cylindra
    * Transport w formie 'pociągów'
    * Przednia część
        * silniki
        * silniki manewrowne
        * harvesters, drony
        * magazyny
        * fabrykatory
        * detektory
        * magazyny wody
        * broń
        * siłownia / rozrywka
    * Środkowa część
        * AI Core
        * Life support
        * Kwatery mieszkalne
        * mesa, żywność
        * Medical
    * Tylna część
        * silniki manewrowne
        * silniki
        * reaktor główny
        * inżynieria
        * kociarnia pacyfikatorów
        * śluzy, ładownia, magazyny

### Co się stanie (what will happen)

* F1: Wejście na pokład. Nowa nadzieja.
    * S1: Coś jest bardzo nie tak z reaktorem i silnikami; Kanta Ravis wzywa SOS
    * S2: Rdzeń reaktora, główny inżynier próbuje go wysadzić, zwalczany przez Ikarię i innych inżynierów; ma pomoc trzech uzbrojonych ludzi.
    * S3: TAI jest uszkodzona; odcięła sobie wyższe kontrolki, sama się lobotomizowała (bo wie z czym ma do czynienia i jest memetycznie skażona).
* F2: Coś jest nie tak
    * S: Inżynier w katatonii, z wydrapanymi oczami (dotknął go Nihilus)
    * S: Ralena (lekarz) 'wszystko jest w porządku, nie ma patogenów itp'
    * S: Znalezione dwa pacyfikatory, które _leżą_. Nie ma w nich woli życia (w tylnej części); koty unikają przodu statku
* F2: Nihilus narasta, dotyka też ratowników.
    * S: Sarea zabija swojego syna i celuje w męża (kapitana)
    * S: Gdzieś tu kapitan Kirten ogłosi kwarantannę
    * S: Zjawy rodziców Ralfa próbują go dorwać; Arnold broni Ralfa
    * S: Ralena się broni, że WSZYSTKO JEST W PORZĄDKU!!! mimo działania i słów Nili
    * S: Ataki Nihilusa: 
        * mechanizmy rdzewieją i ktoś ginie; ktoś inny traci nadzieję i TO jest siła Nihilusa (wskazówka)
        * pacyfikator-zmora, zrobiony z cienia i pustki
        * ojciec Ralfa się pojawia i robi symbol 'przecięcia gardła'
        * żołnierz "nic nie czuję NIC NIE CZUJĘ!" rozbijając ręce i głowę do krwi "NIC NIE CZUJĘ! NIECH TO SIĘ SKOŃCZY!!!"
* F3: Dominacja Nihilusa
    * S: kucharz zatruwa jedzenie
    * S: Arnold obraca się przeciw Załodze

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

Zespół:

* Marcelin Viirdus: advancer i ekspert od zbierania śladów; żołnierz walki kosmicznej
    * OCEAN: (E-N+C+): pozornie nie mający osobowości cichy ekspert od znajdowania problemów tam gdzie nikt się ich nie spodziewa
    * VALS: (Benevolence, Security): "wszystko co robimy, robimy dla nas samych jutro; każdy kiedyś potrzebuje pomocy"
    * Core Wound - Lie: "zginęło zbyt wielu naszych i nic nie możemy na to poradzić" - "jeśli wszystkiego się dowiemy i przed wszystkim zabezpieczymy, przetrwamy"
    * styl: raczej cichy, woli demonstrować umiejętności niż gadać.
* Agnieszka Serkis: advancer i medyk o świetnej intuicji i empatii
    * OCEAN: (E+N+A+): bardzo empatyczna i silna intuicyjnie, szybko się decyduje pod presją. Wrażliwa jak na medyczkę. "Jeśli czegoś nie zrozumiesz, to znaczy, że nie patrzysz wystarczająco głęboko."
    * VALS: (Achievement, Stimulation): dąży do bycia najlepsza w swojej dziedzinie, no bullshit "coś nowego! Coś, czego jeszcze nie robiliśmy!"
    * Core Wound - Lie: "coś DZIWNEGO zniszczyło mój statek" - "jeśli wszystko zrozumiem i znajdę, się to już nigdy nie powtórzy!"
    * styl: niezależna, zdecydowana, gotowa na wyzwania. "Nigdy nie wiesz co spotkasz, ale jak jesteś wyczulona i przygotowana to sobie poradzisz"
* Rafał Kurrodis: advancer, ekspert od zabezpieczeń i inżynier
    * OCEAN: (C+O+E-): Perfekcjonista z umiejętnością dostrzeżenia szczegółów, które innym umykają. Poważny i skoncentrowany na swojej pracy. "Bezpieczeństwo jest moją pierwszą i ostatnią myślą."
    * VALS: (Conformity, Security): Wierzy w istotność zasad i procedur, które chronią życie i majątek. "To nie sprzęt, to nasze życia."
    * Core Wound - Lie: "Ktoś spieprzył i straciłem rękę" - "Jeśli będę bardzo uważał i trzymał się procedur, możemy wszystkich uratować"
    * styl: Praktyczny, metodyczny, dokładny. Zawsze sprawdza podwójnie. "Nie ma miejsca na błędy w kosmosie."
* Lena Morazik: specjalistka od komunikacji, negocjatorka i psychotroniczka. Kiedyś: biuro propagandy.
    * OCEAN: (E+N-A+): Optymistyczna i pełna energii. Talent do łagodzenia napięć. Idealna kumpela. "Możemy rozmawiać o wszystkim."
    * VALS: (Humility, Self-Direction): Wierzy w służbę dla innych, ale każdy ma prawo wyboru jak. "Każdy ma swoje miejsce w Wielkiej Maszynie i każdy znajdzie własne."
    * Core Wound - Lie: "Kazali mi wspierać nieprawdziwy system" - "Jeśli wszyscy mówią prawdę i ujawnimy swoje różnice, znajdziemy coś, co działa"
    * styl: Empatyczna, cierpliwa, niemożliwa do wyprowadzenia z równowagi. Awatar spokoju. "Jeśli Ci źle, porozmawiaj ze mną. Pomogę Ci."
* Jola Seklamant: inżynier strukturalny, specjalista od silników i fabrykacji
    * OCEAN: (C+O+E-): Precyzyjny i skrupulatny, nieustannie szuka usprawnień. "Silniki mówią. Ty nie słuchasz."
    * VALS: (Achievement, Self-Direction): jego osiągnięcia są dowodem jego wartości. "Wszystko potrafię zrobić ze statkiem jeśli trzeba"
    * Core Wound - Lie: "Wszyscy się śmieją z mojej obsesji na punkcie silników" - "Jeżeli stworzę najdoskonalszy silnik, w końcu mnie docenią."
    * styl: Zafascynowany swoją pracą, ciągle na bieżąco. "Szum silników to poezja"
* Igor Seklamant: ekspert od walki wręcz w ograniczonym terenie, dewastator przełamania, koleś od zastraszania i porządku.
    * OCEAN: (E+N-A-): Dominujący, agresywny i kłótliwy, ale lojalny dla swoich. "Moim nic nie będzie. Ty - słuchaj poleceń."
    * VALS: (Face, Family): Ważny dla niego jest respekt, siła i ekipa - zwłaszcza starszy brat. "Najpierw pokazują brzuszek, potem odpowiadają na pytania. W tej kolejności."
    * Core Wound - Lie: "Jak tylko się odwracał, kocili jego siostrę" - "Jeśli sterroryzuje wszystkich, NIKT nie odważy się dotknąć jego bliskich"
    * styl: Wybuchowy i agresywny, ale to kontroluje; to strategia a nie natura. Ryzykuje dla przyjaciół. "Koniec gadania, czas dać w mordę."

Statek - Kantala Ravis 

* Mikael Ravis: kapitan statku, 48
    * OCEAN: (E+C+O-): Autorytarny i honorowy, trzyma się tradycji i zasad. "Honor i lojalność jednostce to najważniejszy atrybut dowódcy."
    * VALS: (Tradition, Benevolence): Utrzymuje i pielęgnuje tradycje swojego ludu, wiedząc, że on pierwszy jest do wymiany
    * Core Wound - Lie: "Kiedyś PRAWIE zniszczyłem Kantalę postępując wbrew zasadom" - "Nigdy nie dojdzie do tragedii jeśli postawię jednostkę przodem"
    * styl: "Honor i tradycja to moje prawo, moja tarcza i mój obowiązek"
    * rola: on pierwszy będzie wspierał Ratowników. Statek ponad wszystko.
* Sarea Ravis: pierwszy oficer statku, 45
    * OCEAN: (C+A+N+): Skupiona, przyjazna, gotowa do poświęceń dla dobra załogi. "Jesteśmy jednym organizmem, a ja jestem jego tętnicą."
    * VALS: (Conservation, Family): W pełni oddana konserwacji statku i dobru swojej 'rodziny'. "Nasza siła tkwi w jedności i oszczędności. To jest nasza droga."
    * Core Wound - Lie: "Moja matka umierała w męczarniach i nic nie dało się zrobić" - "Jeśli zadziałam perfekcyjnie, nikt nie będzie tak cierpiał w cyklu życia"
    * styl: Spokojna, uśmiechnięta, cicha, nieustępliwa. Wsłuchuje się w ciszę statku. "Cisza to nasz nauczyciel. Wsłuchaj się w nią, a zrozumiesz wszystko."
    * rola: Pokazanie korupcji Nihilusa - ona traci nadzieję i chce zabić najbliższych by nie cierpieli.
* Kiran Ravis: 21 lat, junior inżynier i mechanik, pracuje pod opieką starszych mechaników.
    * OCEAN: (O+E+N-): Ciekawy świata, energiczny, zawsze chętny do pomocy. "Chcę zrozumieć jak wszystko działa, żeby móc naprawiać i utrzymywać nasz dom w najlepszym stanie."
    * VALS: (Achievement, Stimulation): "Nie mogę doczekać się, kiedy będę mógł naprawiać silniki samodzielnie! Chcę być tak dobry jak starsi mechanicy!"
    * Core Wound - Lie: "Jestem za młody i niewystarczająco doświadczony, żeby być pożyteczny" - "Jeśli nauczę się jak najwięcej, będę niezbędny dla załogi."
    * styl: Pełen entuzjazmu, otwarty, nieco naiwny, ale zawsze chętny do nauki.
    * rola: zaprzyjaźnił się z Ralfem, nie rozumie co się tu dzieje
* Nila Veress: 23 lata, medyk, zajmuje się opieką nad chorymi i rannymi.
    * OCEAN: (A+E+N+): Wrażliwa, troskliwa, zawsze gotowa do pomocy. "Troszczę się o każdego członka naszej załogi jak o własne dziecko."
    * VALS: (Benevolence, Achievement): "Każde życie jest cenne. Musimy zrobić wszystko, aby zapewnić każdemu zdrowie i bezpieczeństwo."
    * Core Wound - Lie: "Nie udało mi się uratować jednego z naszych" - "Jeśli będę stale się uczyć i doskonalić swoje umiejętności, będę w stanie uratować każdego."
    * styl: Empatyczna, ciepła, bardzo troskliwa. Zawsze gotowa na poświęcenie dla dobra innych.
    * rola: nie rozumie co się tu dzieje i czemu Ralena niczego nie zauważa
* Ilena Veress: 42 lata, naukowiec i odkrywca, specjalizująca się w badaniu nieznanych form życia i anomalii.
    * OCEAN: (O+E+): Ciekawa świata, kreatywna, spontaniczna. "Wszechświat to nieskończona tajemnica, pełna cudów do odkrycia."
    * VALS: (Self-Direction, Achievement): "Chcę odkrywać i uczyć się jak najwięcej o wszechświecie i dzielić się tą wiedzą z innymi."
    * Core Wound - Lie: "Miałam DOWODY na anomalie itp. Gdybym powiedziała, miałabym rodzinę!!!" - "Jeśli zrozumiemy co się dzieje, możemy to naprawić."
    * Styl: Pełna entuzjazmu, nieskończenie ciekawa, nieustannie poszukująca nowych odkryć. Optymistyczna.
    * CORRUPTION: "Nic nie ma znaczenia. Nic. (niszczy swoje badania). Nic nie rozumiemy. Moja rodzina nie miała szans." Samozniszczenie i rozpaczliwy hedonizm.
* Joren Tillak: 55 lat, specjalista ds. recyklingu i utrzymania ekosystemu.
    * OCEAN: (O+N+C+): Znawca swojego fachu, cierpliwy, skoncentrowany na detalach. "Nasze przetrwanie zależy od równowagi. Musimy zrozumieć każdy element ekosystemu."
    * VALS: (Conservation, Family): "Oszczędzamy wszystko co możemy. Każda rzecz, której nie używamy, to rzecz marnotrawiona."
    * Core Wound - Lie: "Nasz ekosystem kiedyś prawie zginął" - "Jeśli będę czujny, mogę zapobiec katastrofie."
    * Styl: Spokojny, rzeczowy, zawsze czujny. 
* Daven Hassik: 45 lat, specjalista od łączności, zbierania danych i informacji.
    * OCEAN: (O+C+A+): Analityczny, spokojny, skoncentrowany na szczegółach. "Informacje to nasza siła. Im więcej wiemy, tym lepiej możemy działać."
    * VALS: (Achievement, Security): "Chcę zebrać jak najwięcej informacji, żeby zapewnić naszej załodze bezpieczeństwo."
    * Core Wound - Lie: "Kiedyś nie zrozumiałem sygnału i przez to wpadliśmy w kłopoty" - "Jeśli będę uważnie słuchać i analizować, zapobiegnę wszelkim problemom."
    * Styl: Wycofany, poważny, zawsze skoncentrowany na swojej pracy.
* Renna Xidus: 35 lat, kucharz i rolnik, dba o zapasy żywności i uprawy na statku.
    * OCEAN: (E+A+C+): Entuzjastyczna, pracowita, skoncentrowana na swojej pracy. "Zdrowa i smaczna żywność to fundament naszego przetrwania."
    * VALS: (Conservation, Achievement): "Musimy oszczędzać i maksymalizować nasze zasoby żywnościowe. Każda strączkowa roślina ma znaczenie."
    * Core Wound - Lie: "Zdarzyło mi się marnować jedzenie" - "Jeśli będę uważnie planować i zarządzać naszymi zapasami, zapewnię nam dostatek."
    * Styl: Żywiołowa, optymistyczna, zawsze gotowa na now
* Varon Zedik: 50 lat, dowódca statku, doświadczony i decyzyjny.
    * OCEAN: (E+O+C+): Decyzyjny, optymistyczny, zorganizowany. "Musimy działać jak jedna załoga, jeden organizm, aby przetrwać."
    * VALS: (Power, Achievement): "Muszę być silnym liderem, aby prowadzić nasz statek ku przetrwaniu."
    * Core Wound - Lie: "Były momenty, kiedy popełniłem błędy jako dowódca" - "Jeśli będę zawsze czujny i gotowy na wyzwania, mogę prowadzić nasz statek bezpiecznie."
    * Styl: Poważny, stanowczy, ale również troskliwy i ochronny wobec swojej załogi.
* Saran Kelos: 37 lat, strażnik i oficer bezpieczeństwa, odpowiedzialny za utrzymanie porządku i bezpieczeństwa na pokładzie.
    * OCEAN: (C+E+A+): Zorganizowany, energiczny, asertywny. "Musimy zawsze być przygotowani na każdą ewentualność, aby chronić nasz statek."
    * VALS: (Security, Achievement): "Chcę zapewnić bezpieczeństwo wszystkim na pokładzie i bronić naszego statku przed zagrożeniami."
    * Core Wound - Lie: "Kiedyś nie byłem w stanie zapobiec incydentowi na pokładzie" - "Jeśli będę zawsze czujny i zdecydowany, mogę ochronić nas wszystkich."
    * Styl: Poważny, zdecydowany, zawsze gotowy do działania i ochrony załogi.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Opis jednostki:

"Wnętrze statku jest surowe i nieprzyjemne dla oka, ale emanuje spokojem i ciszą. Wszędzie panuje półmrok, a jedyne dostępne światło wydaje się pochodzić od niewielkich, oszczędnych źródeł o niskim natężeniu.

Wyraźnie zauważalny jest minimalizm - nie ma tu zbędnych elementów, każdy kąt, każda przestrzeń wydaje się być celowo i efektywnie wykorzystana. Wszystko tu ma swoje miejsce i swoje zadanie. Sprzęt jest stary, ale dobrze utrzymany, z śladami niezliczonych napraw.

Wszystko tu jest skoncentrowane na oszczędności i ochronie - od cichego brzęku maszyn, przez ciężkie, zamykane drzwi, po recykling powietrza i wody, których systemy są widoczne i dostępne dla wszystkich."

Kiran Ravis powitał nowoprzybyłych z charakterystycznym savarańskim tekstem:

* Problem, okolice reaktora. Niestabilność. Ikaria nie współpracuje, nie pamięta wszystkiego
* Silniki... ciche. Nie ten dźwięk.
* Główny Inżynier mówi, że wszystko jest w porządku. Nie jest w porządku.

Agnieszka -> Zespół:

* "Co to znaczy że główny inżynier nie jest w porządku???"

Decyzja - robią komunikację wewnętrzną przy użyciu sandboxów. Jeśli TAI Ikaria coś złapała, nie mogą pozwolić sobie na to, że coś złego się stanie i też się od niej zarażą.

Jola idzie w stronę na silniki. Igor szuka anomalii, przeczesuje no-alert wraz z Jolą. Rafał sprawdza logi, Lena wysłana na psychotronikę by zbadać Itrię dobrze.

Tp +3:

* Vr (p): Igor zauważył coś nietypowego. Widzisz w jednym z tuneli inżynierskich... dwa nie ruszające się żywe koty. To nie jest miejsce dla tych kotów.
    * wysłałeś drona. Koty to zignorowały. TAK APATYCZNE KOTY.
* V (m): Nic tu NIE MA. Zmarznięte, ale nawet do siebie się nie przytuliły.
    * siedzą w ekskrementach. Nie ma 'w środku' niczego. Nie ma powodu takiej pasywności
    * Agnieszka poprosiła o koty. Jola i Igor je pozyskali. Koty nie oponowały. Po prostu 'są'.
* V (e): Drona przeleciała dokładniej i wzmocniła skanowanie.
    * w niektórych miejscach jest rdza. Statek, venty przerdzewiały. Łącznie z terenem pod kotami. Dochodzi to z 'przodu' a drugą stroną jest silnik.
    * jeżeli to idzie wprost, dochodzi z harvesterów
    * ślady kocich łapek - przerdzewiałe ślady.

Rafał analizuje logi. Szuka głównie błędów, errorów itp. Zalanie, uszkodzenia.

* Logi raportują że 'wszystko jest w porządku'. Jak tylko AI dochodzi do jakiegoś momentu zrozumienia, RESETUJE i 'wszystko jest w porządku'
* 14 dni temu na pokład tej jednostki przejęto kapsułę ratunkową. Dwie osoby - ojciec i syn. A 3 dni później był pierwszy 'wszystko w porządku'.

Lena i Rafał skanują TAI Ikarię.

Tr Z +2:

* X (p): (HIDDEN - podatność na Nihilusa) Lena dostała dziwną informację
    * "Rafał, rozmawiamy z Ikarią-24. 23 poprzednie poprzednie wersje Ikarii zostały skasowane. Ikaria nic nie pamięta, bo jej subprocesy nie mają pamięci."
* Vz (p): Lena popatrzyła na Rafała dziwnie. "Ikaria się sama lobotomizowała. Odcięła się od statku."
    * R: Możemy ją naprawić?
    * L: Oczywiście
* X: Lena jest podatna na Nihilusa. She is doomed (dotyka ją anomalia memetyczna).

Próbujemy w głąb pamięci Ikarii zajrzeć. Lenie powinno się udać. I jednocześnie operacja 'budowa rdzenia AI na Lataravii'.

* V: Lena wchodzi w głąb Ikarii, analizuje co tam się dzieje. STRES Leny zaczyna rosnąć liniowo. (infekcja)
    * można to przerwać
* Vz: TYMCZASOWO Lena jest nieprzytomna. Koma. Ale - sygnały przerwane.
    * Lena jest bezpieczna CHYBA że pierwszy dotyk Nihilusa

Silnik savarański jest SABOTOWANY. Zrobiony przez człowieka. Wprawnego. Inżyniera?

Tp+3:

* V: sabotaż jest ciekawy, bo to typowy sabotaż kultu śmierci. Jak się włączy silnik dalekosiężny, statek wskoczy w Eter i nie wyjdzie. To jest sabotaż mający zniszczyć statek. Plus ślady rdzy.
* V: gdzie jest inżynier - główny inżynier coś majstruje przy reaktorze. Z obniżonym poziomem ochrony.
    * Główny Inżynier: "to nie ma znaczenia..."

Igor wpada unieszkodliwia inżyniera. W kącie nieaktywny kot-pacyfikator.

Pojawia się Cień Arnolda. Pokazuje symbol poderżnięcia gardła, nie atakuje.

Igor robi promień światła jak flashbang. Rozproszyć i dowiedzieć się co to za ziółko.

Tr Z +3

* V: Gest podrzynania gardła. Twarz Arnolda.
    * Igor podnosi osowiałego kota i pokazuje go Arnoldowi, blisko, patrzy na efekt.
* Vz: Cień Arnolda nie zrobił żadnego ruchu wobec kota. Kot, z rezygnacją, skoczył. Cień się rozproszył. Masz na ziemi na oko 100-letnie zwłoki kota.

W medycznym, Agnieszka o kotach:

* "Słuchajcie, te koty są... zdrowe. Ale nie mają woli życia. Nie wiem jak to nazwać. Nie reagują na ból, na nic. Nie, nie są stare."

Tr Z +3:

* Xz: 
    * Agnieszka zauważyła, że Lena jest starsza. O jakieś 2-3 lata czy coś. Lena ma mniej energii, jest biologicznie starsza i straciła... część tej radości.
    * Koty też są starsze, ale to nie jest przyczyna ich zachowania
* V (p):
    * Koty COŚ widziały, co odebrało im jakąkolwiek wolę życia. One są częściowo wyssane z energii.
* V (m):
    * Inżynier i koty są dotknięte tym samym. Lena też. Tylko Lena słabiej. -> IKARIA tak samo.
    * Inżynier "To nie ma znaczenia" "Gwiazdy zgasną" "Nic co zrobimy nie ma znaczenia".
* V (e): 
    * Lena jest reanimowana. Widziała coś STRASZNEGO.
    * Inżynierowi nie da się pomóc. Lena doszła do tego dlaczego on to zrobił. Inżynier chciał oszczędzić cierpienia. Bezbolesna śmierć dla wszystkich.
    * Medyczka (Ralena) coś upuściła. Zachowuje się "normalnie", ale ręce jej drżą i próbuje trzymać fason.

Igor kładzie jej rękę na ramieniu i "też to spostrzegłaś". Savaranka. "Nie. Wszystko jest w porządku. WSZYSTKO JEST W PORZĄDKU! NIC SIĘ NIE ZMIENIŁO!!!"

Wsadzamy kota pod prysznic. A potem Lenę -> kot jest jak zawsze, pasywny i nieaktywny. Kotu tak nie pomożemy. Ale Lena lekko odżyła. Dowodem, że kazała się odwrócić w którymś momencie (nie patrz na mnie, Rafał, nie jestem ubrana). Prysznic Lenie pomaga. Kotu nie. Inżynier pod prysznicem jest pasywny. Jemu nie pomogło. Czyli "weszło za głęboko".

Jola poszła do generatorów memoriam w przedniej części statku. Zbliżając się do generatora, słyszysz mokre dudnienie. Igor kieruje się do Joli. Jola widzi inżyniera, nagiego, z wydrapanymi oczami uderzającego głową i rękoma w ścianę koło generatora Memoriam. Tp +3: 

* Vr: Ogłuszony, ale to było trudne. Koleś nie czuje bólu.

Generator Memoriam jest spalony.

Sygnał do Lataravii: potrzebujemy awaryjny generator Memoriam.

* Rafał: lista przydatnych pasażerów
* Igor: info o tych uratowanych i chce z nimi porozmawiać
* Marcelin: idzie do kapsuły ją zbadać

W części mieszkalnej jest trzech savaran. Jeden to młody chłopak, 2x, drugi też 2x a trzeci ~4x~5x.

* Arnold, Ralf, Kiran

.

* Igor: "kapsuła ratunkowa, skąd przybyliście, co się stało?"
* Arnold:
    * poprzednia jednostka została zniszczona, spotkaliśmy potwora. Pustka. Potwór pustki. Pożarł nasz statek. Uratowałem syna.
    * (TEN CIEŃ MA PODOBNE RYSY TWARZY JAK ARNOLD)
* I: Przejdźmy się może do medycznego...
* A: (skłonił się)
* I: (całą trójkę ze sobą)

.

WSTECZNIE: Jola potrzebuje pacyfikatora. Zdrowego. Ustawiamy pułapki, na każdym wyjściu, pięścią w skrzynkę. Rozpełzamy się i polujemy na koty. Wypłoszyć je. Rafał, widzisz kota. Zbliżasz się do niego. Kot ostrożnie się porusza... coś z tym cholernym kotem jest nie tak. Nie do końca wiesz co. Ale kot się rusza. Spojrzał na Rafała. Zamiast oczy ma "portal do świata gdzie zgasły gwiazdy". Rafał kieruje nań strumień światła. Kot jest bardziej zrobiony z cieni niż prawdziwy. On tylko animuje ciało pacyfikatora. A koło niego wszystko jest przerdzewiałe. I zbliża się do skoku na Ciebie.

Tr Z +3:

* Vr (p): Rafał da radę zwiać przed kotem ZANIM ten go Skazi
* V (m): Rafał zwiał do Igora
* Vz (e): Igor łapie kota w skrzynkę. Do śluzy, bo przekorodowuje. ŚPIEWA RADOŚNIE PIEŚŃ VALHALLI! 
* X (p): Skrzynia nie wytrzyma i kot wyleci ze skorodowaną skrzynią.
* Xz (m): ŚLUZA odcięta, bo kot tam został. Ale go wyssało.
* V (e2): Kot wyleciał, ŚLUZA odcięta i Skażona, ale Igor pozbył się anomalnego kota.

Skażona Śluza jest odrzucona...

Joli i Rafałowi udało się złapać jednego przestraszonego pacyfikatora.

SYTUACJA AKTUALNA:

Igor idzie z Arnoldem, Ralfem i Kiri. Jola dała mu czterołapczaka. Kot NIE CHCE tam być. Kiri się uśmiechnął. Wyciągnął rękę. Pacyfikator wlazł Kiri na ramię, ale gdy Kiri próbuje podać kota Arnoldowi, kot protestuje. Nie chce. Próbuje go nie dotknąć. Igor 'ustycznia kota i Arnolda'. Kot 'wrzasnął' jakby nadepnąć mu na ogon, próbuje czmychnął.

Tp +3:

* V: Rzucony kot w Ralfa. Kot jest bardzo niekomfortowy, ale to nie ta sama siła reakcji.

.

* Igor: "idziemy do medycznego", "oni są źródłem; jeśli dla mnie, cena dwóch żyć nie jest wygórowana".
* Agnieszka: "dane o przybyszach są zamaskowane przez Ralenę. Ona je skasowała."

Lataravia wysłała wahadłowiec. Wahadłowiec dokuje w bezpiecznej śluzie. 

Informacje od Marcelina: "Kapsuła jest... trochę dziwna, jest starsza niż wynika z naszych jednostek. Postarzyła się. A jednocześnie jest stabilna. Trzyma się kupy. Nie ma awarii. A jej AI... Marcelin wolał nie uruchamiać. Uznał, że to zbyt niebezpieczne." On rekomenduje wyrzucenie i zestrzelenie z Lataravii.

Igor idzie z Raleną, Arnoldem i Ralfem na pokład wahadłowca.

Tp +3:

* Vr (p): Na pewno wejdą na pokład z Igorem
* X (p): Arnold się wyraźnie zorientował, że coś Igor planuje, ale nie staje Igorowi na drodze
* V (m): Igor "after you", odciął ich w wahadłowcu
    * Ralena, Ralf i Arnold.

Lena rozmawia z Arnoldem, Ralfem i Raleną. Naciska na Ralenę.

Tr Z +4 +3Og +3Ob:

* Xz: 
    * Ralena (medyczka) "wszystko jest w porządku, wszyscy są zdrowi, nic się nie dzieje"
    * Igor "mówiliście, że nie da się tego pokonać, widzieliście jak zostali na statku, otaczało Was... chciałbym wiedzieć więcej... nie wiadomo jak walczyć... poznać najwięcej jak możliwe"
        * KĄTEM 'opiszcie najbardziej jak to było' by ściągnąć Nihilusa.
* Vz:
    * Ralena: "ja nic nie zrobiłam, nic, nic nie mogłam, nic nie widziałam..."
    * Ralf: (też zaczyna panikować powoli) "to było za duże, to było..."
    * Arnold: "NIE ZADAWAJ TEGO PYTANIA, bo to ściągniesz!" /Arnold ma oczy pełne pustki
* Ob:
    * Arnold manifestuje się jako cień. On jest potworem. 
    * Ralena krzyczy "wiedziałam że nie żyjesz! Byłeś stary! Ale musiałam! Za duży! Tylko tak mogłam uratować statek!"
    * Ralf jest magiem. A Arnold jest cieniem.
    * Lena jest wypłaszczona. Psychotroniczka.

LASER W STATEK! Lena nabita i do śluzy. Nabić i wystrzelić, by śluza była otwarta.

(+3Vg).

* X: Wahadłowiec EKSPLODOWAŁ. Nie udało się wypalić laserem, ale udało wysadzić.
* Vg: Lena poza burtą. Nawet nic nie poczuła. Ona już nic nie czuła. Nihilus ją Dotknął.

Resztę uda się uratować.

## Streszczenie

Kantala Ravis uratowała rozbitków z innej jednostki, którzy przynieśli na statek Anomalię Nihilusa. Lataravia przyleciała ratować ratujących. Pacyfikatory - apatyczne, ludzie pod wpływem Nihilusa wpadają w rozpacz ("niech się skończy!") lub w wyparcie ("wszystko w porządku!"). Lataravia odkryła, że rozbitkowie przynieśli anomalię i są najpewniej kotwicą, więc wyrzucili rozbitków w kosmos. Udało im się stracić tylko jednego członka załogi.

## Progresja

* .

## Zasługi

* Jola Seklamant: Wydobywała apatyczne Pacyfikatory z rur, badała silnik (odkrywając sabotaż i Skażenie), pozyskała zdrowego Pacyfikatora i doszła do tego, że mają potężną anomalię - Generatory Memoriam spłonęły.
* Igor Seklamant: chronił swoich jak umiał. Gdy Rafała gonił anomalny Pacyfikator, wsadził go do skrzyni i wywalił ją poza śluzę. Gdy uznał że problemem jest Ralf i Arnold, wsadził ich na wahadłowiec i kazał wysadzić. Działał szybko i zdecydowanie - i bezwzględnie.
* Rafał Kurrodis: Badał logi TAI Ikarii i odkrył, że Ikaria jest bardzo nie w porządku, sama się lobotomizowała. Potem przy pozyskiwaniu Pacyfikatora spieprzał przed anomalnym Pacyfikatorem Nihilusa i wpakował go w pułapkę Igora.
* Agnieszka Serkis: medyczka, którą zaniepokoił stan inżyniera na Kantali Ravis i która zauważyła niezgodności w dokumentacji medycznej. A potem skupiła się na anomalnie osowiałych pacyfikatorach. Więcej badała niż leczyła tym razem.
* Marcelin Viirdus: daleki advancer z wyjątkową antypatią do kotów; doszedł do stanu kapsuły ratunkowej uratowanej z Kantali Ravis. Uniknął wszystkich encounterów.
* Lena Morazik: psychotroniczka i negocjatorka, która zajrzała w głąb TAI Ikarii Dotkniętej przez Nihilusa i dostała anomalię memetyczną. Walczyła z tym; była nawet przydatna - wyciągnęła prawdę od Raleny, było lepiej - ale Nihilus ją pokonał. Igor musiał ją wyrzucić za śluzę, nic już nie czuła. KIA.
* Katrina Kirten: kapitan statku ratunkowego 'Lataravia'; wbrew sobie, zrobiła kwarantannę dla Away Team na Kantali Ravis, bo zasada prosta - energia Interis i ANomalia Nihilusa. Pomogła jak była w stanie, czyli dała wahadłowiec na stracenie.
* Ralena Annitas: lekarz na 'Kantali Ravis', Dotknięta przez Nihilusa; zobaczyła, że Arnold nie jest sobą a jest efemerydą. Dotyk Nihilusa sprawił, że uciekła w szaleństwo. 'Będzie dobrze'. Skończyła na wahadłowcu z Ralfem i Arnoldem, KIA.
* Ralf Tapszecz: na imię ma ARNOLD. Jedyny ocalały ze swojego statku gdzie była Anomalia Nihilusa, uratowany przez 'Kantalę Ravis'; stworzył magią manifestację swego ojca mocą Nihilusa (ojciec RALF jest jego bohaterem). 'Ojciec' - manifestacja Nihilusa - zatruwała Kantalę Ravis, ale ten jego aspekt w któego wierzył 'Ralf' próbowała go zabić (i przez to chronić jego i Kantalę Ravis). Polubił się z Kiranem z Kantali; Igor wpakował Ralfa i Arnolda do wahadłowca, który potem został wysadzony. KIA, ale Nihilus go przywrócił na Neikatis, z mniejszą dozą pamięci. Był ogniście lojalny rodzinie.
* Arnold Tapszecz: Stanowczy i heroiczny, opoka i strażnik. Mało mówi, o wszystkich dba. Świetny w walce, medyk itp. "Pierwszy bohater syna". Efemeryda Nihilusa. Prawdziwe imię - RALF. 
* Kiran Ravis: młodszy inżynier na 'Kantali Ravis' i syn kapitana; powitał Zespół i dał im kody kontrolne i potrzebne dostępy. Polubił się z Ralfem.
* SCN 'Lataravia': neikatiańska jednostka ratunkowa; nie jest szczególnie dobrze wyposażona ani duża, ale ma Lancery i away team.
* JRN Kantala Ravis: savarańska rodzinna jednostka noktiańska; zainfekowana przez anomalię memetyczną Nihilusa po tym, jak uratowała Ralfa.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis, orbita

## Czas

* Opóźnienie: -226
* Dni: 1
