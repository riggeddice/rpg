## Metadane

* title: "Furia poluje na Furie"
* threads: pronoktianska-mafia-kajrata, furia-mataris-agentka-mafii
* motives: manhunt, teren-morderczy, echa-inwazji-noctis, bardzo-grozne-potwory, izolacja-zespolu, misja-survivalowa
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [230723 - Crashlanding Furii Mataris](230723-crashlanding-furii-mataris)

### Chronologiczna

* [230723 - Crashlanding Furii Mataris](230723-crashlanding-furii-mataris)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ?
* Struktura: 
    * explore-and-maneuever
* CEL: 
    * MG
    * Drużynowy

### Co się stało i co wiemy

* .

### Co się stanie (what will happen)

* F1: Konstrukcja sprzętu
    * Wiadomość o Kajrata "przesłuchanie mentalne", "zostawiłem Wam zrzut", "idźcie na Pacyfikę"
    * Konieczność zyskania zasobów - o czym pomyślałby Kajrat?
        * gniazdo jaszczuroskrzydeł
    * IMPULS: pokiereszowane idziecie dalej
* F2: Enklawa Lotosu
    * Potwory zapędzają tam Furie
    * Jest tam Isaura, która się schowała przed przeciwnikami
    * IMPULS: zostańcie, tu będzie Wam lepiej
* F3: Polowanie Czarnych Czaszek na Furie
    * Xavera ich zauważy, mimo że są niewidoczni
    * Niedaleko jest Miasteczko
* F4: Rozdzielone
    * Legion Pacyfiki
    * Trzy awiany i mag, próba dominacji i przejęcia kontroli nad Amandą
    * Ernest Kajrat i sześciu noktian

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Xavera Sirtas
    * OCEAN: (C+A-): żywy ogień, jej dusza jest pełna pasji, a jej słowa ostrzem. "Kto siedzi cicho, tego spotyka wszystko co najgorsze"
    * VALS: (Security, Tradition): wszystko odnosi do tego co już widziała i co było, działa dość tradycyjnie. "Wszystko już było i się stało"
    * Core Wound - Lie: "Przeze to, że dużo mówiłam i szperałam, moi rodzice się rozwiedli" - "Będę walczyć i ognistą walką udowodnię, że warto być razem!"
    * Styl: Zawsze gotowa do walki. Agresywna i asertywna. Dobrze wybiera walki. Jej upór często wpędza ją w kłopoty.
* Ayna Marialin
    * OCEAN: (N+O+): wiruje kalejdoskopem pomysłów, które stale ją inspirują i napędzają. Wiecznie martwi się jutrem i planuje. "WIEM, że nie ma niedźwiedzi. A jeśli są?"
    * VALS: (Family, Universalism): pozytywny wpływ przy pomocy Zespołu uratuje wszystko. "Jesteśmy w stanie kształtować naszą przyszłość. Ostrożnie."
    * Core Wound - Lie: "Nie jestem tak dobra w niczym jak moje przyjaciółki; przeze mnie coś im się stanie" - "Dojdę do tego, rozwiążę to! Nie zaskoczą nas niczym!"
    * Styl: Staranna i metodyczna, Ayna zawsze analizuje sytuację zanim podejmie działanie.
* Isaura Velaska
    * OCEAN: (E+A+): uspokajający głos w zespole, zawsze znajduje chwile na uśmiech i dowcip. "Nieważne co się dzieje, trzymaj fason i się uśmiechaj"
    * VALS: (Harmony, Benevolence): "Zespół to jak rodzina. Musimy być dla siebie wsparciem."
    * Core Wound - Lie: "Rodzina mnie porzuciła, ale Furie adoptowały" - "Jeśli zrobię dla Was rodzinę, wszyscy zawsze będziemy razem bez kłótni i sporów"
    * Styl: Ostoja spokoju wśród chaosu, rozładowuje napięcia i tworzy poczucie wspólnoty i jedności

### Scena Zero - impl

.

### Sesja Właściwa - impl

Wiadomość od Kajrata:

* "Furie, przeciwko Wam są magowie mentalni. To znaczy, że jeśli ktoraś z Was jest złapana, wszystko co wie należy do Przeciwnika i będzie współpracować z przeciwnikiem jakby to był oficer Noctis."
* "Dlatego nie mogę Wam powiedzieć gdzie zostawiam Wam sprzęt ani gdzie Was przechwycę. Wasze kody należą do Przeciwnika."
* "Zrzuciłem Wam 'stashes' z ciepłym ubraniem i sprzętem. Tyle mogłem. Idźcie na Pacyfikę, przechwycę Was."

.

* Ayna: "Magowie nie istnieją. To jakieś narkotyki."
* Amanda: "Czy to ma znaczenie?"
* Ayna: "Nie, nie ma..."
* Xavera: "Ma to o tyle znaczenie, że Kajrat może być niegodny zaufania bo wierzy w magię"
* Ayna: "Pomógł nam już."
* Amanda: "Mógł użyć tego jako skrótu. Mógł coś przekazać w ten sposób. Lub mówić prawdę. Jest taka opcja, nie wiemy."
* Ayna: "Wyłączyłam transpondery w servarach"
* Amanda: "Słusznie, nie pomoże mu to ale nie pomoże też przeciwnikowi."

Statrep: 

* mało jedzenia, wody, zapasów
* bardzo niesprawne servary poza zakresem normalnego działania (jeszcze asset. JESZCZE. Ale mówimy o godzinach pracy.)
* sprzęt ciężki, servarowy a nie lekki
* ubrania to leciutkie kombinezony wewnątrzservarowe a nie dobre stroje

Cel: znaleźć 'stash', rozeznać się i iść w stronę Pacyfiki...

W najbliższej okolicy są:

* 10 km oddaliliśmy się od tego statku kosmicznego
* Teren jest raczej równinny z ruinami
* Są ślady nieuzbrojonych lub nieopancerzonych ludzi; najpewniej to zmienieni lokalsi
* Parę kilometrów stąd jest miasteczko, które jest zniszczone (zdaniem Ayny, jest super niebezpieczne bo tam KTOŚ jest)
* wykorzystując detektory servarów, Ayna znalazła obszar gdzie nikt się nie zbliża - _tangle_ krzewów i mechanizmów, gdzie znajdują się... latające jaszczury. Sporo.
    * i tam tak samo jest na ziemi niedaleko zużyta świeża noktiańska flara, dwie. Już nie są ciepłe. Patrząc na ziemię, ktoś rzucił dwie flary jak już były zimne.

Ayna próbuje zinterpretować wyniki naciskając servara na maksimum psychotronicznie i analitycznie, przekierowując energię.

Tr +3:

* X (p): Ayna przeciąża servar. Energia przekierowana
* V (p): Ayna dostrzegła ślady noktiańskiego servara który ma pattern _stealth_ które prowadzą zarówno DO ŚRODKA _tangle_ jak i ZE ŚRODKA. Ślady wskazują na obciążenie gdy idzie 'do'.
* X (m): Servar Ayny umiera
* X (e): Servar Ayny padł do poziomu 'uwolnienie pilota' się rozsypało. Aynę trzeba WYDOBYĆ. To potrwa jakąś godzinę.

.

* Xavera: Wydobądź Aynę, ja pójdę po zasoby
* Amanda: Jak się rozdzielimy, mogę ufać że wrócisz TY?
* Xavera: Oki, masz rację. Ale jeśli tam jest coś groźnego, Ayna będzie poza servarem to jest delikatna.
* Amanda: Jak stracę Cię z oczu, nie wiem że Ty to Ty.
* Xavera: Zostawmy Aynę, pójdziemy razem póki servary mają energię. Aynie nic nie będzie.
* Ayna: To dobry plan (cicho) (jest w ciemności, jest unieruchomiona... jest jak tuńczyk w konserwie)
* Xavera (po chwili): Ayna, Twój _life support_ działa?
* Ayna: ...chyba nie.
* Xavera: Albo zdejmiemy jej hełm, ale wtedy jest jak w grobowcu.
* Amanda: We dwie szybciej ją wydobędziemy z tego...
* Ayna: ...ale _jak_ to się dzieje, że mechanizmy servara zawiodły aż tak? Mechanizm defensywny... aha. No tak, mamy konfigurację próżniową...

Furie w dygoczących, nieprecyzyjnych servarach podpinają źródło energii do servara Ayny by odblokować zatrzaski i doprowadzić do awaryjnego wyłączenia servara.

(+cierpliwość, specyfikacja, -dygoczące servary)

Tr +3:

* V: Udało się to zrobić. Zajęło jakieś 30 minut, ale udało się uwolnić Aynę - a wpierw zdjęły jej hełm by mogła oddychać.

.

* Ayna: "Nie lubię powietrza na tej planecie..."
* Amanda: "Lepsze to niż żadne"
* Xavera: "Mój lifesupport przestał właśnie działać."
* Amanda: "Zrzuć hełm"
* Xavera: (zdehermetyzowała)

Amanda idzie śladami servara, Ayna między nią i Xaverą. Servar Amandy wyświetlił info o minach. Miny, które servar Amandy widzi, dostrojone na _frequency_ Furii. Miny są w dobrym patternie, jakieś 2-3. Gorzej, że latają jaszczury (jaszczury z ogonami skorpionowatymi).

* Xavera: "Mogę spróbować przejąć kontrolę nad minami i przejąć je dla nas"
* Amanda: "W sensie?"
* Xavera: "Chcesz trzy miny jako awaryjną broń? Bo mogę spróbować je pozyskać."
* Ayna: "Niebezpieczne"
* Amanda: "Potencjalnie przydatne, ale chcemy na odejściu a nie na wejściu"
* Xavera: "Tak, ale jak to gniazdo odpali to będziemy uciekać"
* Amanda: "W zależności od stasha możemy nie być w stanie ich zabrać. Masz rację i podoba mi się idea..."
* Xavera: (patrząc na Aynę) "Masz rację. Chodźmy szybciej." (priv -> Amanda: "Ona marznie")

Niestety, im bliżej podchodzą trzeszczącymi servarami tym większe zainteresowanie jest latających jaszczurów.

* Amanda: "Pójdę sama"
* Xavera: "Mamy ledwo sprawne komunikatory, jeśli Twój servar się wyłączy w połowie... (chwilę myśli) Ayna, możesz przetransferować trochę energii ode mnie do niej?"
* Amanda: "W razie czego by się opędzić"
* Xavera: "Wiem. Ale jeśli Ci padnie w połowie, jest po Tobie. Gdyby Ayna była sama..."
* Ayna: "Dobrze, przemontuję Twój servar na default-eject."
* Xavera: "Amandy nie. Jak jej się wyłączy, podpalimy to gniazdo i ją wyjmiemy."
* Ayna: "..."

Tr Z +3:

* V: Ayna przetransferowała energię od Xavery do Amandy (kompensacja pierwszego X; da to opcję wyjęcia stasha)

Amanda idzie po stash... teren jest paskudny. Śliski, krzewy żyją i się ruszają, tak samo mechanizmy. Amanda widzi anomalny teren. A nad nią latają jaszczuroskrzydła, które ją próbują przewrócić i znaleźć lepsze otwarcie. Servar wysyła proximity alerty cały czas PLUS próbuje stabilizować na rozchwianiu. Teren jest śliski i podejrzany, więc Amanda się ślizga i próbuje 'kontrolowanie spadać'. Cztery punkty podparcia itp. Ile trzeba.

Tr Z +4:

* X: pnącza pooplątywały Amandę, ale ona ma ekstra zapasy energii i się wyrywa. Servar protestuje, ale Amanda ma to gdzieś. Za to Amanda widzi 'zabite' fragmenty struktury - ktoś zabił je servarem w odpowiednich miejscach. Ktoś ułatwił wejście. Amanda korzysta z tych punktów jako podparcia.
* X (p): Amandzie zajmuje to dużo, dużo więcej czasu niż miała nadzieję. Servar ulegnie dodatkowym uszkodzeniom.
* X (m): Amanda przebija się przez to wszystko, spadła dość nisko, uszkadzając nogę servara. Jest w _crawling mode_. Niestety, to ostatnia operacja tej maszyny.
* V (p): Amanda, z bardzo uszkodzonym już servarem dopełzła do stasha. Stash jest przygotowany do 'założenia', ale bez servara tego nie wyprowadzi. Stash jest zasealowany. Jaszczury próbują dziobać i przebić się przez pancerz, ale nieskutecznie. Servar trzyma. Amanda złapała jakiegoś i odrzuciła 'na mięso'. One mają typowe zachowania 'bronić gniazda'.
* Vz (m): Servar dostał jeszcze kilka uszkodzeń, zdzierając pnączometal, pękła szybka w hełmie (jeszcze jest hermetyczna), TAI prawie zgasła, ale Amanda wypełzła na krawędź, widzi prześwit. I ma dupną paczkę na plecach. SHE CRAWLS!
* X (e): Niestety, servar się zablokował. Nic się nie poradzi. Xavera musiała przyjść swoim servarem, użyć dosłownego ognia by odganiać jaszczury. Zdjęła z Amandy paczkę i wróciła, po czym wróciła po Amandę. Amanda nadal ma kontrolę nad servarem, po prostu ma zniszczone servo w prawej ręce, prawej nodze, lewej dłoni i parę innych. 

Wróciły do swojej jaskini. Amanda musiała niestety ejectować servar. Xavera jest jedyną, acz ma uszkodzoną lewą rękę. Przeciążyła ją. I nie ma hermetyzacji hełmu. Za to udało im się wejść w zasoby stasha.

* Zbiór lekkich pancerzy dla różnych _bodytypes_. Też ubrania.
* Baterie dla servara. Ayna spróbuje naładować Xaverę.
* Zbiór broni, sprzętu, kompasy, mapy itp.
    * Mapy mają komentarze.
        * Niedalekie miasteczko jest oznaczone jako 'salvage możliwy, ale niebezpieczne'. 
        * Statek kosmiczny który minęły oznaczony jako 'bio/mental hazard'
        * Jest oznaczona Enklawa na mapie jako 'potencjalne miejsce ostatniej szansy, są dziwni, potencjalnie niebezpieczni, pomocni'.
        * Notatka: 'Enklawa Idralias, pomogą za cenę. Enklawa Kwietników ("ta dziwna"), pomogą za darmo - nic nie jedz. Czarne Czaszki - kill on sight, to lokalne predatory ALE ludzie. Uwaga na pogodę.'
    * Mapa ma główne obszary:
        * Lokalne potwory, też groźne są oznaczone osobno dla servarów i bez
        * Dużo niebezpiecznych ścieżek oznaczonych na mapie, też podziemnych - z zaznaczeniem że servar tam może nie wejść
            * Lokalizacje potencjalnych ukryć
* Żywność, woda, filtratory, detektory...

Xavera ładuje swój servar; Ayna próbuje doprowadzić go do jakiegokolwiek działania. Kanibalizacja servara Amandy oraz Ayny może pomóc. Ayna nie ma fabrykatora, ale może coś zrobić. Niestety, te servary uległy pewnej degradacji - nie są zaprojektowane do działania długoterminowego w takich warunkach.

Tr Z +3 +3Og:

* X (p): Bardzo wolna operacja. To plus ogień dają mi wprowadzenie frakcji.
* V (p): Xavera może opuścić servar, ale mają 'tragarza + detekcję + dywersję'. I sprawiają wrażenie czwórki.
* X (p): Xavera jest poza servarem, jest przezbrojona, Furie nie marzną, mają servar ale remote.

Furie nie wyglądają jak Furie (co też było celem Kajrata). Ayna odpaliła 'sygnał' w stashu, żeby Kajrat wiedział że TEN stash został odpalony. Furie chcą się wycofać; niestety, zobaczyły dwa awiany lecące w stronę jaskini.

* Xavera: "Chcę jeden z tych awianów."
* Amanda: "Swobodniejsze poruszanie się"
* Xavera: "Zostawiamy servar bez hełmu, stoi jak porzucony. On działa. On zaatakuje i mamy dywersję."
* Ayna: "Ci ludzie w awianach... te awiany zachowują się bardziej kompetentnie."
* Amanda: "Wojsko potencjalnie?"
* Ayna: "Popatrz na ruch, nie da się łatwo przycelować, monitorują... na pewno wykryli już servar. Ale nas nie mają jak. Czyli widzą JEDNĄ Furię."

Czyli jest plan - zostawić servara tutaj jako potencjalną pułapkę i przynętę (dywersję), niech zwiąże ich walką. W tym czasie Furie używając niefajnych tuneli opuszczą jaskinię. Spróbują zajść jednego awiana i go zająć a drugiego zestrzelić.

Ayna i Xavera szybko robią odpowiednią symulację ruchów servara by ściągnąć przeciwnika.

Tr Z +4:

* Vz (p): przeciwnik to łyknie; uważa, że tam jest Xavera.
* X (p) X (m): po drugiej stronie jest Furia, która wie jakie plany ma Xavera. I może ją złapać żywcem. (+3Vg)
* V (m): Furie są w stanie uniknąć znalezienia i starcia.

Amanda wycofuje się do jednego z tuneli i prowadzi przez niego pozostałe Furie. Co prawda Kajrat ostrzegł przed tunelami, ale nie mają wyjścia. Amanda SPECJALNIE wybrała tunel, który wygląda na to, że prowadzi na zewnątrz, w stronę awianów a nie - zgodnie z wyszkoleniem Furii - na ambush. Wycofując się Amanda zauważyła, że w servar poleciał kierowany pocisk 'klejowy'. Servar Furii nie jest dość mocny by to szybko rozerwać. Na komunikatorach 'Sprytne, Xavero, ale nie dość dobre'. To Ralena. Furia dowodzi operacją. Ayna jest w szoku i Xavera jej zadała ból; wyszkolenie wygrało.

* Vg: 
    * Xavera, zimno (i cicho) "Dobra, wierzę temu Kajratowi, Ralena by nas nie zdradziła"
    * Amanda: "nieważne czym to osiągnęli, mamy możliwości. Awian."
    * Xavera: "szybko, pełzniemy do cholernego awiana. Dobrze, że rozstawiłyśmy _typowe_ pułapki"
    * CEL: zdobyć awiana. I to jest możliwe.

Ralena wyraźnie działa jak Furia - co więcej, jak Furia rozumiejąca Xaverę. Z 20 Furii znamy los 5 - Xavery, Ayny, Amandy, Raleny i Lucii. Amanda pokręciła głową - Lucia jest stracona. Tego się nie spodziewała i tego dla niej nie chciała. I nie miała wyboru. Zdaniem Amandy, może być więcej Furii niż tylko Ralena...

* Xavera, cicho: "Spróbuję jej nie zabić, może Kajrat jej może pomóc..."

Tunele są wąskie, paskudne, rodem z horroru, korzenie próbujące wślizgnąć się pod ubranie i ssać krew. Trzeba momentami pełzać i poruszać się po fatalnym terenie. Amanda jest PEWNA, że są zakażone jakimś cholerstwem. Ale nie ma wyjścia.

Ex Z +3 +3Or:

* X: Furie są lekko pocięte, poranione, possane krwią i przez to - zakażone.
* Vr (p): Furie przejdą przez ten przeklęty labirynt korzeni i tuneli (+2Vg bo dziewczyny TEŻ znają Furie)
* X: Furie tracą wiekszość non-essential equipment. Nie przejdą z tym, nie w tym wypadku.
    * Ayna uruchamia sygnatury pozostałych servarów, by Ralenę wyprowadzić z równowagi i rozbić przeciwników trochę (+1Vg)
* Vg (m): Furie wypełzły jakimś uszkodzoną szczeliną ze wzgórza; jeden awian w powietrzu, drugi na ziemi. Żaden ich jeszcze nie zauważył. A Amanda ma snajperkę.
    * Xavera: "Amando, Ty celuj w to u góry, ja się zakradam, Ayno - osłaniaj. Jeśli oni zadziałają, STRZELAJ!"
    * (sekunda potem) Xavera: "servar pilnuje awiana..."
        * Xavera wykorzysta jedyny granat jaki ma, by uszkodzić servar. A Amanda zestrzeli awiana, uszkodzi go (ma servarową snajperkę). Ayna kryje... (+1Vr +2Or)
            * Ayna: "mamy jedną minę. Jedną"
            * Amanda: "skąd?"
            * Ayna: "jaszczury. Zabrałam jedną, Xavera osłaniała. Zajęło Ci chwilę (info)"
            * Ayna: "spowolnię Ralenę zrzucając aktywną minę w wejście do jaskini"
* X: Furie zostały zauważone gdy się rozdzieliły na powierzchni. Przeciwnicy NIE SĄ zaskoczeni.
* X (p): Zarówno Xavera jak i Ayna zostały poranione ogniem; pancerze przetrwały i uratowały im życie, ale są ranne.
* X: Ayna skutecznie zawaliła jaskinię, unieszkodliwiając osoby w środku. Ale Ralena zdążyła opuścić jaskinię i pojmać Aynę.
* V: Amanda snajperką ppanc trafiła w awiana w powietrzu; ten się zdestabilizował i zaczyna crashlandować.
* Vr: Xavera, ranna, wykorzystała granat by ŁUPNĄĆ we wrogiego servara. Wybuch go uszkodził ale nie zniszczył. Xavera zignorowała servar by dostać się do awiana i zastrzeliła pilota.
    * Następnie obróciła awiana by przewrócić servar i ruszyła w stronę Amandy. (+3Vg)
        * Xavera: "Amando, łap się!"
* Vg: Xavera, jak to ona, wbiła się awianem. Ralena uskoczyła, Xavera przechwyciła Aynę (jeśli nie porwaną - bardzo ciężko ranną).
* X: Ralena dała radę trafić w awiana. Awian doleci kilkanaście kilometrów w stronę na Pacyfikę i się rozbije, dowodzony przez Xaverę.
* Vg: ranna Xavera, ciężko ranna Ayna, zatruta Amanda wylądowały DALEJ. Bliżej Pacyfiki.

Xavera rozbiła je na terenie 'powojennym', w okolicy zrujnowanego miasteczka (innego). "Xav: Amando, tu łatwiej się schowamy..." "Ama: ma sens". Niedaleko jest zarówno Enklawa Idralias jak i Enklawa Kwietników. Xavera, rozmawiając z Amandą, kieruje awiana w stronę Enklawy Kwietników. Tam jest potencjalnie bezpieczniej. Może tam Kajrat je znajdzie...

Awian jest rozbity. Potrzebne są pasy, sanie i droga do Enklawy Kwietników... przynajmniej tyle, że to na pewno dało się dojrzeć. Kajrat halp pls?

## Streszczenie

Servary przestają działać. Amanda pozyskała stash od Kajrata z gniazda latających jaszczurów, acz kosztem swojego servara. Jedyny sprawny - Xavery - został zmodowany jako jednostka transportowa. Niestety, podpalenie gniazda jaszczurów sprawiło, że Ralena - kontrolowana przez KOGOŚ Furia - zaatakowała siłą 8 servarów i 2 awianów. Zespół dał radę zdjąć 1 awiana a drugiego porwać i jakkolwiek wszystkie Furie są zatrute, Xavera ranna, Ayna bardzo ciężko ranna, ale mają kilkanaście kilometrów przewagi nad atakującymi. Furie dzielnie się bronią, ale zaczynają przegrywać...

## Progresja

* Amanda Kajrat: lekko ranna i zatruta przez straszne korzenie w okolicach Enklaw; ma personal armour i snajperkę ppanc
* Xavera Sirtas: ranna acz funkcjonalna i zatruta przez straszne korzenie w okolicach Enklaw; ma personal armour i sprzęt brawlera
* Ayna Marialin: ciężko ranna (nie może chodzić) i zatruta przez straszne korzenie w okolicach Enklaw; ma personal armour i nie ma sprzętu

## Zasługi

* Amanda Kajrat: weszła servarem w gniazdo jaszczuroskrzydeł by zdobyć _stash_ od Kajrata; opracowała sposób jak uciec od sił Wolnego Uśmiechu i go przeprowadziła. Snajperką zestrzeliła wrogiego awiana i ewakuowała się z Xaverą i Ayną do nowego miejsca. W najlepszej formie z Trzech Furii.
* Xavera Sirtas: najlepsza w walce z trzech Furii; wyciągnęła Amandę z gniazda jaszczuroskrzydeł podpalając je, oddała Amandzie część energii i granatem uszkodziła servar wroga. Gdy Aynie groziło porwanie, zderzyła awianem w porywającą Aynę Ralenę by przechwycić Aynę i wiać
* Ayna Marialin: poświęciła swój servar by wzmocnić detekcję i znaleźć _stash_ Kajrata. Przekierowała energię z servara Xavery do Amandy. Zawaliła jaskinię, gdy Ralena próbowała je pokonać, ale skończyła bardzo ciężko ranna i prawie porwana przez Ralenę.
* Ernest Kajrat: ostrzegł wszystkie Furie o magii mentalnej i porozkładał _stashe_ pomagające Furiom które przetrwały. Monitoruje teren by przechwycić Furie.
* Ralena Karimin: Furia, która została złapana przez siły Grzymościa i przejęta magią mentalną. Przeprowadziła operację złapania Xavery i mimo, że walczyła z TRZEMA Furiami (Amanda, Ayna, Xavera), prawie złapała Aynę. Gdyby chciała zabić a nie złapać, byłaby w stanie ze swoim oddziałem Wolnego Uśmiechu zniszczyć wszystkie trzy Furie

## Frakcje

* Nocne Niebo: rozłożyło _stashe_ dla Furii by były w stanie jak najdłużej przetrwać aż Kariat do nich się dostanie i uratuje
* Wolny Uśmiech: wykorzystuje Ralenę (Furię), by złapać pozostałe Furie. Stracił dwa awiany.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, SW
                    1. Granica Anomalii

## Czas

* Opóźnienie: 0
* Dni: 1

## Specjalne

* .

## OTHER
### Okolice terenu Pacyfiki

* Typ terenu: 
    * https://baronphotography.eu/najpiekniejsze-poloniny-w-bieszczadach/   (1000-1500 m npm)
    * Puna (półpustynia wyżynna) https://pl.wikipedia.org/wiki/Puna_(biogeografia)
    * https://pl.wikipedia.org/wiki/Puna_(biogeografia)#/media/Plik:Peru_-_Altiplano1.jpg

#### 1. Lokalna potworyzowana fauna

* **Koncept wysokopoziomowy**
    * Lokalna niebezpieczna fauna i drapieżniki
* **Agenda** 
    * **Głód**: "Nie jesteś na swoim terenie. Zostaniesz zjedzony i zasilisz ekosystem swoją biomasą."
    * Przypomnij graczom, że są intruzami w tym środowisku i że muszą szanować jego mieszkańców.
    * Zagrażaj graczom poprzez niekontrolowane reakcje zwierząt.
* **Aktywacja**
    * Wyzwalacz: Ruch aktywuje dziką zwierzynę i lokalne drapieżniki.
    * Konflikt: Gracze muszą poruszać się ostrożnie, aby nie wzbudzić ich gniewu, a jednocześnie szybko, aby nie zostać złapani.
* **Zasoby i Aspekty**
    * "_zakłócone zwierzęta_" - miejscowe zwierzęta są niespokojne i mogą zaatakować w każdej chwili.
    * "_ślady zwierząt_" - można je używać do nawigacji, ale mogą prowadzić do niebezpiecznych miejsc.
    * "_zbyt groźny potwór_" - coś, co jest potencjalnym super-hazardem lub maskowaniem.
* **Pokusy i Typowe Porażki** 
    * Obecność groźnego potwora, którego można na coś napuścić
    * Możliwość schronienia się w miejscu wyczyszczonym przez potwora
    * Zbocze góry, po którym gracze próbują przejść, okazuje się miejscem gniazdowania ptaków drapieżnych, które zaatakują, aby chronić swoje młode.
    * Potencjalna żywność

#### 2. Zmienne Warunki Pogodowe

* **Koncept wysokopoziomowy**
    * Nagłe zmiany warunków pogodowych, takie jak burze, mgły, śnieżyce, mogą stanowić duże wyzwanie dla podróżujących
* **Agenda** 
    * **Głód**: "Na tym terenie nie jesteś u siebie. Zmiażdżę Cię pogodą."
    * Wykorzystaj warunki pogodowe do tworzenia nieprzewidywalnych i trudnych sytuacji.
    * Przedstaw graczy trudnymi decyzjami związanymi z pogodą, takimi jak znalezienie schronienia czy kontynuowanie podróży pomimo niebezpieczeństw.
* **Aktywacja**
    * Wyzwalacz: Nagłe zmiany warunków pogodowych wymuszają zmiany w planach graczy i powodują zagrożenia.
    * Konflikt: Gracze muszą zdecydować, czy kontynuować mimo ryzyka, czy szukać schronienia i czekać na lepsze warunki.
* **Zasoby i Aspekty**
    * "_nagła burza_" - niespodziewana burza może zmusić graczy do szukania schronienia lub ryzykowania w burzy.
    * "_gęsta mgła_" - mgła utrudnia widoczność i orientację, co może prowadzić do zgubienia drogi.
    * "_mordercza ulewa_" - usuwa ślady, usuwa widoczność
* **Pokusy i Typowe Porażki** 
    * W gęstej mgle gracz może zgubić drogę i oddalić się od reszty grupy.
    * Nagła burza może spowodować, że gracze staną się mokrzy i zmarznięci, co zwiększa ryzyko hipotermii.
    * Gracz decyduje się kontynuować mimo złych warunków pogodowych, ale z powodu ograniczonej widoczności wpada na niebezpieczną zwierzęcą drapieżnika lub wpada w pułapkę mafii.
    * Gracze znajdują schronienie, ale okazuje się, że jest już zajęte przez nieprzyjazne stworzenie lub gang.

#### 3. Niebezpieczne Ścieżki

* **Koncept wysokopoziomowy**
    * Ścieżki są pełne pułapek i niebezpieczeństw, zarówno naturalnych, jak i tych stworzonych przez ludzi.
* **Agenda** 
    * **Głód**: "Nieznane tereny kryją niezliczone zagrożenia. Każdy krok może być twoim ostatnim."
    * Pokaż niebezpieczeństwo nieznanego i niebezpieczeństwa płynące z poruszania się po niewygodnym terenie.
    * Stwórz sytuacje, w których gracze muszą zdecydować, czy ryzykować, idąc nieznaną ścieżką, czy szukać innej drogi.
* **Aktywacja**
    * Wyzwalacz: Gracze znajdują ścieżkę, która wydaje się prowadzić w kierunku ich celu, ale jest pełna niebezpieczeństw.
    * Konflikt: Gracze muszą zdecydować, czy ryzykować, idąc niebezpieczną ścieżką, czy szukać bezpieczniejszej drogi, która może być dłuższa lub prowadzić w nieznane.
* **Zasoby i Aspekty**
    * "_ścieżka pełna pułapek_" - niebezpieczne pułapki mogą zagrażać bezpieczeństwu graczy.
    * "_ścieżka przez urwisko_" - jedyne droga prowadzi przez stromy klif, który zagraża upadkiem.
    * "_zdradliwe podłoże_" - ścieżka jest pełna kamieni, które mogą spowodować potknięcia lub zwichnięcia.
    * "_błotne osuwisko_" - podłoże się rwie i jest kłopot
* **Pokusy i Typowe Porażki** 
    * Gracz potyka się o kamień i zrzuca cenny ekwipunek w przepaść.
    * Gracz stawia stopę na fałszywym kamieniu i uruchamia pułapkę.
    * Gracz próbuje ominąć niebezpieczną ścieżkę i wpada na mafię.
    * Gracz decyduje się kontynuować, mimo widocznych niebezpieczeństw, ale wpada w pułapkę lub stacza się ze ścieżki.

#### 4. Gniazdo jaszczuroskrzydeł

* **Koncept wysokopoziomowy**
    * W różnych szczelinach i zagłębieniach znajdują się jaszczuroskrzydła, zdolne do uszkodzenia wszystkiego co nie jest servarem
* **Agenda**
    * **Głód**: "Zasilić swoje potomstwo. Osłonić swój teren"
    * Przypomnij graczom, że są na morderczym terenie i zasoby są ograniczone.
    * Spraw, aby graczom zależało na unikaniu konfliktów z tymi przerażającymi istotami.
* **Aktywacja**
    * Wyzwalacz: Gracze zbliżają się zbyt blisko do gniazda, drażniąc jaszuroskrzydła i zmuszając je do obrony swojego terytorium.
    * Konflikt: Gracze muszą wybrnąć z sytuacji bez rozpoczęcia bitwy z ptakami lub bez utraty cennych zasobów.
* **Zasoby i Aspekty**
    * "_głosy na wietrze_" - huki i krzyki pterodaktyli echem niosą się przez wąwozy i doliny, ostrzegając o ich obecności.
    * "_ciało wroga_" - zwłoki lub resztki niedawnej ofiary mogą dostarczyć cennych zasobów, ale ryzyko jest ogromne.
    * "_teren do lotu_" - jaszczuroskrzydła korzystają z przewagi powietrznej, co czyni ich niezwykle niebezpiecznymi przeciwnikami.
* **Pokusy i Typowe Porażki**
    * Strażnik KOGOŚ zauważa.
    * KTOŚ zostaje zaatakowany przez jaszczuroskrzydło i musi stawić czoła tej groźnej istocie.
    * KTOŚ natyka się na pisklę jaszczuroskrzydła i musi zdecydować, czy zabrać je ze sobą, czy zostawić je na pastwę losu.

#### 5. Zrujnowane Miasteczko 

* **Koncept wysokopoziomowy**
    * Kiedyś tętniące życiem miasteczko, teraz puste i opuszczone z powodu katastrofy.
* **Agenda** 
    * **Głód**: "Jeśli wejdziesz, zostaniesz tu. Jeśli nie, nie oddam Ci moich skarbów"
    * Spraw, aby gracze czuli ciężar przeszłości i nadzieję, że może coś się da zdobyć - dużo ryzykując.
    * Daj im możliwość pozyskania czegoś cennego, ale niemałym kosztem
* **Aktywacja**
    * Konflikt: Gracze muszą poradzić sobie z terenem i mieszkańcami, próbując zarazem znaleźć coś wartościowego w ruinach miasteczka.
* **Zasoby i Aspekty**
    * "_widmo przeszłości_" - popękane ulice, zniszczone budynki i porzucone przedmioty codziennego użytku przypominają o lepszych czasach.
    * "_siedliska mutantów_" - skrzywione budynki i podziemne kanały stanowią doskonałe siedliska dla mutantów.
    * "_toxic waste_" - skażenie z powodu którego miasto zostało opuszczone jest nadal obecne, stwarzając dodatkowe zagrożenie.
    * "_zachowane skarby_" - choć większość miasteczka jest w ruinie, mogą tam być jeszcze jakieś wartościowe przedmioty.
* **Pokusy i Typowe Porażki** 
    * Przypadkowe natknięcie się na mutantów.
    * Wybuch gazu lub innej zgromadzonej substancji.
    * Próbując dostać się do schronu, gracz aktywuje starą, jeszcze działającą obronną pułapkę.
    * Gracz znajduje starą, zapomnianą przez czas mapę miasteczka, która może prowadzić do cennych zasobów.
    * Gracze natykają się na ślady innej grupy ocalałych, co może prowadzić do konfliktu lub ewentualnej współpracy.
    * Odnalezienie skarbu, który może przynieść wiele korzyści, ale także przyciągnąć niechciane uwagi.
    * Spotkanie z mutantem, który jest mniej wrogi i chętny do wymiany informacji za zasoby.
    * Gracz doznaje choroby z promieniowania lub skażenia toksycznymi odpadami.
