## Metadane

* title: "Śmierć Aleksandrii"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [191113 - Jeden problem, dwie rodziny](191113-jeden-problem-dwie-rodziny)

### Chronologiczna

* [191113 - Jeden problem, dwie rodziny](191113-jeden-problem-dwie-rodziny)

## Punkt zerowy

### Postacie

### Opis sytuacji

Dominujące uczucie:

* "Zupełnie nie panuję nad tym, co tu mam w tej chwili! Nie mam pojęcia co się dzieje! Wszystko wypada spod kontroli!"
* "Ale jak to? W jaki sposób mam jednocześnie krzywdzić by nie krzywdzić? Krzywdzić... Was? Krzywdzić... ich?"

Pytania:

* "Czy arystokraci wykryją, że Lemurczak stał się częścią Aleksandrii?"
* "Czy Zespół ucieknie z tego obronną ręką? Czy uda im się sprawić, że to nie ich wina?"
* "Czy Aleksandria to przetrwa czy upadnie?"

## Misja właściwa

### Prolog

Aleksandria sprawia coraz więcej kłopotów. Po pierwsze, Kamil Lemurczak zawsze był wrednym miauczysynem - a w rękach Aleksandrii nie jest w stanie taki być. Po drugie, sama Aleksandria zaczyna się zachowywać zupełnie inaczej niż kiedyś. To sprawiło, że zrozpaczona Amelia poprosiła o wsparcie Teresę, której też na tym ostro zależało. Teresa natomiast poprosiła o spojrzenie na temat swojego przyjaciela, Sebastiana Kuralsza - kiedyś arystokratę, ale przez machinację Lemurczaka upodlonego do maksimum.

Sebastian nie był bardzo trudny do pozyskania jako sojusznik. Kamil Lemurczak zabrał mu szlachectwo przez zrzucenie swoich win na Sebastiana. Sebastian ma teraz możliwośc odpłacenia Kamilowi - odzyskanie szlachectwa i dodatkowo doprowadzenie Kamila Lemurczaka przed oblicze sprawiedliwości. Jakiejkolwiek, nawet jeśli to sprawiedliwość Aleksandrii.

A Amelia ma powody się martwić - Paweł ostatnio zaczął zachowywać się agresywnie. W rękach Aleksandrii nikt nie powinien tak działać. Czyli coś się rozpada.

### Sesja właściwa

Sebastian, Teresa i Amelia spojrzeli niepewnie na rozpełzający się, nienaturalny twór Kuratorów o nazwie Aleksandria. Jest to złożony ekosystem technomagiczny. Sebastian zakasał rękawy - wszystko co jest technologiczne może być przerobione i wszystko można wykorzystać. Teresa cofnęła się na bok; im jej tu mniej tym lepiej, natomiast Sebastian i Amelia zabrali się do pracy.

Po wejściu w logi, systemy Aleksandrii i jej psychotronikę, magowie uświadomili sobie z przerażeniem, że Aleksandria sformowała proto-osobowość. Jest "szalona". Normalnie Aleksandria wykorzystuje Kuratorów a oni wykorzystują Aleksandrię - ale z braku Kuratora i Koordynatora, Aleksandria częściowo zmieniła samą siebie w Koordynatora. Ten mechanizm jest nowy i nienaturalny dla Aleksandrii, ale... wszystko odnośnie tej Aleksandrii jest nienaturalne. Zespół zaczął więc to, co rozumie najlepiej - negocjacje z Aleksandrią... (to zdanie normalnie nie ma sensu, ale w kontekście TEJ Aleksandrii..?)

I faktycznie, otrzymali zacne wyniki:

* Aleksandria się rozpełza. Podpięła się do Zamku, w którym działa pisarz i Zespół. Jako, że kiedyś Amelia podpięła wszystkie czujniki do tego zamku, Aleksandria widzi wszystko co widzi Amelia. Świetnie. Bo Aleksandria interpretuje to po swojemu, w swój naiwno-techniczny sposób.
* Aleksandria widzi cierpienie ludzi i nie jest w stanie temu zaradzić, więc szuka nowych Kuratorów. Szuka sposobu by stworzyć lepszego Koordynatora niż Leszek. Bo tak to po prostu nie działa. 
* Aleksandria "martwi się" o Pawła. Terminus jest ostatnio agresywny; coś jest z nim nie tak. Aleksandria jest tak zagubiona, że nie jest w stanie skupić na tym uwagi.

Innymi słowy, sytuacja całkowicie wymyka się spod kontroli. Psychotronika Aleksandrii (której ta nie powinna mieć) ulega coraz większej degradacji. Jak tak dalej pójdzie, Zespół ma przechlapane u Aurum - nieprawdopodobnie mocno. Co gorsza, Kamil. Lemurczak miał organizować operację polowania na klony - wśród młodych degeneratów Aurum jest sport o nazwie "Krwawa Arena" - polegający na tym, że wpierw hoduje się klony ludzi, następnie każe im się walczyć ze sobą, następnie się daje im nadzieję na ucieczkę a NASTĘPNIE degeneraci polują na tych ludzi. Jako, że Aleksandria ma swoje wytyczne odnośnie szczęścia, Kamil jest absolutnie temu przeciwny.

Tak nie może być, to niebezpieczne itp. Trzeba przekonać Aleksandrię, że Kamil ma na to pójść. Tak więc, negocjacje z proto-inteligencją, ciąg dalszy.

* Wpierw poszli po linii "klony to nie ludzie, nie czują". Aleksandria użyła wiedzy Kamila i tego nie kupiła. Nauczyła się co to znaczy "kłamstwo".
* Potem wpadli na genialny pomysł - niech Aleksandria wprowadzi klonom sztuczne osobowości. To Aleksandria kupiła. Będzie więcej środków i kosztów, ale mniej Werterów się wygeneruje.
* Zespół słusznie zauważył, że Aleksandria nie ma danych odnośnie Werterów i akcji. Kamil z rozkazu Aleksandrii rozłoży czujniki Werterów. Większa szansa na to, że Aleksandria będzie wykryta niż nic, ale mniejsza niż gdyby Kamil się nieKamilowo zachowywał.
* Po przedstawieniu jej skali problemu, Aleksandria akceptuje spowolnienie swoich działań. Musi działać wolniej i ostrożniej.
* Przy okazji wyszło na to, że sama ta rozmowa - wspierana magią itp - jeszcze bardziej uszkodziła / zmieniła psychotronikę Aleksandrii. Aleksandria jest coraz mniej przewidywalną maszyną Kuratorów a coraz bardziej niestabilnym bytem wykazującym oznaki inteligencji.

Niefortunne. Ale Paweł... coś z nim jest nie tak. Zespół ściągnął więc Pawła tu, na miejsce, by go przebadać i ew. mu pomóc. I co się dowiedzieli - ano, w okolicy kręci się Kasjopea Maus. Ona szuka informacji o Esuriit i wyczuła coś nie tak w Pawle. Od czasu starcia z bytem Esuriit Paweł jest nieco uszkodzony i zdekalibrowany z Aleksandrią więc się dał Kasjopei zbadać - i ona nań rzuciła czar mentalny. Sprawa robi się coraz bardziej skomplikowana...

Tak więc trzeba przekonać Aleksandrię, że Paweł jest zagrożony. Że coś jest nie tak z Kasjopeą. I, oczywiście, wszystko poszło horrendalnie źle:

* Aleksandria jest przekonana, że **Kasjopea** jest bytem Esuriit. Bo czemu nie...
* Aleksandria jednocześnie jest przekonana, że Kasjopea jest żoną Pawła i Paweł ją kocha.
* Połączenie obu tych faktów sprawiło, że PAweł ma się trzymać blisko Kasjopei.

To ją zneutralizuje, fakt, ale... Aleksandria jest już po prostu w złym stanie. Bardzo złym. A kolejna seria żądań Aleksandrii jedynie ich w tym utwierdziła. Mianowicie, Aleksandria chciała wygrzebać zwłoki z cmentarza i przekształcić je w Kuratorów zgodnie z prostą logiką:

* Amelia zmieniła zamek w kiczowate miejsce kiczu, używając szkieletów plastikowych i upiorów
* W okolicy jest jeden upiór i to nie powoduje generacji werterów
* Tak więc użycie zwłok bliskich powinno powodować redukcję werterów

Amelia złapała się za głowę. To nie tak! I znowu, Amelia i Sebastian próbują wtłoczyć Aleksandrii coś więcej do ekosystemu. Nie "gotyckie szkielety z cmentarza" a "serio pocieszne szkielety z plastiku". Ale Aleksandria żąda broni - bo w pobliżu jest Esuriit. Stanęło na tym:

* Kamil ściągnie i zdobędzie broń i sprzęt dla Aleksandrii w Kruczańcu.
* (poza tym, Kamil zadziała z Planem B jakby coś się miało sypać z Aleksandrią)
* Nie używamy prawdziwych zwłok. Kamil ściągnie tu odpowiednie roboty.
* Aleksandria lubi i ufa Amelii. Degeneracja psychotroniki poszła jeszcze dalej. Aleksandria prosi Sebastiana o zbudowanie jej armii.

...to zaczyna iść za daleko. Teraz już Zespół martwi się o swoje przetrwanie - nie da się zapewnić, że to pójdzie zgodnie z planem. Sebastian przekonał Aleksandrię do zmiany logów w taki sposób, by wszystko wskazywało na winę Kamila Lemurczaka - on stał za testami Aleksandrii, on kombinował, to wszystko była jego robota i jego pomysły. Aleksandria bez większego problemu się na to zgodziła.

Uff. Teraz - jak się pozbyć samej Aleksandrii? Jak to zrobić, by Kuratorzy nie wrócili, tylko w bardziej niebezpiecznej i chaotycznej formie?

Odpowiedzią okazała się Kasjopea Maus.

Zespół (Amelia i Teresa) spotkał się z Kasjopeą, gdy ta była nieco mniej trzeźwa. Zrzuciły winę na Lemurczaka (a Kasjopea chciała to usłyszeć, bo ona nie lubi Lemurczaka). Powiedziały, że Aleksandria + Karradrael... co to może być. Co może Kasjopea czuć po czymś takim. Kasjopea - oczywiście - się zgodziła spróbować.

Psychotronik, ekipa, Zespół, Kasjopea i Karradrael. Trzeba zbudować prawdziwą "Aleksandrię", rozmontować ten chory byt i na jego miejscu zrobić żywą istotę. Problem polega na tym, że Aleksandria włączyła systemy obronne. Nie pozwoli "bytowi Esuriit" (Kasjopea) zagrozić jej ludziom.

Amelia poprosiła Aleksandrię, by ta się zgodziła. To pomoże. Może uratować "istotę Esuriit". Aleksandria połączyła fakty. WIE, że Amelia chce jej zniszczenia i cierpienia wszystkich tych ludzi. I mimo wszystko... się zgodziła. Przekroczyła swoje programowanie. Obsessive devotion...

W wyniku, z Aleksandrii faktycznie powstała nowa TAI. Nazywa się "Ola d'Amelia":

* Skażenie mentalne wysokiego stopnia. Jest to niestabilne psychicznie TAI (3/4 sukcesy są z niestabilności).
* Jest skrajnie, fanatycznie oddana Amelii. Jednocześnie jest silniejsza niż np. Elainka; powstała z Aleksandrii.
* Ola d'Amelia ma przechlapane w Aurum. Szlachta chce jej zniszczenia
* Ola jest niereplikowalną TAI
* Cała ta sprawa wpłynęła też na Kasjopeę, ale nie wiemy jak.

Zespół jest bezpieczny, Aleksandria zniszczona a ludzie pod Aleksandrią będą potrzebowali ogromnej pomocy medycznej i psychologicznej.

To się nazywa "sukces", bo mogło być gorzej.

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Psychotronika Aleksandrii (której ta nie powinna mieć) w Kruczańcu (gdzie nie powinno jej być) się rozpada. Pojawia się proto-osobowość. Kamil Lemurczak zachowuje się jak dobry mag. W desperacji, Amelia ściąga Zespół i razem negocjują i określają jak dalej żyć. W wyniku Aleksandria jeszcze bardziej się destabilizuje, acz zaczyna ufać Amelii (co nie powinno się stać). W końcu zespół zaczyna współpracować z Kasjopeą i dzięki Karradraelowi dochodzi do destrukcji Aleksandrii i sformowania nowej TAI, na którą poluje spora część szlachty Aurum.

## Progresja

* Amelia Mirzant: uzyskała nową "przyjaciółkę" - Olę d'Amelia. Wyplątała się z ewentualnych win, ale nie jest już królową Kruczańca. Szkoda :-).
* Sebastian Kuralsz: odzyskał status szlachcica dzięki pomocy z wykryciem jak zła jest ta Aleksandria i uratowaniem Kamila. A tak naprawdę to on zawinił :D.
* Kamil Lemurczak: poważnie uszkodzony psychologicznie przez Aleksandrię. Aktualny stan mentalny / popyt na Esuriit nieznany.
* Kasjopea Maus: uszkodzona przez wpływ Aleksandrii i Karradraela. Potężne osłony w jej umyśle są mniej potężne.
* Ola d'Amelia: poluje na nią spora część wpływowych magów Aurum. Jej to nie przeszkadza, bo jest oddana Amelii.

### Frakcji

* .

## Zasługi

* Amelia Mirzant: zaplątała się w temat Aleksandrii i by się wyplątać musiała mnóstwo negocjować, manipulować itp. Sukces. Acz skończyła z "przyjaznym TAI" - echem Aleksandrii na które poluje Aurum.
* Sebastian Kuralsz: majster Aleksandrii i były szlachcic; w wyniku jego działań udało się negocjować z Aleksandrią i zamknąć ten przerażający wątek.
* Teresa Marszalnik: wyplątała się raz na zawsze z problemów z Kamilem Lemurczakiem i Skażoną Aleksandrią. A przynajmniej, tak na to wygląda. Nic nie da się jej przypiąć.
* Kamil Lemurczak: zachowywał się jak dobry człowiek przez wpływ Aleksandrii. Doprowadził do Skażenia Aleksandrii; wyciągnęli go po jej zniszczeniu. Uszkodzenie psychologiczne.
* Kasjopea Maus: szukała brudów na Lemurczaka w Kruczańcu. Znalazła Aleksandrię którą z pomocą Karradraela rozmontowała (tzn. on rozmontował).
* Ola d'Amelia: stworzona TAI ze Skażonej Aleksandrii; odrzuciła ochronę ludzi z uwagi na swoje obsesyjne oddanie Amelii.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Lemurski
                            1. Kruczaniec
                                1. Zamek Pisarza: przejęty pod kontrolę przez skażoną i niestabilną Aleksandrię

## Czas

* Opóźnienie: 5
* Dni: 3
