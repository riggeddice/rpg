## Metadane

* title: "Ekologia jaszczurów w Arachnoziemie"
* threads: spawner-godny-verlenlandu
* motives: bardzo-grozne-potwory, hartowanie-verlenow
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [230426 - Mściząb, zabójca arachnoziemskich jaszczurów](230426-mscizab-zabojca-arachnoziemskich-jaszczurow)

### Chronologiczna

* [230426 - Mściząb, zabójca arachnoziemskich jaszczurów](230426-mscizab-zabojca-arachnoziemskich-jaszczurow)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * .
* CEL: 
    * .

### Co się stało i co wiemy

* Kontekst
    * .

### Co się stanie (what will happen)

* S1: .
* S2: .
* S3: .
* S4: .

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Mściząb Verlen
    * Dostał swoje imię po tym, jak pobił pierwszą osobę którą uznał za to, że ukradła mu mleczaka.
    * Chce być Verlenem bojowym, chce wygrywać, nie do końca dba o ludzi. Ważne jest zwycięstwo.
* Fabian Rzelicki
    * ENCAO:  -+--0 |Cichy i elegancki;;Nie dba o to co inni czują i nie interesuje się ich problemami | VALS: Tradition >> Hedonism, Stimulation| DRIVE: Duty to Verlenland
    * Konduktor pociągu maglev. Spokojny, sfatygowany, elegancki dżentelmen w Verlenlandzie.
    * KADENCJA: "Tien Verlen, ogromna przyjemność zobaczyć kogoś pani klasy w naszym pięknym obiekcie poruszającym się po lądzie (z wolną kadencją) (ubrany w wyprasowany mundur, idealnie)"
* Marcel Biekakis
    * ENCAO:  -++0+ |Bezbarwny, przezroczysty;;Ekscentryczny, tatuaże WSZYSTKICH symboli ochronnych | VALS: Humility, Benevolence >> Security, Power| DRIVE: Polecenia Verlenów zasilają tatuaże ochronne
    * Czarny Kieł, w oddziale Mścizęba
* .POTWÓR Jaszczur Tunelowy
    * modelowany po stegozaurze / behemocie, ale też pluje czymś
    * bardzo pancerny, mało słabych stron
* .POTWÓR - Śluzowce
    * akcje: "kamufluje się w cieniu", "zaskakuje ofiarę", "pluje olejem", "przyjmuje formę zainfekowanego", "zmień formę"
    * siły: "mocny jak stal", "umiejętność kamuflażu", "zdolność przekształcania ofiar w zombie", "infekujący olej"
    * defensywy: "byt oparty na oleju", "wygląda jak ofiara", "zombifikowane ofiary jako osłona"
    * słabości: "krew jaszczurów tunelowych", "rozpuszczone ciało"
    * zachowania: "zaskakuj i zombifikuj", "korzystaj z osłony zombifikowanych", "wykorzystuje zombifikowanych jako pionki"
    * ..zainfekował: Damian (ojciec Fryderyka), Aniela (matka Fryderyka), 2 żołnierzy z garnizonu
* .POTWÓR - Crystaller
    * akcje: "imituję dowolny obiekt", "atakuję z zaskoczenia", "kryształowa trumna", "tworzę barierę", "wykorzystuję słabe kinetyczne punkty ofiary"
    * siły: "może naśladować dowolny kształt i formę", "ostry jak brzytwa", "chłonie krew ofiary"
    * defensywy: "kryształowa twardość", "niewykrywalny w formie obiektu", "przezroczysty i niemal niewidoczny"
    * słabości: "narażony na silne uderzenia"
    * zachowania: "atak z zaskoczenia", "chłonie krew ofiary"
* .POTWÓR - myceliański rój
    * akcje: "rosnę niewidocznie", "znienacka zarastam", "rosnę w poprzek drogi", "przenikam przez najdrobniejsze szczeliny", "rosnę na plecach wroga", "korzeniuję w słaby punkt"
    * siły: "ekstremalnie szybkie wzrost", "może rosnąć w każdym miejscu", "przyczepność na poziomie komórkowym", "ostre jak brzytwa strzępki"
    * defensywy: "ekstremalnie szybkie wzrost", "byt nieciągły", "kamuflaż"
    * słabości: "narażony na działanie wysokich temperatur", "wrażliwość na suszę"
    * zachowania: "converge and amass"

### Scena Zero - impl

.

### Sesja Właściwa - impl

Arianna poszła złożyć kondolencje rodzicom Fryderyka (Damian, Aniela). Składa kondolencje - ród Verlenów zawiódł. I chce ich zobaczyć. Rodzice Fryderyka są załamani. Wyglądają na załamanych. Ojciec (Damian) najpewniej jest pacjentem zero - był z Emmanuelle w podziemiach. Zapraszają Ariannę do środka. Acz Arianna - wyczulona na emocje - coś jest nie tak. Arianna została zaproszona. Arianna zaczyna "jaki cudowny chłopak, szkoda że nie ma z nami". Rodzice - nie płaczą. Są smutni, ale nie załamani. Robią wszystko co powinni, ale Arianna spodziewałaby się więcej. Wyglądają jak ludzie którzy stracili kolegę, ale nie syna.

Domek jest zadbany, jest... czysty, lekko zakurzony. Nic nie jest dziwnego, samo w sobie to jest dziwne. Nawet pokój syna jest wysprzątany. Są ślady po erupcji Flary Esuriit, są wysprzątane ale nie wyczyszczone.

Arianna robi niedyskretną uwagę "syn poszedł z dymem, lol". Chce zobaczyć. On - "to bardzo przykre, tien Verlen, ale rozumiem że to nie Wasza wina. Po prostu. Nie mogliśmy temu zapobiec. Nie wiem jak to się stało. Nic nie wskazywało na to, po prostu to zrobił". Matka "gdybym nie zostawiła książki na wierzchu". Ojciec "zawsze tam leżała, nie mogliśmy tego przewidzieć"... jest SMUTEK ale nie ma CIERPIENIA.

Arianna idzie sprawdzić ten pokój nastolatka. Pokój nastolatka nie jest wyczyszczony, jest sprzątnięty. To znaczy, że notatki, pamiętnik itp. będą dostępne. Arianna otwiera okno.

(+Arianna spodziewa się czegoś, -Arianna nie wie czego, -Arianna jest 'out-of-position' +Arianna ma servar lol)

Tr+2:

* "Arianna się wychyla by otworzyć okno, widzi w odbiciu szyby jak ojciec otwiera usta i leci strumień śluzu w jej nieosłoniętą głowę". Arianna korzysta z ciężkich kotar i się zasłania za nią. +3Vg
    * X: przeciwnik ma inicjatywa
    * V: Arianna odzyskała inicjatywę, zerwała kotarę i rzuciła się na bok, ale śluz zatrzymał się na kotarze i na servarze
    * (+1Vr -3Vg) V: Arianna założyła i zahermetyzowała hełm. 5 sekund. Ojciec ucieka.

Arianna go goni. Łapie go przy kuchni. Nigdzie nie widać matki. Arianna nokautuje ojca - silne uderzenie w głowę, przygniecenie do ziemi, zatrzaskuje, wsparcie.

* "Arianna unieruchamia rzucając na niego i przygniatając (ale by nie zabić)"
    * (-płynny byt) Xg: Złapałaś go, ale on się wyślizgnął. Rozpuścił się. Ten "śluz" wraca w jego ciało. Próbuje odpełznąć do najbliższej rury do umywalki!
    * (+W GARNEK GO!) X: daje mu podbicie i wzmocnienie manewru (część odpełzła)
    * (+RĘCZNIKIEM PAPIEROWYM GO! KOlanko pod zlewem, przechwycę część) X: zwiał (rdzeń uciekł)
    * V: Arianna ma próbki stwora. Masz go w garnku. GARNEK ZAHERMETYZOWANY!

Arianna ma próbki przeciwnika. To dobra wiadomość i więcej, niż ma Mściząb. Co dużo mówi.

Viorika przepytuje w tym czasie agentów Czarnego Kła - co wiedzą o przeciwniku, jak bardzo Mściząb spieprzył. Ale liczy na to, że się dowie czegoś o przeciwniku. Oni mają personalne pancerze a nie servary. Jako że stracili kilka osób, może czegoś się dowiedzieć. Czy było AAR itp. Rozmówcą Vioriki został Marcel Biekakis. Z jednej strony wygląda bezbarwnie, z drugiej - ma dziwny symbol na piersi. I dużo tatuaży ezoterycznych symboli (wszystkie ochronne). On dowodzi grupą odkąd dowódca zginął.

* Tien Verlen, nie udało nam się znaleźć ani jednego ciała.
* Trzy osoby. Ostatni zniknął. Z tyłu. Po 10 sekundach odzyskaliśmy LoS. Nie było go - ani ciała, ani pancerza, ani nic.
* Godzinę później znaleźliśmy jego pancerz. Bez niego. Pocięty jak tysiącami pajęczych nitek. Ze wszystkich stron. Jest tu zło, tien Verlen.

Viorika szuka habitatu i metod działania. Czy ktoś widział przeciwnika choć przez chwilę. I czemu nie wezwali wsparcia.

(+Viorika to exemplar, +Mściząb oddał Viorice dowodzenie, -oni nie wiedzą co wiedzą)

Tr Z +2:

* V: 
    * Viorika ma twardy dowód po przegadaniu, że nie ma możliwości by oni nie widzieli potwora. Albo widzieli ale o tym nie wiedzą albo przeciwnik jest niewidzialny.
    * Stwór atakuje TYLKO z zaskoczenia
    * Nigdy nie zabijał więcej niż jednej osoby na raz.
* X: Viorika naciska ZA MOCNO. Marcel jej ufa, ale reszta Kłów - nie. Uważają, że Viorika ich wytraci, bo stanęli za Mścizębem.
* V: Viorika ma PEWNOŚĆ że ma do czynienia z jakąś formą mimika. To pasuje do wszystkich opisów. "Stalagmit zjadający górnika".

Arianna, lekko wstrząśnięta spotkaniem z "jeńcem" i poinformowana przez Viorikę o sytuacji łączy się z Ulą.

* A: "Ula słuchaj nie chcesz się wyrwać od Marcinozaura na jakiś czas"
* U: "Tien Verlen, nie rozumiem co próbujesz zasugerować"
* A: "Próbuję zasugerować że możemy zafundować Ci bardzo ciekawe wakacje z projektem który może Cię zainteresować i nie zawiera Marcinozaurów"
* U: "Nie poradzisz sobie beze mnie? (zdziwienie)"
* A: "Nie chodzi czy sobie poradzę czy nie, ale dużo ludzi ucierpiało i nie chcemy ryzykować że ucierpi więcej. A wolimy mieć wsparcie specjalisty od tematu"
* U: "Jak mniemam, chodzi o jakieś bioformy?"
* A: "Tak, potrzebujemy odtworzyć miejscowe potwory, jakkolwiek źle to nie brzmi, region ma problem bo ich zabrakło"
* U: "Cieszę się, że jest choć jeden Verlen który rozumie sytuację. Będę tam... (chwila ciszy) spieszy Ci się?"
* A: "Tak, spieszy mi się"
* U: "Za godzinę. Najpóźniej dwie. Muszę przekonać Marcinozaura, by zapewnił mi odpowiedni sposób transportu."
* A: "Będę Twoją dłużniczką".
* U: (nic nie powiedziała) (nie uwierzyła)

Są tu indeksy lokalnych potworów, informacje o stworach itp. Viorika przygotowuje dla Uli wyciąg. Arianna widzi, jak śluz próbuje się zbliżać do nieodsłoniętych części ciała, ale to pokazuje jego "działanie". Co więcej - nie ma ubrań rodziców chłopaka. Ubrania wiszą w szafie. Czyli stwór je zsyntetyzował. Shapeshifter.

Niejeden żołnierz spojrzał na Ariannę ze zdziwieniem na rozkaz "sprokurujcie mi mysz". Gdy przyszła z garnkiem. Zastanawiający się żołnierze dostają Spojrzenie Vioriki. Żołnierze dość szybko dostarczyli Ariannie kilka myszy. Arianna wrzuciła mysz do garnka w warunkach kontrolowanych.

Śluz najpierw nie zareagował, potem przybrał kształt myszy. Mysz się wycofała, szuka wyjścia, śluz ją otoczył i... "zeżarł". Rozpuścił od środka. Wypłynął oczami itp. Został szkielet, który też został rozpuszczony, po prostu później. Rozpuszczenie myszy zajęło... 30 min. Gdy mysz była zainfekowana, poruszała się błędnie, skonfundowana. Gdy mysz została zeżarta, śluz przybrał formę szarej myszy, zachowując się jak mysz.

Arianna wydaje rozkaz - w ciągu kilku godzin chcemy uzbroić wszystkich żołnierzy w hermetyczne servary klasy Scutier. Podpiąć scutiery do sentisieci by mieć "disable command". Viorika sprawdza żołnierzy - rekordy, kto zniknął, kto był otępiały itp. Szuka kandydatów. 

Tp+3:

* V: Viorika ma dużą część populacji zbadaną
* X: Rekordy w mieście nie są prowadzone bardzo dokładnie, bo żołnierze mają kochanki i nic złego się nie dzieje
* Vr: Viorika i Arianna zidentyfikowały sześć potencjalnych śluzokandydatów

Dwóch Kłów było zarażonych. Oni poszli badać miejsce zbrodni i Mściząb CHCIAŁ ich opieprzyć itp. bo nie byli na miejscu na czas. Ale dowódca (który nie żyje) go przekonał, żeby im wybaczył. Mściząb nie chciał być okrutny czy głupi wobec swoich ludzi - tym razem to nie zadziałało.

Arianna idzie pogadać z konduktorem pociągu. REKWIRUJEMY POCIĄG NA IZOLATKĘ. Konduktor na imię ma Fabian.

* F: Tien Verlen, ogromna przyjemność zobaczyć kogoś pani klasy w naszym pięknym obiekcie poruszającym się po lądzie (z wolną kadencją) (ubrany w wyprasowany mundur, idealnie)
* A: Widzę że pan również jest człowiekiem kultury i jako ludzie kultury się dogadamy
* F: W rzeczy samej tien Verlen
* A: Muszę czasowo zarekwirować pociąg w celach izolacji niebezpiecznych stworów z tego terenu
* F: Tien Verlen, pociąg do tego nie służy, pociąg musi jeździć i transportować dobra i ludzi
* A: Zdaję sobie sprawę do czego służy pociąg, wiem też jak dobrze jest przystosowany do ciężkich warunków na szlakach Verleńskich więc jest najlepszym obiektem. Jakby niezbędne będzie przeniesienie stworów, łatwe. Konkretne wagony.
* F: (myśli jak żółw) Zgadzam się z rozumowaniem. Muszę poprosić o wypisanie odpowiednich form i formularzy i z przyjemnością udostępnię swój ekwipunek do Twojej dyspozycji.

Arianna chce go przekonać że nie ma czasu i ludzie potrzebują pomocy!

Tp+3:

* V: 
    * F: "czy pozwolisz, że ja wypełnię formularze a Ty je podpiszesz? Wszystko musi być poprawne formalnie a ja wiem jak je wypisać. W ten sposób przyczynię się do chwały Verlenlandu"

Arianna ogłosiła, że chce zrobić checkup. Wszyscy ludzie mają się zgłosić do pociągu na zaawansowane badania. Bo jest skażenie biologiczne. (w rzeczywistości badań nie ma, są fikcyjne, ale to atak psychologiczny na śluzowce).

TEATR na taktykę, czyli base: Typowy. (+pamięć śluzowców działa przeciw nim)

Tp Z +3:

* VrVr: WSZYSTKIE śluzowce nie przyszły.
* Vz: wykorzystanie scutierów i tabletów pokazujących zjadaną mysz sprawiło, że nawet "prepersi" i "paranoicy" przyszli.

.

* Mamy 11-19 śluzowców na wolności. Wiemy, że na wejściu był jeden.
    * 2 Kłów
    * 3 żołnierzy
    * 2 rodziców chłopaka (tych nie złapaliśmy)

Macie tożsamość WSZYSTKICH śluzowców. Nie ułatwia fakt, że śluzowiec może "wydzielić" mysz i zainfekować człowieka z innej formy. Ale nie wydzieli czegoś czym nie był. Śluzowce BARDZO próbują się nie zbliżać do jaszczurów i krwi jaszczurów. Śluzowce NA PEWNO są w kanałach...

Przybywa Ula. Viorika przechwytuje ją na granicy terenu. Ula nie leci jakimś epickim pojazdem. Ona leci jako pasażer szybkiego izolowanego ścigacza. Sama ma najbardziej eleganckiego Lancera jakiego Viorika widziała - takiego jak Arianna. On wygląda jak suknia. Dama typu Anioł Śmierci. Piękny, zaizolowany. Wada - ten Lancer wygląda jakby wojny nie widział. LANCER ULI NIE MA BRONI!!!

* V: (priv -> Ula) "nieczęsto widzę taką konfigurację"
* U: (priv -> Viorika) "chodzi Ci o Lancer analityczny?"
* V: "zupełnie bez broni?"
* U: "jestem na terenie Verlenów. Jestem Blakenbauerem. Nie mam prawa być uzbrojona" (powiedziała to z przekonaniem)
* V: "na tym terenie NIE POSIADANIE BRONI jest... ryzykowne"
* U: "w takim razie zostałam niewłaściwie poinformowana. Dziękuję za informację." (ona nie jest zła, to są... słowa) (Marcinozaur by NIGDY tego nie powiedział)
* V: "Marcinozaur Ci tego nie powiedział?"
* U: (chwila ciszy) "nie rozmawiałam z nim na ten temat. Nie chcę wyglądać zbyt groźnie."
* V: "Fair enough, potrzebujesz broni defensywnej" (Lancer Uli ma podwójny zamknięty system filtrujący i wzmocniony pancerz)

Viorika daje Uli broń do ręki. Jej Lancer nie ma hardpointów... V -> A wysyła visual, tylko się nie przewróć. I Viorika przyznała Uli jednego żołnierza w Scutierze. Bo kurde zabiją Blakenbauerkę XD. MUSI ktoś ją chronić. I jak on powie "padnij", ona ma leżeć XDXDXD. A w międzyczasie robi dump Uli czego się dowiedziały.

* Ula: "Dobrze podeszłyście do tego. (myśli) Jeśli krew jaszczura jest toksyczna, zsyntetyzuję coś, co się rozprzestrzeni. Agent gazowy. Wiralny."
* Viorika: "Z adekwatnymi zabezpieczeniami?"
* Arianna: "Może nietoperze?"
* Ula: "(myśli) Nietoperze mają swój urok, mniej niebezpieczne niż agenty wiralne... (myśli). Jeśli mamy próbkę śluzu..."
* V: "Cały garnek"
* Ula: "Proszę nie przerywaj, myślę..."
* Ula: "Jeśli mamy próbkę śluzu, mogę skierować nietoperze albo faktycznie pająki by celowały TYLKO w te istoty."
* Ula: "Czyli masz cały garnek (z zainteresowaniem)"
* V: "Jest dość mobilny, Arianna złapała"
* A: "Mamy też przedział pociągu i odizolowanych żołnierzy"
* U: "Mamy więc na czym eksperymentować."
* V: "Jeśli możliwe jest odwrócenie, byłoby to preferowane"
* U: "Jeśli jest to możliwe, mi się to uda zrobić. Czy odwracać nawet, jeżeli doszło do uszkodzenia bioformy ludzi? Np. nie uda się przywrócić ich do pełnej funkcjonalności?"
* V: "Jeśli będą SOBĄ to tak, nawet jak słabsi. Jak nie chcą żyć, w las."
* A: "Przywróć wszystkich, sami zdecydują. Jeśli są za słabi, zadecydują rodziny."
* U: "Jak sobie życzycie. Jeśli to w ogóle możliwe, mi się uda to zrobić."

Ula przygotowuje laboratorium, po prostu zajmując kawałki pociągu. Co ciekawe, ona hoduje swoje laboratorium solo.

* TWORZENIE BIOLAB
    * Tr Z M +2 +3Ob:
    * X: manifestacja laboratorium Uli jest absolutnie obrzydliwa. Mysz się "otwiera" itp. Ogólna opinia o Blakenbauerach SPADA. Nawet Viorika się krzywi. Viorika kontroluje lokalną sentisieć by zaakceptować tą Blakenbauerską.
    * Ob: Ula się starała. Bardzo starała. Nagle - jeden z jaszczurów został substratem laboratorium. Jeden z DWÓCH jaszczurów. Wszyscy "ej!" Ula PRÓBUJE udawać że o to chodziło. Wreszcie Ula ma biomasę.
    * Vr: Laboratorium się sformowało. I jest to też przedłużenie servara (bo Ob). Na widok i zapach czegoś takiego biedny żołnierz pilnujący biolab Uli wymiotuje w scutierze. 
        * Ula udaje, że od początku o to jej chodziło. Upewnia się, że nikt się nie zorientował. Viorika udaje, że nie widzi. Arianna pokazuje kciuk do góry. Ula lekko zaciska zęby ze złości i kieruje uwagę na problem. TERAZ jej zależy.
* ODBUDOWA LUDZI ZE ŚLUZOWCÓW - Arianna widzi moc Uli, to nie jest typowa Blakenbauerka. Ona INFEKUJE SOBĄ wszystko.
    * (+Ula jest na prawach arcymaga, +Ula ma wszystkie substraty, +krew jaszczura -śluzowce w pełni pożerają ofiary, -dużo trudniej zregenerować niż zniszczyć)
    * Tr Z M +2 +3Ob
    * X: Uli nie uda się uratować większości śluzowców
    * V: Ula stworzy bioformy (latające MEGA obrzydliwe pijawki) zdolne do infekcji śluzowców
    * Vz: Ula da radę uratować co najmniej jednego śluzowca i usunąć wszystkie długoterminowo
    * Vr: uratowane śluzowce nadają się do życia w społeczeństwie i będzie ich ~7, choć nie wszyscy na 100%.
    * Vz: Ula wprowadza te latające pijawki do ekosystemu na stałe, by Arachnoziem mogło je przywrócić / odbudować w wypadku kryzysu. (nieaktywne "gniazdo pijawek")
        * Ula patrzy z lekką wyższością "mówiłam, tien Verlen?"
            * ludzie rzygają jak koty, biomasa to wciąga
            * Viorika i Arianna widzą - Ula się popisuje.
* ODBUDOWA JASZCZURA
    * Ula "ostatni problem do rozwiązania to jaszczur, prawda?"
    * Viorika "jaszczur robił za..." Arianna "blokadę wyjścia" Viorika "kontrolę ekosystemu"
    * Ula "na czym polega problem?"
    * Viorika "nie ma jaszczura w ekosystemie"
    * Ula (pokazała palcem na martwego jaszczura) "a on?"
    * Viorika "szybko zgnije, nie będzie bariery"
    * Ula (szeroki, lekko szalony uśmiech) "ależ śmierć nie ma znaczenia, biomasa to biomasa, możemy go reanimować, możemy uruchomić jego organy?"
    * Viorika "zadziała jak za życia?"
    * Ula (ten sam uśmiech) "jak z nim skończę, nawet lepiej"
    * Viorika "jeszcze mamy mimika do wytępienia, diabli wiedzą co..."
    * Viorika "czyli tien Blakenbauer, to nie jest krytyczne w tej chwili, odpocznij trochę, kawał dobrej roboty, każdy musi czasem odpocząć"
    * Ula "nieczęsto łączę się z biolabem tak jak teraz, ale w Verlenlandzie to była jedyna opcja (kłamie), więc wolę to zrobić od razu. I tak chcę próbkę, by mój ród zsyntetyzował Wam więcej tych jaszczurów. Zrobią to jak ich poproszę. Ale na razie - odbudujmy płaszczkę z płaszczki."
        * Arianna WIDZI, że Ula nie robi tego dla tych ludzi. Nie robi tego dla Was. Ona to robi dla siebie. Poprosiłyście, więc to robi, bo może. Bo poprosiły i ona pokaże jaka jest dobra.
    * Arianna "nie potrzebujemy ZA DOBREGO jaszczura. Sentisieć się budzi, nie możemy więcej czarować, Blakenbauerska magia."
    * Ula "skoro tak, tien Verlen..." (z nosa już płynie krew)

Na szczęście servar Uli jest skonfigurowany pod komfort. Viorika pokazała jej jak stać na warcie by było mniej nieprzyjemnie.

Arianna kazała dać dane odnośnie mimika który został. Krzyżuje dane z kamer, opisy żołnierzy, kto zniknął i kiedy by zlokalizować gdzie najpewniej jest mimik, w jakich obszarach. I czy w podejrzewanych terenach coś się zmieniło. Nie zauważy. Co więcej, czekają dzień - niech mimik zgłodnieje i zacznie polować. Niech nasi żołnierze polują na mimika w nocy, by go ośmielić. Każdy żołnierz ma kamery tak zamontowane, by patrzyły we wszystkich stronach.

Viorika włącza Mścizęba w akcję polowania na mimika. Viorika powiedziała nie "daję Ci szansę" ale "chcę Cię na tej akcji bo się przydasz. Strategicznie zawiodłeś, ale operacyjnie możesz uratować czyjeś życie". Mściząb jest odizolowany, on NIE WIE o Uli. Viorika "aha, to obrzydliwe coś jest odpowiedzialnością Arianny i nam pomaga". Mściząb "rozumiem (ponuro)". 3 Lancery w powietrzu i grupa żołnierzy przeszukujących okolicę by mimik miał co jeść XD.

WYWABIAMY MIMIKA:

Tr Z +4:

* V: Mimik wyjdzie
* X: Mimik będzie miał efekt zaskoczenia. Ludzie nie są na tyle wyszkoleni.

To nie było w mieście. Koleś na chwilę zboczył z trasy by odlać się na drzewo. Niezgodnie z poleceniem. I nagle drzewo się otworzyło... i ze środka setki nici wystrzeliły w żołnierza...

RATUJEMY ŻOŁNIERZA

(-mimik jest cholernie szybki +Arianna sentisieć)

Tr +3:

* Viorika: remote control i servar w tył, Arianna: sentisieć i szarpnąć glebą koło mimika, Mściząb strzela by mimik się odpieprzył
    * X: Żołnierz będzie ranny. Nici z pyska mimika są niezwykle wręcz ostre. Servar spłynął krwią.
    * V: inicjatywa Zespołu. Mimik nie dał rady wykonać prawidłowo ataku.
* CEL: Ten mimik atakował od tyłu, z zaskoczenia. Ogień zaporowy + karabiny maszynowe.
* (+dużo pocisk, +wystarczająco otwarty teren, +przeciwnik jest wytrącony)

Tp Z +3:

* Xz: Lancer integrity 40%: ostrzeliwany mimik się przekształcił, w taki krystaliczny "kwiat" czy "kokon" i "liznął" wszystkich krystalicznymi nićmi. Lekkie uszkodzenia.
* V: Podbicie - mimik nie ma jak się ruszyć i dostaje rany. Przekształca się błyskawicznie szybko, ale nie ma w co i jak. Kruszą się kryształy. Bardzo cienkie.
* Vr: Mimik został rozstrzelany.

Viorika zbiera żołnierza, MEDIC! Żołnierzowi nic nie będzie tylko mamrocze "nie dawajcie mnie temu potworowi nie dawajcie mnie "sojusznikowi""... Viorika "za głupotę by Ci się należało..."

Arianna i Viorika chcą za karę dać Mścizębowi wszystkie zasługi. Ale nie zasługi Uli. Verlenland sam się reguluje - nie mówimy o obecności Uli. Mściząb jest tym, kto stoi za naprawą sytuacji. I nie jest mu z tym dobrze, że jego zasługi przyszły z litości dwóch Verlenek. I, co gorsza, jednej Blakenbauerki. A Ula - tymczasem - nie ma o niczym pojęcia i próbuje spać w biolabie który syntetyzuje kawałki jaszczura do wrzucania do kopalni...

## Streszczenie

W Arachnoziemie są dwa stwory - śluzowiec i mimik. A+V wprowadziły mechanizmy ochrony ludzi i oddzielania zainfekowanych ludzi od potworów i wezwały Ulę Blakenbauer na pomoc. Ta przybyła, zrobiła biolab i stworzyła płaszczki mające uratować kogo się da; jaszczury tunelowe ściągnie od Blakenbauerów. Sytuacja jest opanowana - gdy Ula zajęła się usuwaniem śluzowców to Verlenki usunęły krystalicznego mimika. Ogólnie - Arachnoziem ustabilizowany. A całość zasług za karę dostał Mściząb.

## Progresja

* Mściząb Verlen: Arianna i Viorika przypisały mu złośliwie wszystkie zasługi z uratowania sytuacji w Arachnoziemie (mimik i śluzowiec). 
* Ula Blakenbauer: lekki szacunek ze strony Arachnoziemczyków; przyszła pomóc, a nie musiała. Ale też zabawne historie, bo jej biolab jest OBRZYDLIWY i jest dowodem na plugawość Blakenbauerów. Opinia ogólnie pomocnej acz primadonny i obleśnej.

## Zasługi

* Arianna Verlen: optymistycznie weszła do domu martwego nastolatka i nie tylko uniknęła śluzowca ale też złapała jego próbki, potem wezwała Ulę Blakenbauer na pomoc. Krzyżuje dane z kamer by pomóc Viorice znaleźć drugiego mimika.
* Viorika Verlen: przepytała żołnierzy i Kłów odnośnie sytuacji, sprawdziła dokumenty żołnierzy by izolować śluzińce, zaopiekowała się pacyfistyczną Ulą i po włączeniu Mścizęba w akcję usuwania krystalicznego mimika opracowała jak go usunąć i go rozstrzelała.
* Ula Blakenbauer: przybyła wezwana na pomoc przez Ariannę by pomóc z Jaszczurami Tunelowymi; szarogęsi się trochę, ale nie jest nieprzyjemna. Stworzyła obrzydliwy biolab i nawet skutecznie uratowała niektóre ofiary śluzowców.
* Marcel Biekakis: nieco dziwny tymczasowy dowódca Czarnych Kłów Mścizęba, zachowuje się normalnie ale ma mnóstwo tatuaży defensywnych. Jako jedyny Kieł ufa Viorice, bo... ufa wszystkim Verlenom. Dobrze zaraportował co i jak.
* Fabian Rzelicki: najbardziej rzetelny z konduktorów maglevów; jak tylko Arianna powiedziała że potrzebny jest pociąg to wypełnij jej wszystkie formy i formularze i oddał pociąg do chowania potworów.
* Mściząb Verlen: załamany zdradą ze strony Emmanuelle, poszedł za rozkazami Vioriki i uczestniczył w operacji zestrzelenia krystalicznego mimika z powietrza.

## Frakcje

* Czarne Kły Mścizęba: dowodzi Marcel Biekakis; wystarczająco kompetentni, ale niskie morale. Viorika ich postawiła na nogi.
* Potwory Arachnoziemskie: Ula Blakenbauer rozpoczęła pracę nad ściągnięciem jaszczurów tunelowych i ustabilizowała kopalnię na tyle, by tymczasowo spawner nie działał.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                           1. Arachnoziem: słynie ze złóż minerałów, niestabilności i wyjątkowo podłych podziemnych potworów; ma garnizon 50 żołnierzy na 20k populacji rozsianych po górzystym terenie.
                                1. Kopalnia
                                1. Stacja Pociągu Maglev
                                1. Garnizon

## Czas

* Opóźnienie: 1
* Dni: 3

## OTHER
### Fakt Lokalizacji
#### Miasto Arachnoziem

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. Arachnoziem
                                1. Arachnoziem
                                    1. Kopalnia
                                        1. Szyby Główne
                                        1. Korytarze Pajęcze
                                    1. Bar Łeb Jaszczura
                                    1. Garnizon
                                    1. Ryneczne Uliczki
                                        1. Sklepy
                                        1. Budynek Poczty
                                        1. Handlarz Minerałów
                                        1. Domy Starszych Mieszkańców
                                        1. Bloki Robotników Kopalni
                                    1. Stacja Pociągu Maglev
                                    1. Hotel Wielka Ruda
                                1. Arachnoziem, okolice

