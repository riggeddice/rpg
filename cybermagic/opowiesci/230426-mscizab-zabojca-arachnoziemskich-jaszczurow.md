## Metadane

* title: "Mściząb, zabójca arachnoziemskich jaszczurów"
* threads: spawner-godny-verlenlandu
* motives: milosc-szczenieca, bardzo-grozne-potwory, hartowanie-verlenow, b-team
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [230419 - Karolinka, nieokiełznana świnka](230419-karolinka-nieokielznana-swinka)

### Chronologiczna

* [230419 - Karolinka, nieokiełznana świnka](230419-karolinka-nieokielznana-swinka)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * .
* CEL: 
    * .

### Co się stało i co wiemy

* Kontekst
    * .

### Co się stanie (what will happen)

* S1: .
* S2: .
* S3: .
* S4: .

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Mściząb Verlen:
    * Dostał swoje imię po tym, jak pobił pierwszą osobę którą uznał za to, że ukradła mu mleczaka.
    * Chce być Verlenem bojowym, chce wygrywać, nie do końca dba o ludzi. Ważne jest zwycięstwo.
* .POTWÓR Jaszczur Tunelowy
    * modelowany po stegozaurze / behemocie, ale też pluje czymś
    * bardzo pancerny, mało słabych stron
* .Blakenbauerski Ixaculat
    * stealth, chryssalid zombification, oil

### Scena Zero - impl

.

### Sesja Właściwa - impl

Arianna i Viorika trochę patrzyły na chorą Elenę. I przy dbaniu o Elenę i Viorika i Arianna wyczuły echo Esuriit. Z Eleny. Od razu sprawa była eskalowana, przenoszona do ważnych Verlenów, bo to jest... Verlenowie bardzo negatywnie patrzą na Esuriit. Na szczęście okazało się, że jakkolwiek Elena ma predylekcje do Esuriit, ale nie jest nim Skażona. Trzeba po prostu trochę pilnować, ale będzie dobrze. Chyba że stanie się coś bardzo traumatycznego.

Ale to sprawiło, że Arianna się tymczasowo wyczuliła na sygnały Esuriit. I to sprawiło, że gdy w sentisieci pojawił się delikatny smak Esuriit z jednego z mniejszych miast Verlenlandu, Arianna to wykryła. Elena jest w dobrych rękach, więc Arianna i Viorika jadą to sprawdzić. "Wakacje".

Miasteczko nazywa się Arachnoziem. Nazwa pochodzi od gigantycznej kopalni. Jak "podziemna sieć pajęcza". Wzgórza, wielka kopalnia, przemysł cięższy, 50k mieszkańców i aż 50-60 żołnierzy. Na miejscu jest już mag - to Mściząb. Problem w tym, że Mściząb ma 17 lat. I chce się wykazać.

Mściząb jest poważnym młodym magiem, pancerz, marsowa mina, mięśnie, blizna na twarzy.

* M: "Arianna. Viorika. Jaka to przyjemność Was widzieć."
    * on jest z bardziej radykalnego skrzydła Verlenów; ważniejsza sława, waleczność niż pomoc. TIENOWATY WŚRÓD VERLENÓW.
* A: "Też się cieszymy."
* M: "Wszystko mam pod kontrolą. Wszystko."
* A: "Nie wątpię."
* V: (ćwiczy charakter i nie mówi)
* A: "Są jakieś sprawy w które powinniśmy się nie mieszać?"
* M: "Jakiś szczyl popełnił samobójstwo, był słaby."

Mściząb jest przekonany, że to samobójstwo jakiegoś dzieciaka koconego w szkole; przekierował sygnały sentisieci na siebie (ukryć Esuriit itp.), zajmuje się tu "czymś", uważa że Arianna i Viorika są przereklamowane. Zwłaszcza Viorika. Viorika z przyjemnością poszła na ostro z Mścizębem. Viorika walczy, Arianna ma się _attunować_ z sentisiecią i samym Mścizębem - zobaczyć co tu się dzieje, czy jest Skażony itp.

Tr Z (Viorika ma przewagę doświadczenia i skilli) +3 (3Xr3Xg):

* Vz: Viorika go wyśmiała. Mściząb atakuje, Viorika (krok w bok) i "z czym do Verlenki, nawet młody dźwiedź by Cię wyśmiał"
    * wyraźnie Mściząb jest DZIKI. Chce skrzywdzić Viorikę. Jest niezwykle sfrustrowany.
        * jest to frustracja nastolatka któremu od dawna nic nie wychodzi.
* Xz: Mściząb jest dobry w walce. Zrobił manewr, ostre cięcie, zranił Viorikę, poszorował po bicepsie.
    * V: "Jesteś Verlenem, choć trochę"
    * M: "Jesteś stara, przereklamowana i słaba!"
    * V: (wyśmiewa) +3Or
* V: Viorika zrobiła mu jajecznicę na miękko. Mściząb jęczy na ziemi. 
    * Viorika się odsuwa i wkurza "jeśli KIEDYKOLWIEK chcesz mieć dzieci musisz bardziej uważać"
    * M: "Nie chcę dzieci... bo jeszcze będą takie jak Ty... jesteś prawie Blakenbauerką!"

Arianna nie wyczuła ŻADNYCH sygnałów Esuriit od Mścizęba. On po prostu taki jest jak mu nie wychodzi. A nie wychodzi. Arianna linkuje się z sentisiecią i chce się dowiedzieć gdzie działają jego ludzi. Czy ma w ogóle jakichś ludzi.

* Arianna ma odpowiedź z sentisieci, ma 25 ludzi, oddział Czarnych Kłów (sam nazwał). Co ciekawe, sentisieć powiedziała, że ma 17 aktywnych z 25. 8 osób nie żyje.
* To była jedyna instacja sygnału Esuriit. Tu nie ma "problemu z Esuriit".
* Były rzucane tu zaklęcia. Mściząb kontra potwory."
* Jego oddział zabił dwa potwory.
* Chłopak, który popełnił samobójstwo, nazywał się Fryderyk. Co ciekawe, sentisieć zna ten rytuał Esuriit. To flara i 'eternal death'. Sprawienie, by jego tkanki były bezużyteczne.
    * to ogólnie rytuał anty-saitaerowy. I robi flarę.
    * Fryderyk pochodzi z rodziny gdzie ojciec jest górnikiem a matka żołnierzem w garnizonie. Matka walczyła z Saitaerem i rytuał był w książce.
* W mieście zginęło ostatnio (tydzień) 7 osób. To nienormalne.

Arianna robi głębsze badanie sentisieci.

Tr Z (Verlen) +3:

* Vr:
    * Fryderyk NIE był kocony. (17 lat)
        * patterny zachowań Fryderyka się zmieniły 2 dni przed śmiercią. Zaczął unikać domu i grzebać w bibliotece.
    * Mściząb pojawił się tu 9 dni temu.
    * 7 dni temu zaczęli umierać ludzie.
    * koło 5 dni temu kilka osób wysłało sygnały poza Arachnoziem, ale sygnały zostały zablokowane przez sentisieć z woli Mścizęba.
    * wzór jest taki, że ludzie giną od jakiegoś potwora. To hunter-killer. Nikt go nie widział. Ale poluje na ludzi i przebija im serca szponem czy coś.
    * Mściząb próbuje osaczyć tego stwora i go zabić.
    * Czarne Kły są rozstawione by 1) znaleźć potwora 2) nikt nie opuścił miasta. Bo gdyby się skupili na dokładnym przeszukiwaniu to jest szansa że by znaleźli.
    * W te dwa dni zabili dwa potwory. Które robiły normalne rzeczy (przesuwały się, łaziły itp.)
* (+M +3Ob -> Tp): Vr
    * Sentisieć zaraportowała coś ciekawego.
        * jaszczury były NIE TAM gdzie zwykle. Dwa pokonane jaszczury były poza kopalnią. One zaatakowały miasto. Ale one były tam "od zawsze", to była parka. NIGDY nie atakowały miasta.
            * on nie interaktował z tymi jaszczurami wcześniej.
        * był ktoś "nieznany" czy "nieupoważniony" w tej kopalni i **coś** tam zrobił. Wcześniej. Sygnatura zbliżona do Blakenbauerów.
        * delikatna sygnatura magiczna tych jaszczurów była trochę inna niż za każdym innym razem. Coś je ZMIENIŁO.
        * nie wyszło nic na co polowałyby Jaszczury. Raczej Jaszczury zostały wypędzone albo sprowokowane albo zmuszone do wyjścia. Były ZMIENIONE.
        * Jaszczury były Dotknięte przez coś, co zawierało krew maga. Dlatego to działało.

Przybywa bohater pokonać potwory. Potwory stwierdzają "damy mu szansę". On NIC NIE ZROBIŁ by je wyciągnąć ale KTOŚ je zmienił. I najpewniej za tym przyszło COŚ co wielkie Jaszczury odpędzały. W walce z Jaszczurami... zginęło 6 Kłów. 2 pozostałych Kłów zabił Łowca.

* X: Tymczasowo ukryte ślady pierwotne. Przez jakiś czas nie da się wszystkiego dowiedzieć. Przeciwnik (Trzecia Strona) jest dobry i to dobrze opracował.
* Vr: 
    * Arianna "złapała" namiar na maga nie rodu Verlen, czarodziejkę, która była w kopalni. Lokalizacja -> jest jako jeden z Czarnych Kłów.
        * Emmanuelle Gęsiawiec. Dla przyjaciół Emma. 17 lat (maskuje się jako 19-letnia czarodziejka maskująca się jako 27-letni brodaty żołnierz i panikuje co 15 sekund).
    * Ojciec Fryderyka zniknął jakieś 10-11 dni temu na jeden dzień.

Okazuje się, że nie było ataków ze strony łowcy-zabójcy w miejscu, w którym jest truchło Jaszczurów.

Viorika się lepiej przygotuje. Sentisieć pod palcem. Jej nowy cel - sprawić, by Mściząb miał trochę strachu bożego. "Zostaniesz Diakonem, nie Verlenem". Jak tylko Aria się podzieli z Vioriką informacjami, Viorika go opierdoli "jeśli będziesz dalej tak traktować ludzi to Cię wyślę Diakonom na przerobienie bo to ich metody działania."

Tr (pełna karta bo dowodzenie i opierdalanie) Z (Arianna i jej argumenty) +3:

* Vr: Mściząb Viorikę zaatakował słownie na te argumenty "Mściząb Diakon":
    * M: Ty sobie myślisz że wszyscy się zgadzają, nie. Nie wszyscy Verlenowie to mokre kluseczki. Niektórzy chcą być lepsi a nie troszczyć się o ludzi. Ty i ta Twoja księżniczka (Ariannia), Wy z Waszego szczepu - nie zrobicie nic. Nie chcecie wojny domowej.
    * A: Szukaliśmy przynęty na potwora. Z człowieka?
    * M: Myślisz że nie próbowałem? Nie wiem jak złapać tego potwora. Nie wiem jak go zanęcić. Ale to wymyślę. Ten potwór... jest w nim taki zły intelekt. To sadysta. Ten potwór się ze mną bawi. Testuje mnie. (jego odczucia). Nawet sentisiecią nie umiem go znaleźć!!!
* X: Mściząb jest zacietrzewiony. Chce się wykazać, wszystko prawie ma, a Arianna i Viorika mu nie dają. Nie słucha ich, bo nie wierzy w ich dobre intencje.
    * A: Co z Ciebie za Verlen skoro trzeba Ci dawać?
        * okazuje się, że on nie chce by mu dać, ale potrzebuje okazji. No ale teraz miał - i spieprzył
* X: Mściząb z całej tej akcji uznał, że jest za słaby. Że jego plan i podejście są dobre, ale jego skille są za niskie. Mściząb jest przekonany, że wygrał z Vioriką i Arianną i może zostać przynętą na potwora i z nim walczyć.

Arianna zdobyła informacje z sentisieci:

* info Arianna x sentisieć
    * TAK, są osoby, które przez pewien czas były niedostępne dzięki sentisieci. Łącznie z OBOMA rodzicami dzieciaka. I - co ciekawe - sentisieć mówi, że rodzice byli w domu gdy chłopak popełnił samobójstwo
    * chronologia
        * WPIERW znika ojciec. I WTEDY po raz pierwszy na terenie pojawia się sygnał Emmanuelle.
        * POTEM przybywa Mściząb.
        * POTEM wychodzą Jaszczury.
        * POTEM pojawia się Łowca.
        * Samobójstwo jest jeszcze później.

Arianna i Viorika chcą przesłuchać Emmanuelle ("Maćka") przy dowódcy Czarnych Kłów. Dowódca Viorice wzbudził pozytywne wrażenie. Koleś swoje walczył, swoje robił, ale jego mag go nie słucha. Nazywa się Stefan. Stefan Farepacz.

Emmanuelle udaje dalej Maćka. Viorika w końcu mówi "Emmanuelle Gęsiawiec. Żądam wyjaśnień."

Tr Z (Verlenki dwie + Verlenterror) +3:

* X: Mściząb przyszedł szybko "jak zachowujecie się wobec moich ludzi"
* X: Viorika go plaskaczuje na "ci ludzie są moi". Viorika sprowadza go na ziemię. Dowódca wyciąga broń i celuje w Viorikę. Emmanuelle też (ale ona z przekonaniem).
* Or: Arianna jest wściekła. Mag WŁASNEGO rodu zachowuje się jak skończony patison. Sentisieć unieruchamia wszystkich (Mścizęba, Stefana, Emmanuelle) i ich rani lekko (wściekłość Arianny). Wszyscy są unieruchomieni, zagrożenie oddalone.

Emmanuelle się ukrywa, ale nic to nie daje - Arianna wzywa ją do raportu i przesłuchuje. Na miejscu się okazuje, że Mściząb wszystko wie, ale oczekuje więcej od Emmanuelle. Nie wiedział, że to ona zrobiła to co zrobiła. Nie wiedział, że to Emmanuelle chciała by dobrze wyszedł. Powiedział, że nie chce mieć z nią już nic wspólnego. Emmanuelle ma złamane serce. Arianna i Viorika chcą ją przechwycić do innych oddziałów Verlenów - dziewczyna jest sensowna, ale musi mieć dobre sterowanie, bo tak jak jest jest kiepsko. A przecież się wkradła, weszła itp.

Wiedziała o tym człowieku (ojcu Fryderyka). On był jej przewodnikiem. Podejrzewała, że coś mu się stało, ale nie miała jak tego zgłosić, bo przecież Mściząb by spytał skąd Emmanuelle wie. A ona nie wiedziała, że on wie że ona jest magiem i kobietą, więc...

...yeah.

Arianna chce mu dokopać. Jak się uda to rozwiązać, przypiszemy mu wszystkie zasługi. O. I wie że jego zasługi są dzięki Ariannie.

## Streszczenie

Opiekując się Eleną, Arianna jest wyczulona na sygnały Esuriit przekazywane przez sentisieć, nawet 'wyciszone', więc A+V ruszyły do Arachnoziem. Tam się okazuje że Mściząb Verlen zrobił blokadę informacyjną bo chce pokonać potwora który tam jest bez pomocy; tyle, że zginęło już kilku ludzi m.in. flarą Esuriit 17-latek. Za wszystkim stoi zakochana w Mścizębie Emmanuelle, która jest w jego oddziale i udaje kogoś innego - ona doprowadziła do rozognienia Tunelowych Jaszczurów. Ale jak Jaszczurów zabrakło, na powierzchnię wyszło Coś Innego, potworny łowca polujący na ludzi...

## Progresja

* .

## Zasługi

* Arianna Verlen: wyczuła ukrywane Esuriit w Arachnoziem; gdy Mściząb walczył z Vioriką, attunowała się do sentisieci i poznała prawdę co się tu dzieje. Odkryła obecność Emmanuelle. "Dobry glina" gadając z Emmanuelle.
* Viorika Verlen: gdy Mściząb (17) ją zaczepił, poszła z nim na ostro i zdewastowała w walce. Opierniczyła Mścizęba i próbowała pokazać mu zniszczenia jakie powoduje. "Zły glina" gadając z Emmanuelle.
* Mściząb Verlen: 17 lat, sfrustrowany że nie może pokonać Łowcy. Zablokował możliwość wezwania pomocy do Arachnoziem mimo ofiar; po zniszczeniu Jaszczurów Tunelowych nie radzi sobie z Łowcą.
* Emmanuelle Gęsiawiec: z pomocą innego Verlena zaaranżowała wyjście Jaszczurów Tunelowych w Arachnoziem by Mściząb mógł je zabić i zostać bohaterem. All for love. Niestety, robiąc to wyciągnęła na powierzchnię Łowcę. Ukrywała się jako 'Maciek' - gdy Arianna ją skonfrontowała, prawda wyszła na jaw i Mściząb się jej wyparł.
* Atraksjusz Verlen: wspierał Emmanuelle Kendrapit w jej próbach sprawienia, by Mściząb był odpowiednio doceniony. Spodobało mu się podejście młodej czarodziejki. Poradzi sobie i się wzmocni lub utonie. Nieobecny na sesji, działa w tle.

## Frakcje

* Czarne Kły Mścizęba: mało istotny oddział mało istotnego Verlena; zniszczył arachnoziemskie jaszczury i nie jest w stanie powstrzymać tego, co wychodzi spod ziemi. Okazuje się, że czarodziejka Emmanuelle Gęsiawiec zinfiltrowała ten oddział chcąc być bliżej kochanego Mścizęba.
* Krwawe Ciernie Verlenów: umożliwili Emmanuelle z Czarnych Kłów zbliżenie Mścizęba do jego legendy i nowego imienia. Dali jej sposób na wywabienie arachnoziemskich jaszczurów i zbliżyli się o krok do stworzenia Wielkiej Areny.
* Potwory Arachnoziemskie: po stracie jaszczurów tunelowych które ekologicznie chroniły 'wyjścia' z kopalni, spawnowały bardzo niebezpieczne potwory które zaatakowały Arachnoziem.

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                           1. Arachnoziem: słynie ze złóż minerałów, niestabilności i wyjątkowo podłych podziemnych potworów; ma garnizon 50 żołnierzy na 20k populacji rozsianych po górzystym terenie.
                                1. Kopalnia
                                1. Bar Łeb Jaszczura

## Czas

* Opóźnienie: 11
* Dni: 2


