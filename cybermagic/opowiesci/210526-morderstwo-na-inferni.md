## Metadane

* title: "Morderstwo na Inferni"
* threads: legenda-arianny
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [231018 - Anomalne awarie Athamarein](231018-anomalne-awarie-athamarein)

### Chronologiczna

* [231018 - Anomalne awarie Athamarein](231018-anomalne-awarie-athamarein)

### Plan sesji
#### Co się wydarzyło

AKT ZBRODNI:

* Jeden z noktian, Tal Marczak, chciał zniszczyć OO "Tivr".
    * Tivr kiedyś był statkiem noktiańskim na którym Aries i jego przyjaciele służyli (Aries Tal)
    * Tal Marczak miał umiejętności (mechanik Inferni), wiedział jak Talaries działa i chciał go sabotować
* Sześciu noktian Inferni zwabiło Tala do ciemnych zakątków K1 i go zabili
* Noktianie Inferni wzięli amnestyki; mieli do nich dostęp dzięki Martynowi.
    * Martyn uaktualnił medikit i dodał memo.

WYKRYCIE:

* Paladyni Verlenów wykryli nieobecność Tala Marczaka na K1. Jest to noktianin? Uciekł?

PODEJRZENIE: 

* Ci sami ludzie którzy stoją za miragentem i za Sebastianem Alariusem stoją za zabiciem Tala.
    * Podejrzane są siły Komodora Walronda, pod admirałem Szradmannem.
    * admirał Rupert Szradmann ma wartości (Potęga, Konformizm, Bezpieczeństwo). Chce ufortyfikować Orbiter i go wzmocnić.
        * siły Szradmanna patrzą BARDZO nieufnie na wszystko i wszystkich kto nie trzyma się zasad wojskowych i "starego Orbitera"

Frakcje:

* Paladyni Verlenów: (tradycja, bezpieczeństwo, samosterowność)
* Noktianie: (osiągnięcia, bezpieczeństwo, konformizm)
* siły śledcze Kramera (dobroczynność, bezpieczeństwo, samosterowność)
    * insp. Paweł Pasierb
* siły śledcze Szradmanna
    * insp. Renata Kleszcz, zimna jak stal. Nie wierzy w winę sił Szradmanna, a już na pewno nie ludzi Walronda
* komodor Walrond
    * ma w historii nienawiść do noktian, odsunięto go od 
    * Tivr miała być jego korwetą

#### Sukces graczy

* Nie będzie problemów i komplikacji dla Inferni
* Nie będzie konfliktów Szradmann - Arianna
* Nie ma zaognienia tematów noktiańskich

### Sesja właściwa

#### Scena Zero - impl

brak

#### Scena Właściwa - impl

2-3 dni debriefingów, poważnych tematów po ostatniej akcji...

Pewnego pięknego wieczoru sierżant Otto, paladyn Verlenów odwiedza Ariannę. Jest cały ślicznie w mundurze, wszystko perfekcyjnie (ma wytatuowany herb Verlenów na sercu). I chce pogadać o jednym członku załogu Inferni. Niejaki Tal Marczak nie wrócił na czas. W momencie, w którym Infernia ma dawać Przykład We Flocie Tala nie ma.

Arianna -> Eustachy -> Elena -> Klaudia. Czyli perfekcyjny łańcuch dowodzenia. Gdzie jest Tal?

"Musiał coś solidnego zbroić, bo nie wiedziałam, że Eustachy ma pojęcie że mamy takiego inżyniera na pokładzie Inferni..."

Klaudia wchodzi w rekordy Marczaka, szuka lokatora / komunikatora i szuka gdzie jest łapczak. Co ciekawe, od DWÓCH DNI nie opuścił Inferni. Która jest w remoncie. Gdzie go na pewno nie ma. Elena jest bardzo zdziwiona - szuka go w Inferni i ofc nie znalazła.

Wczoraj Tal odpiął wieczorem wszystkie lokatory, komunikatory i wyszedł. I od tej pory go tam nie było. Incydent.

Klaudia z radością zalogowała nowy Incydent. Teraz Arianna ma Oficjalny Incydent. I to zgłoszony przez Klaudię...

Potem Klaudia łączy Talowe: keycardę, kamery i szuka. Gdzie on jest. A tymczasem Otto delikatnie zwrócił uwagę Ariannie na to, że Martyn kazał uaktualnić apteczkę o amnestyki. Arianna powiedziała, że to jest na jej żądanie (nieprawdziwie, ale OK). Otto jest usatysfakcjonowany i odejdzie jak coś.

Eustachy szuka Marczaka -> Leona -> Klaudia. Klaudia się wkurzyła.

Klaudia szuka publicznych rekordów, rejestr przejść, żąda kodu oficerskiego od Eustachego. Eustachy pyta Ariannę...

* "Miałeś pogadać z technikami. Czemu zajmuje się tym KLAUDIA?!" - A
* "Poszerzam sektor poszukiwań by znaleźć go szybciej" - E
* "Gadałeś z tymi technikami?" - A
* "No Klaudii się nudzi i potrzebuje ćwiczeń z kontrwywiadu i by przyspieszyć musi mieć kod oficerski a nie dam jej mojego więc możesz wygenerować jednorazowy?" - E
* "Czemu masz problem z daniem kodu?"
* "Bo to mój prywatny. A to jest Klaudia... i nie chcę za 3 miesiące być odprowadzony przez smutnych panów bo połowa tajnych dysków jest wyczyszczona, sprzedana i nie wiadomo gdzie. No i procedury!" - E
* "To idź do niej i ten kod wpisz." - A
* "Nie bo będzie się patrzeć na klawiaturę jak będę wpisywał. TO JEST KLAUDIA! Spec of komputerów! Musiałbym kompa potem wysadzić!" - E
* "Zapoznaj się z załącznikiem wniosków formalnych i zrób ten wniosek" - A

Eustachy przerzucił temat do Eleny. Elena zaskoczona że ONA ma zrobić wniosek formalny. Zajmie się tym...

Elena pyta Ariannę czemu wszystko oficjalne. A powiedziała, że chodzi o to by Eustachy coś z dokumentami. Elena powiedziała, że to Leona ma do niego sprawę osobistą i go szuka (Leona powiedziała osobistą bo inaczej przecież by Elena nie pomogła jakby wiedziała że Leona może go bić). Arianna uznała, że faktycznie JEŚLI KOLEŚ MA COŚ Z LEONĄ to nic dziwnego że zniknął... Arianna sama skomunikowała się z Klaudią...

Klaudia zajęta poszukiwaniami ma wiadomość od Arianny. Arianna ma dla niej kody oficerskie (by spowolnić Leonę). Arianna udzieliła. Szybciej niech znajdzie niż Leona...

* "Słuchaj, co on zrobił?" - Klaudia
* "Nie mam pojęcia, ale miał personalny zatarg z Leoną" - Arianna
* "Ale najpierw Elena, potem Eustachy... dobra, nieważne. Daj kody" - Klaudia

Klaudia dzielnie szuka.

* X: rutynowy check sił Orbitera. Klaudia pokazała - nie ma tematu. 
* V: Klaudia znalazła ostatnią lokalizację. Wchodził do czarnych sektorów K1.
* X: odraczam
* V: Klaudia ma DOWÓD, że wczoraj wszedł do czarnych sektorów i nie wyszedł od tej pory. On serio zniknął.

Klaudia ustawia autoryzowane czujki autoryzowane kodem Arianny i zgłasza to Ariannie. I informuje Elenę. Szczęśliwie, poszedł do miejsca dość odizolowanego i w miarę bezpiecznego. Elena przygotowuje ekspedycję składającą się z jednej Eleny i dostaje wiadomość od Arianny. Ma dodać do ekspedycji Eustachego. A jako przyzwoitkę - Persefona. Elena wzięła servar, Eustachy też - i wymarsz.

Zadanie jest stosunkowo nietrudne, bo to bezpieczniejsze tereny K1.

* V: sukces. Znaleźliście go. Sęk w tym, że jest martwy. Zmiażdżony przez jakieś żelastwo...

Eustachy ostrzega Ariannę. Mają problem. Pierwsze co - Arianna do Leony.

* "Co to za sprawa osobista?" - Arianna
* "Nie chcę o tym mówić" - Leona

Klaudia dostaje zapytanie z Admiralicji. Młodszy służbista, który od niedawna ma pracę. Karol Reichard. Zwraca się do Klaudii, bo NIE WOLNO zamykać kolesia (Tala) na Inferni gdy jest reperacja i naprawa. A lokator pokazuje, że on tam jest. To nieprawidłowe i się nie godzi. Klaudia wyciąga kodeksy, regulaminy... ale najpierw mówi, że szukają go bo go szukają. Najpewniej uciekł na dziewczynki. Klaudia i Karol temat tymczasowo zamknęli. A Klaudia **wie** że Admiralicja K1 wie o zniknięciu Tala.

Arianna zestawiła połączenie: Klaudia - Eustachy - Arianna. Bo sytuacja przestała być zabawna.

* "To co? Do śluzy chuja i udajemy, że zdezerterował?" - Eustachy
* "Zdezerterował na tamten świat?" - Arianna

Arianna zauważyła, że pójście bez kombinezonu do ciemnej strefy to JEST forma samobójstwa. Eustachy uznał, że wie, co robi ze sobą. Pyta czy ma sfabrykować / zamaskować dowody? Zauważył, że kto jak kto ale on umie sfabrykować dowody. Arianna chce wiedzieć co go położyło. Martyn jest potrzebny. Elena przyprowadza Martyna w kombinezonie i stroju.

Martyn robi autopsję. TrZ+2

* V: Martyn dał informację. Zabójstwo z premedytacją. Co najmniej dwie osoby. Jeden trzymał, drugi ściągał żelastwo.
* V: Z Martynem doszliście do tego, że wykorzystane były dwa servary klasy Lancer (najbardziej popularne na K1).
* V: Zrobiły to osoby sprawne w używaniu Lancerów lub ich analogów, ale niekompetentne w maskowaniu śladów. Ani jednego medyka. Co najmniej jedna osoba znała się na strukturach statku. Co najmniej jedna osoba była inżynierem. Lancery zostały w środku wypalone. Tak jakby spodziewali się badania przez maga.

Arianna ma coś ciekawego w rekordach - Tal Marczak był kiedyś noktianinem. Wzięty z planety. Skończył tu, zdeptany przez amatorów. To nie była profesjonalna śmierć.

Eustachy będzie gadał ze swoimi podkomendnymi. Klaudia szuka ludzi wchodzących w ten region. Jak największą ilość ludzi. I niech sprawdza kanały techniczne.

Jak chodzi o WEJŚCIE servarów, dwa Lancery znalazła Elena. Zostały tam. Są skutecznie uszkodzone - Elainki są zepsute, banki danych poniszczone i są uszkodzone też od broni. Jakby Lancer strzelał do Lancera. Elena znalazła w sumie pięć Lancerów, wszystkie uszkodzone w podobny sposób. 

Klaudia używa kodów Arianny, crosschecków, maintenance, selekcjonuje tylko te osoby które NA PEWNO mają sens i możliwości. I osoby które "weszły wcześnie i wiemy kiedy mogły wejść i kiedy on wyjść".

ExZ+3:

* V: Lancery i ogólnie sprzęt należą do ludzi komodora Walronda. Walrond ma duże wonty do noktian. Jego zdaniem integracja to błąd itp. Walrond wykorzystywał te sektory jako miejsce na paintball. Następna ich akcja ma być jutro. Klaudia widzi wejścia i wyjścia. Nie ma wskazań, że to ludzie Walronda. Tak jakby ktoś próbował ich wrobić.

Eustachy równolegle gada ze swoimi technikami. Czy coś widzieli, coś wiedzą itp. Miał wypadek i szuka sprawców. TrZ+2

* XX: Eustachy dał radę doprowadzić do paniki. Ktoś poluje na członków Inferni na K1. Na razie "zewrzeć szyki".
* V: Jak tylko Tivr wylądował, Tal był zestresowany. Wkurzony. Łaził za tym Tivrem. Oglądał go. Niektórzy członkowie go "stary, daj spokój, nie idź tam".
    * Tivr wygląda trochę na projekt noktiański.

Na podstawie tego co Eustachy się dowiedział Klaudia zawęziła informacje.

* V: Zabójcy bardzo dobrze rozeznali teren. Dokładnie tak, jak Eustachy to robi. To nie są randomy - to osoby uczone w sabotażu i działaniu pod ogromną presją. "Komandosi wśród inżynierów". Typ załogi na statkach typu Infernia.

Infernia ma Lancery na pokładzie. Dokładnie cztery.

Arianna -> Kamil. Co tu się dzieje. Wpierw - jak noktianie w Inferni. Kamil jest rozczarowany. Nie przyjęli kultu Arianny, ale pragmatycznie Ariannę wspierają. Są lojalni Ariannie i Inferni. Infernia jest dla nich "drugą Noctis". Noktianie trzymają się razem, na uboczu. Nie skarżą się. Nie rozwiązują tematów oficjalnie, robią wszystko sami. Ci noktianie co są z planety zostali zintegrowani z resztą noktian. Nie zintegrowali się z "Infernią" a z "noktianami na Inferni". Ale są lojalni. Tal Marczak też wyglądał na to, że jest lojalny - przybył z planety, czyli jest na Inferni od niedawna.

Jako, że Arianna pozwala im zachować kulturę i niezależność, biorą na siebie trudne zadania. Nie przez przypadek Eustachy ma sześciu inżynierów noktiańskich. Teraz już pięciu.

Tal był z planety. To ci co ich Arianna uratowała.

ExZ+3.

* XX: Kultura typu "Infernia vs reszta świata". Ktoś na nich poluje. Tylko Infernia może robić to dobrze. Arianna chroni swoich przeciwko Orbiterowi.
* X: noktanie mają tak "własne podejście" i tak "własną kulturę" że nawet Kamil i Arianna nie są w stanie poznać co tam się dzieje w tej małej grupce Inferni.

Eustachy uzbrojony w nową wiedzę, szuka:

* V: noktianie byli tymi co separowali Tala od Tivra. Na pewno Eustachy ma dowody, że kazali mu "to zostawić". Inni inżynierowie. Inni noktianie z innych grup. Jest jakaś relacja z przeszłości między Talem i Tivrem.
* X: to wszystko trwa ogromną ilość czasu. Powoli będą siły Walronda miały swoje ćwiczenia. A servary uszkodzone plus jest tam trup.
* X: interwencja Kramera jest niezbędna. Walrondowcy robią syf.

Arianna i Kramer. Kramer jest zadowolony - Termia pochwaliła Ariannę, bo "jego mała buntowniczka ma nie tylko ząbki ale i śliczną główkę i umie nią ruszać" za akcję z memetyką.

Arianna powiedziała, że tarcia z przyjęciem noktian do załogi są problematyczne. Ktoś jest w to zamieszany. I ktoś zginął. Walrond jest potencjalnym podejrzanym lub kimś do wrobienia. Kramer potwierdził - Walrond jest dowodem mowy nienawiści przeciw noktianom, ale nie jest głupi.

Kramer zatrzyma ćwiczenia Walronda. Jak Arianna będzie coś wiedzieć, Kramer chce wiedzieć.

KRAMER KUPIŁ CZAS. Sporo czasu. Arianna szybko skontaktowała się z Marianem Tosanem i spytała czemu Tivr. Tivr kiedyś nazywał się "Aries Tal", był pojazdem noktiańskim. Dumni byli z tego statku, bo "Aries Tal" był "cruiser-killerem", ogromna duma Noctis.

Eustachy podsumowuje: osoby które mają wiedzę o kanałach, osoby które nie są dobre w walce servarami, osoby które mają wiedzę odnośnie działań jak na Inferni. Osoby które mapowały te kanały. Osoby znające Tala w jakimkolwiek stopniu. Plus Klaudia zauważyła - "Tal Marczak" i "Aries Tal". Tal ma korelację z nazwą korwety?

Zgodnie z plikami "Tal Marczak" nazywał się "Alrund Tal" przed integracją. Czyli jak był złapany to nazywał się "Alrund Tal" i został zesłany na planetę. Potem dopiero by się zintegrować wziął sobie imię Tal Marczak. By go nie rozpoznano? Był kiedyś głównym mechanikiem Tivra. Tivr należał do jego rodziny. To był statek usprawniany przez pokolenia. Wszyscy na tej jednostce byli jakoś połączeni z jego rodziną. Jeńcy itp.

Arianna zauważyła, że Tivr został całkowicie przebudowany z "Aries Tal". To zupełnie inny statek, ale na strukturze oryginału. Jest to, jak zresztą Marian powiedział, świetna jednostka. Orbiter splugawił mechanizmy noktiańskie - nie chcieli dużo zmieniać. Ten statek był dziełem sztuki który latał i strzelał.

Klaudia dodaje: "wiedzą o ćwiczeniach Walronda". Mechanicy Walronda też.

* XXX: sprawa zrobiła się solidna. Nie będzie dało się tego serio ukryć i zamaskować. Teraz Admiralicja żąda krwi.
* V: Klaudia znalazła osoby. Czterech członków Inferni. Dwaj to mechanicy.

Arianna ich przesłuchuje. Nic nie wiedzą.

Martyn robi przegląd apteczek. Nie ma amnestyków. Nie ma w DUŻEJ ilości apteczek amnestyków. Martyn potwierdził, że mieli do czynienia z amnestykami. Czyli macie morderców.

Teoria Klaudii:

* Tal chciał narozrabiać z Tivrem.
* Zrobili sprawę wewnętrznie, wyczyścili kolesia.
* Chcą wrobić kogoś kto nie lubi noktian.
* Jak się wyratować? Amnestyki.

Arianna idzie z problemem do Kramera. Noktianie załatwili to w ten sposób bo nie chcieli by inni noktianie ucierpieli. Zdaniem Kramera OCZYWISTYM JEST, że jeśli Walrond zabije noktianina, to zrobi Admiralicja im z dupy jesień średniowiecza. Arianna zaznaczyła, że noktianie trzymają się razem i Infernia jest dla nich dość unikalną jednostką. Biorą do siebie te złe informacje. Arianna chciałaby swoim noktianom pokazać, że mogą jej zaufać. I dlatego potrzebuje zwolnienia tej czwórki. Dyskretnego.

Arianna nie chce, by poszła wiadomość, że noktianie zabijają nawet swoich...

Trzeba ich przenieść. Trzeba ich ukarać. Ale dyskretnie.

ExZ+3:

* V: Kramer ma pomysł. Przeniesie tą czwórkę na Żelazko. Karnie. Infernia robiła ćwiczenia i podczas ćwiczeń nieszczęśliwy wypadek. Więc osoby które nie dopilnowały które zawaliły są przeniesione na Żelazko by nauczyć się dyscypliny. A Arianna ma okazję przemówić do załogi i powiedzieć im co wynegocjowała i jak jest wynik. Arianna trochę zapłaci reputacyjnie, bo na jej ćwiczeniach ktoś zginął.

Arianna przemawia do noktian na Inferni.

* 8: X
* przygarnęła ich i daje im żyć: 2V
* wynegocjowała z Kramerem rozwiązanie: 3V
* konsekwentnie stoi po stronie noktian: 3V
* nawet amnestyki im nie pomogły przeciwko K1: 2V

"Po tym jak poprosiłam Kramera by część załogi -> noktianie 3 raju, krok łączący astorian z noktianami. Mając was blisko siebie mogę was wspierać, pomóc, pokazać astorianom że noktianom można ufać. Nie było mnie 5 dni, wracam i okazuje się, że całe zaufanie się posypało. Co mam myśleć dowiadując się, że próbując chronić siebie nawzajem, trzymając się razem poświęciliście przyjaciela. Nic nie powiedzieliście, nikomu. Gdybyście powiedzieli, nawet jak mi nie ufacie... może są inni członkowie załogi którym można zaufać? Może oni mogą pomóc, przemówić? Może gdybyśmy wiedzieli jaka jest natura konfliktu, dałoby się wspomóc nieszczęsnego Tala. A tak, nie żyje. Cieszę się, że udało mi się z Kramerem. Że doszliśmy do porozumienia i sprawa przejdzie bez echa. Przeniesiemy ich na Żelazko za niedopatrzenie na czas ćwiczeń. Z mojej winy. Bo ja nie dopilnowałam moich ludzi by zachowywali się jak należy. Powinniście się zastanowić - czy to jest tak jak powinno być. Czy powinnam was zacząć monitorować jak na innych statkach? Dałam wam zaufanie, a nie jesteście skutecznie w stanie zajmować się sobą sami. Chciałabym byście to przemyśleli i dali mi odpowiedź w ciągu następnych dni. I niech to będzie dobra odpowiedź."

* mowa Arianny: 3V

WYNIK:

* X: noktianie na razie trzymają się razem.
* V: w takich wypadkach które mogą wpłynąć na Ariannę czy Infernię nie będą robili rzeczy sami.

## Streszczenie

Noktiański mechanik Inferni, Tal Marczak, chciał zniszczyć OO Tivr (własność noktiańskiej rodziny Tala). Noktianie trzymają się razem - inni mechanicy Inferni doprowadzili do morderstwa Tala na K1 i wzięli amnestyki, by nikt nie wiedział co się stało. Zespół Inferni skutecznie doszedł do tego o co chodzi, odpowiedzialnych za morderstwo noktian przeniesiono na Żelazko a Arianna poważnie opieprzyła swoją noktiańską załogę. Będzie lepiej w przyszłości.

## Progresja

* OO Infernia: w apteczkach Inferni wszyscy mają pomniejsze amnestyki, uaktualnione z rozkazu Martyna Hiwassera (po "Osiemnastu Oczach").
* OO Infernia: załoga jest nieufna wobec K1 i Orbitera. Zwiera szyki przeciwko "obcym". Tylko Arianna i "nasi" z Inferni. A noktianie trzymają się tylko razem.

### Frakcji

* .

## Zasługi

* Arianna Verlen: próbowała utrzymać morale wśród noktian; wynegocjowała z adm. Kramerem, żeby noktian zamieszanych w morderstwo przeniósł na Żelazko a nie wysłał na ścięcie czy coś.
* Eustachy Korkoran: chciał leniwie zrzucić poszukiwania Tala na wszystkich innych, ale jak Tal zginął to wyciągnął kluczowe informacje rozmawiając ze swoimi technikami.
* Klaudia Stryk: szukała Tala Marczaka. Używając kodów Arianny, wiedzy Arianny i Eustachego i dużej ilości cross-sekcji znalazła winnych jego morderstwa. Mistrzostwo w agregowaniu danych i dokumentów.
* Kamil Lyraczek: mimo swoich wybitnych umiejętności ludzkich, inspiracji i perswazji nawet jemu nie udało się dotrzeć do noktian na Inferni. Wie o nich trochę, ale nie jest blisko i nie zna sekretów.
* Otto Azgorn: sierżant paladynów Verlen; ma wytatuowany herb Verlenów na sercu. Zauważył i zgłosił Ariannie, że technik Inferni (Tal Marczak) zniknął. Zdezerterował? Arianna powiedziała, że się tym zajmie.
* Tal Marczak: noktiański ludzki inżynier na Inferni. Kiedyś: Alrund Tal, silnie powiązany z eks-noktiańską korwetą Tivr (kiedyś: Aries Tal). Chciał wysadzić Tivr, więc mechanicy noktiańscy go zabili, by nie było kłopotów.
* Leona Astrienko: włączona w sprawę poszukiwania Tala Marczaka przez Eustachego zaczęła go szukać bo "sprawa osobista". I nagle wszyscy zaczęli Tala szukać, by uratować go przed Leoną XD.
* Elena Verlen: Eustachy poprosił ją by znalazła Tala Marczaka i przygotowała wnioski formalne. Elena rozmawiając z Klaudią i Arianną próbowała wyprostować to szaleństwo - PO CO TO WSZYSTKO. 
* Karol Reichard: Młodszy służbista z admiralicji; zażądał od Klaudii uwolnienie Tala Marczaka z Inferni bo nie wolno go zamykać (a tak pokazują systemy). Zneutralizowany przez Klaudię - szukają go, poszedł na dziewczynki.
* Martyn Hiwasser: kazał uaktualnić apteczkę Inferni o amnestyki; potem zrobił autopsję Tala Marczaka. I wykrył brak obecności amnestyków w noktiańskich apteczkach na Inferni.
* Feliks Walrond: komodor Orbitera (w linii Szradmanna); ma duże wonty do noktian. Jego zdaniem integracja z noktianami to błąd itp. Wykorzystywał czarne sektory do ćwiczeń Lancerami.
* Antoni Kramer: zatrzymał ćwiczenia komodora Walronda, by nie wyszło że noktianie zabili swojego na K1. Potem przeniósł winnych noktian na Żelazko dla Arianny - by nie ucierpieli za mocno a kara by była.
* Marian Tosen: opowiedział o przeszłości OO Tivr; akurat do tego miał dostęp dzięki uprawnieniom adm. Termii.
* OO Tivr: kiedyś: noktiański statek usprawniany przez pokolenia ("Aries Tal"). Wszyscy na tej jednostce byli jakoś połączeni z rodziną Tal. Orbiter splugawił komponenty XD. Atm w rękach komodora Walronda z Orbitera.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy

## Czas

* Opóźnienie: 4
* Dni: 6

## Inne

.

## Konflikty

* 1 - Klaudia szuka informacji gdzie znajduje się Tal Marczak na K1, używając kamer i lokatorów
    * TrZ+2
    * XVXV: Wchodził do czarnych sektorów K1, nie wyszedł od tej pory. Klaudia wie w którym obszarze zniknął.
* 2 - Elena robi wymarsz i przeszukanie Czarnych Sektorów pod kątem Tala. Przeszukiwanie z zespołem.
    * TpZ+2
    * V: sukces, znaleźli ciało
* 3 - Martyn robi autopsję; próbuje dowiedzieć się jak Tal zginął i maksymalne okoliczności jego śmierci
    * TrZ+2
    * VVV: morderstwo z premedytacją, osoby kompetentne w Lancerach ale nie w zacieraniu śladów
* 4 - Klaudia używa kodów Arianny, crosschecków, maintenance, selekcjonuje tylko te osoby które NA PEWNO mają sens i możliwości. Kto to MÓGŁ być?
    * ExZ+3
    * V: Lancery i ogólnie sprzęt należą do ludzi komodora Walronda. Ktoś próbuje Walronda wrobić.
    * V: Zabójcy bardzo dobrze rozeznali teren. Dokładnie tak, jak Eustachy to robi. "Komandosi wśród inżynierów". (po gadaniu Eustachego #5)
* 5 - Eustachy równolegle gada ze swoimi technikami. Czy coś widzieli, coś wiedzą itp. Miał wypadek i szuka sprawców. 
    * TrZ+2
    * XXV: Ktoś poluje na członków Inferni na K1, zwierają szyki. Tal jest powiązany z Tivrem a Tivr wygląda na projekt noktiański.
* 6 - Kamil próbuje dowiedzieć się od noktian odnośnie Tala, odnośnie całej tej sytuacji.
    * ExZ+3.
    * XXX: noktianie mają kulturę typu "my vs reszta świata". Nawet Kamil nic z nich nie wyciągnie. Noktianie trzymają się tylko noktian.
* 7 - Eustachy dociska swoich techników. Nie tylko noktiańskich. Ktoś coś wie?
    * TrZ+2
    * VXX: noktianie separowali Tala od Tivra. Kazali mu "to zostawić". Inni inżynierowie. Ale to trwa MNÓSTWO czasu - trzeba zatrzymać ćwiczenia Walronda.
* 8 - Klaudia mając informacje od Eustachego, Arianny i wiedzy o Talu szuka pod kątem mechaników noktiańskich
    * ExZ+3
    * XXXV: Admiralicja żąda krwi. A Klaudia ma info kto to - znalazła czterech noktiańskich techników na Inferni.
* 9 - Arianna prosi Kramera o to, by nie krzywdził tych noktian - nie chce rozognić wojny noktianie - astorianie. Muszą jej zaufać a to wymaga "leeway".
    * ExZ+3
    * V: Kramer ma pomysł. Przeniesie na Żelazko.
