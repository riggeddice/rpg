## Metadane

* title: Księżniczka Arianna ratuje dzieci w lesie"
* threads: brak
* motives: energia-sempitus, energia-esuriit, energia-alucis, lojalnosc-syntetycznych-intelektow, gang-oportunistyczny, teren-morderczy, znikajacy-ludzie
* gm: żółw
* players: kić, yobolyin

## Kontynuacja
### Kampanijna

* [240114 - O seksbotach i Syntetycznych Intelektach](240114-o-seksbotach-i-syntetycznych-intelektach)
* [240306 - Potworność powojenna Trzykwiatu](240306-potwornosc-powojenna-trzykwiatu)

### Chronologiczna

* [240114 - O seksbotach i Syntetycznych Intelektach](240114-o-seksbotach-i-syntetycznych-intelektach)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * Tardigrade Inferno 'Little Princess'
    * "Little princess in the forest | With so strange hypnotic eyes | She will reverse the fairy tale | And in the end, she’ll have to die"
    * -> Wprowadzamy koncept 'Arianny' jako bajki, ratującej dzieci przed strasznym losem od lokalnej grupy przestępczej.
* Opowieść o (Theme and vision): 
    * "Anomalia pochodząca z bajki trafia do jedynej istoty która potrafi ją odczytać przez rezonans podobnych sytuacji."
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * energia-sempitus: W formie 'Księżniczki Arianny', postaci z bajki unifikującej ludzi i pomagającej im. The Immortal Toy. 'Przetrwasz, skarbie'. ALE TEŻ: noktiańskie maszyny bojowe polujące na zagrożenia.
    * energia-esuriit: Dostarczana przez Czółenko; zasila 'Księżniczkę Ariannę' i przez nią jest konwertowana w energię zasilającą zniszczone maszyny noktiańskie i dzieci.
    * energia-alucis: Zasilająca 'Księżniczkę Ariannę' przez czarodziejkę animującą - Kaellę Sarimanis. 'Arianna' kontaktuje się z Teresą i ściąga ludzi iluzjami w świat zguby.
    * lojalnosc-syntetycznych-intelektow: Zniszczone maszyny noktiańskie reanimowane przez 'Księżniczkę Ariannę' - chronią dzieci i zapewniają bezpieczeństwo noktianom i tylko im.
    * gang-oportunistyczny: grupka osób polujących na dzieci na sprzedaż. 'Księżniczka Arianna' ratuje dzieci od ich złych czynów.
    * teren-morderczy: Las pełen anomalii, potworów, groźnych insektów oraz zniszczonych urządzeń noktiańskich
    * znikajacy-ludzie: działanie gangu polującego na ludzi na sprzedaż (zwłaszcza dzieci).
* Detale
    * Crisis source: 
        * HEART: "Polowanie na dzieci na sprzedaż buduje w protomagu próbę czegoś do obrony - Księżniczka Arianna"
        * VISIBLE: "Dziwne zachowanie Teresy"
    * Delta future
        * DARK FUTURE:
            * chain 1: DZIECI: nie uratujemy, Księżniczka pożre człowiek
            * chain 2: ŹLI LUDZIE: uciekną, porwą dzieci
        * LIGHT FUTURE: 
            * chain 1: DZIECI: uratujemy je
            * chain 2: ŹLI LUDZIE: część uda się jakoś pojmać?
    * Dilemma: brak

### Co się stało i co wiemy

Jest tutaj grupa ludzi - drapieżników - polujących na dzieci. Niekoniecznie są stąd, ale mają całkiem niezły sprzęt i są w stanie infiltrować, porywać i transportować je przez las. Mają w lesie ukryte obozowisko w jednej z fortifarm.

Ci ludzie porwali grupę dzieci w czym jednego protomaga. Kaella (czarodziejka) stworzyła koncept Księżniczki Arianny, jak z bajek Sowińskich. I Księżniczka Arianna wydobyła część dzieci i schowała je w bezpiecznym miejscu pod opieką zniszczonych maszyn i martwych strażników noctis.

Z perspektywy Zespołu, wszystko zaczyna się od Teresy. Ona jest noktianką, więc docierają do niej sygnały od ‘Księżniczki Arianny’, choć nie ma pojęcia kim jest. Teresa aktywnie szuka źródła sygnału, nie do końca rozumie co się dzieje, ale słyszy głosy swoich rodziców i zna imię zaginiętej dziewczynki – Kaelli, choć nie wie kim ona jest.

A Klaudia i Mariusz nie wiedzą co się dzieje z Teresą, więc ją śledzą.

A Drapieżnicy wiedzą, że w lesie porusza się jedna czarodziejka. Idealna ofiara na sprzedaż.

### Co się stanie (what will happen)

* F1: (Co do cholery robi Teresa? :: START)
    * Teresa chodzi po lesie w nocy (która się lasu ogólnie boi)
    * polowanie na Teresę przez trzech Drapieżników (prawie udane)
    * -> co Teresa wie, ale nie JAK wie
* F2: Trop za śladami Teresy
    * 'maskowane ścigacze w lesie' / 'to na pewno noktiańska mafia'
    * 'strażnicy noctis w lesie'
    * 'lair of the children' / 'obozowisko złych ludzi'
* F3: Ratowanie pozostałych dzieci
* F4: Usunięcie Księżniczki Arianny
* CHAINS
    * Drapieżcy
        * (droga ewakuacyjna, bezpieczna ewakuacja, porwanie części dzieci)
        * (pułapka na Zespół, pułapka na Księżniczkę)
        * (zrzucenie winy na Noctis)
    * Księżniczka Arianna
        * (ściąganie dzieci do zaopiekowania, ściąganie ludzi)
    * Esuriit + Sempitus
        * (zjedzenie części dzieci, zjedzenie Kaelii, zjedzenie wszystkich)
        * (spawnowanie Strażników, budowa miejsca)
        * (ściągnięcie i zjedzenie Drapieżnika)
        * (kreacja istoty Esuriit polującej na coś smacznego i jadalnego)
* Overall
    * stakes
        * relacja z Teresą i AMZ
        * uratowanie jak największej ilości dzieci, pojmanie kogoś z napastników
    * opponent
        * Drapieżnicy
        * Esuriit + Księżniczka Arianna
    * problem
        * dzieci znikają
        * AMZ nie może nic wiedzieć bo wszystko zniknie
        * mówimy o zniszczonych maszynach noktiańskich i nieznanym źródle energii
        * tylko Teresa coś widzi

## Sesja - analiza

### Fiszki

* Teresa Mieralit
    * 16 lat: affinity Interis (negamagia) + Exemplis (lecznicza i wzmacniająca); świetna akrobatka. Nie umie walczyć: flashbacks śmierci rodziców (FREEZE)
* Kaella Sarimanis
    * 13 lat: zniknęła, protomag Noctis która miała więcej szczęścia i od 2 lat była adoptowana. Zakochana w bajce Księżniczki Arianny

#### Strony

.

### Scena Zero - impl

brak

### Sesja Właściwa - impl

Szkoła, uczenie się, trochę pracy, trochę sprzatania... Udało się Mariuszowi zrobić świetną, nowoczesną dronę typu _stealth_. Teresa Mieralit. Teresa się nie integruje, nie będzie gadać, raczej trzyma się siebie. I Teresa wieczorami znika. Opuszcza Akademię. Idzie do lasu. A przecież się boi. A przecież las jest niebezpieczny.

Pierwsze metody działania - można albo się skradać albo puścić nową dronę do testu. I iść z tyłu, by być bliżej 'na wypadek problemu'. DWA. Klaudia mówi, że do jakiegoś projektu trzeba coś gdzieś w nocy. W bezpiecznym terenie - na złomowisku itp. Więc w nocy musi działać.

Tp+3:

* V: Bez żadnego kłopotu Klaudia dała autoryzację dla Mariusza i dla siebie.
* Vr: Tyle nocy ile trzeba aż projekt jest skończony.

Usiedliście sobie na kampusie w chatce i czekacie aż Teresa pójdzie na łowy. A drona śledzi Teresę.

TrZ+3:

* Xz: Teresa nie zobaczy drona, ale Ktoś Inny go zlokalizował.
* Vz: Dron stealth jest dobry. Zadziałało. 
    * I co się okazuje - Teresa, całkowicie nieświadoma, łazi po lesie.
        * Jest ubrana w komiczny strój kamuflażowy i czegoś szuka w lesie. Za czymś idzie
    * Są dwie osoby, też w stroju camo, uzbrojeni, jeden WYRAŹNIE zauważył drona.
* V: Dron przesunął się w miejsce gdzie oni nie mogą go dorwać i to wiedzą.
    * Teresa, dalej nieświadoma sytuacji potknęła się i wpadła do rowu.
    * "Komandosi" patrzą na siebie, na nią, na drona i odstępują od akcji. Po czym się oddalają od drona i Teresy.
* V: Odkryte inne siły
    * Dron zrobił szerokie kółka, badania itp.
    * Jest koleś na ścigaczu i jeszcze jeden ścigacz. Teresa dzielnie idzie w kierunku TAMTEGO ścigacza
* V: Dron zderza się ze 'sztuczną sową', strąca ją na ziemię i 'przydepnie' by nie uciec. Agenci Drugiej Strony - w długą. Pełen abort operacji.
    * Teresa macha ręka, wraca do AMZ. Przedtem biegła chwilę, poślizgnęła się.

Zespół pozyskał dronę (z klatką faradaya) i się wycofał. Teresa nadal nic nie wie.

Klaudia decyduje się pogrzebać w różnych bazach danych, dostępach - czy znikali magowie w ostatnim czasie, nienormalne zniknięcia.

TrZ+3:

* Vr: W ciągu ostatnich 3 tygodni faktycznie jest większa ilość zniknięć w terenach zbliżonych do niezamieszkałych i mających powiązanie z lasem. Nie 'magów', tylko młodych ludzi. 
    * Ludzie znikają w miejscach powiązanych z lasem. Zniknęło koło 16 osób. W 3 tygodnie. Niewpływowi. Zwykle noktianie / sieroty...
    * Osoby które zniknęły są niepowiązane. 'Opportunity'.

Teresa wraca na campus. Ubłocona. Nieszczęśliwa. Mariusz i Klaudia czekają przy wejściu i ją przechwytują. Teresa spodziewa się strażników, ale nie Waszej bezczelności. Widzi Was i lekkie zdziwienie, po czym ramiona i mija Was jakby nigdy nic.

* Klaudia: Wiesz, że to nie jest takie proste? Chodź, pokażę Ci coś?
* Teresa: Nie chcę poznawać Twojego nowego chłopaka.
* Klaudia: Jego już widziałaś, chodź pokażę Ci coś.
* Teresa: Jestem trochę zajęta.
* Klaudia: Byciem celem. Chodź, pokażę.
* Teresa: Ale żadnego trucia bo sobie pójdę.

Spotykacie się w altance na kampusie, strażnicy wiedzą że macie prawo tam być. Teresa patrzy, zdziwiona. Mariusz i Klaudia? Klaudia "bierzesz udział w moim projekcie. Mariusz też".

* Klaudia: Nie wiem po co chodzisz do lasu ale stajesz się celem. (pokazuje wrogą dronę)
* Teresa: (patrzy i nie rozumie).
* Mariusz: Uratowaliśmy Cię w lesie.
* Teresa: Wow. Dzięki. Przed czym? Przed szczypawką?
* Klaudia: (pokazała nagranie z drony)
* Teresa: (mniej kolorów na twarzy, strach) Dzięki.
* Klaudia: Czego szukasz? Może Ci pomóc?
* Teresa: (wyraźnie nie chce mówić) To zabrzmi głupio.
* Mariusz: No... ok, ale wiemy za dużo.
* Teresa: No, słyszałam głosy i szłam za nimi. I za dziewczynką w bieli. (omija temat głosów)
    * (dokładny opis dziewczynki - 14 lat, hannah-montana, tiara, cekiny) : Księżniczka Arianna
    * 6 dni temu po raz pierwszy się spotkałam... widziałam ją. Od tej pory chodzę i szukam.
    * (pierwszy raz - bo słyszałam coś, noktiański głos, proszący o pomoc. Musiałam wyjść zobaczyć)
    * Usłyszałam, że Kaella mnie potrzebuje. Nie wiem kim jest Kaella.
        * (Kaella - jest jedną dziewczynką która zniknęła, 13-latka)
* Teresa: Czy... czy to słyszałam jest prawdą? Lub fikcją?
* Klaudia: Trudniejsze pytanie niż się wydaja.
* Teresa: Dobrze. Czy duchy istnieją?
* Klaudia: W uproszczonej wersji - tak.
* Teresa: Ok... (myśli ciężko)
* Teresa: Wiem, gdzie miałam... w sensie, za każdym razem szłam w podobne miejsce.
    * Głos jej rodziców. Poprosili.

Obiecała, że powie im jak usłyszy znowu JEŚLI nikomu nie powie bez zgody. "Pójdę się przebrać".

Mariusz robi badania magiczne drony. Dokładne informacje, wejście w głąb, itp

TpM+3+3Ob:

* X: Drona ulega badaniom destruktywnym. Drona zostanie zniszczona po badaniach.
* V: Logi z drony itp - jak używana, jak działało itp
    * Drona została wyprodukowana jako drona wojskowa "trzeciego sortu" rok temu.
    * Drona była shackowana. Ukradziona i przyporządkowana.
    * 4 tygodnie temu po raz pierwszy działała na tym terenie.
    * Drona pokrywa się ze zniknięciami. Drona była używana też na innych terenach.
        * GDZIEŚ - GDZIEŚ INDZIEJ - TUTAJ
        * 5-tyg kadencje
* V: Sygnał wsteczny do centralki spowodował ujawnienie dla Mariusza wszystkich lokalizacji wszystkich dron. Mają kilkanaście dron.
    * Jedna drona perma-monitoruje AMZ. Drony ogólnie latają, monitorują, szukają - wzór dron to 'hunter pattern'
    * To nie są lokalsi. To grupa, która tu weszła i się pojawiła.
* V: Jak odkładała się magia (pomoc Klaudii)
    * Są lekkie elementy rezydualne - na przestrzeni czasu byli łapani raz magowie raz nie-magowie
    * Odkąd są na tym terenie, zaczyna się energia Esuriit (bliżej niż dalej Esuriit). Muszą być dobrze uzbrojeni.
    * Dwa tygodnie temu doszło do ogromnej manifestacji na linii Alucis + Esuriit.
        * To nie jest pułapka na Teresę. To coś INNEGO.
* X: Przestraszyli się sytuacji z Teresą. Przyspieszają operację zamknięcia.

Czas odwiedzić dyrektora. Przekonać go do jakiegoś planu. Wypłoszyć ludzi na dyrektora. Klaudia i Mariusz czekają aż dyrektor jest sam i idą doń.

* Arnulf: (Ma jeszcze jakieś 38 lat, wita ciepło). Moi ulubieni uczniowie nie powodujący problemów (powiedział to prawie szczerze). Co się dzieje?
* Klaudia: Trochę więcej siły niż mamy. Potrzeba nam pomocy na akcji.
* Arnulf: Słucham :-)
* Klaudia: Jest grupa polująca na młodych ludzi i magów (A spoważniał). Mamy na nich wstępny namiar ale się nie rzucamy.
* Mariusz: Monitorują campus AMZ cały czas.
* Klaudia: Są również bardzo ostrożni, zaalarmowanie policji czy terminusów sprawi że się wycofają i tyle ich widzielśmy. (-> wszystkie dane, zniknięcia, korelacje)
* (Dyrektor bardzo spoważniał, usiadł inaczej). (Klaudia i Mariusz przedstawiają mu plan)
* Arnulf: Plan nie jest zły (prawie szczerze), ale można go usprawnić.
    * Po pierwsze, potrzebujecie coś zdolnego do cichego usuwania przeciwników. Tak, mam takie narzędzia.
    * Po drugie, nie chcecie by oni się czegoś spodziewali. Jeśli niczego się nie spodziewają, można wejść w dzień do obozu i się nie spodziewają.
    * Po trzecie, wszyscy wiemy, że ten teren jest niebezpieczny. Oni też powinni to zauważyć. Więc można zaatakować ich nie atakując. Zasymulować anomalie. Nie terminusów. Potwory. I w chaosie wejść do akcji.
* Mariusz: Niespodzianka to potwory, tak? A dodatkowo prawdziwi komandosi?
* Arnulf: Tak, dwóch wystarczy. Ja i kolega Grubosz.

Arnulf proponuje inny plan:

* AMZ zaprojektuje potwory i zacznie narastać potwory w ich stronę. Żeby wyglądało to jak 'migracja', 'normalne nasilenie'. Ale żeby oni musieli mieć problem.
    * Potwory wdepną w pułapki, miny itp. Plus - dystrakcja.
* W tym czasie Arnulf i Tymon przygotują operację infiltracji. Wejdą tam i zadbają o zdrowie ludzi.
* AMZ nie ma nic przeciwko wprowadzeniu z potworami jakiejś formy choroby / broni biologicznej.

W tym miejscu rola Mariusza i Klaudii jest inna:

* Mariusz ma zadbanie o drony. Zdobycie perfekcyjnej informacji.
* Klaudia ma dowiedzieć się jak nie przerazić jeńców. Jak się z nimi kontaktować. Co im powiedzieć.
* Chcieli porwać Teresę. WIęc Mariusz ma ich zmylić, że Akademia szuka.

Do tego manifestacja Alucis (?). Klaudia ma dowiedzieć się co i jak.

Trzewń oszukuje drony oraz wprowadza fałszywe informacje by czuli się bezpieczniejsi i kupić czas Dyrektorowi.

Tr Z(taktyk+oni nie spodziewają się magii) M+3:

* Vm: 
    * Jak najdłużej nie wiedzą o niczym podejrzanym. Potwory przejdą niepostrzeżenie. Przyjdą z INNEJ STRONY. Perfekcyjna synchronizacja kamer.
    * Co więcej, potwory ich zaskoczyły dzięki prawidłowej sieci kamer i monitoringu. Czyli pierwsze zarażenie atakiem biologicznym było atakiem z zaskoczenia.
    * Drony są pod pełną kontrolą Mariusza.
    * (Nie wszyscy jeńcy są w rękach napastników; jest drugi ośrodek. Miejsce, gdzie są jeńcy i przeszłe dane z dron pokazały, że za każdym razem były odparte ataki napastników. Chroni tego Księżniczka Arianna. I zjadła część napastników którzy próbowali się wedrzeć)
    * Kamery dają bezpieczny _funnel infiltracyjny_.

Klaudia idzie poprosić Teresę o pomoc. Wie gdzie idą. Jak dyrektor niszczy porywaczy, Teresa pomoże Klaudii. A Klaudia odświeża legendę o Księżniczce Ariannie i nauczyć się paru epickich tekstów.

* Teresa: Co jest?
* Klaudia: E... chcesz iść do lasu pomóc porwanym ludziom?
* Teresa: Porwanym?
* Klaudia: Chociaż część. Tam gdzie są źli tam idzie dyrektor z terminusem. Ale jest grupa, która jest pilnowana przez coś co uważa że Teresa jest OK.
* Teresa: ..ja? Ja jestem w porządku?
* Klaudia: Do Ciebie gada, do niczego nie.
* Teresa: (serio zaskoczona) Czyli... Ty, ja i Mariusz idziemy do strasznego miejsca w lesie by pomóc ludziom?
* Klaudia: Zasadniczo. Wpisuje się w legendę Arianny.
* Teresa: Nie jestem Arianną.
* Klaudia: Nie jesteś.
* Teresa: Nawet nie lubię tych ludzi.
* Klaudia: Tym lepiej wpisuje się w historię.
* Teresa: Czemu historia ma znaczenie?
* Klaudia: Bo magia?
* Teresa: Nieważne, mogłam uważać, rozumiem...
* Klaudia: Nie mówię że nie uważałaś... ludzie wierzą więc magia to robi.
* Teresa: Tak między nami? Magia jest głupia i nie ma sensu.
* Klaudia: A to się zgadzam.
* Teresa: Ktoś groźny z nami pójdzie? Nie wiem, ktoś kto umie walczyć?
* Klaudia: Mariusz potrafi krzywo patrzeć
* Teresa: Wow. (wyraźnie się boi)
* Klaudia: Ci źli będą zajęci dyrektorem.
* Mariusz: Było groźniej jak sama poszłaś do lasu a nie wiedziałaś o tym
* Teresa: ...no dobra, pójdę. Sama byłabym szybciej, wiecie o tym.
* Klaudia: Jest to bardzo możliwe.
* Teresa: NIE ŚMIEJ SIĘ. Słyszałam.
* Klaudia: Dobrze.
* Teresa: Ale nie mówcie dyrektorowi. On nie może wiedzieć.
* Klaudia: Nawet nie jest w temacie.
* Teresa: Uff. (ostatnia nadzieja zgasła XD)

Klaudia przygotowuje sobie sprzęt na poradzenie sobie z manifestacją na wypadek problemu...

OPERACJA: SZUKAMY BAZY. Idziemy w kierunku na Ariannę. Trzewń zauważył zniszczony stary noktiański ścigacz. Czeka. Musiał sam przylecieć, ale nie miał jak. Mignęła na nim Księżniczka Arianna. Teresa jest bardzo podejrzliwa.

* Teresa: JA na to nie wsiadam. To wybuchnie.
* Klaudia: Pod tym względem jest całkowicie bezpieczny.
* Mariusz: Ja bym spróbował
* Teresa: Proszę bardzo, jest Twój (lekko nerwowy śmiech).

Mariusz siada na ścigacz. "Widzimy się za kilka godzin". Teresa prychnęła. "Wow" i siadła. Ostrożnie. Klaudia też się zmieści.

Ścigacz się _zmienił_. Jest sprawny, jak kiedyś. Znowu manewruje pomiędzy pociskami i działami. Ale widzicie jednocześnie, że między drzewami. Leci sam i leci prawidłowo. Jest to aż niesamowite. Czujecie mrowienie wynikające ze Skażenia, ale Skażenie jest małe. Dojechaliście w ciągu jakichś... 10 minut? To było szybkie, manewrował perfekcyjnie. I jesteście niedaleko starego, zniszczonego APC. Jest tam sporo maszyn, one wyraźnie zostały przyniesione albo sprowadzone. Te maszyny się ruszają. Nie mają prawa działać, ale działają.

* Coś co było kiedyś gąsienicami czołgu transportuje coś do jedzenia do APC.
* Coś, co kiedyś było czołgiem jest rusztowaniem z działem, ale pilnuje i monitoruje teren
* (mamy maszyny, które nie mają prawa działać. Ale działają. I chronią.)
* wszystko co jest martwe a działa to technologia noktiańska
* cały ten mechosystem jest napędzany martwymi zwierzętami i ludźmi (nie jeńcami)

Tak czy inaczej, maszyny nie wykazują agresji w Waszą stronę, ale też nie wykazują dobrej woli. (przed jazdą Klaudia zaprojektowała scenariusz dla Teresy o Księżniczce Ariannie, wyjaśniła jak ma działać itp). 

Teresa jest biała, łzy z oczu, cicho, zaciśnięte mięśnie. Klaudia potrząsa.

* Klaudia: Musimy ich stąd zabrać
* Teresa: Pociski... 
* Klaudia: Musimy ich stąd zabrać by nie spotkało ich to samo

Klaudia (szybkie google) i przemowa Arianny. Jesteśmy na terenie Alucis. "Arianna powinna to naprawić". Niech Alucis dotknie Teresę.

Ex +3 -> Tr M +3:

* Ob: Manifestacja Alucis.
    * Dookoła Was świat się zmienił. To nie jest już zrujnowana polana z półsprawnymi maszynami. To jest królestwo z bajki. Widzicie swoich ulubionych ludzi.
    * Księżniczka Arianna 'witajcie w domu, tu jesteście bezpieczni'.
    * Nie da się odróżnić rzeczywistości od fikcji.
    * Klaudia: "Tu jesteśmy bezpieczni, ale czekają na nas w domu i będą się martwić. Ale musimy przywrócić zaginionych do domu. Wiemy jak wrócić."
* X: (hidden: Teresa zostaje, tu są wszyscy których kocha). Klaudia konstruuje argument zgodnie z opowieścią. +1Vg
    * Teresa pobiegła i rzuciła się na szyję swoim rodzicom. Manifestacja.
* Vr: 
    * Księżniczka Arianna uśmiecha się uroczo: "Proszę bardzo, czyli moja rola skończona" :-). Ludzie do Was dążą.

Plan w stronę 'closure dla Teresy'. Ostatnia okazja pożegnania, której ona potrzebuje. Trzeba przekonać DZIECKO. Kaellę.

* M->K: "Już czas wracać do domu. Musisz wrócić do rodziców."
    * Przekonać Kaellę że rodzice Teresy muszą odejść może być ciężkie XD.
    * Ale 'Kaella, wróć do rodziców' jest OK.

"To jest pojazd noktiański, dojedziemy do bezpiecznego miejsca, do domu. Weźmy wszystkich do domu." I zaklęcie Trzewnia by APC był APC. (eskalacja)

* Ob:
    * Jedynie Kaella zasilała Alucis nad Esuriit. Teraz Kaella czuje się bezpiecznie. Osłona Alucis spadła.
    * Esuriit zdominowało Alucis i jesteście NIE w manifestacji Alucis a w manifestacji Esuriit.
    * Sama gleba próbuje Was pożreć. "Jeńcy" - oni są 'mniejsi' fizycznie, wyssani z energii. Oni wyglądają jakby nie spali od paru tygodni. Są incoherent.
        * Są ślady autokanibalizmu.
    * Teresa krzyczy, przewraca się na ziemię, jej "rodzice" wyglądają jak zombie nie ludzie i próbują ją pożreć.
    * Księżniczka Arianna zniknęła. A Kaella zasnęła. Jej ciałko nie wytrzymało. (+2Vy +2Oy - reprezentują rozpaczliwy czar Teresy)
* Vm: 
    * APC pod wpływem absolutnej paniki Mariusza zintegrował się ze wszystkimi innymi mechanizmami. Silnik działa. RUSZYŁ z prędkością... amalgamatu technologicznego.
    * Wszyscy spieprzacie przed goniącą Was manifestacją Esuriit.
    * Ludzie nie stanowią problemu. Są za słabi.
    * Teresa stoi przy ścianie APC i patrzy na ścianę.

Po 15 minutach APC-amalgamat zbliżył się do AMZ ścigany przez manifestacje. Wieżyczki osłoniły APC. Problem rozwiązany.

Natychmiast medycy, lekarze, te siły, "nawet nie pytajcie". Klaudia podaje informacje co i jak by wiedzieli jak to rozwiązać.

Dyrektor solidnie opieprzył Klaudię i Trzewnia. Ale bądźmy szczerzy - co mogli zrobić? Był zły na siebie, bo nie mógł osłonić swojej Teresy.

## Streszczenie

Korzystając z odbudowy po wojnie, napływowa grupa gangsterów polowała na dzieci. Gdy złapali Kaellę, młodą protomag Noctis, ta stworzyła iluzję Księżniczki Arianny z bajek, aby im pomogła. Ta - manifestacja Alucis i Esuriit - porwała dzieci od porywaczy i je schowała. Tymczasem Teresa - noktianka z AMZ - dostała sygnał od 'Księżniczki' i zaczęła szukać kierowana głosami swoich zmarłych rodziców.

Klaudia i Mariusz doszli do tego co się stało i po tym jak osłonili Teresę poszli do dyrektora Arnulfa. Opracowali plan ratunkowy po odkryciu co się stało. Arnulf z Tymonem skupili się na gangsterach, Klaudia i Mariusz poszli po dzieci. Plan zakładał odwrócenie uwagi Drapieżników poprzez stworzenie ataku potworów.

Niestety, Teresa ma flashbacki z czasów wojny a Alucis pokazało jej rodziców. Po wyłączeniu iluzji stworzonej przez Księżniczkę Ariannę, wszyscy musieli uciekać przed manifestacją Esuriit, która zagrażała wszystkich. Udało się im bezpiecznie dotrzeć do terenu AMZ, gdzie dzieci otrzymały niezbędną pomoc medyczną.

## Progresja

* Teresa Mieralit: retraumatyzowana przez transformację manifestacji jej rodziców z Alucis w Esuriit, ze złamanym przez to wszystko sercem ponownie.
* Arnulf Poważny: zbudował silniejszy link z Tymonem Gruboszem; wspólnie uczestniczyli w operacji niszczenia porywaczy dzieci.
* Tymon Grubosz: zbudował silniejszy link z Arnulfem Poważnym; wspólnie uczestniczyli w operacji niszczenia porywaczy dzieci.

## Zasługi

* Klaudia Stryk: Pokazała Teresie na czym polega ryzyko sytuacji (z porywaniem) i przekonała ją do współpracy mimo wyraźnego strachu Teresy. Pracując z policyjnymi bazami danych odkryła, że faktycznie ktoś porywa ludzi w okolicy. Zaprezentowała Plan dyrektorowi i zaakceptowała modyfikację.
* Mariusz Trzewń: Stworzył drona stealth i testując go znalazł ludzi polujących na Teresę. Odpędził ich. Badając dronę porywaczy, odkrył, gdzie są i przejął kontrolę nad ich flotą. Podczas akcji ratunkowej, gdy Alucis zmieniło się w Esuriit, magicznie zregenerował zniszczonego APC i ewakuowali się zanim manifestacja ich dorwie.
* Teresa Mieralit: Została dotknięta przez manifestację Alucis Księżniczki Arianny; szukała w lesie swoich rodziców i prawie została porwana. Gdy Trzewń i Klaudia ją spytali co się dzieje, powiedziała im co wie. Poproszona o pomoc w ratowaniu dzieci, poszła mimo że bała się magii. Retraumatyzowana przez sceny wojenne, potem przez wizję swoich rodziców, POTEM gdy Alucis zmieniło się w Esuriit. Ale pomogła.
* Kaella Sarimanis: Porwana przez gang polujący na dzieci i młodzież trzynastolatka. Protomag, eks-Noctis. W panice stworzyła manifestację ‘Księżniczki Arianny’ jako istotę Alucis/Esuriit. Dzięki temu porwane dzieci zostały porwane ponownie do relatywnie bezpiecznego miejsca.
* Arnulf Poważny: 38 lat w tej chronologii. Gdy Klaudia i Mariusz przyszli doń z planem ratowania dzieci, zaproponował plan alternatywny; wykorzystał AMZ do produkcji potworów i wraz z Tymonem wkradł się do obozu. Wspierał Zespół, ale nie pozwoli by jego podwładni ucierpieli.
* Tymon Grubosz: Morderczy terminus, ze wsparciem dyrektora i pod osłoną atakujących potworów i braku detekcji zdewastował obóz porywaczy dzieci. Dyskretna operacja dla dyskretnego problemu.

## Frakcje

* Drapieżcy Neovim: Inteligentny post-wojskowy gang chcący łatwo się dorobić który wszedł na ten teren by porwać dzieciaki i ewakuować się dalej. Nie spodziewali się, że ta ich komórka zostanie wykryta i usunięta przez magów.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Akademia Magii, kampus
                                1. Las Trzęsawny: gdzieś tam znajduje się obozowisko porywaczy dzieci i tam 'Księżniczka Arianna' zrobiła obozowisko chroniące owe dzieci

## Czas

* Opóźnienie: 8
* Dni: 2
