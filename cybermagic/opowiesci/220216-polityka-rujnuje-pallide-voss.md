## Metadane

* title: "Polityka rujnuje Pallidę Voss"
* threads: legenda-arianny
* gm: żółw
* players: fox, kapsel, kić

## Kontynuacja
### Kampanijna

* [220202 - Sekrety Keldan Voss](220202-sekrety-keldan-voss)

### Chronologiczna

* [220202 - Sekrety Keldan Voss](220202-sekrety-keldan-voss)

## Plan sesji
### Co się wydarzyło

Faza 1:

* Iorus. Pierścień Halo. Znajduje się tam niewielka rozproszona kolonia Keldan Voss.
* Keldan Voss zbankrutowali. Zostali kupieni przez Pallidę.
* Pallida sprowadziła swoich ludzi, którzy działają nad stację należącą do Keldan Voss.
    * Drakolici z Keldan Voss mają dość nieprzyjemne zasady. Nie chcą redukować poziomu energii.
        * ewolucja przez magię
    * Pallidanie chcą zdobywać i harvestować kryształy Vitium.
    * Mała stacja, z rozproszonymi stacjami dodatkowymi. Wszędzie są generatory Memoriam.
* Faith Healer Keldan Voss nazywa się Kormonow Voss.
* Z uwagi na wysoki poziom magii, stare mechanizmy itp. mamy promieniowanie - lokalni drakolici i sprowadzeni pallidanie chorują i gasną
* Z uwagi na to, że drakolici nie chcą się integrować i współpracować, wszyscy mają obowiązkowe transpondery / "komórki".
* Drakolici mają swoich "Kroczących We Mgle", którzy mogą działać bez skafandrów i we mgle.

Faza 2:

* Mgła. Część Kroczących jest Skażona i jest częścią Mgły. Część działa bez problemu.
* Pallidanie obwiniają drakolitów o Mgłę i o śmierci spowodowane przez Mgłę.
* Mgła atakuje bazę.
* Dwóch agentów naprawiających generatory Nierzeczywistości zostało Skażonych przez Mgłę.
* Szczepan chce, by Annice się nie udało. On lepiej będzie tym dowodził.
    * 4 członków delegacji -> na orbicie
    * wysłany zespół zabójczy w Mgłę; zabił już 7 drakolitów
* Kormonow chce się pozbyć pallidan. Mgła - nienawiść - go już opętała.
    * Drakolici wiedzą, że Kormonow wysadził generatory by móc ratować umierających drakolitów i palladian.
    * 11 pallidan (sprowadzonych) jest przez Kormonowa "naprawionych". -> 7 zabrała Mgła
    * 12 pallidan "porwanych" -> 5 zabrała Mgła, 3 uratowanych przez drakolitów
* Gdzie są drakolici? => niewielkie generatory Nierzeczywistości, znajduje się baza zasilana Vitium w księżycu.

Faza 3:

* Historia
    * Było tu starcie noktian i drakolitów z Neikatis. Drakolici wzięli się tu stąd, że na Neikatis wojna faeril - drakolici. Tutejsi uciekli w to miejsce. I wtedy napatoczyli im się noktianie. I noktianie stąd uciekli - pokonani przez drakolitów. I tutejsi drakolici to po prostu "chów wsobny" a częściowo "wolni ludzie". Część noktian tu wróciła i zostali przyjęci. I ci ludzie stanowią aktualny kult. 
    * Gdzieś po drodze zwrócili się do Saitaera. I on odpowiedział. Małe dary - utrzymać ludzi przy życiu. I osobą, która zwróciła się do Saitaera był ich naczelny naukowiec, badacz. Ten koleś znalazł faktycznych kultystów Saitaera. To jest przodek naszego szamana. Kormonow Voss jest potomkiem badacza, który "oddał" tą bazę Saitaerowi. 
    * pallidanie są z Neikatis. Oni są bliżej faeril.
* Mateus chce fuzję korporacji i sił Saitaera po rozmowie z Arianną
* Kryształy Vitium PAMIĘTAJĄ bitwę noktian i drakolitów. Jakaś forma vitium jest potrzebna.
* Na orbicie jest mag - Barnaba Lessert. To on jest celem Bii. Bia (klasa: Reitel) chce go pozyskać.
* Szczepan chce, by zespół zabójczy zestrzelił Annikę.
* Szaman, Kormonow Voss, chce by pallidanie przestali ich zabijać - Mgła i wysokie stężenie kryształów Vitium jest niezbędne do życia drakolitów.
    * On jest już homo superior. On i wielu innych drakolitów. A np. Mateus jest "brzydki" by odwrócić uwagę od Perfekcyjnych
* Mgła ma własności korupcyjne.
* Elena
    * Esuriit daje jej nieskończony głód. Obsesja na punkcie poprzedniej formy i poprzedniego życia.
    * Ixion daje jej adaptację. Jej ciało jest morficzne. Korozja i korupcja.

### Co się stanie

* ?

### Sukces graczy

* Baza przetrwa atak Mgły
* Nie dojdzie do eskalacji
* Snajper i mgła?
* Elena...

## Sesja właściwa
### Scena Zero - impl

* .

### Scena Właściwa - impl

Arianna gada z Mateusem - jak działała ta stacja, jak to funkcjonowało itp. By dowiedzieć się gdzie może znaleźć tych ludzi. By to przemycił. Arianna współpracuje z Mateusem.

TrZ+3:

* Vz: Mateus podaje informacje gdzie znajdują się "zakładnicy". Są na innym "kamieniu" w pierścieniu Halo. Tam BYŁA stacja wydobywcza. Skończyły się rzeczy do wydobywania.
* X: Co najmniej kilkunastu drakolitów będzie zagrożonych jeśli zrobią wjazd / interwencję. To są ci "pro-Mateusowi i pro-Ariannowi". Są autodziałka i droidy wydobywcze.
* X: Mateus jest niechętny do przekazywania Ariannie sekretów swojego ludu. Arianna musiała wysłać mu sporo materiałów niepublikowanych od Izy, Kamila itp. Mają materiałów o Sekretach.
* V: +2V
* Vz: Mateus się wygadał. Część z nich są Kroczącymi We Mgle. 3 osoby. Reszta wymarła. I Kroczący dbają, by nic złego nie stało się tamtym co są zakładnikami.

Eustachy kanibalizuje SREBRNĄ ZASTAWĘ ANNIKI! Umorusanymi smarem łapami robi z tego granat srebrny. Annika składa na ręce Arianny protest formalny. Klaudia spuszcza go do kibla. Nigdy nie dotarł do Arianny. Klaudia napisała odpowiednią formę o koniecznym odszkodowaniu (z zaniżonym odszkodowaniem). Annika jest załamana. Czemu ona pisała się na tą misję. Aż łzy w oczach.

Eustachy używa wody królewskiej. Chce TROCHĘ srebra + chce mieć +5 do zachwytu ze strony Anniki potem.

TrZ+4:

* X: 2 łyżeczki odeszły w niepamięć
* V: wszystko inne udało się zachować. Nawet odświeżona itp.

Elena pintką robi za prom i przesuwa odratowanych ludzi z Kastora na statek palladiański. Elena -> Annika: masz iść na statek palladiański. Annika "moi ludzie". Elena "jesteś super irytująca". Elena jest super zirytowana, więc Arianna wysłała ją na pierwszą transzę. A Annika do Eustachego "dziękuję, kapitanie!" Arianna cicho do Eustachego "jesteś złym gliną ja dobrym".

TrZ (Eustachy jej zabrał zastawę) +4:

* V: Parę słów o Annice
    * Annika jest córką wysoko postawionego dygnitarza w Pallidzie.
    * Annika i jej rodzina pracują nad tym, by faeril i drakolici przestali się tak nienawidzieć.
    * To ona przekonała rodziców by to kupić i by wszystko było dobrze. Myślała o sobie jako o "przyjadę, pomogę, naprawię".
    * W przeszłości Anniki i jej rodziny było ostre wzbogacenie się na drakolitach i wojnie. I ona chce zrobić by było dobrze.
    * Boi się, że jak wróci bez sukcesów to wszystko będzie zrujnowane - jej reputacja (mniej) i cele które próbuje osiągnąć (więcej)
    * Annice wskazano tą bazę jako miejsce które można kupić. Też wśród palladian. Rodzice byli przeciw, ale wzięła kredyt.
    * Politycznie została wyrolowana
* V: Annika i magia
    * Ona nie wierzy w istnienie Kroczących We Mgle czy Saitaera.
    * Ona wierzy swojemu magowi. Jej mag - Barnaba - ogólnie nie wierzy w Saitaera. Nie wierzy w ten kult. Uważa, że to zaawansowany magitech.
        * On jest jej doradcą.
    * On dał jej serię rad które doprowadziły do tej katastrofy. Np. o tych generatorach Memoriam.
    * Annika naprawdę szczerze w niego wierzy.
    * Dużo osób twierdzi na statku, że ci drakolici są bezwartościowi. M.in. ich zastępca gardzi tymi drakolitami.

Klaudia leci następną transzą z Eleną. Elena -> Klaudia "potrzebujesz dywersji? Opieprzę ich że nie pomagają. Że nie mają kwarantanny. Ja będę miała dobrą zabawę a Ty masz spokojną akcję." Klaudia -> Arianna -> "Raoul się tym zajmie, niech się uczy bo Ty masz nieuczciwą przewagę." Elena ma smutną minkę. "On się od Ciebie nauczy, jesteś za to odpowiedzialna, nie zniszcz nic." Arianna - "przechwyć Barnabę. Chcę z powrotem Barnabę, Klaudię i Was. Nikt nie ma się zorientować."

Plan B: wysyłamy biurokratę-komandosa (Klaudię) na tamtą jednostkę palladiańską. Eustachy jest "jednym z uratowanych" i infiltruje z Raoulem. Wpierw Raoul i Eustachy - jak wygląda statek, jakie ma pomieszczenia itp. I oni nie lubą drakolitów. Eustachy chce uszkodzić generatory Memoriam. Sukcesywnie padają generatory, wchodzi Klaudia cała na biało i ratuje. Raoul szuka drakolitów, Klaudia po systemach. Raoul ma porwać Barnabę. A w tym czasie Eustachy cały czas robi im wjazd na chatę. I używa ładunków wybuchowych.

Arianna: "Jeżeli nie jesteś w stanie zrobić tego bez ładunków to działaj skutecznie."

Przed operacją Klaudia syntetyzuje odpowiednie karty wejściowe, wejściówki na podstawie karty Anniki. 

TpZ+3: 

* X: strażnicy są podejrzliwi wobec tych kart NIE DLATEGO że są błędne. Dlatego, bo nie ufają Annice. Zakłada, że są drakolitami w przebraniu. I to jest Raoul.
* V: karty są prawidłowe

Przelatują w kolejnej transzy "uchodźców". Na miejscu, tamta jednostka - są 2 żandarmi. Puścili wszystkich poza Raoulem (bo jest noktianinem, podejrzewają że to drakolita przepuszczony przez Annikę). A Raoul to taki drobny krępy noktiański "Rumun". Blady jak cholera. Ma się wygadać.

Raoul, na bajerowanie:

Tr (niepełna karta) Z (bo ma SEKRETY i oni coś podejrzewają) +3:

* X: Raoul dostaje wpierdol. Ale spoko, to taki "poczciwy wpierdol na mikroLeonę"
* Vz: Raoul przekonał ich, że wie Coś Ważnego. Zaczął sypać, że "Annika z drakolitami chce Coś Zrobić".
* Vz: Po wszystkim, Raoul po prostu oddał wpierdol i wrócił do akcji jak Eustachy powie "nadszedł czas".

Dużo oczu na Raoulu. Dywersja pierwszego stopnia udana. Ale niestety na Raoulu nie można polegać w dalszej operacji - aż się uwolni ;-).

Klaudia przypisała siebie do sekretariatu, administracji na karcie a Eustachego do inżynierii.

Eustachy w inżynierii ma przeprawę z kolesiem który go nie kojarzy. Ale Eustachy się postawił i koleś się oddalił. Eustachy -> Klaudia "niech on był ostatnim serwisującym".

Eustachy zdalnie, z konsoli, sukcesywnie przeciąża generatory Memoriam.

Tr (chce by to było dyskretne) Z (nie ta skala problemu, słabe zabezpieczenia itp) +4:

* V: sukcesywnie będą wyłączane. Odpowiedni poziom stresu na pokładzie
* V: inni mają kłopoty by je uruchomić, bo Eustachy skutecznie sabotował odpowiednie komponenty
* V: Eustachy zrobił to całkowicie profesjonalnie. Nikt nawet nie wie że on tu był. Wszystko spada na tamtego kolesia i Raoula.

Klaudia wchodzi jako NIESPODZIEWANY AUDYT do sekretariatu. Wchodzi w czysto mafijne podejście audytorki. Zna się i na statkach, i dokumentach i wszystkim. Panika w sekretariacie.

TrZ (omg ta operacja nie ma sensu więc nic dziwnego że audyt) +3 (omg niech mnie nie zwolnią gadałam z kim śpię)

* V: totalna groza, przerażenie, nie ośmielą się patrzeć na łapki
* Vz: wszystkie rekordy na statku bo groźna audytorka
    * dane o magu - haki dla Anniki
* V: seria intryg
    * Szczepan, zastępca Anniki próbuje ją podkopać i nie będzie płakał jak Annika zostanie tu martwa. Chciał ją zabić - by wyglądało na to, że zabijają ją drakolici.
        * Jego zdaniem drakolici to podludzie bo nie chcą zachować czystej ludzkiej formy. Chcą inżynierii biomagicznej. To jest niebezpieczne.
        * Ludzie z tej frakcji "czystej ludzkiej rasy" powiedzieli Annice o tej kolonii drakolickiej i oni desygnowali Szczepana jako egzekutora
        * Celem Szczepana od początku jest to by operacja się nie udała.
    * Są drakolici których Szczepan porwał nie mówiąc Annice - oni mieli być tymi co niby zabili Annikę. Mag robił na nich badania - co im robi Mgła.
    * Mag ogólnie lubi Annikę i chce ją z tego wyplątać, nie wie o tym że ona ma zginąć, ale wziął kasę i zrobi co miał.
    * Sporo ludzi na statku lubi Annikę i jej plan. Ona nie jest tu sama. Sporo osób chce skończenia tej durnej wojny. Wierzą w Annikę.
* V: Klaudia ma super twarde dowody na to wszystko i dla korporacji i dla Orbitera.
* (+3V bo obniżamy trudność) X: przyjdzie do sekretariatu Ktoś Ważny by OMG AUDYT.
* V: Eustachy zna lokalizację medlabu z drakolitami i maga, który zaczyna robić w gacie przez generatory Memoriam.

Eustachy wysyła Raoula na maga - ma go przechwycić i się z nim ewakuować na prom. Sam idzie po drakolitów. 2 strażników. W środku medlab. Jeden starszy doktorek, jedna młoda. Drakolici na stole, część pokrojonych.

Eustachy rzuca iskierki, pokazuje mgłę - by drakolici wiedzieli, że jest jednym z nich.

TpM+3:

* Xm: inkarnacja. Golem nienawiści, synteza martwego człowieka i mechanizmów za wolą Saitaera. Techniczka krzyczy w panice. Starszy wali w drzwi i się posikał.
* (+3X): Om: pogarda Eustachego i nienawiść drakolitów nadała "duszę" potworowi. On OŻYŁ. Ratuje drakolitów. Ciała drakolitów, uszkodzone itp. zaczynają się zasklepiać mechanizmami.
* Om: moc magiczna kaskadowo przeniosła się między generatorami Memoriam.

Klaudia: rekomendacja? EWAKUACJA STATKU.

Klaudia uruchamia czerwona alarm "konieczna ewakuacja". Natychmiastowy czerwony alarm ORAZ ostrzeżenie Arianny. Wraca do lat szkolnych. Osłonić TAI przed korupcją.

TrZM+3:

* V: Klaudia odpaliła czerwony alarm i złapałaś obraz lekko delirycznego TAI. Kryształy vitium tworzą Mgłę. TĄ Mgłę. Wojna Noctis - Drakolitami, ta wojna lata temu weszła na pokład tego statku...
* Vz: Klaudia użyła Raoula jako soczewki by jako, że jest noktianinem obniżyć poziom między drakolitami i noktianami. (+2V)
* O: Mgła opanowuje jednostkę. Jednostka jest nie do uratowania.
* O: Anomalia Kosmiczna.
* X: Straszliwe straty. Nie słuchają. Klaudia ratuje kogo się da, ale nie ma szans, nie wszystkich się uda.

Elena leci na maksymalnej prędkości by ratować ekipę i kogo się da...

EUSTACHY. Używa drakolitów by ewakuować ludzi. "Na konflikcie nie ma adaptacji. Uda się bez tego". Na ewakuację, taktyka:

TrZ+4:

* V: Udało się ograniczyć straty.
* X: Sporo ludzi zginęło w panice.
* X: Obwiniani są drakolici.
* V: Drakolici ich ratowali.

Mam dobrą wiadomość - na OO Kastor się wszyscy zmieszczą. To **oni** przedobrzyli, Orbitera tu nigdy nie było. Z 150 osób udało się uratować 60.

## Streszczenie

Arianna we współpracy z Mateusem planują uratować zakładników pallidańskich od radykałów keldańskich. Ale by to osiągnąć Eustachy i Klaudia infiltrują wpierw Pallidę Voss, bo okazuje się, że Annikę wrabiają jej własni podwładni. Klaudia zdobyła wszystkie potrzebne dowody, Eustachy chciał ratować ludzi... i Paradoks (po sabotowaniu generatorów Memoriam) spowodował straszne straty w ludziach i przesunął Pallidę Voss w Anomalię Kosmiczną. Oops. Ale dzięki drakolitom (keldanitom) udało się sporo ludzi uratować.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Arianna Verlen: konspiruje z Mateusem by skutecznie wyciągnąć zarówno pallidan od keldanitów jak i keldanitów od pallidan. 
* Eustachy Korkoran: zaprzyjaźnił ze sobą Annikę i poznał jej historię i co ona myśli. Infiltruje Pallidę Voss, sabotował generatory Memoriam i niestety Paradoks gdy dotarł do "sali medycznej" gdzie krojono drakolitów spowodował Kaskadę Paradoksu. Konieczność ewakuacji statku... cholerni kultyści Saitaera.
* Klaudia Stryk: infiltruje Pallidę Voss jako audytorka, zdobyła 100% potrzebnych informacji i twardych dowodów. Potem - kontrolowała ewakuację Pallidy Voss i dzięki niej udało się uratować więcej niż się zdawało.
* Elena Verlen: destabilizuje się; chce NISZCZYĆ dla dywersji (Klaudia ją zatrzymała). Głównie działała jako prom transportujący na lewo i prawo, acz energia ją trochę nosi.
* Annika Pradis: nie ma pojęcia, ale jest wkręcana politycznie. Jej "przyjaciele" manewrują dookoła niej, by móc zniszczyć rekoncyliację pallidanie - keldanici, ją i jej rodzinę. Nie wierzy w istnienie Saitaera XD. 
* Raoul Lavanis: infiltruje z Eustachym Pallidę Voss; wpadł w kłopoty bo nie ufali Annice (karta była prawidłowa), ale sam się uwolnił. 
* Szczepan Kaltaben: okazuje się, że konspiruje przeciwko Annice i chce ją zniszczyć. 
* Mateus Sarpon: pasywne wsparcie dla Arianny. Chce pomóc by wszyscy zostali uratowani i przeniesieni tam gdzie mają być.
* SP Pallida Voss: staje się Anomalią Kosmiczną po tym jak Eustachy wysadził generatory Memoriam i miał Paradoks... ogromne straty wśród pallidan. I na pokładzie pojawiła się Mgła.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Iorus
                1. Iorus, pierścień
                    1. Keldan Voss
  
## Czas

* Opóźnienie: 1
* Dni: 2

## Konflikty

* 1 - Arianna gada z Mateusem - jak działała ta stacja, jak to funkcjonowało itp. By dowiedzieć się gdzie może znaleźć tych ludzi. By to przemycił.
    * TrZ+3
    * VzXXVV: Mateus podaje info gdzie znaleźć zakładników, zagrożonych kilkunastu keldanitów, Mateus się wygadał że część pallidan stało się Kroczącymi we Mgle.
* 2 - Eustachy używa wody królewskiej. Chce TROCHĘ srebra + chce mieć +5 do zachwytu ze strony Anniki potem.
    * TrZ+4
    * XV: odświeżona zastawa dla Anniki. Annika bardziej go lubi niż kiedyś.
* 3 - A Annika do Eustachego "dziękuję, kapitanie!" Arianna cicho do Eustachego "jesteś złym gliną ja dobrym".
    * TrZ (Eustachy jej zabrał zastawę) +4:
    * VV: Annika powiedziała wszystko co wie, poznaliśmy jej historię i to, że najpewniej jest wkręcana politycznie...
* 4 - Przed operacją Klaudia syntetyzuje odpowiednie karty wejściowe, wejściówki na podstawie karty Anniki
    * TpZ+3
    * XV: Raoul wpada w tarapaty a karty są prawidłowe XD. Czyli Raoul jest skuteczną dywersją.
* 5 - Eustachy zdalnie, z konsoli, sukcesywnie przeciąża generatory Memoriam.
    * Tr (chce by to było dyskretne) Z (nie ta skala problemu, słabe zabezpieczenia itp) +4:
    * VVV: sukcesywnie wyłączane, skuteczny sabotaż, 100% profesjonalnie. Nikt nawet nic nie wie.
* 6 - Klaudia wchodzi jako NIESPODZIEWANY AUDYT do sekretariatu. Wchodzi w czysto mafijne podejście audytorki. Zna się i na statkach, i dokumentach i wszystkim. Panika w sekretariacie.
    * TrZ (omg ta operacja nie ma sensu więc nic dziwnego że audyt) +3 (omg niech mnie nie zwolnią gadałam z kim śpię)
    * VVzVVXV: Klaudia ma 100% informacji o intrygach, lokalizacjach, haki itp. Ma super twarde dowody na sabotaż ze strony Szczepana.
* 7 - Eustachy rzuca iskierki, pokazuje mgłę - by drakolici wiedzieli, że jest jednym z nich.
    * TpM+3
    * XmOmOm: kaskadowa destrukcja generatorów Memoriam i inkarnacja, golem nienawiści. Konieczna ewakuacja statku XD.
* 8 - Klaudia uruchamia czerwona alarm "konieczna ewakuacja". Natychmiastowy czerwony alarm ORAZ ostrzeżenie Arianny. Wraca do lat szkolnych. Osłonić TAI przed korupcją.
    * TrZM+3
    * VVzOOX: czerwony alarm, obraz delirycznego TAI, Klaudia widzi co się tu dzieje. Raoul jako soczewka by SPOWOLIĆ wojnę. Ale niestety, mgła - jednostka nie do uratowania i anomalia kosmiczna...
* 9 - EUSTACHY. Używa drakolitów by ewakuować ludzi. "Na konflikcie nie ma adaptacji. Uda się bez tego". Na ewakuację, taktyka:
    * TrZ+4
    * VXXV: ograniczone straty, drakolici uratowali sporo ALE obwiniani że to ich wina XD.
