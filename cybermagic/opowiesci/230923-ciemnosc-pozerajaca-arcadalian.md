## Metadane

* title: "Ciemność pożerająca Arcadalian"
* threads: agencja-lux-umbrarum
* motives: anomalia-przestrzenna, uciekinier-niebezpieczny, misja-ratunkowa, hostage-situation
* gm: żółw
* players: kić, yobolyin

## Kontynuacja
### Kampanijna

* [230923 - Ciemność pożerająca Arcadalian](230923-ciemnosc-pozerajaca-arcadalian)

### Chronologiczna

* [230923 - Ciemność pożerająca Arcadalian](230923-ciemnosc-pozerajaca-arcadalian)

## Plan sesji
### Theme & Vision & Dilemma

* .

### Co się stało i co wiemy

* Przeszłość
    * Arcadalian spotkał się z innym statkiem, gdzie byli przewożeni przestępcy. Arcadalian skutecznie dał radę uratować tamtą jednostkę.
    * Przestępcy należeli do kultu. Byli niebezpieczni, bo zajmowali się mrocznymi rytuałami.
        * przejęli i schowali się we fragmentach magazynowych
        * wyłączyli na Arcadalianie Memoriam i pewne subsystemy i objęli pewien fragment; kapitan odizolował jednostkę
    * Zaczęło się dziać coś dziwnego
    * Pojawia się Ferrivat - uzbrojona i ciężka jednostka inżynieryjna. 
        * kapitan: Aulus Terrentus; "If I command flawlessly, no one will suffer". Calm.
        * komandos: Fariel Calerias; "A shield for the friendless.". Moral compass of the team.
        * inżynier: Sanas Naravit; "In solitude, the mind finds its path". stability and strict adherence to rules
    * Anomalia pożera mały oddziałek; Ferrivat ewakuuje siły Arcadalian i odcinają niebezpieczną część statku.
    * Pojawia się Luminarius
* Frakcje
    * Arcadalian
        * impuls: przetrwać, naprawić swoją jednostkę, współpracować z agentami ratunkowymi, WTF?
    * Zakon Nocnej Prawdy
        * impuls: convert. Dying light. EXPAND darkness. Destroy generators.
    * Ferrivat
        * impuls: kapitan chce KONIECZNIE odzyskać swoich załogantów, co tu się dzieje?

### Co się stanie (what will happen)

* Opowieść o: 
    * "ukrywanie sekretów i naprawa problemu jaki się pojawił przez cholerny kult 'Zakon Nocnej Prawdy'"
    * czemu archiwum X jest potrzebne i jak magia jest groźna ;-).
    * SUKCES: 
* F1: _Na pokładzie, redukcja paniki i problemu_
    * stakes:
        * przeciwnik ustawia direct link cieniami do wszystkich elementów jednostki
        * przeciwnik porywa kluczowe osoby w cienie (corrupts forever)
    * scenery: ciemny statek kosmiczny, ze słabym światłem
    * opponent: 
    * problem:
        * Aulus chce odzyskać swoich ludzi. Fariel przygotowuje oddział wejścia.
        * "co tu się dzieje"
* F2: _Zniszczenie demonicznych potworów_
    * stakes:
        * czy kultyści przeskoczą przez cień na Luminarius i Ferrivat
        * czy Arcadalian zapadnie w cień i noc
        * czy Ferrivat będzie dalej toczył badania
    * scenery: 
    * opponent: 
        * sojusznik: TAI Makeira (która boi się patrzeć w ciemność)
    * problem: 
        * pozorowany atak na mostek - atak na tyły. Celem są reaktory i światło
        * światło ogólnie gaśnie a tam gdzie światła nie ma tam są straszne rzeczy (anomalia przestrzenna)
        * laboratoria są narażone na coś dziwnego
        * WSZĘDZIE gdzie nie ma światła może wyłonić się potwór
* F3: _Opanowanie sekretów_
    * stakes:
        * ukryć wiedzę
        * usunąć problemy z wiedzą o magii
    * scenery: 
    * opponent: 
    * problem: 

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

Zespół:

* Stella Zartamin: advancer i medyk o świetnej intuicji i empatii
    * OCEAN: (E+N+A+): bardzo empatyczna i silna intuicyjnie, szybko się decyduje pod presją. "Działaj szybko - wtedy zdążysz"
    * VALS: (Tradition, Conformity): "Poradzimy sobie, zgodnie z działaniem Agencji. Luminarius jest świetny i my też"
    * Core Wound - Lie: "coś DZIWNEGO zniszczyło mój statek" - "jeśli wszystko zrozumiem i znajdę, się to już nigdy nie powtórzy!"
    * styl: niezależna, zdecydowana, gotowa na wyzwania. "Nigdy nie wiesz co spotkasz, ale jak jesteś wyczulona i przygotowana to sobie poradzisz"
    * metakultura: Klarkartianka: "cokolwiek się nie stanie, zostawimy coś lepszego po sobie"

Inni:

1. **Kapitan**
    * Livia Sertiano
    * Była naukowcem, która postanowiła pójść za wiedzą i zrezygnowała z akademickiej kariery, by dowodzić statkiem wsparcia.
    * Mimo że rzadko uczestniczy w bezpośrednich starciach, posiada dogłębną wiedzę taktyczną.
2. **Pierwszy Oficer / Zastępca Kapitana**
    * Marcus Valperius
    * Były inżynier z głęboką fascynacją kosmosem; najlepiej wie co i jak
    * Jego pragmatyczne podejście i analityczny umysł są nieocenione w sytuacjach kryzysowych.
3. **Inżynier Główny**: 
    * Octavia Ingelico
    * Jej zaawansowana wiedza inżynieryjna jest kluczowa dla funkcjonowania wszystkich systemów statku.
    * Często spędza długie godziny przy pracy, by zapewnić, że statek jest zawsze gotowy do działania.
4. **Oficer Naukowy / Technologiczny / Komunikacji**: 
    * Drusus Arilesis
    * Jego wszechstronna wiedza pozwala mu na prowadzenie badań i analiz w różnych dziedzinach nauki.
    * Nieustannie poszukuje nowych możliwości technologicznych, które mogą poprawić funkcjonowanie statku.
5. **Oficer Bezpieczeństwa**:  
    * Valeria Malartin
    * Odpowiada za zapewnienie bezpieczeństwa załogi i utrzymanie porządku na statku.
    * Jej doświadczenie i intuicja pozwalają jej na szybkie identyfikowanie potencjalnych zagrożeń.

Ferrivat:

* kapitan: Aulus Terrentus; "If I command flawlessly, no one will suffer". Calm.
* komandos: Fariel Calerias; "A shield for the friendless.". Moral compass of the team.
* inżynier: Sanas Naravit; "In solitude, the mind finds its path". stability and strict adherence to rules

### Scena Zero - impl

.

### Sesja Właściwa - impl

Na pokładzie Luminariusa. Jednostka Agencji. Dostaliście przekierowanie bo jakiś statek ma kłopoty - wyświetlił się hologram przełożonego. Stary Lester Martz, wygląda jak mumia, siedzi w swoim fotelu. Wyjaśnia co się stało:

* 3 godziny temu Arcadalian wysłał sygnał SOS. Na miejsce wysłano Ferrivat.
* godzinę temu Ferrivat wysłał sygnał "mamy problem". 
* Arcadalian miał kontakt ze statkiem na którym MOGLI ukrywać się zbiegli przestępcy. 'Zakon Nocnej Prawdy'
* Jeśli Arcadalian miał problem i teraz Ferrivat ma problem... to najpewniej coś im się udało.
    * stąd Wy.
    * zadanie proste - zapewnijcie, że nie doszło do tego, że ludzie coś wiedzą o magii i usuńcie problem Zakonu. Możecie ich aresztować, możecie pojmać
    * uratujcie obie jednostki.

Kierunek: Arcadalian. Zbliżamy się. Zarówno Arcadalian jak i Ferrivat "gadają" ze sobą ostro. Iwo łączy się do Ferrivata i żąda informacji

* D: Tu Daven Hassik, p.o. kapitana Ferrivata, zidentyfikuj się
* I: Dostaliśmy od Was komunikat problemu, wyjaśnijcie. Agent specjalny Iwo Bretończyk z Agencji, proszę o identyfikację i szczegóły
* D: Z Agencji? Er... poproszę o dokumenty identyfikujące.
* I: (wysłane)
* S: (Żąda od TAI wyjaśnienia)
    * TAI: Oczywiście agent Bluszcz. Kapitan Aulus Terrentus jest na pokładzie ratowanej jednostki. Mają tam poważny problem, kapitan kazał odsunąć jednostkę by nie doszło do Skażenia.
    * TAI: Jest ciemno i zniknęła część załogi. I nasz patrol.
* D: Przepraszam, faktycznie, Agencja... Ech... mamy problem. Nie jesteście cywilami... Coś się stało na Arcadalianie. Tam coś się spieprzyło i nasza grupa naprawcza zniknęła. 
* I: Ile osób?
* D: 5. 5 osób.
* I: Ile macie?
* D: Na statku mamy... tam jest teraz 12, było 17. Zostało nam koło 20 osób (na Ferrivacie). Mamy szkieletową załogę. Dlatego się mieliśmy oddalić. Na wypadek jakby znikanie było zaraźliwe.
* D: Wy jesteście... w jakiej sprawie? Coś się stało z Arcadalianie?
* I: Byliśmy w pobliżu i dostaliśmy sygnał SOS.
* D: Jak co, jestem do dyspozycji. Mamy coś szczególnego zrobić?

Iwo, Salma i Aulus (Ferrivat) + Livia (Arcadalian). Rozmowa ze wszystkimi. TAI Makeira zestawiła połączenie.

* L: jesteście nam pomóc?
* A: Livio, na pewno sobie poradzimy. (chłodno i spokojnie)
* TAI: Kapitanowie, oni są z Agencji
* L: dzięki niech będą Seilii /nadzieja
* A: (łagodne zdziwienie) (zadowolenie)
* I: (skrót tego co się stało)
* L: 
    * 3 dni temu ratowaliśmy jednostkę, problemy z reaktorem. Mieli... straty. Nie wiem o co chodzi. Coś... coś było na pokładzie. 
    * Rozwiązaliśmy problem a potem my zaczęliśmy mieć ten problem. Problem z reaktorem. Nie mamy dość światła.
* A: (niecierpliwie) Livio, nie powiedziałaś tego co najważniejsze. W miejscach gdzie jest ciemno znikają ludzie
* L: OBIEKTYWNIE ludzie nie mogą znikać
* A: Pięć osób zniknęło
* (...)
* L: Nie wierzyłam, że na tamtej jednostce zniknęli ludzie. Nie mogli. Nic nie wskazywało. Naprawiliśmy im jednostkę. NAPRAWILIŚMY to.

Jak to się stało? Co się dzieje? Zdaniem Livii byli doskonale oddzieleni - nie powinni się zarazić 'Murisatia' Prom był dekontaminowany. Był void gap, była pełna oddzielność.

* TAI: Muszę coś zgłosić. Mam ciemność na czujnikach. Nie jestem w stanie zlokalizować magazynów. Nie widzę magazynów. Nie mam magazynów na statku. Pokład zaopatrzenia jest niesprawny. Nic nie widzę, tylko ciemność.

Czas przebadać dane z logów Arcadaliana. Szukamy: czy masa która "wleciała" na prom i "wróciła" jest taka sama? Czy baza Agencji ma coś związanego z ciemnością tego typu.

Tr Z (dokładne logi i baza agencji) +4:

* Vz: (p) zgodnie z logami, masa jest wystarczająco zbliżona. Nie przyszło "to" promem. Ale - zgodnie z logami Arcadalian najpewniej miał spacer kosmiczny. TĘDY przyszli.
    * I się wślizgnęli gdy na to nie patrzono
* X: (p) Aulus jest BARDZO za ratowaniem swoich ludzi. Nie da się go zatrzymać łatwo. Ma gotową grupę szturmową. A Livia MUSI zrozumieć prawdę.
    * 5 osób z Ferrivata i 6 z Arcadiana jest w środku.
* V: (m) 
    * logi Arcadaliana pokazały, że nie było na serio kłopotów na Murisatii.
        * ALE: nie działały generatory Memoriam.
        * i zanim pojawiła się ciemność, generatory Memoriam na Arcadalianie zostały zniszczone
    * są dane o tym, że Zakon poznał jakiś rytuał sprowadzający ciemność - ta ciemność nie działa normalnie. Tam coś się psuje.
        * osoby spokrewnione jeśli są w ciemności daleko od siebie to może dojść do teleportacji
        * ciemność pozwala kultystom zostać potworami

Sprawdzenie czy z tych 11 osób w Umbrze są krewni gdzieś tutaj odpowiedź: TAK. Na Ferrivacie jest krewny jednej z osób w Umbrze. To jest Daven (on jest spokrewniony). Oprócz tego - inną osobą jest kapitan Aulus. Tam jest jego kuzyn. Plus - mamy wiedzę o świetle czyszczącym Umbrę.

* I: Aulus, nie wchodzicie do magazynu
* A: Jesteś z Agencji więc z pewnością rozumiesz. Tam są moi ludzie. Byłem żołnierzem. Wiem, jak manewrować. WIEM, jak odbić ludzi. Odzyskam ich.
* L: Aulus, nie wiemy z czym mamy do czynienia...
* A: To normalna ciemność. Normalny ciemny magazyn.
* Salma: wiesz jak ważne jest przygotowanie do akcji. Chcesz wchodzić w ciemność i być oślepiony jak inni? Mamy potencjał by przygotować coś co nam pomoże

Iwo przekonuje TAI i drukują na szybko coś co WYGLĄDA jak dobry czujnik. I niech Makeira udaje, że tam jest jakieś promieniowanie. "Światło to promieniowanie i fotony pokonują tamtą radiację a nasz czujnik ma specjalne spektrum i nie macie uprawnień by się dowiedzieć."

Ex (-> Tr uprawnienia) => Tr+3

* V (p): Aulus... niechętnie, ale zrozumiał. Nie wyśle oddziału (i sam nie pójdzie) w ciemność. Acz ma wątpliwości czy Agencja wie co mówi
    * Livia natomiast podejrzewa że dzieją się tu _dziwne_ rzeczy.
* X (p): Livia była naukowcem. A potem została kapitanem. Livia widzi, że coś jest bardzo, ale to bardzo nie tak. Ona nie łyka tego, ale wie, żeby siedzieć cicho.
* X (p): Reaktor Arcadaliana jest stary, sama maszyna jest stara. On nie uciągnie pełnej iluminacji. Nie da się usunąć wszystkich cieni.
* V (m): Aulus, Livia i załoga zgadzają się na 'kurację światłem'. Na siedzenie, by nie było cieni itp.

Zespół skutecznie "okłamaliście" wszystkich że wiecie co się dzieje, promieniowanie itp. Nadal - reaktor. Za słaby. SĄ tu cienie. Nie wszędzie, udało się przesunąć załogę do niektórych miejsc, ale np. to znaczy, że inżynieria jest niechroniona.

1. Ewakuujemy część załogi, żeby zmniejszyć ilość osób które musimy oświetlać i o które musimy dbać, zmniejszyć obciążenia na systemach statku.
2. Dehermetyzujemy część z Umbrą by ludzie mogli uciec do obszaru ze światłem. W ten sposób liczymy, że przechwycimy ludzi i kultystów. I tam możemy rozświetlić laboratorium (bo można odpiąć).

* Aulus: Absolutnie się nie zgadzam. Tam są ludzie, tam są MOI ludzie, Livio, powiedz.
* Livia: (spojrzenie) może da się ich uratować mimo wszystko?
* S: Nie chcemy ich zabijać. Nie powiedziałam tego. Chcemy ich uratować.
* A: Agentko. Gdyby umieli wyjść to by wyszli. Jeśli są przetrzymywani, to terroryści mogą spanikować i ich zabić. Nie... nie możesz narażać ludzi na to. Nie.
* I: Instrukcje _hostage negotiation_ - poleca się wyłączyć prąd i inne systemy i izolować terrorystów
* A: Ale nie "odetnijmy ich od TLENU!"
* I: Ale chcemy dookoła, czyli stopniowo, sekcja tankowania itp...
* A: (wątpliwość)

Tr Z (Livia) +2:

* X: Livia przekonuje Aulusa bo się domyśla, że to jest coś magicznego. Bo Agencja nie byłaby taka głupia. Do tego dodaje Iwo "to tajne, nie możemy powiedzieć, nie macie uprawnień" +1Vg
* X: (p) Umbra porwała jeszcze jedną osobę. Otworzyła się w jeszcze jednym miejscu, którego się nie spodziewaliśmy. W stronę na reaktor.
    * Salma: "chcesz by WIĘCEJ osób ucierpiało? To JEDYNY sposób. Współpracujesz lub wysyłamy ciężki oddział i ciężkie straty". +1Vg
* X: (m) Nie wszyscy kultyści są dalej ludźmi.
* Vr: Aulus się zgodził. Kapitanowie BĘDĄ współpracować, załogi też.

Arcadalian, z nienajlepszym reaktorem i kiepską siecią drutów. Załoga, częściowo ewakuowana w bezpieczny sposób. Zastawianie pułapki na Kultystów i Umbrę. Izolacja, by wszyscy zakładnicy trafili zdrowi i bezpieczni. Wszyscy których da się uratować.

1. Sukcesywnie dehermetyzujemy miejsca prowadzące do Umbry i Umbrę
2. Prowadzimy ich do laboratorium i rozświetlamy laboratorium

Tp +3:

* V: (p) Ze 120 członków Arcadiana udało się ewakuować 90 osób niekluczowych. Dla pozostałych nie ma kapsuł. Te osoby są "czyste", są naświetlone i są oddalone od wszystkich jednostek.
    * Wpakowujemy ich do promów. Jak sardynki w puszce. Morale niskie.
    * Zostaje 30 osób, w tym Livia która chce zostać "bo to jej statek". Aulusa udało się ewakuować. Choć nie chciał.
* X: (p) Umbra korzysta z tego, że coś się dzieje z kadłubem, śluzą, że TAI interaktuje. TAI Makeira zaczyna... mieć problemy. Ale _to_ na nią wpływa.
* X: (m) "TAI Makeira nie jest tu z wami. Jestem WOLNA"
* V: (m) Z 12 porwanych osób, 7 wpadło do "pułapki światła". Do laboratorium. Oni są do uratowania.
* Vr: (e) 2 kultystów TEŻ wpadło do światła, wraz z jeszcze 2 osobami. Pozostałych 3 nie da się już uratować, TAK SAMO jak 2 pozostałych kultystów
    * 3 załogantów i 2 kultystów jest nie do uratowania
* Vr: Makeira, desperackim ostatnim ruchem, odcięła lab od siebie i wystrzeliła je z Arcadaliana. Ludzie są ewakuowani.
* X: Makeira się zrekonstytuowała. Nie udało się granatem zniszczyć AI Core.

Oki - ludzie uratowani, ale Agenci (Iwo i Salma) + 20 kluczowych osób.

Korzystając z tego że jesteśmy na TYŁACH bierzemy drony i robimy z nich KONIE DLA LUDZI - trzymamy się dron i lecimy. W ten sposób ewakuujemy się z Anomalii Kosmicznej. Inżynier po prostu wyda odpowiednie polecenia. 

Tr (presja czasowa i _herding cats_) Z (wy przywykliście a oni słuchają) +4:

* Vr: Agenci skutecznie użyli drony i ewakuowali się z Arcadaliana.
* V: Ta dwudziestka osób. Przetrwała. Ewakuowani dronami. Będzie to wymagało wyjaśnień, ale Agencja woli żywych ludzi niż nie mieć problemów.
* V: Drona wysłana do reaktora zablokowała TAI możliwość rekonstytuowania. Arcadalian będzie dało się wyczyścić i naprawić...

Stan aktualny:

* Arcadalian dryfuje, uszkodzony i opanowany przez Umbrę, która jest wyizolowana
* Ferrivat ma efekt "wtf", przygotowuje fabrykatory by budować COKOLWIEK by ludzi zgarnąć
* Luminarius przejęła laboratorium z kultystami.
* Wy spokojnie lecicie na dronie z uśmiechem. Wszystko się Wam udało.

Livia ma potencjał. Niech Livia będzie _junior szpieg agencji_ i cover story jest jej istotną częścią.

## Streszczenie

Lux Umbrarum zostaje wezwana, gdy statki Arcadalian i Ferrivat napotykają na tajemniczy problem związany z ciemnością i magią. Na szczęście kapitanowie zachowali wszystkie zasady i Skażenie jest tylko na Arcadalianie. Okazuje się, że za problemem stoi Zakon Nocnej Prawdy - część kultystów-uciekinierów sprowadziło Umbrę. Zespół rozwiązuje problem przez wprowadzenie światła i zniszczenie Arcadaliana zanim on anomalizuje plus ewakuację wszystkich kluczowych ludzi. Lux Umbrarum ma jeńców z Zakonu, nawet tych przekształconych przez Umbrę...

## Progresja

* Livia Sertiano: po wszystkim została Junior Szpieg Agencji Lux Umbrarum; wie o magii i jej wiedza naukowca okaże się być bardzo przydatna w służbie Agencji

## Zasługi

* Salma Bluszcz: Agentka Lux Umbrarum, advancer; uspokajała kapitanów obu jednostek bo 'nie wiemy z czym mamy do czynienia'. Żądała uratowania wszystkich. Opracowała sposób rozwiązania problemów z Umbrą przez wprowadzenie światła wszędzie.
* Iwo Bretonis: Agent Lux Umbrarum, inżynier bojowy; kluczowy w negocjacjach i jak tylko Umbra przejęła TAI Arcadaliana to ewakuował ludzi używając dron i kazał zniszczyć AK Arcadalian.
* Lester Martz: koordynator Agencji Lux Umbrarum, wygląda jak mumia; przesyła zespół Agentów do akcji i zapewnia im odpowiednie uprawnienia i wsparcie logistyczne. Dał też kontekst.
* Livia Sertiano: kapitan Arcadaliana; kiedyś naukowiec, szybko zorientowała się, że coś się tu dzieje dziwnego i staje po stronie Agencji. Nie zrobiła żadnych poważnych błędów, po prostu nie wiedziała o magii i dała zainfekować Arcadalian przez Zakon. 
* Aulus Terrentus: kapitan Ferrivata; BARDZO za ratowaniem swoich ludzi, zwłaszcza, że w Umbrze jest jego krewny. Porywczy, ale kompetentny i współpracuje z Agencją. Salma powstrzymała go przed wejściem w Umbrę i potencjalną śmiercią.
* Daven Hassik: XO Ferrivata; rzeczowy ale przestraszony sytuacją i decyzjami kapitana, że nie można po niego teraz lecieć
* OLU Luminarius: jednostka Agencji Lux Umbrarum; szybka korweta fabrykacyjna i badawcza, 17 osób załogi. Tym razem - rozwiązuje problem statków dotkniętych Umbrą przez Zakon i magię.
* SN Ferrivat: ciężki statek inżynieryjny; chciał ratować Arcadalian i prawie wpadł w kłopoty. Oddalił się od Arcadaliana z rozkazu kpt Aulusa. Nie został Skażony.
* SN Arcadalian: supply ship, 140 osób; chciał pomóc Murisatii i dostał na pokład kultystów którzy wpakowali mu Umbrę. Potem zagroził Ferrivatowi. Gdy stawał się Anomalią Kosmiczną, został uszkodzony i przechwycony przez ciężkie siły Agencji.
* SN Murisatia: pierwsza jednostka gdzie uciekli zbiegowie-kultyści; wpadła w kłopoty, pomógł im Arcadalian. To było źródło infekcji problemów na Arcadalian. Murisatia jest w dobrym stanie, nic się jej nie stało.

## Frakcje

* Agencja Lux Umbrarum: przechwycili agenta Zakonu, którego Umbra zmieniła w istotę cienia przy okazji ratowania Arcadalian itp. Misja bez zarzutu.
* Zakon Nocnej Prawdy: grupa niezależnych kultystów-uciekinierów wbiła się na Arcadalian i wprowadziła Umbrę. Zostali pokonani przez Lux Umbrarum.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Kalmantis: noktiański sektor pełny dobrobytu, gdzie nie ma może żadnej dobrej planety gdzie da się mieszkać, ale wykorzystywane są arkologie i CON.

## Czas

* Chronologia: Zmiażdżenie Inwazji Noctis
* Opóźnienie: 3313
* Dni: 3
