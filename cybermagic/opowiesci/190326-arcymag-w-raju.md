## Metadane

* title: "Arcymag w Raju"
* threads: krysztaly-noktianskiej-pamieci, deprecated
* motives: masowy-atak-mentalny, dont-mess-with-my-people, hostage-situation, obrona-przyczolka, forced-transformation, stracona-nadzieja, stopniowe-tracenie-siebie
* gm: żółw
* players: kić, dust

## Kontynuacja
### Kampanijna

* [190418 - Spóźniona wojna Elizy](190418-spozniona-wojna-elizy)

### Chronologiczna

* [190326 - Arcymag w Raju](190326-arcymag-w-raju)

## Punkt zerowy

### Postacie

* Ataienne: AI i piosenkarka w Trzecim Raju; mindwarp.
* Eliza Ira: zniszczy Trzeci Raj lub go uratuje. Ale iluzja pryśnie.
* Kamil Czawik: terminus, wsparcie tej Dwójki.

### Opis sytuacji

Pytania sesji:

* "Czy Zespół uwolni ludzi i magów spod wpływu Ataienne?" -> default: wszyscy nie żyją
* "Czy wszyscy pozostaną pod kontrolą Ataienne, czy stanie się to dyskretnie?" -> default: Orbiter Pierwszy uruchomi Castigator i zniszczy Trzeci Raj.

Tory:

* Lokalizacja Elizy: 2: TAK, 3: baza, 4: HQ
* Świadomość ludzi wpływu Ataienne: 2: niektórzy (opanowywalne), 3: rozlało się, 4: zamieszki
* Świadomość ludzi Elizy: 2: niektórzy (opanowywalne), 3: zamieszki, 4: wojna przeciw Elizie
* Świadomość Orbitera sytuacji: 2: Castigator na orbicie, 4: bombardment

Sceny:

1. dropship (rok temu): krystaliczny wirus; fortyfikacja w Centrum Ataienne. CEL: odświeżyć sesję, generować punkty torów, podać kontekst Graczom.
2. dropship (teraz): oddział krystalicznych nojrepów. Spotkanie z Elizą. Kamil jeńcem Elizy (?)

## Misja właściwa

Uderzyła Kryształowa Plaga. Kilkanaście zarażonych osób chciało się dostać do kompleksu kontrolnego Ataienne. Pojawiła się barykada założona przez Fergusa oraz Olgę. Jako, że zarażeni nie są w stanie się dostać do środka, część z nich się rozchodzi by zainfekować jak najszerszą populację. Na to odpowiedź Olgi - wykorzystajmy Ataienne jako broń soniczną; Kryształowa Plaga jest na to szczególnie wrażliwa. Udało się, ale niestety Eliza dowiedziała się zarówno o działaniu Ataienne jak i o systemach obronnych Trzeciego Raju.

To niepokojące; arcymagini jest w stanie w końcu przebić się przez ten system obronny. Eliza jest znana z bycia bardzo niebezpieczną. Grzegorz z Orbitera ostrzegł Zespół o tym, że Eliza się uaktywniła; najpewniej to jej robota.

(Dwa tygodnie później.)

Wykryto nojrepy zarażone Krystaliczną Plagą. Budują jakieś działo i celują w Trzeci Raj. Fergus zaproponował wykorzystanie jednego z górników jako dywersję. Niech on - kiedyś agent powiązany z siłami terroru Elizy - pójdzie na teren Fortecy Kryształów i tam dojdzie do tego co na serio planuje Eliza. Celem jest zarażenie górnika Plagą i pozyskanie go na badania do Orbitera.

Górnik Dawid poszedł zatem zorientować się w sytuacji. Bardzo niechętnie i ostrożnie, ale przekonał go Fergus - z uwagi na wpływ Ataienne ludzie i magowie (ogólnie, jeńcy) w Trzecim Raju są posłuszni władzom Orbitera. Dawid znalazł jaskinię i wtedy Zarażony nojrep położył mu rękę na ramieniu. Fergus to natychmiast wykorzystał - uruchomił 'anchor' i porwał statkiem zarówno nojrepa jak i Dawida.

Tam na pokładzie nojrep "pocałował" Dawida. Dawidowi wróciła pamięć. Gdy dowiedział się, że przez nojrepa mówi Eliza, zaatakował go. Zdradziła, on nie chce mieć z nią nic wspólnego. Zostawiła ich na pastwę Astorian oraz nie odrzuciła Saitaera. Eliza spokojnie przetrwała atak i zauważyła, że nojrep nie czuje bólu a ONA już na pewno nie czuje bólu - bo jak, jej tu nie ma. Dawid powiedział, że nie chce jej nigdy widzieć.

Olga poszła porozmawiać z Elizą. Podczas tej rozmowy Eliza wypowiedziała Oldze spokojnie wojnę. Jest skłonna zabić WSZYSTKICH w Trzecim Raju niż pozwolić na to, by Ataienne skaziła ich umysły i zabierała dalej ich tożsamość. Dawid powiedział, że Eliza nie zabiła WROGÓW a SWOICH by mogła? Eliza zauważyła, że jest ostatnim oficerem na Astorii. To jej ludzie. Jest za nich odpowiedzialna.

Olga wyciągnęła z Elizy kilka ciekawostek:

* Eliza nie zabiła ludzi gdy walczyła z Saitaerem, bo nie miała serca. Nie umiała.
* Na planecie Olgi Saitaer został odparty. Właśnie w ten sposób - czystą eksterminacją.
* Eliza nie ma już żadnego celu. Dla ludzi wojna się skończyła, dla niej nie.
* Eliza jest arcymagiem. Nie może poruszać się poza silnym polem magicznym. Jej czas się skończył.
* Eliza żąda wolności dla swoich ludzi. Niech się zasymilują na Astorii, ale niech przynajmniej pamiętają kim są i czym byli.
* Aha, Eliza ma dwóch górników których chciałaby się pozbyć...

Fergus i Olga zaproponowali Elizie, by ona się poddała. Nope, nie jest to opcja. Więc niech Eliza do nich dołączy w formie sojuszu. WOjna się skończyła, wszyscy walczą przeciwko Astorii. Próbują odzyskać Astorię. Po co żyć przeszłością. A niech ludzie którzy chcą wrócą do Elizy.

Po namyśle Eliza się zgodziła. Otworzyła przed Olgą i Fergusem swoją bazę - Krystaliczną Fortecę. Strasznie groźne miejsce. A dołączyło do niej tylko 14 ludzi, w czym jeden mag. Jak wszyscy zauważyli, wojna się skończyła. Nie ma po co jej sztucznie utrzymywać...

Tymczasem na orbicie Castigator był gotowy by zrzucić asteroidę na Elizę, gdyby rozmowy poszły nie tak i gdyby tylko Fergus i Olga dali znać ;-).

**Sprawdzenie Torów** ():

2 Wpływu -> 50% +1 do toru. Tory:

* nieistotne. Dark future niemożliwe.

**Epilog**:

* Eliza nie jest już wrogo nastawiona do Astorii.
* Eliza ma po co żyć - chronić legacy swojej kultury i swojego świata.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
|               |         |             |

Czyli:

* (K): .

## Streszczenie

Eliza Ira zdecydowała się odzyskać swoich ludzi z czasów wojny przetrzymywanych w Trzecim Raju. Stworzyła Krystaliczną Plagę unieszkodliwiającą wpływ mindwarpującej AI, Ataienne. Jednak Fergus i Olga dali radę przekonać Elizę, że wojna się skończyła i ich wspólnym przeciwnikiem jest teraz Astoria. Arcymagini weszła w ostrożny sojusz z Orbiterem Pierwszym, odzyskując kilkanaście osób chcących z nią współpracować.

## Progresja

* Eliza Ira: odzyskała kilkanaście osób chcących z nią współpracować. Dodatkowo, weszła w ostrożny sojusz z Orbiterem Pierwszym.
* Eliza Ira: nienawiść wielu z jej dawnej frakcji do niej jest niesamowita. Tak samo jak nienawiść wielu z Orbitera. Nie jest niczyją ulubioną osobą.

## Zasługi

* Fergus Salien: świetny taktyk i cyber-terminus; wymanewrował kilkukrotnie Elizę i stał za planem przyłączenia Elizy do walki przeciwko Astorii.
* Olga Leszcz: neuronautka i współoperatorka Ataienne; ona głównie rozmawiała z Ataienne i shackowała z Fergusem lokalizację jej bazy by móc napuścić na nią Castigatora.
* Eliza Ira: zdecydowała się na uratowanie swoich ludzi albo śmierć. Skończyła w sojuszu z Orbiterem Pierwszym, by razem odzyskali jak najwięcej Astorii dla ludzkości.
* Ataienne: mindwarpująca TAI o funkcji nadpisywania wspomnień; hipnopiosenkarka holograficzna. Kontroluje Trzeci Raj składający się z jeńców wojennych.
* Grzegorz Kamczarnik: oficer Orbitera Pierwszego na orbicie; ostrzegł Fergusa i Olgę o Elizie i przesłał im jako wsparcie Castigatora.
* Dawid Szardak: górnik z Trzeciego Raju; kiedyś żołnierz. Uważa, że Eliza zdradziła, gdy nie zniszczyła Saitaera (i części populacji Astorii). Bardzo wrogi Elizie.
* OO Castigator: superciężka artyleria Orbitera gotowa do zestrzelenia na planecie Elizy Iry jeśli do niczego nie dojdzie w osi Trzeci Raj - Eliza Ira.

## Frakcje

* Nox Cristallum: pozyskało kilkanaście osób oraz jednego maga. Eliza Ira zaczyna uzupełniać 'grobowiec kryształów' żywymi ludźmi.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, NW
                    1. Ruiniec
                        1. Trzeci Raj
                            1. Centrala Ataienne: centrum sterowania Ataienne; miejsce, z którego jej awatar operuje w świecie realnym
                        1. Kryształowa Forteca: teren na wschód od Trzeciego Raju, pod kontrolą Elizy Iry i ukształtowany na jej podobieństwo
                            1. Rajskie Peryferia: niedaleko Trzeciego Raju, teren pomiędzy Fortecą a Rajem. Tu nojrepy budowały działo by ściągnąć maga.
                            1. Mauzoleum: centralna baza dowodzenia Elizy, miejsce gdzie echa jej zmarłych żołnierzy do niej szepczą i jej moc jest największa.

## Czas

* Chronologia: Aktualna chronologia
* Opóźnienie: 724
* Dni: 1
