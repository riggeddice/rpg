## Metadane

* title: "Osiemnaście Oczu"
* threads: legenda-arianny
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [210512 - Ewakuacja z Serenit](210512-ewakuacja-z-serenit)

### Chronologiczna

* [210512 - Ewakuacja z Serenit](210512-ewakuacja-z-serenit)

### Plan bardzo złożonej sesji

#### Scena Zero - plan

* Jest dzień X.
* Zespół na Tivrze próbuje dogonić Alayę.
* Zespół próbuje unieszkodliwić Alayę.
    * Unieszkodliwić, nie zniszczyć.
* Tosen nie pozwala na zbadanie Alai. Tivr nie posiada penetracyjnych sensorów.
    * Medea powiedziała, że ten Zespół powinien sobie poradzić
* Tivr zawiera
    * Mariana Tosena (ekspert od niebezpiecznych anomalii sił specjalnych Orbitera)
    * 20 robotów bojowych, z Elainkami i z trybem automatycznym (AI-less)
    * 20 załogantów

#### Scena Jeden - plan

* Jest dzień X+4
* Arianna, Klaudia, Eustachy budzą się z medvatów na Alai (nie wiedzą, że są na Alai)
* Czeka na nich nagranie i walizka
    * w walizce są środki amnestyczne
    * MEDVAT 1 (tam nie ma naszych postaci)
        * Leona, "zestresowana" (**notatka 2 X+1 k**): "Elena jest stracona. Arianna jest stracona. Klaudia - stracona. Mam nadzieję, że amnestyki zadziałają". Na filmie celuje w oczy w różnych miejscach, które widzi.
        * Leona, "popękana" (**notatka 3 X+1 k SKASOWANA**): "Inevitable. Inevitable. Oczy są wszędzie."
        * Klaudia, zdenerwowana (**notatka 4 X+2 p**): "Musisz mi zaufać. Byłaś tu już. Nie badaj wszystkiego dokładnie. Zostawiaj sobie wiadomości. Nie próbuj zrozumieć, nie pomoże! Martyn zrobił cykl! Środki amnestyczne... miej je."
        * Eustachy, "popękany / psychotyczny" jak Leona (**notatka 5 X+2 pk SKASOWANA**): "Nigdy nie może być osiemnaście. Jak masz osiemnaście, zniszcz. Nieważne jakie straty. Nieważne co. Pamiętaj, oni są Czerwoni, nie Niebiescy."
        * Klaudia i Arianna, opanowanie (**notatka 6 X+3 p**): "Jesteśmy same, nie wiemy gdzie. Jest cykl medvatu. Nie wiemy co się dzieje i straciłyśmy 3 dni. Nie wiemy gdzie reszta. Chyba przegrywamy."
        * Arianna, disturbed (**notatka 7 X+3**): "30 minut i zostałam sama. Skasowałam trzecią i piątą notatkę. Klaudia nie może ich zobaczyć, ja nie wiem... nie rozumiem. Jestem sama. Muszę... coś naprawić. Klaudia jest w medvacie."
        * Arianna, "popękana", wycina sobie oczy, uwielbienie (**notatka 8 X+3 SKASOWANA**): "korytarz... Klaudio. Musisz. Patrzą. Zniszcz sensory. Nie możesz widzieć. Nie możesz słyszeć. Są wszędzie."
        * Arianna, zdrowa i sfrustrowana (**notatka 9 X+4**): "Skasowałam notatkę 8. Klaudia pod jej wpływem wymaga amnestików. Ja nie. Ale Klaudia zrobiła to co miała - wyłączyła wszelkie sensory wizualne i dźwiękowe z servara. W ogóle, to jest wszystko idiotyczne i śmieszne! Mam wrażenie, że nie możemy wyjść z tego przeklętego medvata!!! Mamy 5 sprawnych servarów, ale tylko ja i Klaudia tu jesteśmy..."
        * Klaudia, zrobiony plan (**notatka 10 X+4**): "Muszę znaleźć sposób, w jaki możemy poruszać się bezpiecznie po statku, jakikolwiek ten statek nie jest. Mamy amnestiki, więc możemy sobie poradzić z tym wszystkim. Ja idę w servarze, Arianna mnie przyciągnie."
        * Klaudia, chora od amnestyków (**notatka 11, X+5**): "Nasze servary działają. Możemy widzieć plamy, nie możemy wyraźnie. Możemy słyszeć rzeczy, nie możemy słów. Nie możemy magii. Zakłóciłam detektory servarów. Aha - na statku coś się dzieje. Walki dogasają i poziom dziwnej energii rośnie. To niepokojące. Jak się źle czuję od tej chemii..."
        * Arianna, drży i ma bandaż na oczach, rozgląda się (**notatka 12, X+6**): "Klaudia przesłuchała skasowane. Eustachy to bohater. Nas wszystkich. Leona też. Autoryzuję zniszczenie wszystkiego i wszystkich. Ludzi może da się obezwład... amnestiki działają. Powoli. Musimy Eustachego... walka się kończy, potrzebujemy."
        * Arianna i Klaudia, w wyraźnym szoku (**notatka 13, X+7**): "Mamy Eustachego i Martyna. Niegroźni. Nie spodziewamy się dużych... musimy zatrzymać Przesilenie. Czemu jesteśmy same? Gdzie wsparcie? Ile czasu?"
    * MEDVAT 2 (tam są nasze postacie)
        * Eustachy: **nagranie 14, X+8**: <loop, Arianna + Eustachy> "Nazywasz się Eustachy. Masz dwie ręce, dziesięć palców. Jesteś inżynierem zniszczenia. Ufortyfikuj ten teren. Strzelaj do każdego kto chce wejść do medvatu z zewnątrz, nawet Eleny."
        * Klaudia, w bardzo złym stanie, "pęknięta", w bieliźnie (przed medvatem) celuje w Eustachego i z bandażem na oczach, w tle pracuje Eustachy: 
            * **notatka 15, X+8**: "To ósma iteracja. Cztery dni. Straciliśmy medvat 1. Tam jest wiedza. Wszystko co pamiętam przed amnestykami. Póki mogę. Póki nie ma... Mamy servary, możemy się w nich poruszać i tylko w nich. Nie próbuj się DOWIEDZIEĆ czy usprawnić sensory. Przetestuj zanim... Nie możemy wyjść poza medvat bez nich. Musisz mieć amnestyki. Nie badaj, nie szukaj, nie próbuj. Musisz... musisz iść do medvata 1. Dowiedzieć się tego co zrobiliśmy. Musisz zatrzymać Przesilenie. Mamy amnestyki, Martyn może zrobić ich więcej. Zostawiaj sobie wiadomości, to jedyna opcja. Czerwoni są groźni. Niebiescy to tylko osoby w tym medvacie. Są tam nieaktywne roboty, były Niebieskie. Wszyscy inni są Czerwoni. Ja nie mogę przesłuchać skasowanych nagrań. Mamy całą wiedzę w medvat 1... Arianna pozwoliła zabijać i zniszczyć. To pamiętam. Aha... Elena. Jak ją znajdziesz do medvata, nie budź. Nie wiem czemu ona..." - przerażenie, zrozumienie, uwielbienie, STRZELA W EUSTACHEGO. Autodefensywny system strzela w nią, uszkadza komputer i jej płuco.
        * Arianna, zirytowana i przestraszona **notatka 16, X+12**: "To dwunasta iteracja, patrz na zegar. Straciliśmy cztery iteracje i nie mamy danych. Wiem, że próbowaliśmy rozwiązać problem Przesilenia, cokolwiek to nie jest. Wiem, że da się uratować ludzi którzy jeszcze żyją. Wiem, że Klaudia jest szczególnie wrażliwa na to wszystko, nie wiem czemu. Wiem, że dane mamy w Medvat 1. Arianno, MUSISZ się dowiedzieć co tam jest. Nie wiem, co dokładnie zrobiliśmy, gdzie jesteśmy i czemu tu jesteśmy. Wiem, że MUSIMY zadziałać zanim Przesilenie się stanie."

#### Sukces

* Usunięcie wiedzy o infohazardzie.
    * Ze wszystkich żywych osób
    * Ze wszystkich komputerów
* NIKT na pokładzie Alai nie może wiedzieć o istnieniu infohazardu "osiemnaście oczu" i związanych z tym konceptów.

#### Środki

* Poniszczone roboty na korytarzach, kiedyś solidne roboty bojowe
* Medvat 1 i Medvat 2 są całkowicie odizolowane od zewnętrza. Klaudia + Eustachy zaizolowali.
* Medvat 2 ma anomaliczne zasilanie. Nikt o tym nie pamięta XD. 
* Medvat 1 nie ma zasilania i jest kompletnie zniszczony przez Leonę od środka. Leona gdzieś tam jest.

#### Przykładowa struktura statku medycznego - Nocna Krypta

* Podwójny pokład górny
    * System komunikacji
    * Ciężkie działo strumieniowe
    * Hangar ŁZ
    * Syntetyzatory ŁZ
    * Syntetyzatory MIRV
    * 20 wyrzutni MIRV
    * Zaawansowany Engineering suite
    * Surgery Suites
* Dysk pod pokładem górnym
    * Mostek z ołtarzem Heleny
    * Nawigacja, sensory
    * System podtrzymywania życia
    * Reaktor
    * Projektor ekranu ochronnego
    * Dwa silniki awaryjne
    * Kabiny dowództwa
* Pokład centralny, za dyskiem
    * Cztery silniki
    * Kabiny załogi
    * Kabiny personelu medycznego
    * Engineering
    * System podtrzymywania życia, dla nie-dysku
    * Medical Treatment: 50 osób
    * Recovery Ward: 20 osób
    * Railway system (transport po Krypcie)
    * Droid / Angel Bay
    * Syntetyzator Aniołów
* Pokład tylny
    * Właz
    * Intensive Care Units
    * Hibernation Suites
    * 2* Biolab
    * 2* Containment Chamber
    * 4* Quarantine System
    * 2* Magazyn
    * Kostnica

#### Wrogowie

* Leona. "Alien".
* Kultyści, uzbrojeni.
* Energia magiczna, energia Przesilenia.
* Osiemnaście Oczu - anomalia wirująca, oparta na energii magicznej

#### Obszary

* Life Support - chroni go Elena, która ma omniwiedzę.
* Biovat - syntetyzujący Oczy oraz Osiemnaście Rzeczy
* Radiowęzeł - tworzący Osiemnaście Oczu
* Reaktor - zainfekowany sigilami Osiemnastek

#### Co się stało w przeszłości

* Przeciwnikiem jest infohazard, bazyliszek, memplex manifestujący się jako "18" oraz jako "oczy".
    * Im więcej wiesz o tym infohazardzie, tym silniejszy ma na Ciebie wpływ.
    * Infohazard dąży do stworzenia Krwawego Rytuału mającego przesunąć Alayę, przeskoczyć ją przez Bramę.
* OO Alaya (medyczny) "złapała" infohazard, ratując jednostkę cywilną lecącą z okolic Asimear w kierunku na Bramę. 
* OO Alaya wysłała sygnał SOS.
* _Arianna, Eustachy, Klaudia_, Elena, Leona i Martyn zostali szybko wysłani tam używając niewielkiej i zwrotnej korwety Tivr.
    * Oprócz nich na pokładzie jest Marian Tosen, ekspert od infohazardów Orbitera
* Pierwsza iteracja
    * Dzień X+1
    * CEL:
        * Dowiedzieć się, co się dzieje. Wyłączyć silniki. Uratować wszystkich.
    * DZIAŁANIA:
        * Śluza:
            * Arianna i Klaudia NATYCHMIAST wpadły pod wpływ infohazardu, po pierwszych zaklęciach i badaniach
            * Martyn, Elena, Leona i Eustachy je unieszkodliwili
            * Martyn ostrzega, że muszą jak najszybciej dostać się do medvatów. Musi zbadać ich fale mentalne.
            * Tosen natychmiast przełącza się na tryb "zostawiam wam informacje". Tu jest **notatka 1**. Informuje o amnestykach.
            * **Tosen wysyła roboty bojowe** mające na celu unieszkodliwiać ludzi. Kupi to czas.
        * Droga Śluza - Medvaty
            * Eustachy i Leona czyszczą drogę ze wszystkich zagrożonych
            * Elena jako advancer. Dotknięta Osiemnastoma Oczami, przekazując informacje.
            * Leona odcina Elenę i właściwie pod pistoletem prowadzi Zespół do Medvatów.
            * Elena jest stracona.
    * KONIEC: 
        * Medvaty 1
            * Leona wpakowuje wszystkich do medvatów, wprowadza amnestyki.
            * Robi **notatkę 2** "Elena jest stracona. Arianna jest stracona. Klaudia - stracona. Mam nadzieję, że amnestyki zadziałają. Celuje w oczy, które widzi.".
            * Sama nie wchodzi do medvata, pilnując wszystkich. Zanim wszyscy się obudzą, sama wpadnie pod Osiemnaście Oczu...
            * Zdąży jeszcze zostawić **notatkę 3** "Inevitable. Inevitable. Oczy są wszędzie."
* Druga iteracja
    * Dzień X+2
    * CEL:
        * Odzyskać Elenę i Leonę. Zrobić co się da by wszystkich uratować?
    * DZIAŁANIA:
        * Medvaty 1
            * Zespół czyta notatkę 2 i 3. Klaudia dedukuje, że jest problem z jakąś formą długoterminowej ekspozycji.
            * Arianna łapie magią infohazard; Martyn wsadza ją do medvata z amnestykami. Klaudia tworzy **notatkę 4** "Nie badaj wszystkiego dokładnie"
    * KONIEC: 
        * Medvaty 1, 30 min później
            * Radiowęzeł. Osiemnaście Oczu. Rozprzestrzenienie wiedzy. Korozja i korupcja.
            * Eustachy ostatkiem sił niszczy głośniki i wszystkie rzeczy które słychać.
            * Eustachy robi **notatkę 5** "Unikaj osiemnastki! Zniszcz osiemnastkę". Z Martynem zamykają od zewnątrz. THEY FALL, Klaudia do medvatu.
* Trzecia iteracja
    * Dzień X+3
        * Medvaty 1
            * Z medvatu wychodzi Klaudia i Arianna. Zapoznają się z notatkami 2-5.
            * Zauważają, że Martyn zaprojektował cykl amnestyczny. Ale nie ma go w okolicy. Nie ma też Eleny, Leony i Martyna.
            * Robią **notatkę 6** "Jesteśmy same, jest cykl medvatu. Nie wiemy co się dzieje i straciłyśmy 3 dni."
            * Klaudia bada serię notatek i też wpada w cykl Oczu. 
            * Arianna unieszkodliwia Klaudię. Kasuje _notatkę 3 i 5_. Robi **notatkę 7**, 15 min później
        * Droga Medvat - Magazyny
            * Arianna opuszcza teren barykady, spotyka się z kognitohazardem i wraca.
    * KONIEC: 
        * Medvaty 1
            * Arianna robi **notatkę 8: "Nie masz rozumieć. Wykonaj rozkazy."**, wycina sobie oczy, idzie do medvatu.
    * Dzień X+4
        * Medvaty 1
            * Klaudia i Arianna oglądają notatki. Klaudia rozumie, że niektóre notatki powodują reakcje. Za blisko infohazarda. 
            * Klaudia wpada w infohazard. Arianna ją unieszkodliwia i robi **notatkę 9**
            * Klaudia po 2 godzinach amnestyzacji robi plan - **notatka 10**
    * Dzień X+5, X+6
        * Medvaty 1
            * Klaudia i Arianna próbują konsekwentnie znaleźć częstotliwości które działają
            * Powstaje **notatka 11** odnośnie częstotliwości, badania Klaudii. Informacja o tym, że walki się kończą i że energia rośnie.
            * Klaudia przesłuchuje skasowane notatki, tworzy agregację którą mówi Ariannie. Arianna robi **notatkę 12**.
    * Dzień X+7
        * Medvaty 1
            * Klaudia i Arianna wyszły w servarach. Udało im się zdobyć Eustachego i Martyna. Bez oczu, w pełnym kognitohazardzie, wpakowały do medvatów.
            * Arianna i Klaudia robią **notatkę 13**.
    * Dzień X+8
        * Medvaty 1
            * Klaudia, Arianna, Martyn i Eustachy idą zniszczyć Przesilenie. Ostry firefight. Zniszczyli kult który PRAWIE ściągnął Osiemnaście Oczu.
            * Na powrocie medvat został zniszczony.
        * Medvaty 2
            * Zespół przelokował się do innego medvata. Eustachy go fortyfikuje na ciągłych amnestykach gdy inni są w Medvacie 2 z autopętlą **nagranie 14**.
            * Klaudia próbuje zrobić agregację notatki i podsumowanie **notatka 15**.
    * Dzień X+9, X+10, X+11
        * DATA CORRUPTED
    * Dzień X+12
        * Medvaty 2
            * Arianna robi **notatkę 16**
    * Dzień X+13
        * Zaczyna się sesja
        * Aktywni: Arianna, Klaudia, Eustachy, Martyn
        * Wrogowie: Kultyści, Potwory, Przesilenie, Persefona, Elena @ LifeSup, Leona @ med_1

### Sesja właściwa

#### Scena Zero - impl

Statek "Gruby Pająk". To jest statek cywilny. On miał katastrofalny problem, leci z Asimear. Alaya go przechwyciła. Niedługo potem Alaya wysłała SOS. Leci w kierunku Bramy. Siły Specjalne Orbitera zorganizowały operację ratunkową, bo ma dostęp do Tivr.

Klaudia wie jednak coś ciekawego o "Pająku". To nie był pojazd cywilny, to był pojazd badawczy zamaskowany jako cywilny. Czyli opowieść Mariana trochę nie trzyma się kupy.

Arianna potrzebuje wiedzieć z czym ma doczynienia i dlaczego ekspert od anomalii się interesuje Pająkiem / Alayą. ExZ+2 (bo Klaudia i Arianna). Sugeruje, że jak dobrze pójdzie to będą mogli też w przyszłości współpracować w tematach około-anomalicznych. On sam odpowiedział (trochę żeby uciszyć Ariannę), że to jest forma testu. Klaudia dodaje, że cokolwiek wiemy o naturze tej anomalii? Arianna dodała, że nie chce ryzykować życia w ciemno. On odpowiedział - tylko najbardziej elitarną część załogi. On traktuje tą misję śmiertelnie poważnie.

* X: Tosen mówi "need to know", powiedział o anomaliach, które JAK WIESZ to jest gorzej.
* Vz: im więcej wiecie, tym większa szansa że anomalia się dostosuje. To moja specjalizacja. Jesteście sami, w sytuacji kryzysowej. Wejdą roboty, on nie wejdzie (bo skazi próbkę).

Klaudia, lekko złośliwie: "W sumie najlepiej samemu sprawdzić co się dzieje".

Zwykłe, hermetyczne Lancery. Żadnych zaawansowanych. Tylko "proste, wytrzymałe rzeczy".

* Arianna: "Klaudio, czy WSZYSCY ludzie zajmujący się anomaliami to biurokraci"
* Tosen: "Szybka praca z dokumentami ratuje życie gdy Cię coś ściga"

Ile muszą wytrzymać? 5-7 dni.

#### Scena Właściwa - impl

Przesłuchali notatki i się zorientowali w sytuacji.

Eustachy widzi śluzę. Sam ją kiedyś zbudował.

Macie w środku części jak z 2-3 robotów. Sporo posłużyło do zbudowania Eustachowych defensyw. Servary są sprzężone i zaizolowane. Jest request komunikacji ze strony Eleny, nikt nie otwiera.

Eustachy składa robota który ma oszacować drogę do Med1. Taki mechaniczny zwiadowca, z przesunięciem detekcyjnym takim jakie zrobiła Klaudia. Rój robotów, Eustachy czaruje.

TrMZ+2:

* Vm: udało się zrobić 4-5 mikrorobotów lotników z detekcją, przesyłających na komputer.
* V: roboty są do wielokrotnego wykorzystania / materiały

Droga robocików od śluzy do med 1.

* Po drodze jest grupa kultystów, nie zauważyli drony ("jacyś ludzie").
* Słychać dudnienie i głosy.
* Przy med 1 jest Leona, ale nie jest na pełnej mocy.

Sytuacja:

* "Jeśli Klaudia połączy fakty to..." - Arianna o krok od wpadnięcia w pułapkę.

Eustachy: idziemy w trójkę. Wysyłamy jednak "włóczkę", wracamy po włóczce do medvat 2. Nawet lepiej - zwiadowca do med 1 kanałem medycznym.

Zwiadowca kanałem medycznym.

* VVXO: Elena przechwyciła dronę, ale znalazła się droga.

Eustachy składa amunicję amnestyczną z Martynem. Wymazanie pamięci do momentu na Tivrze. (ExZ+3):

* Vz: Eustachy zrobił godną amunicję amnestyczną. Wystarczy na kilka osób.

JAKI JEST PLAN DOSTANIA SIĘ DO MED-1?

* Wysłać drona do med-1 i tam ludzi.
    * TrZ+3: ściągamy ludzi na potencjalną Leonę, dywersja pełną gębę.
        * X: utrata kolejnej drony (zostały 2 + wybuchowa)
        * Vz: ludzie poszli. Dywersja jest faktem
        * Vz: głośna drona. Naprawdę dobra dywersja.
* Dostać się do med-3.
    * Klaudia dopełzła w servarze do med-3
* Ściągnąć na kompa notatki z med-1 do med-3 i zabrać ze sobą.
    * TrZ+3: 
        * X: Klaudia czuje za sobą obecność.
        * V: Udaje Ci się kopiować
        * Vz: anomalia przestrzenna, pułapka, by złapać to co za nią.
* Wycofać się do med-2.
    * Klaudia wróciła bezpiecznie.

Klaudia nie chce mieć za sobą cały czas obecności, amnestyk (15 minut) sprawił, że nie pamięta drogi na miejsce. Nie ma tej obecności.

Macie zbiór notatek. Czas się zapoznać.

Martyn składa amnestyki. Potem ogląda ostatni film i jest przywiązany. Celem życiowym Martyna jest wyciągnąć coś, co nie będzie dawało zbyt dużo informacji i jednocześnie nam pomoże. Ma pisać jedną ręką na papierze. I nawet się nie będziemy zastanawiać. To kilka minut. Ryzykujemy Martynowe oglądanie do końca.

Klaudia w servarze patrzy na wszystkich. Arianna -> na Eustachego. Eustachy -> na Martyna. Martyn ogląda nagranie.

Klaudia: TrZ+3

* XX: WSZYSCY skorodowali poza Klaudią. Klaudia musi wszystkich wyłączyć z poziomu servara.
* VV: amnestyk (na wszystkich innych). I Klaudia, niestety, zrozumiała jeden Fakt.

Klaudia pisze notatkę i bierze amnestyk

Macie przed sobą nową notatkę. I bardzo zdewastowanego Martyna. Jego notatkę Klaudia zniszczyła i film skasowała.

"Brak sygnału nie jest bezpieczny (nie muszą "widzieć"). Chaotyzator nie jest bezpieczny. Jedynym bezpieczeństwem jest niewiedza. Na filmie była Leona."

Czyli rozwiązaniem jest połączyć:

* amnestyki
* chaotyzator

Zespół zdecydował się zrobić notatkę i zresetować cykl, bo już czują Obecność.

1. Martyn przerabia amnestyki by nie dało się nic rejestrować (na 2 dni). Plus skasować ostatnie 5 dni.
2. Martyn może robi cuda z pamięcią krótkotrwałą. Możemy iść odruchami i sobie TEŻ to zaaplikować.
3. WPIERW łapiemy kogoś i testujemy na nim czy działa
4. Jak działa, KLAUDIO NIE MYŚL, wrzucamy do life support i ma działać

RESET CYKLU.

Eustachy zdecydował się porwać łapczaka. Wyszedł na korytarz i poluje. Wpierw używa czujek na nadgonienie ofiary, potem strzela. By trafić.

TrZ+2:

* V: Złapał jakiegoś delikwenta.
* X: Oni byli uzbrojeni. Strzelali i uszkodzili Lancera. Ale nie doszło do "przebicia".
* V: Delikwent jest smutny, porwany a pozostali nie żyją (6 ofiar). 

Wsadziliście, amnestyki, KTO rozmawia z naprawionym w kolejnym cyklu? Martyn ustawił na "przed spotkaniem z Grubym Pająkiem". Klaudia z nim rozmawia. Koleś jest kucharz z Alayi. Ma też naprawioną strukturę.

Eustachy wstrzyknął mu środki zapobiegające zapamiętywanie. Delikwent NIE ULEGA anomalizacji. Jeśli nie jest w stanie zapamiętywać, nie jest w stanie anomalizować. Eustachy, niestety, zapoznał się z Faktem. Czuje obecność.

Eustachy bierze amnestyk i pisze notatkę "plan się sprawdza, róbcie TAK". Eustachego wątroba go nienawidzi, jest chory od amnestyków (X).

Martyn robi odpowiednie amnestyki + inhibitory zapamiętywania. (ExMZ+4):

* X: to jest taka duża, dupna butla, która może wybuchnąć.
* Vm: udało się
* Vz: sukces, anestetyki, są nieprzytomni.

Ostatnie zadanie - wsadzić to do life support.

Dywersja DRONAMI: (ExZ+4+3O). JEDNA DRONA ZE SMAKIEM EUSTACHEGO. I ONA MA ODPOWIEDNI ŚRODEK BY TRAFIĆ ELENĘ. Drona z krewką Eustachego ;-)

* Xz: Elena skierowała się na dronę, ale zniszczyła ją na zasięg
* O: Nadal poleciała jak ćma na krew Eustachego. Po sympatii.
* X: Eustachy zaczął coraz mocniej CZUĆ OBECNOŚĆ Eleny. Po sympatii próbuje nawiązać połączenie.
* V: NIEZALEŻNIE OD OKOLICZNOŚCI, Eustachy neutralizuje Elenę.
* V: Eustachy się utrzymał. Mimo Obecności. Mimo PROBLEMÓW. Mimo szeptów ELeny. Utrzymał się. Dla niej. Dla misji!

Gdy butla się rozpyliła, koniec bombardowania Eleną. Wszyscy do śluzy, notatka do Mariana w stylu "NIE REJESTRUJ! NIE MYŚL! NIE PYTAJ! WSADŹ DO MED- i NAPRAW! NIE PACZ!"

I Alaya została ewakuowana...

## Streszczenie

OO Alaya została zainfekowana anomalią memetyczną, infohazardem. Marian Tosen użył Zespołu by zneutralizować infohazard. Zespół przeszedł przez serię iteracji używając amnestyków by w końcu rozwiązać problem Alayi - amnestyki na całą Alayę. Do końca Zespół nie ma pojęcia czym ta anomalia była (bo wiedza ich by zaraziła). Udało się ewakuować większość Alayi.

## Progresja

* Arianna Verlen: wysokie uznanie ze strony Mariana Tosena z grupy antymemetyczej Orbitera.
* Eustachy Korkoran: wysokie uznanie ze strony Mariana Tosena z grupy antymemetyczej Orbitera.
* Klaudia Stryk: wysokie uznanie ze strony Mariana Tosena z grupy antymemetyczej Orbitera.
* Leona Astrienko: wysokie uznanie ze strony Mariana Tosena z grupy antymemetyczej Orbitera.
* Elena Verlen: wysokie uznanie ze strony Mariana Tosena z grupy antymemetyczej Orbitera.
* Martyn Hiwasser: wysokie uznanie ze strony Mariana Tosena z grupy antymemetyczej Orbitera.

## Zasługi

* Arianna Verlen: doszła do tego, że "Klaudia nie może myśleć" bo wpadnie pod infohazard. Planowała wysokopoziomowo jak radzić sobie z infohazardem na Alayi.
* Eustachy Korkoran: operator roju robotów (który sam skonstruował) celem dywersji, mapowania drogi i unieszkodliwiania. Twórca planów taktycznych.
* Klaudia Stryk: przejmuje kontrolę nad komputerami, pracuje z notatkami - składa informacje o infohazardzie z jakim mają do czynienia jako pierwsza pochodna, by nie wiedzieć z czym ma do czynienia. Amnestyki...
* Leona Astrienko: jej bezwzględna determinacja uratowała misję - zaakceptowała stratę Eleny, unieszkodliwiła Skażone Klaudię i Ariannę, wsadziła WSZYSTKICH do biovata z amnestykami i została na straży. Jak zaczęła wpadać pod infekcję, oddaliła się, by na pewno nie zrobić krzywdy reszcie załogi.
* Elena Verlen: wyszła z pozycji advancera by ostrzec zespół; jej omnidetekcja jednak sprawiła, że od razu wpadła pod infohazard. Jako infektant chroniła life support, próbując Skazić Eustachego.
* Martyn Hiwasser: MVP misji. Pracował z amnestykami i inhibitorami zapamiętywania, przygotowując mechanizmy odporności na anomalię memetyczną. Też: poświęcił się z Eustachym (sceny notatkowe), by Arianna i Klaudia zostały "czyste".
* Marian Tosen: ekspert anomalii memetycznych na K1, pod adm. Termią. Jak tylko dowiedział się że Alaya padła, zarekwirował Tivr i wziął ze sobą załogę Inferni. Trochę test, trochę potrzebuje wsparcia. Nie powiedział im o co chodzi (bo nie mógł - jakby wiedzieli, anomalia by ich pochłonęła).
* OO Alaya: statek zainfekowany anomalicznym infohazardem. Chciał przejść przez Bramę w krwawym rytuale. Marian Tosen wysłał elitarną załogę Inferni, by unieszkodliwili hazard i uratowali Alayę.
* OO Tivr: ultraszybka korweta zarekwirowana przez Mariana Tosena do szybkiego transportu magów Inferni (i Leony) na Alayę. Normalnie stacjonarna na K1.

## Frakcje

* Orbiter Negatech: po odkryciu problemu z anomalią memetyczną na OO Alaya, wysłali Tosena i Infernię do unieszkodliwienia Alayi raz na zawsze.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 5
* Dni: 11

## Inne

.

## Konflikty

* 1 - Arianna próbuje wyciągnąć z Mariana Tosena z czym mają do czynienia, czym jest ta straszna anomalia na którą lecą Tivrem. Klaudia jej pomaga.
    * ExZ+2
    * XVz: Need to know. A jak wiecie o tej anomalii, jest gorzej. To jedna z moich specjalizacji, anomalie memetyczne.
* 2 - Eustachy składa rój robotów które mają znaleźć bezpieczną drogę po Alayi do Med1.
    * TrMZ+2
    * VmV: rój mikrorobotów do wielokrotnego wykorzystania
    * KONTYNUACJA KONFLIKTU (działanie robotów, sterowanie): VVXO: Elena (18oczu) przechwyciła jedną z dron, ale Eustachy znalazł drogę.
* 3 - Eustachy składa amunicję amnestyczną z Martynem. Wymazanie pamięci do momentu na Tivrze.:
    * ExZ+3
    * Vz
* 4 - Eustachy wysyła drona do med-1, by tam ściągnąć Skażonych ludzi i odwrócić uwagę od pełznącej po kanałach (do med-3) Klaudii
    * TrZ+3
    * XVzVz: super dywersja, acz stracili kolejną dronę. Zostały tylko 2 + wybuchowa.
* 5 - Klaudia w med-3 przejmuje kontrolę nad komputerami, kopiuje notatki z med-1 do med-3 i zabiera je ze sobą. Praca z komputerami.
    * TrZ+3
    * XVVz: Klaudia czuje OBECNOŚĆ, ale udało jej się to skopiować i używa anomalii przestrzennej by to co na nią PATRZY się złapało i nie mogło wyjść
* 6 - Klaudia patrzy na wszystkich, Arianna -> na Eustachego. Eustachy -> na Martyna. Martyn ogląda nagranie. Cel: zrozumieć co z ważnych faktów jest w usuniętych notatkach.
    * TrZ+3
    * XXVV: korupcja wszystkich poza Klaudią, ale Klaudia zapisała notatkę opisującą notatkę. Rozwiązaniem jest chaotyzator + amnestyk.
* 7 - Eustachy zdecydował się porwać Skażonego randoma by sprawdzić na nim hipotezy. Wyszedł na korytarz i poluje. Wpierw używa czujek na nadgonienie ofiary, potem strzela. By trafić.
    * TrZ+2
    * VXV: złapał kogoś, ale jego Lancer został uszkodzony. Jeden delikwent porwany, reszta nie żyje (6 ofiar zestrzelonych przez Eustachego)
* 8 - Martyn robi odpowiednie amnestyki + inhibitory zapamiętywania, by być w stanie usunąć anomalię z Alayi.
    * ExMZ+4
    * XVmVz: duża, dupna butla co może wybuchnąć, ale sukces - wszyscy będą nieprzytomni.
* 9 - Eustachy robi dywersję dronami. Odwrócić uwagę Eleny od life support. Dywersja z koszulą Eustachego + odpowiedni środek na dronie wybuchowej by ją zamnestyzować.
    * ExZ+4+3O
    * XzOXVV: Elena zniszczyła dronę na zasięg, ale poleciała na koszulę. Eustachy czuje OBECNOŚĆ Eleny po sympatii. Ale neutralizacja Eleny amnestykami się udała i Eustachy się utrzymał. DLA MISJI!
