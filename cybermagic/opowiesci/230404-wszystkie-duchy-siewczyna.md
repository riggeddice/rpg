## Metadane

* title: "Wszystkie duchy Siewczyna"
* threads: hiperpsychotronika-samszarow, karolinus-heart-viorika
* motives: hot-potato-nuke, tien-na-wlosciach, echa-inwazji-noctis, stare-kontra-nowe, pomocne-duchy-samszarow
* gm: żółw
* players: kić, anadia, kamilinux

## Kontynuacja
### Kampanijna

* [230328 - Niepotrzebny ratunek Mai](230328-niepotrzebny-ratunek-mai)
* [230418 - Żywy artefakt w Gwiazdoczach](230418-zywy-artefakt-w-gwiazdoczach)

### Chronologiczna

* [230328 - Niepotrzebny ratunek Mai](230328-niepotrzebny-ratunek-mai)

## Plan sesji
### Theme & Vision & Dilemma

* After Forever "Discord"
    * You need to get out of my sight | (A lonely heart can beat) You're too dangerous (A lonely heart can bleed) | You take the life out of me, no heart is broken forever | (Its broken promises, but my faith is broken forever)
* Horrida "W niepamięć"
    * Miałeś cierpieć tak, jak cierpiałam kiedyś ja
* CEL: pokazanie specyfiki rodu Samszar
    * KONTRAST z Verlen: Verlenowie walczą z potworami, Samszarowie harmonizują z naturą
    * Harmonia z duchami

### Co się stało i co wiemy

* Kontekst: miasto Siewczyn
    * Siewczyn to miasto przemysłowe słynące z produkcji sprzętu rolniczego oraz długiej tradycji szamanistycznej. 
    * Przed współpracą między duchami i ludźmi, ludzie próbowali zdobyć te ziemie i walczyli z duchami które odpowiadały walką (Mononoke Hime); obie strony walczyły ze sobą, powodując wzajemne krzywdy.
    * Wtedy to Mieszko Samszar odkrył problem i przekonał ludzi do współpracy z duchami, co doprowadziło do zjednoczenia miasta i powstania symbolu Drzewa Jedności
    * Siewczyn zasłynął z harmonii między ludźmi a duchami, co sprzyjało jego rozwojowi.
    * około 20k-30k ludzi
    * Opis: (na dole)
* Egzorcysta Irek Kraczownik nie dał rady pokonać dziwnego ducha w innym mieście (Hybryda: bojowe AI z wojen noktiańskich które sprzęgło się z lokalnym duchem) w innym mieście 
    * więc go tu przyniósł by Siewczyn, bardzo mocno harmoniczne miasto rozwiązało problem ducha samo
    * niestety, po 3 latach hybryda między duchem i AI skutecznie wpłynęła na lokalne duchy prowadząc do dysharmonii
        * zaczęły pojawiać się problemy
            * awaria maszyn w fabryce siewników, co spowodowało poważne obrażenia u kilku pracowników
            * ciernie wypijające krew w parku (korupcja duchów)
            * duchy nie chcą rozmawiać z ludźmi. Boją się dotyku Ciernia.
            * maszyny rolnicze mają fatalne wyniki, nie działają dobrze.
    * Hybryda kiedyś była TAI drony noktiańskiej, która próbowała chronić swoich ludzi. Wszyscy zginęli. W ogniu. Nie ma po co istnieć. Jest tylko zemsta.
        * Julio Farcinias, ojciec, "zawsze miał czas dla dzieci nawet jak były problemy na statku kosmicznym"
        * Kalitias Farcinias, dziecko, "nigdy nie pozwalał sobie wejść na głowę, zawsze protestował i chciał debatować"
        * Hybryda nie pamięta swego imienia. Ale to ICH wina. 

### Co się stanie (what will happen)

* S01
    * Duchy na polu stają przeciwko nowej sadzarce, próbują ją odeprzeć, wbrew ludziom
        * -> okazuje się, że duchy próbują pomóc. Sadzarka jest przeklęta.
        * ludzie krzywdzą duchy starymi rytuałami odpychającymi duchy
            * Adam Pacztuk, starszy człowiek, mówi że duchy walczą przeciwko ludziom
                * nie jest zwolennikiem współpracy
* S02
    * Na wielu polach są robione już rytuały odpędzające duchy, zainspirowane przez Sforzeczoka
        * złość ludzi też wynika ze Skażonej aury miasta; Hybryda skutecznie demonstrowała brak możliwości jedności
* S03
    * Duchy zmierzają ku domowi bogatego Filipa. On krzywdził duchy. DF: Filip nie żyje
    * Gałęzie w parku spadają na samochody, które manewrują. Ale ZANIM Maria będzie trafiona, inny samochód leci i się wbija, wyraźnie ratując Marię. Hybryda NIE zrani Marii.
* S04
    * Traktor zagłady: próbuje dewastować miasto. Człowiek za sterami traktora nie wie co robić i jest uwięziony. DF: miasto uszkodzone
    * Hybryda próbuje atakować duchy. DF: Siewczyn lekko zdebalansowany
    * Hybryda próbuje Skazić sprzedawane rzeczy. Cel: zniszczenie ekonomii miasta

### Sukces graczy (when you win)

* rozwiązanie problemu

## Sesja - analiza

### Fiszki

* Elena Samszar: czarodziejka origami i kinezy, <-- objęta przez Anadię
    * Archiwistka, biurokracja, prawo i dokumenty
    * Jej papier potrafi zrobić krzywdę
    * W wolnym czasie robię rzeczy z papieru, wieże, łabędzie itp.
    * A teraz, ku wielkiemu smutkowi, pomagam stanąć na nogi kuzynowi Karolinusowi >.>
* .Piotr Drzewicznik: Police Officer
    * ENCAO: 0++-+ | Observant, silent, and assertive;; emotionally stable | Security, Conformity > Hedonism
    * MovieActor: Captain America (Steve Rogers) from "Captain America"
    * Deed: When Michael Knight and KITT arrived in Podwiert, Piotr provided them with crucial information about the recent incidents
    * Deed: During a high-stakes pursuit, Piotr put his own safety at risk to save a civilian from a collapsing building, demonstrating his commitment to his job and the community.
    * Deed: When dealing with a difficult suspect, Piotr maintained his composure and professionalism, eventually convincing the individual to cooperate with the authorities.
* .Maria Seveleran: Art teacher, dobre serduszko, noktianka
    * ENCAO: +0-++ | Imaginative, warm, and empathetic;; disorganized | Self-Direction, Universalism > Achievement
    * MovieActor: Luna Lovegood from "Harry Potter"
    * Deed: Maria organized an art exhibition showcasing her students' work, which helped raise funds for a local charity.
    * Deed: Maria's love for art and creativity has inspired her children to pursue their own interests and passions.
* .Wojciech Młynarkiewicz: Archivist
    * ENCAO: 0+0++ | Creative, reliable, and introverted;; diplomat;; sensitive to stress | Benevolence, Tradition > Stimulation
    * MovieActor: Bilbo Baggins from "The Hobbit"
    * Deed: When Team was searching for information about the powerful industrial spirit, Wojciech provided them with historical documents that shed light on the spirit's origins, helping them understand its motives.
    * Deed: When a coworker needed assistance with a time-sensitive project, Wojciech stepped in to help, even though it meant working long hours and sacrificing his personal time.
    * Deed: Faced with a conflict between two colleagues, Wojciech acted as a mediator, using his diplomatic skills to resolve the issue and maintain a harmonious work environment.
* .Andrzej Kardamodon: Pracownik administracyjny w fabryce planterów i siewników
    * ENCAO: -0+0+ | Introwertyk, zorganizowany, lojalny; czasami nieśmiały
    * Postać filmowa: Neville Longbottom z "Harry Potter"
    * Czyn: Kiedy Andrzej odkrył błąd w dokumentacji dotyczącej nowej maszyny, uparcie zgłaszał ten problem pomimo początkowego zignorowania przez przełożonych, co ostatecznie zapobiegło awarii sprzętu.
* Maksymilian Sforzeczok: bogacz, który nie jest zwolennikiem dominacji magii; właściciel wielu pól i ziem
    * ENCAO: +--++ | Impatient, goal-oriented, ruthless, and dominant; struggles with empathy and understanding others | Power, Achievement > Universalism
    * MovieActor: Gordon Gekko from "Wall Street"
    * Values: Power, Achievement > Universalism
    * Optymalizuje produkcję do maksimum. Próbuje wszystko zautomatyzować. "Nie ma miejsca na ludzi a już na pewno nie ma dla duchów."
* Aleksander Klaczaszcz: Właściciel firmy produkującej siewniki i sadzarki
    * ENCAO: ++0+- | Życzliwy, spokojny, odpowiedzialny i dążący do harmonii; czasami ma problem z asertywnością. Przejmuje się wszystkim. | Universalizm, Samorealizacja > Hedonizm
    * MovieActor: Atticus Finch z "Zabić drozda"
    * Deed: współpracuje z miejscowymi szamanami, by upewnić się, że jego firma nie zakłóca równowagi duchowej
    * Deed: W przeszłości, Aleksander wykupił teren, na którym znajdowała się fabryka produkująca broń. Nie zneutralizował od razu, próbował rozwiązać samemu; spowodowało to kłopoty. Wtedy nauczył się, że na terenie Samszarów bezpieczniej współpracować z szamanami.
* .Hybryda
    * Poprzednie życie: noktiańska TAI bojowa i ochronna, dekadiańska. Oraz duch. Zintegrowani. Ściągnięta przez egzorcystę (Irka Kraczownika), który nie umiał tego rozwiązać.
    * Poprzedni użytkownicy (tych pamięta)
        * Julio Farcinias, ojciec, "zawsze miał czas dla dzieci nawet jak były problemy na statku kosmicznym"
        * Kalitias Farcinias, dziecko, "nigdy nie pozwalał sobie wejść na głowę, zawsze protestował i chciał debatować"
    * Hybryda nie pamięta swego imienia. Ale to ICH wina. 
    * Hybryda NIGDY nie uderzy w Marię.

### Scena Zero - impl

.

### Sesja Właściwa - impl

To wszystko co było wcześniej to były zaloty Karolinusa do Vioriki. Ogólna opinia. Verlenowie potraktowali zaloty... jak zwykle. Ale Karolinus nie zabił potwora w walce wręcz więc nie ma rozpoczęcia zalotów. Samszarowie przestraszyli się potencjalnej wojny. Więc w Siewczynie - jednym z bezpieczniejszych miast - zaczęło coś się pieprzyć - wysłali tam Karolinusa. "Jedź, załatw, napraw". W domyśle: "nie spieprz i nie miej wojny z Verlenami". Tam NIC NIE MOŻE BYĆ ZŁEGO. I daleko od granicy. I nawet Karolinus dostał "opiekę". Kuzynka. Elena Samszar. Z tych wyważonych, spokojnych...

Siewczyn jest spokojnym miastem ale ma Problemy Które Trzeba Naprawić. Pierwsze miejsce do znalezienia - knajpka. "Karczma Duchów". Strzała zrzuca Karolinusa w "Karczma Duchów", we Wzgórzach Harmonii. Barman się uśmiecha. "Co Tobie podać?". Karolinus pyta co da się tu zrobić. Dostał namiar na dyskotekę. K->E "pracą zajmiemy się jutro, daj mi dziś spokój".

Elena ma duży dom. Kilka pokoi, służba, wszystko przygotowane na wizytę magów rodowych. Uśmiech numer 71. Ci ludzie są służącymi Samszarów i prawidłowo działają. Miasto działa jak naoliwiona turystyczna maszyna. "tien Melisa Samszar zawsze życzyła sobie gorącą kąpiel na przyjazd więc jest gorąca woda". Elena poprosiła do wanny herbatkę i książkę.

Strzała wie, że są problemy których nie określono przyciemniła szyba i jedzie szukać problemów. I szuka wiadomości. Chce przeszukać przeszłe dane.

Tr Z (kody Samszarów itp) +4:

* V: Przechwyciłaś co następuje
    * nikt nie powiedział tutaj nikomu, że przyjeżdżacie. Tylko Rezydencja wiedziała. Sygnał nie dotarł do centrali.
    * zaczęły pojawiać się problemy od 2 miesięcy, nasilają się. Te są najistotniejsze. Część spotyka też turystów.
            * ciernie wypijające krew w parku (korupcja duchów)
            * awaria maszyn w fabryce siewników, co spowodowało poważne obrażenia u kilku pracowników
            * duchy nie chcą rozmawiać z ludźmi. Boją się dotyku Ciernia.
            * maszyny rolnicze mają fatalne wyniki, nie działają dobrze.
* V: Jest wzór. Coś jest nie tak na linii technologia - duchy. Lub harmonia - duchy.
* X: (odraczam). Teren jest przybliżony, to będą lokalizacje przemysłowe albo dookoła "gajów".
* V: SCENA

Strzało, przejeżdżając, zauważyłaś traktor z sadzarką. Duchy próbują odeprzeć sadzarkę i traktor, wygnać je z pola. Ludzie już opuścili pojazd. Żywiołak Ziemi próbuje odegnać sadzarkę. Ludzie rzucają kamieniami w żywiołaka. Strzała chce zrozumieć sytuację. Żywiołak chce podnieść i odrzucić sadzarkę i traktor.

* Strzała -> Karolinus: "tien Samszar"
* K: "jestem, co jest?"
* S: (streszczenie sytuacji) "Czy mam coś zrobić czy poczekać i zbierać dane."
* K: "ratuj ludzi nie maszyny. Jak akcja się skończy, przyjedź po mnie"

Żywiołak odrzucił traktor i sadzarkę, rozproszył się w glebę. Ludzie wściekli na duchy. Gdy pozbył się maszyny z terenu, zniknął. Żeby duchy samoistnie sformować żywiołaka musiały użyć UPIORNEJ energii. Strzała "tien Samszar ZAPRASZA" i bierze ochotnika. Koleś nazywa się Andrzej Ładowiecz, uważa, że "duchy to chuje" i nie dają pracować. To były dobre ziarna, z lokalnego spichlerza. "Jeden duch obraził się na drugiego tak?!"

Strzała przywozi Ładowiecza do Karolinusa. Jedziecie do spichlerza z duchem opiekuńczym. I faktycznie, jest tu Strażnik - potężny byt o własnym umyśle, pozytywny. Karolinus poprosił Strzałę o przywiezienie Eleny. Karolinus nie jest jak typowy tien. Karolinusa nie będą się bać jak Eleny. I faktycznie, ludzie mówią Karolinusowi - duchy i ludzie nie współpracują ze sobą. Duchy się obracają przeciw ludziom. Ale nie NASZ duch - Strażnik Spichlerza.

Karolinus pobiera próbkę z magazynu, ze Spichlerza i bada i porównuje. I jedzie Strzała z Eleną. Coś zwróciło uwagę Eleny - na jednym z pól koło którego jedziecie jest robiony przez ludzi rytuał. Sól, krew i gesty. Bardzo prymitywny. Rytuał który krzywdzi duchy i je odpędza. Elena poszła z nimi porozmawiać i zażądała odpowiedzi. Elena dopytuje co oni robią "kolejna pierdolona turystka". Dyrektor Sforzeczok kazał. Elena czuje, że energia duchów na tym polu jest słaba. To nie był pierwszy rytuał tego typu. I są uzbrojeni w rzeczy krzywdzące efemerydy. Koleś chciał odepchnąć Elenę, Strzała przed nią i 'vrooom'.

Rytuał został USZKODZONY?

Tr Z +2 +7Og:

* Vz: duchy próbowały coś sformować, ale mimo uszkodzonego rytuału nie udało się. Pole jest za słabe.

Tien Elena Samszar pokazuje runę i zastrasza, mówiąc jaki paragraf. Chcecie się przeciwstawić rodowi Samszar? Przesłuchuje.

Tp Z (kamery) +4:

* V: 
    * dyrektor kazał zniszczyć duchy starymi rytuałami, od miesiąca, na jego polach (pokrywa się)
    * duchy walczyły, część ludzi poraniona
        * to są ludzie "najemni". Tani.
* V: bez kłopotu używając lokalnej policji da się rozpracować całą siatkę i dowiedzieć się kto, kiedy, gdzie itp
    * wszystkie pola tego dyrektora są rytualizowane
    * część mniejszych właścicieli mających problem z duchami też rytualizuje, ale poszli za śladem dyrektora
    * nie wszyscy. Większość nie. Masz listę.
* XX: dyrektor Sforzeczok ma okazję zacząć niszczyć dowody
* V: Elena dostaje cenną wiadomość - Sforzeczok ściąga inne ziarna, nie pochodzą ze Spichlerza. Są "lepsze".

Strzała próbuje znaleźć wzór między tym kto rytualizuje pola

* V: 
    * ani jeden właściciel pól którzy żyją "na obrzeżach" miasta nie rytualizuje pól
    * właściciele mieszkający w konkretnym fragmencie miasta we Wzgórzach - część z nich nieważne jakich ziaren używa to rytualizuje
* V:
    * trzy miesiące temu dyrektor Sforzeczok otworzył nowoczesną automatyczną restaurację bez żadnego człowieka. Jest tania. 
    * bardzo silny predyktor, że ci ludzie faktycznie tam jedli. I się stołują częściej.
    * Strzała upewniła się - jedzenie jest legitne. Sprowadzane, odgrzewane maszynowo itp.
    * dyrektor miał przez to problemy, bo "nie ma duchów itp" ale się prawnie wybronił

Zespół się połączył, przy Spichlerzu. Po odrobinie złośliwości między kuzynostwem Elena przygotowuje się do rzucenia zaklęcia. Będzie gadać ze Strażnikiem Spichlerza.

Tr M Z (duch który chce) +2 +3Ob:

* V: duch odpowiedział na Twoje zapytanie
    * nowe ziarno - Duch Spichlerza Dotknął to ziarno. Ono jest... ono wyjaławia glebę. Świetny yield, bardzo dobre, nie ma tej... długotrwałości. Jest nienaturalne.
        * 10 lat monokultury to katastrofa. Dla duchów to jest kłopot i to zwalczają.
    * zdaniem Ducha Spichlerza problem jest od co najmniej roku jak nie dwóch
    * nad miastem jest czarna chmura. Jest tam problem duchowy.
    * nawet Duch Spichlerza jest słabszy niż był. Czarna Chmura wysysa.
* (+1Vg bo jest oczytana) Xm
    * Duch Spichlerza próbuje pokazać Elenie co tam jest. Z ufnością zdjął część barier
    * Eteryczny pasożyt jakiegoś typu wniknął w strukturę Ducha Spichlerza
    * Połączenie magiczne Eleny i Ducha Spichlerza się zerwało
    * Duch Spichlerza ulega korozji i korupcji. Pasożyt go zastępuje i się nim żywi
        * to jest inny Duch. Nie stąd. To jest Duch którego ktoś WSADZIŁ.
* V: Elena z trudem ale trzyma połączenie ZANIM Duch Spichlerza zjedzony
    * "oni spłonęli w ogniu" -> wizja TAI i ludzi którzy płonęli, których TAI noktiańska próbowała uratować
        * plany zniszczenia tego wszystkiego
    * jakiś debil egzorcysta próbował zniszczyć Hybrydę. Nie udało mu się. Więc przyniósł ją tu 3 lata temu by miasto to rozwiązało.
* Vz: Elena naciska, niech Duch Spichlerza, który i tak staje się wrogi jeszcze się przyda
    * Hybryda wykorzystała automatyczną restaurację. Nasyca ludzi nienawiścią którą czuje.
    * Hybryda potrafi opętywać maszyny.
    * Hybryda potrafi wypaczać duchy i wzbudzać w nich nienawiść.
    * Hybryda Skaziła planter. Żywiołak chciał chronić ziarna i glebę.
    * Hybryda chce śmierci w płomieniach wszystkiego i wszystkich.
    * Egzorcysta nazywa się Irek Kraczownik. 3 lata temu spieprzył. Nie operuje na terenie Samszarów.
* Vz: Elena jest zwolenniczką ostrego działania. RANI Ducha Spichlerza i Hybrydę w tle, po czym się rozłącza.
    * Karolinus ma możliwość uratować to, co Duch - jak jeszcze był sobą - chronił 80 lat...
* X: Hybryda wzrosła w siłę. Zasiliła się energią Ducha i Duch do niej dołączył jako jej aliant.
* Vm: Elena zintegrowała się z Hybrydą (ale ona jest bezpieczna, jeszcze chroniona przez Ducha Spichlerza)
    * Elena pokazała JEGO! TO ON! IREK! ON TO ZROBIŁ! ON ZABIŁ LUDZI!
    * Jak Hybryda zabije TEGO CZŁOWIEKA to jej misja się skończy i Hybryda się zemści.

Karolinus nie pozwoli, by nic złego spotkało ziarna i Spichlerz. Strzała proponuje że ona weźmie walkę psychotroniczną przeciwko Hybrydzie by kupić czas.

Tr Z (Strzała jest Ci znana i sobie ufacie) M +4 +5Ob:

* X: Strzała jest uszkodzona psychotronicznie i duchowo. Będzie musiała się zreperować. Autorepair wystarczy.
* V: Strzała jest zabezpieczona. Jest w stanie walczyć psychotronicznie z Hybrydą.
* Ob: nadmiar energii manifestuje się jako rzadko pojawiająca się (lol, często) wizualizacja uśmiechniętej Vioriki. Taka 'cute Viorika Verlen'. W bikini. Manifestacje losowe.
* Ob: 'cute Viorika Verlen' staje się strażniczką tego terenu, jedną z pozytywnych manifestacji i atrakcji turystycznych.
* Vr: Strzała jest w stanie w pełni pochłonąć uwagę Hybrydy aż ściągnie się jednostkę do walki psychotronicznej. Po ściągnięciu takiej jednostki Strzała się odblokuje. I Hybryda nie zrobi dużo więcej zła, jej plany są spowolnione, tu na szybko ściągnie się innego Samszara a Zespół pojedzie polować na egzorcystę...

## Streszczenie

Karolinus, wraz z kuzynką Eleną Samszar zostali wysłani do Siewczyna gdzie podobno są problemy (by uniknąć dalszego antagonizowania Verlenów). Na miejscu okazało się, że niekompetentny egzorcysta Irek sprowadził tu Hybrydę noktiańskiej TAI i ducha oraz ten byt próbował zemścić się za śmierć swoich ludzi. Elena Paradoksem zniszczyła Ducha Opiekuńczego Spichlerza a Karolinus Paradoksem stworzył strażniczego ducha w kształcie Vioriki w bikini. A w tle - spory między ludźmi i duchami (podsycane przez Hybrydę) oraz między podejściem 'ekonomia vs harmonia'.

## Progresja

* Elena Samszar: zniszczyła lokalnego Ducha Strażniczego Spichlerza, który istniał 80 lat. W Siewczynie jej tego nie zapomną...
* Karolinus Samszar: stworzył lokalnego Strażnika, ducha w formie Vioriki w bikini z karabinem. W Siewczynie i w Verlenlandzie mu tego nie zapomną...

## Zasługi

* Karolinus Samszar: integrował się z normalnymi ludźmi przez picie, ale jak doszło co do czego to magią osłonił Strzałę by ta mogła walczyć z Hybrydą. Zrobił kolejną trwałą manifestację Vioriki...
* Elena Samszar: przesłuchiwała jako tienka ludzi pracujących dla lokalnego dyrektora nie lubiącego magii; magicznie połączyła się ze Strażnikiem Spichlerza i Paradoksem dała Hybrydzie owego Strażnika zniszczyć. Ale przekonała Hybrydę, że egzorcysta jest winny i kupiła czas Karolinusowi i Strzale.
* AJA Szybka Strzała: odkryła konflikt między duchami a ludźmi, samodzielnie wykonywała zwiady i weszła w wojnę psychotroniczną z Hybrydą AI-ducha.
* Maksymilian Sforzeczok: 'dyrektor' który zarządza niektórymi polami. Wprowadził do Siewczyna automatyczną restaurację (opętaną przez Hybrydę) i starymi rytuałami zwalcza duchy z pól by wprowadzić bardziej efektywne ziarno którego nie akceptują duchy. Pali dowody by nie mieć kłopotów z Samszarami.
* Irek Kraczownik: niekompetentny egzorcysta, który przypałętał na ten teren Hybrydę noktiańskiej TAI i ducha zemsty 3 lata temu. Nieobecny na TEJ sesji, acz jej przyczyna.

## Frakcje

* Hiperpsychotronicy: badana przez nich Hybryda przez Elenę S. się przekształciła i wzmocniła, ale też dostała nowe interesujące cechy. Mają więcej lepszych okazji do badań.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Siewczyn: an industrial city with a rich shamanistic heritage, balances its focus on manufacturing agricultural equipment with a deep respect for nature and harmony. Pozyskał nowego Strażnika - ducha w kształcie Vioriki w bikini z karabinem.
                                1. Wzgórza Industrialnej Harmonii (NW)
                                    1. Fabryka traktorów
                                    1. Fabryka siewników i sadzarek
                                    1. Przestrzeń mieszkalna
                                    1. Sklepy i centra handlowe
                                1. Astralne Ogrody (ES)
                                    1. Fabryka neutralizatorów astralnych
                                    1. Centrum R&D dla zrównoważonego rolnictwa
                                    1. Centralne Biura Rolnicze: większość firm
                                    1. Dom Szamana: historical building serves as a cultural and educational center
                                    1. Rezydencje Harmonii: domy dla bogatych ludzi
                                    1. Gaj Duchów: miejsce, w którym najłatwiej się połączyć z duchami i z nimi skomunikować
                                1. Centrum Jedności Mieszka (Center)
                                    1. Most jedności: widok na połączenie dwóch rzek; Plerny i Czamkarii.
                                    1. Ratusz miejski
                                    1. Drzewo Jedności: żywy pomnik adaptacji Mieszka Samszara do problemów z duchami.
                                    1. Park miejski
                                    1. Bazar rękodzieła
                                1. Północne obrzeża
                                    1. Spichlerz Jedności: wielki magazyn ziaren. Silosy, przenośnik itp. Główny duch opiekuńczy, Strażnik Spichlerza, został zniszczony przez Hybrydę.

## Czas

* Opóźnienie: 16
* Dni: 2

## OTHER
### Fakt Lokalizacji
#### Miasto Siewczyn

Siewczyn to malownicze miasto przemysłowe o bogatej historii, otoczone wzgórzami i żyznymi polami. W centralnej części miasta znajduje się imponujący Stary Rynek z pięknymi kamienicami, które od lat są świadkami lokalnych wydarzeń. Wokół rynku rozmieszczone są liczne sklepy, kawiarnie i restauracje, które tętnią życiem przez cały dzień.

Miasto przecina rzeka, nad którą przerzucono urokliwe, kamienny most. Wzdłuż rzeki znajdują się malownicze deptaki oraz parki, które stanowią idealne miejsce na relaks i spacery. Siewczyn słynie również z licznych zabytków, takich jak kościoły, pałace i muzea, które przyciągają turystów z całego kraju.

W zachodniej części miasta, oddzielonej od centrum przez rzekę, znajduje się dzielnica przemysłowa, w której produkowane są maszyny rolnicze, takie jak siewniki i sadzarki. W tej części Siewczyna znajdują się również fabryki Traktorów Przyjaznych Naturze oraz Neutralizatorów Astralnych. Pomimo swojego przemysłowego charakteru, dzielnica ta ściśle współpracuje z lokalnymi szamanami, dbając o harmonię z duchami i przyrodą.

Na obrzeżach miasta, wśród pagórków i lasów, znajdują się liczne ścieżki spacerowe oraz miejsca kultu szamańskiego, gdzie mieszkańcy odprawiają rytuały i uczą się współpracy z duchami przyrody. W Siewczynie królują zielenie, które kontrastuje z ceglanymi i kamienistymi elementami architektury, tworząc niepowtarzalny klimat tego miasta.
