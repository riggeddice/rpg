## Metadane

* title: "Bladawir kontra przemyt tienów"
* threads: triumfalny-powrot-arianny, program-kosmiczny-aurum, naprawa-swiata-przez-bladawira
* motives: zdrajcy-eksterminacja, the-hunter, beznadziejny-szef, starry-eyed-novices, weteran-genialny
* gm: żółw
* players: fox, kić, kapsel

## Kontynuacja
### Kampanijna

* [231129 - Niemożliwe nieuczciwe ćwiczenia Bladawira](231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)

### Chronologiczna

* [231129 - Niemożliwe nieuczciwe ćwiczenia Bladawira](231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA: 
    * Amaranthe "Razorblade"
        * "Going up to the top, never stop | Never give it up, never fall, never drop | You gotta give it up to the beat of your heart | And if you don't, we take it from the start"
        * Bladawir oczekuje od wszystkich za dużo, łącznie ze sobą. nie ufa nikomu i niczemu.
* Opowieść o (Theme and vision)
    * Bladawir jest bezwzględnym potworem i skupia swoją uwagę na wyczyszczeniu Orbitera. Szuka źródła przemytu tienów poza planetę.
    * SUKCES
        * .
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * zdrajcy-eksterminacja: Bladawir zniszczy wszystkich stojących naprzeciw Orbitera - łącznie z kapitanem Szymonem Orzesznikiem (kolega Kurzmina).
    * the-hunter: 'Ioragamis', stworzona przez Zaarę Mieralit 'kiedyś-TAI-Elainka' do zlokalizowania tienów na Karsztarinie. Z rozkazu Bladawira ma zbudować link Orzesznik - Arianna. Poświęcona na zniszczenie.
    * beznadziejny-szef: Bladawir, jakby POWIEDZIAŁ Ariannie że podejrzewa Orzesznika o współpracę z Syndykatem, Arianna zadziałałaby zupełnie inaczej.
    * starry-eyed-novices: grupa schowanych tienów (4 osoby), pragnących dołączyć do programu kosmicznego, ukrywający się przed Orbiterem na Karsztarinie.
    * weteran-genialny: Szymon Orzesznik, który przy użyciu Orbitera robi kanał przerzutowy dla tienów przy użyciu stoczni Neotik.
* Detale
    * Crisis source: 
        * HEART: "Bladawir, potworny komodor-inkwizytor" ORAZ "Orzesznik który pragnie by wszyscy mieli prawo do kosmosu"
        * VISIBLE: "Bladawir zastawia pułapkę na statki cywilne które transportują tienów na Valentinę"
    * Delta future
        * DARK FUTURE:
            * chain 1: Orzesznik ginie z rąk Inferni, strzelając do niej pierwszy
            * chain 2: Arianna becomes a monster, isolated by her world
        * LIGHT FUTURE:
            * chain 1: Orzesznik jest aresztowany przez Bladawira ALBO ucieka z Orbitera
            * chain 2: ?
    * Dilemma: brak, sesja funkcjonalna

### Co się stało i co wiemy

* Przeszłość
    * 
* Teraźniejszość
    * .

### Co się stanie (what will happen)

* Problem Bladawira
    * jak wypłoszyć Orzesznika i go aresztować?
        * Arianna. Jeśli wykona polecenia, dobrze. Jak nie, będzie jego fregata 'Adilidam'.
        * musi ZROZUMIEĆ jak szybko Arianna operuje Infernią
    * jak udowodnić, że Orzesznik współpracuje z Syndykatem?
        * musi dojść do przemytu albo prób
    * jak upewnić się, że jego jednostki są w stanie robić wszystko wystarczająco dobrze?
        * trening, testy, monitoring
* F1: Bladawir testuje Infernię i swoje jednostki
    * Vision
        * Bladawir: czy są w stanie osiągnąć odpowiednią PRĘDKOŚĆ?
        * Bladawir: czy są w stanie zbadać każdą jednostkę opuszczającą Karsztarin?
            * (Zaara na korwecie Isratazir pd. Waritnieza)
            * (Samuel Fanszakt na Actazir pd. Darbika)
        * Bladawir szuka jednostek opuszczających Karsztarin i tylko tych. Pod kątem obecności konkretnej sygnatury energii.
            * Infernia, Isratazir, Actazir nie mają za zadanie oficjalnie skanować tych jednostek
            * Tego jest ZA DUŻO
            * (tymczasem The Hunter robi presję na Orzesznika)
    * CEL
        * pokazanie, że Bladawir czegoś SZUKA
        * pokazanie, że ludzie Bladawira robią to na czym mu zależy
    * stakes
        * wyczerpanie, zniszczenie morale
        * reputacja Arianny jako piesek Bladawira
        * możliwość wyjścia na najlepszych anty-przemytowców
    * opponent
        * nieludzkie metody Bladawira, jego determinacja
    * problem
        * samotność i nieufność Bladawira
    * koniec fazy
        * docieramy do Karsztarina
* F2: Szukanie przemytu na Karsztarinie
    * CEL
        * wykorzystanie Arianny do WYPŁOSZENIA Orzesznika i odkrycia jak to robi
        * może zaskarbienie sympatii Arianny u Orzesznika?
        * Orzesznik to przyjaciel Kurzmina; rozbicie linka?
    * stakes
        * 
    * opponent
        * 'Ioragamis', stworzona przez Zaarę Mieralit 'kiedyś-TAI-Elainka'
    * problem
        * 
    * koniec fazy
        * 

## Sesja - analiza

### Fiszki

Zespół:

* Leona Astrienko: psychotyczna, lojalna, super-happy and super-dangerous, likes provocative clothes
* Martyn Hiwasser: silent, stable and loyal, smiling and a force of optimism
* Raoul Lavanis: ENCAO: -++0+ | Cichy i pomocny;; niewidoczny| Conformity Humility Benevolence > Pwr Face Achi | DRIVE: zrozumieć
* Alicja Szadawir: młoda neuromarine z Inferni, podkochuje się w Eustachym, ma neurosprzężenie i komputer w głowie, (nie cirrus). Strasznie nieśmiała, ale groźna. Ekspert od materiałów wybuchowych. Niepozorna, bardzo umięśniona.
    * OCEAN: N+C+ | ma problemy ze startowaniem rozmowy;; nieśmiała jak cholera;; straszny kujon | VALS: Humility, Conformity | DRIVE: Uwolnić niewolników
* Kamil Lyraczek
    * OCEAN: (E+O-): Charyzmatyczny lider, który zawsze ma wytyczony kierunek i przekonuje do niego innych. "To jest nasza droga, musimy iść naprzód!"
    * VALS: (Face, Universalism, Tradition): Przekonuje wszystkich do swojego widzenia świata - optymizm, dobro i porządek. "Dla dobra wszystkich, dla lepszej przyszłości!"
    * Styl: Inspirujący lider z ogromnym sercem, który działa z cienia, zawsze gotów poświęcić się dla innych. "Nie dla chwały, ale dla przyszłości. Dla dobra wszystkich."
* Horst Kluskow: chorąży, szef ochrony / komandosów na Inferni, kompetentny i stary wiarus Orbitera, wręcz paranoiczny
    * OCEAN: (E-O+C+): Cichy, nieufny, kładzie nacisk na dyscyplinę i odpowiedzialność. "Zasady są po to, by je przestrzegać. Bezpieczeństwo przede wszystkim!"
    * VALS: (Security, Conformity): Prawdziwa siła tkwi w jedności i wspólnym działaniu ku innowacji. "Porządek, jedność i pomysłowość to klucz do przetrwania."
    * Core Wound - Lie: "Opętany szałem zniszczyłem oddział" - "Kontrola, żadnych używek i jedność. A jak trzeba - nóż."
    * Styl: Opanowany, zawsze gotowy do działania, bezlitośnie uczciwy. "Jeśli nie możesz się dostosować, nie nadajesz się do tej załogi."
* Lars Kidironus: asystent oficera naukowego, wierzy w Kidirona i jego wielkość i jest lojalny Eustachemu. Wszędzie widzi spiski. "Czemu amnestyki, hm?"
    * OCEAN: (N+C+): bardzo metodyczny i precyzyjny, wszystko kataloguje, mistrzowski archiwista. "Anomalia w danych jest znakiem potencjalnego zagrożenia"
    * VALS: (Stimulation, Conformity): dopasuje się do grupy i będzie udawał, że wszystko jest idealnie i w porządku. Ale _patrzy_. "Czemu mnie o to pytasz? Co chcesz usłyszeć?"
    * Core Wound - Lie: "wiedziałem i mnie wymazali, nie wiem CO wiedziałem" - "wszystko zapiszę, zobaczę, będę daleko i dojdę do PRAWDY"
    * Styl: coś między Starscreamem i Cyclonusem. Ostrożny, zawsze na baczności, analizujący każdy ruch i słowo innych. "Dlaczego pytasz? Co chcesz ukryć?"
* Dorota Radraszew: oficer zaopatrzenia Inferni, bardzo przedsiębiorcza i zawsze ma pomysł jak ominąć problem nie konfrontując się z nim bezpośrednio. Cichy żarliwy optymizm. ŚWIETNA W PRZEMYCIE.
    * OCEAN: (E+O+): Mimo precyzji się gubi w zmieniającym się świecie, ale ma wewnętrzny optymizm. "Dorota może nie wiedzieć, co się dzieje, ale zawsze wierzy, że jutro będzie lepsze."
    * VALS: (Universalism, Humility): Często medytuje, szuka spokoju wewnętrznego i go znajduje. Wyznaje Lichsewrona (UCIECZKA + POSZUKIWANIE). "Wszyscy znajdziemy sposób, by uniknąć przeznaczenia. Dopasujemy się."
    * Core Wound - Lie: "Jej arkologia stanęła naprzeciw Anomaliom i została Zniekształcona. Ona uciekła przed własną Skażoną rodziną" - "Nie da się wygrać z Anomaliami, ale można im uciec, jak On mi pomógł."
    * Styl: Spokojna, w eleganckim mundurze, promieniuje optymizmem 'że z każdej sytuacji da się wyjść'. Unika konfrontacji. Uśmiech nie znika z jej twarzy mimo, że rzeczy nie działają jak powinny. Magów uważa za 'gorszych'.
    * metakultura: faerilka; niezwykle pracowita, bez kwestionowania wykonuje polecenia, "zawsze jest możliwość zwycięstwa nawet jak się pojawi w ostatniej chwili"

Castigator:

* Leszek Kurzmin: OCEAN: C+A+O+ | Żyje według własnych przekonań;; Lojalny i proaktywny| VALS: Benevolence, Humility >> Face| DRIVE: Właściwe miejsce w rzeczywistości.
* Patryk Samszar: OCEAN: A+N- | Nie boi się żadnego ryzyka. Nie martwi się opinią innych. Lubi towarzystwo i nie znosi samotności | VALS: Stimulation, Self-Direction | DRIVE: Znaleźć prawdę za legendami.
    * inżynier i ekspert od mechanizmów, działa świetnie w warunkach stresowych

Siły Bladawira:

* Antoni Bladawir: OCEAN: A-O+ | Brutalnie szczery i pogardliwy; Chce mieć rację | VALS: Power, Family | DRIVE: Wyczyścić kosmos ze słabości i leszczy.
    * "nawet jego zwycięstwa mają gorzki posmak dla tych, którzy z nim służą". Wykorzystuje każdą okazję, by wykazać swoją wyższość.
    * Doskonały taktyk, niedościgniony na polu bitwy. Jednocześnie podły tyran dla swoich ludzi.
    * Uważa tylko Orbiterowców i próżniowców za prawidłowe byty w kosmosie. Nie jest fanem 'ziemniaków w kosmosie' (planetarnych).
* Kazimierz Darbik
    * OCEAN: (E- N+) "Cisza przed burzą jest najgorsza." | VALS: (Power, Achievement) "Tylko zwycięstwo liczy się." | DRIVE: "Odzyskać to, co straciłem."
    * kapitan sił Orbitera pod dowództwem Antoniego Bladawira
* Michał Waritniez
    * OCEAN: (C+ N+) "Tylko dyscyplina i porządek utrzymują nas przy życiu." | VALS: (Conformity, Security) "Przetrwanie jest najważniejsze." | DRIVE: "Chronić moich ludzi przed wszystkim, nawet przed naszym dowódcą."
    * kapitan sił Orbitera pod dowództwem Antoniego Bladawira
* Zaara Mieralit
    * OCEAN: (E- O+) "Zniszczenie to sztuka." | VALS: (Power, Family) "Najlepsza, wir zniszczenia w rękach mojego komodora." | DRIVE: "Spalić świat stojący na drodze Bladawira"
    * corrupted ex-noctian, ex-Aurelion 'princess', oddana Bladawirowi fanatyczka. Jego prywatna infomantka. Zakochana w nim na ślepo.
    * afiliacja magiczna: Alucis, Astinian

Siły przemytowe na Karsztarinie

* Szymon Orzesznik
    * OCEAN: (A+ N-) "Każdy człowiek może osiągnąć wielkość" | VALS: (Universalism, Benevolence) "Jesteśmy światłem dla tych, którzy nas potrzebują." | DRIVE: "Tworzenie lepszego świata przez bezpośrednią pomoc."
    * kapitan sił Orbitera, niezależny i inteligentny agent
* Alira Tessalon
    * OCEAN: (O+E-) | "Odkryjmy sekrety na rzecz Aurum, nie Orbitera" | VALS: (Tradition, Achievement) | DRIVE: "Aurum ma być wielkie! Nie będzie Orbiter nas ograniczać!"
    * 28-letnia tienka, cicha badaczka powiązana z Alteris, widząca różne rzeczy anomalne
* Orion Tessalon
    * OCEAN: (C+A+) | "Współpraca i precyzja dadzą Aurum przewagę" | VALS: (Tradition, Security) | DRIVE: "Aurum nie potrzebuje Orbitera."
    * 29-letni tien, powiązany z Astinian, porządkujący i działający szybciej niż wydawałoby się to możliwe. "Biokomputer".

### Scena Zero - impl

.

### Sesja Właściwa - impl

Od pewnego już czasu Infernia jest pod dowództwem komodora Bladawira. Bladawir zdalnie spotkanie.

* Bladawir: Darbik, Waritniez, Verlen. Wasze zadanie jest proste. Od dzisiaj rozwiążemy problem z przemytem.
* Bladawir: Zadanie trudne, ale nie niemożliwe dla moich kapitanów.
* Bladawir: Będziecie dostawali sygnatury jednostek. Macie je zbadać. Macie upewnić się, że nie występuje tam energia magiczna o podanej sygnaturze.
* Bladawir: Zaara, do Isratazir. Samuel, do Actazir.
* Bladawir: Każda jednostka która cokolwiek przemyca ma być odpowiednio ukarana i ma to być zademonstrowane, ku chwale Orbitera.

Tak czy inaczej, Arianna dostała przetransmitowaną sygnaturą energii magicznej. Na pierwszy rzut oka Klaudia: energii Alteris i Astinian. Eustachy dostał dostęp do dopalaczy zewnętrznych - 'rakiety zewnętrzne'. Bladawir udostępnił Eustachemu DUŻO takich pakietów, które trzeba troszkę dopasować do Inferni. Bladawir daje Eustachemu do dyspozycji niekoniecznie idealne materiały. Dobre na fregatę, ale dla Inferni niekoniecznie.

Eustachy wysyła informacje do centrali Bladawira i do Arianny. Kilkanaście minut później dostaje Eustachy odpowiedź o żądanie specyfikacji... (dużo takich - sensowne pytania jeśli ktoś nie ufa Eustachemu że ten się znać na Inferni). Eustachy odpowiada PRAWIDŁOWO a potem 'idź się chędoż na drzewo wiem co robię a Ty jesteś niekompetentny i chcę gadać z Twoim przełożonym'. Eustachy w swój unikalny uroczy sposób zawiązuje przyjaźnie.

Kapitan Darbik, z Actazir chce rozmawiać z Eustachym po powyższym.

* Arianna: O co chodzi? Co Eustachy zrobił?
* Darbik: Kapitan Verlen, Twój inżynier zażądał rozmowy ze mną po tym, jak zaproponował ryzykowne elementy do zewnętrznych silników.
* Arianna: Jest w tym jakiś problem?
* Darbik: ZAŻĄDAŁ komunikacji ze mną.
* A->E: Eustachy, co napisałeś w requeście do silników?
* Eustachy: odpowiedział XD
* Arianna: prześlij to następnym razem przeze mnie. Możesz porozmawiać z przełożonym.
* Darbik: PORUCZNIK Eustachy Korkoran jak mniemam?
* Eustachy: Zgadza się
* Darbik: Chciał pan ze mną rozmawiać.
* Eustachy: Tak, dostałem listę rzeczy (i tłumaczy, krok po kroku). I natrafiłem na ścianę. Jak sprawnie i szybko no to... proszę o potwierdzenie, że moje sugestie będą rozpatrzone a nie prawa pracownicze i w ogóle.
* Darbik: Infernia nie jest jednostką standardową jak widzę.
* Eustachy: No nie jest
* Darbik: I mógł pan po prostu odpowiedzieć na te wszystkie pytania.
* Eustachy: Cała jednostka wie że to flota do zadań specjalnych!!!! (+wściekłość)
* Darbik: Panie Korkoran. Flota, może i wie. Ja niekoniecznie. Nie jest pan ani pana statek tak ważny. Ale, wypełnił pan dokument. Otrzyma pan sprzęt o który pan prosi. 
* Eustachy: I ZAJEBIŚCIE I TO MI SIĘ PODOBA.
* Darbik: Będziecie zajmowali się przemytem. To jest aktualne zadanie specjalnie godne jednostki takiej jak Infernia. I mój statek.
* Darbik: Panie Korkoran, wszystko da się załatwić, ale grzeczniej. Nie jest pan cudem natury i mówiąc brutalnie, nie ma pan dekoltu który pozwala na takie zachowanie. Czy się zrozumieliśmy?
* Eustachy: Tak, zrozumieliśmy się.
* Darbik: Dobrze. Sprzęt będzie dostarczony zgodnie z tym czego szukacie.
* A->E: Słuchaj, jest taki komodor, nazywa się Bladawir, robi dużo złego ludziom takim jak my. I my po to jesteśmy by nie robił.
* Eustachy: Mogę go odstrzelić?
* Arianna: To nie takie proste.
* Eustachy: Wyślij mnie na wymianę, zakradnę się, implodują reaktor.
* Arianna: WIesz kogo mają na pokładzie? MAGA MENTALNEGO.

Eustachy sprawdził co do niego dotarło - sprzęt jest dobrej jakości, niesabotowany i faktycznie taki jak Eustachy zamówił. Brakuje jakichś niewielkich elementów, co więcej - nie jest to tak dobrze zrobione jak Klaudia ale jest dobrze. Eustachy to dobrze zamontował.

Klaudia jest ciekawa czego on szuka. Klaudia jest katalistką, naukowcem i jest w stanie ogarnąć. W jakiej formie ta energia może być przemycana? Klaudia jest blisko K1; dane z K1 odnośnie energii w kontekście przemytu. Coś podobnego.

Tr Z +3:

* Vz: Klaudia ma pewne informacje
    * ten zrost energii o tej sygnaturze będzie maskować wykorzystywanie sensorów i systemów. STĄD magowie.
    * na K1 są sygnały o obecności tych energii wykorzystywanych do maskowania transportu na jednostkach Orbitera.
* V: Klaudia ma WIĘCEJ informacji
    * ten zrost energii kojarzony jest z przemytem tienów z Aurum na Program Kosmiczny Aurum.
    * pozytywni ludzie Orbitera pomagają Aurum uważając, że Aurum ma prawo być w kosmosie.
    * I BLADAWIR CHCE TO ZNISZCZYĆ.
* Vr: Klaudia dała Zaarze to wykryć - Zaara ma poczucie że umie Klaudię przetestować.

Infernia się przygotowuje do działania. Misja się zacznie.

...nie wiedzieliście po co są silniki. 2 dni później.

PRZYKŁADOWA OPERACJA. Infernia jest już ZMĘCZONA. 11h wszyscy ostro pracują i pilnują. I Bladawir wysyła kolejny sygnał. Inferni wyspecyfikował konkretny statek. Żeby Infernia była w stanie dotrzeć i zmienianiem silników, Infernia musi działać DOBRZE.

TrZ+3:

* V: jest sukces. Eustachy i jego mody Inferni zadziałały. Jednostka działa tak Eustachy się spodziewał.
* X: przez brak komunikacji jest gorzej niż gdyby poszedł za Darbikiem. Nie wiedział, że to 'wymiana koni'.
* Vz: Eustachy będzie w stanie to skorygować, zrobić raport i pokazać że TYM RAZEM wie jak to zrobić by było taniej i lepiej w tego typu misji

Infernia zbliża się do randomowej jednostki Orbitera. Raoul, advancer, robi spacer kosmiczny między dwoma ruszającymi się jednostkami.

Tr+2:

* X: Coś poszło nie tak - tamten statek troszkę skorygował lot i Raoul przestrzelił
    * STATEK: "WTF, co to ma znaczyć, Infernia? Nie zbliżajcie się do nas."
    * Arianna: "Testujemy nowego pilota, pomóżcie nam w ćwiczeniach"
* (+Vz) X: 
    * Statek POMOŻE wam w ćwiczeniach. Uda się. A Raoul znalazł normalny niegroźny przemyt.
    * (nie ma dziwnej energii)
    * (Arianna powinna powiedzieć o przemycie)

Arianna, wbrew sobie, zgłasza to Bladawirowi. Ale zamiast "20 kg" zgłasza "3 kg". Mniejszy wymiar kary. Klaudia zna odpowiednie regulacje.

Arianna chce współpracować z kapitanem tamtego statku. Ex Z(za reputację Arianny). "Przepraszam coś odpadło, nasz ryzykowny manewr, głupio, chcemy pomóc". Plausible deniability.

Tr +3:

* V: Bladawir do niczego się nie mógł przyczepić i pomogła kapitanowi i statkowi Orbitera
* X: Arianna zaczyna być kojarzona z resztą psów Bladawira. Bo ludzie nie wiedzą, jak pomogła kapitanowi itp.

TE OPERACJE TAK WYGLĄDAJĄ. Infernia jest coraz bardziej zmęczona, nie tylko Infernia - inne jednostki też są dociśnięte. Ale Klaudia zauważa coś ciekawego - jest wzór jak chodzi o selekcję jednostek przez Bladawira. On to ukrywa. Ale wyraźnie Bladawir interesuje się jednostkami wlatującymi na Karsztarin i wylatującymi z Karsztarina.

Komodor Bladawir chce zrobić TERROR wśród przemytu z i na Karsztarin.

Tr Z +2:

* X: Bladawir jedynie dodał do siebie reputację 'kij w dupie', jeszcze bardziej go nie lubią. Powoli zbliżamy się do 'wysyłamy kolesia na wschodni front'.
* Vz: Bladawir dzięki swojej załodze zrobił terror i skutecznie posadził sporo karier. (TAK Infernia przemyca. TAK Bladawir nie wie).

Czas płynie, operacja trwa, Bladawir wymienił jedną korwetę na inną. Infernia też KIEDYŚ będzie zwolniona.

Cel Zespołu - znaleźć magów Aurum na Karsztarinie i ich ostrzec. Że muszą przeczekać. Jeśli wymyślimy jak zrobić by Bladawir osiągnął cel ale nie osiągnął, musimy zmienić cel.

Arianna -> Marcinozaur, czy szpiegatorowi coś wiadomo? Klaudia szyfruje a Arianna mówi kodem Verlenów. KONFLIKT dotyczy kodowania. Bladawir używa Inferni jako przynęty.

Tr Z (wydarzenia z Verlenlandu + intelekt Klaudi) +3:

* Xz: Arianna dla Bladawira dowiaduje się tego czego Bladawir nie mógł się dowiedzieć. Przesunął Ariannę na granicę (niszczy reputację itp). Arianna kontaktuje się z planetą i Bladawir CZEKA.
    * on WIE że Arianna to robi, ale nie wie CO się dowiaduje
* X: Bladawir dowie się, że FAKTYCZNIE są działania Aurum i są siły współpracujące. I że Verleni coś wiedzą - ale sami nie uczestniczą. (+Arianna KSIĘŻNICZKA VERLENLANDU)
    * M: Aria
    * Arianna: Marcinozaur, dobrze Cię słyszeć (okres, wsparcia rodziny)
    * M: Rozumiem, świnie. Zawsze jest problem ze świniami.
    * Arianna: Kuzyn, warchlak, wielki knur do wypasania, wielkiego i obleśnego z załogi, nie wiem jak to wytrzymuje...
    * M: Rozumiem, rozumiem, śmierdzi i nie wiesz jak dotknąć. Najlepiej wcale. Ale tak to jest, jak jest wieprz i trzeba wypasać...
    * Arianna: Głupio bo kuzynowi zależało byśmy się zajęli wieprzem, sprawa cuchnąca... ale jak kuzyn bardzo prosi to co zrobisz...
    * M: Dlatego odleciał wtedy do Głównego Holdu. Zawsze prędzej czy później pojawia się możliwość wrócenia do cywilizacji, albo opuszczenia Verlenlandu na prowincję :-).
    * ... (i wszystko po świniach i prowincjach)
    * -> Marcinozaur może dać informacje:
        * Są osoby które rekrutują tienów i dają im szansę poza Orbiterem i poza planetą
        * Są tieni którzy za tym idą, zwłaszcza z poparciem rodzin
        * Czasem jest kontakt, przez kurierów i agentów. To się dzieje. 
        * Marcinozaur wie, gdzie na planecie można znaleźć kogoś od werbunku.
        * Marcinozaur tego nie wie, ale to co mówi potwierdza hipotezę Karsztarina oraz tego że Orbiter pomaga.

Arianna potrzebuje innego podejścia. Niech to będzie podejście przez Elenę... znowu przez krew.

TrZM+3+3Ob:

* X: Elena nie ceni magów Aurum. Zupełnie.
    * Arianna: Eleno, znowu potrzebuję pomocy, mogą ucierpieć magowie z Aurum, musisz mi pomóc ich namierzyć. Bo jak ten śmierdzący knur dotrze do nich pierwszy, oni nie mają szansy na legendę
    * Elena: ZNOWU Ty (rezygnacja). Sama sobie nie radzisz. I zajmujesz się... jakimiś... niepotrzebnymi magami. Oni nic nie osiągną. Nie są Verlenami. Nie są kompetentni.
    * Arianna: Nie dasz im szansy? Nie każdy ma tak łatwo jak my że może iść do szkoły... pamiętasz Mai? Nie miała wcześniej szansy.
    * Elena: Ok. Ale. Maja próbowała. Oni? Tutaj? Nie. Oni nic nie próbują. (po chwili) PRAWIE wszyscy.
    * Arianna: Mówimy o tych magach którzy nie zdołali się dostać na Castigator, na około, lepsze / gorsze jednostki które funkcjonują. Będą w stanie sprawdzić wartości jak dostaną wyzwania. 
* Xz: Zdaniem Eleny reguły mają znaczenie WIĘKSZE niż dobre serce itp.
    * Elena: Arianno, oni działają wbrew systemowi. Tak nie wolno. Mogli się dostać, popatrz, byle ściek trafił na Castigator. Ale oni chcą być dumni z Aurum. Nie chcą być częścią Orbitera.
    * Arianna: Gdybyś miała opcję: Castigator lub coś innego, co wolisz?
    * Elena: (myśli) mnie nie przesunęli na Królową ani Castigator (myśli)... byłam zbyt niebezpieczna. Wtedy. Dla wszystkich.
    * Arianna: Jak nie dla nich, zrób to by ludzie tacy jak Bladawir nie dostali kredytu z osiągnięć. Jak uzyska więcej wpływów w Orbiterze, do czego wykorzysta? (+1Vg)
* Vm: Magiczny link z Eleną przekazał jej uczucia Arianny.
    * Elena nie rozumie czemu Ariannie zależy, ale czuje jak bardzo jej zależy. I jak bardzo Arianna chciałaby by było jak kiedyś.
    * Elena zrobi to nie dla Bladawira czy dla Aurum, ale dla Arianny.
    * Ale powie "whatever, ok."
        * Elena nie wie że Arianna wie że Elena wie i co czuje

Elena robi infiltrację. Dowiaduje się tego czego ma się dowiedzieć. Po swojemu. W swoim uroczym stylu.

Tr+3:

* X: Elena podpada na lewo i prawo, co jej nie przeszkadza. To Elena z misją.
* V: Elena dowiaduje się Ariannie tego co ona potrzebuje.
* X: Elena by zamaskować ślady musiała coś zepsuć, ale wyszło że to sabotaż "z przekory". Elena dostaje opinię niesterowalnej i złośliwej. A ona ukrywa sekret.
* Vr: Elena dyskretnie dowiedziała się szczegółów, mimo Kurzmina (który nic nie wie) i krwią przekazała sygnał Ariannie.

Arianna DOWIADUJE się tego co ważne:

* Kurzmin wie o przerzucie tienów. Nie wie DUŻO, ale wie KTO. Szymon Orzesznik (kapitan statku), on tym dowodzi z ramienia Karsztarina.
* Kurzmin nie zna wielu więcej detali. Tienowie nie wiedzą.
* Na Karsztarinie COŚ jest. To potwór. Chyba. Coś poluje na tienów i na siły Orzesznika. Dlatego jest presja. Orzesznik podejrzewa Bladawira.

Jeśli Bladawir wprowadził potwora, JAKIEŚ ślady musiały się pojawić. W danych Karsztarina powinny być. W logach. W parametrach itp. Klaudia je znajdzie. Zrobi co może. Klaudia ma DUŻĄ przewagę mimo że Zaara mogła się przygotować, bo Zaara też nie miała swobody i nie jest ekspertką od technomancji. Klaudia wspierana Infernią łączy się z TAI Karsztarina.

Tr+3:

* X: Zaara dowiaduje się, że KTOŚ wie o potworze. Nie wie KTO.
* X: Potwór zmusił Orzesznika do zrobienia ewakuacji tienów. Bladawir jest blisko sukcesu.
* X: Nie da się udowodnić, że to jest Zaara. (+1Vg)
* V: Klaudia ma nagrania pokazujące że faktycznie pojawił się potwór i są silne przesłanki, że to Bladawir
    * Potwór został podstawiony w okolicach waśni z Ewą Razalis.
        * Już WCZEŚNIEJ Bladawir podejrzewał, że tam jest przemyt ludzi
        * Czy Bladawir uderzył w Razalis między innymi dlatego?
* V: Dane dostaje Ewa Razalis. "Jakiś lojalista Ewy musiał to wygrzebać by pogrążyć Bladawira".
* X: "Kontrola celna". Klaudia zasugerowała w mailu do kmdr Razalis by 'odpłacić pięknym za nadobne'
    * Inne jednostki robią kontrolę celną jednostek Bladawira. W tym Inferni.
    * Nie udało się wszystkiego pochować itp, NAPRAWDĘ kompetentna kontrola
        * Arianna na dywanik (nie u Bladawira), te sprawy...
            * Bladawir ma opierdol za Ariannę i całokształt
        * Orzesznik jest w stanie się ukryć. Wycofał tienów - ale nie tam gdzie chciał, po prostu musiał opuścić Karsztarin w okolice Stoczni Neotik.

Bladawir dostaje SOLIDNY opierdol. I za hipokryzję (Arianna) i za operację i za potwora itp. Nie osiągnął tego co chciał. On nigdy nie wiedział o Orzeszniku. Bladawir żąda Ariannę na dywanik.

* Bladawir: Kapitan Verlen. Przez Ciebie (zbiera myśli, jest tak zły)... nie udało się osiągnąć sukcesu operacji. Nie upilnowałaś swojej załogi.
* Arianna: Co właściwie byłoby uznane za sukces operacji? Wykrycie przemytu? Anomalnego potwora na statku? Kto mocniej złamał przepisy Orbitera, komodorze? Rozumiem, że nie upilnowałam ludzi. W pańskim wypadku nie było to nieupilnowanie ludzi. Za przeproszeniem, to należało do dobrych Orbiterowców. Nie chcę słuchać reprymendy kogoś, kto zachował się w ten sposób.
* Bladawir: (zaciśnięte zęby) Ty jesteś od wykonywania poleceń. Ja jestem od myślenia.
* Arianna: Nie mam nic przeciwko wykonywaniu poleceń. Te polecenia ranią członków Orbitera, nie są dla dobra Orbitera.
* Bladawir: Te rzeczy są dobre dla ludzi. Orbiter jest środkiem.
* Arianna: Dla Krzysztofa, który stracił nogę? Przepraszam komodorze, ale na niektóre rzeczy nie patrzymy w ten sam sposób.
* Bladawir: Jeśli tak, rozważ, czy nie lepiej byłoby Ci poza moim dowództwem. Dam Ci możliwość odejścia. Masz potencjał. Ale niekoniecznie nadajesz się na to, co ja robię.
* Arianna: Rozważę pańską propozycję.
* Bladawir: (autentycznie zdziwiony) (Arianna jest zdziwiona że on jest zdziwiony) (na jego twarzy lekko złośliwy uśmiech)
* Bladawir: Orbiter jest jedynym gwarantem bezpieczeństwa w kosmosie. Nie masz pojęcia jakie potwory się kryją tutaj. Jesteś Verlenką. Powinnaś rozumieć hartowanie przez potwory. Zanim pojawi się Lewiatan.
* Arianna: Rozumiem. Nie widzę związku z tą sytuacją.
* Bladawir: Wszystko sprowadza się do jednego pytania - czy umiesz wykonywać polecenia. Czy CHCESZ wykonywać polecenia. Straciliśmy kilku tienów. Nie wiem jak ich uratować.
* Arianna: Co ma pan na myśli, komodorze?
* Bladawir: Szkoda mi, gdy nawet śmieć cierpi.
* Arianna: (zdziwiona twarz)
* Bladawir: Ewa Razalis była zbyt łagodna. Zagrażała Orbiterowi. Musiała się wzmocnić. Ty byłaś śmieciem, ale się okazało, że nie jesteś. (chwila ciszy)
* Bladawir: Nie potrzebuję kogoś, kto... kwestionuje rozkazy. Ja wiem co robię. Ty nie wiesz, co robię. Nie wiesz, z czym walczę.
* Arianna: Dlatego tu Infernia miała problemy z silnikami, nasza załoga jest nietypowa i proaktywna.
* Bladawir: Dobra załoga jest proaktywna. Powinienem był powiedzieć Wam o krótkoterminowości silników. Nie powinienem był powiedzieć Wam nic innego. Cel misji pozostaje tajny.
* Arianna: Sądzi pan że proaktywne działanie pojedynczych jednostek mogło doprowadzić do sukcesu? Nawet bez pana moglibyśmy interweniować. Jeśli chodziło o uratowanie kogoś...
* Bladawir: Inaczej definiujemy 'uratowanie'. Upoważniłem Was do zestrzelenia jednostki, która ma sygnaturę i ucieka i nie możecie jej dogonić. Bycie martwym jest preferowaną opcją do bycia śmieciem w kosmosie.
* Bladawir: Nie zrobiłaś niczego źle. Poza... słabością. Nie kazałem Ci usunąć kontrabandy, więc nie dziwię się że ją miałaś. Ale nie stać Was na słabość. Nie, jeśli pracujesz pode mną.
* Bladawir: Więc rozważ. Możesz odejść. Ale jak zostaniesz - oczekuję kompetencji i siły.
* Arianna: Rozumiem, panie komodorze.

Arianna widzi wyraźnie - komodor Bladawir wygląda, jakby się postarzał 10 lat. Jego plan się rozsypał.

(CZEMU Bladawir był zaskoczony)

Tr +3:

* X:
    * Bladawir się spodziewał:
        * Bladawir WIE, że Arianna jest pyskata, wie, że jest proaktywna itp. Pamięta dane z Królowej, rekomendację Bolzy itp.
        * Bladawie WIE, że Arianna jest 'bohaterką' i ma kompleks bohaterki
    * Bladawir dostał INNĄ Ariannę.
        * Ta Arianna jest 'grzeczna'
        * Ta Arianna zachowuje się prawidłowo
    * WNIOSEK: Arianna jest czyimś agentem. Ona nie musi CHCIEĆ tu być, ale jest mu wsadzona.
        * ALE nie sabotuje lub sabotuje za dobrze
    * WNIOSEK: NIE MOŻE zaufać Ariannie. Ona nie może się dowiedzieć.


## Streszczenie

Komodor Bladawir postanowił usunąć przemyt tienów z Karsztarina (najpewniej Program Kosmiczny Aurum), każąc swoim jednostkom robić wyczerpujące patrole i przeszukiwanie jednostek pod kątem owego przemytu. Tempo jest zabójcze nawet dla Arianny i Inferni - zwłaszcza, że Arianna nie wie po co to wszystko robi. Śledztwo Klaudii pokazuje, że Bladawir najpewniej umieścił potwora na Karsztarinie by zmusić przemytników do rozpaczliwego ruchu. Arianna nie może nikogo ostrzec, ale używa połączenia Krwi z Eleną - i w ten sposób dowiaduje się o roli Orzesznika. Klaudia dyskretnie przekazuje informacje Ewie Razalis. Operacja Bladawira się nie tylko nie udaje, ale trafił na dywanik i jego plan się rozsypał. Bladawir powiedział Ariannie, że ona ALBO będzie z nim współpracować albo on jej nie chce. Nadal nie wie, że to Arianna jest architektem jego porażki.

## Progresja

* Arianna Verlen: Dostała nieproporcjonalnie dużo opiernicz za przemyt rzeczy na Inferni, by ukarać Bladawira. Ale też ma opinię 'lizuski Bladawira' za podłe akcje celne.
* Arianna Verlen: Bladawir jej nie ufa, uważa ją za agentkę Innej Strony, ale nie rozumie jej zachowania. Jest skłonny pozwolić jej odejść bez konsekwencji.
* Antoni Bladawir: Przegrał operację 'usunięcia zdrajcy z Orbitera', nad którą pracował od dawna. Największa porażka od bardzo dawna. Do tego ogromny OPR z Admiralicji za działania, sabotaż i niedopilnowanie Inferni. Ogólnie, poważny cios.
* Antoni Bladawir: Dowiedział się, że Verlenowie wiedzą o 'przesuwaniu tienów z Aurum na Orbitę' ale w tym nie uczestniczą. I o 'Ariannie, księżniczce Verlenlandu' i części jej powiązań.
* Elena Verlen: nie ma żadnego szacunku do magów Aurum. I jej zdaniem reguły mają znaczenie WIĘKSZE niż dobre serce itp.
* Elena Verlen: podpada na Castigatorze na lewo i prawo, bo wykonuje misję dla Arianny. Sabotaż dla maskowania śladów wyszedł że przekora. Dostaje opinię niesterowalnej i złośliwej.

## Zasługi

* Arianna Verlen: Gdy Bladawir kazał szybko badać statki pod kątem celnym, podejrzewała coś dziwnego. Zgłosiła mniejszy wymiar przemytu by osłonić część osób. Skutecznie zarządzała zmęczeniem załogi i używając Krwi poprosiła Elenę o informacje na temat Karsztarina. Mimo że prawie została zneutralizowana, udało jej się wyprowadzić przemytników tienów w kosmos poza szpony Bladawira. On nadal nic nie wie.
* Klaudia Stryk: Nie zaakceptowała czystej sygnatury energii od Bladawira; doszła do jej znaczenia i do przemytu tienów z Aurum. Korzystając z danych K1 określiła wzory przemytu na Karsztarinie. Odkryła plan Bladawira zawierający potwora na Karsztarinie i dyskretnie przekazała wiadomość do Ewy Razalis używając przelotu przy Karsztarinie. Architektka porażki Bladawira. Pewnych rzeczy przy Klaudii się nie robi.
* Eustachy Korkoran: z uwagi na unikalność Inferni powiedział kpt. Darbikowi że ten się nie zna. O dziwo, Darbik odpowiedział i dał Eustachemu to czego potrzebował. Okazało się, że Darbik miał rację bo Eustachy nie znał celu misji. Nic, zacisnął zęby i zrobił raport TANIEJ i LEPIEJ.
* Szymon Orzesznik: inteligentny i niezależny, przemyca tienów z Aurum w kosmos. Kapitan Orbitera. Bladawir zastawił nań pułapkę i go przyskrzynił - ale Arianna go uwolniła i dała mu uciec, o czym nawet nie wiedział. Przyjaciel Leszka Kurzmina.
* Antoni Bladawir: tyran planujący zniszczenie 'zdrajcy Orbitera' przemycającego tienów z Aurum przez Karsztarin gdzieś poza Orbiter. Zaara na jego polecenie opracowała potwora wprowadzonego na Karsztarin (za czasów starcia z Ewą Razalis). Wszystko opracował - wygnanie zdrajcy z Karsztarina i przechwycenia go jego jednostkami które nawet nie wiedzą w jakiej operacji uczestniczą. Ale Arianna i Klaudia pokrzyżowały mu plany, jego działania się rozpadły. PRAWIE powiedział Ariannie o co chodzi, ale nie powie tego przecież 'wrogiej agentce'...
* Elena Verlen: czarny koń Arianny; komunikacja po Krwi sprawiła, że Elena za plecami Bladawira mogła pomóc Ariannie dowiedzieć się co wie Kurzmin (swoją omnidetekcją). Zapłaciła reputacją, ale dowiedziała się tego co było potrzebne.
* Zaara Mieralit: neutralizowała Klaudię i jej infiltrację (nieskutecznie; Klaudia wdarła się do systemów Karsztarina). Ostrzegła Bladawira o sytuacji zanim go wezwano na dywanik. Z woli Bladawira zrobiła potwora na Karsztarinie (z TAI), który miał wypłoszyć Orzesznika. Udało jej się, ale inne elementy planu się rozsypały.
* Marcinozaur Verlen: kontakt i przyjaciel Arianny, 'szpiegator', który dyskretnie przekazał Ariannie to co ona potrzebowała wiedzieć o transferze tienów poza Aurum.
* Kazimierz Darbik: p.d. Bladawira i kapitan OO Actazir, zimny i uczciwy, acz bardzo 'gadzi'; gdy Eustachy się rzucał, upomniał go za ton ale udostępnił sprzęt jakiego Eustachy zażądał. Acz Eustachy ma u niego krechę.
* Raoul Lavanis: jako advancer robił operację poszukiwania przemytu na kadłubie innej jednostki. Mimo że szukał 'specjalnego', to znalazł 'zwykły' przez co Arianna ma kłopot. Plus, nawet on nie miał jak tego robić.
* Ewa Razalis: dzięki danym dyskretnie przekazanym przez Klaudię udało jej się uderzyć w Bladawira i trochę się zemścić (a jemu zniszczyć operację).
* OO Infernia: pod kontrolą Bladawira, z zewnętrznymi silnikami dzięki Eustachemu, prowadzi operacje celne i szukające przemytu tienów (i innych rzeczy). Sama ma przemyt na pokładzie przez co Arianna ma OPR.
* OO Karsztarin: na pokładzie tej niezbyt monitorowanej jednostki przemycani są tieni z Aurum, jak i Zaara umieściła potwora mającego wypłoszyć przemytników i przemycanych. Obie sprawy zniknęły (Arianna i Klaudia).

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Pierścień Zewnętrzny

## Czas

* Opóźnienie: 6
* Dni: 5
