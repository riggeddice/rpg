## Metadane

* title: "Maja chciała być dorosła"
* threads: pronoktianska-mafia-kajrata, samotna-maja-w-strasznym-aurum
* motives: complete-monster, break-the-cutie, aurum-wasn-wewnatrzrodowa, casual-cruelty
* gm: żółw
* players: kamilinux, kić

## Kontynuacja
### Kampanijna

* [230627 - Ratuj młodzież dla Kajrata](230627-ratuj-mlodziez-dla-kajrata)

### Chronologiczna

* [230627 - Ratuj młodzież dla Kajrata](230627-ratuj-mlodziez-dla-kajrata)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ?
* Struktura: 
    * 230613-typ-sesji-rajd-na-fortece
* CEL: 
    * MG
    * Drużynowy
        * Zlokalizować miejsce, w którym handluje się ludźmi. Złamanie tego miejsca.
    * Indywidualny
        * Amanda: "Przejąć interes dla Kajrata."
        * Karolinus: "Zrzucić winę na Fabiana, niech Lemurczakowie."

### Co się stało i co wiemy

* Kontekst
    * Złoty Cień to elegancka grupka przestępcza w rękawiczkach załatwiająca to co jest potrzebne wtedy gdy to potrzebne
        * Są wspierani przez kilku Samszarów
    * Klinika Oteriiel zajmuje się adaptacją i dostarczaniem odpowiednich osób do odpowiednich celów
* Wydarzenia
* Struktura
    
### Co się stanie (what will happen)

* S1: 
* SN: 
    * Jonatan Lemurczak chce dziewczynę wyglądającą jak Vanessa.
    * Vanessa chce krwawych walk gladiatorów.
    * Jonatan i Vanessa dostaną 10 dziewczyn, jako nagrody na igrzyska i prezent.
    * Wirgot chce ekspertyzy Jonatana wrt. kogoś (Mai?)
    * Albert nie chce mieć NIC wspólnego z Lemurczakami i Wirgotem

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* .

### Scena Zero - impl

.

### Sesja Właściwa - impl

Parę dni później. Elena była "tam na dole", więc ją przesłuchują. Ale Karolinus miał amnestyki PLUS Karolinus był z Wacławem PLUS Itria odwaliła niezłe przedstawienie i wszyscy myśleli, że Karolinus poszedł za dziewczynkami. -> Karolinus nic nie wie.

Kontakt Ernesta Kajrata.

* E: Tien Samszar, jestem zdziwiony, że dotrzymałeś słowa. Magowie są w moich rękach.
* K: Dotrzymaliśmy, nie było łatwo, co z Talią?
* E: Ja też dotrzymam słowa. Załóż, że Twoja Strzała jest w dobrych rękach. Talii.
* E: Nie będę ukrywał, polubiłem Cię, młody magu. /uśmiech rekina.
* E: Mam coś, co może Tobie pomóc. Za darmo dla mnie. Nic od Ciebie nie chcę a Tobie się może przydać. Zainteresowany?
* E: Masz problem z jakimiś tajnymi bazami, z tego co rozumiem. I eksperymentami. Prawda? Twój kolega zniknął...
* K: Raczej kolegi szukam, z bazami nie mam problemu.
* E: Nie będziesz w stanie tego zrobić jak się Tobą zajmą.
* E: Na szczęście ten problem da się rozwiązać cudzymi rękami. Jest miasto Karmazynowy Świt. W tej chwili goszczą tam dwie... szemrane osoby. Tien Lemurczak i tien Lemurczak. Są tam robione rzeczy z ludźmi. Wy - Samszarowie - robicie coś z ludźmi. Nie Ty osobiście, co to to nie. Ale Lemurczakowie są tam bo tego chcą.
* E: Tien Samszar, jaka byłaby szkoda, gdyby Lemurczakowie uznali, że TAMCI Samszarowie chcą ich... wyruchać. Zbadaj sytuację i spróbuj doprowadzić do tego że cokolwiek pójdzie nie tak, jest winą osób z tamtej bazy.
* E: Amando, mówimy o miejscu gdzie działa półświatek. W białych rękawiczkach. Byłoby nieeleganckie, gdyby - jak już jesteśmy gośćmi na tym terenie - taki półświatek został nieruszony. Tien Samszar doceniłby, gdyby poziom przestępczości się zmniejszył. Prawda?
* K: Tak | A: Oczywiście
* E: Gdzieś tam się coś robi z ludźmi. Podejrzewam handel ludźmi. Zostawiam Was z tą informacją.

Koniec kontaktu.

Karmazynowy Świt. Karolinus i "służka" zameldowali się w hotelu "Gwiezdna Rozkosz". Dobra klasa, reputacja _love hotel_, pasuje do reputacji Karolinusa i na pewno nie ma tam Mai i Alberta.

Karolinus wysyła req do sentisieci i dostaje info:

* Maja Samszar. Jest na wakacjach. Faktycznie, jest na wakacjach. 
* Albert Samszar. Wakacje, ale dokładniej pilnuje córki.
* Wirgot Samszar. On zarejestrował w sentisieci Lemurczaków.
* Jonatan Lemurczak.
* Vanessa Lemurczak. Jej zdjęcie pokazuje uroda klasy Diakonki.

Karolinus próbuje sentisiecią zbadać co się dzieje.

TrM+2+3Ob:

* V: Część rozmów i sygnałów
    * Wirgot COŚ CHCE od Jonatana. Ale nie mówi co bo "sentisieć".
    * Wirgot da w darze Jonatanowi i Vanessie 10 osób. "Dobrze dostosowanych"
    * Jonatan, patrząc PROSTO na Vanessę zażądał, by jedna dziewczyna wyglądała jak Vanessa. Ta się uśmiechnęła do Jonatana. To nie był miły uśmiech.
    * Vanessa porusza się niepokojąco szybko
    * Vanessa zażyczyła sobie najsilniejszego gladiatora. Jonatan powiedział "musi przeżyć". Vanessa wzruszyła niezadowolona ramionami. Wirgot "oczywiście, tien Lemurczak".

Cel Karolinusa - niech na TYM terenie nie dochodzi do handlu ludźmi. I niech Lemurczakowie nie dostaną tych 10 ludzi. Ani żadnych kolejnych. Kiepsko się robi tu interesy.

Zarówno Wirgot jak i Lemurczakowie są w Pałacu Harmonii. Najlepszym, najdroższym itp. Tam jest arena gladiatorów i są plotki, że tam się da dostać "wszystko". Albert i Maja też są w Pałacu Harmonii. ALE odkąd pojawili się Lemurczakowie, Albert zmienił pokój swój i Mai. "Jak najdalej od plebsu".

* V: Karolinus dowiedział się o czymś ciekawym. Szukał kto da Wirgotowi niewolników. Dotarł do oferty w sentisieci, dyskretnej, "jesteś w luksusowym kurorcie i jesteś tien Samszar. Jakie masz życzenie, będzie zaspokojone".
    * Istnieje grupa "przestępcza w białych rękawiczkach" działająca dla Samszarów.
    * Wirgot korzysta z grupy "Złoty Cień". Oni załatwiają wygodne rzeczy. Działają na tym terenie.

Karolinus -> Złoty Cień. 

* K (na sentisieci): "Jaki koszt dwóch dorosłych bliźniaczek?"
* ZC -> K (telefon): "Tien Samszar, nazywam się Nadia. To zaszczyt. Czy pierwszy raz masz przyjemność korzystać z naszych usług?"
* K: "Tak"
* ZC: "Zatem - witamy w Złotym Cieniu, tien. Jakieś preferencje co do bliźniaczek? Czy powrócą w stanie... nienaruszonym? To znaczy - bez uszkodzeń fizycznych?"
* K: "Tak"
* ZC: "Doskonale! Jakieś preferencje co do ciała, umysłu... innych rzeczy? Czasu?"
* K: "Nie"
* ZC: "Tien Samszar, są do dyspozycji praktycznie natychmiast, dwie wesołe i ochocze damy. Pierwsza dawka gratis."
* K: (adres w hotelu Gwiezdna Rozkosz)
* K: "A możesz mojemu koledze przekazać dobrą nowinę, że jedna z niewolnic czeka w holu?"
* ZC: "Mogę powiedzieć wszystko. Acz mam w wytycznych przekazać, KTO mi to powiedział, tien Karolinusie Samszar."
* ZC: "Problem polega na tym, że były robione dowcipy. I szefostwo je ukróciło. Więc... nie możemy. Rozmiesz, jako poważny tien."

Plan:

* Cel - niech Maja skontaktuje się z Karolinusem i poprosi, by on zmienił ją w Vanessę.
* By to się stało, Maja musi chcieć wyglądać jak Vanessa. Choć tymczasowo.
* By to się stało, trzeba się dowiedzieć, CZEGO Maja może chcieć.
* -> Karolinus chce się dowiedzieć o niej więcej i zrobić tak by Maja chciała wyglądać jak Vanessa.
* -> Amanda zadzwoni i przekaże wiadomość jako Złoty Cień.

Karolinus kontaktuje się z Mają.

* M: Tien Karolinusie, to przyjemność /uśmiech
* K: Byłem tu przy okazji..
* M: Wakacje?
* K: Powiedzmy, malutkie
* M: Czy będziesz chciał się spotkać byśmy wypili herbatkę? Ja podam :-)
* K: Powiedzmy...
* M: (kilkanaście eleganckich tematów...)
* K: Jak relacje z ojcem?
* M: Bardzo dobrze, dziękuję, szanowny ojciec dobrze o mnie dba. Nie mogłoby być lepiej.
* K: Jak Ci się żyje w hotelu, czegoś Ci brakuje..?
* M: Nie mogłabym być szczęśliwsza i niczego mi nigdy nie brakuje. Mój szanowny ojciec dobrze dba o moje dobre interesy i życie.
* K: Między nami - znajoma która chciała zobaczyć to miejsce, marzenie (o Amandzie jaka to wspaniała dziewczyna). Tylko nikomu o tym nie mów, bo jest spoza rodu.
* M: To jest... (rozejrzała się) ZAKAZANY ROMANS? O_O
* K: (poważne przytaknięcie, palec do ust)
* M: Ojej... (błyszczące oczy)

Tr Z (+Maja Ci ufa -Maja nie chce podpaść +Maja ma taki charakter) +3:

* V: Maja się odblokowała trochę
    * "Czytałam książki, których nie powinnam była czytać." - Maja ma świecące oczy.
    * "Nie, na mnie nigdy tak nie patrzył. Nie wiem jak to jest. Skończę w jakimś aranżowanym małżeństwie z jakimś 80-letnim Sowińskim" - Maja ponuro.
    * K: "Spokojnie, wydoroślejesz, będziesz dojrzalsza, będą patrzeć..."
    * M: "Nie będę mieć okazji... jak coś przeskrobię to tata mnie zamknie w wieży czy coś..."
    * K: "Jak wydoroślejesz i będziesz mieć dorosłą sylwetkę to się przekonasz że mam rację - to dobrze, że nikt nie patrzy tak na dzieci."
    * K: (+3Oy) (dodaje sugestię o Romeo Verlenie, że on przecież też na nią spojrzy inaczej) (jak tylko Maja się przyznała, że przez Elenę Romeo zerwał z nią wszystkie kontakty)
    * K: (daj spokój, Elena wszystkim problemy robi, żebyś wiedziała jakie ja miałem problemy): (+2Or)
* X: 
    * Maja mówi szczerze, że jeśli będzie rozrabiać to tata zapowiedział, że zamknie ją w wieży czy coś XD. Nie chce.
    * K: "Nie raz Cię wyciągałem i z wieży też Cię wyciągnę"
    * M: `*_*`
* Oy: (podbicie) Maja jest bardziej zainteresowana Romeem niż na to wyglądało. Żali się Karolinusowi, że on zerwał kontakt, że ona jest... sama, że tylko on jej pomagał, i że ona ma "wujka Karolinusa" i "no, nie przyjaciela... (czerwona)" Romeo.
* X: (podbicie) Maja będzie miała złamane serce po tej operacji - bo ona ucierpiała. Nie spodziewała się tego. Niby jej wina, ale Karolinus jej nie ostrzegł. A ona nie wiedziała.
* Or: Maja jest WŚCIEKŁA na Elenę. Elena zabrała jej chłopaka (Romeo). Źle to zinterpretowała - Elena odepchnęła Romeo od Mai, bo Elena była Romeem zainteresowana. (Karolinus nie potwierdza, nie zaprzecza, ale kiwa głową rozumiejąco. I dając znać Mai, że niekoniecznie się myli)

Maja prosi Karolinusa, by on dał jej "efektowny, dorosły wizerunek". Karolinus ściągnął przeciętne dziewczyny i Vanessa, która się wyróżnia.

* Vr: Karolinus uprzedzał Maję, że przecież Vanessa jest prawdziwą czarodziejką, i to jest jego rolodeck dziewczyn które go tu zainteresowały, ale Maja PLIIIIS choć na chwilkę... ona mu pomoże w utrzymaniu magii i sentisieci...

Karolinus "niechętnie daje się Mai przekonać". Niech Maja docelowo wygląda jak Vanessa. By zobaczyła "jak to jest".

Tr Z (Maja pomaga) M +3 +3Ob:

* Vm: magia Karolinusa jest zamaskowana przez magię Mai. Maja wygląda i ma głos Vanessy.
* Vr: Maja jest dobrze ukryta w sentisieci. Maja może przez pewien czas żyć życiem Vanessy. Być wolna.
* X: Maja zostawiła za sobą echo wskazujące, że TU JEST MAJA. Ono się nie rozproszy.
* V: Zostaje ślad wskazujący, że KTOŚ chciał skrzywdzić Maję i zostawił echo. Nikt nie uwierzy że to był Karolinus - nawet Maja.
* Vz: Maja miała trochę zabawy, popatrzyła jak to jest, zobaczyła jak może być w przyszłości i jak można być traktowanym jak nie jest się Mają Samszar pod kloszem Alberta Samszara.

Mamy Maję tam gdzie chcemy i tak jak chcemy. Czas na to, by odpowiednio załatwić temat "Maja będzie TAM i zostanie wciągnięta przez Lemurczaków".

Amanda przyhacza Maję na imprezie. Amanda jest przebrana "po cywilnemu". Porządnie umalowana, włosy przekolorowana, maska kota, łatwo zaprzyjaźnia się z Mają prawiąc komplementy, stawiając drinka i podając odpowiednią tabletkę.

Tp Z +3 (+Amanda to pro, +Maja to noob, +Maja czuje się bezpieczna, +Maja nie wie jak żyć):

* Xz: nie do końca mamy korelacje czasowo-skanujące (Maja będzie u Lemurczaków dłużej niż wszyscy myśleliśmy)
* Vz: Maja zaprzyjaźniła się z Amandą (CAT LADY!)
* X: (podbicie) Oryginalna Vanessa była bardzo niezadowolona i dała to odczuć Mai.
* X: (manifest) Maja mimo że to były tylko 2h będzie potrzebować 2 tygodni w szpitalu. Vanessa + Jonatan.
* V: Maja będzie dobrze wystawiona Wirgotowi. Ma podaną tabletkę (którą Jonatan elegancko rozproszy) i operacja się uda.

Amanda załatwiła, by pojawił się komunikat od pachoła mafii do Wirgota, że niewolnica wyglądająca jak Vanessa jest gotowa z ramienia Złotego Cienia. Wirgot wysłał dwójkę ludzi i Cat Lady elegancko wycofała się w cień.

Karolinus - zauważyłeś, że po godzinie NADAL sygnatura Mai jest. Coś poszło nie tak. Nikt nie wie, że Maja jest w rękach Lemurczaków. Karolinus idzie do pokoju Alberta.

* Albert: "Tien Karolinusie Samszar. Czemu przychodzisz przeszkadzać mojej córce? I mnie?"
* K: "Byłem w pobliżu to pomyślałem przyjdę zagadać. Byłem z nią umówiony i się nie pojawiła. To nie w jej stylu."
* A: (czerwienieje) "Maja jest dobrze wychowaną dziewczyną. Nie przyjdzie bo jej zakazałem."
* K: "Spytaj się jej przy mnie. Jeśli powie 'nie', to sobie pójdę"

Tr +3:

* Vr: Albert pójdzie do Mai.

Po chwili wrócił, drzwi prawie eksplodowały: "GDZIE JEST MAJA!" (zmartwiony i _wściekły_) Gdzie ją... (i po chwili się zreflektował)... "CZEMU TU PRZYSZEDŁEŚ SKORO TY JĄ MASZ?!" Karolinus: "przepraszam, myślałem że Maja jest w środku, ale macie inne problemy..."

Albert szybko spojrzał na Karolinusa, nie znalazł w nim śladu bezczelności i nieprawdy (pobieżnie) po czym ruszył do pokoju amplifikować sentisieć.

* V: Karolinus przekonał Alberta (w 10 sekund) że może pomóc w wykryciu Mai bo wie o niej sporo. Bo ona go lubi. Jak wujka.

Zaklęcie detekcyjne Mai, szukamy jej.

Tr Z M +3 +3Ob:

* Vz: (podbicie) Albert dzięki Karolinusowi szybciej namierzył Maję
* Vm: (manifest) Karolinus z pomocą Alberta wyczyścił swój ślad z sentisieci. Nic nie pokazuje, że Karolinus czy Amanda mieli coś z tym wspólnego. Nic. I Albert ma lokalizację Mai szybko.
* Vr: (eskalacja) Albert jest wdzięczny Karolinusowi i poprawił opinię na jego temat. Bardzo. Zwłaszcza w kontekście Mai.
* X: Wirgot jest "na straży" Lemurczaków. To neutralizuje sentisieć Alberta.
* X: Albert jest zbyt zestresowany by móc używać skutecznie sentisieci. Nie spodziewał się Lemurczaków. Nie tak.
* V: Albert rozluźni. Widząc, jak bliski był straty córki i że nie mógł jej ochronić zdecydował się na to, by jednak dać jej trochę wolności i nie stracić córki. By była bezpieczna. Córka > dobre małżeństwo córki.

Albert i Karolinus biegną do odpowiedniego pokoju. Na drodze - Wirgot

* W: "tien Albercie..."
* A: "tam jest moja córka!"
* W: "nie, tien Albercie..."
* A: "ZEJDŹ MI Z DROGI!" i używa magii przeciw Wirgotowi. Ten oddaje.

Karolinus próbuje przekonać Alberta i Wirgota do rozwiązania sprawy.

Tr Z (+dowody) +3:

* Vz: (podbicie) pomagamy Mai. Wirgot SPIERDALAJ z drogi.
* V: (manifest) Wirgot, Albert oraz Karolinus ratują Maję razem.
* Vz: (podbicie) Albert jest skłonny do "zamknięcia granic" - to, że są jakieś mroczne siły, jakieś Złote Cienie, jakieś bazy podziemne - to może skrzywdzić jego córkę.
* V: (manifest) Z pomocą Strzały i Kajrata da się sfabrykować dowody, że hiperpsychotronicy są powiązani z cierpieniem Mai. A Albert jednej rzeczy - cierpienia córki - nie wybaczy.
* X: Amanda MUSI działać by ratować Maję.
* X: Lemurczakowie będą uznani za takie same ofiary jak Maja
* Vr: Szefostwo Złotego Cienia zostanie uznane za współwinne. Nie są arystokratami ale gdyby nie oni to nic z tego by się nie stało. Za daleko poszli. Weszli w politykę.

Tymczasem, nie czekając na nic, Amanda rozstawiona z ciężką snajperką na dachu, chroniona przed sentisiecią, próbuje odepchnąć (kogo ja okłamuję - zastrzelić ale nie terminalnie) Vanessę. Amanda nie jest fanką tortur. Ale spoko, to tienka.

Tr Z (+namierzanie ze strony Karolinusa / +Mistrzostwo Amandy, -Pałac Harmonii jest chroniony, +sentisieć i defensywy szaleją) +2:

* V: Amanda bezpiecznie się przedostała i rozłożyła snajperkę
* X: Amanda musi porzuć broń po operacji
* Xz: Są ranni wśród ludzi; namierzanie Karolinusa pod stresem było gorsze
* X: (podbicie) Karolinus jest odpowiedzialny za swojego snajpera.
* V: (podbicie) Vanessa jest odbita od Mai.
* V: (manifest) Vanessa jest solidnie ranna. Nie chce walczyć. Przerwała krwawy trans.
* V: Amanda się bezpiecznie oderwała i wycofała.

EPILOG:

* Maja - 2 tygodnie w szpitalu. Vanessa zna się na swojej robocie, bo Maja by nie przeżyła.
* Albert - stoi po stronie Mai. Zaufał Karolinusowi.
* Wirgot - bardzo próbuje udawać że nic nie wie. MA WPIERDOL. Od Lemurczaków i od Alberta. Ale nie jest skazany na banicję czy coś. Za to jakiego planu nie miał, zarzuca
* Lemurczakowie - dostali odszkodowanie i przeprosiny. Opuścili ten teren.
* Złoty Cień - traci "głowę". Konieczne jest zreorganizowanie firmy _concierge_.

## Streszczenie

Karolinus namówiony przez Kajrata chce zrzucić swoje winy na współpracujący z handlarzami ludźmi Złoty Cień. By ich zniszczyć, manipuluje młodą Maję i wystawia ją jako ofiarę dla Lemurczaków zaproszonych przez Wirgota. Maja zostaje strasznie skrzywdzona, ale Złoty Cień jest złamany, Karolinus ma sojusz z Albertem, Lemurczakowie zostali odepchnięci a Wirgot ma w Albercie śmiertelnego wroga.

## Progresja

* Maja Samszar: BROKEN! Maltretowana 3h przez Lemurczaków (Jonatan, potem Vanessa), złamana i rozbita. 3 następne tygodnie w szpitalu na rekonstrukcję.
* Maja Samszar: COŚ wstrzykiwał jej Jonatan Lemurczak. COŚ jej zrobił. Nie wiedział, że jest tienką, więc to może działać dziwnie.
* Maja Samszar: Jej rekonstrukcja wymaga: amnestyków, integracji z duchami ("przeszczep"), detoksu, regeneracji tkanki... naprawa ciała, umysłu, duszy.
* Maja Samszar: Od tej pory przygotowanie jedzenia jest tym co ją 'stabilizuje' i jej obsesją, ma luki w pamięci, całkowicie traci link z duchami Samszarów i randomowo się boi i płacze.
* Maja Samszar: Ojciec - Albert - skupia uwagę na niej jako osobie a nie "perfekcyjnej córeczce by ją dobrze wydać". Chce jej dobra i regeneracji.
* Maja Samszar: Jej pamięć rejestruje rzeczy źle. Rozerwała link z Karolinusem (nie ostrzegł jej) i z Eleną (odbiła jej Romeo). Jest skrzywdzona i sama.
* Albert Samszar: przekierował uwagę na Maję, schodząc z pozycji, reputacji itp. Sojusz z Karolinusem, którego uważa za pozytywny wpływ na Maję.
* Albert Samszar: jego zdaniem, hiperpsychotronicy są powiązani z cierpieniem Mai. A on nie wybaczy cierpienia córki. ŚMIERTELNY WRÓG Wirgota Samszara, Vanessy Lemurczak, Jonatana Lemurczaka
* Karolinus Samszar: sojusz z Albertem Samszarem, oparty o to, że oboje chcą jak najlepiej dla Mai.
* Vanessa Lemurczak: Ustrzelona przez Amandę snajperką, wymaga regeneracji kilka dni.

## Zasługi

* Karolinus Samszar: wykorzystał Maję jako przynętę, manipulując ją by ona sama chciała być 'jak dorosła Vanessa' i zmieniając ją magią. Ale gdy Maja była u Lemurczaków odpowiednio długo, przekonał Alberta że ona potrzebuje pomocy i skupił wszystkich Samszarów do pomocy Mai. Udało mu się rozognić wojnę Albert - hiperpsychotronicy i odepchnąć Lemurczaków, ale jakim kosztem...
* Amanda Kajrat: przyhaczyła Maję na imprezie, dała jej tabletkę i przekazała agentom Wirgota by dali ją Lemurczakowi. Potem dostała się na dach i ustrzeliła Vanessę Lemurczak gdy ta torturowała Maję. Nie dbała o stan Mai - to tienka. Wszystko dla Kajrata - przechwycić dlań grupę przestępczą Złoty Cień.
* Maja Samszar: naiwna uwierzyła Karolinusowi i chciała zobaczyć jak to jest gdy jest się dorosłą kobietą i "nie Mają". Dostała wygląd Vanessy Lemurczak. Poszła do baru, gdzie dostała od Amandy pigułkę, trafiła do Lemurczaków na 3h zabawy. Skończyła w bardzo ciężkim stanie.
* Jonatan Lemurczak: nie lubi Vanessy. Jest u Samszarów z prośby Wirgota i podejmowany jako gość. By jej 'pokazać' zażądał niewolnicy wyglądającej jak ona (i dostał Maję). Miał 3h zabawy z Mają, uwzględniając chemikalia.
* Vanessa Lemurczak: nie lubi Jonatana. Gdy Jonatan bawił się z niewolnicą wyglądającą jak Vanessa (Maja), zrobiła jej straszną krzywdę by pokazać Jonatanowi co jego może czekać. Demonstracja koszmarności. Ustrzelona przez Amandę snajperką, wymaga regeneracji kilka dni.
* Wirgot Samszar: silny mag który zaprosił Jonatana i Vanessę do Karmazynowego Świtu. Chce ich zachwycić hedonizmem. Potrzebował pomocy Jonatana by coś osiągnąć, nawet, jeśli nie był fanem działań Lemurczaków. Chronił ich lojalnie przed Albertem aż się dowiedział co się stało.
* Albert Samszar: na wakacjach, odstresowuje się; o dziwo, niespecjalnie interesuje się hedonizmem. Trzymał Maję pod kloszem, przez co wpadła w pierwszego manipulatora. Zawiódł córkę. Ale by ją ratować był skłonny nawet samemu zaatakować dwóch Lemurczaków.
* Nadia Obiris: agentka Złotego Cienia; na callu z Karolinusem powiedziała jakie blondynki ma dla niego do oferty. Karolinus ją namierzył sentisiecią i podał lokalizację Amandzie.

## Frakcje

* Złoty Cień Karmazynowego Świtu: grupa przestępcza "w białych rękawiczkach" działająca dla Samszarów. Lokalni, eleganccy, bogaci, mają możliwości. W Karmazynowym Świcie (Samszar) dostarcza eleganckim i bogatym tego, czego sobie życzą. Współpracują z handlarzami ludźmi i lokalnym biznesem.
* Złoty Cień Karmazynowego Świtu: ich centrala i dowodzenie zostało zniszczone przez Samszarów, bo "z ich winy" Maja Samszar wpadła w ręce Lemurczaków. Są bez głowy i nie mają władzy.
* Hiperpsychotronicy: zdobyli wroga w Albercie Samszarze; uważa, że to oni doprowadzili do skrzywdzenia jego córki. Te plotki sprawiają, że muszą działać wolniej i ciszej.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Karmazynowy Świt
                                1. Centralny Park Harmonii
                                    1. Pałac Harmonii: super-luksusowy hotel gdzie byli Lemurczakowie, Wirgot, Albert i Maja. I gdzie doszło do Złamania Mai.
                                    1. Gwiezdna Rozkosz: mniejszy love hotel gdzie mieszkał Karolinus z Amandą

## Czas

* Opóźnienie: 4
* Dni: 3

## Specjalne

* .

## OTHER
### Fakt Lokalizacji
#### Karmazynowy Świt

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Karmazynowy Świt: luksusowe miasteczko Samszarów; ekonomia oparta na usługach, ze szczególnym naciskiem na turystykę, wellness i rozwój osobisty
                                1. Centralny Park Harmonii
                                    1. Eko-spa
                                    1. Sztuczne Plaże
                                    1. Ogrody medytacyjne
                                    1. Pałac Harmonii: luksusowe miejsce odpoczynku i epicki hotel
                                    1. Gwiezdna Rozkosz: mniejszy love hotel
                                1. Rezydencje
                                    1. Szkoły i sale treningowe
                                    1. Biblioteka
                                1. Zewnętrzny pierścień wygaszający: zapewnia ciszę i spokój
                                1. Kraina Winorośli: winnice, winiarnie, restauracje
                            1. Karmazynowy Świt, okolice
                                1. Miasteczko Płomiennik
                                    1. Klinika Oteriiel: zajmują się wszystkim co jest potrzebne odnośnie modyfikacji bioformy, korupcji i dostarczania młodych dam i lordów do towarzystwa
                                    1. Domy
                                    1. Komenda Straży
                                    1. Tereny Rolne
                                