## Metadane

* title: "Pustogorski Konflikt"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [190419 - Osopokalipsa Wiktora](190419-osopokalipsa-wiktora)

### Chronologiczna

* [190419 - Osopokalipsa Wiktora](190419-osopokalipsa-wiktora)

## Budowa sesji

Pytania:

1. .

Wizja:

* Wiktor zmusił Pustogor do rzucenia nadmiernej ilości terminusów, przez co Pustogor nie miał dość sił
* Korzystając z okazji, kilku Miasteczkowców rzuciło na rynek mnóstwo artefaktów z kryjówek
* Część z tych artefaktów ma paskudne efekty uboczne, np. Amplifikator Dominacji

Tory:

* Król Grzymość: 5
* Wojna Pustogorska: 3
* Eksodus Miasteczka: 5
* Cierpienie Ludzi: 4
* Ranna Pięknotka: 4

Sceny:

* Scena 1: Pięknotka w Górskiej Szalupie (zatrzymać wiec)
* Scena 2: Pięknotka na tropie czarodziejek lub Grzymościa

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena 1: Pięknotka w Górskiej Szalupie**

Przez działanie Wiktora doszło do tego, że Pustogor musiał rzucić nadmiar terminusów dookoła. Trochę się przeszmuglowało różnych artefaktów i spraw. Barbakan jest wściekły. Karla kazała Pięknotce - bo ta ma dobre relacje z Miasteczkiem - dojść do tego kto jest odpowiedzialny za szmugiel i go aresztować. Pięknotka nie była w stanie tego zrobić. Karla wysłała innych magów na to, jednak doszło do eskalacji sytuacji i Miasteczkowcy zamknęli terminusa w piwnicy.

Karla ma dwie opcje. Uderzyć potęgą Pustogoru lub zdeeskalować sytuację. Dlatego posłała Pięknotkę i Alana. Alana do innego miejsca w Miasteczku, Pięknotkę bo - jako kobieta - jest mniej groźna. Pozornie. Alan... cóż. Ma opinię i nie nadaje się na negocjatora.

Pięknotka znalazła się w Miasteczku, w Górskiej Szalupie. Wpuścili ją do środka, bo przyszła sama i bez power suita. Oczywiście, jest gotowy Cień.

Na miejscu odbywa się wiec. Grupa Miasteczkowców, na pierwszym planie - Wojmił Siwywilk. Jest to Rycerz Potęgi x Firebrand, wolnościowiec. Zaczął przemową, że Barbakan zabiera najpotężniejsze artefakty i Senetis podmienia je na jakieś byle co lub wypłaca. Fakt, Pięknotka to wie - ale tylko te z Black Technology. Jego przemowa podgrzewa tych 15-20 magów. Pięknotka nie jest zbyt szczęśliwa.

Pięknotka uderzyła w to, że Wojmiłowi zabrali artefakt - ale ten artefakt jest zły. Integrator z układem nerwowym co wyżera duszę i sprawia, że zabijasz. Wojmił powiedział, że ma prawo decydować. Pięknotka, że nie kosztem innych. Zwróciła się też do innych - dostali odszkodowania i pokazała jakie to są artefakty. A przy okazji pokazała co się dzieje jak Pustogor tego nie zatrzymał, co się stało w wyniku użycia takich artefaktów (TrZ+3:13,3,6=S). Linia podburzania Miasteczkowców padła. Wojmił nie dostanie tego, co chce. Miasteczkowcy zaczęli ze sobą gadać.

Olaf zwrócił się do Pięknotki na uboczu bezpośrednio. Co z jego dwoma dziewczynami. Czasami terminusi robią mały nalot by porwać jakieś artefakty Miasteczkowcom; przodują w tym zwłaszcza terminusi z fortu Mikado (nie Solus; Solus jest spoko). Jest to element ryzyka. Ale tym razem po nalocie zniknęły dwie dziewczyny - Talia i Irma. Jeden terminus pokonał czterech Miasteczkowców i system defensywny. Olaf przekazał psychoskan Pięknotce.

Zgłosił? Oczywiście, że tak. Ale Barbakan teraz wszystko ignoruje. Jest przeładowany. A laski zniknęły wczoraj. Ale ktoś uderzył w NASZYCH ludzi. Pięknotka powiedziała - może wziąć tą sprawę. Ale ją martwi to, co poszło w miasto. Niech Olaf wypuści terminusa i nie przeszkadza jej w śledztwie. I pomoże zebrać informacje o tych co najbardziej niebezpiecznych artefaktach. (Tr+2:12,3,6=SZ). Reputacja Barbakanu przeważyła - Olaf pomoże. I terminus wyjdzie.

Na koniec wyszedł Wojmił. Powiedział, że jak oddadzą terminusa, skończą w większej dupie niż są teraz. Pokonfiskują im artefakty, poaresztują... a tak mają przynajmniej argument przetargowy. Pięknotka wystawiła Wojmiła na celownik - a ile jemu udało się sprzedać? Jak bardzo on się boi, skoro chce doprowadzić do tego, żeby Pustogor nie zaczął pilnować niebezpiecznych artefaktów? Sukces. Wojmił został zagłuszony przez pozostałych Miasteczkowców. Nadal nie ufają Barbakanowi, ale są skłonni wypuścić terminusa i deeskalować sytuację.

Wojmił najpewniej opuści Miasteczko do Wolnych Ptaków.

**Scena 2: W poszukiwaniu dziewczyn**

Karla spytana czego nie powiedziała, powiedziała coś jeszcze Pięknotce - tak jak kilka lat temu Pustogor opuściła grupa bardziej radykalnych magów i poszła na południe tak teraz nadszedł czas na ponowny _purge_. Niech najbardziej radykalni magowie opuszczą teren i dołączą do Wolnych Ptaków, na południu, w bardziej niebezpiecznym miejscu.

Pięknotka dowiedziała się od Karli, że w rajdach (których Karla nie rekomenduje, acz je rozumie) wykorzystywane są inne modele power suitów, nie pustogorskie. Pięknotka wzięła więc nagranie Olafa i przepuściła je przez systemy Barbakanu. Na co ona patrzy. Zobaczyła power suit starej klasy z neurosprzężeniem, który rozwala czterech magów. Nie są w stanie działać tak szybko jak neurowspomagany power suit. Ruchy kojarzą jej się z Cieniem. Jakby to była żywa istota. Wydobyła numer seryjny power suita.

Niestety, w tej chwili power suit jest nieaktywny. Nie jest to Pustogorski power suit, ale jest to jeden z tych "rajdowych". Grzymość powinien coś więcej wiedzieć. Albo on, albo ktoś w forcie Mikado. Może.

Pięknotka poszła więc do... Erwina. Największego eksperta od power suitów w historii Pustogoru i pokazała mu nagranie. Czy może Erwin go znaleźć. Erwin się zastanowił. (Tp:9,3,4=S). Erwin zna ten power suit. Naprawiał go; był w bardzo kiepskim stanie. Miał rozwalony cały system sterowania. Kupiec prosił o system do walki wręcz; Erwin wykorzystał system z innego modelu, ale ten power suit nie wytrzymałby takich manewrów. Ktoś inny go zmienił. I to zmienił go w sposób niemożliwy zgodnie ze strukturą power suita.

Erwin w skrócie powiedział, że TEN power suit nie może tak się poruszać. Nie bez bionano, podobnego do Cienia. Czyli to, co widzą na nagraniu to nie jest ten power suit. To coś podobnego do niego. Ale Erwin wie, komu naprawiał power suita. To był niejaki Marcel Nieciesz, terminus pustogorski z fortu Mikado.

Pięknotka poszła go odwiedzić w Dzielnicy Mieszkalnej. Przywitał ją z uśmiechem - to też sympatyczny, waleczny terminus. Z przyjemnością Pięknotce pomoże - ma chwilę przerwy. Pięknotka chce dowiedzieć się od niego na temat tego konkretnego power suita. Chce zobaczyć jak wygląda sytuacja. (Tp+2:SS). Marcel zapytany o powersuita się uśmiechnął, po czym zaatakował Pięknotkę włączając selfactivated coś, co nie ma prawa być selfactivated. Torturowana maszyna otoczyła terminusa który złapał Pięknotkę za gardło. Palec zaczął zbliżać się w kierunku ust Pięknotki...

...na co uruchomił się Cień. Pięknotka się odrywa od tego czegoś. (TpZ:9,3,4=S). Cień napiął się i eksplodował, odrzucając torturowany power suit przeciwnika i rekonstruując się na Pięknotce. Pięknotka poczuła nienawiść i miłość power suita. Cień "rozpoznał" przeciwnika. Pięknotka widząc stan psychiczny swojego przeciwnika, że został pochłonięty przez power suit, dała Cieniowi lekki upust.

Cień wyrwał kość Marcelowi z barku. Power suit Marcela jednak kontynuował walkę - apartament jest zrujnowany. Wojna przebiła się przez ścianę, do sąsiedniego apartamentu. Finalnie jednak Cień zwyciężył. Wrogi power suit i terminus trafili od razu zapakowani do Senetis. Jako całość.

Pięknotka poprosiła w Senetis o wstępną analizę. Dostała odpowiedź - przykład Black Technology pochodzący ze Szczeliny. Nie jest to pierwszy tego typu przypadek, jest to zmodyfikowany mimik symbiotyczny.

Tej informacji nie udało się ukryć przed magami.

**Scena 3: W poszukiwaniu dziewczyn, podejście dwa**

Pięknotka zdecydowała się wymusić na Senetis wydobycie informacji od Marcela. Nawet kosztem uszczerbku na zdrowiu. Złamał wszystkie zasady i procedury. Senetis przeskanowało jego umysł (SS). Okazało się, że mimik podczepił się pod komponent związany z rajdami i atakowaniem. Chęć dominowania, bycia lepszym, "ci brudni miasteczkowcy". Dlatego mimik zadziałał i terminus jak tylko miał kontakt z mimikiem to od razu przegrał. Nie miał szans.

Wziął sobie dwie dziewczyny, bo na nie zasłużył, po prostu. Są w tej chwili pod Pustogorem, niedaleko. Mimik wytworzył tam potężne pole łamiące umysły w odpowiedzi na jego pragnienie; jest tam druga część mimika, trochę więcej śluzu.

Widząc sytuację, Pięknotka zdecydowała się posłać tam po prostu oddział konstruminusów. One wypalą teren i uratują nieszczęśniczki.

Wpływ:

* Ż: 5
* Zespół: .

**Sprawdzenie Torów** ():

2 Wpływu -> 50% +1 do toru. Tory:

* Król Grzymość: /5
* Wojna Pustogorska: /3
* Eksodus Miasteczka: 2/5
* Cierpienie Ludzi: 2/4
* Ranna Pięknotka: /4

**Epilog**:

* Nieszczęśniczki zostały uratowane. Trafiły do Szpitala Terminuskiego i wrócą do normy, acz opuszczą ten teren. Już nie chcą.
* Marcel wydobrzeje po dwóch tygodniach i wróci do służby. Nigdy już nie zrobi rajdu. Będzie rajdy zwalczał.
* Napięcie między Miasteczkiem a Barbakanem rośnie. Część magów rozważa odejście do Wolnych Ptaków.

## Streszczenie

Napięcia na linii Barbakan - Miasteczkowcy w Pustogorze są silne; działania Wiktora jedynie przyspieszyły konflikt. Na rynek dostało się sporo niebezpiecznych artefaktów i Pięknotka musiała pomóc grupie Miasteczkowców. Zdesperowani, złapali terminusa i zamknęli go w piwnicy bo podczas jednego z rajdów na Miasteczkowców magowie z Fortu Mikado porwali dwie dziewczyny. Okazało się, że za tym stoi zaawansowany mimik symbiotyczny; Pięknotka z pomocą Cienia pokonała Skażonego terminusa i uwolniła czarodziejki.

## Progresja

* .

### Frakcji

* 

## Zasługi

* Pięknotka Diakon: zaczęła jako negocjatorka z Miasteczkowcami a skończyła walcząc w Cieniu przeciwko wzmocnionemu mimikowi symbiotycznemu Marcela.
* Wojmił Siwywilk: Rycerz Potęgi i Firebrand; chciał podpalić Miasteczko przeciw Barbakanowi, ale Pięknotka rozmontowała jego argumenty. Najpewniej odejdzie do Wolnych Ptaków.
* Erwin Galilien: najwybitniejszy ekspert od power suitów; ostrzegł Pięknotkę że power suit Marcela nie jest normalnym power suitem, bo zachowuje się nietypowo.
* Olaf Zuchwały: wyrósł na nieformalnego przywódcę Miasteczkowców; negocjował z Pięknotką i powiedział jej o tragedii dwóch Miasteczkowiczanek.
* Marcel Nieciesz: sympatyczny terminus z Fortu Mikado w Pustogorze, który padł ofiarą mimika symbiotycznego i porwał dwie Miasteczkowiczanki. Jego mimik został pokonany przez Cienia.
* Karla Mrozik: raz na jakiś czas organizuje w Pustogorze kryzys, by co bardziej zawadiackich wolnościowców z Miasteczka wygnać do Wolnych Ptaków. Pustogor przede wszystkim.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Rdzeń
                                    1. Barbakan: miejsce, gdzie Pięknotka dostaje rozkazy od Karli
                                1. Interior
                                    1. Laboratorium Senetis: miejsce które bada wszystkie artefakty i anomalie Szczeliny; tam też trafił Marcel w mimiku symbiotycznym
                                    1. Dzielnica Mieszkalna: miejsce ciężkiego starcia pomiędzy Pięknotką a Marcelem w mimiku
                                1. Eksterior
                                    1. Miasteczko: siedziba wolnościowców i osób zdecydowanie niechętnych kontroli
                                        1. Knajpa Górska Szalupa: najlepsza knajpa w Miasteczku i przy okazji nieformalne centrum dowodzenia ruchu oporu


## Czas

* Opóźnienie: 2
* Dni: 1
