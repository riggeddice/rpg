## Metadane

* title: "Nastolatka w bieliźnie na dachu w burzy"
* threads: mlodosc-klaudii
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [211010 - Ukryta wychowanka Arnulfa](211010-ukryta-wychowanka-arnulfa)

### Chronologiczna

* [211010 - Ukryta wychowanka Arnulfa](211010-ukryta-wychowanka-arnulfa)

### Plan sesji
#### Co się wydarzyło, historycznie

* W okolicach Trzęsawiska Zjawosztup pojawił się oficer Saitaera. Naprzeciwko niemu stanął Wiktor Satarail - i stał się częścią Trzęsawiska.
* Noctis oddelegowało 'ON Tamrail' z eskortą by zapewnił bezpieczeństwo tego obszaru. Tamrail został Skażony mocą Saitaera i rozpadł się na terenie zwanym w przyszłości Nieużytkami Staszka.
* Siły Noctis znalazły się w krzyżowym ogniu między mocą Saitaera i Astorii. Ich bazą stało się Czółenko (stąd Esuriit w przyszłości), główne walki toczyły się o obszar Nieużytków Staszka i elementów Lasu Trzęsawnego.
* Aż Noctis zostało pokonane. Wycofali się, biedni, na Trzęsawisko Zjawosztup - gdzie już rekonstruował się Wiktor Satarail.
* Doszło do masakry...
* Teraz jesteśmy 711 dni po oficjalnym zakończeniu wojny i zmiażdżeniu sił noktiańskich. 
* Ciężko uszkodzona jednostka 'koordynacyjno-dowódcza' Ślepacz została podstawą areny migświatła. Nadmierna moc psychotroniczna na potrzeby.

#### Co się wydarzyło, na tej sesji

.

#### Sukces graczy

* odwrócić uwagę Saszy i Protazego od AMZ

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Noc. Ostra burza. Ksenia budzi Klaudię - "widziałaś to?". Klaudia patrzy, ostra burza. Nikogo nie widzi. Ksenia, sfrustrowana, "BYŁA TAM". "Dziecko, młoda, w bieliźnie, na dachu Złomiarium". Nie całkowicie dziecko, młoda nastolatka?

Klaudia jest lekko sceptyczna. Ksenia nie jest przekonana, że coś widziała. Klaudia wraca do łóżka. Parę minut później Ksenia budzi Klaudię "patrz szybko". Klaudia patrzy. FAKTYCZNIE, jest tam. To Teresa Mieralit. Ksenia wyskakuje - praktycznie "trzeba ją ratować"! Klaudia bierze jakiś ciuch. Miga błyskawica - Teresa zniknęła. Znowu. Ksenia jest PRZEKONANA że widziała kogoś i że musi ją znaleźć i ratować. Klaudia, wiedząc, że Teresa jest w systemie AMZ mówi, że ją poznaje. 

Klaudia wzięła ciepłe ponczo przeciwdeszczowe. Obie - Klaudia i Ksenia - pobiegły w kierunku Złomiarium i AMZ. Klaudia zna drogi; szybko poprowadziła Ksenię do Złomiarium. Ksenia zatrzymała Klaudię - "ktoś tu jest, widziałam ruch, ona jest gdzieś tu" - w okolicy Złomiarium, na dole. Klaudia woła "Teresa", by głos się niósł. Ksenia zaczyna szukać. Mimo deszczu i kiepskich śladów Ksenia jest dobra w szukaniu, ma talent.

ExZ+3:

* XXX: Ksenia nie znalazła nikogo. Klaudia zaczyna podnosić głos - "Teresa, to nie jest zabawne". I wtedy przybył terminus Sasza.

.

* "Terminus? Co pan tu robi?" - Klaudia, zaskoczona
* "Co wy tu robicie?" - zaskoczony terminus

Klaudia szybko pinguje Arnulfa. Hipernet odcięty. Ksenia próbuje odpowiedzieć, ale Klaudia wbija jej się w słowo - "wydawało nam się, że widzimy coś na dachu". Sasza jest bardzo zdziwiony - na dachu? Niech one wrócą do akademika. On sprawdzi o co chodzi. Klaudia prychnęła. Sasza: "Kim jest Teresa?" "Uczennicą. Podejrzewam, że lunatykuje". "To trzeba jej pomóc..." - Sasza teraz włączył jetpack i leci na dach. Niech Klaudia i Ksenia krzykną jak ją zobaczą. Trzeba ją przechwycić.

Ksenia do Klaudii - wtf? Klaudia nie ma połączenia ze Strażniczką... dopiero dwa dni temu została wyłączona.

Ksenia pokazała Klaudii ręką - Teresa jest na krawędzi budynku, wyraźnie ukrywa się przed terminusem. "Jesteś w stanie się dostać do niej przed terminusem?" -> Ksenia ocenia sytuację. Trudno jej będzie, bardzo.

Klaudia ma ogromną prośbę do Ksenii - "Ten terminus nie lubi naszego dyrektora. Jest podejrzliwy wobec AMZ. A ta dziewczyna przeszła swoje podczas wojny i się go wystraszy bardziej niż powinna. Ja ją zawołam, ale Ty odwróć uwagę terminusa. Nie udało się przekrzyczeć burzy czy coś... odwróć jego uwagę." Ksenia rzuciła Klaudii dziwne spojrzenie - "wisisz mi opowieść. Zabierz ją do naszego pokoju. Lub daj jej klucz, nie wiem. Pomogę Ci."

Ksenia odwraca uwagę terminusa - próbuje wpaść w tarapaty w innym miejscu. I przeraźliwie krzyczy "HELP!".

TrZ+3:

* X: opsnęła się i poturbowała
* Vz: terminus ją złapał zanim coś jej się poważnego stało. Ksenia jest obita. Ma siniaki. Ma też opierdol od terminusa.

Klaudia krzyczy do Teresy po noktiańsku: "Zejdź do mnie! Niebezpieczeństwo!" Kiepski noktiański, ale zawsze.

TrZ+3:

* X: Terminus podejrzewa, że dziewczyny COŚ planują, ale nie ma dowodów. Będzie je grillował.
* V: Teresa zwinnie ześlizgnęła się, zeskoczyła i schowała na ziemi, niedaleko Klaudii. Klaudia widzi rany na jej ciele. Rany się zamykają - ale to nadal boli.

Klaudia podrzuca jej cieplejsze ciuchy w krzaki. Teresa wzrusza ramionami, zakłada. WIE, że musi się stąd wydostać. Klaudia rzuca Teresie klucz do swojego pokoju w akademiku. Niech Teresa tam się schowa. Teresa mówi, że nie dostanie się do Arnulfa - Sasza ma dwóch pomocników. Klaudia powiedziała - jak ktoś ją zapyta, Teresa lunatykowała.

* Xz: Teresa MUSIAŁA biec inną ścieżką - więc napotkała ochronę AMZ. Powiedziała, że lunatykowała...
* X: ...ale jej nie uwierzyli, więc się "przyznała", wraca od chłopaka z którym spędziła noc. Gorzej, że jest nieletnia.
* V: spisali ją (Teresa), ale "wróciła" do akademika. Gorzej, że jej zeznanie jest w systemie itp.

A Klaudię i Ksenię złapał terminus. Weszli do Złomiarium, by nie być na deszczu.

* "Czemu byłyście na zewnątrz? Kim jest Teresa?"
* "Uczennicą".
* "Lunatykuje?"
* "Zdarza jej się, z tego co wiem. Kiedyś mi to powiedziała..."

Klaudia próbuje okłamać Saszę. Klaudia nie jest w tym dobra, ale Sasza nie spodziewa się prób kłamania. Klaudia ma świetną opinię. Ale Sasza podejrzewa że dziewczyny coś ukrywają. A Klaudia próbuje mówić jak najwięcej prawdy. Klaudia próbuje siebie i Ksenię wyplątać... nie są w stanie odwrócić uwagi od Teresy, nie tu i nie teraz.

Tr (niepełna) Z (zaufanie do Klaudii) +3:

* X: Sasza przyjrzy się uważnie Teresie. Podejrzewa ją o prostytucję. "Lunatykuje my ass".
* Xz: Sasza uważa, że Klaudia i Ksenia pomagały Teresie - niekoniecznie wiedziały, ale na pewno łączą szyki by chronić czarodziejkę AMZ. WIE, że schowały Teresę bo poznał ponczo.
* Vz: Sasza odpuszcza Klaudii i Ksenii. Nie są jego celem. Jego celem nie są czarodziejki AMZ. Tylko ostro je opieprzył, wyjaśnił, że chronią dziewczynę - sierotę - która najpewniej się prostytuuje. KSENIA NIC NIE WIE I MA SZCZĘKĘ PRZY ZIEMI. Klaudia zaskoczona z innych powodów. Sasza zauważył, że Klaudia przekazała jej ponczo. Sasza powiedział, łagodniej, że on rozumie lojalność grupową itp. Ale prostytucja nieletnich przez sierotę to nie jest sposób na życie.
* X: Klaudia chciała mu pomóc (dowiedzieć się czemu Sasza tu jest), ale terminus ją spławił. Niech ona i Ksenia idą do domu. 

Klaudia nie zgadza się na coś takiego. Ona musi wiedzieć na czym polega problem. Ale... jak niebezpieczny jest Sasza dla niej i dla Arnulfa?

WSTECZNY KONFLIKT: co Klaudia wie o Saszy? 

ExZ (uprawnienia) +3 (wcześniejsze badania + Tymon):

* Xz: Sasza i inni terminusi wiedzą, że Klaudia szukała na ich temat. I na temat Saszy.
* Vz: Klaudia zebrała informacje o terminusach jakie chciała zebrać:
    * Sasza nie jest antynoktiański. Jest przeciwny Tymonowi, Orbiterowi i samowolce. Ale nie Noctis. Nie lubi ich, ale np. Teresy by pożałował a nie zniszczył.
    * Sasza jest nieustępliwy. Nie darowałby Strażniczce. To jest abominacja.
    * Współpraca Tymon x Talia jest abominacją - łączenie, integracja z noktianką tam gdzie Tymon nie kontroluje sytuacji.
    * Saszy zależy na magach AMZ. Chce, by te dzieciaki były w dobrej formie. He cared for a sleepwalker - abortował misję by pomóc dziewczynie.
    * Naprawdę. Naprawdę jest przeciwny prostytucji nieletnich i desperacji noktian. Uważa, że Grzymość się rozwinie i to jest złe.
    * Gdyby Sasza wiedział, że Arnulf opiekuje się Teresą a ona jest prostytutką (nie jest...) to użyłby to przeciwko niemu.
    * Gdyby doszło co do czego, Sasza chroniłby Talię przed napastnikami.

POWRÓT DO RZECZYWISTOŚCI:

Klaudia mówi Saszy, że to ona organizuje większość współpracy między terminusami i AMZ. Widzi więcej, ma większe dostępy. Może pomóc. Sasza spytał, bezpośrednio, czemu Klaudia szukała informacji na jego temat wczoraj. Klaudia - jest zainteresowana dobrem Akademii i nie rozumie motywów terminusów a on kręcił się dookoła AMZ.

Tr(niepełny)Z+3+3O (Klaudia przekierowuje na innych z grzeszkami):

* V: Klaudia zrzuciła z siebie podejrzenia Saszy. Faktycznie, może mu się przydać. Godne narzędzie.
    * "AMZ jest ważne. Produkuje kadry dla nas wszystkich na przyszłość. A co jeśli produkuje złe kadry? Jeśli nie da się im zaufać?" - Sasza, ze smutkiem.
    * "Jestem blokowany przez źle rozumianą lojalność. Nie postrzegaj terminusów jako siłę, która wam zagraża..." - Sasza, wyjaśniając.
* X: Klaudia dała Saszy informacje o tym jak niektórzy magowie AMZ (uczniowie) robią złe rzeczy wobec ludności. Tzn. nadużywają, zabierają im surowce by wzmocnić rodziny itp. Sasza dzięki temu mógł zadziałać i "naprawić zło", ale Klaudia została "konfidentką" i tamci niekoniecznie ją lubią. Ona może z tym żyć.
* Vz: Sasza powiedział Klaudii czego szuka i o co chodzi
    * Arnulf został zwiedziony przez Tymona. Tymon jest na tym terenie z jakiegoś powodu. Działa w imieniu Orbitera, nie Szczelińca.
    * Sasza szuka, co Tymon zrobił w AMZ. Jaki tajny projekt realizuje - sam lub z dyrektorem. Dlatego wszedł tu po cichu. Nie wie gdzie szukać, jeszcze.
    * Sasza podejrzewa po dniu dzisiejszym że Teresa jest kluczem - a dokładniej, do linku pomiędzy albo Tymonem i Grzymościem albo Arnulfem i Grzymościem.
    * Prywatnie, po czynach Tymona, Sasza podejrzewa, że Tymon jest linkiem pomiędzy Orbiterem a Grzymościem.
* X: Klaudia powiedziała, że potrzebuje czasu. Poszuka i o Grzymościu i o Tymonie. Sasza powiedział, że Klaudia potrzebuje ochrony jeśli ma zamiar to robić. Sasza, innymi słowy, NIE ZEZWOLIŁ JEJ na to by szukała w kierunku na Grzymościa...
* V: ...ale zaakceptował to, że potrzebuje czasu i że pasywnie spróbuje coś znaleźć. Ale aktywnie jej nie wolno.

Czyli Klaudia jest w dziwnej sytuacji XD. Ok, ale Sasza częściowo spowolniony. Klaudii się udało. Uff.

Klaudia wróciła do pokoju. Czeka tam na nią Ksenia z dziwną miną i nie widać nigdzie Teresy. Klaudia patrzy na Ksenię. Ksenia pokazuje palcem szafę. Klaudia uniosła brwi. Ksenia pokiwała głową. Ksenia, cicho: "jak tylko przyszłam, ona była schowana w szafie. Jest tam i nic nie mówi. Kazała mi iść spać... nie wiedziałam jak do Teresy podejść."

Klaudia: "Możesz wyjść.". Szafa, jak to szafa, milczy. Klaudia się upewnia że nie ma oczu od terminusa i nikt ich nie śledzi (Ksenia potwierdziła, że jest bezpiecznie) i Klaudia otworzyła szafę. Widzi senną Teresę w połamanej pozycji. "Idź sobie". Klaudia "wyłaź stąd". Teresa - "Nie, dobrze mi tu.". Klaudia zrywa z niej ponczo i rzuca w nią zapasową koszulą nocną.

* "No w tym nie wrócę do pokoju..." - Teresa, rozbudzona. 

Klaudia kazała jej iść do łóżka. Teresa powiedziała, że nie. Spisali ją za PROSTYTUCJĘ (Teresa jest oburzona). Klaudia powiedziała, że reputacja jej nie obchodzi - śpią razem. Ale plecami do siebie. Teresa nie oponowała. Szybko i mocno zasnęła. Klaudia: "Ksenia, nie daj jej wyjść cichcem. Chcę z nią pogadać rano". Ksenia, z podejrzliwością: "Kim jest Teresa Mieralit? Fakty mi się nie zgadzają a Ty wiesz wszystko bo siedzisz w systemach".

Klaudia NIE MA SIŁY ukrywać prawdy przed Ksenią. Wypaliła wszystkie umiejętności oszukiwania i krążenia. Więc Klaudia przestała mówić. Nie chce by była szansa że głos się poniesie. Bierze dronę, klawiaturę i wpisuje tam by nie wyszło poza nich. Klaudia compeluje Ksenię, by to był ich sekret. I wpisuje Ksenii "sierota wojenna. Noktianka". Ksenia ma wielkie oczy. "Czyli ona jest tu tak jak ja byłam w rękach tego serpentisa". Klaudia: "dyrektor próbuje ją chronić." Ksenia: "rodzice? Krewni?" Klaudia nic nie wie...

Ksenia obiecała, że zachowa to dla siebie. Nie chce krzywdy Teresy. Nie boi się jej mimo że jest noktianką. Jest po prostu piętnastolatką z problemami i tyle. Klaudia "dlatego MUSIAŁAM zabrać ją od terminusa". Ksenia "oni nie wiedzą???" Klaudia "nie oficjalnie...". Dopiero wtedy do Ksenii dotarło to, co Klaudia powiedziała...

RANEK

Klaudię obudził łoskot. Ksenia zastawiła pułapkę i Teresa w nią wpadła. Klaudia i Ksenia podskoczyły na łóżkach. Teresa z miną zbolałego psa zaklęła cicho po noktiańsku.

* "Naprawdę?" - Teresa
* "Naprawdę." - Ksenia

Teresa przewróciła oczami. Jest ubrana w stare ciuchy Klaudii, z tych gorszych. I tak są za duże. Widząc minę Klaudii, źle zinterpretowała - "oddałabym Ci". Klaudia stwierdziła, że przeżyje bez nich.

* "I po co się budziłyście? I tak wyjdę, chyba... że... nie." - Teresa, widząc miny dziewczyn.

Klaudia spytała Teresę, czemu wyszła na dach w środku burzy. Teresa udzieliła bardzo logicznej (w swojej głowie) odpowiedzi:

* nikt nie będzie jej szukał podczas burzy na dachu w najbardziej metalowym budynku ever. To jest głupota.
* jest lekko ubrana, bo nie chce zamoczyć i pobrudzić czy popsuć swoich dobrych ubrań. Nie chce, by ktokolwiek wiedział.
* uciekała przed terminusem i jego partnerami. Jednego wprowadziła w pułapkę i spadł ze schodów (to nie był terminus).
* a jak chodzi o to... burza. Błyskawice. Krawędź. Przeżyje lub zginie. Adrenalina. ŻYJE. Teresa ma ogień w oczach. To nie deathwish. To coś innego.

Ksenia rozumie lepiej niż Klaudia, acz Klaudia widzi przerażenie w oczach Ksenii jak Teresa to mówi. "Mogłaś zginąć". "O to chodzi - ale przeżyłam. Cała natura i terminus po jednej stronie, ja po drugiej. I przeżyłam. I przetrwałam.". Klaudia "i wciąż mógł Cię złapać". Teresa "nie wiedziałby, gdyby nie Ty. Nie szukał mnie, nie wiedział o mnie. Przeczekałabym do rana..."

Ksenia ma wątpliwości. Klaudia też. Teresa nie. Klaudia wie, że Teresa lekko szaleje, bo jest sama. Klaudia - "musisz zamieszkać w akademiku. Musimy porozmawiać z dyrektorem". Teresa się przestraszyła. "Nie mogę." Klaudia: "myślę, że to nie jest zły pomysł". Teresa, mroczno "to bardzo zły pomysł". Klaudia "więc nie jesteś w stanie tego wytrzymać? Za duże wyzwanie?" Teresa na to "coś palnę lub będę musiała się socjalizować. A uwierz mi, ja was wszystkich nienawidzę. Prawie wszystkich - poprawiła się".

Klaudia wie jedno. Po tym wyznaniu jakby Ksenia nie wiedziała to by się zorientowała. Teresa nie zsocjalizowana... i jest jej to potrzebne jeśli ma udawać astoriankę. Ksenia do Teresy: "słuchaj, nieważne kim jesteś, jeśli ktoś na Ciebie zapoluje chcesz by inni uczniowie patrzyli na Ciebie jak na jedną z nich. Koleżankę obronią."

Teresa rozłożyła bezradnie ręce. Klaudia powiedziała, że załatwi temat u dyrektora.

ARNULF w domku Arnulfa. I Klaudia. Jest wcześnie rano, koło 6. Arnulf nie śpi - jest dyrektorem. Klaudia, Ksenia i naburmuszona Teresa.

* Panie dyrektorze, poprosimy o pokój trzyosobowy - Klaudia
* Koleżanka jest podejrzana o prostytucję i musimy ją zresocjalizować jako okazy cnót niewieścich - rozchichotana Ksenia
* <Arnulf patrzy z niedowierzaniem na Teresę>
* Musiałam JAKOŚ dorobić na te ciuchy - Teresa, z rezygnacją.
* ... - dyrektor patrzy pustymi oczami.
* Zgadzam się - dyrektor. Zamyka drzwi.

Klaudia weszła do systemu, przydzieliła trzyosobowy pokój dla trójki dziewczyn a ich poprzedni dwuosobowy zarezerwowała. Niech Ksenia i Felicjan mają coś od życia jak ich najdzie ochota ;-). W końcu - she owes Ksenia...

## Streszczenie

W środku nocy Klaudia i Ksenia widzą Teresę w środku burzy na dachu Złomiarium. Poszły ją ściągnąć i natknęły się na terminusa. Ksenia odciągnęła Saszę od Teresy, Klaudia Teresę wysłała do ich pokoju w akademiku (gdzie Teresa spisana za nieletnią prostytucję). Sasza wyjaśnił, że nadmiar lojalności wobec AMZ jest szkodliwy. Klaudia ogromnym wysiłkiem spowolniła Saszę i odwróciła jego uwagę. Potem Klaudia i Ksenia zdecydowały, że zsocjalizują małą noktiankę - i zdobyły trzyosobowy pokój w akademiku mimo protestów Teresy.

## Progresja

* Teresa Mieralit: spisana za podejrzenie prostytucji (SRS!). Podejrzewa ją o to Sasza i pół AMZ po nocnym spacerze w "lekkim stroju".
* Teresa Mieralit: mieszka w akademiku AMZ z Ksenią i Klaudią.
* Ksenia Kirallen: mieszka w akademiku AMZ z Klaudią i Teresą.
* Ksenia Kirallen: ma dostęp do "pokoju schadzek" w akademiku AMZ tylko dla niej (czyli chwilowo też dla Felicjana ;p)
* Klaudia Stryk: mieszka w akademiku AMZ z Teresą i Ksenią.
* Klaudia Stryk: KONFIDENTKA. Dała Saszy informacje o tym jak niektórzy magowie AMZ (uczniowie) robią złe rzeczy wobec ludności. Tamci jej za to bardzo nie lubią.

### Frakcji

* .

## Zasługi

* Klaudia Stryk: wymanewrowała Saszę - wpierw ukryła przed nim Teresę, potem spowolniła jego działania przeciw Tymonowi i odwróciła uwagę od Teresy. Zmusiła Teresę do jakiejś socjalizacji i poprosiła Ksenię, by ta jej w tym pomogła.
* Sasza Morwowiec: terminus chcący wykazać zdradę Arnulfa i Tymona; przeciwny samodzielności AMZ, ma grudge do Tymona i wierzy, że Tymon współpracuje z Grzymościem. Anulował misję, by ratować lunatyczkę w AMZ (to była Teresa i jej nie dorwał). Powiedział Klaudii, że szuka linka Tymon - Grzymość. Wbrew pozorom, całkiem spoko koleś. Nie nienawidzi noktian. Ale żąda porządku.
* Ksenia Kirallen: lojalna przyjaciółka Klaudii; dała się złapać terminusowi by odwrócić uwagę od Teresy na prośbę Klaudii nie wiedząc o co chodzi. Wie, że Teresa jest noktianką i chce jej mimo wszystko pomóc i ją zsocjalizować. Klaudia BARDZO docenia.
* Teresa Mieralit: noktiańska czarodziejka z deathwish?; trochę się boi astorian i trochę ich nienawidzi, więc jest na uboczu. Gdy terminus infiltrował AMZ rozebrała się do bielizny (by nie uszkodzić ubrania) i schowała się na dachu w burzy na Złomiarium dla dreszczyka. Uratowana przez Klaudię, podejrzana o prostytucję i współpracę z Grzymościem (o którym nawet nie wie), skończyła śpiąc na łóżku Klaudii a potem - w pokoju z nią i Ksenią. Całkowicie dzika, niezsocjalizowana.
* Waldemar Grzymość: ojciec Rolanda; boss mafii na terenie Szczelińca. Sasza Morwowiec podejrzewa go o prostytucję nieletnich i współpracę z Orbiterem przez Tymona.
* Arnulf Poważny: przespał infiltrację AMZ przez Saszę. Gdy się obudził, Teresa była podejrzana o prostytucję i Klaudia + Ksenia chciały ją w pokoju. Arnulf bardzo starał się nie łączyć tych zdarzeń i dał im trzyosobowy pokój...

### Frakcji

* .

## Plany

* Klaudia Stryk: wykorzystać Tymona do zdobycia informacji o Grzymościu by móc je zapodać Saszy żeby ten odpieprzył się od Tymona.

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Akademia Magii, kampus: zinfiltrowany przez Saszę z agentami. Teresa się schowała, ale spisała ją straż AMZ.
                                    1. Akademik: Teresa spała w pokoju Klaudii i Ksenii, dziewczyny załatwiły sobie trzyosobowy pokój.
                                    1. Złomiarium: podczas burzy na szczycie znajduje się Teresa Mieralit - phantom of the Academia..?

## Czas

* Opóźnienie: 2
* Dni: 1

## Konflikty

* 1 - Ksenia zatrzymała Klaudię - "ktoś tu jest, widziałam ruch, ona jest gdzieś tu" - szuka Teresy.
    * ExZ+3
    * Ksenia nie znalazła nikogo. Znalazł ją i Klaudię terminus Sasza i słyszał jak wołają Teresę.
* 2 - Ksenia odwraca uwagę terminusa - próbuje wpaść w tarapaty w innym miejscu. I przeraźliwie krzyczy "HELP!".
    * TrZ+3
    * XVz: poturbowała się, ale terminus ją uratował. Odwróciła uwagę.
* 3 - Klaudia krzyczy do Teresy po noktiańsku: "Zejdź do mnie! Niebezpieczeństwo!" Kiepski noktiański, ale zawsze.
    * TrZ+3
    * XV: Terminus podejrzewa, że dziewczyny COŚ planują, ale Teresy nie złapał. Teresa dotarła do Klaudii.
* 4 - Teresa mówi, że nie dostanie się do Arnulfa. Klaudia powiedziała - idź do akademika. Jak ktoś ją zapyta, Teresa lunatykowała.
    * TrZ+2
    * XzXV: Teresa napotkała ochronę AMZ; nie uwierzyli jej, uznali ją za nieletnią prostytuującą się czarodziejkę XD. Ale dotarła do AMZ.
* 5 - Klaudia próbuje okłamać Saszę. Klaudia nie jest w tym dobra, ale Sasza nie spodziewa się prób kłamania. Klaudia próbuje siebie i Ksenię wyplątać...
    * Tr (niepełna) Z (zaufanie do Klaudii) +3:
    * XXzVzX: Sasza przyjrzy się Teresie i wie, że dziewczyny jej pomagały (bo lojalność AMZ). Odpuszcza Ksenii i Klaudii. Spławił Klaudię.
* 6 - WSTECZNY KONFLIKT: co Klaudia wie o Saszy? 
    * ExZ (uprawnienia) +3 (wcześniejsze badania + Tymon):
    * XzVz: Sasza i inni terminusi wiedzą, że Klaudia o nich szukała. Klaudia zebrała informacje o Saszy jakie chciała zebrać.
* 7 - Klaudia mówi Saszy, że to ona organizuje większość współpracy między terminusami i AMZ. Widzi więcej, ma większe dostępy. Może pomóc. Ale musi wiedzieć w czym.
    * Tr(niepełny)Z+3+3O (Klaudia przekierowuje na innych z grzeszkami)
    * VXVzXV: Klaudia zrzuciła z siebie podejrzenia Saszy, została konfidentką, Sasza powiedział Klaudii o co chodzi, ZAKAZAŁ jej działania przeciw mafii ale zaakceptował pasywne zbieranie informacji.

## Projekt sesji

* Overarching Theme:
    * Theme & Feel: 
        * .
    * V&P:
        * .
    * adwersariat: 
        * .
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * Role i postacie
        * Olaf Zuchwały -> chce być łącznikiem między Miasteczkiem + Kryształowym Smokiem a Astorią.
        * Ksenia & Felicjan -> chcą normalizacji Noctis - Astoria.
        * Sasza & Protazy -> chcą ujawnić prawdę o Strażniczce i zdradzie Tymona i Arnulfa
        * Ralena Drewniak -> chce zniszczyć wszystkich noktian. Nie może się to tak skończyć. Terminuska.
        * Mariusz Trzewń -> bardzo antynoktiański; napadli i jeszcze to wszystko.
        * Teresa Mieralit -> na krawędzi dachu, na granicy śmierci podczas burzy.
* Co się działo w przeszłości
    * 
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * Sasza dojdzie do tego że Strażniczka jest anomalna.
    * Ksenia i Felicjan --> zdrajcy.

Dane projektowe:

* "27 dni po poprzedniej"
* Ksenia -> Klaudia, „Zamek Weteranów dla noktian”
* problem przekonania że tymi weteranami też trzeba się zająć.
* Petycje, działania, Ksenia na froncie a na banku Klaudia
* Duże wsparcie Felicjana - jeśli im nie pomożemy, w czym jesteśmy lepsi?
* Strażniczka Alair jest niestabilna - za mało energii, za dużo żądamy, pobiera Skażoną energię… Klaudia pełni rolę ukrywacza gdy Tymon i Talia próbują jakoś to skorygować.
* Felicjan -> Klaudia: niech Augustino dostanie jakieś podstawowe miejsce i sprzęty w okolicach Lasu. Sam tam zginie. A próbuje pomagać. 
* KONIEC SESJI: podpalenie domu noktiańskich weteranów. Ksenia wyciąga weteranów, nie osoby które podpaliły. Felicjan jej pomaga. 
* Oddział Kryształowy Smok (noktianski) w okolicy Pacyfiki. Zajęli ten teren. Pustogor nic nie robi - nie ma na to sił.
* Tymon ma problemy z lokalnym terminusem, Saszą Morwowcem. Sasza uważa, że Tymon ma swoją agendę i ogólnie nie działa na korzyść Pustogoru. Sasza robi query. Klaudia to zauważa.
* Ogólna niechęć astorian do noktian. Próby linczu (Felicjan x Ksenia świadkami, oni próbują temu zapobiec i zostają pobici).

Sceny:

* Teresa podczas burzy, w nocy. Terminus szczurzy w AMZ, ona się schowała. Zero strachu XD.
* Ksenia chce by Klaudia jej pomogła napisać petycję o Dom Noktiańskich Weteranów.
* Sasza dopytuje Klaudię o dziwne rzeczy które się tu dzieją. Bo jej ufa. Jeszcze.
* Strażniczka jest niestabilna. Drony atakują studentów (Arnulf przedstawia to jako ćwiczenia ). 
    * M.in. Polują na Teresę Mieralit. 15-letnią czarodziejkę mieszkającą chyba na terenie akademii.
* Problemy z integracją Strażniczki i jej komponentów. A z drugiej strony węszący terminus Sasza wraz z ichnim psychotronikiem, Protazym.
* Ralena chce zniszczenia Domu Weteranów Noctis, podburza ludzi.

Dramatis personae (na potem):

* Protazy Tukan: psychotronik lojalny Saszy; 
* Felicjan Szarak: pragnie normalizacji Noctis - Astoria; 
* Ralena Drewniak: terminuska z nienawiścią ku Noctis;
* Ksenia Kirallen: pragnie Domu Weteranów dla Noctis; 
