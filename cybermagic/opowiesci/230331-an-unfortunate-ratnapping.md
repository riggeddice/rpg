## Metadane

* title: "An Unfortunate Ratnapping"
* threads: magiczne-szczury-podwierckie
* motives: nuisance-pests, wyscig-o-zasoby, naprawa-wizerunku-grupy
* gm: żółw
* players: kić, BloodyWind, HelenDeer

## Kontynuacja
### Kampanijna

* [230303 - The goose from hell](230303-the-goose-from-hell)
* [230325 - Ten nawiedzany i ta ukryta](230325-ten-nawiedzany-i-ta-ukryta)

### Chronologiczna

* [230303 - The goose from hell](230303-the-goose-from-hell)

## Plan sesji
### Theme & Vision

* .

### Co się stało (what happened?)

VISION level, by modified chat-gpt:

Podwiert, a relatively peaceful industrial city, was taken aback when the magical rats first appeared. The serene atmosphere was disrupted as the rats began to establish their nests in various hidden locations around the city, such as the abandoned Schron TRZ-17 in Las Trzęsawny, and Magazyny sprzętu ciężkiego. Initially, these creatures were merely an oddity, catching the attention of a few observant citizens who noticed their peculiar magical abilities.

As the rat population grew, they became a nuisance to the general population. These magical rodents caused minor disruptions to daily life, especially around the bustling areas like Osiedle Leszczynowe and Osiedle Rdzawych Dębów, by causing electrical disturbances and occasionally using their limited magical powers to create confusion and mischief. The people of Podwiert found themselves struggling with the unexpected consequences of the rats' presence.

Unbeknownst to most, Ozitek Industries, a once-renowned company located in the Kompleks Korporacyjny, stumbled upon a groundbreaking discovery. While investigating the rats' peculiar abilities in an effort to eliminate the pesky creatures, the company's researchers found that the life force of these magical rodents could be harnessed as an incredibly efficient energy source. This revelation had the potential to revolutionize their battery production, giving Ozitek Industries the advantage they desperately sought.

Recognizing the opportunity at hand, Ozitek Industries discreetly reached out to the Guardians of Harmony, a respected mercenary security company known for their expertise in combating supernatural threats. The company offered a lucrative contract to the Guardians, asking them to capture and deliver the magical rats to their facility. The Guardians, unaware of Ozitek Industries' true intentions, agreed to assist in what they believed was a noble cause: protecting the people of Podwiert from the troublesome rats.

As the city continued to grapple with the growing rat problem, the magical school in Podwiert, located in Osiedle Leszczynowe, was approached by local authorities from the Komenda policji, requesting their help in addressing the issue. The school administrators, recognizing the potential danger posed by the rats, decided to dispatch two of their brightest young mages, Alex and Carmen, to investigate the situation and find a solution to the rat problem.

### Co się stanie (what will happen)

* S0: 
* S1: 
* SA: 
* SB: 

### Sukces graczy (when you win)

* 

## Sesja właściwa

### Fiszki
#### 1. AMZ

* Paweł Szprotka: weterynarz
    * (ENCAO:  -++0+ |Skłonny do refleksji;; Skryty;; Trwożliwy | VALS: Benevolence, Universalism, Self-direction >> Achievement| DRIVE: Utopia (zwierzaki))
* Teresa Mieralit: nauczycielka magii leczniczej i katalistka (disruptorka magii) w Szkole Magów
    * (ENCAO:  +0+-+ |Manipulatorka i meddler;; Kreatywna, tworząca wiecznie coś nowego| VALS: Benevolence, Achievement >> Face, Security| DRIVE: AMZ będzie najskuteczniejsze)

### Scena Zero - impl

.

### Sesja Właściwa - impl

You find yourself in city of Podwiert. An industrial city. Carmen, Alex, Julia and one Tobias (cat). You were sent by the school to hunt rats. SOMEONE had a Paradox and there are magical rats in the city. Which kind of sucks. And we don't want problems with people so we need to solve it.

1. because it 100% had to be Paweł -> it wasnt him.
2. because someone had to and you were running the slowest.
3. because last time you succeeded.
4. because you have a cat. And that qualifies you to be monster hunters.

You know there are rats here. They can drain electricity. Rats aren't the largest and most dangerous ones. They are a nuisance but not a danger.

In order to solve rat problem, what do we need (and how can we get it):

1. where are the rats?
    1. we can use a super-successful hunting cat named Tobias
2. lure -> get rats to a specific place
    1. we need to know how to lure those rats
        1. test. A single rat.
3. trap (not kill even if easier but we have Julia for SOMETHING)
    1. to make a trap, we need to test. A single rat.
4. THEN WE BRING RATS TO ETHICS TEACHER :D:D:D:D

You have the whole city. There are rats draining electricity. How to find out where the rats are? -> look where the electricity is lower. Or an environment where a rat can thrive.

Carmen starts correlating map of electricity outages and fluctuations on map of the Podwiert city using the governor AIs.

Tr +3:

* X: there is a rumor that it is Academy's students who _made the rats_. People don't love you.
* Vr: Carmen has a map of electric outages, there is one particular place - warehouses.
    * Magazyny sprzętu ciężkiego
* Vr: Carmen sees that there is at least one more nest. But that nest is outside normal area of the city. Warehouses are the core nest in the Podwiert, other rats need to come from outside
* X: the helpful governor AI told Carmen: 
    * that there is another faction which request the same information. Someone is trying to seek rats. This is a mercenary company focusing mostly on dealing magical stuff and anomalies. 
    * since 10 days the speed of electricity draining has slowed, even dropped. Someone is reducing the amount of rats. Maps on this company. "Guardians of Harmony".
    * they don't like mages.

So we have a location. Warehouses. And we didn't even have to talk to people ^__^.

Julia makes a rat trap. The "completely not harmful one which only contains them" trap type. As a lure - some food and some electricity. Paweł is consulted, school library is researched.

Tr Z (Paweł) +2:

* XX: the trap does not keep those rats. There needs to be a different way.
* X: Julia has no way to trap a first rat with anything mechanical. Somehow.

Plan B. We are using a cat. Alex is sneaking a cat upon warehouses and the cat shall capture a single rat.

Tr Z (Tobias is a Pacifier type of cat) +2 +3Or:

* V: Tobias has been sneaked in and out
* Or: Tobias has killed a set of rats
* Vz: After a while Tobias is satiated with killing and decided to bring one to you
* V: Alex managed to evacuate himself and Tobias and one rat out of that place without being seen.

Upon seeing a rat, Julia knew what was wrong. The rat has also some... magnetic properties which interfered with the normal types of traps she tried. Stupid rats. Now she redesign a trap. A glass 'bucket' to capture rats. With lure inside. A variant on a pigeon trap ( https://www.videoman.gr/en/143547 ).

Julia and Paweł are designing a New Better Rat Trap. Paweł with a lure, Julia... with a pigeon trap variant. And to make it large-ish. THERE ARE RATS TO CATCH.

Tr Z (Paweł) +4:

* V: the trap will work, however is large, cumbersome and unwieldy
* X: the other party is aware there are MORE rat hunters.
* Vz: perfect lure made it so the other party's rats go towards this rat trap and the general population is like "oh, so Academy is doing _something_ about those rats".
    * this will make a _trend_ - this will solve the rat nest in the warehouse.

While you were doing trap stuff, the second day, you were approached by a guy. And he approached you.

* R: "So you decided to come and solve the problem you created. Will you capture all of them?"
* A: "What can I say to a guy who uses railgun to hunt a rat?"

Radosław, a leader of the Guardians of Harmony cell here, asked if he can rely on the mages to solve the rat problems. He is unpleasant and hostile, got met with the same level of hostility. Getting an answer "we are not responsible for the rats so we cant assure no rats" he decided to go and do his thing.

Our current strategy - go over the top. Do MORE. And investigate (this can be outsourced to faculty, but they will need many rats to extract _magical signature_).

* Vr: the trap has managed to capture _enough_ rats for the magical signature extraction.

So basically, this nest has been mostly solved. When the nest is cleaned, Julia leaves a note "yes we TOOK CARE OF IT". Because someone will come and check.

Tobias isn't here. It is somewhere closer to one of the buildings. Tobias is not in distress. It is a playful cat...

Tobias is located in a truck belonging to the Guardians. It is hidden inside. There is a completely uninterested driver and yeah, doesn't look like a catnapping.

Carmen makes a rubbish bin fly with telekinesis to completely distract the driver (and THEN Alex will sneak on the truck to acquire his cat >.> ).

Tp Z (unexpected) M +2 +3Ob:

* Vz: driver is distracted. He was driving slowly and 3 rubbish bins float in front of his truck. 
    * HE SLAMS THE BRAKES! "RATS HAVE SPECIAL QUALITIES! EPHEMERAL! DANGER! NEED REINFORCEMENTS!"

Alex sneaks on a truck to get a cat:

Tr Z (full distraction) +3:

* V: easy sneak.

In the truck, one Tobias playing with a set of rats in a cage and this truck is full of rat cages with alive rats. Alex wants to steal those rats. So he sneaks behind to disable a guy and for Carmen to RATNAP THE CAGES!

* V: the guy from Guardians got approached from behind by Alex, he is distracted by Carmen
* X: the guy was surprised but wasn't completely surprised. Struggle.
* X: this is a fight. Alex breaks his arm in the process, and gets some light hits.
* X: the guy managed to beat Alex (like boxing match) and he has seen his face.
* (magic: -> Tp, +3Ob)
    * PLAN: rat cage is selected, marked, dropped on a guy and THEN gets tele-hit by materializing Alex.
    * V: plan was a success.

Bloodied Alex, it is hard to walk, one innocent cat, one truck and LOTS of rat cages AND reinforcements coming.

Carmen takes the rat cages, unconscious guy and telekinetically tries to move them _somewhere else_. And rat nest gets his radio so he is un-locateable.

Tr M +3 +3Ob +5Og (representing prying eyes):

* V: Carmen managed to transport it into the closest warehouse which doesn't have people
* V: people didn't see it XD.
    * when Ephemeral alert was run, people evacuated the place
    * the guy called Ephemeral and then got hit by Alex after 1 min
    * it was all hidden in a truck, so...
* Vm: cages -> sled with no friction, WHEEE!!! into the forest

THEN reinforcements came but the Team is far away from here, in the forest XD.

Carmen, in the forest, clears memory of the Alex - guy fight and implants a memory that there was an ephemeral, Alex threw himself to save the guy, rats escaped (because ofc they did) and mages managed to save the day.

Tr M Z (plausibility, the guy would NOT expect Alex to attack him and esp. overpowering him) +3 +3Ob:

* Xm: The guy and a RANDOM RAT swapped personalities and souls.
    * rat doesn't speak.


## Streszczenie

In the city of Podwiert, magical rats cause chaos by draining electricity. Alex, Carmen, Julia, and their cat Tobias are sent by their magical school to solve the problem. They identify the rat nest in the warehouses and learn about the involvement of a mercenary company, the Guardians of Harmony, who have been secretly reducing the rat population.

Using a specially designed trap, the team captures the rats and encounters Radosław, the leader of the local Guardians of Harmony cell, who confronts them. They deny responsibility for the rat problem and continue their mission.

When Tobias goes missing, they find him in a Guardians' truck filled with caged rats. After a scuffle, Alex and Carmen steal the cages and accidentally swap Guardian's soul with that of a random rat. Earlier on, they had one problem. Now they have two.

## Progresja

* .

## Zasługi

* Julia Kardolin: skilled in creating traps, she designs a trap to catch the magical rats and singlehandedly solves the main rat nest in the warehouses.
* Alex Deverien: skilled in stealth and combat, who helps capture rats, confronts the Guardians of Harmony, and retrieves his cat Tobias after a scuffle with a member of Guardians of Harmony.
* Carmen Deverien: heavily uses telekinetic abilities; she helps locate the rat nest with social media, transports the captured rats via telekinesis, and accidentally replaces the defeated Guardian's soul with a random rat's one.
* Paweł Szprotka: assists Julia in designing the perfect rat trap and lure. Not really present; more of advisory role.
* Radosław Turkamenin: Straż Harmonii; an officer of Guardians' cell, who confronts the team about the rat problem; extremely paranoid about the mages.
* kot-pacyfikator Tobias: mischevious one, helps capture the rats and plays a crucial role in the story when it is found in the Guardians' truck (where he sneaked in to hunt some rats).

## Frakcje

* Akademia Magiczna Zaczęstwa: Carmen, Alex i Julia skupili się na łapaniu i usuwaniu magicznych podwierckich szczurów. Przy okazji wplątali się w solidny konflikt ze Strażą Harmonii. Teraz to pewne, że to AMZ ma coś wspólnego z tymi szczurami XD.
* Straż Harmonii Szczelińca: Tym razem polują na magiczne szczury w Podwiercie. Podejrzewają (plotki), że za tym stoi AMZ. Stracili ciężarówkę i jedną osobę - plus cały transport magicznych szczurów.

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert: magical rats cause problems by draining electricity in the city
                                1. Magazyny Sprzętu Ciężkiego: core rat nest in Podwiert using a map of electrical outages. The team captures rats here and later confronts the Guardians of Harmony
                                1. Las Trzęsawny: After capturing the rats from the Guardians' truck, Carmen transports the cages, unconscious Guardian, and her teammates to a forest for safety. Close to Warehouses.

## Czas

* Opóźnienie: 9
* Dni: 2

## Inne

.