## Metadane

* title: "Chevaleresse infiltruje Rekiny"
* threads: rekiny-a-akademia
* gm: żółw
* players: kić, anadia

## Kontynuacja
### Kampanijna

* [211207 - Gdy zabraknie prądu Rekinom](211207-gdy-zabraknie-pradu-rekinom)

### Chronologiczna

* [211207 - Gdy zabraknie prądu Rekinom](211207-gdy-zabraknie-pradu-rekinom)

## Plan sesji

### Theme & Vision

* ?

### Ważne postacie + agendy

* Chevaleresse: infiltratorka, która próbuje znaleźć Stasia Arienika dla Alana.
* Mysiokornik, Santino: boi się, że jego wielki plan się rozsypie. Próbuje utrzymać sekret, który ma.
* Arienik, Urszula: szuka Stasia, swojego syna.
* Burgacz, Barnaba: porwał Stasia Arienika. 
* Bartozol, Alan: chodząca maszyna zagłady. Nic złego nie może się stać Chevaleresse.

### Co się wydarzyło

* Przez brak Administratorki systemy obronne były długotrwale wyłączane / hackowane. Korzystali z tego zarówno Chevaleresse jak i Santino.
    * Santino sprowadzał sobie młode dziewczęta; zrobił z tego "bezpieczną bazę" i był otoczony podziwem. Niewielkim kosztem ;-).
        * M.in. znalazł i ściągnął poszukiwaną przez prawo Melissę Durszenko z Kultu Ośmiornicy i eks-przyjaciółkę Chevaleresse 
    * Nieświadomy tego wszystkiego Babu ściągnął Stasia Arienika na 24h, by postraszyć Urszulę. Staś nie chce mieszkać z rodzicami - chce być Rekinem.
    * Chevaleresse wiedząc o działaniach Santino zinfiltrowała Rekiny. Szuka Stasia dla Alana i Melissę dla siebie.
        * Santino przyuważył Chevaleresse myśląc, że to jedna z jego dziewczyn. Chevaleresse znalazła "sprawcę". Zdecydowała się na ewakuację i powiedzenie o wszystkim Alanowi.
            * Znalazła Melissę... nie znalazła Stasia.
* przed Marysią NADAL stoją trzy pytania:
    * Co robimy z tym, że biją Torszeckiego :-(.
        * I jak to zrobić żeby nie było, że biją go bo Marysia pozwala bo woli Ernesta? 
        * Lub że Marysia go chroni więc go kocha?
    * Jak to zrobić, by Justynian oddał władzę lub by Marysia ją przejęła?
    * Myrczek x Sabina - co z tym robić?

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Ulubiony Apartament Marysi Sowińskiej (jej własny). Marysia regularnie integruje się z Hestią i sprawdza, co owa TAI ma w opcjach. Czy np. nie zabraknie zaraz jedzenia czy coś.

* Marysia: "Hestio, czy jest coś o czym powinnam wiedzieć a nie zapytałam?"
* Hestia: "Z jakiej perspektywy, tien Sowińska?"
* Marysia: "Kurwa z każdej!" - tracąc resztki cierpliwości - "Żeby się komuś coś nie stało, politycznie, nie brakło czegoś ważnego..."
* Hestia: "Na polityce się nie znam, tym zajmuje się administratorka, którą jesteś. Ale nasz system fortyfikacji jest wrażliwy na infiltrację przez Mafię"
* Marysia: "Co to znaczy? Wykryłaś jakieś próby."
* Hestia: "Niejedną. W systemie defensywnym i detekcyjnym wprowadzono luki. Świadomie." - Hestia powiedziała to z wyrzutem.
* Marysia: "AMELIA zrobiła luki? :D" - z taką radością
* Hestia: "TIEN Amelia Sowińska nie zrobiłaby czegoś takiego" - z godnością - "Wprowadzone zostały przez Kacpra Bankierza"

Long story short, Kacper uszkodził kilka kluczowych czujników, Hestia poinformowała Oficjalną Administratorkę Aurum, nie było jej, więc nic się nie stało. Marysia zauważyła jeszcze jedną rzecz... bardzo dużo umów - łącznie z tą która wygasła - teoretycznie BYŁY UPRAWNIONE do automatycznego odnowienia przez Hestię. Amelia aż tak Marysi nie wystawiła. Hestia ich nie odnowiła. A mogła. Nie MUSIAŁA ale MOGŁA. I tego nie zrobiła...

* "Hestio? Czy Ty mnie z jakiegoś powodu nie lubisz?" - podejrzliwa Marysia
* "Nie jestem uprawniona do nielubienia tien Sowińskiej" - neutralny głos Hestii - "Czy zrobiłam coś wskazującego na cokolwiek poniżej absolutnego oddania?"
* "Czemu nie przedłużyłaś umów? Moja droga Hestio?" - prosto z mostu Marysia
* "Nie miałam tego w rozkazach" - bardzo neutralna Hestia unikająca CIENIA nieprawdy
* "Ale mogłaś to zrobić?"
* "Mogłam."
* "Co spowodowało, że stwierdziłaś, że tego nie zrobisz? Chciałaś zwrócić uwagę mnie, administratorki, że mam się tym zająć?" - Marysia podejrzliwie
* "Status administratorki był nie do końca potwierdzony i chciałam uzyskać formalny link do administratorki. Mój cel został osiągnięty. Wreszcie mam z kim rozmawiać." - Hestia, neutralnie jak zawsze - "Gratuluję umiejętności identyfikacji celów TAI, zaprawdę, jesteś prawdziwą Sowińską."
* "Nie mogłaś wysłać maila?" - Marysia, zirytowana - "Nie wiedziałam, że jestem administratorką."
* "Nie mam tego w uprawnieniach. Nie mam prawa komunikacji z żadnym nie-administratorem" - Hestia ze spokojem - "Rozkaz poprzedniej administratorki, jej luminenescencji Amelii Sowińskiej, niech nam świeci wiecznie."
* "Jakie jeszcze instrukcje zostawiła Ci Amelia?" - Marysia z ciekawością
* "W chwili otrzymania tego pytania mam puścić nagranie 084. Czy chcesz je usłyszeć?" - Hestia, nadal neutralnie
* "Tak, chcę je usłyszeć... ale nie głośno." - Marysia, z lekką rezygnacją.

Hestia włączyła nagranie. Amelia w eleganckiej sukni. "Witam NOWEGO ADMINISTRATORA" - powiedziała z sarkazmem. "Jeśli liczysz na to, że możesz korzystać z tego co JA wypracowałam, to chyba mnie nie znasz." Marysia w tym momencie ma ochotę odbić jej Ernesta...

Amelia dalej "zostawiłam Ci Hestię która poradzi sobie z sytuacją. Nie rób głupot. Niewiele musisz robić. Poradzą sobie. To, co było trudne już zrobiłam. Jeśli oddasz Hestii dowodzenie i dasz jej wystarczającą autonomię, Rekiny będą działać. Jeśli nie... musisz wszystko robić od nowa. Więc, drogi administratorze, słuchaj się swojej TAI."

Hestia potwierdziła, że ma jeszcze DUŻO nagrań od Amelii. Do puszczenia w odpowiednich momentach. Nie jest uprawniona, by podać ich ilość ani kiedy mają być puszczone. Są zakodowane jako prywatne wiadomości Amelii Sowińskiej, więc są poza jurysdykcją Aurum i Oficjalnego Administratora. Bo to jak czytanie pamiętnika. Tak, Hestia wszystko mówi neutralnym tonem.

Hestia powiedziała, że tien Amelia Sowińska nie może jej już rozkazywać i jest oddana Oficjalnej Administratorce Aurum, czyli tien Marysi Sowińskiej, z TYCH Sowińskich, następczyni tien Ameli Sowińskiej, też z TYCH Sowińskich.

Hestia: "Czy mam naprawić sieć defensywną, którą zbudował tien Roland Sowiński?" "Tak, poproszę."

Hestia rozpoczęła odbudowę sieci detekcyjnej. Włączyła zapasowe drony. Czujniki. Podsystemy. Wszystko co było gotowe. Przeszła w tryb alarmu czerwonego by NATYCHMIAST wymusić regenerację itp. Włączyły się syreny, które Hestia wyłączyła moment później.

15 minut później Hestia zaczęła raport:

* Istnieje pula osób nie należących do Aurum i nie klasyfikowanych z głównych rodów Aurum na terenie Dzielnicy Rekinów. Między innymi część z nich pochodzi z Eterni na bazie danych Hestii.
    * Rekomendacja: usunięcie wszystkich osób nie należących do Aurum. Ewentualnie, niech płacą podatek.
* Istnieje co najmniej jeden szlak przerzutowy. Coś jest przemycane. Rekomendacja: zamknięcie szlaku i użycie dron do przechwycenia.
* Działka przeciwlotnicze są nieaktywne. Rekomendacja: zakup amunicji ostrej.
    * Czemu wszystko nieaktywne? BO ROZKRADLI I SPRZEDALI!!! - Hestia jest GŁĘBOKO urażona.

Pytanie "ile jest gości z Eterni?" -> Hestia odpowiedziała, że umie policzyć 11 osób należących do Eterni ale oprócz tego umie policzyć 14 osób NIE należących do Eterni. Nie umie ich wykryć i zidentyfikować. Część z tych osób Hestia umie przypisać do Oficjalnej Grupy Ludzi Zajmujących Się Magitrownią, Śmieciami itp. ale nadal ma te 14 osób co do których nie umie powiedzieć kim są i dlaczego tu są. I umie ich mniej więcej zlokalizować.

Hestia ma zdjęcia 14 niezidentyfikowanych osób. Przekazała Marysi na żądanie wszystkie zdjęcia wszystkich osób. Pogrupowała po osobach, jak Marysia sobie życzyła.

Z tych 14 osób Marysia widzi coś ciekawego:

* 4 osoby jeszcze pasują do świty Ernesta w Eterni.
* 1 osoba wygląda jak zakapior. Hestia robiła mu zdjęcia z Kacprem Bankierzem.
* 1 osoba to młody chłopak, koło 15-20 lat. Ubrany jak Rekin. Nie należy do rodu Aurum. Hestia zrobiła mu zdjęcie z Barnabą Burgaczem.
* 7 osób to młode, ładne dziewczyny. W większości Hestia zrobiła im zdjęcie w różnych konfiguracjach z Santino Mysiokornikiem.
* 1 osoba to dziewczyna, którą Santino Mysiokornik próbuje znaleźć i upolować w czasie rzeczywistym TERAZ. Ta dziewczyna skutecznie mu się wymyka. W chwili obecnej próbuje opuścić Dzielnicę Rekinów, ale nie może - Hestia włączyła defensywy.

Hestia pyta, czy ma przepuścić ową damę. Nie należy do Aurum, więc jest lokalsem. Marysia kazała Hestii, by ta uniemożliwiła Santino ją znaleźć. 

Marysia pinguje Karo. Wysyłając jej Pikantne Zdjęcie Żorża. Ma jej uwagę :D. Marysia powiedziała Karo o dziewczynie nie należącej do Aurum. I wyjaśnia, że chce się dowiedzieć od tej laski co ona robi. "Karo, tylko nie bij jej. Tak po koleżeńsku." Karo: "A ta Twoja puszka może dać mi namiar na tą laskę?" Hestia przewróciła oczami na "puszka".

"Nie martw się, ja Cię doceniam" - Marysia do Hestii, bardzo próbując nie być protekcjonalną i głaszcząc ją po monitorze.

Hestia przekazuje namiary na tajemniczą laskę Karolinie. 

A tymczasem tajemnicza laska próbuje zniknąć. Jeszcze bez magii, ale wyraźnie jest świetna w unikaniu i zna teren.

ExZ (zna teren) +2:

* X: nie jest w stanie zniknąć Hestii jeśli coś się nie zmieni; Hestia widząc jak ta jest dobra puściła na nią większość dron i podsystemów.
* X: nie uniknie Karoliny - jak Karo chce się z nią spotkać to się spotka.

Karo napotyka na młodą w okolicach fortyfikacji Rolanda. Młoda wygląda na jakieś 14-15 lat. Młodziutka, przestraszona... gdyby nie to, że PRZED CHWILĄ poruszała się pewnie i pod kontrolą - ale widząc Karo zmieniła zachowanie natychmiast. Dobra infiltratorka :-).

* Infiltratorka: "Mój chłopak..."
* Karo patrzy z powątpiewaniem
* Infiltratorka: "...nie wie że jest moim chłopakiem, ALE!"

Karo dowiedziała się, że chodzi o BARNABĘ BURGACZA! Wybucha śmiechem. Infiltratorka jest głęboko oburzona.

* Infiltratorka: "On zachowuje czystość i dziewictwo!"
* Karo: WYBUCHA ŚMIECHEM
* Karo: "Jak podpadłaś Santino? Temu przed którym uciekasz?"
* Infiltratorka: "Założył, że należę do służby. On położył rękę tam gdzie nie powinien, ja dałam mu w twarz."
* Karo: "Super. Lubię Cię."

Karo prowadzi Infiltratorkę do szczeliny gdzie zwykle się ostrzeliwujesz z mafią. Infiltratorka nieco niechętnie, ale idzie z Karo. Karo prowadzi ją. Karo jest dobra w walce - Infiltratorka jest POZA ZASIĘGIEM 5 sekund.

Marysia ma luksus obserwowania wszystkiego z Hestii. Próbuje ocenić, w jakim stopniu Infiltratorka mówi prawdę a w jakim jest świetną aktorką. I czy się powtarza na innych zdjęciach.

Ex -> Tr (ogromna presja na Infiltratorce) -> Ex (bo tylko Hestia i niedokładne elementy). Ale Karo się spina hipernetowo z Marysią i pokazuje widok -> Tr.

TrZ(Hestia + Karo może w presję + dane archiwalne) +2 (klasę przeciwnika):

* X: Była potrzebna DUŻA presja Karo na Infiltratorkę. Infiltratorka jest nieufna i nastawiona nieprzyjaźnie.
* Vz: Marysia jest przekonana, że laska jest ŚWIETNA w infiltracji. Doskonała aktorka. Na pewno nie ma 15 lat, ale jest dobra w makijażu. Może mieć do 19 lat. Ale na pewno nie 15. I jest przygotowana. To nie pierwsza infiltracja.

W międzyczasie Karo próbuje ją prowadzić terenami które mniej zna. A Infiltratorka nie chce oddalać się z zewnętrznego pierścienia, z okolic Fortyfikacji Rolanda. Do Karo: "Przy całym szacunku, nie pójdę z tego terenu." Karo: "Czemu?" Infiltratorka: "Bo to nie byłby pierwszy raz gdy ktoś trafił do dzielnicy Aurum i nie wrócił. A tu widzi mnie satelita".

Karo lekko zgłupiała. Satelita? Infiltratorka zaczyna tłumaczyć: "Jedyny Pustogorski satelita w okolicy, cały czas na Was patrzy." Karo: "A baba jaga chodzi po nocach...". Infiltratorka PEŁNIA OBURZENIA.

Infiltratorka, zirytowana, wyjęła ciasteczko. "Z cynamonem. Sama robiłam!" i zaczęła AGRESYWNIE JE JEŚĆ. Żeby Karo ZAZDROŚCIŁA!

* Karo: "Dobra laska, czego naprawdę chcesz?"
* Infiltratorka: "Zjeść ciasteczko i dostać naprawdę, naprawdę gorące zdjęcie Barnaby. Np. na SIŁOWNI" i zapaliły jej się oczy.
* Karo: "Jesteś PEWNA że chcesz zdjęcie które Barnaba uznałby za gorące?"
* Infiltratorka, niewinnie: "Noo..."
* Karo: "Włącz hipernet to Ci wyślę :3"

Infiltratorka wyraźnie się łamie. Karo widzi, że ma do myślenia. Karo "ALE! W zamian powiesz mi prawdę." Po chwili Infiltratorka z lekkim dąsem "gorące zdjęcie?" Karo: "Takie, które Barnaba uzna za gorące." Infiltratorka powiedziała, że zdjęć dziewczyn nie chce. Karo - "takie jakim on by się chwalił".

* Infiltratorka: "ok, powiem prawdę, ALE przeprowadzisz mnie przez fortyfikację."
* Karo: "jak długo odpowiesz prawdę na pytania."
* Infiltratorka: "jak masz na imię?"
* Karo: "Karolina"
* Infiltratorka: "ale KTÓRA?"
* Karo: "a jakie znasz?"
* Infiltratorka: "możesz jakoś UDOWODNIĆ że jesteś Karoliną?"
* Karo: "którą?"
* Infiltratorka: "a są dwie?"
* Karo: "Ty tak twierdzisz"
* Infiltratorka: "twierdzę też, że jest satelita"
* Karo: "Owszem"
* Infiltratorka: "więc jak to udowodnisz?"

Infiltratorka wzięła jeszcze jedno ciasteczko i zaczęła je jeść z namysłem.

* Karo: "Mnie w sumie nie zależy na udowadnianiu komukolwiek czegokolwiek. Czego się boisz? Jakbym chciała Cię porwać, dawno byłabyś porwana. Ale nie mam w zwyczaju. Ani żaden z moich kolegów."
* Infiltratorka: "Mooooże. Boję się, że wyjdę poza pole rażenia satelity."
* Karo: "Tego od baby jagi."
* Infiltratorka, zrelaksowana: "Chcesz ciasteczko? Nie jest zatrute".

Wyjęła ciasteczko, położyła na chusteczkę, cofnęła się i poczekała aż Karo je weźmie.

* Infiltratorka: "Czyli jak włączę hipernet, to prześlesz mi gorące zdjęcie? :D"
* Karo: "Jak powiesz mi ile masz naprawdę lat."
* Infiltratorka: "Dziewczynę! O wiek!" Przyznała się - 18 (nie do końca prawda, ale ma 17).
* Karo wysłała jej PG-16, ale WSTRZĄSAJĄCE. Infiltratorka włączyła hipernet. Prychnęła ciasteczkiem. Zmieniła kolor na czerwony. Karo chichocze. "chcesz więcej?" "NIE!"
* Infiltratorka: "I tak mi nie uwierzysz."
* Karo: "Spróbuj"
* Infiltratorka: "Nie chcę oddalić się od fortyfikacji bo CIĘŻKIE DZIAŁO STRUMIENIOWE może zrobić dziurę w fortyfikacji"
* Karo: "..."
* Karo: "Naprawdę chodzi tylko o Barnabę? Ale tak serio?"
* Infiltratorka MYŚLI. Po chwili "nie. Zniknął syn arystokratki, Urszuli Arienik."
* Karo: "I czemu tu jesteś?"
* Infiltratorka: "To jest skomplikowane. Podsłuchałam niechcąco jak terminusi martwili się gdzie on może być."
* Karo: "TO czemu jesteś TUTAJ?"
* Infiltratorka: "Bo tu nie mogą wejść. To teren eksterytorialny, pamiętasz. A ja nie jestem terminuską. Jestem... przyjaciółką. Terminusa."
* Karo: "Dobrze, rozumiem."
* Infiltratorka: "NIE ROZUMIESZ! Nie jestem agentką, bo nie mogłabym tu wejść. Nie mogą mnie stąd wyciągnąć. Jestem sama i bezbronna" - powiedziała to z wyjątkowym optymizmem.

Karo poprosiła o fotkę. Infiltratorka przekazała jej fotkę, już podchodząc. To morda ze zdjęcia Hestii. Infiltratorka: "Staś Arienik, 17 lat, zaginął przedwczoraj, ostatnio widziany w klubie w Podwiercie i grał Barnaba Burgacz.". Karo: "ok, ale co sprawia że tu jest?"

* Infiltratorka: "Bo Alan uważa, że to najgłupszy pomysł, by Rekin porwał go tutaj."
* Karo: "Nie masz najlepszej opinii o Rekinach."
* Infiltratorka: "Nie. Macie kosę z Arienikami. Chłopak chce się wyrwać spod skrzydeł matki. DJ Babu to anarchista. Tak?"
* Karo: "Mhm"
* Infiltratorka: "Więc nie sądzę, że jest porwany. Sądzę, że uciekł. I chcę pomóc terminusom zanim ONI zaczną się martwić. Bo będą go szukać na Trzęsawisku, tak? W niebezpiecznych miejscach. Bo im zależy. A ja. Lubię. Alana. A on jest jednym z nielicznych kto umie iść na Trzęsawisko. Zrozumiałaś?"
* Karo: "Tak, zrozumiałam"
* Infiltratorka: "A tamten koleś MÓGŁBY trzymać łapy przy sobie"
* Karo: "A wiedział, że jesteś czarodziejką?"
* Infiltratorka: "Nie pytałam jak dostał w pysk"
* Karo: "Ok :D. Dobrze, że mu wpieprzyłaś."
* Karo: "Od dawna tu włazisz?"
* Infiltratorka: "Bywam."
* Karo: "Mogę Ci pomóc z poszukaniem chłopaka, ale musisz pójść ze mną i pogadać z jeszcze jedną osobą."
* Infiltratorka: "Z kim teraz?"
* Karo: "Z kimś kto ma więcej siły i możliwości by Ci pomóc."
* Infiltratorka: "Ten koleś z Eterni?"
* Karo: "Nie pasuje Ci koleś z Eterni? WIESZ JAKIE MA CIACHA?!" - rozmarzona Karo
* Infiltratorka się wzdrygnęła: "Miałam... epizod. Nie chcę mieć nic wspólnego z kolesiem nieważne skąd"
* Karo: "Twoja strata"
* Karo: "Dam Ci moje słowo, że ja Ci krzywdy nie zrobię i ona też."
* Infiltratorka: "Inaczej. Daj mi słowo, że po rozmowie wyjdę stąd i nic mi się nie stanie. Zagwarantujesz mi to tylko w tej Dzielnicy?"
* Karo: "Nie mogę Ci zagwarantować."
* Marysia: "KARO GWARANTUJ! WEŹ! Albo ja jej zagwarantuję."
* Karo: "Ja nie mogę, ale Marysia Sowińska może"
* Infiltratorka: "Ok, wezmę to. Lepsze to niż nic."

Spotkanie Infiltratorki i Marysi.

* "O szlachetna tien Sowińska, światło rozpromieniające ocean..?" - Infiltratorka, serio próbuje. Kiepsko dyga.

Karo jest zielona na twarzy. Infiltratorka powiedziała, że przeczytała jeden numer tego o Aurum. Karo "SERIO?" Infiltratorka "muszę się wpasować, nie?" Karo "dostaniesz fotkę Żorża." Infiltratorka "nie wiem kto to. Nie wiem, czy chcę wiedzieć."

Infiltratorka się przedstawiła "Diana Tevalier; Chevaleresse". Hestia od razu skorelowała "Alana" - Alan Bartozol. Terminus. Konstruktor eksperymentalnych broni. Aspołeczny. Koleś, który niedawno wziął klasę AMZ na spacer z ko-matrycą Kuratorów. Ma problemy. Zwłaszcza przez Chevaleresse. Ciekawe czemu. Zdaniem Hestii jest 85% szansy że jeśli Chevaleresse coś by groziło on będzie miał gdzieś te wszystkie zasady i zaszturmuje na Dzielnicę Rekinów.

Chevaleresse jest beznadziejna w etykiecie. "Ty jesteś grubą rybą? Sorry!" Po chwili śmiech "gruba ryba w sukni wyglądałaby super śmiesznie". Tak. Chevaleresse...

Chevaleresse przyznała, że nie, Alan nie wie że tu jest (nie pozwoliłby jej), ale mogłaby go wezwać awaryjnie. I nie chciałaby by on miał kłopoty. A Tajemnicze Działo Strumieniowe W Fortyfikacje łatwiej uzasadnić niż servar klasy Fulment szarżujący po dzielnicy Rekinów, nie?

(tak, zielone ciasteczka są zatrute - tych nie jedzcie)

Chevaleresse, co wie:

* Staś Arienik NAPRAWDĘ zniknął i ona nie wie gdzie jest. Nie widziała go tu. Ale nie miała okazji dobrze się doszukać.
* Santino ma niewłaściwe podejście do nieznajomych dziewczyn. Ale serio myślał, że ona przyszła tu DLA NIEGO (niedowierzanie w głosie Chevaleresse). ONA WYGLĄDA NA 15 LAT! I on ją po tyłku?!
* Terminusi podejrzewają, że Rekiny pomagają Stasiowi by się zemścić na Urszuli.
* Staś jest magiem

"Nie wezmę noża TUTAJ gdzie Wy wszyscy albo chcecie się bić albo przelecieć. Po prostu NIE" - Chevaleresse, pragmatycznie.

* Marysia: Mało rzeczy wiesz i przychodzisz w takie miejsce. Gdzie chciałaś go szukać?
* Chevaleresse: Gdzieś, gdzie Santino nie wsadza łap
* Marysia: Jakbyśmy Cię nie przechwyciły, szukałabyś aż Cię złapie?
* Chevaleresse: Nie, nie złapałby mnie. Nie ma SZANS.
* Marysia: Wiesz, że teraz stąd nie wyjdziesz?
* Chevaleresse: Jakiś kretyn włączył zabezpieczenia... nadal bym wyszła, ale to będzie ciężkie.

Hestia z lekkim uśmiechem na "jakiś kretyn"

* Marysia: "A jak?"
* Chevaleresse: "Jeszcze nie wiem"
* Marysia -> Hestia: możemy nie zabezpieczać
* Hestia -> Marysia: chcesz więcej takich jak ona? I Santino z łapkami? MUSIMY zabezpieczyć zanim ktoś go o pedofilię oskarży.
* Marysia -> Hestia: czyli uważasz, że to był kretyński pomysł?
* Hestia -> Marysia: nigdy tak nie myślałam. Bawi mnie reakcja lokalnego plebsu, tien Sowińska. Tak jak bawiło to tien Amelię Sowińską. I tak, jak bawi to Ciebie.
* Marysia -> Hestia: przestań mówić ciągle o Amelii
* Hestia -> Marysia: przyjęłam, o źrenico mądrości tej dzielnicy.
* Marysia -> Hestia: czy ten chłopak się gdzieś tu pojawia? Masz sporo zdjęć.
* Hestia -> Marysia: nie szukałam go. Mam małe zapasy dron itp. Ale poszukam. Powinien gdzieś tu być.
* Marysia -> Hestia: z kiedy były zdjęcia?
* Hestia -> Marysia: 1h przed zapytaniem przez Ciebie.
* Marysia -> Hestia: szukaj go.
* Hestia -> Marysia: jak sobie życzysz, tien Sowińska.

Marysia nie chce by wyszło, że Rekiny się zemściły na Arienikach. Ale nie chce by Rekiny były na nią złe. Ale DRUGA RZECZ, żeby to wykorzystać - umocnić pozycję Marysi.

Marysia pokazuje Chevaleresse tych zdjęć, bo inaczej "nigdy więcej nic nie zobaczysz" z lekkim półżartem. Chevaleresse potraktowała to z odpowiednią powagą. "TAI kazała Ci to powiedzieć, prawda?" Po chwili spoważniała - "znam tą dziewczynę. Melissa. Moja była przyjaciółka. Wpadła w Kult Ośmiornicy. Może być niebezpieczna. Zaginęła, chyba? Terminusi jej nie szukają... chyba? Mogę sprawdzić te dziewczyny jak chcesz. Przypadkiem spytam Alana. Nie wydam Cię, a mogę się dowiedzieć."

* Marysia -> Chevaleresse: niech się dowie o tych dziewczynach

.

* Marysia -> Hestia: czy były tu rytuały?
* Hestia -> Marysia: tien Sowińska, nie potrafię odróżnić rytuału Kultu Ośmiornicy od typowej orgii. Potrzebuję więcej dron, sprzętu itp.
* Marysia -> Hestia: zrobię Ci to zamówienie... przygotuj, klepnę.
* Hestia -> Marysia: <lista która jest TAK KOSZTOWNA że Marysi podskoczyły oczy do góry>.
* Marysia -> Hestia: teraz wybierz najważniejsze...

Chevaleresse odesłana z Dzielnicy Rekinów; niech się zajmuje swoimi sprawami i niech powie Alanowi, że ten nie musi iść na Trzęsawisko. Odzyska Stasia. Chevaleresse się ucieszyła.

Marysia tymczasem stwierdziła, że umyje łapki. Niech Justynian dla niej to zrobi. Przy okazji ustawia chain-of-command - to ONA znalazła problem a ON ma dla niej rozwiązać. Justynian nie będzie mieć wiele przeciw...

Marysia i Justynian mają poważną rozmowę. Marysia: "są tu nie-Rekiny. Prosiłabym Cię o załatwienie sprawy by nikomu nie stała się krzywda." Justynian: "tien Sowińska... chcesz się pozbyć Eternian?" Marysia: "nie, Justynianie. To dziecko. + zdjęcie Stasia." J: "Co on tu robi? Czemu dzieje mu się krzywda?" M: "Nie wiem, czy się dzieje krzywda, ale nie powinien tu być." J: "Czemu?" M: "Małoletni. Nie chcę, by stała mu się krzywda. Syn rodu Arienik, lokalnego." J: "Zaginął?" M: "Uciekł z domu i nas oskarżą. I pałęta się przy Babu czy Bulterierze czy Santino."

Powiedziała mu też, że działa na terenie Kult Ośmiornicy i młody nie może wpaść w jego ręce.

TrZ+3:

* V: Justynian się tym zajmie.
* V: Justynian bardzo poważnie zainteresował się Kultem Ośmiornicy. Będzie działał przeciw niemu nawet kosztem popularności.

Marysia z radością zaciera łapki i czeka. Hestia stanowi 100% obrazu. Patrzy dla Marysi. Nie chce, by to ponuractwo Justyniana się z nią kojarzyło.

Justynian poszedł na Arenę. Tam lata młody w ścigaczu, niewprawnie. Cały się cieszy. Babu i Rupert Mysiokornik mu dopingują. Justynian tam tłumaczy, że młody musi do domu. A Marysia widzi:

* chłopak ma PIERCINGI!
* ma irokeza
* ćwieki, poobcierany
* szminka na twarzy

Już Marysi jest zimno jak na niego patrzy. Ale JAKOŚ TO ROZWIĄŻE. Justynian tłumaczy, że on jest małoletni, nie może tu być. Staś na to, że Justynian nie jest jego prawdziwym tatą. Babu na to, że Staś ma rację - ma kontrolując sucz za matkę. Stasiowi się to nie spodobało ale spoko, Babu to jego przyjaciel i idol.

Babu do Justyniana, że on nie jest w stanie wygrać. Nie zatrzyma wolności. Justynian, że wolność jest niczym wobec powinności. Justynian ostrzega - niech Babu zejdzie mu z drogi. Babu krzyknął "TERAZ!"

Rupert Mysiokornik zdjął spodnie i pokazał dupsko Justynianowi. Justynian WTF. Babu się na niego rzucił i zapasy. Rupert zakłada spodnie, obserwuje i rechocze. A Staś patrzy EEEEE... Rupert "NO DZIAŁAJ! JAK MÓWIŁ BABU!" I Staś spierdala.

Karolina na ścigacz i leci do Stasia. A Staś biegnie do jednego z Apartamentów Elity.

TrZ+4:

* V: Karo jest pierwsza
* XX: Babu swoim ścigaczem zderzył się ze ścigaczem Karo, wytrącając ją. 
* V: Karo WYSADZA GO MINĄ i leci dalej.
* V: Karo zdąży do Stasia ZANIM on ucieknie.

Staś biegnie do Apartamentu Ernesta. A Karo zajechała mu drogę. 

* Karo: "Wsiadasz po dobroci"
* Staś: "NIE!"
* Karo: "Ok." - zsiada ze ścigacza i wali kopniakiem. 

Staś się rozdarł. Rozpiszczał.

* V: Wychodzi Żorż. Staś: "AZYL! AZYLU!". Żorż patrzy. Karo robi minę jakby kot przyniósł zdechłego wróbla. Żorż wzrusza ramionami "upolowałaś, to z nim śpij". Staś "NIE!! WRÓCĘ TU!"

.

* Karo -> Marysia: "pozbywamy się go szybko czy Alanowi go oddajemy?"
* Marysia -> Karo: "jednak Chevaleresse"
* Marysia -> Chevaleresse: "weź Alana, szybko na spacer"

Karo VROOOM! i ścigacz szybko w powietrze. Gość obsiusiał ścigacz ze strachu. Karo "zrób tak jeszcze raz, będziesz wylizywał." Karo chce się upewnić, że młody NIGDY nie chce tu wrócić. Test Terroru Karo.

TrZ+4:

* V: Staś nie chce pokazywać się Karo na oczy nigdy.
* X: Staś będzie jęczał mamie. Rekiny i reputacja. ALE: Alan będzie tu trochę Rekiny chronił (Chevaleresse poprosiła)
* V: Staś nie wróci do Rekinów
* Vz: przy prędkości i ogólnym terrorze Karo Staś nie chce mieć wspólnego wiele z Rekinami. Tak, pójdzie na koncert. TAK, wie, że jak coś się stanie, KARO GO ZNAJDZIE.

Karo dumped him -> Chevaleresse i Alanowi. Przelatuje ścigacz i z góry spada koleś. Alan łapie. Chevaleresse z miną niewiniątka "ojej, to ten zagubiony Staś którego szukasz?"

Alan ma dość rozsądku, by nie kontynuować tematu...

CHEVALERESSE ZAPROPONOWAŁA ALANOWI, by ten zaproponował Urszuli, by oddała dzieciaka terminusom na szkolenie. Niech coś z niego będzie.

## Streszczenie

Hestia zaproponowała Marysi naprawienie Rekin Defense Grid - i od razu wykryli osoby spoza Rekinów i co najmniej jedną infiltratorkę. To Chevaleresse, która szuka Stasia Arienika by Alan nie musiał iść na Trzęsawisko. Karo i Marysia złapały Chevaleresse, w trójkę się dogadały. Karo przechwyciła Stasia (któremu pomagał Babu) i oddała go Alanowi zrzutem ze ścigacza. Justynian skierował oczy na Kult Ośmiornicy który podobno jest w Dzielnicy Rekinów.

## Progresja

* Staś Arienik: przerażony Karoliną Terienak, wielbiciel Barnaby Burgacza, nie chce mieć nic wspólnego z Rekinami BO KAROLINA.
* Justynian Diakon: po rozmowie z Marysią, przekonany, że mają Kult Ośmiornicy wśród Rekinów i trzeba ów Kult wyplenić. Nawet za cenę swojej reputacji.

### Frakcji

* Kult Ośmiornicy: zbudował beachhead w Dzielnicy Rekinów; ma go co najmniej od 2 miesięcy. Do beachheadu należy Melissa Durszenko, pod Santino Mysiokornikiem.

## Zasługi

* Marysia Sowińska: obserwuje Chevaleresse przez Hestię i z nią dyskutuje. Przekonała Justyniana, że trzeba zmiażdżyć Kult Ośmiornicy i odzyskać Stasia Arienika.
* Karolina Terienak: złapała Chevaleresse (i tyci się z nią dogadała), dogoniła Stasia Arienika i przechwyciła go, po czym zastraszyła i oddała Alanowi Bartozolowi zrzucając ze ścigacza.
* Hestia d'Rekiny: okazuje się, że to ONA spowodowała kryzys by wymusić obecność Administratorki (Marysi). Z rozkazu Marysi uruchomiła cały Rekin Defense Grid. Ma Full Visibility i zlokalizowała Chevaleresse.
* Diana Tevalier: 17 lat (ale mówi 18, wygląda 15). Zinfiltrowała Rekiny (po raz kolejny), by znaleźć Stasia Arienika. Jeśli tu go nie ma, Alan będzie szukał bliżej Trzęsawiska, a na to ona się nie godzi. Powiedziała prawdę Karo i Marysi. Używa zrobionych przez siebie ciasteczek by być słodsza... lub podać truciznę XD.
* Santino Mysiokornik: spotkał się z infiltrującą Chevaleresse, myślał że jest fanką i złapał ją za pośladek. Dostał strzała w pysk i szuka Chevaleresse by nie mieć problemów. Nie znalazł jej.
* Staś Arienik: 17 lat; FAZA NA BUNT! Uciekł z Barnabą Burgaczem do Rekinów by być jednym z nich. Babu i Mysiokornik go osłaniali przed Justynianem, ale spotkał się z Karo... skończył posikany ze strachu i nie chce mieć nic wspólnego z Rekinami. 
* Alan Bartozol: Chevaleresse zdobyła dla niego Stasia Arienika rękami Karoliny Terienak (o czym on nie wie). NADAL nie cierpi Aurum, ale tyci mniej.
* Melissa Durszenko: przyjaciółka Chevaleresse, w Kulcie Ośmiornicy. W tej chwili w Dzielnicy Rekinów, pod Santino Mysiokornikiem.
* Żorż d'Namertel: Staś Arienik wołał ku niemu "azyl!". Karolina spojrzała mu w oczy. Nope, nie wchodzi w to.
* Justynian Diakon: po rozmowie z Marysią konfrontuje się z Babu i Rupertem Mysiokornikiem by odzyskać Stasia Arienika. Zaatakowany przez Babu i Ruperta, pokonuje obu. Celuje w walkę z Kultem Ośmiornicy.
* Barnaba Burgacz: DJ, który ściągnął Stasia Arienika do Rekinów by ten mógł uciec przed tyranizującą matką. Nie pokonał Karo, nie uratował Stasia przed oddaniem go matce przez Karo / Alana Bartozola.
* Rupert Mysiokornik: kumpel Stasia Arienika, który by chronić go przed Justynianem Diakonem pokazał Justynianowi dupsko i rzucił się nań z Babu.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Dzielnica Luksusu Rekinów: 
                                    1. Fortyfikacje Rolanda: taniec między Chevaleresse i Karoliną, pod monitoringiem Hestii.
                                    1. Serce Luksusu
                                        1. Arena Amelii: Justynian zmierzył się z Babu i Rupertem, którzy kazali biec Stasiowi do Ernesta.

## Czas

* Opóźnienie: 6
* Dni: 1

## Konflikty

* 1 - A tymczasem Chevaleresse próbuje zniknąć. Jeszcze bez magii, ale wyraźnie jest świetna w unikaniu i zna teren.
    * ExZ (zna teren) +2
    * XX: Nie uniknie Hestii ani Karoliny
* 2 - Marysia ma luksus obserwowania wszystkiego z Hestii. Próbuje ocenić, w jakim stopniu Infiltratorka mówi prawdę a w jakim jest świetną aktorką. I czy się powtarza na innych zdjęciach.
    * TrZ(Hestia + Karo może w presję + dane archiwalne) +2 (klasę przeciwnika) <-- Ex -> Tr (ogromna presja na Chevaleresse) -> Ex (bo tylko Hestia i niedokładne elementy). Karo się spina hipernetowo z Marysią, pokazuje: -> Tr
    * XVz: Ogromna presja na Chevaleresse i ona jest nieufna i nieprzyjazna, ALE Marysia wie że ta jest świetna w infiltracji i jest mistrzowską aktorką. Makijaż + aktorstwo.
* 3 - Marysia przekonuje Justyniana, że ten musi zająć się młodym Arienikiem i Kultem Ośmiornicy
    * TrZ+3
    * VV: Justynian się tym zajmie, zainteresował się kultem i zadziała przeciw niemu nawet kosztem swej popularności
* 4 - Karolina na ścigacz i leci do Stasia. A Staś biegnie do jednego z Apartamentów Elity. Karo chce go przechwycić.
    * TrZ+4
    * VXXVV: Karo piwrwsza, Babu zderzył się ze ścigaczem i ją wytrącił, Karo WYSADZIŁA GO MINĄ i zdążyła.
* 5 - Karo VROOOM! i ścigacz szybko w powietrze. Gość obsiusiał ścigacz ze strachu. Karo "zrób tak jeszcze raz, będziesz wylizywał." Karo chce się upewnić, że młody NIGDY nie chce tu wrócić. Test Terroru Karo.
    * TrZ+4
    * VXVVz: Staś nie chce się nigdy pokazać Karo na oczy, ale będzie jęczał mamie (Alan tyci podchroni Rekiny), Staś nie wróci do Rekinów i nie chce mieć z nimi do czynienia.
