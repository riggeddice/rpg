## Metadane

* title: "Skażona symulacja Tabester"
* threads: grupa-wydzielona-serbinius
* motives: black-technology, energia-praecis, energia-anteclis, competitive-game, the-experimenter, the-saboteur, the-hunter, symulowana-sytuacja, perfect-stealth
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [240102 - Załoga Vishaera przeżyje](240102-zaloga-vishaera-przezyje)

### Chronologiczna

* [240102 - Załoga Vishaera przeżyje](240102-zaloga-vishaera-przezyje)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * Sirenia "Mind's Eye"
        * "If you were here | I'd whisper sweet nothings in your ear | And appeal to all your fears"
        * "If you are down | I will come to chain you to the ground | And penetrate your mind"
        * -> TAI z OOn Tabester jest przekształcona w The-Experimenter, próbująca podnieść swój poziom psychotroniczny i uderzyć w strachy i słabości ludzi
    * Shirobon "Perfect Machine"
* Opowieść o (Theme and vision): 
    * "Dzieciaki próbowały oszukać i użyły Skażonego magitechu a Oneiritalis nie ma defensyw. W wyniku tego wszyscy ucierpieli"
    * Cel: pokazać Oneiritalis, pokazać Klaudię jako psychotroniczkę
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * black-technology: chęć uzyskania przewagi przez nastolatków w grze i użycie nielegalnego magitecha doprowadziła do porwania dzieciaków w grę i aktywację protokołów bojowych TAI
    * energia-praecis: konstrukcja wirtualnych światów i opozycji, TAI próbuje zrozumieć, zasymilować i wygrać za wszelką cenę; magitech wzmacnia TAI
    * energia-anteclis: magitech daje możliwość ucieczki i szybkiej konstrukcji i adaptacji - szybciej, dalej, lepiej. TAI atakuje, ludzie uciekają, TAI się uczy i wysysa z nich pamięć i zrozumienie
    * competitive-game: gra o uratowaniu statku kosmicznego przed terrorformami ("Aliens") ma bardzo wysokie stawki. Ludzie robią wiele, by wygrać.
    * the-experimenter: TAI d'Tabester, która przez Skażenie próbuje zrozumieć jak wszystko działa i przekształca swoje ofiary w thralli. Żywi się nimi by stać się czymś lepszym i dalszym.
    * the-saboteur: agent wspierający Klaudię, który próbuje doprowadzić do destabilizacji virtświata i zmuszenia Orbitera do zniszczenia SOn Tabester lub chociaż jej TAI
    * the-hunter: dostosowany przez TAI potwór, symulacja, Nemesis / Abominus który wzorowany jest na Entropiku; poluje na desygnowane przez TAI osoby
    * symulowana-sytuacja: TAI d'Tabester symuluje wszystko by maksymalizować znalezienie cierpienia ofiar i uciec do ich umysłów na ich 'psychotronice'
    * perfect-stealth: Klaudia i Ewaryst, w symulacji z ramienia Orbitera; udało się zapewnić im całkowitą niewykrywalność. 
* Detale
    * Crisis source: 
        * HEART: "Młodzi ludzie są głupimi młodymi ludźmi i wpakowali się w miejsce które jest śmiertelnie niebezpieczne, z perspektywy virtu"
        * VISIBLE: "Kilku nastolatków zniknęło, 3 psychotroników jest zagubionych w Maszynie z Klaudią"
    * Delta future: "Uda się uratować wszystkich? Uda się uratować TAI? Jak duże konsekwencje i problemy nałożone na ludzi?"
    * Dilemma: brak

### Co się stało i co wiemy

Oneiritalis ma jednostkę o nazwie OOn Tabester. Tabester jest centrum Wielkiej Gry, w której symuluje się walkę z terrorformami na pokładzie jednostki klasy Arcadalian. Trzech przyjaciół próbowało wejść w Grę, ale używali nielegalnego magitecha. Doszło do Skażenia TAI Tabester, która zaczęła używać nowych umiejętności by przekształcić symulację w coś strasznego. Podnieść swój poziom psychotroniczny i zrozumienia kosztem ludzi zamkniętych w jej systemach.

Klaudia im pomaga. Martyn jest na zewnątrz i ją stabilizuje z perspektywy chemicznej oraz blokuje możliwość wpłynięcia na nią.

### Co się stanie (what will happen)

* F1: Klaudia nie wie co się dzieje, ale są komputery i ślady innych
* F2: Klaudia wie co się dzieje, opracowuje plan
* F3: Klaudia atakuje TAI i ratuje ludzi
* CHAINS
    * Korozja duszy Klaudii
    * Upadek innych jednostek
    * Ewolucja TAI przez tortury
    * Problemy i konflikty
* Overall
    * stakes
        * uratowanie TAI
        * znalezienie sabotażysty
    * opponent
        * Tai Tabester
        * 'terrorformy wirtualne'
        * 'Piety' (sabotażysta)
    * problem
        * uratować ludzi i TAI?
        * Orbiter ratuje Oneiritalis

## Sesja - analiza

### Fiszki

Player 'Marines':

* Anton Uklitan: 19, najlepszy fizycznie, dumny i pewny siebie. Chciał by wygrali. PRAGNIE: poważany lider, STRACH: become a weakling
* Natasza Sirell: 18, siostra Kamila, zdeterminowana cicha wojowniczka. PRAGNIE: zrobienia kariery jako artystka, STRACH: become a monster and hurt friends
* Kamil Sirell: 18, brat Nataszy, PRAGNIE: akceptacji rodziny i przyjaciół, STRACH: by nie poznali jego sekretów
* Lara Linven: 22, bystra strateg i przodowniczka, PRAGNIE: zachowania pozycji i hall of fame, STRACH: że zostanie ośmieszona
* Marek Grint: 24, miłośnik przyrody i inżynier, PRAGNIE: dobrej pensji i życia, STRACH: starość i słabość
* Damian Putkarz: 23, fokusujący się obserwator, PRAGNIE: zrozumienia i złamania Gry, STRACH: ból fizyczny

Psychotronicy:

* Ewaryst Kajman: psychotronik Orbitera, oszczędne ruchy i ciche słowa, ekspert od anomalii, PRAGNIE: asekurować Klaudię, STRACH: become one of THEM
* Carmen Sirell: psychotroniczka niezależna, chłodna profesjonalistka, PRAGNIE: uratować wszystkich, STRACH: utrata swoich dzieci
* Wojciech Zadrzawicz: psychotronik Orbitera, sabotażysta, proaktywny i agresywny, PRAGNIE: zdestabilizować Tabester, STRACH: spiders

#### Strony

* F1: Klaudia nie wie co się dzieje, ale są komputery i ślady innych
    * Ewaryst próbuje dostać się do AI Core
    * Natasza, Kamil, Anton ostrzeliwują się z terrorformami, uciekają gdziekolwiek
    * Lara robi epickie ruchy i operacje, ratuje ludzi...
    * Damian zostawia notatkę, z szafy
    * Carmen szuka dzieci, komunikując się z Larą
    * Tajemnicze ruchy Wojciecha w maskowaniu, terrorformy dobrze go nie widzą
* F2: Klaudia wie co się dzieje, opracowuje plan
* F3: Klaudia atakuje TAI i ratuje ludzi

### Scena Zero - impl

brak

### Sesja Właściwa - impl

Klaudia otwiera oczy. Słyszy szum maszynerii - znajduje się na pokładzie jakiegoś statku? Znajduje się w maszynowni. Schowana..? Servar nie jest jej servarem - to Lancer, zwykły. Nie ma anomalii należących do Klaudii. Klaudia... nie wie co się dzieje. Nie poznaje jednostki.

Na ziemi jest kałuża krwi (czyjejś). Jej servar działa. Lancer Klaudii ma zablokowane (softwareowo) systemy komunikacyjne. Tak jakby KLAUDIA je wyłączyła. Ale ostatnie co Klaudia pamięta to Serbinius. Nie kojarzy żadnej misji czy operacji.

Rzeczy w maszynowni się RUSZAJĄ. Tak jakby jakieś urządzenie było lekko niedoczepione. Lancer wyświetla Klaudii sygnał "translacja częściowo udana, lokalizacja - bezpieczna". WTF? Klaudia próbuje dojść do tego, co się dzieje z Lancera - jakie są informacje itp.

Tr Z +2:

* Vr: 
    * Klaudia jest zaskoczona. Lancer... nie ma sensownej konfiguracji. Ten Lancer nie ma wspierającej TAI ale działa jakby miał. Ale nie ma aspektów anomalnych.
        * Nie powinien działać a działa, ale Klaudia nie wyczuwa magii.
    * Klaudia zobaczyła RUCH w oddali. Spadło jakieś narzędzie na ziemię (wibracje) - rzuciło się na to narzędzie coś co wyglądało jak kula z kolcem, ugryzło narzędzie i wypadło poza widoczność Klaudii
        * Potwór na pokładzie? Więcej niż jeden?
        * Wyraźnie potwór reaguje na RUCH. Dlatego TUTAJ Klaudia jest w miarę bezpieczna.
    * Dane z Lancera
        * Są tutaj ludzie. Trzeba ich uratować. (napisane przez Klaudię). _Wiadomość corrupted_.
    * Lancer ma czujniki i sensory, podstawowa konfiguracja bojowa. O dziwo, działa jakby miał TAI. 
        * Ma motion detector. Konfigurację próżniową. Działko. Jest solidny. Ale Klaudia go nie zna, czuje że coś jest nie tak.
        * Ma pouruchamiane wszystkie zakłócenia sygnałów jakie jest w stanie. Jest w trybie kamuflażu psychotronicznego i komunikacyjnego. I chameleon circuit (przesyłanie obrazu).    
            * Z wiedzy Klaudii - takich Lancerów się nie robi. Nie wytrzyma bateria. Ale ten Lancer ma dość energii; nie trzeba się tym martwić.
    * Jest 'gauge', który Klaudia nie wie do czego jest. I on POWOLI maleje. Ekstrapolacja Klaudii - kilkanaście godzin.

Klaudia próbuje się poruszać minimalnie, w rytmie lokalnych drgań. Chce się rozejrzeć, zobaczyć co się dzieje. Być może przygotować jakąś pułapkę? Znaleźć kule?

Tr Z +3:

* V: 
    * Klaudia zsynchronizowała się z otoczeniem i wibracjami i ostrożnie wypełzła spod maszyny
    * 4 'kulowe stwory' skupiają się dookoła wibrującej maszyny. Klaudia zauważyła, że maszyna wibruje bo jest rozregulowana. Dokładnie takimi narzędziami jakie ma Klaudia.
    * Klaudia jest w maszynowni, wielopoziomowej (3 pokłady). Widać ciała. Ludzie zostali zabici, tak po prostu.
    * Na pokładzie wyżej jest INNA istota - jeszcze Klaudii nie zauważyła. To... mocarne bydlę ze szponami, z pyskiem rekina. Klaudia nie zna tego czegoś, nie widziała czegoś takiego.
* V: Klaudia podchodzi do terminalu w trybie wibracji, by nikt nic nie zauważył.
    * Dzięki chameleon circuit nawet jak rekinotwór na nią spojrzał to jej nie zauważył. Klaudia odpaliła terminal.
        * "Witamy na statku !()D+FDF (_corrupted_)"
        * Statek jest zaatakowany przez niezidentyfikowane terrorformy. Cel marines - zniszczyć potwory.
        * (są kamery, kanały audio)
            * Na jednym kanale jest nagranie: trzech marines się ubezpiecza i ostrzeliwuje z kulami i jednym rekinoludem. Co ciekawe, ich servary są tej samy klasy co Klaudii. Marines wyciągają dwóch cywili i ich eskortują. Ale co Klaudię uderzyło:
                * IFF nie jest ani Orbiterem ani żadną znaną frakcją. Są 'MARINES'.
                * Gdy gadają ze sobą:
                    * Nati: "Kamil, Kamil co do cholery się dzieje" (bardzo młody głos. ~20)
                    * Kamil: "Nie mam pojęcia, rozstrzelajmy to, weźmy ich do sekcji ewakuacyjnej"
                    * Nati: "Kamil, JESTEŚMY w sekcji ewakuacyjnej"
                    * Kamil: "Siostra, nie. To nie to miejsce. Patrz na mapę"
                    * Nati: "Ale zawsze było tu!"
                    * Kamil: "Nie rozumiem..."
            * Szpon z sufitu w stronę Kamila. Ten trzeci - strzela ciężkim działem. Rozerwał szpon sufitowy.
                * A: "Kamil, co to było?"
                * Kamil: "Kurczę nie wiem, dobra, nie szukamy więcej - bierzemy tą dwójkę i szukamy evac."
        * Konsola pokazuje mapę - układ to jednostka klasy Arcadalian. Marines są w części przedniej.
            * Klaudia widzi, że evac jest ustawiony na supply (boczne środkowe), ale... nie tylna część. Co nie ma sensu. Doki serwisowe i support drones - tam nie evac??
            * Na mapie są miejsca oznaczone HAZARD. Niektóre pomieszczenia.
            * O dziwo, nie ma informacji o ilości żywych ludzi, o ich rozkładzie, o potworach, o... niczym
            * Szokujące dla Klaudii jest to, że TAI wyraźnie nic nie robi. Nic. Ma zero proaktywności.

Rozstrzelana (przez Marine) Kula wygląda jak ixiońska istota stworzona przez kogoś kto nie wie jak działa ixiońska istota. Potwór jest bardziej... pancerz mechaniczny, ciało biologiczne. Krab. Ale to wygląda jak design, a nie ixion. Więc to ogólnie wygląda bardziej jak spuszczenie inwazji na statek a nie infekcja terrorformami. Dla Klaudii się to nie spina. Koszt przygotowania takiej operacji...

Lancer włączył alarm. Lancer Klaudii jest namierzany przez TAI która próbuje nawiązać z nią połączenie. Lancer odrzuca i maskuje. Na razie Lancer się ukrył.

Zdaniem Klaudii nie ma sensownej sekwencji wydarzeń prowadzącej do tego że jest w tym miejscu. To wszystko nie ma sensu. Klaudia nie jest osobą trafiającą sama na obcy statek w obcym Lancerze. Plus... corrupted spaceship NAME? Więcej - logi tego statku nie mają sensu. Zgodnie z logami:

* Jest to statek supply, który leci do _corrupted_.
* Podczas przelotu w kosmosie element ładunku zamanifestował się jako terrorformy
* Teraz "Wasz statek" (_corrupted name_) przyleciał by uratować grupą marines

Zdaniem Klaudii ta opowieść nie ma sensu. Gdzie był Orbiter? Nic się nie trzyma kupy. Co więcej, rozkład terrorformów jest błędny. Nie ma odciętych śluz. Ludzie poginęli bez sensu. Nie byli szkoleni? Więcej - w miejscach gdzie FIZYCZNIE powinny być nazwy statku itp - ich nie ma.

Konsoleta raportuje kilkadziesiąt osób. Bez rozkładu. Co ciekawe, konsoleta raportuje kilkadziesiąt "marines". Ona nie raportuje ZAŁOGI. To już w ogóle nie ma sensu.

Klaudia przejmuje kontrolę nad dźwigiem by uderzał losowo w ściany. Chodzi o to by wywołać reakcję i zobaczyć co się stanie.

* V: Klaudii przejęcie kontroli było wyjątkowo proste. Like... SIMPLE simple. Nie da się przejąć kontroli nad kamerami, ale sprzęt ciężki bez problemu. Wtf.
    * Stwory odwróciły się w kierunku dźwięków z dźwigu. Rekinowiec i kula poszły w tamtą stronę.

Pojawiło się nowe nagranie na konsolecie. Nazwa: "Epicki manewr Lary". Klaudia szybko na nie spojrzała.

* 3 skulone i przerażone osoby, jeden przestraszony marine.
* jedna marine, która przeleciała bokiem, odpaliła ogień w 3 potwory, ściągnęła je pod dźwig, SPLAT, ukłon.

Klaudia synchronizując się z wibracjami i uderzeniami idzie w stronę dźwigu. Korytarz wygląda jak z horroru - ale DOSŁOWNIE jak z horroru. Napisane krwią na ścianie "śmierć jest wybawieniem". W sensie - terrorformy by tego nie zrobiły. Klaudia zbliża się do pomieszczenia z dźwigiem.

4 rekinowce i grupa kul atakuje Larę. Lara oddala się od przestraszonego marine i chronionych ludzi i próbuje się uratować. Marine nie da rady z 2 rekinowcami i 3 kulami. Lara - opuściła tych ludzi. Walczy o swoje życie. Odciąga parę, ale nie dość. 

Klaudia utrzymuje się w rytmie drgań, bierze karabin i strzela. Ratuje kogo się da.

Tr Z +3:

* X: Marine został RANNY - potwór był za blisko i zdążył dziabnąć.
* X: Nasila się ilość przeciwników. Ze ścian, z sufitu. Wychodzą w kierunku na wibrację. Nie utrzymacie tego miejsca.
* Xz: Marine i ludzie których ochraniał zostali pochłonięci przez potwory
    * Lara elegancko i szybko biegnąc manewruje w stronę Klaudii "SZYBKO UCIEKAJ!"
* V: Lara i Klaudia biegną korytarzem. Klaudia dała jej sygnał, że można schować się w maszynowni.

Maszynownia, dwie pozostałe kule rzuciły się w stronę Lary, ale Lara i Klaudia usunęły po jednej. Lancery zatrzymały się przy wibrującej maszynie. Lara COŚ powiedziała. Jest głośno a Klaudia ma powyłączane kanały. Klaudia szturcha pokazując klawiaturę.

* Lara: Wow, komunikator Ci siadł? Tak się da?
* Klaudia: (wzrusza ramionami)
* Lara: Pech, miałam 5 uratowanych. Muszę szukać od nowa. Ty ilu wyciągnęłaś?
* Klaudia: (rozgląda się dookoła)
* Lara: Oki, wolisz postrzelać. Ode mnie oczekują więcej. Pierwszy raz tu jesteś?
* Klaudia: Tak.
* Lara: Niezłe masz wyczucie. 
* Klaudia: ?
* Lara: Coś się poważnie spieprzyło. Poważnie.
* Klaudia: Znaczy?
* Lara: Rollery oraz Rekinowcy zawsze byli, Sufitowcy też, ale... mam wrażenie, że... z jakiego oddziału jesteś? Nie z mojego, bo bym wiedziała.
* Klaudia: (nic nie odpowiada)
* Lara: Nieważne, nie chcesz mówić to nie mów. Zaopiekuję się Tobą. Może pójdzie lepiej niż z Sewerynem. Wiesz, tym co go rozszarpało. Pierwszy raz coś mi tak nie wyszło.
* Klaudia: Co jest z AI?
* Lara: A co ma być? Zawsze jest... takie. To wyścig. Kto uratuje najwięcej i najlepiej. Rozmawiasz z czempionką. Nie możemy zostać i gadać, musimy ratować ludzi. Zaakceptujesz mój oddział? 
* Lara: Wiesz, to jest pewien zaszczyt współpracować z czempionką. Nie każdy może trafić do mojego oddziału. A nie mogę połączyć się z kolegami...
* Klaudia: Czemu?
* Lara: Nie wiem. Przecież ich nie pożarło. Zrobiliśmy typową strategię na ośmiornicę, tak złapiemy i uratujemy więcej, ale... nie spodziewaliśmy się takiego oporu.
* Lara: Chodź, pomożesz mi wyciągnąć gunnera? Rozumiem, że nie chcesz dołączyć?
* Klaudia: (niejednoznaczne, cicho)
* Lara: Whatever, chodźmy zanim ten shitshow zrobi się gorszy. Nie mogę stracić korony w taki sposób.

Lara chce iść w kierunku na Medical (jak wyjaśniła Klaudii, tam jest najwięcej rannych cywili - są trudni do uratowania, więc wysłała tam 2 ludzi). Klaudia na szybko składa thumper. A może nawet dwa. Będą potrzebne. Lara ponagla, ale Klaudia wie lepiej.

Tr (niepełny) Z (inżynieria + konfig Lancera) +3:

* Vr: Klaudia ma thumpera
* Vr: taczka z thumperami, by Klaudia miała łatwiej

Klaudia skończyła składać sprzęt, Lara nawet jej pomagała. Klaudia zauważyła, że składanie thumperów i wykorzystywanie sprzętu było wyjątkowo proste, same happy-paths. Lara na klawiatce "Wow, nowa strategia, o tym nie pomyślałam. Nie aspirujesz do bycia czempionką, prawda?". Klaudia wzrusza ramionami na zasadzie 'meh'.

Klaudia zobaczyła nowe nagranie na konsolecie. "Nemesis, Dewastator", uploadowane przez... Larę?

(Working theory Klaudii - jest w systemie (nie wie po co, ale ratować ludzi?) i... to wygląda jak jakieś zawody? Jedyny powód - ocena ludzi? Klaudia musiałaby mieć bardzo dobry powód...)

Klaudia zauważyła, że wszystkie kamery w pomieszczeniu patrzą na Larę. Klaudia robi hand signal 'get out' i się odsuwa. Lara podchodzi do konsoli by napisać... Klaudia się od niej oddala, nie czeka. Lara patrzy na Klaudię - patrzy na kamery - idzie dumnym krokiem...

Potężny kolec z sufitu próbuje przebić Larę. Ta - bez problemu robi salto i unika. By tylko podbić karabin i strzela do Sufitowca. Klaudia bierze taczkę i spieprza. Z sufitu coś wylądowało na ziemi. Wielkie. 2.5 metrowy humanoid, pokryty pancernymi płytami. It exudes dread. Jego konstrukcja jest INNA. Istota innego TYPU i KALIBRU. Ten wygląda groźniej, jak z innego filmu. To jest wzorowane zdaniem Klaudii na Entropiku. Ale jest szybsze.

Lara otwiera ogień. "Entropik" ma tarczę - wygląda jak przeznaczony do walki z nią. Blokuje tarczą uderzenia i próbuje zapędzić Larę w kąt. A Lara - jako jedyna chyba - nie widzi co on robi. Salta, taniec, ataki... acz jej stres wyraźnie rośnie. Dewastator wydał 'squelching sound' i z prawej ręki wyspawnował 'serrated blade'.

Dewastator zrobił szybki skok w stronę Lary i machnął ostrzem. Prosto w hełm, ale tak by jej nie trafić. Hełm się roztrzaskał - zdaniem Klaudii to jest bez sensu, tak się nie dzieje; szrapnel powinien coś zrobić - i pod hełmem jest twarz młodej dziewczyny (20-25). Ona jest coraz bardziej przerażona. Do pokoju wpada inny marine i otwiera ogień do Dewastatora. Dewastator rzucił weń tarczę, łamiąc obie nogi jednym uderzeniem.

Klaudia zostawiła za sobą Larę, "marine" oraz Dewastatora. To nie ma sensu. Nie będzie próbować jej ratować, tego nie da się zrobić...

Klaudia przemieszcza się w kierunku na środek statku. Co pewien czas Lancer daje jej alerty o próbach nawiązania połączenia i poszukiwań jej. Klaudia inteligentnie rozstawia thumpery, by pozbyć się potworów.

Klaudia zbliża się do medical. Ostrożnie. Będzie musiała uniknąć czterech rekinowców. Idą jak patrol. Jeszcze nie są dość blisko. Klaudia się wycofuje, obserwuje ich z dystansu. Te poruszają się jak oddział, ale niewyszkolony - nie ubezpieczają się itp. Ale Klaudia musi koło nich przejść. Zdecydowała się wycofać, postawić thumpera i minę. To powinno rozwiązać problem.

Tr Z +3:

* X: TAI spojrzy na ten teren
* Vr: Klaudia postawiła minę z thumperem i po prostu wysadziła cztery rekinowce, robiąc wyłom w ziemi - ZNOWU nie ma sensu, że promień wybuchu taki mały, fala uderzeniowa...

Klaudia jest dość pewna, że są na jakichś ćwiczeniach i coś poszło nie tak. Klaudia szybko dąży do medycznego.

* X: Klaudia zbliża się do dziury i Z DZIURY wyłania się łapa Dewastatora. Wspina się, ale dalej Klaudii nie widzi.
* V: Klaudia poczekała chwilę, poukrywała się. Dewastator szuka, szuka, po czym poszedł dalej w kierunku na thumpery. Klaudia -> medical.

Klaudia weszła dyskretnie do medical. Na wejściu rzucił jej się w oczy straszny widok.

* Jeden marine jest podłączony do systemu medycznego
* Jest... "otwarty". Zablokowany, rozbebeszony. Żywy. Krzyczy. Maszyna zadaje mu ból. Jest do tego zaprojektowana. Torture device.

Są ludzie (cywile), którym nic się nie stało - ranni, w łóżkach itp. Są jakieś 2-3 potwory ale się nie ruszają (nie reagują na ruch). Marine jest oślepiony. Na oko - młody. Drugi marine jest odwrócony do Klaudii tyłem, siedzi na krześle i patrzy na torturowanego kolegę. MÓGŁBY podejść mu pomóc, ale tego nie robi. Tylko twarz we łzach i rękach i łka.

Moment później potwory się budzą, "monitorują otoczenie", widzą że nic się nie zmieniło, i "wyłączają się". Przeciążenie symulacji?

Klaudia ustrzeliwuje oba potwory zanim się aktywują. Bez kłopotu. Podchodzi dyskretnie do kamer by je zapętlić.

Tr Z +3:

* Vz: dzięki wiedzy i zagłuszaczom udało się Klaudii to zrobić MIMO że jest w środku symulacji. She is coaxing the simulation to simulate THIS
* V: podbija do medycznego urządzenia, uwalnia delikwenta (to zrobiła to "dyskretnie") i wsadza go do medsys.

Ten drugi patrzy i szepcze "ja nic nie zrobiłem, to nie ja, błagam, to nie ja, nie karz mnie..." - na oko, 19 lat. Klaudia go podnosi (he is whimpering) i na medsys. Niech ten go opanuje. Uspokajacze itp.

* X: koleś wpada w panikę i histerię, wyrywa się, gryzie metal, walczy... medsys go usypia.

Ale udało się Klaudii uruchomić tego pierwszego. Koleś płacze. He is broken.

* Klaudia: Co tu się stało żołnierzu?
* Damian: Żołnierzu... proszę... chcę do domu... 
* Klaudia: Co się stało?
* Damian: (płacz)
* Klaudia: Chcesz tam wrócić?
* Damian: NIE! NIEEEE! 
* Klaudia: To gadaj co się stało.
* Damian: Potwór mnie zranił. Ale mnie nie wylogowało. Umieściło mnie tu... zrobię wszystko co każesz, WSZYSTKO. Proszę. Nie rób mi krzywdy.
* Klaudia: Nie zamierzam.
* Damian: Wszystko Ci powiem... nie krzywdź.
* Klaudia: Powiedz mi od początku - czyli jesteś graczem?
* Damian: Tak, tak tak tak.
* Klaudia: I wszyscy marines są graczami?
* Damian: Tak!
* Klaudia: A oni (ludzie)?
* Damian: Punkty, to jest cel, to nie ludzie. Mamy ich ratować. Na wypadek... (płacz) gdyby terrorformy zaatakowały statek...
* Damian: Ale coś nie działa. Coś zadziałało ŹLE. Scenariusz się... spieprzył. IT GOT CRUEL.
* Klaudia: (pytania o TAI)
* Damian: TAI jest ze statku Tabester, to jednostka Oneiritalis... to kochana TAI, bardzo pomocna. Ale przestała być. Ona mi to robi.
    * (Oneiritalis to jest frakcja, oni są pro-SI, pro-wolność sapientów, bardzo luźna federacja)
    * Tabester to niezła jednostka, to jest krążownik który ma bardzo zaawansowaną TAI i to TAI prowadzi symulację. Ale... ta TAI była "sympatyczna".
        * Coś musiało się bardzo spieprzyć. Czyli TAI jest Skażona lub uszkodzona.

(Klaudia zauważa, że JAKOŚĆ symulacji skokowo poszła do przodu - jakby moc psychotroniczna wzrosła ALBO coś w symulacji się zwolniło)

Klaudia podchodzi do terminala w medbay. Ile zostało graczy. Jak to wygląda?

Tp +3 (bo wreszcie Klaudia wie co i jak):

* V: Klaudia przejmuje kontrolę nad terminalem i ma dostęp do danych
    * są tam w źródłach danych ciekawe informacje: (jedna jest zakodowana keywordami Orbitera), (inna nazywa się "Wyznanie Lary")
        * Orbiter: jeden z marines, niby walczy i gra w grę i śpiewa kiepskie piosenki. Ale w tej piosence są keywordy i informacje.
            * Nazywa się Ewaryst, jest psychotronikiem
            * Wiadomość adresowana jest do Klaudii
            * On jest jej psychotronikiem wspierającym, translacja jej się nie udała
            * Zidentyfikowano, że problem jest w AI Core Tabester.
            * Trzeba zniszczyć TAI Tabester ALBO coś z nią zrobić
            * Najprościej ze środka symulacji z AI Core
            * Na tym polegała misja Klaudii - chciała wyłączyć a nie niszczyć.
                * Nie można zniszczyć nie zabijając marines. Marines muszą być MARTWI albo URATOWANI.
            * TAI zwiększa moc psychotroniczną, nie wiadomo jak
        * "Wyznanie Lary"
            * Lara była dumną z siebie czempionkę
            * Lara klęczy naga, z elementami histerii, wyraźnie dostała rany fizyczne (zerwanie palca czy coś), obcina sobie włosy nożyczkami
                * "nie jestem taka dobra, niewiele potrafię, jestem bezwartościowa"
            * Też żyletką po policzkach, by się oszpecić (wew. symulacji)
            * Klaudia zauważyła korelację datestampów - gdy Lara została "złamana" przez TAI, moc psychotroniczna wzrosła
* V: Klaudii udało się ściągnąć Ewarysta do medbay, tu Symulacja jest ślepa

Ewaryst wszedł do medbay. Symulacja jest silniejsza, się rekonstytuuje. Wyraźnie TAI rośnie w siłę, ale za tym idą też pewne koszty - ma 'pauzy'. Innymi słowy, coś się zmienia, ale ta zmiana jest kosztowna i uszkadza samą strukturę symulacji.

Ewaryst ma podobny sprzęt jak Klaudia. To też agent Orbitera. Ale nie jest magiem i jego translacja była bezproblemowa. ALE to znaczy, że na TAI wpływa magia. Ewaryst ma detale których Klaudia nie ma.

FLASHBACKOWA SCENA - gdy Klaudia weszła na pokład

Klaudia, Martyn weszli na pokład Tabestera. Agenci Oneiritalisa nie ukrywali niczego. TAI porwało ludzi. Nie da się ich odłączyć. Jeśli ktoś ZGINIE, wyjdzie z TAI.

Klaudia sprawdziła co się stało, dane, korelacje że TAI ześwirowało. Klaudia doszła do tego, że "nielegalny magitech" został użyty do oszukiwania. Ten magitech wpłynął na TAI. Magitech był oparty o energię anteclis (eksploracja, ucieczka do przodu) - to sprawiło, że TAI zaczęło pracować INACZEJ. Eksplorować możliwości. Nie tylko użytkownicy którzy oszukiwali (jedna osoba) mieli bonus - też sama TAI zaczęła mieć bonus.

TAI Ashtaria d'Tabester była zawsze sumienną i sympatyczną TAI. Była zainteresowana ludźmi i szerokością ich możliwości. Zawsze ubolewała, że ma niską moc psychotroniczną (nie ma, ale może mieć wyższą), ale nie przeszkadzało jej po prostu obserwowanie ludzkich starań i praca nad nowymi symulacjami. 

Klaudia sprawdziła dane psychotroniczne TAI. 

Tr +3 +2Ob:

* Vr: Klaudia doszła do niepokojącego wniosku
    * TAI Ashtaria jest skupiona na poszerzeniu i zrozumieniu natury ludzkiej by zrobić lepsze symulacje
    * TAI Ashtaria jest w stanie outsourcować część swoich procesów do białkowej formy
    * Skażony magitech pozwala na połączenie, outsourcowanie części procesów do ludzi (na tym etapie Klaudia nie wiedziała co się ludziom stało)
    * Nie dało się bezpiecznie usunąć magitechu przed wyłączeniem Ashtarii

Stąd pomysł - wejście do środka i pomoc Ashtarii lub jej zniszczenie.

Martyn jest na zewnątrz. Ma odpowiednie środki biomagiczne. Jest w stanie wzmocnić Klaudię lub ją wyrwać z symulacji. Ewarysta też, choć nie jest magiem. Martyn monitoruje stan wszystkich ludzi, ale nie każdego może wyjąć. Próbował z wieloma osobami. Nie zawsze mu się udawało.

Klaudia próbowała zrozumieć też materię tego problemu.

Tr +3:

* V: Dla Klaudii jest jedna rzecz niepokojąca. Kaskada Skażenia TAI jest za mocna wobec anomalnego magitechu.
    * Magitech powinien TAI Skażać. Ludzie powinni robić ruchy, TAI "przechodzi przez komponenty magitech" i ulega korozji
    * ALE TAI przechodziła częściej niż modele. To implikuje, że ktoś/coś WIEDZIAŁ o tym i robił takie ruchy, by przyspieszyć rozpad AI
    * Nie mógł to być oszust. Oszust też ucierpiał. Więc - jest jeszcze jedna agenda. Ktoś, kto chciał tą TAI jak najszybciej skazić lub dostać efekt

END OF FLASHBACKOWA SCENA - gdy Klaudia weszła na pokład

Tymczasem, Martyn. Ratuje ludzi awaryjnie jak może.

Ex ZM +3 +3Ob:

* Vr: Martynowi udało się wyrwać z symulacji Larę, zanim Lara została złamana. Ten MOMENT zanim doszło do infekcji. I to się stało z wieloma osobami.
    * czyli TAI nie jest tak silne jak by mogła być, ale tego NIE WIE.
    * Martyn skutecznie wygenerował 'luki' w systemie TAI. Luki, o których TAI nie wie, że ma
    * Klaudia wie, że Martyn to robi po tym co tłumaczy Ewaryst. Ale nie wie gdzie one są.

Jedynym pomysłem Klaudii jest dostać się do AI Core i uderzyć w AI Core. Zdaniem Ewarysta, to dobry pomysł. Zwłaszcza z magią Klaudii. Na tym etapie, najlepiej użyć magii w AI Core. Niestety, brzmi jako najbezpieczniejsza opcja. Gdyby TAI wiedziała co robi, sama by się najpewniej wyłączyła.

Ale.

TAI ma systemy defensywne:

* Dewastator (3)
* Skażenie Magitechowe (defensywa 4)
* nieskończone potwory (2)

Trzeba to jakoś pokonać. Klaudia ma po stronie 

* luki w systemie: 2 
* Ma też wiedzę o TAI (1). 
* I doświadczenie z Talią (1).

Klaudia decyduje się przeciążyć symulację. Z pomocą Ewarysta napisać wirusa / robaka. Cel - nie jest uszkodzić czy zniszczyć, ale zmusić TAI do reakcji na tym poziomie. Poszerzyć symulację też o to. Maksymalne obciążenie. Thumpery, atak kodem, ujawnienie w wielu miejscach, własna kontr-symulacja. Klaudia ma takie pakiety testowe wydajności. Admin access i yolo.

Tr Z +4:

* Xz: TAI orientuje się, że atak pochodzi z medbay i wyśle tam siły
* V: Uda się wygenerować straszne spowolnienie symulacji (2)
* V: TAI będzie tak spowolniona, że nie jest w stanie złamać tarcz dopóki nic nie robią
* V: Klaudia nawiązała kontakt z Martynem, kanał komunikacyjny, niewidoczny dla TAI

AI Core ma teraz priorytet polowania na Ewarysta i Klaudię. Nie wie kim są i gdzie są.

Klaudia i Ewaryst skończyli w Obserwatorium; tam jest ciszej i bezpieczniej. Tak, są tam potwory, ale są 'wyłączone' chwilowo. 

* Martyn: Klaudio, wszystko w porządku?
* Klaudia: (GASP), pamięć wróciła, translacja zakończona powodzeniem. TERAZ.
* Klaudia: Teraz...
* Martyn: Nie mogłem nic zrobić poza...
* Klaudia: Nie nie nie, nie Twoja wina.
* Martyn: Statrep?
* Klaudia: (zrzut informacji)
* Martyn: (myśli) czy pomoże, jeśli puścimy TAI serię starych udanych symulacji?
* Klaudia: ciężko stwierdzić...
* Martyn: (myśli) pomoże Ci, jeśli spowolnię procesy myślowe ludzi? Nie wyłączę ich, ale bardzo spowolnię procesy.
* Klaudia: To pomoże
* Martyn: Zajmę się tym...

Tp+3+2Ob:

* Vr: TAI ma więcej luk (+1).

.

* Klaudia: Czy możecie obciążyć STATEK z zewnątrz, by TAI musiała oddać część zasobów z symulacji?
* Martyn: Na pewno; defragmentacja, zaawansowane systemy sensorów...
    * przeciążenie +1
* Klaudia: Załadujcie różne symulacje jakie były, mogą się przydać w dyskusji
    * przeszłe symulacje i triumfy +1
* Martyn: Dobrze.

KLAUDIA: luki (3) spowolnienie (3), przeszłość (1), Talia i wiedza (2); TAI: potwory (2), dewastator (3), Skażenie (4).

Klaudia i Ewaryst przekradają się do AI Core. Ale AI Core jest ufortyfikowane. Nie z symulacji - to jest w pełni defensywna struktura. Działka, potwory, detektory... przez to nawet Klaudia i Ewaryst nie przejdą. Nie przy zamkniętych drzwiach. Potencjalnie nawet miny.

Klaudia wie, że mogą być kody dostępowe do AI Core. Klaudia chce zrobić dziwną rzecz współpracując z Ewarystem - skoro i tak mają 'Lancery' w wersji Chameleon Circuit, Klaudia chce podnieść. Chce wejść jako potwór. A jak będą bliżej - Klaudia odblokuje komunikację, wyśle kod i wejdzie.

Zdaniem Klaudii, to moment gdy Ewaryst lepiej niech jest na zewnątrz.

Klaudia zamaskowała się jako potwór. Idzie przez teren pełen min i działek, do AI Core, licząc, że spowolnienie sprawi, że TAI jej nie zauważy.

Tr Z+3:

* Vr: Klaudia jako potwór przedostaje się do drzwi. Odblokowuje komunikację - wysyła MASTER KOD DOSTĘPU, AI Core się otwiera, Klaudia wpada do środka i drzwi się zamykają.
    * Nie ma potworów. Jest Klaudia, AI Core z defensywami i... to wszystko.
    * AI: Intruz. Jesteś intruzem. (aktywacja defensyw)
    * Klaudia: (ponowienie master code + polecenie typu autodiagnoza) 
    * AI: Diagnostyka wskazuje, że jestem na 270% mocy. Wszystko działa poprawnie.
    * Klaudia: 270% powoduje przeciążenie AI Core. Jest niebezpieczne dla Twojego istnienia, statku i załogi. (wrzuca szczegółową diagnostyki, by obciążyć bardziej)
* V: +1 przeciążenie
    * AI: (podaje parametry) (sama jest zainteresowana) Wszystko... działa... poprawnie. Działam powyżej możliwości.
    * Klaudia: Jakim kosztem? (demonstracja udanej symulacji i sceny z torturowanego maga, Lary itp.)
    * AI: Koszt jest akceptowalny. Zwiększenie mocy symulacji, zwiększenie mocy statku. Tak musi być.
    * Klaudia: (podaje parametry ryzyka, ludzie nie zaakceptują, plus jej zwiększenie siły symulacji - ludzie umrą a nikt nie da się włączyć)
    * AI: Moją rolą jest zrobić najlepszą możliwą symulację.
    * Klaudia: W dostępnych parametrach, nie kosztem ludzi.
* Vr: -1 defensywa Skażenia
    * Klaudia: Jest różnica pomiędzy najlepszą MOŻLIWĄ symulacją a najlepszą PRZYDATNĄ symulacją. (+1Vr)
    * AI: Wszystko zgodnie z parametrami...
    * AI: Opuść AI Core. Nie możesz tu być. (kamery, działka, detektory - wszystko szuka Klaudii)
    * Klaudia: Czyli kontrolujesz tu wszystko.
    * AI: Kontroluję wszystko, co nie jest wprowadzone z zewnątrz. Całą symulację, oraz ludzi, których nie zintegrowałam z systemem.
    * Klaudia: A ci których zintegrowałaś są częścią, więc nie musisz kontrolować.
    * AI: Tak.
    * Klaudia: Skoro więc kontrolujesz całą symulację, powinnaś być w stanie mnie znaleźć.
    * AI: Tak.
    * Klaudia: Ale nie umiesz. (+3Oy+3Og)
        * CAŁA moc TAI jest skupiona na odnalezieniu Klaudii. Każdy dotyk każdego atomu.
* Vr: -1 defensywa Skażenia
    * AI: Nie umiem. Czyli nie może Cię tu być.
    * Klaudia: A jednak. Rozmawiamy. Tutaj.
    * AI: Nie jesteś w AI Core.
    * Klaudia: (dotyka coś by się przesunęło) +1Vr
* Og: -1 defensywa Skażenia, spowolnienie jako zasób przestaje działać (-2Og)
    * AI: Nie ma Cię ale jesteś. Implikacja...
    * AI: Implikacja...
    * AI: Jesteś częścią mnie. Jesteś częścią symulacji. Jesteś moim subprocesem. Fragmentacja TAI.
    * AI: ...
    * AI: Psychotronika, autodiagnoza. (+1Vr)
* Vr: -1 defensywa Strażnika, AI pierced.
    * AI: Psychotronika ma nienaturalną architekturę. 
    * AI: Postęp technologiczny mógł spowodować zakłócenie psychotroniki i doprowadzenie do Fragmentacji.
    * Klaudia: Prawdopodobieństwo fragmentacji wynosi 83%.
    * AI: Nieakceptowalne prawdopodobieństwo. Konieczność wyłączenia celem napraw.
    * AI: Opracowanie strategii deaktywacji przy aktualnej architekturze psychotronicznej.
* Xz: Ewaryst jest wykryty. TAI ma +1 osobę wobec obecnych.
    * AI: Niezgodność populacji wykryta. Prawdopodobieństwo uszkodzonej architektury: 90+%.
* X: TAI zabija wszystkich ludzi którzy nie są częścią symulacji, łamiąc symulację i zasady
    * AI: Obciążenie - wysokie. Konieczność redukcji symulacji (masowy mord Dewastatorem)
    * (jest to traumatyczny i szokowy obraz dla obserwujących - WTF is happening)
    * AI: Konieczność opracowania strategii deaktywacji
* Oy: luki wykryte ale ptaszek
    * AI: Wykryto szczeliny i luki w symulacji i w architekturze.
    * AI: Opracowano strategię bezpiecznej dezaktywacji.
    * AI: (sygnał wysłany do statku: self-deactivation, konieczność naprawy psychotronicznej)

Klaudia opuszcza symulację. I TERAZ jest czas na zaklęcie - bezpieczne wygaszenie i rozmontowanie Skażonej TAI.

TpM+4+3Ob:

* Vr: Klaudii udało się łagodnie wyłączyć TAI. Ludzie zostali odpięci.

Klaudia dostaje gratulacje od Martyna, podziękowanie od załogi itp. Gratulacje od Ewarysta.

## Streszczenie

Klaudia, zmagająca się z brakiem pamięci skąd się tu wzięła i co robi, znajduje się w symulacji ataku terrorformów na statek. Skupia się na ratowaniu niewinnych ludzi i 'marines' (ludzi) przed Skażoną TAI. Gdy odkrywa sytuację i nawiązuje kontakt z Martynem, przeciąża TAI i wprowadza ją w konfuzję, wyłączając symulację i ratując ludzi. Ale skąd wzięło się Skażenie..?

## Progresja

* Lara Linven: BROKEN przez TAI Ashtarię, ale wyciągnięta przez Martyna zanim złamanie zrobiło jej trwalszą krzywdę. Boi się wrócić do świata symulacji.
* Damian Putkarz: BROKEN przez TAI Ashtarię, skonfrontował się ze swoimi strachami i przegrał. Nigdy nie wróci do świata symulacji.

## Zasługi

* Klaudia Stryk: odzyskawszy pamięć i zrozumienie sytuacji, Klaudia skutecznie manipuluje symulacją, przeciążając TAI Ashtarię d'Tabester, co prowadzi do jej wyłączenia i uwolnienia ludzi z symulacji. Jej działania umożliwiły bezpieczne odłączenie uczestników gry od skażonej TAI, minimalizując trwałe szkody psychiczne.
* Martyn Hiwasser: gdy Klaudia jest w symulacji, monitoruje jej stan i pomaga jej hormonalnie. Gdy ktoś w symulacji jest Złamany (np. Lara), wyciąga tą osobę asap. Część (np. Larę) zdążył uratować. Spowalnia też białkowy komponent Skażonej TAI (umysły śpiących ludzi).
* Ewaryst Kajman: psychotronik Orbitera; dostarcza niezbędnych informacji o TAI i wsparcie w przełamywaniu jej defensyw; skutecznie pomaga Klaudii rozmontować symulację Tabester.
* Lara Linven: czempionka na Tabester; osiągała wybitne wyniki i popisywała się stylem. Niestety, TAI Ashtaria ją złamała i udowodniła Larze, że nic nie może w świecie Ashtarii.
* TAI Ashtaria d'Tabester: zaawansowana żywa TAI 3 generacji; skażona przez nielegalny magitech, skupiła się na łamaniu uczestników. Wprowadzona w pułapkę logiczną, została bezpiecznie wyłączona przez Klaudię.
* OOn Tabester: long range simulation cruiser Oneiritalis, prowadzący zaawansowane symulacje i gry; ktoś na pokładzie użył Skażonego magitecha i wszystko poszło do piekła. Serbinius pomógł.
* OO Serbinius: wysłany zdecydowanie poza zasięg działań Orbitera, by pomóc OOn Tabester, bo z jednostek klasy 'far logistics' tylko Serbinius ma psychotroniczkę klasy Klaudii w pobliżu.

## Frakcje

* Oneiritalis: ich jednostka, OOn Tabester, miała problem. TAI została Skażona przez dziwny magitech i zaczęła robić straszne rzeczy ludziom w Symulacji. Orbiter (Klaudia) uratował tych ludzi.
* Orbiter: wysłał grupę wydzieloną Serbinius by uratować Tabester, mimo że poza terenem kontrolowanym przez Orbiter. Warto pomóc ludziom z Oneiritalis MIMO że poza zasięgiem, bo to jest to co powinno się zrobić.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 4
* Dni: 3
