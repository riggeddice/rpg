## Metadane

* title: "Nienawiść do świń"
* threads: legenda-arianny
* gm: żółw
* players: kić, kapsel, fox

## Kontynuacja
### Kampanijna

* [200822 - Gdy Arianna nie patrzy](200822-gdy-arianna-nie-patrzy)

### Chronologiczna

* [200822 - Gdy Arianna nie patrzy](200822-gdy-arianna-nie-patrzy)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* ?

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Arianna wyszła w końcu ze szpitala. Nareszcie jest aktywna i sprawna. I pierwsze, co ją spotkało - opiernicz u admirała Kramera. Ale za co teraz?! Nic nie zrobiła!

Kramer opieprzył Ariannę, że robi jakieś pojedynki dla gawiedzi. Że powstała holodrama "Sekrety Orbitera". Że to wszystko naruszyło strukturę szlachty eternijskiej i relacjom Eternia - Orbiter. A potem jeszcze Arianna podkupywała się Eterni (to była Klaudia w imieniu Arianny o czym Arianna nic nie wie).

Aha, Martyn jest w brygu za napaść magiczną na trzech thugów (tu Arianna ma szczękę przy podłodze). Arianna, całkowicie załamana i nie wiedząca co odpowiedzieć Kramerowi (który wyraźnie myśli, że Arianna coś wie), wysłała szybko sygnał do Eustachego. Czy on może jej coś podpowiedzieć?

WCZEŚNIEJ:

Eustachy dowiedział się, że Martyn kibluje w kiciu. Poszedł go odwiedzić - o co chodzi? Martyn wyjaśnił, że był z pewną damą. I owa dama była napadnięta przez trzech takich co chcieli ją i jego skrzywdzić. To Martyn użył magii...

Eustachy koniecznie chce Martyna wyciągnąć z tego. Kim jest ta dama? Martyn nie powie. Odmówił. Czemu? Bo mu Eustachy ją poderwie...

...

Eustachy pogadał ze strażnikami. Małe przekupstwo i Martyn trafi do celi, gdzie nie ma dam w pobliżu (tylko ONA będzie przychodzić). Nie będzie Martyn miał szczęścia. To będzie prawdziwa kara. Ale nie dowiedział się niczego szczególnego, poza tym, że Klaudia wie.

TERAZ: 

Arianna nie dowiedziała się niczego ani od Eustachego (który nie wie) ani od Klaudii (która wyjaśniła "mniej wiesz, lepiej śpisz"). Kramer ciężko westchnął. Nic nie poradzi. Tak czy inaczej, Arianna ma nową, bardzo trudną misję. Oczywiście, Arianna zamieniła się w słuch ;-).

Kramer wyjaśnił Ariannie, że mają dyskretną misję. Coś ważnego i tajnego. Co gorsza, musi zachować dyskrecję przed załogą póki nie dotrą do celu.

* Na Astorii istnieje miasteczko o nazwie Trzeci Raj, tam znajduje się populacja zasymilowanych noktian.
* Pierwszym noktiańskim kapitanem jest Sebastian Alarius, pilotujący normalnie Galaktycznego Tucznika i transportującego glukszwajny na orbitę.
* Sebastian Alarius został aresztowany za przemyt i zamknięty na brygu Kontrolera Pierwszego. Ale Kramer tego nie kupuje.
* W Trzecim Raju dzieje się coś bardzo złego. Spisek?
* W Trzecim Raju znajduje się eksperymentalna, bardzo potężna TAI o imieniu Ataienne. Tam jest jej centrala. Powiązana z admirał Termią.

Oficjalnie, Arianna i załoga Inferni są ukarani i odsunięci z poważnych rzeczy - zwłaszcza za "Sekrety Orbitera". Dlatego mają lecieć Galaktycznym Tucznikiem, że niby transportują świnie na orbitę. ALE mają dowiedzieć się co tam się dzieje.

Z uwagi na to, że mogą być promieniowania myśli, magowie szukający i próbujący się dowiedzieć - tylko Arianna (arcymag) może o tym wiedzieć przed opuszczeniem Kontrolera. Więc niech Arianna zachowa pokorę i zrobi to skutecznie i dyskretnie. Arianna, oczywiście, się zgodziła.

...

Arianna przekazała oficjalne rozkazy załodze. Załoga się odpowiednio zmartwiła - Elena wpada w depresję, Eustachy zrezygnowany. Ale Klaudia - nie. Klaudia zaczęła próbować przekonywać Ariannę. Arianna powiedziała, że nic nie mogą zrobić, nie są powyżej świń i Klaudia ma to zaakceptować.

Jasne.

Zdaniem Klaudii świnie to zło. Są nudne. Niszczą anomalie. Nie. Po prostu nie. Klaudia poszła do Kamila i powiedziała mu, jaką mają misję. Kamil powiedział, że jeśli Arianna sobie tego życzy to taka misja musi być. Klaudia teatralnie westchnęła. Kamil NIE ROZUMIE.

"Te świnie żrą magię. Są nudne. Zniszczą mi anomalie. Zjedzą nam panią komodor. Zjedzą Ariannę, Kamilu"

Ten argument przeważył. Kamil jest przerażony, że faktycznie Arianna ucierpi przez te glukszwajny i przez tą misję. Teraz to jest sprawa istotna, niebezpieczna i znacząca (V + XXXXX). KAMIL TO ZROBI! KAMIL URATUJE ARIANNĘ! KAMIL ZROBI BUNT!!!

...wait what?

Klaudia zorientowała się, że sprawa wyrwała jej się spod kontroli za późno. Arianna też. Kamil użył swoich kontaktów, swoich umiejętności społecznych i udało mu się zgromadzić sporo załogi Inferni oraz sporo osób, które faktycznie stoją za Arianną i którym Arianna kiedykolwiek pomogła. Te siły przygotowały się do faktycznego buntu przeciwko admiralicji, Orbiterowi i czemukolwiek...

Okazało się, że Orbiter ma dużo większą ilość energii, resentymentów i wszystkiego, czego się nikt nie spodziewał. Kamil skutecznie dał radę zapalić zapałkę nienawiści. Czemu pomagać Trzeciemu Rajowi? Głupi Kramer. Tyle zawdzięczają Ariannie. Tępią Eternię. Nie, wprowadzają tu Eternię. Ogólny chaos.

Arianna WIE, że to się skończy tragedią. Chyba, że ONA coś zrobi...

PROJEKT INIT

* Wstępna pula: 10 * (X)
* Arianna, sama w sobie ze swoimi umiejętnościami: (VVV)

Ale to za mało. Arianna sama nie da rady opanować czegoś takiego. Na szczęście, jest Eustachy! Eustachy zaproponował Ariannie, że pomoże jej zrobić dobre wejście.

"Żeby być dobrym komodorem, trzeba mieć KLASĘ! I dobrą załogę", Eustachy

Eustachy złożył najlepszy show pirotechniczny, godny Eurowizji. Coś, co sprawi, że Ariannę będą słuchać i będą zafascynowani. +(V), +(Xp). Arianna zgodziła się - w tym momencie weźmie wszystko - ale naprawdę, naprawdę boi się, że z tego wyjdzie jakaś straszna żenada. Poprawiło Ariannie humor, że na miejscu będzie też większość kultystów Arianny - osób, które faktycznie jej ufają i w nią wierzą +(V). Dzięki temu ma silniejsze przekonywanie.

Elena zaproponowała Ariannie, że jeśli to kwestia buntu i zachowań niegodnych oficera, Arianna powinna rozważyć uruchomienie Leony. Ariannie się ten pomysł WYBITNIE nie spodobał, ale nie dało się odmówić argumentów Eleny - Leona jest straszna, niebezpieczna i wszyscy się jej boją. Plus Leona słynie z tego, że w odpowiedni sposób traktuje wszystkich którzy nie trzymają się zasad. Więc Leona jest tu perfekcyjna.

Ale to wymaga Martyna. Który to Martyn jest w tej chwili w brygu.

Arianna pomyślała, czy nie skontaktować się z Kramerem - niech on pomoże. Ale nie, nie chce tego robić - Kramer ma teraz straszne problemy w admiralicji, bo jego zaufana Arianna spowodowała te wszystkie problemy. Ale są tam też inni admirałowie - i Arianna chce się zwrócić do admirał Termii, bo jest szansa, że Aleksandra Termia się będzie świetnie bawić kosztem Arianny i Kramera. A to nie będzie wtedy Kramera ekstra kosztować.

Tak więc Arianna odwiedziła admiralicję. Poszła spotkać się z adm. Termią. Ta przywitała Ariannę nieśmiertelnym hasłem:

"O, nasza mała buntowniczka", admirał Aleksandra Termia

Arianna wyjaśniła, że chce rozwiązać problem buntu bez rozlewu krwi. Ona za tym nie stała, ale może to wygasić. Może mniej ludzi ucierpieć, też z jej załogi. Ogólnie, może pójść dużo lepiej - a Aleksandra Termia słynie z tego, że jest skłonna dać szansę i zaryzykować. Arianna wyjaśniła, że chce postawić Leonę, ale do tego celu potrzebny jej wypuszczony z brygu na pewien czas Martyn.

"I wszystko to - cały ten bunt, cała ta intryga - wszystko tylko po to, by mieć z Martynem małe tete a tete w kabinie?!", rozbawiona admirał Aleksandra Termia

Jako jeden z głównych motywatorów Arianna podała Termii: "Myślę, że będzie miała pani z tego dużo dobrej zabawy". Na takie słowa Termia się zgodziła (V) - zwolni Martyna. Co więcej, (V) - Aleksandra Termia uznała działanie Arianny za dobry ruch taktyczny. Kramer nie ucierpi w żadnym stopniu przez to, że Arianna przyszła do Termii...

Kolejnym krokiem było to, że Arianna ustaliła z Termią konsekwencje - część buntowników trafi do kolonii karnej. (+Xp). Dzięki temu Arianna zapewniła sobie kompetentne oczy i załogę w Trzecim Raju - niech ta kolonia będzie tam ;-). Pasuje zarówno Ariannie jak i Termii.

Dobrze, czas wyciągnąć Martyna. Dzięki staraniom Eustachego, Martyn został zamknięty niedaleko nudnego kolesia kolekcjonującego zdjęcia filiżanek... i jest zdewastowany psychicznie. Jak jednak dowiedział się o sytuacji, Martyn postawił się na nogi. Zaprotestował, nie chce budzić Leony. Ale nie ma wyjścia.

"Biedna Leona. Nawet jak jest śmiertelnie ranna i nieprzytomna to jest przydatniejsza niż regiment wojska...", Martyn

Martyn postawił Leonę z pomocą Eustachego, pompując w nią stymulanty i energię plus odpowiedni sprzęt i implanty (ExM):

* V: Napromieniowana, animowana magią Martyna Leona wstała.
* V: (+VVV). Leona, Inkarnacja Terroru w najczystszej formie, awatar terroru. 
* V: Ekipa się już NIGDY nie zbuntuje. ZROZUMIELI Leonę, zwłaszcza po tym, jak ona strzeli w losowego łapczaka w tłumie (by zranić).
* XX: Leona strasznie ucierpiała przez to wszystko. Strasznie. +2 tygodnie leżenia w szpitalu. Ta akcja ją zdewastowała.

Przed samą konfrontacją z buntownikami Arianna potrzebowała jeszcze jednego wsparcia. Poszła do Tadeusza Ursusa.

Tadeuszowi Ursusowi uratowała życie Klaudia. Arianna go odkaziła. Przedtem był ten pojedynek. Ursus WISI Ariannie. Jednak samo spotkanie było ciekawe; Tadeusz był przekonany, że Arianna przyszła mu się "ofiarować", by zapomniał o Elenie. Niestety, nie pomoże. Kocha Elenę. Arianna musiała mieć dziwną minę to słysząc. No, nie, ona przyszła go wykorzystać w polityczny sposób. Ale nie musi mu tego mówić.

* V: SUKCES! Arianna przekonała go, by dał jej się politycznie wykorzystać (+V)
* V: Więcej, Tadeusz ogłosi się prowodyrem rebelii (+V) i weźmie na siebie konsekwencje. Tyle dla Arianny może zrobić.
* V: Admiralicja wie, że to nie on, więc nie dostanie za dużych konsekwencji ze strony Orbitera.
* X: Ale gniew załogi Inferni i buntowników skupi się przeciwko Tadeuszowi, będzie tym "złym manipulatorem".
* X: Tadeusz Ursus dokładnie wie, że Arianna go wrabia. Ale weźmie to na siebie. Mimo wszystko.
* X: Dostanie prawdziwy WPIERNICZ z Eterni. Tam się to jego podejście nie podoba.
* X: Gorzej, bez sojuszników (np. Arianny) arystokraci Eterni go zniszczą. Te czyny pokazały nieakceptowalną słabość.
* V: zbliżył się do Arianny. Będą chętni zostać w sojuszu. A jest lojalny.

Ale to dobra wiadomość. Arianna jest gotowa! [10V, 10X, 2Xp]. Czas na wielkie spotkanie, przemowę i zniszczenie tego co się tu dzieje.

Rozwiązanie puli:

* VV: Tadeusz + epicka klasa Eustachego; fakt, że Tadeusz jest prowodyrem buntu (a nie Arianna) + wejście smoka w wykonaniu Arianny zdewastowało morale buntowników
* VV: A pojawienie się Leony i jej przemowa jak to oni zdradzili wszystko co Arianna reprezentuje + strzał w tłum było superhitem. Zwłaszcza, że widać w jakim Leona jest stanie.
* Xp: Admiralicja żąda krwi. Skończy się na "kolonii karnej" w Trzecim Raju.
* V: Bunt opanowany dzięki charyzmie i epickiej przemowie Arianny.

Arianna jest zadowolona. Ale chce jeszcze odwrócić szkody wyrządzone Kramerowi. Plus, zredukować napięcia między frakcjami Orbitera.

* Xp: krzyżyk żenady. Arianna w maksymalnie siarzasty sposób została uwieczniona...
* X: "Sekrety Orbitera", odcinek specjalny. Chyba Arianna zmonopolizuje to reality show Orbitera...
* X: konsekwencje buntu - Arianna traci dostęp do Inferni przez miesiąc.
* X: podczas buntu i zamieszek gdzieś w brygu ktoś zabił Sebastiana Alariusa. Czyli Kramer miał rację, tu chodziło o coś więcej.
* X: napięcia w Orbiterze między frakcjami są bardzo widoczne. To się rozsypuje. Napięcie ogromne. A Arianna była katalizatorem...

PROJEKT END

...i po tym wszystkim zostaje tylko oddać Martyna strażnikowi w więzieniu, chwilę odpocząć i lecieć Galaktycznym Tucznikiem do świń na planetę...

## Streszczenie

Po pojedynku Kramer opieprzył Ariannę i dał jej dyskretną tajną misję - ma niby w niesławie wozić świnie, naprawdę ma pomóc Orbiterowi rozwiązać spisek w Trzecim Raju. Ale wysłanie Arianny na świnie wywołało bunt na Kontrolerze Pierwszym; Arianna musiała rozpaczliwie deeskalować, nawet budząc Leonę. Udało się bunt opanować, acz wysokim kosztem - pierwszy post-noktiański kapitan nie żyje, załoga Inferni częściowo zesłana będzie do Trzeciego Raju a Arianna miała bardzo żenujące wejście.

## Progresja

* Arianna Verlen: "mała buntowniczka" admirał Aleksandry Termii; ta wredna żmija Ariannę trochę lubi. I dokuczać jej też.
* Arianna Verlen: delikatny sojusz i cień przyjaźni z Tadeuszem Ursusem, eternijskim arystokratą Orbitera.
* Arianna Verlen: KRZYŻYK ŻENADY. Siara. Efektowne wejście smoka. Najbardziej kiczowate działania, ale uwiecznione przez "Sekrety Orbitera"...
* Leona Astrienko: postawiona awaryjnie przez Martyna ucierpiała koszmarnie. Ekstra 2 tygodnie zdjęcia z akcji, bo poniszczona.
* Leona Astrienko: Ikona Terroru, Inkarnacja i Awatar Terroru. Stoi lojalnie za Arianną i będzie jej egzekutorem wobec KAŻDEGO. +999 do zastraszania i dyscyplinowania.
* Tadeusz Ursus: zgromadził na sobie nienawiść buntowników z Inferni i Orbitera, bo "jest prowodyrem" (wziął na siebie by czyścić Ariannę).
* Tadeusz Ursus: sytuacja beznadziejna; pokazał słabość i arystokraci Eterni są zdecydowani go zniszczyć. Bez sojusznika (Arianny?) nie ma szans.
* Tadeusz Ursus: delikatny sojusz i cień przyjaźni z Arianną Verlen.
* Antoni Kramer: straszny cios w reputację i zasoby; bunt spowodowany przez załogę Inferni uderzył w jego silną stronę i pokazał, że Kramer nic nie kontroluje.

### Frakcji

* Orbiter: dużo bardziej niestabilny i rozbity niż się wydaje, nie tylko między admirałami i komodorami, ale ogólnie. Trzyma się do kupy chyba tylko z inercji O_O.

## Zasługi

* Arianna Verlen: nie ona zaczęła bunt, ale ona go musiała zakończyć - od negocjacji z adm Termią, przez manipulację Tadeuszem Ursusem do wielkiej przemowy do buntowników.
* Klaudia Stryk: gdy dowiedziała się, że Arianna będzie wozić świnie (i glukszwajny), zrobiła WSZYSTKO by do tego nie doszło. "WSZYSTKO" skończyło się buntem. Ups.
* Eustachy Korkoran: udowodnił, że nie umie zmusić Martyna i Klaudii do gadania jak nie chcą - ale robi świetną pirotechnikę Ariannie, by ta miała dobre wejście.
* Antoni Kramer: dał Ariannie tajną misję w Trzecim Raju i ukrył to za karą do wożenia świń; strasznie zapłacił gdy część Orbitera się zbuntowała.
* Kamil Lyraczek: podburzony przez Klaudię, by chronić Ariannę doprowadził do buntu na Orbiterze. Klaudia skutecznie ukryła ten fakt przed wszystkimi, by nikt nie wiedział że to on zaczął.
* Martyn Hiwasser: na tyle dobry lekarz, że mimo że był w kiciu to trzeba było go wyciągnąć by postawił Leonę by nie ucierpiała za mocno. Nie chce zdradzić tożsamości damy, którą chronił - nawet, jeśli dzięki temu by wyszedł.
* Aleksandra Termia: admirał. Świetnie się bawi widząc bunt na Kontrolerze Pierwszym i to jeszcze przez Ariannę i Kramera. Pomogła Ariannie wydobyć Martyna by ją sprawdzić.
* Tadeusz Ursus: jeszcze ranny i leży w łóżku, ale już wziął na siebie organizację buntu by osłonić Ariannę. Jest honorowy - chce jej pomóc, bo ona pomogła jemu.
* Sebastian Alarius: kapitan Orbitera noktiańskiego pochodzenia zwykle transportujący glukszwajny między Trzecim Rajem a Orbiterem; poszedł zbyt legalistycznie i Raj ryzykuje śmierć przez brak środków. Niestety, zginął (KIA) w więzieniu podczas zamieszek.
* Leona Astrienko: wybudzona z regeneracyjnej śpiączki przez Martyna by być terrorem buntowników, zapłaciła strasznie zdrowiem, ale pomogła Ariannie opanować bunt. Strzeliła do jednego z buntowników; nic tak nie działa dobrze.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy         

## Czas

* Opóźnienie: 1
* Dni: 6

## Inne

Planowana sesja:

* Admirał Kramer opiernicza Ariannę z uwagi na pojedynek (o czym A. wie) i Martyna (o czym A. nie wie)
* Klaudia + Eustachy dostają informacje o świniach.
* Admirał Kramer tłumaczy Ariannie że z tymi świniami jest ciekawy problem.
* Zespół ląduje Galaktycznym Tucznikiem przy Trzecim Raju
* Zespół ma do czynienia z małym atakiem nojrepów. Widzi w jak złym stanie jest Raj.
...
