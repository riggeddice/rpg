## Metadane

* title: "Kult Saitaera w Neotik"
* threads: legenda-arianny, dotyk-saitaera, spustoszenie
* gm: żółw
* players: fox, kapsel, kić

## Kontynuacja
### Kampanijna

* [211215 - Sklejanie Inferni do kupy](211215-sklejanie-inferni-do-kupy)

### Chronologiczna

* [211215 - Sklejanie Inferni do kupy](211215-sklejanie-inferni-do-kupy)

## Plan sesji
### Co się wydarzyło

* ?

### Co się stanie

* Leona x Diana x Kasandra -> więzienie? Eustachy, test : DONE
* _patrz operacje_

### Sukces graczy

* Inteligentna ewakuacja ludzi?
* Zatrzymanie kaskady Spustoszenia?
* Jakie jest źródło energii ixionu?
* pająkostwór ixioński
* blokada payloadu memetycznego do Inferni / Diany.

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Następny dzień przywitał Zespół w wesołym nastroju. Trzystronna kłótnia - Kasandra, Diana, Leona. Kasandra i Diana chcą aresztować Leonę. Leona nie chce ;-). Eustachy i Arianna proponują odroczyć karę Leony. Kasandra nie wie, że Diana to Anomalia Kosmiczna...

Skanery Inferni wykryły zbliżającą się jednostkę. Tivr. I są kody autoryzacyjne Klaudii.

Arianna -> Kasandra, bo zgłaszała znikających ludzi. Kasandra -> szeregowy Maciej Kalmarzec. Koleś jest przestępcą seksualnym. Polowała na niego. Widziała jakie ma wzory zachowań. I nagle zniknął. Nie ma go dwa dni, myślała że uciekł. Następnego dnia wrócił. I nie jest już przestępcą. Na oko - miragent. Pracuje w okolicach laboratorium medycznego.

Pomysł Eustachego - niech Infernia zje kogoś zainfekowanego. Będzie jak otwarta księga. Klaudia na pewno się nie zgodzi.

Arianna robi SPEECH do załogi. Podnosi morale. Integruje ich. Iza na wzmocnienie.

"Ostatnimi czasy Infernia przeszła duże trudności. Zaczęliśmy od sektoru Mevilig, potwór Esuriit. Natrafiliśmy na potwora ixiońskiego. W tym momencie jesteśmy... mamy kryzys. Musimy zadać sobie pytanie po co dołączymy do załogi Inferni. Zawsze prestiżowa jednostka. Być tu to zaszczyt. Ale czemu tak było? Walczymy z potworami. Trudności, z którymi inni sobie nie radzą. Czasem walka z tym pochłania więcej zasobów. Musimy spojrzeć w otchłań by pokonać zło w niej się znające. W obliczu walki tracimy ludzi. Ludzie pożerani przez różne byty. Nie odżałujemy tych którzy zginęli przy infekcji ixiońskiej i przy poprzednich zadaniach. Leotis, Cień Nocnej Krypty, Zona Tres. Musimy pogodzić się ze stratami, pozostać dalej zjednoczeni by móc pokonać mimo tego co musieliśmy poświęcić. Proszę Was o dużo, ale musimy zostać zjednoczeni - i na tej stacji jest potwór ixioński któremu musimy stawić czoła. Ten jeden raz - za mną, u mego boku - pokonamy potwora. Potem wolny wybór. A ci co chcą wrócić do Orbitera - mogą. Inni - Tivr. Ale jak najwięcej zdecyduje się zostać i walczyć na pokładzie naszej wspaniałej jednostki. Wszyscy się przyzwyczaimy i oswoimy Dianę. Infernię. Nasz statek, na którym zawsze musimy polegać."

TrZ (Maria i jej feromony) +4:

* V: zostaną i dopiero potem podejmą decyzję
* X: unikalna kultura Inferni wrze
* V: w większości zostaną na dłużej
* X: kult Zbawiciela-Niszczyciela rozgorzał. Arianna - Eustachy.
* X: załoganci AKTYWNIE rozprzestrzeniają kult Zbawiciela - Niszczyciela. Infernia we flocie staje się "dziwna".

Klaudia zadokowała. Potem krótki spacer kosmiczny. Infernia przedtem "wywietrzyła", żeby nie było śladów. Plus Maria kazała dodawać do jedzenia XD. Klaudia wzięła z K1 poza zestawem standardowych zapasów (których na pewno brakuje Inferni) coś jeszcze. Klaudia odezwała się do Mariana Tosena - czy nie ma / nie zna czegoś na potwora ixiońskiego. Coś w stylu Serenita. Klaudia dostała eksperymentalną broń. To bydlę jest jak wielki jetpack. Nieporęczne. Wielkie. Wali strumieniem. Wymaga "zaparcia się" i ładowania.

Elena chodziła po stacji i mapowała gdzie jak się czuje i co potrafi wykryć. Nadal jest słaba, nie do końca rozumie co czuje, ale jest skuteczna. Arianna chce ją uziemić, by ta mogła się poruszać po stacji nie dotykana energią. Klaudia odmawia użycia odłamka Serenita. Lepiej użyć Inferni i przekonać Eustachego. 

Eustachy -> Diana: ocalimy wielu ludzi. Eustachy: "ona jest 'zła' przeze mnie". "gdyby nie ona to bym nie żył" - i naświetla sytuację z tym jak była w obszarze Skażonym i otworzył skafander. Diana jest skonfliktowana. "Nie każ mi tego robić". "Nie każę, ja cię proszę". "A nie mógłbyś po prostu kazać?" "Potraktuj to jako lekcję życia."

Tr (niepełny) Z (gorące serce) +4 (relacja + historia) - i Eustachy wygrzebuje wszystkie rzeczy...:

* V: Diana się zgodzi.
* V: Diana CHCE współpracować z Eustachym a nie tylko słuchać
* X: Diana po prostu, po prostu Eleny nie toleruje. "Za młoda, za niedojrzała"

.

* Elena -> Arianna: Ty chyba żartujesz.
* Arianna -> Elena: potrzebujemy dokładnego obrazu. To nie jest kwestia sympatii.
* Elena -> Arianna: my się pozabijamy
* Arianna -> Elena: nie zobaczysz, potwór Cię pokona i pożre. A nad Infernią mamy kontrolę.

Arianna linkuje Elenę i Infernię. Infernia jako uziemienie dla Eleny. Klaudia zarządza, Eustachy stabilizuje Dianę i Infernię, Arianna steruje energią, Elena zaciska ząbki.

ExZM+3:

* Vz: Link udał się perfekcyjnie. Elena przekierowuje energię w Infernię.
* Vz: jest możliwość "emergency link breakdown". Czyli Infernia nie może zostać Skażona przez ixiońskiego potwora na stacji.

Elena lekko błędnymi oczami patrzy na Ariannę. "Mam czujniki. Boli mnie silnik. Mam... dużo... dziwnych odczuć." Ale mogę przekierować energię, pasywnie ją przekierowuję.

Teraz - Klaudia nadaje dostępy Elenie. A Eustachy flirtuje z Kasandrą by nikt nie widział co można zrobić ich stacji. Adam używa swoich kodów itp.

TrZ (kody Adama + backdoor) +3:

* X: Elena jest wykrywalna przez ludzi znających niektóre niebezpieczne miejsca
* V: Hestia daje uprawnienia
* Vz: Hestia aktywnie odblokowuje itp.
* V: Operacja niewidzialna dla Hestii. Niby ixiońskie AI... ale AI.

Elena infiltruje stację. W Eidolonie. Mając uziemienie w Inferni.

TrZ (Eidolon + działania Klaudii) +4 (przygotowania + brak presji czasowej):

* V: Elena ma wysokopoziomowy obraz sytuacji i pewne wnioski.
* V: Elena usłyszała coś niepokojącego. I przesłała to przez Infernię. SAITAER. On tu jest.
* Vz: Elena zmapowała ludzi. Co się dzieje z nimi.
    * Elena ma pewne wyczucie. Jak jest blisko, potrafi wyczuć. To NIE SĄ miragenty. To są ludzie z specyficznym neurosprzężeniem. Łączy ich z... Hestią. A Hestia też jest macką. Spustoszenie.
    * Takie coś potrzebuje ogromnej ilości energii. Więc na pewno reaktor ma przyłącze (złodziejka). I idąc od tej złodziejki - centrum Skażenia
* X: Diana majaczy. Nie do końca kontaktuje, bo stężenie energii i woli kultystów Saitaera jest za duże.

Elena wróciła. Jest dość osłabiona, ale całkowicie stabilna. Diana, jak tylko Elena się oddaliła, to Diana jest stabilna. Ale jest roztrzęsiona. I - co dziwne - nadal jest przekonana, że jest silniejsza. "Skoro nawet TA LASKA to przetrwała to ja to zjem."

Na Tivrze przylecieli też komandosi Verlenów. 5 osób. 2 cybersprzężonych, 2 zwykłych i Otto.

Nereida nie jest jednostką Saitaera. Ta druga Nereida jest "czysta".

Na PEWNO przeciwnik chce dostać Infernię w swoje łapki, ale nie chce się dać zdekonspirować.

POTENCJALNIE:

1. Nereida dla Eleny
2. Adam, Kasandra. Czego od nich oczekiwać.
3. Mechanicy. Infernia się z trudem sama zregeneruje. 
4. Koloidowe statki. Tivr jest szybszy. Tivr nie zobaczy - ale Infernia tak, zwłaszcza z sensorami
5. Spustoszenie / potwór w Stoczni Neotik
6. Hestia - wszechoczy Spustoszenia
7. Kult Saitaera
8. Czy broń, którą przywiozła Klaudia zadziała.
9. Wiemy, że byty ixiońskie chcą zdobyć Infernię.

-> Adam, Kasandra: mamy ogląd sytuacji. Szukamy informacji o koloidowych jednostkach. Przetestujemy działo. I dopiero wtedy dalsze decyzje.

Czyli cel: zdobyć Nereidę dla Eleny, przygotować ćwiczenia, złapać / usunąć statek koloidowy.

Arianna informuje Adama Szarjana o planie. W skrócie:

* bierzemy Nereidę na próbny lot, z Tivrem
* łączymy z czujnikami
* szukamy statku koloidowego
* przypadkiem zestrzelimy + dziwne działo
* dalsze decyzje
* nasza zwłoka nie rozwije ixionu bo nie mają mocy - póki w ukryciu póty byty ixiońskie nie będą miały się jak rozprzestrzeniać (po co + prąd)

Klaudia dostaje info od Kasandry (kiedy ludzie się zmieniają), zapisy ze stoczni. Klaudia bierze Elenę (jako technika), wydobywa od Adama Szarjana, z logów technicznych stacji. Kiedy pojawiły się pierwsze fluktuacje. Zawahania w systemach. Może uda się określić co było przejęte. A Elena tam była.

TrZ (szerokie źródła danych) +3:

* X: nie da się DOKŁADNIE określić
* Xz: dane zostały odpowiednio - za dużo sprzecznych sygnałów. By dostać dokładną odpowiedź musimy mieć próbkę.
* Vz: Klaudia doszła do tego po typach backupów i dziwnych backupach. Hestia została stracona jakieś pół roku temu - da się skorelować z dziwną awarią anomaliczną na Netrahinie (Saabar).

Pół roku oznacza, że ten temat się POWOLI rozwija. Czyli przeciwnik czeka.

Klaudia będzie chciała zdobyć listę jednostek które zostały tu zbudowane. Jakie jednostki mogą być zarażone / specjalne.

TrZ (szerokie źródła danych) +3:

* Vz: Klaudia ma listę jednostek które interaktowały z tą stocznią.
* V: Klaudia ma listę jednostek PODEJRZANYCH.
    * Netrahina jest jedną z tych jednostek. I Netrahina ma pecha.
    * Część z tych jednostek to potencjalnie corruptory - ta lista MUSI trafić do Kramera.

Arianna -> Szarjan. Powiedziała plan. I Elena ma Eidolona sentisprzężonego + używała sentisprzężonego statku. Ma expa.

TrZ (już Szarjanowi udowodniła) +4 (gravity of situation):

* V: Szarjan stoi za przekazaniem Nereidy na testy Elenie. To znaczy, że to się stanie.
* X: jest nieufny bo nie wie co się dzieje; Infernia nie jest normalną jednostką a kult lolitki przyprawia go o dreszcze. Nie skojarzył jeszcze że "Aria Vigilus" to Arianna.
* Vz: Szarjan w pełni współpracuje z Arianną i oddaje jej się pod komendę nieformalnie.

Adam przekonuje, że dla pełni testu i dla stabilizacji psychotroniki Nereidy Nereida musi być uzbrojona w prawdziwą broń. Badania Nereidy #1 wykazały, że elementem psychotycznego szoku było to, że Nereida była bezbronna i wiedziała o tym. "Nie ma nadziei, nic nie mogę zrobić". Klaudia przedstawiła odpowiednie nagranie.

TrZ (jego status) +2:

* V: Bez komentarza, bez pytania, Nereida została właściwie uzbrojona. Eustachy ma co wzmacniać potem

NEXT ONE: "Ups, to tam był statek? XD"

## Streszczenie

Klaudia wróciła z bronią anty-Serenitową od Tosena, Arianna z feromonami Marii nadała Inferni nową kulturę - miłośnicy lolitek x kultyści x noktianie. Elena uziemiona w Inferni zinfiltrowała Stocznię ukryta przez Klaudię i odkryła niższe Spustoszenie + kult Saitaera. Nereida jest czysta od Saitaera; przetransferowana na Infernię dla Eleny i uzbrojona. Klaudia ma listę jednostek potencjalnie w rękach Saitaera / Spustoszonych. Przygotowania do kontrataku skończone.

## Progresja

* Arianna Verlen: aspekt Vigilusa (Vigilus - Nihilus).Załoga i Kult jest przekonana, że jest Aspektem Zbawiciela.
* Eustachy Korkoran: aspekt Nihilusa (Vigilus - Nihilus). Załoga i Kult jest przekonana, że jest Aspektem Niszczyciela.
* Klaudia Stryk: ma ze Stoczni Neotik listę jednostek podejrzanych o to, że zostały zarażone albo przez Kult Saitaera albo przez niższe Spustoszenie.
* Diana d'Infernia: absolutnie nie znosi Eleny. Nie chce z nią współpracować, pomagać itp. Tylko na wyraźny rozkaz Eustachego.
* Diana d'Infernia: aktywnie CHCE pomóc Eustachemu. Nie wykonuje rozkazów ze strachu, ale też bo chce.
* OO Infernia: Nereida przekazana do Inferni jako jednostka eksperymentalna którą ma sterować Elena. Tymczasowo.
* OO Infernia: unikalna kultura: noctis x kult Vigilus - Nihilus x miłośnicy Diany - lolitki z dziwnymi żartami. Stworzona przez syntezę mowy Arianny, zdolności Izy i feromonów Marii.

### Frakcji

* .

## Zasługi

* Arianna Verlen: przemowa + feromony Marii zmieniły kulturę Inferni w dziwną; uziemiła Elenę w Inferni by ta mogła zinfiltrować Stocznię i zaprojektowała plan - przekazać Nereidę Elenie i zdobyć koloidowy statek.
* Eustachy Korkoran: przekonał Dianę, że Elena musi być uziemiona w Inferni i sprawił, że Diana CHCE współpracować z Eustachym. Bardzo zajmował się Dianą by ją stabilizować i usprawnić.
* Klaudia Stryk: od Tosena pozyskała eksperymentalną broń ręczną anty-Serenitową do walki z ixionem; prowadziła link Eleny x Inferni, ukryła Elenę przed Hestią d'Neotik i pozyskała listę potencjalnie Skażonych jednostek interaktujących z Neotik.
* Kasandra Destrukcja Diakon: chce aresztować Leonę; daje się jednak przekonać Ariannie, że dużo ważniejszy jest ixioński potwór na Neotik.
* Izabela Zarantel: pomaga Ariannie w odpowiednim natężeniem efektowności by zrobić najlepszy speech ever.
* Maciek Kalmarzec: Neotik; przestępca seksualny, najpewniej zdaniem Kasandry miragent? Coś z nim nie tak, pracuje przy medlab.
* Maria Naavas: jej feromony pomogły w mindwarpowaniu przez Ariannę i Izę załogi Inferni i zbudowaniu jej nowej, unikalnej kultury (kult x pro-lolitka x noktianie).
* Elena Verlen: uziemiona przez Klaudię i Ariannę w Inferni zinfiltrowała Stocznię Neotik i odkryła Kult Saitaera oraz niższe Spustoszenie.
* Lutus Amerin: tymczasowy kapitan Tivr, kiedyś noktianin. Przyleciał z Klaudią Tivrem na Neotik by wesprzeć Ariannę.
* Adam Szarjan: stoi za przekazaniem Nereidy na testy Elenie; w pełni współpracuje z Arianną i oddaje jej się pod komendę nieformalnie. Przekonał Stocznię, że Nereidę przekazaną Elenie należy prawidłowo uzbroić.
* Diana d'Infernia: nadal jest przekonana, że jest silniejsza niż ixioński potwór Saitaera na Neotik. "Skoro nawet TA LASKA (Elena) to przetrwała to ja to zjem.". Chce współpracować z Eustachym. "Kochana lolitka Inferni".

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Pierścień Zewnętrzny
                1. Stocznia Neotik: zawiera Kult Saitaera i niższe Spustoszenie; zinfiltrowana przez Elenę.
 
## Czas

* Opóźnienie: 1
* Dni: 1

## Konflikty

* 1 - Arianna robi SPEECH do załogi. Podnosi morale. Integruje ich. Iza na wzmocnienie.
    * TrZ (Maria i jej feromony) +4
    * VXVXX: zostaną, w większości na dłużej, unikalna kultura Inferni: Arianna: zbawiciel, Eustachy: niszczyciel, załoganci AKTYWNIE rozprzestrzeniają kult Zbawiciela - Niszczyciela. Infernia we flocie staje się "dziwna"
* 2 - Eustachy -> Diana: ocalimy wielu ludzi. Eustachy: "ona jest 'zła' przeze mnie". "gdyby nie ona to bym nie żył". Chce Dianę przekonać, by ta pomogła Elenie się w niej uziemić.
    * Tr (niepełny) Z (gorące serce) +4 (relacja + historia) - i Eustachy wygrzebuje wszystkie rzeczy...:
    * VVX: Diana się zgodzi, współpracuje z Eustachym a nie tylko słucha. Ale nie znosi Eleny.
* 3 - Arianna linkuje Elenę i Infernię. Infernia jako uziemienie dla Eleny. Klaudia zarządza, Eustachy stabilizuje Dianę i Infernię, Arianna steruje energią, Elena zaciska ząbki.
    * ExZM+3
    * VzVz: link perfekcyjny; Elena uziemiona w Inferni i możliwość "emergency link breakdown". Czyli Infernia jest bezpieczna od ixiońskiego potwora.
* 4 - Teraz - Klaudia nadaje dostępy Elenie. A Eustachy flirtuje z Kasandrą by nikt nie widział co można zrobić ich stacji. Adam używa swoich kodów itp.
    * TrZ (kody Adama + backdoor) +3
    * XVVzV: Elena jest wykrywalna przez ludzi, Hestia daje uprawnienia, aktywnie odblokowuje itp. Operacja niewidzialna dla Hestii.
* 5 - Elena infiltruje stację. W Eidolonie. Mając uziemienie w Inferni.
    * TrZ (Eidolon + działania Klaudii) +4 (przygotowania + brak presji czasowej)
    * VVVzX: Elena ma wysokopoziomowy obraz, wie o Saitaerze, zmapowała ludzi - Spustoszenie niższego stopnia. Diana majaczy; przerywamy operację.
* 6 - Klaudia dostaje info od Kasandry, zapisy ze stoczni. Klaudia + Elena (technik), wydobywa od Adama Szarjana, z logów technicznych stacji. Kiedy pojawiły się pierwsze fluktuacje. Co przejęte?
    * TrZ (szerokie źródła danych) +3
    * XXzVz: nie da się DOKŁADNIE określić bez próbki; ale po backupach - Hestia stracona pół roku temu. Skorelować z awarią na Netrahinie / Saabar z Mychainee.
* 7 - Klaudia będzie chciała zdobyć listę jednostek które zostały tu zbudowane. Jakie jednostki mogą być zarażone / specjalne.
    * TrZ (szerokie źródła danych) +3
    * VzV: Klaudia ma listę podejrzanych jednostek; m.in. Netrahina. Ta lista MUSI trafić do Kramera.
* 8 - Arianna -> Szarjan. Powiedziała plan. I Elena ma Eidolona sentisprzężonego + używała sentisprzężonego statku. Ma expa.
    * TrZ (już Szarjanowi udowodniła) +4 (gravity of situation)
    * VxVz: Szarjan przekaże uzbrojoną Nereidę Elenie na testy, ale jest bardzo nieufny. Oddaje się Ariannie pod komendę.
* 9 - Adam przekonuje, że dla pełni testu i dla stabilizacji psychonitroniki Nereidy Nereida musi być uzbrojona w prawdziwą broń.
    * TrZ (jego status) +2
    * V: Bez komentarza, bez pytania, Nereida została właściwie uzbrojona. Eustachy ma co wzmacniać potem.
