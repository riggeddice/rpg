## Metadane

* title: "Karawana Lustrzanej Wody"
* threads: brak
* motives: energia-alucis, bardzo-grozne-potwory, the-doppelganger, misja-ratunkowa
* gm: żółw
* players: kić, fox

## Kontynuacja
### Kampanijna

* [250108 - Straszny Wilk spod Przygrzybka Górskiego](250108-straszny-wilk-spod-przygrzybka-gorskiego)

### Chronologiczna

* [250108 - Straszny Wilk spod Przygrzybka Górskiego](250108-straszny-wilk-spod-przygrzybka-gorskiego)

## Plan sesji
### Projekt Wizji Sesji

* INSPIRACJE
    * PIOSENKA WIODĄCA
        * brak
            * .
    * Inspiracje inne
        * Walka z bardzo niebezpiecznym mentalnym potworem.
* Konwencja
    * Niebezpieczny Anomalny Przelotyk
        * Jest tu potwór. Dzieje się coś złego.
        * Bardzo niebezpieczni przeciwnicy, detale mają znaczenie.
        * Postacie Graczy: raczej bezpieczne, chyba, że zrobią coś nierozsądnego; bardzo silne.
* Opowieść o (Theme and Vision):
    * "_Czasami próba ratowania ludzi wiąże się ze śmiertelnym niebezpieczeństwem, zwłaszcza w Przelotyku_"
        * Karawana przyszła pomóc ludziom. Niestety, to był potwór Alucis.
    * Lokalizacja: Przelotyk
* Motive-split
    * energia-alucis: Zakłócenia mentalne pochodzące z potwora; ludzie nie widzą potwora tylko swoje sny, a Lustro Wody powoli ich pożera.
    * bardzo-grozne-potwory: Lustro Wody Alucis, który zakłóca myślenie i zniekształca rzeczywistość dla ofiar. A potem spawnuje Doppelgangery.
    * the-doppelganger: Adrian - "wodna" istota Alucis zastępująca powoli ludzi i infekująca ludzi w Karawanie.
    * misja-ratunkowa: Karawana ma problem, bo wpakowała się w pułapkę Potwora Alucis, 'ensnarera'. Nie do końca wiedzą co się dzieje i są powoli przejmowani przez The Doppelganger.
* O co grają Gracze?
    * Sukces:
        * .
    * Porażka: 
        * .
* O co gra MG?
    * Highlevel
        * .
    * Co uzyskać fabularnie
        * .
* Agendy
    * .

### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

KONTEKST:

* 

CHRONOLOGIA:

* jak powyżej

PRZECIWNIK:

* Lustro Wody

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

* .

### Fazy sesji

FAZA 1: Trudny teren, ratujemy ludzi którzy zaginęli w jaskini

* Tor Snu Alucis: 0-6, skok 2, 10 min ea.
* Tor ratowania kilkunastu ludzi: 0-10
* Licznik Czasu: 0-20

Inne:

* Mamy obozowisko ratowników, ale oni też zaczynają odpływać przez Alucis.
* A w jaskini są fantazmaty, istoty szepczące

Są też Zastąpniki Alucis, zajmujące miejsca ludzi.

## Sesja - analiza
### Fiszki

* .Brygida Verlen:
    * Perfekcja Człowieczeństwa: perfekcja Sprawy, inkwizytorka i niszczycielka zła i Skażenia, perfekcyjna w walce i elegancka.
    * Praecis - Exemplis materializuje się jako "wszystko, co jest nieperfekcyjne i szkodliwe na naszej drodze zostanie zniszczone". Potężna czarodziejka. Puryfikatorka + Ogień.
    * Arogancka i ŚWIETNA w tym co robi. Śliczna, nietknięta wizualnie. Exemplar Verlenów.
* Mateusz Orkliacz: Bardzo spokojny i bardzo kompetentny kapral Verlenlandu. Marzyciel: [O+ C0 E- A+ N-].

***
***

### Scena Zero - impl

.

### Sesja Właściwa - impl

Zespół wraca z Przelotyka w okolice Verlenlandu. Przechwytuje wiadomość SOS. "Pod ziemią"

Viorika i Arianna mają towarzystwo oddziału, jakieś 8 osób. A+V najpewniej poruszają się ścigaczami, oddział w APC. 

Ludzie z Przygrzybka powiedzieli że TAK, tam jest dawny kamieniołom. Ale z uwagi na obecność podziemnej rzeki został opuszczony. Czasem się zagłębiają tam ludzie szukający łupu. Ale rzadko - bo to jest podobno niebezpieczne miejsce.

Mały zwiad - Verlenki przodem, APC tyłem, jak standardowo.

Viorika robi zwiad. Ostatnio był ixient w okolicach Przygrzybka, więc kto wie co tam się dzieje? Szuka śladów ixientów i innych śladów magii, karawany. Czy są tam ludzie. Itp.

Ex Z +3:

* PRAWDA:
    * Teren wygląda na nie-dotknięty ixiońsko. Wygląda "typowo", jeżeli można o Przelotyku
    * Karawana tam jest. Jest przy kamieniołomie. Ale też są ślady, że mniej więcej JECHALI. Skręciła na kamieniołom, co jest mniej typowe.
    * Wiadomość dobiega z karawany.
    * Ludzie z karawany nie zauważyli Vioriki ani drony, są rozłożeni obronnie, ale _kiepsko_.
* X: Vioriką zainteresował się drapieżny latający potwór, interient, reprezentowany przez burzę.
    * Lecąca w stronę Vioriki potężna chmura burzowa. Nie jest bardzo szybki.
* Vz:
    * Kolejny pass, kolejne obserwacje pokazały coś ciekawego - jest _sporo_ istot w okolicy. Te istoty nie atakują. Nie ma w nich agresji.
    * Istoty w okolicy, potwory, jest _"bardzo spokojnie"_.

Arianna i Viorika zrobiły "swoją" chmurę burzowę i wpakowały w to flary itp. By odstraszyć drapieżnika. (+kategoria)

* X: DRAPIEŻNIK JEST ZAINTERESOWANY. Sezon godowy? Tak, drapieżnik interesuje się "chmurą". Zaczął walić błyskawicami. Zaczął się "prężyć".

Verlenkom udało się "odprowadzić" chmurę i drapieżnika. Ale drapieżnik ma filuterny, godowy okres, goni. To _NIE_ jest coś, czego spodziewałyby się po drapieżnikach w okolicy. Czyżby wpływ Alucis? Chciał zobaczyć partnerkę swojego gatunku, zobaczył partnerkę swojego gatunku...

Po odrobinie manewrowania wylądowały przy Karawanie. Ale poza zasięgiem działek - na wszelki wypadek.

Licznik Czasu: 2.

Viorika wchodzi do Karawany. Arianna ubezpiecza. Viorika - długo jej nie zauważyli. Są... są kiepskiej natury. Robią co powinni, ale powoli, ospale, jak... we mgle. Jeden, bardziej kumaty pokazał palcem. Inny spojrzał i nie do końca sensownie działa. Zbiera się ekspedycja do zejścia w podziemia.

Ci ludzie są rozespani. Jak "naćpani". Ktoś jest w kamieniołomie. Ten człowiek.

* Viorika: Czemu zboczyliście z trasy.
* Adrian: Nazywam się Adrian. 
* Viorika: (miarkuje się), plaskacz by obudzić.

Ex +3:

* X: "Adrian" się nie zdestabilizował.

Adrian padł na ziemię. Otrząsnął się. "Pomogliśmy im. Znaczy nie pomogliśmy. Próbowaliśmy. Nie bij mnie (prawdziwe uczucie!)"

Adrian się JAKOŚ trzyma. Powiedział - jest kilkanaście osób w jaskini. Część z ludzi ma dzieci. Te dzieci są w karawanie (tak, te dzieci tu są).

Drugi człowiek, Rafał, podszedł do Adriana i Vioriki. "Pocałuj mnie". Adrian odepchnął Rafała.

Arianna kazała zabezpieczyć chemicznie swoich ludzi. Jeśli to nie pole magiczne... to chemia?

Wszyscy są raczej uśmiechnięci i zadowoleni.

Interis-Alucis jest, ale za słabe. Ci ludzie wyglądają jak Skażeni Alucis, ale nie jak pod wpływem stałego pola. Są błodzy.

Grupa która ma zejść w dół ma... wątpliwości.

Radiostacja Karawany. Ona działa, ona jest w pętli, ona ma nagraną wiadomość. To głos Adriana.

Adrian ma problem by powiedzieć kiedy przyszli, jaki czas... on się zachowuje jakby nie do końca wie co się dzieje.

Arianna patrzy na maszyny. To co Adrian mówi **nie ma sensu**.

Viorika sprawdza rzeczy w obozie. Arianna kieruje ludzi że są rzeczy do zrobienia. Oni - są ULTRA podatni na każdą sugestię. Arianna - słyszy też głosy z jaskini. WIDZI w oddali jedną kobietę. Nie ma światła, tylko sylwetka. Ale jest tam. Porusza się, ale trochę się kołysze. Coś mamrocze. Arianna wzmacnia głośniki kierunkowe i słyszy: "Piętnaście jabłuszek i odrobina cukru... szarloteczka, smaczniutka.... Pawełek tak się ucieszy..." (schyla się, coś podnosi z ziemi i wkłada to do ust).

Arianna bierze miskę, układa, że to niby "talerz z jedzeniem", szarlotka na talerzu.

Tr +4:

* Vr: Kobieta szlochnęła, wybiega z jaskini, potykając się i uderzając, taka jak pijana.

Arianna widzi - w złym stanie. Odwodniona, osłabiona. Części zębów nie ma, ale rzuca się zjeść "szarlotkę" i mamrocze coś. Arianna pozwoli jej żuć materiał. Widzi wyraźny znak Skażenia Alucis. Jest wilgotna; Alucis poszła z wody.

TYMCZASEM Viorika. Czy ci ludzie mają zapiski - coś z wodą? Gdzie pobierali wodę? Czy wodę pobrali stąd? Skąd mieli wodę.

Tr Z+4:

* X: Te parametry na tyle nie mają sensu, że V dwa razy to sprawdziła. Tu - to kwestia czasu.
* V:
    * Ta Karawana jest tu od ponad 24h. SOS nie jest tu od 24h. Zgodnie z logami - od 3h.
    * Zgodnie z logami - mają wodę SWOJĄ i mają zasady by nie pić wody "znikąd". Są przygotowani jak sensowna Karawana.
    * Ci ludzie faktycznie zboczyli, bo były sygnały SOS z kamieniołomu. I szli procedurą. Ale jak pierwsza grupa weszła (liny, komunikatory). Każdy widzi kogoś. Ale to zawiodło.
    * "Myśleliśmy, że mamy kontakt, ale nie mieliśmy"
    * Potem kombinowali co z tym robić. Serio nie wiedzą. Więc przygotowali - LINY, pełen sprzęt. Powiązać się linami.
    * To jest ostatni zapis. Doszło do... tak jakby bomba Alucis. Albo "coś".

Arianna próbuje dojść do tego, czy gejzer wybuchł. Czy coś się stało.

* X:
    * PRAWDA:
        * Na pewno była jakaś forma gejzera. Coś, co emituje raz na jakiś czas. To wpływa na anomalne byty, na potwory - i stąd widać, że tu jest "spokojniej"
        * Kamieniołom był zrobiony w miejscu, gdzie tego nie było. "Prefabrykowane miasto duchów". Ale Prefabrykaty zostawiają kiepskie ruiny.
        * WIEMY że to nie jest siła naturalna z kamieniołomu.
        * NIE, to nie jest tak, że teren jest wilgotny. Może - ktoś porozchlapywał.
    * Nie da się więcej wydobyć.

Licznik Czasu: 4.

Viorika desygnuje żołnierza praktykanta. On jest królikiem doświadczalnym. On ma się zbliżyć i utrzymywać stały kontakt z APC, gdzie jest medyk. Ale ma w osobistym pancerzu i masce, nie ma hermetyzacji, czy się Skazi czy nie. I tak kilkanaście minut. I ma mieć coś srebrnego. Po kilkunastu minutach go nie parzy.

Arianna próbuje wyciągnąć coś z tej "kobiety z szarlotką". Spróbować pójść po linii "gdzie rosły najlepsze jabłka, z kim szarlotki". By zostać przy szarlotkach. Co ona mówi co się mogło dziać. Co widziała PRZED szarlotką. Tego typu rzeczy.

Ex Z +4:

* Vr: "Kobieta z szarlotką"
    * Ona tam zeszła. Nie wie co było w jaskini.
    * Ona potwierdza historię. To jest prawda. WIECIE - jaskinia nawarstwia Alucis.
    * Ona chciała wrócić do jaskini ale musiała wyjść.
    * "Tańczył na tafli wody i był taki piękny".
* Vz: Ona powiedziała więcej o detalach
    * "Tafla lustrzanej wody, lustro, lustrzana woda".
    * Tańczyło lustro, figura, piękna. On był taki piękny, ale to ja tańczyłam. Tańczyłam z nim.
    * To nie ona znalazła lustro wody. To lustro wody znalazło ją.
    * "Gdy byłam gotowa, znalazła mnie."

Viorika chce walnąć serią stymulantów w Adriana. To ciężka bateria stymulantów a on może coś powiedzieć. Viorika do oddziału "pilnować się wzajemnie".

Adrian panikuje by nie dostać strzykawką. JEGO A NIE MNIE w stronę Rafała.

Tr Z+3 +3Oy:

* V: Adrian się wyrywał i szamotał. Ale serwopancerz to serwopancerz. Lekarz wsadził mu BATERIĘ ŚRODKÓW.

...Adrian zaczął się _destabilizować_. Chce UCIEC, nie chce rzucać się na nikogo. W tej chwili nie celuje by na kogoś się rzucić. Raczej - ucieka. W kierunku na kamieniołom.

* V: Arianna rzuciła się w swoim serwopancerzu. Epicki _leap_, zgarniając z ziemi. Nabrała go miską. To jest... spoista ciecz. Spójny glut.

Żołnierz + neutralizator biohazard. A Arianna na kotwiczkach a glut nie ma jak się dobrze sformować. (+2 Vg)

* X: Glut zatruty traci spójność i się rozchlapuje. Nadal - nie ma ludzi w pobliżu a to co odpadło nie rusza się. Wyraźnie degeneruje.
* V: Glut zamknięty w _biohazard container_.

Arianna ma gluta.

Łatwo zbadać czy "glut" Skaził wodę. Prosty test - TAK, zapasy wody i żywności są Skażone. To jest WEKTOR Alucis. Czyli oni spożywali "potwora".

Licznik Czasu: 7.

Zgodnie z manifestem, zniknęło kilkanaście osób. I nie wiemy kto kiedy został Skażony.

Verlenki kazały żołnierzom zamknąć ludzi. Plan zarządzania kryzysem:

* Dzieci śpią i nic nie robią, izolacja.
* Zamykamy ich w RÓŻNYCH grupach, by była grupa-po-grupie.
* Żołnierze pilnują i wejścia i Verlenek. Backup.
* Praktykant odwozi trochę do przodu.
* "Może być potrzebna akcja ratunkowa, jest czas po którym macie zejść".

Verlenki mają odpowiednią dawkę chemikaliów.

Arianna ma pomysł na narkotyk, za pomocą którego "wejdą w fazę". Ale nie mają kompetencji by zrobić taki narkotyk.

Czas iść na polowanie...

CEL: ratujemy ludzi. Nie polujemy na potwora.

Słychać głosy z jaskini. Wiadomo, że gdzieś ci ludzie tam są. JAK możecie poradzić sobie - jak można odnaleźć ludzi w takich jaskiniach? Viorika wysyła dronę. I liczy matematycznie echa by znaleźć gdzie są ludzie. Viorika skupia się na tych "łatwych do znalezienia".

Tp +4 +3Og:

* Og: Jaskinie... _wyglądają pięknie_. Są naprawdę, naprawdę _ładne_. ALE drona daje sygnały. Viorika prawie weszła na ścianę której nie widziała.
    * Viorika ustawia w servarze "auto-override za jakiś czas".
* Vr: Udało Wam się znaleźć 3 wynędzniałe osoby. Tak serio, to Wam trochę migocze...
    * Verlenki KAŻDEGO traktują strzałką. Jeden wydał _nieludzki dźwięk_ i zaczął uciekać.
    * Jeden facet zaczął płakać, załamany.
* V: Udało się wydobyć jeszcze 3 osoby. Już pięć z kilkunastu. Ale już trzeba się cofać.

Verlenki się wycofują. Przekazują żołnierzom, idą dalej.

* Licznik Czasu: 10/20
* Tor ratowania kilkunastu ludzi: 5/10
* Skażenie: 1

Jesteście w Korytarzach Alucis. Na pierwszy rzut nie widać ludzi "łatwych do dostania sie". Ale - drona zlokalizowała niewielki tunel z opcją częściowo w kanale wodnym.

Gdy zbliżyły się do strumienia - strumień jest lekko Skażony ale nie jest wektorem Skażenia. Ten strumień ma nutę Interis.

Verlenki się kłócą - która idzie, która zostaje. "Księżniczka musi świecić przykładem, ja idę!" "A jak ja będę wyglądać jak Cię puszczę!"

Viorika jest kompetencyjnie silniejsza. Ona tam wchodzi. W jaskinię. W wodę. I chcemy KOGOŚ wyciągnąć, niekoniecznie wszystkich.

Tp +4 +5Og:

* V: Viorice udało się przedostać na drugą stronę tunelu. Drona od razu ruszyła. Jaskinia się rozszerza. "Fajne miejsce", ono jest ukształtowane płynem. Ta strona nie wygląda jak coś gdzie doszli ludzie. To wygląda jakby tu żył potwór.

Drona znalazła... salę balową. Sześć osób tańczy, w pełnej radości, żyrandole, muzyka... piękne miejsce. Cudowne.

Nie widać żadnego potwora.

* V: Echolokacja pancerza pokazuje, że TAK, jest wąsko ale nie tak wąsko. Masz dużo kanałów.

Viorika ostrożnie idzie na czworakach w serwopancerzu. 

Arianno - masz wrażenie że presja na Ciebie _zelżała_. Łatwiej się myśli. Arianna chce być PEWNA że uwaga zelżała - tak JEST.

Tr +4:

* X:
    * Ariannie chwilę to zajęło; praktycznie hipnotyczne wzory plus sobie nie ufa.
    * Rozkład tych 'gluto-kawałków' dookoła to podobny pattern jaki Arianna by zrobiła gdyby chciała rozkładać holoprojektory.
    * "To jest dowód, że tam COŚ jest więcej niż Nieczłowiek-Adrian"

Nie ma tych kawałków blisko strumienia gdzie jest energia Interis. Arianna robi _cleaning_ po ścianach, by to coś się zmartwiło.

* X: Udało Ci się to czyścić, ale nie przyciągasz uwagi z Vioriki. I dostaniesz próbkę.

Viorika - weszłaś do sali balowej. Viorika otwiera ogień do ludzi którzy tam są. Trzeba ich zabrać.

Tr Z +3 +3Og:

* X: Serwopancerz włączył wszystkie sygnały alarmowe i złamał polecenie Vioriki, unikając... czegoś? BÓL. Leżysz na ziemi, na plecach.
    * Viorika zagryza wargę i próbuje _push forward_, otrzeźwić się.
* Vr: Przez MOMENT zobaczyłaś jak serwopancerz się odsunął i kolejny kamień PRAWIE złamał nogę. Coś atakuje.
* V: Poczułaś jeszcze większy cios, poszła Ci krew, nie doszło do przebicia, ale _czujesz_ jak serwopancerz strzela, choć nie wiesz do czego. CEL JEST W ZASIĘGU. Pewna jest.

Nacisk na Ariannę znika. Coś się dzieje. Słyszy dźwięki ostrzeliwania i padających głazów.

Viorika jest _uszkodzona_. Potwór jest ciężko ranny i się wycofuje. Widzisz plugawe, rtęciowe macki, na suficie, odpełzające. Widzisz ludzi którzy kulą się po ścianach. Jesteś w stanie go zniszczyć. Parę kroków, świetne miejsce do strzelania. Ma złamane żebro.

Arianna włącza override. Niech Viorika wraca. A Viorika na "zakończenie" wysyła dronę na krzywdę potwora.

Tr Z +3:

* V: Arianna OVERRIDOWAŁA Viorikę. Servar wraca awaryjnie asap. Arianna dostaje kody błędów i uszkodzeń; dostał. Servar wstrzyknął V. stymulanty.
* V: Viorice udało się zgarnąć jeszcze dwie osoby. Ewakuujemy się.

Dzięki temu że Arianna zniszczyła bioprzekaźniki - Viorika wyszła ze strumienia. Widać, jak ciężko dostała. Verlenki się ewakuowały bezpiecznie ASAP.

Udało się Wam wycofać - Viorika faktycznie... zniechęciła stwora do ataku. Żołnierze przejęli i się stabilizujecie. Ale obie są Skażone Alucis, tylko lekko. Sama aura tego miejsca jest niebezpieczna.

Udało się uratować... z 15 osób, 9. To dużo. Zwłaszcza patrząc, że cała Karawana.

Sukces. Trzeba się przegrupować i działamy dalej.

***
***

## Streszczenie

Zespół przechwycił SOS z dawnego kamieniołomu, gdzie znaleziono karawanę skażoną Alucis. Ludzie byli ospali i zdezorientowani, a woda okazała się wektorem skażenia. Okazało się, że przyczyną jest Lustro Wody ze starego kamieniołomu, źródło Alucis. Arianna i Viorika uratowały karawanę i udało im się uszkodzić potwora, ale tienki musiały się ewakuować. Ewakuowano 9 z 15 osób porwanych w kamieniołomie plus resztę Karawany. Viorika została ranna, ale udało im się osłabić wpływ potwora.

## Progresja

* Arianna Verlen: Reputacja w Przelotyku wśród ludzi: +1. Pomaga ludziom gdy jest w stanie, nawet, gdy nie musi.
* Viorika Verlen: Reputacja w Przelotyku wśród ludzi: +1. Pomaga ludziom gdy jest w stanie, nawet, gdy nie musi.
* Viorika Verlen: (Pęknięte Żebro), (Ranna) i (Uszkodzony Serwopancerz). Wymaga dwa tygodnie regeneracji w dobrych warunkach w Verlenlandzie.

## Zasługi

* Arianna Verlen: Doszła do tego co się dzieje na przykładzie Kobiety Z Szarlotką. Złapała Doppelgangera 'przez miskę', a potem po zniszczeniu bioprzekaźników Lustrzanej Wody wycofała Viorikę z bloodlust.
* Viorika Verlen: Jej pomysł z stymulantami wykrył Doppelgangera; poszła o krok za daleko walcząc z Lustrzaną Wodą i dotknęło ją Alucis, ale udało jej się uratować więcej ludzi a Ariannie wycofać Viorikę.
* Mateusz Orkliacz: Kapral z Verlenlandu, wspierający Ariannę i Viorikę w operacjach Przelotyku. Dowodzi żołnierzami by zabezpieczyć Karawanę pod wpływem Potwora Alucis. Dobrze zarządza ludźmi, lojalny Verlenkom.

## Frakcje

* .

## Fakty Lokalizacji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Pasmo Przyverleńskie
                            1. Stary Kamieniołom Tyrlicki: Gdzieś pod nim i w nim znajduje się Lustro Wody Alucis, bardzo niebezpieczny potwór spawnujący Doppelgangery.

## Czas

* Opóźnienie: 8
* Dni: 1

## Inne

.
