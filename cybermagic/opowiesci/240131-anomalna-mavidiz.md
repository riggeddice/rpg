## Metadane

* title: "Anomalna Mavidiz?"
* threads: triumfalny-powrot-arianny, naprawa-swiata-przez-bladawira
* motives: the-dominator, energia-alteris, broken-mind, energia-alucis, o-dziwo-normalny-kompetentny-agent
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [240124 - Mikiptur, zemsta Woltaren](240124-mikiptur-zemsta-woltaren)

### Chronologiczna

* [240124 - Mikiptur, zemsta Woltaren](240124-mikiptur-zemsta-woltaren)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA: 
    * Hack Sign Aura (evil version)
        * .
* Opowieść o (Theme and vision)
    * Mavidiz jest anomalną jednostką, Orbiter nie ma dość sił by zajmować się wszystkimi. Ta sesja i los Mavidiz pokazuje, czemu Orbiter jest potrzebny.
    * Mavidiz posiada niezwykle groźnego Nihilosekta na pokładzie, ukrytego w anomalnych korytarzach
        * (osy interis: 3, spawn more places: 5)
        * chain: Tivr: (odcięcie - zagubienie w Alteris - )
    * SUKCES
        * .
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * the-dominator: Nihilosekt (Unumens + Interis); pojawił się i pożarł dwóch agentów Aureliona i agenta Orbitera. Teraz próbuje zdominować wszystkich innych i dołączyć do Roju.
    * energia-alteris: jednostka Mavidiz; jest anomalna. Jest praktycznie nieskanowalna. Ma błędne sygnały i nieodpowiednie informacje. Orbiter ją olewa.
    * broken-mind: Agaton Mikran, mag. Całkowicie zdominowany przez Nihilosekta, acz jego magia naturalnie jeszcze walczy. 
    * energia-alucis: manifestacje Mavidiz, wywoływane przez echo Agatona Mikrana. Demonstracje że coś jest nie tak na pokładzie tej jednostki.
    * o-dziwo-normalny-kompetentny-agent: kapitan Ernest Bankierz zadziałał zgodnie z procedurami i nie badał dokładnie Mavidiz.
    
### Co się stało i co wiemy

.

### Co się stanie (what will happen)

* F0: 
    * .
* F1: Poszukiwania prawdy na Mavidiz
    * .
    * Q:
        * Czy Zespół musi być uratowany?
        * Czy Zespół skończy ze strasznymi stratami?
* CHAINS
    * Problemy i konflikty
        * .
* Overall
    * stakes
        * .
    * opponent
        * .
    * problem
        * .

## Sesja - analiza

### Fiszki

Zespół:

* Leona Astrienko: psychotyczna, lojalna, super-happy and super-dangerous, likes provocative clothes
* Martyn Hiwasser: silent, stable and loyal, smiling and a force of optimism
* Raoul Lavanis: ENCAO: -++0+ | Cichy i pomocny;; niewidoczny| Conformity Humility Benevolence > Pwr Face Achi | DRIVE: zrozumieć
* Alicja Szadawir: młoda neuromarine z Inferni, podkochuje się w Eustachym, ma neurosprzężenie i komputer w głowie, (nie cirrus). Strasznie nieśmiała, ale groźna. Ekspert od materiałów wybuchowych. Niepozorna, bardzo umięśniona.
    * OCEAN: N+C+ | ma problemy ze startowaniem rozmowy;; nieśmiała jak cholera;; straszny kujon | VALS: Humility, Conformity | DRIVE: Uwolnić niewolników
* Kamil Lyraczek
    * OCEAN: (E+O-): Charyzmatyczny lider, który zawsze ma wytyczony kierunek i przekonuje do niego innych. "To jest nasza droga, musimy iść naprzód!"
    * VALS: (Face, Universalism, Tradition): Przekonuje wszystkich do swojego widzenia świata - optymizm, dobro i porządek. "Dla dobra wszystkich, dla lepszej przyszłości!"
    * Styl: Inspirujący lider z ogromnym sercem, który działa z cienia, zawsze gotów poświęcić się dla innych. "Nie dla chwały, ale dla przyszłości. Dla dobra wszystkich."
* Horst Kluskow: chorąży, szef ochrony / komandosów na Inferni, kompetentny i stary wiarus Orbitera, wręcz paranoiczny
    * OCEAN: (E-O+C+): Cichy, nieufny, kładzie nacisk na dyscyplinę i odpowiedzialność. "Zasady są po to, by je przestrzegać. Bezpieczeństwo przede wszystkim!"
    * VALS: (Security, Conformity): Prawdziwa siła tkwi w jedności i wspólnym działaniu ku innowacji. "Porządek, jedność i pomysłowość to klucz do przetrwania."
    * Core Wound - Lie: "Opętany szałem zniszczyłem oddział" - "Kontrola, żadnych używek i jedność. A jak trzeba - nóż."
    * Styl: Opanowany, zawsze gotowy do działania, bezlitośnie uczciwy. "Jeśli nie możesz się dostosować, nie nadajesz się do tej załogi."
* Lars Kidironus: asystent oficera naukowego, wierzy w Kidirona i jego wielkość i jest lojalny Eustachemu. Wszędzie widzi spiski. "Czemu amnestyki, hm?"
    * OCEAN: (N+C+): bardzo metodyczny i precyzyjny, wszystko kataloguje, mistrzowski archiwista. "Anomalia w danych jest znakiem potencjalnego zagrożenia"
    * VALS: (Stimulation, Conformity): dopasuje się do grupy i będzie udawał, że wszystko jest idealnie i w porządku. Ale _patrzy_. "Czemu mnie o to pytasz? Co chcesz usłyszeć?"
    * Core Wound - Lie: "wiedziałem i mnie wymazali, nie wiem CO wiedziałem" - "wszystko zapiszę, zobaczę, będę daleko i dojdę do PRAWDY"
    * Styl: coś między Starscreamem i Cyclonusem. Ostrożny, zawsze na baczności, analizujący każdy ruch i słowo innych. "Dlaczego pytasz? Co chcesz ukryć?"
* Dorota Radraszew: oficer zaopatrzenia Inferni, bardzo przedsiębiorcza i zawsze ma pomysł jak ominąć problem nie konfrontując się z nim bezpośrednio. Cichy żarliwy optymizm. ŚWIETNA W PRZEMYCIE.
    * OCEAN: (E+O+): Mimo precyzji się gubi w zmieniającym się świecie, ale ma wewnętrzny optymizm. "Dorota może nie wiedzieć, co się dzieje, ale zawsze wierzy, że jutro będzie lepsze."
    * VALS: (Universalism, Humility): Często medytuje, szuka spokoju wewnętrznego i go znajduje. Wyznaje Lichsewrona (UCIECZKA + POSZUKIWANIE). "Wszyscy znajdziemy sposób, by uniknąć przeznaczenia. Dopasujemy się."
    * Core Wound - Lie: "Jej arkologia stanęła naprzeciw Anomaliom i została Zniekształcona. Ona uciekła przed własną Skażoną rodziną" - "Nie da się wygrać z Anomaliami, ale można im uciec, jak On mi pomógł."
    * Styl: Spokojna, w eleganckim mundurze, promieniuje optymizmem 'że z każdej sytuacji da się wyjść'. Unika konfrontacji. Uśmiech nie znika z jej twarzy mimo, że rzeczy nie działają jak powinny. Magów uważa za 'gorszych'.
    * metakultura: faerilka; niezwykle pracowita, bez kwestionowania wykonuje polecenia, "zawsze jest możliwość zwycięstwa nawet jak się pojawi w ostatniej chwili"
* Władawiec Diakon: tien, corruptor of ladies, duelist, second-in-command na statku kosmicznym (XO)
    * ENCAO: (E+C+) | intrygancki;; kontrolowany wulkan;; pająk w sieci | VALS: Hedonism, Power | DRIVE: Perfekcja Zaufania
    * styl: nieskazitelny oficer, delikatny uśmiech, niezmiennie pewny. Gabriel Durindal (Gundam Seed Destiny)
    * piosenka wiodąca: Epica "Obsessive Devotion"

Tivr:

* Markus Wąż
    * gardzi Arianną za Bladawira, słucha poleceń, sztywny, przygotowany na wszystko, szybki refleks i duma

Mavidiz:

* Igor Stratos: kapitan; obsesyjny na punkcie swojej firmy, bogactwa itp
* Andrea Weiss: ładunek; charyzmatyczna, obsesyjnie pragnie być kochana
* Borys Kragin: inżynier; nie kojarzy statku, obsesyjnie szuka swojej żony
* Grigor Tarnow: ochrona; silny i wytrzymały, obsesyjnie walczy z 'eisenkriegerami'
* Rita Stratos: medyczka, obsesyjnie skupiona na higienie i czystości.
* Agaton Mikran: mag, pusty w sercu, nawigator
* Karl Murnoff: dumny eks-Aurelionowiec, pragnął wielkich czynów
* Tara Ogniczek: xenobiolog, ambitna i zrobi wszystko dla kariery. Teraz - obsesyjnie zakochana w Grigorze i chce do łóżka z każdym

Castigator:

* Leszek Kurzmin: OCEAN: C+A+O+ | Żyje według własnych przekonań;; Lojalny i proaktywny| VALS: Benevolence, Humility >> Face| DRIVE: Właściwe miejsce w rzeczywistości.
* Patryk Samszar: OCEAN: A+N- | Nie boi się żadnego ryzyka. Nie martwi się opinią innych. Lubi towarzystwo i nie znosi samotności | VALS: Stimulation, Self-Direction | DRIVE: Znaleźć prawdę za legendami.
    * inżynier i ekspert od mechanizmów, działa świetnie w warunkach stresowych

Siły Bladawira:

* Antoni Bladawir: OCEAN: A-O+ | Brutalnie szczery i pogardliwy; Chce mieć rację | VALS: Power, Family | DRIVE: Wyczyścić kosmos ze słabości i leszczy.
    * "nawet jego zwycięstwa mają gorzki posmak dla tych, którzy z nim służą". Wykorzystuje każdą okazję, by wykazać swoją wyższość.
    * Doskonały taktyk, niedościgniony na polu bitwy. Jednocześnie podły tyran dla swoich ludzi.
    * Uważa tylko Orbiterowców i próżniowców za prawidłowe byty w kosmosie. Nie jest fanem 'ziemniaków w kosmosie' (planetarnych).
* Kazimierz Darbik
    * OCEAN: (E- N+) "Cisza przed burzą jest najgorsza." | VALS: (Power, Achievement) "Tylko zwycięstwo liczy się." | DRIVE: "Odzyskać to, co straciłem."
    * kapitan sił Orbitera pod dowództwem Antoniego Bladawira
* Michał Waritniez
    * OCEAN: (C+ N+) "Tylko dyscyplina i porządek utrzymują nas przy życiu." | VALS: (Conformity, Security) "Przetrwanie jest najważniejsze." | DRIVE: "Chronić moich ludzi przed wszystkim, nawet przed naszym dowódcą."
    * kapitan sił Orbitera pod dowództwem Antoniego Bladawira
* Zaara Mieralit
    * OCEAN: (E- O+) "Zniszczenie to sztuka." | VALS: (Power, Family) "Najlepsza, wir zniszczenia w rękach mojego komodora." | DRIVE: "Spalić świat stojący na drodze Bladawira"
    * corrupted ex-noctian, ex-Aurelion 'princess', oddana Bladawirowi fanatyczka. Jego prywatna infomantka. Zakochana w nim na ślepo.
    * afiliacja magiczna: Alucis, Astinian

### Scena Zero - impl

.

### Sesja Właściwa - impl

Bladawir robi opieprz Ariannie za Eustachego.

* Bladawir: Kapitan Verlen, nie spieszyło Ci się by ratować Waritnieza.
* Arianna: To było wszystko co mogłam zrobić po tym jak część oficerów była uszkodzona po spotkaniu z kralothem
* Bladawir: I tak po prostu... Twój bezwartościowy 1 oficer poleciał sobie z prędkością spacerową?
* Arianna: Mój pierwszy oficer nie docenił wagi wydarzenia. My też mieliśmy informacje że pan komodor ma być w okolicy.
* Bladawir: Mam nadzieję, że Twój pierwszy oficer się do CZEKOKOLWIEK nadaje. Straciłem dobrego kapitana przez niego.
* Arianna: Ma swoje zalety, komodorze
* Bladawir: Miło było, jak wyszłaś przez szereg próbując nieco nieefektywnie mnie chronić przed siłami Orbitera. Co chciałaś osiągnąć?
* Arianna: Nie wiem, komodorze, może by pana na front wschodni nie wysłali. Nie doceniłam złej reputacji komodora.
* Bladawir: Nie wiem w co grasz. (ze zwężonymi oczami)
* Arianna: Odnoszę wrażenie, że nie w tą samą grę co pan.
* Bladawir: Jeśli JA w szachy, TY w warcaby. Wspomagana przez AI. Ujawniłaś Syndykat. Oni wiedzą. Skąd wiedziałaś?
* Arianna: Co konkretnie?
* Bladawir: O Syndykacie.
* Arianna: Połączyłam kropki, wyciągnęłam wnioski ze wszystkiego co robiliśmy, wyciągnęliśmy informacje z kralotha...
* Bladawir: Nie wyglądasz na tak inteligentną.
* Arianna: Od tego mam Klaudię.
* Bladawir: Kolejny parametr którego mogłem nie uwzględnić... dobrze, załóżmy, że jest tak jak mówisz.
* Bladawir: Jest coś co może pomóc udowodnić Twoją tezę.
* Arianna: Co takiego?
* Bladawir: Zdrajca, czyli kapitan Ernest Bankierz, wysłał jedną ze swoich... niecnotek na jednostkę lecącą na Neikatis. Tyle zdążyłem się dowiedzieć. Nie dolecicie tam Infernią, nie zdążycie. Niech Twoja Klaudia zdobędzie dostęp do Tivr - szybkiej korwety. Lećcie na Mavidiz. Przechwyćcie ją. Przesłuchajcie niecnotkę. Znajdźcie dowód na zdradę Bankierza.
* Arianna: Rozumiem komodorze, że nie mamy twardych dowodów?
* Bladawir: Nie, z Aurelionem nie masz twardych dowodów. Ale ona leci na Neikatis statkiem nie-Orbitera? Została wysłana na front wschodni. Nikt nie chce lecieć na Neikatis, tam żyją tylko gnidy.
* Arianna: Jakaś konkretna niecnotka, jak to pan komodor ujął?
* Bladawir: Tara Ogniczek. Bardzo ambitna młoda dama, mająca ogromny potencjał. I nagle - leci na Neikatis. Po... kursie, że tak powiem, u kapitana Ernesta Bankierza (z odrazą). I jeszcze urodzona na Orbiterze. Czystej krwi.

Bladawir mówi, że ma dowód na działania kapitana Bankierza - odesłał swoją dziewczynkę na Mavidiz. Arianna MUSI tam lecieć Tivrem ASAP. Tam coś się stało i ją wysłał na Neikatis.

Mavidiz jest jednostką ANOMALNĄ. Jest skażona Alteris (pochodzi z Anomalii Kolapsu) -> struktura jest nienaturalna. Mavidiz ma wyjątkowo małą załogę, by się nie zorientowała że działa.

Klaudia chce załatwić Tivr. Skoro nie lubią Bladawira, to Bladawir zacznie protestować przed udostępnieniem Ariannie Tivra. Arianna ma INICJATYWĘ i Bladawir próbuje to zablokować:

Tp (Bladawir jest nielubiany, WSZYSCY wrogowie Bladawira chcą by Arianna była daleko) +3:

* V: Tivr zostanie użyczony Ariannie na czas specjalnej misji (zwiad Syndykatu na Mavidiz)
    * w życiu nie dostalibyście Tivra, ale Arianna "syndykat, odkryłam"

Coś o Tarze Ogniczek, czemu miałaby lecieć?

Tp +3:

* V: 
    * ambitna młoda dama, ogromny potencjał, ksenobiolog, specjalizuje się w dziwnych potworach itp. 
    * wie co chce, ambitna, daleko się posunie, zrównoważona
    * narobiła sobie problemów na K1, poważnych problemów.
* V:
    * Tara pozyskiwała różne próbki, w wyniku problemów udało jej się zrzucić winę na kogoś innego. Ale jej nie wybaczono. Uciekała o krok przed problemami.
    * W końcu zwiała do Bankierza (inni: została pozyskana do Bankierza)
    * "słodka i niewinna". "to na pewno plotki na jej temat" - a potem śpi z profesorem. A potem -> Ernest na mostek, tam kraloth. I nagle... Neikatis?

Tivr UDAJE że leci na Neikatis z ładunkiem do badań. I chce ją ściągnąć. Gdzieś w pobliżu, przyjdą pomóc na pokład, powie jej się co ma na pokładzie. I powinna przyjść. Klaudia musi spreparować listy przewozowe, że ma 'macki do possania' i jak dotkniesz to problem. I tak się ściągnie Tarę na statek. (Klaudia to przygotowuje bez problemu).

Chcemy na Tivr Raoula. On jest zawsze bezpiecznym wyborem.

Na Tivrze wita pilot Markus Wąż. 100% profesjonalizmu i kija w tyłku. Markus podał parametry na konsoli i lecą. Raoul nie gada jak nie musi, Markus nie chce się zbliżać do załogi Inferni ale robi co powinien.

Mavidiz w zasięgu. Komunikuje się z Tivrem.

* Borys: Borys z tej strony. Kragin. Co tu robicie? 
* Arianna: Przesyłka na Neikatis, szybka, do biolabu. Miała czekać jednostka na której mieliśmy tankować.
* Borys: Nic nie widzieliśmy. Orbiter Was zawiódł.
* Arianna: Niedobrze, nie mamy zasięgu. Nie wie pan kapitan gdzie możemy zatankować.
* Borys: (śmiech) Jestem inżynierem, nie kapitanem. (myśli) Nic nie ma w pobliżu. Ale my jesteśmy.
* Borys: Czekaj.
* Borys: (po 5 minutach) Możecie podlecieć do nas. Domontujemy Was. Nie dotankujemy Was, nie mamy jak, ale Was możemy podpiąć i polecimy razem.
* Arianna: Lepsze niż zginąć w ciemności kosmosu
* Borys: Masz lepszą opcję?
* Arianna: Nie mam, przyjmiemy
* Borys: Umiecie podlecieć blisko? Nasz advancer pomoże sprząc statki. Macie advancera?
* Arianna: Mamy
* Borys: Świetnie, pomoże Karlowi i Andrei. 

Tivr podlatuje do Mavidiz. Markus pilotuje dokładnie, z 80% większą precyzją i dokładnością.

TrZ+3:

* X: Markus próbował, ale niestety doszło do niewielkiej kolizji. Będą odszkodowania, problemy itp.
* Vr: ...ale Markusowi się udało.

Raoul ma minę typu "serio?", ale nic nie mówi. Raoul ogólnie nie mówi. Markus próbuje nie patrzeć na załogę Inferni. Skupia się na sensorach i monitorach.

* M: Pani kapitan, melduję kolizję. Ta jednostka... źle pokazuje się na monitorach i na sensorach. Nie wiem jak to powiedzieć - nie powinno jej tam być.
* Arianna: Dobrze, kontynuuj manewry.

Wiadomość z Mavidiz:

* Igor: Tu Igor, kapitan jednostki. Doszło do kolizji, z Waszej winy. Potwierdzacie?
* Arianna: Potwierdzamy.
* Igor: Zakładam, że będzie zadośćuczynienie?
* Arianna: I tak idziecie nam na rękę, coś się ogarnie.
* Igor: Cieszę się. Oby to był początek DOBRYCH relacji Mavidiz z Orbiterem...

Raoul i dwie osoby z Mavidiz zaczęły spinać siatką itp. Dwie godziny później jednostki zostały połączone i ustabilizowane z perspektywy pędu. I powstał "korytarz powietrzny", śluza - śluza i można wejść na pokład Mavidiz.

Mavidiz stoi otworem. Ta jednostka jest anomalna. To da się wyczuć. Magowie czują przepływ energii magicznej. Wszędzie mnóstwo robotów pełniących rolę załogi. MNÓSTWO. Powitał koleś - złoty łańcuch, nagi tors i spodnie w moro. Koleś się uśmiecha do Was, widząc dziewczyny napina mięśnie. "Grigor jestem, ochrona."

Eustachy - widzisz że koleś ma blizny i rany. Sporo musiał walczyć. I całkiem niedawno.

Kapitan wygląda jak się spodziewacie - szara broda, szary wąs, czapka, fajka bez czegokolwiek. Typowy kapitan.

* Igor: Igor Stratos, kapitan tej pięknej jednostki. A Wy jesteście gośćmi.
* Arianna: Na to wychodzi. Arianna Verlen, kapitan... tamtej jednostki.
* Igor: Verlen. Wy jesteście... (myśli), była o Was taka bajka, nie?
* Arianna: Niejedna.
* Igor: (uśmiech). Dobrze.

Wiecie dlaczego celnicy nie chcą nic robić. Jest ten efekt, że statek się "zagina". Arianna widziała kątem oka przelatującego owada. Osę? Muchę? I Arianna BARDZO nie chce sięgać do Eteru Nieskończonego. A Klaudia kataloguje to co widzi.

Koło Klaudii wyrósł niespodziewanie szef ochrony. Grigor. Klaudia się spłoszyła. 

* Grigor: "Chcesz się bić?"
* Klaudia: Bić? To z Eustachym
* Grigor: Nie, z eisenkriegerami.

Grigor tłumaczy, że każdy z robotów jest eisenkriegerem. Pilnuje Klaudii jako samiec alfa i przeprowadza. Rita opieprza Klaudię że jest brudna i ma się wykąpać.

Tr Z +3:

* V: Klaudia wie jak się poruszać - Rita TEŻ unika niektórych miejsc.
    * Zarówno Rita jak i Grigor są trochę nie tak...
    * zaczęli się całować...

Klaudia wraca. Zobaczyła rudowłosą kobietę która otworzyła usta do krzyku, wpełzł jakiś owad i halucynacja się rozwiała. Klaudia nie zna tej kobiety... Klaudia wraca. Alteris nie jest tak mocne - udało się wrócić.

Kapitan na mostku jest chciwy, myśli w jaki sposób zarobić jak najwięcej. Podszedł do Arianny Borys.

* Arianna: Wie pan, kapitanie, możemy się przydać, może mój inżynier spojrzy na wasz statek i coś naprawić
* Igor: Twój inżynier oszaleje. Tylko Borys sobie z tym radzi. Jakoś.
* Borys->Arianna: Widziałaś może... (zawiesił głos) czerwone wodorosty?
* Arianna: Nie, czerwonych wodorostów nie...
* Igor: Borys, daj spokój. Statek na Ciebie działa. Idź, wypij coś.
* Borys: (próbuje się skupić) ale nie jest w stanie, oddala się.
* Arianna: Widziałam jakieś owady. Macie owady?
* Borys: SZYBKO się odwrócił do Arianny, błysk w oku
* Igor: Owady? Tu? Nie. Borys, mamy owady?
* Borys: ...nie... nie możemy mieć. (błysk zgasł)

Jesteście na tej uroczej jednostce. Mavidiz.

* Raoul: Pani kapitan, dalsze rozkazy?
* Arianna: Nie wchodź na Mavidiz. Złóż raport sytuacyjny z tego jak jednostka wygląda.

Tr Z +3:

* V: Raoul się waha by coś powiedzieć.
    * R: Kapitan Verlen. Oni są dziwnymi advancerami.
    * Arianna: ?
    * R: Ich... nigdy nie oderwali się od pokładu Mavidiz. Zawsze są styczni ze swoją jednostką.
    * R: Klasa 'Arcadalian', 140 osób załogi, minimum koło 30.
    * R: (tłumacząc co widzi, potwierdził że nie ma bardzo dużego Skażenia Alteris)

Klaudia chce zidentyfikować potencjalne źródło energii. Ma spacer po statku, jest Eustachy który może mechanicznie wypatrzyć co się tu dzieje i Arianna z dużym polem wyczucia.

* X: Nie da się jednoznacznie określić konkretnego pola magicznego
    * Klaudia byłaby w stanie wykryć jedno silne pole. Nie ma go tu. Coś innego.
    * TO co widzicie wskazuje na silne pole magiczne. Nie ma go.
    * To nie jest tło, to nie jest pole magiczne.

Arianna -> Igor: "jakie uszkodzenia statku? Co zgłasza TAI?"

Tp (Igor wierzy w to że jak pokaże logi, info -> więcej kasy z Orbitera) +3:

* X: Przekonywanie, rozmowa itp. trwa. Kapitan jest w "logicznej pętli". Jakieś 15 min. Ale 15 minut to paliwo. (ODLEGŁOŚĆ: ++)
* V: Igor bez problemu pokazuje logi Ariannie i Klaudii.
* V: Kapitan jest DOŚĆ SPRAWNY w używaniu TAI, ale Klaudia dała mu lepsze 'query'. Klaudia ma większe uprawnienia niż kapitan chciał. TAI stoi przed Klaudią otworem.

Zgodnie z danymi z TAI, rzeczy nie do końca się spinają.

* Część załogi jest nie ta jaka powinna być.
    * "czerwonowłosa kobieta" nazywa się Aurelia Kragin. Żona Borysa. ("czerwone wodorosty")
    * kto jest a nie powinien: (Tara Ogniczek), (Karl Murnoff - advancer), (nawigator - Agaton Mikran)
    * nie ma 5 osób, w czym Aurelii Kragin
* Macie dossier załogi.
    * jeszcze 3 miesiące temu NIKT nie miał zachowań anomalnych
    * medyczka miała podejście - prysznice, KONIECZNE. Lapisowaną wodą. "nie możecie być nieczyści"
* Logi są wykasowane - są wykasowane, ale głupio
* Wszyscy wierzą że to roboty defensywne, więc Alteris dostarcza. Roboty są 'koszerne'.

Eustachy robi randomowe uszkodzenia i sabotaże - część by "zepsuło się, jest kosmos", część "uszkodzenie zmniejsza dochody" a część "zagrożenie dla całego statku". Zmapować czy to co mamy cechuje się formą intelektu czy to random. Zgodnie z danymi Klaudii - Tara była wprowadzona PO tym jak statek był Skażony.

Eustachy się oddala tam gdzie nikt nie widzi, gdzie jest mniej robotów - w mniej uszczędzany fragment gdzie jest sabotaż i zaczynasz sabotowanie.

Tr +3:

* V: DOWIESZ SIĘ tego co chcesz się dowiedzieć
* X: Mam ruch
* (+1Vg+3Or) X: atak.

Eustachy sabotuje, w tych ciemnych zakamarkach, w tych Skażonych przez Alteris miejscach... usłyszałeś bzyczenie. Osa. Siada na Lancerze. Lancer zaczyna korodować. Ilość os rośnie.

Ex Z +3 +3Or:

* X: Jesteś SAM. Komunikacja odcięta. Osy uszkodziły Lancer (pęknięty pancerz)
    * (po hipernecie: OGNIA!!! Strzel we mnie!!!: +5Og)
* X: Atak Tivra poważnie uszkodził Mavidiz.
    * Włączyły się światła awaryjne
* (+1Vg) Eustachy maksymalizuje temperaturę: X: ciężko uszkodzony Lancer, biegnie, ignoruje, skacze przez wyłom i Raoul wyciąga na Tivr. Osy nie przetrwały próżni.

Arianna i Klaudia są na jednostce.

* Arianna: Proszę się uspokoić, zaatakowaliście mojego człowiek
* Igor: Wasz człowiek uszkodził mój statek!
* Klaudia: WASZ STATEK ZAATAKOWAŁ MÓJ CZŁOWIEK!
* Igor: Jesteście piratami?! Jesteście...
* Arianna: Zaatakowaliście nas na waszej jednostce! W komputerze czerwone ramki błędów, nic nie zostało zrobione!
* Igor: Kłamstwo! Nic tam nie ma, dałem nawet dostęp!

AURELIA NA WSZYSTKICH EKRANACH! Arianna: "Borys, czerwone wodorosty, Twoja żona, Aurelia, jest nawet z Tobą, byliście razem!"

ExZ+4:

* Vr: Borys złapał się za głowę i zaczął krzyczeć
    * jak pod jego skórą coś się rusza w okolicy szyi, z tylnej części głowy

Arianna robi FULL BORYS POWER, Exemplis. Niech to Borys.

-> TrZM+4+5Ob:

* Ob:
    * LUDZIE WYCZYSZCZENI Z OS!
    * OSOPOTWÓR ROJOWY WZMOCNIONY!!!

Eustachy w skafandrze trafia na Mavidiz. Z Klaudią próbują wykorzystać zasady działania robotów, żeby kupić ludziom czas na dostanie się na mostek (statek chce bronić siebie i załogę). PLUS Arianna rekomenduje użycie zraszaczy + gaśnice i kontrolowane otwieranie śluz i ścieżka życia

Tr Z +4:

* X: Osy mają kilka miejsc 'startowych', one mają dużo miejsc przez które się poruszają, bardzo trudno je odciąć. Osy 'przechodzą' przez ścianę, robią niewielke dziury.
* (+Vg) Vz: Roboty są uaktywnione jako zasób. Działają by walczyć z osami.
* Vr: Ludzie są w miarę bezpiecznym przyczółku - doki serwisowe. Potwór się zbliża.

Operacja 'RAIN OF FIRE!'. Eustachy używa zbiornika paliwa, systemy zraszaczy, zbiorniki w których substancje są przechowywane. Przepina węże. Ale do silników NA PEWNO idą węże. Silnik ma i paliwo i system gaśniczy. Używasz kompresorów, narzędzi pod ręką i odwraca ciąg.

Tr Z +4:

* X: szef ochrony rzuca się w ogień z podwójnym miotaczem ognia, ratuje kogoś innego
* V: udało się przepiąć. RAIN OF FIRE.
* V: osy się cofnęły, ratują królową.

Advancer (Karl) przejął kontrolę nad sytuacją. "Ruszamy się albo wszyscy zginiemy!" (plaskacz tu i plaskacz tam)

PIERWSZY: Klaudia opracowuje podejście przez Alteris, by skutecznie 'odciąć' część statku by Eustachy mógł użyć Alteris i odciąć fragment statku na stałe. Arianna zarządza ludźmi że ROBIĄ RZECZY i dzięki nim to będzie działać.

Tr Z+4:

* Vr: udało się Klaudii to zrobić.
* V: Klaudia robi tą pułapkę przestrzenną używając jako przynęty krwi ludzi.
    * a medyczka zdezynfekuje ciała ludzi

DRUGI: Arianna skłania ludzi by robili rzeczy i uwierzyli

* Vr: ludzie wierzą że to się uda
* V: ludzie wiedzą, że Orbiter ich uratował i bez Orbitera by nie mieli szans - problem "Tivr uszkodził" itp zaniknie

TRZECI: Eustachy ma jeden strzał by to dobrze odstrzelić i sensownie odspawać

* Vr: Eustachemu udało się sensownie wydzielić fragment jednostki
* Vz: czysta operacja, taka 'idealna'. Nikt nie ucierpiał, macie dość tlenu itp.

Tivr można wysłać po posiłki (SOS itp.) a Wy zachowujecie spokój na pokładzie... wraku by nikt nie zrobił nic głupiego i operacja jest udana.

## Streszczenie

Bladawir skierował Ariannę na SCA Mavidiz używając OO Tivr. Tam - o czym nikt nie wie - jest Nihilosekt. Madiviz jest anomalnym statkiem - co gorsza coś jest nie tak z załogą wpadającą w obsesje i pętle; pojawiają się halucynacje i z ludźmi coś jest nie tak. Gdy Zespół zostaje zaatakowany, z trudem sabotują jednostkę i robią ostatni przyczółek przy silnikach. Eustachemu udaje się oddzielić 'zarażony' fragment jednostki i Mavidiz - co prawda ciężko zniszczona - przetrwała.

## Progresja

* Antoni Bladawir: dzięki operacji Arianny na Mavidiz uzyskał eks-Aurelionowca (Karl Murnoff) i eks-agentkę Ernesta Bankierza (Tarę Ogniczek).

## Zasługi

* Arianna Verlen: negocjując z Bladawirem zrzuciła część uwagi na Klaudię ('ta mądra'); utrzymała nerwy na wodzy gdy załoga Mavidiz okazała się być 'dziwna'. Stworzyła barierę ogniową przeciw nihilosektowi i ufortyfikowała miejsce przed potwornym insektem. 
* Klaudia Stryk: dostała dostęp do Tivru i dowiedziała się kim jest Tara Ogniczek; analizując anomalie na Mavidiz doszła do tego z czym walczą, mniej więcej. Opracowała jak użyć Alteris by odciąć część statku i zrobić sanktuarium.
* Eustachy Korkoran: sabotuje Mavidiz redukując zagrożenie ze strony nihilosekta, walcząc z nihilosektem prawie zginął. Wrócił na Tivr i stamtąd mając jeden strzał odstrzelił Skażony fragment Mavidiz, zapewniając, że nihilosekt nie ma bezpiecznego dojścia do załogi.
* Raoul Lavanis: połączył Tivr i Mavidiz siatką jako advancer; gdy Eustachy wypadł w kosmos walcząc z nihilosektem, przechwycił Eustachego na pokład Tivra, ratując mu życie.
* Markus Wąż: nie lubi załogi Inferni za współpracę z Bladawirem, ale absolutny profesjonalista; kompetentny pilot Tivra który dogonił Mavidiz. Zderzył się z Mavidiz przez elementy Alteris, co sprawiło mu ogromny wstyd.
* Antoni Bladawir: wysłał Ariannę Tivrem (którego miała zdobyć) na Mavidiz, by przejąć Tarę (od Ernesta Bankierza) jako dowód, że Ernest jest zdrajcą.
* Igor Stratos: kapitan Mavidiz; obsesyjny na punkcie swojej firmy, bogactwa itp. Pozwala Tivrowi na działania pod warunkiem że Orbiter za to płaci a nie on.
* Borys Kragin: inżynier; nie kojarzy statku, obsesyjnie szuka swojej żony. Kontaktuje się z Tivrem i ogólnie pomaga.
* Grigor Tarnow: ochrona; silny i wytrzymały, obsesyjnie walczy z 'eisenkriegerami'. Zginął walcząc z osami nihilosekta. KIA.
* Rita Stratos: medyczka, obsesyjnie skupiona na higienie i czystości. Bardzo creepy. Nihilosekt ją dopadł jak wszystkich, Arianna wyciągnęła.
* Karl Murnoff: dumny eks-Aurelionowiec, pragnął wielkich czynów. Po tym jak Mavidiz został opanowany przez nihilosekta, wpadł w jego ręce. Teraz - przechwycony przez Bladawira.
* Tara Ogniczek: xenobiolog, ambitna i zrobi wszystko dla kariery. "Słodka i niewinna". "To na pewno plotki na jej temat" - a potem śpi z profesorem. Specjalizuje się w dziwnych potworach. Nihilosekt ją dopadł jak wszystkich, Arianna wyciągnęła.
* OO Tivr: szybka korweta którą pozyskała Klaudia; wsparcie logistyczne i bojowe. Strzał uszkodził Mavidiz, ale uratował Eustachego przed nihilosektem. Na końcu - poleciał po pomoc.
* ONS Mavidiz: nieskanowalna jednostka o anomalnych cechach (Alteris), konstrukcja z Anomalii Kolapsu; tam pojawił się Nihilosekt i ten statek miał służyć transportowi Tary na Neikatis. Przechwycony przez Tivr pd załogi Inferni.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Pierścień Zewnętrzny

## Czas

* Opóźnienie: 2
* Dni: 5

## OTHER
### Kształt SC Mikiptur

Guns > Endurance > Armour > Speed > Utility

korweta dalekosiężna, 30 osób (oczekujesz: 40)

* Przednia Część:
    * Mostek: Miejsce komendy statku z konsolami nawigacyjnymi, systemami obronnymi i komunikacyjnymi.
    * Centrum Komunikacyjne: Z antenami i urządzeniami do komunikacji dalekiego zasięgu.
    * Strefa Relaksu: Miejsce wypoczynku z systemem rozrywki.
    * Kuchnia i Jadalnia: Przystosowana do gotowania i spożywania posiłków w warunkach mikrograwitacji.
    * Dział laserowe
    * AI Core: Diva
    * Magazyny
* Środkowa Część:
    * Boczne baterie laserów
    * Moduły Mieszkalne
    * Indywidualne kajuty dla załogi.
    * Łazienki
    * Kabina kapitana.
    * Żywność
    * Medical
    * Sala Gimnastyczna
    * Magazyn Uzbrojenia
    * Śluza główna
    * Orchard / "Sad": hydroponika; unikalna cecha
    * Life Support
* Tylna Część:
    * Magazyny
    * Zapasy wody, paliwa...
    * Maszynownia
    * Segment Napędowy: Główne silniki statku oraz systemy wsparcia życia.
    * Sala Techniczna: Dla napraw i konserwacji sprzętu w warunkach kosmicznych.
    * Pomieszczenie Awaryjne: Z kapsułami ratunkowymi i innym sprzętem awaryjnym.
    * Doki Serwisowe
    * Miny, drony minowate (ixiońskie i inne)
