## Metadane

* title: "Wypadek w Kramamczu"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [190724 - Odzyskana agentka Orbitera](190724-odzyskana-agentka-orbitera)
* [190925 - Wrobieni Detektywi](190925-wrobieni-detektywi)

### Chronologiczna

* [190724 - Odzyskana agentka Orbitera](190724-odzyskana-agentka-orbitera)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

brak

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Pustogor dostał distress call. W Kramamczu, dość mało istotnym miasteczku, doszło do Skażenia biologicznego. Najpewniej winny jest - oczywiście - Wiktor Satarail. Jako, że Pięknotka jest specjalistką od Wiktora, została wybrana na ochotniczkę przez Karlę. Do pomocy dostała Ksenię. Ksenia jest noktiańską terminuską; całkowicie odrzuciła kulturę, zwyczaje, podejście Noktis. Ksenia traktuje Noktis dokładnie tak jak każdy neofita traktuje kulturę którą opuścił. Innymi słowy, Karla nie łączy jej nigdy z akcjami powiązanymi z Noktis.

Kramamcz nie ma powiązania z Noktis. Tak więc Ksenia, jedna z lepszych wojowniczek i "dama z kijem w dupie" jest idealna do akcji. Będzie wsparciem Pięknotki, która dla odmiany zna się na Wiktorze.

Pięknotce udało się zdobyć pierwsze informacje wynikające z distress call - w Kramamczu jest "biofarma". Tam - tak jak jedwabniki produkują jedwab, tak na biofarmie produkowane są różnego rodzaju włókna. I tam doszło do infestacji - część owadów rozlazła się po terenie, część zaczęła robić rzeczy anomalne i niewłaściwe. Ogólnie - out of control. Ogłoszono alarm, ludzie pozamykali się w domach. Niebezpieczeństwo jest czerwone jeśli tam jest Wiktor, żółte jeśli nie. Pięknotka jest na forpoczcie, Ksenia pomoże w śledztwie.

Szczęśliwie, lokalne owady nie są niebezpiecznymi bioformami.

Opiekun owadów i właściciel kompleksu Włóknin Kramamczy zamknął się w biurze i nie chce z nikim rozmawiać, szuka sposobów jak to porozwiązywać. Ksenia powiedziała, że pozbiera ślady. Pięknotka, że zajmie się tematem. Pięknotka poszła do właściciela kompleksu - Dariusza. Dariusz przywitał ją z radością. Powiedział, że był Wiktor Satarail i on uwolnił robotnice. Pięknotka nie wierzy mu zupełnie... to nie jest w stylu Wiktora.

Pięknotka zażądała, by Dariusz pokazał Pięknotce królowe. Zaprowadził ją do podziemnych, ogromnych farm. Pięknotce od razu rzuciło się w oczy, że jakkolwiek kompleks jest zadbany, ale wyraźnie jest w średnim stanie jak nie gorszym. Pięknotka rozgląda się po lokalu i próbuje wykryć poziom stresu Dariusza. (Tp: KS). Historia, którą on mówi o Wiktorze zupełnie nie trzyma się kupy. Co więcej, cały ten kompleks jest po prostu... cały ten kompleks jest na granicy agonii. Jego stan techniczny jest po prostu beznadziejny. Co więcej - Dariusz sam nie wierzy w Wiktora.

Pięknotka szybko strzeliła do Trzewnia - jak wygląda sytuacja finansów Włóknina? Trzewń szybko odpowiedział - firma jest dochodowa, ale ma długi. Były swego czasu prowadzone różnego rodzaju inwestycje, przez poprzednika Dariusza, ale były nieudane. Dariusz jest aktualnym właścicielem. Co ciekawe, poprzedni właściciel teraz dołączył do Rexpapier. I Rexpapier konkuruje z Włókninem Kramamczy. Trzewń wykrył jeszcze, że od pewnego czasu Dariusz próbuje zatrudnić kompetentnych pracowników, ale nie jest w stanie - nie ma ich.

Pięknotka próbuje dojść do tego kto mu grozi. Co poszło nie tak. (Tp:P) Dariusz wszystko Pięknotce powiedział:

* próbuje uratować swoją firmę przed konkurencją ze strony Rexpapier
* Rexpapier najprawdopodobniej wprowadzili sabotażystę; nikomu nie stała się krzywda, ale ubezpieczenie go zrujnuje JEŚLI to nie Wiktor lub atak. Jeśli to pracownik typu zaniechanie...
* chciał wezwać terminusa z nadzieją, że terminus pomoże. Terminus chroni, pomaga i ratuje. A on już nie ma do kogo się zwrócić. Nie wie jak radzić sobie z czymś takim.
* Włóknin to firma rodzinna. On nie chce oddać jej za bezcen Rexpapier. Dariusz próbuje uratować honor swoich przodków.

Ksenia znalazła dowody, że tu nigdy nie było Wiktora Sataraila. Zabrała się za poszukiwania prawdy, faktów i cennych informacji. To jest potencjalny problem - Ksenia jest z tych bardziej "inkwizytorskich" terminusów. Ona wykona zadania perfekcyjnie, ale w niej nie ma litości.

Tak czy inaczej, Pięknotka wie jak to rozwiązać. Wpierw - pościągać do siebie wszystkie owady na wolności. Do tego celu używa królowych i swojej naturalnej syntezy plus odpowiednio zaawansowanego zaklęcia (TrMZ:S). Pięknotka skutecznie ściągnęła wszystkie owady, które pozostały na wolności. Szczęśliwie, faktycznie wszystko wskazuje na to, że poza jakimiś ocalałymi jednostkami (które i tak wymrą) jedyne co stało się jako poważna szkoda to szkody finansowe + wezwanie terminusów.

No i na to weszła Ksenia. Zaczęła serię pytań odnośnie tego, czemu to niby jest Satarail - to nie ma NIC wspólnego z władcą bagna.

Pięknotka zaczęła zadawać pytania pomocnicze, tendencyjne, by Ksenię przekierować na "on naprawdę wierzył w Sataraila ale to jest sabotaż". (Tr:P). Ksenia się, niestety, zorientowała co się dzieje. Powiedziała, że on nie wierzył w Sataraila i go wezwał bo...? Ksenia stwierdziła prostym językiem - każdy ma swoje miejsce w egzystencji a on je właśnie złamał. Powinien zostać usunięty. Dariuszowi puścił pęcherz ze strachu - połączenie oblicza Ksenii i jej zelotycznego podejścia...

Pięknotka rzuciła się do interwencji. Czy Ksenia nie sądzi, że sabotaż powinien być karany? Oczywiście, że tak. Ale jednocześnie - on musi zapłacić, bo zamiast powiedzieć Ksenii i Pięknotce prawdę kręcił jak szalony. Powinien był przyznać wszystko i całą prawdę Pustogorowi i prosić o pomoc. Pięknotka wysłała jej po hipernecie - czy Ksenia uważa, że ktoś by przyszedł? Ksenia odpowiedziała z absolutną wiarą, że ona by przyszła. Po to jest terminuską.

Co zatem zrobić z Dariuszem? Pięknotka zaproponowała coś, co działa po słabościach Ksenii - niech jakiś procent tego co on robi trafia do starych terminusów. Przymusowe zadania po niewielkiej marży - ale jednocześnie gwarantowane zamówienia. Ksenia po krótkim namyśle się zgodziła. Widzi korzyść z tego dla wszystkich. Ale zażądała publicznego przyznania się do braku wiary w terminusów i postanowienia poprawy. Dariusz zgodził się, bardzo skwapliwie. Pięknotka ma w duszy cichy facepalm... i tak się gościu wywinął bardzo małym kosztem - w końu to Ksenia.

Pięknotka połączyła się z adresem hipernetowym z którym nigdy nie chciała się łączyć. Kasjopea Maus.

Pięknotka od razu poszła za ciosem. Mała firma, która próbuje budować dla starych terminusów wygodne stroje i włókna, pod atakiem większego konkurenta. Sabotaże, podkupywanie pracowników, odbieranie umów i zleceń. I ona - Kasjopea Maus - dla odmiany po tej dobrej stronie. Wyciągająca wszystko na światło dzienne. Kasjopea zastrzygła uszami. Lubi takie działania, zwłaszcza, jeśli działa "za chwałę Pustogoru". To znaczy, że dla odmiany terminusi nie chcą jej robić krzywdy. Pięknotka powiedziała, że jej imię nie jest w to powiązane. (Tp: S). Kasjopea się ucieszyła. Bierze to.

Jednocześnie Ksenia powiedziała Pięknotce, że ma zamiar zrobić oficjalne śledztwo odnośnie tej sprawy - sabotaż, działania tego typu. Pięknotce nawet nie przeszkadza, że Ksenia wzięła to na siebie - tu nie będzie w stanie zrobić wiele zła swoim fanatyzmem i podejściem. Nie są w stanie jej kupić, nie zastraszą jej, nie mają jej praktycznie jak zatrzymać. Ksenia sama nie ma niczego by nic nie dało jej się zabrać. A nikt nie odważy się zaatakować terminusa o coś takiego.

Więc po raz pierwszy Pięknotka jest naprawdę zadowolona z wyniku śledztwa i z tego, że ona tam była...

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Wrabiają Wiktora Sataraila w atak na niewielką firmę, Włóknin z Kramamczy! Pięknotka i Ksenia wysłane by to naprawić! Pięknotka przyzwała z powrotem wszystkie królowe które pouciekały a Ksenia odkryła sabotaż wewnętrzny. Dariusz próbuje wyciągnąć ubezpieczenie. Pięknotka wezwała Kasjopeę (dziennikarkę) do pomocy i przekonała Ksenię, żeby ta odpuściła Dariuszowi tylko znalazła osoby winne tej sytuacji i prowokacji.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: zamiast pójść w zniszczenie Włóknina, skupiła się na uratowaniu tej niewielkiej firmy - poprosiła Kasjopeę i zneutralizowała Ksenię.
* Ksenia Kirallen: inkwizytorka i mistrzyni śledztw. Chce zniszczyć Włóknin po tym jak się dowiedziała o sytuacji; ale uspokoiła ją Pięknotka i Kasjopea. Buduje śledztwo w tej sprawie.
* Dariusz Kuromin: właściciel Włóknina Kramamczy; w rozpaczy powiedział że zaatakował go Wiktor, a tak naprawdę to był sabotaż.
* Mariusz Trzewń: analityk Barbakanu Pustogoru; powiedział Pięknotce o sytuacji finansowej Włóknina. Innymi słowy - mają przyczynę.
* Kasjopea Maus: wezwana jako rozpaczliwe wsparcie przez Pięknotkę; zaraz elegancko zabrała się do znalezienia taniego ale pewnego zlecenia dla Włóknina - dla weteranów terminuskich

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Kramamcz
                                1. Włóknin: niewielka firma rodzinna, chwilowo bankrutująca przez akty sabotażu i ataku innej firmy

## Czas

* Opóźnienie: 3
* Dni: 1
