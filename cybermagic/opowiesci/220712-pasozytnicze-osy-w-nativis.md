## Metadane

* title: "Pasożytnicze osy w Nativis"
* threads: historia-eustachego, arkologia-nativis, plagi-neikatis, zbrodnie-kidirona
* gm: żółw
* players: wikimet, anadia

## Kontynuacja
### Kampanijna

* [220723 - Polowanie na szczury w Nativis](220723-polowanie-na-szczury-w-nativis)

### Chronologiczna

* [200415 - Sabotaż Miecza Światła](200415-sabotaz-miecza-swiatla)

## Plan sesji
### Co się wydarzyło

* Arkologia Nativis dostała infekcję Trianai; zaczęło się od os
* Ekspert od biologii i biokontroli - Lycoris - stała się źródłem infekcji i złożyła doskonałą biokonstrukcję. 
    * Osy + ainshkery + kralvaty. A sama Lycoris -> mix
* Jej zaawansowany ścigacz klasy Remora ma własną dedykowaną TAI o imieniu Serentina.
* Czemu Lycoris zainfekowała własną arkologię?

### Co się stanie

* Cel Trianai: 
    * expand
* Typ Trianai:
    * ainshker, kralvath
    * roje owadów, skażenie biologiczne / choroby (madness)
* Kolejność:
    * osy -> osy + ainshkery + kralvathy

### Układ arkologii

* Aspekty:
    * Bardzo dużo roślin, drzew itp. Piękne roślinne miejsce.
    * Bardzo spokojna arkologia, silnie stechnicyzowana i sprawna.
    * Dominanta: faeril > drakolici, ale próba balansowania. Niskie stężenie AI.
    * BIA jako centralna jednostka dowodząca, imieniem "Prometeus"
* Struktura:
    * Wielkość: 4.74 km kwadratowych, trzy poziomy
    * Populacja: 11900 (Stroszek)
        * Operations & Administration: 1100
        * Extraction & Manufacturing: 3213
        * Medical: 355
        * Science: 360
        * Maintenance: 700
        * Leisure: 1000
        * Social Services: 240
        * Transportation: 800
        * Training: 229
        * Life Support: 500
        * Computer & TAI & Communications: 500
        * Engineering: 500
        * Supply / Food: 1200
        * Security: 300
    * Bogactwo: wysokie. Technologia + piękna roślinność.
    * Eksport: żywność (przede wszystkim), komponenty konstrukcyjne, hightech, terraformacja
    * Import: luxuries, prime materials, water, terraforming devices, weapons
    * Manufacturing: jedzenie, konstrukcja, terraformacja
    * Kultura: 
        * dominanta: piękno, duma z tego co osiągnęli, duma z tego że nie ma wojny domowej faeril/drakolici
        * wyjątek: brak.
    * Przestrzenie
        * rozrywki: parki, natura, zdrowe ciało - zdrowy duch
        * inne: hotele, laboratoria terraformacyjne, restauracje...
    * Zasilanie
        * solar + microfusion
    * Żywność
        * różnorodna; to miejsce jest źródłem żywności
        * przede wszystkim roślinna, ale też zawiera elementy zwierzęce (te najczęściej z importu)
    * Life support => terraformacja + filtracja
    * Comms => biomechaniczne okablowanie + normalne sieci
    * TAI => BIA wysokiego stopnia imieniem Prometeus
    * Starport => large
    * Infrastructure => high-tech, dużo półautonomicznych TAI, wszystko nadrzędne przez Prometeusa. Świetny transport publiczny. 
    * Access & Security => arkologia z silnymi blast walls, silnymi liniami dostępowymi.

Historia: 

* w trakcie wojny domowej sprzedawała żywność na 2 fronty co spowodowało znaczne bogactwo ale też częściową niechęć innych arkologii
* obecnie prowadzi ekspansje na struktury zrujnowane/opuszczone po wojnie żeby całkowicie się uniezależnić od dostaw z poza planety

### Sukces graczy

* Ostrzec inne arkologie, wezwać pomoc
* Zablokować śluzy, przejść w tryb ufortyfikowany

## Sesja właściwa
### Scena Zero - impl

5 dni temu.

Lycoris znajduje się w restauracji, je sobie z Jarlowem. Jarlow to promyczek nieszczęścia. Narzeka strasznie. Niech przekona kuzyna. Ma żałosne oczka. Nowe drzewa, z wyższą konwersją tlenową i takie piękne. Lycoris powiedziała, że perspektywę Rafała musi poznać. ZRÓBMY TO RAZEM! Interesujące te drzewa, nie widziała ich jeszcze, ale z przyjemnością z nimi popracuje nad badaniami.

Lycoris chce trafić do Rafała. "Jarlow, spróbuję przekonać Rafała, Ty przygotuj drzewa". Niech te drzewa są możliwe ale nie koniecznie. Nie wie czy zdoła. Lycoris chce dowiedzieć się co Jarlow ukrywa.

TrZ+3+3Og:

* V: Jarlow się przyznaje. Schował eksperymentalny ogród w arkologii. Nowy typ drzew, coś między drzewami i bluszczem, z neuropołączeniem.
* V: Jarlow dostarcza całość dokumentacji. 100%. Wszystko. I faktycznie drzewa będą odmontowane; tylko tyle by przetrwać.
* Vz: Wszystko poszło dokładnie jak powinno - kwarantannowane, bezpieczne itp.

Lycoris x Rafał ws drzew. Rafał szczęśliwy, arkologia działa prawidłowo - ale CO LYCORIS CHCE.

Lycoris chce więcej zielonych stref, powietrze dla ludzi, bardziej zieleńsze. By Nativis wyglądała coraz lepiej. Coraz piękniejsze otoczenie. Wszystko ruszy, przyspieszymy projekt terraformacyjny. Ale trzeba specjalnych drzew. Lycoris "mamy lepsze drzewa, efektywniejsze, potęga wzrośnie". Lycoris przekonała Rafała - rzadka odmiana. Trzeba szybko. Dobra okazja. Nie marnuj.

Rafał spojrzał na Lycoris... "ok, idziemy w to, wiesz w co robisz. Ty się znasz"

### Scena Właściwa - impl

Lycoris jest wyciągnięta z biovatu. Budzi się. Trzech kolesi w hazmatach. Jeden z nich wymiotuje, dwóch postawiło Lycoris na nogi.

Tr+3:

* X: "zainfekowany" zaatakował tego trzeciego.
* V: Lycoris wciągnęła kolesia do biovatu i zamknęła od środka, patrząc jak ten drugi w hazmacie walczy z jednym który jest zainfekowany.

Damian (hazmatowiec, medyk) panikuje. "Ale Staszek!". Staszek to koleś z medycznego którego atakuje infektant. Do pomieszczenia wleciały osy. Infektant zaatakował Staszka a osy na niego.

Lycoris nie chce czekać aż coś będzie dobrze, stwierdziła, że musi zaatakować. Wyskoczyła z biovatu, zwinęła Damianowi broń (emiter elektryczny) i strzeliła wyładowaniem. 

Tr (niepełny) +2:

* X: Staszek trafiony, pada na pysk
* V: sukces, infektant unieszkodliwiony

Pierwsze trafienie - Staszek dostał i padł (oops). Drugi strzał - infektant. Ten też padł na pysk. Szybko oddaje broń Damianowi "strzelaj w osy" "JAK!". Lycoris szybko ubiera się, Damian w osy pięścią i hazmatem odganiając je od szybko i drastycznie ubierającej się Lycoris. Lycoris, szybko ubrana, opanowała sytuację. Ale - coś porusza się w rurach...

TYMCZASEM Serentina. Wyczuła, że miragent jest aktywny - czyli awaryjny plan zadziałał. Ale niestety miragent jest w jakimś sektorze który jest odcięty. Serentina d'Remora poleciała więc szybko - dotrzeć przez punkty kontrolne do Lycoris.

TrZ (znasz teren) +3:

* X: lecisz spokojnie i szybko - nagle - security
    * komunikat: "podejrzewana... Lycoris... o spowodowanie klęski biologicznej... zatrzymać ją jak się ją zidentyfikuje"
    * Serentina: syntetyzuje głos Rafała nie znoszący sprzeciwu "mam eskortować podejrzaną". 
* X: sceptyczni, sprawdzają. COŚ jest za nimi. Serentina otwiera ogień.
* X: 3-4 ainshkery atakuje żołnierzy. Serentina nie jest w stanie zdjąć ich z żołnierzy
* X: Serentina leci do tunelu. Za Serentiną - 2-3 ainshkery.
    * Serentina strzela do rury gdzie jest woda, strzela, robi parę i jak ainshkery wejdą w promień - wyładowanie elektryczne +2Vg +3Og
* Og: Serentina usmażyła ainshkery, ale uległa uszkodzeniu. Zniszczony podsystem radiowy (komunikacja zewnętrzna), ale ainshkery zdjęte.
    * jednak podtrzymywanie życia. Life support mniej istotny :3.

Doleciała do drzwi za którymi powinna być Lycoris. Z tej strony uniknęła kilku grup infektantów i jakiegoś ainshkera. Po tej stronie - 4 zainfekowane osoby. Czekają. Serentina połączyła się z Lycoris używając KRZYKU (czyli krzyczy, serio XD).

* S: "Lycoris!"
* L: Lycoris cichutko do drzwi zdając sobie sprawę z ainshkera w rurach "czy dasz radę tu wejść albo otworzyć? XD"
* S: "Czterech zombiaków po tej stronie. Macie broń?"

Damian pomaga Staszkowi. Lycoris próbuje otworzyć drzwi. Serentina czyści wejście:

* V: Serentina unieszkodliwiła czterech infektantów. Ale zbliżają się 2 ainshkery - jeszcze daleko, ale.

Mimo ryzyka, Lycoris próbuje zamrozić Staszka w biovacie. Damian go szybko rozbiera i wsadza do biovatu, Lycoris zaczyna operację "zamrażam" a Serentina otwiera drzwi (przekonać drzwi, że ma uprawnienia i powinny się otworzyć) - daje radę.

TrZ+2:

* X: Gdy Damian rozbiera Staszka, Lycoris widzi że w miejscach ukąszenia przez osy są... bąble co na tym zainfekowanym. Staszek jest zarażony.
* V: Staszek trafia do biovatu.
* V: Lycoris skutecznie zamroziła Damiana. Staszek nic nie wie.
* X: AINSHKER Z RUR jest już blisko.

Serentina dotarła do Lycoris i ogień zaporowy w rury, niech Lycoris robi swoje bez ryzyka.

TrZ+2:

* V: Ainshkery ogień zaporowy; póki strzelasz w rury, póty nic nie wyjdzie.
* V: Bojaźń boża w ainshkerach. Nie wyjdą póki nie będą mieć Was osaczonych.

Lycoris ŻĄDA od Damiana prawdy. Niech powie prawdę. Nie czas na ukrywanie czegokolwiek.

TrZ+2:

* X: Damian ma złamane morale. Pękł. Płacze. Mało użyteczny. To tylko Damian z medycznego.
* V: Lycoris z liścia i GADAJ: przyznał. I ma nic nie mówić. Ale Staszek i Maciek mieli za zadanie reanimować.
* V: Damian powiedział prawdę. Lycoris to wszystko stworzyła. Ona stoi za plagą os, nie wiadomo czemu. I dlatego reanimowali miragenta. Bo tylko miragent może dojść do tego co chce Lycoris i o czym Lycoris myśli.

Zamrażamy:

* Vz: Lycoris, ekspert w swojej dziedzinie zamroziła Staszka. Będąc miragentem.

Lycoris (miragent) komunikuje się z Rafałem. Może się uda. Lycoris -> Rafał.

* "Rafał!"
* "DLACZEGO ZAINFEKOWAŁAŚ MOJĄ ARKOLOGIĘ!"
* "Czemu jest skażone, co się stało! I co mam zrobić by to odwrócić!"

Serentina cicho przypomina Lycoris, że kuzyn chciał ją aresztować. Lycoris - ustalmy hasło. Jeśli ja je pamiętam, to ja a nie ktoś inny. Lycoris powiedziała, żeby Rafał zatrzymał się przed operacją "wybuch gamma" (usuwanie rzeczy biologicznych tego terenu). Ona zobaczy co się da zrobić, kogo uratować itp.

Serentina próbuje - wziąć dwójkę (Lycoris, Damiana) i zrzucić Damiana przy pobliskim patrolu. Nie przyda im się do niczego a szkoda nie uratować.

TrZ (wiedzy i znajomości terenu, komunikacji itp) +2:

* Xz: Serentina skutecznie przeleciała, ominęła zagrożenia, porysowała się solidnie - i prosto na patrol sił Rafała. I patrol otworzył do niej ogień.
* V: Serentina zwinęła się z ognia i schowała w innym tunelu arkologii. 

Lycoris i Serentina opieprzyły ten patrol, oddając Damiana i oddaliły się w głąb. Patrol do Was -> uaktualniona mapa patroli. Lycoris z uśmiechem zabiera.

Lycoris z Serentiną próbują dojść do tego co jest praprzyczyną. Co spowodowało tą zarazę. Żeby nie zniszczyć czegoś bardziej istotnego - by móc odbudować.

TrZ (znajomość bazy) +2:

* X: Nie wszystko dokładnie da się zapamiętać, miragent ma luki.
* X: Nikt praktycznie nie wierzy. Bo Damian wypaplał, że Lycoris jest miragentem. (Rafał nie uwierzy)
* Vz: Lycoris i Jarlow pracowali z dziwnymi drzewami. Drzewa mają określoną ilość materii. Czyli coś innego - zapasy żywności.
    * Zauważyłyście - podczas wszystkich tych działań, WSZYSTKICH tych komunikacji - wszyscy skupiają się na TEJ arkologii - ale nie na zewnętrznych bytach gdzie jest żywność.

Lycoris i Serentina planują. O co chodzi. Lycoris wychodzi z tego, że to nie jest jakiś hive overmind. To coś innego. Pasożyt? Coś, co potrafi "wejść w głowę", bo inaczej oryginalna Lycoris NIGDY nie zdradziłaby arkologii. A jaki jest główny cel pasożyta? Nie zabić żywiciela i się rozprzestrzenić. Wniosek? Pasożyt chce się rozprzestrzenić. No i nie ma dość materiału biologicznego z samych drzew na osy ORAZ ainshkery. Więc skąd materiał biologiczny?

Żywność. Arkologia Nativis jest głównym eksporterem żywności. A wraz z żywnością może przejść ten cholerny pasożyt dalej.

Więc, Lycoris -> Rafał (komunikacja). Niech pod żadnym pozorem niczego nie eksportuje. Prośba od Lycoris - to jest cel pasożyta.

Tymczasem Damian ostrzegł Rafała, że ma do czynienia ze starym miragentem, nie doszło do pełnego przeniesienia.

Rafał. Zareagował na "Lycoris" z przekąsem. Lycoris (miragent) - jest problem. Żywność jest skażona. Jeśli wyeksportujemy gdziekolwiek, dojdzie do rozniesienia. A nasze dobre imię (Nativis) będzie zrujnowane, bo pasożyt. Stracimy dobre imię na amen. Będziemy skojarzeni z transportowaniem chorób. Trzykrotne sprawdzenie przez NIEZARAŻONYCH ekspertów. Pasożyt to drzewo, sprzężone z osami. Rafał - "nigdy nie zamontowałaś drzew". Lycoris - "mamy takie miejsce... przypadkiem". Rafał - jesteś tylko miragentem. Lycoris - "prawdziwa jest niedysponowana, jestem jedyną osobą doinformowaną która może coś wskórać. Jestem nośnikiem jej wiedzy TYLKO DLATEGO ŻE NIE NIĄ?" "No muszę przyznać że jesteś tak arogancka i bezczelna jak oryginał".

Rafał nie wierzy w to, że Lycoris jest poprawna - jest miragentem, nie udało się... a eksperci zewnętrzni COŚ WYKRYJĄ. A "oryginalna Lycoris" mogła SAMA to zaplanować, pod kontrolą pasożyta. Ja próbuję ocalić co stworzyłeś.

TrZ (faktycznie jako miragent JESTEŚ Lycoris) +4:

* X: Rafał chce zachować próbki pasożyta. Jako potncjalną broń biologiczną. I niech Lycoris to zdobędzie.
* V: Rafał przyznał się do czegoś Lycoris. Od pewnego czasu OSZCZĘDZAJĄ na niektórych rzeczach z żywnością. To sprawia, że nie chce ekspertów zew. Arkologia oszukiwała na rolnictwie.
* V: Rafał AKCEPTUJE to, że trzeba zatrzymać eksport. Że ryzyko za duże.

Rafał zaakceptował, że całkowicie wyczyszczą i zajmą się wszystkimi systemami rolniczymi. A ona - miragent - będzie się poruszać i niech Rafał odwlecze plan "operacja Gamma". Uda się uratować wszystko, potrzeba więcej czasu. +1 Vg.

* Vg: Rafał zaakceptował plan.

URATUJMY LYCORIS. I arkologię.

WYNIK:

* Arkologia Nativis ma dostęp do próbek Pasożyta (cykl drzewa - osy, atak mentalny itp.).
* Arkologia Nativis musiała płacić kary za niedostarczenie żywności do innych miejsc i ściągać z innych miejsc. Cios reputacyjny.
* Damian uciekł z Nativis; on wie co tu się stało.

## Streszczenie

W arkologii Nativis pojawiły się dziwne osy / drzewa i zainfekowały bioinżynier arkologii z rodu rządzącego, Lycoris. Z kopii biologicznej Lycoris złożono miragenta trzeciej generacji; ów miragent z pomocą swej Remory doszedł do tego, że celem os jest rozprzestrzenienie Pasożyta dalej, do innych arkologii. Miragent zablokował eksport i Nativis przeszła w tryb bezpiecznego kontrolowanego czyszczenia. I mają próbki Pasożyta jako potencjalną broń biologiczną.

## Progresja

.

### Frakcji

* Arkologia Nativis: próbki Pasożyta Neikatiańskiego (osy / drzewa)
* Arkologia Nativis: silny cios w reputację, bo miała eksportować żywność a zablokowała eksport (bo Pasożyt)

## Zasługi

* Serentina d'Remora: dedykowana inteligentna TAI połączona z szybkim ścigaczem klasy Remora; wyprowadziła miragenta Lycoris z kłopotów i opracowała plan jak uratować ludzi i jaki jest cel Pasożyta.
* Lycoris Kidiron: naukowiec i ekspert od bioinżynierii i terraformacji; oryginał spowodował Plagę pod wpływem Pasożyta. Miragent zrozumiał cel Pasożyta - rozprzestrzenić Pasożyta do innych arkologii - i współpracując z Rafałem uniemożliwił tą sytuację.
* Rafał Kidiron: bezwzględny szef ochrony Nativis należący do kasty rządzącej i kuzyn Lycoris; dobrze zarządza Nativis słuchając rad i zostawiając ekspertom działania. Opanował Pasożyta z pomocą miragenta Lycoris i pozyskał próbki Pasożyta jako broń biologiczną. 
* Jarlow Gurdacz: wybitny architekt i ekspert od piękna Nativis; ściągnął drzewa które okazały się być Pasożytem i umieszczając je by przetrwały niedaleko lifesupport zaczął Plagę.
* Damian Marlinczak: pomniejszy medyk; uczestniczył w reanimacji miragenta Lycoris. Stracił dwóch przyjaciół, dowiedział się o Pasożycie i powiedział Rafałowi o miragencie. Nikt ważny, acz zna sekret Nativis i nie chce "zniknąć".

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Nativis: klejnot Neikatis, jednocześnie chłodna i wymagająca perfekcji. Nie ma wojny domowej ale eksportuje żywność i bogaci się innymi arkologiami.
                        1. Poziom 1 - Dolny
                            1. Północ - Stara Arkologia: opanowana przez Pasożyta Neikatis; sektory starsze i bardziej siermiężne niż reszta arkologii
                            1. Wschód
                                1. Farmy Wschodnie: zainfekowane przez Pasożyta, nie doszło do eksportu. Dokładnie przeczyszczone przez miragenta Lycoris.
                            1. Zachód
                                1. Stare TechBunkry: w jednym z nich przebudzono miragenta Lycoris by poradzić sobie z infekcją Pasożyta

## Czas

* Opóźnienie: -6554
* Dni: 9

## Konflikty

* 1 - Lycoris chce dowiedzieć się co Jarlow ukrywa o tych dziwnych drzewach
    * TrZ+3+3Og
    * VVVz: Jarlow przyznał się że już je postawił w arkologii, dostarcza całość dokumentacji i w pełni współpracuje z Lycoris.
* 2 - Lycoris jest wyciągnięta z biovatu. Budzi się. Trzech kolesi w hazmatach. Jeden z nich wymiotuje, dwóch postawiło Lycoris na nogi.
    * Tr+3
    * XV: Lycoris uratowała Damiana przez infektantami Pasożyta, acz Staszek został zainfekowany
* 3 - Lycoris wyskoczyła z biovatu, zwinęła Damianowi broń (emiter elektryczny) i strzeliła wyładowaniem. 
    * Tr (niepełny) +2
    * XV: Staszek zastrzelony wyładowaniem, infektant też :D.
* 4 - Serentina d'Remora poleciała więc szybko - dotrzeć przez punkty kontrolne do Lycoris.
    * TrZ (znasz teren) +3:
    * XXXXOg: zatrzymana w punkcie kontrolnym, ale ainshkery ich napadły i się prześlizgnęła. Poraziła ainshkery prądem, ALE uszkodziła swój lifesupport.
* 5 - Damian pomaga Staszkowi. Lycoris próbuje otworzyć drzwi. Serentina czyści wejście
    * TrZ+2
    * V: Serentina unieszkodliwiła 4 infektantów łamiąc im nogi.
* 6 - Mimo ryzyka, Lycoris próbuje zamrozić Staszka w biovacie.
    * TrZ+2
    * XVVX: Staszek zarażony, ale w biovacie i zamrożony. Ainshkery z rur za blisko.
* 7 - Serentina dotarła do Lycoris i ogień zaporowy w rury, niech Lycoris robi swoje bez ryzyka
    * TrZ+2
    * VV: Ainshkery z rur odparte, nie wyjdą. Ogień zaporowy.
* 8 - Lycoris ŻĄDA od Damiana prawdy. Niech powie prawdę. Nie czas na ukrywanie czegokolwiek.
    * TrZ+2
    * XVV: Damian ma złamane morale, powiedział "Lycoris" że jest miragentem itp. I całą prawdę jaką zna.
* 9 - Serentina próbuje - wziąć dwójkę (Lycoris, Damiana) i zrzucić Damiana przy pobliskim patrolu. Nie przyda im się do niczego a szkoda nie uratować.
    * TrZ (wiedzy i znajomości terenu, komunikacji itp) +2
    * XzV: Serentina ostrzelana przez patrol, ale schowała się w tunelu. Oddała Damiana.
* 10 - Lycoris z Serentiną próbują dojść do tego co jest praprzyczyną. Co spowodowało tą zarazę. Żeby nie zniszczyć czegoś bardziej istotnego - by móc odbudować.
    * TrZ (znajomość bazy) +2
    * XXVz: Damian wypaplał że Lycoris to miragent i miragent ma luki, ale widzą o co chodzi - to kwestia eksportu żywności (i Pasożyta)
* 11 - Lycoris przekonuje Rafała że to ważne by zatrzymać żywność
    * TrZ (faktycznie jako miragent JESTEŚ Lycoris) +4
    * XVVVg: Rafał zachowuje próbki Pasożyta, Lycoris wie że oszczędzają na żywności, ale zatrzymają eksport. Plan będzie wykonany.

## Kto

* Lycoris Kidiron, 57 lat (ze wspomaganiami drakolickimi dającymi jej efektywność 3x-latki) (objęta przez Wiki)
    * Wartości
        * TAK: Bezpieczeństwo, Osiągnięcia ("dzięki mnie ta arkologia będzie bezpieczna i osiągniemy najwyższy poziom eksportu żywności")
        * NIE: Konformizm ("nikt, kto próbował się dopasować do innych nie doprowadził do postępu")
    * Ocean
        * ENCAO: +-0+0 (optymistyczna, nieustraszona, lubi się dogadywać z innymi, nie patrzy na ryzyko)
    * Silniki:
        * TAK: Kapitan Ahab: polowanie na białego wieloryba do końca świata i jeszcze dalej, nieważne co przy tym stracimy. (tu: "zrobię najlepszą arkologię, lepszą dla ludzi niż planeta macierzysta")
    * Rola:
        * ekspert od bioinżynierii i terraformacji, pionier (zdolna do przeżycia w trudnym terenie)

* Rafał Kidiron
    * Wartości
        * TAK: Achievement, Power("wraz z sukcesami przychodzi władza, wraz z władzą musisz osiągać więcej sukcesów")
    * Ocean
        * ENCAO: 00+-0 (precyzyjny i dobrze wszystko ma zaplanowane, lubi się kłócić i nie zgadzać z innymi)
    * Silniki:
        * TAK: Duma i monument: to NASZE sukcesy są najlepsze i to co MY osiągnęliśmy jest najlepsze. Musimy wspierać NASZE dzieła i propagować ich chwałę.
    * Rola:
        * kuzyn Lycoris, security lord, w wiecznym konflikcie z Gurdaczem (security & money - beauty)
* Jarlow Gurdacz
    * Wartości
        * TAK: Hedonism, Face NIE: Conformism ("najpiękniejsza arkologia, klejnot Neikatis - i to nasza!")
    * Ocean
        * ENCAO: 000++ (kocha piękno i ma mnóstwo świetnych pomysłów)
    * Silniki:
        * TAK: Artystyczna dusza: Stworzyć piękno, pokazać je całemu światu lub wręcz przeciwnie, zachować dla godnej publiczności. Być docenionym.
    * Rola:
        * ekspert od roślinności, architektury i zdrowia arkologii, w wiecznym konflikcie z Rafałem Kidironem (security & money - beauty)

* 
    * Wartości
        * TAK: 
        * NIE: 
    * Ocean
        * ENCAO: 
    * Silniki:
        * TAK: 
    * Rola:
        * 

