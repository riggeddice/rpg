## Metadane

* title: ""
* threads: planetoidy-kazimierza, sekret-mirandy-ceres
* gm: żółw
* players: kić, anadia

## Kontynuacja
### Kampanijna

* [?](220503-krolowa-przygod-PLC)

### Chronologiczna

* [?](220503-krolowa-przygod-PLC)

## Plan sesji
### Co się wydarzyło
#### Co się wydarzyło w tym terenie?

* Inicjacja bazy Ukojenie Barana
    * mag eternijski, Bruno Baran, założył niewielką bazę daleko od Eterni z częścią ludzi 12 lat temu
    * automatycznie zaczął się kręcić profit
    * wśród górników Elsa Kułak została nieformalną liderką. Niestety, "demokracja" nie przypasowała Baranowi. Odizolował się.
* Migracja Mardiusa
    * noktiańscy komandosi Tamary Mardius złożyli tu swój nowy dom wśród lapicytu. Podeszli do górników i Barana z neutralnością. Ot, są.
    * mało kto jak noktianie umieją żyć w kosmosie. W tym miejscu znaleźli świetne miejsce i ich umiejętności stały się bardzo przydatne.
    * siły Mardius znalazły opcję szmuglowania przez statek Gwiezdny Motyl.
* Imperium Blakvela
    * 3 lata temu pojawił się eternijski szlachcic, Ernest Blakvel. Stwierdził, że władza w kosmosie to coś dla niego. Zaczął umacniać się w tym terenie.
    * Blakvel i Mardius weszli w stały konflikt. Mardius chroni miejscowych, Blakvel chce ich podbić.
* Pojawienie się Strachów
    * 2 lata temu pojawiły się Strachy. Stworzone z programowalnej materii (morfelin), nie wiadomo czemu są i czym są.
    * Mardiusowcy aktywnie chronią lokalnych.

#### Strony i czego pragną

* Górnicy w Domenie Ukojenia i mały biznes: 
    * CZEGO: zachować niezależność, handel itp. Przetrwać. Zarobić.
    * JAK: sami się zbroją
    * OFERTA: standardowa oferta terenowa, plotki itp.
    * KTOŚ: Elsa Kułak (przełożona, trzyma twardą ręką), Antoni Czuwram (górnik, solidny stateczek i niezły sprzęt - dużo wie), Kamil Kantor (szuka ojca Leona który odkrył sekret Mirnas i zaginął), Bruno Baran (mag eternijski i założyciel; Strachy to destabilizacja Blakvela przy obecności lapizytu), Kara Szamun (młódka z rodziny Szamun, pilot i nawigator transportowca na Asimear.)
* Przemytnicy Blakvela: 
    * CZEGO: zdobyć prymat w Domenie Ukojenia
    * JAK: zmiażdżyć Mardiusa, potem przejąć Talio i zestrzelić bazę Barana.
    * OFERTA: świetne rozeznanie w pasie, największa siła ognia, wiedza o SWOICH skrytkach, wsparcie piratów etnomempleksu Fareil (synthesis oathbound pro-virt, pro-numerologia)
    * KTOŚ: Ernest Blakvel (arystokrata eternijski)
* Przemytnicy Mardiusa: 
    * CZEGO: zregenerować siły, odzyskać Tamarę, ukryć się przed Blakvelem
    * JAK: w ciągłej defensywie, ufortyfikowanie Sarnin, operacje militarne anty-Blakvel
    * OFERTA: wiedza o Miragencie i Aleksandrii, świetne rozeznanie w pasie, wiedza o SWOICH skrytkach, znajomości wśród lokalnych.
    * KTOŚ: Tamara Mardius (eks-komandos noctis, Alexandria), Deneb Ira (regeneruje komandosów po sprawie z Aleksandrią)
* Taliaci: (mieszkańcy Talio)
    * CZEGO: rzeczy wysokotechnologiczne
    * JAK: handel, współpraca
    * OFERTA: mają dostęp do WSZYSTKICH frakcji i są wiecznie neutralni, reaktory termojądrowe na podstawie deuteru i trytu na Talio.
* Strachy
    * CZEGO: Brak woli. Reakcja. Zniszczyć wszystko.
    * JAK: flota inwazyjna
    * OFERTA: zniszczenie, targetowane lub nie.

### Co się stanie

* .

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Sesja właściwa - impl




## Streszczenie



## Progresja

* Bartek Wudrak: chce zrobić wpierdol Antosowi Kuraminowi za zamknięcie Kary w szafie.


* .

### Frakcji



* Rozdzieracze Berdysza: praktycznie zniszczeni; zostało 2 + Berdysz i nie mają statku. Ale są na wolności i gotowi do 

## Zasługi

* Miranda Ceres: 
* Anna Szrakt: 


* Helena Banbadan: 
* Romana Kundel: 
* Antos Kuramin: 
* Ursyn Uszat: barman w Koronie; 
* SC Królowa Przygód: 

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Pas Teliriański
                1. Planetoidy Kazimierza
                    1. Domena Ukojenia
                        1. Szamunczak

## Czas

* Opóźnienie: 6
* Dni: 2

## Konflikty

* 1 - 
    * 
    * 
* 2 - 
    * 
    * 
* 3 - 
    * 
    * 
* 4 - 
    * 
    * 
* 5 - 
    * 
    * 
* 6 - 
    * 
    * 
* 7 - 
    * 
    * 
* 8 - 
    * 
    * 
* 9 - 
    * 
    * 
* 10 -
    * 
    * 

## Kto tu jest
### Załoga Królowej

* Prokop Umarkon: 
    * rola: kapitan, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: extravert, agreeable (outgoing, enthusiastic, trusting, empathetic)
    * values: power + conformity
    * wants: pragnie zostać KIMŚ, wydobyć się z bagna
* Łucjan Torwold: 
    * rola: administrator / logistyka, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: neurotic, conscientous (very worried, scared of cults, very organized)
    * values: security + conformity
    * wants: przesądny; boi się duchów i kultu. Narzeka. Chce bezpiecznego, prostego życia
* Gotard Kicjusz: 
    * rola: technical guy, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: open, low-neuro, low-consc (extra stable, spontaneous, eccentric)
    * values: self-direction
    * wants: przygód, odzyskać rodzinę
* Helena Banbadan "Medyk"
    * rola: eks-pirat, medyk, wojownik
    * personality: neuro, low-extra, agreeable (raczej cicha, odwraca oczy, raczej empatyczna)
    * values: security (self>others), hedonism (narcotics)
    * wants: odkupienie, bezpieczeństwo, być sama daleko od tego wszystkiego
* Wojciech Kaznodzieja "Profesor"
    * rola: eks-pirat, hacker, psychotronik, były neuropilot Orbitera
    * personality: open, low-neuro (arogancki, otwarty na możliwości, lubi gadać o anomaliach i zbiera opowieści o duchach)
    * values: power, security
    * wants: JESTEM EKSPERTEM, chwała i prestiż
* Szymon Dyrlan
    * Wartości
        * TAK: Achievement, Family (tu: Blakvel), Security (dla jednostki, załogi i siebie)
        * NIE: Stimulation, Hedonism
    * Ocean
        * ENCAO:  +---0
        * Wyrachowany, szuka własnego interesu
        * Beztroski i swobodny; żyje według własnych przekonań
        * Niestały i zmienny, często zmienia zdanie i podejście
        * Odważny, śmiały i przedsiębiorczy
    * Silniki:
        * TAK: Ćma w płomienie: gwiazdy które świecą najjaśniej gasną najszybciej; nie chcę żyć niezauważony. Mogę zginąć, ale BĘDĘ ZAPAMIĘTANY! Żyjesz raz!
        * TAK: Kolekcjoner: kolekcjoner broni różnego rodzaju
        * NIE: Ochraniać słabszych: ja jestem w stanie się obronić i nie zostanę zniszczony. Ich trzeba chronić by nic złego ich nie spotkało.
            * interpretujemy jako: "słabi nie nadają się do niczego, tylko do eksploatacji"
    * Rola:
        * Soldier, Advancer, Blakvelowiec

### Pasażerowie

* Anna Szrakt
    * rola: wojownik (multi-weapon), "opiekunka do dzieci", nauczycielka życia w społeczeństwie
    * personality: low-open, low-extra, low-neuro (praktyczna, raczej cicha ALE JAK POWIE, bardzo stabilna, taka "zimna zakonnica, sucz z piekła")
    * values: community / tradition, benevolence (społeczeństwo, trzymamy się razem, pomagamy sobie BO MASAKRA BĘDZIE)
    * wants: pomóc osobom dookoła, dać drugą szansę, chronić "swoje" dzieciaki
* Bartek Wudrak
    * rola: delinquet teen, nożownik, aspiruje do bycia górnikiem, 17
    * personality: low-agree, low-cons, low-extra, open (spontaniczny, cichy, nie planuje, 0->100 i atak)
    * values: security, community
    * wants: nikt mnie nie skrzywdzi, wolność od wszystkiego, chronić swoich, Romana ;-)
* Romana Kundel
    * rola: delinquet teen, złodziejka, aspiruje do bycia górnikiem, 16
    * personality: low-agreeable, open, low-extra (wycofana, lekko socjopatyczna, otwarta i chętna próbowania innych rzeczy)
    * values: hedonism, self-direction
    * wants: nikt mi nie będzie rozkazywał, kasa na virt lub kryształy, lepsze życie
* Kara Prazdnik
    * rola: delinquet teen, aspiruje do bycia administratorem i/lub idolką, ekspert od scamów, 17
    * personality: conscientous, extraversion, agreeable (zorganizowana, metodyczna, wie co robić, bardzo głośna i kocha śpiewać, artystyczna inklinacja)
    * values: benevolence, tradition (religion)
    * wants: dowodzić innymi, wykazać się w sytuacji kryzysowej

### Luxuritias, Źródło Obfitości

* Antoni Piscernik
    * Wartości
        * TAK: Hedonism, Universalism, Power
        * NIE: Self-direction, Achievement
    * Ocean
        * ENCAO:  +0--0
        * Łatwo się rozprasza
        * Amoralny, jedyną zasadą jest korzyść
        * Odważny, śmiały i przedsiębiorczy
    * Silniki:
        * TAK: Pogarda wobec INNYCH: pokazać komuś swoją głęboką pogardę do X (osoby/grupy), publicznie obrazić i poniżyć X. Demonstracja obrzydzenia / osłabienie X.
        * TAK: Tępić debili: nienawidzę głupoty do poziomu NUKLEARNEGO. Niech cierpią. Sama przyjemność. I zemsta.
        * NIE: Corrupted guardian: zrobiono mi coś strasznego. Nie jestem już w pełni osobą. Ale nikogo innego nie spotka to co mnie, ochronię innych.
            * interpretujemy jako: "they all shall fall they all shall bow to power"
    * Rola:
        * panicz, niebezpieczny i amoralny luxuritias; polujemy na Karę
* Mateusz Piscernik
    * Wartości
        * TAK: Tradition, Conformity, Family (tu: Luxuritias)
        * NIE: Universalism, Face
    * Ocean
        * ENCAO:  --+00
        * Stabilny emocjonalnie, trudno wyprowadzić z równowagi
        * Nie znosi być w centrum uwagi
        * Dyskretny, sekrety weń są jak w grób
    * Silniki:
        * TAK: Awans za wszelką cenę: albo lepsza firma albo wojna o stanowisko z innymi. To stanowisko będzie moje.
        * TAK: Harmonia naturalna: balans między technologią i naturą, utrzymanie cudów natury. Lasy Amazonii.
        * NIE: Ratunek przed chorą miłością: mój kuzyn się zakochał w osobie ze straszną przeszłością i osobowością. Ślub / partnerstwo nie może dojść do skutku!
            * interpretujemy jako: miłość dla Antoniego za wszelką cenę
    * Rola:
        * młodszy kuzyn wspierający Antoniego
* Nadia Kantarin
    * Wartości
        * TAK: Humility, Power, Face
        * NIE: Self-direction, Tradition
    * Ocean
        * ENCAO:  0+--0
        * Impulsywny, nie do końca kontroluje emocje
        * Opryskliwy, bezceremonialny i obcesowy
        * Nikomu nie ufa i nie polega na innych
    * Silniki:
        * TAK: Przyjazny Kameleon: adaptacja do sytuacji społecznych, każdemu pokazuje taką twarz jaka jest najbardziej lubiana. Chce być lubiany i kochany.
        * TAK: Queen Bee: pokazać swoją reputację i wyższość nad innymi swojej grupce osób dookoła. Np. przyjezdna mg na konwentach z adoratorkami vs osoba na środku;-)
        * NIE: Zrozumieć kulturę: dowiedzieć się jak najwięcej o grupie i kulturze, o ich nawykach, siłach i sztuce.
            * interpretujemy jako: "there is only power and one culture. Luxuritias is the strongest"
    * Rola:
        * strażniczka i agentka chroniąca Piscerników


*  
    * Wartości
        * TAK: 
        * NIE: 
    * Ocean
        * ENCAO:
    * Silniki:
        * TAK: 
        * TAK: 
        * NIE: 
            * interpretujemy jako: 
    * Rola:
        * 

### Szamunczak

*  Anton Kuramin
    * Wartości
        * TAK: Tradition, Face, Achievement 
        * NIE: Conformity, Humility
    * Ocean
        * ENCAO:  -0+-0
        * Nie dba o to co inni czują i nie interesuje się ich problemami
        * Nie lubi ryzyka, woli wszystko przemyśleć kilka razy
        * Punktualny i nie tolerujący spóźnień
    * Silniki:
        * TAK: Broken soul: zostawcie mnie wszyscy w spokoju. Nie chcę wiedzieć, nie chcę interaktować, nie chcę musieć. Za dużo. Dążę do samotności i ciszy.
        * Do the right thing: niezależnie od tego co jest wygodne czy tanie, własne zasady i własny kompas moralny są najważniejsze. By spojrzeć w lustro.
        * NIE: 
            * interpretujemy jako: 
    * Rola:
        * 
