## Metadane

* title: "Kuźnia soli i ognia"
* threads: brak
* motives: ?
* gm: żółw
* players: fox, igor, magdawrr

## Kontynuacja
### Kampanijna

.

* [210306 - Wiktoriata](210306-wiktoriata)
* [240327 - Syrenopajęczak politycznie skomplikowany](240327-syrenopajeczak-politycznie-skomplikowany)

### Chronologiczna

.

* [210306 - Wiktoriata](210306-wiktoriata)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * Archenemy "War Eternal":
        * "Back against the wall | In danger of losing it all | Search deep inside | Remember who you are"
        * Zacietrzewienie w walce między Solą oraz Ogniem.
    * Inspiracje
        * "Twisted Man Looking in the window", creepypasta o potworze chroniącym człowieka przed mordercą
        * "Kto się boi Virginii Woolf", fragment z zemstą osoby, która nie powinna była zginąć
        * w sumie nie wyszło
     * Uczucie dominujące
        * "To skomplikowane" + "mhm, jak to teraz rozwiążemy"
* Opowieść o (Theme and vision):
    * "Sekrety małych miasteczek"
    * "Chesterton's Fence - jeśli nie wiesz czemu coś tu jest, nie dotykaj tego"
    * Lokalizacja: ???
* Po co ta sesja?
    * Jak wzbogaca świat
        * Manifestacja Kuźni, do przekazania Verlenlandowi
    * Czemu jest ciekawa?
        * Dużo klocków wchodzących ze sobą w interakcje
        * Jak to naprawić?
    * Jakie emocje?
        * "Mściząb znowu coś odpierniczył..." -> "...co on zrobił... czemu..."
            * Od lekkiej frustracji do pełnego szoku - Fallen Verlen
            * I zrozumienie DLACZEGO upadł. I swojej roli w tym upadku.
        * "Coś tu jest poważnie nie tak, ale damy radę" -> "CZEMU NIE WEZWAŁYŚMY POMOCY!"
            * Od przekonania o możliwościach do uzmysłowienia sobie z CZYM walczą
            * Dlaczego Verlen zarządzający tym terenem nic nie robi i go nie ma?!
        * "Nie chcę musieć wybierać Miasteczko / Mściząb!"
            * prawdziwa pokusa związana z Darami Leniperii - I can get it all
            * dylemat. Mściząb w końcu mógł być 'sensowny', ale nie miał szczęścia
        * "Rozwalę tego potwora!!! Za Mścizęba!!!"
            * widząc, co Leniperia zrobiła Mścizębowi.
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * .
    
    * the-corruptor: Leniperia, istota Esuriit/Interis. Stopniowo pozbawia Cię wszystkiego, wzmacniając Cię darami, jeśli je przyjmiesz. Wzmacnia Twoją obsesję, zabierając wszystko inne. Wzmacnia Cię krwawymi ofiarami. Tym razem dorwała Mścizęba, młodego Verlena, który rozpaczliwie pragnął pokazać Ariannie że jest lepszy od niej
    * the-addicted: Mściząb Verlen. Jego narkotykiem jest potęga i akceptacja Rodu, jest możliwość upokorzenia Arianny i udowodnienia, że jest lepszy od niej. Coraz bardziej traci wszystko by stać się jak ona. Unchained.
    * gift-is-anthrax: Leniperia zostawia podarunki i możliwości wzmocnienia się; te podarunki wzmacniają, ale kosztem powolnego uzależnienia od Leniperii.
    * dziwna-zmiana-zachowania: Mściząb zrobił się kompetentny oraz bardzo agresywny wobec Arianny; rywalizuje z nią i może nie daje rady, ale jest bliżej niż powinien być. Jest też okrutniejszy.
    * hartowanie-verlenow: tu widać, co się dzieje, gdy 'słaby niegodny Verlen' napotyka siłę, która próbuje z tego skorzystać. Jak łatwo 'słabi Verlenowie' przechodzą na Drugą Stronę. Wyraźna ich słabość.
    * kult-mroczny: stworzony sztucznie, przez Leniperię, ale zawsze; więcej osób należy do sił Potwora i więcej osób próbuje być lepszymi i płacą więcej i więcej. Część z nich 'nawrócił' Mściząb. Część przyjęło dary.
    * lost-my-way: Mściząb. Wczorajszy Mściząb by krzyczał i płakał widząc potwora, jakim się ostatecznie stał. Dzisiejszy poświęca ludzi by się wzmocnić i służy Leniperii.
    * pakt-z-diablem: Leniperia może uratować Mścizęba, może wzmocnić Ariannę czy Viorikę. Ale koszt - niekoniecznie świadomy - to uzależnienie ich od siebie.
    * potwor-lewiatan: Leniperia nie może być zniszczona normalnymi metodami, jest niesamowicie potężna, działa przez Agentów i przez swoje Manifestacje / spawny.
    * pulapka-bardzo-skomplikowana: Leniperia szuka następnego agenta, Mściząb zasugerował (nieświadomie) Ariannę. Cała sesja jest pułapką na Ariannę i Viorikę.
    * zawisc-mordercza: Mściząb wobec Arianny - jego skaza sprawiła, że Leniperia miała otwarcie i mogła Mścizęba Dotknąć.
    * potwor-spawner: Leniperia jest w stanie wykorzystać część swojej energii by móc spawnować inne potwory.
    * koniecznosc-pelnej-dyskrecji: Arianna i Viorika działają na terenie innego Verlena, któremu nie zależy tak na ludziach w swojej domenie. A one chcą zatrzymać Mścizęba jak on robi głupoty.
    * ratowanie-dzieciaka: Karasiożer Verlen jest zapatrzony w Mścizęba, jest powiązany z siłami na tym terenie. Mściząb przysposabia go do dołączenia do Leniperii.
* O co grają Gracze?
    * Sukces:
        * Uratować Mścizęba
        * Uratować Miasteczko
        * Karasiożer jest bezpieczny przed Leniperią
        * Odegnać Leniperię z Verlenlandu
    * Porażka: 
        * W służbie Leniperii
        * Śmierć zarówno Mścizęba jak i Miasteczka
        * Karasiożer przyjął dar Leniperii
* O co gra MG?
    * Highlevel
        * Ciekawa sesja dla Magdy, podczas której może kombinować i szukać ciekawych rzeczy
        * Pokazać Igorowi i Magdzie jak gra Fox jako społeczna jednostka "pozytywna"
            * -> Magda jako mag, Fox jako destabilizująca się oficer w Custoris-class, Igor jako syn marnotrawny
            
    * Co chcę uzyskać fabularnie
        * Spróbuję dołączyć je do Leniperii. Niech się Skażą.
            * Używając Interis zabiorę im umiejętność ostrzeżenia o Leniperii.
        * Karasiożer dołącza do Leniperii.
        * Zarówno Miasteczko jak i Mściząb są zniszczone.
        * Nie są w stanie ostrzec nikogo przed Leniperią; ona nadal tu jest i się żywi.
        * Wszyscy są przekonani, że to wszystko jest winą Arianny - tak jak Elena.
        * Pojawia się Mroczny Oddział Leniperii.
* Default Future
    * Miasteczko zostanie zjedzone
    * Mściząb wciągnie pod kontrolę Leniperii Karasiożera Verlena
    * Leniperia będzie w stanie rozprzestrzenić się na kolejne miejsca; ma niewielki, dobrze ukryty oddział
* Dilemma: Mściząb CZY Miasteczko? Wezwać pomoc CZY samemu?
* Crisis source: 
    * HEART: "The child who receives no warmth will burn the village just to feel its warmth"
    * VISIBLE: "Znikają ludzie w niedalekiej okolicy"
        
### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

* Na tym terenie znajduje się Kuźnia. Źródło niebezpiecznych energii magicznych, pragnąca stworzyć perfekcyjnych wojowników.
    * Energie: Exemplis (dążenie do perfekcji) + Unumens (jednomyślność, jedna wola, jedne działania)
    * Zneutralizowano Kuźnię farmą świń, na której też są glukszwajny, dawno temu. Nikt nie pamięta czemu Farma tam była; ona była ZAWSZE.
* Farma Świń znajduje się na doskonałym terenie, na którym biznesowo lepiej postawić Uzdrowisko.
    * Farma była uznana ogólnie przez ludzi za coś, co przeszkadzało od dawna.
* W wyniku machinacji udało się przekupić syna opiekuna Świń. Farma przeszła pod kontrolę włodarzy Uzdrowiska.
* Ojciec walczył ze wszystkich sił; udało się go unieszkodliwić przez włodarzy Uzdrowiska.
* Kuźnia się odrodziła. 400 dni po wydarzeniach zaczęła podnosić paranoję oraz zaczynać kulty Soli oraz Ognia.
    * Kuźnia spawnuje Świniotaury, polujące na ludzi. Ludzie obracają się przeciw innym ludziom.
    * W odpowiedzi pojawił się Wieprzkielet, ratujący ludzi od Kuźni przez ich eksterminację lub zwalczanie. To echo Ojca.
* Powracają do domu: Igor (lokals, świetny wojownik i zwiadowca), Fox (kuzynka Igora; soulbound w Custorisa) oraz Magda (czarodziejka, utrzymująca Fox przy życiu)

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

* Faza 0:
    * Odparcie ataku Świniotaurów i Ognia w pociągu jadącego do Świńkuźni
* Faza 1:
    * Sól jest ufortyfikowana wśród jedzenia; Ogień wśród przemysłu i broni. Dworek jest niezależny.
        * Rodzina Zespołu jest wśród Soli
    * Ogień zaatakuje Sól; rajd mający na celu odwrócić uwagę od kradzieży jedzenia.
    * Sól planuje uderzenie w przetwórnie; tam jest broń
    * Na tereny farmy świń porywani są ludzie (tam będą przerobieni w Świniotaury)
    * len: 6
* Faza 2:
    * Rozwiązanie ostateczne - Ogień decyduje się na wykorzystanie materiałów wybuchowych i traktorów. Sól - na zatrucie jedzenia.
    * Kuźnia prowadzi do wielkiej wojny, każdy przeciwko każdemu. Świniotaury pomagają obu stronom.
    * len: 6
* Overall
    * chains:
        * zaufanie Zespołowi Ogień / Sól: 3/3
        * poziom kultu: 4/4
        * Kuźnia Awaken: 3/3 (wymaga ofiar)
        * ofiary: 4/4
    * stakes:
        * śmierć cywili
        * manifestacja Kuźni
    * opponent
        * Kuźnia
    * problem
        * Kuźnia jest niewidoczna i nie ma kontaktu z ludźmi dotkniętymi symbolem Ognia oraz Soli
            * (póki mają symbol na sobie, komunikacja jest niemożliwa bo Kuźnia zmienia ich mowę)

Co się muszą dowiedzieć:

* Farma Świń nie istnieje, jest zniszczona - w jej miejsce budowane jest uzdrowisko i są ciężkie maszyny
    * Dowodzi wszystkim ród Wyszczyków; oni w końcu doprowadzili do usunięcia drażniącej ich farmy świń
        * Patryk Wyszczyk, młody bogacz, który przybył z miasta z żoną, Alicją Kaldra
* Produkcja maszyn została znowu przestawiona na broń; jest to pod kontrolą Ognia
    * dowodzi rodzina Felirus
* Rolnictwo i magitrownia jest pod kontrolą Soli
    * dowodzi rodzina Igora; Mochowniacy.
    * magitrownia jest dość niestabilinym obiektem w chwili obecnej; podejrzewają Ogień (jest przeciążona)
* Nikt nie wie skąd przychodzą Świniotaury ani dokąd idą
    * wszyscy podejrzewają Maćka Wieprzyna, syna; on jest niewrażliwy.

Profil:

* Igor: miał być pierworodnym i przejąć ziemię i dom, ale wyjechał do wojska. Świetnie walczy i to "swój chłop".
    * F: wyjechał do wojska zamiast objąć ziemię bo... 
    * M: jest w stanie się dogadać z wszystkimi bo... 
    * najcenniejsza rzecz jaką ma... 
* Magda: obca na tym terenie, czarodziejka Ixion + Praecis; konstruktorka adaptacji i dominacji. Badaczka i ekspertka od maszyn.
    * F: co kluczowego kiedyś zrobiła co będzie przydatne... 
    * I: trzyma się z resztą zespołu bo... 
    * jest znana szczególnie z uwagi na... 
* Fox: Custoris-class, kuzynka, "nie żyje". Świetna jednostka przełamania. Oficer, umie gadać z ludźmi. Niewrażliwa na wpływy.
    * I: zginęła bohatersko bo... 
    * M: ma nadzieję i chce tu zobaczyć... 
    * najlepszy przyjaciel to... 

## Sesja - analiza
### Mapa Frakcji



### Potwory i blokady

* Świniotaur
    * silny, szybki, świniotwór, pancerny
    * zachowania: dyskretnie ALBO frontalnie, porywa ludzi

F1:

* Fox musi mieć baterię, ładowanie z elektrowni
* Ogień porwał kilka osób z Soli w odwecie za Renatę
* Przesłuchiwana jest młoda Renata (o czym nie mówią)
* Ogień zrobi ambitny rajd; kilka osób zginie

### Fiszki

* Patryk Wyszczyk: dumny, patrzy z góry
* Maciek Wieprzyn: złamany, unika ludzi
* Adam Mochowniak: cyniczny, bezkompromisowy
* Bianka Felirus: pełna pasji i żądna sprawiedliwości
* Renata Felirus: komicznie pesymistyczna, Abdul
* X: małomówny, złośliwy ALE Prowl
* X: cichy, enigmatyczny ALE Ravage
* X: twardy, żartowniś, Warpath
* X: błyskotliwy, secluded, Mirage
* Mirela: w centrum uwagi, Springer


Andrzej, Mateusz, Paulina, Karolina, Szymon, Kamil, Dominika, Sylwia, Szymon, Wojciech


### Scena Zero - impl

Albert biegnie do tyłu, do magazynu, po broń. 

Lidia rzuca zaklęcie - zespoliła drzwi pociągu i odcięła.

Tp M +2:

* V: odcięcie wagonu
* V: uprzezroczyszczenie; widzisz świniotaury demolujące i porywające ludzi
* -> Tr +2Ob, V: użycie magii by zmienić mechanizmy, części pociągu do unieruchomienia i ratować ludzi. Więzimy i chronimy ludzi z przodu.
    * udało się pojmać 1-2 świniotaurów, ALE oni porwą kilka osób.
* Vr: udało się nie tylko pojmać świniotaury ALE do tego udało się zatrzymać osoby porwane. Nie zostały porwane.
    * 5 świniotaurów do Świńkuźni ucieka

Kasandra widzi jak drzwi są wysadzone. Wpada trójka ludzi, chcą łapać żywność. Kasandra się aktywuje, błyska diodami i wieżyczki. Cel - odstraszyć.

Tp+3:

* V: koleś się przewrócił, po czym - w DŁUGĄ. Nic nie chcą. Mają emblematy o kształcie świecy.

Albert widzi dwóch kolesi z emblematami świecy / płomieni, szabrują żywność, mają nieprzytomnego konduktora i przygotowują się do zeskoczenia na dół, z pociągu. Albert się podkrada by ogłuszyć. 

Tp +3:

* X: Nie udało się podkraść; zauważyli Cię. Zaczęli zrzucać żywność i łapać za broń
* V: Albert jest PRZEKONYWUJĄCY z shotgunem. Oni spojrzeli na siebie, na tory, na Alberta, na tory. Pozwolił im odejść i ich zapamiętał.

Oni faktycznie chcieli ukraść konserwy i jedzenie.

Badamy świniotaura - Lidia w akcji.

Tr Z (dwa egzemplarze + sprzęt) +3:

* X: Badanie było... problematyczne. Świniotaury nie przetrwały badania.
* Vr: Mamy serię podstawowych faktów
    * Świniotaur nie wykazuje wyższych cech psychicznych. To jest bardziej białkowy robot niż żywa istota.
    * Świniotaur jest kopiuj-wklej. Bardziej manifestacja niż żywa istota.
* V: To było uproszczenie
    * Świniotaury są "pochodną". Materia organiczna. One ogólnie są zrobione z ciał, i to z ciał świni. Ale według macierzy.
    * Da się znaleźć DNA człowieka w środku. Czyli jest jakaś forma użycia człowieka do budowy świniotaurów.
    * Wieprzne jednostki bojowe

Kasandra + Albert słuchają argumentów. Albert wysyła dronę by zobaczyć świnie itp - nie widzi farmy świń, tam są maszyny ciężkie które coś budowały, ale stoją. Wojna domowa w miasteczku (okopy, zasieki, deski w oknach). 

Drona pokazuje ogólnie rozumiany SMUTEK. I opuszczoną magitrownię w średnim stanie. Ona działa - chodzi na automatach, ale żadna frakcja do niej się nie zbliża.

Albert przekonuje ludzi do tego, że jest po ich stronie "jedziemy dalej" i gdy jest konsensus to presja i jedziemy dalej. Bo tam jest potencjalnie bezpieczniej (nie być tu):

Tp +3:

* Vr: jest grupa ludzi która chce jechać i ten głos stał się przeważający
* V: po przekonaniu ludzi - jedziemy, szybko i sprawnie. Pociąg się trzyma mimo odrażającego wyglądu.

Na stacji mnóstwo ludzi patrzy.

### Sesja Właściwa - impl

* Albert: miał być pierworodnym i przejąć ziemię i dom, ale wyjechał do wojska. Świetnie walczy i to "swój chłop".
    * F: wyjechał do wojska zamiast objąć ziemię bo to jest zadupie a on chciał czegoś więcej niż ziemie ze świniami.
    * M: jest w stanie się dogadać z wszystkimi bo jest stąd ale ma autorytet przez swoje umiejętności i obycie.
* Lidia: obca na tym terenie, czarodziejka Ixion + Praecis; konstruktorka adaptacji i dominacji. Badaczka i ekspertka od maszyn.
    * F: co kluczowego kiedyś zrobiła co będzie przydatne, bo uleczyła chorobę świń; zna się na świniach i na specyfice tego typu rzeczy.
    * I: jest znana szczególnie z uwagi na to, że nawet świniami zajmowała się jak normalnymi pacjentami
* Kasandra: Custoris-class, kuzynka, "nie żyje". Świetna jednostka przełamania. Oficer, umie gadać z ludźmi. Niewrażliwa na wpływy.
    * I: zginęła bohatersko bo mieli wydostać zakładników i wszystko szło ok ale na końcu była pułapka. I dzięki osłonie Kasandry udało się wycofać.
    * M: najlepszy przyjaciel Kasandry to ktoś kompetentny i wymieniali się doświadczeniami, ale zdystansowany do ludzi. Kasandra dobrze sobie radziła i doń dotarła.
    
Zbliżacie się do Świńkuźni. Idą na dworzec - ludzi tam już nie ma. Rozeszli się. W stronę Alberta (który jest na dworcu) idzie drobna dziewczyna. Na oko - 2x. Mirela. Mirela wyjaśniła świeczki Albertowi i dała mu jedną. Teraz jest połączony z kultem ognia, przynajmniej częściowo.

Lidia i Kasandra idą w kierunku magitrowni. Dziewczyna i Custoris.

Tp +2 +3Ob:

* X: Lidia otwiera się na energię magiczną i czujesz jak ta energia "wchodzi w głąb". Ona jest brudna. Ona jest... nieobsłużona. Lidia jest widoczna w polu magicznym.
* V: Energia to splot dwóch - Exemplis + Unumens.
* -> Tr+2+3Ob, Lidia próbuje wyczuć
    * V: Magitrownia jest podziemna, ona czerpie z czegoś "głęboko". Tam jest źródło energii, bardzo stare. Przeciążyło magitrownię.
    
W tym czasie Kasandra próbuje dojść do tego z czego ta magitrownia powinna czerpać

Tr Z +2:

* V: Informacje na ten temat są zastrzeżone. Wysoce zastrzeżone. Ale - jest powiązanie z obecnością świń. "Magitrownia i świnie są ważne, są potrzebne razem."
  * Albert autoryzował, Kasandra swoimi danymi, wiedzą, informacjami itp -> by odblokować i ostrzegła. +1Vg
* V: Kasandra dostała ostrzeżenie o kognitohazardzie. Informacja: sekta. Wojna. Po pokazaniu sytuacji odpowiedź: POTENCJALNA MANIFESTACJA W TOKU.
  
Kasandra opracowała mniej więcej co się tu dzieje. I trzeba to teraz rozwiązać...

## Streszczenie



## Progresja

* .

## Zasługi

* .


## Frakcje

* .

## Fakty Lokalizacji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski

## Czas

* Opóźnienie: ?
* Dni: ?

## Inne
