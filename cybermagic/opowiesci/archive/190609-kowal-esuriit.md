## Metadane

* title: "Kowal Esuriit"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [190527 - Mimik śni o Esuriit](190527-mimik-sni-o-esuriit)

### Chronologiczna

* [190527 - Mimik śni o Esuriit](190527-mimik-sni-o-esuriit)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. Czy Pięknotka uratuje Mirelę przed Marcelem?
2. Czy Pięknotka uratuje innych przed Electrumem?

### Wizja

* .

### Tory

* .

Sceny:

* Scena 1: .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena 1: Sny Mireli**

Pięknotka, trzy dni później, ZNOWU jest na Trzęsawisku. Wezwała ją, dla odmiany, Adela. Powiedziała, że terminusi powiedzieli, że te sny które ma Mirela są potencjalnie niebezpieczne. A dokładniej, Krystian tak powiedział. Adela zaproponowała neuronautę by spojrzał w głowę Mireli. Pięknotka facepalmowała.

Mirela. Niezbyt Spokojny Mimik. Powiedziała, że "ta dziewczyna" się załamie. Powiedziała, że "ta dziewczyna" ma smak stalowej pieśni i jodu. Pięknotka uznała, że Mirela jest najmniej przydatną jednostką śledczą w okolicy. Udało się zawięzić miejsce - tancbuda w Czółenku; tam jest katalizator. Zapytana, czy chce iść z Pięknotką - Mirela powiedziała, że chce iść ratować innych. Idą więc...

**Scena 2: Mimik w Czółenku**

Pięknotka wykryła kątem oka cień. Coś, co wygląda jej na dronę terminuską monitorującą. A Mirela zachowuje się nie do końca naturalnie; drona nie powinna jej wykryć jako viciniusa, ale może zwrócić uwagę na anormalnie zachowującego się mimika. Pięknotka błyskawicznie zadziałała - skupiła się na supermarkecie i walnęła w to swoją biokreacją; niech pojawią się uniesienia miłosna. Ma to odwrócić uwagę drony od mimika i kupić czas. (TpZ: S). Pięknotka dała radę przekierować uwagę drony. Ale skąd tu drona terminuska?

Mirela wyjaśniła Pięknotce, co wykrywa. Jest dziewczyna wpadająca w skrajną rozpacz i głód. Są też inni głodni ludzie, ale mało. I ta dziewczyna jest powiązana z jednym z bunkrów - i tam Mirela czuje potężne, przyciągające uczucie. Obezwładniające. Potrafi wskazać, który to bunkier. Pięknotka nie chce ciągać za sobą Mireli, ale nie chce też by ta została wykryta. Pięknotka wsadza do silosa Mirelę i próbuje sprawić, by ta nie wpadła w kłopoty - przekonuje ją; cokolwiek się nie stanie, NIE MOŻE SIĘ STĄD RUSZYĆ itp. (Tp:S). Mirela poczeka i dała Pięknotce wszystkie wytyczne.

Pięknotka poszła do desygnowanego bunkra, skutecznie omijając terminuską dronę. Pięknotka zauważyła, że kilka dron monitoruje ten teren i bunkry, ale kontroler wyraźnie nie wie, któego bunkra szuka. Pięknotka dotarła do właściwego bunkra; nie czuje w ogóle Esuriit. Ale zdaniem Mireli "to" tu jest. Nic, Pięknotka nic z tym nie jest w stanie zrobić - na szczęście ma przygotowane odpowiednie kwiaty od Wiktora Sataraila. (TrZ:S). Kwiaty uległy Skażeniu Esuriit. A więc tu jest albo była ta energia - wszystko wskazuje na to, że energia Esuriit jest tu bardzo duża, ale "niedostępna", "sealowana". Dlatego nie da się jej wykryć - bo jej nie ma. Ona się pojawia i zanika. A Mirela wyraźnie czuje jej obecność. Gorzej - ta energia nie jest stacjonarna. Niekoniecznie to będzie ten sam bunkier następnym razem.

Czas na terminusa. Niech się zajmie Esuriit, nie Mirelą. Pięknotka wysłała sygnał do drony. Dostała po hipernecie odpowiedź od terminusa. To musiał być, oczywiście, Marcel Sowiński.

Marcel powiedział, że tam jest efemeryda Esuriit; niestabilna i pojawia się, gdy ktoś ją wezwie. On chce dowiedzieć się co to jest i ją docelowo usunąć. Nie da się kontrolować czegoś, czego nie rozumieją. Marcel zauważył, że Pustogor nie wysłał wsparcia - nie wierzą w tą efemerydę, więc jego celem jest wezwać tą efemerydę używając ludzi. Nie powinno być problemu i nikt nie powinien zginąć. Zdaniem Pięknotki, sporo "powinien".

Pięknotka zaczęła z nim negocjować. Niech się zgodzi na to, żeby zrobić to inaczej, nie dręczyć ludzi. Marcel powiedział, że nie ma innej opcji - ta efemeryda jest trochę jak dżin. Potrzebuje woli i aktywatora plus ogromnego cierpienia. Pięknotka wpadła na pomysł - grupa osób w opiece paliatywnej. Spróbowała Marcela przekonać do tego wariantu (Tr:P). Marcel się zgodził - ale Pięknotka ma tydzień by sprowadzić tego typu osoby. Niech ona zapewni, by to zadziałało. Tymczasem on zostanie tutaj i spróbuje jakoś ponaprawiać to, co się stało; w tym momencie nie potrzeba jakichś ekstra energii i cierpień ludzi.

To jest cena, której Pięknotka nie zapłaci. Nie zgadza się. Nie pozwoli na to, by wywołać ten typ rozpaczy u kogokolwiek. Marcel jedynie wzruszył ramionami - to jest powód, czemu od dowodzi tą operacją a nie Pięknotka.







Pięknotka wychodzi. Trzeba znaleźć "głodną dziewczynę". Ale do tego - znowu - potrzebna jej Mirela.



**Sprawdzenie Torów** ():

* Marcel na tropie Mireli: 3: 1
* Zdrowie Mireli: 3: 
* Sprowadzenie Electrum: 4: 2
* Cierpienie ludzi: 4: 2

**Epilog**:

* 

## Streszczenie

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: 
* Mirela Satarail: 
* Marcel Sowiński: 

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Astoria
            1. Szczeliniec
                1. Powiat Pustogorski
                    1. Czółenko: 
                        1. Tancbuda: 
                        1. Bunkry: 
                        1. Opuszczony Silos: 
                    1. Trzęsawisko Zjawosztup

## Czas

* Opóźnienie: 3
* Dni: 
