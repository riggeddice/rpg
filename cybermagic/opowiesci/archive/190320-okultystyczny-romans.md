## Metadane

* title: "Okultystyczny Romans"
* threads: nemesis-pieknotki
* gm: żółw
* players: pielgrzym, roni, morti

## Kontynuacja
### Kampanijna

* [190320 - Okultystyczny Romans](190320-okultystyczny-romans)

### Chronologiczna

* [190320 - Okultystyczny Romans](190320-okultystyczny-romans)

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

1. Rafał pod wpływem furii prawie zabił Szczepana (silny chłopak); Jan i Ewelina go powstrzymali. Dowiedzieli się od wstrząśniętego Rafała, że żywi się złością i złą energią.
2. Kacper dostał się do tajemniczego grimoire którego użyła Marzena. Okazało się, że to drona bojowa imieniem Tantaliza. Mają wspólne interesy.
3. Kacper porozmawiał z doktorem Tapirem (lokalnym magiem) - Tantaliza ujawniła jego kłamstwa. Ale Tapir wie, że Kacper wie o magii i chce go wyczyścić.
4. Marzena przyzwała Tantalizę i Kacper bez osłony stracił pamięć. Tantaliza potem mu ją odda.
5. Wydobyli posążek Arazille spod wody. Tam - decyzja, co robić dalej. Rafał umrze, Marzena też.
6. Oddali teren Arazille w zamian za rozwiązanie problemu Tapira oraz naprawienie przyjaciół.
7. Teren wpadł pod kontrolę bogini.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
|               |         |             |

Czyli:

* (K): .

## Streszczenie

Rafał zginął na motorze; Marzena go wskrzesiła w kółku okultystycznym, nieświadoma tego, że użyła mocy Tantalizy (uszkodzonej drony bojowej polującej na Arazille). Zespół składający się z trójki ludzi doprowadził do tego by pomóc Rafałowi i Marzenie wrócić do normalności, ale oddali cały teren Arazille i jeden z nich zintegrował się z Tantalizą (droną bojową). Są jednymi z nielicznych zdolnych do poruszania się na terenach Arazille.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Ewelina Renia: mól książkowy i nocny marek; przyjaciółka Marzeny i okultystka. Zdolna do działania na terenie Arazille.
* Jan Wędrzec: student cybernetyki z robotem; wydobył Arazille z jeziora. Świetny ścigacz na motorze. Człowiek zdolny do działania na terenie Arazille.
* Kacper Marczok: kiedyś człowiek, po wpływie Arazille stał się jednością z Tantalizą. Pragnie większej mocy. Zdolny do działania na terenie Arazille.
* Włodzimierz Tapir: mag z Protektoratu; odpowiedzialny za porządek na tym terenie (i pilnowanie by nie pojawiła się Arazille / wykrycie Tantalizy). Skończył we Śnie Arazille.
* Tantaliza: sonda bojowa z Orbitera zaprojektowana do walki przeciw Arazille. Uszkodzona, przeszła w tryb książki. Była kusicielką i grimoire. Skończyła jako powerup dla Kacpra. KIA.
* Arazille: uśpiona przez posążek, na dnie jeziora. Przebudzona przez Zespół. Zgodnie z umową, oddała Rafałowi i Marzenie życia - za co objęła władzą Czarnopiór.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
            1. Protektorat Życia
                1. Kalimien
                    1. Czarnopiór: kiedyś spokojna mieścina po wojnie z Arazille; teraz niestety wpadł w ręce Arazille i stał się terenem zakazanym dla Protektoratu.

## Czas

* Data: 20350525
* Dni: 2
