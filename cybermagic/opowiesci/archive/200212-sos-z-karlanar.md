## Metadane

* title: "SOS z Karlanar"
* threads: legenda-arianny
* gm: żółw
* players: kić, fox, samael

## Kontynuacja
### Kampanijna

* [200610 - Ixiacka wersja Malictrix](200610-ixiacka-wersja-malictrix)

### Chronologiczna

* [200610 - Ixiacka wersja Malictrix](200610-ixiacka-wersja-malictrix)

## Uczucie sesji

* .

## Punkt zerowy

* Astorianie prowadzą badania i próbują zrozumieć dziwne działania tej anomalicznej asteroidy. Krzywdzą eks-noktian.
* Eks-noktianie sprowadzają Zarazę (plagę szaleństwa) na Karlanar.
* Hestia wzywa SOS i pacyfikuje wszystkich na stacji wedle możliwości.

### Postacie

.

## Misja właściwa

### Scena zero

.

### Sesja właściwa

SOS z Karlanar wysłane przez TAI Hestia na stacji Karlanar. Informacja o potencjalnej Irytce Sprzężonej, Hestia próbuje opanować magów, ale sama Hestia nie zdziała tak wiele ile trzeba. W odpowiedzi Arianna daje rozkaz - trzeba zdążyć zanim Hestia straci kontrolę. Hestia jest zbyt słabą TAI by sobie poradzić z taką sytuacją. Klaudia z Eustachym budują eksplozję a Klaudia ustala, jak muszą lecieć by dzięki tej eksplozji być bardzo, bardzo szybkim.

Dostali się szybciej niż ktokolwiek myślał że jest to możliwe. Szybkie badania Klaudii i Martyna pokazały, że załoga jest napromieniowana magią; energia magiczna jest przez to absolutnie poza kontrolą. Ale za to są odporni na Irytkę i inne choroby - to jest super nieoczekiwany efekt uboczny. Niestety, Infernia nie wytrzymała lotu - skanery zewnętrzne są ciężko uszkodzone. Ale zdążyli zanim Hestia straciła kontrolę.

Po nawiązaniu połączenia z Karlanar przekonali Hestię, że są Infernią; TAI nie mogła uwierzyć, że zdążyli tak szybko. Jak Hestia utrzymała kontrolę? Uśpiła WSZYSTKICH na statku przez odpowiednie zmiany w systemie podtrzymywania życia. Martyn natychmiast z tego skorzystał i ruszył do badania ofiar Irytki.

To nie Irytka.

Ta choroba jest budowana na szkielecie Irytki Sprzężonej, ale jest syntetycznie stworzona w laboratoriach. Martyn doszedł też do tego, że zaraźliwa jest też przez ludzi, co normalnie nie występuje przy zwykłej Irytce. Aha, ma echa noktiańskie - co jest najciekawsze i najbardziej niepokojące ze wszystkiego. Martyn błyskawicznie odizolował wszystkich zarażonych (i tych niezarażonych też), wsadził ich do hibernacji i podał im środki antymagiczne. Dzięki temu są niegroźni. No i, "przypadkowo", Infernia kontroluje stację Karlanar i może określić co tu się dzieje.

Klaudia ciężko westchnęła. Siadła do dokumentów i zaczęła czytać. Znowu zamiast być naukowcem - jest biurokratką. Wygrzebała z systemów kilka nowych informacji:

* osoby które były najpewniej wektorem zarażenia to są ludzie; pracowali przy jednym z sektorów asteroidy bazy Karlanar. Tam często coś się psuło.
* na stacji są Skażeńce, lub viciniusy. Coś. Zbierane są czasem grupy polujące na nie by je sprzedawać.
* wewnątrz asteroidy jest anomaliczny obszar; Hestia się przypatruje temu terenowi

No dobrze. Czas więc przejść się w kierunku na ten sektor Karlanar gdzie się psują sensory. Ekipa poszła w tamtym kierunku.

Tymczasem - tajemnicze viciniusy uderzyły w stację Karlanar od środka, przez ściany. Wdarły się do containment chambers, wykradły inne krystaliczne viciniusy, po czym jeszcze zniszczyli TAI Hestia - centralny komputer ze wszystkim. Zniszczenia byłyby katastrofalne, gdyby nie działania Persefony Inferni - Persefona sprzęgła się z działkami Inferni i otworzyła ogień, zabijając grupy krystaloformów i zmuszając ich do ucieczki... po czym Persefona sprzęgła się z systemami stacji które były zarządzane przez Hestię wcześniej. Ciężko idzie, ale działa. Persefona jest stacją Karlanar oraz Infernią.

Tymczasem zespół szuka śladów Irytki na zewnętrznej części asteroidy. Klaudia skupia energię magiczną i korzystając z tego że Martyn ma strain Irytki, wyszukała ją. Normalnie tak się nie da, to jest niebezpieczne - ale Klaudia jest uodporniona na plagi przez ich "lot po wybuchu". Doszła do tego, że źródło Irytki jest gdzieś w środku asteroidy. Dodatkowo w miarę blisko powierzchni. I jest tam coś jeszcze - a dokładniej, częściowo krystaliczny statek kosmiczny, dość uszkodzony.

I wtedy dotarła do nich informacja o ataku na centralę i o zniszczeniu Hestii. Eustachy z Persefony otworzył ogień - torpeda uderzyła w tajemniczy statek a fala uderzeniowa poszła w dół. Krystaloformy zostały poranione, sporo zginęło. To zmusiło tych na pokładzie stacji Karlanar do wycofania się do statku.

Zespół wrócił na stację i Klaudia z podwójną energią rzuciła się do danych Hestii a Martyn do badania krystaloformów. Czy to ma coś wspólnego z ludźmi? Ogólnie, nie wygląda to zbyt optymistycznie. I faktycznie, udało im się wygenerować niepokojącą serię faktów. Odtworzyli całość historii na bazie danych ze zniszczonej Hestii i krystaloformów.

* Kiedyś stacja Karlanar była noktiańską stacją. Kiedyś bardziej bojowa, przekształcona w badawczą.
* Weszli na stację astorianie i zaczęli eksterminację. Noktianie uciekli w formę krystaloformów na bazie magitechnologii Elizy Iry. W procesie wymarli wszyscy magowie.
* Krystaloformy sobie żyją poza stacją, kiepsko. Astorianie żyją na stacji.
* Pojawia się Paweł Głazorzec, aktualny dowódca stacji. Zesłany za niegodne zachowanie, ale dobrze pozycjonowany.
* Podczas jednej z wielu atrakcyjnych rozrywek znajdują krystaloforma. Paweł delikwenta bierze do badań - okazuje się, że to był człowiek. Paweł go sprzedaje.
* Zaczyna się biznes oparty na polowaniu na krystaloformy i ich sprzedaż. Nawet dzieci.
* Krystaloformy projektują broń w oparciu o Irytkę...

To tłumaczy atak na Hestię i odbicie "swoich". To noktianie, a raczej - co z nich zostało. Arianna się zapaliła - chce ich do załogi.

Zespół projektuje bardzo skomplikowaną dywersję. Odezwa do stacji (że niby dopiero przylecieli i wykryli że krystaloformy to żywi ludzie), pozorowane dupbum, spin doktor do kryształów - i udało się Ariannie nawiązać połączenie z jednym z krystaloformów. Odpowiedział - mają death cult. To jest death cult vs inni eks-noktianie. Death Cult chce zniszczyć asteroidę z rozkazu Iary - matki, której dzieci Paweł sprzedał. Arianna powiedziała, że jest dla nich wszystkich nadzieja. Krystaloformy mają wojnę domową.

Arianna włącza pełną moc magiczną. Full Cult Power. Moc Arianny vs bomba Iary. Wszystko po to by ewakuować jak najwięcej osób - i ludzi i krystaloformów - z asteroidy zanim bomba Iary zniszczy wszystko. Udało się, acz Arianna 2 tygodnie jest nieprzytomna przez przeciążenie magiczne. Sama asteroida została zniszczona wraz z bazą; Infernia z trudem odpełzła biorąc większość populacji stacji i mniejszość krystaloformów...

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Stacja Karlanar wysyła SOS, Infernia go odbiera. Na miejscu okazuje się, że panuje syntetyczna Irytka stworzona przez krystaloformicznych noktian w odwecie za sprzedaż ich dzieci. Zespół ewakuuje co się da zanim noktiański death cult niszczy asteroidę. Arianna akceptuje status półbogini.

## Progresja

* Arianna Verlen: ciężko napromieniowana arcymag, 2 tygodnie przesiedziała na kaszkach i środkach regenerujących.

### Frakcji

* .

## Zasługi

* Arianna Verlen: bohaterka, odpaliła pełną moc swojego kultu by tylko uratować załogę stacji Karlanar. Zaakceptowała status półbogini.
* Klaudia Stryk: szalona; falą eksplozji przyspieszyła Infernię. Dodatkowo, znalazła tajemniczy noktiański statek i poznała prawdę z systemów Hestii.
* Martyn Hiwasser: badacz krystaloformów oraz ekspert od neutralizacji magów udając, że są pod wpływem Irytki. Nie chciał nikogo skrzywdzić i mu się udało.
* Eustachy Korkoran: kilka dywersyjnych eksplozji, wsparcie dla Klaudii i torpeda unieszkodliwiająca noktiański statek. 100% zadowolenia.
* Paweł Głazorzec: fatalny i arogancki dowódca stacji Karlanar; sprzedawał krystaloformy (eks-noktian) dla zarobku. Nic mu się nie stało mimo wszystkiego co zrobił.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Kosmos: znajdowała się tam stacja badawcza Karlanar, która została ostatecznie zniszczona przez starcie bomby Iary oraz magii Arianny

## Czas

* Opóźnienie: 100
* Dni: 1

## Inne

### Co warto zapamiętać

.
