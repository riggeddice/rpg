## Metadane

* title: "XXX"
* threads: rekiny-a-akademia
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [211127 - Waśń o ryby w Majkłapcu](211127-waśń-o-ryby-w-majklapcu)

### Chronologiczna

* [211127 - Waśń o ryby w Majkłapcu](211127-waśń-o-ryby-w-majklapcu)

### Plan sesji

#### Theme & Vision

* "Rekiny chcą mieć najsilniejszego wojownika za przełożonego, Justynian chce iść w odbudowę."
* "Rekiny chcą epicką walkę i Arkadia staje po stronie Justyniana."

#### Ważne postacie

* ?

#### Co się wydarzyło

* .

#### Sukces graczy

* .

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Marysia dostała wiadomości od Torszeckiego - jak zawsze. Standardowy raport.

* Rekiny odbudowują Arenę Gladiatorów jako najważniejsze miejsce.
* Marysia w końcu obiecała im walkę godnych przeciwników.
* Torszecki zadał pokorne pytanie "czy masz pomysł kto będzie tam walczył"
    * --> oczywiste, Rekiny. CO MOŻE PÓJŚĆ NIE TAK?

Dzień później. Torszecki wysłał Marysi pokorny raport

* Wśród Rekinów krąży plotka i opowieść. Rekiny się zapalają.
* Zgodnie z tradycją Areny Amelii, ten Rekin dowodzi, kto pokona na Arenie konkurencję.
    * --> ofc Roland Sowiński.
* Marysia - Justynian?
* Torszecki zaznacza, że się MARTWI o Marysię.

Oczywiście, Marysia może poprosić KOGOŚ by walczył w jej imieniu - ale wtedy inni też mogą. A najlepszą wojowniczką jest Arkadia, która raczej nie będzie walczyć dla Marysi. Ale... gdyby Marysia zmieniła się w NIEDŹWIEDZIA..? Lub pumę?

1. CO CHCĄ REKINY? Czemu organizowana jest walka gladiatorów.
    * chcą mieć rozrywkę
2. Czemu Rekiny chcą by dowodził zwycięzca?
    * to jest honorowy tytuł. Ma znaczenie tylko prestiżowe
3. Kto zdaniem Rekinów ABSOLUTNIE nie może dowodzić?
    * terminus -> bo jest kimś z zewnątrz i z tych "starych"
    * zwykły człowiek -> taki dyshonor
    * eternianin -> BO ETERNIANIN
4. Jeśli Ernest przystąpi i wygra to..?
    * OH SHIT.

Marysia poszła odwiedzić Ernesta. To dobry moment :3. Do pokoju obok, naturalnie. Zdaniem Ernesta Azalia chodzi z miną burzy gradowej przez to że nie można wyburzyć Areny. Ernest dowiedział się, że Arena nosi nazwę Areny Amelii - więc jest powiązana z jego ukochaną, WIĘC ZOSTAJE. Azalia może być smutna ;p. Marysia TERAZ jest smutna.

Marysia zorientowała się, że ciągle "używając imienia Amelii nadaremno" sprawia, że JEJ się opłaca by Ernest wierzył w dobrą Amelię. Im bardziej mówi "Amelia to, tamto" tym mniej wiarygodne będize "a tak naprawdę to Amelia to zła laska była". Więc Czas Decyzji się zbliża...

Ernest się zainteresował propozycją Marysi - można zmienić nazwę Areny na Arenę Amelii i Ernesta. Ernest nagle się zmotywował do wygranej... zwłaszcza, że "rządziłby Rekinami" a ma w sobie coś z trolla. Więc zaproponowała, że Ernest może oddać władzę Azalii i Żorżowi. TERAZ Ernest się ucieszył. Na pewno idzie w stronę władzy. Idzie to wygrać.

TrZ (bo chce zdeptać Rekiny i pokazać im różnicę skali i mocy) +3:

* Vz: W uznaniu za wybitne pomysły Marysi Ernest zdecydował jej oddać władzę na tym terenie po tym, jak skończy naprawiać co powinien (Żorż, Azalia, zmiana nazwy Areny)
* V: Rekiny nie uznają, że Marysia dowodzi tylko z łaski Ernesta. Ok, oddał jej władzę, ale dlatego, bo go przekonała a nie bo dał swojej kochance z łaski.

Marysia zdecydowała się NIE ryzykować i nie naciskać mocniej. Ma w końcu Arenę Gladiatorów do rozwiązania.

Następny dzień.

Do Karoliny zebrała się delegacja Rekinów. Złapały ją w ciemnym zaułku. Karo lubi tam chodzić. Okazja na wpiernicz. Ale ich jest pięć. To dużo. Poprosili (dokładniej to Franek Bulterier) Karo, by Marysia przekonała Ernesta, by ten nie uczestniczył w walkach ("niech Marysia obieca mu inną pozycję czy coś"). Karo prosto - boicie się? Franek się żachnął, wtedy Mysiokornik - a czy Daniel uczestniczy w walkach? Karo potwierdziła. Mysiokornik spokojny. Daniel MOŻE pokonać eternianina. Tylko on.

Karo nie komentuje. Wierzy w brata... ale Ernest też jest spoko ;-).

Karo informuje Sensacjusza o planowanej napierniczance. Przydałby się lekarz w pobliżu. Sensacjusz westchnął. Powiedział, że jego glizda się wypasła. Zadziała. Sensacjusz obiecał, że zapewni więcej glizdowatych mechanizmów leczących. Mniej godne, trochę bardziej śmierdzą, ale zadziałają.









## Streszczenie



## Progresja

* .

### Frakcji

* .

## Zasługi

* Marysia Sowińska: 

* Izydor Grumczewicz

* Karolina Terienak: uznała, że chce pomóc Iwanowi i rozwiązać problem lokalnego oddziału mafii; zebrała drużynę (Torszecki i Arkadia), po wyłapaniu kotów z Danielem manewrowała ścigaczem by uratować od mafii Arkadię i ewakuować Daniela.
* Daniel Terienak: chce pomóc domowej sentisieci, więc zainteresował się pomocą morderców ryb w Majkłapcu (by się zbliżyć do czarodziejki z AMZ). Złożył czar z Pawłem Szprotką - artefakt szukający kotów. Doszedł do tego, że Iwan Zawtrak nie mówi całej prawdy; wszedł na twardo i jakkolwiek dostał cios, to prawda wyszła na jaw. Włamał się skutecznie do mafii i podłożył im narkotyki.
* Ksenia Kirallen: oddelegowywana do wszystkich małych waśni pomiędzy firmami, bo jest absolutnie przerażająca i jeśli to coś nieważnego, NIKT JEJ NIE CHCE. 3 mc temu rozwiązała problem z Paprykarzem Z Majkłapca. A teraz pojawił się inny i jej nie wezwano. ALE! Jak tylko dostała dowody, że działa tam mafia (Stella Amarkirin) to ruszyła do działania.
* Genowefa Krecik: 22 lata, następczyni potęgi Krecików z Majkłapca; przekonana że morderstwo ryb i gęsi to wina "noktian" z wegefarmy. Nie ma najlepszej opinii o Danielu, mimo, że on dał radę.
* Paweł Szprotka: zajmuje się leczeniem ryb i gęsi w Majkłapcu z ran kotów-pacyfikatorów (z ramienia farmy Krecik). Z samego nagrania zauważył, że koty były czymś naćpane - ktoś im coś podał. Karo poprowadziła go by z Danielem stworzył detektor kotów po sierści.
* Iwan Zawtrak: 44 lata; właściciel kociarni i miłośnik tych zwierzaków. Nie chce się opłacać mafii, ale gdy mafia wypuściła część jego kotów i je podtruła by były agresywne, zmienił trochę zdanie. Zaufał Karo i Danielowi, powiedział im o co chodzi. Na uboczu, uczciwy, ale trudny w obejściu.
* Rafał Torszecki: nie znalazł dla Karo info o mafii i tylko podpadł mafii, ale zaproponował transponder co Karo przekształciła w wabik na koty. Dostarczył kompromitujące twarde narkotyki Danielowi, by ten mógł podłożyć je mafii. Pomógł Karo w manipulowaniu mafii.
* Arkadia Verlen: gdy usłyszała, że może bić i pomóc komuś to dołączyła do Karo. Gdy Daniel przypadkiem odpalił cichy alarm, zaatakowała mafię FRONTALNIE - i Karo ją przechwyciła ścigaczem. Dobra dywersja. Potem przekazała Ksenii info o Stelli z mafii.
* Stella Amakirin: eks-noktiańska czarodziejka, eks-mafia Kajrata (57 lat); ma kilka osób i szuka małych biznesów by dawały jej niewielki haracz. Szuka emerytury. Niestety, napotkała na ambitne Rekiny i na jej małą komórkę Karolina i Arkadia napuściły KSENIĘ!

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Dzielnica Luksusu Rekinów: 
                                    1. Fortyfikacje Rolanda
                                    1. Serce Luksusu
                                        1. Lecznica Rannej Rybki
                                        1. Apartamentowce Elity
                                        1. Fontanna Królewska
                                        1. Kawiarenka Relaks
                                        1. Arena Amelii: 
                                    1. Obrzeża Biedy
                                        1. Hotel Milord
                                        1. Stadion Lotników
                                        1. Domy Ubóstwa
                                        1. Stajnia Rumaków: tak, nie znajdują się tam konie a ścigacze. Oczywiście.
                                    1. Sektor Brudu i Nudy
                                        1. Komputerownia
                                        1. Skrytki Czereśniaka
                                        1. Magitrownia Pogardy
                                        1. Konwerter Magielektryczny

## Czas

* Opóźnienie: ?
* Dni: ?

## Konflikty

* 1 - Paweł Szprotka próbuje dojść do tego co jest nie tak z tymi kotami na nagraniu
    * TrZ+2
    * V: Paweł pokazuje Karo niektóre ruchy tych kotów. "On się nie bawi. On chce zabić. Chce zranić. Ten kot jest opętany nienawiścią. Koty tak nie mają. Co im zrobili?"
* 2 - Daniel, lepszy w tropieniu niż Karo, westchnął i zabrał się do szukania kociego futra...
    * ExZM+2:
    * XzXVz: Daniel wpadł do stawu uszkadzając nawierzchnię i dostał opierdol za dewastację mienia. Ale znalazł cholerne kocie futro...
* 3 - Paweł i Daniel (który się uwolnił od Genowefy) składają czar razem. Daniel artefaktyzuje detektor w podrdzewiałej podkowie, Paweł dostarcza "jądra" detektora.
    * TrZM+3, 5O (5 nie 3, bo nie są kompatybilni ze sobą)
    * XVzVm: mają perfekcyjny detektor kotów i perfekcyjny wabik. Ale Genowefa uważa, że Daniel nic nie umie i po co tu jest? Disdain.
* 4 - Daniel próbuje ocenić, czy Iwan udaje czy naprawdę przejmuje się tą sytuacją. O co tu chodzi? Daniel by się dowiedzieć generuje presję.
    * Tr+2+3O (presja - sukces, ALE)
    * XV: Daniel przesadził z presją i został walnięty w ścianę. Ale widzi, że Iwan mówi prawdę acz niecałą. Ale zależy mu na innych lokalsach i nie chciał krzywdy ani ich ani kotów
* 5 - Karo "nie chcesz ze szczeniakami, poradzimy sobie inaczej. Więcej kombinowania, ale..." -> Iwan ma współpracować.
    * TrZ+3+3O (presja - sukces, ALE)
    * VzXzX: Iwan powiedział im wszystko i chce ich nauczyć sterowania kotami. Nope. Daniel ani Karo NIE UMIEJĄ W KOCIE KOMENDY.
* 6 - Karo zaproponowała Danielowi - łączą siły. Nagrać (złapać komendę) Iwana, by kot zostawił wabik. Nawet jak nie zadziała, może pomóc.
    * TpZM+3
    * VXVm: komenda działa, z dużą ilością decybeli (niestety), ale działa. To i wabik zadziała na futrzaki.
* 7 - Czyli Karo i Daniel mają: klatki, lokalizację, wabik, stopper kotów, ścigacze. Czas zapolować na futra.
    * TrZ+4+3Vg (reprezentują absolutną przewagę)
    * VVVV: udało im się wszystkie wyłapać, nawet te co miały małe szanse. Ściągnięty Szprotka co je wyratuje. A Iwan lubi te dwa Rekinki.
* 8 - Torszecki się wyraźnie zasępił. Karo chce Nazwiska, lokalizacje, działanie, co tu mafia robi. Torszecki zainwestował sporo środków i uruchomił sporo przysług. Mafia nie jest jego siłą.
    * Ex+3
    * XXX: Torszecki nie dał rady, spalił kilka przysług i mafia się nim interesuje.
* 9 - Karo i Daniel próbują przekonać Iwana do planu podłożenia mafii fałszywego okupu
    * TrZ+2
    * XzXV: Iwan potrzebuje zapewnienia że mafia się na nim nie zemści a Karo dołożyła kasy do okupu. Ale - przekonali Iwana
* 10 - Daniel tropi cholernym kotem drogę do ukrytej bazy mafii.
    * TrZ+2
    * XXV: Daniel dał radę zlokalizować kryjówkę... odpalając cichy alarm. Ale wie gdzie są i wie o alarmie.
* 11 - Arkadia stwierdziła prostą rzecz - nie ma możliwości zrobienia tego cicho i dyskretnie. Ona odciągnie uwagę mafii. Dywersja. 
    * ExZ (Karo + kamuflaż Arkadii + jej monumentalnie ryzykowny plan) +3
    * Vz: Arkadia zaatakowała. Dwa granaty. Ściągnęła uwagę. Okrzyk bojowy "Aurum! Sowińscy!" (Karo ma XD). Gdy 4 osoby wypadły z budynku, Arkadię przechwyciła Karo i odleciały. 
* 12 - Dwa pojazdy ścigają Karo. Ona próbuje kupić czas Danielowi.
    * TrZ (wszyscy znają okolicę, ale Karo ma po prostu lepszy ścigacz) +3 +3Oy:
    * VV: Sukces na obu liniach
* 13 - TYMCZASEM DANIEL próbuje się włamać, zdobyć fakty i podłożyć twarde narkotyki (od Torszeckiego)
    * TrZ (dywersja) +3
    * XVV(+3Vz)Vz: Daniel zauważony, ale podmienił i zwinął dowody. Zwiał bez szkody dzięki Karo i się wycofali bezpiecznie.
