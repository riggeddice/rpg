## Metadane

* title: "."
* threads: serce-planetoidy-kabanek
* motives: 
* gm: żółw
* players: ???

## Kontynuacja
### Kampanijna

.

* [241016 - Nie dotarła do celu](241016-nie-dotarla-do-celu)

### Chronologiczna

* [241127 - Meksykański impas Świnki](241127-meksykanski-impas-swinki)

## Plan sesji
### Projekt Wizji Sesji

**WAŻNE**: Kontekst sesji i okolicy oraz Agendy itp DOKŁADNIE opisane w [Thread 'serce-planetoidy-kabanek'](/threads/serce-planetoidy-kabanek). Tam też znajduje się rozkład populacji itp.

* INSPIRACJE
    * PIOSENKA WIODĄCA
        * [Digital Daggers- The Devil Within](https://www.youtube.com/watch?v=G049LeVG6BI)
            * "I made myself a home in the cobwebs and the lies. I'm making all your tricks, I will hurt you from inside"
            * "You'll never know I hurt you. You won't see me closing in"
            * "Look what you've made of me"
        * [Within Temptation - The Gatekeeper](https://www.youtube.com/watch?v=-CmlTii18yI)
            * "One by one they died | A massacre that took all night | They had no chance, it was no fight | You can't kill what has been killed before"
    * Inspiracje inne
        * Evraina, kiedyś noktiańska jednostka bojowa, potem jednostka 'cywilna', odrestaurowana, zniszczy każdego kto stał za zabiciem "jej ludzi"

* Opowieść o (Theme and Vision):
    * "__"
        * .
    * "__"
        * .
    * Lokalizacja: Libracja Lirańska. Najpewniej planetoida Oratel a potem okolice Anomalii Kolapsu.
* Motive-split
    * the-revenant: TAI Evraina, w imieniu załogi "Obfitego Sadu". Zmuszona do zabicia młodych ludzi, zdecydowała się ich pomścić w okrutny sposób, robiąc "pętlę przeszłości" jako "tymczasowo opętany statek".
    * the-truthseeker: Ypraznir; próbują dojść do tego z kim współpracuje Tara i Anton. Tara musi być w końcu winna, skoro od jej pojawienia się Anton zdradził.
    * zemsta-uzasadniona: TAI Evraina ze "Szczęśliwej Świnki" zabija tych, którzy zabili "jej ludzi". Inżynier apokalipsy i intryg.
    * mroczna-konspiracja: TAI Evraina - sama - replikuje przeszłość. Tak, jak jej załoga zginęła, tak zginęli ludzie z Ypraznir. Tak, jak Antoni zatruł załogę Evrainy, tak Evraina doprowadziła do "zatrucia" tu.
    * wrobieni-lecz-niewinni: Anton i Tara Mewerk. Co prawda Anton doprowadził do zniszczenia "Obfitego Sadu", ale Tara jest niewinna.
    * potwor-scooby-doo: Evraina udaje, że Sandra jest duchem i opętała TAI Świnki. W rzeczywistości za wszystkim stoi Evraina.
    * pakt-z-diablem: Możliwość pozyskania sojuszu z Ypraznir przez Zespół, jeśli zrobią to dobrze.
* O co grają Gracze?
    * Sukces:
        * Niech zdecydują, co z tym zrobić. Niech zrzucą to na... ducha?
        * Niech pokażą Evrainie, że dalsza walka nie ma sensu. Nie zabije wszystkich.
        * Niech uratują Tarę, niewinną, byłą załogantkę.
        * Poznać metody działania Ypraznir.
    * Porażka: 
        * Zniewoleni przez Ypraznir.
        * Stracą Tarę i świetnych dostawców.
        * Potencjalnie przeniesienie na "Świnkę".
* O co gra MG?
    * Highlevel
        * Zabić WSZYSTKICH, którzy wkroczyli na pokład "Obfitego Sadu" i zmienili nazwę na "Szczęśliwą Świnkę".
    * Co uzyskać fabularnie
        * Pokazać Evrainę i jak Evraina zemściła się na oprawcach swoich ludzi. Jak niesamowicie groźna jest ta TAI.
        * Pokazać, czemu Evraina przestała walczyć przeciwko Ypraznir.
* Agendy
    * the-revenant: Evraina; zniszczyć wszystkich, którzy kiedykolwiek weszli na pokład "Świnki" (kiedyś: "Obfity Sad").
    * the-truthseeker: Ypraznir; poznać prawdę o tym jaka organizacja powtarza ich metody. Kto próbuje wejść na ten teren. Z kim współpracuje Tara?

### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

KONTEKST:

* ?

CHRONOLOGIA:

* ?

PRZECIWNIK:

* ?

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)


### Co obiecałem

#### 1. Opis sesji:

Wolna Planetoida Oratel. Kilkanaście tysięcy ludzi, 4 jednostki astronomiczne od gwiazdy, koło potężnego, Anomalnego cmentarzyska statków, z których część nigdy-nie-istniała.

Wy jesteście załogantami **Ricognitus**, niewielkiej jednostki (30 osób załogi) specjalizującej się w eksploracji niebezpiecznych anomalii, pozyskiwania z nich cennych rzeczy i sprzedaży ich na Oratel (lub w innych miejscach). Nie nazwalibyście się "nieskazitelnymi"; macie swoje na sumieniu i jesteście barwną gromadką, ale poruszacie się po tej mniej ciemnej stronie prawa. Np. nie macie do czynienia z niewolnictwem, anomalizacją ludzi i innymi paskudnymi tematami. Nie robicie też paskudnych rzeczy w okolicy Oratel; nie w chwili, gdy Oratel jest czymś najbliższym Waszego domu. Owszem, ścierają się tu różne frakcje - ale wszyscy trzymają się balansu sił i nikt nie chce szkodzić Oratel ani jej potencjałowi.

Możecie tu dobrze żyć - jeśli macie czym zapłacić, nikomu poważnemu nie podpadniecie i dobrze zarządzacie ryzykiem.

Na Oratel jest Wasza była załogantka - Tara. I Tara wysłała do Was sygnał SOS. Ona i jej mąż zawsze Wam pomagali i zawsze dawali Wam dobre stawki i zasoby lepszej jakości; byli ostrożni i nie walczyli z żadną frakcją - a jednak stało się im coś złego. Jednocześnie są też dość szemrani ludzie, którzy chcą przekonać załogę Ricognitus do znalezienia dość specyficznej i szkodliwej Anomalii... za niemałą cenę. 

A Ricognitus ma mało zasobów po poprzednich nieudanych akcjach i trochę pieniędzy i sprzętu by się przydało...

#### 2. Parametry techniczne sesji:

* **Czas sesji**: 210 minut, z możliwością przedłużenia do 240 minut jeśli zbliżamy się do końca.
* **Ilość graczy**: 2-5.
* **Sesja jest dość otwarta, bo to miejsce jest żywe**.
    * Wy decydujecie który z wątków otworzycie i którym się zajmiecie. **Ja zakładam, że spróbujecie pomóc Tarze** (bo się Wam opłaca i bo Tara jest przyjaciółką), ale nie będę naciskał.
    * Gwarantuję **co najmniej** 20 nazwanych NPC o różnych osobowościach. Obecność nazwanego NPCa nie oznacza, że jest ważny. Jest sporo wątków.
* **Stawka**
    * Jeśli nic nie zrobicie, Tara i jej mąż umrą w męczarniach. Będą torturowani. Jako, że tylko Wy chcecie im pomóc, ich los jest w Waszych rękach.
        * Istnieje ścieżka, w której możecie ich uratować, choć to nie jest najprostsza ścieżka.
    * Jeśli będziecie zachowywać się niewłaściwie czy podpadniecie niewłaściwym osobom, możecie musieć opuścić Oratel.
        * Możemy zamknąć sesję wcześniej, bo np. zabijam Wasze postacie jeśli zachowujecie się w sposób nieakceptowalny w Oratel.
* **Konwencja**
    * **Świat**: Eteryczne Zaćmienie, kosmos + magia. Jesteście na planetoidzie.
    * **Typ sesji**: 
        * Śledztwo, ale nie daję Wam gwarancji na dojście do prawdy. Macie wiele wątków, co podniesiecie to Wasze. Rekomenduję pomoc Tarze, nie będę Was zmuszał.
        * Więcej śledzenia, infiltracji, rozmawiania i negocjacji niż walki. Nie spodziewam się dużo walki.
        * Jeśli się nie postaracie / podniesiecie inny wątek niż Tara, nie poznacie prawdy.
    * **Filmy podobne do sesji**
        * Archiwum X, Blade Runner (1983), Alien 1, Altered Carbon, książki Dead Space.
        * Z tym wyjątkiem, że tu raczej nikt nie ginie. Za mała populacja ludzka i ludzie są zbyt wartościowi.
    * **Letalność i Pozycjonowanie**: 
        * Wasze postacie nie zginą i szczególnie nie ucierpią jeśli nie zrobią nic głupiego.
        * Jesteście małymi rybkami w wielkim bajorze, ale macie dobrą reputację na Oratel i nie macie wrogów. Rywali - tak. Wrogów - nie. Raczej podchodzi się do Was pozytywnie.
        * Jeśli komuś **naprawdę** podpadniecie (zwłaszcza organizacjom przestępczym), nie dojdzie do walki. Zatruję Wam jedzenie i umrzecie w ten sposób, bez możliwości reakcji.
        * Ludzie są wartościowym zasobem. Czemu chcesz zabić, skoro możesz sprzedać w niewolę? Jeśli kogoś zabijecie / okaleczycie bez zgody odpowiednich sił, patrz punkt powyżej.
    * **Styl prowadzenia**
        * Gramy intencyjnie. Póki wszyscy nie rozumiemy co akcja ma zrobić w fikcji, akcja nie wejdzie do fikcji.
            * Nie da się "przypadkiem" zepsuć sobie sesji czy postaci; to musi być świadoma decyzja.
        * Będę Wam przerywał, jeśli nie rozumiem do czego dane działanie / opis ma prowadzić. 
            * Każde słowo Gracza / MG ma prowadzić do czegoś. Nie ma "pustych słów"; jeśli nie wiem po co coś jest powiedziane, będę pytał.
        * Będę Was ostrzegał, jeśli Wasze ruchy są niezgodne z zasadami Oratel (bo postacie wiedzą). 
            * Pozwolę Wam zrobić co chcecie, ale będę bezwzględnie egzekwował logikę świata i Wasze pozycjonowanie.
            * Jeśli np. wpakujecie się w niefortunną sytuację poprzednimi decyzjami, możecie skończyć w sytuacji, w której (X) zaczną przesuwać fabułę w stronę dla Was mało akceptowalną. 
                * Wniosek - nie wpakujcie się w niefortunną sytuację poprzednimi decyzjami. Będę próbował Was osaczyć.
        * Jeśli uważam deklarację za nierealistyczną w kontekście, będę pytał "pokaż mi ciąg logiczny który prowadzi do tego, że TO jest możliwe dla tych postaci w tej sytuacji"

#### 3. Wasze Postacie

* Myślcie jak o: załoga statku w **Alien 4** czy załoga statku **Firefly**. Barwna gromadka, kompetentna, nie zawsze idealna, nie jesteście głodni ale nie macie dobrobytu.
    * Każda postać "coś robi" na statku kosmicznym plus posiada przydatne umiejętności. 
    * Postacie winny być kompetentne.
    * Postacie winny być takie, byśmy my chcieli im kibicować.
* Osoby, które _przetrwałyby_ w takich warunkach społecznych i kulturowych jak Oratel czy Ricognitus.
    * Czyli ktoś kto losowo "daje w mordę" losowym napotkanym osobom odpada.
* Osoby, które mogą mieć konflikty z innymi osobami na statku, ale są w stanie przetrwać z nimi 3 miesiące zamknięte w niewielkiej przestrzeni.
* Każdej postaci zależy na Ricognitus i załodze Ricognitus.
* Każda postać ma znajomości, przyjaźnie lub rodzinę na Oratel. Macie powiązanie z osobami i frakcjami na Oratel.
* Nie oczekuję paladynów czy osób o wysokim altruizmie. Nie przeszkadzają mi takie postacie, ale nie mogą antagonizować innych frakcji.

#### 4. Kontrakt i zasady gry

Triggery:

* Triggery:
    * niewolnictwo, cierpienie nastolatków, tortury, organizacje przestępcze, szaleństwo, obsesja, kanibalizm, głód, duchy, magia, transformacja ludzi wbrew ich woli, fizyka i logika
* NIE BĘDZIE tych Triggerów na sesji:
    * cierpienie zwierząt, tortury nieletnich, nic-z-tego-powyżej aplikowanego do Postaci Graczy, dokładnych opisów powyższego; raczej suche fakty.

Inne zasady:

* Na sesji "podsłuchiwanie" (pasywne uczestnictwo nie będąc w roli Gracza) występuje wtedy i tylko wtedy jeśli:
    * mam entuzjastyczną akceptację wszystkich graczy. Nie "no ok" a "chcę by to się stało", bez jakiejkolwiek presji zewnętrznej.
    * osoby podsłuchujące nie mają kamer i są wyciszone.
* Jeśli dowolna osoba przy stole czuje się niekomfortowo **z jakiegokolwiek powodu**, ma prawo przerwać sesję. Pozostałe osoby przy stole winny przyjąć to ze zrozumieniem i wspólnie rozwiążemy sprawę. 
    * Coś ważnego dla jednej osoby nie może być uznane za błahe.
* Gracze "pasywni" (nie wykonujący proaktywnych akcji) mogą się liczyć, że w którymś momencie MG przekieruje na nich spotlight i możliwość przeprowadzenia akcji.
* Wspólnie budujemy opowieść; MG nie wie jak sesja się skończy.
* Szanujemy wszystkich Graczy i KAŻDY Gracz ma miejsce na zmianę rzeczywistości. Nie przeszkadzamy sobie.
* System intencyjny - nie pytam "co robisz" a "co chcesz osiągnąć". Puste akcje nie wchodzą do gry.
* Gracze pchają akcję, Mistrz Gry reaguje na działania Graczy. 
* Wyłączone PvP (poza intencyjnym), postacie Graczy (zwykle) nie giną, ogólnie mało "walki".
* Każdy konflikt, który nie był negocjowany przez Graczy jest uznany za **entuzjastycznie przyjęty** przez wszystkich Graczy.
    * Gracz, którego postać nie jest w konflikcie ma prawo podnieść, że dany (X) jest dlań nieakceptowalny.



## Sesja - analiza
### Fiszki

.

### Scena Zero - impl

.

### Sesja Właściwa - impl

.

## Streszczenie



## Progresja

* .

## Zasługi

.

## Frakcje

* .

## Fakty Lokalizacji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Libracja Lirańska

## Czas

* Opóźnienie: 3
* Dni: 1

## Inne

.
