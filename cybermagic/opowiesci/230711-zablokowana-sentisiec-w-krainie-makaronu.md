## Metadane

* title: "Zablokowana sentisieć w krainie makaronu"
* threads: brak
* motives: tien-na-wlosciach, magowie-ignoruja-obowiazki, gra-w-cykora, b-team, teen-drama
* gm: żółw
* players: kamilinux, anadia

## Kontynuacja
### Kampanijna

* [230704 - Maja chciała być dorosła](230704-maja-chciala-byc-dorosla)

### Chronologiczna

* [230704 - Maja chciała być dorosła](230704-maja-chciala-byc-dorosla)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ?
* Struktura: 
    * 
* CEL: 
    * MG
        * Rozwiązanie przeszłości + 
    * Drużynowy
        * 
    * Gracze
        * 

### Co się stało i co wiemy

* Petra wyśmiała Wiktora "co on może, jest tylko alchemikiem". Wiktor powiedział, że jego alchemia zmiażdżyłaby to miasteczko gdyby nie jej sentisieć.
* Petra powiedziała 'dawaj, coś się będzie działo'.
* Wiktor CHCE przeprosin i NIE CHCE być wyśmiany.
* Petra CHCE udowodnić swoje umiejętności i NIE CHCE złamać warunków gry. "W sumie, nigdy nie powiedzieliśmy że jak się mag przypałęta to mi nie może pomóc"

### Co się stanie (what will happen)

* F0: Echo przeszłości
    * Karolinus: (Amanda) przekonaj kogoś by jako Samszar frontował moją frakcję "Złotego Cienia", może Albert?
    * Elena: po amnestykach, niewiele pamięta, ale ogólnie się udało.
    * Do Karmazynowego Cienia przybywają magowie.
    * "Zapewnijcie, by Irkowi nie stało się nic głupiego"
* F1: Przybycie na miejsce
    * Ludzie wiedzą o tym że ich tienka lata po okolicy i im nie pomaga, ale jest Irek
    * Irek próbuje pomagać duchom, które są osłabione i Skażone
    * Pierwsze pomniejsze strainy makaronostworów
    * Petra szuka Wiktora
* F2: Rozpoznanie - pierwszy potwór
    * Petra nie chce użyć sentisieci, walczy o nią
    * Makaronotwory (woda -> makaron), potem pszenicotwory, potem komarotwory
    * Irek walczy z potworami swoimi nędznymi siłami
* F3: Wielka Płaszczka Strumieniowa (skażająca strumień)
    * Tu już nawet Wiktorowi się wszystko wymyka spod kontroli
    * Toxic Elixir oraz Płaszczka są za silne, zaczynają overmutować. ("Tornadron and creator")

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Petra Samszar: 21, najszybsza z dziennikarek, przyszła superstar dziennikarstwa
    * OCEAN: (E+A-): rywalizująca i zawsze chce mieć rację, uwielbia prędkość i widoki. Łatwo się złości. "Wszystkie oczy NA MNIE!"
    * VALS: (Face, Achievement): wiecznie znudzona, trenuje prędkość, drony i dziennikarstwo. Barbed quips. Dotrzymuje słowa. "To wszystko na co was stać?! Szybciej! MOCNIEJ!"
    * Core Wound - Lie: "niezauważana wśród rodzeństwa, nieważna, najmniej zdolna" - "I must rise like a superstar! I must be famous and awesome! Then they will see me!"
    * styl: superstar reporter śledczy, drama queen, "_Nie możesz ignorować prasy. Albo będziesz współpracował, albo sama się wszystkiego o Tobie dowiem._"
* Wiktor Blakenbauer: 22, ekspert od środków psychoaktywnych
    * OCEAN: (C+O-): cierpliwy z charakterem lodowca, monotematyczny i monomaniczny, "Trochę cierpliwości i po mojemu będzie."
    * VALS: (Humility, Universalism): nikt nie jest specjalny, wszyscy podlegają tym samym regułom, "Jak jest, tak było i będzie. Kolejna iteracja tego samego cyklu."
    * Core Wound - Lie: "monomaniczny Blakenbauer alchemik" - "wzmocnię alchemię i udowodnię wszystkim, że alchemią osiągniesz wszystko"
    * styl: beznamiętny alchemik, "_I make elixirs. That's what I do. When I do this elixir I will make another one._"
* Irek Kraczownik: bojący się Blakenbauerów egzorcysta chroniący ludzi
    * OCEAN: (N+C-): zrezygnowany, wiecznie zmęczony, szybko zmienia tematy, szuka następnego zagrożenia, "Muszę to zrobić. Dla nich!"
    * VALS: (Benevolence, Security): chronić ludzi nawet kosztem siebie, "Nie chodzi o mnie, chodzi o nich!"
    * Core Wound - Lie: "Magowie Aurum zniszczyli mnie i wszystko o co walczyłem" - "Ja już się nie liczę, mnie nic nie uratuje. Chodzi o nich."
    * styl: znerwicowany panikarz który robi co może by chronić ludzi i przekonać innych do tego samego. Unika magów, raczej chce siedzieć z innymi ludźmi.

### Scena Zero - impl

Elena wróciła po amnestykach. Nie pamięta co powiedziała Hiperpsychotronikom. Karolinus i Elena spotkali się w Karmazynowym Świcie, w hotelu Gwiezdna Rozkosz. Elena MOGŁABY użyć anty-amnestyków, ale nie wie jakie to były a "nie te" anty-amnestyki mają paskudne efekty uboczne. To niebezpieczne.

Wiadomość od Amandy: "tien Samszar, potrzebna mi osoba z Twojego rodu. Ktoś, kto ma więcej pomysłów niż pieniędzy, niekoniecznie jest szczególnie inteligentny i może objąć przynoszący korzyści biznes, frontując go swoją osobą. Powodzenia. Opłaci Ci się."

Karolinus od razu pomyślał o Wacławie. Wacław przecież spieprzył u Hiperpsychotroników i jak on jest potencjalnie po amnestykach i "bez pracy", to można mu wpakować dziewczyny dzięki mafii ^_^.

Karolinus -> Wacław:

* W: Karolinusie... co u Ciebie? /lekko oszołomiony, jest w jakimś eleganckim miejscu a niedaleko siedzi Itria z tablicą i kredą i robi równania typu "torturowanie magów == złe".
* Karolinus: Mam robotę dla Ciebie, złota robota, pełno dziewczyn, będziesz zadowolony
* W: Tak? Słucham! Karo... wywalili mnie! Nie pamiętam szczegółów... ale wywalili.
* Karolinus: TO przeszłość, szykuj się na przyszłość, zapewniona, lekka robota. Będziesz zadowolony /oczko

(+Karolinus dostarczył, +Itria będzie za, +Wacław nie pamięta że to Karolinus mu spieprzył, -Wacław nikomu nie ufa teraz)

Tp+3:

* X (podbicie): Wacław będzie nalegał na te dziewczyny
* Vr (podbicie): Wacław jest zainteresowany ale chce szczegółów
* X (manifest): Wacław oczekuje stałego dostępu do ciągle nowych ładnych dziewczyn.
* Vr: (manifest): Wacław pójdzie w to, bo nagroda > koszt, ale będzie zadawał pytania.
* Vr: (podbicie): Wacław wierzy że Karolinus jest jedną z nielicznych osób którym na nim zależy i która w niego wierzy
* V: (manifest): Wacław jest LOJALNY Karolinusowi, bo Karolinus wielokrotnie udowodnił, że dba o niego.
* V: (eskalacja): Wacław JESZCZE przez pewien czas spowalnia Itrię -> Amanda ma Itrię z którą musi 'dealować'.

Do Karmazynowego Cienia zjeżdżają się magowie. Nie wiadomo czemu. (Karolinus podejrzewa Amandę).

Elena dzięki znajomościom z Antoniną ma namiar na znajomego Blakenbauera który ją lubi. Ignatus Blakenbauer. To jest nerd, z dziwnymi zainteresowaniami, ale Elena pomogła mu znaleźć różne rzeczy w Gwiazdoczach (choć nie wiedziała czemu ich szuka).

* W: Znaleźć Ci co?
* Elena: Jakieś informacje na temat dziwnych eksperymentów psychotronicznych u Samszarów - czy ktoś coś znikał, statystyki od lekarza o amnezją, na ilu osobach testują... statystyki. Może coś wiąże ich ze sobą? Szukam wzorów działania. 
    * ((JEŚLI MU SIĘ UDA: czy jest też link z Eternią, bo to potencjalnie powiązane. I czy nie krzywdzą duchów.))
* W: Nie ma sprawy, Eleno. 

Ex Z +3:

* X: (podbicie) Ignatus został Zauważony przez Hiperpsychotroników
* V: (podbicie) Ignatus dostarczy wartościowe informacje Elenie. (jest link z Eternią)
* X: (manifest) Hiperpsychotronicy muszą COŚ zrobić z Ignatusm. I nie wiedzą jak. Ale muszą.
* X: (eskalacja) Hiperpsychotronicy znaleźli link między Ignatusm i Eleną.
* V: (manifest) Na końcu sesji Elena dostaje CENNE źródło wskazujące na dane z Eterni.

Kojarzycie Irka Kraczownika? Egzorcysta od 7 boleści. Który miał sąd polowy itp. Który jest u Samszarów i ma Robić Rzeczy. Dostaliście wytyczne. "Irek jest w Triticatusie. Warto, by niczego nie spieprzył." Elena i Karolinus wyraźnie się nudzą i nic nie robią. A Karolinus ma OPINIĘ że jak się nudzi to atakuje Verlenów. Bo podbija do Vioriki. Więc - trzeba go stąd odesłać. Plus, bo nikt nie chce, by Karolinus miał TRAUMĘ po Mai.

Karolinus chce się dowiedzieć jacy magowie dotarł do miasta. 3 magów. To magowie słynący ze "stabilizacji", "żeby było dobrze" itp. Okazuje się, że Złoty Cień jest "dobrze umoczony". Faktycznie Złoty Cień stabilny zależy na tym wielu magom.

### Sesja Właściwa - impl

Triticatus. Elena **od razu** zauważa, że duchy są PRZYGASZONE. Coś jest z nimi nie tak. Jest w okolicy tien. Ale jej nigdzie nie ma. Oraz sentisieć jest zablokowana przez lokalnego tiena.

"Lokalna" tienka (ta co przybyła rozwiązać problem) nazywa się **Petra Samszar**. I nie wiadomo gdzie jest. Lokalsi nie wiedzą. Ona wie.

Elena próbuje kontaktować się z duchami. Czemu przygaszone i co się dzieje. I gdzie jest Petra.

Tr Z M +3 +3Ob:

* X: Petra przechwyciła sygnał i się sama skontaktowała.
    * Petra: tien Samszar? Co tu robisz? Halo? Ja to naprawiam?
    * Elena: tien Samszar, no chyba że Ci idzie kiepsko skoro mnie tu wysłali.
    * Petra: idzie mi ŚWIETNIE, Eleno. Nie przeszkadzaj, odsuń się, nie dotykaj sentisieci. Najlepiej, nie wiem, kup sobie pamiątkę z makaronu.
    * Elena: dlaczego duchy są smutne?
    * Petra: naprawiam to właśnie?
    * Elena: nie ruszam sentisieci, ale nie przejdę obok duchów. Dowiem się od nich.
    * Petra: jak chcesz /prych.
    * Elena: tylko się nie opluj.
    * Petra: zasyczała (rozłączyła się)
* Vr: Duchy informują o sytuacji
    * jest negatywny wpływ z zewnątrz, którego nie leczy sentisieć. Bo jest zablokowana.
        * wygląda, jakby Petra próbowała zaszkodzić terenowi. Gdyby jej nie było, sentisieć wyleczyłaby ten wpływ sama.
        * część makaronu ożyła, w formie makaronowęży i zaczęła polować na ludzi. Ale jak gryzie, to nic nie robi, bo makaron jest miękki.
* Vz: Elena znalazła przy użyciu duchów 9 instancji 'żywego wężomakaronu' który pełza i włazi we włosy. I Irka, który egzorcyzmuje wężomakaron. Petry nadal nie ma. Ale są drony zwiadowcze. Najpewniej Petry. Sporo dron. Jedna drona na Elenie.

Elena robi małe strzałeczki z papieru by rozstrzelać te drony. 

Tp M +3 +3Ob: 

* V (podbicie): Elena zestrzeliła dronę śledzącą Elenę.
* Xm: (PARADOKS): interakcja czarów. AKTUALNY wpływ + Paradoks Eleny + zablokowana sentisieć ->
    * duchy zaczynają podlegać temu samemu efektowi co makaron. 
        * (coś animuje makaron i nadaje mu Mroczną Agendę - to samo teraz wpływa na DUCHY)
    * to długoterminowy proces.
    * SOLIDNIE zablokowana sentisieć.
* V (manifest): Elena zestrzeliła WSZYSTKIE drony Petry. Petra nie ma dron.
* V (eskalacja): Elena ma 'klucz dzikich papierowych samolotów'. Broń. Słucha jej.

TERAZ ELENA JEST WŁADCZYNIĄ NIEBIOS A NIE PETRA!!!

* Petra: Co Ty zrobiłaś! CO TY ZROBIŁAŚ!!! /wściekłość i strach
* Elena: Trzeba było nas nie śledzić.
* Petra: (zaniemówiła) wiedziałam, dyletantka, niekompetentna idiotka... 
* Elena: Nie musisz się przedstawiać
* Petra: (rozłączyła się)

Po rozmowie z Eleną, Petra jest przekonana, że Elena jest psychopatką. Jest szalona, jest nieodpowiedzialna i ogólnie jakkolwiek Petra nie jest awatarem dobrego zachowania, ale ELENA JEST GORSZA!

Czy Petra wiedząc, że Elena jest w pobliżu się przezwycięży i przeprosi Wiktora i osiągnie zniszczenie płaszczki? `Tr+3`

* VVrV: TAK.
* VzX: częściowo.

Karolinus idzie pić z Irkiem. Irek jest wykończony. Znowu. Ale tym 'robił makaron'.

* I: tien Samszar, jeden z nielicznych którym zależy na czymkolwiek...
* Karolinus: miło CIę widzieć, słyszałem że masz ciężki dzień chodź ja stawiam
* I: nie mogę pić. Musimy pomóc innym. Tien Samszar, ci ludzie są opuszczeni przez swojego tiena. 
* Karolinus: dlatego tu jesteśmy.
* I: to jakaś młoda lafirynda. Ona nic nie dba o tych ludzi. Nie to co Ty. Pomóż mi uratować to wszystko. Ty masz sentisieć.
* Karolinus: jak to nie dba?
* I: jak się pojawiła, zaczęła robić jakieś reportaże, jakieś filmiki społecznościowe, nie robiła tego co tienka powinna. A potem zaczęły się problemy z makaronem. I zaczęła latać.
* I: z tego co wiem, problemy były ZANIM ona się pojawiła, ale to takie normalne, międzyludzkie. I miała je rozsądzać. I jej się nie chciało. A teraz są magiczne. A ja nie jestem silnym magiem.
* Karolinus: masz sugestię co należy zrobić?
* I: TAK! Cieszę się, cieszę się, że podjąłeś tą decyzję! Dołącz do Grupy Proludzkiej Aurum! To należy zrobić! Wspólny front magów przeciwko magom znęcającym się nad ludźmi!
* Karolinus: a poza socjalnymi rzeczami?
* I: Ogólnie, to jeśli nie znajdziemy sposobu, to poproszę kogoś z GPA z rodu Samszar by tu przybył i pomógł opanować tą... _damę_ (z pogardą). Ale jeszcze mam nadzieję, że da się ją... przekonać.
* Karolinus: a gdzie ona jest?
* I: nie wiem, czegoś szuka na gwałt. Coś monitoruje. Nie wiem, rozpełzanie się makaronu? Będzie w powietrzu. Łap ją na sentisieci.
* Karolinus: kontaktuje się z kimś, władzami?
* I: robiła co miała robić na odpieprz i poszła robić swoje.

Wiktor próbuje _coś_ zrobić z płaszczką. W końcu Petra poprosiła i przeprosiła i czas kończyć.

Ex Z M +3 +3Ob:

* XXOb: (niemożność opanowania płaszczki bez ELIKSIRU APOKALIPTYCZNEJ ŚMIERCI; sentisieć wypaczyła płaszczkę)

Karolinus -> Petra:

* Petra: Halo? O co chodzi, jestem zajęta. /nacisk (rozbrajają z WIktorem chędożoną płaszczkę)
* Karolinus: Z tej strony Karolinus Wielki, natychmiastowo chcę się spotkać
* Petra: Jesteś na tym terenie?
* Karolinus: Tak
* Petra: To się możesz spotykać z taką... głupią Eleną. Nie mam czasu. Mam problemy do rozwiązania.
* Karolinus: Przybyłem rozwiązać problemy...
* Petra: ELENA Cię wezwała? Byś naprawiał po niej co spieprzyła?
* Karolinus: Dostałem polecenie z samej góry. Nie będziesz współpracować, zgłoszę samej górze. I to że wyłączyłaś sentisieć. ALbo się spotkamy i pogadamy albo kolejne telefony.
* Petra: "JA wyłączyłam sentisieć?" /niedowierzanie "Przed chwilą próbowałam ją włączyć, ale ELENA ją zablokowała. Chcesz to mi pomóż ją odblokować!"
* Karolinus: To gdzie się spotkamy?
* Petra: (myśli) Czekaj. Daj mi 30 minut.
* Karolinus: Ok. Czekam na kontakt.

.

* Karolinus -> Irek: "Dołączę do GPA ale muszę lecieć naprawiać. Będę dzwonił."
* I: OBIECUJESZ? 
* Karolinus: Tak.
* Irek: (morale++, robi odpowiednie ruchy by ułatwić i rekomendować Karolinusa)

.

Słychać krzyk na zewnątrz. Ogromny strain makaronu robi się w takiego dupnego węża i on oplątał człowieka, i oni oboje są opętani tym samym duchem. Makaronostwór łapie innego nieszczęśnika i przyciąga człowieka.

Elena używa swoich 'ptaków' żeby odcinać wszystko, co makaronotwór emituje makaronem poza ciało.

Tp Z +2:

* X: makaron oblepia 'strzałki' Eleny; część jej ptaków obraca się na mroczną stronę i Elena musi wygasić te elementy czaru (zamiast 20 zostaje 12)
* Vz: (podbicie) udało się oderwać temu człowiekowi którego makaronotwór łapał.
* V: (manifest) makaronotwór nie jest w stanie nikogo skrzywdzić póki są tu ptaki.

Wspierani przez 'ptaki', Elena i Irek owijają streczem makaronotwora żeby ta cholera nie mogła się ruszać i skończyła jak powiązany snop siana.

Tr Z +3 +3Or:

* V: udało się Irkowi i Elenie zacząć oplątywanie. Makaronotwór niewiele zdziała, jest zablokowany na tym terenie.
* X: Elenie nie zależy, żeby człowiekowi w środku nic się nie stało i to widać.
* X: Elena dostaje reputację, że "nie chodzi o ludzi, tylko o duchy".
* V: Makaronotwór wyłączony z akcji.

Elena. Irek. Karolinus. Mają MOMENT, zanim tego się powiększy i będzie więcej.

* Karolinus chce spotkać się z Petrą i włączyć sentisieć.
* Elena nie chce wyjść na idiotkę - to PETRA wyłączyła. ONA tylko przypieczętowała.

Spotkanie z Petrą.

* Petra: Ty chyba żartujesz, przyprowadziłeś tu tą szkodniczkę?
* Karolinus: Nie ma czasu na wyjaśnienia, są wielkie problemy, włączamy sentisieć
* Petra: Masz rację. PRZEZ NIĄ! (pokazała palcem)
* Elena: Żeby Ci ten palec nie zwiędł. Nie zwalaj na kogoś innego. Trzeba było nie blokować sentisieci i współpracować. Kooperacja nie jest Twoją mocną stroną.
* Petra: I KTO TO MÓWI! Blokujesz sentisieć i nie współpracujesz tylko zniszczyłaś moje drony.
* Elena: Ty zablokowałaś sentisieć. Przybyłaś po tiktoki i selfiki a nie porządną pracę. Nie bawić się.
* Petra: Ja KONTROLOWAŁAM sentisieć. Ty swoją niekompetencją zablokowałaś ja na dobre.
* Elena: Nagle się zablokowała...

Karolinus patrzy. "Przestańcie, potrzebuję kogoś kto mi pomoże ją odblokować."

* Petra: ja Ci pomogę. Nie wiem co ONA zrobiła, ale ja Ci pomogę. To socjopatka.
* Elena: ja też Ci mogę pomóc. Na pewno bardziej kompetentnie niż ona.
* Karolinus: Petra, po prostu nie przeszkadzaj, my odblokujemy z Eleną sentisiec.
* Petra: Spodziewałam się, że wybierzesz tą lafiryndę.
* Elena: Trzeba było makaron przyprowadzić by się Tobą zajął...
* Karolinus: (po cichu) wybrałem ładniejszą

Próba odblokowania sentisieci. Karolinus i Elena. System jest niestabilny.

Ex Z (Elena) M +3 +5Ob (wzrost Skażenia i problemów) +3Og (Trzecia Stronę):

Patrzą na trudność.

* Karolinus: Może nam się w dwójkę nie udać.
* Petra: Słuchaj, jeśli ta... bezwartościowa tienka przeprosi i przyzna się do tego, żę to z jej winy, pomogę. Ale ona próbuje wszystko zrzucić na mnie i bez niej kontrolowałam sytuację. Teraz wszystko wyrwało sie spod kontroli. Robię co mogę, wezwę wsparcie, ale... (lekka bezradność)
* Karolinus: Przysłali mnie tu bym rozwiązał problem. Elena nawiązała z Tobą kontakt na moją prośbę. Ty się rozłączyłaś. Później wyszło co wyszło...
* Petra: Nazwij po imieniu - Twoja podwładna spieprzyła.
* Karolinus: ...dlatego nie wracajmy to co było tylko naprawmy problem bo jeśli nie naprawimy to napiszę raport na Ciebie i na Elenę i nic nie będzie przyjemnego. Mnie przysłano tu bym to naprawił. Po Tobie.

Próba zastraszania Petry przez Karolinusa. `Ex Z +2 +3Or.`

* Vz (podbicie): Petra TYMCZASOWO idzie za planem, ale potem się okaże, że Karolinus blefował i nie miał prawa dowódczego.
* Xz (podbicie): Petra jest WŚCIEKŁA i na Karolinusa (bo ją oszukał i ośmieszył) i na Elenę. Nie zapomni tego.
* X (manifest): Petra jest wrogiem Karolinusa i Eleny. Chce udowodnić ich "zło".
* X (eskalacja): Petra NIE UMIE się uspokoić. Chce pomóc, ale po prostu NIE UMIE. I zarówno Elena i Karolinus to widzę.

Jednak rzucamy czar we dwójkę. Petra po prostu w tym stanie się nie przyda.

`Ex Z (Elena) M +3 +5Ob (wzrost Skażenia i problemów) +3Og (Trzecia Stronę)`:

* Vm (podbicie): Sentisieć zaakceptowała Karolinusa wspieranego przez Elenę za 'uprawnionych'. Sentisieć DALEJ jest zablokowana, ale zaczęła operację 'czyszczenia'.
* Vr: (manifest): Sentisieć wraca do normy, pod kontrolę. (-2Ob)

Petra patrzy z autentycznym szokiem. (jej zdaniem: Elena to jakaś "praktykantka" a Karolinus to "pro mag od sentisieci"). Elena dalej naciska na sentisieć - co tu się dzieje?

* Og: Trzecia Strona osiąga cel niszczenia _dowodów_
* V: Elena dostała widok na dziwną sytuację
    * W górze strumienia jest martwa płaszczka Blakenbauerów, jakaś... dziwna i mocna
    * Elena wykryła oddalającego się Wiktora Blakenbauera
    * Sytuacja wraca pod kontrolę.

Petra w milczeniu ewakuuje się z tego terenu. Elena "idź robić rolki, tylko do tego jesteś dobra". Petra zacisnęła pięści, ale NIC nie powiedziała. Karolinus: "drzwi zamknij za sobą, w jaskini mieszkasz?"

## Streszczenie

Rośnie napięcie między Hiperpsychotronikami i Eleną, bo Elena znalazła połączenie ich badań z Eternią. Karolinus nadał Wacława Amandzie... łącznie z Itrią. Tymczasem w Triticatusie (mieście makaronu) tienka mająca rozwiązać problem, Petra, weszła w głupi zakład z Wiktorem Blakenbauerem. Gdy Zespół tam trafił, Elena pokłóciła się ostro z Petrą, zestrzeliła jej drony i Paradoksem zablokowała sentisieć. Petra, przekonana, że Elena jest psychopatką przeprosiła Wiktora. A tymczasem Ożywiony Makaron stanowił kłopoty. W końcu Karolinus odblokował sentisieć, Wiktor zniszczył płaszczkę i wszystko JAKOŚ wróciło do normy, acz Petra została wrogiem Zespołu.

## Progresja

* Karolinus Samszar: Wacław jest LOJALNY Karolinusowi, bo Karolinus wielokrotnie udowodnił, że dba o niego. I zapewnia mu dziewczyny przez Złoty Cień. 
* Karolinus Samszar: Petra Samszar jest przekonana, że on tylko udaje pozytywnego. Chce wykazać jego wielkie zło.
* Elena Samszar: Petra Samszar jest przekonana, że ona jest psychopatką i nie dba o nikogo. Chce to ujawnić i udowodnić.
* Elena Samszar: po operacji chronienia duchów udowodniła w okolicy, że dba o duchy, ale o ludzi zupełnie nie.
* Wacław Samszar: jest LOJALNY Karolinusowi, bo Karolinus wielokrotnie udowodnił, że dba o niego. I zapewnia Wacławowi dziewczyny przez Złoty Cień.
* Wacław Samszar: ma gwarantowane (przez Karolinusa) dziewczyny ze Złotego Cienia i dlatego frontuje Złoty Cień i swoje interesy w tym obszarze.
* Petra Samszar: wie, że Karolinus i Elena S. nie są pozytywni. To okrutne osoby a Elena to czysta psychopatka. Petra - jako dziennikarka - wykaże mrok tej parki.

## Zasługi

* Karolinus Samszar: wsadził Wacława (za regularne dziewczyny) Amandzie w Złotym Cieniu jako front, powiedział Irkowi że dołączy do Grupy Proludzkiej Aurum, ogólnie mediator i dyplomata. Nieskutecznie zastraszał Petrę, wraz z Eleną odblokował sentisieć - PERFEKCYJNIE ją odblokował i Petra aż była w szoku. Rozwiązał kryzys ^_^.
* Elena Samszar: potraktowana przez Hiperpsychotroników amnestykami i nie wie co jej zrobili. Mistrzyni ciętej riposty konfliktująca się z Petrą; używając znajomego Ignatusa Blakenbauera doszła do linku eternijskiego Hiperpsychotroników. W Triticatusie skonfliktowała się z Petrą, rozwaliła jej drony, Paradoksem zablokowała sentisieć, uratowała nieszczęśnika opętanego przez makaron i ogólnia była niebezpieczna i konfliktowa. Ale skuteczna.
* Petra Samszar: jako tien na włościach miała chronić Triticatus, ale była znudzona; chciała pokonać Wiktora Blakenbauera i pokazać mu że jako alchemik jest mało groźny. Myliła się. Po tym jak zablokowała sentisieć ścięła się z Eleną S. i gdy TAMTA zablokowała sentisieć na dobre, Petra przeprosiła Wiktora i chciała wycofać działania. Uznała, że Elena to psychopatka. Chciała współpracować JEŚLI będzie przeproszona. Nie została przeproszona, tylko wzgardzona przez zarówno Karolinusa i Elenę.
* Wiktor Blakenbauer: gdy Petra wjechała mu na ambicję, powiedział jej że może zniszczyć cały Triticatus gdyby nie sentisieć. I faktycznie, jego eliksiry skutecznie powodowały problem - ale gdy Petra przyszła i przeprosiła, anulował operację. Świetny alchemik o rybich oczach.
* Irek Kraczownik: wypuszczony przez Samszarów, dołączył do Grupy Proludzkiej Aurum i pomaga ludziom w Triticatus jak jest w stanie. Wpierw odbił się od Petry - tience nie chce się 'robić', potem temat przejął Karolinus i Elena. Ale Irek robi co może by ratować ludzi. Postawił reputację na szali, by Karolinus dostał szansę w GPA.
* Wacław Samszar: Karolinus wkręcił go w frontowanie Złotego Cienia dla Amandy. Wacław, po amnestykach ("niekompetentny dla Hiperpsychotroników") uznał, że TYLKO Karolinusowi na nim zależy i tylko Karolinus jest jego prawdziwym przyjacielem.
* Ignatus Blakenbauer: nerd, kolega Eleny S.; z dziwnymi zainteresowaniami (które pomagała mu badać). Znalazł dla Eleny połączenie pomiędzy Hiperpsychotronikami i Eternią, acz wpakował się na ich celownik.

## Frakcje

* Grupa Proludzka Aurum: Irek Kraczownik do nich (teraz) należy; agresywnie rekrutują Samszarów licząc na to, że harmonia zadziała. Na razie - niski success rate.
* Hiperpsychotronicy: wypuścili Elenę S. i Wacława i OD RAZU żałują - Elena grzebie w ich Eternijskich działaniach, przy użyciu Ignatusa Blakenbauera, czyli poza zasięgiem. Muszą coś z tym zrobić - i z Eleną i z Ignatusem, ale co i jak? Ogólnie ostatnio raczej tracą a nie zyskują.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Triticatus: miasteczko niedaleko Mirkali słynące przede wszystkim z produkcji świetnego tradycyjnego makaronu; niedaleko granicy z Blakenbauerami; około 11k osób
                                1. Makaroniarnia (NW)
                                    1. Strumień Pszeniczny
                                    1. Systemy Irygacyjne
                                    1. Wielki Młyn: miejsce, gdzie ziarna pszenicy są mielone na mąkę.
                                    1. Semolinatorium: tu m.in. przez mieszanie tworzy się ciasto do makaronu. Maszyny, sprzęt itp.
                                    1. Pola Pszenicy: tu znajdują się rozległe pola upraw pszenicy, niezbędne do produkcji makaronu.
                                    1. Formatornia: tu są maszyny do formowania makaronu
                                    1. Wielka Suszarnia: tu się suszy makaron, duża otwarta przestrzeń
                                1. Pszenicznik (SE)
                                    1. Magazyny: tu są pakowalnie i magazyny
                                    1. Dworzec Maglev
                                    1. Obszar administracyjny
                                    1. Rezydencje mieszkalne
                            1. Triticatus, północ: 
                                1. Dopływ Strumienia Pszenicznego

## Czas

* Opóźnienie: 4
* Dni: 3

## Specjalne

* .

## OTHER
### Fakt Lokalizacji
#### Triticatus

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Triticatus: miasteczko niedaleko Mirkali słynące przede wszystkim z produkcji świetnego tradycyjnego makaronu; niedaleko granicy z Blakenbauerami; około 11k osób
                                1. Makaroniarnia (NW)
                                    1. Strumień Pszeniczny
                                    1. Systemy Irygacyjne
                                    1. Wielki Młyn: miejsce, gdzie ziarna pszenicy są mielone na mąkę.
                                    1. Semolinatorium: tu m.in. przez mieszanie tworzy się ciasto do makaronu. Maszyny, sprzęt itp.
                                    1. Pola Pszenicy: tu znajdują się rozległe pola upraw pszenicy, niezbędne do produkcji makaronu.
                                    1. Formatornia: tu są maszyny do formowania makaronu
                                    1. Wielka Suszarnia: tu się suszy makaron, duża otwarta przestrzeń
                                1. Pszenicznik (SE)
                                    1. Magazyny: tu są pakowalnie i magazyny
                                    1. Dworzec Maglev
                                    1. Obszar administracyjny
                                    1. Rezydencje mieszkalne
                            1. Triticatus, północ: 
                                1. Dopływ Strumienia Pszenicznego

