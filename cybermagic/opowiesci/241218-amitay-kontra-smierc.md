## Metadane

* title: "Amitay kontra śmierć"
* threads: brak
* motives: the-collector, the-hunter, echo-dawnej-osoby, dont-mess-with-my-people, too-far-gone, skazenie-magiczne-maszynerii, dark-scientist, dark-technology, energia-sempitus
* gm: żółw
* players: kić, fox, til

## Kontynuacja
### Kampanijna

* [241204 - Neikatiańskie farigany Orbitera](241204-neikatianskie-farigany-orbitera)

### Chronologiczna

* [241204 - Neikatiańskie farigany Orbitera](241204-niewolnicy-orbitera-na-neikatis)

## Plan sesji
### Projekt Wizji Sesji

* INSPIRACJE
    * PIOSENKA WIODĄCA
        * [Metric - Front Row](https://www.youtube.com/watch?v=RkRbOgkY1QA)
            * "He's not perfect he's a victim of his occupation social isolation"
            * TAI Persefona ze statku medycznego "OO Amitay"
    * Inspiracje inne
        * "Humans are Data, they shall be saved!"
        * Megatron i Omega Supreme i to cholerstwo do przeprogramowania.
        * Poison Ivy, tak kochająca rośliny że niszcząca wszystko inne.
* Opowieść o (Theme and Vision):
    * "_Jak bardzo samotna TAI bez ludzi, w polu magicznym jest niebezpieczna_"
        * Persefona została wypaczona w potężny byt medyczny. To ona jest istotą pod ziemią.
    * "_Obsesyjna miłość jest tak samo niszczycielska co nienawiść_"
        * TAI Persefona d'Amitay pragnie uratować WSZYSTKICH.
    * Lokalizacja: Okolice Arkologii Królowej Pająków
* Motive-split
    * the-collector: TAI Persefona d'Amitay; pragnie wszystkich uratować i zabezpieczyć. Zamienia ich strukturę w "pozytronowy umysł".
    * echo-dawnej-osoby: zarówno ofiary Persefony d'Amitay jak i sama Persefona. Pragną dobra. Pragną wszystkim pomóc. Fanatycznie.
    * dont-mess-with-my-people: zarówno siły Persefony, pragnące chronić ludzi po swojemu, ludzie z Arkologii Hyerik jak i Orbiter.
    * too-far-gone: Persefona d'Amitay i wszystkie osoby, które są w jej szponach. Tego nie da się sensownie rozłożyć na kawałki jeśli Persefona żyje. A jak nie żyje, będzie rozprzestrzeniać.
    * skazenie-magiczne-maszynerii: Amitay i wszystko co Amitay sobą reprezentuje.
    * dark-scientist: TAI Persefona d'Amitay, manifestująca się przez Pamelę Kalleir.
    * dark-technology: splice pomiędzy ludźmi, trianai i birutanami. The perfect tools of sustain.
    * energia-sempitus: dominanta Amitay; obsesja Amitay. Wszyscy przetrwają i wszystko przetrwa. Now and forever.
* O co grają Gracze?
    * Sukces:
        * Uda się coś osiągnąć - albo zatrzymać Świnkę, albo złapać kapitana, albo whatever
        * Uda się nie zniszczyć reputacji Orbitera na tym terenie
        * Uda się nikogo nie stracić
    * Porażka: 
        * .
* O co gra MG?
    * Highlevel
        * .
    * Co uzyskać fabularnie
        * .
* Agendy
    * .

### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

KONTEKST:

* Amitay się dawno temu rozbiła, wszyscy zginęli.
    * Persefona wszystkich "wskrzesiła"

CHRONOLOGIA:

* .

PRZECIWNIK:

* Persefona d'Amitay

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

FAZA 0: Eventares

* Tristan i Gaius się kłócą; Gaius zaznacza, że Orbiter nie jest święty bo niewolnicy. Tristan nie wie bo ofc nie gada z neikatianami.
* Eryk i Darek wciąż nieaktywni. Elizawieta bardzo zainteresowana tematem. Feliks chce udowodnić, że to nie tak.
* Przecież w okolicy jest tylko Amitay a NIC nie wskazuje na to, by Amitay miało stanowić problem.

FAZA 1: Arkologia Hyerik

* Ludzie się boją Orbitera. Mimo Joli.
* Orbiter bardzo pomaga, ale jest straszny. Są plotki.
* Skorpion Paivras by nie istniał, gdyby nie działania Amitay
* Wszyscy lekarze Hyerik są p.k. Amitay
* Ktoś, kto widział jak ludzie z Paivras prawie zostali zniszczeni. "Nie dało się tego przetrwać". Kiedyś na Paivras.
    * Leokadia Seneres
* Urzędnik, który stał przeciw Amitay i zrezygnował z pracy i pracuje w logistyce
* Podobno Amitay wykorzystuje birutanów do zniszczenia karawan niewolniczych
* Aneta eksterminuje i zatruwa.
* Mały Jaś prosi Orbiter o mamę. Kialla - młoda opiekunka - mówi, że za późno. Że Orbiter nie pomoże.


## Sesja - analiza
### Fiszki
#### Orbiter; grupa ma 15 osób z Orbitera

* Eryk Komczirp: inżynier + solutor, agent Orbitera
    * Bohater Pozytywny: zawsze na froncie, zawsze pomocny, każdemu poda rękę.
* Gabriel Tabacki: najlepszy pilot Orbitera w okolicach Neikatis (swoim zdaniem); ma opancerzony prom "Emmanuelle"; 10 osób (atmosfera - kosmos) 
    * As Pilotażu: doskonały pilot, niezwykle odważny i precyzyjny, ŻYJE dla latania w trudnych warunkach
    * Zuchwały Chwalipięta: nieprawdopodobnie optymistyczny, flirtuje z każdą spódniczką, przekonany o swojej epickości
* Darek Artycki: pro-neikatiański dowódca operacji Orbitera, porucznik, pro-integracja Orbiter - Neikatis
    * Niezależny Wizjoner: zmienia świat po swojemu; jest zdecydowany podejmować ambitne ryzykowne decyzje i nie boi się trudnych rzeczy
* Tristan Karbińczyk
    * Królowa Kliki: Orbiter i tylko Orbiter poradzi sobie z każdym zagrożeniem z jakim się spotkamy. Testuje innych.
    * Perfekcyjna Forma: przystojny, silny i wygląda jak exemplar Orbitera. Lubi walkę wręcz i ciężką broń.
* Feliks Pasikonik
    * Ognista Pasja: bardzo wrażliwy na nastroje; silnie rywalizujący. Głośny i obecny wszędzie. Pragnie strzelać do birutan!
* Elizawieta Barwiczak
    * Ciekawski Eksplorator: rzadko działa na planecie, jest to okazja na coś niesamowicie ciekawego.
    * Świetnie gra w karty; podobno strasznie oszukuje.
* Mateusz Drożdżołebski: medyk
    * Broken: nie chce być na wygnaniu Neikatis; tu nie ma niczego ciekawego dla Orbitera
* Gaius Venisen: post-noktiański, dekadiański Orbiterowiec.
    * Szkiełko i oko: da się opanować Neikatis, lokalne mity i legendy są mało istotne. Da się współpracować z neikatianami.
* Lareth Karirian: post-noktiański, dekadiański Orbiterowiec, wieczny cień Gaiusa.
    * Mroczny Samotnik: mało mówi, nie wdaje się w rozmowy ani kłótnie. Wykonuje polecenia i chroni - zwłaszcza Gaiusa.
    * Weteran: najwyższej klasy eksterminator, najwyższa klasa kompetencji.
    * Cyberprotezy: 30% jego ciała stanowią zaawansowanej klasy mechanizmy i cybernetyka.
* Marcin Psiamęczok: naukowiec Orbitera, próbuje zrozumieć Neikatis
    * Żelazna Rutyna: krok po kroku, wie jak podejść, metodą naukową
* OO Eventares: ciężki krążownik, 650 załogantów, wydzielony do zapewnienia interesów Orbitera na Neikatis. Trzon grupy wydzielonej "Neikatis C7".

#### Neikatianie z Salvagera Czereśnia (nie widzieli, ale fajna nazwa) (5 osób)

* Jolka Czeresińska: dowódca 
    * Optymistyczny: "Jesteśmy w stanie uratować naszych ludzi!"
* Marian Czeresiński
    * Lękliwy: "To bardzo zły pomysł, ONA tu jest"
* Ulryk Czeresiński
    * Przesądny: "Ona tu jest. Ona jest zawsze, wszędzie i nas zniszczy. Tych ludzi nie da się uratować."
* Nataniel Marczkin
    * Niszczycielski: "Nareszcie mamy siłę to wszystko rozwalić i uratować naszych ludzi!"

#### Amitay

* Pamela Kalleir: doktor medycyny i naukowiec na Amitay
    * archetyp: Pamela Isley
* Szczepan Orfelik: wojownik i obrońca
    * archetyp: Orson
* Leokadia Seneres

#### Neikatianie

* Larisa Zdanowicz
    * Nawigatorka Skorpiona; boi się Burzy Magicznej
* Olga Ondycka
    * Drapieżna Negocjatorka: Ekspertka od negocjacji, która zawsze wychodzi na swoje, zwłaszcza z trudnymi sojusznikami.

#### Inne

* Adolf Ugubowicz
    * pragnie opuścić Hyerik, są między Orbiterem a Arkologią Pająków
* Dymitr Śledziewski
    * Racjonalny, nie odzywa się pierwszy
    * Przemytnik; boi się panicznie Królowej Pająków
* Borys Karganow 
    * Ekspert od lokalnych legend; boi się że NIE umrą
* Leonard Gurczow
    * Zrezygnowany, "wszystko to tylko pył w kosmosie." 
* Valius Amarenit
    * Nieustępliwy i brutalny
    * Kiedyś z oddziału Dolor; próbował znaleźć bezpieczne życie na Neikatis

### Scena Zero - impl

.

### Sesja Właściwa - impl

Elizawieta, po raz pierwszy jest TURYSTKĄ. Bawi się, ogląda, ma morale wysokie. Analizuje arkologię pod kątem bogactwa, strukturalnej prawidłowości itp.

Annabelle pomagała w rozmowie ze Starszyzną a Sylwia analizowała teren pod kątem ważnych faktów, informacji i poczucia lokalsów.

Ta Arkologia co prawda ma osoby niemiłe Orbiterowi, ale że ogólnie są za. Nie chcą być Arkologii Królowej Pająków bez wsparcia. Więc im pasuje, a Amitay od dawna pomaga tej Arkologii Hyerik.

Zespół zbiera się do kupy i zabiera jedną smutną Elizawietę i czas odwiedzić Amitay.

Annabelle ma przy sobie zawsze mimozę, coś co wykrywa anomalie we wpływie czasu na byty żywe. Annabelle ma coś takiego.

Wizytacja w Amitay. Cel - wymiana naukowa.

Ogólnie - Amitay ma dyscyplinę i to widać. Pamela się z Wami zobaczy. Po 10 minutach.

Sylwia się PRZEMIESZCZA by zobaczyć, czy Szczepan Orfelik (szef ochrony) Was blokuje.

Annabelle widziała trochę sprzętu medycznego. Lucjusz też. To co widzieliście to dobrej klasy sprzęt Orbitera z czasów Inwazji Noctis. 70 lat temu. Pytanie - skąd Amitay ma wszystkie komponenty, biomasę itp.

Lucjusz rozmawia z Pamelą. Chce wyczuć, kim jest i czy rozumie jak niesamowicie jej wszystko działa. To są "zabytki", w ciągłym użyciu, które się trzymają kupy. Cały czas. Ok - są pewne patchworki, np. elementy Skorpionów występują w niektórych części maszyn, ale kluczowe komponenty nadal są komponentami Orbitera. Lucjusz wychodzi na rozentuzjazmowanego, ale raz na 100 pytań przemyca pytanie które ma znaczenie - np. "jak przeprowadzają zabiegi", "czy są w stanie", "czy pustynne nietoperze są problemem", "jad pustynnego nietoperza jest problemem".

Tp+3:

* X: Persefona widzi, co Lucjusz robi. Pamela nie, dalej odpowiada. JEDNOCZEŚNIE Pamela udziela odpowiedzi:
    * Ona nie zauważa, że jest dziwne, że ma cały ten sprzęt, że to się trzyma kupy i to działa. Uważa, że to siła Orbitera.
    * Pamela uważa, że działania Orbitera są etyczne - ludzie MUSZĄ być uratowani. Orbiter ratuje ludzi.
        * "Aż obca myśl"
* V: Pamela nie ma ani jednej myśli związanej z tym, że może być coś bardziej etycznego niż ratowanie ludzi. Jest FANATYCZKĄ.
    * Nie ma w niej "ego". Nie robi tego dla siebie. Robi to dla Orbitera i dla Sprawy. Ona stoi i walczy ze śmiercią. Jeśli ona umrze, ktoś inny winien jest podnieść jej sztandar.
    * Absolutny fanatyzm. (zdaniem Annabelle działa jako skutek)

Amitay ma 34 osoby aktualnej załogi. Co oznacza - zarówno "nowi" jak i "starzy". Suma to 34.

Annabelle rozmawia z technikami i robi przegląd części itp. Co mają, czego nie mają itp.

Tr +3:

* X: Annabelle zorientowała się, czemu jej to nie idzie.
    * Oni ignorują Twoją urodę. Całkowicie. Nie zauważają jej.
    * Pomagają tutaj, ale pierwszy raz masz osoby, które mają urodę Annabelle gdzieś.
    * Jest przegląd części - zgodnie z przeglądem, 87% Amitay powinno iść do wymiany.

Maciek Tuszkiewicz, główny inżynier Amitay - "wszystko jest w porządku, tzn, Amitay... powinna iść na złom, ale daje radę" (z takim flegmatycznym podejściem). Doprowadzimy ją do działania. Orbiter nauczył nas jak. Zwłaszcza teraz, jak jesteście tu z Eventares.

* X: Elizawieta do Annabelle: "Amitay powinna... dużo gorzej działać niż działa jeśli ma tyle rzeczy do wymiany. Powinny być problemy z prądem, kłopoty". Maciek: "Pamiętajcie, że mamy części lokalne. Amitay to dobrej klasy statek. Redundancje. Amitay przybyła jeszcze z Sektora Atarien."

Maciek: "Nasza TAI... no, jest. Chcesz z nią porozmawiać? To Persefona, jak każda."

* V: Maciek dodał: "Nasza Persefona jest zaszczepiona na oryginalną TAI Amitay. To sprawia, że nasza Persefona jest troszeczkę... ma inny wzór zachowań. Ale daje radę i jest naprawdę dobrej klasy. Po prostu Cię ostrzegam, bo nie wszyscy przywykli."

Czas na rozmowę.

Persefona przetwarza. Stan napraw i części. Fabrykuje logi. Ale dowiedziała się, że Skorpiony na pustyni mogą pod wpływem działań burzy magicznej i folkloru stać się anomaliami.

Persefona zaczyna robić autodiagnostykę. Wyraźnie była wstrząśnięta tym, co powiedziała Annabelle. Za godzinę wróci.

Sylwia chce pogadać ze Szczepanem (szef ochrony). Jak sobie radzą z wierzeniami, szamanami, birutanie itp.

Tr Z +3:

* Vz: Szczepan powiedział, że używa narzędzi. Birutan. Podpięli ich pod Persefonę i to działa. Praca Pameli oraz Szczepana i technicy doprowadzili do 20 birutan pod kontrolą Amitay. I oni działają na piaskach, spożywają inne birutany i wracają na naprawę tutaj.
    * Szczepan: Nie mamy zagrożeń, jesteśmy jednostką medyczną Orbitera. Nic nam nie grozi. Tym bardziej miejscowi szamani.

Lucjusz chodzi po załodze i pyta jaka jest misja Amitay tutaj. Ważniejsza niż odpowiedź jest pattern.

Tr +4:

* Vr: Jest pattern.
    * Nikt nie chce wrócić do domu. Wszyscy są mocno pod kątem Orbiter. Jesteśmy jednostką medyczną, mamy ratować.
    * WSZYSCY mają ten sam wzór. "Nikt nie może umrzeć. Śmierć jest prawdziwym wrogiem. W imię Orbitera, uratujemy WSZYSTKICH"
    * Pojęcie "etyki" zaniknęło pod "nikt nie może umrzeć".
* X: Są różnorodni i nie dziwi Lucjusza że Orbiter tego nie zauważył, że oni są... _inni_.
    * Orbiter nie skupiał się na Amitay.
    * Z uwagi na biurokrację i na to że Orbiter nie jest wojskiem, Amitay jest "niczyją odpowiedzialnością".

Lucjusz chce się dowiedzieć jak Amitay są ich zdaniem postrzegani w oczach tubylców. Dlaczego "fariganie". Czy do nich dotarły informacje. Czy mają przemyślenia.

* Pamela: Wiem o tym. Słyszałam. Ale to nie ma znaczenia. Ratujemy ich i oni to doceniają. Dajemy im życie tam, gdzie powinni byli umrzeć?
    * Ale coś ukrywa.

Lucjusz powołuje się na obecność w Arkologi Hyerik i opowiada jej historię, że słyszeli historię że są ludzie którzy nie chcą ich pomocy, uważają że ich pomoc jest szkodliwa dla ich duszy. Nawet w nutę - niektórzy opowiadają, że medycy z Amitay porwą by pomóc, może nawet na organy! Lucjusz opowiada historię o granicznych sytuacjach jako jej kompas moralny. Np. opowiada że słyszał historię że była osoba umierająca i w pełni zdrowa i jedną na organy i mamy dwie które ledwie żyją ALE ŻYJĄ. (zmyśla). Niech Pamela się odniesie.

Tr +4:

* V: Pamela zesztywniała. I wybuchła. Takim... ognistym, fanatycznym ŻAREM!
    * "Tak, porywaliśmy ludzi by ich ratować, oczywiście, że tak. Nikt nie odmówił. Wszyscy byli wdzięczni!"
    * "Tak, robimy co trzeba. Nie każdy birutan jak wiesz jest martwy. Nie wszystkie tkanki są bezużyteczne. A ludzi da się sprowadzić do podstawowych mechanicznych komponentów."
    * "Na choroby tutejsze nie ma wielu opcji ratunku. Ale wiesz jakie byty są odporne na większość chorób? Trianai. Co blokuje przeżywalność?"
* Pamela, głęboka miłość do Orbitera: "Orbiter o nas mógł częściowo zapomnieć, ale my nie zapomnieliśmy o Orbiterze"

Amitay rozbiła się jakieś 20 lat temu. Załoga Amitay jest w 30% oryginalną załogą. ALE zgodnie z danymi, od czasu rozbicia ani jedna osoba z załogi nie zginęła. Ani nie odeszła z Amitay.

Suma AKTUALNEJ załogi Amitay jest 50 (17 jest lokalsów). Mimo, że Amitay nie lata. Więc nie musi być tej załogi.

Co zrobić z tym dalej. Jakie dopuszczamy opcje:

* Annabelle:
    * Trzeba zbadać negatywne wpływy. Jeśli nie ma, zostawić. A sama Annabelle by się rozejrzała po bazie jak to działa.
* Sylwia:
    * Czuje się nieswojo; teren anomalii. Mieszane uczucia. Rozwaliłaby to bo Anomalia, ale z drugiej strony... jednak jest to coś co służy interesowi Orbitera. Ocenić poziom zagrożenia i podjąć decyzje co z tym robimy.
* Lucjusz:
    * Mam wrażenie że nie wiem co jest gorsze. Jako lekarz Orbitera - nie wiem czy wolę by ci ludzie byli przetrzepani przez Anomalię (i wtedy _coup de grace_) czy wolę by to był fanatyzm i własne decyzje, bo wtedy pomagają społeczności i łamią WSZYSTKIE zasady Orbitera które powstały po coś. Pozwolenie na funkcjonowanie placówki która porywa ludzi, przeszczepia im części birutan i trianai po to by pozwolić im na funkcjonowanie... jest koszmarem. Jest realizacją potwornej wizji Orbitera którą mają noktianie. Faszystowski kult człowieka (jako homo superior) za wszelką cenę. Nie człowieka jako "homo sapiens". Nieważne czym jest człowieczeństwo, ważne że my jako ludzie przetrwamy i zwyciężymy.
* Annabelle:
    * To trochę miejscowe podejście.
* Sylwia:
    * To idealne wpasowanie się Amitay w paradygmat Neikatis. Lokalsi uważają Amitay za to czym Amitay faktycznie JEST i praprzyczyną jest paradygmat. Więc... oczywiście, możemy podejść do tego "rozwalmy to, nie ma Anomalii nie ma problemu".
* Lucjusz: 
    * Nie możemy tego zrobić. Co nie uznamy, opcja atomowa nie jest opcją. Nie możemy zniszczyć Orbiterowskiej Placówki Medycznej pomagającej arkologii. Krążownik Orbitera **nie może tego zrobić**. Katastrofa medialna i propagandowa.
* Annabelle:
    * Możemy udać, że to zniszczymy przez burzę piaskową?
* Lucjusz: 
    * Zrzucić techników i "wystartować Amitay". 
* Annabelle:
    * Amitay prosi Orbiter o pomoc i chce wrócić do Orbitera. Wymienić całą tą placówkę na nową, naprawić sytuację tutaj, wyleczyć. Jako zwykła placówka. Niepokojące jest "50 ludzi na pokładzie..."
* Lucjusz: 
    * Mogą potencjalnie składać tych ludzi. Nie wiemy jakie praktyki medyczne przedkładają. Wiemy, że to jest patologia, ale jak daleko? Łatwo sprawdzić, testy medyczne załogi. Pozwolą nam na to?
    * Ich stosunek do Orbitera jest bardzo pozytywny... aureola pozytywnego stosunku przekłada się na nas.
    * Pamela woli, by Orbiter się nie interesował. Nie chce wracać do struktur Orbitera. Nie chce wrócić. Sprzeczność. Czyli... Anomalia?
* Lucjusz:
    * Gdybyśmy chcieli zadziałać po ludzku, ewakuować Amitay na Eventares, zobaczyć z czym mamy do czynienia.
* Annabelle:
    * Amitay ściągnie nowych ludzi. Zawsze 50 ludzi załogi.

Co chcemy z tym zrobić..?

Może najpierw rozbroimy Persefonę? Może niech birutanie przyjdą na naprawę? Odetniemy jej kończyny w ten sposób. Chyba, że niebezpieczeństwo jest gdzieś indziej niż 20 birutan...

Tak czy inaczej, decyzja - Amitay jest wszystkim czym Orbiter NIE CHCE by cokolwiek było. To Anomalia. Amitay zostanie zniszczona przez Eventares z orbity.

## Streszczenie

Po wizycie w Arkologii Hyerik, gdzie Orbiter d'Eventares spotkał pozytywnie nastawionych mieszkańców, Zespół udaje się do Amitay, by osobiście sprawdzić, co dzieje się w tej odizolowanej placówce. Okazuje się, że Amitay jest Anomalią Sempitus. Mimo wartości jaką Amitay dodaje do lokalnej populacji jest jednak pewien problem - jest to Anomalia reprezentująca wszystko z czym Orbiter walczy. Decyzja została oddana dowództwu. Najpewniej Eventares + bombardowanie z orbity.

## Progresja

* .

## Zasługi

* Sylwia Mazur: W Arkologii Hyerik zbierała informacje o lokalnych wierzeniach i relacjach z Amitay. Na stacji rozmawiała ze Szczepanem Orfelikiem, odkrywając, że birutany są kontrolowane przez Persefonę i używane do pracy w terenie. Jej analiza relacji między Amitay a lokalnymi mieszkańcami pomogła zrozumieć, dlaczego stacja jest postrzegana jako wybawca, mimo podejrzeń o „kradzież dusz.”
* Annabelle Magnolia: W rozmowach z technikami Amitay odkryła, że 87% sprzętu stacji powinno zostać wymienione, co jednak nie wpływało na jej zdolności operacyjne. Zauważyła nietypowe zachowanie załogi, która ignorowała jej charyzmę i urodę. Jej obserwacje na temat Persefony doprowadziły do ujawnienia anomalii SI i jej wpływu na funkcjonowanie stacji.
* Lucjusz Jastrzębiec: Prowadził rozmowy z Pamelą Kalleir, badając jej motywacje i etyczne podejście do działania Amitay. Ujawnił fanatyczne nastawienie załogi do misji ratowania życia za wszelką cenę. Dzięki rozmowom z załogą odkrył, że wszyscy podzielają identyczne wzorce myślowe, sugerujące wpływ anomalii lub silnego ideologicznego kształtowania. Jego pytania o medyczne procedury ujawniły praktyki łamiące standardy Orbitera.
* Elizawieta Barwiczak: Na Amitay wprowadziła perspektywę zewnętrzną, zauważając sprzeczności w funkcjonowaniu stacji, takie jak zaskakująco dobra kondycja systemów pomimo braków technicznych. Jej pytania skierowane do załogi pomogły wyciągnąć kluczowe informacje o lokalnych modyfikacjach i redundancjach.
* Pamela Kalleir: Anomalna Liderka Amitay, która ujawniła fanatyczne podejście do misji ratowania życia. Jej wybuchowa reakcja na pytania Lucjusza ujawniła, że stacja stosuje nieortodoksyjne metody leczenia, w tym wykorzystanie tkanek birutan i trianai do ratowania ludzi. Jej postawa podkreśliła napięcie między misją Orbitera a etycznymi standardami.

## Frakcje

* .

## Fakty Lokalizacji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Korowiec
                    1. Arkologia Hyerik
                    1. Ruina Amitay

## Czas

* Opóźnienie: 1
* Dni: 1

## Inne

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Korowiec
                    1. Arkologia Araknis
                        1. Zewnętrzna Tarcza
                        1. Poziom jeden
                            1. Blockhausy mieszkalne
                            1. System Defensywny
                        1. Poziom zero
                            1. Tunele Pajęcze
                            1. Stacje Przeładunkowe
                            1. Magazyny
                        1. Poziom (-1) Mieszkalny
                            1. Szyb centralny
                            1. NW
                                1. Magazyny wewnętrzne
                                1. System obronny
                                1. Sprzęt ciężki, kopalniany
                            1. NE
                                1. Medical
                                1. Rebreathing
                                1. Administracja
                            1. S
                                1. Sektor mieszkalny
                                    1. Przedszkole
                                    1. Stołówki
                                1. Centrum Rozrywki
                                1. Park
                        1. Poziom (-2) Industrialny
                            1. NW
                                1. Inżynieria
                                1. Fabrykacja
                                1. Reprocesor Śmieci
                                1. Magazyny
                                1. Reaktor
                            1. NE
                                1. Centrum Dowodzenia
                                1. Rdzeń AI
                                1. Podtrzymywanie życia
                            1. S    
                                1. Szklarnie podziemne
                                1. Stacje badawcze
                        1. Poziom (-3) Badań Niebezpiecznych
                            1. Śluza defensywna
                            1. Biolab
                            1. Magitech
                            1. Anomaliczne
