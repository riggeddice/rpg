# Motive
## Metadane

* Nazwa: Magowie ignorują obowiązki

## Krótki opis

Mag miał coś zrobić. Mag tego nie zrobił. Wszystko idzie w cholerę bo mag robi coś innego.

## Opis

Mag miał coś zrobić. Mag tego nie zrobił. Wszystko idzie w cholerę bo mag robi coś innego. W ramach tego motywu pokaż, jak inne siły próbują działać skoro mag nic nie robi. Pokaż zbliżającą się failure cascade, bo z jakiegoś powodu to mag miał robić - albo pokaż, że tak naprawdę ten mag nigdy nie był do tego potrzebny. Pokaż wyraźnie problemy wynikające z braku działania maga i co się przez to dzieje, jak z braku maga systemy zależne od jego obecności przestają działać.

## Spoilers
