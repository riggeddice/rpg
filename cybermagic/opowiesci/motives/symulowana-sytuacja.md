# Motive
## Metadane

* Nazwa: Symulowana sytuacja

## Krótki opis

Przygotowanie i przeprowadzenie scenariusza czy gry wojennej, których celem jest skonfrontowanie zespołu z niebezpieczną sytuacją.

## Opis

Przygotowanie i przeprowadzenie scenariusza czy gry wojennej, których celem jest skonfrontowanie zespołu z niebezpieczną sytuacją. Potencjalnie Grupa ma niesprawdzone, mniej doświadczone osoby. Potencjalnie te osoby mogą nawet nie wiedzieć o tym, że to symulacja. Tak czy inaczej, niebezpieczeństwo powinno być pozorowane. Z perspektywy postaci - albo oni uczestniczą w takim scenariuszu albo go muszą zaprojektować i częściowo odegrać.

## Spoilers
