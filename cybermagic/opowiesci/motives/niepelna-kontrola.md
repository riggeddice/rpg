# Motive
## Metadane

* Nazwa: Niepełna kontrola

## Krótki opis

Ktoś nie do końca kontroluje swoje ruchy i działania. Może to imbalans emocjonalny, może opętany a może coś innego.

## Opis

Ktoś nie do końca kontroluje swoje ruchy i działania. Może to imbalans emocjonalny, może opętany a może coś innego. W ramach tego motywu graj próbami utrzymania kontroli i zachowania siebie jak i efektami które się dzieją, gdy ktoś traci kontrolę nad sobą.

## Spoilers
