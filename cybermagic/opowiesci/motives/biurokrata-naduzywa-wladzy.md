# Motive
## Metadane

* Nazwa: Biurokrata nadużywa władzy

## Krótki opis

Ktoś, komu powierzono zajmowanie się dokumentami i sprawianie, by System działał prawidłowo wykorzystuje swoje uprawnienia do własnych celów i interesów.

## Opis

Ktoś, komu powierzono zajmowanie się dokumentami i sprawianie, by System działał prawidłowo wykorzystuje swoje uprawnienia do własnych celów i interesów. Bardzo trudno jest odkryć ten typ ingerencji nie mając własnego biurokraty albo sojuszników w urzędzie. W pewien sposób, z perspektywy zespołu, to jest coś w stylu _deus ex machina_ - system nie zauważa żadnego błędu. W tym motywie graj przede wszystkim przyczyną, dla której odpowiednio wysoko postawiony biurokrata zaryzykował karierę by osiągać cele nielegalnie.

## Spoilers
