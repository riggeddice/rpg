# Motive
## Metadane

* Nazwa: Lojalny do końca

## Krótki opis

Opłaca się zdradzić lub uciec. Nie opłaca się zostać. Ale KTOŚ jest lojalny do końca, niezależnie od ceny.

## Opis

Opłaca się zdradzić lub uciec. Nie opłaca się zostać. Ale KTOŚ jest lojalny do końca, niezależnie od ceny. Jest to działanie nielogiczne i nieopłacalne. Pokaż charakter i hart ducha osoby, która zdecyduje się być lojalna. Pokaż, dlaczego ktoś inny zasłużył na taką nieskończoną lojalność. Podkreśl jak bardzo to nietypowe (w ramach tego motywu). Podkreśl więzi i honor - lub nacisk kulturowy który do tego prowadzi.

## Spoilers
