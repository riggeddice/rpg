# Motive
## Metadane

* Nazwa: Dywersja gone wrong

## Krótki opis

On chciał tylko odwrócić od czegoś uwagę. Ale w wyniku stało się coś gorszego.

## Opis

On chciał tylko odwrócić od czegoś uwagę. Ale w wyniku stało się coś gorszego. Ten motyw pokazuje _failure cascade_ albo konkretne problemy wynikające właśnie z próby zrobienia dywersji, co miało nieoczekiwane efekty uboczne. W ramach tego motywu określ dywersja _przed czym_ i czym była dywersja, a potem podnieś problemy wynikające z samej natury dywersji. To 'nieudana dywersja' staje się kolejnym problemem do rozwiązania. 

## Spoilers
