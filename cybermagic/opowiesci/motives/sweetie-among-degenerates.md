# Motive
## Metadane

* Nazwa: Sweetie among Degenerates

## Krótki opis

Mamy postać 'słodką i niewinną'. Sympatyczną. I ta postać jest otoczona osobami znającymi twarde i okrutne życie. I wzajemna nieufność i różnice kulturowe działają.

## Opis

Mamy postać 'słodką i niewinną'. Sympatyczną. I ta postać jest otoczona osobami znającymi twarde i okrutne życie. I wzajemna nieufność i różnice kulturowe działają. W tym motywie graj różnicami między 'sweetie' a 'degenerates'; oni niekoniecznie są w bezpośrednim konflikcie, ale Degenerates śmieją się z naiwności Sweetie i mogą ją troszkę podpuszczać. Najpewniej obie strony nie są wobec siebie wrogie, raczej działa to w formie pobłażania i niezadowolenia. Pomyśl o tym jak 'młody porucznik' (sweetie) w towarzystwie 'żołnierzy co wiele widzieli i mają gruby humor' (degenerates) bardziej niż 'królewna śnieżka i siedmiu krasnoludzków, edycja xxx'.

## Spoilers
