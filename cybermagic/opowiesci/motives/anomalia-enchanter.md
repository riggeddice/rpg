# Motive
## Metadane

* Nazwa: Anomalia, Enchanter

## Krótki opis

Anomalia magiczna, która po spotkaniu nadaje magiczny efekt osobie, który przez pewien czas się utrzymuje

## Opis

Anomalia magiczna, która po spotkaniu nadaje magiczny efekt osobie, który przez pewien czas się utrzymuje. Np. "studnia życzeń", "jezioro a po wykąpaniu jesteś silniejszy", "las w który jak wejdziesz to za Tobą łazi duch". W ramach tego motywu graj raczej efektem anomalii na ludziach a samą anomalię trzymaj w ukryciu, tzn. "jak to się stało że zwykli ludzie zachowują się W TAKI SPOSÓB i TO było możliwe". Pomyśl o efektach ubocznych anomalii.

## Spoilers
