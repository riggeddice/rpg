# Motive
## Metadane

* Nazwa: The Desecrator

## Krótki opis

"Superman jest rasistą!". The Desecrator skupia się na tym, by przekształcić jakiś Symbol lub Wiarę, coś ważne dla grupy w źródło zła lub zniszczyć symbolikę tego czegoś.

## Opis

"Superman jest rasistą!". The Desecrator skupia się na tym, by przekształcić jakiś Symbol lub Wiarę, coś ważne dla grupy w źródło zła lub zniszczyć symbolikę tego czegoś. Koncentruje swoje działania na profanowaniu, zanieczyszczaniu lub całkowitym zniszczeniu tego, co jest uznawane za święte, wartościowe lub spajające dla danej społeczności czy grupy.

The Desecrator nie skupia się na jednostkach czy miejscach - jego prawdziwym celem jest sfera symboliki i ogólnie rozumianej Świętości. Zrobienie haremu z kapłanek jest czynem mającym pokazać, że bogini nie chroni swoich służek i każdy może upaść. Atakuje linię moralną i emocjonalną, próbując zabrać nadzieję i pokazać, że wartościowy jest jedynie świat doczesny - nie ma znaczenia tożsamość czy przynależność.

The Desecrator wygrywa, jeśli ludzie odwracają się od Symbolu lub jeżeli Symbol zaczyna być postrzegany jako źródło zła i problemu.

W ramach tego motywu pokaż jak społeczność odwraca się od Symbolu, jak traci wiarę. Pokaż, jak Symbol staje się postrzegany jako mniej wartościowy. Pokaż jak ważny był dla Społeczności. Graj też próbami utrzymania świętości w obliczu czynów The Desecrator.

## Spoilers
