# Motive
## Metadane

* Nazwa: Zagrożenie Pryzmatyczne

## Krótki opis

Pryzmat - w co wierzy odpowiednia populacja, może stać się prawdą. Z pewnych przyczyn jest duże zagrożenie, że na tej sesji Pryzmat zadziała mocniej przy mniejszej populacji.

## Opis

Pryzmat - w co wierzy odpowiednia populacja, może stać się prawdą. Z pewnych przyczyn jest duże zagrożenie, że na tej sesji Pryzmat zadziała mocniej przy mniejszej populacji. To sprawia, że trzeba unikać bycia przekonanym o czymś, mówienia niektórych rzeczy czy wierzenia w niektóre sprawy. W ramach tego motywu pokaż jak ludzie reagują na zagrożenie pryzmatyczne, pokaż zmieniającą się rzeczywistość oraz nie zapomnij pokazać potęgi - Pryzmat jest jednym z najsilniejszych efektów w EZ.

## Spoilers
