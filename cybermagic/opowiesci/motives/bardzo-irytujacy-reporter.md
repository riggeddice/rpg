# Motive
## Metadane

* Nazwa: Bardzo irytujący reporter

## Krótki opis

Czy to dziennikarz, gazetka szkolna czy prasa, nie da się działać dyskretnie a jeszcze non stop o coś pytają.

## Opis

Czy to dziennikarz, gazetka szkolna czy prasa, nie da się działać dyskretnie a jeszcze non stop o coś pytają. W ramach tego motywu prasa odkrywa rzeczy które wszystkim nie pasują, nie da się działać normalnie bo opinia publiczna i wszystko jest rejestrowane. A jeszcze dziennikarz jest stronniczy i wyraźnie ma agendę pod kątem której robi artykuł. Dodajmy fakt, że może używać ukrytych kamer, dron i zaskakiwać nieprzygotowane osoby pytaniami...

## Spoilers
