# Motive
## Metadane

* Nazwa: AMZ - Oczy Zaczęstwa

## Krótki opis

AMZ posiada dziesiątki dron monitorujących wszystko co się dzieje w Zaczęstwie. I TAI AMZ dyskretnie dba o to, by wszystko na terenie Zaczęstwa działało dobrze.

## Opis

**Ten motyw działa TYLKO w Zaczęstwie i okolicy.** AMZ posiada dziesiątki dron monitorujących wszystko co się dzieje w Zaczęstwie. I TAI AMZ dyskretnie dba o to, by wszystko na terenie Zaczęstwa działało dobrze. Ten motyw skupia się na wszechobecnym monitoringu Zaczęstwa przez TAI AMZ oraz na próbie dbania, by wszystko działało wystarczająco dobrze, z lekkimi ingerencjami tu i tam by zapewnić prawidłowość. W ramach tego motywu spraw, by właściwe osoby pojawiły się we właściwym miejscu, by 'przypadkiem' właściwe osoby były poinformowane a magowie będą tam, gdzie ich pomoc jest potrzebna.

## Spoilers
