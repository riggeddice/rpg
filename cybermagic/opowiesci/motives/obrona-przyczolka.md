# Motive
## Metadane

* Nazwa: Obrona Przyczółka

## Krótki opis

Przyczółek - miejsce, które udało nam się pozyskać na wrogim lub niebezpiecznym terenie. I teraz trzeba go utrzymać i obronić przed pojawiającymi się atakami.

## Opis

Przyczółek - miejsce, które udało nam się pozyskać na wrogim lub niebezpiecznym terenie. I teraz trzeba go utrzymać i obronić przed pojawiającymi się atakami. Przyczółek jest miejscem może nie idealnym, ale dość bezpiecznym i należącym do cywilizacji. W kontraście Okolica jest miejscem bardzo niebezpiecznym i niefortunnym. W ramach tego motywu graj właśnie tym kontrastem i pokaż siły zagrażające Przyczółkowi - trzeba obronić Przyczółek przed zniszczeniem.

## Spoilers
