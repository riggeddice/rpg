# Motive
## Metadane

* Nazwa: Mastermind gra na dwa fronty

## Krótki opis

Mastermind udaje, że jest aktywną stroną konfliktu po którejś ze stron, a tak naprawdę pragnie zwycięstwa też tej drugiej strony.

## Opis

Mastermind udaje, że jest aktywną stroną konfliktu po którejś ze stron, a tak naprawdę pragnie zwycięstwa też tej drugiej strony. Z jakiegoś powodu, żeby mastermind mógł osiągnąć swój sukces, obie strony muszą doprowadzić do pewnych działań. To sprawia, że mastermind aktywnie wspiera jedną ze stron, ale drugiej podpowiada co ma robić i osłabia działania swojej strony by druga też osiągnęła odpowiednie sukcesy. Dowcip polega na tym, że nikt nie kojarzy masterminda z drugą ze stron. W ramach tego motywu pokaż te działania - COŚ próbuje osiągnąć X, ale AKURAT ZBIEGIEM OKOLICZNOŚCI coś innego zrobiło Y co zapobiegło tego X. Pokaż, że tu się dzieje więcej niż się wydaje.

## Spoilers
