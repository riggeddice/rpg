# Motive
## Metadane

* Nazwa: Potwór z horroru

## Krótki opis

Specjalny typ przeciwnika. Nic o nim nie wiadomo, poza tym, że jest potężny i zabija - lub robi jeszcze gorsze rzeczy z ofiarami.

## Opis

Specjalny typ przeciwnika. Nic o nim nie wiadomo, poza tym, że jest potężny i zabija - lub robi jeszcze gorsze rzeczy z ofiarami. Zwykle znajduje się w cieniu i nie rozumiemy nawet jego agendy. Nie do końca wiemy gdzie jest i co potrafi. Gdy ujawnisz przeciwnika, da się go zabić i przestaje być straszny. W ramach tego motywu stwórz potwora z dużą mocą i nietypową agendą i wykorzystuj go tak, by maksymalnie destabilizować zespół i ich stresować. Pozostawiaj przeciwnika w cieniu i nie zdradzaj co potrafi, ale pokazuj ślady jego istnienia - np. inne potwory w jego obecności próbują nie być widoczne.

## Spoilers
