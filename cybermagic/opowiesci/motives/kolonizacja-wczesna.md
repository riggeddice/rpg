# Motive
## Metadane

* Nazwa: Kolonizacja, wczesna

## Krótki opis

Kolonizacja planety / planetoidy we wczesnym etapie. Problemy ze sprzętem, eksploracja, nie znamy terenu i dopiero się ludzie docierają.

## Opis

Kolonizacja planety / planetoidy we wczesnym etapie. Problemy ze sprzętem, eksploracja, nie znamy terenu i dopiero się ludzie docierają. Możesz swobodnie wykorzystywać rzeczy niespodziewane i nieoczekiwane. Graj nieznanym, nieoczekiwanym, problemami sprzętowymi i dylematami w którą stronę warto rozwijać kolonię.

## Spoilers
