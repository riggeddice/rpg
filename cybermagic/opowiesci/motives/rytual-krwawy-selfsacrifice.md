# Motive
## Metadane

* Nazwa: Rytuał, krwawy, self-sacrifice

## Krótki opis

W wyniku tego rytuału ktoś się poświęcił, by pomóc innym i by ogólnie było lepiej. Nie było presji.

## Opis

W wyniku tego rytuału ktoś się poświęcił, by pomóc innym i by ogólnie było lepiej. Nie było presji. To sprawia, że magia odpowiada inaczej. Nie ma negatywnych energii związanych z rytuałem i mimo obecności krwi nie ma tutaj dostępu do energii Esuriit. Energia skupia się by pomóc tym, w których stronę rytuał był kierowany. W ramach tego motywu graj głęboką sympatią i miłością osoby, która się poświęca. Pokaż więzi i przyczynę, dlaczego ktoś to zrobił. Pokaż, że uwolniono zbyt dużą energię - zbyt niebezpieczną energię - ale nie było tam zła.

## Spoilers
