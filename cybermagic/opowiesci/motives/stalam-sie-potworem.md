# Motive
## Metadane

* Nazwa: Stałam się potworem!

## Krótki opis

Patrząc na siebie, Ofiara odczuwa obrzydzenie. Jest czymś innym niż chciała. Pragnie powrotu do poprzedniej formy, niech będzie jak przedtem.

## Opis

Patrząc na siebie, Ofiara odczuwa obrzydzenie. Jest czymś innym niż chciała. Pragnie powrotu do poprzedniej formy, niech będzie jak przedtem. W ramach tego motywu pokaż ruchy, jakie wykonuje Ofiara próbując wrócić do przeszłości lub próbując odkupić wszystko co zrobiła. Pokaż obrzydzenie, przebijającą się szlachetność oraz walkę między TERAZ a WTEDY.

## Spoilers
