# Motive
## Metadane

* Nazwa: Pakt z diabłem

## Krótki opis

Jest coś, czego ktoś pragnie. Diabeł może to dostarczyć, ale trzeba zapłacić straszną cenę.

## Opis

Jest coś, czego ktoś pragnie. Diabeł może to dostarczyć, ale trzeba zapłacić straszną cenę. Mimo, że diabeł reprezentuje zło które wszyscy chcą wyplenić, tym razem jest w stanie rozwiązać problem. Ale za jaką cenę? Co KTOŚ odda diabłu, żeby stało się to czego pragnie? Diabeł nie musi być osobą - to może być oczywiście 'Syndykat Aureliona', ale to też może być 'Arazille', 'Esuriit' czy cokolwiek. Ważne, że żądanie diabła jest nieakceptowalne i cena jest sroga - tak jak nagroda. W ramach tego motywu spróbuj pokazać, dlaczego pakt z diabłem był dla kogoś DOBRĄ rzeczą, co dzięki temu dostał - a potem pokaż cały tragizm konsekwencji tego paktu.

## Spoilers
