# Motive
## Metadane

* Nazwa: Niechętnie spełniana rola

## Krótki opis

Był świetnym wojownikiem, ale nie chce walczyć. Byłaby świetną modelką, ale nie chce tego robić. Ktoś byłby w czymś świetny i może pomóc, ale bardzo nie chce tego robić.

## Opis

Był świetnym wojownikiem, ale nie chce walczyć. Byłaby świetną modelką, ale nie chce tego robić. Ktoś byłby w czymś świetny i może pomóc, ale bardzo nie chce tego robić. W ramach tego motywu pokaż osobę, która jest w stanie pełnić rolę która jest potrzebna, ale bardzo nie chce tego robić. Daj uzasadniony powód czemu ktoś NIE CHCE pełnić tej roli i w ramach motywu graj tym konfliktem między 'nie chcę' a 'to najlepsza opcja'.

## Spoilers
