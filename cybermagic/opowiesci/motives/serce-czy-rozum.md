# Motive
## Metadane

* Nazwa: Serce czy rozum

## Krótki opis

Dylemat - iść za sercem, czy za rozumem? To, czego pragniemy czy to, co rozsądne?

## Opis

Dylemat - iść za sercem, czy za rozumem? To, czego pragniemy czy to, co rozsądne? W ramach tego motywu podkreśl to jako dominujący konflikt i spraw, by nie było oczywiste który wariant jest jednoznacznie lepszy. Podnoś problemy wynikające z braku podjęcia decyzji. Spraw, by ten dylemat został rozwiązany na końcu sesji.

## Spoilers
