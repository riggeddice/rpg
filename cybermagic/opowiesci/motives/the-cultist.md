# Motive
## Metadane

* Nazwa: The Cultist

## Krótki opis

"Jej Świat nadejdzie!" The Cultist uważa się za narzędzie swojej Pani w świecie materialnym. Podporządkował się Jej sprawie bez końca, odrzucając swoje poprzednie życie.

## Opis

### 1. Co reprezentuje

"Jej Świat nadejdzie!" The Cultist uważa się za narzędzie swojej Pani w świecie materialnym. Podporządkował się Jej sprawie bez końca, odrzucając swoje poprzednie życie. Kultysta równie dobrze może być paladynem boga światła, który odrzucił swoje poprzednie życie by zmieniać świat w hołdzie dla swego Pana, jak i mrocznym kultystą odprawiającym krwawy rytuał dla swojej Pani. To, co charakteryzuje The Cultist to oddanie dla Sprawy oraz przekonanie, że jego życie jest mniej warte niż to, co może zrobić.

Jest to postać, która w pełni akceptuje rolę służebnika, kierując się przekonaniem, że każde działanie na rzecz Sprawy ma nieograniczoną wartość, nawet kosztem własnego życia. Dla The Cultist, cel usprawiedliwia środki, a życie osobiste i materialne jest tylko narzędziem w dążeniu do realizacji wyższych idei.

Po prawdzie, nie potrzebujemy nawet sił nadprzyrodzonych. Istnieją 'superfani marki' / ludzie opętani obsesją ideologiczną. Ci ludzie potrafią spełnić agendę The Cultist. Jak długo ich życie jest wtórne wobec Sprawy...

### 2. Kiedy wygrywa

The Cultist nie musi przeżyć by zwyciężyć. Musi jednak sprawić, by Kult stał się potężniejszy, zgodnie z tym co dany Kult uważa za potężniejszy. Jako przykład:

* Gdy Kult jego Pani rośnie, bogaci się w zasoby lub członków.
* Gdy działa w imię Sprawy jego Pani, gdy świat zbliża się do ideału zgodnego ze Sprawą.
* Gdy spełni się określone Proroctwo lub odprawiony będzie trudny Rytuał.
* Gdy jego Pani zamanifestuje się w tym świecie

### 3. Jak działa

Przede wszystkim - The Cultist rzadko działa sam. Zwykle jest wielu kultystów (acz jeden The Cultist, chyba, że to implementacja zbiorowa). Graj dużą ilością członków Kultu albo graj potęgą mocy i wiedzy które posiada The Cultist. The Cultist może mieć dostęp do mocy nadprzyrodzonych i nie zawaha się ich użyć.

The Cultist dodatkowo nie rozpatruje rzeczy takich jak 'muszę to przeżyć' czy 'to niezdrowe'. Niezależnie od wyzwania, idzie w ogień z uśmiechem, bo jego Pani prowadzi jego kroki.

Działania The Cultist mogą wahać się od pokojowych do ekstremalnie agresywnych, w zależności od charakteru Sprawy i aktualnych potrzeb Kultu. 

* działania społeczne, mające na celu pozyskanie sympatii i wsparcia 
* działania skierowane na rozprzestrzenianie doktryny Kultu 
* działania tajne lub otwarcie wrogie, skierowane przeciwko przeciwnikom Sprawy

### 4. Jak wykorzystywać ten motyw

W ramach tego motywu podkreśl przerażające i nieludzkie oddanie fanatyzmu The Cultist. Podkreśl różnicę pomiędzy kulturystami a normalnymi ludźmi. Pokaż, jak adwersarze The Cultist nie zdają sobie sprawę jak daleko The Cultist się posunie lub jak nienaturalnie (z perspektywy zwykłego człowieka) The Cultist myśli. Pokaż głębię wiary The Cultist i jak daleko ów jest skłonny się posunąć. Jeśli to mroczny kult, pokaż bezduszne okrucieństwo The Cultist - sprawa jest wszystkim.

Pokaż atrakcyjność Kultu w świecie, w którym ludzie nie wiedzą co robić. Pokaż, że Kult daje cel istnienia ludziom, którzy pragną tego celu.

I koniecznie pokaż i uzmysłów Graczom co się stanie, jeśli The Cultist wygra.

### 5. Przykłady

* Lorgar Aurelian z W40k: Primarch, który stworzył Kult Imperialny, oddany Imperatorowi ponad posłuszeństwo dla Imperatora. A potem stał się istotą Chaosu. Oczywiście.
* Joseph Seed z "Far Cry 5": Przywódca kultu "Project at Eden's Gate", który wierzy, że został wybrany przez Boga, by uratować ludzi przed grzechem i zbliżającym się końcem świata.

## Spoilers
