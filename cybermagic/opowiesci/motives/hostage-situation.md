# Motive
## Metadane

* Nazwa: Hostage Situation

## Krótki opis

Sytuację utrudnia obecność zakładników, których nie można tak po prostu 'odrzucić' lub poświęcić.

## Opis

Sytuację utrudnia obecność zakładników, których nie można tak po prostu 'odrzucić' lub poświęcić. To sprawia, że typowe strategie nie zadziałają - mamy dylematy moralne i taktyczne, które wynikają z obecności zakładników. W ramach tego motywu pokaż, że jeśli choć trochę nie da się przeciwnikowi tego co chce, przeciwnik zabije lub skrzywdzi zakładników. Wprowadź elementy negocjacji, strategii i manewrów dookoła zakładników, jak i presji na osoby nie będące zakładnikami.

## Spoilers
