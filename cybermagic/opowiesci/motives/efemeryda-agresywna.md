# Motive
## Metadane

* Nazwa: Efemeryda agresywna

## Krótki opis

Pojawił się niestabilny niebezpieczny byt (efemeryda), który stanowi zagrożenie i jest niekompatybilny z normalnym ekosystemem.

## Opis

Pojawił się niestabilny niebezpieczny byt (efemeryda), który stanowi zagrożenie i jest niekompatybilny z normalnym ekosystemem. Ta efemeryda nie jest związana z konkretnymi emocjami, raczej stanowi niszczycielską manifestację nadmiernej energii magicznej. W ramach tego motywu skupiamy się na zniszczeniach wywoływanych przez efemerydę i na powstrzymaniu jej, jak i na reakcji ludzi dookoła na ową efemerydę.

## Spoilers
