# Motive
## Metadane

* Nazwa: Swatanie nieświadomego

## Krótki opis

Swatka chce sprawić, by dwie osoby miały się ku sobie. Oni o tym nie wiedzą.

## Opis

Swatka chce sprawić, by dwie osoby miały się ku sobie. Oni o tym nie wiedzą. W ramach tego motywu graj konspirą, pokaż jak to się składa do kupy i ogólnie pokaż wszystkie machinacje stojące za łączenie tej dwójki w parę.

## Spoilers
