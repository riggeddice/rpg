# Motive
## Metadane

* Nazwa: Forced Transformation

## Krótki opis

Stopniowa transformacja w coś innego, zwykle agresywna, zwykle nieunikniona. Ze świadomością tego czym się stajesz.

## Opis

Transformacja w Terrorforma, zwykle agresywna, zwykle nieunikniona. Ze świadomością tego czym się stajesz. W ramach tego motywu ktoś ulega transformacji, jest przekształcany w coś innego wbrew swojej woli. Przez pewien czas umysł jeszcze zostanie na swoim miejscu, ale to tylko kwestia czasu. Graj absolutnym horrorem tego, co dzieje się z ofiarą i reakcją otoczenia na ofiarę.

## Spoilers
