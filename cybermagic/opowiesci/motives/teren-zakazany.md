# Motive
## Metadane

* Nazwa: Teren, zakazany

## Krótki opis

Czegoś nie wolno na tym terenie robić. Może na niego wchodzić, może czarować. Tak czy inaczej - ten zakaz jest istotny.

## Opis

Czegoś nie wolno na tym terenie robić. Może na niego wchodzić, może czarować. Tak czy inaczej - ten zakaz jest istotny. W ramach tego motywu skupiamy się na tym dlaczego teren jest zakazany i czego nie wolno tam robić, jak i potencjalne reakcje turystów / nielokalsów oraz lokalsów. Odbijaj potrzebę sytuacyjną od zakazu i silnie graj tym co wolno i czego nie wolno.

## Spoilers
