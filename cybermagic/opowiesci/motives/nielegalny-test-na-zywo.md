# Motive
## Metadane

* Nazwa: Nielegalny test na żywo

## Krótki opis

Jakaś frakcja testuje coś, czego nie powinna w warunkach naturalnych, wśród cywili tego nieświadomych.

## Opis

Jakaś frakcja testuje coś, czego nie powinna w warunkach naturalnych, wśród cywili tego nieświadomych. Nawet jeśli wszystko się uda, jest to nieetyczne i nieprawidłowe. Są potencjalne efekty uboczne i problemy. Coś może się wyrwać spod kontroli. Plus - pojawia się _opening_ i może ktoś spróbować przejąć kontrolę nad testowanym wynalazkiem.

## Spoilers
