# Motive
## Metadane

* Nazwa: Przemyt

## Krótki opis

Czy przemycamy coś istotnego, czy przemyt jest tylko w tle, jest czymś istotnym.

## Opis

Czy przemycamy coś istotnego, czy przemyt jest tylko w tle, jest czymś istotnym. Określ, jaką rolę ma przemyt w relacji do postaci. W ramach tego motywu użyj przemytu albo by dostarczyć jednej ze stron coś ważnego (sprzęt, dowody), albo jako konieczność czy niemożność wykonania pewnych akcji.

## Spoilers
