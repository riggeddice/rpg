# Motive
## Metadane

* Nazwa: Rekonstrukcja ruiny

## Krótki opis

Sytuacja jest fatalna, ale stabilna. Nic to, trzeba zakasać rękawy i to naprawić...

## Opis

Sytuacja jest fatalna, ale stabilna. Nic to, trzeba zakasać rękawy i to naprawić... Z jakiegoś powodu jest bardzo źle i jakkolwiek krótkoterminowo nic się całkowicie nie rozsypie, to jednak długoterminowo jest to nieakceptowalne. Ten motyw przedstawia tego typu sytuację oraz postacie które próbują sytuację naprawić, by długoterminowo ‘ruina’ stała się sprawnym bytem.

## Spoilers
