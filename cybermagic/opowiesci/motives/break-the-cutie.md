# Motive
## Metadane

* Nazwa: Break the cutie

## Krótki opis

TRIGGER! To jest sesja, na której doszło do strasznego skrzywdzenia osoby niewinnej i poczciwej.

## Opis

TRIGGER! To jest sesja, na której doszło do strasznego skrzywdzenia osoby niewinnej i poczciwej. Nie jest to motyw który stosuję często, ale czasem ruchy graczy nie zostawiają mi wiele pól decyzyjnych. Ten wątek skupiony jest dookoła złamania niewinnej osoby lub konsekwencji tego że ta osoba została złamana - na graczy i innych Aktorów.

## Spoilers
