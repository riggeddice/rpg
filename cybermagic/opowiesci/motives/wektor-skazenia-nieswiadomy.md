# Motive
## Metadane

* Nazwa: Wektor Skażenia, nieświadomy

## Krótki opis

Skażenie rozprzestrzenia się przez świadome istoty, które nie są świadome tego, że rozprzestrzeniają Skażenie.

## Opis

Skażenie rozprzestrzenia się przez świadome istoty, które nie są świadome tego, że rozprzestrzeniają Skażenie. To może być kwestia tego, że np. to są dzieci, które się bawią i roznoszą Skażenie. To może być ktoś kto jest w epicentrum Skażenia, ale sam nie jest dotknięty objawami Energii. W ramach tego motywu graj tym że nie wiadomo naprawdę jak Skażenie się rozprzestrzenia oraz tym, że pojawia się w miejscach nieoczekiwanych zgodnych z lokalizacją wektora.

## Spoilers
