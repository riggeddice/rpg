# Motive
## Metadane

* Nazwa: Rozmrażanie Królowej Lodu

## Krótki opis

Królowa Lodu. Od dawna niczego nie czuje. A jednak pod wpływem wydarzeń zaczyna się powoli rozmrażać.

## Opis

Królowa Lodu. Od dawna niczego nie czuje. A jednak pod wpływem wydarzeń zaczyna się powoli rozmrażać. Pokaż Królową Lodu i jej zimne podejście, nie zbliżającą się do nikogo. Pokaż też wydarzenia i osoby sprawiające, że Królowa Lodu zaczyna ponownie żyć i działać w świecie żywych. Pokaż rodzące się uczucie (niekoniecznie romantyczne, mentor - mentee też może być).

## Spoilers
