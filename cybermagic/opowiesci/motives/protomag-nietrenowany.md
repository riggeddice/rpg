# Motive
## Metadane

* Nazwa: Protomag, nietrenowany

## Krótki opis

Protomag mniej więcej rozumie co potrafi i co się dzieje, ale nie potrafi kontrolować swojej mocy. Nie dostał jednak szkolenia, ale próbuje używać tego co umie i ma.

## Opis

Protomag mniej więcej rozumie co potrafi i co się dzieje, ale nie potrafi kontrolować swojej mocy. Nie dostał jednak szkolenia, ale próbuje używać tego co umie i ma. W ramach tego motywu skup się na efektach ubocznych utraty kontroli ze strony protomaga, pokaż jak ten brak szkolenia jest niebezpieczny zarówno dla protomaga jak i jego otoczenia. Pokaż zniszczenia, mimo, że protomag NAPRAWDĘ NIE CHCIAŁ.

## Spoilers
