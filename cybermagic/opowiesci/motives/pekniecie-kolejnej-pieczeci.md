# Motive
## Metadane

* Nazwa: Pęknięcie kolejnej pieczęci

## Krótki opis

Istnieje stare zło, coś potężnego i demonicznego co ma ograniczoną moc przez serię pieczęci. Jedna z nich właśnie pęka.

## Opis

Istnieje stare zło, coś potężnego i demonicznego co ma ograniczoną moc przez serię pieczęci. Jedna z nich właśnie pęka. W ramach tego motywu pokaż zmianę wynikającą z pękającej pieczęci, pokaż koszmarność starego zła i pokaż działania dążące do pęknięcia owej pieczęci. Albo gracze albo przeciwnicy są aktywnie zainteresowani ową pieczęcią. Niech gracze zorientują się co prowadzi do pękania pieczęci i niech przyspieszą ten proces - lub go zatrzymają. Zawsze jest to jakaś forma wieloetapowego rytuału.

## Spoilers
