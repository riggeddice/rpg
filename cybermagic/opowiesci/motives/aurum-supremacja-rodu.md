# Motive
## Metadane

* Nazwa: Aurum - supremacja rodu

## Krótki opis

Każdy ród Aurum próbuje być jak najlepszy, największy i najwspanialszy, używając swej unikalności i zasobów.

## Opis

Każdy ród Aurum próbuje być jak najlepszy, największy i najwspanialszy, używając swej unikalności i zasobów. To sprawia, że wiele rodów rywalizuje między sobą i wykonuje dziwne manewry w ukryciu - ukrywając rzeczy przed innymi rodami lub przed pewnymi członkami swojego rodu. Czasem posuwają się za daleko. W tym motywie skupiamy się na używaniu unikalnych cech rodu Aurum by osiągnąć przewagę, której inny ród nie jest w stanie osiągnąć.

## Spoilers
