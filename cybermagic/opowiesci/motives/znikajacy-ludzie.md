# Motive
## Metadane

* Nazwa: Znikający ludzie

## Krótki opis

Na tym terenie zaczęli znikać ludzie. Jest to potencjalnie bardzo niebezpieczny objaw _czegoś_ co znajduje się w cieniu.

## Opis

Na tym terenie zaczęli znikać ludzie. Jest to potencjalnie bardzo niebezpieczny objaw _czegoś_ co znajduje się w cieniu. W ramach tego motywu skup się na tym, że ludzie znikają i nie da się ich znaleźć. Że powinni gdzieś być. Na okolicznościach zniknięć i przyczynie tego że to się dzieje. Podkreśl reakcje otoczenia na znikających ludzi i psychozę "czy ja będę następny".

## Spoilers
