# Motive
## Metadane

* Nazwa: Tożsamość, kwestionowalna

## Krótki opis

Ktoś potencjalnie podaje się za kogoś, kim nie jest. Dużo wskazuje na to, że ktoś się podszywa.

## Opis

Ktoś potencjalnie podaje się za kogoś, kim nie jest. Dużo wskazuje na to, że ktoś się podszywa. Może dekadianin podaje się za savaranina, może mafiozo udaje, że jest terminusem. Może Nikodem Dyzma podszywa się za wpływowego potężnego człowieka. W ramach tego motywu skup się na budowaniu niepewności wobec osoby podszywającej się, ale nie ma nic pewnego.

## Spoilers
