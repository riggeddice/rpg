# Motive
## Metadane

* Nazwa: Test, okrutny

## Krótki opis

"Żeby pokazać lojalność do organizacji, musisz zabić policjanta. Jesteś z nami czy przeciw nam"?

## Opis

"Żeby pokazać lojalność do organizacji, musisz zabić policjanta. Jesteś z nami czy przeciw nam"?

To jest test, który ma doprowadzić do sytuacji bez wyjścia dla drugiej strony. Jeśli to zrobi, nie ma już odwrotu. Zmieniła się sytuacja w sposób nieodwracalny. Ktoś zrobił coś czego nie da się zapomnieć i trudno wybaczyć.

W ramach tego motywu albo postaw Test przed postaciami graczy, albo pokaż jak obecność testu zmienia sytuację dla kogoś innego. Zrób z tego naprawdę trudny dylemat i pokaż okrucieństwo i potworność drugiej strony.

## Spoilers
