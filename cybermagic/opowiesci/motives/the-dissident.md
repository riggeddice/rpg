# Motive
## Metadane

* Nazwa: The Dissident

## Krótki opis

"Nie jestem jednym z Was". The Dissident jest częścią społeczności której nie chce być i próbuje się od niej oderwać.

## Opis

### 1. Co reprezentuje

"Nie jestem jednym z Was". The Dissident jest częścią społeczności której nie chce być i próbuje się od niej oderwać. Czy to przez urodzenie, czy to przez swoją przeszłość jest powiązany z Grupą, ale jest rozczarowany tym czym ta Grupa jest i się stała. Najpewniej kiedyś był silnym punktem tej Grupy, ale dziś - mimo, że jest jej częścią - chce się z niej wyrwać i nie być z nią więcej powiązany. The Dissident nie chce być uzależniony od Grupy i nie chce musieć się jej podporządkować i cierpieć za jej błędy. Chce być wolny.

The Dissident może chcieć Grupę zdradzić dla własnej korzyści (lub bo widzi jej mroczne strony), ale nie jest to konieczne.

### 2. Kiedy wygrywa

The Dissident wygrywa, gdy nieodwracalnie opuszcza grupę na warunkach, na których chce ją opuścić i nie da się go zatrzymać / zmusić do powrotu.

* Może chcieć ukraść odpowiednią ilość zasobów / dóbr i zacząć dobre życie
* Może chcieć po prostu opuścić Kult / Grupę Przestępczą i móc żyć w pokoju, niczego im nie zabierając
* Może chcieć zabrać inne osoby ze sobą i zrobić rozłam w grupie

Tak czy inaczej, The Dissident chce być wolny od swojej pierwotnej grupy.

### 3. Jak działa

Działa z cienia, maskując swoje prawdziwe intencje przed swoją Grupą; wykorzystuje zasoby, środki i wiedzę Grupy by zbudować dla siebie możliwość opuszczenia terenu. Wykorzystuje przeciwników Grupy i fabrykowane przez siebie kryzysy, by odpowiednio odwrócić uwagę Grupy i odpowiednio się pozycjonować / ustawić / móc opuścić to miejsce. Szuka sojuszników i potencjalnie sabotuje Grupę, by być tam gdzie być musi. Chroni swoją tożsamość za wszelką cenę. Często ma dylemat moralny - w Grupie w końcu często byli jego przyjaciele i bliscy.

### 4. Jak wykorzystywać ten motyw

Pokaż okowy Grupy na swoich członków. Owszem, to co robi The Dissident jest moralnie niepoprawne, ale czy taka forma kontroli nad członkami Grupy jest moralna? Czy życie poza Grupą na innych warunkach jest takim grzechem? Kto ma rację - The Dissident czy Grupa?

Skup się na wewnętrznym konflikcie i dylematach moralnych The Dissident. Dominujące uczucie to rozterka dookoła tematów takich jak lojalność, wolność, oraz cena odcięcia się od przeszłości. 

### 5. Przykłady

* Cypher, Matrix: wziął red pill, ale tak naprawdę pragnie wrócić do świata kontrolowanego przez Maszyny i chciał wrócić do Maszyn i zostać bogatym politykiem.
* Mr. Orange, Reservoir Dogs: policjant w ukryciu infiltrujący bandę przestępczą, który chce doprowadzić do ich aresztowania.
* Jason Bourne, z cyklu Ludluma: zabójca pracujący dla organizacji rządowej chce ich opuścić i nie chce ich krzywdzić; chce tylko żyć w spokoju.
