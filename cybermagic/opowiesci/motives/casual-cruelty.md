# Motive
## Metadane

* Nazwa: Casual Cruelty

## Krótki opis

Ktoś wykazuje się okrucieństwem. Nie dlatego, że to konieczne. Dlatego, bo może. I nie dba o to, co się stanie.

## Opis

Ktoś wykazuje się okrucieństwem. Nie dlatego, że to konieczne. Dlatego, bo może. I nie dba o to, co się stanie. To niekoniecznie jest wielkie okrucieństwo; to są rzeczy typu "zrobię to, bo zrobienie czegoś innego byłoby po prostu niewygodne". W ramach tego motywu pokaż właśnie to - pomniejsze "bezinteresowne" akty okrucieństwa, braku wygody itp. Pokaż też, co z tego wynika. Pokaż kogoś, kto jest posunięty o krok za daleko.

## Spoilers
