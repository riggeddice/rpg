# Motive
## Metadane

* Nazwa: Entitled Neighbor

## Krótki opis

Mam basen w domu. Sąsiad nie ma basenu. Wracam do domu i nagle jego dzieci są w moim basenie...

## Opis

Mam basen w domu. Sąsiad nie ma basenu. Wracam do domu i nagle jego dzieci są w moim basenie. Ten motyw skupia się na sąsiadach, którzy uważają że wszystko dookoła powinno należeć do nich zgodnie z zasadą "ExCuSe Me?!", którzy wykorzystują cudze rzeczy i traktują je jak własne. Może sąsiad odkrył coś, czego nie powinien. Może stała się mu krzywda (np. nadepnął na szkło w cudzym domu). Może sprowadził inne osoby którym nie powiedział że to nie jego. W ramach tego motywu graj ogromną roszczeniowością sąsiada i jego całkowicie niezachwianym przekonaniem, że ma prawo do czegokolwiek uzna za słuszne.

## Spoilers
