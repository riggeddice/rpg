# Motive
## Metadane

* Nazwa: Deathwish

## Krótki opis

Ktoś pragnie śmierci. Nie chce więcej żyć. Nie widzi po co i dlaczego miałby dalej żyć. Ale chce zrobić ostatnią rzecz.

## Opis

Ktoś pragnie śmierci. Nie chce więcej żyć. Nie widzi po co i dlaczego miałby dalej żyć. Ale chce zrobić ostatnią rzecz. Ten motyw skupia się na próbie osiągnięcia czegoś przed śmiercią lub na tym, by zginąć w odpowiedni sposób.

## Spoilers
