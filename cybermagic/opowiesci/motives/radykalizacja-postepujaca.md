# Motive
## Metadane

* Nazwa: Radykalizacja, postępująca

## Krótki opis

Czyjeś ideały zostały naruszone. Próbował zrobić to zgodnie z zasadami. Nic to nie dało. Potrzebne są silniejsze środki.

## Opis

Czyjeś ideały zostały naruszone. Próbował zrobić to zgodnie z zasadami. Nic to nie dało. Potrzebne są silniejsze środki.

Ktoś widzi, że normalne metody nie działają. Albo ludzie się przyzwyczaili, albo nie wyrządza dość dużych szkód. Nic się nie zmienia lub jest coraz gorzej. To sprawia, że potrzebne są mocniejsze metody.
W ramach tego motywu pokaż radykalizację normalnych osób. Pokaż co sprawia radykalizację. Nie graj tym jednowymiarowo - obie strony mają rację, choć niekoniecznie podchodzą do tego właściwie. 

## Spoilers
