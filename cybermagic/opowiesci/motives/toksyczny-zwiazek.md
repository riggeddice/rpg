# Motive
## Metadane

* Nazwa: Toksyczny związek

## Krótki opis

On i ona nie są dla siebie dobrzy - niszczą się wzajemnie (lub jedna strona niszczy drugą). A jednak trwają razem, co powoduje straszne kłopoty.

## Opis

On i ona nie są dla siebie dobrzy - niszczą się wzajemnie (lub jedna strona niszczy drugą). A jednak trwają razem, co powoduje straszne kłopoty. W ramach tego motywu podkreślaj toksyczność związku i pokaż jak obie strony działają na siebie niszczycielsko, pokaż jak inni wchodzą z tym związkiem w interakcję i ogólnie jak wszystko idzie w niewłaściwą stronę.

## Spoilers
