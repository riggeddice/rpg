# Motive
## Metadane

* Nazwa: Zdrajcy eksterminacja

## Krótki opis

Ktoś zdradził ważną sprawę. Przez to wszyscy ucierpieli. Zdrajca musi zginąć.

## Opis

Ktoś zdradził ważną sprawę. Przez to wszyscy ucierpieli. Zdrajca musi zginąć.

W ramach tego motywu skup się na tym, dlaczego zdrajca zdradził. Jakie miał dzięki temu korzyści. Czego uniknął i co dostał. A z drugiej strony skup się na tym, kto próbuje zabić zdrajcę. Jak bardzo będzie okrutny? Czy kara jest współmierna do winy? Czy zdrada miała takie konsekwencje by eksterminacja była konieczna? I jaki collateral damage jest akceptowalny?

## Spoilers
