# Motive
## Metadane

* Nazwa: Dyskretny Stalker

## Krótki opis

On lub ona ma stalkera. Stalker robi zdjęcia, monitoruje i nie wiadomo jak daleko się posunie. Stalker jest niewidoczny i wie dużo o swojej ofierze.

## Opis

On lub ona ma stalkera. Stalker robi zdjęcia, monitoruje i nie wiadomo jak daleko się posunie. Stalker jest niewidoczny i wie dużo o swojej ofierze. W ramach tego motywu pokaż, że stalker ma wiedzę przekraczają to, co inni wiedzą o celu. Stalkera bardzo trudno zauważyć lub złapać, jego agenda jest całkowicie nieznana. Stalker ma dostęp do swojej ofiary nawet w miejscach w których by się tego nie dało spodziewać. Podkreślaj paranoję oraz poczucie, że nie wiadomo co się zaraz stanie.

## Spoilers
