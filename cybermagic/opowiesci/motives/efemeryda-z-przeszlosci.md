# Motive
## Metadane

* Nazwa: Efemeryda z przeszłości

## Krótki opis

Coś złego się stało w przeszłości lub ktoś to zrobił. To jest powiązane z silnym sygnałem emocjonalnym. "To" wróciło jako efemeryda.

## Opis

Coś złego się stało w przeszłości lub ktoś to zrobił. To jest powiązane z silnym sygnałem emocjonalnym. "To" wróciło jako efemeryda. W ramach tego motywu skupiamy się na koncepcie 'the past conquers the present', czy 'blast from the past', czyli na interakcji czegoś o czym nikt (może) nie wiedział i nagle wpływa to na stan aktualny. Pojawiają się anomaliczne efekty, ale by pozbyć się problemu trzeba wpierw zrozumieć przeszłość. 

## Spoilers
