# Motive
## Metadane

* Nazwa: Arystokratka, Niesforna

## Krótki opis

Młoda arystokratka, która nie przywykła do tego, że ktoś jej mówi 'nie'. Rozkapryszona, robi co chce, nie patrząc na tych co próbują utrzymać ją przy życiu.

## Opis

Młoda arystokratka, która nie przywykła do tego, że ktoś jej mówi 'nie'. Rozkapryszona, robi co chce, nie patrząc na tych co próbują utrzymać ją przy życiu. Może pakuje się w bardzo niebezpieczne sytuacje, a może specjalnie próbuje zgubić osoby, które próbują ją pilnować. Tak czy inaczej, nasza arystokratka w końcu wpadnie w odpowiednio poważne kłopoty.

## Spoilers
