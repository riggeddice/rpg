# Motive
## Metadane

* Nazwa: The Experimenter

## Krótki opis

"Znajdę to antidotum". The Experimenter próbuje rozwiązać jakiś problem (naukowy / inżynieryjny) i robi mroczne eksperymenty na różnych ofiarach, by w końcu znaleźć to czego szuka.

## Opis

"Znajdę to antidotum". The Experimenter próbuje rozwiązać jakiś problem (naukowy / inżynieryjny) i robi mroczne eksperymenty na różnych ofiarach, by w końcu znaleźć to czego szuka. Skupa się na rozwiązaniu określonego problemu, który może mieć charakter naukowy lub inżynieryjny. Przeprowadza eksperymenty na ludziach nie patrząc na konsekwencje, gdyż cel - wynik udanego eksperymentu - uświęca środki.

W ramach tego motywu skup się na tym, by pokazać konsekwencje działań The Experimenter - z jednej strony znikający ludzie, mroczne eksperymenty, ludzie muszący żyć z tym co im zrobiono a z drugiej strony poparcie - dzięki działaniu The Experimenter jakiejś populacji faktycznie będzie lepiej (np. możnym czy chorym). Można prowadzić ten archetyp jak mrocznego szalonego naukowca samotnika (Wyspa Dr Moreau), można jako ekstremistę, który próbuje zrobić coś dobrego za wszelką cenę.

The Experimenter wygrywa, gdy uda mu się znaleźć rozwiązanie problemu, nad którym pracuje, niezależnie od kosztów moralnych i ludzkich, które poniesie w tym procesie. Jego "sukces" często przybiera formę przełomu naukowego lub technologicznego, ale może być równocześnie tragiczny w skutkach dla tych, którzy padli ofiarą jego eksperymentów.

## Spoilers
