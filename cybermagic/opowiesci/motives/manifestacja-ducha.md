# Motive
## Metadane

* Nazwa: Manifestacja ducha

## Krótki opis

Duch to zapętlone echo kogoś, kto już nie żyje, niezdolny do uczenia się czy ewolucji. Mamy ducha dążącego do swej agendy.

## Opis

Duch to zapętlone echo kogoś, kto już nie żyje, niezdolny do uczenia się czy ewolucji. Mamy ducha dążącego do swej agendy. Określ czym ten duch był i czego pragnął, jak zginął i jaka była ostatnia myśl jaką miał w głowie. Na podstawie tych parametrów w ramach tego motywu pokaż ducha, który rozpaczliwie próbuje wykonać swoje ostatnie myśli, nie zauważając że świat się zmienił. Pokaż tragizm odbicia ducha w Eterze i niemożność zrobienia niczego by duch się czegoś nauczył.

## Spoilers
