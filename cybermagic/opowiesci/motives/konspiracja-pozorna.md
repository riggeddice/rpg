# Motive
## Metadane

* Nazwa: Konspiracja pozorna

## Krótki opis

Wszystko wskazuje na świetnie skoordynowaną akcję mrocznych sił. Nie tym razem. To naturalna słabość systemu albo po prostu pech.

## Opis

Wszystko wskazuje na świetnie skoordynowaną akcję mrocznych sił. Nie tym razem. To naturalna słabość systemu albo po prostu pech. Z perspektywy osób dowodzących wygląda na to że po 2 stronie jest skuteczna konspiracja. W praktyce - seria niezależnych słabych punktów ujawniła się jednocześnie i gdy część słabych punktów zadziałała to następne tak samo zaczęły się odpalać. W ramach tego motywu buduj wyobrażenie potężnego intryganta pociągającego wszystkie sznurki ale pamiętaj że tak naprawdę to jest zwykła _failure cascade_ gdzie jedna słabość się ujawnia zaraz potem uruchamia się następna bo system jest coraz słabszy.

## Spoilers
