# Motive
## Metadane

* Nazwa: Zrzucenie winy na innych

## Krótki opis

Wszystko wskazuje na to, że winna jest Strona XXX. Ale naprawdę za wszystkim stoi zakamuflowana Strona YYY.

## Opis

Wszystko wskazuje na to, że winna jest Strona XXX. Ale naprawdę za wszystkim stoi zakamuflowana Strona YYY. W ramach tego motywu zamaskuj jakieś działania jako działania kogoś innego i pokaż wszelkie działania i implikacje. To zwykle kwestia aktywnego zrzucania winy a nie przypadkowego.

## Spoilers
