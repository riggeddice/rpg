# Motive
## Metadane

* Nazwa: Audyt infiltracyjny

## Krótki opis

Audyt w ukryciu, którego funkcją jest znalezienie niezgodności i problemów, po czym cicha ewakuacja.

## Opis

Audyt w ukryciu, którego funkcją jest znalezienie niezgodności i problemów, po czym cicha ewakuacja. W idealnym świecie, nikt nie jest świadom obecności audytora. W praktyce, wychodzi jak w praktyce. W ramach tego motywu graj dyskretnymi ruchami audytorów, że 'coś jest nie tak', przeszukiwaniem ukrytych problemów i maskowaniem audytora za Prawdziwy Katastrofalny Problem I Zagrożenie.

## Spoilers
