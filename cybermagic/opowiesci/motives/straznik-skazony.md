# Motive
## Metadane

* Nazwa: Strażnik, Skażony

## Krótki opis

Strażnik, ktoś, kto miał chronić i ratować. Skażony, obraca się przeciw temu co ma chronić; stracił duszę.

## Opis

Strażnik, ktoś, kto miał chronić i ratować. Skażony, obraca się przeciw temu co ma chronić; stracił duszę. Czy to terminus danego terenu pokonany przez coś co próbował zniszczyć, czy to policjant skorumpowany przez mafię, czy to naturalny leszy obracający się przeciw lasowi - mówimy o motywie w którym strażnik staje się źródłem problemu. W ramach tego motywu pokaż działania Strażnika oraz jego wpływ na otoczenie.

## Spoilers
