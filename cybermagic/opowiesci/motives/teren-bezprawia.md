# Motive
## Metadane

* Nazwa: Teren bezprawia

## Krótki opis

Niezależnie co się na tym terenie nie stanie, stróże prawa odwrócą oczy. To teren bezprawia, na którym _anything goes_.

## Opis

Niezależnie co się na tym terenie nie stanie, stróże prawa odwrócą oczy. To teren bezprawia, na którym _anything goes_. W ramach tego motywu podkreśl kontrast między terenem bezprawia a każdym normalnym terenem dookoła, umieść coś ważnego dla kogoś na terenie bezprawia i graj maksymalnie tym specyficznym rodzajem terenu.

## Spoilers
