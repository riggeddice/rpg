# Motive
## Metadane

* Nazwa: Energia Alteris

## Krótki opis

Energia Alteris. Rzeczywistość-jest-inna-niż-myślimy. Niemożność poznania. Labirynt pojęć. Nie-rzeczywistość. Korozja znaczeń.

## Opis

Energia Alteris. Rzeczywistość-jest-inna-niż-myślimy. Niemożność poznania. Labirynt pojęć. Nie-rzeczywistość. Korozja znaczeń. W ramach tego motywu pokaż rozpadanie się znaczeń, labirynt pojęć i przekształcającą się rzeczywistość w sposób całkowicie niemożliwy. Jest to anomalia nieuchwytna, walka o zachowanie ZNACZENIA i jakiekolwiek STABILNOŚCI w świetle wszechzmienności i rozpadu samego zrozumienia. W ramach tego motywu graj tym, że rzeczy nie mają sensu, że nic nie jest do końca prawdziwe i brakiem możliwości ufania swoim zmysłom.

## Spoilers
