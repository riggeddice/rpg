# Motive
## Metadane

* Nazwa: Konstrukcja superbroni

## Krótki opis

Ktoś buduje superbroń (lub dedykowaną broń), co jest operacją złożoną i wymagającą działania Rytualnego (seria czynności i substratów).

## Opis

Ktoś buduje superbroń (lub dedykowaną broń), co jest operacją złożoną i wymagającą działania Rytualnego (seria czynności i substratów). W ramach tego motywu skup się na procesie konstrukcji i przyczynach konstrukcji owej superbroni. Co jest potrzebne i jak wygląda ten proces.

## Spoilers
