# Motive
## Metadane

* Nazwa: Manhunt

## Krótki opis

Manhunt. Polowanie na ludzi. Uciekinier ucieka a Łowcy na niego polują.

## Opis

Manhunt. Polowanie na ludzi. Uciekinier ucieka a Łowcy na niego polują. W ramach tego motywu określmy kto i dlaczego jest Uciekinierem oraz kto jest Łowcą. Co ma Uciekinier do dyspozycji i co może zrobić? Jakimi narzędziami dysponuje Łowca? W ramach tego motywu pokaż obie strony - polujących i uciekinierów. Pokaż reakcję ludzi dookoła na to wszystko.

## Spoilers
