# Motive
## Metadane

* Nazwa: Hot potato person

## Krótki opis

Jest pewna osoba i nie wiadomo co z nią zrobić, nikt jej nie chce a nie da się zwolnić. Komu ją wsadzimy? Jaką rolę pełni?

## Opis

Jest pewna osoba i nie wiadomo co z nią zrobić, nikt jej nie chce a nie da się zwolnić. Komu ją wsadzimy? Jaką rolę pełni? Z jakiegoś powodu nasza 'hot potato person' jest uznana za albo bezużyteczną albo politycznie niebezpieczną i nikt nie chce mieć z nią specjalnie do czynienia. Trzeba gdzieś ją wsadzić, komuś umieścić i coś zrobić - ale co i komu? W ramach tego motywu skup się na konieczności wsadzeniu komuś tej problematycznej osoby, na konsekwencjach tego dla tych którym ową osobę wsadzamy oraz na samej osobie. Nie może mieć zbyt wysokiego morale w aktualnej sytuacji. Czemu nikt jej nie chce? Jak bardzo jest tego świadoma? Co z tym robi?

## Spoilers
