# Motive
## Metadane

* Nazwa: Corrupting Mentor

## Krótki opis

Każdy ufa swemu zaufanemu mentorowi. Jednak ten mentor, świadomie lub nie, jest jak Palpatine dla Anakina...

## Opis

Każdy ufa swemu zaufanemu mentorowi. Jednak ten mentor, świadomie lub nie, jest jak Palpatine dla Anakina. Rady mentora zdecydowanie przybliżają osobę do osiągnięcia jej celu, ale kosztem jest dusza / człowieczeństwo. W ramach tego motywu pokaż interakcję między mentorem a mentee; pokaż zmianę w mentee i spraw, by sytuacja nie była jednoznacznie zła.

## Spoilers
