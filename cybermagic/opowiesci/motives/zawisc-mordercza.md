# Motive
## Metadane

* Nazwa: Zawiść, mordercza

## Krótki opis

Balladyna i Alina. Jedna ma wszystko, czego pragnie druga. Doszło do nieuzasadnionego morderstwa, bo to zbyt niesprawiedliwe.

## Opis

Balladyna i Alina. Jedna ma wszystko, czego pragnie druga. Doszło do nieuzasadnionego morderstwa, bo to zbyt niesprawiedliwe. Pokaż zarówno Balladynę jak i Alinę jako osoby wartościowe, które się starają i wiele osiągnęły. Ale Balladyna ma Skazę, której Alina nie ma. W ramach tego motywu graj zbrodnią i karą. Pokaż jak czyn Balladyny zamienił wszystkie jej marzenia w ruinę.

## Spoilers
