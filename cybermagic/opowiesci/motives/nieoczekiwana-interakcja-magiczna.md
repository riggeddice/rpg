# Motive
## Metadane

* Nazwa: Nieoczekiwana interakcja magiczna

## Krótki opis

Interakcja dwóch niezależnych zaklęć lub anomalii doprowadziła do bardzo nietypowego efektu, czegoś, czego nikt się nie spodziewał.

## Opis

Interakcja dwóch niezależnych zaklęć lub anomalii doprowadziła do bardzo nietypowego efektu, czegoś, czego nikt się nie spodziewał. To sprawia, że nikt nie rozumie co tu naprawdę się dzieje. W ramach tego motywu skup się na kaskadzie anomalii, efekcie motyla i konieczności eksploracji co się tu stało i jakie są prawidła działania w tej dziwnej sytuacji.

## Spoilers
