# Motive
## Metadane

* Nazwa: The Revenant

## Krótki opis

"Ty umrzesz przede mną". The Revenant jest istotą, która zginęła lub prawie zginęła i żyje tylko dla zemsty na swoich oprawcach.

## Opis

### 1. Co reprezentuje

"Ty umrzesz przede mną". The Revenant jest istotą, która zginęła lub prawie zginęła i żyje tylko dla zemsty na swoich oprawcach. Najprawdopodobniej wygląda inaczej niż kiedyś, najpewniej ma mnóstwo blizn lub przekształceń po tym co The Revenant się stało. Jedynym celem pozostaje zemsta. Oni mają cierpieć tak, jak The Revenant przed ‘śmiercią’.

### 2. Kiedy wygrywa

The Revenant wygrywa, kiedy wszystkie osoby odpowiedzialne za jego los zaznają takiego samego losu jak on - lub gorszego. Okrucieństwo zemsty The Revenant ma odzwierciedlać głębię doznanych przez niego krzywd. Dla The Revenant, zakończenie jego misji nie oznacza radości czy ulgi, ale możliwość ukojenia w Zapomnieniu.

### 3. Jak działa

The Revenant się nie zatrzymuje, nie zna litości i wobec swojego celu zachowuje się ze skrajnym okrucieństwem. Jego działania są skrupulatnie planowane i mają na celu maksymalizację cierpienia oprawców. Co prawda zwykle unika ranienia niewinnych, jego metody często prowadzą do zniszczeń, które mogą dotykać także osób postronnych. The Revenant ma nieskończoną cierpliwość i absolutną determinację. Był w piekle i je opuścił - nie patrzy na to, co dzieje się dookoła. Tylko zemsta ma znaczenie, niezależnie od kontekstu ofiar.

### 4. Jak wykorzystywać ten motyw

W ramach tego motywu graj kontrastem pomiędzy przeszłością The Revenant i teraźniejszością. Pokaż kim The Revenant mógł się stać, gdyby jego los się nie potoczył tak jak się potoczył. Pozwól graczom odkryć sekret stojący za The Revenant. Pokaż także nieprawdopodobnymi zniszczeniami dokonanymi w ciele i w umyśle The Revenant - niewiele pozostało z osoby, zostaje przede wszystkim zemsta.

Potencjalnie, pokaż zrehabilitowanego / korzystnego eks-złoczyńcę, na którego poluje The Revenant.

### 5. Przykłady

* "Hrabia Monte Christo": Edmond Dantes jest klasycznym przykładem The Revenant w literaturze, który powraca, aby zemścić się na tych, którzy niesprawiedliwie go uwięzili.
* "The Crow": Eric Draven, który powraca z zaświatów, aby pomścić swoją śmierć i śmierć swojej narzeczonej śmierć
* Phantasm, z "Batman: Mask of Phantasm". "Zemsta? Jaki sens ma zemsta?" -> "Jeśli tego nie rozumiesz, nie będę Ci tego wyjaśniać. Żegnaj."
