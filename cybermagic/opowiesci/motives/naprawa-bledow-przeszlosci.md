# Motive
## Metadane

* Nazwa: Naprawa błędów przeszłości

## Krótki opis

Ktoś kiedyś zrobił coś złego z powodu błędu i ktoś inny bardzo ucierpiał. Pojawiła się okazja rehabilitacji, naprawy lub zadośćuczynienia.

## Opis

Ktoś kiedyś zrobił coś złego z powodu błędu i ktoś inny bardzo ucierpiał. Pojawiła się okazja rehabilitacji, naprawy lub zadośćuczynienia. Graj poczuciem winy tego co zrobił błąd. Pokaż jak to zmieniło negatywnie jego życie. Pokaż, jak jest skłonny zrobić wiele by naprawić dawne błędy.

## Spoilers
