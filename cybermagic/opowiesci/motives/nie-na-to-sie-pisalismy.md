# Motive
## Metadane

* Nazwa: Nie na to się pisaliśmy

## Krótki opis

Agent przyszedł zająć się problemem X, ale okazuje się że prawdziwy problem to Y. Y >> X. Agent nie wie jak to ruszyć i nie ma jak się wycofać.

## Opis

Agent przyszedł zająć się problemem X, ale okazuje się że prawdziwy problem to Y. Y >> X. Agent nie wie jak to ruszyć i nie ma jak się wycofać. Najpewniej pojawia się uczucie 'omg w co się wpakowałem', chęć wyplątania się i niekoniecznie chęć do rozwiązania problemu jaki został zakontraktowany. W ramach tego motywu graj konfuzją i niechęcią; pokaż niechęć Agenta do problemu Y i jego zainteresowanie X. Agent najpewniej w ogóle nie ma kompetencji radzenia sobie z Y.

## Spoilers
