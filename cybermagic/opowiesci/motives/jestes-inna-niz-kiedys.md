# Motive
## Metadane

* Nazwa: Jesteś inna niż kiedyś

## Krótki opis

Agentka wróciła na ten teren, po zmianach lub przejściach. Niby jest jak dawniej, ale dawni przyjaciele i sojusznicy nie do końca rozumieją zmiany w Agentce, nie są pewni jej lojalności czy działań. Zmieniła się.

## Opis

Agentka wróciła na ten teren, po zmianach lub przejściach. Niby jest jak dawniej, ale dawni przyjaciele i sojusznicy nie do końca rozumieją zmiany w Agentce, nie są pewni jej lojalności czy działań. Zmieniła się. To powoduje dużą podejrzliwość - zarówno jak chodzi o umiejętności Agentki, jak chodzi o jej motywacje czy działania. Badania i testy? Shadowing? Co się stało Agentce, czemu się zmieniła i co się tam stało w przeszłości? W ramach tego motywu kontrastuj oczekiwania z rzeczywistością, pokaż działanie Agentki jak i reakcję otoczenia. Określ główną różnicę między działaniem Agentki teraz a kiedyś i główną przyczynę podejrzliwości.

## Spoilers
