# Motive
## Metadane

* Nazwa: Anomalia, Memetyczna

## Krótki opis

Anomalia Memetyczna. Jeśli o niej wiesz lub ją postrzegasz, wpływa na Ciebie.

## Opis

Anomalia Memetyczna. Jeśli o niej wiesz lub ją postrzegasz, wpływa na Ciebie. Ten typ anomalii zmienia sposób myślenia i postrzegania swoich ofiar i rozprzestrzenia się przez postrzeganie i poznawanie natury samej anomalii. Bardzo skomplikowany przeciwnik i element prowadzenia.

## Spoilers
