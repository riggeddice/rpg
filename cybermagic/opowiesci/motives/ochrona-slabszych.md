# Motive
## Metadane

* Nazwa: Ochrona słabszych

## Krótki opis

Ktoś ma kogoś pod ochroną i próbuje go obronić, zwykle przed przeważającymi siłami.

## Opis

Ktoś ma kogoś pod ochroną i próbuje go obronić, zwykle przed przeważającymi siłami. Zwykle w ramach tego motywu Chroniący nie jest w stanie poradzić sobie samemu, ale robi co może. Postaw graczy przed konfliktem - pomóc Chroniącemu (wtedy zwykle chroniona jest osoba 'problematyczna') lub pokonać Chroniącego (wtedy zwykle mamy kogoś kto święcie wierzy że robi dobrze ale coś jest nie tak). Ważne, że sam fakt ochrony powoduje pewne niespodziewane konflikty które trzeba wygasić.

## Spoilers
