# Motive
## Metadane

* Nazwa: The Aspirant

## Krótki opis

"Zasługuję na więcej!". The Aspirant uosabia kombatywną merytokrację - najlepszy winien być na szczycie.

## Opis
### 1. Przykładowe warianty

The Aspirant jest skupiony na osiągnięciu odpowiedniego poziomu kompetencji oraz - mając go - na udowodnieniu go i wejściu na kolejny poziom. Przejęcie grupy, organizacji czy dostanie się do elitarnej grupy.

* Doskonały pilot, który pragnie dołączyć do elitarnego oddziału Asów Pilotażu Czarnego Ostrza
* Wojownik sztuk walki, który pragnie stoczyć pojedynek z mistrzem dojo, by zająć jego miejsce
* Artysta na początku kariery, który pragnie pokazać się grupie potencjalnych mecenasów jako osoba świetna i kompetentna
* Jeden z członków zarządu, który ma szansę zostać CEO - i zrobi wszystko by zyskać tą pozycję
* Kultysta, który dołącza do istniejącego kultu oraz pragnie przejąć nad nim kontrolę - inni są niegodni i nie ma w nich wiary
* Gangster, który trafia do więzienia i pragnie zająć odpowiednie miejsce w hierarchii, udawadniając swoją reputację i kompetencję
* Naukowiec dążący do odkrycia przełomowej teorii, która zapewni mu miejsce w encyklopediach.
* Ambitny sportowiec trenujący nieustannie, by zakwalifikować się do drużyny narodowej i wziąć udział w olimpiadzie.
* Wychowanek, który próbuje pokazać Mistrzowi, że nauczył się wszystkiego i teraz on jest mistrzem.

### 2. Co reprezentuje

"Zasługuję na więcej!". The Aspirant uosabia kombatywną merytokrację - najlepszy winien być na szczycie. Symbolizuje nieustępliwą ambicję oraz dążenie do kompetencji i prestiżu związanego z osiągniętym poziomem. The Aspirant zawsze pragnie czegoś na _poziomie społecznym_ - bycie uznanym za godnego _czegoś_, dostanie się do elitarnej grupy lub przejęcie kontroli nad jakąś grupą. Zawsze posiada określone kompetencje by pełnić te role (niekoniecznie POTRAFI to robić, czasem ma zasoby, wpływy lub po prostu jest najlepszą osobą na to miejsce). Nawet, jeśli The Aspirant chcąc coś przejąć może iść na skróty, to jednak The Aspirant zna wartość ciężkiej pracy i z optymizmem podejmuje się wyzwań.

The Aspirant pokazuje, jak mocno można pragnąć osiągnięcia celu i jak daleko jest się gotowym pójść, by ten cel zrealizować.

The Aspirant reprezentowany jest przez następujące aspekty Silnika Konfliktu:

* Cel Społeczny: do czego The Aspirant dąży? Czy chce przejąć organizację, dostać się do grupy, otrzymać tytuł czy uzyskać należny sobie prestiż?
* Przekonanie: w czym The Aspirant jest dobry? Czemu uważa, że może osiągnąć to do czego aspiruje? Co sprawia, że może osiągnąć Cel Społeczny?
* Bariera: co (lub kto) stoi na drodze The Aspirant (nawet, jeśli to tylko prawda w jego oczach)?
* Wyzwanie: jak konkretnie The Aspirant może pokonać Barierę i udowodnić swoją kompetencję osobom kontrolującym Cel Społeczny?

{% image "./the-aspirant.png", "[The Aspirant, ilustracja archetypu (Dall-E)" %}

### 3. Gdzie jest silnik konfliktu

* The Aspirant próbuje osiągnąć Cel Społeczny i rywalizuje z wieloma innymi aspirantami. Jest ograniczona ilość miejsc.
* The Aspirant musi czasami posunąć się bardzo daleko i skrzywdzić ludzi dookoła siebie. Musi poświęcić więcej, niż by chciał.
* The Aspirant musi pokonać Barierę, co często wiąże się z działaniami które niszczą czyjąś reputację lub kogoś mogą upokorzyć.
* The Aspirant przez samą swoją obecność destabilizuje istniejącą tkankę społeczną, co wiąże się z chaosem i reakcją otoczenia.

### 4. Kiedy wygrywa

The Aspirant wygrywa, jeśli osiągnie Cel Społeczny.

* "Tak. Moja kolej": The Aspirant przejmuje kontrolę nad organizacją / trafia do grupy / zostaje uznany mistrzem.
* "Wiem, kiedy będzie mój czas": The Aspirant nie zwyciężył, ale dostał jasną ścieżkę jak się poprawić, by osiągnąć Cel Społeczny.
  * Nie jest dość dobry (jeszcze), są lepsi od niego i wie, kiedy będzie dość dobry. To jest sukces w rozumieniu tej Agendy.
* "Godna próba": The Aspirant wywarł wrażenie na Celu Społecznym; co prawda nie wygrał, ale doprowadził do zmiany, na której mu zależało.

### 5. Kiedy przegrywa

The Aspirant przegrywa, jeśli utraci możliwość osiągnięcia Celu Społecznego lub skończy złamany.

* "Nigdy nie zostanę mistrzem": The Aspirant orientuje się, że nie jest w stanie osiągnąć Celu Społecznego - z przyczyn ekonomicznych, społecznych, kompetencyjnych
* "Oni zawsze byli iluzją...": The Aspirant niszczy Cel Społeczny lub orientuje się, że Cel Społeczny nigdy nie był tym w co The Aspirant wierzył - i jest nienaprawialny
* "Sam zniszczyłem swoją przyszłość": The Aspirant poświęcił za dużo i utracił możliwość osiągnięcia Celu (np. przeciążył ciało i zniszczył możliwość zostania sportowcem)
* "Nie jestem i nie będę godny": The Aspirant został poniżony lub utracił swoje Przekonanie.

### 6. Jak działa

The Aspirant jest potężny i ma przewagi, które pozwolą mu aspirować do osiągnięcia Celu Społecznego - czy jest bardzo kompetentny, czy umocowany, czy ma unikalną wiedzę i umiejętności. Niezależnie, czy działa jawnie czy z ukrycia, skupia się na Wyzwaniu którym chce przebić się przez Barierę. Chce udowodnić innym oraz sobie, że faktycznie jest godny Celu Społecznego i chce zająć swoje zasłużone miejsce na koralowym tronie.

The Aspirant wierzy, że zarówno Cel Społeczny skorzysta, jak i że The Aspirant skorzysta. The Aspirant nie jest przeciwny zreformowaniu organizacji do której dołączy; nie jest zaślepiony.

Do typowych działań The Aspirant będą należały:

* Testowanie Celu Społecznego: czy są tym, do czego warto aspirować? Czy są tam słabsi od niego?
* Testowanie Przekonania: czy tym co posiada jest w stanie osiągnąć Cel Społeczny? Czy on jest dość dobry?
* Ciągłe rozbudowywanie swoich zasobów, możliwości, kompetencji, mentorów i sieci
* Testowanie Bariery i uderzanie w Barierę
* Konstrukcja oraz rozbudowa Wyzwania, zwłaszcza publicznie

### 7. Jak wykorzystywać ten motyw

Skup się na pokazaniu ludzkiej ambicji i determinacji, ale też w kontekście merytokracji - najlepsi winni dowodzić. The Aspirant ma _jakiś_ powód aspirować do tego, do czego aspiruje. Nie chce zostać zwykłym uzurpatorem. Pokaż siłę przekonania The Aspirant oraz pragnienie prawdziwego _należenia_ do czegoś większego. Pokaż też ciemną stronę ambicji i mroczne konsekwencje działań The Aspirant, łącznie z destabilizacją wynikającą z jego działania.

Można też spojrzeć na to, jak ambicja kształtuje życie jednostki, jakie są koszty poświęcenia na rzecz osiągnięcia celu, oraz co dzieje się, gdy cel zostaje osiągnięty lub gdy aspiracje zostają rozbite. I co się dzieje z osobami, które nie były dość dobre.

### 8. Jakie ma ruchy

* 1 - Pokonanie bezpośredniego rywala w uczciwej walce ŻEBY udowodnić swoją przewagę
  * sukces: The Aspirant zyskuje szacunek i rozpoznawalność, umacniając swoją pozycję wśród rywali.
  * przykład: Artysta wygrywa prestiżowy konkurs, pokonując utytułowanych konkurentów, i zdobywa uwagę mecenasów.
* 2 - Publiczna demonstracja kompetencji, Przekonania i dopasowania ŻEBY zbudować poparcie i dowód społeczny
  * sukces: The Aspirant zostaje uznany jako kompetentny i godny pełnienia roli, buduje poparcie społeczne
  * przykład: Pilot demonstruje swoje umiejętności, wystarczające by móc zostać Asem Pilotażu w Elitarnej Grupie
* 3 - Konstrukcja Wyzwania ŻEBY znaleźć słaby punkt Bariery lub uszkodzić Barierę
  * sukces: The Aspirant poznaje słaby punkt Bariery lub ją uszkadza
  * przykład: Kultysta pragnący przejąć Kult tworzy anomalię magiczną, z którą Kult powinien sobie poradzić. Jeśli nie, zainterweniuje.
* 4 - Demonstracja intencji The Aspirant ŻEBY zmusić Cel Społeczny do uznania pozycji i celu The Aspirant
  * sukces: The Aspirant nie może zostać zignorowany; Cel Społeczny musi się odnieść do obecności The Aspirant.
  * przykład: "Wasz plan nie ma prawa zadziałać i proponuję zrobić to lepiej; udowodnię Wam, że mam rację."
* 5 - Konfrontacja z niebezpiecznym wyzwaniem ŻEBY sprawdzić, czy The Aspirant jest dość dobry
  * sukces: The Aspirant udowadnia sobie swoje umiejętności oraz prezentuje Celowi Społecznemu jak jest dobry
  * przykład: Naukowiec przedstawia kontrowersyjną teorię, wywołując debatę, która zwraca na niego uwagę społeczności naukowej.
* 6 - Zdobycie cennego mentora lub sojusznika ŻEBY przyspieszyć rozwój i zdobyć przewagę
  * sukces: The Aspirant zyskuje dostęp do unikalnej wiedzy, zasobów lub wpływów umacniających jego pozycję.
  * przykład: Młody polityk zdobywa poparcie wpływowego polityka, co otwiera mu drogę do szybszego awansu w strukturach.

### 9. Przykłady

* Rocky Balboa z serii "Rocky": początkowo mało znany bokser, aspiruje do zostania mistrzem świata w wadze ciężkiej. Niezachwiana wiara w siebie i mordercze treningi.
* Naruto z serii Naruto: aspiruje do stania się pełnoprawnym członkiem społeczności Konoha Village, zyskać ich szacunek i uznanie.
