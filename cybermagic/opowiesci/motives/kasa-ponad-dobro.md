# Motive
## Metadane

* Nazwa: Kasa ponad dobro

## Krótki opis

"To nieważne, że możemy im pomóc. Stracimy 8% potencjalnych przychodów". Tylko finanse się liczą.

## Opis

"To nieważne, że możemy im pomóc. Stracimy 8% potencjalnych przychodów". Tylko finanse się liczą. W ramach tego motywu graj konfliktem pomiędzy tymi, którzy chcą pomóc i chcą by było dobrze a bezdusznymi mocodawcami, którzy patrzą tylko na 5% zysk. Tu nie chodzi tylko o pieniądze, to mogą być dowolne zasoby.

## Spoilers
