# Motive
## Metadane

* Nazwa: Wychowywanie dzieciaka

## Krótki opis

Jedna ze stron ma ze sobą 'dzieciaka', który nie zachowuje się prawidłowo. Trzeba go skorygować i nauczyć jak dobrze żyć z innymi.

## Opis

Jedna ze stron ma ze sobą 'dzieciaka', który nie zachowuje się prawidłowo. Trzeba go skorygować i nauczyć jak dobrze żyć z innymi. Dzieciak ma swoje zdanie jak należy żyć, nikogo nie słucha i ma wszystko ogólnie gdzieś. Albo to 'bad boy', albo 'prince', albo 'mean girl'... ogólnie, fatalna sprawa. W ramach tego motywu graj złym charakterem dzieciaka i tym, że dzieciak nie jest fundamentalnie zły, tylko nie jest zsocjalizowany. Motyw może mieć alias 'resocjalizacja dzieciaka'.

## Spoilers
