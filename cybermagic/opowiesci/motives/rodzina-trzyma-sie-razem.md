# Motive
## Metadane

* Nazwa: Rodzina trzyma się razem

## Krótki opis

Rodzina jest jaka jest, nie każdego się lubi - ale w sytuacji w której trzeba rodzina staje razem by wesprzeć jednego jej członka.

## Opis

Rodzina jest jaka jest, nie każdego się lubi - ale w sytuacji w której trzeba rodzina staje razem by wesprzeć jednego jej członka. W ramach tego motywu daj możliwość zdobycia wsparcia przez rodzinę członkowi tej rodziny. Daj też możliwość pomocy członkowi rodziny który ma gorsze chwile. Pokaż, że rodzina sobie pomaga, nawet, jeśli nie wszyscy się szczególnie lubią. Na czym polega problem dookoła którego zbiera się rodzina i jak wiele trzeba było zapłacić by rodzinę było stać na pomoc?

## Spoilers
