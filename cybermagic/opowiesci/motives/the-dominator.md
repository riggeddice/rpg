# Motive
## Metadane

* Nazwa: The Dominator

## Krótki opis

The Dominator to istota bezwzględnie przejmująca kontrolę nad swoimi ofiarami, zmieniając je w swoje marionetki lub wyłączając niektóre możliwości ich działania.

## Opis

The Dominator to istota bezwzględnie przejmująca kontrolę nad swoimi ofiarami, zmieniając je w swoje marionetki lub wyłączając niektóre możliwości ich działania. The Dominator pragnie CZEGOŚ używając kontroli i zapewniając sobie tyle ile potrzebuje. W ramach tego motywu wykorzystaj fakt, że ludzie czegoś nie mogą lub muszą robić. Graj tym, że The Dominator może przejąć kontrolę nad różnymi ludźmi i oni nawet nie muszą być tego świadomi.

## Spoilers
