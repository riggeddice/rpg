# Motive
## Metadane

* Nazwa: Miłość ponad życie

## Krótki opis

Kochał brata tak bardzo, że zaryzykowałby życie swoje i swojego zespołu by tylko mieć szansę go uratować...

## Opis

Kochał brata tak bardzo, że zaryzykowałby życie swoje i swojego zespołu by tylko mieć szansę go uratować... Ten motyw skupia się na głębokiej miłości i oddaniu dwóch osób do siebie. Może być to zdrowe, może być to bardzo niszczycielskie. Tak czy inaczej, obecność tak silnego oddania zmienia decyzję i sprawia, że normalnie racjonalne osoby zachowują się w wątpliwy często sposób. W ramach tego motywu pokaż więź pomiędzy dwójką ludzi - romantyczną czy braterską - wraz z wszystkimi implikacjami. Skup się na sytuacjach, w której jednostki mniej sobie oddane mogłyby zawieść, ale te nie zawiodą. Pokaż, jak jeden pójdzie za drugim w piekło, niezależnie od ceny.

## Spoilers
