# Motive
## Metadane

* Nazwa: Grade of Success

## Krótki opis

Grade of Success reprezentuje to, że da się osiągnąć sukces większy lub jeszcze większy. Tu jest nieliniowy przyrost sukcesu sesji na podstawie działań Graczy.

## Opis

Grade of Success reprezentuje to, że da się osiągnąć sukces większy lub jeszcze większy. Tu jest nieliniowy przyrost sukcesu sesji na podstawie działań Graczy. Żeby dobrze wykorzystać ten motyw musisz dać możliwość przerwania sesji w kilku miejscach, gdzie gracze osiągają poszczególne sukcesy. Na przykład - uratowanie silnych ludzi, uratowanie wszystkich ludzi, uratowanie wszystkich ludzi oraz pozyskanie dokumentów. I teraz chcesz by każdy kolejny poziom sukcesu wymagał dużo więcej ryzyk i inwestycji ze strony Graczy. Czyli - o co grają i jak daleko się posuną, z widocznym zwrotem w wypadku sukcesu i bardzo bolesnymi konsekwencjami w wypadku porażki.

## Spoilers
