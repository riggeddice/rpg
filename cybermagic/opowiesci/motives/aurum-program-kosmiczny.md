# Motive
## Metadane

* Nazwa: Aurum - Program kosmiczny

## Krótki opis

Aurum próbuje zdobyć niezależność od Orbitera w kosmosie. Orbiter zwalcza programy kosmiczne Aurum.

## Opis

Aurum próbuje zdobyć niezależność od Orbitera w kosmosie. Orbiter zwalcza programy kosmiczne Aurum. W tym motywie pojawiają się albo działania Aurum mające na celu dać im dostęp do kosmosu niezależnie od Orbitera albo działania Orbitera mające na celu uniemożliwić i sabotować programy Aurum. Na wysokim poziomie obie strony są świadome o działaniach tej drugiej strony, ale żadna ze stron nie może pozwolić sobie na otwartą konfrontację.

## Spoilers
