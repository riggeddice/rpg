# Motive
## Metadane

* Nazwa: Local Team, decent

## Krótki opis

Zespół jest / ma do czynienia z solidną lokalną ekipą. Nic szczególnego, nic wybitnego. Ot, dobra ekipa lokalna.

## Opis

Zespół jest / ma do czynienia z solidną lokalną ekipą. Nic szczególnego, nic wybitnego. Ot, dobra ekipa lokalna. W ramach tego motywu podkreśl **lokalność** tego zespołu. Mają do dyspozycji przyjaciół i wsparcie. Znają doskonale teren. Mają antagonistów oraz umocowanie. Podobnie ważne jest to _kim są_ jak i to _co potrafią_.

## Spoilers
