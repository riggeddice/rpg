# Motive
## Metadane

* Nazwa: Pracownik - naleśnik

## Krótki opis

Czyjś pracownik to totalny naleśnik. Nie wykonuje obowiązków, przeacza rzeczy i przez to coś bardzo poszło nie tak.

## Opis

Czyjś pracownik to totalny naleśnik. Nie wykonuje obowiązków, przeacza rzeczy i przez to coś bardzo poszło nie tak. Przez zaniedbania lub głupie ruchy Naleśnika coś się spieprzyło i Naleśnik próbuje albo to ukryć albo zrzucić winę na kogoś innego. Tak czy inaczej, Naleśnik nie jest najlepszym pracownikiem na świecie, więc Coś Poszło Nie Tak.

## Spoilers
