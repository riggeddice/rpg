# Motive
## Metadane

* Nazwa: Okrutne gry Aurum

## Krótki opis

Arystokraci Aurum na swoim terenie są jak władcy absolutni. Nie jest rzadkością traktowanie ludzi jak pionki.

## Opis

Arystokraci Aurum na swoim terenie są jak władcy absolutni. Nie jest rzadkością traktowanie ludzi jak pionki, nie dbając o ich zdrowie czy stan. Ten wątek pokazuje bezduszność Aurum w kontekście pionków - zwykłych ludzi wykorzystywanych do rozgrywek arystokratów.

## Spoilers

* **Mroczne serce**
    * **Głód**: "Jedynie zabawa ma znaczenie. Ludzie przemieleni przez Grę."
        * bezduszność i okrucieństwo wobec 'pionków' w grze
        * Aurum jako najwyższa wartość, arystokraci Aurum jako ci, którzy się liczą
    * **Ofiara**: człowieczeństwo, autonomia i równość
        * 'pionki' walczą o przetrwanie, tańcząc jak władcy Aurum im zagrają, nie mając szans na lepszy los
        * 'gracze' muszą posuwać się dalej i dalej, odrzucając część człowieczeństwa, by wygrać z 'równymi sobie'
* **Agenda - co chcesz osiągnąć**: 
    * Pokaż desperację 'pionków', hedonizm części 'graczy' i desperację innych 'graczy'.
    * Stwórz interesujące gry i splugaw je cierpieniem
    * 'Tańcz, zaprosili Cię na bal, bierz co chcesz póki zabawa jeszcze trwa'
* **Dark Future - wizja maksymalnej porażki Graczy**:
    * Odbyła się gra i wygrały niewłaściwe osoby
    * Mnóstwo pionków jest rannych, zginęło lub oddano je komuś innemu
    * Część arystokratów ma nowe długi, są podlegli komuś złemu
