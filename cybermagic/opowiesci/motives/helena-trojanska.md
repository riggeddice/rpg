# Motive
## Metadane

* Nazwa: Helena Trojańska

## Krótki opis

"Jej uroda wysłała tysiące okrętów na śmierć". Nie ona to zrobiła, ale dla niej to się stało.

## Opis

"Jej uroda wysłała tysiące okrętów na śmierć". Nie ona to zrobiła, ale dla niej to się stało. Na tej sesji z uwagi na piękno, urodę lub charyzmę jednej z postaci wszystko idzie w cholerę. Ta postać nie jest źródłem zniszczenia, ale zdecydowanie jest przyczyną. Nie musi być tego świadoma, nie musi tego chcieć - ale bez niej to wszystko by się nie stało.

## Spoilers
