# Motive
## Metadane

* Nazwa: Dark Temptation

## Krótki opis

Mroczna pokusa. Demon kusi kogoś, oferując rozwiązanie trudnego problemu. W zamian za coś, czego pragnie.

## Opis

Mroczna pokusa. Demon kusi kogoś, oferując rozwiązanie trudnego problemu. W zamian za coś, czego pragnie. W ramach tego motywu daj Demonowi możliwość rozwiązania poważnego problemu Ofiary, ale za to otrzyma coś czego pragnie. Najprawdopodobniej chodzi o przełamanie Pieczęci lub o zbliżenie Ofiary do kolejnego poziomu Korupcji / Skażenia.

## Spoilers
