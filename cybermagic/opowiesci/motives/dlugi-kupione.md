# Motive
## Metadane

* Nazwa: Długi, kupione

## Krótki opis

Dłużnik ma długi i Nabywca je kupił. Teraz Dłużnik ma przechlapane, bo Nabywca chce albo mieć zwrot z inwestycji albo zmusić Dłużnika do czegoś.

## Opis

Dłużnik ma długi i Nabywca je kupił. Teraz Dłużnik ma przechlapane, bo Nabywca chce albo mieć zwrot z inwestycji albo zmusić Dłużnika do czegoś. W tym motywie warto wyjść od interakcji Dłużnika z Nabywcą, czego Nabywca chce od Dłużnika i głębokiej niechęci Dłużnika do spełnienia żądań Nabywcy.

## Spoilers
