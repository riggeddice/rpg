# Motive
## Metadane

* Nazwa: Waśnie przygraniczne

## Krótki opis

Granice są zawsze nieostre. "Czy to moje czy Twoje". A sąsiedzi się niezwykle spierają.

## Opis

Granice są zawsze nieostre. "Czy to moje czy Twoje". A sąsiedzi się niezwykle spierają. W ramach tego wątku skupiamy się na co najmniej dwóch stronach, które mają mniejsze lub większe prawo do działania w ramach danej operacji, ale zdecydowanie chcą tu być. Waśnie sąsiedzkie, konflikty, walka o zasoby, "bo to po mojej stronie", "bo to od Ciebie przylazło".

## Spoilers
