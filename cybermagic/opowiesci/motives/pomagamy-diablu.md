# Motive
## Metadane

* Nazwa: Na ratunek diabłu

## Krótki opis

Mimo, że diabeł reprezentuje zło które wszyscy chcą wyplenić, tym razem - wbrew sobie - trzeba mu pomóc i go chronić.

## Opis

Mimo, że diabeł reprezentuje zło które wszyscy chcą wyplenić, tym razem - wbrew sobie - trzeba mu pomóc i go chronić. Najważniejsze w tym motywie jest określenie, dlaczego musimy aktywnie pomóc diabłu. W ramach tego motywu daj postacią do dyspozycji nieetyczne środki diabła i jego okrutne zasoby. Pokaż im jak w pewien sposób zdradzają wszystko o co walczyli w przeszłości. Skonfrontuj ich byłymi sojusznikami którzy kwestionują czy teraz aby nie są tacy jak Diabeł. 

## Spoilers
