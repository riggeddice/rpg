# Motive
## Metadane

* Nazwa: Dwie areny jednocześnie

## Krótki opis

Haker i komandosi. Haker ułatwia komandosom wejście, komandosi dają hakerowi dostępy. Co dzieje się w jednej arenie zmienia stan drugiej. 

## Opis

Haker i komandosi. Haker ułatwia komandosom wejście, komandosi dają hakerowi dostępy. Co dzieje się w jednej arenie zmienia stan drugiej. W ramach tego motywu graj zmianami aren, interakcjami pomiędzy arenami i wprowadzaj rzeczy w jednej arenie którą tylko druga arena może rozwiązać.

## Spoilers
