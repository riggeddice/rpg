# Motive
## Metadane

* Nazwa: Energia Exemplis

## Krótki opis

Energia Exemplis. Ochrona przez czystość i intensyfikacja perfekcji. Usuwanie wszystkiego, co nie pasuje, i wzmacnianie tego, co pozostaje. Idealne formy, bez skazy, w blasku białego światła. Zabezpieczenie przez oczyszczenie.

## Opis

Energia Exemplis to siła, która dąży do zachowania i wzmacniania idealnych cech ludzkości. Reprezentuje ochronę, która jest jednocześnie wyzwaniem do osiągnięcia perfekcji. Exemplis nie tylko chroni, ale także wypala wszelkie niedoskonałości, pozostawiając tylko to, co czyste i doskonałe. Energia prowadzi ku doskonałości, ale za cenę utraty wszelkich odchyleń od normy. Exemplis nie toleruje słabości, a jej światło przenika każdą duszę, zmuszając do walki z własnymi niedoskonałościami.

W ramach tego motywu podkreśl transformację w to, co najlepsze - ale kosztem utraty indywidualności i wolności. W tym motywie warto eksplorować ideę, że ochrona przez czystość może prowadzić do stagnacji i braku różnorodności. Wykorzystaj konflikty między dążeniem do ochrony a pragnieniem wolności. Pokaż postacie, które stają przed wyborem: poddać się blaskowi Exemplis i stać się doskonałym, ale ograniczonym bytem, czy walczyć o swoje niedoskonałości i zachować wolność, ryzykując wszystkim innym.

Przykłady wykorzystania na sesji:

* bezpieczeństwo i czystość oferowane przez Exemplis a człowieczeństwo, wolna wola i pozostanie sobą
* ideał ludzkości a niewielkie, wartościowe niedoskonałości sprawiające że jesteśmy ludźmi
* upadek przez Perfekcję - doskonałe, bezduszne byty; exemplarzy a nie osoby

## Spoilers

