# Motive
## Metadane

* Nazwa: Budowa Węzła Energii

## Krótki opis

Ktoś lub coś próbuje dostarczyć odpowiednią ilość energii by skazić dany teren i zmienić go w Węzeł / miejsce anomalne.

## Opis

Ktoś lub coś próbuje dostarczyć odpowiednią ilość energii by skazić dany teren i zmienić go w Węzeł / miejsce anomalne. Najprostszym sposobem jest przez dostarczenie odpowiedniej ilości krwi i uczuć. Mogą zadziałać też różnego rodzaju rytuały albo akumulacja artefaktów. W ramach tego motywu graj tym że ktoś konstruuje odpowiedni węzeł i przygotowuje teren pod _coś_. Pokaż jak zmieniane miejsce się przekształca. Pokaż postęp konstruowanego węzła i początki anomalnych działań.

## Spoilers
