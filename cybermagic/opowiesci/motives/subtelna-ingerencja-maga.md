# Motive
## Metadane

* Nazwa: Subtelna ingerencja maga

## Krótki opis

Na pierwszy rzut oka, nie ma tu magów. W praktyce, było jedno subtelne zaklęcie.

## Opis

Na pierwszy rzut oka, nie ma tu magów. W praktyce, było jedno subtelne zaklęcie które całkowicie zmieniło sytuację. Ten motyw jest wykorzystywany by coś 'nieprawdopodobnego' jednak się zdarzyło, czy właściwa osoba podejmie 'odpowiednią' akcję. Seria niewielkich zbiegów okoliczności, które jednak mają za sobą maga. W ramach tego motywu nie ma wielu magów podczas opowieści; dlatego ten mag działający z cienia był tak skuteczny.

## Spoilers
