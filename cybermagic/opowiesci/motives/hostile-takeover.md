# Motive
## Metadane

* Nazwa: Hostile Takeover

## Krótki opis

Organizacja należy do Dawnego Szefa. W sposób polityczny, ekonomiczny lub militarny Nowy Szef przejął nad nią kontrolę i zaprowadza swoje porządki.

## Opis

Organizacja należy do Dawnego Szefa. W sposób polityczny, ekonomiczny lub militarny Nowy Szef przejął nad nią kontrolę i zaprowadza swoje porządki. Najważniejszym elementem tego motywu jest akt Hostile Takeover - jak to się stało, że Dawny Szef stracił władzę? Co z tego wynika? Co robi Dawny Szef a jakie porządki zaprowadza Nowy Szef? Czy wspierać czy powstrzymać? Jak się organizacja podzieliła na kawałki?

## Spoilers
