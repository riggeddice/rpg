# Motive
## Metadane

* Nazwa: The Pastmaster

## Krótki opis

"The Past shall conquer the Present". The Pastmaster z jakiegoś powodu nie akceptuje świata i czasu dzisiejszego. Próbuje doprowadzić do świata który był kiedyś.

## Opis

"The Past shall conquer the Present". The Pastmaster z jakiegoś powodu nie akceptuje świata i czasu dzisiejszego. Próbuje doprowadzić do świata który był kiedyś. Czy to rekonstruktor pragnący odtworzyć konkretne wydarzenia z przeszłości, czy facet który porwał dziecko udając że to jego dawno zmarły syn czy anomalia magiczna sprawiająca że ludzie przeżywają po raz kolejny to co się działo – The Pastmaster jest obsesyjnie skupiony na przeszłości i na tym co działo się kiedyś. Najprawdopodobniej jest bytem magicznym albo niestabilnym psychicznie. W swoim celu jest dość monotoniczny i może pomylić niektóre osoby z ich „wersjami z przeszłości”.

W ramach tego motywu podkreślaj brak nowych myśli ze strony The Pastmaster. Tak, wykonanie może być bardzo innowacyjne, ale wydarzenia muszą być odtworzone dokładnie tak jak miały miejsce. Nie pojawią się nowe koncepty czy nowe istoty. Celem jest przywrócenie przeszłości kosztem teraźniejszości.

The Pastmaster wygrywa, jeżeli uda mu się sprawić, że wszyscy grają w jego grę i sytuacja potoczy się tak jak potoczyła się kiedyś. Jeśli teraźniejszość stanie się cieniem przeszłości.
