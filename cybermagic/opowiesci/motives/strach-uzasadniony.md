# Motive
## Metadane

* Nazwa: Strach, uzasadniony

## Krótki opis

Ktoś chce lub musi coś zrobić, ale jest to absolutnie przerażające. Nie jest w stanie się przez to przebić i tego zrobić.

## Opis

Ktoś chce lub musi coś zrobić, ale jest to absolutnie przerażające. Nie jest w stanie się przez to przebić i tego zrobić. Jest coś, co gra w duszy nieszczęśnika i nieszczęśnik nie jest w stanie przez to przejść. Coś, co trzeba zrobić i jest po prostu zbyt straszne. Może to konieczność zmierzenia się ze swoimi terrorami, może to paniczny strach przed walką z kralothem i ryzykiem zostania przekształconym a może strach przed tym co może się z Tobą stać jeśli Dotknie Cię jeden z bogów. W ramach tego motywu pokaż ten strach i jak on wpływa na osobę dotkniętą, pokaż też jak to demoralizuje wszystkich dookoła.

## Spoilers
