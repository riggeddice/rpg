# Motive
## Metadane

* Nazwa: Rywal z przeszłości

## Krótki opis

Ktoś ma Rywala, kogoś z kim rywalizował poważnie w przeszłości. I Rywal właśnie powrócił.

## Opis

Ktoś ma Rywala, kogoś z kim rywalizował poważnie w przeszłości. I Rywal właśnie powrócił. Rywal może być sojusznikiem (szukającym pomocy _best of the best_), może chcieć wrócić do rywalizacji a może się pojawił i jego obecność sama jest katalizatorem dawnych wydarzenń. W ramach tego motywu graj przeszłością i relacjami między rywalami; ich 'frenemy' status i tym, że jakkolwiek rywalizacja się skończyła, ale częściowo 'nadal' w tym zostali.

## Spoilers
