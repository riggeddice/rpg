# Motive
## Metadane

* Nazwa: Miłość, zakazana

## Krótki opis

On kocha ją a ona jego. Ale nigdy nie mogą być razem! Jak Romeo i Julia, ciągną do siebie wbrew rodzinom...

## Opis

On kocha ją a ona jego. Ale nigdy nie mogą być razem! Jak Romeo i Julia, ciągną do siebie wbrew rodzinom... Ten motyw skupia się na dwóch kochankach, którzy pragną być razem ale nie mogą - na ich działaniach by być razem i na reakcji rzeczywistości na ich próby. W ramach tego motywu graj tym, jak to wszystko się destabilizuje przez ich działania i próby powstrzymania miłości (lub konsumpcji miłości).

## Spoilers
