# Motive
## Metadane

* Nazwa: Jestem ponad prawem

## Krótki opis

Ktoś pełni rolę 'arystokracji'. Robi rzeczy nieakceptowalne i szkodliwe, ale jest chroniony, prawo tej osoby nie dotyczy. Ta osoba uważa się za 'lepszą' - nie musi się dostosować.

## Opis

Ktoś pełni rolę 'arystokracji'. Robi rzeczy nieakceptowalne i szkodliwe, ale jest chroniony, prawo tej osoby nie dotyczy. Ta osoba uważa się za 'lepszą' - nie musi się dostosować. W wyniku tego im 'wolno' robić rzeczy których nikt inny robić nie może - "jeździ BMW 300 km/h i nie ma nagrań z kamer", "jego przeciwnicy znikają". Pokaż w jaki sposób robi to degenerację społeczną. Pokaż, jak reagują na to inni. Pokaż konsekwencje tego dla wszystkiego.

## Spoilers
