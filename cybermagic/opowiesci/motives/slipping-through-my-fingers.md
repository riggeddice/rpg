# Motive
## Metadane

* Nazwa: Slipping Through My Fingers

## Krótki opis

Wszystko wydawało się pod kontrolą. Wszystko wydawało się znane i opracowane. A jednak... wszystko się rozpada. Nie udało się. Nic nie jest wiadome. I wszystko idzie w jedną stronę - utratę reszty.

## Opis

Wszystko wydawało się pod kontrolą. Wszystko wydawało się znane i opracowane. A jednak... wszystko się rozpada. Nie udało się. Nic nie jest wiadome. I wszystko idzie w jedną stronę - utratę reszty. W tym motywie występuje ktoś kto kontrolował sytuację i o wszystkim wiedział (np. Gul Dukat) i w chwili, w której wszystko powinno działać, wszystko mu się rozsypało. Okazuje się, że czegoś nie wiedział, że coś jest niezgodne z jego planem. Najprawdopodobniej rozpaczliwie próbuje uratować co się da i jak się da. Może być też powiązane z Heroic BSOD lub Villanous BSOD.

W ramach tego motywu graj tym, że osoby znające taktyka nie wiedzą co robić. Taktyk nigdy nie zachowywał się w taki sposób. Jest zrozpaczony, załamany i nie wie co się dzieje. I jeśli nic nie zrobi, przegra wszystko. Ale nie wie co robić.

## Spoilers
