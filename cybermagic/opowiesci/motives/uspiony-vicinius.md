# Motive
## Metadane

* Nazwa: Uśpiony Vicinius

## Krótki opis

Man-Bat. Blink. Ktoś jest viciniusem, ale nie wie o tym lub ukrywa drugą naturę. Działa jako człowiek w dzień, vicinius w nocy.

## Opis

Man-Bat. Blink. Ktoś jest viciniusem, ale nie wie o tym lub ukrywa drugą naturę. Działa jako człowiek w dzień, vicinius w nocy. W ramach tego motywu jesteś w stanie terroryzować używając dwóch form śmiertelnie niebezpiecznej istoty - pozyskuje wiedzę w dzień, działa w nocy. Coś sprawiło, że vicinius się uaktywnił. Vicinius ma swoją agendę, niekoniecznie tą samą co oryginalny 'nosiciel'.

## Spoilers
