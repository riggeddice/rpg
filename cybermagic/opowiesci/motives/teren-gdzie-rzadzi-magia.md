# Motive
## Metadane

* Nazwa: Teren, gdzie rządzi magia

## Krótki opis

Ten teren jest niezwykle niebezpieczny, obcy i niestabilny. Magia w ogromnej intensywności zmiażdży każdego.

## Opis

Ten teren jest niezwykle niebezpieczny, obcy i niestabilny. Magia w ogromnej intensywności zmiażdży każdego. Pacyfika, Sanktuarium Kazitan, Szczelina. W ramach tego motywu zaznacz jak niezwykle silny jest ten teren i pokaż postaciom, że to nie jest miejsce dla nich. Skonfrontuj ich z konsekwencjami podniesionej energii magicznej i pokaż co spotyka tych, którzy nie szanują anomaliczności tego terenu.

## Spoilers
