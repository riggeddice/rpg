# Motive
## Metadane

* Nazwa: The Man in Black

## Krótki opis

"Niczego tu nie ma". The MiB próbuje zamaskować i ukryć prawdę o czymś, nieważne jak daleko się nie musi posunąć.

## Opis

"Niczego tu nie ma". The MiB próbuje zamaskować i ukryć prawdę o czymś, nieważne jak daleko się nie musi posunąć. The MiB skupia się na tym, by prawda nie wyszła na jaw. Nikt nie może się o CZYMŚ dowiedzieć, niezależnie w jaki sposób nie zostanie to osiągnięte. Zwykle ma do dyspozycji siły Agencji, wsparcie, reputację oraz sporo środków. Usuwa ślady, podkłada fałszywe tropy, racjonalizuje itp.

W ramach tego motywu pomyśl dlaczego The MiB próbuje ukryć prawdę i sprawić by nigdy nie wyszła na jaw. Czego się boi. Przed czym chroni. Dlaczego to robi. Kto mu stoi na drodze i co się stanie jak prawda wyjdzie na jaw. To niekoniecznie będzie dobre / złe dla społeczności - ale jest powód czemu MiB robi co robi.

The MiB wygrywa, jeśli prawda nie wyjdzie na jaw, wszystkie ślady zostaną usunięte i nie uda się już nigdy poznać prawdy.

## Spoilers
