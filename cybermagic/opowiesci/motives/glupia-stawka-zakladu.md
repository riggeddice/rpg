# Motive
## Metadane

* Nazwa: Głupia stawka zakładu

## Krótki opis

Ktoś zrobił zakład. Przegrał coś, czego nie powinien. Teraz trzeba to naprawić lub ukryć przed bliskimi. 

## Opis

Ktoś zrobił zakład. Przegrał coś, czego nie powinien. Teraz trzeba to naprawić lub ukryć przed bliskimi. W ramach tego motywu pokaż, jak hazard / zakłady są problematyczne, jak ktoś postawił coś czego nie powinien pod wpływem chwili i jak wszystko idiotycznie kaskadowało.

## Spoilers
