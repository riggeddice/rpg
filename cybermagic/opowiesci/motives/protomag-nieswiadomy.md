# Motive
## Metadane

* Nazwa: Protomag, nieświadomy

## Krótki opis

Dookoła tej osoby dzieją się dziwne rzeczy. Jest protomagiem, ale nie ma o tym pojęcia i nie wie jak z tym sobie radzić.

## Opis

Dookoła tej osoby dzieją się dziwne rzeczy. Jest protomagiem, ale nie ma o tym pojęcia i nie wie jak z tym sobie radzić. Jako że magia odpowiada na bardzo silne emocje, moc protomaga będzie manifestować się w chwilach wielkiej radości lub absolutnego strachu. W tym motywie graj dziwnymi rzeczami zdarzającymi się niedaleko postaci, konfuzją i zaskoczeniem oraz tym, że protomag naprawdę nie kontroluje swoich umiejętności.

## Spoilers
