# Motive
## Metadane

* Nazwa: The Devastator

## Krótki opis

"Nic nie pokona Dewastatora". Nieprawdopodobnie potężna i praktycznie niepokonana istota skupiająca się na destrukcji. Albo konkretnej rzeczy, albo w sposób niekierowany.

## Opis

"Nic nie pokona Dewastatora". Nieprawdopodobnie potężna i praktycznie niepokonana istota skupiająca się na destrukcji. Albo konkretnej rzeczy, albo w sposób niekierowany. The Devastator jest synonimem katastrofy i zagłady, z dużą ilością 'accidental casualties' i destrukcji dookoła. The Devastator działa brutalnie, nie zważając na efekty uboczne czy ofiary dodatkowe. Jego metody są brutalne i bezpośrednie, często obejmujące masowe zniszczenia.

Charakterystyczne dla The Devastator jest to, że się nie zatrzymuje - idzie za celem do końca. Ale The Devastator zwykle posiada jakiś słaby punkt i po znalezieniu go da się go odgonić / przesunąć / pokonać.

W ramach tego motywu graj The Devastator jak siłą natury - nie da się z nim walczyć (przynajmniej nie od razu), da się jedynie ograniczyć skutki jego działań. Pokaż niszczycielski wpływ The Devastator na otoczenie. Pokaż, jak ludzie cierpią przez jego obecność. Pokaż ogólną beznadziejność, niskie morale i koszty radzenia sobie po przejściu The Devastator.

## Spoilers
