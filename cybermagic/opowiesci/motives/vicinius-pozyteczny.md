# Motive
## Metadane

* Nazwa: Vicinius, pożyteczny.

## Krótki opis

Pożyteczna istota magiczna, która pomaga ludziom w konkretnej niszy. Np. glukszwajn lub pacyfikator.

## Opis

Pożyteczna istota magiczna, która pomaga ludziom w konkretnej niszy. Np. glukszwajn lub pacyfikator. W ramach tego motywu pokaż albo w jaki sposób wygląda hodowla pożytecznych viciniusów albo graj ich przydatnością dla społeczeństwa. Być może efekty uboczne takiego viciniusa są wyjątkowo niekorzystne dla kogoś, być może ktoś bardzo nie chce mieć tego typu viciniusów w pobliżu.

## Spoilers
