# Motive
## Metadane

* Nazwa: Aurum - waśń wewnątrzrodowa

## Krótki opis

W ramach tego samego rodu Aurum różne podfrakcje i interesy o coś walczą między sobą.

## Opis

W ramach tego samego rodu Aurum różne podfrakcje i interesy o coś walczą między sobą. W tym motywie występują co najmniej dwie frakcje lub osoby z tego samego rodu, próbujące osiągnąć zupełnie inne rzeczy i przez to zwalczające się przez proxy albo bezpośrednio. Waśń i działanie mogą być nieproporcjonalnie wysokie albo adekwatne. Ważnym jest to, że w wyniku tego typu waśni nie dochodzi do trwałego ucierpienia żadnego maga rodu.

## Spoilers
