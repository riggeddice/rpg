# Motive
## Metadane

* Nazwa: The Facade

## Krótki opis

"Jestem perfekcyjny". The Facade to osoba skupiona na wizerunku i byciu odpowiednio postrzeganą ponad rzeczywistość.

## Opis

### 1. Przykładowe warianty

Mamy różne sposoby "udawania rzeczywistości", zwłaszcza w kontekście EZ.

* Zubożały arystokrata, który dalej wydaje pieniądze jakby był bogaty i potężny, nie akceptując że już nie może tak żyć.
* Czarodziejka, która używa magii by wszyscy (łącznie z nią) myśleli, że jej rodzina - która zginęła w pożarze - nadal jest z nią.
* Wojownik lub terminus, który nie jest już zdolny do walki, ale udaje, że nadal jest niepokonaną maszyną zniszczenia.
* Konsultant, który buduje w mediach społecznościowych obraz mistrza w swym fachu, ale naprawdę nie dowozi i próbuje uciec przed katastrofą.
* Uczeń w szkole, który buduje wśród rodziny obraz świetnego ucznia, a tak naprawdę ściąga na lewo i prawo i nic nie umie?
* Magnat kandydujący na prezydenta miasta, który zmusza swoją rodzinę do zachowywania się jak perfekcyjna rodzina.

### 2. Co reprezentuje

"Jestem perfekcyjny". The Facade to osoba skupiona na wizerunku i byciu odpowiednio postrzeganą ponad rzeczywistość. The Facade reprezentuje pragnienie utrzymania wizerunku (i potencjalnie prestiżu) za wszelką cenę. Rzeczywistość jest wtórna do tego jak jesteśmy postrzegani.

The Facade reprezentuje ucieczkę od prawdy, zagłębiając się coraz bardziej w labirynt kłamstw i iluzji. W niektórych wariantach The Facade jest świadom iluzji, w niektórych nie - ale kluczem dla The Facade jest zbudowanie określonego obrazu i utrzymanie go przed ważnymi dla siebie osobami (lub ogólną populacją).

The Facade reprezentowany jest przez następujące aspekty Silnika Konfliktu:

* Wizerunek: czym The Facade pragnie być i jaki obraz pragnie pokazać światu?
* Rzeczywistość: jak jest naprawdę? Kim jest The Facade i jaki jest jego kontekst?
* Ruina: co sprawiło, że The Facade utracił Rzeczywistość i jest mniejszy niż jego Wizerunek? Może rodzina zbankrutowała, a może pragnie, by rodzice myśleli że jest najlepszym uczniem w klasie?
* Target: przed kim The Facade chce utrzymać Wizerunek? Przed wszystkimi, czy tylko przed konkretną grupą ludzi?
* Rozpacz: jak daleko The Facade się posunie, by ukryć przed Targetem prawdę? Co zrobi?

### 3. Gdzie jest silnik konfliktu?

The Facade żyje w dwóch światach - bolesnej rzeczywistości i pięknej iluzji. Gdy te światy wchodzą w kolizję, The Facade staje się niebezpieczny i iluzja zwycięża z rzeczywistością.

* The Facade działa, jakby Wizerunek był rzeczywistością i aktywnie wymusza, by świat działał jakby Wizerunek był prawdą. A ludzie mają swoje agendy.
* The Facade aktywnie i dyskretnie próbuje odwrócić lub zamaskować Ruinę, zwalczając wszystko co pokazuje prawdę.
* The Facade żyje dla Wizerunku; potrafi podejmować (błędne) decyzje jakby Wizerunek był prawdziwy i pociągnąć przyjaciół na samo dno.
* Im bardziej przepaść między Wizerunkiem i Rzeczywistością się rozwiera, tym bardziej The Facade eskaluje swoje zachowania, nie akceptując prawdy.
* Każdy, kto próbuje doprowadzić do pokazania Rzeczywistości The Facade lub otoczeniu staje się śmiertelnym wrogiem The Facade.

### 4. Kiedy wygrywa

Zwycięstwo The Facade jest powiązane z przekształceniem Wizerunku w Rzeczywistość - odwrócenie Ruiny i życie w świecie iluzji.

* "To tylko tymczasowe kłopoty": The Facade odbudował dość Zasobów, by Rzeczywistość zbliżyła się do Wizerunku.
* "Zawsze byłem najlepszy": The Facade tak bardzo rozbudował Wizerunek i matnię iluzji, że niezależnie od Rzeczywistości wszyscy w to wierzą i wierzyć nie przestaną.

### 5. Kiedy przegrywa

Porażka The Facade jest powiązana z utratą Wizerunku lub całkowitym zrujnowaniem przez Ruinę.

* "To nie jest prawda": Target poznaje prawdę o The Facade oraz The Facade spada z piedestału i jego Wizerunek się rozpada.
* "Masz rację. Kłamałem.": The Facade musi uzmysłowić sobie, że Wizerunek nie jest prawdziwy. Musi zaakceptować Rzeczywistość.
* "Mylisz się! JESTEM PERFEKCJĄ!": The Facade jest ostatnią osobą wierzącą w Wizerunek. "Żyje w swoim świecie", oderwany od innych.
* "To koniec...": Ruina zabrała The Facade wszystko. Wizerunek jest niemożliwy do utrzymania i The Facade bankrutuje.

### 6. Jak działa

The Facade działa głównie poprzez manipulację, tworzenie iluzji i omijanie prawdy. Buduje iluzje na iluzjach, udaje, że rzeczywistość jest czymś innym niż jest naprawdę i potrafi posunąć się niezwykle daleko by ukryć Ruinę i sprawić, by Target wierzył w Wizerunek. The Facade żyje w przeświadczeniu, że jest w stanie doprowadzić Rzeczywistość do Wizerunku - lub że może czerpać korzyści z Wizerunku by móc się szybko ewakuować jak będzie trzeba.

The Facade jest osobą bardzo szczegółową i precyzyjną. Typowe działania:

* Stosowanie magii, technologii czy manipulacji społecznej do kreowania fałszywego wizerunku.
* Manipulowanie informacjami i ludźmi wokół siebie, aby utrzymać pozory.
* Wykorzystywanie swoich zasobów i umiejętności w celu zabezpieczenia swojej tajemnicy przed światem.

The Facade działa nie tylko samemu, ale też przez agentów. Kluczem jest to, że trudno określić _co jest prawdziwe_ a _w co wierzą inni_. Mistrz manipulacji i gaslightingu, The Facade potrafi budować dziesiątki fałszywych, spójnych opowieści i plotek.

### 7. Jak wykorzystywać ten motyw

Pokaż, jak bardzo niszczycielskie mogą być sieci społeczne i oczekiwania ludzi w obliczu The Facade. Pokaż, z jakim kosztem powiązane jest życie w iluzji i w kłamstwie. Pokaż, że The Facade jest też postacią tragiczną - przez odrzucenie prawdy musi niszczyć i nie jest zdolny do osiągnięcia prawdziwego szczęścia czy spełnienia. 

Pokaż też, jak bardzo nierozsądne jest wierzenie The Facade i sieciom społecznym bez spojrzenia na dowody, fakty i "sprawdzam".

### 8. Jakie ma ruchy

Ruchy The Facade są skierowane dookoła budowy i utrzymywania Wizerunku, ale też dookoła manifestacji swojej potęgi do niszczenia tych którzy stoją na drodze Wizerunku.

* 1 - Wykorzystanie Wizerunku, prestiżu i zasobów do zniszczenia przeciwnika ŻEBY wzmocnić Wizerunek oraz osłabić przeciwnika
  * sukces: przeciwnik zostaje poważnie uszkodzony fizycznie, politycznie lub społeczny a Wizerunek zostaje wzmocniony
  * przykład: poinformowanie ludzi, którzy ufają The Facade, że przeciwnik sprawia mu ogromną przykrość. "Czy naprawdę nic nie uwolni mnie od tego przeklętego XXX?"
* 2 - Manipulowanie ludźmi by pozyskać Zasoby ŻEBY móc udowodnić Wizerunek i oddalić Ruinę
  * sukces: część ludzi jeszcze bardziej wierzy w Wizerunek oraz The Facade dostaje dostęp do Zasobów
  * przykład: przekonanie inwestorów, że nieistniejący projekt się opłaci i zdobycie dofinansowania; teraz inwestorom też zależy
* 3 - Demonstracja Wizerunku i możliwości ŻEBY zdemoralizować przeciwników
  * sukces: uderzenie w morale i pewność siebie tych, którzy w Wizerunek nie wierzą
  * przykład: wojownik który nie może już walczyć używa swojej reputacji do zastraszenia potencjalnych rywali - zwłaszcza że podopieczni Wojownika "pozwól mi, mistrzu, nie brudź rąk!"
* 4 - Konwersja Zasobów w Wizerunek ŻEBY zbliżyć Rzeczywistość do Wizerunku
  * sukces: dzięki pewnym wydatkom, Rzeczywistość The Facade staje się bliższa Wizerunkowi.
  * przykład: Konsultant wykorzystuje płatne promocje w mediach społecznościowych, podbija liczbę obserwujących, wzmacnia iluzję eksperckości.
* 5 - Bezwzględne uderzenie w przeciwnika ŻEBY usunąć prawdę i uratować Wizerunek
  * sukces: przeciwnik jest złamany, nieprzekonani są w szoku, prawda nie zostaje ujawniona.
  * przykład: Uczeń, który ściąga, sabotuje projekt grupowy - "to ich wina", "nawet mój geniusz nie mógł pomóc, oni ściągali i nic nie zrobili"
* 6 - Rozpuszczenie plotek i kłamstw ŻEBY zamaskować prawdę i rozbić ruchy przeciwników
  * sukces: jeszcze trudniej dojść do tego co jest prawdą a co fikcją, przeciwnicy nie wiedzą jak zaatakować The Facade.
  * przykład: zlecanie trollom fabrykację kłamstw i puszczenie plotek, by zdyskredytować rywala
* 7 - Pozyskanie sojuszników przekupstwem, groźbą lub majestatem ŻEBY poszerzać wpływy
  * sukces: większe siły i wpływy polityczne; więcej Zasobów w rękach The Facade
  * przykład: "Masz okazję współpracować ze mną. Wiesz kim jestem, z kim współpracuję i co potrafię. Możesz też stać przeciwko mnie."
* 8 - Demonstracja konsekwencji walki z Wizerunkiem i korzyści ze współpracy ŻEBY zneutralizować przeciwnika
  * sukces: przeciwnik dołącza do The Facade, czerpiąc korzyści ze współpracy (lub ze strachu)
  * przykład: publiczne zniszczenie pomniejszego agenta przeciwnika wraz z wyciągniętą ręką do przeciwnika - "działajmy razem"
* 9 - Używanie sekretów, wiedzy o innych czy poszerzonych uprawnień ŻEBY zyskać przewagę lub obronić swoją pozycję.
  * sukces: dezorganizacja przeciwnika lub dostęp do nowych Zasobów
  * przykład: "Słyszałem, że Twoja żona ma romans z ambasadorem. Ambasador sam mi powiedział. Możemy o tym zapomnieć, ale..."

Definicja Zasobu:

* coś materialnego ale powszechnego (pieniądze, schronienie...)
* coś unikalnego (rzadki magitech, mroczna technologia, )
* wiedza (o Ruinie, o słabościach Ruiny, o tym czy lekarstwo działa...)
* wpływ (ludzie robią to co chcę, ludzie mi współczują, ludzie nie stają przeciw mnie, ludzie obracają się przeciw moim wrogom...)

### 9. Przykłady

* Nikodem Dyzma: bezrobotny który trafia do śmietanki towarzyskiej samą bezczelnością, szczęściem i przebiegłością, trzymając iluzję.
* "Nowe Szaty Cesarza": w tej bajce cesarz jest nagi i zmusza rzeczywistość do uwierzenia w jego wybitne szaty. Wizerunek nie bycia głupcem czy osobą niekompetentną przeważa nad rozsądkiem.
* IRL: sporo kultów i firm działa zgodnie z tą zasadą; nieważne jak jest, ważne jak nas postrzegają

