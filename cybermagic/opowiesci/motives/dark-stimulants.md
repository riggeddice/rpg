# Motive
## Metadane

* Nazwa: Dark Stimulants

## Krótki opis

Dopalacze, które mają dać Ci siłę. Będziesz lepszy. Silniejszy. Ale musisz dużo oddać by móc ich używać.

## Opis

Dopalacze, które mają dać Ci siłę. Będziesz lepszy. Silniejszy. Ale musisz dużo oddać by móc ich używać. W ramach tego motywu graj zarówno wzmocnieniem dopalonych osób jak i kosztami i tym, co musi być oddane by stymulanty działały prawidłowo. Dla niektórych użycie dark stimulants to decyzja nieodwracalna. Czasem - jedna z nielicznych opcji.

## Spoilers
