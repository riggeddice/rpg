# Motive
## Metadane

* Nazwa: Potwór - mastermind

## Krótki opis

Potwór skupiający się na ustawieniu wszystkiego tak jak powinno być wtedy gdy powinno być. Zarządza i manipuluje, samemu nie walcząc.

## Opis

Potwór skupiający się na ustawieniu wszystkiego tak jak powinno być wtedy gdy powinno być. Zarządza i manipuluje, samemu nie walcząc. Zawsze ma konkretny cel i agendę prowadząc inne siły by doprowadziły do idealnego stanu Potwora. Nie musi być inteligentny, wystarczy, że ma dość dużą moc wpływu. W ramach tego motywu zbuduj tego potwora, jego agendę i jakimi środkami będzie ją realizować pozostając samemu w cieniu.

## Spoilers
