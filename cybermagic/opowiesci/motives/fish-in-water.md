# Motive
## Metadane

* Nazwa: Fish in water

## Krótki opis

Postać ("ryba") znajduje się w środowisku którego jest perfekcyjnie nauczona i doń perfekcyjnie zaadaptowana.

## Opis

Postać ("ryba") znajduje się w środowisku którego jest perfekcyjnie nauczona i doń perfekcyjnie zaadaptowana. W tym motywie graj przygotowaniem Postaci do danego terenu, perfekcji adaptacji i przygotowania. Postać jest w swoim świecie, niezwykle trudno jej w czymkolwiek dorównać i nie tylko teren zna ale wykorzystuje wszystkie jego aspekty. Pokaż różnice kulturowe między 'rybą' a innymi. Teraz to inni nie są na swoim terenie a 'ryba' - tak.

## Spoilers
