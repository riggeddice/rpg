# Motive
## Metadane

* Nazwa: Symbol lepszego jutra

## Krótki opis

Coś lub ktoś reprezentujące lepszy świat o jaki warto walczyć. Nie tylko o przetrwanie - o coś więcej.

## Opis

Coś lub ktoś reprezentujące lepszy świat o jaki warto walczyć. Nie tylko o przetrwanie - o coś więcej. W ramach tego motywu skup się na wpływie tego symbolu na ludzi dookoła, zarówno tych identyfikujących się z symbolem jak i innych. Pokaż, jak ten symbol ma wpływ na innych, jak inspiruje ich do walki, nadziei lub dążenia do lepszych dni. Pokaż także, jak różne grupy mogą go interpretować lub używać w różny sposób, co może prowadzić do konfliktu. Pokaż, jak symbol jest zagarniany i reinterpretowany, by wspierać różne ideologie i podejścia. A niektórzy mogą chcieć pozyskać symbol tylko dla siebie.

## Spoilers
