# Motive
## Metadane

* Nazwa: Broken conditioning

## Krótki opis

Ktoś został przekształcony by pełnić konkretną rolę lub konkretne zadanie. Jednak owo przekształcenie / programowanie zostało złamane i ta osoba jest wolna.

## Opis

Ktoś został przekształcony by pełnić konkretną rolę lub konkretne zadanie. Jednak owo przekształcenie / programowanie zostało złamane i ta osoba jest wolna. Mogło się to stać dzięki magii, dzięki narkotykom czy bardzo silnym uczuciom (zwłaszcza w polu magicznym czy Skażeniu Pryzmatycznym). W ramach tego motywu pokaż kogoś, kto został _conditionowany_ do pełnienia konkretnej roli i jak po złamaniu ta osoba działa i zachowuje się inaczej. Pokaż dylematy oraz konflikty pomiędzy poprzednią i aktualną formą. 

## Spoilers
