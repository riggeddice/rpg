# Motive
## Metadane

* Nazwa: The Hunter

## Krótki opis

"Nieuchwytny apex predator". The Hunter to zabójca, polujący bezwzględnie na swoje ofiary. Mistrz polowania i atakowania z zaskoczenia, wzbudza strach i niepewność.

## Opis

### 1. Co reprezentuje

"Nieuchwytny apex predator". The Hunter to zabójca, polujący bezwzględnie na swoje ofiary. Mistrz polowania i atakowania z zaskoczenia, wzbudza strach i niepewność. The Hunter może być wielkim kotem, może też być szalonym komandosem polującym na kogoś. Niezależnie od tego, czy działa jako mistrz polowania w dzikiej dżungli, czy jako nieuchwytny zabójca w miejskiej dżungli, jego celem jest wyśledzenie, zaskoczenie i pokonanie ofiary. Często w sposób demonstrujący, że przeciwnik nigdy nie miał szans.

### 2. Kiedy wygrywa

The Hunter triumfuje, gdy jego cel zostaje pokonany – najlepiej w sposób, który podkreśla umiejętności łowieckie The Hunter. Czasami pełni też rolę 'the-assassin' - wtedy wygrywa, jak cel na który poluje zostanie zabity.

### 3. Jak działa

Finty, pułapki, ataki z zaskoczenia. Praktycznie nigdy nie działa frontalnie. The Hunter wybiera moment i miejsce konfrontacji, zawsze starając się utrzymać przewagę niespodzianki. Jego metody są różnorodne, od klasycznych zasadzek, poprzez zaawansowane pułapki, aż po wykorzystanie środowiska przeciwko ofierze.

The Hunter praktycznie zawsze działa sam. Ma przewagę fizyczną (i czasem technologiczną) nad Graczami. Nie zawsze musi być świetny w długotrwałej walce - ale zawsze tak ustawia sytuację, by móc przeciwnika osaczyć i pokonać.

### 4. Jak wykorzystywać ten motyw

Skup się na budowaniu napięcia i strachu związanego z ciągłym poczuciem bycia obserwowanym i polowanym. Skup się na wpływie The Hunter na społeczność - pokaż, jak różne postacie próbują przewidzieć ruchy nieuchwytnego drapieżnika, oraz jak radzą sobie z ciągłym zagrożeniem. Pokaż, jak społeczność próbuje w pewien sposób go przebłagać lub oswoić. Może dostarczają ofiary? Może ściągają coraz groźniejszych łowców do upolowania The Hunter? Może zmieniają swoje zachowanie?

Wykorzystaj fakt, że nikt nie wie kim/czym jest The Hunter, gdzie jest i kogo zaatakuje. Głównym uczuciem jest niepewność i działanie na krawędzi. The Hunter może być WSZĘDZIE.

### 5. Przykłady

* Predator z filmu "Predator": dość oczywiste, prawie trope-namer
* Elim Garak, "Deep Space Nine", gdy był pod wpływem 
* Kraven the Hunter, przeciwnik Spidermana
* Seryjny morderca, nieuchwytny przez policję

## Spoilers
