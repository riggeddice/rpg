# Motive
## Metadane

* Nazwa: Stara jednostka bojowa

## Krótki opis

Stara, uszkodzona, nie do końca sprawna jednostka militarna. Potencjalnie autonomiczna. Potencjalnie z własną agendą. Bardzo niebezpieczna.

## Opis

Stara, uszkodzona, nie do końca sprawna jednostka militarna. Potencjalnie autonomiczna. Potencjalnie z własną agendą. Bardzo niebezpieczna. W ramach tego motywu pokaż agendę jednostki, pokaż też jej niesamowite uszkodzenia (implikujące nie do końca pełną sprawność). Graj tym jak obecność starej jednostki z całym jej doświadczeniem i siłą ognia destabilizuje daną sytuację.

## Spoilers
