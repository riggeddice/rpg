# Motive
## Metadane

* Nazwa: Strefa Sanktuarium

## Krótki opis

Miejsce, które stanowi ochronę i miejsce ukojenia. Miejsce bezpieczne i przyjemne w odróżnieniu od wszystkiego co jest dookoła.

## Opis

Miejsce, które stanowi ochronę i miejsce ukojenia. Miejsce bezpieczne i przyjemne w odróżnieniu od wszystkiego co jest dookoła. Miejsce, w którym nie trzeba się martwić, że ktoś Cię zaatakuje. Miejsce, w którym możesz odpocząć. Miejsce, o które warto walczyć.

W ramach tego motywu pokaż wpływ Sanktuarium na otoczenie. Pokaż, jak ludzie się zachowują tu a jak tam. Pokaż, co dzięki Sanktuarium zyskaliśmy i co byśmy stracili, gdyby Sanktuarium nie było. Pamiętaj - by istniała Strefa Sanktuarium, musi być jej odwrotność, jakiś kontrast.

## Spoilers
