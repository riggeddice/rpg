# Motive
## Metadane

* Nazwa: Oszczędzanie na wszystkim

## Krótki opis

Szukamy oszczędności wszędzie gdzie się da. Nie tylko mamy nie najlepszej klasy sprzęt, ale próbujemy też usunąć nieefektywności.

## Opis

Szukamy oszczędności wszędzie gdzie się da. Nie tylko mamy nie najlepszej klasy sprzęt, ale próbujemy też usunąć nieefektywności. W ramach tego motywu pokaż sposoby radzenia sobie z problemem marnotrawstwa, poszukiwaniem oszczędności oraz często za dalekim pójściem w stronę oszczędności i co się wtedy sypie. Ogranicz redundancję i pokaż ten świat ubóstwa ;-).

## Spoilers
