# Motive
## Metadane

* Nazwa: Ratunek za wszelką cenę

## Krótki opis

Ktoś lub coś potrzebuje pomocy. Nie da się tej osobie pomóc posiadanymi zasobami. Więc potrzebne jest pójście o parę kroków dalej, tam, gdzie nie powinno się iść.

## Opis

Ktoś lub coś potrzebuje pomocy. Nie da się tej osobie pomóc posiadanymi zasobami. Więc potrzebne jest pójście o parę kroków dalej, tam, gdzie nie powinno się iść. Może w stronę kradzieży, krzywdzenia innych, magii czy Esuriit? W ramach tego motywu podkreśl konsekwencje i zniszczenia wynikające z ruchów ratunkowych, ale pokaż czystą intencję ratującego.

## Spoilers
