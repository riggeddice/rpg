# Motive
## Metadane

* Nazwa: Perfect Vision

## Krótki opis

Jedna ze stron lub frakcji widzi WSZYSTKO co się dzieje z jakiegoś powodu. Może ma poukrywane czujniki, może przewagę magii?

## Opis

Jedna ze stron lub frakcji widzi WSZYSTKO co się dzieje z jakiegoś powodu. Może ma poukrywane czujniki, może przewagę magii? W ramach tego motywu korzystaj z tego, że jedna ze stron ma ABSOLUTNĄ przewagę percepcyjną nad innymi stronami i widzi WSZYSTKO. Demonstruj tą przewagę i korzystaj z niej.

## Spoilers
