# Motive
## Metadane

* Nazwa: Dotyk Saitaera

## Krótki opis

Manifestacja Adaptacji. Adaptacja za wszelką cenę. Obecność boga i jego celu - perfekcyjna forma przy odrzuceniu wszystkiego co zbędne.

## Opis

Manifestacja Adaptacji. Adaptacja za wszelką cenę. Obecność boga i jego celu - perfekcyjna forma przy odrzuceniu wszystkiego co zbędne. Człowieczeństwo konfrontuje się z perfekcją adaptacji, czasem w sposób udany a czasem nie. Saitaer pragnie odbudowy swego martwego świata i dostosowania się do tego świata, którego jeszcze nie rozumie w pełni. W ramach tego motywu podkreśl konflikt między człowieczeństwem a pragnieniem perfekcyjnej adaptacji, jak i pokaż co dzieje się z tymi, których dotknął Saitaer i jego ideały.

## Spoilers
