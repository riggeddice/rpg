# Motive
## Metadane

* Nazwa: Krzywdzę, by się chronić

## Krótki opis

Ktoś zrobił coś złego, by się przed czymś chronić. "Gdybym nie podpalił stodoły, wyszłoby na jaw że zdradziłem żonę." 

## Opis

Ktoś zrobił coś złego, by się przed czymś chronić. "Gdybym nie podpalił stodoły, wyszłoby na jaw że zdradziłem żonę." W ramach tego motywu osoba krzywdząca może działać w samoobronie ("zaatakował mnie, odepchnąłem, nie wiedziałem że zginie") czy wyprzedzająco ("gdybym nie zniszczył jego reputacji, moja by ucierpiała"). Zawsze jednak doszło do czegoś złego i jest ryzyko, że to zło będzie kaskadować i za jednym złym czynem musi pójść drugi lub trzeci.

## Spoilers
