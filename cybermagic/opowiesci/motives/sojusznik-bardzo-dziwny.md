# Motive
## Metadane

* Nazwa: Sojusznik, bardzo dziwny

## Krótki opis

Współpracujemy z kimś DZIWNYM, bardzo odchylonym od norm społecznych lub o obcych celach.

## Opis

Współpracujemy z kimś DZIWNYM, bardzo odchylonym od norm społecznych lub o obcych celach. Ta osoba może mieć dziwne cele, nietypowe metody lub zachowywać się 'głupio'. Nie jest to osoba, z którą byśmy współpracowali w normalnych sytuacjach. Ten dziwny sojusznik może być po stronie Zespołu, lub po stronie którejś z frakcji. W ramach tego motywu podkreślaj dziwność sojusznika i jego interakcje z normalsami. Np. Olga Myszeczka. 

## Spoilers
