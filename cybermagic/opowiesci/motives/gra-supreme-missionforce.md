# Motive
## Metadane

* Nazwa: Gra Supreme Missionforce

## Krótki opis

Supreme Missionforce to świetna grupowa gra strategiczna, rozgrywana na całym świecie.

## Opis

Supreme Missionforce to świetna grupowa gra strategiczna, rozgrywana na całym świecie. Jest fenomenem kulturowym, gra PvP z inkarnacją, uczącą umiejętności strategicznych, taktycznych, koncentracji i alokacji oddziałów. Ma nagrody, turnieje lokalne itp. Jest oglądana, są na jej punkcie robione zakłady i rozgrywki. Sterowana jest przez samego Yyizdatha, z setkami prywatnych modów i serwerów. W sesji powiązanej z tym motywem dzieje się coś ważnego właśnie w kontekście Supreme Missionforce.

## Spoilers
