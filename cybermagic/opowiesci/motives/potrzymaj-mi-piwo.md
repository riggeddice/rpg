# Motive
## Metadane

* Nazwa: Potrzymaj mi piwo

## Krótki opis

Ktoś chce się popisać i robi coś niezwykle głupiego. Sytuacja robi się fatalna.

## Opis

Ktoś chce się popisać i robi coś niezwykle głupiego. Sytuacja robi się fatalna. W ramach tego motywu pokaż niemożebną głupotę tego popisywania się, efekty uboczne i wynikające problemy. Wykorzystasz motyw prawidłowo, jeśli gracze co najmniej raz facepalmują. W ramach tego motywu albo sprzątamy po piwoszach albo naprawiamy wszystko co się posypało albo ukrywamy sprawę.

## Spoilers
