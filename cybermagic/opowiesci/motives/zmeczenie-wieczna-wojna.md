# Motive
## Metadane

* Nazwa: Zmęczenie wieczną wojną

## Krótki opis

Od dawna jedyne co pamięta to walka, konflikt, ścieranie. Ale zasoby nie te, przyjaciół już nie ma a i ciało nie w formie. Może czas przestać walczyć?

## Opis

Od dawna jedyne co pamięta to walka, konflikt, ścieranie. Ale zasoby nie te, przyjaciół już nie ma a i ciało nie w formie. Może czas przestać walczyć? Ten motyw skupia się na kimś, kto ma ogromne możliwości i okazje do walki, ale już po prostu ma dosyć. Teoretycznie poczucie obowiązku zmusza do dalszej walki, ale w sumie nawet już nie pamięta o co walczy i co może osiągnąć. W ramach tego motywu pokaż zmęczenie wojownika. Pokaz niezwykłą kompetencję, ale też brak chęci do niszczenia. I interakcję wojownika ze społeczeństwem, które dalej postrzega wojownika jako aktywne zagrożenie lub mesjasza.

## Spoilers
