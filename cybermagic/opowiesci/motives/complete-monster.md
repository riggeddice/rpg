# Motive
## Metadane

* Nazwa: Complete Monster

## Krótki opis

Complete Monster - Niektórych ludzi nie da się już traktować jak pobratymców.

## Opis

Complete Monster - Niektórych ludzi nie da się już traktować jak pobratymców. Niektórych ludzi nie da się już traktować jak pobratymców. To co zrobili wyklucza ich z grona ludzi. W tej opowieści występuje tego typu osoba, wypaczająca sesję bezwzględnością, okrucieństwem i swoimi celami.

## Spoilers
