# Motive
## Metadane

* Nazwa: Energia Anteclis

## Krótki opis

Energia Anteclis. Ucieczka przed entropią. Innowacja i ucieczka do przodu. Zawsze o krok dalej niż entropia.

## Opis

Energia Anteclis. Ucieczka przed entropią. Innowacja i ucieczka do przodu. Zawsze o krok dalej niż entropia.

Jest to energia bycia tam, gdzie jeszcze nikogo nie było – innowacja, ucieczka przed entropią, znalezienie drogi, na którą nikt nie wpadł. Rzeczy możliwe, ale nietypowe. Niezależnie od poziomu trudności da się znaleźć jakieś rozwiązanie i nawet jeśli nie da się kontrolować przyszłości, da się użyć nowych narzędzi by kupić sobie jeszcze trochę czasu. Znajdziemy rozwiązanie, w miejscu którego nikt nigdy nie szuka.

W ramach tego motywu graj ‘slippery’ - coś czego nikt się nie spodziewa. Jeśli jest chociaż jedna ścieżka, zostanie wybrana. Podejście typu karaluch. A jednocześnie niesamowita innowacja. Podstawowe uczucie jakie gracze powinni czuć w połączeniu z tą energią to „zaskoczenie”. 

## Spoilers
