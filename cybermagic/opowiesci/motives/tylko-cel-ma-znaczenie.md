# Motive
## Metadane

* Nazwa: Tylko cel ma znaczenie

## Krótki opis

Cel uświęca środki. Poświęci wszystko, odrzuci wszystko, ale cel zostanie osiągnięty. Nieważne jak zapamięta Cię historia. Cel i wynik.

## Opis

Cel uświęca środki. Poświęci wszystko, odrzuci wszystko, ale cel zostanie osiągnięty. Nieważne jak zapamięta Cię historia. Cel i wynik. "Well-intentioned extremist". Col. Hanse Castillo. W ramach tego motywu pokaż osobę, która posuwa się za daleko, by osiągnąć swój cel. W swoich oczach jest bohaterem i nie dba o nic innego niż osiągnięcie celu.

## Spoilers
