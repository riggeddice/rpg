# Motive
## Metadane

* Nazwa: Wojna Bogów

## Krótki opis

Energie i siły dwóch Bogów się ścierają, w wyniku czego dochodzi do czegoś wielkiego, katastrofalnego lub nieprzewidzianego.

## Opis

Energie i siły dwóch Bogów się ścierają, w wyniku czego dochodzi do czegoś wielkiego, katastrofalnego lub nieprzewidzianego. Może to konieczność doeskortowania kogoś reprezentującego jedną energię do drugiej. Może to emergentny efekt ścierania się boskich mocy. Może to jedna energia miażdżąca energię drugą? W ramach tego motywu pokaż różnice energii między siłami bogów, zrób z tego ścierania się energii ważny komponent. Skup się na tym, by pokazać INTEREAKCJE między różnymi energiami.

## Spoilers
