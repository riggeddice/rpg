# Motive
## Metadane

* Nazwa: The Warden

## Krótki opis

"Nigdy stąd nie wyjdziesz". Tam COŚ jest, coś strasznego, uwięzionego w ciemności. The Warden dba o to, by to się nie wydostało.

## Opis

"Nigdy stąd nie wyjdziesz". Tam COŚ jest, coś strasznego, uwięzionego w ciemności. The Warden dba o to, by to się nie wydostało. To co jest uwięzione w ciemności jest przerażające i niebezpieczne. The Warden dba o to by nikt więcej nie ucierpiał przez to coś. Jeżeli by chronić szerszą populację musi zabić osoby próbujące się tam dostać, to to zrobi. Niebezpieczny w walce i perfekcyjnie znający swój teren, chroni rzeczywistość przed swoim więźniem.

W ramach tego motywu pokaż, że The Warden nie jest zainteresowany niczym innym niż uniemożliwienie dostępu do więźnia. Niezależnie od tego jak wielki jest kryzys, The Warden nie udostępni więźnia nikomu. Najpewniej The Warden jest jakimś bytem ponadczasowym – organizacją, duchem, systemem AI. Jest niemożliwy do przekupienia i w pełni oddany swojej pracy.

The Warden wygrywa, jeśli uda mu się uniemożliwić wyjście więźnia na wolność bądź kontakt z więźniem.

## Spoilers
