# Motive
## Metadane

* Nazwa: The Survivor

## Krótki opis

"Jest jak karaluch". The Survivor przetrwa w najbardziej ekstremalnych warunkach, niezależnie od kosztu.

## Opis

### 1. Przykładowe warianty

* Samotny żołnierz na tyłach wroga, podejmujący trudne decyzje by przetrwać, kosztem okolicznych cywili
* Ostatni mechanik zniszczonego statku kosmicznego, który żywi się ciałami swoich towarzyszy i próbuje wysłać SOS
* Więzień obozu pracy, współpracujący z prześladowcami, by jemu nie stało się nic złego
* Przestępca unikający agentów, którzy na niego polują (manhunt)
* Dziki kot, który wpadł w pułapkę i odgryzł swoją łapę by móc przetrwać.

### 2. Co reprezentuje

"Jest jak karaluch". The Survivor przetrwa w najbardziej ekstremalnych warunkach, niezależnie od kosztu. Dla The Survivor jedynym celem jest przetrwanie, niezależnie od moralnych, etycznych czy fizycznych kosztów. Reprezentuje walkę o życie w sytuacjach, gdy szanse na przetrwanie wydają się minimalne. Jest personifikacją ludzkiego instynktu przetrwania, który w skrajnych warunkach popycha jednostkę do działania poza granicami moralności, etyki i możliwości.

The Survivor definiowany jest przez następujące aspekty:

* Zagrożenie: The Survivor znajduje się w skrajnie wrogim środowisku, ale potrafi w nim funkcjonować, zaadaptować się i przetrwać.
* Blizny: The Survivor wiele już poświęcił i już jest _niekompletny_. Może mieć rany fizyczne lub psychiczne, ale już coś zapłacił.
* Bezwzględność: The Survivor zrobi wszystko, by przetrwać i zobaczyć jeszcze jeden wschód słońca.

### 3. Gdzie jest silnik konfliktu?

* The Survivor usuwa z wyprzedzeniem wszystkie zagrożenia prawdziwe lub postrzegane.
* The Survivor robi rzeczy niemoralne; zatracił to co "normalne" w skrajnych warunkach w których żyje / żył.
* The Survivor wrzuci innych pod autobus, by jedynie przetrwać. Patrzy na wszystko bardzo pragmatycznie.
* Jeśli ktoś coś ma a The Survivor potrzebuje do przetrwania, The Survivor to pozyska.

### 4. Kiedy wygrywa

The Survivor wygrywa, gdy uda mu się przetrwać.

* Przetrwał bardzo trudną sytuację, może wrócić do poprzedniego stanu (sprzed Zagrożenia)
* Dostał się do potencjalnie bezpiecznego miejsca, jest poza zasięgiem Zagrożenia

### 5. Kiedy przegrywa

Porażka The Survivor jest powiązana z jego śmiercią / zniszczeniem:

* The Survivor przegrywa, jeśli zginie. Tyle i tylko tyle. Jeśli nie przetrwa lub nie może przetrwać.
* Zamknięcie The Survivor w więzieniu niekoniecznie jest pełnią porażki

### 6. Jak działa

Typowe działania:

* Unikanie konfrontacji i minimalizowanie swojej obecności. 
  * Jest mistrzem kamuflażu, zarówno w sensie fizycznym, jak i społecznym. 
  * Bardzo trudny do wykrycia i do usunięcia, skupia się mniej na rozmnożeniu czy zniszczeniu czegoś a dużo bardziej na przetrwaniu. 
* Wykorzystanie każdego dostępnego zasobu do maksymalizacji szans na przetrwanie, nawet jeśli wymaga to poświęceń.
* Doskonale adaptuje się do zmieniających się warunków, wykorzystując wszystko, co może mu pomóc przetrwać. 
* Tam, gdzie inni się poddadzą, tam The Survivor walczy dalej, niezłomny.
* Gotowość do podejmowania trudnych decyzji, które mogą obejmować ryzyko własnego życia lub życia innych w imię większego dobra.

The Survivor jest również biegły w wykorzystywaniu otoczenia do swoich celów, potrafiąc przekształcić nawet najbardziej niegościnne środowisko w swój azyl.

### 7. Jak wykorzystywać ten motyw

Wykorzystuj właśnie pragnienie przetrwania za wszelką cenę. Nie ma czegoś czego The Survivor nie zrobi żeby przetrwać - łącznie z odgryzieniem własnej łapy jeśli to będzie konieczne.

Pokaż wytrwałość ludzkiej (lub nieludzkiej) natury, siłę instynktu przetrwania i wpływ skrajnych warunków na osobowość oraz wartości jednostki. Jeśli brakuje zasobów, pokaż, jak The Survivor jest skłonny robić straszne rzeczy by zapewnić sobie przetrwanie. Pokaż też ostrzeżenie przed ceną, jaką czasem trzeba zapłacić za życie za wszelką cenę. "My opuściliśmy Wietnam, ale Wietnam nie opuścił nas."

### 8. Jakie ma ruchy

* 1 - Opuszczenie niebezpiecznego miejsca poświęcając coś bardzo cennego ŻEBY się wymknąć z Zagrożenia
  * sukces: The Survivor się wyślizgnął, nawet, jeśli traci część ciała lub coś na czym mu bardzo zależało
  * przykład: Zostawia swoją bazę, surowce i przyjaciół by tylko się wymknąć i uniknąć rajdu policji
* 2 - Skierowanie uwagi gdzieś indziej ŻEBY kupić sobie czas i uniknąć problemu
  * sukces: The Survivor wymanewrował tych którzy nań polowali; nie wiedzą kim jest i gdzie jest lub rozproszył ich zasoby
  * przykład: Wybuch bomby w zupełnie niepowiązanym miejscu lub zabicie niewinnego, by motyw The Survivor został zgubiony
* 3 - Atak wyprzedzający, zniszczenie kogoś lub czegoś ŻEBY to się nie stało dlań zagrożeniem w przyszłości
  * sukces: coś co może zagrozić The Survivor nie będzie w stanie tego zrobić; może ktoś zginął, może coś zniszczono?
  * przykład: Podpalenie hotelu, gdzie w sejfie gromadzone są dowody na działanie The Survivor
* 4 - Pozyskanie kryjówki, zasobu czy zapasu żywności ŻEBY móc przetrwać w ukryciu / w trudnych warunkach
  * sukces: The Survivor może przycupnąć na pewien czas i nie musi się wychylać
  * przykład: pieczara "dla szczura", trochę wzmocniona - miejsce, gdzie nikt normalny by się nie schował
* 5 - Zmiana lokalizacji do bezpiecznego miejsca ŻEBY uniknąć zaciskającej się pętli
  * sukces: The Survivor jest w nowym miejscu i wszystkie stare ślady doń już nie prowadzą
  * przykład: wejście na teren większego drapieżnika w formie zakamuflowanej, by jego zapach odstraszał psy tropiące
* 6 - Zdrada lub przekupstwo ŻEBY przetrwać w sytuacji konfrontacji
  * sukces: The Survivor najpewniej zostałby zniszczony, ale przetrwa, acz poświęci wszystko inne
  * przykład: Przekazanie informacji o lokalizacji sojuszników wrogowi w zamian za gwarancję bezpieczeństwa
* 7 - Zdobycie zasobów w ekstremalnych warunkach ŻEBY przetrwać pewien czas
  * sukces: The Survivor ma środki, czy to w burzy piaskowej czy odzyskując wodę z moczu
  * przykład: wykorzystanie chemicznie zanieczyszczonego źródła wody, rozumiejąc konsekwencje - ale to jedyna opcja teraz
* 8 - Zdobycie zasobów w ekstremalny sposób, kosztem niewinnych czy niemoralnie ŻEBY przetrwać pewien czas
  * sukces: The Survivor ma dostęp do ważnych zasobów, acz ogromnym kosztem moralnym
  * przykład: "To byli członkowie mojej załogi. Teraz to tylko efektywne źródło protein"
* 9 - Zastawienie pułapki w swoim terenie ŻEBY usunąć potencjalne zagrożenie
  * sukces: To, co polowało na The Survivor zostaje usunięte i polować nań już nie może
  * przykład: zaminowanie swojej bazy; strata części zasobów, ale zniszczenie grupy polującej
* 10 - Konstrukcja bazy / schronienia ŻEBY przetrwać sytuację w której większość zginie
  * sukces: The Survivor przetrwa skrajnie niekorzystne warunki
  * przykład: Zbudowanie ziemianki pod zniszczonym statkiem kosmicznym i użycie pancerza statku do schowania się przed burzą

### 9. Przykłady

* Robinson Crusoe lub "The Martian" (książka Weira): delikwent został sam na Marsie i musiał przetrwać, niesamowicie kombinując i optymalizując zasoby.
* Zec Chelovek z cyklu o Jacku Reacherze: delikwent przetrwał obóz, praktycznie stracił człowieczeństwo, ale dożył godnego wieku.
* "127 godzin": Ralston został uwięziony przez głaz przygniatający mu rękę w skałach. Po pięciu dniach prób wydostania się, Ralston amputował sobie rękę przy użyciu kieszonkowego noża.
* Matka w okupowanym mieście, zmuszona do zabicia niemowlaka bo płacz doprowadzi do śmierci całej rodziny.
