# Motive
## Metadane

* Nazwa: Przeciwnik: Terrorform

## Krótki opis

Na tej sesji występuje Terrorform, istota która oprócz bycia niebezpieczną z uwagi na swoją naturę powoduje strach.

## Opis

Na tej sesji występuje Terrorform, istota która oprócz bycia niebezpieczną z uwagi na swoją naturę powoduje strach. Terrorform zwykle kojarzony jest z energią ixiońską i głębokim Skażeniem jakiegoś nieszczęśnika.

## Spoilers
