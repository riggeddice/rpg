# Motive
## Metadane

* Nazwa: Dobry uczynek się mści

## Krótki opis

Ktoś chciał pomóc. Przez to ma problemy i potencjalnie potrzebuje pomocy.

## Opis

Ktoś chciał pomóc. Przez to ma problemy i potencjalnie potrzebuje pomocy. Ten motyw skupia się na 'ratowaniu kogoś kto próbował ratować'. Pokazaniu, że gdyby nic ta osoba nie zrobiła, nie wpadłaby w takie tarapaty. Skonfliktuj 'jak mu pomóc by samemu nie ucierpieć'. Pokaż poniesione koszty i ogólnie uczucie 'serio?'

## Spoilers
