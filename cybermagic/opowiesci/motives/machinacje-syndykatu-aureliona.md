# Motive
## Metadane

* Nazwa: Machinacje Syndykatu Aureliona

## Krótki opis

Syndykat Aureliona ma dziesiątki macek, próbujących oplątać i wciągnąć ludzi w sieć Syndykatu. W tej opowieści pojawia się jedna z macek.

## Opis

Syndykat Aureliona ma dziesiątki macek, próbujących oplątać i wciągnąć ludzi w sieć Syndykatu. W tej opowieści pojawia się jedna z macek. W ramach tego motywu graj bezdusznością Syndykatu, ich chęcią maksymalizacji zysku i korzyści, traktowaniu ludzi jak zasobu oraz specyficznej hierarchii pansystemowego Syndykatu. Pokaż, że wszyscy są elementem morderczej, efektywnej i bezdusznej maszyny próbującej zarobić jak najwięcej i nie zwracającej uwagi na pojedyncze jednostki.

## Spoilers
