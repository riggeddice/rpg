# Motive
## Metadane

* Nazwa: Teren, spawner

## Krótki opis

Ten teren (nawiedzony cmentarz, biolab) ma umiejętności spawnowania pomniejszych bytów i potworów. To sprawia, że nigdy nie jest się bezpiecznym.

## Opis

Ten teren (nawiedzony cmentarz, biolab) ma umiejętności spawnowania pomniejszych bytów i potworów. To sprawia, że nigdy nie jest się bezpiecznym. W ramach tego motywu spawnuj różnego rodzaju potwory i pokaż niebezpieczeństwa wynikające z tego, że teren projektuje swoje niebezpieczeństwo i siłę poza pewien zasięg i obszar. Pokaż wpływ terenu na innych, zaatakuj cywili potworami i zademonstruj działanie spawnera i jego wpływ na otoczenie.

## Spoilers
