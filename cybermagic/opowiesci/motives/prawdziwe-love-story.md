# Motive
## Metadane

* Nazwa: Prawdziwe love story

## Krótki opis

On ją kocha. Ona kocha jego. Oni z jakiegoś powodu nie są razem, ale próbują i może im się udać.

## Opis

On ją kocha. Ona kocha jego. Oni z jakiegoś powodu nie są razem, ale próbują i może im się udać. Przy tym motywie pokaż naprawdę głęboko kochające się osoby które próbują albo zostać razem, albo doprowadzić do tego by ta druga osoba była szczęśliwsza. Odbijaj problemy, czemu to jest niefortunne / niemożliwe. Spraw, by strony faktycznie próbowały sprawić, że ta druga osoba będzie szczęśliwa.

## Spoilers
