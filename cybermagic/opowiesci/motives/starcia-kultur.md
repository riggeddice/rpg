# Motive
## Metadane

* Nazwa: Starcia kultur

## Krótki opis

Mamy więcej niż jedną kulturę i rozgrywamy te kultury i ich unikalne cechy w kontraście do siebie. Są bardzo różne i to powoduje problemy.

## Opis

Mamy więcej niż jedną kulturę i rozgrywamy te kultury i ich unikalne cechy w kontraście do siebie. Są bardzo różne i to powoduje problemy. W ramach tego motywu skup się na pokazaniu różnic między kulturami, ich przeszłych waśni i różnic. Nawet, jeśli chcą tego samego z uwagi na przeszłe zaszłości nie są w stanie elegancko współpracować ze sobą.

## Spoilers
