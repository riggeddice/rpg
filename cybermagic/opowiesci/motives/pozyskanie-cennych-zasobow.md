# Motive
## Metadane

* Nazwa: Pozyskanie cennych zasobów

## Krótki opis

Ktoś potrzebuje cennych zasobów, kluczowych do dalszej części działań. I udało mu się je pozyskać.

## Opis

Ktoś potrzebuje cennych zasobów, kluczowych do dalszej części działań. I udało mu się je pozyskać. W ramach tego motywu graj poszerzeniem możliwości frakcji, która potrzebowała kluczowych zasobów i udało się te zasoby pozyskać. Pokaż zmianę sytuacji. Pokaż, co da się teraz zrobić. Alternatywnie - pokaż operację pozyskiwania tych cennych zasobów.

## Spoilers
