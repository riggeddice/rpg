# Motive
## Metadane

* Nazwa: The Savior

## Krótki opis

"Uratuję wszystkich". The Savior skupia się na pomocy konkretnej osobie czy grupie którą próbuje uratować, kosztem wszystkiego innego.

## Opis

"Uratuję wszystkich". The Savior skupia się na pomocy konkretnej osobie czy grupie którą próbuje uratować, kosztem wszystkiego innego. The Savior jest z jakiegoś powodu silnie związany z Ofiarą i próbuje zapewnić by Ofiara wróciła do chwały i potęgi. To może być strażak ratujący własne dzieci z płomieni, to może być też paladyn walczący o naprawę swej skompromitowanej organizacji. Tak czy inaczej, ten archetyp zawiera element poświęcenia - pójście o krok za daleko lub działania ostateczne. 

W ramach tego motywu graj determinacją The Savior oraz tym, że fundamentalnie jego cele są poprawne - acz w danym kontekście jego metody mogą być złe lub jego cel może być zły (np. próbuje uratować demoniczne potwory). Pokaż, że każdy Gracz w innym kontekście mógłby być jak The Savior. Potencjalnie spróbuj graczy skusić - może uda się przekonać ich, że The Savior ma rację.

The Savior wygrywa, gdy zapewni że ci których próbuje osłonić / uratować będą bezpieczni.

## Spoilers
