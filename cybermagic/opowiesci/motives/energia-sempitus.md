# Motive
## Metadane

* Nazwa: Energia Sempitus

## Krótki opis

Energia Sempitus. Nieśmiertelność, nieskończona ekspansja i przetrwanie za wszelką cenę.

## Opis

Energia Sempitus. Nieśmiertelność, nieskończona ekspansja i przetrwanie za wszelką cenę. 

Energia powiązana z survivalem, odpornością na zło które nas otacza i wolą przetrwania za wszelką cenę. Nieważne czego nie musisz zrobić przetrwa i będzie funkcjonować dalej. Wieczna walka z entropię ą czy popraw ekspansję czy optymalizację, czy próbę spowolnienia procesów starzenia lub zastępowanie siebie mechanizmami. Celem nie jest adaptacja a przetrwanie i wykorzystywanie wszystkiego do granic możliwości nie z perspektywy siebie, a z perspektywy potencjalnych zasobów wszechświata.

W ramach tego motywu graj ostrożnością, przetrwaniem i próbą odsunięcia od siebie entropii. Graj dążeniem do nieśmiertelności i zachłannej ekspansji do momentu aż dalsza ekspansja nie ma sensu, bo koszt pozyskania będzie większy niż wartość eksploatacji.

## Spoilers
