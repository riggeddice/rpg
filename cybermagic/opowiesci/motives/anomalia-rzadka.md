# Motive
## Metadane

* Nazwa: Anomalia, Rzadka

## Krótki opis

Na tej sesji występuje szczególnie dziwna i potężna Anomalia Magiczna.

## Opis

Na tej sesji występuje szczególnie dziwna Anomalia magiczna. Ten motyw skupia się na interakcjach postaci z anomalną rzeczywistością. To typ anomalii wymagającej pewnych badań i eksploracji, do końca nie wiadomo co robi i czym jest.

## Spoilers
