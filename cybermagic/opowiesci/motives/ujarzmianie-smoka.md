# Motive
## Metadane

* Nazwa: Ujarzmić Smoka

## Krótki opis

Jeździec próbuje ujarzmić straszliwego nieokiełznanego Smoka, który próbuje Jeźdźca zmiażdżyć i zniszczyć.

## Opis

Jeździec próbuje ujarzmić straszliwego nieokiełznanego Smoka, który próbuje Jeźdźca zmiażdżyć i zniszczyć. Pokaż Smoka jako nieprawdopodobnie potężną bestię (anomalię, energię, konstrukt). Pokaż Jeźdźca jako skutecznego agenta. Pokaż sojuszników Jeźdźca. Niech Jeździec opracuje współpracę z innymi siłami, którymi wspólnie wpadną na to jak cholernego Smoka ujarzmić. Niech Smok niszczy i dewastuje, nieokiełznany. Pokaż dylematy i straty wynikające z próby opanowania koszmaru.

## Spoilers
