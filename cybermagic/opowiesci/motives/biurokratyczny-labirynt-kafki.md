# Motive
## Metadane

* Nazwa: Biurokratyczny labirynt Kafki

## Krótki opis

Tysiące dokumentów. Nie wiadomo jak dojść do prawdy. Wszystko niby da się osiągnąć, ale JAK?

## Opis

Tysiące dokumentów. Nie wiadomo jak dojść do prawdy. Wszystko niby da się osiągnąć, ale JAK? W ramach tego motywu podkreśl biurokrację jako inkarnację Molocha - system który dominuje wszystkie elementy tego systemu. Dostęp do odpowiednich dokumentów może wiele rozwiązać, ale nie wiadomo gdzie są lub jak ich szukać. Wypełnienie form pomoże, ale które i do kogo adresować...

## Spoilers
