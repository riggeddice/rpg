# Motive
## Metadane

* Nazwa: The Corruptor

## Krótki opis

"Zrób tylko tą jedną rzecz". The Corruptor skupia się na zmianie systemu wierzeń i funkcjonowania swoich ofiar, by zachowywały się inaczej i coś innego uważały za słuszne. 

## Opis

### 1. Co reprezentuje

"Zrób tylko tą jedną rzecz". The Corruptor skupia się na zmianie systemu wierzeń i funkcjonowania swoich ofiar, by zachowywały się inaczej i coś innego uważały za słuszne. Subtelnie manipuluje swoimi ofiarami i sprawia, że stopniowo stają się czymś czego kiedyś by nie zaakceptowały. Czy to droga od paladyna do kanibalistycznego berserkera, czy od grzecznej damy do panienki lekkich obyczajów, The Corruptor skupia się na przekształceniu swoich ofiar w sposób taki, by nawet nie wiedziały, że się zmieniają. Zmienia system wierzeń i funkcjonowania swoich ofiar.

### 2. Kiedy wygrywa

The Corruptor wygrywa, gdy jego ofiary przyswajają i akceptują nowe wartości jako własne. Zwycięstwo jest punktem końcowym stopniowej ewolucji – momentem, gdy ofiary nie tylko akceptują nowy system wartości, ale również zaczynają działać zgodnie z nim, często nie zdając sobie sprawy z głębi swojej transformacji.

### 3. Jak działa

The Corruptor wykorzystuje szeroki wachlarz narzędzi, od manipulacji psychologicznej, poprzez wykorzystanie słabości ofiar, aż po stosowanie biochemicznego wspomagania. Jego działania są dopasowane do indywidualnych predyspozycji ofiar, tak aby każdy krok na drodze przemiany wydawał się naturalnym i własnym wyborem. Corruptor działa przez zmianę kontekstu, podsuwanie fałszywych przekonań lub wykorzystywanie sytuacji kryzysowych. Często te kryzysy są budowane przez The Corruptor.

Pamiętaj - Corruptor nie kontroluje umysłów swoich ofiar, ale modyfikuje ich system wartości.

### 4. Jak wykorzystywać ten motyw

Skup się na subtelnej grze psychologicznej i stopniowej przemianie. Pokaż, jak ofiary przechodzą od oporu do akceptacji, aż w końcu do pełnego zaangażowania w nowy sposób bycia. Pokaż mechanizmy obronne i momenty zwątpienia, jak również punkty zwrotne, które przekształcają niechęć w pragnienie. Graj zmianą zachowań ofiar Corruptora i podkreślaj, że to co kiedyś dla nich było nieakceptowalne stało się nie tylko normalne ale i pożądane. Zaznacz stopniową zmianę a nie jednorazowe przekształcenie.

Główne uczucie powiązane z The Corruptor to bezradność. Bezradność w obliczu transformacji przyjaciela, bezradność w obliczu upiornej sytuacji i niemożność zaatakowania The Corruptor wprost.

### 5. Przykłady

* Imperator Sheev Palpatine ze "Star Wars": on doprowadził do upadku Anakina Skywalkera i przejął kontrolę nad Senatem.
* Saruman z "Władcy Pierścieni": Kiedyś wielki czarodziej, Saruman staje się agentem Saurona i został wypaczony w agenta Ciemności, kaskadując Wypaczenie dalej.

## Spoilers
