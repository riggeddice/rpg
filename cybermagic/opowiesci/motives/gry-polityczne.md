# Motive
## Metadane

* Nazwa: Gry polityczne

## Krótki opis

Chodzi o przetasowania między frakcjami, kto weźmie więcej a kto mniej, kto zajmie dominujące zasoby. Polityka między frakcjami.

## Opis

Chodzi o przetasowania między frakcjami, kto weźmie więcej a kto mniej, kto zajmie dominujące zasoby. Polityka między frakcjami. Próby przejęcia terenu, zajęcia dominującej pozycji, upokorzenie przeciwnika itp. W ramach tego motywu pokaż ruchy dziejące się w tle, angażuj graczy w możliwość wspierania różnych frakcji i pokaż że rzeczy dzieją się w tle i nie chodzi o to co widać ale też rzeczy których nie widać.

## Spoilers
