# Motive
## Metadane

* Nazwa: Katastrofalny Paradoks

## Krótki opis

Przyczyną wszystkiego stało się zaklęcie, które w najgorszy możliwy sposób wyszło poza kontrolę.

## Opis

Przyczyną wszystkiego stało się zaklęcie, które w najgorszy możliwy sposób wyszło poza kontrolę. W ramach tego motywu możesz grać na problematyczności zaklęcia, pokazując charakter maga oraz wyrywając wszystko poza kontrolę. Ten motyw skupia się na naprawie tego, co niefortunnym zaklęciem udało się spieprzyć.

## Spoilers
