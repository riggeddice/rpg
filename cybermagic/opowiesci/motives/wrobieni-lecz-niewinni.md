# Motive
## Metadane

* Nazwa: Wrobieni lecz niewinni

## Krótki opis

Oni tego nie zrobili! Lecz wszystkie dowody są przeciwko nim. Ktoś ich wrobił.

## Opis

Oni tego nie zrobili! Lecz wszystkie dowody są przeciwko nim. Ktoś ich wrobił. W ramach tego motywu pokaż, jak ktoś próbuje się oczyścić z zarzutów lub ktoś próbuje unikać prawa i konsekwencji (nie swoich czynów). Pokaż jak ludzie się odwracają, przyjaciele nadal zostają itp. Ogólnie, pokaż cały ekosystem dookoła tego motywu. KTO ich wrobił i dlaczego? Co dzięki temu ma?

## Spoilers
