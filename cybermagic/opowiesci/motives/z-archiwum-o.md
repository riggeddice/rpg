# Motive
## Metadane

* Nazwa: Z archiwum O

## Krótki opis

Dziwne i niespodziewane wydarzenia w kosmosie z którymi spotyka się Orbiter są tagowane jako 'Z archiwum O'. Przede wszystkim śledztwa, gdzie pojawiło się coś dziwnego.

## Opis

Dziwne i niespodziewane wydarzenia w kosmosie z którymi spotyka się Orbiter są tagowane jako 'Z archiwum O'. Przede wszystkim śledztwa, gdzie pojawiło się coś dziwnego.

## Spoilers
