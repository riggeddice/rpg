# Motive
## Metadane

* Nazwa: Trucizna, opóźniona

## Krótki opis

Ofiara została otruta. Teraz to tylko kwestia czasu jak umrze. Z czasem jest w coraz gorszym stanie.

## Opis

Ofiara została otruta. Teraz to tylko kwestia czasu jak umrze. Z czasem jest w coraz gorszym stanie. W ramach tego motywu pokaż konsekwentne gaśnięcie ofiary, pokaż jak jest coraz słabsza i coraz mniej może.

## Spoilers
