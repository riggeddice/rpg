# Motive
## Metadane

* Nazwa: Walka o jej cześć

## Krótki opis

Ktoś obraził damę albo naraził ją na dyshonor. Cała ta afera po to, by bronić jej honoru.

## Opis

Ktoś obraził damę albo naraził ją na dyshonor. Cała ta afera po to, by bronić jej honoru. Konieczność obrony dobrego imienia.

## Spoilers
