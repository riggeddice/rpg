# Motive
## Metadane

* Nazwa: Spotlight na postać

## Krótki opis

Na tej sesji skupiamy się na jednej konkretnej postaci i chcemy ją pokazać i na niej się skupić.

## Opis

Na tej sesji skupiamy się na jednej konkretnej postaci i chcemy ją pokazać i na niej się skupić. Ten motyw eksploruje postać, jej quirki i interesujące podejście. Pokazuje co odróżnia tą konkretną postać od innych i pozwala jej się wykazać - albo w pozytywny albo negatywny sposób.

## Spoilers
