# Motive
## Metadane

* Nazwa: Competitive game

## Krótki opis

Niektóre gry i turnieje trzeba wygrać. Rozgrywka, w której miejsce nie na podium to powód do smutku. Tu jest ten typ gry.

## Opis

Niektóre gry i turnieje trzeba wygrać. Rozgrywka, w której miejsce nie na podium to powód do smutku. Tu jest ten typ gry. W ramach tego motywu pokaż jak daleko się niektórzy posuną, by wygrać tego typu grę. Występuje tu śmiertelnie ważna rozgrywka, którą ktoś koniecznie chce wygrać. O co toczy się stawka? Tu nie ma "grania dla zabawy" - zwycięży tylko jedna osoba / strona.

## Spoilers
