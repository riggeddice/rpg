# Motive
## Metadane

* Nazwa: Arogancja Młodości

## Krótki opis

Wszyscy mówią, że się nie da. Że to niemożliwe. Ale młody kompetentny agent uważa inaczej. Działa sam, wbrew wszystkim. Co prowadzi do katastrofy.

## Opis

Wszyscy mówią, że się nie da. Że to niemożliwe. Ale młody kompetentny agent uważa inaczej. Działa sam, wbrew wszystkim. Co prowadzi do katastrofy. Czasami jest powód czemu procedury wyglądają tak a nie inaczej. Czasami trzeba podjąć smutne i trudne decyzje. Ten motyw pokazuje, dlaczego. Pokaż konsekwencję tego, że młoda osoba zadziałała wbrew procedurom lub radom starszych wiedząc o nich - i jednak oni mieli rację a młoda osoba nie.

## Spoilers
