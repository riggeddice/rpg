# Motive
## Metadane

* Nazwa: Bunt Maszyn

## Krótki opis

Stworzone przez ludzi narzędzia obracają się przeciwko ludziom, będąc świadome lub nie.

## Opis

Stworzone przez ludzi narzędzia obracają się przeciwko ludziom, będąc świadome lub nie.

W tym motywie pokaż, jak maszyny przestają być narzędziami w naszych rękach - mogą zyskać świadomość i podjąć decyzję o działaniu na własną rękę, ale równie dobrze mogą działać mechanicznie, zgodnie z błędnymi algorytmami lub bez żadnej świadomej intencji.

## Spoilers
