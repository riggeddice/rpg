# Motive
## Metadane

* Nazwa: Złe echo epickiej imprezy

## Krótki opis

Była świetna impreza - alkohol, zabawa, swawole. Ale coś było powiedziane lub coś się tam wydarzyło. I teraz jest problem.

## Opis

Była świetna impreza - alkohol, zabawa, swawole. Ale coś było powiedziane lub coś się tam wydarzyło. I teraz jest problem. Ten motyw skupia się na próbie rozpaczliwego poradzenia sobie z echami tego co wydarzyło się na owej imprezie. Kto zrobił coś głupiego? Jakie mogą być tego konsekwencje? Kto ucierpi przez to? Jak mitygować ten problem?

## Spoilers
