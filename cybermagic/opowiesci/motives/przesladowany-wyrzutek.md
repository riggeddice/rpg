# Motive
## Metadane

* Nazwa: Prześladowany wyrzutek

## Krótki opis

Mieszka na uboczu. Unika normalsów. A jednak próbują się go pozbyć i robić mu kłopoty.

## Opis

Mieszka na uboczu. Unika normalsów. A jednak próbują się go pozbyć i robić mu kłopoty. W ramach tego motywu graj konfliktem 'oni wszyscy' kontra 'wyrzutek'. Pokaż różnice pomiędzy nimi i pokaż jak daleko jest skłonna się posunąć społeczność by wyrzutek sobie poszedł.

## Spoilers
