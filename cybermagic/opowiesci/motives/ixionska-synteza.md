# Motive
## Metadane

* Nazwa: Ixiońska synteza

## Krótki opis

Przez działanie energii ixiońskiej doszło do syntezy pomiędzy niepodobnymi do siebie bytami. Np. w formie transorganiki.

## Opis

Przez działanie energii ixiońskiej doszło do syntezy pomiędzy niepodobnymi do siebie bytami. Np. w formie transorganiki. Energia ixiońska potrafi połączyć rzeczy nie pasujące do siebie, tworząc nienaturalne, obce struktury, bardzo niebezpieczne dla normalnych osób. W ramach tego motywu pokaż nienaturalne struktury ixiońskie i ich wpływ na ludzkie i inne byty - czy to w formie terrorformów czy w formie rzeczy jakie nie powinny nigdy być połączone. Pokaż zarówno nadzieję niesioną przez perfekcję ixiońskiej adaptacji jak i rozpacz wynikającą z tego, że jest to coś czego ludzie nie powinni używać z uwagi na niekontrolowalność energii ixiońskiej.

## Spoilers
