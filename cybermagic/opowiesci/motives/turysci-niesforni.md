# Motive
## Metadane

* Nazwa: Turyści, niesforni

## Krótki opis

Grupa Turystów, którzy nie są na swoim terenie i nie do końca słuchają poleceń. I trzeba jakoś tym wszystkim zarządzać.

## Opis

Grupa Turystów, którzy nie są na swoim terenie i nie do końca słuchają poleceń. I trzeba jakoś tym wszystkim zarządzać. Turyści robią to co zwykle - rozłażą się, ignorują potencjalne zagrożenia, chcą się dobrze bawić (iść za swoją agendą). W ramach tego motywu pokaż różnice kulturowe, różne podejścia jak i wykorzystuj Turystów by generować problemy dla graczy. Turyści to może też być grupa dzieciaków, lub grupa naukowców lub tieni nie pasujący dla danego terenu.

## Spoilers
