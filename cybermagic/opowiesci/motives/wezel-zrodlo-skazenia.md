# Motive
## Metadane

* Nazwa: Węzeł, źródło Skażenia

## Krótki opis

Węzeł, który stanowi szerokie Źródło Skażenia; rozprzestrzenia się. Tworzy efemerydy, przekształca rzeczywistość i Skaża. 

## Opis

Węzeł, który stanowi szerokie Źródło Skażenia; rozprzestrzenia się. Tworzy efemerydy, przekształca rzeczywistość i Skaża. Nieprawdopodobnie trudny do pokonania magicznego, spawnuje nowe efemerydy oraz przekształca rzeczywistość dookoła. W ramach tego motywu pokaż potęgę węzła, wykorzystuj szeroko Skażenie magiczne i pokaż problemy jakie mają ludzie w konfrontacji z czymś tej klasy.

## Spoilers
