# Motive
## Metadane

* Nazwa: Energia Praecis

## Krótki opis

Energia Praecis. Władza nad otoczeniem, ludzka władza nad światem, konstrukcja kontrolująca świat, zrozumienie i kontrola.

## Opis

Energia Praecis. Władza nad otoczeniem, ludzka władza nad światem, konstrukcja kontrolująca świat, zrozumienie i kontrola.

Jest to energia optymistycznej kreacji - przejęcia kontroli nad rzeczywistością, czy to przez zasady, cywilizację czy też technologię. Nieważne co się nie stanie, jako ludzkość będziemy w stanie zbudować narzędzia, dzięki którym to powstrzymamy - jak zawsze do tej pory.

W ramach tego motywu graj tym optymizmem, nieskończoną falą kreacji i determinacji. Ludzka natura, która nie poddaje się tylko próbuje rozbudowywać rzeczywistość, by fundamentalnie ludzkości służyła.

## Spoilers
