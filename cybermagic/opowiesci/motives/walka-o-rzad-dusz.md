# Motive
## Metadane

* Nazwa: Walka o rząd dusz

## Krótki opis

Próba przekonania lub wpłynięcia na grupę lub osobę, by zmienili plan lub poszli za Nowym Przywódcą.

## Opis

Próba przekonania lub wpłynięcia na grupę lub osobę, by zmienili plan lub poszli za Nowym Przywódcą. Grupa lub osoba robi błąd; idzie za Starym Przywódcą, kłóci się, jest gotowa do rozróby itp. Wchodzi on - Nowy Przywódca. I próbuje wpłynąć na grupę lub osobę, by pokazać im że warto iść inaczej, robić coś innego, jeszcze jeden zryw. W ramach tego motywu potraktuj grupę jako przedmiot / zasób i skup się na walce między ideologiami i starciu Nowego Przywódcy ze Starym Przywódcą.

## Spoilers
