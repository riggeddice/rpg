# Motive
## Metadane

* Nazwa: Energia Interis

## Krótki opis

Energia Interis. Pustka wszelkich znaczeń. Niczego nie ma, niczego nie było, niczego nie będzie i nic nie ma znaczenia.

## Opis

Energia Interis. Pustka wszelkich znaczeń. Niczego nie ma, niczego nie było, niczego nie będzie i nic nie ma znaczenia. Nieważne czego nie zrobimy, wszystko się skończy i żadna z naszych decyzji i żaden z naszych ruchów nie ma znaczenia. Jesteśmy tu tylko moment, wobec Ogromu Pustki. A wszechświat się skończy i nie bedzie niczego.

W ramach tego motywu graj Pustką, Ogromem i Brakiem. Pokaż bezsilność ofiar Interis. Pokaż, jak tracą nadzieję. Nie da się zniszczyć Pustki - da się ją tylko wypełnić - ale ogrom Interis sprawia, że to jest niemożliwe. Null and void. Rozpacz, degeneracja, entropia i koniec wszystkiego.

## Spoilers
