# Motive
## Metadane

* Nazwa: Anomalia, Przestrzenna

## Krótki opis

Przestrzeń w obszarze działania tej anomalii nie działa tak jak by się nam wydawało.

## Opis

Przestrzeń w obszarze działania tej anomalii nie działa tak jak by się nam wydawało. Ten motyw skupia się na interakcji Anomalii z postaciami, konsekwencjach istnienia tej Anomalii oraz na sposobach w jakich nie można polegać na tym, że przestrzeń działa tak jak nam się wydaje.

## Spoilers
