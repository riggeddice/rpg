# Motive
## Metadane

* Nazwa: Nieudany Eksperyment

## Krótki opis

Nieudany eksperyment lub produkt - ktoś próbował zrobić coś nowego i coś poszło trochę nie tak. Ogólnie sporo działa, ale ma paskudne efekty uboczne.

## Opis

Nieudany eksperyment lub produkt - ktoś próbował zrobić coś nowego i coś poszło trochę nie tak. Ogólnie sporo działa, ale ma paskudne efekty uboczne. W ramach tego motywu pokaż ogólną przydatność danego bytu, ale podkreśl nieakceptowalne efekty uboczne lub problemy wynikające z niekompletnośi eksperymentu. Co trzeba zapłacić, by to zadziałało?

## Spoilers
