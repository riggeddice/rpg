# Motive
## Metadane

* Nazwa: Zaraza, powoli infekująca

## Krótki opis

Zaraza magiczna lub naturalna, infekująca powoli.

## Opis

Zaraza magiczna lub naturalna, infekująca dość powoli. Może być utajona lub jawna, może mieć efekty na poziomie magicznym lub absolutnie niemagiczne. W ramach tego motywu graj tą zarazą i reakcją ludzi na potencjalną zarazę. Jeśli to coś w stylu Irytki, graj niestabilnością magiczną. Jeśli to coś niebezpiecznego, graj reakcją ludzi i paniką. 

## Spoilers
