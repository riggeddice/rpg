# Motive
## Metadane

* Nazwa: Anomalia, Demoniczna

## Krótki opis

Demoniczna Anomalia aktywnie chce zranić i skrzywdzić ludzi i magów, zadając im trwałe rany duszy.

## Opis

Demoniczna Anomalia aktywnie chce zranić i skrzywdzić ludzi i magów, zadając im trwałe rany duszy. Ten typ anomalii posiada własną agendę i aktywnie poluje na ofiary. By mogła zadziałać, ofiara musi zaakceptować pakt. To nieodwracalnie zmieni ofiarę. Przy tym motywie graj tym co stało się z ofiarami i pokaz niezwykłe okrucieństwo anomalii demonicznej. Pokaż jak przyjaciele próbują ratować ofiarę, ale nie zostało dość do ratowania.

## Spoilers
