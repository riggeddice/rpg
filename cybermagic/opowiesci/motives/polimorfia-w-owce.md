# Motive
## Metadane

* Nazwa: Polimorfia w owcę

## Krótki opis

Ktoś lub coś zostało zmienione w 'owcę' wbrew swojej woli. Nawet jeśli owca jest obiektywnie silna, jest to dla owcy ryzykowne, osłabiające albo szkodliwe.

## Opis

Ktoś lub coś zostało zmienione w 'owcę' wbrew swojej woli. Nawet jeśli owca jest obiektywnie silna, jest to dla owcy ryzykowne, osłabiające albo szkodliwe. W ramach tego motywu konstrastuj aktualną formę owcy z tym, czym owca była. Pokaż smutek owcy i zagrożenia grożące owej owcy. Graj zagrożeniami i morale owcy. Co owcę spotyka w nowej formie i jak to się mapuje na jej poprzednie życie.

## Spoilers
