# Motive
## Metadane

* Nazwa: Wyścig o zasoby

## Krótki opis

Jest ograniczona ilość pewnych zasobów i więcej niż jedna strona próbuje zdobyć jak najwięcej.

## Opis

Jest ograniczona ilość pewnych zasobów i więcej niż jedna strona próbuje zdobyć jak najwięcej. Strony niekoniecznie są skonfliktowane ze sobą, ale wszystkim zależy na pozyskaniu jak największej ilości zasobów. Najpewniej będą podkopywane dołki, sporo dywersji i prób zajęcia nie swojego terenu. W ramach tego motywu graj ograniczonym czasem dostępności zasobu i graj tym, że nie wystarczy zasobu dla wszystkich. Dzięki temu automatycznie buduje się presja i postacie się konfliktują z innymi frakcjami.

## Spoilers
