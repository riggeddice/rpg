# Motive
## Metadane

* Nazwa: Dom w płomieniach

## Krótki opis

Miejsce uważane przez postacie za bezpieczną przystań ulega kataklizmowi.

## Opis

Miejsce uważane przez postacie za bezpieczną przystań ulega kataklizmowi. Panika wśród ludzi, looterzy, coś się pali, coś wybucha, część urządzeń i budynków nie działa prawidłowo, potencjalnie atak z zewnątrz, potencjalnie naruszenie struktury. Tymczasowo nie można temu miejscu ufać.

## Spoilers
