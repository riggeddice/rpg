# Motive
## Metadane

* Nazwa: Presja czasu

## Krótki opis

W ramach Presji Czasu masz do czynienia z sytuacją, gdzie POSTACIE muszą działać szybko. Nie ma czasu na wsparcie, nie ma czasu na planowanie.

## Opis

W ramach Presji Czasu masz do czynienia z sytuacją, gdzie POSTACIE muszą działać szybko. Nie ma czasu na wsparcie, nie ma czasu na planowanie. Nie oznacza to, że GRACZE muszą być pod presją - to zależy od sytuacji i celu. Skup się na tym, by ograniczyć dostęp do zasobów i możliwości bo gracze mają niewiele akcji zanim nie dojdzie do manifestacji problemu.

## Spoilers
