# Motive
## Metadane

* Nazwa: Przynęta, atrakcyjna

## Krótki opis

Bardzo atrakcyjna Przynęta. Coś, co sprawia, że z tego miejsca naprawdę nikt nie chce odchodzić, nieważne jak groźnie.

## Opis

Bardzo atrakcyjna Przynęta. Coś, co sprawia, że z tego miejsca naprawdę nikt nie chce odchodzić, nieważne jak groźnie. Potencjalnie w rękąch kogoś/czegoś, co zastawia pułapkę, ale może też po prostu 'być'.

## Spoilers
