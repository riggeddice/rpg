# Motive
## Metadane

* Nazwa: Czystka etniczna

## Krótki opis

Jakaś grupa społeczna celuje w eksterminację innej grupy społecznej. Albo opuszczą ten teren albo zginą.

## Opis

Jakaś grupa społeczna celuje w eksterminację innej grupy społecznej. Albo opuszczą ten teren albo zginą. W ramach tego motywu pokaż nieracjonalność i czystą nienawiść i strach. To dość ciężki motyw.

## Spoilers
