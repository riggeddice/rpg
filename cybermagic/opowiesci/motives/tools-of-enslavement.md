# Motive
## Metadane

* Nazwa: Tool of Enslavement

## Krótki opis

Narzędzia zniewolenia; odebrania agendy i woli, przekształcenia osoby w bezwolne narzędzie posiadacza. Ciało i/lub wola, nawet dusza...

## Opis

Narzędzia zniewolenia; odebrania agendy i woli, przekształcenia osoby w bezwolne narzędzie posiadacza. Ciało i/lub wola, nawet dusza. Czy to poprzez kontrolę podstawowych instynktów i mechanizmów nagrody i kary, czy to przez przekształcenie umysłu i sposobu działania i myślenia. W ramach tego motywu pokaż potęgę i okrucieństwo tych narzędzi oraz wpływ tego na osoby w otoczeniu. Zarówno na tych którzy próbują się bronić jak i tych, którzy już zostali złamani i ich bliskich. Ten motyw technicznie zabiera _agendę_ zniewolonym postaciom, nawet jeśli jeszcze 'w środku' coś jest.

## Spoilers
