# Motive
## Metadane

* Nazwa: Ukryty anioł stróż

## Krótki opis

Ona jest chroniona. Ktoś nad nią czuwa i próbuje ją osłonić. Nie wiadomo kim jest anioł stróż, który nigdy się nie ujawnia. 

## Opis

Ona jest chroniona. Ktoś nad nią czuwa i próbuje ją osłonić. Nie wiadomo kim jest anioł stróż, który nigdy się nie ujawnia. Z jakiegoś powodu anioł stróż nie może - lub nie chce - ujawnić się osobie, którą chroni. Nie wchodzi z nią w bezpośrednią interakcję tylko patrzy i pilnuje by nic złego się nie stało. Może wspierać osobę chronioną finansowo, ale na pewno dba też o jej bezpieczeństwo fizyczne i by nic złego się jej nie stało. W ramach tego motywu potężnego anioła stróża mającego bezpośrednią relację z osobą, którą chroni. Stwórz też powód, czemu nie powinni się spotkać. Może anioł stróż jest członkiem mafii a osoba chroniona jego zaginionym synem?

## Spoilers
