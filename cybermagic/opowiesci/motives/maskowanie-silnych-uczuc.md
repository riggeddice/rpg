# Motive
## Metadane

* Nazwa: Maskowanie silnych uczuć

## Krótki opis

Ktoś jest pod wpływem stałych silnych uczuć (strach, rozpacz...) i próbuje je ugasić CZYMŚ INNYM, zapełnić pustkę działaniami.

## Opis

Ktoś jest pod wpływem stałych silnych uczuć (strach, rozpacz...) i próbuje je ugasić CZYMŚ INNYM, zapełnić pustkę działaniami. Może oddaje się dekadencji, może próbuje zapełnić pustkę hedonizmem, może rozkazuje za mocno i jest zbyt okrutny. Może pragnie kolekcjonować sztukę? Tak czy inaczej, to co ROBI jest tylko próbą zamaskowania / ugaszenia tego pod jakim jest wpływem i co go stresuje najbardziej.

## Spoilers
