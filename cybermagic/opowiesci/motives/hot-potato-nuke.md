# Motive
## Metadane

* Nazwa: Hot potato nuke

## Krótki opis

Mamy coś klasy 'odbezpieczony granat', co może wybuchnąć. Co z tym zrobić? Komu go wsadzić?

## Opis

Mamy coś klasy 'odbezpieczony granat', co może wybuchnąć. Co z tym zrobić? Komu go wsadzić? W tym motywie spotykamy się z niemożliwym do zwykłego porzucenia czy zniszczenia 'granatem', który trzeba komuś przekazać i komuś fundamentalnie zrobić krzywdę. W ramach tego motywu skup się na konsekwencjach wsadzeniu komuś 'granatu' i/lub na konieczności znalezienia sposobu rozwiązania tego cholernego 'granatu' i szkód jakie może wyrządzić - zanim wybuchnie.

## Spoilers
