# Motive
## Metadane

* Nazwa: Publiczny pojedynek

## Krótki opis

Publiczny pojedynek pomiędzy dwoma godnymi zawodnikami. Emocje sięgają zenitu. Uczciwy czy..?

## Opis

Publiczny pojedynek pomiędzy dwoma godnymi zawodnikami. Emocje sięgają zenitu. Uczciwy czy..? W ramach tego motywu mamy przygotowania do pojedynku, zakłady, działania mające doprowadzić do samego pojedynku i ogólnie rozumiany _buzz_. Pokaż entuzjazm i jak obaj zawodnicy są wspierani przez odpowiednie strony.

## Spoilers
