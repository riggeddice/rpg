# Motive
## Metadane

* Nazwa: Uzasadniona Zemsta

## Krótki opis

Ktoś ucierpiał przez Sprawcę. Teraz ma sposobność i chce się odpłacić na Sprawcy w sposób który Sprawcę zrani.

## Opis

Ktoś ucierpiał przez Sprawcę. Teraz ma sposobność i chce się odpłacić na Sprawcy w sposób który Sprawcę zrani. Zemsta jest siłą, która wypacza obie strony - Ofiarę oraz Sprawcę. W ramach tego motywu pokaż fakt, że zemsta jest uzasadniona i sprawiedliwie jest ją przeprowadzić, jednocześnie pokaż fatalne konsekwencje zemsty. Pokaż jak czyn Sprawcy i zemsta Ofiary stworzyły pętlę, układ, w którym oni są uwikłani do końca świata.

## Spoilers
