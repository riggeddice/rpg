# Motive
## Metadane

* Nazwa: Suicide by cop

## Krótki opis

Ktoś ma dość życia, ale nie wie jak skutecznie popełnić samobójstwo. Ale zrobienie czegoś by policja musiała go zabić powinno działać.

## Opis

Ktoś ma dość życia, ale nie wie jak skutecznie popełnić samobójstwo. Ale zrobienie czegoś by policja musiała go zabić powinno działać. W ramach tego motywu ta osoba spróbuje zdobyć zakładników, zaatakować ważne miejsce itp. Coś, co zmusza policję bądź siły porządkowe do zajęcia się sprawą. A jako, że ta osoba jest uzbrojona i potencjalnie bardzo niebezpieczna dla zakładników lub cywili, policja będzie zmuszona zabić tę osobę.

## Spoilers
