# Motive
## Metadane

* Nazwa: Pozornie w kajdanach

## Krótki opis

Mamy go. Jest uwięziony. Nic nie może zrobić. Ale naprawdę dokładnie o to mu chodziło i jest dokładnie tam gdzie chciał.

## Opis

Mamy go. Jest uwięziony. Nic nie może zrobić. Ale naprawdę dokładnie o to mu chodziło i jest dokładnie tam gdzie chciał. Przeciwnik jest pozornie bezsilny a jednak czuje się komfortowo i czeka. W ramach tego motywu przeciwnik jest dokładnie tam, gdzie chciał i zgodnie z Dark Future zwycięży. To tylko kwestia poczekania. Pokaż umiejętności manipulacji przeciwnika, daj graczom odczuć że coś jest bardzo nie w porządku i buduj słabości które przeciwnik może wykorzystać tylko będąc w środku. Albo odwróć sytuację i spraw by to gracze byli "w pułapce"

## Spoilers
