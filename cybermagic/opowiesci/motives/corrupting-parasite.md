# Motive
## Metadane

* Nazwa: Corrupting Parasite

## Krótki opis

Coś wydaje się przydatnym narzędziem lub wzmocnieniem. Niestety, ten byt lub artefakt jest pasożytem corruptującym użytkownika.

## Opis

Coś wydaje się przydatnym narzędziem lub wzmocnieniem. Niestety, ten byt lub artefakt jest pasożytem corruptującym użytkownika. Użytkownik pasożyta staje się coraz bardziej _inny_, coraz bardziej staje się czymś _mniej_, z uszkodzoną duszą lub człowieczeństwem. W ramach tego motywu pokaż interakcję między pasożytem a użytkownikiem; pokaż zmianę w użytkowniku i pokaż jak działa pasożyt.

## Spoilers
