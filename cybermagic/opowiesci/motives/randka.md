# Motive
## Metadane

* Nazwa: Randka

## Krótki opis

Dwie osoby decydują się na chwilę wytchnienia, nawet jeśli nic z tego miałoby nie być.

## Opis

Dwie osoby decydują się na chwilę wytchnienia, nawet jeśli nic z tego miałoby nie być. W ramach tego motywu pokaż jak postacie (lub jedna postać XD) się starają zrobić dla drugiej coś miłego. Skup się na dialogach, pokaż zwykłą _sympatię_ między postaciami i budujący się między nimi link. Jakie mogą być tego konsekwencje? Jak daleko można pójść? To dość wolny motyw jeśli zawiera postać gracza. Jeśli nie - w wyniku tego, że mamy dwóch Aktorów na randce może sporo rzeczy się spieprzyć (waśnie rodzinne, coś poszło nie tak, mixed signals, teraz się kochamy i uciekamy z domu...)

## Spoilers
