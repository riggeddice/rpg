# Motive
## Metadane

* Nazwa: Konflikt wewnątrzzespołowy

## Krótki opis

W ramach tego motywu wchodzimy 'do środka' grupy - rozgrywamy charaktery postaci między sobą, wewnętrzne konflikty i agendy. Bardziej co jest w środku niż co na zewnątrz.

## Opis

W ramach tego motywu wchodzimy 'do środka' grupy - rozgrywamy charaktery postaci między sobą, wewnętrzne konflikty i agendy. Bardziej co jest w środku niż co na zewnątrz. W grupie mamy różne indywidualności i one się ścierają. Ktoś oszukuje w karty. Ktoś kogoś nie lubi. Ktoś z kimś rywalizuje. Ktoś z kimś śpi. I w ramach tego motywu skupiamy się właśnie na tego typu interakcjach.

## Spoilers
