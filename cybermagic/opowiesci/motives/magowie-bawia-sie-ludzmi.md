# Motive
## Metadane

* Nazwa: Magowie bawią się ludźmi

## Krótki opis

Magowie to ci potężni, homo superior. Magowie bawią się ludźmi jak dzieci zabawkami.

## Opis

Magowie to ci potężni, homo superior. Magowie bawią się ludźmi jak dzieci zabawkami. W ramach tego motywu podkreśl beztroskie podejście do losu ludzi ze strony magów. Pokaż jak magowie niekoniecznie traktują ludzi jak osoby sobie równe. Pokaż też jak zabawki próbują się buntować czy protestować, a magowie traktują to z pobłażaniem i lekkim rozbawieniem.

## Spoilers
