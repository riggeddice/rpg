# Motive
## Metadane

* Nazwa: Ruination Unsealed

## Krótki opis

Wojownik się ogranicza z uwagi na efekt uboczny lub siłę ognia. Wojownik jest autoryzowany do użycia pełnej siły ognia. Ruination Unsealed, pełna moc.

## Opis

Wojownik się ogranicza z uwagi na efekt uboczny lub siłę ognia. Wojownik jest autoryzowany do użycia pełnej siły ognia. Ruination Unsealed, pełna moc. (SOCOM ze Scrapped Princess). W ramach tego motywu pokaż kogoś o standardowej sile ognia i możliwościach, a potem pokaż go na pełnej mocy. Pełna moc zawsze kosztuje - czy to masz ograniczoną kontrolę, czy też ma permanentne szkody i rany, czy też ma ogromne AoE. Co Wojownik musiał zapłacić za pełną moc? A może to kwestia odpowiednika 'ostatecznej broni' i kosztu tej broni na ludziach dookoła?

## Spoilers
