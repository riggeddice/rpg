# Motive
## Metadane

* Nazwa: Wzięcie winy na siebie

## Krótki opis

Z jakiegoś powodu Niewinny bierze na siebie winę za coś, czego nie zrobił.

## Opis

Z jakiegoś powodu Niewinny bierze na siebie winę za coś, czego nie zrobił. Może to kwestia niepoczytalności, magii mentalnej, chęci krycia kogoś czy świadomości że dostanie mniejsze konsekwencje? Tak czy inaczej, w ramach tego motywu podkreśl komplikacje jakie z tego wynikają i jak ciężko jest podążąć za czymś przez to, że oficjalnie winny jest znaleziony.

## Spoilers
