# Motive
## Metadane

* Nazwa: Potwór Scooby Doo

## Krótki opis

Wszystko wskazuje na to, że jest tu potwór. W praktyce, w tle jest koleś przebrany za potwora.

## Opis

Wszystko wskazuje na to, że jest tu potwór. W praktyce, w tle jest koleś przebrany za potwora. Ten delikwent najprawdopobniej ma stealth suit, iluzje, kontrolę nad terenem, magię polimorficzną itp. Z jakiego powodu on przebrał się za potwora? Co chce osiągnąć? Czy w pobliżu jest FAKTYCZNY potwór który wygląda podobnie?

## Spoilers
