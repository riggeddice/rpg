# Motive
## Metadane

* Nazwa: Ukryta Instalacja Wojskowa

## Krótki opis

W okolicy jest coś zamaskowanego. Może tajne więzienie, tajna baza? Tak czy inaczej, wiedza o tym nie jest jawna.

## Opis

W okolicy jest coś zamaskowanego. Może tajne więzienie, tajna baza? Tak czy inaczej, wiedza o tym nie jest jawna. W ramach tego motywu graj tym jak ta instalacja/baza wpływa na otoczenie. Może plotki? Może coś wyciekło? Może ludzie znikają? A może nikt nic nie wie i to jest spokojna okolica nie mająca pojęcia co się dzieje w ukryciu?

## Spoilers
