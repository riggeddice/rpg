# Motive
## Metadane

* Nazwa: Uciekinier, niebezpieczny

## Krótki opis

Więzień lub ścigany uciekinier, który jest bardzo niebezpieczny i zagraża ludziom dookoła.

## Opis

Więzień lub ścigany uciekinier, który jest bardzo niebezpieczny i zagraża ludziom dookoła. W ramach tego motywu pokaż obławę, działania uciekiniera, próby go przechwycenia, polowanie nań, ale też to że jest to mordercza jednostka zdolna do robienia strasznych rzeczy. Pokaż jak obecność uciekiniera zmienia sytuację.

## Spoilers
