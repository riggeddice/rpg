# Motive
## Metadane

* Nazwa: Efemeryda zrodzona z emocji

## Krótki opis

Potężne pole magiczne. Potężne emocje. Narodziny efemerydy, echa emocjonalnego idącego za dominującym impulsem.

## Opis

Potężne pole magiczne. Potężne emocje. Narodziny efemerydy, echa emocjonalnego idącego za dominującym impulsem. Efemeryda odpowiada na bardzo silne emocje i jest ich manifestacją; robi to, co osoba CHCIAŁABY zrobić lub co podświadomie uważa że POWINNO się stać, ale osoba jest nieświadoma obecności efemerydy. W ramach tego motywu graj silnymi emocjami, pokaż bezwzględość i czystość intencji efemerydy i ujawnij mroczne serce osoby która stworzyła nieświadomie efemerydę. 

## Spoilers
