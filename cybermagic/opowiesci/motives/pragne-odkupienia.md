# Motive
## Metadane

* Nazwa: Pragnę odkupienia

## Krótki opis

Ktoś kiedyś zrobił coś bardzo złego - ale próbuje temu zadośćuczynić. Obraca swoje umiejętności i starania by zniwelować to co zrobił.

## Opis

Ktoś kiedyś zrobił coś bardzo złego - ale próbuje temu zadośćuczynić. Obraca swoje umiejętności i starania by zniwelować to co zrobił. Nawet jeżeli nie jest w stanie pomóc tamtym, próbuje zostawić świat lepszym i znaleźć wybaczenie choćby dla siebie w swoim sercu. W ramach tego motywu podkreśl potworność tego co KTOŚ kiedyś zrobił. Pokaż, że był potworem. Ale pokaż też, że w tej chwili jest siłą dobra. Graj na kontraście i stwórz dylemat moralny w sercach graczy.

## Spoilers
