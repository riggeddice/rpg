# Motive
## Metadane

* Nazwa: System - Moloch

## Krótki opis

Moloch the incomprehensible prison! Moloch whose mind is pure machinery! System pożerający The Commons. Dictatorless dystopia. Zbiór bezdusznych zasad, system w których nie ma jednej osoby winnej. Niepokonany, bo nieobecny choć wszechobecny.

## Opis

Moloch the incomprehensible prison! Moloch whose mind is pure machinery! System pożerający The Commons. Dictatorless dystopia. Zbiór bezdusznych zasad, system w których nie ma jednej osoby winnej. Niepokonany, bo nieobecny choć wszechobecny. W ramach tego motywu pokaż SYSTEM TYPU MOLOCH jako przeciwnika. Call Centre, w którym nie ma jednej osoby mogącej cokolwiek zmienić przez zasady. Pokaż bezsilność walki z Molochem, bezduszność Molocha i interakcję ludzi z Molochem.

## Spoilers
