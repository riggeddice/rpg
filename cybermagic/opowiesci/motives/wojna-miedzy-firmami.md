# Motive
## Metadane

* Nazwa: Wojna między firmami

## Krótki opis

Czasem spory gospodarcze przechodzą na wyższą fazę. To ta sytuacja.

## Opis

Czasem spory gospodarcze przechodzą na wyższą fazę. To ta sytuacja. W ramach tego motywu skupiamy się na dwóch firmach lub organizacjach walczących o skonfliktowane zasoby i na konsekwencjach tych sytuacji dla firm i otoczenia.

## Spoilers
