# Motive
## Metadane

* Nazwa: Teren, Skażony

## Krótki opis

Ten teren ma szczególnie dziwne efekty magiczne i nie jest do końca stabilny magicznie, ale sam w sobie nie stanowi szczególnych problemów.

## Opis

Ten teren ma szczególnie dziwne efekty magiczne i nie jest do końca stabilny magicznie, ale sam w sobie nie stanowi szczególnych problemów. Pokaż, że rzeczy działają nie do końca tak jak się spodziewamy, że nie do końca można oczekiwać że normalne zasady rzeczywistości będą działać normalnie.

## Spoilers
