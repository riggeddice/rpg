# Motive
## Metadane

* Nazwa: Stracona nadzieja

## Krótki opis

Ktoś stracił nadzieję i zmienia swoje strategie, obniża swój poziom i zaczyna rezygnować z czegoś w czym jest świetny.

## Opis

Ktoś stracił nadzieję i zmienia swoje strategie, obniża swój poziom i zaczyna rezygnować z czegoś w czym jest świetny. Kluczem jest to, że ktoś bardzo dobry i bardzo skuteczny z jakiegoś powodu rezygnuje. Nie jest dość dobry. Może to młoda tancerka, która marzyła o scenie. Może to stary wojownik, który mówi 'never again'. Graj konfliktem pomiędzy kompetencją a brakiem nadziei i pokaż, jak wszystko co działało z tego powodu zaczyna się powoli sypać.

## Spoilers
