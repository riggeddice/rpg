# Motive
## Metadane

* Nazwa: Don't mess with my people

## Krótki opis

Oficer nie pozwala na to, by jego ludzie ucierpieli. Jako, że ktoś skrzywdził jego ludzi, czas odpowiedzieć zwielokrotnionym cierpieniem.

## Opis

Oficer nie pozwala na to, by jego ludzie ucierpieli. Jako, że ktoś skrzywdził jego ludzi, czas odpowiedzieć zwielokrotnionym cierpieniem. Kto skrzywdził ludzi Oficera? Na czym to polegało? Jak wygląda kontratak Oficera? Czy to odpowiednio nieproporcjonalna i odstraszająca metoda?

## Spoilers
