# Motive
## Metadane

* Nazwa: Walka o miedzę

## Krótki opis

Ten kawałek ziemi jest pomiędzy dwoma sąsiadami. Nie wiadomo dokładnie do kogo należy. Oboje twierdzą, że do nich.

## Opis

Ten kawałek ziemi jest pomiędzy dwoma sąsiadami. Nie wiadomo dokładnie do kogo należy. Oboje twierdzą, że do nich. To może być kawałek ziemi (miedza), drukarka w biurze... to może być całkowicie nieistotny, ale skonfliktowany między dwoma stronami zasób. Wojna może toczyć się od pokoleń (albo: manager przekazuje waśń następcy), obie strony okopały się na swoich stanowiskach i już nie miedza jest ważna a też to by MIEĆ RACJĘ. W tym motywie graj niesamowitym zacietrzewieniem i przekonaniem o konieczności naprawienia Niesprawiedliwości Dziejowej (miedza nie jest w naszych rękach).

## Spoilers
