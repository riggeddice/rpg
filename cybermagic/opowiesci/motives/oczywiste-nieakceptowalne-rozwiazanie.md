# Motive
## Metadane

* Nazwa: Oczywiste nieakceptowalne rozwiązanie

## Krótki opis

Problem da się rozwiązać w oczywisty sposób. Jest to rozwiązanie nieakceptowalne.

## Opis

Problem da się rozwiązać w oczywisty sposób. Jest to rozwiązanie nieakceptowalne. Np. "w szkole na stałe będą glukszwajny" lub "zabijmy dzieciaka który jest kotwicą efemerydy". Pokaż, jak niektóre osoby są zwolennikami tego rozwiązania. W ramach tego motywu stwórz nieakceptowalne rozwiązanie i pokaż konflikt między osobami, które uważają rozwiązanie za mniejsze zło i osobami, które tego nigdy nie zrobią i nie zaakceptują. 

## Spoilers
