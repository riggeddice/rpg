# Motive
## Metadane

* Nazwa: Pojedynek rywali

## Krótki opis

Pojedynek rywali oznacza pojedynek dwóch podobnych umiejętnościami i kompetencjami postaci. Zwycięży tylko jedna.

## Opis

Pojedynek rywali oznacza pojedynek dwóch podobnych umiejętnościami i kompetencjami postaci. Zwycięży tylko jedna. Tym razem nie skupiamy się na publiczności i reagowaniu innych; tym razem skupiamy się na podobieństwach między rywalami i na efektownej walce między nimi. W ramach tego motywu skup się właśnie na demonstracji jak bardzo rywale są do siebie podobni, ale też na tym, jak bardzo różne filozofie i punkty widzenia reprezentują.

## Spoilers
