# Motive
## Metadane

* Nazwa: Klatka na potwora

## Krótki opis

Dzika bestia, anomalny stwór, bądź nienormalnie silny przeciwnik krąży po okolicy. I trzeba go złapać żywcem.

## Opis

Dzika bestia, anomalny stwór, bądź nienormalnie silny przeciwnik krąży po okolicy. I trzeba go złapać żywcem. W tym motywie konfrontujesz graczy z wyzwaniem, jakim jest ujęcie niebezpiecznego stworzenia żywcem - zabicie będzie kontrproduktywne. W ramach motywu podkreśl wyzwanie jakie stanowi złapanie potwora żywcem. Ważne będzie planowanie, przygotowywanie pułapek i wykorzystanie wszystkiego co mają w otoczeniu by złapać tą istotę.

## Spoilers
