# Motive
## Metadane

* Nazwa: Sinful Hedonism

## Krótki opis

Ktoś pragnie jakiejś transgresji dla własnej przyjemności. Jest to niewłaściwe, ale Ktoś i tak dąży by to pozyskać - podstępem lub siłą.

## Opis

Ktoś pragnie jakiejś transgresji dla własnej przyjemności. Jest to niewłaściwe, ale Ktoś i tak dąży by to pozyskać - podstępem lub siłą. Nieważne, jakie będą konsekwencje i kto ucierpi. Ważne, że ktoś osiągnie to czego pragnie. W ramach tego motywu pokaż jak bardzo czyjaś natura uległa uszkodzeniu przez pragnienie transgresji, do czego się posunął, by ową transgresję osiągnąć i jakie były negatywne skutki dla otoczenia.

## Spoilers
