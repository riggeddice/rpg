# Motive
## Metadane

* Nazwa: Sabotaż, niespodziewany

## Krótki opis

Jedna rzecz to sabotaż w warunkach, w których to ma sens ale w TYM WYPADKU nikt nie spodziewałby się sabotażu...

## Opis

Jedna rzecz to sabotaż w warunkach, w których to ma sens ale w TYM WYPADKU nikt nie spodziewałby się sabotażu... Sabotaż jest dziwny czy dlatego że dookoła są sami sojusznicy, czy dlatego że po prostu nie widać celu. W ramach tego motywu graj tym, że tak naprawdę nie wiadomo czemu można ufać. Każdy element sprzętu potencjalnie jest sabotowany. Każda osoba potencjalnie mogła być sabotażystą. I nadal nie znamy agendy - nie wiemy czemu do tego doszło

## Spoilers
