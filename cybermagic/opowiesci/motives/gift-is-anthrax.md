# Motive
## Metadane

* Nazwa: Gift is Anthrax

## Krótki opis

"Mam dla Ciebie prezent. To wąglik." Dar, który okazał się być niebezpieczny i/lub szkodliwy.

## Opis

"Mam dla Ciebie prezent. To wąglik." Dar, który okazał się być niebezpieczny i/lub szkodliwy. Z jakiegoś powodu Darczyńca przekazał Obdarowanemu coś, co okazało się 'wąglikiem'. Czy Darczyńca wiedział co robi i chciał zaszkodzić Obdarowanemu? Czy Obdarowany wierzył, że poradzi sobie z 'wąglikiem' i był świadomy tego co się stało? Ten motyw praktycznie skupia się na interakcji osób z owym 'wąglikiem' i jego konsekwencjach.

## Spoilers
