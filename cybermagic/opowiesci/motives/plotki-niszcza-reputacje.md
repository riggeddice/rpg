# Motive
## Metadane

* Nazwa: Plotki niszczą reputację

## Krótki opis

Ktoś rozpuścił podłą plotkę na temat Ofiary lub jej frakcji. I Ofiara próbuje coś zrobić, by naprawić sytuację.

## Opis

Ktoś rozpuścił podłą plotkę na temat Ofiary lub jej frakcji. I Ofiara próbuje coś zrobić, by naprawić sytuację. W ramach tego motywu pokaż konsekwencje plotki, pokaż uderzenie w reputację, pokaż reakcję Ofiary i pokaż co z tego wynika - walka pomiędzy Ofiarą i tymi co rozpuszczają plotki.

## Spoilers
