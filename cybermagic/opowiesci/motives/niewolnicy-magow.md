# Motive
## Metadane

* Nazwa: Niewolnicy magów

## Krótki opis

Pewna grupa ludzi nie ma wyboru - musi robić to, co magowie sobie życzą. Ich ciało i umysł nie należą naprawdę do nich.

## Opis

Pewna grupa ludzi nie ma wyboru - musi robić to, co magowie sobie życzą. Ich ciało i umysł nie należą naprawdę do nich. W tym motywie podkreśl to, że niewolnicy naprawdę nic nie mogą zrobić i magowie mają nad nimi absolutną kontrolę. Podporządkuj się albo spotka Cię jeszcze gorszy los.

## Spoilers
