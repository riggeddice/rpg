# Motive
## Metadane

* Nazwa: Policja odwraca oczy

## Krótki opis

Z jakiegoś powodu policja / terminusi CZEGOś bardzo stara się nie zauważać.

## Opis

Z jakiegoś powodu policja / terminusi CZEGOś bardzo stara się nie zauważać. Może są przekupieni, może ktoś jest VIPem, może mają swoje biasy czy chcą by komuś się upiekło (bo dziecko). W ramach motywu dojdź do tego czemu dany czyn nie wiąże się z naturalną reakcją policji i pokaż, że to nie jest typowe. Może jest typowe w tym miejscu, ale nie jest typowe ogólnie. Daj przewagę jednej stronie dzięki anomalnemu zachowaniu policji.

## Spoilers
