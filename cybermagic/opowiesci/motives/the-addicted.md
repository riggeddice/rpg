# Motive
## Metadane

* Nazwa: The Addicted

## Krótki opis

"Daj mi więcej...". The Addicted jest istotą, która jest uzależniona i niekompletna bez Narkotyku. 

## Opis
### 1. Przykładowe warianty

Wszystkie warianty The Addicted są połączone z fundamentalną niezgodą do tego czym się stał i jak bardzo nienawidzi tego czym jest, ale nie potrafi się uwolnić od Narkotyku.

* Narkoman, który zniszczył swoją rodzinę i zranił swych bliskich by tylko dostać kolejną dawkę narkotyku.
* Wampir / potwór polujący na ludzi by nakarmić się tym czego potrzebuje, brzydząc się sobą i tym czym się stał.
* Naukowiec obsesyjnie poszukujący zakazanej wiedzy, ignorując wszelkie etyczne granice i ostrzeżenia kolegów.
* Hacker uzależniony od wirtualnej rzeczywistości, który spędza coraz więcej czasu w świecie wirtualnym, zaniedbując swoje realne życie i zdrowie.
* Zdesperowany hazardzista, który ryzykuje wszystkim, co ma, w poszukiwaniu wielkiej wygranej, która odmieni jego życie.
* Artysta obsesyjnie poszukujący inspiracji w ekstremalnych doświadczeniach, tracąc kontakt z rzeczywistością i płacąc zdrowiem.

### 2. Co reprezentuje

"Daj mi więcej...". The Addicted jest istotą, która jest uzależniona i niekompletna bez Narkotyku. The Addicted uosabia dążenie do wypełnienia pustki istnienia za pomocą "Narkotyku" - substancji, doświadczenia, osoby, czy idei, które stają się obsesją. Nieważne kim The Addicted nie jest, ta osoba straciła swoje dawne życie i jej myśli, intelekt i marzenia krążą dookoła Narkotyku. Ta obsesyjna potrzeba wypacza jej osobowość, odbierając ludzkość i przekształcając w coś niekompletnego i zdesperowanego.

The Addicted reprezentowany jest przez następujące aspekty Silnika Konfliktu:

* Poprzednie Życie: Kim był The Addicted zanim wpadł w szpony Narkotyku? Jak rysowała się jego światła przyszłość? Kim mógł zostać i jak by się jego życie potoczyło?
* Narkotyk: Czym jest Narkotyk i jak The Addicted się od niego uzależnił? Czemu zaczął i czemu kontynuuje?
* Bliscy: Kto jest dookoła The Addicted? Kto go wspiera? Kto pragnie jego powrotu do zdrowia? Kto jest jego potencjalną ofiarą?
* Dealer: Kto ma dostęp do Narkotyku i może go zapewnić The Addicted? Czego chce od The Addicted i w jakich okolicznościach mu go dostarczy?
* Serce: Na czym The Addicted zależy? Z czego - mimo uzależnienia - The Addicted jeszcze nie zrezygnował?

### 3. Gdzie jest silnik konfliktu

* Bliscy wierzą, że The Addicted będzie w lepszym stanie, próbując go chronić oraz stabilizować. To się nie stanie.
* The Addicted niszczy wszystko dookoła siebie, kradnąc, kłamiąc i sprzedając by tylko dostać następną dawkę Narkotyku.
* Dealer żąda pieniędzy i Zasobów od The Addicted, który dla Narkotyku zrobi wszystko.
* The Addicted próbuje samemu siebie zatrzymać robiąc samoniszczycielskie ruchy, póki jeszcze jakoś się kontroluje.

### 4. Kiedy wygrywa

The Addicted wygrywa, jeśli uwolni się od Narkotyku albo zapewni sobie nieskończony dostęp do Narkotyku.

* "Już nigdy mi nie zabraknie": The Addicted zapewnia sobie stały dostęp do Narkotyku, który się nie skończy
* "Nigdy więcej. Nigdy.": The Addicted uwolni się od nałogu, znajdzie pomoc i zacznie się rehabilitować
* "Już nie jestem sam": Jeśli Narkotyk jest ideą albo niebezpiecznym memem, to The Addicted wygrywa, jeśli rozprzestrzeni ów mem dalej i nie będzie jedyną obsesyjną istotą
* "Przynajmniej ona jest bezpieczna": Serce jest bezpieczne, zabezpieczone. Nieważne co się nie stanie z The Addicted, Serce jest bezpieczne i uratowane.

### 5. Kiedy przegrywa

The Addicted przegrywa, jeśli Narkotyk go zniszczy albo straci resztkę autonomii jaką posiada.

* "Tylko śmierć jest ucieczką": The Addicted popełnia samobójstwo albo wykonuje samobójczą akcję.
* "The drugs won": The Addicted się poddaje. Odrzuca Serce. Akceptuje, że jest tylko marionetką w rękach Narkotyku.
* "Zniszczyłem wszystko co kochałem": The Addicted traci Serce. Serce ulega Narkotykowi albo staje się splugawione.

### 6. Jak działa

The Addicted jest napędzany desperacką potrzebą zdobycia Narkotyku. Podejmie każdą akcję, niezależnie od konsekwencji moralnych czy społecznych. Jego działania są irracjonalne, często niebezpieczne zarówno dla niego, jak i dla otoczenia. 

Może manipulować, kłamać, walczyć - zrobi WSZYSTKO - aby osiągnąć swoje cele. W miarę jak uzależnienie narasta, granice, których nie przekroczy, stają się coraz mniej wyraźne.

### 7. Jak wykorzystywać ten motyw

Skoncentruj się na przedstawieniu tragicznej natury uzależnienia i jego wpływu na jednostkę oraz jej otoczenie. Pokaż wewnętrzny konflikt The Addicted - pokaż resztkę szlachetności która została pod kontrolowanymi przez zniekształcone impulsy istotą. Pokaż konflikt The Addicted, jego walkę z pragnieniem zerwania z nałogiem oraz nieustającą porażką w tej walce. Pokaż, że nie zawsze The Addicted jest winny tego co mu się stało. Pokaż, czym The Addicted mógł być. Pokaż jak daleko się posunie. Pokaż, że posuwa się coraz dalej.

Pokaż, jak bardzo The Addicted nie chce być tym czym jest, ale nie ma już szczególnie wyboru. Pokaż, jak obsesja zmienia percepcję świata i jego samego. Pokaż też ból i rozpacz bliskich, którzy są bezsilni wobec spirali zagłady dotykającej The Addicted.

### 8. Jakie ma ruchy

* 1 - Ukrywanie Narkotyków i uzależnienia ŻEBY uspokoić otoczenie i Bliskich i zredukować podejrzenia
  * sukces: The Addicted jest traktowany normalnie w społeczeństwie, nie jest obserwowany ani nie ma interwencji, Bliscy się uspokajają
  * przykład: uzależniony od krwi wampir próbuje zachowywać się jak zwykły człowiek, by nikt nie zwracał nań uwagi i zrzuca z siebie podejrzenia
* 2 - Brutalne przestępstwo ŻEBY pozyskać Narkotyk lub zadowolić Dealera
  * sukces: The Addicted zrobił podły czyn, dzięki czemu pozyskał dawkę Narkotyku lub spłacił dług wobec Dealera
  * przykład: napad na kogoś by zabrać mu pieniądze, za co będzie mógł kupić Narkotyk
* 3 - Zniszczenie czegoś w szale ŻEBY odepchnąć od siebie Bliskich (ochronić ich) i zregenerować stan psychiczny
  * sukces: The Addicted jest stabilniejszy psychicznie, Bliscy się doń niebezpiecznie nie zbliżają
  * przykład: uzależniona od komputera dziewczyna krzyczy i niszczy swój pokój, by rozładować stan emocjonalny i pokazać że nie jest OK.
* 4 - Kłamstwo i manipulacja że potrzebuje Zasobów ŻEBY naprawdę pozyskać nową dawkę Narkotyku
  * sukces: The Addicted wysysa od Bliskich Zasoby, by za nie pozyskać Narkotyk
  * przykład: "miałem wypadek, ale jeśli teraz dam 500 zł, nie będę aresztowany, PROSZĘ!"
* 5 - Bardzo ryzykowna akcja nie dbająca o siebie, by dostać się gdzieś gdzie może zdobyć Narkotyk ŻEBY zapewnić sobie źródło
  * sukces: The Addicted dostaje się gdzieś, gdzie Narkotyk się znajduje lub może go sobie zapewnić
  * przykład: Artysta uzależniony od emocjonujących zdarzeń prześlizguje się na bal maskowy mafii, nie dbając o konsekwencje
* 6 - Próba samoleczenia w ukryciu ŻEBY pozbyć się uzależnienia od Narkotyku, ze strasznymi efektami ubocznymi
  * sukces: The Addicted jest trochę mniej kontrolowany przez Narkotyk, acz z ogromnym kosztem psychicznym i fizycznym
  * przykład: Naukowiec uzależniony od zakazanej wiedzy niszczy swoje badania i odcina się od środowiska, które nakręca jego obsesję.
* 7 - Obsesyjne poszukiwanie zamienników Narkotyku ŻEBY znaleźć coś mniej szkodliwego
  * sukces: The Addicted może zmienić Dealera i Narkotyk, The Addicted spowolnił swoje uzależnienie
  * przykład: Wampir, który zamiast polować na ludzi, zaczyna poszukiwać krwi zwierzęcej lub sztucznych zamienników
* 8 - Ruch niszczycielski który zostaje zatrzymany w ostatniej chwili ŻEBY uratować Serce przed Narkotykiem
  * sukces: The Addicted wykonywał koszmarny ruch, ale go przerwał. Akcja "pusta".
  * przykład: "Jeśli dostarczysz mi swoją ukochaną siostrę, dostaniesz dawkę". Już ją prowadził, ale zmienił zdanie. Nie zrobił tego.

### 9. Przykłady

* Gollum z "Władcy Pierścieni": Gollum i jego obsesja na punkcie Pierścienia
* Wampir z fikcji, przemieniony wbrew swojej woli; nie potrafi się powstrzymać przed ssaniem krwi, ale nienawidzi tego czym się stał
