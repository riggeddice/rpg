# Motive
## Metadane

* Nazwa: Dziwna Zmiana Zachowania

## Krótki opis

Ktoś zachowywał się w pewien sposób. Nagle zachowuje się inaczej, w sposób który nie pasuje do tej osoby. Coś się zmieniło.

## Opis

Ktoś zachowywał się w pewien sposób. Nagle zachowuje się inaczej, w sposób który nie pasuje do tej osoby. Coś się zmieniło. W ramach tego motywu użyj zmiany zachowania albo jako powolna zmiana albo jako demonstracja że dzieje się coś bardzo dziwnego i niewłaściwego. To potencjalnie dobry motyw typu plot-point.

## Spoilers
