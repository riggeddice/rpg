# Motive
## Metadane

* Nazwa: Klątwożyt

## Krótki opis

Jak pasożyty, ale dotyczą energii magicznych i magów; byty magiczne pasożytujące na magach i ich energiach.

## Opis

Jak pasożyty, ale dotyczą energii magicznych i magów; byty magiczne pasożytujące na magach i ich energiach. Osoby podległe klątwożytom mogą zachowywać się inaczej lub ich magia może nie działać prawidłowo. W ramach tego motywu pokaż wpływ klątwożyta na swojej ofierze i na jej otoczeniu. Określ z jakiego typu klątwożytem masz do czynienia i odpowiednio zadziałaj.

## Spoilers
