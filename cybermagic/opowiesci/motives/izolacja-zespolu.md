# Motive
## Metadane

* Nazwa: Izolacja zespołu

## Krótki opis

Z jakiegoś powodu Zespół nie ma kontaktu z nikim poza sobą samym. Jest skazany na siebie i zdany tylko na siebie.

## Opis

Z jakiegoś powodu Zespół nie ma kontaktu z nikim poza sobą samym. Jest skazany na siebie i zdany tylko na siebie. Ten motyw dramatycznie zmniejsza ilość surowców dostępnych zespołowi, nie tylko w formie znajomości i komunikacji ale też typowych narzędzi i wsparcia frakcji. W ramach tego motywu podkreślaj niedostępność typowych technik i narzędzi, buduj konieczność korzystania z tego co zespół ma pod ręką i tymczasowych zasobów oraz podkreślaj całkowitą samotność i możliwość polegania tylko na sobie.

## Spoilers
