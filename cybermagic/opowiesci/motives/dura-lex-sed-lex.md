# Motive
## Metadane

* Nazwa: Dura lex, sed lex

## Krótki opis

Bezwzględne prawo którego należy przestrzegać z uzasadnionych powodów, niezależnie od tego jaką krzywdę wyrządzamy.

## Opis

Bezwzględne prawo którego należy przestrzegać z uzasadnionych powodów, niezależnie od tego jaką krzywdę wyrządzamy. Pokaż konflikt między prawem a człowieczeństwem. Pokaż przyczynę, czemu prawo musi być przestrzegane, ale pokaż też czemu w tym jednym wypadku nie jest to najlepszą opcją. W ramach tego motywu graj konsekwencjami twardego prawa i zmuś graczy do skonfrontowania się z decyzją: prawo czy to co dobre.

## Spoilers
