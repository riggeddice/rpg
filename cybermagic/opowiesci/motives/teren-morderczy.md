# Motive
## Metadane

* Nazwa: Morderczy teren

## Krótki opis

Bez servarów czy pojazdu postacie nie są w stanie przetrwać. Teren na którym się znajdują aktywnie próbuje wszystkich zabić.

## Opis

Bez servarów czy pojazdu postacie nie są w stanie przetrwać. Teren na którym się znajdują aktywnie próbuje wszystkich zabić. Tu nie chodzi o "po prostu bardzo trudny teren" - tu chodzi o teren aktywnie złowrogi. Australia z memów. Teren nie jest po prostu trudny, ma aktywne hazardy tylko czekające aż ktoś się pomyli i zrobi błąd. W ramach tego motywu podkreślaj straszny teren, zaznaczaj ślady po poprzednich osobach które zostały pokonane przez ten teren i aktywnie zagrażaj wszystkim aktorom poruszającym się w tym terenie.

## Spoilers
