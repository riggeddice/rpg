# Motive
## Metadane

* Nazwa: The Gatekeeper

## Krótki opis

The Gatekeeper reprezentuje byt, który jest strażnikiem uniemożliwiającym komuś wejście na dany obszar (lub zdanie danego testu).

## Opis

The Gatekeeper reprezentuje byt, który jest strażnikiem uniemożliwiającym komuś wejście na dany obszar (lub zdanie danego testu). The Gatekeeper jest nie tylko fizyczną barierą, ale także testem dla tych, którzy próbują przekroczyć zakazaną granicę. Może używać zagadek, fizycznych wyzwań, lub działań mentalnych, by jedynie godne osoby mogły przekroczyć próg. W ramach tego motywu graj wartością terenu chronionego przez The Gatekeeper i tym konfliktem między 'godny - niegodny'.

The Gatekeeper nie jest tylko zwykłym 'potworem'. Będzie doskonale kontrolować teren i atakować zarówno na poziomie fizycznym, mentalnym jak i emocjonalnym, by tylko zapewnić by jedynie godne osoby mogły się dostać tam gdzie powinny. The Gatekeeper może służyć jako ostateczna przeszkoda lub jako mechanizm upewniający się, że tylko prawidłowe osoby mogą osiągnąć to czego The Gatekeeper strzeże.

Kluczowe jest, aby wykorzystywać ten motyw tak, aby gracze czuli ciągłą obserwację i ewaluację. Czy są godni? Czy 'ci inni' są godni? The Gatekeeper może nawet nie być potężną istotą i może być cieniem wymagającym szacunku dla starych zasad, ale może też być potężnym bytem mającym dostęp do czegoś czego gracze potrzebują.

## Spoilers
