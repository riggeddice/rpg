# Motive
## Metadane

* Nazwa: Strach ma wielkie oczy

## Krótki opis

Strach ma wielkie oczy. Wydawało się, że TO będzie straszne i potworne. W praktyce - nie jest. To tylko COŚ.

## Opis

Strach ma wielkie oczy. Wydawało się, że TO będzie straszne i potworne. W praktyce - nie jest. To tylko COŚ. Pokaż różnicę pomiędzy tym co się danej osobie wydawało a tym co się stało naprawdę. Pokaż, że to nigdy nie było takie straszne lub że już teraz nie jest takie straszne. Może ktoś kto był terminatorem po prostu się zestarzał. Może ty stałeś się silniejszy. Może potwór nigdy nie był taki straszny jak się wydawało. Tak czy inaczej - pozorny koszmar okazał się być zwykłym ‘czymś’.

## Spoilers
