# Motive
## Metadane

* Nazwa: Dark Technology

## Krótki opis

Zakazana, niebezpieczna technologia lub komponent, używany w jakimś celu. Czy warto było jej użyć?

## Opis

Zakazana, niebezpieczna technologia lub komponent, używany w jakimś celu. Czy warto było jej użyć? W ramach tego motywu pokaż dylematy wynikające z asocjacji z osobami używającymi tego typu technologii, dylematy czy warto użyć wiedząc o potencjalnych ryzykach czy gdy już coś pójdzie nie tak - pokaż JAK poszło nie tak i jakie były tego paskudne efekty uboczne.

## Spoilers
