# Motive
## Metadane

* Nazwa: Pułapka, bardzo skomplikowana

## Krótki opis

Konstrukcja, przygotowanie lub manifestacja nieprawdopodobnie złożonej i skomplikowanej pułapki która zupełnie na pułapkę nie wygląda.

## Opis

Konstrukcja, przygotowanie lub manifestacja nieprawdopodobnie złożonej i skomplikowanej pułapki która zupełnie na pułapkę nie wygląda. Najprawdopodobniej kilkunastoetapowa, skomplikowana pułapka która jest bardzo skuteczna i bardzo droga. Czemu nie dało się tego zrobić inaczej? Co wymaga tak złożonej pułapki? W ramach tego motywu skup się zarówno na kosztach jak i na działaniu pułapki, skup się na konieczności pozyskaniu komponentów jak i na budowaniu iluzji że to pułapką nie jest. Pokaż koszt na wszystkich wynikający ze stawiania tak złożonej pułapki. I docelowo ją odpal w niesamowicie efektowny sposób ;-).

## Spoilers
