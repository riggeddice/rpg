# Motive
## Metadane

* Nazwa: Energia Esuriit

## Krótki opis

Energia Esuriit. Nieskończony Głód, niepohamowana ambicja korodująca duszę. Niekończąca się eksploatacja. Uzależnienie. Wypaczenie, monomania, obsesja.

## Opis

Energia Esuriit. Nieskończony Głód, niepohamowana ambicja korodująca duszę. Niekończąca się eksploatacja. Uzależnienie. Wypaczenie, monomania, obsesja. Ambicja - im więcej osiągniesz tym bardziej pragniesz jeszcze więcej. Nienawistny głód, w wyniku którego nie postrzegasz rzeczywistości jaką jest, tylko jako coś co należy pożreć.

W ramach tego motywu podkreśl korozyjny wpływ energii Esuriit. Pokaż miłość zmieniającą się w nienawiść. Pokaż dzieci pożerające rodziców. Pokaż upadek duszy. Pokaz energię w taki sposób by gracze nigdy nie chcieli jej używać – bo Esuriit – reprezentujące Pożądanie i Nienawistny Głód - zawsze wygrywa z racjonalnym ludzkim umysłem. Pytanie tylko „kiedy”.

## Spoilers
