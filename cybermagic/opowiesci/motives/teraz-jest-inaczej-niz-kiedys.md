# Motive
## Metadane

* Nazwa: Teraz jest inaczej niż kiedyś

## Krótki opis

Sytuacja się zmieniła. KIEDYŚ coś było dobrym rozwiązaniem, TERAZ już nie jest. KIEDYŚ coś działało, TERAZ już nie.

## Opis

Sytuacja się zmieniła. KIEDYŚ coś było dobrym rozwiązaniem, TERAZ już nie jest. KIEDYŚ coś działało, TERAZ już nie. Może kiedyś nie było innego wyjścia i trzeba było płaszczyć się przed mafią a teraz już nie? Może kiedyś Esuriit było jedyną opcją i przestało być? Może stare metody przestały działać? Tak czy inaczej - sytuacja się zmieniła ale nie wszyscy to jeszcze widzą / nikt tego jeszcze nie widzi.

## Spoilers
