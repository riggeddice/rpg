# Motive
## Metadane

* Nazwa: Oswajanie uciekiniera

## Krótki opis

Zosia-samosia. Uciekinier, który 'nie potrzebuje nikogo i poradzi sobie sam'. I próba oswojenia owego uciekiniera.

## Opis

Zosia-samosia. Uciekinier, który 'nie potrzebuje nikogo i poradzi sobie sam'. I próba oswojenia owego uciekiniera. Pokaż, jak uciekinier nikomu nie ufa i chce być maksymalnie niezależny. W ramach tego motywu graj konfliktem między uciekinierem i impulsem 'zostawcie mnie' i próbami pomocy uciekinierowi. Czemu uciekinier uciekł? Co się tu stało? Kto chce go oswoić i dlaczego? Jakie korzyści ma z tego uciekinier, jakie oswajający?

## Spoilers
