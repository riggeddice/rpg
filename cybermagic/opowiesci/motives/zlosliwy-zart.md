# Motive
## Metadane

* Nazwa: Złośliwy żart

## Krótki opis

Dzieje się coś bardzo absurdalnego i to jest wina Rywala albo Trickstera.

## Opis

Dzieje się coś bardzo absurdalnego i to jest wina Rywala albo Trickstera. Cała sytuacja jest spowodowana przez KOGOŚ i teraz my musimy z tym żyć. W ramach tego motywu nie musisz zdradzać, że przyczyną jest żart. Ważne, że ta zabawna i absurdalna sytuacja jest spowodowana przez inteligentną agendę i jest zrobiona 'dla zabawy' lub by zrobić z tego filmik i dostać 100 lajków. W tym motywie wzmocnij uciążliwość i absurdalność.

## Spoilers
