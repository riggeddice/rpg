# Motive
## Metadane

* Nazwa: Głupota i bezmyślność

## Krótki opis

Przyczyną problemów była typowa ludzka głupota i bezmyślność. Coś, co nie musiało zaistnieć, ale niestety... ludzie to ludzie.

## Opis

Przyczyną problemów była typowa ludzka głupota i bezmyślność. Coś, co nie musiało zaistnieć, ale niestety... ludzie to ludzie. Pokaż, jak wszystkiego dało się uniknąć, gdyby tylko nie głupie ruchy ludzi które nie musiałyby być głupie. Nie pomyśleli, nie zależało im...

## Spoilers
