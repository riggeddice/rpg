# Motive
## Metadane

* Nazwa: Rekonstrukcja życia

## Krótki opis

Z jakiegoś powodu komuś się rozpadło życie. Teraz ktoś próbuje to wszystko naprawić.

## Opis

Z jakiegoś powodu komuś się rozpadło życie. Teraz ktoś próbuje to wszystko naprawić. Nie jest to proste, bo wpływa to na tą osobę, jej przeszłość jak i otoczenie społeczne. Jako przykład - ktoś przeszedł przez Skażenie i jest inny. Albo ktoś się stoczył i próbuje zacząć od nowa. Albo ktoś był przestępcą i próbuje zacząć gdzieś indziej. W ramach tego motywu spójrz na osobę której rozpadło się życie, osoby próbujące to życie zrekonstruować jak i wpływ wszystkiego na otoczenie.

## Spoilers
