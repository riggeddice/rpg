# Motive
## Metadane

* Nazwa: Hartowanie Verlenów

## Krótki opis

Słaby Verlen jest niegodny. Verlena trzeba wzmocnić lub złamać.

## Opis

Słaby Verlen jest niegodny. Verlena trzeba wzmocnić lub złamać. Ten motyw skupia się na działaniach radykalnych odłamów Verlenlandu, które dążą do maksymalnego zahartowania i wzmocnienia Verlenów. Ciągłe testowanie, wrzucanie na głęboką wodę, śmiech podczas cierpienia. Unikalne cechy Verlenów dających im szansę na osiągnięcie Legendy o jakiej marzą.

## Spoilers
