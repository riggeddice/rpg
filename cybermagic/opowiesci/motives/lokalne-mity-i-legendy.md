# Motive
## Metadane

* Nazwa: Lokalne mity i legendy

## Krótki opis

Każde miejsce ma swoje lokalne mity i legendy. Na tej sesji stają się istotne.

## Opis

Każde miejsce ma swoje lokalne mity i legendy. Na tej sesji stają się istotne. Czasami są to opowieści o potworach. Czasami coś co wydarzyło się w przeszłości. Czasem tajemnicza Atlantyda. Są to unikalne mity i legendy tego konkretnego miejsca.

## Spoilers
