# Motive
## Metadane

* Nazwa: Stopniowe tracenie siebie

## Krótki opis

Z jakiegoś powodu ktoś jest przeklęty. Stopniowo traci siebie, swoją naturę i swoją osobowość. Jest coraz mniej sobą. To nieodwracalny (?), stopniowy proces.

## Opis

Z jakiegoś powodu ktoś jest przeklęty. Stopniowo traci siebie, swoją naturę i swoją osobowość. Jest coraz mniej sobą. To nieodwracalny (?), stopniowy proces. W ramach tego motywu pokaż kogoś, kto z przyczyn (choroba, klątwa, narkotyki, artefakt, korupcja, forced-cyborgization) powoli przestaje być sobą. Pokaż, jak walczy nowa natura ze starą osobowością. Pokaż, że coraz mniej walczy - że coraz bardziej nowa forma wygrywa. Pokaż potworność nowej formy, nowej duszy.

## Spoilers
