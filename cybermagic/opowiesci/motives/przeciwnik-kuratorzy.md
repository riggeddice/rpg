# Motive
## Metadane

* Nazwa: Przeciwnik: Kuratorzy

## Krótki opis

Na tej sesji występują Kuratorzy, samoreplikujący 'ratownicy' ludzkości

## Opis

Na tej sesji występują Kuratorzy, samoreplikujący filantropiści ludzkości. Kuratorzy i Żywe Aleksandrie są bardzo niebezpiecznym przeciwnikiem - każda osoba, która trafi do Aleksandrii zasila Kuratorów wiedzą. Do tego Kuratorzy mają replikatory i fabrykatory. Apex technical / psychotronic enemy.

## Spoilers
