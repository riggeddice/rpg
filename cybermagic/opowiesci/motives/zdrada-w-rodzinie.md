# Motive
## Metadane

* Nazwa: Zdrada w rodzinie

## Krótki opis

Brat obraca się przeciwko bratu. Nawet najbliżsi Cię zdradzą.

## Opis

Brat obraca się przeciwko bratu. Nawet najbliżsi Cię zdradzą. Czy to kwestia zdrajcy na statku kosmicznym, w firmie czy brata z własnej krwi - gramy tu na tym, że komuś naprawdę zaufaliśmy i pomogliśmy, był 'jak brat' a jednak zdradził. W ramach tego motywu spowoduj problem wynikający ze zdrady. Pokaż, że dla niskich pobudek zdrajca zaryzykował wszystko, o co wspólnie walczyliście.

## Spoilers
