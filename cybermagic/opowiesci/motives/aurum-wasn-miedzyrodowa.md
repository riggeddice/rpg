# Motive
## Metadane

* Nazwa: Aurum - waśń międzyrodowa

## Krótki opis

Rody Aurum często kopią dołki pod innymi rodami. Tego się nie zapomina...

## Opis

Rody Aurum często kopią dołki pod innymi rodami. Tego się nie zapomina... Ten wątek skupiony jest dookoła co najmniej dwóch rodów, gdzie jeden ród robi coś drugiemu z uwagi na przewinę prawdziwą lub wyimaginowaną. Waśń i działanie mogą być nieproporcjonalnie wysokie albo adekwatne. Ważnym jest to, że w wyniku tego typu waśni nie dochodzi do trwałego ucierpienia żadnego maga rodu.

## Spoilers
