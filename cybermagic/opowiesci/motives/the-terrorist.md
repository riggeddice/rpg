# Motive
## Metadane

* Nazwa: The Terrorist

## Krótki opis

"Wasze cierpienie zmieni świat na lepsze." The Terrorist próbuje doprowadzić do zmian politycznych przemocą i terrorem.

## Opis

### 1. Co reprezentuje

"Wasze cierpienie zmieni świat na lepsze." The Terrorist próbuje doprowadzić do zmian politycznych przemocą i terrorem. Nie ogranicza się do fizycznego niszczenia; jego celem jest zaszczepienie lęku i niepewności, co ma zmusić społeczeństwo lub rząd do podjęcia konkretnych działań lub zaniechania pewnych praktyk. Działa z przekonaniem, że cel uświęca środki, a przemoc jest nie tylko usprawiedliwiona, ale i konieczna.

### 2. Kiedy wygrywa

The Terrorist wygrywa, gdy jego działania skutkują realną zmianą polityczną, społeczną lub ekonomiczną.

Cele cząstkowe:

* jak największe nagłośnienie jego działań
* rosnąca chęć spełniania jego żądań i presja polityczna / rosnący terror i dostosowanie się do woli The Terrorist
* ustępstwa ze strony władz, zmiana przepisów, podniesienie świadomości, destabilizacja i chaos

### 3. Jak działa

The Terrorist często działa w ukryciu, stosując taktykę zaskoczenia. Kieruje swoje ruchy przeciwko ludności cywilnej i skupia się na efekcie szokowym - im więcej ludzi jest przerażonych, tym lepiej. Może korzystać z bomb, ataków cybernetycznych, porwań, zamachów itp. Działania te są zwykle dobrze zaplanowane i mają na celu wywołanie maksymalnego rozgłosu.

The Terrorist może działać samodzielnie lub jako część większej grupy. Kluczem do działania The Terrorist jest bardzo dobre zdefiniowanie celów do osiągnięcia.

### 4. Jak wykorzystywać ten motyw

Pokaż dynamikę strachu - jeśli NIE WIESZ o terroryście, jest fundamentalnie niegroźny. Jeśli ludzie by akceptowali ryzyko, jest fundamentalnie niegroźny. To reakcja ludzi na The Terrorist daje mu siłę. Pokaż też pętlę narastającej przemocy - "zemsta rodzi zemstę". Warto skupić się tu na konsekwencjach moralnych działań terrorystycznych i na ranach psychicznych generowanych przez terroryzm wobec ofiar.

Skup się na motywacjach i oddaniu które kierują The Terrorist. Pokaż dylematy i wybory, przed którymi stają ofiary, władze i sami terroryści. Prezentuj także konsekwencje ich działań zarówno dla nich samych, jak i dla otaczającego ich świata. "Kto ogniem walczy, w ogniu płonie jego rodzina" czy jakoś tak.

### 5. Przykłady

* "V for Vendetta": przemowy, wysadzania budynków itp. aby obalić tyranię i inspirować ludzi do walki o wolność
* Just Stop Oil itp.: radykalne ruchy ekologiczne destabilizujące struktury by wymusić zmiany polityczne
