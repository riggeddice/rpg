# Motive
## Metadane

* Nazwa: The Hopeless

## Krótki opis

"Wszystko stracone...". The Hopeless działa z obowiązku w obliczu Apokalipsy, próbując zminimalizować cierpienie swoje i innych, nie wierząc w żaden sukces.

## Opis

### 1. Co reprezentuje

"Wszystko stracone...". The Hopeless działa z obowiązku w obliczu Apokalipsy, próbując zminimalizować cierpienie swoje i innych, nie wierząc w żaden sukces. Zmiana biegu wydarzeń jest niemożliwa, więc The Hopeless z miłości oraz obowiązku próbuje znaleźć ukojenie idąc mroczną ścieżką. To wyraz akceptacji najgorszego możliwego scenariusza i próba znalezienia w nim odrobiny godności czy ulgi. Kluczowe do zrozumienia jest to, że głównymi motywatorami The Hopeless są miłość oraz obowiązek.

Jest to ostateczny los większości istot dotkniętych przez Interis. "Niszczę by chronić".

### 2. Kiedy wygrywa

The Hopeless wygrywa, gdy jego działania skutkują zmniejszeniem cierpienia, nawet jeśli jest to kosmetyczna poprawa wobec ogromu tragedii. W końcu - w innym wypadku by popełnił samobójstwo. 

* Jeśli The Hopeless ma swoich bliskich jako zakładników Apokalipsy, będzie działał, by móc się do nich zbliżyć by zakończyć ich cierpienie.
* Jeśli The Hopeless jest członkiem grupy którą bardzo ceni, będzie działał, by przy minimalnym cierpieniu wszyscy mogli zginąć, nie czując strachu czy braku nadziei. 
* Jeśli The Hopeless uważa, że może umrzeć z godnością, po wykonaniu zadania.

### 3. Jak działa

The Hopeless działa w oparciu o przekonanie, że wszystko stracone, ale mimo to próbuje złagodzić cierpienie wokół siebie. Działania są przesiąknięte poczuciem konieczności i fatalizmu. Wybiera mniejsze zło, podejmując często okrutne i nieetyczne decyzje, które jednak prowadzą do redukcji cierpienia lub podążania Mroczną Drogą.

Czasami The Hopeless może pęknąć. Wtedy zaczyna wpadać w berserk, w szaleństwo. Pokonany silnymi emocjami może zrobić coś głupiego - The Hopeless fundamentalnie **nie chce istnieć**, ale nie może przestać działać póki nie osiągnie swojego celu i nie wykona zadania.

The Hopeless zwykle jest w pionie dowódczym - zwykle dowodzi ludźmi albo jest osobą na której polegają inni (np. jest kapłanem, ważnym urzędnikiem czy mężem stanu). Nie ma w nim dawnej charyzmy, została jedynie pusta ludzka powłoka wykonująca ostatni rozkaz. The Hopeless może organizować plany ewakuacji, tworzyć schronienia dla prześladowanych lub, w skrajnych przypadkach, przeprowadzać akty eutanazji, by oszczędzić innym długiego cierpienia.

### 4. Jak wykorzystywać ten motyw

Koniecznie zwróć uwagę na to, jak rozpacz i brak nadziei wypacza człowieka. Pokaż fatalizm. Pokaż, jak fundamentalnie dobra i kompetentna osoba która stała się The Hopeless wykorzystuje swoje talenty do efektywnego dążenia do celu którego nie pragnie, ale nie widzi innej drogi. Pokaż, że nawet, jeśli uda się naprawić sytuację - dla The Hopeless jest już za późno. Bo to, co The Hopeless zrobił jest zaprzeczeniem wszystkiego w co wierzył.

Ogólnie, skup się na tragedii osoby która ulega Przeznaczeniu lub Apokalipsie, idąc drogą, którą nikt nie chce podążać. Podkreśl wewnętrzny konflikt między dawnymi przekonaniami a fatalistycznym przyjęciem rzeczywistości.

### 5. Przykłady

* John Frobisher, Torchwood: organizował działania rządu by wydać dzieci w obliczu kosmitów; zabił swoje, by nie spotkał ich podobny los
* Wanda Firebaugh, Erfworld: najpierw walczyła z przeznaczeniem, potem po torturach poszła w realizację Proroctwa, kosztem wielkich osobistych i moralnych strat.
* Nienazwana pani kapitan z cyklu "Bolo" Laumera / Webera: zbierała zdrowych ludzi z planet, rozdzielając rodziny siłą i gdy dotarła do zdrowej planety wszystkich tu zostawiła i się zastrzeliła.
