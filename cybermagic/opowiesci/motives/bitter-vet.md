# Motive
## Metadane

* Nazwa: Bitter Veteran

## Krótki opis

Starzy, doświadczeni weterani którzy wiele razy dostali w kość i nie mają entuzjazmu do KOLEJNEJ rzeczy która na nich spadnie.

## Opis

Starzy, doświadczeni weterani którzy wiele razy dostali w kość i nie mają entuzjazmu do KOLEJNEJ rzeczy która na nich spadnie. Ci weterani są agentami najwyższej klasy, są dużo lepsi niż otaczający ich normalsi. Jednocześnie z jakiegoś powodu nikt ich nie słucha lub ich zdanie nie jest traktowane tak jak powinno. Ci weterani raczej trzymają się razem, próbują wykonywać swoją robotę tak dobrze jak to tylko możliwe i niespecjalnie integrują się z ludźmi poza swojego grona. „Musiałbyś tam być by zrozumieć”. W ramach tego motywu wykorzystuj umiejętności weteranów i graj na kontrast pomiędzy miłą, sympatyczną, ale mniej kompetentną grupą i cynicznymi, wrednymi weteranami wiedzącymi jak rozwiązać problem.

## Spoilers
