# Motive
## Metadane

* Nazwa: The Detective

## Krótki opis

"Dojdę do tego, jaka jest prawda". The Detective skupia się na tym, by znaleźć sprawcę zbrodni i ujawnić go społeczeństwu, niezależnie od moralności.

## Opis

"Dojdę do tego, jaka jest prawda". The Detective skupia się na tym, by znaleźć sprawcę zbrodni i ujawnić go społeczeństwu, niezależnie od moralności. 

Skupia się przede wszystkim na ujawnieniu prawdy i zrozumieniu wszystkich relacji pomiędzy komponentami. Nie zaakceptuje fałszu - musi naprawdę zrozumieć co się dzieje by móc podjąć właściwą decyzję. Każdy, nawet najdrobniejszy ślad, zostanie przebadany, rozpatrzony i prawidłowo wykorzystany. Traktuje to bardzo ambicjonalnie i nie spocznie, póki nie pozna prawdy.

W ramach tego motywu pokaż, jak najdrobniejsze ślady zmieniają się w całość w jego rękach. Jak potrafi połączyć najdrobniejsze komponenty. Jak bardzo oddany jest prawdzie i jak trudno jest go przechytrzyć. Najprawdopodobniej w tym motywie gracze stoją przeciwko niemu - pokaż jak trudno jest stworzyć scenę, która zmyli The Detective.

The Detective wygrywa, jeżeli pozna prawdę i dowie się co się dzieje - i jeśli będzie w stanie podjąć decyzję na podstawie tej wiedzy.

## Spoilers
