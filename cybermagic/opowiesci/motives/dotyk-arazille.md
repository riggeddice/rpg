# Motive
## Metadane

* Nazwa: Dotyk Arazille

## Krótki opis

Manifestacja Ukojenia. Królowa Niewolników, Snów i Luster. Obecność bogini - Ukojenie za cenę odrzucenia rzeczywistości.

## Opis

Manifestacja Ukojenia. Królowa Niewolników, Snów i Luster. Obecność bogini - Ukojenie za cenę odrzucenia rzeczywistości. Próba naprawy i działania konfrontuje się z możliwością odpoczynku i ukojenia bez końca, wiecznego snu bez cierpienia i bólu. Arazille pragnie zgasić światło i pozwolić ludziom odpocząć, nieważne co się nie dzieje. W ramach tego motywu pokaż potęgę Luster Arazille, działania Bogini i jej kultu i podkreśl konflikt pomiędzy pragnieniem odpoczynku i ukojenia a koniecznością wiecznej walki o lepsze jutro. Pokaż też, co dzieje się z tymi, których dotknęła Arazille i jej ideały.

## Spoilers
