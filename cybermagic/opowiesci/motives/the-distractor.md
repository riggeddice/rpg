# Motive
## Metadane

* Nazwa: The Distractor

## Krótki opis

"Popatrz tutaj!". The Distractor skupia się na kontrolowaniu uwagi. Odwraca uwagę od czegoś innego, co jest dlań ważniejsze.

## Opis

"Popatrz tutaj!". The Distractor skupia się na kontrolowaniu uwagi. Odwraca uwagę od czegoś innego, co jest dlań ważniejsze. The Distractor skupia się na osiągnięciu zupełnie innego celu, ale tamten cel jest delikatny i wrażliwy. Potrzebne są różne dywersje. Coś, co sprawi że cel nie będzie zagrożony. The Distractor działa po to, by wszyscy skupili się na zasłonie dymnej a nie na kluczowym celu. The Distractor generuje dużą ilość ruchów i chaosu, by nikt nie zauważył głównego celu.

W ramach tego motywu pomyśl co The Distractor chce osiągnąć naprawdę (Cel) i czemu ten Cel jest tak delikatny - jak można nim zachwiać. Czemu The Distractor tak bardzo chce osłonić tamten cel? Na czym zależy mu tak bardzo? Ile jest skłonny zapłacić i poświęcić?

The Distractor wygrywa, gdy Cel zostanie osiągnięty lub nie da się go już zatrzymać. Przetrwanie The Distractor jest drugorzędne.

## Spoilers
