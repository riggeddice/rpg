# Motive
## Metadane

* Nazwa: Konieczność pełnej dyskrecji

## Krótki opis

Problem polega na tym, że nikt nie może się niczego dowiedzieć. Trzeba rozwiązać sprawę 'cicho' i 'od środka', by nikt nic nie wiedział.

## Opis

Problem polega na tym, że nikt nie może się niczego dowiedzieć. Trzeba rozwiązać sprawę 'cicho' i 'od środka', by nikt nic nie wiedział. Sytuacja robi się czytelna, widoczna, a to spowoduje zainteresowanie sił które nic wiedzieć nie mogą. Więc - musimy rozwiązać to sami i, co gorsza, dyskretnie.

## Spoilers
