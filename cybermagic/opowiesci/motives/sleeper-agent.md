# Motive
## Metadane

* Nazwa: Sleeper Agent

## Krótki opis

Ktoś bardzo długo był ukryty, działając pod przykrywką - ale czas się ujawnić i zacząć działać.

## Opis

Ktoś bardzo długo był ukryty, działając pod przykrywką - ale czas się ujawnić i zacząć działać. Pokaż osobę, która ogólnie miała pewne zaufanie i dostępy a teraz jest w stanie je wykorzystać, działając z zaskoczenia oraz poza typowymi defensywami. Zapewnij wyraźny kontrast między dawna rolą w ukryciu a aktualną formą, a także pokaż konsekwencje tej zmiany dla innych. Dlaczego agent był w ukryciu i co sprawiło, że się ujawnił? Jaką ma agendę? Jak daleko się posunie?

## Spoilers
