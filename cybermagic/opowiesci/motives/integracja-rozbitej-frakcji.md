# Motive
## Metadane

* Nazwa: Integracja rozbitej frakcji

## Krótki opis

Struktura sesji. Sztab dowodzący Grupy został zniszczony. Cel - reintegracja i odbudowa pod swoją kontrolą.

## Opis

Struktura sesji. Sztab dowodzący Grupy został zniszczony. Cel - reintegracja i odbudowa pod swoją kontrolą. Odnoszę do odpowiedniej struktury sesji.

Poniższe skupia się na roli Graczy (Integratorów):

* Wizja
    * Głód: "Ta zrujnowana Grupa jest jak pęknięty miecz. Możemy go odbudować i przekształcić w wartościowe narzędzie, ale to wymaga pracy."
    * Stan Aktualny: Grupa jest zatomizowana, różne subfrakcje się zwalczają, każdy ciągnie w swoją stronę, walka o kluczowe zasoby, Grupa jest osłabiona a jej cel - stracony.
    * Stan Porażki: Grupa się rozpadła, subfrakcje przejęły działania, zasoby rozkradzione, jednostki zwalczone i pokonane. "Bankructwo".
    * Stan Pożądany: Grupa jest silna, pod kontrolą Integratorów, większość zasobów jest przejęta, potencjalni zdrajcy wyplenieni i wszystko co ważne zostało w grupie.
* Strategie
    * Identyfikacja subfrakcji od której zacząć integrację i przejąć kontrolę
    * Zdobywanie zaufania poszczególnych subfrakcji i integrowanie ich ze swoją
        * Usuwanie kluczowych członków sprzecznych / szkodliwych subfrakcji
    * Umacnianie swojej subfrakcji i zwalczanie innych subfrakcji
        * Zwalczanie dezintegrujących Grupę subfrakcji lub subfrakcji o sprzecznych celach z Graczami
        * Walka o kluczowe surowce, zasoby i sojuszników
        * Motywowanie i dawanie wspólnego kierunku i celu Grupie i subfrakcjom
    * Zmniejszanie ilości konfliktów
        * Mediacja i negocjacje w celu zakończenia konfliktów i sprzeczek między subfrakcjami
        * Tworzenie warunków do współpracy, demonstracja obszarów potencjalnej współpracy
    * Ochrona i zabezpieczanie Grupy przed zewnętrznymi wrogami
    * Budowanie nowej kultury, nowa identyfikacji, nowego celu. Stara Grupa umarła, nowa jest feniksem.

## Spoilers
