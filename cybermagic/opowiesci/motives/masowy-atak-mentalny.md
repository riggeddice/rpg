# Motive
## Metadane

* Nazwa: Masowy Atak Mentalny

## Krótki opis

Dana grupa osób zachowuje się odmiennie. Coś się im stało, coś zakłóciło ich sposób myślenia albo percepcję.

## Opis

Dana grupa osób zachowuje się odmiennie. Coś się im stało, coś zakłóciło ich sposób myślenia albo percepcję. W ramach tego motywu podkreśl jak dziwnie zachowują się ludzie i co się im stało; niech Zespół musi kombinować jak ich nie skrzywdzić, a jednocześnie jak sobie poradzić z głównym problemem mimo masowej psychozy.

## Spoilers
