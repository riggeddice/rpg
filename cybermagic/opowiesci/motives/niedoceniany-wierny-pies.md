# Motive
## Metadane

* Nazwa: Niedoceniany wierny pies

## Krótki opis

Ktoś jest wiernym sojusznikiem, skutecznym 'Dragonem'. Jednocześnie jest niedoceniany przez tą osobę.

## Opis

Ktoś jest wiernym sojusznikiem, skutecznym 'Dragonem'. Jednocześnie jest niedoceniany przez tą osobę. Pokaż konflikt pomiędzy Psem a Dowódcą. Pokaż, jakie decyzje Pies musi podejmować i jak to dla niego problematyczne. Pokaż też oddanie Psa i jego skuteczność. Może warto zagrać na przejęciu Psa na swoją stronę? Może 

## Spoilers
