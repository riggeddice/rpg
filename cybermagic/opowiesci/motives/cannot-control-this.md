# Motive
## Metadane

* Nazwa: Cannot Control This

## Krótki opis

"Złapał Polak Tatarzyna...". Myślisz, że jesteś w stanie to kontrolować. Nie da się. To się wyrwie. I będzie problem.

## Opis

"Złapał Polak Tatarzyna...". Myślisz, że jesteś w stanie to kontrolować. Nie da się. To się wyrwie. I będzie problem. Mamy do czynienia na przykład z próbą opanowania i badania bardzo wirulentnej zarazy. Lub ze zdominowanie m potężnego potwora. Od razu widać, że byt kontrolowany jest poza zasięgiem tych którzy próbują go kontrolować. To, że się wyrwie to tylko kwestia czasu. I wtedy dojdzie do katastrofy.

W ramach tego motywu graj wbudowanym konfliktem - kontrolowanie tego czegoś jest tego warte. Praktycznie niezaprzeczalnie wartościowe. Jednak spętanie tego jest nieprawdopodobnie trudne i prawie na pewno to się nam wyrwie spod kontroli. Dodaj też aspekt etyczny.

Endgame tego motywu polega na tym, że byt kontrolowany się wyrywa i dochodzi do nieuniknionej katastrofy.

## Spoilers
