# Motive
## Metadane

* Nazwa: Przepaście między klasami

## Krótki opis

"Arystokratka nie może dbać o człowieka". Osoby z różnych klas społecznych nie mogą być przyjaciółmi.

## Opis

"Arystokratka nie może dbać o człowieka". Osoby z różnych klas społecznych nie mogą być przyjaciółmi. W tym motywie skupiamy się na problemach wynikających właśnie z tego podejścia. Coś złego się stało bo ktoś boi się stracić twarz, lub jego grupa społeczna go skrzywdzi lub odrzuci z uwagi na zadawanie się z „tymi innymi”.

## Spoilers
