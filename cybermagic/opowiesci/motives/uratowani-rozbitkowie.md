# Motive
## Metadane

* Nazwa: Uratowani rozbitkowie

## Krótki opis

Doszło do katastrofy czy innego problemu. Mamy pulę nieszczęśników. I ich odratowaliśmy. Ale co przyszło z nimi?

## Opis

Doszło do katastrofy czy innego problemu. Mamy pulę nieszczęśników. I ich odratowaliśmy. Ale co przyszło z nimi? Może mamy dodatkowego potwora kryjącego się wśród załogi. Może są z zupełnie innej puli kulturowej i są po prostu odmienni. Może wszystko jest OK, ale systemy naszego statku nie radzą sobie z nową, większą populacją? W ramach tego motywu graj kontrastem między rozbitkami i aktualnymi członkami załogi, graj też potencjalnymi zagrożeniami czy możliwościami. Może postacie należą do rozbitków?

## Spoilers
