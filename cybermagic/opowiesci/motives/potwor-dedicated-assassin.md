# Motive
## Metadane

* Nazwa: Potwór - dedicated assassin

## Krótki opis

Potwór perfekcyjnie zaprojektowany do eliminacji konkretnego celu na konkretnym terenie. Zabójca sztucznie zaprojektowany.

## Opis

Potwór perfekcyjnie zaprojektowany do eliminacji konkretnego celu na konkretnym terenie. Zabójca sztucznie zaprojektowany. Zwykle jest perfekcyjnym _counterem_ konkretnej siły, strony lub frakcji, ale ma mnóstwo słabości innego typu. W ramach tego motywu zbuduj niesamowicie niebezpiecznego przeciwnika, dedykowanego przeciwko KOMUŚ i skup się na tym, by ten Assassin skupiał się na osłabieniu lub eliminacji tej konkretnej strony. Pokaż konieczność poradzenia sobie z sytuacją obecności Assassina i interakcji z Assassinem.

## Spoilers
