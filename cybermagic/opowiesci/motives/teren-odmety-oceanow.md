# Motive
## Metadane

* Nazwa: Odmęty, oceany

## Krótki opis

Sesja ma miejsce głęboko w niebezpiecznych, skażonych wodach dużych zbiorników wodnych.

## Opis

Sesja ma miejsce głęboko w niebezpiecznych, skażonych wodach dużych zbiorników wodnych. Mogą się w niej pojawiać potwory morskie, prądy magiczne, anomalna flora oraz fauna, w tym ryby o magicznych właściwościach.

## Spoilers
