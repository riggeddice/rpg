# Motive
## Metadane

* Nazwa: Ku pokrzepieniu serc

## Krótki opis

Ta sesja zawiera elementy kojące serca i pokrzepiające graczy. Reorientuje graczy w tym, że fundamentalnie jest coś dobrego i nie są sami, jest coś o co warto walczyć. Że w 'Dark Heroic' jest też 'Heroic'.

## Opis

Ta sesja zawiera elementy kojące serca i pokrzepiające graczy. Reorientuje graczy w tym, że fundamentalnie jest coś dobrego i nie są sami, jest coś o co warto walczyć. Że w 'Dark Heroic' jest też 'Heroic'. Bardzo dobry motyw do wyciągnięcia po czymś mrocznym i strasznym, też jeśli gracze mają gorszy dzień. Przykładowe elementy kojące i pokrzepiające (by Darken): 'dowie się, że inni go chwalą sądząc, że ich nie słyszy', 'zobaczy efekty swoich dawnych działań, które już nie pamięta', 'uratowanie kogoś, kto potem ma dzieci', 'porażka, która nie miała znaczenia, bo i tak by się to wydarzyło i ta osoba nie miała na to wpływu. Ale i tak dokonała czegoś, co inna postać którą uważa za lepszą w podobnej sytuacji zrobiła gorzej', 'wybaczenie kogoś, kto sądził, że mu nie wybaczy'. 

## Spoilers
