# Motive
## Metadane

* Nazwa: Misja survivalowa

## Krótki opis

Obcy, wrogi teren. Nie ma dość surowców na wszystko. Trzeba przetrwać i doczekać ratunku lub samemu zbudować ewakuację.

## Opis

Obcy, wrogi teren. Nie ma dość surowców na wszystko. Trzeba przetrwać i doczekać ratunku lub samemu zbudować ewakuację. W ramach tego motywu pokaż przetrwanie, walkę z otoczeniem. Pokaż, jak powoli sprzęt z jakim weszliśmy zaczyna się sypać i trzeba znaleźć analogiczny. Wygeneruj Presję przez brakujące surowce podstawowe (jedzenie, picie, spanie) i przez konieczność wykonania ruchu bo inaczej jest po tobie. Skup się na aktywnej florze i faunie dookoła i na nich jako na frakcjach z impulsami.

## Spoilers
