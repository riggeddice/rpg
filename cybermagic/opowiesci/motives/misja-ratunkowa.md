# Motive
## Metadane

* Nazwa: Misja ratunkowa

## Krótki opis

Struktura sesji. Film katastroficzny z perspektywy ratowników. Ratujemy kogo się da wśród różnorodnych frakcji i agend.

## Opis

Struktura sesji. Film katastroficzny z perspektywy ratowników. Ratujemy kogo się da wśród różnorodnych frakcji i agend. Odnoszę do odpowiedniej struktury sesji.

Poniższe skupia się na roli Graczy (Ratowników):

* Rola Graczy: nadzieja i lepsze jutro
    * mimo potworności Katastrofy, są jednostki zdolne i chętne do pomocy.
    * pomóżcie ludziom, wyciągnijcie co się da, dajcie im nadzieję na jutro.

## Spoilers
