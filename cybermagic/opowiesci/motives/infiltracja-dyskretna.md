# Motive
## Metadane

* Nazwa: Infiltracja, dyskretna

## Krótki opis

Trzeba się gdzieś wkraść w sposób niepostrzeżony i coś osiągnąć. Ważne jest zachowanie ciszy, by nikt nic nie wiedział.

## Opis

Trzeba się gdzieś wkraść w sposób niepostrzeżony i coś osiągnąć. Ważne jest zachowanie ciszy, by nikt nic nie wiedział. W ramach tego motywu pojawia się maskowanie, przebieranie się, przeczekiwanie, dywersja i wszelkie inne podobne metody działania.

## Spoilers
