# Motive
## Metadane

* Nazwa: Pomocne duchy Samszarów

## Krótki opis

Ród Samszar słynie z bardzo pomocnych duchów wspierających i opiekuńczych, które zawsze pomogą.

## Opis

Ród Samszar słynie z bardzo pomocnych duchów wspierających i opiekuńczych, które zawsze pomogą. W ramach tego motywu silnie graj duchami. Pokaż, jak zapewniają harmonię, piękno, spokój i dobre rady danemu terenowi. Pokaż, jak działają przeciwko zewnętrznym zagrożeniom i próbują chronić ludzi. Pokaż też ich dobry wpływ na lokalnych ludzi.

## Spoilers
