# Motive
## Metadane

* Nazwa: Beznadziejny szef

## Krótki opis

Czyjś przełożony jest głupim tyranem, albo jest słaby, albo nic nie robi. Stanowi to problem.

## Opis

Czyjś przełożony jest głupim tyranem, albo jest słaby, albo nic nie robi. Stanowi to problem. Może przez jego głupie decyzje coś bardzo nie działa. Może trzeba działać obok niego. Może trzeba go zadowalać w idiotyczny sposób. Może też rozgrywać ludzi przeciw sobie używając głupiego systemu oceny. W ramach tego motywu rozgrywaj szefa jako ważną siłę powodującą chaos i konieczną do przezwyciężenia.

## Spoilers
