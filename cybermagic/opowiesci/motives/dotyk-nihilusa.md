# Motive
## Metadane

* Nazwa: Dotyk Nihilusa

## Krótki opis

Manifestacja Entropii. Nieskończona rozpacz przed nieuniknionym. Rozpad wszystkiego. Obojętna droga do zapomnienia.

## Opis

Manifestacja Entropii. Nieskończona rozpacz przed nieuniknionym. Rozpad wszystkiego. Obojętna droga do zapomnienia. Nadzieja konfrontuje się z pustką. Żadne ludzkie czyny nie mają znaczenia. Nic nie jesteśmy w stanie zmienić. Ostatnie gwiazdy zgasną. W ramach tego motywu pokaż nieuniknioną, beznadziejną walkę przeciwko entropii. "Rage against the dying of the light". Rozpaczliwa walka o zachowanie czegokolwiek w obliczu rozpadającej się rzeczywistości i w obliczu obojętnej pustki i zapomnienia.

## Spoilers
