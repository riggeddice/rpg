# Motive
## Metadane

* Nazwa: Into the Abyss

## Krótki opis

Zaczyna się od okrucieństwa. Tracimy nadzieję. A potem kaskaduje jeszcze bardziej, jest jeszcze gorzej. Nic nie można zmienić, ale idziemy dalej przez koszmar z jednym, ostatnim celem...

## Opis

Zaczyna się od okrucieństwa. Tracimy nadzieję. A potem kaskaduje jeszcze bardziej, jest jeszcze gorzej. Nic nie można zmienić, ale idziemy dalej przez koszmar z jednym, ostatnim celem. Sesje z tym motywem są dość ciężkie, bo zaczynają się od beznadziejnej sytuacji gdzie ludzie robią co się da by przetrwać a potem idziemy dalej i dalej i dalej. "Kurczakownia". "Finis Vitae". "Faza Esuriit". Opowieści, gdzie wszyscy których spotkaliśmy już najpewniej są zgubieni i możemy uratować minimum, mimo, że wszyscy dookoła błagają o pomoc. A jeden błąd i dołączymy do zestawu nieszczęśników bez nadziei. W ramach tego motywu pokaż Abyss, pokaż jego wpływ na wszystko i wszystkich i, co najważniejsze, **ostro uderzaj w postacie graczy**. Są o krok od tego, by Abyss spojrzał i na nich.

## Spoilers
