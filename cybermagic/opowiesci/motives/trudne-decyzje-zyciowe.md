# Motive
## Metadane

* Nazwa: Trudne decyzje życiowe

## Krótki opis

Głód dziecka czy współpraca z mafią. Gra w SupMis czy szukanie pracy. Trudne wybory i dylematy życiowe.

## Opis

Głód dziecka czy współpraca z mafią. Gra w SupMis czy szukanie pracy. Trudne wybory i dylematy życiowe. W ramach tego motywu pokaż że oba wybory są wartościowe i nietrywialne, ale coś trzeba wybrać. Często sesja będzie osadzona właśnie dookoła tego wyboru.

## Spoilers
