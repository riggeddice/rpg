# Motive
## Metadane

* Nazwa: Wyścig zbrojeń

## Krótki opis

Dwie strony się zwalczają i obie wyciągają coraz cięższą broń, coraz nowocześniejszą, coraz groźniejszą.

## Opis

Dwie strony się zwalczają i obie wyciągają coraz cięższą broń, coraz nowocześniejszą, coraz groźniejszą. Czasem jedna ze stron nawet nie wie, że uczestniczy w wyścigu zbrojeń. W ramach tego motywu pokaż tą eskalację dwustronną, pokaż, że wiele innych rzeczy musi zostać poświęcona i stracona by tylko już nawet nie wygrać ale NIE PRZEGRAĆ.

## Spoilers
