# Motive
## Metadane

* Nazwa: Niekompetentna pomoc

## Krótki opis

Ktoś chciał pomóc, naprawdę. Ale nie jest dość dobry lub o czymś nie wiedział i wszystko poszło cholernie nie tak.

## Opis

Ktoś chciał pomóc, naprawdę. Ale nie jest dość dobry lub o czymś nie wiedział i wszystko poszło cholernie nie tak. W ramach tego motywu podkreśl dobre serce i chęć pomocy, ale jednocześnie pokaż jak bardzo sprawa była poza poziomem możliwości pomagającego.

## Spoilers
