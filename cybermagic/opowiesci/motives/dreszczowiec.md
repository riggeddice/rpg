# Motive
## Metadane

* Nazwa: Dreszczowiec

## Krótki opis

Ta sesja zawiera elementy horroru.

## Opis

Ta sesja zawiera elementy horroru. Postacie nie wszystko kontrolują, rzeczywistość okazuje się groźniejsza niż się wydaje i sytuacja jest dużo bardziej opresyjna. Wiele osób może zginąć.

## Spoilers
