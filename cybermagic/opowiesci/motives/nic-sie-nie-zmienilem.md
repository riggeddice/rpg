# Motive
## Metadane

* Nazwa: Nic się nie zmieniłem

## Krótki opis

"Nadal jestem tą samą osobą. Nadal jestem młody. Nadal mogę to co kiedyś!". Po drastycznych zmianach, ktoś odmawia akceptacji nowego życia.  

## Opis

"Nadal jestem tą samą osobą. Nadal jestem młody. Nadal mogę to co kiedyś!". Po drastycznych zmianach, ktoś odmawia akceptacji nowego życia. Rozpaczliwe trzymanie się starego życia i starego potencjału nie zauważając, że nowe życie jest może inne ale nadal niesie wartość. Pokaż w tym motywie to kurczowe trzymanie się, pokaż rozpacz i desperację.

## Spoilers
