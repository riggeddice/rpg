# Motive
## Metadane

* Nazwa: Starry-Eyed Novices

## Krótki opis

Starry-Eyed Novices to niedoświadczone jednostki, które z optymizmem i lekką niezgrabnością wchodzą w groźny świat.

## Opis

Starry-Eyed Novices to niedoświadczone jednostki, które z optymizmem i lekką niezgrabnością wchodzą w groźny świat. Robią błędy na poziomie założeń ("na pewno chce mi pomóc"), na poziomie implementacyjnym ("to się robiło tak") i ogólnie starają się wykazać a wychodzi jak zwykle. W ramach tego motywu graj kontrastem między Starry-Eyed Novice a BitterVet, pokaż ten optymizm i nadzieję młodości i pozwól im wnieść nie zawsze skutecznej ale zawsze optymistycznej energii do systemu.

## Spoilers
