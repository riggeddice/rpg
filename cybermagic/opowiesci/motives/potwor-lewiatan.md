# Motive
## Metadane

* Nazwa: Potwór - Lewiatan

## Krótki opis

Lewiatan to praktycznie niezniszczalna istota, wypaczająca rzeczywistość dookoła siebie. 

## Opis

Lewiatan to praktycznie niezniszczalna istota, wypaczająca rzeczywistość dookoła siebie. Na tej sesji Lewiatan się pojawia jako siła wypaczająca sesję, praktycznie jako katastrofa naturalna. W ramach tego motywu pokaż pierwotną siłę Lewiatana nie jako przeciwnika z którym się walczy a jako istotę, która jest i zmienia rzeczywistość. To Lewiatan jest _apex entity_ sesji, nie ludzie.

## Spoilers
