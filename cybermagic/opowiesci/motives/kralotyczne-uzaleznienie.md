# Motive
## Metadane

* Nazwa: Kralotyczne uzależnienie

## Krótki opis

Ofiary kralothów pragną im służyć. To połączenie nieprawdopodobnie silnych afrodyzjaków i rekonstrukcji biologicznej. Ten motyw nie wymaga kralotha, tylko jego efektów ubocznych.

## Opis

Ofiary kralothów pragną im służyć. To połączenie nieprawdopodobnie silnych afrodyzjaków i rekonstrukcji biologicznej. Ten motyw nie wymaga kralotha, tylko jego efektów ubocznych. Ogólnie, motyw skupia się na efekcie uzależnienia kralotycznego i kralotycznych efektów dookoła ofiar. W ramach tego motywu pokaż destrukcję mentalną jaką robi uzależnienie tego typu, pokaż jak osoby padają i co dzieje się dookoła ich.

## Spoilers
