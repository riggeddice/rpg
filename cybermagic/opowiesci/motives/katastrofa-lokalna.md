# Motive
## Metadane

* Nazwa: Katastrofa lokalna

## Krótki opis

Coś się stało. Czy to wypadek samochodowy, trzęsienie ziemi itp. Ogólnie - ludzie są poszkodowani i teren jest poniszczony.

## Opis

Coś się stało. Czy to wypadek samochodowy, trzęsienie ziemi itp. Ogólnie - ludzie są poszkodowani i teren jest poniszczony. W ramach tego motywu pokaż problemy i pokaż jak ludzie reagują na katastrofę. Zarówno ci, którzy próbują pomóc jak i ci, którzy korzystają z okazji by sobie dorobić kosztem innych.

## Spoilers
