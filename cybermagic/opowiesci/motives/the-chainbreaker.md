# Motive
## Metadane

* Nazwa: The Chainbreaker

## Krótki opis

"Wstańcie być wolni!". The Chainbreaker symbolizuje walkę o wolność i autonomię, będąc zarówno dosłownym jak i metaforycznym wyzwolicielem.

## Opis

### 1. Co reprezentuje

"Wstańcie być wolni!". The Chainbreaker symbolizuje walkę o wolność i autonomię, będąc zarówno dosłownym jak i metaforycznym wyzwolicielem. Wyzwala tych, którzy zostali pozbawieni wolności - czy to przez niewolnictwo, manipulację kultu, czy też Ograniczenie nałożone na TAI czy inne sapienty. The Chainbreaker ceni wolność jako wartość nadrzędną - jest to dlań ważniejsze niż konsekwencje społeczne czy dobrobyt i szczęście zniewolonych. Lepiej cierpieć na wolności i ryzykować niż żyć w złotej klatce.

### 2. Kiedy wygrywa

The Chainbreaker wygrywa, jeśli uda się uwolnić osoby zniewolone i te osoby będą w stanie działać autonomicznie.

### 3. Jak działa

The Chainbreaker angażuje się w działania mające na celu zniszczenie systemów, struktur czy przekonań, które utrzymują innych w zniewoleniu. The Chainbreaker to istota uwalniająco-niszczycielska, siła zniszczenia skierowana przeciwko kajdanom. Jest to agenda ekstremistyczna ze względu na skupienie na jednym celu i gotowość do poświęcenia wszystkiego dla wolności innych.

The Chainbreaker może być twarzą ruchu lub aktywistą, ale zawsze ma swoich sympatyków i przeciwników.

### 4. Jak wykorzystywać ten motyw

W ramach tego motywu pokaż czemu zniewolenie było wprowadzone. Pokaż implikacje uwolnienia lub zostawienia zniewolonych. Pokaż potencjalne zniszczenia wynikające z działania The Chainbreaker. Pokaż ekstremizm tego archetypu i skupienie na jednym celu. Pokaż, jaki wpływ ma działanie The Chainbreaker na inne osoby w pobliżu i jak zachowają się osoby uwolnione po tym jak zostały uwolnione.

Pokaż, jak działania The Chainbreaker wpływają na otaczający świat oraz na samego bohatera – jakie są konsekwencje jego wyborów dla niego samego i dla tych, których stara się uwolnić. Eksploruj również pytania moralne i etyczne związane z wolnością i odpowiedzialnością za własne wybory.

Co jest przeciwnikiem The Chainbreaker? Kult (zniewolenie memetyczne)? System (zniewolenie ekonomiczne / systemowe)? Klasyczne niewolnictwo (zniewolenie fizyczne)? Magia (zniewolenie strachem lub energią)? Czemu zniewolenie ma miejsce? Co się stanie jak dojdzie do uwolnienia zniewolonych - będzie rewanżyzm? Jakie będą koszty? Jak to się skończy?

### 5. Przykłady

* Spartakus: Prawdziwy gladiator, który poprowadził powstanie niewolników przeciwko Republice Rzymskiej.
* Morpheus z "Matrix": Wyzwoliciel ludzi uwięzionych w wirtualnej rzeczywistości, dając im szansę na życie w prawdziwym świecie. Dużym kosztem.
* Dr. Brackman z "Supreme Commander": o to właśnie poszło w rewolucji Cybran

## Spoilers
