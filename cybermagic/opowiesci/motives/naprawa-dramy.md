# Motive
## Metadane

* Nazwa: Naprawa dramy

## Krótki opis

Doszło do ogromnej dramy. Ludzie ze sobą nie gadają lub coś sobie zarzucają. I teraz naprawiamy tą dramę, odbudowując linie komunikacyjne.

## Opis

Doszło do ogromnej dramy. Ludzie ze sobą nie gadają lub coś sobie zarzucają. I teraz naprawiamy tą dramę, odbudowując linie komunikacyjne. W ramach tego motywu skupiamy się na odkrywaniu o co chodzi, czemu nie gadają, czemu się kłócą, co tu teraz nie działa i naprawie tego wszystkiego. Czasem to poważniejsze tematy (ojciec zostawił dziecko i potrzebuje pomocy) czasem bardziej dobijające (zupa była za słona i reaguje passive aggressive). Tak czy inaczej, ten motyw skupia się na naprawie tego wszystkiego i konsekwencjach próby naprawy.

## Spoilers
