# Motive
## Metadane

* Nazwa: Inżynieria społeczna

## Krótki opis

Konstrukcja otoczenia lub zasad dla danej grupy społecznej by osiągnąć odpowiedni efekt.

## Opis

Konstrukcja otoczenia lub zasad dla danej grupy społecznej by osiągnąć odpowiedni efekt. Wykorzystanie sił i nacisków, by doprowadzić do odpowiedniego zachowania danej grupy czy populacji. Można to zaaranżować przez odpowiednie wydarzenia i układ przestrzeni, można przez wprowadzenie innej populacji.

## Spoilers
