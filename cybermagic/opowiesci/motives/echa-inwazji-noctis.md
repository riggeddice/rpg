# Motive
## Metadane

* Nazwa: Echa Inwazji Noctis

## Krótki opis

Inwazja się skończyła, ale noktianie zostali. Noktianie i astorianie spotykają się z problemami po Inwazji.

## Opis

Inwazja się skończyła, ale noktianie zostali. Te opowieści są o tym z jakimi problemami spotykają się noktianie po Inwazji, oraz jak astorianie radzą sobie z echami wojny oraz obecnością noktian.

## Spoilers

* **Mroczne serce**
    * **Głód**: "Wojna się skończyła. Rany zostały."
        * noktianie: zmuszeni do życia w nowym, nieznanym, wrogim świecie który ich nie akceptuje
        * astorianie: konfrontowani z noktianami, echa wojny i ran wojennych
    * **Ofiara**: człowieczeństwo
        * astorianie traktują noktian źle, bo wiele wycierpieli. Dobry noktianin to martwy noktianin.
        * noktianie próbujący się zaadaptować są stygmatyzowani i są ludźmi drugiej kategorii
        * Inwazja kaskaduje na następne pokolenia...
* **Agenda - co chcesz osiągnąć**: 
    * pokaż konflikty między noktianami a lokalną społecznością - pokaż, że noktianie nigdy nie są członkami grupy
    * pokaż cierpienie astorian wynikające z Inwazji - złe traktowanie noktian jest zrozumiałe
    * pokaż osobiste walki wszystki z traumą, stratą i tęsknotą - coping is hard, hope is eternal
* **Dark Future - wizja maksymalnej porażki Graczy**:
    * eskalacja przemocy i spirali nienawiści, do momentu linczu lub morderstw
    * kolejne pokolenia rozróżniają na 'noktian' i 'astorian', cierpią też niewinne dzieci
