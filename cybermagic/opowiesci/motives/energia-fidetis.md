# Motive
## Metadane

* Nazwa: Energia Fidetis

## Krótki opis

Energia Fidetis. Wartość egzystencji, dobre życie, fatum, szczęście i właściwe miejsce w egzystencji.

## Opis

Energia Fidetis. Wartość egzystencji, dobre życie, fatum, szczęście i właściwe miejsce w egzystencji.

Energia wiary w to, że jeśli wszyscy będziemy starali się, że będzie dobrze po naszemu, to jakoś się ułoży. Energia fatalistyczna, ale też pełna optymizmu. Świat, o który warto walczyć. Fatum i szczęście. Jeśli będziemy starali się w ramach swoich możliwości, świat zadziała, jak powinien, bo tak jest napisane w gwiazdach.

W ramach tego motywu graj ogólnie rozumianym przeznaczeniem i konfliktami pomiędzy różnymi przez znaczeniami różnych sił, dla których idealny świat wygląda inaczej - czasem nawet jest sprzeczny. Pokazuj cykliczność i harmonijny porządek. Łowca i Ofiara. Życie Które Ma Sens. Pokaż, że da się osiągnąć szczęście i sprawczość, oddając poziom Wizji w ręce ogólnie rozumianego Fatum.

## Spoilers
