# Motive
## Metadane

* Nazwa: The Saboteur

## Krótki opis

"Zniszczę to." The Saboteur chce coś unieszkodliwić, zniszczyć lub wysadzić. Coś co działa teraz ma zostać neutralizowane.

## Opis

"Zniszczę to." The Saboteur chce coś unieszkodliwić, zniszczyć lub wysadzić. Coś co działa teraz ma zostać neutralizowane. The Saboteur chce coś unieszkodliwić, zniszczyć lub wysadzić. Coś co działa teraz ma zostać neutralizowane. The Saboteur może być osobą, potworem albo grupą. Cel może być osobą, organizacją albo konstrukcją. Chodzi o unieszkodliwienie, osłabienie bądź neutralizację czegoś co w tej chwili działa dobrze ale z jakiegoś powodu jest niepożądane dla The Saboteur albo mocodawców. 

W ramach tego motywu graj kontrastem pomiędzy stanem rzeczywistym i tym jak wygląda rzeczywistość przed sabotażem i ewentualnymi konsekwencjami udanego sabotażu. Określ też motywację tego sabotażu i sposób w jaki on ma być realizowany. 

The Saboteur wygrywa, jeżeli uda się doprowadzić do nieodwracalnego sabotażu i to co było jednością się rozpadnie.
