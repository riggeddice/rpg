# Motive
## Metadane

* Nazwa: Naprawa wizerunku grupy

## Krótki opis

Dobre imię jakiejś grupy jest zagrożone i ta grupa robi wszystko by oczyścić się z zarzutów i odbudować dobry wizerunek. 

## Opis

Dobre imię jakiejś grupy jest zagrożone i ta grupa robi wszystko by oczyścić się z zarzutów i odbudować dobry wizerunek. Ten motyw nie oznacza, że ta grupa „to co się jej zarzuca” faktycznie zrobiła. To tylko oznacza, że przypisano tej grupie jakiś zły czyn i musi się ochronić. Jest to coś problematycznego lub upokarzającego. Jest też szansa, że istnieje inna grupa, która z przyjemnością zrani reputację grupy próbującej się chronić. W ramach tego motywu graj desperacją grupy próbującej chronić reputację, ich silnymi zdeterminowanymi ruchami oraz ryzykami przypadkowego wpakowania się w jeszcze większe kłopoty wizerunkowe.

## Spoilers
