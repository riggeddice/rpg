# Motive
## Metadane

* Nazwa: Teren, gdzie rządzi arystokracja

## Krótki opis

Niezależnie co się na tym terenie nie stanie, mamy dwa rzędy ludzi. Arystokracja (np. tienowaci) i wszyscy inni. Dwa poziomy praw.

## Opis

Niezależnie co się na tym terenie nie stanie, mamy dwa rzędy ludzi. Arystokracja (np. tienowaci) i wszyscy inni. Dwa poziomy praw. W ramach tego motywu podkreśl całkowite bezprawie arystokracji i wolność ich działania oraz poziom pokory reszty populacji. Pokaż ludzi, którzy nic nie mogą zrobić nieważne jakby się starali. Pokaż, że arystokracja może wszystko.

## Spoilers
