# Motive
## Metadane

* Nazwa: Szczur zapędzony do kąta

## Krótki opis

Ktoś nie ma drogi ucieczki, nie ma jak się wycofać. Nie widzi żadnego sposobu na rozwiązanie problemu. Więc jest jak szczur zapędzony do kąta - atakuje jak umie. Last stand.

## Opis

Ktoś nie ma drogi ucieczki, nie ma jak się wycofać. Nie widzi żadnego sposobu na rozwiązanie problemu. Więc jest jak szczur zapędzony do kąta - atakuje jak umie. Last stand. W ramach tego motywu graj motywacją Szczura, podkreśl brak nadziei i desperację. Szczur robi złe, niszczycielskie, potencjalnie straszne rzeczy - ale Szczur nie widzi innego wyjścia jak osiągnąć swój cel i/lub przetrwać.

## Spoilers
