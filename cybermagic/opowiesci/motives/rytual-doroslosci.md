# Motive
## Metadane

* Nazwa: Rytuał Dorosłości

## Krótki opis

Ktoś przechodzi przez Rytuał Przejścia, by być uznanym za dorosłego w swej kulturze.

## Opis

Ktoś przechodzi przez Rytuał Przejścia, by być uznanym za dorosłego w swej kulturze. Może być duchowy, fizyczny, emocjonalny. Jest silnie powiązany z wartościami społeczności. W ramach tego motywu podkreślaj konieczność przejścia przez Rytuał osobę samodzielnie, przemianę, kulturę wymagającą rytuału oraz ogólnie "rytualistyczną" naturę sytuacji. To, co dana osoba musi zrobić niekoniecznie będzie efektywne, ale na pewno sprawi, że dana osoba się sprawdzi. Możesz grać na konflikcie "to głupie, powinno się robić TAK" przeciwko "nie, moja kultura wymaga TAK".

## Spoilers
