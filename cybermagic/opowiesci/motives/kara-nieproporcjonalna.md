# Motive
## Metadane

* Nazwa: Kara, nieproporcjonalna

## Krótki opis

Za przejście na czerwonym świetle - śmierć. Kara nieproporcjonalna do winy. 

## Opis

Za przejście na czerwonym świetle - śmierć. Kara nieproporcjonalna do winy. Ten motyw kojarzy się z żartami z W40k czy Sędziego Dredda. Jakaś siła próbuje karać nieprawdopodobnie mocno za normalne wykroczenia życiowe. Graj tym motywem by pokazać absurdalność ale też psychozę wynikającą z takiego podejścia. 

## Spoilers
