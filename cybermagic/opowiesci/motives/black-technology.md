# Motive
## Metadane

* Nazwa: Black Technology

## Krótki opis

Superzaawansowana ale kontrolowana technologia, której nie potrafimy zreplikować w sensowny sposób lub nie rozumiemy. Clarketech.

## Opis

Superzaawansowana ale kontrolowana technologia, której nie potrafimy zreplikować w sensowny sposób lub nie rozumiemy. Clarketech. Jest to dość unikalne narzędzie, zwykle anomalne i dające unikalne szerokie możliwości. W ramach tego motywu pokaż implikacje wykorzystania Black Technology i zainteresowanie wszystkich dookoła tym narzędziem.

## Spoilers
