# Motive
## Metadane

* Nazwa: Dark Scientist

## Krótki opis

Na tym terenie jest amoralny naukowiec, który robi eksperymenty zakazane i nieetyczne.

## Opis

Na tym terenie jest amoralny naukowiec, który robi eksperymenty zakazane i nieetyczne. Ten motyw skupia się na eksploatacji granic moralności, nauki i etyki, prezentując postać, która zignorowała te granice w imię "postępu". Ambicja i żądza wiedzy wymagają prędkości. Podkreśl cierpienie ofiar, ale podkreśl też osiągnięcia naukowca. 

## Spoilers
