# Motive
## Metadane

* Nazwa: Życie w złotej klatce

## Krótki opis

Ma wszystko czego zapragnie. Ale jest pod ciągłą opieką i nic nie może zrobić samemu.

## Opis

Ma wszystko czego zapragnie. Ale jest pod ciągłą opieką i nic nie może zrobić samemu. W tym motywie skupiamy się na konflikcie pomiędzy osobą, która próbuje coś zrobić niezależnie a nadopiekuńczą grupą, która próbuje zadbać by nic się tej osobie nie stało. W tym motywie graj brakiem doświadczenia osoby w złotej klatce, jej próbami osiągnięcia jakiejkolwiek autonomii oraz tym, że ci którzy próbują ją chronić poruszą niebo i ziemię by ta osoba wróciła pod ich kontrolę.

## Spoilers
