# Motive
## Metadane

* Nazwa: Subtelna słodka manipulacja

## Krótki opis

Bo ona jest taka BIEDNA i tak bardzo POTRZEBUJE POMOCY, i i i tam się stało TAKIE ZŁO. Subtelna manipulacja mająca przesunąć ruchy Ofiary w odpowiednią stronę.

## Opis

Bo ona jest taka BIEDNA i tak bardzo POTRZEBUJE POMOCY, i i i tam się stało TAKIE ZŁO. Subtelna manipulacja mająca przesunąć ruchy Ofiary w odpowiednią stronę. W tym motywie występuje Biedne Maleństwo, które bardzo próbuje przekonać Ofiarę do zrobienia czegoś i to w taki sposób, by Ofiara zrobiła to nie mając pojęcia o manipulacji.

## Spoilers
