# Motive
## Metadane

* Nazwa: Energia Unumens

## Krótki opis

Energia Unumens. Jedność - zarówno jako 'oneness' jak i 'unity'. Jesteśmy częścią czegoś większego i razem zwyciężymy, razem nic nas nie zatrzyma.

## Opis

Energia Unumens. Jedność - zarówno jako 'oneness' jak i 'unity'. Jesteśmy częścią czegoś większego i razem zwyciężymy, razem nic nas nie zatrzyma.

Energia jedności, unifikacji, jednomyślności oraz „w kupie siła”. Nieważne która jednostka upadnie, jako grupa jesteśmy w stanie przetrwać wszystko. Poddanie indywidualności celom grupowym i zapewnienie optymalizacji dla grupy a nie dla jednostki. Jeżeli odrzucimy indywidualność i skupimy się na zwycięstwie grupy, jako ludzkość nie tylko przetrwamy, ale zwyciężymy.

W ramach tego motywu graj konfliktem pomiędzy indywidualnością (wolną wolą) a kolektywizmem (poddaniem się grupie). Pokaż niesamowitą efektywność oddania wszystkiego grupie, która jednak odbiera nam wiele z tego, co stanowi o naszym człowieczeństwie.

## Spoilers
