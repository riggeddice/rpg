# Motive
## Metadane

* Nazwa: Ochrona za wszelką cenę

## Krótki opis

Mimo, że Strażnik ryzykuje swoim życiem - tym razem ochroni ją niezależnie od ceny. Last Stand.

## Opis

Mimo, że ryzykuje swoim życiem - tym razem ochroni ją niezależnie od ceny. Last Stand. Strażnik nie pozwoli, by się stała krzywda Chronionej. Posunie się tak daleko jak to możliwe. W ramach tego motywu pokaż wolę i przekonanie tak wielkie, że ochrona się kończy dopiero w momencie, w którym ciało Strażnika nie daje już rady. Pokaż inspirację, którą Strażnik generuje wobec tych którzy widzą poświęcenie.

## Spoilers
