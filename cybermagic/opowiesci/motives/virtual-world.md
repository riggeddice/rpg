# Motive
## Metadane

* Nazwa: Virtual World

## Krótki opis

Duża część tej sesji jest oparta w jednym ze skonstruowanych światów MultiVirt.

## Opis

Duża część tej sesji jest oparta w jednym ze skonstruowanych światów MultiVirt. Każdy z tych światów - Smocze Wojny, Supreme Missionforce itp. ma inne reguły, zasady i fizykę. Wszystkie działają na zasadzie awataryzacji - wchodzisz do wirtualnego świata i jesteś jego częścią. W ramach tego wątku odbij się od tego co możesz zrobić przede wszystkim w VirtWorldach - gry, letalność, gildie. Ale też pokaż interakcje VirtWorlda z prawdziwym światem.

## Spoilers
