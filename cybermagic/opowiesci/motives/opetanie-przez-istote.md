# Motive
## Metadane

* Nazwa: Opętanie przez Istotę

## Krótki opis

Jakaś istota (duch, demon) steruje tą osobą; może gdy śpi, może zawsze. Tak czy inaczej, on nie jest sobą.

## Opis

Jakaś istota (duch, demon) steruje tą osobą; może gdy śpi, może zawsze. Tak czy inaczej, on nie jest sobą. Gdy dana osoba jest opętana, nadal ma ograniczenia _hosta_ - niemag nie stanie się magiem, nie będzie też super silny. W ramach tego motywu graj kontrastem między kimś gdy jest opętany a gdy jest 'normalny'. Pokaż agendę opętującego.

## Spoilers
