# Motive
## Metadane

* Nazwa: I will never join you!

## Krótki opis

I Will Never Join You! to jest ostateczny 'act of defiance'. Pokazanie, że jest jeszcze inna droga. Że możesz kontrolować swoje przeznaczenie. Że ONI nie wygrają. Straszne poświęcenie.

## Opis

I Will Never Join You! to jest ostateczny 'act of defiance'. Pokazanie, że jest jeszcze inna droga. Że możesz kontrolować swoje przeznaczenie. Że ONI nie wygrają. Straszne poświęcenie. Czy to życie, czy kończyna, czy coś jeszcze innego. W ramach tego motywu graj tą hardością ludzkiej natury, tą niezgodą na czynienie zła i tym jak daleko ktoś się posunie by nie być agentem Zła.

## Spoilers
