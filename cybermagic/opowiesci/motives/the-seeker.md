# Motive
## Metadane

* Nazwa: The Seeker

## Krótki opis

"Figurka Sokoła będzie moja". The Seeker skupia się na jednej określonej rzeczy/osobie którą próbuje odnaleźć i pozyskać.

## Opis

"Figurka Sokoła będzie moja". The Seeker skupia się na jednej określonej rzeczy/osobie którą próbuje odnaleźć i pozyskać. Zawsze wie czego dokładnie szuka, ale nie zawsze wie gdzie to jest lub co to jest - np. może szukać swojego biologicznego syna, ale nie wie jak wygląda. The Seeker skupia się na tej jednej jedynej rzeczy i próbuje ją znaleźć i pozyskać niezależnie od okoliczności.

W ramach tego motywu graj demonstracją przeszłych ruchów The Seeker jak i nieuniknionym zbliżaniem się The Seeker do celu. Pokaż jak działa The Seeker, jak daleko jest skłonny się posunąć do tego by pozyskać to na czym mu zależy.

The Seeker wygrywa, gdy udaje mu się pozyskać tą konkretną rzecz której szuka i uda mu się dokonać ekstrakcji z miejsca akcji.

## Spoilers
