# Motive
## Metadane

* Nazwa: Spotlight na lokację

## Krótki opis

Na tej sesji skupiamy się na jednej konkretnej lokacji. By rozwiązać sesję, trzeba zrozumieć to miejsce i jego specyfikę i się doń zaadaptować.

## Opis

Na tej sesji skupiamy się na jednej konkretnej lokacji. By rozwiązać sesję, trzeba zrozumieć to miejsce i jego specyfikę i się doń zaadaptować. Ten motyw eksploruje lokację i jej unikalność - co odróżnia to miejsce od innych, zarówno w pozytywny jak i negatywny sposób. W ramach tego motywu skup się na historii i bogactwie lokacji, frakcjach dotykających tą lokację i usnuj intrygę dookoła zrozumienia znaczenia tego miejsca dla innych.

## Spoilers
