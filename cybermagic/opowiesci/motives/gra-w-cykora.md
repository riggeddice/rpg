# Motive
## Metadane

* Nazwa: Gra w 'Cykora'.

## Krótki opis

Dwa samochody jadą naprzeciw siebie. Jeden się podda pierwszy i zjedzie na pobocze. Kto będzie cykorem?

## Opis

Dwa samochody jadą naprzeciw siebie. Jeden się podda pierwszy i zjedzie na pobocze. Kto będzie cykorem? W tym motywie chodzi o to 'kto mrugnie pierwszy'. Dwie uparte strony jadą na zderzenie czołowe i to się po prostu nie może skończyć dobrze. W ramach tego motywu podkreślaj upór i niechęć do osiągnięcia celu, bo NIE MOGĘ PRZEGRAĆ! z obu stron. Obie strony są skłonne spalić cały świat, ale żadna z nich nie będzie tą, która się podda pierwsza.

## Spoilers
