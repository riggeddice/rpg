# Motive
## Metadane

* Nazwa: Wsparcie Artylerii

## Krótki opis

KTOŚ ma wsparcie artylerii. Nieprawdopodobnej, odległej siły ognia, acz nieszczególnie precyzyjnej. Pozwala to na zniszczenie ogromnej ilości rzeczy, ale z ryzykiem.

## Opis

KTOŚ ma wsparcie artylerii. Nieprawdopodobnej, odległej siły ognia, acz nieszczególnie precyzyjnej. Pozwala to na zniszczenie ogromnej ilości rzeczy, ale z ryzykiem. W ramach tego motywu daj jednej ze stron możliwość wezwania Artylerii lub pokaż teren pod wpływem Artylerii. Pokaż zniszczenia, niebezpieczeństwa oraz konsekwencje. Graj ciągłym zagrożeniem Artylerii i wpływem tego zagrożenia na działania różnych stron.

## Spoilers
