# Motive
## Metadane

* Nazwa: B Team

## Krótki opis

Jest 'drużyna A'. To ci najlepsi. No i jest... ta ekipa. Drużyna B. Wadliwa, z przegrywami i w ogóle trochę nie do końca.

## Opis

Jest 'drużyna A'. To ci najlepsi. No i jest... ta ekipa. Drużyna B. Wadliwa, z przegrywami i w ogóle trochę nie do końca. Nie znaczy to że zespół graczy to drużyna B - to oznacza, że na sesji pojawia się drużyna B i ma to ogromne znaczenie. Może to sojusznicy zespołu. Może zespół wchodzi z zewnątrz i musi współpracować z drużyną B. Przy tym motywie graj słabościami drużyny B, pokaż czemu są przegrywami. Niekoniecznie poradzą sobie tam, gdzie poradziliby sobie kompetentni agenci. Zespół będzie często za nimi poprawiał lub wykorzystywał ich wady.

## Spoilers
