# Motive
## Metadane

* Nazwa: Miłość, szczenięca

## Krótki opis

Młodzi się kochają. Młodzi robią głupoty. My musimy naprawiać...

## Opis

Młodzi się kochają. Młodzi robią głupoty. My musimy naprawiać... Z jakiegoś powodu działania młodych spowodowały efekt motyla i teraz trzeba to naprawić zanim system się totalnie rozchwieje. A MOGLI PO PROSTU POGADAĆ JAK DOROŚLI! Ale nieee... Więc w ramach tego motywu skupiamy się na demonstracji problemów wynikających z miłości dwóch młodych, zaślepionych osób które nie wiedzą lepiej. Lub jednej osoby do drugiej (nieodwzajemnionej).

## Spoilers
