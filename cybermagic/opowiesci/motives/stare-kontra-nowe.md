# Motive
## Metadane

* Nazwa: Stare kontra nowe

## Krótki opis

Zawsze było w jeden sposób. A teraz ktoś chce, by było zupełnie inaczej. I to zniszczy wszystko.

## Opis

Zawsze było w jeden sposób. A teraz ktoś chce, by było zupełnie inaczej. I to zniszczy wszystko. W ramach tego motywu graj konfliktem między tymi, którzy chcą by zostało po staremu a tymi, którzy chcą wszystko zmienić. Konserwatyści chcą, by nic się nie zmieniło nie dbając o ludzki koszt braku zmian. Postępowcy nie patrzą na to co działa, chcą osiągnąć swoje cele i nieważne jest to jak było i że to działało.

## Spoilers
