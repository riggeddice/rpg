# Motive
## Metadane

* Nazwa: The Resurrector

## Krótki opis

"Powrócisz do życia". The Resurrector próbuje wskrzesić, reanimować lub przywrócić do tego świata coś, co jest zasealowane / nieżywe / zniszczone.

## Opis

### 1. Co reprezentuje

"Powrócisz do życia". The Resurrector próbuje wskrzesić, reanimować lub przywrócić do tego świata coś, co jest zasealowane / nieżywe / zniszczone. Niezależnie od tego, czy chodzi o autowskrzeszenie, wskrzeszenie dawno zmarłego władcy, czy też przywrócenie życia ukochanej osoby, The Resurrector wykorzystuje wiedzę tajemną, zakazane rytuały lub naukowe eksperymenty przekraczające znane granice etyki i możliwości. To nigdy nie jest proste i nigdy nie jest tanie.

### 2. Kiedy wygrywa

The Resurrector odnosi sukces, gdy uda mu się osiągnąć swój cel i przywrócić życie temu, co było zniszczone lub utracone. Jego zwycięstwo niekoniecznie oznacza spełnienie oczekiwań ani pozytywny wynik. Po drodze wskrzeszenia może zostawić za sobą pasmo zniszczeń i zgliszczy i złamanych żyć.

### 3. Jak działa

W zależności od sytuacji, jego metody mogą wymagać poszukiwań pozyskania rzadkich komponentów albo znalezienie dobrowolnej ofiary. Często przeprowadza niebezpieczne eksperymenty lub wykonuje zakazane rytuały. Nierzadko wymaga to ogromnych ofiar lub osobistych poświęceń. Cel przekracza granice moralne lub etyczne; The Resurrector skupia się na celu nade wszystko.

The Resurrector może być wspierany przez Kult, lub być mędrcem mającym wiedzę większą niż inni.

### 4. Jak wykorzystywać ten motyw

Skup się na eksploracji motywów przekraczania granic oraz na obsesji The Resurrector - czy to, co próbuje wskrzesić jest warte tych poświęceń? Graj kontrastem między pierwotnymi intencjami a rzeczywistymi skutkami. Pokaż fundamentalnie pozytywną agendę w połączeniu z koszmarnymi metodami - i osobę idącą w głąb, bo nie ma innego wyjścia.

Ile The Resurrector musiał poświęcić? Komu oddał się w służbę? Ile musiał poświęcić? Ile zapłaci samemu? Ile zapłacą inni, niewinni? Czy osoba wskrzeszana pragnęłaby tego?

To Dark Agenda powiązana ze stratą, z którą się nie można było pogodzić, albo z rozpaczliwą ucieczką przed nieubłaganym.

### 5. Przykłady

* "Full Metal Alchemist", bracia Elric próbują wskrzesić matkę.
* "When Dreams Collide". Główny przeciwnik. 'I love him.'

## Spoilers

