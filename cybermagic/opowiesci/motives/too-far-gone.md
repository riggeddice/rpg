# Motive
## Metadane

* Nazwa: Too Far Gone

## Krótki opis

Ktoś może był do uratowania, ale zrobił coś nieodwracalnego. Nie można już tej osoby uratować. Ta osoba sama nie uważa się już za człowieka.

## Opis

Ktoś może był do uratowania, ale zrobił coś nieodwracalnego. Nie można już tej osoby uratować. Ta osoba sama nie uważa się już za człowieka. "Poszłam za daleko, z tego nie ma możliwości powrotu". To jest sytuacja, w której inni też patrzą na tą osobę jako na "wściekłego psa". Z litości - odstrzelić.

## Spoilers
