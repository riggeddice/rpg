# Motive
## Metadane

* Nazwa: Szkolenie dzieciaka

## Krótki opis

Jedna ze stron ma ze sobą 'dzieciaka', którego chce przy okazji wyszkolić. Niech dzieciak nabierze praktyki.

## Opis

Jedna ze stron ma ze sobą 'dzieciaka', którego chce przy okazji wyszkolić. Niech dzieciak nabierze praktyki. Oczywiście, dzieciak strasznie próbuje się wykazać i pokazać swoją przydatność, ale mimo pewnej kompetencji zupełnie nie ma umiejętności zarządzania swoimi zasobami i energią. W ramach tego motywu graj porywczością i próbami wykazania się ze strony dzieciaka, jak i tym że nie wszystko dzieciakowi wychodzi.

## Spoilers
