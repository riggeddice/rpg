# Motive
## Metadane

* Nazwa: Bardzo groźne potwory

## Krótki opis

Obecność bardzo potężnych potworów wypacza sesję lub jest celem sesji.

## Opis

Na tej sesji potwory stanowią znaczącą siłę wypaczającą sesję lub będącą przyczyną sesji. Potwory te mają swoje działania i w jakiś sposób konfliktują się z dominującymi siłami sesji.

## Spoilers

* **Mroczne serce**
    * **Głód**: "To potwory są dominatorami tego terenu, nie ludzie"
        * ludzie czują się bezradni i przerażeni, utracili swoją autonomię.
    * **Ofiara**: swoboda, bezpieczeństwo
        * ludzie nie są w stanie działać w normalny sposób przez obecność potworów
        * bezpieczeństwo i pokój zostają poświęcone, jako potwory niszczą miasta i wieś.
        * ludzie obracają się przeciw sobie próbując przetrwać
* **Agenda - co chcesz osiągnąć**: 
    * wchodź w szkodę ludziom, zagrażając im lub infrastrukturze
    * pokaż potęgę i niebezpieczeństwo obecności potwora. Demonstruj siłę.
    * poluj na ludzi lub coś, co wchodzi w szkodę ludziom
* **Dark Future - wizja maksymalnej porażki Graczy**:
    * Wszystko, na czym innym stronom zależało zostaje zniszczone
    * Potwory jako kataklizm, niszczący wszystko wypracowane przez ludzi
