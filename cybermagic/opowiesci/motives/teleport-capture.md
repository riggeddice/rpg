# Motive
## Metadane

* Nazwa: Teleport capture

## Krótki opis

Dotychczas bezpieczny sposób transportu został przejęty przez Łowcę. Teleportacja (lub inny sposób transportu) jest przekierowana tam, gdzie zależy Łowcy.

## Opis

Dotychczas bezpieczny sposób transportu został przejęty przez Łowcę. Teleportacja (lub inny sposób transportu) jest przekierowana tam, gdzie zależy Łowcy. Ten motyw jest powiązany z Black Technology, Anomaliami lub bardzo zaawansowanym magitechem, w innym wypadku nie mamy bezpiecznego sposobu transportu. W ramach tego motywu dostajesz dostęp do Teleport Capture - kto i dlaczego próbuje przechwycić (co?). Jeśli odpowiesz na te pytania, określisz agendę Łowcy i interakcję otoczenia z Łowcą i agendą Łowcy, masz ciekawą scenę.

## Spoilers
