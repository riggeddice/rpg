# Motive
## Metadane

* Nazwa: O dziwo normalny kompetentny agent

## Krótki opis

W tej opowieści występuje kompetentny agent jakiejś strony. Jest po prostu normalny i kompetentny w tym co robi. Mimo, że wszystko wskazuje że jest z nim coś nie tak.

## Opis

W tej opowieści występuje kompetentny agent jakiejś strony. Jest po prostu normalny i kompetentny w tym co robi. Mimo, że wszystko wskazuje że jest z nim coś nie tak. W ramach tego motywu masz coś co wygląda na niewłaściwe / dziwne. Osoba zachowująca się niepokojąco. W praktyce to jest jednak normalna osoba.

## Spoilers
