# Motive
## Metadane

* Nazwa: The Inquisitor

## Krótki opis

"Skażenie i zło będą wyplenione". Ekstremistyczny z natury The Inquisitor skupia się na usunięciu zła i Skażenia z danego obszaru czy osoby.

## Opis

"Skażenie i zło będą wyplenione". Ekstremistyczny z natury The Inquisitor skupia się na usunięciu zła i Skażenia z danego obszaru czy osoby. The Inquisitor nie musi posiadać twardych dowodów na to, że coś złego tu się znajduje. Nawet szansa na to, że Skażenie czy Nieczystość mogła dotknąć jakąś osobę bądź społeczeństwo jest wystarczająca by The Inquisitor próbował rozwiązać ten problem. Głęboko oddany Czystości i swojej sprawie, nie negocjuje z bytami Skażonymi albo niegodziwymi.

W ramach tego motywu graj oddaniem, fanatyzmem oraz bezwzględnością The Inquisitor. Zwykle jest dobrze wyposażony i zwykle dużo wie o sprawie. Pokaż czarno-białe spojrzenie. Pokaż też oddanie Sprawie - próbę uratowania i ochrony ludzi i populacji. Możesz grać konfliktem pomiędzy pomocą ludziom i czyszczeniem problemu.

The Inquisitor wygrywa wtedy, gdy wszystko co nie czyste i Skażone stanie się czyste i piękne. Albo pod kontrolą, albo zniszczone albo wyczyszczone. 

## Spoilers
