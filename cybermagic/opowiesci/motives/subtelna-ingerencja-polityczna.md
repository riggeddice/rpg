# Motive
## Metadane

* Nazwa: Subtelna ingerencja polityczna

## Krótki opis

Coś niewłaściwego się dzieje. Więc Ktoś robi niewielkie ruchy polityczne - poinformowanie, przesunięcie - by temu zapobiec lub by stało się to co powinno.

## Opis

Coś niewłaściwego się dzieje. Więc Ktoś robi niewielkie ruchy polityczne - poinformowanie, przesunięcie - by temu zapobiec lub by stało się to co powinno. Ten motyw jest wykorzystywany by coś 'nieprawdopodobnego' jednak się zdarzyło, czy właściwa osoba podejmie 'odpowiednią' akcję. Osoba za to odpowiedzialna pozostaje politycznie 'czysta' a wszystko idzie jak powinno. W ramach tego motywu skup się na wyniku ingerencji a nie na samej ingerencji - czemu ktoś wykonał ruch po to, by doprowadzić do zaistnienia danej sytuacji? Kto to jest i co zyskuje? Przeciwko czemu się blokuje?

## Spoilers
