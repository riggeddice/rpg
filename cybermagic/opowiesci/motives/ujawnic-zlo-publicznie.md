# Motive
## Metadane

* Nazwa: Ujawnić zło publicznie

## Krótki opis

Ktoś w przeszłości zrobił coś bardzo złego. A teraz ktoś inny próbuje to ujawnić, by zniszczyć zbrodniarza.

## Opis

Ktoś w przeszłości zrobił coś bardzo złego. A teraz ktoś inny próbuje to ujawnić, by zniszczyć zbrodniarza. W ramach tego motywu skup się na konsekwencjach ujawnienia jak i konsekwencjach ukrycia. Desperackie ruchy strony próbującej ukryć sekret i determinacja strony ujawniającej. Przekonywanie i znajdowanie dowodów dla szerszej publiczności. Podkreśl walkę o prawdę, niebezpieczeństwo związane z odkrywaniem tajemnic i konsekwencje ujawnienia sekretu. Pokaż też reakcję społeczeństwa na odkrycie i konsekwencje dla zbrodniarza.

## Spoilers
