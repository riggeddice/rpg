# Motive
## Metadane

* Nazwa: Misja ratunkowa to pułapka

## Krótki opis

Ratownicy próbują uratować nieszczęśników. Ale w praktyce to była pułapka mająca złapać ratowników.

## Opis

Misja, która z pozoru miała na celu ocalenie niewinnych, była w rzeczywistości starannie przygotowanym planem, mającym na celu złapanie ratowników. Pomimo wpadnięcia w pułapkę, ratownicy są w stanie uratować część nieszczęśników, ale dużym kosztem. Muszą borykać się nie tylko z problemami wynikającymi z pierwotnej misji, ale również z nowo powstałymi trudnościami. Tymczasem ci, co zastawili pułapkę się świetnie przygotowali i są gotowi, ale to nie znaczy, że wszystko łatwo pójdzie zgodnie z planem.

## Spoilers
