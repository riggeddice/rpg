# Motive
## Metadane

* Nazwa: Niestabilna magia

## Krótki opis

Z jakiegoś powodu magia nie działa tak jak zawsze; jest mniej stabilna, nie można jej do końca ufać.

## Opis

Z jakiegoś powodu magia nie działa tak jak zawsze; jest mniej stabilna, nie można jej do końca ufać. W ramach tego motywu pokaż jak magia generuje nieoczekiwane efekty, kuś używaniem magii i wypaczaj wszelkie efekty by wychodziło ogólnie dziwniej.

## Spoilers
