# Motive
## Metadane

* Nazwa: Skażenie magiczne maszynerii

## Krótki opis

Jakiś ważny budynek, firma, obiekt czy konstrukcja techniczna uległa Skażeniu wypaczającemu naturę tego bytu.

## Opis

Jakiś ważny budynek, firma, obiekt czy konstrukcja techniczna uległa Skażeniu wypaczającemu naturę tego bytu. Ten byt dalej robi to co w przeszłości, ale inaczej, w bardziej _żywy_ sposób. W ramach tego motywu podkreśl nienaturalność maszyny czy lokacji, zagroź ludziom bezpośrednio używając nowo-nabytych mocy anomalicznej maszynerii i pokaż że dzika, niekontrolowana magia jest czymś czego nie przewidzisz i nie kontrolujesz.

## Spoilers
