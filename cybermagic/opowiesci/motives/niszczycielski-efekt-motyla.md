# Motive
## Metadane

* Nazwa: Niszczycielski efekt motyla (failure cascade)

## Krótki opis

Mały nieznaczący ruch małej nieznaczącej osoby doprowadził do czegoś co zagraża sprawnemu systemowi.

## Opis

Mały nieznaczący ruch małej nieznaczącej osoby doprowadził do czegoś co zagraża sprawnemu systemowi. Stało się coś małego: ktoś kogoś zdradził, ktoś się spóźnił na spotkanie… w wyniku czego coś jeszcze poszło nie tak, w wyniku czego kaskada problemów rośnie w formie wykładniczej. W ramach tego motywu skonfrontuj graczy z faktycznie dużym problemem, który już troszeczkę trwa, ale którego praprzyczyną jest coś naprawdę małego. Alternatywnie spraw, by niewielkie działania jakieś strony na oczach graczy zaczęły w absurdalny sposób kaskadować

## Spoilers
