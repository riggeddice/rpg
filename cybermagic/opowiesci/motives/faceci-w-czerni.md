# Motive
## Metadane

* Nazwa: Faceci w czerni

## Krótki opis

My znamy prawdę. Oni nie mogą znać prawdy. Kluczem jest ukrycie czegoś i usunięcie informacji z powszechnej świadomości.

## Opis

My znamy prawdę. Oni nie mogą znać prawdy. Kluczem jest ukrycie czegoś i usunięcie informacji z powszechnej świadomości.

## Spoilers
