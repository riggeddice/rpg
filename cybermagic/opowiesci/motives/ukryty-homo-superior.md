# Motive
## Metadane

* Nazwa: Ukryty Homo Superior

## Krótki opis

Ktoś jest czymś więcej niż zwykły człowiek. Szybszy, lepszy, doskonalszy. Ale to ukrywa. Jest niespodziewanie hiperkompetentny.

## Opis

Ktoś jest czymś więcej niż zwykły człowiek. Szybszy, lepszy, doskonalszy. Ale to ukrywa. Jest niespodziewanie hiperkompetentny. W ramach tego motywu _homo superior_ może unikać wykrycia, może unikać odkrycia tożsamości (bo ktoś na niego poluje) lub próbuje kimś się opiekować. Podczas pracy z tym motywem odbijaj od rzeczy które nie wyglądają "normalnie" (bo homo superior radzi sobie za dobrze) i działaj dookoła wielkiego sekretu.

## Spoilers
