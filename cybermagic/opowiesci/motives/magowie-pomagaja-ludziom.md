# Motive
## Metadane

* Nazwa: Magowie bawią się ludźmi

## Krótki opis

Magowie to ci potężni, homo superior. Więc pomagają ludziom, chcąc być dobrymi członkami społeczności.

## Opis

Magowie to ci potężni, homo superior. Więc pomagają ludziom, chcąc być dobrymi członkami społeczności. W ramach tego motywu, magowie angażują się w społeczność, korzystając ze swojej mocy do pomagania innym i przyczyniania się do dobra ogółu. Te czyny nie zawsze są jednak bezinteresowne. Pomoc może wynikać z poczucia obowiązku, pragnienia aprobaty społecznej, czy zasady równowagi. Bywa, że jest też narzędziem do zdobycia wpływów, kontroli lub innego rodzaju korzyści. W ramach tego motywu pokaż konsekwencje tej pomocy, jak też to, że przez różnorodność magów i agend magów wielu ludzi po prostu nie wierzy w to, że magowie chcieliby im pomagać.

## Spoilers
