# Motive
## Metadane

* Nazwa: Nuisance pests

## Krótki opis

Na tej sesji pojawiają się irytujące szkodniki, które nie są groźne, ale przeszkadzają wszystkim.

## Opis

Na tej sesji pojawiają się irytujące szkodniki, które nie są groźne, ale przeszkadzają wszystkim. Szkodniki stanowią powód do irytacji, ale nie robię nikomu szczególnej krzywdy. Ich obecność jednak skutecznie ludzi irytuje i destabilizuje delikatne i niezbyt stabilne systemy. W tym motywie wykorzystuj szkodniki jako ciągłe źródło irytacji dla różnych stron konfliktu i jakkolwiek nigdy nie są przeciwnikiem głównym, ale powinny wszystkim zaleźć za skórę. Są trudne do usunięcia.

## Spoilers
