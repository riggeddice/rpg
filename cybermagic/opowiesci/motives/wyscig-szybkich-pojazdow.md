# Motive
## Metadane

* Nazwa: Wyścig szybkich pojazdów

## Krótki opis

Szybkie pojazdy i świetni piloci. Tylko jeden może mieć pierwsze miejsce.

## Opis

Szybkie pojazdy i świetni piloci. Tylko jeden może mieć pierwsze miejsce. W ramach tego motywu wykorzystaj wyścig albo jako okoliczności podczas których ma się stać coś ważnego, albo jako przyczynę, bo komuś szczególnie na zwycięstwie zależy. Możesz też podkreślić jakość pojazdów i ich specyfikę. Teren nie musi być zwykły; może być całkiem skomplikowany i niebezpieczny.

## Spoilers
