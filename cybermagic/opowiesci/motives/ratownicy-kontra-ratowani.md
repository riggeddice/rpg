# Motive
## Metadane

* Nazwa: Ratownicy kontra Ratowani

## Krótki opis

Ratownicy chcą ratować Ofiary, ale muszą ich wpierw pokonać / rozbić. Czyli - Ofiary bronią się przed Ratownikami.

## Opis

Ratownicy chcą ratować Ofiary, ale muszą ich wpierw pokonać / rozbić. Czyli - Ofiary bronią się przed Ratownikami. Może to kwestia wpływu mentalnego, może indoktrynacji a może strachu / niezrozumienia sytuacji. Tak czy inaczej, by móc kogoś uratować, musimy z nim walczyć.

## Spoilers
