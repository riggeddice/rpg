# Motive
## Metadane

* Nazwa: Lustra Arazille

## Krótki opis

Arazille odbija się we wszystkich lustrach. Na tej sesji lustra rozumiane jako drzwi do jej mocy pełnią ważną rolę.

## Opis

Arazille odbija się we wszystkich lustrach. Na tej sesji lustra rozumiane jako drzwi do jej mocy pełnią ważną rolę. Lustro może przekształcić magię, może pokazać komuś coś innego, może zamanifestować obecność bogini, może kogoś uwolnić od czegoś. Sny o lustrach mogą prowadzić do Arazille. W ramach tego motywu, graj lustrami i ich specyfiką w kontekście magii i samej Arazille. Pokaż lustra, które robią coś więcej i reakcje ludzi na te lustra.

## Spoilers
