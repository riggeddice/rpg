# Motive
## Metadane

* Nazwa: Dobra impreza

## Krótki opis

Czasem nie chodzi o zabijanie smoków a o dobrą zabawę w unikalnej kulturze.

## Opis

Czasem nie chodzi o zabijanie smoków a o dobrą zabawę w unikalnej kulturze. Ten motyw skupia się właśnie na konstrukcji dobrej zabawy, gry, rozgrywki lub na interakcji z powyższą. W ramach tego motywu daj postaciom możliwość interakcji z unikalnymi cechami kultury robiącej ową zabawę, rywalizacji i ogólnie dobrej, wesołej atmosferze.

## Spoilers
