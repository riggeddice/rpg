# Motive
## Metadane

* Nazwa: Weteran, Genialny

## Krótki opis

Ten Weteran wiele widział, z wieloma rzeczami się spotkał i ogólnie ma rady i działania na większość sytuacji.

## Opis

Ten Weteran wiele widział, z wieloma rzeczami się spotkał i ogólnie ma rady i działania na większość sytuacji. Groźny przeciwnik lub świetny sojusznik, popełnia bardzo mało błędów i realistycznie ocenia sytuację. Ma dostęp do kontaktów i narzędzi. W ramach tego motywu wykorzystuj kompetencję Weterana albo jako przeciwnika albo jako sojusznika, pokaż jego doświadczenie i - jeśli jest sojusznikiem - możesz podnieść stopień trudności by część misji wykonał Wetern nie zabierając wartościowych akcji Graczom.

## Spoilers
