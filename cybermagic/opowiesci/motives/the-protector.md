# Motive
## Metadane

* Nazwa: The Protector

## Krótki opis

"Musisz wpierw przejść przeze mnie". The Protector skupia się na ochronie Księcia we wszystkich istotnych wymiarach i nie pozwoli, by Księciu stała się krzywda.

## Opis

"Musisz wpierw przejść przeze mnie". The Protector skupia się na ochronie Księcia we wszystkich istotnych wymiarach i nie pozwoli, by Księciu stała się krzywda. Książę to może być „młodsza siostra”, „organizacja” lub faktyczny książę. Sam The Protector też może być organizacją, magiczną efemerydą lub bardzo potężnym paladynem. Zwykle The Protector jest bardzo potężny, na zupełnie innym poziomie skali niż inne siły. Nie oznacza to, że The Protector będzie chronić Księcia przed wszystkim co Księcia może czekać. Działa wtedy, kiedy pojawia się problem nieodwracalny i w zakresie tego przed czym ma chronić. Pewnym wariantem tego motywu może być też ojciec, który uniemożliwia komukolwiek zbliżyć się do jego córki.

W ramach tego motywu graj powodem, dla którego Książę jest wspierany przez The Protector. Z czego wynika oddanie? Czy Książę w ogóle jest świadomy, że jest chroniony? Jak daleko The Protector się posunie? Co jest siłą zagrażającą Księciu i czego pragnie?

The Protector wygrywa, jeżeli Księciu nic złego się nie stanie i jeżeli przejdzie przez to nienaruszony.
