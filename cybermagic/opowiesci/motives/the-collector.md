# Motive
## Metadane

* Nazwa: The Collector

## Krótki opis

"Wszystkie będą moje". The Collector to ktoś, kto gromadzi unikatowe byty lub informacje - lub próbuje zdobyć wszystkie egzemplarze czegoś.

## Opis

### 1. Co reprezentuje

"Wszystkie będą moje". The Collector to ktoś, kto gromadzi unikatowe byty lub informacje - lub próbuje zdobyć wszystkie egzemplarze czegoś. Jego istnienie jest zdominowane przez obsesję na punkcie gromadzenia.  The Collector symbolizuje pragnienie posiadania, dążenie do kompletu oraz obsesję, która często przewyższa inne aspekty egzystencji.

Kolekcja to może być, jako inspiracja:

* Symbole potęgi (medale z pola bitwy, arystokratów w swojej kieszeni)
* Egzotyczne i rzadkie encje / bibeloty (znaczki, rzadkie zwierzęta, figurki z dziwnych krajów)
* Sekrety i opowieści ('teczki' na ludzi, rzadkie księgi czarów, mechanizmy odszyfrowywania)
* Wspomnienia i dokonania ('byłem na każdym kontynencie', 'obraziłem każdego prezydenta na świecie', 'wiem jak działa każdy silnik')

### 2. Kiedy wygrywa

The Collector odnosi sukces, gdy jego kolekcja staje się kompletna, kiedy zdobędzie to, czego dotąd nie posiadał. Dla niego sukces to nie tylko posiadanie obiektu, ale i to, co posiadanie danej kolekcji sobą reprezentuje. Władza, tajemna wiedza, prestiż?

### 3. Jak działa

The Collector wykorzystuje swoją rozległą wiedzę i sieć kontaktów do zdobywania kolejnych obiektów swoich pożądań. Może to robić poprzez uczciwe zakupy, wymianę czy rozwiązania siłowe. Posunie się do wszystkiego by zdobyć 'ostatni znaczek', którego nie posiada. O swojej domenie wie bardzo wiele, a swoją kolekcję trzyma w miejscu zwykle niedostępnym dla śmiertelników. Zwykle ma bardzo dużo środków, pieniędzy i agentów. Musi móc zarówno rozbudować jak i utrzymać kolekcję.

### 4. Jak wykorzystywać ten motyw

W ramach tego motywu graj obsesją The Collector. On nie jest w stanie się zatrzymać i mimo, że posiada tak wiele - jest tego wiecznie za mało. Pokaż nieprawdopodobne bogactwo jego kolekcji i pokaż, jak wiele był w stanie już zgromadzić i osiągnąć. Skoncentruj się na obsesji i jej wpływie na osobowość kolekcjonera, jak również na tych, którzy go otaczają. Pokaż wewnętrzną pustkę The Collector, pokaż, jak obsesja zmienia percepcję wartości i jak wpływa na relacje z innymi.

### 5. Przykłady

* Pokemony. Po prostu - każdy koleś z POKEMONÓW próbujący zdobyć je wszystkie.
* Jonah Magnus z "The Magnus Archives": kolekcjonuje wiedzę, informacje o Anomaliach, opowieści i rytuały.
* Thanos z uniwersum Marvela: nie jestem fanem tego przykładu, ale w sumie działa - kolekcjonuje 'kamienie' by uzyskać wielką moc.
* Eirene z Solty Rei
* Postać z anime, która próbuje "pokonać ich wszystkich"
