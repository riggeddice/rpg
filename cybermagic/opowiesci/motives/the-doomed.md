# Motive
## Metadane

* Nazwa: The Doomed

## Krótki opis

"To się tak NIE SKOŃCZY!". The Doomed jest osobą skazaną na zagładę, walczącą przeciwko zaciskającym się kleszczom Ruiny.

## Opis

### 1. Przykładowe warianty

Nadzieja - może da się uratować przez Ruiną? Ale też nieuchronność zbliżającej się Ruiny.

* Bogaty człowiek chory na śmiertelną chorobę, rozpaczliwie próbujący znaleźć lekarstwo przed śmiercią
* Osoba ukąszona przez wampira zmieniająca się w potwora, starająca się zachować człowieczeństwo
* Dawny informator mafii, dookoła którego zaciskają się kleszcze szantażu z poprzedniego życia
* Ofiara broni biologicznej z ograniczonym czasem, rozprzestrzeniająca Zarazę by zmusić władze do poszukiwania lekarstwa

### 2. Co reprezentuje

"To się tak NIE SKOŃCZY!". The Doomed jest osobą skazaną na zagładę, walczącą przeciwko zaciskającym się kleszczom Ruiny. The Doomed reprezentuje niezgodę na tragiczny los i walkę przeciwko niemu do końca, nawet jeśli musi zapłacić bardzo wysoką cenę. Konflikt pomiędzy tym, do czego zmusza go Ruina a jego naturalnym stanem.

Jeśli musi umrzeć, umrze na swoich warunkach. Jeśli może przetrwać, spróbuje przetrwać. Jak daleko The Doomed się posunie? Ile odrzuci by przetrwać lub osiągnąć swój cel? W którym momencie śmierć jest lepsza niż to, by Ruina zwyciężyła?

The Doomed reprezentowany jest przez trzy kluczowe aspekty Silnika Konfliktu:

* Ruina: co sprawia, że The Doomed jest skazany na zagładę?
* Nieuchronność Ruiny: Ruiny nie da się usunąć, można ją jedynie spowolnić. The Doomed jest już martwy.
* Niezgoda na Ruinę: The Doomed uważa, że Ruinę da się zatrzymać i że da się żyć jak dawniej. Że "da się to opanować".

### 3. Gdzie jest silnik konfliktu?

* Normalne osoby dookoła chcą rozwagi, ostrożności i działania niedesperackiego. The Doomed wie, że Ruina się zbliża i że się musi spieszyć.
* Działania The Doomed, które prowadzą do krzywdy ludzi dookoła - niewinnych lub jego bliskich.
* The Doomed zużywa zasoby i krzywdzi ludzi sobie bliskich, by tylko oddalić Ruinę.
* The Doomed wykorzystuje nieetyczne i mroczne rytuały i technologię. Nic nie ma znaczenia, tylko oddalenie Ruiny.

### 4. Kiedy wygrywa

Zwycięstwo The Doomed jest powiązane ze zniszczeniem Ruiny lub akceptacją Ruiny i/lub potencjalnej śmierci.

* "Ruina mnie nie zniszczy. Pokonam ją.": jeśli Ruina jest możliwa do pokonania, The Doomed wygra, jeśli może ją pokonać (np. wyleczyć chorobę).
* "Ostatnia akcja na moich warunkach": The Doomed może uznać, że w obliczu Ruiny śmierć jest preferowaną opcją. Ostatnia akcja na swoich warunkach, osiągnięcie celu jaki może osiągnąć.
* "Ruina zwycięży, ale to nie mój koniec.": jeśli Ruina jest nieuleczalną chorobą, The Doomed może uznać, że śmierć/szpital w otoczeniu bliskich jest akceptowalną opcją. Koniec walki.

### 5. Kiedy przegrywa

Porażka The Doomed jest powiązane ze zniszczeniem Ruiny lub akceptacją Ruiny i/lub potencjalnej śmierci.

* "Czemu nie? Czemu bronić się przed Ruiną?": jeśli Ruina wpływa na ciało i umysł (np. transformacja w potwora), The Doomed może zaakceptować potworyzację i użyć Ruinę jako kolejnego narzędzia.
* "Przegrałem. Odejdę na własnych warunkach.": w obliczu nieuchronności Ruiny i braku możliwości zwycięstwa, The Doomed popełnia samobójstwo (jeśli jeszcze jest w stanie).
* "Jeśli ja nie wygram, nikt nie wygra!": The Doomed skupia się przeciwko wszystkim którzy go powstrzymywali - zginie i będzie cierpiał, ale oni będą cierpieć BARDZIEJ.

### 6. Jak działa

The Doomed jest postacią pełną mrocznego optymizmu - nieważne co się nie dzieje, on tak nie odejdzie i tak nie skończy. Jest w stanie pokonać Ruinę. Jest w stanie sobie poradzić z Ruiną. Jest w stanie potencjalnie ją wykorzystać.

* jeśli to jakaś forma potworyzacji, używa aspektów potwora
* jeśli to jakaś choroba odbierająca mu możliwość poruszania się, używa agentów lub działa z internetu
* jeśli to działania mafii, kieruje mafię przeciwko innym by móc pozyskać tymczasowe środki

The Doomed ignoruje ostrzeżenia i rady, pędząc ku swojemu celowi z niezłomną determinacją. Wszystkie jego działania są napędzane desperacją i świadomością, że nie ma już nic do stracenia - albo coś zrobi, albo może położyć się i wybrać śmierć (lub gorszy los). The Doomed jest gotów zrobić dosłownie wszystko, aby tylko uniknąć Ruiny.

Typowe działania The Doomed są skupione dookoła walki z Ruiną:

* wzmocnienie siebie albo oddalenie Ruiny: ryzykowne eksperymenty medyczne, mroczna magia, black technology
* pozyskiwanie zasobów, by walczyć przeciwko Ruinie
* głębsze zrozumienie Ruiny, by skuteczniej ją oddalić
* wykorzystywanie ludzi dookoła, by tylko spowolnić Ruinę
* wyniszczanie zasobów i pieniędzy swoich i rodziny - nie ulegnie Ruinie!

The Doomed do końca wierzy, że jest w stanie powstrzymać Ruinę. Jeśli The Doomed uzmysłowi sobie nieuchronność Ruiny, najpewniej wchodzi w tryb "ostatniej akcji". 

### 7. Jak wykorzystywać ten motyw

Pokaż, jak bliska jest linia pomiędzy heroizmem a szaleństwem, gdy ktoś walczy ze swoim przeznaczeniem. Pokaż, jak skrajna determinacja daje jednocześnie niesamowity silnik i możliwości, ale jednocześnie niszczy wszystko dookoła. The Doomed fundamentalnie nie jest osobą ZŁĄ - ale desperacka próba uniknięcia Ruiny sprawia, że posuwa się za daleko i robi rzeczy nieakceptowalne. 

### 8. Jakie ma ruchy

Co prawda ruchy The Doomed są nastawione przeciwko Ruinie, ale jeśli ktoś staje mu na drodze, The Doomed może tych ruchów użyć przeciwko niemu.

* 1 - Wykorzystanie Ruiny przeciwko niewinnym ŻEBY pozyskać Zasoby
  * sukces: ma dostęp do potrzebnego Zasobu
  * przykład: Osoba zarażona śmiertelnym wirusem grozi jego rozprzestrzenieniem w miejscu publicznym, chyba, że dostanie pewną kwotę.
* 2 - Wykorzystanie Ruiny do bezpośredniego starcia z przeciwnikiem ŻEBY pokazać siłę / go odepchnąć i osłabić
  * sukces: przeciwnik jest osłabiony (zaufanie, Zasoby, zdrowie) a The Doomed ma lepszą pozycję
  * przykład: Osoba ukąszona przez wampira używa swoich nowo nabytych nadludzkich zdolności, by zastraszyć i przejąć gang, który ją ścigał.
* 3 - Wykorzystanie Zasobów lub ludzi ŻEBY spowolnić Ruinę
  * sukces: The Doomed ma więcej czasu, acz kosztem posiadanych Zasobów
  * przykład: inwestycja dużej ilości pieniędzy, by informatorzy mafii nie powiedzieli mafii prawdy
* 4 - Zawarcie niebezpiecznego paktu lub sojuszu ŻEBY zyskać moc do walki z Ruiną
  * sukces: The Doomed staje się dużo silniejszy, kosztem mrocznych i nieetycznych konsekwencji i zobowiązań
  * przykład: Bogaty człowiek chory na śmiertelną chorobę inwestuje swoje ostatnie zasoby w tajemniczą sektę obiecującą uzdrowienie.
* 5 - Testowanie Eksperymentu na kimś innym ŻEBY zrozumieć Ruinę lepiej
  * sukces: The Doomed wie więcej o Ruinie, jest w stanie ją oddalić i potencjalnie zbudował agenta. Kosztem czyjegoś życia.
  * przykład: Naukowiec poszukujący lekarstwa na swoją chorobę, przeprowadza ryzykowne testy na złapanych ludziach.
* 6 - Poświęcenie czegoś nieodwracalnego ŻEBY osłabić Ruinę
  * sukces: The Doomed jest mniej sobą, ale Ruina zagraża mu trochę mniej.
  * przykład: Osoba chora cyborgizuje, wycinając sobie chore narządy. Traci część pamięci i uczuć, ale kupuje więcej czasu.
* 7 - Konstrukcja Rytuału, Eksperymentu, Pułapki ŻEBY zaatakować Ruinę
  * sukces; Ruina jest odsunięta lub uszkodzona
  * przykład: Rytuał z krwi niewinnych, za pomocą którego Potworyzacja zostaje spowolniona.

Definicja Zasobu:

* coś materialnego ale powszechnego (pieniądze, schronienie...)
* coś unikalnego (rzadki magitech, mroczna technologia, )
* wiedza (o Ruinie, o słabościach Ruiny, o tym czy lekarstwo działa...)
* wpływ (ludzie robią to co chcę, ludzie mi współczują, ludzie nie stają przeciw mnie, ludzie obracają się przeciw moim wrogom...)

### 9. Przykłady

* Osoba ukąszona przez wilkołaka, która JESZCZE nie uległa swojej drugiej naturze. Może używać cech _homo superior_, ale zaczyna zmieniać się psychicznie.
* Grave z Gungrave; pod koniec anime nie ma dostępu do środków regenerujących, więc się rozpada fizycznie. Albo zakończy sprawę, albo zginie bez zwycięstwa.
* Nike z wybitnego cyklu Bolo; gdy jej psychotronika zaczęła się rozpadać, poprowadziła bohaterską szarżę chroniącą kolonię przed Golemami.
