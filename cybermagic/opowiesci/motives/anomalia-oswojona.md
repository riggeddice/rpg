# Motive
## Metadane

* Nazwa: Anomalia, Oswojona

## Krótki opis

Oswojona potężna anomalia magiczna. Kontrolowana siłą lub miłością. Niezwykle mocna, ale działająca w pewnych granicach. Ale anomalii nigdy nie można w pełni ufać.

## Opis

Oswojona potężna anomalia magiczna. Kontrolowana siłą lub miłością. Niezwykle mocna, ale działająca w pewnych granicach. Ale anomalii nigdy nie można w pełni ufać. Ten motyw reprezentuje 'kontrolowaną' anomalię magiczną, która wykonuje polecenia lub wspiera swojego użytkownika. W ramach tego motywu graj obcością i nienaturalnością anomalii - czy da się ją naprawdę kontrolować? Czy jej dziwne impulsy są możliwe do powstrzymania? Czy można lub warto 'karmić' ją tym co jest potrzebne? Czy człowiek kiedykolwiek może czuć się bezpieczny w obliczu anomalii tego typu?

## Spoilers
