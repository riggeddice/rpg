# Motive
## Metadane

* Nazwa: Gang, oportunistyczny

## Krótki opis

Pojawiła się okazja - władza i prawo działają słabiej. Pojawia się oportunistyczna grupka chętna do zdobycia zasobów kosztem słabszych.

## Opis

Pojawiła się okazja - władza i prawo działają słabiej. Pojawia się oportunistyczna grupka chętna do zdobycia zasobów kosztem słabszych. Nie muszą być bardzo mocni czy bardzo dobrze skoordynowani, w kontekście tego z czym mają do czynienia są wystarczająco dobrzy. Może polują na młode dziewczyny? Może włamują się do domów? Tak czy inaczej - są tu i trzeba jakoś sobie z nimi poradzić.

## Spoilers
