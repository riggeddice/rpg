# Motive
## Metadane

* Nazwa: Mroczna Konspiracja

## Krótki opis

Mroczna konspiracja; grupa ukryta w cieniach i poruszająca nićmi jak pająk. I w tych niciach - postacie.

## Opis

Mroczna konspiracja; grupa ukryta w cieniach i poruszająca nićmi jak pająk. I w tych niciach - postacie. Tak naprawdę nie wiadomo co jest pod kontrolą Organizacji i gdzie się znajduje. Ogólna atmosfera i nastrój zagubienia, 'wtf' i niewiedzy czy dana osoba z którą rozmawiamy jest faktycznie nieświadoma czy jest w mackach Konspiracji. Po drugiej stronie są oficjele i ogólnie potężne postacie. Nigdy nie wiadomo, co można zrobić i z czym mieć do czynienia.

## Spoilers
