# Motive
## Metadane

* Nazwa: Perfect Stealth

## Krótki opis

Jakaś jednostka, strona lub frakcja jest CAŁKOWICIE NIEWYKRYWALNA. Daje jej to niesamowitą przewagę strategiczną.

## Opis

Jakaś jednostka, strona lub frakcja jest CAŁKOWICIE NIEWYKRYWALNA. Daje jej to niesamowitą przewagę strategiczną. Z czego wynika ta niewykrywalność? Czy pozbawi się w którymś momencie niewykrywalności by zrobić kluczowy cel, czy ją zachowa? W ramach tego motywu graj ograniczeniami perfect stealth i tego, jak Druga Strona musi kombinować by wykryć ruchy Niewykrywalnego - jakie to koszty i konsekwencje.

## Spoilers
