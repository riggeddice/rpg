# Motive
## Metadane

* Nazwa: Handel Złym Towarem

## Krótki opis

Handlowanie narkotykami, ludźmi, zwłokami... rzeczami, którymi normalnie nie powinno się handlować.

## Opis

Handlowanie narkotykami, ludźmi, zwłokami... rzeczami, którymi normalnie nie powinno się handlować. W ramach tego motywu skup się na konsekwencjach tego handlu, pokaż jaki ma to też wpływ na handlujących, pomyśl o typie organizacji która się tym zajmuje i jak dochodzi do zakupu i sprzedaży. Uwzględnij też niezadowolenie i aspekty moralne, najlepiej przez pryzmat różnych metakultur.

## Spoilers
