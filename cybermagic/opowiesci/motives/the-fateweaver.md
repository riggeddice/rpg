# Motive
## Metadane

* Nazwa: The Fateweaver

## Krótki opis

The Fateweaver reprezentuje byt, który prowadzi wszystkie pionki do spełnienia swojego planu. Nieważne czy te pionki tego chcą czy nie i czy wiedzą, że są pionkami.

## Opis

The Fateweaver reprezentuje byt, który prowadzi wszystkie pionki do spełnienia swojego planu. Nieważne czy te pionki tego chcą czy nie i czy wiedzą, że są pionkami. Głównymi narzędziami The Fateweaver jest manipulacja wydarzeniami dookoła by każdy robił to co powinien zgodnie z planem tego bytu. W ramach tego motywu graj tym że występuje ogromne szczęście lub ogromny przypadek. Jakoś wszystko układa się w taki sposób jak powinno. Rzeczy które powinny się udać się udają, nieważne jak mało są prawdopodobne.

## Spoilers
