# Motive
## Metadane

* Nazwa: Fish out of water

## Krótki opis

Postać ("ryba") znajduje się w nieznanym i niekomfortowym środowisku.

## Opis

Postać ("ryba") znajduje się w nieznanym i niekomfortowym środowisku. W tym motywie graj nieporadnością lub niewiedzą postaci, zachowującą się niewłaściwie czy źle czytającą sytuację. To nie musi być tylko komedia ("Major Payne", "Full Metal Panic"), można grać też tym motywem w poważnych klimatach ("dzieci po wojnie na terenie bez wojny i dziwne nawyki"). Tak czy inaczej - w ramach tego motywu wykorzystuj różnice kulturowe i oczekiwania oraz zupełnie inne skillsety Ryby i Otoczenia.

## Spoilers
