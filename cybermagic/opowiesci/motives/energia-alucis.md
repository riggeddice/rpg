# Motive
## Metadane

* Nazwa: Energia Alucis

## Krótki opis

Energia Alucis. Ukojenie przez Ułudę, Sen - Piękno - Wolność. Lustra uwalniające od rzeczywistości.

## Opis

Energia Alucis. Ukojenie przez Ułudę, Sen - Piękno - Wolność. Lustra uwalniające od rzeczywistości. W ramach tego motywu kuś Ukojeniem, pokaż wpływ uspokojenia i Pięknego Snu. Graj zwłaszcza konfliktem między Snem jednostki a Cierpieniem rodziny.

## Spoilers
