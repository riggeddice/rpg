# Motive
## Metadane

* Nazwa: Crowd Control

## Krótki opis

Jest duża populacja, która coś chce osiągnąć. Crowd Control to sposób na kontrolę tłumu, by wyłączyć ich możliwości działania.

## Opis

Jest duża populacja, która coś chce osiągnąć. Crowd Control to sposób na kontrolę tłumu, by wyłączyć ich możliwości działania. Grupa (mob, tłuszcza...) jest czymś więcej niż zbiorem jednostek a CC jest mechanizmem radzenia sobie z grupami - czy przez użycie gazów bojowych, broni sonicznej czy czegoś tego typu. W ramach tego motywu dostarcz jednej ze stron narzędzi CC i pokaż jak mogą działać lub umożliw ich wykorzystanie.

## Spoilers
