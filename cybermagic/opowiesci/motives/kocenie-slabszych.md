# Motive
## Metadane

* Nazwa: Kocenie słabszych

## Krótki opis

Ktoś kogoś koci i się nad kimś znęca. To prowadzi do nieoczekiwanych problemów.

## Opis

Ktoś kogoś koci i się nad kimś znęca. To prowadzi do nieoczekiwanych problemów. Kocenie całkowicie zmienia dynamikę grupy - część osób bardzo próbuje się nie pokazywać, część staje za kocącym a kocony zwykle zostaje sam. W ramach tego motywu pokaż implikacje kocenia i zogniskuj jakieś wazne wydarzenie dookoła nieracjonalnego zachowania agentów podległych koceniu bądź kocących.

## Spoilers
