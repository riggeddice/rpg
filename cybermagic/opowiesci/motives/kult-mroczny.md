# Motive
## Metadane

* Nazwa: Kult, mroczny

## Krótki opis

Tam, gdzie jest potęga, tam są ludzie chętni do uzyskania tej potęgi. Czasem za wszelką cenę.

## Opis

Tam, gdzie jest potęga, tam są ludzie chętni do uzyskania tej potęgi. Czasem za wszelką cenę.

Oni wiedzą, że to co robią jest niezgodne z zasadami społecznymi. Oni bardzo często wiedzą, że to co robią jest bardzo niebezpieczne. Często jednak taki kult to jedyna rodzina, którą mają niektórzy. Inni tak długo byli w kulcie, że myślą jak kultyści. Jeszcze inni mogą myśleć w dzięki temu kultowi mogą osiągnąć coś więcej, wierząc, że pomagają.

Tak czy inaczej tego typu kult jest miejscem niebezpiecznym. Używają sił, których nie do końca rozumieją, używając magii, która niekoniecznie jest pod ich kontrolą i ostatecznie może to się skończyć katastrofą.

Pokaż jak bardzo kult oplątał okolicę. Pokaż wpływy oraz możliwości takiego kultu. Pokaż, że nawet nie wiadomo kto do kultu należy a kto nie. Każdy może być jednym z nich. 

## Spoilers
