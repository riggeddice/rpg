# Motive
## Metadane

* Nazwa: Tool of Corruption

## Krótki opis

Narzędzia korupcji; splugawienia wszystkiego co czyste i piękne w mroczne lustro będące parodią. Narkotyki, artefakty...

## Opis

Narzędzia korupcji; splugawienia wszystkiego co czyste i piękne w mroczne lustro będące parodią. Narkotyki, artefakty. Coś, co wzmacnia mroczne instynkty ofiary i przekształca cnoty w ich mroczną stronę. Pragnienie pomocy staje się berserkerskim szałem, chęć oddania głosu innym - uległością. W ramach tego motywu wprowadź Narzędzia Korupcji i pokaż ich wpływ na niewinne ofiary, jak stają się czymś mniejszym i jak ich cnoty są przekształcone w przywary. Kluczem do tego motywu jest to, że motyw sprawia że stajesz się czymś _mniej_, ale nie postrzegasz tego w ten sposób.

## Spoilers
