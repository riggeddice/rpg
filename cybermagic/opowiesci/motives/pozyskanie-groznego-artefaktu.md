# Motive
## Metadane

* Nazwa: Pozyskanie groźnego artefaktu

## Krótki opis

Istnieje bardzo niebezpieczny artefakt, który właśnie wpadł w ręce kogoś kto go mieć nie powinien.

## Opis

Istnieje bardzo niebezpieczny artefakt, który właśnie wpadł w ręce kogoś kto go mieć nie powinien. W ramach tego motywu graj możliwością użycia artefaktu lub tym, że już został użyty. Pokaż Pęknięcie Rzeczywistości w małej skali. Pokaż Skażenie tej klasy, z którą normalne siły w tej opowieści sobie po prostu nie poradzą.

## Spoilers
