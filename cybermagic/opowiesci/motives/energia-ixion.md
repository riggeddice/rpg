# Motive
## Metadane

* Nazwa: Energia Ixion

## Krótki opis

Energia Ixion. Adaptacja kosztem wszystkiego nie-kluczowego. Odrzucenie człowieczeństwa dla skuteczności. Transorganizacja.

## Opis

Energia Ixion. Adaptacja kosztem wszystkiego nie-kluczowego. Odrzucenie człowieczeństwa dla skuteczności. Transorganizacja.

Energia transformacji i dopasowania się do wrogiego otoczenia. Nieważne co się nie stanie w jakiejś formie przetrwamy i nie ma co płakać za ludzkością, bo gdyby była doskonała to nie wymagałaby tych przekształceń. Nieważne, czy częściowo zmieniamy się w maszyny czy stajemy się bytami cyfrowymi. Niezależnie od okoliczności zaadoptujemy się i będziemy najpotężniejszymi i najgroźniejszymi organizmami.

W ramach tego motywu graj zarówno zwiększeniem potęgi jak i odrzucaniem rzeczy, które chcielibyśmy zachować, ale których musimy się pozbyć by móc stać się najlepsi. Fundamentalnie jest to jedna z najbardziej nieludzkich energii – optymalizacja, skuteczność, absolutna bezwzględność w dążeniu do celu. 

## Spoilers
