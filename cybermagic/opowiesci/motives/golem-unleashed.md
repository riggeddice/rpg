# Motive
## Metadane

* Nazwa: Golem Unleashed

## Krótki opis

Golem - sztuczny twór, potężny i pod perfekcyjną kontrolą. O - już nie. Wyrwał się spod kontroli i trzeba go zatrzymać. 

## Opis

Golem - sztuczny twór, potężny i pod perfekcyjną kontrolą. O - już nie. Wyrwał się spod kontroli i trzeba go zatrzymać. Ten motyw skupia się na Golemie - czymś stworzonym, mającym konkretną funkcję o czym wszyscy uważają że jest kontrolowane. Czym jest Golem, jaką ma funkcję i co sprawiło że wyrwał się spod kontroli (fundamentalnie wykonując podobne czynności jak te do których był zaprojektowany)?

## Spoilers
