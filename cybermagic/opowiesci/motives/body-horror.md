# Motive
## Metadane

* Nazwa: Body Horror

## Krótki opis

Przekształcenie ludzkiego ciała w coś koszmarnego, zwykle z efektem szoku.

## Opis

Przekształcenie ludzkiego ciała w coś koszmarnego, zwykle z efektem szoku. Najczęściej ten motyw pojawia się w połączeniu ze Skażeniem albo eksperymentem. Ofiara najczęściej jest świadoma tego, co jej zrobiono. Graj tym motywem by pokazać nieludzkość i zagrożenie ze strony siły, która do Body Horroru doprowadziła i by pokazać jak wysokie są stawki.

## Spoilers
