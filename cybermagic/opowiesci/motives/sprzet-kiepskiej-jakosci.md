# Motive
## Metadane

* Nazwa: Sprzęt kiepskiej jakości

## Krótki opis

Niezadbany lub zbyt zużyty sprzęt stanowi istotny komponent. Nie spodziewaliśmy się, że będzie tak źle.

## Opis

Niezadbany lub zbyt zużyty sprzęt stanowi istotny komponent. Nie spodziewaliśmy się, że będzie tak źle. Skup się na demonstracji, jak kiepski sprzęt utrudnia normalne operacje. W ramach tego motywu pokazuj problemy i demonstruj nieoczekiwane komplikacje.

## Spoilers
