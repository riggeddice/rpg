# Motive
## Metadane

* Nazwa: Wojna domowa

## Krótki opis

W domu, mieście lub państwie grupy, które kiedyś co najmniej się tolerowały rzucają się sobie do gardeł walcząc o władzę.

## Opis

W domu, mieście lub państwie grupy, które kiedyś co najmniej się tolerowały rzucają się sobie do gardeł walcząc o władzę. Może być to wojna zbrojna albo zimna. Tak czy inaczej, kiedyś sojusznicy lub co najmniej niechętni współpracownicy aktywnie walczą o władzę przeciw sobie.

## Spoilers
