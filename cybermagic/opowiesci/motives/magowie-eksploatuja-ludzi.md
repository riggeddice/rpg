# Motive
## Metadane

* Nazwa: Magowie eksploatują ludzi

## Krótki opis

Magowie są silniejsi i potężniejsi niż ludzie. I nadużywają tej władzy, traktując ludzi jako zwykłe zasoby. Nie są okrutni, tak jak nie jesteś okrutny wobec np. kur.

## Opis

Magowie są silniejsi i potężniejsi niż ludzie. I nadużywają tej władzy, traktując ludzi jako zwykłe zasoby. Nie są okrutni, tak jak nie jesteś okrutny wobec np. kur. W ramach tego motywu pokaż, że dla magów ludzie nie mają znaczenia jako osoby a tylko jako zasoby.

## Spoilers
