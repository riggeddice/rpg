# Motive
## Metadane

* Nazwa: Teen Drama

## Krótki opis

Niskie intrygi i dokopywanie sobie przez nie do końca dorosłe (emocjonalnie i może fizycznie) postacie.

## Opis

Niskie intrygi i dokopywanie sobie przez nie do końca dorosłe (emocjonalnie i może fizycznie) postacie. Petty, yet inspired. Więcej energii Aktorzy poświęcają na dowalanie sobie i 'to nie ja zrobiłem' i 'to nie moja wina' niż na rozwiązaniu problemu. Rola postaci graczy to zwykle "ci trzeci". Zwykle. Intrygi "o nic ważnego" destabilizują nie tylko grupę ale też jakieś ważne rzeczy. W ramach tego motywu graj: niechęcią bez poważnych przesłanek między postaciami (np. "spojrzał na dziewczynę" a nie "otruł mi psa"), resentymentami, tu nie ma do końca WINNYCH; zwykłe kolizje charakteru i pomniejsze starcia. I pamiętaj - w tym motywie postacie zachowują się nieracjonalnie, preferując swoje ego i "to ja mam rację" nad rozwiązanie problemu.

## Spoilers
