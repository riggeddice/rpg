# Motive
## Metadane

* Nazwa: Broken mind

## Krótki opis

Spotkawszy się z atakiem mentalnym czy czymś niemożliwym do zaakceptowania, ktoś jest złamany mentalnie i emocjonalnie.

## Opis

Spotkawszy się z atakiem mentalnym czy czymś niemożliwym do zaakceptowania, ktoś jest złamany mentalnie i emocjonalnie. Ktoś napotkał coś, przez co nie może być w stanie już działać normalnie. Jego umysł uciekł do innego miejsca. Zachowuje się irracjonalnie i nie akceptuje rzeczywistości jaka przed nim jest. W ramach tego motywu pokaż złamaną osobę i jej nienaturalną reakcję na pozornie normalne rzeczy. Pokaż, że to była droga w jedną stronę. Pokaż, że normalne rozumowanie nie działa. Spraw, by postacie nie wiedziały - pomóc, _coup de grace_, ale co najważniejsze - CO TU SIĘ STAŁO.

## Spoilers
