# Motive
## Metadane

* Nazwa: The Rival

## Krótki opis

"Nie, ON mnie nie pokona!". The Rival próbuje być LEPSZY od swojego Adwersarza. Jest zaślepiony i nie odda honoru ani kompetencji Adwersarzowi.

## Opis

### 1. Co reprezentuje

"Nie, ON mnie nie pokona!". The Rival próbuje być LEPSZY od swojego Adwersarza. Jest zaślepiony i nie odda honoru ani kompetencji Adwersarzowi.

The Rival ma wieczny kompleks bycia GORSZYM od Adwersarza. Może Adwersarz jest bardziej lubiany? Ma lepsze umocowanie polityczne? "Nie zasłużył" na to co ma? Niezależnie od okoliczności, The Rival jest zdeterminowany by wygrać i pokonać Adwersarza, by raz na zawsze udowodnić że jest od niego lepszy.

### 2. Kiedy wygrywa

The Rival wygrywa, jeśli Adwersarz jest upokorzony lub jeśli wygrywa znacząco z Adwersarzem. Samo zwycięstwo może nie wystarczyć - najlepiej, jeśli to jest publiczne.

### 3. Jak działa

The Rival działa bardzo osobiście. Skupia się na tym co robi Adwersarz i próbuje osiągnąć to lepiej lub szybciej. Jeśli działania Adwersarza mogą osiągnąć skutek przed działaniami The Rival, The Rival może zadziałać wyprzedzająco i bardzo ryzykownie. Jeśli The Rival potrzebuje pomocy Adwersarza, rozpala to jeszcze większy ogień w jego sercu. Jeśli Adwersarz prosi o pomoc The Rival, ten może pomóc, ale pokaże jak bardzo jest LEPSZY od Adwersarza.

The Rival jest osobą kompetentną - musi być - ale fundamentalnie ma _Great Flaw_. Coś, co sprawia, że jest _mniejszy_ i co sprawia, że nie osiągnie wielkości którą by mógł osiągnąć. Ta wada może być wykorzystana przez Trzecią Stronę, sterując The Rival.

### 4. Jak wykorzystywać ten motyw

W ramach tego motywu pokaż, jak zaślepienie The Rival prowadzi do robienia niepotrzebnych zniszczeń. Jak The Rival skupiając się na swojej obsesji jest 'mniejszy', mniej wartościowy i jest sam dla siebie wrogiem.

### 5. Przykłady

* Vegeta dla Goku z Dragon Ball ("muszę być najsilniejszy i najlepszy")
* Asuka dla Shinji z Evangeliona ("jestem lepsza od Ciebie, ale nie jestem Twoim wrogiem")

## Spoilers
