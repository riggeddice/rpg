# Motive
## Metadane

* Nazwa: Karne podłe zadanie

## Krótki opis

Ktoś coś zrobił. Musi zrobić 'shit-taska', np. 'szczoteczką umyć kibel'. Oczywiście, nikt nie lubi robić takich rzeczy...

## Opis

Ktoś coś zrobił. Musi zrobić 'shit-taska', np. 'szczoteczką umyć kibel'. Oczywiście, nikt nie lubi robić takich rzeczy... W ramach tego motywu możesz przesunąć odpowiednie postacie w miejsce, w które normalnie by nie poszły i korzystać z tego. Podkreśl bezwartościowość zadania, niekompatybilność jego z osobami wykonującymi. Możesz też grać co zrobi osoba by tylko nie robić tego bullshitowego zadania.

## Spoilers
