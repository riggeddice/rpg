## Metadane

* title: "Echo z odmętów przeszłości"
* threads: brak
* motives: anomalia-oswojona, dark-stimulants, energia-alucis, energia-alteris, energia-sempitus, izolacja-zespolu, rekonstrukcja-ruiny, historie-sprzed-pekniecia, teren-odmety-oceanow
* gm: fox
* players: igor, oliwia, random-1

## Kontynuacja
### Kampanijna

* [240117 - Echo z odmętów przeszłości](240117-echo-z-odmetow-przeszlosci)

### Chronologiczna

* [230813 - Jedna tienka przybywa na pomoc](230813-jedna-tienka-przybywa-na-pomoc)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * .
* Opowieść o (Theme and vision): 
    * Praca dla korporacji nie kończy się po śmierci
* Motive-split
    * energia-sempitus: nieśmiertelność, cała placówka chce żyć wiecznie. To dotyczy martwych ludzi i systemów. Systemy dadzą się naprawić, a nawet pomogą.
    * energia-alucis: ułuda, wszystko w placówce jest tak jak było dawniej, tylko mamy kilka drobnych problemów i brak kontaktu z centralą.
    * energia-alteris: wewnątrz podwodnej placówki działa jak na powierzchni [chłodno, lekka bryza, zapach morza]
* Detale
    * Crisis source: 
        * HEART: ""
        * VISIBLE: ""
    * Delta future
        * DARK FUTURE: Stacja jest skażona magiczne, więc z litości dla skażonych ludzi kapitan decyduje się ją zniszczyć.
        * LIGHT FUTURE:
            * chain 1: Gracze znajdują dla postaci na stacji sposób na dalsze życie
            * chain 2: Gracze pomagają postaciom na stacji spocząć w pokoju, oczyszczając anomalię*
    * Dilemma: czy po śmierci powinno się przywracać ludzi do życia? Czy nieumarli mają prawa? 

### Co się stało i co wiemy

* Podwodna stacja badawcza korporacji Medimmortal, założona koło 10 lat przed pęknięciem rzeczywistości. Miała zajmować się badaniem flory, fauny i minerałów w rowie Penelope, na którego skraju się znajduje.
    * Celem korporacji było przedłużenie życia ludzkiego poza granice biologicznych możliwości ciała, do tych celów badano florę i faunę z różnych części wszechświata. Na Astorii wykryto obiecujące stworzenie, Viverejuvenis (Virenis) - "Żywa Młodość" - homaropodobne stworzenie o wysokiej inteligencji, które podczas wylinki jest w stanie regenerować swoje ciało, utrzymując młodość.
* Gdy przyszło rozdarcie rzeczywistości placówka straciła kontakt z powierzchnią, a seria awarii doprowadziła do uszkodzeń w placówce, zalania jej i śmierci załogi. Oraz powstania silnej anomalii alucis + alteris + sempitus .
    * Sempitus - nieśmiertelność, cała placówka chce żyć wiecznie. To dotyczy martwych ludzi i systemów. Systemy dadzą się naprawić, a nawet pomogą.
    * Alucis - ułuda, wszystko jest tak jak było dawniej, tylko mamy kilka drobnych problemów i brak kontaktu z centralą.
    * Alteris - wenątrz = powierzchnia [chłodno, lekka bryza, zapach morza]
    * Anomalia rozchodzi się powoli, ale nie po wodzie, co oznacza, że będzie powoli promieniować na “wewnątrz” połączone do bazy. Anomalia przenosi się tylko przez pomieszczenia. Elektronika w niej po prostu działa, trupy wstają, wewnątrz staje się powierzchnią, a żywy zamienia się w martwy [na przestrzeni długiego czasu].

### Co się stanie (what will happen)

.

## Sesja - analiza
### Fiszki

* Elżbieta Sanchez: Kierownik Badawczy, Profesor
    * Ekspertka od farmaceutyki, kosmetyki i chemii 
    * Udaje oddaną korporacji oraz badaniom dla niej, jednak w rzeczywistości zależy jej tylko na osiągnięciu długowieczności dla siebie. Jednak chce też zachować młody wygląd, dba o cerę, robi projekty na boku, którymi się nie dzieli robiąc specyfiki dla siebie.
    * Elżbieta nie będzie chciała, by zespół opuścił stację. Niezależnie od wszystkiego, ona chce kontynuować. 
* Jędrzej Sanchez: Specjalista ds. Bezpieczeństwa, Inżynier
    * Ekspert ds. bezpieczeństwa placówki badawczej, nadzorujący zabezpieczenia i środki ostrożności, również “szef ochrony”. Ekstramalnie znudzony i unika realnej pracy. Najchętniej do końca życia siedziałby w placówce, bo tu się nic nie robi. Silne więzi rodzinne, ochroni siostrę, mimo że udaje, że mu nie zależy [nie wychodzi]
    * Będzie chciał pozostać, jeśli to możliwe. Nie chce umierać, poprze siostrę. Ma dostęp do systemów awaryjnych stacji, może zarządzić kwarantannę i lockdown.
* Karolina Jensen: Oceanograf i biolog morski, Profesor
    * Ekspertka od głębokiej oceanografii i biologii morskiej.
    * Miała być kierownikiem badawczym, ale Elżbieta była bardziej lubiana przez kierownika [podejrzewa, że zaskarbiła sobie jego sympatię. Ma rację]. Zazdrosna o Elżbietę. Obecnie stosunkowo nieprzydatna, jej czujniki do badań nie działają - były w pewnym oddaleniu, teraz nie wiadomo nawet czy istnieją. Jej czujniki lokalne pokazują, czasem dziwne i potencjalnie niebezpieczne odczyty, dlatego rekomenduje nie wychodzić.
    * Jeśli Elżbieta chce kontynuować to ona nie chce. Jeśli to możliwe chce wrócić na powierzchnię. Po naprawieniu czujników może być oczami graczy na zewnątrz.
    * Będzie załamana, że nie żyje. Da się przekonać komuś, komu ufa, ale na pewno nie Elżbiecie.
* Jakub Rasmussen: Biolog Morski, Profesor
    * Badacz lokalnej fauny i flory, ze szczególnym uwzględnieniem potencjalnych zastosowań medycznych.
    * Zajmuje się viverejuvenis w akwarium. 
    * Starszy i zmęczony życiem. Bardzo zasłużony, bardzo inteligentny i obeznany w swoich dziedzinach. Jeśli dowie się, że wszyscy nie żyją to on chce odejść. Dość już zrobił.
    * Nie chce harować do końca wieczności. Będzie chciał odejść, zaproponuje wyjście w skafandrach na zewnątrz, ale nikogo przekonywał nie będzie. Może sam wyjdzie.
* Mateusz Schmidt: Lekarz Morski, Profesor
    * Lekarz specjalizujący się w medycynie podwodnej i analizie wpływu środowiska morskiego na zdrowie ludzkie.
    * Robi checkupy załodze, wszystko jest  zawsze ok. Za długo był bezczynny, skoro korpo nie istnieje to nie dostanie wypłaty, chce wrócić i próbować ułożyć sobie życie na nowo. Chce do domu. Po prostu. Zostawił tam żonę i dzieci… Chce się dowiedzieć, co się z nimi stało.
    * Jeśli gracze zaproponują to on wchodzi na pokład. Potencjalna pierwsza ofiara.
* Anna Ivanova: Główny Inżynier, Inżynier
    * Stara się, by wszystko działało. Chciałaby odzyskać zalane pomieszczenia, oczyścić life support [dziwnie pachnie], generalnie uważa, że dzięki niej to wszystko jakoś działa. Ma podejście - jak działa nie dotykaj, bo zepsujesz. Stosunkowo wyluzowana, lubi jak jest zrobione po kosztach i lubi komfort. Scrapperka i mistrzyni improwizacji. Odkąd elektryk wyszedł i nie wrócił, nie odważyła się wyjść.
    * “Nie dotykaj, bo zepsujesz”, ale tęskni za jakimś wyzwaniem. Chciałaby zrobić coś więcej niż w kółko naprawiać działające systemy.
* Robert Tisso: Główny elektryk, Inżynier
    * Wyszedł sprawdzić co z generatorami energii, od razu po opuszczeniu umarł.
* Gerald Barowiecki: Kapitan i sponsor Kelpie
    * Kapitan spełnia swoje marzenie o podmorskiej przygodzie. Bardzo poczciwy człowiek, chce pomóc, ale wie, że nie jest specjalistom, wycofa się jeśli zobaczy taką potrzebę.

#### Strony

* Załoga łodzi podwodnej Kelpie
* Załoga stacji badawczej Medimmortal

### Scena Zero - impl

Gracze wykonują testowe zanurzenie łodzią podwodną Kelpie. Celem łodzi jest dotrzeć do odkrytej wcześniej rafy koralowej Prisma, położonej na głębokości 200 metrów pod wodą, pobrać z niej próbki i powrócić. 

Na miejscu szybkie oględziny rafy dronami wykazują, że jest ona poddawana działaniu silnego podwodnego prądu Alteris, który jest zresztą bezpośrednią przyczyną występowania rafy na tej głębokości. Podczas badania rafy drona znajduje też archaiczną sondę oceaniczną sprzed pęknięcia, przysypaną piaskiem w samym środku rafy.

Oficer ochrony, wyposażony w lapisowany skafander do nurkowania wychodzi z łodzi i płynie na teren rafy, by odzyskać sondę. Przy użyciu swojej siły wyrywa ją z piasku, jednak zmieniające swój układ kamienie zagradzają mu drogę powrotu. Dyplomata z pokładu statku  wysyła nurków, by okrążyli rafę i czekali w pogotowiu. Inżynier wychodzi w swoim ciężkim skafandrze w stronę rafy i przy użyciu narzędzi górniczych pozbywa się kamiennej kopuły i wydobywa oficera. Oficer, używając motorka wyciąga ich oboje z niebezpiecznej rafy, wyciągając ze sobą zdobytą sondę. Zostają przechwyceni przez nurków i bezpiecznie wracają na pokład.

Ze starej sondy udaje się wydobyć stare mapy oceanu oraz część danych z jej badań. Inżynier naprawia sondę, włączając ją w swój arsenał.

### Sesja Właściwa - impl

Załoga rusza w miejsce, z którego dochodzi nadawany sonarem sygnał SOS. Trafiają tam na położoną na skraju rowu Penelope stację badawczą. Wysłane zostają drony, mające na celu zbadać stację od zewnątrz. 

* Na stacji panuje jednolita temperatura, koło 10 stopni.
* W okolicy stacji unosi się ciało w kombinezonie, które zostaje zabrane na pokład do sekcji

Sekcja ciała

* Ciało jest w doskonałym stanie, nie zaczęło się rozkładać
* Ciało było w kombinezonie połączone ze stacją, dostawało ze stacji tlen
* Mężczyzna był palaczem, ale jego płuca wydają się być w dobrym stanie
* Żadnych widocznych przyczyn śmierci

Gracze postanawiają wejść na pokład. Łódź łączy się śluzą ze stacją, śluza zostaje przecięta, by załoga mogła wejść do środka. Po chwili gracze w kombinezonach wchodzą na pokład, by po chwili znaleźć się w towarzystwie Jędrzeja, celującego do nich z broni. Przekonują Jędrzeja, że przypłynęli pomóc i muszą się spotkać z dowódcą. Jędrzej prowadzi ich do zarządcy stacji: Elżbiety. Elżbieta chce przede wszystkim odzyskać łączność z centralą Medimmortal - korporacji medycznej, do której należy stacja. Gracze nie znają takiej korporacji, musiała zniknąć po pęknięciu rzeczywistości.

Kojarzą za to głównego CEO Medimmortal, który jest uważany za jednego z założycieli Trójzęba, który wraz ze swoim medycznym zespołem pracował dla odbudowy ludzkości po pęknięciu.

Gracze kierują się do inżyniera, chcą też zdobyć wgląd w technologie, użyte w bazie, jednak Anna nie jest zbyt chętna do udostępnienia - zamiast tego chce ich kierować przez krótkofalówkę. Inżynier z oficerem ochrony wspólnymi siłami próbują przywrócić przekaźniki do komunikacji z powierzchnią.

* Ich operacje zwabiły jakieś stworzenie, które zbliża się do bazy
* ...ale połączenie z powierzchnią zostało ustanowione. 

Gracze idą do jednego z naukowców, Karoliny, dowiadują się, że stacja prowadziła badania nad homarami, które są w stanie wymieniać komórki, by zachować wieczną młodość. “Badania są już na ostatniej prostej”. Oferują jej, że może użyć sprzętu na ich łodzi, by pchnąć badania. Karolina zgadza się i z nimi idzie. Karolina pada martwa od razu po przekroczeniu śluzy. Gracze robią to jeszcze kilka razy, Karolina krzyczy i przybiega Jędrzej, który zabiera ją do medBayu, gracze nie zostają do niej wpuszczeni przez Jędrzeja, który zaczyna krzywo na nich patrzeć.

Gracze decydują się zszyć zmarłego Roberta, którego wyłowiono wcześniej w kombinezonie i zobaczyć, czy zostanie ożywiony.

* Zmarły był bardzo mocno naruszony i zostaną ślady.
* Udaje się w większości zszyć Roberta i dzięki wytworzeniu sztucznej skóry zamaskować bardziej widoczne ślady modyfikacji. Z daleka nikt się nie zorientuje.

Gracze chcą tę bazę wcielić pod swoją organizację. W tym celu muszą przekonać swojego kapitana do swojej wizji oraz zarządcę bazy. U kapitana inżynier i oficer ochrony proponują, by nie mówić osobom z bazy, co się dzieje, chcą udać, że skontaktowali się z centralą i przejąć placówkę po cichu. Poczciwy kapitan dopytuje, by mieć lepszy obraz sytuacji, chce by wszystko było zrobione dobrze i moralnie.

* Kapitanowi nie podoba się opcja wykorzystywania biednych ludzi, powinni przynajmniej dostać wybór czy wolą spocząć w pokoju, czy pracować dla nich. Nalega, by im powiedzieć przynajmniej, że są martwi.
* Kapitan wierzy w kompetencje swoich oficerów, jednak zaczyna tracić zaufanie do ich decyzji.

Dyplomata zmienia front. Proponuje, by powiedzieć członkom stacji prawdę, wcielić ich jeśli że chcą do organizacji i prowadzić badania, by się przekonać, czy nie da się ich przywrócić do życia.

* Kapitan jest nastawiony sceptycznie po wcześniejszych propozycjach, jednak argumenty dyplomaty ostatecznie go przekonują. Prosi dyplomatę, by porozmawiał z zarządcą stacji i miał oko na pozostałych oficerów.

Inżynier i oficer ochrony chcą się upewnić, że powiedzenie, co się dzieje nie zabije załogantów - więc mówią Annie i udowadniają przy śluzie. Anna jest załamana, chciała wrócić do rodziny po tym kontrakcie, miała się żenić. Anna potrzebuje czasu, by dojść do siebie.

Dyplomata rozmawia z zarządcą stacji. Elżbieta jest chętna do współpracy, ale nie chce by wiedział ktokolwiek poza nią. Ponadto zachowuje funkcję głównego zarządcy stacji i autonomię w decyzjach w jej obrębie. Chce też dostawać dodatkowe dobra luksusowe według swoich zachcianek. Dowiedziawszy się, że Anna wie, sugeruje też, że się jej pozbędzie. Dyplomata zgadza się na jej warunki, ale nie chce, by Anna zginęła, proponuje, że ją przekona, by zachowała sekret. Ostatecznie Anna jest witalnym członkiem stacji, jedynym inżynierem ze znajomością tych konkretnych technologii. Elżbieta zgadza się.

Gracze rozmawiają z Anną. Anna jest załamana, jej życie się skończyło. Dyplomata chce, by zachowała tajemnicę i przesyłała im dodatkowo raporty z poczynań szefowej, przekonuje, że dostanie nowe technologie do zabawy, że przyślą tu nowych ludzi i może kogoś sobie znajdzie, że będą badać ich “schorzenie”, by mogli powrócić na powierzchnię. Anna chce zobaczyć przed śmiercią słońce, więc ostatecznie zgadza się na słowa dyplomaty.

## Streszczenie

Eksperymentalna łódź podwodna Kelpie działająca w okolicach Trójzębu odkrywa podwodną stację badawczą sprzed pęknięcia, na której znajduje 'żywych' ludzi. Badając sprawę odkrywa anomalię, która ożywia zmarłych w obrębie bazy oraz utrzymuje urządzenia techniczne w działaniu, nawet jeśli nie powinny. Ostatecznie Kelpie przez oficerów nawiązuje współpracę ze stacją.

## Progresja

* .

## Zasługi

* Klasa Inżynier: Ruszył na pomoc oficerowi ochrony, gdy ten miał problem z wydostaniem się z alterisowej rafy Prisma, naprawił sondę i wydobył z niej dane. Zbadał dronami stację badawczą Medimmortal, naprawił przekaźniki do komunikacji, ostatecznie traci zaufanie kapitana Barowieckiego próbując go przekonać do oszukania załogi stacji, potwierdza, że powiedzenie prawdy o anomalii nie zabije załogantów informując Annę, wpędzając ją tym w depresję.
* Klasa Dyplomata: przekonuje Karolinę, że na Kelpie znajduje się laboratorium, którego mogłaby użyć, przekonuje kapitana Barowieckiego do swojego pomysłu na pomoc stacji i wcielenie jej do organizacji, przekonuje Elżbietę do współpracy oraz Annę, by pracowała dla nich, dając jej nowy sens życia.
* Klasa Oficer Ochrony: Odzyskał sondę morską sprzed pęknięcia z dna alterisowej rafy Prisma, ostatecznie traci zaufanie kapitana Barowieckiego próbując go przekonać do oszukania załogi stacji, potwierdza, że powiedzenie prawdy o anomalii nie zabije załogantów informując Annę, wpędzając ją tym w depresję.
* Gerald Barowiecki: identyfikuje niedomówienia oraz nie do końca czyste intencje w słowach oficerów i wybiera najbardziej moralną opcję z proponowanych, traci zaufanie do inżyniera i oficera ochrony, przez co nie zauważa, że jego dyplomata może zadziałać wbrew niemu. Nie wiem o szczegółach układu z bazą i uważa, że postąpił słusznie.
* Elżbieta Sanchez: ostatecznie zrealizowała swoje pragnienie nieśmiertelności, zapewnia sobie niemal wieczne stanowisko zarządcy stacji w luksusie i wygodzie oraz swobodę działania w obrębie stacji.
* Jędrzej Sanchez: interweniuje, gdy nieznane siły włamują się na stację oraz gdy Karolina jest potencjalnie zagrożona.
* Anna Ivanova: pomaga w naprawie przekaźników komunikacji, po etapie załamania z powodu swojej śmierci oraz śmierci bliskich decyduje się współpracować z Kelpie i poczekać na “lekarstwo” na jej stan; chce przed śmiercią ujrzeć słońce.
* Robert Tisso: znaleziony przez Kelpie martwy, pokrojony i złożony do kupy, ożywiony przez bazę. Na jego ciele widać ślady po składaniu.

## Frakcje

* Korporacja Medimmortal: założona koło 10 lat przed pęknięciem rzeczywistości; badała flory, fauny i minerałów w rowie Penelope, na którego skraju się znajduje. Cel: przedłużenie życia ludzkiego. Na Astorii wykryto obiecujące stworzenie, Virenis (Viverejuvenis) - "Żywa Młodość" - homaropodobne stworzenie o wysokiej inteligencji, które podczas wylinki jest w stanie regenerować swoje ciało, utrzymując młodość.
* Korporacja Medimmortal: Ma podwodną stację badawczą w okolicach Rafy Prisma, która działa dzięki nieumarłym agentom, nie wiedzącym o świecie po Pęknięciu Rzeczywistości; współpracują z załogą Kelpie.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Wielkie Jezioro Indygo
                    1. Obszar Sojuszu Letejskiego
                        1. Rafa Prisma
                        1. Podwodna Stacja Badawcza Medimmortal
                        1. Rów Penelope

## Czas

* Opóźnienie: 133
* Dni: 1

