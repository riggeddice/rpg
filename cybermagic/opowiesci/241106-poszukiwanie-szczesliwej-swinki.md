## Metadane

* title: "Poszukiwanie Szczęśliwej Świnki"
* threads: serce-planetoidy-kabanek
* motives: the-collector, infiltracja-dyskretna, izolacja-zespolu, krzywdze-by-sie-chronic
* gm: żółw
* players: kić, fox, til, vivian

## Kontynuacja
### Kampanijna

* [241016 - Nie dotarła do celu](241016-nie-dotarla-do-celu)

### Chronologiczna

* [241016 - Nie dotarła do celu](241016-nie-dotarla-do-celu)

## Plan sesji
### Projekt Wizji Sesji

**WAŻNE**: Kontekst sesji i okolicy oraz Agendy itp DOKŁADNIE opisane w [Thread 'serce-planetoidy-kabanek'](/threads/serce-planetoidy-kabanek). Tam też znajduje się rozkład populacji itp.

* INSPIRACJE
    * PIOSENKA WIODĄCA
        * [Funker Vogt - What If I'm Wrong?](https://www.youtube.com/watch?v=4su5Nus3byM)
            * "If life is a task | But there is too much to ask | If I've been mistaken - all along"
            * Gabriel Lodowiec próbuje naprawić sytuację, ale jego działania prowadzą do katastrof politycznych.
            * Zespół widzi, że Lodowiec chce dobrze, robi dobrze, ale antagonizuje przy tym lokalsów.
        * [Powerwolf - Sacramental Sister](https://www.youtube.com/watch?v=tw3Mf9m3Jj0)
            * "Sacramental sister in heaven or hell | When the night is falling, no moral can tell | Sacramental sister, to God you are sworn | When desire calling, the Bible is torn"
            * Sprzeczności i kolizje. Nie ma jednej ścieżki. Złapać winnych czy uratować przewożonych? Zniszczyć niewolniczy statek i "Orbiter niszczy cywili" czy ich puścić?
    * Inspiracje inne
        * Deal with the Devil
* Opowieść o (Theme and Vision):
    * "_Niebezpieczeństwo jest wszędzie a nawet Orbiter nie może zrobić wszystkiego tam gdzie chce tak jak chce_"
        * Na Planetoidzie Oratel postaciom grozi wiele różnych rzeczy. Orbiter nie powinien się tam pojawiać...
        * Oratel jest tak ciężko uzbrojona, że Tezremont niewiele może zrobić by ją uszkodzić. Plus, PR...
    * "_Zrobić, co należy czy co wizerunkowo poprawne?_"
        * SC "Szczęśliwa Świnka" jest pozornie jednostką cywilną, "niegroźną". Orbiter niszczący Świnkę to cios wizerunkowy.
        * Ale uratowanie tych dziewczyn czy doprowadzenie kapitana do stanu bezpieczeństwa ma znaczenie. Ale jak to zrobić?
        * NAPRAWDĘ Świnka jest zaminowana? A póki nic się nie dzieje, może przyjść wsparcie z Anomalii Kolapsu...
    * "_Tezremont podupadł. Nie jest tym, czym powinien być. Lepsza jednostka Orbitera by to zrobiła._"
        * GDYBY Tezremont leciał szybciej, GDYBY miał broń w lepszym stanie, GDYBY miał bardziej zmotywowaną załogę...
        * Lodowiec przegrywa przez to, że Tezremont jest jednostką w tak złym stanie.
    * Lokalizacja: Libracja Lirańska. Najpewniej planetoida Oratel a potem okolice Anomalii Kolapsu.
* Motive-split
    * the-collector: ktoś próbuje pozyskać Annabelle do kolekcji ładnych dziewczyn; chce ją porwać i wykorzystuje do tego celu Roberta Ptaszka
    * infiltracja-dyskretna: próba zinfiltrowania Oratel by pozyskać informacje o Szczęśliwej Śwince, jej trasie i jej agendzie
    * izolacja-zespolu: Zespół jest na wrogim terenie i to w przebraniu; nie ma nic co Orbiter może zrobić by im pomóc
    * krzywdze-by-sie-chronic: Ptaszek, który wystawia Annabelle Fabryce. Neporyk, który robi Świnką straszne rzeczy, by nic nie stało się jemu.
* O co grają Gracze?
    * Sukces:
        * Uda się coś osiągnąć - albo zatrzymać Świnkę, albo złapać kapitana, albo whatever
        * Uda się nie zniszczyć reputacji Orbitera na tym terenie
        * Uda się nikogo nie stracić
    * Porażka: 
        * .
* O co gra MG?
    * Highlevel
        * Pokazać, że Orbiter jest najsilniejszy na tym terenie, ale nie jest wszechmocny
        * Pokazać, że na Oratel NAPRAWDĘ nie lubi się Orbitera i niewiele można na to poradzić
        * Pokazać jak niesamowicie niebezpieczne jest to miejsce i że ta Grupa Wydzielona Orbitera nie jest dość kompetentna
            * Lodowiec zawodzi jako polityk; Orbiter pomaga ludziom, ale nadal stanowi "demoniczne zło" w okolicy
            * Pawilończyk zawodzi jako kapitan; Świnka stanowi dlań za duży problem
    * Co uzyskać fabularnie
        * Porwać Annabelle XD
        * Zniszczyć relacje Orbiter - Oratel
* Agendy
    * The Collector: chce porwać Annabelle w dyskretny sposób

### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

KONTEKST:

* Zgodny z THREAD

CHRONOLOGIA:

* Znalezieni przekształceni górnicy przez anomalię; Orbiter to przechwycił (Lodowiec)
* Lucjusz odkrył, że za tym stoi statek "Szczęśliwa Świnka"

PRZECIWNIK:

* brak, anomalia?

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

FAZA 1: Oratel

* Noktianka odmawia zejścia ze statku
* Polujemy na Idolkę; flashbang + gaz
    * w tle jest Markus, odmawia pomocy; Topokuj też padnie
    * savaranie (przekształceni)
* Mateusz Knebor jest akceptowany na Oratel; pomógł im wiele razy i potrafi wyprowadzić problemy. "coś za coś"

## Sesja - analiza
### Fiszki
#### Orbiter, OO Geisterjager, fregata dowódcza

* Gabriel Lodowiec: komodor Grupy Wydzielonej Mrówkojad Lirański oraz kapitan OO Geisterjager
    * Commander: komodor Orbitera; dowodzi 2 statkami. Dobrze przydziela ludzi do zadań. Dobry taktyk. KIEPSKI w tematy HR.
    * Ekspresyjna Monomania (Light Yagami): bardzo wybuchowy, ale przywiązany do zasad i procedur. Zrobi to, co trzeba, by wygrać i wygra WŁAŚCIWIE. (O- C+ E0 A0 N+)
    * Próżniowiec, Uprzedzenie Do Grupy (noktianie), Weteran: bardzo doświadczony, nie lubi noktian. Ale jeszcze bardziej nie lubi przegrywać misji.
    * Echo Porażki: za Lodowcem do dzisiaj idzie fama "tego, który zrobił przerażającą mowę". Jest osobą, którą się straszy noktian.
* Dariusz Mordkot: pierwszy oficer, marine, dowodzi grupą uderzeniową.
    * Solutor: marine, uzbrojony i dobrze wyposażony
    * Niezależny Wizjoner (Miorine): bierze zadanie i je wykonuje. Pomysłowy, gotowy do nietypowych sytuacji. Lubi się chwalić i pokazywać klasę swych ludzi. (O+ C+ E+ A- N+)
    * Wytworna Elegancja: zawsze doskonale ubrany, w nieskazitelnym mundurze. Śliczne zęby i szeroki uśmiech. No z plakatu rekrutacyjnego.
* Renata Kaiser: oficer komunikacyjny
    * Jest Druga Szansa: wybacza, wierzy, że każdy - nawet noktianin - ma prawo do drugiej szansy. Sympatycznie neutralizuje Lodowca. (O+ C0 E+ A+ N0)
    * Lekko paranoiczna i oddana Lodowcowi
* Ofelia Miris: marine, druga dowodząca grupą szturmową
* OO Geisterjager: fregata dowódcza, 80 osób

#### Orbiter, OO Tezremont, szybka korweta adaptacyjna

* Arnold Pawilończyk: kapitan OO Tezremont pod Gabrielem Lodowcem; około 23 lata
    * Optymistyczny Entuzjazm (Bastian): Arnold jest optymistą i wierzy, że jak zrobi się wszystko dobrze to się jakoś ułoży. Odważny i skłonny do ryzyka. (O+ C0 E+ A+ N-)
    * Trochę za Młody: Arnold jest niestety bardzo wrażliwy na ładne dziewczyny i nieco za bardzo zależy mu na tym co pomyślą inni. Chce wszystkich zadowolić.
* Mateusz Knebor: pierwszy oficer OO Tezremont; 
    * Bezwzględny Mastermind (Shockwave): jego celem jest udowodnienie, że Pawilończyk nie jest dobrym kapitanem i spełnienie celu Orbitera. (O+ C+ E0 A- N0)
    * Kontakty z Syndykatem: nie wie o tym, ale ma informacje oraz dodatkowy sprzęt itp. od ludzi Syndykatu. To oni pokazali mu bezużyteczność Arnolda.
* Natalia Luxentis: cicha noktiańska advancerka, która dołączyła do Orbitera nie widząc lepszej opcji; około 33.
    * Advancer, Inżynier: lepiej czuje się w kosmosie niż w statku czy na planecie, nie jest może dobra w walce, ale świetnie radzi sobie z naprawami w kosmosie.
    * Marzyciel (Luna): ślicznie rysuje swoje światy i swoje historie, rzadko rozmawia z innymi. Ma bardzo neutralną minę, pragnie zadowalać innych. Lubi sprzątać. (O+ C- E- A+ N-)
    * Próżniowiec, Savaranka, Ten Obcy: noktiański nabytek Orbitera; Natalia próbuje być maksymalnie przydatna na pokładzie, ale unika kontaktu z innymi jak może. Overloaded.
* Sebastian Warząkiewicz: ogólnie 'dobry koleś' grupy, inżynier, kompetentny i lubiany, acz bardzo intensywny.
    * Inżynier, kowboj: dobrze strzela, dobrze walczy i spełnia się jako inżynier załogi. Zawsze pierwszy rusza w bój.
    * Ognista Pasja, Uprzedzenie do grupy: chce się bawić, tańczyć i śpiewać. A przy okazji wkopać noktianom, bo zniszczyli majątek jego rodziny i utknął tutaj. "Główny bohater". (O+ C- E+ A- N+)
    * Bodybuilder, Nieformalny Przywódca: Sebastian UWIELBIA ćwiczenia fizyczne i praktycznie ćwiczy cały czas. Do tego zna mnóstwo opowieści o Mroku Noctis i świetnie opowiada.
* Czesław Truśnicki: marine Orbitera, przypisany do OO Tezremont.
    * Solutor: świetny marine i żołnierz. 
    * Opiekuńczy Mentor: z natury dba o harmonię i rozwój innych, dba o każdego członka swojej załogi. Nie uważa, że cierpienie jednostki jest warte sukcesu grupy.
* Grzegorz Dawierzyc: drugi oficer i inżynier
    * Kompetentny oficer, choć niszczy morale podwładnych. Na pewno dowodzi inżynierią i silnikami na OO Tezremont.
    * Wieczny Maruda: narzeka na wszystko, ALE jest lojalny kapitanowi i Orbiterowi. Po prostu trudno z nim wytrzymać. (O- C+ E? A- N+)
    * Istnieje powód, czemu aż tak bardzo narzeka. Nie wiemy jednak jaki. PLACEHOLDER.
* Fred Topokuj: marine Orbitera, przypisany do OO Tezremont.
    * Klanowiec: tylko Orbiter; nikt inny nie ma znaczenia
* Ewan Pilczerek: pilot myśliwca przechwytującego Tezremont
    * Rozdzieracz Iluzji (Dolan): widzi jak jest i mówi co jest. Raczej z boku, mało rozmawia z załogą. Gra sobie w gry w myśliwcu. WIE, że się zapuścił.
    * Zdemotywowany: uważa, że wielkie sukcesy i działania się skończyły i to już nie jest czas dla niego czy Orbitera tutaj
* Ilona Smarznik: artylerzystka i oficer defensyw Tezremont
    * Strażnik Harmonii: nie odzywa się niepytana, dba, by nikt nie miał problemów i się na nikogo nie złościł.
* OO Tezremont: szybka korweta adaptacyjna, 22 osoby
    * zdolna do poważnej zmiany wyglądu i posiada więcej skrytek niż powinna; działa jak QShip.

#### Planetoida Oratel

* Markus Serret: doloryta, anty-Orbiterowy szef ochrony na Oratel
    * Płomień Rebelii: staje naprzeciw wszystkim wielkim grupom i organizacjom. Nienawidzi ich i nimi gardzi. Tylko samodzielność Oratel. "Nie dać ani paluszka". Zero subtelności.
    * Weteran: dużo walczył, dużo przeżył i jest wyjątkowo niebezpieczny.
    * Neurosprzężenie: wzmocnione mięśnie, przekształcony w maszynę do zabijania. To nadało mu jeszcze większy płomień i nienawiść do Orbitera, Aureliona itp.
* Ramon Ajrecz: poluje na wartościowych ludzi na Oratel i nie tylko

### Scena Zero - impl

.

### Sesja Właściwa - impl

Intelligence Orbitera -> Statek "Szczęśliwa Świnka" jest powiązany z planetoidą Oratel. Czyli tam powinny być jakieś wiadomości.

* Pawilończyk: Lecimy na Oratel, wypytamy wszystkich, dowiemy się gdzie jest Świnka i ich przechwycimy
* Annabelle: Ucieknie nam
* Knebor: Wyślemy noktiankę, ona się dowie, przechwycimy i damy radę
* Sylwia: Nie wyślemy jej
* Annabelle: Noktianka może mieć wrogów na planetoidach i nie chce zejść z okrętu.
* Lucjusz: Możemy zdjąć mundur by się dowiedzieć rzeczy. Po prostu.

Annabelle zgarnie dane Orbitera, przejrzeć, jakie są tu jednostki raz na jakiś czas - z jakiej jednostki przylecieć. Zwłaszcza z przemiałem ludzi.

Tr +3:

* Vr: Annabelle zmajstrowała, przygotowała dokumenty które pozycjonują Was za jednostkę Telimar.
* X: Telimar ma w niektórych miejscach wrogów, o czym Orbiter nie wie. Lubiana przez Oratel, ale podpadła niektórym. I nielubiana przez Orbitera.
* V: Macie dobre aspekty do dodania. Ta jednostka jest "taka jak chcemy".

Przebranie gotowe, nie wchodzą tam jako Orbiter.

Tezremont ląduje oficjalnie na Oratel jako dywersja, po czym leci prom z Zespołem (4 + Topokuj + Truśnicki). Zasób (chaos).

Po drodze - degeneracja wizualna pancerza Isy. Isa jest salvagerem; z odrobiną power tools jest w stanie poradzić sobie z popsuciem wizualnym pancerza.

Tp +2:

* X: Ten pancerz tak będzie wyglądał aż dojdziemy do cywilizacji Orbiterowej
* X: Pancerz wygląda na groźny ale uszkodzony; zwraca uwagę w formie "jestem edgy dark lordem".
    * "Isa Mrocznym Lordem Orbitera"
* (poddaję) Nikt nie uzna Isy za agentkę Orbitera. Po prostu nie.

Annabelle - raczej negliż, Isa - czaszki szczurów, Sylwia - staje się niewidoczna.

W końcu - dotarli na Planetoidę Oratel.

Annabelle ciągnie do kupowanie fajnych rzeczy, Lucjusz "daje się oszukać" i kupuje, Sylwia jest niewidoczna, Isa jest groźna, działa jako ochroniarka.

Trzech facetów, solidnych, podchodzi i proponuje usługi. Lucjusz udaje że jest grubą rybą (jest za nim gruba ryba, ktoś solidny za nimi siedzi i mają kasę). Lepiej być pośrednikiem niż bogatym człowiekiem; nie chcą dać się porwać. A Isa chce pokazać groźność, by nikt nie próbował nic głupiego robić.

A Sylwia ocenia ludzi, którzy gadają. Kim oni są i jak działają.

Tr Z+3:

* XX: Oni ocenili Zespół. Uznali Was za osoby podatne na negocjacje. Uznali że bardzo Wam zależy i że nie znacie reguł. I POTRZEBUJECIE towaru.
    * (Przekazali informacje Agentom Fabryki; tu jest coś potencjalnie wartościowego do pobrania)
* V: Oni są drobnymi scammerami i chcieli "stówkę", ale COŚ zobaczyli, i chcą spełnić te parametry. Uznali, że KTOŚ będzie z nimi rozmawiał. Nie w tym miejscu toczą się poważne interesy, widzicie że TAM - w ciemniejszych uliczkach - toczą się interesy.
* V: Isa wywiera wrażenie bardzo groźnej. Jest kompetentna, ale jest też "dark and edgy". I tak, zakładają wszyscy, że jest za Wami ktoś inny.

Bar "Ćwierkający Ptaszek", gdzie Robert Ptaszek zaprosił Zespół by się dostać bliżej do Annabelle. To dobrej klasy wieczorowa restauracja. Ludzie są lepiej ubrani. Isa odstaje. Sylwia jest niewidoczna. Neony, muzyka, ładnie pachnące jedzenie. Ptaszek próbuje podnieść jakość swoich zysków korzystając z "nowej Egzotycznej Piękności - Annabelle".

Annabelle chce pociągnąć Ptaszka za język, by wiedzieć co podają jako jedzenie. Lucjusz chce ocenić mięso; spróbować go, ale też dobrze ocenić jego jakość; bierze na wspomaganie środek działający jak wzmacniacz smaku.

Tp+3:

* X: Annabelle się musi zgodzić na cholerne karaoke by odwrócić uwagę skuteczniej
* V: Bardzo smaczne. To 'long pork'. Maksymalnie usunięte ze Skażenia Skażone ludzkie mięso. Kilka dni czasu.

Gdy Annabelle robi karaoke i efektowny wywiad, Lucjusz 'spikuje' koktajl / poncz chemikaliami. Niech wszyscy są na lekkim haju, beztroscy, emocjonalni. A Annabelle zaczepia panów i eskaluje ten efekt, prowokuje innych facetów.

Analiza konfliktu; co się tu stanie:

* Isa Coś Robi. Jako nietrzeźwa. Robi bydło, udając nietrzeźwą; niech na niej jest uwaga wszystkich.
* Lucjusz zatruwa poncz zanim to ruszy; niech więcej ludzi ma problemy z dezinhibicją.
* WPIERW Annabelle robi karaoke, wdzięczy, wywiad, taniec, chaos itp.
* Lucjusz kłóci się o udział w zyskach
* To wszystko zaczyna działać, ludzie robią się rozochoceni, ochrona ma za dużo do monitorowania
* Sylwia korzystając z okazji wkrada się na zaplecze, neutralizuje obserwujących i pozyska informacje o Szczęśliwej Śwince i jej trasie

Matematycznie:

* Konflikt zaczyna od Heroicznego. Ale: +dywersja +trucizna +Annabelle oraz chaos
* Konflikt staje się Trudny z Przewagą

Isa robi bydło: "Panie więcej tego rumu bo idę do dumu!", zatacza się itp.

Tr Z +4 +3Og:

* V: Isa robi bydło. Zwraca uwagę.
* V: Lucjusz zatruł poncz. Best impreza w dekadzie. Dodał LSD - chce by ludzie byli podatni na sugestie. 
* Vz: Ludzie są podatni na sugestię. Są jak glina w okrutnych łapkach Annabelle.

Annabelle - wszystkie oczy na nią, niech się żrą między sobą. Wzbudza chaos i zazdrość. "Oni się pocałują dla mnie", opowiadając o brzydkiej koleżance która odbiła chłopaka i namawiając facetów do całowania się ze sobą.

* X: Annabelle prima cel numer jeden. "CHODŹ KOCHANA!"
* Og: Wpada ekipa porządkowa. Tu się robi bydło.
* Vr: Sylwia niezauważenie prześlizgnęła się na zaplecze. Jest tam i jej nie widać.
* Vr: Sylwia pozyskała informacje o dostawach, kto z kim itp.
    * TAK, tu była "Świnia". Tak, mięso pochodzi ze Świni. Więcej - lokalsi NIE WIEDZĄ, że jedzą ludzi.

Annabelle przekonuje Ptaszka - chodźmy na stronę. "Trochę się robi bydło, może pójdziemy na stronę". Z Ptaszkiem udajecie się przez zaplecze przez tunele do takich lepszych kwater.

* V: Sylwia zachowuje swobodę ruchu. Jest niewykryta i może robić Rzeczy.

Annabelle, prowadzi Cię Ptaszek, mijacie dwóch silnych uzbrojonych facetów i do luksusowych kwater. W łóżku gadamy o składzie wieprzowinki.

* Annabelle: Nie chcemy sprawiać problemów.
* Ptaszek: Moja droga, jest taki statek, przewożą wieprzowinę. Czasem dostajemy... coś innego. Ale jest zdrowe. Sam jadłem, wiem.
* Annabelle: Bliższe układy?
* Ptaszek: Kapitan jest dobrym człowiekiem. Miał problemy biznesowe, ale to dobra rodzina. Próbowali przeżyć. Ogólnie go lubimy. Nie zrobi krzywdy naszym i pomogliśmy. 
    * (on wierzy w to co wierzy)

Tr Z +3:

* X: Trochę więcej czasu niż chcecie
* Xz: Annabelle bardzo mu się spodobała, będzie mu na niej zależeć
* V: Kapitan Klaudiusz Neporyk.
    * Ptaszek WIE, że Neporyk nie jest osobą, która CHCE robić takie rzeczy.
    * Ptaszek WIE, że Neporyk musi robić niektóre rzeczy; jest bardzo zestresowany.
    * Ludzie ludzie Neporyka - jest to patriota, oddaje planetoidzie, nigdy nie robi powiązanemu
    * Neporyk to "dupa". Nie umie strzelić w twarz.
    * Neporyk "nie ma innego wyjścia", musi to zrobić.
* V: Ptaszek wie o NieGórnikach
    * "Fabryka" robi to skutecznie. On robi to taniej. I mniej skutecznie. Ale podobno dobrze na tym zarabia.
    * Ptaszek nie wie więcej o "Fabryce". Ale Ptaszek się boi "Fabryki"
        * "Będziecie służyć żywi lub umarli brzmi jak coś co zrobiłaby Fabryka".
        * Ptaszek wie od Neporyka. Ptaszek niewiele wie. Ma nadzieję, że Orbiter coś zrobi.

Jeśli Świnka tu dokowała, musi korzystać z usług portu. Zaopatrzenie, zapasy, paliwo. Więc - rejestry portowe - nie tylko dokładnie kiedy była ale też kiedy będzie. Zwłaszcza, że Ptaszek od dłuższego czasu robi te rzeczy.

Lucjusz chce złożyć zamówienie na tonę miejsca. Dostanie wiadomość od kucharza: "dwa miesiące".

Lucjusz przepytuje kucharza. Kucharz podał trasę, następne punkty itp. To oznacza, że mają dość dużo informacji jaką wybrać trasę by przechwycić Świnkę przy Anomalii Kolapsu. Więc powoli czas przejść na prom, wrócić na Orbiter i przejść na Świnkę. Ale - trzeba wrócić, i to w miarę bezpiecznie. On oraz Isa są niestety w głównej sali, z ochroną, monitorowani gdzie wszystko jest kontrolowane.

Sylwia tymczasem zostaje na _stealth_. Zapoznaje się z trasami jakby trzeba było się wycofać.

15 minut później - sygnał Annabelle przeszedł w tryb "uśpiona". Annabelle jest nieaktywna. Jest nieprzytomna. Ale jak, kiedy i co się stało - ostatnio była w łóżku z Robertem Ptaszkiem.

Sylwia - jedyna zdolna do dyskretnego działania - przechodzi do infiltracji. Dociera do "lepszych kwater" - i tam jest dwóch mięśniaków. Pilnują. Sylwia nie do końca ma się jak przekraść koło kamer i koło tych mięśniaków; zwyczajnie tu nie ma do tego warunków.

Dobra - Lucjusz i Isa robią burdę. Plotki o pochodzeniu mięsa (korzystając z podatności innych na sugestię). PTASZKA OSZUKALI NIECH PRZYJDZIE SIĘ WYJAŚNIĆ!

Tp+3:

* V: Ptaszek musi przyjść. I przyjdzie.
* V: Weźmie tą dwójkę mięśniaków.

Ptaszek i mięśniaki minęli Sylwię. Oni zeszli z pola widzenia; Ptaszek wyraźnie wygląda na smutnego (stracił Annabelle). Sylwia rzuca dymem by ukryć się przed kamerami i wbija.

TrZ+3+3Og (strzela w drzwi):

* V: Sylwia przebiła się i jesteś w pomieszczeniu gdzie nikogo nie ma; 
* Vz: Sylwia poczuła się troszkę słabo (residual gas), ale przeszła dalej. Znalazła przejście - przyłapała tunel. 4 savaran odwraca się w stronę Sylwii.
* VrV: Udało jej się unieszkodliwić savaran z zaskoczenia - kilka strzałów z pistoletu. Annabelle klapnęła na ziemię grzecznie.
* X: Niestety, mocodawcy savaran zorientowali się, co tu się dzieje.

Sylwia bierze Annabelle na ramię i zwiewa; wolniej, bo jednak to nie jest dobra sytuacja a Sylwia nie jest tak silna. Sylwia NAJKRÓTSZĄ DROGĄ leci na publiczny teren. A w tym tunelu - zostawia caltropsy by jej nie ścigano.

* X: Sylwia wycofała się, ale z drugi strony widzi że biegną ludzie. Uzbrojeni. Sylwia zatrzasnęła drzwi by je zabarykadować i ją fortyfikuje.

Lucjusz i Isa próbują się oderwać od sali głównej, bo Sylwia wysłała sygnał, że potrzebuje pomocy.

Tr Z+2:

* X: "Pan Ptaszek tu nie decyduje; sprawa idzie pod szefostwo ochrony, macie tu siedzieć".
* X: WOLNOŚĆ OBYWATELA! regularne zamieszki. Zamieszki się zaczęły. Zamieszki są krwawo pacyfikowane przez ochronę. Lucjusz i Isa są 'grzeczni', więc im nikt nic nie robi.
* Vr: Isa zostaje jako dywersja i dewastator, zrobiła lukę i Lucjusz się wymknął. I Lucjusz biegnie w stronę Sylwii; ona nie ma jak się wydostać.
* V: Isa przeprosiła, zapłaciła karę (niemałą, ale ją stać) i na prom grzecznie będzie musiała iść.

Sylwia rzuca granat dymny, strzela kilka razy w stronę zbliżających się uzbrojonych ludzi i się barykaduje w pokojach Ptaszka.

Tp +3:

* V: Zabarykadowała, zadziałało.

Lucjusz dociera blisko dymu. Są tam ci ludzie; Lucjusz przekształca magią dym - niech dym zatrzyma absorbcję tlenu. Niech się zaczną dusić. Do utraty przytomności. Lucjusz jest poza zasięgiem kamer, ma swobodę czarowania.

TrZM+3Ob:

* Vr: Udało się ich unieszkodliwić. 
* Xm: SKUPIŁEŚ PEŁNIĘ ENERGII. Ten dym... zaczął się rozprzestrzeniać i rozrastać. Ta trójka została pożarta przez dym. Dym zaczął się rozprzestrzeniać - jest jeszcze "u góry" a nie na dole. Kamery zaczęły się topić. Dym zaczął nabierać... "cech". Ten gaz "przesuwa się" w stronę Ptaszka. Chce go zabić.

Isa się ujawnia, że jest magiem, wiedząc o sytuacji - jest w stanie im pomóc, zna się na walce z potworami. Drze papier "nie będę biła ludzi" i zarządza akcją kryzysową. Przejmuje dowodzenie. Ludzie się mają ewakuować, Ptaszek ma z nią zostać.

Tr +3 +3Vg:

* X: Jakiś kretyn nie posłuchał, wbiegł w gaz i zginął w agonii.
* Vr: Ptaszek próbował uciec. Ale Isa go trzyma.
* V: Isa żąda od Ptaszka informacji czemu zostawił Annabelle na stracenie. Ptaszek się przyznał - nie chciał. Ale to Fabryka. Nie odmawia się Fabryce! (on wierzy, że to Fabryka)
* V: Ptaszek pomoże, zrobi wszystko BO SIĘ BOI ISY I CHCE ZNOWU ODZYSKAĆ KONTAKT Z ANNABELLE

Potworny mgłodym się zbliża. To jest problem i to duży. Isa wali krzesłem w twarz Ptaszka, rozmarowała krew na krześle, po czym rzuca w mgłodym krzesłem i używa mocy że TO JEST PTASZEK. Chce, by mgłodym wykonał swoją funkcję i sam się rozproszył.

TrZM+3+3Or+3Ob:

* V: Mgłodym się zatrzymał na krześle. Teraz krąży dookoła. 2 tygodnie ten klub jest zablokowany, bo mgło-dym jest tu uwięziony.
    * Isa zostanie bohaterką; nie chce ryzykować nic więcej.

Sytuacja jest pod kontrolą. Na planetoidzie już nie mają niskiego profilu; będą rozpoznawani. Ale mają Ptaszka i Isę jako kontakty.

Co się szczególnie rzuciło w oczy Zespołowi jak chodzi o Oratel:

* Nikt **nie szukał maga** który stworzył mgłodym który zniszczyła Isa. Nikogo to nie interesowało. Tak jakby nie spodziewali się, że "ktoś nieznany" za tym mógł stać.
    * czyżby tego było więcej? czy to normalne?
* Nikt nie był rozbrojony. Nie zabrali Zespołowi broni, a Isa miała nawet serwopancerz.

To ciekawe obserwacje odnośnie Oratel...

Powrót na OO Tezremont. 

I Tezremont leci w kierunku na Anomalię Kolapsu, po odpowiedniej trasie. Zespół jest lekko zmartwiony tym, że kapitan chce 2G przyspieszenia a pierwszy oficer zaznacza, że to niebezpieczne; nie robili takich stress testów Tezremont od dawna. Więc lecą z 1G przyspieszenia, 4h do 150 kps. Nadal zdążą przed Świnką.

Minie jakiś dzień, zanim zbliżą się do Świnki. Lucjusz decyduje się sprawdzić co jest nie tak z ich savaranką.

Natalia (Savaranka) się chowa przed innymi załogantami, ale Sylwia PRZYŁAPUJE Natalię.

TrZ+2:

* X: Załoga widzi, że się interesują savaranką, więc coś może być nie tak.
* Vz: Sylwia przyłapała ją, jak coś ukrywała. Sylwia wie, gdzie jest skrytka. To jakieś zapiski. Odprowadza ją do Lucjusza. Potem wraca i patrzy. To są rysunki. Rysunki ludzi. Niewprawne.

Lucjusz robi testy medyczne savarance. Dokładne - implanty, Skażenie, pełne obrazowanie medyczne itp.

Tr Z+3:

* X: Savaranka się zorientowała co się dzieje, jest przerażona, ale jeszcze idzie za instrukcjami
    * dla granicznej sytuacji "musisz się zdecydować: jest częścią załogi lub nie. Słuchasz itp."
* Vz: Lucjusz jest dokładne testy
    * stan żywienia - jest savaranką
    * z perspektywy biologicznej, młodość, pierwsze 10 lat - nie była savaranką
    * ona miała ciężkie życie. Są stare rany, są blizny, są ślady rzeczy, uszkodzeń, chipów
    * teraz jest czysta
    * są ślady Skażenia, ale stare
* V: 
    * Natalia była Skażona mniej więcej w tym czasie gdy dołączyła do savaran. To było ciężkie Skażenie. 
    * Za czasów savarańskich miała modyfikacje, potem je usunięto, potem nie miała już żadnych.

Co prowadzi do wywiadu medycznego:

* Lucjusz: Jak masz na imię i ile masz lat?
* Natalia Luxentis. 31 lat.
* Lucjusz: Wyniki które mam pokazują mi coś nietypowego. Czy w jakiś sposób ingerowano w ciało przy magii?
* Natalia: Chyba nie. Nie chciałabym.
* Lucjusz: Czy zostałaś savaranką z własnej woli?
* Natalia: JESTEM savaranką. Ze statku Luxentis.
* Lucjusz: Nie pamiętasz niczego innego? Więc... nie wiem co się wydarzyło, ale ktoś dokonał ingerencji. Był tam chirurg?
* Natalia: Wypadek. Katastrofa. Śmierć wszystkich. Luxentis mnie uratował. Natalia Luxentis.
* Lucjusz: Czemu nie jesteś na Luxentis?
* Natalia: Nie pytaj.
* Lucjusz: Czy robili coś w Twoim ciele bez Twojej woli? Wbrew Tobie?
* Natalia: Nie. Nie wiem.
* Lucjusz: Czy teraz uważasz Tezremont za swój statek?
* Natalia: Teraz jestem Natalia Tezremont. Ale nie chcę zapomnieć Luxentis. Luxentis to dom.
* Lucjusz: Czy Luxentis został zniszczony?
* Natalia: Nie pytaj.
* Lucjusz: Jak to się stało że Tobie nic się nie stało?
* Natalia: Byłam ambasadorką. Łatwiej rozmawiać ze mną niż z savaranami. Byłam poza Luxentis. Teraz - sama.

Zdaniem Lucjusza, Luxentis jest Fabryką. Uważa, że Fabryka jest powiązana z savaranami. Bo "naprawa do optymalizacji" brzmi jak coś bardzo savarańskiego... przynajmniej "ich" Natalia jest czysta.

## Streszczenie

Zespół infiltruje planetoidę Oratel, z którą powiązana jest "Szczęśliwa Świnka". Zamaskowani przylatują promem z Tezremont, niepowiązani. Barwna czwórka szybko łapie zainteresowanie lokalsów, w czym Roberta Ptaszka (właściciela "Ćwierkającego Ptaszka"), który korzysta z okazji by poszerzyć biznes. Annabelle wykorzystuje wdzięk i chaos, aby przyciągnąć uwagę, Lucjusz zatruwa poncz, rozpraszając ludzi, a Sylwia, niewidoczna, wkrada się na zaplecze. Gdy Annabelle idzie z Ptaszkiem do łóżka, zostaje unieszkodliwiona gazem; Sylwia ją odbija ale nie ma dość siły ognia. Isa kupuje czas Lucjuszowi, by ten pomógł Sylwii ale Paradoks stworzył mglistego potwora zabijającego ludzi. Gdy Lucjusz i Sylwia pełzając ewakuują siebie i Annabelle, Isa staje naprzeciw mglistemu potworowi i go unieszkodliwia; na 2 tygodnie więzi go w "Ćwierkającym Ptaszku".

Robert Ptaszek, wdzięczny zespołowi, zostaje ich kontaktem i sojusznikiem (boi się Isy i lubi Annabelle). Przyznał się - podstawił Annabelle, bo kazali mu ludzie których uważa za Fabrykę. A nie odmawia się Fabryce. Już na Tezremoncie Lucjusz przesłuchuje "ich savarankę" - okazuje się, ze Natalia może być powiązana z Fabryką czy coś o niej wiedzieć, ale nie chce nic mówić...

## Progresja

* Natalia Luxentis: załoga orientuje się, że Zespół ma na nią szczególne baczenie; jeszcze bardziej oddalona od reszty załogi niż zwykle. Sytuacja idzie ku wrzeniu.
* Annabelle Magnolia: Robert Ptaszek na Oratel jest nią żywotnie zainteresowany i jej pomoże.
* Isa Całujek: Robert Ptaszek na Oratel się jej boi i jej pomoże.
* Isa Całujek: uznana za _edgy (czaszki szczurów na pancerzu XD)_ i ma tego typu pancerz, ale szanowana na Oratel za to że pomogła pokonać anomalię
* Robert Ptaszek: wspiera Isę (bo się boi) i Annabelle (bo ją lubi).

## Zasługi

* Sylwia Mazur: infiltratorka; wkradła się na zaplecze Ćwierkającego Ptaszka, zdobywając informacje o trasie "Szczęśliwej Świnki" i uratowała nieprzytomną Annabelle wbijając z pistoletem i znajdując tajne przejście
* Annabelle Magnolia: manipulatorka; urokiem, wdziękiem i aktorstwem odwracała uwagę, pozyskała Ptaszka i zdobyła informacje o Fabryce i Szczęśliwej Śwince. Unieszkodliwiona gazem, prawie porwana przez... agentów Fabryki?
* Lucjusz Jastrzębiec: dyskretnie zatruł poncz w barze, wzmagając podatność na sugestię u gości, co zwiększyło chaos. Ratując Sylwię, Paradoksem stworzył magiczny dym zabijający przeciwników i pomógł jej zwiać z Annabelle.
* Isa Całujek: dywersantka i ochroniarka; odegrała rolę groźnego MROCZNEGO ochroniarza, sprowokowała chaos "Ćwierkającym Ptaszku" i umożliwiła Sylwii i Lucjuszowi działanie. Gdy pojawiła się anomalia, stanęła jako mag naprzeciw niej i wyciągnęła od Ptaszka wszystkie informacje o Fabryce jakie ów miał.
* Robert Ptaszek: niezwykle przystojny właściciel Ćwierkającego Ptaszka na Oratel; wpierw chciał z Annabelle zrobić biznes, ale potem mu się spodobała. Oddał ją jednak agentom Fabryki(?) gdy zażądali. Wspiera Szczęśliwą Świnkę; wie, że jedzą ludzkie mięso i są tam jakieś problemy. Został informatorem i sojusznikiem Isy i Annabelle, acz nie wie że są z Orbitera. Śmiertelnie boi się Fabryki.
* Klaudiusz Neporyk: z opowieści Ptaszka ma dobre intencje, lecz jest w trudnej sytuacji; czasem dostarcza ludzi na mięso, czasem gorsze rzeczy. Jest zestresowany, nie jest osobą agresywną ani bojową. Nie zrobi krzywdy Oratelczykom.
* Natalia Luxentis: okazuje się, że urodziła się jako nie-savaranka, lecz był wypadek. Adoptował ją savarański statek Luxentis, z którym coś się stało. Była kiedyś ciężko Skażona. Coś wie o Fabryce i tym co się dzieje, ale nie mówi.
* Arnold Pawilończyk: ostrożny, nie chce antagonizować Oratel. Słucha rad Zespołu i reszty załogi. W końcu decyduje się na dyskretną infiltrację a potem łagodniejsze przyspieszenie Tezremont.
* Mateusz Knebor: pragmatycznie chce wysłać noktiankę na zwiady na Oratel; nie dba o jej uczucia. Potem ostrzega przeciw pełnemu 2G przyspieszenia; Tezremont nie był testowany pod kątem tych przyspieszeń od dawna.
* OO Tezremont: okazuje się, że nie był testowany na pełnej mocy od dawna. Jest ryzyko, że może nie być w stanie osiągnąć swojej pełnej mocy. Zmierza do Anomalii Kolapsu, gonić Świnkę.
* SC Szczęśliwa Świnka: statek handlowy, transportujący też ludzkie mięso do jedzenia; znamy jego trasę i wiemy, że leci gdzieś w stronę Anomalii Kolapsu.
* SC Luxentis: tylko z opowieści Natalii; savarański statek, który uratował małą Natalię jako dziewczynkę i ją przygarnął. Natalia nie chce powiedzieć, co się stało z Luxentis.

## Frakcje

* Lirańska Fabryka: zgodnie z analizami Zespołu: tajemnicza frakcja przekształcająca ludzi, "naprawiająca ich"; chyba powiązana z savaranami lub filozofią savarańską.

## Fakty Lokalizacji

* Planetoida Oratel: nikt nie szukał maga, który stworzył potworny mgłodym. Nikogo to nie interesowało. Tak jakby "to się zdarzało".
* Planetoida Oratel: nikt nie był rozbrojony. Nie zabrali Zespołowi broni, a Isa miała nawet serwopancerz. Ludzie są uzbrojeni, jakby spodziewali się wewnętrznego wroga.
* Planetoida Oratel: obszary publiczne są bezpieczne, pod kamerami itp. Ale mniej zamieszkałe uliczki są niebezpieczne i ochrona tam nie ingeruje. Jeśli nie robisz nic głupiego, nic Ci nie będzie.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Libracja Lirańska
                1. Planetoida Oratel

## Czas

* Opóźnienie: 1
* Dni: 1

## Inne
