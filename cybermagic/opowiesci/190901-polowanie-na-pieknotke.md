## Metadane

* title: "Polowanie na Pięknotkę"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [190906 - Wypadek w Kramamczu](190906-wypadek-w-kramamczu)

### Chronologiczna

* [190906 - Wypadek w Kramamczu](190906-wypadek-w-kramamczu)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

brak

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Dzień 1:

Niedawno, Alan i Pięknotka mieli krótkie starcie. Nic poważnego - po prostu Pięknotka wybrała mało optymalną decyzję i przez to, zdaniem Alana, spieprzyła. Mogło rozwiązać problem szybciej i mniejszym kosztem dla Barbakanu. Zdaniem Pięknotki - so what. Alan i Pięknotka się lekko poprztykali o to - nic poważnego.

Pięknotka dostała zadanie. Podobno w Zaczęstwie, w jednym z bloków na Osiedlu Ptasim doszło do rozrostu z Trzęsawiska. Podejrzewają Wiktora - dlatego idzie Pięknotka z Alanem jako wsparciem. Podzielili się robotą - Pięknotka sprawdzi teren, Alan sprawdzi okolicę.

Pięknotka, ostrożna jak zawsze, poszła do budynku. Faktycznie, w tym mieszkaniu jest _growth_. Jest nietypowy, nie pasuje do Trzęsawiska. Pięknotka pobrała próbki - _growth_ sam odpadł. Za nim znajduje się runa mentalna oszałamiająca, atakująca Pięknotkę. (TrZ=P). Pięknotka straciła częściowo przytomność, Cień się uruchomił sam w reakcji. Pięknotka po chwili się ocknęła - poczuła głęboki smak nienawiści i krwi. Jest w środku walki z kimś - zabójcą. Rany Cienia się nie leczą. Zabójca tauntuje Pięknotkę. Chce by ona oddała mu Cienia.

Pięknotka z trudem przechwyciła kontrolę nad Cieniem; czas na wprowadzenie mózgu do tej walki. Nie może połączyć się z Alanem; runa sprawiła, że zapomniała jak.

Pięknotka delikatnie przekierowała Cienia - niech przeciwnik myśli, że Cień nadal jest tępy. Ale niech zbliży Pięknotkę do broni. Pięknotka się tam znalazła - ale przeciwnik jest naprawdę dobry. Ciął Pięknotkę nożem w tors; Cień to zablokował ale to go spenetrowało. Nóż penetruje Cień. Czyli jest jakaś antybroń na Cienia - a Pięknotka nie do końca wie jaka.

Pięknotka przetoczyła się i strzeliła karabinem laserowym, po czym znowu się przetoczyła. (Tp1:P). Niestety, przeciwnik rzucił nożem - uszkodził jej broń. Cień włączył komunikator do Alana. Pięknotka spytała przeciwnika co wie o Cieniu.

* ixion i esuriit. Jedyny w swoim rodzaju.
* pochodzi z Orbitera - nie pasuje do astoriańskiej terminuski
* "zmasakrowałaś grupę moich ludzi, psychopatko, w Enklawach"

Przeciwnik powiedział Pięknotce, że zabierze jej Cienia bo ona zabija jego magów. Wbił w nią sztylet; ten próbuje przekonstruować jej myśli. Ona i Cień walczą. Wtedy - błysk światła. Alan wystrzelił Fulmenem, działo Koenig, tryb artyleryjski. Wiązka przeszła przez budynek. Kilka osób lekko rannych, w budynku dziura na wylot (wiązka skupiona). Przeciwnik... poleciał z odrzutem; z pewnością jest ciężko ranny. Pięknotka była w stanie się otrząsnąć, po chwili. Alan podbiegł do niej, uszkadzając pomieszczenia cywilne.

Alan natychmiast wezwał wsparcie. Ewakuował Pięknotkę. Podczas tego Pięknotka próbuje odpytać Cienia co się stało. Integruje się z nim. Udało jej się - zobaczyła wizję. Gdy Pięknotka straciła przytomność, Cień się uruchomił i dostał granatem bardzo złej energii dla siebie. To go osłabiło dramatycznie. Przeciwnik i tak był zdziwiony że Cień dalej walczy.

Dzień 2:

Pięknotka obudziła się w szpitalu w Barbakanie. Jest przykuta do łóżka. Lucjusz ją uwolnił; powiedział, że już jest w lepszym stanie. Wyjaśnił, że to pobieżna trauma. Dobrze to wszystko przetrwała - Pięknotka ma wrażenie, że i tak dostała wsparcie neuronauty. Cień dużo ognia przejął na siebie.

Pięknotka dowiedziała się też, że terminusi chcą ją pomścić. Nie jest może ich maskotką czy najbardziej lubiana na świecie, ale jest jedną z nich i ogólnie jest lubiana. Czekają na nią wiadomości do odebrania z wyrazami współczucia itp. Jest też wiadomość od Karli: "zamknij tą sprawę". Piękotka zauważyła, że Karla nie sprecyzowała jak.

Jest też wiadomość od Mariusza Trzewnia. Wiadomość jest typu "pomścimy Cię. Enklawy będą płonąć". Pięknotka natychmiast się z nim połączyła i nie pozwoliła mu niczego robić w tamtym kierunku - nie wytłucze się wszystkich psów bo jeden ugryzł. Pięknotka dała radę go przekonać i zatrzymać też innych krewkich terminusów, m.in. z pomocą Karli.

Pięknotka poprosiła Minerwę o pomoc.

Minerwa przyszła do szpitala odwiedzić Pięknotkę. Jeśli chodzi o Cienia - stwierdziła - nie skończy zdrowa. Wyjaśniła Pięknotce, że Cień ma słabości:

* Cień jest połączeniem energii ixiońskiej oraz energii esuriit
* Przeciwnik użył granatu łączącego esuriit i puryfikację; to zdestabilizowało Cienia. Tymczasowo musiał odbudować balans.
* Ta puryfikacja pomogła trochę Pięknotce

Chwilę porozmawiały też o tym, czy Cień może uzyskiwać świadomość. Minerwa absolutnie odrzuciła tą możliwość - jej zdaniem (w odróżnieniu od Talii) subturing nigdy nie zostanie świadomą istotą. Nawet superturing nie jest w stanie. Talia uważa inaczej, ale Talia jest fanatyczką.

Minerwa chce zbadać Cienia. Pięknotka poprosiła Blakenbauera o kajdanki dla siebie...

Minerwie się udało. Podleczyła Cienia; pomogła mu w stabilizacji. Dodatkowo wydobyła informacje o napastniku; Cień w końcu go przecież skosztował. Ale w wyniku Skaziła cały pokój w szpitalu. Lucjusz Blakenbauer po prostu wszedł i patrzył...

Dzięki danym od Cienia udało się zidentyfikować napastnika. To jest Józef Małmałaz. Zabójca. Noktianin. Nie powiązany ze Szczelińcem, kolekcjoner niebezpiecznych artefaktów. Nie dba tak naprawdę o "swoich" - zależy mu tylko na artefaktach. Modus operandi? Okrutny. Będzie ranił i zabijał, acz jak nie musi - nie zrobi. Nie odpuści póki ma szanse - ale jeśli nie ma, zemści się i wróci np. za pół roku. Zgodnie z modus operandi Józefa, będzie polował albo na Pięknotkę albo na Alana (z zemsty).

Pięknotka ostrzegła Alana. Terminus podziękował i się ucieszył. Z przyjemnością się zmierzy i zniszczy takiego przeciwnika.

Dzień 3:

Pięknotka wyszła ze szpitala. Wszystko wskazuje na to, że to jest dobry, udany dzień. Musiała się wytłumaczyć kwatermistrzowi za zniszczenia w szpitalu (dowiedziała się, że Alan był na dywaniku za dewastację działem Koenig). I około 14:00 dostała dziwny sygnał na hipernecie. Dziwny, osłabiony i w bardzo złej formie. Sygnał przychodzi od Chevaleresse.

Pięknotka bierze wsparcie i szuka źródła sygnału. Źródło sygnału jest w formie beacona - stały sygnał. Wskazuje na Nieużytki Staszka. Wzięła ciężkie wsparcie kilku terminusów.

Chevaleresse związana, na krześle, zakneblowana. Wyraźnie co najmniej raz została uderzona mocno w twarz. Nie jest przytomna - połączyła się z virtem swoimi unikalnymi mocami; dzięki temu Pięknotka dostała od niej słabą wiadomość. Za drzwiami - Alan, z dwoma nożami wbitymi. Jeden to nóż zadający maksymalne cierpienie i przebudowujący umysł, drugi to leczniczy i zadający ból. Dzięki szybkiej reakcji Chevaleresse, Alanowi nie stanie się stała krzywda - ale miesiąc w szpitalu poleży jak nie dłużej.

Co się stało - Alan nie przewidział, że Józef zapoluje na Chevaleresse. I już nie miał wyboru. Kiedyś by poświęcił "cywila", ale Chevaleresse jest naprawdę jego słabym punktem.

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Noktiański zabójca chciał pozyskać Cienia; pokonał Pięknotkę, ale Alan go zestrzelił. Potem zabójca złapał Chevaleresse i korzystając z niej jako zakładniczki wysłał Alana do szpitala. Pięknotka doszła do tego, jak się nazywa z pomocą Minerwy i Cienia.

## Progresja

* Alan Bartozol: na miesiąc trafił do szpitala przez dwa sztylety Józefa Małmałaza
* Józef Małmałaz: na parę dni trafił do szpitala przez Alana Bartozola i działo Koenig (jak zeszły stymulanty)

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: przegrała walkę z Józefem; uratował ją Alan. Ale - udało jej się dowiedzieć kim był napastnik, zatrzymać głupi atak na enklawy a potem uratowała Alana i Chevaleresse.
* Alan Bartozol: uratował Pięknotkę przed Józefem strzałem z Fulmena (700 metrów przez kilka ścian); potem by chronić Chevaleresse dał się Alanowi ciężko poranić.
* Józef Małmałaz: noktiański łowca ciekawych przedmiotów, bardzo groźny zabójca. Wpierw zranił Pięknotkę (został ciężko ranny przez Alana), potem na stymulantach porwał Chevaleresse i ciężko zranił Alana.
* Mariusz Trzewń: organizował kampanię do tępienia noktian w enklawach; przekonany przez Pięknotkę że to zły pomysł
* Lucjusz Blakenbauer: doprowadził Pięknotkę do porządku i pozwolił Minerwie czarować w szpitalu. Bardzo potem tego żałował (odkażanie sali).
* Minerwa Metalia: przyszła pomóc Pięknotce zregenerować Cienia i dowiedzieć się kim jest napastnik. Udało się, acz rozwaliła pokój w szpitalu.
* Diana Tevalier: porwana przez Józefa, wykorzystana by Alan się poddał. Nie straciła przytomności umysłu - weszła w virt i zaalarmowała Pięknotkę o problemie.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Osiedle Ptasie: miejsce zastawienia pułapki na Pięknotkę przez Józefa, gdzie w końcu Alan ją obronił i wyciągnął.
                                1. Nieużytki Staszka: miejsce, dokąd porwano Chevaleresse oraz gdzie Józef skroił Alana.
                            1. Pustogor
                                1. Rdzeń
                                    1. Szpital Terminuski: miejsce regeneracji Pięknotki oraz ixiońskiego czaru Minerwy.

## Czas

* Opóźnienie: 2
* Dni: 3
