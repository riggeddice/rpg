## Metadane

* title: "Weterynarz i Verlen w Przelotyku"
* threads: brak
* motives: arogancja-mlodosci, aurum-wasn-miedzyrodowa, bardzo-grozne-potwory, dobry-uczynek-sie-msci, magowie-pomagaja-ludziom, vicinius-pozyteczny, wziecie-winy-na-siebie
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [240808 - Nemesis i Muza](240808-nemesis-i-muza)

### Chronologiczna

* [240808 - Nemesis i Muza](240808-nemesis-i-muza)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * Kate Ryan "Free Your Mind":
        * Hydra jest jednostką pozytywną, też jest Blakenbauer i Verlen
    * Inspiracje
        * Classic sci-fi o dr Evelyn Mercer
     * Uczucie dominujące
        * "SERIO?!"
* Opowieść o (Theme and vision):
    * "Blakenbauer ratuje potwora w miasteczku Serpiniek"
    * "Verlen próbował rozwalić potwora który 'enslavował' miasteczko, ale nie wyszło"
        * Gadobij Verlen: młody (19), który próbował ratować Serpiniek
        * Czesław Blakenbauer: młody (22), który próbował ratować Hydrę Serpińską
        * Wielobij Verlen: starszy (26), który próbuje pomścić brata
    * Lokalizacja: Serpiniek
* Po co ta sesja?
    * Jak wzbogaca świat
        * Blakenbauer ratujący ludzi ORAZ potwora
        * Nowi Verlenowie i Blakenbauerowie
    * Czemu jest ciekawa?
        * ?
    * Jakie emocje?
        * "SERIO?!"
* Motive-split
    * arogancja-mlodosci: zarówno Blakenbauerzy jak i Verlenowie wychodzą od ambitnej mowy, zakładając, że dadzą radę i są nieśmiertelni.
    * aurum-wasn-miedzyrodowa: Blakenbauerzy kontra Verlenowie; Verlen wpierw wszedł ostro przeciw Blakenbauerowi, potem Blakenbauer uciemiężył Verlena. Z Arianną i Vioriką łamiącymi wzór.
    * bardzo-grozne-potwory: zarówno Hydra żyjąca w Serpiniek jak i Toperzowiec na łapkach atakujący Serpiniek
    * dobry-uczynek-sie-msci: Gadobij próbował pomóc Serpiniek i napotkał na serię problemów. To samo spotkałoby Czesława gdyby nie Arianna i Viorika.
    * magowie-pomagaja-ludziom: Gadobij Verlen oraz Czesław Blakenbauer ruszyli ratować miasteczko Serpiniek, mimo, że było zupełnie nie po drodze i poza ich zakresem.
    * vicinius-pozyteczny: Hydra, chroniąca Serpiniek
    * wziecie-winy-na-siebie: Tyraniusz Blakenbauer, który jest całkowicie niewinny ale jest radosnym kozłem ofiarnym wszystkiego złego co zrobili inni Blakenbauerowie
* O co grają Gracze?
    * Sukces:
        * Uratować Miasteczko Serpiniek
        * Wielobij jest zadowolony, Czesław Blakenbauer też
    * Porażka: 
        * ?
* O co gra MG?
    * Highlevel
        * ? 
    * Co chcę uzyskać fabularnie
        * Zniszczenie Hydry przez Wielobija
        * Ciężko ucierpi Czesław Blakenbauer
* Default Future
    * Wielobij wpadnie i zniszczy Hydrę
    * Czesław Blakenbauer ma problem
* Dilemma: Mściząb CZY Miasteczko? Wezwać pomoc CZY samemu?
* Crisis source: 
    * HEART: "Hydra stała się słaba a Gadobij poszedł ostro"
    * VISIBLE: "Gadobija nam zbili"
        
### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

* Niedaleko pojawił się Shrieker Nest (Toperze), niebezpieczne dla miasteczka; Czesław próbuje zrobić to 'ekologicznie' po tym, jak Hydra zaczęła chorować po jedzeniu potworów
* Serpiniek ma smaczne potrawy, dość zdrowe i pomniejsze zdrowotne wartości (bo Hydra)
* Serpiniek jest wskalnym miasteczkiem, w stronę na Cieniaszczyt
* Gadobij Verlen zaatakował Serpiniek chcąc zniszczyć Hydrę; nie udało mu się, Hydra + Blakenbauer > Gadobij.

Q: 

* Gdzie był Gadobij? + zainteresowanie Wielobija Verlena (starszego brata)
* Co tam się dzieje?
* Jak pokonać potwora?

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

* Faza 1:
    * ?
    * len: 6
* Faza 2:
    * ?
    * len: 6
* Overall
    * chains:
        * Wielobij na terenie: 3/3
        * Wielobij niszczy Hydrę i Blakenbauera: 8/8
    * stakes:
        * śmierć cywili
        * szacun Gadobija i Wielobija
    * opponent
        * Wielobij, Czesiek, Hydra
    * problem
        * Wielobij i jego honor
        * Czesiek i jego miłość
        * Serpiniek i ich niechęć do współpracy


## Sesja - analiza
### Mapa Frakcji

.

### Potwory i blokady

* Hydra
* Toperze

### Fiszki

.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Miasteczko w Verlenlandzie - Krwawiróg. Arianna i Viorika tam trafiła to _distress call_. Arianna i Viorika po wydarzeniach z Mścizębem są wyczulone na "młody Verlen coś jest nie tak". Sentisieć Odpowiedziała. Jest tam ciężko ranny młody Verlen, niezbyt z wysokich lotów, ale nie jest to ostateczne dno. 

Gadobij Verlen, 19 lat. Zabił jaszczurkę jak był mały.

Gadobij leży w szpitalu i jest półprzytomny. To są dane jakie są z sentisieci. Jest w szpitalu pod dobrą opieką lekarzy Verlenlandu. Jego życiu nie zagraża nic, ale sentisieć nie wie co mu się stało i gdzie ucierpiał - to jest dziwne.

Gadobij jest w złym stanie, ale nic zagrażającego życiu i zdrowiu. Ot, 2 tygodnie (przy opiece medycznej) ciężkiego 'regenerowania', problemy z jedzeniem i ogólnie jest zatruty. Aż dziwne patrząc na sentisieć że dotarł do szpitala. Zmierza w jego stronę jego brat. Wielobij Verlen.

Gadobij jest osobą która raczej dobrze wybiera bitwy i jakkolwiek pragnie chwały, ale bardziej pragnie kohezji rodu. Być jak brat. To jest taki "pozytywny Verlen", chce pomagać innym, odważny, spoko Verlen. Nie ma odchyłów.

Lekarka nazywa się Ewelina Mistowir. Jest 51-letnią kobietą, bardzo sympatyczną, która patrzy na Ariannę jak na objawienie. 100% rojalistka. Okazuje się, że część danych jest utajniona przez brata Gadobija, Wielobija. Arianna przybywa tu bo Wielobij może sobie nie poradzić - wszystko wskazuje na to, że może być źle.

Ewelina się zastanawia chwilę, po czym powiedziała:

* Gadobij Verlen jest zatruty i to jest magiczna trucizna. To jest środek... spreparowany. To nie był potwór. Dużo wskazuje na Blakenbauera.
* Gadobij nie ucierpiał na terenie Verlenlandu. Został przyniesiony przez swoich ludzi, którzy też mieli objawy trucizny. Ale mniejsze.
* Przybył z kierunku przelotyckiego; jego ludzie też są w szpitalu. W lepszym stanie, acz są pokąsani. On też. Wszystko wskazuje na Blakenbauera   
    * potwór, pokąsanie, trucizna
* Pokąsanie wskazuje na coś toperzopodobnego. Ale też na coś ogromnego. Tam jest więcej niż jedna istota.
* Gadobij powiedział "Blakenbauer pomógł potworowi"; tyle był w stanie powiedzieć.
* Efekt jadu: ciężka biegunka, problemy z pamięcią, ogólne osłabienie organizmu... UCIEMIĘŻ JAK CHOLERA.

Sentisieć potwierdza - Przelotyk. 2 dni temu opuścił Verlenland, parę godzin temu wrócił.

Viorika idzie rozmawiać z ludźmi Gadobija. Jako bojowa Verlenka, szorstka. Żąda AAR.

Tp +2:

* V:
    * nie pamiętają wszystkiego, pamiętają urywki. Różni - różne urywki. Ale chcą pomóc.
    * na pewno szli walczyć z potworem. Gadobij chciał zniszczyć groźnego potwora, by pomóc ludziom.
    * na pewno pamiętają tam, że potwór był "za duży".
    * potwierdzają obecność Blakenbauera. W odpowiednim momencie stanął przeciwko Verlenowi.
    * pamiętają takie latające skrzydlaki.
    * tyle wiedzą: to nie jest teren Aurum
    * Gadobij był źródłem informacji o Blakenbauerze i potworze. Skąd wiedzą? Gadobij im powiedział, że to był Blakenbauer.
        * Verlen: "Blakenbauerze, to jest miejsce w którym Twoje mroczne machinacje zostaną zniszczone!"
        * Blakenbauer: "Verlenie, Twoja TYRANIA się skończy"

Gadobij chce być bohaterem pierwszego planu, ale bardziej chce pomóc innym. Zaakceptuje być tienem bohaterem swoich ludzi i by nikt o nim nie wiedział. To sprawia, że Viorika wie, że mają za szerokie query - Mściząb this is not.

Czekamy na Wielobija.

Wielobij Verlen przybył. Pancerz, żołnierze, oddział uderzeniowy - 20 osób, dobra formacja, wyszkoleni i lojalni. Mają prowiant, 2 APCty i szeroki sprzęt. Wyraźnie widać, że Wielobij nie wie z czym idzie walczyć po samym sprzęcie jego ludzi.

Wielobij jest złośliwy i pełen pasji, frontalny atak z charakteru. Wbija do szpitala zamaszyście, widzi dwie Verlenki i tak trochę sklęsł. I patrzy na nie z lekką irytacją i rezygnacją.

* Wielobij: "Viorika Verlen? Co Ty tu robisz..?"
* Viorika: "Jestem. Interesuję się dobrem kuzynów."
* Wielobij: "No super. To możesz wrócić tam, skąd przyszłaś. Poradzimy sobie."
* Viorika: "Widzę jak sobie radzicie"
* Wielobij: "Uważasz, że Gadobij jest niezdolny do poradzenia sobie? (ostrzeżenie)"
* Viorika: "Sensowna osoba która ma pecha, ale tam może być coś więcej"
* Wielobij: (złagodniał) "Jest coś więcej. Jest pieprzony Blakenbauer. Na pewno śpi ze zwierzętami. Zoofil."
* Wielobij: "Czyli - sytuacja pod kontrolą, rozwalę to. Możecie iść... zająć się swoimi rzeczami."
* Viorika: "Kiepskie wykorzystanie dostępnych zasobów. Taktycznie - kiepska decyzja"
* Wielobij: "Chcecie się oddać pod moje dowodzenie? (z niedowierzaniem). Oczywiście, przyjmę, rozwalimy go razem, na chwałę Verlenlandu"
* Viorika: "Raczej myślałam o współpracy."
* Wielobij: "A :-("
* Wielobij: "Wolę pomścić honor brata samemu. Wiesz, wszystko zostaje w rodzinie."
* Wielobij: "Nie chcę z Wami współpracować; gdzie dowódców trzech tam wszystko się pieprzy. Nie macie nawet oddziału."
* Viorika: "My JESTEŚMY oddziałem"
* Arianna: "Spójrz na siebie, jesteście gotowi na wszystko, nie macie nawet wywiadu"
* Wielobij: (irytacja) "Mamy wywiad, po prostu tam jest Blakenbauer, to może być coś innego niż było."
* Arianna: "Jaki Blakenbauer?"
* Wielobij: "Zoofil, jak każdy"
* Arianna: "Oni się różnią - jaka jest jego specjalizacja?"
* Wielobij: "No tego nie wiem. Ale nie wyślę ludzi by się dowiedzieli."
* Arianna: "Masz nas"
* Wielobij: "Mam Was wysłać na zwiad?"
* Arianna: "Nie chcemy Ci ukraść chwały i pomszczenia brata"

Arianna i Viorika przekonują Wielobija że chcą "iść na zwiad" i ma poczekać. On ma chwałę, a one rozeznają teren i pomogą by więcej ludzi nie zostanie straconych.

Tp +3:

* V: Wielobij poczeka. To jest sensowny plan taktyczny. On się zajmie bratem i wierzy, że A+V dowiedzą się i on może zadać Cios Ostateczny.
    * nadal jest założenie że on wejdzie i dojdzie do konfrontacji

Wielobij dzieli się swoim intelem z Verlenkami:

* Nie wiem, jak Gadobij do tego doszedł, ale szedł do miasteczka o nazwie Serpiniek. Jest to jedno z miasteczek w skale.
* Mieli tam jakiś poważny problem z potworem, Gadobij o tym usłyszał i uznał, że Verlen powinien pomóc, nieważne że poza naszymi terenami. Kilka godzin w głąb. Na zewnątrz.
* Dostarczę Wam ścigacz zwiadowczy. Dość pancerny. Ale niech wróci!
* Wielobij nie wie, skąd Gadobij wiedział. 

Ścigacz zwiadowczy. A+V wiedzą GDZIE lecą, wiedzą mniej więcej na czym polega problem i mają pewność, że Wielobij nie wbije im noża w plecy. Więc - wyjątkowo komfortowa sytuacja. Przed wyjazdem Arianna i Viorika jeszcze rozpytują wśród kontaktów i żołnierzy Gadobija - co on wiedział i na co szedł.

Tr Z (Wielobij oficjalnie endorsuje A+V) +3:

* V: Sporo osób ma różne informacje, z czego się dało zebrać różne wartościowe rzeczy
    * Gadobij słyszał, że tam są problemy z rolnictwem, co wynika z toperzy. Toperze robiły krzywdę ludziom.
    * Ogólnie ludzie na terenie mieli problemy z potworami; tzn, zgryzoty związane z potworami, ale nie ma detali. Nie chwalili się - głównie o toperzach.
    * Nic nie wiedział o żadnym Blakenbauerze
    * Miejscowi nie mówili NIC o tym "czymś dużym". Wcale to się nigdy nie pojawiło   
* Vz: Są też inne plotki o okolicy, które są ciekawe
    * Są plotki, że tam są groźne potwory. I faktycznie, to się potwierdza. Ale te potwory omijają Serpiniek.
    * Jest coś co "odpycha", "odstrasza" potwory - ale przestaje być skuteczne.
    * Mało osób i grup chce atakować Serpiniek.
    * Nie jest wykluczone, że tam JEST coś dużego lokalnego od dawna. Ale nie ma potwierdzenia czy dowodu a to nie jest ważne miejsce więc magowie rzadko tam chodzą.

Następnego ranka, ze świtem, A+V wystartowały w kierunku na Serpiniek. Trasa na Serpiniek nie jest "fajna". Trzeba poruszać po dość groźnym terenie, i to teren jest dość z uskokami itp. Ścigacz daje radę, ale to + potencjalne zagrożenia spowalnia. Wielobij nie jest osobą, która będzie się bardzo spieszyć jak podejmie decyzję; więcej sensu ma podejście do tematu nie na szybko i nie na luzie, ale bez szczególnego dzikiego galopu.

Tp +2:

* X: Po drodze jest więcej potworów i trudnego terenu, więc A+V nie mogą iść całkowicie dyskretnie. Tracimy sporo czasu, bo musimy jechać naokoło, bo są stwory, trzeba poczekać itp.
* X: Z uwagi na profil przeciwników i chęć zachowania dyskrecji ostatnie kilka kilometrów musi być na piechotę.
* V: Dyskrecja zachowana; udało się wejść pod miasteczko niezauważenie dla wszystkich.

Wszystko wskazuje na to, że ilość toperzy się zmniejszyła, nie są tak chętne do zbliżania się do miasteczka jak były i większych potworów tu nie ma. Miasteczko jest 'on alert', ale nie 'on high alert' i ludzie pracują normalnie.

Viorika obserwuje teren. Chce poznać sytuację - co jak się rusza, anomalii, zrozumienia sytuacji.

Tp+3:

* X: Blakenbauer zobaczył ukryte Verlenki, ale nie wie kto to. Wie tylko że są tu zwiadowcy.
* Vr: Ruchy przeciwników
    * Toperze są perfekcyjnie skoordynowane. Bardziej jak jeden organizm niż grupa toperzy.
    * Toperze BARDZO nie chcą zbliżać się do miasteczka. Chciałyby. Ale się stresują bo...
    * Na dachach są dwie zakamuflowane żaby. One są maskowane, ale to wyraźnie żabska.
    * Kilka dzieciaków podlewało żaby wodą.
    * Gdy żaby zobaczyły Verlenki, skupiły się na byciu bardzo niezauważalnymi żabami. Próbują być niewidoczne.

Lancery A+V mają wspomaganie zwiadowcze, silniki skokowe itp. Szybko skalują teren by dostać się do miasteczka zanim ktokolwiek zdąży zareagować.

Tr Z+3:

* X: A+V są szybkie, ale oni też. Mają procedury. Widząc Lancery, ktoś pokazał palcem i krzyknął. Ktoś pobiegł do środka. Ludzie zaczynają się przygotowywać (procedura). Żaby udają że ich nie ma.
* V: Mimo, że są ostrzeżeni i ktoś jest w środku, A+V dotarły do miasteczka zanim cokolwiek się stało. Ktoś obsadzał działko, ale za wolno.

Arianna podbija do dzieciaka, zdejmuje hełm i "hej, gdzie jest wujek co dał wam żaby".

Tp+3:

* X: Dziecko sparaliżowane strachem. Arianna puszcza, by nie robić traumy i ma minę w stylu SRS.
    * "Dzieci zwykle tak nie reagują"

Viorika ściąga hełm i do najbliższego silnego faceta i wyglądającego na to że wie co robi, jakiś z ochrony. 

* Viorika: "Dzień dobry, możesz nas zaprowadzić do... jakkolwiek Wam się przedstawił człowiek od żab?"
* Ozmir: "Dzień dobry... wysyłają dzieci na zwiad?"
* Viorika: "Chcę porozmawiać."

Tr Z (młodziutka zwiadowczyni) +3:

* X: Dobrze, ale zostawiacie broń. Nie możecie mieć broni jeśli wchodzicie do bezpiecznego miejsca.
* V: Będziecie mogły spotkać się z "dorosłymi". Zaprowadzi Was do bezpiecznego miejsca gdzie spotkacie się ze wszystkimi. (nie przeszkadzają mu lancery ale przeszkadza mu broń)

Loadout na potwory zostaje w bezpiecznym miejscu. Verlenki "nie dotykajcie, serio". I idą w głąb jaskiń. Do sali niebieskiej. Arianna się zorientowała, że zrobili kółko 2 razy by kupić czas. Ale - sala niebieska.

Podczas drogi Arianna zagaja jak nastolatka, smalltalk. "Co u was, jak wykulicie, jak myślicie ktoś nas wysłał, jesteśmy BARDZO NIEBEZPIECZNE wiecie...". Co oni widzą:

* Uważają, że A+V to zwiadowcy z Verlenlandu i niedaleko jest ich większy oddział. Uspokajają je, że mogą odejść w spokoju, nic złego się nie stanie itp.
* Są dość zrelaksowani, uważają, że kontrolują sytuację - choć martwi ich obecność A+V.
* Nie mają pojęcia, że Verlenki są tak agresywne; myślą, że to normalny plan taktyczny i mają wizję Verlenów wg Blakenbauerów.
* Patrzą na A+V jak na _brainwashed kids_ - nastolatki które nie znają innego życia i są SUPER POWAŻNE. I MAJĄ WAŻNĄ MISJĘ.

To dość spora pieczara naturalna, gdzie są krzesła. Co ciekawe, ma dziury w ścianie o średnicy pół metra. Jest tam kilka osób, poza strażnikami uzbrojonymi, też w ppanc.

Jest tu STARSZYZNA. 3 starych ludzi. Nie ma Blakenbauera.

* Viorika ocenia ich: "ok, żaden z nich nie jest nikim z kim chcemy rozmawiać, nie ma go tutaj"; zatrzymuje się, odwraca i w kierunku wyjścia. "Nie chcemy rozmawiać z NIMI."
* Członek starszyzny prychnął. Inny: "Poczekaj, młoda damo.". Viorika prychnęła.
* Grażyn: "Nazywam się Grażyn Ozdamrin."
* Viorika: "Nie tym, z kim chciałam porozmawiać..."
* Arianna: "Daj mu się wypowiedzieć do końca, jesteśmy gośćmi, nie możemy się tak zachowywać za każdym razem"
* Viorika: "(z niecierpliwością) No dobra..."
* Grażyn: "Jestem jednym z triumwiratu rządzącym tym terenu. Jestem... najbardziej ważną osobą. Nie znajdziesz nikogo wyżej."
* Viorika: "Ale nie stworzyłeś żab. Z nim rozmawiam."
* Arianna: "Co tu się dzieje?"
* Viorika: "Tak tak, oni nam będą truć dupę a on zniknie i nic się nie dowiemy."
* Grażyn: "To bardzo proste, tien Verlen. On tu był i Wasz współtien go wygnał." (lekki uśmiech)
* Viorika: "Nie wierzę, że się wycofał. To bardzo nie w stylu Blakenbauerów. I dla odmiany nie chcę mu wklupać a z nim pogadać. Bo przyjdzie ktoś go zbić. To nie groźba."
* Czesław Blakenbauer: "I oto jestem!"
* Arianna: (w śmiech) "Czemu Wy zawsze robicie takie wejścia! Mogłeś sobie RAZ odpuścić! Ostatnim razem jak pomocnik to zrobił to sprowadziłyśmy niepotrzebnie kłopoty. Porozmawiajmy zamiast cyrki."
* Viorika: ... (patrzy z niesmakiem)
* Czesław: "Nie wiem kim jesteś, tien Verlen, ale nie próbuj swoich mrocznych sztuczek. Nie chcę z Tobą walczyć, tu są dzieci." (na to poruszenie wśród starszyzny)
* Viorika: "Bo przegrasz"
* Arianna: "To jest nas troje"
* Czesław: "Więc oto jestem. Czego pragniesz, tien Verlen? (zmierzył Viorikę spojrzeniem i coś chciał powiedzieć, ale się ugryzł w język)"
* Arianna: "tien Arianna Verlen."
* Czesław: "(smutek) To niemożliwe. ARCYMAG Arianna Verlen nie szłaby tu sama."
* Viorika: "Dlatego jest ze mną."
* Czesław: "(większy smutek) Czyli Viorika Verlen? Deadly Duo? Problematyczna Parka?"
* Viorika: "Dobrze wiedzieć, jakie się ma ksywy wśród sąsiadów"
* Arianna: "Ja też mam jakąś? Poczuję się urażona jak nie mam"
* Czesław: "Nie pokonacie mnie tak łatwo! Jestem Czesław Blakenbauer, najlepszy weterynarz jaki istnieje"
* Viorika: "A to bardzo prawdopodobne."
* Czesław: (spojrzał podejrzliwie) "Czasami przychodzę ratować niewielkie mieściny przed problemami."
* Viorika: "Po cholerę zeżarliście nam kuzyna?"
* Czesław: "No po co nas atakował? Powiedziałem, że ten potwór da się odgonić, nie trzeba go krzywdzić, nie wróci, ale NIEEEE zastrzelmy go, przyzwijmy inne."
* Viorika: "Co masz na myśli?"

Ta trójka zaczęła sobie docinać z uwagi na zwyczaje, braci itp. Czesław chciał pomóc miasteczku odganiając potwory. Nie wiedział o bracie. Viorika mu docina to on jej wyciągnął nawet KAROLINUSA. To jest lekko bezczelne. I nie jest bardzo znane. Czesław powiedział, że nie ma w planach walki z Verlenami.

Czesław wyjaśnił, że jest konkretny kuzyn - Tyraniusz Blakenbauer, który z radością weźmie na siebie wszystko złe. 

Czesław nie mając nic jako skuteczny neutralizator Gadobija użył "antybiotyku dla świń", który działa, ale ma się po nim upiorną sraczkę. To był efekt uboczny.

Plan:

* A+V i Czesław robią Mroczny Plan Tyraniusza - co tu robił, jaką miał agendę itp
* Że niby przyszedł Gadobij i uratował miasteczko, za co Tyraniusz go skrzywdził
* ...i to jest do przekazania Wielobijowi. Łącznie z Najbardziej Prawdopodobną Lokalizacją Tyraniusza.
* Czesław przekaże Tyraniuszowi, że "Problematyczna Parka" to czarodziejki z którymi da się dogadać.

Sekrety Serpiniek zostały uratowane. A Czesław weterynarz nie ucierpiał z ręki Wielobija.

## Streszczenie

Arianna i Viorika są wyczulone na chłopców Verlenlandu po sprawie z Mścizębem, co doprowadziło ich do Gadobija zatrutego przez potwora i - co gorsza - Blakenbauera. Podejrzewając nieporozumienie, tienki spowolniły działania Wielobija i obiecały mu pomoc i weszły do Serpiniek - miasteczka, w którym doszło do sprawy. Okazuje się, że zarówno Gadobij jak i Blakenbauer chcieli pomóc miasteczku a wyszło jak zwykle. Verlenki dogadały się z Blakenbauerem i winę zrzucono na kozła ofiarnego - Tyraniusza Blakenbauera, który specjalnie bierze takie rzeczy na siebie.

## Progresja

* Arianna Verlen: znana jako element Problematycznej Parki z Vioriką
* Viorika Verlen: znana jako element Problematycznej Parki z Arianną

## Zasługi

* Arianna Verlen: zamiast zwalczać Wielobija, weszła z nim w sojusz i przekonała do współpracy. Potem nawiązała dialog z Czesławem Blakenbauerem i dyplomatycznie rozwiązały sprawę, zrzucając winę na Tyraniusza zamiast tępić weterynarza który po swojemu próbował ratować miasteczko.
* Viorika Verlen: pozyskuje informacje od ludzi Gadobija, potem zrobiła dobry zwiad w Serpiniek i wykryła obecność Blakenbauera. Udaje szorstką i ostrą w rozmowie by ułatwić Ariannie.
* Gadobij Verlen: usłyszał o potworze i przybył ratować ludzi w Serpiniek; niestety, poszedł "heroicznie" i starł się z Czesławem Blakenbauerem chcącym tego samego. Skończył zatruty na kiblu z amnestykami. Bardzo sympatyczny młody Verlen.
* Wielobij Verlen: przybył pomścić brata z oddziałem gotowym do walki, jednak wykazał się rozsądkiem, akceptując plan Arianny i Vioriki. Gdy już widzi, że nikt nie nastaje na honor brata, pomocny i życzliwy innym Verlenkom, choć nie przepuści Blakenbauerowi.
* Czesław Blakenbauer: weterynarz przekonany o okrucieństwie Verlenów; gdy Gadobij się pojawił to wzmocnił Hydrę by ją wyleczyć i pomóc. Podał Gadobijowi antybiotyki-amnestyki (niestety); na szczęście, posłuchał Arianny i Vioriki, dogadał się z nimi i uniknął Krwawej Zemsty ze strony Wielobija.
* Ewelina Mistowir: lekarka-verlenlandrynka w Krwawirogu; zdiagnozowała stan Gadobija i odkryła, że zatrucie ma magiczne podłoże, co skierowało śledztwo na właściwe tory. Wykryła, że Gadobij i jego ludzie mieli pomoc medyczną. Zafascynowana Arianną; fangirl, mimo, że ma 53 lata.
* Grażyn Ozdamrin: członek starszyzny Serpiniek w Przelotyku; nieufny wobec Verlenek, ale pomógł utrzymać spokój w Serpiniek. Zależy mu na mieście i na spokoju w nim. Inteligentny.
* Tyraniusz Blakenbauer: desygnowany przez Blakenbauerów kozioł ofiarny na krzywdy Verlenów; świetnie się bawi jako Wielki Zły. Tym razem wziął winę za zatrucie Gadobija.

## Frakcje

* .

## Fakty Lokalizacji

* Krwawiróg: w Verlenlandzie, niedaleko Przelotyka; słynie z dobrego szpitala.
* Serpiniek: w Przelotyku, obszar Pasma Przyverleńskiego; znajduje się tam Hydra chroniąca miasteczko.
* Serpiniek: słynie z bardzo dobrego jedzenia o lekkich parametrach medycznych; nieźle ufortyfikowane i z dobrymi procedurami. Mimo, że dookoła są problemy, tu nie przychodzą.
* Serpiniek: częściowo w ścianie skalnej, częściowo poza nią. Rolnicze 'pueblo'-pattern.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. Krwawiróg
                    1. Przelotyk
                        1. Pasmo Przyverleńskie
                            1. Serpiniek
## Czas

* Opóźnienie: 11
* Dni: 3

## Inne
