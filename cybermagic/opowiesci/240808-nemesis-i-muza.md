## Metadane

* title: "Nemesis i Muza"
* threads: mscizab-rywal-arianny
* motives: the-corruptor, the-addicted, broken-mind, gift-is-anthrax, dziwna-zmiana-zachowania, lost-my-way, zawisc-mordercza, potwor-spawner
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [240807 - Jastrzębiusz, tien, który nic nie widział](240807-jastrzebiusz-tien-ktory-nic-nie-widzial)

### Chronologiczna

* [240807 - Jastrzębiusz, tien, który nic nie widział](240807-jastrzebiusz-tien-ktory-nic-nie-widzial)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * Lydia the Bard "Isabella's Villain Song": 
        * "Stand upstraight, stick it through | Uphold the family that hurts and harms you and asks too much of you"
        * "How could you when I was just a child | Support the family don't stray display obey you must be perfect"
        * "what could you possibly expect was gonna happen when you made me | tend to a plant with poison and expect a flower are you crazy?"
        * Kluczem jest **smutek** oraz przekroczenie "breaking point"
    * Inspiracje
        * "Black Swan" (film), o perfekcji i obsesji perfekcji i jak daleko się można posunąć
        * Vinerva @ Shadows of the Forbidden Gods
            * Vinerva to bogini, która daje bogactwo, zdrowie i potęgę, skażając serca i prowadząc do zniszczenia
            * Vinerva, technicznie, jest **the-corruptor**. Czyli ona pragnie Cię wzmocnić by Cię usidlić.
            * Złapała i usidliła Mścizęba
* Opowieść o (Theme and vision):
    * "Dziecko, nigdy dość dobre, naciśnięte za mocno, próbuje wygrać szacunek i miłość. Ale nie daje rady."
    * "The child who receives no warmth will burn the village just to feel its warmth"
        * Mściząb bardzo pragnął być jak Arianna czy Viorika czy inni wartościowi członkowie Verlenów. Ale nie jest w stanie.
        * 2 lata temu Mściząb natrafił na Ariannę gdy robił coś bardzo głupiego i ona go upokorzyła.
        * Mściząb miał pecha natrafić na istotę [Interis/Esuriit]. Coś, co "daje" mu moc w zamian za ofiary i odbiera mu człowieczeństwo.
        * Mściząb pragnie osiągnąć swój poziom kompetencji i umiejętności. Pragnie być JAK ARIANNA. Nie ma po co żyć jako śmieć.
    * Lokalizacja: Verlenland
* Po co ta sesja?
    * Jak wzbogaca świat
        * Pokaże, jak Verlenowie traktują "słabe" jednostki
        * Pokaże wysoki poziom autonomii w rodzie Verlen
        * Pokaże pomocnych Verlenów, którzy chcą pomóc - i tych, którzy trzymają się okrutnych reguł Verlenlandu, starej daty.
    * Czemu jest ciekawa?
        * Pokazuje Ariannie i Viorice ich własne lustro
        * Dylemat: uratować OFIARY młodego Verlena czy samego Verlena czy spróbować uratować wszystko ryzykując utratę wszystkiego?
    * Jakie emocje?
        * "Mściząb znowu coś odpierniczył..." -> "...co on zrobił... czemu..."
            * Od lekkiej frustracji do pełnego szoku - Fallen Verlen
            * I zrozumienie DLACZEGO upadł. I swojej roli w tym upadku.
        * "Coś tu jest poważnie nie tak, ale damy radę" -> "CZEMU NIE WEZWAŁYŚMY POMOCY!"
            * Od przekonania o możliwościach do uzmysłowienia sobie z CZYM walczą
            * Dlaczego Verlen zarządzający tym terenem nic nie robi i go nie ma?!
        * "Nie chcę musieć wybierać Miasteczko / Mściząb!"
            * prawdziwa pokusa związana z Darami Leniperii - I can get it all
            * dylemat. Mściząb w końcu mógł być 'sensowny', ale nie miał szczęścia
        * "Rozwalę tego potwora!!! Za Mścizęba!!!"
            * widząc, co Leniperia zrobiła Mścizębowi.
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * the-corruptor: Lycia Sylver, prawdziwa mastermind stojąca za wydarzeniami i wszystkimi potworami. Ta, która dotarła do Mścizęba i skorzystała z jego słabości. Ta, która chciała więcej Verlenów na chwałę rodu Sylver.
    * the-addicted: Mściząb Verlen. Jego narkotykiem jest potęga i akceptacja Rodu, jest możliwość upokorzenia Arianny i udowodnienia, że jest lepszy od niej. Coraz bardziej traci wszystko by stać się jak ona. Unchained.
    * gift-is-anthrax: Leniperia zostawia podarunki i możliwości wzmocnienia się; te podarunki wzmacniają, ale kosztem powolnego uzależnienia od Leniperii.
    * broken-mind: Mściząb, który stracił wszelką nadzieję i odrzucił Verlenland, stając się terrorformem. MUSI zniszczyć Ariannę.
    * dziwna-zmiana-zachowania: Mściząb zrobił się kompetentny oraz bardzo agresywny wobec Arianny; rywalizuje z nią i może nie daje rady, ale jest bliżej niż powinien być. Jest też okrutniejszy.
    * lost-my-way: Mściząb. Wczorajszy Mściząb by krzyczał i płakał widząc potwora, jakim się ostatecznie stał. Dzisiejszy poświęca ludzi by się wzmocnić i służy Leniperii.
    * zawisc-mordercza: Mściząb wobec Arianny - jego skaza sprawiła, że Leniperia miała otwarcie i mogła Mścizęba Dotknąć.
    * potwor-spawner: Leniperia jest w stanie wykorzystać część swojej energii by móc spawnować inne potwory.
* O co grają Gracze?
    * Sukces:
        * Uratować Mścizęba
        * Uratować Miasteczko
        * Karasiożer jest bezpieczny przed Lycią i Leniperią
        * Odegnać Lycię i Leniperię z Verlenlandu
    * Porażka: 
        * W służbie Leniperii
        * Śmierć zarówno Mścizęba jak i Miasteczka
        * Karasiożer przyjął dar Leniperii
* O co gra MG?
    * Highlevel
        * Chcę pokazać im słabe i mroczne strony Verlenów. Te fajne już znają.
    * Co chcę uzyskać fabularnie
        * Spróbuję dołączyć je do Leniperii. Niech się Skażą.
            * Używając Interis zabiorę im umiejętność ostrzeżenia o Leniperii.
        * Karasiożer dołącza do Leniperii.
        * Zarówno Miasteczko jak i Mściząb są zniszczone.
        * Nie są w stanie ostrzec nikogo przed Leniperią; ona nadal tu jest i się żywi.
        * Wszyscy są przekonani, że to wszystko jest winą Arianny - tak jak Elena.
        * Pojawia się Mroczny Oddział Leniperii.
* Default Future
    * Miasteczko zostanie zjedzone
    * Mściząb wciągnie pod kontrolę Leniperii Karasiożera Verlena
    * Leniperia będzie w stanie rozprzestrzenić się na kolejne miejsca; ma niewielki, dobrze ukryty oddział
* Dilemma: Mściząb CZY Miasteczko? Wezwać pomoc CZY samemu?
* Crisis source: 
    * HEART: "The child who receives no warmth will burn the village just to feel its warmth"
    * VISIBLE: "Znikają ludzie w niedalekiej okolicy"
        
### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

* Mściząb napotkał Lycię (wspieraną przez Leniperię) na jednej z innych operacji. Jego głód pokonania Arianny sprawił, że Lycia znalazła okazję.
* Mściząb przyjął kilka Darów Leniperii niszcząc tworzone przez nią istoty, dzięki czemu jego reputacja zaczęła rosnąć.
* Leniperia przybyła do Rinosates; zaczęła rozpuszczać macki. Mściząb - nieświadomy - wspiera Leniperię w zniszczeniu Rinosates.
* Karasiożer przybył z rozkazu Jastrzębiusza, niech się uczy od lepszych od siebie.
* Ludzie znikają, Mściząb zwalcza Spawnera, ale za wolno i nieskutecznie
* Leniperia skupia się na ludziach próbujących opuścić to miejsce
* Część ludzi wie o tym że "emigranci nie dotarli", ale Karasiożer ani Mściząb nic z tym nie robią
* Palaos decyduje się na ryzykowną wyprawę - niech Viorika lub Arianna pomogą...

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

* "The Spawner" - wprowadzenie potwora, pierwsza sesja
    * Faza 2: "Spawner falls" (75 min) <- Mściząb 
        * Tu celem Mścizęba jest skłonienie Karasiożera do wzięcia Darów Leniperii.
            * s1: brak nadziei, starcie co i jak (konieczność deeskalacji)
            * s2: starcia z oddziałem potworów (wzięcie tabletek)
                * hulk, kamienniak
            * s3: porwani ludzie - Mściząb i Karasiożer chcą wejść odzyskać (z sukcesem!)
                * decadia invictus na pierwszej linii
        * Operacja wykorzystania ludzi by zlokalizować i zniszczyć Spawnera
            * s3: 
        * Uda się, ale straty...
        * Mściząb chce zniszczyć Dekadię Invictus, infiltracja + wysunąć ich na pierwszy ogień
        * -> aktywnie graj o zniszczenie miasteczka i porywanie ludzi przez Spawnera
        * -> aktywnie graj o to, by Mściząb zniszczył Spawnera
        * -> aktywnie graj o to, by Karasiożer pozyskał Dar Leniperii
        * -> aktywnie graj o śmierć cywili i Dekadię Invictus CHYBA że Gracze użyją Darów
        * -> pokaż, że to NIE KONIEC problemu 
            * (nie zgadza się ilość osób znikniętych)
            * (nie zgadzają się opowieści tych co zostali)
            * (potwór, który został)
* "The Mastermind" - wprowadzenie Leniperii, druga sesja
    * Interludium: to-co-będzie-potrzebne-do-demonstracji
    * Faza 3: "Konspiracja narasta"
        * ? 
    * Faza 4: "Mastermind ujawniony, wojna domowa"
        * ?
* Overall
    * chains:
        * energia i zasoby Zespołu: max 2
        * morale Zespołu: max 2
        * Dusza Karasiożera: (pragnę Daru - Mściząb ma rację - (Mściząb jest super) - Arianna jest super - można zostawić to innym)
        * To wszystko wina A+V: ((nie) - w sumie bo tu są - tak - gdyby nie one byłoby dobrze)
        * Służki Leniperii: ((nie) - wzmocnienie działa - muszę mieć więcej - upadłe Verlenki)
        * rozpad Rinosates: (pokój - (starcia) - przelew krwi - interwencja tiena - wojna domowa)
        * Armia Leniperii: (brak - byle co - (przydatna) - oddział - spora - legion)
        * problemy polityczne: ((brak) - czemu to są A+V - czemu szkodzą A+V - wygnanie stąd - wojna domowa)
    * stakes:
        * śmierć Mścizęba, śmierć Karasiożera, Dar Karasiożera
        * reputacja Arianny i Vioriki, problemy polityczne
        * rozprzestrzenienie się sił Leniperii
        * śmierć Rinosates, zniszczenia w Rinosates
    * opponent
        * Mściząb, Spawner, Leniperia
    * problem
        * Leniperia jest nie do ruszenia
        * Mściząb jest 'za daleko' i jest złamany przez Verlenów (a potem przez Leniperię)

## Sesja - analiza
### Mapa Frakcji

* Jastrzębiusz Verlen (Lokalny tien Verlen)
    * potężny politycznie izolacjonista, pro-darwinistyczny; ludzie to zasób
    * oneliner: "jeśli sobie nie poradzą sami, będę interweniował - ale zapłacą"
    * eksport: (irrelevant)
    * import: (irrelevant)
    * zasoby: silna siła ognia, magitech
    * agenda: 'stal hartuje się w ogniu', 'za wszystko płacisz'
    * cechy: 'piekło było na Ziemi, on stał na górze', skrajny militarysta
* Kult Kła Zemsty Rinosates (około 300 osób): 
    * pro-leniperiański kult stworzony przez Mścizęba Verlena
    * oneliner: "Siła i determinacja ugasi Twoje cierpienie"
    * eksport: bezpieczeństwo, szkolenia bojowe, broń, morale, usługi (patrol, fanatyczna walka)
    * import: ludzie (okolica), Dary Leniperii (Leniperia)
    * zasoby: oddział, fanatyzm, broń, znajomość lokalsów i terenu
    * agenda: ekspansja, ochrona okolicy
    * cechy: edgy warriors, fanatical
* Rinosates Decadia Invictus (około 80 osób):
    * (będzie ciekawiej, jeśli to miasteczko z populacją postnoktiańską; pasuje do przyszłej historii Arianny)
    * dekadiańska grupa samopomocowa dążąca do rozwiązania co tu się dzieje
    * oneliner: "noktianie powinni trzymać się noktian"
    * eksport: bezpieczeństwo, wiedza o anomaliach, wiedza o terenie, hideouts
    * import: sprzęt, intel, material, medykamenty
    * agenda: co tu się dzieje?, ochrona okolicy, ochrona swoich
    * cechy: bardzo nieufni, klanowcy dekadiańscy, anty-magowie, anty-verlenowie
* Rinosates Lojaliści Verleńscy (około 900 osób):
    * lokalni ludzie nadal wierzący w Verlenów i w ich pomoc
    * oneliner: "pomagali nam a my im; to nowa sytuacja"
    * eksport: (irrelevant)
    * import: (irrelevant)
    * zasoby: dostęp do większości miejsc i rzeczy w Miasteczku
    * agenda: ekspansja, przetrwanie, wspieranie Verlenów
    * cechy: pozytywnie nastawieni, przetrwamy piekło, populacja
* Rinosates Proemigranci (około 700 osób):
    * lokalni ludzie uważający, że Verlenowie zawiedli
    * oneliner: "mają jedno zadanie - a i tak zawiedli"
    * eksport: główne źródło ofiar dla Potworów
* Unaffiliated (około 4k osób)

### Potwory i blokady

* Jednostka Oddziału Spawnera
    * akcje: "wolno przeładowujący się strzał", "ranię szponem", "udaję kamień"
    * siły: "niewidoczny bez ruchu", "bardzo silny", "spojrzenie Koszmaru"
    * defensywy: "bardzo ciężki pancerz"
    * słabości: "wolny i toporny", "niezgrabny", "wolno strzela", "mało celny"
    * zachowania: "atakuję frontalnie", "atakuję z zaskoczenia"
* Spawner
    * akcje: "wystrzeliwuję Sieć Rozpaczy", "zamykam ofiarę w kokonie", "atakuję pnączomacką", "Oczy Terroru", "Chowam się w ścianie", "spawnuję Wojownika"
    * siły: "atak z tysiąca stron", "corrupting touch", "nieprawdopodobnie silny"
    * defensywy: "macki zasłaniają skałą", "nieciągły byt roślinny", "Interis chroni przed żywiołami"
    * słabości: "wolny", "nieciągły i ma jądro"
    * zachowania: "otacza przeciwnika", "corruption over destruction", "atakuje spod ziemi"
* Labirynt Korytarzy
    * akcje: "zagubienie", "nie da się manewrować", "za ciasno by przejść", "atak od tyłu", "szczelina"
    * siły: "ryzyko zawału", "odcięcie od sojuszników", "spowolnienie ruchów bo ciasno", "fałszywe sygnały sensorów"
    * słabości: "stałe punkty orientacyjne"

Inne - jak się manifestują Dary Leniperii?

* Dziwne Zwoje, wzmacniające użytkownika
* Dziwne Żywe Tatuaże
* Stymulanty (w strzykawkach)
* -> ważne: nie działają, jeśli ich nie weźmiesz aktywnie.

### Fiszki

* Ślicznospad Verlen: tien Dun Latarnis; 31-letni elegancki miłośnik sztuki i kompetentny administrator. Elegancik, ale dobrze walczy. Pattern: (perfumowany arystokrata)
* Tiana Grolmik: podkochuje się w Mścizębie i zostaje przynętą; lojalna i naiwnie odważna. Pattern: (młoda Asuka Langley)
* Jastrzębiusz Verlen: Zimny, pragmatyczny, darwinista, skupiający się na eksperymentach i militariach. Pattern: (Shockwave IDW)
* Karasiożer Verlen: Optymistyczny i zaczepny aspirujący wojownik, 16 lat, Exemplis+Alucis. Pattern: (Powerglide G1)
* Palaos Kregner: Stary zwiadowca, przekradł się wierząc, że Viorika pomoże. Noktianin, klarkartianin. Pattern: (Westley 'as you wish')
* Tiberius Skarn: członek Decadii Invictus; silny wojownik, acz już stary. Poświęci się za przyjaciół. Pattern: (stary Schwarzenegger)
* Nariad Fargelin: członek Decadii Invictus; łowca nagród, świetny sabotażysta, szlachetny i bystry. Pattern: (commander Ashley Lynx @ Solty Rei)
* Julia Strzałowik: ciekawska i odważna, wierzy we wspieranie Verlenów. Pattern: (Hermiona Granger)
* Zara Skarn: młoda, ambitna inżynierka, śmiała i kreatywna, pro-Verleńska. Pattern: (Kaylee Frye z "Firefly")
* Szymon Garwok: żarliwy i cichy, 'moja wiara w Verlenów moim sukcesem', Kieł Zemsty. Pattern: (mnich z 'Rogue Squardron')
* Leon Oszakowski: cichy specjalista od walki z potworami. Pattern: (Menasor G1 'power to destroy')
* Kasia Mribkit: infiltratorka, chroniąca Karasiożera, zawsze w okolicy. Pattern: (Ravage IDW)
* Eryk Tewatrik: arogancki naukowiec i wynalazca, który 'odkrył' Dary Leniperii. Pattern: (Brainstorm IDW)
* Mściząb Verlen: magia przede wszystkim bojowa i działania rozproszone (delegacja). Ekspresja: kontrola, link (Exemplis, Praecis).
* Symfonides Verlen: soniczny Verlen z wielkim sercem (Exemplis, Alteris). Pattern: (Michaelangelo+Blaster G1)
* Gwiazdodeszcz Verlen: AoE, masowy deszcz destrukcji, kochający efektowne rzeczy. (Exemplis, Praecis). Pattern: (Archer FateStayNight + dramatics)
* Techniczuś Verlen: kocha high-tech, konstruktor, precyzyjny miłośnik technologii. (Exemplis, Praecis). Pattern: (Donatello + lubi mieć wrażenie u dziewczyn)

### Scena Zero - impl

Wejście do tej sesji - co już wiemy:

* Jako, że Elena pokazała słabość w oczach jastrzębi Verlenlandu, Arianna ma presję społeczną by pokazać że jest GODNA.
* Do Arianny i Vioriki dotarł zwiadowca (Palaos) z Dekadii Invictus
* Jastrzębiusz Diakon jest super-jastrzębiem; Mściząb i Karasiożer próbują rozwiązać problem potwora na terenie Rinosates
* Jastrzębiusz wierzy, że Arianna i Mściząb to dwójka kochanków; nie patrzył na ten teren. Sentisieć nie WIDZI niektórych rzeczy.
* Mściząb zachowuje się jak nie on: triggery, wzmocnienie?, odciął Karasiożera z sentisieci. A na terenie jest potwór działający jak strateg.
* Mściząb buduje coraz silniejszą frakcję Kłów Zemsty, silnie oddanych mu agentów.
* Ludzie znikają i pojawiają się siły i oddziały Potwora. A Jastrzębiusz niczego nie widział.
* Arianna i Viorika mają plan (jeszcze nie zrealizowany)
    * CZEMU: czy to nie pułapka? czy Mściząb jest sobą?
    * Viorika przeładowuje Mścizęba sygnałami (by go przeciążyć i by wyłączył triggery na sentisieci)
    * Arianna kontaktuje się z Decadią Invictus
* Mściząb będzie ignorował większość triggerów które da się sensownie ukryć i zamaskować. Jego zdaniem większość to fałszywe pozytywy. No i ta cholerna impreza Verlenów - RÓŻNI SZUKAJĄ GŁUPICH RZECZY.
* Tiberius jest przekonany, że Mściząb wystawia Dekadię Invictus na eksterminację i próbuje budować swoje siły
* Tiberius znalazł (po radach Arianny i na co patrzeć) serię zarażonych agentów. Z 80 Decadia Invictus zarażonych jest 11. Są spenetrowani.
* Zgodnie z pomysłami Arianny i Vioriki, Jastrzębiusz nie wykazał się kompetencją tiena rodu.
* Karasiożer gardzi Eleną i nie chce być tak bezużyteczny jak ona; jest młodym zaciekłym idealistą.

### Sesja Właściwa - impl

Jastrzębiusz się nie patyczkował jak podjął decyzję.

4 ciężkie czołgi. Cały oddział. Około 1000 osób. Rekwizycja wszystkich Verlenów na miejscu - Symfonides, Gwiazdodeszcz, Techniczuś. Nawet nie wiedzieli co się stało.

* T: Ale... tien Jastrzębiuszu, co się dzieje? (jeszcze jakąś dziewczynę głaszcząc)
* Jastrzębiusz, w Chevalierze: (absolutny niesmak) Zdrajca w szeregach Verlenów. Jest zdrajcą albo był słaby. Tak czy inaczej, ratujemy go.
* Verlenowie: (cisza) (bez gadania zaczęli się zbierać)
* Jastrzębiusz: za wolno, panowie. Bierzcie przykład z dziewczyn (wskazał Ariannę i Viorikę).

W ciągu 10 minut mają podstawową grupę szturmową. Ale do zebrania pełni sił potrzeba 2h. Jastrzębiusz nie chce czekać.

* G: Jastrzębiuszu kochany, powiedz mi, nie będziemy używali artylerii? Bo to nasi ludzie?
* J: Nie. Precyzyjne, bezwzględne uderzenia. Nie zabijamy verlenlandczyków.
* G: Tak jest.
* J: Sentisieć nie jest stabilna, więc uważajcie. Coś jest z nią nie tak.
* T: Ale... to Ty dowodzisz sentisiecią.
* J: (głosem jak do dziecka) Dlatego Wam to mówię.
* Jastrzębiusz: musimy przyjąć założenie, że wszyscy kultyści staną przeciwko nam.
* Viorika: Dostosowujemy się do sytuacji
* J: (z lekkim przekąsem) Powiedziała jak prawdziwy generał Verlenów. 
* Viorika: Jak prawdziwy generał, który był w stanie coś wygrzebać.
* J: (winced) Proponuję standardową Verleńską taktykę. Wchodzimy, unieszkodliwiamy, wychodzimy. Nie rozmawiamy. Ogłuszamy na wejściu.
* S: Tien Jastrzębiuszu! (szok) (wszyscy Verleni są w szoku)
* J: Ja decyduję o planach, ja dowodzę terenem

Arianna po hipernecie pokazała innym szczegółowy briefing. Verlenowie odpisali sygnał "THANKS!". Viorika się zorientowała, że Mściząb jest GOTOWY na atak - plan Jastrzębiusza niekoniecznie jest idealny. Jastrzębiusz ma prosty plan - młot rażący, uderzyć z zaskoczenia, zagazować zewnętrzną część miasta i go unieszkodliwić. Plan zadziała jeśli ma przewagę technologiczną i zaskoczenia.

Jastrzębiusz, na prośbę Arianny, skontaktował się z Karasiożerem by wydobyć z niego informacje.

Ex +3 +9Og (reprezentują sukces; zaawansowane Ex oznacza przekazanie danych):

* V: Karasiożer WIDZIAŁ Muzę
    * Jastrzębiusz wyjaśnia: 
        * piękna Blakenbauerka z podziemi. Nazywa się Lycia. Ona pomaga Mścizębowi osiągnąć sukces przeciw Nemesis.
        * ona zapewnia, by niektóre złe rzeczy się nie mogły stać, np. Nemesis nie może łatwo atakować ludzi. Mściząb ją chroni w sentisieci.
        * Lycia jest niezwykle cicha i dyskretna. Na pewno jest drakolitką i to widać. Jest PIĘKNA.
    * Lycia jest pod ziemią. Tam jest ze swoim Potworem. Ma też część rannych verlenlandczyków i ich leczy.
        * Potwór jest takim... glutowatym stworzeniem. Syntetyzuje leki, stymulantem itp.
        * Lycia musi być blisko powierzchni.
    * Zdaniem Karasiożera Mściząb jest głęboko zakochany.

Arianna próbuje szybko dojść do tego czy się pokrywają ludzie, którzy mogli widzieć Muzę z tymi z Dekadii Invictus.

Tp +3:

* X: Agenci przeciwnika zaraportowali, że jest Narada Wojskowa. Lycia ostrzegła Mścizęba.
* V: Żaden z "czystych" członków Decadii Invictus nie wiedział o Muzie i nie widział Muzy. To implikuje, że jeśli ktoś ją widział, to będą "agenci".
    * Najpewniej mamy Blakenbauerkę z Interis?

Viorika zbiera oddział i dobiera wyposażenie do oddziału pod kątem misji do wykonania. Chce pójść na mniej lub bardziej dyskretny zwiad w stronę tuneli. Viorika wybiera grupę noktian z Dekadii Invictus, czystych, i wysyła ich na zwiad po tunelach. Chce iść z nimi. Klasyczne 'scout commando action'. I za pomocą sentisieci i znajomości tuneli i wiedzy noktian - zachodzimy Muzę od dołu. Sześciu dobrych zwiadowców plus Viorika schodzą w głąb tuneli Verlenlandu. Viorika jest bardzo wyczulona na sentisieć; to jest to o co jej chodzi. Nawet jeśli Mściząb ukrywa Muzę, jej ślady zostają w sentisieci. Więc Viorika może lepiej wyczuć obecność Muzy lub jej ślad. Więc chodzi o takie poruszanie się po tunelach, by znaleźć ślad w sentisieci - a zwiadowcy pokażą jej w miarę bezpieczne przejścia

Ex +4:

* V: Viorika szuka śladów Nemesis albo Muzy. Nie ma szczególnej preferencji którą znajdzie.
    * są skalniaczki, ale nie widzą Vioriki i Zespołu
    * skalniaczki stoją by pilnować CZEGOŚ. One COŚ obstawiają. Po prostu nie były przygotowane na takie przejście - że one nie widzą dobrze. Nie mają dobrych detektorów.
    * lokalizacyjnie to idzie bardziej w stronę imprezy. Głęboko pod imprezą.
* Viorika przygotowuje się do walki jakby co i wysyła aktywnego pinga po sentisieci. Chce zobaczyć co się tam dzieje. Chce zamapować tunele.
    * +3Ob: Viorika wysyła ping. Poznajmy rozkład terenu pilnowanego przez skalniaczki
* V: Viorika wysłała pinga. Sentisieć zachowuje się ślamazarnie i dziwnie, ale odpowiedziała. Zdaniem sentisieci nie ma tu żadnych skalniaczków. Pokazuje Ci pieczarę. Ale ona jest pusta. Nie ma tu żadnych skalniaczków. Sentisieć "jest na środkach usypiających". W takim razie Viorika kazała sentisieci maksymalnie osłonić dronę i wysłała dronę zwiadowczą.

Przed Vioriką pieczara. A w niej, wielki dziwny kwiat. Są ludzie którzy są podpięci do tego kwiatu. Kwiat pulsuje jak żywa istota i coś syntetyzuje. Ludzie są żywi - jest ich kilkunastu. Są sparaliżowani i nic nie mogą zrobić. Część z tych ludzi to osoby, które zniknęły. Oni zasilają kwiat, kwiat coś w nich wtłacza. Cała jaskinia jest... miejscem rytuału. Kwiat wygląda jak pasożyt na tej jaskini.

Viorika zdecydowała się bezpiecznie wycofać ze swoimi noktianami. Coś tu poszło nie tak i warto o tym wiedzieć.

Arianna zaproponowała Jastrzębiuszowi inny plan - ONA pójdzie do Mścizęba a Jastrzębiusz i Verlen pójdą do Muzy. Bo tego się nikt nie spodziewa. Arianna tłumaczy, że to jego plan i ona jest marionetką a tam wejdą kompetetni ludzie i kompetentny dowódca

* J: Jeśli potrzebujemy kompetetnych ludzi to po co mi ta trójka (nie zakwestionował kompetentnego dowódcy)

Jastrzębiusz się zgodził. Z perspektywy taktycznej ma to sens, choć to "niewłaściwe". Ale dobrze, zrobimy to.

ZASKOCZENIE przeciwnika:

Tr Z +3:

* X: przeciwnik się tego może nie spodziewał (nie wiemy), ale był gotowy na różne rzeczy, bo Arianna jest arcymagiem a Viorika strategiem
* X: przeciwnik nie spodziewał się TEGO, ale miał doskonale zastawioną pułapkę i plan, więc nie jest całkowicie rozbity
* X: przez to, że trójka pozostałych Verlenów nie potraktowała tego poważnie, Jastrzębiusz ma hard mode.

WYNIK Jastrzębiusza:

Ex +3 +3Ob (i 3 krzyżyki krytyczne w tym jako Xr): VrXX

* Jastrzębiusz przetrwał, ciężko ranni Verleni, ogromne straty w ludziach, Lycia się wycofała, Lewiatan też

JAK ARIANNA CHCE PODEJŚĆ DO MŚCIZĘBA. Mściząb jest bardziej w obozie, mniej w budynku. Jest w Chevalierze. Ma kilka wyjść z tuneli, są dobrze obstawione. Są trzy. Ale ten teren ma ich ogólnie sporo. Część tuneli zaminował. I będzie miał jeden ukryty tunel dla Lycii.

Arianna idzie za planem - zagazować tych, którzy są przy zejściach, po czołgu na wyjścia - czyli uderzyć w siły Mścizęba i zabezpieczyć teren przed wyjściem istot Nemesis. Dodatkowo - nie chodzi o gaz usypiający a o 'smoke' który ma sprawić, że przeciwnicy są ślepi i nic nie mogą zrobić. A sama Arianna przygotuje rytuał, by dać swoim ludziom 'smoke-piercing'.

Arianna chce dość duży oddział potraktować magią, więc mówimy o DUŻEJ ilości energii. Przepuszczamy to przez visiat Arcymaga, więc nie masz 'podniesionego Paradoksu'. Stan emocjonalny - stabilny, nikt nie musi ginąć. Więc jej zależy. Plus Arianna nie może przegrać przy Mścizębowi, to jak "być pobitym przez dziewczynę". Źródło energii: sama Arianna. Ona jako generał, Verlenland ją wspomoże. Zaklęcie jest dość subtelne; energia pochodzi ze skali. I ma próbkę dymu.

Tr Z M +3 +3Ob:

* V: Żołnierze Arianny mają przewagę militarną. Przeciwnicy dalej mogą walczyć, ale straty będą małe.
* X: Arianna się świeci; ma status Skażona. Za dużo energii nawet dla niej.
* Ob: Arianna i Mściząb stają się "championami". Stają naprzeciw siebie jako rywale, "kochankowie" (do niczego nie doszło ale chodzi o legendę). To jest Prawdziwe Starcie. Sentisieć nie wpływa na bitwę.
* X: Mściząb wyczuł to zaklęcie zanim Arianna dotarła. On wie że to jest ostateczne starcie ze swoją nemesis.
* V: Minimalne straty w ludziach. Minimalne. Żołnierze Arianny mają dramatyczną przewagę a Mściząb "nie potrzebuje" swoich ludzi czyli z nich czerpie.

Arianna wchodzi na pole bitwy jako Jastrzębiusz. Wpierw spada dym. Potem wpada Arianna.

* M: "Arianna Verlen. (czysta nienawiść)"
* A: (ignoruje, nie jest po to by rozmawiać)

Arianna stawia się w pozycji strzeleckiej by go ustrzelić z działa a potem chce się zbliżyć do Mścizęba.

Tr Z +3 (starcie tytanów):

* V: Arianna skutecznie zmusiła Mścizęba do uniku i wycofania się strzelając, po czym niespodziewanie użyła kotwiczek by się do niego zbliżyć
* X: Mściząb niekoniecznie spodziewał się ruchu Arianny, ale złapał Verlenlandczyka i rzucił go w Ariannę; ta skorygowała lot i wyglebiła o budynek. Uszkodzone ramię, a Mściząb z ostrzem w nią.
* (+Vg) V: Arianna uszkadza budynek by spadł na Mścizęba, teraz on manewruje i Arianna ma lepszą pozycję.
    * M: "Zawsze najlepsza"
    * A: "Zawsze najgorszy"
* V: Arianna wyprowadziła pozorny lekceważący atak, który jest dyskretnym uderzeniem mającym go zranić. Mściząb PRZEWIDZIAŁ to że to manewr; ciął by zabić. Arianna rozpaczliwym ruchem; uszkodzony pancerz na torsie; to by ją zabiło. Celował by zabić.
    * M: "Jesteś przewidywalna. Przereklamowana. Myśleli, że jeśli spróbują mnie zniszczyć, uda im się to?"
    * A: "Prosisz się o to. Gdybym chciała Cię zniszczyć, już byś nie istniał."
    * Mściząb chce zderzyć się z Arianną i wyłamać jej ręce. Ona używa kotwiczek, by się oddalić w bok.
* X: Arianna miała dobry ruch. Ale jest Skażona, jest wolniejsza a on jest za szybki. Lancer nie ma tej prędkości. Faktycznie, palący ból. Jedna ręka jest poważnie uszkodzona. Lancer zabezpieczył Ariannę przed wyłamaniem ręki, ale doszło do zwichnięcia czy coś. A Lancer ma nieaktywną rękę.
    * M: "Jestem lepszy od Ciebie. Mogłem być lepszy. Nigdy mi nie dano szansy."
    * A: "Jesteś Verlenem, nie czekasz aż ktoś Ci coś da. Nie byłeś w stanie sam sobie wziąć."
    * M: "Zabierali mi wszystko. Ja zabiorę Ciebie. Od nich."
    * Arianna manewruje by w zacięciu nie zauważył czołgu. Niech dostanie od czołgu.
* (+3Vg): V: Mściząb PRAWIE złapał Ariannę. Ale strzał z czołgu wyrwał mu bark i rękę. Pełen SZOK.
* (+3Ob): Arianna używa kotwiczek i się do niego "przytula" by go przygnieść. Unieruchomiła Mścizęba pod swoim servarem.
    * M: "(dziki wrzask wściekłości, nie ma w nim nawet cienia umysłu, to jest czysta, zwierzęca nienawiść)"

Arianna TRZYMA Mścizęba i zakotwicza go w ziemi. Dzieje się kilka rzeczy w tym samym czasie:

* Mściząb się nie może wyrwać. Wije się, ale Arianna trzyma mocno.
* Sentisieć zaświeciła się na czerwono. Wykryła krytyczny alert i zagrożenie dla Verlena.
* Pojawiła się Viorika na polu bitwy
* Arianna POCZUŁA niż USŁYSZAŁA "wyrzekam się Was wszystkich! Wyrzekam się Verlenlandu!"
    * Sygnatura Mścizęba się zmienia w sentisieci, INNA sentisieć zaczyna walczyć z Verleńską
* Jego kultyści na polu bitwy zaczynają krzyczeć.

Ex Z +3:

* X: Arianna DEWASTUJE jego ciało. Niszczy co się da, by tylko... dało sie coś uratować
* (+3Ob) X: Viorika używa mocy sentisieci by ustabilizować sytuację. Wzmocnić sentisieć by zaczęła działać.
    * Mściząb się zreintegrował mocą sentisieci Verlenlandu, która próbuje go uratować i utrzymać przez Exemplis. Zerwał Ariannę z siebie. Wszyscy jego kultyści go wzmacniają przez Krew. Jego wzór się przesunął - Praecis Esuriit. Stracił Exemplis. Zreintegrował się z Lancerem - są jednością.

.

* M: "Czego oczekiwaliście gdy mnie stworzyliście? Czego oczekiwaliście, gdy mi wszystko zabieraliście? Verlenland mnie nie zniszczy!"

Po sentisieci: "VERLEN DOWN!" i Jastrzębiusz używający pełni mocy lorda sentisieci by ewakuować się i innych z pułapki. I echo mentalne "Nie jesteś Arianną... taka szkoda. Tyle pracy. Ale się nadasz." Oraz Jastrzębiusz, zimno i spokojnie, wydający polecenia "zastrzelcie ich wszystkich. Oni już nie są naszymi ludźmi."

Viorika wydaje polecenie eksterminacji. Zabić wszystkich kultystów. Masę krytyczną osób zasilających Mścizęba. Oraz - drill bomb. Zniszczyć kwiat.

Arianna wyskakuje z lancera, rzuca szybkie zaklęcie, niech Lancer złapie Mścizęba i odciągnie go od ludzi. Niech biegnie dalej, gdzie może być ostrzeliwywany przez czołg.

+P -> Tr+3:

* +P: Viorika używa sentisieci by w odpowiedni sposób zabijać kultystów i wzmocnić sentisieć by robiła "konflikt wewnętrzny". Vz: nawet nie trzeba było zabić "wszystkich"; dysonans i verleńska sentisieć "ogłuszyły" na moment Mścizęba. Wyraźnie nie jest... w pełni "aktywny".
* +M: Xz: Arianna jest ZBYT ranna; plan prawidłowy, ale wyskoczyła z Lancera i gdy rzucała zaklęcie, ból przeważył. Straciła kontrolę. WSZYSTKO strzela do Mścizęba. Sentisieć nie utrzymała, a z perspektywy Arianny "on jest zdrajcą Verlenów i wszystkiego co oni reprezentują". Sentisieć zaczęła go pożerać a czołgi itp. go rozstrzelały.

10 minut później, jak pandemonium zaczęło się zmniejszać, Jastrzębiusz, Symfonides i dwóch nieprzytomnych Verlenów wraz z 7 ludźmi opuścili jaskinie. Stracili kilkadziesiąt osób. Ale Jastrzębiusz ich ewakuował.

## Streszczenie

Jastrzębiusz Verlen mobilizuje potężne siły, w tym czołgi i żołnierzy, aby stawić czoła zagrożeniu. Podczas przygotowań odkrywają, że Muza to drakolitka (Blakenbauerka? Sylver?) o imieniu Lycia, która wpływa na Mścizęba i wykorzystuje go do swoich celów. Viorika prowadzi zespół zwiadowców do podziemnych tuneli, gdzie odkrywają wielki, pulsujący kwiat zasilany przez uprowadzonych ludzi. Zdają sobie sprawę, że przeciwnik wykorzystuje ludzi jako substrat do tworzenia potworów i że byli bardzo narażeni atakiem biologicznym.

Arianna proponuje plan, w którym to ona skonfrontuje się z Mścizębem, podczas gdy Jastrzębiusz i reszta sił zajmą się Lycią, bo tego się nikt nie spodziewa. W trakcie bitwy Arianna i Mściząb stają naprzeciw siebie w zaciętym pojedynku. Mściząb, pod wpływem manipulacji, odrzuca Verlenland i sentisieć, przesuwa się na Praecis Esuriit i staje się terrorformem. Arianna, mimo odniesionych ran, niszczy Mścizęba dzięki wsparciu swoich oddziałów i Vioriki.

Jednocześnie Jastrzębiusz i jego siły ponoszą ciężkie straty w walce z Lycią i potworem. Choć udaje im się przetrwać, Lycia i jej potwór się ewakuują a straty Verlenów są duże.

## Progresja

* .

## Zasługi

* Arianna Verlen: po odkryciu natury zagrożenia, zaproponowała odważny plan skonfrontowania się bezpośrednio z Mścizębem, odwracając uwagę przeciwnika od głównego ataku na Lycię (co zniszczyło plan Lycii). Pokonała w walce Mścizęba jako terrorforma, z pomocą Vioriki, czołgów itp. Zaproponowała dym na polu bitwy, by minimalizować straty wśród żołnierzy. Dla niej Mściząb był zdrajcą i umarł jak zdrajca.
* Viorika Verlen: wyjątkowa taktyczna bystrość i mistrzostwo w korzystaniu z sentisieci. Poprowadziła zespół noktiańskich zwiadowców w niebezpieczne podziemia, gdzie odkryła pulsujący kwiat zasilany uprowadzonymi ludźmi. Podczas bitwy wykorzystała sentisieć do destabilizacji mocy Mścizęba, koordynując eliminację kultystów, co uniemożliwiło mu pokonanie Arianny.
* Jastrzębiusz Verlen: Po otrzymaniu informacji o zagrożeniu, zareagował błyskawicznie, mobilizując potężne siły wojskowe, w tym ciężkie czołgi i żołnierzy. Zaatakował Lycię i jej siły. Gdy się okazało, że to pułapka, ewakuował pozostałych Verlenów i bez cienia skrupułów eksterminował swoich Skażonych żołnierzy. Bezwzględny ale kompetentny dowódca, choć kiepski tien.
* Mściząb Verlen: Pod wpływem Lycii, znacząco zwiększył swoją moc i przyjął dary Leniperii. W trakcie konfrontacji z Arianną, gdy przegrywał, odrzucił Verlenland i przechodząc na Praecis Esuriit stał się terrorformem. KIA, zabity przez Ariannę, Viorikę i żołnierzy.
* Lycia Sylver: piękna drakolitka, która skusiła Mścizęba na 'jestem tylko słabą Blakenbauerką' i teraz polowała na Ariannę i innych Verlenów. Pokonała w podziemiach CZTERECH Verlenów i oddział, ale nie osiągnęła żadnego celu strategicznego. Wykorzystywała uprowadzonych ludzi do zasilania pulsującego kwiatu i wzmacniania Leniperii. Podczas ataku Jastrzębiusza zdołała uniknąć pojmania, ewakuując się wraz ze swoimi siłami i zadając ciężkie straty Verlenom.
* Karasiożer Verlen: Dostarczył kluczowych informacji na temat działań Mścizęba i obecności Lycii, nieświadomie pomagając w ujawnieniu spisku. Ciężko ucierpiał przesyłając energię do Mścizęba po Darze Leniperii, ale wyjdzie z tego bez szwanku, bo nie wiedział co robi.
* Symfonides Verlen: szybko zareagował na wezwanie Jastrzębiusza, wziął udział w ataku na siły Lycii; ciężko ranny, ale żywy.
* Gwiazdodeszcz Verlen: szybko zareagował na wezwanie Jastrzębiusza, wziął udział w ataku na siły Lycii; ciężko ranny, ale żywy.
* Techniczuś Verlen: szybko zareagował na wezwanie Jastrzębiusza, wziął udział w ataku na siły Lycii; ciężko ranny, ale żywy.

## Frakcje

* .

## Fakty Lokalizacji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. Rinosates
                                1. Poziom podziemny
                                    1. Kompleks Mieszkalny Alfa: są tam trzy kompleksy, mniej więcej po 2k osób każdy
                                        1. Mieszkania
                                        1. Strefa relaksu
                                        1. Magazyny i schowki
                                        1. Strefa Zarządcza Życiem: mikro-reaktor, centrala, ciepło, wentylacja, recykling wody...
                                    1. Sektor ekonomiczny
                                    1. Sektor medyczny
                                    1. Inżynieria
                                    1. Reaktor główny
                                1. Poziom naziemny
                                    1. System obronny
                                    1. Windy i wentylacje
                                    1. Odnawialna energia
                                    1. Domy mieszkalne: nie wszyscy są noktianami i nie wszyscy chcą tak żyć
                                    1. Strefa Handlowa
                                    1. Strefa Produkcyjna

## Czas

* Opóźnienie: 1
* Dni: 1

## Inne

