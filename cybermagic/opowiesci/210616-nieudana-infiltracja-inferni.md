## Metadane

* title: "Nieudana infiltracja Inferni"
* threads: legenda-arianny
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [210818 - Siostrzenica Morlana](210818-siostrzenica-morlana)

### Chronologiczna

* [210818 - Siostrzenica Morlana](210818-siostrzenica-morlana)

### Plan sesji
#### Co się wydarzyło

.

#### Sukces graczy

* Infernia przetrwa bez szczególnych uszkodzeń
* Infernia dotrze do Kariatydy i nawiąże kontakt z Sorianem
* Infernia uniknie ataków Matadora

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Marian Tosen poprosił do siebie załogę Inferni. A dokładniej - trójkę przełożonych. W sprawie Anomalii Kolapsu.

* Anomalia Kolapsu zaczyna "znikać" statki.
* Problemy jakie miała Netrahina przybyły z Anomalii Kolapsu. (co pamięta Zespół)
    * Tosen nie ma pojęcia o odblokowanych Persefonach. 
* 4 statki Orbitera zniknęły. + 4-5 cywilne zniknęli.
* Opresor też zniknął. Opresor nie powinien być zniszczony przez AK. System anty-anomalne z silną Persefoną.
* Nieznane jednostki w AK. Nie znamy struktury tych jednostek - nieznane statki. Poruszają się płynnie po AK i nie atakują a uciekają.

Tosen spytał, czy skoro Infernia już tam leci, czy czego potrzebujecie? Arianna poprosiła o dziesięć Kirasjerów (roboty bojowe Tosena). Eustachy poprosił o własne drony + wzmocnienie obrony punktowej. Własne drony - "żywy pancerz" + samozniszczenie + lekkie działko. Tosen zapewni taki sprzęt. Po utracie Opresora, podejrzewa anomalie memetyczne.

Arianna wygłasza mowę do załogi - lecą w niebezpieczne miejsce, trzeba współpracować ze sobą. Nie mogą się powtórzyć sytuacje z ostatnich dni. TrZ+2: (XV: uda się, zaraportują Ariannie dziwne rzeczy, uwzględniają gravitas sytuacji, ale... nie są szczęśliwi. Nie mają tego wysokiego morale co czasami. Nie rozumieją po co tam lecą - "dowiedzieć się co się dzieje" mniej motywujące niż "ratujemy statek".)

Do Arianny przyszedł Roland Sowiński z 3 guwernantkami. Chce wejść na pokład. Arianna mówi NIE jego guwernantkom.

Konflikt Arianny, _sense motive_, +Eustachy jako zasób (rozprasza guwernantki). Eustachy podbija stawkę, zgrywa niedostępnego. Guwernantki reagują na Eustachego, Roland nic nie zauważa (w jego głowie nawet ta myśl się nie pojawia): ExZ+3

* V: Arianna widzi coś ciekawego. Roland chce lecieć, ale jedna z tych guwernantek ADAPTUJE do dwóch pozostałych. Jedna tu nie pasuje. Jest czymś innym niż zwykłą guwernantką.
* V: Arianna przekierowała "tą niewłaściwą guwernantkę" na Eustachego. Eustachy na nią wywiera presję.

Eustachy chce dramatycznie utrudnić jej działanie. Niech się odsłoni. Coś, co sprawi, że Klaudia może ją znaleźć / określić. Dowiedzieć się kim ona jest czy w jakiej jest organizacji. Eustachy charyzma na maksimum. ExZ+3+2o+1o. 

* O_love_x: laska jest zmieszana, zaskoczona, nie przywykła że ktoś się nią interesuje. Frazy, słowa, kadencja... wskazuje na Aurum, na arystokrację. Klaudia powiedziała, że to jest ARYSTOKRATKA W PRZEBRANIU. Ona jest przebudowana by być "tą guwernantką".

Eustachy podnosi stawkę - "rodzice z Aurum, poznaję po akcencie". Rozbić ją, wytrącić z równowagi.

* V_arianna : Po tym jak Arianna dołączyła do konwersacji wysyłając Rolanda na rozmowę z Kamilem. Niech Kamil opowie wszystko co najgorsze odnośnie AK i zniechęci kolesia. A Arianna pomogła Eustachemu w rozpracowaniu co to za dama - wyizolowaliście ją. I Arianna poznała pomniejszy ród - mamy do czynienia z tien Blakenbauer.

Kamil skutecznie przekonuje ekipę, że AKolapsu jest najgroźniejszą rzeczą ever.

* O_eustachy: Kamilowi udaje się ich przekonać. Roland i guwernantki (te dwie) PANICZNIE nie chcą lecieć na tą akcję. Elena to wszystko obserwuje, a dokładniej obserwuje flirt Eustachego z guwernantkami nie wiedząc że to na rozkaz Arianny. Obserwuje to zza ramienia Klaudii, która nawet tego nie zauważyła. A Eustachy eskaluje - "moje panie, jest tak bezpiecznie na warsztatach Inferni że jest OK. Czasem potrzebny jest remont..."

Jako, że guwernantka jest zdecentrowana, Eustachy powiedział "tien Blakenbauer, o co w tym wszystkim chodzi?". +3V

* XX: guwernantka jest przerażona, spoliczkowała Eustachego i odchodzi. Eustachy zatrzymuje jej rękę "na razie gramy grzecznie. Chyba nie chcesz żeby Twój przydupas dowiedział się kim jesteś. Nie zrobię ci krzywdy."

"Puść mnie, bo będę krzyczeć". "Ty chyba nie wiesz z czego jestem znany. Szlachcianki zwykle przy mnie krzyczą."

* Arianna do Rolanda - "proszę, zostaw nam jedną guwernantkę. Będziemy Cię mogli wziąć na następną misję i będziemy dobrze przygotowani. Dobrze dopasujemy."
* Roland, w odpowiedzi - oddał guwernantkę Ariannie XD.

Guwernantka trochę oklapła. Eustachy bierze guwernantkę na Infernię. Ona się odwraca i dostaje klapsa od Eustachego. 

* X: guwernantka UCIEKA. Wykręca się Eustachemu, szczupak, UCIEKA.

Eustachy puszcza Leonę za guwernantką. Nie przeszkadza mu, że będzie poobijana. Leona poluje!

* V: Leona złapała, upolowała i przyprowadziła.

ROLAND PATRZY ZE SZCZĘKĄ KU ZIEMI. Co tu się stało?

* Eustachy: "Spokojnie, panie ważny. Kobitki tak przy mnie mają. Odstawimy nieuszkodzoną." 
* Roland: "...", guwernantki PRZERAŻONE.

Izabela stoi i patrzy z blankiem na tą scenę. Nie umie z tym NIC zrobić w ramach "sekretów orbitera". To jest nie do uratowania. Zawieszona jak nigdy.

Arianna pokazowo opieprza Leonę. Próba rozpaczliwego uratowania sytuacji. Podbudowanie reputacji i zamaskowanie tej tragedii która się stała.

* V: to jest niesprawiedliwe, ale Ariannie się udało. Udało się Ariannie skutecznie przekonać, że sytuacja tego wymagała. Wyciągnęła PRZEDMIOT od guwernantki - że niby ona coś ważnego ukradła. I nagle to jest wszystko "akceptowalne". Roland bardzo, BARDZO przeprasza Ariannę.
* X: Roland publicznie i na miejscu zwolnił guwernantkę. Za wszystko co zrobiła. Powiedział Ariannie - tamta guwernantka jest JEJ. I niech weźmie sobie inną guwernantkę, Kasię.

Elena została wysłana do zajęcia się Niewinną Guwernantką. Niech wejdzie na statek, wyjdzie z niego i niech coś z tego co chciał będzie spełnione. Elena jako oficer społeczny Inferni... smutny widok.

DOBRA. Wracamy do naszej Blakenbauerki. Wróciła do przytomności. Jest w brygu Inferni. Eustachy chce wsadzić na nią obrożę z ładunkiem wybuchowym, ale Arianna jest przeciwna. Arianna NIE CHCE krzywdy sąsiada XD. Blakenbauerowie są... problematyczni.

Blakenbauerka siedzi w kącie i patrzy wrogo. Przychodzi do niej Klaudia. Konfunduje ją, po czym sobie idzie.

Klaudia próbuje z Eleną dojść do tego kim jest ta Blakenbauerka. Bo Elena sporo krzywd na Orbiterze robiła Blakenbauerom a ich sporo tu nie jest. Klaudia ma listę Blakenbauerów w ostatnich latach, Elena może powiedzieć co komu robiła. Plus identyfikacja tych Blakenbauerów. Kim jest ta Blakenbauerka?

(TrZ+2): 

* V: To jest Flawia Blakenbauer. Elena wpakowała ją w GIGANTYCZNE długi. Flawia musiała uciekać z Orbitera przed dłużnikami. Nie powinna była wracać.
* V: Sowińscy wykupili długi Flawii. Dawno temu. Czyli można powiedzieć, że Flawia w pewien sposób jest na usługi Sowińskich. Swego czasu Flawia zajmowała się Anastazją.
* XX: "Ta guwernantka" była z Rolandem od dawna. Flawia najpewniej jest z Rolandem od niedawna. Flawia podmieniła inną guwernantkę. ROLAND ZWOLNIŁ NIE TĄ LASKĘ XD.

Arianna przesłuchuje Flawię. Eustachy przesłuchuje z Arianną. Co Flawia chciała osiągnąć. 

Flawia powiedziała najpierw, że ona UCIEKAŁA, bo Arianna (Verlenka) chciała oddać ją Eustachemu. I Arianna by nie uciekała? Arianna pomyślała... może to by była lepsza opcja. Eustachy wyjaśnił jej, że Infernia jest dużo bardziej kompetentna niż się wydaje. I ona na to wpadła. I to wszystko była gra. Flawia jest zdruzgotana.

Eustachy wyjaśnia Flawii w jak złej jest sytuacji na K1. Nie zatrudnią jej nawet na żebranie pod kościołem. Jej jedyna opcja to dołączenie do Inferni. Może jej to nie pasować, ale ej. Arianna na to - jeśli Flawia z nimi zostanie, może Flawia mieć oko i na Sowińskiego i na Ariannę. Dodatkowo może odpracować swój czyn, nie ma konsekwencji...

TrZ+2, na wyciąganie prawdy:

* XX: Flawia zaczyna się podłamywać. NIE MOŻE. Wpadła w długi, zaczęła blefować i poszło na ostro. Sowiński kupił jej długi. Ona NIE MOŻE dołączyć do Inferni, bo Sowiński zmaterializuje jej długi i ród ma kłopoty. A jej długi ma patriarcha Anastazy. Dlatego m.in. Flawia była guwernantką Anastazji (nie dogadywały się).
* X: Flawia ma ogromną, ogromną, kosmiczną kosę z Eleną. Gdyby nie Elena, nic z tego nigdy by się nie stało.
* V: Klaudia zaproponowała rozwiązanie - niech Flawia "zinfiltruje" Infernię i "szpieguje" Ariannę. Robi raporty. Niech jej misja się zmieni - od tej pory Flawia jest przypisana do Inferni. I Infernia cośtam z Anastazją.

I w ten sposób na Infernię dołączyła tien Flawia Blakenbauer...

## Streszczenie

Marian Tosen poprosił Infernię o to, by sprawdzili co się dzieje w Anomalii Kolapsu - podejrzewa anomalie memetyczne, skoro Orbiter traci tam statki  (m.in. OO Opresor) i pojawiają się jednostki nieoznaczonej frakcji. Tymczasem Roland Sowiński chce na pokład. Arianna wybiła mu to z głowy, ale wykryli agentkę wśród jego guwernantek - Flawię Blakenbauer, która miała się nim opiekować i jest trochę zniewolona przez Sowińskich. By zadośćuczynić po tym co Elena zrobiła i trochę Flawii pomóc, Flawia dołączyła do Inferni. "Oficjalnie" udało jej się Infernię zinfiltrować...

## Progresja

* Elena Verlen: uważa, że Eustachy flirtuje z KAŻDYM. A zwłaszcza z Flawią >.>.
* Elena Verlen: straszne poczucie winy za to co zrobiła kiedyś Flawii. Ale nie jest w stanie nic z tym zrobić.
* Flawia Blakenbauer: oddelegowana przez Anastazego Sowińskiego na Infernię jako, że "skutecznie ją zinfiltrowała a Infernia szuka Anastazji".
* Flawia Blakenbauer: absolutna nienawiść do Eleny. Zemści się na niej. Zniszczy ją.
* OO Infernia: wzbogaciła się o 10 'Kirasjerów' (roboty bojowe Tosena), drony i point defense.

### Frakcji

* .

## Zasługi

* Arianna Verlen: przekonała Rolanda by nie leciał z nimi do Anomalii Kolapsu, wykryła tożsamość Flawii i dała jej szansę na dołączenie do Inferni. Mistrzyni odwracania uwagi i wrabiania innych że są winni.
* Eustachy Korkoran: dobry glina i zły glina jednocześnie - zdecentrował i ogłupił Flawię, po czym pokazał jej, że Infernia ma świetny system zabezpieczeń przed infiltracją. Szacun!
* Klaudia Stryk: odkryła tożsamość Flawii (ukrytej pod zmienioną formą) na podstawie wiedzy z dokumentów Orbitera i pamięci Eleny. Zaproponowała by Flawia dołączyła do Inferni.
* Elena Verlen: dawno, dawno temu zrobiła KATASTROFALNĄ krzywdę Flawii, wpakowała ją w długi i doprowadziła do jej dewastacji.
* Flawia Blakenbauer: Elena kiedyś wpakowała ją w GIGANTYCZNE długi. Sowińscy ją wykupili i ona jest teraz infiltrating agent ("Bond"). Teraz miała zajmować się Rolandem, ale Roland chciał na Infernię, więc ona "zinfiltrowała" Infernię. Próbowała się wybronić lub uciec, ale nie była w stanie. Finalnie - dołączyła do Inferni pod pretekstem "infiltracji i szukania Anastazji".
* Roland Sowiński: koniecznie chciał lecieć Infernią na SPECJALNĄ MISJĘ. Jak się okazało (Kamil <3) jak jest niebezpieczna, już nie chciał. Zwolnił guwernantkę (Flawię), bo Arianna zrobiła scenę że ta niby go okradła.
* Kamil Lyraczek: skutecznie przekonał Rolanda Sowińskiego (i jego guwernantki), że Anomalia Kolapsu to najstraszniejsze miejsce ever. Nie chcą tam lecieć :D.
* Leona Astrienko: Rola: "pies gończy". Wystrzeliła i dogoniła uciekającą przez hangary Flawię, złapała i unieszkodliwiła ją, po czym przyniosła z powrotem na Infernię :-).
* Izabela Zarantel: wróciła na stałe do załogi Inferni. Będzie pracować nad ciągiem dalszym Sekretów Orbitera.
* Marian Tosen: martwi się, że Infernia może zniknąć w Anomalii Kolapsu, więc przekazał jej trochę sprzętu i dobrych rad. Jest ekspertem od anomalii memetycznych (więc nie jego działka), ale poczciwy z niego mag.
* OO Opresor: bardzo potężny statek anty-anomaliczny z potężną Persefoną; zniknął w Anomalii Kolapsu i Marian Tosen się o niego strasznie martwi. Też się przez to martwi o Infernię.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy
                    1. Hangary Alicantis: gdzie doszło do próby infiltracji Inferni przez Flawię Blakenbauer jako guwernantka Rolanda Sowińskiego.

## Czas

* Opóźnienie: 3
* Dni: 5

## Inne

.

## Konflikty

* 1 - Arianna robi _sense motive_ przeciwko Rolandowi Sowińskiemu i jego guwernantkom. Coś tu jest nie tak
    * ExZ+3
    * VV: "niewłaściwa" guwernantka przekierowana na Eustachego - zajmij się nią.
* 2 - Eustachy ją decentruje, niech ona się odsłoni i pokaże kim jest i czego szuka
    * ExZ+3
    * O_love_x: zmieszana, zaskoczona... to ARYSTOKRATKA W PRZEBRANIU!
    * V_arianna: mamy do czynienia z Blakenbauerką. Arianna poznała manieryzmy a Eustachy dobrze robił presję.
* 3 - Kamil Lyraczek próbuje maksymalnie zniechęcić Rolanda do podróży do Anomalii Kolapsu (perswazja, opowieści)
    * ExZ+3, ciąg dalszy
    * O_eustachy: Elena widzi flirt i mega nie docenia. Roland i guwernantki PANICZNIE nie chcą lecieć do AKolapsu.
* 4 - Eustachy kontynuuje decentrowanie i rozbicie tien Blakenbauer
    * ExZ+3+3
    * XXX: guwernantka zachowuje się jakby on ją molestował. Jest przerażona. Po chwili - wyrywa się Eustachemu i ucieka.
* 5 - Leona w pościg, jak pies gończy. Przyprowadź mi guwernantkę.
    * Tr+3
    * V: sukces.
* 6 - Arianna próbuje uratować wizerunek (kłamstwo, deception) pokazowo opieprzając Leonę i że "guwernantka nas okradła!"
    * ExZ+3+3
    * VX: Sukces. Ale Roland natychmiast zwolnił guwernantkę.
* 7 - Klaudia z pomocą Eleny próbuje odkryć w dokumentach (biurokracja, kojarzenie faktów) kim jest ta guwernantka. Kto to jest.
    * TrZ+2
    * VVXX: Flawia Blakenbauer, ale ona ma inny morf. Czyli Roland zwolnił NIE TĄ guwernantkę. Na inną padło XD.
* 8 - Eustachy i Arianna naciskają Flawię by poznać prawdę (przesłuchiwanie, konfrontacja). 
    * TrZ+2
    * XXXV: Flawia ma straszne długi i kosę z Eleną. Nie może dołączyć do Inferni. Ale może ją "skutecznie zinfiltrować" dla Sowińskiego ;-)
