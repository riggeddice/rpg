## Metadane

* title: "Kapitan Verlen i pojedynek z marine"
* threads: kosmiczna-chwala-arianny
* motives: aurum-program-kosmiczny, rekonstrukcja-ruiny
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [220921 - Kapitan Verlen i Królowa Kosmicznej Chwały](220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)

### Chronologiczna

* [220921 - Kapitan Verlen i Królowa Kosmicznej Chwały](220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)

## Plan sesji
### Theme & Vision

* Królowa Kosmicznej Chwały, support ship

### Co się wydarzyło KIEDYŚ

* .

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

* .

### Co się stanie (what will happen)

* S01: pierwsze działanie w stylu ćwiczeń. FAILURE. Nawet jak się da strzelić, nie działa jak powinno.
* 
* S0N: gra w karty, przegrał (Jan Kirpin) cnotę przyjaciółki (Medea Szarprak) do marine Szymona Wanada.
    * Nie odważy się odmówić BO LEONA.
* S0N: statek nie jest w pełni sterowny a trzeba doprowadzić go do działania.
* S0N: Puryści Seilii chcą pozbyć się Leony i Irkana + Triaca. Leona ich chroni.
    * 3 ze statku 7 z Aurum, 2 osoby z Aurum to advancerzy (tien Szczepan Myrczek + tien Mariusz Bulterier)
    * tak, robią też sabotaż przeciw Leonie
* S0N: część załogi: nie lubimy Aurum. Nie będziemy się starać (bosman Tadeusz Ruppok). Resentymenty, czemu mamy pracować?
* S0N: przerażony Tomasz Dojnicz (marine) ma zatrzymać bójkę między ludźmi - "ludzie Aurum" vs "ludzie - załoga"
    * "Aurum forever"
* S0N: Klaudiusz Terienak: ta jednostka miała dla niego wartość BO WŁADAWIEC. Pozbyć się Arianny.
* S0N: Oficer łącznościowy (tien Maja Samszar) nie przekazuje Ariannie niczego
    * Inspekcja, Arianna nie jest gotowa
    * It is personal; Romeo kiedyś skrzywdził kuzynkę Mai.

.

* Pierwszy oficer - Władawiec Diakon - podrywa na lewo i prawo bawiąc się hormonami.
    * W medycznym jest tien Klaudiusz Terienak który chce zdobyć uznanie Władawca, ale boi się Leony.
* Aurum podzieliło się na frakcje i próbują wygrywać między sobą.
* Leona Astrienko jest tu na wakacjach - ale też ma uniemożliwić śmierć kogokolwiek czy jednostki.
    * 2 sarderytów, Irkan i Triac nie dają się jej ale też w sumie nikomu skrzywdzić.
* Leona + 5 marines opanowuje tą 40-osobową jednostkę.
* STRASZNA nieufność Orbitera do Aurum
* Klarysa Jirnik, artylerzystka i odpowiedzialna za broń
* tien Grażyna Burgacz, logistyka i sprzęt
* tien Arnulf Perikas, fabrykacja i produkcja - on jest bardzo za Arianną i najbardziej rozczarowany Alezją

### Sukces graczy (when you win)

* .

## Sesja właściwa
### Wydarzenia przedsesjowe

.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Na Królowej rzeczy są przebudowywane. Królowa dostała prośbę ze strony innej jednostki, by ta się przesunęła. Maja pyta, czy ma odpowiedzieć to co do tej pory? "Że jest awaria silników?". Maja ma powiedzieć, że przesuną się TERAZ. Aktywnie nie sabotuje Arianny...

Pilot Stefan patrzy na Ariannę niepewnie. "Pani kapitan... a zadziała?" "Powinno - jesteś pilotem. Wiesz kto najczęściej przy konsoli". "To znaczy, POWINNO działać... jeśli systemy nie zrobią czegoś dziwnego."

Daria robi diagnostykę. Czy da się przesunąć statek. Na szczęście w pobliżu nic nie ma - inne statki UNIKAJĄ Królowej. Daria chce LEKKO rozpędzić statek, taki dryf. Minimalne przeciążenia.

TrZ+3:

* V: Statek da się przesunąć. Trzeba wejść w bebechy i zrobić to "spinając druciki". 
* X: Maja Samszar "dostałam request że nie przesunęliśmy się i czy coś się stało."
* Xz: Królowa włączyła autopilot. SILNIK. Zaczyna iść na pełną moc. Pilot próbuje przejąć kontrolę - "abort" -> konfetti.
* X: (systemy do manewrowania na krótkim dystansie -> silniki manewrowe) -> Daria włącza to. Odcinamy silnik i na krótko spinamy. (+1Vg). Daria przywykła do pracy z NORMALNYM systemem. Ale tu ktoś się bawił. Praca na niestandardowej jednostce. Ktoś sprzedał autopilota. TAI jest przyduszona, uśpiona. Królowa leci na pełnej mocy prosto na inną jednostkę. Maja Samszar "kapitan Verlen? Pytają o co chodzi...". Maja ma ostrzec.
* Vz: Daria przejęła kontrolę nad silnikami, manualną. Część podsystemów przestała działać. Daria ma paskudne podejrzenie - Królowa jako jednostka nie powinna tak się zachowywać. To nie tylko kwestia inżyniera. CAŁA JEDNOSTKA jest starsza i w gorszym stanie niż wynika ze specyfikacji.

Arianna próbuje wyprowadzić tą jednostkę by kręciła kółeczka i nie było hańby.

Tr Z (Daria manualnie w silniki) +3:

* VV: Arianna kręci kółeczka ale już na osobnej orbicie. Przesunęła się. A Daria wyłączyła silniki i statek zatrzymał się.

Daria jest zaskoczona stanem jednostki. Arianna też. Maja Samszar "komunikacja ze strony Jasnego Światła." Koleś wyraźnie nie chciał wprowadzić ARIANNĘ w kłopoty, ale wprowadziłby z przyjemnością ALEZJĘ. Wiedział jak to się skończy.

.

Alezja -> Arianna: "kapitan Kurzmin." smutny uśmiech "byliśmy razem w akademii. Byłam od niego ZAWSZE lepsza. Rywalizowaliśmy. Ja wygrałam Królową... od padł na Jasne Światło /smutny śmiech". Królowa nie zachowywała się tak wcześniej, nie aż tak. To stało się niestety pod moimi rządami. Wiele systemów nie działało. Semla w ogóle nie rozpoznawała swojej jednostki, kazałam ją uśpić.

Kurzmin -> Arianna, na priv: "kapitan Verlen, czy potrzebujesz pomocy?". "Na razie trudno stwierdzić czego nie potrzebuję, prawdzie mówiąc.". "Czy możesz mi tylko powiedzieć co zrobiłaś z Alezją?" "Jest moim drugim oficerem - jako wsparcie by wyjaśniła mi co się dzieje." "Czy... Alezja... bardzo zawaliła? Kojarzę ją jako dumną i...solidną. To nie była zdrowa rywalizacja ale była uczciwa. Tu się stało coś dziwnego. Nie lubię Alezji. Ale nie wierzę, że tak spieprzyła. Proszę Cię o sprawiedliwość dla niej mimo że ona NIGDY nie poprosiłaby o sprawiedliwość dla mnie. (uśmiech). Wiem co oznacza ród Verlen."

Arianna dostaje wiadomość od kaprala Psa. "kapitan Verlen, czy naprawdę zasady Aurum są wskazane na tej jednostce?" "Nie, jesteśmy na mojej jednostce". "W sektorze 11, w mesie przeprowadzana jest kara fizyczna. Znaczy chłosta." "Bez mojej wiedzy?" "Weź 2 marines, idziemy tam".

Arianna w towarzystwie 2 marines: Szymon Wanad, Tomasz Dojnicz. I mesa, pięciu marynarzy, klęczą, a tien Arnulf Perikas ma nahajkę i wali po plecach jednego. 

Arnulf: "Z braku AI ludzie pełnią funkcję a oni zawiedli". Dlatego tylko trzy uderzenia i to nie takie mocne. Chodzi o upokorzenie a nie zadanie cierpienia. Arianna: "człowiek by zrozumiał błąd musi opracować. Oddelegujemy pana Dojnicza by wyczyścili kible na pokładzie." Arnulf "tak jest. Rozumiem, że wszystkie kary fizyczne mam anulować? Wszyscy oficerowie mają anulować?"

Arianna ma mały WTF. Czyli trzeba zakazać im tego robić XD. Dosłownie... Arianna 30-60 min przygotować się jak nieprzydatne są kary fizyczne. Ci oficerowie nie są tu przypadkowo. To nie są najlepsi oficerowie Aurum na Orbiterze, ale nie są też najgorsi. To konkretna populacja, taka, która ma zawalić.

Arianna ogłosiła brak kar fizycznych. Nikt się nie zmartwił. Wszyscy się zdziwili (wtf), ale niech będzie - Arianna niech ma. Choć Grażyna "ale serio? Pani kapitan, oni są miękcy." Arianna "niech zahartują się przez ciężką pracę, upokorzenie nie jest budujące - tylko wysyła na urlop". Grażyna wzruszyła ramionami. Niech jest jak Arianna chce.

Do Arianny przyszła Grażyna potem. "pani kapitan ale... proszę spojrzeć na parametry". Arianna widzi jedną rzecz - jednostka ma nisko współpracującą załogę. Ludzie są słabi i niemrawi.

Arianna za "kapitańskie" organizuje ekspres do kawy i najlepsi mają dostać ekspres do swojego działu. Próba zbudowania zaufania w załodze a nie tylko wśród oficerów. To odbędzie się za 2 dni.

Klaudiusz Terienak przedłożył raport Ariannie - 6 rannych. Pobitych solidnie. Jeszcze za czasów Alezji.

SKĄD? Chcieli pobić Sarderytów i weszła Leona...

Jeden z inżynierów ma prośbę do Darii. "pani porucznik, przeniesienie?". Daria pyta co się stało. GRAŁ W KARTY. Przegrałem do Wanada. Nie chcę dać mu tego co wygrał. "Pani porucznik, ALBO dostanę przeniesienie ALBO coś mi się stanie w ciasnych zakamarkach jednostki." "PANI PORUCZNIK JA PIŁEM! Przegrałem cnotę przyjaciółki. PAL DIABLI WANAD, LEONA ASTRIENKO MNIE ZABIJE!"

Daria: "i po to jest Arianna :D".

Z tej opowieści wynika że to INŻYNIER zaproponował Medeę. Nie Wanad. Inżynier jest załamany - nie wie co mu przyszło do głowy. "Tak, karty, alkohol to norma". Alkohol wprowadza bosman Tomasz Ruppok. Ruppok nie jest z Aurum. 

Załoga podzielona jest częściowo na ludzi pochodzenia Aurum, częściowo nie Aurum. Ruppok jest zdecydowanym nie-Aurumistą, protestował przeciwko karom fizycznym i słyszał "statek Aurum, działamy jak Aurum". Ruppok jest z K1 ale nie z Orbitera - plebsem K1. Nie-Aurum: Valentina, K1, próżniowcy.

Arianna wzywa do siebie Szymona Wanada. Zabójca przyszedł. To jest koleś który jak dostanie rozkaz to wrzuci dziecko w ogień... albo to zrobi jak nikt nie patrzy. Wanad tak patrzy ze spokojem. Czeka na opierdol, nie wie za co.

"Może i bym chciała Cię opierdolić ale nie mam powodu" - zdziwienie Wanada - "jestem zawiedziona pierwszym spotkaniem, mieliśmy nierówne spotkanie i pojedynek nam nie wyszedł." Wanad się uśmiechnął "nie sądzę że pani kapitan chce". Arianna "z innych stron pochodzę".

Ustalili pojedynek do utraty przytomności lub poddania się.

SALA TRENINGOWA. Broń do walki wręcz - specjalizacja Verlenów. Oboje mają ostrą broń, on ma taką podpiłowaną. Jest solidny, ma mnóstwo blizn. Arianna ma nadzieję na refleks. Leona jako sędzia - ma pancerz osobisty. Lokalizacja - elastyczny ring, nierówny teren.

Arianna prowokuje, by myślał, że jest sytuacja która daje mu zdecydowaną przewagę - żeby się wybił i zaszarżował na Ariannę. Arianna krótkim mieczykiem popisuje się jak arystokratka mieczykiem. Styl walki 'cute aristo girl', książkowy. Szymon się oblizał. (obniżenie klasy trudności). Arianna odsłania konkretne miejsce by wiedzieć gdzie zaatakuje.

Tr Z (pozory i wrażenie) +4:

* Koleś RYKNĄŁ lecąc na Ariannę i skupił się na tym by ją poszarpać. Xz: Arianna chciała się odsunąć by mu z bara. On jest za szybki. Zrobił zwód i odciął Ci część włosów. Jako trofeum.
* X: Tym razem cięcie poszło po lewej nodze i ręce. Spowolnił Cię acz nie tak jak arystokratkę. Chciał KREW i KRZYWDĘ. Arianna chce zimne podejście. I między nogami w śródstopie. Bronią.
* V: Cios Arianny go totalnie zaskoczył i zrobił odruchowy unik ale się przewrócił. Na plecy. (+2Vg).
* V: Arianna skacze na niego, on robi atak bronią -> ale Arianna jest skuteczna. Przebija mu rękę z bronią. On upuszcza broń. Arianna wycofuje broń, ale KOLEŚ NIE MA PRAWEJ RĘKI. (+1Vg)
* V: Arianna wyciągnęła broń z rannej ręki i natychmiast obróciła ją przeciw zdrowej ręce. On chciał złapać Ariannę i użyć swojej masy ale jego ręka napotkała na ostrze Arianny. TERAZ wrzasnął. Ma dwie niesprawne ręce.

Poddał się. Arianna pomogła mu wstać ciągnąc za koszulę. Szymon jest wściekły, dyszy, ale jest szacunek. Arianna oswoiła go. Odcięła sobie kawałek włosów "włóż sobie pod poduszkę byś pamiętał" i wsadziła mu do kieszeni na piersi. ARIANNA GO OSWOIŁA. Pet killer.

Gdy Arianna przeszła koło Leony, ta powiedziała jej cicho "jedz zapieczętowane konserwy i użyj zapieczętowanych leków". Uśmiech "może coś z Ciebie będzie".

Arianna BARDZO poważnie potraktowała radę Leony. "Słyszałam, że trzymają się Ciebie dobre żarty, ale nie że aż takie". Leona: "Alezja też tak myślała."

Arianna, w głowie: Są dokładnie cztery kobiety na pokładzie nie kochające Władawca - Daria, ja, Leona i Grażyna (która nie spełnia klasycznych kanonów piękna)...

(a kto za wszystkim stoi? Siły specjalnie ORbitera uniemożliwiające program kosmiczny Aurum ^_^)

## Streszczenie

Królowa miała się przesunąć, ale prawie się rozbiła; ta jednostka jest rozkradziona. Arianna zatrzymała biczowanie załogantów i doszła do tego, że załoga jest niemrawa i skonfliktowana bo część ludzi jest z Aurum a część nie. Perfect storm of suck. By zatrzymać Wanada przed "wygraniem cnoty koleżanki inżyniera" stoczyła z nim pojedynek i wygrała. Leona ostrzegła, by Arianna nic nie jadła i nie piła. Czyli coś jest w jedzeniu. To tłumaczy czemu Królowa jest tak zdegenerowaną jednostką. Aha, Arianna ma wsparcie kapitana Leszka Kurzmina, dawnego rywala Alezji.

## Progresja

* Szymon Wanad: Arianna go pokonała i zrobiła power move. Zdecydował, że jest większym drapieżnikiem od niego i do niej dołączył.

### Frakcji

* .

## Zasługi

* Arianna Verlen: zatrzymała praktykę biczowania załogi (wprowadzoną przez Aurum) i stoczyła pojedynek z Szymonem Wanadem, psychopatycznym marine. Pokonała go i oswoiła. Przestała jeść w kantynie XD.
* Daria Czarnewik: uruchomiła silniki Królowej i Królowa wyszła poza kontrolę; z trudem odzyskała kontrolę nad jednostką. CO TU SIĘ DZIEJE?!
* Stefan Torkil: pilot Królowej Kosmicznej Chwały; robi co może. Nie pochodzi z Aurum.
* Maja Samszar: koordynowała sygnały między Arianną a Leszkiem Kurzminem. Aktywnie Arianny nie sabotowała, ale nie pomagała.
* Alezja Dumorin: zna się z Leszkiem Kurzminem, rywalizowali w Akademii. Była świetna. Była wredna dla Leszka.
* Leszek Kurzmin: chce pomóc Ariannie (nawet dyskretnie) by ratować Królową Kosmicznej Chwały i Alezję. Rywalizował z Alezją w Akademii, nie wierzy jak bardzo upadła.
* Erwin Pies: dyskretnie przekazał Ariannie, że Arnulf Perikas biczuje ludzi na pokładzie jednostki - wprowadza zasady Aurum. Ogólnie, Arianna ufa mu najbardziej a on jej najbardziej pomaga.
* Arnulf Perikas: wprowadza zasady Aurum na Królowej Kosmicznej Chwały - biczuje ludzi bo są niekompetentni z braku TAI. Jak tylko Arianna powiedziała że mu nie wolno, przestał.
* Grażyna Burgacz: wskazała Ariannie, że na Królowej załoga jest miękka i niewiele robi. Są konflikty w załodze.
* Szymon Wanad: wygrał dziewczynę w 'Trzy Diamenty' (by tormentować jej chłopaka, inżyniera) a potem stoczył pojedynek z Arianną. Mimo że ją zranił, pokonała go okrucieństwem i podstępem.
* Leona Astrienko: sędzia pojedynku Arianna - Szymon. Uznała, że coś z Arianny będzie i ostrzegła ją by ta nic nie jadła i nie piła - tylko konserwy. Alezji też kiedyś powiedziała.
* Tomasz Ruppok: starszy mat; próżniowiec z K1 na Królowej, który specjalizuje się w produkcji alkoholu i hazardzie na Królowej.
* Władawiec Diakon: okazuje się, że na pokładzie są cztery osoby które go nie kochają - Daria, Arianna (nowe), Leona (scaaary) i Grażyna (nie spełnia klasycznych kanonów). He did stuff to water/food...
* OO Królowa Kosmicznej Chwały: część sprzętu rozsprzedana, ledwo sprawna TAI Semla (w trybie uśpionym), załoga jest w konflikcie: Aurum - nie-Aurum i niekoniecznie chcą współpracować.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 2
* Dni: 2

## Konflikty

.

* 1 - Daria robi diagnostykę. Czy da się przesunąć statek. Na szczęście w pobliżu nic nie ma - inne statki UNIKAJĄ Królowej. Daria chce LEKKO rozpędzić statek, taki dryf. Minimalne przeciążenia.
    * TrZ+3
    * VXXzXVz: Statek jest przesuwalny i Daria steruje ręcznie; Królowa ma autopilot i prawie uszkodziła inną jednostkę. Ktoś nieźle się bawił - sprzedali autopilota? XD
* 2 - Arianna próbuje wyprowadzić tą jednostkę by kręciła kółeczka i nie było hańby.
    * Tr Z (Daria manualnie w silniki) +3
    * VV: Skuteczne, acz kręci kółeczka.
* 3 - Arianna walczy z marine jako 'cute aristo girl', książkowy styl (by odwrócić uwagę). Szymon się oblizał. (obniżenie klasy trudności). Arianna odsłania konkretne miejsce by wiedzieć gdzie zaatakuje.
    * Tr Z (pozory i wrażenie) +4
    * XzXVVV: Koleś ryknął na Ariannę i odciął jej trochę włosów jako trofeum i zranił do krwi, wylizał z mieczyka. Arianna go przewróciła, przebiła ręce i i unieszkodliwiła. Her pet killer marine now.
* 4 - 
    * 
    * 
* 5 - 
    * 
    * 
* 6 - 
    * 
    * 
* 7 - 
    * 
    * 
* 8 - 
    * 
    * 
* 9 - 
    * 
    * 
* 10 -
    * 
    * 
