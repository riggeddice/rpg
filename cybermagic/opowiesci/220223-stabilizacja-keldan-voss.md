## Metadane

* title: "Stabilizacja Keldan Voss"
* threads: legenda-arianny
* gm: żółw
* players: fox, kapsel, kić

## Kontynuacja
### Kampanijna

* [220216 - Polityka rujnuje Pallidę Voss](220216-polityka-rujnuje-pallide-voss)

### Chronologiczna

* [220216 - Polityka rujnuje Pallidę Voss](220216-polityka-rujnuje-pallide-voss)

## Plan sesji
### Co się wydarzyło

Faza 1:

* Iorus. Pierścień Halo. Znajduje się tam niewielka rozproszona kolonia Keldan Voss.
* Keldan Voss zbankrutowali. Zostali kupieni przez Pallidę.
* Pallida sprowadziła swoich ludzi, którzy działają nad stację należącą do Keldan Voss.
    * Drakolici z Keldan Voss mają dość nieprzyjemne zasady. Nie chcą redukować poziomu energii.
        * ewolucja przez magię
    * Pallidanie chcą zdobywać i harvestować kryształy Vitium.
    * Mała stacja, z rozproszonymi stacjami dodatkowymi. Wszędzie są generatory Memoriam.
* Faith Healer Keldan Voss nazywa się Kormonow Voss.
* Z uwagi na wysoki poziom magii, stare mechanizmy itp. mamy promieniowanie - lokalni drakolici i sprowadzeni pallidanie chorują i gasną
* Z uwagi na to, że drakolici nie chcą się integrować i współpracować, wszyscy mają obowiązkowe transpondery / "komórki".
* Drakolici mają swoich "Kroczących We Mgle", którzy mogą działać bez skafandrów i we mgle.

Faza 2:

* Mgła. Część Kroczących jest Skażona i jest częścią Mgły. Część działa bez problemu.
* Pallidanie obwiniają drakolitów o Mgłę i o śmierci spowodowane przez Mgłę.
* Mgła atakuje bazę.
* Dwóch agentów naprawiających generatory Nierzeczywistości zostało Skażonych przez Mgłę.
* Szczepan chce, by Annice się nie udało. On lepiej będzie tym dowodził.
    * 4 członków delegacji -> na orbicie
    * wysłany zespół zabójczy w Mgłę; zabił już 7 drakolitów
* Kormonow chce się pozbyć pallidan. Mgła - nienawiść - go już opętała.
    * Drakolici wiedzą, że Kormonow wysadził generatory by móc ratować umierających drakolitów i palladian.
    * 11 pallidan (sprowadzonych) jest przez Kormonowa "naprawionych". -> 7 zabrała Mgła
    * 12 pallidan "porwanych" -> 5 zabrała Mgła, 3 uratowanych przez drakolitów
* Gdzie są drakolici? => niewielkie generatory Nierzeczywistości, znajduje się baza zasilana Vitium w księżycu.

Faza 3:

* Historia
    * Było tu starcie noktian i drakolitów z Neikatis. Drakolici wzięli się tu stąd, że na Neikatis wojna faeril - drakolici. Tutejsi uciekli w to miejsce. I wtedy napatoczyli im się noktianie. I noktianie stąd uciekli - pokonani przez drakolitów. I tutejsi drakolici to po prostu "chów wsobny" a częściowo "wolni ludzie". Część noktian tu wróciła i zostali przyjęci. I ci ludzie stanowią aktualny kult. 
    * Gdzieś po drodze zwrócili się do Saitaera. I on odpowiedział. Małe dary - utrzymać ludzi przy życiu. I osobą, która zwróciła się do Saitaera był ich naczelny naukowiec, badacz. Ten koleś znalazł faktycznych kultystów Saitaera. To jest przodek naszego szamana. Kormonow Voss jest potomkiem badacza, który "oddał" tą bazę Saitaerowi. 
    * pallidanie są z Neikatis. Oni są bliżej faeril.
* Mateus chce fuzję korporacji i sił Saitaera po rozmowie z Arianną
* Kryształy Vitium PAMIĘTAJĄ bitwę noktian i drakolitów. Jakaś forma vitium jest potrzebna.
* Na orbicie jest mag - Barnaba Lessert. To on jest celem Bii. Bia (klasa: Reitel) chce go pozyskać.
* Szaman, Kormonow Voss, chce by pallidanie przestali ich zabijać - Mgła i wysokie stężenie kryształów Vitium jest niezbędne do życia drakolitów.
    * On jest już homo superior. On i wielu innych drakolitów. A np. Mateus jest "brzydki" by odwrócić uwagę od Perfekcyjnych
* Mgła ma własności korupcyjne.
* Elena
    * Esuriit daje jej nieskończony głód. Obsesja na punkcie poprzedniej formy i poprzedniego życia.
    * Ixion daje jej adaptację. Jej ciało jest morficzne. Korozja i korupcja.

Faza 4:

* Uratowani, znajdują się na Keldan Voss - ale są oblężeni przez Mgłę i anomalie mgieł
    * Chcą, by Sebastian i Barnaba odpowiedzieli za swoje zbrodnie - chcą ich oddać Mgle
* Pojawia się Żelazko z Olgierdem i Marią na pokładzie
    * jest tam też Janus Krzak; on ma pomysł, uważa, że da się jakoś rozmontować tą Anomalię.
    * Olgierd chce doprowadzić do pokoju między tym wszystkim - nie da się doprowadzić tej bazy do działania bez wszystkich sił
        * Elena chce uratować wszystkich. Uważa, że jest w stanie. Olgierd chce Elenę zablokować.
* Pojawia się ratunkowy statek Pallidan - "Światło Nadziei"
    * Chcą odzyskać i zdezanomalizować Pallida Voss.
    * Chcą uratować swoich, też Annikę. Annika jest obwiniona za wszystko.

### Co się stanie

* ?

### Sukces graczy

* Baza przetrwa atak Mgły
* Nie dojdzie do eskalacji
* Elena...
    * Korozja detekcji i umysłu, mutacja ciała. Obsesja piękna.

## Sesja właściwa
### Scena Zero - impl

* .

### Scena Właściwa - impl

Na pokładzie Inferni jest Barnaba. Eustachy musiał wylądować na Keldan Voss i zamontować kilka generatorów Memoriam na szybko. Annika próbuje trzymać wysoko morale, ale nie wychodzi jej to. Sama płacze w poduszkę jak myśli, że nikt nie widzi. Mamy anomaliczny statek na orbicie. Szczepan (zdrajca Anniki) - jest na Kastorze (pod pretekstem "potrzebujemy go tutaj").

Na to wszystko pojawiły się dwie nowe jednostki - jedna to Orbiterowe Żelazko. Olgierd przyleciał wspomóc Ariannę. Druga jednostka to "Światło Nadziei". Statek ratunkowy / puryfikujący Pallidan. Cywilny ale uzbrojony. Tomasz Kaltaben jest kapitanem.

Arianna przesłuchuje Szczepana. Ma wszystkie fakty, logi, wszystko z komputerów (od Klaudii). Arianna mówi Szczepanowi że wie wszystko i chce "prawdy". Jest bohaterką Orbitera. Może mu pomóc - jeśli zechce. Jest po tej samej stronie co Szczepan, w końcu.

TpZ (dane Klaudii) +3:

* Vz: Szczepan przyznał się do konspirowania przeciwko Annice. 
    * Potwierdził, że Tomasz jest jego bratem. Mają dobre relacje. Tomasz nie jest świadomy planu Szczepana, ale też nie znosi drakolitów.
* Xz: Wieść, że Arianna "stanęła po stronie" Szczepana dotrze do Tomasza i do drakolitów.
* V: Szczepan przyznaje, że ogólnie wśród Pallidan jest konflikt na linii Pallidanie - drakolici. Że korporacja jest po prostu niespójna w wartościach i zasadach.
* V: Szczepan będzie współpracował z Arianną. Widzi ją jako jedyną nadzieję. Wierzy, że Arianna jest jego jedyną nadzieją.

Arianna chce, by Szczepan był jej agentem przy Annice. Ma pomóc Annice, ona mu ufa. By to wszystko załagodzić. Szczepan DALEJ wierzy, że Arianna wykorzysta to wszystko do zniszczenia drakolitów.

Rozmowa Arianny, Eustachego, Klaudii:

* Potrzebujemy komory do stabilizacji Eleny.
    * wcześniejsza osobowość, wcześniejsza pamięć, to, kim Elena była PRZEDTEM.
    * krew Arianny jako arcymaga
    * "feromony Eustachego" by częściowo ją karmić
    * odpowiednie balansowanie energii magicznej

Na planetoidzie. Eustachy stawia te cholerne generatory. Nie na poziomie wszystkiego - chodzi o osłonięcie tylko głównego kompleksu. To działa. Elena "zbliżają się do Was. Ufortyfikuj pozycję".

Eustachy ustawia generator - 40% zasięgu generatora, 30%. Jak się wycofamy do wewnętrznego to generator dostanie pełną moc by odpychał mgłę na pełnym zasięgu. I my do kontrataku. Eustachy ma bardzo zaszumiony sygnał. Coś próbuje się przebić. Eustachy nagrywa, wysyła do Klaudii.

TrZ+2:

* X: połączenie jest niemożliwe do nawiązania z Kastora.
* Xz: szepty, sygnały itp. są dewastujące dla morale wszystkich nie-Orbiterowców. To są już nie tylko dźwięki wojny. To też osoby porwane przez Mgłę. Stanowią Mgłę.
* Vz: "Mateus... wsparcie humanitarne... lekarze i żywność... wysłaliśmy..."

Klaudia przesyła sygnał PRZEZ Elenę i jej Eidolona. Korzystając z okazji, że Elena ma omnidetekcję. Z nadzieją, że Elena dojdzie do siebie + wyczyści sygnał. 

* X: Elena złapała PEŁEN sygnał. Pełne spektrum sygnału. Elena ma nową misję. "Ta Mgła jest sterowana".
* V: Elena utrzymuje kontrolę. Zczytała sygnał. I mamy przekaźnik do komunikacji z Mateusem.

Elena chce iść zniszczyć BIĘ. Póki ona żyje (BIA), póty Mgły są kontrolowane, póty przeszłość łączy się z przyszłością i póty wszystko staje się jednością. ALE - póki BIA tu jest to drakolici mogą tu żyć w stabilnym ekosystemie. Ta BIA dała im te możliwości. Test Mgieł itp. Więc można zniszczyć BIĘ, ale to zmieni kulturę bardzo mocno.

Eustachy fortyfikuje wejścia poza jednym. Wszystkie. Otwiera, zamyka, śluzy, masakruje, aż się nie połapią "ci z przeszłości".

TrZ+3:

* V: eksterminowane pierwsze fale.
* V: poprawione morale lokalnych ludzi. I drakolici co przyszli pomóc są "wow, jemu nie podpadać".

Annika, zainspirowana przez Eustachego, próbuje zainspirować ludzi. Eustachy próbuje ją od tego odwieść. Nie wyszło jej. "Kiedy znajdujesz dobre rozwiązanie z gówna i patyków ale ktoś oszczędza na patykach a inwestor próbuje oszczędzić na gównie."

Tr (niepełny) Z (Eustachy, Orbiter) +2:

* X: Część osób ogólnie jest przekonana, że z Anniki nigdy nic nie będzie. Jak ona dowodzi to są zgubieni.
* Vz: Annika doprowadziła do tego, że FAKTYCZNIE morale jest wyżej (częściowo dzięki Orbiterowi i Eustachemu) ale ONI WIDZĄ ŻE JAK WSPÓŁPRACUJĄ TO IDZIE LEPIEJ. Bo na pewno to nie ONA stoi za tym co jest złe.

Elena chce zniszczyć BIĘ. Arianna "nie możemy teraz". Elena ze wściekłością rozsiekała kamienie na kawałki... po czym zaakceptowała decyzję Arianny. Słucha się rozkazów. NADAL jest z nimi.

Jakie mamy siły:

* Drakolici, ugodowa (Mateus): chce współpracy z Pallidanami, chce ich przejąć w imię Saitaera
* Drakolici, radykalni (szaman): chce... cholera wie czego, ale to jest radykalne. Korupcja w imię Saitaera?
* BIA: chce powrotu do przeszłości. Chyba. Reprezentuje energie magiczne. Odpychana generatorami Memoriam, ale się wzmocniła
* Pallidanie, lokalsi: oni chcą wrócić do domu i zapomnieć. Ewentualnie, pokój z drakolitami 
* Pallidanie, orbita: chcą mieć SUKCES, uratować jak najwięcej ludzi się da i odzyskać Szczepana
* Pallidanie, antydrakoliccy: oni chcą utopić Annikę i doprowadzić do zniszczenia projektu Keldan Voss
* Orbiter: chce rozwiązać problem z małą kolonią w której coś się spieprzyło

Zatem ruch Zespołu - sytuacja jest stabilna. Podmieniamy software BIA. Czyli - niech BIA dostanie lobotomię od Eleny i na to miejsce wsadzi się pokorne i poczciwe TAI noszone przez Klaudię w plecaku. Jakieś pomniejsze TAI, coś niższego niż Elainka. Cera. Niech to coś PRZEZ CHWILĘ pełni rolę BII. A potem się zainstaluje coś, co ma sens.

Elena infiltruje w kierunku na BIA.

TrZ (homo superior) +4+5O:

* O: obsesja Eleny. Będzie piękna.
* X: Szaman. Zobaczył Elenę. Zobaczył piękno. Powiedział jej, że to nie zadziała - nie zadziała rozsiekanie.
* V: Elena odparła. ZRANIŁA go i odepchnęła. Ale nie zasymilowała.
* X: Elena wbiła się w środek BII. Nie udała się transplantacja. TAI zaskwierczała i wymarła - niekompatybilność hardware.
* O: Elena spojrzała na szamana. Zintegrowała go z BIĄ. Szamanowi udało się przejąć kontrolę nad bazą. BIA wróciła do działania. Ale Mgła jest lepiej kontrolowana. Zaczyna się wycofywać.

Elena wraca do bazy, w formie "ludzkiej", lekko z pochyloną głową. Elena jest zmartwiona, nie o takie zwycięstwo walczyliśmy, ale... cóż.

## Streszczenie

Ewakuacja pallidan na Keldan Voss się udało, acz Eustachy musiał zniszczyć anomalnych napastników z Mgły i naprawić morale Anniki. Elenie (słyszącej szepty) udało się zniszczyć BIA i wprowadzić na jej miejsce amalgamat szamana. Arianna ma polityczną kontrolę nad sytuacją a dzięki Eustachemu udało się sytuację ustabilizować militarnie. Dzięki Klaudii wiedzą co i jak. Teraz już tylko zostaje zostawić Keldan Voss w stabilnej formie.

## Progresja

* Annika Pradis: ogólna opinia keldanitów i pallidan z Keldan Voss - nic z niej nie będzie. Kiepski dowódca. Naiwna, dobre serce, ale niewiele zdziała.
* Elena Verlen: obsesja na punkcie swojej urody; słyszy szepty Keldan Voss. Szepty Saitaera? Szepty Mgieł? Nawet ona nie wie.

### Frakcji

* .

## Zasługi

* Arianna Verlen: politycznie zapewnia, by uciekinierzy z Pallidy Voss mogli znaleźć tymczasowe schronienie w Keldan Voss. Przesłuchała i wkręciła Szczepana. 
* Eustachy Korkoran: na szybko montuje w Keldan Voss generatory Memoriam, by móc ewakuować nadmiar pallidan z Kastora. Po ostrzeżeniu Eleny zrobił killzone i ochronił pallidan podnosząc im morale.
* Klaudia Stryk: wykorzystała Elenę jako filtr na sygnały z mgieł, dzięki czemu uratowała sporo istnień i zrozumiała co się tu dzieje. 
* Elena Verlen: wykryła atak Mgły na Eustachego; Klaudia przez nią (omni) przepuściła sygnał dla wyczyszczenia. Usłyszała WSZYSTKO. Szepty Saitaera. Zinfiltrowała BIA, nie wyszło jej wprowadzenie BIA, ale zintegrowała szamana z BIA. Coraz bardziej ma cechy terrorforma a nie maga.
* Szczepan Kaltaben: powiedział Ariannie WSZYSTKO, oddał się w jej ręce wierząc, że to i tylko to go uratuje.
* Tomasz Kaltaben: kapitan Światła Nadziei; pallidańskiego statku mającego uratować co się da z kolonii Keldan Voss.
* Annika Pradis: próbuje trzymać wszystkim morale, ale nie wychodzi. Złamane morale. Po killzone Eustachego podniosła morale ludzi... ale kosztem swojej reputacji.
* SP Światło Nadziei: statek ratunkowy / puryfikujący Pallidan. Cywilny ale uzbrojony. Tomasz Kaltaben jest kapitanem. Przybył do Keldan Voss pomóc rozwiązać sprawę.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Iorus
                1. Iorus, pierścień
                    1. Keldan Voss
 
## Czas

* Opóźnienie: 1
* Dni: 3

## Konflikty

* 1 - Arianna przesłuchuje Szczepana. Ma wszystkie fakty, logi, wszystko z komputerów (od Klaudii). Arianna mówi Szczepanowi że wie wszystko i chce "prawdy".
    * TpZ (dane Klaudii) +3:
    * VzXzVV: Wieść po jakiej stronie Arianna -> drakolici, ale przyznał się do wszystkiego, powiedział o relacjach z Tomaszem, widzi Ariannę jako jedyną nadzieję.
* 2 - Na planetoidzie. Eustachy stawia te cholerne generatory. Elena "zbliżają się do Was. Ufortyfikuj pozycję". Coś próbuje się przebić. Eustachy nagrywa, wysyła do Klaudii.
    * TrZ+2
    * XXzVz: szumy, szepty, sygnały demoralizujące nie-Orbiter. Destruktory morale. 
* 3 - Klaudia przesyła sygnał PRZEZ Elenę i jej Eidolona. Korzystając z okazji, że Elena ma omnidetekcję. Z nadzieją, że Elena dojdzie do siebie + wyczyści sygnał. 
    * TrZ+2
    * XV: Elena ma pełen sygnał, usłyszała wszystko. Usłyszała WSZYSTKO.
* 4 - Eustachy fortyfikuje wejścia poza jednym. Wszystkie. Otwiera, zamyka, śluzy, masakruje, aż się nie połapią "ci z przeszłości".
    * TrZ+3
    * VV: eksterminacja pierwszej fali, poprawione morale. Killzone.
* 5 - Annika, zainspirowana przez Eustachego, próbuje zainspirować ludzi. Eustachy próbuje ją od tego odwieść. Nie wyszło jej.
    * Tr (niepełny) Z (Eustachy, Orbiter) +2
    * XVz: morale jest wyżej i jest zaufanie do Orbitera. Ale z Anniki nic nie będzie, zdaniem wielu.
* 6 - Zatem ruch Zespołu - sytuacja jest stabilna. Podmieniamy software BIA. Czyli - niech BIA dostanie lobotomię od Eleny. Elena infiltruje w kierunku na BIA.
    * TrZ (homo superior) +4+5O:
    * OXVXO: Elena ma obsesję na punkcie urody. Szaman ją wykrył, Elena go ZRANIŁA i nie poradziła z transplantacją... więc transplantowała SZAMANA w BIĘ. Horror, ale skuteczny.
