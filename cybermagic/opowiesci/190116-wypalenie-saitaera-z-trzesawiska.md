## Metadane

* title: "Wypalenie Saitaera z Trzęsawiska"
* threads: pieknotka-kaplanka-saitaera, saitaer-bog-primusa
* motives: wojna-bogow, escort-quest, stopniowe-tracenie-siebie, wsparcie-artylerii, teren-morderczy, forced-transformation
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [190113 - Chrońmy Karolinę przed uczniami](190113-chronmy-karoline-przed-uczniami)

### Chronologiczna

* [190113 - Chrońmy Karolinę przed uczniami](190113-chronmy-karoline-przed-uczniami)

## Projektowanie sesji

### Pytania sesji

* 

### Struktura sesji: Frakcje

* Trzęsawisko: WYLĘGARNIA, REZERWAT

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. ...

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (21:35)

Pięknotka w Barbakanie. Tam czeka na nią dwóch "skazańców" - Hieronim Maus i Kasjopeaa Maus. Na odprawie - Karla. Powiedziała, że ani Kasjopea ani Hieronim nie umieją używać power suitów - a dodatkowo Kasjopea jest potencjalnie niebezpieczna. Aha, i ani Hieronim ani Kasjopea nie przywykli do poruszania się po bagnie.

Pięknotka zdecydowała się zrobić z nimi krótki bieg po sali treningowej by sprawdzić ich umiejętności i pokazać im jak głupie mają odruchy. Ku jej zdziwieniu, Kasjopea jest kompetentna. Umie walczyć... gorzej, LUBI walczyć. Hieronim się zmęczył na 10 metrze...

Pięknotka widziała już wszystko. Poprosiła Karlę o dywersję. Karla wysłała Alana na Zjawosztup... musi mieć kogoś z dużą siłą ognia. Kogoś, kto odwróci uwagę Trzęsawiska DOBRZE i SKUTECZNIE.

Pięknotka poprosiła też Karlę, by ASD "Centurion" sprawił, że ołtarz odtworzy się na w miarę bezpiecznym miejscu. Karla spytała ze spokojem gdzie ma to być. Pięknotka poprosiła, by to była Głodna Ziemia. Karla przekazała wytyczne do Centuriona... Pięknotka NIE CHCE walki bóstw przy Toni. Nie wie, co by się mogło stać.

**Scena**: (21:44)

Arena walki wybrana. Pięknotka w power suicie i dwóch Mausów przygotowali się na operację. Pięknotka chce się dowiedzieć od Kasjopei co ona tu tak na serio robi. Nie kupiła, że Kasjopea jest "ofiarą". (Tp+1:10,3,3=S). Kasjopea wzruszyła ramionami. Potęga. Chwała. Karradrael kontra Saitaer. Najlepsza holokostka w historii! Pięknotka powiedziała zrezygnowana, że wlecze turystkę... ale niech ona zrobi JEDNĄ głupią rzecz... Kasjopea skwapliwie się zgodziła na walkę na warunkach Pięknotki. No, nieźle...

Kapłan jest nieszczęśliwy. Jest tłuściutki i nie ma wprawy w tego typu terenie. W ogóle nie czuje się dobrze. Pięknotka poczuła potężny wstrząs. To Alan coś zdetonował. To czas, by weszli na Trzęsawisko. Zasady proste - idą po śladach, nie obok, słuchają się. Tak zrobią. Mają się słuchać i będą. Kasjopea jest cała szczęśliwa, Hieronim składa się ze smutku...

Dobrze więc. Trzęsawisko czeka... A na początku, wielkie błota. Trzeba pomanewrować. Każdy krok jest ważny, trzeba manewrować i skakać. Magowie dostali buty do odpowiedniego poruszania się na tym. Pięknotka patrzy na Hieronima. To się nie skończy dobrze. Kasjopea zaproponowała, że ona pójdzie tyłem. Pięknotka idzie przodem i patrzy jak sobie radzi Hieronim. Ma oczy dookoła głowy. Dostrzegła (Tp+1:10,3,3=S) grupujące się magogi. Te cholerstwa zaczną miotać rzeczami - od gniazd os po orzechy - najlepiej w Hieronima. Acz i Pięknotka może oberwać. Ogólnie nie są bardzo groźne, jeśli leci się szybkim power suitem...

Pięknotka wycofała ich na kawałek twardszego gruntu. Po czym zaczęła sukcesywnie wystrzeliwywać magogi (Tr+2:9,3,5=SS). Udało się, acz cholerne magogi lekko pokąsały Hieronima. Pięknotka przeprowadziła ich przez bagnisty teren i poszła dalej, lecząc Hieronima najlepiej jak może medikitem. Pięknotka poczuła kolejne trzęsienie, gdy Centurion walnął działami orbitalnymi ponownie...

ZNOWU Pięknotka jest ostrożna, i wypatrzyła (Tp:S) xirathira (-5 Wpływu Żółw). Śmiertelnie niebezpieczny przeciwnik obserwuje ich z bezpiecznego miejsca. Pięknotka WIE, że jeśli xirathir zaatakuje, to wtedy, gdy ona nie będzie w stanie sensownie działać. Xirathir nie poluje z głodu - on poluje z żądzy mordu. To vicinius, nie Skażeniec. A fakt tego co dzieje się na bagnie nie nastraja go pozytywnie do ludzi i magów.

Pięknotka się nie pieprzy. Poprosiła ASD "Centurion" o walnięcie z działa orbitalnego w pozycję xirathira. Centurion poprosił o potwierdzenie - są w fali uderzeniowej. Pięknotka potwierdziła, podcięła kapłana i sama padła z Kasjopeą. Pięknotka próbuje chronić i Hieronima i Kasjopeę. I używa silniczków by zostać na ziemi. (Tr+1:8,3,6=P). Power suit Pięknotki został uszkodzony - poszły silniczki z pleców, ale uratowała zarówno Kasjopeę jak i Hieronima. Xirathir WYPAROWAŁ. Z ziemi doszło do erupcji morderczych macek - Trzęsawisko próbuje się bronić przed bolesnym dla siebie atakiem.

Hieronim się rozkleił - stracił wiarę i siłę. Kasjopea jest zafascynowana, zobaczyła swój Majestat... (Łt:11,3,1=S). Powstrzymała Kasjopeę przed uruchomieniem nożem Hieronima. Na gniewne spojrzenie Kasjopei Pięknotka spytała, czy chce już ich oboje zabić. Żadnej magii i żadnej krwi. Kasjopea odpowiedziała nienawistnym spojrzeniem ale schowała broń. Zadała ból Hieronimowi by go wytrącić ze stuporu - udało się...

Pięknotka skorzystała z okazji - stadko spłoszonych jeleni (-2 Wpływ) czy coś wpadło w macki. W efektowny sposób zostały porozrywane. Pięknotka szybko przeprowadza magów przez bagno, korzystając z okazji. Dzięki Ci smoku za Alana i Centuriona...

(Tp:7,3,4=SS). Po Manifestacji Karradraela pokąsanie przez magogi będzie zbyt silne dla Hieronima.

W końcu dotarli do okolic Głodnej Ziemi. Trzeba przejść przez to miejsce dotknięte Energią Mentalną. Wszyscy chlapnęli sobie przed akcją odpowiednie reduktory, ale nie spodziewali się wpływu mentalnego Ołtarza (Tp:9,3,3=S). Dotarli do samego ołtarza Saitaera, czując potęgę obcego bóstwa. Saitaer zagnieździł się w Trzęsawisku, niemożliwy do zdarcia czy odepchnięcia...

...to jest, tylko, że tu jest kapłan Karradraela o ogromnej mocy. Ktoś, kto oddał całe swoje życie Karradraelowi. Ktoś, kto chce oddać wszystko Karradraelowi. I stał się kanałem kanalizującym potęgę Karradraela. Zamanifestował się Karradrael i uderzył energią w ołtarz (Pięknotka: 6,3,6=P). Saitaer nie kontratakował - przekształcił Pięknotkę ponownie. Wzmocnił po raz kolejny swój hold na jej ciele. Coraz łatwiej przychodzi mu modelowanie jej ciała wedle swoich potrzeb...

Potężna erupcja energii, po chwili Saitaer został wyżarty z Trzęsawiska. Hieronim padł na ziemię bez cienia przytomności, pokonany przez magogi i emitowaną energię. Kasjopea jest ledwo przytomna - za duże energie. Pięknotka nie poczuła tego z energiami, jej ciało się zaadaptowało mocą Saitaera...

Pięknotka zgarnia obu na ramiona i próbuje wrócić ścieżką inną do Pustogoru. Chce wrócić bezpiecznie. Na drodze stanął jej... Wiktor Satarail w drugiej formie. Powiedział jej, że dalej nie przejdzie - Hieronim tu zostaje z uwagi na zbrodnie. Pięknotka skorzystała z okazji i rzuciła w niego Kasjopeą. Wiktor odruchowo ją złapał i objawił się Karradrael - po raz kolejny. Wyczyścił Wiktora ze wpływu Saitaera. Wiktor padł rażony energią. Kasjopea... no cóż, ona jest kiepskim przewodnikiem.

Pięknotka z trudem wzięła Hieronima i Kasjopeę oraz wyczłapała z bagna. Misja wykonana - nie ma już wpływów Saitaera na Trzęsawisku...

Wpływ:

* Ż: 5 (10): 
* K: 1 (6): 

Skuteczne Akcje: .

**Epilog** (23:20)

* Wiktor Satarail nie jest pod wpływem Saitaera.
* Trzęsawisko też nie jest pod wpływem Saitaera. Ołtarz został wypalony przez Karradraela.
* Pięknotka stała się podatna na transformację Saitaera.
* Wiktor, odbicie Trzęsawiska, jest wściekły na Pustogor. Wypowiada im wojnę.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |    3    |     10      |
| Kić           |    3    |      8      |

Czyli:

* (K): .
* (Ż): Wiktor Satarail idzie na wojnę z Pustogorem po tym wszystkim. Chcieli zniszczyć mu dom i serce, on zniszczy ich. (2)

## Streszczenie

Pięknotka dostała zadanie eskortowania kapłana Karradraela do ołtarza Saitaera. ASD Centurion oraz Alan odwracali uwagę Trzęsawiska. Plan się udał, choć w wyniku Pięknotka została znowu przekształcona przez Saitaera. Za to, nie ma już wpływu Saitaera w okolicy - więcej, udało się zdjąć też wpływ z Wiktora.

## Progresja

* Pięknotka Diakon: wśród załogi ASD Centurion ma reputację szalonej, zarówno w negatywnym spojrzeniu jak i z pewnym podziwem. Co więcej, przetrwała.
* Pięknotka Diakon: jej Wzór jest łatwy do zmodyfikowania przez Saitaera. Za dużo razy to już robił i zbyt dobrze ją już zna.
* Wiktor Satarail: oczyszczony z wpływów Saitaera, ale jednocześnie wściekły na Pustogor za rany zadane Trzęsawisku, jak i na Pięknotkę za zabranie mu potencjalnych jeńców. 
* Saitaer: utracił _hold_ na Trzęsawisku Zjawosztup i w Wiktorze Satarailu. Uzyskał możliwość łatwej transformacji Pięknotki.
* Alan Bartozol: Wiktor Satarail go nienawidzi i chce go zniszczyć

## Zasługi

* Pięknotka Diakon: eskortowała (niechętnie) cywili na Trzęsawisko by zniszczyć wpływ Saitaera. Udało jej się - dodatkowo rozproszyła wpływ Saitaera na Wiktorze. Uległa transformacji Saitaera (znowu).
* ASD Centurion: statek artyleryjski na orbicie Astorii który wpierw doprowadził do wymuszenia regeneracji ołtarza w odpowiednim miejscu a potem wspierał ogniem Pięknotkę.
* Alan Bartozol: wraz ze swoją małą ekipą szturmowców odwracał uwagę Trzęsawiska od Pięknotki. Z powodzeniem. Zdobył ogromną nienawiść Wiktora.
* Wiktor Satarail: próbował rozpaczliwie zatrzymać atak magów na Trzęsawisko - bez skutku. Potem Pięknotka rzuciła weń Kasjopeą, rozpraszając wpływ Saitaera. Jest wściekły jak osa.
* Hieronim Maus: tłuściutki kapłan Karradraela, najlepszy nośnik na Astorii. Bardzo nieszczęśliwy, bo musiał być odeskortowany do ołtarza Saitaera na Trzęsawisku.
* Kasjopea Maus: poszła jako dziennikarka uczuć na Trzęsawisko. Motywowała Hieronima. Niespecjalnie przeszkadzała Pięknotce, acz skończyła jako pocisk balistyczny niosący Karradraela.
* Karradrael: doprowadzony przez swojego kapłana do ołtarza Saitaera rozpoczął wypalanie - Saitaer nie podjął rękawicy, więc go odepchnął od zarówno Trzęsawiska jak i Wiktora.
* Saitaer: po raz kolejny odparty; tym razem wypalony z Wiktora Sataraila i z Trzęsawiska Zjawosztup. Nie podjął rękawicy Karradraela by rozpocząć wojnę totalną.

## Frakcje

* Astoriańskie Siły Defensywne: skierowali ASD Centurion (statek artyleryjski) do wsparcia Pustogoru przed potencjalnym zagrożeniem ze strony Saitaera.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Trzęsawisko Zjawosztup: cel skoordynowanego ataku ASD Centurion oraz Alana, które NADAL było w stanie zagrozić Pięknotce.
                            1. Głodna Ziemia: miejsce walki bogów - Ołtarz Saitaera kontra kapłan Karradraela. Miejsce ponownego Przekształcenia Pięknotki przez Saitaera.

## Czas

* Opóźnienie: 1
* Dni: 2

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* nic

## Wykorzystana mechanika

1811
