## Metadane

* title: "Mikiptur, zemsta Woltaren"
* threads: triumfalny-powrot-arianny, naprawa-swiata-przez-bladawira
* motives: pulapka-bardzo-skomplikowana, zemsta-uzasadniona, the-devastator
* gm: żółw
* players: fox, kić, kapsel

## Kontynuacja
### Kampanijna

* [240110 - Wieczna wojna Bladawira](240110-wieczna-wojna-bladawira)

### Chronologiczna

* [240110 - Wieczna wojna Bladawira](240110-wieczna-wojna-bladawira)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA: 
    * Elfen Lied - Lilium
        * Pain, sorrow, torment and DEVASTATION
        * Mikiptur zaprojektowana jako pułapka na siły Bladawira, coś co zniszczy wszystko za czym Bladawir stoi
        * "Within a minute".
* Opowieść o (Theme and vision)
    * Zemsta za to co się stało na Koronie Woltaren kaskaduje. Wieczna wojna Bladawira z Aurelionem, po drodze wszyscy cierpią...
    * Na Mikiptur znajduje się Dewastator - (ixioński amalgamat z Diakonki i innych magów), 'Diakonka' będąca narzędziem masowego zniszczenia.
        * (kinetyczna osłona, wir defensywny, mechanoostrza, tysiące mechanicznych os, amplifikator kralotyczny)
    * SUKCES
        * Isratazir przetrwa, Mikiptur zostanie przejęta.
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * the-devastator: kralotyczny amalgamat w formie dziewczyny, 'Diakonka która już nie jest osobą'. Nieprawdopodobna siła ognia, swarmer. Rozproszona (niejednorodna).
    * zemsta-uzasadniona: Bladawir zniszczył wszystko za czym stał kapitan Roman Ocheljos, więc Ocheljos zniszczy siebie i Bladawira.
    * pulapka-bardzo-skomplikowana: Aurelion robi kontratak na Bladawira uderzając przez kapitana Ocheljosa i skażoną Diakonkę. Ona "powinna" zniszczyć Bladawira. 5 magów.
    * energia-alteris: The-Devastator, Diakonka, wykorzystuje manipulacje przestrzenne do walki i uniknięcia ognia i pocisków.
    * energia-ixion: The-Devastator, Diakonka, jest manifestacją Perfekcyjnej Adaptacji.
    * energia-esuriit: The-Devastator, Diakonka, żywi się załogą statku którą ma by wzmacniać swoje energie i się zasilać.
* Detale
    * Crisis source: 
        * HEART: "Bladawir walcząc z Aurelionem robi straszne rzeczy. Po drodze niszczy ludzi. Ci ludzie oddadzą się Aurelionowi by zniszczyć Bladawira."
        * VISIBLE: "Isratazir potrzebuje pomocy, Infernia jest najbliżej."
    * Delta future
        * DARK FUTURE:
            * chain 1: Isratazir zniszczony lub przejęty
            * chain 2: Stacja przeładunkowa zniszczona lub przejęta
            * chain 3: Ogromne straty na pokładzie Inferni
        * LIGHT FUTURE:
            * chain 1: BLAME: Bladawir udowodnił, że tam się coś złego stało, ma możliwość działań mimo problemów. Więcej - są próby ZABICIA Bladawira jako ryzyko.
            * chain 2: WARNING: Miraris pojmany żywcem / zabity. Łącznie z jego istotą.
            * chain 3: THE EYE: Bladawir jest tak zajęty walką z działaniami na Orbiterze, że nie ma czasu na Infernię
    * Dilemma: Bladawir - na pewno potwór, ale czy nie wsparcie?

### Co się stało i co wiemy

* Mikiptur jest jednostką dostarczoną przez Syndykat Marikowi Woltarenowi, który pragnie odzyskać Lidię i pomścić Anetę.
    * Ważnym komponentem Mikiptur jest 'TAI Diva', neurokontrolowany (ledwo) the-devastator
    * O czym Marik nie wie, Mikiptur jest na misji samobójczej
* Mikiptur próbuje zniszczyć Bladawira i jego siły; jest nadspodziewanie dobrą i szybką jednostką
    * the-devastator próbuje dodać ludzi do swojego Syfonu Esuriit, zasilającego the-devastator
* Mikiptur złapał Isratazir w pułapkę.

### Co się stanie (what will happen)

* F0: Władawiec wchodzi na Infernię
    * Arianna przebudzona awaryjnie, po przekazaniu informacji Bladawirowi
    * Eustachy dowiaduje się od Larsa, że ich komodor chce zniszczyć Infernię
* F1: Ratujemy Isratazir i ścigamy Mikiptur
    * Mikiptur poważnie uszkodził Isratazir z Waritniezem; Waritniez się oderwał.
    * Mikiptur ma miny i zna teren
    * Mikiptur jest jednostką szybką, leci na stację przeładunkową
    * Q:
        * Czy Mikiptur ucieknie na stację i ją ufortyfikuje?
        * Czy Isratazir odzyska swoich ludzi?
        * Czy Isratazir PRZETRWA?
        * Czy Infernia przetrwa?
* F2: Walka z Mikiptur
    * "SOS, SOS, uratujcie nas!"
    * Przebudzenie Dewastatora
    * Mikiptur przygotowuje się do salwy rakietowej (TAI override)
    * Devastator Unchained
    * Q:
        * Jak duże są straty?
        * Czy uda się zrobić z tego widowisko 'Orbiter i Bladawir są bezradni'?
* CHAINS
    * Problemy i konflikty
        * reputacja: (Bladawir ucierpi, Arianna ucierpi, zniszczyli jednostkę cywilną - opowieść o Lidii...)
        * straty: (Isratazir)
* Overall
    * stakes
        * .
    * opponent
        * .
    * problem
        * .

## Sesja - analiza

### Fiszki

Zespół:

* Leona Astrienko: psychotyczna, lojalna, super-happy and super-dangerous, likes provocative clothes
* Martyn Hiwasser: silent, stable and loyal, smiling and a force of optimism
* Raoul Lavanis: ENCAO: -++0+ | Cichy i pomocny;; niewidoczny| Conformity Humility Benevolence > Pwr Face Achi | DRIVE: zrozumieć
* Alicja Szadawir: młoda neuromarine z Inferni, podkochuje się w Eustachym, ma neurosprzężenie i komputer w głowie, (nie cirrus). Strasznie nieśmiała, ale groźna. Ekspert od materiałów wybuchowych. Niepozorna, bardzo umięśniona.
    * OCEAN: N+C+ | ma problemy ze startowaniem rozmowy;; nieśmiała jak cholera;; straszny kujon | VALS: Humility, Conformity | DRIVE: Uwolnić niewolników
* Kamil Lyraczek
    * OCEAN: (E+O-): Charyzmatyczny lider, który zawsze ma wytyczony kierunek i przekonuje do niego innych. "To jest nasza droga, musimy iść naprzód!"
    * VALS: (Face, Universalism, Tradition): Przekonuje wszystkich do swojego widzenia świata - optymizm, dobro i porządek. "Dla dobra wszystkich, dla lepszej przyszłości!"
    * Styl: Inspirujący lider z ogromnym sercem, który działa z cienia, zawsze gotów poświęcić się dla innych. "Nie dla chwały, ale dla przyszłości. Dla dobra wszystkich."
* Horst Kluskow: chorąży, szef ochrony / komandosów na Inferni, kompetentny i stary wiarus Orbitera, wręcz paranoiczny
    * OCEAN: (E-O+C+): Cichy, nieufny, kładzie nacisk na dyscyplinę i odpowiedzialność. "Zasady są po to, by je przestrzegać. Bezpieczeństwo przede wszystkim!"
    * VALS: (Security, Conformity): Prawdziwa siła tkwi w jedności i wspólnym działaniu ku innowacji. "Porządek, jedność i pomysłowość to klucz do przetrwania."
    * Core Wound - Lie: "Opętany szałem zniszczyłem oddział" - "Kontrola, żadnych używek i jedność. A jak trzeba - nóż."
    * Styl: Opanowany, zawsze gotowy do działania, bezlitośnie uczciwy. "Jeśli nie możesz się dostosować, nie nadajesz się do tej załogi."
* Lars Kidironus: asystent oficera naukowego, wierzy w Kidirona i jego wielkość i jest lojalny Eustachemu. Wszędzie widzi spiski. "Czemu amnestyki, hm?"
    * OCEAN: (N+C+): bardzo metodyczny i precyzyjny, wszystko kataloguje, mistrzowski archiwista. "Anomalia w danych jest znakiem potencjalnego zagrożenia"
    * VALS: (Stimulation, Conformity): dopasuje się do grupy i będzie udawał, że wszystko jest idealnie i w porządku. Ale _patrzy_. "Czemu mnie o to pytasz? Co chcesz usłyszeć?"
    * Core Wound - Lie: "wiedziałem i mnie wymazali, nie wiem CO wiedziałem" - "wszystko zapiszę, zobaczę, będę daleko i dojdę do PRAWDY"
    * Styl: coś między Starscreamem i Cyclonusem. Ostrożny, zawsze na baczności, analizujący każdy ruch i słowo innych. "Dlaczego pytasz? Co chcesz ukryć?"
* Dorota Radraszew: oficer zaopatrzenia Inferni, bardzo przedsiębiorcza i zawsze ma pomysł jak ominąć problem nie konfrontując się z nim bezpośrednio. Cichy żarliwy optymizm. ŚWIETNA W PRZEMYCIE.
    * OCEAN: (E+O+): Mimo precyzji się gubi w zmieniającym się świecie, ale ma wewnętrzny optymizm. "Dorota może nie wiedzieć, co się dzieje, ale zawsze wierzy, że jutro będzie lepsze."
    * VALS: (Universalism, Humility): Często medytuje, szuka spokoju wewnętrznego i go znajduje. Wyznaje Lichsewrona (UCIECZKA + POSZUKIWANIE). "Wszyscy znajdziemy sposób, by uniknąć przeznaczenia. Dopasujemy się."
    * Core Wound - Lie: "Jej arkologia stanęła naprzeciw Anomaliom i została Zniekształcona. Ona uciekła przed własną Skażoną rodziną" - "Nie da się wygrać z Anomaliami, ale można im uciec, jak On mi pomógł."
    * Styl: Spokojna, w eleganckim mundurze, promieniuje optymizmem 'że z każdej sytuacji da się wyjść'. Unika konfrontacji. Uśmiech nie znika z jej twarzy mimo, że rzeczy nie działają jak powinny. Magów uważa za 'gorszych'.
    * metakultura: faerilka; niezwykle pracowita, bez kwestionowania wykonuje polecenia, "zawsze jest możliwość zwycięstwa nawet jak się pojawi w ostatniej chwili"

Castigator:

* Leszek Kurzmin: OCEAN: C+A+O+ | Żyje według własnych przekonań;; Lojalny i proaktywny| VALS: Benevolence, Humility >> Face| DRIVE: Właściwe miejsce w rzeczywistości.
* Patryk Samszar: OCEAN: A+N- | Nie boi się żadnego ryzyka. Nie martwi się opinią innych. Lubi towarzystwo i nie znosi samotności | VALS: Stimulation, Self-Direction | DRIVE: Znaleźć prawdę za legendami.
    * inżynier i ekspert od mechanizmów, działa świetnie w warunkach stresowych

Siły Bladawira:

* Antoni Bladawir: OCEAN: A-O+ | Brutalnie szczery i pogardliwy; Chce mieć rację | VALS: Power, Family | DRIVE: Wyczyścić kosmos ze słabości i leszczy.
    * "nawet jego zwycięstwa mają gorzki posmak dla tych, którzy z nim służą". Wykorzystuje każdą okazję, by wykazać swoją wyższość.
    * Doskonały taktyk, niedościgniony na polu bitwy. Jednocześnie podły tyran dla swoich ludzi.
    * Uważa tylko Orbiterowców i próżniowców za prawidłowe byty w kosmosie. Nie jest fanem 'ziemniaków w kosmosie' (planetarnych).
* Kazimierz Darbik
    * OCEAN: (E- N+) "Cisza przed burzą jest najgorsza." | VALS: (Power, Achievement) "Tylko zwycięstwo liczy się." | DRIVE: "Odzyskać to, co straciłem."
    * kapitan sił Orbitera pod dowództwem Antoniego Bladawira
* Michał Waritniez
    * OCEAN: (C+ N+) "Tylko dyscyplina i porządek utrzymują nas przy życiu." | VALS: (Conformity, Security) "Przetrwanie jest najważniejsze." | DRIVE: "Chronić moich ludzi przed wszystkim, nawet przed naszym dowódcą."
    * kapitan sił Orbitera pod dowództwem Antoniego Bladawira
* Zaara Mieralit
    * OCEAN: (E- O+) "Zniszczenie to sztuka." | VALS: (Power, Family) "Najlepsza, wir zniszczenia w rękach mojego komodora." | DRIVE: "Spalić świat stojący na drodze Bladawira"
    * corrupted ex-noctian, ex-Aurelion 'princess', oddana Bladawirowi fanatyczka. Jego prywatna infomantka. Zakochana w nim na ślepo.
    * afiliacja magiczna: Alucis, Astinian

Siły inne:

* Władawiec Diakon: tien, corruptor of ladies, duelist, second-in-command na statku kosmicznym (XO)
    * ENCAO: (E+C+) | intrygancki;; kontrolowany wulkan;; pająk w sieci | VALS: Hedonism, Power | DRIVE: Perfekcja Zaufania
    * styl: nieskazitelny oficer, delikatny uśmiech, niezmiennie pewny. Gabriel Durindal (Gundam Seed Destiny)
    * piosenka wiodąca: Epica "Obsessive Devotion"

### Scena Zero - impl

Arianna leży w medycznym, półprzytomna, przykryta ledwo szmatą, z kroplówką. Bladawir spojrzał, "zadowalające" i wyszedł po paru pytaniach.

### Sesja Właściwa - impl

Arianna i Klaudia regenerują u siebie. Eustachy - do Ciebie przyszedł Lars.

* Lars: Eustachy, mistrzu, przed Infernią stoi wielki problem.
* Eustachy: ?
* Lars: (ciężko westchnął). Na pokład Inferni pojawi się KOCHANEK KAPITAN VERLEN. Ściągnęła sobie kochanka. 
* Eustachy: I?
* Lars: (z niedowierzaniem). Twój wielki plan nie zadziała.
* Eustachy: Ja mam plan?
* Lars: Rozkochać w sobie panią kapitan?
* Eustachy: (bleh) to by było nieprofesjonalne
* Lars: Rozumiem. Masz głębszy plan. Jest powiązany z tym, że komodor próbuje zniszczyć Infernię?
* Eustachy: Odstaw dropsy albo się podziel.
* Lars: Nie, to poważne - komodor.
* Eustachy: Jest tak improwizowana, Infernia, że ja jestem kluczem. Ona rozpada się w locie to się rozleci.
* Lars: Nie zastanawia Cię, czemu Infernia ma najwięcej magów w załodze na populację?
* Eustachy: Nie myślałem. Bo statek do zadań specjalnych. 
* Lars: Uważaj na kochanka Arianny. On może przejąć pod Tobą Infernię.

Infernia nie ma chwilowo rozkazów - ma być 'w odwodach', czyli 'nic mądrego nie ma robić'. Sprawy celne itp. Bladawir wziął 2 statki - Adilidam oraz A'tazir poleciały zająć się innymi tematami.

Władawiec Diakon, p.o. medycznego oficera. Zaara jest z Bladawirem w tej chwili.

Wiadomość na kanale priorytetowym.

* Isratazir: "SOS".
* Eustachy -> Lars: "Ogarnij!"

Tr (niepełny) Z +3:

* Vz: Infernia jest skalibrowana a Lars zna się na rzeczy
    * To nie jest prawdziwy sygnał SOS.
        * Isratazir, ostrzegam komodora, najpewniej jesteśmy martwi, NIE ZBLIŻAJCIE SIĘ!
        * Jeszcze się trzymamy, co najmniej dwie jednostki wsparcia, nie jedna!
        * (Infernia jest 30 min od Isratazir)
    * Lars ma wezwać wsparcie i Infernia leci.

Władawiec ma obudzić Ariannę 15 min do.

Eustachy leci z NORMALNĄ prędkością Inferni. Wezwane było SOS. Infernia jest najbliżej. 20 min po Inferni pojawi się następna jednostka, (Sarkalin, korweta). Eustachy dąży do stabilnego zbliżenia się do Isratazir. Eustachy dąży do przeczesania sygnału.

Isratazir jest ciężko uszkodzona, bardzo ciężko, dostała salwą, ale nie jest zniszczona. Drugi statek oddala się od Isratazir. To też jest korweta, dość szybka. Isratazir ma poważne straty w załodze i sprzęcie. Eustachy połączył się z Sarkalin - leci pomóc na Isratazir, ale wyśle pintkę na Isratazir. Combat medic, inżynier, by ogarnąć co i jak czy to pułapka. Sarkalin ma śledzić kierunek Mikiptur. Dowódca Sarkalin jest młody, mało doświadczony, więc dostaje opieprz prewencyjny.

Eustachy zaczyna budzić dziewczyny, 10 min. do wejścia w zasięg Isratazir.

Arianna pod prześcieradłem, Władawiec ją budzi i briefuje. Arianna widzi decyzje i szybko idzie na mostek.

* Eustachy: "Mam nadzieję, że zadziałałem zgodnie z sumieniem pani kapitan"
* Arianna: "Skan"

Tr Z +3:

* Vz: W okolicy nie ma żadnych innych jednostek, nic co Infernia widzi (brak czegoś wskazującego na koloid)
* Vr: Isratazir wygląda na zaminowany. Solidnie zaminowany.

Arianna wydała rozkaz - niech Sarkalin się wycofa jak będzie zaatakowany.

Klaudia robi szerokie serie skanów w stronę na Isratazir. Chce się dowiedzieć - co i jak, co się dzieje itp. I czy da się nawiązać połączenie.

Tr Z+3:

* Xz: TAI jest zniszczona (COŚ JEST BARDZO NIE TAK). Została wysadzona. Tam nic nie ma. Banki danych zostały zabrane. Doszło do insercji.
    * pomóc ludziom, czy banki danych (które są zabrane z Isratazir)?
    * Arianna -> POMAGAMY.
* V: Isratazir jest nie tylko zaminowany - reaktor jest zdestabilizowany. Reaktor jest bombą.

Wiadomość od Bladawira:

* Bladawir: Kapitan Verlen, musisz złapać tamtą jednostkę. Jesteś w stanie?
* Arianna: W tym momencie jest poza naszym zasięgiem.
* Eustachy: (nie powiedziałbym)
* Bladawir: Jesteś pewna?
* Arianna: Tak, jestem pewna, komodorze
* Bladawir: Rozumiem (myśli). Uratuj kogo się da. Bądź ostrożna.  (+1 do przekonania Bladawira, że Arianna jest agentem)
* Arianna: Tak jest, komodorze
* Klaudia -> Bladawir: "potencjalnie mamy maga na pokładzie korwety"
* Bladawir: Zrozumiałem.

Jeśli tam jest Malictrix, Klaudia jest w stanie zbudować odpowiednik neutralizatora. Bo celem Malictrix może być ściągnięcie innej jednostki, ściągnąć ją i zniszczyć. Sandbox niby bezpieczny, ale. A jak uzna że ściągnęło załogę to koniec.

Najpewniej chcą zniszczyć jak najwięcej osób. Najpewniej nie zniszczyć 'jedną' osobę. Można więc zasymulować holowanie na Orbiter by kupić czas. Na pewno mają ograniczony czas istnienia. Opcja 1: zostawiamy ich samych sobie. Opcja 2: rozbrajamy miny ORAZ potem wybuchowe systemy.

Eustachy ma plan - zasłonić gwiazdę i nadawać morsem, używając "cienia" przez pintkę. To jest mors i ma przekazać im "wyjdźcie, bezpiecznie itp, przechwycimy Was w kosmosie". 

Ex Z+3:

* Vr: nawiązana komunikacja - oni "plazmą się przebijają morsem przez grodzie", Klaudia to czyta.

Paolo Rudd, nawiązał połączenie, oficer ochrony.

* Kapitan i większość sztabu jest porwana
* Część załogi jest porwana
* Przeciwnik miał maga.
* Kapitan zostawił zapis z bitwy, jest w kosmosie. W ciemnej kapsule (podał trajektorię).
* To Wielki Wróg (Syndykat), to była pułapka
* Jest sporo ludzi którzy przeżyli, ale oni są poodcinani. To jest pułapka. Rekomenduje: 'zostawcie nas'.
* Ludzie są jeńcami TAI; nałożono jej coś.

Pintka z Raoulem -> za kapsułą. 

Klaudia ma plan - zneutralizujmy TAI Malictrix. Robi sandboxa i przygotowuje zaklęcie. Atak memetyczny. Gdy TAI wyśle sygnał, to atak memetyczny uderzy w tą TAI. A Infernia ma bardzo wzmocnioną psychotronikę a Klaudia jest zaawansowaną psychotroniczką. I jest unikalnie dobrą czarodziejką.

Tr ZM +3 +3Om:

* Ob: 
    * Infernia jest OBUDZONA. Niewiele może, ale nie śpi.
    * Malictrix ORAZ TAI statku zostały spożyte przez Interis.
    * AI Core jest puste. Nie przeszkodzi. Nie pomoże. Nic nie robi.
    * Infernia MOŻE szeptać do Eustachego. Ale jeszcze nie może słowami.

Eustachy rozminowuje to cholerstwo. Teraz już z grupą innych inżynierów. Nie musi sam.

Tr Z +3:

* X: Dalekosiężne detektory pokazują, że Mikiptur odwróciła się w stronę Sarkalina. Sarkalin wpadł na minę czy coś.
* Vz: Inżynierowie i Eustachy robią robotę.

Arianna porzuca inżynierów XD. Leci na pełną mocą na Mikiptur. Arianna - pełna moc silników. Nie przesterowuje ale pełna moc.

Tr Z+2:

* X: Mikiptur widzi zbliżającą się Infernię, tak 'dla przyjemności' laserem w Sarkalin. Walą by wyrządzić zniszczenia.
* X: Sarkalin też będzie potrzebować pomocy - załoga nie przeżyje bez tego.
* V: Arianna zbliżyła się do Sarkalin i Mikiptur; nadal poza zasięgiem, ale już... jest zagrożenie
* (+M): Arianna wzmacnia działa Inferni magią, by sygnatura była chora. Mikiptur SPIEPRZA JAK MYSZ. Z tym nie walczy, NOPE!

Infernia skupia się na pomocy Sarkalin. Tymczasem, Eustachy:

* Rozmontowałeś miny i możesz wejść na pokład. 
* Eustachy wraz z inżynierami zaprowadza porządek na jednostce.

TrZ+3:

* X: Niestety, niektórzy zginęli - zbyt ranni, zbyt daleko, zbyt pod gruzami. Nie ma Inferni i pełnych sił medycznych. Nie ma też tu wiele rzeczy a trzeba iść powoli.
* Vz: Udało się uratować większość ludzi
* X: Straciliśmy kilka osób do ukrytych ładunków na pokładzie - oni zostawili 2-3 miny też w środku

Infernia jest w stanie zarządzać tym co się tu dzieje zanim pojawią się następne jednostki Orbitera. Klaudia koordynuje. Bladawir gdzie był? Zajmował się tematem Ernesta Bankierza - kapitana który zabił kralotha. Innymi słowy - gdyby był na swojej pozycji, to by do tego wszystkiego nie doszło. A tak tylko Infernia była w pobliżu.

Arianna i Infernia nie są w stanie ścigać i złapać tamtej jednostki. Mikiptur - hunter killer Syndykatu - skutecznie ucieknie przed Infernią.

Klaudia ma pomysł - wykorzystać Ariannę i jej reputację, powiedzieć, że Syndykat zaatakował i porwał oficera Orbitera.

Klaudia szybko doszła do danych z Orbitera:

* Ten Mikiptur to ten sam IFF, ale inna jednostka. 
* Mikiptur należy do Woltarenów, tak jak Korona Woltaren.
* Zemsta za zemstę. I tak do końca...

Arianna: "Jedyne co tworzy Bladawir to kolejnych Bladawirów". Ale Syndykat chce zamaskować za zemstą Woltarenów, a naprawdę chce uderzyć w Bladawira. ALE NA PEWNO DZIENNIKARZE TO KUPIĄ. Dodajmy fakt, że Bladawir był w złym miejscu, nie na swojej pozycji...

Infernia koordynuje operacje ratunkowe i wraca na K1...

Arianna:

* Spotkaliśmy się z Woltarenowi, obiecałam że wspomogę ich jak się spotkamy w przyszłości (nagrania)
* Ktoś podszywa się pod Woltarenów lub ich wykorzystuje a nie mogliby bo ich nie stać (znaliśmy ich) (nagrania)
* I porównanie TEJ WALKI i tego co na Koronie (nagrania)
* KTOŚ się podszywa i atakuje Orbiter. Aurelion jest realnym zagrożeniem.
* Pokazanie, że Aurelion działa na tym terenie - porwał Natalię, współpracował z kralothem...
* (plus kto podejrzewałby Verlenkę o jakieś ruchy polityczne)

WNIOSEK: jesteśmy w stanie wojny z Syndykatem Aureliona.

Tr Z +3:

* Xz: (Arianna to mówi by chronić komodora Bladawira, ona tak się stara by nie spotkały go negatywne konsekwencje) -> ogólna opinia
* X: Mikiptur w chaosie polowania na Aureliona, na "to wszystko" - dał radę dostać się do przeładunku i opuścił teren Orbitera
* X: Potężne uderzenie w Kurzmina, bo on "jest nierozsądny" i przez niego potencjalnie Syndykat miał dostęp do magów Aurum. Orbiter musi zwierać szyki - program kosmiczny Aurum trzeba eksterminować.

## Streszczenie

Syndykat zdecydował się zaatakować Bladawira i wysłał Dewastatora przy użyciu Mikiptur jako pułapki. Skutecznie zniszczył Isratazir. Arianna śpi, więc Eustachy (nie spiesząc się) przyleciał ratować Isratazir. Infernia rozmontowała miny i wyciągnęła kogo się dało, mimo, że Mikiptur uszkodził Sarkalin. Klaudia i Arianna zrobiły co mogły, by ograniczyć polityczny fallout. Arianna ujawniła obecność Syndykatu, ratując co się da z reputacji Bladawira i jednocząc Orbiter.

## Progresja

* Leszek Kurzmin: potężny cios - "jest nierozsądny" i przez niego potencjalnie Syndykat miał dostęp do magów Aurum. I to z winy Arianny XD.
* Antoni Bladawir: potężna strata autorytetu, opinii świetnego oficera itp. Gdyby nie Arianna, straciłby poziom komodora.
* OO Infernia: przebudzenie jej anomalii umożliwiła jej pożarcie TAI Malictrix i neutralizację psychotronicznych pułapek na Isratazir.

## Zasługi

* Arianna Verlen: skoordynowała obronę przed Dewastatorem, skupiła się na ratunku Isratazir ponad polowaniem na Mikiptur. Potem neutralizowała polityczny fallout by chronić Bladawira i ujawniła obecność Syndykatu.
* Klaudia Stryk: wykorzystała psychotronikę i magię do neutralizacji zagrożenia ze strony TAI Isratazir; niestety, anomalia Inferni się przebudziła przy okazji. Odkryła i ujawniła rolę Syndykatu w ataku Mikiptur.
* Eustachy Korkoran: rozminował Isratazir i opracował plan jak uratować ludzi minimalnym kosztem. Niestety, błędne decyzje strategiczne zniszczyły Sarkalin oraz poważnie naraziły Isratazir a nawet Infernię.
* Antoni Bladawir: był w złym miejscu, wierząc w swoje plany i działania i został wymanewrowany przez Aureliona. Gdyby nie Arianna i Infernia, przestałby być komodorem.
* Lars Kidironus: ostrzegł Eustachego o potencjalnych problemach z Władawcem i Bladawirem. Oczywiście, błędnie.
* Władawiec Diakon: bezwzględnie flirtuje z Arianną, ale postawił ją wtedy kiedy powinien tak jak powinien.
* Raoul Lavanis: odzyskał kapsułę z danymi z ataku na Isratazir, co było kluczowe dla morale załogi i gromadzenia informacji na temat wroga.
* OO Isratazir: Mimo ciężkich uszkodzeń i strat spowodowanych przez Dewastatora z Mikiptur, załoga wykazała wyjątkową odporność i determinację w obronie statku, co umożliwiło późniejsze operacje ratunkowe i minimalizację strat.
* OA Mikiptur: zmieniona w jednostkę Aureliona zawierająca The Devastator, zniszczyła Isratazir i polowała też na Infernię. Próbuje zniszczyć reputację Bladawira i udaje powiązanie z Woltarenami.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Pierścień Zewnętrzny

## Czas

* Opóźnienie: 2
* Dni: 3
