## Metadane

* title: "Korkoran płaci cenę za Nativis"
* threads: kidiron-zbawca-nativis, neikatianska-gloria-saviripatel
* motives: wojna-domowa, perfect-vision, dark-temptation, rekonstrukcja-ruiny, zdrada-w-rodzinie, szczur-zapedzony-do-kata, machinacje-syndykatu-aureliona, system-moloch, symbol-lepszego-jutra
* gm: żółw
* players: fox, kapsel

## Kontynuacja
### Kampanijna

* [230719 - Wojna o arkologię Nativis - nowa regentka](230719-wojna-o-arkologie-nativis-nowa-regentka)

### Chronologiczna

* [230719 - Wojna o arkologię Nativis - nowa regentka](230719-wojna-o-arkologie-nativis-nowa-regentka)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * "Broni Faith - Human Radio Mix"
* CEL: 
    * Highlv
        * Kapsel potrzebuje godnej, uczciwej walki w mechanice walki z inicjatywą i wymiarami
        * KTO przejmie kontrolę nad arkologią Nativis - Laurencjusz i Tymon czy Infernia?
    * Tactical
        * ?

### Co się stało i co wiemy

* Frakcje Arkologii
    * **Lobrak + Laurencjusz**
        * impuls: wyjść z tego cało, scorruptować Eustachego, przejąć kontrolę lub się wycofać
            * "Syndykat to Syndykat, wszystko zrobiła Izabella"
            * "Pomogę Ci ze wszystkim. Nie zdradziłem Kidirona, Izabella mi kazała"
            * "Chcesz, by Arkologia płonęła? I tak tylko Syndykat ma surowce w pobliżu. I się podzieli z Regentem"
            * "Jak to się dzieje, że widzisz co się tu dzieje? Masz detektory."
        * techniki i zasoby:
            * Oddział bojowy do ataku na farmy
            * Neuroobroży
            * Baza w Starej Arkologii
            * Siły Syndykatu zdolne do zniszczenia Arkologii, łącznie z zamaskowaną artylerią w piaskach
        * ograniczenia:
            * jeśli tego nie rozwiąże, jest po nim
    * **Tymon**
        * impuls: patricide
        * techniki: pas szahida, wpływ na ojca, mały oddział (3 osoby)
        * ograniczenia: kocha ojca, "czemu nigdy mnie nie wybrałeś!"
    * **Lertysowie**
        * impuls: naprawa szkód po Kidironie
    * **Regentka Kalia**
        * impuls: integrate and rebuild, ochrona arkologii
    * **Rebelianci**
        * impuls: atak na LITL i Infernię
            * atak z zaskoczenia
        * slogan: "KIDIRON MUST DIE! Infernia must die!!!"
    * **Hełmy Draglina**
        * impuls: odzyskać Kidirona
        * slogan: "Tylko Kidiron. PRAWDZIWY Kidiron."

### Co się stanie (what will happen)

* F1: 
    * Draglin
    * Lobrak chce negocjować 
        * (zapułapkował Starą Arkologię i porozstawiał miny w różnych miejscach)
        * (oddział uderzy w Farmy, w Magazyny nie może)
* F2:
    * Last stand Tymona (Muzeum Kidirona)
* SN: Kalia poważnie rozważa sojusz z Lobrakiem dla ochrony Arkologii. "Eustachy lub Kidiron to rozwiąże, oni nie wiedzą"
* SN: Draglin chce odzyskać Kidirona i zniszczyć Lobraka i innych zdrajców
* SN: Tymon vs Wujek

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Marcel Draglin: oficer Kidirona dowodzący Czarnymi Hełmami, dumny "pies Kidirona"
    * (ENCAO: --+00 |Bezbarwny, bez osobowości;;Bezkompromisowy i bezwzględny;;Skupiony na wyglądzie| VALS: Family (Kidiron), Achievement >> Hedonism, Humility| DRIVE: Lokalny społecznik)
    * "Nativis będzie bezpieczna. A jedynym bezpieczeństwem jest Kidiron."
    * Najwierniejszy z wiernych, wspiera Rafała Kidirona do śmierci a nawet potem. Jego jedyna lojalność jest wobec Rafała Kidirona. TAK, to jego natura a nie jakaś forma kontroli.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Status:

* największą siłę militarną oraz polityczną ma Eustachy
    * Czarne Hełmy, Infernia, 2 statki kosmiczne, największy teren, rząd dusz
* Lobrak i ekipa nie mają jak uciec
* perfect vision
* przewaga magów: Eustachy + potencjalnie Ralf, oni zero

Problemy:

* Lobrak, Tymon i Laurencjusz na wolności
    * mają jakieś siły, ale nie takie jak E. 
    * nie mają fabrykatorów, E. ma stopniowo wracające online
* Mamy Skażonych piratów z Inferni. To jest problem. Szaleni.
    * -> Draglin rzucił na nich Czarne Hełmy

Draglin kontaktuje się z Eustachym:

* D: komendancie, gdzie jest Kidiron? Sytuacja jest bezpieczniejsza. Chcę go albo pod moją obserwacją albo na Inferni. Tylko w moich rękach lub na Inferni jest bezpieczny
* E: jest w bezpiecznym miejscu, nie mogę powiedzieć gdzie ani potwierdzić że żyje przez infiltrację
* D: komendancje, rozumiem. ONI są wszędzie (rozejrzał się po ścianach). Kidiron **musi** wejść w bezpiecznie miejsce (obsesyjnie)
* E: jest w bezpiecznym miejscu
* D: czy jest w tak bezpiecznym miejscu jak Infernia?
* E: porównywalnie bezpiecznie (spoiler: ma jednego szczura który lubi Ardillę i dlatego chroni Kidirona)

Tr Z +3:

* V: Draglin jest uspokojony, że Eustachy robi co powinien

.

* Draglin -> Eustachy: "chcesz samemu zabić swojego kuzyna by zmazać skazę na rodzinie, czy nie widzisz powodu?"
* E: Czemu w ogóle miałbym go zabijać
* D: (patrzy jak na idiotę) zdradził Kidirona. Zdradził arkologię. Zdradził nawet Ciebie. Twojego Wujka. WSZYSTKICH. (patrzy z niedowierzaniem) wolałbyś go zniewolić neuroobrożą, dla mnie to gorsze, ale... 
* E: Może nic nie robić?
* D: Nie ma takiej opcji. Zdrajca dostaje kulkę albo obrożę. (już nie może doczekać aż wsadzi kulkę Laurencjuszowi Kidironowi)
* E: Historię piszą zwycięzcy, może jego forma buntu to forma lojalności?
* D: Czyli neuroobroża. Nie wiem, jak wolisz. (po chwili) Wiesz, jeśli to kwestia optyki, może mu się stać wypadek i może umrzeć broniąc swoich żołnierzy, wiesz, coś heroicznego zrobi. Ale Laurencjusz nie.
* E: Nie zdradził, próbował ugrać... stare się skończyło... może jakaś banicja, degradacja, pozbawienie wpływów...
* D: (z łagodną pobłażliwością) Eustachy, wiesz, że każdy banita to lord na wygnaniu jak jakaś inna Arkologia znajdzie okazję. Najlepiej wypalić od razu, bezboleśnie. (po chwili) Kidiron włożyłby mu coś do głowy i zrobił z niego agenta.
* E: Wrócimy do rozmowy, dobrze?
* D: Dobrze. Czyli... nie oczekiwać wypadku? Chcesz go żywego?
* E: Tak
* D: I mam _naprawdę_ się postarać by był żywy? Nie starać a wiesz, STARAĆ starać?
* E: TAK.

.

* Vz: Draglin nie będzie próbował eksterminować Tymona Korkorana. Więcej, spróbuje go wziąć żywcem.
* X: Draglin i Wujek mają kosmiczne starcie o to.
    * W: "to mój syn"
    * D: "widać jabłko spadło z gruszy". "Ty jesteś sensowny. Twój syn to gówno. I jak gówno skończy. Pytanie czy rozsmarowany na podeszwie, czy w śmietniku."
    * W: "jestem skłonny obrócić Infernię przeciw Arkologii jeśli mi go zabijesz, tam była czarodziejka mentalna!"
    * D: "i widać coś Ci zrobiła, skoro tak chronisz zdrajcę. Znasz zasady."
    * OBOJE SĄ MEGA OKOPANI NA POZYCJI
* V: Tymon nie zginie z ręki Czarnych Hełmów, Draglin szybciej zastrzeli swoich ludzi niż da im zabić Tymona. Dla Eustachego, dla Kidirona. I trochę dla Wujka. Trochę.

Draglin dostał wiadomość, że Czarne Hełmy osaczyły Tymona i zmusiły go do schowania się w Muzeum Kidirona. Czarne Hełmy zadbają, by Tymon nie miał drogi ucieczki. Draglin wezwał broń ogłuszającą jako wsparcie. Chce Tymona żywego. Wujek, CAŁKOWICIE nie ufający Draglinowi, idzie z Hełmami. Ma nadzieję, że Tymon się podda jemu.

* E -> T: Poddaj się, nie dzicz, nikomu nie stanie się krzywda
* T: Eustachy, jeszcze nie wygrałeś. Nie wygraliście. Jeszcze wygram! (on nie wierzy w to co mówi)
* E: (naświetla rozmowę co chcieli zrobić; nie spotka neuroobroży ani samobójstwa, Hełmy go wezmą żywcem, możesz wyjść i się poddać)
* T: Ty, Eustachy. Ty, znowu. Wpierw zabrałeś mi infernię. Potem - ojca. A teraz dzięki TOBIE mam być żywy? 
* E: Ja zagwarantowałem, że z mojej ręki nie stanie CI się krzywda i moi ludzie nie spowodują nieszczęśliwego wypadku. Jeśli jesteś na tyle słaby że nie potrafisz się z tym pogodzić i sam postanowisz odejść na własnych warunkach, kim jestem by Cię powstrzymywać. Ale Wujek nie byłby zadowolony z takiego obrotu sprawy. Tym bardziej, że ja starałem się zrobić wszystko by Cię nie spacyfikowali, on również. A że jestem obecnie małym dyktatorem arkologii, moje słowo stało się prawem. Więc ostatnim aktem wolności jaki możesz dostać nie podporządkowując się by wywalić ład... masz pistolet przy pasie.
* T: Czyli JA zapewniłem TOBIE dyktaturę? Zabiłem Kidirona i TY będziesz czerpał...
* E: Czemu myślisz że zabiłeś Kidirona?
* E: prowokuje go do powiedzenia: "to JA za wszystkim stałem, to JA za wszystko odpowiadam"

(+są emocje, +chce koniecznie pokazać swoją epickość, ENT: chce porozmawiać z ojcem .. init: Ex)

Tr +4 +5Og:

* Og (p):
    * Tymon wybuchł na wizji. "INFILTRATOR był konieczny! Nie chciałem tego, ale Kidiron MUSIAŁ umrzeć! Boże ci wszyscy niewinni (załamał)"
    * Tymon koniecznie, koniecznie chce się spotkać z Wujkiem i vice versa
    * E: "i ci wszyscy niewinni zginęli za darmo, bo Kidiron żyje, od POCZĄTKU byłem od Ciebie lepszy pod KAŻDYM względem, Infernia wybrała mnie i na moich działaniach ZAWSZE Arkologia wychodziła na plus, a jak Ty za coś się brałeś to Arkologia potrzebowała ratunku... lub na zero. Przez Twoją głupotę, pychę... spowodowałeś śmierć kilkudziesięciu osób, zniszczenia jakich koszt w miesiącach i latach a efekt działań jest taki że z Twoich założeń NIC się nie spełniło." MAKSYMALIZACJA TRIGGERÓW. +2Vg +3Og +5Or +3Or
* X: Wujek naprawdę chce uratować syna. "Nie zasłużył na TO, nie TAK"
* X: Tymon i Wujek zginą w płomieniach.
* Or: Tymon jest ZŁAMANY. Wujek też "Tymku, to nie była dobra droga, powiedz to naprawimy to wszystko, **dalej jesteś moim synem**.". Tymon zaczął mówić
    * T: "Tato... czemu wybrałeś JEGO nade MNIE. Starałem się. Naprawdę się starałem."
    * T: Infiltrator przyszedł z organizacji sprzymierzonej z Lobrakiem. I Laurencjusz w to poszedł od razu. Tymon najpierw nie chciał, ale potem chciał. Zwłaszcza, Egzotyczna Piękność.
    * T: Lobrak nigdy nie dowodził, zawsze dowodził Laurencjusz i Tymon, ale Lobrak dawał dobre rady.
    * T: Lobrak nie powiedział im co zrobi Infiltrator.
    * T: Laurencjusz zapewnił sprzęt i ukrycie bo Kidironowie mieli _master password_. Rafał Kidiron UFAŁ RODZINIE.
    * T: Lobrak sam zbliżył się do Laurencjusza a Tymon zawsze był blisko niego.
    * T: To wszystko to był plan Syndykatu Aureliona (czego Tymon nie wie, ale Wy wiecie)
    * T: (przekazał plany militarne, rozlokowanie itp)
    * T: nie wie co się stało piratom, ale był _relay_ który używał Lobrak. I to powoduje szaleństwo.
    * T: Laurencjusz ukrył, że do Arkologii weszło koło 50 osób spoza Arkologii, wszyscy pod dowodzeniem Laurencjusza i Tymona.
    * T: Lobrak (Laurencjusz) zaminował wiele ważnych obiektów, ale nie mają kluczowych bo Laurencjusz nie miał tam dostępu.

Wujek do niczego się nie nadaje. Skupia się na Tymonie. Draglin przejął dowodzenie nad siłami Inferni i wysłał Hełmy + Infernię na eksterminację piratów tych co szaleją.

* E: Moi piraci to czarodziejka mentalna, ona to zrobiła. To nie ich wina.
* D: Wiem. Ale popatrz. Czy moje Hełmy są kochane? Nie są. Ale wszyscy to co trzeba. Czy chcesz by Infernia pokazała "ojej, moi ludzie są inni i ich nic nie spotka". Ile osób w arkologii po tym wszystkim, po ZDRADZIE KORKORANA uwierzy w 'ojej czarodziejka mentalna'?
* E: Mam na to dowody
* D: Nawet Kalia nie da Cię rzędu dusz. Pozwól mi zrobić to co trzeba, wyczyścić robactwo, i Infernia będzie czysta i nieskażona. Ludzie będą ją kochać. A ja będę tym, kogo się boją. Jak chciał Kidiron. infernia ma być kochana i podziwiana a ci ludzie pod kontrolą mentalną niszczą tą piękną wizję.
* E: Pojmaj ich, nie zabijaj. Nasi ludzie, nawet sprowadzeni na manowce będą nam potrzebni. A w ramach odpokutowania grzechów będą pracować ciężej za mniej...
* D: Wyślę zatem Twoich ludzi z Inferni, acz wyposażę ich w ogłuszacze. 
* E: Wyślij swoim jako odwody jakby co. Problem z uratowaniem, zutylizujemy.
* D: Tak jest, komendancie.

TEST na ratowanie piratów. Eustachy dodatkowo tłumaczy im co się stało z towarzyszami broni. "ALBO się poddacie ALBO Was zniszczę".

Tp +2 (3Xg (straty u zdrowych) 2Xr (straty u Skażonych)) NEED: 3V:

* VzVV: UDAŁO SIĘ URATOWAĆ SKAŻONYCH PIRATÓW (bez 1 czy 2), ale bez strat. Draglin dobrze dowodził.

OSTATNIM co Wam zostało to Lobrak i Laurencjusz oraz ich ~20 żołnierzy. Większość z tego nie pochodzi z Arkologii. No i zaminowane obiekty.

Infernia -> Eustachy. Pokazała wizję Egzotycznej Piękności (Izabelli) leżącej z wydrapanymi oczami na ziemi. Pokazała Izabellę na pokładzie Inferni, z szmatką zasłaniająca oczodoły która klęka przed Eustachym.

* E: jeszcze nie pora na to
* I: nie chcę Twojej śmierci. Ona może Cię chronić.
* E: jestem uradowany faktem tej możliwości, ale mogę chronić się sam
* I: (obraz eksplodującej arkologii i Eustachego potrzaskanego poprzebijanego rzeczami)

Ostateczne negocjacje. Eustachy - Lobrak.

* L: Komendancie Eustachy Korkoran, nie doceniłem CIę
* E: Jak wielu Tobie podobnych
* L: Mnie podobnych czyli jakich?
* E: Nie jesteś pierwszą osobą, która mnie nie doceniła i musi przełknąć gorycz porażki. Ale zamotałeś najmocniej.
* L: To pozwól że wynegocjuję z Tobą warunki naszej współpracy. Oczywiście, z Tobą jako osobą wyżej postawioną. (uśmiech bez radości) Wiem, kiedy przegrałem. Nadal twierdzę, że bardziej opłaca się Tobie oraz Arkologii współpraca ze mną niż zabicie mnie. Jest powód, czemu Kidiron, świętej pamięci, ze mną współpracował.
* E: Dlaczego chcieliście śmierci Kidirona?
* L: Kidiron nie chce współpracować. Rafał Kidiron niestety był chętny do korzystania ze środków Syndykatu ale nie chciał zapłacić. (z lekką irytacją) Tłumaczyłem mocodawcom, że Kidiron i Wasza arkologia jest lepsza żywa i stabilna. Niestety, pojawiła się czarodziejka Izabella i nie miałem wyboru.
* E: Mamy czarodziejkę.
* L: To ją przesłuchaj. Zobaczysz że mówię prawdę. Pojawiła się... 
* L: (Eustachy pokazuje stan Izabelli) (Lobrak się wdrygnął) Czyli Tymon miał rację, z Infernią jest coś nie tak.
* E: To nie Infernia (Lobrak czeka. Eustachy uśmiecha się szyderczo)
* L: Porozmawiajmy zatem. Ty chcesz zdrowej arkologii która ma finanse, z zadowolonymi obywatelami, gdzie zdrajcy trafiają pod prawo arkologii, dobrze rozumiem? (pomocnie dodał: ja nie jestem obywatelem Arkologii więc nie dotyczą mnie jej prawa, nie mogę być zdrajcą)
* E: Możesz być szpiegiem
* L: (uśmiech) Jestem. Jestem agentem Syndykatu. Oczywiście, że jestem. Ale Syndykat da Ci to, czego potrzebujesz.
* E: Czego?
* L: Unikalnych części kontrolujących Infernię. Spokoju i wolności. Dobrej załogi. Robienia tego co chcesz.
* E: Cena?
* L: Syndykat szuka miejsca, które może W SOJUSZU współpracować z Syndykatem na Neikatis. Miała to być ta Arkologia. Pracowałem zarówno z Kidironem jak i nad Kidironem, ale Izabella... zmusiła mnie do działania wbrew mojej woli. (szeroki uśmiech) Chciałbym Ci pomóc, żeby Izabella trafiła pod Twoje skrzydła. Na nic innego nie zasłużyła. Mam tylko nadzieję, że WIE co się z nią dzieje (z jadem)
* E: Chcę się spotkać z Twoimi przełożonymi
* L: Nie ma problemu, mogę przekazać Cię do kogoś wyżej. ALe to znaczy, że musisz umożliwić mi kontakt. Bo... zagłuszacie mnie.
* E: Podaj pasma, hasła, skontaktujemy i przedstawimy i rozwiążemy problem. Syndykat chciał sojusznika i pozbyć się Kidirona, narobił szkód, sporo umoczyliście... w ramach nowego startu musielibyśmy wynegocjować dobre warunki.
* L: Oczywiście. Arkologia jest uszkodzona. Nie macie części, macie zatrutą żywność - nie całą - ale dość. Arkologia nie jest w stanie spełnić wymagań. A jednostka Syndykatu ma wszystko by się pojawić jako sojusznik i jako pomoc dla nowego reżimu. Z perspektywy Syndykatu Eustachy Korkoran jest lepszą opcją niż Laurencjusz Kidiron.

Ardilla i Eustachy się kłócą - czy to nie zniszczy arkologii? Jak to zrobić by ich przepędzić? Czy na SERIO ten sojusz ma sens? Ardilla nie chce dogadywać się z przestępcami. Z perspektywy Eustachego - albo jesteśmy młodszym partnerem biznesowym albo konkurencją na rynku. Chce mieć świadomość, że wchodzą w układ i planować co z tym robić dalej. Zdaniem Ardilli - "nie wchodzisz do kasyna, nie stracisz pieniędzy". Jeśli będzie przyczółek, będą temu współwinni. Zdaniem Ardilli - walczmy, by U NAS było jak najlepiej. Nie pogarszajmy sytuacji. Co te slumsy zrobią w szczurowisku jeśli arkologia wejdzie w sojusz z Syndykatem?

Wykres siła - uczuć Eustachego. (lojalność - miłość - strach; gdzieś tam, bliżej strachu). Ardilla: "jeśli się kogoś boisz i on odwraca wzrok, możesz coś zrobić. PRÓBUJ! Nasze szczury nie mają nic do stracenia!!!" Ardilla jest symbolem lepszego jutra, nadziei, nie-poddawania się.

* Eustachy: "musimy naprawić skutki ingerencji Syndykatu w arkologię i niekompetentnego dowodzenia Kidirona, na obecną chwilę mam świadomość że jestem w złej pozycji negocjacyjnej. Za dużo potrzebuję a możliwości zapewnienia przez Syndykat za kuszące. Nie podejmę tej decyzji, nie zatrzasnę tych drzwi, ale nie zostawię ich otwartych. Powiedz, gdzie są bomby i my je rozbroimy a swoim bossom przekaż, że jak pokaże się ktoś z Syndykatu w Arkologii niekoniecznie chcemy interesów. Musimy posprzątać bajorko."
* Lobrak: "czyli - my spokojnie opuszczamy Arkologię, rozbrajamy, dajemy Ci namiar i jesteś w stanie się z nami skontaktować. Dobrze rozumuję?"
* Eustachy: "ogarniemy nasz burdelik, a potem... potem się zobaczy"
* Lobrak: (z uśmiechem) "oczywiście, z przyjemnością. Rozbroimy, nie ma sensu uszkadzać przyszłego sojusznika. Czy pożyczysz Szare Ostrze, czy jesteś skłonny zezwolić na lądowanie naszej jednostki w Arkologii by nas ewakuowali?"
* Lobrak: (z większym uśmiechem): "zostawimy Wam w prezencie Izabellę. Zwłaszcza, jeśli mi obiecasz, że będzie cierpieć. Tak personalnie. Nie wiesz, ile pracowałem nad tym sojuszem. A ona wszystko zniszczyła."

EKSPLOZJA. Lobrak tak zaskoczony jak Wy. Muzeum Kidirona eksplodowało. 

Wiadomość od Draglina:

* D: "Eustachy?"
* E: "No?"
* D: "Kondolencje. Składam. Nie spodziewałem się, że Tymon się wysadzi."
* E: "Wysadził się?"
* D: "Mam nagrane. Nie miałem z tym nic wspólnego (szczerze)."
* D: "Wśród ofiar jest też jego ojciec."

(jego jednego Eustachy mu nie zabierze, zabrał wszystko inne)

## Streszczenie

Draglin zapędził Czarnymi Hełmami Tymona Korkorana do Muzeum Kidirona. Używając sił swoich i Inferni (pod rozkazami Eustachego) uratował piratów pod mentalnym wpływem. Negocjacje Lobrak - Eustachy pokazały Eustachemu, że nie są w stanie pokonać Syndykatu. Ardilla nie pozwala mu dołączyć Arkologii do Syndykatu - to nie to co powinno być. W chwili, w której Eustachy skonsolidował pełnię sił i mocy Tymon eksploduje wysadzając siebie i Wujka. Eustachy zostaje bez swojego sumienia, sam z Ardillą, Draglinem, Kalią i uszkodzoną Arkologią. Naprzeciw Syndykatu i Lobrakowi, który chce sojuszu.

## Progresja

* .

## Zasługi

* Ardilla Korkoran: przede wszystkim skupia się na Ralfie, monitoruje go i patrzy na Izabellę. Przez to przeoczyła sytuację z Wujkiem. Ale chroni Arkologię przed Eustachym, który próbuje wejść w sojusz z Syndykatem Aureliona. Pokazuje Eustachemu, że to nie jest dobra opcja. Jest ostatnim elementem sumienia Eustachego. Gdyby nie Ardilla to Lobrak by wygrał, bo Eustachy by oddał mu Arkologię.
* Eustachy Korkoran: próbuje uratować Tymona przed Draglinem, ale też łamie Tymona pokazując mu, że jest lepszy we WSZYSTKICH wymiarach i WSZYSTKIM co robi (dzięki czemu, pośrednio, Tymon się wysadził zabierając Wujka ze sobą). W porównaniu z Draglinem jest dobrą osobą z dobrym sercem XD.
* Marcel Draglin: brutalny ale skuteczny dowódca, słuchający poleceń Eustachego. Chce maksymalnej kary dla wszystkich zdrajców i chce by Infernia wyglądała na Źródło Światła I Dobroci. Niesamowicie kompetentnie przesuwa oddziały i próbuje osłonić Eustachego od okrucieństwa świata XD.
* Tymon Korkoran: naciśnięty przez Eustachego poza zakres maksimum, został złamany. Powiedział Eustachemu o wszystkich planach KiKo, po czym wysadził się z Wujkiem (swoim ojcem). "Zabrałeś mi wszystko, ojca Ci nie oddam". KIA.
* Bartłomiej Korkoran: częściowo Dotknięty przez Izabellę, częściowo żałujący swoich decyzji odnośnie Tymona, poszedł go przekonać. Tymon się wysadził, zabijając siebie i Bartłomieja. Bartłomiej MÓGŁBY może go powstrzymać... ale nie miał do tego serca. Jeden raz gdy potrzebował rodziny, został sam. KIA.
* Tobiasz Lobrak: ujawnił się Eustachemu jako aktywny agent Aureliona. Negocjuje z nim otwarcie, chce pomóc Arkologii Nativis - ale pokazuje, że potęga Aureliona i tak zmiażdży Nativis i wszystko inne.
* Izabella Saviripatel: żywa demonstracja dla Lobraka co Eustachy ma do dyspozycji; jest mentalnie w piekle
* OO Infernia: nadal kusi Eustachego; tym razem pokazuje mu Izabellę na kolanach przed nim, zrekonstruowaną mocą Nihilusa w posłuszną agentkę Eustachego

## Frakcje

* Infernia Nativis: Eustachy i Ardilla są jedynymi żywymi przywódcami frakcji. Ale Arkologia Nativis są pod zbrojną kontrolą Eustachego.
* NeoNativis KiKo: frakcja została zniszczona. KIA.
* Syndykat Aureliona Saviripatel: niedaleko, na orbicie; mają carrier-class i planują przechwycenie Arkologii Nativis. Stracili Izabelę, odzyskali Lobraka.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Nativis
                        1. Poziom 2 - Niższy Środkowy
                            1. Wschód
                                1. Centrum Kultury i Rozrywki
                                    1. Muzeum Kidirona: last stand Tymona Korkorana. Eksplodowało, gdy Tymon wysadził siebie oraz swojego ojca (Bartłomieja)

## Czas

* Opóźnienie: 0
* Dni: 1
