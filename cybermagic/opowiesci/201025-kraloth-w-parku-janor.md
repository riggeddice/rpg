## Metadane

* title: "Kraloth w parku Janor"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [201011 - Narodziny paladynki Saitaera](201011-narodziny-paladynki-saitaera)

### Chronologiczna

* [201011 - Narodziny paladynki Saitaera](201011-narodziny-paladynki-saitaera)

## Punkt zerowy

### Dark Past

Kiedyś:

* Krystyna Senonik była człowiekiem. Policjantka w Praszalku, dowodziła.
* Krystyna wykryła konspirację między działaniami Janora i Kamarona. Janor zagroził jej dziecku, bardzo poważnie i skutecznie.
* Krystyna zaczęła pozwalać na coraz więcej odnośnie nieetycznych działań Janora. He pushed her further and further.
* Krystyna odnalazła Kult Ośmiornicy. Kult dał jej opcję "nigdy nie będziesz się już wstydzić"
* Krystyna poszła w mrok. She corrupted her own child.
* Kaskadowo, Krystyna przejęła kontrolę nad parkiem i zaczęła gromadzić i tworzyć Kult na własną rękę.
* GDYBY NIE TO, że Hestia zgłosiła obecność Grzymościowca (gdy ten uciekał przed Korupcją), to nic by nie było wiadomo.

Niedawno:

* Grzymość wysłał swojego agenta, Agatona, by ten się dowiedział co tu się dzieje.
* Agaton i Pięknotka weszli w potężną walkę; Agaton padł, Pięknotka zginęła do Cienia.
* Kult jest coraz potężniejszy; w parku rozrywki inkarnował się kraloth.
* Minerwa poszła rozwiązać problem i znalazła "winnego" - to mag, Aleksander Muniakiewicz. Mag został oddany do Pustogoru.
* Kraloth nie był w stanie pokonać i złamać Minerwy, więc oddał jej Aleksandra.
* Na teren przybył terminus Maciej Oczorniak; delikwent wpadł w szpony kralotha.

### Opis sytuacji

* Maciej jest terminusem w szponach kralotha. Park korumpuje.

### Dark Future

* Mnóstwo ludzi wymrze i wyginie.
* Pięknotka będzie przyciągnięta do Kultu Ośmiornicy.

### Istotne Postacie

* Hestia d'Janor: TAI parku rozrywkowego. Zaniepokojona firefightem z GRZYMOŚCIOWCAMI - a dokładniej, Kamaronem.
* Adam Janor: właściciel parku rozrywkowego. Ustawiony jako mastermind przez Krystynę. Bardzo wpływowy człowiek.
* Krystyna Senonik: "nothing but my hate and carousel of agony". Janor zniszczył jej wszystko co miała, zmusił do mieszkania z nim. She found the Tentacle.
* Karol Senonik: dziecko Krystyny; 17 lat. "Każda laska jego". Kralotycznie urzekający.
* Maja Janor: ustawiona jako zabawka Karola przez Krystynę i samego Karola.
* Rafał Kamaron: agent Grzymościa (Wolnego Uśmiechu); skorumpowany przez Kult. Mag, potrafi walczyć. Atm unieszkodliwiony.
* Agaton Ociegor: chemia i negacja życia.
* Aleksander Muniakiewicz: kiedyś wesoły i charyzmatyczny arystokrata; teraz niebezpieczny i bez granic; conduit Kultu Ośmiornicy.
* Maciej Oczorniak: terminus, nie jest magiem pierwszego rzędu; wpadł w szpony kralotha.

## Misja właściwa

Pięknotka otworzyła oczy. To było ponad 2 tygodnie morderczego cierpienia, ciągłej pracy Lucjusza Blakenbauera, dekontaminatory, transfuzje, praca nad Wzorem i wiele, wiele innych rzeczy. Pięknotka nie czuje, że jej ciało należy do niej. Nie czuje się... sobą. Nie czuje się Diakonem. Ale mentalnie jest stabilna. Zobaczyła szczupłą twarz Lucjusza. Jej hipernet jest słaby - nienaturalnie słaby. Lucjusz wyjaśnił, że musiał ją odciąć od hipernetu. Pustogor jest zajęty rozprawą z mafią; nie mieli czasu na Pięknotkę. Ona jest na akcji, bo Lucjusz jej potrzebował do tematów okołokralotycznych i Trzewń wyraził zgodę.

Lucjusz wyjaśnił Pięknotce, że Saitaer przerobił Pięknotkę podobnie, jak Minerwę - ale tym razem zrobił to lepiej. Lucjusz doprowadził Pięknotkę do tego, że ona potrzebuje transfuzji raz w tygodniu - czystej, puryfikowanej krwi. Wyjaśnił jej, że jest w Rezydencji Blakenbauerów. Tu są zapasy tego typu krwi i stare maszyny, ale Lucjusz musi pozyskać więcej tego typu krwi lub osób do baterii.

Szczęśliwie, Pustogor jest w chaosie - nic nie zauważyli. Wojna z siłami Grzymościa jest bardzo duża. Lucjusz zauważył, że mógł ukryć nieobecność Pięknotki m.in. dlatego, bo oddał Agatona Ociegora, lekarza Grzymościa, jako osobę którą złapała Pięknotka _undercover_. Oddał go do Pustogoru.

Rezydencja Blakenbauerów jest w półmroku. Lucjusz jest w niej sam. Wskazał Pięknotce miejsce, gdzie ta może przetestować swoje nowe umiejętności. Ta sala wygląda jak miejsce wyjątkowo chorych eksperymentów. Pięknotka podczas testów doszła do realizacji:

* jest transorganikiem; nie jest czysto organiczną strukturą.
* Cień stał się jej częścią; jest w stanie emitować Cienia, włączać jego elementy.
* utraciła Esuriit z Cienia; nie ma przez to części destrukcyjnych możliwości.
* jest wykrywalna przez wyjątkowo czułe osoby - Kajrat, czy Liliana Bankierz.
* jej moc magiczna jest silnie powiązana z ixionem; ma niestabilność magiczną podobną do Minerwy.
* za to fizycznie... jest najsilniejszy, najszybszym i najodporniejszym terminusem jakiego ma Pustogor.

Pięknotka nie znalazła swojego limitu. Ale gdy szła "dalej", poczuła, że ixiońska część visiatu zaczyna adaptować Pięknotkę - więc musiałaby mieć kolejną transfuzję. Lucjusz zatrzymał Pięknotkę i jej wyjaśnił, że wszystkimi tymi transfuzjami próbuje zachować "słaby" komponent maga. Na pytanie Pięknotki odnośnie tego jakiego ona jest rodu, jaka Matryca kontroluje jej transformację, Blakenbauer się uśmiechnął. Pięknotka jest najbliżej rodu i matrycy Blakenbauer. Blakenbauerzy mają wieczne problemy w walce między dwoma formami; jego krew pomogła Pięknotce w stabilizacji visiatu i odepchnięciu ixiońskiej energii Saitaera.

Niestety, z uwagi na to, że Pięknotka nie żyje - Karradrael czy Arazille nie są w stanie jej "naprawić". Saitaer jest jedynym, co trzyma Pięknotkę przy życiu. Nie może się go pozbyć, nie może zmienić wzoru. To czym Pięknotka jest teraz tym zostanie - lub pójdzie jeszcze bardziej w ixion.

Lucjusz powiedział Pięknotce, że najsensowniej by było gdyby została jego narzeczoną - w ten sposób będzie uzasadnione, czemu Pięknotka mieszka w Rezydencji. CZEKAJ CO? Przecież raz na tydzień wystarczy! Owszem, ale Pięknotka nie chce być pod monitorami Pustogoru. Plus, jeśli Saitaer widzi oczami Pięknotki, ona nie chce pokazać mu Barbakanu i planów Karli, nie? Pięknotka podziękowała Lucjuszowi za ofertę; musi to przemyśleć. Dużo tego wszystkiego nagle. No i Erwin...

Pięknotka poszła do Minerwy. Minerwa też jest w Rezydencji. Zdaniem Pięknotki ta Rezydencja jest najsmutniejszym, najbardziej ponurym i najbardziej _creepy_ miejscem jakie kiedykolwiek widziała. I tak, Rezydencja żyje. Pięknotka to _czuje_. 

Gdy tylko Pięknotka spotkała się z Minerwą, poprosiła ją, by ta zajęła się Erwinem. Erwin nie może zostać z Pięknotką, nie teraz - nie wiadomo co by się stało. Minerwa podziękowała i powiedziała, że z radością zajmie się Erwinem. Ona go dalej kocha.

Minerwa zapytana o to, co z parkiem rozrywki, wyjaśniła co zrobiła. Był tam mag, Aleksander Muniakiewicz. Minerwa go przesłuchała, lekko torturując. Potem oddała do Pustogoru w imieniu Pięknotki. A co powiedział:

* chciał mieć ładne kobiety jako zabawki
* używał środków które pozyskał od ludzi Grzymościa, opartych na kralotycznych płynach
* kilkanaście osób było już dotkniętych; Minerwa WSZYSTKO przekazała do Pustogoru i wymusiła, by przysłali tymczasowego rezydenta

Poleciał tam terminus. Maciek Oczorniak. Pięknotka wie, że koleś nie jest zbyt kompetentny; kompetentnych rzucili na Grzymościa. Oczorniak jest dość młody i pewny siebie, ale bardzo uważny - patrzy, pilnuje, szpera. Pochopny ale niegłupi. A Pustogor miał pilnować jego linii 'sygnału'. Jakby coś się działo, ktoś ma przylecieć. Minerwa powiedziała, że rozpatrywany był Gabriel na tą akcję - ale podpadł Tymonowi. Pięknotka jest zdziwiona...

Zdaniem Pięknotki, coś mogło zostać. Minerwa powiedziała, że rozwiązała problem. Ale Pięknotka przypomniała jej, że to nie takie proste - był przecież ktoś przed kim uciekał Grzymościowiec; Pięknotka po prostu nie wierzy, że tak bezproblemowo to wszystko wyszło. Minerwa się zgodziła, że tak może być - ale w takim razie idzie z Pięknotką. Nie pozwoli, by Pięknotka poszła tam sama w stanie niestabilności ixiońskiej...

..."absolutnie NIE!" - Lucjusz Blakenbauer.

Pięknotka przekonała Lucjusza, że tam nic złego się nie dzieje. Przecież Minerwa dała radę. Lucjusz jest podejrzliwy, ale fakty są faktami - wierzy, że Minerwa dała radę. Z jakiegoś powodu jednak Lucjuszowi zależy na tym, by KTOŚ ogłosił z nim narzeczeństwo, co Pięknotkę nieźle rozbawiło. Na pytanie czemu potrzebna mu narzeczona, Lucjusz powiedział, że dzięki temu żadna kuzynka ani kandydatka nie pojawi się w tej Rezydencji - a ród może chcieć kogoś do niego wysłać. Jakby miał narzeczoną, nikogo nie wyślą.

Minerwa i Pięknotka awianem Lucjusza poleciały do Praszalka. Minerwa prowadzi. A raczej, ujeżdża płaszczkę. Like, seriously.

Środek białego, pięknego dnia. Park rozrywki wygląda ślicznie, wesoło i zabawowo. Gdzieś w okolicy kręci się lokalny terminus. Ogólnie - spokojny dzień w parku rozrywki. A Minerwa dalej ma kontrolę nad Hestią ;-). Pięknotka spytała Minerwę, czy dalej używają tych chorych środków chemicznych i co się działo ostatnimi czasy. (TrZ+3)

* Terminus jest bardzo częstym gościem parku rozrywki.
* Parkiem dowodzi nadal Janor; coś co Pięknotce się nie podoba to fakt, że jego córka jest praktycznie ODDANA Karolowi Senonikowi. I nie ona jedna.
* Terminus też dobrze się bawi przez obecność młodych ślicznych dziewczyn. "Zapomniał się".
* V: Hestia nie ma informacji o środkach chemicznych; jest całkowicie odcięta od sporej części parku. Kody kontrolne.
* XX: Druga strona wie, że ktoś tu szuka i grzebie. Monitorują Hestię. Wiedzą, że ktoś tu działa i szuka.
* V: Minerwa overridowała Hestię i wysłała drony. Zobaczyła, de facto, orgię. Nowe atrakcje są oparte o obszary erotyczne. Są dobrze ukryte. I akumulują magię.

Pięknotka zakłada, że terminus jest albo kupiony albo głupi. Co jest bardziej prawdopodobne? Bez połączenia z Pustogorem / hipernetem Pięknotka nie może łatwo tego sprawdzić; zadzwoniła do Lucjusza Blakenbauera. Lucjusz odebrał, lekko zmierzły. Pięknotka poprosiła o sprawdzenie gościa.

* V: Maciej nie jest magiem łatwym do kupienia. To ideowiec, i to uważny. Ani 'głupi' ani 'przekupny' do niego nie pasują.
* XV: Lucjusz jest bardzo zajęty, nie może teraz rozmawiać. Ale: nie, ten gość nie ma tendencji epikurejskich. To raczej wycofany, poważny mag.

Zdaniem Pięknotki jest czymś naszpikowany. Tamten Maciej zareagowałby z oburzeniem na takie rzeczy jak ten Maciej sam robi i na nie pozwala. Czyli mamy **kolejnego już** maga który działa nie tak jak powinien na tym terenie. Pięknotka zatem spróbuje obserwować maga z oddali. Zrozumieć plan dnia - co robi, kiedy, jak ogarnia teren... Pięknotka używa swojego nowego, lepszego ciała - włączyła osłonę Cienia, więc jest mniej widoczna. Plus, czysty węch.

* VVV: jej nowa bioforma działa. Wyczuła po zapachu. Maciej jest pod kontrolą kralotha. Na tym terenie jest kraloth. Pełnowartościowy cholerny kraloth.

I to wszystko tłumaczy.

Jedyna szansa Pięknotki jest taka, że i ona i Minerwa nie są normalnymi magami. Obie są ixiońskimi hybrydami. A Pięknotka wie przeciwko czemu walczy. I jest z urodzenia i wykształcenia Diakonem. Pięknotka ma plan. I jest to epicki, Pięknotkowy plan. PORWĄ Z MINERWĄ TERMINUSA I GO WYLECZĄ. Po to jest w końcu Lucjusz.

To jest terminus który jest ~10 dni na tym terenie. Kraloth nie chce zwracać na siebie uwagi, więc operuje w parku rozrywki. Nie powstrzymuje terminusa przed robienie tego co terminus zwykle robi. Plus, Minerwa nadal kontroluje Hestię. Pięknotka wpadła na pomysł - stworzyć małą efemerydę emocjonalną niedaleko domostw ludzkich. Coś niewielkiego co wymaga ingerencji ze strony terminusa. Bo jak nie to wzbudzi uwagę i kogoś tu mogą wysłać.

Czas przygotować efemerydę. Pięknotka woli zostawić to Minerwie. Minerwa inkarnuje jakiś porzucony rower; niech on stanie się nośnikiem efemerydy. Energia w okolicy pasuje. (TrM+2: XVVXXV). Minerwie udało się stworzyć pasującą efemerydę, która nie robi krzywdy, ale jest kłopotliwa. Gorzej, że nasyciła ją energią ixiońską; ten rower się wije i krzyczy, próbując przekształcić formę w bardziej ludzką. Nigdy nie był człowiekiem, ale ten rower "transferował" ze swojego poprzedniego posiadacza. Ku zdziwieniu Minerwy, która się tego NIE spodziewała, krzyczący rower odjechał w miasto. Ale doprowadzi terminusa do pułapki tu, poza miastem.

I faktycznie, terminus w swoim 'Lancerze' przyleciał dość szybko, goniąc ów efemeryczny rower. Nie jest super ostrożny; wyczuwa to jako jedyne zagrożenie. Nie jest zły i jest zdeterminowany by to rozwalić zanim ktoś z Pustogoru przyleci. Niestety, Minerwie udało się wprowadzić do roweru dominujący _strain_ ixionu - regeneracja, przeżywalność, odbudowa. Więc gdy dotarł do obszaru pułapki, Pięknotka stanęła przed decyzją - rozmontować efemerydę lub łapać terminusa. To nie była trudna decyzja.

Efemeryda: +3V. On się nie spodziewa: +2V. Pięknotka zna szkolenie terminusa i wie o nim: +2V. Wsparcie Minerwy (technomantka): +2V. Lancer: +3X. Kontrola terenu (ambush): +2V. No i Pięknotka używa formy Cienia: +3O.

* OO: Pięknotka przeszła w formę Cienia i poczuła, jak jej visiat zaczął adaptować i elementy saitis zaczęły nadpisywać jej zdrowe komórki. Zaatakowała od pleców, rozrywając 'Lancerowi' silniki skokowe. Nie ma jak zwiać łatwo. Lancer obrócił się, by Pięknotkę rozwalić, nie wiedząc czym ona jest; uważa ją za anomalię.
* XV: W chaosie i walce, efemeryda opuściła ten teren. Trzeba będzie ją kiedyś złapać. Kiedyś. Pięknotka w formie Cienia bez większych wysiłków rozbroiła Lancera; niewyszkolony terminus nie jest dla niej problemem.
* V: Z uwagi na teren, Lancer nie był w stanie użyć komunikatorów; został pilotowi tylko hipernet, ale nie chce go używać by nie ściągnąć Pustogoru. Lancer jest odcięty.
* V: Pięknotka odskoczyła; Minerwa strzeliła potężnym działem strumieniowym prosto w Lancera niszcząc mu obwody kontrolne. Zanim pilot zdążył coś zrobić, Pięknotka podskoczyła i wbiła weń swoje szpony. Wstrzyknęła odpowiednio zsyntetyzowane środki by delikwent stracił przytomność, biorąc poprawkę na kralotyczne środki.
* V: Z uwagi na formę Cienia i adaptację, Pięknotka zintegrowała się z wyłączonym Lancerem. Jej wewnętrzny reaktor zasilił serwomotory. Pięknotce jest trochę niedobrze, ale... działa.

Po wycofaniu się, Pięknotka nie ma pojęcia jak się wyplątać z Lancera, więc Minerwa wraca trójkę do Lucjusza Blakenbauera...

Na miejscu Lucjusz przywitał ją swojskim "Ty idiotko... kolejna transfuzja. Nie masz ich dużo..."

Po dniu ciężkiej pracy Lucjusz wyjaśnił terminusowi po przejściach że Pięknotka jest _undercover_, wyciągnęła go stamtąd i musi tak pozostać. On musi wezwać SOS z Pustogoru - muszą to rozwiązać, bo jest tam kraloth.

Pięknotki nic a nic nie zdziwiło, że oddziałem szturmowym przeciwko kralothowi dowodzi Alan...

## Streszczenie

Dużo ciężkiej pracy Lucjusza i Minerwy, ale Pięknotka wróciła do "normy", acz potrzebuje regularnych transfuzji. Pięknotka oddała Minerwie Erwina; sama myśli jak zamieszkać z Lucjuszem i nie udawać jego narzeczonej. Chciała skończyć sprawę w parku rozrywki; z Minerwą odkryły że za wszystkim stoi cholerny kraloth. Porwały terminusa przejętego przez kralotha by on wezwał SOS z Pustogoru.

## Progresja

* Pięknotka Diakon: koniecznie potrzebuje transfuzji puryfikacyjnej krwi raz na tydzień, pełna transfuzja. Pod opieką Lucjusza Blakenbauera.
* Agaton Ociegor: oddany przez Lucjusza Blakenbauera jako "sukces Pięknotki" jako dywersja do Pustogoru. Siedzi zamknięty w więzieniu.
* Roland Grzymość: zamknięty w Pustogorze, w więzieniu. 
* Minerwa Metalia: przejmuje Erwina Galiliena; ona nadal go kocha a Pięknotka jej go "oddała".
* Maciej Oczorniak: strasznie psychicznie uzależniony od kralotha; zdewastowany tym co zrobił i ogólnie biedny - złamał wszystkie swoje ideały. Pomoc psychologa w Pustogorze.

### Frakcji

* Wolny Uśmiech: siły Grzymościa są w otwartej wojnie z siłami Pustogoru. Organizacja jest praktycznie zdziesiątkowana.
* Kult Ośmiornicy: inkarnował się kraloth w parku rozrywki Janor; dał radę rozprzestrzenić korupcję.

## Zasługi

* Pięknotka Diakon: próbuje się odnaleźć w świecie, w którym jest nie-sobą. Znajduje z Minerwą kralotha w parku rozrywki i porywa zniewolonego terminusa używając nowej siły.
* Lucjusz Blakenbauer: naprawił Pięknotkę, używając matrycy Blakenbauer do kontroli sił Saitaera. Zaprosił Pięknotkę do Rezydencji; tu będzie bezpieczna.
* Minerwa Metalia: pomaga Lucjuszowi w rekonstrukcji Pięknotki, potem zastawia z Pięknotką pułapkę na kralotycznie zniewolonego terminusa i robi ixiońską efemerydę co krzyczy.
* Maciej Oczorniak: zniewolony przez kralotha młody, idealistyczny terminus; porwany przez Pięknotkę i naprawiony przez Lucjusza. Wezwał SOS do Pustogoru mówiąc im o kralocie.
* Hestia d'Janor: nadal pod kontrolą Minerwy; pomogła Minerwie zlokalizować kralotha.

## Plany

* Lucjusz Blakenbauer: szuka narzeczonej, by nie mieć narzeczonej z Rodu ani żadnej kuzynki czy coś. To JEGO Rezydencja i taka ma zostać.

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Jastrzębski
                            1. Praszalek, okolice
                                1. Park rozrywki Janor: zasiedlony przez kralotha z Kultu Ośmiornicy. Hestia dalej pod kontrolą Minerwy.
                                1. Lasek Janor: miejsce pułapki - efemeryda ixiońska by "Cień" porwał terminusa.
                        1. Powiat Pustogorski
                            1. Pustogor, okolice
                                1. Rezydencja Blakenbauerów: dom Lucjusza, ponury i ciemny. Toczą się tam prace nad Pięknotką, Kajratem i Amandą.

## Czas

* Opóźnienie: 16
* Dni: 2

## Inne

### Budowa sesji

#### Co się działo

* Zaczęło się od kombinacji Shame + Resentment. Próba wyjścia z tego stanu emocjonalnego doprowadziła do pojawienia się Kultu Ośmiornicy.
* W chwili obecnej Kult Ośmiornicy ma już swoje macki w miasteczku.
* W pewien sposób wyzwalaczem jest rozprzestrzenienie idei poza miasteczko.
* Hestia d'Janor jest TAI systemu rozrywkowego; jej przełożony jest już Kultystą.

#### Dark Future

* Park Rozrywki Janor stanie się stabilnym corruptorem okolicy.

#### Pytania

1. .

#### Dominujące uczucia

* .

### Komentarz MG

* Pokażę kralomut - człowieka / maga przekształconego daleko przez kralothy.
* Pokażę Kult Ośmiornicy, czemu jest groźny i do czego prowadzi.
