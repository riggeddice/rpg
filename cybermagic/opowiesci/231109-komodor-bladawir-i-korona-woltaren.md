## Metadane

* title: "Komodor Bladawir i Korona Woltaren"
* threads: triumfalny-powrot-arianny
* motives: energia-ixion, beznadziejny-szef, biznesowa-okazja, casual-cruelty, przeciwnik-terrorform, przemyt, uspiony-vicinius, oszczedzanie-na-wszystkim
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [231011 - Ekstaflos na Tezifeng](231011-ekstaflos-na-tezifeng)

### Chronologiczna

* [231011 - Ekstaflos na Tezifeng](231011-ekstaflos-na-tezifeng)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA: 
    * Nemesea "Believe"
        * "I still remember | The time you told me | That I would never | Climb too high on my own"
        * "Take a good look at what I've become | I don't need you to be proud | It took my strength to unlock my dreams"
        * Aneta Waltaren sama uzyskała wszystko. Ad astra. Elenie też nikt nie ufa.
        * Komodor Bladawir nie ufa Ariannie i tienom. Wykorzystuje Ariannę do działań punitywnych wobec Anety. Bladawir uważa ich za zbyt nieostrożnych.
        * Kurzmin korzysta z okazji by dać szansę wartościowym tienom.
* Opowieść o (Theme and vision): 
    * "Jak się posuniesz za daleko, COŚ Cię pożre i zniszczy, zwłaszcza w kosmosie"
    * 'Korona Waltaren' została Dotknięta przez ixion. Prefektiss wdarł się na pokład i zjadł Anetę.
    * Elena wyczuwa że coś jest nie w porządku, ale nikt jej nie ufa. NIKT. Zrobiła haję komodorowi i przez to Bladawir wysyła Ariannę. By 'posprzątała' po kuzynce.
    * SUKCES
        * uratowanie załogi 'Korony'
        * nauczenie młodych tienów czegoś przydatnego
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * energia-ixion: na tej sesji element ixioński zapewnia Prefektiss i dążenie do doskonałości ze strony głodnej sukcesu Anety
    * beznadziejny-szef: komodor Bladawir, który naciska na Anetę i gardzi przybyszami z Kolapsu. A potem na Ariannę i Elenę. Z jednej strony zrobił dobrze i miał rację, z drugiej bez niego to by się JESZCZE nie stało. Ale też komodor Walrond, który traktuje Elenę jak maskotkę.
    * biznesowa-okazja: Aneta, zdesperowana poszła za okazją przemytu czegoś w przestrzeni Orbitera. Ale naprawdę to prefektniss był PRAWDZIWYM towarem i źródłem.
    * casual-cruelty: Bladawir wobec wpierw Anety ("sprawdzajcie ją dokładnie, nie ufamy osobom z Anomalii") a potem wobec Arianny ("Twoja kuzynka spieprzyła, Ty to sprawdź")
    * przeciwnik-terrorform: odpowiednio dostosowany przez nanofara pacyfikator (którego za czasów Eleny jeszcze nie było)
    * przemyt: praprzyczyna wszelkich problemów. Aneta doprowadzona do ostateczności skupiła się na przemycie jako okazji pozyskania pieniędzy i części
    * uspiony-vicinius: Aneta, a raczej Prefektiss. Działa jak Aneta do momentu gdy już nie ma wyjścia i musi działać nieco inaczej.
    * oszczedzanie-na-wszystkim: 'Korona Waltaren' jest biedną jednostką i to widać. Średnia załoga, kiepski kształt, oświetlenie, stara Semla itp.
* Detale
    * Crisis source: 
        * HEART: "Obecność Prefektiss przy braku zaufania ze strony Orbitera"
        * VISIBLE: "Wszyscy zbierają się dookoła kapitana, nawet, jeśli to niebezpieczne"
    * Delta future
        * DARK FUTURE:
            * chain 1: Ofiary śmiertelne, zniszczenie 'Korony', ich trauma itp
            * chain 2: Trauma tienów i jeszcze gorsza opinia o Elenie "bo nie naprawiła"
            * chain 3: Chwała dla komodora Bladawira i Walronda za rozwiązanie problemu
        * LIGHT FUTURE:
            * chain 1: Aneta i Quasar są zniszczeni, też Nanofar. Załoga ewakuowana zanim do czegoś doszło.
            * chain 2: "bez Eleny byśmy nie wiedzieli" plus "wtedy jeszcze nie było tak źle"
            * chain 3: Komodor Bladawir miał rację. Ale Walrond powinien był Eleny posłuchać.
    * Dilemma: brak, sesja funkcjonalna

### Co się stało i co wiemy

* Przeszłość
    * Aneta, naciśnięta przez komodora, poszła by przetransportować coś z okolic Iorusa w okolice Neikatis. Dużo pieniędzy. Złapał ją Prefektiss
    * 'Aneta' rozłożyła nanofar w 'Sadzie' (Sad jest miejscem od zawsze w rękach rodziny Waltaren)
* Teraźniejszość
    * .

### Co się stanie (what will happen)

* F1: Demonstracja ruiny 'Korony Waltaren'
    * CEL
        * pokazanie komodora Bladawira jako 'small petty tyrant' - Infernia rozwiązuje 'problem' i dokumentacje na 'Koronie'
        * pokazanie ruiny 'Korony' i optymistycznej załogi wrogiej Orbiterowi i Ariannie
        * niech Klaudia i Arianna robią rzeczy jakich NIE CHCĄ. (potem Bladawir sprawdzi po nich i wyśmieje niekompetencję)
        * niech Arianna naprawdę nie cierpi Bladawira i uważa go za dupka
    * S2: Klaudia i Arianna na audycie
        * ludzie dookoła Anety ją chronią i wspierają
            * dzieciak chce zaatakować Klaudię / Ariannę. Aneta zatrzymuje
            * pierwszy oficer Owcarz 'na pewno jesteś z siebie dumna, TIEN.'
            * Aneta się nie da. Dumna.
        * rzeczy są nie tak zadbane jak powinny
        * statek w złym stanie
            * statek 'buczy' przez brak konserwacji silników
            * oświetlenie nie do końca; Semla też odpowiada powoli
            * recykling nie daje rady (acz załoga sama czyści)
            * jedzenie jest kiepskiej jakości
            * czasem wieszające się systemy nawigacyjne
        * audyt wykazuje problemy
            * zagrożenie środowiskowe i sanitarne
            * nie ma niektórych komponentów związanych z jednostką w pamięci Semli przez nietypową strukturę
            * systemy ratunkowe są nieodpowiednie do populacji
* F2: Terrorform na 'Koronie Waltaren'
    * CEL
        * sesja właściwa
        * delta (dark, light)
    * S0: Komodor Bladawir tłumaczy
        * Elenie Coś Nie Pasowało, rzucała czar, trzeba było płacić odszkodowanie. "Takiemu śmieciowi jak Aneta". Niech Arianna leci udowodnić że jest OK
        * Kurzmin: 'weź młodych, niech się przydadzą' i 'weź szybką korwetę 'Paprykowiec'' (współsponsorowana przez koncern Samszarów z Aurum)
    * S5: Zespół kontra Orbiter 
        * Wszyscy chronią Anetę i ją kochają. A to już nie Aneta.
    * stakes
        * czy będą duże ofiary? Czy wszyscy przejdą z tego bez szwanku?
        * czy Arianna i Elena będą miały dalej jakąś reputację?
    * opponent: 
        * Prefektiss, w ciele Anety i sterowany przez nich pacyfikator ixioński
            * wij który wykształcił szpony z kości Anety i metalu
        * Kwazar, pacyfikator - czarnowłosy pacyfikator - jest Skażony ixiońsko w terrorforma.
            * trzy Pacyfikatory: 'Zaćmiaczek', 'Mgławica', 'Kwazar'.
            * działa w próżni, destabilizuje TAI, zmienia kolor, transorganiczny pancerz i szpony, nadnaturalnie szybki i zwinny, Obraz Terroru (pokazuje wizję cierpiącej Anety by zrobić szok)
        * Załoga NIE CHCE współpracować z Infernią. Gardzi nią. Trzyma się razem
    * problem:
        * Anety nie ma. Jest tylko Prefektiss. Ale oni o tym nie wiedzą.
    * koniec fazy:
        * starcie z 'Anetą' i 'Kwazarem', zwłaszcza w świetle 
    * INSTANCES
        * pacyfikatory nie polują na anomalie (a Kwazara nie widać); są powycofywane i pochowane
        * Lidia nie chce bez kotków
        * ludzie nie ufają Orbiterowi; Elena uszkodziła Orchard. To kwestia 'religijna'
    * DARK
        * patrz: dark future
* F3: Debriefing
    * CEL
        * Bladawir chce zasługi dla siebie

## Sesja - analiza

### Fiszki

Zespół:

* Leona Astrienko: psychotyczna, lojalna, super-happy and super-dangerous, likes provocative clothes
* Martyn Hiwasser: silent, stable and loyal, smiling and a force of optimism
* Raoul Lavanis: ENCAO: -++0+ | Cichy i pomocny;; niewidoczny| Conformity Humility Benevolence > Pwr Face Achi | DRIVE: zrozumieć
* Alicja Szadawir: młoda neuromarine z Inferni, podkochuje się w Eustachym, ma neurosprzężenie i komputer w głowie, (nie cirrus). Strasznie nieśmiała, ale groźna. Ekspert od materiałów wybuchowych. Niepozorna, bardzo umięśniona.
    * OCEAN: N+C+ | ma problemy ze startowaniem rozmowy;; nieśmiała jak cholera;; straszny kujon | VALS: Humility, Conformity | DRIVE: Uwolnić niewolników
* Kamil Lyraczek
    * OCEAN: (E+O-): Charyzmatyczny lider, który zawsze ma wytyczony kierunek i przekonuje do niego innych. "To jest nasza droga, musimy iść naprzód!"
    * VALS: (Face, Universalism, Tradition): Przekonuje wszystkich do swojego widzenia świata - optymizm, dobro i porządek. "Dla dobra wszystkich, dla lepszej przyszłości!"
    * Styl: Inspirujący lider z ogromnym sercem, który działa z cienia, zawsze gotów poświęcić się dla innych. "Nie dla chwały, ale dla przyszłości. Dla dobra wszystkich."
* Horst Kluskow: chorąży, szef ochrony / komandosów na Inferni, kompetentny i stary wiarus Orbitera, wręcz paranoiczny
    * OCEAN: (E-O+C+): Cichy, nieufny, kładzie nacisk na dyscyplinę i odpowiedzialność. "Zasady są po to, by je przestrzegać. Bezpieczeństwo przede wszystkim!"
    * VALS: (Security, Conformity): Prawdziwa siła tkwi w jedności i wspólnym działaniu ku innowacji. "Porządek, jedność i pomysłowość to klucz do przetrwania."
    * Core Wound - Lie: "Opętany szałem zniszczyłem oddział" - "Kontrola, żadnych używek i jedność. A jak trzeba - nóż."
    * Styl: Opanowany, zawsze gotowy do działania, bezlitośnie uczciwy. "Jeśli nie możesz się dostosować, nie nadajesz się do tej załogi."
* Lars Kidironus: asystent oficera naukowego, wierzy w Kidirona i jego wielkość i jest lojalny Eustachemu. Wszędzie widzi spiski. "Czemu amnestyki, hm?"
    * OCEAN: (N+C+): bardzo metodyczny i precyzyjny, wszystko kataloguje, mistrzowski archiwista. "Anomalia w danych jest znakiem potencjalnego zagrożenia"
    * VALS: (Stimulation, Conformity): dopasuje się do grupy i będzie udawał, że wszystko jest idealnie i w porządku. Ale _patrzy_. "Czemu mnie o to pytasz? Co chcesz usłyszeć?"
    * Core Wound - Lie: "wiedziałem i mnie wymazali, nie wiem CO wiedziałem" - "wszystko zapiszę, zobaczę, będę daleko i dojdę do PRAWDY"
    * Styl: coś między Starscreamem i Cyclonusem. Ostrożny, zawsze na baczności, analizujący każdy ruch i słowo innych. "Dlaczego pytasz? Co chcesz ukryć?"
* Dorota Radraszew: oficer zaopatrzenia Inferni, bardzo przedsiębiorcza i zawsze ma pomysł jak ominąć problem nie konfrontując się z nim bezpośrednio. Cichy żarliwy optymizm. ŚWIETNA W PRZEMYCIE.
    * OCEAN: (E+O+): Mimo precyzji się gubi w zmieniającym się świecie, ale ma wewnętrzny optymizm. "Dorota może nie wiedzieć, co się dzieje, ale zawsze wierzy, że jutro będzie lepsze."
    * VALS: (Universalism, Humility): Często medytuje, szuka spokoju wewnętrznego i go znajduje. Wyznaje Lichsewrona (UCIECZKA + POSZUKIWANIE). "Wszyscy znajdziemy sposób, by uniknąć przeznaczenia. Dopasujemy się."
    * Core Wound - Lie: "Jej arkologia stanęła naprzeciw Anomaliom i została Zniekształcona. Ona uciekła przed własną Skażoną rodziną" - "Nie da się wygrać z Anomaliami, ale można im uciec, jak On mi pomógł."
    * Styl: Spokojna, w eleganckim mundurze, promieniuje optymizmem 'że z każdej sytuacji da się wyjść'. Unika konfrontacji. Uśmiech nie znika z jej twarzy mimo, że rzeczy nie działają jak powinny. Magów uważa za 'gorszych'.
    * metakultura: faerilka; niezwykle pracowita, bez kwestionowania wykonuje polecenia, "zawsze jest możliwość zwycięstwa nawet jak się pojawi w ostatniej chwili"

Castigator:

* Leszek Kurzmin: OCEAN: C+A+O+ | Żyje według własnych przekonań;; Lojalny i proaktywny| VALS: Benevolence, Humility >> Face| DRIVE: Właściwe miejsce w rzeczywistości.

Randomy z Castigatora:

* Marta Keksik: OCEAN: E+C-A- | Pełna pasji, rozwiązuje problemy siłą. | VALS: Power, Stimulation | DRIVE: Udowodnić swą wartość.
    * świetna duelistka, elegancko ubrana, zimne i stalowe spojrzenie i bardzo duża asertywność. Chroni brata i 'swoich'.
* Patryk Samszar: OCEAN: A+N- | Nie boi się żadnego ryzyka. Nie martwi się opinią innych. Lubi towarzystwo i nie znosi samotności | VALS: Stimulation, Self-Direction | DRIVE: Znaleźć prawdę za legendami.
    * inżynier i ekspert od mechanizmów, działa świetnie w warunkach stresowych

INNI:

* Antoni Bladawir: OCEAN: A-O+ | Brutalnie szczery i pogardliwy; Chce mieć rację | VALS: Power, Family | DRIVE: Wyczyścić kosmos ze słabości i leszczy.
    * "nawet jego zwycięstwa mają gorzki posmak dla tych, którzy z nim służą". Wykorzystuje każdą okazję, by wykazać swoją wyższość.
    * Doskonały taktyk, niedościgniony na polu bitwy. Jednocześnie podły tyran dla swoich ludzi.
    * Uważa tylko Orbiterowców i próżniowców za prawidłowe byty w kosmosie. Nie jest fanem 'ziemniaków w kosmosie' (planetarnych).

WOLTAREN:

* Krzysztof Woltaren: OCEAN: E-C+ | Optymistycznie podchodzi do życia; Zorganizowany i odpowiedzialny; Promuje pozytywne nastawienie | VALS: Self-Direction, Conformity | DRIVE: To NASZ statek 
* Lidia Woltaren: 
    * OCEAN: A+E+ | Empatyczna, łatwo nawiązuje kontakt; Energiczna, nie opuszcza jej entuzjazm; Pełna pasji w każdym działaniu | VALS: Benevolence, Hedonism | DRIVE: ?
    * dziecko, zajmuje się kotkami
* Tomasz Rewernik: OCEAN: A+N+ | Opiekuńczy; Niepokoi się o dobro załogi; Bardzo ostrożny | VALS: Universalism, Benevolence | DRIVE: ???

### Scena Zero - impl

Trzy miesiące temu.

Arianna jest na ZDALNYM spotkaniu z komodorem Antonim Bladawirem. Elegancko wygląda, w wyprasowanym mundurze, pod kancik.

* Bladawir: kapitan Verlen?
* Arianna: komodorze Bladawir?
* Bladawir: potrzebna jest inspekcja pewnej jednostki. Podobno masz kompetentną osobę od dokumentacji
* Arianna: mam, co mamy zrobić?
* Bladawir: jest jednostka cywilna. 'Korona Waltaren'. To jakaś korweta dalekosiężna, o bezsensownym modelu biznesowym.
* Bladawir: liczę, że Twoja ekspertka od dokumentacji zrobi odpowiedni raport i poradzi sobie z 'Koroną'. 
* Arianna: coś specyficznego? Podpadli w jakimś aspekcie?
* Bladawir: (spojrzał lekko zdziwiony i nieco się rozchmurzył) proszę... (lekko pokręcił głową) nie chcę śmieci w kosmosie. Ta jednostka to jednostka śmieciowa. Same z nią będą problemy. Sama prawda ich pogrąży.
* Bladawir: to stara śmieciowa jednostka dowodzona przez śmiecia. Dlatego mówię - samą prawdą wyczyścisz to z kosmosu.

Klaudia robi szukanie w dokumentach i o Bladawirze i o Koronie i relacji między nimi.

Tp +3:

* V:
    * Korona Waltaren jest statkiem który pochodzi z okolic AKolapsu. To nie jest statek Orbitera. Walteren to rodzina salvagerów. To jest statek rodzinny, zaadaptowany do życia w 'cywilizacji'
    * Korona jest statkiem ubogim. Dbają jak mogą - to ich dom - ale mają problemy finansowe. Nota bene, Bladawir na nich najeżdża. I zawsze płacą KARY.
    * Aktualnym dowódcą statku jest Aneta Waltaren. Jest uznana za dobrego dowódcę. Załoga strasznie związana.
    * Korona ma sad. Jedno pomieszczenie jest zaprojektowane jako sad. To jest 'od zawsze', ich rodzinne.
    * KANONICZNIE powinna być na 30-40 osób, ma 20 osób załogi (2 dzieci)
    * 'Korona' jest z AK, ale CHYBA była scramblowana przez Alteris. Jej struktura jest chora. Nie 'pomieszczenia' a wszystko inne
* V: 
    * Bladawir nie lubi: ANI 'arystokratycznych śmieci' ANI 'śmieci z AK' ANI 'śmieci z Aureliona'...
    * Bladawir w przeszłości stracił ludzi z oddziału którzy pomagali na jednostce z rodziny Waltaren. Ojca Anety.

'Korona'. Na pokład wchodzi Arianna, Klaudia i inżynier.

Zapach z tego sadu JEST. To NOWY zapach. Serio - mięty i jabłek. I trochę pleśniowato. Statek jest powyginany. Przywitał was młody człowiek. Na oko 16. "Tomek". Tomek prowadzi okrężną drogą. Arianna i Klaudia się przyglądają. Klaudia robi pokaz (zestaw przepisów i wymogów, wyciąga notes i zaczyna robić notatki). Przybiegły koty. I Lidia. Próbuje ratować sytuację. A zapach powinien być niwelowany.

W KOŃCU doprowadził do pani kapitan, na mostek. Spod mostka wydostał się pierwszy oficer. Aneta jest ubrana w najpiękniejszy mundur ever.

* Arianna: pani pokaże nam statek?
* Aneta: nie mamy nic do ukrycia.

Audyt... pokazuje że jest źle. Jednostce nic nie grozi, nie tak, że coś jest BARDZO nie w porządku ale jest jak w poprzednich audytach. Niedociągnięcia, rzeczy potencjalnie... np. nieaktualne medyczne rzeczy. Kapsuły ratunkowe itp też tragedia. Klaudia widzi SUPER WOLNĄ SEMLĘ. I raz na jakiś czas pacyfikator poluje na Ariannę - ale Lidia to blokuje. Czyli pacyfikatory SĄ WYCZULONE JEST ŹLE.

Klaudia robi pełen audyt i inwentaryzację, gdy Arianna i technik pomagają w sprawdzają rzeczy a załoga próbuje ukrywać.

Tp +3:

* V: Klaudia od ręki znalazła serię dewastujących rzeczy.
    * zagrożenie środowiskowe i sanitarne (LIFE-SUPPORT, do tego jest 'mechanizm recyklingowy' nie działa; używają jednej z kapsuł by wrzucać nadmiary; zgodnie z Semlą chcieli nabić filtry jak dotrą do Valentiny)
    * nie ma niektórych komponentów związanych z jednostką w pamięci Semli przez nietypową strukturę
    * systemy ratunkowe są nieodpowiednie do populacji
    * nawigacja się zawiesza
    * (oni wykorzystują flagę/banderę z Neikatis Orbiter ich nie może zatrzymać XD)
    * oni w ogóle nie chcieli wlecieć w sektor Orbitera, ale Bladawir SKIEROWAŁ was w tą stronę
* X: Klaudia jest w stanie POZORNIE bezpiecznie fałszować raport, ale ONA jest świadoma, że to wyjdzie
* V: Klaudia to cholera raportowa. Widać wyraźnie - część starych raportów nie ma wszystkich informacji, ale one ZAWSZE trafiają do kompletnego obrazu jednostki. Czyli ta jednostka ma dużo więcej audytów niż się wydaje. 
* V: Klaudia zebrała dane z systemów wskazujące na DEWASTUJĄCY stan jednostki. Ma czas życia około 3 lat jeśli nic się nie zmieni. Klaudia może walnąć im grzywnę 3x tego co do tej pory.

I co z tym zrobić? Arianna pogada z panią kapitan. Przedstawia jej rzeczy jakie Klaudia znalazła. Grzywna TYLE. Statek i tak za 3 lata idzie do kasacji...

Z klapki przy ścianie wypełza Lidia. Z kotem. "QUASAR BIERZ JĄ!". Quasar... nic.

* Aneta: Kapitan Verlen. Sytuacja nie wygląda dobrze, ale sobie poradzimy.
* Arianna: Fatalnie nawet do przetrwania waszej jednostki
* ANeta: Nie jest wszystko stracone. Jesteśmy o jedno dobre zlecenie od płynności i naprawy wszystkich luk. 
* Arianna: Jesteście w trakcie czy szukacie?
* Aneta: (wyważone spojrzenie) Znajdziemy niedługo. Nikt nie wierzył, że uda nam się opuścić Anomalię Kolapsu. Ten stan... to tylko przejściowy problem.
* Arianna: Trwa już rok.

...

* Aneta: mój ojciec, przy wszystkich zaletach, był dość złym człowiekiem. Nie dbał o życie ludzkie. My dbamy. Teraz to nasz statek. Przerywamy tą ciągłość. Wszyscy tutaj (pokazała załogę) CHCĄ by to było inaczej. Mój ojciec akceptował tego typu śmierci. Ja nie. Po prostu (głos załamał lekko) te grzywny nas zabijają.
* Arianna: gdzie teraz lecicie?
* Aneta: Valentina.

Klaudia i Arianna wymyślają CHORY SPOSÓB. Nie ma możliwości robienia grzywny JEŚLI są ratowani - czyli prosili o ratunek. TAK, będą musieli zapłacić za ratunek i części, ale to MNIEJ niż grzywna. A z Valentiny mogą uciekać gdzieś dalej. Klaudia do raportu przedkłada POTRÓJNĄ grzywnę z rozkazu Arianny (to pokaże komodorowi co i jak)

Tr Z +3:

* chain1: ukrycie śladów i uwiarygodnienie ('formalnie dobrze', 'dobrze dla komodora')
    * V: (1) FORMALNIE DOBRZE
    * Vr: (3) WYKORZYSTALI KRUCZKI NIC NIE DAŁO SIĘ ZROBIĆ XD
* chain2: żeby to było na tyle wolne żeby oni 'uciekli' grzywnie (plus: skasowanie krok dalej) ('uciekną', 'skasuje się grzywna')
    * Vz: (2) UCIEKNĄ przed grzywną
    * X: (4) Grzywna będzie w mocy ale oni wiedzą i mogą się przygotować.

EPILOG:

* Bladawir: _tien_ Arianno Verlen, jestem pod wrażeniem. (mlask mlask). Masz dobrą agentkę. Ale nie na tyle dobrą by przestępca nie uciekł!
* Arianna: nie byliśmy w stanie nic na to zrobić... przygotowali się na nasze przybycie
* Bladawir: nie drażni Cię, że śmieć z Anomalii wykorzystał nasze dobre serca i przechytrzył Twój statek?
* Arianna: co mam zrobić komodorze, nie będę ścigać, tu są ludzie warci uratowania a niewarte śmiecie nie...
* Bladawir: (lekki uśmiech) masz rację. Masz rację, śmieci nie warto ścigać. Ale jak tu wrócą, grzywna będzie czekać. I cieszę się, że znalazłyście co znalazłyście (okrucieństwo w oczach)

### Sesja Właściwa - impl

Chronologia aktualna. Castigator. Arianna i Infernia pomagają Kurzminowi. NADAL NIKT NIC NIE WIE bo Klaudia. Martyn desygnowany do pomocy psychologicznej. Leona - POLUJ! A jak nie było, stworzono jej coś. Klaudia otworzyła sakiewkę i porozkładała XD. Kamil od czasu do czasu puszcza jakieś 'kazania Arianny', Arianna ich nigdy nie nagrała, ale Kamil nagrywa to co Arianna mówi i wybiera co lepsze treści. Program "dobre słowo Arianny na dzień". Kurzmin się z tego leje. Arianna chce się schować pod ziemię. Leona to subskrybuje. Lars szuka w tym spisku - próbuje podzielić to, puścić od tyłu, SZUKA ZNACZENIA.

Arianna jest wezwana na mostek Inferni przez Klaudię. Komodor Bladawir ma do niej SPRAWĘ.

* Bladawir: kapitan Arianno Verlen, cieszę się, że jedna osoba z Twojej rodziny jest kompetentna. Mówię oczywiście o Tobie.
* Arianna: moja rodzina coś panu zrobiła? Dostał pan świnię w paczce?
* Bladawir: świnię? (zdziwienie)
* Arianna: just-verlen-things, u nas to działa trochę inaczej, świnia w paczce to objaw sympatii
* Bladawir: kapitan Verlen, nie jesteś śmieciem z Aurum tylko jesteś oficerem Orbitera. Tu nie mamy nic wspólnego ze świniami.
* Bladawir: chciałem jedynie powiedzieć, że... to Ty się wyrzekłaś swojej bezwartościowej kuzynki czy ona Ciebie?
* Arianna: jak sobie przypominam, ona mnie
* Bladawir: czyli ma trochę honoru...
* Bladawir: Twoja kuzynka napotkała 'Koronę Waltaren', może kojarzysz ten statek.
* Arianna: coś mi się przypomina. 
* Bladawir: to ci, co wykorzystali słabość i dobre serce Orbitera przeciwko nam.
* Arianna: możliwe że było. Co zrobiła Elena panie komodorze?
* Bladawir: weszła na pokład jednostki szukając... _anomalnego problemu_ (tu spoważniał). Gdy komodor Walrond kazał jej się wycofać, powiedziała, że coś jest nie tak.
* Bladawir: dobrze, że znalazła przemyt. Konfiskata i grzywna. Źle, że - wbrew zasadom Orbitera - zniszczyła im jakieś drzewo..? MAGIĄ! ORBITER! PŁACIŁ! ODSZKODOWANIE!
* Bladawir: nie posłuchała polecenia KOMODORA. Komodor dla takiego śmiecia z Aurum to jest jak bóg.
* Bladawir: KAPITAN VERLEN. Leć na tą cholerną jednostkę, na 'Koronę' i udowodnij swojej tępej kuzynce używając swojej dokumentalistki, że WSZYSTKO JEST W PORZĄDKU i... 
* Bladawir: (po chwili, gdy lekko opadł)... napraw to. Pokaż, że jesteś inna. Orbiterowi.
* Arianna: Dobrze komodorze (uśmiecham się uprzejmie)

Arianna BARDZO nie chce temu komodorowi wejść w drogę.

Problem w tym, że nie możesz oddalić Inferni za daleko, bo Admiralicja będzie mieć pytania. A Infernia jest SPECJALNA. Na szczęście Kurzmin rozwiązaniem.

* Kurzmin: Weź Paprykowiec.
* Arianna: Masz jakąś wolną jednostkę?
* Kurzmin: Paprykowiec jest korwetą, szybką, dalekosiężną, patrolowiec ratunkowy.
* Arianna: Do konserw?
* Kurzmin: Współinwestowany przez ród Samszar. Ten fragment rodu od rolnictwa. Stąd nazwa. Paprykowiec, bo są od papryki. (oczy martwego człowieka) To sprawna konserwa. Przepraszam - jednostka (poprawił się) (serio nie chciał)
* Kurzmin: To będzie potencjalnie niebezpieczna akcja?
* Arianna: Nie wiem co Elena znalazła. Ale jeśli nie słuchała rozkazów, to coś solidnego
* Kurzmin: ...albo uznała rozkazy za głupie.
* Arianna: To prawda, czyli coś znalazła. Nie ma zamiłowania do rozwalania losowych holograficznych drzew
* Kurzmin: ...co? Drzew? Na statku? Dlaczego na statku drzewa?
* Arianna: Nie wiem. Mają sad. Tak zwany
* Kurzmin: bogatemu wszystko wolno
* Arianna: no właśnie nie bardzo bogatemu - ostatnio ledwo finansowali statek
* Kurzmin: rozważ wzięcie dwóch moich. Martę i Patryka.
* Arianna: polecasz mi ich bo uważasz że mają kompetencje?
* Kurzmin: Eustachego nie weźmiesz, bo Infernia (on nie zna powodu). Patryk jest inżynierem. Marta natomiast... ma wyrzuty i strach (nie przyzna się), że gdyby była trochę lepsza, ona byłaby na miejscu Natalii.
* Kurzmin: Ale jeśli przewidujesz kralotha, to nie...
* Arianna: Nie przewiduję kralotha. Oby.
* Kurzmin: Jeśli weźmiesz Patryka, a nie Martę, dajemy jej sygnał, że w nią nie wierzymy. A że ona nie odwiedziła Twojego lekarza...
* Arianna: O...
* Kurzmin: Mhm. Ona taka jest.
* Arianna: Rozumiem
* Kurzmin: Jeśli poprawi Ci to humor, najostrzej ścierała się z Eleną.
* Arianna: To na pewno będzie... dobrze.

Paprykowiec. Arianna, Klaudia, Marta, Patryk i KTOKOLWIEK. Marta wchodząc na pokład, lekki uśmieszek 'co za idiota nazwał tą jednostkę...'. Patryk: 'ej, to moja rodzina, mój wujek to zaproponował!'. Marta: (roll-eyes) 'widać...' (z uśmieszkiem). Arianna: 'mój kuzyn nazywa się Marcinozaur i się z tego śmiejemy, nazwa to konsekwencje...'. Patryk ukłonił się Ariannie, Marta lekko prychnęła, nie dała rady sprowokować. Ale jest grzeczna.

Klaudia próbuje znaleźć dane o 'Koronie' z danych Eleny i Walronda

Tp +3:

* X: Komodor Walrond (ten od Eleny) myśli, że Arianna acessuje ten raport by chronić kuzynkę. Nepotyzm i pat pat pat. I powie o tym Elenie. Że to jej nie pomoże.
* X: Przez to co zrobiła Elena nie dało się walnąć grzywny. Aneta skutecznie obroniła potrójną grzywnę czynem Eleny. Bladawir jest wściekły na Elenę.
    * trasa 'Korony' jest z okolic Iorusa.
    * statek jest w lepszym stanie niż w raporcie Klaudii
    * zgodnie z raportem
        * Elena napisała 'coś jest bardzo nie tak ale nie wiem co to, ale to jest niebezpieczne, muszę znaleźć'
        * Komodor: 'czy to kraloth'
        * Elena: 'nie... nie ma tam Esuriit, wcale'
        * KOmodor: 'coś Ci się kojarzy, cokolwiek?'
        * Elena: 'nic. Nic nie czuję, nie wiem... (słyszysz zagubienie) jeśli rzucę zaklęcie...'
        * Komodor: 'nie zezwalam, nie czaruj, chodź na statek'
        * (Aneta): 'bardzo proszę nie czarować, zwłaszcza nie w tym SADZIE!'
    * 'zgodnie z informacją od Anety, WIĘCEJ ANOMALII NIŻ KIEDYKOLWIEK DZIĘKI PEWNEJ ADVANCERCE ORBITER PŁAĆ.'

Paprykowiec dociera do 'Korony Waltaren'. Korona czeka już na Orbiter, poinformowana wcześniej.

Na miejscu - zapach mięty i jabłek. Przynajmniej bez tej pleśni pod spodem. Tomasz i Lidia czekają. Marta zaczyna kaszleć. "co to..." Patryk "zachowuj się (cicho)" Marta (chciała coś powiedzieć ale nagle szczęka do ziemi). Lidia podeszła do Arianny i złapała za rękę i DZIĘKUJĘ ZA URATOWANIE STATKU. Marta ma szczękę przy ziemi, cicho ("chce pani kapitan chusteczkę dezynfekcyjną?"). Arianna patrzy na Martę z niezrozumieniem. Marta, cicho "dziwne dzieci o dziwnych pomysłach mogą mieć dziwne choroby". Patryk "JAKA ŚLICZNA DZIEWCZYNKA!"

Statek - jest TROSZKĘ INNY. Jest lepiej zadbany. Wyraźnie były tu naprawy, pieniądze. Znowu - nie jest to robota stoczni, czy czegoś zaawansowanego. Ale inżynierowie i substraty itp. Klaudia WIE że 'już grzywna będzie', ale nie musi o tym mówić. Nie jesteśmy na audycie. KOMODOR oczekuje że wejdziecie WSZYSTKO OK i wyjdziecie. Arianna ma punkty dodatnie. I komodor jest załamany przez Elenę. Arianna jako arcymag wyczuwa większą ilość pomniejszych anomalii niż ostatnio. Koty muszą być tłuste. Acz ich nie widać..?

* Arianna: Gdzie koty, nie przylecą przywitać?
* Lidia: (chce coś powiedzieć)
* Tomasz: (patrzy na nią pobłażliwie)
* Lidia: (zdeprymowana) nie, nie... kotki... nie, nie chcą.
* Arianna: coś im się stało?
* Lidia: nie, są osowiałe... takie... no, są zwykle w moim pokoju. (smutna)
* Tomasz: (wyjaśniająco) Pacyfikatory po tym jak Kwazar zniknął się zrobiły bardzo smutne. One po prostu źle... źle przeżyły stratę kompana.
* Lidia: (pokiwała główką)
* Arianna: byliście w dalszych terenach?
* Tomasz: to było gdy byliśmy przy Iorusie, gdy były naprawy. Musiał gdzieś biedak się zawieruszyć...
* Lidia: PORWALI GO! On by sam nas nie zostawił!
* Tomasz: no... to jest możliwe... (niechętnie) ale nie do końca wiem jak...
* Marta: pacyfikatory jako SPOŁECZNE koty? Pierwsze słyszę.
* Lidia: (płomienie w oczach) Kwazar mnie kochał! A Zaćmiaczek i Mgławica to ciężko przeżywają!
* Marta: ok, ok, nie przeczę (potrząśnięcie głowy, nie będzie się z DZIECKIEM kłócić), Patryk rzuca jej groźne spojrzenie

Mostek.

Aneta jest uśmiechnięta. Tym razem ma 'normalny' mundur. Jest czysty ale nie jest galowy. Nawigacja działa. Statek jakoś działa. Jest w lepszym stanie. Nadal ma 3 lata, ale nie jest bardzo problematyczny.

* Aneta: Kapitan Verlen, dziękuję za ostatnie
* Arianna: nie ma sprawy, niech lepiej mi pani powie co się stało przy drzewie
* Aneta: (zachmurzona), NIE TYLKO ONA. Pierwszy oficer PRÓBUJE NIE POKAZAĆ ale się nie cieszy.
* Aneta: czarodziejka Orbitera zniszczyła nam drzewo. Krzew (poprawiła się). Magią. Potem to... izolowaliśmy, grzebaliśmy w glebie...
* Arianna: możemy podejść do sadu i zbadać?
* Aneta: może nie..? (z nadzieją) mamy sad i niech zostanie.
* Arianna: ufam mojej kuzynce w sprawach...
* Aneta: KUZYNCE? (z niedowierzaniem)
* Wojciech: Aneto...
* ANeta: Nie, Wojtek. (patrzy na Ariannę) możemy zrobić coś innego? Nie chcę... oki, chcesz ją ochronić. Rozumiem. A możemy zrobić to inaczej? Naprawdę zależy nam na tym sadzie. To nasze dziedzictwo.
* Arianna: A jak jest tam większa anomalia? Obie wiemy jakie są podsystemy statku, dalej rzeczy nie są sprawne. Jeśli coś się wślizgnęło, tykająca bomba...
* Klaudia: Pacyfikatory się boją
* Wojciech: Aneto, rozważ. Ta agentka Orbitera jest inna.
* Marta: (cicho) że co? 
* Marta: (po chwili do niej dotarło) ELENA COŚ SPIEPRZYŁA? ZNOWU?
* Patryk: czy możesz, raz jeden, po prostu... nie wiem... nic nie mówić? Proszę?
* Aneta: Nie. Ten sad jest jedyną rzeczą jaką mam po DZIADKU. Zostaw go. Proszę.
* Wojciech: Dziadek by wolał bezpieczeństwa swojej rodziny.
* Aneta: (spojrzenie wszyscy przeciwko mnie). A możesz jakoś... zmniejszyć siłę tego czaru? Nie mamy silnych generatorów memoriam.

Sad. Arianna, Klaudia, Marta, Patryk, Aneta, Wojtek, LIDIA (która łazi za Anetą). Sad wygląda OBŁĘDNIE. Nie jest to wielkie pomieszczenie, ale jest trochę palmiarniowate.

* dziwne rośliny.
* ziemskie rośliny.
* holodrzewa, krzewy, nenufary

.

* Arianna: co wyczyściliście bo nie ma zapachu zgilizny?
* Aneta: nic, wyczyściliśmy. 
* Arianna: nie trzeba nic wymieniać
* Aneta: nic nie trzeba było wymieniać
* Wojciech: (patrzy na Ariannę ze zdziwieniem) skąd pytanie?
* Lidia: chodzi o nenufary?
* Arianna: Nenufary? W wodzie, więc wonieją
* Lidia: no bo ich nie było
* Wojciech: tak, ale to nie było pytanie. Pytanie było o czyszczenie. Lidio... (tonem 'dorośli rozmawiają')
* Aneta: Lidio, słonko, nic się nie stało. Nenufary, to nie są wodne, nie te. Te mamy stamtąd. Zainstalowaliśmy je w okolicach Iorisa.
* Lidia: są takie FAJNE! Da się w nich położyć!
* Arianna: A.

Klaudia chce zbadać nenufary używając SKANERA TKANKOWEGO. Czy jest tam coś magicznego, odchylenia magiczne itp.

Tr Z +3:

* XX: formy detekcji bez magii nie są w stanie Z PEWNYCH PRZYCZYN nic powiedzieć

.

* Aneta: 'to jest magiczne? Musimy dowieźć na Neikatis? To ładunek a nie chcemy zniszczyć.'

Elena zniszczyła krzew jabłoni który MIAŁ SZANSĘ. To jest KRZEW który robi OWOC i nazywają go JABŁONIĄ. Patryk patrzy na 'krzew jabłoni' i bardzo chciałby coś powiedzieć ale tego nie zrobi. WŁAŚNIE - co zrobili z odpadami?

* Aneta: odpadami?
* Arianna: tak, containowaliście
* Aneta: no, tak. Woda, zmieszać, zostawić... powinno wystarczyć, prawda?
* Arianna: gdzie ta woda?
* Aneta: (myśli) podlaliśmy kwiaty. Ale to nie było silne zaklęcie.
* Klaudia: też do nenufarów?
* Aneta: ...tak?
* Wojciech: to BYŁO silne zaklęcie
* Aneta: nie, nie było. No nic się nie dzieje.
* Wojciech: myślałem, że sad umrze. Oni też tak myśleli. Orbiter.
* Aneta: ale NIC SIĘ NIE DZIEJE. Energia nie mogła być silna... była... była w czarodziejce.

Arianno : Elena promieniuje, ale to jest obwód zamknięty przy słabych memoriam. TEN SAD NIE MA DOŚĆ ENERGII by pomieścić Paradoks Eleny.

Arianna sterowana przez Klaudię pod kątem energii i badań próbuje znaleźć to co containowało energię Eleny

(P-- : maskowanie, P++: arcymag, P: Klaudia, P: sympatia Eleny)

Tp M +3 +3Ob +3Og (ixion assault):

* Ob: Arianna ORAZ Klaudia skupiają się na energii. Na poczyszczeniu. Na piercowaniu. Na zrozumieniu. Pełen stupor, paraliż, fokus, energie. ZA DUŻE energie.
    * Elena: "WTF?!"
    * Arianna: "Elena?"
    * Elena: "Co... czego... jesteś... co Ty robisz? JAK? CZEMU!?" (słychać 'obolałość' w głosie)
    * Elena: "Nie wystarczy Ci, że psujesz moją karierę to jeszcze mi się wpierniczasz w środek pojedynku? WYNOŚ SIĘ! NIE WIDZĘ! NIE WIEM CO SIĘ DZIEJE!"
    * Arianna: "Chciałabym ale nie jestem w stanie, sprawdzałam tą anomalię..."
    * Elena: "Powiedz, że minęła jedna milisekunda, że czas płynie inaczej."
    * Klaudia: "Nie wiem"
    * Elena: "A TO CO?!"
    * Arianna: "Klaudia, moja oficer łączności"
    * Elena: "WOOOOW, użyłaś oficer łączności by mi przeszkadzać, WOW!"
    * Arianna: "ZAKLĘCIE POŁĄCZY BY PRZESZKODZIĆ W POJEDYNKU"
    * Elena: "Nie dlatego. Nawet się zainteresowałaś co robię, po prostu... o, Elena, mam do niej pytanie nawet NIE WIEM CO ROBI ALE SIĘ BĘDĘ WPIERDALAĆ LOSOWĄ OFICER ŁĄCZNOŚCI!"
    * Arianna: "jesteś zajęta i nie chcesz rozmawiać... nie poradzę że sytuacja inaczej..."
    * Elena: "WYJDŹ. Z MOJEJ. GŁOWY!" /Elena amplifikuje magię
* Og: ATAK KWAZARA!
* X: 
    * Marta ciężko ranna, Lidia też.
* V: 
    * Arianna poczuła krew na ustach. Nie swoją. Otworzyłaś oczy. Link z Eleną się nie zerwał ale się przekształcił.

Na ziemi leży ciężko ranna Lidia. Lidia w szoku tylko szepcze 'Kwazar...'. Marta też krwawi i ona broniła jak mogła. Krew Marty obudziła Ariannę.

Pacyfikator... nie, _TERRORFORM NA PODSTAWIE PACYFIKATORA_ leci w stronę Arianny ze szponami, drenując energię. Arianna próbuje rozwalić fragment zraszacza by potraktować Terrorforma...

Ex +3:

* X: Pancerz--. Terrorform spenetrował pancerz
* X: Terrorform da radę Ariannę ZRANIĆ (lekka rana, pójdzie krew)
    * KLAUDIA rzuca czymś co ZMIENIA PRZESTRZEŃ. Anomalia Klaudii. (+3Vz +3Ob X -> Xz)
* Vr: Anomalia Alteris przesunęła przestrzeń, wszystko się 'poszerzyło' tymczasowo (superbańka). 
* Ob: Anomalia Alteris zaczęła łamać pomieszczenie, sad na kawałki. Rysy, pęknięcia, woda tryska.
    * Nenufar nie jest nenufarem. To jakaś forma transorganicznego fabrykatora. Też mimiczny.
* Ob: 
    * Anomalia Alteris wchodzi w interakcję z 'nenufarem ixiońskim' i nanofar zaczyna się rozprzestrzeniać dramatycznie.
* X: I NA TO anomalia się zapada. Mamy pomieszczenie pełne nanofara, ixioński terrorform który znika i jest niewidoczny, ciężko ranną Martę na ziemi, Patryka który próbuje, Wojtka chroniącego Lidię, Anetę która dochodzi do siebie.
    * Ten statek raczej nie przetrwa.

Klaudia chce rzucić zaklęcie. Terrorform znika.

Patryk bierze broń i zaczyna ostrzeliwać. COŚ. Jest przestraszony ale nie jest spanikowany. Patryk próbuje osłaniać Ariannę ogniem zaporowym.

Ex+2:

* Vr: Terrorform ŹLE ZROZUMIAŁ sytuację i faktycznie skupił się na Patryku. Patryk widząc zarys terrorforma zamiast walczyć i być rannym zaczął SPIEPRZAĆ. Potknął się. Terrorform nie trafił. Patryk żre gruz. Ale Arianna ma czas na czar.

Arianna uruchamia zaklęcie Animacji. Arianna CHCE potężną Entropię. Przyzywa pełnię energii. Ożywić wystające elementy, by dały radę.

Tr M +3 +5Ob:

* X: Ożywiająca energia Arianny rozprzestrzenia się na cały statek. 'Korona' jest zgubiona.
* Ob: Rozrywająca energia Animacji Arianny animuje WSZYSTKO. CAŁY STATEK. Cały statek staje przeciwko sobie samemu. To miesza sygnały perfekcyjnej adaptacji.
* Vr: Arianna KUPUJE czas by terrorform nie zdążył dorwać wszystkich i by nanofar nie rozprzestrzenił się poza pomieszczenie.
* Vb: Arianna ANIMOWAŁA WSZYSTKO. WSZYSTKO ODPIERA IXION.
    * Krzyk Anety. Niewyobrażalny.
    * Aneta WYRWAŁA Z SIEBIE CZERWIA

Klaudia odciąga Lidię i ją owija by nic nie wyleciała i wytarguje. 

Tr +3:

* X: Upiorny krzyk. Wojciecha dotknął nanofar i jego ręka ulega przekształceniu
* Vr: Klaudia osłaniana przez Wojciecha owinęła Lidię i ją wyciąga (za 'kokon').
* V: Klaudia AMPUTUJE rękę. Wojciech pada na kolana. Z ręki krew. Nanofar pochłania rękę.
* X: Terrorform skutecznie rzucił się i zniszczył baterię. Servar wytrzyma TYLKO do wyjścia z pomieszczenia.
* V: Wyciągnięta Marta.

Z terrorformem walczy Aneta. A raczej jej zwłoki. Ona ANIMUJE statek w imieniu rodu.

Ciężko ranny Wojciech, wyciągana Marta, Patryk, któremu NIC NIE JEST i który przejmuje 'kokonki' i... statek. 

ALARM DEKOMPRESYJNY! Klaudia włącza alarm i Semli rozkaz ewakuacji.

Tr +3:

* V: zrozumieli, większość załogi się ewakuowała z pomocą Statku
* X: są ofiary śmiertelne

Paprykowiec zamknął śluzę i się oddala od umierającej Korony. W otwartej śluzie Korony pojawia się terrorform. Skacze. 

Tr

* X: Terrorform PRÓBOWAŁ wyskoczyć i złapać. Ale 'Aneta' złapała kociaka. Terrorform posiekał ciało na kawałki. Ale statek został w kosmosie a Paprykowiec jest na tyle szybki, że terrorform stracił swoje okno.

Paprykowiec, z przeładowaną załogą, wraca na Castigator...

EPILOG / DEBRIEFING

Komodor Bladawir i Arianna.

* Bladawir: Kapitan Verlen, doskonała robota. Udało Ci się udowodnić, że miałem rację, że Cię tam wysłałem.
* Arianna: (uśmiech)
* Bladawir: Dokonałaś więcej niż myślałem. Zrehabilitowałaś kuzynkę (on jest przekonany że zrobiłaś to specjalnie). Do tego, komodor Walrond oraz ja jesteśmy w stanie wykazać to jako sukces Orbitera. W końcu, gdyby nie on, nigdy nie wiedzielibyśmy o zagrożeniu. Dobra robota, pani kapitan.
* Arianna: Doskonała robota dwóch zgranych komodorów
* Bladawir: Udowodniłaś mi też, że nawet śmieć... nie, że nawet w Aurum, wśród śmieci, są diamenty. A załoga 'Korony' trafi w okolice Anomalii, gdzie ich miejsce. Będziemy łaskawi, uznając te problemy za działania pani kapitan. Wyślemy ich na nasz koszt.

## Streszczenie

Komodorowi Bladawirowi podpadła "Korona Waltaren" i wysłał Ariannę z zespołem na inspekcję. Podczas inspekcji Arianna i Klaudia odkrywają różne problemy, w tym zagrożenia środowiskowe i sanitarne. Mimo problemów, załoga broni Anety i staje za swoim statkiem; kolejna grzywna może "Koronę" utopić. Arianna opracowuje plan, który pozwoli statkowi uniknąć grzywny przez złożenie prośby o ratunek. Kilka miesięcy później jednak Elena szukając kralotha wykrywa coś dziwnego na tej jednostce, ale nie umie tego nazwać. Okazuje się, że na "Koronie" która była niedaleko Iorusa zalągł się terrorform ixioński i gdyby nie poświęcenie młodej tienki, Marty, Zespół mógłby zginąć. Zespół ewakuuje Koronę (która zostaje porzucona) i mimo wielu rannych udaje się większość uratować. Bladawir jest zadowolony - ałoga 'Korony' trafi w okolice Anomalii, 'gdzie ich miejsce'.

## Progresja

* Elena Verlen: potężny opieprz od komodora Walronda, który uważa ją za jakąś maskotkę. Elena ma to gdzieś.
* Marta Keksik: bardzo ciężko ranna w wyniku ataku terrorforma. 2 tygodnie w infirmarium, bo ixiońska infekcja. Do tego kolejny szok - wpierw alteris, teraz ixion. I 'dorośli' nie dali rady.
* Feliks Walrond: dzięki Elenie i jej zniszczeniu 'drzewa' miał problem. Dzięki jej wykryciu terrorforma ixiońskiego miał bonus. Wdzięczny Bladawirowi i w sumie wyszedł do przodu.
* Antoni Bladawir: pełnia zasług za to, że skutecznie ograniczył problem ixiońskiego terrorforma na "Koronie Woltaren". Zgodnie z papierami, 'miał rację' doczepiając się do tej jednostki.
* Wojciech Mykirło: skończył ciężko ranny dotknięty przez nanofar ixioński gdy Klaudia urżnęła mu rękę by go ratować.
* Lidia Woltaren: BARDZO ciężko ranna w wyniku ataku terrorforma; co najmniej miesiąc w laboratorium naprawczym z uwagi na infekcję ixiońską wywołaną ranami terrorforma. Ale kto za to zapłaci?

## Zasługi

* Arianna Verlen: zadowala Bladawira (potrójną grzywną) jak i ratuje Koronę ('proście o ratunek'). Nie chce mieć wroga w Bladawirze. Gdy Elena wykryła coś na 'Koronie', Arianna potraktowała to poważnie. Zauważa, że Lidia jest smutna i nie ma z nią kotów - WIDZI że coś jest nie tak. Gdy pojawia się terrorform ixioński, rozpętuje pełną magię by tylko kupić czas. Dark-Awakened Anetę Woltaren, która wyrwała Prefektissa z siebie i dowodziła ewakuacją jednostki. BARDZO bliska śmierci na tej akcji.
* Klaudia Stryk: skanując dane 'Korony' odkryła, że ta ma jakieś 3 lata życia jak nic się nie zmieni. Znalazła hazardy godne 3-krotnej grzywny (co zrobiła z rozkazu Arianny) ale też (z rozkazu Arianny) przeszła biurokracją by oni mogli uciec przed grzywną. W samej akcji z terrorformem użyła Anomalii alteris, rozpychając rzeczywistość (i poważnie uszkadzając Koronę). Gdy Wojciecha dotknął nanofar i zaczął ixiońską infekcję, nie zawahała się - ucięła mu rękę, ratując mu życie.
* Elena Verlen: wykryła 'coś' na Koronie Woltaren (ale nie kralotha; tam był ixioński pacyfikator-terrorform). Pewna że coś jest nie tak zignorowała polecenie kmdr. Walronda i rzuciła czar, uszkadzając 'drzewo'. OPIEPRZ.
* Leszek Kurzmin: w dobrej wierze dał Ariannie na akcję Patryka i Martę, bo są kompetentni i mogą coś fajnego zrobić. Prosta akcja. Nie wiedział, że wysyła ich na terroforma ixiońskiego XD. Zaproponował Ariannie korwetę 'Paprykowiec'.
* Marta Keksik: uważa Elenę za bezużyteczną i nie boi się o tym mówić - chce być LEPSZA. Ma lekceważący ton ale słucha się hierarchii. Jednak gdy atakuje terroform a Arianna i Klaudia są w stuporze, Marta odważnie stawia mu czoła i kończy bardzo ciężko ranna.
* Patryk Samszar: niekompetentny w walce i TYLKO DLATEGO PRZETRWAŁ starcie z terrorformem, ale chętny i nieco _zbyt_ stabilny emocjonalnie. Zafascynowany Lidią ("jaka śliczna dziewczynka!"). Wykonuje robotę tak dobrze jak się można spodziewać po młodym tienie w kosmosie; o dziwo, faktyczny asset XD.
* Aneta Woltaren: świetny dowódca 'Korony', broni się przed grzywnami Orbitera i organizuje załogę dookoła siebie. Wyciągnęła ekipę z Anomalii Kolapsu chcąc dać im lepsze życie. Niestety, przez Bladawira była zmuszona do pójścia w stronę bardzo ryzykownego kursu na Iorus i tam dorwał ją Prefektiss. KIA. Reanimowana przez Ariannę, echem zgromadziła 'Koronę' dookoła siebie by walczyć z ixionem i kupić czas i nadzieję załodze.
* Krzysztof Woltaren: pierwszy oficer Korony Woltaren o nastawieniu technicznym, chroni Anetę przed problemami z audytami. Po śmierci Anety i zniszczeniu jednostki, on został dowódcą zgrai która była 'Koroną Woltaren'.
* Wojciech Mykirło: załogant Korony Woltaren zajmujący się sadem i biologicznymi komponentami. Chce współpracować z Orbiterem, uważa że to da więcej sukcesów. Gdy Lidia była ranna, zasłonił ją przed nanofarem i terrorformem.
* Lidia Woltaren: młoda (14?) załogantka 'Korony' opiekująca się Pacyfikatorami. Narwana i ekspresyjna, łazi za kotami w kocie zakamarki i podziękowała Ariannie uściśnięciem dłoni za 'ratunek statku'. Zaatakowana przez sterrorformowanego Pacyfikatora Kwazara, skończyła ciężko ranna na ziemi. Przeżyła tylko dzięki ixiońskiej infekcji.
* Tomasz Rewernik: młody (17?) załogant 'Korony', przywitał audytorów i próbuje być strasznie poważny. Tak bardzo próbuje być poważny, że wpada w śmieszność. Dowód, że 'Korona' jest jednostką rodzinną.
* Antoni Bladawir: komodor. Tępił 'Koronę Woltaren' dla prywaty i traktuje nie-Orbiter jak śmiecie. W końcu 'Korona' miała problem z terrorformem i wysławszy tam Ariannę - jego przypuszczenia się spełniły. Mściwy i niemiły.
* Feliks Walrond: komodor szukający kralotha; zignorował żądanie Eleny że tam coś jest. Potem poważnie Elenę opieprzył bo przez nią musiał płacić odszkodowanie. Dzięki Bladawirowi, wyszło na jego.
* OO Paprykowiec: patrolowiec ratunkowy Castigatora. Jednostka szybka i dalekosiężna. Współinwestowana przez ród Samszar (stąd nazwa). 
* SC Korona Woltaren: korweta ultradalekosiężna o obniżonej załodze, powstała jako jednostka dotknięta alteris w Anomalii. Ma 'sad' z przyczyn historycznych. KIA; ixioński terrorform plus Arianna zmienili ją w derelict.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 5
* Dni: 7

### Kształt Korony Waltaren

Endurance > Speed > Utility > Armour > Guns

korweta dalekosiężna, 20 osób (oczekujesz: 40)

* Przednia Część:
    * Mostek: Miejsce komendy statku z konsolami nawigacyjnymi, systemami obronnymi i komunikacyjnymi.
    * Centrum Komunikacyjne: Z antenami i urządzeniami do komunikacji dalekiego zasięgu.
    * Strefa Relaksu: Miejsce wypoczynku z systemem rozrywki.
    * Kuchnia i Jadalnia: Przystosowana do gotowania i spożywania posiłków w warunkach mikrograwitacji.
    * Działko laserowe
    * AI Core: Semla. Kiepska.
    * Magazyny
* Środkowa Część:
    * Moduły Mieszkalne
    * Indywidualne kajuty dla załogi.
    * Łazienki
    * Kabina kapitana.
    * Żywność
    * Medical
    * Sala Gimnastyczna
    * Magazyn Uzbrojenia
    * Śluza główna
    * Orchard / "Sad": hydroponika; unikalna cecha
    * Life Support
* Tylna Część:
    * Magazyny
    * Zapasy wody, paliwa...
    * Maszynownia
    * Segment Napędowy: Główne silniki statku oraz systemy wsparcia życia.
    * Sala Techniczna: Dla napraw i konserwacji sprzętu w warunkach kosmicznych.
    * Pomieszczenie Awaryjne: Z kapsułami ratunkowymi i innym sprzętem awaryjnym.
    * Doki Serwisowe
