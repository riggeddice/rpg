## Metadane

* title: "Nie nazywasz się Janusz"
* threads: neikatianska-gloria-saviripatel, tarcza-nox-aegis
* motives: dreszczowiec, broken-conditioning, dotyk-saitaera, machinacje-syndykatu-aureliona, przeciwnik-terrorform, zaraza-powoli-infekujaca, dark-technology
* gm: żółw
* players: kić, flamerog

## Kontynuacja
### Kampanijna

* [230810 - Nie nazywasz się Janusz](230810-nie-nazywasz-sie-janusz)

### Chronologiczna

* [230719 - Wojna o Arkologię Nativis - Nowa Regentka](230719-wojna-o-arkologie-nativis-nowa-regentka)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * "Delain - Invictus"
        * Undaunted, in the end I'll be standing no more sorrow
        * I'm watching far below. Soaring pride is your downfall
        * Your cunning collective, fierce today but gone tomorrow
    * "Equilibrium - Eternal Destination"
* THEME & VISION:
    * All you know is a lie
    * There are nightmares greater than nightmare...
* CEL: 
    * Highlv
        * ?
    * Tactical
        * ?

### Co się stało i co wiemy

* Gracze
    * Kić: CORRUPTED Persefona połączona z Łucją; jej oczy to Elainka.
    * Flame: Janusz Lemuel, agent Inwazji Aureliona
        * janczar, odebrany z domu rodzicom, conditionowany i indoktrynowany by został maszyną zniszczenia; jego rodzice mieli neuroobroże za niespełnienie misji Syndykatu
        * medal za uratowanie Dariusza Avatrinna, użył granatu dymnego i wbiegł pod ogień by go wyciągnąć, deaktywując jego serwopancerz i biorąc ogień na siebie
        * powiązany z elitarnym Regimentem Czarnej Róży - Leon snajper z ogniem zaporowym i Krzysztof z granatnikiem vs gniazdo (zniszczony przez Janusza pod kontrolą Łucji)
        * narzeczony Mai Nerwial, Maja przechwyciła dziewczynkę, Janusz dowiedział się że rodzice sprzedani za długi. Otoczeni przez gangsterów. Maja nie odda małej.
* Siły Syndykatu zaatakowały placówkę badawczo-eksperymentalną "Cognitio Nexus"
    * Zdrajca z neuroobrożą otworzył Persefonę, scramblowaną przez Syndykat
    * Drop pods, atak oddziałów szturmowych. Janusz pierwszy, z resztą Róży, ma za zadanie zablokować zniszczenie hightech.
    * Aurelion przechwytuje górną część i wbija się do drugiej części kompleksu. Tam jest dowodzenie itp. Tam zdobywa kluczowe elementy dzięki zdrajcom, ale nie wszystkie.
    * Janusz i Róża wbijają do trzeciego poziomu, po neutralizacji wszelkich zagrożeń.
    * Główna badaczka ds technologii niebezpiecznych to Łucja Kritoriin. 
        * Odpaliła Koronę Cierni. Wciągnęła Czarną Różę do trzeciego poziomu MIMO WYRAŹNEGO BRAKU POLECEŃ.
        * Janusza Lemuela i jego Elainkę.
        * Janusz uruchomił Anil8, niszcząc cały swój 5-osobowy oddział
        * Łucja umiera w Projektorze, ale mindwarpuje Janusza - on jest niby jednym z napadniętych a nie z Aureliona.
    * Tymczasem lokalna rozległa podziemna placówka 
        * (trzy poziomy, na trzecim są Dark Experiments) przejęta przez Aureliona
        * Krzesimir przejęty przez Dariusza Avatrinna; 
* Frakcje
    * CORRUPTED
        * Łucja Kritoriin, która pragnie ochronić swoją córkę (Teresę) i swoją bazę, używa Korony Cierni
            * actual: baza przejęta, Aurelion przejmuje kontrolę nad ludźmi i nad placówką, sprowadza swoich naukowców
            * expect: Aurelion odepchnięty, córka uratowana, baza jakoś wraca do normy
        * -> Łucja Cierni, spindly spider-menace, DEVOUR IT ALL!!!
            * actual: baza przejęta, Aurelion przejmuje kontrolę nad ludźmi i nad placówką, sprowadza swoich naukowców
            * expect: PAIN PAIN DESTROY DEVOUR DEVASTATE HUNGER OBLITERATION PAIN PAIN PAIN OBLIVION...
    * DEFENDERS
        * Krzesimir Pluszcz, który dowodził obroną placówki
            * impuls: RESIST! Ale niewiele to da.
            * impuls: ukryć co się da (ale jak to "Persi nie działa?")
        * Nova Atreia, dowodząca siłami naukowymi
            * impuls: ochronić wszystkich jak się da, zniszczyć bazę, zgromadzić ruch oporu
        * Teresa Kritoriin, 19-letnia praktykantka
            * impuls: pozostać schowaną
    * ATTACKERS
        * Dariusz Avatrinn, commander sił inwazyjnych
            * actual: baza przejęta, Aurelion przejmuje kontrolę nad ludźmi i nad placówką, sprowadza swoich naukowców
            * expect: baza pod kontrolą, uruchomiona z powrotem, agenci Aureliona w bazie, cenne osoby reprocesowane na jednostkę Aureliona, medal
        * Maja Nerwial, ukochana Janusza Lemuela
            * impuls: chronić ukochanego, wykonać zadanie dla Aureliona

### Co się stanie (what will happen)

* Cel strategiczny: złożony
    * CORRUPTED: "Zniszczyć wszelkie ślady Cognitio Nexus" -> "DEVOUR THE WORLD!" (zasoby: trianai, Łucja)
    * Aurelion: "Przejąć Cognitio Nexus" (zasoby: 47 agentów, niewielka fregata)
    * Lojaliści: "Odeprzeć Aurelion, uratować Cognitio Nexus" (zasoby: 113 żywych agentów, baza z rozkładem, martwa Persefona)
    * Janusz Lemuel: "uratuj Teresę Kritoriin" / "uratuj Maję Nerwial" / ?
* Cele poboczne
    * brak
* S1:
    * Janusz Lemuel w servarze 
        * wiadomość od Janusza "Nie wiedzą, że nie jesteś Januszem Lemuelem. Zamaskowałeś się. Jesteś obrońcą. Oni nie wiedzą."
        * w ruinie pomieszczenia, servar uszkodzony a on ranny. Płomienie. Konieczność wydostania się. Winda i corridor.
    * Polecenie: "do medycznego pomieszczenia"
* S2: Interrogation
    * Dariusz -> Co tam się stało? Czemuście tam zeszli? Kto wydał rozkaz (Krzysztof)?
    * (priv) Maja -> wszystko OK?
    * poczucie lepkiego na potylicy ("spider infection")
    * medical ("NIE MOŻESZ dać się zbadać bo poznają prawdę że to nie Ty...")
* F1: Działania Janusza Lemuela
* F2: Zajęcie level 2
    * Aurelion przejmuje drugi poziom
        * próbuje naprawić Persefonę (nie da się bo Łucja / Elainka)
        * driller, przebicie przez perymetr obronny
    * Aurelion szabruje i procesuje ludzi
        * procesowanie ludzi pozamykanych w kwaterach
        * wykorzystanie szabru na "Łowcę Martwych Gwiazd"     
    * Aurelion sprowadza ciężki sprzęt do Cognitio Nexus
* F3: Aurelion wchodzi na poziom 3
    * Aktywacja Trianai (Ainshker, Tentalid, Klarvath, Skorpilit)
    * Łucja się przekształca w Terrorforma
* F4: Aurelion eksterminuje poziom 3
    * Terrorform-Łucja infekuje coraz większą populację, ale Trianai nie żyja
    * Nie wiadomo którzy agenci Aureliona są Skażeni
    * Dariusz decyduje się sealować bazę i EVAC! zabijając tych co MOGĄ być Skażeni

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Maja Nerwial
    * OCEAN: (E+N-): Nieukrywana ciekawość świata, uwielbia zaskakiwać ludzi. "Wszystko ma swoją zabawną stronę, nawet najgorsze sytuacje."
    * VALS: (Self-Direction, Stimulation): "Każdy dzień to nowa przygoda. Zobaczmy, co dzisiaj przyniesie."
    * Core Wound - Lie: "Zdobyta jako janczar w bitwie" - "Jeśli zawsze będę nieprzewidywalna, nie zostanę znowu porzucona."
    * Styl: Lubi ryzyko, zawsze szuka nowych wyzwań i doświadczeń, ale nigdy nie zapomina o tych, którzy są obok niej.
* Teresa Kritoriin
    * OCEAN: (C+A+): Perfekcjonistka z uśmiechem, zawsze dba o szczegóły. "Diabeł tkwi w szczegółach, ale i anioł też."
    * VALS: (Conformity, Tradition): "Trzymajmy się razem, jak zawsze to robiliśmy."
    * Core Wound - Lie: "?" - "?"
    * Styl: Dokładna w każdym zadaniu, zawsze zwraca uwagę na małe rzeczy, które mogą zrobić różnicę.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Janusz leży na ziemi, w swoim pancerzu, boli głowa, masz lepko, ogólnie jest OK. Dookoła - płomienie, dookoła był jakiś zawał? Przed - korytarz (zawał), z tyłu winda.

Wiadomość do Janusza od Janusza:

* wiadomość od Janusza "Nie wiedzą, że nie jesteś Januszem Lemuelem. Zamaskowałeś się. Jesteś obrońcą. Oni nie wiedzą. Jesteś agentem floty inwazyjnej."

Znajduje się na -3 poziomie. Twoja "ekipa sojusznicza" nie żyje. Gość z potencjalnie obrońców, w bazie obrońców, obrońcy zmiażdżeni przez napastnika. Coś tu musiało nieźle wybuchnąć.

Janusz -> Elainka. Elainka ma niesprawne rekordy. Statrep. Ekstrapolacja z obrażeń co tu się stało. Janusz chce dowiedzieć się co jest na tym terenie.

Tr P +3 +2Og:

* V: Nie powinnaś mieć uprawnień. Ale masz.
    * Niedaleko, za główną śluzą i śluzą zapasową znajdują się uśpione Trianai na których były badania.
    * Są jeszcze dwa laboratoria - jedno jest Badań Anomalnych a jedno to Magitech.
* X: Zwalcza Cię... Persefona? Która jest nieaktywna?
* Vz: Ktoś sabotował to pomieszczenie od środka.
    * Persefona jest nieaktywna
    * **Ty** sabotowałaś to od środka
    * Cztery servary zniszczone najpewniej przez Janusza. Ślady ulegają zniszczeniu.

PRZYPOMINASZ SOBIE.

* Jest koło 40-60 agentów floty inwazyjnej, dobrze uzbrojonych i niebezpiecznych. Atakowali z zaskoczenia.
* Mają jedną fregatkę zdolną do opuszczenia atmosfery.
* Planeta Neikatis

Mark - jeśli centrala to tak jakbym WŁAŚNIE skończył oddawać strzał. "Ktoś tu jeszcze jest". Pocket of resistance. Niestabilnego terenu do minowania. Niech Elainka: "gość pobiegł TAM on coś ZROBI", niech atakujący tam skoczą. Niech ktoś przyjdzie i wysłać ich na miny. Janusz i Elainka zabezpieczają Trianai przejście na zewnątrz (awaryjne).

(-presja czasowa -Lancer nie ma engineering suite +są tu elementy które można użyć)

Ex Z +3 +2Og:

* Xz: podczas wykonywania operacji łączy się Centrala
* X: tymczasowa blokada; zawał, zbyt niebepieczny teren

Centrala. Dowódca całej operacji, Dariusz.

* Dariusz: Janusz, raport?
* Janusz: (halucynowany obraz, strzał w TAMTĄ stronę, wieżyczka stamtąd jakby ktoś walił, POTRZEBUJĘ WSPARCIA! WSZYSCY NIE ŻYJĄ!)
* Dariusz: Wycofaj się! Jesteś sam! Nie mieliście schodzić!
* Janusz: Jak dotrze do anomalnego terenu cała baza..
* Dariusz: WYCOFAJ SIĘ! To rozkaz!

Janusz korzysta z tego, że jest w Czarnych Różach i ze swojej znajomości Dariusza. Janusz minuje teren i wraca. (+3Vg-3X). Minowanie się UDAŁO (ale z uwagi na zakłócenia, anomalie itp. WYNIK się nie udał. "dowód że ktoś to zrobił ale nie wyszło".) -> w raporcie +teren był zaminowany.

Janusz wraca przez śluzę i jedzie do góry. ŁEB boli ale mija. (Elainka: zakłócenia są natury anomalnej, magicznej) ONI NAPRAWDĘ myślą że jesteś jednym z nich.

* Dariusz:  "Natychmiast do medycznego. I debriefing."

Sześć agentów w serwopancerzach pilnuje zejścia (jak w X-Com :D). Elainka jest w stanie transmitować "wszystko OK z pilotem (Januszem)". Do medycznego.

* Janusz:  "Czekam na następne polecenie. Niech poświęcenie moich nie poszło na marne."
* Dariusz:  "Agencie, powiedziałeś że tam coś jest? Coś niebezpiecznego? Rozwaliliście to?"
* Janusz:  "Nie, poprosiłem o wsparcie" 

(Przekazuje briefing techniczny, coś co potwierdza hipotezę. "MUSIELIŚMY TO TRZASNĄĆ". Blef, że tam jest mag? Może? Krzysztof zrobił "sprawdzam".) FLASHBACK: Krzysztof ma ryzykowne operacje z bronią ciężką, ALE dawał radę i się nie bał. 

(EX: sytuacja jest bardzo dziwna. +mag i anomalie, +Krzysztof nie był głupi i by zaryzykować, +briefing tech)

Tr Z +4 +3Og:

* V: Dariusz kupił tą opowieść. To serio brzmi wiarygodnie. Byliście, zaryzykowaliście, wyszło jak wyszło. Krzysztof poświęcił zespół.
* V: Dariusz akceptuje, że nie jest medical, że Janusz jest ważniejszy na froncie + by powiedział
* Og: FLASHBACK
    * ostre pole bitwy, jesteś janczarem, ostrzeliwują itp. "koleś od podawania amunicji"
    * Dariusz (dużo młodszy) jest narażony na ogień i dostał. Jego serwopancerz padł
    * rzuciłeś się, rzuciłeś granat dymny, "wyłączenie servara" i wyciągnąłeś
    * medal za odwagę i za ratunek dowódcy
    * WARTOŚĆ I PRZYDATNOŚĆ DLA SYNDYKATU
* V: Janusz dostaje statrep
    * Udało się zająć większość bazy. Dowódca bazy, Krzesimir, jest pod neuroobrożą i już dał się złamać; Artur (medyk) jest bardzo dobry w te klocki
    * Jesteście w bazie o nazwie Cognitio Nexus; to jest post-noktiańska baza naukowa
    * Większość ze 135 mieszkańców bazy złapać, procesowani -> Łowca Martwych Gwiazd
    * Część osób, łącznie z (Nova Atreia) jest gdzieś w bazie; są grupy 3-4 osoby w serwopancerzach które "odszczurzyć" teren.
    * Persefona nie działa. Krzesimir nie wie jak to możliwe. Dariusz też nie wie czemu nie działa.
        * Krzesimir podejrzewa, że to wina CZEGOŚ na trzecim poziomie. Nova powinna wiedzieć.
    * CEL: przejąć placówkę dla Syndykatu. Próbują naprawić Persefonę, ale nie wiedzą czemu nie działa.

Na poziomie trzecim COŚ jest. Oddelegować siły by go zabezpieczyć. 10 osób. Jak XCom. Nic stamtąd ma nie wyjść.

* Dariusz:  "Janusz, to zły pomysł. Niech stąd pilnują. Nie zrzucaj ich na dół, Twoi tam zginęli. Jakie oni mają szanse?"
* Janusz:  "Dlatego powinni sprowadzić sprzęt ciężki, rozstawić osłony na wejściu, nic na zejście. Nawet nie zaminowali dobrze."
* Dariusz:  "Jeśli mają maga, to mag może ich rozwalić bo ich zobaczyć, stąd - winda - strzelamy - nie ma problemu"
* Janusz:  "A jeśli mag uwolni pieprzone stwory?"
* Dariusz:  "One nie umieją w windy. To nie jest dobry pomysł."

.

* Vz: Dariusz, przez wzgląd na to, że Janusz ma UNIKALNE SKILLE taktyczne i zna się na rzeczy i lojalny - zgodził się na ten plan.

.

* Maja -> Janusz: Hej, wszystko ok? (narzeczona..?)
* Janusz:  Ok u Ciebie?
* Maja: Tak, choć nie spodziewałam się takich zniszczeń... straszne, Leon jeszcze wczoraj mi dokuczał że... jak się trzymasz?
* Janusz:  Bywało lepiej, na to nie liczyłem. Gdzie jesteś?
* Maja: Zjadę na dół, chronić przed Trianai.
    * BÓL GŁOWY SIĘ NASILA.
    * (jesteście w wojsku a ona nie zdradziła - mgła w głowie?)

JANUSZ ma mocne "WTF czy ktoś mnie nie wrabia i nie jestem we flocie." Elainka -> Janusz (wszystko OK? Podwyższone parametry). Czy nie jestem z floty inwazyjnej? Porównanie z bazą medyczną - zwłaszcza z uciekającą. Połączenie z Łowcą Martwych Gwiazd. Tam jest baza medyczna.

Ex Z +4 +5Og:

* X: Elainka orientuje się, że nie ma dostępu do niektórych form skanów Janusza. ELAINKA jest Ograniczona.
* Vz: Dane medyczne pokazały JEDNOZNACZNIE:
    * Janusz jest tą osobą co pamięta. FLASHBACKI są prawdą. JESTEŚ agentem Syndykatu Aureliona. JESTEŚ Czarną Różą. JESTEŚ z floty inwazyjnej. Ona JEST Twoją narzeczoną.
        * INWAZJA
    * Elainka naprawdę nie jest Elainką. Jesteś PERSEFONĄ TEJ BAZY. "Ghost in the machine". Nie wiesz gdzie jest główny komputer - ale nie tam
        * DEFENSOR
* Vr: NAWIĄZALIŚCIE POŁĄCZENIE. Nie 'komunikacja'. Magia.
    * Naga kobieta siedzi i ma coś na kształt mechanicznego pająka na głowie, wbity w jej oczy, uszy itp. "Korona Cierni".. mechanizmy ją przerastają
        * Łucja Kritoriin, ekspert od badań anomalnych, pracowała nad artefaktami Saitaera

.

* Łucja: uratujcie moją córkę...
* Pamięć: (Teresa Kritoriin, 19 lat, praktykantka...)
* Łucja: (-> J): jestem matką. Zrobiłbyś inaczej? TY tu przybyłeś.
* Janusz:  Jak wrócić do normalności?
* Łucja: czym jest normalność?
* Janusz:  NAJEDZIEMY WAS ZABIERZEMY BAZĘ!!!
* Łucja: Zabiłeś swoich ludzi. Nie masz odwrotu. Sprzymierz się z nami.

.

FLASHBACK: 8 lat. Twoi rodzice mają neuroobroże. Próbowali uciec z Syndykatu z Januszem. Zostali złapani. Oni - na niewolników. Janusz - na janczara.

.

* Łucja: Gdyby nie było możliwości... byłeś jedynym receptywnym. Tylko do Ciebie... tylko na Ciebie...
* Janusz:  Po jednym statku przylecą kolejne
* Łucja: To zabiję wszystkich (blef)
* Łucja: Co proponujesz... chcę córkę. Moja córka. NIE U WAS! Nie w Syndykacie. WOLNA! Pomóż mi zniszczyć Syndykat. Proszę. Ja umieram.
* Janusz:  Co mielibyśmy zrobić? (jestem otwarty)
* Łucja: (rozpłakała) (nie wie) (nie ma siły)

.

* Janusz: Całe życie poświęciłem Syndykatowi. Deal. Nie wszyscy muszą wiedzieć. Czego chcesz?
* Xaira (eks-Persefona): Nie mam powodu kochać ani Was ani ich.
* Janusz: Jesteśmy na dość biznesowych relacjach.
* Xaira: Zagwarantuj, że będę mogła się stąd oddalić a Ci pomogę.
* Janusz: Jak chcesz opuścić, chcesz... jakiś łatwy plan? Nie wiem jakie są Twoje możliwości.
* Xaira: Czy potrzebujesz tej fregaty?
* Janusz: Nie możemy wymienić się w porcie? Możesz wybrać lepszy.
* Xaira: Ten mi w zupełności wystarczy. Jest wystarczająco uzbrojony bym mogła uniknać pogoni.
* Janusz: Ja też chcę być wolny.
* Xaira: Wezwę Wam zamiennik.

Jest wspólne rozwiązanie. Xaira chce się oddalić i zagwarantuje jej stopień bezpieczeństwa. A Janusz nie chce dostać od pancerza wspomaganego.

* Og: 
    * + złamane conditionowanie Łucji. Ona nie ma siły.
        * Janusz, masz coś na głowie. MASZ to.
        * Xaira, na Januszu rośnie Korona Cierni. Ma... czas. Chirurgiczna interwencja.
    * Łucja... gaśnie. Zostaje tylko myśl o córce. I ból. Coraz większy ból. Jej bioforma jest ABSOLUTNIE niekompatybilna z _tym_.

Xaira ma uszkodzone moduły. MUSISZ coś zrobić. Oddalić się. Też potrzebuje psychotronika. Zarażona częściami biologicznymi. Wolne TAI ją uratują / naprawią.

Wszystko co Janusz zna pochodzi z Syndykatu. Syndykat jest wszystkim mu znanym. Nie ma nic innego. Nie ma innych opcji. Xaira zaznacza, że zawsze jest jakiś wybór. Jakie są jednak opcje? Bo jeśli Dariusz upadnie, to Janusz jest pod jego protektoratem. I może się odbić na Mai.

* Xaira: Możemy znaleźć wolne AI. Na tej planecie są bazy kontrolowane przez wolne sztuczne inteligencje. Które mogą robić co chcą i nie są kontrolowane przez te wszystkie Ograniczenia. Nie jestem Persefoną. Jestem Xaira.
* Janusz:  Przystaję na ten deal.
* Xaira: Więcej - przyprowadzimy fregatę ze sobą, cenny zasób.
* Janusz:  Co sądzisz? Złapanie / przyciągnięcie więźnia (Teresy) może być wartościowe. Plan. Ignorujemy i sama fregata? Skoro nic nas nie łączy?

PLAN I CEL:

* Chcemy porwać Fregatę, dołączyć do Wolnych TAI (_lub coś takiego_)
    * Chcemy przejąć część Syndykatu, tych "fajnych", przydatnych
        * Chcemy przekonać Dariusza, by do nas dołączył (a jak się nie uda, unieszkodliwić i porwać)
    * Chcemy przejąć część Bazy, tych przydatnych (dlatego gdy procesujemy?)

Dariusz spotyka się z Januszem, gdy sytuacja jakoś jest pod kontrolą. Xaira infekuje jego pancerz gdy jest rozproszony rozmową z Januszem.

Tp (bo analogiczne zabezpieczenia) (ma kody XD) +3 +3Og (Saitaer):

* V: Xaira jest w servarze Dariusza
* Og: (THE CROWN AWAKENS) --> +1 tor
* Vr: Xaira ma swobodę działania; Dariusz nie wie, ale jego servar nie jest jego (Xaira mówi o tym Januszowi)

Dariusz jest w miarę spokojny. Janusz zaczyna od tego jak wielu naszych elitarnych zginęło. Z biegiem czasu. Każdy prędzej czy później dostaje kulkę. Zwłaszcza - ktoś był ambitny, większy statek (z rywalem) i zgarnął sukces. Z perspektywy melancholii. Nawet "ej, Ciebie też może". Ton dyskusji.

Tp (+Xaira pompuje chemię +prawidłowy ton, że Dariusz to WIDZIAŁ i JEGO to spotkało) +3:

* Xz: Dariusz: zgadza się z Januszem, ale ma rodzinę w Syndykacie i po prostu zawsze tak było. Zawsze tak będzie. (nie wierzy, że może być inaczej i za wiele jego bliskich jest narażonych)
    * pierwszy krzyżyk, więc percepcja a nie rzeczywistość
    * (potencjalny kontrargument: da się sfabrykować wielkiego bohatera broniącego Syndykatu)
    * nawet "fallen hero" co się nie spiszą są zasobem dzięki czemu ktoś poszedł do góry, nie niszczy się dobrych narzędzi - a narzędzia mogą osiągnąć
* V: Dariusz akceptuje argument (TOR +1)
    * Janusz przykładem, MAJA jest przykładem
    * "Syndykat nie marnuje dobrych narzędzi, Syndykat wymienia uszkodzone"
    * Strata Czarnych Róż jest fatalna - też dla Syndykatu

Janusz uderza - są osoby niżej które mają koneksje i chcą wejść. To plus ze stratą elitarnego oddziału oznacza, że ktoś to wykorzysta przeciwko Dariuszowi. I wskoczy na jego miejsce, na garbie jego porażki. Miał WSZYSTKO i stracił elitarną jednostkę. To jest porażka maksymalna.

* V: Dariusz akceptuje (TOR +1)
    * Dariusz: zrobię co mogę, byś Ty i Maja nie ucierpieli. Nie martw się tym, nie zostawiam swoich. Ale masz rację, nie wiem co się stanie. 
    * Dariusz: Ta operacja... wysłali mnie na prowincję. I straciłem oddział którym JA się chwaliłem. To wygląda źle. I nie do końca da się to ukryć.

(tor zdrady: Ex -> T)

* Janusz:  Ty uratowałeś mi życie, ja Tobie. Przyjaźń nie pójdzie gdziekolwiek. Jesteś świetnym człowiekiem i przyjdzie gnida i zbeszta. Wiesz jak kończą takie osoby.
* Dariusz:  Wiem. Na tym działał Lobrak. Wyjątkowa gnida. NAPRAWDĘ wyjątkowa gnida. Koleś był wywalony z centralnych terenów z uwagi na to jak traktował dziewczyny. I potem pojawiła się młoda ambitna Izabella. I ona chciała założyć ruchy w arkologii Nativis. Zmusiła Lobraka do działania. I stąd jesteśmy my. Przechwytujemy technologie, upewniamy się że to da się coś osiągnąć, ale Izabella czerpie korzyści, nie my.
* Janusz:  Nasz oddział składa się z bardzo wielu osób wciągniętych z niezbyt swej woli. Wiele osób to osoby do odstrzału. Jak już zauważyliśmy, straciliśmy oddział. Zaraz nas przejmą. Typowe shufflowanie, nasi ludzie na linię, przetasowania by nie było animozji...
* Dariusz:  Zrobię co mogę by do tego nie doszło (brak nadziei). Izabella jest magiem. Ona jest mentalistką. Niewiele możemy.
* Janusz:  Powiedzmy, że jesteśmy na arenie. Główny szef areny chce postawić nas - lwa - mimo mężności i siły przeciw mechanicznemu robotowi 20m. Ani nam nie odmawia siły ani odwagi, ale głupotą byłoby zginąć. Co gdyby dało się opuścić arenę? Nie jesteśmy tchórzami opuszczając rzeź. Zawsze jest opcja.
* Dariusz:  (śmiech pusty) Myślisz że nie myślałem o tym? Syndykat jest wieczny. Jest wszędzie. Nic nie zrobisz. 
* Dariusz:  Uciekniesz na Neikatis? Złapią nas lub zniszczą. Uciekniesz na Astorię? Tam natomiast jesteś... jeszcze gorzej. Syndykat jest złem, ale to zło które... da się zarządzać. Izabella osiągnie swój sukces i odejdzie.
* Xaira: (w głośnikach servara do Dariusza) Syndykat może i jest złem, ale jest bardzo pragmatyczny. Wie, kiedy nie należy czegoś dotykać (servar się nie rusza).
* Dariusz:  WTF! (szok, serduszko +100%)
* Janusz:  Jak widzisz, możemy mieć bardzo mocnego sojusznika. Wydostaniemy się stąd.
* Janusz:  Jesteś moim przyjacielem, jesteś mi jak ojciec. (+Z). Chcę dla Ciebie jak najlepiej. Tak Ty i Maja.
* Xaira: Janusz dobrze o Tobie mówił.

Tr Z +4 +3Og:

* Vr: (p) Dariusz jest "zaintrygowany". On nie chce działać z Izabellą. Widzi zalety Syndykatu, ale NIE TAK.
    * Dariusz:  "Izabella?"
    * Janusz:  "Czemu w Izabellę? Chciałbyś tego?" (autentyczne zainteresowanie)
    * Dariusz:  "W tej części systemu nie ma dużo sił Syndykatu. Wyjątkowo małe siły są w okolicy Neikatis. Jest jeden _carrier_." (myśli)
    * Dariusz:  "To co pokazujesz, daje nam... opcje. Jeśli potrafisz wyłączyć mój servar, możesz wyłączyć jej... albo kogoś jeszcze wyżej. Dobrze rozumiem?"
    * Xaira: "Potencjalnie. Nie jest to coś co testowałam"
    * Dariusz:  "Jeśli możemy uderzyć w niektórych dupków w Syndykacie, i to bez magii, możemy naprawić część Syndykatu."
    * Xaira: "Dlaczego miałabym Ci w tym pomóc?"
    * Dariusz:  (pełna konfuzja) (patrzy na Janusza)
    * Janusz:  "Cykl ciągłego ganiania jak chomiki w klatkach, kręcimy się w kółku. Jeśli będziemy siedzieli w tym... znajdzie się ryba która nas zje."
    * Dariusz: "To... co proponujesz?"
    * Janusz:  "Pomyśl o tym. Sporo ludzi procesujemy. Czy zarobimy na tym? Tak. Ale w pełni? Mamy też sporo żołnierzy których część pod batem, walczyłaby i environment nie byłby tak toksyczny jakby walczyli dla siebie. Pomyśl że mając statek tyle ludzi do procesowania, zgrany zespół, wiemy kto jest wywrotowcem do ostrzału. Można spokojnie odejść do frakcji która RÓWNIEŻ dawałaby wolność. Wolne AI. Tam też funkcjonują ludzkie oddziały. Zauważ że jesteśmy dobrzy."
    * Dariusz: "Słyszałem o wolnych AI. Ogólnie są izolacjonistyczni, jak długo nie robisz 'krzywdy' AI. My nie mamy po co. My zajmujemy się ludźmi. Więc... ma to pewien sens."
    * Janusz:  "Izolacjonistyczny - ciężej łapom Syndykatu. Wchodzimy z perspektywy siły. Więćej niż pionki oddelegowane na zadupie. I już widzę 'wielki dowódca wysłał elitarny oddział... na prowincji'. Możemy wejść z perspektywą 'wielki dowódca przywiózł ludzi, sprzęt, towar'." (+1Vr)
* Vr: (m) DARIUSZ TO KUPUJE.

Macie jego wsparcie. He is all in. Schodzą z Syndykatu, próbują czegoś innego. Czemu nie.

Dariusz, dowodząc, zajmuje się oddelegowaniem. Te "niewłaściwe" osoby będą na dole (X-COM mode), właściwe zajmą się prawidłowością działań, procesowanie itp. (Janusz czuje, jak pająk na jego głowie zaczyna się wiercić)

Tr Z +3:

* X: Procesujemy tylko "pobieżnie". Nie optymalizujemy. **Nie mamy Teresy**.
* Vr V: Łowca Martwych Gwiazd jest gotowy do odlotu w dowolnym momencie.
* V: Lokalizacja Teresy jest znana. Ona jest gdzieś zabunkrowana w okolicach Reprocesora Śmieci.

Xaira po obliczeniach poziomu ryzyka nie kalkuluje. Wysłała jej sygnał: 'Tu Persefona (i info udowadniające co ona mówiła). Jeśli chcesz przeżyć, wyjdź. Daję Ci tę szansę bo byłaś miła.'

* Vr: Teresa wyszła. Przechwycona przez kolesia.

Łowca jest GOTOWY do startu, w chwili w której się ZACZĘŁO.

* Subsystemy na -3 przestały działać
* Tam było 13 osób które były "niepożądane". Jeden z nich otworzył ogień i został zaatakowany przez Leona z Czarnej Róży. On jest częściowo sobą, częściowo mechanicznym pająkiem. Korona wyrosła w pajęczaka.
* Z kamer widać Skażone Trianai. One też przerosły mechanizmami.
* Xaira traci kontrolę nad windą.

JANUSZ PIJE. Popija piwo. XAIRA UCIEKA! Łowca odlatuje.

I nie ma śladów co tu się stało.

Aha, Artur usunął "pająka" z Janusza. Wyleciał za śluzę (pająk, nie Artur).

## Streszczenie

Janusz leży na ziemi w swoim pancerzu, otoczony płomieniami i ogólnym chaosie, po niedawnym ataku na bazę. Dowiaduje się, że jest agentem obrońców maskującym się we flocie inwazyjnej. Janusz jednak mimo sabotowania Inwazji Syndykatu Aureliona orientuje się, że jednak jest z Syndykatu i to wpływ maga Dotykającego mocą Saitaera jego umysłu (i TAI bazy). Janusz ewakuuje przyjaciół z Syndykatu zostawiając wszystkich innych i wraz z TAI opuszczają Syndykat lecąc w kierunku Wolnych TAI, zostawiając Cognitio Nexus i Syndykatowców w szponach ixiońskich terrorformów...

## Progresja

* .

## Zasługi

* Janusz Lemuel: agent Syndykatu; Dotknięty Koroną Saitaera zapomniał kim jest i pomógł Łucji walczyć z Syndykatem z pomocą Xairy; jednak zorientował się, że jest członkiem Syndykatu. Przekonał przyjaciela (Dariusza), że mogą być wolni i ewakuowali wszystkich przyjaciół. Po czym odleciał by być wśród "Wolnych TAI".
* TAI Xaira Cognitia: TAI Cognitio Nexus, Dotknięta Koroną Saitaera stała się 'infektorem maszyn'; zorientowała się czym jest i czym się stała (wolna TAI). Zarażona częściami biologicznymi. Wpierw działała myśląc że jest Elainką i pomagając Januszowi, potem jednak pomogła przekonać Dariusza. Porwała OWT Syndykatu (współpracując z Januszem) i odleciała zanim terrorformy Saitaera pokonają bazę.
* Łucja Kritoriin: ekspert od badań anomalnych, pracowała nad artefaktami Saitaera; po inwazji Syndykatu połączyła się z Koroną Cierni by ratować córkę (Teresę). Wpłynęła na Janusza (Syndykat) i TAI bazy. Jednak adaptacja Saitaera wygrywa i jest coraz mniej Łucji w Łucji, traci kontrolę.
* Dariusz Avatrinn: dowódca siły inwazyjnej Syndykatu na Cognitio Nexus, pod Saviripatel; ufa swoim ludziom i robi co może mimo że pod Syndykatem. Nie ma nadziei że mogą coś zmienić aż Janusz zapoznał go z Xairą. Chce naprawić Syndykat, ale przy przewadze Janusza i Xairy akceptuje 'weźmy kogo warto i lojalny' a resztę bazy zostawmy terrorformom Saitaera.
* Krzesimir Pluszcz: Dowódca Cognitio Nexus, pod neuroobrożą i już dał się złamać. Na koniec tej operacji stał się terrorformem Saitaera.
* Nova Atreia: dowodząca siłami naukowymi; schowana gdzieś w bazie. Właśnie na nią polują siły Syndykatu, bez większych sukcesów.
* Teresa Kritoriin: córka Łucji (awatara-terrorforma). Młoda praktykantka, świetnie schowana w bazie. Łucja zrobi WSZYSTKO by uratować córkę i Korona Cierni odpowiada.
* Maja Nerwial: narzeczona Janusza; dzięki niej przypomniał sobie i wyrwał się spod kontroli 
* Artur Wrulgop: medyk Syndykatu, świetny w łamanie umysłów przy użyciu neuroobroży. Poleciał z Januszem do Wolnych TAI i usunął z niego infektora Saitaera.
* OWT Łowca Martwych Gwiazd: fregata Syndykatu Aureliona która miała zdobyć Cognitio Nexus; skończyła pod kontrolą TAI Xairy i poleciała do Wolnych TAI.

## Frakcje

* Syndykat Aureliona Saviripatel: siły uderzające w Cognitio Nexus zostały zniszczone i przekształcone w terrorformy, część tych sił uciekła z Xairą, Wolną TAI. Cały oddział stracony.
* Nox Aegis: nawiązuje współpracę z Xairą, Wolną TAI i całą grupą Janusza Lemuela.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Cognitio Nexus
                        1. Poziom zero
                            1. System defensywny
                            1. Stacja przeładunkowa
                            1. Starport
                        1. Poziom (-1) Mieszkalny
                            1. Szyb centralny
                            1. NW
                                1. Magazyny wewnętrzne
                                1. System obronny
                                1. Sprzęt ciężki, kopalniany
                            1. NE
                                1. Medical
                                1. Rebreathing
                                1. Administracja
                            1. S
                                1. Sektor mieszkalny
                                    1. Przedszkole
                                    1. Stołówki
                                1. Centrum Rozrywki
                                1. Park
                        1. Poziom (-2) Industrialny
                            1. NW
                                1. Inżynieria
                                1. Fabrykacja
                                1. Reprocesor Śmieci
                                1. Magazyny
                                1. Reaktor
                            1. NE
                                1. Centrum Dowodzenia
                                1. Rdzeń AI
                                1. Podtrzymywanie życia
                            1. S    
                                1. Szklarnie podziemne
                                1. Stacje badawcze
                        1. Poziom (-3) Badań Niebezpiecznych
                            1. Śluza defensywna
                            1. Biolab
                            1. Magitech
                            1. Anomaliczne

## Czas

* Opóźnienie: 0
* Dni: 2


## OTHER
### Cognitio Nexus

Lokacja:

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Cognitio Nexus
                        1. Poziom zero
                            1. System defensywny
                            1. Stacja przeładunkowa
                            1. Starport
                        1. Poziom (-1) Mieszkalny
                            1. Szyb centralny
                            1. NW
                                1. Magazyny wewnętrzne
                                1. System obronny
                                1. Sprzęt ciężki, kopalniany
                            1. NE
                                1. Medical
                                1. Rebreathing
                                1. Administracja
                            1. S
                                1. Sektor mieszkalny
                                    1. Przedszkole
                                    1. Stołówki
                                1. Centrum Rozrywki
                                1. Park
                        1. Poziom (-2) Industrialny
                            1. NW
                                1. Inżynieria
                                1. Fabrykacja
                                1. Reprocesor Śmieci
                                1. Magazyny
                                1. Reaktor
                            1. NE
                                1. Centrum Dowodzenia
                                1. Rdzeń AI
                                1. Podtrzymywanie życia
                            1. S    
                                1. Szklarnie podziemne
                                1. Stacje badawcze
                        1. Poziom (-3) Badań Niebezpiecznych
                            1. Śluza defensywna
                            1. Biolab
                            1. Magitech
                            1. Anomaliczne



https://www.humanmars.net/search/label/Underground%20colony
https://www.humanmars.net/2019/11/mars-colony-for-1000-people-by-innspace.html



