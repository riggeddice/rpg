## Metadane

* title: "Pionier w Anomalii Kolapsu"
* threads: legenda-arianny
* gm: żółw
* players: arwenn, mactator

## Kontynuacja
### Kampanijna

* [200415 - Sabotaż Miecza Światła](200415-sabotaz-miecza-swiatla)

### Chronologiczna

* [200415 - Sabotaż Miecza Światła](200415-sabotaz-miecza-swiatla)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* Miecz Światła ciężko ranny i uszkodzony, Persefona przejmuje kontrolę.
* Śmierć wszystkich na Luminusie

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

Koło 100 lat temu, przed pojawieniem się Saitaera i przed wykryciem Obłoku Lirańskiego do tego miejsca zawitał jeden z pierwszych statków badawczych, Pionier 774, dowodzony przez Olafa Murczanta. Jego oficer naukowa, Annika Seler, próbowała zbadać anomalię w Obłoku Lirańskim - i przepuściła przez siebie ogromną ilość energii. W ten sposób Astoria odkryła Libracja Lirańska (przez potężną emisję kosmiczną) a los Pioniera 774 pozostał nieodkryty.

Słowniczek (z naszego na transkrypcję głosową EN): 

* "Miecz Światła" --> Lightsaber
* Aneta --> Anette (objęta przez Kamila jako Antoni na sesji)
* Mateusz --> Matthew (objęty przez Maćka)
* Okręt Floty Orbitera Luminus --> Luminous

## Punkt zero

The spaceship, Luminous, is dying. It’s reactor somehow started to fail. Some creatures which once used to be the crew members started approaching from the corridor, running towards two Marines trying to escort six crew members to the only safe place on the Luminous – the AI Core.

Marines ran, one of them picked the wounded crewmember and the other threw fire extinguisher at approaching creatures. They started escorting whomever they could, picking up the pace. (VVVX). They succeeded, they have even managed to take one of the approaching creatures with them for study – however one of the people escorted got seriously wounded.

Luminous started to shut down and it’s captain wasn’t able to do anything about it. They only hope was for someone to save them now.

## Misja właściwa

The SOS signal sent by dying luminous was picked up by the lightsaber. The lightsaber’s captain, Matthew, made short calculations – luminous was located inside „collapse anomaly”, in the lyran nebula located in the astorian sector. That place in itself was extremely dangerous, especially because luminous was located in so-called „ship graveyard” - the place where the anomaly was the strongest.

It has never stopped Matthew, he knew what his spaceship was capable of and he knew he was able to save whomever needed – of all the ships in the Starfleet, lightsaber was the most capable.

When they approached the ship graveyard, Lightsaber sensors way able to pick up unmanned Hunter Killer drones (ŁZa). The hunter killers were circling around the luminous, and the luminous seemed completely shut down. That in itself was quite interesting, because no sane captain would ever shutdown whole starship.

Annette went to work – she tried to recalibrate the sensors to understand what in the world is happening with luminous and to scramble the messages between hunter killers in such a way that hunter killers decide that the lightsaber is their mothership. (VVVXXX)

As a result of the bold actions, the hunter killers truly stopped paying attention both to the luminous and to the lightsaber. Even more, Annette has managed to make some of the hunter killers believe they should listen to TAI Persephone’s (Lightsaber’s Tactical Artificial Intelligence) orders. However, while reconfiguring hunter killers, some of them opened fire on lightsaber – the armour was slightly damaged, even if it was not anything very dangerous.

What is worse, while Persephone, acting on Annette’s orders, tried to hijack the hunter killers databases, the hunter killers have managed to scramble Persephone as well. Therefore for a while whole lightsaber was disabled.

And that is when the magical corruption which influenced the luminous was able to start influencing the lightsaber as well. But because of the difference between classes of luminous and lightsaber, this process was taking much longer.

Matthew decided that it is dangerous to do anything with the luminous while they are inside the ship graveyard. He activated the tractor beam and started pulling luminous outside the anomaly (which was supposed to take several hours). Annette noticed, that there is some kind of force influencing the luminous – some force pulling luminous towards the center of the anomaly, the nebula. This in itself was extremely interesting. Might require some more detailed work. (VVVV)

Annette recalibrated the sensors again – what is really happening on the luminous. And it so happened that luminous was completely disabled, reactor was completely cold and auxiliary reactor was malformed (which in itself was strange). All humans were located next to an AI Core, life-support systems were down (so they had at most four hours of life remaining until oxygen and stuff goes away) and there was some movement next to the bridge - but the movement was classified by sensors as ‘nonhuman’ and ‘anomalous’.

There was something really sketchy going out inside luminous. Matthew decided to take an away team of 20 Marines, Annette, and they decided to breach the luminous. Time to evacuate the people and make sure they survive in the safe place – on the lightsaber.

The breach was successful. Matthew still inside the luminous and he noticed something extremely strange – of all the people in the Starfleet, Matthew was really interested in spaceships, their types and internal structure. Luminous was supposed to be a Kairen-class utility ship, designed to either make small repairs or small fabrication or disassembling and transporting stuff. However what he has seen was an amalgamation of two starships really - a Kairen-class (as it should be) and one other ship. A much smaller and much older unit. (VV). Matthew has identified the other spaceship as pioneer. Even more, it was the pioneer 774, the first ship that has managed to stumble upon the anomaly and has paid the highest possible price.

Knowing all that, Matthew was able to find some secret passages and weaknesses in pioneers construction, so that he was able to take the Marines and move through wars, directly into the AI core. While doing that he ordered Annette to establish communication link with the lightsaber – and it succeeded too (VOXXV). However by establishing this link, Annette was able to send corrupted signals from the corrupted a high core on luminous directly into the Persephone which was already damaged after encounters with hunter killers.

In other words, Persephone got convinced she has to save everyone, including the pioneers crew. Persephone warned Annette, that there is something really strange going on here and that she can detect multiple signals from multiple artificial intelligences. Persephone was not able to determine and see that her own subsystems were being assaulted by the „ghost” of pioneers scientific officer, Annika.

In the meantime, Matthew was able to enter the AI Core with his Marines. He told them he comes from the lightsaber and he comes to save them. He asked the captain of the luminous what the hell was going on here and Toby (captain of luminous) obviously did not manage to get out of this mess 100% sane. He started rambling something about ghosts and about the crew members becoming some technological nightmares. He did manage to save a lot of his crew, but obviously the current situation was a bit too difficult for him to handle.

When Annette came to rejoin with Matthew, she decided to scan everyone here to find out who is corrupted and who is not – to be able to determine who can enter the lightsaber and who should not. But this can proved something quite unfortunate (XXOOV): according to the scan, absolutely everyone here was already corrupted by the rogue magic.

And Annette, Matthew and his Marines were starting to become corrupted as well. So they have very limited time. Also, during this can Annette has managed to detect that something is wrong with the AI core. The core has this spaceship’s Persephone mixed with a biological component – with the former science officer of the luminous. The AI core mixed with the officer are still reconstructing themselves into one entity and Annette doesn’t really want to catch it even with a 10 foot pole. Of course, Annette wanted the captain discreetly about this situation.

So at this point Matthew has managed to determine what was happening – it was quite probable that the past was somewhat mixing with the present. Perhaps the captain of the luminous was not really going insane, perhaps there is something wrong on a very different level. So Matthew pulled Toby on the side and asked him if he can recognize any of the crew members which are not really his crew members (XXVVV).

The result was quite interesting. Toby said he can hardly recognize some of his crew members. He doesn’t remember having that many crew members. Matthew asked him with the name „Olaf” (captain of the Pioneer) and Toby reacted – in his mind he is both Toby and Olaf. Sometimes he remembers she is Toby sometimes remembers he is Olaf. But this allowed Matthew to determine which of the crew members of luminous are more the past selves and which are steel in the present. Matthew has also managed to get information that the problems on pioneers started when Annika - the science officer – started touching the anomaly using magic.

And this has reminded Matthew of a communication he received 15 minutes ago. This medical officer swore to everything holy that she did not drink while on the duty but she has seen a cute looking redhead who could not have been there, who has been a ghost. At that point Matthew ignored this information but now he ordered Persephone to find if there was a person like that on the original pioneer.

And Persephone answered – there was a person like that. It was Annika. This means that Annika has invaded the lightsaber. So even if Matthew manages to contain the luminous, he needs to solve the lightsaber problem to before lightsaber follows luminous fate.

At that point Matthew decided he doesn’t have any time to waste. He ordered the engineer to take the thaumatic (magical) radiators and position them on the luminous – to reduce the general amount of magical energy, to create a magical scenic so that luminous has more time before it mixes completely.

Another order – luminous has a very heavily shielded cargo bay. This one is usually used for dangerous anomalies. Matthew ordered that all the crew members of luminous enter that particular cargo bay and they are supposed to be contained there and only there. That way lightsaber should not get contaminated as quickly.

(VVXO). Matthew stands succeeded, however during that process an engineer contaminated himself. With sadness, Matthew ordered the Marines to put him in the containment chamber with the luminous refugees. During all of that Marines escorting an engineer and placing the radiators, one of them got very hurt and also magically corrupted. That guy was sent to the medbay.

A current set of information both Annette and Nash possess the them one direct conclusion – some were inside the ship graveyard there exists an original pioneer. Matthew has two options – either shield lightsaber, believe in the ship, make sure everything works and fly as fast as possible to leave the anomaly or find the pioneer and solve the direct problem.

The first approach is safer and allows the lightsaber to save people already on the ship. The second approach is way more dangerous and difficult but allows the lightsaber to save more of the luminous crew, including those people who at this point are corrupted by this strange energy.

Matthew decided that this is not really a choice. He ordered Annette to go outside, into the void to use her delicate senses and find where the pioneer is located.

Annette used her magic and connected it with lightsaber systems. (VVXV). She sent the mental net searching for the signal of original pioneer. She succeeded – she found the coordinates of the pioneer and she learned that some of its crew may be alive. It may be possible to save some of pioneers crew.

But Annette felt the presence. It was Annika. Annika whispered that she needs Annette to merge with Persephone – that way they will be able to undo everything. Annette felt the order given to her by the ghost entity (VX) - she was able to push it away for a while. After several days in the anomaly Annette won’t be able to hold off. She will comply whatever Annika wants her to do.

This is one of the worse fates Annette can imagine for herself. As far as possible, she has returned to the lightsaber and warn the captain what is happening. Also she told the captain where pioneer is located. Matthew ordered Annette to make some simulations – what can they do to push luminous outside of the anomaly and in the same time use the lightsaber to go deeper into the anomaly to get to pioneers education.

(OOXX): Annette found one or two ways how this can be done. Unfortunately, every single approach requires doing some dark stuff, things the admiralty will look at with grant contempt. Matthew decided that saving some more people from the luminous is worth torturing some other people who were bound to die anyway. And so he ordered the lightsaber to execute this plan.

Matthews crew was not really happy about it, but they noted captain so they did what was ordered. Some of the Marines were very unhappy - they did not sign up for this type of work, but it was impossible to miss the fact that they have saved both luminous and more of the corrupted crew then with any other approach.

And so after luminous exit the anomaly, the lightsaber returned to save the pioneer. The hunter killers what he is caught helping the luminous get deeper and deeper into the darkness.

(VXVXV): the venture into the deep anomaly was hard on the lightsaber. The hunter killers were helping the lightsaber destroyer hunter killers and clear the path, while the crew was navigating the lightsaber among the derelict ships and malformed magical structures. The closer they got to the pioneer, the stronger was Annika’s grip on the hunter killers and the weaker was Persephone’s grip on them. But the lightsaber itself survived. This spaceship truly was a technological marvel.

When they approached the pioneer, Matthew ordered the lightsaber’s tractor beam and pull pioneer with them. Unfortunately, pioneer became a part of the anomaly. It was not possible to pull it away from there. But Annette used her connection to the ghost and once again worked with the sensors of the lightsaber. (VV)

What Annette saw on the monitors was unfortunate. The pioneer wasn’t really here. Inside the pioneer there was some kind of techno-biological amalgam. There was absolutely no normal life forms inside. The ship was completely anomalous. There was nothing the lightsaber could have done.

But Annette had an idea – she asked Annika to pull from her memories and try to re-create what ever used to be in the pioneer. After all, this particular anomaly could collapse realities, therefore it might be possible to properly extract the people.

And indeed it had worked – the corrupted pioneer has burst the hibernation capsules with the crew inside, minus Annika. But this act depleted Annika’s grip on pioneers reality. The bio-techno-magical amalgam which used to be Annika started to randomly and rapidly expand in every possible direction, feeling extreme pain. Because Annika was linked with Annette, Annette felt the same pain too. She begged the captain to make it stop.

Matthew first made sure the hibernation capsules and databanks from the pioneer were recovered and then shot three thaumic torpedoes directly into the pulsating blob which used to be pioneer. The explosion of the torpedoes meeting the volatile anomaly has managed to scar the lightsaber, but this tractor of the spaceship survived. Limping, the lightsaber left the anomaly. And saved the Luminous.

Mission completed.

## Streszczenie

Luminus - statek użytkowy Orbitera - wysłał SOS z Cmentarzyska Statków w Anomalii Kolapsu. Miecz Światła przybył mu na pomoc; okazało się, że Luminus jest zainfekowany przez echo Pioniera 774 - pierwszego statku który odkrył ową Anomalię. Miecz Światła uratował tyle załogi ile był w stanie, wydobył Luminus z Anomalii i nawet udało mu się uratować część zahibernowanej załogi Pioniera - niszcząc przy okazji gasnące echo oficer naukowej Pioniera, która spowodowała te wszystkie problemy.

## Progresja

* Mateusz Sowiński: dostał potężny opieprz od Admiralicji za: narażenie statku, nieludzkie metody ratowania Skażonej załogi Luminusa. Ale dzięki temu - uratował więcej.
* Mateusz Sowiński: dostał pochwałę od Admiralicji za: uratowanie Luminusa, zdobycie banków danych i części załogi legendarnego Pioniera 774.

### Frakcji

* .

## Zasługi

* Mateusz Sowiński: kapitan Miecza Światła; podjął serię ryzykownych decyzji by uratować załogę Luminusa i Pioniera. Osobiście poznał, że Luminus stapia się z Pionierem w Anomalii Kolapsu.
* Aneta Szermen: oficer naukowy Miecza Światła; zainfekowana przez ducha Aniki (o.n. Pioniera), wykryła gdzie znajduje się Pionier w Anomalii i wiele razy rekalibrowała sensory Miecza by móc zlokalizować co tu się dzieje.
* Maciej Brzeszczak: oficer inżynier Miecza Światła; skutecznie wprowadził odpromienniki thaumiczne na Luminusie, acz zainfekował się echem Pioniera; skończył w izolatce.
* Tobiasz Agronom: kapitan Luminusa; częściowo zmiksował się z nim kapitan Olaf z Pioniera. Póki był w dobrej formie, póty dobrze dowodził. Potem, cóż. Skończył w izolatce.
* Ildefons Szombar: oficer medyczny Miecza Światła; dobry lekarz, ale czasami pije na służbie. Widział ducha Anniki, co natychmiast zgłosił.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Libracja Lirańska
                1. Anomalia Kolapsu
                    1. Cmentarzysko Statków: gdzie Miecz Światła uratował Luminus a potem wyciągnął ze starego Pioniera banki danych i załogę

## Czas

* Opóźnienie: -250
* Dni: 3
