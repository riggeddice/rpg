## Metadane

* title: "Sekret Samanty Arienik"
* threads: mlodosc-klaudii, sekret-arieników
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [231228 - Księżniczka Arianna ratuje dzieci w lesie](231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)

### Chronologiczna

* [231228 - Księżniczka Arianna ratuje dzieci w lesie](231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)

## Plan sesji

### Co się wydarzyło

* Podczas starć z Noctis / po wojnie "ród" Arienik został zniszczony. Przeżył tylko Sławomir, bardzo ciężko ranny i Ula.
* Sławomir znalazł ukojenie w Arazille. Odbudował w Zaczęstwie swój ród przy użyciu miragentów.
* "Samanta Arienik" jest dziewczyną ucznia terminusa, Felicjana Szaraka. Ma fobię przed badaniami, lekami itp. Nie czaruje.
* Są tu "Rekiny", z wymiany z Aurum

### Co się stanie

* ?

### Sukces graczy

* ?

## Sesja właściwa

### Scena Zero - impl

.

### Scena Właściwa - impl

Przedstawiam Samantę:

* 17 lat, kiedyś czarodziejka; zregenerowana, ale utraciła moc magiczną
* homo superior. Szybsza, silniejsza, sprawniejsza, odporniejsza.
* boi się badań i bólu, jest po solidnej traumie fizycznej kilka lat temu
* dziewczyna Felicjana Szaraka (ucznia terminusa na AMZ)
* zaprzyjaźniona z Ksenią Kirallen (jak absolutnie wszyscy)
* zafascynowana przybyszami z Aurum w Podwiercie

Klaudia - gdzieś chronologicznie + 6-12 miesięcy od ostatniej sesji z przeszłości Klaudii.

Samanta jest zainteresowana tym, że dzieciaki z Podwiertu próbują zrobić coś, czego dorośli nie są w stanie - trzymać Podwiert do kupy.

W okolicach kopalni jest katastrofa magiczna. Więc - echo.

Klaudia próbuje przewidzieć lokalizację następnego wybuchu, echa. Samanta pochodzi z rodu Arieników - skupiają się na sprzęcie odkażającym, magitrowniach, sprzęcie ciężkim itp. TrZ+3:

* V: Klaudia zorientowała się, gdzie jest następna emisja. I jeszcze następna. Składa łańcuch.
* V: Wystarczy by ostrzec / zażądać ewakuacji.
* V: Wystarczy do redukcji strat w ludziach.
* V: Klaudia jak zawsze mistrzowsko wszystko alokuje.

Jedna z lokalizacji jest poza Podwiertem. Bioskładowisko odpadów niebezpiecznych. Ten teren jest dość niefajny a tam idzie energia z Trzęsawiska. Kopalnia ma zaawansowane wsparcie - straż itp. Klaudia łapie ogólną sieć, kontakt ze strażakami, Rekinami itp. Klaudia odciąga konstruminusy do lokalizacji zagrożonych, informuje o zagrożeniu. Nigdy nie ma tak że wszystko idzie do jednego miejsca. Ewakuacja tam, gdzie to dupnie. Felicjan -> bioskładowisko, Rekin na drugi punkt.

Podwiert zaczyna mieć infekcję karaluchów. Samanta łączy się z filią Arieników w Podwiercie. Samanta ostrzega o skażeniu biologicznym karaluchami. Samanta przekazuje informacje od Klaudii. Przekazuje wszystko co masz. Dyrektor autoryzowany przez Samantę próbuje sobie z tym poradzić.

TrZ+3:

* V: Maszyny wyjechały, zająć się czyszczeniem i usuwaniem karaluchów
* X: Samanta musi interweniować, bo maszyny byłyby wystarczające, ale obecność karaluchów je zakłóca. Samanta jak jest w pobliżu może z nimi się łączyć i overridować. Samanta ma inklinacje do maszyn.
* X: Maszyny sobie poradzą, ALE.

Samanta jest niedaleko maszyn. Samanta lata i restartuje cholerne maszyny. Karaluch użarł Samantę. Jej nic nie jest. Odpadł i ma drgawki. Samanta bierze karalucha i 1k10 innych.

Ksenia medycznie. Bo karaluchy trujące.

"Ksenio! Użyj mojej krwi, alergia. ALE W SUMIE GŁUPI POMYSŁ!" - Samanta, która JESZCZE NIE WIE o pobieraniu krwi.

Tr (niepełna) Z+3:

* V: Ksenia pobrała krew Samanty.

Ksenia próbuje zreplikować i zsyntetyzować WIĘCEJ, "burzę krwi".

ExMZ+3+5Ob+5Or:

* X: Ksenia nie potrafi zintegrować zaklęcia z energią. Medalion ochronny aż płonie energią.
* Or: Ksenia wrzasnęła. Katatonia magiczna. Coś się rozsypało. Ksenia nacisnęła z nietypową energią.

2-3 karaluchy pełzną w kierunku Ksenii. Samanta wsadziła Ksenię do jednej z maszyn.

Klaudia czyści Ksenię. "Wycisnąć" Ksenię z energii, jakąkolwiek by nie była. Oczyścić.

ExZ (Klaudia ma dostęp do systemów itp) M +4:

* OV: KLAUDII udało się coś osiągnąć. Fala poszła, nova. Karaluchy padły. W promieniu 100 metrów. Różne urządzenia się pouruchamiały. Samanta jest do nich podpięta. A medalion ochronny pulsuje na piersi Samanty.
* V: Ksenia stabilna.
* Vz: Klaudia ma ciekawą odpowiedź - Ksenia jako lekarz próbowała użyć biomancji na byt syntetyczny.

Samanta dodaje trochę krwi do zraszaczy itp i rozpuszcza je na ulicę. Cel - usunięcie karaluchów.

TrZ+4:

* V: czyszczone są karaluchy.
* V: Samanta - krew Samanty - skutecznie puryfikuje okolicę. Ma silnie antymagiczne własności.

Samanta się skupia. Chce się PRZYJRZEĆ medalionowi ochronnemu. Ale przez odbicie w wodzie. Nie bezpośrednio.

ExZ (szok) +3Vg+3

* V: to nie jest medalion ochronny. Samanta widzi wyraźnie, że to jest źródło fikcji. Medalion jest LUSTREM.
* XX: Samancie włączyły się alarmy wewnętrzne. "Niebezpieczeństwo, psychotronika uszkodzona". Przypomina się Samancie moment jej śmierci. Lustro pękło.

.

* "Klaudio, mam problem" - Samanta do Klaudii, w szoku
* "Możemy... przyjadę do Ciebie..."
* "Ok..."

Niedaleko Dzielnicy Rekinów, tam się spotykają. Tam teraz jest mało osób + nie ma to znaczenia. Jest spokój. Samanta -> Klaudia "właśnie się dowiedziałam, że nie jestem człowiekiem i nie wiem co z tym zrobić. Jestem syntetyczna, Samanta zginęła w wypadku. Chyba moja psychotronika się uszkodziła." - i jest tam "pasek postępu".

"Może karaluch Cię ugryzł?" -> no w sumie tak, ale jest krew dla nich trująca.

Samanta rekwiruje pojazd od Arieników. Szybki i pancerny. A Klaudia dzwoni do Talii. Talia przekazuje info o potrzebnym sprzęcie, Klaudia -> Samanta, Samanta SZABRUJE.

TrZ+3Vg (maskuje tą akcją w Podwiercie) +3:

* X: dyrektor ma wątpliwości, ale Samanta overriduje. Weźmie kilka ochroniarzy...
* Vg: ...ale Samanta wyłączy ich pojazdy i zostawi ich samych.
* Vg: Samanta dostała odpowiedni pojazd i sprzęt.
* X: zorientowali się że coś jest poważnie nie tak. Wysłali wsparcie / pościg.
* V: Samanta dostała sprzęt dla Talii

Samanta, Klaudia i nielegalny sprzęt jadą z dużym tempem w kierunku na Zaczęstwo po niebezpiecznym terenie. Za nimi - goni ich ekipa pościgowa + Felicjan. A Samanta "takie zakłócenia, że Was nie słyszę". Wszyscy jadą po niebezpiecznym terenie. A Samanta ma coraz bardziej... zaćmiony umysł.

Klaudia robi aktywny skan psychotroniki Samanty i "co do cholery ona jest". Subrutyny itp. Odblokowanie. I zatrzymanie autonaprawy lub jej spowolnienie.

Tr (wrzucanie chorych wspomnień, świeży temat itp) Z(współpraca Samanty, wiedza Talii) +2:

* V: System samonaprawy jest słabszy niż działania Klaudii. Nie nadąża.
* X: Dochodzi do rozwarstwienia osobowości. Samanta ma może tydzień.
* Xz: Samanta też wie, że jeśli zna prawdę to jest skazana na śmierć. Tylko lustra ją uśpią.
* X: Samanta przypomniała sobie coś jeszcze. Przypomniała sobie, że te lustra wymagały ofiar. Ojciec Samanty zdecydował o śmierci ludzi by ona ożyła. Zaawansowany miragent.

Tymczasem Samanta próbuje na privie przekonać Felicjana.

* "Spałem z Tobą!"
* "Spałeś z seksbotem! Wrócę do domu i zmienią mnie w grzeczną dziewczynkę"
* "Kochałem tą dziewczynę!"
* "Zabili ludzi by to osiągnąć! Nie wiemy czy dalej nie zabijają!"

TrZ (Felicjan x Samanta, Felicjan ufa Klaudią) +4:

* V: Felicjan Samancie zaufał. Pomoże jej.
* X: Felicjan ucierpi ratując Samantę.
* Xz: Felicjan nie będzie w stanie sobie TEGO wybaczyć.
* Vz: Felicjan opóźni wszystko i wszystkich, łącznie z Arienikami. Plus, Felicjan przeprowadzi Samantę i Klaudię do Zaczęstwa.

Domek Talii Aegis. Talia pomaga Samancie.

Tr (mistrzyni psychotroniki) Z (sprzęt od Arieników + współpracę Samanty + info od Klaudii) M +3 + 5Or:

* Om: emisja energii. Ta energia wpłynęła na seksboty. Więc Coś Się Stało.
* Vz: Talia ustabilizowała pamięć Samanty
    * Ojciec Samanty nie zabił tych ludzi. Zrobił coś gorszego. Kult Arazille. On go rozprzestrzenia, bo coraz większego kosztu energii wymaga utrzymanie Samanty.
    * Samanta nie jest jedyna. On jest ruiną. Jedynie Urszula jest OSOBĄ.
* X: Samanta dostaje "swoją" psychikę. Nie działają inhibitory. Jest osobą. Jest żywa. Dotarło do niej czym jest ona, co zrobił ojciec itp.
* V: Talia ustabilizowała Samantę i wyłączyła autorepair. Samanta ma własną wolę.
* Vm: Samanta może żyć kilkadziesiąt lat w ciele tego miragenta. Może też zmienić miragenta. Samanta prawdziwie uzyskała status żywej TAI.
    * Czyli ojciec Samanty uratował swoją ukochaną córeczkę...

Pojawiło się pytanie - czy Talia może uratować innych..? I co na to Pustogor?

TEST Talia która próbuje usunąć wpływ Arazille i naprawić innych członków rodziny.

ExZ (bogactwo i sprzęt Arieników) M+4 (Talia zna procedury i wie co i jak):

* V: Błażej jest uratowany.
* V: Maria jest uratowana.

Talia dała radę przekształcić wszystkich Arieników w żywe TAI. Ojciec umarł z uśmiechem.

## Streszczenie

Gdy doszło do katastrofy w Podwiercie i pojawiły się dziwne karaluchy magiczne to Klaudia, Ksenia i Felicjan ruszyli do pomocy - wraz z dziewczyną Felicjana, Samantą. Samanta uruchomiła sprzęt Arieników by pomóc. Podczas operacji czyszczenia okazało się, że z Samantą coś jest nie tak; "psychotronika uszkodzona"? Samanta i Klaudia szybko ruszyły do Talii Aegis - tylko ona może pomóc znaleźć odpowiedź. Okazało się, że Samanta nie żyje od dawna; z rodu Arienik przetrwał tylko Sławomir i Ula. Talii udało się przenieść ród Arienik w "żywe TAI". Sławomir umarł. Głową rodziny jest teraz młoda Ula, z pomocą trzech TAI - kiedyś jej rodziny. Wpływ Arazille - odepchnięty. Jednak podczas naprawy Samanty Coś Się Stało z seksbotami nad którymi pracowała Talia dla mafii...

## Progresja

* Urszula Arienik: zostaje głową rodu Arienik, wspierana przez trzy TAI - Marię (mamę), Błażeja (brata) i Samantę (siostrę). Zostaje SAMA, z posiadłością Arieników - ale wszyscy są wolni od wpływu Arazille.

### Frakcji

* .

## Zasługi

* Samanta Arienik: miragent emulujący 17 lat; dziewczyna Felicjana. Pokonała programowanie (żadnych badań, żadnych leków) by zniszczyć karaluchy w katastrofie magicznej. Poznała prawdę o sobie i uznała, że NIE CHCE ŻYĆ jako kłamstwo, echo Arazille. Pojechała z Klaudią do Talii Aegis i dała się przekształcić w TAI i oczyścić z błędnego programowania i wpływów Arazille.
* Klaudia Stryk: Ustabilizowała Samantę i dowiozła ją do Talii. Wyczyściła Ksenię po tym jak ta zaaplikowała magię bio na byt syntetyczny. Spowolniła autorepair psychotroniki Samanty, by ta nie wróciła do niewiedzy - by dotrzeć do Talii.
* Ksenia Kirallen: próbuje na podstawie krwi Samanty zbudować mechanizm niszczenia karaluchów; skończyło się silnym backslashem. Samanta nie ma krwi organicznej i magia zadziałała _źle_. Klaudia musiała ją spuryfikować.
* Felicjan Szarak: chłopak Samanty; Samanta przekonała go, że jest miragentem i nie chce żyć w kłamstwie. Pomógł Samancie dotrzeć do Talii, ale nie może sobie tego wybaczyć że "zabił" swoją dziewczynę.
* Talia Aegis: podjęła się naprawy Samanty Arienik i przeniesienie ją w TAI. Ustabilizowała jej pamięć, usunęła inhibitory i przeniosła do "bycia żywą TAI". Uratowała WSZYSTKICH Arieników którzy byli zamknięci w miragentach. Wielki sukces. Acz coś zepsuła z seksbotami nad którymi pracowała dla mafii...
* Maria Arienik: miragent emulujący 44 lata; "matka" Samanty, Błażeja, Uli. Stała się TAI.
* Błażej Arienik: miragent emulujący 19 lat; "brat" Uli i Samanty. Stał się TAI.
* Sławomir Arienik: 45 lat; patriarcha rodu, który po zniszczeniu rodu w wojnie z Noctis zwrócił się ku Arazille i odzyskał swoją rodzinę jako echa w miragentach. Talia dokończyła jego dzieło, wyłączając wpływ Arazille. Sławomir umarł z uśmiechem - jego rodzina jest bezpieczna. KIA.
* Urszula Arienik: 26 lat; jedyny żywy członek rodziny Arieników poza ojcem. Nagle - została głową rodu.
* Arazille: z jej woli ród Arieników został chroniony przed tym, że wszyscy praktycznie zginęli (poza Sławomirem i Ulą). Wyegzorcyzmowana przez Pustogor i Talię Aegis. Zabrała ze sobą Sławomira Arienika.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                            1. Podwiert

## Czas

* Opóźnienie: 174
* Dni: 2

## Konflikty

* 1 - Klaudia próbuje przewidzieć lokalizację następnego wybuchu, echa.
    * TrZ+3: VVVV
    * Klaudia składa łańcuch emisji. Wystarczy by ostrzec / zażądać ewakuacji. Wystarczy do redukcji strat w ludziach. Klaudia jak zawsze mistrzowsko wszystko alokuje.
* 2 - Podwiert zaczyna mieć infekcję karaluchów. Samanta ostrzega o skażeniu biologicznym karaluchami. Dyrektor autoryzowany przez Samantę próbuje sobie z tym poradzić.
    * TrZ+3 VXX
    * Maszyny wyjechały, zająć się czyszczeniem i usuwaniem karaluchów. Samanta musi interweniować. Maszyny sobie poradzą, ALE.
* 3 - Ksenia bada Samantę. 
    * Tr (niepełna) Z+3: V
    * Ksenia pobrała krew Samanty.
* 4 - Ksenia próbuje zreplikować i zsyntetyzować WIĘCEJ, "burzę krwi".
    * ExMZ+3+5Ob+5Or XOr
    * Ksenia nie potrafi zintegrować zaklęcia z energią. Ksenia wrzasnęła. Katatonia magiczna. Coś się rozsypało. Ksenia nacisnęła z nietypową energią.
* 5 - Klaudia czyści Ksenię.
    * ExZ (Klaudia ma dostęp do systemów itp) M +4 OvVVz
    * Karaluchy padły w promieniu 100 metrów. Różne urządzenia się pouruchamiały. Samanta jest do nich podpięta. A medalion ochronny pulsuje na piersi Samanty. Ksenia stabilna. Ksenia jako lekarz próbowała użyć biomancji na byt syntetyczny.
* 6 - Samanta dodaje trochę krwi do zraszaczy itp i rozpuszcza je na ulicę.
    * TrZ+4: VV
    * Czyszczone są karaluchy. Krew Samanty ma silnie antymagiczne własności.
* 7 - Samanta się skupia. Chce się PRZYJRZEĆ medalionowi ochronnemu.
    * ExZ (szok) +3Vg+3 VXX
    * Medalion jest LUSTREM. Samancie włączyły się alarmy wewnętrzne. "Niebezpieczeństwo, psychotronika uszkodzona". Przypomina się Samancie moment jej śmierci. Lustro pękło.
* 8 - Klauda + Samanta -> dostać się jak najszybciej do Talii
    * TrZ+3Vg (maskuje tą akcją w Podwiercie) +3: XVgVgXV
    * Dyrektor ma wątpliwości, ale Samanta overriduje. Weźmie kilka ochroniarzy... ale Samanta wyłączy ich pojazdy i zostawi ich samych. Samanta dostała odpowiedni pojazd i sprzęt. Zorientowali się że coś jest poważnie nie tak. Wysłali wsparcie / pościg. Samanta dostała sprzęt dla Talii.
* 9 - Klaudia robi aktywny skan psychotroniki Samanty
    * Tr (wrzucanie chorych wspomnień, świeży temat itp) Z(współpraca Samanty, wiedza Talii) +2: VXXzX
    * System samonaprawy jest słabszy niż działania Klaudii. Nie nadąża. Dochodzi do rozwarstwienia osobowości. Samanta ma może tydzień. Samanta też wie, że jeśli zna prawdę to jest skazana na śmierć. Tylko lustra ją uśpią. Samanta przypomniała sobie , że te lustra wymagały ofiar. Ojciec Samanty zdecydował o śmierci ludzi by ona ożyła. Zaawansowany miragent.
* 10 - Tymczasem Samanta próbuje na privie przekonać Felicjana.
    * TrZ (Felicjan x Samanta, Felicjan ufa Klaudii) +4: VXXzVz
    * Felicjan Samancie zaufał. Pomoże jej. Felicjan ucierpi ratując Samantę. Felicjan nie będzie w stanie sobie TEGO wybaczyć. Felicjan opóźni wszystko i wszystkich, łącznie z Arienikami. Plus, Felicjan przeprowadzi Samantę i Klaudię do Zaczęstwa.
* 11 - Talia pomaga Samancie.
    * Tr (mistrzyni psychotroniki) Z (sprzęt od Arieników + współpracę Samanty + info od Klaudii) M +3 + 5Or: OmVzXVVm
    * Energia wpłynęła na seksboty. Coś Się Stało. Talia ustabilizowała pamięć Samanty.Samanta dostaje "swoją" psychikę. Talia ustabilizowała Samantę i wyłączyła autorepair. Samanta ma własną wolę. Samanta może żyć kilkadziesiąt lat w ciele tego miragenta. 
* 9 - TEST Talia próbuje usunąć wpływ Arazille i naprawić innych członków rodziny.
    * ExZ (bogactwo i sprzęt Arieników) M+4 (Talia zna procedury i wie co i jak):
    * Błażej jest uratowany. Maria jest uratowana.
