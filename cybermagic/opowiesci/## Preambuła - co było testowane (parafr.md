## Preambuła - co było testowane (parafraza rozmów)

1. "Niech to będzie śledztwo typu dedukcyjnego"
    * rozumienie: śledztwo gdzie coś się stało i Wy "odkrywacie fakty" a nie "generujecie kto zabił" (przez indukcję lub abdukcję).
2. "Ale niech to nie będzie mały byt, niech świat będzie otwarty"
    * rozumienie: możecie iść gdzie chcecie, robić co chcecie i są różne wątki które możecie podjąć lub nie, jak przy Cesarzowej.
    * rozumienie: bardzo dużo wątkówi i możliwości, gdzie podejmiecie tylko to co Was interesuje; mam Was nie ograniczać.
3. "Chcemy móc robić nasze własne postacie"
    * Moja interpretacja: Waszą odpowiedzialnością jest zapewnienie, by postacie miały kontrolę niszy oraz był powód by działały razem. 
    * Ja Wam pomogę z Pozycjonowaniem jak jestem w stanie, ale jeśli bierzecie coś dla siebie, dostajecie 100% odpowiedzialności za domenę.
4. "Nie chcemy się rozdzielać, niech postacie działają razem w scenach"
    * rozumienie: **Wam** na tym zależy, ja umożliwię Wam byście mogli wygrać sesję jeśli zadziałacie w ten sposób.
    * rozumienie: **Wy** będziecie o to walczyć, bo to są ruchy Graczy a nie ruchy MG. MG nie decyduje o ruchach Graczy i ich postaci.
















## 4. "Nie chcemy się rozdzielać"

Paweł, cytat z Ciebie: "mam wrażenie, że na jakimś etapie główny tej cel sesji to było sprawdzenie, jak się ma rozwikłanie tego typu intrygi w **drużynie która ze sobą współpracuje**, to jest **wspólnie uczestniczy w scenach itd.**, bo miałem wrażenie, że trudno to zrobić w EZ".

Z mojej perspektywy, ostatecznie to **Wy** zdecydowaliście się tego nie sprawdzać. Wyjaśnienie poniżej.

Niestety, **to należy do Was, nie do mnie.** Ja jako MG nie mogę sterować Waszymi ruchami, mogę jedynie Was zachęcać (ALE: patrz "otwarty świat", gdzie chcecie mieć wolność). Zrobiłem więc wszystko co mogłem by Was zachęcić:

* pokazałem Wam w scenie zero, że jak współpracujecie to budujecie na swoich siłach
* zostawiłem Was w pierwszej scenie razem, w Skorpionie, mówiąc co możecie zrobić
* zapewniłem, że sesja jest rozwiązywalna i nawet dość prosta jeśli się nie rozdzielicie

Jedyne czego nie zrobiłem to twarde dodanie tego do Kontraktu, ale Wy mogliście to zrobić. To Wy decydujecie o swoich ruchach. Wy zdecydowaliście czy iść razem czy się rozdzielić. Pierwszy, precedensowy ruch który dałem Pawłowi? "Idę do Jadwigi, sam", co nadało przykład innym Graczom i wszyscy Gracze zaczęli działać solo. 

W czym - zauważcie - filmy typu kryminał mają rozdzielanie się detektywów. Rzadko jeśli jest **kilka** głównych postaci to detektywi działają razem. Co innego Poirot i Hastings, czy Holmes i Watson, gdzie drugi jest tylko wsparciem tego pierwszego.

Co przetestowaliśmy?

1. **"Współpracuje"** - to zrobiliście. To robiliście cały czas. Nie w jednej przestrzeni ale jako **Gracze** współpracowaliście ze sobą.
2. **"Współpraca" w scenie zero** - to zrobiliście. Udowodniliście, że umiecie budować na swoich ruchach i działaniach.

Reszta należy do Was, nie do mnie. Przypominam - nie jest moją rolą jako MG mówić Wam co Wy macie robić. Jeśli to było kluczowe i o tym zapomniałem, mogliście dodać do Kontraktu. To też nie zostało zrobione.

Więc jak ważne dla Was to serio było?

Ja też jestem tu by się bawić; nie będę Was moderował w zakresie rzeczy które należą do Was - zwłaszcza że Wy robiliście postacie więc to Wasza, nie moja odpowiedzialność by zapewnić ten komponent Pozycjonowania.


## 5. Podsumowanie


