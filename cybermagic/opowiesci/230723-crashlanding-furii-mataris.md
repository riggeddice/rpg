## Metadane

* title: "Crashlanding Furii Mataris"
* threads: pronoktianska-mafia-kajrata, furia-mataris-agentka-mafii
* motives: manhunt, teren-morderczy, echa-inwazji-noctis, bardzo-grozne-potwory, izolacja-zespolu, misja-survivalowa
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [230723 - Crashlanding Furii Mataris](230723-crashlanding-furii-mataris)

### Chronologiczna

* [230723 - Crashlanding Furii Mataris](230723-crashlanding-furii-mataris)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ?
* Struktura: 
    * survive-and-hold
* CEL: 
    * MG
    * Drużynowy

### Co się stało i co wiemy

* Furie Mataris - wyspecjalizowane do walki na planecie.
* Zestrzelone, rozbiły się na południu Pustogoru

### Co się stanie (what will happen)

* F1: Lądowanie
    * Wiadomość od Kajrata "uważajcie, działam w pobliżu, przechwycę Was; nie dajcie się złapać! wrogowie mają magię!"
    * Amanda ma spotkanie z awianem i od razu z groźnym paszczogruntem
    * Na Amandę polują 'psy', Amanda ucieka przez groźny teren; awian próbuje ją ostrzelać i informuje innych
* F2: Konsolidacja
    * Ayna odpala Swarmlingi, Xavera strzela do awiana
    * Chowamy się przed potworami oraz polujowcami
    * Wioska noktiańska pod kontrolą Kwiatu; Xavera enthralled
    * Drzewa żywią się krwią
    * Droga do Pacyfiki; anomalia przestrzenna
* F3: Rozdzielone
    * Legion Pacyfiki
    * Trzy awiany i mag, próba dominacji i przejęcia kontroli nad Amandą
    * Ernest Kajrat i sześciu noktian

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Xavera Sirtas
    * OCEAN: (C+A-): żywy ogień, jej dusza jest pełna pasji, a jej słowa ostrzem. "Kto siedzi cicho, tego spotyka wszystko co najgorsze"
    * VALS: (Security, Tradition): wszystko odnosi do tego co już widziała i co było, działa dość tradycyjnie. "Wszystko już było i się stało"
    * Core Wound - Lie: "Przeze to, że dużo mówiłam i szperałam, moi rodzice się rozwiedli" - "Będę walczyć i ognistą walką udowodnię, że warto być razem!"
    * Styl: Zawsze gotowa do walki. Agresywna i asertywna. Dobrze wybiera walki. Jej upór często wpędza ją w kłopoty.
* Ayna Marialin
    * OCEAN: (N+O+): wiruje kalejdoskopem pomysłów, które stale ją inspirują i napędzają. Wiecznie martwi się jutrem i planuje. "WIEM, że nie ma niedźwiedzi. A jeśli są?"
    * VALS: (Family, Universalism): pozytywny wpływ przy pomocy Zespołu uratuje wszystko. "Jesteśmy w stanie kształtować naszą przyszłość. Ostrożnie."
    * Core Wound - Lie: "Nie jestem tak dobra w niczym jak moje przyjaciółki; przeze mnie coś im się stanie" - "Dojdę do tego, rozwiążę to! Nie zaskoczą nas niczym!"
    * Styl: Staranna i metodyczna, Ayna zawsze analizuje sytuację zanim podejmie działanie.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Kapsuła ratunkowa. Czerwone alarmy. Jesteś Furią Mataris - augmentowaną homo superior. Twój statek, Twój dom został zniszczony. Masz nadzieję, że inne Furie przetrwały. Widzisz miasto, ale kapsuła kieruje się w stronę innego obiektu, poza miastem - coś na kształt arkologii. I wiadomość na komunikatorze: "Tu major Ernest Kajrat, nie kierujcie się na Pustogor, kierujcie się na Pacyfikę; tam jestem w stanie Was wyciągnąć. Niebezpieczny teren (streszczenie zagrożeń terenu). Łowcy na Was będą polować. Nie dajcie się złapać. Przyjdę po Was."

Amanda doceniła optymizm Kajrata. Ale tymczasem leci i się elegancko musi rozbić. W pobliżu ma na HUD inne kapsuły, inne Furie. Nie jest sama. Przynajmniej tyle. DUPNĘŁO.

Amanda się otrząsnęła; świetne zabezpieczenia. Szybko bierze snajperkę, radio, backpack i wyskakuje. Znaleźć w miarę bezpieczne miejsce z którego może coś zrobić. Jej HUD pokazuje lokalizację innej kapsuły stosunkowo niedaleko, w stronę na Pacyfikę. Amanda usłyszała charakterystyczny dźwięk awiana. Amanda szybkim przełącznikiem przestawia kapsułę w slightly delayed self-destruct.

Amanda korzysta z eksplozji kapsuły by odpełznąć dyskretnie i nie dać się zlokalizować.

Tr +3:

* X (p): zorientują się trochę później
* V (p): Amanda ma okazję zejść z fatalnego dla niej terenu
* Vr (m): Amanda da radę się ukryć w Lesie Pustym przed awianem

Amanda nie ma żadnych sygnałów, ale są procedury operacyjne. Amanda WIE, że one albo pójdą do Lasu albo do Pacyfiki. Amanda jest sama do momentu konsolidacji. Cicho pełznie do lasu, unikając awiana, szukając przewagi. Gorzej - Amanda widzi WIĘCEJ niż jeden awian. Na samą Amandę przeznaczono trzy awiany...

Amanda weszła do lasu, ostrożnie. Od razu rzucił jej się w oczy żołnierz noktiański - a raczej to co z niego zostało - leżący przy drzewie. Drzewo wbiło w niego gałęzie i go wyssało. Amanda omija szerokim łukiem.

Tr Z +3:

* X (p): Amanda NIE MOŻE tu zostać
* V (p): Amanda widzi różne pnącza i inne elementy zmierzające w jej stronę; jej servar jest w stanie odgarnąć i zerwać, ale ich jest więcej. Amanda w długą.
    * WSZYSTKIE drzewa są niebezpieczne i chcą się nią pożywić
* V (m): Amanda przebija się przez drzewa nie otwierając ognia (by nikt jej nie wykrył), używając wibroostrza by się przebić i naciska lekko uszkodzony servar do maksimum
* X (m): po drugiej stronie drzew awiany złapią ślad Amandy
    * z dobrych wieści, Amanda zbliża się do INNEJ kapsuły
* V (e): Amanda dociera bliżej kapsuły, przebijając się przez cholerne drzewa i zauważa jeden servar sojuszniczki oplątany mocno pnączami; sojuszniczka z nimi walczy jak może
    * przewaga Amandy: Lucia nie ma wibroostrza. Amanda ma.
    * Amanda: "Lucia, awaryjny coolant drop!" i atakuje wibroostrzem (+3Vg)
* Xz (e): Lucia zrobiła co miała, ale pnącz jest coraz więcej; Lucia MUSIAŁA zacząć strzelać. Strzelanie sprawia, że Lucia jest w stanie się wydobyć.
    * Amanda zostawia Lucię; nie ma opcji, Lucia zostanie złapana
        * jeśli cokolwiek mają zrobić, nie mogą dać się wszyscy złapać
* V (e+): Amanda korzysta, że Lucia została z tyłu i zarówno awiany jak i pnącza skupiają się na niej. Używa Lucii jako dywersji i wydostała się w innym miejscu. Korzystając z okazji - w długą, ile silnik dał.

Amanda widzi 'z tyłu', jak do Lucii zbliżają się awiany. I sama "widzi" coś co się do niej zbliża - psy? Jakieś dziwne.

Amanda wyznacza trasę w kierunku na Pacyfikę, nie jest pierwszą oczywistą trasą. Jest niedaleko tej trasy takie... "gniazdo szerszeni", ale większe i paskudniejsze. Amanda zbliża się do 'granicy szerszeni' by szerszenie skierować przeciwko psom. Pełna prędkość servara Amandy. Przebiec i wkurzyć szerszenie by psy zostały z tyłu. Nie używa broni, ale bierze skałę i rzuca w gniazdo.

Tr Z +4:

* V (p): Amanda bezpiecznie przebiega niedaleko szerszeni i je aktywuje
* X (p): Jeden awian leci w tą stronę, by zobaczyć psy i co tu się stało
* V (m): Szerszenie usuwają psy; Amanda jest bezpieczna

Przeciwnicy spodziewają się coś znaleźć w okolicy psów, więc Amanda - ukryta z ciężką snajperką - spróbuje zestrzelić awiana, by wyglądało na wypadek. To powinno ich zniechęcić a przynajmniej kupić jej dość czasu.

* X: niestety, nie wygląda na wypadek
* V: awian został zestrzelony

Amanda spiernicza dalej. Amanda wie, że otwartą przestrzenią ją na pewno znajdą; szuka czegoś, co da jej jakąś osłonę.

* Vz: Amanda znalazła dobry teren; jest coś na kształt wielkich drapieżnych krystalicznych jastrzębi? Dużo ich. Ale da się pełznąć _pod_ nimi - jest duża szansa, że awiany nie odważą się tam zbliżyć.
    * Więc Amanda poszła tamtą drogą

Amanda ZGUBIŁA awiany, a przynajmniej tymczasowo. Na oko ich jest kilkanaście. Minus jeden. "Straciłam Lucię, oni stracili jednego łowcę." Wiadomość na ultrakrótkich falach, od Ayny.

* Ay: "Tu Ayna, ktoś poza mną i Xaverą jest w pobliżu?"
* Am: "Amanda. Lucię dorwali."
* Ay: "Mamy firebase, w jaskini, niedaleko krystalicznych ptaków. Masz koordynaty (przesłała)"
* Ay: "Mamy sprawne servary, trochę sprzętu, sensowny recon, medical i baterie. Co masz?"
* Am: "Lekko uszkodzony servar, snajperka z amunicją, standardowe wyposażenie z poda, mogę wymagać medical."
* Ay: "Przeprowadzam Cię mapą."

Amanda bez większego kłopotu się tam dostaje. Faktycznie, dwie Furie są gotowe, w opłakanym stanie, ale się trzymają. Amanda ma servar w lepszym stanie niż Ayna i Xavera.

* Xv: "To nie są żołnierze, co na nas poluje. Nie wiem co to, ale to nie są zwykli żołnierze z okolicy. To nie dekadianie."
* Am: "Może ci z miasta?"
* Xv: "Gdzie ten cholerny Kajrat?"
* Am: "Mówił, że przyjdzie, musimy mu to ogłosić - czyli wrogom"
* Ay: "Żywność będzie problemem"
* Xv: "No to trochę schudnę"
* Am: "Jak będziemy racjonować, przeżyjemy dłużej (pokazała racje i wodę). Jesteśmy na planecie, da się coś złapać. Przeczekamy pierwsze polowanie."
* Xv: "Nie jesteśmy w najlepszym miejscu..."
* Am: "Chwilowo osłania nas od awianów, Was też ścigały takie... psy?"
* Xv: "Aynę tak, zestrzeliłam je. A potem awiana."
* Ay: "Niedaleko jest placówka noktiańska, na oko kilkadziesiąt żołnierzy?"
* Am: "Skąd tu?"
* Ay: "Rozbity statek. Fortyfikacja. Możemy tam się przezbroić lub znaleźć inne Furie."
* Xv: "Było nas 20. Nie możemy być jedyną trójką."
* Am: "Po ile awianów puścili na Wasze pody? Na mój - trzy"
* Xv: "Na mój jeden, ale głupio spadłam. Ayna miała trzy."
* Am: "Lucia miała 10, ale wiedzieli że ona tam jest. Ilość zasobów na nas rzuconych jest absurdalna"
* Xv: "Ale są kiepscy, naprawdę kiepscy!"
* Ay: "To cywile? Tak wygląda."
* Am: "Nie pokonamy tak dużej grupy"
* Ayna: "opracuję plan dostania się do Fortyfikacji, trzy warty, zostajemy do wieczora i idziemy wieczorem"
* Amanda: "dobry pomysł. Choć nie - a co jeśli przyjdą kompetentni agenci a nie cywile?"
* Xavera: "tu lepiej przetrwamy niż pójdziemy od razu"
* Ayna: "nie, Amanda ma rację, nie możemy ryzykować"
* Amanda: "jak nas namierzą, pozamiatane, wyślą militarną grupę i..."
* Xavera: "możemy sprawdzić tunel tutaj. Wygląda na robiony przez ludzi."
* Ayna: "wygląda na niesamowicie STARY. Nie wiem czy chce znaleźć co tam siedzi."
* Amanda: "patrząc po tych szerszeniach... nie chcemy. Kajrat mówił, że teren jest piekielnie niebezpieczny. Lepiej nie grzebać."

Krótki moment na regenerację, konsolidację zasobów i sprzętu, bardziej wyrównany rozdział zasobów i czas dotrzeć do rozbitego statku który stał się fortecą noktian. Może tam jest Kajrat. Zwłaszcza, że skoro to statek co stał się fortecą, lokalsi go nie dorwali BO GDYBY DORWALI, to by nie było tam 'noktiańskich noktian'. Amanda się zastanawia, czy można coś zrobić z psami i psowatymi. Ayna może mieć pomysł. Ayna, osoba o najdziwniejszych pomysłach, dokładnie badała faunę i florę Astorii. "Bo nie wiadomo co się stanie". Ta cecha, która była dość irytująca okazała się bardzo przydatna.

Tr Z +3:

* X (p): niewielki efekt uboczny użycia odpowiednich żuków; śmierdzi jak cholera i przyciąga niektóre potwory
* V (p): Ayna wie o pewnym typie żuków które powinny tu gdzieś być.

Amanda i Xavera idą zbierać żuki w tej jaskini.

* Vz (m): mają dość, by wprowadzić to w życie.

Dziewczyny wysmarowały się żukami. A dokładniej - servary. Powinny być odporne na psy itp. To daje im dość duże możliwości działania i poruszania się. Czas spierniczać i dostać się do noktiańskiej fortecy.

Tr Z +4:

* Vr (p): nie ma żadnych encounterów ze strony awianów itp. Furie ich zgubiły, przynajmniej tymczasowo.
* Vr (m): "o dziwo", awiany bardzo, bardzo omijają obszar fortecy noktiańskiej. Furie dobrze omijają potencjalne zagrożenia (Ayna i jej wiedza pomogły) i docierają z zaskoczenia do fortecy noktiańskiej.
* X (p): forteca noktiańska wygląda "prawidłowo". Są strażnicy, są działka, forteca jest i stoi. 
    * Ale jest cicho radioelektronicznie. I to jest dziwne. Wiadomości typu "chodźcie do nas", "tu jest bezpiecznie", ale generyczne. Noktiańskie generyczne.
    * Amanda na ultrakrótkim: "nie podoba mi się to"
* V: MIMO, że Furie mają wszystkiego dość i rozpaczliwie szukają sojuszników, analizują fortecę
    * strażnicy nie są na high alert. Po prostu są. Robią swoje, ale nie zachowują się jak w niebezpiecznym terenie
    * strażnicy mają uśmiechnięte, spokojne podejście
    * mundury, broń itp - one są zaniedbane. Tak jakby nikt o to nie dbał.
    * dane wskazują, że to SĄ noktianie. Furie słyszały o tym statku.
    * ale coś jest nie tak

.

* Xavera: "spróbuję się zakraść"
* Ayna: "nie wiem"
* Amanda: "to zły pomysł moim zdaniem. Coś jest bardzo nie tak, oni..."
* Xavera: "nie zachowują się jak noktianie powinni? I nikt ich nie atakuje? A powinien?"
* Amanda: "chociażby?"
* Xavera: "nie jestem głupia, widzę, że coś jest nie tak. Ale oni mają zasoby a my nie. Dotrwamy do tej Pacyfiki?"
* Ayna: "z moich obliczeń, tak."
* Amanda: "wolałabym podjąć to ryzyko, mówiąc szczerze. Jeśli nie mamy dość zasobów, spróbowałabym zdobyć je nocą"
* Ayna: "nie wiem co się stało tym noktianom i nie wiem czy chcę jeść ich zasoby"
* Amanda: "point. Idziemy stąd."
* Xavera: "spojrzenie do środka nic nam nie zrobi. Chyba."
* Amanda: "właśnie - chyba. Ayna, czy psopodobne cosie... rozpoznajesz to jakoś?"
* Ayna: "nie, nie wiem co to było."
* Amanda: "widziałam (pseudoszerszenie). Mówi Ci to coś?"
* Ayna: "nie, przepraszam. Nie doczytałam."
* Amanda: "albo nie było tego w naszych źródłach. Za dużo nie wiemy."
* Xavera: "dobra, rozumiem. Przekonałaś mnie. CZEMU nie są zestrzeleni."
* Amanda: "nie wiem i nie mamy zasobów by się dowiedzieć. Znajdźmy Kajrata, skonsolidujmy więcej sił... wtedy możemy kombinować. Nawet jak zestrzelimy awiany, skończy nam się amunicja."
* Xavera: "jedyny problem - nie wiem czy tam nie ma jakiejś Furii."
* Amanda: "jeśli tam jest i to bezpieczne miejsce, nic jej nie grozi."
* Xavera: "a jeśli to groźne miejsce? Możemy ją wyciągnąć. Jeśli tam jest."
* Amanda: "jeśli jest i jeśli poziom zagrożenia jest za duży dla jednej Furii i za niski dla kilku. Z czarnej dziury nikogo nie wyciągniesz, nie wiesz z czym się mierzymy tutaj. A nie ma nas dość by wysłać na zwiady jeśli nie wróci."
* Xavera: "masz rację. Po prostu nie chciałam stracić nikogo poza Lucią."
* Amanda: "w tej chwili niestety naszym priorytetem jest próba przetrwania. Jeśli MOŻEMY kogoś uratować, zrobimy to. Ale poziom ryzyka niewiadomy?"
* Xavera: "chodźmy stąd"
* Ayna: "coś gorszego niż śmierć, na Astorii..."
* Amanda: "potencjalnie"

Obserwacja HUD, wieczorno-nocny spacer do Pacyfiki. Szukamy miejsca, które będzie jakoś bezpieczne. Jakieś 50-60 km do przejścia i to w servarach. Ale nie można się poruszać całkowicie swobodnie, więc to potencjalnie nawet parę dni. Co możemy zrobić? Trzeba iść. Trzy Furie idąc w miarę bezpiecznie, uzupełniając wodę i zapasy żywności.

Amanda skupia się na puryfikacji i znalezieniu wody. Ayna robi co może, by przesterować servary (które są krótkoterminowe mimo wszystko) - niech servary wyciągną ile się da z silnika i reaktora. Niech idą najdalej jak mogą. Bo to, fundamentalnie, stanowi główny problem w tej chwili. Dystans.

Tr +3:

* V (p): Ayna wyciągnie JESZCZE TROCHĘ. Nie dojdą do Pacyfiki, ale dojdą dalej niż powinny. Acz pewnym kosztem.
* X (p): Ayna niestety wygeneruje ogromny dyskomfort. Przegrzewanie, niższa precyzja, wyłączenie nie-krytycznych części TAI...
    * Ayna bardzo przeprasza, ale tego nie wie jak ruszyć.
* X (m): Servary nie będą precyzyjne. Nie będą dobrze strzelać np. snajperką. Będą się "trząść".

Amanda szuka dobrego miejsca gdzie je zaparkować, gdzieś, gdzie mogą zostać na potem. Bo to umożliwi odzyskanie sprzętu potem. Ayna i Amanda znajdą takie miejsce na mapie. Xavera tylko westchnęła cicho. Furie doprowadzą servary na krytycznych zasobach na minimalnym poziomie AI - do self-destruct.

Zapas wody - udało im się znaleźć jeziorko, gdzie da się wodę pozyskać. Trzeba to tylko zrobić.

## Streszczenie

Noktiańska jednostka przewożąca Furie Mataris została zestrzelona i część Furii spadło w okolice Pustogoru. Astorianie polują na Furie, Kajrat je ostrzega i formuje swoje Nocne Niebo. Amanda odzyskała kontakt z dwoma innymi Furiami i przechodzą przez tereny na południu Pustogoru, próbując dostać się do Pacyfiki. Tam czeka na nie Kajrat. A po drodze - rozbity dziwny noktiański statek, polujący na Furie itp.

## Progresja

* Lucia Veidril: złapana przez salvagerów i sprzedana Wolnemu Uśmiechowi

## Zasługi

* Amanda Kajrat: liniowa Furia Mataris która unika wykrycia przez astorian po rozbiciu się. Schowała się przed awianami, autodestructowała kapsułę, zostawiła Lucię jako dywersję nie mogąc jej pomóc i ostrożnie prze w kierunku na Pacyfikę - tylko tam jest jakakolwiek nadzieja w aktualnej beznadziejnej sytuacji.
* Xavera Sirtas: fierce and loyal; najlepsza wojowniczka Furii Mataris w okolicy. Ma dość agresywne plany - chce się zakraść, ustrzelić itp. Ale daje się przekonać, że to nie jest najlepszy pomysł. Opiekuje się Ayną.
* Ayna Marialin: bardzo ostrożna Furia Mataris; moduje servary najlepiej jak potrafi by jak najdłużej przetrwały. Zapamiętała rozkład terenu i dekoduje dane od Kajrata. Ostrożna, staje po stronie Amandy.
* Lucia Veidril: złapana w głodne rośliny w Lesie Pustym, była zmuszona do ostrzeliwania się by się ochronić. To sprawiło, że została zaatakowana i pojmana przez przeciwników. Służy jako dywersja dla innych Furii.
* Ernest Kajrat: próbuje uratować jak najwięcej Furii Mataris swoim pomniejszym oddziałem szturmowym który uruchomił protokół Nocnego Nieba. Ostrzegł Furie przed potencjalnymi ludzkimi drapieżnikami i działa jak może.

## Frakcje

* Nocne Niebo: ostrzega Furie o tym, że wszyscy na terenie na nich polują i próbuje osłonić i odzyskać jak najwięcej Furii; niestety, nie ma jeszcze dużych sił. Dopiero teraz się kształtuje.
* Wolny Uśmiech: korzysta z wojny by przejąć jak najwięcej Furii i noktian dla siebie. Adaptacja, dostosowanie lub sprzedaż.
* Wolni Miasteczkowcy Pustogoru: część z nich idzie za ogłoszeniem Wolnego Uśmiechu by polować i pozyskać Furie (noktianki) i innych noktian.
* Czerwone Myszy Pustogorskie: część z nich idzie za ogłoszeniem Wolnego Uśmiechu by polować i pozyskać Furie (noktianki) i innych noktian.
* Terminuscy Strażnicy Szczeliny: w tle próbują powstrzymać działania Wolnego Uśmiechu celem pozyskania noktian z różnych jednostek na sprzedaż, adaptację lub dostosowanie do siebie

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, SW
                    1. Granica Anomalii
                        1. Las Pusty, okolice: wygłodzony las zapolował i zneutralizował jedną Furię; miejsce polowania na Furie
                        1. Ruiny Kaliritosa: kiedyś potężna jednostka noktiańska i forteca. Teraz - miejsce, gdzie są dziwni noktianie.

## Czas

* Chronologia: Zmiażdżenie Inwazji Noctis
* Opóźnienie: -384
* Dni: 2

## Specjalne

* .
