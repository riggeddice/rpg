## Metadane

* title: "Romeo, dyskretny instalator Supreme Missionforce"
* threads: hiperpsychotronika-samszarow, samotna-maja-w-strasznym-aurum, karolinus-heart-viorika, turniej-supmis-41
* motives: gra-supreme-missionforce, dobry-uczynek-sie-msci
* gm: żółw
* players: kamilinux, anadia, kić

## Kontynuacja
### Kampanijna

* [230613 - Zaginięcie psychotronika Cede](230613-zaginiecie-psychotronika-cede)

### Chronologiczna

* [230613 - Zaginięcie psychotronika Cede](230613-zaginiecie-psychotronika-cede)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ?
* CEL: 
    * Maja jest samotna i nie ma z kim rozmawiać
    * Romeo. I decyzja Karolinusa i Eleny - co z nim? XD.
    * Działania
        * Elena: duchy, historia bazy, 
        * Karolinus: Maja, sekrety mroczne, 
        * Strzała: diversion

### Co się stało i co wiemy

* Kontekst
    * .
* Wydarzenia
    * Maja potrzebuje sprzęt do Supreme Missionforce. Romeo jej zainstaluje.
    * Romeo - technomanta wysokiej klasy - przyjechał 

### Co się stanie (what will happen)

* S1: SOS Karolinus! Zabiłam maga!!! (Maja) I tam jest ten straszny duch! On go nie pokona!!! I jest zasealowana!!!
* S2: "Bronić studni!!!"
    * Romeo: "to nieważne kim jestem", Duch "nie dojdziesz do centrum"
    * poziom 1: działka, blokady drzwi (Romeo je WYSADZIŁ)
    * poziom 2: roboty opętane przez duchy, efemerydy. I wszechobecne techno + wyładowania elektryczne
    * poziom 3: mackowa efemeryda strażnicza, gaz

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Maja Samszar
    * ENCAO:  ++-0- |Moralizatorska| VALS: Stimulation, Conformity >> Power| DRIVE: Rite of passage
    * styl: UR z elementami WB; lekko emocjonalnie niestabilna arystokratka która UWIELBIA wszystkich pouczać i dużo mówi. DUŻO.
* Romeo Verlen
    * ENCAO:  +00-+ |Konfliktowy;; drama queen;;Z poczuciem humoru;; płomienny | VALS: Stimulation, Tradition >> Universalism | DRIVE: folk hero ratujący Maję (BWR)
    * styl: (BWR), wesoły i niezwykle agresywny, combat engineer z ogromną ilością sprzętu bojowego

### Scena Zero - impl

.

### Sesja Właściwa - impl

Strzała przechodzi w tryb aktywny. Większość systemów już działa. Centrala ma dla niej nowy rozkaz. Dyrektywę. "Z przyczyn POLITYCZNYCH rodu Samszar obowiązkiem Strzały jest wychowanie Karolinusa i Eleny. Doprowadzenie do tego, by dobrze współpracowali jako zespół. Strzała jest autoryzowana do dodawania utrudniających parametrów operacji. Mają być exemplarami rodu."

Karolinus dostaje na hipernecie sygnał SOS od Mai. Rozpaczliwie się łączy. O DRUGIEJ RANO!

* Maja: "Nie chciałam go zabić! Pomóż mi, proszę!!! Nikt inny nie przychodzi mi do głowy!"
* K: "Ale kogo nie chciałaś zabić"
* M: "(Z lekką histerią) Był wybuch, i nie mam z nim kontaktu! Kolega! Tata mi nie pozwalał mi mieć kolegów, nie takich, nie z plebsu!"
* K: "Gdzie jesteś?"
* M: "Karmazynowy Świt."
* M: "Tylko Ty możesz mi pomóc, inni będą próbowali raczej uratować moją reputację."
* K (fwd Elena). E: dobra, możemy jechać, jest duch.
* K -> S: info

Dla Strzały to wszystko nie ma sensu. Podpina się do źródeł info lokalnych - nic nie zarejestrowano. Zespół jedzie tam z dużą prędkością.

Sygnał Mai prowadzi do dużej firmy. To jest firma typu duże biuro. Szlaban, jeden strażnik emeryt.

Strzała - pierwsze co się rzuciło w oczy taka DUPNA CIĘŻARÓWKA. Stoi na parkingu. Rejestracja jest 100% fikcyjna. Ona nawet nie udaje że nie jest fikcyjna. Strzała parkuje koło ciężarówki. I tracker przyczepiony do ciężarówki.

* Strażnik: "Tien Samszar?" /w eter, z nadzieją
* (Karolinus się przedstawia) "To my, służbowo, proszę otworzyć, chcemy wjechać i powiedzcie co to za ciężarówka"
* S: "Och tien Samszar, jak dobrze... przyjechała młoda tien Samszar i powiedziała że to specjalna operacja i w sekrecie"
* E: "Tą ciężarówką?"

Strzała - skanuje ciężarówkę. Pierwsza niespodzianka - ciężarówka ma ekrany ochronne i ciężki pancerz. To ciężka maszyna. Elena decyduje się na to, by użyć sentisieci by cofnąć się czasem do momentu jak ciężarówka była otwarta by dowiedzieć się czegoś o tej ciężarówce - cargo, osoby...

(-seal Nataniela +sentisieć)

Tr+2+3Ob:

* V: Elena widzi:
    * gdy ciężarówka się otworzyła, na pewno w środku są jeszcze 2 osoby. Nie wyszły. Koleś w pancerzu dał im znać gestem że mają zostać
    * Maja wie, że ci ludzie tam są.
    * coś z sentisiecią tu jest nie tak. Zachowuje się trochę inaczej. Masz takie poczucie.
* X: Albert dowiaduje się, że tu są inni magowie rodu Samszar po sentisieci.
* Ob: Nataniel TEŻ się dowiaduje że tu się coś dzieje.
    * Jakiś Samszar coś zrobił z sentisiecią na tym terenie. Dawno robione. Tu _coś jest nie tak_ i ktoś o tym wie.
* Ob: Elena + Strzała wykryły bardzo zaawansowany sprzęt w ciężarówce. Jakiś sprzęt mechaniczny ciężki. Żrący dużo prądu. Ale nieaktywny. Niemagiczny.
    * TAKIE WIELKIE KOMORY DO KLONOWANIA LUDZI! Dwie.
    * NAWET ludzie w ciężarówce wiedzą że tu jesteście XD.
    * Ciężarówka się nie rusza. Za to z perspektywy Strzały ona się włącza. Sygnatura energetyczna, acz maskowana, rośnie.

Karolinus patrzy w tym czasie na kamery. Ciężarówka przyjechała koło 1:00 rano. Wysiadł z ciężarówki koleś w pancerzu (nie servarze), po czym podszedł do drugich drzwi, otworzył i pomógł Mai zejść. Maja ofc w sukni. On facepalmował. Potem wszedł do ciężarówki, Maja pogadała ze strażnikiem, on wziął plecak. Koleś nie szanuje arystokracji - to widać. Nie pomógł Mai wejść na schody aż zaczęła nalegać. I zniknęli w budynku. Widzi na kamerach jak koleś wyciągnął jakieś urządzenie i bada ściany, Maja na niego patrzy. (Strzała zidentyfikowała to jako detektor kabli). W końcu z Mają znalazł wejście do piwnicy. I tam zeszli.

I tam jest on, Maja i Maja coś do niego mówi, on zaczął mierzyć pomieszczenie, a potem zmiażdżył kamery w piwnicy. (ale detektor Mai pokazuje, że Maja dalej jest w tej piwnicy). Karolinus - wyczuwasz od Mai emanacje magiczne. Jakby próbowała czarować albo używać sentisieci. Ale nic się nie dzieje.

Karolinus i Elena spokojnie idą do piwnicy, strażnik wyłącza wszystkie zabezpieczenia po drodze. A zabezpieczeń nie ma wiele. 

Maja "ROZERWIJ SIĘ! PRZESTAŃ ISTNIEĆ!" bo na pewno w końcu się rozpadnie.

(+dwójka magów +Maja jest dość słabym magiem -Maja to kłębek emocji +Karolinus primary)

Tr M 2P +2:

* Ob: zaklęcie Mai całkowicie wymyka się spod kontroli czyjejkolwiek i staną się Dziwne Rzeczy
* Vm: Moc Karolinusa dominuje moc Mai. Masz inicjatywę. (podbicie)
* Vm: Moc Karolinusa wygrywa

Moc Mai zaczyna się rozpełzać, pojawiają się reakcje sentisieci, jakieś ektoplazmy, macki duchowe. Emanacja energii magicznej tylko Mai.

Ale widać coś innego - dziura w ziemi otoczona polem siłowym. Czymś co wygląda jak pole siłowe. Pomarańczowe, lekko połyskujące. Nie ma żadnej rysy. Pod spodem są ślady eksplozji (pod polem).

* MAJA SIĘ MOCNO WTULA w Karolinusa "PRZYSZEDŁEŚ! URATUJESZ MNIE I KOLEGĘ!" i ostro płacze. I Maja "on nawet nie jest moim przyjacielem!!!" (płacząc!)
* Elena "Maja!" (odwraca jej głowę) "Kto to jest i jak się znaleźliście!" "I czemu uważasz że przybyłaś!" "Mówiłaś o duchu"
* M: "DUCH MA MACKI!" "nie powinno go tu być".

(+Elena ma głos nauczycielki, +Maja ufa Karolinusowi)

Tp +2:

* Vr: Maja zaczyna mówić
    * przyjechał, pomóc, to znaczy - nie mieliśmy rewanżu, a ja nie mogę grać w Supreme Missionforce bo mi nie pozwalają! A Tata nie pozwala!
    * ale... ale udało się zdobyć sprzęt i trzeba zamontować i tu się da dobrze ukryć, sprawdziłam sentisiecią!
* V: To Romeo Verlen, on pomógł Mai w instalacji. On jej nawet nie lubi a ona jego.

Karolinus klepie Maję po plecach "wracaj do Strzały, my się tym zajmiemy". Maja odmawia. Nie ma mowy. Ona nie zostawi Romeo samego. Bo jeszcze zastrzeli Karolinusa jeśli nie wie że jest "swój". Bo to psychopatyczny Verlen bo ma broń.

* V: Karolinus ZMUSIŁ Maję do pójścia do Strzały w formie "jeśli tu zostaniesz to nie pomogę." Maja pyta "a jak pójdę to pomożesz, uratujesz Romeo?" K: "Tak, uratuję go".

.

* RUCH: wiadomość na sentisieci do Eleny: "Albert Samszar. Co zrobiłaś z moją córką. Masz 3 sekundy."
* Elena: "..."

PLAN:

* Wysokopoziomowo - Strzała zabiera Maję do Ogrodów Medytacyjnych super szybką prędkością.
* Karolinus i Elena jechali na sprawdzenie Strzały po rekonwalescencji, tu info o anomaliach. Przejeżdżali obok.
* Elena coś wykryła z duchami.
* Super że Albert tu jest, bo mogą z ekstra doświadczonym magiem zobaczyć co się tu dzieje. Niech nam pomoże.

Strzała i Maja lecą. Strzała skanuje _lore_ z missionforce. Przykłady zbliżone do obecnej sytuacji. Strzała nie przywiązuje się do głosu, ma TAI któregoś z postaci Missionforce "można jej zaufać". Strzała ciągnie z tego co Maja zna i rozumie - jeśli Maja chce uniknąć kłopotów i by Verlen uniknął kłopotów, musi zamaskować ruchy sentisiecią. "Dasz radę, nie po to wygrałaś te wszystkie rzeczy". "Uwierz w siebie".

(-histeria Mai, +missionforce, +Karolinus, -ojciec)

Tr +3:

* X (inicjatywa Mai)
* V (inicjatywa Strzały) -> Strzała jest "duchem opiekuńczym Karolinusa"
* X (inicjatywa Mai) -> Maja przekazała Strzale ze swojej perspektywy co tu się dzieje: Verlen próbuje jej pomóc, ojciec JEJ nie pozwala, ojciec chce zachować twarz, ona nie wie co się dzieje i niech ktoś pomoże
    * "właśnie próbujemy to zrobić, ale musisz pomóc nam pomóc sobie, bez Twojej pomocy nie pomożemy jemu ani Tobie" (Maja oddaje inicjatywę +1Vg)
* V: Maja będzie skutecznie maskować w sentisieci
* (+3Vy -1Vg) Maja maskuje a Strzała ją chce zdeployować niezauważenie
    * Vr: podniesienie następnego
    * V: Strzała umieściła Maję przy medytacji. I ma tu siedzieć. I ma udawać że nic nie wie.
* V: Maja BĘDZIE udawać że nic nie wie
    * jeżeli cokolwiek się stanie Romeo, Maja NIGDY nie wybaczy Karolinusowi i Elenie

Czas na Elenę. Gada z Albertem.

Elena wpierw MUSI grać na czas by Strzała umieściła Maję na miejscu a POTEM Elena musi skłonić Alberta by "zobaczył" że Maja tam (przy Ogrodach Medytacji) jest po sentisieci. A potem pochlebić mu by chciał pomóc z tą podziemną bazą.
A w tym czasie Karolinus będzie próbował wyciągnąć z podziemi Romeo.

(-Albert wie że Maja to ziółko, +to nie jest normalne miejsce dla Mai, +Albert CHCE chronić twarz Mai)

Tr Z +2 (inicjatywy -> Albert, leci tu, wściekły):

* E: "Maja była ostatnio z Karolinusem, BARDZO się przyjaźnią! Karolinus i Maja rozmawiali o Viorice, mam nadzieję że Viorika nie będzie zazdrosna."
    * Vz: Elena ma inicjatywę. Albert jest... wtf.
* A: "Tien Samszar, czy coś piłaś? Gdzie jest moja córka?"
    * "Nie piłam, w tej książce tien Samszar..."
    * "GDZIE jest moja córka..."
    * "negatywny wpływ alkoholu więc nie piję alkoholu..." (+1Vr)
    * V: podbicie na kolejny sukces.
    * Xz: Albert uważa Elenę za podfruwajkę. To jest zapijaczone, gadatliwe, irytujące cholerstwo.
    * X: Albert uważa Karolinusa (bo się zadaje z Eleną) i Elenę (bo z nim tak gada) za NIEPOWAŻNYCH
    * Albert "TIEN SAMSZAR. Czy wiesz coś o mojej córce, czy nie? Mówię poważnie."
    * "A ja poważnie tienowi odpowiadam, z Karolinusem"
    * "ostatnią niańką... wie pan co ostatnia niańka? Ostatnia niańka, zawał, z VERLENLANDU!" +2Vg
    * Vz: UDAŁO SIĘ, Maja jest wykryta przez Alberta w Ogrodach. A Strzała pobiła rekord prędkości.

Albert odleciał, czystym niesmakiem XD.

Karolinus tymczasem próbuje wyciągnąć Romeo. Albo nie - czeka na Elenę. I dopiero potem zejdzie w podziemia. Pole siłowe przepuszcza Elenę i Karolinusa. Jesteście w... czymś co było poczekalnią, ale granat nie pomógł. Na ścianie ślady ektoplazmy. Ta podziemna "baza" nie jest szczególne duża - słychać techno. Muzyka techno. Kiepska. "Ogary Zębatek". I losowe wyładowania elektryczne - ktoś autentycznie na tym piętrze puścił techno i włączył prąd w podłogę / ścianę.

Elena próbuje połączyć się i skomunikować z "duchem" infekującym robota. Wrogo nastawiony?

Tr +2:

* X: wyczułaś coś... dużego, coś upiornego, z głębi bazy, niżej. Coś podłego. Coś głodnego. To jest jakaś forma ducha, ale poza Waszą kontrolą. Poza Waszą sentisiecią - a tutejsza sieć jest skonfigurowana inaczej.
* V: skomunikowałaś się z duchem - on... coś mu zrobiono. Coś tu było robione z duchami.
* X: tak byłaś skupiona tak się łączyłaś że... ugrzęzłaś w tym sygnale, w tym duchu. Karolinus Cię odciągnął.
* X: w plecy ducha uderzył pancerz. Staranował robota. Verlen wbił się w tego robota i go przewrócił, po czym zerwał się i podbiega do Was "tego nie zabiję, nie da się, wiejmy stąd!". Jest ranny.

Karolinus: "WIEJEMY STĄD!" i za ramię Elenę. Elena się ocknęła.

Wyszli z Romeo. Na KANAPKĘ. Elena - Romeo - Karolinus. Przeszli przez barierę, bo bariera ich puściła bo nie wykryła nie-Samszara XD. Wyszliście bezpiecznie z tego.

* Romeo: "Ja mam transport" (niskim głosem)
* Elena: "Wiem, pozdrów Viorikę, następnym razem jak będziesz chciał pomagać młodym Samszarkom to nie wchodź na nie swoje ziemie to może się skończyć ostrym konfliktem politycznym"
* Romeo: "To może Ty pomożesz młodym Samszarkom bym ja nie musiał? Nawet jej nie lubię!"

Romeo i Elena się pożarli dość ostro, ale Karolinus był dobrym rozjemcą. Romeo odjechał w ciężarówce do Verlenlandu.

## Streszczenie

Strzała ma za zadanie wychować i nauczyć współpracy Karolinusa i Eleny. Karolinus jest wezwany do interwencji, gdy Maja przesyła sygnał SOS. Maja twierdzi, że niechcący mogła zabić kolegę. Ekipa dociera na miejsce, gdzie odkrywają ciężarówkę z zaawansowanym sprzętem. Maja jest w stanie panicznym, a jej magia wymyka się spod kontroli - próbuje przebić dziwne pole siłowe otaczające dziurę w piwnicy biurowca. Karolinus i Elena zmuszają Maję do opuszczenia miejsca, obiecując uratować Romeo. W międzyczasie, Strzała zabiera Maję do Ogrodów Medytacyjnych by nie miała problemów z ojcem. Elena zyskuje na czasie przed ojcem Mai (Albertem), dopóki Maja nie jest bezpiecznie umieszczona w Ogrodach. Potem wydobywają Romeo z podziemi zauważając, że ktoś tam ma tajną dziwną bazę do eksperymentów na duchach.

## Progresja

* Elena Samszar: zdaniem Alberta Samszara, gdy pije to nie da się z nią dogadać i jest niezwykle irytująca. Ogólnie - zwykle niegodna uwagi.
* Karolinus Samszar: zdaniem Alberta Samszara - zwykle niegodny uwagi. Zadaje się z Verlenami i marnuje czas.

## Zasługi

* Karolinus Samszar: Maja do niego wysłała SOS bo mu ufa; wciągnął Elenę i po analizie zapisu z kamer doszedł do tego, że Maja z Romeem coś robiła. Wyciągnął Elenę (zsynchronizowaną z dziwnym duchem) z piwnicy i robił za "dobrego wujka" Mai, uspokajając ją cały czas.
* Elena Samszar: przeszła przez sentisieć i wykryła obecność monterów w ciężarówce i zmiany w sentisieci (acz zaalarmowała wszystkie strony łącznie z Albertem). Bablała Albertowi kupując czas Strzale na ewakuację Mai, nawet kosztem swojej reputacji. Na końcu połączyła się z dziwnym duchem, co ją wyłączyło z akcji na moment.
* AJA Szybka Strzała: identyfikuje urządzenia Romeo jako detektory i ciężarówkę jako pojazd opancerzony, rozstawia drony monitorujące teren. Potem - uspokaja Maję i leci z pełną prędkością by ją umieścić w bezpiecznym miejscu zanim jej ojciec się wścieknie.
* Maja Samszar: uroczy diabełek (UR: creativity), nieszczęśliwa bo musi być "elegancką damą" a ma talent w Supreme Missionforce. Opieprzyła Romeo we wściekłości gdy on ją wyśmiał (bo nikogo nie miała) i Romeo przyjechał jej to zamontować. Znalazła miejsce, ale jak się okazało że jest tam ukryta baza i Romeo wpadł w dziurę, spanikowała. Wezwała Karolinusa (bo nikogo nie miała) i nie chciała zostawić Romeo (KTÓREGO NAWET NIE LUBI!). Lojalna. W końcu - nie wpadła w kłopoty, bo z rozkazu Karolinusa i Strzały schowała się w Ogrodach Medytacji XD
* Romeo Verlen: waleczny acz pełen pasji (BWR: individual, order, emotion). Nie lubi Mai Samszar, ale chciał podjechać jej zamontować Supreme Missionforce Chambers by ta mogła ćwiczyć i być lepsza. By "nie było wstydu jak przeciw niej walczy". Pożyczył ciężką ciężarówkę, monterów i podjechał do Karmazynowego Świtu (z 3 monterami i bardziej doświadczonymi ludźmi). A potem z Mają znalazł Dziwną Ukrytą Bazę i się uwięził XD. Odpalił techno i wyładowania by nie znalazły go potwory i schował się w kwaterach mieszkalnych. Jak Samszarowie wyszli, pomógł im z jednym potworem i w "kanapce" został ewakuowany przez Samszarów.
* Albert Samszar: ojciec Mai, dba o poprawność etykiety i niespecjalnie wie co robi Maja (UW: structure). Gdy Maja dała ślady obecności o 2 rano w okolicach Centrum Danych Symulacji Zarządzania, ruszył jej na pomoc komunikując się z wykrytą tam Eleną Samszar. Jego uwaga odwrócona, dzięki działaniom Eleny i Karolinusa Maja nie wpadła w kłopoty. Dba o Maję.
* Nataniel Samszar: władający amnestykami czarny Samszar (BRG: refusal to slow), dogadał się z Albertem odnośnie utrzymania lokalnej bazy w Karmazynowym Świcie w ukryciu za to, że da Mai coś co jej się spodoba i przyda. Odzyskał kontrolę nad Techbunkrem Arvitas.

## Frakcje

* Hiperpsychotronicy: mieli dobrze i sensownie zamaskowaną nieaktywną bazę. Na tyle, że Romeo Verlen chcący Elenie Verlen postawić Supreme Missionforce ją przypadkiem znalazł (bo szedł z tego samego paradygmatu). Musieli na gwałt chować bazę, by nie wyszło na jaw co tam się działo.

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Karmazynowy Świt: luksusowe miasteczko Samszarów; ekonomia oparta na usługach, ze szczególnym naciskiem na turystykę, wellness i rozwój osobisty
                            1. Karmazynowy Świt, okolice
                                1. Centrum Danych Symulacji Zarządzania: zużywa dużo mocy dla utrzymania danych i symulacji managementu. Więcej maszyn niż ludzi. Tradycja dla tradycji?
                                    1. Techbunkier Arvitas: trzypoziomowy techbunkier ze schodami, znajduje się pod Centrum Danych; chwilowo opanowane przez duchy?
                                        1. Kontrola bezpieczeństwa (1): jak wielki duchopotwór próbował złapać Romeo, ten rzucił granatem uszkadzając bazę oraz swój pancerz
                                        1. Kwatery mieszkalne (1): tam się schował Romeo Verlen przed duchami i dziwnymi robo-ektoplazmo-formami

## Czas

* Opóźnienie: 4
* Dni: 2

## OTHER
### Fakt Lokalizacji
#### Karmazynowy Świt



1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Karmazynowy Świt: luksusowe miasteczko Samszarów; ekonomia oparta na usługach, ze szczególnym naciskiem na turystykę, wellness i rozwój osobisty
                                1. Centralny Park Harmonii
                                    1. Eko-spa
                                    1. Sztuczne Plaże
                                    1. Ogrody medytacyjne
                                    1. Pałac Harmonii: luksusowe miejsce odpoczynku i epicki hotel
                                1. Rezydencje
                                    1. Szkoły i sale treningowe
                                    1. Biblioteka
                                1. Zewnętrzny pierścień wygaszający: zapewnia ciszę i spokój
                                1. Kraina Winorośli: winnice, winiarnie, restauracje
                            1. Karmazynowy Świt, okolice
                                1. Centrum Danych Symulacji Zarządzania: zużywa dużo mocy dla utrzymania danych i symulacji managementu. Więcej maszyn niż ludzi. Tradycja dla tradycji?
                                    1. Techbunkier Arvitas: trzypoziomowy techbunkier ze schodami, znajduje się pod Centrum Danych; chwilowo opanowane przez duchy
                                        1. Kontrola bezpieczeństwa (1)
                                        1. Kwatery mieszkalne (1)
                                        1. Sala konferencyjna (1) 
                                        1. Centrum Danych (2)
                                        1. Artefaktorium (2) 
                                        1. Komunikacja i monitorowanie (2)
                                        1. Headquarters (3)
                                        1. Laboratoria (3): infuzja i agregacja bytów astralnych
                                        1. Izolatoria (3)
