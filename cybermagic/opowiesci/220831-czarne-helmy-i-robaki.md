## Metadane

* title: "Czarne Hełmy i Robaki"
* threads: historia-eustachego, arkologia-nativis, zbrodnie-kidirona
* gm: żółw
* players: fox, kapsel, kić

## Kontynuacja
### Kampanijna

* [220723 - Polowanie na szczury w Nativis](220723-polowanie-na-szczury-w-nativis)

### Chronologiczna

* [220712 - Pasożytnicze osy w Nativis](220712-pasozytnicze-osy-w-nativis)

## Plan sesji
### Theme & Vision

* Nativis 

### Co się wydarzyło KIEDYŚ

* Nativis była dość ubogą arkologią; ale 24 lat temu zaczęła zarabiać jedzeniem i się rozbudowywać
    * W ten sposób ród Kidiron doszedł do władzy

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

* Problemy Darii. Janek dołącza do grupy Robaków.
    * "Czarne Hełmy" - elitarna grupa Kidironów - ich tępią.
* Bartłomiej Korkoran poważnie zastanawia się co 

### Co się stanie (what will happen)

* S0: atak na Robaki wraz z kuzynem
* S02: szukamy Janka
* S03: XXX podsłuchuje na Inferni
* S04: Wasze działania

### Sukces graczy (when you win)

* .

## Sesja właściwa
### Scena Zero - impl

.

### Sesja Właściwa - impl

* Arkologia Nativis BYŁA ubogą arkologią do momentu aż zaczęła stawać się eksporterem żywności ok. 20 lat temu. Żywność wcześniej była problematyczna; prawda jest taka z biegiem czasu dopracowała żywność i wszystko zaczęło działać. To sprawiło że ród Kidironów trzyma arkologię.
* Mieliśmy plagę Trianai w części starej arkologii.
* Eustachy ma kuzyna. Kuzyn ma na imię Tymon. 

PARĘ MIESIĘCY WCZEŚNIEJ.

Jest "kult śmierci". Grupa terrorystyczna. Wiecie, wszystko co najlepsze. Mówią na siebie "Robaki". 

Tymon zawsze chce pokazać że Korkoranowie pro-Infernia i pro-pokazania pokazania epickości. Robaki mają spotkanie w starej części arkologii - w niezbyt używanym terenie.

Ardilla robi konflikt na szczurzenie starych dziwnych artefaktów. Coś nietypowego. Coś, co po prostu nie trzyma się kupy. Czy na coś napotkała w przeszłości. Generalnie jak coś wchodzi na arkologię to Ardilla, ciekawska szczurzy po dostawach, magazynach itp. Najpewniej Janek ją wpuszcza.

Tr Z (Janek: manifest, wiedzę o terenie, co kiedy gdzie) +3:

* X: Janek dostał solidny opierdol "za niewinność" - za to że pozwala takiej Ardilli łazić, sprawdzać itp. On ma pracować a nie podrywać.
* X: Janek i Ardilla są osobami "z zewnątrz". Oni są przypisani do Inferni a nie do arkologii. To powodowało pewne problemy, niesforność.
    * Ludzie ZACHOWUJĄ SIĘ bardziej podejrzanie jak ktoś coś przemyca (+2Vg)
* V: Różnymi kanałami przemycane są różne rzeczy na arkologię. To nie jest dziwne. To co dziwne - NIC nie kojarzy się z kultem śmierci.
    * Raczej rzeczy związane z... badaniami biologicznymi?
* V: OKAZJA pomocy przemytnikowi
* V: Na bazie manifestów i tych wszystkich informacji - wszystko wskazuje że Kidironowie też coś przemycają. Czarne, nietypowe transporty. To sprawia, że wyjątkowo łatwo jest przetransportować czy przemycić niektóre rzeczy jak o tym wiesz.    

Czarne Hełmy Kidironów. Ich grupa szturmowa. Elitarni. Ardilla podczas szczurzenia zauważa jak młody chłopak ucieka, wieje. Przemytnik. Czarne Hełmy jak go znajdą to złapią i zamkną. A koleś ma loot. Ardilla PRZECHWYTUJE chłopaka jak Czarne Hełmy poza polem widzenia.

Tr Z (ATV) +3: 

* V: Koleś przechwycony. (+2Vg)
* Vz: Ardilla skutecznie schowała paczkę gdzieś "u góry". Gdzieś gdzie człowiek nie wlezie. 
* V: I WSTECZNIE DAJEMY KOLESIOWI INNĄ PACZKĘ, Z OPOWIADANIAMI EROTYCZNYMI SOFTCORE (+1Vg). Hełmy złapały. "Pałujemy".
* Vg: Hełmy nie mają pojęcia o obecności Ardilli. Psikus udany. 100%.

Paczka zawiera komponentny do urządzenia bioanalitycznego. To jest coś, co bada byty żywe.

Celina z przyjemnością zabrała się do badania sprzętu. Ona nie jest może ekspertem od wszystkich analizatorów bio ale wiele widziała.

Tr (niepełna) Z (narzędzia Inferni) +3 (sprzęty Inferni + dane z arkologii):

* V: To jest komponent narzędzi do zaawansowanych badań nad różnego rodzaju żywnością. Potrzebuje energii, bazy.
    * Ale ta arkologia MA TAKIE RZECZY. Acz to ludzie Lycoris mają do tego dostęp.

Eustachy sabotuje czujkę i zbiera dane. Kult działa w ukryciu. Eustachy zakłada farbę, wybucha w twarz - wysoce kwasowy / zasadowy / odbarwia skórę. Dziad trafi do medycznego i Eustachy wie że koleś jest z kultu i jest ekspertem do tego. I to podkopuje jego opinię eksperta. ON TO SPARTOLIŁ.

Ex (przeciwnik ma ekspertów) Z (Infernia ma czujki a Eustachy ma pomysły) +3 ->
Tr (przeciwnik ma ekspertów ale nie od sabotażu a od sprzętu) Z +4 +5Og:

* Vz: że nikt nie zauważy pułapki.
* V: w pełni udane, mamy dekonspirację eksperta.
* X: sprzęt został zniszczony przez eksplozję (a dowody zatarte), jest kilku rannych "kultystów"
* O: SUKCES. Wiem już jak zaczęła się plaga Trianai. Właśnie z tego. Ale udało się sprawić, że nikt nie powiązał tego z Ardillą ani z tamtym przemytnikiem.
    * Robaki wierzą, że to były działania nieetycznych Kidironów
    * Kidironowie wierzą, że to Robaki poszły za daleko

Celina próbuje się dowiedzieć kto trafił do szpitala w tym okresie. Kto trafił do szpitala z określonymi ranami (jakie wyspecyfikował Eustachy). Poszła robić za wolontariat.

Tp Z(wolontariat, zna się) +4:

* V: Znamy nazwiska.
    * Wojciech Czerpń, technik pracujący pod Lycoris mający dostęp do tego sprzętu na co dzień. On jest "ekspertem" Robaków.

Celina jest tą społeczną. Poszła na przeszpiegi - pogawędzić z jednym z nich, zrozumieć jakie ma intencje itp. Po prostu pogadać. Trochę posterować dyskusję na ewentualne "życie jest bez sensu" itp. Zrozumieć o co chodzi i czego chcą.

Tr +3:

* V: Celina trochę zna się na ludziach. Wie czym są kulty śmierci. To nie jest kult śmierci. Nie mają nihilistycznych, doomerskich zachowań. Po prostu nie.
* X: Pojawia się asocjacja Celina - ci ludzie. (oficjele widzą zainteresowanie Celiny, po prostu "interesuje się tymi ludźmi")
* V: Jest część wspólna - wszyscy martwią się żywnością w arkologii, że ta arkologia jest handlarzem śmiercią. Że jedzenie truje ludzi. TO CONSPIRACY THEORISTS!
    * Wszystkie sprzęty są sabotowane albo zakazany dostęp! Dane w komputerach są poukrywane! Lycoris truje ludzi by byli posłuszni Kidironom!

Ich profil nie pasuje jednak do kultu śmierci. Nikogo stamtąd. To... inna sprawa. Może dlatego Czarne Hełmy ich tępią? Ale reakcja Czarnych Hełmów jest nieproporcjonalna do ich groźności. Czarne Hełmy są raczej OK jako frakcja.

(I w ten sposób (krótki) widać jak agenci Inferni chronią arkologię jak arkologia o tym nie wie.)

WIECIE kto jest w Robakach. Wiecie, że kuzyn chce zrobić wjazd. Wiecie że z Wami. 2 tyg później. 

Wiemy, kto pójdzie na spotkanie Robaków. Jak się dowiedzieć gdzie to spotkanie będzie? Wiemy, że w ciągu 2-3 dni.

......
Podsumowując:

* PRZESZŁOŚĆ
    * CZAS: 2 miesiące przed sesją na CES Purdont (-70 dni czy coś)
        * kuzyn Tymon prosi o wsparcie w zniszczeniu Robaków, kultu śmierci / terrorystów -> Kidironowie docenią a Infernia zyska
    * CZAS: 3 miesiące przed sesją na CES Purdont (-100 dni czy coś)
        * Ardilla pozyskała przemycany przedmiot (coś do badań biologicznych?), Eustachy sabotował na przyszłość
    * CZAS: 2 miesiące przed sesją na CES Purdont (-65 dni czy coś)
        * Eustachy wysadził owy kiedyś sabotowany przedmiot do badań biologicznych; doszliście do kilku kultystów (z nazwiska)
        * Celina doszła gadając z nimi, że to nie kult śmierci - to paranoicy że ŻYWNOŚĆ JEST SKAŻONA I WSZYSCY UMRĄ
                * z przyczyn politycznych "omg kult śmierci"
                * bardzo dziwne że Kidironowie ich tak nie cierpią
        * Za tydzień Tymon będzie miał siły gotowe do uderzenia i za tydzień kult ma mieć spotkanie wg danych Tymona
    * CZAS: 2 miesiące przed sesją na CES Purdont (-60 dni czy coś)
        * CEL:
            * jak znajdziecie miejsce spotkania kultystów?
            * im skuteczniej ich zmiażdżycie i odstraszycie tym większe zadowolenie Kidironów i zyski dla Inferni
            * jak się Wam uda uderzyć w głowę Robaków to dużo zyskacie
            * kto wspiera Robaki finansowo? <-- niewielka szansa że się uda
......

Eustachy chce, żeby Czarne Hełmy weszły i wytępiły Robaki. Ale chwała dla Czarnych Hełmów? My ich nie lubimy. Więc:

* Kult Robaka nie wie, że MY wiemy.
* Czarne Hełmy nie wiedzą, że my wiemy.
* "Sabotaż" -> Hełmy mają robić czystki, przesłuchania. Mają się przygotować.
* Hełmom - "to przydupasy, rozwalicie raz dwa" + sabotaż z pułapką Eustachego, Eustachy uratuje Hełmy.

Ardilla będzie wrabiać przemytnika / Robaki w to, że jest tam problem. Ale Ardilla ma LEPSZY pomysł - sfabrykować pseudodowody by Hełmy się zainteresowały naprawdę Robakami. Czyli najpierw Eustachy pogada z Tymonem.

* Tymon: "Powstrzymam swoje okrutne żądze i nie będę pluł w twarz najsilniejszym z nich"

Eustachy przygotowuje inteligentną PROWOKACJĘ. Plan w planie. Chce, by doszło do sprzeczki a jako, że Tymon się rzuci na dużego dzika by mu wpierdzielić to będzie klepał matę. Test manipulacji i strategii. Strategia to siła Eustachego ale manipulacja już mniej. Tymon jest naiwny, nie jest dobry w takie tematy.

Tp (niepełny) Z (zaufanie + dążenie do chwały) +3:

* Vz: Tymon pójdzie do niebezpiecznego miejsca wierząc, że jest w bezpiecznym miejscu by obserwować jak oni się zbierają. Żeby szpiegować skutecznie i żeby mógł zdobyć chwałę dla Inferni osobiście.
* V: Tymon spowolni Kidironów; da się zrealizować plan tak jak chce Eustachy.
* V: Wszystkie negatywne efekty uboczne spadną na Tymona i Kidironów, nikt nie dowie się o współpracy Eustachego, Ardilli i Celiny

Ardilla pójdzie na spacer, znajdzie jednego z Robaków i podłoży mu dyskretnie pluskwę. Dzięki temu będziemy mieli obraz sytuacje - gdzie są potencjalne miejsca spotkań Robaków. Niezauważenie, bo wiemy kiedy spotkanie. Na pewno odbijają się kartą po robocie - pluskwa na kartę a DRUGA podłożona.

Tr Z (zmęczenie delikwenta) +4:

* X: Koleś uzna, że Ardilla go klepnęła w tyłek czy coś. Że go podrywa.
    * "Młoda laska się mną zainteresowała, mam jeszcze ten seksapil!"
* X: Zauważył że nie ma portfela w tylnej kieszeni. Mina mu się zmieniła.
* Vz: Koleś OPIERDOLIŁ Ardilę z góry na dół. "JAK CIĘ RODZICE WYCHOWALI!" Ardilla "rodzice już nie żyją" smutno.
    * dała portfel z pluskwą. Koleś się zastanowił chwilę, dał jej 200 kredytek i wizytówkę. Jak się zastanowi, niech zadzwoni.

Dwie pluskwy różnego typu, więc udało się zamapować lokalizacje. Mamy informacje gdzie w Starej Arkologii (lepiej rozebrać niż naprawiać). Uruchomili jeden ze starych generatorów. Czyli mamy lokalizację ostatniego spotkania. A patrząc na sygnatury energetyczne, tamten generator jest ciągle 'gotowy do uruchomienia'.

Tymon poszedł SZPIEGOWAĆ. Nie dotarł do celu. Jakiś Robak ściągnął w inny korytarz, gdy Tymon za nim podążył, cóż, było ich kilku. Tymon dostał parę kopniaków i jego duma i morale ucierpiały.

PLAN: Eustachy zsabotuje żywność tej arkologii. W imieniu Robaków. To spowoduje dużą złość ludzi przeciwko Robakom. Robaki nie są zbyt kompetentne jako sabotażyści czy wojskowi, więc uderzenie w transport żywności będzie wystarczające. Spowoduje to, że ludzie dostaną śniadanie z 1h opóźnieniem. Transport jest pojazdem szynowym, nieopancerzonym ale zamkniętym transportującym z zewnątrz tutaj. Nikt nie chroni transportu, bo po co.

Eustachy sabotuje szyny. I podkładane jest coś co wskazuje na Robaki. Np. ulotkę "ta żywność jest niebezpieczna nie jedzcie jej ta partia żywności jest szczególnie Skażona i musieliśmy działać"

Tr (są to miejsca dość publiczne) Z (nikt się nie spodziewa A ZWŁASZCZA EUSTACHEGO) +2:

* V: doszło do sabotażu. Sabotaż był wredny, faktycznie, udało się bezproblemowo uszkodzić szynę i cały transport żywności zniszczony.
* Xz: nikt się nie spodziewa i te elementy nie były w najlepszym stanie. Poważne uszkodzenia strukturalne.
* X: w wyniku sabotażu są RANNI
* X: Robaki wiedzą że ktoś ich wrabia i szukają trzeciej strony. Kidironowie widzą, że Coś Jest Nie Tak. Ale jeśli efekty dostaną, to są skłonni zaakceptować. ŻÓŁW: +3Vg na zachętę.
* X: Dziadek Celiny i Janka + Wujek Bartłomiej się zainteresowali tematem. Ktoś tu robi brudną robotę. Ludzie są WŚCIEKLI na Robaki. Część Robaków decyduje się przycupnąć; nie o takie Robaki walczyli.
    * ...musimy zrzucić na Kidironów lub na Tymona...

Oki - ale jak ściągnąć Hełmy w tamtą stronę? Prosty pomysł - Ardilla ma wizytówkę do tego sympatycznego kolesia (Social Services - przemoc, pomoc innym, social worker). Na imię ma Stanisław. Ardilla mały detektyw, z lipną licencją detektywa, i natrafiła na Robaki, i tego jest za dużo, i chciała się dowiedzieć więcej o Robakach i okradła i głupio ale potem był ten wybuch.

Tr Z (sabotaż) +3 (koleś jest dość poczciwy, on to łyknie):

* X: czuje współodpowiedzialność. Nie tak miało być. Aktywnie będzie zwalczał Robacze ruchy (dobrym słowem).
* V: Stanisław nawet przez moment nie pomyśli, że Ardilla może go wkręcać.
* X: Stanisław będzie próbował Ardillę RATOWAĆ. Życie detektywa nie dla takiego maleństwa. "W arkologii znajdzie się bezpieczniejsza praca dla Ciebie".
* X: Stanisław jest rozpoznany jako "były Robak który zdradził". To znaczy, że ma przechlapane u wszystkich. Zostaje mu rodzina. (+4Vg na zachętę)
* V: Stanisław ma wsparcie Kidironów. Poszedł, przyznał się, oni zbudowali program tranzycji: od "Robaka" do "dobrego człowieka". Więc ogólnie nie poszło mu najgorzej.

Czarne Hełmy wpadły i uderzyły w Robaki. Robaki zostały zmiażdżone.

Dziadek i Wujek podejrzewają Tymona, zostało kilka śladów wskazujących na kompetentną osobę a Tymon po prostu pasuje do wzoru. Fakt, że chce się przypodobać Kidironom jedynie zwiększa. Kidironowie też to zauważyli i jako, że Hełmy zmiażdżyły Robaki to Tymon został "ozłocony" - przesunięty na zupełnie inną pozycję. A im wyżej idzie Tymon wśród Kidironów tym bardziej oddala się od wujka (czyli swojego ojca).

## Streszczenie

Tymon Korkoran chce pomóc Arkologii Nativis by zniszczyć kult śmierci Robaków. Zespół doszedł do tego że to nie kult śmierci; to grupa radykalnych ludzi uważających że coś jest nie tak z żywnością i robiących badania żywności by do tego dojść. Nevertheless, Eustachy zrobił straszny sabotaż (ranni itp) by ludzie znienawidzili Robaki, Ardilla jedną osobę wyciągnęła z Robaków by nic jej się nie stało i ogólnie Robaki zostały zmiażdżone. Tymon poszedł do góry w hierarchii Kidironów, acz Dziadek i Wujek są przeciw tak ostrym działaniom.

## Progresja

* Jan Lertys: opierdol i nieufność ludzi z Nativis, że pozwala Ardilli z Inferni się "wałęsać na lewo i prawo". On jest z Inferni, nie z Nativis.
* Bartłomiej Korkoran: coś jest nie tak, Kidironowie i Robaki nie mogli zrobić tego sabotażu; zajmie się tym i wejdzie w głąb. Podejrzewa Tymona Korkorana.
* Karol Lertys: coś jest nie tak, Kidironowie i Robaki nie mogli zrobić tego sabotażu; zajmie się tym i wejdzie w głąb. Podejrzewa Tymona Korkorana.
* Tymon Korkoran: przesunięty na zupełnie inną pozycję, wielka chwała u Kidironów. PLUS nieufność ojca (Bartłomieja Korkorana) i Karola Lertysa (dziadka Celiny i Janka)
* Stanisław Uczantor: wzgardzony i stracił pracę jako social worker, bo "eks-Robak i to zdrajca"

### Frakcji

* .

## Zasługi

* Eustachy Korkoran: sabotował kult Robaków bombą w analizatorze żywności i zaeskalował konflikt Kidironowie - Robaki, wszystką chwałą (i konsekwencjami) obarczył kuzyna Tymona. Zrobił ostry sabotaż szyn w wyniku którego sporo rannych i zniszczeń -> wszystko na Tymona a oficjalnie Robaki.
* Ardilla Korkoran: uratowała przemytnika i przekazała Eustachemu paczkę z analizatorem żywności; gdy chciała podłożyć pluskwę Stanisławowi to on ją opieprzył i będzie resocjalizował. Ale wyciągnęła go z Robaków i sprowadziła na dobrą drogę.
* Celina Lertys: doszła do tego, że Robaki robią analizy żywności i dlatego Kidironowie ich tępią..? Jako "ta społeczna" poznała kilka nazwisk i doszła, że to nie kult śmierci.
* Jan Lertys: pomagał Ardilii w scoutowaniu przemytników i przesunął się na pozycję "Infernia" a nie "Nativis" w postrzeganiu; 
* Wojciech Czerpń: technik pracujący pod Lycoris mający dostęp do analizatora żywności na co dzień. On jest "ekspertem" Robaków od badań żywności. Ważny w Robakach.
* Tymon Korkoran: kuzyn Eustachego; bardzo chce się przypodobać Kidironom; Eustachy wpakował go w pułapkę by Robaki go sklepały. Tak dąży do chwały i potęgi że wpadł w to jak śliwka w kompot. De facto został "tym przez kogo rozwiązano konflikt" w oczach Kidironów i Nativis.
* Stanisław Uczantor: social worker którego chciała okraść Ardilla i który ją będzie resocjalizował (XD). Były Robak; pod wpływem Ardilli opuścił Robaki i oddał się pod opiekę Kidironów

### Frakcji

* Kidironowie Nativis: też coś przemycają w arkologii Nativis, coś knują i nad czymś pracują.

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Nativis
                        1. Poziom 1 - Dolny
                            1. Północ - Stara Arkologia
                                1. Blokhaus E: tam znajdowała się baza Robaków i tam zostali zmiażdżeni przez siły Czarnych Hełmów

## Czas

* Opóźnienie: -84
* Dni: 12

## Konflikty

* 1 - Ardilla robi konflikt na szczurzenie starych dziwnych artefaktów. Coś nietypowego. Coś, co po prostu nie trzyma się kupy. Czy na coś napotkała w przeszłości.
    * Tr Z (Janek: manifest, wiedzę o terenie, co kiedy gdzie) +3
    * XXVVV: Janek ma opierdol za niewinność i są "z zewnątrz", ale mają co jest przemycane i Ardilli udało się pomóc przemytnikowi i info że Kidironowie też coś przemycają
* 2 - Ardilla podczas szczurzenia zauważa jak młody chłopak ucieka, wieje. Przemytnik. Czarne Hełmy jak go znajdą to złapią i zamkną. A koleś ma loot. Ardilla pomaga.
    * Tr Z (ATV) +3
    * VVzVVg: przemytnik przechwycony, Ardilla ma jego paczkę, Czarne Hełmy nie wiedzą o obecności Ardilli
* 3 - Celina z przyjemnością zabrała się do badania sprzętu. Ona nie jest może ekspertem od wszystkich analizatorów bio ale wiele widziała
    * Tr (niepełna) Z (narzędzia Inferni) +3 (sprzęty Inferni + dane z arkologii)
    * V: To jest komponent narzędzi do zaawansowanych badań nad różnego rodzaju żywnością. Potrzebuje energii, bazy. Niby Kidironowie to mają...
* 4 - Eustachy sabotuje czujkę i zbiera dane. Kult działa w ukryciu. Eustachy zakłada farbę, wybucha w twarz - wysoce kwasowy / zasadowy / odbarwia skórę.
    * Ex (przeciwnik ma ekspertów) Z (Infernia ma czujki a Eustachy ma pomysły) +3 -> Tr (przeciwnik ma ekspertów ale nie od sabotażu a od sprzętu) Z +4 +5Og
    * VzVXO: nikt nie zauważy pułapki, dekonspiracja eksperta Robaków, zniszczony sprzęt i ranne Robaki, SUKCES ale tak zaczęła się plaga Trianai. Wzmocnienie konfliktu Kidironowie - Robaki
* 5 - Celina próbuje się dowiedzieć kto trafił do szpitala w tym okresie. Kto trafił do szpitala z określonymi ranami (jakie wyspecyfikował Eustachy). Poszła robić za wolontariat.
    * Tp Z(wolontariat, zna się) +4
    * V: Nazwisko - Wojciech Czerpń
* 6 - Celina jest tą społeczną. Poszła na przeszpiegi - pogawędzić z jednym z Robaków, zrozumieć jakie ma intencje itp. Po prostu pogadać.
    * Tr +3
    * VXV: to nie kult śmierci, Robaki widzą zainteresowanie Celiny, wie, że martwią się stanem żywności w Arkologii.
* 7 - Eustachy przygotowuje inteligentną PROWOKACJĘ. Plan w planie. Chce, by doszło do sprzeczki a jako, że Tymon się rzuci na dużego dzika by mu wpierdzielić to będzie klepał matę.
    * Tp (niepełny) Z (zaufanie + dążenie do chwały) +3
    * VzVV: poszedł, obserwował, spowolnił Kidironów by myśleli że to jego plan i efekty uboczne spadną na Tymona i Kidironów.
* 8 - Ardilla pójdzie na spacer, znajdzie jednego z Robaków i podłoży mu dyskretnie pluskwę. Dzięki temu będziemy mieli obraz sytuacje - gdzie są potencjalne miejsca spotkań Robaków.
    * Tr Z (zmęczenie delikwenta) +4
    * XXVz: koleś uznał, że Ardilla go podrywa i kradnie mu kasę; opierdolił z góry na dół. Ardilla "rodzice nie żyją" smutno więc on jej pomoże
* 9 - Eustachy sabotuje szyny. I podkładane jest coś co wskazuje na Robaki. Np. ulotkę "ta żywność jest niebezpieczna nie jedzcie jej ta partia żywności jest szczególnie Skażona i musieliśmy działać"
    * Tr (są to miejsca dość publiczne) Z (nikt się nie spodziewa A ZWŁASZCZA EUSTACHEGO) +2
    * VXzXXX: udany sabotaż, poważne uszkodzenia, są ranni, Robaki i Kidironowie wiedzą że tu coś NIE TAK, dziadek + wujek zainteresowali się tematem -> wina na Tymona
* 10 - Ardilla mały detektyw, z lipną licencją detektywa, i natrafiła na Robaki, i tego jest za dużo, i chciała się dowiedzieć więcej o Robakach i okradła i głupio ale potem był ten wybuch. Wyciągnąć Stanisława z Robaków i puścić tam Hełmy
    * Tr Z (sabotaż) +3 (koleś jest dość poczciwy, on to łyknie)
    * XVXXV: Stanisław zwalcza robacze ruchy i wierzy Ardilli, ratuje Ardillę, ma opinię "Robak który zdradził" ale ma wsparcie Kidironów. 
* 11 - 
    * 
    * 


## Kto
### Arkologia Nativis
#### Kidironowie - centrala

* Lycoris Kidiron, 57 lat (ze wspomaganiami drakolickimi dającymi jej efektywność 3x-latki) (objęta przez Wiki)
    * Wartości
        * TAK: Bezpieczeństwo, Osiągnięcia ("dzięki mnie ta arkologia będzie bezpieczna i osiągniemy najwyższy poziom eksportu żywności")
        * NIE: Konformizm ("nikt, kto próbował się dopasować do innych nie doprowadził do postępu")
    * Ocean: ENCAO: +-0+0 (optymistyczna, nieustraszona, lubi się dogadywać z innymi, nie patrzy na ryzyko)
    * Silniki:
        * TAK: Kapitan Ahab: polowanie na białego wieloryba do końca świata i jeszcze dalej, nieważne co przy tym stracimy. (tu: "zrobię najlepszą arkologię, lepszą dla ludzi niż planeta macierzysta")
    * Rola:
        * ekspert od bioinżynierii i terraformacji, pionier (zdolna do przeżycia w trudnym terenie)
* Rafał Kidiron
    * Wartości
        * TAK: Achievement, Power("wraz z sukcesami przychodzi władza, wraz z władzą musisz osiągać więcej sukcesów")
    * Ocean
        * ENCAO: 00+-0 (precyzyjny i dobrze wszystko ma zaplanowane, lubi się kłócić i nie zgadzać z innymi)
    * Silniki:
        * TAK: Duma i monument: to NASZE sukcesy są najlepsze i to co MY osiągnęliśmy jest najlepsze. Musimy wspierać NASZE dzieła i propagować ich chwałę.
    * Rola:
        * kuzyn Lycoris, security lord, w wiecznym konflikcie z Gurdaczem (security & money - beauty)

#### Kidironowie / "władza" - support

* Tymon Korkoran (kuzyn Eustachego)
    * Ocean: ENCAO:  +--00
        * Jeżeli czegoś chce, weźmie to dla siebie; zazdrosny i zawistny
        * Niecierpliwy, chce TERAZ i nie będzie czekać
        * Egzaltowany, w przejaskrawiony sposób okazuje uczucia
    * Wartości:
        * TAK: Universalism, TAK: Stimulation, TAK: Tradition, NIE: Face, NIE: Humility
    * Silnik:
        * TAK: Być jak idol: iść w ślady swojego Idola i Mentora. Stać się tym czym idol się stał.
        * TAK: Wrócić do ukochanej: byłem głupi i ją straciłem. Zrobię wszystko, by móc do niej wrócić.
* Laurencjusz Kidiron (młody Kidiron, chce być ZNACZĄCY)
    * Ocean: ENCAO:  +0---
        * Nie planuje długoterminowo, żyje chwilą
        * Nudny. Nie ma nic ciekawego do powiedzenia i niezbyt się czymkolwiek interesuje.
    * Wartości:
        * TAK: Face, TAK: Power, TAK: Hedonism, NIE: Achievement
    * Silnik:
        * TAK: Przejąć władzę nad ogranizacją: ten ród / ta firma będą MOJE. Podbiję je, zmanipuluję lub doprowadzę do tego pokojowo.
* Karol Lertys (dziadek)
    * Ocean: ENCAO:  -00-+
        * Zrzędliwy i kłótliwy
        * Skryty; zachowujący swoją prywatność dla siebie
        * Chętny do eksperymentowania i testowania nowych rzeczy
    * Wartości:
        * TAK: Achievement, TAK: Family, TAK: Security, NIE: Stimulation, NIE: Self-direction
    * Silnik: (wszystko by tylko pomóc swoim wnukom; KIEDYŚ przemytnik potem inżynier arkologii i zna tajne skrytki)
        * TAK: Herold Baltazara: zwiększać wpływ i moc kogoś innego, powiększać ich wpływy.
        * NIE: Do the right thing: niezależnie od tego co jest wygodne czy tanie, własne zasady i własny kompas moralny są najważniejsze. By spojrzeć w lustro.

### Infernia

* Bartłomiej Korkoran, wuj i twarda łapka rządząca Infernią.
    * Ocean: ENCAO:  +-000
        * Bezkompromisowy, nieustępliwy, niemożliwy do zatrzymania
        * Ciekawski, wszystko chce wiedzieć i wszędzie wsadzić nos
    * Wartości:
        * TAK: Family, TAK: Self-direction, TAK: Achievement, NIE: Tradition, NIE: Humility
    * Silnik:
        * TAK: Inkwizytor: niszczenie fake news, publiczne pokazywanie hipokryzji, ujawnianie bolesnych prawd
        * TAK: Kolekcjoner: kolekcjonuje coś rzadkiego ()
    * Rola: wuj, kapitan Inferni
* Celina Lertys, podkochująca się w Eustachym 
    * Ocean: ENCAO:  --+-0
        * Stanowczy i silny, zdecydowany
        * Skryty; zachowujący swoją prywatność dla siebie
        * Nigdy nie wiadomo, co jest prawdą a co maską
    * Wartości:
        * TAK: Universalism, TAK: Tradition, TAK: Security, NIE: Power, NIE: Humility
    * Silnik:
        * TAK: Harmonia naturalna: balans między technologią i naturą, utrzymanie cudów natury. Lasy Amazonii.
        * NIE: Awans za wszelką cenę: albo lepsza firma albo wojna o stanowisko z innymi. To stanowisko będzie moje.
    * Rola: science / bio officer + medical. Młoda podkochująca się w Eustachym :D. Złota, płonąca skóra z błękitnymi włosami, może robić wyładowania
* Jan Lertys
    * Ocean: ENCAO: -0-0+
        * Zapominalski; non stop trzeba coś przypomnieć
        * Obserwator; raczej stoi z boku i patrzy niż działa bezpośrednio
        * Łatwo wywrzeć na tej osobie wrażenie
    * Wartości:
        * TAK: Humility, TAK: Universalism, TAK: Self-direction, NIE: Achievement, NIE: Security
    * Silnik:
        * TAK: Pokój: Kapuleti i Monteki MUSZĄ żyć w pokoju. Unikajmy starć, idźmy w pacyfizm. Wygaszanie konfliktów, harmonia.
        * TAK: Ratunek przed chorą miłością: TYLKO NIE Celina -> Eustachy. Pls.
    * Rola: First Mate (siłowy / ładownia); chce zejść do bazy bo tu pracuje jego ukochana Daria
* Emilia d'Infernia
    * "żona" Bartłomieja. Żywy arsenał. Egzekutor Bartłomieja. (Emilia -> "Egzekucja MILItarnA")

## Układ arkologii Nativis

* Aspekty:
    * Bardzo dużo roślin, drzew itp. Piękne roślinne miejsce.
    * Bardzo spokojna arkologia, silnie stechnicyzowana i sprawna.
    * Dominanta: faeril > drakolici, ale próba balansowania. Niskie stężenie AI.
    * BIA jako centralna jednostka dowodząca, imieniem "Prometeus"
* Struktura:
    * Wielkość: 4.74 km kwadratowych, trzy poziomy
    * Populacja: 11900 (Stroszek)
        * Operations & Administration: 1100
        * Extraction & Manufacturing: 3213
        * Medical: 355
        * Science: 360
        * Maintenance: 700
        * Leisure: 1000
        * Social Services: 240
        * Transportation: 800
        * Training: 229
        * Life Support: 500
        * Computer & TAI & Communications: 500
        * Engineering: 500
        * Supply / Food: 1200
        * Security: 300
    * Bogactwo: wysokie. Technologia + piękna roślinność.
    * Eksport: żywność (przede wszystkim), komponenty konstrukcyjne, hightech, terraformacja
    * Import: luxuries, prime materials, water, terraforming devices, weapons
    * Manufacturing: jedzenie, konstrukcja, terraformacja
    * Kultura: 
        * dominanta: piękno, duma z tego co osiągnęli, duma z tego że nie ma wojny domowej faeril/drakolici
        * wyjątek: brak.
    * Przestrzenie
        * rozrywki: parki, natura, zdrowe ciało - zdrowy duch
        * inne: hotele, laboratoria terraformacyjne, restauracje...
    * Zasilanie
        * solar + microfusion
    * Żywność
        * różnorodna; to miejsce jest źródłem żywności
        * przede wszystkim roślinna, ale też zawiera elementy zwierzęce (te najczęściej z importu)
    * Life support => terraformacja + filtracja
    * Comms => biomechaniczne okablowanie + normalne sieci
    * TAI => BIA wysokiego stopnia imieniem Prometeus
    * Starport => large
    * Infrastructure => high-tech, dużo półautonomicznych TAI, wszystko nadrzędne przez Prometeusa. Świetny transport publiczny. 
    * Access & Security => arkologia z silnymi blast walls, silnymi liniami dostępowymi.

Historia: 

* w trakcie wojny domowej sprzedawała żywność na 2 fronty co spowodowało znaczne bogactwo ale też częściową niechęć innych arkologii
* obecnie prowadzi ekspansje na struktury zrujnowane/opuszczone po wojnie żeby całkowicie się uniezależnić od dostaw z poza planety
