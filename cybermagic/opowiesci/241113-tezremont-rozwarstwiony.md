## Metadane

* title: "Tezremont rozwarstwiony"
* threads: serce-planetoidy-kabanek
* motives: the-man-in-black, handel-zlym-towarem, hostage-situation, b-team
* gm: żółw
* players: kić, fox, til, vivian

## Kontynuacja
### Kampanijna

* [241106 - Poszukiwanie Szczęśliwej Świnki](241106-poszukiwanie-szczesliwej-swinki)

### Chronologiczna

* [241106 - Poszukiwanie Szczęśliwej Świnki](241106-poszukiwanie-szczesliwej-swinki)

## Plan sesji
### Projekt Wizji Sesji

**WAŻNE**: Kontekst sesji i okolicy oraz Agendy itp DOKŁADNIE opisane w [Thread 'serce-planetoidy-kabanek'](/threads/serce-planetoidy-kabanek). Tam też znajduje się rozkład populacji itp.

* INSPIRACJE
    * PIOSENKA WIODĄCA
        * [Funker Vogt - What If I'm Wrong?](https://www.youtube.com/watch?v=4su5Nus3byM)
            * "If life is a task | But there is too much to ask | If I've been mistaken - all along"
            * Gabriel Lodowiec próbuje naprawić sytuację, ale jego działania prowadzą do katastrof politycznych.
            * Zespół widzi, że Lodowiec chce dobrze, robi dobrze, ale antagonizuje przy tym lokalsów.
        * [Powerwolf - Sacramental Sister](https://www.youtube.com/watch?v=tw3Mf9m3Jj0)
            * "Sacramental sister in heaven or hell | When the night is falling, no moral can tell | Sacramental sister, to God you are sworn | When desire calling, the Bible is torn"
            * Sprzeczności i kolizje. Nie ma jednej ścieżki. Złapać winnych czy uratować przewożonych? Zniszczyć niewolniczy statek i "Orbiter niszczy cywili" czy ich puścić?
    * Inspiracje inne
        * Deal with the Devil
* Opowieść o (Theme and Vision):
    * "_Zrobić, co należy czy co wizerunkowo poprawne?_"
        * SC "Szczęśliwa Świnka" jest pozornie jednostką cywilną, "niegroźną". Orbiter niszczący Świnkę to cios wizerunkowy.
        * Ale uratowanie tych dziewczyn czy doprowadzenie kapitana do stanu bezpieczeństwa ma znaczenie. Ale jak to zrobić?
        * NAPRAWDĘ Świnka jest zaminowana? A póki nic się nie dzieje, może przyjść wsparcie z Anomalii Kolapsu...
    * "_Tezremont podupadł. Nie jest tym, czym powinien być. Lepsza jednostka Orbitera by to zrobiła._"
        * GDYBY Tezremont leciał szybciej, GDYBY miał broń w lepszym stanie, GDYBY miał bardziej zmotywowaną załogę...
        * Lodowiec przegrywa przez to, że Tezremont jest jednostką w tak złym stanie.
    * Lokalizacja: Libracja Lirańska. Najpewniej planetoida Oratel a potem okolice Anomalii Kolapsu.
* Motive-split
    * the-man-in-black: TAI Evraina, TAI kontrolna SC Szczęśliwa Świnka. Ukrywa informacje o działaniach Świnki i wsparciu Ypraznir.
    * handel-zlym-towarem: Szczęśliwa Świnka robi co może, by zarobić, spłacić dług itp. Ma tam ludzi do transformacji w imieniu Ypraznir - organizacji przestępczej.
    * hostage-situation: TAI Evraina trzyma Szczęśliwą Świnkę w szachu; są tam miny, ładunki wybuchowe i cywile. Evraina zrobi wszystko, by udowodnić, że Orbiter zabił niewinnych.
    * b-team: wyraźnie Tezremont nie jest gotowy do tak skomplikowanej operacji; załoga Tezremont jest rozleniwiona i nie mają wprawy do walki "na serio". Są zbyt pewni siebie jako Orbiter.
* O co grają Gracze?
    * Sukces:
        * Uda się coś osiągnąć - albo zatrzymać Świnkę, albo złapać kapitana, albo whatever
        * Uda się nie zniszczyć reputacji Orbitera na tym terenie
        * Uda się nikogo nie stracić
    * Porażka: 
        * .
* O co gra MG?
    * Highlevel
        * Pokazać, że Orbiter jest najsilniejszy na tym terenie, ale nie jest wszechmocny
        * Pokazać jak niesamowicie niebezpieczne jest to miejsce i że ta Grupa Wydzielona Orbitera nie jest dość kompetentna
            * Lodowiec zawodzi jako polityk; Orbiter pomaga ludziom, ale nadal stanowi "demoniczne zło" w okolicy
            * Pawilończyk zawodzi jako kapitan; Świnka stanowi dlań za duży problem
    * Co uzyskać fabularnie
        * Zniszczyć wartościowe 
* Agendy
    * The MiB: chce ukryć wpływ Ypraznir i zrzucić winę na Orbiter. Lub uciec w okolicę Placidistanu.
    * The Rival: chce pokazać 

### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

KONTEKST:

* Zgodny z THREAD

CHRONOLOGIA:

* Znalezieni przekształceni górnicy przez anomalię; Orbiter to przechwycił (Lodowiec)
* Lucjusz odkrył, że za tym stoi statek "Szczęśliwa Świnka"

PRZECIWNIK:

* brak, anomalia?

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

FAZA 1: Szczęśliwa Świnka

* Świnka ma 0.1g przyspieszenia; 15 min: +1 kps
    * w momencie przechwycenia potrzebuje 2 dni do celu lecąc z 20 kps
* Świnka wystrzeliła serię 'chaff'; ominąć? Przelecieć? (to są miny-rakiety); Tezremont musiałby stracić trochę czasu
    * Tezremont niekoniecznie ma sprawne pointdefences; nie testowali tego.
* Świnka zostawiła minę pop-up
* Myśliwiec przechwytujący nie jest w stanie lecieć za oddalającą się szybką jednostką; Ewan go skonfigurował do zabawy.
    * To uruchomi 'Unda Tenebrosa'
* Świnka leci na Placidistan; wysyła przed siebie szybką jednostkę (prom SOS). 
* Świnka faktycznie jest zaminowana; kapitan zginie, jeśli ujawni prawdę. TAI Evraina już o to zadba.


## Sesja - analiza
### Fiszki
#### Orbiter, OO Geisterjager, fregata dowódcza

* Gabriel Lodowiec: komodor Grupy Wydzielonej Mrówkojad Lirański oraz kapitan OO Geisterjager
    * Commander: komodor Orbitera; dowodzi 2 statkami. Dobrze przydziela ludzi do zadań. Dobry taktyk. KIEPSKI w tematy HR.
    * Ekspresyjna Monomania (Light Yagami): bardzo wybuchowy, ale przywiązany do zasad i procedur. Zrobi to, co trzeba, by wygrać i wygra WŁAŚCIWIE. (O- C+ E0 A0 N+)
    * Próżniowiec, Uprzedzenie Do Grupy (noktianie), Weteran: bardzo doświadczony, nie lubi noktian. Ale jeszcze bardziej nie lubi przegrywać misji.
    * Echo Porażki: za Lodowcem do dzisiaj idzie fama "tego, który zrobił przerażającą mowę". Jest osobą, którą się straszy noktian.
* Dariusz Mordkot: pierwszy oficer, marine, dowodzi grupą uderzeniową.
    * Solutor: marine, uzbrojony i dobrze wyposażony
    * Niezależny Wizjoner (Miorine): bierze zadanie i je wykonuje. Pomysłowy, gotowy do nietypowych sytuacji. Lubi się chwalić i pokazywać klasę swych ludzi. (O+ C+ E+ A- N+)
    * Wytworna Elegancja: zawsze doskonale ubrany, w nieskazitelnym mundurze. Śliczne zęby i szeroki uśmiech. No z plakatu rekrutacyjnego.
* Renata Kaiser: oficer komunikacyjny
    * Jest Druga Szansa: wybacza, wierzy, że każdy - nawet noktianin - ma prawo do drugiej szansy. Sympatycznie neutralizuje Lodowca. (O+ C0 E+ A+ N0)
    * Lekko paranoiczna i oddana Lodowcowi
* Ofelia Miris: marine, druga dowodząca grupą szturmową
* OO Geisterjager: fregata dowódcza, 80 osób

#### Orbiter, OO Tezremont, szybka korweta adaptacyjna

* Arnold Pawilończyk: kapitan OO Tezremont pod Gabrielem Lodowcem; około 23 lata
    * Optymistyczny Entuzjazm (Bastian): Arnold jest optymistą i wierzy, że jak zrobi się wszystko dobrze to się jakoś ułoży. Odważny i skłonny do ryzyka. (O+ C0 E+ A+ N-)
    * Trochę za Młody: Arnold jest niestety bardzo wrażliwy na ładne dziewczyny i nieco za bardzo zależy mu na tym co pomyślą inni. Chce wszystkich zadowolić.
* Mateusz Knebor: pierwszy oficer OO Tezremont; 
    * Bezwzględny Mastermind (Shockwave): jego celem jest udowodnienie, że Pawilończyk nie jest dobrym kapitanem i spełnienie celu Orbitera. (O+ C+ E0 A- N0)
    * Kontakty z Syndykatem: nie wie o tym, ale ma informacje oraz dodatkowy sprzęt itp. od ludzi Syndykatu. To oni pokazali mu bezużyteczność Arnolda.
* Natalia Luxentis: cicha noktiańska advancerka, która dołączyła do Orbitera nie widząc lepszej opcji; około 33.
    * Advancer, Inżynier: lepiej czuje się w kosmosie niż w statku czy na planecie, nie jest może dobra w walce, ale świetnie radzi sobie z naprawami w kosmosie.
    * Marzyciel (Luna): ślicznie rysuje swoje światy i swoje historie, rzadko rozmawia z innymi. Ma bardzo neutralną minę, pragnie zadowalać innych. Lubi sprzątać. (O+ C- E- A+ N-)
    * Próżniowiec, Savaranka, Ten Obcy: noktiański nabytek Orbitera; Natalia próbuje być maksymalnie przydatna na pokładzie, ale unika kontaktu z innymi jak może. Overloaded.
* Sebastian Warząkiewicz: ogólnie 'dobry koleś' grupy, inżynier, kompetentny i lubiany, acz bardzo intensywny.
    * Inżynier, kowboj: dobrze strzela, dobrze walczy i spełnia się jako inżynier załogi. Zawsze pierwszy rusza w bój.
    * Ognista Pasja, Uprzedzenie do grupy: chce się bawić, tańczyć i śpiewać. A przy okazji wkopać noktianom, bo zniszczyli majątek jego rodziny i utknął tutaj. "Główny bohater". (O+ C- E+ A- N+)
    * Bodybuilder, Nieformalny Przywódca: Sebastian UWIELBIA ćwiczenia fizyczne i praktycznie ćwiczy cały czas. Do tego zna mnóstwo opowieści o Mroku Noctis i świetnie opowiada.
* Czesław Truśnicki: marine Orbitera, przypisany do OO Tezremont.
    * Solutor: świetny marine i żołnierz. 
    * Opiekuńczy Mentor: z natury dba o harmonię i rozwój innych, dba o każdego członka swojej załogi. Nie uważa, że cierpienie jednostki jest warte sukcesu grupy.
* Grzegorz Dawierzyc: drugi oficer i inżynier
    * Kompetentny oficer, choć niszczy morale podwładnych. Na pewno dowodzi inżynierią i silnikami na OO Tezremont.
    * Wieczny Maruda: narzeka na wszystko, ALE jest lojalny kapitanowi i Orbiterowi. Po prostu trudno z nim wytrzymać. (O- C+ E? A- N+)
    * Istnieje powód, czemu aż tak bardzo narzeka. Nie wiemy jednak jaki. PLACEHOLDER.
* Fred Topokuj: marine Orbitera, przypisany do OO Tezremont.
    * Klanowiec: tylko Orbiter; nikt inny nie ma znaczenia
* Ewan Pilczerek: pilot myśliwca przechwytującego Tezremont
    * Rozdzieracz Iluzji (Dolan): widzi jak jest i mówi co jest. Raczej z boku, mało rozmawia z załogą. Gra sobie w gry w myśliwcu. WIE, że się zapuścił.
    * Zdemotywowany: uważa, że wielkie sukcesy i działania się skończyły i to już nie jest czas dla niego czy Orbitera tutaj
* Ilona Smarznik: artylerzystka i oficer defensyw Tezremont
    * Strażnik Harmonii: nie odzywa się niepytana, dba, by nikt nie miał problemów i się na nikogo nie złościł.
* OO Tezremont: szybka korweta adaptacyjna, 22 osoby
    * zdolna do poważnej zmiany wyglądu i posiada więcej skrytek niż powinna; działa jak QShip.

#### SC Szczęśliwa Świnka

* Klaudiusz Neporyk: kapitan Szczęśliwej Świnki, pod kontrolą Ypraznir
    * Ma dobre serce, ale musi spłacić długi bo jego rodzina ucierpi; nie powie niczego o tym, kto go kontroluje
* Julia Neporyk: pierwszy oficer Szczęśliwej Świnki, dająca nadzieję
    * Optymistka w Koszmarze: Szuka pozytywów nawet w najgorszych sytuacjach, próbując odnaleźć piękno i sens.
* Emilia Stawicka
    * Po przegranym wyroku, jej dusza zgasła. Chłodna, odpycha wszystkich.
* Adam Trybicki
    * Fatalista: Przekonany, że przeznaczenie jest nieuniknione.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Tezremont leci z prędkością 1G. -> 3 dni na dolecenie GDZIEŚ do celu.

Tezremont... nie działa z pełną mocą. Jakieś 60%. Całość rozprzężenia. Kapitan jest najbardziej skupiony. Niestety, Tezremont ma arogancję Orbitera.

Annabelle TAKA ZMARTWIONA - inni muszą pocieszyć, że wszystko dobrze działa. Że faktycznie Tezremont ma potencjał który powinna mieć. Niech jej zaimponują, niech jest lepiej niż w rzeczywistości. Niech działa trochę lepiej i szybciej.

Tr +3 +3Or:

* Or: Annabelle skutecznie zadziałała. Tezremont działa szybciej, działa lepiej. Tezremont NIGDY nie był wykorzystywany na pełnej mocy. Tezremont ma awarię. Innymi słowy - niektóre systemy nie są dość dobrze funkcjonujące. Silniki manewrowe są uszkodzone. Ogólnie działają, ale nie mają "płynności". Warząkiewicz działa.
* (+3Vz), Isa pomaga w naprawie: Vz: udało się. Isa jest ekspertką od taśmy izolacyjnej. Na szybko. I faktycznie - doprowadziła silniki do działania. Nie można im w pełni ufać, to jest "naprawa na szybko", przy otwartych silnikach i bez spowolnień, ale będzie wystarczająco dobra operacyjnie. Naprawy zajęły dobre pół dnia. Ostatecznie jest w porządku, ale bym nie przeciążał.
* Isa podczas naprawy, badań... faktycznie, Tezremont nie jest w stanie operować na pełnej mocy. Działka działają - zarówno działko makronowe jak i rakiety. 

Raczej potrzebujemy bliżej 2 dni by dolecieć do celu.

Sylwia robiła różne materiały szkoleniowe i programy szkoleniowe. Poważnie rozważa rzucenie wyzwania załogantom, żeby przeszli przez scenariusz szkoleniowy zmodyfikowany by Tezremont musiał się zmierzyć z symulacją lokalnych statków na bazie wiedzy o lokalnych statkach. Np. wykorzystać Natalię by się dowiedzieć o niespodziankach lokalnych statków. Doprowadzić do sytuacji, w których symulacja skopie Tezremontowi dupę. Czyli - test Kobayashi Maru. Debriefing.

Sylwia chce pokazać załodze, że nie mogą siedzieć z pełnym samozadowoleniem. Trzeba to dobrze opracować. A jak zauważył Lucjusz - "ludzie Lodowca sobie poradzili".

Symulacja "niby ścigamy Świnkę" ale jest chroniona przez Syndykat Aureliona i zamiast Świnki macie 3 statki. Nawet lokalne statki. Dać przeciwnym statkom miejsca gdzie mamy gorzej obstawione rzeczy przez naszych ludzi - mamy defensywy które po prostu nie działają idealnie.

* Sylwia dostarcza zakres symulacji, plan, by to ogólnie ustawić.
* Isa przygotowuje "co nastąpi jak się przejmą". Rozwieszone tablice informacyjne. Wywieszone "będę organizowała trening bojowy". I pokazać realne uszkodzenia korwety. Bo Isa wie gdzie są problemy statku.
* Natalia jest wykorzystana do urealniania profili statków. 
* Annabelle może zasymulować informacje na co można zadziałać. Są na statku bardziej problematyczne grupy - i niech to rozbicie przeszkodzi w wygraniu. Przez to że nie działaliśmy jako załoga nie wygraliśmy (jeden z czynników).
* Lucjusz jest na mostku i z kapitanem - dodać dodatkowej powagi symulacji (stoi i ocenia), ale z drugiej strony też zrobić tak, by załoga przyzwyczajała się jako nieformalny XO kapitana. Budowanie pozycji że jak w kluczowym momencie wydać polecenie by posłuchali.

Tr Z+3+1Ob:

* Vz: Isa chce, by oni zaczęli dbać o swój statek. Że Tezremont w tym stanie po prostu się nie nadaje do pełnego działania. Niech coś nie będzie wykryte. Ostrzejszy manewr i problem. Tezremont stać na więcej niż teraz robi i może się to skończyć katastrofą.
* V: Annabelle chce, by się zorientowali o tym, że nie mogą być tak porozbijani. Że "Lodowiec by to zrobił". Że są gorsi niż Geisterjager, ale stać ich na więcej.
* Vr: istnieje grupa załogantów, którzy chcą by Tezremont był lepszy, by mógł, by mieć dumę z jednostki. Że to... to jest problem.

Sylwia musi: opracować realistyczną symulację, pokazać właściwe lekcje. Wykorzystuje Natalię (która min powiedziała o strategii na kosmiczny śrut). 

AI Tezremont jest... potencjalnie sprawne, ale nie jest wykorzystana taktycznie. Tezremont nie ma doświadczenia bojowego.

Tr +3 +1Ob:

* V: Z ich perspektywy to jest realistyczne.
* X: Istnieje grupa, która twierdzi, że nie ma co się starać.
* X: Są próby podminowania ćwiczeń, tego wszystkiego - "Tezremont jest wystarczająco dobry". Innymi słowy - rośnie polaryzacja.
* V: Są dwie silnie spolaryzowane grupy. Jedna jest "za", druga "nie ma po co". Grupa "przeciw" raczej się ukrywa

Lucjusz próbuje wyczaić pierwszego oficera - do której grupy należy. Jakie są postawy na mostku. Nie musi wydawać rozkazów, chce zobaczyć jak wygląda morale na mostku, kompetencje, kogoś wymienić...

Tp +3:

* X: Pierwszy Oficer uważa że to wszystko jest wielka manipulacja i niekoniecznie chcecie dobra tej jednostki. "Lucjusz komisarz polityczny".
* X: Kapitan zrobił poważny błąd. Natychmiast pierwszy oficer go nadpisał. Załoga posłuchała pierwszego oficera. W kryzysie słuchają Knebora. A kapitan skończył z "omg nie jestem dość dobry". Oni się posypali. I do teraz nie wiedzą co robić. Mamy kryzys dowództwa i nikt nie wie kto naprawdę dowodzi.
* V: Lucjusz podniósł ten temat, że Knebor zadziałał źle (na debriefing). Gdy się wpierdzielił kapitanowi, Lucjusz "dał mu po łapach". Tak jak on podważył kompetencje kapitana (chain of command) to Lucjusz to natychmiast wypunktuje - tego rodzaju działania są nie w porządku.
    * Knebor NATYCHMIAST się wycofał i zamknął. Pełni tylko rolę XO.
    * Kneborowi zależy na Tezremoncie. Lojalista korwety i Orbitera, ale przeciwnik kapitana. Jego zdaniem kapitan jest problemem. Ale Knebor nie będzie podważał kapitana publicznie; Lucjusz jest jego wrogiem numer jeden.
    * (+1 Ob)
* X: Kapitan powiedział, że chce wymienić pierwszego oficera pod wpływem Lucjusza. Ilona zaprotestowała. Knebor "panie kapitanie, ma pan prawo decydować o jednostce ale to duży błąd." De facto - Lucjusz zrobił mu 'coup'. Counter-coup. On wcześniej ostrzegł o tym kapitana. "O to Lucjuszowi chodziło od samego początku".
    * (+1Ob)
* Lucjusz: To decyzja kapitana, ale to jest zależne od wyników. Dali dupy. Jeśli uważają, że są kompetentni to niech to ma przełożenie na wyniki. (+1Vg).
* X: Kapitan powiedział "ROBIMY TO". Kapitan rozkazał wymienić oficera. Jego kadra oficerska ma zero morale. Stracili wiarę w kapitana. Oficerowie nie wierzą ani w Zespół ani w kapitana.

Lecimy na operację mając zerowe morale, ale Isa jest pierwszym oficerem. Na mostku jest cisza. I Knebor się odezwał:

* Knebor: Panie i panowie, robimy to czego Orbiter sobie życzy. Pawilończyk jest kapitanem.
* Morale Tezremonta i prędkość działania Tezremonta spadła.
    * Jesteście agentami Lodowca, ale takimi "kiepskimi".
    * Oficerowie mają zniszczone morale.
    * Oficerowie NIE POWIEDZĄ i NIE POKAŻĄ załodze co się tu stało

Jak zadziałała symulacja: (11 V 5X) -> XVVV

* Załoga widzi, że są nie dość mocni i nie mają pewnego zaufania wobec Tezremonta. Nie do końca są tak mocni. Motywuje do dodatkowych sprawdzeń, pilnowania i ostrożności.
* Niech załoga uzna wymianę pierwszego oficera (Isa zamiast Knebora) za pozytywną rzecz. Isa jest dobrą osobą na dobrym miejscu (przewrót).
* Malkontenci uważają, że to jest Wielka Polityka. Że to kolejny dowód, że Orbiter "krewnych i znajomych" a Pawilończyk to łyknął. Więc - nie ma co próbować.
* Kadra oficerska uznała to jako przewrót i manipulację ze strony Pawilończyka / Lodowca / Zespołu.

Isa tym bardziej wprowadza ćwiczenia dla załogi - zwłaszcza sytuacje stresowe - a Sylwia je prowadzi. I bardzo na tych zajęciach z załogą się połączyć. Żadna "pani Kwiatkowska a Isa jestem, ale 10 pompek".

Tr Z +3:

* V: Załoga wie co robić, rozumie mniej więcej Isę i ma "stałe fragmenty gry". Coś przećwiczyli pod kątem Świnki.
* Tego typu ćwiczenia są stałym fragmentem gry od tej pory. A Sylwia zrobi to dobrze.
* X: Polaryzacja się rozszczepiła jeszcze bardziej - zwolennicy / przeciwnicy Knebora, malkontenci / optymiści. Innymi słowy - Tezremont leci w najgorszym stanie ever.

Oficerowie przestali udzielać rad prawidłowo. Knebor "złamał regulamin". Tymczasowo oni wcale nie są przekonani że chcą, by operacja się udała.

Ilona Smarznik informuje "zlokalizowałam Świnkę". Świnka jest w zasięgu... 1.5 dnia od planetoidy Placidistan (licząc że się zbliżamy).

Tezremont zbliża się do Świnki. Isa bierze Knebora na stronę.

* Knebor: Pani oficer?
* Isa: Masz ochotę na lunch?
* Knebor: Lubię jeść samemu. Ale możemy porozmawiać w sali konferencyjnej. Gwarantuję, że nie będę podważał pani kompetencji jeśli okażą się wysokie.
* Isa: Panie Knebor... możemy przejść na Ty? Isa jestem.
* Knebor: Wolałbym zostać w komitywie formalnej.
* Isa: Nie planowałam tego, nikogo do tego nie namawiałam, nie wiem jak to się stało. Mam nadzieję, że nie będzie między nami złej krwi.
* Knebor: Oboje jesteśmy oficerami Orbitera.
* Isa: Panie Knebor. Nie o to mi chodzi. Ja tylko mówię, że całe pana działanie na statku było profesjonalne i powinno zostać docenione. Kapitan uznał, że należy mnie awansować nie zmieniło mojego zdania. Jeśli ma pan jakąś potrzebę czy sugestię, będę wdzięczna i nie uznam za podważanie kompetencji.
* Knebor: Tezremont jest na granicy ruiny. Proszę zapewnić, żeby nie stał się naszym grobem. Mi się nie udało.
* Isa: Jeszcze nie jest naszym grobem. Jeśli coś złego się stanie, nie za pana kadencji.
* Knebor: Stało się to za mojej kadencji. Proszę zapewnić, że nie pójdzie to dalej. Nie jest godna bycia jednostką Orbitera.
* Isa: Co się stało?
* Knebor: Kapitan. Więcej nie powiem, bo jestem lojalny kapitanowi, nieważne czy zasłużył czy nie. Powodzenia (lekki uśmiech).

Isa odebrała wrażenie, że Knebor nie będzie konspirował przeciwko Isie. Nie będzie próbował odzyskać stanowiska pierwszego oficera. Uważa Lucjusza za wroga numer jeden.

Lucjusz przygotowuje prom abordażowy. Jeśli chcemy zatrzymać statek, "pełnimy rolę piratów". Wraz ze zbliżaniem się Świnka zaczyna przyspieszać. Komunikacja ze strony Świnki.

* Julia: Tu XO Szczęśliwej Świnki. Oddalcie się - jesteście na naszej trasie i naszym torze.
* Ilona: Tu jednostka Orbitera Tezremont. Żądamy możliwości wejścia na pokład. Wiemy, że przewozicie ludzi.
* Julia: Dokonujecie aktu piractwa. Nie macie prawa wejścia. Nie macie żadnych praw!
* Isa: Zajmijcie stanowiska bojowe.

Świnka wystrzeliła "chmurę śmieci" na prostej trasie. Tam może być mina.

Isa decyduje się rzucić zaklęcie, niech sygnatura rakiety będzie "większa" by miny zadziałały na rakietę. Dokładnie sygnatura Tezremonta. Isa przekierowuje energię, ładuje potężną energię do rakiety, by generować odpowiednią sygnaturę energii. Świnka to poczuje, ale mina też.

Tr ZM +3 +4Ob:

* Vr: Zneutralizowane pole minowe. Przelatujemy przez pole eksplozji.

Załoga jest szczęśliwa. YEAH NASZ MAG. Natalia ZNIKNĘŁA. Natalia zakłada kombinezon advancera. Ale jest wstrząśnięta.

Świnka wysłała prom. W kierunku na planetoidę Placidistan. Prom leci z absurdalnym przyspieszeniem 7G. Maksymalnie 3-4 osoby. I prom wysyła sygnał "SOS PIRACI ORBITERA! SOS ATAKUJE TEZREMONT!" odlatując w stronę na Placidistan (co przechwytuje Ilona).

Prom Świnki odlatuje. Nie zdąży wyhamować. Przygotowanie do abordażu.

* Julia: Przysięgam na wszystkich bogów, Saitaera, Seilię, że jak tylko się zbliżycie - zestrzelimy was. Nie macie prawa. Zestrzelimy wszystko co spróbujecie zrobić. Zestrzelimy Was!

Isa i Sylwia wchodzą na pokład promu. I dwóch marines. A Natalię na zewnątrz.

Jak tylko prom ruszył z Tezremonta działko główne Świnki się obraca, by móc namierzyć od Julii

* Julia: My zginiemy i Wy zginiecie, nie zbliżajcie się do nas!
* Lucjusz: Nie chcemy Was zabić.
* Julia: To się nie zbliżajcie! (ona się boi, to słychać)
* Lucjusz: Tu Lucjusz Jastrzębiec, oficer Orbitera z Tezremonta. Dostaliśmy potwierdzoną informację o niedozwolonych materiałach i handel ludźmi. Przyczyna naszej interwencji.
* Julia: Kłamstwa.
* Lucjusz: Potwierdzenie z planetoidy Oratel że do tego rodzaju i handlu doszło. Wyślijcie manifest statku.
* Julia: Nie macie uprawnień na tym terenie. 
* Lucjusz: Obywatelskie zatrzymanie. Jeśli przewozicie ludzi w ładowniach, nasze uprawnienia nie mają znaczenia. Jeśli się mylimy, zadośćuczynimy za interwencje.
* Lucjusz: Jeśli nie znajdziemy dowodów na to, że dokonujemy tego rodzaju to sprawa będzie jasna. Ale to oznacza natychmiastowe poddanie się do inspekcji i niepodejmowanie działań pozbycia się ładunku.
* Julia: Nie podejmujcie żadnych działań. W ciągu 10 minut wrócimy do Was.
* Lucjusz: Jeśli z tego wynikają pani wątpliwości - nasze działa są bliżej niż cokolwiek może zrobić Fabryka.
* Julia: (śmiech przez łzy) Dajcie nam 10 minut.

Abordażujemy. Tezremont "macie 30 sekund do poddania się inspekcji po której podejmiemy ruchy i działania"

* Julia: Chodźcie! Chodźcie wszyscy! Zabijemy was wszystkich!

Tezremont niszczy działko Świnki. Świnka próbuje przyspieszyć / zwalniać - prom ma większą moc. Śluza. Abordażują. Zespół wchodzący na pokład:

* Sylwia jako advancer
* marines. Przez śluzę - flashbang.
* Isa.

Czeka pani oficer w kombinezonie. Dostała flashbangiem. Przechwycona, nie jest uzbrojona. Nie jest komunikatywna. Ma transmiter, nie ma pułapek.

* Klaudiusz: Tu kapitan. Orbiter, poddajcie się, albo zginiecie zarówno Wy jak i wszyscy na pokładzie.

Isa podłącza się do systemu statku. Statek "z dumą" prezentuje że jest zaminowany. Mamy Natalię na zewnątrz Świnki. Niech Natalia przedostanie się na zewnątrz mostka i zadziała stamtąd.

Tr Z +2:

* Vz: Coś tu jest nie tak.
    * Julia przyszła bezpośrednio.
    * Kapitan wyraźnie jest przekonany że coś jest nie tak.
    * Isa nie musiała hackować systemu. System sam jej pokazał te miny.
    * Odbieracie wrażenie, że niekoniecznie oni decydują co tu się tu dzieje.
    * Oni nie chcą zabić Orbitera. Oni nie chcą ginąć.

Meksykański impas. Wszystko zależy od Natalii..?

## Streszczenie

Tezremont, działająca na zaledwie 60% swoich możliwości, wyrusza na misję ścigania przemytniczego statku "Szczęśliwa Świnka". Na pokładzie Tezremonta załoga zmaga się nie tylko z technicznymi awariami, ale również z kryzysem dowództwa, napięciami wśród oficerów i polaryzacją morale. Przez machinacje Lucjusza Knebor przestał być pierwszym oficerem; a potem weszli na Świnkę, która okazała się być pułapką. Świnka zagroziła samozniszczeniem, mając na pokładzie oficerów Orbitera...

## Progresja

* Isa Całujek: awansowana na pierwszego oficera przez Pawilończyka w miejscu Knebora.
* Mateusz Knebor: zdegradowany z pozycji pierwszego oficera, ale dalej robi co może by ratować Tezremont.
* Lucjusz Jastrzębiec: całkowity brak zaufania oficerów Tezremont (poza Pawilończykiem) do niego; to "pies Lodowca" i mistrz intrygowania.
* OO Tezremont: całkowicie rozsypane morale i bardzo niska efektywność jednostki, mimo, że oficerowie próbują ekranować załogę.

## Zasługi

* Sylwia Mazur: Przygotowała realistyczną symulację bojową, która obnażyła słabości Tezremonta i załogi, zmuszając ich do refleksji nad ich przygotowaniem. Prowadziła ćwiczenia stresowe, które poprawiły zgranie części załogi, mimo oporu malkontentów. Wzięła udział w abordażu „Szczęśliwej Świnki” jako taktyczka i advancer, wspierając marines w przejęciu statku.
* Annabelle Magnolia: Mobilizowała załogę, by zwiększyć efektywność Tezremonta mimo jego ograniczeń technicznych. Jej entuzjazm i zdolność motywowania innych poprawiły działanie systemów statku i morale części załogi. W symulacji zwracała uwagę na znaczenie współpracy, przyczyniając się do lepszego zrozumienia problemów wewnętrznych Tezremonta.
* Lucjusz Jastrzębiec: Podważył działania pierwszego oficera Mateusza Knebora, co doprowadziło do jego degradacji i powołania Isy na jego stanowisko. Konsekwentnie dążył do przywrócenia łańcucha dowodzenia, choć kosztem morale kadry oficerskiej. Podczas negocjacji z załogą "Szczęśliwej Świnki" wykorzystał swoją pozycję i autorytet Orbitera, by uniknąć eskalacji konfliktu.
* Isa Całujek: Przeprowadziła kluczowe naprawy silników manewrowych, pozwalając Tezremontowi zwiększyć wydajność na czas misji. Po awansie na pierwszego oficera zjednoczyła część załogi, organizując ćwiczenia stresowe i naprawiając relacje po kryzysie dowództwa. Odegrała kluczową rolę podczas pościgu (magia i neutralizacja torpedy).
* Natalia Luxentis: Skutecznie dostosowała profile lokalnych statków do symulacji bojowej Sylwii, zwiększając ich realizm. Podczas abordażu "Szczęśliwej Świnki" działała na zewnątrz statku, próbując zneutralizować sytuację.
* Mateusz Knebor: Były pierwszy oficer Tezremonta, którego decyzje i konflikty z kapitanem Lucjuszem doprowadziły do jego degradacji. Choć lojalny wobec statku, podważał kompetencje dowódcy, co przyczyniło się do kryzysu morale kadry oficerskiej. Po degradacji wycofał się z działań konspiracyjnych, koncentrując się na zadaniach operacyjnych.
* Ilona Smarznik: Specjalistka wywiadu i analizy, która zlokalizowała "Szczęśliwą Świnkę" i monitorowała jej ruchy podczas operacji. Nieco zastraszona sytuacją, nie chce się odzywać niepytana (błędnie boi się losu Knebora).
* Klaudiusz Neporyk: Kapitan "Szczęśliwej Świnki", który groził samobójczym zniszczeniem swojego statku, aby zapobiec jego przejęciu. Jego desperacja była wynikiem presji ze strony niezidentyfikowanych sił zewnętrznych.
* Julia Neporyk: Wyszła naprzeciw Zespołu, dostała ogłuszającym granatem i dzięki temu (po odzyskaniu świadomości) będzie w stanie wyjaśnić Zespołowi ich nędzną sytuację.
* Arnold Pawilończyk: Flip-flopuje. Potem zrobił poważny błąd. Natychmiast pierwszy oficer go nadpisał. Potem zdegradował pierwszego oficera. Pokazał, że jest nie dość silny jako kapitan. Dołączył do frakcji Lucjusza.
* OO Tezremont: Zmaga się z technicznymi awariami i wewnętrznymi podziałami załogi; skutecznie jednak zaatakował i przechwycił Szczęśliwą Świnkę.
* SC Szczęśliwa Świnka: Uciekał przed Tezremontem, ale mu się nie udało. Gdy Orbiter zaabordażował, Świnka ostrzegła przed samozniszczeniem i wzięła Orbiter na zakładników. A przynajmniej w to wierzy.

## Frakcje

* .

## Fakty Lokalizacji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Libracja Lirańska

## Czas

* Opóźnienie: 3
* Dni: 1

## Inne
### Niezrealizowane:

* Dlaczego nie dotarła Viv na czas?
* Pawilończyk i kapitan Fuksja Rakańska (milf); w Przeładowni jest mało ludzi, są jakieś towary. Fuksja pomoże Pawilończykowi.
* Znajdźcie informację o "nieGórnikach" na planetoidzie Kabanek. Tam jest centrum cywilizacji
    * marudny Warząkiewicz opowie że Lodowiec to kupa lodu i debil
* Maria Kownicz + Jacek Paszągiewicz; oni dowodzą Kabankiem
    * Jest tam silny koleś, Janusz Czerwikowicz (Terroryzujący Despota). Nie lubi Orbitera. Nie pozwoli się szarogęsić.

FAZA 2: Szczęśliwa Świnka

* Świnka ma 0.1g przyspieszenia; 15 min: +1 kps
    * w momencie przechwycenia potrzebuje 3 dni do celu lecąc z 20 kps
* Świnka wystrzeliła serię 'chaff'; ominąć? Przelecieć? (to są miny-rakiety); Tezremont musiałby stracić trochę czasu
    * Tezremont niekoniecznie ma sprawne pointdefences; nie testowali tego.
* Świnka zostawiła minę pop-up
* Myśliwiec przechwytujący nie jest w stanie lecieć za oddalającą się szybką jednostką; Ewan go skonfigurował do zabawy.
* Świnka leci na Placidistan; wysyła przed siebie szybką jednostkę (prom SOS). 
* Świnka faktycznie jest zaminowana; kapitan zginie, jeśli ujawni prawdę. TAI Evraina już o to zadba.

### RANDOMOWE FISZKI

* Kamil Drobiakis
    * Zapisano w Gwiazdach: będzie walczył o to, o co należy walczyć. Będzie dzielnie wspierał.
    * BARDZO duże poczucie sprawiedliwości.
* Marek Osadowski
    * Pionier: Uwielbia pracować w trudnych warunkach, jest specjalistą od naprawiania wszystkiego przy minimalnych zasobach. Ma nieustępliwą determinację.
    * Dotknięty Lacrimor: Przygnieciony brutalnością przestrzeni kosmicznej, zaczyna poszukiwać ekstremalnych, nielegalnych środków, by przetrwać.
* Iwan Beresin
    * Opiekuńczy Mentor: Zawsze służy radą i wsparciem młodszym członkom załogi, zapewniając im spokojne miejsce do rozwoju.
* Paweł Krzypiowski
    * Strażnik Harmonii: bez tego wszyscy zginiemy i się skończy
    * Piękno Interis: Pogodzony z nieuchronnością końca, widzi piękno w przemijaniu, co czyni go spokojnym obserwatorem chaosu.
* Sara Woźniak
    * Artystyczna Dusza: cicha, uparta ale ustawione co i jak lubi
* Kamila Figlet
    * Optymistka w Koszmarze: Szuka pozytywów nawet w najgorszych sytuacjach, próbując odnaleźć piękno i sens.
* Emilia Stawicka
    * Po przegranym wyroku, jej dusza zgasła. Chłodna, odpycha wszystkich.
* Adam Trybicki
    * Fatalista: Przekonany, że przeznaczenie jest nieuniknione.

### Blueprint Świnki

Endurance > Speed > Utility > Armour > Guns

duża korweta transportowa, 30 osób

* Przednia Część:
    * Mostek
    * Centrum Komunikacyjne
    * Strefa Relaksu
    * Kuchnia i Jadalnia
    * Działko laserowe (z górnej strony)
    * AI Core
    * Magazyny
* Środkowa Część:
    * Jawna część
        * Moduły Mieszkalne
            * Kajuty
            * Łazienka soniczna
            * Sala Gimnastyczna
        * Kabina kapitana.
        * Żywność
        * Medical
        * Śluza główna
        * Life Support
    * Tajna część
        * Miejsca przemytu i transportu ludzi
        * Gaz
        * Ładunki wybuchowe
* Tylna Część:
    * Magazyny
    * Zapasy wody, paliwa...
    * Maszynownia
    * Główne silniki statku
    * Inżynieria
    * Doki Serwisowe

Co ma:

* 1 Prom Ratunkowy (napęd chemiczny, szybki, małe delta-v)
* 1 'chaff'; kosmiczne śmieci z poukrywanymi 3 minami-rakietami
* 1 'pop-up mine'