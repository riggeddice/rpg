## Metadane

* title: "To, co zostało po burzy"
* threads: brak
* motives: dreszczowiec, milosc-ponad-zycie, teren-morderczy, potwor-z-horroru, one-by-one-they-die, izolacja-zespolu, burza-magiczna
* gm: żółw
* players: fox, kapsel, kić

## Kontynuacja
### Kampanijna

* [221006 - Ona chce dziecko Eustachego](221006-ona-chce-dziecko-eustachego)

### Chronologiczna

* [221006 - Ona chce dziecko Eustachego](221006-ona-chce-dziecko-eustachego)

## Plan sesji
### Theme & Vision

* Nativis ma straszliwe potwory i nie ze wszystkim da się walczyć
* Nativis ma straszny teren
* Nativis ma też zespoły złomiarzy i ekstraktorów
* Planeta nie należy do nas...

### Co się stanie (what will happen)

przed sesją:

* Zniknęła Kamila (żona Antoniego); Antoni ją zostawił przy anomalnym miejscu i gdy wrócił, nie było jej

.

* S00: PRZESZŁOŚĆ, SETUP: 
    * Rufus, Anna, (Aniela, Zofia, Eustachy), Antoni robią operację harvestu po Burzy Piaskowej
        * harvest 'ruchomych wyładowań'
        * pomniejsze kwarcyty z wyładowaniami + 'resztki' (cenne do magitrowni)
        * duży kwarcyt manipulujący piaskiem i wirem
        * POKAŻ: Antoniego i przyjaźń Antoni - Anna - Rufus; Anna i jej mechaniczny ptak

.

S0 -> S1:

* Anna pojechała z Antonim - był sygnał Kamili. Rufus nie chciał jechać, ale Anna pojechała. Miało być za chwilę. Ale nie zdążyli i przyszła burza.
* Rufus rozpaczliwie szuka Eustachego by ten mu pomógł odzyskać Annę. Jest to niebezpieczne, potrzebny mu mag i upoważnienie.

.

* S01: TERAŹNIEJSZOŚĆ, SETUP: 
    * Anna z ekipą nie wróciła z sektora 74s; tam było coś wskazującego na Birutan i los Kamili. -> Rufus, ratujmy Annę
    * Wiadomość od Kalii -> Eustachego, good natured ribbing przez Cypriana i retorta Michała.
* S02: Zbliżamy się do sygnałów
    * Przechwycony sygnał: Anna1
    * Cyprian ma lekkie nerwy; Rufus i Michał go opieprzają
* S03: Łazik
    * Samobójstwo Mirki.
    * Advancer w cieniu dojrzy Scutiera wskazującego na Annę
* S04: Jaskinia 1
    * Złamany Antoni z martwym Benkiem
    * Wiadomość od Anny (Anna2)
    * Mirraith DOTKNĄŁ MENTALNIE Michała ("Sekret Rufusa - co jest specjalnego w Seiren")
    * Rufus: "Nie! Patrz na niego! Nie zostawię jej samej, Anna nie skończy jak Antoni!!!"
    * Jaskinia 2 ma sygnały tektoniczne; coś tam się dzieje. I jest transponder. Trzeba się spieszyć.
* S05: Jaskinia 2
    * Wiadomość od A->M
    * Anna, stoi w jaskini i patrzy na ścianę.
    * Mirraith: "Nie! Patrz na niego! Nie zostawię jej samej, Rufus nie skończy jak Antoni!!!"; pożera go.
    * Mirraith: "Ale... dasz mi się zaprosić na kolację?"
* S06: Ucieczka
    * Cyprian złamany. "Nie jesteście ludźmi!!!". Widzi Annę jako jedyną osobę, ich jako potwory.
    * Antoni opętany przez mirraitha. Anna 1: "Nie wrócę do domu".

Z CZYM WALCZYMY:

* mirraith (mirror wraith | Anna) <-- IMPOSSIBLE
    * "mirror of soul": pokazuje to co było w sercu; widzisz to czego pragniesz
    * "mirror form / souleat": wygląda jak ofiara + pożera ją
    * "displacement": nie jest tam gdzie się wydaje; opętuje Scutiery. Gdzie Scutier tam mirraith
    * "send back corrupted signal": wysyła sygnał jaki się pojawia
* krystalnik
    * wyładowanie elektryczne, manipulacja piaskiem, chce zjeść energię

TEKSTY które się pojawią:

* Kalia1: "Ta wiadomość jest dla Eustachego: tu Kalia. Nie wiem czy mnie poznajesz. Ale... dasz się mi zaprosić na kolację? ALE TY STAWIASZ! I NIE POMYŚL NIC! To jest..."
* Anna1: "Kocham Cię, najdroższy. Nie powinnam nigdy tu przyjeżdżać. Nie wrócę do domu... Kocham Cię, Rufusie..."
* Anna2: "Mam sygnał Kamili, Antoni. Wreszcie ją mam!"
* Antoni1: "Kamilo? Kamilo!"
* Antoni2: "Halo? Jest tam ktoś? Ktokolwiek? Pomocy..."
* Mirka1: "Aaah! Dobra, Benek, to Ty... Benek? BENEK?!"
* Benek1: "Tu Benek; nie wykrywam żadnych anomalii; schodzi w głąb."
* A->M: "Jej sygnał dochodzi stamtąd? Jesteś całkowicie pewna?" "Tak, to dziwne, ale tak jest"

Lokacje / aspekty:

* burza piaskowa. 
    * Aspekty: nic nie widać, static, echo sygnałów
* jaskinie. 
    * Aspekty: ciasno, niebezpieczna trasa, krystalniki, łatwo uszkodzić Scutiery
    * Jaskinia 1
        * Znajduje się tu złamany Antoni, głaszczący w Scutierze martwy Scutier z Benkiem "Kamila... moja Kamila..."
    * Jaskinia 2
        * Znajduje się tu mirraith
* derelict. 
    * Aspekty: echo komputerowe, migające światła, zwłoki Mirki (samobójstwo)

### Sukces graczy (when you win)

* run away, cut losses

## Sesja właściwa

### Fiszki

ŁAZIK SEIREN:

* Zofia d'Seiren: salvager Serien (Seiren Local / combat / medical / tectonics) faeril: "Anna mnie uratowała gdy nikt nie dał mi szansy"   <-- KIĆ
    * (ENCAO: -0--+ |Unika kłopotów, roztargniona| VALS: Conformity, Security >> Hedonism| DRIVE: Wieczny dług (Anna))
    * "Życie może jest ciężkie, ale możemy sobie pomóc; działajmy razem." "Uważaj... to może być niebezpieczne."
* Aniela Myszawcowa: salvager Serien (Seiren Local / advancer / signal operations) faeril: "Im doskonalsza technologia, tym lepiej kontrolujemy sytuację"   <-- FOX
    * (ENCAO: -0+00 |Wszystko rozwiążę teorią;;Przedsiębiorcza i pomysłowa| VALS: Achievement, Face| DRIVE: Neikatis ma tyle sekretów i je zrozumiem)
    * "Dzięki technologii Neikatis stanie przed nami otworem. Tyle jeszcze nie rozumiemy, ale nauką i postępem złamiemy tą planetę"
* Anna Seiren: salvager Serien (owner, analityczka / radar / hacker) faeril: mechaniczny ptakozwierz na którym jej zależy, life is pain work is life
    * (ENCAO: -00+-, Nie lubi ryzyka + Altruistyczny, TAK: face + achi, NIE: Self-direction, Strach przed zapomnieniem)
    * "Walczmy o lepszy świat! Jak długo unikniemy Birutan, jesteśmy w stanie zbudować własne małe sukcesy!"
    * kadencja: optymistyczny głos typu Sisko
* Rufus Seiren: salvager Serien (owner, pilot / heavy stuff / appraisal ) faeril: skupienie na przychodach i miłość do sprzętu | Lancer z pochodnią plazmową
    * (ENCAO: +-00- |Szuka wyjaśnień w istniejącej rzeczywistości + Niefrasobliwy, beztroski, TAK: Benevolence, Hedonism >> Face | DRIVE: Hardholder)
    * "Bez nas to wszystko by się rozpadło", "Chcecie wrócić do domu ubodzy? :D"
    * kadencja: wesoły, HERE COMES ARTILLERY!!!
* Michał Uszwon: salvager Serien (hired muscle / engineer) faeril: robota czeka.
    * (ENCAO: 00-0+ |Zakłóca spokój; nie toleruje spokoju i nudy| VALS: Face >> Achievement| DRIVE: Poznać sekret Rufusa - skąd ma Seiren?)
    * "Jak masz czas gadać to masz czas pracować"
    * kadencja: twardy, nie okazuje słabości, gardzi mamlasami
* Cyprian Kugrak: salvager Serien (hired muscle / combat) faeril: tylko ludzkie formy
    * ENCAO:  0-0++ |Unika nieprzyjemnej roboty;;Lubi pochwały| VALS: Hedonism, Family| DRIVE: Bezpieczeństwo swego dziecka
    * "Nienawidzę tych wszystkich anomalicznych gówien..."
    * kadencja: pomocny, dobrą rękę poda

ŁAZIK Uśmiech Kamili:

* Antoni Grzypf: salvager Uśmiechu (owner)
    * (ENCAO: -+0-0 |Niepokojący;;Apatyczny| VALS: Family, Power >> Face, Hedonism| DRIVE: Odnaleźć Kamilę, Zadośćuczynienie za zdradę)
    * "Straciłem to co kochałem najbardziej. Tylko pracą mogę zagłuszyć pustkę."
    * kadencja: puste serce, obsesja na punkcie Kamili, nie ma uczuć
* (Kamila, nieobecna salvager Uśmiechu, echo)
    * zniknęła - próbowała komuś pomóc, niestety, najpewniej natknęła na Birutan

### Scena Zero - impl

Trzy miesiące temu.

Wujek chce, by Eustachy wiedział jak działa Neikatis, jak wyglądają sytuacje. W związku wsadza Eustachego w rzeczy. I tym razem wsadził Eustachego do salvagera. Przez pewien czas po burzy planeta ma wyższe pole magiczne. I pojawiają się nietrwałe anomalie czyli efemerydy. Plus trwałe anomalie. Rzeczy, które da się pozyskać i spieniężyć. Salvagerzy próbują szybko po burzy piaskowej znaleźć miejsce bogate w anomalie ale nie ZBYT bogate i zebrać co się da, by odsprzedać to arkologii.

"Skorpion" to klasyczny pojazd typu salvager; do 10 osób. Duże koła, mały kadłub, opcje załadunku. Eustachy pełni rolę TECHNIKA POKŁADOWEGO. Bo wujek prosił. Anna podeszła z uśmiechem i optymizmem. Rufus ma Lancera z pochodnią plazmową. Ogólnie - dowodzenie miło przyjęło Eustachego. Zofia i Aniela kiwnęły głowami a Antoni nawet nie zauważył obecności Eustachego.

Przestrzeń z godną ilością anomalii. Drobne anomalii przez coś większego:

Tr Z (ogromne doświadczenie) +3:

* Vz: Aniela widzi dużą ilość małych piki. Potencjalnie duży krystalnik.
* Vr: Jest tam potencjalnie więcej niebezpieczeństw co wynika z podłego terenu ale macie maga.

Rufus "Świetnie, Anielo. Piękne złoże pieniędzy." Aniela "Wygląda odpowiednio." Rufus: "Zagrożenie?" Aniela "Żółte + teren". Rufus skierował Skorpiona.

Nadal w obszarze pyłu. Czyli ograniczona widoczność. Na szczęście Scutiery są skonfigurowane na detekcję i syfonowanie magii. Na detektorach widać pomniejsze punkty magii. Rufus "dobrze, ekipa, zbieramy!" a sam został na odwodach.

Zofia zajęła się czystą ekstrakcją; dzięki Anieli wie gdzie są krystalniki. Rufusa na odwodach. Antoni odpala mały krystalnik i Eustachy ma się tym zająć. Potem Eustachy coś odpali i jak coś bardzo nie wyjdzie to będziemy patrzeć co dalej. Tu jest wyścig z czasem.

Tr Z (nie pierwszy raz, info od Anieli) +4:

* X: Mechaniczny ptak Anny zestrzelony przez krystalnik. Anna: "Jastrząb!", distress
* X: Krystalniki zaczęły się budzić. Plan był dobry - ale coś się stało i po prostu nagle tego jest sporo.
* V: Udało się bezpiecznie wycofać, krystalniki strzelają, ale ich siła ognia nie jest dość duża.

Rufus dał Eustachemu konsoletę, niech postrzela. Gatling.

Tr Z (gatling) +3:

* V: rozstrzeliwujesz te krystalniki a zespół łazi i syfonuje.
    * Rufus oraz Antoni próbują dotrzeć do Jastrzębia.
* Vr: Osłaniasz ich bez kłopotu, Antoni pierwszy dotarł do Jastrzębia.

I wtedy pod Rufusem i Antonim zaczął pojawiać się wir piaskowy. To krystalniki. Ponad dwumetrowa struktura z wirujących kryształów która emituje małe wyładowania. Rufus nie ma dobrego strzału; zapada się trochę. Eustachy zmniejsza szybkostrzelność, podnosi prędkość wylotową i strzela POD LANCERA by zeszklić piasek. By Lancer mógł strzelać.

* (+3Or): X: Plan jest dobry. To zadziała. Ale nie zdążysz i Rufus. Ktoś musi kupić czas inaczej.
* Vz: Operacja się uda - jeśli komuś uda się kupić czas to automatycznie Rufus jest w stanie poradzić sobie z krystalnikiem.

Jak kupić czas z krystalnikiem? Polecenie - jedna skupia ogień na krystalniku i się zbliża, potem druga. Podskakują i odskakują. Inny pomysł - granat soniczny by wywołać rezonans.

Aniela podskakuje i bierze uwagę.

Tr Z (odwrócona uwaga) +2 +5Or:

* V: udało się ściągnąć ogień i nie przyjęłaś szczególnych ran
* X: nie udało się odwrócić uwagi od Anieli; krystalnik walnął w nią potężnym wyładowaniem.
* Vz: granat soniczny. W kierunku na lewą część "torsu" potwora. Stwór opada.

Rufus pochodnią plazmową wchodzi w rdzeń cholerstwa. Antoni zbiera energię.

* (+Vg) Vg: Zofia szybko wciągnęła Anielę na pokład Skorpiona i zadbała o nią. Scutier... elektronika sfajczona. Po prostu nie. Ale izolacja itp zadziałały. Tydzień przy aktualnych elementach medycznych aż będzie lekka praca.

Więcej szczególnych problemów już nie było...

### Sesja Właściwa - impl

Burza piaskowa. Jesteś w Nativis. Bar Śrubka z Masła. Eustachy siedzi sobie i pije z Jankiem Lertysem. Wpada praktycznie przestraszony Rufus. Idzie prosto do Eustachego. "Będę potrzebował Twojej pomocy." Antoni zabrał Annę i pojechali ratować Kamilę. Ponad 10 godzin. Pojechali skorpionem Antoniego.

Rafał Kidiron. "Każdy Korkoran potrzebuje wielkich czynów by się wykazać na tle tak szlachetnego rodu". Dobrze, udostępni Eustachemu satelitę. Czemu nie. Wykorzysta to pokazując wujkowi jak bardzo jest to pomocne...

Eustachy potrzebuje Lancera. I magią wzmacnia Skorpiona pomagając lokalnemu inżynierowi.

Tr Z (sprzęt + inżynier) M (inżynier) +3 +3Ob:

* Vz: Skorpion WYTRZYMA burzę. W obie strony. Potem jest zniszczony. + generator memoriam.
    * Rufus jest mega wdzięczny
    * technicznie tamten Skorpion miał 5 osób.
* Vz: Sprzęt zapasowy Inferni jest wzmocnieniem Skorpiona. Czyli elektronika zadziała.

Skorpion od środka wygląda... jak mechaniczna, pancerna krypta. Wasz Skorpion wyruszył.

Jeszcze na odjeżdżaniu wiadomość przechwyciliście:

* "Rafał Kidiron. Powodzenia."
* Kalia1: "Ta wiadomość jest dla Eustachego: tu Kalia. Nie wiem czy mnie poznajesz. Ale... dasz się mi zaprosić na kolację? ALE TY STAWIASZ! I NIE POMYŚL NIC! To jest..."

Zofia masz mapę satelitarną dzięki Eustachemu. Możesz dzięki temu określić najlepszą drogę - minimalna ilość anomalii (wykrywane rzeczy + radary + kierunek poprzedniego Skorpiona). Satelita przekazuje pozycję drugiego Skorpiona; jest dość daleko (1h drogi), jest nieruchomy. Są tam jaskinie o własnościach ekranujących.

Zofia ustala trasę na znaną pozycję łazika. W tych jaskiniach zwykle nie ma wartościowych anomalii. Zofia wytycza najbezpieczniejszą trasę, ale bierze poprawkę na burzę.

Ex Z (satelita) + 4 + 5Or (stres Rufusa):

* Vz: Skorpion dojedzie tam nie najszybszą drogą ale bez zbędnych uszkodzeń.
* Vr: Zofia wytyczyła i nadepnęła na pedał serio mocno
    * Rufus: "Możemy jechać szybciej?" Eustachy "Jedziemy szybciej niż moglibyśmy". Rufus zamknął mordę.

Anielo, monitorujesz systemy, kamery itp. "Ćmy" oblepiły jednostkę i czekają. Generatory działają. Siedzi tam i czeka. Eustachy odpala racę; chce zobaczyć zachowanie robactwa.

Zakłócony sygnał z "Uśmiechu Kamili". W ogóle dziwne, że dotarł do Was. Satelita nie odbierze.

Tr Z (sprzęt Inferni) +3:

* Vz: 
    * są szepty i dziwne sygnały, rozmowy
    * Anna1: "Kocham Cię, najdroższy. Nie powinnam nigdy tu przyjeżdżać. Nie wrócę do domu... Kocham Cię, Rufusie..."
* X: ZAINTERESOWANIE stworów. Próbują się przebić.
* Vz:
    * gdy odpalona jest flara to szepty się zmniejszają bo część stworów leci ale wraca
    * "jedzenie". One postrzegają nas jak jedzenie.

Rufus krzyczy: "Powiedz jej że jesteśmy bezpieczni! Powiedz jej że jedziemy! Powiedz jej że jest blisko!". Eustachy: "może zlecieć się więcej." Rufus: "Jeden sygnał. Powiedz jej tylko że jesteśmy..." A Aniela chce zbadać sygnał.

* X: To JEST głos Anny. To ona. TAI potwierdziła łącznie z analizą sentymentu. To na 100% ona. --> Rufus po usłyszeniu koniecznie chce jej powiedzieć.

Rufus nadaje "jedziemy". Nagra i odtworzy i co stwory podłapią i czy do SZEPTÓW doda się głos Rufusa.

* X: Rufus wysłał sygnał "jedziemy". Stwory się zainteresowały, sygnał radioelektroniczny. Szybciej mocniej.
    * Aniela ma sygnał ze Skorpiona. Bardzo uszkodzony. Anna + Kalia "Dasz mi się zaprosić na kolację?"
    * Ten sygnał ze Skorpiona sprawił, że stwory odleciały. A po chwili wróciły. Ostrożnie.

Jedziemy dalej, stwory musimy olać bo na razie nic nie zrobimy... Cyprian "będzie... ciekawiej niż miałem nadzieję". A Michał "zamknij mordę."

Docieracie do terenu, na szczęście burza jest trochę mniejsza. Gdy się zbliżyliście powiedzmy 100 metrów do drugiego Skorpiona, wszystkie stwory odleciały. Po prostu odleciały. Podjeżdżacie do łazika, ten łazik jest w kiepskim stanie - dostał od burzy. I jest zdehermetyzowany, drzwi są otwarte. Niezamknięte. Tu jest sporo sygnałów 'w powietrzu', to sygnały 'stare'. 

Tr Z (sprzęt Inferni) +3:

* X: nie da się z poziomu Skorpiona znaleźć ŹRÓDEŁ sygnałów. Musisz mieć triangulację.

Zofia odpycha Rufusa w kąt, odpala Scutiera i bierze skanery. Zofia idzie w burzę. Zatrzymuje ją Michał. Chce iść z nią i niech nie jest idiotką. Cyprian "Zosiu, jesteś naszym jedynym medykiem. Ani Tobie nikt nie pomoże ani Ani nikt nie pomoże." 

Cyprian i Zofia wychodzą. I do łazika. Nic Was nie atakuje. Jeden Scutier z aktywnym transponderem jest na Skorpionie. I znaleźliście. Skorpion jest poszarpany przez burzę i przez te latające "szczury". Elektronika migocze. Reaktor pulsuje. Nietypowy tryb reaktora.

Martwa Mirka - otworzyła drzwi łazika i zdehermetyzowała hełm. Jej ostatni sygnał:

Mirka1: "Aaah! Dobra, Benek, to Ty... Benek? BENEK?!"

Zofia chce zwrócić ciało Mirki na pokład i je zbadać. 

Rufus szaleje - chce jak najszybciej skomunikować się z Anną. Michał i Eustachy mu nie pozwalają.

Zofia i Michał łączą siły i skanują sygnały mechaniczne i biologiczne. Michał jeszcze -> Eustachy: "panie kapitanie, mogę iść do tego Skorpiona i spróbować coś z nim zrobić by piękna Aniela mogła nie wiem, coś się dowiedzieć?"

ZESPÓŁ: 

* Rufus + Michał są na tamtym Skorpionie ("Uśmiechu Kamili"). Dowiedzieć się co sprawiło że opuścili łazik. Po tym wrócić.
* Zofia + Aniela są na Seirenie. Aniela dekoduje dane i szuka, Zofia robi autopsję.
* Eustachy + Cyprian idą w jaskinie. I Eustachy robi nić Ariadny.

.

Aniela korzysta z pierwszych sygnałów od Michała. (+2Vg)

* V: 
    * Anna3: "Antoni... jak Rufus się dowie że mnie porwałeś..." Antoni: "Aniu, nie miałem wyjścia, wybacz."
    * Anna2: "Mam sygnał Kamili, Antoni. Wreszcie ją mam!"
    * Benek1: "Tu Benek; nie wykrywam żadnych anomalii; schodzi w głąb."
    * Antoni2: "Halo? Jest tam ktoś? Ktokolwiek? Pomocy..."
    * A->M: "Jej sygnał dochodzi stamtąd? Jesteś całkowicie pewna?" "Tak, to dziwne, ale tak jest"
* V: 
    * Kornelia "tu Kornelia... jestem w środku burzy... nie wracam... zginę tutaj... pomocy..." (ten nie jest retransmitowany)
* V: Udało się zlokalizować transponder scutiera. On jest prawdziwy, pasuje do Kornelii.
    * K: "halo? Pomocy? Co się dzieje? Czemu oni wszyscy? Nie wchodzę tam. Wolę zginąć tu. Ale może ktoś tu jest?"

Eustachy, jaskinie w których się znajdujesz są fatalne. Strukturalnie są słabe, jest ciemno, jest ślisko i mają "ostre krawędzie" i szczeliny. Eustachy ma dobry sprzęt. Cyprian nie. Rufus ma ogarnąć i przygotować rzeczy. Eustachy ma upewnić się, że to zadziała. Lekko wzmocnić jaskinię.

RETRANSMISJA: "Mam sygnał Kamili, Antoni. Wreszcie ją mam!"

## Streszczenie

Neikatis miewa burze piaskowe które z uwagi na strukturę piasku powodują efekty magiczne i po których pojawiają się krótkotrwałe anomalie. Salvagerzy zbierają te anomalie i drenują je, by energię dostarczyć arkologii. Eustachy został przydzielony do salvagera "Seiren", by uczyć się jak wygląda życie w arkologii. Niestety, współwłaścicielka Seiren została porwana przez dowódcę innego salvagera i odjechali w burzę piaskową by ratować JEGO żonę. Seiren jedzie w kierunku na sygnał drugiego salvagera przez burzę, w której odbijają się dziwne głosy i sygnały komunikacyjne. Napotykają ów "Uśmiech Kamili", ale część osób jest martwa i nie ma śladów życia...

## Progresja

* .

## Zasługi

* Eustachy Korkoran: gatlingiem uratował Rufusa przed dużym krystalnikiem; potem wzmocnił Skorpiona częściami Inferni magią i dogadał się z Kidironem by mogli jechać w burzę.
* Zofia d'Seiren: combat / medical / tectonics Seiren; skutecznie ekstraktuje energię z krystalników. Radzi sobie z pilotażem Seiren mimo dziwnych sygnałów i stworów. Autopsja nieżywej Mirki z "Uśmiechu"
* Aniela Myszawcowa: advancer / signal operations Seiren; wykrywa krystalniki i trasuje. Granatem sonicznym zniszczyła dużego krystalnika, ale tydzień była poza akcją. Dekodując dane znalazła sygnał Kornelii.
* Rafał Kidiron: zgodził się, by Eustachy wziął skorpiona (Seiren) i pojechał ratować Annę.
* Kalia Awiter: młoda dama, bardzo zainteresowana Eustachym. Chce się z nim umówić na kolację, ale on stawia ;-).
* Rufus Seiren: posiadacz lancera z pochodnią plazmową; walcząc z krystalnikami stopił dużego potwora. Spanikowany, gdy Antoni porwał Annę i oddał dowodzenie Eustachemu.
* Anna Seiren: posiadaczka mechanicznego ptaka; porwana przez Antoniego by pomogła mu odzyskać i uratować Kamilę.
* Cyprian Kugrak: nie pozwolił Zofii iść ryzykować; ona jest jedynym medykiem. Wie, że jest najmniej potrzebny i bierze groźne role na siebie. Gentle ribbing Eustachego bo Kalia ;-).
* Michał Uszwon: inżynier Seiren. Cytat "zamknij mordę". Nie boi się ryzyka, wraz z Cyprianem blokują ruchy Rufusa (który chce ryzykować bo Anna). Naprawia z Rufusem systemy Uśmiechu Kamili.
* Antoni Grzypf: przyjaciel Rufusa i Anny; porwał Annę by odzyskać swoją ukochaną Kamilę którą kiedyś zostawił gdzieś na jednym ze złóż w tej okolicy.
* JAN Seiren: salvager skorpion Arkologii Nativis; poważnie wzmocniony częściami Inferni przez magię Eustachego, pilotowany przez Rufusa pojechał w Szepczące Wydmy podczas burzy piaskowej...
* JAN Uśmiech Kamili: salvager skorpion Arkologii Nativis; zniszczony przez burzę piaskową na Szepczących Wydmach.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Nativis
                        1. Poziom 2 - Niższy Środkowy
                            1. Wschód
                                1. Centrum Kultury i Rozrywki
                                    1. Bar Śrubka z Masła: ulubiony bar Eustachego; pije sobie z Jankiem Lertysem aż pojawił się Rufus by prosić o pomoc w uratowaniu Anieli.
                    1. Arkologia Nativis, okolice
                        1. Szepczące Wydmy: sektor lodowej pustyni, gdzie udał się "Uśmiech Kamili" w środku burzy. A za nim Seiren. Silne anomalie podczas burzy.

## Czas

* Opóźnienie: 32
* Dni: 2

