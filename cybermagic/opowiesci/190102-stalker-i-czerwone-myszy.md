## Metadane

* title: "Stalker i Czerwone Myszy"
* threads: pieknotka-kaplanka-saitaera
* motives: teren-morderczy, gry-polityczne
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [190101 - Morderczyni jednej plotki](190101-morderczyni-jednej-plotki)

### Chronologiczna

* [190101 - Morderczyni jednej plotki](190101-morderczyni-jednej-plotki)

## Projektowanie sesji

### Pytania sesji

* Czy Adela zachowa wystarczającą ilość 'normalnych' klientów by przetrwać?
* Czy Adela zwróci się w kierunku mafii?
* Czy Adela zaatakuje Pięknotkę bezpośrednio : WIEMY ŻE NIE

### Struktura sesji: Eksperymentalna eksploracja / generacja

* **Scena, Konsekwencja, Niestabilność, Opozycja, Trigger, Pytania**
    * Czerwone Myszy w salonie Pięknotki
    * Pięknotka staje się oficjalnym punktem "to be"
    * CZY Pięknotka chce zmienić profil klientów?
    * init sesji
    * Czy biznes Adeli przetrwa?
        * Czy Czerwone Myszy odejdą do Pięknotki?
        * Czy Adela musi zwrócić się do szemranych elementów?

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. ...

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (10:47)

_Pustogor, gabinet Pięknotki_

W nocy, Pięknotka sobie śpi. Mieszkanie terminusa, ma zabezpieczenia. Wchodzi infiltrator. (TrZ:9,3,5=P). Obudził Pięknotkę i powiedział, że po tym co zrobiła nie jest bezpiecznie spokojnie sobie spać i nie robić nic produktywnego. Wyjaśnił Pięknotce, że jest "lokalnym ubezpieczycielem i reporterem". Pilnuje, by nic złego się nie działo w samym Pustogorze; jeśli Pięknotka jest zainteresowana wynajęciem go, to on z przyjemnością zadba o jej bezpieczeństwo. Zwłaszcza, że Pięknotka zakłada frakcję.

Co?

Pięknotka albo zakłada frakcję albo do jakiejś dołącza. Tak jak jest po tym co zrobiła z Karolem jest zbyt narażona. Pięknotka zdecydowała się upewnić - jeśli go wynajmie, to będzie faktycznie ją chronił (Tp:S). Jak to tajemniczy infiltrator wyjaśnił, on jest zabójcą, nie terminusem. Ale zabijanie to zbyt ciężka robota i zbyt niebezpieczna - dlatego zdecydowanie woli chronić niż zabijać.

Powiedział, że jest reporterem i próbuje dowiedzieć się, czy Pięknotka zakłada swoją frakcję. Pięknotka powiedziała, że nie ma takich planów. Reporter podsumował:

* Pięknotka nie jest chroniona jakby miała frakcję; she is vulnerable
* Pięknotka nie ma intencji działania w ramach frakcji
* Działania jakie Pięknotka zrobiła wcześniej były robione w ramach retaliacji - they hurt me, I hurt them.

Czemu zinfiltrował Pięknotkę? Bo uznał, że to może być wyzwanie. Bo powiedziano mu, że będzie to potencjalnie duży problem. Pięknotka spróbowała zatem go zidentyfikować - o czym pisze, jaki jest jego zakres tematyczny, jako reporter? (Tp: SS). Powiedział jej, wiedząc, że Pięknotka może go dzięki temu zidentyfikować.

* Zajmuje się tematami przestępstw i anomalii magicznych. Działań terminuskich i chroniących. I tych, które trzeba odnaleźć.
* Zajmuje się polityką lokalną. Nie jest na tyle zainteresowany osobami i kto z kim śpi, bardziej implikacjami dla Pustogoru i interesów Pustogoru.

Tajemniczy Przybysz (Jan Kramczuk) dał Pięknotce kupon na zniżkę w salonach obronnych NeoDef, ukłonił się i wyszedł.

_Pustogor, gabinet Pięknotki, rano_

Do Pięknotki przyszła Teresa, wykorzystać swój kupon. Pięknotka chce Teresę ABSOLUTNIE oczarować i absolutnie olśnić (TrZ:10,3,5=SS). Skonfliktowany Sukces. Teresa wygląda jak bóstwo i jest rozanielona. Myszy uznają, że to jest THE place a Pięknotka jest THE person. Piękna, zdolna oraz niebezpieczna. Dokładnie taka osoba, jak chcą. To nie chodzi nawet o to, że jest im potrzebna - pasuje im do profilu. "They want her because she belongs".

Przy okazji, Teresa jest teraz chodzącą reklamą - zadziała i w szkole magów i wśród Myszy w Pustogorze. Czyli się rozniesie.

_Pustogor, gabinet Pięknotki, po południu_

Waldemar odwiedził Pięknotkę. Mag Czerwonych Myszy. Jednocześnie, właściciel firmy zabezpieczeń ochronnych NeoDef. Zaproponował Pięknotce przyłączenie do Myszy i zapewnił następujące korzyści:

* Klienci i wsparcie ze strony Myszy w pozyskiwaniu klientów
* Ochrona i współpraca, m.in. w ramach ochrony terenu (np. Podwiertu, Żarni i okolic)
* Sieci informacyjne Myszy

Pięknotka nie jest zainteresowana dołączeniem, ale próbuje powiedzieć to bardziej dyplomatycznie. Waldemar powiedział jej wyraźnie - gdyby on chciał uderzyć w Pięknotkę, nie uderzałby w nią bezpośrednio w Pustogorze. Uderzyłby w teren, który Pięknotka kontroluje. Zaznaczył, że ON tego nie zrobi i Myszy tego nie zrobią. Ale Pięknotka albo będzie miała wsparcie, albo będzie musiała jakoś sobie z tym poradzić sama.

_Pustogor, Barbakan, Rada Terminusów, wieczór_

Terminus Tadeusz Rupczak jest tym, który zajmuje się sprawą Pięknotki.

Pięknotka uargumentowała chęć odrzucenia wszystkich możliwych terenów tym, że chce skupić się na swoim biznesie. Nie chce mieć własnego tereni. Nie chce mieć terenu, który może zostać zaatakowany przez jej wrogów. Nie chce mieć frakcji. Chce być apolityczna. Chce zostać terminusem lotnym, wykonującym zadania jak najemnik dla Pustogoru - bez możliwości odrzucenia. To jest "terminus Pustogoru".

Rupczak przyjął to do wiadomości. Akceptuje to.

Pięknotka użyła potęgi Barbakanu by znaleźć informacje o swoim tajemniczym infiltratorze. Wykorzystała potęgę Barbakanu - Barbakan wie wszystko co się dzieje w Pustogorze. (Łt: 11,3,1=SS). Infiltrator będzie wiedział, że Pięknotka wykorzystała zasoby Barbakanu by go znaleźć.

Dowiedziała się kto to jest - Jan Kramczuk, miłośnik zakładów wszelkiego rodzaju. Reporter, infiltrator, czasem pracuje dla terminusów Pustogoru. Jest tym wszystkim. Powiązany z Dare Shiver.

Pięknotka wysłała Janowi sygnał, że wie, że to on. On odesłał jej sygnał "disappointed" (z uwagi na metody, nie z uwagi na to, że znalazła). Pięknotka uznała, że chwilowo ona i Jan nie mają ze sobą wspólnego miejsca.

Wpływ:

* Ż: 8
* K: 5

(koniec 12:24)

* Czerwone Myszy mają mały rozłam. Jedna frakcja chce bardzo wzmocnić Adelę - skoro Adela chce być z nimi, ma być najlepszą kosmetyczką w okolicy. Druga frakcja chce iść do Pięknotki mimo wszystko - niech Pięknotka w końcu dołączy do Myszy, więc warto tak to zrobić.
* Czerwone Myszy dostały nowy teren. Kreacjusz Diakon poprosił, że skoro Pięknotka chce się pozbyć jego terenu, to on chce Myszy. Tak to pochlebiło, że się zgodzili.

**Scena**: (21:41)

* Czy Pięknotka kupi Jana Kramczuka? Czy wyprawa na bagna sprawi, że się do niej jednak jakoś przekona?
* Czy trzeba będzie Pięknotkę uratować z bagna? (lokalne)

Pięknotka wzięła zwykłe kosmetyki, dodała do nich "made in Cieniaszczyt" i podniosła cenę. Działa - to jest biznes mający wygenerować jakieś złoto :D. Dowiedziała się też o rozłamie wśród Myszy - część wspiera Adelę, część ją. A ona? Ma zamiar opchnąć jak największą ilość kosmetyków "made in Cieniaszczyt" zanim inne gabinety się zorientują. No i oczywiście Pięknotka chce zmienić zabezpieczenia...

Wyprawa na bagna. Pięknotka zaprosiła Jana, by pokazać mu co i jak - ale ustaliła serię zasad. "I will do what I can to protect you, but at YOUR own risk". Jan się zgodził. Pięknotka wpierw wybrała bezpieczną trasę by się nauczył chodzić po bagnie a dopiero potem weszła w Trzęsawisko.

_Mgliste Trzęsawisko Zjawosztup_

Pięknotka wyjaśniła Janowi, że Trzęsawisko jest dziwne nawet jak na standardy magów. Nawet puryfikatorzy czy kataliści nie dali rady. Podobno kiedyś ktoś próbował. Podobno nadal chodzą po tym Trzęsawisku...

Pięknotka chce pokazać Janowi coś imponującego. Nie pnączoszpona - Toń Pustki. Jest to wystarczająco imponujące a Jan może zobaczyć coś bardzo, bardzo ciekawego. Niektórzy mówią, że Toń Pustki pokazuje prawdę. Zazwyczaj tak robi.

Gdy Pięknotka przemierza bagno, nie rozmawia. Większość skupienia jest na otoczeniu, nie na Janie. (Tp:SS). Jednak mimo wszystko Pięknotka dostrzegła pnączowęże zbliżające się do niej i Jana. Żeby szybko rozwiązać problem, Pięknotka szybko rzuca pod nogi Jana śmierdziel zniechęcający węże i daleko w krzaki coś, co je zachęci. Udało jej się pokazać Janowi, jak węże zmieniają kierunek. Czarodziej uniósł brew. Nie pomyślałby o tym. Pięknotka potwierdziła - próba ataku tych węży nie skończyłaby się dobrze. Zupełnie nie.

By dotrzeć do Toni, trzeba było przedostać się przez trudny akrobatycznie obszar. I to w miarę cicho. Zarówno Jan jak i Pięknotka są w tym dobrzy; dlatego Pięknotka wybrała to konkretne miejsce. I tym razem bez problemu dostali się do Toni.

_Trzęsawisko Zjawosztup, Toń Pustki_

Pięknotka pokazała Janowi Toń i dała linę. Niech spojrzy do środka jeśli chce zaryzykować. Pięknotce w Toni pojawił się Saitaer; coś tam się pokazuje. Toń pokazuje prawdę. Czy Pięknotka może spojrzeć? Nie może zaufać Janowi... czy może?

Pięknotka związała się odpowiednio i przywiązała. Powiedziała, że najpóźniej za minutę Jan ma ją wyjąć. I nie ma za żadne skarby stawać między Pięknotką a Tonią...

Pięknotka otworzyła się na Toń. Toń dotknęła Pięknotkę - próba nawiązania połączenia. (Tr:7,3,6=P). Nawiązała...

Ciemność. Pięknotka znajduje się w jakiejś bazie, wygląda na kosmos. Ale to nie musi być kosmos. I widzi coś niepokojącego - biovat. Nietypowy biovat, pokryty obcą technologią. A w tym biovacie - ciało Saitaera. Ten biovat nie pochodzi z Trzęsawiska - to miejsce to tylko dywersja. Ciało Saitaera jest budowane zupełnie gdzie indziej. Saitaer odezwał się do Pięknotki - jest dumny ze swojej córki i z tego jak ją przebudował. Tak jak jest dumny z Minerwy i tego co zrobił z niej, jak ją przemodelował.

Jan wyjął ją z Toni. Pięknotka czuje obecność Saitaera z tyłu swojej głowy...

Efekt Jana: (Tr:7,3,6=S). Pięknotka wynurzyła go z Toni; Jan jest wyraźnie wstrząśnięty. Spytał, czy Pięknotka często korzysta z tej wyroczni? Praktycznie nigdy... Zdaniem Jana, nikt nie powinien wiedzieć o tym miejscu - to by było dla nich niebezpieczne. Toń wciąga.

Pięknotka czuje obecność boga. Ale nie przeszkadza jej to tak bardzo. Kiedyś byłaby przerażona, teraz... teraz to mniej ważna sprawa. Nawet się cieszy...

By jeszcze zrobić wrażenie, Pięknotka zdecydowała się zrobić jedną akcję - pokazała jak zbiera niektóre, bardzo trudne do zdobycia korzenie. Używa pudełka by wpierw wyłapać pewne niebezpieczne owady. Niestety, podczas tego zbierania część owadów boleśnie pokąsiła Pięknotkę i Jana; szczęśliwie, nie dość by musieli uciekać w panice. Pięknotka spokojnie zauważyła, że za 2h stracą czucie w kończynach więc muszą powoli wracać. Dała coś, co złagodzi efekty, ale muszą przejść przez Szpital.

(Tp:9,3,3=S) i tym razem udało im się wrócić bez większych problemów.

Jan, po wizycie na Trzęsawisku, powiedział Pięknotce wyraźnie, że zmienił swoją opinię na jej temat. Rozumie już, czemu Pięknotka ma takie a nie inne podejście - ma inne obszary, w których szuka ryzyka. Rozstali się w zgodzie i z uśmiechami. Acz - szybko poszli do Szpitala.

Wpływ:

* Ż: 10
* K: 6

**Epilog** (22:30)

* Jan i Pięknotka są na pozytywnej stopie.
* Saitaer podłączył się ponownie do Pięknotki, ale jej to nie przeszkadza
* Pięknotka wie, że ciało Saitaera gdzieś rośnie, ale nie na Trzęsawisku
* Adela i Pięknotka mają obie wsparcie Myszy; obie dalej są w grze

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |         |             |
| Kić           |         |             |

Czyli:

* (K): 
* (Ż): 

## Streszczenie

Myszy napuściły Kramczuka, by ten wlazł nocą do Pięknotki - udało mu się, nie jest zbyt chroniona. Pięknotce spodobał się ów reporter-infiltrator i by się z nim bliżej poznać, przeszła się z nim na Zjawosztup. Tam, Toń pokazała Pięknotce Saitaera i sprzęgła go z nią ponownie. Dodatkowo, Pięknotka pozbyła się wszelkiego terenu a Myszy się podzieliły - część z nich wspiera Adelę i chce, by Adela przerosła Pięknotkę a część wspiera Pięknotkę z nadzieją, że dołączy do Myszy.

## Progresja

* Pięknotka Diakon: pozbyła się jakiegokolwiek terenu - za żaden nie odpowiada. Od tej pory jest terminusem lotnym, wykonującym działania dla Pustogoru.
* Pięknotka Diakon: znowu poczuła głos Saitaera w jej głowie; ale nie jest to złe. Tym razem nic jej z jego strony nie grozi.
* Jan Kramczuk: zobaczył w Toni coś, co dało mu zahaczkę do nowej historii, nieprawdopodobnej. I takiej, co wpędzi go w kłopoty.

## Zasługi

* Pięknotka Diakon: wyplątała się ze wszelkich wymagań terenowych. Potem zaprosiła stalkera (Jana) na Trzęsawisko i spojrzała w Toń, odzyskując kontakt z Saitaerem.
* Jan Kramczuk: infiltrator; wszedł do mieszkania Pięknotki niezauważony i ją obudził by przepytać. Reporter, stowarzyszony z Dare Shiver. Potem łaził z Pięknotką po Trzęsawisku i spojrzał w Toń.
* Teresa Mieralit: przyszła skonsumować darmowy kupon od Pięknotki i wyszła jako megaepicka reklama gabinetu Pięknotki. Pięknotka przeszła samą siebie.
* Waldemar Mózg: zaproponował Pięknotce dołączenie do Myszy i ostrzegł, że przecież Pięknotka ma wrogów w Pustogorze. Dlatego Pięknotka pozbyła się odpowiedzialności.

## Frakcje

* Czerwone Myszy Pustogorskie: są rozdarte między wspieraniem Pięknotki Diakon i Adeli Kirys. Aha, odpowiadają też za teren w Żarni i Podwiertem.
* Dare Shiver: próbowali pozyskać Pięknotkę (i zawiedli), po czym ich agent z Pięknotką wszedł na Trzęsawisko i zobaczył coś za bardzo niebezpiecznego.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Gabinet Pięknotki: który jako dom okazał się zbyt słabo zabezpieczony (Jan tam wlazł)
                                1. Barbakan: miejsce dowodzenia, gdzie Pięknotka wyplątała się z wszelkich wymagań co do terenu
                        1. Trzęsawisko Zjawosztup
                            1. Toń Pustki: która objawiła Pięknotce Saitaera i umożliwiła ich podpięcie

## Czas

* Opóźnienie: 1
* Dni: 3

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* nic

## Wykorzystana mechanika

1811
