## Metadane

* title: "Sekrety Kariatydy"
* threads: legenda-arianny
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [210526 - Morderstwo na Inferni](210526-morderstwo-na-inferni)

### Chronologiczna

* [210526 - Morderstwo na Inferni](210526-morderstwo-na-inferni)

### Plan sesji
#### Co się wydarzyło

.

#### Sukces graczy

* Infernia przetrwa bez szczególnych uszkodzeń
* Infernia dowie się o AKTYWNEJ Kariatydzie i roli Soriana

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Będzie Tivr - można przenieść Elenę. Przeniesiemy Elenę - nie jest bezpośrednio pod Eustachym. No i można pomóc noktianom.

5 lat temu zniknął statek OO Kariatyda. Był to statek mocno powiązany z komodorem Walrondem. Walrond nie chce znaleźć Kariatydy.

Eustachy proponuje - "chitynowy" pancerz dookoła Inferni, tam na pancerzyku podstawowe urządzenia, tam zanomalizowana Persefona (ta ich). Niech myśli że działa, Infernia pociąga za sznurki a Klaudia robi odpowiednio anomalną powłokę.

Roland Sowiński. Uratowany ostatnio z Serenita. Nie wiedział, że Arianna jest prawdziwa. Są KRESKÓWKI na planecie, popularne odnośnie Arianny. I zwiększają to, że Arianna nie jest prawdziwa. Kreskówki to SuperDeformed i maksymalnie przejaskrawione. Jest tam tylko Arianna, Elena (jako emo) i Eustachy (jako superkomandos). Arianna chce wiedzieć KTO to wypuszcza i na jakiej podstawie...

Czy się zgodzi dać Ariannie środki na przebudowę? Arianna przysiadła i opowiedziała o tym że do Anomalii, że to istotna misja... ZMYŚLAM! Niebezpieczna anomalia, nie wiemy jakie ryzyka, potencjalnie ryzyko dla Orbitera...

TrZ+4: 

* X: Tak się napalił, że załatwi, ale jedną misję leci z Infernią.
* X: On będzie Twoim Paladynem. On będzie dbał o Twoją REPUTACJĘ. Biały Rycerz Arianny.
* X: Sekrety Orbitera będą reanimowane. Izabela wraca. On za to wszystko płaci.
* V: Będą środki na podstawowe rzeczy jakie chce zrobić Eustachy i Klaudia.
* X: Sowińscy stwierdzą WTF! CZEMU ONA BIERZE NASZĄ KASĘ! Rozkochała w sobie Rolanda!
* X: Roland stwierdza, że się musi z Arianną ożenić. Sowińscy panikują. Chcą rozbić ten związek. Roland będzie o niego walczył. Zaczyna się ESKALACJA. NIE WIERZĄ JEJ!!!

Sowińscy zapewnili mnóstwo świetnego sprzętu, przedmiotów, anomalii... cokolwiek trzeba. Eustachy i Klaudia widzą, że ten sprzęt... cóż, Sowińskich ktoś oszukał. Wyraźnie brakuje im kompetencji w określeniu co jest wartościowe a co nie. Oczywiście, Roland oraz Sowińscy wierzą że to najlepszy sprzęt jest...

Eustachy chce zapewnić dobrej klasy, porządny statek do Anomalii Kolapsu. Tym co ma... (ExZ+3). Ma wprawę, niedawno maskował Infernię jako Goldariona. No i kadrę inżynierską. Podwładni, inżynierowie i sprzęt.

ExZ+3:

* X: materiały były gorsze niż się wydawało. Maskowanie działa, ale jest bardziej kosztowne; nawet Sowińscy zauważyli jakąś akcję.
* X: lepiej, byście przyszli z jakimś sukcesem... bo inaczej będzie "zmarnotrawiony sprzęt Sowińskich"
* X: informacja o tym, że lecicie do Anomalii Kolapsu zająć się cholerną Kariatydą została rozprzestrzeniona. Sowińscy + Verlenowie rękoma Arianny lecą ratować Kariatydę.
* O: ważny statek Orbitera zaginął. Plotka, że dama Sowińskich jest tym zainteresowana. Inne arystokratki skupiły się by ta dama nie była jedyną. Dyskretna akcja stała się głośna i niesamowicie efektowna. ARYSTOKRATKI FINANSUJĄ TĄ MISJĘ!!! Uratujcie Kariatydę dla nich!

Teraz to sprawa PRESTIŻOWA.

Izabela dotarła do Inferni. Zostanie na dłużej. 100% promienności.

Inspektor Galwarn. Kazał Ariannie zostawić Kariatydę w spokoju. Statek jest pod konspirą. Arianna zauważyła, że nie jest pod konspirą bo Orbiter jest bardziej kompetentny. Plus arystokracja jest bardzo zainteresowana teraz tym tematem - nie można tego tak zostawić. Galwarn jest podłamany.

Arianna powiedziała, że może spróbować uspokoić sytuację. Ale chce Tivr. Można powiedzieć, że Kariatyda została zniszczona. Galwarn powiedział, że NIE MOŻNA tego powiedzieć - Kariatyda to błąd Orbitera. To może wywołać hasło sabotażu lub coś tego typu. Arianna zaproponowała, że ona wyciszy sprawę - to będzie "inny statek". Np. "Aria XXX" i wszyscy źle usłyszeli.

Ofc Zespół musi wiedzieć o co chodzi XD. Kogo lepiej spytać niż nieco nawiedzonego Mariana Tosena? (Tr+2 -> Z)

Tosen opowiedział o Kariatydzie - powiązane z ekosystemem TAI na K1. Ani jednej osoby na pokładzie Kariatydy. Jest to sterowane przez specjalne TAI i ta TAI porwała Kariatydę. Klaudia powiązała to z Rziezą - czyli Kariatyda została "porwana" przez TAI które uciekały przed Rziezą 5 lat temu...

* cel 1: "co się stało z Kariatydą i ekosystemem TAI"
* cel 2: "jak ukryć fakt istnienia Kariatydy"

W jaki sposób pozbyć się informacji o Kariatydzie:

1. Orbiter robi konkurs "wyślij KARIATYDA" na numer 999 99 92 i wygrasz przejażdżkę statkiem
2. Orbiter zmienia nazwię jakiegoś luksusowego statku na Kariatyda
3. SEKRETY KARIATYDY - HISTORIA PRAWDZIWA. Kreskówka.

To skutecznie zaszumi sygnał. Ale to znaczy, że Infernia uczestniczyła w kampanii reklamowej. Infernia jest poszkodowana. Jest OFIARĄ. OFIARĄ PODŁEJ KAMPANII REKLAMOWEJ. Dostali informację, że statek potrzebuje pomocy - czemu mieli kwestionować.

ExZ+3:

* V: Inferni i Arianny nie spotkała krzywda że się sprzedali. Najwyżej że są naiwni.
* X: Duże niezadowolenie na to, że kontrywiad Orbitera nie zadziałał - uczciwi żołnierze zostali wkręceni. Nastroje anty-Luxuritias.
* X: Część Aurum na K1 chce współpracować prawie tylko z Arianną i Infernią. W końcu skoro jest zaręczona z Rolandem...
* V: Orbiter znalazł WAŻNĄ MISJĘ W ANOMALII dla Arianny by pokazać arystokratom że to wszystko miało znaczenie. I nie ma zamieszek ani problemów.

## Streszczenie

Arianna chce Tivr do swojej floty. Do tego celu zdecydowała się poznać sekrety Kariatydy - czemu Walrond tak unika tego statku i nazwy? Niestety, Arianna i Eustachy rozognili opowieść o "Orbiterze, który porzucił swoich w Anomalii Kolapsu" a Arianna dowiedziała się dyskretnie, że OO Kariatyda to specjalny statek - nie można powiedzieć, że był zniszczony. Ten statek został porwany przez TAI i uciekł do Anomalii Kolapsu. Arianna, promienna mistrzyni PR, zmieniła to w "KONKURS. Wyślij KARIATYDA i wygrasz przejażdżkę luksusowym statkiem". Aha, Roland Sowiński (uratowany przez Ariannę z Odłamka Serenita) chce się z nią ożenić.

## Progresja

* Arianna Verlen: chce się z nią ożenić Roland Sowiński. Sowińscy chcą zniszczyć ten związek. Ona próbuje nie być w tym związku XD.
* Arianna Verlen: ona i Infernia są naiwne - dali się wykorzystać w chytrej reklamie Luxuritias.
* Arianna Verlen: część Aurum na K1 chce współpracować tylko z nią. Bo nie zostawia swoich w potrzebie. I bo Roland chce się z nią ożenić.
* OO Infernia: opancerzona "chityną" i z zewnętrzną Persefoną. Eustachy uczynił cuda tym, co miał dzięki Aurum.

### Frakcji

* .

## Zasługi

* Arianna Verlen: by zdobyć Tivr odnalazła OO Kariatyda który zaginął w Anomalii Kolapsu. Chce wyruszyć na jego poszukiwanie, ale wpakowała się w intrygę ekosystemu TAI na K1. Zaszumiła konkursem "luksusowy statek Luxuritias".
* Eustachy Korkoran: by zdobyć pieniądze i środki na wyprawę do Anomalii Kolapsu przy marnych środkach z Aurum rozpalił arystokratki opowieścią o Kariatydzie.
* Roland Sowiński: oczarowany przez Ariannę i wdzięczny jej za uratowanie z Odłamka Serenita zdecydował się z nią zaręczyć. Ale sfinansował jej wyprawę do Anomalii Kolapsu. Kiepskim sprzętem...
* Rafael Galwarn: inspektor Orbitera, siły specjalne K1. Ukrywał informacje o Kariatydzie aż Arianna i Eustachy weszli mu w szkodę. Zaakceptował pomysł "konkursu i reklamy" i użył środków K1 by prawda nie wyszła na jaw.
* Marian Tosen: nie jest może ekspertem od Anomalii Kolapsu, ale słyszał o fiasku Kariatydy. Opowiedział o tym Ariannie.
* OO Kariatyda: całkowicie autonomiczny Okręt Orbitera, który 6 lat temu został porwany przez TAI i uciekł do Anomalii Kolapsu. By nie było kłopotów, K1 ukrywa informację że Kariatyda kiedykolwiek istniała.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy

## Czas

* Opóźnienie: 4
* Dni: 3

## Inne

.

## Konflikty

* 1 - Arianna przekonuje Rolanda, by ten sfinansował jej wyprawę po Kariatydę. W końcu go uratowała
    * TrZ+4
    * XXXVXX: Roland się w niej zakochuje i chce się ożenić. Sowińscy chcą rozerwać ten związek. Sekrety Orbitera wracają.
* 2 - Eustachy próbuje zrobić COKOLWIEK z tych materiałów od Sowińskiego z Aurum. Infernia potrzebuje sprzętu. (Inżynieria i dowodzenie).
    * ExZ+3
    * XXXO: Sprawa stała się mocno prestiżowa. Nie można marnować sprzętu Sowińskich. Plus, frenzy arystokratek chcących ratować Kariatydę.
* 3 - Arianna próbuje poznać prawdę o Kariatydzie od Mariana Tosena - prośba o pomoc i informacje.
    * Tr+2
    * VV: informacja o przeszłości Kariatydy na poziomie o jakim Marian wie
* 4 - Arianna i Galwarn robią potężną dezinformację - "Kariatyda" to nowy statek luksusowy, nie jakaś inna stara jednostka. Arianna chce też reputację ratować.
    * ExZ+3
    * VXXV: Infernia i Arianna uznani za naiwnych, użyci przez reklamę. Część Aurum chce pracować tylko z nimi, nie z K1. No i Orbiter znalazł WAŻNĄ MISJĘ W ANOMALII dla Inferni, by udobruchać.
