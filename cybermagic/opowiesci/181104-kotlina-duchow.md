## Metadane

* title: "Kotlina Duchów"
* threads: olga-regentka-czarnopalca
* motives: efemeryda-agresywna, przesladowany-wyrzutek, straznik-skazony, wezel-zrodlo-skazenia, vicinius-pozyteczny, perfect-vision, teren-spawner
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [181101 - Wojna o uczciwe półfinały](181101-wojna-o-uczciwe-polfinaly)

### Chronologiczna

* [181101 - Wojna o uczciwe półfinały](181101-wojna-o-uczciwe-polfinaly)

## Projektowanie sesji

### Struktura sesji: Bossfight

* Obserwowalność
    * Przyczyny: gromadząca się energia pod Żarnią która się przesunęła odradzając echo Czarnopalca w Kotlinie
    * Wyzwalacz: nagromadzenie energii magicznej i Skażenie lokalnego terminusa, Wawrzyna Towarzowskiego
    * Sprawca: brak; Szczelina
    * Zjawisko: pojawiające się w okolicy "duchy"
* Boss
    * Czym jest: wir energii zaczepiony w przeszłości
    * Sukces bossa: przeszłość reanimowana w Kotlinie
    * Porażka bossa: energia rozładowana
    * Siły: corruption, powoływanie echa przeszłości, swarm, kineza
    * Słabości: byt energetyczny, pryzmat, glukszwajny
* Dark Future
    * Glukszwajny eksplodują i pouciekają ze strachu
    * W Kotlinie Mikarajły dojdzie do ukonstytuowania się Armii Duchów
    * Wawrzyn ewoluuje w viciniusa pod kontrolą Węzła
* Player Devastation (1k3)
    * 4: Skażenie magiczne czarodziejek
    * 8: Skażenie i rany; coraz gorzej się walczy
    * 12: Ciężko ranne czarodziejki
    * 16: Portal do innego Świata ("wróć do domu") + Pastmaster
* Długość toru kupowania czasu:
    * 5: wsparcie Olgi
    * 10: wsparcie Epirjona
    * 15: wsparcie Pustogoru
* Trigger
    * Z Grubej Rury
    * Jakaś technomantyczna sprawa w okolicy Trzęsawiska z którym Pięknotka zupełnie sobie nie radzi (faza 1)

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) 
* Ż: (element niepasujący) 
* Ż: (przeszłość, kontekst) 

### Dark Future

1. Wewnątrz bossfighta

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

* Pięknotka chce zmienić teren i musi się tam dostać - najlepiej ze współpracy innego maga lokalnego
* Olga desperacko próbuje zrzucić z siebie jakichś fanatyków z Komitetu Obrony Szczelińca i magów terminusów

## Misja właściwa

**Scena**: Walka z technomantycznym bossem (22:39)

Pięknotka znalazła się w okolicach Cyberszkoły, schowana za budynkiem; śledzi (sama) energetyczną anomalię zbliżającą się do ognisk cywilów. Jej umiejętności są żałośnie kiepskie w walce z niematerialnym bytem energetycznym... Minerwa jest już w drodze, ale musi dotrzeć. Przeciwnik jest na korzystnym dla siebie terenie plus dookoła są ludzie. Pięknotka jest odpowiedzialna za okolicę...

Energetyczny byt technomantyczny... jak ściągnąć uwagę? Pięknotka się szybko rozejrzała - może może coś zrobić tym co jest dookoła. Pięknotka widzi dwie opcje - zwrócić uwagę tego na siebie lub ochraniać cywili. To wygląda jak kula energii. Pięknotka strzeliła w to z błyskawicy z nadzieją by to przekierowało się na nią. (Ł). Sukces - kula energii została trafiona błyskawicą i zwróciła się w kierunku Pięknotki.

Kula energii przekształciła się w takiego energetycznego pająka i wystrzeliła w Pięknotkę. Ta natychmiast odbiła swoim zaklęciem - uziemić to cholerstwo. Chce uziemić samą KULĘ energii błyskawicami. Drenaż. (Magiczny Trudny test: 9-3-6=S). Potężna rana w pajęczaka (4).

Pajęczak zaczął skupiać uwagę na jadącym samochodzie. Pięknotka otoczyła go elektrycznym pastuchem - rzuciła coś, co uniemożliwi mu ewakuację, coś, co go utrzyma. Zasób. (Tp:10-3-3=S). Zasób zadziałał; pajęczak nie ma jak się wycofać. Jest w miarę zablokowany na tym obszarze i uszkodzony. Dobre wieści.

Pająk wyemitował wiązkę nova, łącząc się z siecią. Pięknotka skupiła się by to złapać; zraniło ją to jednak. Nie może sobie pozwolić na to, by to cholerstwo uciekło. W finale zdecydowała się na rzucenie potężnego czaru dewastującego (elektryczność). (MTp:10-3-3=SS). Potężne uderzenie w pająka, potwór został zniszczony; force-feedback blackoutował jednak nieszczęsną Pięknotkę.

Minerwa ją znalazła leżącą na ziemi, nieprzytomną i ranną.

* Mikrotor zniszczenia bossa: trwałość: 10/10
* Mikrotor: wsparcie: 5/10 (1k3/3 silnik)
* Tor Rany Pięknotki: 5/10 (1k3/3 silnik)
* Akcje (nie Wpływ): 6

Wpływ:

* Żółw: 0
* Kić: 0

**Scena**: Interludium w Pustogorze (23:01)

W Barbakanie w Pustogorze, Tymon i Pięknotka. Tymon martwi się tym, że Trzęsawisko spawnowało nową efemerydę w formie takiej kulki. Pięknotka się złości, że jej zakres umiejętności zupełnie nie pasują do tego typu terenu... Tymon przyznał, że Pięknotka nie za ten teren powinna odpowiadać. Po prostu tu nie pasuje. No i ma dojazd; nie chce dojeżdżać, ma gabinet na głowie...

Pięknotka jest nadal lekko ranna - utrzymuje się do końca sesji.

Z powrotem w gabinecie Pięknotki - Marlena na nią czeka. Stwierdziła, że się odwdzięczy - znalazła (rękami Kiryła) dla Pięknotki teren lepszy niż Zaczęstwo. To okolice Żarni i Czarnopalca. Mieszka tam jedna czarodziejka, niechętna innym magom i ludziom. Powinno być mniej kłopotów. Pięknotka podziękowała. Faktycznie tamte okolice wyglądają na lepsze. Marlena powiedziała, że dowie się jak terminus może najlepiej tam pomóc. Pięknotka podziękowała i powiedziała, że docenia - ale pojedzie sama.

I faktycznie, pojechała. (23:12)

**Scena**: Czarnopalec, Olga vs ludzie (12:17)

* Olga Myszeczka, która nie radzi sobie z komitetem
* Policja, która po raz kolejny ją chce przesłuchać

Pięknotka pojechała więc do Czarnopalca. Dostała informacje ze stacji Epirjon, bezpośrednio od Kiryła. Powiedział, że terminus Wawrzyn jest naleśnikiem i zgodzi się na pomoc Pięknotki - a Olga w Czarnopalcu powinna się zgodzić. Kirył jak co, jest dostępny na Epirjonie.

Czarnopalec. Olga. Pięknotka podjeżdża a tam... samochód policyjny. I gorathaul na drzewie, obserwujący budynek z ostrożnością.

Po komunikacji przez hipernet, Olga powiedziała Pięknotce, że policja chce ją zabrać i przesłuchać. Znowu. Chodzi o to, że oskarżają ją o bycie wiedźmą ORAZ o to, że jest hackerem i hackuje coś po internetach. Ona nawet nie rozumie co się dzieje a viciniusy są problematyczne jak zostają same. A zdaniem Olgi lokalny terminus jest bezużyteczny - ma ją chronić i ratować przed takimi problemami!

Pięknotka wchodzi do domu Olgi. Tam siedzi podłamana Olga i oficer policji Adrian Wężomór. Kwadratowa szczęka. Oficer się zdziwił z obecności Pięknotki - wylegitymował ją i wyszło, że Pięknotka jest z obrony terytorialnej. Ucieszył się. Policjant nie chce robić Oldze kłopotów ani jej nękać; ale musi robić swoją pracę. Pięknotka wypracowała rozwiązanie które sprawia, że Olga nie musi stąd się ruszać. (Tp: 10-3-3: P). Policjant się zgodził - on pojedzie i załatwi. (ale Oldzie poszło w szkodę i to solidnie - gorathaul jest winny; plus, gorathaul zaatakuje).

Pięknotka powiedziała Oldze, że chce przejąć ten teren i Olga od ręki się zgodziła. Wawrzyn naleśnior. Miłą dyskusję przerwała im... mgła. Olga natychmiast wypadła i zatrzymała gorathaula przed próbą teleportacji. Gorathaul próbował coś zaatakować, coś daleko - wymagającego mgły i teleportacji. Pięknotka oczekuje wyjaśnień od Olgi. Gorathaul nadal popatruje w tamtą stronę...

Śledzie i gorathaul wyczuwają energię magiczną. Energia jest gdzieś _tam_. I to musi być jakaś energia magiczna która zachęca tej klasy viciniusy by tam poszły. Co się dzieje?

Pięknotka powiedziała Oldze, że ona musi poczekać na swojego policjanta. Ona się dowie co tu się dzieje. I poszła.

**Scena**: Czarnopalec, okolice Kotliny. Pre-bossfight. (12:46)

Pięknotka zbliżyła się do Kotliny. Wyczuwa energię magiczną z tamtego kierunku. Czego Kirył jej nie powiedział? Kirył odpalił wzmocnione czujniki Epirjona - przebił się przez ekran ochronny i znalazł Pięknotkę; widzi, że tam jest coś nie tak, ale nie wie co - teren jest ekranowany przez Epirjonem. Pięknotce powiedział że jest tam inny mag, ale ma zakłócenia.

Pięknotka się przestealthowała by zobaczyć co się dzieje. I dała radę podejść Wawrzyna, terminusa. Skutecznie się chowając przeszła przez drzewka do niewielkiej kotlinki. Zobaczyła tam maga - lokalnego terminusa, czyli Wawrzyna. Wawrzyn zabił śledzia syberyjskiego - poświęcił go - wobec Kotliny. Kotlinie zmienił się poziom energii. Jako, że Pięknotka widzi tu teleportacyjną pułapkę na gorathaula, gorathaul byłby następny.

Nieźle - Wawrzyn robi coś naprawdę złego. Najpewniej to on ukrył dane przed Epirjonem.

Pięknotka chce w takiej sytuacji rozwalić Wawrzyna. Jest co najmniej częściowo skorumpowany. Pięknotka kombinuje jak weń walnąć błyskawicą...

Wawrzyn podniósł rękę i aportował starszego człowieka. "Krew przeszłości...". Pięknotka strzeliła błyskawicą. Chce kupić sobie chwilę i uratować człowieka. (MTr:9-3-5=S). Błyskawica trafiła w Wawrzyna który wpadł do Kotliny. Człowieka teleportował Kirył gdzieś... indziej. Prosto do Olgi.

Pięnotka odpaliła natychmiast sygnał alarmowy do wszystkich terminusów.

Kotlina się obudziła.

**Scena**: Czarnopalec, Kotlina Mikarajły; boss (13:05)

Pięknotka nie pozwoliła Oldze się tu pojawić bo straci swoje viciniusy. Kotlina uderzyła. Kotlina zbudowała leyline'y w kierunku na wszystkie centralne okoliczne ośrodki z ludźmi i zaczęła pompować duchy i upiory. Let the past become the present. Efemeryda.

Pięknotka skupiła siły i walnęła w efemerydę - "teraz jest pięknie, nie wracajmy", kontr-energia efemerydy, mentalna. (MTr:10-3-5=MS). Wzmocniony efekt Skażenia - ZARÓWNO devastation JAK I efekt poszedł do przodu (3,5). Pięknotka POŁĄCZYŁA się z efemerydą. Zatrzymała ją i kupiła jej czas... aż glukszwajn zapobiegł Skażeniu Pięknotki.

Olga i Pięknotka stoją przeciwko Efemerydzie. Wawrzyn wydostał się z kotliny z armią upiorów. Pięknotka musi się wyplątać ze splątania Efemerydy; jest utopiona w efemerydzie mimo Glukszwajna (Tp:10-3-3): Magiczne Skażenie. Sukces, ale Skażenie dotknęło też Olgi. Czarodziejki się już nie pieprzą - zdjęły limitery i używają pełni mocy.

Pięknotka uderza w Wawrzyna w momencie w którym Gorathaul weń uderza. (Kić: -1 Wpływ: Wawrzyn miał drill instructora który NAPRAWDĘ był chujem). Pięknotka celuje w to, by go tak pojechać ostro, by go trochę otrząsnąć; by go osłabić. (MTr+3:8-3-6:MS). Pięknotka dała radę Wawrzyna trochę wyprowadzić; (-5 Wpływ) do tego stopnia, że gorathaul dokonał egzekucji Wawrzyna; wzmacniając energię Kotliny po raz kolejny - martwym magiem.

Olga wpadła w panikę. Glukszwajny wpierniczają co się da. Gorathaul spożywa zwłoki. Pięknotka patrzy "wtf". Krew maga zawirowała w Kotlinie i wśród niezliczonej armii duchów Pięknotka zobaczyła też puste oczy Wawrzyna. Atak - terror - przeciwko Pięknotce. Ona kontruje to swoim "happy fluffy". Mimo przeważającej mocy Kotliny, Pięknotka przesunęła Pryzmat; dlatego tylko MTr. (MTr:10-3-5=MS).

Pięknotka, nieświadomie dla siebie, użyła Magii Krwi. This shall NOT expand. Nie będzie nowego Trzęsawiska. "Terrorform, appear". "Ateno, pomóż mi". "Erwinie, potrzebuję Cię". Echa przeszłości Pięknotki pojawiły się walczyć z przeszłością Kotliny. (+4 tor dewastacji).

Epirjon otworzył ogień z orbity. Pięknotka nawet nie wiedziała, że Epirjon jest uzbrojony!!! Kirył krzyknął, że wsparcie z Pustogoru jest niedaleko. Contain it!

Pięknotka wie, jakie siły ma "Atena". "Terrorform" rzucił się dewastować Kotlinę i rozpraszać energię; on zginie na pewno. Pięknotka szuka hunter-killer spell - niech "Atena" to znajdzie co trzyma Efemerydę do kupy, "Terrorform" niech odsłoni to jądro a "Erwin" niech w to uderzy jak w Szczelinę. Pięknotka się skupia by pokierować swoimi Echami - nie ma już siły na nic więcej. (Tr:8-3-6=SS).

Ku przerażeniu Pięknotki moc "Ateny" okazała się mocą PRAWDZIWEJ Ateny. Pojawiła się "druga" stacja Epirjon. Ta z koszmarów Ateny. I ta stacja weszła w bezpośrednią walkę z Kotliną. Dwie wersje przeszłości zaczęły się zwalczać. Glukszwajny w panice pouciekały, jeden eksplodował blisko Olgi; czarodziejka MUSI do szpitala. Pięknotka jest sprzężona z tym wszystkim co się dzieje i jest przerażona - bo zaczerpnęła energię od PRAWDZIWEJ Ateny, która jest w szpitalu i nie powinna...

Szczęśliwie, pojawiło się wsparcie terminusów z Pustogora. Ta Kotlina już nie stanie się Trzęsawiskiem.

* Player Devastation (1k3): 14
    * 4: Skażenie magiczne czarodziejek
    * 8: Skażenie i rany; coraz gorzej się walczy
    * 12: Ciężko ranne czarodziejki + poddające się glukszwajny
    * 16: Portal do innego Świata ("wróć do domu") + Pastmaster
* Długość toru kupowania czasu: 13
    * 5: wsparcie Olgi
    * 10: wsparcie Epirjona
    * 15: wsparcie Pustogoru

Wpływ:

* Żółw: 0 (10)
* Kić: 3 (4)

**Epilog**: (13:53)

* Olga trafia do szpitala. Ktoś się musi zająć viciniusami - problem Pustogoru.
* Pięknotka trafia na 3 dni do szpitala.
* Atena ma przedłużenie o tydzień w szpitalu, ku swemu wielkiemu niezadowoleniu (ale rozumie)
* Kotlina została dezaktywowana
* Wawrzyn powróci jako upiór w jakiejś formie (-5 wpływu)

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |   0     |     10      |
| Kić           |   5     |      6      |

Czyli:

* (K): Pięknotka obejmuje teren Podwiert-Żarnia-Czarnopalec i oddaje Zaczęstwo (1)
* (K): -

## Streszczenie

Pięknotka ma dość Zaczęstwa - serio ten teren jej nie pasuje. Po pokonaniu energoforma Marlena zaproponowała jej zmianę regionu na Czarnopalec. Kirył z Epirjona jej pomagał znaleźć tą lokalizację; okazało się, że tamtejszy terminus jest Skażony i służy efemerydzie. Pięknotka z Olgą bohatersko wstrzymały efemerydę aż pojawiło się wsparcie z Pustogoru. Kirył został bohaterem a Pięknotka dostała gwarancję, że tamten teren jest jej (a przynajmniej że Zaczęstwo nie jest jej).

## Progresja

* Pięknotka Diakon: nie odpowiada już za Zaczęstwo. Za to odpowiada za Czarnopalec, Podwiert oraz Żarnię (i okolice)
* Pięknotka Diakon: trzy dni w szpitalu - detoks i odkażanie po sprawach z Efemerydą Kotliny Mikarajły
* Olga Myszeczka: tydzień w szpitalu - detoks i odkażanie po sprawach z Efemerydą Kotliny Mikarajły
* Atena Sowińska: jeszcze tydzień musi poleżeć w szpitalu, bo Pięknotka wykorzystała jej energię podczas działań związanych z Krwią

### Frakcji

* 

## Zasługi

* Pięknotka Diakon: chcąc się wyplątać z Zaczęstwa wpadła w świeżo formującą się Efemerydę Kotliny niezauważalną dla Epirjonu. Kupiła z Olgą czas by terminusi uratowali sytuację.
* Marlena Maja Leszczyńska: wdzięczna Pięknotce za pomoc Zaczęstwiakom pomogła jej znaleźć lepszy teren niż Zaczęstwo. Znalazła jej Podwiert i skontaktowała z Kiryłem.
* Kirył Najłalmin: naleśnik okazał się wybitnym ekspertem od teleportacji na stacji Epirjon. Został bohaterem pomagając Pięknotce osłonić się przed Efemerydą Kotliny.
* Olga Myszeczka: straumatyzowana ludźmi hodowczyni viciniusów; z pomocą Pięknotki wyplątała się z posterunku i przyszła jej z pomocą walczyć z Efemerydą Kotliny. Skończyła w szpitalu.
* Adrian Wężomór, policjant w okolicach Czarnopalca (z Podwiertu); Kwadratowa szczęka, ale nie chce Oldze dokuczać. Olgę nękają ludzie a on musi to rozwiązać - na jej niekorzyść.
* Wawrzyn Towarzowski: terminus w okolicach Czarnopalca. Skażony przez efemerydę, został zagryziony przez gorathaula gdy Pięknotka wprowadziła go w trans zapomnienia. KIA.

## Plany

* 

### Frakcji

* 

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Barbakan: miejsce poważnych dyskusji Pięknotki i innych terminusów (m.in. Tymona).
                            1. Zaczęstwo: miejsce wesołe, przebojowe i lekko anarchistyczne. Kolor: technomancja, magia, niedaleko Trzęsawiska. Zupełnie nie pasuje do Pięknotki :D
                                1. Cyberszkoła: gdzie w okolicy doszło do manifestacji bytu energetycznego i Pięknotka musiała to rozmontować...
                            1. Czarnopalec
                                1. Kotlina Mikarajły: znajduje się niedaleko Czarnopalca; próbowała przekształcić się w stabilną efemerydę, ale terminusi pustogorscy to zatrzymali.
                                1. Pusta Wieś: mieszka tam tylko Olga ze swoimi viciniusami. Czasem odwiedza to miejsce policja, bo Olgi to wyjątkowo nie lubią.
                            1. Podwiert: miasto kontrolujące m.in. Czarnopalec i Żarnię - można powiedzieć, powiatowe
                                1. Komenda policji: stąd pochodzi Adrian Wężomór i pod tą komendę podlega Olga Myszeczka

## Czas

* Opóźnienie: 3
* Dni: 2

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* Start z Grubej Rury 3
* Test Bossfight (Barbatos Last Stand)
* cel MG: nadawać negatywne aspekty postaci Gracza przez mikrokonflikty

## Wykorzystana mechanika

1810
