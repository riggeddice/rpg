## Metadane

* title: "Verlenka na grzybkach"
* threads: rekiny-a-akademia
* gm: żółw
* players: anadia, kić

## Kontynuacja
### Kampanijna

* [210615 - Skradziony kot Olgi](210615-skradziony-kot-olgi)

### Chronologiczna

* [210615 - Skradziony kot Olgi](210615-skradziony-kot-olgi)

### Plan sesji
#### Co się wydarzyło

.

#### Sukces graczy

* .

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

* Q: Projekt AMZ na Nieużytkach Staszka. Realizowany przez studentów. Coś na zaliczenie, coś przydatnego. Coś, w czym uczestniczy Julia. Co to jest?
* A: Robot rewitalizujący teren. Wyrywa chwasty, pieli grządki, podgrzybia je...
* Q: Jak może przydać się w tego typu projekcie Liliana?
* A: Niech robot ładnie wygląda, niech nie wygląda PRZERAŻAJĄCO. Plus, tworzy dokumentację techniczną. Plus, szuka informacje i przenosi ciężkie rzeczy.

Zespół: Julia, Ignacy, Triana, Liliana.

Projekt toczy się dobrze. Testują pierwszą wersję robota na Nieużytkach. Nagle - coś ODERWAŁO RĘKĘ robotowi. Jakby coś odstrzeliło. Pierwsza myśl - "to może miało być w nas". Potem - Teresa Mieralit. Druga myśl - to nie w jej stylu. Julia przewraca robota za drzewo i sama się chowa.

Liliana zaczęła składać iluzję PLUS przesunięcie głazu. Niech przeciwnik nie ma pojęcia o tym, w co strzelać i blokuje kamień.

TrM+2:

* Om: FILMOWY PAS ASTEROIDÓW. Wiruje i lata dookoła. Plus, dziesiątki Lilian, Julii, Ignacych i Trian, wszyscy GADAJĄ...

Julia wzywa wsparcie lokalnego terminusa. Niedługo pojawi się terminus. Kolejny pocisk strzaskał jeden z głazów. Julia zauważyła, że przeciwnik umie rozpoznać który robot jest prawdziwy. Ale nie celuje w ludzi. Liliana robi WSZYSTKO by utrzymać zaklęcie.

Julia próbuje zobaczyć skąd lecą pociski. Jakakolwiek forma namiaru. Teraz Julia chce użyć magii - jakakolwiek wiedza pod kątem triangulacji. Julia wie jak wygląda broń na ścigaczach. Julia chce zobaczyć w promieniu 2 km efekty echa w wyniku broni. Julia zna robota jak wierzch własnej broni. Ma dostęp do pocisku WIĘC może użyć sympatii.

TrM+2:

* V: Julia ma namiar. To nie jest ścigacz. To Lancer uzbrojony w gaussowy railgun.

Julia decyduje się przekształcić na szybko robota. Odwracacz kinetyczny. Co leci w robota, odwraca się do źródła. Julia wykorzystuje źródło zasilania robota by to osiągnąć (ExZM+2+2O):

* V: mamy odchylenie. Pocisk ominie robota
* X: uszkodzenie robota
* O: partial success, ale kosztem reaktora
* V: pełen deflektor
* X: projekt zrujnowany...
* V: uszkodzony Lancer w zauważalny sposób

Pocisk uszkodził robota i zepsuł reaktor. Po chwili, pocisk się odwrócił i wrócił do nadawcy. Nadawca był w trybie uników. Jednak nie da się uniknąć railguna. Samo w sobie daje to Julii ciekawą informację - TAI Elainka jest wyłączona. Pilot wyraźnie nie jest szczególnie też dopasowany do Lancera - nie był w stanie uniknąć / opanować sprawy. 

Lancer został uszkodzony, m.in. rozsypał się railgun. Lancer się wycofuje.

Niecałe 2 min później pojawiła się Urszula (Miłkowicz) z ramienia terminusów.

* "Czy to KOLEJNY dowcip czy serio wzywałaś pomocy?" - Urszula, nieufna jak cholera

Julia pokazała resztki robota. Pociski. Urszula nieufnie, ale obiecała że się tym zajmie.
Zdaniem Liliany za wszystkim stoi Zygmunt Zając. Ta organizacja na pewno ma z nimi na pieńku...

Zespół powrócił do pracy nad robotem w trybie przyspieszonym... a Julia wyjaśniła Urszuli jak Lancer został uszkodzony i wszelkie szczegóły.

Niecały dzień później.

Marysia. Jesteś w Podwiercie, w hotelu, imprezuje sobie spokojnie i wpada PRZYDUPAS. Rafał Torszecki (cel życiowy: być najlepszym przydupasem Marysi na świecie?). Przynosi WIADOMOŚCI.

Terminusi aresztowali Marka Samszara. Za atak Lancerem na grupę studentów AMZ. Znaleźli go w lesie w Lancerze. Strzelał do terminusów. Ta, Urszula, wjechała mu jak walkiria z gorącym nożem w masło... (kilka Rekinów z uznaniem "Samszar się kurde postawił", "ale że terminusom O_O") acz sporo jest zaszokowanych. Samszar nie ma w sobie cienia agresji. Nie jest też mściwy. Ale strzelał do terminusa XD.

Marysia żegna drinka (za nią drepcze Rafał) i pyta Julii czy wszystko w porządku. Tak, ale projekt od nowa... wymieniły się informacjami.

Marysia idzie odwiedzić Samszara w ARESZCIE w Barbakanie Pustogoru. Kazała Torszeckiemu dowiedzieć się czemu ten debil (Samszar) atakował terminusów. To bez sensu. Pijany czy coś? XD

Areszt.

Samszar siedzi w areszcie z miną Wallenroda Narodu. Zgnębiony, ale spokojny. Na widok SOWIŃSKIEJ wstał i się ukłonił. Sprzedał Marysi tanią historyjkę jak to próbuje odzyskać Arkadię i dlatego strzelał do terminusów by się nie poddać bez walki, plus chciał pomścić swoją porażkę z Arkadią i ostrzelał studentów AMZ. Nie ostrzelał Marysi bo... no bez przesady :D.

Marysia próbuje złamać Marka Samszara. Kto za tym stoi? Dlaczego? (ExZ+2) (Marysia jako dobroduszny tyran, zirytowany bo drinka nie mógł dopić XD)

* V: Czy to on tam był? Czemu się nie martwi? Wyznał:
    * Arkadii COŚ BYŁO NIE TAK. Była psychotyczna. Marek podejrzewa otrucie czy coś.
    * Marek zaproponował Arkadii, by ta poszła w Lancerze do lasu i postrzelała do bestii czy czegoś.
        * Lancer ma wbudowany antitoxin. Środki redukujące.
    * Arkadia, niestety, jest za szybka i za dobra. Poleciała na radar.
    * Arkadia rozwaliła coś i dostała. Chciała walczyć na serio, ale antitox zaczął działać; przekonał ją by uciekła do lasu.
    * Przejąłem jej Lancera i strzelałem do terminusów by zwrócić ich uwagę - ja mogę być aresztowany. Ona nie.
* X: "Nikt nic nie wie"? Dzieki podsłuchom, terminusi wiedzą wszystko.
* V: Marek przedstawił plan Arkadii:
    * Arkadia nie jest już silnie psychotyczna. Znajdzie źródło trucizny i doprowadzi go do sprawiedliwości.
    * Potem sama się odda sprawiedliwości.

Marek nie wierzy, że ktoś mógłby ich podsłuchiwać. Arystokratów nie wolno podsłuchiwać...

Marysia wraca do Podwiertu, do hotelu. Jej cel - znaleźć Arkadię i pomóc jej znaleźć truciciela... ale gdzie jest Arkadia? Nie ma jej w pokoju, nie ma jej w okolicy. Cholera wie gdzie jest. Ale - Marysia zobaczyła Urszulę. Urszula TEŻ szuka Arkadii (o czym z radością zaraportował Torszecki).

Torszecki przekazał Marysi, że Arkadia BYŁA w pokoju, wróciła i poszła do pokoju na randkę z Różewiczem Diakonem. Ale już jej tam nie ma. Marysia spróbowała nawiązać kontakt z Arkadią na hipernecie, ale ta konsekwentnie odbija.

Marysia podeszła do Uli i poszła linią "wyciągnęłam dla Was od Marka to i to. Teraz Ty powiedz mi co wiecie czego ja nie wiem odnośnie Arkadii"

Tr+3:

* XX: Marysia JAWNIE kolaboruje z Rekinami. "Wrogowie Sowińskich (Verlenowie) kończą w pierdlu".
* V: Zapewniona współpraca Uli z Marysią w tej sprawie.
* V: PEŁNA współpraca między Ulą i Marysią (pokazuje, że Uli zależy na wyniku bardziej niż na tym by pognębić Rekiny)

Ula zaczyna co wie:

* Lancer należał do rodu Verlen. Pojawił się gdzieś tutaj, od Rekinów. Ma wyłączoną Elainkę; nie ma zaawansowanej TAI.
* Ula ma ślady wskazujące na to, że osoba sterująca Lancerem była otruta. I to jest dowód, że nie był to Samszar, który nie ma trucizny (z logów)
* Barbakan przeanalizował ślady tych środków - to była jakaś forma halucynogenów opartych o grzyby.
    * Dokładniej: środek "imprezowy", mający wprowadzić osoby w stan euforii, lekkie halucynki, "jest super, fajnie"...
        * MYRCZEK!!!

Marysia łączy się z Arkadią po raz kolejny. Ale tym razem nie jest to ping. To wiadomość. "Wiem że wiesz że to Myrczek. Wiem że wiesz że nie jest do tego zdolny. Spotkajmy się na Nieużytkach, tam, gdzie była impreza grzybowa. Mam go i przyprowadzę." (i Marysia weźmie Torszeckiego jako statystę, ON NIC NIE WIE!)

Tr+2:

* XX: Arkadia atakuje z zaskoczenia.
* X: Rafał ma ostry wpiernicz, Ula jest ranna.
* V: Arkadia się pojawi

Spotkanie na Nieużytkach. Teren jest nierówny, nieco niebezpieczny, z zasłonami itp. Ula przygotowała sprzęt, uzbrojenie... Torszecki nie wie o co chodzi i nikt mu nie tłumaczy. Ale jest szczęśliwy, że Sowińska mu ufa i bierze go na ważną akcję.

Ula ma skanery, detektory, wszystko. Jest gotowa, w ukryciu.

Ula jest pierwszą osobą, która dostaje nożem w plecy. Arkadia z zaskoczenia zakrada się i atakuje wpierw Ulę, a potem Rafała. Gdy stoi nad nimi z nożem, Marysia próbuje ją spowolnić i zagadać - "to nie on". Arkadia nie wie kogo ma bić, dalej jest lekko psychotyczna. Marysia kupuje czas.

* X: Arkadia się odwraca i rzuca nożem w Ulę...
* V: Ula odpala "tazer" i wali w Arkadię ogromnym wyładowaniem. Arkadia traci przytomność.
* Ula dostała w ramię z bronią i jest unieszkodliwione. Ale Arkadia jest wyłączona.

Rafał płacze, ale to nie jest "śmiertelna" rana, tylko bardzo bolesna. Arkadia wyraźnie wie jak krzywdzić.

EPILOG:

* Julia i zespół naprawiają robota, rozpaczliwie, na szybko. Zdały. Na trójkę. Minus.
* Wyszło na to, że faktycznie to była mikstura imprezowa Myrczka. Ale nie on podał to Arkadii.
* Arkadia i Marek zostali aresztowani na pewien czas.
    * Ula poprosiła dowództwo, by byli w celach przyległych. Jej prośba została przyjęta.
    * Arkadia była o 100% bardziej smutna.
* Arkadia dostała BANA od terminusów w Pustogorze na JAKĄKOLWIEK broń. Jest jedynym magiem bez prawa używania jakiejkolwiek broni
    * Arkadia zaczęła studiować broń improwizowaną...
* Rekiny uznały, że Marysia Sowińska to okrutna bestia jest. Fajna. Umie użyć terminusów jako broni.
* Ula zaczęła czytać o rodach Aurum...
* Relacja Marysia - Arkadia jest w ruinie. Ale relacja Marysia - Ula się usprawniła.

## Streszczenie

Zespół AMZ składał projekt zaliczeniowy, gdy zaatakowała Arkadia Verlen na grzybkach ze swojego Lancera, uszkadzając projekt (robota). Julia wezwała Ulę (terminuskę) na pomoc, Ula unieszkodliwiła Marka (który podmienił Arkadię). Jako, że Julia i Triana były zajęte ratowaniem projektu, Marysia rzuciła się na znalezienie "kto atakuje Julię". Dotarła do niewinności Marka, dotarła do Arkadii i z pomocą Uli unieszkodliwiła Arkadię. Jedyne straty - ranna Ula i ranny Torszecki. Ale kto podał Arkadii te grzybki i jak?

## Progresja

* Marysia Sowińska: jawnie kolaboruje z terminusami wykorzystując ich jako broń przeciwko Arkadii. "Wrogowie Sowińskich kończą w pierdlu". Jest bezwzględną Sowińską.
* Urszula Miłkowicz: niechętny szacunek do Marysi Sowińskiej. Będzie z nią współpracować. Marysia nie jest ani zła ani głupia. Da się z nią sporo zrobić.
* Arkadia Verlen: straciła servar klasy Lancer. Ma ABSOLUTNEGO bana na jakąkolwiek broń. Nie wolno jej używać ŻADNEJ broni. Plus, tydzień prac społecznych.

### Frakcji

* .

## Zasługi

* Marysia Sowińska: przeprowadziła dochodzenie - kto chciał zestrzelić robota kultywacyjnego Julii? Znalazła Arkadię na grzybkach i współpracując z Ulą ją unieszkodliwiła podstępem i fortelem.
* Julia Kardolin: wezwała terminusów (Ulę) jak była ostrzeliwana i odwróciła railgun Lancera Arkadii. Potem przejęła dowodzenie nad zespołem - muszą oddać robota na czas XD
* Triana Porzecznik: pracuje nad robotem kultywacyjnym; zupełnie nie radzi sobie jak jest ostrzeliwana przez Lancera.
* Ignacy Myrczek: jego grzybki były użyte do wpłynięcia na Arkadię; pracuje nad robotem kultywacyjnym i jest bezwartościowy pod ostrzałem.
* Liliana Bankierz: podejrzewa, że uszkodzenie robota kultywacyjnego to plan firmy Zygmunt Zając. Ale odda się pod dowodzenie Julii by dostarczyć robota na czas.
* Marek Samszar: niewinny, ale wziął na siebie winę (przejął od Arkadii Lancera), by Arkadia mogła szukać bezpiecznie tego co ją otruł grzybkami.
* Arkadia Verlen: pod wpływem grzybków wpierw zestrzeliła robota kultywacyjnego a potem zapolowała na Ulę po komunikacie od Marysi. Mistrzyni noży i genialna kinetka. Assassin-build.
* Urszula Miłkowicz: niechętnie współpracująca z Marysią uczennica terminusa. Bardziej kompetentna niż się wydaje, choć kij w tyłku. Pokonana z zaskoczenia przez Arkadię (nie doceniła), potraktowała ją tazerem jak już była ranna i na ziemi. Umie współpracować jeśli chce.
* Rafał Torszecki: Rekin. Cel życiowy - być największym przydupasem Marysi Sowińskiej na świecie. Dość przydatny w pozyskiwaniu informacji, acz nie ma zupełnie inicjatywy. Wykonuje rozkazy, nie myśli proaktywnie. Skończył jako przynęta na Arkadię.
* Różewicz Diakon: Rekin. Specjalizuje się w kwiatach, pięknie i truciznach. Piękny i nieskazitelnie elegancki. Użyty przez Arkadię by dowiedzieć się więcej o tym środku który na nią działa.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Nieużytki Staszka: Zespół robił tam projekt zaliczeniowy z robotem rewitalizującym a Arkadia ostrzelała go z Lasu Trzęsawiskowego swoim Lancerem.
                                1. Las Trzęsawny: prowadzi do Podwiertu; tam Ula znalazła Marka Samszara w Lancerze Arkadii i go unieszkodliwiła.
                            1. Pustogor
                                1. Rdzeń
                                    1. Barbakan
                                        1. Kazamaty: podziemne więzienie Pustogoru, w którym trzymany jest Marek Samszar by skruszeć.
                            1. Podwiert
                                1. Las Trzęsawny: prowadzi do Zaczęstwa; 
                                1. Dzielnica Luksusu Rekinów: miejsce desygnowane dla młodych władców Aurum w Podwiercie, w kierunku na Las Trzęsawny ;-).
                                    1. Obrzeża Biedy
                                        1. Domy Ubóstwa: miejsce, gdzie mieszkają "możne" Rekiny ale nie "najmożniejsze" Rekiny. Np. tam mieszka Rafał Torszecki czy Arkadia Verlen.

## Czas

* Opóźnienie: 15
* Dni: 1

## Inne

.

## Konflikty

* 1 - (magiczny) Liliana buduje barierę z latającego gruzu by nie dało się ich zobaczyć i by osłonić ich przed railgunem
    * trudny magiczny +2
    * Om
* 2 - (magiczny) Julia składa zaklęcie mające odwrócić pocisk railguna i uszkodzić jakoś strzelającego Lancera.
    * trudny magiczny +2
    * vxovxv: namiar, odchylenie, odwrócenie pocisku, uszkodzenie ścigacza, uszkodzenie robota, zrujnowany projekt
* 3 - Marysia przesłuchuje Samszara w kazamatach pod Barbakanem - by dowiedzieć się KTO za tym stoi (Arkadia).
    * ekstremalny zasobowy +3
    * vxv: Marysia przekonuje Ulę do współpracy - razem szybciej znajdą i unieszkodliwią Arkadię. Ula powie Marysi co wie i vice versa.
    * Tr+3
    * XXVV
* 5 - Marysia i Ula zastawiają pułapkę na Arkadię, by ją unieszkodliwić
    * Tr+2
    * xxxv: Ula ranna, Rafał ranny
