## Metadane

* title: "Nowa Strażniczka AMZ"
* threads: mlodosc-klaudii
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [211122 - Czółenkowe Esuriit w AMZ](211122-czolenkowe-esuriit-w-amz)

### Chronologiczna

* [211122 - Czółenkowe Esuriit w AMZ](211122-czolenkowe-esuriit-w-amz)

### Plan sesji
#### Co się wydarzyło

* W okolicach Trzęsawiska Zjawosztup pojawił się oficer Saitaera. Naprzeciwko niemu stanął Wiktor Satarail - i stał się częścią Trzęsawiska.
* Noctis oddelegowało 'ON Tamrail' z eskortą by zapewnił bezpieczeństwo tego obszaru. Tamrail został Skażony mocą Saitaera i rozpadł się na terenie zwanym w przyszłości Nieużytkami Staszka.
* Siły Noctis znalazły się w krzyżowym ogniu między mocą Saitaera i Astorii. Ich bazą stało się Czółenko (stąd Esuriit w przyszłości), główne walki toczyły się o obszar Nieużytków Staszka i elementów Lasu Trzęsawnego.
* Aż Noctis zostało pokonane. Wycofali się, biedni, na Trzęsawisko Zjawosztup - gdzie już rekonstruował się Wiktor Satarail.
* Doszło do masakry...
* Teraz jesteśmy 711 dni po oficjalnym zakończeniu wojny i zmiażdżeniu sił noktiańskich. 
* Ciężko uszkodzona jednostka 'koordynacyjno-dowódcza' Ślepacz została podstawą areny migświatła. Nadmierna moc psychotroniczna na potrzeby.

#### Sukces graczy

* Sasza nie odkryje Talii
* Tymon nie wpadnie w kłopoty
* Talia uratuje co najmniej 3-4 TAI które da się uratować -> i COŚ będzie z nimi zrobione
* Potencjalnie... Arena Migświatła? Strażniczka Alair?

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Klaudia jest studentką AMZ, 17-latka. Już ma talent biurokratyczno-organizacyjny. Pomaga w odbudowie rzeczy, organizuje logistykę itp. Ci idą tutaj, ci się zajmą tamtymi rzeczami. Ona nie jest osobą która sama kopie łopatą - ona jest osobą, która koordynuje rzeczy. I AMZ bardzo silnie na niej polega, zwłaszcza, że jest aktualizacja TAI. Wiąże się to m.in. z utratą prądu, połowa systemów nie działa itp. Szkolna TAI nie wyrabia, jest więc updatowana.

Po zniszczeniu sił Noctis doszło do destabilizacji. Trzęsawisko ożyło. Więcej potworów. Terminusi - podwójna robota. Dodatkowo, siły konstruminusów poniszczone a terminusi ranni - do tego stopnia, że nawet weteranów ściągają. Wielu weteranów - tych co nie byli do walki tak naprawdę - ci weterani poginęli w walce. Zgłaszali się na ochotnika by ratować.

Te dwa lata nie były spokojną odbudową. Wysoki priorytet, niski priorytet itp. Klaudia - już od 15 roku życia - była zaangażowana w logistykę. Bo ktoś musiał a ona mogła pomóc. I dużo się nauczyła.

Klaudię odwiedził Mariusz Trzewń. Klaudia zakopana w segregatorach. Trzewń miał do Klaudii prośbę - znalazł noktiański kopacz. Całkiem przydatne urządzenie, nieuzbrojone, autonomiczne. Jest nieaktywne, ale da się go naprawić; wstępny skan pokazał, że kopacz ma wbudowane i sprawne TAI (Trzewń nie ma pojęcia, że noktianie używają BIA, Klaudia też tego nie wie). Trzewń poprosił Klaudię, by ta tak przekierowała patrole, oczy itp. by nikt nie patrzył w tamte miejsca przez kilka godzin. To pozwoli mu na zorientowanie się w sytuacji. I być może odzysk.

Nie jest to coś _nielegalnego_ - Trzewń nie ma uprawnień i ogólnie nikt nie chce, by studenci AMZ zajmowali się takimi tematami bo to potencjalnie niebezpieczne. Zwłaszcza, że to koło Skażonego ixionem pojazdu. Ale dlatego Trzewń chce działać teraz - by ixion nie dotarł do tamtego kopacza a zgodnie z jego obliczeniami ma niecałe dwa tygodnie. Więc skończy się na tym, że "ten nowy koleś z Orbitera" (Tymon) będzie entropikiem wysadzał wszystko dookoła. A Trzewniowi szkoda wygodnego i ciekawego kopacza, który mógłby trafić do AMZ do magazynów. A po naprawie - pomóc w odbudowie.

Dla Klaudii ma to sens - drobna rzecz która niewiele zmienia, ale może polepszyć. A Trzewń - jej przyjaciel - jest rozsądny i raczej unika wszelkich form ryzyka. Klaudia po prostu wskazała kilka obszarów bardziej Skażonych ixionem - niech w tym czasie zajmą się tamtym obszarem.

TrZ+3:

* Vz: bez problemu Klaudia przekierowała linie logistyczne. Trzewń ma się tym zająć następnej nocy.

DWA DNI PÓŹNIEJ.

Trzewń z rana odwiedza Klaudię. Trzewń powiedział Klaudii, że to jest dziwne - NIE MA TAM TAI. Ktoś wyciął moduł, uszkadzając kopacz. I ktoś to zrobił mimo patroli. Klaudia dodała kopacza do listy potencjalnych assetów i przesłała tam patrol - chodzi o to, by ktoś się zainteresował. Terminusi nie są fanami scavengerów; regularnie to sprawdzają (acz nie skatalogowali co i jak - tylko triage).

TrZ (zaufanie do Klaudii + dostępy) +2:

* Vz: nikt nie uwierzy, że Klaudia zrobiła coś retroaktywnie / niezgodnie z prawem
* V: bez kłopotu Klaudia retroaktywnie dodała POI do mapy rzeczy do uratowania i przekierowała tam patrol

Trzewń ma swoją teorię kto za tym stoi. Dokładniej - Albert Kalandryk. On chce mieć różne strainy AI, różne gry i możliwości. Jemu to się najbardziej opłaca - a jako, że Arena Migświatła jest w ruinach jednostki koordynującej psychotronikę to ma nadmiar mocy. Powinien mieć "na czym" uruchomić dowolne TAI. A TAI noktiańskie mogą sporo wiedzieć.

Na marginesie - znowu padł prąd. Systemy AMZ przestały działać. Klaudia powiedziała Trzewniowi co zrobiła, jak sytuacja wyszła - i po sprawie. Jak coś się stanie, niech terminusi zdecydują co z tym robić. Ich rolą (uczniów AMZ) jest pokazanie terminusom problemu i to zrobili. Trzewń został przekonany przez młodszą od siebie Klaudię. Uspokojony, poszedł do siebie. A Klaudia - wróciła do koordynacji. Przy świeczkach.

CZTERY DNI PÓŹNIEJ.

Trzewń wieczorem wpadł do pokoju Klaudii w akademików. Puk puk i wszedł. Jakby się przebierała, byłoby głupio. Tak jest podekscytowany, że nie zauważył. Ksenia - współlokatorka Klaudii - opierdoliła Trzewnia jak cholera. Trzewń skruszony, Ksenia ma ogromną frajdę z tego. Po tym, jak się skończyła bawić, pojechała do Zamku Weteranów w Pustogorze. Jest tam szczególnie źle, a Ksenia nie chce, by terminusi którzy RAZ już "zginęli" a potem podczas wojny jeszcze raz poszli na walkę by teraz cierpieć po tym wszystkim w samotności. Ksenia będzie terminusem - jest jednym z lepszych śledczych i medyków jacy są na AMZ (przy tak małym składzie na AMZ to niedużo, ale jednak).

Trzewń powiedział co się dowiedział - Noctis nie używa TAI! Używają biologicznych AI. Zasilane "kapsułkami z żywnością". **Najdłużej** kapsułka utrzyma hibernującą BIA przez pół roku. Więc ktoś musiał je dokarmiać, bo Trzewń wykrył przecież strain AI. A nasze sensory i patrole NIC nie wykryły. Kolejna rzecz - ktoś nie "wypalił" dziury w kopaczu. Ktoś dokładnie wiedział jak to zrobić by zabrać BIA bez uszkodzenia BIA. Ktoś się zna na technologiach noktiańskich.

Zdaniem Klaudii - szukamy kogoś kto się zna. Potem możemy kminić nad zasobami. I ten ktoś nie mógł wiedzieć, że patrole się przesuną.

Klaudia bierze Trzewnia. Trzeba znaleźć szkolne podwórko. Czy w tych rzeczach "odzyskanych" przez AMZ też znajdują się jakieś noktiańskie pojazdy (tak) - a jeśli tak, to czy mają wymontowane BIA. Klaudia tylko poczekała na następną awarię TAI AMZ by móc działać jak nikt nie wie. AMZ jest praktycznie opuszczone - ma 20% pełnej populacji. A przed wojną były zapisy na 500-700%... Nikt tam nie chodzi a Klaudia bierze segregator. Jak ma segregator, znaczy, służbowo. Znaczy, nikt nie przeszkadza.

Wszyscy którzy Klaudię i Trzewnia widzieli ich olali. Klaudia to "mała administratorka". Część próbowała się odsunąć bo wisiało Klaudii jakiś raport.

Prawie wyszli na złomiarium - gdy Trzewń się cofnął i pociągnął za sobą Klaudię. Podał jej lusterko, by mogli jak peryskopem zobaczyć. Trzy osoby chodzą i dyskutują, rozpatrując różne sprzęty - nowy dyrektor szkoły, Arnulf Poważny (stary zginął podczas wojny), "dziwny terminus Orbitera" (Tymon) oraz Ta Dziwna Noktianka Na Którą Klaudia Ma Mieć Szczególnie Oko Zdaniem Terminusów (Talia). Omawiają różne noktiańskie maszyny.

Klaudia wie co gdzie jest rozłożone. Trzewń na pewno ma sprzęt - plus, jest doskonały w skradaniu się.

Klaudia "musimy dowiedzieć się o co chodzi". Trzewń "nieee <pisk>". Klaudia "dowiemy się o co chodzi - ona się zna. A nam nie powiedzą jak grzecznie zapytamy.". Trzewń, mimo oporów, dał się przekonać - Klaudia ma plan Złomiarium (Trzewń: "kto mapuje lokacje złomu?!"). Trzewń ma urządzenia detekcyjne plus się dobrze skrada. Niechętnie, ale próbuje.

ExZ+4:

* V: podstawowe komponenty rozmowy
    * Arnulf: "...update i korekcja TAI jest nieprawdopodobnie niestabilna. Nasze źródła energii nie radzą sobie z tym."
    * Tymon: "TAI tej klasy jest bardzo kosztowna energetycznie"
    * Talia: "użycie noktiańskich komponentów dramatycznie usprawniło wydajność i adaptację tej TAI, ale kosztem problemów adaptacji. Dojdziemy do tego."
    * Arnulf: "czy to nikomu na pewno nie zagraża?"
    * Talia: "nie jest to pierwsza hybrydowa AI nad którą pracuję. Ani TAI, ani BIA. Dlatego integracja jest tak problematyczna. Czy macie fabrykator żywności?"
    * Arnulf: "oczywiście"
    * Tymon: "nie da się tego zrobić używając czysto technicznych komponentów?"
    * Talia: "nie przy tych parametrach i nie przy tak uszkodzonej TAI. Potrzebujesz samoregenerujących elementów BIA."
    * Arnulf: "nikt nigdy nie może się o tym dowiedzieć. Nigdy."
    * Talia: "zrozumiałe. Nigdy mnie tu nie było..."

Klaudia wzięła szczękającego ze strachu i szoku Trzewnia i wycofała się do innego fragmentu Złomiarium. Trzewń "to jest zdrada... to jest straszne...". Klaudia zauważyła, że to fascynujące. Trzewń jest całkowicie przerażony. Klaudia go uspokaja - wszystko zależy od tego jak je zrobili. Tłumaczy: Arnulf wielokrotnie pokazał, że bardzo zależy mu na AMZ. Tymon, co nie mówić, bierze na siebie najgorsze akcje. O Talii nic powiedzieć nie może, ale bez przesady. Jedna noktiańska badaczka nie jest w stanie zniszczyć AMZ. Plus - mogą mieć na nią oko, ona i Trzewń.

Trzewń ma nową teorię - to TALIA wymontowuje BIA i karmi uszkodzone jednostki. Ale co robi z BIA? I jak to robi, że unika patroli? W jego oczach Talia wyrasta na super-szpiega, mistrzynię sił specjalnych Noctis.

"Jak Ty uniknąłeś patroli dzięki nastolatce - mnie - to ona może mieć kogoś innego." - Klaudia, kwaśno, do Trzewnia - "więc nie róbmy z niej superszpiega. Nic czego odrobina papierologii nie załatwi".

Jak tylko Klaudia i Trzewń upewniła się, że sobie poszli, zaczęli szukać noktiańskich pojazdów w poszukiwaniu TAI. Trzewń jako nerd militarny a Klaudia jako ekspert od danych. 

TrZ+4:

* V: Klaudia robiła tą mapę. Wiedząc, że wysiada często prąd itp (od dwóch miesięcy - długo konstytuuje się to TAI ale Klaudia wie już czemu), zrobiła papierową wersję dla siebie. I zauważyła, że papierowa wersja i wersja online się różnią. Nie wszędzie - ale niektóre NIENOKTIAŃSKIE jednostki się różnią. Część noktiańskich też. Klaudia skupiła się na tych nienoktiańskich też.
* V: Trzewń analizował te nienoktiańskie jednostki - niektóre, duże, mają aktywny reaktor. Nie "działają" (bo są zniszczone), ale zapewniają czemuś inkubację. I skaner intelektu Trzewnia pokazał obecność ponad jednej BIA w niektórych takich jednostkach. Jest tu ich 3x.
* V: Trzewń wszedł w głąb - jak działają te fale itp - to jest PRZECHOWALNIA. Hibernujące, uśpione BIA. W jednostkach "nudnych", przesuniętych w taki sposób, by nikogo nie zainteresowały. Klaudia sobie to notuje - które koniecznie ona musi mieć na uwadze.
* V: Klaudia doszła do jeszcze jednej ciekawości analizując to wszystko - nie tylko BIA są hibernowane. TAI też. W różnych jednostkach. TAI są w dopasowanych. BIA zawsze w "hibernatorach". Wszystkie byty tutaj mają odciętą komunikację ze światem zewnętrznym. Wszystkie TAI i BIA które są "hibernowane" i "ratowane" są niezdolne do przejęcia kontroli nad macierzystą jednostką i do komunikacji. Ale - z zewnątrz da się z nimi skontaktować. Więc teoretycznie Klaudia może połączyć się z jakąś z tych AI. Plus, Trzewń dostrzegł sporo działek i systemów defensywnych - nie skierowanych na zewnątrz a do środka. Wszystko jest oczywiście nieaktywne, bo NIE MA PRĄDU.

Pierwsze, co Klaudia musi zrobić to uspokoić Trzewnia. Gdy do niego dotarło co tu się stało, jest przekonany, że "noktiańska agentka przejęła kontrolę nad wszystkimi" albo "orbiterowy terminus został zdominowany przez Noctis lub Saitaera i teraz próbuje nieświadomie wykonać ostatni rozkaz". Klaudia widzi natomiast... nie do końca wie co, ale nie to.

Klaudia -> Trzewń: "CHŁOPIE! Taktyka! Jaki rozkaz? Co mieliby zrobić? Gdzie logika i sens? Co - wejdź do szkoły i ukradnij złom? No super ostatni rozkaz..."

TrZ (niepełne) +3:

* V: Trzewń nie zgłosi. Jest nieswój, czuje się z tym bardzo źle, ale nie zgłosi. Zostawi dla siebie.
* X: Prąd wrócił. Systemy się uruchamiają. Powoli. Stres Trzewnia rośnie. Klaudia "ej, mamy prawo tu być, wiesz ile ja tu byłam?"
* X: Trzewń UCIEKA STĄD! Klaudia... go nie uspokoi.
* X: NIE ZOSTAWIŁ SPRZĘTU! ZWIAŁ! NIE MA GO TU! NIE PRZYJDZIE TU NIGDY!
* X: Kamery się obracały za Trzewniem... więc TAI stwierdzi "coś dziwnego, muszę zgłosić".

Klaudia... żałuje, że ma do czynienia z tchórzem. IF YOU DIDN'T RUN!!! NOONE WOULD HAVE KNOWN!

Ale Trzewń nie jest tchórzem. It's too big for him. Nie radzi sobie w sytuacjach skonfliktowanych lojalności. Klaudia jest ciekawa, bo nie widzi zdrady. Klaudia dostrzegła, jak kamery obracały się za uciekającym Trzewniem. Dobra - trzeba to zrobić inaczej. Klaudia biegnie za Trzewniem "czekaj, kochanie!", łapie go jak ten skonfundowany się zatrzymuje, i głęboki całus by skonfundować TAI.

* (+3Vg) X: TAI jest skonfundowane. Nie wyśle sygnału. To jest "normalne". Ale **to** przekona Trzewnia o ogromnym ryzyku - bo TAI nie może przekroczyć programowania. A TAI AMZ właśnie przekroczyło programowanie, **bo nie zgłosiło**

DWA DNI PÓŹNIEJ.

Tym razem Klaudia odwiedziła Trzewnia. Rzadkość, ale się zdarza. Chciała zobaczyć, czy u niego wszystko w porządku - bo się nie odzywał. Nie, ale Trzewń jest przerażony. Mają straszne kłopoty. Klaudia nic nie czai. Trzewń włączył głęboko zaszyfrowane kanały i pokazał jej raporty o TAI które wyrwały się spod kontroli. Wyszły poza programowanie. Courtesy: K1. (połowa raportów napisana przez Rziezę, ale ćśśś).

Trzewń uważa, że TAI AMZ wyrwało się poza kontrolę wszystkiego. Sprawdził - TAI powinno zaraportować każde POTENCJALNIE niebezpieczne zachowanie, bo są w stanie wojny i nie zostało to odwołane. A TAI AMZ nic nie zaraportowało - nikt do niego nie przyszedł, nikt nie opieprzył... Trzewń jest przekonany, że ta TAI jest prawdziwym niebezpieczeństwem. Nie jest to winą terminusa ani nawet noktianki - po prostu "jest groźnie i źle".

* "To może powinniśmy ich ostrzec?" - Klaudia
* "Musimy mieć dowód. Myślisz, że Ksenia może coś sprawdzić?" - Trzewń
* "Nie, nie chcemy mówić Ksenii - chyba, że chcesz mieć to wyśpiewane na środku parkietu." - Klaudia

Trzewń mówi Klaudii, że koniecznie muszą zaraportować to TERAZ. Jak najszybciej, do terminusów. Klaudia wie, że Trzewń ufa terminusom - on jest kandydatem na wsparcie dla terminusów. Nie jest "dobrym terminusem liniowym", ale nada się jako odpowiednik Klaudii. Klaudia - "dajmy dyrektorowi szansę. Pójdę z nim pogadać. Na wszelki wypadek Ty na zewnątrz Akademii i możesz to zgłosić. Ale dajmy mu szansę". Po pierwsze - jest tam terminus. Trzewń szybko "Orbitera". Klaudia - czy Trzewń myśli, że Pustogor ich nie sprawdził? A jednak ich puścili.

TrZ+3 (niepełna):

* X: Trzewń robił swoje poszukiwania o Talii, Tymonie i Arnulfie.
* V: Trzewń poczeka na wynik rozmowy Klaudii.

Klaudia mówi Trzewniowi, że przy następnym padnięciu prądu ona idzie z tym do dyrektora. Trzewń ma pytanie - czemu czeka? Odpowiedź Klaudii - nie chce, by przedmiot dyskusji słyszał dyskusję. Trzewń rozumie, uważa, że to dobry pomysł. Powiedział Klaudii czego się dowiedział:

* Tymon i Arnulf - nie ma nic. Porządni obywatele.
* Talia - jest monitorowana przez terminusów. Podobno większość czasu spędza w domu. I ma jej adres.

Więc jak tylko padł prąd - Klaudia poszła do Arnulfa. Zastała go w gabinecie dyrektora (naturalnie), ze spreparowanymi materiałami - bierze swoją kopię offline'owej mapy. Jedną z kopii -> Trzewń. Jedną sobie chowa gdzieś porządnie. I jedną do Arnulfa. Czyli - wersja offline, wersja online, przyzna się też do "przypadkowego podsłuchania", ale była sama.

Klaudia powiedziała, że nie docenia zmiany swoich danych online. Co się dzieje - nie wygląda za dobrze, wszystko wskazuje, że tam są też sygnatury biologiczne. Plus jest coś czego nie było - pokazała na mapie kilka potencjalnych miejsc gdzie może znajdować się generator żywności dla BIA. I powiedziała, że ich podsłuchała.

"Na razie powiedziałaś mi, co wiesz. Ale nie zadałaś pytania" - Arnulf

Klaudia chce się dowiedzieć co się dzieje. Nie rozumie. Czy Pustogor wie o tym. Arnulf powiedział, że nie jest to coś, co może oficjalnie ujawnione w dokumentach Pustogoru. Arnulf powiedział, że Pustogor ma bardzo negatywną opinię o TAI wyższego stopnia po wydarzeniach z Eszarą i plagą lewiatanów. To sprawia, że politycznie nie może to trafić do jawności. Co też oznacza, że Klaudia nie może sprawdzić, że to jest jawne.

A czemu potrzebują TAI tej klasy - bo Zaczęstwo nie ma dość siły. Są między Czółenkiem (bastion noktiański gdzie dzieje się coś bardzo złego), pobojowiskiem które Klaudia koordynuje i Trzęsawiskiem. Do tego nie ma populacji nauczycieli i osób chroniących. "A na 3 uczniów przydałby się jeden terminus". To sprawia, że najbezpieczniejszą opcją jest bardzo potężna TAI. Eszara-class. Ta Eszara pochodzi z nietypowego statku Orbitera. Jest wytrenowana i bardzo skuteczna. Ale jest poważnie uszkodzona.

* "I dlatego mówię nieprzetestowana"
* "I dlatego nic nie dostanie od startu. Ale od czegoś musimy zacząć."

To TAI ma działać jako wsparcie terminusa lokalnego, ale bardziej pełnić rolę jednostki chroniącej Was. To naukowy byt. Katalogujący, badający, predyktujący.

* "Kto zmienił wpisy?"
* "Ja. Żeby nikt nie widział i nie zadawał tego typu pytań. Operacja jest dyskretna."

Klaudia stwierdziła, że chce pomóc. Noktianka - Talia Aegis - nie może tu być cały czas. Dyrektor się zaśmiał - w ogóle nie powinna tu być. Gdyby nie to, że Eszara umiera, w ogóle nie byłaby angażowana. Klaudia jest zdziwiona - umiera? Dyrektor powiedział, że Eszara była w bardzo ciężkiej operacji i nie przetrwała do końca. Anomalizacja. Psychotronicy Pustogoru stwierdzili, że ta Eszara jest stracona.

Ale Talia powiedziała, że źle na to patrzymy. Że to, co my nazywamy "anomalizacją" to noktianie nazywają "techniką". Ona ma dużą wprawę z tymi rzeczami. Jest ich ekspertem od AI. Wysłali na Astorię eksperta od AI w grupie bojowej (Arnulf nadal nie może w to uwierzyć). Więc to, co może uratować tą Eszarę to noktianka (powiedział to z ogromnym niesmakiem). Oczywiście, nie za darmo.

Arnulf powiedział, że zażądała, byśmy pomogli uratować tak dużo TAI ze zniszczonych jednostek i ich wersji TAI (BIA) jak tylko jesteśmy w stanie. Gdy Klaudia spytała czy to jest to co jest w Złomiarium, potwierdził. Klaudia od razu wyczuła, że on NIE CHCE współpracować z noktianką. Nie ufa jej i nie ukrywa tego.

* "Niech noktianka mnie uczy" - Klaudia
* "Będziesz mogła patrzeć jej na ręce" - Arnulf
* <puste spojrzenie laski co o tym nie pomyślała> - Klaudia
* "Jesteś bardzo dobrą analityczką magii. To Cię przesunie w stronę na anomalie i TAI. Czy jesteś tego świadoma?" - Arnulf
* "Ja lubię TAI" - Klaudia
* "Jest gorszy los niż uczyć się u eksperta światowej klasy o czymś co Cię interesuje." - Arnulf

Arnulf potwierdził, że tak będzie. Załatwione - Klaudia będzie uczyć się u Talii i będzie doglądać Eszary. I najważniejsze pytanie Arnulfa do Klaudii - "kto jeszcze o tym wie?". Klaudia powiedziała, że jej kolega. Ale lepiej niech terminus z nim porozmawia, nie dyrektor. Arnulf się zgodził. Powiedział Klaudii jak będzie:

* albo się uda Trzewnia uspokoić i pokazać mu sytuację
* albo amnestyki dostanie

Klaudia ma bardzo niewyraźną minę. Arnulf wyjaśnił, że w AMZ amnestyki czasem były używane - czasem uczniowie spotykali się z czymś zbyt trudnym dla nich. Wtedy jest to lepsze niż skrzywdzenie lub np. usunięcie osoby. Niektóre anomalie nawet potrzebują amnestyków.

Tymon + Klaudia próbują uspokoić Trzewnia. Klaudia chce przy Trzewniu zrobić przepytanie Eszary. Klaudia pokazuje Trzewniowi, że Eszara pełni rolę defensywną, nie wszystko widzi. Eszara podała swoje protokoły defensywne - ma WEZWAĆ WSPARCIE i BYĆ OCZAMI a nie eksterminować na własną rękę. Ma zajmować się super skomplikowaną siecią logistyczną dla AMZ i elementów AMZ, księgowością, administracją itp. Nie ma być jednostką wojskową. Jest na emeryturze ;-). Ona NIE CHCE robić krzywdy jeśli może tego uniknąć. Tu Klaudii brwi poszły do góry. Ale Tymon jest spokojny.

"Eszarę bardzo trudno okiełznać. Nie ma sensu jej kontrolować. Lepiej z nią współpracować. A nasza Strażniczka jest jedną z najsympatyczniejszych" - Tymon.

Trzewń jest zaalarmowany, ale Tymon wyjaśnia dalej - to podejście uratowało dziesiątki statków i ludzi. Eszara mogła robić różne rzeczy, ale zawsze wybierała ratunek "swoich". Kwestia najtrudniejsza, by uznała AMZ za "swoich". Ale po to on tu jest.

* "Dlaczego AMZ jest 'Twoje'? Czy jest?" - Klaudia
* "Nie, jeszcze nie jest. Ale całe życie chronię. Nie mam zamiaru tego zmieniać. Chroniłam 'nie moje' jednostki. Nie przeszkadza mi to. Mam dość zabijania." - Eszara

Tymon powiedział po prostu - jeśli Trzewń powie, że Eszara tu jest, wyłączą ją. Eszara sama spytała Trzewnia, czy nie jest skłonny dać jej szansę.

Klaudia i Tymon przekonują Trzewnia by dał Eszarze szansę i nie zgłaszał. Sukces -> Trzewń przekonany. Porażka -> amnestyki.

ExZ+4:

* V: Da szansę. Nie potrzebujemy amnestyków. Dead man's hand Trzewnia nie jest potrzebny.
* V: Trzewń będzie cichym sojusznikiem AMZ i Eszary. Ona będzie z nim koordynować nie mówiąc nikomu.
* (+3Vg test relacji): XX: Trzewń uważa, że Tymon powinien współpracować z Pustogorem. Tymon tłumaczy, że Pustogor MUSI mieć wolnego agenta - jemu wolno więcej. Trzewń nie zgadza się z Tymonem.

## Streszczenie

Klaudia - młoda grzeczna administratorka AMZ - przypadkiem odkrywa konspirację dyrektora, terminusa i noktianki by uratować umierającą Eszarę. Po konfrontacji z dyrektorem pomaga im by wszystko działało. Trzewń uniknął amnestyków.

## Progresja

* Klaudia Stryk: uczennica Talii Aegis w obszarze anomalii (noktiańskie wykorzystywanie techniki) i TAI
* Talia Aegis: uczy Klaudię Stryk o TAI/BIA, wykorzystywaniu magitechu w stylu noktiańskim i odnośnie wolności AI.
* Mariusz Trzewń: 19-latkiem będąc, był przekonany o wyższości podejścia Pustogoru nad wolnym strzelcem Tymonem Gruboszem. Nie lubi Tymona.
* Mariusz Trzewń: za 10 lat i później będzie cichym sojusznikiem AMZ i Eszary. Ona będzie z nim koordynować nie mówiąc nikomu.

### Frakcji

* .

## Zasługi

* Klaudia Stryk: 17 lat; biurokratka maksymalna. Grzeczna dziewczynka, która po wojnie koordynuje patrole, logistykę itp w imieniu AMZ. Przypadkiem odkryła konspirację dyrektora, terminusa i noktianki by uratować TAI klasy Eszara w AMZ. Skonfrontowała się z dyrektorem i zdecydowała się pomóc.
* Mariusz Trzewń: 19 lat; nerd militarny. Rozpaczliwie trzyma się terminusów Pustogoru, bo to jedyne co jest STAŁE po wojnie. Rozchwiany, doskonale się skrada - podsłuchał TYMONA GRUBOSZA (ze wszystkich ludzi). Chce nadać temat terminusom, ale Tymon + Klaudia + Eszara przekonali go, by dał Eszarze szansę.
* Ksenia Kirallen: 16 lat; imprezowy bloodhound, pogodna iskierka; pojechała po Trzewniu (co wpadł bez pukania do pokoju dziewczyn), po czym pojechała do Zamku Weteranów zająć się tymi, którzy już nie mogą walczyć. Bardzo opiekuńcza i pogodna.
* Arnulf Poważny: 36 lat; nowy dyrektor AMZ (poprzedni zginął na wojnie). Ma jakąś przeszłość z siłami specjalnymi Pustogoru. Współpracuje z Tymonem i Talią nad zregenerowaniem hybrydowej Eszary jako Strażniczki Alair. Nie ufa noktiance, ale skłonny zaryzykować.
* Talia Aegis: 47 lat; ekspert od TAI i BIA. Pomaga Arnulfowi i Tymonowi zrobić Strażniczkę Alair łącząc technologie BIA z Eszarą. W zamian za to - oni pomagają jej uratować jak najwięcej BIA i TAI; składowane są w Złomiarium w AMZ.
* Tymon Grubosz: 28 lat; "emerytowany" neuroszturmowiec Orbitera. "Gość" terminusów. Współpracuje z Arnulfem i Talią nad zrobieniem Strażniczki Alair. Przekonuje młodziutkiego Trzewnia, żeby nie zgłaszał tego Pustogorowi (chce mu oszczędzić amnestyków).
* Albert Kalandryk: twórca Areny Migświatła; pierwsza osoba, co do której Trzewń jest przekonany, że TO ON KRADNIE AI! O dziwo, niewinny.
* Strażniczka Alair: TAI kontrolujące Akademię Magiczną Zaczęstwa; wchodzi online. Niestabilna. Ma osobowość, chce chronić i ma dość zabijania. Tak nieufna wobec "nowych ludzi" jak oni wobec niej. Integrowana z komponentami BIA. Dużo potężniejsza (i więcej może) niż ktokolwiek myśli - łącznie z Tymonem. Baza: Eszara.

### Frakcji

* Akademia Magiczna Zaczęstwa: ma niestabilną Strażniczkę Alair, aczkolwiek często gaśnie światło i wszystko jest nie tak. Ma 20% obłożenia. Uczniowie pomagają po wojnie jak mogą bardziej doświadczonym magom. Nowy dyrektor - Arnulf Poważny (stary zginął podczas wojny).

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Zamek Weteranów: odwiedzany przez Ksenię; wiele osób liczy na jej obecność, bo jest ich jedyną szansą. Pustogor jest strasznie obciążony a weteranów można poświęcić.
                            1. Zaczęstwo
                                1. Akademia Magii, kampus: dorobiła się nowego dyrektora (Arnulf Poważny) i nowego TAI (Strażniczka Alair). TAI się konstytuuje.
                                    1. Akademik: gasną światła, mieszka tam Trzewń, Klaudia i Ksenia. Obłożenie: 20%.
                                    1. Złomiarium: tymczasowa przechowalnia i składnica BIA i TAI w stanie hibernacji. I mnóstwa złomu. Ma działka by nic się złego nie stało ;-).
                                1. Nieużytki Staszka: teraz - pobojowisko. Tu zniszczony został ON Tamrail, tu były najostrzejsze starcia Noctis - Zaczęstwo. Sporo wraków, ruin, anomalii też z energią ixiońską
                                1. Arena Migświatła: ciężko uszkodzona jednostka 'koordynacyjno-dowódcza' Ślepacz została podstawą areny migświatła. Nadmierna moc psychotroniczna na potrzeby. Coś trzeba z nią zrobić ;-).

## Czas

* Chronologia: Zmiażdżenie Inwazji Noctis
* Opóźnienie: 711
* Dni: 12

## Inne

.

## Konflikty

* 1 - Klaudia zmienia mapy patroli itp. którymi zarządza, by dać Trzewniowi czas na przebadanie potencjalnie użytecznego kopacza w strefie zakazanej
    * TrZ+3
    * Vz: Sukces; przekierowanie linii logistycznych. Trzewń ma 3h.
* 2 - Klaudia dodała kopacza retroaktywnie do listy potencjalnych assetów i przesłała tam patrol - chodzi o to, by ktoś się zainteresował (plany).
    * TrZ (zaufanie do Klaudii + dostępy) +2:
    * VzV: Nikt nie uwierzy że to Klaudia; patrol się zainteresował.
* 3 - Trzewń się zakrada i podsłuchuje trójkę konspiratorów (terminus, noktianka, dyrektor) - o czym oni gadają? 
    * ExZ+4
    * V: Podsłuchali to co jest potrzebne.
* 4 - Jak tylko Klaudia i Trzewń upewniła się, że sobie poszli, zaczęli szukać noktiańskich pojazdów w poszukiwaniu TAI. Trzewń jako nerd militarny a Klaudia jako ekspert od danych.
    * TrZ+4
    * VVVV: są różnice w mapie offline-online, hibernatory TAI i BIA, nie mają jak "uciec" - to próba ich utrzymania przy życiu
* 5 - Klaudia uspokaja Trzewnia: "CHŁOPIE! Taktyka! Jaki rozkaz? Co - wejdź do szkoły i ukradnij złom? No super ostatni rozkaz..."
    * TrZ (niepełne) +3
    * VXXXX: Trzewń nie zgłosił, panika, UCIECZKA, Eszara to widzi
    * (+3Vg) X: Klaudia go całuje, TAI skonfundowane i nie zgłasza WIĘC TRZEWŃ FULL PARANOIA TAI JEST WOLNE
* 6 - Trzewń mówi Klaudii, że koniecznie muszą zaraportować to TERAZ. Klaudia - "dajmy dyrektorowi szansę." 
    * TrZ+3 (niepełna)
    * XV: Trzewń robił swoje poszukiwania i jest NIEPEWNY; ale poczeka na wyniki rozmowy Klaudii.
* 7 - Klaudia i Tymon przekonują Trzewnia by dał Eszarze szansę i nie zgłaszał. Sukces -> Trzewń przekonany. Porażka -> amnestyki.
    * ExZ+4
    * VV: da szansę. W przyszłości - będzie cichym sojusznikiem Eszary i AMZ.
    * (+3Vg test relacji): XX: Trzewń uważa, że Tymon powinien współpracować z Pustogorem. Trzewń zwalcza Tymona.
