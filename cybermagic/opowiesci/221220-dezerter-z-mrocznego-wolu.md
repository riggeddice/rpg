## Metadane

* title: "Dezerter z Mrocznego Wołu"
* threads: brak
* motives: echa-inwazji-noctis, b-team, z-archiwum-o
* gm: żółw
* players: kić, anadia

## Kontynuacja
### Kampanijna

* [221220 - Dezerter z Mrocznego Wołu](221220-dezerter-z-mrocznego-wolu)

### Chronologiczna

* [220706 - Etaur - dziwnie ważny węzeł](220706-etaur-dziwnie-wazny-wezel)

## Plan sesji

### Fiszki

#### 1. Mroczny Wół (18 osób) (Sludge removal barge - kosmiczna śmieciarka)
##### 1.1. Dowodzenie

* Tytus Muszczak: (faeril), kapitan
    * ENCAO:  0---+ |Egocentryczny;;Nadęty i napuszony;;Religijny| VALS: Conformity, Tradition >> Stimulation| DRIVE: This is our DESTINY
    * "napotkaliśmy na coś niebezpiecznego i Wincenty poświęcił się by wszystkich ratować"
* xo: N/A
* Maja Szewieczak: engineering, comms (atarienka); wyjątkowo kompetentna 
    * ENCAO:  -+000 |Nie lubi ryzyka;;Podejrzliwa| VALS: Power, Face >> Humility, Tradition| DRIVE: Wyrwać się z tej budy
    * "zrobię wszystko, jeśli mnie zabierzecie z tej jednostki; nie mogę skończyć na ŚMIECIARCE!"
* comms: też Maja ;-)
* Nela Kaltaner: science; śliczna i wycofana drakolitka
    * ENCAO: 0+-00 |Zwiewna i niestała;;Nikomu nie ufa| VALS: Self-direction, Conformity, | DRIVE: Ukryć sekret (GUARDIAN)
    * "odkryję to, o co chodzi"

##### 1.2. Operacja

* artylerzysta: N/A
* Wincenty Jarosz: (sybrianin), security, molestował Nelę i gdy chciał zrobić jej krzywdę. Nie znosił modyfikacji bioform. KIA. 
    * ENCAO:  00--+ |Samolubny, skupiony na sobie| VALS: Face, Hedonism, Family >> Tradition, Stimulation| DRIVE: Tainted Love
    * "CZEMU JESTEM TUTAJ?!"
* Krzysztof Workisz: pilot Mrocznego Wołu
    * ENCAO:  -0-0+ |Żyje chwilą;;Zachowuje swoją prywatność dla siebie| VALS: Achievement, Family >> Stimulation| DRIVE: Ochrona rodziny
    * "by sobie dorobić!"; "przemyt i próby"
* Juliusz Akramantanis: advancer, (dekadianin)
    * ENCAO:  --0-+ |Niepokojący;;Innowacyjny, szuka nowych rozwiązań| VALS: Benevolence, Family >> Tradition| DRIVE: Złota klatka Lauriego (możliwość hobby)
    * "leave me alone"; ma kolekcję krwawych kryształów z każdego zabitego przeciwnika (34)
* Nadja Kilmodrian: security_2, (sybrianka)
    * ENCAO:  -000+ |Mistrz improwizacji| VALS: Power, Hedonism >> Conformity| DRIVE: Wygrać w rywalizacji; jest najlepsza z security, odzyskać nadzieję

### Theme & Vision

* Horror-aspect: "sometimes the monster under the bed will want to save you"
* Shame: "some of us just want old wounds to be hidden"
* Sesja detektywistyczna

### Co się wydarzyło KIEDYŚ

* Nela Kaltaner (drakolitka) ma mroczny sekret. GUARDIAN zabił jej ojca, gdy ten próbował zrobić jej krzywdę. Wszyscy myśleli, że to ona.
* Maja Szewieczek (atarienka) została odesłana tutaj, by ją chronić. Jej rodzina współpracowała z Syndykatem Oriona a tu nikt nie myśli, że Mai coś może grozić.
* Wincenty Jarosz (sybrianin) odkrył sekret Marty, acz nie STRAŻNIKA. Zrobił z niej "swoją dziewczynę". Z Mają też sypiał, ale dla przyjemności.
* Tytus podglądał wszystkie dziewczyny; Wincenty nawet mu udostępniał Nelę.

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

* Mroczny Wół transportuje duży obiekt (Spory, acz aktywny fragment jednostki noktiańskiej; komponent badawczy magii)

### Co się stanie (what will happen)

* S0: Przedstawienie detektywów
* S1: 

### Sukces graczy (when you win)

* .

## Sesja właściwa
### Wydarzenia przedsesjowe

.

### SPECJALNE ZASADY SESJI

.

### Scena Zero - impl

1. Q: Jak się nazywacie?
    1. Anadia: Adela
    2. Kić: Marianna
1. Q: Komu podpadłyście i dlaczego?
    1. A: Zainteresowałyśmy się czymś co komuś bardzo nie pasowało i nie dałyśmy się od tego odgonić (coś na linii Syndykatu Aureliona i Orbitera)
2. Q: Kto na K1 jest Waszym głównym sojusznikiem?
    1. A: Zafascynowany zwłokami i śladami zwłok kontroler i koordynator imieniem Baltazar.
3. Q: Jaki macie atut którego nikt się nie spodziewa?
    1. A: Pewien KOMODOR Orbitera miał problem i pomogłyśmy to rozwiązać dyskretnie. Wisi nam. Plus pała sympatią i chce się przespać z Adelą. Nazywa się Robert Warłomkacz
4. Q: Dlaczego Wół uznawany jest za statek wyrzutków / niższej klasy nawet jak chodzi o "śmieciarki"
    1. A: Bardzo różnorodna zbieranina, tam się przesuwa osoby niewygodne, kiepskie, nie działające zgodnie z systemem.
5. Q: Jaki był oficjalny powód przeniesienia osoby którą przenieśli niesprawiedliwie?
    1. A: Głośna akcja; podobno ta osoba jest niekompententa. Patrząc, że JEST niekompetentna to było oczywiste że to ruch polityczny. DEMONSTRACYJNIE ruch polityczny.
6. Q: Kto by zyskał na tym, że to będzie dezercja
    1. A: po pierwsze, kapitan (bo nikt się nie wpieprza). Po drugie, komodor konkurujący z Robertem Warłomkaczem imieniem Adam Puszczerczyk. Wiele osób zyska, bo on ma plamę na honorze.

### Sesja Właściwa - impl

Mroczny Wół jest godny swojej nazwy. Zapach ma iście wołowy. Ma nieodświeżany system podtrzymywania życia i ludzie pracują tam fizycznie. Znaczy jedzie starym marynarzem. Sam Wół jest solidną jednostką, całkowicie nieodświeżoną, farba pozdzierana i są ślady pyłu kosmicznego i pomniejszych kolizji. Wół to solidna jednostka o zdecydowanie za dużych silnikach (do ciągnięcia), odpowiednich nanowłóknach (do holowania) i z wzmocnionym kadłubem przednim (do taranowania jak trzeba). W środku nie jest bardzo jasny, nie jest bardzo czysty.

I w to trafiły dwie słitaśne inspektorki - Adela i Marianna. Na wejściu przechwyciła Was p.o. szefa ochrony - Nadja Kilmodrian. I już na pewno "jestem PEWNA że nie zdezerterował. Nie miałby jak. Plus, to nie ten typ. Raczej pomóżcie znaleźć co tu się stało i gdzie jest. To na pewno ten nowy artefakt." Nadja prowadzi Was do dowódcy statku - Tytusa.

Rozmowa z Nadją pokazuje jedną rzecz jasno:

* transportują lub transportowali coś "dziwnego", noktiańskiego
    * "bo tak powiedział advancer" (Nadja nie używa imienia advancera)
* kapitan kazał porzucić artefakt gdy tylko Wincenty zniknął
    * "advancer zaproponował, by wejść na pokład artefakt ale kapitan się nie zgodził"
* Nela chciała zbadać ten artefakt, ale Wincenty się nie zgodził.

Nadja przedstawiła stan wszystkich skafandrów, sprzętu itp. - Nadja powiedziała z dumą że sprawdziła OSOBIŚCIE i faktycznie stan się w większości zgadza. Mają o dwa skafandry za dużo i 2 kapsuły ratunkowe za mało. Zdaniem Nadji - i jest z tego dumna - znalazła więcej niezgodności niż Wincenty. Jej zdaniem ten statek po prostu nie miał dobrego szefa ochrony i ona jest lepsza. Zapasy się nie zgadzają, przeszukała statek i Wincentego nie ma.

* N: "Zrobiłam własne śledztwo. Mam zapisy z kamer. Wincenty nie opuścił tej jednostki. Ale go nie ma. Mówię - jakaś anomalia. MAGIA."
* M: "Zapisy z kamer - kiedy ostatni raz kamera go złapała?"
* N: "Na mostku, gdy wyłączał kamery" /lekkie oburzenie "I nie był to jedyny raz."
* M: "Co robił gdy kamery były wyłączone?"
* N: "Nie wiem, bo kamery były wyłączone..."
* A: "Nigdy go nie zapytałaś?"
* N: "Nie, nie wiedziałam. Naprawdę nie wiedziałam że to robił. To niezgodne z przepisami."
* M: "Owszem, bardzo niezgodne."

I okazuje się, że miał autoryzację od kapitana. Kapitan może nie wiedział kiedy kamery są wyłączone, ale na pewno pozwolił na to Wincentemu. Nadja nic nie ukrywa - ona chce być nowym szefem security.

Adela ściąga informacje z K1 o Nadji. Dlaczego skończyła na tej jednostce?

Tr Z (uprawnienia) +3:

* Vz: "Czemu Nadja tu trafiła? Czemu jest tutaj?"
    * Nadja próbuje przez łóżko awansować jak najwyżej. Przespała się z pewnym komodorem i jego żona miała z tym problem.
    * Nadja nie jest NAJLEPSZA. Bardzo rywalizuje, jest dość agresywna, ale nie jest najbystrzejsza ani szczególnie lojalna. Nikt się za nią nie wstawił. Ale pracuje solidnie.
    * Nadja rywalizuje z Wincentym. Co gorsza, MOŻE jest lepsza od niego w niektórych kwestiach, bo COŚ ROBI. Ale nie widać powodu, czemu miałaby się go pozbyć. Jej zdaniem "on nie uciekł".

.

* Adela: "a Twoim zdaniem co się mu stało skoro nie uciekł?"
* Nadja: "magia. Noktiańskie statki i noktiańska magia, czy coś. Nie wierzę, że noktianie nie mają magii. Zgodnie z systemami nie zniknął żaden skafander, kapsula itp - więc był, wyłączył kamery i zniknął. Magia."

Marianna chce wiedzieć od Nadji czy ona coś wie o tym, by Wincenty robił coś nielegalnego. Uzasadnione podejrzenia itp.

Tr Z (uprawnienia) +3:

* V: Nadja "nielegalnego? Chyba nie. Niemoralnego? Hm, próbował każdą dziewczynę wziąć do łóżka, a drakolitkę traktował trochę jak szmatę. Plus wiecie, sama lubię pójść sobie do łóżka, nie śledzę co robi dowódca. Rozrywki to rozrywki. To, lub hazard."
    * Wincenty nie grał hazardowo. On lubił dziewczyny. I tępić Nelę. Upokarzanie jej też go bawiło.

Marianna weszła w interakcję z TAI Semla. Autoryzowała jej się i Semla odpowiedziała ze zrozumieniem. Semla słucha Marianny nad kapitanem. Marianna chce raport stanu systemów od momentu zniknięcia do teraz. Jeśli ktoś go zniknął, to mamy dwie opcje - albo ciało jest albo go nie ma. Jak jest, to GDZIE jest? Jak nie ma, JAK zostało zatomizowane? Więc - stan wszystkich śluz oraz stan systemu podtrzymywania życia - lifesupport który dostaje zwłoki będzie procesował. Też stan upłynniania śmieci. Wszystkie potencjalne metody pozbywania się trupa. A Adela chce wiedzieć - jeśli ktoś chciał go schować to musiał wiedzieć że wyłącza kamery. Czyli Marianna szuka ZMIANY w systemach Semli (spike itp.) po wyłączeniu kamer.

Tr Z (dane są wyjątkowo dobre jak na Semlę) +3:

* Vr: Marianna znalazła spike. W systemie podtrzymywania życia. System działa na zdecydowanie podniesionej mocy; dlatego nie daje rady z filtracją. Od tej pory CAŁY CZAS działa podkręcony.
* Vz: Zgodnie z danymi Semli nie ma żadnego śladu, żeby żywy Wincenty znajdował się na pokładzie Mrocznego Wołu. Nie je, nie pije, nie oddycha (zużycie life support) - jeśli jest 18 osób to lifesupport powinien wykazać. Semla po wyfiltrowaniu podniesionego poziomu pokazuje stabilne zużycie trochę niższe. Czyli Wincenty nie używa lifesupport statku.

Marianna pyta Baltazara o to, jak zachowałby się TAKI system lifesupport w połączeniu ze zwłokami. 

* B: "Mariś, ogólnie... masz ode mnie wykres. Robiłem symulację, to jedna z typowych." /wykres nie pasuje, acz ma spójną charakterystykę.
    * (Tu Baltazar tłumaczy czemu ukrywanie zwłok w lifesupport jest głupie)
        * obciążasz i to widać
        * nie masz jak tego wsadzić dyskretnie, zostawiasz 9999 śladów
* M: "Co innego może wygenerować TAKIE wykresy jak pokazuje Semla?"
* B: "Mariś, nie wiem, ja się... znam na zwłokach. Ale mogę... pospekulować jakie zwłoki zrobiłyby taki wykres"
* M: "Zawsze coś... a możesz się dowiedzieć?"
* B: "Ale to ze zwłokami jest ciekawsze niż lifesupport"
* M: "Ja potrzebuję info o lifesupport, nie o zwłokach."
* B: ":-("
* M: "Znajdziemy zwłoki kolesia to Ci się dam nimi pobawić"

.

* X: ogólna nieufność wobec inspektorek na statku (bo każdy ma coś na sumieniu)
* Vr: Baltazar poszuka wśród innych nerdów.
* V: odpowiedź SZYBKO.

A tymczasem Nadja -> Adela "Przypomniałam sobie coś. W sumie, wiem, że Wincenty znalazł coś... niewłaściwego należącego do advancera." (dalej nie mówi jego imienia). "Nie wiem co to dokładnie jest, ale wiem gdzie to jest. W jednej z wnęk. Dość dobrze schowane. To chyba broń, ale nie jestem pewna. Nie widziałam takiej. A jestem z security." "Wiem, bo to było nagrane na kamerach jak Wincenty to znalazł i skonfrontował się z advancerem. Potem się pokłócili, Wincenty uderzył, advancer oddał i Wincenty odpuścił, nazwał go tylko 'sick puppy'". /Nadja TROSZKĘ się obawia advancera.

Adela ogląda video. Na video nie widać DOKŁADNIE co znalazł Wincenty, ale na 100% pokrywają się rzeczy, które Nadja mówiła.

Tymczasem Marianna - za każdym razem kiedy kamery były wyłączone, przesłuchuje załogę (wszystkich) gdzie był Wincenty i najlepiej kiedy. Dzięki temu ma nadzieję dowiedzieć się CZEMU wyłączał i CO robił. Jednocześnie, Adela przydusza kapitana - czemu autoryzował wyłączanie kamer. Do tego Adela chciałaby przycisnąć advancera (co może nie być łatwe) oraz wejść do lifesupport i zobaczyć co jest grane.

Marianna - linia czasowa załogi. Informacja o ofierze. Czy coś się dzieje nie w porządku i wszyscy udają że nie widzą. Itp. Mapa ciepła załogi i mapa ruchów Wincenta.

Tr Z (uprawnienia + to co już wiesz) +3:

* Vr: Marianna dowiedziała się o stanie załogi
    * na tej jednostce nie dzieje się nic szczególnie złego - są przemyty, przekręty... - ale nie ma np. Syndykatu czy mrocznych narkotyków
    * Wincenty na początku był taki hardy, ale potem zmiękkł. Skupiał się raczej na dziewczynach. Dużo miał do dziewczyn.
    * Kapitan próbuje być uczciwy i łagodny. Raczej tak lekko podchodzi. Zależy mu na tej jednostce i uważa, że bez Wołu to Orbiter jest zagrożony (serio) bo śmieci polecą coś uszkodzą itp. Kapitan bierze statek i jego cel na poważnie. Ale jaki jest napuszony...
    * wszystkich ZDZIWIŁO zniknięcie Wincenta. W sensie - raczej ktoś by go skopał "za dziewczyny", ale nie tak.
* X: Marianna dowiedziała się o dużej ilości rzeczy nieregulaminowych "a tamten to robi X". Czyli próba wykorzystania inspektora by dowalić bliźniemu swemu.
* X: By coś można było dowiedzieć się więcej, Marianna musiała część tych drobnych głupot obsłużyć. Żeby nie było w raportach że coś. Mnóstwo drobnej, żmudnej roboty którą serio powinien był zrobić Wincent. Plus pokazuje to jak bardzo kapitan nie patrzy na załogę.
* X: Przy takiej ilości haków i małych rzeczy Wół będzie miał "audyt" i WSZYSTKO MA WRÓCIĆ DO NORMY. Surowe prawo Orbitera jak pisane. Wyrzutki dostaną wpierdol. Ale Marianna poznała ruchy Wincenta. Wie już, że on wyłączał kamery by odwiedzać albo Maję albo Nelę albo inną damę z załogi. Ale jest jednoznaczne info - jak dziewczyna mówiła 'nie', Wincenty dawał jej spokój.
    * Nieufność i paranoja na jednostce jest bardzo, bardzo wysoka. I wobec "innych" i wobec audytora.

Mając wiedzę jaką się udało zdobyć, Adela przyciska kapitana Tytusa

TrZ (uroda Adeli) +3:

* X: kapitanowi się wyraźnie Adela spodobała. Niestety bez wzajemności. Próbuje być elegancki i szarmancki. Wychodzi jak zwykle. JEŚLI Adela będzie czarująca i będzie grać w tą grę, to testy nie będą trudniejsze. (Adela "czego ja nie robię dla Orbitera...")
* V: Kapitan potwierdził, że wszystkie ruchy Wincenta były autoryzowane. Powiedział też o ostatnich ruchach jednostki - noktiański wrak, na pokładzie BYŁ advancer ale nic nie przyniósł itp. Kapitan SERIO nie ma nic a nic pojęcia; wyraźnie świetnie się dogadywał z szefem ochrony (na pewno nie on zabił).
    * "najpewniej było coś groźnego na statku noktiańskim i Wincenty poświęcił się by nas ratować" - dlatego nie pozwolił na to by advancer go tam szukał. "bo noktiańskie statki są z definicji niebezpieczne."
* Vz: kapitan odpowiedział na pięknolicą Adelę. Przyznał się z wielkim wstydem - "Wincenty przekonywał, ja się broniłem! Broniłem się, bo jak to wygląda, kapitan i członek załogi! Ale Wincenty przekonywał i Nela przekonywała i w końcu poszedłem z Nelą do łóżka".
    * kapitan mówił, że Nela jest nieśmiała, że on (Wincenty) będzie chronił ten sekret. I Wincenty przekonał go i było warto.
    * "ok, Wincenty traktował Nelę krótko i ostro, ale ona jest drakolitką. Wyraźnie to lubi."

BALTAZAR ma odpowiedzi dla Marianny. "Miałem rację Mariś, miałem! Nie potrzebowałaś info o lifesupport. Potrzebowałaś o zwłokach. Znalazłem sygnaturę zwłok która na to odpowiada. Ale powiedz mi - czy to sygnatura którą Ty widziałaś naprawdę?" "Jeśli tak, masz poważny problem." I zaczął tłumaczyć:

* Po pierwsze, to pasuje do sygnatury zwłok, ale "przetworzonych".
* Nie ma krwi. Krew została usunięta w większości.
* Ciało było wystawione na energię Esuriit przez pewien czas.
* Ciało było przemrożone w próżni.

Zdaniem Baltazara, są podstawy by wezwać wsparcie...

## Streszczenie

Z Mrocznego Wołu - 'śmieciarki' Orbitera holującego anomalny derelict noktiański - zniknął szef ochrony w niemożliwych okolicznościach. K1 wysłał dwie inspektorki by odkryły co się stało z potencjalnym dezerterem. Okazało się, że najpewniej 'dezerter' został zabity przez efekt magiczny a sam statek ma dość niegodne postacie. Sam zaginięty szef ochrony też miał coś do kobiet i źle traktował oficera naukowego Wołu, drakolitkę.

## Progresja

* OO Mroczny Wół: ogólnie bardzo duża nieufność wobec inspektorek bo każdy ma coś na sumieniu; co gorsza, przez zgłaszanie formalne potem będą wielkie czystki na statku.

## Zasługi

* Adela Myrias: inspektorka K1 szukająca informacji o dezercji szefa ochrony z Wołu; dużo ściągania danych z K1 dało jej obraz o znikniętym szefie ochrony i o załodze Wołu. Przydusza kapitana (który jest kobieciarzem) i dzięki swej urodzie dowiaduje się, że kapitan nie jest tak czysty wobec dziewczyn.
* Marianna Atrain: inspektorka K1 szukająca informacji o dezercji szefa ochrony z Wołu; skupia się na aspekcie samego szefa ochrony i tego kim był i jak się zachowywał. Dużo pracy z TAI Semla i z szerszą populacją załogi. Skupia się na ofierze a potem na danych statystycznych i grupowych, z czego zbudowała kontekst i mapę.
* Nadja Kilmodrian: p.o. szefa ochrony, sybrianka; chroni dobre imię szefa ochrony (bo by nie uciekł) mając chrapkę na jego miejsce. Wyraźnie nie lubi noktiańskiego advancera. Próbowała awansować przez łóżko, źle trafiła i wylądowała na Wole. Dość agresywna, ale nie jest najbystrzejsza ani szczególnie lojalna. Acz pracowita i solidna.
* Tytus Muszczak: kapitan Wołu; faeril; napuszony i dał się przekonać szefowi ochrony by molestować drakolitkę (Nelę). Autoryzował szefa ochrony do wyłączania kamer.
* Baltazar Kwarcyk: koordynator TAI i ruchu na K1; nerd zafascynowany zwłokami każdego typu i przyjaciel oraz kontakt Marianny i Adeli. Odkrył, że wykresy z lifesupport wskazują na potencjalne zwłoki po przetworzeniu przez energię magiczną, m.in. Esuriit.
* Juliusz Akramantanis: advancer noktiański na Mrocznym Wole, dekadianin; bił się z szefem ochrony i nazwano go 'sick puppy', niepokojący jak cholera. Przemyca coś w jednej z wnęk statku.
* OO Mroczny Wół: nieodświeżona, cuchnąca ciężka barka czyszcząca trasy w Zewnętrznym Pierścieniu Astorii; transportuje anomalny derelict noktiański i na pokładzie doszło do dezercji w niemożliwy sposób - zniknął szef ochrony. Wysłano dwie inspektorki by dojść do tego o co chodzi.

## Frakcje

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Pierścień Zewnętrzny

## Czas

* Opóźnienie: -311
* Dni: 3
