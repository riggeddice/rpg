## Metadane

* title: "Keldan Voss, kolonia Saitaera"
* threads: legenda-arianny
* gm: żółw
* players: fox, kapsel, kić

## Kontynuacja
### Kampanijna

* [220105 To nie pułapka na Nereidę...](220105-to-nie-pulapka-na-nereide)

### Chronologiczna

* [220105 To nie pułapka na Nereidę...](220105-to-nie-pulapka-na-nereide)

## Plan sesji
### Co się wydarzyło

* Iorus. Pierścień Halo. Znajduje się tam niewielka rozproszona kolonia Keldan Voss.
* Keldan Voss zbankrutowali. Zostali kupieni przez Pallidę.
* Pallida sprowadziła swoich ludzi, którzy działają nad stację należącą do Keldan Voss.
    * Drakolici z Klanu Mgły, mają dość nieprzyjemne zasady. Nie chcą redukować poziomu energii.
        * ewolucja przez magię
    * Pallidanie chcą zdobywać i harvestować kryształy Vitium.
    * Mała stacja, z rozproszonymi stacjami dodatkowymi. Wszędzie są generatory Nierzeczywistości.
* Faith Healer Keldan Voss nazywa się Kormonow Voss.
* Z uwagi na wysoki poziom magii, stare mechanizmy itp. mamy promieniowanie - lokalni drakolici i sprowadzeni pallidanie chorują i gasną
* Z uwagi na to, że drakolici nie chcą się integrować i współpracować, wszyscy mają obowiązkowe transpondery / "komórki".
* Drakolici mają swoich "Kroczących We Mgle", którzy mogą działać bez skafandrów.

### Co się stanie

* ?

### Sukces graczy

* Zatrzymanie Kormonowa przed całkowitą degeneracją bazy.
* Zatrzymanie Anniki Pradis przed eksterminacją / uciemiężeniem wszystkich drakolitów na Keldan Voss
    * Pomoc / ratunek dla drakolitów dotkniętych Korozją. Oni nie mogą żyć bez Nierzeczywistości przez Skażenie.

## Sesja właściwa
### Scena Zero - impl

* .

### Scena Właściwa - impl

2 tygodnie później - Infernia w ruinie. Tivr Skażony. Wszystko popsute. Ranni. Dewastacja. Wszystko co kochamy.

Co było w raportach do Kramera:

* Saitaer działa w okolicach Stoczni Neotik.
* Saitaer / Stocznia miała ixiońskie łapki w różnych jednostkach.
* Anomalia Kosmiczna Infernia. Ale grzeczna i kontrolowana.
* Klaudia dyskretnie podała informacje o stanie Eleny -> Arianna dała informację.
* Lista osób zabitych i rannych. Z okolicznościami. Przekierowanie większość winy na Saitaera i pułapki, mniej na Infernię i Dianę.
* Gdy Leona torturowała Eustachego, Diana była stabilna.

Kramer -> Arianna, Eustachy. Dywanik.

* K: "Czy Elena Verlen jest stabilna?"
* A: "Jest stabilna, ale musimy skupić się na Krypcie".
* E: "Nie"
* A: "Niezły pomysł"
* K: "Jak ona to przyjmie?"
* K: "Badania anomaliczne, badania psychologiczne -> Elena"
* K: "Zygfryd Maus będzie przydzielony na Infernię jako komisarz"

Kramer wyjaśnił mniej więcej czemu wysłał tam Zespół - bo drakolici wiedzą więcej na temat anomalizacji ludzi i magów. Powiedział o relacjach Pallidian i drakolitów z Keldan Voss.

Oki - Elena na rozmowę / ewaluację psychologiczną. Można ją "poprawić" i "skorygować". Elenę do doku Inferni na K1. Arianna -> Elena, że to może jej pomóc i jej nie zostawi. Arianna wrabia Elenę, że Klaudia oszukała wyniki. A naprawdę to nie oszukała by Elena weszła w to na luzie.

Arianna chce, by Elena poczuła się pewniej i żeby podczas testu wyszła jej natura.

ExZ+3:

* X: MANEWR: +2X.
* X: MANEWR: +2X.
* X: Elena przeszła test. Bez porządnej, tygodniowej obserwacji Elena jest enigmą dla nich.

Arianna dostała wyniki. Rozmawia z nią lekarz. Wygląda, że jest w porządku. Rekomendują zatrzymanie na obserwację. Rekomendacja odrzucona.

Zespół z radością leci na Keldan Voss. Jakim statkiem? OO "Kastor". Jest to fregata, mniejsza, szybsza. Dalekosiężna fregata. Na 30-40 aktywnych członków załogi.

Dotarliście. Dolecieliście spokojnie do Pasa. Jest sygnał SOS. Z bazy. Z centralnej części bazy. Nie działają generatory Memoriam. Jest na orbicie też Pallida Voss - korporacyjny statek kosmiczny. Nie jest aktywna.

Annika Pradis jest w bazie. Szczepan Kaltaben dowodzi na orbicie. On nie ewakuuje, bo Annika czeka na Orbiter. Eustachy przesłuchuje delikwenta.

TrZ+3: 

* X: Szczepan słucha się dowódcy. Jest lojalny Annice.
* Vz: "Oni złamali procedury". Nie ewakuuje, bo nie wie jak to naprawić. Nie boi się.
* XX: Wysyła to, co Annika chciała wysłać. Szczepan nie wyśle WSZYSTKIEGO. Szczepan ma przygotować ewakuację. Szczepan odmawia - musicie mieć zgodę Anniki.

Eustachy: "Należysz do tych co nie pójdzie do kibla bez rozkazu...". Niech koleś jest GOTOWY na ewakuację jak coś złego się stanie.

Klaudia analizuje dane wejściowe. Z timeline, historią i dziurami logicznymi. Czemu Annika tam jest? I jakie TAI jest na Keldan Voss?

TrZ+3

* V: Chronologia jest tyci inna. Po przybyciu Pallidan ZA BARDZO zwiększyli ilość generatorów. Przedtem populacja miała ich za mało. Ten statek jest na orbicie od dawna. Od momentu zakupu (5 mc temu) on lata, sprzedaje, przewozi ludzi.
* V: "Procedura" - drakolici PLUS ludzie, którzy byli sprowadzani mieli obowiązek noszenia komunikatorów, transponderów itp. I drakolici je powycinali. To FAKTYCZNIE wygląda na generatory. Po zniszczeniu jest MNIEJ, jest dużo za mało generatorów Memoriam. Ale ta sekta - ewolucja przez magię.
* V: TAI która jest w tej bazie nie pasuje wzorem do desygnowanej Hestii. Podobno to Hestia, ale to na pewno nie Hestia. Co coś innego. To wygląda bardziej na BIA-class niż TAI-class. TAI wyłączona?
* X: Nie ma możliwości jasnego stwierdzenia.

Oki, czas na Annikę. Eustachy -> Annika.

Annika powiedziała, że jak kupiono tą bazę - 30% odsetek śmiertelności dzieci. Wystawianie na kryształy vitium. Więc wymieniła generatory Memoriam. Zakazała starego kultu. Nie zainteresowała się kultem, nie wie czego to kult. Zaczęła sprowadzać ludzi z zew by poszerzyć pulę genową. Kazała te transpondery - to akurat zgodnie z zasadami korporacyjnymi. Annika ogólnie wierzy, że przyjdzie Orbiter (domyślnie: najlepiej Arianna) i NAPRAWI.

Baza ma niesamowicie duże skrzydło medyczne. Annika wyjaśniła, że to przez to, że oni umierają przez vitium.

NAJPIERW kontakt z Mateusem Sarponem, z drakolitów. Arianna, przedstawia się jako Arianna. Mateus wyjaśnił na czym polega problem - ludzie umierają. Nie ma opcji technologicznych, mają stary sprzęt. Więc bez siły Saitaera się nie da. (WHAT?)

Mateus wysłał Zespołowi dane z wydobycia - stare są X, nowe są 3X. Korporacja wprowadziła swój sprzęt, inwestycję itp. Ale nowy sprzęt wymaga Generatorów Memoriam. A w tym gasną drakolici...

Mateus powiedział o tych morderstwach - były delegacje i wymarły.

Plan jest taki:

* Korpo tu zostaje. Drakolici się znają na wydobyciu, ale nie na sprzedaży. Kogoś potrzebują.
* Drakolitów nie może być za dużo, by Saitaer nie urósł.
* Wdzięczność obu stron - korpo oraz drakolici.
* Niech Annika spuści, obniży wymagania.

## Streszczenie

Kramer wysłał załogę Inferni na Keldan Voss (stację pod kontrolą pallidan i źródło kryształów vitium), bo tam mogą więcej wiedzieć o anomalizacji ludzi i magów. Co więcej, jest poważny problem polityczny i techniczny - pallidanie zwalczają lokalnych drakolitów i kolonia ma problem typu wojna domowa. Na miejscu okazało się, że są CO NAJMNIEJ dwa stronnictwa a Klaudia uznała, że lokalna Hestia nie jest Hestią. Więc czym jest?

## Progresja

* .

### Frakcji

* .

## Zasługi

* Arianna Verlen: overridowała lekarza, lepiej dla Eleny jeśli poleci z nimi. Przekierowała Eustachego do rozmowy z pallidanami, sama skupia się na drakolitach z kultu Saitaera.
* Eustachy Korkoran: przesłuchał Szczepana Kaltabena; przypadkiem Annika uznała go za dowódcę sił Orbitera i ufnie oddała się pod jego dowodzenie.
* Klaudia Stryk: Wyłapywała nieścisłości w historii Pallidan i starała się zrozumieć sytuację. Doszła do tego, że lokalna Hestia to chyba nie Hestia. Więc co?
* Elena Verlen: przeszła przez przyspieszone testy psychologiczne, ale nie jest z nią w porządku. 
* Annika Pradis: pallidanka; dowodzi bazą. Strasznie naiwna; wierzy w DOBRO Orbitera, w Ariannę. Chce dobrze, ale: zakazała dawnego kultu, sprowadza ludzi z zewnątrz... wierzy, że wszyscy mogą się lubić.
* Szczepan Kaltaben: pallidanin; drugi po Annice; na orbicie. Trzyma się ŚLEPO rozkazów Anniki nawet za cenę śmierci dużej ilości ludzi i postawienia się Eustachemu.
* Mateus Sarpon: drakolita; drugi po Kormonowie; wyjaśnił Ariannie czemu kolonia się zbuntowała - żyją z woli Saitaera a przez generatory Memoriam ich ludzie umierają. Plus pallidanie na nich polują.
* Kormonow Voss: drakolita; faith healer; wielki nieobecny i overlord kultystów tej bazy.
* Zygfryd Maus: zostanie przydzielony na Infernię jako komisarz, kapelan i ogólnie rozumiane wsparcie duchowe.
* OO Kastor: Dalekosiężna fregata. Na 30-40 aktywnych członków załogi. Mniejsza i szybsza. Tymczasowo pod kontrolą Arianny.
* SP Pallida Voss: statek Pallidan krążący nad Keldan Voss, którego celem jest opanować sytuację ze zbuntowaną kolonią.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Iorus: gazowy gigant; ma pierścień halo i kilkadziesiąt księżyców. Najdalszy obszar Sektora Astoriańskiego uważany przez Orbiter za coś wpływem Orbitera.
                1. Iorus, pierścień: jest traktowany jako obszar cywilizowany przez Orbiter i otoczenie. Znajdują się tam liczne byty, kolonie i grupy "pod opieką Orbitera".
                    1. Keldan Voss: rozproszona kolonia kupiona przez Pallidę, harvestują kryształy Vitium. Przestały działać generatory Memoriam i nierzeczywistość miesza się z prawdą.
                
 
## Czas

* Opóźnienie: 15
* Dni: 3

## Konflikty

* 1 - Arianna chce, by Elena poczuła się pewniej i żeby podczas testu wyszła jej natura.
    * ExZ+3 XXX
    * MANEWR: +2X. MANEWR: +2X. Elena przeszła test. Bez porządnej, tygodniowej obserwacji Elena jest enigmą dla nich.
* 2 - Eustachy przesłuchuje Szczapana
    * TrZ+3 XVzXX
    * Szczepan słucha się dowódcy. Jest lojalny Annice. "Oni złamali procedury". Nie ewakuuje, bo nie wie jak to naprawić. Nie boi się. Wysyła to, co Annika chciała wysłać. Szczepan nie wyśle WSZYSTKIEGO. Szczepan ma przygotować ewakuację. Szczepan odmawia - musicie mieć zgodę Anniki.
* 3 - Klaudia analizuje dane. Czemu Annika tam jest? I jakie TAI jest na Keldan Voss? 
    * TrZ+3 VVVX
    * Po przybyciu Pallidan ZA BARDZO zwiększyli ilość generatorów. Drakolici PLUS ludzie mieli obowiązek noszenia komunikatorów, powycinali je. To FAKTYCZNIE wygląda na generatory. Po zniszczeniu jest MNIEJ, jest dużo za mało generatorów Memoriam. Ale ta sekta - ewolucja przez magię. TAI która jest w tej bazie nie pasuje wzorem do desygnowanej Hestii. Podobno to Hestia, ale to na pewno nie Hestia. Co coś innego. To wygląda bardziej na BIA-class niż TAI-class. TAI wyłączona? Nie ma możliwości jasnego stwierdzenia.