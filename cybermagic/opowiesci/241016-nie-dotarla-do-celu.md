## Metadane

* title: "Nie dotarła do celu"
* threads: serce-planetoidy-kabanek
* motives: integracja-rozbitej-frakcji, casual-cruelty, b-team, tozsamosc-kwestionowalna
* gm: żółw
* players: kić, fox, til, vivian

## Kontynuacja
### Kampanijna

* [241002 - Wsparcie dla Geisterjager](241002-wsparcie-dla-geisterjager)

### Chronologiczna

* [241002 - Wsparcie dla Geisterjager](241002-wsparcie-dla-geisterjager)

## Plan sesji
### Projekt Wizji Sesji

**WAŻNE**: Kontekst sesji i okolicy oraz Agendy itp DOKŁADNIE opisane w [Thread 'serce-planetoidy-kabanek'](/threads/serce-planetoidy-kabanek). Tam też znajduje się rozkład populacji itp.

* INSPIRACJE
    * PIOSENKA WIODĄCA
        * ?
    * Inspiracje inne
        * ?
* Opowieść o (Theme and Vision):
    * "?"
    * Lokalizacja: Okolice Anomalii Kolapsu, Libracja Lirańska.
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * integracja-rozbitej-frakcji: Pawilończyk vs Lodowiec i vice versa, Orbiter vs Libracja i vice versa, Warząkiewicz vs noktianka... i w tym wszystkim Zespół próbujący reperować Tezremont jak się da
    * casual-cruelty: Warząkiewicz wobec noktianki; znęca się nad nią dla przyjemności. Też nie wiadomo czemu tu uciekła i tu została.
    * b-team: Załoga na Tezremoncie. Nie dlatego, że tacy MUSZĄ być. Po prostu TERAZ tacy są i nikt nie wymaga od nich niczego więcej. Przekonali kapitana, że 60% mocy statku to 90% i się nie starają, choć potrafią.
    * tozsamosc-kwestionowalna: Noktianka imieniem Natalia. Jest savaranką, zachowuje się jako savaranka, ale nie do końca. Prosi o rozkaz na piśmie czy ma problemy z dużą ilością osób w mesie.
* O co grają Gracze?
    * Sukces:
        * .
    * Porażka: 
        * .
* O co gra MG?
    * Highlevel
        * .
    * Co uzyskać fabularnie
        * 
* Agendy
    * Gabriel Lodowiec (the-inquisitor)
        * wyplenić negatywne problemy z anomaliami i innymi siłami na tym terenie

### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

KONTEKST:

* Zgodny z THREAD

CHRONOLOGIA:

* Lodowiec robi co może by znaleźć przyczyny anomalii i problemów z ludźmi
* Lodowcowi kończą się surowce, nie dostał wsparcia

PRZECIWNIK:

* brak, anomalia?

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

Faza 0: TUTORIAL

* Tonalnie
    * .
* Wiedza o świecie
    * Planetoidy, ubóstwo, problemy finansowe
    * Jakiego typu osobą jest Gabriel Lodowiec
* Kontekst postaci i problemu
    * .
* Implementacja
    * Planetoida wysyła SOS
    * Lodowiec przygotowuje Geisterjager, atakują planetoidę. Dowodzi osobiście, z orbity. Ludzie zabarykadowani
    * Bierze część wody i zapasów. (pokazuje graczom że to problem)

## Sesja - analiza
### Fiszki
#### Orbiter, OO Geisterjager, fregata dowódcza

* Gabriel Lodowiec: komodor Grupy Wydzielonej Mrówkojad Lirański oraz kapitan OO Geisterjager
    * Commander: komodor Orbitera; dowodzi 2 statkami. Dobrze przydziela ludzi do zadań. Dobry taktyk. KIEPSKI w tematy HR.
    * Ekspresyjna Monomania (Light Yagami): bardzo wybuchowy, ale przywiązany do zasad i procedur. Zrobi to, co trzeba, by wygrać i wygra WŁAŚCIWIE. (O- C+ E0 A0 N+)
    * Próżniowiec, Uprzedzenie Do Grupy (noktianie), Weteran: bardzo doświadczony, nie lubi noktian. Ale jeszcze bardziej nie lubi przegrywać misji.
    * Echo Porażki: za Lodowcem do dzisiaj idzie fama "tego, który zrobił przerażającą mowę". Jest osobą, którą się straszy noktian.
* Dariusz Mordkot: pierwszy oficer, marine, dowodzi grupą uderzeniową.
    * Solutor: marine, uzbrojony i dobrze wyposażony
    * Niezależny Wizjoner (Miorine): bierze zadanie i je wykonuje. Pomysłowy, gotowy do nietypowych sytuacji. Lubi się chwalić i pokazywać klasę swych ludzi. (O+ C+ E+ A- N+)
    * Wytworna Elegancja: zawsze doskonale ubrany, w nieskazitelnym mundurze. Śliczne zęby i szeroki uśmiech. No z plakatu rekrutacyjnego.
* Renata Kaiser: oficer komunikacyjny
    * Jest Druga Szansa: wybacza, wierzy, że każdy - nawet noktianin - ma prawo do drugiej szansy. Sympatycznie neutralizuje Lodowca. (O+ C0 E+ A+ N0)
* Ofelia Miris: marine, druga dowodząca grupą szturmową
* OO Geisterjager: fregata dowódcza, 80 osób

#### Orbiter, OO Tezremont, szybka korweta adaptacyjna

* Arnold Pawilończyk: kapitan OO Tezremont pod Gabrielem Lodowcem; około 23 lata
    * Optymistyczny Entuzjazm (Bastian): Arnold jest optymistą i wierzy, że jak zrobi się wszystko dobrze to się jakoś ułoży. Odważny i skłonny do ryzyka. (O+ C0 E+ A+ N-)
    * Trochę za Młody: Arnold jest niestety bardzo wrażliwy na ładne dziewczyny i nieco za bardzo zależy mu na tym co pomyślą inni. Chce wszystkich zadowolić.
* Mateusz Knebor: pierwszy oficer OO Tezremont; 
    * Bezwzględny Mastermind (Shockwave): jego celem jest udowodnienie, że Pawilończyk nie jest dobrym kapitanem i spełnienie celu Orbitera. (O+ C+ E0 A- N0)
    * Kontakty z Syndykatem: nie wie o tym, ale ma informacje oraz dodatkowy sprzęt itp. od ludzi Syndykatu. To oni pokazali mu bezużyteczność Arnolda.
* Natalia Luxentis: cicha noktiańska advancerka, która dołączyła do Orbitera nie widząc lepszej opcji; około 33.
    * Advancer, Inżynier: lepiej czuje się w kosmosie niż w statku czy na planecie, nie jest może dobra w walce, ale świetnie radzi sobie z naprawami w kosmosie.
    * Marzyciel (Luna): ślicznie rysuje swoje światy i swoje historie, rzadko rozmawia z innymi. Ma bardzo neutralną minę, pragnie zadowalać innych. Lubi sprzątać. (O+ C- E- A+ N-)
    * Próżniowiec, Savaranka, Ten Obcy: noktiański nabytek Orbitera; Natalia próbuje być maksymalnie przydatna na pokładzie, ale unika kontaktu z innymi jak może. Overloaded.
* Sebastian Warząkiewicz: ogólnie 'dobry koleś' grupy, inżynier, kompetentny i lubiany, acz bardzo intensywny.
    * Inżynier, kowboj: dobrze strzela, dobrze walczy i spełnia się jako inżynier załogi. Zawsze pierwszy rusza w bój.
    * Ognista Pasja, Uprzedzenie do grupy: chce się bawić, tańczyć i śpiewać. A przy okazji wkopać noktianom, bo zniszczyli majątek jego rodziny i utknął tutaj. "Główny bohater". (O+ C- E+ A- N+)
    * Bodybuilder, Nieformalny Przywódca: Sebastian UWIELBIA ćwiczenia fizyczne i praktycznie ćwiczy cały czas. Do tego zna mnóstwo opowieści o Mroku Noctis i świetnie opowiada.
* Czesław Truśnicki: marine Orbitera, przypisany do OO Tezremont.
    * Solutor: świetny marine i żołnierz. 
    * Opiekuńczy Mentor: z natury dba o harmonię i rozwój innych, dba o każdego członka swojej załogi. Nie uważa, że cierpienie jednostki jest warte sukcesu grupy.
* Grzegorz Dawierzyc: drugi oficer i inżynier
    * Kompetentny oficer, choć niszczy morale podwładnych. Na pewno dowodzi inżynierią i silnikami na OO Tezremont.
    * Wieczny Maruda: narzeka na wszystko, ALE jest lojalny kapitanowi i Orbiterowi. Po prostu trudno z nim wytrzymać. (O- C+ E? A- N+)
    * Istnieje powód, czemu aż tak bardzo narzeka. Nie wiemy jednak jaki. PLACEHOLDER.
* Fred Topokuj: marine Orbitera, przypisany do OO Tezremont.
    * Klanowiec: tylko Orbiter; nikt inny nie ma znaczenia
* OO Tezremont: szybka korweta adaptacyjna, 22 osoby
    * zdolna do poważnej zmiany wyglądu i posiada więcej skrytek niż powinna; działa jak QShip.

### Scena Zero - impl

Kapitan Mariusz Grządzik, ognisty pasjonat, ma za zadanie dostarczyć Isę do reszty Zespołu.

Isa miała zostać doprowadzona do odpowiedniego miejsca by spotkać się ze swoimi przyjaciółmi. Ale coś poszło nie w porządku. Isa potrafi analizować ruchy ciał niebieskich i frachtowiec którym leciała zwyczajnie się nie zatrzymał tam gdzie powinien. 

Isa natychmiast udaje się na mostek naprawić tą sprawę.  

Isa podchodzi "coś jest mega nie tak i kapitan zrobi marine jesień średniowiecza". I młody szczyl. I Isa obrabia go jako weteranka.

Tp+3:

* Vr: Żołnierz wszedł na mostek.

Isa nie czeka aż młody żołnierz rozwiąże sprawę sam - weszła na mostek, bo młody zwyczajnie zostawił drzwi otwarte. Kapitan jest wściekły na mostku. tym bardziej jak zobaczył, że Isa mówi prawdę i ma rozkazy pojawić się na planetoidzie. ale jak ją tam dostarczyć? Delta-V przecież kosztuje. 

Isa proponuje by kapitan dał jej kapsułę ratunkową i ją tam wysadził. Ale Isa jest wartościową terminuską; kapitan na to się kategorycznie nie zgadza. 

Tr Z+4 +3Og:

* X: Kapitan KATEGORYCZNIE nie zgadza się na to. +2Og +1Vg
    * Kapitan ma rację i nikt tam na nią nie czeka. I on wyszedł na głupka.
    * "Pan mnie zgubił"
* Vr: Kapitan się zgadza. Ona leci promem, spowolnienie, sama czeka.

INNE MIEJSCE:

Komodor Lodowiec rozmawia z Lucjuszem. Lucjusz, odpicowany mundur (10 razy droższy; mam kasę i nie zawaham się jej użyć).

Komodor Lodowiec dostał informację o tym, że jest jedna czarodziejka, którą należy odebrać z planetoidy i która się szarogęsi. Dlatego chciał spotkać się z Lucjuszem. gdy Lucjusz powiedział Lodowcowi kim jest Isa, Lodowiec się poważnie zmartwił. mają czarodziejkę na potencjalnie noktiańskiej planetoidzie. To jest coś co wymaga natychmiastowego działania. Misja ratunkowa. 

Tymczasem Isa nie znalazła nikogo, kto by na nią czekał. Przekonała swoim autorytetem terminuski towarzyszącym jej żołnierzom, że mają wracać na frachtowiec. Sama udała się do najbliższego baru - jest dokładnie jeden na tej górniczej planetoidzie. 

Stwierdziła, że warto zaznajomić się z miejscowymi i zrobić dobre wrażenie; w końcu są oddelegowani do tej okolicy a jak doświadczenia z Neikatis pokazały, miejscowi bywają przydatni. Napiła się w barze, nieświadoma tego że została otruta. druga strona nie jest też świadoma tego że ma do czynienia z magiem - biologiczna struktura maga jest na tyle odmienna, że trucizna nie działa jak powinna. 

Tymczasem Lucjusz próbuje dogadać się z załogą Tezremonta; Pierwszy oficer mówi, że lecą na pełnej mocy, ale wyraźnie tego nie robi. 

Annabelle wpływa na oficera w inżynierii (Grzegorza Dawierzyca), znanego narzekacza. I chwali go “ja widzę jak ciężko tu pracujesz”, próbując wpłynąć pozytywnie na jego morale i na morale jednostki. Próbuje przekonać tego oficera, że Isa (przyjaciółka) jest w dużym niebezpieczeństwie i Tezremont musi lecieć bardzo szybko. Annabelle wyciąga z narzekania rzeczy które on uważa że robi świetnie. "Szanuję za wysiłek jaki wkładasz"!

TrZ+4+3Og (plotki):

* Vz: załoga ma szczękę przy ziemi. (narzekacz "jesteś miłą osobą" i do załogi "panowie, weźcie się do pracy, damy dobrze radę")

Isa orientuje się, że została otruta; biega po małej planetoidzie ze stymulantami bojowymi, by neutralizować truciznę. "Czuje" oczy przypatrujące się niej.

Pawilończyk dotarł komunikatem do Isy. Ona powiedziała, że jest otruta ale sytuacja pod kontrolą. Sylwia Mazur poleciała z marines i przejęła Isę z planetoidy; zdecydowała się nie robić nic innego. Szkoda problemów dla Orbitera. Isa na pokładzie.

Lucjusz - lekarz - używa magii stabilizującej Isę.

Czar: organizm Isy bardzo aktywnie zaczyna zwalczać toksyny, w przyspieszonym tempie przez wątrobę, organy wewnętrzne, substancje chemiczne. Wzmocnienie jej naturalnych cech.

TpM + 3Ob +3:

* V: Detoksyfikacja

Annabelle, do Dawierzyca, pokazała "temu szeregowemu oczy się zaświeciły po Twoim komplemencie, lepiej pracuje" - udowodniła, że podejście Dawierzyca usprawniło pracę załogi i warto to robić częściej. Nadal stabilizuje i usprawnia załogę.

Tr +4:

* V: Dawierzyc chwali tego chłopaka (publicznie), powodując atak serca u innych.
    * Annebelle zobaczyła CIEŃ na twarzy Dawierzyca. Coś mu to przypomniało lub uzmysłowiło. Coś go zmartwiło. Ale co?

### Sesja Właściwa - impl

Lucjusz znowu znalazł się na mostku Tezremonta, zaproszony przez kapitana Pawilończyka. Pawilończyk zorganizował ucztę - to znaczy, nie tylko konserwy ale tak samo normalniejsze jedzenie. Dlaczego? Udało się pozyskać Isę. Dla większości będzie jedzenie, ale nie dla siebie. Dlaczego? Kapitan zjada ostatni. 

"Każdy na statki ma swoją rolę, to nie fair wobec załogi, kapitan bierze na klatę co najważniejsze". (spojrzenie z uznaniem od pierwszego oficera)

Na mostku wiadomość do Pawilończyka - komodor Lodowiec chce z nim rozmawiać, prywatnie. Pawilończyk stwierdził, że nie ma sekretów przed załogą. Lodowiec zaznaczył, że jak najbardziej, są sekrety przed załogą i to jest dla ich dobra. Pawilończyk jest pewny swoich oficerów. Lucjusz wszedł kapitanowi w słowo i wyjaśnił, że czasami są sekrety których kapitan powiedzieć nie może bo to jest nieuczciwe wobec załogi. 

TrZ+3:

* V: Pawilończyk to akceptuje.
* V: Lodowiec docenia i uważa, że Lucjusz ma być na spotkaniu Pawilończyk - Lodowiec.

Co się okazało - Lodowiec bardzo martwi się za małą populacją ludzi w załodze Tezremont wobec ilości magów na korwecie. Ale zgodnie z rozkazami nasza czwórka (Sylwia, Annabelle, Lucjusz, Isa) mają działać jako jeden zespół, więc Lodowiec nie jest w stanie ich sensownie rozdzielić. Pawilończyk uważa, że nie ma problemów. Lodowiec WIE że będą problemy. 

Z pomocą przyszedł Lucjusz; będzie zaprowadzony jakiś osłabiacz visiat oraz będą inne środki; Annabelle coś wymyśli. Lodowiec jest sceptyczny, ale nic z tym nie może zrobić. 

Dodatkowo Lodowiec przypomniał, że są pewne problemy ze Skażonymi przez Anomalię ludźmi. Teraz jak są magowie, można się tym zająć. Zdaniem Lodowca, noktianka może coś wiedzieć.

* Pawilończyk: "Przesłuchajmy noktiankę"!
* (spoiler) Przesłuchana przez Pawilończyka noktianka nic nie powiedziała. Pawilończyk jest po prostu za łagodny.

Tymczasem Isa była bardzo niezadowolona z tego powodu, że mogła zaginąć gdzieś na planetoidzie. Wysłała mail z opierniczem wyjaśniającym sytuacją. Mail dostała, oczywiście, Renata. Która nic z tym zrobić nie może. 

Tr+2:

* V: Renata COŚ znalazła i TO JEST PRAWDĄ.
* X: Renata OPRÓCZ TEGO znalazła spisek, który jest całkowicie fikcyjnym spiskiem.
* V: Renata POTRZEBUJE Isy do rozwiązania tego spisku. Tego prawdziwego.

Renata wyciągnęła dłoń w stronę Isy. Zdaniem Renaty to jest spisek; ktoś próbuje sprawić, by Lodowiec bardzo źle wyglądał. Pytanie, ile osób w ten sposób zniknęło w okolicach Anomalii Kolapsu. Isa była pierwszą o której wiadomo, ale nie znaczy to, że jedyną. 

Renata pomoże Isie. Ale w zamian - “Isa weźmie Annabelle na smycz”. Bo zdaniem Renaty Annabelle jest niesamowicie niebezpieczna, zwłaszcza wobec Lodowca czy Pawilończyka. Isa niczego nie obiecując wstępnie zawiera sojusz z Renatą by chronić Lodowca i zbadać kto stoi za owym spiskiem. 

Tak czy inaczej, Renata na to spojrzy.

Lucjusz bada górnika (Anomalnego Górnika z poprzedniej opowieści). 

Badania biologiczne, chemiczne a potem z tych badań magicznie a potem z tego wytłumić instynkty i nawiązać kontakt. Dostać się do tego, czym "był". Badania podstawowe mówią Skażenie magiczne. Na pierwszy rzut ingerencje chirurgiczne które w podobnym czasie co anomalizacja.

"Jaki jest jego główny motor, czemu chce kopać, jaki bodziec pozwoli nim sterować"?

Tr+Z+3:

* V: Górnik jest interesujący - koleś jest "dobrym pracownikiem". Jego motywuje praca. Chce być przydatny. Narzędzie powinno być przydatne. Anomalia to zapętliła. Anomalia i chirurgia - zmieniły osobę w narzędzie. Seria randomowych ruchów by zadziałało.
* X: Górnik umiera na stole operacyjnym. Nie da się go już uratować. (+2Vg) (+M)
* Vz: Dobry sprzęt: masz detektor; jesteś w stanie rośliny - kwiat w klapę munduru (kwiatostan). które reagują na to. (plus, inne egzemplarze). Energie: Ixion + Praecis. To nie jest "narzędzie magiczne w rękach doktora". To dyletant mający anomalię. MNIEJ WIĘCEJ: miesiąc temu A GÓRNICY POZYSKALI ICH 3 tygodnie temu.

Ok. Chcemy się dowiedzieć więcej. Do Lodowca - prośba o kolejny obiekt badawczy (Anomalnego Górnika). Lodowiec prześle.

Tymczasem Sylwia zapoznaje się z marines, chce opowieści "from the trenches". I co oni wiedzą o całej tej sytuacji. "Plotki które warto znać"

Tr Z+3:

* V:
    * "Orbitera tu nie lubią, ale pieniądze Orbitera tak. Kapitan Pawilończyk ma kochankę na planetoidzie Kabanek."
        * Kochanka Pawilończyka: Illumina Dorsz. Idolka na planetoidzie Kabanek. The best of the best.
    * Tu jest niewolnictwo. Orbiter nie akceptuje niewolnictwa. Nikt nie atakuje Orbitera, ale nikt nie gada z Orbiterem.
    * Orbiter ma FATALNĄ sławę, a zwłaszcza Lodowiec.
    * Kapitan Pawilończyk jest lubiany przez załogę.
    * Lokalsi nie uważają tej jednostki za statek Orbitera, nie robi rzeczy które robi Orbiter. (nie do końca marines wiedzą CZEMU tak jest)
    * Jeśli Ci źle, możesz postraszyć noktiankę. Takie hobby dla niektórych.
    * To nie jest jedyny raz ani pierwszy raz. Wykorzystuje się anomalie do robienia rzeczy ludziom. I Lodowiec trochę walczy z wiatrakami.
    * Podobno część dziewczyn pracujących SAME sobie to robią; same niszczą swoje umysły przy niektórych klientach.
    * NIGDY jednostki Orbitera tutaj nie ucierpiały naprawdę. Lodowiec robi dobrą robotę ekranując Pawilończyka od niebezpiecznych akcji.
* -> Tp+3: X: Marines trzymają dystans, bo uważają, że Sylwia jest wysłana przez Orbiter na tajną misję; czemu Orbiter by wysyłał osobę klasy Sylwii?

Annabelle się krząta po statku, patrząc jak działa praca na pokładzie. Gdzie dałoby się usprawnić - ale nie ciężej a mądrzej.

* Noktianka pracuje cały czas. Śpi, je konserwę, pracuje, odpoczywa, pracuje, nie pojawia się.
* Jednostka jest 60% efektywności. Ludzie przekonali Pawilończyka, że to jest optymalna moc statku, bo szkoda ciężko pracować.

Annabelle używa umiejętności administracyjno-naukowych. Chce być tą osobą, która dała załodze "masz mniej roboty". Podbudować jako dobra dusza na pokładzie statku.

TrZ+3:

* V: Dzięki Annabelle oni mają mniej roboty - doceniają to. (light glow).
* Vr: Annabelle jest lubiana wśród załogi - ułatwiła wielu ludziom pracę, mimo że nie musiała.

I Annabelle decyduje się spotkać z noktianką i dowiedzieć o niej jak najwięcej.

* V: Annabelle może nie porusza się najciszej ale nie musiała. Bo na noktiankę krzyczy głośno Warząkiewicz.

Warząkiewicz wyraźnie zastrasza oraz terroryzuje przerażoną noktiankę. Savaranka się kuli i próbuje wyglądać na niegroźną i bezproblemową. Warząkiewicz na nią krzyczy i żąda od niej informacji o tym, gdzie schowała rzeczy i co próbuje zrobić. Annabelle staje w obronie noktianki i pyta Warząkiewicza czemu tak Się zachowuje wobec członka załogi.  Warząkiewicz mówi, że noktianka jest agentką i złodziejką. Że może noktianka dała radę przekonać kapitana, ale jego nie przekona. Annabelle go podpuściła i Warząkiewicz powiedział wyraźnie, że uważa kapitana Pawilończyka za zbyt łagodnego, niekompetentnego, ale wartego wzmacniania. Warząkiewicz technicznie wykopał na siebie grób i wie o tym. Zostawił Annabelle i noktiankę same, ostrzegając savarankę, że na tym się nie skończy. znajdzie jej kryjówki. Udowodni, że savaranka jest problemem. 

 Noktianka usiadła na chwilę na ziemi i zaczęła oddychać powoli by się uspokoić. Annabelle cierpliwie czeka o czym kazała noktiance iść ze sobą do mesy. Noktianka nie czuje się komfortowo w towarzystwie tak dużej ilości osób (Co jest bardzo zaskakujące dla Annabelle; savaranie są bardzo grupową kulturą).  

Annabelle wzięła noktiankę do sali konferencyjnej i dała jej zjeść smaczne jedzenie. Savaranka cicho odpowiada na pytania Annabelle; nie chce niczego mówić, działa na poziomie rozkazów.  

Wysokopoziomowy skan intencji savaranki. Co chce robić w tym czasie. I potrzebuje pomocy w laboratorium.

Tr +3:

* Savaranka nie wygląda na to, że ma złą intencję. Ale jest niesamowicie ostrożna - ona jest przekonana że ktoś jej zrobi krzywdę. I nadal tu jest. Ona panicznie boi się opuścić ten statek.
* Savaranka czyści - to MUSI być czyste, ale ona czyści bo ona... lubi jak jest czysto. Podobają jej się usprawnienia.
* "Usprawnienia to rozkazy, tak? Rozkazy są na piśmie? Pismo mogę okazać?"
* "Nie umiem pracować w laboratorium".

...

Dotarliście na Kabanek. 

A Isa - wiadomość od Renaty. "Potwierdź - jesteś czarodziejką?" "Tak."

* Renata: Ktoś uznał, że da się Ciebie... przechwycić. Komodor Lodowiec nie dostał rozkazów. Nikt nie wiedział, że tu będziesz. Nie wiem, czy Twoje rozkazy są prawdziwe.
* Renata: "Komodor był celem spisku"

Isa decyduje się wejść we współpracę z Renatą, ale na swoich zasadach:

Tr +4:

* X: Lodowiec dostał informację o spisku Renaty itp. (wobec Annabelle) i ma CRINGE LV 7! Nie wie co myśleć.
* V: Renata uważa, że komodor był celem spisku i to znaczy, że Isa jest osobą, która też jest ofiarą. Można jej bardziej zaufać.
* Vr: Renata uważa, że można zaufać Isie, bo ona zna rzeczy, jest doświadczona i ona nie jest TAKA JAK Annabelle.
* (+2Vg): X: Renata oczekuje, że Isa opanuje Annabelle jako PRAWA I DOJRZAŁA część zespołu.
* (poddaję konflikt) Renata poszuka czy ktoś jeszcze by ucierpiał itp. Czy Isa była pierwsza.

Lucjusz. Czas użyć magii i chirurgii do pracy z kolejnym Anomalnym Górnikiem. Lucjusz NIE chce tego robić destruktywnie; chciałby, żeby nieszczęśnik przetrwał. Lucjusz będzie delikatniej traktował NieGórników.

Cel - wyciągnąć ich ostatnie wspomnienia, emocje, zapisy pamięci - interfejs pamięci...

Tr+ZM+3+3Ob+3Og (nie dość ludzi):

* V: Masz część obrazów i wspomnień. _Exemplis_
    * niektórych może da się uratować; 
    * (może da się połączyć ich umysły. Suplementować umysły wzajemnie - powiększyć - połączyć krwiobiegi) +Vg+Vb
* V: Umysły zintegrowane. Rozmontowanie tego będzie trudne, ale masz dużo szerszy obraz sytuacji.
    * Oni się wcześniej nie znali. Pochodzą z różnych planetoid. Zostali porwani i sprzedani.
    * Statek kosmiczny. Sygnaturę "świni".
    * Lecieli -> Anomalia Kolapsu. Tam jest "cyborg"
    * Ostatnie przyjemne wspomnienie: gdzie zaczęła się spirala mienia przerąbane
        * Gangi ludzi, "statek świni", mniej niż 20 osób. MNIEJSZE BAZY.
    * Masz informacje o DWÓCH KONKRETNYCH PLANETOIDACH.
* Vb:
    * Dwa ludzkie ciała połączone systemem dializy
    * Rurki uległy zastąpieniu - naczynia krwionośne zaczęły się rozrastać jak korzenie roślin; na ich naczynia, łącząc ze sobą, sieć poza ciałami, symbiotyczna
    * Maszyneria pompująca, układ krwionośny wrósł w część technologiczną i wytworzył całą powierzchnię; punkty zaczepu dla potencjalnie nowych obiektów
    * -> +PP przy pracy z tą Anomalią
    * Informacja GDZIE I CO. Chcesz wiedzieć kto i jak za tym stoi.

Lucjusz wydobył informację: statek który doprowadził do przekształcenia Górników w NieGórników nazywa się "Szczęśliwa Świnka". To jest transportowiec. Ale naprawdę to szybka jednostka, która maskując się przemyca ludzi.

Jako, że laboratorium na Tezremoncie stało się dziwnym, anomalnym miejscem, Lucjusz poprosił kapitana o odcięcie pomieszczenia od załogi. By to nie wyglądało źle. Kapitan potwierdza.

## Streszczenie

Terminuska Isa miała dołączyć do zespołu, lecz nikt na nią nie czekał; ulega otruciu w miejscowym barze. Przechwycił ją Tezremont, gdzie spotkała się z resztą zespołu. Renata jest poważnie zaniepokojona tym co się dzieje i odkrywa, że Isa nie jest pierwszą osobą która "potencjalnie zniknęła", uważa to za spisek przeciw Lodowcowi i wchodzi we współpracę z Isą za opanowanie Annabelle. Annabelle stabilizuje załogę i poznaje sekrety noktianki. Sylwia dowiaduje się jak wygląda stan Orbitera i Pawilończyka a Lucjusz badając Anomalnych Górników, odkrywa, że zostali oni przekształceni przez Anomalnego Chirurga i wszyscy przeszli przez statek "Szczęśliwa Świnka". Anomalne pomieszczenie na Tezremoncie zostało odcięte przez Pawilończyka. Otwartym zostaje ryzyko nadmiaru magów na ludzką populację Tezremont...

## Progresja

* Lucjusz Jastrzębiec: zarówno Lodowiec jak i Pawilończyk uważają go za kompetentnego i mu ufają.
* Isa Całujek: zobowiązana przez Renatę Kaiser do kontrolowania Annabelle by ta nie robiła nic głupiego i nie podrywała Lodowca.
* Sebastian Warząkiewicz: wrogość do Annabelle Magnolii; uważa Annabelle za niebezpieczną i wrogą Orbiterowi, sympatyczkę noktian
* OO Tezremont: na jego pokładzie powstało technomagiczne, organiczne laboratorium badawczo-lecznicze Anomalnych Górników pod kontrolą Lucjusza.
* OO Tezremont: niebezpiecznie wysokie stężenie pola magicznego wobec populacji ludzkiej

## Zasługi

* Sylwia Mazur: Przeprowadziła rozmowy z marines, zbierając informacje o nastrojach i plotkach; osłoniła się przed oskarżeniami marines, że jest agentką Orbitera w mrocznych celach.
* Annabelle Magnolia: Buduje morale Tezremont i odbudowuje Tezremont jako jednostkę; wpłynęła na Dawierzyca i skonfliktowała się z Warząkiewiczem o noktiankę.
* Lucjusz Jastrzębiec: Ustabilizował Isę po otruciu a potem przeprowadził badania na Anomalnych Górnikach, odkrywając powiązanie ze "Szczęśliwą Świnką" oraz tworząc anomalne laboratorium na Tezremont.
* Isa Całujek: Przetrwała truciznę na planetoidzie; zawarła sojusz z Renatą by unieczynnić spisek przeciw Lodowcowi (?).
* Mariusz Grządzik: Kapitan frachtowca; stanowczy i lekko wybuchowy dowódca; mimo niechęci dostarczył Isę na planetoidę. Wbrew niej zapewnił jej bezpieczeństwo (dezorganizując swój lot) i dał znać Lodowcowi że ten spieprzył.
* Arnold Pawilończyk: zorganizował ucztę z okazji przybycia Isy, dba o morale załogi i serio robi jak najlepiej dla nich może. Ma kochankę na Kabanku. Odizolował anomalne laboratorium na Tezremoncie do wyłącznej kontroli Lucjusza.
* Renata Kaiser: odkryła istnienie spisku uderzającego w Lodowca i "znikającego" ludzi. Nawiązała współpracę z Isą za kontrolę nad Annabelle. Przygotowana na długofalowe śledztwo.
* Gabriel Lodowiec: szybko zainicjował misję ratunkową Isy i ostrzegł o nierównowadze magów i ludzi na Tezremont. Skupia się na ratowaniu ludzi przekształconych przez Anomalię i na ekranowaniu Pawilończyka od problemów.
* Grzegorz Dawierzyc: upiornie narzekający oficer inżynierii, który okazuje się potrafi być motywujący (podpuszczony przez Annabelle), ale to coś mrocznego mu przypomniało.
* Natalia Luxentis: obsesyjnie dba o czystość i bezpieczeństwo. Nic nie powiedziała Pawilończykowi gdy ją przesłuchiwał, pozycjonuje się jako savaranka ale... chyba nią nie jest. Mimo braku zaufania jest lojalna załodze Tezremont.
* Sebastian Warząkiewicz: agresywnie zaczepia noktiankę oskarżając ją o szpiegostwo i kradzież; sprowokowany przez Annabelle PRAWIE ją uderzył i podważył kompetencje Pawilończyka. 
* SC Szczęśliwa Świnka: Transportowiec odpowiedzialny za przewóz i przekształcenie Górników; przemytnicza jednostka, maskująca się pod pozorem transportowca
* OO Tezremont: szybko uratował Isę z planetoidy górniczej gdzie była otruta; potem miejsce badań Anomalnych Górników. Na jego pokładzie powstało laboratorium Lucjusza.

## Frakcje

* .

## Fakty Lokalizacji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Libracja Lirańska

## Czas

* Opóźnienie: 2
* Dni: 1

## Inne
