## Metadane

* title: "Szafirianin znalazł starą minę"
* threads: brak
* motives: the-devastator, echa-inwazji-noctis, skazenie-magiczne-maszynerii, presja-czasu, dom-w-plomieniach, energia-interis, kaskadujaca-fala-emocji
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [241027 - Szafirianin znalazł starą minę](241027-szafirianin-znalazl-stara-mine)

### Chronologiczna

* [241002 - Wsparcie dla Geisterjager](241002-wsparcie-dla-geisterjager)

## Plan sesji
### Projekt Wizji Sesji

* INSPIRACJE
    * PIOSENKA WIODĄCA
        * [L'ame Immortelle - Love is Lost](https://www.youtube.com/watch?v=7BbQKphWU0k)
        * "In our time where love is gone | The harvest of souls it has begun | The dark clouds from above come down | Satan came to claim his crown"
        * W dniu, w którym Interis opanowało statek wszystko zostało zniszczone. "It's not the evil from down below... it's due to the evil in us all"
    * Inspiracje inne
        * brak
* Opowieść o (Theme and Vision):
    * "_Bezpieczeństwo zależy od człowieka i ludzkiej woli._"
        * To nie mina, nie katastrofa zniszczyła Szafirianin. To ludzka słabość i poddanie się Rozpaczy. Nasze słabości.
        * Są jednostki, które stają naprzeciw Interis. Te jednostki mogą uratować wszystkich.
    * "_It's not the evil from down below... it's due to the evil in us all._"
        * Sama mina Interis nie doprowadziłaby do zniszczenia wszystkiego. Niestety, wielu ludzi na to poszło.
        * Czy da się wybaczyć po tym wszystkim, co tu się stało?
    * Lokalizacja: Transfer Astoria - Neikatis
* Motive-split
    * the-devastator: anomalna mina astoriańska, która zboczyła z kursu i pragnie zniszczyć wszystko co jest w stanie zniszczyć. Tu: "SC Szafirianin". Dotknięta Interis.
    * echa-inwazji-noctis: Anomalna mina Interis pochodzi z czasów Inwazji Noctis. Długo czekała na okazję zaatakowania czegoś. I jej się udało.
    * skazenie-magiczne-maszynerii: Anomalna mina (rakieta + pop-up) Interis, pochodząca z czasów Inwazji Noctis. Próbuje zniszczyć wszystko, ale nie jest perfekcyjnie sprawna.
    * presja-czasu: SC Szafirianin się stopniowo rozpada; mina go zniszczy. Trzeba uratować załogę - ale też trzeba wezwać SOS. I trzeba pozbyć się miny.
    * dom-w-plomieniach: SC Szafirianin, kochane miejsce przez załogę, rozpada się przez Anomalną Minę Interis i zmienia się w miejsce wojen domowych gdzie ludzie wbijają sobie nóż w plecy. "To nie ma sensu...".
    * energia-interis: mina pop-up dotknięta Interis. Szuka statków, które wysysa z energii i wprowadza Rozpacz. Potem atakuje i doprowadza do tego, by załoga sama niszczyła swój statek.
    * kaskadujaca-fala-emocji: osoby dotknięte Interis tracą nadzieję. To widzą inne osoby i też tracą nadzieję. Kaskada Rozpaczy się nie kończy i wszystko kończy się tragicznie.
* O co grają Gracze?
    * Sukces:
        * Uratować jak najwięcej ludzi
        * Zniszczyć minę Interis
    * Porażka: 
        * Tylko Sylwia przeżyje
* O co gra MG?
    * Highlevel
        * Pokazać Sylwię i coś epickiego
        * PRIMARY: zaimplementować Theme & Vision
    * Co uzyskać fabularnie
        * W sumie? Zabić Eryka, zniszczyć Szafirianin i zabić jak najwięcej ludzi
* Agendy
    * Devastator: chce zniszczyć statek. Włamać się, wysadzić, wyłączyć. Czerpie ze strachu i beznadziejności; czerpie i wzmacnia.
    * Ludzie: chcą przetrwać, nieważne jakim kosztem.
    * Skażeni ludzie: chcą po prostu by to się skończyło lub szukają jakiejkolwiek ucieczki.

### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

KONTEKST:

* 

CHRONOLOGIA:

* 

PRZECIWNIK:

* 

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

Faza 0: Statek

* Tonalnie
    * brak
* Wiedza
    * statek ma pewne konflikty, ale wystarczająco dobrze działa.
    * przyjaciel Sylwii jest przedsiębiorczym, skutecznym agentem.
* Kontekst postaci i problemu
    * Chcę pokazać graczce przyjaciela (Eryka Komczirpa) jako zdecydowanego, optymistycznego inżyniera 
* Implementacja
    * Walery, który daje ekstra żołd załodze. A zwłaszcza Łucji, bo będzie miała dziecko (wczesne stadium).
    * Cezary, który lekko podrywa Trianę, Łucja jest wściekła i ją opieprza, Tatiana rozjemczyni
    * Borys wyśmiewa Trianę za Arboledo. Triana spokojnie się odgryza. Oboje w śmiech.
    * Łucja się uczy od Eryka. Lubią ze sobą pracować.
    * Tatiana, która integruje całość załogi. Przekonuje Eryka, że Triana jest spoko noktianką.
    * Niewielka awaria, którą Cezary naprawia ASAP. "Nie może być żadnych słabości"
    * Cezary łazi za Sylwią - "prawdziwa agentka!" ku śmiechowi Łucji i Tatiany.

Faza 1 (20:00 - 21:00): DEWASTACJA

* Atak: Statek poważnie uszkodzony. Dehermetyzacja. Death. Sylwia uwięziona.
    * Sylwia uwięziona
* Szepty: Komunikator wysyła Szepty. Ludzie tracą nadzieję.
    * "Halo... Sylwia... ja żyję. Jestem... jestem ranny. Ale żyję. (kaszel). Boli jak skurwysyn."
* Wszystko Stracone: ludzie dotknięci działają tak, jakby to był ostatni dzień życia.
    * gwałt, ludzie oddają się przyjemności
* Recovery: odbudowujemy link i połączenia.
    * Borys oraz Kamil składają siły
* Kill: nie wszystkim się uda; zaczyna się pętla apokalipsy.
    * Niektórych ludzi nie da się ratować. Jest uwięziony przy maszynie; może spłonąć żywcem
* Reaktor traci moc; coś nas wysysa.
* Silnik niestabilny. Trzeba naprawić za wszelką cenę.
    * Łucja lub Arboledo? Cezary?
* Atak Corrupted zabija trzech ludzi.
* NIE MOŻNA włączyć mocy reaktora itp.
    * Bo Mina uderzy ponownie.
* Znaleziony Eryk, był ranny i został zabity przez randomowego zrozpaczonego człowieka.
    * NOOO!!!

| Kto                 | 00:30                 | 01:15                      |
|---------------------|-----------------------|----------------------------|
| **The Devastator**  | atak, szepty          | ładowanie się w energię    |
| **Corrupted Ones**  | "wszystko stracone"   | próba przejęcia statku     |
| **Humans**          | recovery, wtf..?      | próba odzyskania statku    |
| **Decay**           | kill several humans   | silnik jest niestabilny    |


Faza 2 (21:00 - 22:00): SHIP IS DYING

* Tatiana zbiera kultystów. Coup the grace is what we need.
* Kamil chce odbudować statek, Borys mówi że wszystko stracone i trzeba ewakuować
    * walka?


| Kto                 | 02:00                 | 03:00                       |
|---------------------|-----------------------|-----------------------------|
| **The Devastator**  | rip and reconstruct   | next phase: annihilation    |
| **Corrupted Ones**  | kult chroni minę      | masowe zabójstwa i samobójstwa |
| **Humans**          | uciekajmy! nadzieja-- | ucieczka do lifesupport     |
| **Decay**           | lifesupport fails     | ship starts to shatter      |


## Sesja - analiza
### Fiszki
#### SC Szafirianin, 30 osób załogi

* Walery Muszkin: kapitan SC Szafirianin
* Eryk Komczirp: inżynier + solutor, agent Orbitera
    * Bohater Pozytywny: zawsze na froncie, zawsze pomocny, każdemu poda rękę.
* Tatiana Muszkin: pierwszy oficer SC Szafirianin
    * Jest Druga Szansa: zawsze rozjemczyni, zawsze pomagała...
    * Broken; jej ukochany mąż umarł. Pod wpływem Interis po prostu chce umrzeć. "Everything falls".
* Borys Muszkin: szef ochrony SC Szafirianin, brat Tatiany
    * Rozdzieracz Iluzji: jest tylko prawda. Zimny, cyniczny ale twardy jak cholera. Napędzany złością. Lubi Trianę i jest wobec niej ciepły.
    * Potężny, dużo ćwiczy. Masywny wojownik.
* Cezary Pamportus: oficer komunikacyjny SC Szafirianin oraz inżynier
    * Perfekcja Człowieczeństwa: to my, nasza ludzka siła jest najlepsza. My będziemy rządzić! Fan katajinów.
    * Broken; usłyszał SZEPTY
* Łucja Pamportus: inżynier SC Szafirianin
    * Ognista Pasja: wielka fanka swojego męża, niechętna noktiance
* Triana Kasvart: pilot SC Szafirianin oraz ekspertka od ładowni
    * Strażniczka Harmonii: niech się nikt nie kłóci, wszystko da się rozwiązać
* SC Szafirianin: 30 osób załogi, statek transportowy, sprawna jednostka na dość bezpiecznej trasie Astoria - Neikatis

#### Postacie Graczy

* .

### Scena Zero - impl

Kontekst:

* Były pewne problemy na trasie Astoria - Neikatis; na jednym statku doszło do aktu sabotażu i wszyscy zginęli. Miesiąc temu. Samozniszczenie?
    * Orbiter patrolował teren, ale niczego nie znalazł
* Orbiter oddelegował na jednostkę cywilną Szafirianin dwóch agentów - Eryka i Sylwię.
    * I tak chce ich na Neikatis na pewien czas.
    * Eryk i Sylwia są za. Szafirianin to dobra, przyjazna jednostka.
* Nikt nie spodziewał się problemów właśnie z Szafirianinem, ale tam są osoby na których pewnemu komodorowi zależy...

Szafirianin nie jest najszybszą jednostką, ale daje radę. To ma być kilka dni w orbicie transferowej. Eryk i Sylwia mają za zadanie dołączyć do placówki na orbicie Neikatis, na HPO (High Planetary Orbit) i stamtąd mają czekać na dalsze rozkazy, chodzi o monitorowanie jednej arkologii. A po drodze - czy Szafirianin cokolwiek wie odnośnie tego sabotażu.

Gdzieś drugiego dnia podróży - kapitan prosi wszystkich do mesy. Szafirianin ma 30 osób załogi max. 

Walery Muszkin, kapitan, robi ogłoszenie.

* bardzo dobry kontrakt; Neikatis potrzebuje pewnych substancji wymagających szybkości i my możemy ją dostarczyć.
* na przestrzeni ostatnich 2 lat Walery wypłacał 80% umówionej pensji. Ludzie na to poszli.
* nareszcie jest okazja ODDAĆ. Inwestycja się zwróciła. Jako, że zapłata była z góry - jak tylko dotrą na Neikatis, będzie wypłacona premia.
    * morale++
    * ludzie też rozluźnili na widok Orbitera; ogólnie ta populacja, oni nie są wrodzy Orbiterowi. Oni serio cieszą się że Eryk i Sylwia tu są.
* Walery też powiedział, że Łucja za kilka miesięcy musi opuścić statek na pewien czas. 
    * ludzie podchodzą, gratulują, Łucja cała czerwona, kapitan dorzucił ekstra bonus dla niej "na dobry start rodziny"
    * Cezary, oficer komunikacyjny, chodzi za Sylwią jak piesek normalnie. On jest "ludzkość to, ludzkość tamto" a Sylwia jest twarzą plakatów.
* Tatiana, xo, "to wszystko, możecie działać, jak coś wiecie gdzie jesteśmy :-)"

.

* Eryk: Niepokoi mnie ta noktianka, nie miała czystych papierów
* Sylwia: Który noktianin ma?
* Eryk: Na zniszczonej jednostce też był noktianin. Myślisz, że to siły Dolor?
* Sylwia: Ja wiem? Nie pasuje.
* Eryk: Pogadam z Tatianą. Ty spróbuj z noktianką, co? Tak... Ty masz wyczucie.
* Sylwia: I tak miałam taki plan.
* Eryk: (z uśmiechem) A potem pójdę do Łucji. Powiedziała, że jest coś, czego się chciała nauczyć; też jest inżynierem.

Eryk zawsze coś ZROBI nie lubi CZEKAĆ... Sylwia rozumie :-). Sylwia i Eryk się znają nie od dzisiaj. Nie jest może paragonem Orbitera, ale jest sensownym, sympatycznym inżynierem i dobrym strzelcem. Eryk nie jest osobą robiącą kryzysy, on jest z tych pomocnych i optymistycznych.

Sylwia przejrzała papiery noktianki; nasza noktianka jest klarkartianką (skupienie na prawdzie, na dobrej robocie). Z papierów wiadomo, że zabiła trzy osoby. Jedną w samoobronie, dwie podobno "niesprowokowana". Ale ona sama mówiła, że tylko się broniła. Wysadziła ich bombą, bo inaczej oni by zabili ją. Ale dla Eryka ona już jest podejrzana. Triana (noktianka) ma - co też jest podejrzane - seksbota na pokładzie. Swojego. Męskiego. Kapitan i pierwsza oficer akceptują. A seksbot dubluje do cięższych prac. Triana się seksbotem NIE DZIELI. Dla Eryka ona jest TURBO podejrzana XD.

Na mostku są Cezary oraz Triana. Sylwia widzi, jak... Cezary po prostu Trianę podrywa. 

* Triana: ale wiesz, że masz żonę?
* Cezary: no a Ty masz seksbota; Łucja nie będzie mieć nic przeciwko
* Triana: będzie mieć. Ja bym miała... 
* (tu zauważają wejście Sylwii)
* Cezary: Agentka Mazur! Jak świetnie! (gwiazdki w oczach) (Triana przybiera maskę profesjonalizmu)
* Sylwia: To, że Łucja schodzi ze statku to nie przyzwolenie na podboje (puszcza oczko)
* Cezary: Nie, nie, nie o to mi chodzi (tu Triana się uśmiecha lekko)
* Triana: Cezary, z tego co kojarzę, agent Komczirp był zainteresowany zrobieniem czegoś z Waszymi silnikami, interesowało Cię to?
* Cezary: Tak, oczywiście!
* Sylwia: Podbij do maszynowni, jest tam. Eryk mówił, że się tam wybiera.
* Cezary: Jasne, dzięki!

Triana wróciła do pilotażu. Sylwia wie, że Triana nie musi się na tym szczególnie skupiać; wszystko działa i są w orbicie transferowej. Jest JEDNA linia którą lecą. A Triana jest dobrym pilotem. Noktiański pilot to zawsze coś. Już na pierwszy rzut oka Sylwii Triana nie wygląda na sabotażystkę; dobrze jest zintegrowana z załogą.

* Sylwia: Cezary tak dopiero teraz czy od dłuższej chwili?
* Triana: Cezaremu się podobam; lubi egzotyczne dziewczyny (lekki uśmiech). Ale to tylko słowa. Kocha swoją żonę. Gdyby jej nie było, tak, próbowałby. Nie obraziłby się gdybym poszła z nim do łóżka. Ale nie pójdę.
* Sylwia: Jego pech.
* Triana: Łucja urwałaby mu pewne... części ciała.
* Sylwia: Dosyć kluczowe. Nie byłoby więcej małych Cezarzątek?
* Triana: W rzeczy samej (lekki uśmiech).
* (chwila rozmowy między nimi)
* Triana: Na naszej jednostce jest sześciu noktian. Ale tylko ja mam... powiedzmy, prominentną rolę. Wszyscy jesteśmy zintegrowani.
* Triana: Może zabrzmieć to dziwnie, ale... to dobrze że tu jest Orbiter.
* Sylwia: Nieoczekiwane stwierdzenie.
* Triana: Trasa na Neikatis ma droższe zasoby. Bo jedna z poprzednich jednostek zginęła. (Sylwia wie o której Triana mówi)
* Triana: W świetle powyższych wiadomości uważam, że Wasza obecność jest wskazana.
* Sylwia: Czego się po nas spodziewasz?
* Triana: Niczego. I nie spodziewam się problemów. Ale jak będą... to nas obronicie. Orbiter. (lekki uśmiech)
* Triana: Orbiter ma może złą reputację u noktian, ale bądźmy rozsądne; mogło być dużo gorzej. A bez Orbitera nie mamy bezpiecznego handlu.
* Sylwia: Noktianie nie mają najczystszej reputacji ogólnie. Dobrze się uzupełniamy w tej kwestii.

Wszedł szef ochrony. Borys. Brat Tatiany. Wchodzi groźnie, ale Sylwia widzi, że to trochę udawana groźność.

* Borys (pokazuje palcem na Trianę): Zdrajczyni!
* Triana: Nie zjadłam Twojego jedzenia z lodówki.

Sylwia WYRAŹNIE widzi, że Borys ratuje Trianę przed złym Orbiterem.

* Sylwia: Zamiast ratować Trianę, lepiej byś zadbał o cnotę Cezarego (z lekkim przekąsem).
* Borys: Też zauważyłaś? Już? On zdradził Łucję z... chyba każdą załogantką.
* Triana: Cezary nie jest tego typu osobą, Borysie.
* Borys: Ty i Łucja. Jedyne dwie, które tego nie widzą. Mam zacząć wymieniać?
* Triana: (lekko posmutniała) Nie, nie musisz. Ale ja z nim nie pójdę do łóżka.
* Borys: (złagodniał) Wiem. Ale wiesz jaki jest. Nieważne jak bardzo nie chcesz tego wiedzieć.
* Borys (do Sylwii): Spodziewałem się... jakiegoś... kurczę, czegoś innego niż Wy. Jestem zaskoczony pozytywnie.
* Sylwia: Miło mi to słyszeć.
* Borys: Statek korzysta z Agenta Komczirpa i z Twojej obecności, chyba. Tak czy inaczej, dzięki.
* Sylwia: (lekki salut, łapie spojrzenie Borysa) Nie zamierzam robić krzywdy nikomu z Twojej załogi bez dobrego powodu.
* Borys: Siostra mi mówiła, że nie będzie z Wami kłopotu. Nie wierzyłem. Miała rację.

I podróż toczy się dalej...

### Sesja Właściwa - impl

Jeden z kolejnych dni. Sylwia wraca ze sparingu z Borysem do swojej kwatery którą dzieli z Erykiem (do niczego nie doszło, on jest dżentelmenem). Sylwia i Eryk na _comms_

* Eryk: Wszystko w normie, pokazuję Łucji jak zrobić małe usprawnienie dookoła wentylacji
* Sylwia: Przewietrzenie pomoże.
* Eryk: To całkiem fajna...

BŁYSK. WSTRZĄS. Statek zgasł. Czerwone alarmy. TAI wyłączona. Sylwia natychmiast zakłada _emergency gear_, leci na mostek. Przed Sylwią zamknięta śluza. Nieważne - najpierw Eryk; Sylwia bierze sprzęt i leci do Eryka.

Tyle, że Sylwia widzi przez okienko śluzy prowadzące do maszynowni z jej lokalizacji, że... widzi szczelinę. W statku jest dziura. Statek został przebity na wylot. Nie ma jak się dostać do maszynowni w tej chwili.

* Eryk: Halo... (kaszel)
* Sylwia: Gdzie jesteś, jak pomóc?
* Eryk: Maszynownia. Jestem uwięziony, ranny... ale się nie wykrwawię. Spróbuję przywrócić prąd. Co się stało?
* Sylwia: Nie mam pojęcia. Przy śluzie od mojej strony jest Twój sprzęt. Możesz otworzyć?
* Eryk: Jestem częściowo niemobilny. Jestem zaklinowany. Chyba złamałem nogę. Doprowadź komunikację do działania; powinna działać jak jest awaryjka. Nie wiem czemu nie działa.
* Sylwia: (opisała jak to wygląda)

Sylwia widziała coś jakby... _Armour Piercing_ uderzył w nieopancerzoną jednostkę i przebił ją na wylot. Czyli... ktoś powinien był użyć _High Explosives_, ale tego nie zrobił. Dlatego w ogóle Szafirianin żyje. Ale co się stało? Nie było żadnej zmiany kursu, nie było NIC. Wyłączone są wszystkie systemy.

* Eryk: Łucja szła do swojej kwatery. Nie była już ze mną. Jestem tu z kilkoma technikami, spróbuję przejąć dowodzenie. Muszę się do nich dostać, bo jesteśmy w różnych pomieszczeniach.
* Sylwia: zrób to... dasz radę.
* Eryk: Nie takie rzeczy robiliśmy. Sylwio, doprowadź komunikację i TAI do działania. Nie mam kontaktu z TAI. Nie wiem, czy MAMY TAI.'
* Sylwia: Załóż że nie, będzie bezpieczniej. Ja bym strzelała w TAI
* Eryk: Cholera... myślałem, że się POMYLILI strzelając AP. Ale może nie. Efektywnie nas wyłączyli.
* Sylwia: Nie wiem. 

Sylwia porzuca sprzęt przy śluzie. Przechodzi w tryb bojowy. Trzeba rozwiązać sprawę a nikt nie spodziewa się agentki Orbitera na pokładzie. Sylwia biegnie w kierunku mostka. Z jednej z kwater Sylwia słyszy krzyk "pomocy!". Drzwi są zamknięte i nie ma prądu. Uderza w drzwi "nie możesz wyjść czy coś poważnego?"

* Głos: NIE MOGĘ WYJŚĆ POŻAR!
* Sylwia przejmuje dowodzenie: Weź się ogarnij, uruchom system ręcznie!
* Głos: JAK! JAK! (głupia panika) WIDZĘ DYM!
* (przy okazji Sylwia słyszy z kilku kabin głosy "co się dzieje" "ciemno" "awaria?" "na pomoc, ktoś tam jest?" - i jeszcze jeden głos wskazuje na pożar)

Sylwia uruchamia głośniki serwopancerza. "It's a drill". Ma wprawę w tego typu sprawach. Instruktaż na szybkości. Zmienia to w element programu szkoleniowego.

Tp Z+4:

* Vr: Udało się ręcznie odpalić systemy ppoż. Nadal ludzie są w lekkiej panice, ale nie zginą a statek przeszedł w tryb "awarii". To jest dobre.
* V: Sylwia ich częściowo uspokoiła. Nadal nie wiedzą co się dzieje, ale wiedzą, że sytuacja jest pod kontrolą.
* V: Sylwia może ich zostawić samych. Zajęło to chwilę - niestety, kilkanaście minut poszło - ale ci ludzie są pod kontrolą i opanowani. Można ich samych zostawić.

Sylwia idzie dalej na mostek. Zobaczyła jak jakiś podsystem się zawalił. Uszkodzenia są większe niż się wydawało. Droga na mostek jest częściowo zrujnowana, ale nie jest to problem dla Sylwii. 

Na komunikatorze pojawił się sygnał, dźwięk, niewyraźny, zaniknął. Coś próbowało się połączyć ale nie dało rady. Internal statku.

* Eryk: Słyszałaś to?
* Sylwia: Mhm. Nie wiem co to i nie wiem czy chcę wiedzieć.
* Eryk: Nie widzę niczego zbliżającego się do nas. Udało mi się wykorzystać kilka rzeczy jako proximity; nic się nie zbliża.
* Sylwia: Jeszcze.
* Eryk: Tak, jeszcze.

Sylwia jest przy drzwiach na mostek. Są pośpiesznie zabarykadowane, ale nikt nie próbował się tu dostać.

Komunikator się włączył. Jest słaby sygnał.

* Tatiana: kapitan nie żyje... kapitan nie żyje... mój kochany Walery nie żyje... (she's completely broken)

Sygnał zaniknął znowu.

* Sylwia: Wiesz skąd to poszło?
* Eryk: To od Tatiany?
* Sylwia: Mhm. Mostek zabarykadowany.
* Eryk: Mostek na pewno ma swój awaryjny system. Ale to nie poszło z mostka; poszło gdzieś z mojej części statku. Z kwater Walerego i Tatiany? Tam też powinien być Borys.
* Sylwia: Wbijam się na mostek.

Sylwia próbuje użyć serwopancerza i próbuje skorzystać z okazji, że drzwi są naruszone i je WYRWAĆ serwopancerzem.

Tr +3:

* X: Zajmuje to nieprzyjemną ilość czasu i serwopancerz rzuca sygnały alarmowe, ale jeszcze wszystko jest OK.
* V: Sukces. Sylwia WYRYWA drzwi (przy protestach serwopancerza) i wbija się na mostek.

Dwie martwe osoby; jacyś oficerowie. Zginęli od uszkodzeń wstrząsu. Aktywny węzeł komunikacyjny i alarmowe subsystemy aktywne. Sylwia słyszy dźwięki z pokoju konferencyjnego. Struggle. Sylwia szybko się przesuwa.

Triana jest lekko przyduszona przez Cezarego, który w nią wchodzi. Ona się próbuje bronić, ale nie ma SIŁY.

Sylwia natychmiast ZDEJMUJE Cezarego z Triany. On się miota przez moment, ale po chwili wybucha płaczem. Triana zresztą też, ale z innych powodów, kuli się. Czerwone awaryjne światła dodają surrealizmu scenie.

* Cezary: Nawet tej jednej małej rzeczy nie dostanę... (płacz absolutnej rozpaczy)
* Triana: Dziękuję... dziękuję... Wieczny Saitaerze, dziękuję Ci... (rozpaczliwie próbuje się czymś okryć)
* Sylwia: Nie kwalifikuję się na Saitaera.
* Triana: (zbyt zajęta by odpowiedzieć)

Sylwia uważa, że THIS IS FUCKED UP.

* Sylwia: Co o tym wiesz?!
* Triana: Niewiele, coś... coś nas zaatakowało. Wyglądało... zimny statek. 
* Sylwia: Co to jest zimny statek?
* Triana: Statek w stanie uśpienia, bezzałogowy. Z jakiegoś powodu... au. Z jakiegoś powodu bezzałogowe statki nie działają.
* Cezary: Łucja nie żyje. MY nie żyjemy. Wszystko jest zniszczone. Jesteśmy już martwi.
* Sylwia: I to Ci nie daje ŻADNYCH praw.
* Cezary: Widziałem jej śmierć... (płacz)
* Triana: Ja Cię broniłam. Ja Cię ZAWSZE broniłam (wściekła, przerażona, zrozpaczona). Broniłam Cię przed Łucją! (Triana się rozkręca)
* Sylwia: Triana!
* Triana: (z lekko obłędnymi oczami, ale obłęd mija) Tak? Ty dowodzisz. Tylko... tylko poprawię mundur.
* Sylwia: Gdzie mogę go zostawić? (wlecze go do kabiny)
* Triana: CZEKAJ! 
* Sylwia: Tak?
* Triana: (nie wie jak to powiedzieć): ...jeśli pójdziesz z mostka, zostanę tu sama. Jeśli ktoś przyjdzie, ja się nie obronię.
* Cezary: ...przepraszam, nie wiem co mnie opętało. Nie zrobię krzywdy, przysięgam.
* Sylwia: Miałeś swoją szansę.
* Triana: Nie wierzę Ci. (podeszła do jednego z zabitych oficerów, wyraźnie się wzdrygnęła, zabrała jego nóż) (drży jej w rękach, ale jest)
* Cezary: Jestem inżynierem... i od komunikacji, mogę odbudować system komunikacyjny.
* Triana: Nie wierzę że to zrobisz.
* Sylwia: Eryk? Jak daleko jesteście i czy da się do Was dotrzeć?
* Eryk: Bez mostka wiele nie zrobimy.
* Sylwia: Jestem na mostku, czego Ci trzeba? Mam też sytuację (hasła kodowe)
* Eryk: ...przytrzaśnij go ruiną, wygnij metal, nie wiem. I ubierz go. Potrzebuję by TAI mostka przejęła statek.
* Eryk: I nie włączaj reaktora i nie uruchamiaj statku na pełnej mocy. Mógłbym, ale pokażemy wtedy...
* Sylwia: ...że żyjemy, wiem.
* Eryk: Po mojej stronie udało mi się zebrać 3 osoby. Naprawiamy... odzyskujemy kontrolę, gasimy pożary, metaforycznie i dosłownie. Statek... to dziwne.
* Eryk: Borys żyje. Przejmuje dowodzenie, bo Tatiana... do niczego się nie nadaje. Oczekiwałem od niej więcej. Od Cezarego też.
* Sylwia: Tak... nie Ty jeden.
* Eryk: Możesz uruchomić wewnętrzne komunikatory? Takie, byśmy nie tylko my mogli gadać?
* Sylwia: Spróbuję.
* Triana: ...nie wiem jak. Jestem pilotem, znam się na ładunku. I nie... nie chcę zabijać. Zabiłam ich bo musiałam... nie umiem... NIE CHCĘ walczyć... (wpada w pętle, ale się zbiera)
* Sylwia: Nikt nie oczekuje, że będziesz walczyć.

Sylwia bierze porządną rurę i podmontowuje Cezarego do krzesła. Nie odejdzie stąd. Nigdzie stąd się nie ruszy i Triana jest bezpieczna nawet jeśli z nim.

TpZ+3:

* Vz: Cezary jest przyspawany. Nie ma jak się wydostać. Sylwia MYŚLAŁA czy nie zrobić, by go bolało, ale to nie metoda Orbitera.

Cezary próbuje uruchomić łączność:

Tr+3:

* X: Cezary robi to dłużej niż chcemy. Sytuacja jest trudna a on nie jest zmobilizowany, on NIE WIERZY w sukces.

Sylwia widzi, że jest potrzebne coś innego. Inne podejście. Trzeba ich uspokoić. Najpierw Triana, potem Cezary.

* Sylwia: jesteś silna. Przetrwałaś. Robisz to, co należy. Dasz sobie radę.
* Triana: ...ale po co... wierzyłam w ten statek, w tą załogę...
* Sylwia: jedna osoba nie powinna zepsuć całej reszty, wszędzie są ci źli.
* Triana: ...kapitan nie żyje. Pierwsza oficer zawiodła...
* Sylwia: oni potrzebują wsparcia. Borys potrzebuje Twojej pomocy.
* Triana: (podniosła głowę) Borys przeżył? Borys działa?
* Sylwia: pomaga Erykowi.
* Triana: (otarła łzy) jestem już zmęczona...
* Sylwia: wierzę. Ale jesteśmy tu, jest nas więcej i jeśli będziemy działać razem to powinniśmy sobie z tym dać radę. Nie będę Cię okłamywać, jest źle. Nie wiem co się dzieje i potrzebuję pomocy by to zrozumieć.

Sylwia decyduje się wesprzeć Trianę.

Tr Z+4:

* X: Do Cezarego doszło, co zrobił i jakie były IMPLIKACJE jego czynów. Najpewniej będzie chciał popełnić samobójstwo albo COŚ. Ale nie przeciw Trianie.
* Vr: Triana... doszła częściowo do siebie. Trzyma się. "To są moi ludzie, jestem im to winna, nieważne co myślę, jestem im to winna. Gloria Saitaer."
* X: Triana nie chce i nie będzieć mieć nic wspólnego z Cezarym. Jeśli on się do niej zbliży, zaatakuje. She got paranoid.
* X: Dark Communication.
    * Cezary doprowadził komunikator do częściowego działania.
        * Łucja: "Halo..? (szloch) Proszę, niech ktoś odpowie"
        * Cezary (RYKNĄŁ!): "ŁUCJO! Kochanie, żyjesz?! O boże..."
        * Łucja: "Czarku... oni próbują mnie zabić. Jest tu kilka osób. Polują na mnie. (wymieniła kilka imion) Oni oszaleli. Schowałam się... a silnik... jest niestabilny."
        * Cezary: Kochanie uratuję Cię, przysięgam... uratuję Cię...
        * Sylwia: Gdzie jesteś?
        * Łucja: (lokalizacja zbliżona do Waszej części, przy jednym silniku). Silnik ma wyciek, trzeba go jak najszybciej naprawić. Ale jeśli ja...
        * Cezary: Ja to zrobię! Nie idź tam!

Sylwia przekazuje rozmowę Erykowi.

* Eryk: szlag... to tłumaczy parametry. Myślałem, że to uszkodzona diagnostyka, ale nie.
* Sylwia: gdzie mam iść i co mam zrobić?
* Eryk: ja z mojej strony... nie będę ukrywał, to będzie niebezpieczne.
* Sylwia: powiedz mi coś nowego
* Eryk: przygotuję serię mikroładunków kierunkowych ORAZ osłabię jednostkę strukturalnie. Odepniemy silnik od statku. Orbiter zobaczy... jak go dobrze wysadzimy.
* Sylwia: zrobimy to. 
* Eryk: Ty musisz z zewnątrz...
* Sylwia: wyciągnąć Łucję, jeśli tam jest.
* Eryk: Łucja tam jest, wiem o tym, widzę ją na detektorze. Moim. IFF.
* Sylwia: to tym bardziej muszę ją stamtąd wyciągnąć.
* Eryk: Jedna rzecz mnie niepokoi. Jedna (lekki śmiech). Mamy... za duże zużycie energii.
* Sylwia: coś nas wysysa.
* Eryk: M-hm. (ponuro). A nie mam jak wysłać wiadomości do Orbitera.
* Sylwia: odpowiednio finezyjny wybuch da radę.
* Eryk: Przeładuję zdalnie ten silnik. 
* Sylwia: technicznie będziesz w stanie?
* Eryk: Jak mi dacie kontrolę z poziomu mostka... pytanie, czy mi się uda wycofać na czas. Tak czy inaczej, ściągnę się na liną i kierunkowo wysadzę... jakieś 15% statku.

.

* Sylwia: co widziałeś, że uznałeś że Łucja nie żyje, i GDZIE?
* Cezary: ...widziałem. 
* Sylwia: CO?!
* Cezary: pocisk rozdarł ją...
* Sylwia: Nie miałeś jak tego zobaczyć. Nie na tej konsoli.

Komunikator znowu się uruchomił.

* Borys: Tu Borys.
* Cezary: Borys, żyjesz!
* Borys: Nie pierdol, status. Ja dowodzę? (z lekkim niedowierzaniem)
* Sylwia: Stało się tak, że jesteś jedyny co może.
* Borys: Orbiter. To dobrze. Słuchaj, znasz się na tym, bo ja nie.
* Sylwia: Sytuacjach kryzysowych?
* Borys: Nie kurde, zbieraniem pietruszki. A teraz słuchaj. Mam siedem osób. Wiecie gdzie są ludzie do uratowania? Jesteśmy ślepi. 
* Sylwia: (podaje kajuty gdzie zrobiła porządek) (podaje okolice Łucji i silnika).
* Borys: Dobra, idę tam. 
* Sylwia: Potrzebujesz wsparcia. (ludzie chcą zabić Łucję)
* Borys: NIKT nie zabije Łucji. To moja załogantka. Walery dał jej premię.
* Sylwia: I dlatego potrzebujesz wsparcia.
* Borys: Dojdź do tego co tu się dzieje, musiałem... (lekko załamał mu się głos)... musiałem zabić kolegę. Bo on na mnie ruszył z nożem pierwszy.
* Sylwia: Szaleństwo czy zdrada?
* Borys: (myśli) Desperacja. Chciał, bym go zabił. Nie miał jaj. Ale ja zrobię co trzeba. Oceniaj mnie potem. Ale to nie Twoja jurysdykcja. Potem o tym pogadamy (tak, mówi do siebie).

Sylwia CZUJE podniesione pole magiczne. I to nie wynika ze _statku_. Coś się stało przy trafieniu. Co gorsza - to Interis.

* Sylwia: Borys, cokolwiek robisz, upewnij się, że będą mieć nadzieję. Ci ludzie POTRZEBUJĄ nadziei.
* Borys: Sylwio z Orbitera, ja sam nie mam nadziei. Ale to nie ma znaczenia, wiesz czemu? Bo nigdy jej nie miałem. To wszystko to bullshit. Rozwalimy ten bullshit. Rozumiesz?
* Sylwia: Wystarczy mi. Ale zrozum to. Tobie to wystarczy, oni muszą mieć nadzieję. Normalnie bym nie proponowała, chcesz trochę propagandowych nagrań popuszczanych?
* Borys: Pierdol się.
* Sylwia: Jak zmienisz zdanie...
* Borys: Pierdol się. (z lekkim rozbawieniem)

Tp+3:

* V: Możesz na Borysa liczyć.
* Vr: Borys opanuje swoich ludzi.
* X: Borys PO PROSTU nie jest w stanie pomóc wszystkim. I jest zbyt uparty. A Sylwia nie ogarnie wszystkich wszędzie.
* V: Borys pozwoli na puszczenie nagrań propagandowych z radiowęzła plus jakieś słowa Triany jako dowód że żyje.

.

* Triana: JA mam być symbolem? Ja mam im powiedzieć, że będzie lepiej?
* Sylwia: Nieważne kto. Ciebie znają. Poza tym... to Twój statek. A Borys potrzebuje każdej pomocy jaką może dostać. Zwłaszcza Twoją.
* Triana: Zrobię co jestem w stanie.

Ex Z +4 (DRAW 3):

* VzXX: pomogło. Zmniejszyło wpływ Interienta na statek.

Na ledwo sprawnej konsoli poszły dźwięki agonii kilku osób, gdy jakiś fragment statku znowu się zawalił. Triana ma łzy, ale nadaje dalej.

* Eryk: (kaszel) Słyszałaś?
* Sylwia: Muszę wyjść. Nie ma wyboru. Nie przetrwamy.
* Eryk: Masz rację. Dzięki... zrobiłem diagnostykę. Coś nas wysysa i to coś jest w kosmosie.
* Sylwia: Biorę Twój ciężki laser
* Eryk: Bierz, autoryzuję Cię. Mam... na taką chwilę mam Twoją rękawiczkę.
* Sylwia: Każdy ma swój fetysz...
* Eryk: ...to nie to... wiesz, ile mogę za nią dostać?
* Sylwia: W sumie coś w tym jest.
* Eryk: (kaszel). 
* Sylwia: Jak się trzymasz? Status?
* Eryk: Przetrwam. Mam... tabletki, painkillery, mechanizmy na napromieniowanie... wyjdę z tego, serio.
* Sylwia: Ile mam czasu?
* Eryk: Kilka godzin i leki zaczną schodzić. Ale potem też mogę coś zrobić. Np. rozszabruję Twoją apteczkę; przejdziesz do mnie kosmicznie, dobrze? Z apteczką? Bo czuję się... nienajlepiej. Ale nic czego Orbiter nie naprawi.
    * (wysłał status)
    * Sylwia widzi: napromieniowany, złamana noga, otwarte, pęknięte żebro, wstrząs mózgu, ogólne potłuczenia
* Sylwia: Jak z tego wyjdziemy dobrze, dorzucę Ci autograf.
* Eryk: Docenię. A dokładniej, mój siostrzeniec jest Twoim fanem.
* Sylwia: ...wiedziałam na co się piszę, nie narzekam.
* Eryk: (lekki śmiech zakończony kaszlem)

Sylwia zbiera się, by wyjść w kosmos. Utrzymuje z Borysem kontakt, co się dzieje itp.

Tr Z +3 (draw 3):

* XXVz: interpretacja: częściowy sukces z dużymi stratami.

.

* Borys: Łucja żyje, mam ją. Z dwoma osobami idziemy na mostek.
* Borys: Zaatakowali nas od pleców.
* Sylwia: Członkowie załogi czy ktoś obcy?
* Borys: Dwóch załogantów. Nazwali to _coup de grace_. Bo i tak nic to nie zmieni. Byli słabi. Poddali się iluzjom. Ale wszyscy nie żyją.
* Sylwia: Uważaj na swoich ludzi
* Borys: Jest nas czwórka; damy radę.
* Sylwia: (powiedziała o sprawie z Cezarym)
* Borys: Święte Gówno W Konserwie. Jak ja to powiem... to mam... dobra, nie idę na mostek. Mówiłaś o tych w kabinach.
* Sylwia: Mhm.
* Borys: Rozpocznę ewakuację statku. Mamy kapsuły.
* Sylwia: ABSOLUTNIE nie. Nie możesz. Nie póki nie zniszczę tego na zewnątrz.
* Borys: Jest JESZCZE coś na zewnątrz?
* Sylwia: (powiedziała mu prawdę). Ty masz twardą dupę, Ty możesz wiedzieć.
* Borys: ...powiedz mi, że Triana się trzyma.
* Sylwia: Na razie tak.
* Borys: Dobra. Zrobię co mogę czym mam. 
* Sylwia: Witaj w klubie.
* Borys: Czyli siorka jest... Skażona. Nie chcę jej zabijać. (cicha rezygnacja)
* Sylwia: Żadnych takich. Borys. Kontroluj swoje myśli. Pamiętasz tamtą jednostkę? Tak umarła tamta jednostka.
* Sylwia: Jesteś OSTATNIM źródłem nadziei jakie mają. Ty i Triana. Razem jesteście tym, co może im to dać. Nie możecie ich zawieść.

Tp Z +4:

* V: Borys się otrząsnął. Zrobi co trzeba.

Sylwia wychodzi w kosmos. Zrobiła wszystko, co była w stanie na statku. Borys i Triana kontrolują załogę a Eryk zajmuje się inżynierią.

Pierwsze co Sylwia robi to odpala skanery. Sprzęt Orbitera daje małą sygnaturę ale jest naprawdę mocny.

Tr Z +3:

* X: Sylwia poczuła STRASZLIWY wstrząs. Kolejny strzał w statek. Poziom rozpaczy na statku wzrósł. Strzał nie ma zniszczyć. Są dodatkowe straty, ale to nie są Eryk, Triana, Łucja ani Borys. Szafirianin jest SŁABY strukturalnie.
* (Sylwia na zewnątrz podpina się pod czujniki Szafirianina jako amplifikator i maskowanie by puścić więcej) -> Tp: 
* V: Masz to. To jest... MINA. Pop-up mine. To wygląda jak Skażona mina z czasów wojny. Astoriańska konstrukcja. Ta mina jest STARA i SKAŻONA; podpięła się takimi dalekosiężnymi "mackodrutami" do statku. Jest jakieś 1 km stąd. Mina ssie zarówno materiał (stąd strukturalnie statek się psuje) jak i energię. Jak i emocje.

.

* Eryk: Silnik się destabilizuje coraz mocniej. Mamy kilkanaście minut. Mogę coś z tym spróbować zrobić... spowolnić...
* Sylwia: Nie obiecywałam, że oddam Twój laser, prawda? Potnę to na kawałki
* Eryk: Sylwio, NIE! Nie wrócisz!
* Sylwia: Zakład? Masz lepszy pomysł?
* Eryk: Obrócimy statek i silnikiem...
* Sylwia: Masz parę minut na to i statek jeszcze bardziej wysadzisz. Potraktuj to jako awaryjny.
* Eryk: ...uważaj na siebie... odwrócę jego uwagę.
* Sylwia: ...nie wiem czy jest CO odwracać... ale bez głupich pomysłów, mój jest wystarczająco głupi.

Sylwia wysyła drona przodem oraz przeładowuje sygnaturę drona. Sama - wiedząc że to tylko 1 km - leci na pełnej mocy silników by wylądować na powierzchni miny pop-up...

Ex Z +4:

* X: Mina zdążyła zestrzelić drona i celuje w ogólną stronę Sylwi; ta musiała przyspieszyć.
* X: Sylwia włączyła PEŁNĄ moc silników manewrowych - leci prosto na minę pop-up, nie da rady wyhamować. Serwopancerz będzie uszkodzony a Sylwia potłuczona, ale pełna hermetyzacja.
* Vr: Sylwia UTRZYMAŁA się miny pop-up. Pęd oderwał minę od Szafirianina. Sylwia w przerażeniu widzi, jak ona leci z miną a macko-druty zaczynają wracać do miny, jak pajęczyna pragnąca ją objąć...

Sylwia trzyma się mocno działka pop-up, unikając czegokolwiek i laserem inżynieryjnym WYPALA macki z powierzchni miny pop-up. (+Z -> Tr+4)

* X: Sylwia nie nadąża; natychmiast włączyła silniki manewrowe i oderwać się z miny. Straciła delta-v, nie ma jak WRÓCIĆ na macierzystą jednostkę, ale strzela dalej. Ona i mina oddalają się od statku.
* V: Sylwia laserem górniczym wypaliła 'macki'; mina próbuje namierzyć Sylwię działkiem, ale to działko nie od tego a dystans za krótki. Sylwia jest w stanie manewrować by nie dać się trafić. I tak się oddalają.
* Sylwia ma CHORY pomysł. Chce odpowiednio laserem górniczym zdeformować lufę by eksplozja odepchnęła ją do Szafirianina. +2Vg
    * Sylwia: Powinniście być odpięci, więc silnik powinien być lepszy
    * Eryk: Nie jest, ale mamy moc statku. Mogę coś z tym zrobić.
    * Sylwia: Pasuje. Eryk... jak to zadziała, to stawiasz mi największe piwo w okolicy.
    * Eryk: co? XD
* Vz: EKSPLOZJA. Sylwia zdeformowała lufę miny i leci prosto na Szafirianin. Mina się sama zniszczyła. Sylwia NATYCHMIAST blokuje tarczą...
* V: Sylwia ma połamaną rękę, bark, parę innych rzeczy - ale jest na Szafirianinie, był CLANG. Sylwia się doczołga w servarze do śluzy, ale potem padnie...

Orbiter przybył, zanim ktoś jeszcze zginął...

## Streszczenie

Statek cywilny Szafirianin, transportujący materiały z Astorii dla arkologii Neikatis zostaje trafiony przez nieznany pocisk, który uszkadza jego strukturę, zabijając kapitana i deaaktywując główne systemy. Sylwia działa, pomagając załodze opanować sytuację kryzysową. Eryk, uwięziony w maszynowni, wspiera ją zdalnie, mimo swoich obrażeń.

Sylwia wkracza na mostek, gdzie odkrywa trupy i szaleństwo Cezarego wobec Triany. Po opanowaniu sytuacji Agenci dochodzą do tego, że statek jest celem starej, skażonej astoriańskiej miny pop-up skażonej Interis. Przejęci destrukcyjną energią Interis, niektórzy członkowie załogi zaczynają zdradzać oznaki obłędu.

Sylwia wychodzi na zewnątrz, aby zmierzyć się z miną, mimo świadomości, że może to być misja samobójcza. Używa ciężkiego lasera, by uszkodzić minę i zniwelować jej działanie. Ostatecznie udaje się jej zniszczyć urządzenie i wrócić na pokład, choć sama doznaje poważnych obrażeń. Z pomocą Orbitera, który przybywa na czas, załoga Szafirianina zostaje uratowana, a kryzys zażegnany.

## Progresja

* Sylwia Mazur: miesiąc w szpitalu na stacji neikatiańskiej. I szacun do przodu od Orbitera i nowe video.
* Eryk Komczirp: miesiąc w szpitalu na stacji neikatiańskiej. I szacun do przodu od Orbitera i nowe video.
* Tatiana Muszkin: zrezygnowała z roli pierwszego oficera na Szafirianinie, ale została na jednostce.
* Borys Muszkin: nowy kapitan SC Szafirianin; w sytuacji kryzysowej stanął na wysokości zadania.

## Zasługi

* Sylwia Mazur: Przejęła dowodzenie po awarii Szafirianina, uratowała Trianę przed atakiem Cezarego i zniszczyła Skażoną minę pop-up, ratując statek. Z sukcesem utrzymywała załogę przy życiu i dbała o morale, wykrywając Interis jako przyczyny większości problemów.
* Eryk Komczirp: Mimo poważnych obrażeń, wspierał Sylwię, działając w maszynowni i monitorując stan SC Szafirianina. Przejął kontrolę nad systemami awaryjnymi, dowodził maszynownią i okazał się świetnym wsparciem dla Sylwii.
* Walery Muszkin: KIA; kapitan SC Szafirianin o dobrym sercu i chętnie dzielący się z załogą dobrem. Jego śmierć była szokiem, który odbił się na morale załogi i złamał Tatianę.
* Tatiana Muszkin: złamana przez śmierć Walerego; opanowana przez Interis nie potrafiła opuścić pokoju. Zawiodła jako pierwszy oficer.
* Borys Muszkin: Przejął dowodzenie po śmierci kapitana, dbał o organizację działań i wspierał załogę, mimo że sam był pozbawiony nadziei. Jego niezłomne wsparcie było kluczowe dla załogi, Sylwii i Triany.
* Cezary Pamportus: Po emocjonalnym załamaniu i skrzywdzeniu Triany podjął próbę uruchomienia systemów komunikacyjnych, co okazało się kluczowe do nawiązania kontaktu z Łucją. Broken soul. Interis wygrało.
* Łucja Pamportus: Była bliska śmierci i prześladowana przez chaos, ale ostrzegła o sytuacji i dotarła do Borysa. Ognista, zazdrosna o swojego męża.
* Triana Kasvart: Dość optymistyczna noktiańska pilot; z początku była ofiarą Cezarego, ale utrzymała nerwy na wodzy i pomogła Sylwii podtrzymać morale załogi. Znalazła siłę, by wesprzeć Borysa i nie poddać się wpływom Interis, choć w obliczu załamania Cezarego utraciła zaufanie do niego.
* SC Szafirianin: pomimo uszkodzeń i destabilizacji strukturalnej, udało mu się przetrwać dzięki współpracy załogi i agentów Orbitera. Stracił kapitana i część załogi, ale jest do odratowania.

## Frakcje

* brak

## Fakty Lokalizacji

* brak

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Transfer Astoria - Neikatis

## Czas

* Opóźnienie: 1
* Dni: -572

## Inne

.