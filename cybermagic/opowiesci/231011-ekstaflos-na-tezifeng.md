## Metadane

* title: "Ekstaflos na Tezifeng"
* threads: triumfalny-powrot-arianny
* motives: body-horror, hostage-situation, kralotyczne-uzaleznienie, machinacje-syndykatu-aureliona, misja-ratunkowa-to-pulapka, sinful-hedonism, glupia-stawka-zakladu
* gm: żółw
* players: kapsel, fox

## Kontynuacja
### Kampanijna

* [200624 - Ratujmy Castigator](200624-ratujmy-castigator)

### Chronologiczna

* [200624 - Ratujmy Castigator](200624-ratujmy-castigator)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * Sonata Arctica "End of this chapter"
        * "I'm not all that stable | You should know by know that you are mine"
* Opowieść o (Theme and vision): 
    * "Pechowe spotkanie z Syndykatem i kralotycznym sojusznikiem Syndykatu zniszczyło jednostkę"
    * "There are far worse things and fates than death, everything can be used"
    * SUKCES
        * uratowanie (swojej załogi) > (TAI (informacja)) > (maga) > ("flesh mannequins")
* Dilemma: "kill or fall"
    * Atak w pierwszej praktycznie scenie
    * Tezifeng jest uzbrojony i ma świetne reakcje
    * Zwalczanie maga lub zmuszanie go do czarowania zabija jego załogę (len toru: 5, len toru Skażenia: 5)

### Co się stało i co wiemy

* Przeszłość
    * Podczas starcia z Castigatorem, korweta 'Tezifeng' (30 os) skanowała różne obiekty dalekosiężne, dowodzona przez 2 arystokratów Aurum
        * Jedną z osób jest Klaudiusz Terienak, którego Arianna zna z "Królowej" (pies na laski), drugą jest Natalia Gwozdnik - roztrzepana ale sprawna tienka, z tendencjami do służbistości
            * Klaudiusz miał pewne skargi ze strony cywilnych dam. Natalia go trzymała twardo w ryzach.
    * Tezifeng napotkał na jednostkę Syndykatu, gdzie występował kraloth.
        * Natalia została zabrana z pokładu. Klaudiusz natomiast... "działania potwora kosmicznego". Tezifeng został zmieniony w Łowcę mającego odwrócić uwagę i kupić czas
            * TAI Semla została odpowiednio Ograniczona.
    * Tezifeng na pewno zaraził co najmniej jedną jednostkę.
* Teraźniejszość
    * Kurzmin informuje Ariannę; normalnie wysłałby innych z Aurum by nie było hańby, ale nie może (Castigator w ruinie). A Klaudiusz mimo pewnych... cech... jest pożyteczny i pomocny
        * Tezifeng leci w kierunku na jednostkę medyczną 'Maiatea', jest poza efektywnym zasięgiem powrotu na Castigator.
        * "Czy możecie ich zasilić i z nimi wrócić? Coś się musiało stać. Normalnie wysłałbym Elenę, ale..."
* Frakcje
    * Kurzmin: uratuj Natalię, Klaudiusza i załogę
    * Orbiter: znajdź Syndykat
    * Esurienci:
        * cele
            * dodać ofiary do Ekstaflos (kraloth flower), poznać ich strach i złamać ciała i umysły
            * priorytety: Arianna > kobiety > inni. Hunger and craving.
        * siły
            * Manekiny: zaadaptowane flesh-dolls służące Ekstaflosowi
                * nieważne jakiej płci byli załoganci, wszyscy są teraz już kobietami
            * Inflorescent: sercem jest w _life support_, ale jego macki są na całym statku. Zeiram-face-like.

### Co się stanie (what will happen)

* Opowieść o: 
    * "Pechowe spotkanie z Syndykatem i kralotycznym sojusznikiem Syndykatu zniszczyło jednostkę"
    * "There are far worse things and fates than death, everything can be used"
    * **SUKCES**: uratowanie (swojej załogi) > (TAI (informacja)) > (maga) > ("flesh mannequins")
* F1: _Jeszcze Castigator_
    * Eustachy napotyka niewielkie ognisko Skażenia. Tam jest też Elena. Pierwsze spotkanie.
        * Elena wpakowała się w środek, otoczona ze wszystkich stron (bo ratowała ludzi)
        * Eustachy ma pod sobą mechanizm szczękokształtny
        * Ludzie uciekają w próżnię
    * Kurzmin informuje Arię o Tezifeng
* F2: _Tezifeng, intruzja_
    * Piękne dziewczyny przejmują Zespół.
    * Granat dehermetyzuje, wpuszczony jest _bioadapter kralotyczny_
    * "dziewczyny" okazują się Manekinami
        * błyszczące srebrne włosy i oczy, które przypominają rozgwieżdżone niebo, emanuje zimną elegancją
        * głęboko zielone oczy i delikatna karnacja
        * Egzotyczna Piękność o błękitnej skórze, płynne, czerwone włosy poruszają się jak żywe płomienie.
        * piękne fioletowe oczy zdają się czytać twoje myśli, niezwykle kształtna sylwetka
    * DARK: 
        * infekcja Leony, infekcja Martyna, infekcja wszystkich obecnych. Status "kralothtouched"
        * Infernia strzela i niszczy fragment Tezifeng, wysysając wszystkich w kosmos i uszkadzając swoją strukturę
        * Tezifeng przechwytuje 8 członków załogi Inferni, podkłada 1 Manekin i kilkanaście Robaków
* F3: _Tezifeng, arcymag kralotyczny_
    * magia
        * Manekiny są uzbrojone; przekształcane są szybsze i drapieżniejsze, nieprawdopodobna kwasowość ciała
        * Dark Infection (spawn eel monster), enslaved
        * Wave of pain, Wave of Extasy, Wave of Command
    * Manekiny
        * szybkie, częściowo płynne, ostrza z kości
        * słabe ppanc
        * Extasy and Command
    * Inflorescent
        * len 5 (żywotność, giną też jego podwładni)
        * len 5 (mentalnie, zakochany w swoim świecie gdzie on rządzi)
        * wszystko porośnięte pulsującymi żyłokształtymi plechowatymi bluszczami


## Sesja - analiza

### Fiszki

Castigator:

* Leszek Kurzmin: ENCAO: 0-+++ | Żyje według własnych przekonań;; Lojalny i proaktywny| VALS: Benevolence, Humility >> Face| DRIVE: Właściwe miejsce w rzeczywistości.

Zespół:

* Leona Astrienko: psychotyczna, lojalna, super-happy and super-dangerous, likes provocative clothes
* Martyn Hiwasser: silent, stable and loyal, smiling and a force of optimism
* Raoul Lavanis: ENCAO: -++0+ | Cichy i pomocny;; niewidoczny| Conformity Humility Benevolence > Pwr Face Achi | DRIVE: zrozumieć
* Kamil Lyraczek
    * OCEAN: (E+O-): Charyzmatyczny lider, który zawsze ma wytyczony kierunek i przekonuje do niego innych. "To jest nasza droga, musimy iść naprzód!"
    * VALS: (Face, Universalism, Tradition): Przekonuje wszystkich do swojego widzenia świata - optymizm, dobro i porządek. "Dla dobra wszystkich, dla lepszej przyszłości!"
    * Styl: Inspirujący lider z ogromnym sercem, który działa z cienia, zawsze gotów poświęcić się dla innych. "Nie dla chwały, ale dla przyszłości. Dla dobra wszystkich."
* Horst Kluskow: chorąży, szef ochrony / komandosów na Inferni, kompetentny i stary wiarus Orbitera, wręcz paranoiczny
    * OCEAN: (E-O+C+): Cichy, nieufny, kładzie nacisk na dyscyplinę i odpowiedzialność. "Zasady są po to, by je przestrzegać. Bezpieczeństwo przede wszystkim!"
    * VALS: (Security, Conformity): Prawdziwa siła tkwi w jedności i wspólnym działaniu ku innowacji. "Porządek, jedność i pomysłowość to klucz do przetrwania."
    * Core Wound - Lie: "Opętany szałem zniszczyłem oddział" - "Kontrola, żadnych używek i jedność. A jak trzeba - nóż."
    * Styl: Opanowany, zawsze gotowy do działania, bezlitośnie uczciwy. "Jeśli nie możesz się dostosować, nie nadajesz się do tej załogi."
* Lars Kidironus: asystent oficera naukowego, wierzy w Kidirona i jego wielkość i jest lojalny Eustachemu. Wszędzie widzi spiski. "Czemu amnestyki, hm?"
    * OCEAN: (N+C+): bardzo metodyczny i precyzyjny, wszystko kataloguje, mistrzowski archiwista. "Anomalia w danych jest znakiem potencjalnego zagrożenia"
    * VALS: (Stimulation, Conformity): dopasuje się do grupy i będzie udawał, że wszystko jest idealnie i w porządku. Ale _patrzy_. "Czemu mnie o to pytasz? Co chcesz usłyszeć?"
    * Core Wound - Lie: "wiedziałem i mnie wymazali, nie wiem CO wiedziałem" - "wszystko zapiszę, zobaczę, będę daleko i dojdę do PRAWDY"
    * Styl: coś między Starscreamem i Cyclonusem. Ostrożny, zawsze na baczności, analizujący każdy ruch i słowo innych. "Dlaczego pytasz? Co chcesz ukryć?"

Tezifeng: 

* Klaudiusz Terienak: ENCAO: +-000 |Bezkompromisowy, nieustępliwy| VALS: Power, Stimulation| DRIVE: Przejąć władzę

### Scena Zero - impl

Leszek Kurzmin. Zaprosił Ariannę po Castigatorze. Eustachy jeszcze czyści Castigatora. Leona też. Muszą się wybiegać

* Kurzmin:  Arianno, nie wiem jak poradziłaś sobie z tą cholerną Królową. Oni są niemożliwi.
* Arianna:  Starzy załoganci?
* Kurzmin:  Wszyscy tienowaci... przepraszam.
* Arianna:  Byłam młodsza, bardziej cierpliwa, podobne podejście jak Leona.
* Kurzmin:  A ja mam tylko Elenę... lub aż Elenę.
* Arianna:  No... 
* Kurzmin:  Myślałbym, że przez to że się znamy będzie pożyteczna.
* Arianna:  Bardzo źle jest?
* Kurzmin:  Jest oddalona od wszystkich. Wykona polecenie.
* Arianna:  Nie gorzej niż zawsze.
* Kurzmin:  Mam prośbę. Dyskretną. Zresztą, zrozumiesz.
* Kurzmin:  Są różni tienowie. Mam dwójkę, wysłałem ich na patrol. To Klaudiusz Terienak, którego kojarzysz. I Natalia Gwozdnik. Są na patrolu. Powinni byli wrócić. Nie wrócili.
* Kurzmin:  Może tak powiem - Natalia jest jak Elena jak chodzi o zasady.
* Kurzmin:  Nie, więcej, transponder działa. Wiem gdzie są, ale mają najpewniej awarię komunikacji. Sęk w tym, że są poza obszarem powrotu.
* Kurzmin:  Normalnie wysłałbym moich. To się zdarza. I oni mogli coś zrobić bo będą "bezpieczni".

Żeby nie mieli za dużych kłopotów. Niepokoi mnie że nie mają komunikacji. Ale się ruszają, transponder działa, wszystko działa.

Leona zakład z Eustachym. Możemy im pomóc - niszczyć "potworki". Echa Saitaera. 

* Eustachy: "Chodźmy, pomóżmy, ale jeden raz zachowujmy się odpowiedzialnie."
* Leona: "(spojrzenie z niewiarą) SERIO? Ty? Pierwszy niszczyciel?"
* Eustachy: "Ten JEDEN raz"

Tr Z +3:

* V: Leona zaakceptowała warunki, acz jest w szoku.
    * E: "zawsze mogę wpaść z ogniem i mieczem, ale wpaść ze scyzorykiem i świerszczykiem i nie wysadzić to sztuka, w moim wykonaniu"
    * Leona: "bo jesteś magiem. Mi nie pozwalają wpaść z ogniem i mieczem." (chwila myślenia) "w sumie nie pytam. W sumie i tak to robię."
    * E: "no właśnie, pokażmy że umiemy inaczej by wkurzyć Ariannę by wiedziała że możemy ale nie chcemy!"
    * Leona: "O_O /śmiech"

Eustachy i Leona czyszczą "śmieci" post-Saitaerowe. Leona opowiada dowcipy o blondynkach i się śmieje, potem o zwłokach i też się śmieje.

Tr Z +3:

* X: E+L czyszczą, czyszczą, i - z tyłu coś Was zaszło. Leona by była w stanie poradzić. Tyle, że coś zestrzeliło to cholerstwo. Ktoś inny. Ignoruje Was ta jednostka, z Castigatora.
    * Leona: "ukradła mojego killa! Ja liczę ile zabiłam! Nie zrobisz z tym nic?! -> E"
    * E: "zabite to zabite..."
* Xz: Leona: "Eustachy, ktoś Ci wsadził neuroobrożę. Ale ja mam w naszym związku jaja." (Leona leci w kierunku tamtego servara). Eustachy, z trudem, za nią.
    * Leona: "Oi, zabiłaś mojego killa! Wisisz mi jednego stwora, mamy konkurs!"
    * Elena: "Jeśli Wy polujecie na stwory żeby... kto zabije więcej, to idźcie gdzieś indziej. Ja czyszczę Castigator."
    * Eustachy: "pan maruda, niszczyciel uśmiechu, może dołączysz do naszych małych zawodów, rywalizacja -> wydajni"
    * Elena: "pani maruda, jeśli można. I ja jedna pokonam Waszą dwójkę."
    * Eustachy: "nie chamuj się skarbie, pokaż gdzie jej miejsce; przełknąć dumę i pewność siebie"
* X: Leona jest WŚCIEKŁA.
    * Leona: "moja droga, zgadzam się na to, pod warunkiem, że dodamy coś do tej pięknej kolekcji. Ta osoba która przegra biega nago dookoła Castigatora."
    * Elena: "moment, na takie rzeczy się nie zgadzam!"
    * Leona: "bo przegrasz, i wiesz o tym"
    * Eustachy: "oddaję Ci pola byś mogła pokazać jej gdzie jej miejsce"
    * Elena: "nie będę... nie chcę Cię widzieć nago, WSZYSCY Cię widzieli nago!"
    * Eustachy: "czego nie można powiedzieć o Tobie?!"
        * "EU: jestem na etapie patrzę jak świat płonie XD"
    * Eustachy: Leona, nie zabijaj, możesz okaleczyć ale... nie zabijaj...
    * Leona: Ciebie, droga tienko nie zabiję bo jesteśmy w kosmosie. Ale będziesz biegała nago dookoła Castigatora. Jeśli ja przegram to co?
    * Elena: E?
    * Elena: Nic od Ciebie nie chcę, jesteś... jakaś chora.
    * Leona: (-> Eu) Czy ja jestem chora, sadystyczna albo nienormalna? To nie jest pytanie podchwytliwe ale jest jedna odpowiedź
    * Eustachy: Troszkę... czasem... ale nie trzeba korygować.
    * Leona: Moja droga, jesteś TCHÓRZEM. Boisz się. Przegrasz z człowiekiem. Masz przewagi - znasz stację, statek, ale boisz się ze mną przegrać.

Tr Z +4 (taunting):

* X: 
    * Elena: nie będę biegała nago cokolwiek się nie stanie, nie ma takiej opcji, jesteś... jesteś CHORA. Nie jestem taką dziewczyną jak Ty.
    * Leona: o, pierwsza święta tienka.
    * Eustachy: da się wymyśleć coś innego
    * Leona: no właśnie, Eustachy ma zawsze dobre pomysły (i patrzy na Eustachego z wyczekiwaniem)
    * Eustachy: zabawa w służkę? Przez jakiś czas. (+1Og)
* V (p): 
    * Elena: zakład z Tobą zrobię. Nie ma szans, nie masz TY żadnych szans. ŻADNYCH.
    * Leona: będziesz służyć na kolanach, zobaczysz. Tackę będziesz nosić. Z jajkami na miękko aż będą miękkie jak lubię.
* Vz: 
    * Elena: wchodzę w to. (wściekła) (wściekły sposób)
    * Leona: (wściekła). (sadystyczny sposób)
* V: obie idą na pełną moc...

...

* Or: rany Eleny.
* Og: rany Leony.
* Ob: (1Ob) magia Eleny
* Leona: zabija lepiej, wspomagana, (większość osób chce porażki Eleny) : 6V3Vz+3Or
* Elena: omnidetekcja, znajomość jednostki: 6X+3Og

6V6X3Vz3Or1Ob (V: Leona, X: Elena)

* V: Leona zaczyna i skutecznie eksterminuje - ma lepszy sprzęt i jest szybsza. Elena idzie na całość: +2Or
* V: Leona po prostu jest szybsza i skuteczniejsza w eksterminacji, Leona jest szybsza i szybciej dociera. Leona - ona jest w miarę blisko by Elena nic nie zdążyła. "moje kille, zabiłaś MOJEGO pająka"
* X: Elena wpakowała Leonę na delikatny teren i Leona tam ugrzęzła. Sama zaczęła zbierać pająki jak szalona. Teraz Leona się wściekła i stara. +3Oy
* X: Elena używa omnidetekcji, skutecznie i szybko czyści "gniazda" i Leona szybciej zabija, ale zabija 2-3 naraz a Elena miejsca gdzie są po 10. +2Oy
* Oy: Leona wygrała. Połączenie chemikaliów, uszkodzonego kombinezonu itp, plus popchnięcie Eleny. Bardzo bliska walka, obie były dobre, ale Leona była lepsza.

.

* Leona: "chcesz mi służyć w fartuszku czy bez?"
* Elena: "(żebyś spłonęła w ogniu!)"
* Leona: "byłam tam, spoko miejsce. Jak wrócę z akcji to pogadamy, moja droga. Jajka na miękko. Dobrze, że się obroniłaś przed bieganiem nago!"

.

* Leona: "Eustachy, załatwiłeś mi NAJLEPSZĄ ROZGRYWKĘ EVER"!
* Eustachy: "A mówiłem, zróbmy to jak dorośli ;-)"

TYMCZASEM, monitoring Castigatora wszystko to wykrył. I Kurzmin patrzy na Ariannę, ona na niego i nic nie mówią.

* Kurzmin:  To ta od bicia szlachty?
* Arianna:  To była Elena, tak?
* Kurzmin:  Tak. Sama rozumiesz. Ale nie użyła magii. (pocieszająco)
* Arianna:  To dobrze... faktycznie czyścilibyśmy... wyrobiła się...
* Kurzmin:  Jak nie wiem jak ja ją ustabilizuję, nie ma przyjaciół i to wszystko... odbije się przeciw niej.
* Arianna:  Leszek jakbym wiedziała jak jej pomóc, pomogłabym na Flarze.
* Kurzmin:  Poradzimy sobie /fałszywy głos że będzie dobrze

### Sesja Właściwa - impl

Parę godzin później Leona cała szczęśliwa siedzi u Eustachego i je hot-dogi; glow-up. Zbliżamy się do Tezifeng. Powinien być w komunikacji krótkodystansowej. Jest połączenie.

* Klaudiusz:  Tu Klaudiusz Terienak, jednostka Aurum..? /zdziwienie "Infernia?"
* Arianna:  Arianna Verlen, OO Infernia.
* Klaudiusz:  Kapitan Verlen? /dzika radość Jak ja Cię... PANI dawno nie widziałem
* Arianna:  Ja Ciebie też, Klaudiuszu, zepsuła się kom. są problemy...
* Klaudiusz:  Przechwyciliśmy ważną osobę. Blackops Orbitera. Mamy nie komunikować się z nikim szczególnie, mamy dostarczyć ją do medycznego.
* Arianna:  Nie zaraportowaliście C?
* Klaudiusz:  Bo nam nie wolno. Ograniczyła nam TAI.
* Arianna:  Gdzie się spotkaliście z blackops?
* Klaudiusz:  Nie mogę powiedzieć przez komunikację. Ale możecie się z nią spotkać. To ważne, by... nikt nie wiedział (konspiracja)

Infernia skanuje Tezifeng. Szuka czy sygnatury się zgadzają itp. Nawet TAI im nie działa dobrze XD. Lars skanuje.

Tr +2:

* X (p): Arianna widzi maskowanie. Da się je spenetrować. Ale Tezifeng nie ma maskowania.
    * coś w środku zagłusza, skądś przyszło. Agent musiał przynieść.
* V (p): Skan pokazuje, że na pokładzie jest wszystko nie tak. Sygnatury życia się nie zgadzają.
    * jest... dwa razy więcej osób? Ale nie ma ekstra sprzętu. Nie '20' a '40' osób, ale nie ma ekstra sprzętu
    * jest maskowanie ORAZ sprzęt zagłuszający komunikację

.

* "Leona zaraportuj że szalet czysty"
* Klaudiusz jak zobaczył Leonę to się wzdrygnął ale potem się ucieszył. "Pani kapitan ma na pokładzie... współczuję"
    * Ale NAPRAWDĘ się ucieszył.

Arianna próbuje skontaktować się z bazą by dostać info o agencie blackops "nie musicie potwierdzać ani zaprzeczać, ale czy mamy otworzyć ogień". Zdobycie informacji na szybko.

Tr +4 (bo jak coś się dzieje to MUSIMY wkroczyć) + 2Og (bo Arianna się MEGA rzuca i jeśli tam coś się dzieje):

* Og (p): pod wpływem silnych namów ze strony Arianny przegrzebali szybko i ją spriorytetyzowali
    * "tam są pewne działania, ale nie powinny być na tej jednostce jeśli coś się naprawdę nie spieprzyło"
    * nie mają uprawnień do ograniczania Semli.
    * jeśli nie rozwiążecie czysto, 
* X (p): szarogęszenie się Arianny bardzo zirytowało niektóre osoby.
* X (p): mają trochę więcej czasu niż Arianna by chciała. Idzie wolniej.
* X (m): PRZERYWAMY I WKRACZAMY, za długo trwa - ale najpewniej nie ma agenta.

Dwie pinasy. Raoul jako pierwsza pinasa, on za pierwszą pinasą, robi przekaźnik. 'piercować' przez zagłuszacz a z drugiej strony "wszyscy". Flota inwazyjna. Maskowanie jest dwustronne. Arianna podejrzewa Syndykat...

* Inwazja:
    * Arianna, Leona, combat medic, combat engineer: "dostarczamy paliwo"

plan taktyczny - niech tu jest komitet spodziewający się jednego kolesia z paliwem. 

Tr Z+4:

* Vz: Arianna, Leona i ekipa się bez problemu wbili.
    * Już na wejściu masz wrażenie coś nie tak, ale Lars. Lars MEGA NUDNĄ PREZENTACJĘ.

"Egzotyczna Piękność o błękitnej skórze, płynne, czerwone włosy poruszają się jak żywe płomienie". Ona na to "O".

* Vr: Leona uderzyła by ogłuszyć. Sukces.
* X: Arianna czuje emanację magiczną skierowaną na E.P. To magia życia. Lecznicza. Skażona.
* (+magia arcymaga) V: udało się wypiąć E.P. z tej sieci sympatii.
    * Moc Klaudiusza ale INNA. I w ciele tej dziewczyny też jest Klaudiusz. Jego ślad. Tkanka. Ale dziwna.
    * Klaudiusz poczuł CIERPIENIE
* (-magia arcymaga) Vz: Arianna wycofała się z całą ekipą i z jeńcem.
* V: Arianna zrobiła _sweep magiczny_ korzystając z sytuacji
    * moc Klaudiusza jest bardzo wzmocniona. Bardzo. One wszystkie go zasilają.
    * sam Klaudiusz ma jakąś groteskową formę. Coś go zmieniło. Jest jak... rdzeń chorego kwiatu.
    * a w środku każdej dziewczyny jest (kralotyczny wij)

Wycofujemy na Infernię. A dziewczynę do badań.

Jednak wzywamy SOS - kralotyczne skażenie. Nie macie siły na to. MOŻEMY działać, ale NIE URATUJEMY.

Tezifeng jak tylko Arianna spróbowała się oddalić w pintce, ruszył i rozgrzał broń. Eustachy na Inferni widząc to też rozgrzał broń.

* Klaudiusz: "Arianno, jeśli nie możesz być moja, będziesz martwa"
* Arianna:  "Wypuść mnie a dostarczę Ci Leonę na pokład" (przeciągam)
    * -> E: (STRZELAJ PIERWSZY!!!!)
* Leona: "Co?" (patrzy na broń) "Znaczy tak, dobry pomysł, chciałabym" (dziki uśmiech)

Tr Z+4: (Klaudiusz się rozślinił)

* V: Klaudiusz strzela po Eustachym
* X: Pintka nigdzie nie poleci
* V: Klaudiusz jest ZASKOCZONY przez Eustachego

Eustachy rozbraja korwetę - niszczy działka.

Tr +3:

* V: Działka korwety są zniszczone
* X: Część załogantów nie żyje. 4-5.
* X: Zniszczone mostek i komunikacja. Statek wymaga poważnych napraw.
* V: Silnik korwety zniszczony
* Vr: Wszelkie formy komunikacji zniszczone, WIĘC nie ma możliwości szantażu. I zniszczone zagłuszanie.

Ta jednostka nigdzie nie poleci, Wy macie dowód (E.P.), magia ma 'void gap'. Efektywnie - korweta jest niegroźna.

I druga pintka leci po Ariannę...

## Streszczenie

Jeszcze na Castigatorze, Arianna i Kurzmin z przyjemnością odbudowali znajomość i wspominki. Gdy Elena chcąc ratować Leonę 'zabiła' jej anomalię, Leona wyzwała ją na pojedynek. Elena przegrała, choć było blisko; będzie musiała być służką Leony przez pewien czas, co Castigator odnotował z radością. Kurzmin poprosił Ariannę o znalezienie i pomoc dwóm tienom w rutynowym patrolu (najpewniej przekroczyli paliwo). Niestety, Tezifeng, korweta Castigatora podczas operacji celnych natrafiła na coś kralotycznego. Klaudiusz Terienak został zmieniony w ekstaflos a Natalia Gwozdnik została porwana. Pułapka na Infernię została wykryta przez Ariannę (bo Klaudiusz zachowywał się nie tak jak powinien). Szybka operacja wejścia - wydobycia jednego jeńca (marionetki kralotycznej) i zbadanie sytuacji sprawiła, że Eustachy działkami unieszkodliwił Tezifeng i Arianna wezwała wsparcie - kraloth to coś bardzo wysokiej klasy.

## Progresja

* Leona Astrienko: Elena będzie jej służką przez jakiś tydzień gdy będzie okazja; wygrała zakład eksterminacji małych 'ech Saitaera' na Castigatorze.
* Elena Verlen: będzie służką Leony przez jakiś tydzień gdy będzie okazja; przegrała zakład eksterminacji małych 'ech Saitaera' na Castigatorze.
* Elena Verlen: serio nie cierpi Eustachego i Leony. Naprawdę. Może nie do poziomu nienawiści, ale są jej najmniej ulubionymi ludźmi.
* OO Tezifeng: ostrzelana przez Infernię, potrzebuje dobrych 3 tygodni napraw, co najmniej.

## Zasługi

* Arianna Verlen: z przyjemnością odbudowała znajomość z Kurzminem, martwi się izolacją Eleny i pomoże Kurzminowi z dwoma zaginionymi tienami. Gdy się okazało, że coś dziwnego się dzieje na Tezifeng, nie weszła na pokład 'po prostu' tylko weszła z dywersją. Kazała wycofać oddział po złapaniu jeńca a potem odwróciła uwagę ekstaflorisa, by Eustachy unieszkodliwił Tezifeng. Świetnie rozegrana akcja.
* Eustachy Korkoran: redukował szaleństwa Leony z zakładami, ale gdy Leona weszła w starcie z Eleną to jej dopingował i podżegał. Skutecznie ostrzelał Tezifeng i mimo strat w 'kralotycznych marionetkach' na Tezifeng, unieszkodliwił ten statek tak, że ekstaflos nie był w stanie zrobić niczego sensownego.
* Leszek Kurzmin: z przyjemnością odbudował kontakt z Arianną; poprosił ją o dyskretne rozwiązanie sprawy z Klaudiuszem i Natalią. Wyraźnie próbuje robić dobrą minę do złej gry, pomagać młodym tienom i wydobyć najlepsze diamenty z tego co tam ma.
* Leona Astrienko: weszła w zakład z Eustachym kto więcej zniszczy 'ech Saitaera' po Rozalii. Tauntowała Elenę w zakład i go wygrała (acz było blisko); Elena będzie jej służką przez pewien czas. Potem zrobiła insercję na Tezifeng; ogłuszyła kralotyczną Marionetkę (z trudem) i ją przechwyciła. Ogólnie, świetny dzień.
* Elena Verlen: sama, na boku od wszystkich. Weszła w konflikt z Leoną (nie sprowokowała!) i weszły w zakład o zabijanie 'Skażeń Saitaera'. Przegrała zakład, przez co będzie musiała być służką Leony przez jakiś czas.
* Raoul Lavanis: desygnowany przez Ariannę do bycia dywersją na pintce. Zadanie spełnił bez słowa i bez błędu. Niewidoczny i skuteczny.
* Lars Kidironus: paranoiczny; z braku Klaudii on skanował Tezifeng, próbując określić, czy sygnatury życia się zgadzają. Przed insercją zrobił "MEGA NUDNĄ PREZENTACJĘ" - każdy członek załogi i jak groźny. O dziwo, pomogło to Ariannie, która poznała brak Egzotycznej Piękności.
* Klaudiusz Terienak: podczas działań celnych napotkał na kralotha, co zmieniło go w ekstaflos. Stał się żywą pułapką, infekującą statki i przekształcającą ludzi w piękne dziewczyny w swoim Kwiecie. Najbardziej na świecie pragnął zdominować Ariannę i Leonę, ale jego pułapka nie zadziałała. Przechwycony przez siły Orbitera.
* Natalia Gwozdnik: podczas działań celnych napotkał na kralotha, zniknęła. Jej los jest nieznany. Podobno bardzo obiecująca tienka na Orbiterze.
* OO Tezifeng: korweta szybkiego i dalekiego patrolowania przypisana do Castigatora; pełniła rolę jednostki celnej (celnik). Spotkała kralotha i Klaudiusz Terienak stał się ekstaflosem. Ostrzelana przez Infernię, wymaga solidnych napraw.
* OO Infernia: szybka i skuteczna; przechwyciła Tezifeng i z woli Eustachego ją ostrzelała. Zero uszkodzeń, zero nieefektywności. Idealna misja dla Inferni.
* OO Castigator: uszkodzony zewnętrznie i strukturalnie, lekko Skażony, ale ogólnie działa bez zarzutu. Centrum dowodzenia Kurzmina i tymczasowa platforma szkoleniowa...

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita

## Czas

* Opóźnienie: 1
* Dni: 2

## OTHER
### Status Skażony

* bardziej wrażliwy na energię magiczną i silniejsza magia, ale mniej kontrolowana
* ZAWSZE: +1X, +1Ob
* DODATKOWO JEŚLI MAGIA: 1V -> 1Vb, 1X -> 1Xb, +1Ob

### Status Rozproszony

* pod wpływem silnych emocji / myśli o czymś innym
* -1V, 3X -> 3Xg, +3Og
* Og reprezentuje "skupiony na swoich myślach i swoim działaniu" lub "działanie automatyczne"
* można wymienić Og na Or - sukces ALE zadajesz sobie ranę by się skupić

### Status Solidnie Ranny

* osłabiony i mniej zdolny do działania
* -1V, 2V -> 2Or (sukces ale dalsza rana); możesz usunąć Or
