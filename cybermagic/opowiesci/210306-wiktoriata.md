## Metadane

* title: "Wiktoriata"
* threads: legenda-arianny
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [210302 - Umierająca farma biovatów](210302-umierajaca-farma-biovatow)

### Chronologiczna

* [210302 - Umierająca farma biovatów](210302-umierajaca-farma-biovatow)

## Punkt zerowy

### Dark Past

* .

### Opis sytuacji

* .

### Postacie

* .

## Punkt zero

N/A

## Misja właściwa

Ktoś musi udać się do pracy administracyjnej. Apollo i Viorika walczą o prawo nie bycią tą osobą. Apollo jest od niej starszy i bardziej doświadczony. Viorika ma dopiero 18 lat, ma za sobą urok, umiejętności taktyczne i ogólnie rozumianą młodzieńczą arogancję. Apollo ma za sobą fakt, że regularnie trenuje z żołnierzami. Viorika była bardziej zadaniowa - ma coś do robienia to robi. W związku z tym jej żołnierze są dobrze dowodzeni (przez Viorikę), ale aż tak bardzo jej nie ufają jak żołnierze Apolla ufają swojemu szefowi.

W związku z tym gdy sierżant Czapurt zaproponował Viorice, że może wygrać prawo nie wykonywania pracy administracyjnej, Wirek się ucieszyła. Zwłaszcza, że Viorika jest LEPSZYM taktykiem niż Apollo.

Dwa oddziały po 30 osób. Viorika i Apollo nie uczestniczą w ćwiczeniu - widzą mapę i wydają polecenia. Oddziały są uzbrojone w paintball. Mają za zadanie kontrolę punktów - klasyczny Domination.

Apollo i Viorika rozmawiają wcześniej. Krzyżowo próbują się zdrażnić i podejść: Viorika przypomina fuckup Apolla, Apollo zauważa, że Viorika do wszystkiego używa sentisieci i niczego innego - i na pewno nie będzie dobrze dowodzić oddziałem. Apollo wie, że Viorika jest dobrym taktykiem i ma nadzieję wprowadzić ją w pułapkę - jeśli Viorika skupi się na zbyt zaawansowanym planie, za dużo zaplanuje i za mało zostawi inicjatywy żołnierzom, to tu będzie kłopot. Plus, Apollo zrobił mowę swoim żołnierzom - powiedział, jakie są stawki. Żołnierze Apolla mu wierzą i mu ufają, daleko się dla niego posuną. Viorika po prostu dostała kompetentny oddział, to nie są "żołnierze Vioriki".

Viorika próbuje wkurzyć Apolla, on ją wyprowadzić i wymanewrować: (XXXVVX). Apollo w warunkach społecznych wymanewrował Viorikę. Udało jej się wkurzyć Apollo i "wygrała pojedynek słowny", dogadała mu. Ale on dał radę wmanewrować ją na to, że musi zaplanować DOKŁADNIE, bo to ONA ma wygrać. Czyli coś czego Viorika nigdy by nie zrobiła - przyniosła plan dokładniej niż zwykle. Jak było z Lucjuszem - Viorika nic nie wiedziała o terenie więc dała 100% autonomii. Tym razem Viorika zrobiła dobry plan, ale ten plan zakładał, że jej ludzie zachowają się prawidłowo. Innymi słowy, że będą kompetentni i entuzjastyczni, że koordynacja zagra. A Apollo w rzeczywistości rozgrywa to tak, by disruptować tą koordynację. 

* Przewaga wynikająca z rozmowy: +3 A.
* Oddział Apolla, randomy Vioriki: +2 A.
* Przewaga wynikająca z taktyki: +2 V.
* Baza obu oddziałów: 5.

Wynik:

* Pierwsze pozycje, pierwsze zajęcie terenu. (+2 za taktykę): XVV. Wygrywa Viorika. Jej plan był świetny, udało jej się pierwszej doprowadzić swoich do odpowiedniego miejsca. (+V)
* Ludzie Apolla atakują konkretne pozycje bardzo mocno, coś co jest nielogiczne ale uderza w MORALE. To jest coś, czego Viorika nie przewidziała - jej zdaniem Verlenowie są kompetetni. Apollo natomiast poszedł w terror / disruption (+2 morale): XX. Udało mu się rozbić jeden z suboddziałów. (+X): udało mu się sprawić, że część odwodów Verlenów straciło pozycję. Viorika straciła część sił. (+XX)
* Celem ćwiczenia jest utrzymanie punktów przez pewien czas. Viorika ma większość punktów, ale skeleton crew. Więc Viorika zmieniła plan: wysyła 2 skautów którzy mają dawać sygnał jakby było to 10-12 osób. Taki młot do uderzenia w plecy. Kupić czas. XVV -> straciła tych skautów, it was cold-blooded. Ale udało się spowolnić na tyle, że zyskała sporo punktów.
* BATTLE RAGE. Żołnierze Apolla po prostu próbują się przebić. Fakt, że Viorika straciła część oddziału jako dywersję jedynie ich upewnia w tym. A Viorika "oddaje" przednie punkty by zająć tylne punkty Apolla.: (+1A). VXX. Apollo wygrywa.
* Plan Vioriki w akcji: VXXX -> plan jest świetny, działa. Ale Viorice kończą się ludzie. She lost it.

Plan Vioriki był obiektywnie lepszy. W warunkach polowych, bojowych najpewniej by zadziałał lepiej, z mniejszymi stratami i lepszym sukcesem. Ale fundamentalnie ćwiczenie wygrał Apollo - na linii społecznej i miłości żołnierzy do siebie.

Wieczorem, gdy Viorika, wściekła jak osa i trochę podłamana siedzi w kwaterze, odwiedził ją sierżant Czapurt. Spytał Viorikę, czemu przegrała. Viorika stwierdziła, że nie ustawiła prawidłowo łańcucha dowodzenia - nie wykonali jej poleceń dokładnie. To jest prawda, gdyby byli automatami, słuchaliby jej poleceń dokładnie. Sierżant Czapurt parę kolejnych godzin wyjaśnił jej dlaczego relacje z żołnierzami są ważne, jak to wygląda i dlaczego Apollo wygrał. Viorika nie była szczęśliwa z wykładu, ale doceniła i uznała, że to jest kierunek który musi usprawnić.

Viorika powiedziała sierżantowi, że jej zdaniem "she was set up for fail". Sierżant nie zaprzeczył.

Viorika pogratulowała Apollo zwycięstwa. Walnęła sobie meliskę, ale pogratulowała. Podbije do niego tak, by to było słyszane przez ludzi z zewnątrz, ale nie w oczywisty sposób. Pozwala mu troszkę się chwalić i pokazać jaki jest epicki. Chodzi o to by Viorika wyszła jako ta, co pogratulowała uczciwego zwycięstwa i wyszła z tego z dumą.

Dwa dni później Viorika znalazła się w sekcji administracyjnej. A dokładniej - w miejscowości Poniewierz, dość spokojnej mieścinie która zajmuje się stabilizacją anomalii i rzeczy, które wydobyli z Trójkąta Chaosu. Viorika siedzi w papierach, niezbyt szczęśliwa. Totalnie nieszczęśliwa. A w miasteczku jest garnizon żołnierzy (tu musi być), znudzonych jak mopsy. Gdy nic się nie dzieje to się nie dzieje. Ale jak się dzieje - trzeba dużo i szybko.

Viorika nie wytrzyma, po prostu nie wytrzyma takiego po prostu nie robienia. Pracy z dokumentami. Tak więc zaczęła proponować rzeczy, robić rzeczy. Szybko dowiedziała się kilku faktów.

* Żołnierzy jest sporo. To sprawia, że raz na jakiś czas niewielkie oddziały robią rajdy do Trójkąta Chaosu. Nic szczególnie groźnego; ot, zebrać anomalie, poćwiczyć... - to jest forma rozrywki i zarobku
* Dom Uciech Wszelakich. To miejsce jest największym Domem Uciech (i nie tylko) w okolicy Holdu Bastion. Musi być.
* Od cywili się dowiedziała, że zrobiło się mniej bezpiecznie w okolicach Trójkąta. Zniknęła jedna grupka 3-osobowa eksploratorów. Ogólnie, Trójkąt jest groźniejszy niż był. Plotki mówią o Blakenbauerach.
* Najlepsza grupa żołnierzy ekstraktująca anomalie to "Zaginione Szpady". Są DUŻO lepsi niż jakiekolwiek inne oddziały, przynoszą o 300% więcej anomalii.
* Próba znalezienia zaginionych cywili zakończyła się niepowodzeniem. Najpewniej porwali ich Blakenbauerowie. Co dziwne, rodziny tych eksploratorów też zniknęły (parę dni potem). Stąd plotki o Blakenbauerach.
* Sporo wił jest w okolicy. Od niedawna. 2-3 tygodnie. Niegroźne, z zasady. Ale to dziwne.
* Mała potyczka z oddziałami Blakenbauerów w Trójkącie. Taka z rannymi.

Viorika ma dość opowieści o "Krwawej Lady Blakenbauer". Poszła pić z żołnierzami i opowiedziała serię dobijających anegdot. Chce przesunąć percepcję Mileny Blakenbauer z "okrutnej krwawej damy" na "słitaśna catgirl, 'I want to lick your blood, senpai!' ". A żołnierze tutaj nie znają Vioriki z tej "złej strony", dla nich jest młodą Verlenką która jest z nimi i dla nich sympatyczna. I jeszcze zesłana do dokumentów - czego NIKT nie chce robić i NIKT jej nie życzy. ExZ+3.

* XX: obraz zadziała - lokalnie. Milena Blakenbauer jest traktowana jako mem. Są jej obrazy.
* V: udało się pozbyć "Krwawej Lady Blakenbauer".

To nie jest tak, że nigdy ludzie nie znikają. Ale to, że rodziny zniknęły - to jest bardzo dziwne. Poszukiwacze (żołnierze) poszli dwa razy - w żadnym wypadku niczego nie znaleźli. Mag próbował pomóc, ale też niczego nie znalazł.

W związku z powyższym Viorika udała się do komunikacji z Lucjuszem Blakenbauerem. Może Blakenbauerowie coś wiedzą? Bo zwykle nie porywają rodzin w ten sposób - ale ślady wskazywały na obecność "żołnierzy Blakenbauerów".

Komunikacja hipernetowa. Lucjusz i Viorika. Viorika zaczęła normalnie "nie będę marnować Twojego ani mojego czasu, przejdę do rzeczy". Przypomniała, że on jej wisi. Powiedziała, że zniknęli ludzie. Nie jest to w stylu Blakenbauerów, ale były ślady ich jednostek. Niech Lucjusz ich znajdzie. Lucjusz wyraźnie się zdziwił. Poprosił o WSZYSTKIE informacje i ślady. Powiedział, że do niej wróci jak się wszystkiego dowie. Czegokolwiek dowie. Bo to faktycznie jest możliwe - ale Lucjusz nie wie niczego o tym.

Lucjusz vs Blakenbauerowie:

* X: Lucjusz nie jest w stanie znaleźć niczego odnośnie sprzętu i śladów wskazujących na Blakenbauerów.
* V: Lucjusz potwierdził, że SĄ te sprzęty pochodzące od Blakenbauerów. Ale te osoby zniknęły w okolicach Trójkąta.
* XX: Lucjusz powiązał to z siłami specjalnymi Blakenbauerów. A dokładniej - siły dowiedziały się, że Lucjusz się interesuje.
* VX: Lucjusz wykluczył powiązanie z Mileną lub Wiktorem. To jest jakieś działanie "wyżej". Jest jakaś akcja sił specjalnych Blakenbauerów w okolicach Trójkąta.
* VV: Lucjusz ma niezbite dowody na to, że Blakenbauerowie **nie są powiązani** ze zniknięciem. Im samym cywile też znikają.
* X: Lucjusz spalił super tajną i istotną operację Blakenbauerów. Kierownictwo jest BARDZO niezadowolone. Lucjusz spalił operację współpracując z Verlenami...

Lucjusz wrócił do Vioriki z informacjami, dwa dni później:

* Sprzęt który Viorika znalazła faktycznie należał do Blakenbauerów. Agenci którzy używali tego sprzętu byli KIA w dzikiej sentisieci. 2 lata temu.
* Lucjusz przekazał sygnaturę tego sprzętu, żeby Viorika mogła sama porównać. Nowy i tamten. Jak Viorika będzie widziała Blakenbauerowy sprzęt, będzie w stanie potwierdzić i datować po skorodowaniu dziką sentisiecią.
* Lucjusz potwierdził, że w okolicach znalazł biosygnatury wił Blakenbauerów. I tego nie umie wyjaśnić. Ale to nie są wiły "bojowe". One nie umieją walczyć. Może umieją opętać - cholera wie. Lucjusz wątpi.
* Wiły są niezdolne do używania tamtego sprzętu. Cokolwiek go używało to nie były siły Blakenbauerów. To musiało być coś innego lub ktoś inny.
* Na bazie informacji Blakenbauerów, duża szansa, że to ludzie Verlenów odpowiadają za porwania ich ludzi. Co, zdaniem Lucjusza, oznacza, że ktoś świadomie podkłada ślady.
* Na terenie Verlenów nie ma oddziałów Blakenbauerów. Jeśli były, nie ma ich w żadnych rejestrach czy informacjach. **Jedyne Blakenbauerowe cosie na terenie Verlenów to wiły.**

To ostatnie zdanie bardzo wzbudziło zainteresowanie Vioriki. Czyli Lucjusz przypadkowo wygadał się, że wiły na terenie są powiązane z Blakenbauerami. Viorika oczywiście nie dała po sobie pokazać, że Lucjusz coś powiedział. Viorika próbuje go zagadać, odwrócić uwagę... NIE. Viorika ma plan. Żeby odwrócić uwagę Lucjusza, zaprosiła go na obiad. Innymi słowy, obiad wdzięczności. W końcu mają neutralne miejsce ;-). (program "obiad za informacje": Viorika w to może iść i z radością wygra). Zdaniem Vioriki, Apollo byłby z niej dumny.

V vs L: TrZ+3:

* VVV: Viorika całkowicie odwróciła uwagę Lucjusza. On nie ma pojęcia, że jej powiedział. Jej eliminacja programu Blakenbauerów będzie kompletnie niezauważona aż będzie za późno.
* XXX: Viorika zostanie uznana przez Blakenbauerów za "mistrzynię szpiegów". She is the spymistress, kontrwywiad. Udaje niewinną i słodką, ale w rzeczywistości jej umysł stoi za dużą ilością kontr-planów.

Więc używając swojej potęgi mrocznej dokumentacji (bleh), Viorika zaczęła rozkazy przenoszenia wił. A jednocześnie zainteresowały ją te fakty odnośnie tego sprzętu. Viorika wie, że w okolicy są magazyny sprzętu odzyskanego z Trójkąta Chaosu i nie tylko. Tam na pewno są jakieś sprzęty Blakenbauerów. Np. takie, które udało się odnaleźć i odzyskać.

Viorika wie, że to nie Blakenbauerowie porwali tych ludzi i że Blakenbauerowie też stracili ludzi. Wie, że ktoś podkłada fałszywe dowody. Chciałaby pomocy Lucjusza - mało kto tak jak Blakenbauer potrafi szukać biośladów; plus, jeśli tam były ślady wił, to Lucjusz jest w stanie je przesłuchać :D. Więc - Viorika wpierw chce się skonsultować z sierżantem Czapurtem. Czapurt uznał, że plan Vioriki jest całkiem niezły. Autoryzował Viorikę do użycia aż do 50 żołnierzy z Poniewierza (50% żołnierzy Poniewierza). Viorika, wiedząc, że ma wsparcie militarne i błogosławieństwo starego sierżanta zdecydowała się na rozmowę z Lucjuszem.

Viorika poszła w taki plan - skoro i tak idzie z Lucjuszem na kolację, użyje flirtu jako narzędzie do przekonywania. Jakieś anegdotki, trochę zabawy - ale nic poważnego. She does intend to enjoy it. Wie, jaka jest opinia o Verlenach (że nie mają poczucia humoru i mają kij w tyłku) więc ma zamiar pozycjonować się inaczej. Plus... flirt dla flirtowania też jest ok ;-). W sumie - Lucjusz jest fajny. I egzotyczny.

Zatem na obiedzie trochę z Lucjuszem flirtowała, TROCHĘ, ale celem była wspólna akcja. Niech Lucjusz pomoże Viorice a Viorika pomoże Lucjuszowi. Znajdą i zaginionych Blakenbauerów i zaginionych Verlenów. No i cywili. Bo potrzebne są połączone siły. Plus jest ktoś, kto próbuje poszczuć na siebie oba rody by odwrócić od siebie uwagę. (TrZ+4): VXV: Lucjusz pomoże; co prawda będzie samemu na akcji, ale Blakenbauerowie nic nie wiedzą. Lucjusz jest całkowicie incognito. I odda się pod dowodzenie Vioriki - ona wyraźnie ma jakiś plan. On się dostosuje, będzie pełnił rolę doradczą (co, jak Viorika zauważyła, jest podobne do relacji z Mileną).

Lucjusz zaproponował prosty plan - w okolicy są wiły. On się dowie od nich co widziały. To może wskazać Viorice i Lucjuszowi w którą stronę iść. Viorice się ten pomysł BARDZO spodobał. Tak więc Viorika musiała tylko wprowadzić szczupłego, smutnie wyglądającego chłopaka (Lucjusz oczywiście przyszedł wyglądając zupełnie inaczej) do Domu Uciech Wszelakich. I, jako, że "chłopaczek" był z Vioriką, nikt nawet się nie zainteresował. A na miejscu Lucjusz z Vioriką zaczęli odwiedzać wszystkie wiły, po kolei. I nie po to co się wydaje ;-). Lucjusz przesłuchuje wiły w imieniu Blakenbauerów o to, co widziały. Viorika podpowiada pytania. Wiły ze szczęką ku ziemi odpowiadają - nie spodziewały się tien Blakenbauera przesłuchującego je w tej okolicy. (TrZ+2).

* V: Lucjuszowi udało się zlokalizować właściwe wiły - bo nie wszystkie coś widziały.
* XX: De facto? Wiły się wygadały odnośnie planów. Lucjusz wie, że Viorika wie. Viorika ma inne źródło, nie tylko Lucjusza (więc go można osłonić). Lucjusz zaczyna się orientować, że spieprzył.
* V: Są dwie grupy wił. Jedna odpowiada za infiltrację (Lucjusz jest zdziwiony, Viorika ma potwierdzenie), druga sprzedaje dopalacze (Lucjusz jest zdziwiony, Viorika też trochę). Tych drugich jest dużo mniej.
* XX: Część z wił wpadła w szok przy przesłuchaniach; Lucjusz musiał je ratować magicznie. Na to zareagował garnizon. Na to zareagowała Viorika. I całe przebranie Lucjusza psu w dupsko.
* V: Tylko wiły grupy PIERWSZEJ wpadły w szok. Te sprzedające - nie. One powiedziały, co się stało w domu rodzin eksploratorów.

To się zaczęło po tym, jak pewna populacja żołnierzy, którzy kupili środek psychoaktywny miała efekty uboczne. Ci, którzy mieli efekty uboczne zaczęli polować na ludzi. To faktycznie byli żołnierze Verlenów - po prostu używali sprzętu Blakenbauerów (wziętego z magazynu rzeczy odzyskanych). Wiły podeszły i przeszkodziły żołnierzom w udawaniu, że cywile uciekli - żołnierze chcieli cywili spakować i porwać. Obecność wił, z zaskoczenia, sprawiła, że żołnierze porwali ludzi i uciekli bez pakowania. Gdy wiły chciały zatrzeć ślady (nie wiedząc co robić), pojawili się INNI żołnierze Verlenów i wiły same pouciekały. Tak, podejmowanie decyzji przez wiły "co robić" zajęło kilka godzin... to nie są najmądrzejsze istoty. Nie w tej psychoformie.

Lucjusz poznał próbkę środka psychoaktywnego. To "wiktoriata", stworzona przez Wiktora Blakenbauera. Nie ma wielu znanych efektów ubocznych; psychoza jest czymś nowym. Sam środek jest stosunkowo nowy; Wiktor po prostu chciał sobie dorobić, najwidoczniej. A żołnierze chcieli być jak najlepsi. Wiły zidentyfikowały, co to za grupa żołnierzy - to "Zaginione Szpady", ci najlepsi. Ci najbardziej dekorowani. 8 osób.

Lucjusz nie ma pojęcia co te psychozy mogą z nimi robić. To coś nowego. Wiktoriata też jest czymś nowym. Wiły to wiły, nic nie rozumieją. Próbowały, ale, no, to tylko wiły.

Na informację o Blakenbauerze wpadło tu z 15 żołnierzy, w servarach i wszystkim. Uszkadzając budynek. Viorika musiała zasłonić sobą Lucjusza i powiedzieć, że ma autoryzację sierżanta Czapurta. Dla żołnierzy było to... (TrZ+4):

* V: wystarczające; oddali się pod dowodzenie Vioriki
* X: był incydent, czy dwa. Wystrzał z działka, zniszczenie sprzętu, takie tam. Za co Viorika i dowodzący poważnie opieprzyli znerwicowanego łapczaka (pierwsza akcja).
* V: dyskrecja. Nikt nic nie wie, na razie.

Viorika ma po swojej stronie zaskoczenie. Psychotyczni żołnierze Verlenów nie mają pojęcia, że ktoś wie o nich. Zachowują się "normalnie", poza momentami, w których wychodzą polować. A Viorika ma swoją grupę, która jest na służbie...

Nie. Plan Vioriki jest prosty. Środki usypiające. Jak będzie noc, Viorika po prostu zagazuje WSZYSTKICH śpiących żołnierzy. A psychotycznych wsadzi się do celi. (TrZ+3): 

* V: udało się ich zagazować i wsadzić do celi
* V: nikomu nic się nie stało; bez strat w ludziach
* V: wszystko jest wyciszone; nikt nic nie wie. To była w 100% dyskretna akcja i dobrze dobrany środek

Lucjusz został jeszcze tydzień z Vioriką. Musi pomóc tym biednym żołnierzom z antidotum i innymi środkami; drugą linią wysłał sygnał do Blakenbauerów a dokładniej Wiktora - zaprzestań dystrybucji tego gówna. Viorice spodobała się ta postawa.

Jeśli chodzi o nieszczęśników (Blakenbauerskich i Verleńskich cywili) - wszyscy byli torturowani. Nic nie chcieli osiągnąć, oni po prostu byli LEPSI od tych zwykłych ludzi. Illusions of grandeur. Z 15 zaginionych 12 udało się uratować...

## Streszczenie

Po przegranym pojedynku na oddziały z Apollem, Viorika trafia do papierologii w Poniewierzy. Tam dowiaduje się, że Blakenbauerowie chyba porywają ludzi. Współpracując z Lucjuszem Blakenbauerem, odkrywa prawdę - to psychoza wywołana przez narkotyki bojowe sprzedawane Verlenom przez Blakenbauerów. Przypadkowo z Lucjuszem rozbija siatkę szpiegowską Blakenbauerów na terenie Verlenów. Plus, zalicza początek romansu z Lucjuszem.

## Progresja

* Milena Blakenbauer: zamiast "Krwawej Lady Blakenbauer" jest "fear me, senpai! Kyaa!" na terenie rodu Verlen. Milenie się to NIE podoba, ale nie wie skąd się to wzięło. Jest traktowana jako mem.
* Lucjusz Blakenbauer: przypadkowo wykrył tajną operację sił specjalnych Blakenbauerów w Trójkącie Chaosu i ją SPALIŁ. Współpracując z Vioriką VERLEN. Powiedział jej o wiłach. Szefostwo jest z niego MEGA niezadowolone. Podpadł. Bardzo.
* Lucjusz Blakenbauer: Viorika i ogólnie Verlenowie zauważyli, że jeśli może to pomoże - zwłaszcza ludziom i słabszym od siebie. To jego zdecydowana słabość. Tajnym agentem nie będzie, ale dzięki temu można mu zaufać bardziej niż przeciętnemu Blakenbauerowi.
* Viorika Verlen: zneutralizowała tajną siatkę szpiegowsko-infiltracyjną Blakenbauerów w formie wił, wprowadzając własne, czyste, z innego źródła. Dzięki, Lucjusz :D. Za to dostała pochwałę z kontrwywiadu.
* Viorika Verlen: po zniszczeniu planu z wiłami uznana przez Blakenbauerów za "mistrzynię szpiegów". She is the spymistress, kontrwywiad. Udaje niewinną i słodką, ale w rzeczywistości jej umysł stoi za dużą ilością kontr-planów.
* Lucjusz Blakenbauer: jakieś pół roku po tym wydarzeniu został parą z Vioriką Verlen.
* Viorika Verlen: jakieś pół roku po tym wydarzeniu została parą z Lucjuszem Blakenbauerem.

### Frakcji

* Ród Blakenbauer: tracą siatkę szpiegowską wił na terenie rodu Verlen. Przez Viorikę i Lucjusza.

## Zasługi

* Viorika Verlen: nieświadomie rozwaliła plan sił specjalnych Blakenbauerów oraz wraz z Lucjuszem (którego ściągnęła) uratowała przed psychotycznymi żołnierzami Verlenów grupę cywili Blakenbauerów i Verlenów.
* Apollo Verlen: używając umiejętności społecznych i nieuczciwej przewagi zmiażdżył Viorikę w pojedynku oddziałów. Ona była lepsza taktycznie, jego żołnierze jego uwielbiali. Plus, wkręcił Viorikę.
* Przemysław Czapurt: wkręcił Viorikę i Apollo w pojedynek by Viorika przegrała i nauczyła się roli umiejętności społecznych. Potem autoryzował Viorikę do wspólnej operacji z Lucjuszem Blakenbauerem.
* Lucjusz Blakenbauer: dotrzymuje słowa; pomaga Viorice uratować zaginionych. Rozbija (przypadkowo) siatkę szpiegowską Blakenbauerów na terenie Verlenów. Identyfikuje wiktoriatę, detoksyfikuje psychotycznych żołnierzy Verlenów i ratuje kogo się da.
* Wiktor Blakenbauer: BG; 20. Twórca super-skutecznego psychoaktywnego środku o nazwie wiktoriata; rozprzestrzenia przez wiły. By dorobić i eksperymentować, sprzedaje go do Verlenów. Zatrzymany przez Lucjusza, bo wiktoriata ma psychotyczne efekty uboczne.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. Poniewierz: niedaleko Trójkąta Chaosu, dość spokojna mieścina skupiona na stabilizacji tego, co zdobywa się w Trójkącie. 
                                1. Garnizon
                                1. Archiwum Poniewierskie
                                1. Dom Uciech Wszelakich
                                1. Magazyny Anomalii: miejsce, gdzie przechowywane są odzyskane i anomaliczne przedmioty przed przeprocesowaniem ich w coś wartościowego. Tam znajdowały się używane przedmioty Blakenbauerów.
                            1. Trójkąt Chaosu: teren sporny między Blakenbauerami i Verlenami, gdzie da się wydobywać dziwne rzeczy w wyniku konfliktów sentisieci. PLUS teren anomaliczny.

## Czas

* Opóźnienie: 122
* Dni: 9

## Inne

### Projekt sesji

* Wiktoriata: jest to psychoaktywny środek Blakenbauerów, stworzony przez Wiktora Blakenbauera.
* Kilku żołnierzy Verlenów bardzo źle zareagowało na wiktoriatę; wpadł w psychotyczne polowanie na ludzi.
* Wiła próbuje ich jakoś stabilizować. Niewiele jest w stanie zrobić.
* Działają w obszarze Holdu Bastion, niedaleko granicy z Blakenbauerami; w okolicach Trójkąta Chaosu. Bazę / stację mają w Poniewierzy.

### Sceny sesji

* ?

## Idea sesji

* ?
