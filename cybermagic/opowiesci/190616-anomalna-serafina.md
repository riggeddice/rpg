## Metadane

* title: "Anomalna Serafina"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [190527 - Mimik śni o Esuriit](190527-mimik-sni-o-esuriit)

### Chronologiczna

* [190527 - Mimik śni o Esuriit](190527-mimik-sni-o-esuriit)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. ?

### Wizja

* Kilka artefaktów zostało zlokalizowanych w okolicy Podwiertu; Pięknotka z Erwinem muszą je odnaleźć. Wsparcie - trzech konstruminusów.
* Serafina zaproszona do zrobienia występu dla firmy Miriko.
* Torba Serafiny - neutralizator artefaktów; w Miriko.

### Tory

* Artefakty: drugi - trzeci - ukrycie - neutralizacja - wywiezienie
* Zrzucenie winy: poszlaki - dowody - niewinność
* Szkody: niewielkie - duże - katastrofa
* Zdrowie Zespołu: ranni - ciężko - nieaktywni
* Złapanie Serafiny: uszkodzony sprzęt - ranna - pokonana

Sceny:

* Scena 1: .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena 1: Spotkanie u detektywa**

Żuwaczka powiedział Pięknotce, że oddał artefakt Pustogorowi. Tak więc nie rozumie czemu Pięknotka o to pyta. Powiedział - Krystian Namałłek zabrał ten artefakt dla Pustogoru. Ale... Pięknotka wie, że Namałłek jest na Trzęsawisku. To nie mógł być on. Pięknotka zarejestrowała jego zeznania - czas, miejsce itp. Zaraz potem strzeliła komunikatem do Wiktora Sataraila. Satarail powiedział, że to nie mógł być Namałłek - siedzą właśnie na Trzęsawisku. A więc Pięknotka ma do czynienia z kimś specjalizującym się w chaosie. I kimś, kto wiedział, że się nie nadzieje na Namałłka. I nie wiedział, że Pięknotka wie gdzie on jest.

Zgodnie z danymi _tactical ops_ jeden artefakt powinien być w Sensoplex, gdy pojawia się Koloseum. Drugi najpewniej był przechwycony przez mafię. Po ten mafijny poszedł tchórzliwy Tukan; Pięknotka już się nie może doczekać.

Do Pięknotki przyszedł Tukan - przyniósł jej drugi artefakt. Jest cały z siebie zadowolony. Pięknotka podejrzewa, że coś jest nie tak. Tukan powiedział, że badanie tego artefaktu jest niebezpieczne - powiedział tak Ernest Kajrat. Kajrat pytał Tukana kto dowodzi operacją - powiedział, że Pięknotka. I Kajrat zabezpieczył artefakt. Pięknotkę to mega wkurzyło - kazała Tukanowi to sprawdzić bo diabli wiedzą co się stanie w bazie. (TpZ: S). Tukan się opamiętał i zachowuje się jak terminus.

Tukan powiedział, że ten artefakt - anomalia - jest sztuczny. Zostało to zrobione przez mafię, tak chamsko. Praktycznie krzyczy "suckers". Karla by Pięknotce NIE wybaczyła - Tukanowi też nie.

Pięknotka zostawia Tukana by zajął się Żuwaczką i dostał informacje. Ma nic nie interpretować - wyjąć dane. Pięknotka szuka śladów dających informacje o artefakcie. Jak najwięcej rzeczy które mogą pomóc to znaleźć, coś, co da podstawy na poddanie w wątpliwość Namałłka. (TrM: SS).

Tukan dał interesujące informacje:

* Ta osoba musiała znać Namałłka. Perfekcyjnie działa jak on. Wie co robi - mistrz kamuflażu.
* Wszystko wskazuje na Namałłka.
* Anomalia jest niestabilna. Dopóki choć jedna część anomalii jest gdzieś tu, nie można wywieźć tego z Podwiertu.
* Jest to w jakiś sposób powiązane z imprezami mającymi miejsce w Dzielnicy Biurowej - był bilet na zamkniętą imprezę firmy Miriko, 10-lecie.
* W ciągu tygodnia anomalie się zdestabilizują i wylecą w powietrze. Nikomu na tym nie zależy.

**Scena 2: Czarny plan Kajrata**

W tym czasie Pięknotka poszła porozmawiać z Ernestem Kajratem, którego przechwyciła w okolicy siedziby Miriko. Kajrat się ucieszył widząc terminuskę; są z nim dwie przestraszone dziewczyny, czarodziejki. Pięknotka je odesłała, Ernest się zgodził. Powiedział Pięknotce, że z przyjemnością z nią się spotka w środku Miriko. Zaprosił Pięknotkę do pokoju konferencyjnego; gdy ta przyjęła jego ramię, Ernest powiedział, że ma dla Pięknotki prezent.

Na miejscu wezwał Serafinę i zażądał od niej, by oddała Pięknotce jakąś swoją ekstrawagancką suknię. Serafina ma w oczach zimny, lodowaty gniew, prastarą furię. Pięknotka szuka powodów tej furii (Tp: SS). Okazuje się, że nasza słodka Serafina - cieniaszczycka performerka - nienawidzi z całego serca Pustogoru i rzeczy powiązanych z Pustogorem. Ale fasada kochanej, miłej i uroczej. Gdy Serafina odeszła, Kajrat spytał Pięknotkę co ona chce.

Pięknotka spytała, czy on chciał ją obrazić; Kajrat nie wie o co chodzi. Aaa, tamta anomalia. Nie, nie o to mu chodziło. Nie ma jej już - dał ją w prezencie Serafinie. Z niecierpliwością oczekuje na nagłówki "terminusi pustogorscy ukradli cieniaszczyccej performerce piękną broszkę". Ale Kajrat może odebrać broszkę Serafinie - jeśli Pięknotka stoczy z nim pojedynek. Z nim lub z Serafiną.

Pięknotka zamiast tego stwierdziła, że jednak chciałaby suknię. Ernest poprosił o Serafinę. Gdy przyszła, Pięknotka chciała zbadać jak Serafina ma się do Ernesta. O dziwo, nie boi się go. Uważa go za sojusznika, nie za wroga. Nie uważa go za zagrożenie. Tam jest coś więcej, dużo więcej. Dodatkowo, ona wie, że broszka jest niebezpieczna - dlatego jej nie nosi.

Pięknotka jest przekonana, że na samej imprezie dojdzie do przekazania artefaktu od "Namałłka". Chce więc ściągnąć Tukana na tą imprezę. By to osiągnąć, próbuje przekonać Serafinę. (Łt: S). Serafina się zgodziła zaprosić dwóch terminusów, co ze śmiechem wskazał Kajrat. Pięknotka powiedziała, że się może odwdzięczyć - może pomóc z zmienieniem jej w bóstwo. Serafina pierwotnie odmówiła, ale Kajrat potwierdził, że Pięknotka jest naprawdę dobra. Serafina się zgodziła - nie miała wyjścia bo Kajrat i maska.

**Scena 3: Serafina**

Pięknotka ściągnęła special delivery rzeczy do Miriko. Tak, żeby dało się zrobić epicki makijaż i upiększenie Serafiny.

Pięknotka zabrała się za upiększanie piosenkarki. O dziwo, piosenkarka jest tak dobrą aktorką, że gdyby Pięknotka nie wiedziała, przeoczyłaby stres czy nienawiść. Pięknotka zrobiła z niej najpiękniejszą gwiazdę na scenie. I Pięknotka coś wyczuła - Serafina jest magiem rodu. Ma wbudowaną matrycę - ale Pięknotka nie zna tej matrycy. Czyli to jest ród spoza Astorii. Pięknotka próbuje się dowiedzieć o niej jak najwięcej, znaleźć jakieś wspólne znajomości. (Tp: P,SS)

* Wspólny znajomy: Kasjan Czerwoczłek. Kiedyś uczeń terminusa, wyrzucony.
* Wspólne miejsca: Dawno temu, Serafina była w Pacyfice. Też się pojawiło imię Nikoli.
* Rozumie jak działają anomalie i jak są niebezpieczne. Nie martwi się prezentem od Ernesta. Nie odda po dobroci.
* Miała przyjaciół w Pustogorze. Czas przeszły.

Pięknotka szybko sprawdziła - Serafiny nie ma w systemach Pustogoru. To znaczy, że kiedyś nazywała się inaczej i była kimś innym. Dlatego najpewniej nie chce jechać do Pustogoru.

Serafina też miała pytanie do Pięknotki - dlaczego tak utalentowana kosmetyczka została terminusem. Dlaczego Ernest traktuje ją jak psychopatycznego potwora. Pięknotka powiedziała, że jest terminuską bo dba o swój dom i nie lubi, jak się dzieje niepotrzebna krzywda. Pięknotka zauważyła, że Serafina ma nadzieję, że KIEDYŚ Ernest uzna ją za sobie równą. I to jest niepokojące.

**Scena 4: Impreza**

Wieczorem, impreza.

Tukan przebrany jak niewolnik - nagi tors, spodnie od garnituru z lakierkami i obroża. Pięknotka ma facepalm. Nie chce mieć go blisko. Tukan jest wyraźnie zawstydzony i wściekły na Pięknotkę.

* Trzeba było się wykazać JAKIMIŚ jajami - Pięknotka
* Trzeba było mi tego nie robić - Tukan
* Wolisz by ten cholerny artefakt jebnął w powietrze?! - Pięknotka
* Kajrat ma jakiś plan, nie pozwoli, by Podwiert wyleciał w powietrze... - Tukan, zrezygnowany

Tukan skupił się na szukaniu i analizie artefaktów: (TrM+2: P). Najgorszy covert ops ever. Znalazł artefakty, oba - i oba są Serafiny. Ona ma obie te anomalie i te anomalie są jakoś zmodyfikowane. Są silniej kontrolowane niż powinny być. Nadal są niebezpieczne, ale inaczej. Z tego wynika, że Serafina ma je od samego początku. W wyniku porażki wszyscy magowie wiedzą, co on właśnie zrobił.

Przyszło dwóch thugów Kajrata i wywaliło Tukana. Lekko go sponiewierali, ale lekko. Tukan - głębokie upokorzenie. Pięknotki nikt nie usunął z imprezy. Pięknotka jeszcze przypatrzyła się samej Serafinie - ma do czynienia z iluzjonistką i czarodziejką soniczną. Świetna, optymistyczna performerka.

**Scena 5: Koloseum?**

Następnego dnia wieczorem ma się pojawić Koloseum. Pięknotka wezwała wsparcie - dwa punkty terminusów. Karla jest zaskoczona. Pięknotka jednak prosi o dziesięciu terminusów, bo boi się Ernesta. Nie wiadomo co tam się stanie. A sprawa może być apokaliptyczna.

Pięknotka spotkała się z Serafiną. Serafina spytała, czy Pięknotka byłaby skłonna ją oprowadzić po Koloseum. Pięknotkę lekko zdziwiła ta prośba - w co ten Kajrat gra? Serafina powiedziała, że nawet ma lekki power suit, dzięki któremu będzie bezpieczna. Pięknotka podejrzewa, że Kajrat chce ją zestrzelić snajperką i wywołać straszną wojnę dyplomatyczną; martwa Serafina może być sporo dlań warta.

Pięknotka powiedziała Serafinie "tak". Ale przed zabraniem tam Serafiny zamierza upewnić się, że nie ma przy sobie żadnej z dwóch pozostałych anomalii. Tukan ma ją sprawdzić wcześniej...

Pięknotka spytała Serafinę czemu Kajrat jej pomaga. Serafina nie wie - on chce ją wzmocnić, ucieszył się, że te anomalie jej pomagają. Powiedziała Pięknotce, że te anomalie się zmieniły w kontakcie z nią. Serafina w pewien sposób jest "trzecią anomalią". Tamta - "czwarta" - zdecydowanie Serafinę usprawni. A Kajrat - zdaniem Pięknotki - gra w wojnę. Chce wywołać wojnę między Cieniaszczytem a Pustogorem. Pustogor musi zatrzymać Serafinę (bo rozkazy) a Kajrat będzie ją chronić (bo "jest tym dobrym"). Brzmi to bardzo jak Ernest Kajrat, doskonały strateg i sadystyczny potwór.

Pięknotka powiedziała, że nie pozwoli Serafinie wejść do Koloseum z tymi anomaliami. Serafina zauważyła, że ona nie może pozbyć się anomalii; asymiluje je.

Pięknotka spróbowała przekonać Serafinę, by ta zdecydowała się jej pomóc - niech pokaże, w jaki sposób działają te cząstkowe anomalie. Tak, by terminusi mogli rozmontować tą trzecią i nie zrobić nikomu krzywdy. Serafina nie chce się zgodzić; musi zasymilować trzecią. Ale Pięknotka nie popuści. (Tr+2: SS). Pięknotka przekonała Serafinę - niedaleko Trzęsawiska, Pięknotka, katalista i Serafina.

Tymczasem Kajrat zebrał grupę cieniaszczyckich magów którzy mają chronić Serafinę.

Pięknotka odwiedziła Grzymościa. Poprosiła, by on zatrzymał Kajrata, by nie doszło do wojny. Grzymość się zgodził. Zatrzyma Kajrata. Ale jest personalnie zły na Pięknotkę.

**Scena 6: Przy Trzęsawisku**

Niedaleko Trzęsawiska: Serafina, Tukan, Pięknotka.

Tukan próbuje dowiedzieć się od Serafiny jak działają te anomalie. Słodka i uśmiechnięta Serafina wyjaśnia i pokazuje - to krystaliczne anomalie używające nienaturalnego źródła energii.

Ma specyficzny power suit. Pięknotka od razu łączy się z Erwinem i Minerwą, by dowiedzieć się coś więcej odnośnie tego power suita. Jest to model jakiego Pięknotka nigdy nie widziała. (Tr:TAK ALE).

* Power suit jest bazowany na klasie 'recon'. Scout, lekki.
* Power suit ma strukturę krystaliczną. Ma elementy kryształów wbudowane. Te kryształy są kompatybilne z tymi anomaliami PLUS zamykają anomalie w kryształy.
* Amplifikuje zdecydowanie jej umiejętności soniczne i iluzyjne. Jest w stanie tworzyć całe światy iluzyjne.
* Model jest stosunkowo delikatny; pistolet przebije pancerz. To lekki power suit. Niebojowy.

Grzymość wysłał wiadomość Pięknotce. Odwołał Kajrata. Kajrat nie stanowi zagrożenia, ale żeby ją chronić wynajęta jest grupka najemników Cieniaszczytu. KONKRETNIE z Cieniaszczytu. I jakkolwiek Kajrat to anulował, to wcześniej przekierował płatność na sponsorów dziewczyny. Mają się ukrywać i działać by ją chronić. Tyle Grzymość wie i tyle mógł Pięknotce powiedzieć.

Ostatnie, co Kajrat mógł zrobić - zatruł studnię. Pięknotka jest mu "bardzo wdzięczna".

Jakikolwiek plan porwania Serafiny właśnie poszedł w łeb.

**Sprawdzenie Torów** ():

* Artefakty: drugi - neutralizacja - powerup - trzeci - wywiezienie: 3
* Zrzucenie winy: poszlaki - dowody - niewinność: 2
* Szkody: niewielkie - duże - katastrofa: 0
* Zdrowie Zespołu: ranni - ciężko - nieaktywni: 0
* Złapanie Serafiny: uszkodzony sprzęt - ranna - pokonana: 0

**Epilog**:

* Serafina ma grupę 5 najemników pilnujących, by nie stała jej się krzywda
* Serafina zasymilowała dwie anomalie do pewnego stopnia, usprawniając swoją moc

## Streszczenie

W Podwiercie pojawiła się Serafina, piosenkarka zbierająca i asymilująca anomalie. Wyraźnie pomaga jej Kajrat, który ją dodatkowo chroni. Pięknotka dostała zadanie odzyskać te niestabilne anomalie - ale Serafina jest lubiana przez kilku cieniaszczyckich bonzów; nie można odebrać jej anomalii siłą. Dodatkowo, Serafina ma w sobie stary gniew na Pustogor za coś, co się zdarzyło w Pacyfice. Wszystko zbliża się w kierunku na wojnę lub konflikt zbrojny.

## Progresja

* Ernest Kajrat: Grzymość mu zdecydowanie mniej ufa. "Mad Dog Kajrat", nie zaufana prawa ręka - czemu chciał wojny?
* Ernest Kajrat: Zły na Pięknotkę - wprowadziła Grzymościa do walki z Kajratem zamiast stanąć naprzeciw niego osobiście.
* Tomasz Tukan: Upokorzony i pogardzany przez mafię. Ma jeszcze mniejszy kręgosłup niż kiedykolwiek.
* Serafina Ira: Banshee (servar) osiągnął pełną moc jako iluzyjno-soniczny generator obrazu i dźwięku.

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: zatrzymuje wojnę Grzymościem, 
* Serafina Ira: Cieniaszczycka Banshee, czarodziejka soniczna i iluzji. Mag rodu. Uśmiechnięta i wesoła piosenkarka, która zbiera anomalie a w sercu ma nienawiść do Pustogoru.
* Krystian Namałłek: Serafina podrzuca dowody, że to on jest winny zdobycia anomalii; skutecznie to odsunęło od akcji część pustogorskich terminusów.
* Antoni Żuwaczka: właściciel artefaktu - oddał go "Namałłkowi". Chciał się przydać a wyszło jak zwykle.
* Tomasz Tukan: niezły neuronauta, katalista i terminus ale nie ma kręgosłupa i nie radzi sobie z konfliktem. Poniewierany i upokarzany przez mafię, mimo, że powinien słuchać Pięknotkę..
* Ernest Kajrat: mastermind stojący za "niech Pustogor i Cieniaszczyt staną do walki przeciw sobie". Trochę pomaga Pięknotce, bardzo Serafinie. Grzymość go w końcu zatrzymał, ale zaczął ziarno konfliktu.
* Ronald Grzymość: może i mafioso, ale patriota. Nie pozwoli na to, by Kajrat podpalił Pustogor - zdecydował się go zatrzymać.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Dolina Biurowa: siedziba firmy Miriko, gdzie był koncert Serafiny oraz większość spotkań.

## Czas

* Opóźnienie: 3
* Dni: 3
