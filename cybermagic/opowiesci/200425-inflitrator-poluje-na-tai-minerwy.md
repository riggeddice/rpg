## Metadane

* title: "Infiltrator poluje na TAI Minerwy"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [200418 - Wojna Trzęsawiska](200418-wojna-trzesawiska)

### Chronologiczna

* [200418 - Wojna Trzęsawiska](200418-wojna-trzesawiska)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* .

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

* Od czasu do czasu Minerwa pomagała w naprawie i usprawnianiu pomniejszych cywilnych Technologicznych AI w Zaczęstwie i okolicach.
* Technożercy są wielkimi przeciwnikami zaawansowanych magitechów, a już w ogóle TAI. Zakupili customizowaną zaraźliwą klątwę od Auhag.
* Klątwa rozprzestrzenia się po Zaczęstwiackich TAI a Technożercy robią wszystko co w ich mocy by winną okazała się Minerwa.
* Wysłali kupionego od Auhag Bio-Infiltratora by tam gdzie Minerwa robiła korekty i naprawy, tam dochodziło do katastrofy.
* TAI "Strażniczka" jest zmartwiona aktualnym obrotem sytuacji. Co gorsza, jej psychotronika jest ograniczona z uwagi na reakcje Cyberszkoły na Trzęsawisko.

## Punkt zero

.

## Misja właściwa

Parę dni później Pięknotka sygnał od Minerwy. Minerwa prosi o wsparcie w Zaczęstwie, asap. Minerwa wysłała sygnał, że jest harrasowana przez terminusa. A dokładnie, terminus ją dorwał w Zaczęstwie (bo tam pojechała). Tym terminusem jest Tomasz Tukan. I teraz ją oskarża o uszkadzanie cywilnych TAI. Więc Minerwa dyskretnie wezwała Pięknotkę na pomoc, w czasie w którym sama próbuje wygadać się od problemów z terminusem i jego pomagierką.

Dopytana po hipernecie, Minerwa zaczęła wyjaśniać:

* Nic złego nie zrobiła, ok?
* Czasami były zlecenia naprawy jakiegoś TAI albo tweakowania, albo usprawnienia... to to zrobiła.
* I teraz Tukan o to się nad nią znęca, że podobno te TAI zachowują się niewłaściwie. Ale zdaniem Minerwy to potwarz jest. Ktoś próbuje ją wrobić i przejąć rynek!

Minerwa wchodząca na rynek cywilnych TAI... jest to nędzne. Pięknotka wierzy że Minerwa się tego podejmuje, ale... ech. Minerwa, mistrzyni militarnej psychotroniki zajmująca się czymś takim? Tak czy inaczej, Pięknotka wchodzi do akcji.

Na miejscu - Tomasz Tukan i młoda, ładna uczennica terminuska. Laura Tesinik. Nic szczególnego, według papierów. Stoją w zaułku i Tukan przesłuchuje Minerwę; wyraźnie zirytowała go obecność Pięknotki. Pięknotka weszła z uśmiechem: "Paluszku, czemu dręczysz Minerwę" (Tr: 8v8):

* V: Tukan jest wyprowadzony z równowagi, solidnie.
* X: Tukan pójdzie o krok za daleko, ale potem (Tukan x Grzymość).
* V: Linia Tukan - Laura jest uszkodzona. Laura nie ma takiego zaufania do Tukana jak Gabriel do Pięknotki.
* X: Laura przez to ucierpi, docelowo; przez Grzymościa.
* V: To będzie uroczo kompromitujący Tukana materiał i Pięknotka zdobęcie opcję dostania o tym informacji.

Tukan stracił kontrolę nad sytuacją. Opieprzył Pięknotkę, solidnie i mocno. I Minerwę. Powiedział Pięknotce, że TAI nad którymi pracowała Minerwa są uszkodzone. Te AI są splugawione jak magia Minerwy. Memo od szefa firmy TAI przekształciło w penisy. Elektroniczna niańka która prawie zadusiła dziecko. Tak więc niech Minerwa się nie pieprzy - on chce listę jej klientów, wszystkich. Bo jego psychotroniczka (Laura) też nie chce chodzić od firmy do firmy, jak powiedział. Gdy Pięknotka spytała Laurę co ona uważa - ta odpowiedziała. Uważa to co powinna - to co Tukan, jej przełożony. W gniewie Tukan tego nie zauważył.

Minerwa podniosła głos. Jej magia działa. Jej wiedza działa. A Tukan jest niekompetentnym kretynem jeśli uważa że to jej wina.

Pięknotka powiedziała, że potrzebuje listy klientów gdzie były kłopoty od Tukana. Tukan powiedział, że to JEGO sprawa. Minerwa na to, że jeśłi on nie powie to ona powie - prawda taka, że jeśli przez dumę Tukana ucierpią ludzie, to on będzie w dupie. Tukan jest biały ze wściekłości.

Laura zaczęła podawać firmy i jakie były problemy. Tukan do niej - "uczennico!". Laura się lekko skurczyła, ale powiedziała, że Pięknotka ma prawo wiedzieć zgodnie z kodeksem. Tukan powiedział Laurze, że potem o tym porozmawiają. Pięknotka wysłała Laurze link - jeśli Tukan przekroczy linię, Laura wie gdzie ją znaleźć. Minerwa tylko wtarła sól w ranę Tukanowi - gdy nie kontrolowała magii była bardziej kompetentna od niego. Tukan aresztował Minerwę na 24h - za potencjalną manipulację dowodami. Ona z nim pójdzie, czuje się pewniej wiedząc że KOMPETENTNY terminus zajmuje się tą sprawą, czyli Pięknotka.

Szczęśliwie, tym razem Minerwa nie miała żadnego zlecenia w Zaczęstwie (tak powiedziała, nie chcąc się przyznać że sprzedawała z Aurum). Na hipernecie, Minerwa przekazała listę wszystkich swoich klientów. Pięknotka poczekała tylko aż się Tukan z Minerwą i Laurą oddalą, po czym połączyła się ze Strażniczką.

Zgodnie z tym co mówi Strażniczka, Akademia Magii jest nie dość izolowana od magii i Skażenia z Trzęsawiska jak na TAI jej klasy (Trzęsawisko -> Cyberszkoła -> Strażniczka) - dlatego Strażniczka działa na 30% swojej mocy (jej słowa). Pięknotka podaje jej listę tych które miały kłopoty i tych które JESZCZE nie miały ale Pięknotka się spodziewa kłopotów. Niech Strażniczka znajdzie gdzie są kłopoty, gdzie Pięknotka ma iść. (Tr+2=8v8)

* VV: Strażniczka jest w stanie znaleźć miejsce zanim stanie się coś naprawdę złego.
* XX: Problem się już zaczął i jest sporej skali
* V: Strażniczka daje Pięknotce cenną podpowiedź - coś co zauważyła.
* V: ...wraz ze sposobem wykrycia / typem bytu

Strażniczka powiedziała, że monitorowała dronami akurat jeden z obszarów - Nieużytki, szukając problemów z Satarailem. W związku z tym rekonfigurowała drony (z pomocą dwóch studentów Akademii Magii), którzy nadal nie wiedzą że ona tam jest lub czym ona jest. I Strażniczka konfigurowała pod kątem biologicznym i biomagicznym. Znalazła ślad biomagiczny - konkretnie biomagiczny - ale niepasujący do żadnej bioformy zgodnej z Trzęsawiskiem którą ona zna. I ta bioforma weszła w interakcję z Elektronicznym DJem na jednej z imprez która ma miejsce. DJ jest DJem dla grupy studentów Akademii Magicznej. I tak, Minerwa naprawiała i korygowała tego DJa, ale potem ta bioforma dyskretnie się tam dostała. I drona jej NIE WIDZIAŁA, wykryła zaawansowanymi sensorami magitechowymi. Jakiś infiltrator.

Impreza ma miejsce **teraz**.

Pięknotka leci tam żeby jak najszybciej złapać dowody i coś z tym zrobić. Natychmiast wzywa Gabriela. I kazała mu zdobyć coś co się zna na technologii. A sama próbuje zdążyć (Ex: jak szybko tam dotarła, +2 za Strażniczkę i poszerzenie uprawnień Pięknotki):

* XV: dotarła tam zanim studenci zniszczyli dowody, ale JUŻ się toczy "walka"

Pięknotka dotarła i widzi, z powietrza: delikatnej konstrukcji, spory DJ wali falami sonicznymi i szaleje. A dookoła magowie rzucają czary, zwiększając próg Skażenia. Pięknotka... odpaliła power suit i spadła, prosto na czterołapczastego DJa. Awian sam wyląduje. Plus Pięknotka wali na sygnale "PROSZĘ NATYCHMIAST PRZESTAĆ" przez megafon i spada. Zero problemu ze strony DJa, ale Pięknotka musi odeprzeć czary studentów... (Tr+2: V). ElektroDJ rozwalony (depnięty), studenci rozgromieni, materiał zabezpieczony.

Perfekcyjna akcja terminuski.

Po pół godziny pojawił się Gabriel. Pięknotka kazała mu wsadzić szczątki do awiana i zaczyna się problem badań. Niech Gabriel z Erwinem to zbadają. Jako, że Erwin się dobrze zna z Minerwą, to będzie najlepszą do tego osobą. I Pięknotka poprosiła by Strażniczka monitorowała sytuację. Strażniczka odpowiedziała że ma mniejszą moc niż zwykle. Kiedy będzie Tymon... Strażniczka jest patchworkowo wprowadzona do Akademii. Nie ma izolacji przed zakłóceniami magicznymi tego typu co teraz przychodzą z Cyberszkoły.

Oki... stan Strażniczki martwi trochę Pięknotkę. Skomunikowała się więc z Tymonem. Musi wiedzieć więcej i wiedzieć jak jej pomóc. (TrZ+3: XV). Tymon nie ma wyjścia widząc stan Strażniczki. Wyjaśnił o co chodzi.

* "Strażniczka Alair" to skrót od "Eszara d'Aleph Airen". Aleph Airen to był poprzedni statek na którym służył Tymon. To był ciężki krążownik wsparcia i wojny elektronicznej.
* "Strażniczka" to Eszara. To waży kilka ton. Szkoła w środku miała układy i elementy pasujące do Eszary - superciężki kompleks. Tymon po prostu wprowadził tam właściwą TAI.
* Zamontowali to jego kumple z brygady. To było trochę szmuglowania. Studenci się zorientowali, ale myśleli, że to skrzydło remontują.
* Strażniczka nie jest w pełni kompatybilna z tamtymi połączeniami, z reaktorem, z izolacją... Eszary są bardzo, bardzo delikatne. Na pełnej mocy Eszara potrafi sterować flotą. Ale aż tyle szkoła jej nie da.
* Eszara d'Aleph Airen umierała, nie miała "niczego". Zamontowanie jej tutaj utrzymało ją w dobrym stanie.
* Aleph Airen był statkiem wsparcia - jego funkcją była koordynacja i sprzęg. Tak więc ta Eszara jest troszkę mniejsza. Skonfigurowana do tyci innych operacji.

I teraz biedna Eszara jest w za małym miejscu, za mała moc i ogólnie jest słabo izolowana i ma błędne rzeczy. Do tego zamontowana dość wprawnie, ale nie przez ekspertów od psychotroniki. Plus, Eszara statku kosmicznego jest na planecie. W szkole. Z innymi połączeniami i dronami. Eszara wsparcia nie ma czego wspierać.

Pięknotka przekazała Strażniczce hasło od Tymona - jeśli ma niski poziom energii, niskie dopasowanie psychotroniczne, niech przejdzie w hibernację. Ale na razie Pięknotka nic nie poradzi na stały rozpad Eszary...

W końcu Pięknotka dostała informacje od Erwina i Gabriela. Ciekawe informacje: (ExZM+3: V)

* V: Erwin zauważył, że czar Minerwy był poprawny. Zmiany dokonane przez Minerwę nie są błędne; może to zeznać.
* X: Minerwa i raczej tylko Minerwa będzie w stanie określić pochodzenie tego cholerstwa. Erwin nie jest weapon specialist.
* V: To jest jakaś forma wirusa AI. Bardzo skomplikowany, zbudowany dedykowanie, ale NIE pod Minerwę. To jest wirus bojowy, sabotażowy. Nie pochodzi z Orbitera, nie ten strand.

Podsumowując, ktoś NIE w Orbiterze sabotuje AI tutaj. I ktoś uwziął się na Minerwę - więc sabotuje jej AI. Wprowadza to jakąś bioformą, to nie może być z Trzęsawiska, tam nie ma takich możliwości na pasożyty AI. To nie jest też ixiońskie. Nie ma tu żadnych nienaturalnych energii.

Pięknotka wie już, że szuka bioformy. A bioformy są czymś, co ona potrafi znaleźć.

NAJBARDZIEJ zmartwiło Pięknotkę, że Strażniczka - Eszara - nie ma systemów defensywnych przed tym wirusem. Eszary - a zwłaszcza ta - nie są na takie rzeczy odporne. Przynajmniej wykryje jakby do tego doszło... więc Pięknotka dała jej wytyczne - niech wejdzie w tryb hibernacji jeśli będzie zainfekowana.

Dobrze. Nie ma czasu do stracenia - Pięknotka musi znaleźć bioformę, zwłaszcza, jeśli bioforma zagraża Strażniczce. Pojechała do Erwina i rzuciła mu się w ramiona. Ale biznes to biznes - trzeba zbudować biotracker tego cholerstwa. (ExZM+4: 13+3Ev11)

* V: Istnieje tracker. Ten tracker działa i wyszukuje w krótkim zasięgu.
* X: Opinia publiczna faktycznie jest przeciwko Minerwie. Co nie powiedzieć, w końcu to tylko jej rzeczy są problematyczne.
* O: Wyszukuje w dalekim zasięgu; jeśli Pięknotka jest w pewnym promieniu + decay to działa || potwór wpłynął na Arenę Migświatła (ona ma inercję, ale przez moment będzie ciężko).

Pięknotka zdecydowała przygotować specjalną truciznę w formie gazowej skalibrowaną przeciwko tej bioformie. Siadła w swoim laboratorium i zabrała się do pracy (TrMZ+2: XVVV): udało jej się sformować fajną truciznę przeciwko bioformie. Sęk w tym, że jest też trująca dla ludzi. Ale co tam - niech ich tam nie ma. Z zabitą bioformą Pięknotka będzie w stanie wyciągnąć Minerwę - laboratorium potwierdzi to wszystko co odkrył Erwin z Gabrielem. A że na razie dowody nie są uszkodzone...

Tracker Pięknotki doprowadził ją do Cyberszkoły. Potwór jest gdzieś w środku. Chwilowo jest turniej w jakąś grę komputerową - jest tam kilkanaście osób. Ale szkoła jest duża, Pięknotka będzie w stanie znaleźć wroga. Tracker NIE prowadzi w kierunku turnieju.

Podchody (TrZ+3, do VVV). Pięknotka ogarnia tą szkołę; za często swoim zdaniem tu bywa:

* V: Pięknotka zaszła przeciwnika, udało jej się do niego dostać
* V: Pięknotka jest całkowicie niezauważona, przeciwnik nie wie że ona nań poluje
* V: Pięknotka ma czystą linię ataku i odcina przeciwnikowi możliwość wiania.

Przeciwnik to biomechaniczny pajęczak o długości przedramienia, w trybie stealth, ładuje się właśnie z jednego z komputerów (i nieświadomie Skaża się energią Trzęsawiska - nic to z perspektywy Pięknotki nie zmienia). Pajęczak ma silny odwłok to wstrzykiwania sygnałów do TAI. Nie przebije to power suita, ale poradzi sobie z cywilnymi TAI.

* V: Egzekucja pajęczaka trucizną - Pięknotka zdjęła go niezauważenie

Terminuska jest zadowolona. Wyszła cichcem ze szkoły. Nikt nawet nie wie że tam była.

Pajęczak stanowi wystarczający dowód, że to nie Minerwa. Wypuszczą ją.

## Streszczenie

Problemy na Trzęsawisku wpływają na Cyberszkołę w Zaczęstwie, co uszkadza możliwości TAI "Strażniczki", czyli Eszary d'AlephAiren. Tymczasem jakaś tajemnicza siła uszkadza cywilne TAI, które usprawniała Minerwa. Pięknotka odpiera Tukana, który chce udowodnić winę Minerwy i odkrywa, że za tym wszystkim stoi ktoś z Aurum, ktoś kto posiada profesjalnego pajęczego Infiltratora Iniekcyjnego, zaprojektowanego do uszkadzania AI. Dzięki temu Pięknotka wyciągnęła Minerwę z aresztu.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: chcąc pomóc Minerwie przed fałszywym oskarżeniem o uszkadzanie TAI odkryła i zniszczyła Infiltratora Iniekcyjnego. Spadła z awiana by spłaszczyć elektro-DJa.
* Minerwa Metalia: dorabia sobie usprawniając TAI. Jako, że część się psuje w groźny sposób, aresztował ją Tukan. Ale naprawdę to był sabotaż... kogoś z Aurum?
* Tomasz Tukan: neuronauta-terminus; nie cierpi Minerwy i jej "chorej" magii, nie cierpi Pięknotki. Chce koniecznie udowodnić winę Minerwy; upokorzony przed studentką przez Pięknotkę.
* Laura Tesinik: niedoświadczona terminuska i asystentka Tukana; psychotroniczka, która zna chyba wszystkie regulacje i działa zgodnie z każdą jedną z nich.
* Gabriel Ursus: przywołany do pomocy przez Pięknotkę by przenieść uszkodzonego elektronicznego DJa do Erwina; tam odkryli obecność Infiltratora Iniekcyjnego Aurum.
* Erwin Galilien: mistrz delikatnej katalizy, z Gabrielem odkrył, że za problemami TAI nie stoi Minerwa a Infiltrator Iniekcyjny Aurum
* Strażniczka Alair: naprawdę Eszara d'AlephAiren; z uwagi na wpływ Trzęsawiska na Cyberszkołę w coraz gorszej formie. Operuje na coraz mniejszej mocy. Tymon się martwi.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Sypialnia Szczelińca: sporo cywilnych TAI jest tam uszkodzonych przez Infiltratora; wszystko by dorwać Minerwę
                                1. Cyberszkoła: destabilizuje się i wysyła kłopotliwe sygnały z uwagi na wpływ Trzęsawiska. Potencjalny ogromny problem.
                                1. Akademia Magii, kampus 
                                    1. Budynek Centralny: okazuje się, że w środku jest Strażniczka - a dokładniej, Eszara-class TAI zamontowana przez Tymona i kumpli.
                                1. Nieużytki Staszka: miejsce, gdzie impreza studentów Akademii Magii została rozgoniona przez elektro-DJa a potem Pięknotkę

## Czas

* Opóźnienie: 3
* Dni: 2
