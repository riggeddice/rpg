## Metadane

* title: "Koszt ratowania Torszeckiego"
* threads: rekiny-a-akademia, amelia-i-ernest
* gm: żółw
* players: anadia, kić

## Kontynuacja
### Kampanijna

* [211012 - Torszecki pokazał kręgosłup](211012-torszecki-pokazal-kregoslup)

### Chronologiczna

* [211012 - Torszecki pokazał kręgosłup](211012-torszecki-pokazal-kregoslup)

### Plan sesji
#### Ważne postacie

* Ernest Namertel: Rekin z Eterni, jest w nim śmiertelnie zakochana Amelia Sowińska.
* Rafał Torszecki: przydupas Marysi, który nie może zdzierżyć, że Ernest ma WIĘCEJ niż ona.
* Franek Bulterier: Rekin, potężny i groźny jak cholera. Człowiek szafa. Mały ród, mało mówi, mocno bije.
* Marek Samszar: Rekin, który stracił Arkadię Verlen (bo ukradł dla niej kota) i wzdycha za nią po nocach.

#### Co się wydarzyło

.

#### Sukces graczy

* .

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Ernest będzie żądał głowy. Marysia wpadła na pomysł by powiedzieć mu prawdę (czego się nigdy nie robi i nikt nie spodziewa) - to był Torszecki. Ale pod wpływem Złego Potwora Z Bagien. Więc w ten sposób Torszecki uratowany i Ernest "zadowolony". A nie tacy jak Ernest próbowali pokonać Władcę Trzesawiska. Tzn. Marysia nie wie wiele na ten temat, ale WSZYSCY miejscowi się go boją. Tzn. Julia, Triana trwożnie unikała tych ziem, Sensoplex w Podwiercie został przez niego zniszczony i przekształcony w farmę biokoszmarów zanim terminusi to zniszczyli.

Ogólna opinia jest taka - podpadnij Wiktorowi Satarailowi i zostaniesz zniszczona.

Ale co to dla Marysi :D. Ona jest Sowińska, poradzi sobie :D. (dobrze że nie ma tu Julii ani Triany - byłby facepalm)

Marysia skontaktowała się z Pawłem Szprotką. Paweł jest nieswój - nie przywykł do komunikacji z możnymi tego świata. Marysia poprosiła go o zestawienie spotkania Marysia - Olga. Zgodził się. Paweł spytał Marysię czy to PRAWDA, że pojawił się eternianin? LORD eternijski? Marysia potwierdziła - przyjechał w pokojowych zamiarach. Paweł się przestraszył.

Jeśli Marysia chce poznać PRAWDĘ na temat Pawła i czemu ten się boi, Ex. Powód prosty - on chroni tego z całych sił i jej nie ufa. Nie chce go zastraszać dla kaprysu ;-). Więc - Marysia zaproponowała mu, że wpadnie do niego i w cztery oczy wyjaśni mu o co chodzi. Paweł jest wstrząśnięty tą propozycją, ale nie jest przeciw. Najpierw słabo odmawiał - nie ma gdzie przyjąć. Marysia powiedziała, że przecież przychodzi na herbatkę do Triany czy Julii. Paweł zauważył lekko przerażony, że Triana to wyższe sfery (córka sławnego naukowca) a Julia... w sumie, Julia to Julia XD. No w sumie...

Paweł z przyjemnością przyjmie Marysię w swoim naturalnym otoczeniu. Poprosi Myrczka by wyszedł z pokoju w akademiku.

Marysia odwiedziła akademik AMZ. Tym razem leci do CHŁOPCA. No dobra, do Pawła.

Paweł przyjął Marysię w pokoju pełnym ZAPACHÓW. W pokoju mieszka Paweł i Myrczek. Więc zapach grzybni, jakieś zwierzaczki, też pobandażowane... Marysia wzięła ciasto i alkohol, ale po zastanowieniu, to nie ten target.

Paweł przyjął Marysię własnoręcznie zrobionym ciastem roślinnym. Coś wegańskiego. Na liściu. Wygląda... podejrzanie. Stwierdzenie "przyprawione grzybami" nie poprawiło Marysi pewności siebie - ale Paweł zjadł pierwszy. I PRZEŻYŁ. Wprawne oko Marysi zobaczyło ubóstwo - np. Paweł ma pocerowane ubrania. "Ignacy dorabia sobie jako kucharz" - powiedział Paweł jakby to było całkowicie oczywiste.

"Czy jest coś co ja powinnam wiedzieć? Czy znasz tego konkretnego maga? Bo może ja powinnam o tym wiedzieć żeby chronić tę okolicę" - Marysia, tak przekonywująco szczerze jak tylko umie kłamać do Pawła. To jest pełen ZASÓB. Bo Paweł widział Marysię tylko w trybie "jestem strażniczką" a nie w trybie "jestem oportunistką". Marysia jest na miejscu, przyskrzyniła Pawła, jest pomiędzy "sweet" i "imperious". Paweł nieprzyzwyczajony.

ExZ+3:

* Xz: Paweł jest przekonany, że jedyny powód czemu Marysia przyjechała to to, by go wypytać. Nie chciała go odwiedzić. Bo jest tienką.
* Vz: ...ale jest tienką i jest w sumie jedyną osobą, która może go osłonić przed eternianinem.
    * Paweł: on JEST z Eterni. Uciekł.
    * "Czyli Ty się urodziłeś w Eterni?" - "Tak. I teraz eternianin przybył po mnie..."
* V: Marysia go uspokoiła. Eternianin NIE WIE o nim a od niej się nie dowie. A ona przecież chroni. A ten eternianin nie jest problemem. Może inny tak - ale nie ten. Ten jest całkiem spoko. Paweł jest trochę uspokojony, acz Marysia widzi, że ten koleś ma galopujące fobie i triggery. "Idealny szef ochrony".
    * Wymsknęło mu się, że ma "ducha opiekuńczego" w AMZ (kobietę). Dama W Błękicie -> nie wie kim ona jest, ale jest "kochana". Ona pokazała mu różne kryjówki, plany ewakuacyjne itp. Dzięki temu śpi spokojnie.
    * "Coś od Ciebie chce w zamian?" - pyta Marysia, która węszy niebezpieczeństwo. On na to poważnie - "Tak, mam doglądać jej rumaka.". Okazuje się, że w AMZ jest koń. Magiczny koń. Płochliwy. I on ma go doglądać. Za to ona mu pomaga. Nie pytał czyj jest ten koń, bo nie chciał zwracać na Damę w Błękicie uwagi. Ale sam nie znalazł. Koń jest na campusie.

Marysia spróbowała przypadkowo wpaść na konia. Dała radę. Znalazła go. Poczciwy czterołapczak. Starszy już koń, z pozytywną energią magiczną. Marysia mizia konia po chrapkach, koń odwzajemnia czułość i na Marysię spada UKOJENIE. Lekkie, ale zawsze.

Czyj jest ten koń? Niczyj. Ale kiedyś należał do Ksenii Kirallen. Oddała konia na campus uczelni - bo tu jest bezpieczny i tu będzie mu dobrze. I będzie mógł pomagać innym.

NASTĘPNEGO DNIA.

Paweł poinformował Marysię hipernetem, że Olga z przyjemnością Marysię przyjmie.

Marysia poleciała swoim ścigaczem do Czarnopalca. Miejsce nadal wygląda jakby przeszło po apokalipsie. Wymarła wioska, jedyne miejsce to domek Olgi na uboczu. Wraz z jej szerokimi ziemiami. Dlaczego to jej ziemie? Bo ona je objęła i nikt jej nie próbował ich zabrać. Marysia widzi dziwne, atrakcyjne rośliny, nietypowe zwierzaki - ogólnie, atrakcyjne miejsce które WYGLĄDA jakby chciało Cię zabić. I grzyby.

Olga jest sympatyczna; nie patrzy w oczy, jest ubrana praktycznie. Marysia przedstawiła jej problem - zginął jeden człowiek i kwestia tego, by nie zginął ktoś jeszcze. Czy dałoby się zrobić tak, że jest opętany przez potwora? By nie eskalować? By nie rozpętała się z tego wojna?

Olga - no ale jakiś niewinny potwór czy klad potworów będzie atakowany / będą na niego polowali terminusi. To skrzywdzi potwory. Marysia odbiła, że to nie musi być vicinius - to może być bezrozumny potwór jako siła natury.

"Jego umysł jest inny niż nasz". - Olga do Marysi - "Nie wiem, co możesz mu dać. Ale najlepiej weź jakąś operę. Wróć pojutrze."

Marysia zaproponowała Oldze pomoc - zwierzęta, karma czy coś. Niestety, Olga weszła w tłumaczenie czemu na tym terenie to zły pomysł i czemu glukszwajny + ekologia + energie + gleba... dwie godziny później Marysia żałowała, że zapytała.

DWA DNI PÓŹNIEJ.

Marysia przyleciała ścigaczem. W domku Olgi czekał Wiktor Satarail. Olga też była obecna, ale wyszła zająć się zwierzątkami. 

Wiktor jest przesympatyczny i elegancki. Po usłyszeniu jak wygląda sprawa poprosił Marysię, by wyciągnęła dłoń. Z jego dłoni wypełzł robak, coś pomiędzy szczypawką i osą. Wiktor powoli zbliża robaka do ręki Marysi i patrzy jej w oczy. Marysia nie okazuje słabości. Robacza iniekcja BOLI, ale nie boli bardzo mocno; w końcu robak się umościł. Wiktor powiedział, że Marysia jest wektorem do Torszeckiego. Pasożyt przepełznie do Torszeckiego i nie zostanie w Marysi. Ona nie będzie mieć efektów ubocznych, bo on został ładnie poproszony.

Ale Torszecki jest tylko żywicielem pośrednim. Wystąpią w nim symptomy. I doktor stwierdzi to co powinien.

Marysia chciała dopytać, dowiedzieć się - co to jest? Co się stanie? Wiktor Satarail jedynie stwierdził, że potwór musi być wiarygodny i nikt niewinny nie ucierpi. Plus, nikt nie zginie. A Marysia jest albo bardzo głupia, naiwna, lub zdesperowana - dała sobie wsadzić robaka pod skórę. Wiktor stawia na to trzecie.

Kiedyś Marysię zawoła do siebie. Wtedy ona ma przyjść. To nie była prośba z jego strony.

Marysia opuściła to miejsce z lekkimi ciarkami i robakiem pod skórą. I w rękawiczce. Nawet w dwóch by nie wyglądało dziwnie.

W DOMU - Marysia powiedziała Karolinie co zrobiła i jaki ma plan. Karolina spojrzała na wódkę i wzięła solidny, słowiański łyk.

* Karolina: "Słuchaj, czy Ty chcesz za Torszeckiego wyjść?"
* Marysia: "Mówiłaś, że o niego nie dbam. A co, mam pozwolić go zabić?"
* Karolina: "No nie..."
* Marysia: "Masz inne rozwiązanie? Pozwolić Ernestowi go zabić? Niech gania za Torszeckim?"

Marysia: "Pogadaj z lekarzem bym mogła dotknąć Torszeckiego." Karolina: "Sama sobie gadaj i dotykaj..." Marysia: "CHODZI O ODWRÓCENIE UWAGI LEKARZA" Karolina: "Bo on mnie nie lubi, uważa mnie za mafię."

Ale jak Marysia bez Karoliny ma odwiedzić Torszeckiego i dać mu robaka jak lekarz na nią PATRZY i to PODEJRZLIWIE? I co powie Torszeckiemu?

Dobra. Desperackie czasy, desperackie metody. Marysia ZROBIŁA WŁASNORĘCZNIE CIASTECZKA. Są przypalone, najpewniej niejadalne, ale własne. Marysia MUSI dostać się do Torszeckiego i przekazać mu robaka. Jakoś. Więc musi przejść przez lekarza. Który jej nie cierpi i chroni przed nią Torszeckiego (który wyłączył hipernet).

Marysia idzie do lekarza - Sensacjusza Diakona. Próbuje przekonać go, że musi spotkać się z Torszeckim. Jak on się czuje, podziękować mu, przeprosić. Nawet ciastka mu zrobiła. Że sama. Sensacjusz się ZAWIESIŁ. "Ja mogę panu wyjaśnić. Całą noc myślałam.". MEGA ZBOLAŁA MINA SENSACJUSZA. ON JUŻ WIE. ON NIE CHCE SZCZEGÓŁÓW. A już na pewno żeby ona się nie rozpłakała... To byłby crit.

"Gdybym go przepraszała przy panu to by było krępujące"

TrZ+4+5O:

* X: Sensacjusz wierzy. WIERZY. WIE. Że Torszecki x Marysia to fakt. Widzi po tych ciastkach. To jest dowód.
* Vz: Sensacjusz dopuści Marysię do Torszeckiego, ale będzie obecny. Bo ciasteczka.
* V: Sensacjusz zostawi Marysię i Torszeckiego razem. Nie będzie przy nich.
* O: Pojawiają się plotki o Marysia x Torszecki, ale te plotki na razie nie są bardzo wiarygodne.
* Vz: Sensacjusz ZMIENIA ZDANIE o Marysi. To zagubiona młoda kobieta, przemierzająca skomplikowany polityczny świat. Ale zdolna do miłości. Nie to co Amelia.

Marysia idzie odwiedzić Torszeckiego w jego pokoiku w szpitalu. Lekarz taktownie zostaje na zewnątrz i nie podgląda by nie zobaczyć czegoś, czego nie chce widzieć. A Marysia na wejściu "Tosiu, jak dobrze Cię widzieć!"

Torszecki zgłupiał.

* "Mój drogi, nie jesteś wart by wypowiedzieć o Ciebie wojnę z Eternią! Mam inne rozwiązanie" - faktycznie oburzona Marysia
* Zgadzam się z Twoim rozumowaniem - Torszecki
* Mam rozwiązanie. Rafał, ono nie jest przyjemne. Ale będziesz żył i nikt Cię nie będzie ścigał. Ale nie możesz o tym nikomu powiedzieć. Bo nie będziesz żył bo ja tego dopilnuję. - Marysia, coraz bardziej zdesperowana by nie wyszło że go lubi. By sobie nie pomyślał.
* ... - Torszecki, nic nie rozumiejąc. Bo nic nie wie. I słyszy słowa. I nie rozumie.
* Powiem Ernestowi że to Ty zabiłeś jego człowieka. Ale powiemy że byłeś opętany. I to opętany zabiłeś jego człowieka. Czy na to przystajesz?
* No... nikt nie uwierzy. Znaczy, jak na Twój plan to jest... kiepski? - Torszecki, nie dodając dwa i dwa
* Kolejna część planu... będziesz opętany - Marysia
* JAK! Jestem dokładnie przebadany. Nie mam opętania. Nie jestem pod wpływem. Nie zadziała - Torszecki, zimno - Łapiesz się desperackich sposobów
* <Marysia ściąga rękawiczkę> I właśnie, khm, przyszłam Cię opętać, mój drogi. Dotknę Cię i przekażę Ci opętanie.
* Co? - Torszecki - Jak? Ale... co?
* Muszę Cię dotknąć ręką. Widzisz tą rękę? Tu jest opętanie...
* Marysiu? Czy Ty się... czy Ty się czujesz dobrze? - Torszecki, z FAKTYCZNĄ troską - Słuchaj, jak mnie zabije, to mnie zabije. Będę walczył...
* Rafale. Trzy zdania. By CIę nie zabił Ernest poszłam do Wiktora. Załatwiłam opętanie. Ma efekty uboczne. Ale to musi być wiarygodne. Albo przyjmiesz opętanie albo sztylet Ernesta.
* Marysiu..? POSZŁAŚ DO SATARAILA?
* <DOTYKAM GO! MA ROBAKA!>.

Boli. Ale zacisnął zęby. Krzyknął, z zaskoczenia. LEKARZ NIE PRZYSZEDŁ. ZINTERPRETOWAŁ PO SWOJEMU. Marysia przekazała robaka.

* Nie wiem co powiedzieć... - Torszecki w głębokim szoku
* Najlepiej nic - Marysia. 100% zażenowania całą sytuacją. Bo jeszcze coś powie. I wkurw jest.
* Poszłaś dla mnie do Sataraila. Sama. Zaplanowałaś to wszystko. Dla mnie. - Torszecki, dalej w szoku. - Przeniosłaś... coś. W sobie.
* Ale o co Ci chodzi? - Marysia, wzrokiem typu "wtf, to nic takiego".
* Narażałaś się dla mnie. Sama. Nie użyłaś kogoś. Pierwsza rzecz jaką zrobiłaś sama i to dla mnie - Torszecki, w coraz większym szoku - I zaufałaś mi z sekretem który może Cię pogrążyć. Tym, że ukrywasz mnie przed Ernestem, że to sfabrykowałaś.
* Nie jest to PIERWSZA rzecz którą zrobiłam sama. Nie wszystko o mnie wiesz. Należy dbać o swoich ludzi. A po drugie, ostatnie czego chcemy to wojna z Ernestem z Eterni. I Morlanem. To byłaby polityka. - Marysia, spokojnie i zimno. Choć ma dużą ochotę rzucić w niego ciasteczkami.
* Zrobiłaś **dla mnie** ciasteczka? - Torszecki ma apogeum szoku.
* Rafał, a jak miałam tu wejść jak mnie Sensacjusz nie lubi a Ty wyłączyłeś hipernet? - Marysia
* ...wow - Torszecki ma pełną przebudowę rzeczywistości
* To się nazywa TAKTYKA WOJENNA - Marysia. Z kropką nienawiści.
* ... - Torszecki trawi. Wziął ciasteczko i skosztował. Wyraźnie się skrzywił. Ale udaje, że mu smakują. To widać.
* Możesz powiedzieć, że Cię żołądek boli. OPĘTANIE POWINNO ZACZĄĆ DZIAŁAĆ w ciągu kilku dni. Nikomu się masz nie sypnąć. I przeżyj.
* Nikomu nic nie powiem. Przysięgam. - Torszecki. Jedząc ciasteczka. Z bananem na pysku.
* Mogą być drastyczne skutki uboczne, ale będziesz żył. (Marysia specjalnie nie mówi czy po ciasteczkach czy "opętaniu")

A MARYSIA IDZIE PIĆ. OSTRO PIĆ. TO WYMAGA PICIA.

## Streszczenie

By ratować Torszeckiego, Marysia wchodzi w sojusz z Wiktorem Satarailem. On dał jej podskórnego robaka którego dostanie Torszecki, by uzasadnić dziwne zachowanie. Wiktor "dostanie swoją zapłatę" od "kogoś winnego". Marysia przekonała też Sensacjusza, że ona x Torszecki. Zaczęły się też pojawiać takie plotki...

## Progresja

* Marysia Sowińska: pojawiają się plotki na temat jej romansu z Torszeckim. Na razie ignoruje.
* Marysia Sowińska: Sensacjusz Diakon uważa ją za zagubioną młodą kobietę, ale zdolną do miłości. Nie to co Amelia. Czyli Sensacjusz jest potencjalnym sojusznikiem.
* Paweł Szprotka: jest przekonany, że Marysia Sowińska (tienka) traktuje go tylko jak zasób. Ale obiecała, że będzie go chronić. Więc jest dobrze.
* Rafał Torszecki: pojawiają się plotki na temat jego romansu z Marysią Sowińską. Na razie nie podsyca.
* Rafał Torszecki: jest przekonany, że Marysia Sowińska się w nim podkochuje. Mówi mu groźne dla siebie sekrety i zaryzykowała nawet Sataraila by go ratować.
* Sensacjusz Diakon: zmienia opinię na temat Marysi Sowińskiej. To zagubiona młoda kobieta, przemierzająca skomplikowany polityczny świat. Ale zdolna do miłości. Nie to co Amelia.

### Frakcji

* .

## Zasługi

* Marysia Sowińska: wzięła Pawła Szprotkę pod ochronę przed Eternią, po czym wynegocjowała z Satarailem, że on da jej Robaka do zainfekowania Torszeckiego (by go chronić). Wszystko zaczyna wyglądać jakby podkochiwała się w Torszeckim.
* Paweł Szprotka: przyznał się Marysi, że jest zbiegłym eternianinem i zdobył zapewnienie, że ta będzie go chronić. Zorganizował Marysi spotkanie z Olgą. Jest weterynarzem. Chroni go też "Dama w Błękicie". Boi się tego, że tu jest Ernest z Eterni. Uważa, że Ernest jest tu go porwać (srs?).
* Ignacy Myrczek: okazuje się, że dorabia sobie jako kucharz i jest współlokatorem Pawła Szprotki.
* Olga Myszeczka: skontaktowała Marysię z Wiktorem, nie chce nic za to. Praktyczna i sympatyczna, acz żyje na uboczu. Ostrzegła Marysię, że Wiktor nie myśli jak człowiek.
* Wiktor Satarail: po otrzymaniu od Marysi opery dał Marysi robaka do ratowania Torszeckiego. "Winny zapłaci". Uznał Marysię za zdesperowaną lub arogancką.
* Rafał Torszecki: nie wiedział jak z tego z Ernestem wyjść, ale Marysia przyszła do niego z planem. Zaakceptował ten plan - wziął od niej robaka podskórnego. Musi go w końcu kochać.
* Sensacjusz Diakon: Marysia go wkręciła, że ona x Torszecki i to łyknął. Pozwolił im się spotykać i kibicuje gorąco ich związkowi (mezaliansowi?).
* Teresa Mieralit: dla Pawła Szprotki jest "Damą w Błękicie". Chroni go i daje mu pracę, bo chce jego powodzenia. Tak jak kiedyś Klaudia i Ksenia chroniły ją. I ofc Arnulf.
* Ksenia Kirallen: KIEDYŚ miała konia z pozytywną energią magiczną. Po jej "wypadku" oddała go na kampus uczelni. Bo tu jest bezpieczny i może pomagać innym.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Dzielnica Luksusu Rekinów
                                    1. Serce Luksusu
                                        1. Lecznica Rannej Rybki: leży tam Torszecki, Marysia z nim rozmawia bo Sensacjusz pozwala i Marysia przekazuje mu robaka od Sataraila.
                            1. Czarnopalec
                                1. Pusta Wieś: Wiktor Satarail spotyka się z Marysią Sowińską i daje jej robaka podskórnego. On dostanie swoją zapłatę.
                            1. Zaczęstwo
                                1. Akademia Magii, kampus
                                    1. Akademik: Marysia spotyka się z Pawłem Szprotką, poznaje jego historię i je wegańskie jedzenie zrobione przez Myrczka na tę okazję.

## Czas

* Opóźnienie: 2
* Dni: 4

## Konflikty

* 1 - Marysia Sowińska próbuje poznać prawdę o Pawle Szprotce i co ten ma do Eterni.
    * ExZ+3
    * XzVzV: Paweł jest przekonany, że Marysia przybyła tylko go wypytać, ale może go uratować i osłonić. Więc powiedział prawdę. I Marysia będzie go chroniła przed Eternią. PLUS dama w błękicie, kimkolwiek jest.
* 2 - Marysia idzie do lekarza - Sensacjusza Diakona. Próbuje przekonać go, że musi spotkać się z Torszeckim. "Gdybym go przepraszała przy panu to by było krępujące"
    * TrZ+4+5O
    * XVzVOVz: Sensacjusz wierzy że Torszecki x Marysia. Dopuści do ich spotkania. Pojawiają się o nich plotki. A Sensacjusz zmienia zdanie o Marysi na lepsze.

