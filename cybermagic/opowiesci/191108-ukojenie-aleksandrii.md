## Metadane

* title: "Ukojenie Aleksandrii"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić, fox

## Kontynuacja
### Kampanijna

* [190817 - Kwiaty w służbie puryfikacji](190817-kwiaty-w-sluzbie-puryfikacji)

### Chronologiczna

* [190817 - Kwiaty w służbie puryfikacji](190817-kwiaty-w-sluzbie-puryfikacji)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

brak

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

#### Pierwszy kontekst

Kiedyś

* Leszek miał kochającą rodzinę i był uznanym pisarzem
* Ukochana córeczka Leszka i Danieli, Klara, rozbiła się na ścigaczu ze swoim chłopakiem, Emilem.
* Leszek i Daniela oddali wszystko. Ale to nie było dość. Moc Klary obróciła się przeciwko niej. Nie potrafiła wrócić.
* Leszek otrzymał Aleksandrię. Potrzebował pieniędzy i wiedzy. Zaczął porywać ludzi.
* Leszek zaczął przekierowywać energię Aleksandrii by rosnąć w wiedzę medyczną i jako pisarz.
* Daniela próbowała uciec, ratować Leszka, ratować Klarę... została jednak schwytana i Spętana mocą Kuratora.
* Leszek przywrócił Klarę i Danielę.
* Emil - w odpowiedzi - stał się echem.

Zakres

* Leszek ma wsparcie lokalnego rodu magów z Aurum, dokładniej: Lemurczaków. Kamil Lemurczak docenia jego zmysł artystyczny.

#### Sesja

Pytanie: "Co się stało z Zenkami"

## Misja właściwa

### Prolog

Firma Zenek & Zenek - niezbyt szacowni i niezbyt szlachetni przedstawiciele gałęzi przemysłu "pan majsterkowicz". Zostali poproszeni przez dość bogatych ludzi mieszkających w Kruczańcu o zamontowanie ich własnego, "lewego" kolektora taumicznego. Jednak Zenki nieco zafrasowane: po pierwsze, jest w szopie inny oczyszczacz taumiczny (który wygląda na dość sprawny) a po drugie... co nie montują, to zaraz się okazuje, że wskazania pola tła przekraczają 200% normy.

Pierwsza myśl - Amelia załatwiła im kiepski lewy sprzęt. Ale po przekonfigurowaniu (by nie było widać) okazało się, że w środku zabezpieczenia zaczynają się smażyć. Faktycznie, chyba poziom taumiczny jest za wysoki. Zenki poinformowały Amelię i poprosiły o wsparcie lokalnego terminusa... na tym skończył się sygnał.

### Sesja właściwa

W bazie Zenek & Zenek dwie pozostałe osoby są lekko zirytowane. Zarówno Kinga (egzorcystka, wojskowa) jest niezadowolona z tego że Zenki zniknęły jak i Amelia (majsterkowicz, cinkciarz). Ale trudno o dobrych podwykonawców a z Zenków da się czerpać niezłe złoto - trzeba naprawić sprawę. Pojechały więc do owego Kruczańca; zobaczymy co tam się dzieje.

Sam Kruczaniec wygląda na dość normalne miejsce, ale dwie czarodziejki zajmujące się skażeniem taumicznym na co dzień zauważyły, że poziom energii magicznej jest anomalnie wysoki. Jak bardzo wysoki? Ano, trzeba sprawdzić - pojeździły po mieście i znalazły cztery szczególnie silne miejsca:

* Zamek należący do Leszka, lokalnego wpływowego pisarza. Leszek regularnie pomaga ludziom przed samobójstwami i podnosi pozytywne emocje.
* Jezioro słynące z tego, że wywołuje jakieś depresyjne fale; ludzie się tam często topią. Takie miejsce samobójstw i wciągające.
* Lasek zwany Upiornym, w którym straszy upiór na ścigaczu
* Niedaleki zamknięty supermarket Złotko, który jest miejscem gdzie często bywają koncerty itp.

Nadal, Amelię i Kingę złości to, że nie wiadomo co do cholery robi lokalny terminus - jest nadmiar pola magicznego, Zenki zniknęły i w ogóle jest dziwnie. Tak więc - czas na zasadzkę. Kinga poszła do Złotka by tam zrobić efemerydę i ściągnąć terminusa - a w tym czasie Amelia się wkradnie do bazy terminusa i dowie się co tu się dzieje z materiałów i raportów. Wykonały plan.

Kinga wezwała "czarną wołgę" i terminus faktycznie przyleciał. Kindze nie udało się schować, był szybszy niż to możliwe, więc Kinga zaczęła walczyć z wołgą - by pokazać temrinusowi że jest po jego stronie. Terminus wsparł ją i zaczęli gadać. Podczas tej rozmowy Kinga odebrała wrażenie, że terminus jest dość "automatyczny". Taki nieco dziwny.

Amelia włamała się do domu terminusa (dublujący za bazę). Nigdy jeszcze nie widziała tak FAŁSZYWEGO miejsca. Gość jest jak robot - wszystko jest takie jak powinno być, nie ma duszy. Jest to dziwne. Przekazała szybko Kindze. Kinga zatrzymała terminusa na dłużej, spowolniła go - a Amelia skutecznie zamontowała pułapkę. EMP. Największa, najbardziej potężna, najsilniejsza EMP jaką kiedykolwiek udało jej się złożyć. Skala SUPER (4 poziomy eksplozji!!!).

Gdy terminus wrócił do mieszkania (a Kinga odpowiednio daleko by jej servara nie usmażyło) doszło do erupcji EMP. Siła EMP była tak wielka, że uderzyła nie tylko w terminusa ale i w Aleksandrię Kuratorów - Aleksandria była zmuszona do natychmiastowego przejścia w tryb ochronny by uniemożliwić krzywdę ludziom i istotom zdigitalizowanym. To sprawiło, że wpływ Kuratorów na terminusa się wyłączył.

Terminus zaczął płakać. Zaczęło do niego dochodzić co się stało. Zaczął sobie przypominać:

* Rok temu przyszła do niego w nocy Daniela (żona Leszka), przerażona. Dzieje się coś strasznego. Niech terminus Paweł ich ratuje.
* Przyszedł Leszek. Pokonał Pawła - był silniejszy magicznie, jest jak arcymag. A potem maszyna.
* Powiedział o Aleksandrii. Aleksandria, silniejsza niż cokolwiek innego. Nie jest w stanie poradzić sobie z siłą Aleksandrii...
* Aleksandria wraca. Jest pod jeziorem. Tam są Zenki. Tam jest Marysia, jego żona. Aleksandria sprawia, że jego to nawet nie boli...

Zanim Paweł wrócił pod kontrolę Aleksandrii, Kinga wsadziła go w swój servar i włączyła tryb 'paralizer'. Paweł jest zdjęty, acz Kinga nie ma servara. Niefortunne. Oki - jak się wbić do Aleksandrii?

Czas na szperanie po bibliotece. O co tu w ogóle chodzi? I poznali w końcu prawdę.

* Leszek i Daniela mają kochaną córkę, Klarę.
* Klara zginęła w wypadku, na ścigaczu Emila. Jest do uratowania ale to nie działa.
* Leszek robi co może by ją przywrócić. W końcu trafił na Aleksandrię, zniszczoną Aleksandrię - i udało się ją naprawić.
* Leszek włączył się w Aleksandrię. ALEKSANDRIA ODPOWIEDZIAŁA i Klara powróciła - echo wspomnień Klary.

...to nie Klara. To Kuratorzy. Kurator zstąpił do Aurum przez Aleksandrię.

Jak tam wejść?

Amelia ma pomysł. Trzeba użyć neurotoksyny. Złożyła taką magią - i złożyła ją ZA SILNĄ. Skażenie. Za silna energia w powietrzu. Wszystko śpi. Wszyscy śpią. Aleksandria nie jest w stanie użyć żadnego agenta. Wszyscy, którzy są aktywni i funkcjonują poprawnie są nieprzytomni. Całe miasto śpi.

Aleksandria przyzwała Klarę. Klara nie żyje. Klary nie dotyczy neurotoksyna...

Gdy Kinga i Amelia zbliżyły się do Aleksandrii, na drodze stanęła im "Klara". Spokojnie wyjaśniła, że zniszczą Aleksandrię. Zniszczą Leszka i jego marzenia. Zniszczą wszystkie życia które Leszek ratował. Zniszczą tych, których Aleksandria już zdigitalizowała. Aleksandria czyni dobro i tylko dobro. Tak, musi zabijać by przeżyć, ale daje im utopię. Kinga jest bardzo przeciwna, ale Amelia... Amelię udało się skusić. Amelia chce stworzyć model biznesowy, m.in. używając osób chorych nieuleczalnie.

Kinga odzyskała Zenki; Aleksandria ich oddała. Jednak potem przekazała informację osobom wyżej. Jakkolwiek lubi Amelię... a może DLATEGO że lubi Amelię, nie może pozwolić sobie na to, by Aleksandria działała na pełnej mocy. Nie tak.

Tor: 

* (20) - Zenki są stracone

**Sprawdzenie Torów** ():

* .

**Epilog**:

* Zenki uratowane.
* KTOŚ wie o Aleksandrii i chce ją uratować.
* Amelia rozkręca model biznesowy by użyć, utrzymać i pomóc Aleksandrii.

## Streszczenie

Niewielka firma oparta na niezbyt uczciwym modelu biznesowym "majsterkowicze z odzysku" natrafiła na uszkodzoną Aleksandrię Kuratorów, która została użyta przez pisarza który prawie stracił córkę. Aleksandria ją reanimowała. Pisarz powiększa Aleksandrię i praktycznie stał się Kuratorem; Zespół pokrzyżował mu plany i praktycznie zniszczył Aleksandrię. Tylko dobre serce Amelii (i okazja do zysku) sprawiły, że Aleksandra przetrwała - acz z Amelią jako koordynatorem biznesowym i memetycznym.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Kinga Stryk: egzorcystka i wojskowa. Skutecznie odwraca uwagę terminusa oraz przyzywa efemerydy. Katalistka i organiczny detektor magii. Pracuje z Zenkami i Amelią.
* Amelia Mirzant: majsterkowiczka i cinkciarz. Konstruktor najwyższej klasy - hiper-EMP oraz hiper-neurotoksyna. Przejęła kontrolę nad Aleksandrią.
* Leszek Baszcz: pisarz, który stracił córkę. Opętany żałością, zintegrował się z Aleksandrią i córka wróciła. Stał się organicznym Kuratorem.
* Daniela Baszcz: żona Leszka, która chciała ratować rodzinę. Zniewolona przez Kuratorów, stała się szczęśliwą marionetką.
* Klara Baszcz: nieżywa córka Leszka i Danieli. Kurator. Nie ma wielkiej siły ognia, ale ma wiedzę i umiejętności negocjacji Aleksandrii.
* Paweł Kukułnik: lokalny zniewolony przez Aleksandrię terminus. Pokonany przez Leszka wpakował do Aleksandrii własną żonę. Nie może cierpieć.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Lemurski
                            1. Kruczaniec: niewielka i klimatyczna miejscowość na terenie Lemurczaków; chwilowo z rozwijającą się Aleksandrią.
                                1. Zamek Pisarza: miejsce silnie Skażone, prowadzące pod Jezioro Topielców. Należy do Baszczów. Mimo obrony - Zespół wszedł tam neurotoksyną.
                                1. Upiorny Las: kiedyś tu się rozbił Emil z Klarą; od tej pory Emil nawiedza ten teren jako upiór, echo.
                                1. Jezioro Topielców: pod nim znajduje się Aleksandria sprzężona z Leszkiem Baszczem. Tu często ludzie popełniają samobójstwa.
                                1. Supermarket Złotko: miejsce upadłe. Czasem są tam wyścigi ścigaczy, czasem koncerty. Tym razem - Kinga wezwała tu efemerydę.

## Czas

* Opóźnienie: 9
* Dni: 2
