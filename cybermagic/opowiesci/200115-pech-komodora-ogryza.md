## Metadane

* title: "Pech komodora Ogryza"
* threads: unknown
* gm: żółw
* players: kić, fox

## Kontynuacja
### Kampanijna

* [200106 - Infernia i Martyn Hiwasser](200106-infernia-martyn-hiwasser)

### Chronologiczna

* [200106 - Infernia i Martyn Hiwasser](200106-infernia-martyn-hiwasser)

## Uczucie sesji

* .

## Punkt zerowy

.

### Postacie

.

## Misja właściwa

### Scena zero

.

### Sesja właściwa

Martyn Hiwasser zebrał Janet i Klaudię. Powiedział im o tym, jak ciężkie życie ma Wioletta - Ogryz jej nie lubi (bo Martyn ją lubi), ogólnie trzeba by jej pomóc. Martyn wpadł w przeszłości w pułapkę bo szedł na randkę; nie widzi nadal że to jest coś co może go pokonać i zrobić mu koszmarną krzywdę (jest przewidywalny). Janet i Klaudia spytały czego Martyn od nich chce - on chce, by Wioletta sprawdziła się w grach wojennych. Pokonała siły Ogryza. A raczej - była ponad najniższym agentem Ogryza.

Dziewczyny zaproponowały Martynowi by jednak poszedł na następną randkę ale dlatego bo to najpewniej pułapka. Martynowi nie trzeba dwa razy takich rzeczy mówic - ale w tle Janet zaczęła działać i szukać jak zastawić pułapkę na Ogryza. I faktycznie - gdy Ogryz napadł Martyna (a raczej jego ludzie, też ci interesujący się grami wojennymi), zaczął atakować ich oddział żołnierek, które lubią MArtyna, z różnych jednostek. No i ludzie Ogryza pobili kilka fajnych, ładnych dziewczyn (żołnierek) które były lubiane na stacji. Sam Martyn trafił do szpitala a i sporo dziewczyn jest poturbowanych. Ludzie Ogryza natomiast trafili pod "czułą opiekę Janet".

W tym momencie plan Janet się zmienił. Nie jest jej celem próba wsadzenia Wioletty na "lepszą niż ktoś od Ogryza". Jej celem jest "niech Ogryz weźmie Wiolettę do swojego oddziału". Celem tego spotkała się z Sonią Ogryz - żoną Dominika - i zaczęła jej opowiadać o sytuacji. Okazało się, że Sonia jest zafascynowana podróżami kosmicznymi, walkami, wszystkim... tym co robi Janet. a czego nie robi Dominik. Usłyszawszy jaki numer Ogryz wykręcił Wioletcie przez Hiwassera, Sonia się wzburzyła. Zrobiła awanturę Dominikowi, publiczną, jeszcze bardziej dewastując jego reputację. Co gorsza, przekazała Janet jeden ze statków kosmicznych które otrzymała w spadku - a dokładniej, przekazała Janet "Rozbójnika" z bardzo wybuchową i chętną do walki załogą.

Oczywiście, w wyniku tego wszystkiego Ogryz się z Janet silnie skonfliktował. Janet ma wroga...

Kolejnym krokiem jaki stanął przed Klaudią i Janet było to, że Wioletta naprawdę, naprawdę nie do końca potrafi czegokolwiek konstruktywnego w grach wojennych Orbitera zrobić. Janet wzięła grupę agentów Ogryza których przechwyciła i pokazała im ich przyszłość - są "oskarżeni" o bycie złymi ludźmi, bicie kobiet... ogólnie, przechlapane. Jak mogą się zrehabilitować? Ona im pomoże - ale niech zrobią z Wiolettę agentkę która ma sens i może skutecznie zadziałać. Jako, że Janet obiecała pomóc im oczyścić reputację, postarają się z całych sił, nawet Wiolettę zarekomendują.

Tymczasem Klaudia rozpuszczała wideo i podgrzewała obrazy z walk, nakręcając wszystkich na Ogryza. Doszło do tego, że członkowie jego załogi w pięciu do ubikacji musieli zacząć chodzić. To strasznie eskalowało, dużo powyżej tego co planowała Klaudia i Janet. Ups. Ale dzięki temu Ogryz zrozumiał, że ma problem. Faktyczny problem, a nie "oj, coś jest nie tak".

I na to weszła Janet - spotkanie z Ogryzem. Podczas spotkania wyszła ze stanowiska "nie chciałam Rozbójnika, ale mam, pomogę Ci z tym tutaj":

* Janet musi wziąć Damiana Ogryza na bezpieczną, fajną akcję; na przykład... zdobycie Dorszant?
* Martyn Hiwasser musi przeprosić Ogryza (nie wie za co, ale przeprosi, to MARTYN jest)
* Damian Ogryz weźmie Wiolettę i da jej uczciwą szansę; i dla bezpieczeństwa i dlatego, że... no... ech.
* Między Janet i Ogryzem będzie zgoda.

W wyniku całości sesji:

* Wioletta, okazało się, nie sprawdziła się w grach wojennych. Nie jest to jej miejsce. Martyn się pomylił.
* Ogryz ma zniszczoną reputację na Kontrolerze. Musi iść w kosmos, nie jest w stanie działać jako polityk. Sonia jest szczęśliwa.
* Sporo osób skończyło w szpitalu. I nic nie zostało rozwiązane.

Sukces?

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Janet Erwon pomagała załodze Inferni znaleźć miejsce dla Wioletty na Kontrolerze Pierwszym. W wyniku intrygi Janet, Martyna i Klaudii - komodor Ogryz ma przechlapane na Kontrolerze Pierwszym, Janet dostała nowy statek (Rozbójnik) i musi z Ogryzem zdobyć stację Dorszant. A Wioletta, jak się okazało, jest dość niekompetentna w grach wojennych Orbitera. No i Ogryz ma przed sobą koniec kariery politycznej i początek kariery militarnej. Wszyscy przegrali..?

## Progresja

* .

### Frakcji

* .

## Zasługi

* Klaudia Stryk: w roli manipulatorki i biurokratki; ma doskonałe pomysły i wpakowuje Ogryza w coraz większe tarapaty podkręcając spiralę nienawiści na Kontrolerze Pierwszym.
* Janet Erwon: komodor Orbitera; bardzo lubi różnego rodzaju manewry taktyczne, oskrzydlanie i practical jokes. Poszła o krok dalej niż chciała i "wygrała" akcję z Ogryzem...
* Martyn Hiwasser: by chronić Wiolettę, połączył siłę Klaudii i Janet, powodując zamieszki na Orbiterze. Idzie na każdą randkę na jaką może. Skończył przez to w szpitalu.
* Wioletta Keiril: ma przechlapane na Kontrolerze Pierwszym; Janet i Klaudia wpakowały ją do sił Ogryza. Niestety, nie sprawdziła się, ale została już z Ogryzem...
* Dominik Ogryz: komodor Orbitera na Kontrolerze Pierwszym. Przez kosę z Martynem Hiwasserem i wejście w grę Janet i Klaudii stracił lukratywne miejsce jako polityk i wyruszył w kosmos jako podróżnik ku radości swojej żony. Aha, z Janet. I jednym statkiem mniej.
* Sonia Ogryz: żona Dominika Ogryza; z ambicjami. Absolutnie zachwycona Janet Erwon - oddała jej swój statek Rozbójnik. Marzy o przygodach!

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy: titan-class Orbiter HQ.
            

## Czas

* Opóźnienie: 17
* Dni: 3

## Inne

### Co warto zapamiętać

W pewnym momencie gracze byli zablokowani. W tym momencie kazałem wymienić 3 powody czemu Ogryz nie chce Wioletty w oddziale. To były:

* Jest słaba i niewyszkolona
* Ma lepszych
* Chce dopiec Martynowi

Są też rzeczy jakie mogą go przekonać. Jakie?

* Odzyska szacunek, PR
* Wioletta ma potencjał, ale wymaga szkolenia
* Gry wojenne nie są o nic ważnego
* W tej sytuacji tak jak jest Ogryz nie ma jak wygrać. Wioletta może uratować sprawę.

To dało graczom cele taktyczne. Każdy z tych "minusów" był aktywny. "Plusy" były pasywne. Celem graczy było zrobienie tego, by aktywnych plusów było więcej niż aktywnych minusów.

Technika zadziałała.
