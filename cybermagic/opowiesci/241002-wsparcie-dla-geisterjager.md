## Metadane

* title: "Wsparcie dla Geisterjager"
* threads: serce-planetoidy-kabanek
* motives: the-inquisitor, energia-praecis, zasoby-ludzkie-przeksztalcone, bunt-maszyn, zasoby-ograniczone, starcia-kultur
* gm: żółw
* players: kić, fox, til

## Kontynuacja
### Kampanijna

* [241023 - Nieplanowana ixiońska okazja](241023-nieplanowana-ixionska-okazja)
* [241218 - Amitay kontra śmierć](241218-amitay-kontra-smierc)

### Chronologiczna

* [210519 - Osiemnaście Oczu](210519-osiemnascie-oczu)

## Plan sesji
### Projekt Wizji Sesji

WAŻNE: Kontekst sesji i okolicy DOKŁADNIE opisany w Thread 'serce-planetoidy-kabanek'. Tam też znajduje się rozkład populacji itp.

* PIOSENKA WIODĄCA: 
    * Inspiracje
        * "Osiemnaście Oczu"
        * "Gdy Rzeczywistość Pękła, Brama Lirańska stała się Anomalią Kolapsu"
        * Ofiary z ludzi, by z Anomalii Praecis uzyskać Pracowników.
    * [Narcotic Thrust - "I like it"](https://www.youtube.com/watch?v=kq1iZ9cWeAI)
        * "I hate the treadmill everyday | I hate the mundane things they say | The boredom sets in nine to five | At night that's when I come alive"
        * "I like it when we go to extremes | I like it when we enter my dreams | I like it when I feel your touch | I like it so much"
        * Bardziej ambitne osoby w okolicach Anomalii Kolapsu chcą korzystać z okazji, chcą korzystać z życia.
        * Ludzie jako zasoby i marzenia ludzkie. Ludzkie cierpienie. Ludzka ambicja. Czasem białkowe ciała są najlepszą opcją.
* Opowieść o (Theme and Vision):
    * "You do what you can, to survive".
        * Natalia dołączyła do sił Orbitera, bo nie widziała lepszej nadziei dla siebie. Nie chciała zostać Tancereczką. A nie wierzy, że Orbiter coś z tym zrobi.
        * SC Patriaklest jest jednostką, która wie jak dostać się koło Anomalnych Korwet w Anomalii, by dotrzeć do miejsca Transformacji.
        * Ludzie często sprzedają innych ludzi, bo można z nich zdobyć akceptowalne pieniądze. Zwłaszcza, jak się jakieś znajdzie. Zwłaszcza noktianki.
    * "Są ludzie, którym nadal zależy"
        * Komodor Lodowiec przy wszystkich swoich wadach uratuje noktian. Wszystkich.
        * Kapitan Pawilończyk próbuje pomóc Lodowcowi i ludziom w swojej załodze.
        * Czesław Truśnicki, próbuje chronić kapitana Pawilończyka przed złem.
    * Lokalizacja: Okolice Anomalii Kolapsu, Libracja Lirańska.
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * the-inquisitor: Gabriel Lodowiec, wchodzi w głąb intrygi przy Kabanku szukając anomalnych krzyżówek, noktiańskich spisków i walczący, by Orbiter dalej miał możliwości działania.
    * energia-praecis: wtłoczenie człowieka do niewłaściwego schematu by ciężko pracował; "anomalia oswojona". To jest Twoje miejsce w egzystencji, pełne zniewolenie.
    * zasoby-ludzkie-przeksztalcone: wykorzystywanie ludzi przez anomalizowanie ich, transformacja ludzi w 'bioroboty', co wyskoczyło przy górnikach na planetoidzie LLP-112.
    * bunt-maszyn: zanomalizowani ludzie (bardziej golemy) zaczęli niszczyć wszystko dookoła.
    * zasoby-ograniczone: poważny problem, przed którym stoi Lodowiec. Potrzebuje sprzętu i jednostki wsparcia, nie ma nic.
    * starcia-kultur: pomiędzy praktycznie wojskowym OO Geisterjager Lodowca i luźnym acz wciąż kompetentnym OO Tezremont Pawilończyka.
* O co grają Gracze?
    * Sukces:
        * Zintegrować się z załogą i ją zrozumieć.
    * Porażka: 
        * Nieuzasadniona krzywda Natalii
* O co gra MG?
    * Highlevel
        * Sesja jest "CHARACTER-FIRST". Celem jest pokazanie kilku ciekawych postaci i lokalizacji.
        * PRIMARY: chcę, by zapoznali się z lokacją, z kilkoma postaciami i zrozumieli Anomalie oraz ten teren.
        * Cel: krótka sesja, w wyniku której gracze zapoznają się z postaciami, miejscem itp.
        * Czyli ta sesja to SCENA ZERO, ale szersza.
    * Co uzyskać fabularnie
        * Natalia ma problem, i ten problem jest coraz gorszy. Polaryzacja rośnie.
* Agendy
    * Gabriel Lodowiec (the-inquisitor)
        * wyplenić negatywne problemy z anomaliami i innymi siłami na tym terenie

### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

KONTEKST:

* Zgodny z THREAD

CHRONOLOGIA:

* Lodowiec robi co może by znaleźć przyczyny anomalii i problemów z ludźmi
* Lodowcowi kończą się surowce, nie dostał wsparcia

PRZECIWNIK:

* brak, anomalia?

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

Faza 0: TUTORIAL

* Tonalnie
    * Akcja, tutorial mechaniki
    * Pokazanie że jest ciężko
* Wiedza o świecie
    * Planetoidy, ubóstwo, problemy finansowe
    * Jakiego typu osobą jest Gabriel Lodowiec
* Kontekst postaci i problemu
    * .
* Implementacja
    * Planetoida wysyła SOS
    * Lodowiec przygotowuje Geisterjager, atakują planetoidę. Dowodzi osobiście, z orbity. Ludzie zabarykadowani
    * Bierze część wody i zapasów. (pokazuje graczom że to problem)

Faza 1:

* CZEMU RAZEM | CO PRZESKROBALIŚCIE
* "Przybyliśmy"
* Renata atakuje przybyty statek
* Mowa Lodowca, przesuwa ich na Tezremont (noktianie, kapitan Pawilończyk)
* Opowieść o złych noktianach Warząkiewicza, Natalia jest podejrzana, widzą jak się chowa, przyszła 2 tyg temu

## Sesja - analiza
### Fiszki
#### Orbiter, OO Geisterjager, fregata dowódcza

* Gabriel Lodowiec: komodor Grupy Wydzielonej Mrówkojad Lirański oraz kapitan OO Geisterjager
    * Commander: komodor Orbitera; dowodzi 2 statkami. Dobrze przydziela ludzi do zadań. Dobry taktyk. KIEPSKI w tematy HR.
    * Ekspresyjna Monomania (Light Yagami): bardzo wybuchowy, ale przywiązany do zasad i procedur. Zrobi to, co trzeba, by wygrać i wygra WŁAŚCIWIE. (O- C+ E0 A0 N+)
    * Próżniowiec, Uprzedzenie Do Grupy (noktianie), Weteran: bardzo doświadczony, nie lubi noktian. Ale jeszcze bardziej nie lubi przegrywać misji.
    * Echo Porażki: za Lodowcem do dzisiaj idzie fama "tego, który zrobił przerażającą mowę". Jest osobą, którą się straszy noktian.
* Dariusz Mordkot: pierwszy oficer, marine, dowodzi grupą uderzeniową.
    * Solutor: marine, uzbrojony i dobrze wyposażony
    * Niezależny Wizjoner (Miorine): bierze zadanie i je wykonuje. Pomysłowy, gotowy do nietypowych sytuacji. Lubi się chwalić i pokazywać klasę swych ludzi. (O+ C+ E+ A- N+)
    * Wytworna Elegancja: zawsze doskonale ubrany, w nieskazitelnym mundurze. Śliczne zęby i szeroki uśmiech. No z plakatu rekrutacyjnego.
* Renata Kaiser: oficer komunikacyjny
    * Jest Druga Szansa: wybacza, wierzy, że każdy - nawet noktianin - ma prawo do drugiej szansy. Sympatycznie neutralizuje Lodowca. (O+ C0 E+ A+ N0)
* Ofelia Miris: marine, druga dowodząca grupą szturmową
* OO Geisterjager: fregata dowódcza, 80 osób

#### Orbiter, OO Tezremont, szybka korweta adaptacyjna

* Arnold Pawilończyk: kapitan OO Tezremont pod Gabrielem Lodowcem; około 23 lata
    * Optymistyczny Entuzjazm (Bastian): Arnold jest optymistą i wierzy, że jak zrobi się wszystko dobrze to się jakoś ułoży. Odważny i skłonny do ryzyka. (O+ C0 E+ A+ N-)
    * Trochę za Młody: Arnold jest niestety bardzo wrażliwy na ładne dziewczyny i nieco za bardzo zależy mu na tym co pomyślą inni. Chce wszystkich zadowolić.
* Mateusz Knebor: pierwszy oficer OO Tezremont; 
    * Bezwzględny Mastermind (Shockwave): jego celem jest udowodnienie, że Pawilończyk nie jest dobrym kapitanem i spełnienie celu Orbitera. (O+ C+ E0 A- N0)
    * Kontakty z Syndykatem: nie wie o tym, ale ma informacje oraz dodatkowy sprzęt itp. od ludzi Syndykatu. To oni pokazali mu bezużyteczność Arnolda.
* Natalia Luxentis: cicha noktiańska advancerka, która dołączyła do Orbitera nie widząc lepszej opcji; około 33.
    * Advancer, Inżynier: lepiej czuje się w kosmosie niż w statku czy na planecie, nie jest może dobra w walce, ale świetnie radzi sobie z naprawami w kosmosie.
    * Marzyciel (Luna): ślicznie rysuje swoje światy i swoje historie, rzadko rozmawia z innymi. Ma bardzo neutralną minę, pragnie zadowalać innych. Lubi sprzątać. (O+ C- E- A+ N-)
    * Próżniowiec, Savaranka, Ten Obcy: noktiański nabytek Orbitera; Natalia próbuje być maksymalnie przydatna na pokładzie, ale unika kontaktu z innymi jak może. Overloaded.
* Sebastian Warząkiewicz: ogólnie 'dobry koleś' grupy, inżynier, kompetentny i lubiany, acz bardzo intensywny.
    * Inżynier, kowboj: dobrze strzela, dobrze walczy i spełnia się jako inżynier załogi. Zawsze pierwszy rusza w bój.
    * Ognista Pasja, Uprzedzenie do grupy: chce się bawić, tańczyć i śpiewać. A przy okazji wkopać noktianom, bo zniszczyli majątek jego rodziny i utknął tutaj. "Główny bohater". (O+ C- E+ A- N+)
    * Bodybuilder, Nieformalny Przywódca: Sebastian UWIELBIA ćwiczenia fizyczne i praktycznie ćwiczy cały czas. Do tego zna mnóstwo opowieści o Mroku Noctis i świetnie opowiada.
* Czesław Truśnicki: marine Orbitera, przypisany do OO Tezremont.
    * Solutor: świetny marine i żołnierz. 
    * Opiekuńczy Mentor: z natury dba o harmonię i rozwój innych, dba o każdego członka swojej załogi. Nie uważa, że cierpienie jednostki jest warte sukcesu grupy.
* Grzegorz Dawierzyc: drugi oficer i inżynier
    * Kompetentny oficer, choć niszczy morale podwładnych. Na pewno dowodzi inżynierią i silnikami na OO Tezremont.
    * Wieczny Maruda: narzeka na wszystko, ALE jest lojalny kapitanowi i Orbiterowi. Po prostu trudno z nim wytrzymać. (O- C+ E? A- N+)
    * Istnieje powód, czemu aż tak bardzo narzeka. Nie wiemy jednak jaki. PLACEHOLDER.
* OO Tezremont: szybka korweta adaptacyjna, 22 osoby
    * zdolna do poważnej zmiany wyglądu i posiada więcej skrytek niż powinna; działa jak QShip.

### Scena Zero - impl

Geisterjager, fregata dowódcza komodora Lodowca dostała sygnał SOS z planetoidy LLD-44. Grupa marines Orbitera ma przeprowadzić operację ratunkową z rozkazu komodora Lodowca. Teoretycznie nie muszą, ale Lodowiec nie zostawi planetoidy samej.

Operacja składa się z 4 promów bojowych, w sumie 20 marines, w servarach klasy Lancer.

Marines podłączają się do kamer podczas lądowania, ale lokalna TAI odpiera atak - wyraźnie jest nieco zagubiona i nie wie co robić. Jeden z marines wysyła kody lokalne Search and Rescue. Pomogło. AI dała wjazd do systemu. Da się zobaczyć co się dzieje.

Część górników została bardzo zmieniona. Są jakby częściowo zintegrowani z mechanizmami, ale nie jako jeden organizm tylko jako cyberwszczepy. i ci górnicy kopią. Wszędzie. Bez celu i całkowicie losowo.  

Marine wyciągnęła informację z TAI - kiedy to się zaczęło i skąd to się wzięło? Jej głównym celem było to, by na pewno nie wpaść w tarapaty. Polecenie komodora Lodowca - naszym ludziom nic się nie może stać. 

Tp +3:

* V: Zgodnie z danymi AI nic na nie wskazuje na zagrożenie z zewnątrz. Nie wnieśli niczego szczególnego. NAGLE, wpierw pierwszy zaczął 'kopać', potem drugi, potem trzeci. Nie, nie dokopali się do niczego. Czyli... nie ma powodu?
* Vr: 
    * AI próbuje być pomocna - pokazuje że ludzie się nie integrowali z tymi górnikami WCZEŚNIEJ. Oni nie przybyli w jednym czasie, ale nigdy nie byli traktowani jak reszta załogi.
    * To są ludzie biologicznie, ale lekarz traktował ich inaczej - dostarczył inne dane. 
    * To jest 14 górników z 40-osobowej populacji.
* V: Marine podpięli się do systemu głosowego z pomocą AI.

Gdy temat został podniesiony przez marines - nie mogą wpakować się w sytuację ratunkową, póki nie wiedzą co się dzieje (jako środek wpływu i negocjacji z górnikami), pozamykani w różnych miejscach górnicy zaczęli odpowiadać, że to nie są ludzie i nigdy nie byli ludzie. Ta planetoida kupiła tych pracowników i coś nagle bardzo poszło nie tak. to nie powinno być zaraźliwe. 

Komodor Lodowiec powiedział dowodzącej operacją po stronie marines Ofelii Miris, że życzy sobie, by na pewno nic złego nie stało się jego ludziom. Akceptuje straty w górnikach, nie akceptuje problemów po stronie Orbitera. 

Mając te wytyczne, marines przygotowali wejście z dwóch stron - zarówno od strony głównej windy jak i od strony awaryjnej. Skażeni górnicy nie zwracali szczególnej uwagi na Orbiter i nic nie wskazywało na jakąś szczególną komunikację pomiędzy nimi - Ofelia zdecydowała się uratować wszystkich górników, łącznie z tymi Skażonymi. obezwładniamy a nie zabijamy. 

Wpierw trzeba stworzyć bezpieczny korytarz by móc ewakuować nieSkażonych górników. 

* Lodowiec: "Status"
* Ofelia Miris: "Część ludzi w bazie zainfekowana, podobno inny typ ludzi niż ci w bazie, ewakuujemy niezarażonych"
* Lodowiec: "Spróbujcie zredukować zniszczenia. Priorytetyzujcie swoich nad innych."

Konfiguracja RIOT CONTROL. Lądujemy. Winda.

Tp +3:

* V: Udało się WEJŚĆ do bazy. Strzał gumowej broni ich oszołomił, ale nie wyłączył.
* X: Winda została uszkodzona i NAWET jeśli nic się nie dało zrobić to trudno to będzie wyjaśnić górnikom.
* V: Dwójka unieszkodliwiona i "gotowa do badań".
* X: Ktoś dostał kilofem lub laserem i jest lekko ranny, ale jest dehermetyzacja. (lepsze to niż śmierć kogoś)
* V: Udało się zabezpieczyć wszystkie ważne miejsca i tereny.
* V: Life support ich unieczynnił (chorych)

Udało się przejąć kontrolę nad bazą.

Debriefing.

* Lodowiec: Pani Miris, prosiłem wyraźnie o zachowanie rozsądku.

Komodor Lodowiec nie był szczególnie zachwycony wynikiem operacji. Jednak rozumie presję znajdującą się na marines. Zdaniem Lodowca nie wiadomo czy ranny marine będzie miał jakieś problemy, a Geisterjager nie ma magicznych badaczy na pokładzie.  

Tak czy inaczej, Lodowiec zażądał, by marines pozyskali część sprzętu, wody itp. z tej planetoidy jako zapłatę za ratunek. Nie jako akt piracki, a z uwagi na konieczność wyższą. On będzie rozmawiał z przedstawicielami planetoidy, by wszyscy rozeszli się w miarę zadowoleni.

### Sesja Właściwa - impl

Dwa tygodnie później.

* Q: CO PRZESKROBALIŚCIE ŻE WAS TU WYWALILI?
* A (Fox): Byłoby super, wyciągnęliśmy coś anomalnego co MIAŁO BYĆ bezpieczne a nie była. Annabelle miała swoje powody.
* Q: CZEMU JESTEŚCIE RAZEM I MOŻECIE SOBIE BEZGRANICZNIE UFAĆ?
* A: (Kić): basic training, i od tamtej pory razem. Te dwie wzięły najpierw maga naiwnego a on uratował łuski niejeden raz.

Zespół znajduje się na transportowcu i dociera do Libracji Lirańskiej. Ku wielkiemu zdumieniu zespołu, wpakowano ich w prom i zamiast dotrzeć na jakąś stację, zostali wprowadzeni przy niewielkiej stacji przeładunkowej Orbitera. 

Czeka na nich 4 żołnierzy, zdumionych faktem, że to tylko trzy osoby. Zanim prom zdążył się ewakuować, drobna blondynka w mundurze oficera zrobiła haję - nie ma jednostki wsparcia i dostała tylko 3 osoby. Poinformowała kapitana transportowca, że ma zamiar napisać skargę. Gdy prom odleciał, z uśmiechem zwróciła się do Zespołu przedstawiając się jako Renata Kaiser, oficer łącznościowy Geisterjager. 

Zespół nie jest w najlepszych humorach – o komodorze Lodowcu słyszeli wiele nieidealnych rzeczy. Ale cóż - taka misja. 

Annabelle przed akcją poszukała informacji o Geisterjager i w co w ogóle się pakują.

Tr Z+3:

* X: Annabelle skupiła się na najbardziej przystojnej osobie (Dariusz). 
* X: Lodowiec wie że Annabelle szczurzyła o Dariuszu (i oczywiście mu powiedział)
* V: Masz informacje jak wpływać na Dariusza: stały Zasób w konfliktach z nim.

Niestety, Annabelle zamiast skupiać się na sytuacji taktycznej, w pełni zainteresowała się bardzo przystojną osobą. Reszta zespołu lekko facepalmowała. Tego można było się po Annabelle spodziewać... 

Samo spotkanie z komodorem Lodowcem było nieco inne niż można było się spodziewać. Lodowiec zachowuje się skrajnie profesjonalnie, ale widać w jego ruchach lekką desperację.  

Gdy tylko zorientował się, że Annabelle smali cholewki do Mordkota, trochę umarł w środku. Naprawdę miał nadzieję na kompetentnych agentów, nawet jeśli problematycznych. Ale zgodnie z papierami ten zespół powinien dawać radę. A obecność Sylwii jedynie udowadnia, że powinni być naprawdę kompetentni. 

Gabriel Lodowiec z przyjemnością zniszczył marzenia Annabelle (w swojej głowie), bo poinformował Zespół, że przenosi ich na swoją drugą jednostkę - Tezremont. Mają za zadanie wyprowadzić tamtego kapitana na prostą, bo on nie ma kontroli nad HRem. 

To brzmi dziwnie. 

Lucjusz skorzystał z okazji, że dużo jest wymienianych oczek i spojrzeń, że Mordkot jest bardzo zainteresowany Annabelle a Sylwia prowadzi kompetentną rozmowę i samemu zaczął wykorzystywać swój status outsidera, by zrozumieć co tu się dzieje naprawdę. Lucjusz prawidłowo zidentyfikował komodora Lodowca jako osobę mniej ogarniętą politycznie i taką z “kijem w dupie".

I o co chodzi z "tą niebezpieczną noktianką" o której Lodowiec mówi, że trzeba chronić przed nią kapitana Pawilończyka?

Tr Z+4:

* X: Renata będzie Was OBSERWOWAĆ, jest sceptyczna i nie rozumie czemu tu jesteście. Bo przecież Annabelle nie może serio przybyć tu dla Dariusza.
* V: 
    * Lodowiec NIE KONTROLUJE korwety Tezremont. On WYDAJE JEJ ROZKAZY ale jej NIE KONTROLUJE. Nie ma kontroli nad HR tej jednostki.
    * Lodowiec faktycznie chce mieć dwie sprawne jednostki a uważa że ma jedną. Nie ufa kapitanowi Pawilończykowi.

Okazało się, że sytuacja jest dużo bardziej skomplikowana. Lodowiec nawet nie wie jak dużo powiedział Lucjuszowi - serio nie jest to komodor dobry politycznie. 

2 tygodnie temu na pokład Tezremont trafiła noktianka. Zdaniem Lodowca, ta noktianka to egzotyczna piękność (z perspektywy Pawilończyka), która na pewno jest szpiegiem. Bo po co miałaby tu jakaś noktianka trafiać? 

Lodowiec uważa Pawilończyka za młodego i obiecującego kapitana, ale nie ma kontroli nad jego załogą ani nawet nad jego korwetą. Dlatego chce tam Zespół. niech pokierują młodego kapitana, jak doświadczony sierżant pokieruje niedoświadczonym porucznikiem. 

Lodowiec nie ma wyjścia - musi zaufać nowoprzybyłym, bo po prostu nie ma żadnych innych środków. A Sylwia - jego zdaniem - powinna stanowić dobry wpływ.  

Lodowiec dał się Zespołowi odświeżyć; Mordkot z przyjemnością odprowadził trójkę do tymczasowych kwater. Tam Annabelle znalazła coś wspólnego (bo stalkowała go WCZEŚNIEJ) i "przypadkiem" podniosła temat jego interesujący, czyli "sport to zdrowie". Zaczęła z Dariuszem flirtować i wyciągać informacje.

Tp +3:

* V: Ogląd na sytuację statku. Dariusz opowie.
    * Jest problem z dziewczynami. Nie ma ich. Lodowiec nie pozwala na fraternizowanie się załogi. Lodowiec uważa to za niebezpieczne.
    * Lodowiec nie jest głupi, więc od czasu do czasu przybija do Kabanka.
    * Lodowiec podejrzewa wielki noktiański spisek, ALE ratuje też noktian i noktianki.
    * Poziom zasobów jest krytycznie niski.

Gabriel Lodowiec jest oficerem starej daty. Nie pozwala na fraternizowanie się załogi w pionie przywódczym. Jeśli dowodzisz kimś, nie możesz iść z tą osobą do łóżka. nie jest na szczęście całkowitym głupcem i wykorzystuje Planetoidę Kabanek jako miejsce, gdzie marynarze mogą wydać żołd i pofraternizować się lokalnie. Ale mimo, że on i Renata mają się ku sobie, wyraźnie do niczego nie doszło. Chyba.

Annabelle zaznaczyła, że ona i Dariusz NIE SĄ w tym samym pionie dowódczym. Dariusz się BARDZO ucieszył i faktycznie, pomyślał o tym - ale nie pomyślał, że Annabelle o tym pomyśli (bo ruch Lodowca przesunął Zespół na Tezremont). Annabelle się w duchu uśmiechnęła - ona, Egzotyczna Piękność, została wysłana na jednostkę z młodym kochliwym kapitanem. Wyraźnie Lodowiec nie pomyślał. Na pewno Renata Kaiser zrobi mu opieprz...

Tymczasem Sylwia zbiera plotki i informacje o jednostce Lodowca. Informacje czy np. ta załoga coś wie o drugiej korwecie.

* Lodowiec próbuje trzymać reżim wojskowy. Udaje się jakoś.
* Ludzie którzy NIE chcieli z nim zostać trafili często do Pawilończyka.
* Kapitan Pawilończyk wierzy w misję Lodowca - nie w "noktianie są źli" a w "trzeba ratować ludzi i Orbiter"
* Pawilończyk jest trochę "I CAN FIX HIM" wobec Lodowca. Dlatego wziął noktiankę...

Sylwia chciała jeszcze upewnić się, że będzie mieć układy tu, na Geisterjagerze.

Tp +3:

* V: masz kontakty na Geisterjagerze, wśród ludzi, oni wiedzą, że tam jedziesz. Laska od materiałów szkoleniowych.
* V: jesteś nieIdolką, wiedzą że mogą to wykorzystać i przez to że wiedzą to Ty możesz ich wykorzystać.

Tezremont. Ich nowa korweta.

Fakt - Zespół przebywa promem z Geisterjager. To oznacza, że wszystko powinno być w porządku. Nadal jednak zespół nie spodziewał się, że kapitan przywita ich osobiście w towarzystwie zaledwie 2 lekko rozkojarzonych żołnierzy. 

Kapitan Pawilończyk przywitał ich z ogromnym entuzjazmem i radością, a jego oczy od razu zostały przyciągnięte przez biust Annabelle. Kapitan bardzo próbował udawać, że nie patrzy, a Annabelle bardzo próbowała udawać, że nie zauważa. 

Pawilończyk zaproponował obiad - lepszej klasy jedzenie niż zwykle. Gdy Lucjusz zauważył, że chyba są jakieś problemy z zasobami, kapitan powiedział, że jest to okazja, którą trzeba świętować. On jednak nie będzie mógł zjeść z nimi - on jest kapitanem i musi się oszczędzać. Musi dać dobry przykład załodze. 

Annabelle zrobiła słodkie oczka, typu "Będę się sama czuła źle jak pan nie zje". Oczywiście, kapitan nie był w stanie odeprzeć słodkich oczu Annabelle. 

W czym jeden z żołnierzy zorientował się mniej więcej co się dzieje. Oddalił się na stronę na moment i poprosił przez komunikator kogoś o pojawienie się. I pojawił się - kapral Truśnicki, dla odmiany weteran. Truśnicki poprosił kapitana, by ten poszedł z nim, bo pojawiła się drobna komplikacja wymagająca ingerencji kapitana. Konkretny błąd w korwecie. 

Annabelle nie pozwoli, by zdobycz wymknęła jej się z ręki. Słodko podniosła, że to na pewno nie będzie ten błąd (zmyślony), ale podobne objawy dają błędy TEN TEN i TEN (prawdziwe). Truśnicki spoważniał. To jest zastraszanie.

Tr Z +3:

* V: Marine spojrzał poważnie. On WIE że ma do czynienia z osobą groźną. On zwiększy ochronę na statku. Lekko się przestraszył i oddalił.

Kapral Truśnicki przeprosił. Jest coś czym on musi się zająć, wyraźnie nie docenił powagi tego błędu. Zwiększy ochronę na statku. (to jest na wpół ostrzeżenie i na wpół groźba). Oddalił się szybko, a Zespół poszedł jeść do mesy. Tam są praktycznie wszyscy - ale nie ma Pierwszego Oficera, nie ma Kaprala Truśnickiego, nie ma też noktianki. Za to ludzie skupieni są dookoła potężnie zbudowanego mięśniaka (Warząkiewicza). Do momentu, aż weszła Annabelle.

Annabelle przysłuchuje się z zainteresowaniem, jak Warząkiewicz opowiada o noktianach. Jego opowieści są ponure i pełne krwi. Wyraźnie dogadałby się z komodorem. I - co bardzo ważne - ludzie są wsłuchani w jego słowa. Annabelle wintegrowuje się w załogę, starając się, by jak najchętniej z nią współpracowali. 

Tymczasem Lucjusz znajduje jedną osobę nie zainteresowaną Annabelle. To drugi oficer, artylerzysta oraz inżynier w jednym. Grzegorz Dawierzyc. Lucjusz wypytuje go o statek, o noktiankę, o kapitana. Co tu się dzieje? Lucjusz pozycjonuje się jako doświadczony wyga, który wiele już widział i ogólnie chce pomóc. A Dawierzyc LUBI narzekać. 

Dawierzyc opowiedział Lucjuszowi, że kapitan jest trochę jak maskotka - wszyscy go lubią, nikt nie da zrobić mu krzywdy i jakkolwiek kapitan może nie jest najlepszy, ale korweta wystarczająco dobrze działa. Dawierzyc uważa, że największą słabością kapitana jest jego dobre serce. Po prostu przyszła jakaś noktianka i poprosiła o dołączenie jej do załogi i kapitan się na to zgodził - wbrew obiekcjom większości zespołu. 

Sylwia korzysta z okazji i szuka savaranki - nigdzie jej nie ma, co więc robi? Czemu jej nie ma?

Tr Z +3:

* V: Znalazłaś savarankę.

Savaranka znajduje się gdzieś w odizolowanej części statku. Sylwia przyłapała ją, jak savaranka coś namiętnie czyściła. Jedno trzeba oddać - ta savaranka za żadne skarby nie spełnia definicji piękności. Dość chuda, mały biust, czysty i zadbany, ale używany mundur szeregowca Orbitera. 

Gdy tylko savaranka zobaczyła Sylwię, zastygła. Nie ze strachu, że robi coś źle - nie wie, co ma robić, więc oszczędza energię. Acz Sylwia dostrzegła, że element strachu wystąpił na początku komunikacji. Czyli ona się boi. 

Savaranka, wypytana przez Sylwię, powiedziała, że jest tutaj, bo tu jest najbezpieczniej. Nie powiedziała tego wprost, ale savaranka uważa wszystkie te byty w Libracji Lirańskiej za bardzo niebezpieczne, choć nie powiedziała czemu. Czyli noktianka uznała Orbiter za najbezpieczniejsze miejsce w którym może się znaleźć.  

Sylwia nie wykryła złych intencji u savaranki, ale dalej nie rozumie dlaczego ona tu jest.

## Streszczenie

Annabelle, Sylwia i Lucjusz trafili pod komodora Lodowca, który ich oddelegował na Tezremont. Lodowiec chce, by pomogli młodemu kapitanowi z wyjściem na ludzi i rozwiązaniu problemu 'groźnej noktianki'. Okazuje się, że Lodowiec ma dwie jednostki o różnych kulturach i mało zasobów. Zespół skupia się na zrozumieniu sytuacji na obu statkach.

## Progresja

* Annabelle Magnolia: Nawiązała relacje z Dariuszem Mordkotem i zrobiła na nim research; ma +Z w kontaktach z Mordkotem.
* Dariusz Mordkot: zainteresował się Annabelle; Annabelle ma +Z w kontaktach z nim.
* Renata Kaiser: dużo bardziej sceptyczna wobec Zespołu i uważnie ich obserwuje, podejrzewając ukryte intencje.
* Czesław Truśnicki: bardzo nieufny wobec Annabelle; uważa ją za bardzo niebezpieczną. Będzie ją monitorował a może nawet ją sabotował.

## Zasługi

* Sylwia Mazur: upewnia się, że nic nie przeoczyła; zdobywa kontakty na Geisterjagerze oraz znalazła ukrytą noktiankę na Tezremoncie, by potem z nią porozmawiać, próbując ją zrozumieć.
* Annabelle Magnolia: urocza i manipulatywna; flirtowała z Mordkotem (którego researchowała wcześniej). Wykorzystała swój urok przeciw kpt Pawilończykowi i nie pozwoliła marine odciągnąć go od niej.
* Lucjusz Jastrzębiec: zauważył braki komodora Lodowca w polityce i zdobył od niego więcej informacji niż ten zamierzał ujawnić. Wykorzystał rozmowę z Dawierzycem, by zrozumieć stan Tezremont.
* Gabriel Lodowiec: bardzo pragmatyczny; pomoże planetoidzie LLD-44, ale nie pozwoli na rany swoich ludzi. Nieco nieżyciowy; nie pozwoli na fraternizowanie się załogi. Z braku innych opcji przydzielił Zespół do OO Tezremont i ujawnił im więcej informacji niż zamierzał. Uważa noktiankę za potencjalne zagrożenie i próbuje odzyskać kontrolę nad Tezremont. Nadal uprzedzony do noktian.
* Ofelia Miris: oficer operacyjna Geisterjager, przeprowadziła operację ataku na planetoidę LLD-44 celem uratowania górników i pojmania górników anomalnych. Sukces, pojmała wszystkich; jeden jej człowiek został ranny.
* Dariusz Mordkot: z przyjemnością odprowadził Zespół i poopowiadał Annabelle o sytuacji, nie ukrywając nią zainteresowania.
* Renata Kaiser: piekielnie opiekuńcza wobec Lodowca; przywitała Zespół i zrobiła haję o brak jednostki wsparcia, po czym jej zadowolenie z Zespołu przerodziło się w strach, gdy zobaczyła działania Annabelle i to, że Annabelle może przejąć kontrolę nad Tezremontem.
* Arnold Pawilończyk: młody i entuzjastyczny kapitan Tezremont; uległ urokowi Annabelle. Zrobił lepszy posiłek na cześć przybycia Zespołu, ale sam nie chce jeść, bo się nie godzi. Ma dobre intencje, ale brak mu doświadczenia w zarządzaniu załogą. 
* Natalia Luxentis: savaranka; bardzo lubi utrzymywać czystość, działa sama. Lekko zalękniona. Powiedziała Sylwii, że jest tu, bo jest tu najbezpieczniej.
* Sebastian Warząkiewicz: snuje straszne opowieści o noktianach, podsycając do nich niechęć; ma silne wpływy wśród załogi.
* Czesław Truśnicki: gdy tylko usłyszał o problemach z Annabelle, natychmiast się pojawił by odciągnąć kapitana. Gdy Annabelle mu pokazała, że jest niebezpieczna, oddalił się planując jak to rozwiązać.
* Grzegorz Dawierzyc: bardzo lubiący narzekać drugi oficer; powiedział Lucjuszowi kontekst Tezremont i pożalił się na kapitana oraz na noktiankę.
* OO Geisterjager: fregata dowódcza Lodowca; 30 marine, 60 załogantów. Dobrze uzbrojona, nadaje się do abordażu i zdolna do zadania dużych obrażeń.
* OO Tezremont: szybka korweta qship Pawilończyka; 10 marine, 20 załogantów. Większa niż zwykle korwety, ma zdolność udawania innej jednostki.

## Frakcje

* Orbiter Lirański: ma kilka baz z zapasami w Libracji Lirańskiej oraz kilka stacji detekcyjnych i bojowych. Ogólnie słaba obecność.
* Orbiter Grupa Wydzielona Mrówkojad Lirański: oddział Gabriela Lodowca; ma Fregatę Geisterjager oraz Korwetę Tezremont. Mało zapasów i kiepska pozycja polityczna. Brak jednostki wsparcia.

## Fakty Lokalizacji

* Planetoida Kabanek: nazwana tak od dobrobytu (relatywnego), średnicę około 3-4 km. Znajduje się tam stacja przeładunkowa, zrobiona w miejscu dawnych operacji górniczych.
* Planetoida Kabanek: Stacja jest jednocześnie portem, "motelem" oraz nie jest może najważniejszą lokalizacją w okolicy, ale bywa odwiedzana przez różne "ciekawe postacie".
* Planetoida Kabanek: Miejsce, gdzie można pozyskać sprzęt, pohandlować, spotkać odpowiednie osoby, naprawić coś czy załadować zapasy i żywność. Dość bezpieczne.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Libracja Lirańska
                1. Planetoida LLD-44: Część górników 'oszalała', zaczęli losowo kopać w różne miejsca. Geisterjager ich uratował. 

## Czas

* Opóźnienie: -411
* Dni: 1

## Inne

.