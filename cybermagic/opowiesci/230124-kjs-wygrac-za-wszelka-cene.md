## Metadane

* title: "KJS - Wygrać za wszelką cenę!"
* threads: przeznaczenie-eleny-verlen
* motives: aurum-wasn-wewnatrzrodowa, szkolenie-dzieciaka, tien-na-wlosciach, magowie-ignoruja-obowiazki, echa-inwazji-noctis
* gm: żółw
* players: kić, anadia, kamilinux

## Kontynuacja
### Kampanijna

* [230124 - KJS - Wygrać za wszelką cenę](230124-kjs-wygrac-za-wszelka-cene)

### Chronologiczna

* [230124 - KJS - Wygrać za wszelką cenę](230124-kjs-wygrac-za-wszelka-cene)

## Plan sesji
### Theme & Vision

* Wygrać za wszelką cenę! + Lone, too young wolf
    * Franka Potente "Believe" & Musica "Flower of Life"
    * Stawać się lepszym (trening, ćwiczenia) CZY wygrywać (Szeptomandra)?
    * Uczyć się na swoich błędach CZY wygrywać? (Elena nie chce współpracować z Apollo VS Elena i Apollo rozmontowują)
* Dylematy
    * Czy skupić się na samodzielnym pokonaniu Szeptomandry (więcej esencji) czy na obronie ludzi?
    * Czy narazić życie młodych ludzi czy zdobyć więcej esencji?    (Karol: "to są magowie, nie warto im pomagać - ludziom tak.")
    * Wykorzystać dziecko (Elenę) czy współpracować z nią?
* DARK FUTURE
    * Szeptomandra ucieknie LUB jej destrukcja spowoduje kaskadowy szok po młodych ludziach dookoła.
    * Elena wejdzie w walkę z Szeptomandrą i nie jest w stanie wygrać. Szepty Esuriit.
    * Zaczną się krwawe walki dookoła

### Co się stanie (what will happen)

.
* S00: SETUP: 
    * Pojedynek gladiatorów na arenie. Serena zawsze wygrywa. Ale teraz przegra z Martą. Widzi w tle Emilię która patrzy i kręci głową.
* S01: 
    * Wyjaśnienie przez Karola, że skończyła się energia. Muszą zdobyć mimo obecności maga niedaleko. Mają dossier Apollo, który powinien tam być.
    * Opis lokacji: Miasteczko, Arena | skały i okolice. Gdzieś są źródła esencji którymi da się zasilić Kajis. Można je wygrać.
    * Wpakowali się na walkę z wężotworem; dwóch chłopaków próbuje walczyć z potworem. Poranieni. MOŻNA UŻYĆ. Chodzi o olśnienie Apollo Verlena.
* S02: 
    * "Tępy Verlen, my krwawimy a on z łóżka nie wyjdzie". Starcie z Eleną. Bez magii. Elena nie wygra (Ex).
* S0N: 
    * Wycieczka w Skały, Elena też idzie. Sama. Walczyć z Szeptomandrą.
    * Szept Esuriit. Elena nie może uratować wszystkich jak nie jest mocniejsza

### Sukces graczy (when you win)

* devour Szeptomandra.

## Sesja - analiza

### Fiszki
#### 1. Fiszki Kajis

* Karol Atenuatia: ???: "???" (szary prochowiec, siwiejący brunet, oddalone oczy, uszkodzona proteza nogi)
    * (ENCAO:  +-+00 |Płomienny;;Żarliwy| VALS: Family, Security >> Self-direction| DRIVE: Noctian Legacy)
        * core wound: Nie możemy wszystkich uratować. Przegraliśmy. Nie mam szans, mam tylko walkę.
        * core lie: Walka by być wolnym i uratować kogo się da jest ostatnim co mogę zrobić.
    * styl: cichy, z boku AŻ eksploduje cytatem z Księgi Wiecznej Wojny.
    * "Wolność wymaga walki i poświęcenia."
    * SPEC: miragent, typ: infiltrator

#### Fiszki Koszarowa Chłopięcego

* Apollo Verlen: mag, sybrianin: "żyje się raz, piękne panny!" (UWIELBIANY przez żołnierzy i ludzi)
    * (ENCAO:  ++-00 |Mało punktualny.;;Beztroski i swobodny| VALS: Face, Hedonism >> Security | DRIVE: Uwielbienie Tłumów)
        * core wound: never good enough. Nie jest tak dobry jak mógłby być. Niezaakceptowany przez swoje słabości. Nie dość dobry jako Verlen.
        * core lie: jeśli nie zrobisz wszystkiego pókiś młody, nigdy niczego nie zrobisz! Czas płynie i tracisz życie!
    * styl: głośny, płomienny, wszechobecny - i Johnny Bravo ;-)
    * "żyjemy raz, więc żyjmy pełnią życia, razem!!!" - dArtagnan "we're gonna be drinking"
    * SPEC: dowodzący oficer, morale, walka bezpośrednia
* Elena Verlen: czarodziejka, sybrianka: "you must be the best there is"

Uczniowie:

* Serena Krissit
    * (ENCAO:  --0-+ |Nie kłania się nikomu;;Prowokacyjna;;Skryta| VALS: Achievement, Security| DRIVE: Pokój i bezpieczeństwo)
    * styl: pracowita, chce pokazać
    * "Wojna się nie skończy tylko dlatego że nie ma noktian. Prawdziwy wróg jest _tam_."
* Marta Krissit: <-- WHISPERED
    * (ENCAO:  +---0 |Narcyz;;Nie ma blokad| VALS: Family, Stimulation, Achievement| DRIVE: Lepsza od siostry; przejąć kapitanat)
    * styl: aggresive, shouty
    * "Jestem lepsza od Ciebie i od każdego!"
* Adam Praszcz
    * (ENCAO:  +0-0- |Pretensjonalny;;Zapominalski;;Żywy wulkan| VALS: Hedonism, Tradition >> Humility| DRIVE: Odbudować reputację najlepszego)
    * styl: nerwowy, gubi myśli, świetny fizycznie, PRACOWITY
    * "Nie rozumiem co się dzieje, ale widać nie pracuję dość mocno"
* Franciszek Korel: <-- WHISPERED
    * (ENCAO:  -0+00 |Nie znosi być w centrum uwagi;;Cierpliwy| VALS: Universalism, Self-direction >> Face| DRIVE: Niestabilny geniusz Suspirii)
    * styl: Jurek
    * "Zwyciężymy, nieważne z czym mamy do czynienia. To tylko kwestia tego ile zapłacisz."
* Wojciech Namczak: (ten od głupich Verlenów)
    * (ENCAO:  000-+ |Sarkastyczny i złośliwy;;Łatwo wywrzeć na nim wrażenie| VALS: Stimulation, Conformity >> Humility| DRIVE: Mam rację.)

INNI:

* Jakub Klardat: dowódca defensyw bazy 
    * (ENCAO:  +--0- |Niestały i zmienny;;Nie przejmuje się niczym;;Apodyktyczny| VALS: Hedonism, Humility| DRIVE: Unikalne przeżycia)
    * chce, by Apollo zapłacił za swoją niekompetencję. Wie, że coś jest nie tak, ale Apollo powinien się tym zająć.
* Emilia Lawendowiec: aktualna czempionka
    * (ENCAO:  -0-0+ |Mało punktualna.;;Chętna do eksperymentowania| VALS: Achievement, Conformity| DRIVE: Queen Bee)
    * chce być najlepsza i dlatego bierze prywatne lekcje od Apollo. Płaci za to ciałem.
* Zuzanna Kraczamin: medical + monsterform
    * (ENCAO:  -00-+ |jest w niej coś dziwnego;;Kontemplacyjna| VALS: Power, Universalism| DRIVE: W poszukiwaniu najlepszego potwora)
    * nie jest stąd; z przyjemnością robi nieetyczne stwory
* .Bartosz 
    * (ENCAO:  --00+ |Unika pokus ciała;;Surowy, wymagający| VALS: Self-direction, Face| DRIVE: Ekstremista)
* .Aleks 
    * (ENCAO:  00+-0 |Anarchistyczny;;Perfekcyjnie wszystko planuje| VALS: Power, Hedonism >> Tradition| DRIVE: Ważny sojusznik)

### Scena Zero - impl

* Kamil -> Lucas
* Kić -> Lidia
    * Któryś z Sowińskich był nachalny. Dostał w mordę. Kajis ją wyciągnął.
* Anadia -> Żaneta
    * Problem z systemem feudalnym i magią.

Problem z energią. Trzeba pozyskać. Są problemy. I Kajis się musi ukrywać.

Dwa dni wcześniej. Serena jest atletką. Masz 17 lat. Uzbrojona i po drugiej stronie jest Twoja siostra, Marta. Serena chce jak najlepiej pokazać się łowcom talentów, nie tylko pokonać Martę.

Ex+3: 

* XXX: Marta była SZYBSZA niż powinna, lepsza. Nie tylko przedostała się szybko. Wygrała nie po prostu - wygrała DOMINUJĄCO.

Serena patrzy na Martę z zaciekawieniem, nie złością. "Czegoś się naćpała?". Marta jest agresywna, ale nie w taki sposób. Jest zbyt pod wpływem czegoś. Serena idzie pod logiką "to nie ma sensu", przechodzi po starciach, jak bardzo je przegrywała, trendy spadkowe a nagle taki skok?

Tr Z (logika działa) +4 +3Or:

* Or: Serena jest na ziemi; krwawi. "Tylko cieniasy uciekają się do czegoś takiego". Chce ją wkurzyć.
* Vr: Marta straciła kontrolę. Odpowiedź na potem...

### Sesja Właściwa - impl

Kajis wylądował niedaleko Skał Gubernatora. Karol wykrył jednego maga i lokalizację czegoś anomalnego w Skałkach.

Tr Z +1:

* X: Nie da się zdobyć pełnych informacji.
* X: Próby przechwycenia info przez Kajis zostały wykryte.
* V: Karol doszedł do czegoś zabawnego:
    * Apollo ma ze sobą nastoletnią (14-16) kuzynkę. Też maga.
    * Apollo biega za spódniczkami i nie zajmuje się zarządzaniem. Ani kuzynką. 28 lat.
* X: Verlenland wiedzą, że ktoś próbował się wbić i nie wyszło. "hahaha". Czyli pomniejsze słowo pogardy.
* Vz: Dzięki świetnym detektorom i mechanizmom włamu Kajisa mamy:
    * Verlenowie nie są fanami dopingu. Ani Apollo ani "ta mała".
    * Verlenowie WIEDZĄ że coś jest nie tak bo był pojedynek Sereny i Marty, sióstr Krissit. I tam się tematem zainteresował Apollo. Przez moment
    * Plotka głosi, że Apollo daje prywatne korepetycje czampionce.
    * "Ta mała" się zainteresowała tematem bardziej niż Apollo.
    * Co grozi za dopalacze? W sumie - dyskwalifikacja i ciężki opierdol. Ewentualnie nawet więzienie. Zależy.

Lucas, idziesz po mieście i szukasz potencjalnej przestrzeni gdzie oczekiwałbyś czarodziejki. Spytał lokalnych dziewczyn bo ma wiadomość. Zachichotały "wiadomość" i skierowały go do Krwawego Topora (bar). 

Tam Elena w konflikcie z jakimś kolesiem, który obraził Apolla że on z łóżka nie wychodzi.

Ex+3:

* Vr: Elena łokciem w nerkę korzystając 
* Or: Elena złamała nos używając czoła
* Og: koleś odrzucił Elenę prosto w krzesła i stoły
* Og: kopniak prosto w żebra pancernymi butami. Czarodziejce skończyła się ochota do walki.
* (+5Ob) Ob: Elena straciła kontrolę, mogła poddać a poraniła go ciężko.

Lucas podchodzi do Eleny i mówi, że czas wyjść, chodzi o śledztwo. Elena "ja decyduję czy to ważne". Lucas przekonał Elenę by poszła z nim, jeśli to ważne.

TYMCZASEM: Żaneta idzie na kort sportowy znaleźć Martę lub Serenę. Serena tam jest, ćwiczy ostro. Ma coś z nosem. Żaneta zagadała do Sereny. Jest fanką. Zmartwiła się, że Marta coś bardzo nie tak.

Serena "nie mówmy o tym, to w końcu siostra. A pamiętasz jak walczyłam z duszącym wężem? 18 sekund przetrwałam". Żaneta "wężem, dziewczyną, nauczyłabym się...". Serena "chcesz sparing? Jestem słabsza niż zwykle..." Żaneta "silniejsza, chciałabym się nauczyć." Serena "oook" (troszkę niepewna, nie przywykła do fana).

Żaneta chce się zakumplować przez bitkę. Zna plotki, chciałaby uściślić. Jest na poziomie walki regularnej, walczy jak żołnierz.

Tr Z (przewaga doświadczenia) +4 (CEL: zakumplować a nie wygrać):

* V: Serena jest zadowolona z walki z Żanetą
* V: Serena POLUBIŁA Żanetę; trik lub dwa zobaczyła ale sama też coś pokazała
* V: Serena zapewniła, że Serena się nie zorientowała, że Żaneta nie jest stąd
* (Serena potem będzie wyśmiana że się nie zorientowała)

Serena powiedziała Żanecie co wie o narkotykach:

* Marta nie brała narkotyków, ona chce wygrywać ale by nie wzięła.
* Były testy chemiczne - nic nie wykazały. Po walce. To naturalny doping, jej... ciało w pełni skupiło się na celu. Nie wiem, adrenalina czy coś. 
    * Nasza medyczka była szczęśliwa... (czyli coś jest nie tak) /NASZA MEDYCZKA JEST DZIWNA
    * Ale ta czarodziejka, ona wie o co chodzi, ona coś wyczuła. A jej kuzyn nie! (zdziwienie). DZIECKO WYCZUŁO A DOROSŁY MAG NIE!
    * To był pierwszy raz Marty. Wakacje, spa? Nie... nie wie nic o tym. Nie spodziewała się.
    * Serena nie wie nic o tym by Marta z kimś spała

Tr Z (zaufanie) +3:

* Vz: Serena wyznała w tajemnicy, że PODOBNO Marta zaczęła kręcić z Franciszkiem. Ale to niemożliwe bo Marta ma charakter a Franciszek nie ma charakteru. Zero. Koleś celofan.

TYMCZASEM Lidia poszła do medyczki (Zuzanny). 

Bez kłopotu Lidia trafiła. Szpital w Koszarowie będzie spory. Głowa medycyny to Zuzanna Kraczamin. Ale ma sporo magitechu. I już na pierwszy rzut oka - Zuzanna jest drakolitką. To ciekawe samo w sobie.

* Lidia stara się nie kłamać: "pomagam tien Verlen w przeprowadzeniu dochodzenia odnośnie tego pojedynku. Moim zadaniem jest dowiedzieć się od pani co się stało medycznie."
* Zuza: "tien Apollo Verlen czy tien Elena Verlen? Apollo powinien być zajęty a Elena, cóż, nie wpadnie na to."
* Lidia (z lekkim uśmiechem): "Ktoś jej próbuje podpowiedzieć"
* Zuza (uśmiech): "Centrala chce, by to było zrobione dobrze?"
* Lidia wzrusza ramionami.
* Zuza: Słucham zatem. Czego potrzebujesz?
* Zuza: Pełna rekonfiguracja hormonalna.
* Lidia: Ktoś wyregulował na tip top?
* Zuza: Tak, co więcej - ktoś bazował na jej naturalnych hormonach. Podejrzewam jakieś biolab.
* Zuza: tien Elena Verlen to zauważyła. I nie wiem jak. Czekam na nowe rekordy - porównam z próbkami.

TYMCZASEM LUCAS:

* E: "Coś w zamian? A zrobienie czegoś DOBREGO dla ludzi to nie jest coś w zamian?!"
* L: "Jak dorośniesz to zrozumiesz inny punkt widzenia."
* E: "JAK DOROSNĘ?!?!?!"
* ...
* E: Po co Ci bateria? Co chcesz wysadzić?
* L: Utknęliśmy tutaj.
* E: Jeździsz od miasta do miasta i robisz sztuczki.
* E: Daję Ci słowo, że jeżeli osiągnę sukces między innymi dzięki Twojej wiedzy, o kuglarzu, to dostarczę Ci baterię. Pasuje?
* L: To będzie Twój sukces.
* L: Idziemy naprzód ze śledztwem. Spotkanie w czwórkę.

Elena, Lidia, Żaneta i Lucas.

Elena wyzwała Lucasa na pojedynek. Lucas odmawia pojedynku. Elena powiedziała, że ma prawo. Lucas daje cenniejsze rzeczy dziewczynom i idzie się bić z poturbowaną pietnastolatką.

Elena: "to sprawa honorowa, nie mogę nie walczyć."

Zaczyna się pojedynek, 1 sekunda, Elena w pozycji bojowej, Lucas "poddaję się" i pada na ziemię. Elena patrzy z takim niedowierzaniem. I "no.". Lucas łapie się za brzuch i krzyczy "medyk!!!".

Elena patrzy na Was. Odwraca się i odchodzi. Lucas -> dziewczyny "sami go złapiemy". Ona nie wraca. Wie o medyczce.

## Streszczenie

Koszarów Chłopięcy jest miejscem, gdzie młodzi z Verlenlandu ćwiczą i rozwijają cnoty sportowe. Pojawiły się tam nietypowe narkotyki / środki, które nie są pochodzenia stricte chemicznego. Zespół Kajisa ma minimalny poziom energii i musi awaryjnie tam lądować by jak najszybciej uzyskać baterie irianium lub esencji. Na miejscu Lucas wdał się w pyskówkę z nastoletnią Eleną Verlen - nie pomoże jej rozwiązać problemu, bo nie współpracuje z gówniarami.

## Progresja

* .

## Zasługi

* Lucas Oktromin: traktuje Elenę jak małą gówniarę, zero szacunku, nawet jak ona się stara. Odmówił jej pojedynku i współpracy, acz zapewnił z jej strony pomoc w zdobyciu baterii do Kajisa.
* Lidia Nemert: unika kłamania; kiedyś Sowiński coś jej chciał zrobić i dostał w mordę (stąd jest na Kajis). Współpracując z Zuzanną (lekarzem) zdobyła info na temat natury dziwnych narkotyków.
* Żaneta Krawędź: strasznie młodo wygląda; zaprzyjaźniła się z Sereną i zdobyła od niej informacje o życiu Marty i potencjalnym wskazaniu na dziwną formę 'naturalnych' dopalaczy. Nie znosi magów Aurum.
* Karol Atenuatia: właściciel Kajis; próbował dowiedzieć się coś o lokalizacji w której się znajdują, ale wie jedynie o Elenie i tym że w Koszarowie Chłopięcym jest "coś" z dopingiem.
* Elena Verlen: 15 lat. Pomalowana na emo. W drobnym mundurze, mającym do niej pasować, z odznakami za wytrwałość i umiejętność pilotowania. Broni honoru Verlenów bijąc się z Wojtkiem (19-latkiem), ale nie ma siły fizycznej mimo techniki; wygrała przez automatyczne użycie magii. Chciała pomocy Lucasa i Zespołu by odkryć co jest dopalaczem w okolicy, ale potraktowali ją jak dziecko. Nie dała się ponieść emocjom, odeszła. Sama rozwiąże problem mimo nich.
* Apollo Verlen: młody kobieciarz (28). Jak tylko pojawił się na terenie Koszarowa, zaczął korzystać ze swojego statusu i umiejętności i dobrze planować czas. Acz przy okazji FAKTYCZNIE podnosi umiejętności Emilii specjalnymi treningami.
* Emilia Lawendowiec: aktualna championka Koszarowa Chłopięcego; uczestniczy w specjalnych treningach Apollo Verlena. 
* Serena Krissit: starsza siostra, bardzo ćwiczy i chce być jak najlepsza. Zaskakująco przegrała z młodszą Martą (jakaś forma dopingu?). Zaprzyjaźniła się z Żanetą i powiedziała jej co wie na temat łóżkowych spraw Marty, wskazując na Franciszka.
* Marta Krissit: młodsza siostra, wzmocniła się amplifikatorem i pokonała starszą od siebie Serenę. W zamian za to oddała się Franciszkowi.
* Zuzanna Kraczamin: lekarz Koszarowa Chłopięcego (ENCAO:  -00-+ |jest w niej coś dziwnego;;Kontemplacyjna| VALS: Power, Universalism| DRIVE: W poszukiwaniu najlepszego potwora). Odkryła, że "doping" polega na pełnej rekonfiguracji hormonalnej. Chce pomóc to rozwiązać, ale nie wie z kim współpracować.
* Wojciech Namczak: agresywny i nabuzowany chłopak podkochujący się w Emilii; zabolało go, że Apollo udziela jej "prywatnych lekcji" więc powiedział to na głos i wdał się w bitwę z Eleną. Bez magii. Mimo, że Elena ostro walczyła, ale w końcu straciła kontrolę i skrzywdziła go magią. Skończył w szpitalu na 2 tygodnie.
* KDN Kajis: kończy się energia, więc Kajis ląduje w Verlenlandzie i maskuje się jako skała by ludzie na pokładzie zdobyli dlań jakąś energię.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. Koszarów Chłopięcy: miasteczko "koszarowe", gdzie dzieciaki ćwiczą sport i starają się być jak najlepsze. Młodzi ludzie i dużo podstaw do ćwiczeń.
                                1. Miasteczko: miejsce, gdzie młodzi śpią, handlują i mogą się bawić. Ogrodzone murami - by nikt nie uciekł XD.
                                    1. Bar Krwawy Topór: ulubiony bar młodzieży, gdzie Elena biła się z Wojtkiem (i użyła magii intuicyjnie, niechcący).
                                1. Wielki Stadion: główna arena i miejsce gdzie dzieciaki rywalizują ze sobą.


## Czas

* Chronologia: Zmiażdżenie Inwazji Noctis
* Opóźnienie: 4737
* Dni: 2
