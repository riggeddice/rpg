## Metadane

* title: "Polowanie na biosynty na Szernief"
* threads: agencja-lux-umbrarum, problemy-con-szernief
* motives: dziwna-zmiana-zachowania, energia-unumens, pakt-z-diablem, ratownicy-kontra-ratowani, kult-mroczny, faceci-w-czerni, lojalnosc-syntetycznych-intelektow, czystka-etniczna
* gm: żółw
* players: werka, ukasz, zorba, magda_zaba, piotrek

## Kontynuacja
### Kampanijna

* [231119 - Tajemnicze tunele Sebirialis](231119-tajemnicze-tunele-sebirialis)

### Chronologiczna

* [231119 - Tajemnicze tunele Sebirialis](231119-tajemnicze-tunele-sebirialis)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA: 
    * Melodicka Bros "Every Breath You Take" (Police cover): https://www.youtube.com/watch?v=qeoUMoSEFqU
        * "Oh can't you see, you belong to me"
        * Unumens, unifikacja w jedności. PARASEKT (przeciwnik) chce założyć Rój na stacji i doprowadzić ją do perfekcyjnego działania.
* Opowieść o (Theme and vision): 
    * "'Możliwość wyprowadzenia stacji na lepsze' versus 'Tu jest coś złego i należy to usunąć'. Nie zawsze ZGODNOŚĆ i JEDNOŚĆ jest tym co jest poprawne."
        * Inwestor Klaudiusz Widar skupił się na zwiększonej efektywności i zaczynają się możliwości inwestycyjne. Będzie walczył z Agencją.
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * dziwna-zmiana-zachowania: Malik nie działa w pełni jako agent Agencji (potencjalny przeciwnik); co więcej, skąd to zwalczanie NIEKTÓRYCH drakolitów i biosyntów?
    * energia-unumens: Parasekt próbuje zagnieździć się w okolicach nieużywanego LifeSupport i dołączyć wszystkich do Jedności / Kultu Parasekta.
    * pakt-z-diablem: Mimo, że nikt nie wie o magii, widać różnicę w statystykach. Rada jest zwolennikami zachowania status quo, bo NIC ZŁEGO SIĘ NIE DZIEJE.
    * ratownicy-kontra-ratowani: stacja nie jest świadoma istnienia magii a Szernief ma problemy finansowe. Savaranie rozumieją 'poświęcenie'. Więc nikt nie rozumie CZEMU się pozbyć 'stwora'.
    * kult-mroczny: Kult Parasekta. Ludzie stopniowo stają się częścią ekosystemu Unumens, usprawniając stację na chwałę Jedności i Unifikacji. Radzie to pasuje.
    * faceci-w-czerni: rola Zespołu (Lux Umbrarum). Usunąć problem Unumens, stworzyć 'plausible deniability' i zapewnić by to się nie działo nadal.
    * lojalnosc-syntetycznych-intelektow: Sebastian, biosynt, chce jak najlepiej dla stacji i Kalisty MIMO, że to go naraża i są czystki etniczne przeciwko biosyntom. Też: Delgado wobec Sebastiana.
    * czystka-etniczna: Ludzie próbują usunąć biosynty pod wpływem Parasekta i tematom nadanym przez Parasekta.
* Detale
    * Crisis source: 
        * HEART: "sin (grzech): próba zarobienia na stację za wszelką cenę; skupianie się na swojej domenie i na dorobku i przez to wkradł się tu Parasekt Unumens"
        * VISIBLE: "Od miesiąca znikają ludzie (po przechwyceniu statku). Walka z syntami. Kalistę chroni Sebastian, 'drakolita' (biosynt). Nikt nic nie widzi."
    * Delta future
        * DARK FUTURE:
            * chain 1: CONTROL: Parasekt przejmie kontrolę nas stacją, podnosząc jej jakość i działania. Może współpracować z Radą, może skupić się na savaranach...
            * chain 2: EXECUTION: Synty (mała ich populacja) zostaną zniszczone.
            * chain 3: PRAWDA: wiedza o magii wyjdzie na jaw, konieczność aplikowania masowych amnestyków. Zniszczenie stacji?
        * LIGHT FUTURE: 
            * chain 1: 
            * chain 2: 
            * chain 3: Nikt jak o magii nie wiedział tak nie wie
    * Dilemma: brak

### Co się stało i co wiemy

* Kontekst stacji
    * CON Szernief została zaprojektowana jako stacja sprzedająca Irianium. Jest to miejsce o podniesionej ochronności elmag (lokalizacja: przy Sebirialis).
    * Okazuje się, że populacja która zainteresowała się tymi warunkami to byli Savaranie.
        * I inwestorzy są w klopsie. Dla savaran nadmiar energii to radość. Nie chcą kupować seks-androidów ani niczego. Pełna oszczędność.
            * Savaranie to NAJGORZEJ z perspektywy korporacji i inwestycji XD
        * Inwestorzy żądają zwrotu
    * Zarządcy CON Szernief szukają kogoś kto będzie tam chciał żyć i najlepiej za to jeszcze zapłaci
        * Stanęło na ściągnięciu Kultystów Adaptacji. Ale oni nie są jacyś super niebezpieczni, sami tak mówią ;-).

### Co się stanie (what will happen)

?

## Sesja - analiza

### Fiszki

CON Szernief:

* Janvir Krassus
    * OCEAN: (O+ C+) | "Przez współpracę i innowacje budujemy lepszą przyszłość." (Locke) | VALS: (Tradition, Security) | DRIVE: "Przekształcenie Szernief w zaawansowaną kolonię."
    * on jest odpowiedzialny za współpracę z NavirMed
* Larkus Talvinir
    * OCEAN: A-O- | "Żartuję nawet w złych sytuacjach; prowokuję i nie boję się konfliktów" | VALS: Stimulation, Universalism | DRIVE: "Zabawa kosztem innych to moja rozrywka"
    * główny inżynier w sztabie
* Marta Sarilit
    * OCEAN: (N+ O+) | "Jeśli nie będziemy dość szybcy, zostaniemy zniszczeni." | VALS: (Benevolence, Power) | DRIVE: "Naprawa i lecznie"
    * dowodzi więzieniem NavirMed; to jej operacja
* Felina Amatanir
    * OCEAN: (N- C+) | "Nieustraszona i niezależna, wyróżnia się determinacją i profesjonalizmem" | VALS: (Power, Security) | DRIVE: "Porządek i sprawiedliwość, ponad kontrakt"
    * drakolitka o zwiększonych parametrach; wywalili ją z zarządu i z dowodzenia ochroną po tej operacji
* Kalista Surilik
    * OCEAN: (A+E+) | "Odważna i zdecydowana, w gorącej siarce kąpana" | VALS: (Benevolence, Achievement) | DRIVE: "Prawda MUSI wyjść na jaw"
    * już z nią Agencja miała do czynienia...

#### Strony

.

### Obietnica / Zajawka

"Wasz agent był PIERWSZYM, któremu ONI to zrobili!"

W normalnej sytuacji, gdyby zwróciła się do was jakaś dziennikarka, to w ogóle nie bralibyście pod uwagę jej słów. Szczególnie, gdy wasz agent terenowy w Stacji Kosmicznej zdecydowanie im zaprzeczał. Kalista jednak była niezwykle skuteczna. „Szczególnie uciążliwa”, jak powiedziałby Wasz przełożony z Agencji Rządowej ds. eliminacji i ukrywania skutków anomalii magicznych, który już wielokrotnie musiał zostawać na nadgodzinach, by uniemożliwić Kaliście publikację prawdy na temat magii. Społeczeństwo nie może wiedzieć, że magia istnieje. 

A ta sama Kalista, która przysporzyła wam tyle problemów nalega, byście zbadali pozornie spokojną stację w poszukiwaniu... no właśnie, czego? Sama wścibska dziennikarka nie jest Wam w stanie powiedzieć wiele więcej, niż że ma to coś wspólnego z uciekinierami, więźniami i… androidami?

Zważywszy na to, że na tej Stacji już wcześniej były problemy z magią i przez wzgląd na słowa Kalisty zostaliście wysłani by dowiedzieć się, co się dzieje na stacji. I jeśli będzie trzeba, zatuszować tego skutki przed społeczeństwem.

Powodzenia, Agenci. 

### Scena Zero - impl

Ochrona, starcie z ludźmi bijącymi biosynta.

TrZ+3

* X: biosynt bardzo uszkodzony
* X: ostrzegawczy ALE NIE WYGLĄDA NA TO
* V: odstąpili
* V: "też synt"

Mapowanie:

* Werka: Oficer Naukowy
* Ukasz: Biurokrata
* Zorba: Dyplomata
* Magda: Sabotażysta
* Piotrek: Hacker

### Sesja Właściwa - impl

Zdalne spotkanie z Malikiem

Tr+3:

* V: rezygnacja, WIE że może być lepiej, współpracuje ofc. Był pierwszą osobą.
* X: nie pójdzie na Luminarius
* V: dane: tydzień potem w terenach niezamieszkałych
* V: co sobie przypomni (żądło)
* X: sabotażystka wysadza drzwi i Malik jest ranny plus "WTF?!"
* V: BUM! sukces
* V: evac

Malik @ Luminarius, badania

* X: bolesne i okrutne inwazyjne metody
* X: "to są biosynty", sabotaż
* X: grupki polują na Delgado
* V: vigilantes, roznoszą sensory dla Luminariusa oprócz łażenia i szukania biosyntów
* VV: Pasożyt. Mamy go.
* X: Malik jest w beznadziejnym stanie. Kilka miesięcy regeneracji.
* V: ...ale przetrwa
* V: WIĘC mamy prawdę. Biosynt został zabity pierwszy. Savaranin sfałszował dane. Mamy potencjalnie sporo problemów na stacji.

A biosynty gdzie?

Ex+3:

* V: Było 17, potem 9, zostało 7. Sebastian jest przykładem biosynta
    * -> zawsze możemy je znaleźć i zlokalizować

Może Felina da radę pomóc? Niech poroznoszą 'krewetkowe sensory magii' po stacji.

Tr +3:

* X: Wyglądają IDIOTYCZNIE i są duże i niezgrabne
* V: Wiecie GDZIE jest mapa energii magicznych na stacji, pobieżnie
* X: krewetkowy wandalizm. Ludzie niszczą te sensory
* X: Rada stoi przeciwko Agencji w tej kwestii; z perspektywy Rady nie dzieje się nic złego
* X: Felina ma bana ze strony Rady. Nie może Zespołowi pomóc.

Oficer Naukowy robi SZOK - atakując w Pasożyta antidotum, by zobaczyć zarażonych.

TrZ+3:

* V: Szok, sukces
* X: Parasekt odrzucił szkodzący mu link; drugi raz to nie zadziała
* V: WSZYSCY połączeni z Parasektem dostali wpiernicz (+2V do detekcji też dzięki sensorom krewetkowym)
* V: Mamy większość zlokalizowanych
    * -> 2 osoby z Teraquid, WIĘKSZOŚĆ ochrony bez Feliny
* V: Mamy zlokalizowanych WSZYSTKICH zarażonych
    * -> też Larkus i kilka ekstra ważnych osób.
    * potencjalnie centrum Skażenia to parki, lifesupport itp

Czas jakoś naprawić sytuację dla biosyntów by pozyskać ich wsparcie

TrZ+3:

* X: ktoś musi być właścicielem prawnym biosyntów
* VV: Artur będzie za nie odpowiedzialny i jest obowiązek za nie zadbania; ochrona MUSI je chronić
* (Dobra, mamy coś dla biosyntów. Pogadajmy z Sebastianem i niech da nam link do Delgado. (KONTYNUACJA konfliktu))
* X: Sebastian chroniąc Kalistę skrzywdził agresywnych Skażonych Savaran
* VV: Sebastian będzie gadał i po usłyszeniu wszystkiego dał warm link do Delgado

Sabotażystka idzie spotkać się z Delgado. Ten masakruje atakujących Savaran (niekoniecznie Skażonych)

TrZ+3

* X: Delgado zabija na oczach Sabotażystki savaranina i przykłada nóż do gardła kolejnego
    * Sabotażystka robi HEADSHOT tamtego by dostać zaufanie. TrZ+3 -> TpZ+3, +5 pkt niegodziwości
* V: Zaufanie ze strony Delgado
* X: Jeśli Agencja dostarczy sprzęt naprawczy/ładujący, Delgado pomoże.
* V: Delgado zgromadzi biosynty bojowe (on, Vanessa, Sebastian) i pomogą zniszczyć Parasekta

By móc przetestować antidotum na Parasekta i prowadzić dalsze badania, trzeba pozyskać człowieka. Sabotażystka porywa jednego.

Tr+3:

* X: 'amassed forces' Parasekta mające chronić Parasekta, savaranie, drakolici itp
* X: Kalista się dowiaduje o tym, że coś się dzieje i mniej więcej co (acz bez magii)
* VV: Sabotażystka pozyskuje człowieka dla Oficera Naukowego

Badamy człowieka, eksperymentując i budując antidotum, też używając magicznych reagentów.

Tr+3+3Ob:

* V: coś co utrzyma przy życiu nawet jak zginie pasożyt
* X: Kalista zrobi mistrzowski artykuł; Agencja ma kolejny problem
* Ob: 'Ewolucja' badanego człowieka, przekształca się w potwora
* X: Wstrzyknął Oficerowi Naukowemu pasożyty, ale MECHANIZM DEFENSYWNY AGENCJI (implant) zrobił heavy purge

Czas na masowe zaszczepienie zarażonej populacji. Pomoc Kalisty (o dziwo, dała się przekonać jak słyszy o groźnej chorobie).

ExZ+3:

* X: +psucie reputacji biosyntów bo w sumie 'to ich wina nie?'
* X: działania niepożądane więc Agencja skłamała
* X: GRZYB (cordyceps), choroba, fakty -> Kalista. (+2V)
    * -> ale przeniesienie 'kłamstwa' na Kalistę, więc ona została skompromitowana przez Agencję
* V: Masowe i skuteczne bolesne szczepienia

Sabotażystka odwraca uwagę od ruchów biosyntów by te mogły wejść i zniszczyć problem (Parasekta)

Tr+3:

* X: zniszczenie za blisko nierejestrowanej rury z tlenem; ogromny wybuch
* X: Kalista dostała rykoszetem; jej reputacja jest bardzo uszkodzona
* V: Biosynty zniszczą Parasekta i koszmar się skończy

DODATKOWO, ZMIANA:

* dewastacja reputacji Kalisty
* zniszczenia itp zrzucone na Mawira Honga
* Artur dostał potężny cios reputacyjny przez biosynty (on jest za nie odpowiedzialny oficjalnie)

(koniec; 17 -> 9 pkt niegodziwości, bo 8 poszło na przekierowanie do Mawira i Kalistę)

## Streszczenie

Kalista wezwała Agencję bo biosynty są niszczone (ludzie zabijają je pierwsi) i dzieje się 'coś dziwnego' a 'człowiek Agencji' jest niekompetentny - okazało się, że jest Skażony Unumens (ma pasożyta). Agencja zrzuciła 'legalność' biosyntów na arystokratę Artura, zapewnia sobie współpracę z biosyntami i kieruje je do zniszczenia Parasekta Unumens. By odwrócić uwagę od swoich działań - zdradza Kalistę i Mawira (i wszystkie problemy zrzuca na nich). Udało się jednak większość uratować, mimo śmierci kilku osób w tak trudnej sytuacji.

## Progresja

* Malik Darien: wymaga paru tygodni ciężkiej regeneracji i znowu może służyć Agencji.
* Kalista Surilik: przez działania Agencji dostała silny cios reputacyjny i przesunęła się bliżej Mawira Honga (który też dostał za niewinność). Ogromna nieufność wobec Agencji.

## Zasługi

* Klasa Oficer Naukowy: bada Malika i ekstraktuje z ciała Parasekta, składa 'detektory krewetkowe', robi _feedback shock_ by ujawnić wszystkie osoby zarażone i gdy próbuje zrobić szczepionkę na Parasekta, zostaje zarażony przez Skażeńca. Ale przetrwa to.
* Klasa Biurokrata: znajduje sposób by znaleźć prawne zabezpieczenie dla biosyntów (budowanie ich zaufanie), zrzucenie odpowiedzialności na Artura. Potem opracowanie planu masowego szczepienia i znalezienia podstawy prawnej. I na końcu - zrzucenie win z Agencji na Mawirowców i Kalistę.
* Klasa Dyplomata: przekonanie Kalisty do współpracy, przekonanie szerszej populacji że szczepienie to jedyne wyjście (presja na Parasektowców), zrzucenie win z Agencji na Mawirowców i Kalistę.
* Klasa Sabotażysta: wbija do pomieszczenia z Malikiem eksplozją i zatrzymuje go przed samobójstwem; gdy idzie porozmawiać z Delgado to zabija na jego oczach savaranina by zyskać zaufanie. Porywa człowieka by testować antidotum na Parasekta i odwraca uwagę od ruchów biosyntów by te mogły wejść i zniszczyć Parasekta.
* Klasa Hacker: zestawienie połączenia zdalnego z Malikiem, odkrycie danych z kamer: to savaranie atakowali biosynty pierwsi, śledzenie WSZYSTKICH ludzi by z szokiem dojść do tego kto jest zarażony, budowa fałszywych dowodów (że Agencja jest niewinna).
* Malik Darien: rezydent Agencji na Szernief, gdy doszło do intruzji więźniów (i Parasekta) na Szernief ruszył za nimi i został pierwszym Skażonym. Nieświadomy prowadził ludzi do Parasekta. Chciał się zabić gdy odzyskał kontrolę przesłuchiwany przez Hackera, ale skończył boleśnie na stole operacyjnym. Po paru tygodniach regeneracji będzie znowu sprawny.
* Felina Amatanir: próbuje pomóc Agencji, ale jest zablokowana przez Radę; część jej podwładnych (o czym nie wie) jest zainfekowana Parasektem. Nadal dała polecenie pomocy w roznoszeniu 'krewetkowych sensorów' po stacji i podpowiada Agencji tam gdzie jest w stanie.
* Kalista Surilik: wezwała Agencję bo coś jest bardzo nie tak; gdy już przyskrzyniła fakty (ale bez magii) Agencja poprosiła ją o współpracę przeciw chorobie pasożytniczej. Kalista na to poszła, ale były efekty uboczne i Agencja zrzuciła na nią problemy. Ucierpiała, przesunęła się bliżej Mawira Honga i dalej od Feliny.
* Delgado Vitriol: morderczy biosynt przeciwny ludziom i ludzkości; eksterminuje ludzi atakujących inne biosynty i dowodzi lokalnymi biosyntami. Próbował skrzywdzić Kalistę, ale Sebastian go zatrzymał. Gdy tylko Agencja pokazała że jest skłonna też zabić ludzi, zgodził się pomóc w zniszczeniu Parasekta - za system ładowania który mogą wykorzystać w głębi stacji Szernief.
* Sebastian-194: z ukrycia chroni Kalistę z sobie tylko znanych powodów. Gdy została zaatakowana przez Parasektowców, Sebastian ich brutalnie pobił. Gdy pojawiła się Agencja, po przekonaniu, dał im ciepły kontakt do Delgado.
* Vanessa d'Cavalis: uczestniczyła w operacji eliminacji Parasekta za prośbą Delgado. Nadal chroni Artura Tavita jako jego lojalna strażniczka (i pojawiają się plotki że nie tylko jako strażniczka).
* Artur Tavit: arystokrata na Szernief; walczy o prawa biosyntów i współpracuje z Vanessą d'Cavalis. Agenca dała mu 'własność' wszystkich biosyntów przez co nie wolno ich niszczyć, ale on płaci za wszystkie problemy z nimi reputacyjnie.

## Frakcje

* Con Szernief: pojawienie się Parasekta ze statku więziennego i infekcja części populacji sprawiła pojawienie się ruchów integracyjnych oraz pewnych aktów sabotażu. Po działaniach Agencji, Kalista i Mawirowcy są odepchnięci a ruchy integracyjne rosną w siłę. Nie mówiąc o pojawieniu się biosyntów, które też się gnieżdżą na stacji.
* Con Szernief Teraquid: Kompania (pluton) Teraquid przybyła tu 'na emeryturę' i wpakowała się w coś czego nie do końca rozumie. Mieli walczyć z biosyntami, nie wiedzą co się dzieje. Na razie patrolują bo to mają robić.
* Con Szernief Mawirowcy: coraz bardziej odrębni od reszty stacji, ostatni ruch Agencji (zrzucenie na nich winy za sabotaż) jedynie dokończył dzieła ich izolacji. Uważani za 'gangsterów' i nielubiani, izolują się. Kalista z nimi współpracuje.
* Con Szernief Querenci: frakcja uszkodzona; Kalista i Felina wchodzą w konflikt (bo Kalista 'kręci z Mawirowcami' i 'robi złe rzeczy'). Querenci są osłabieni, ale są dalej przekonani że coś jest nie tak.
* Con Szernief Justarianie: skupiają się przeciwko Radzie jeszcze bardziej, rośnie niezadowolenie i resentymenty (m.in z uwagi na biosynty, Teraquid itp. - wygląda jakby arystokraci kręcili z biosyntami?)
* Con Szernief Arystokraci: dostali rykoszetem za to, że 'Artur interesuje się biosyntami' a przedtem 'Aerina robiła złe rzeczy mając Vanessę'. Siedzą cicho na wszelki wypadek.
* Con Szernief Biosynty: 7 z 17 wolnych biosyntów (3 w konfiguracji bojowej (Vanessa, Delgado, Sebastian)) + arystokrata Artur Tavalis. Dostali od Agencji mechanizm ładowania się, pomogli w zniszczeniu Parasekta. Silne resentymenty przeciw ludziom na stacji.
* Con Szernief Unifikaci: frakcja się zawiązuje z części savaran i drakolitów współpracujących pod energią Unumens; mieszane patrole polują na biosynty (zwykle kończąc marnie). Zawiązują się przeciw biosyntom i Radzie.
* Con Szernief Rada: skonfliktowana. Jakkolwiek coś wyraźnie się dzieje, Rada nie ma nic przeciwko możliwości rozpoczęcia starć między drakolitami i savaranami by móc wprowadzić martial law (stąd Teraquid). Zneutralizowali Felinę, ale nie wiedzą w którą stronę iść. Larkus z Rady jest pod wpływem Parasekta.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Kalmantis
            1. Sebirialis, orbita
                1. CON Szernief
                    1. Powłoka Wewnętrzna
                        1. Poziom Minus Jeden
                            1. Obszar Mieszkalny Savaran: tu schował się Rovis i tu mieszka Jola-09. Zajmuje to bardzo mało przestrzeni jak na populację.
                        1. Poziom Minus Dwa 
                            1. Więzienie: zagnieździła się Pięknościarka Alucis, uspokajając i kojąc więźniów
                        1. Poziom Minus Trzy
                            1. Wielkie Obrady: tam spotyka się Rada w swoim centrum dowodzenia

## Czas

* Opóźnienie: 244
* Dni: 5
