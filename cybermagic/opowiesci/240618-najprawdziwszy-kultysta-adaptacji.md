## Metadane

* title: "Najprawdziwszy Kultysta Adaptacji"
* threads: problemy-con-szernief
* motives: the-protector, the-aspirant, the-hunter, the-inquisitor, energia-ixion, dom-w-plomieniach, dark-temptation, lost-my-way, spotlight-na-postac, walka-o-rzad-dusz
* gm: żółw
* players: mactator, seba, marcin_p, lordan

## Kontynuacja
### Kampanijna

* [240417 - To nie jest moja córka](240417-to-nie-jest-moja-corka)

### Chronologiczna

* [240417 - To nie jest moja córka](240417-to-nie-jest-moja-corka)

## Plan sesji
### Projekt Wizji Sesji

* Opowieść o:
    * Inspiracja:
        * Neon Genesis Evangelion
            * "Shinji, weź się w garść i WALCZ, przestań się martwić!"
            * samozniszczenie poszczególnych jednostek, presja ponad siły
        * Mortal Love "Sanity"
            * "I know I'm self destructive, | I keep letting myself down | But my life in chaos always kills it in the end"
                * Kalista: ona stworzyła Ragasza. Ona boi się używania magii.
                * Mawir: pattern 'Redfog' (SupCom). Znajdzie i zniszczy przeciwnika czystą nienawiścią. Jest przestarzały, ale da radę.
    * Theme & Message: 
        * "Duty. Odpowiedzialność. Jak daleko się posuniesz dla swojej Sprawy?"
            * _Kalista zrobi wszystko dla Stacji, nawet swoim kosztem._
            * _Mawir obroni Kult - to jest kolejne wyzwanie ze strony Saitaera. Tylko silniejszy ma prawo przetrwać. Older doesn't mean useless._
            * _Ragasz jest winny Saitaerowi perfekcję. Ten Kult jest nie dość dobry._
            * _Kultyści nie wiedzą w którą stronę iść - Ragasz czy Mawir?_
            * _Felina nie pozwoli, by Stacja ucierpiała, nawet, jeśli musi zrobić coś drastycznego._
        * "Zagubieni w tym, co jest właściwe. Zmęczeni wieczną wojną. Nie są pewni, czy robią dobrze."
            * _Mawir nie wie, czy powinien walczyć. Jest zmęczony. Nie rozumie._
            * _Ralena nie wie co robić i w którą stronę iść._
            * _Kalista widzi działania Ragasza. Czy ona też jest potworem? Czy nie ma wyjścia?_
    * Jakie emocje?
        * FAZA 1: 
            * "Nasi sojusznicy są rozbici. Są atakowani. I tracimy zaufanie kultu."
            * "Czy Mawir w ogóle jest w stanie pokonać Ragasza? Czy powinien?"
            * "Czy Ragasz jest KORZYSTNY? Jego moce pomagają."
        * FAZA 2: 
            * "Dobra - kultystą jest Ragasz. To on! Wiem kim on jest!"
            * "Jaką decyzję podejmiemy? Mawir czy Ragasz? Kto powinien rządzić Kultem? Ragasz jest charyzmatyczny i ma potęgę."
    * Lokalizacja: Szernief
* Po co ta sesja?
    * Jak wzbogaca świat
        * pokazuje saitaerowców, ixion i presję ixiońską.
        * głęboka eksploracja postaci Mawira
    * Czemu jest ciekawa?
        * konflikt wewnętrzny: 
            * Kalista, Mawir, Ralena. Żadne z nich nie wie w którą stronę iść.
        * konflikt zewnętrzny: 
            * wieczna walka o rząd dusz saitaerowców przez Ragasza.
            * próba eksterminacji Czarodziejki (zaczynając od Aeriny)
            * Saitaerowcy uszkadzają Stację w imię Adaptacji; Felina vs Saitaerowcy
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * the-aspirant: Kultysta Saitaera spoza Stacji (Ragasz). Pragnie przejąć Kult Saitaera - kompetencją i skutecznością.
    * the-hunter: Terrorformy-kultyści próbują wykończyć Kalistę, Mawira oraz Ralenę. Mawir próbuje odnaleźć i zniszczyć Ragasza.
    * the-protector: Kalista. Zrobi wszystko by ochronić stację. Idealistka, skupia się na tym, by jak najwięcej dało się zrobić dla Stacji.
    * the-inquisitor: Felina. Tym razem zrobi WSZYSTKO by zabezpieczyć Stację. Współpracuje z Teraquid i chroni oraz osłania.
    * energia-ixion: ewolucja Kultu Saitaera przez Ragasza. Istnienie potworów i terrorformów. Oraz czysta energia Ragasza - ewolucja i transorganika.
    * dom-w-plomieniach: wojna domowa między saitaerowcami oraz saitaerowcy - Stacja.
    * dark-temptation: saitaerowcy są skonfliktowani - w którą stronę iść. Stacja czy Kultysta, który wyraźnie ma potęgę? Też Ralena - what is she and what's the future?
    * lost-my-way: Kalista, Mawir, Ralena - konfrontują się z tym że nie są tym czym powinni być. Kwestionują swoje decyzje i życie.
    * spotlight-na-postac: Mawir Hong. Przestarzały, dumny, będzie walczył. Ale NIE poświęci Kalisty, choć może Ralenę.
    * walka-o-rzad-dusz: Aspirant pragnie skusić Kultystów i dać im prawdziwą wolność Saitaera.
* O co grają Gracze?
    * Sukces:
        * Jest decyzja odnośnie Stacji i Stacja nie zostaje zniszczona
    * Porażka: 
        * Stacja zostaje zniszczona, Kalista ginie
* O co gra MG?
    * Highlevel
        * zniszczenia na Stacji niemożliwe do ukrycia
        * pokazanie horroru ixionu
    * Co chcę uzyskać fabularnie
        * ujawnienie Kalisty jako czarodziejki
        * decyzja: Ragasz czy Mawir ze strony graczy
* Default Future
    * samobójczy kultyści Saitaera wysadzają poszczególne części Stacji, niszcząc dowodzenie i zabijając Kalistę.
    * Ragasz nie pragnie zniszczenia Kultu. Jeśli się myli, sam się zniszczy.
* Dilemma: Kto powinien rządzić Kultem?
    * -> pokaż świetne rzeczy które robi Ragasz
    * -> pokaż corrupted gifts
* Crisis source: 
    * HEART: "Duty towards Saitaer" vs "Duty towards the station"
    * VISIBLE: ""
* Agendy
    * Mawir (the-hunter)
        * poluje na Ragasza, tylko jeden może rządzić Kultem Saitaera
    * Kultysta (the-aspirant)
        * pragnie przejąć Kult mrocznymi darami
    * Kalista (the-protector)
        * pragnie ochronić Stację za wszelką cenę. Ona stworzyła Ragasza, ona zapłaci cenę.

### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

KONTEKST:

* Zgodny z CHRONOLOGIĄ

CHRONOLOGIA:

* 2 miesiące temu na Stacji pojawił się Kultysta, który przybył jako bogaty przedsiębiorca
    * Kultysta jest szczerym wyznawcą Saitaera, uważa lokalny kult za nie dość godny Jego mocy.
* niedawno Ralena stała się osobą
* Ragasz uzyskał dzięki mocy Kalisty moc ixiońską / Saitaera. He terrorformed. Teraz jest gotowy by przejąć Kult
    * tworzy terrorformy
    * daje dary

PRZECIWNIK:

* Ragasz


### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

Faza 0: TUTORIAL

* Tonalnie
    * Pokazanie ZMĘCZENIA oraz rozsypywania się wszystkiego
    * Mawir, który się poddaje? Czy który atakuje i się chowa?
* Wiedza o świecie
    * Kult Saitaera i o co w nim chodzi.
    * Potęga Mawira i Mawir jako dewastator, ale niegłupi.
* Kontekst postaci i problemu
    * Pokazanie problemu Mawira - jest skonfliktowany i nie wie co robić, traci kontrolę.
    * Pokazanie Feliny jako ekstremistki. "Głupia dziewczynka" zdaniem Mawira, odpychająca drakolitów.
* Implementacja
    * Gracze jako Mawir, który zatrzymuje grupę drakolitów przed okradnięciem sklepu.
        * "Mawir, siła bez poświęcenia"
        * odzyskać kontrolę
        * POKAZAĆ IM dumę, potęgę i to, że jest przerażający
    * Felina chce aresztować Mawira i drakolitów
        * co zrobią gracze?

.

* Faza 1: "Budowa Mrocznego Serca"
* Faza 2: "Wojna o kult i śmierć czarodziejki"

.

| Kto                 | 1 (s0 + 30 min)   | 2 (s0 + 60 min)             | 3 (s0 + 90 min)          | 4 (s0 + 120 min) |
|---------------------|-------------------|-----------------------------|--------------------------|------------------|
| **Aspirant**        | hunt Aerina       | civil war, attack Sorceress | coalesce cult, spawn     | bombo-calypse    |
| **Mawir**           | dominate cult     | civil war, conflict (eat)   | force alliance and kill  | eat Sorceress / die to Aspirant |
| **Czarodziejka**    | point at places   | rytuał detekcji & is target | rytuał separacji & bait  | death in fire           |
| **Felina**          | cooperation       | Teraquid vs Saitaer         | isolate, separate, fight | protected what possible |
| **Echoes**          | Sulut widzi przypadki | corrupted cases         | Ralena chooses Saitaer   | nic                     |

Overall

* chains
    * Zawarte w Fazach powyżej
* stakes
    * 
* opponent
    * .
* problem
    * .

## Sesja - analiza
### Fiszki

.

### Scena Zero - impl
#### Jak sesja została opisana ludziom

Zubożała Stacja Kosmiczna od dawna ma problemy: z integracją kultur, z anomaliami magicznymi, z maksymalizacją zysków dla inwestorów. Niestety, przez te problemy Stacja od dawna była beczką prochu czekającą na eksplozję. I w końcu na Stacji pojawił się ktoś, kto próbuje ją zdestabilizować wykorzystując potwory (które teoretycznie nie istnieją). I co gorsza, ten ktoś najpewniej jest magiem.

Na tej Stacji jest też jedna lokalna czarodziejka - Wasza sojuszniczka. Jest niewyszkolona i ukrywa swoją tożsamość przed wszystkimi, łącznie z Wami. Wielokrotnie z Waszą pomocą uratowała Stację Kosmiczną przed anomaliami. Niestety, przybysz próbuje ją zmusić do ujawnienia się, co będzie dla niej katastrofą. Ale czarodziejka nie dopuści do śmierci niewinnych lub zniszczenia Stacji; jest idealistką.

Wy jesteście grupą lokalnych mieszkańców Stacji (np. inżynier środowiskowy, celnik, księgowy). Macie przyjaciół na Stacji, koneksje, pracę i zasoby. Wy - w odróżnieniu od większości Stacji - wiecie o magii. I Wy stajecie w samym środku tej sytuacji. Stacja to Wasz dom.

Czy uratujecie czarodziejkę przed śmiercią lub ujawnieniem? Czy dogadacie się z przybyszem? Przyjmiecie jego cele? A może pomożecie lokalnemu przywódcy kultu? Czy uda się Wam - zwykłym ludziom - uratować Stację przed rozerwaniem pomiędzy tymi siłami? I czy przy tym wszystkim uda się sprawić, by wiedza o magii się nie rozprzestrzeniła?

Jeśli Wam się nie uda, Stacja Kosmiczna Szernief może zostać zniszczona.

#### Implementacja właściwa

Dewastacja sklepu, dowodzi dewastacją Fulgor Sanat:

"Sanat, co Ty do cholery robisz?!"

Tr+3:

* V: jest inny kapłan Saitaera, "lepszy" niż Mawir
* X: wpadają siły porządkowe Feliny
* XXX: próba deeskalacji kończy się ogólną opinią wśród kultystów "Mawir się starzeje, nie jest tym czym był kiedyś"
* V: Felina uwierzy że nie można eskalować i lepiej wypuścić po tym jak kultyści zapłacą grzywnę.

### Sesja Właściwa - impl

3 dni później.

Wojna gangów się rozpala. Wszystko wskazuje na to, że jest ktoś jeszcze oprócz Mawira. Ale kto to może być? Zespół analizuje przepływ finansów, przepływ danych z kamer - jakoś muszą się z nim spotykać, jakoś te informacje muszą płynąć. Więc - pieniądze, rozkazy, kto kiedy się pojawił. Co się tu dzieje? Kim jest Kultysta?

Ex+2:

* X: Ragasz wykonuje akcję przeciwko Czarodziejce; Czarodziejka jest coraz bardziej odcięta.
* X: Coraz gorętsza wojna domowa na CON Szernief; to się samo nie zdeeskaluje.
* V: Informacje o oficerach Ragasza. Wiadomo kto roznosi polecenia.
* X: Ragasz jest chroniony. Nie da się do niego praktycznie dojść.
* X: Ragasz wie, kim jest Czarodziejka. Zna tożsamość Kalisty.
* V: Zespół zna tożsamość Ragasza.

Mawir złapał Zespół. Kto jest przeciwnikiem. Mawir wie, że Zespół coś wie. Zespół się wykręcił; przekazał Mawirowi informację o oficerach. To jest coś, co można przekazać i warto przekazać.

Zbierzmy dane co tu naprawdę się dzieje. Dlaczego Ragasz pobiera takie ingredienty a nie inne. Czemu atak na sklepy. O co chodzi.

TrZ+3:

* V: Celem jest destabilizacja stacji + pozyskanie konkretnych przedmiotów; podniesienie wiary w Saitaera jako "nie mamy już wyjścia".
    * Emocje prowadzą do wzmocnienia magii Ragasza. Stacja stanie się stacją Saitaera.

Dorian, przechwycony przy okazji, odpowiedział na kilka pytań odnośnie Kultu. Sam się martwi sytuacją.

Tr +3:

* V: Mawir robi się łagodny - taka ogólna opinia. Mawir się kończy.
* V: Mawir NIE WIE, czy powinien działać. W końcu Druga Strona faktycznie ma moc Saitaera a on nie...

Arystokrata i Biosynt korzystają z okazji i chcą spotkać się z Aeriną Cavalis. Aerina chętnie spotka się z przyjacielem, zwłaszcza w tak trudnej sytuacji. 

Zanim pojawił się Ernest Cavalis, Zespół przekonał Aerinę do spotkania z Ragaszem. Business opportunities. Bo Zespół chce móc podejść do Ragasza, porozmawiać, zrozumieć...

TrZ+2

* V: Sukces, Aerina im to załatwi
* ALE: Aerina i Mawir się rozdzielają

I podczas spotkania atakuje ich służący ("mawirowiec", saitaerowiec), który ma za zadanie zabić Aerinę (Ragasz wydał ten rozkaz gdy podejrzewał Aerinę o bycie czarodziejką).

Tp +3

* X: Najemnik chroniący Aerinę zginął
* X: Zabójca schował się za Aeriną; nie ma jak do niego się sensownie dostać; ZRANIŁ ją
* V: Zabójca odcięty od Aeriny przez biosynta
* X: Udało się zabić Zabójcę
* V: Aerina jest niewinna i nie jest czarodziejką. Ragasz już to wie; nie będzie więcej ataków na Aerinę
* (Aerina wstrząśnięta i przerażona)

Estril nie zgadza się na wykorzystywanie jego córki instrumentalnie. To jest nieakceptowalne. Zespół przekonuje go, że przecież Aerina miała dotyk magii w przeszłości. A skąd wiedzą? Bo czarodziejka. I, faktycznie, Kalista musi pojawić się osobiście by przekonać Estrila i Aerinę do pomocy (ku wielkiej złości Aeriny, bo Kalista nic jej nie powiedziała). Estril jednak ochroni Kalistę; uważa to za właściwe w aktualnej sytuacji.

Estril chowa Kalistę w bezpiecznym miejscu z pomocą Zespołu, ALE Ragasz wie już kim ona jest. I wysłał swoich najlepszych zabójców by załatwić Kalistę. (TO BYŁ BŁĄD ZESPOŁU!)

Ex Z+2:

* X: Kalista jest w stanie się bronić magicznie; jednak zostaje ciężko ranna i potrzebna jej pomoc medyczna. Kalista nic nie zrobi.
* X: Tożsamość Kalisty jako czarodziejki zostaje ujawniona.
* X: Pojawia się kolejne shardowanie Stacji - część osób ogniskuje się dookoła Kalisty, część Mawirowców zostaje Ragaszowcami na zawsze. Czyli Stacja ma jeszcze więcej małych frakcji.
* V: Kalista przetrwa; Mawir Hong ją uratował od zabójców.

Nie da się "po prostu" zabić Ragasza. To nie przejdzie. Nie da się tego zrobić dyskretnie a jego moc polityczna jest tak duża, że mawirowcy zrobią rzeź. Czyli Ragasz jest dość bezpieczny. Dość. Bo Zespół ma jako wsparcie Ralenę - "mimika". I pojawił się chory plan:

* Niech komandosi Estrila zabiją Ragasza kiedyś w kiblu, jak on się nie spodziewa.
* Niech Ralena zasymiluje Ragasza i przejmie jego tożsamość i miejsce.

I mniej więcej to zostało zrobione. Inżynier przekierował kamery na pętlę gdy Ragasz był odizolowany, czterech komandosów Estrila wkroczyło by rozstrzelać Ragasza zanim ten się nawet zorientował...

Tr Z+2:

* V: Zlokalizowali Ragasza w odpowiednim miejscu izolacji (kiblu), a Ragasz nie ma powodu podejrzewać ludzi Estrila (to byłoby dla Estrila samobójstwo polityczne)
* V: Potraktowanie Ragasza gazem + rozpylonym lapisem z wentylacji + brak osób po drodze. Ragasz nie ma możliwości użycia pełni mocy czy Drugiej Formy.
* V: Czwórka komandosów, Ragasz rozstrzelany na śmierć.
* (+3Ob +2Vb +2Xb): V: Ralena 'zjadła' Ragasza i przybrała jego formę. Ma jego wygląd i formę, nie jego wiedzę. Estril to ukrywa; "Ralena jest w jego kabinach".
* V: Gdy Zespół spotkał się z Mawirem, powiedzieli że Ragasz nie żyje i Ralena jest Ragaszem. Mawir to łyknął - Ragasz był nie dość silny, więc Ralena go pokonała. Ale niech "Ragasz" mu służy. Ralena skwapliwie się zgodziła.

Zespół postanowił pomóc Stacji. Korzystając z okazji. Zróbmy reality show "życie ulicy w ukryciu", takie "z kamerą wśród zwierząt". Może uda się to dobrze sprzedać, merch, monetyzować. Może wyprowadzi się tą Stację ku lepszemu. I pracuje nad tym Aerina Cavalis z resztą Zespołu.

TrZ+4:

* V: rozpoznawalność Stacji rośnie; też ilość problemów się nieco zmniejszy
* X: radykalni savaranie będą bardzo niezadowoleni ze straty wartościowej energii
* X: ściągamy na stację różne szemrane typy i nowe problemy
* X: rośnie zainteresowanie Agencji - w końcu Coś tu się stało
* V: większe prosperity, a o to chodzi!
* X: więcej freaków...

Ale rośnie:

* popularność Stacji jako ciekawego miejsca jakie mamy.
* dobrobyt
* populacja (awanturnicy, wyrzutki), czyli ROZWODNIENIE.
* pieniądze, możliwe do przeznaczenia na stan Stacji.

## Streszczenie

Kultysta - Ragasz - zaczął rozbudowywać mandat Saitaera na Stacji, destabilizując ją mocniej. Zmienieni ragaszowcy prawie zabili Aerinę i PRAWIE dorwali Kalistę. Mawir nie był pewny, czy ma mandat walki z istotą Saitaera. Szczęśliwie, Mawir się otrząsnął i to on uratował Kalistę z ręki ragaszowców. Zespół wszedł w ścisły sojusz z Estrilem i z jego pomocą zabili Ragasza, rozstrzeliwując go w ubikacji a "Ralena" go zjadła, stając się "Ragaszem". Stacja jest zdestabilizowana politycznie, ale Mawir i "Ragasz" naprawią to od strony mawirowców a Aerina z Zespołem zrobiła akcję propagandową ściągającą nowych ludzi na Stację. Gorzej, że tożsamość Czarodziejki jest ujawniona...

## Progresja

* Kalista Surilik: tydzień w szpitalu, wymaga pomocy medycznej. Jej tożsamość jako czarodziejki została ujawniona. 
* Ralena Karwist: trwale ma formę Ragasza Morkola, kapłana Saitaera mawirowców i Kultysty.
* Mawir Hong: pojawia się plotka, że się zaczyna starzeć. Kto wie, może się kończy?

## Zasługi

* Klasa Biosynt Medyczny: nie pamiętam
* Klasa Inżynier Unifikator: nie pamiętam
* Klasa Arystokrata Popularyzator: nie pamiętam
* Klasa Drakolita Szuler: nie pamiętam
* Fulgor Sanat: pierwszy wyznawca Ragasza; stanął po jego stronie by dostarczać mu jak najwięcej materiałów. Zamknięty przez Felinę w więzieniu na czas sesji XD.
* Mawir Hong: nieco niepewny co powinien robić, w końcu ON nie jest magiem a Ragasz tak, czy Saitaer wybrał Ragasza? W końcu uznał, że nie - on jest Mawirem, Saitaer żąda zwycięstwa najlepszego. Uratował Kalistę przed siepaczami Ragasza i wrócił do kontrolowania mawirowców na Szernief. Wykazał się wielkodusznością.
* Ragasz Morkol: Kultysta Saitaera który ma moc ixiońską; prawie zabił Kalistę i Aerinę, zmienił sporo mawirowców w ixionformy i trochę odciągnął kult od Mawira. Ostatecznie - rozstrzelany z lapisem w ubikacji.
* Kalista Surilik: czarodziejka która zrobi WSZYSTKO by ratować Stację. Ujawniła się Estrilowi (co dało info Ragaszowi), po czym użyła magii by nie zostać zniszczoną przez Ragasza. Ciężko ranna, jak nigdy.
* Felina Amatanir: bardzo twardo naciska na mawirowców, bo próbuje utrzymać stację w ryzach. Nie rozumie wojny wewnętrznej między Mawirem i Ragaszem; nie rozumie wpływu magii na tą dwójkę.
* Aerina Cavalis: teraz jest przekonana, że miała rację chcąc rozwodnić Stację. Pomaga Zespołowi wpierw wpakować Ragasza w pułapkę, potem z Zespołem tworzy stały reality show by ściągnąć ludzi na Stację.
* Estril Cavalis: chroni córkę przed planami antyRagaszowymi mogącymi ją skrzywdzić; sam idzie na pierwszy front walki z Ragaszem. Jego komandosi zabili Ragasza, on sam osłonił Kalistę i przejął jej frakcję.
* Ralena Karwist: nadal szuka swojej drogi; dla Kalisty i Zespołu "zjadła" Ragasza i na stałe przyjęła jego formę. Chwilowo pod Mawirem, stabilizuje swój umysł i nowe umiejętności.

## Frakcje

* Anomaliści Szernief: dołączył do nich Ernest Cavalis i Aerina; Ernest chce przejąć dowodzenie. Osiągnęli duży sukces niszcząc Ragasza. Co więcej, Ralena (też tu) przyjęła formę Ragasza.
* Con Szernief Mawirowcy: zdestabilizowani. Część zrobiła się bardzo agresywna, część się Zmieniła. Wielu wie o magii. Teraz "Ragasz" (Ralena) i Mawir będą to musieli opanować.
* Con Szernief Oszczędnościowcy: wzmocnieni i dużo bardziej fanatyczni niż wcześniej, zwłaszcza w obliczu ruchów Aeriny oraz Ragasza. Będą mocniej naciskać na oszczędność; WIDZĄ że coś jest nie tak.

## Fakty Lokalizacji

* CON Szernief: Ragasz zrobił ruch - prawie zabił Kalistę i Aerinę; Kalista została ujawniona jako czarodziejka. Ragasz został pożarty przez Ralenę. Stacja stabilniejsza, ale nadal niestabilna - Sia jest wzmocniona. Agencja się zainteresowała Stacją.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Kalmantis
            1. Sebirialis, orbita
                1. CON Szernief

## Czas

* Opóźnienie: 3
* Dni: 4
