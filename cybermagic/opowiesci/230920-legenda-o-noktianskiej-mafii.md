## Metadane

* title: "Legenda o noktiańskiem mafii"
* threads: pronoktianska-mafia-kajrata, furia-mataris-agentka-mafii
* motives: teren-morderczy, echa-inwazji-noctis, infiltracja-dyskretna, casual-cruelty, gang-oportunistyczny
* gm: żółw
* players: kić, fox

## Kontynuacja
### Kampanijna

* [230913 - Operacja: spotkać się z Dmitrim](230913-operacja-spotkac-sie-z-dmitrim)

### Chronologiczna

* [230913 - Operacja: spotkać się z Dmitrim](230913-operacja-spotkac-sie-z-dmitrim)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ?
* Struktura: 
    * ?
* CEL: 
    * MG
    * Drużynowy
* THEME
    * 

### Co się stało i co wiemy

* Dane
    * Kajrat wie, kto stoi za porwaniem Furii - Grzymość
    * Kajrat chce pozyskać maga, w końcu, by odzyskać Aynę
    * KONKLUZJA: porywamy maga

### Co się stanie (what will happen)

* F1: _Dotarcie do Zaczęstwa, do Przytułku Cichych Gwiazd_
    * stakes:
        * Dmitri wpadnie w kłopoty
        * Furie ściągną kłopoty na noktian
        * Grzymościowcy dorwą Furie
    * scenery:
        * klatki (nie z noktianami i na dziewczyny)
        * złomiarska chata w lesie
    * opponent: 
        * Grzymościowcy, reprezentowani przez Ksawerego Kleszcza (człowiek, chce 'noktiańskie laseczki') ("SKĄD MASZ LEKI! PRACUJESZ SAM?")
        * Dmitri, chroniący Furie
    * problem:
        * Grzymościowcy skrzywdzą Dmitra i jego żonę, Petrę
        * dotrzeć do Podwiertu
        * pociąg i Podwiert
            * potwory na trasie Podwiert - Zaczęstwo; potrzebny jest pociąg
            * "jesteś malutką noktianką; zrobisz czego chcę i nikt się nie dowie" w pociągu
* F2: _Zaczęstwo_
    * stakes:
        * Furie zostaną wykryte
        * Furie sprowadzą kłopoty na cywili
        * Furie wpadną w czyjeś ręce
    * opportunities:
        * Talia, Konrad Warłocznik
        * baza z Przytułka 
        * las z innymi noktianami
    * scenery:
        * klatki (nie z noktianami i na dziewczyny)
        * Przytułek Cicha Gwiazdka
        * Fortifarma Lechotka (pozytywne miejsce)
        * Fortifarma Amanuel
        * Kasyno Marzeń
    * opponent: 
        * Zaczęstwo, jako miasto
        * Grzymościowcy
    * problem:
        * gdzie jest jakiś mag Grzymościa?
            * Talia? Dziewczyny z AMZ?
        * jak się połapać?
        
### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Xavera Sirtas
    * OCEAN: (C+A-): żywy ogień, jej dusza jest pełna pasji, a jej słowa ostrzem. "Kto siedzi cicho, tego spotyka wszystko co najgorsze"
    * VALS: (Security, Tradition): wszystko odnosi do tego co już widziała i co było, działa dość tradycyjnie. "Wszystko już było i się stało"
    * Core Wound - Lie: "Przeze to, że dużo mówiłam i szperałam, moi rodzice się rozwiedli" - "Będę walczyć i ognistą walką udowodnię, że warto być razem!"
    * Styl: Zawsze gotowa do walki. Agresywna i asertywna. Dobrze wybiera walki. Jej upór często wpędza ją w kłopoty.
* Dmitri Karpov: astoriański sojusznik Kajrata chcący zniszczenia Grzymościa
    * OCEAN (O- C+): very hands-on and methodical, gruff and silent. "Keep your friends close, but your enemies closer."
    * VALS: (Tradition, Power): strength and control became paramount after his loss. While vengeance burns in his heart, he's not careless. "Strength allows you to shape the world."
    * Core Wound - Lie: "straciłem to co kochałem najbardziej przez mafię" - "jeśli ich zniszczę, znajdę lepsze życie"
    * Styl: relentless and gritty, willing to work with anyone; sometimes too trusting of those who promise aid against Grzymość
    * Loss: Wolny Uśmiech framed Dmitri's family in a major scandal, causing them to lose their standing in society
    * Work: Zwiadowca i Identyfikator zagrożeń; ma dobry pojazd (Narveen) i porusza się dobrze w trudnych warunkach
* Bogdan Gwiazdocisz: założył przytułek powojenny pod Zaczęstwem, gdzie nie zadaje nikomu pytań
    * OCEAN (N+ E-): pełen refleksji i samokrytyki. "Przeszłość kształtuje, ale nie definiuje."
    * VALS (Benevolence, Universalism): "Każdy zasługuje na drugą szansę, bez względu na przeszłość."
    * Core Wound - Lie: "zniszczyłem tak wiele istnień, wierzyłem w cel uświęca środki" - "jeśli uratuję kogo się da i będę nieść dobro, być może pozyskam wybaczenie"
    * Styl: Bogdan jest powściągliwy, zawsze unika rozmów o sobie, w jego oczach widać ciężar winy i pragnienie odkupienia.
    * Deed: served as an interrogator and his methods were ruthless, employing psychological and physical torture to extract information from prisoners
* Feliks Samagród: mag Grzymościa, specjalizujący się w kontroli mentalnej i łamaniu umysłów
    * OCEAN (E+ C-): Charyzmatyczny i uroczy, impulsywny i zawsze lgnie do młodych dam. "Przyjemność i styl są tym, co odróżnia nas od zwierząt."
    * VALS (Hedonism, Conformity): Każdemu według zasług. Ci, co nas napadli muszą odpłacić. "Ci, którzy już raz odrzucili człowieczeństwo posłużą reszcie."
    * Core Wound - Lie: "Noctis zabrało mi WSZYSTKO" - "Mafia zapewni siłę i sprawiedliwość tam, gdzie terminusi nie mieli jaj."
    * Styl: Feliks jest wytworny i ma srebrne usta, łatwo przekonując innych do podążania za swoimi kaprysami. Jest znany z wystawnych przyjęć gdzie demonstruje lalki.
* Konrad Worłocznik: przyjaciel Dmitriego i Petry
    * Styl: Konrad jest zawsze elegancko ubrany, choć jego ubrania są już trochę przestarzałe. Jego postawa i uśmiech nadal świadczą o pewności siebie.

### Scena Zero - impl

.

### Sesja Właściwa - impl

10 dni później, w domku złomiarskim Dmitriego oraz Petry. Dodatkowe dane:

* Terminusi robią swoje. Skupiają się na ochronie terenu. Tępią noktian... tych co wystawiają pyszczki.
    * Noktianin jedzie pociągiem i "nie wygląda i nie zachowuje się noktiańsko", jest spokój
    * Porządek, ordnung i zasady. Nie obchodzą ich tematy międzyludzkie - oni są na potwory i magię.
* Ludzie są bardzo anty-noktiańscy.
    * Terminusi nie odwracają oczu, bo pilnują porządek
* Furie wyglądają jak znaturalizowane noktianki poprzedniej generacji
* Tu wchodzi Grzymość. Nie wiadomo skąd ma kasę (Dmitri nie wie)
    * on daje lokalsom "możecie kopać noktian", zasłużoną zemstę
    * on rozpowszechnia środki psychoaktywne i rozrywkę z nieznanych źródeł
    * terminusi nic z nim nie robią, bo to zdestabilizuje sytuację. Traktują jako zło konieczne. Tymczasowo.
    * Arnulf nie jest zwolennikiem Grzymościa. O nie.
    * Grzymość poluje "na obrzeżach" prawa
        * domy publiczne (domy publiczne ogólnie są legalne. Ale Grzymość ma takie... specjalne)
        * kasyno
        * ma wpływy w części fortifarm
    * W lasach są noktianie. I wymierają.
* Plan Dmitriego jest zabawny - ma dla Was "fałszywe dokumenty". One nie są do końca fałszywe. Nie są wasze i nie wytrzymają inspekcji ręcznej.
    * Dmitri zawiezie do Podwiertu. Tam i tak są składowiska itp, więc tam musi oddać znalezione.
    * Tam bierzecie pociąg do Zaczęstwa. Tam jest baza Grzymościa.
        Tam jest Talia Aegis. Czarodziejka. Ekspert od psychotroniki. Pod "ochroną" terminusów, mieszka niedaleko kampusa uniwersyteckiego.

Dwunastego dnia miałyście pojechać z Dmitrim. W nocy dziesiątego dnia obudził Was Dmitri.

* D: "Musicie się schować, ludzie Grzymościa tu są. I nie MOGĘ ich odeprzeć, bo jak odeprę..."
* D: "Zwykle nie mieli sprzętu, to ich typowa wizyta. Chcą kasy. I pokazać kto tu rządzi."
* D: (gorzki uśmiech) "Załatwisz tych, przyjdą kolejni. Jeśli coś im zrobię, jestem w dupie."
* D: "Zwykle przychodzą, dadzą mi w mordę raz czy dwa a potem pójdą..."

Amanda i Xavera się chowają. W miejscach, gdzie mogą się ukryć.

Tr Z +2:

* Vz: skutecznie schowały się przed ludźmi Grzymościa.

Ludzi Grzymościa jest czterech. Są w lekkich pancerzach bojowych. Fortifarma nie próbowała ich zestrzelić, nie otwierali drzwi itp. Grzymościowcy po pewnym czasie weszli i faktycznie, mimo dobrego sprzętu do przetrwania, nie zauważyli Was - Furie są gdzieś przy generatorze (ciepło itp). Grzymościowcy powiedzieli, że "jedną noktiankę im dał to szukają więcej". I wrócili do dyskusji po średnim przeszukaniu.

* G: A co to za medykamenty? To nie od nas?
* D: To dla żony, od was to mam.
* G: Nie nie nie, nie od nas, takich nie mamy. Najpierw nas nie wpuszczasz, potem się zadajesz z kimś innym, może z jakąś mafią?

Krzyk. Po dźwiękach, coś w stylu łamanego palca.

* G: Skąd to masz?
* D: Od was!
* G: Przyprowadź mi mi tą tam, jego żonę. 
* D: Dobra! Znalazłem w jednej z tych jednostek. Były tam leki.
* G: Nie jesteś dobrym obywatelem. A wiesz co spotyka złych obywateli?

Grzymościowcy torturują Dmitriego i Petrę. A Furie cicho się wydostały z fortifarmy, na zewnątrz. Dwa ścigacze dwuosobowe. Furie - szybko zamaskowane jako Czerwone Myszy - _vroom vroom_ i spieprzają. Zamieszanie jakby tu wpadły. Ale - by to uwiarygodnić - Amanda odpaliła defensywy fortifarmy na moment (że defensywa, że ktoś się zbliża) i wyłączone znowu. By uwiarygodnić.

* X: tortury będą spore. Też, by 'odreagować'.
* X: to nie Myszy. To już wiadomo. To jest dowód na INNĄ grupę przestępczą, INNĄ mafię, która skorzystała z okazji bo nie ma zasobów. Na tym terenie jest inna mafia.
* X: ścigacze ruszyły i się zablokowały. Kolesie wypadają z budynku i Wy jesteście w sumie przy lesie. Ale ścigacze mają broń.
* Vz: Furie wymieniły spojrzenie, Amanda szybko wyjęła zawleczkę z granatu w ścigaczu i Furie łapiąc broń lecą w długą. Kolesie biegną za nimi strzelając, ale nie trafiają.
    * "Łap je łap je!"
    * "Ta czarniutka jest moja!" (właśnie ten koleś leci w kierunku ścigacza z granatem)
* V: Jeden ścigacz zniszczony granatem (ich własnym). Drugi ścigacz jest uszkodzony ale sprawny. Jeden koleś ma potrzaskane nogi. Ścigaczem. Drugi koleś nie żyje. Dwóch pozostałych takich... zamroczonych. A Furie w las.

Dwóch zamroczonych kolesi, jeden uszkodzony ścigacz. Jeden umierający pod zniszczonym ścigaczem. Amanda bezwzględnie zastrzeli jednego Grzymościowca. Jest jeden z wyraźną mową ciała "WTF CHCĘ DO DOMU" i drugi - a raczej druga - która szuka Was w lesie. Amanda strzela do laski.

* X: Laska skutecznie się wycofała i ewakuowała się do fortifarmy. Niestety, Amanda nie mając swojej broni i mając - dla odmiany - kompetentnego przeciwnika w pancerzu nie była w stanie jej zabić. Acz laska jest ranna i najpewniej nie ma ochoty na walkę.
* V: Xavera osłaniana przez Amandę szybkimi susami dotarła do faceta. Laska próbowała się wychylić z fortifarmy ale strzał Amandy ją bardzo zniechęcił. Laska zmieniła plan.

.

* Facet: "nie zabijaj mnie! Poddaję się!"
* Xavera: "rozważę to, rozbieraj się"
* Facet: (zaczyna się rozbierać)
* (fortifarma na głośnikach): "PATRYK walcz z nią! Nie bądź tchórzem (powstrzymywany śmiech)"
* Patryk: (desperackie spojrzenie na Xaverę) (Xavera się uśmiecha słodko) (Patryk rozbiera się szybciej)
* Patryk: ona nam kazała! Nie rób mi krzywdy!
* Irilea: Patryk... (rozczarowanie)
* Xavera: Nieźle was gnoi, co? Wygląda na taką
* Patryk: TAK TAK! Irilea jest straszna!
* Xavera: Egzotyczne, imigranci Was z interesu wygryzają?
* Patryk: Śliczna Irilea... ona jest... ona po prostu jest. Nie wiem, ok?
* Irilea: Tak trzymać! Patryk walcz z nią! Szermierka słowna! (cichy śmiech) Patryk w swoim żywiole!
* Xavera: Takiego wsparcia dawno nie widziałam u sojuszników.
    * Irelia. Furia Irelia. I to może być ona. Ten sam śmiech. Ta sama dzikość w walce. Ale nie jest sadystyczna... nie była.
* Irilea: Patryku, Patryku, trzymasz głowę w nocniku... (sing-song)

Gdy Patryk się PRAWIE pozbył zbroi, Irilea daje znać "jeśli nie zdejmiesz zaraz gaci, zestrzelę Cię!". Patryk zamarł. "Irilea... ale... ale..."

* Vr: Xavera zadała ból Patrykowi i zmusiła go do współpracy ZANIM Irilea odpaliła system fortifarmy. Ścigacz podjechał pod Amandę. Spierniczają, unikając pierwszego działa. Patryk zrobił pod siebie ze strachu.

Patryk jest w panice, w mantrze o Irilei... "ona jest psychiczna, nienormalna...". Xavera "nie znałeś koleżanki".

Tp (czy dobrze zinterpretuje) +3:

* V: Patryk wie, że ona jest "z zewnątrz". Jest od Grzymościa. Jest maskotką zespołu. Cicha, sympatyczna... ale ona powoduje złe rzeczy.
    * ona sprawiła, że oni tu przyjechali. "czy mają dość jaj by być twardzi i groźni" - to sprawiło, że oni chcieli się wykazać przed nią.
    * ona jest dzika w łóżku, niesamowicie lojalna wobec Grzymościa
    * chronologia się zgadza - Iriliea może być Furią.
    * Patryk skamle - ogólnie, to była niesankcjonowana akcja. Irilea ich podpuściła a oni poszli...
        * Irelia miała niesamowicie silną wolę i była świetna w pułapkach, przyczajaniu się, oszustwach...
    * (+3Vg -> oszukujemy Patryka że tu jest inna mafia)
* X: Patryk doszedł do tego, że one są z Noctis. Że są jak Irilea. Że mają te same patterny.
* V: Legenda o mafii noktiańskiej gdzie są Furie.
* V: WSZYSTKIE dowody świadczą o tym, że mafia noktiańska jest tu od dawna. I że to był element Inwazji. I "Saitaer" czy "wygnanie" to tylko zasłona. I teraz mafia noktiańska zainteresowała się Grzymościem - bo porwał Furie. Które o tym nie wiedziały. Ale ON z mafii noktiańskiej WIEDZIAŁ.

Furie rozbijają ścigacz o budynek policji w Podwiercie z karteczką "Grzymość mówi że jesteście głupi". Po czym dwie młode damy, których NIE było już w tym czasie w okolicy ścigacza spokojnie poszły na pociąg.

Gdy mówimy o pociągu nie mówimy o PKP. To czołg na szynach. Maglev. Furie spokojnie weszły do pociągu na fałszywych papierach i pojechały do Zaczęstwa. Nikt szczególnie nie zwraca uwagi na Furie. Jeden facet, taki większy, podchodzi do Furii.

* Facet: "Jesteście pieprzonymi noktiankami"
* Amanda: "(głębokie znużone westchnięcie) Kolejny. Fakt, że nasi rodzice byli kim byli nie oznacza że ja wiem co to Noctis, zostaw mnie pan w spokoju."
* Facet: (chce złapać Amandę za ramię) (KTOŚ INNY złapał jego za rękę)
* Ktoś inny: "Zostaw je. To dzieciaki."
* Facet: To noktianki
* Ktoś inny: JA mówię że nie. (a jest większy)

Amanda i Xavera nie miały szczególnych kłopotów. Gdy jednak wyszły z pociągu, od razu oczom Furii - kobieta, która udaje że na nie nie patrzy. W średnim wieku. Lokalna, na oko. Na stacji jest jeden konstruminus który monitoruje sytuację, więc tu Furiom nic nie grozi. Amanda i Xavera próbują się zorientować w sytuacji.

Tp+3:

* V: To nie tylko kobieta. Ona jest z co najmniej jednym facetem i wymieniła się spojrzeniem z facetem z pociągu. Który już się nie zbliża. To wygląda jak grupa, nie osoba.
* V: "Dwie słodkie naturalizowane noktianki" nieświadomie poszły w kierunku przytułka. W odpowiednim miejscu i momencie zeszły z głównej drogi by się schować przed facetem z pociągu którego widziały.
    * I tam spotkały czterech innych silniejszych facetów. Oni próbowali Was zaskoczyć. Z chloroformem. Ale PRÓBOWALI to jest keyword.

TrZ+3:

* V: (p) Skutecznie Furie odbiły się i wyrwały z walki i chloroformu i wybiegły na ulicę uciekając. Dwóch w pogoń, jeden naprawia jajecznicę.
* X: (p) "miały szczęście, uciekły i tam był konstruminus". DALEJ będą chcieli je wyhaczyć, ale nie POLUJĄ aktywnie.
* X: (m) delikwent co dostał w jaja był SYNEM szefa tego gangu. I doznał poważnych obrażeń jajecznych. Teraz szef gangu CHCE tą dwójkę (Furie). Będzie szukał.
* X: (e) jest drona, która poluje na Furie - bo konstruminus je uratował. Ten gang ma teraz apetyt na Furie.

Przytułek.

* Jest na skraju, na "terenach niczyich".
* Jest tam kilka uzbrojonych osób. I jest jeden konstruminus. Na stałe. Nie zwraca uwagi na noktian.

.

* B: Witajcie, z pewnością przyjechałyście z Podwiertu
* A: Tak
* B: "Nazywam się Bogdan Gwiazdocisz. Nie zadajemy tu pytań. Bądźcie tu jak w domu. Zaakceptujcie, że nie wszyscy... są w dobrym stanie, ale nikomu nie stanie się krzywda i wszyscy są bezpieczni."
* Koleś nie zadaje pytań, wyraźnie próbuje pomóc, powiedział what-is-what i co tu jest i im grozi, powiedział gdzie są ważne miejsca, powiedział że pomoże znaleźć pracę jak coś.
    * I terminusi pomogą jak coś im grozi. Nie są na bezpiecznym terenie, ale SAM TEREN jest dość chroniony.
* Przytułek to "kampus" a nie jeden budynek.

Dzień - dwa Furie się zabunkrowały i udają że wszystko jest normalne. Tylko Irelia mogłaby ich poznać (bo jest Furią i zna potencjał), ale Irelia poszła na reedukację.

Są tu różni ludzie - wolontariusze, osoby które już mają pracę itp. W działaniach wolontariatu i humanitarnych BARDZO łatwo o czarne charaktery. Jeśli Furie zaangażują się w to by pomagać, jest spora szansa że spotkają kogoś z mafii. Potencjalnie są w stanie namierzyć maga mafii. Małymi kroczkami. A przy okazji rozeznanie, wykrycie itp. I w dodatku nie można stwierdzić - dobra opinia w oczach organizatora przytułka to jest nic. A jeśli pomożemy, to będzie dobrze.

## Streszczenie

Furie z Karpovami opracowywały infiltrację i jak się zachowywać jako lokalsi. Jednak pewnej nocy pojawili się Grzymościowcy by torturować Karpovów (bez sensu). Furie jako Czerwone Myszy spróbowały porwać ścigacze - nie wyszło. Skończyło się na walce (masakrze), gdzie jako NOWA mafia, noktiańska, odparły Grzymościowców (i dowiedziały się, że jest tu inna Furia, Irelia, mindwarpowana). Po dotarciu do Zaczęstwa przez Podwiert Furie wpakowały się na gang porywający młode kobiety (który ich nie porwał) i schowały się w Przytułku. Nie wiedzą o tym, ale gang na nie dalej poluje, nie wiedząc kim są...

## Progresja

* .

## Zasługi

* Amanda Kajrat: gdy nie udało się cicho ukraść ścigacza, zostawiła granat i ustrzeliła przeciwników (acz nie Irelię). Złamała morale Grzymościowców. Dotarła do Zaczęstwa.
* Xavera Sirtas: gdy nie udało się cicho ukraść ścigacza, przechwyciła i złamała Patryka. Korzystając, że łowcy dziewczyn skupiają się na Amandzie, skrzywdziła faceta i się oderwały. Dotarła do Zaczęstwa.
* Dmitri Karpov: torturowany przez Grzymościowców; jest link 'skąd masz te leki'. Szczęśliwie, przetrwał.
* Petra Karpov: torturowana przez Grzymościowców, ale przetrwała.
* Irelia Kairanolis: kiedyś Furia; miała niesamowicie silną wolę i była świetna w pułapkach, przyczajaniu się, oszustwach. Teraz 'Irilea', maskotka zespołu. Cicha, sympatyczna... ale podpuszcza i psychotyczna. 'Irilea' świetnie walczy i sama jest w stanie mając dobry teren walczyć z Amandą i Xaverą. Nie dba o swoich sojuszników - Grzymościowców.
* Patryk Majwuron: Grzymościowiec; by zaimponować 'Irilei' torturował Karpovów. Pobity i shańbiony przez Xaverę, doszedł jednak do linku 'Furie - Irelia'. Skończył oddany Grzymościowi w pohańbiony sposób.
* Bogdan Gwiazdocisz: opiekun przytułka; nie zadaje pytań, zaprosił Amandę i Xaverę do Fortifarmy CustoLucis przy Zaczęstwie i powiedział jej podstawowe informacje what-is-what itp.

## Frakcje

* Nocne Niebo: po raz pierwszy powstała plotka 'mafia noktiańska' przez działania Amandy i Xavery. Cóż, Furie poszły na całość - niech Grzymość myśli, że to był kluczowy plan Inwazji... 
* Wolny Uśmiech: stracił niewielki oddział p.d. Irelii. Wie, że na terenie działa 'mafia noktiańska', co nadaje im głęboką paranoję.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Powiat Przymurski
                                1. Fortifarma Karpovska
                            1. Zaczęstwo, obrzeża
                                1. Przytułek Cicha Gwiazdka: przytułek dla ofiar wojny, nieważne: Noctis, Astoria, itp. Zarządzany przez Bogdana Gwiazdocisza.

## Czas

* Opóźnienie: 10
* Dni: 3

## Specjalne

* .

## OTHER

.