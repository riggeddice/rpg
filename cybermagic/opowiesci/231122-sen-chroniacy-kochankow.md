## Metadane

* title: "Sen chroniący kochanków"
* threads: agencja-lux-umbrarum, problemy-con-szernief
* motives: bardzo-irytujacy-reporter, gift-is-anthrax, dont-mess-with-my-people, energia-alucis, faceci-w-czerni, gang-oportunistyczny, milosc-zakazana, rodzina-trzyma-sie-razem, starcia-kultur, subtelna-ingerencja-polityczna, symbol-lepszego-jutra, teren-bezprawia, zagrozenie-pryzmatyczne
* gm: żółw
* players: igor, amelia, quarka

## Kontynuacja
### Kampanijna

* [231119 - Tajemnicze tunele Sebirialis](231119-tajemnicze-tunele-sebirialis)

### Chronologiczna

* [231119 - Tajemnicze tunele Sebirialis](231119-tajemnicze-tunele-sebirialis)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA: 
    * Luca Turilli "Prophet of the Last Eclipse"
        * "She's ready to turn off her life's flame | To free all his people from hell | But no, he doesn't want to accept thos solution | Prefering to think of how he can escape"
        * "She looked at him praying "forgive me" | But he could nost answer at all | She was gonna be killed by the | Mass of those riles | Now screaming and trying | To stop all their pain"
        * "She was now lying there | Dying in a lake of ice and blood ... | When her blood ran through the dark ice | The surface began to tremble "
        * Ta scena się PRAWIE wydarzyła, gdyby nie interwencja Pięknościarki, manifestacji Arazille, która się 'przyczepiła' do Joli-09 i do Rovisa.
* Opowieść o (Theme and vision): 
    * "Głupia miłość młodych ludzi uratowała CON Szernief przed katastrofą wynikającą z przeciążenia pryzmatycznego w kierunku zniszczenia."
        * Inwestor Damian Orczakin (Locke@SoltyRei) dąży do turystyki -> promuje areny i wzmacniacze -> by doprowadzić do wojny domowej by interwencja wyczyściła stację i dała lepszą populację.
            * nie wie o magii i konsekwencjach uruchomienia kierunku na _The Slaughter_ z zagrożeniem pryzmatycznym.
            * to on skłonił NavirMed do dodatkowych "hate" pakietów dla drakolitów
        * Inwestor Klaudiusz Widar (biurokrata) dąży do maksymalizacji pieniędzy ze stacji pod kątem inwestycji w eksport energii
            * to on skłonił NavirMed do pakietów zwiększających efektywność; nawet dopłaca by dodatkowo je brali dla zwiększonej efektywności.
        * Gdy nienawiść rośnie, zaczyna się Skażenie Pryzmatyczne. On chciał ją chronić, nawet stając przeciw swoim 'braciom'. Wtedy - manifestacja Arazille
        * Za każdym razem jak nienawiść za bardzo rośnie, Pięknościarka się manifestuje i koi kogoś.
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * bardzo-irytujacy-reporter: Kalista. Kalista jest PRZEKONANA że dodawane jest coś do wody i ogólnie nikt nic z tym nie robi. Robi własne śledztwa.
    * gift-is-anthrax: wektor, za pomocą którego dostarczany jest destabilizator emocjonalny. Tylko najsilniejsi pracują w kosmosie / w kopalni i oni biorą odpowiednie stymulanty i środki.
    * dont-mess-with-my-people: savarańska grupa samopomocy w odpowiedzi na inwazję drakolickich zbirów. Opcja eskalacji BARDZO wysoko.
    * energia-alucis: Ukojenie Arazille, Pięknościarka, która koi w odpowiedzi na "zdradę rasy" wynikającą z działań Rovisa i Joli-09.
    * faceci-w-czerni: rola Zespołu (Lux Umbrarum). Usunąć energię Alucis, stworzyć 'plausible deniability' i zapewnić by to się nie działo nadal.
    * gang-oportunistyczny: pierwotna grupka drakolitów, wzmacnianych przez stymulanty i okazję NavirMed.
    * milosc-zakazana: Rovis, drakolicki security (prawa ręka) oraz Jola-09, savarańska inżynier solarna i kochana artystka ("cichy śpiew w próżni")
    * rodzina-trzyma-sie-razem: tu osobno: Savaranie (defensywa) ORAZ dwa drakolickie gangi.
    * starcia-kultur: savaranie i drakolici, eskalowane przez wzmacniacze drakolickie
    * subtelna-ingerencja-polityczna: Inwestor Damian Orczakin próbuje zdestabilizować kolonię nadając temat drakolitom i savaranom
    * symbol-lepszego-jutra: Felina. Walczy o to, by był porządek, dba o wszystkich i wszystko. Jeden z głównych integratorów Kolonii.
    * teren-bezprawia: tereny savarańskie na CON Szernief rządzą się swoimi prawami, tak samo jak tereny maszynowni gdzie jest Arena i gdzie rządzi gang drakolicki.
    * zagrozenie-pryzmatyczne: narastające konflikty pomiędzy siłami grożą potencjalnymi manifestacjami _The Slaughter_ lub _The Bliss_.
* Detale
    * Crisis source: 
        * HEART: "sin (grzech): próba doprowadzenia do wojny domowej i zarabiania na tym przez inwestora prowadzi do manifestacji Pięknościarki"
        * VISIBLE: "Ukojenie Arazille (plaga snu), rosnąca agresja, stacja wymyka się spod kontroli i potencjalna konieczność sił obcych (z czym walczy Felina)"
    * Delta future
        * DARK FUTURE:
            * chain 1: SEE NO EVIL: nic nie wyjdzie na jaw, proceder będzie kontynuowany, więzienie i badania są dalej, NavirMed nie ucierpi w żaden sposób
            * chain 2: EXECUTION: inwestorzy, uwzględniając Kalistę zostaną zabici przez albo łowców albo przez więźniów. Eryk skutecznie dojdzie do tego że krwią MOŻE zniszczyć Więzienie.
            * chain 3: PRAWDA: wiedza o magii wyjdzie na jaw, konieczność aplikowania masowych amnestyków.
        * LIGHT FUTURE: 
            * chain 1: NavirMed musi wycofać działania i siły z terenu, odpowiednie osoby po stronie CON ucierpią
            * chain 2: Uda się uratować część ludzi, ewakuować itp.
            * chain 3: Nikt jak o magii nie wiedział tak nie wie
    * Dilemma: brak

### Co się stało i co wiemy

* Kontekst stacji
    * CON Szernief została zaprojektowana jako stacja sprzedająca Irianium. Jest to miejsce o podniesionej ochronności elmag (lokalizacja: przy Sebirialis).
    * Okazuje się, że populacja która zainteresowała się tymi warunkami to byli Savaranie.
        * I inwestorzy są w klopsie. Dla savaran nadmiar energii to radość. Nie chcą kupować seks-androidów ani niczego. Pełna oszczędność.
            * Savaranie to NAJGORZEJ z perspektywy korporacji i inwestycji XD
        * Inwestorzy żądają zwrotu
    * Zarządcy CON Szernief szukają kogoś kto będzie tam chciał żyć i najlepiej za to jeszcze zapłaci
        * Stanęło na ściągnięciu Kultystów Adaptacji. Ale oni nie są jacyś super niebezpieczni, sami tak mówią ;-).
* Przeszłość
    1. 6 miesięcy temu: CON Szernief wróciło do działania; wszystko wydaje się OK
    2. 3 miesiące temu: Stymulanty
        * Wprowadzone zostały zasady konkurencyjne (Widar). Drakolici indywidualnie, savaranie grupowo. Savaranie mają przewagę.
        * Drakolici zaczęli kupować stymulanty wzmacniające, za czym stoją siły Klaudiusza Widara
        * Aniela z NavirMed na prośbę Damiana Orczakina dodała do niektórych stymulantów Paranoizator.
            * -> rośnie niezadowolenie i chęć walki
        * Drakolici uważają, że savaranie grają nieczysto i 'odpychają' savaran. Savaranie z nimi walczą.
        * Powstają grupki, m.in. gang Mawira Honga.
    3. 2 miesiące temu: Syn pacyfistycznego Terimana, Rovis, decyduje się porwać Jolę-09
        * TU JEST SCENA ZERO
        * Rovis jest skonfliktowany przez Doriona (próbuje być lepszy niż Rovis i jeszcze jednego savaranina)
        * Rovis i Jola-09 się w sobie zakochują.
        * Po 2 dniach morderczy atak savaran, czego się nikt nie spodziewał. Ogromne straty i zniszczenia. Felina ledwo utrzymała.
    5. 1 miesiąc temu: Rovis i Jola-09 się spotykają, co zostało wykryte.
        * Dorion i 3 siepaczy ruszyli za Rovisem i Jolą.
        * Showdown w okolicach korytarzy w Promenadzie - pojawienie się Pięknościarki
    6. DO TERAZ
        * Wszyscy myślą że Rovis nie żyje (ukrywa się w strefie Savaran z Jolą-09)
        * Mawir i jego siły są coraz bardziej pożerani przez chęć zemsty i walki. Krew napędza moc. 
            * SUKCES MAWIRA: krwawa wojna pomiędzy savaranami a drakolitami; 'wypadki'. I dorwanie Rovisa.
            * Ruchy: zbrojenie się, propaganda, porywanie savaran
        * Teriman współpracuje z Kalistą i Feliną; nie wie co się dzieje, oddaje się Saitaerowi
            * SUKCES TERIMANA: odzyskanie Rovisa, opanowanie i kontrola drakolitów
            * Ruchy: neutralizacja, nadawanie tematu Felinie, agregacja drakolitów
        * Orczakin pragnie by NavirMed znalazło sposób na neutralizację 'hate plague'
            * SUKCES ORCZAKINA: nikt nie wie o nim, nie ma hate plague, populacja będzie zastąpiona przez 'normalnych'
            * MROCZNY SUKCES: wygaszenie hate plague doprowadzi do zwycięstwa Arazille
            * Ruchy: propaganda, ściąganie oddziału ciężkiego, uspokajanie, 'trust in science'
        * Pięknościarka pragnie Ukoić
            * manifestacja: Rovis/Jola (zagrożenie), duże stężenie ludzi z jednolitymi emocjami, niewidoczna dla maszyn, mindwarp. WIĘZIENIE.
            * SUKCES: everybody sleeps
            * Ruchy: ukojenie, ochrona
        * Rovis / Julia pragną być razem i przetrwać
        * Savaranie nie rozumieją co się dzieje i chcą się utrzymać i ratować stację.
* Teraźniejszość
    * .
* Ślady
    * .


### Co się stanie (what will happen)

* F0: TUTORIAL
    * (Rovis z Dorianem +1) porywają Jolę-09 (w okolicy 5 savaran)
    * Dorion próbuje być lepszy niż Rovis i jeszcze jednego savaranina
    * -> "i wtedy były największe zamieszki i najwięcej krwi popłynęło".

## Sesja - analiza

### Fiszki

Luminarius

* Lester Martz, dowódca operacji

CON Szernief:

* Janvir Krassus
    * OCEAN: (O+ C+) | "Przez współpracę i innowacje budujemy lepszą przyszłość." (Locke) | VALS: (Tradition, Security) | DRIVE: "Przekształcenie Szernief w zaawansowaną kolonię."
    * on jest odpowiedzialny za współpracę z NavirMed
* Larkus Talvinir
    * OCEAN: A-O- | "Żartuję nawet w złych sytuacjach; prowokuję i nie boję się konfliktów" | VALS: Stimulation, Universalism | DRIVE: "Zabawa kosztem innych to moja rozrywka"
    * główny inżynier w sztabie
* Marta Sarilit
    * OCEAN: (N+ O+) | "Jeśli nie będziemy dość szybcy, zostaniemy zniszczeni." | VALS: (Benevolence, Power) | DRIVE: "Naprawa i lecznie"
    * dowodzi więzieniem NavirMed; to jej operacja
* Felina Amatanir
    * OCEAN: (N- C+) | "Nieustraszona i niezależna, wyróżnia się determinacją i profesjonalizmem" | VALS: (Power, Security) | DRIVE: "Porządek i sprawiedliwość, ponad kontrakt"
    * drakolitka o zwiększonych parametrach; wywalili ją z zarządu i z dowodzenia ochroną po tej operacji
* Kalista Surilik
    * OCEAN: (A+E+) | "Odważna i zdecydowana, w gorącej siarce kąpana" | VALS: (Benevolence, Achievement) | DRIVE: "Prawda MUSI wyjść na jaw"
    * już z nią Agencja miała do czynienia...

#### Strony

.

### Scena Zero - impl

(rozpisanie Tp+4 poniżej:)

Po ciemnej stronie stacji cylindrycznej znajduje się trzech przygotowanych do walki drakolitów, ubranych w skafandry inżynieryjne. Widzą w oddali pięciu savarańskich ekspertów od paneli słonecznych, korygujących awarie. Drakolicy poruszają się skutecznie - wpierw obezwładniają pierwszą dwójkę, jednocześnie usuwając duży reflektor pozycyjny, umożliwiający Savaranom pracę w tym miejscu. Następnie dwóch drakolitów atakuje z zaskoczenia pozostałą dwójkę Savaran, a szef napastników, Rovis, łapie savarańską ekspertkę i unieszkodliwia komunikację, używając odpowiedniego urządzenia.

Operacja się udała.

Trzej drakolici używając magnesów przyczepili czterech Savaran do powierzchni stacji, by nie odlecieli w próżnię i zdecydowali się zabrać ostatnią savarańską ekspertkę ze sobą, zgodnie z rozkazem Mawira.

Jeden z drakolitów, Dorian, sprzeciwił się Rovis, ponieważ sam chciał dowodzić operacją i nie podobało mu się, że Rovis ma takie powodzenie. Zauważył, że mogą porwać więcej niż tylko tę jedną Savarankę. Rovis słusznie zauważył, że szef życzy sobie tej i tylko tej kobiety. Po krótkiej kłótni Dorian został przekonany i z lekko zgryźliwą miną wykonał polecenie. Drakolici ewakuowali się z pojmaną savarańską ekspertką, nikomu nic szczególnego się nie stało.

Agenci Lux Umbrarum skończyli oglądać raport zakończony słowami "i wtedy doszło do największych zamieszek w historii stacji..."

Następnym nagraniem, które dotarło ze stacji i wywołało niepokój, była informacja od sarderyckiego lekarza. Ujawnił, że ponad 50 osób nieprzerwanie śpi – zjawisko to rozpoczęło się około dwóch miesięcy temu, a przyczyny ich stanu pozostają nieznane, gdyż badania nie wykazują nic niezwykłego. Lekarz zasugerował, że warto zbadać tę sprawę i ustalić, co się tam dzieje, ponieważ jeśli nie znajdzie się rozwiązania, rozważa wykorzystanie ich organów, takich jak zdrowe nerki czy sprawne ręce, które mogłyby pomóc osobom bardziej potrzebującym niż ci, którzy pozostają w śpiączce.

Zespół badawczy podejrzewa obecność magii, podobnie jak agencja. Jednakże zespół nie ma pewności, z czym ma do czynienia, i został zobowiązany do zajęcia się tą sprawą oraz znalezienia rozwiązania.

### Sesja Właściwa - impl

Luminarius dociera do CON Szernief.

Jeszcze zanim w pełni zadokowali na stacji, podłamała ich struktura tej stacji – większość populacji stanowią Savaranie, obecni są także liczni drakoliccy kultyści. Inwestorzy, którzy chcieli odzyskać pieniądze i zarobić więcej, sprzedając dobra na stacji, szybko zrozumieli, że Savaranie, co oczywiste, niewiele kupią.

Oficer naukowy badał dane znajdujące się w komputerze Luminariusa, starając się ustalić, z czym mogą mieć do czynienia i czy cokolwiek wskazuje na zjawiska anormalne czy magiczne. Szybko natrafił na Alucis – Snu Ukojenia. Wydaje się, że potencjalnie śpiący ludzie są dotknięci przez Alucis. Jednak aby magia mogła się pojawić, musi znaleźć zaczepienie w czymś lub wykorzystać jakąś energię. Alucis sam w sobie nie pojawiłby się znikąd, skąd zatem to wszystko się wzięło?

Oficer Naukowy działa dalej "Popatrzmy na dane. Co z nich wynika. Czy widać wzory. Kto był pierwszą śpiącą osobą. Co mi to mówi." Itp.

Tp+4:

* V: hipoteza, że jeśli woda neutralizuje to więcej spać będzie savaran. A jednak: 20 savaran, 30 drakolitów. Więc od nich się zaczęło.
* V: pięć pierwszych śpiących osób to Dorian i 2 osoby z gangu Rovisa oraz 2 innych gangsterów. Więc ludzie powiązani z Rovisem. Ale bez niego.
    * Rovis zniknął, nie pojawia się szczególnie.

Hacker zabrała się do działania. "Jakie są ruchy Rovisa"?

TrZ+4:

* X: Kalista wie
* V: Ruchy Rovisa
* V: Ilość ofiar podczas odbijania savaranki
* V: Rovis nie walczył na serio

Po co zatem Mawirowi, szefowi gangu, była potrzebna savaranka? Okazało się, że porwana savaranka nazywa się Jola-09 i jest savarańskim odpowiednikiem gwiazdy pop. Czasami cicho nuci w ciemności, gdy wszyscy patrzą w gwiazdy. Porwanie tej konkretnej savaranki spowodowało dość duże irytacje po stronie savaran.

Dobrze, ale dlaczego? Skąd w ogóle konflikt między Savaranami a drakolitami – te dwa ludy bardzo rzadko wchodzą w interakcje ze sobą. Jak pokazały przeszłe dane, około 3 miesiące temu doszło do zmiany zasad odnośnie wynagradzania ludzi. Wcześniej wynagrodzenia miały wysoką podstawę, a pod wpływem działań inwestora Klaudiusza Widara, zostało zmienione to na system premiowy.

Savaranie mieli to gdzieś, drakolicy byli za, bo to przecież jest mechanizm lepszy – dostanie więcej. Jednak po wprowadzeniu tego systemu doszło do tego, że savaranie – lud bardzo grupowy i społeczny – zaczęli ściśle współpracować ze sobą i dzielili się prowizją. Natomiast drakolicy – bardzo indywidualistyczny lud – działali indywidualnie i nie byli w stanie pokonać grupy savaran pracujących w jedną stronę. To spowodowało duże niesnaski pomiędzy ludami, ale stacja była w stanie na tym trochę zaoszczędzić.

Porwanie Joli miało zmusić Savaran do rozmowy i do jakiejś formy kompromisu. Z uwagi na to, że stacja ma za mało ludzi jak na przestrzeń, savaranie nie byli w stanie Joli-09 odnaleźć. 

Aż jakimś cudem im się to udało.

Hackera dostała dostęp do danych z kamery, pokazującej rajd savaran na drakolitów. Mimo że dane z kamery nie były szczególnie wyraźne ani dobrze widoczne, podczas odbijania savarańskiej gwiazdy pop,były rejestrowane przez kamerę wziętą przez jednego z nich i później uploadowane.

Wszyscy członkowie agencji są obeznani z walką. Od razu rzuciło im się w oczy kilka anomalii – Rovis zdecydowanie nie walczył na serio. Walczył dobrze, ale nikomu nie zrobił poważnej krzywdy. Savaran było około 50, mimo że z natury jest to stosunkowo słabszy lud, ale byli w stanie zabić jednego drakolitę i ciężko poturbować pozostałych. Zazwyczaj broni tego miejsca 8 drakolitów, ale jakimś cudem 4 z nich nie było obecnych. Kolejny zbieg okoliczności.

Hakerka od razu wywnioskowała, że potencjalnie za tym wszystkim stoi Rovis. W jaki sposób Savaranie mieliby wiedzieć o tym miejscu? Czemu było mniej strażników? Czemu Rovis nie walczył tak ostro? Czyżby Rovis miał słabość do Joli?

Oficer naukowy wysunął teorię, że wszystkie te zamieszki i zmiany wyglądają, jakby inwestorzy próbowali pozbyć się Savaran z tej stacji. 

Niestety działania hakerki sprawiły, że Kalista – lokalna dziennikarka i wielki ból głowy agencji – dowiedziała się, że agencja tu węszy i dzieje się coś dziwnego...

(pula jest kontynuowana uwzględniając Inspiratorkę)

* V: Rovis żyje, ukrywa się na terenach savaran
* V: Kalista rzucona na temat, zainteresuje się
* X: Kalista podejrzewa Alteris i magię
* V: Przeszłe badania Kalisty odnośnie NavirMed
* V: Inspiratorka (Dyplomata) rozmawia z praktykantem w NavirMed

Nadszedł czas, aby uruchomić Inspiratorkę. Wiemy, że w okolicy jest osoba, która zna się na magii, należy do Rady i jest postrzegana pozytywnie przez nasz Zespół (i go tak postrzega). To Felina – drakolicka ekspertka od ochrony. Inspiratorka (Dyplomata) porozmawiała z Feliną i od razu zaczęła od mocnego uderzenia: „Wiemy, że nie możesz nam powiedzieć, ale mam pewną teorię – po prostu mrugnij dwa razy, jeśli Rovis ukrywa się na terenach savaran. Felina była bardzo zdziwiona, ale potwierdziła.

Aby odwrócić uwagę Kalisty, zespół nadał jej temat ‘Romeo i Julia’ jako opowieść o Rovisie i Joli-09. Hakerka zagłębiła się w sprawy, nad którymi pracuje Kalista, i odkryła, że od pewnego czasu Kalista podejrzewa radę stacji o dodawanie czegoś do wody. Znalazła dowody na zmowę Rady z firmą NavirMed. Początkowo Widar skoncentrował się na stymulantach, aby drakolici wydawali więcej pieniędzy i byli najlepsi, a jakiś czas po rozpoczęciu ‘plagi nienawiści’, Orczakin zaczął dyskretnie komunikować się z NavirMed w sprawie środków uspokajających i redukujących napięcie.

Świetnie.

Inspiratorka (Dyplomata) skontaktowała się z NavirMed, a dokładniej z praktykantem tam umieszczonym przez Agencję. Ten, przekonany przez Inspiratorkę, że to ważna sprawa, udostępnił wszystkie dane o stymulantach.

Okazało się, że istnieje nie jeden, ale trzy typy stymulantów:

* Stymulant normalny podstawowy – ma sprawić, by drakolicy byli lepsi. Zamówiony przez Widara.
* Stymulant z domieszką paranoi i agresji – zamówiony przez Orczakina 3 miesiące temu. Od tego momentu zaczęły się rosnąć siły Mawira i zwiększać agresja.
* Stymulant z domieszką uspokajaczy – zamówiony przez Orczakina niedawno. To ma naprawić to, co zostało zepsute wcześniej.

Jeszcze świetniej.

Na podstawie tych informacji oficer naukowy wysunął pewną hipotezę: nie musieli wiedzieć o magii z uwagi na podniesione pole energii magicznej na tej stacji. Jednak i tak popełnili błąd. Wprowadzenie stymulantów z paranoją i zwiększeniem agresji przy podniesionym polu magicznym może prowadzić do manifestacji energii Esuriit (Nienawistny Głód). Problem polega na tym, że w tej chwili mamy do czynienia z manifestacją Alucis. Energia Esuriit kontruje energię Alucis. Jeżeli jedna z tych energii zostanie usunięta lub ograniczona, druga może zdominować sytuację. Tak więc muszą jednocześnie pozbyć się obu energii i jeszcze nieznacznie obniżyć pole magiczne, w przeciwnym razie stacja będzie miała katastrofalne problemy.

No po prostu idealnie.

A Zespół DALEJ nie ma pojęcia co spowodowało manifestację Alucis.

Inspiratorka (Dyplomata) chciała porozmawiać z osobami, które zrobiły rajd - odbiły Jolę-09. 

Inspiratorka (Dyplomata) postanowiła porozmawiać z osobami, które przeprowadziły rajd i odbiły Jolę-09. Większość z nich znajduje się w więzieniu, przynajmniej część z nich. Co bardzo zaskoczyło Inspiratorkę, zarówno Savaranie jak i drakolicy w więzieniu zachowywali się bardzo podobnie, niezależnie od tego, czy byli faktycznie zamieszani w tę sprawę, czy nie. Byli bardzo pasywni, łagodni i sympatyczni. Nie wydawało się, aby na czymkolwiek im zależało, i mieli taki błogi uśmiech. To wyraźnie wskazuje na manifestację Alucis – to cholerstwo się rozprzestrzenia.

Na podstawie tego wszystkiego, oficer naukowy stwierdził, że ma dostęp do kilku różnych próbek:

* czyste stymulanty,
* stymulanty nienawiści,
* stymulanty ukojenia,
* krew ludzi śpiących od początku,
* krew ludzi z więzienia, którzy nie śpią,
* krew ludzi nie dotkniętych przez Alucis.

W przypadku czystej krwi najlepszą grupą są Savaranie, ponieważ każdy drakolita jest inny i zmodyfikowany inaczej. Jednak Savaranie stanowią świetną bazę i punkt wyjściowy. Więc Oficer naukowy robi badania szukające i porównujące co się tu dzieje:

Tr Z +4:

* X: nie da się nikomu nic udowodnić formalnie, dobrze zrobione
* V: mamy różne miejsca Alucis, mamy różne 'poziomy wpływu', mamy jak to wykryć po czystych hormonach
* V: mamy jak to SKONTROWAĆ - Alucis przez stymulanty Esuriit, Esuriit przez stymulanty Alucis.
    * acz to niebezpieczne.

Mamy dość. Kończy się czas, jest coraz niebezpieczniej - czas spróbować OBUDZIĆ kogoś dotkniętego Alucis. Kogoś, kto był na początku. Przesłuchajmy go. Dowiedzmy się, co naprawdę się stało. Oficer naukowy przygotowuje antidotum i rozpoczyna swoje działania.

ExZ+3+3Ob:

* X: Dzięki Kaliście jest opinia "OMG AGENCJA ROBI EKSPERYMENTY NA LUDZIACH". (+1 pkt Toru Niegodziwości)
* Ob: Badany człowiek jest wiecznie GŁODNY i podpięty do manifestacji Alucis
* X: Badany koleś umarł (+5 tor niegodziwości)
* V: Uda się przesłuchać jedną z osób
    * WIEMY co się stało - wpierw Rovis skupiał się na Joli-09 odkąd została umyta; Dorian podejrzewał problemy od samego początku.
    * Rovis spotykał się z Jolą.
    * Ich miłość została odkryta. Siepacze Mawira poszli rozwiązać problem Rovisa (acz nie zabijać Joli)
    * Jola i Rovis się wycofali do ciemnych tuneli i Rovis chronił Jolę.
    * Gdy ich krew się połączyła pojawiła się ONA. Pięknościarka Arazille.
        * ona chroni ich związek
* ObOb: Pięknościarka jest zlinkowana nie tylko ze śpiącymi ale też z nieśpiącymi. 5k osób trzeba przenieść w inteligentny sposób. Agencja ma problem.
* XX: Stacja jest na granicy wojny domowej oraz Agencja jest jedną z sił winnych (3-miesięczny ban na kopalnię)

Ale udało się dojść do tego jak działamy z sytuacją. Pięknościarka jest zaczepiona przede wszystkim w Rovisie i Joli. Agencja zaczęła więc wielowymiarowe działanie:

* Stworzyć lapisowane antidotum, które ma odpiąć 5k ludzi. (redukcja Esuriit i Alucis jako pole magiczne)
* Przesiedlić Jolę i Rovisa poza stację. (redukcja energii Alucis na stacji)
* Wycofać ustawę by redukować 'competetiveness' stacji (redukcja Esuriit na stacji)
* Konsekwentne i stabilne działanie w ciągu następnych dwóch tygodni by to się udało

Oficer Naukowy pracuje nad antidotum i prawidłowej aplikacji środków dla wszystkich obecnych. W tym czasie Inspirator (Dyplomata) skupia się na rozmowie z savaranami.

Tr Z +3:

* V: Jola i Rovis mogą opuścić to miejsce JEŻELI weźmie się innych savaran (około 150) ze sobą
* V: Oki, za coś zaawansowanego technologicznie co pomoże grupie. Nie trzeba radzić sobie z przenoszeniem 150 savaran.

CZYLI:

Dla dyplomaty ta sytuacja była czymś całkowicie nowym. Wchodząc do blockhausu, który bardziej przypominał mrowisko niż miejsce normalnego życia ludzi, miała okazję porozmawiać z Jolą-09. Przy wejściu stało czterech strażników. Zarówno Jola-09, jak i strażnicy komunikowali się niesamowicie oszczędnie, co było kolejnym wyzwaniem dla dyplomaty. Podczas rozmowy dyplomata poruszyła kwestię Rovisa, zasugerowawszy, że opuszczenie tego terenu przez Rovisa i Jolę-09 byłoby korzystne. Jak zatem można to osiągnąć?

Po dłuższej dyskusji, w której ponad 90% słów padło z ust dyplomaty, ustalono, że Jola-09 i Rovis mogą opuścić ten teren, pod warunkiem, że zabrani zostaną również inni członkowie rodziny Joli-09 z klanu Savaran. Wykazując się niesamowitym refleksem, dyplomata zapytała, co jeśli dostarczą coś, co przyda się wszystkim?

I tak oto Rovis i Jola-09 opuszczą ten teren w zamian za zaawansowany mechanizm do recyklingu dostarczony przez agencję. Czyli Savaranie przehandlowali Jolę-09 i jedną osobę wchodzącą do klanu Savaran za zaawansowany aparat do recyklingu. Bardzo savarańskie podejście.

Następnym ruchem Dyplomaty była rozmowa z Radą i Inwestorami.

Tr Z +4:

* V: Wycofają ustawę
* V: Odpuszczą trochę finansowanie by się to nie rozpadło (szantażyk <3)

CZYLI:

Dyplomata, spotykając się z radą i inwestorami, od samego początku zaznaczyła, że sytuacja zmierza ku gorszemu, co podchwyciła Felina. Wyraziła też przekonanie, że należy podjąć działania, a agencja wie, co należy zrobić, choć to może się nie spodobać. Dyplomata zwróciła uwagę, że problemy zaczęły się od momentu zmiany systemu wynagradzania - wtedy narodziły się elementy zwalczania i nienawiści, które teraz eskalują, zagrażając stacji.

Widar zaznaczył, że sytuacja jest opanowalna, jeśli Felina będzie efektywnie wykonywać swoją pracę. Orczakin dodał, że jeśli Felina nie jest w stanie sobie poradzić, to on jest gotów ściągnąć ludzi z zewnątrz. Zanim dyskusja stała się bardziej napięta, dyplomata delikatnie zasugerowała, że problem zaczął się również, gdy dostarczono te stymulanty i gdy potencjalnie zaczęto robić dziwne rzeczy z nimi. Nie powiedziała niczego, co mogłoby skłócić inwestorów, ale dała im do zrozumienia, że wie o co chodzi i że informacja o tym będzie wyglądała źle, jeśli trafi do Kalisty.

Pod wpływem demonstracji wiedzy dyplomaty oraz konieczności współpracy między nią, inwestorami i resztą rady, doszło do niezbędnych zmian, aby stacja mogła przetrwać kolejny dzień.

Pozostał jednak nierozwiązany problem z Kalistą.

Kalista. Hackerka skupiła się na wprowadzaniu odpowiednich rzeczy. Wpierw Oficer Naukowy napromieniował trupa dodając mu nowotwór i kilka innych chorób, by wyszło na to, że to nie z ich winy on umarł - po prostu 'się zdarzyło'. Choroby współistniejące. Następnie Hackerka dodała swoje unikalne działania by wprowadzić odpowiednie dane do systemów Stacji.

Tr Z+3:

* X: Kalista niestety drąży sprawę, ale dowody zmanipulowane przez Oficera są wystarczające, by dobrze ochronić Agencję. Śmierć to nie +5 pkt a tylko +2 pkt niegodziwości.
* X: To wszystko stało się nieukrywalne. Kalista ZNOWU wyszła na plus i ZNOWU doszło do ciosu w reputację inwestorów i wierchuszki stacji. Nie bardzo poważnie, ale wystarczająco by było 'my - oni'.
* V: Kalista do niczego ważnego nie doszła. Agencja wykonała swoją pracę idealnie.

I w ten sposób kolejne działania Agencji zadziałały śpiewająco...

## Streszczenie

Na stacji CON Szernief konflikt między savaranami a drakolitami, podsycany przez inwestorów i stymulanty, prowadzi do zagrożenia pryzmatycznego. Dochodzi do manifestacji Esuriit i Alucis. Punktem kluczowym okazała się miłość Rovisa i Joli-09, prowadząca do rozprzestrzeniającego się Snu Ukojenia zwalczającego i zwalczanego przez Nienawiść Esuriit. Lux Umbrarum interweniuje, tworząc antidotum i przesiedlając kochanków. Kryzys został rozwiązany i stacja uspokojona tymczasowo, ale Kalista jest na tropie 'dziwnych rzeczy' a zaufanie populacji stacji do Inwestorów i Rady spada.

## Progresja

* .

## Zasługi

* Klasa Oficer Naukowy: Walter; odnalazł starcie Alucis-Esuriit, zbadał śpiących i stworzył antidotum by móc ludzi wyciągnąć ze Snu. Znajdował rzeczy które do siebie nie pasowały i wyciągał z nich dalsze wnioski.
* Klasa Dyplomata: Ola; wydobyła od Feliny że ta wie gdzie jest Rovis, ułagodziła napięcia na stacji i doprowadziła do miękkiej ewakuacji Rovisa i Joli ze stacji za recykler. Skutecznie zarządziła naprawą sytuacji.
* Klasa Hacker: Mery; zebrała informacje o przyczynie problemów (inwestorzy), wbiła się w dane Kalisty odkrywając jej hipotezy i potrafiła prześledzić ruchy Rovisa jak i nagrania z walki gdy savaranie odbijali Jolę-09.
* Rovis Skarun: przez swoją decyzję o porwaniu Joli-09 wplątał się z nią w relację romantyczną. Jego konflikt z Dorionem ją ujawnił, i gdy uciekał z ukochaną doszło do manifestacji Alucis. Opuścił stację przez Agencję.
* Jola-09 Szernief: savarańska inżynier solarna i artystka, porwana przez Rovisa się w nim zakochała. Gdy Rovis ją osłaniał przed swoimi eks-przyjaciółmi, jej krew posłużyła do ko-manifestacji Alucis. Opuściła stację za sprzęt do recyklingu.
* Klaudiusz Widar: Inwestor; zwolennik pójścia po 'industry', zmienił zasady Con Szernief na bardziej competitive (co zwiększyło zyski) i z NavirMed produkował stymulanty dla drakolitów (większe zyski).
* Damian Orczakin: Inwestor pro-turyzm; chce się pozbyć savaran (i kultystów) ze stacji. On wprowadził paranoizator do stymulantów z nadzieją, że zamieszki. Poszło za ostro, więc potem próbował to neutralizować.
* Felina Amatanir: wie gdzie schował się Rovis i próbuje opanować i utrzymać CON Szernief do kupy, ale ma problemy przez energie magiczne. Współpracuje z Agencją. Mimo problemów, dalej ma zaufanie populacji stacji.
* Kalista Surilik: dziennikarka skupiona na prawdzie i uczciwości, podejrzewa Inwestorów o dodanie czegoś do wody i skutecznie ujawniła niektóre ruchy Agencji. Świetnie zbiera informacje, ma je też spoza stacji.
* Mawir Hong: jeden z bardziej agresywnych drakolitów, pracujący w Energii, niepisany 'szef gangu'. Najmocniej dotknięty Esuriit.
* Dorion Fughar: przyjaciel i rywal Rovisa, który próbuje stać się prawą ręką Honga. Współuczestniczył w ataku na Jolę-09. Pierwszy stał za próbą rozwalenia Rovisa za 'zdradę rasy'. Skończył Uśpiony przez Alucis. Odratowany przez Agencję.
* Szymon Alifajrin: 'praktykant' Agencji, umieszczony w NavirMed; udostępnił dane o stymulantach (trzy strainy). Bardzo nie chce tu być.

## Frakcje

* Con Szernief: savaranie pozyskali zaawansowany sprzęt do recyklingu (dzięki Agencji). Brak wiary że Inwestorzy i Rada wiedzą co robią narasta.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Kalmantis: noktiański sektor pełny dobrobytu, gdzie nie ma może żadnej dobrej planety gdzie da się mieszkać, ale wykorzystywane są arkologie i CON.
            1. Sebirialis, orbita
                1. CON Szernief: 
                    1. Powłoka Zewnętrzna
                        1. Panele Słoneczne: tam podczas naprawy paneli została porwana Jola-09 przez Rovisa i drakolitów
                    1. Powłoka Wewnętrzna
                        1. Poziom Minus Jeden
                            1. Obszar Mieszkalny Savaran: tu schował się Rovis i tu mieszka Jola-09. Zajmuje to bardzo mało przestrzeni jak na populację.
                        1. Poziom Minus Dwa 
                            1. Więzienie: zagnieździła się Pięknościarka Alucis, uspokajając i kojąc więźniów
                        1. Poziom Minus Trzy
                            1. Wielkie Obrady: tam spotyka się Rada w swoim centrum dowodzenia

## Czas

* Opóźnienie: 282
* Dni: 3
