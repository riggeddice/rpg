## Metadane

* title: "Astralna Flara kontra Domina Lucis"
* threads: historia-arianny
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [221130 - Astralna Flara w Strefie Duchów](221130-astralna-flara-w-strefie-duchow)

### Chronologiczna

* [221130 - Astralna Flara w Strefie Duchów](221130-astralna-flara-w-strefie-duchow)

## Plan sesji

### Fiszki
#### 1. Astralna Flara (55 osób max)
##### 1.1. Dowodzenie

* Arianna Verlen: kapitan
* Daria Czarnewik: engineering officer, (4 inż pod nią)
* Alezja Dumorin: eks-kapitan, Orbiter, 
    * ENCAO:  +0--0 |Amoralna, skuteczna| VALS: Hedonism, Face, Power| DRIVE: Wędrowny mistrz Ryu
* Władawiec Diakon: pierwszy oficer, tien, (p.o. Stefana) 
    * ENCAO:  +-0-0 |Intrygancki;;Żywy wulkan| VALS: Hedonism, Self-direction| DRIVE: Follow My Dreams, Korupcja anioła
* Grażyna Burgacz: logistyka i sprzęt (officer), tien (3 osoby pod nią) 
    * ENCAO:  -0+-- |Powściągliwa i 'nudna';;Ascetyczna| VALS: Humility, Tradition| DRIVE: Starszy Brat
* Arnulf Perikas: fabrykacja i produkcja (officer), tien (2 inż pod nim) 
    * ENCAO:  0-0-- |Hardheaded;;Napuszony| VALS: Tradition, Family| DRIVE: Wzbudzenie zachwytu
* Maja Samszar: comms officer, tien (p.o. Klarysy jak K. nie może) 
    * ENCAO:  0+-0- |Moralizatorska| VALS: Stimulation, Conformity >> Power| DRIVE: Długi
* Kajetan Kircznik: medical officer (5 pod nim) (czarny, afro, paw, augmentacje bio)
    * ENCAO:  +-0+0 |Anarchistyczny;;Stabilny emocjonalnie;;Z poczuciem humoru| VALS: Self-direction >> Power| DRIVE: Jestem unikalny

##### 1.2. Operacja

* Klarysa Jirnik: artillery, (p.o. Mai jak Maja nie może) 
    * ENCAO:  00+-- |Wymagający;;Małostkowy| VALS: Tradition, Conformity|DRIVE: Sprawiedliwość
* Tomasz Ruppok: starszy mat (13 osób + 2 starszych)
    * ENCAO:  +0--- |Mało punktualny;;Kłótliwy;;W przejaskrawiony sposób okazuje uczucia| VALS: Hedonism >> Achievement, Family| DRIVE: Apokalipta
* Rufus Warkoczyk: starszy mat (13 osób + 2 starszych)
    * ENCAO:  0-+-0 |Zarozumiały;;Bezkompromisowy, niemożliwy do zatrzymania;;Zorganizowany| VALS: Self-direction, Power| DRIVE: Odkrycie konspiracji ("ktoś nas sabotuje")
* Marcelina Trzęsiel: inżynier syntezy (pod Arnulfem), pod opieką tien Terienaka
    * ENCAO:  0+0-+ |Małostkowa;;Dramatic shifts in mood;;Różnorodność form| VALS: Hedonism, Achievement >> Security| DRIVE: "Co za tą górą"
* Elena Verlen: pilot / advancer

##### 1.3. Infiltracja / Starcie

* Gerwazy Kircznik: sierżant marine, Orbiter
    * ENCAO:  +0+-0 |Głośny;;Honorowy| VALS: Humility, Face| DRIVE: Lokalny społecznik
* Erwin Pies: kapral marine, Orbiter
    * ENCAO:  -0-+- |Skryty;;Wiecznie zagubiony;;Praktyczny| VALS: Tradition, Stimulation >> Power| DRIVE: Ochraniać słabszych
* Szymon Wanad: marine, Orbiter
* Marcel Kulgard: marine, Orbiter, stary znajomy Arianny
    * ENCAO:  000-+ |Demotywuje innych;;Innowacyjny| VALS: Achievement, Face| DRIVE: Zbudować legacy
* Tomasz Dojnicz: marine, Orbiter
    * ENCAO:  --0+0 |Nie znosi być w centrum uwagi;;Dusza towarzystwa| VALS: Conformity, Hedonism >> Power| DRIVE: Arlekin Maytag
* Szczepan Myrczek: advancer, tien, seilita,
    * ENCAO:  --+0- |Stabilny emocjonalnie;;Zawsze bardzo zajęty| VALS: Face, Achievement, Hedonism >> Self-direction| DRIVE: Zasady są święte
* Mariusz Bulterier: advancer, tien, seilita, sybrianin 
    * ENCAO:  0-+-- |Prostolinijny i otwarty;;Nie kłania się nikomu;;Uroczysty i poważny| VALS: Humility, Tradition| DRIVE: Nawracanie
* Hubert Kerwelenios: advancer, sarderyta
    * ENCAO:  -0+-0 |Kontemplacyjny, refleksyjny;;Kompleks paladyna| VALS: Humility, Tradition >> Stimulation| DRIVE: Odbudowa i odnowa

##### 1.4. Other

* Ellarina Samarintael: Egzotyczna Piękność
    * ENCAO:  +-00+ | Spontaniczna;; Chipper;; | VALS: Power, Hedonism >> Family | DRIVE: Wyciągnąć kogoś z bagna, dobrze wyjść za mąż

#### 2. Athamarein, korweta

* Gabriel Lodowiec: komodor-in-training
    * ENCAO: 0+-+0 |Spokojna fasada;;Kontrolowany przez emocje | VALS: Tradition, Security >> Power, Face| DRIVE: Procedury, poprawność systemu, kaganek oświaty.
* Leszek Kurzmin: dowódca jednostki
    * ENCAO: 0-+++ | Żyje według własnych przekonań;; Lojalny i proaktywny| VALS: Benevolence, Humility >> Face| DRIVE: Właściwe miejsce w rzeczywistości.
* Alan Nierkamin: eks-lokalny przewodnik, advancer
    * ENCAO: ++-0- |Nietolerancyjny, musi być tak jak uważa;;Dokładnie przemyśli wszystko zanim coś powie| VALS: Power, Achievement >> Tradition| DRIVE: wprowadzi Orbiter na Nonarion

#### 3. Nonarion Nadziei (stacja cywilna)

* Leo Kasztop: sprzedawca sekretów na Nonarionie (atarienin)
    * ENCAO:  0-+00 | Intrygancki, polityka;;Nie odracza| VALS: Self-direction >> Stimulation, Tradition| DRIVE: Wygrać w rywalizacji)
* .Franciszek: enforcer Nonariona (atarienin)
    * ENCAO:  +00-- | Lubi rutynę;;Uszczypliwy i zgryźliwy;;Lubi wyzwania | VALS: Achievement >> Security| DRIVE: Komfortowe życie
* .Wojciech: agent Aureliona (faeril)
    * ENCAO:  ---+0 |Bezbarwny, przezroczysty;;Niemożliwy do ruszenia| VALS: Power, Humility >> Tradition | DRIVE: Supremacja Aureliona

#### 4. Domina Lucis

* Sarian Xadaar: pierwszy oficer i dowódca Dominy Lucis (dekadianin) 
    * ENCAO:  0-+00 |Niemożliwy do zatrzymania;;Lojalny i oddany grupie| VALS: Benevolence, Tradition >> Face| DRIVE: Niezłomna Forteca
    * "Victorious, to the end!!!"
* Kirea Rialirat: advancer Dominy Lucis (savaranka)
    * ENCAO:  --+00 |Odporna na stres;;Introspektywna| VALS: Humility, Conformity | DRIVE: Potrzeba samotności i ciszy; problemy z ludźmi i towarzystwem
* Tristan Rialirat: wujek Kirei, lekarz (savaranin); ciężko poraniony kiedyś, powymieniane na prymitywne implanty. Ledwo ludzka forma.
    * ENCAO:  0-+-0 |Brutalny, bezwzględny;;Solidny, twardy;;Zawsze wie co, gdzie i kiedy| VALS: Face, Conformity >> Humility, Hedonism| DRIVE: Dominacja
    * "Uratuję Cię, Kireo, moja kochana iskierko - jak nie dał rady Twój ojciec"
* Axel Nargan: zainfekowany kapitan Dominy Lucis (dekadianin)
    * ENCAO:  00+-- |Agresywny;; Bezpośredni| VALS: Security, Power | DRIVE: Zwyciężyć w sporze
    * "We are immortal"
* Akara Marilinias: inżynier i dziewczyna Franka Mgrota z Orbitera; zainfekowana (żywa broń)
    * ENCAO:  +0--- |Spontaniczna;;Lubi rutynę| VALS: Achievement >> Stimulation| DRIVE: "The Modern End"
    * "Będziecie cierpieć jak cierpię ja"
* 3*serpentis
* 200*"cywil"

### Theme & Vision

* Kampania
    * Problemy kulturowe; Orbiter nie respektuje tego co wypracował Nonarion
        * Ratowanie zniszczonego statku - TAI też potrzebuje pomocy (TAI Mirtaela d'Hadiah Emas)
        * Wyjście na pokład Nonariona kończy się... źle
    * Daria nie ma łatwego powrotu
    * Władawiec corruptuje Elenę
* Ta sesja
    * Domina Lucis - ostatnia ukryta baza Orbitera w terenie
    * Tristania "Let's celebrate the modern end" + "Aphelion"

### Co się wydarzyło KIEDYŚ

* Domina Lucis, support ship noctis dała radę schować się na planetoidzie TKO-4271
    * Sarian Xadaar przejął dowodzenie by utrzymać i wesprzeć siły Noctis; zaprojektował operację "ghost ship"
    * Trzech serpentisów zostało uśpionych, łącznie z wszystkimi ludźmi w fatalnym stanie - w tym ich kapitan (broń biologiczna)
* Domina Lucis dała radę ufortyfikować pozycję i zapewnić dość działań z fissili, wody itp. Ma grazery i blueprinty.
* Z biegiem czasu inne jednostki przestały latać w tym terenie, bo "duchy".
    * Domina Lucis zaczęła polować na statki niewolnicze. Zawsze ta sama procedura - 1 serpentis na pokład, eksterminacja, uwolnienie niewolników, statek -> TKO-4231.

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

* Gabriel Lodowiec nie pozwoli na to, by siły Noctis działały na tym terenie - a na pewno nie jako pełnoprawna baza
    * A już na pewno nie jeśli tam jest Sarian Xadaar, niszczyciel statku medycznego "Optymistyczny Uśmiech"
* Sarian uratuje noktian. Uruchomił wszystkie elementy i surowce. Obudził też kapitana Axela.
    * Atak biologiczny, atak kanonierkami, synteza jednostek które mają uciec z tej bazy.
    * W ostateczności, to jest akcja samobójcza Sariana.

### Co się stanie (what will happen)

* S1: Planetoida odpala pełen reaktor i pełną moc. Xadaar chce zmusić Orbiter do działania zanim wezwą wsparcie. 
* S2: Tristan przylatuje do Kirei
    * Lodowiec nie zgadza się, by byli w jednej celi. Trzeba dokładnie zbadać (Kajetan (med), Gerwazy (mil))
    * Upokorzenie Tristana - wyjęcie implantów. 
    * Tristan rozmawia z Kireą o "Rialirat". "Wrócimy na Rialirat".
* S3: Lodowiec każe Ariannie szybko syntetyzować drony i skanery.
    * Arnulf ma problem ze skanerami, Daria jest potrzebna do adaptacji.
    * "Które obiekty niebieskie w okolicy mają siły noktiańskie? Kto ma tam coś co może być użyte przeciw?"
        * -> 4/11 mniejszych obiektów. Jeden się poddaje. 2/3 osoby, jedna popełnia samobójstwo.      
* S4: Maja Samszar, Grażyna Burgacz "czemu my ich tak traktujemy? Czy tak musimy?" -- Arnulf "tak, bo taki rozkaz"
* S5: pintka z kilkonastoma noktianami ucieka w teren ze sporą ilością śmiecia kosmicznego
    * -> Lodowiec chce przechwycić Athamarein -> Sarian każe wystrzelić MIRV z planetoidy -> Lodowiec chroni Athamarein -> miny orbitalne
        * "Arianno, przechwyć lub zniszcz to!"
        * Arianna bombardowana przez serię małych, mało zgrabnych, samobójczych kanonierek
    * Śmieciowy wahadłowiec "Litość Orbitera" atakuje Athamarein. Dobra załoga (4-5 osób), statek w ruinie
        * Kurzmin uszkadza Athamarein by wyprowadzić ją z tego losu. Plus rakieta za uciekinierami.
        * Kurzmin i Lodowiec chcą żywych na pokładzie wahadłowca.
            * Przeżył kapitan Axel Nargan. Był w 'safe room'. Jest na stymulantach i jest chory.
* S6: Atak serpentisa. W cieniu pocisków, w cieniu ognia leciał na minimalnym cieple do Athamarein.
    * Kurzmin ratuje Lodowca; opuszcza Athamarein. Nie może wyłączyć jednostki, ale jest w stanie wymanewrować.
    * ratujemy Athamarein.
* S7: Tristan zabija Kireę, by jej oszczędzić losu na Orbiterze.
* S8: Magowie przetrwają PLAGĘ. Kurzmin też - ma ciało viciniusa.

### Sukces graczy (when you win)

* przetrwanie i wycofanie się

## Sesja właściwa
### Wydarzenia przedsesjowe

.

### SPECJALNE ZASADY SESJI

.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Arianna ma savarankę w celi. I ma problem - co powiedzieć Lodowcowi z tego co powiedziała Kirea. Arianna decyduje się podzielić WSZYSTKIMI informacjami z komodorem. On chce mieć wgląd w informacje, więc Arianna nie chce ryzykować. Gabriel ma proste spojrzenie na sprawę. (K - Kurzmin)

* G: Xadaar zniszczył "Optymistyczny Uśmiech" podczas wojny. To była najprostsza droga ucieczki i ją wybrał niszcząć statek medyczny.
* A: Zależało mu na ewakuacji jego ludzi
* G: NOKTIANIE nie niszczą statków medycznych. Myśleli, że nie żyje. Mogę musieć wezwać posiłki - Xadaar nie może stąd uciec.
* A: Noktianie dadzą radę się wycofać, wycofajmy się by wezwać pomoc. Myślę, żeby grać na czas jak to możliwe - wsparcie. Niby mają tylko jednostkę wsparcia, więc mają broń na planetoidzie, ale rozwalili statki z tych regionów. Może to nie nasza klasa, ale...
* G: Jak udało im się zdobyć statki bez... śladu. Bez uszkodzeń. Nie warto atakować ufortyfikowanej noktiańskiej pozycji
* A: Mamy zakładnika i powiedzieliśmy im o tym, możemy spróbować wejść w rozmowy i odesłać zakładnika jako mediatora. Niech zakładnik zobaczy jak potężny jest Orbiter, neich porzucą nadzieję.
* G: Orbiter zmiażdżył Noctis. Oni wiedzą jak groźni jesteśmy.
* A: Ona uważa, że albo oni albo my zginiemy - ich ostatnia pozycja
* G: Dlatego wycofujemy. Kapitan Verlen (uśmiech), nie zamierzam atakować ufortyfikowanej noktiańskiej pozycji. To jest głupota. Ale nie pozwolę, by ktoś UCIEKŁ. 
* G: Odeślij mi zakładniczkę, chcę pokazać Centrali prawdziwą noktiankę i zdobyć posiłki. Nie możemy jej uwolnić. Ta noktianka przeżyje, Arianno. Jej nie stanie się krzywda. Jeśli ją odeślemy, to zginie.
* A: Wie pan, komodorze, jak są straty w cywilach to jej rodzina może zginąć. 
* G: Spróbujmy zaproponować by ktoś z jej rodziny kto przeżył do niej dołączy. By nie była sama i nie zginie.
* (Daria offscreen): a może zamieszkają w domenie Orbitera, zapewnimy bezpieczny transport itp. Transport i fundusz na start. Rozłam w obozie przeciwnika. I ludzie mają opcje.
* A: Możemy im zaproponować że niezależnie od wyniku cywile mogą opuścić ten teren. Odeskortujemy ich po walce.
* G: Tak, ALE. Ci ludzie mogą opanować Flarę, więc to muszą być ich stateczki. I nasi marines muszą opanować.
* Gabriel przesłał Ariannie informacje o Sarianie Xadaarze. W danych okazało się, że:
    * Xadaar był pierwszym oficerem Dominy Lucis. Ale kapitanem był Axel Nargan.
        * Xadaar był tym, kto wydał rozkaz zniszczenia "Optymistycznego Uśmiechu". Nargan był ciężko ranny i był niedysponowany.
* A: Odeślijmy noktiankę. On uważa Orbiter za potwory. Zbombardujemy cywili.
* G: Dobry pomysł.
* K: Z całym szacunkiem, to się nie uda. Sam bym mógł zaproponować oddanie JEDNEGO cywila by zniszczyć GRUPĘ cywili.
* A: Wraz z nią prześlijmy informacje - ocaliliśmy noktian z jednostki niewolniczej i odesłaliśmy na Nonarion
* K: Przepraszam, kapitan Verlen, ale jako adwokat diabła; odesłaliśmy ich jako okrutny żart. Na Nonarionie ZNOWU będą zniewoleni z tego co mówiła oficer techniczny
* A: Weźmy ich stamtąd
* G: To byłby dobry pomysł. Ale nie możemy się rozdzielić. Uciekną.
* A: Nie sądzę, że damy radę. Raczej nastawi się na atak.

(planowanie)

Ogólny plan jest taki:

1. Budujemy sondę i ta sonda opuszcza ten teren by sygnał relay szedł do Orbitera. Mówimy o Xadaarze i o problemach z bazą noktiańską. Chcemy wsparcia.
2. Budujemy KOLEJNĄ sondę i ta leci na Nonarion - by ktoś przywiózł noktian których oddamy. Czyli 'bounty od żywych i zdrowych noktian których oddajemy swoim'. Daria pomoże sformułować by było wiadomo o co chodzi.
3. Wysyłamy sygnał na stację, że do Keiry może dołączyć ktoś z rodziny jakby miało dojść do masakry, by nie została sama na świecie. I ten sygnał formułuje Daria i Arianna.
    * i czyta to Ellarina. Która prezentuje to z Keirą.

By Xadaar nie wszedł w last stand. I by oczyścić pole bitwy przed walką. By cywile nie cierpieli. A najlepiej uniknąć walki.

Podczas tych rozmów Kurzmin powiedział, że coś się dzieje - sygnatura cieplna i energetyczna planetoidy wzrosła. Pełna moc wszystkich fabrykatorów.

* Kurzmin "panie komodorze, te planetoidy dookoła tej głównej. Tam może coś być. Lub ktoś."
* Lodowiec "skanujemy. Dokładny skan, znajdźmy co i jak."

Athamarein skanuje mniejsze planetoidy - co tam jest i jak.

Tr Z(manewr, czas, wszystko)+3:

* X: Athamarein musi się przesuwać do skanowania. Czyli Flara też. Czyli da się przewidzieć ich lokalizację. 
    * Nieregularne skanowanie, czyli WOLNIEJSZE.
* V: Athamarein wykryła "coś" w 4/11 planetoid. Tam coś jest.
    * Ktoś kto chce atakować planetoidę główną ma preferowane miejsce ustawienia - tam mogą być miejsca ochronne.
    * Może 'materiały wybuchowe'. Przygotowane pod zaatakowanie?
* Vr: Arianna zasugerowała skan pod materiały wybuchowe:
    * 2 planetoidy mają 3 + 3 osoby. 2 inne mają materiały ORAZ działka. I po jednej osobie. Plus to samobójcze akcje.
        * Lodowiec się zasępił. "Nie obejdzie się bez rozlewu krwi i masakry noktian. Trudno."
            * Kurzmin się skrzywił na "trudno" Lodowca.
* Vr: Kurzmin kazał skanować dokładniej. I dokładniej. I DOKŁADNIEJ.
    * K: "Komodorze, w śmieciu dookoła planetoidy są miny. Nieaktywne."
    * G: "Tego nam brakowało. Nie możemy tam się w ogóle zbliżyć. Po co zatem planetoidy z ludźmi w środku skoro i tak są miny?"
    * A: "Odwrócić uwagę"
    * G: "To jest zimne. Stracić kilka osób tak po prostu... bezsensowna śmierć."
    * A: "Jeśli strata kilku osób pozwoli przeżyć kolonii... na pewno tak myśli Xadaar. Widział pan podejście Keiry. Dziewczyna chciała się rzucić na piłę tarczową."
    * G: "Tak, ale DLACZEGO?"
    * A: "Przyjemniejsze od tortur..?"
    * G: (niesmak) "Jak ja nie lubię tych noktian... ludzkie życie ma WARTOŚĆ."

Gabriel jest zwolennikiem "Pax Orbiter". Kurzmin też, ale jest bardziej humanitarny.

* K: "panie komodorze, ta noktiańska baza jest niegroźna. Oni nie wykonywali operacji przeciwko nikomu."
* G: "kapitanie Kurzmin. To jest noktiańska baza na terenie Orbitera..."
* A: "...poza terenem Orbitera właściwie..."
* G: "...NA TERENIE ORBITERA który tymczasowo jest leasingowany grupom takim jak Nonarion. Oni liczą na naszą ochronę. To NASZA wina, Kurzmin, Verlen. Nie mamy ludzi ani sprzętu. I masz potem - niewolnictwo, neikatiańskie TAI, wszystko. Zagrożenia. I my - Orbiter - zawodzimy. Ta noktiańska baza jest dowodem naszej porażki."
* A: "jeśli pokojowa noktiańska baza jest powodem porażki to jakie są powody do radości?"
* G: "(you've got to be f-ing kidding me) kapitan Verlen. Oni tylko dlatego są pokojowi, że nie mają czym nas zniszczyć."
* A: "są daleko od Orbitera i nie musieli nas zniszczyć?"
* G: "ich bohaterowie to nasi zbrodniarze i vice versa."

Daria i Arianna opracowują sygnał, którym przekonają noktian, że im nic teoretycznie nie grozi. Daria mówi z serca "jest opcja życia inaczej. Orbiter jest jaki jest, ale jeśli jesteście zwykłymi non-combatants to da się jakoś żyć i zintegrować; pracowałam z noktiańskim advancerem a mój kapitan był eks-marine Orbitera. Więc jak opuścicie stację, nic Wam nie grozi i macie szansę na nowe życie. A to czy będzie dobre zależy tylko od Was."

Ellarina to opowie wraz z Keirą, która stoi tam niepewnie jako props. I nikt nie kazał jej się uśmiechać. Kirea powiedziała (cichutko) że dostała obiad i był smaczny. I że Orbiter ma dużo więcej niż 50 statków.

Tr Z (Kirea + Ellarina) +3 +3Or:

* Vr: pobieżne siły noktiańskie odpowiadają na wezwanie. "niezdecydowani".
* Xz: Orbiter skutecznie manipuluje - "twarda" frakcja jest jeszcze bardziej twarda. Ale nie tak by skrzywdzić noktian.
* Vz: nie-ekstremistyczne siły są w chaosie i niepewności. Po co ta wojna? Przegrali. Może pogodzić się z nowym światem.
    * EKSTREMISTYCZNE siły uważają, że trzeba chronić noktian.
* Vr: "life will be never be the same again". Baza noktiańska nawet po pełnym sukcesie nie osiągnie już tej harmonii co kiedyś. Orbiter po prostu przyleci większymi siłami.
    * z perspektywy noktian "to nie jest walka o obronę a o jak najlepsze warunki kapitulacji lub życia"
    * eksterminacja Orbitera niczego nie rozwiąże

Sondy. Daria konstruuje sondy używając materiałów oraz resztek Isigtand. Plus zna teren, wie gdzie i jak użyć relay w Planetoidzie Kazmirian. Używa skanów z Athamarein by umiejscowić by nie było łatwo umiejscowić czy zestrzelić. Maja pomaga w budowie sygnału.

TrZ (wsparcie, wiedza) +3

* Vz: sondy uda się zbudować
* X: Lodowiec jest PRZEKONANY że się udało właśnie dlatego bo to jest na rękę Darii a nie element jego planu.
* V: wyszło SZCZEGÓLNIE dobrze i sprawnie. Lepiej niż ktokolwiek by się spodziewał + znajomość terenu. To dowód na (X) Lodowca.
* Xz: faktycznie, doszło do utraty kontroli nad sondami po operacji. Ale sondy działają dalej i RETRANSMITUJĄ to co się stało. Czyli sprawa z tą noktiańską bazą stanie się PUBLICZNA. To znaczy że jeśli Orbiter dotrzyma słowa, część noktian wyjdzie spod ziemi. Jeśli nie, noktianie wiedzą. Ogólnie, Nonarion zobaczy siłę Athamarein (lub dyplomacji). A dodatkowo - okaże się że "te duchy" to był noktianin Xadaar. Co daje noktianom silną sugestię poddania się Orbiterowi...

Czas na reakcję drugiej strony. Jedna z planetoid wysłała sygnał na Athamarein. Poddają się. I Gabriel ma problem. "jak ich wziąć - przecież nam wybuchną przy advancerze". W końcu - pintka. Samosterowana pintka wzięła dwie osoby. Trzecia popełniła samobójstwo.

Arianna widzi zdegustowaną minę Gabriela, że jeden noktianin popełnił samobójstwo. Zdaniem Darii, to tylko pokazuje reputację Orbitera. 

Zaraz potem z planetoidy wyskoczył w kosmos noktianin w skafandrze z plecakiem odrzutowym. Leci w kierunku Athamarein. Nie ma szans dolecieć szybko. W kapsułę go. TAK to jeden noktianin, na oko, silnie zcybernetyzowany. Flara kontynuuje operację budowy "nowej lepszej klatki" a noktianin w "kiepskiej klatce" czeka grzecznie na dalsze działania. Kurzmin się tym zajmuje, normalna procedura. Całą operację podjęcia i obsługę transmitujemy na żywca - Orbiter nie ma nic do ukrycia.

* K -> A: "Kapitan Verlen, pomożesz mi?"
* K -> A: "Nowy noktianin - Tristan - ma >50% implantacji i trzeba to wyjąć (komodor kazał). Pomożesz z humanitaryzmem?"

Arianna + Daria chcą zobaczyć tego noktianina. Brzydki. Składa się z kantów. Ponad 50% ciała zniszczone. Mało energii. Savaranin który miał wypadek i ma plebsiaste implanty. Sęk w tym, że podlega pod regulacje cyberkomandosa (ciało stanowiące broń). A savaranin ma ciało które może pełnić rolę broni - ma zamiast ręki mechanizm itp. A to jak zabrać inwalidzie wózek XD.

Daria chce pokryć z własnych pieniędzy jakieś implanty dla niego - i to proponuje Ariannie.

* G: "Kapitan Verlen? Dawno się nie widzieliśmy."
* A: "Na czym stanęła rozmowa zanim przyszłam"
* G: (podniósł brwi) "Widzę, że kpt Kurzmin potrzebował wsparcia moralnego. Tak, zdejmujemy implanty i przestajemy retransmitować."
* D: "Strzelamy sobie w kolano, bo nic nie jest ukryte"
* A: "Trochę tak"
* G: "Nie pozwolę cyberkomandosowi noktiańskiemu wejść na jednostkę wsparcia."
* ...

Gabriel oddał Ariannie dyskusję z savaraninem. Nie może "cyberkomandos ze wszczepami" wejść na Flarę. Kurzmin przeprosił prywatnie Ariannę. Nie o to mu chodziło. On organizuje operację rozłożenia noktian by dało się ich ewakuować i nic im się nie stało. Zdaniem Kurzmina około 50 osób się podda. W swoich jednostkach + tym co Flara zbuduje. Wiadomości z bazy. Acz nie rozmawialiśmy z dowodzeniem. Dowództwo Noctis jest milczące.

Orbiter przyśle jednostki. W ciągu niestety co najmniej doby. Lodowiec dostał rozkazy "nie pozwólcie Xadaarowi uciec." Czyli zbrodniarze wojenni - aresztowani, zabici lub zostają w tej bazie aż pojawi się wsparcie. I Lodowiec zaprosił Ariannę i Kurzmina.

* G: "Jakieś sugestie?" (lekko zrezygnowany) "Nie chcę łamać słowa. Nie zrobimy krzywdy tym noktianom. Ale chyba musimy zażądać poddania lub grać na czas. Oni coś wymyślą."
* K: "Nasze miny, rozstawić dookoła terenu"
* A: "Gorzej jak uciekający cywile wpadną"
* G: "To niech nie uciekają"
* K: "Miny, rozstawienie... zajmie sporo czasu. Plus jak to zrobić."

Rozpoczynamy plan "rozsiewamy śmieci". Czyli - dodawanie dużej ilości śmieci kosmicznych by mała jednostka nie mogła prześlizgnąć się tam gdzie Was nie ma. Noktianie przez to, że część się poddała nie mogą np. strzelać MIRV - bo zniszczą "swoich". Gdy tylko Flara zaczęła rozsiewać śmieci, pojawił się komunikat ze strony Dominy Lucis.

Na mostku siedzi kapitan. W pełnym rynsztunku. Powiedział, że to jest akt wojny i że będzie musiał odpowiedzieć. Orbiter NIE wygra.

Komodor zwołał spotkanie.

* G: "Rekomendacje..."
* K: "Mamy rozkazy. Musimy zaatakować albo... nie wiem jak inaczej ich zatrzymać."

Gabriel Lodowiec włączył transmisję na Dominę Lucis.

"Pozwólcie, że Wam coś pokażę" - i tu puścił nagranie nekroTAI (Zarralea). Pokazał czym ona jest. Nie pokazał jak powstała. Lodowiec pokazał pełnię satysfakcji, gra. ON GRA. ON ODGRYWA. "Mamy sposób, jak przekształcić TAI w agenta który będzie nam lojalny i będzie nam służył. Żywy lub martwy, BĘDZIESZ nam służył. Chyba, że. Jest to technologia Orbitera, która jest zakazana i nie jest używana. Ale - jak sam powiedziałeś, kapitanie Nargan - jesteśmy w potencjalnie stanie wojny. I jeżeli spróbujesz zranić Orbiter lub nie podporządkujesz się - będziesz służył nam po drugiej stronie. Ci, którzy się poddali, są bezpieczni. Ci, którzy nie atakują Orbitera - są bezpieczni. Ci, którzy próbują zniszczyć Pax Orbiter... kto lepiej pomoże w ich zwalczaniu niż Wy? Więc - gwarantujemy że nic się nie stanie tym co opuszczą bazę."

* N: "Interesujące. Orbiter jednak jest w stanie jest w stanie upaść niżej niż myślałem."
* G: "Zdesperowani ludzie idą w desperackie metody. Użyliśmy tego pierwszy raz teraz. Nie zmuszajcie mnie do użycia ponownego."
* N: "Jakie są wasze warunki?"
* G: "Xadaar i Ty oraz inni zbrodniarze wojenni noktiańscy zostają aresztowani i osądzeni. Żaden noktianin, który nie popełnił zbrodni wojennych a tylko, no, walkę - nic się nie stanie. Jakaś forma integracji."
* N: (przez twarz przeszły mu emocje) "Tym razem wygraliście. Nie potrafię wyobrazić sobie takiego poziomu zła poza Saitaerem."
* N: (po krótkiej pauzie) "Wrócę do Was za godzinkę. Upewnię się, że nikt więcej nie chce się poddać."

Lodowiec i ekipa. Kurzmin ma niepewną minę. Lodowiec do niego "Kapitanie Kurzmin, dzięki temu zarówno chronimy cel misji jak i minimalizujemy straty wśród ludzi. Zarówno naszych jak i ich. A to była kierunkowa wiązka - to nie tak, że wszyscy noktianie którzy się poddali będą wiedzieli co im powiedziałem." Kurzmin uważa, że noktianie wysadzą bazę i problem jest rozwiązany.

Przy okazji, doszło do bardzo smutnego incydentu. Savaranie znajdowali się w zabezpieczonym kontenerze, byli razem. Tristan i Kirea. Kirea położyła głowę na ramieniu Tristana, po czym Tristan jednym szybkim ruchem zabił Keirę. Następnym krokiem była dehermetyzacja kontenera, by zginąć wraz z "krewną". Orbiter ich nie złapie. Przybył uratować Keirę i uratował Keirę.

Znowu, 30 minut później.

* N: "Komodorze, czy przyjmiesz kolejnych 23 poddających się noktian?"
* G: "Oczywiście. Kapitanie, wiem, że to trudna decyzja. Uhonorujemy ich status. Nie stanie im się krzywda."
* N: "Cieszę się. Osobiście, nie wierzę Wam. Ja rekomendowałem by walczyli do końca, ale to ich decyzja. Spotkamy się w piekle po tym, jak odbierzecie noktian. Przylecą nieuzbrojonymi statkami."
* G: "Wasza walka z nami nie ma żadnego sensu."
* N: "Moim ludzie są lojalni. Spotkamy się w piekle."

Kurzmin ma propozycję - on wie że to samobójczy atak, więc jak coś się dzieje, WIEJEMY! Poza zasięg. Lodowiec zauważa, że jeśli atak jest powiązany z próbą ucieczki kogoś, to właśnie wszystko stracone. A na to Arianna, że jakby im wsadzić nekroTAI, to nic im nie zadziała. Więc - przechwycić ostatni statek z tymi co się poddają i zbliżyć nekroTAI do systemów Dominy Lucis. Teraz nawet Lodowiec ma niepewną minę - on nie chce łamać słowa. To miała być honorowa walka. Zdaniem Arianny, chodzi o ilość żyć (najbardziej) i wykonanie zadania (pośrednio).

Tr Z (Arianna po prostu ma rację) +2:

* Vz: Lodowiec pójdzie za tym planem. Ale on jest osobą uczciwą, więc odda Ariannie honor za wymyślenie...
* X: Lodowiec jest komodorem i osłoni Ariannę po tym jak będzie fallout. Nie odda tego honoru. Ale Elena będzie wiedzieć i ONA Ariannę opieprzy. Rift.

Operacja "zdrada" rozpoczęta. Trzy statki noktiańskie, niewielkie, każdy na pokładzie ma ~8 osób. Statki przejęte przez marines i faktycznie, żadnych niespodzianek. Jeden ze statków dostał Zarraleę i ma za zadanie wrócić. A Zarralea bierze na pokład zwłoki savaran i komunikuje się JAKO ONI z centralą. Oni chcą umrzeć razem z noktianami.

ExZ (savaranie są cenieni) +3: 

* X: nie ma zaufania ze strony noktian
* Vr: Zarralea uniknęła pocisku MIRV i się zbliżyła
* Vr: Zarralea wbiła się w system siłą syreny. Samozniszczenie nie jest możliwe. Wszystkie TAI i systemy analogiczne są wyłączone, zahipnotyzowane śpiewem
* X: jest tam ten jeden agent który jest niewrażliwy...
* V: Zarralea przetrwa.

Jest na stacji jeden agent niewrażliwy. Zarralea dała radę wysłać sygnał zanim się musiała "odciąć" by agent jej nie dorwał. Serpentis. Serpentis nie był w stanie zniszczyć Zarralei, nie był też w stanie zrobić większości rzeczy które by chciał. Ale mógł dokonać egzekucji wszystkich noktian łącznie z atomizacją ciał, a potem samobójstwo własne. Bardzo smutny koniec ostatniego serpentisa Dominy Lucis.

Dla Arianny i Darii to był pewien szok - nie spodziewały się aż TAKIEJ reakcji na Orbiter.

## Streszczenie

Lodowiec dostał rozkazy pojmać / zabić dowódcę Dominy Lucis. Użył nekroTAI by odebrać resztkę nadziei noktianom (you will serve alive or dead) i jakkolwiek sporo noktian się poddało, główni popełnili samobójstwo. Orbiter zajął Dominę Lucis i Strefę Duchów, acz kosztem reputacji. Ogromny, bezkrwawy sukces wzmacniający Pax Orbiter dzięki propagandzie i Zarralei.

## Progresja

* Gabriel Lodowiec: przemowa o nekroTAI i że noktianie BĘDĄ służyć, żywi lub martwi, w imię Pax Orbiter. Uszkodził poważnie reputację Orbitera i swoją, ale osiągnął sukces - zdobył bezstratnie Dominę Lucis.

### Frakcji

* Orbiter: w Obłoku Lirańskim jest uważany za byt przerażający i absolutnie bezwzględny, zwłaszcza po retransmisji Darii. NekroTAI, Ograniczanie neikatiańskich TAI itp. Pax Orbiter znajduje coraz większą pulę wrogów.

## Zasługi

* Arianna Verlen: dzieli się wszystkimi informacjami od Kirei z Lodowcem; wszystko planuje nie pod kątem honoru czy bezpieczeństwa a uratować jak najwięcej istnień ludzkich. Wkręca Lodowca w mowę mającą doprowadzić do poddania się noktian lub samobójstw noktian, ale by minimalna ilość ludzi ucierpiała. Humanitarna socjopatka - nie chce niczyjego cierpienia, ale ratowanie istnień przede wszystkim.
* Daria Czarnewik: wraz z Ellariną opracowuje sygnał mający za zadanie przekonać noktian do poddania się. Retransmituje sygnał z Orbitera o NekroTAI i wielką mowę Lodowca by Nonarion miał dowody jak groźny i bezwzględny i niemoralny jest Orbiter a zwłaszcza Lodowiec.
* Gabriel Lodowiec: dostał z Orbitera za zadanie unieszkodliwienie Xadaara; nie jest fanem samobójstw noktiańskich, ale zrobił przemowę której celem było ich zastraszyć przez nekroTAI. Doprowadził do tego, że udało się uniknąć walki z noktianami. Koło 200 'uratowanych', koło 40 popełniło samobójstwo. Zniszczył swoją reputację i uszkodził reputację Orbitera wśród lokalsów. Ale wygrał.
* Leszek Kurzmin: logistycznie przeprowadził operację poddawania się noktian, by na pewno nie ucierpiał nikt niewinny i nie dało się zniszczyć Flary ani Athamarein. Wykonywał wielokrotne skany aż doszedł do tego, że teren jest silnie zaminowany. Wiedział, że Domina Lucis nie będzie bez defensyw.
* Sarian Xadaar: KIA; zniszczył "Optymistyczny Uśmiech" (medical Orbitera) podczas wojny; dekadianin; przebudził kpt Nargana w obliczu Zarralei i Orbitera. Zginął z ręki serpentisa by nie wpaść w ręce Orbitera.
* Axel Nargan: KIA; dekadianin i kapitan Dominy Lucis; dobry strateg, ale w obliczu nekroTAI i "służenia żywy lub martwy" nie był w stanie wiele zrobić. Pozwolił odejść noktianom którzy chcieli, potem w obliczu Zarralei dał się zabić serpentisowi.
* Tristan Rialirat: KIA. Savaranin. Przybył na Orbiter się poddać by być z Kireą ("krewną"). Zabił ją i siebie by nie wpadli w ręce Orbitera.
* Kirea Rialirat: KIA. Element propagandowy dla Ellariny by zmusić noktian do poddania się. Z radością zobaczyła Tristana - krewnego. Zginęła z jego ręki, by ją ratować przed służeniem w imię Orbitera.
* Ellarina Samarintael: przeprowadziła akcję propagandową (używając Kirei jako propsa) pokazującą noktianom, że warto się poddać bo nie mają jak obronić tego co mają teraz jak Orbiter o tym wie. Zresztą bardzo skutecznie.
* NekroTAI Zarralea: jednoosobowo weszła na TKO-4271, syrenim śpiewem unieszkodliwiła ludzi i wyłączyła selfdestruct. Serpentis nie był w stanie się do niej dostać; musiał terminować wszystkich noktian na Dominie Lucis.
* OO Astralna Flara: konstrukcja serii sond i skanerów, by zrozumieć niespodzianki Strefy Duchów. Konstrukcja containment chambers do złapania noktian. Konstrukcja transmiterów itp. Ogólnie, potężna jednostka konstrukcyjna pełnej mocy by nie dało się ze Strefy Duchów uciec ani jednemu noktianinowi.
* OO Athamarein: ostrożnie skanuje okolicę Strefy Duchów i wykrywa miny i inne noktiańskie niespodzianki; przygotuje odpowiednie kąty rażenia, by nikt nie dał rady się wycofać z Dominy Lucis.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Libracja Lirańska
                1. Anomalia Kolapsu, orbita
                    1. Strefa Upiorów Orbitera
                        1. Planetoida Lodowca: kiedyś TKO-4271; znajduje się tam Domina Lucis i jest przygotowana całkiem dobra baza Noctis. Lodowiec dał radę ją zdobyć bez jednego wystrzału dzięki bezwzględności i podstępowi.

## Czas

* Opóźnienie: 1
* Dni: 2

## Konflikty

.
