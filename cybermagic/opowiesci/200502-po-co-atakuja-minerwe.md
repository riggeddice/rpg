## Metadane

* title: "Po co atakują Minerwę?"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [200425 - Infiltrator poluje na TAI Minerwy](200425-inflitrator-poluje-na-tai-minerwy)

### Chronologiczna

* [200425 - Infiltrator poluje na TAI Minerwy](200425-inflitrator-poluje-na-tai-minerwy)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* .

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

* Od czasu do czasu Minerwa pomagała w naprawie i usprawnianiu pomniejszych cywilnych Technologicznych AI w Zaczęstwie i okolicach.
* Technożercy są wielkimi przeciwnikami zaawansowanych magitechów, a już w ogóle TAI. Zakupili customizowaną zaraźliwą klątwę od Auhag.
* Klątwa rozprzestrzenia się po Zaczęstwiackich TAI a Technożercy robią wszystko co w ich mocy by winną okazała się Minerwa.
* Wysłali kupionego od Auhag Bio-Infiltratora by tam gdzie Minerwa robiła korekty i naprawy, tam dochodziło do katastrofy.
* TAI "Strażniczka" jest zmartwiona aktualnym obrotem sytuacji. Co gorsza, jej psychotronika jest ograniczona z uwagi na reakcje Cyberszkoły na Trzęsawisko.

## Punkt zero

.

## Misja właściwa

Po wyciągnięciu Minerwy, Pięknotka powiedziała jej, że Minerwa musi dojść do tego jak działa ta "klątwa AI". To co psuło jej TAI. Kto to zrobił, kto jest autorem czegoś takiego. Akurat jak chodzi o psychotronikę zmilitaryzowaną, Minerwa się na tym solidnie zna.

* V: Minerwa zna nazwisko. Jest to Feliks Mirtan, kiedyś w Orbiterze i nie powiązany z Astorią - teraz żyje w Aurum jako sprzedawca zmilitaryzowanych TAI. Warte, bo Minerwa i Pięknotka mają dowód - to nie chodziło o Minerwę (z TAI).

Czyli to, co uszkadzało AI nie jest celowane w Minerwę (acz jakkolwiek nie jest to dedykowane, ale Feliks powinien mieć rekordy co komu sprzedawał). Pięknotka chciałaby wiedzieć skąd jest ten Infiltrator, ten komponent biologiczny, ale nie chce przekazywać tego do Pustogoru - a nuż jej zabiorą pajęczaka, bo Tukan bruździ. Lekko nieszczęśliwa, bo nieco bez środków, Pięknotka pojechała do przedstawiciela pomniejszego biznesu - diakońskiej hodowli ludzi. Do Kreacjusza Diakona.

A akurat Kreacjusz i Pięknotka są spokrewnieni. Po krótkiej, luźnej rozmowi przeszli do konkretów. Pięknotka dała Kreacjuszowi uszkodzonego i niesprawnego Infiltratora. Kreacjusz przyjął Infiltratora z radością. Z przyjemnością przyjmie go jako zapłatę za swoje usługi :D. Zabrał się do badania Infiltratora, zarówno na poziomie biologicznym jak i programowania. (Tr)

* V: Typowy Infiltrator którego da się kupić z kartelu Aurum - mafii Aurum, której do tej pory nie było
* VV: Nie jest szczególnie utajniony; jest napędzony przez śmieci od Minerwy. Ktoś zabrał jej śmieci, jej chusteczki... pod tym kątem zebrał informacje o jej unikalnym Wzorze magicznym opartym o ixion. To jest kalibrowane by uszkodzić AI z tym konkretnym wzorem. Ale - zdaniem Kreacjusza - to zadziałało TYLKO przez przypadek. A mianowicie, gdyby Minerwa była zwykłym magiem, to to nie byłoby wystarczające dla tego Infiltratora.

Wróg Minerwy jest albo niekompetentnym magiem albo wie dużo więcej o ixionie niż Pięknotka, Kreacjusz itp. Co więcej, infiltrator "pamięta" gdzie był, mniej więcej.

Na prośbę Pięknotki, Kreacjusz włączył swoje laboratoria by zregenerować Infiltratora. By dowiedzieć się gdzie to było i o co chodzi. Odwrotne programowanie - a on jest kompetentny w tym temacie.

* X: Infiltrator po skończonej akcji się rozpłynie we mgle; zniknie. Coś będzie robił, nie przeciw Minerwie.
* VM, VZ: Infiltrator jest zregenerowany. Uzupełnienie tego zabicia działają lepiej niż wcześniej, jest naenergetyzowany i nieco niestabilny, ewoluuje. Ale - w dyskretny sposób może doprowadzić Pięknotkę (i opcjonalnie Kreacjusza) do miejsca gdzie został skonfigurowany - a nawet może osób.

Kreacjusz oddał Pięknotce kontrolę nad Infiltratorem. Będzie słuchał jej poleceń, ale potem chce go z powrotem. Infiltrator skupił się na Podwiercie - więc tutaj znajduje się źródło, tu został "zaprogramowany" przeciw Minerwie. Pięknotka się uśmiechnęła - kto Infiltratorem walczy, od Infiltratora ginie. O dziwo, Infiltrator idzie w kierunku na Sensoplex - czyli tam ktoś skonfigurował tą istotę. Pięknotka poszła za nim. W jednym z tuneli Sensoplex Pięknotka zobaczyła ślady - rzeczy od Minerwy. Śmieci, rzucone na kupkę. Infiltrator jest też w stanie iść do osoby, która była obecna podczas rytuału. 

Pięknotka poszła więc za Infiltratorem, który doprowadził ją do Osiedla Tęczy w Podwiercie. Tam pajęczak poszedł bezbłędnie do jednego z bardziej zapuszczonych domów i przez okno do łazienki wszedł do środka. Po chwili Pięknotka usłyszała głośny krzyk kobiety. Ktoś jest pod prysznicem i pajęczak wlazł przez okno. Krzyk. Po chwili odgłos kilku strzałów. "Zabij to!!". I ktoś... zabił pajęczaka (a dokładniej: uszkodził rdzeń kontrolny; autonomiczny pajęczak sam sobie gdzieś odszedł).

Pięknotka formuje usypiający granat magiczny i wrzuca go przez okno. (TpM). Wszyscy są nieprzytomni. Niestety, czar Pięknotki wpłynął też na ściany budynku - wydzielają też to usypiające coś. To miejsce będzie bardzo letargiczne. A Pięknotka ma gwarancję, że w środku wszyscy są nieprzytomni.

Pięknotka założyła maskę przeciwgazową i wbiła, rozwalając drzwi. Tam - naga kobieta (prysznic, uśpiona) i silny młodzian, powiązany z gangiem Technożerców. Nie są bogaci; gość miał pracę, ale stracił ją do tańszych TAI. Pięknotka związała go jak wieprzka i zaniosła do Sensoplexu, zostawiając dom z rozwalonymi drzwiami otwarty (acz zostawiła przymknięte). Gość jest w Sensopleksie, związany, z workiem na głowie. Pięknotka pomogła mu się ocknąć. Potem wydzieliła magiczne serum prawdy by wszystko jej powiedział. (MTp).

* V: Powiedział jej wszystko: to Technożercy chcieli zemścić się na technologiach ale nie wiedzieli jak. Więc kupili Infiltratora i skupili go na Minerwie, bo jest najlepszym magiem od tego. Ich szef zna szczegóły. A szefem - jak powiedział - jest Halina Sermniek.

Pięknotka kojarzy Halinę Sermniek - akurat ONA jest czarodziejką. Tylko, że Pięknotka nie miała pojęcia o korelacji Halina - Technożercy. Halina raczej była bliżej Kreacjusza. Jest kimś w stylu społeczniczki, co w ogóle nie pasuje do tępienia Minerwy. Faktycznie, dane z infiltratora pokazują, że ona tam mogła być. Ale... to nie dowód pod względem prawa. Tyle, że nikt tego nie wie.

Pięknotka zdecydowała się odwiedzić Halinę, gdy ta jest sama. Halina przeprowadziła się do Osiedla Tęczy w Podwiercie - tam raczej mieszkają ubożsi. Rzadko magowie. Mieszka sama, nic dziwnego. Pięknotka zdecydowała się wbić do jej mieszkania. Halina - z danych Pustogoru - nie radzi sobie dobrze z siłą ze strony władzy. Pięknotka chce wejść niezauważenie do jej mieszkania - używa rośliny by ta otworzyła klamkę (TpM): S. Pięknotce udało się wejść niezauważenie. EFEKT UBOCZNY: rośliny Haliny rosną i się zmieniają. Właściwie - wraca do domu i ma dżunglę.

Halina wróciła wieczorem, zmęczona. W pokoju, po ciemku czeka Pięknotka w power suicie. Pięknotka zaczęła coś mówić... a Halina strzeliła w nią wyładowaniem błyskawicy. (Tr: )

* XX: Wyłączony power suit (do końca)
* V: Pięknotce nic nie jest
* X: Cień wyrwał Pięknotkę z power suita
* X: Pięknotka opanowała Cienia, ale Cień zdemolował mieszkanie

Pięknotka nie włączyła przecież power suita w tryb bojowy a w tryb oczekiwania. Błyskawica przeładowała reaktor i power suit się wyłączył. Na razie nie ma kłopotów. Ale obudził się Cień, wyrwał się z power suita (lekkie uszkodzenia), po czym zaatakował by zabić. Pięknotka utrzymała kontrolę i Cień tylko nanitkami poszatkował mieszkanie. Halina straciła przytomność z szoku.

Ok - tego Pięknotka się ZUPEŁNIE nie spodziewała. Przeszukuje Halinę - ma kilka artefaktów tego typu. Zostały skonfiskowane przez terminuskę. De facto Pięknotka zabrała jej WSZYSTKO co mogło być bronią, artefaktem lub mieć wartość. Błyskawica skutecznie też usmażyła Halinową elektronikę. 

Pięknotka sprawdziła pobieżnie swój power suit; wymaga naprawy. A nie powinno być oficjalnie bo Pięknotka dostanie opiernicz. Następnym krokiem - Pięknotka ją związała używając jej pasków. I sole trzeźwiące. Halina się rozkaszlała.

Pięknotka powiedziała Halinie, że to wszystko to konsekwencje ataku na terminuskę. Te wybite okna, pazury wszędzie i to, że Halina jest pozbawiona przedmiotów. Halina jest przerażona tym wszystkim - powiedziała, że Pięknotka nie miała prawa jej tak zaskakiwać, nie wkradać się do jej domu jak zabójczyni. Gdy Pięknotka powiedziała, że uwielbia magów jak Halina którzy mogą niszczyć innym magom życie. Halina powiedziała, że jeśli Pięknotka ją napadła bo z kimś ją pomyliła...

W końcu Pięknotka powiedziała, że chodzi o Minerwę. Halina skrzyczała Pięknotkę - NIE DA SIĘ skierować infiltratora przeciwko pojedynczemu magowi, nie tak. To miało celować po prostu. Pięknotka ugryzła się w język - Minerwa ma energię ixiońską którą pajęczak może znaleźć.

Pięknotka nacisnęła i poznała prawdę - Halina dostaje fundusze z Aurum, po prostu, i wykorzystuje je by pomagać ludziom i magom którzy są anty-technologiczni i niekoniecznie radzą sobie z wysokim poziomem stechnicyzowania. I Halina dostała "żądanie zapłaty" - jeden ze sponsorów wysłał jej infiltratora którego miała poszczuć na magów w Szczelińcu. Więc Halina "błędnie" zrobiła rytuał by terminusi rozwiązali problem a ona dalej miała finansowanie. Halina próbuje powstrzymać Technożerców przed pójście w bardziej twarde, radykalniejsze metody.

Pięknotka naciska. Żąda od niej nazwiska sponsora który przesłał jej Infiltratora oraz "nie było mnie tu" + nie informanie owego sponsora. Dodatkowo Pięknotka dodała jej jeszcze narkotyki do przesłuchań, bo nią gardzi i nie dba o to jaki jest wynik. (TrZ+4)

* VV: Pięknotka ma gwarancję, że Halina nie powie niczego ani sponsorowi ani odnośnie obecności Pięknotki czy bycia napadniętą (w swoim rozumieniu). Jest odpowiednio zastraszona.
* V: Pięknotka poznała też nazwisko owego sponsora. To Nikodem Larwent. Pięknotka wie jedną rzecz - Larwent sam ma firmę zajmującą się TAI. Chce wejść na rynek... 

Zapytana czy wie, Halina potwierdziła - ale uznała, że te pieniądze może wykorzystać na pomoc lokalnych ludzi a terminusi rozwalą infiltratora bez problemu. Halinie do głowy nie przyszło, że ten infiltrator może być trudny lub groźny.

* V: Pięknotka rozbija pewność siebie Haliny. Halina nie ma zamiaru już dowodzić. Będzie pomagać ludziom, ale nie dowodzić i nie dotykać tematów związanych ze sponsorami itp. Pięknotka zrobiła straszny cios odnośnie możliwości pozyskania pieniędzy czy sponsorów Technożerców.

Pięknotka wzięła swój power suit, z trudem, i wywlokła go Cieniem. Cień jest silny ale nie jest w takich tematach idealny, bo wyraża swoją dezaprobatę. Chciałby spożyć sobie jakąś Halinkę, lub trochę krwi... I Pięknotka jest pewna jednej rzeczy - Halina nie będzie więcej bawić się w szpiegostwo przemysłowe czy działania przeciwko lokalnemu biznesowi.

A Erwin został poproszony o naprawienie tego power suita...

## Streszczenie

Okazuje się, że za atakiem na Minerwę stali Technożercy - a dokładniej, ich liderka - Halina. Pięknotka ostro weszła do Haliny, dewastując jej mieszkanie. Dowiedziała się, że Halina chciała dać swoim sponsorom z Aurum coś czego żądali - i przypadkowo (nie wiedząc o tym) skalibrowała Infiltratora na Minerwę. Pięknotka złamała Halinę, zmusiła ją do opuszczenia Technożerców. Nie ma mowy, że czarodziejka będzie wspierać taką organizację ludzi.

## Progresja

* Halina Sermniek: oddaje przywództwo Technożerców, nie jest w stanie dalej nimi dowodzić - nie po zastraszeniu przez Pięknotkę
* Halina Sermniek: rezygnuje z kontaktów z Aurum; uważa, że to nie jest bezpieczne dla niej osobiście.
* Halina Sermniek: święcie przekonana, że terminusi z Pustogoru to okrutne potwory
* Pięknotka Diakon: opinia wyjątkowo okrutnej terminuski wśród cywili; wchodzi twardo w pacyfistkę Halinę. Ale jest niesamowicie skuteczna - to się liczy.

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: pełne zastraszenie Haliny; weszła w ciemności PLUS po aktywacji Cienia dokonała demolki. Zastraszanie było super-efektywne.
* Minerwa Metalia: analizując anty-TAI z Infiltratora doszła do tego, że to kupione anty-TAI Mirtana; to nie chodziło o atak na nią.
* Feliks Mirtan: kiedyś agent Orbitera i nie powiązany z Astorią - teraz żyje w Aurum jako sprzedawca zmilitaryzowanych TAI. Zrobił anty-TAI użyte przez Infiltratora.
* Kreacjusz Diakon: kuzyn Pięknotki; dostarczył jej biolab do dyskretnej analizy Infiltratora. Oczekiwał Infiltratora jako zapłaty; alas, Infiltrator uciekł.
* Halina Sermniek: szefowa Technożerców (do teraz). Czarodziejka błyskawic. Pomaga ludziom którzy są wypierani przez TAI z rynku pracy, ale też sabotuje Infiltratorem dla sponsora. Walnęła artefaktyczną błyskawicą i uszkodziła power suit Pięknotki.
* Nikodem Larwent: sponsor Technożerców z Aurum; zapewnił im Infiltratora i niszczyciel TAI. Chce wejść na rynek Szczelińca, TAI cywilne - i nie zawaha się przed użyciem sabotażu.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Sensoplex
                                1. Osiedle Tęczy: tam mieszkają biedniejsi ludzie. Oraz Halina Sermniak, która chce im pomóc - liderka Technożerców (jeszcze).

## Czas

* Opóźnienie: 0
* Dni: 2
