## Metadane

* title: "Przeznaczeniem Szernief nie jest wojna domowa"
* threads: agencja-lux-umbrarum, problemy-con-szernief
* motives: dom-w-plomieniach, stracona-nadzieja, symbol-lepszego-jutra, starcia-kultur, wrobieni-lecz-niewinni, the-hunter, the-fateweaver, naprawa-bledow-przeszlosci, nie-na-to-sie-pisalismy, energia-fidetis, spotlight-na-lokacje, rytual-krwawy-selfsacrifice, radykalizacja-postepujaca
* gm: żółw
* players: kić, drakestar, chorąży_głuś

## Kontynuacja
### Kampanijna

* [231213 - Polowanie na biosynty na Szernief](231213-polowanie-na-biosynty-na-szernief)

### Chronologiczna

* [231213 - Polowanie na biosynty na Szernief](231213-polowanie-na-biosynty-na-szernief)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * Battle Beast "Armageddon"
        * "Reach for the stars with the heart that you've | Torn apart, patch it up and arise | You're the Goddess of the Light | Blazing high, satisfied, riding through the skies | This is your Armageddon"
        * "You're mesmerised by the sights and the lights | In the darkest of night, despite your own destruction"
        * Widzę tu 'musimy iść dalej, daleko, by zapewnić SWOIM LUDZIOM lepsze jutro żeby uratować stację'
        * Energia Fidetis - 'fatum'. Ludzie boją się biosyntów, nikt nie ufa Radzie i pojawiły się potwory odpowiadające na Paradygmat. Pojawia się spirala. "Lepsze jutro dla naszych".
* Opowieść o (Theme and vision): 
    * "PERFEKCYJNE społeczeństwo na stacji wymaga, by supły na tkaninie losu zostały wycięte"
        * Łowca wycina supły na tkaninie losu
        * Tkaczka naprawia tkaninę losu i doprowadza ją do perfekcji
    * "Utrata nadziei i rozbita spójność sprawia, że skupienie nie na Stacji a na swojej podgrupie zniszczy Stację. Fidetis - 'lepsze jutro' - optymalizuje pod każdą grupę i naprawia stację"
        * Doszło do poważnego rozbicia jedności stacji
            * Kalista udowodniła artykułami że są mroczne interesy Rady, że na Sebirialis są mroczne laboratoria, więzienia itp. To zniechęciło normalsów jak cholera.
            * Mamy serię zdrad w przeszłości: biosynty, Kalista (bo Agencja). Ludzie (bo Rada). Rada (bo Inwestorzy). Teraquid (bo Rada).
        * Każda grupa skupia się na sobie i próbuje uratować stację po swojemu
            * "Zapomnieliśmy, że to nasz wspólny dom i że jak nie współpracujemy, zostanie zniszczony"
            * -> Savaranie i Drakolici żyją obok siebie, są polowania na biosynty. Każdy żyje 'dla siebie', acz nawet główne frakcje się atomizują.
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * dom-w-plomieniach: otwarte polowanie na biosynty, ludzie Honga wypowiedzieli posłuszeństwo, starcia między gangami. Ale NADAL wszyscy chcą dobra dla stacji!
    * stracona-nadzieja: tym razem dotyczy KULTUR a nie ludzi. Lokalne frakcje nie wierzą, że INNE frakcje chcą ich dobra. Każda skupia się na sobie.
    * symbol-lepszego-jutra: są na jednej stacji. WSZYSCY chcą dobra dla stacji. Mimo atomizacji, NADAL stacja jest tego warta i NADAL chcą o nią walczyć. Też: Aerina.
    * starcia-kultur: część osób chce z tym WALCZYĆ, część chce się CHRONIĆ, Savaranie uważają że poświęcenie nie jest złe, Rada chce wszystko mieć pod kontrolą przy użyciu Teraquid.
    * wrobieni-lecz-niewinni: Mawir Hong i Kalista. Oni nie zrobili tego o co się ich podejrzewa. To przekolorowuje całkowicie ich działania na Stacji.
    * the-hunter: Istota Fidetis. Każde odstępstwo od działań wspierających dobrobyt jak rozumiany w Rytuale Krwi zostanie usunięte. Zabity, wraca.
    * the-fateweaver: Istota Fidetis. Zapewnia każdej frakcji wszystkie środki, których potrzebuje by dobrobyt jak rozumiany przez ową frakcję mógł zostać zrealizowany.
    * naprawa-bledow-przeszlosci: wszystkie działania na Stacji prowadzą do tego samego poziomu. Wszystko się psuje, nie ma zaufania i nikt nie wierzy że inni chcą dobrze. Trzeba to naprawić.
    * nie-na-to-sie-pisalismy: Kompania Teraquid. Mieli rozwalić biosynty a to z czym mamy do czynienia jest absolutnie poza ich możliwościami. Co więcej, na tej stacji mieli mieć emeryturę i okazję do wylizania ran - a jednak skończyli w najgroźniejszej operacji życia.
    * energia-fidetis: 'the fate is victory'. Wszyscy tracą nadzieję i pragną dominacji 'swego stylu'. Fidetis odpowiada i powołuje Łowcę. Przeznaczenie - najsilniejsza monokultura i pomoc swoim. "Lepsze jutro".
    * spotlight-na-lokacje: CON Szernief. By być w stanie rozwiązać problem tej lokacji trzeba zrozumieć dokładniej całą lokację - jak jej pomóc, co można zrobić.
    * rytual-krwawy-selfsacrifice: Tom-74, savaranin, był chory. Uznał, że jego życie jest finansowo nieopłacalne do leczenia. Poświęcił się by wesprzeć stację i swoją podopieczną, Się-03.
    * radykalizacja-postepujaca: W wyniku rytuału krwi dotyczy wszystkich; Sia chce by wszyscy żyli jak savaranie, Mawir chce osiągnąć wielkość godną jego boga i każda grupa skupia się na sobie.
* Detale
    * Crisis source: 
        * HEART: "sin (grzech): strach przed TYMI INNYMI plus podstawowa utrata nadziei że może być kiedykolwiek lepiej. Atomizacja społeczeństwa. Dominacja NASZYCH poglądów."
        * VISIBLE: "Dosłownie, polowanie na ludzi. Pojawia się poczucie przeznaczenia nieuchronnej walki, choćby z uwagi na prowokację przez zabójcę (Hunter)."
            * Drakolici Honga 
                * Są gotowi na walkę z potworem i wszystkim innym. To jest test Saitaera. Oni są prawdziwie czyści XD.
                * Kalista idzie z nimi, chcąc udowodnić niewinność wszystkich.
            * Drakolici Terimana
                * Są gotowi walczyć za jedność stacji. Oni nie stracili nadziei. Oni są potencjalnie największymi sojusznikami.
            * Savaranie Wita
                * Ogólnie ufają Agencji za recykler. Chcą dobra stacji za wszelką cenę, łącznie z eksterminacją sił Sii lub wszystkich co stoją na drodze lepszej stacji.
            * Savaranie Sii
                * ?
            * Rada ?
                * Aerina ?
                * Felina ?
            * Arystokraci
                * Skompromitowany Artur ?
            * Biosynty uważają to za atak ze strony Ludzi.
    * Delta future
        * DARK FUTURE:
            * chain 1: ?
            * chain 2: ?
            * chain 3: PRAWDA: wiedza o magii wyjdzie na jaw, konieczność aplikowania masowych amnestyków. Zniszczenie stacji?
        * LIGHT FUTURE: 
            * chain 1: ?
            * chain 2: ?
            * chain 3: Nikt jak o magii nie wiedział tak nie wie
    * Dilemma: brak

### Co się stało i co wiemy

* Kontekst stacji
    * CON Szernief została zaprojektowana jako stacja sprzedająca Irianium. Jest to miejsce o podniesionej ochronności elmag (lokalizacja: przy Sebirialis).
    * Okazuje się, że populacja która zainteresowała się tymi warunkami to byli Savaranie.
        * I inwestorzy są w klopsie. Dla savaran nadmiar energii to radość. Nie chcą kupować seks-androidów ani niczego. Pełna oszczędność.
            * Savaranie to NAJGORZEJ z perspektywy korporacji i inwestycji XD
        * Inwestorzy żądają zwrotu
    * Zarządcy CON Szernief szukają kogoś kto będzie tam chciał żyć i najlepiej za to jeszcze zapłaci
        * Stanęło na ściągnięciu Kultystów Adaptacji. Ale oni nie są jacyś super niebezpieczni, sami tak mówią ;-).
* Przeszłość
    * 4 miesiące temu udało się rozwiązać problem z energią Unumens, gdzie wszyscy byli unifikowani w jeden umysł
        * Kalista i Mawir Hong zostali publicznie obarczeni winą przez Agencję (zrzucona wina)
        * Udało się wszystko zachować i zatrzymać
    * 2 miesiące temu wielka firma zdecydowała się wygryźć stację Szernief z rynku baterii słonecznych
        * Ilość pieniędzy podupadła, ilość awarii wzrosła
        * Wzrasta presja
        * Kalista ogłasza artykuł o problemach i braku etyki u Rady
        * Sia robi Krwawy Rytuał Pomyślności, poświęcając swojego chorego przyjaciela (akceptowalne u savaran)
            * -> pojawia się energia Fidetis
    * 1 miesiąc temu doszło do problemu z awarią w kosmosie (SCENA ZERO)
        * Do tej pory mamy coraz większą atomizację
            * Savaranie: Sia (agresywne działania), Wit (izolacja), Ikta (współpraca)
            * Drakolici: Mawir (agresja izolacja), Teriman (współpraca)
            * Teraquid: ostrożność


### Co się stanie (what will happen)

* F0: TUTORIAL
    * Zespół gra trzema drakolickimi inżynierami próżniowymi. Dostali wiadomość, że powinni pomóc grupce savaran.
        * (tak naprawdę to jej nie dostali)
    * Wybuch. Unlikely (sabotaż biosyntów?).
    * (byli NIE TAM GDZIE MIELI i byli kompetentni by pomóc - rozszczelnienie jednego, jeden odlatuje, dwóch rozpaczliwie pilnuje by nie doszło do kaskady eksplozji)
* Szachy / AGENDA
    * Pretient Team
        * SUKCES: wojna domowa
        * Tkaczka: 
            * powoduje/zapobiega awariom, compele, działania mentalne
            * skupia grupy wobec swoich heimatów i swoich poziomów wizji, zwiększając sub-kohezje
        * Łowca: 
            * eliminuje niezaadaptowanych savaran i drakolitów (bunt, chaos, sabotaż przemyt terroryzm)
            * poluje na arystokratów, bo oni z definicji nie pełnią żadnej pożytecznej roli
            * z uwagi na swoją specyfikę wszystko wygląda jakby to działania biosyntów były
    * Biosynty: 
        * SUKCES: 
        * podejrzewają Teraquid że to ich wina i oni napadają na drakolitów i savaran by rozpętać piekło
        * szukają na stacji miejsc w których mogą się naprawiać i zainstalować
        * -> Łowca na nich poluje. Nie pasują do sieci Tkaczki.
    * Mawir: 
        * dąży do zbudowania najpotężniejszej grupy bojowej ze swoich drakolitów
            * -> Tkaczka daje mu na to możliwość, pozycjonując że Mawir ma rację oraz dając mu okazję walki z biosyntami
                * wyjątkowo dobrze działają systemy; jest w stanie zrobić odpowiednio nieprzyjemne miejsce walki
                * lekarz (Sulut Kridalir) mu pomaga dostarczając mu odpowiednie środki i mechanizmy
            * -> Łowca
                * taktyczne zabójstwa, by Mawir i jego ludzie MUSIELI walczyć z savaranami, biosyntami i ludźmi Terimana
            * ENDGAME: Mawir zmierzy się z biosyntami
    * Teriman + Ikta-04: 
        * dążą do zbudowania mostu między savaranami i drakolitami; chcą współpracującej stacji
            * -> Tkaczka:
                * sabotuje niektóre subsystemy by sami savaranie lub sami drakolici nie dawali sobie rady
                * zapewnia wyjątkowo dobre funkcjonowanie systemów, gdzie wszyscy współpracują; drakoliccy inżynierowie i savarańscy inżynierowie -> jedzenie, komfort...
            * -> Łowca: 
                * eliminuje osoby z frakcji Terimana i Ikty którzy chcą działać INACZEJ
    * Wit: 
        * dąży do maksymalizacji oszczędności pośród savaran 
            * SUKCES: savaranie żyją taniej i lepiej, wzmacniając swoją kulturę
        * -> Tkaczka: wzmacnia radykalizm jego frakcji. Taniej i lepiej.
        * -> Tkaczka: Tworzy sytuacje, gdzie optymalizacje Sii prowadzą do nieoczekiwanych, ale pozytywnych skutków dla różnych grup na stacji, wspierając jej wizję.
        * -> Łowca eliminuje wszystkich którzy próbują odejść / nie skupiają się na izolacji.
    * Sia: 
        * dąży do maksymalizacji oszczędności i optymalizacji CAŁEJ stacji, niezależnie od konsekwencji. Zaadaptujcie się do oszczędności!
            * SUKCES: wszyscy żyją jak savaranie
        * sabotuje wszelkie komponenty które nie należą do kluczowych, nieważne czyje
        * -> Tkaczka: wzmacnia radykalizm jej frakcji
    * Rada: 
        * chce wymiany populacji. Savaranie i Drakolici statystycznie sprawiają więcej problemów niż jakakolwiek inna frakcja.
            * SUKCES: stacja się stabilizuje i nie trzeba wysyłać Teraquid na populację, przeciw Felinie i ochronie
        * -> Łowca próbuje ich usunąć
        * -> Tkaczka zwiększa awaryjność systemów w ich okolicy; Teraquid musi ich ciągle ratować.
    * Teraquid: 
        * NIE NA TO SIĘ PISALI. Chcą stąd znikać. To 47 uzbrojonych i niebezpiecznych ludzi; chcieli emerytury a nie walki z biosyntami i stacją...
            * SUKCES: rozwalają biosynty, nikogo nie muszą zabijać. Oraz chronią radę.
        * Są ostatnim co stoi pomiędzy Radą a Łowcą
        * Nie wierzą, że to biosynty.
    * Arystokraci i Kalista:
        * Aerina oraz Artur próbują przywrócić pokój na stacji i wejść w komunikację z biosyntami. Współpracują z Kalistą. Dyskretnie.
        * Zbierają informacje co tu się dzieje
        * Chroni ich Vanessa i Sebastian, dyskretnie.
* Łańcuchy
    * Krucjata defensywna Sii
        1. Drakolita pracuje z savaraninem i awaria (Tkaczka). Jednak ludzie Sii traktują to jak atak.
        2. Sia wysyła część oddziału przeciw drakolitom, na drodze stają siły Mawira
            * siły porządkowe ich muszą rozdzielić
            * "Mawir to Ty robisz sabotaże" | "NIE BYŁEM TO JA!!! TO CHOLERNE BIOSYNTY!!!"
    * Sabotaż Sii
        1. Savaranie Sii wkradają się i uszkadzają systemy dystrybucji. Luksusowe jedzenie przestaje działać i się psuje.
        2. Savaranie Sii udowadniają na wykresach i parametrach, że stacja może być ŁATWA do utrzymania.
        3. czemu nie wyłączyć części gdzie są biosynty i nie przejść stacji w low power mode? Tylko basic maintenance. Poradzą sobie.
        4. praca dłużej i wydajniej, pod kątem kompetencji
    * Teraquid atakuje Teraquid
        1. Bardzo skuteczny atak na Radę, ale opanowany i obroniony przez Teraquid
            * to byli ich ludzie; celem było nie wysyłanie Teraquid do głębi stacji
    * Mawir atakuje biosynty
        1. Starcie ludzi Mawira z innymi w knajpie 'TechnoBaletnicy' (sabotowanej przez Się)
        2. Oddział Mawira wchodzi w głąb, szukać biosyntów. Nie jest w stanie ich zlokalizować, ale przetrwali pułapki
        3. Mawirowcy spotykają Kalistę która używa 100% swoich skilli by go zatrzymać - niech nie atakuje biosyntów! Nie ma dowodów! To może być Teraquid!
    * Teriman + Ikta stabilizują stację
        1. Drakolita próbuje agitować przeciw współpracy. Łowca go zabija w okolicach tuneli inżynieryjnych przy Promenadzie.
        2. Wszyscy winią za to biosynty
        3. Gdy dochodzi do niestabilności przy ładowaniu baterii, ogromną ofiarą udaje się uratować stację.
    * Desperacki ruch biosyntów
        1. Biosynty stabilizują bazę w okolicy reaktorów awaryjnych, mniej używanych, na poziomie -3
        2. Delgado ściera się z Łowcą. Pokonał go DWA razy, ale już ma niesprawną rękę
        3. Delgado planuje porwać jakiś statek i opuścić tą stację, ale gdzie?
    * Łowca karmi Tkaczkę. On jest ręką, ona mózgiem
        1. Tkaczka jest w centrum Savaran, adaptując się wizualnie. Łowca jest manifestowany jako panteropająk

## Sesja - analiza

### Fiszki

CON Szernief:

* Janvir Krassus
    * OCEAN: (O+ C+) | "Przez współpracę i innowacje budujemy lepszą przyszłość." (Locke) | VALS: (Tradition, Security) | DRIVE: "Przekształcenie Szernief w zaawansowaną kolonię."
    * on jest odpowiedzialny za współpracę z NavirMed
* Larkus Talvinir
    * OCEAN: A-O- | "Żartuję nawet w złych sytuacjach; prowokuję i nie boję się konfliktów" | VALS: Stimulation, Universalism | DRIVE: "Zabawa kosztem innych to moja rozrywka"
    * główny inżynier w sztabie
* Marta Sarilit
    * OCEAN: (N+ O+) | "Jeśli nie będziemy dość szybcy, zostaniemy zniszczeni." | VALS: (Benevolence, Power) | DRIVE: "Naprawa i lecznie"
    * dowodzi więzieniem NavirMed; to jej operacja
* Felina Amatanir
    * OCEAN: (N- C+) | "Nieustraszona i niezależna, wyróżnia się determinacją i profesjonalizmem" | VALS: (Power, Security) | DRIVE: "Porządek i sprawiedliwość, ponad kontrakt"
    * drakolitka o zwiększonych parametrach; wywalili ją z zarządu i z dowodzenia ochroną po tej operacji
* Kalista Surilik
    * OCEAN: (A+E+) | "Odważna i zdecydowana, w gorącej siarce kąpana" | VALS: (Benevolence, Achievement) | DRIVE: "Prawda MUSI wyjść na jaw"
    * już z nią Agencja miała do czynienia...

#### Strony

.

### Scena Zero - impl

Gracze grają grupą drakolickich inżynierów próżniowych; dostali polecenie od dowództwa, żeby przedostać się do miejsca gdzie normalnie pracują savaranie. Są na zewnętrznej powłoce stacji Szernief. Dochodzi do wybuchu - jeden savaranin odlatuje w kosmos, jeden jest ranny. Savaranie próbują pomóc temu który jest ranny, ale odlatujący wyleci poza zasięg pomocy.

Jeden z inżynierów rzucił się żeby złapać odlatującego od stacji nieszczęśnika, korzystając ze swojej liny. Okazuje się że jego lina także została sabotowana (?) A silniki manewrowe nie działają. Udało mu się jednak tak skierować tor lotu, że poleciał w kierunku na savaran. Ci go przechwycili i uratowali. Nikomu nic się nie stało dzięki współpracy pomiędzy dwoma ludami, co nie dzieje się często.

Tr Z +3:

* X: udało Ci się to wszystko zrobić, ale silnik manewrowy też nie działa.
* X: okazuje się, że z liną jest coś nie tak - sabotaż. Lecicie w stronę kompatybilną z savaranami.
* V: sukces. Savaranie ściągnęli. Oboje jesteście uratowani.
* Vz: mimo słabego sprzętu udało się wyjść na bohatera.

Ranny savaranin (hermetyzacja nie działa, sabotowany sprzęt). Ranna kończyna.

Tp +2:

* Vr: udało Ci się zahermetyzować savaranina. Nie umrze.

Robota wykonana.

Koniec akcji. Agenci przestają oglądać nagranie które wysłała im Aerina, która informuje ich o tym co jest dziwnego (na nagraniu):

* Po pierwsze, jeśli to był sabotaż to był jakiś niesamowicie dobry sabotaż. Nie ma sensownych śladów.
* Po drugie, oni w ogóle nie powinni tu być. Mimo, że drakolici się zarzekają że dostali takie rozkazy, dowództwo nie kazało im zbliżać się w tamtą stronę.
* Po trzecie, pomiędzy biosyntami, kompanią zbrojną i tym wszystkim Aerina naprawdę nie wie co robić. Ma nadzieję że tak jak agencja kiedyś jej pomogła, tak pomoże znowu.

WYBÓR POSTACI:

* Drake: sabotażysta
* Chorąży: inżynier
* Kić: inspirator

### Sesja Właściwa - impl

Luminarius zbliża się do stacji. Trzy potencjalne strategie:

* Rozmowa z drakolitami by się coś dowiedzieć. Gabriel (ten bohater ze sceny zero).
* Inne przypadki sabotażu - więcej, części wspólne itp.
* Rozmowa z Aeriną

Idąc po stacji, zespół orientuje się, że stacja jest dużo bardziej zubożała niż kiedyś. Coś się zmieniło; z perspektywy finansowej jest dużo gorzej niż kiedykolwiek. Zespół zaczął analiza sytuacji od rozmowy z Aeriną, która z radością przyjęła ich w swojej kwaterze. 

* Aerina: Wszyscy podejrzewają biosynty.
* Sabotażysta: CO TU SIĘ STAŁO? (widząc ślady wybuchu)
* Aerina: (posmutniała) Ktoś... gdy mnie nie było to KTOŚ próbował wysadzić moją kwaterę.

Co i kto próbował wysadzić w tym pomieszczeniu?

* V: To nie był 1 wybuch. To była seria ruchów - do niszczenia najbardziej energożernych rzeczy.
    * Aerina wierzy, że ktoś próbował zrobić jej krzywdę
* V: Aerina nie zdaje sobie z tego sprawy, ale wie więcej niż powiedziała
    * Savaranie podzielili się na frakcje. Atomizacja.
    * Drakolici mają DWIE frakcje - zwolennicy współpracy oraz ludzie Mawira
    * Sia-03 jest skrajnie radykalna i oni niejeden raz już coś sabotowali

Sabotażysta od razu zauważył że to nie takie proste; gdyby ktoś chciał wysadzić arystokratkę, byłby w stanie to zrobić. To była fachowa robota której celem było to żeby na pewno nikt nie ucierpiał ale żeby zniszczyć wszystkie bardziej energożerne i energokosztowne elementy. Dopytując arystokratkę co mogło się stać lub kto mógł za tym stać, ona w sumie nie wie. Ale powiedziała więcej niż się wydawało. Zespół mający wiedzę o tej stacji był w stanie dopowiedzieć sobie resztę szczegółów, zwłaszcza zasilany danymi z TAI stacji.

W międzyczasie do pokoju weszła Kalista. Chciała coś powiedzieć i pokazać Aerinie, ale widząc agencję spojrzała na nich z ogromną niechęcią i jak najszybciej się ulotniła zaznaczając, że Aerina nigdy nie powinna była ich tu sprowadzać. Tymczasowo zespół nic z tym nie robi - nie ma ani środków ani konieczności.

Zgodnie z danymi, jest 14 ofiar; wina spada na biosynty. Wszyscy je podejrzewają. Poza Kalistą.

Oki - porozmawiajmy z Sią-03, jako główną podejrzaną tego sabotażu. Sia spotka się zdalnie. Inżynier pyta ją o działania sabotażowe. To POCZĄTKOWE czy Aeriny?

Tp +3:

* Vr: Sia przyznaje się do sabotażu dużej ilości małych rzeczy; wszystkie 'zbędne'
    * pokazuje wykresy
    * nie przyznaje do sabotowania w kosmosie.
* (+Vg) Pytanie o które sabotaże, śmierci itp
    * Sia demonstruje swoje sabotaże i plany przeszłe. Ona nie ukrywała.
        * "wszyscy żyją jak savaranie"
        * Sia nie zachowuje się jak savaranka.
* (+Vg -> TrZ) Działania są niewspółmierne i to jest nieefektywne. Niszcząc sprzęt niszczysz zasoby.
    * Vg: Sia patrzy i WIDAĆ po niej że do niej to dotarło. Po chwili potrząsnęła głową.
        * Niestety, to jest niezbędne. Trzeba. Inaczej stacja nie przetrwa.
            * Za bardzo radykalna.
* Vr: Sia się zaczerwieniła na rozmowy o radykalizmie.
    * "Wszyscy marnują energię. Mój przyjaciel nie. On nie żyje. Chcę tylko by wszyscy oszczędzali."
        * (Tom-74 był jej przyjacielem. Był chory i dało się go uratować. Za drogo. Zniknął 2 miesiące temu.)
* (+X) V: Sia jest przeciwna temu by mówić, ale powiedziała
    * (Tom-74) odszedł i zniszczył się dla dobra stacji, zabijając się i robiąc jakiś rytuał krwi.
        * Sia musiała go zabić. Tom chciał zapewnić idealny świat.
        * Tom był mentorem Sii. Był 'jak ojciec'. On był ciężko chory.
* X: Sia nie wierzy w magię, ale wierzy w sens tego co Tom zrobił. Wierzy w symboliczny sens i uważa, że ona odejdzie tak samo.

Ogólnie z rozmowy z Sią-03 wybrzmiewa jej ogromne rozczarowanie, rozgoryczenie i smutek. Sia-03 straciła swojego mentora, którego uważała za ojca. Jeżeli on zginął przez brak energii, dlaczego inni mogą tą energię marnować? Fakt że lokalny lekarz, Sulut, rekomendował żeby Tom-74 jednak dostał wsparcie medyczne nie zmienia nic faktu, że Tom był prawdziwie głębokim savaraninem. A Sia poszła za jego prośbą.

Ogromnie agencję zaniepokoił fakt, że Sia mówiła coś o rytuale krwi przeprowadzonym przez nią i Toma. Ten rytuał pochodzi z czasów jak Tom był na innym statku kosmicznym; to była ofiara dobrowolna raz na rok, mająca przynieść dobrobyt i powodzenie.

Mając powyższe, Sabotażysta idzie przebadać akty sabotażu, dowody rzeczowe itp. Zwłaszcza te elementy które mają straty życia itp. A w tym czasie Zespół idzie pogadać z drakolitami którzy byli w Scenie Zero.

JEDEN z drakolitów - Gabriel - był we frakcji Mawira i zmienił na Terimana. Stwierdził, że współpraca jest fajniejsza. Dostał porządny wpierdol od ludzi Mawira ale potem zostawili go w spokoju.

* G: Przyszliście rozmawiać z bohaterem? :D /technobaletnica knajpa
* C: Sabotowany sprzęt.
* G: (w fatalnym stanie, nie mieliby jak wrócić)
* C: Czy to jest sprzęt co pobierają, na osobistym stanie...?

Tr Z+3:

* Vz: Gabriel myśli, uświadomił. DUŻE OCZY.
    * skafandry są doprodukowane osobiście. NIKT nie sprawdził? A ja sprawdziłem linę. A poszło.
    * (magia nie istnieje więc to musiały być biosynty)
* X: (przyjdzie Mawirowiec robić dym)
* V: Gabriel w sekrecie mówi
    * "ale dobrze wyszło, bo patrzyłem na tych savaran jak na takie... śmieszne ludki, dziewczyny się często nie myją. A teraz - patrz, pracujemy razem"
    * (nie wyszło masz przyjaciela)

Szafa grająca się zepsuła, po prostu. Poszedł dym. (SABOTAŻ). Do Gabriela podchodzi inny drakolita. Dorion. Zaczepia Gabriela "już się nie płaszczysz savarańskim panom?". Inspiratorka -> Dorion, przełamuje

* (+2Vg) V: Dorion przerwał, obwąchał ją "przynajmniej Ty się myjesz".
* X: "Jeśli nie atakowaliście to chcę uwierzyć ale próbuję zrozumieć"
    * Dorion: jasne. Mawir WSZYSTKO udowodnił Kaliście i co? I dalej 'my robimy'. Ta pieprzona Agencja, wrobili nas. Kalista też ucierpiała. Za dowody.
    * Dorion: a TERAZ jeszcze wrabiają nas w te morderstwa. Nie zabijamy savaran.
    * (AGENCJA WRABIA MAWIRA W MORDERSTWA)
* V: Rozwikłanie co się dzieje i co robi Mawir
    * Mawir ma dość bycia napastowanym przez słabeuszy.
    * Ale są biosynty zesłane przez Saitaera. 
    * Mawir ma GDZIEŚ to że na nas nadają. To nie nasza wina. Savaranie atakują. Dobre. Walka.
        * nie wiem KTO ich zabija ale dzięki temu jest walka.

Inspiratorka prawidłowo podzieliła stanowiska dwóch odmiennych od siebie ideowo drakolitów. 

Gabriel wierzy, że to wszystko był sabotaż. To musiał być sabotaż, mimo że sabotażysta nie miał jak tego wszystkiego zrobić i zrealizować. Zarówno savaranie jak i on sprawdzali sprzęt przed tym jak wyruszyli na akcje i sprzęt po prostu nie miał jak przestać działać. Ale wyszło na dobre – drakolici będą współpracować z savaranami i wśród savaran pojawiła się Ikta-04 która też do współpracy dąży. 

Zdaniem Gabriela kimkolwiek był sabotażysta, zawiódł. Zamiast doprowadzić do czyjejś śmierci jedynie doprowadził do współpracy pomiędzy dwoma ludami.

Dorion natomiast ma bardzo odmienne spojrzenie na wszystko. Jego zdaniem, savaranom całkowicie odbiło. Rada wrabia jego frakcję – frakcję Mawira – w „te wszystkie morderstwa” po tym jak agencja wrobiła jego frakcję w „te wszystkie sabotaże”. Jednak oni nie patrzą na to jak na coś z jakiego powodu należy płakać. To jedynie kolejny test Jego Ekscelencji, Saitaera, tak jak on zesłał im biosynty do walki.

Dorion też chce by stacja była szczęśliwa. Ale Mawir robi to zupełnie inaczej. Ich zdaniem to bardzo korzystne, ktoś zabija ludzi przez co Mawir ma z kim walczyć – savaranie ich atakują i zastawiają na nich pułapki by pomścić swoich. Acz Mawir NIKOGO nie zabił.

PODSUMOWANIE CO WIEMY:

* Jest Sia, która jest savaranką i jak na savarankę dużo energii by być OGNISTĄ savaranką. Atakuje rzeczy które są 'kosztowne'. I jest ognista. 
    * I charyzmatyczna. Ale za nią idą.
    * Tom-74 zrobił rytuał krwi.
* Jest Mawir, bardziej radykalny.
* Jest Gabriel, nietypowe jest zmiana strony tak po prostu.
    * "sojusz Terimana i Ikty możliwy DZIĘKI temu sabotażowi"
* Interakcja z Kalistą była za krótka. Strzela focha.

Sabotażysta sprawdza rzeczy sabotowane. KTO sabotuje? Badając przypadki - odrzucić sabotaże oszczędnościowe i znaleźć inne. To, czego nie da się przypisać savaranom.

Tr Z +4:

* Vz: Od razu rzuciło się:
    * Sabotaże Sii widać.
    * Występują pomniejsze sabotaże innych frakcji. -> KIEDYŚ NIE BYŁO. Radykalizacja rośnie ogólnie.
    * to jest to ze sceny na kamerach (ten sprzęt powinien działać, zbiór nieprawdopodbnego pecha)
    * Lina się zepsuła. Nie ma logicznego wyjaśnienia. **Magia**.


Zespół konsultuje się z bazą danych na statku kosmicznym (Luminariusie). Jednoznacznie wszystko wskazuje na Energię Fidetis. „Dobrobyt i lepsze jutro”. Najprawdopodobniej rytuał magii krwi doprowadził do tego, że uwolniona została energia, która ma za zadanie pomóc populacji tej stacji. 

Problem polega na tym, że to jest rytuał, który pochodzi ze statku savarańskiego. Na takich statkach jest jeden kierunek i jedna strategia przetrwania. Na tej stacji każda frakcja ma własną strategię wygrywającą. Tak więc energia Fidetis sprawia, że każda grupa jest wzmacniana po swojemu nawet jeżeli to wchodzi w kolizję z inną grupą.

W czym jednak się manifestuje ta energia? Gdzie jest punkt zaczepienia?

Zwykle magia osadza się w życiu albo w czymś ważnym dla kogoś. Może być powiązanie przez sympatię. Pierwsze myśli sabotażysty trafiły na śmierć Toma. Tom-74 był savaraninem. To sprawia, że po zrobionym rytuale Sia by nie zostawiła jego ciała na pastwę losu. Najsensowniej - dla savaran – byłoby wykorzystać go w jakimś systemie recyklingowym, by nawet po śmierci się przydał.

Nadal nie rozwiązuje to problemu adresowania, które posiada magia. Ale daje bardzo silny wskaźnik na to, gdzie może znajdować się jeden z punktów manifestacji.
By rozwiązać ten problem, poprosili o pomoc Felinę.

Felina chciałaby być bardziej pomocna, ale rada zablokowała jej ruchy; ona sama proaktywnie nie muszę poprosić o pomoc. Ale jak jest poproszona o pomoc przez agencję to jest w stanie im pomóc - jako osoba prywatna, nie jako szefowa ochrony czy członek rady.

Felina pokazała agencji przerobione zdjęcie ciała; ofiary ‘potwora’, które dostała od Kalisty. A Kalista dostała je od Mawira. Wszystko wskazuje na to, że Mawir aktywnie poluje na tego potwora i chce ochronić stację tam gdzie wszyscy inni - jego zdaniem – zawiedli. 

W ciele są dziury jakby się coś wbiło i wysysało. Czy z ukrycia, pościg... (Mawir zrobił zdjęcie i podobno walczył z potworem)

Tr Z+3:

* X: Nie da się zdobyć kluczowych danych przez to jak bardzo zmienił wygląd Mawir ('Photoshop')
* V: Oki, da się wyciągnąć wnioski.
    * Savaranin zaatakowany z zaskoczenia
    * Przebity w czterych miejscach, na bokach
    * Pattern pajęczaka

Czyli jest tu potwór. To sprawia, że bezpieczniej byłoby iść z KIMŚ a nie samemu. Najlogiczniejszą opcją jest 'kompania Teraquid'. Więc Zespół spotkał się z ich przełożonym, Kaldorem.

Kaldor jest starszym wojskowym. Wygląda na jakieś... 60 lat? I jest ranny. Rozorane ramię - w ciągu ostatniego tygodnia. 

Kaldor wyraźnie jest w sytuacji dla siebie bardzo niekomfortowej. Został ściągnięty do polowania na biosynty, ale już kilka razy ochronili członków rady przed zabiciem ze strony niezidentyfikowanego pająkopodobnego kota (Łowcy). I mimo że za każdym razem łowca został zabity przez oddział chroniący radę, łowca zawsze wracał.

Kaldor nawet pokazał lokalizację w których to cholerstwo wracało. Z jego perspektywy to musi być jakieś działanie bardzo zaawansowane technologicznie; to nie mogą być biosynty. Kaldor przyznał się, że podejrzewa że to może być ich konkurencja która próbuje ich zniszczyć, ale nie wie kto i dlaczego. Tak czy inaczej, „drony” (The Hunter) polują na radę i to tylko kwestia czasu kiedy im się uda zabić włodarzy stacji.

Rewelacje przełożonego Teraquid zmusiły zespół do poważniejszego zastanowienia się. Jeżeli łowca jest w stanie powrócić po zabiciu, to nawet lapisowanie jego ciała nie rozwiąże problemu. Zdecydowanie pomoże dotarcie do pierwotnego źródła manifestacji (system recyklingowy w którym skończyło ciało Toma), ale dobrze byłoby tam dotrzeć przed wsparciu siły ognia.

Zespół bierze ze sobą 4 Teraquidowców uzbrojonych i idziecie szukać po recyklingu w serwopancerzach z "wykrywaczami karaluchowymi". Mamy też mapy Feliny i zgodnie z ruchem walki po okopach. Wykorzystujemy drony do monitoringu.

Tp+4:

* Vr: Udało Wam się wymanewrować potwora. Nie ma jak 'wyjść'.
* V: Jesteście na miejscu rytuału. System recyklingowy.

Tr Z +3:

* V: System recyklingu ZOSTANIE WYCZYSZCZONY
* Xz: Podczas rozkładania i pracy nad systemem zorientowałeś się, że część dron wydaje alarmowe sygnały.
* Vr: gaśnicą lapisową wygasiliście potwora, tą iterację.
* X: potwór słabszy budzi się w innym miejscu
* X: potwór próbuje się zasilić i wzmocnić. Ma tego pecha, że natrafia na Mawira Honga.
* V: podsystem recyklingowy jest naprawiony
* X: Sia jest podniesiona, może dalej prowadzić, ma aspekt potwora. Jest silniejsza.
* V: macie dokładny rozkład (dzięki Inżynierowi) która pasta, jak, gdzie trafiła itp.

Dzięki wykorzystaniu odpowiedniej technologii, wspomaganych serwopancerzy i lapisu udało się zniszczyć materialną formę łowcy w miejscu manifestacji. Niestety, Tom zginął nie w losowym systemie recyklingowym a w miejscu, w którym produkowana jest savarańska papka żywnościowa. To pokazuje w jakich miejscach łowca mógł powrócić - musiał mieć dostęp przez ową papkę.

Jedno miejsce, w którym łowca się zamanifestował - bardzo osłabiony przez lapis - to były okolice Mawira Honga. Gladiator skutecznie rozwalił potwora, podnosząc jedynie swoją reputację w formie „ oni nie dali rady, ale ja tak.” Co interesujące – Mawir wzmocnił też reputacyjnie Kalistę. 

Tymczasem mniej interesująca się polityką stacji Agencja skupiła się na wykorzystanie Feliny i jej sił do jednoczesnego uderzenia lapisem we wszystkie Skażone papki. W ten sposób potwór został nieodwracalnie zniszczony.

Potwór został zniszczony. Acz Sia i Mawir są wyżej w pozycji. Teraquid jest w gorszej sytuacji niż kiedyś.

Inspiratorka odpala umiejętności dyplomatyczne. Podnieść status, pozycję i zaufanie do Teraquid. Niech Teraquid chce współpracować z Agencją. Podrasować sceny ich starcia, skłonienie szefa Kompanii do wypowiedzenia się że jest 'dla dobra stacji' itp.

Tr Z+4:

* V: Teraquid będzie traktowana jak 'obca metakultura'. Ostrożnie neutralnie.
* X: Rada jest uważana za wrogów mieszkańców stacji
* V: Teraquid się instaluje na stacji jako 'główna baza'. Jest przyjazny Agencji. Acz nie wiedzą o magii. Co zwiększa dramatycznie siłę ognia Agencji.

Koniec.

## Streszczenie

Na Stacji doszło do odprawienia savarańskiego rytuału Krwi dobrobytu w formie energii Fidetis. Problem w tym, że savaranie mają jedną wizję sukcesu a Szernief ma N grup. Więc każda grupa jest wzmacniana w _inny_ sposób, co powoduje ciekawe problemy (np. sabotaże by działali razem, lub savaranie atakujący Mawirowców by ci mogli być silniejsi). Agencja odkrywa obecność Łowcy (polującego na Radę) i go neutralizuje. Niestety, Tkaczka integruje się z Sią-03 tworząc nową frakcję.

## Progresja

* Sia-03 Szernief: jest ascendowana przez integrację z Tkaczką Fidetis, może dalej prowadzić frakcję Oszczędnościowców. Jest silniejsza niż była, częściowo homo superior.
* Kalista Surilik: dzięki działaniom Mawira, jej pozycja jest bardzo podniesiona.
* Mawir Hong: miejsce, w którym Łowca się zamanifestował - bardzo osłabiony przez lapis - to były okolice Mawira. Mawir Łowcę pokonał i zjadł jego kawałek. Bardzo podniesiona pozycja.

## Zasługi

* Klasa Dyplomata: wyłapała różnice ideowe między drakolitami i doszła do radykalizacji. Podniosła pozycję Teraquid i dodała Teraquid zaufanie do Agencji. Nowa populacja i to przyjazna celowi.
* Klasa Sabotażysta: określił że Aerinie nic nie groziło przy sabotażu Sii; doszedł do jej motywu. Udowodnił obecność magii przez nienaturalne sabotaże. Doszedł do roli Toma-74 i że on był przyczyną.
* Klasa Inżynier: wygasił system recyklingowy gdzie skończył Tom-73 i zalapisował go; znalazł gdzie rurami idzie biomasa i przez to zniszczył aspekt Łowcy energii Fidetis.
* Aerina Cavalis: próbuje jakoś przywrócić pokój na stacji mimo, że Sia-03 wysadziła jej pokój (by zredukować marnotrawstwo energii)
* Felina Amatanir: nadal zablokowana przez Radę, ale zebrała dane zdjęciowe od Kalisty i pokazała je Agencji - tak wygląda ofiara potwora (The Hunter). I że Kalista dostała je od Mawira.
* Kalista Surilik: z ogromną niechęcią przyjęła pojawienie się Agencji na terenie Szernief znowu. Unika ich. Zebrała dane o tym że coś się tu dzieje i przekazała je Felinie. Ogólnie, offscreenowa.
* Mawir Hong: uważa zmianę stanu stacji w Wielki Test Saitaera. Wielokrotnie walczył z Łowcą i nawet go pokonał. Zjadł jego kawałek.
* Sia-03 Szernief: po tym jak jej mentor - Tom-74 - zrobił rytuał Krwi, ona została Dotknięta Fidetis. Założyła frakcję Oszczędnościowców, sabotując na lewo i prawo niepotrzebne do przetrwania rzeczy. Niech wszyscy żyją jak savaranie. Po tym jak Agencja zniszczyła Łowcę, Sia ascendowała połączona z Tkaczką.
* Gabriel Septenas: kiedyś Mawirowiec, ale po tym jak się rzucił uratować savaran i został przez nich krzyżowo uratowany, pod wpływem Fidetis, został Unifikatą.
* Dorion Fughar: zastępca Mawira, który dobrze reprezentuje myślenie szefa - to kolejny test Saitaera. A Agencja wrobiła ich w to że oni robią sabotaże. Nie oni. Chce, by stacja była szczęśliwa.
* Kaldor Czuk: starszy wojskowy, wygląda na 60 lat. Pokazał lokalizację Łowcy i miejsca jego manifestacji, nie wie co to jest. Dostarczył Agencji 4 Teraquidowców do pomocy w zniszczeniu Łowcy.

## Frakcje

* Con Szernief Querenci: niewiele się dowiadują, ale udaje im się pozyskać nagrania wskazujące że coś jest tu dziwnego
* Con Szernief Arystokraci: frakcja zostaje praktycznie nieaktywna. Część trafia do Popularyzatorów, część się rozprasza.
* Con Szernief Mawirowcy: budują swoją frakcję jako grupę bojową. To kolejny Wielki Test Saitaera, który zesłał im potwora - i Mawir go zniszczył a nawet podjadł.
* Con Szernief Justarianie: ich przekaz, że to wszystko wina Rady ma coraz większy sygnał i znaczenie.
* Con Szernief Unifikaci: najbardziej wzmacniana frakcja, Fidetis im najmocniej pomaga. Celem jest integracja i zdobycie najlepszych rzeczy i cech savaran i drakolitów. (Ikta-04, Teriman Skarun)
* Con Szernief Oszczędnościowcy: Sia-03; pragnie zredukować koszty za wszelką cenę. Robią sabotaże i uszkodzenia rzeczy zżerających za dużo energii i nie będących kluczowe dla stacji.
* Con Szernief Biosynty: Łowca na nich poluje bo nie pasują do sieci Tkaczki; Delgado skutecznie zniszczył Łowcę kilka już razy, ale jest coraz bardziej uszkodzony.
* Con Szernief Rada: nie wiedzą jak radzić sobie z zastałą sytuacją; skupiają się na przetrwaniu osłaniani przez Teraquid.
* Con Szernief Teraquid: Chronią Radę przed Łowcą i ogólnie skupiają się na osłonie czego się da i 'co do cholery tu się dzieje'. Osiedlili się na Szernief po turbulencjach po sugestii Agencji.
* Con Szernief Popularyzatorzy: przede wszystkim Aerina; skupia się na przyciąganiu ludzi do stacji (it is awesome!)

## Fakty Lokalizacji

* CON Szernief: savarański rytuał Krwi doprowadził do sprzecznych manifestacji Fidetis i polowania na Radę przez Łowcę. Szczęśliwie, bez szczególnych zniszczeń doprowadzona do działania.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Kalmantis
            1. Sebirialis, orbita
                1. CON Szernief

## Czas

* Opóźnienie: 133
* Dni: 2

## INNE
### Potwory na sesji

* Istota Fidetis
    * aspekt "Tkaczka"
        * naprawia uszkodzone losy
        * stawia Compel
        * umieszcza ludzi tam, gdzie mogą przynieść największe korzyści
    * aspekt "Łowca"
        * rekonfiguruje się i usuwa 'niepodporządkowane' jednostki
        * usuwa 'niewłaściwe' osoby w sposób zasadny

