## Metadane

* title: "Sklejanie Inferni do kupy"
* threads: legenda-arianny
* gm: żółw
* players: fox, kapsel

## Kontynuacja
### Kampanijna

* [211208 - O krok za daleko](211208-o-krok-za-daleko)

### Chronologiczna

* [211208 - O krok za daleko](211208-o-krok-za-daleko)

## Plan sesji
### Co się wydarzyło

* Elena odwiedza Ariannę. Chce opuścić Infernię. I ratować wszystko. Nie ma dla siebie tu miejsca.      DONE
    * Ixiońska korozja Eleny        DONE
* Kasandra Destrukcja Diakon zwabia Eustachego w pułapkę. Dla Leony.            DONE
    * Leona chce URATOWAĆ Eustachego używając emitera Esuriit.
    * Eustachy FAKTYCZNIE zauważa że jest coś dziwnego z działkami i systemami

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Noc Eustachego:

Po poprzednich wydarzeniach, Klaudia na szybko poprosiła Ariannę o możliwość udania się na K1. Musi zdobyć pewne informacje, musi szybko dowiedzieć się co tu się stało i dzieje - może jest w stanie szybko coś zrobić, pomóc i zadziałać. Arianna się zgodziła. Lepiej mieć wiedzę Klaudii.

Ogólnie, Infernia jest w doku na szybko i wszystko działa. Jest bardzo ciężko uszkodzona i ledwo aktywna, ale jest. Diana flirtuje z Eustachym - jest grzeczna i nic dziwnego nie robi, ale po prostu nie rozumie co on od niej chce. Infernia chce z Eustachego zrobić Admirała Floty. Nie wie co to znaczy "Admirał Floty", ale to brzmi jak coś ważnego. Więc Infernia uważa, że Eustachy powinien owym admirałem floty być. 

Z tego Eustachy ma prosty wniosek - Infernia jest "słodka". Się stara. Próbuje mu pomóc Ale potrzebuje edukacji. Mimo swojej wiedzy o komputerach i systemach, Infernia/Diana nie rozumie życia.

Noc. Eustachy śpi na stacji (oczywiście, po co ma spać na Inferni? - mimo lekkich protestów Diany). I obudziła go Diana przez sprzężenie. Dwóch załogantów Inferni - Rafał i Marek - się biją. Eustachy nie do końca rozumie o co chodzi, czemu się biją i czemu Diana go budzi. Diana radośnie zauważa, że biją się chyba o nią. Bo Marek chce zejść z Inferni. A Rafał nie pozwala. Ale Marek chyba chce opuścić Infernię na stałe - a przynajmniej to wynika z ich rozmów.

Nie, Diana nie jest szczególnie składna.

Śpiący Eustachy "Mają się nie bić na statku". Diana spytała czy może ich zjeść? NIE! Nie zjadamy ludzi na Inferni! Diana lekko protestuje, to jest przydatne i zasila jej główne systemy. Eustachy. Się. Nie. Zgadza. Diana nie oponuje. Eustachy wzdycha ciężko - niech Diana ich zamknie w celi. On tam pójdzie i się tym zajmie. Ale wpierw wyjaśnia Dianie, że nie mogą wszystkiego pożerać, nie jest Serenitem. Diana na to "Serenit też pożremy. Jak go spotkamy to pożremy". Eustachy jest weteranem. On rozumie, że Diana jeszcze nie ogarnia. Infernia nie ma SZANS z Serenitem...

Eustachy idzie do tych w celach których zamknęła tam Infernia. Rafał, pierwszy. Jest spokojny i dość szczęśliwy, jego oczy żarliwie pałają światłem fanatyzmu. Eustachy już ma kropelkę. Rafał tłumaczy, że Infernia jest statkiem błogosławionym, że to nowy element nowego, lepszego świata. Eustachy... on... rozumie. Ok. Niech Rafał robi swoje. Rafał entuzjastycznie przytaknął (a Diana szepcze radośnie Eustachemu do uszka, że Rafał MA W KAJUCIE FIGURKĘ ARIANNY I SIĘ DO NIEJ MODLI!). Diana nie rozumie implikacji, Eustachy się trochę martwi.

Potem Eustachy odwiedził Marka w celi. Marek jest przerażony. Rozpaczliwie próbuje uciec ze statku. "NIECH SIĘ PAN ZLITUJE! TO SERENIT!". Eustachy chce go przekonać - ci przerażeni może trafią na Tivr. Jako rotacja. Tam odbębni 2 tygodnie, tu tydzień. By zobaczył, że to go nie pożre, że to jest bezpieczne.

Tr (niepełne) Z (zaufanie) +2:

* X: gość panikuje, trzęsie się, płacze
* X: jego przyjaciel jest tym "bez twarzy" i on mu się śni... (nie śni. Diana robi tak jak jest Leona)

Czekaj, co? Diana wyjaśnia z uśmiechem, że ona robi tak, że jak on miał przyjaciela i za nim tęskni to ona syntetyzuje jego twarz ze ściany. Jak Marek się budzi, to pierwsze co widzi to twarz swojego przyjaciela w ścianie, ale jak mrugnie, to to znika. Eustachy ma XD. Spacyfikował Dianę. Niech tak nie robi. Diana w konfuzji - przecież Leona tak robi i ludzie to lubią. NIE. NIE LUBIĄ. Niech Diana nie robi NIC dziwnego. Diana, z lekkim nadąsaniem, ok.

A Markowi Eustachy tłumaczy i pociesza, że nie będzie widział twarzy. TO TYLKO PTSD. Więc jako kara - niech Marek i Rafał umyją szalet razem. Diana z pytaniem do Eustachego - czy jak się odwrócą to szalet znowu ma być brudny? NIE! Żadnych mindfucków! Diana nie rozumie, ale zrobi to, co Eustachy chce.

Nocne przygody Eustachego skończone...

Poranek Arianny:

Arianna z rana ma wizytę Eleny. Elena ma złamane morale. Chce opuszczenia Inferni. Arianna naciska - Elenie gdzieś indziej będzie lepiej? Elena pokazuje Ariannie, że nosi długie rękawice, bo jej żyły pulsują błękitno-zielonkawym światłem. Tylko twarz jest w miarę stabilna. W miarę. Elena nie jest już czarodziejką, jest viciniusem. Boi się, że zrobi komuś krzywdę. Boi się tego, co się staje Ariannie. Arianna skoroduje. Infernia zniszczyła Eustachego. A Elena nie chce być osobą, która musi zastrzelić Ariannę czy Eustachego.

Arianna się uśmiechnęła smutno do Eleny. Jeśli przesadziła, skorodowała itp - zabije ją nie Elena a Leona. Elena nie mogła zaprzeczyć.

Arianna wyjaśniła Elenie, że ma dla Eleny plan - niech Elena stabilizuje ludzi na Tivrze. Niech działa tam. Niech zapewni, by było bezpieczne miejsce dla lojalnych agentów Orbitera. A ona, Arianna, opanuje Infernię. Elena powiedziała jednak, że odmawia działania z Eustachym.

TrZ (Elena ufa Ariannie) +3:

* Vz: Z uwagi na to, że Ariannie ufa, zastanowi się. -> zostanie na Tivrze. Jej noga nie stanie na Inferni.

Elena poprosiła Ariannę o ściągnięcie swojego Eidolona sentisieciowego. I o ściągnięcie tu Tivra. Arianna się zgodziła.

Gdy Elena opuściła kwaterę Arianny, Arianna poszła na Infernię, do Eustachego. Chce wydobyć Gwiezdnego Ptaka i Eidolona Eleny.

* Eu: "Infernia nie zniszczy statku z czystej złośliwości bo jej nie pozwolę"
* Ar: "Wyciągnij jej tego Gwiezdnego Ptaka - niech ma jednostkę przy sobie, może nas szybko wspomóc"
* Eu: "Czemu pozbywamy się najlepszej jednostki?"
* Ar: "Co chcesz osiągnąć trzymając ją tutaj?"
* Eu: "Elena jest zarażona... pewną chorobą... której nazwy nie wypowiadamy i nie chcę jej odciągać od głównej osi choroby - mnie?"
* Ar: "Z tą chorobą nie powinno być problemu jak będzie daleko - jak trzeba, będą filmy z Eustachym"
* Eu: "To zabolało..."

Podczas tej rozmowy Eustachy się dowiedział, ku swemu przerażeniu, że zgodnie z wieloma kiepskimi fanfikami on ma prawdziwą miłość - i jest nią Klaudia. Klaudia, która jest niedostępna i nie widzi Eustachego. Klaudia, która podobno lubi dziewczyny. I Eustachy za nią podąża jak ćma w ogień. I te fanfiki występują na Inferni... Eustachy, coś pomiędzy szokiem, irytacją i WTF wysłał wiadomość do Klaudii - te dane muszą być anihilowane, skasowane itp.

Tymczasem Diana zmaterializowała się za Arianną i powiedziała jej o szpiegach Rolanda. Że zeżarła 3 szpiegów. Ci pierwsi, dwaj, mieli za zadanie robić Ariannie zdjęcia z ukrycia, ukraść jej bieliznę itp. Ale ten mechanik którego znalazł jej Roland (brzydki i "stary" (40)) był szpiegiem Rolanda i jego zadaniem było monitorowanie stanu Inferni, zbieranie info o jej podróżach, jej podsystemach itp. Zupełnie inny cel. Nie sabotaż, ale dokładna obserwacja i monitoring sytuacji.

Diana powiedziała, że może zjeść Elenę jak Ariannie się ona znudzi.

* Arianna -> Diana: "Jeśli zjesz Elenę będziesz miała kij w dupie i nie spodobasz się Eustachemu, bo on kija w dupie nie lubi". Diana się wzdrygnęła. Na 100% nie zje Eleny.

Ok. Ale sprawy związane z figurką Arianny itp. są dla Arianny niepokojące. To jest coś, co Arianna chce omówić ze źródłem prawdy - Kamilem. Więc poszła z Kamilem Lyraczkiem porozmawiać.

Kamil przyznał się, że on stoi za figurkami. Współpracuje z Izą. Powiedział Ariannie o Arii Vigilus. Dychotomia Zbawiciel (Aria) - Niszczyciel (Eustachy). Arianna "czyli to ONI Cię nawrócili a nie Ty ich?!". Kamil na to, że z Izą zrozumieli prawdę. Nie był pełny, teraz jest. Arianna ma do myślenia - nie chce skończyć jako bogini...

Arianna -> Eustachy: zespół ma agonalnie niskie morale. Coś z tym wszystkim trzeba zrobić. Maria Naavas zaproponowała amnestyki - ale one nie zadziałają. Może feromony? Arianna się zgodziła, lekkie feromony - narkotyki są optymalne w tej sytuacji. Maria się lekko uśmiechnęła. Powiedziała, że Olgierd by się nie zgodził. Arianna powiedziała, że ona się zgodzi. Maria jest coraz bardziej przekonana, że jej miejscem jest Infernia.

"Ten statek żyje, ale to nie jest wada. Trzeba pogadać. Trzeba sprawić, by załoga uznała, że Infernia jest MIŁA."

Jak zatem uzupełnić załogę? Jak to zrobić, by to wszystko zadziałało? Możemy iść po rekrutacji kultystów, więźniami, ale chyba nie tędy droga. Chcesz się oglądać za plecy? Jak zatem można naprawić sytuację? Ale niech ludzie się dowiedzą "nasza kochana Anomalia Infernia" a nie ten potwór zjadający ludzi. Rekrutacja - uderzamy w statek piracki i rekrutujemy w ten sposób? ^^

Tak czy inaczej, Eustachy zaczyna edukację Diany. Po to, by Diana WYGLĄDAŁA na słodszą i poczciwą Anomalię Kosmiczną. Arianna -> Eustachy "kiedy jesteś miłą osobą"? Eustachy - gdy składa modele. To jest proces uspokajający modeli. Więc Eustachy składa model. Bo jakoś musi Dianę nauczyć...

Tr (niepełny) Z (bo naprawdę chce) +3:

* X: ona jest słodsza i poczciwsza, ale w taki brutalniejszy sposób. Myśli, że styl myślenia Eustachego jest popularny dla populacji. Żarty.
* V: TAK, Diana potrafi być słodsza i poczciwsza. W stylu Eustachego. Nie zachowuje się tak strasznie.
* X: dla nowo przybyłych ludzi to jest jeszcze straszniejsze. Diana w ludzkiej skórze. Odzywa się, próbuje być miła.
* X: "to nasza młodsza siostra Diana". Niektórzy którzy to łyknęli powodują bardzo, bardzo _creepy_ reakcję u innych ludzi. ONA MYŚLI, ŻE TAK JAK LEONA TAK LUDZIE POWINNI SIĘ ZACHOWYWAĆ.

Maria uniosła brew na to co się udało przekazać Dianie i kierunek, w który Eustachy idzie. Oook. Ale ma się to ludziom podobać i niektórym się będzie podobać. 

Maria przygotowuje odpowiednie feromony by ludzi przekonać, by ich "wzmocnić".

ExZM+3:

* Vz: efekt persystentny. Feromony Marii będą skutecznie wpływać na załogę. Dla ludzi to będzie uspokajające i w sumie fajne.

Przemowa Arianny - by ludzie dali szansę. By uspokoić. Przy konsultacji z Izabelą. Kultyści, noktianie, miłośnicy psychopatycznej Diany-lolitki.

TrZ (Iza jest mistrzynią) +3:

* ? :

ZGUBIŁEM INFORMACJĘ. ALE BYŁ OSTATECZNY SUKCES NA LOLITKĘ - DIANĘ.

Dobrze. Trzeba troszkę podreperować Infernię. Arianna jest zajęta, wysłała Eustachego. Eustachy poszedł więc po inżynierów. DAJCIE MNIE ICH. Z bazy. Spotyka Kasandrę Diakon, która chce zwabić Eustachego, że systemy defensywne są częściowo aktywne a częściowo nie. Eustachy zdecydował się z nią pójść. Z perspektywy Kasandry - Eustachego wciąga w pułapkę. Z perspektywy Eustachego, na serio coś z tą bazą jest nie tak...

TrZ+3:

* X: Wymaga ogromnego skupienia ze strony Eustachego. Coś jest nie tak. Eustachy łączy się z Infernią - coś jest nie tak.
* Xz: Pełna integracja z systemami. Coś wciąga Eustachego. Całe skupienie uwagi.
* V (+magia): Eustachy wyczuwa ixioński byt na pokładzie stacji. Gdzieś tu jest jakiś potwór.

"gdzieś tu jest potwór" - info do Arianny. Na stacji jest byt ixioński. Eustachy łączy się z Infernią - 

* O: Skażenie ixiońskie. NIE WYSZŁO. Coś wyssało energię magiczną. Stracona przytomność. Infernia nadaje SOS do Arianny.

ARIANNA USPOKAJA INFERNIĘ / DIANĘ! BO EUSTACHEGO MI ZJEDZĄ! Arianna próbuje maksymalnie uspokoić Dianę. Niech ta nic głupiego nie robi.

TrZ (bo ufa Ariannie) +3:

* Vz: Diana ufa, że Arianna uratuje Eustachego. Nie strzela. Nie włącza silników. Jest grzeczna.

Arianna szuka Leony. Leona po prostu jest nieobecna. Wyłączyła komunikatory. Następny cel Arianny - Elena. Ta reaguje. "Arianno?". "Spotkamy się na stacji - jestem gotowa. Ratujemy Eustachego." Elena jest sceptyczna - co się stało z Eustachym? Arianna nie wie, ale bez niego Diana jest niemożliwa do kontroli. Elena się skupia i wspiera. A Arianna szuka Kasandry i info o Kasandrze.

TrZ (bo Diana i Klaudia) +4:

* V: Info o Kasandrze. Kasandra jest osobą z wydziału wewnętrznego Stacji. Od dłuższego czasu raportowała, że coś jest nie tak. Też, że znikali ludzie, koty, szczury itp. Ale komputer nic nie pokazuje. Hestia pokazuje że jest OK. I Kasandra była nieco... zdesperowana.

Elena zaprowadziła Ariannę w jedno z "cichych miejsc". Roboty sprzątające. Arianna chce ożywić roboty sprzątające. Małe rzeczy. By ci na stacji zobaczyli że coś jest nie tak.

TrZM+3:

* X: Fala ożywiająca poszła bardzo szeroko. Sporo mamy ożywionych rzeczy. Elena ma minkę typu XD z anime.
* Vm: one są niegroźne dla Arianny i NIKT nie może zaprzeczyć że coś się tu odpierdala. AGRESYWNIE SPRZĄTAJĄCE ROOMBY!

Hestia zaraportowała "problematyczną sytuację". Czyli przynajmniej tyle. Arianna i Elena korzystają z okazji i szukają gdzie Eustachy może być.

Tymczasem Eustachy. Budzi się. LEONA i Kasandra. Leona ma podłego, paskudnego czerwia Esuriit, który Leonę koroduje, ale zada STRASZNE obrażenia Eustachemu. EUSTACHY GRA NA ZWŁOKĘ! NIE CHCE CZERWIA! "Skąd masz czerwia itp"

Tr (niepełny) Z (Kasandra jest niepewna + Leona Cię lubi) + 4 (GDZIE JESTEŚMY!):

* X: CZERW JEST STYCZNY (Leona przyłożyła). Ja pierdolę jak to boli. (+2Vg)
* V: Kupujesz czas. Leona NIE CHCE robić krzywdy - chce wyleczyć. Kasandra coraz mniej przekona. Eustachy "JA BYŁEM ZLINKOWANY Z INFERNIĄ!"
* V: OPANOWANA DIANA. Diana nie robi nic głupiego. Eustachy "CHCESZ POMAGAĆ, NIE PRZESZKADZAJ! Bo nikt nie wyjdzie żywy!" Dwa wdechy, namierz, pomagaj Ariannie... NIC NIE RÓB NA SIŁĘ! JAK W PANIKĘ, przegraliśmy. Eustachy wyjaśnia Leonie (+3Vg). Jeśli rozerwie połączenia, jakby zrzuciła kaganiec Dianie. Eustachy był gotów poświęcić siebie. Dał Leonie do myślenia. Kasandra chce przerwać operację - Leona skutecznie i szybko unieszkodliwia Kasandrę.
* V: Dość czasu jest kupione.
* V: Eustachy / Infernia rejestrują to dla potomności. Jest TWARDY dowód, że Infernia jest pod kontrolą - nic złego nie zrobiła. A nigdy nie była pod taką presją.
* V: NIE TRZEBA Z Leoną walczyć. Eustachy udowodnił, że kontroluje Infernię, że mimo presji czerwia nic złego się nie dzieje. Że to teraz on. I że gdzieś tutaj jest PRAWDZIWA anomalia ixiońska. I bez Inferni by o tym nie wiedział.

Leona, pozwalając Ariannie rozplątać Eustachego "te czarodziejki są takie naiwne".

Elena nie zbliża się do Eustachego. Mówi Ariannie, że jeśli na stacji jest potwór, ona go znajdzie. Arianna kazała Elenie uważać. Elena będzie ostrożna. W końcu Esuriit i Ixion, prawda?

Już na pokładzie Inferni Arianna -> Adam Szarjan "jest problem". Macie Skażenie ixiońskie. Przynieśli też Kasandrę. Adam na to: "Co jej się stało?" A Leona: "Spotkała się ze mną. Tzn, uratowałam ją" (przypominając sobie o kłamstwach). Arianna - "wyrwały się roboty spod kontroli, Eustachy podłączył się do systemów i ixiońsko coś go zaatakowało." Adam to łyknął - wszystko trzyma się kupy. Więc jak tylko Kasandra się obudzi i Klaudia wróci, może jak Elena coś znajdzie... skupią się na tym jak znaleźć ixiońskiego potwora na stacji.

## Streszczenie

Ixioński wariant Inferni okazuje się całkiem słodki i pod kontrolą Eustachego. Morale Inferni jest w ruinie. Z pomocą Marii Arianna przesuwa kulturę Inferni w stronę "kultyści + noktianie + miłośnicy psychopatycznej lolitki Diany". Eustachy wpada w pułapkę Leony, która chce go wyleczyć czerwiem Esuriit - ale Eustachy przekonuje ją, że ma Dianę pod kontrolą. Jednak gdzieś w tle znajduje się prawdziwy ixioński potwór na stoczni Neotik.

## Progresja

* Diana d'Infernia: jest słodsza i poczciwa w brutalny sposób, co dla nowych jest STRASZNE. "Młodsza siostrzyczka", creepy like fuck. W stylu Eustachego. Ale JEST słodsza.
* Elena Verlen: NOWA KARTA POSTACI. Skażenie Ixionem i Esuriit. Zmiana wyglądu. Nie ma dla niej powrotu do Verlenlandu. Nie jest już czarodziejką.
* OO Infernia: Eustachy udowodnił, że jak był torturowany przez Leonę to jednak Infernia nie zrobiła nic głupiego. Więc nie jest tak niebezpieczna dla załogi jak się wydawało.

### Frakcji

* .

## Zasługi

* Arianna Verlen: zatrzymała Elenę na Inferni (dokładniej: Tivrze), uspokoiła Dianę która chciała niszczyć by ratować Eustachego, poznała prawdę od Kamila odnośnie swej przyszłości (awatar Vigilusa), decyzja o feromonach - nowa kultura Inferni. I zrobiła panikę w Stoczni Neotik - czy Hestia to pokaże?
* Eustachy Korkoran: nie udało mu się ustabilizować załogi, ALE znalazł ixiońskiego potwora analizując systemy Stoczni (wpadając w pułapkę Kasandry/Leony). Gdy był związany, przekonał Leonę, że kontroluje Dianę. Więcej - zarejestrował dla potomności, że FAKTYCZNIE Diana nic głupiego nie zrobiła gdy był torturowany.
* Diana d'Infernia: przekonana, że może POŻREĆ Serenit; nieco nie ma balansu między wiedzą i mocą. Dość słodka, jak na ixiońską stabilną efemerydę. Modeluje charakter po Leonie; psychopatyczna słodka lolitka. 
* Elena Verlen: chciała opuścić Infernię po tym wszystkim; Arianna przekonała ją, że jak będzie dowodzić Tivrem to pomoże Orbiterowi. Poważnie Skażona Ixionem i Esuriit; nie ma statusu już czarodziejki. Nie ma dla niej powrotu do domu.
* Roland Sowiński: NIEOBECNY, ale wprowadził dwie kategorie agentów na Infernię. Okazuje się, że zewnętrzna kategoria (leszcze) mają być "typowi", ale wewnętrzna kategoria - dokładna analiza czego Infernia chce się dowiedzieć, systemów Inferni itp.
* Kamil Lyraczek: wyjaśnił Ariannie, że z Izą tworzą kult dookoła Arianny (Vigilusa) i Eustachego (Nihilusa). Figurki itp. Jest za głęboko jej oddany. Arianna się zmartwiła...
* Maria Naavas: zaproponowała Ariannie feromony by pomóc załodze przetrwać na Inferni (uspokajające feromony "dla kotów"). Ogromny sukces - adaptacja udana, załoga może działać na Inferni, choć kultura wyszła nieco dziwnie.
* Kasandra Destrukcja Diakon: wydział wewnętrzny Stoczni Neotik. Zaniepokojona tym, że ludzie zachowują się dziwnie i Hestia coś ukrywa, współpracuje z Leoną by porwać Eustachego. Znokautowana przez Leonę gdy Eustachy udowodnił, że nie mógł stać za zniknięciami ludzi.
* Leona Astrienko: zapolowała na Eustachego by go uratować od ixionu; zmanipulowała Kasandrę Diakon, by ta jej pomogła. Zastawiła pułapkę, używając czerwia Esuriit ZRANIŁA Eustachego. Dała się jednak mu przekonać, że on kontroluje Dianę. Została jako morderca magów; ktoś musi skończyć Eustachego i Ariannę jak nie będzie wyjścia...
* Adam Szarjan: przekonany przez Ariannę, że ten ixioński potwór na Neotik jest prawdziwy i nikt nie wie o jego istnieniu bo potencjalnie współpracuje z Hestią.
* OO Infernia: żywy, regenerujący się ixioński statek. Znajduje się w zewnętrznym, ixion-friendly doku dobudowanym i niestycznym z Neotik.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Pierścień Zewnętrzny
                1. Stocznia Neotik: gdzieś na stoczni znajduje się prawdziwy ixioński potwór a jej Hestia wyraźnie jest poza kontrolą kogokolwiek.
                    1. Zewnętrzny dok ixioński: niepołączony fizycznie ze Stocznią, w którym leży Infernia i próbuje regenerować.
 
## Czas

* Opóźnienie: 1
* Dni: 2

## Konflikty

* 1 - Eustachy chce przekonać załoganta - ci przerażeni może trafią na Tivr. Jako rotacja. Tam odbębni 2 tygodnie, tu tydzień. By zobaczył, że to go nie pożre, że to jest bezpieczne.
    * Tr (niepełne) Z (zaufanie) +2
    * XX: gość panikuje, trzęsie się i płacze; Diana ma chore pomysły i dobrze się bawi modelując LEONĘ.
* 2 - Arianna wyjaśniła Elenie, że ma dla Eleny plan - niech Elena stabilizuje ludzi na Tivrze. Niech nie odchodzi.
    * TrZ (Elena ufa Ariannie) +3
    * Vz: Z uwagi na to, że Ariannie ufa, zastanowi się. -> zostanie na Tivrze. Jej noga nie stanie na Inferni.
* 3 - Eustachy zaczyna edukację Diany. Po to, by Diana WYGLĄDAŁA na słodszą i poczciwą Anomalię Kosmiczną.
    * Tr (niepełny) Z (bo naprawdę chce) +3
    * XVXX: jest słodsza i poczciwa w brutalny sposób, co dla nowych jest STRASZNE. "Młodsza siostrzyczka", creepy like fuck. W stylu Eustachego. Ale JEST słodsza.
* 4 - Maria przygotowuje odpowiednie feromony by ludzi przekonać, by ich "wzmocnić", że Diana jest spoko.
    * ExZM+3
    * Vz: Efekt persystentny. Feromony Marii będą skutecznie wpływać na załogę. Dla ludzi to będzie uspokajające i w sumie fajne.
* 5 - Z perspektywy Eustachego, na serio coś z tą bazą jest nie tak - hackuje systemy by sprawdzić co nie działa
    * TrZ+3
    * XXzVO: ogromne skupienie Eustachego (Leona go zaszła), pełna integracja z Eustachym (bazyliszek Leony), Eustachy poczuł ixioński byt, Paradoks i Leona go zneutralizowała
* 6 - ARIANNA USPOKAJA INFERNIĘ / DIANĘ! BO EUSTACHEGO MI ZJEDZĄ! Arianna próbuje maksymalnie uspokoić Dianę. Niech ta nic głupiego nie robi.
    * TrZ (bo ufa Ariannie) +3
    * Vz: Diana ufa, że Arianna uratuje Eustachego. Nie strzela. Nie włącza silników. Jest grzeczna.
* 7 - Arianna szuka Kasandry i info o Kasandrze
    * TrZ (bo Diana i Klaudia) +4
    * V: pełne info o Kasandrze - wydział wewnętrzny Stoczni
* 8 - Elena zaprowadziła Ariannę w jedno z "cichych miejsc". Roboty sprzątające. Arianna chce ożywić roboty sprzątające. Małe rzeczy. By ci na stacji zobaczyli że coś jest nie tak
    * TrZM+3
    * XVm: sporo ożywionych rzeczy, ale niegroźne. Faktycznie, alarm / panika na stacji.
* 9 - Leona ma podłego, paskudnego czerwia Esuriit, który Leonę koroduje, ale zada STRASZNE obrażenia Eustachemu. EUSTACHY GRA NA ZWŁOKĘ! NIE CHCE CZERWIA!
    * Tr (niepełny) Z (Kasandra jest niepewna + Leona Cię lubi) + 4 (GDZIE JESTEŚMY!)
    * X: czerw przyłożony, STRASZNE cierpienie. +2Vg
    * VVVVV: kupiony czas, Diana opanowana, rejestrowanie tego dla potomności (Infernia jest pod kontrolą), Leona odpuściła walkę.
