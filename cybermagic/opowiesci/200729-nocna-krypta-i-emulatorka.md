## Metadane

* title: "Nocna Krypta i Emulatorka"
* threads: triumfalny-powrot-arianny
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [200722 - Wielki Kosmiczny Romans](200722-wielki-kosmiczny-romans)
* [191218 - Kijara, córka Szotaron](191218-kijara-corka-szotaron)

### Chronologiczna

* [200722 - Wielki Kosmiczny Romans](200722-wielki-kosmiczny-romans)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* Laura jest Skażona - Kijara vessel.
* Kijara powraca, z własną Emulatorką.
* Nocna Krypta zapuszcza stałe korzenie w Ariannie.
* Mirela zostaje na Nocnej Krypcie by chronić innych.

Czego chcą strony:

* Kijara -> Laura: vessel; dzięki Laurze wróci.
* Laura -> Kijara: dzięki Kijarze zniszczy Eternię. Zniszczenie Eterni > contained Kijara. W końcu Orbiter sobie poradzi.
* Mirela, Zespół -> Laura: uratować, opanować, złapać i wykraść z Krypty.
* Laura -> Mirela: corrupt, pokazać jej PRAWDĘ.
* Laura -> Zespół: uciec z Krypty
* Krypta -> Mirela, Laura: NAPRAWIĆ
* Krypta -> Mirela, Arianna: infekcja Oliwią Kurint (eks-kapitanem Krypty)

Taktycznie:

* Laura: tworzy thralle, asymiluje ludzi i dąży do zdobycia Supercore.
* Krypta: chroni wszystkich, chce Laurę wpakować do biovatu i wyłączyć. Wie, że Laura nie ma mocy na Supercore.

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Punkt zero

* **Laura** zniknęła w Nierzeczywistości, po otrzymaniu błędnej Mapy Eterycznej, w (200129-nieudana-niechetna-inwazja).
* **Kijara** d'Esuriit, po wymanewrowaniu jej ze stacji Dorszant wpadła w Nierzeczywistość, w (191218-kijara-corka-szotaron).
* Laura i Kijara się gdzieś jakoś spotkały. Laura nie była w stanie pokonać nanitkowego koszmaru. Kijara się w niej zaszczepiła. Laura jest Skażona.
* Nocna Krypta odzyskała Laurę i wpakowała ją do siebie na pokład. Próbuje odciąć Kijarę, ale to tak średnio wychodzi.
* Damian Orion wykrył, że sygnatura Laury jest w okolicach Nocnej Krypty. Poprosił Ariannę, by ta przyzwała Kryptę.

## Misja właściwa

Na pokładzie OO Minerwa, Arianna, Eustachy, Klaudia, Leona i Martyn. Damian dał im krótki briefing - trzeba wezwać Kryptę i uratować jego Emulatorkę, Laurę. Ale jak Arianna ma wezwać Kryptę? Nie jest to tak łatwy problem jak by się mogło wydawać. Przy okazji, Damian pokazał im Klaudię - jedna z Emulatorek została wysłana, by porwać i przyprowadzić Klaudię dla Arianny. Ku wielkiej uciesze Eustachego.

Przede wszystkim, potrzebna jest konkretna lokalizacja - im bliżej Obłoka Lirańskiego i Anomalii Kolapsu, tym łatwiej będzie wezwać Kryptę. Tak więc Damian poleciał do Obłoku Lirańskiego - miejsca, gdzie stężenie magii jest wyższe i jest wysoka szansa na to, że uda się wezwać Kryptę. Dalej jednak brakuje im czegoś co zdecydowanie pomoże.

Pytanie - kiedy najczęściej Krypta się pojawia? Odpowiedź - gdy ktoś jest bardzo zagrożony. Zwłaszcza Arianna. Eustachy dla żartu zaproponował, by wyrzucić Ariannę ze śluzy. Ku przerażeniu wszystkich, Damian wziął to na serio - można zdehermetyzować jej pancerz. Ariannie pomysł się bardzo nie spodobał. Wtedy Damian zaproponował swoją Emulatorkę, Mirelę. A Klaudia po przejrzeniu systemów na Minerwie uznała, że warto wykorzystać Pain Module - podniesienie poziomu energii i cierpienia powinno wezwać Kryptę.

Mirela zgłosiła się na ochotniczkę. Damian zaaprobował. Arianna uznała, że wszyscy są niespełna rozumu - i Eustachy potwierdził że uważa to samo. On zaproponował przestępców. Damian zauważył, że nie mają dostępnych żadnych a Emulatorka w kosmosie przeżyje krótki czas.

No nic, trzeba to zrobić. Arianna połączyła się z systemami i spróbowała wezwać Kryptę. I wystrzelili Minerwę ze śluzy.

* X: Krypta opiera się powrotowi; walczy z Arianną. To jest dziwne; nie będzie dało się wprowadzić oddziału na pokład, max 5 osób
* VO: Krypta zaczęła się materializować, z energią Esuriit. Jej ŁZy walczą z nanitkami próbującymi pożreć Kryptę.
* XX: Niestety, Krypta jest w formie bojowej; walczy z tymi cholernymi nanitkami. Nie mają jak wejść czy wyjść.

Gdy jedna z pozyskujących ŁeZ próbuje przechwycić Mirelę, Arianna wpadła na szatański plan. Torpedy abordażowe które mają odbić się od ekranu ochronnego i doprowadzić do tego, żeby ŁZy przechwyciły Zespół. Droga w jedną stronę, ale się udało (VXVV) - nie cały sprzęt, ale wystarczająco dużo udało się na Kryptę ze sobą zabrać. Gdy ŁZy zaczęły nieść wszystkich do containment chambers, Eustachy odpalił miny klasy Claymore, kierunkowe - rozwalił ŁZy, a Zespół oderwał się i szybko zwiał do jednego z korytarzy.

Są na pokładzie, mają większość sprzętu. Cała szóstka jest gotowa do działania (A, E, K, L, Martyn, Mirela). Nie oznacza to, że są gotowi - ale przynajmniej mają wszystko czego potrzebują.

Pierwsze kroki na pokładzie Nocnej Krypty nic nie różniły się dla Arianny od ostatniej wizyty. Ot, Krypta jest nieco dziwniejsza, ale to nie jest powód by się martwić. Za to niedługo później Eustachy zauważył kilka osób przed sobą. Pięciu ludzi. Spojrzeli na Zespół i zaczęli się wycofywać. Po chwili zobaczyli coś z prostopadłego korytarza i zaczęli uciekać. Stamtąd wypadł inny człowiek i zaczął ich gonić.

Zespół poszedł ostrożnie za nimi. Zobaczyli jak ten goniący człowiek zaczął pożerać (fizycznie) jednego z tych na ziemi, jeszcze żywego, a pozostali próbowali rozpaczliwie otworzyć zamknięte drzwi. Eustachy strzelił w pożerającego; ogłuszył ghula. Martyn, Eustachy, Klaudia i Leona zaczęli zbliżać się do nieszczęśników.

Klaudia zaczęła od analizy "ghula".

* V: Delikwent jest zdecydowanie Skażony Esuriit. Esuriit jest w jego ciele, żyłach, kościach, ustawia jego układ nerwowy.
* V: Ma wszczepione nanitkami obwody zawierające dziwne, nienaturalne elementy Esuriit.
* XX: To jest zaraźliwe. Bardzo. Może wstrzyknąć Esuriitowe nanitki do ofiary.

Zanim ghul dał radę skrzywdzić Klaudię, Leona go zabiła. Kilka strzałów, głowa + usunięcie motoryki ciała. Jak wszyscy na nią spojrzeli zdziwieni, uśmiechnęła się. Wie jak się walczy z potworami. Zabiła też tego rannego, którego zaczął ghul pożerać; koleś TEŻ był Skażony nanitkami które już zaczęły przebudowę.

Nie pomogło to na komunikację z czterema pozostałymi ludźmi. Są w panice. Jednak Ariannie udało się ich trochę uspokoić i zapytała ich skąd są, co pamiętają i co tu robią. Zaczęli tłumaczyć - nie byli w ogóle w kosmosie tylko na planecie, zidentyfikowali to jako sektor Lacarin. 

FLASHBACK. SYGNAŁ. Nocna Krypta zbudowała im halucynację. Są w biurze na planecie, zaczyna się wyciek gazu. Praktycznie nie mają czasu. Eustachy natychmiast przygotował się do działania - wybił szybę (V), użył izolatora (taśma izolacyjna) by natychmiast uszczelnić gaz (V). Arianna zrobiła szybki przewiew. Udało im się pokonać halucynacyjny flashback Krypty.

No, to było ostre. Czyli Krypta jest niesamowicie niebezpieczna z tym wszystkim na pokładzie. Bardziej niż kiedykolwiek.

Zespół przeszedł się po okolicy - udało im się zebrać 14 cywili. Z różnych sektorów. Żywe, interesujące źródło wiedzy. To jest coś! Ale nie są w stanie znaleźć Emulatorki, jeśli będą mieć ze sobą cywili. Tak więc Zespół wybrał pewien konkretny fragment Krypty, niedaleko ściany wyjściowej. Eustachy go przygotował do eksplozji, by mogli po prostu rozwalić fragment kadłuba. Dodatkowo ufortyfikowali to tak, by ludzie byli bezpieczni - a Klaudia i Arianna przygotowały detektory i reduktory sygnału i Pryzmatu.

* XX: Niestety, nie mają dużo czasu aż Potwory się zorientują co tu się dzieje i znajdą to jako źródło sygnału.
* XV: Defensywy Eustachego poradzą sobie z małymi grupkami, ale nie z potężnymi bossami.
* XVX: Są chronieni. Na razie.
* XXVV: Jeśli potwory przyjdą, mechanizm autowysadzenia. Wszyscy zginą. Coup de grace. Dehermetyzacja kadłuba.

Nikomu nie podoba się ten wynik, ale nie mają nic lepszego. Nie mają ani składników, ani części, ani możliwości zatrzymania tego, co tam się dzieje.

Poszli za detektorem Mireli. A jak to zrobić by się nie zgubić?

"Zostawiam ścieżkę za sobą. Taką nić Ariadny. Z materiałów wybuchowych." - Eustachy, oczywiście

Detektor wskazał jeden z biolabów. I faktycznie, znaleźli tam Laurę - Laura jest przytomna, aktywna, i właśnie wyciąga kogoś z biovata. Niestety, jest silnie Skażona Esuriit. Ona jest "pacjentem zero". Ale w Laurze pozostało sporo Laury; ona nie chce robić krzywdy nikomu tutaj. Skupia się na zniszczeniu Eterni. Chce zainfekować Eternię używając Kijary. 

Mirela powiedziała Laurze, że ta musi wrócić do Damiana. Laura się zgodziła - ale WPIERW Eternia zostanie zniszczona.

HALUCYNACJA. Są w Eterni. Uciekają. Są porządkowymi Eterni i uciekają przed opętaną przez Esuriit Laurą, która była niedawno torturowana przez nich. I która ich atakuje i pożera. Mirela natychmiast przypomina sobie jak to szło i zatrzymuje się, by walczyć z Laurą - kupić innym czas. Klaudia, mająca największe doświadczenie z konstruktami astralnymi, tworzy wizję jak Damian Orion się pojawia by uratować Zespół przed Laurą.

* V: Udało się. Udało się doprowadzić do tego, by Damian uratował Mirelę. Laura jest... niemożliwa do pokonania przez Mirelę. Delta(Laura, Mirela) jest jak Delta(Mirela, Leona).
* VX: Mirela jest tylko lekko ranna - psychiczny cios.

Krypta przesunęła ich w inną HALUCYNACJĘ - tym razem Mireli.

Mirela stoi, wśród zgliszczy. We wściekłości i nie kontrolując emocji Mirela zabiła swoją rodzinę.

* O: Helena skupiła swoją wolę w tym miejscu, stabilizując sytuację.
* X: Mirela jest Wstrząśnięta. Faktycznie, Laura zaczyna do niej trafiać. Mirela może... musi coś zmienić...
* VV: Eustachy przejął rolę Damiana. Zespół przejął kontrolę nad wizją. Wyciągnęli Mirelę z tego. Nie może tu być.

Mirela zaczęła przekonywać Laurę. Zespół próbował Laurę zatrzymać. Laura pokazała, że Mirela nie jest w stanie z nią walczyć - siła ognia Laury jest niewyobrażalna, ciało Emulatorki wzmocnione przez nienawiść Kijary. Mirela kupuje czas, ale przegrywa.

Zainterweniowała Nocna Krypta/Helena.

HALUCYNACJA: Arianna siedzi na mostku Inferni. Koło niej siedzi kapitan Oliwia z Nocnej Krypty. Tłumaczy spokojnie, że Krypta ma pasożyta Esuriit. Pasożyt może chcieć wygrać, ale Krypta może go usunąć - tylko, że wtedy zginie mnóstwo ludzi i nie ma pewności, że się uda. Arianna dostała od Oliwii zadanie - niech wycofa kogo się da, by Krypta mogła usunąć wszelkie ślady Esuriit.

Arianna wie jednak, że jeśli ona ewakuuje Laurę z Krypty, to przesunie koszmar Kijary na Sektor Astoriański. Arianna się bardzo, bardzo zafrasowała. Nie wie co może zrobić. Nic nie udając, ale jednak widok Laury/Kijary wstrząsnął arcymag. Czegoś takiego jeszcze nie widziała.

...

Tymczasem jedynym, co obudził się wychodząc z biovatów jest Eustachy. Wypadł na ziemię, nie dał się ani zainfekować ani wsadzić robotom. Jak to Eustachy, wysadził to, co próbowało go wsadzić do biovata i wypełzł. Po chwili znalazł Klaudię, wyjął ją z biovatu i wlepił jej LEPISZCZE ZŁA. Potem zrobił to samo z Arianną (z ogromną radością - on po prostu uwielbia robić lepiszcza).

Arianna, Eustachy i Klaudia weszli w ostrą kłótnię. Klaudia chciała ratować Laurę i Kryptę. Arianna - trzeba ZNISZCZYĆ Kryptę do ostatniego atomu, doprowadzić do tego, że faktycznie uda się zniszczyć wszelkie ślady Esuriit - a na pewno Kijary - z Sektora Astoriańskiego. Eustachy się z nią zgadza. Klaudia chce ratować Laurę - ważniejsze są relacje z Damianem Orionem. Mirela stałaby po stronie Klaudii, ale nie wie o dyskusji.

Arianna jest kapitanem. Podjęła decyzję. Trzeba ratować Sektor Astoriański.

Wyciągnęli wszystkich których się dało i podreperowali Mirelę. Mają swój oryginalny oddział i nikogo więcej. Poczekali pół dnia, aż Laura zlokalizowała i przeszła przez zabezpieczenia. Alarmy Eustachego pokazały, że cały ich mechanizm obronny dla cywili się rozpadł wobec istoty klasy Laury. Poświęcili wszystkich cywili, ale poszli do tamtego miejsca i w kontrolowany sposób odpalili mechanizmy ewakuacyjne. Jak tylko wylecieli poza zasięg Krypty, przechwyciła ich OO Minerwa...

Damian Orion nie docenił. Nie był zadowolony, uważał, że Arianna podjęła błędną decyzję. Ale zaakceptował. Nocna Krypta się zdematerializowała, Damian przetransportował (po trzydniowym skanie i dekontaminacji) Zespół na Kontroler Pierwszy.

A tam - spotkanie Arianny z ADMIRAŁEM ANTONIM KRAMEREM.

Admirał zaczął Ariannę chwalić - jak ładnie rozwiązała problem z plotkami, jak dobrze to zrobiła, że nic nie popsuła. A Arianna stoi tam, czuje się jak ostatni śmieć - bo właśnie poświęciła około 50 osób zgodnie z tym co mówiła jej Oliwia d'Nocna Krypta...

* V: Kramer zdobędzie prawo do użycia Castigatora.
* XV: Niekwestionowane dowody Arianny na Esuriit podnoszą reputację zarówno Arianny jak i Kramera. 
* X: Kirasjerzy są rozczarowani decyzją Arianny. Toteż admirał Termia.

Po przygotowaniu wszystkiego i wezwaniu Nocnej Krypty (przy użyciu poświęcenia grupy skazańców, podobnie jak robiła to załoga Minerwy ale skuteczniej) przed działo rozpalonego Castigatora, ten wystrzelił wiązkę anihilacyjną. A Leona na pokładzie Castigatora była obserwatorką ;-).

* X: Oliwia, bilansujący umysł Nocnej Krypty został bezpowrotnie zniszczony.
* O: Skazańcy, ci którzy mieli przyzwać Kryptę cierpieniem - wszyscy zginęli. Nie dało się ich uratować.
* V: Kompetentny Castigator WYPALIŁ Nocną Kryptę, do ostatniego atomu.

Sukces...

## Streszczenie

Laura, Emulatorka na Nocnej Krypcie została Skażona przez Kijarę. Laura opanowana wizjami Esuriit chciała zniszczyć Eternię. Nie dało się jej powstrzymać - Mirela, Zespół - byli za słabi. Arianna wezwała Kryptę pod ogniem Castigatora i wypaliła Kryptę i Kijarę do zera, uszkadzając "zdrowie psychiczne Krypty". Zginęło kilkudziesięciu ludzi, ale Arianna dostała podziękowanie od Admiralicji. Damian Orion jest rozczarowany.

## Progresja

* Arianna Verlen: dostaje szacun za ochronę Sektora Astoriańskiego przed Kijarą d'Esuriit i za zniszczenie straszliwej Anomalii.
* Arianna Verlen: Kirasjerzy i admirał Aleksandra Termia są nią bardzo rozczarowani. Nie "źli" a "oczekiwali dużo więcej".
* Antoni Kramer: dostaje szacun za ochronę Sektora Astoriańskiego przed Kijarą d'Esuriit i za zniszczenie straszliwej Anomalii.
* AK Nocna Krypta: Oliwia, umysł kapitan Krypty został zniszczony bezpowrotnie przy wypaleniu przez Castigator. Pozostała tylko Helena.

### Frakcji

* .

## Zasługi

* Arianna Verlen: wezwała Kryptę nie robiąc nikomu nadmiernej krzywdy, obiecała grupie ludzi że ich uratuje - ale widząc koszmar Laury d'Esuriit podjęła decyzję o ewakuacji z Krypty i zniszczenie tego statku używając Castigatora.
* Klaudia Stryk: rozwiązywała astralne konstrukty Krypty oraz badała ghule Esuriit. Ona pierwsza doszła do tego, na czym polegał problem Krypty.
* Eustachy Korkoran: dużo wysadzał - kierunkowe miny by dostać się na Kryptę, fortyfikacja korytarza przed ghulami Esuriit, a potem lepiszcze Ariannie i Klaudii by wyszły ze stupora ;-).
* Leona Astrienko: nie jest w stanie walczyć przeciw istocie klasy Emulatorki, ale chroniła skutecznie Zespół przed ghulami Esuriit, zabijając je precyzyjnie.
* Damian Orion: chciał uratować Laurę. Okazało się, że Arianna wybrała bezpieczeństwo. Jest rozczarowany Arianną, ale nadal na poprawnym poziomie.
* Mirela Orion: nieskończenie oddana Damianowi Emulatorka, wzięła Czarną Aleksandrię by wezwać Kryptę. Nie była w stanie pokonać Laury d'Esuriit. Misja przegrana.
* Laura Orion: KIA. Kijara nie zniszczyła jej rdzenia; Laura chciała zniszczyć tylko Eternię. Nie miała jednak szans - skończyła w ogniu Castigatora.
* Kijara d'Esuriit: KIA. Chciała inkarnować się w Sektorze Astoriańskim, ale wypalenie Castigatorem uniemożliwiły ten plan.
* Antoni Kramer: jak tylko admirał dowiedział się o powadze sytuacji, stanął po stronie Arianny i sprawił, by Castigator mógł zniszczyć Kryptę. Dostał podziękowania z Admiralicji i upewnił się, że Arianna też je dostanie.
* AK Nocna Krypta: zainfekowana Kijarą d'Esuriit, generująca halucynacje i astralne struktury. Miała 50-60 osób na pokładzie, gdy została zestrzelona przez Castigator.
* OO Minerwa: Statek dowódczy Kirasjerów, nazwany tak po porażce Damiana odnośnie Minerwy Metalii. Supernowoczesny pojazd NeoMil, sterowany przez Zodiac (czarodziejkę, nie TAI). Ma 'czarną Aleksandrię' - pain module.
* OO Castigator: Kramer załatwił uprawnienia, Arianna przyzwała Kryptę, anihilator Castigatora wypalił Nocną Kryptę do zera, niszcząc zagrożenie Esuriit.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Libracja Lirańska: gdzie Arianna wezwała Nocną Kryptę by wejść na jej pokład. I gdzie była infekcja nanomaszynami Esuriit.
        1. Sektor Lacarin: odnośnie którego Nocna Krypta zbudowała halucynację w Zespole i który był źródłem części ludzi uratowanych przez Kryptę.

## Czas

* Opóźnienie: 1
* Dni: 6
