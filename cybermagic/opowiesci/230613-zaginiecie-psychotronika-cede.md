## Metadane

* title: "Zaginięcie psychotronika Cede"
* threads: hiperpsychotronika-samszarow
* motives: mroczna-konspiracja
* gm: żółw
* players: kamilinux, kić

## Kontynuacja
### Kampanijna

* [230516 - Karolinka - raciczki zemsty Verlenów](230516-karolinka-raciczki-zemsty-verlenow)

### Chronologiczna

* [230516 - Karolinka - raciczki zemsty Verlenów](230516-karolinka-raciczki-zemsty-verlenow)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ?
* CEL: 
    * 
    * Działania
        * Karolinus: Maja, sekrety mroczne, 

### Co się stało i co wiemy

* Kontekst
    * .
* Wydarzenia
    * 

### Co się stanie (what will happen)

struktura sesji: "230613-typ-sesji-odkrycie-sekretu" (narzędzia-mg)

* Strażnicy: Fabian Samszar
    * "Żołnierze mafii", duchy, amnestyki, 
* Beneficjenci: Celina, Cede...
* Prawdziwy Sekret: 
    * Cede był w Gwiazdoczach i Siewczynie
        * w Gwiazdoczach: szukał informacji o duchach (jako psychotronik i ekspert od TAI)
        * w Siewczynie: robił jakieś badania
    * Cede miał wsparcie Samszarów
    * Cede miał ogromne uprawnienia i nikomu nic nie mówił
    * Cede pracował z dziwnymi roślinami choć się na nich nie znał
    * Cede miał sporo pieniędzy, z tym jest powiązany Nataniel
* Fałszywy Sekret: 
    * dziewczyna Cede, Marysia, ma sfałszowaną pamięć
        * ale nie wszystkie jej koleżanki, zwłaszcza te spoza Powiatu
    * Cede uczył się od duchów jak wzmocnić TAI
    * Cede był szantażowany przez mafię by zniszczyć życie Mai Samszar na potrzeby Verlenów
    * Pieniądze Cede pochodzą z tego, że prostytuował dziewczyny
    * Fałszywi współpracownicy mówią o przemycie sprzętu noktiańskiego dzięki mafii
    * Mafia atakuje
    * Cede miał referat o tym jak to duchy mogą pomóc TAI w uczeniu się
* S1: .
* SN: 
    * .

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Lara Ukraptin
    * ENCAO:  +--+0 |Nie planuje długoterminowo;; Opiekuńcza| VALS: Stimulation Self-direction >> Power| DRIVE: Pantha Rhei
    * sympatyczna studentka psychotroniki w Gwiazdoczach, zna się na rzeczy (23)
* Fabian Samszar 
    * ENCAO: -++-0 |Powściągliwy;; Bezwzględny| VALS: Family, Self-direction >> Hedonism | DRIVE: Eternal Evolution
    * ekspert od 

* .Herbert Samszar
    * ENCAO:  0-0++ | Nie ma blokad;; Uparty jak osioł | VALS: Benevolence, Hedonism >> Self-Direction | DRIVE: Tajemnica Wszechświata
    * styl: GB; peaceful, silent, cycle-focused, nurturing | tradition, community, faith, nature
    * magia: "ten, który śpiewa roślinom"
    * ruch: "ten byt to nie było coś czemu powinno się było dać szansę"

### Scena Zero - impl

.

### Sesja Właściwa - impl

chronologia: po Siewczynie przed Mają i podziemną bazą

Karolinus ma wiadomość - Cede zniknął. Tak po prostu "zniknął". To jest, wiedziałeś że go nie ma, że jest zajęty - ale Celina wysłała wiadomość, że całkowicie zniknął. Że to nie jest kwestia kontraktu. Sęk w tym, że Cede to typ akademicki. Nerd. Brat jest ostatnim co jej zostało. Celina nie mieszka na Waszym terenie. Chwilowo, od roku, jest u Sowińskich.

Co Celinę zmartwiło - inni członkowie rodziny (jeszcze jeden brat i ciotka) nie mają poczucia, że coś jest nie tak. Cede nie ma, no bo nie ma.

* Celina: przepraszam tien Samszar, nie wiem do kogo się zwrócić. On zniknął.
* K: dawny znajomy, pomogę go odnaleźć.
* K: (uspokaja)
* C: (czegoś nie powiedziała)
* K: ostatni kontakt?
* C: Gwiazdoczy

Cede jest psychotronikiem. Nie ma nic wspólnego z duchami. Co więcej, zawsze nieswojo wobec duchów miał. Cede i Karolinus się poznali, gdy Cede dla zakładu skonfigurował Elainkę jako seksbota w servarze - był niezły ubaw. Atm Cede powinien mieć 31 lat (ale nie zachowuje się jak dorosły).

Karolinus naciska na Celinę, by powiedziała wszystko. Ona na to "tien Samszar, nie śmiałabym narzekać... ale Ty, jesteś MAGIEM, jesteś... ze szczytu. Cede ma rodzinę. Ma przyjaciół - podobno. Nikt się nie zainteresował. Nikt. A Ciebie informuję, w rozpaczy - i Ty się zainteresowałeś. Bardzo Ci dziękuję."

Między Cede i Celiną (23) jest 8 lat różnicy. Cede traktował siostrę jak "małe coś do chronienia". On ją wyprawił do Sowińskich - kariera, środki itp. On inwestował w Celinę. A ona wiernie brata wspierała.

Akta Cede - gdzie ostatnio pracował.

Strzała: Ex (+przewaga: TAI i uprawnienia Karolinusa) +2:

* X: Druga Strona dowiaduje się że KTOŚ interesuje się Cede.
* Vz: 
    * Zgodnie z dokumentami, Cede pracuje dla _Cyrdis_, niewielkiej firmy (startup) mającej wspomagać TAI nowoczesnymi metodami. 
    * Ale zgodnie z dokumentami _Cyrdis_ ma lokalizację w Siewczynie i nie ma dużo inwestorów. A patrząc na dane Cede, on musiał pracować z podniesionymi uprawnieniami i większym budżetem. Coś robił. I to w Gwiazdoczach, gdzie _Cyrdis_ nie ma lokalizacji
    * Cyrdis ma inwestorów wśród Sowińskich - nie ma opcji że Samszarowie dadzą Sowińskim uprawnienia do rekonfiguracji TAI.
    * Cede, zgodnie z papierami, dalej pracuje w Cyrdis. Nawet ciągle płaci podatki. Zgodnie z papierami "żyje i ma się dobrze".

Cede ma mieszkanie w Gwiazdoczach. Co jest ciekawe, bo pracuje w Siewczynie. 

Karolinus wrzuca dane do bazy danych, sprawa "klanowa". Gdzie jedzie i dlaczego. Zniknięcie OBYWATELA wymaga zainteresowania tiena rodu! "Czarna skrzynka" ("idę na pół godziny na miasto", jak mnie zabiją to wiadomo gdzie).

Gwiazdoczy.

Szukamy w mieszkaniu Cede. Mieszkanie jest zamknięte. To jest takie mieszkanie jak po Cede - niewielkie, wygodne na oko, tanie (bo kasa dla siostry i rodziny i na dziewczyny). Cede zawsze lubił zaszaleć, zwłaszcza wśród dziewczyn. A mieszkanie - nie musi być drogie, i tak tylko tam śpi. Jest wynajęte.

* Miłosz 
    * ENCAO:  -+0+- |Obserwator;;Maruda | VALS: Tradition, Security| DRIVE: Lawful Tyrant
    * właściciel. 

.

* M: "prawdziwy tien!"
* M: "poziomki w tym roku są kiepskie, można coś z tym zrobić?"
* M: "Cede, tien Samszar, to był PLAYBOY. Cede... on kiedyś źle skończy przez te dziewczyny. Same dziewczyny. A niektóre... (pokręcił głową i mlasnął). Daje szczegóły itp."

Karolinus widzi jedno. Te dziewczyny są "ostre", ostrzejsze niż zwykły typ. Cede musiał bardzo... zaostrzyć swój smak. Miłosz się trochę bał tych dziewczyn. Miłosz NIE CHCE NIC MÓWIĆ, ale... tak, odurzające... nie może powiedzieć że nie... lub tak... i nie wchodził mu do mieszkania (kłamstwo: wchodził), ale.... nie zdziwiłby się gdyby tam były ostrzejsze środki...

Miłosz otworzył drzwi do mieszkania Cede. 

Mieszkanie jest opuszczone. Tzn. Jego rzeczy tam są, ale co najmniej 2-3 miesiące go tam nie było. Miłosz widząc lodówkę zapłakał. Wszystko jest opłacane, "nie ma problemów". Miłosz dostaje premię za to by nie wchodził.

Miłosz PODEJRZEWA że Cede jest człowiekiem z mafii - i dlatego ta premia.

Jest tam komputer i notatki. Strzała - analizuje notatki i dane. Karolinus zabiera "fizyczny dostęp do kompa Strzały".

(+przewaga: zaawansowana psychotronika. -podobna klasa przeciwnika +przewaga: Karolinus i jego znajomość Cede +przewaga: czas i agregacja wszystkich innych danych i fizyczny dostęp)

Tr +3:

* X: second tripwire: Druga Strona wie, że tu też są działania dookoła Cede, acz nie wie jakie
    * Strzała wie o tripwire.
* (+Vz, Strzała ma DOMINUJĄCĄ siłę a chce rozebrać demona na kawałki i się dowiedzieć)
    * V: (podbicie) Strzała ma PEWNOŚĆ, że jakiekolwiek dane są na tym kompie, są one fałszywe, sfałszowane.
    * V: (manifest) Strzała rozebrała biednego demona.
        * Demon jest Samszar-grade. To jest typowy demon rodowy. Wyżej niż policja.
        * Demon został zbudowany przez coś w stylu Strzały albo Eszary. Zbudowany przez TAI.
        * Dane na tym komputerze są tylko takie, jakie są "pozwolone".
            * To samo dotyczy rzeczy w mieszkaniu najpewniej
    * Vz: Strzała poszła za linią Demona, by zlokalizować i dowiedzieć się więcej
        * Strzała dostała _typ_ kontaktu - odbiorcą jest _jednostka do walki psychotronicznej_ w Siewczynie.
            * Cede i uprawnienia do tej klasy jednostek. NOPE! Nawet by nie umiał.
                * To jest jednostka Samszarów, ograniczona do swoich rozkazów.

Aleksander Samszar.

* A: Karolinusie?
* K: Aleksandrze, proszę o pomoc - szukam dobrego znajomego, inżyniera psychotroniki, który zaginął i trop prowadzi do Siewczyna, do jednostki psychotronicznej.
* A: Do tej jednostki psychotronicznej której używam? /zdziwiony
* K: No... tak. Mamy zagwozdkę /opowiada historię
* A: (chwilę myśli). To ciekawe, ta jednostka była wolna gdy ją wziąłem. To jest jednostka publiczna... sprawdzę Ci listę poleceń, daj mi godzinkę.
* K: Będę bardzo wdzięczny.

Dziewczyna Cede!!! Aleksander pracuje, dziewczynę odwiedzić można.

Nie było trudno znaleźć Larę. Na kampusie uniwersyteckim. Umówili się na kawę. Bliżej wieku Karolinusa niż Cede, co pasuje do Cede ;-). Celina była nią rozgoryczona - nie interesowała się swoim znikniętym chłopakiem.

* Lara: "Cede? Ach, on. Zerwał ze mną. Publicznie!" /uraza "Powiedział, że teraz jak ma pieniądze to może sobie pozwolić na KAŻDĄ, rozumiesz? Przepraszam, rozumie tien" /słodka minka (dała listę osób które pamięta że tam były)

Karolinusowi coś nie pasuje.

(-ona mówi prawdę +Karolinus jest magiem +Karolinus ma dane od Celiny)

Tr Z +2:

* (X): odraczam na moment.
* (X): Karolinus się orientuje, że Lara ZA DOBRZE pamięta.
    * Cede się tak nie zachowuje. Po prostu. Nie publicznie. Nie zraniłby dziewczyny publicznie.
    * Lara naprawdę, NAPRAWDĘ jest zła na Cede. Ktoś, kto jej _to_ zrobił zniszczył w jej oczach reputację Cede.
    * Użyto na nią magii.
    * -> ona ORAZ JEJ PRZYJACIÓŁKI będą pamiętać to samo...
    * Ktoś wstecz zatruł jej wspomnienia wstecz
* Karolinus decyduje się użyć magii by poznać jej sekret. On chce poznać PRAWDĘ. Co się stało, co pamięta.
    * (+M+3Ob+3Or)
* V: (podbicie) Karolinus WIE, że na nią rzucono zaklęcie i wie, że nie tylko na nią. Co więcej, to był ktoś z rodu Samszar. Czyli stoi za tym jakiś mag znający się na magii mentalnej. I ten mag nie miał nic przeciwko masowym uszkodzeniom dużej ilości ludzi. Co gorsza - (3Or) reprezentują uszkodzenia mentalne; pułapkę.
* Ob: Lara kiedyś kochała Cede. TO WRACA! Lara jest śmiertelnie, na zabój zakochana w Cede.
* Vm: 
    * Karolinus poznał maga, który zrobił do Larze. 
        * To Fabian Samszar. 44 lata. Ekspert od magii mentalnej, który jest bardzo poważany i mnóstwo zrobił dla rodu. Fabian uczył Karolinusa niektórych elementów magii mentalnej - dlatego od razu dało się poznać Wzór
        * Fabian jest bardzo ceniony i poważany w rodzie.
    * Pamięć Lary się zregenerowała, co dało Karolinusowi RÓŻNICĘ między tym, co Lara myślała a co się stało
        1. Cede nie ma nic wspólnego z narkotykami. Nic. To wszystko było zaszczepione przez Fabiana.
        2. Cede nie zdradzał dziewczyny. Oki - jak nie był z Larą to był z innymi. Ale jak był z Larą to ok.
        3. Cede nie ma interakcji z mafią. A dużo na to wskazywało, np. dane z apartamentu które widziała Strzała.
        4. PRAWDĄ jest to, że Cede miał dużo pieniędzy.
        5. Przed zniknięciem Cede się bardzo martwił projektem w pracy. Ale nic nie powiedział.
        6. Cede nigdy nie powiedział Larze gdzie pracuje i co robi. Powiedział "to coś bardzo ważnego".

Później, po wszystkim, Aleksander powiedział że _jednostka psychotroniczna_ nie miała żadnych rozkazów z Gwiazdoczów. To znaczy, że ktoś dał jej tajną misję. Ale Fabian nie ma na to uprawnień...

To znaczy, że jest **więcej niż jeden potężny mag rodu Samszar**. I to jest kłopot.

Co na razie wiemy:

* Fabian Samszar miesza w tym garze
* Ktoś ma uprawnienia do dania specjalnej misji _jednostce do walki psychotronicznej_
* Wiemy, że Cede jest psychotronikiem
* Wiemy, że Cede poruszał się między Gwiazdoczami i Siewczynem
* Wiemy, że tym zajmują się jacyś tajemniczy magowie rodu Samszar i są dość bezwzględni

Strzała widzi sytuację. Jej psychotronika jest w overdrive.

Ex Z +4:

* X: Strzała WIE, że się spotkała z czymś z tego wcześniej. Przebłyski. Halucynacje na psychotronice. Ale nie wiadomo o co chodzi.
* Vz: Strzała, gdy była naprawiana po powrocie z Sanktuarium Kazitan miała wyłączoną psychotronikę. Byłaś naprawiana. Widziałaś Cede. On tam był. Coś robił... nie pamiętasz... ale on chyba Strzałę badał. Nie sam. Sporo osób. I Strzała nie była tam gdzie powinna być - przenieśli ją. I ta _jednostka psychotroniczna_ też tam była - niekoniecznie ciałem, ale coś tam było. Uczestniczyła w tym. Aha, i Strzała ma _geas_. Blokadę. Nie pamięta. Nie może o tym pamiętać. Ta blokada wygląda na STARĄ.

Strzała informuje Karolinusa o specyficznym stanie swojej psychotroniki i o tym że widziała Cede.

A Karolinus strzela do Aleksandra wiadomością.

* A: Karolinusie? /lekko zaniepokojony
* K: Mamy problem i nie umiem sobie z tym poradzić, jestem zbyt młodym magiem bez pozycji w rodzie. Potrzebowałbym Twojej pomocy /opis problemu
* A: Zniknął Twój kolega, ktoś z naszego rodu robi takie rzeczy naszym obywatelom, Strzała ma uszkodzoną psychotronikę. Coś pominąłem?
* K: Rodzinę, nie tylko koledzy.
* A: "Masz talent do mieszania się w trudne sprawy." /zaniepokojony "Czego ode mnie oczekujesz, w czym chcesz mojej pomocy"?
* K: Nie wiem, przerasta mnie to. Chcę tylko znaleźć Cede. A ktoś go ukrył przeznaczając na to duże środki.
* A: Czyli... chcesz, żebym Ci pomógł odzyskać kolegę. Dał jakiś trop, popytał.

(+Aleksander jest dobrą osobą i mu się to nie podoba, -Aleksander się BOI o Karolinusa, -Aleksander nie chce otwierać frontu z tamtą frakcją)

Ex Z +4 (połowa krzyżyków zielona - reprezentują "ugodę"):

* Xg: (podbicie) Aleksander spróbuje MEDIOWAĆ by Karolinus dostał co chce a druga strona była nienarażona
* Xg: (manifest) Aleksander zażąda od drugiej strony bezpieczeństwa dla Karolinusa
* Vr: Aleksander "zapomniał" powiedzieć, że Strzała coś wie.

Aleksander negocjował z Drugą Stroną. Wynegocjował spotkanie Karolinusa oraz Fabiana, gdzie Karolinus skończy z amnestykami. Ale "zapomniał" powiedzieć, że Strzała ma rejestr i dane. "Zapomniał" powiedzieć, że ukrył w Strzale odpowiednie środki anty-amnestyczne.

Dzięki temu Karolinus będzie "bezpieczny" od Drugiej Strony, będzie odblokowany, oni się niczego nie spodziewają (a nawet uważają, że Aleksander jest ich sojusznikiem) a Karolinus może porozmawiać o Cede z Fabianem.

Karolinus i Fabian. Fabian był nauczycielem Karolinusa, specjalizował go w magii mentalnej. Owszem, był surowy i groźny, ale nigdy nie był "zły". Nie robił krzywdy.

* F: Poszedłeś w stronę wyjątkowo niebezpieczną, drogi Karolinusie.
* K: Chciałem się dowiedzieć co z moim kolegą. To źle szukać znajomych?
* F: (chwilę myśli) Nie. Miałeś pecha (szczerze). Zawsze byłeś lojalny. Po prostu miałeś pecha.
* K: Pomożesz mi go odzyskać?
* F: Tak, jak skończy. Robi w tej chwili coś bardzo istotnego, a zrobiliśmy błąd - nikt go miał nie szukać. Trudno o dobrych psychotroników których nikt nie chce. Nie spodziewałem się, że Ty będziesz go szukał.
* K: Jak długo w ukryciu?
* F: Przy projektach naukowych nie da się jednoznacznie odpowiedzieć. Ale mogę go zwolnić za trzy miesiące. Najwcześniej.
* K: Nie musisz go zwalniać jeśli tego chce, ale możemy z nim się skontaktować na 15-20 min rozmowy?
* F: My?
* K: Ja.
* F: I jego urocza siostra?
* K: Ja i siostra zdalnie.
* F: (uśmiech) To niesamowite, że ta młoda dama, tak... pozornie nieistotna, a tak weszła w szkodę.
* K: Wystarczyło, by nie zniknął.
* F: Wystarczyłoby też, gdyby jej się zdarzył nieszczęśliwy wypadek, ale nie jesteśmy potworami. Jest powód, czemu musiał zniknąć.
* K: Spotkanie zdalne jest realne?
* F: Bez problemu. Jestem w stanie zaaranżować serię zdalnych spotkań, więcej nawet niż jedno. Muszę tylko upewnić się, że stworzymy odpowiednią historię czemu jest daleko i niedostępny. Ale to się da załatwić.
* K: Zgadzam się. Będę czekał na kontakt.
* F: Mam dla Ciebie amnestyki. Dopóki ich nie zażyjesz, nie rozejdziemy się stąd.
* K: Jak daleko wstecz będą działać?
* F: Od momentu, w którym wykonałeś pierwszą akcję poza kontaktem z Celiną. Bo nie możemy obu stron ukryć. Celina będzie pamiętała kontakt. Więc Ty nie możesz go zapomnieć.
* K: Jak będę miał pamięć, będę szukał Cede.
* F: Nieskutecznie.
* K: Bo wiecie co zablokować.
* F: Dokładnie. Zajmę się tym na tyle, byś nieskutecznie go szukał. Nie martw się, nie będzie potrzeby nowych amnestyków.
* K: Jaka jest gwarancja zdalnego połączenia?
* F: Celina jest gwarancją. Ale nie zapamiętasz gwarancji. Ale Cede się do Ciebie odezwie i będziesz pamiętał, że go znalazłeś. Ale nie od razu - bo muszę zapewnić wszystko...
* F: Przykro mi, że jestem zmuszony do tak... drastycznych środków. Wiem, że Tobie bardziej. Uwierz, gdybyś wiedział nad czym on pracuje, zgodziłbyś się ze mną.

Karolinus niechętnie wziął amnestyki. Faktycznie, następnego dnia nic nie pamiętał.

## Streszczenie

Przyjaciel Karolinusa, Cede, zniknął. Siostra - Celina - będąca na terenie Sowińskich poprosiła go o pomoc (bo nikt inny nie pomaga). Karolinus i Strzała odkryli, że Cede robił coś w Siewczynie i Gwiazdoczach i ktoś udaje, że Cede miał problemy z mafią. Dalsze poszukiwania wykazały, że w sprawę zamieszana jest jednostka do walki psychotronicznej (???) oraz wysoko postawieni Samszarowie. Karolinus współpracując z Aleksandrem dotarł do swojego przeszłego nauczyciela, Fabiana, i zażył amnestyki. Ale Aleksander zostawił w Strzale środki naprawcze - które zadziałają po pewnym czasie.

## Progresja

* Karolinus Samszar: skończył z amnestykami, zażył od Fabiana. Zapomniał o wszystkim odkąd Celina się z nim skontaktowała.
* AJA Szybka Strzała: okazuje się, że jej pamięć jest uszkodzona; robili z nią jakieś eksperymenty. Ma geas i nie jest w stanie sobie przypomnieć.
* Lara Ukraptin: odbudowana pamięć przez Karolinusa, głęboko zakochana w Cede. Dzięki Karolinusowi - bardziej niż kiedykolwiek ;p.

## Zasługi

* Karolinus Samszar: gdy jego przyjaciel, Cede, zniknął to się zainteresował - przegrzebał prawdę od fałszu i gdy zobaczył że sprawa jest dla niego za ostra, poprosił Aleksandra o pomoc. Skończył rozmawiając z Fabianem i wziął amnestyki.
* AJA Szybka Strzała: analizuje dane komputera w pokoju Cede, zmasakrowała biednego demona i znalazła że w tle jest jednostka do walki psychotronicznej. Została niewykryta.
* Cede Burian: przyjaciel Karolinusa; 31 lat, kompetentny psychotronik z noktiańskim szkoleniem. Zaplątał się w coś z Samszarami i zniknął. Bardzo rodzinny i sympatyczny. Przeprowadzał niedawno analizę Strzały.
* Celina Burian: 23 lata, na terenie Sowińskich, siostra Cede. Blisko brata; do tego stopnia się o niego martwi że nawet poprosiła o pomoc maga rodu Samszar. Ogólnie unika magów.
* Fabian Samszar: potężny mentalista powiązany z dziwnymi eksperymentami i podziemnymi bazami Samszarów, kiedyś nauczyciel Karolinusa. Powiązany ze zniknięciem Cede.
* Lara Ukraptin: dziewczyna Cede, studentka. Jej pamięć została przekształcona przez Fabiana Samszara, ale Karolinus ją odbudował Soczewką.
* Aleksander Samszar: cichy sojusznik Karolinusa i Strzały przeciwko Mrocznym Samszarom; "zdradził" Karolinusa i nadał Fabianowi info że Karolinus się interesuje Cede. A potem dostarczył Strzale anty-amnestyki. 

## Frakcje

* Hiperpsychotronicy: nieprawdopodobnie bezwzględni (zniszczyli reputację Cede, amnestykowali jego rodzinę i dziewczynę...), mają dostęp do jednostki do walki psychotronicznej w Siewczynie. Na pewno mają bazy zarówno w Gwiazdoczach jak i w Siewczynie. Należy do nich Fabian, który amnestykował Karolinusa.
* Hiperpsychotronicy: ich cichym wrogiem jest Aleksander Samszar, który wspiera Karolinusa przeciw nim (np. anty-amnestyki przez Strzałę). Udało im się też uszkodzić psychotronikę Strzały.

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Gwiazdoczy
                            1. Siewczyn

## Czas

* Opóźnienie: 2
* Dni: 3

## OTHER
### Fakt Lokalizacji
#### Miasto Gwiazdoczy

Dane:

* Nazwa: Gwiazdoczy
* Lokalizacja: Świat|Primus|Sektor Astoriański|Astoria|Sojusz Letejski|Aurum|Powiat Samszar|Gwiazdoczy

Fakt:

Wizualnie, miasto cechuje się harmonijnym połączeniem starych, kamiennych budynków inspirowanych architekturą Oxfordu i Lund (czyli gotyckich), wspomaganych przez sentisieć i nanitki by stworzyć nową, lepszą całość. Wiele budynków posiada nietypowe, dynamiczne formy i struktury, które zdają się wyłaniać się z dawnych murów. Niesamowicie zielone miejsce, co jest wspierane zarówno przez duchy jak i przez sentisieć.

Kalejdoskop Astralny pozwala zobaczyć co jest, co było lub co być może; Gwiazdoczy znajduje się w przestrzeni Szczeliny Światów.

Gwiazdoczy jest niesamowicie dobrze skomunikowane siecią tramwajów i pociągów. Utopijne sci-fi, ale samo miasto nie jest ogromne - ok 120k ludzi.

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Gwiazdoczy
                                1. Centrum (Centrum)
                                    1. Sklepy i kawiarnie
                                    1. Puby i restauracje
                                1. Dzielnica Akademicka (NW, W)
                                    1. Uniwersytet (NW)
                                    1. Kampus uczelniany
                                    1. Wielka Biblioteka (W)
                                    1. Czytelnie naukowe
                                    1. Muzeum Historii
                                1. Szczelina Światów (E)
                                    1. Kalejdoskop Astralny 
                                    1. Archiwa duchów
                                    1. Plac rytuałów
                                    1. Instytut Sztuki Wspomaganej
                                    1. Wielki Cenotaf Skupiający
                                    1. Magitrownie puryfikacyjne
                                1. Dzielnica Technologii (S)
                                    1. Park Technologiczny
                                    1. Warsztaty i inkubatory
                                    1. Centralna stacja pociągów
                                    1. Centrum Eszary
                                1. Dzielnica studencka (N)
                                    1. Strefa Sportowa

