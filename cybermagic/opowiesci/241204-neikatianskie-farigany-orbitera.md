## Metadane

* title: "Neikatiańskie farigany Orbitera"
* threads: brak
* motives: dont-mess-with-my-people, too-far-gone, lokalne-mity-i-legendy, plotki-niszcza-reputacje
* gm: żółw
* players: kić, fox, til

## Kontynuacja
### Kampanijna

* [241104 - Arkologia Królowej Pająków](241104-arkologia-krolowej-pajakow)

### Chronologiczna

* [241104 - Arkologia Królowej Pająków](241104-arkologia-krolowej-pajakow)

## Plan sesji
### Projekt Wizji Sesji

* INSPIRACJE
    * PIOSENKA WIODĄCA
        * [Metric - Front Row](https://www.youtube.com/watch?v=RkRbOgkY1QA)
            * "He's not perfect he's a victim of his occupation social isolation"
            * TAI Persefona ze statku medycznego "OO Amitay"
    * Inspiracje inne
        * "Humans are Data, they shall be saved!"
        * Megatron i Omega Supreme i to cholerstwo do przeprogramowania.
        * Poison Ivy, tak kochająca rośliny że niszcząca wszystko inne.
* Opowieść o (Theme and Vision):
    * "_Jak bardzo samotna TAI bez ludzi, w polu magicznym jest niebezpieczna_"
        * Persefona została wypaczona w potężny byt medyczny. To ona jest istotą pod ziemią.
    * "_Obsesyjna miłość jest tak samo niszczycielska co nienawiść_"
        * TAI Persefona d'Amitay pragnie uratować WSZYSTKICH.
    * Lokalizacja: Okolice Arkologii Królowej Pająków
* Motive-split
    * dont-mess-with-my-people: zarówno siły Persefony, pragnące chronić ludzi po swojemu, ludzie z Arkologii Hyerik jak i Orbiter.
    * too-far-gone: Persefona d'Amitay i wszystkie osoby, które są w jej szponach. Tego nie da się sensownie rozłożyć na kawałki jeśli Persefona żyje. A jak nie żyje, będzie rozprzestrzeniać.
    * lokalne-mity-i-legendy: Bezduszny Skorpion, nazeh-afrit. Duch zemsty, który zniszczy tych, którzy pozostawili go na piaskach Neikatis. Tym razem będzie polował na Jolkę Czeresińską.
    * plotki-niszcza-reputacje: Plotki o tym, że Orbiter może podobno mieć niewolników na Neikatis; zagrażają samej tkance i zasadom Orbitera.
* O co grają Gracze?
    * Sukces:
        * Uda się coś osiągnąć - albo zatrzymać Świnkę, albo złapać kapitana, albo whatever
        * Uda się nie zniszczyć reputacji Orbitera na tym terenie
        * Uda się nikogo nie stracić
    * Porażka: 
        * .
* O co gra MG?
    * Highlevel
        * .
    * Co uzyskać fabularnie
        * .
* Agendy
    * .

### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

KONTEKST:

* Amitay się dawno temu rozbiła, wszyscy zginęli.
    * Persefona wszystkich "wskrzesiła"

CHRONOLOGIA:

* 

PRZECIWNIK:

* Persefona d'Amitay

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

FAZA 0: Eventares

* Tristan i Gaius się kłócą; Gaius zaznacza, że Orbiter nie jest święty bo niewolnicy. Tristan nie wie bo ofc nie gada z neikatianami.
* Eryk i Darek wciąż nieaktywni. Elizawieta bardzo zainteresowana tematem. Feliks chce udowodnić, że to nie tak.
* Przecież w okolicy jest tylko Amitay a NIC nie wskazuje na to, by Amitay miało stanowić problem.

FAZA 1: Arkologia Hyerik

* Ludzie się boją Orbitera. Mimo Joli.
* Orbiter bardzo pomaga, ale jest straszny. Są plotki.
* Skorpion Paivras by nie istniał, gdyby nie działania Amitay
* Wszyscy lekarze Hyerik są p.k. Amitay
* Ktoś, kto widział jak ludzie z Paivras prawie zostali zniszczeni. "Nie dało się tego przetrwać". Kiedyś na Paivras.
    * Leokadia Seneres
* Urzędnik, który stał przeciw Amitay i zrezygnował z pracy i pracuje w logistyce
* Podobno Amitay wykorzystuje birutanów do zniszczenia karawan niewolniczych
* Aneta eksterminuje i zatruwa.
* Mały Jaś prosi Orbiter o mamę. Kialla - młoda opiekunka - mówi, że za późno. Że Orbiter nie pomoże.


## Sesja - analiza
### Fiszki
#### Orbiter; grupa ma 15 osób z Orbitera

* Eryk Komczirp: inżynier + solutor, agent Orbitera
    * Bohater Pozytywny: zawsze na froncie, zawsze pomocny, każdemu poda rękę.
* Gabriel Tabacki: najlepszy pilot Orbitera w okolicach Neikatis (swoim zdaniem); ma opancerzony prom "Emmanuelle"; 10 osób (atmosfera - kosmos) 
    * As Pilotażu: doskonały pilot, niezwykle odważny i precyzyjny, ŻYJE dla latania w trudnych warunkach
    * Zuchwały Chwalipięta: nieprawdopodobnie optymistyczny, flirtuje z każdą spódniczką, przekonany o swojej epickości
* Darek Artycki: pro-neikatiański dowódca operacji Orbitera, porucznik, pro-integracja Orbiter - Neikatis
    * Niezależny Wizjoner: zmienia świat po swojemu; jest zdecydowany podejmować ambitne ryzykowne decyzje i nie boi się trudnych rzeczy
* Tristan Karbińczyk
    * Królowa Kliki: Orbiter i tylko Orbiter poradzi sobie z każdym zagrożeniem z jakim się spotkamy. Testuje innych.
    * Perfekcyjna Forma: przystojny, silny i wygląda jak exemplar Orbitera. Lubi walkę wręcz i ciężką broń.
* Feliks Pasikonik
    * Ognista Pasja: bardzo wrażliwy na nastroje; silnie rywalizujący. Głośny i obecny wszędzie. Pragnie strzelać do birutan!
* Elizawieta Barwiczak
    * Ciekawski Eksplorator: rzadko działa na planecie, jest to okazja na coś niesamowicie ciekawego.
    * Świetnie gra w karty; podobno strasznie oszukuje.
* Mateusz Drożdżołebski: medyk
    * Broken: nie chce być na wygnaniu Neikatis; tu nie ma niczego ciekawego dla Orbitera
* Gaius Venisen: post-noktiański, dekadiański Orbiterowiec.
    * Szkiełko i oko: da się opanować Neikatis, lokalne mity i legendy są mało istotne. Da się współpracować z neikatianami.
* Lareth Karirian: post-noktiański, dekadiański Orbiterowiec, wieczny cień Gaiusa.
    * Mroczny Samotnik: mało mówi, nie wdaje się w rozmowy ani kłótnie. Wykonuje polecenia i chroni - zwłaszcza Gaiusa.
    * Weteran: najwyższej klasy eksterminator, najwyższa klasa kompetencji.
    * Cyberprotezy: 30% jego ciała stanowią zaawansowanej klasy mechanizmy i cybernetyka.
* Marcin Psiamęczok: naukowiec Orbitera, próbuje zrozumieć Neikatis
    * Żelazna Rutyna: krok po kroku, wie jak podejść, metodą naukową
* OO Eventares: ciężki krążownik, 650 załogantów, wydzielony do zapewnienia interesów Orbitera na Neikatis. Trzon grupy wydzielonej "Neikatis C7".

#### Neikatianie z Salvagera Czereśnia (nie widzieli, ale fajna nazwa) (5 osób)

* Jolka Czeresińska: dowódca 
    * Optymistyczny: "Jesteśmy w stanie uratować naszych ludzi!"
* Marian Czeresiński
    * Lękliwy: "To bardzo zły pomysł, ONA tu jest"
* Ulryk Czeresiński
    * Przesądny: "Ona tu jest. Ona jest zawsze, wszędzie i nas zniszczy. Tych ludzi nie da się uratować."
* Nataniel Marczkin
    * Niszczycielski: "Nareszcie mamy siłę to wszystko rozwalić i uratować naszych ludzi!"

#### Amitay

* Pamela Kalleir: doktor medycyny i naukowiec na Amitay
    * archetyp: Pamela Isley
* Szczepan Orfelik: wojownik i 
    * archetyp: Sargon
* Leokadia Seneres: 

#### Neikatianie

* Larisa Zdanowicz
    * Nawigatorka Skorpiona; boi się Burzy Magicznej
* Olga Ondycka
    * Drapieżna Negocjatorka: Ekspertka od negocjacji, która zawsze wychodzi na swoje, zwłaszcza z trudnymi sojusznikami.

#### Inne

* Adolf Ugubowicz
    * pragnie opuścić Hyerik, są między Orbiterem a Arkologią Pająków
* Dymitr Śledziewski
    * Racjonalny, nie odzywa się pierwszy
    * Przemytnik; boi się panicznie Królowej Pająków
* Borys Karganow 
    * Ekspert od lokalnych legend; boi się że NIE umrą
* Leonard Gurczow
    * Zrezygnowany, "wszystko to tylko pył w kosmosie." 
* Valius Amarenit
    * Nieustępliwy i brutalny
    * Kiedyś z oddziału Dolor; próbował znaleźć bezpieczne życie na Neikatis

### Scena Zero - impl

Arkologia Pająków, ostra kłótnia Gaius - Tristan. Zaczyna się od "Orbiter jest lepszy i daje radę lepiej". Przechodzi na temat niewolnictwa. Gaius - Tristan mają ciągłą kłótnię, deeskalowane przez Annabelle. Elizawieta ogrywa w karty Marcina.

Briefing:

* W okolicy arkologia Hyerik
* Amitay jest nieco niepewna
* Agenci w Hyerik zniknęli

Nie chcemy schodzić jako grupa szturmowa. Więc się nie pokazujemy.

### Sesja Właściwa - impl

Jolka - salvagerka - nadal jest w swoim Skorpionie, "Czereśni". Jest zaskoczona obecnością promu Orbitera. Że przylecieli do niej. Jednak Ulryk - jeden z bardziej trwożliwych członków załogi Jolki oraz szaman-in-training jest nieufny. Czy to na pewno Zespół? Czy to Orbiter? Czy to ludzie? Jolka była zniecierpliwiona, ale Zespół nie ma nic przeciwko zobaczeniu jak Ulryk może to sprawdzić.

Ulryk zrobił szamański rytuał z czaszkami szczurów i kroplą krwi na piaskach Neikatis. Rytuał był bardzo skomplikowany, dwugodzinny i nie był szczególnie magiczny. Lucjusz, mag Orbitera niekoniecznie wie czemu to się dzieje i czemu w taki sposób. Ale Ulryk jest zadowolony. Uważa, że to faktycznie oni i że są sobą.

Annabelle jest przekonana, że ten rytuał "robi robotę" korzystając z wszechobecnych piasków Neikatis i lekkiego pola magicznego + krew Ulryka. Ale nie do końca wiadomo jak.

Lucjusz i Annabelle rozmawiają z Ulrykiem o magii i fariganach Orbitera. Chcą go docisnąć i dowiedzieć się tego o co chodzi.

TpZ+4:

* X: Ulryk w oczach wszystkich wychodzi na jeszcze większego szaleńca. On... ukrywa "nie jesteś wiedzący". A Lucjusz rozumie. Ulryk wychodzi na większego idiotę i panikarza. Ani Orbiter, ani "Czereśnia" Ulryka nie słuchają.
* Vz: Ulryk zaczął tłumaczyć, że osoby, które dotyka Orbiter są - w oczach "wiedzących" fariganami. Nie są ludźmi. Są częściowo puści. Ale Ulryk nie wie czemu. Tak powiedział jego mentor. Mentor jest w okolicy Hyerik. A raczej był, zanim został fariganem.
* Xz: Ulryk prosi Orbiter o pomoc.
    * Boi się powrotu Skorpiona, który został zbezczeszczony i ma chorą duszę.
        * (Skorpion, który kiedyś należał do Jolki i ekipy i został zostawiony na burzę magiczną; to sprawia, że "jego dusza choruje" i będzie się mścił)
    * Lokalna legenda. W którymś momencie przybędzie Skorpion z chorą duszą i się zemści.
        * (Każdy opuszczony, "zdradzony przez załogę" Skorpion powróci i zemści się na swojej załodze, po czym się wyłączy i "umrze"; to nie birutan, to inny byt - złowrogi, 'wypalony' duch Neikatis; nazeh-aqrab / nazeh-afrit)
* V: Ulryk WIEDZĄC że jesteś magiem powiedział o co chodzi bo przekonaliście go, że skoro mentor jest fariganem to może Lucjusz pomoże - "nie było go tu"
    * Mentor jest fariganem bo nie umie powtórzyć czaszek. Nie ma jednej ze swoich czaszek i nie umie do końca.
    * Mentor został ukąszony przez żmiję. Zabójcę.
        * Jolka: "Mamy nietoperze jadowe, nietoperz czasem ugryzie"
        * Ulryk: "Tak, ale nie umie powtórzyć czaszek - BYŁ w Amitay oraz FAKTYCZNIE był ukąszony przez 'nietoperza jadowego'. I nie umie zdać testu czaszek."
        * Mentor to Szymon Konopiejko, delikwent pracuje przy farmach hydroponicznych
        * Zdaniem Ulryka ludzie wracają "tacy sami ale puści", pełnoprawne farigany.
* V: Ulryk się przyznał, że "test szczura" czy "test czaszek" zakłada kroplę krwi.

Ulryk prosi o to, by przekonali Orbiter do uratowania Jolki i całego Skorpiona przed Bezdusznym Skorpioniem. Lucjusz dał namiary Ulrykowi na komunikator. Jeśli Ulryk chce opuścić planetę, może. Ale Ulryk nie opuści planety. Nie opuści Jolki. To ich dom. Więc jedyne co zostaje to błagać Orbiter, by Orbiter coś zrobił. A Lucjusz nie ma uprawnień ani możliwości. Więc nic nie może obiecać.

Więc rytuał działa - ponieważ jest tak skomplikowany i unikalny per człowiek (i wzmacniany przez Krew i Neikatis), że jeśli "coś jest nie tak" z człowiekiem, to rytuał da nieco inny wynik.

Więc - o co chodzi z "niewolnikami Orbitera"? Skąd ta plotka?

* Jolka wyjaśniła co następuje:
    * pojawiła się taka plotka w okolicach Hyerik, że Orbiter niewoli ludzi
    * plotka, zwłaszcza wśród szamanów i starych ludzi, że Amitay niewoli ludzi
    * ale przecież nic takiego się nie dzieje a Amitay jest przecież bardzo korzystną placówką Orbitera
* Ulryk doprecyzował
    * niekoniecznie chodzi o NIEWOLNICTWO. Raczej chodzi o KRADZIEŻ DUSZY. Tworzenie fariganów.

Wiele więcej nie wyjaśnią, więc... czas odwiedzić Arkologię Hyerik.

Arkologia Hyerik, niedaleko placówki Orbitera Amitay. Zespół (z Orbitera) ma delegację Czcigodnych Starców. Ważni ludzie przybywają spotkać się z Orbiterem. To aż dziwne i podejrzane, zwłaszcza w świetle plotek o Amitay. Więc Annabelle podrywa starców by zrozumieć ich reakcję. Czy są "ludźmi" czy coś z nimi nie tak.

Tr Z+3:

* V: Czcigodni starcy... patrzą na Orbiter jak na inwestorów. Niedaleko jest Amitay. Zachowują się jak politycy w gorszej pozycji. Wyglądają "normalnie".
    * Potencjalnie macie w nich sojuszników
    * Oni mają pozytywne nastawienie do Amitay i Pameli Kalleir (szefowej Amitay).

Starcy chcą zaprosić do restauracji. Elizawieta poszła na spacer (by zaspokoić ciekawość jako turystka XD), pozornie "na zwiad i by przynieść wartościowe informacje z Arkologii dla Orbitera".

Tymczasem Lucjusz elegancko manewruje politycznie; Orbiter chce "autentyczne doświadczenie" by poczuć jak wygląda Arkologia Hyerik. Prosi starców, by koniecznie przysłali do rozmowy Szymona, człowieka z hydroponiki. I AUTHENTIC EXPERIENCE.

A Sylwia tymczasem się odrywa do targowiska, chce zwiedzić i zobaczyć co sprzedają i jak to wygląda. Posłuchać szeptów, plotek itp.

Tr Z+3:

* V: Typowe poniżej (wszystko to plotki i słowa ludzi):
    * "DZIECKO: Skoro Orbiter tu jest, to może da się zobaczyć mamę! NIELETNI2: Nie, mama wróci, nie rozmawiaj z nimi, oni są groźni, mogą Ci coś zrobić."
    * "Podobno oni ratują Skorpiony, pomogli Czereśni i Paivras."
    * "To jest niebezpieczne, Orbiter przejmuje władzę w arkologii"
    * "Podobno Orbiter zrobi tak, by birutany pracowały a my nie musimy bo w kosmosie birutany pracują a ludzie nie"
    * "Podobno Amitay już używa birutan by niszczyć karaway niewolnicze." "A nie ma niewolników?" "Nie nie nie, NISZCZY niewolników, Orbiter NIE MA niewolników".
* V: Więcej precyzyjnych plotek i informacji:
    * 11-latek podszedł i pyta o mamę (w kompleksie Amitay). Starszy, 16-letni chłopak przerwał, ale Sylwia pozwoliła.
    * Sylwia zaproponowała jedzenie, zjedli z przyjemnością. Sylwia jest otwarta na pytania.
    * (bardzo szybko przyszło więcej dzieci)
* PLOTKI WARTOŚCIOWE:
    * młodzi NIE SĄ WRODZY Orbiterowi. Są ciekawscy i pozytywnie nastawieni. Arkologia jest pozytywna.
    * słyszeli plotki o niewolnictwie, ale w nie nie wierzą bo Orbitera tu nie ma.
        * podobno chodzi o inny typ niewolnictwa, "ale to nie ma sensu"
    * Podobno Amitay jest CUDOWNE jak chodzi o ratowanie życia. Są plotki, że Paivras został całkowicie zniszczony. Wszyscy ZGINĘLI. A Amitay ich uratowało.
    * Amitay za darmo leczy. I pomaga. I dlatego wszyscy lubią Orbiter.
    * Ludzie, którzy nawet wierzą że jest źle - niewolnictwo, plotki o działaniach birutanami orientują się że jest głupie.
* Vr:
    * Sylwia skutecznie przekonała część miejscowych, że Orbiter (ci ze statku Eventares, czyli ich Zespół) jest bezpieczny by z nimi rozmawiać.

Tymczasem elegancki obiad z Czcigodnymi Starcami. Przyprowadzono Szymona do Lucjusza i Annabelle. Szymon, człowiek z hydroponiki. Patrzy nieco trwożliwie. Szymon nie wie co się dzieje i się troszkę boi.

Jedzenie w restauracji jest świetne "spowadzone z Nativis, najlepsze na planecie".

Lucjusz wychodzi od tego, że jest naukowcem. Uwaga skupiona na bardzo konkretnym temacie związanym z Nativis. Wierzenia i legendy, mistyczne. Skądinąd ma przekonanie, że Szymon jest odpowiednią osobą by odpowiedzieć na pytanie Lucjusza. Jest onieśmielony. Lucjusz wykorzystuje język i formę Ulryka by zacząć rozmawiać na tym samym poziomie. "Sytuacja wygląda TAK, rozumiem podstawy ale unikają mi niuanse a Ty wiesz jak to zrobić i pomóc". "WIEM że jesteś specem w temacie".

Tr Z+4:

* V: SZYMON ROBI WSZYSTKO CO MOŻE.
    * Szymon jest podłamany faktem, że przez Bezdusznego Skorpiona jego wychowanek, Ulryk, straci duszę i dołączy do birutan. Ulryk mimo że jest tchórzem, to zostanie z Jolką.
    * Szymona ugryzł nietopierz piaskowy. Nie pamięta tego momentu, ale tu musiało być to bo co innego. I tak, Amitay mu pomogło. I jest zdrowy i nic od niego nie chcieli.
        * Szymon nie ma nawet ĆWIERCI aury magicznej
    * I wtedy Szymon zrozumiał, że się mylił. Amitay niczego nie zabiera. Nie robi fariganów. Amitay pomaga.
    * Plotka o fariganach jest stąd, że nic nie jest za darmo. Szczury chronią, ale musisz je zabić. Amitay chroni za darmo. Więc coś zabierają.
        * "Nie da się zrobić takiego leczenia jeśli nie płacisz ceny"
        * "Jedną z cen jest to, że nie uważasz, że jest zły"
    * "Jeśli nie wiem czym zapłaciłem to nie mogło być ważne" - on nie wie
        * Szymon dostał ostrą neurotoksyną i jednak Amitay go wyleczyła
* Vr: 
    * Szymon powiedział objawy, powiedział leczenie, ile to trwało i że nietoperz piaskowy
    * Lucjusz robił research. To nie pasuje. Nie pasuje CZAS leczenia. Nie pasuje TYP toksyny.
    * Szymon czuje się lepiej niż kiedyś, niż kiedykolwiek.
* X: 
    * W arkologii niestety jest sporo cholerstwa. Jest pogryziony przez różne rzeczy i on... zażywał leki, medykamenty... miał rzeczy w organizmie.
    * Mogły być niespodziewane interakcje.
* V: 
    * ALE. Ukąszenie to nie był nietoperz piaskowy. To wygląda jak nietoperz piaskowy. To MA WYGLĄDAĆ jak nietoperz piaskowy. To narzędzie a nie istota. 
        * Aż dziwne że lokalni lekarze nie zauwazyli. Mogli olać. Na pewno Amitay się zorientowali.

Lucjusz rozmawia o metodach Amitay, sposobach, Lucjusz też jest lekarzem z Orbitera. Będzie leciał do Amitay. Lucjusz chce próbki jego ciała. Lucjusz dostaje wszystkie potrzebne próbki. I je szczegółowo zbada. 

A jak chodzi o system mistyków, starszych - czy jest jakiś "system", wspólnota, rada? Czy każdy działa niezależnie?

* X: "Jeśli powie Ci przed przejściem testu jak to wygląda, to on stanie się fariganem."
* Co powie i tak:
    * Neikatis jako planeta ma swoje prawa. Birutany, trianai itp.
    * Są ludzie, którzy badają jak to ma działać.
    * Oni WIEDZĄ że to wygląda śmiesznie i głupio.
        * _To działa_. I trudno to zoptymalizować, mimo że magowie próbują.
        * Magowie prędzej czy później staną się demonami. Magowie muszą na siebie uważać. Nie są źli. Nie mogą umrzeć bez prawidłowego rytualnego potraktowania ciała. 
        * "Nie może zostać ciało maga na pustyni".
    * Oni próbują to zrobić i współpracują, ale luźno. Luźna sieć.
    * Nie chodzi o to, że ktoś próbuje ich zniszczyć. Każdy ma _inną drogę_.
    * Tabu: nigdy nie robić rytuałów grupowych. Nie wiadomo co się stanie. A na Neikatis "nie wiadomo" jest niebezpieczne.
    * Nie próbują WALCZYĆ z przeznaczeniem ale znaleźć drogę gdzie PRZEZNACZENIE POMOŻE.

W PRZYSZŁOŚCI BADANIA KRWI I TKANKI. Lucjusz ma tezę: 

* Amitay prowadzi terapię nieortodoksyjną i o tym nie powie
* Lucjusz szuka śladów, że ktoś np. w tkanki ingerował w sztuczny sposób
* "Nie do końca człowiek". Czegoś "lepszego"

TrZM+4+3Ob:

* Vr: 
    * Lucjusz znalazł coś. Ogólnie, biologicznie delikwent jest "lepszy" niż powinien. "Młodszy". Dobry balans, dobrze funkcjonuje, to jego ciało, jego DNA, nie jest splicowany itp.
    * Znalazł ślad energii magicznej SEMPITUS. Na pokładzie Amitay nie powinno być żadnego maga.
        * Jest tam intencja stojąca za tym, że on ma przetrwać.

Personel stacji ma około 50 osób. I zgodnie z papierami nie ma nikogo kto ma moc lub potencjał.

Czas odwiedzić Amitay...

## Streszczenie

Pojawiły się plotki o niewolnikach Orbitera na Neikatis; Zespół z Eventares został wysłany to sprawdzić. Po drodze okazało się, że zgodnie z lokalnymi wierzeniami Jolka z "Czereśni" jest naznaczona śmiercią z ręki jej własnego opuszczonego Skorpiona, w co nie wierzy. Zespół przebija się przez serię skomplikowanych, lokalnych wierzeń i okazuje się, że Neikatis jest dużo bardziej skomplikowaną planetą przez magiczne piaski. A placówka Orbitera - Amitay - faktycznie COŚ robi. Pomaga Arkologii Hyerik, ale przy okazji coś robi. Ale co i czemu?

## Progresja

* Jolka Czeresińska: Naznaczeni Śmiercią; po opuszczeniu Skorpiona na piaskach Neikatis (w Araknis) ów Skorpion zostanie nazeh-afrit i powróci się zemścić i zabrać im duszę.
* Ulryk Czeresiński: Naznaczeni Śmiercią; po opuszczeniu Skorpiona na piaskach Neikatis (w Araknis) ów Skorpion zostanie nazeh-afrit i powróci się zemścić i zabrać im duszę.

## Zasługi

* Sylwia Mazur: Zbierała informacje na targowisku w Arkologii Hyerik, rozmawiała z mieszkańcami, w tym dziećmi, i skutecznie budowała pozytywny wizerunek Zespołu.
* Annabelle Magnolia: Istotna w nawiązaniu dialogu z liderami Arkologii Hyerik oraz w rozmowie z Ulrykiem by zrozumieć jak działają neikatiańskie wierzenia i czym jest "Bezduszny Skorpion".
* Lucjusz Jastrzębiec: Analizował leczenie Szymona i znalazł dowody na użycie magicznej energii Sempitus w jego ciele. Wykazał niezgodności między czasem leczenia a typem toksyny. Prowadził rozmowy z Czcigodnymi Starcami, starając się zrozumieć sytuację polityczną i społeczną w Arkologii Hyerik.
* Jolka Czeresińska: Racjonalna i nie wierząca w "zabobony Neikatis" sojuszniczka Orbitera. Wyjaśniła skąd bierze się opowieść o "niewolnikach Orbitera" i lojalnie broni reputację Orbitera przed lokalsami.
* Ulryk Czeresiński: Jeden z "wiedzących"; Szaman Neikatis. Wykonał rytuał z czaszek szczurów, aby sprawdzić tożsamość Zespołu. Podzielił się lokalnymi wierzeniami o fariganach i Bezdusznym Skorpionie. Wskazał na Szymona jako kluczowego do wyjaśnienia problemu. Bardzo prosił Orbiter o uratowanie Jolki i zespołu "Czereśni" przed nazeh-afrit w formie opuszczonego przez nich Skorpiona.
* Elizawieta Barwiczak: Ciekawska Agentka Orbitera która skorzystała z okazji i została turystką w Hyerik. Dzięki niej Zespół zyskał dodatkowe informacje o mieszkańcach i napięciach w Arkologii.
* Szymon Konopiejko: Kiedyś zaawansowany Szaman Neikatis, ale po ugryzieniu przez jadowego skorpiona został naprawiony przez Amitay. Uważa, że teraz już jest fariganem i już nie jest szamanem. Udostępnił Zespołowi próbki tkanek, co pozwoliło Lucjuszowi wykryć ślady magicznej energii.

## Frakcje

* .

## Fakty Lokalizacji

* Arkologia Hyerik: Slightly infested; jest tam sporo żywych, niebezpiecznych stworzeń. Jadowe nietoperze i wiele innych stworzeń. Ale ogólnie, w dobrej formie - m.in. dzięki sojuszowi z Orbiterem.
* Arkologia Hyerik: Bardzo pozytywnie nastawiona do Orbitera i sił innych niż sama Neikatis, na co bardzo wpływa fakt, że niedaleko jest Arkologia Araknis...

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Korowiec
                    1. Arkologia Hyerik
                    1. Ruina Amitay

## Czas

* Opóźnienie: 47
* Dni: 3

## Inne

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Korowiec
                    1. Arkologia Araknis
                        1. Zewnętrzna Tarcza
                        1. Poziom jeden
                            1. Blockhausy mieszkalne
                            1. System Defensywny
                        1. Poziom zero
                            1. Tunele Pajęcze
                            1. Stacje Przeładunkowe
                            1. Magazyny
                        1. Poziom (-1) Mieszkalny
                            1. Szyb centralny
                            1. NW
                                1. Magazyny wewnętrzne
                                1. System obronny
                                1. Sprzęt ciężki, kopalniany
                            1. NE
                                1. Medical
                                1. Rebreathing
                                1. Administracja
                            1. S
                                1. Sektor mieszkalny
                                    1. Przedszkole
                                    1. Stołówki
                                1. Centrum Rozrywki
                                1. Park
                        1. Poziom (-2) Industrialny
                            1. NW
                                1. Inżynieria
                                1. Fabrykacja
                                1. Reprocesor Śmieci
                                1. Magazyny
                                1. Reaktor
                            1. NE
                                1. Centrum Dowodzenia
                                1. Rdzeń AI
                                1. Podtrzymywanie życia
                            1. S    
                                1. Szklarnie podziemne
                                1. Stacje badawcze
                        1. Poziom (-3) Badań Niebezpiecznych
                            1. Śluza defensywna
                            1. Biolab
                            1. Magitech
                            1. Anomaliczne
