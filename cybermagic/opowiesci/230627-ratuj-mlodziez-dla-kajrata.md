## Metadane

* title: "Ratuj młodzież dla Kajrata"
* threads: hiperpsychotronika-samszarow, pronoktianska-mafia-kajrata
* motives: echa-inwazji-noctis, aurum-supremacja-rodu
* gm: żółw
* players: kamilinux, anadia, kić

## Kontynuacja
### Kampanijna

* [23006 - Piękna Diakonka i rytuał nirwany kóz](230606-piekna-diakonka-i-rytual-nirwany-koz)

### Chronologiczna

* [23006 - Piękna Diakonka i rytuał nirwany kóz](230606-piekna-diakonka-i-rytual-nirwany-koz)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ?
* Struktura: 
    * 230613-typ-sesji-rajd-na-fortece
* CEL: 
    * MG
    * Drużynowy
        * Dostarczyć Strzale osoby znajdujące się w Fortecy
        * Pomóc Strzale się ewakuować
    * Indywidualny
        * Elena: "Ktoś przekształca duchy do torturowania dzieci - nie chcesz pozyskać takiego ducha?"
        * Karolinus: "Dostaniesz wsparcie w znalezieniu swojego przyjaciela, kogoś, kto może działać w cieniu w odróżnieniu od Was"


### Co się stało i co wiemy

* Kontekst
    * Strzała ma uszkodzoną psychotronikę, ktoś grzebał. Trzeba zbadać. Potrzebna jest najlepiej Talia Aegis.
    * Talia pomoże (słowami Kajrata) jeśli uda się uratować "dzieci" trzymane w Technoparku. Warguna i Mitrię.
        * Dowodem na obecność Mitrii jest to, że Cede był uczony przez Talię. Czyli przez Mitrię. It's killing her.
* Wydarzenia
    * .
* Struktura: 230613-typ-sesji-rajd-na-fortece
    * OFIARA: Człowieczeństwo: system jako Moloch jest bezduszny. Odbiera człowieczeństwo swoim podwładnych i zmienia ich w trybiki maszyny.
    * (1) Znając słaby punkt Placówki, zdobywanie Zasobów lub wzmocnienie Słabego Punktu
        * _sukces_: mamy sposób wejścia do Placówki
        * _dark future_: Konieczność znalezienia innego słabego punktu plus Przeciwnik wie o operacji Rajderów.
        * _pokusa_: znalezienie sojusznika w środku, poznanie planów Placówki, wzmocnienie słaby punkt, awaryjna droga ucieczki, spowolnienie czasu reakcji Przeciwnika, zdobycie sojuszników
        * _porażka_: Przeciwnik się spieszy, straty w sprzęcie, atak sił Przeciwnika 
    * (2) Infiltracja Placówki i zlokalizowanie Targetu
        * _sukces_: udane uniknięcie wykrycia, zlokalizowanie Targetu
        * _dark future_: Nie ma dobrego dojścia do Targetu. Trzeba będzie przebić się siłowo i liczyć na umiejętność adaptacji.
        * _pokusa_: okazja zdobycia czegoś jeszcze (spełniającego cele), tymczasowe bezpieczne miejsce (przyczółek), uszkodzenie Zasobów Przeciwnika, ułatwienie wycofania się.
        * _porażka_: Target jest w trudnym miejscu, ekstra zabezpieczenia, Przeciwnik lokalizuje Rajderów, Elita poluje na Rajderów, brak bezpiecznego miejsca
    * (3) Pozyskanie Targetu
        * _sukces_: przebicie się przez zabezpieczenia i pozyskanie Targetu. Uruchamia się 'tor alokacji środków Przeciwnika'.
        * _dark future_: Target jest pułapką Przeciwnika i jest już przeniesiony. Elita interweniuje.
        * _pokusa_: przekierowanie uwagi Przeciwnika gdzieś indziej (spowolnienie dywersją), Target jest w dużo lepszym stanie, pozyskanie sojusznika "w środku".
        * _porażka_: uszkodzenie lub utrata Targetu, wzmocnienie zabezpieczeń przez Przeciwnika, ostry limit czasowy, utrata zasobów, krzywdzenie niewinnych
    * (4) Ewakuacja z Placówki
        * _sukces_: opuszczenie Placówki z Targetem
        * _dark future_: Wszystkie drogi ewakuacji są zablokowane i pełne pułapek. Placówka konfrontuje się z Rajderami pełnymi siłami.
        * _pokusa_: dywersja spowolniająca Przeciwnika, uszkodzenie Placówki i jej głównych celów, przez ruchy Przeciwnika pojawia się nieoczekiwana droga
        * _porażka_: odcięcie drogi Rajderom, atak Elit, uszkodzenie lub utrata Targetu, krzywdzenie niewinnych, uwięzienie Rajderów, ostra utrata zasobów, konieczność planu B
    * (5) Uniknięcie kontrataku
        * _sukces_: zatarcie śladów lub fałszywe ślady
        * _dark future_: Przeciwnik natychmiast przystępuje do kontrataku, z pełną wiedzą o miejscu docelowym Rajderów i z zamiarem odzyskania Targetu.
        * _pokusa_: skierowanie Przeciwnika na innego przeciwnika
        * _porażka_: Przeciwnik poluje na Rajderów i/lub ich cel, konieczność wyścigu, uszkodzenie lub utrata Targetu, 

### Co się stanie (what will happen)

* S1: 
* SN: 
    * Spotkanie z Herbertem w Verlenlandzie, anty-amnestyki, jaszczur
    * Kajrat
    * Operacja: ekstrakcja

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Herbert Samszar
    * ENCAO:  0-0++ | Nie ma blokad;; Uparty jak osioł | VALS: Benevolence, Hedonism >> Self-Direction | DRIVE: Tajemnica Wszechświata
    * styl: GB; peaceful, silent, cycle-focused, nurturing | tradition, community, faith, nature
    * magia: "ten, który śpiewa roślinom"
    * ruch: "ten byt to nie było coś czemu powinno się było dać szansę"
* Ernest Kajrat
    * ENCAO: +0+-0 | Dominujący i asertywny;; Pewny siebie i ambitny;; ambitne i agresywne plany | VALS: Power, Achievement >> Stimulation | DRIVE: Rekonstrukcja Noctis
    * styl: BR; aggressive, ambitious, plan-focused, ruthless | "agresywnie poświęci większość rzeczy by osiągać dalsze plany"
    * ruch: "extreme devastation and malice", "use the family against you", "use resources and sleeper agents"
    * "hate and hunger burns his soul, he controls it like a plasma lance to carve a better future for his people"
* Mścigrom Verlen
    * ENCAO: 0+++- | Przeszłość jest kluczem do przyszłości | VALS: Tradition, Achievement >> Self-Direction | DRIVE: Past ideals, traditions and duty
    * styl: WG; spolegliwy, sympatyczny, bardzo sztywne podejście | "sprawdzi w przeszłości jak zadziałać w teraźniejszości"
    * ruch: 
    * "wszystko co jest potrzebne jest w naszej przeszłości, wszystko da się rozwiązać przez analizę tego co było"
* Impresja Ignicja Incydencja Diakon
    * ENCAO:  +--0+ | dramatic, spontaneous, liberty | VALS: Self-direction, Hedonism, Stimulation | DRIVE: Przygody!!!
    * styl: BR; dramatic, flamboyant, spontaneous, sensitive, "I lead you follow"
    * "wszystko jest jednym wielkim doświadczeniem, rytuałem przejścia, przeprowadzę Cię przez to!"
    * "ROZUMIEM co się tu dzieje" (fiksacja na kolejnej dziwnej teorii)
    * magia: "astralne wizje i mieszanie rzeczywistości"; "amplifikacja tego co jest"
    * ruch: "wchodzi, wszystko zmienia i pozostawia za sobą pożogę"
    * piękna brunetka
* .Dwumetrowy Laserowy Jaszczur
    * Akcje: 'lazzzer breath', 'destroy terrain with lazzzer'
    * Defensywy: 'wężoszyja i lekki pancerz'
    * Słabości: 'brak szczególnych; po prostu nie jest bardzo groźny'

### Scena Zero - impl

.

### Sesja Właściwa - impl

GDZIE wjechać do Verlenlandu by było bezpiecznie? Strzała raz czy dwa opiekując się Samszarami zjechała z drogi. Zna kawałeczki.

Tr Z +2:

* V: (podbicie) jest miejsce, miejsce jest "bezpieczne", nikt Wam złego słowa nie powie
* V: (manifest) żadnych problemów

Miasteczko - okolice miasteczka Duchowiec Śmieszny. Strzała wie o niedaleko znajdującej się jaskini, która czasami służy do wędzenia wędlin. Chyba. Albo innych potworów.

* Vz: Strzała dotarła do jaskini.

Jak tylko ruszyliście do Verlenlandu, Strzała włączyła nagranie wideo -> Karolinus. Karolinus widzi sam siebie. Nagranie. Strzała otwiera mały slot i napis "Karolinus, włóż rękę". Nie wygląda to podejrzanie, co to to nie XD.

Elena i Herbert wymieniają spojrzenia.

Karolinus zażył anty-amnestyki. Potrwa kilka godzin zanim to wszystko się zintegruje, ale to nie problem.

Jesteście w jaskini.

Herbert zaczął opowiadać.

* Spotkałem ICH gdy zajmowałem się roślinami regenerującymi duchy. Dekonstruowały i rekonstruowały.
* Tak naprawdę, to kluczem było leczenie duchów, ale to się przerodziło w coś innego.
* Herbert dostał amnestyki. Nie wszystko pamięta. Ale pamięta, że to są bardzo potężni magowie.
* Wie, że ONI robią COŚ z duchami, ale do tego celu muszą być naprawione.
    * Dekonstrukcja duchów, COŚ i rekonstrukcja duchów.
    * Oni go znaleźli. Aktywnie szukali.
    * Chcieli replikowalny mechanizm "wejścia w DNA duchów", rośliny są dobrym transmiterem.
    * Duchy były _używane_ do czegoś. Transmitowane.
* Herbert pamięta lokalizacje czterech miejsc.
    * (to gdzie wpadła Maja)
    * (Gwiazdoczy, GDZIEŚ w Technoparku)
    * (Siewczyn)
    * ((czwarta lokalizacja))
* Wszystkie lokalizacje i bazy były powiązane z psychotroniką TAI.
* Karolinus: "gdzie widziałeś Cede?"
* H: "na pewno był w Technoparku w Gwiazdoczach, tam go widziałem"
* Elena: "czy duchy ucierpiały? Jak je zmodyfikowali?"
* H: "ucierpiały, to wiem, sporo, naprawdę sporo. Mieli niski success rate"
* E -> K: "kim jest Cede? Czy miał coś wspólnego z krzywdzeniem duchów?"
* K -> E: "Cede to mój stary kumpel którego siostra prosiła żebym go poszukał. Zniknął. Razem byliśmy na studiach. Był psychotronikiem, szkolonym przez noktian. Mieszkaliśmy razem przez pewien czas studiów. Od pół roku się nie odzywa."
* E -> H: "kiedy robiłeś te rzeczy?"
* H: (oś czasowa: już po zniknięciu Cede)
* H: (podejrzewa, że Cede powinien być w Gwiazdoczach)
* Strzała -> K, E: (pamięta że Cede grzebał jej w psychotronice)
* Karolinus: "Cede pracuje dla Samszarów, w tajnym obiekcie, tak jak Herbert pracował z roślinami i duchami to Cede też pracował z roślinami... dziewczyna Cede I CAŁA RODZINA POZA SIOSTRĄ miała sfałszowaną pamięć... Cede uczył się od duchów jak wzmocnić TAI. Nie miał innej opcji. Fabian - który podał mi amnestyki - on szantażował Cede. Chyba."

Strzała -> K, E: "Na ile Herbertowi ufacie?". Herbert MIAŁ podane amnestyki. Najpewniej. Więc niewiele wie. Herbert bardzo mało pamięta - nie pamięta co ZROBIŁ, nie do końca PO CO robił...

Strzała -> K, E: "udawajcie że jesteście rozczarowani, nic nie wiecie i nic z tym nie możecie zrobić. Ryzyko - ktoś kto go monitoruje może chcieć wiedzieć co i jak się dzieje".

Elena próbuje przekonać Herberta, że FAKTYCZNIE nie będą tego ruszać, nie mają jak, "to nie ty nas zawiodłeś", "miałam nadzieję że dowiemy się od Ciebie więcej po tym co dla Ciebie zrobiliśmy..." "cieszę się że dzięki naszym działaniom mogłeś w końcu przekroczyć pewien próg w swoim życiu z Itrią..."

Tr +3:

* X (podbicie) X (manifest): Herbert jest przekonany. Druga Strona nie jest i WIE że Wy "sporo" wiecie.
* Vr: (podbicie): w tej chwili Druga Strona uważa, że nie warto się z Wami o to użerać. Za mało wiecie. Nie szkodzicie im.

Herbert odwieziony, Wy tu wróciliście. Jest to dobre, bezpieczne miejsce. Taka... mała baza w Verlenlandzie. O zapachu wędzonej wędliny.

CO WIEMY:

* Cede jest bezpieczny, robi projekt i wróci za jakiś czas. Nie wiemy czy go dalej szukać.
* Projekt - stoją za nim Samszarowie, odłam mafii Samszarów (?) który broni interesów projektu i ma duże środki i władzę.
* Robią niebezpieczne rzeczy, "programują duchy też wiedzą noktiańską?"
    * JEST ekspert psychotronik od tech. noktiańskich. Nazywa się `Talia Aegis`.
* Pozbyliśmy się szpiegów, niewygodnych elementów (Herberta) itp.

.

* Strzała: "Karolinus, możemy dowiedzieć się co próbowali ukryć w mojej psychotronice."
* Karolinus: "Jak się tego dowiemy?"
* Strzała: "Z pomocą Talii. Musimy... musimy się do niej dostać."
* Karolinus: "A gdzie jest?"
* Strzała: "Daleko. Jest to coś czym nie powinieneś się sam zajmować, ja to zrobię, ale potrzebuję Twojego wsparcia."
* Karolinus: "?"
* Strzała: "Przede wszystkim - krycia. Wysłałeś mnie podrywać laski czy coś."
* Karolinus: "Sama pojechałaś na misję, tak? Czy razem jedziemy?"
* Strzała: "Ty tu zostajesz"
* Strzała: "To nie będzie proste, spróbuję zdobyć Wam wsparcie w jej wyciąganiu. I Ty możesz mi pomóc w tym wsparciu. Mam pomysł KTO. Ale Ty będziesz lepszym przekonywaczem."
* Karolinus: "Dasz na kartce szczegóły czy teraz powiesz?"
* Strzała: "Kajrat. Ernest Kajrat. On ma środki i możliwości. Porusza się między różnymi rodami. Prowadzi biznesy. Różne. Również mafijne."
* Karolinus: "No dobra. Jak go znajdę?"
* Strzała: "Ja go znajdę. Przekonaj go."
* Elena: "A... nie będzie nas za dużo kosztowało? I nie mam na myśli pieniędzy?"
* Strzała: "Dlatego to Wy negocjujecie ^__^"
* Karolinus: "Ubierz się pięknie. Dekolt i te sprawy"
* Elena: "Chcesz mnie przehandlować na pomoc Strzale?"
* Karolinus: "Kolejny klocek - on nam, wydostaniemy Talię i ona pomoże Cede. Pomożemy Kajratowi."
* Karolinus: "Ty ciągle pytasz Eleno o to z kim spałem, niespełnione marzenia?"
* Elena: (sugestia: jak był z Cede w pokoju i miał podane amnestyki to nie wie co się tam działo)
* Karolinus: "Cede miał kasę, spraszaliśmy dziewczyny i bawiliśmy się w ciemny pokój"

ŁĄCZYMY SIĘ DO KAJRATA. Elena - negocjuje. Kajrat kodowany jest jako X.

* Kajrat: tien Elena Samszar. Dużo o Tobie słyszałem.
* Kajrat: jak pokorny właściciel firmy spedycyjnej może Ci pomóc? 20 kg mąki?
* Elena: panie Kajracie, potrzebujemy... wiemy, że może nas pan skontaktować z Talią Aegis; potrzebna jest nam jej ekspertyza w zakresie AI. Mamy dla niej interesujący przypadek.
* Kajrat: piękna Talia jest zajęta. Ale z pewnością doceni zainteresowanie rodów Aurum. Chyba, że... dla przyjaciół Talii można zrobić wyjątek.
* Elena: jak można stać się przyjaciółką Talii? Ostatnio chciałam się z nią bardzo zaprzyjaźnić?
* Kajrat: (uśmiechnął) ktoś w rodzie Samszar robi Talii krzywdę. Nie wiem, czy wie o tym. Ale krzywda się dzieje.
* Elena: na czym krzywda polega? (zaskoczenie)
* Kajrat: czy znasz pojęcie 'sympatii'? (z niesmakiem). Przepraszam, tien Samszar, OCZYWIŚCIE że wiesz co to jest. Talia jest atakowana po sympatii. Przez coś w mieście 'Gwiazdoczy'. Z tego co moi... niezależni kupcy się dowiedzieli, jest taki inkubator przedsiębiorczości. Tam przetrzymywane są dzieci. I te dzieci są kanałem komunikacyjnym z Talią.
* Elena: czy to znaczy że to jej dzieci?
* Kajrat: nie wiem na czym to polega. To nie jej dzieci. Ale Wargun i Mitria są z nią sprzężeni. I magowie rodu Samszar... chyba... krzywdzą te dzieci i Talię. I _przyjaciel Talii_ by to zatrzymał. Chciałbym _odzyskać_ te dzieci.
* Elena: w jakim wieku są?
* Kajrat: pomiędzy 16 a 20. Nie wiem. Ich statek został zniszczony.
* Elena: to noktiańskie dzieci?
* Kajrat: tak. To noktiańskie dzieci. (cicha furia w głosie)
* Elena: pomożesz nam odzyskać dzieci? Swoich niezależnych kupców?
* Kajrat: gdybym miał dostęp... tam jest bariera. Tylko mag rodu Samszar może tam wejść. I wyłączyć.
* Elena: a na inne rzeczy możemy liczyć na pomoc?
* Kajrat: tien Samszar, oczywiście że tak. Mam mąkę, sukno, granatniki...
* Elena: papier?
* Kajrat: jeżeli czegoś potrzebujecie, dacie mi znać. Talia może zginąć przez tą sympatię.

(przewagi: +Kajrat tego chce i potrzebuje tej dwójki)

Ex Z +3:

* V: mafia i Kajrat traktuje temat jako czystą, elegancką transakcję. Póki E+K nie wystąpicie przeciwko noktianom lub przeciwko mafii Kajrata to Kajrat nie będzie robić niczego przeciwko E+K, Kajrat nie będzie nadużywał swoich 'niezależnych kupców', nie będzie szantaży itp.
* V: Kajrat widzi, że są pewne niewygodne ruchy robione przeciwko noktianom na tym terenie. Więc wysyła swoją "córkę" do pomocy. Amandę. Amanda ma pomóc i chronić młodych Samszarów. Jak długo oni nie nastają przeciwko noktianom.
* Vz: Kajrat podał wszystkie szczegóły które ma.
    * zarówno Wargun jak i Mitria są magami.
        * Wargun nie jest podpięty do Talii, ale Mitria tak
            * Mitria jest psychotroniczką. Ktoś czerpie przy jej użyciu z wiedzy Talii
            * Mitria jest w gorszym stanie fizycznym, nie tylko psychicznym. Wargun jest w lepszym
        * Zarówno Wargun jak i Mitria mają specjalny wzór "rodowy" noktiański - Ira.
        * Z perspektywy Kajrata - Mitria "zaczęła atakować Talię" w okolicach zniknięcia Cede. (pół roku temu)

Potrzebujecie 1-2 dni aż Kajrat zmontuje odpowiednie siły na tym terenie.

* Firma gdzie jest bariera nazywa się VisagoEkoHard. Tam zajmują się odnawianiem i optymalizacją ekologiczną TAI.
    * Kupujesz TAI, regenerujesz, szkolisz...
        * (Elena jest zainteresowana bibliotecznym ekologicznym zrównoważonym TAI cywilnym do biznesu)

.

1. Jak się wbić do Placówki?
2. Infiltracja i lokalizacja Celu
3. Pozyskanie Celu
4. Ewakuacja
5. Przekierowanie ("ktoś inny jest winny")

Strzała próbuje zdobyć plany budynku w którym znajduje się inkubator. PLUS plany z sentisieci. Strzała wychowywała różnych magów. Ma stare kody kogoś innego. Ktoś wychowany, poszedł w jakąś inną stronę.

(+poszerzenie uprawnień ORAZ kompensacja krzyżyka, +szerokie dane z różnych źródeł, +w sumie nie ma tam dużo miejsca i kryjówek, a jako że studenci to oni wrzucają własne skany z laborek)

Tp +4 +2Ob +1Or:

* Or: Strzała się zablokowała.
    * Strzała ma malware. Aktywnie zwalcza coś co próbuje uszkodzić pamięć. Strzała JESZCZE wygrywa.
    * Strzała robi backup.
* X: TO KTOŚ INNY PRÓBOWAŁ SIĘ WŁAMAĆ! -> wild goose chase.
* V: (podbicie) Strzała ma pobieżną mapę
* V: (manifest) Strzała WIE, gdzie jest prawdopodobne miejsce "dzieci". Pod budynkiem inkubatora jest ekranowana winda itp.
    * Wiadomo do którego biura iść - gdzieś tam jest ukryta winda.
        * VisagoEkoHard, ale to jest pomieszczenie gdzie robione są TESTY na TAI.
* Ob: 
    * Wacław Samszar wie, że Karolinus adresował sentisieć w okolicy Inkubatora. Czegoś szukał.
    * Karolinus wykrył obecność Wacława Samszara.

Tp (+reputacja Karolinusa bawidamka, +kampus uniwersytecki) +3:

* V: (podbicie) Wacław to łyknął. Karolinus szuka dziewczyny po sentisieci.
* V: (manifest) Wacław to olał. Nawet nie zgłosił mimo że powinien. #just-karolinus-things

KAROLINUS IDZIE ZA REPUTACJĄ! Komunikacja.

* Wacław: czego, kolego? Jestem zajęty /niecierpliwość
* Karolinus: jestem przy okazji w okolicy, chodź zagadać na dwa zdania, mam sprawę
* Wacław: jaką sprawę? Ty tylko dziewczynami się zajmujesz.
* Karolinus: pokażę Ci coś i mi się odwdzięczysz. Może masz znajome.
* Wacław: nie każdy z nas jest sterowany niską chucią
* Karolinus: (zdjęcie na smartfonie Itrii)
* Wacław: WTF! Co? /Karolinus poczuł falę sentisieci
* Karolinus: poznam Cię z nią.
* Wacław: Z taką laską? Czemu chcesz mnie z nią poznać? /niecierpliwy
* Karolinus: wymiana. Ja Cię z taką a TY mnie z inną. Tylko wiesz... to jest PEWNIAK. Liczę na to samo...
* Wacław: Karolinusie... ja nie mam takich koleżanek... /smutek w głosie /zrobiłem coś źle w życiu
* Karolinus: dobra, poznasz, będziesz mi winien przysługę. Pokaż jakie masz siostry, kuzynki.

(+Karolinus. Ma Reputację. +Itria. -zadanie do wykonania pod groźbą. +przekonanie, że to tylko moment)

Tr +4:

* X: (podbicie) Trzeba spotkać Wacława z Itrią.
* X: (manifestacja) Póki Itrii (lub laski analogicznej) nie ma w okolicy, Wacław nigdzie nie idzie.
* V: (podbicie) Obecność Itrii wyciągnie go spod ziemi
* X: (eskalacja) Wacław nie wierzy że ma jakiekolwiek szanse z Itrią. Nawet ITRII by nie uwierzył. Potrzebny będzie coaching od Karolinusa. Długoterminowy.
* (+3Vg) "jeśli on zaliczył bo wytrenowałem to Ty też zaliczysz". V: 
    * jeśli (Itria tu jest) i Karolinus obieca Wacławowi że pomoże mu coachingiem i aktywnym flirtowaniem to Wacław jest skupiony na Itrii.

Amanda ma amnestyki i koktajl narkotyczny. Wstrzyknie mu się.

KAROLINUS JEDZIE DO ITRII. Do Fortu Tawalizer. Strzałą. Itria zrobiła jakąś wielką galę puszczania baniek mydlanych i wszyscy są w dziwnych strojach. Ładnych. Wygląda na wieczór poezji. Dziwny.

* Itria: "Tien Samszar! Niezaprzeczalny ekspert od kóz!"
* Karolinus: (kłania się). Publiczność klaszcze brawo.
* (randomowy koleś przebrany za stokrotkę deklamuje coś o paprotkach Karolinusowi)
* (Karolinus widzi ślady zniszczeń)
* Itria: Przybyłeś z najlepszym wierszem wieczoru by na rumaku porwać mnie w noc?
* Karolinus: (mówi wiersz. Porywa Itrię)
* Itria: (jeszcze w ścigaczu z czystym śmiechem) "Wyciągnąłeś mnie z najgorszej imprezy na jakiej byłam!"
* Karolinus: Mamy robotę do wykonania i nie taką jak myślisz
* Itria: Jaką? Znalazłeś kozę do uratowania? Muszę przyznać, że Ty i Twoja koleżanka wykazaliście się dużą determinacją by uratować los tych cudownych stworzeń.
* Karolinus: A masz chęć spróbować koleżankę?
* Itria: Tak dużo możliwości, tak mało czasu. Zobaczymy gdzie powieje wiatr przyszłości (nawet nie spytała czemu porywacie)
* Strzała: (generuje Karolinusowi teksty) "Szlachetna dama, księżniczka ratująca smoka!" "Zmylony przez złego czarnoksiężnika giermek pilnuje zamku ale chcemy uratować księżniczkę która jest w środku i ktoś musi odwrócić uwagę giermka!"

Tr P (she is bored) +3:

* Xz: (podbicie): Itria spojrzała w oczy Karolinusa, zamrugała i "będziesz mi musiał opowiedzieć o co chodzi..."
    * chodzi o porwane dzieci które my próbujemy porwać i oddać opiekunowi. I chodzi o to, by tamten mag był zajęty i nie miał nadmiaru nieprzyjemności jeśli jest niewinny
* V: (podbicie): Itria będzie na miejscu i masz jej niską współpracę
* V: (manifest): Itria w to wchodzi. AKTYWNIE będzie udawała zainteresowaną Wacławem.
* X: Itria. Jest. ZAINTERESOWANA. Ruchami. Zespołu. BĘDZIE POMAGAĆ! PO SWOJEMU!
* Vr: (eskalacja): Itria ma jako PIERWSZY CEL wyciągnąć Wacława z Mrocznej Sekty.

Itria. Rozwiązuje. Problem z Wacławem.

Elena ukrywa Amandę przed sentisiecią.

Tp +4 +3Ob:

* Vr: Amanda jest ukryta przed sentisiecią. Może wejść.

.

Strzała próbuje przejąć kontrolę nad psychotroniką całego Inkubatora. Strzała jest jedna, mniej sprawna niż zwykle, ale nie ma to znaczenia - tu nie ma super potężnej psychotroniki.

Tr +3:

* V: (podbicie): da się bezpiecznie wejść do Inkubatora i da się otworzyć windę.
* V: (manifest): kontrola Inkubatora do windy włącznie.
* X: (podbicie): ślady że 'Strzała gone rogue'

Mamy dostęp do windy. Wiemy że jest to dostęp w miarę bezpieczny. Zasuwamy do windy - jedyne co to wyjście by mało kto widział. Amanda widzi obecność dwóch strażników w samym Inkubatorze. Patrolujących ręcznie. Na oko - kompetentni, ale... wobec studentów. I rozluźnieni.

Amanda używa dmuchawki z trucizną. Uśpić.

Tp +3:

* X: Trzeba było ich zabić. Uśpienie to za mało. Wymarli.
* Vr: Elena jest przekonana, że Amanda ich tylko uśpiła.

Zjeżdżają w dół windą. Tam jest działko, czeka na wejściu. Amanda wciąga Elenę na sufit (przedtem). Elena przejmuje kontrolę nad działkiem sentisiecią; autoryzuje tą subbazę w sentisieci.

Tp+3+3Ob:

* V: (p): działko jest spowolnione i baza reaguje powoli
* X: (p): baza jest skalibrowana pod Wacława. Nie nad wszystkim Elena przejmie kontrolę.
* Ob: SYGNAŁ w sentisieci. Wacław to olał, jest zajęty Itrią. Ale KTOŚ przybędzie. Limit czasowy. Krótki.
* V: (m): mechanizmy nie będą Wam stawiać oporu.

W tej bazie jest sześć pomieszczeń, jedno "wzmocnione". Trudno się tam dostać. Jest zamknięte. Jedne drzwi się otwierają i wychodzi dwóch kolesi. Amanda atakuje i ogłusza.

Tp +3:

* X: (p) krzyk i lament
* V: (p) obezwładnienie obu
* V: (m) "Tylko Wacław może tam wejść" "nie dostaniesz się tam" (powiedział jak gruba stal itp.)

Elena próbuje się dostroić do drzwi (+ma próbkę Wacława, +ma chwilę czasu)

Tr +4 +3Ob:

* Vr: (p) drzwi zostaną otwarte.
* Vm: (m) drzwi się otworzyły. 

W środku łóżka z metalowymi blokadami. Wenflony, środki, elektrody - cały ekosystem pod magią Eleny się 'otwiera'. Oni zostają uwolnieni. Były na nich prowadzone badania. Amanda paskudnie klnie po noktiańsku.

* Vm: (e) nie tylko magia Eleny zrobiła 'latające dywany' wyciągające nieszczęśników ale WSZYSTKIE INNE DRZWI się zapiekły. Ktoś próbuje wyskoczyć i drzwi się nie otworzą. Elena ochroniła biedaków przed Amandą, która by się NIE powstrzymała.

Elena, Amanda i 'biedacy' ewakuowali się. Amanda zgarnia środki którymi byli faszerowani - dla Kajrata. By wiedział jak im pomóc. Elena szuka dokumentów i USB itp. Zgarnia. Są tu.

NIE MA DOŚĆ ALARMU.

Strzała z dwoma nieszczęśnikami w nocy jedzie prosto do Kajrata.

Itria pozwoliła Karolinusowi odejść. Powiedziała Wacławowi, że ONA teraz z nim porozmawia o Mrocznych Sektach.

## Streszczenie

Herbert powiedział o lokacjach baz i że to prace na duchach. Strzała wie o tym że ma uszkodzoną psychotronikę - skontaktowała Zespół z Kajratem (S. potrzebuje pomocy Talii). Kajrat da dostęp do Talii, jeśli Zespół pomoże uratować noktiańskich młodych magów z niewoli Samszarów. Po znalezieniu bazy Karolinus wywabił na Itrię (którą dołączył do drużyny XD) Wacława (strażnika i ekstraktora) a Elena z Amandą Kajrat wydobyły nieszczęsnych noktian. Strzała odjechała do Kajrata. Kajrat zostawił Zespołowi Amandę do pomocy.

## Progresja

* Karolinus Samszar: przypomniał sobie anty-amnestykami rzeczy które Fabian mu odebrał amnestykami.
* Wacław Samszar: absolutnie zafascynowany Itrią, do poziomu 'dam jej się wyciągnąć z tzw. Mrocznej Sekty'.

## Zasługi

* Karolinus Samszar: dostarczył Strzale mapę inkubatora, ale ujawnił się przed Wacławem. Cóż - szuka dziewczyn. Potem skusił Wacława używając Itrii, POZYSKAŁ ową Itrię i przypadkiem dołączył ją do drużyny. Pomógł Strzale w ratowaniu noktian i ewakuacji do Talii Aegis.
* Elena Samszar: ostro negocjuje z Kajratem, zapewniając m.in. wsparcie Amandy. Potem ukrywa Amandę sentisiecią, wchodzi do bazy hiperpsychotroników i wyciąga uwięzionych noktiańskich magów kombinacją magii i sentisieci. 
* AJA Szybka Strzała: anty-amnestykowała Karolinusa, po czym skontaktowała zespół z Kajratem by móc naprawić swoją psychotronikę używając Talii Aegis. Zdobyła plany inkubatora gdzie jest baza hiperpsychotroników i podpowiadała Karolinusowi jak gadać z Itrią ;-).
* Amanda Kajrat: 26-letnia noktiańska komandoska; ma pomóc i chronić młodych Samszarów z woli Ernesta Kajrata. Zabiła dwóch strażników bazy hiperpsychotroników, przeprowadziła Elenę bezpiecznie do uwięzionych magów.
* Herbert Samszar: powiedział Zespołowi wszystko co wie o hiperpsychotronikach, m.in. że oni jakoś wpływają na duchy i jakie mieli lokacje. Wrócił robić swoje niegroźne rzeczy.
* Ernest Kajrat: skorzystał z desperacji Strzały odnośnie psychotroniki i zapewnił wsparcie Talii w zamian za uratowanie dwóch noktiańskich magów z niewoli Samszarów. Wysłał młodym Samszarom Amandę.
* Wargun Ira: noktianin, 19 lat, technomanta; uratowany przez Zespół.
* Mitria Ira: noktianka, 18 lat, _conduit_ do wiedzy Talii Aegis i psychotroniczka; gdy Mitria jest torturowana, wiedza Talii jest przekazywana Mitrii i przez nią - hiperpsychotronikom. Uratowana przez Zespół.
* Talia Aegis: psychotroniczka połączona sympatią z Mitrią; gdy Mitria jest torturowana, wiedza Talii jest przekazywana Mitrii i przez nią - hiperpsychotronikom. Talia czuje echa, nie może spać i degeneruje.
* Impresja Ignicja Incydencja Diakon: Karolinus zrobił z niej żywą przynętkę dla Wacława. ŻYWO zainteresowana działaniami Zespołu - 'dołącza do zespołu' jako awatar chaosu. Zajmuje Wacława który olał eksfiltrację Warguna i Mitrii. Wyciągnie Wacława z Mrocznej Sekty.
* Wacław Samszar: młody (24) mag hiperpsychotroników, pilnował Warguna i Mitrii by ekstraktować wiedzę z Talii. Karolinus go skusił Itrią i Wacław skupił się na Itrii a nie na zadaniu. Wacław DA SIĘ WYCIĄGNĄĆ Itrii z hiperpsychotroników.

## Frakcje

* Nocne Niebo: pozyskało Warguna Irę i Mitrię Irę - dwóch magów, przy pomocy Karolinusa i Eleny Samszar. Oddelegowali Amandę Kajrat do chłodnego sojuszu z dwójką młodych Samszarów.
* Hiperpsychotronicy: stracili Warguna Irę i Mitrię Irę pod bazą w Gwiazdoczach; Wacław też się okazał niegodny zaufania.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. Duchowiec Śmieszny: miasto nazwane tak dlatego, że jest niedaleko Samszarów a duchy były dla jakiegoś Verlena śmieszne.
                            1. Duchowiec Śmieszny, okolice: w okolicznej jaskini Karolinus, Elena S., Strzała i Herbert dyskutowali o możliwościach
                        1. Powiat Samszar
                            1. Gwiazdoczy
                            1. Gwiazdoczy, okolice
                                1. Technopark Jutra
                                    1. Inkubatory małego biznesu: pod jednym z inkubatorów była baza Samszarów, gdzie prowadzono ekstrakcję wiedzy z Talii przy użyciu Mitrii. Zespół uratował magów noktiańskich.
## Czas

* Opóźnienie: 2
* Dni: 5

## OTHER
### Fakt Lokalizacji
#### Miasto Gwiazdoczy

Dane:

* Nazwa: Gwiazdoczy
* Lokalizacja: Świat|Primus|Sektor Astoriański|Astoria|Sojusz Letejski|Aurum|Powiat Samszar|Gwiazdoczy

Fakt:

Wizualnie, miasto cechuje się harmonijnym połączeniem starych, kamiennych budynków inspirowanych architekturą Oxfordu i Lund (czyli gotyckich), wspomaganych przez sentisieć i nanitki by stworzyć nową, lepszą całość. Wiele budynków posiada nietypowe, dynamiczne formy i struktury, które zdają się wyłaniać się z dawnych murów. Niesamowicie zielone miejsce, co jest wspierane zarówno przez duchy jak i przez sentisieć.

Kalejdoskop Astralny pozwala zobaczyć co jest, co było lub co być może; Gwiazdoczy znajduje się w przestrzeni Szczeliny Światów.

Gwiazdoczy jest niesamowicie dobrze skomunikowane siecią tramwajów i pociągów. Utopijne sci-fi, ale samo miasto nie jest ogromne - ok 120k ludzi.

Technopark Jutra (Gwiazdoczy, Okolice) to prawdziwa wizja przyszłości, łącząca zaawansowane technologie z głębokim szacunkiem dla natury. Oferuje on nie tylko miejsce do pracy, ale także przestrzeń do nauki, eksploracji i relaksu.

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Gwiazdoczy
                                1. Centrum (Centrum)
                                    1. Sklepy i kawiarnie
                                    1. Puby i restauracje
                                1. Dzielnica Akademicka (NW, W)
                                    1. Uniwersytet (NW)
                                    1. Kampus uczelniany
                                    1. Wielka Biblioteka (W)
                                    1. Czytelnie naukowe
                                    1. Muzeum Historii
                                1. Szczelina Światów (E)
                                    1. Kalejdoskop Astralny 
                                    1. Archiwa duchów
                                    1. Plac rytuałów
                                    1. Instytut Sztuki Wspomaganej
                                    1. Wielki Cenotaf Skupiający
                                    1. Magitrownie puryfikacyjne
                                1. Dzielnica Technologii (S)
                                    1. Park Technologiczny
                                    1. Warsztaty i inkubatory
                                    1. Centralna stacja pociągów
                                    1. Centrum Eszary
                                1. Dzielnica studencka (N)
                                    1. Strefa Sportowa
                            1. Gwiazdoczy, okolice
                                1. Technopark Jutra
                                    1. Brama wjazdowa
                                    1. Inkubatory małego biznesu
                                    1. Siedziba główna
                                    1. Park i rekreacja
                                    1. Pola testowe
                                    1. Obelisk Mędrców
