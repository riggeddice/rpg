## Metadane

* title: "Herbata, grzyby i mimik"
* threads: wojna-o-dusze-szczelinca
* motives: corrupting-parasite, wojna-miedzy-firmami, pracownik-nalesnik, teren-skazony, katastrofa-lokalna
* gm: żółw
* players: kić, kapsel

## Kontynuacja
### Kampanijna

* [190313 - Plaga jamników](190313-plaga-jamnikow)

### Chronologiczna

* [190313 - Plaga jamników](190313-plaga-jamnikow)

## Projektowanie sesji

### Pytania sesji

* .

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Past

1. Mimik ściągnął w okolice potwory i inne istoty, które wymagały czegoś więcej niż konstruminusów. Almeda straciła power suit w akcji.
2. Mimik Symbiotyczny zainfekował Almedę. Almeda zaczęła coraz skutecznie pokonywać potwory.
3. Almedzie było mało - rozpoczęła wojnę między dwoma frakcjami przez wprowadzenie grzybołaza do Herbastu.
4. Mafioso wysłał swoich ludzi by wyjaśnili Jadwidze, że to głupi pomysł. Jadwiga ucierpiała - uszkodziło jej farmę grzybów.
5. Jadwiga wezwała na pomoc magów z Pustogoru.
6. Almeda podpaliła Herbast gdy przychodzą terminusi.

### Dark Future

1. Almeda umiera.
2. Mimik ucieka.
3. Jadwiga jest zmuszona do opuszczenia terenu.
4. Dariusz zajmuje hold na terenie.

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (20:50)

_Skałopływ, Herbast_

Nasi terminusi zbliżają się do HerbAst. A tu pożar. Płoną herbaty, eksplodują maszyny. Pożar jest solidny, dotyczy kilku budynków. Zagrożeni: [ludzie, maszyny, towar]. Antoni przejmuje dowodzenie by ugasić pożar i ochronić ludzi. (Tp:S). Udało się - oddał Pięknotce możliwość działania. Pięknotka użyła bomb gaśniczych (TpM+2=S). Cały ten potężny pożar został ugaszony. Nie ma katalistów w okolicy, ale jest dwóch konstruminusów. I nie ma magów.

Konstruminus odmawia współpracy z terminusami, co ich bardzo dziwi. Nie ma ich w bazie. Nieistotne; nasi terminusi wykorzystali analityczne artefakty by dowiedzieć się co tam jest i co spowodowało pożar. (TrZ=SS). Eksplodujące, ogniste grzyby - obraz został w lustrze. I ktoś humanoidalny je podłożył. Jadwiga? (Tp=S). Nie, niemożliwe. To taka grzybowa mama. Wszystko wskazuje na nią, ale... ona nie poświęciłaby swoich ukochanych grzybów.

Przybył Darek. W panice. MIAŁ AMULET ODPORNY PRZED OGNIEM. I spłonął. Kupił go od Stacha Sosnowieckiego (tu facepalm Pięknotki i Antoniego). Spróbowali go uspokoić (Tp=SS), ale jedynie udało się go zacietrzewić na Jadwigę. Ale się wstrzyma, nie będzie utrudniać.

_Skałopływ, droga na komendę_

Skażony Konstruminus przez Almedę zestrzelił pojazd Zespołu. Konstruminus udawał że strzela do Antoniego ale przestał. I Antoni go rozsiekał (Tp:SS). Przybył Baltazar i zaczął się żołądkować. Przyjechali na safari polować na jego konstruminusy. Dla Antoniego było to NAJGŁUPSZE co usłyszał. Wyciągnął brudy Baltazara i przedstawił dowód że coś tu się dzieje (TpZ+3=S). Baltazar jest wkurzony, ale będzie współpracował.

_Skałopływ, Podfarma_(22:00)

Jadwiga dała Pięknotce system grzybowy. Żywy komputer. Cel ma na sobie zarodniki. Pięknotka synchronizuje się z grzybem. (TrZ:12,3,4=S). Mają dane osoby. Almeda Literna. Antoni od razu skupił się na Almedzie - o co tu chodzi? (Tp:9,3,3=S). Wykryli mimika symbiotycznego na Almedzie. Almeda jest już daleko przekształcona, ale nie dość daleko.

_Niedaleko Skałopływu, Klify_(22:20)

Zespół dotarł do takiego pustynnego terenu. Almeda tam jest. [obszary: Almeda nie żyje, sprzężona z mimikiem, ogromna siła ognia, pancerz]. Mapa z plechy najlepsza.

Antoni wyzwał ją na pojedynek (Łt:11,3,1=S). Almeda wyszła. Powiedziała, że wie o magach - robiła to wszystko, by mieć wreszcie godnego przeciwnika do walki. Pięknotka zaatakowała z zaskoczenia swoją snajperką (Tp:12,3,2=S). Power suit został uszkodzony. Mimik nie ma pola biomagicznego. Almeda jest otwarta (pancerz--) - Antoni wykorzystuje. Atakuje na pełnej mocy, nie dbając o stan Almedy (Tp=S). Udało się uszkodzić powiązanie między nimi.

Mimik infekuje Antoniego (Tp:S.) Odparł mimika. Antoni zmasakrował mimika najbrutalniej jak umiał.

Almeda umiera. Pięknotka odpala artefakt stabilizujący. Almeda jakoś funkcjonuje. Almeda dostała odpowiednią pomoc i jakoś rozwiązali tą sprawę.

Wpływ:

* Ż: 17 (22)
* KK: 3

**Sprawdzenie Torów** ():

2 Wpływu -> 50% +1 do toru. Tory:

* Almeda umiera.
* Mimik ucieka.
* Jadwiga jest zmuszona do opuszczenia terenu. 10 = VVVXV
* Dariusz zajmuje hold na terenie. 12 = XVXXXX

**Epilog** (22:45)

* Jadwiga jest zmuszona do opuszczenia tego terenu ze swoimi grzybami, dyskretnie
* Dariusz nadal działa tak jak zawsze działał, ale Jadwiga musi się wynieść

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
|               |         |             |

Czyli:

* (K): Dariusz chce robić herbatę z grzybów (1)
* (Kc): Dariusz nie widzi w Jadwidze wroga (1)
* (Kc): Baltazar dostanie pomoc by nie był sam (2)
* (K): Pięknotka zarufusia trochę grzybów (1)

## Streszczenie

Na Skażonym terenie, w Skałopływie, doszło do waśni pomiędzy bizneswoman od grzybów a biznesmanem-mafioso od herbatek. Pięknotka i Antoni dotarli na miejsce, zagasili pożar, przetrwali atak zdominowanego konstruminusa i odnaleźli PRAWDZIWEGO sprawcę waśni - przejętą przez mimika symbiotycznego policjantkę Almedę. Skutecznie ją pokonali, ale w wyniku tego wszystkiego Jadwiga (bizneswoman od grzybów) musi opuścić Skałopływ, przy ogromnej żałości dla siebie i miłośników grzybów z Pustogoru.

## Progresja

* .

## Zasługi

* Pięknotka Diakon: synchronizowała się z grzybami i gasiła pożar bombą chemiczną; pokonała z Antonim mimika symbiotycznego.
* Antoni Kotomin: dowodził ludźmi w obronie przeciwpożarowej a potem słownie wdeptywał idiotów w ziemię; pokonał z Pięknotką mimika symbiotycznego.
* Dariusz Bankierz: mag-mafioso, dowodzi Herbastem. Dobry organizator i ogólnie nie jest szczególnie ZŁY. Taki kindermafioso. Nie poszerzył wpływów i potęgi, ale doprowadził do wygnania Jadwigi.
* Jadwiga Pszarnik: królowa grzybów i właścicielka Podfarmy. Ultra-wyspecjalizowana biomantka. Skłócona z Dariuszem przez mimika w rękach Almedy; zmuszona do opuszczenia Skałopływu.
* Almeda Literna: policjantka zainfekowana Mimikiem Symbiotycznym; bardzo ambitna i chętna do ochrony. Niestety, skłóciła Dariusza i Jadwigę przez mimika.
* Baltazar Rączniak: terminus ze Szczelińca, lokalny; coś tam zawalił w przeszłości i musi zajmować się tym miejscem. Nieszczęśliwy, nie lubi "Centrali" w Pustogorze.
* Stach Sosnowiecki: KIEDYŚ WCZEŚNIEJ był na tym terenie i sprzedał amulet ognioodporny Dariuszowi. Oczywiście, ów amulet spłonął.

## Frakcje

* Terminuscy Strażnicy Szczeliny: często wysyłają swoich terminusów do pomagania też innym terenom, m.in. niedaleko poza obszar Pustogoru w kierunku na Enklawy

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, SW
                    1. Granica Anomalii
                        1. Skałopływ: miasteczko na Terenach Skażonych, dobrze bronione. Dwa główne biznesy - grzyby i herbata z odpowiednio Skażonego terenu.
                            1. Strażnica: centrum defensywne Skałopływu, dowodzone przez Baltazara Rączniaka.
                            1. Herbast: herbaty i kawy astoriańskie; zarządzane przez mafioso imieniem Dariusz Bankierz.
                            1. Podfarma: farma dziwnych, Skażonych grzybów dowodzonych przez Jadwigę.
                            1. Hotel Pustogorski: miejsce, gdzie śpią i mają jako bazę Pięknotka oraz Antoni.
                            1. Osiedle Bezpieczne: Tam mieszkają wszyscy ludzie w Skałopływie.
                            1. Jezioro Macek: główne źródło energii magicznej i Skażenia dla Skałopływu. Opanowane przez magitrownię.
                            1. Martwy Step: anomalny teren na północnym wschodzie Skałopływu; bardzo suche miejsce z dużą ilością kryjówek i załamań terenu.

## Czas

* Opóźnienie: 3
* Dni: 2
