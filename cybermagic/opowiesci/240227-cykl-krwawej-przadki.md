## Metadane

* title: "Cykl krwawej prządki"
* threads: brak
* motives: energia-fidetis, energia-esuriit, the-fateweaver, tools-of-enslavement, zemsta-uzasadniona, strach-ma-wielkie-oczy, lojalny-do-konca, pragne-odkupienia
* gm: żółw
* players: magdawrr, kić

## Kontynuacja
### Kampanijna

* [240227 - Cykl krwawek prządki](240227-cykl-krwawej-przadki)

### Chronologiczna

* [240227 - Cykl krwawek prządki](240227-cykl-krwawej-przadki)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * L'Ame Immortertelle "Rearranging"
        * But in the end it all stays the same | No changes made, no lessons learned | Am I the one who is to blame | For smashing crowns that I have earned
        * Fateweaver zamyka cykl.
* Opowieść o (Theme and vision): 
    * "Zemsta jest cyklem. Zemsta nigdy się nie kończy. Cierpienie raz zadane wróci i skrzywdzi ponownie. Czy przerwiesz cykl?"
        * Michał: Straciłem rodzinę. Rodzina Róży mnie zniszczyła i zabrała mi wszystko. ZABIŁEM moją rodzinę. Zrobili mi straszną krzywdę. Żadna inna rodzina nie będzie tak cierpieć jak moja.
        * Róża: Michał zabił moją rodzinę, wszystko co kochałam. Teraz ja zniszczę jego i wszystko co on kocha.
    * Lokalizacja: Arkologia Terkelis
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * energia-fidetis: cykl nienawiści oraz zemsty, zawsze jest Zabijający i Zabijany. Łowca i Ofiara. Z tego zrodzony jest Fateweaver Pętli, który powtarza wydarzenia na innych podmiotach.
    * energia-esuriit: cykl nienawiści oraz zemsty, zawsze jest Zabijający i Zabijany. Łowca i Ofiara. Z tego zrodzona jest Fateweaver Pętli, który powtarza wydarzenia na innych podmiotach.
    * the-fateweaver: Pajęczy pasożyt Esuriit-Fidetis. Inkarnacja Cyklu. Zawsze jest Zabijający, zawsze jest Zabijany. Zawsze jest Łowca, zawsze jest Ofiara. Żąda krwi niewinnych by się zasilić. Tatuaż.
    * tools-of-enslavement: sama Róża. Jej pocałunek ją żywi, nadając esurientowe wyssanie. Zabija ofiary nadając im Obsesyjny Rozkaz najpierw.
    * zemsta-uzasadniona: pętla zemsty. Wpierw rodzina Michała została zniszczona przez ojca Róży, potem Michał zniszczył rodzinę Róży, potem Róża polując na Michała zabija niewinnych...
    * strach-ma-wielkie-oczy: Michał miał być tym straszliwym potworem, niezwykle zdolnym groźnym i niebezpiecznym. Okazał się starym słabym człowiekiem, nie mogącym walczyć z potworną Różą.
    * lojalny-do-konca: Michał przez swoje działania i przeszłość ma grupę lojalnych sobie ludzi, przydatnych i dobrych. Tu: Wincenty Teriak (subtyp: 'Cyclonus/IDW').
    * pragne-odkupienia: Michał robi wszystko, by zadośćuczynić za swoją przeszłość. Filantrop, który robi co w jego mocy by pomóc niewielkiej arkologii w której w końcu zamieszkał.
* Dilemma: Sprawiedliwość czy Wybaczenie?

### Co się stało i co wiemy

* Róża Kalatrix pochodzi z mafijnej rodziny (jak Andrea). Jej rodzice przejęli potęgę i majątek rodziny Michała, używając Michała do zniszczenia jego rodziny.
* Michał się uwolnił dzięki Fateweaverowi. Okrutnie się zemścił na rodzinie Róży, zabijając wszystkich (chciał ŁĄCZNIE z Różą).
    * Fateweaver przeniósł się na Różę.
* Michał Castelli, pozbawiony esurienckiej energii Fateweavera, wykorzystał pieniądze Kalatrix + Castelli by pomóc w upadającej arkologii Terkelis.
* Róża i _jej echo_ 

### Co się stanie (what will happen)

* F0: TUTORIAL
    * Młoda Róża bawi się z koleżanką w rezydencji rodziców.
    * Wpada Michał z kolegą, terroryzują, łapią całą rodzinę. Poza Różą. Strzelają i zabijają matkę. Szukają Róży.
        * Róża wyjdzie czy wypchnie koleżankę?
        * Michał wszystkie drzwi zamyka kinezą, część strażników stoi po jego stronie (Rafał, który pomagał Róży)
        * oczy goreją krwią, twarz pokryta jest jakby porcelanową maską
* F1: Róża jest ukryta
    * 'Michał Castelli' jest znanym i szanowanym członkiem społeczności
* F2: Róża jest wykryta
    * Wincenty atakuje Różę
    * Instytut, Lucyna Castelli jest tam 
* F3: Ostateczna konfrontacja i decyzja
    * servar. W jego rezydencji albo w przeładowni
    * ona jest jak on był
* Overall
    * chains
        * ?
    * stakes
        * czy cykl trwa dalej?
        * Lucyna, Wincenty, ilość niewinnych
    * opponent
        * Michał, ale też sama Róża
    * problem

## Sesja - analiza
### Fiszki

* Michał Castelli: Derek Powers PROFIL POWAŻANIA|BatmanBeyond
    * Wincenty Frak: Cyclonus|IDW
    * Lucyna Castelli: HotRod|G1

#### Strony

.

### Scena Zero - impl

Mała dziewczynka. Bawi się ze swoją koleżanką w takim dworku. Podwieczorek. Dźwięki strzałów. Chce się schować gdzieś gdzie - wentylacja. Chce móc się dowolnie wycofać.

Ex +3:

* Vr: Dziewczynka jest w stanie się wycofać.
* V: Dziewczynka pomoże uciec koleżance

Uderzenie telekinetyczne. 4 osoby to ochroniarze. 1 to służący. Jego oczy ociekają krwią. A skóra jest porcelanowa.

Tortury rodziców.

### Sesja Właściwa - impl

Dziewczynka miała na imię Róża. Po latach udało się zlokalizować Michała. Zostawił za sobą ślad krwi. Twoi rodzice nie byli jedyni. Róża i Jola. Róża - Dyplomata - jest z wyższych sfer.

* PYTANIE: W jakich obszarach po dotarciu do arkologii szukałyście wrogów Michała lub informatorów?
* ODPOWIEDŹ: brudne szczury uliczne.
    * macie kontakt z Karolem Wisielnickim (Starscream)
* PYTANIE: A gdzie i z kim przestawała Róża - jakie kontakty zdobyła przyjaźń i zaufanie?
* ODPOWIEDŹ: aktywiści. 
    * kontakt z Tomasz Dąbek, aktywista.

Rezydencje - tam mieszka Róża z Jolą, ale też mieszka w jednej z nich Michał Castelli. On nie wie kim Róża jest.

Jak się dostać do Michała Castelliego? Co robi w Instytucie?

Róża by chciała, by Jola wbiła się do środka LUB przyprowadziła gościa by przekupić albo zniewolić. Zdobyć dostęp do Instytutu. Instytut oficjalnie zajmuje się badaniami nad chorobami dziecięcymi. Jola rozkminia gdzie ludzie z Instytutu się udają po pracy i potem jak będzie miała ogarnięte, spróbuje wziąć i pójść do tej kawiarni i posłuchać, zidentyfikować kto i co - szuka na tym etapie ludzi którzy są niezadowoleni, lub gryzie sumienie, bardziej podatnych na porwanie lub przekonywanie.

Obserwacja Instytutu: sam Instytut jest chroniony oraz monitorowany. W Instytucie działają. To są Castelliego. Instytut jest świetnie CHRONIONYM miejscem. Ma automatyczne działka.

Tr Z+3 +2Or:

* V: masz 2 potencjalne osoby
    * jedna wpadła w długi. Potrzebuje pieniędzy (Janusz).
    * druga ma poczucie winy. Nie wytrzymuje psychicznie ale się boi. (Edward)
    * obie osoby są niskiego stopnia w Instytucie

Róża się przebiera by się lepiej wtapiać i idzie do kawiarenki Przyszłość. Róża wchodzi. Pierwszy - ojcowski, chce pomóc (Edward). Drugi opryskliwy (Janusz).

Jola potyka kelnera, by ten się przewrócił i coś się stłukło. Odwrócić uwagę i zrobić szkło. Janusz na szkło (zaciąć), Edwardowi Róża pomaga wstać, zacina go żyletką. Oboje mają być pod kontrolą. 

Tr Z (Róża) +3 +3Or:

* Vr: Jolka przewróciła kelnera. Kelner się wywrócił i rozbiła mu się butelka. Jolka pociągnąła Janusza i przewróciła go na szkło, jest ranny, przedramię pociekło. Róża złapała Edwarda i krew poszła z dłoni. Jest dotyk pająk - ciało.
    * Róża złapała Janusza za krwawiącą rękę i pająk - ciało. Masz obu.
* V: takie rzeczy się zdarzają. Nikt nie zwraca na to uwagi. Jakiś śmiech.
    * Obaj patrzą na Różę szklanymi oczami.  (+1Vg)
* Xz: Róża została rozpoznana. Więc "arystokratka pragnie się zabawić z normalnymi ludźmi". Pokój.

.

* Janusz: Co pragniesz wiedzieć - pała żarliwie. On CHCE pomóc Róży.
* Róża: Co się dzieje w instytucie, wszystkie informacje, co jest podejrzane

.

* Do Instytutu przybywają dzieciaki z różnych miejsc. Często dziwne, niebezpieczne, problematyczne.
* Na tych dzieciakach robione są eksperymenty - jakieś leki, substancje
* Czasami krzyczą
* Jest duża dyscyplina. Dzieją się tam rzeczy DZIWNE, NIENATURALNE.
* Czasami umierają
* Czasami niektóre dzieciaki trafiają do prywatnej armii -> do arkologii, jako normalni obywatele. I Michał Castelli fałszuje im papiery
* Nad Instytutem czuwa Castelli i jego córka. Lucyna Castelli.
* Widzieli Michała. Michałowi zawsze towarzyszy Wincenty (neurosprzężony komandos).
* Instytut ma systemy defensywne w środku. Nie tylko by nikt nie wszedł. Nikt nie może wyjść.
* Ciała dzieciaków są badane, niektóre -> żywność a niektóre do chłodni i są potem wywożone

Róża: Edward i Janusz mogą iść robić swoje rzeczy. Niech - jeśli mają możliwość - porozmawiają z dziećmi i dowiedzą się czegoś. Nazwiska. Skąd dzieci są.

Róża chce się dowiedzieć jakie słabe punkty Michała, Lucyny i Wincentego. Jola skupia się na przeciwnikach Instytutu, i wrogach samego Michała.

Tr (niepełny) +3:

* V: przeciwnicy Michała:
    * "stara gwardia". Zanim Michał tu był to rządziła tu grupa bogatszych ludzi. Oni byli ubożsi. Arkologia była uboższa. Oni stracili wpływy, reputację...
        * oni są przeciwnikami Michała. Bogaci ludzie, rodzina Kilskich to ogromni przeciwnicy Michała. Wincenty zabił kilku z nich (próby zabójstwa)
    * "humanitaryści". Im się nie podoba, że Michał próbuje działać "dziwny sposób z dziwnymi ludźmi".
    * Michał mocno zainwestował w arkologię. W logistykę, w handel, dofinansował, użył kontaktów personalnych - jest lubiany.
        * Wincenty przybył z nim. Stary przyjaciel.
* X: Wincenty wie, że KTOŚ szuka informacji na ich temat. Może Kilscy. Może ktoś inny. Ale ktoś się interesuje.
* Vr: Coś o przeszłości Michała i ekipy
    * Michał przybył do tej arkologii uciekając przed czymś. Do dzisiaj ucieka przed czymś lub kimś. I jest czujny.
    * Michał uratował życie Wincentemu. Tak wielokrotnie. A Wincenty jemu. Jak bracia.
    * Lucyna nie jest krwią z jego krwi. Jest przybraną córką. Ona jest jego spadkobierczynią. Dla Lucyny zrobiłby wszystko.
    * Lucyna wyszła z tego Instytutu. "Przeszła przez Instytut"
        * Nikt nie chce zadzierać z Lucyną
    * Instytut nazywa się 'Instytut Alany Castelli'
        * Alana Castelli to była córka Michała. Nie żyje.
* V: Alana...
    * Zgodnie z informacjami które pozyskała Róża na przestrzeni czasu
        * Alana zginęła dawno temu. Michał ją zabił.
        * Michał zabił całą swoją rodzinę zanim został służącym rodziców Róży
        * Michał - gdy był służącym rodziców Róży - miał 39 lat. Alana miała 7 lat. Jego żona miała 38 lat. Michał zabił wszystkich. A potem rodzice...
    * BOLI CIĘ GŁOWA. Pulsuje Ci ręka.

Róża chce się dowiedzieć czegoś o sobie i tatuażu. Róża zakłada kaptur, wychodzi w nocy z bogatej dzielnicy do obszarów inżynierii. Tam jest ciepło i głośno. Zawsze tam są jacyś ludzie. I często tam są ludzie 'wypić i spokój'. Jola szuka i izoluje dla Róży nieszczęśnika.

Tp Z +2:

* V: Jola odizolowała jakiegoś nieszczęnika. Róża podchodzi do niego z uśmiechem. On się głupkowato uśmiecha do Róży.

Strach w oczach pijaka zmienia się w uwielbienie, gdy krew dotyka pająka. Robi dla Róży wszystko.

* (9V3Or): Or: nie boli Cię głowa. Jesteś w swoim pokoju i czujesz się WYŚMIENICIE. Minęły 2-3h.

Jola + Róża poszły polować na kolejną osobę. Tatuaż wygląda tak samo.

* V: Róża i Jola szły w podobne miejsce. Tym razem są tam policjanci. Coś musiało się stać.
    * "to musiała być straszna śmierć"
    * "co to mogło zrobić?"
    * "ojciec dwójki dzieci"
    * "co tu się stało, to musi być Instytut"
    * "ani kropli krwi w ciele"
* V: Jola się przekradła.
    * policjanci próbowali ZOBACZYĆ Jolę. Nie umieli.
    * trudno rozpoznać ciało. Ani kropli krwi. Wyssane do zera.
        * Róża ich zabiła

Gdy się zbieracie do działania - przyjechał pojazd opancerzony. 4 uzbrojonych agentów, wyszkolonych, młodych (dzieciaki z Instytutu) oraz Wincenty. Rozmawiają z policjantami.

Jola robi coś co powinno zaskutkować "nie chodzić" ale bez szczególnego działania.

Tr Z+3:

* Vz: Jola podeszła tam gdzie nie powinna. NIKT jej nie zauważył - ale jeden dzieciak tak. "Hej, co tu robisz?". Inni patrzą bez zrozumienia. Wincenty patrzy na dzieciaka. Patrzy na Jolę. Patrzy na dzieciaka.
    * "Co widzisz? CO WIDZISZ?! OGNIA!!!"
    * Jola mina 'przerażona wtf' i ucieka.
    * nikt nie strzela
    * Wincenty: "czy ten człowiek nie ma krwi? Stracił całą krew?" (przerażony)
* X: Nie dowie się więcej.
    * Wincenty "Diva... (ale nie usłyszane)"
    * Jola zapamiętuje co widział.

Pojazd opancerzony UCIEKŁ.

Jola próbuje kręcić się koło Instytutu i dać się zobaczyć. Gdzie jest ten jeden wychowanek, ten, który ją widzi. (WIDZIAŁAŚ tego jednego. Jest zawsze w towarzystwie Wincentego). Jola szuka wśród wychowanków Instytutu kogoś kto ją widzi.

Róża zapolowała na kolejną ofiarę. Chce się dowiedzieć, czy może uwolnić czy znowu zabije. I nagrać to na kamerze - by mieć pewność, że zobaczy. I nie jak ostatnio a nakazać tej osobie się uwolnić - "jesteś wolny".

Tr Z (szczury uliczne) +1 (porusza się po wrogim obszarze):

* V: Róża przesuwa się po ciemniejszych miejscach - UNIKNĘŁAŚ CUDEM patrol policyjny, drony. Trudno się porusza. Ale się udało. Nadal - jest w stanie zlokalizować kogoś, kto naprawia jakieś uszkodzone urządzenie.
    * poczułaś jak Twój tatuaż, jak pająk wbił żuwaczki w jego ciało.

Róża przemyka się z powrotem do pokoju. Wie już, że sytuacja jest niebezpieczna i umie coś więcej. Próbuje sięgnąć do swojej pamięci. Przygotowanie - jakieś zioła i płyny, w internecie "top 10 na udaną autohipnozę".

Ex +2 +4Or:

* Or: 
    * jak chodzi o tatuaż, JAK się pojawił, SKĄD?
        * uciekałaś z dziewczynką. UCIEKAŁAŚ. I... i prawie zginęłaś. 8-letnie dziewczynki uciec same?
            * było źle. Było tak strasznie. I pojawił się pająk. "to niesprawiedliwe, prawda?" "skrzywdzili Cię, prawda?" "musisz się zemścić, prawda?"
                * zaakceptowałaś opiekę pająka. Dał Ci wiele. Ty go karmisz. On Cię wzmacnia.
                * KAŻDA osoba którą KIEDYKOLWIEK przejęłaś jest martwa. Za Tobą też zostają strumienie krwi. Zasiliłaś pająka.
                * to żywa istota która jest Twoją częścią
* X: Michał wie KIM jesteś. Oni się zbliżają.
* X: WALCZYŁAŚ. A raczej - PAJĄK WALCZYŁ TWOIM CIAŁEM. (+4Vg)
* Vg: 
    * Kojarzysz WIĘCEJ. Rozumiesz WIĘCEJ. Pochodzisz z rodziny mafijnej.
    * Michał pochodził z konkurencyjnej mafii.
    * Michał miał tatuaż pająka. Miał go na twarzy.
        * Pająk go uwolnił. A teraz Ty masz pająka.
        * Rodzice Róży użyli narkotyków. Michał zabił wszystkich których kochał pod rozkazami rodziców. 
* Vr: 
    * Jola nie istnieje. Nigdy nie było Joli. Jola jest manifestacją Twojej energii magicznej. Jola istnieje - dla Róży. I tylko dla Róży.

Róża WRÓCIŁA DO SIEBIE. Znajduje się w okolicach magazynów. Ubranie porozdzierane, podarte. Co najmniej 30-40 osób nie żyje. Wybuchy, wyssanie z krwi, pocięcie, ślady ostrzy pajęczych. Policjanci, ludzie Michała, jakiś cywil. Ale Ty - Ty się oddalasz. Bezpiecznie i niewidocznie.

W chaosie - wszyscy rzucili się za Różą - duża populacja. Jola tylko czekała, cierpliwie jak pająk. I szukała. Wypatruje kogoś podatnego na przejęcie, wartościowego. Nieuważnego, nieostrożnego. Cokolwiek co ma wejście do Instytutu.

Tr +4 +3Or:

* Or: Jola musiała zabić dwie osoby, ale dostała się do kogoś bardziej wartościowego
* Vr: Naukowiec, który wie co tam się dzieje.
    * Adam Wojczak

W obszarze Energii - elektrownie, reaktory, inne takie siedzi Róża z porcelanowym obliczem i krwawiącymi oczami. Do niej Jola przynosi przerażonego naukowca (Adama).

* Adam: Kim Ty jesteś?! Czego chcesz!?
* Róża: Co się tam dzieje, tam gdzie pracujesz.
* Adam: Ty jesteś Diwą! Prządką Krwi! Seilio, ratuj nas!!! MAM RODZINĘ!!!

Róża chce wiedzieć wszystko o Michale. Co tam robi. Prywatnie też.

* Adam: Michał jest stary i słaby. Michał ma słabe serce i odrzuca przeszczepy, nie da się go zcyborgizować, nie da się też go naprawić w inny sposób.
* Adam: Naprawdę, Michał przekazał wszystko Lucynie. On nadal dowodzi, ale ona jest jego PRAWDZIWĄ spadkobierczynią. 
* Adam: Michał stara się, by żadne dziecko nie skrzywdziło swojej rodziny. On próbuje... 

Róża chce to skończyć. Róża się chce zabić? Dogadać z Michałem? Nie chce dalej przekazać Pająka... nie chce...

Róża nie zjadła naukowca. Dała mu odejść. I dała komunikator. Chce z Michałem porozmawiać.

5h później telefon.

* Michał: Diwa. Krwawa Prządka. Królowa Cyklu.
* Róża: A więc dzwonisz, chcesz się spotkać?
* Michał: Tak to zakończymy? Tak. Spotkajmy się w magazynach. Przyjdę sam. Jedno z nas wyjdzie. Ale - nikogo więcej nie zabijaj. Pasuje?
* Róża: Nie chcę nikogo zabijać. Dlatego... wiem już co robisz w Instytucie. To moja jedyna nadzieja.
* Michał: Nadzieja? Jesteś Diwą?
* Róża: Nie chcę robić tego co do tej pory. Nie kontrolowałam tego. Wiesz o tym najlepiej.
* Michał: Chciałbym... chciałbym móc powiedzieć, że nigdy nie mogłem przerwać (żal). Jak się nazywasz?
* Róża: Róża
* Michał: Przepraszam. Nie wiem kim jesteś. Nie znam żadnej Róży. A... a wiele osób zostawiłem, którzy mogli przejąć Diwę. 
* Róża: Jestem tą osobą. Mam Diwę. Potrzebuję się jej pozbyć. Ale nie w sposób by mogła dalej szerzyć zniszczenie. Chcę to skończyć raz na zawsze, nie tylko się uwolnić.
* Michał: Próbuję naprawić to co zrobiłem. Pomogę Ci.
* Róża: Dobrze.
* Michał: Spróbuję... zadośćuczynić to co się stało. To co Ty zrobiłaś. Ale musisz mi zaufać. Musimy... muszę Cię zamknąć gdzieś, gdzie Diwa nie przejmie kontroli. Ale nie zrobię Ci krzywdy. Nie mam PO CO robić Ci krzywdy. Wierzysz mi?
* Róża: Wierzę, bo jesteś moją ostatnią szansą.
* Michał: Zróbmy to.

## Streszczenie

W Arkologii Terkelis, Róża odnalazła Michała - osobę, która zniszczyła jej rodzinę. Róża próbuje znaleźć sposób jak zniszczyć Michała, eksperymentując ze swoimi mocami na ludziach. Tymczasem Michał, próbując naprawić przeszłość, inwestuje w arkologię i pomaga jej mieszkańcom - a zwłaszcza dzieciom dotkniętym magią. Róża poznaje swoją prawdziwą naturę - jest ofiarą Prządki, tak jak kiedyś był Michał. Decyduje się przerwać destrukcyjny cykl i współpracować z Michałem by pozbyć się cyklicznej Prządki raz na zawsze.

## Progresja

* Róża Kalatrix: docelowo będzie uwolniona od Prządki; Michał i Instytut jej pomogli. Zaprzyjaźni się z Lucyną Castelli.
* Lucyna Castelli: zaprzyjaźni się z Różą Kalatrix. One się rozumieją - obie były ofiarami magii. Przejmie bogactwo i potęgę Michała.

## Zasługi

* Róża Kalatrix: Michał wymordował jej rodzinę pod wpływem Prządki. Sama przemierza drogę od zemsty do poszukiwania uwolnienia od mrocznej mocy, która nią kieruje. Ma krew na rękach tak jak Michał; podjęła decyzję o współpracy z Michałem nawet jak ją to zabije by tylko pozbyć się cyklu Prządki.
* Jola Kalatrix: manifestacja energii magicznej Róży (i nieprawdziwa przyjaciółka Róży), która próbuje ją chronić, pomagać jej i dostarczać jej ludzi. Pozyskała naukowca dla Róży i ogólnie dbała o to, by Róży niczego ważnego nie zabrakło.
* Michał Castelli: pod neuroobrożą Kalatrix wymordował swoją rodzinę, pod Prządką wymordował Kalatrix. Gdy był już wolny, przeznaczył swój majątek i majątek Calatrix by stać się filantropem. Pomaga dzieciom dotkniętym przez magię i Arkologii Terkelis. Negocjując z Różą, zawarł z nią pokój i jej też pomoże.
* Wincenty Frak: neurosprzężony komandos i lojalny przyjaciel Michała; chroni go jak może przed Różą. Bezskutecznie, zginął w walce z Prządką. KIA.
* Lucyna Castelli: przybrana córka Michała, dziedziczka jego działań i wpływów. Kiedyś skrzywdzona przez magię, naprawiona w Instytucie. Lojalnie pomaga "ojcu".
* Adam Wokniaczek: naukowiec porwany przez Jolę, którego Róża wykorzystuje do zdobycia informacji o działaniach w Instytucie Castelli oraz o Michale i Lucynie. Nie został zjedzony przez Prządkę; stworzył kanał komunikacyjny Róży z Michałem.
* Janusz Umizarit: pracownik Instytutu, który staje się ofiarą Róży i jej mocy. Ma długi. KIA (pożarty przez Prządkę).
* Edward Kopoktris: pracownik Instytutu, którego wyrzuty sumienia i strach przed konsekwencjami stają się punktem zaczepienia dla Róży; próbował jej tatusiować. KIA (pożarty przez Prządkę)

## Frakcje

* Rodzina Castelli-Kalatrix: nowo powstała organizacja przestępcza samopomocy na Neikatis, z połączenia sił i przyszłych środków Róży i Michała.

## Fakty Lokalizacji

* Arkologia Terkelis: podupadająca, uboga arkologia, w którą jednak dużo zainwestował Michał Castelli i założył tam Instytut Alany Castelli dla dzieci skrzywdzonych przez magię.
* Arkologia Terkelis: obecność Róży (Krwawej Prządki) doprowadziła do śmierci kilkudziesięciu policjantów i innych osób w Arkologii. Poziom paniki rośnie.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Samrajia
                    1. Arkologia Terkelis: podupadająca, uboga arkologia, centrum Rodziny Castelli-Kalatrix.
                        1. Instytut Alany Castelli: ufortyfikowany Instytut gdzie prowadzone są badania i próby pomocy 'niechcianym i niebezpiecznym' dzieciom, zwłaszcza skrzywdzonym przez magię.

## Czas

* Chronologia: Zmiażdżenie Inwazji Noctis
* Opóźnienie: 6166
* Dni: 6
