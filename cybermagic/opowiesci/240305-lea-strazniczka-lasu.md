## Metadane

* title: "Lea, strażniczka lasu"
* threads: rekiny-a-akademia
* motives: bardzo-grozne-potwory, dobry-uczynek-sie-msci, krzywdze-by-sie-chronic, magowie-pomagaja-ludziom, teen-drama, pomocne-duchy-samszarow, spotlight-na-postac, the-warden, the-seeker, lost-my-way
* gm: żółw
* players: anadia, kić

## Kontynuacja
### Kampanijna

* [230325 - Ten nawiedzany i ta ukryta](230325-ten-nawiedzany-i-ta-ukryta)
* [220816 - Jak wsadzić Ulę Alanowi](220816-jak-wsadzic-ule-alanowi)

### Chronologiczna

* [230325 - Ten nawiedzany i ta ukryta](230325-ten-nawiedzany-i-ta-ukryta)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * INKUBUS SUKKUBUS "Nymphomania"
        * "He's the fingers, she's the matches | She's the fire, he'll be the ashes"
        * wiedzą, że mogą coś znaleźć. CHCĄ coś znaleźć. Ale nie powinni.
* Opowieść o (Theme and vision):
    * "Lea - strażniczka ludzi przed PRAWDZIWYM ZŁEM, która chroni ich większymi horrorami. Bo dookoła są złe rzeczy i lepiej ich nie przyzywać..."
        * Mariusz Kupieczka: AMZ, chce pomóc jako detektyw. Plus Triana.
        * Marek Samszar: Rekin, główny 'nekromanta' szukający śladów i tego co się tam stało. Plus Triana.
        * Triana Porzecznik: AMZ, dostarcza pieniędzy i zaawansowanego sprzętu
        * Malena Barandis: Rekin, bardzo zainteresowana historią
    * Lokalizacja: Podwiert, okolice (las)
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * bardzo-grozne-potwory: to, co śpi w Lasie Trzęsawnym. To, co próbuje szeptać do archeologów. Echa z czasów wojny. To, przed czym Lea próbuje chronić wszystkich.
    * dobry-uczynek-sie-msci: autentycznie magowie AMZ i Rekinów próbują pomóc lokalnemu 'archeologowi'. Niestety, nie powinno się tego robić, nie przez tak młodych magów.
    * krzywdze-by-sie-chronic: Lea, która zamiast ostrzec i powiedzieć że to ważne, to niebezpieczne itp. woli zrobić EfemeHorrora i pozwolić im ucierpieć samym. Lea woli by myśleli że jest zła na Rekinkę.
    * magowie-pomagaja-ludziom: AMZ + Rekiny by pomóc odkryć ciekawe tajemnice z przeszłości
    * teen-drama: potężny problem na linii Ernest - Lea. Lea ubiera się jak arystokratka eternijska za czym niesie się kod kulturowy i nie zamierza nic z tym robić. Robi co chce.
    * pomocne-duchy-samszarow: paradoksalnie, EfemeHorror Lei, mający na celu odepchnąć wszystko i wszystkich od Lasu Trzęsawnego, zwłaszcza w bardziej niebezpiecznych miejscach.
    * spotlight-na-postac: Lea Samszar, skupiamy się na niej, jej zainteresowaniach, motywacjach i działaniach
    * the-warden: Lea Samszar, uniemożliwiająca komukolwiek dostanie się do informacji i działań w okolicy Lasa Trzęsawnego. A już w ogóle - Markowi.
    * the-seeker: Bogdan Gwiazdocisz, szukający zakończenia opowieści wielu ludzi którzy ucierpieli i sekretów przeszłości z pomocą magów AMZ i Rekinów.
    * lost-my-way: Lea Samszar, która tak próbuje chronić przyjaciela i innych że sama ich rani i nie zauważa jak bardzo robi głupotę i błąd.
* O co gra MG?
    * Marek wyzywa Leę - ona jest NAJGORSZA. Jak może robić takie rzeczy?
        * Docelowo: wygnanie Lei.
        * LUB: Lea jest skłonna walczyć przeciw Markowi
    * Ogromne zniszczenia na obrzeżach Podwiertu - EfemeHorror się manifestuje... (Lea unleashuje Dark Construct)
    * LUB pomniejsze manifestacje, bo przebudzone zostało coś co powinno spać. (Lea pozwala im szukać czego chcą)
    * Lea ubiera się niesamowicie ekstrawagancko - z elementami kultury arystokratki eternijskiej, z arkinem. WIĘC - ścięcie z Ernestem, a zwłaszcza z Keirą. 
* Default Future
    * Coś kurczę obudzą spod Podwiertu lub zostaną ranni XD
    * Lea... Lea tak tego nie zostawi. Ona ich NIE zostawi.
* Dilemma: Marek, Ernest czy Lea?
* Crisis source: 
    * HEART: "Lea musi pozostać nieznana, więc ukrywa swoją naturę"
    * VISIBLE: "EfemeHorror."
        
### Co się stało i co wiemy

* Bogdan Gwiazdocisz, stary 'archeolog' próbujący rozwiązać stare zagadki dotyczące m.in. jego przytułka
    * Poszukiwacze historycznych zagadek, AMZ + Rekiny
        * Detektywi umarłych - zaginęły dzieciaki, ale też noktianie
        * co tam się znajduje, jak to wygląda
* Lea postawiła EfemeHorror blokujący
* Jednocześnie Lea miała kłopot z Ernestem...

### Co się stanie (what will happen)

* F0: TUTORIAL
    * Keira -> Marysia z uwagi na Leę.
* F1: Co tu się dzieje?
    * Arkadia wyśmiewa Marka Samszara. Co, DUCH go pokonał?
    * Marek mówi, że magowie AMZ oraz Rekiny są ranne.
        * Lea -> zostawcie to
    * tam COŚ jest i to potencjalnie jest niszczycielskie
    * WAIT współpraca?
        * Tylko Rekiny są w stanie coś z tym zrobić
* F2: To wszystko Lea O_O
    * seria ataków, oskarżeń itp
    * Lea wyłącza EfemeHorrora (atak wolnych bytów) LUB Lea nie wyłącza i opuszcza teren.
* Overall
    * chains
        * Lea i Marek w nienawiści: (simmer - oskarżenia (bitch vs bitch) - pojedynek - hatred)
        * Wygnanie Lei: (Ernest wściekły - Lea odmawia - Lea opuszcza Rekiny - Lea zostaje zaatakowana przez Keirę)
        * Poziom zniszczeń: (niski - materiałowe - ludzkie - apokalipsa)
        * Reputacja Marysi: (kiepsko kontroluje - wcale nie kontroluje - wtf przez nią bo Lea to Rekin)
    * stakes
        * poziom Marysi
    * opponent
        * Lea
        * To Co Śpi Pod Lasem
    * problem
        * human drama XD
        * niektórych rzeczy nie powinno się badać

## Sesja - analiza
### Fiszki

* Lea Samszar: czarodziejka, źródło problemów, 24
    * ENCAO:  +-+-0 |Arogancja i poczucie wyższości;;Kłótliwa;;Królowa lodu;;Overextending| VALS: Face, Universalism >> Hedonism| DRIVE: Deal with pests
    * styl: Adelicia (Rental Magica)
    * przewaga: ogromna siła magii - Horrory (efemerydy strachu i koszmarów na podstawie sacrifice)
* Malena Barandis: czarodziejka, winggirl Lei, 25
    * ENCAO:  000-+ |Wyrachowana;;Proponuje niekonwencjonalne, dziwne i często śmieszne rozwiązania| VALS: Security, Tradition >> Family| DRIVE: Impress the superior / senpai
    * styl: młoda Asuka ale z nastawieniem na swoje korzyści / Blaster TF G1
    * przewaga: czarodziejka soniczna, elegancka arystokratka, BARDZO dobre zmysły (ród Barandis)
    * bardzo zainteresowana historią i rzeczami historycznymi
* Triana Porzecznik: kochliwa i romantyczna konstruktorka która jest świetnie ubrana i lubi dotykać ludzi.
    * (ENCAO: ++0++ | Przyjazna i przytulna;; Roztrzepana;; Wiecznie śpiąca| VALS: Achievement, Family | DRIVE: Pomóc tym co pomocy potrzebują, PUPPY!!!)
    * seksowna majsterkowiczka lubiąca być na świeczniku ale jeszcze bardziej lubiąca przyjaciół o talencie do dekonstrukcji
    * "Kiedyś dorównam ojcu w konstrukcjach - na pewno mogę zalać problem energią. Tylko muszę wziąć coś by nie spać aż 5 godzin."


### Scena Zero - impl

brak

### Sesja Właściwa - impl

Marysia osiągnęła ostatnio duży sukces - Ernest zerwał z Amelią. Poruszenie. Keira dyskretnie ciągle coś mówi Ernestowi, ale on to spokojnie osłabia, osłabia itp. Ale Keira jest WŚCIEKŁA. Rzuciła _spojrzenie_ Marysi i poszła do Keirowych spraw. Ernest JEST lekko zirytowany, ale zignorował Keirę i poszedł zająć się Marysią.

* Ernest: Śniadanko?
* Marysia: Tak. Wszystko w porządku?
* Ernest: Tak...
* Marysia: Keira nie wyglądała na zadowoloną bo mnie widzi
* Ernest: Keira nie jest niezadowolona, ale uważa że powinnaś coś zrobić a nie robisz. Nie, nie to (uśmiechnął) - to wieczorem. Lub za 10 min, przed śniadankiem? (uśmiech)
* Marysia: To CO powinnam zrobić?
* Ernest: Jedna Rekinka się bardzo obraźliwie ubiera.
* Marysia: Ok..? Co to znaczy obraźliwie wg KEIRY? (WTF!)
* Ernest: Jest ubrana jak eternijska tienka bliska egzekucji. Pokazuje, że stanowi niebezpieczeństwo Esuriit. To... triggeruje Keirę. I ta tienka ma fałszywy arkin.
* Ernest: Co więcej, ona połączyła to z innymi symbolami stroju. Ogólnie... łączy te aspekty alarmowe z aspektami stroju symbolizującymi niewinność i pewną formę tabu.
* Ernest: To jest sygnał całkowitego braku szacunku. Zdaniem Keiry, sygnał, że nie kontrolujesz Rekinów. JEJ zdaniem nie powinnaś być ze mną bo nie kontrolujesz... albo powinienem Ci pomóc.
* Marysia: A Twoim zdaniem?
* Ernest: Ta tienka nie jest Twoim odbiciem. Tzn. Ty jesteś Tobą. Ona jest sobą. Ale nie obrazisz się jeśli tą tienkę spotka wizyta w rynsztoku itp?
* Marysia: Która to tienka? Porozmawiam z nią.
* Ernest: Lea Samszar.

Lea ogólnie traktuje ludzi jak powietrze. Nie robi krzywdy (choć są plotki), ale nie jest miła. Zabawne, że podobno po prośbie Karoliny kazała przynieść człowieka do swojego hotelu. Nie do swojego pokoju. Podobno robi na nim mroczne eksperymenty. Przynajmniej taka plotka. Ernest się ucieszył, że chcesz wpierw się tym zająć - on nie chce eskalować.

Zdaniem Marysi wpierw śniadanko. Zdaniem Ernesta, są inne rzeczy. WPIERW inne rzeczy, POTEM śniadanko, POTEM Lea. Zarówno Marysia jak i Ernest akceptują ten plan.

PIĘKNY DZIEŃ. Dobry humor itp. No i Lea w hotelu. Więc - Marysia kontaktuje się z Karoliną. Spotykają się w starej Rezydencji.

* Marysia: Karo? Znasz Leę?
* Marysia: Plotka głosi, że dostarczyłaś Lei młodego człowieka na mroczne eksperymenty.
* Karolina: Połowę sama rozpuszczam.
* Marysia: Masz z Leą kontakt do rzeczy?
* Karolina: Nie. Był człowiek który miał poważny problem a Lea była w pobliżu. 
* Marysia: Widziałaś jak się ubiera?
* Karolina: No, jak ona. A co, policja modowa czy coś?
* Marysia: (wyjaśnia problem)
* Karolina: Pogadaj, powiedz, że to nieodpowiednie i zobaczysz że się natychmiast z tego wycofa.
* Karolina: Tej lasce ktoś wsadził kij w dupę i niekoniecznie ktoś chce go wyjmować. Ale nie jest głupia. Nie wiem czego oczekujesz.
* Karolina: Faktycznie, pomogła mu. Jeszcze nie skończyła, ale pracuje nad tym. To był jakiś trudny problem.

Marysia wysyła prośbę o możliwość spotkania z Leą. Lea się zgodziła. Lea jest ubrana w mix eternijskich strojów, wygląda bardzo egzotycznie. To, na pięknej blondynce daje morderczy efekt. I jeszcze z kapelusikiem.

* Marysia: dzień dobry, Leo
* Lea: (lekki ukłon) tien Sowińska?
* Marysia: słyszałam dużo interesujących informacji na Twój temat, pomyślałam, że warto Cię poznać i porozmawiać. Też o modzie.
* Lea: tien Sowińska, jestem jedynie jedną z mniej interesujących osób w Twojej małej domenie. Żyję na uboczu i nikomu nie szkodzę.
    * (CZEMU ONA JEST REKINEM? LEA JEST TU PÓŁ ROKU LUB WIĘCEJ! I NIGDY SIĘ NIE NASUNĘŁA MARYSI)

Marysia: Widzę, że nosisz takie stroje. Wiesz, co one oznaczają. W związku z tym chcę dowiedzieć się dlaczego. I skłonić do zmiany zdania. To niezgodne z gościnnością, procedurami itp. Nie wypada, bo to więcej mówi o Tobie niż obraża ich.

Tr Z +3:

* Vz: Lea zaczęła wpierw mówić: "w bardzo nieelegancki sposób zerwał z tien Amelią Sowińską, bawiąc się sercem i uczuciami niewinnej tien Sowińskiej - nie pozwolimy na to w Aurum". To jest pretekst ale nie cały powód. Powód jest inny: "Ernest Namertel sprowadza eternijskie zwyczaje tutaj. Nie będzie rządził tym terenem. Nie będzie rządził Rekinami. Nie powie mi co ja mam robić. Nie dbam o niego."
* V: Eternia reprezentuje wszystko czego Lea nienawidzi. Eternia reprezentuje kontrolę do poziomu zniewolenia. W jej oczach Marysia wprowadzająca się do Ernesta reprezentuje Aurum rządzone przez Eternię. I to jej mały protest. Lea nie jest przyjaciółką Amelii. Lea nie ma przyjaciół. Ona chce być sama.
    * Marysia stawia sprawę: "To nie moja decyzja. To Rody. Rody chcą, by on się tu dobrze czuł. Chcesz stawić czoła Aurum i Sowińskim?" (+3Oy)
* V: Lea się ukłoniła. Widać zimne zaciśnięte zęby. 
    * Lea: "Tien Sowińska, zrobię, jak Rody sobie życzą. Ale ja nie pójdę do łóżka eternijczyka i nie będę miała z nim nic do czynienia."
    * Marysia: "Na szczęście nikt tu nie zmusza nikogo do tego"
    * Lea: "Rozumiem, tien Sowińska."

Lea się ukłoniła. "Gdy się oddalisz, przebiorę się, by rody nie były niezadowolone. I Ty."

* Marysia: Potrzebujesz pomocy z tym człowiekiem?
* Lea: Z tym człowiekiem? Nie. Absolutnie nie. Poradzę sobie.
* Marysia: Ale on żyje i wszystko w porządku?
* Lea: Gdyby nie to, że widok zwykłego człowieka jest niegodny tien Sowińskiej, mogłabym go zaprezentować. Nic mu się nie stało. Po prostu trudno mi odpiąć... to co się mu stało. Ale to zrobię.
* Lea: (po zastanowieniu) Czy mogę otrzymać dwie służki na okres 15 minut? Najlepiej dziewice?
* Marysia: W jakim celu? (XDXDXD)
* Lea: Muszę zapłacić, by móc osiągnąć sukces. To pomoże. Duch... horror... jest Dotknięty Esuriit. Muszę go zwabić przez Krew. Dwie dziewice i trochę krwi wystarczy.
* Marysia: Co się stanie z dziewicami?
* Lea: Zostaną dalej dziewicami (szybko, widząc minę Marysi). Będą żywe. Potrzebuję trochę krwi. Dziewic.
* Marysia: Samej krwi. Wystarczy Ci krew dziewic?
* Lea: Technicznie, tak, ale jeszcze ciepła.
* Marysia: Jak dużo?
* Lea: Jedna strzykawka na dziewicę. Taka strzykawka do badania krwi.
* Marysia: Czy może być z tej samej dziewicy czy z dwóch różnych?
* Lea: Czy tien Sowińska ma wątpliwości czy da się znaleźć AŻ DWIE dziewice w okolicy? (z niedowierzaniem)
* Lea: Utrudnieniem jest to, że muszą mieć ponad 16 lat. (z powagą)
* Marysia: Dlaczego powyżej 16 lat? Tak z ciekawości.
* Lea: Bo tak jest w krwawych rytuałach. Nie wiem czemu. Ludzie w to wierzą, więc tak jest.

Marysia łączy się z Hestią. Ma dostęp do danych medycznych.

* Marysia: Hestio, potrzebuję informacji o 2 dziewicach > 16 roku życia. Dostępnych tu blisko.
* H: ...zbliża się impreza?
* Marysia: (mówi co Lea chce zrobić i prosi o opinię)
* H: brzmi jak fatalny pomysł.
* Marysia: Masz alternatywy?
* H: Powiedz jej, że nie ma dziewic. 
* Marysia: NIE TEN PROBLEM! Jak rozwiązać problem horroru przyczepionego do człowieka
* H: Tien Samszar będzie wiedział więcej. Marek Samszar.

Marysia -> Lea

* Marysia: Da się inaczej? Mniej krwi, jesteś na tyle dobra że powinno się udać. A magia krwi potrafi być niebezpieczna. Wierzę w Twoje umiejętności. 
* Lea: Jeśli nie mamy dwóch dziewic, są inne metody. Dostałam prośbę od tien Terienak - a była bardzo miła - żebym doprowadziła człowieka do dobrej formy.
* Marysia: Czy tien Terienak Ci zagroziła?
* Lea: (zaskoczona) Nie, rodzeństwo Terienaków zachowało się... standardowo. Ale tien Terienak była nadzwyczaj pokorna gdy prosiła. Zależało jej na tym, by człowiek był w dobrej formie. Pod wpływem dobrego humoru, zaskoczona... podejściem tien Terienak się zgodziłam zanim sprawdziłam przypadek. A moje słowo będzie dotrzymane. I cóż - muszę pomóc człowiekowi. /mówi to, ale nie jest z tym źle
* Marysia: Jakie są alternatywy?
* Lea: Tien Sowińska, to mój problem. Mam inne metody i rytuały i zapewnię, by człowiekowi nie stała się krzywda.
* Marysia: A Tobie?
* Lea: (szczęka w dól) Że JA? JA miałabym zaryzykować dla... dla śmiecia? Dla ludzkiego śmiecia? Dla tien Terienak? Tien Sowińska, to tylko tien Terienak! (z oburzeniem)
* Marysia: Rozumiem, chciałam się upewnić że jesteś bardzo zaangażowana w sprawę, cieszę się że masz na względzie własne zdrowie.

.

* V: Lea coś ukrywa, coś kręci, coś nie mówi, coś plecie pajęczynę...
    * ona naprawdę chce pomóc temu człowiekowi. Ale się nie przyzna nawet na torturach. Nie wiesz czemu.
    * tu nie chodzi o Karo.
    * POTENCJALNIE CZUJE SIĘ WINNA! Może to JEJ wina?

Znowu Hestia.

* Marysia -> Hestia: Czemu przysłana?
* H: Lea sama chciała tu przyjść. Chciała być sama, z daleka od rodu i wszystkich.
* Marysia: Wiadomo dlaczego?
* H: Lea nie ufa swojemu rodowi.
* Marysia: A wiadomo dlaczego?
* H: Nic nie wiadomo. Lea ogólnie nie lubi magów. I WYJĄTKOWO nie cierpi duchów.
* Marysia: A wiemy dlaczego?
* H: Brak danych, tien Sowińska. Trzeba znaleźć... masz jakichś szpiegów wśród Samszarów?

Marysia -> Lea:

* Marysia: Jak myślisz, skąd taki Horror mógł się pojawić?
* Lea: (zaskoczona) Z lasu.
* Marysia: Co to znaczy?
* Lea: Las zawiera straszne rzeczy. Tam... zginęło dużo ludzi. Wojna. Stare technologie. Duchy. Rzeczy, które... nie chcą być martwe.
* Lea: Jeśli mam podejrzewać skąd taki Horror mógł się wziąć... to z lasu.

.

* X: Lea widzi, że Marysia za dużo wie. Czyli Lea widzi, że Marysia wie rzeczy których nie powinna wiedzieć.
* X: Lea wie, że musi się spieszyć. Musi maskować ślady. Musi zadziałać asap i się odseparować. Unikać Marysi. Podnieść 'act haughty'.
* Vr: TAK Marysia ma pewność. Lea FAKTYCZNIE stworzyła tego Horrora. I chce to naprawić. I nie chciała człowiekowi robić krzywdy. Horror miał mu POMÓC.
    * PLOTKA (prawdziwa): magowie dokuczali Lei, że ona lubi tego człowieka i się z nim przyjaźni. By udowodnić że to nieprawda, Lea poświęciła go w rytuale.
        * OD TEJ PORY zostawili Leę oraz człowieka w spokoju, ale niektórzy magowie dalej człowiekowi dokuczali.

.

* Marysia: W moim kręgu nie ma dziewic i nie znajduję dziewic pod ręką.
* Lea: Nie ma problemu, tien Sowińska. Nie potrzebuję nic innego tylko ochrony przed Eternią.
* Marysia: Myślę, że nikt z Eterni nie będzie Cię nagabywał - jakby się taka sytuacja zdarzyła to zgłoś to do mnie i dziękuję za uszanowanie zdania Rodów i zmianę stroju.
* Lea: Oczywiście, tien Sowińska. Nie jestem w stanie sprzeciwić się woli Rodów. Ale nie będę współpracować z Eternią.
* Marysia: Oczywiście. Bardzo miło się z Tobą rozmawiało. Dziękuję za ciepłe przyjęcie.
* Lea: (visible confusion)
* Marysia: Życzę powodzenia
* Lea: Dziękuję, tien Sowińska. Nie zawiodę słowa danego tien Terienak.
* Marysia: Dziękuję.

Marysia wiedząc i domyślając się połączyła fakty - nie chce iść do Karo. Za to udała się do Ernesta i powiedziała, że się udało. Ernest cały zadowolony. Przekaże Keirze. Podziękował jej ładnie. Marysia podziękowała za informacje - nie była świadoma - i przykro jej że zostali urażeni. Ernest przytulił Marysię. Powiedział, że Keira nic nie zrobi Lei.

ExZM+4+3Ob:

* Vm: Lei udało się naprawić Michała. Użyła swojej krwi.
    * Ernest to wyczuje. Keira też.

Karo, Marysia wyraźnie dobrze się bawi, zauważyłaś coś dziwnego. ZDEWASTOWANA "grupka bojowa". Ktoś wpierdolił Rekinom. Tak... nędznie wpełzli do Enklawy. Solidny wpierdol. Aż kilka Rekinów podeszło. Marek Samszar oraz Malena Barandis. Są ranni. 

* Marek: ja pierdolę...
* Arkadia: kto Ci to zrobił i czy przeżył?
* Marek: cholerny potwór...
* Malena: (pochlipuje)
* Arkadia: gdzie? (z szerokim uśmiechem)
* Karolina: jaki potwór?
* Marek: nie wiem, wyglądał na efemerydę, animował... jakieś gówno z wojny
* Malena: nie 'gówno z wojny' a ścigacz klasa Remor zintegrowany z trzema innymi i elementami organicznymi, o smaku ixionu.
* Marek: nie ixion a... jakaś forma ducha.
* Arkadia: wow... jeśli to nie jest super groźne, to dobrze że zerwaliśmy.
* Marek: czy AMZ przetrwali?
* Karolina: kto tam był? Ludzie, magowie...
* Marek: my, dwóch z AMZ, człowiek. Z AMZ... taki Mariusz, i Triana.
* Karolina: Triana?
* Marek: Do medycznego. Karo, możesz sprawdzić czy AMZ nic nie jest (wymownie ignoruje Arkadię)
* Karolina: Zostali? Wycofywali?
* Marek: Próbowałem... próbowaliśmy osłonić AMZ. Z tym robotem Triany. Robot nie miał szans. Nasze ścigacze były za wolne.
* Karolina: Arkadio? Idziesz ze mną czy?
* Arkadia: Jasne.

Obie na ścigaczu Karo.

* Arkadia: Jak ich znajdziemy, ja jestem dywersją, Ty ich ratujesz, pasuje?
* Karolina: Pasuje.
* Arkadia: (szeroki uśmiech Verlenki)

Karo znalazła ich szybko. Triana jest ranna, na oko złamana ręka. Mariusz jest nieprzytomny. Starszy człowiek leży i ciężko oddycha. Arkadia od razu w pozycji "jak to tu jest to walczę".

* Arkadia: Pierwsza pomoc, Karo?
* Karolina: Pomagamy czym się da.
* Arkadia: Osłaniaj mnie.

Arkadia udziela pierwszej pomocy. Jest to widok rzadki.

Tr Z +2:

* Vz: Arkadia ich ustabilizowała - nikomu nic poważnego się nie stanie.
* X: Arkadia jest Verlenką. Zadaje niepotrzebny, nadmierny ból, bo "walczysz z potworami"
* X: Arkadia jest przekonana, że to próbowało ich zabić. (coś go odgoniło?)

Karo nie ma miejsca dla Arkadii. Arkadia wróci sama. Karo udzieliła im pomocy w AMZ. (+2Vr)

* Vr: Karo przyleciała, szybko, pomogła, TAK JAK TO MOŻLIWE - dodała punkt szacunku Rekinom. "jak trzeba, to robią co powinni i to b. kompetentnie choć niezgrabnie bo Arkadia".
* X: nie dowiaduje się nic więcej

Powrót do Rekinów.

* Arkadia: Dobrze, tam jest coś groźnego. Widziałam rany, widziałam ślady.
* Karolina: Mechaniczne, żywe...
* Arkadia: Mechaniczne... nie było tego już. Zniknęło. To się pojawia... kiedyś. Trigger? To nie jest element ekosystemu, to coś nowego, śledzę ślady w lesie od zawsze.

Karo, Marysia -> w lekarskim skrzydle do Marka i Maleny. O dziwo, jest tam Lea.

* Karolina: (kiwa głową grzecznie Lei)
* Lea: (patrzy) Właśnie wychodziłam. Dobrze, że nic Ci nie jest, Maleno. Marek - (myśli chwilę) - nieważne. (wychodzi)
* Marek: (patrzy zdziwiony za Leą) -> do Maleny: WTF, o co chodziło?
* Malena: wiesz, nie osiągnąłeś jej wysokich standardów
* Marek: pierdolić jej standardy
* Malena: Lea jest... świetna. Jeśli chce byśmy byli lepsi, powinniśmy być.
* Marek: nie jest moją mamą.
* Malena: (wzruszyła ramionami) (syknęła) no nie byliśmy dość dobrzy. 
* Karolina: żeby co?
* Malena: ten duch nas pokonał. Przeżyli (z nadzieją)?
* Karolina: trochę poobijani, ale przeżyli, nic im nie będzie
* Karolina: czemu wybraliście się szukać ducha?
* Marek: to nie tak. Chcieliśmy pomóc... jest taki starszy człowiek, on kiedyś podczas wojny...
* Malena: po wojnie
* Marek: PO WOJNIE prowadził przytułek. I on szukał informacji, co się stało z niektórymi ludźmi, nazwiskami, oddziałami...
* Malena: i AMZ pomogli, ale nie mieli Samszara.
* Marek: no i ja chciałem im pomóc. Wiesz, po pierwsze, to AMZ. W sensie, nie robimi z nimi nic, nie? A moglibyśmy.
* Malena: historia. Jestem bardzo zainteresowana historią. Więc oczywiście że poszłam na ochotniczkę. Lea mi nie pozwoliła, ale się pomyliła.
* Karolina: nawet jej się zdarza (złośliwe)
* Marek: no właśnie... poradzilibyśmy sobie z każdym normalnym duchem, nie wiem co to było
* Karolina: może to nie był duch do końca?
* Marek: nie wiem, nie mam pojęcia. To było straszne.

(Karo widziała ślady. Widziała jak szli i pełzli. Potwór MÓGŁ ich zniszczyć. A nie zrobił tego. Dziwne).

Chronologia:

* Bogdan robi wyprawę by znaleźć ślady zaginionych i pomóc rodzinom dowiedzieć się co i jak.
* Triana i Mariusz poszli z ramienia AMZ
* Marek i Malena poszli z ramienia Rekinów. Marek chciał... poprawić status Rekinów. Plus - może pomóc to pomoże. Malena - dla historii.
* Poszli na pola i szukali duchów, robili operacje, dowiedzieli się czegoś nawet. Bogdan NAPRAWDĘ szuka historii.
* NAGLE znikąd pojawił się potwór. I był za mocny. Po prostu.
* Malena ma świetną pamięć i świetne umiejętności obserwacji - podała dokładniej jak to wyglądało. Dla Karo - potwierdzenie - to nie próbowało ich zabić. Ale krzywdziło. Mocno.

To była jakaś forma efemerydy lub ducha, animująca wraki wojskowe. Niebezpieczny byt. I to potencjalnie (dane Arkadii) pojawił się "znikąd". "Skonstruował się" gdy oni zaczęli czarować. Ale to nie był Paradoks - ktoś by zauważył.

Plan Karo jest taki:

* dać czas AMZ się ogarnąć
* następnego dnia ich odwiedzić i wypytać co jak gdzie - może coś do namierzenia dziadostwa
* do następnego dnia siłę łowczą.
    * Arkadia, Karo, Daniel, Barnaba Burgacz, Franek Bulterier, Marek Samszar

Karo zbiera to na następny dzień jako siłę ognia. Ta ekipa pójdzie za Karo. Arkadia umie się podporządkować, Bulterier będzie walczył, Burgacz OK. Daniel też.

Gdy tylko Marysia się dowiedziała, że jej TRIANĘ skrzywdzili, to wyszła z łóżka Ernesta. Chce coś zrobić.

* Ernest: Lea odprawiła jakiś potężny rytuał krwi. Niepokojące.
* Marysia: Lea miała wyleczyć człowieka który (nakreśla mu to). Ale (ubierając się) dostałam informację, że Rekiny dostały wpierdol. I Trianę mi skrzywdzili.
* Ernest: Trianę też? Ona... to jest bardzo sympatyczna dziewczyna.

Marysia mówi Ernestowi o tym, że Lea wyleczyła człowieka z Horrora którego sama zrobiła. I podejrzenia - czy czasem nie jest odpowiedzialna za to co jest w lesie (nawet jak nie specjalnie, ale high school drama). Więc w tym momencie - jeśli tak jest - to czy Ernest może pomóc ale niech nie pokaże nikomu że wie.

Ernest powiedział, że można to sprawdzić. Jeśli Keira dostanie trochę krwi Lei i zobaczy potwora, mają dowód.

Marysia NIE DARUJE rannej Triany. Idzie do Lei.

* Lea: tien Sowińska? Jestem zmęczona.
* Marysia: Dobrze, nie będę owijać w bawełnę. Jesteśmy sami?
* Lea: Zawsze jesteśmy sami.
* Marysia: Czy słyszałaś o (sytuacji w lesie)
* Lea: Wiem, rozmawiałam z Maleną i Markiem. Niefortunna sytuacja.
* Marysia: Jest podejrzenie, że ten potwór jest Twój. Chcę Cię chronić. Czy możesz udowodnić że nie Ty jesteś źródłem potwora?
* Lea: A tien Sowińska może udowodnić, że nie jest źródłem?
* Marysia: Nie mam takiej magii
* Lea: Ja mam takie umiejętności, ale nie mam jak tego udowodnić. Nie udowodnię, że nie jestem wielbłądem. /frustracja
* Lea: Kto za tym stoi? Eternia?
* Marysia: Za czym?
* Lea: Za tym podejrzeniem?
* Marysia: Nie, nie Eternia, czemu Eternia miałaby za tym stać?
* Lea: Bo... nieważne. Dobrze, skąd to podejrzenie? Jakie są przesłanki?
* Marysia: Wiem, że stworzyłaś pierwszego efemerycznego horrora by się znęcać nad ludźmi.
* Lea: (strach) (uśmiech) Masz rację, tien Sowińska. I?
* Marysia: To prowadzi do prostego wniosku, że mogłaś zrobić tego drugiego potwora. A ten skrzywdził bliskie mi osoby. (spokojny, zimny głos)
* Lea: Tien Sowińska nie ma osób sobie bliskich. Nie wypada. (spokojny głos, bez cienia uczucia)

Marysia chce, by Lea się albo przyznała albo dała dowód, że jest niewinna. I naciska. Pokazuje jej, że jej ZALEŻY na Trianie. Jej przyjaciółka. Jedna z pierwszych osób z którymi się zaprzyjaźniła i które poznała.

Tr Z +3:

* X: wszyscy którzy się dowiedzą będą wściekli na Leę.
* Vr:
    * Lea: Tak, stworzyłam efemeryczny horror. (usiadła) Ja stworzyłam to coś w lesie. Musisz przyznać, tien Sowińska, że jest to piękne narzędzie zniszczenia. (smutna)
    * Lea: Zapewniłam, by efemeryczny horror nikogo nie zabił. Ale oczywiście muszą być ślady poważnych uszkodzeń cielesnych.
    * Marysia: Dlaczego go stworzyłaś?
    * Lea: Las jest bardzo niebezpiecznym miejscem. Magowie, którzy próbują... odkryć co tam się znajduje mogą przebudzić rzeczy których nie powinni budzić. To ich zrani. Nie chciałam, by tien Sowińska miała problem (kłamie)
    * Marysia: Mogłaś powiedzieć a nie tworzyć strażnika. Przyjść i powiedzieć. Plan jak oczyścić ten teren. Skoro wiesz że tam jest coś strasznego.
    * Lea: Powiedziałam Malenie. Powiedziałam Markowi.
    * Marysia: Przed tym jak dostali wpierdol?
    * Lea: Przedtem. Jeśli nie byli w stanie pokonać MOJEGO konstruktu jakie szanse mają z tym co tam jest?
    * Marysia: Musimy popracować nad Twoimi umiejętnościami komunikacyjnymi...
    * Marysia: Oni pójdą zniszczyć ten konstrukt.
    * Lea: Jak mniemam, mam potem zbudować kolejny?
    * Marysia: Nie. Masz powiedzieć, co tam się czai byśmy mogli oczyścić ten teren. Możesz nam pomóc oczyścić ten teren. Może nawet nauczysz się współpracy w grupie.
    * Lea: Nie wiem co tam się czai. 
    * Marysia: Skąd wiesz że się tam czai? Twoje założenie? Oparte o coś więcej?
    * Lea: Jestem wyczulona na niektóre... rzeczy. To tam jest. Ale nie wiem co to jest dokładnie.
    * Marysia: Skąd wiesz, że to jest niepokonalne?
    * Lea: Nie wiem. Nie interesuje mnie to. Poszli tam, było to niebezpieczne, odmówili gdy powiedziałam że mają nie iść więc postawiłam im na drodze Strażnika (kłamie)
    * Marysia: Chcesz się bawić w stawianie strażników? Czy zneutralizujesz strażnika i zmontujemy ekipę, która pomoże...
    * Lea: Tien Sowińska... jeśli wiesz o tym, to jest to Twoja decyzja. Mnie w to nie mieszaj.
    * Marysia: Nie stawiaj następnego
    * Lea: Jak sobie życzysz, tien Sowińska. Ale wyniki następnych ruchów archeologicznych zostawiam w Twoich rękach.

Marysia -> Karo:

* Marysia: Czy możesz wziąć Keirę i rozwalić tego potwora?
* Karolina: Po co mi Keira? To człowiek.
* Marysia: Dobra w walce. Pomoże. I chce pomóc.
* Karolina: Nie. Nie bierzemy Keiry. To nie jest grupa, do której pasuje w jakikolwiek sposób.
* Marysia: Ok. A ja Wam się mogę przydać? /frustracja i zbulwersowana że Triana ranna
* Karolina: Siedź na dupie z tyłu.

Czas na potwora...

## Streszczenie

Marysia zatrzymała Leę przed naruszaniem kulturowego tabu eternijskiego (stroju) i przy okazji dowiedziała się, że Lea próbuje pomóc innym, ale nikt nie może o tym wiedzieć. Tymczasem jej EfemeHorror poranił grupę Rekinów i AMZ współpracujących ze sobą w celu pomocy staremu 'archeologowi' szukającemu prawdy o ofiarach wojny.  

## Progresja

* .

## Zasługi

* Marysia Sowińska: po wyjściu z łóżka Ernesta pomogła rozwiązać problem Lei, która łamie eternijskie tabu. Doszła do tego, że Lea próbuje pomóc Rekinom i ludziom, ale robi to tak by nie pokazać że pomaga. Doszła do tego, że Lea stoi za EfemeHorrorem z lasu i kazała Lei przestać to cholerstwo wzmacniać.
* Karolina Terienak: pomaga rannym w lesie i szybko transportuje wszystkich, po czym montuje oddział do walki z EfemeHorrorem który pociął AMZ i Rekiny.
* Lea Samszar: wyleczyła Michała rytuałem z jednego EfemeHorrora i zaraz stworzyła nowego EfemeHorrora mającego odpędzać magów badających las (by czegoś nie obudzić). Nie chce współpracować ani się komunikować z innymi. Nie pokaże po sobie, że jej zależy. Wykonuje polecenia Marysi ("Rodów"), choć uważa Marysię za pionka Rodów. Łamie tabu Eterni, bo nie zgadza sie na Eternię i ich wpływy tutaj. Ale poddała się woli Marysi.
* Marek Samszar: próbował pomóc Gwiazdociszowi w sprawie badań z czasów wojny. Ucierpiała przez EfemeHorror. Wyśmiała go Arkadia, że nie dał rady.
* Triana Porzecznik: próbowała pomóc Gwiazdociszowi w sprawie badań z czasów wojny. Ucierpiała przez EfemeHorror.
* Malena Barandis: niesamowicie świetna percepcja, winggirl Lei; poszła z Markiem i AMZ pomóc Gwiazdociszowi w sprawie badań z czasów wojny. Ucierpiała przez EfemeHorror.
* Keira Amarco d'Namertel: bardzo niezadowolona z uwagi na to, że Lea łamie zasady strojów eternijskich (tabu). Marysia ją uspokaja, zajmując się tą sprawą.
* Ernest Namertel: z przyjemnością spędza czas z Marysią w łóżku; wyjaśnił Marysi problem tabu strojów eternijskich które łamie Lea. Potem powiedział jej, że Lea używa rytuału Krwi...
* Bogdan Gwiazdocisz: stary już człowiek, który próbuje znaleźć _closure_ jak chodzi o wszystkie osoby które stracił podczas wojny i świeżo po wojnie. Operacja archeologiczna w Lesie Trzęsawnym, okolice Podwiertu i próba integracji Rekinów i AMZ. Świetnie opowiada. Skończył ranny przez EfemeHorror.
* Mariusz Kupieczka: mag AMZ, który próbował pomóc Trianie i Gwiazdociszowi w archeologii z czasów wojny. Ucieszył się mogąc pracować z Rekinami. Ucierpiał walcząc z Horrorem.
* Michał Klabacz: nieszczęśnik, którego uratowała Lea od jej własnego efemerycznego horrora. Nie ucierpiał trwale. Może już opuścić Dzielnicę Rekinów.
* Arkadia Verlen: zaskoczona siłą EfemeHorrora; zdecydowanie pomoże Karolinie z nim walczyć. Wyśmiała Marka, że pokonał go Horror. Gdy walka z potworem - pierwsza na froncie.

## Frakcje

* .

## Fakty Lokalizacji

* Podwiert: w Lesie Trzęsawnym oprócz starego echa z czasów wojny znajdują się tam nadal Rzeczy Które Nie Chcą Spać, jeśli wierzyć Lei Samszar. Fakt, wymaga to czyszczenia i ochrony.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Dzielnica Luksusu Rekinów
                                1. Las Trzęsawny

## Czas

* Opóźnienie: 4
* Dni: 4
