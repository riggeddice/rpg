## Metadane

* title: "Powrót Minerwy z terrorforma"
* threads: autonomia-neotik, reintegracja-minerwy-metalii
* motives: rywal-z-przeszlosci, dark-technology, golem-unleashed, przeciwnik-terrorform, naprawa-bledow-przeszlosci, ixionska-synteza, milosc-ponad-zycie
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [181003 - Lilia na trzęsawisku](181003-lilia-na-trzesawisku)

### Chronologiczna

* [181003 - Lilia na trzęsawisku](181003-lilia-na-trzesawisku)

## Projektowanie sesji

### Struktura sesji: Niestabilny Trójkąt

* **Strona, potrzeba, kogo bije, czym?**
    * Erwin Galilien
    * Konieczność ochrony Nutki za wszelką cenę
    * Pokonuje Wiktora Sataraila
    * Gorącą furią Nutki i umiejętnościami ucieczki
* **Strona, potrzeba, kogo bije, czym?**
    * Adam Szarjan
    * Poczucie zagrożenia dla okolicy przez Erwina Galiliena (Evangelion mode)
    * Pokonuje Erwina Galiliena
    * Dane z Laboratoriów Orbitera Pierwszego, czysta siła ognia
* **Strona, potrzeba, kogo bije, czym?**
    * Wiktor Satarail i Trzęsawisko Zjawosztup
    * Chęć ochrony swojego domu i wszelkiego życia, w tym kontekście Nutki
    * Pokonuje Adama Szarjana
    * Organiczne ataki, na które Adam jest zdecydowanie słaby + to jego teren
* **Czemu gracze muszą w to wejść?**
    * Erwin ucieknie z Nutką z tego terenu jeśli nie uda się niczego z tym zrobić, nawet za cenę rozpalenia Trzęsawiska
* **Trigger?**
    * Przybywa Adam do Pięknotki

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) brak
* Ż: (element niepasujący) Lilia jest zdecydowanie przeciwna zarówno Adamowi jak i Nutce - nie staje po stronie Adama
* Ż: (przeszłość, kontekst) Mroczny czyn Galiliena w przeszłości, mroczny czyn Szarjanów w przeszłości

### Dark Future

1. Erwin ucieknie przez Mglistą Ścieżkę na Trzęsawisku i zniknie
2. Wiktor Satarail się uruchomi na samym Trzęsawisku

Pogoda, otoczenie:

* Fatalna pogoda, obniżona widoczność
* Sam Zjawosztup to Trudny Teren, na którym są endemiczne Echa i problemy.

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

## Misja właściwa

**SCENA:**: Adam dowiaduje się gdzie jest Erwin; Lilia zostaje zmuszona do współpracy z Adamem. (18:40)

Salon kosmetyczny Pięknotki. Ładny dzień, Lilia jako klientka. Do salonu wszedł mag - Adam Szarjan. Lilia się skrzywiła - ona zna Szarjana i go wyraźnie nie lubi. Adam usiadł zrobić sobie zabieg kosmetyczny.

Adam powiedział Pięknotce, że szuka Galiliena. Chce porozmawiać z doskonałej klasy mistrzem servarów - on jest z Orbitera, Galilien z Pustogoru. Brzmi legalnie, ale Pięknotka chce dowiedzieć się o co chodzi. Konflikt Typowy (9-3-3 = S). Adam może i jest świetnym żołnierzem, ale niestety, powie co myśli jak Pięknotka spyta. Martwi się Nutką. Uważa, że jest niebezpieczna. Przybył, by przekonać Galiliena, że Nutka musi być rozmontowana na kawałki i zniszczona.

Pięknotki to nie ucieszyło. Podsłuchująca Lilia jest wyraźnie skonfliktowana - z jednej strony chciałaby się pozbyć Nutki. Z drugiej, wyjątkowo nie lubi Szarjana - i teraz dopiero Pięknotka widzi jak BARDZO go nie lubi.

W Adamie nie złej woli ani złej intencji - jest incognito (jak chodzi o plany). Szuka sposobu, by zapobiec tego co uważa za tragedię która się na pewno wydarzy. Usłyszał o Nutce przez opowieści.

Pięknotka zaprosiła Adama na obiad. Zgodził się z przyjemnością - wyszli do "Pustułki" (niezłej klasy jadłodajni Pustogorskiej). Pięknotka skorzystała z okazji by wydobyć od niego wszystko co się da na temat tej sprawy. Wszystko. No i dowiedzieć się o co chodzi z Lilią (10-3-3 = SS). [Skonfliktowanie = przypomni sobie Lilię i problemy z nią].

* Adam martwi się, że Nutka wpadnie w berserk pod wpływem konkretnego typu Skażenia
* Adam uważa, że Nutka ma cechy mentalne Nojrepów
* Galilien użył czarnej magii by móc stworzyć Nutkę. Adam i Erwin się znają.
* Lilia - próbowała wpierw przez łóżko (kilka) a potem politycznie dostać się do Koszmaru (oddziału Adama)
* Przez Lilię bardzo kompetentna terminuska zrezygnowała z Koszmaru.
* Kiedyś, Adam i Erwin byli kolegami - zdrowa rywalizacja. Ich drogi się rozeszły, bo Erwin nie zgadzał się na to, by Szarjanowie używali czarnej magii i eksperymentów. A teraz sam zbudował taki Power Suit jak Nutka...

Adam ma kryształ zaprojektowany by obudzić nojrepową naturę Nutki, ale w sposób kontrolowany. Pięknotka zjadła smaczny obiad w miłym towarzystwie. Powiedziała Adamowi, że zaprosi Erwina na kolację. Adam zgodził się z przyjemnością.

Krótka rozmowa Pięknotki z Lilią. Lilia ma fatalny humor - co tu robi Adam Szarjan. Pociągnięta za język - powiedziała. Adam ma gdzieś na terenach Skażonych fabrykę Nojrepów. Badania na ludziach, servarach i viciniusach... oraz nojrepach, by podnosić poziom technologiczny servarów Szarjana.

Oki, to sprawia, że Pięknotka skontaktowała się z Ateną Sowińską. To ciekawe. Atena nie chciała mówić; wyraźnie się wymigiwała. Pięknotka jednak przekonała ją do powiedzenia tego, co wie (Konflikt Typowy). Atena opowiedziała, że:

* Siły polityczne powiązane z Orbiterem Pierwszym powstrzymywały Epirjon przed patrzeniem na wschód
* Siły stojące za usunięciem Ateny też są powiązane z Orbiterem Pierwszym
* Na wschodzie MOGĄ znajdować się tego typu laboratoria i połączenia o których nikt nie wie
* Atena nie wierzy, żeby Adam Szarjan był w to zamieszany

* Ż: 3
* K: 1

**SCENA:**: Galilien w Miasteczku opuścił Szczelinę; ścina się z Adamem i ewakuuje na Trzęsawisko. Lilia leci za nim. (19:45)

O czwartej rano powrócił Erwin Galilien. Jak kiedyś Pięknotka prosiła, gdy tylko wraca ze Szczeliny, wysyła jej sygnał "wróciłem, jestem bezpieczny". Galilien żyje samotnie, więc Pięknotka prosiła, by jej choćby się meldował. Tym razem Pięknotka się obudziła od rana - dała absolutnie priorytetowy sygnał swojemu hipernetowi.

Pięknotka zaprosiła Erwina na 8 rano na śniadanie do siebie. Erwin przyjął zaproszenie.

Erwin przybył i dał Pięknotce w prezencie _rosa crystallica_ - różę złożoną z kryształów, żywy, krystaliczny byt. Nie wie czy to jest niebezpieczne, ale nie powinno być niebezpieczne. Pięknotka wzięła Erwina znowu do Pustułki na śniadanie. Adam ma powiedziane "hang around there", np. co 30 minut. Tak, żeby się sam pojawił. By to wzmocnić, Pięknotka upewniła się rozmową, by Erwin NIGDY nie uwierzył, że Pięknotka cokolwiek wiedziała o Adamie (12-3-0). Sukces. Erwin NIGDY w to nie uwierzy.

Przybył Adam. Erwin się zdziwił, ale lekko ucieszył. Do momentu, aż po wymianie starych opowieści Adam powiedział po co jest. Erwin w życiu nie zgodzi się pokazać Adamowi Nutki - i Pięknotka ma wrażenie, że Adam ma rację w tej kwestii - Erwin użył zakazanych magitechowych technik. Takich groźnych. Adam oskarżył Erwina o nieroztropność, która może zagrozić wielu ludziom. Erwin powiedział Adamowi, że sytuacja jest pod kontrolą. Nikt nie ucierpiał i nikt nie ucierpi.

* "Jak śmiałeś użyć technik Karola, których nawet on nie dał rady zastosować?!" - Adam
* "A skąd pomysł, że to Karol wynalazł te techniki?" - Erwin - "To była Minerwa"
* "Minerwa odeszła z Orbitera z Tobą. Wygodne." - Adam
* "QED, Adamie. To jest bezpieczne. A tak w ogóle, nie zrobiłem tego." - Erwin

Sytuacja zaczyna się zagęszczać. Pięknotka spytała Adama, kiedy uzna, że Nutka jest bezpieczna. Adam powiedział, że gdy Nutka przejdzie test psychometryczny i gdy okaże się odporna na kryształ kolonialny. On nie chce niszczyć Nutki - on nie chce dopuścić do drugiego "Projektu Kotlin". Erwin natomiast nie chce, by Adam poznał jego metody. Ale co ważniejsze, czy ta energia z kryształu jest energią taką jak Szczeliny? On nie jest terminusem międzyplanetarnym - on jest tylko nurkiem Szczeliny.

Pięknotka wpadła na pomysł - niech Adam pójdzie z Erwinem do Szczeliny i pobiorą sygnatury energii. Adam sprostował - Nutka musi być odporna na rzeczy jakie mogą się pojawić. Pięknotka zauważyła, że nojrepy rzadko podchodzą do Pustogoru. Adam zauważył, że wtedy Erwin jest podatny na KAŻDEGO maga mu wrogiego. Erwin MUSI być odporny na różne formy energii, inaczej Nutka będzie jego śmiercią. Adam poprosił Erwina, by ściągnął Minerwę - jeśli ona się tak na tym zna, niech pomoże to wyjaśnić. Erwin powiedział, że to niemożliwe. Minerwa została w Szczelinie.

[Kić: mam wizję opowieści w której wraca Minerwa. Przez test zrobiony przez Adama i Atenę.]

Argumentacja Pięknotki jest prosta:

* Adam nie chce iść do _authorities_ ale to zrobi. Plus, nie chce tu być ale będzie - trzeba mu udowodnić, że Nutka jest bezpieczna.
* Erwin uważa, że Nutka jest bezpieczna, ale jeśli zaufa niezależnemu kataliście (Atenie), sam zdobędzie spokój ducha - bo sam nie wie czy jest bezpieczna czy nie.
* Adam musi przysiąc, że nie skupia się na szpiegostwie przemysłowym.
* Pięknotka zapewnia dostęp do bezpiecznego _testing grounds_ w Pustogorze. Adam oddaje kryształ do analizy Atenie. Tam jest robiony test na hali w obecności Ateny. 

Test Trudny typu "tak, ale" i +2 żetony za pomysł i zaufanie (12-3-4 -> SS), włączając Zasób (Atena, hala). Wynik skonfliktowania: walka z Nutką/Minerwą.

* Ż: 6
* K: 2

**SCENA:**: Cisza przed burzą. Hala oraz Atena. Wstępne przygotowania. (21:40)

Spotkanie Pięknotki i Ateny. Osobiste spotkanie, w salonie Pięknotki. Atena przybyła, z charakterystycznym dla siebie czarem i elegancją. I miną wskazującą na lekkie zniecierpliwienie. Pięknotka zreferowała Atenie problem i oczy Atenie się zapaliły. Jest to coś, co ją zainteresowało. Atena dostanie za to przysługę u Pięknotki, podobnej klasy. Kapitan stacji Epirjon oczywiście się zgodziła. ALE! Musi móc też przeanalizować Nutkę. Bez tego będzie to trudniejsze.

No i Erwin, znowu. Powiedział, że Minerwa to zrobiła. Ona się poświęciła, by oni mogli uciec. Nie było innej opcji. On nawet nie wiedział co ona planuje zrobić. Powiedział, że ta energia zniszczy Nutkę. Minerwa nigdy nie rozwiązała tego problemu... ale na to odpowiedziała Pięknotka - Atena Sowińska. Ona jest odpowiedzią. Atena może pomóc coś zrobić z tą energią, ale to wymaga kalibracji. Pięknotka nie wie co się stanie, ale Atena tam będzie. (Typowy, 9-3-3 -> S). Erwin się zgodził. Niech Atena spojrzy.

Atena powiedziała Pięknotce, że potrzebuje trzy dni. Zażądała Galiliena - on musi jej pomóc. No to Pięknotka go ściągnęła i poszła załatwić halę testową.

NAWET przy zasobach Pięknotki jest to konflikt Trudny (10-3-6 -> S). Sukces. Pięknotce udało się ustalić Kompleks Testowy na czas działań. To jest w sumie dobry dzień dla Pięknotki-biurokratki XD.

* Finalny Tor Zniszczenia: 0

Adam jeszcze spotkał się z Pięknotką. Ostrzegł ją, że najpewniej Nutka przejdzie w tryb transorganiczny i będzie musiała być zniszczona. On osobiście ubolewa nad tym, że tak się to musi skończyć. Ma nadzieję, że się myli. Zdaniem Adama, cztery lata temu on się mylił a Erwin miał rację. Czas, by spłacić rachunki.

* Ż: 7
* K: 2

**SCENA:**: Wojna z Minerwą (21:59)

Hala Kompleksu Testowego. Działa bojowe w gotowości. Adam jest w swoim servarze, toteż Pięknotka. Erwin jest z Nutką. Nutka lekko trwożliwie pytała czemu tu jest i co się dzieje; Erwin ją uspokaja. Gdy Atena podała jej kryształ, Nutka krzyknęła, po czym przekształciła się w Terrorforma. Terrorform zaatakował Adama, który elegancko uniknął ataku i użył broni drzewcowej by nabić terrorforma na pal.

_Eskalacja_:

* Terrorform ma formę mutowalnej, ewolucyjnej istoty transorganicznej
* W środku zamknięte są Minerwa i Nutka, mentalnie
* Zespół musi poradzić sobie z atakami terrorforma i zniszczyć powłokę - po czym dotrzeć do środka

Atak Terrorforma: 6-3-4. Bo mają dobry teren. Ale Erwin jest lepszym projektantem niż był przy projekcie Kotlin. (Porażka). Terrorform uderzył i rozwalił broń drzewcową; Adam musiał rozpaczliwie ratować się waląc full power jetpack - walnął w sufit i uszkodził servar.

Pięknotka walnęła potężnym eliksirem. Coś, co powstrzyma szybkie transformacje i ewolucję terrorforma. Niech terrorform będzie słabszy; niech nie będzie w stanie szybko się adaptować. Coś, co działało regularnie na Trzęsawisku Zjawosztup. Seria eliksirów działających losowo. Magia oraz Zasób. Trudny (13-3-4). Porażka (reroll: Sukces). Udało się, ale dużym kosztem - Galilien zasłonił Pięknotkę. Terrorform uderzył z sześciu stron i ciężko ranił Erwina - ale NIE DOBIŁ. Nie skupia się na nim.

(Ż: -5: ciężko ranny Galilien)

Atak Terrorforma: 6-3-3. Osłabiony przez eliksir, uderzył ponownie. Adam ZNOWU się nie spodziewał - wystrzelił by całym ciałem odepchnąć terrorforma i polecieć na koniec areny. Udało mu się, ale jednocześnie jego servar ma przebicia. Do Adama dotarło - MINERWA jest podstawą terrorforma. Ona zna jego ataki i triki. Servar jest praktycznie niesprawny; Adam się katapultował i servar eksplodował na terrorformie. Adam wpadł w mały awian. Terrorform wygląda na lekko oszołomionego.

Atena w połączeniu z Pięknotką utrzymały Erwina przy życiu oraz włączyły mu pełne stymulanty. On potem będzie w jeszcze gorszym stanie, ale teraz BĘDZIE działał. Atena wydała Erwinowi rozkaz "żołnierzu, pokonaj to". I chwilę potem, do Pięknotki "Zintegruj ich mentalnie. Otwórz kanał. Zanim as awianów zginie.". Sama ustawiła filtr. Test Trudny (9-3-5 -> Skonfliktowany Efekt Skażenia).

(Ż: -5: shockwave na Atenę przez wykorzystanie)

Efekt Skażenia miał zdewastować Pięknotkę; terrorform obrócił efekt przeciw niej. Ale Atena przeforsowała i wzmocniła sygnał przekazując "ludzki" strach i ból Pięknotki terroformowi, przypominając jak to jest być człowiekiem. Jednocześnie całą falę uderzeniową przyjęła na siebie. Atena, świecąc Skażeniem jak radioaktywny chomik padła na ziemię, wystarcza jej jednak woli by trzymać kontakt.

Galilien jest przytomny i komunikuje się z terrorformem. Atena jest praktycznie nieprzytomna. Adam stracił servar i już jest w awianie. Terroform atakuje, mimo ogłuszenia i połączenia mentalnego (7-3-2). W końcu, przez konfuzję i uszkodzenia, nie dał rady. Adam ściągnął uwagę terrorforma, ale w awianie trudniej go trafić. A awian i działka bezlitośnie prują ogniem.

Pięknotka strzela elementalnym pociskiem, eliksirem. Dodatkowo osłabić. Zadać obrażenia. Oszołomić. Pięknotka chce odsłonić rdzeń. Test Heroiczny:

* Pięknotka ma pełną kartę (9)
* Pięknotka ma: oszołomionego przeciwnika (1), odwrócona uwaga (1), eliksiry oraz działa (1), przycelowanie (1)
* Pięknotka ma Magię i Zasoby (6)

Test Heroiczny (15-5-15). Reroll (15-5-14), Sukces. Strzał Pięknotki odsłonił Rdzeń terrorforma, w który błyskawicznie uderzył Adam awianem. Terrorform został zneutralizowany. Teraz uderzył w niego mentalnie Erwin, przywołując Nutkę... ale odnalazł Minerwę.

Terrorform zrekonstytuował w nową transorganiczną formę. Nadal servar. Ale inny.

Minerwa powróciła.

Atena sama wstała mimo swojego stanu. Jest Skażona do poziomu braku możliwości rozróżnienia rzeczywistości i fikcji. Kazała wszystkim się ewakuować, póki jeszcze rdzeń Epirjona działa. Pięknotka próbowała ją z tego otrząsnąć. I uderzyła Atenę wielokrotnie po twarzy - Trudny (10-3-5 = SS). Udało się, ale Atena DOSŁOWNIE eksplodowała. Pięknotka jest lekko ranna, w Atenie nie zostało wiele energii...

Wszyscy trafią do Szpitala Terminusów. A Adam zwinie Nutkę, by zrobić na niej badania - co do cholery się dzieje. I by nikt nie wiedział co tu się stało.

* Ż: 1
* K: 7

(koniec: 23:16)

## Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |    0    |    11       |
| Kić           |    1    |     7       |

Czyli:

* (Ż): (1) Lilia wini Atenę i Adama za stan Erwina
* (K): (2) Minerwa ma stabilną strukturę, zarówno fizyczną jak i mentalną
* (K): (1) Adam i Erwin odbudowali kontakty, dookoła Minerwy
* (K): (1) Adam zażąda, by Orbiter Pierwszy pomógł Atenie w powrocie na Epirjon
* (K): (2) Jest delikatne połączenie mentalne między Erwinem a Minerwą

## Streszczenie

Adam Szarjan, dawny kolega Erwina pojawił się by uratować go przed problemami z Nutką. Okazało się, że Nutka jest śmiertelnym zagrożeniem pod wpływem energii Nojrepów. Wraz z Ateną i Pięknotką udało im się zneutralizować problem, ale w miejsce Nutki pojawiła się Minerwa - prawdziwa osoba, na bazie której powstała Nutka. Atena i Erwin w bardzo ciężkim stanie trafili do szpitala.

## Progresja

* Pięknotka Diakon: reputacja wyjątkowo niszczycielskiej. Wypożyczyła salę i 2 osoby skończyły krytycznie w szpitalu a as awianów stracił servara.
* Erwin Galilien: jego servar (Nutka) jest zastąpiony przez Minerwę.
* Erwin Galilien: ma połączenie mentalne pomiędzy sobą a Minerwą.
* Erwin Galilien: bardzo ciężko ranny; co najmniej tydzień w szpitalu + rehabilitacja.
* Atena Sowińska: ma dostęp do Epirjona przez działania Adama Szarjana.
* Atena Sowińska: bardzo ciężko ranna; co najmniej tydzień w szpitalu + rehabilitacja.
* Lilia Ursus: obwinia Adama Szarjana oraz Atenę Sowińską o stan Erwina Galiliena. Plus, dalej uważa, że Adam jest złym magiem z dobrym PRem.
* Lilia Ursus: ma w przeszłości wywalenie dobrej terminuski z Koszmaru oraz próbę wejścia do Koszmaru przez łóżko.

## Zasługi

* Pięknotka Diakon: przekonywała, knuła, robiła administrację, wciągnęła Atenę... i na końcu jako jedyna jest tylko lekko ranna.
* Lilia Ursus: okazało się, że ma przeszłość powiązaną z Adamem. Nie lubi Adama, nie ma zamiaru z nim współpracować nawet jak ona by na tym zyskała.
* Erwin Galilien: sam się ograniczał, by Nutce nie stała się krzywda. Bardzo ciężko ranny osłaniając Pięknotkę w walce z terrorformem. Stracił Nutkę, odzyskał Minerwę.
* Atena Sowińska: stanęła na wysokości zadania by przekształcić Nutkę w Minerwę. Dużo zaryzykowała i wzięła ogień na siebie. W przeszłości ma straszliwe demony.
* Adam Szarjan: o dziwo, przybył by pomóc Erwinowi bez ukrytej agendy. Dużo poświęcił, by pomóc "Nutce" stać się Minerwą. Bardzo pozytywna postać.

## Frakcje

* Koszmar z Neotik: elitarny oddział Neotik, do którego należy Adam Szarjan i który ma najgroźniejsze eksperymenty i broń ciężką. Przez działania Lilii Ursus i jej próbę wejścia do Neotik przez łóżko, Koszmar stracił inną świetną terminuskę. 
* Grupa Wydzielona Neotik: specjalna grupa Orbitera która specjalizuje się w dziwnych eksperymentach przy użyciu Dark Technology; kiedyś współpracowała z nimi tien Minerwa Diakon. Aktywny agent - Adam Szarjan.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Gabinet Pięknotki: miejsce głównych spotkań Pięknotki i jednocześnie miejsce bezpieczne.
                                1. Pustułka: niezłej klasy jadłodajnia; Adam i Pięknotka zjedli tam obiad i rozmawiali. A potem Erwin i Pięknotka.
                                1. Kompleks Testowy: najsilniej ufortyfikowane miejsce w Pustogorze. Miejsce testów na Nutce.

## Czas

* Opóźnienie: 2
* Dni: 5

## Narzędzia MG

### Budowa sesji

**SCENA:**: Adam dowiaduje się gdzie jest Erwin; Lilia zostaje zmuszona do współpracy z Adamem.
**SCENA:**: Galilien w Miasteczku opuścił Szczelinę; ścina się z Adamem i ewakuuje na Trzęsawisko. Lilia leci za nim.
**SCENA:**: Dewastacja w walce między Nutką (pełna furia) i Adamem prowadzi do przyciągnięcia Wiktora. All-out-3-way-war. Wygrywa Adam, Galilien ucieka.
**SCENA:**: Erwin i Nutka uciekają przez Mglistą Ścieżkę. Adam nie dał rady ich zatrzymać. Wiktor walczy z Adamem i zmusza go do ewakuacji.

### Wykorzystana mechanika

1810
