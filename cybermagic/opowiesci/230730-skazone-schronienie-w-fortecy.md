## Metadane

* title: "Skażone schronienie w Fortecy"
* threads: pronoktianska-mafia-kajrata, furia-mataris-agentka-mafii
* motives: manhunt, teren-morderczy, echa-inwazji-noctis, bardzo-grozne-potwory, izolacja-zespolu, misja-survivalowa, stopniowe-tracenie-siebie, stracona-nadzieja
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [230729 - Furia poluje na Furie](230729-furia-poluje-na-furie)

### Chronologiczna

* [230729 - Furia poluje na Furie](230729-furia-poluje-na-furie)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ?
* Struktura: 
    * explore-and-maneuever
* CEL: 
    * MG
    * Drużynowy

### Co się stało i co wiemy

* .

### Co się stanie (what will happen)

* F1: Enklawa Lotosu
    * Potwory zapędzają tam Furie
    * Jest tam Leira, która się schowała przed przeciwnikami
    * IMPULS: zostańcie, tu będzie Wam lepiej
* F2: Polowanie Czarnych Czaszek na Furie
    * Xavera ich zauważy, mimo że są niewidoczni
    * Anomalia Przestrzenna -> Rozdzielone
* F3: Złapane
    * Legion Pacyfiki
    * Trzy awiany i mag, próba dominacji i przejęcia kontroli nad Amandą
    * Ernest Kajrat i sześciu noktian

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Xavera Sirtas
    * OCEAN: (C+A-): żywy ogień, jej dusza jest pełna pasji, a jej słowa ostrzem. "Kto siedzi cicho, tego spotyka wszystko co najgorsze"
    * VALS: (Security, Tradition): wszystko odnosi do tego co już widziała i co było, działa dość tradycyjnie. "Wszystko już było i się stało"
    * Core Wound - Lie: "Przeze to, że dużo mówiłam i szperałam, moi rodzice się rozwiedli" - "Będę walczyć i ognistą walką udowodnię, że warto być razem!"
    * Styl: Zawsze gotowa do walki. Agresywna i asertywna. Dobrze wybiera walki. Jej upór często wpędza ją w kłopoty.
* Ayna Marialin
    * OCEAN: (N+O+): wiruje kalejdoskopem pomysłów, które stale ją inspirują i napędzają. Wiecznie martwi się jutrem i planuje. "WIEM, że nie ma niedźwiedzi. A jeśli są?"
    * VALS: (Family, Universalism): pozytywny wpływ przy pomocy Zespołu uratuje wszystko. "Jesteśmy w stanie kształtować naszą przyszłość. Ostrożnie."
    * Core Wound - Lie: "Nie jestem tak dobra w niczym jak moje przyjaciółki; przeze mnie coś im się stanie" - "Dojdę do tego, rozwiążę to! Nie zaskoczą nas niczym!"
    * Styl: Staranna i metodyczna, Ayna zawsze analizuje sytuację zanim podejmie działanie.
* Isaura Velaska
    * OCEAN: (E+A+): uspokajający głos w zespole, zawsze znajduje chwile na uśmiech i dowcip. "Nieważne co się dzieje, trzymaj fason i się uśmiechaj"
    * VALS: (Harmony, Benevolence): "Zespół to jak rodzina. Musimy być dla siebie wsparciem."
    * Core Wound - Lie: "Rodzina mnie porzuciła, ale Furie adoptowały" - "Jeśli zrobię dla Was rodzinę, wszyscy zawsze będziemy razem bez kłótni i sporów"
    * Styl: Ostoja spokoju wśród chaosu, rozładowuje napięcia i tworzy poczucie wspólnoty i jedności; dystyngowana, zachowuje się jak arystokratka
* Leira Euridis
    * OCEAN: (C+N-O-): Chłodna i skoncentrowana, jej stalowe nerwy sprawiają, że nigdy nie traci zimnej krwi. "Ruch po ruchu, krok po kroku - to jest sposób, aby przetrwać."
    * VALS: (Achievement, Self-Direction): Uważa, że przez ciężką pracę i samodzielność można osiągnąć cel. "Nie mogę polegać na nikim innym niż ja sama."
    * Core Wound - Lie: "Byłam zabawką dla możnych, gdy byłam słabą dziewczynką" - "Muszę być cicha i ostrożna, ponieważ ludzie są nieprzewidywalni i niebezpieczni."
    * Styl: Cicha i ostrożna samotniczka, Leira jest zawsze czujna i gotowa do walki. Jej chłodny spokój skrywa gorącą pasję do ochrony tych, których kocha.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Awian się po prostu rozbił, kilka minut później. Ale te kilka minut to 17 kilometrów dalej. Ayna słabo pokazała na kontrolki

* Ayna: "Dlatego nie używają dron. Coś je zakłóca. Coś nie daje im możliwości działania. Jakaś forma... Skażenia?"
* Amanda: "Nie mamy czasu się tym przejmować" (first aid na Aynę)
* Xavera: (przygotowuje tobogan i harvestuje sprzęt gdy Amanda zajmuje się Ayną)
* Ayna: "Mogę chodzić..." (próbuje) "Nie, nie mogę". (po chwili) "Zostawcie mnie z jakąś bronią, opóźnię ich"
* Amanda: "Jak będzie tak daleko, teraz nie trzeba"
* Xavera: "Nie ma mowy, niedaleko jest Enklawa. Uratujemy Cię."
* Amanda: "Zrobimy pułapkę?"
* Xavera: "Tak, mogę. Ale to potrwa (patrzy na Aynę). A ten awian widzieli na pewno wszyscy."

Amanda robi _scrappy trap_ by opóźnić i spadają.

Tr +3:

* V (p): Xavera ma _coś_ co może się przydać - medkit, liny, nieznane środki, broń
* X: Niestety, to wszystko w tym czasie

Furie tak szybko jak są w stanie zbliżają się w kierunku na Enklawę. (konflikt na badanie).

* V (p): 
    * Amanda zauważa, że ktoś ich śledzi. W oddali. Ma dobrze zamaskowany pancerz i mignął jej hełm w kształcie czarnej czaszki. Szybkie info do Xavery i Ayny.
    * Xavera: "On ma sprzęt."
    * Amanda: "Ma też pancerz, nie poradzimy"
    * Xavera: "My TEŻ mamy pancerze" (acz mówi to przez ból, bo mimo medkita jest ranna)
    * Amanda: "Wszystkie jesteśmy ranne i nie wejdziemy w sensowne zwarcie"
* V (m): Furie zrobiły coś czego nikt się nie spodziewał. Biegną na pełnej mocy. Ignorując teren. Ayna ma SMG i celuje by robić ogień zaporowy jak trzeba; nie trafi (nie ma opcji), ale przeciwnik nie będzie chciał się wystawiać. 

I wtem - szczekanie. Trzy psy wyskakują i MIJAJĄ Furie. Tam mały patrol - 4 osoby. Machają do Furii. Wyglądają na lokalsów - kiepskojakościowy sprzęt, zero servarów... w stronę na Enklawę. Furie są gotowe do walki, ale oni przygotowują broń i rozstawiają się by osłaniać Furie. Widać formacje. Niedługo - psy wracają. Furie ich mijają i idą na Enklawę. Ekipa wraca do Was, uspokojona, ale przygotowując ogień przeciwko terenowi. 

* Raab: "Nazywam się Raab Navan" - jeden z nich - "jestem z ramienia Enklawy Kwietników, Leira powiedziała, że będziecie zainteresowani że jest bezpieczna".
* Xavera i Ayna wymieniły spojrzenia. Leira jest najbardziej ksenofobiczna, cicha, chłodna, konserwatywna i ogólnie 'nie dotykaj mnie' ze wszystkich Furii.
* Raab: (fachowo patrzy na Aynę) "Potrzebujecie medykamentów i odpoczynku. Jak tylko dotrzemy do Fortecy to Leira Was wprowadzi; powiedziała, że nikomu innemu nie uwierzycie."
* Xavera: "Tak, to brzmi jak Leira" (z lekkim sceptycyzmem)

Enklawowcy zaprowadzili Was do siebie. Forteca wygląda jak coś godnego nazwy. To ma działa. Może nie orbitalne, ale to ma BATERIE dział. Z wielu różnych statków. To patchwork.

* Xavera: (cicho) "Wszędzie są astorianie... tej armii nie pokonamy" (na oko jest koło 200+ osób, nie licząc zwierzy, dobrze wychowanych). Co gorsza, Furie się czują coraz gorzej - są Zatrute (przez pnącza). Amanda i Xavera są _alert_, Ayna odpływa. Pojawia się Leira. Leira jest uśmiechnięta. Od razu Xavera i Amanda są nieufne.
* Leira: Jak to dobrze, że nie jestem jedyna. Dostałyście wiadomość od Kajrata?
* Xavera: Tak, dostałyśmy. Kim on jest?
* Leira: Nie mam pojęcia, jakiś oficer noktiański kontynuujący wojnę. Chyba. (shrug)
* Amanda: Jak tu dotarłaś?
* Leira: Rozbiłam się niedaleko, ostrzeliwałam się, dotarłam tutaj. A dokładniej - przechwycili mnie. I okazało się, że nie chcą krzywdy, chcą pomóc. Szukają więcej wojowniczek.
* Amanda: I im tak po prostu zaufałaś?
* Leira: Czekam na rozkazy. Dostałaś jakieś? /s
* Amanda: Tak, dostałam, bądźcie wolne. Od Kajrata.
* Leira: Nie jest moim bezpośrednim przełożonym. Czekam na dowódcę FURII. Naszego. Nie Kajrata. I mam zamiar użyć tej placówki by odzyskać nasze siostry. Jakoś.
* Leira: Dobrze. Nie będziemy Was trzech rozdzielać, z oczywistych przyczyn. (mental note Amandy: 'nie będziemy'). Dostaniecie... tamten 'pokój', tylko dla Was. Możecie zamknąć drzwi itp. Nie jesteście jeńcami. Możecie mieć broń. Chcecie medyka czy medykamenty? I chcecie, nie wiem, jedzenie i wodę, czy wolicie racje? Macie jeszcze jakieś?
* Amanda: Zostaniemy przy swoich rzeczach; poprosimy o medyka ALE użyje naszych medykamentów.
* Leira: (uśmiech) Paranoiczna jak zawsze, co, Amando?
* Amanda: Dobrze mi to zrobiło.
* Leira: (szerszy uśmiech) (rzadko się uśmiecha) Nie ma sprawy, przekażę Centrali. Jestem Waszym łącznikiem. Nic Wam nie grozi, jakby chcieli mnie zabić czy coś mi zrobić, dawno by to zrobili.
* Xavera: Zakładając, że WIESZ że coś Ci zrobili
* Leira: Prawda, ale czuję się normalnie. Zróbcie sobie warty czy coś.
* Xavera: (z oburzeniem) ...czy coś... Leira, jesteś FURIĄ.
* Leira: Mam nadzieję, że masz rację. Że pojawi się dowódca, będzie rozkaz i będzie dobrze.
* Amanda: (kręci głową, kładzie rękę na ramieniu Xavery)
* Leira: (kręci głową) (cicho) Ja chcę na nasz statek...
* Amanda: Nie Ty jedna...

Leira zaprowadziła Was i się oddaliła. Niedługo później przyszedł medyk. Ma fartuch i wszystko, ma skalpele, środki itp. Dostał środki z medkitów Zespołu, spojrzał uważnie

* Medyk: "Muszę Was też zbadać."
* Amanda: "Najpierw ona"

Medyk skupia się na Aynie. Próbuje jej pomóc tym co ma.

Tp+3:

* X (p): Ayna potrzebuje albo medvat albo bardziej zaawansowanych rzeczy albo długiej rekonwalescencji
* V (p): Ayna jest stabilna
* V (m): Ayna za 2 tygodnie przy samych komponentach jakie ma Zespół będzie combat-worthy. Ale nadal będzie słaba.

.

* Medyk: "Dobra, macie też widzę środki które Wam pomogą na te zarazy które złapałyście. Bierzcie TO i TO i TO. Powiem Leirze, że ma 2 tygodnie czasu."
* Amanda: "Na co?"
* Medyk: "By Was przekonać, żebyście zostały. W innym wypadku, pójdziecie wolno." (widząc miny Furii) "Słuchajcie, nie używacie naszych zasobów, tak?"
* Amanda: "No nie, niewiele"
* Medyk: "Więc Was puścimy jak będziecie chciały. Odpłacicie jakąś nie wiem, akcją przeciw Czarnym Czaszkom i będziemy kwita. Pasuje?"
* Ayna: "Czemu Ty mówisz w imieniu całej Enklawy? Rozmawialiście o tym?"
* Medyk: "Leira przekonała Centralę. Furie potrzebują wolności. Ona powiedziała, że tu zostanie jeśli Was puścimy."
* Xavera: "LEIRA oddała wolność za nas?"
* Medyk: "Nie ma w niej nadziei. Tu może zrobić coś dobrego. I Wy też możecie zostać. Więcej, zachęcimy Was. Ale wiecie. Nic siłą. Ale gdzie pójdziecie?"

Medyk opuścił pomieszczenie. Ayna zrobiła _battle sign_, że najpewniej jesteście podsłuchiwane. W rozumieniu: ona by podsłuchiwała. Amanda kiwa głową, że zgadza się z tą opinią.

* Xavera: "Cieszę się, że po raz pierwszy od dawna widzę radosną Leirę."
* Amanda: "To prawda..."
* Ayna: "Jeśli Leira tu zostanie, jest tym więcej powodów byśmy my też chciały."
* Amanda: "To może mieć sens. Ale i tak musimy dać sobie więcej czasu. Nie jest to decyzja która podejmowałabym na szybko."
* Ayna: "Będzie warto zapoznać się z lokalną kulturą i miejscem; Leira nie podjęłaby takiej decyzji..."
* Amanda: "...bez powodu"
* Ayna: "Acz cieszę się, że wreszcie znalazła miejsce, w którym czuje się dobrze."
* Amanda: "Tu się zgadzamy."

Furie ustawiają warty (acz mogą je rozluźnić z czasem). Tak czy inaczej - trzeba się wyspać... (WSZYSTKIE Furie zauważyły, że racji mają na 3 dni...). Amanda wpadła na pomysł - jeśli przeprowadzą akcję przeciwko Czarnym Czaszkom, Czaszki będą miały zasoby. Amanda i Xavera opracowują plan. Ayna pomaga go złożyć.

* Ayna: "Najlepiej, jeśli przejdziecie się i popytacie ludzi o Czaszki, jak działają itp. A przy okazji rozejrzycie się, czy coś można użyć przeciwko Czaszkom."

Następnego dnia Amanda i Xavera się rozłażą i szukają informacji o Czaszkach, jak oni działają, w czym się specjalizują, co można zrobić itp. A przy okazji szukają jakichkolwiek anomalii dotyczących samej Fortecy w której są. I Amanda chce podpytać Leirę przy okazji.

Tr Z +4 (test na rzeczy dziwne):

* V (p): Amanda zauważyła, że Leira wie WYJĄTKOWO MAŁO o Czaszkach i sytuacji militarnej. Furia która nie wie? I której specjalnie nie zależy? Coś jest bardzo nie tak.
* X: Nie można Leirze ufać; nie można wziąć ją na operację. Amanda by chciała porwać Leirę jak co, wyciągnąć ją stąd, ale na razie TA Leira jest dziwna. A minęły tylko 3 dni.
* Xz (p): Ayna jest naturalnie słaba; nie nadaje się na operację.
* V (m): Xavera zauważyła, że wszyscy są tak samo spokojni i uprzejmi. To jest dziwne. Łącznie ze zwierzętami. Coś tu jest cholernie nie tak. I wszyscy są szczęśliwi (good honest work and good life).
    * Leira też jest tym dotknięta
    * Xavera ani Amanda nie widzą sposobu wbicia się do tej Fortecy. Po prostu nie. Detektory, działka, patrole...
* Vz (e): Amanda zauważyła kilka rzeczy
    * całość jedzenia i picia to roślinna dieta. Nie mają mięsa
    * w ekipie są ludzie z wytatuowanymi Czarnymi Czaszkami. Są noktianie. Są astorianie. Wszyscy są szczęśliwi.
    * część zwierząt porośnięta jest dziwnymi kwiatami. Część ludzi też, ale to ukrywa.
    * w powietrzu WSZĘDZIE jest specyficzny zapach
    * czyli - mamy _time limit_. Zarodniki. Patrząc po Leirze - 3 dni siedzenia tu to za dużo. Ale Leira jadła i piła wcześniej. A nadal jest Leira w Leirze. Więc... 5 dni?
        * danger: Ayna miała otwarte rany. I nie ma jak jej wyciągnąć teraz.

Na pytanie "po co wojownicy" całkowicie losowa osoba odpowiedziała (Amanda zauważyła, że nieważne KOGO pyta), że:

* muszą pozyskiwać więcej broni, pancerzy itp
* rzeczy na handel
* rozbudowa Fortecy. I potencjalnie inne Fortece.

(na tym etapie Xavera już się rwie do walki, ale ona dobrze wybiera walki; siedzi cicho, ale bardzo ma z tym problem; co więcej, zaczyna już powoli opłakiwać Aynę)

* Amanda -> Leira: "Medyk powiedział, że w ramach odwdzięczenia się mamy zrobić akcję przeciwko Czaszkom"
* Leira: "Tak, dobrze powiedział."
* Amanda: "Z kim to obgadać?"
* Leira: "Ze mną? Jestem w końcu Furią."
* Amanda: "Czego chcecie z tej grupy? Potrzebny mi intel"
* Leira: "Po prawdzie, sytuacja jest dość beznadziejna. Oni nie mają obozowiska. To nomadzi. Oni polują na nas. Próbują... próbują zabić wszystkich i przejąć Fortecę. Nie wiem, polują dla sportu?"
* Leira: "Więc mamy grupę agentów o nieznanych możliwościach znajdujących się dookoła Fortecy polujących na handel, na patrole itp. Najważniejsze byłoby zmiażdżyć siły atakujące kupców. Ale... wybacz, w trójkę nie damy rady (Amanda widziała, że Leira się poprawiła gdy chciała powiedzieć 'Wy dwie')"
* Amanda: "Lokalsi nie umieją?"
* Leira: "Brakuje im czystego umysłu walki. Nie mają taktyki, umieją walczyć, ale nie mają pomysłowości. Sęk w tym, że ja też nie... (ponuro). Może Ayna coś wymyśli?"
* Amanda: "Zaproponujemy plan - pójdą za nim?"
* Leira: "Powinni. Jeśli plan jest sensowny, tak."
* Amanda: "Skąd będą wiedzieć że jest sensowny jak nie mają wiedzy?"
* Leira: "Ja go ocenię." (Amanda zauważyła, że Leira instynktownie pozycjonowała się po stronie Fortecy przeciw Furiom i tego nie zauważyła)

Furie, dowodzone przez Amandę, opracowały plan:

* Amanda i Xavera "uciekają" z Fortecy kradnąc cenne surowce
* Forteca wysyła za nimi pościg, pościg jest szybki
* Pojawia się _opening_ w którym Czaszki mogą wpaść, ukraść i uciec
* W rzeczywistości pościg jest dużo szybszy i pomoże A+X pokonać Czaszki

Trzeba to zrobić odpowiednio wiarygodnie, ale nawet eks-Czaszkowcy powiedzieli, że to ma szansę zadziałać. Nikt nie spodziewa się, że Forteca wpadnie na tego typu plan. Trzeba zrobić to odpowiednio atrakcyjne i kuszące dla Czaszkowców. Zasoby główne zostają z Ayną - Xavera + Amanda nie będą ich ryzykować w walce. W końcu muszą wrócić do Fortecy, choćby po Aynę...

Amanda i Xavera 'kradną' ścigacz. Gonić ich też będą pojazdy. I psy. (KAJRAT TEŻ MYŚLI ŻE UCIEKŁY)

Ex +4 +3Or (rany i ptaszki):

* Or (p): Uda się ściągnąć Czaszkowców, ale będzie firefight. By uwiarygodnić, podczas firefighta Xavera jest ciężko ranna. ZE STRONY FORTECY! (maybe they didn't mean to but...)
    * Amanda i Xavera spieprzają na pełnej mocy tym ścigaczem, za nimi strzelają. Duże działo walnęło pociskiem. Odłamek poważnie, poważnie zranił Xaverę. Krew trysnęła. Amanda AUTOPILOT i medkit.
    * Xavera jest blada i nieprzytomna po medkicie. Leży na ziemi, ale już się nie wykrwawia. Gorszy stan niż Ayna.
    * (Ten pocisk w Xaverę zmusił Kajrata do ataku)
* V (m): Jeśli Amandzie uda się odpowiednio wyprowadzić tą cholerną jednostkę i Czaszkowców, Forteca ich uratuje. I uderzą w Czaszkowców.
    * Amanda manewruje cholerny ścigacz, udaje że ucieka, Czaszkowcy ją gonią i ostrzeliwują, Amanda robi co może... i WTEDY przyspieszają ścigający i wbijają w Czaszkowców ignorując Amandę.
* X: Ścigacz Amandy dostał rakietą. Rakieta nie miała ładunku. Wykoziołkował i walnął o ziemię. Uderzenie - Amanda ma _short blackout_, potłuczona; Xavera w gorszym stanie niż była.
    * Amanda widzi jak JESZCZE inne siły atakują zarówno Czaszkowców jak i Fortecę.
    * Komunikator noktiański 'stay put! Wyciągamy Was!'
    * Amanda: "Ayna tam jest!"
    * Kajrat: "Wy jesteście tu."
    * Isaura: "Amando, Ayna jest w Fortecy?"
    * Amanda: "Tak. To była część planu! Polowanie na Czaszkowców!"
* Or: Amanda traci przytomność, dostała za mocno. Bełkocze plan.
    * Isaura: (póki Amanda słyszy) "komendancie Kajrat, wyciągnij je. Plan B. Te dwie potrzebują natychmiastowej ekstrakcji, Ayną zajmiemy się potem."

EPILOG:

Amanda budzi się gdy Isaura wstrzykuje jej stymulanty. Isaura się uśmiecha smutno.

* Isaura: "Amando, statrep. Xaverę wprowadziliśmy w śpiączkę, by ją naprawić. Mocno dostała, ale wyjdzie z tego."
* Amanda: "Ayna i Leira są w Fortecy. Leira... nie rozumiem statusu. Nie wiem czy jak ją zabierzemy to będzie dobre dla niej. Ayna ciągle jest 'nasza'. Im dłużej czekamy by ją zabrać tym dłużej tak zostanie."
    * powtarza plan łowów na Czaszkowców i co spotkało Furie o których wie
* Isaura: "Czyli Forteca nie wie, że Was nie porwaliśmy. Forteca nie wie, że miałyście z Komendantem kontakt."
* Amanda: "Wiedzą tylko tyle co Ayna w najgorszym wypadku a nawet ja jestem zaskoczona..."
* Isaura: "Komendant ma plan jak wydobyć Aynę."
* Amanda: (wstaje)
* Isaura: (zatrzymuje) "Komendant ma... nietypowe metody. Ma zamiar użyć negocjacji."

Tr +3:

* V (p): Fundamentalnie, Forteca się zgodziła na plan Kajrata
* V (m): I mają zamiar utrzymać Aynę nieskażoną 
* V (e): Nawet Leirę da się odzyskać, choć będzie nieszczęśliwa
    * Amanda: "wyglądała na tak szczęśliwą..."
    * Isaura: "jak każdy pod wpływem kontroli mentalnej... nasza Leira by tak się nie zachowała."

Do pomieszczenia wszedł mężczyzna. Ernest Kajrat.

* Kajrat: "Isauro, plan zatwierdzony. Odzyskam Furie od Fortecy. Potrzebujemy wymienić za nie jednego maga."
* Isaura: "Komendancie, nie mamy dostępu do maga."
* Kajrat: "Mam trzy Furie, jedna jest nieaktywna, jedna jest ranna ale się zregeneruje i jedna jest aktywna. Zdobędę maga."
* Isaura: "Możemy ufać Fortecy?"
* Kajrat: "Nie. Ale maga przyjmą z radością. Da im więcej niż dwie Furie."

## Streszczenie

Po rozbiciu awiana Furie nie mają już zasobów ani możliwości. Mimo, że Czarne Czaszki na nie polowały, uciekły do Fortecy Symlotosu (nie wiedząc co to jest). Tam dostały pomoc i spotkały Leirę (kolejną Furię) która zaakceptowała Symlotos. Furie dzielnie broniły się przed Skażeniem, skupiając się na pomocy Symlotosowi i zwalczaniem Czaszek. Niestety, operacja zwabiania Czaszek w pułapkę skończyła się ciężką raną Xavery. Ale Kajrat wspierany przez kolejną Furię, Isaurę, pozyskał Amandę i Xaverę, po czym otworzył negocjacje z Symlotosem by odzyskać Leirę i Aynę za maga.

## Progresja

* Amanda Kajrat: lekko ranna (2 dni regeneracji), przechwycona przez siły Kajrata i inkorporowana do oddziału Isaury.
* Xavera Sirtas: bardzo ciężko ranna, wprowadzona w śpiączkę przez medyków Kajrata; za dwa miesiące będzie w stanie operacyjnym przy aktualnym poziomie technologicznym w obszarze Enklaw
* Ayna Marialin: za 2 tygodnie będzie działać; znajduje się w Fortecy Symlotosu. Zgodnie z umową z Kajratem Lotos jej nie dotknie, ale wymienią ją za maga.
* Leira Euridis: dotknięta przez Symlotos, agentka Fortecy choć jeszcze nie do końca tego świadoma. Kajrat może ją wykupić za maga.

## Zasługi

* Amanda Kajrat: przeprowadziła Furie przez piekło do Symlotosu, tam ustrzegła je przed Skażeniem a potem opracowała plan pozyskania zasobów i dała się uratować Kajratowi. Nigdy nie straciła nadziei i wymanewrowała wszystkich przeciwników - myśleli, że Amanda się złamie, podda czy straci nadzieję, ale Amanda szła do przodu i kombinowała. Niezłomna NAWET jak na Furię.
* Xavera Sirtas: ranna, ale przez zaciśnięte zęby prze do przodu. Zauważyła, że ludzie na obszarze Symlotosu nie zachowują się naturalnie, coś ich zmieniło. Skończyła ciężko ranna i w sztucznej śpiączce.
* Ayna Marialin: zbyt ranna; prosi by ją zostawiły, ale X+A się nie zgadzają. Odkryła, że na tym terenie nie działają drony. Pomogła z zaprojektowaniem planu zmiażdżenia Czarnych Czaszek.
* Isaura Velaska: Furia współpracująca z Kajratem; wyciągnęła Amandę i Xaverę z ognia między Symlotosem a Czarnymi Czaszkami.
* Leira Euridis: Furia, którą uratowała Forteca Symlotosu. Zaadaptowana przez nich, została przewodniczką dla Trzech Furii. Jest szczęśliwa, pierwszy raz ma spokój. Jeszcze nie jest zainfekowana Symlotosem.
* Ernest Kajrat: oficer logistyczny jednostki wsparcia; ma niewielką bazę w obszarze Enklaw. Wyciągnął Amandę i Xaverę i negocjuje z Symlotosem oddanie Ayna i Leiry za maga oraz pomoc z Czaszkami. Pragmatyk jak cholera.
* Raab Navan: jeden ze speakerów Symlotosu, dowodził operacją mającą uratować Trzy Furie przed Czarnymi Czaszkami. Z powodzeniem.

## Frakcje

* Nocne Niebo: pozyskało dwie Furie: Amandę i Xaverę (ta jest bardzo ciężko ranna), kosztem operacji atakującej patrol Fortecy i Czarne Czaszki.
* Czarne Czaszki Enklaw: skutecznie zaatakowali siły Fortecy i Amandę, po czym zostali zaatakowani i odparci przez siły Kajrata ratujące Amandę i Xaverę.
* Kwietnicy Lotosu: przez to że mają Aynę i Leirę wynegocjowali z Kajratem, że on będzie chronił ich karawany handlowe przed Czarnymi Czaszkami i dostarczy im maga.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, SW
                    1. Granica Anomalii: na tym terenie drony z obniżoną psychotroniką bardzo szybko ulegają Skażeniu - przez Pacyfikę i Szczelinę
                        1. Forteca Symlotosu: niezwykle pancerna Forteca opanowana przez Symlotos; ma działa i defensywy z wielu jednostek

## Czas

* Opóźnienie: 0
* Dni: 3

## Specjalne

* .

## OTHER
### Okolice terenu Pacyfiki

* Typ terenu: 
    * https://baronphotography.eu/najpiekniejsze-poloniny-w-bieszczadach/   (1000-1500 m npm)
    * Puna (półpustynia wyżynna) https://pl.wikipedia.org/wiki/Puna_(biogeografia)
    * https://pl.wikipedia.org/wiki/Puna_(biogeografia)#/media/Plik:Peru_-_Altiplano1.jpg
