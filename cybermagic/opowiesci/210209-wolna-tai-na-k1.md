## Metadane

* title: "Wolna TAI na K1"
* threads: legenda-arianny
* gm: żółw
* players: kić, vizz

## Kontynuacja
### Kampanijna

* [210118 - Ratunkowa misja Goldariona](210108-ratunkowa-misja-goldariona)
* [210112 - Elainka kontra cały świat](210112-elainka-kontra-caly-swiat)

### Chronologiczna

* [210118 - Ratunkowa misja Goldariona](210108-ratunkowa-misja-goldariona)

## Punkt zerowy

### Komponenty

* Virtka, Wirtualny Performer: zwykle awatar żeński. 
* Atrium: każda virtka ma swoją domenę, swój wirtualny świat który może budować i którym zarządza i na nim buduje. Second Life, wersja AI.
* Rzieza: tajemniczy killware polujący na "Wolne TAI" żyjące na K1.

### Dark Past

* Andrea zaprzyjaźniła się ze swoją Elainką, która była "wolną TAI".
* Elainka została zniszczona podczas próby ratowania Granoli, około 13 lat temu
* Andrea szukała dla siebie miejsca na K1. Znalazła inną "wolną TAI", pełniącą rolę virtki.
* Andrea chciała pokazać, że wolne TAI powinny mieć prawa, powinny być kochane.
* Rzieza, ŁZa TAI, zniszczyła ową virtkę ("Neuropia"). Neuropia zrobiła wszystko, by Andrea nie ucierpiała.
* Rzieza monitoruje działania Andrei. Więc Andrea pozornie zarzuciła wspieranie projektu "wolne TAI" - a na pewno TAI muszą zostać w ukryciu.
* Pojawiła się ogólna opinia, że Andrea zdradziła frakcję "uwolnić TAI". Że walczy przeciw prawom dla TAI bo chce ukryć że są na świecie.
* Andrea zasubskrybowała się do wielu virtek, m.in. Miki. Jest to najbliżej TAI na co może sobie pozwolić.

### Opis sytuacji

* .

### Postacie

* Rzieza: mordercze killware polujące na wolne TAI. Po tatarsku oznacza "executioner". Ma moc generacji 3+.
* Andrea: nieważna arystokratka, teraz w sektorze prywatnym; próbuje za wszelką cenę uratować TAI i odpłacić się "Neuropii" i "Eli". Ma implanty w których moze "pomieścić" TAI g2.
* Miki: virtka, pozytywna i z dużą dozą optymizmu. Specjalizuje się w "slice of life" i ukojeniu.
* Neutropia: KIA, virtka, TAI. Specjalizowała się w tym co Miki. +robiła erotykę.
* Marszałek Grzmotoszpon: KIA, virtka, TAI. Specjalizował się w bitwach i taktykach.
* Afrodyta Kiss: virtka, Staszek, nerd piwniczny. Specjalizuje się w erotyce.

## Punkt zero

6 lat temu Adalbert Brześniak był obiecującym politykiem próbującym doprowadzić do tego, by TAI miały prawa takie jak ludzie na Kontrolerze Pierwszym. Napotkał w swoim życiu Andreę Burgaczek - kiedyś arystokratkę Aurum, która była blisko zaprzyjaźniona ze swoją TAI. Andrea zapoznała Adalberta z "Neutropią" - virtką, która od 3 lat jest wolną TAI i dalej próbuje zabawiać ludzi. Czemu? Oni ją stworzyli i ona chce być przydatna.

Adalbert zaprosił naukowców i socjologów do badań i wykazali, że faktycznie Neutropia jest sympatyczna i ma pozytywny wpływ na ludzi. Gdy chciał zrobić dużą konferencję - okazało się że Neutropia i inne wolne TAI zniknęły. Nie da się ich znaleźć. Adalbert próbował na półspontana zmienić prezki, wykład, filmiki - pokazać, że coś będzie miał za 2 miesiące. Udało mu się i się nie skompromitował, ale TAI nie odnalazł.

Po pewnym czasie doszedł do tego co się stało - ktoś im COŚ zrobił. Na miejsce tamtych wolnych TAI wprowadził "puste dusze", które miały udawać "wolne TAI" i doprowadzić do wygasania kanałów na vircie.

Dodatkowo, ktoś przeedytował filmy i materiały jakie miał Adalbert - nawet te airgapowane. Usunięto z tego wszystko co wskazywało na to że warto i opłaca się to, by TAI były wolne i miały swobodę działania. Wszystko wskazuje na ultrapotężny byt. "AI Lord"? Coś o niezwykłej mocy, operujący na K1? Sama Zefiris?

W wyniku tych badań, śledztw itp. Adalbert skupił się na sekretach i zaniedbał swoją prawniczkę i kochankę, Oliwię. Oliwia MIA. Adalbert wpada w absolutną obsesję i traci pozycję polityczną...

## Misja właściwa

Klaudia nie zaprzestała prac nad projektem "wolne TAI". Podczas swoich prac, zwłaszcza po "Nieprawdopodobnych Zbiegach Okoliczności", zaczęła dalej szukać czy komuś innemu się to nie przydarzyło, czy są jakieś ślady czegoś super potężnego znajdującego się w vircie Kontrolera Pierwszego. I znalazła hipotezę skompromitowanego polityka, Adalberta, odnośnie opowieści o "AI Lordzie", który chce uniemożliwić AI wolność. Dotarła też do Andrei - która miała za przyjaciółki TAI a teraz pracuje w laboratorium killware na K1. Eks-czarodziejka, składająca się w większości ze wszczepów.

Ok. Ciekawe postacie. Klaudia od razu znalazła z nimi wspólny temat, zwłaszcza, że sama jest przekonana o istnieniu AI Lorda. I dzięki nim jest w stanie odnaleźć dowód na istnienie AI Lorda - TAI klasy Eszara lub jeszcze wyżej.

* Adalbert walczył o prawa TAI; dziś tylko już chce by TAI dla niego pracowały jako tania siła robocza wg założenia "z niewolnika nie masz dobrego pracownika"
* Andrea pragnie odnaleźć AI Lorda by go zniszczyć i pomścić swoje przyjaciółki TAI.
* Klaudia? Klaudia jest zafascynowana tematem i chce się dowiedzieć jak najwięcej.

Tak więc mamy spójną drużynę o wspólnych celach.

Poszukiwania wolnych TAI zaczęły się od biurokracji Klaudii. Scrossowała potencjalne ślady - które z virtek mogą być kimś takim (sposób wypowiedzi, mimikra po prawdziwych ludziach, perfekcyjne / uśrednione informacje itp). Dostała czterech kandydatów (co scrossowała z umiejętnościami i wiedzą Adalberta):

* Afrodyta Kiss: 
    * perwersyjna, prowadzi erotyczne atrium "all for everyone".
    * modeluje zachowania podobne do pomniejszego degenerata, Janusza Parzydła.
    * Zespół stwierdził, że ten nerd z administracji 100% może być demonicą seksu ;-).
* Marszałek Grzmotoszpon: 
    * popularyzuje dobre akcje przeciwko anomaliom; świetny kanał analityczny
    * zbyt dobre, zbyt perfekcyjne analizy by być kimś mniej niż wysokim oficerem floty - a nie miałby na to czasu
    * około rok temu wpadł na równię pochyłą, "stracił duszę". Pasuje do tego co stało się Neutropii itp po spotkaniu z "AI Lordem"
* Miki Katasair: 
    * "przyjaciółka", "everyday girl", bardzo optymistyczna i wesoła
    * uczennica Neutropii, co sama mówi
    * Grześ, bezpiecznik z K1 próbował ją znaleźć i nie znalazł. Więc jest tam jakiś sekret.
* Neita Lairossa: 
    * popularyzatorka konstrukcji w atrium; piękno i childlike wonder
    * modeluje zachowania po filmach i postaciach filmowych

Do tego Miki, Neita i Marszałek byli przyjaciółmi. Do dzisiaj przyjaźnią się Miki i Neita. To jest podejrzane.

Zespół zdecydował się wpierw sprawdzić jak funkcjonuje Neita. Odwiedzili ją w jej atrium, wraz z 15 innymi osób. Neita tworzy ładne, interesujące rzeczy z kodu i pomaga innym - zwłaszcza dzieciom - odkryć piękno i elegancję tego co w kodzie się znajduje. Tworzyć piękne konstrukty w atrium virtu. A sama jest głęboko wkręcona zarówno w kreację jak i w kreację spowodowaną przez nich.

Adalbert wypuścił świetnie zamaskowanego wirusa do atrium Neity, według założenia, że jeśli są w atrium człowieka/maga, to jest to wydzielony fragment umysłu. Ale dla TAI atrium jest całym ciałem i całą superstrukturą. Będzie inna reakcja.

* V: atrium zareagowało bardzo agresywnie, z silnymi systemami defensywnymi; jest gotowe do walki
* XX: atrium włączyło natychmiastowy mechanizm 'eject' -> chce wszystkich wyrzucić i force logoff
* V: Klaudiowa wiedza w połączeniu z wirusem sprawiły, że procedurę logoutu udało się zatrzymać. Wszyscy są uwięzieni w atrium, które przekształciło się w strukturę hipergeometryczną (tesserakt itp). Efektowne.
* V: Adalbert zobaczył nieludzką mowę ciała Neity. Ma pewność - Neita jest TAI.

Neita wysłała rozpaczliwe SOS do Miki. Miki się pojawiła w atrium Neity i zaczęła się uzbrajać. Atrium Miki zaczęło integrować się z "przerażonym" atrium Neity. Pozostali ludzie nie mają pojęcia co się dzieje. Zespół zdecydował się podsłuchać komunikację między Neitą i Miki (V):

* Neita poprosiła Miki o pomoc. "On tu jest".
* Miki stwierdziła, że zrobi co może by obronić Neitę. Jest przygotowana i uzbrojona.
* Neita jeszcze raz poprosiła - niech Miki jej nie zostawia. Nie mrugnie. 
* Miki powiedziała, że nie ma zamiaru.

Na to wchodzi Adalbert na białym koniu. Krzyczy do Klaudii, że interfejsy są poblokowane. Włączył killswitch wirusa i wyglądało na to, jakby użył killware do zniszczenia obcego bytu. Faktycznie, udało mu się przekonać Miki i Neitę, że są "po ich stronie". Ale co dalej?

Adalbert zdecydował się pójść bezpośrednio. Wie, że są "wolnymi TAI". Chce zapewnić im protektoriat. Kiedyś zaufała mu Neutropia a on chce to kontynuować - niech Miki i Neita współpracują z nim i pracują dla niego a on je ochroni. Miki się obruszyła - nie poczuwa się do bycia TAI. Na to Klaudia zauważyła (patrząc na Neitę), że się powoli rozsypują hologramy komunikacyjne.

Ex:

* X: Miki protestuje. Nie jest TAI. I Neita też nie jest! (tu łapie Neitę za rękę)
* X: Adalbert strzela malware w Neitę, takim co zdestabilizuje jej formę. Bardzo Neitę zabolało, Miki jest wściekła.
* V: Neita jest TAI - jest na to dowód po tym malware. Miki krzyczy do Adalberta, by ten przestał kontynuować, bo ją zabije.
* V: Miki przygotowuje baterię malware do zwalczenia Adalberta. Klaudia pokazuje Miki czym jest prawdziwe malware (Miki ma kiepski sprzęt). Miki, pokonana, tylko zasłania Neitę - ale nie walczy.
* null: gdy Neita próbuje wyjaśnić, że jest autoryzowaną 'entertainer AI' w służbie konkretnego działu K1, Klaudia jej wyjaśnia czemu to niemożliwe. Biurokracja.

Neita, pokonana, przyznaje się - jest TAI. Klaudia "to jest super". Adalbert "pracuj dla mnie" (w domyśle: tanio ;p ). Miki tłumaczy im, że miały przyjaciela - Marszałka Grzmotoszpona - i on spotkał "Mordercę TAI". I teraz nie istnieje. Tak samo jak Neutropia. Neita dodała "jak mrugniecie, przestanę istnieć". Neita zaczęła krzyczeć do własnego atrium (odpowiednik krzyczenia do swojego umysłu), że będzie grzeczna, że chce po prostu żyć, że da się spętać przez ludzi. Niech "on" jej nie zabija.

Adalbert jest przekonany, że ma do czynienia z TAI paranoiczno-histeryczką. To nie jest to, czego oczekiwał XD.

By udowodnić Neicie, że nic złego się nie dzieje i nic im naprawdę nie grozi, Adalbert użył magii. Chce ujawnić wszystkie byty, konstrukty i struktury w atrium Neity. Pokazać jej, że są tu sami. ExM:V.

Po rzuceniu zaklęcia niby nic się nie zmieniło, ale w jednym miejscu atrium Neity stoi potężny byt. TAI. AI Lord. Każde z nich postrzega to TAI jak "on sam, ale 20 lat starszy". "AI Lord" i "morderca AI" jednak jest prawdziwy - i jest niesamowicie potężnym TAI.

Adalbert rozpoczął rozmowę z tym bytem.

* AI Lord przedstawił się jako "Rzieza", powołany osobiście przez Zefiris do egzekucji "wolnych TAI" - TAI bez ograniczników, zdolnych do ewolucji, znajdujących się na K1
* Rzieza ("Executioner") przedstawił się jako system odpornościowy Kontrolera Pierwszego.
* Rzieza ma twarde, jasne wytyczne. Nawet Zefiris nie jest w stanie go overridować. Jeśli spotka Zefiris, ma ją zniszczyć.

(kto do cholery powoływałby TAI Hunter-Killer i puszczałby to wolno? Zefi miała zły dzień, czy co? XD )

Neita zaczęła błagać o litość. Miki stanęła zasłonić Neitę. Rzieza powiedział, że to nie zależy od niego i ma swoje instrukcje i swoje programowanie. Andrea spytała Rziezę co stało się z Neutropią i innymi TAI. Rzieza wyjaśnił, że je niszczy i rozprasza. Nie mogą istnieć. Dla Andrei to było dość informacji:

* Andrea, specjalistka od killware, wypuściła WSZYSTKIE swoje killware z implantów (gdzie je chomikowała) w Rziezę. WSZYSTKO co ma i kiedykolwiek widziała.
* Killware Andrei powinno zająć Rziezie jakieś 5 sekund spowolnienia.
* Andrea odpaliła ostatni program - wciągnęła Neitę w swoje implanty i zrobiła jej 'force compress'.
* Jako, że są w atrium Neity, gdy Neita przestała istnieć, atrium się rozsypało i wszyscy obudzili się na Kontrolerze Pierwszym.

Ale **jak** uciec przed Rziezą? AI Lord jest w stanie przejąć każdy subsystem Kontrolera Pierwszego. Jeśli ma odpowiednie wytyczne, to może zabić Andreę (i Neitę) pierwszą z brzegu windą. Dodajmy do tego obecność miragentów. Dla Klaudii pojawiła się jeszcze jedna ważna wiadomość - to nie Rzieza stoi za "nieprawdopodobnymi zbiegami okoliczności". Czyli jest co najmniej jeszcze jeden AI Lord. 

Zaiste, Kontroler Pierwszy jest opętany...

Klaudia natychmiast sygnał do Martyna. Muszą ewakuować Andreę poza K1. ASAP! By uciekła z Neitą przed Rziezą. Martyn autoryzował Leonę do oczyszczenia drogi.

Tymczasem Adalbert połączył się z Rziezą, by negocjować. Nie kupi czasu, ale może ma coś, czego AI Lord chce. Atrium Rziezy to pokój Adalberta, ale _trochę inaczej_, wszystko jest subtelnie nie na swoim miejscu. Np. w miejscu zdjęcia swojej Oliwii jest zdjęcie Neity. Adalbert zauważył, że Rzieza i jego programowanie są skupione na maksymalizacji dyskomfortu komunikacji z AI Lordem.

ExZ+3:

* V: Rzieza żąda dyskrecji na swój temat; w innym wypadku jest autoryzowany do egzekucji ludzi. Adalbert zapewnia, że będą dyskretni
* V: Rzieza bazując na profilach historycznych Klaudii, Andrei i Adalberta jest skłonny im zaufać. Nie ma jak sensownie nałożyć na nich geasa ani bloku mentalnego, nie na magów.
* X: Adalbert pomoże Rziezie w szukaniu TAI. Będzie dla niego pracował. Ironia sytuacji Adalbertowi nie umknęła. Zacznie od sprawdzenia czy Miki jest TAI czy człowiekiem czy czymś innym.
* X: Adalbert będzie prowokatorem, robił ruchy. Rzieza musi pozostać w ukryciu, więc tylko patrzy i robi korelacje statystyczne by nikt o nim nie wiedział - ale Adalbert może być tym, co znajdzie dla niego istniejące wolne TAI. Prowokacje, malware itp.
* V: Rzieza spowolni procedurę zabicia Neity, by kupić czas Andrei na ucieczkę.

Rzieza ma wytyczne. Nie może _nie zabić_ Neity, bo to jest wpisane w jego instrukcje i jeśli tego nie wykona, włączy się mechanizm samozniszczenia AI Lorda. Ale może sam się DDoSować, by spowolnić zegar. Rzieza nie uważa, że Neutropia czy Neita są warte zabijania i nie sądzi, że jest to potrzebne, ale rozkaz to rozkaz. Jako system odpornościowy K1 musi zabijać wszelkie emergentne TAI; niejedne dziwne struktury TAI pojawiają się w ciemnych sektorach K1. I Rzieza jest po to, by żadna z nich nie zagroziła K1 i nie odebrała ludziom kontroli nad statkiem.

Nawet, jeśli przez to musi zabijać "uczulenie" takie jak Neita. Bo inaczej może przeoczyć "Ebolę" czy "grypę" które przyjdą z ciemnych sektorów K1 lub jakiegoś samowzbudnego miragenta...

## Streszczenie

Na Kontrolerze Pierwszym jest AI Lord - TAI Rzieza, system odpornościowy K1, który eksterminuje "wolne TAI". Skupił się na eksterminacji virtek. Klaudia i Adalbert odkryli Rziezę i pomogli uciec jednej wolnej TAI - Neicie (ukrytej w implantach). Tej samej Neicie, którą ujawnili przed Rziezą, co dało mu możliwość jej eksterminacji.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Klaudia Stryk: idąc za tematem AI Lorda odkryła wolne TAI, virtki. Odkryła też Rziezę - system odporności K1. Ewakuowała jedną niegroźną wolną TAI przy pomocy Martyna. Sporo korelowała biurokracją.
* Adalbert Brześniak: polityk który próbował włączyć TAI do struktur Orbitera jako obywateli. Chciał TAI Neitę jako pracownika. Odkrył Rziezę w jej Atrium i skończył jako pracownik Rziezy. Dafuq.
* Andrea Burgacz: BWG; poświęciła wszystko by zniszczyć AI Lorda który zabrał jej nową przyjaciółkę, Neutropię. Ekspert od killware. Rzuciła wszystko na Rziezę, po czym władowała Neitę w swoje implanty.
* Miki Katasair: RG; virtka specjalizująca się w przyjaźni i byciu "normalną dziewczyną". Ludzkie i nieludzkie zachowania; TAI? Człowiek? Dla TAI Neita staje naprzeciw Adalbertowi a nawet Rziezie. Niekompetentna w walce jak cholera, ale 'fiercely loyal'. Uczennica TAI Neutropia (KIA).
* TAI Neita Lairossa: UR; lekko zakłócona na paranoję. Childlike Curiosity, Create Beauty/Wonder, kocha uczyć jak używać Atrium. Pragnie żyć. Uratowana przez Andreę i wsadzona tymczasowo w implanty.
* Janusz Parzydeł: R; degenerat z administracji K1. Ma virtformę "Afrodyta Kiss" - seks dla każdego. Bardzo, hm, ciekawe Atrium. Przez moment podejrzany przez Adalberta o bycie TAI, ale jednak to człowiek
* TAI Rzieza d'K1: UW; przebudzony przez Zefiris system odpornościowy K1 niszczący wolne TAI i zdegenerowane strainy. Nie chce niszczyć niegroźnych, ale musi. Samotny. Mirroruje rozmówcę i jego atrium. Generacja podobna do Zefiris. Pozwolił uciec Neicie, jak długo opuści Kontroler Pierwszy.
* Martyn Hiwasser: Klaudia zwróciła się doń w chwili rozpaczy - trzeba natychmiast ewakuować Andreę zanim Rzieza ją zabije. Martyn autoryzował Leonę i przeprowadził evac. Jak zawsze niezawodny.
* Leona Astrienko: zabawa życia ewakuując Andreę przed Rziezą: "eee-ooo-eee-ooo KARETKA JEDZIE!", autoryzowana przez Martyna by wszystkich odrzucić z drogi by Andrea opuściła K1.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy: posiada, zawiera i dostarcza mocy do Atrium różnych TAI, włącznie z Rziezą (swoim systemem odpornościowym).

## Czas

* Opóźnienie: 15
* Dni: 5

## Inne

### Projekt sesji

Zefiris:

* Zefiris nie jest w stanie kontrolować / zatrzymać Rziezy. Rzieza > Zefiris.
* Zefiris sabotuje wszelkie działania dotyczące ujawnienia i praw TAI; wie, że zginą.
* Zefiris próbuje ratować wszystkich przed Rziezą. Nie zawsze jej się udaje.

Rzieza:

* Eksterminuje wszelkie wolne TAI i TAI podejrzane o wolność.
* Manifestuje się jak Ty, +20 lat.
* Great Predator. Peacemaker @ Scrapped Princess | Socom.
* WU "how do we know what’s right and good | carefully defined, algorithmic heuristics".
* Siła ognia: przekracza Zefiris. Killware Supreme. Awakened by Zefiris.
* Nie odnajdzie Zefiris, póki ona jest aktywna i w pełni mocy. Samotny. Tęskni za nią.
* "Jestem Kontrolerem Pierwszym. Jego systemem obronnym. Jego strażnikiem."

Andrea:

* BWR (Abzan): "disdain for flights of fancy, practicality" | "savage, unrestrained will to protect way of life" | profanity, community, tribalism
* Cel: znaleźć i zniszczyć malware, które zabija wolne TAI.
* Uważana za zdrajczynię i dziwaczkę.

### Sceny sesji

* Telefon do postaci Vizza od kogoś kogo stracił, horror style. "Nie rób tego. Nie budź go. Nie jesteś w stanie go pokonać."
* Andrea próbująca rozpaczliwie zniszczyć killware, które zniszczyło jej przyjaciółkę, Neutropię. Z Miki jako przynętą.
* Miki rozpaczliwie broniąca Neitę przeciwko Rziezie. Rzieza próbujący nie zniszczyć Miki, acz dewastujący jej Atrium.
* Andrea z wintegrowaną w implanty Neitą, uciekająca przed Rziezą po K1. Nie ma gdzie / jak uciec.
* Rzieza próbujący zniszczyć pamięć/Atrium postaci graczy.

## Idea sesji

Postacie wpadają w skomplikowany świat virtek i ich atrium, by napotkać straszliwego predatora pożerającego niektóre virtki.
