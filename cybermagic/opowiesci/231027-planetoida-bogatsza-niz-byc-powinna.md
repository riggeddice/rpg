## Metadane

* title: "Planetoida bogatsza niż być powinna"
* threads: planetoidy-kazimierza, sekret-mirandy-ceres
* motives: energia-alteris, bardzo-grozne-potwory, niekompetentna-pomoc, ukryty-aniol-stroz, koniecznosc-pelnej-dyskrecji, przyneta-atrakcyjna
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [220327 - Wskrzeszenie Królowej Przygód](220327-wskrzeszenie-krolowej-przygod)

### Chronologiczna

* [220327 - Wskrzeszenie Królowej Przygód](220327-wskrzeszenie-krolowej-przygod)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA: 
    * Devouring Stars "Whispers in the Void"
* Opowieść o (Theme and vision): 
    * "W Planetoidach Kazimierza kryją się straszne rzeczy - przede wszystkim energia Konstrukcji, ale też Nie-Rzeczywistości"
    * Po pojawieniu się Królowej jako jednostki górniczej, zespół zaczął robić rzeczy bardziej ryzykowne niż są w stanie sobie z tym poradzić
    * SUKCES
        * uratowanie jak największej ilości ludzi, nie wzięcie jaj, uratowanie załogi
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * energia-alteris: (drp Miranda): tu się manifestuje przez niepostrzegalną anomalię która Zmienia górnika w Alterienta którą widzi tylko Miranda
    * bardzo-grozne-potwory (drp Miranda): Alterient, która próbuje pożreć wszystkich... i nikt jej nie postrzega
    * niekompetentna-pomoc: nie ma tu blakvelowców ani górników najlepszej klasy. Ogólnie, nic co robią nie jest dość dobre na NieGórnika.
    * ukryty-aniol-stroz: Miranda. Miranda to ukryty anioł stróż. Tylko ona może chronić przed Alterientem.
    * koniecznosc-pelnej-dyskrecji: nikt nie może dowiedzieć się o obecności Mirandy i jej znaczeniu
    * przyneta-atrakcyjna: bardzo bogata planetoida; nie dość że szybciej do domu to jeszcze można pozyskać sporo pieniędzy
* Detale
    * Crisis source: 
        * HEART: "niski poziom kompetencji górników i nie wiedzą co dokładnie robić", "Altarient jest niewykrywalna dla górników"
        * VISIBLE: "nikt nie widzi, że to jest NieGórnik tylko potwór kosmiczny"
    * Delta future
        * DARK FUTURE:
            * chain 1: Ofiary śmiertelne
            * chain 2: Alterient ucieknie
            * chain 3: Królowa zostanie uszkodzona
            * chain 4: Królowa będzie podejrzewana przez Blakvelowców
        * LIGHT FUTURE: "Nikt nie wie o Królowej, nie ma ofiar, Altarientianin zostanie odepchnięty i zniszczony"
    * Dilemma: brak, sesja funkcjonalna

### Co się stało i co wiemy

* Przeszłość
    * .
* Teraźniejszość
    * .

### Co się stanie (what will happen)

* F0: _Pierwsza akcja górnicza_
    * INSTANCES
        * Juliusz i Łucjan się żrą, Prokop próbuje deeskalować, widać kto tu rządzi
        * Gotard 'nie potrzebujemy tyle płacić za Waszą ochronę', Kazimierz 'płacicie tanio i cieszcie się że tak tanio'
            * Znowu nie działają działka. Nie da się ich zamontować na Królowej? Coś jest jak zawsze nie tak
        * Jakaś akcja że sprzęt się popsuje i zaraz górnik będzie ranny (co zrobi Królowa?)
* F1: _Podejrzanie dobry asteroid_
    * stakes:
        * czy Królowa pozostanie w ukryciu czy będzie musiała zabić ludzi?
        * czy ktoś poza szkieletową załogą PŁG wróci bezpiecznie?
        * czy uda się pozyskać jakąś kasę? czy uda się NIE wziąć jajek NieGórnika?
    * opponent: 
        * NieGórnik. Nie jest prawidłowo postrzegany; każdy widzi go jako tego kim powinien być
            * potwór musi pożreć kilka osób, a potem chce wrócić z ekipą
        * Blakvelowcy. Wierzą w swoją percepcję, nie do końca rozumieją co się tu dzieje
    * problem:
        * wszyscy myślą, że górników było N-1, a było N; tylko Miranda postrzega prawdę
        * Miranda musi pozostać w ukryciu
    * koniec fazy:
        * zostajemy / wracamy (z potworem / bez)
    * INSTANCES
        * Mikołaj i Bartek kłócą się o plugawość Królowej ('organiczna papka do jedzenia'). Seweryn broni Królowej. Łucjan ma wątpliwości.
        * przebudzenie NieGórnika, Miranda zauważa, że jednej osoby nie ma a widzi inną
            * (tam są dane z tego co się stało z górnikiem; jajko weń weszło)
        * NieGórnik poluje na innego delikwenta i go zabija, nikt nic nie zauważa
        * kłótnie
            * Grzesiek próbuje ukraść więcej kasy dla siebie, szuka co lepszych kawałków
            * Mikołaj zaczyna dramę bez powodu, np. 'Bartek co nam Blakvelowcy skoro możemy mieć kasę'
            * Bartek idzie w stronę konieczności odbudowy i poszerzenia bazy górniczej
            * Prokop próbuje stabilizować, Łucjan unika i robi swoje, Gotard 'trzeba było zainwestować więcej, patrz tą planetoidę, nawet sensory dobrze nie czytają'
    * DARK: 
        * chain 1: Dodatkowe, ogromne ofiary śmiertelne (poza 2 osobami)
        * chain 2: wracamy z jajkiem NieGórnika
        * chain 3: Królowa jest wykryta i musi zabić bo ktoś wie
        * chain 4: wracamy z NieGórnikiem
        * chain 5: wracamy bez niczego (kasa)

## Sesja - analiza

### Fiszki

Zespół:

* Prokop Umarkon: 
    * rola: kapitan, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: extravert, agreeable (outgoing, enthusiastic, trusting, empathetic)
    * values: power + conformity
    * wants: pragnie zostać KIMŚ, wydobyć się z bagna
* Łucjan Torwold: 
    * rola: administrator / logistyka, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: neurotic, conscientous (very worried, scared of cults, very organized)
    * values: security + conformity
    * wants: przesądny; boi się duchów i kultu. Narzeka. Chce bezpiecznego, prostego życia
* Gotard Kicjusz: 
    * rola: technical guy, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: open, low-neuro, low-consc (extra stable, spontaneous, eccentric)
    * values: self-direction
    * wants: przygód, odzyskać rodzinę
* Seweryn Grzęźlik:
    * rola: psychotronik, blakvelowiec
    * personality: neuro, low-extra, consc (workaholic, obsesyjny, cichy, skłonności do wybuchania, umiłowanie porządku)
    * values: self-direction, self-enhancement
    * wants: zrozumieć sekret Mirandy, Blakvel forever, ciszy i rozwoju

Blakvelowcy i górnicy:

* Grzegorz Fabutownik: górnik w przestrzeni Blakvela
    * OCEAN: E+A- |Zawsze szuka adrenalinki i kasy;; Nie dba o innych, może kraść | VALS: Power, Hedonism | DRIVE: Kasa i wszystkie laski jego
* Mikołaj Resztkowiec: górnik w przestrzeni Blakvela
    * OCEAN: E+N- |Nie kłania się nikomu;; Ciekawski;; Oczytany, ceni wiedzę dla wiedzy| VALS: Hedonism, Achievement| DRIVE: Drama
* Bartek Burbundow: górnik w przestrzeni Blakvela
    * OCEAN: A+O+ |Mało energii;; Kreatywny, tworzy wiecznie coś nowego;; Pomocny, starający się wesprzeć innych| VALS: Face, Tradition| DRIVE: Odbudowa i odnowa
* Juliusz Cieślawok:
    * OCEAN: A-O- |Fatalne żarty;; Antagonizuje;; Ciemięży ludzi| VALS: Stimulation, Universalism | DRIVE: Niska zabawa i ciemiężenie bliźniego
* Kazimierz Zamglis:
    * OCEAN: A-N+ |Żyje w swoim świecie i ma wszystko gdzieś;; Dość agresywny;; Unika hałasu | VALS: Security, Self-Direction | DRIVE: moja korporacja jest najlepsza
* .5 innych górników

#### Strony

* Górnicy w Domenie Ukojenia i mały biznes: 
    * CZEGO: zachować niezależność, handel itp. Przetrwać. Zarobić.
    * JAK: sami się zbroją
    * OFERTA: standardowa oferta terenowa, plotki itp.
    * KTOŚ: Elsa Kułak (przełożona, trzyma twardą ręką), Antoni Czuwram (górnik, solidny stateczek i niezły sprzęt - dużo wie), Kamil Kantor (szuka ojca Leona który odkrył sekret Mirnas i zaginął), Bruno Baran (mag eternijski i założyciel; Strachy to destabilizacja Blakvela przy obecności lapizytu), Kara Szamun (młódka z rodziny Szamun, pilot i nawigator transportowca na Asimear.)
* Przemytnicy Blakvela: 
    * CZEGO: zdobyć prymat w Domenie Ukojenia
    * JAK: zmiażdżyć Mardiusa, potem przejąć Talio i zestrzelić bazę Barana.
    * OFERTA: świetne rozeznanie w pasie, największa siła ognia, wiedza o SWOICH skrytkach, wsparcie piratów etnomempleksu Fareil (synthesis oathbound pro-virt, pro-numerologia)
    * KTOŚ: Ernest Blakvel (arystokrata eternijski)
* Przemytnicy Mardiusa: 
    * CZEGO: zregenerować siły, odzyskać Tamarę, ukryć się przed Blakvelem
    * JAK: w ciągłej defensywie, ufortyfikowanie Sarnin, operacje militarne anty-Blakvel
    * OFERTA: wiedza o Miragencie i Aleksandrii, świetne rozeznanie w pasie, wiedza o SWOICH skrytkach, znajomości wśród lokalnych.
    * KTOŚ: Tamara Mardius (eks-komandos noctis, Alexandria), Deneb Ira (regeneruje komandosów po sprawie z Aleksandrią)
* Taliaci: (mieszkańcy Talio)
    * CZEGO: rzeczy wysokotechnologiczne
    * JAK: handel, współpraca
    * OFERTA: mają dostęp do WSZYSTKICH frakcji i są wiecznie neutralni, reaktory termojądrowe na podstawie deuteru i trytu na Talio.
* Strachy
    * CZEGO: Brak woli. Reakcja. Zniszczyć wszystko.
    * JAK: flota inwazyjna
    * OFERTA: zniszczenie, targetowane lub nie.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Ponad miesiąc minął odkąd Królową odkryto i podreperowano. Od tej pory Królowa pełniła rolę statku górniczego. Kiepsko.

* Nie ma dobrych sensorów na to
* Nie ma lasera górniczego i nie da się zamontować dobrego lasera bo reaktor
* Ma cargo, ale na 'średni transportowiec'. Więc na szybko jej wsadzili tam kilka osób.
    * Poza PŁG i Sewerynem pojawiło się jeszcze kilka osób. Żołnierze Blakvela i górnicy.

I Królowa, nie w pełni jeszcze sprawna, poleciała górniczyć. Tzn. jest transportowcem do którego wsadza się rudę. Żadna praca nie hańbi, zwłaszcza, że Miranda tego nie robi ;-).

Królowa leci przez Planetoidy Kazimierza do obszaru, gdzie prospektor powiedział, że będą wartościowe rzeczy. Na pokładzie załoga jest odpowiednio znudzona.

* Mikołaj (górnik): "Jak się nazywały te planetoidy, sorki, zapomniałem?"
* Kazimierz (żołnierz): "Planetoidy Kazimierza".
* Mikołaj: A jak ty się nazywałeś?
* Kazimerz: (zimne spojrzenie) Kazimierz Zamglis. Górniku.
* Mikołaj: To Ty tu rządzisz, nie? Przywykłeś?
* Kazimierz: lepiej wróc do swoich. Naprawdę, przyda Ci się to.
* Mikołaj: (z uśmiechem) Tak jest. 
* Kazimierze: (odprowadza go spojrzeniem, po czym wraca do czytania. Czyta jakiegoś erotyka.)

W innym miejscu: 

* Bartek (górnik): Co... co tam _jest_? (z obrzydzeniem)
* Gotard: papka dla pięknej damy. Jej posiłek.
* Bartek: WTF? Co to za statek?
* Gotard: najlepszy jaki mamy, gwarantuję Ci to (z uśmiechem). Ale nie martw się, nie żywi się ludźmi.
* Bartek: i ci Blakvelowcy o tym WIEDZIELI?
* Gotard: najwyraźniej (nie tracąc optymizmu). Dostarczyli nam papkę. Lubię mówić o niej 'kleik'.
* Bartek: (nie ma słów) To śmierdzi...
* Gotard: no ja bym tego nie jadł, nie muszę też wąchać.
* Bartek: i wszyscy... wszystkim to pasuje? Lecimy na jakiejś żarłocznej jednostce?
* Gotard: słuchaj, Królowa jest jaka jest, ale działa i wiernie służy. Gdy działa (poprawił się)

Dotarliście do miejsca, gdzie prospektorzy coś znaleźli. Od razu Prokop przejmuje dowodzenie.

* P: "Dobrze, zatrzymujemy Królową, rozstawiamy sensory zewnętrzne..."
* Grzegorz: "Moment, kto umarł i zrobić Cię dowódcą?"
* Prokop: "To mój statek?"
* G: "Blakvel zawsze dowodzi?"
* Bartek: "Grzesiek, kurde, znowu się chcesz wymigać od roboty?"
* Grzegorz: "Nie. Nie widzę powodu, czemu ON ma dowodzić. On miał nas dowieźć, za co dostanie dolę."
* Prokop: "Chwila, tak to nie działa. Sam jestem górnikiem. My w troje jesteśmy górnikami."
* Juliusz (żołnierz): "Słuchajcie, naprawdę, nie obchodzi mnie JAK to rozwiążecie. Mam was chronić. Jak chodzi o mnie, mam was w dupie. (z takim lekceważeniem)"
* Kazimierz: "Grzegorz, pamiętasz, jak ostatnio oszukałeś mnie na 10 kredytek?"
* Grzegorz: "Nie oszukałem, nie doprecyzowaliśmy warunków!"
* Kazimierz: "Prokop dowodzi. Rozstawiaj."
* Juliusz: "Z tak niskich pobudek?"
* Kazimierz: "Z tak niskich pobudek. Nie chcę tu siedzieć za długo. Nie chcę, by marnowali czas na kłóceniu się."

Górnicy się kłócą, ale rozstawiają, wszyscy, co jest zabawne. 

* Grzesiek zajadle kłóci się o marżę. "jeśli oni mają prawo harvestować, to jak to wpłynie na pieniądze?" 
* Prokop "mamy długi i musimy płacić za ochronę"
* Gotard: "czemu wasza dwójka dostaje 30% całej kasy? Przed czym chcecie nas tu chronić?"
* Juliusz: "30% na dwóch? Blakvel sobie nieźle policzył"
* Kazimierz: "mniej gadania, górnicy, więcej górniczenia. Lubię ciszę radiową podczas operacji wojskowych."
* Gotard: "Jakiej operacji wojskowej? Rozstawiamy sensory i drony, szukamy minerałów!"
* Kazimierz: "jeśli ja i Juliusz tu jesteśmy, jest to operacja wojskowa, zrozumiałeś? A teraz się zamknij. Jestem zajęty." (i czyta dalej erotyka)
* Juliusz: (pokręcił głową i się uśmiechnął, ale aprobuje ruchy Kazimierza)
* Grzegorz: "30% na dwa darmozjady!"
* Kazimierz: "5% Twojego urobku idzie do mnie. Jeszcze jedno słowo, a 10%."

Królowa zrobiła blipa na sensorach. By sprawdzić, jak żołnierze działają. Czy się znają, czy to totalny plebs, czy im zależy, jaki ich cel...

Tr +3:

* X: Niższe zaufanie wszystkich do sensorów Królowej.
    * J: Kto jest mechanikiem tego gówna?
    * G: Tylko nie gówna, Królowa to solidna jednostka.
    * J: Masz to miesiąc. Z czego większość w naprawie. Masz z tego długi.
    * G: Wierzę, że dzięki Królowej osiągniemy wielkość. I wiesz, Blakvel nie dał nam dobrego sprzętu
    * K: Jeszcze jedno słowo o Blakvelu i będziesz wpierdalał tą papkę...
* Vr: Żołnierze są w dobrej pozycji bojowej, asekurują, przygotowali wyrzutnie... może jest ich dwóch, ale wiedzą co robią.
    * Czyli nie są tylko na pokaz
    * Acz nie spodziewają się niczego, żadnego ryzyka itp

Królowa wykrywa swoimi sensorami, że w jednym fragmencie rozstawianej pajęczynie sensorów jest przegrzanie. Zaraz coś wybuchnie. Malfunction sprzętu. Królowa szybko pinguje Gotarda. Mapka - ping - przegrzanie.

* Gotard: "Bartek przesuń się! To wybuchnie!"
* Bartek: (szybko się przesuwa)
* (coś się usmażyło)
* Bartek: "Kurde..."
* Juliusz, filozoficznie: "Pójdzie wam z pensji. Sprzęt Blakvela jest niezawodny."
* Bartek: (chciał coś powiedzieć, widząc miny Juliusza i Kazimierza zmilczał)
* Seweryn -> Gotard: Niby Ceres, ale dobrze działa. Acz nie wiem CZEMU.
* Gotard -> Seweryn: Bo nas lubi. Bo szanujemy ją.
* Seweryn -> Gotard: Powiedz to swojemu kolesiowi z logistyki który rozstawia świeczki i modli się by zły duch statku go nie pożarł
* Gotard -> Seweryn: słuchaj, Łucjan ma odchyły, ale to jest fachowiec. On po prostu boi się mięsożernego statku.
* Seweryn -> Gotard: Królowa jest mięsożerna? Ta papka jest na mięsie?
* Gotard -> Seweryn: Nawet my nie jemy mięsa. Nie sądzę.

.

Następne 3 godziny mniej więcej tak wyglądają. Rozstawianie sieci, wysyłanie dron, frakcje i zadowolenie. A Królowa monitoruje i pilnuje by nikomu nic się nie stało. A żołnierze przeszli w 'spocznij'. Kazimierz wraca do erotyków, Juliusz szuka okazji, by dawać ludziom demerity by móc potrącić im trochę kasy.

Zgodnie z danymi prospektora, nie ma tu dużo Strachów. Praktycznie wcale ich nie ma. Jest to nieczęste, ale są takie miejsca. I w końcu - trafienie. Po kolejnych 6-7 godzinach odwiertów i poszukiwań udało się znaleźć bardzo bogatą planetoidę. Grzesiek od razu zaczął narzekać, że czemu 30% a tak w ogóle...

Królowa zbliżyła się do odpowiedniej planetoidy. Spory byt - paręset metrów promienia. I są miejsca, gdzie są lepsze potencjalne i gorsze potencjalne. Z pomocą Królowej udaje się znowu rozłożyć sieć, rozłożyć drony i górnicy lecą robić odwierty, testy itp. Królowa dostaje pierwsze odczyty. Widzi minę Łucjana.

* Łucjan: mam złe przeczucia.
* Prokop: Łucjan, Ty ZAWSZE masz złe przeczucia. (statystycznie jest to prawda)
* Łucjan: popatrz na te odczyty.
* Prokop: Łucjan, NIGDY nie widziałem tak dobrych odczytów, nie w tej fazie
* Łucjan: właśnie. Za dobrze, za dobre minerały, za drogo.
* Prokop: Łucjan, 30% dla Blakvela, swoja dola dla górników... nie jest tak dobrze jak Ci się wydaje.
* Łucjan: jeśli odczyty są prawidłowe, będziemy tu najwyżej... 4-5 dni. Nie dwa tygodnie. Ale nie rozumiem niektórych odczytów.
* Prokop: Łucjan... (cicho) mamy sprzęt Blakvela. Ja się założę że połowa tych odczytów tak... TROCHĘ działa. Popatrz na przykład na te linie spektralne. One nie mają sensu.

Królowa robi własną analizę. Analizę w oparciu o swoją niewielką wiedzę górniczą. Ale o wszystko co wie. Zwłaszcza w obszarze zagrożeń - niech to im się uda.

Ex Z (bo possała dane i ma wiedzę o magii) +2:

* X: (p)
    * Seweryn: "Ceres! CERES! RESET! Ja pierniczę, zawiesiła się..."
    * Gotard: "Weź ją dokarm."
    * Seweryn: (macha rękoma z niedowierzaniem) "To nie jest PIES"
    * Gotard: "No dokarm ją. Może pomoże."
    * (żołnierze patrzą na siebie i się uśmiechają)
* X: (m) większość osób po prostu nie UFA Królowej. Słaby sprzęt, słabe sensory, TAI Ceres ze wszystkich rzeczy i wpierdala 'kleik'... 

Analizując różne parametry nagle Królowa się wyrwała sama. Coś jej nie pasuje na sensorach. Nie wie DOKŁADNIE co jest nie tak, ale coś JEST nie tak. To 'intuicja'. Coś jest nie tak. Szybko mignęła Łucjanowi komunikatem 'siła sygnału 100% jakość 0%'.

* Łucjan: Gotard, przekieruj mi sensory (dał koordynaty i parametry)
* Gotard: Łucjan... co?
* Łucjan: I przekieruj do mnie sygnał

Tr Z (bo Łucjan) +3:

* V: (p) Miranda ma to czego szukała
    * to 'niepokojące' pochodzi z obszaru gdzie nie powinno być górników. Ale jest tam Grzesiek. Zmienił miejsce gdzie powinien pracować. (najpewniej znalazł lepszy i nie chciał się dzielić)
    * Łucjan - mimo że powinien coś zauważyć - nic nie zauważył.
    * Łucjan: "dobra, fałszywy alarm, Grzesiek poszedł gdzieś indziej zbierać."
    * Gotard: "a to gnida (ze śmiechem), i tak Blakvelowcy mu wezmą"
* Vz: (m) Miranda ma podwójny sygnał transpondera Grześka. To NIE jest możliwe.
    * Jeden jest "tam przy tamtym miejscu", drugi się przemieszcza w jego oryginalne miejsce.
    * Łucjan tego nie widzi. Miranda też tego nie widziała. W tym sygnale COŚ jest.
* V: (e) Miranda miałaby ciarki jakby miała czym. To jest jakiś sygnał memetyczny?, ale nie do końca. W tym sygnale jest wpisane 'nie zauważasz'. Ale teraz Miranda to widzi. Czyli teraz Miranda widzi to co jest.

Miranda włącza high priority alert. Podwójny transponder. Coś niemożliwego. Grzesiek.

* Łucjan: coś jest nie tak.
* Prokop: Grzesiek! Zmieniłeś pozycję! Znalazłeś tam bogactwo?!
* Kazimierz: ...nie wiem co temu TAI odpierdala, ja widzę JEDEN transponder i DLACZEGO JESTEŚ NIE TAM GDZIE POWINIENEŚ?
* Juliusz: 5% więcej dla Blakvela. Jak mam Cię chronić...
* Bartek, cicho: ...wcale nie chronisz...
* Grzesiek: (dziwny, nienaturalny dźwięk, jak przesunięty w fazie)
* Juliusz: Dobra dobra, przestań pieprzyć tylko pracuj. I nawet nie próbuj zataić co tam się dzieje.

Miranda próbuje to zdekodować. Wiedząc zarówno o sygnale dziwnym z rudy jak i o tym co tam się dzieje.

Ex Z (zbiór danych + wiedza o której Miranda nie wie że ją ma) +3:

* X: (p) Mirandzie zajmuje to więcej czasu niż by chciała
* X: (m) Miranda 'wpada w ciąg' analizy (co jej normalnie się nie zdarza) i się 'zawiesza' z perspektywy Seweryna
    * Seweryn: "ja pierniczę, czujniki nie działają i się zawiesza"
    * Gotard: "wiesz, pracuje z tym co ma..."
    * Seweryn: "znaleźliście _dziwną_ jednostkę"
    * Gotard: "normalnie tak się nie zachowuje"
    * Seweryn: "nie wiesz co jest normalne po miesiącu."
    * Gotard: ".."
    * Juliusz: "dobra, WYŁĄCZ ten komunikat alarmowy, to wyraźnie false alarm. Czesiek, idź do Grześka, zobacz co on tam ma..."
* X: (e) Seweryn wyłączył alarm; potraktował Mirandę jak Ceres. Miranda jest przeładowana danymi. Co więcej - jest też Dotknięta anomalią; ruch Seweryna pomógł.
    * Miranda się otrząsa
    * Miranda widzi zdecydowany ślad anomalii Altarient i to takiej która 'uniemożliwia wykrycie'
        * Miranda JEST W STANIE im to uzmysłowić, ale to najpewniej ją ujawni.
        * Miranda WIDZI rzeczywistość taką jaką ona jest.
    * sygnał był 'hipnotyczny': odbiorca-słyszy-to-czego-oczekuje, to jest maskowanie

Górnicy jeszcze robią robotę. Anomalny sygnał dochodzi z konkretnego miejsca na planetoidzie - tam, gdzie jest "podwójny transponder". Czyli Grzesiek coś musiał ruszyć. To jest po drugiej stronie planetoidy - jakby Miranda mogła tam spojrzeć...

Miranda ma przechlapane, ALE. Jest tam Łucjan. Koleś paranoiczny. Nie ufa nikomu i niczemu. Miranda, rozmawiając z Łucjanem zacznie wplatać rzeczy głosem Ceres. Tak, by Łucjan uznał, że coś jest nie tak. Miranda wysyła do WSZYSTKICH komunikat X X X X X a do Łucjana X X Y X X X X. I Łucjan 'słyszycie to?' a wszyscy 'wtf?' i Łucjan zaczyna myśleć i wątpić. Miranda to złamała, bo miała wątpliwości i to wydedukowała. Może z Łucjanem też się tak uda :D. Miranda mimikuje zachowanie anomalii ale wprowadza to tak, by Łucjan mógł wydedukować i zobaczył rzeczywistość-która-nie-jest-tym-czym-jest.

Ex (P: paranoja Łucjana i Łucjan ma od początku złe przeczucia, P: Miranda DOKŁADNIE wie co robi i jak dojść do tego, P: Łucjan jest bardzo bardzo myślący)...

Tr Z +3:

* X: (p) Łucjan wątpi w to, że dobrze to interpretuje. Zajmuje to dużo więcej czasu niż mieliśmy nadzieję.
* X: (p)
    * Czesiek: "Juliuszu, nie uwierzysz" (normalny głos). "Tu są naprawdę fajne minerały."
    * Grzesiek: "<DŹWIĘK>"
    * Czesiek: "Musimy się podzielić. Nie ma 'pierwszy znalazłem'."
    * Grzesiek: "<DŹWIĘK>"
    * Juliusz: "Dobra, skupiamy się w tamtym kierunku"
    * Prokop: "Moment, ale będzie daleko do Królowej"
    * Juliusz: "To przesuniemy Królową..? (rezygnacja)"
    * Prokop: "Łucjan, Gotard, idziemy przesunąć Królową - i ostrożnie z siecią czujników".
* V: Łucjan. "O nie..." (ZROZUMIAŁ)
    * Łucjan: "Prokop, Gotard, to jakaś anomalia."
    * Prokop: "Łucjan, mówiłem Ci... czasem jest trochę szczęścia"
    * Łucjan: "Nie rozumiesz, to anomalia która wpływa na poznanie."
    * Prokop: "Dobrze, porozmawiamy o tym jak przesuniemy statek."
* Xz: Kazimierz może gardzi ludźmi, ale nie zostawi nikogo kogo nie musi. A on nie postrzega Grześka jako Skażonego. I - przypominam - nie wierzy czujnikom Mirandy/Królowej.
* V: Miranda może mówić do Łucjana. Łucjan będzie uważał, że to część jego psyche, która zwalcza anomalię Altarient.

(Miranda wie już, że musi ostro poczyścić logi po tej akcji XD)

Miranda podpowiada, że można zrobić alarm zderzeniowy - Królowa się będzie przesuwać i Was zostawi. "get back or stay". Łucjan przyjął jako potencjalny plan.

* Łucjan: "Musimy przekonać Prokopa i Gotarda i Seweryna. Naszą załogę. I Blakvelowych żołnierzy."
* Miranda: "Oni nam nie wierzą"
* Łucjan: "Jeśli mi się udało to wydedukować, da się zrobić coś by zrozumieli. Nie mam pomysłu jak. Ale możemy ściągnąć jedną osobę do Królowej wysyłając jej alarm że coś jest nie tak. Gotard może to sfabrykować."

.

* Vr: Łucjan i Miranda prowadzą serię przykładów, dowodów, linii dla Gotarda. Gotard 'nie widzi' anomalii, ale WIERZY że tam jest anomalia, choćby z uwagi na za dużą ilość dziwnych rzeczy. I ufa Łucjanowi.
    * Gotard jest OTWARTY i jest skłonny zaufać Łucjanowi. I Miranda mówi do niego 'jak psyche'. To znaczy, że Gotard UWIERZYŁ. I MA DOWODY. To złamało błędny sygnał Anomalii.

W czasie jak Gotard i Łucjan dochodzili do prawdy a Prokop zirytowany nic nie czaił, Królowa - zgodnie z poleceniem - przesunęła się na drugą stronę planetoidy. Miranda zobaczyła obraz sytuacji.

* W miejscu, w którym powinien być Grzegorz nikogo nie ma.
* Tam, gdzie Grzegorz się przesunął jest jaskinia w którą Grzegorz się wkopał. Tam są... metalizowane jaja?
* Jest tam też ciało Grzegorza. It's dead. To jeden transponder.
* Z grupą stoi nienaturalnie wydłużony i metalizowany Grzegorz. Z pleców wyrasta mu jajo, które zostało potraktowane energią z lasera górniczego. Pozostałe jaja są nieaktywne. Na oko.
* Oni wszyscy rozmawiają o tym, że trzeba te 'minerały' zabrać na Królową. Bo odkryli samorodki czy coś.
    * Łucjan jest biały. Gotard też. Prokop nie rozumie (bo nie widzi prawdy).

Miranda stwierdziła 'fuck this shit'. Ma jedno działko. Kiepskie. Wycelowała w NieGrzegorza i otworzyła ogień. Wiedząc o sytuacji, czeka spokojnie. Zwłaszcza, gdy NieGrzegorz wziął jedno z jaj by pokazać je z bliska Juliuszowi. Zanim dojdzie z jajem do Juliusza - OGNIA.

Tr +3:

* V: działko, jakkolwiek stare i mało celne, przebiło się przez NieGrzegorza i zadało mu koszmarne obrażenia. Byt został odrzucony i odepchnięty. Ale nie jest jeszcze zniszczony.
    * reakcja wszystkich: SZOK. Żołnierze też. Kazimierz stał z boku (zawsze stoi z boku) i on już coś widzi. Ale jeszcze nie wie co.
* X: NieGrzegorz został ranny, ale dał radę się schować / uciec. Królowa nie ma siły ognia na to.
* Vr: zbiorowa iluzja padła. Wszyscy w maksymalnym tempie widząc co tu się odpierdala uciekają na Królową ASAP.
* V: Miranda elegancko sfałszowała logi, pochowała rzeczy, "że to wszystko zaleta załogi" i Królowa SPIEPRZA oznaczając to miejsce jako RED RISK OF DEATH OR WORSE.

Epilog:

* Prospektor dostał wpierdol. Co prawda nie mógł wiele, ale KTOŚ musi zapłacić, więc on.
* Królowa nie została obciążona kosztami. Więcej, dyskretnie pokombinowano z zamontowaniem jej lepszego działka.
* Potwora nie znaleziono, ale z uwagi na dystanse kosmiczne nie stanowi to problemu.

## Streszczenie

Naprawiona Królowa Przygód została skierowana jako kiepski statek górniczy przez Blakvelowców. Niestety, planetoida która miała być wartościowa miała 'gniazdo' nieaktywnych Alterientów i jeden z górników został Zmieniony. Miranda kombinowała, jak uzmysłowić to ludziom niezdolnym do postrzegania zagrożenia i znalazła swoją drogę przez paranoicznego Łucjana. Potem - widząc NieGórnika - rozstrzelała go jedynym sprawnym działkiem i zasługi skromnie oddała załodze. Jeszcze jedna misja, która się udała bez strat załogi...

## Progresja

* .

## Zasługi

* Miranda Ceres: na typowej operacji górniczej ratowała załogę przed kiepskim sprzętem i awariami udając Ceres. Gdy pojawiła się anomalia Alteris to najpierw wylogikowała z czym ma do czynienia, potem użyła paranoi Łucjana by on też do tego doszedł a potem udawała że jest psyche ludzi i gadała do nich bezpośrednio. Na końcu - zestrzeliła NieGórnika i pokasowała odpowiednio logi.
* SC Królowa Przygód: dość naprawiona, ma pojedyncze stare działko, nie ma możliwości podmontowania lasera górniczego, ma kiepskie sensory itp. Zestrzeliła NieGrzegorza na misji górniczej.
* Prokop Umarkon: nieskończenie optymistyczny, co prawie zgubiło zespół. Blakvelowcy kazali mu robić operacje górnicze, przyjął na klatę, ale do końca nie widział potwora Alteris. Na szczęście ufa swojej załodze.
* Łucjan Torwold: MVP sesji. Jego paranoja i nieufność doprowadziły do tego, że był w stanie zobaczyć potwora Alteris i Miranda mogła do niego dotrzeć. Oczywiście, przez tą samą paranoję nikt mu nie wierzył.
* Gotard Kicjusz: niezależnie od okoliczności bronił Królowej że nie jest złym statkiem. Odpowiednio przesądny, uważa, że Królowa 'ich lubi'. Jego otwartość sprawiła, że zaufał Łucjanowi, że jest potwór Alteris, choć go nie umiał najpierw zobaczyć.
* Seweryn Grzęźlik: narzeka na Ceres. Bo się zawiesza, bo coś nie działa, ogólnie - wyraźnie nie chce być na tej jednostce. Ale dzięki swojej kompetencji wyprowadził Mirandę z pętli po zobaczeniu potwora Alteris.
* Grzegorz Fabutownik: górnik Blakvelowców. Narzeka na kasę, łasy na kasę, pragnie kasy dla siebie, antagonizuje wszystkich innych i jak widzi okazję na łatwy zarobek, bierze dla siebie. Napotkał jajo Alterienta i na nim się wykluło. KIA.
* Mikołaj Resztkowiec: lubi zaczynać dramę i antagonizować np. Kazimierza. Poza tym - wystarczająco kompetentny i nic wielkiego nie zrobił.
* Bartek Burbundow: nie jest fanem Blakvelowców, nie uważa, że powinno się tyle się im płacić za ochronę. Zwolennik czystości - bardzo obrzydzony tym, że Królową trzeba karmić organiczną papką. Jednocześnie - uczciwy górnik.
* Juliusz Cieślawok: żołnierz Blakvelowców, który chciałby jak największej ilości ludzi zabrać kasę i ich pociemiężyć. Dowodzi operacją bo nikt inny (Kazimierz) nie chce. Gdyby nie Miranda, zostałby Zmieniony przez Alteris.
* Kazimierz Zamglis: czytający erotyki żołnierz Blakvelowców, gardzący górnikami których ma chronić, ale kompetentny i nie jest sadystyczny. Łatwo się irytuje i tnie kasę górników, ale jeśli wszystko działa prawidłowo to się nie przypieprza.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Pas Teliriański
                1. Planetoidy Kazimierza
                    1. Domena Ukojenia

## Czas

* Opóźnienie: 44
* Dni: 3

## OTHER
