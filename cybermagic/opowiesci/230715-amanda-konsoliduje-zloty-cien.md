## Metadane

* title: "Amanda konsoliduje Złoty Cień"
* threads: pronoktianska-mafia-kajrata
* motives: wojna-domowa, integracja-rozbitej-frakcji, magowie-eksploatuja-ludzi, wyscig-o-zasoby, beznadziejny-szef
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [230708 - Wojna w złotym Cieniu](230708-wojna-w-zlotym-cieniu)
* [230711 - Zablokowana sentisieć w krainie makaronu](230711-zablokowana-sentisiec-w-krainie-makaronu)

### Chronologiczna

* [230708 - Wojna w złotym Cieniu](230708-wojna-w-zlotym-cieniu)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ?
* Struktura: 
    * integracja-rozbitej-frakcji
* CEL: 
    * MG
    * Drużynowy
        * Przejąć kontrolę nad Złotym Cieniem dla Kajrata

### Co się stało i co wiemy

* Czym jest Grupa? Czemu jej istnieje jest wartościowe?
    * Złoty Cień Karmazynowego Świtu - przestępcza grupa concierge pomagająca dominującemu rodowi Samszarów w tym czego chcą w tym luksusowym miejscu
* Dlaczego Grupa została bez głowy / dowodzenia?
    * Szefostwo zostało wyrżnięte po aferze z Mają.
* Jakie są 3-4 Kluczowe Zasoby należące do Grupy?
    * Lista Samszarów z którymi da się współpracować i którzy są zainteresowani tego typu rzeczami, też znajomości
    * Legalne biznesy wspierające ZCKŚ finansowo
    * Klinika bioformacji Oteriiel w niedalekim Płomienniku
    * Magazyny dóbr wszelakich, też egzotycznych
    * Grupy wyspecjalizowane w pozyskiwaniu odpowiednich osób i przerzucie ludzi
* Jakie są 2-3 siły z różnymi Agendami w ramach Grupy?
    * **Lojaliści**: chcą utrzymywać współpracę z Samszarami jak zawsze
        * impuls: integracja
        * mają: biznesy, magazyny, większość siły ognia, morale, Samszarów
        * słabość: chaos, brak dowodzenia
    * **Bioformaci**: chcą się skupić na Oteriiel i stanowić niezależny byt, acz dalej współpracujący
        * impuls: fortyfikacja i negocjacja
        * mają: morale, wiedzę, Płomiennik
        * słabość: łańcuch dostaw, mało pieniędzy
    * **Secesjoniści**: mają dość tego że są zabawką w rękach Samszarów i ich dowodzenie może umrzeć i to nie z jej winy
        * impuls: grab and run
        * mają: część biznesów, siłę ognia
        * słabość: niska konsolidacja, oportunizm
* Jakie są 3-5 pomniejsze siły w ramach Grupy?
    * **Łowcy**: distant agents; pozyskują wszystko czego Samszarowie sobie życzą. Teraz nie wiedzą czy nie lepiej znaleźć lepszy sojusz inaczej
        * impuls: find allies, mercenary
        * mają: specjalistyczne grupy
        * słabość: rozproszeni, daleko
    * **Przemytnicy**: czemu nie zarobić WIĘCEJ?
        * impuls: pieniądze
        * mają: magazyny, środki, siłę ognia, tajne aukcje
        * słabość: oportunizm
    * **Ekstatycy**: eksperci od przyjemności i luksusu, z przyjemnością znajdą lepsze warunki gdzieś indziej
        * impuls: pieniądze, okazje do badań
        * mają: wiedzę, kompetencję, część winorośli, narkotyki
        * słabość: niska siła ognia
    
### Co się stanie (what will happen)

* F2: Obrona swojej subfrakcji przed innymi subfrakcjami (oraz siłami zewnętrznymi), 'stop the bleeding', ustabilizowanie swojej roli dowódczej
    * 8 noktian przejmuje 15 napastników
    * działania z Wacławem Samszarem
    * przybycie Emila
* F3: "Fire and motion" - zajmowanie innych subfrakcji i ważnych zasobów zanim zrobią to inni; wojna o subfrakcje
    * konieczność pracy nad Wacławem by przekonał innych Samszarów
    * w tle - Amanda zajmuje ważne miejsca i działania korzystając z chaosu (middle manager)
* F4: Konfrontacja z głównymi rywalami o kluczowe zasoby
* F5: Ostateczna Konsolidacja - które zasoby zdążymy utrzymać, które subfrakcje zintegrować? Co zatrzymamy zanim zabezpieczymy okno kradzieży i ucieczki?
* INNE: nielegalne wyścigi, podziemne walki, czarny rynek, orgie

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Wacław Samszar
    * OCEAN: (E+C-): ekscentryczny i arogancki, lubiący być w centrum uwagi i impulsywny. "Bawmy się i korzystajmy z życia, konsekwencje naprawi sentisieć. Nie po to jestem tienem by się pieprzyć."
    * VALS: (Power, Hedonism): ceni sobie status i przyjemności wszelakie. "Życie to zabawa! Mam prawo bawić się i cieszyć się jak chcę!"
    * Core Wound - Lie: "Zawsze byłem traktowany jak nieudacznik z powodu braku bystrości" - "Skoro tak, wezmę co mogę używając pozycji i kasy ORAZ pomogę jak mogę tym, co są warci i mnie lubią."
    * styl: lekko niepewny siebie, ale arogancki mag żądający pokory i docenienia. Ale jeśli się zdobędzie jego przyjaźń, zostanie lojalny.
* Tadeusz Samszar
    * OCEAN: (C+A-): pozornie niezniszczalny, bezwzględny, kontrolujący. "Moje zasady, moja kontrola. Działasz jak sobie życzę lub lecisz. Każdego da się zastąpić."
    * VALS: (Power, Achievement): uwielbia wyzwania, pokonywanie przeciwności. "Każda organizacja może zostać przebudowana w coś wartościowego. Pytanie ile tłuszczu usuniesz."
    * styl: bezkompromisowy dyktator, dyrektywny, traktuje innych jak śmieci.
* Emil Samszar
    * OCEAN: (O+N+): Inteligentny i ma ogromne zainteresowania, martwi się o potencjalne problemy. "Muszę zrozumieć i przygotować wszystko, zanim podejmę decyzję."
    * VALS: (Self-Direction, Universalism): Ceni prawdę i odkrycia ponad wszystko inne. "Nauka, rozwój oraz Aurum to klucz do poprawy świata."
    * Core Wound - Lie: "Nazwali mnie ekstremistą i odebrali mi możliwości badań" - "Udowodnię, że moje badania mogą zmienić świat, nawet jeśli muszę to zrobić sam."
    * Styl: Lekko zagubiony i niespokojny, ale świetny w tym co robi; zawsze głodny wiedzy. "Na przekór, nauka musi iść naprzód. Pojmiemy niewyobrażalne, choćby miał to być nasz ostatni oddech."
* Bella Samszar
    * OCEAN: (E+O+): towarzyska, bardzo spontaniczna, uwielbia nowe pomysły i wymyślać rzeczy. "TEGO jeszcze nikt nie wymyślił!"
    * VALS: (Hedonism, Stimulation): dobre jedzenie, towarzystwo, świetne imprezy i niedomiar surowców. "Niech się dzieje, człowieku!"
    * Core Wound - Lie: "zostałam sama w rodzie po śmierci rodziny" - "jeśli będę epicentrum dobrej zabawy, nikt mnie nigdy nie opuści"
    * styl: żywiołowa, zawsze uśmiechnięta i pomocna ze świetnymi pomysłami, grace and charm. "_Bawmy się tak, jakby jutro wszyscy mieli umrzeć!_"
* Kasimir Esilin
    * OCEAN: (E-C+O-): zamknięty w sobie i poważny, świetnie planuje, myśli tylko o misji, dość formulaiczny. "Nie wiem o co im chodzi, ale wiem, że z nimi wygram."
    * VALS: (Security, Conformity): przewidywalność i bezpieczeństwo. Nie wychylanie się. Minimalizacja niespodziewanego. "Dyscyplina, porządek, planowanie, ochrona."
    * Core Wound - Lie: "byłem żołnierzem elitarnej jednostki, wywalili mnie jak śmiecia bo poznałem prawdę" - "ochronię wszystkich innych i zreintegruję ich w imię Nocnego Nieba"
    * styl: twardy wojownik z dobrym sercem, który wykona zadanie ale spróbuje zmniejszyć ilość strat. "Bezpieczeństwo wymaga ciągłej walki."

### Scena Zero - impl

.

### Sesja Właściwa - impl

Amanda dostała wsparcie od Kajrata. Kasimir Esilin. Starszy wojownik, z tych cichych i mało odkrywczych na polu bitwy, ale umie się zachować. On dowodzi 7 innymi noktiańskimi żołnierzami. Amanda aż się zdziwiła z tak dużego wsparcia. Amanda wie, że dostanie Wacława i wie, że w jej pułapkę wpadnie jakaś grupka. Mając wsparcie Kasimira Amanda ma możliwość uderzać lub się bronić. A Lojaliści się biją z Secesjonistami. Dodaj fakt, że w okolicy poza Wacławem jest jeszcze TRZECH Samszarów. Którzy chcą naprawić sprawę. Po kolei - Amanda chce, by Kasimir przejął tych co zaatakują. Bilgemener nie ma dużej siły ognia, więc się przyda jakakolwiek :-).

Skoro Lojaliści się kłócą z Secesjonistami, Amanda chce Bioformatów. Amanda nie musi ich przejąć - chce nawiązać z nimi kontakt. Ale do tego pomoże Wacław. Więc... Amanda czeka na ruch Kasimira i ma nadzieję, że Wacław szybko się pojawi - nie ma legitymizacji. I jest jedynym sposobem na poznanie planów pozostałej trójki.

Kasimir obstawił magazyn i przygotował pułapkę. (+zaskoczenie, +swój teren, +wyszkolenia -> Tp Z +3 +3Og):

* V (p): Przemytnicy wpadają, zaczynają szukać rzeczy - nagle światło i siły Kasimira, w moro z karabinami. I Kasimir 'gratuluję, a teraz porozmawiajmy'
* V (m): Przemytnicy są Złamani. 
* V (e): Kasimir przejął tych Przemytników bez walki i skłonił ich do oddania Amandzie ich magazynów. Amanda wzbogaciła się o kasę i 12 osób.
* Vz: (p) Bilgemener zrobił propagandę i pokazał, że faktycznie on próbuje przejąć to wszystko, ustabilizować. Nowy don, nowe źródło stabilności.
* V: (m): Sporo mniejszych grupek, takich 2-3 osobywych czy pomniejszych przemytników widząc coraz gorętsze walki zastanawia się, czy nie warto dać szansy Bilgemenerowi
* Vr: (e): Nawet magowie - zwłaszcza Tadeusz - uznają działania Bilgemenera za sensowne.

Bilgemener jest zarówno pod wrażeniem jak i przerażony. On wie, że tego by nie zrobił. Zaczyna się orientować, że WTF i potrzebuje Amandy. Równolegle Amanda kazała Bilgemenerowi dowiedzieć się czegoś o tych trzech Samszarach - kim są, jakie dokonania i czego chcą. Bilgemener _is on it_.

Amanda rozważa _evening prowl_, pod przykrywką Lojalistów oraz Secesjonistów. Jedni 'z nami lub przeciw nam', inni 'grab & run'. Dzięki Bilgemenerowi Amanda wie w co celować. Tu magazyn ostrzelać, kogoś zajść od tyłu i zastraszyć i zabrać mu coś wartościowego (lub zagrozić rodzinie). Amanda ma pracowitą noc.

Tr Z +3:

* X (p): Wacław dotrze do Bilgemenera gdy Amandy tam nie ma.
* X (m): Wacław NIE UFA Amandzie i noktianom. Nie kontroluje ich więc im nie ufa.
* V (p): Amandzie udało się przeprowadzić operację, w wyniku czego kilka osób dołączy do Bilgemenera.
* Vz (m): Amandzie udało się grać chaosem na tyle, że Bilgemener dostanie kilkanaście osób (kilku secesjonistów, kilku przemytników, jakiś ekstatyk) z jakimś sprzętem i jakąś wiedzą. Większość niebojowych ale normalni ludzie mafii.
    * Złoty Cień pod kontrolą Bilgemenera może pełnić część swoich usług.

Zmęczona Amanda wróciła do centrali. Od razu dostała wiadomość od Nadii, że 'tien już jest i życzy sobie obecności Amandy asap'. Bilgemener kazał wysłać ale tak, by mógł powiedzieć że nie kazał. Amanda doceniła zarówno dupochron jak i lojalność. Amanda też zwężyła oczy. Karolinus miał to załatwić. Karolinus wyraźnie skupił się na swoich korzyściach. Ale cóż - Amanda może z tym pracować.

Amanda idzie na to spotkanie po 10-minutowym prysznicu. Czysta, żakiecik, żeby nie wyglądać jak prosto z pola bitwy. Plus Wacław nie musi wiedzieć, że Amanda jest militarna. Amanda nie chce być 'subservient'. A czego chce Wacław? (Wacław oczekuje stałego dostępu do ciągle nowych ładnych dziewczyn. Pójdzie w to, bo nagroda > koszt, ale będzie zadawał pytania. Wacław oczekuje też pokory i traktowania). Karolinus przekazał Amandzie, że Wacław oczekuje odpowiedniej pokory, dostarczania ładnych dziewczyn i nie jest szczególnie sprytny - ale pójdzie w plan Amandy, jeśli Amanda będzie "wystarczająco grzeczna".

Ogarnięta Amanda, nie przeciągająca, idzie tam na spotkanie. Wacław czeka z Bilgemenerem.

* W: Pani Amanda.
* A: Tien Samszar /lekki ukłon i uśmiech
* W: Nie widzę Cię w sentisieci.
* A: (między wierszami, że Karolinus i ona mają bliską relację)
* W: Dlatego Karolinus mnie poprosił, bym Ci pomógł. Bo go zadowalasz w łóżku... (pokręcił głową i uśmiech perverta) Co za pies.
* B: (nic nie mówi, analizuje, rejestruje...)
* W: Karolinus obiecał mi sporo ładnych dziewczyn. Co najmniej dwie nowe na tydzień.
* B: Tien Samszar, gdy organizacja się odbuduje, nie będzie to problemem. Będzie to przyjemnością.
* W: Cieszę się że uważamy tak samo. O co mnie pokornie prosicie? (Amanda widzi jak Wacław zmierzył ją wzrokiem)
* A: Tien Samszar, to może być bardzo owocna współpraca dla wszystkich zainteresowanych. Nie tylko w kwestii dziewczyn, ale również w kwestii Twojego statusu w rodzie.
* W: (jest zainteresowany)
* A: Gdybyś był tym, kto ustabilizował sytuację na tym terenie, myślę, że ród zdecydowanie by to docenił (+P). (Amanda zmienia nomenklaturę)
* W: Po co mi do tego ładna noktianka skoro mam Bilgemenera?
* A: Bo mogę zrobić rzeczy, których Bilgemener nie może.
* B: Tien Samszar, ona naprawdę jest dobra. Myślałem, że Ty jesteś jej tienem, nie wiedziałem że Karolinus... ale ona jest dobra.
* W: Nie lubię noktian. Nie ufam Wam.
* A: Jest to zrozumiałe.
* W: Zwłaszcza, jak nie widzę Cię w sieci.
* B: Tien Samszar, ona jest Twoją lojalną agentką. Przez Karolinusa.

(Amanda wie, że on nie ufa noktianom, ale nie wie że za nią stoi organizacja. Pójście logiką w stronę - tien Samszar, jestem noktianką, jak każdy szukam bezpiecznej przystani, Karolinus jest przykładem takiej przystani; przekonać go, że Amanda nie jest dla niego niebezpieczna bo nie ma nic innego do zrobienia jak się z kimś związać - tu z Karolinusem). 

* A: tien Samszar, jestem noktianką, jak każdy szukam bezpiecznej przystani. Mam niewielką grupę przyjaciół, próbuję jakoś przetrwać w tym świecie. I ich da się zobaczyć w sentisieci.
* W: dobra, wracajmy do najważniejszego. Gdzie moje dziewczyny?
* B: tien Samszar, będą jutro...
* W: chcę jakieś dzisiaj. (z naciskiem).
* A: Nadię możesz mieć dzisiaj (zdjęcie, mniej zachęcające). Jutro możesz dostać naprawdę ładne dziewczyny.
* W: mogę poczekać na naprawdę ładną dziewczynę. (mierzy Amandę spojrzeniem). 
* A: Gwarantuję Ci, że nie będzie noktianką.
* W: Mam tendencje do nie współpracowania z ubranymi noktiankami. Jak nie ma lepszej, wejdę w to. Jeśli udowodnisz lojalność i mnie zaspokoisz.
* A: Nie Ty, Karolinus zaproponuje to innemu Samszarowi. Chciał dać Ci szansę (+P)

Ex -> Tr +3:

* X (p): Amanda jeden raz spełni żądania Wacława
* Vr (p): Wacław będzie usatysfakcjonowany jak dostanie co chce RAZ
* V (m): Wacław uważa, że wszystko jest na swoim miejscu
    * Noktianka pyszczyła, ale na rozkaz go zaspokoiła więc wszystko jest na swoim miejscu
    * Wacław czuje się uspokojony, Amanda nie stanowi problemu
* X (m): Wacław pozazdrościł Karolinusowi (z opowieści Amandy)
    * Od teraz Wacław sobie wzywa Amandę gdy chce. She is oficially his pet noctian. W praktyce - Wacław ma ładniejsze, więc po co mu Amanda.
    * Wacław dopierdala Karolinusowi, że odebrał mu noktiankę i on ją udomowił a Karolinus nie umiał.
    * Wacław czasem używa Amandy też do odpowiedzi na niektóre pytania odnośnie "jak zrobić X" by być silniejszym.
    * +2Vg
* X: To stawia Bilgemenerowi Amandę w odpowiednim świetle - nieważne kim jest i co za nią stoi, Amanda ulegnie Samszarowi jak powinno być.
* V: 
    * Wacław jest 'corruptible', czyli Amanda po odpowiednio długim czasie jest w stanie go przekształcić w dobre narzędzie dla Kajrata
    * Inni Samszarowie uwierzą w 'pet noctian'. Bo to wiarygodne i normalne. I to jest zrozumiałe, że Wacław ukrył ją w sentisieci lub że Karolinus ukrył ją dla niego
    * Wacław może chcieć, by Amanda zrobiła dla niego coś co tylko ona umie. A Amanda może tego używać do swoich celów.

Wacław kazał Amandzie się rozebrać by sobie obejrzeć. Skrzywił się na widok jej blizn - nie jest fanem. Trudno. Wywalił Bilgemenera. I Amanda musiała na kolanach go zaspokoić i... to było wszystko. Potem się mogła spokojnie ubrać. Zdaniem Wacława - właśnie została wytresowana. Acz od czasu do czasu trzeba przypominać. Ale nie za często, bo blizny >.>.

Wacław uspokojony i odpowiednio kontrolowany. Złoty Cień odpowiednio aktywny. Amanda potrzebuje od Wacława informacji o tych magach:

* Emil. 41 lat. "stary i nudny". Z Cieniem współpracował. Nieudany naukowiec. Zainteresowany Kliniką Oteriiel - tam coś robią co go interesuje.
* Bella. 24 lat. Hot and sweet. Chce zreformować Cień - mniej eksploatacji ludzi. Zdaniem Wacława - głupota. 
* Tadeusz. 43 lata. "tyran". Chce naprawić i się wycofać. Stabilizacja i spokój, nie dba o mafię. Wszyscy się go boją.

Amanda od razu wpada na pomysł jak to rozwiązać - trzeba uderzyć przez Tadeusza. Bilgemener jako 'don', ona jako asystentka i Wacław jako słup. Tadeusz sam pomoże Wacławowi opanować Złoty Cień i przekaże go pod kontrolę Wacława. Bo to jest w jego interesie. Zwłaszcza, jeśli Tadeusz część osób już spacyfikował.

Spotkanie następnego dnia.

* T: Tien Wacławie, poradziłeś sobie nadzwyczaj dobrze
* W: Dziękuję, tien Tadeuszu. To wszystko zasługa mojego Bilgemenera i Amandy (pat pat po główce)
* T: (zwężone oczy) (nic nie mówi)
* B: (przedstawia statystyki i informuje co dalej)
* T: Skąd miałeś dodatkowe siły?
* A: Najemnicy, tien Samszar, za przyszłe środki.
* T: Noktianie? Skąd wiedzieliście, że nie otworzą ognia do cywili?
* W: Sentisieć.
* A: Tien Samszar, a co by potem ze sobą zrobili?
* T: Podejrzany zbieg okoliczności. Wszystko się za bardzo składa w jedną całość. Bilgemener, skąd (dane, paramsy ale też że nigdy nie był taki ambitny)
* B: Tien Samszar, bo gdy nie byłem ambitny, byłem pod ludźmi którzy spieprzyli.
* T: W sumie, jest to _jakaś_ opcja...

Tr Z +3:

* Vr (podbicie): Tadeusz akceptuje Bilgemenera jako przełożonego a Wacława jako front. Bo tak to czyta.
* X (podbicie): Tadeusz potrzebuje jeszcze jednego tiena jako wsparcie intelektualne. Bilgemener rekomenduje Karolinusa.
* Vr (manifest): Tadeusz uważa Bilgemenera wspieranego przez Karolinusa i Wacława za dobry Złoty Cień. Da mu szansę do pierwszej poważnej wtopy
    * duża część Lojalistów dołączy do Bilgemenera
    * Tadeusz i Bella będą współpracować przeciw Secesjonistom. Oni chcą jakichś reform, więc dostaną. Zobaczymy co wyjdzie.
* Vr (eskalacja): Tadeusz autoryzował Bilgemenera, że jeśli musi, może coś zrobić z Kliniką Oteriiel.

Oddział Amandy zostaje u Bilgemenera na pewien czas. By on nie zapomniał. A Bilgemener z nowymi zasobami sprowadza Wacławowi naprawdę ładne dziewczyny.

## Streszczenie

Amanda dostaje wsparcie od Kajrata w formie Kasimira i żołnierzy; wykorzystuje to do obrony Bilgemenera i poszerzanie jego sił. Pozornie uległa Wacławowi Samszarowi, dając mu poczucie bezpieczeństwa i wciągając go w bycie słupem Amandy w Złotym Cieniu. Przekonali razem Tadeusza Samszara, by Lojalistów dodać do Złotego Cienia - a Secesjoniści i element Lojalistów to tymczasowo problem Belli. Klinika Oteriiel pozostaje nierozwiązana i 'neutralna', acz pracuje nad nią Bella.

## Progresja

* Amanda Kajrat: dostała wsparcie od Kajrata. Kasimir Esilin oraz 7 noktiańskich żołnierzy. Lojalni, cisi, robią operacje militarne. Mają też akceptowalny sprzęt.
* Amanda Kajrat: opinia pokornej służki Wacława Samszara u samego Wacława i innych osób widzących sytuację. Bilgemener widzi więcej, ale wie gdzie jest power level.
* Karolinus Samszar: nagle stał się odpowiedzialny za powodzenie Złotego Cienia wraz z Wacławem Samszarem. Mają szansę do pierwszej poważnej wtopy Złotego Cienia.
* Wacław Samszar: nagle stał się odpowiedzialny za powodzenie Złotego Cienia wraz z Karolinusem Samszarem. Mają szansę do pierwszej poważnej wtopy Złotego Cienia.

## Zasługi

* Amanda Kajrat: oddała smyczkę Wacławowi Samszarowi, przez co nieformalnie dowodzi Złotym Cieniem. Poszerzyła wpływy Cienia i dodała do Cienia grupę Przemytników, frontem są Wacław i Karolinus plus przejęła kontrolę nad większością Cienia z tła.
* Wacław Samszar: pies na dziewczyny; upokorzył Amandę (bo nie lubi noktianek), ale będzie z nią współpracował. Jest udanym frontem na Złoty Cień - jak długo ma ładne dziewczyny. Else, Amanda będzie pełnić tą rolę. "Przejął" Amandę od Karolinusa jako swoją maskotkę / agentkę.
* Rufus Bilgemener: aktualny don Złotego Cienia namaszczony przez Tadeusza Samszara, pracujący 'pod Wacławem i Karolinusem', acz wie, że Amanda ma najlepsze pomysły i bez niej sobie nie poradzi. Administruje Złotym Cieniem.
* Emil Samszar: 41 lat, już zaplątany w Złoty Cień, który dostarczał mu badań itp; rywalizuje z Bellą o elementy Secesjonistów i Oteriiel.
* Bella Samszar: 24, event manager. Ma dobre serce i zna się na rzeczy, chce zreformować Złoty Cień ale czerpać zeń korzyści; skupia się na Secesjonistach i części Lojalistów oraz na Oteriiel.
* Kasimir Esilin: 37, starszy żołnierz Nocnego Nieba; przybył i wspiera Amandę niezależnie od tego co się stanie. Opracował plan złapania żywcem Przemytników atakujących magazyn i go przeprowadził sprawnie i bez słowa.
* Tadeusz Samszar: 43, chce jak najszybciej naprawić Złoty Cień i zająć się czymś innym. Widząc Wacława Samszara uznał, że to Bilgemener jest mózgiem operacji - nie zauważył znaczenia Amandy. Dał Bilgemenerowi szansę do pierwszego dużego fuckupu.

## Frakcje

* Złoty Cień Karmazynowego Świtu: większość Lojalistów i siły Bilgemenera trafiły pod kontrolę Bilgemenera, Wacława Samszara i przez to Kajrata. Część Świtu jeszcze jest pod kontrolą Belli, która próbuje go zreformować.
* Nocne Niebo: pozyskuje większość Złotego Cieniu Karmazynowego Świtu dzięki działaniom Amandy Kajrat, zaczyna mieć macki w Wacławie Samszarze. 

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Karmazynowy Świt, okolice
                                1. Strefa ekonomiczna
                                    1. Biurowce

## Czas

* Opóźnienie: 1
* Dni: 3

## Specjalne

* .

## OTHER
### Fakt Lokalizacji
#### Karmazynowy Świt

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Karmazynowy Świt: luksusowe miasteczko Samszarów; ekonomia oparta na usługach, ze szczególnym naciskiem na turystykę, wellness i rozwój osobisty
                                1. Centralny Park Harmonii
                                    1. Eko-spa
                                    1. Sztuczne Plaże
                                    1. Ogrody medytacyjne
                                    1. Pałac Harmonii: luksusowe miejsce odpoczynku i epicki hotel
                                    1. Gwiezdna Rozkosz: mniejszy love hotel
                                1. Rezydencje
                                    1. Szkoły i sale treningowe
                                    1. Biblioteka
                                1. Zewnętrzny pierścień wygaszający: zapewnia ciszę i spokój
                                1. Kraina Winorośli: winnice, winiarnie, restauracje
                            1. Karmazynowy Świt, okolice
                                1. Miasteczko Płomiennik
                                    1. Klinika Oteriiel: zajmują się wszystkim co jest potrzebne odnośnie modyfikacji bioformy, korupcji i dostarczania młodych dam i lordów do towarzystwa
                                    1. Domy
                                    1. Komenda Straży
                                    1. Tereny Rolne
                                