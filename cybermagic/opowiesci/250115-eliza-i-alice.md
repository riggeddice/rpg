## Metadane

* title: "Eliza i Alice"
* threads: brak
* motives: light-and-darkness, the-martyr, temat-tabu, dark-stimulants, arogancja-mlodosci, the-revenant, niedoceniany-wierny-pies, nic-sie-nie-zmienilem
* gm: żółw
* players: windhalm, magdawrr, lordan

## Kontynuacja
### Kampanijna

* [250115 - Eliza i Alice](250115-eliza-i-alice)

### Chronologiczna

* [250115 - Eliza i Alice](250115-eliza-i-alice)

## Plan sesji
### Projekt Wizji Sesji

* INSPIRACJE
    * PIOSENKA WIODĄCA
        * [Yuki Kajiura - Aura evil version](https://www.youtube.com/watch?v=lq62OepQOHM)
        * [Fujiwara no Mokou - Grilled Bird](https://www.youtube.com/watch?v=Uq9TTMcVr6U)
        * Amalgamacja muzyki
            * "Kill! Kill! Kill! Kill! Kill! Kill! I cannot die!"
            * Eliza - Alice, dwa byty. Jedna pragnie ratować, druga niszczyć i pomścić.
    * Inspiracje inne
        * Isekai Anime, ale na odwrót. 
        * Koreański horror z jedną dziewczyną rozbitą na dwa byty
* Konwencja
    * Świat
        * Sci-fi Archiwum X, "Magia w kosmosie", ale działamy w dużej firmie. SCP, Orion's Arm
    * Prowadzenie
        * Filmy: Archiwum X, Hercules Poirot, detektywistyczne
        * Przybywamy na miejsce, gdzie jest coś nadnaturalnego i to rozwiązujemy przez gadanie z ludźmi i poznawanie prawdy.
        * Postacie Graczy: nie dzieje się im krzywda chyba że zrobią coś bardzo głupiego
* Opowieść o (Theme and Vision):
    * "_Pride comes before the fall. Nikt nie jest perfekcyjny. Nikt nie jest niezastąpiony. Nieważne jak młoda i skuteczna nie jesteś, nie możesz wszystkiego._"
        * Eliza zawsze była "miracle worker", przekraczając swoje możliwości. I to ją ostatecznie zgubiło. Duma i pycha.
    * "_W każdej osobie znajduje się światło i mrok. Intensywne osoby są zdolne do wielkich poświęceń i wielkich zniszczeń._"
        * Eliza była, za życia, BARDZO intensywną osobą. Gdy kochała, kochała bardzo. Gdy nienawidziła, nienawidziła bardzo.
        * To sprawiło, że po śmierci się rozszczepiła na dwa byty - kochającą (Eliza) i niszczycielską (Alice).
    * "_Zło spowodowane przed śmiercią może powrócić jako zemsta._"
        * Wszyscy, którzy skrzywdzili Elizę lub zniszczyli coś dla niej cennego - Alice pomści cierpienie niedocenianej Elizy.
    * "_Pomożesz kilka razy to się przyzwyczają. Will be taken for granted._"
        * Eliza by przeżyła, gdyby ci, którym pomogła jej pomogli w jej najtrudniejszych chwilach.
    * Lokalizacja: Technopark pod Cieniaszczytem
* Motive-split
    * light-and-darkness: dwoista natura Elizy i Alice. Eliza zawsze chroni. Alice pragnie pomścić to co się stało. Obie są tym samym duchem.
    * the-martyr: Eliza skupiona na swoim bohaterstwie, pragnie być bohaterką którą powinna być. Tą, która poświęci się za wszystkich i uratuje wszystkich. I to ją zabija.
    * temat-tabu: Nikt nie mówi głośno o śmierci Elizy. Wszyscy myślą, że postacie wiedzą, że Eliza nie żyje.
    * dark-stimulants: Eliza, która zażywa narkotyki stymulujące by być w stanie pracować na pełnej mocy. Te stymulanty ją zabijają.
    * arogancja-mlodosci: Eliza nigdy nie dorosła. Zawsze uważała, że jest w stanie zrobić wszystko lepiej i skuteczniej niż inni. Dlatego została 'bohaterką'.
    * the-revenant: Alice, mroczne odbicie Elizy, które pragnie pomścić krzywdy, których Eliza doznała. Alice, która pragnie zniszczyć dzieło SerelTech i wykrzyczeć cierpienie Elizy w świat.
    * niedoceniany-wierny-pies: Eliza, która wszystkim pomagała i która pragnęła każdemu dać to, czego pragnie. Eliza, którą wszyscy traktowali jako "zawsze tu będzie".
    * nic-sie-nie-zmienilem: Eliza nie wie, że nie żyje. Ona nadal rozwiązuje problemy wszystkich - to ona wezwała Graczy, by ją usunęli.
* O co grają Gracze?
    * Sukces:
        * ALBO zniszczą SerelTech ALBO zniszczą Alice (i Elizę). Progresja SerelTech? Oddanie ukochanemu Elizy tego, co doń należało?
    * Porażka: 
        * Alice przetrwa.
* O co gra MG?
    * Highlevel
        * Pokazać Scholli jak to wszystko działa. Dużo Stereotypów, klocków i komponentów EZ.
        * Pokazać, czemu Blakenbauerowie są tak potężnym i niebezpiecznym rodem. Tomfoer jest KOSZMARNY.
        * Pokazać, że między Rodami - nawet wrogimi - jest możliwość dogadania się. Pomóżmy ludziom za wszelką cenę.
        * Pokazać dylematy międzyrodowe. Dlatego m.in. wprowadzam Smoczkogryza Verlena. Twardego łowcę potworów i obrońcę ludzi, dość nieugiętego w zasadach.
    * Co uzyskać fabularnie
        * Niech teren będzie uznany za "Skażony i niebezpieczny".
        * Niech Tomfoer zostanie zabity a Blakenbauer przeżyje. I niech to zrani jego serce wobec przyjaciółki z Verlenlandu.
        * Niech rozerwie się link między Szałwiuszem Blakenbauerem i Ragną Verlen.
* Agendy
    * .

### Co obiecałem

W firmie SerelTech toczyły się próby nad stworzeniem zaawansowanej aplikacji działającej w świecie wirtualnym. Niestety, zaczęły się tam dziać dziwne rzeczy - wypadki, problemy z aplikacją a nawet podejrzenie sabotażu. A terminy nie wybaczają!

Wy jesteście grupą dyskretnych konsultantów rozwiązujących nietypowe problemy. Normalnie firma taka jak SerelTech nawet by na Was nie spojrzała, ale w SerelTech pracuje Wasza zaufana znajoma, Eliza. I Eliza poleciła Waszą firmę. I wygraliście kontrakt.

Zdaniem Elizy, dzieją się tu dużo gorsze rzeczy niż się wydaje na powierzchni. Nie chodzi o problem z kontraktem. Chodzi o zdrowie i życie pracowników SerelTech. Dlatego poprosiła o pomoc Was - ludzi „spoza branży”, którzy „niejedno widzieli”.

I, faktycznie, wszystko wskazuje na to, że biura SerelTech albo są nawiedzane przez niebezpiecznego ducha (który... wydostał się z wirtualnego świata?), albo przez nie mniej niebezpiecznego sabotażystę. Ale dlaczego? I co tu się dzieje?

Waszym zadaniem jest naprawić sytuację w SerelTech i odkryć co się dzieje.

### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

KONTEKST:

* Eliza Derrist była lekko zubożałą nie-tienką i nie-czarodziejką z Aurum. Przybyła do Cieniaszczytu z pieniędzmi, umiejętnościami i marzeniami.
* Eliza zawsze była skrajnie ambitna i dumna. Wyjątkowa pracowitość i talent do konstrukcji światów wirtualnych.
* Eliza dołączyła do niewielkiej firmy, SerelTech, z dużymi marzeniami. Stworzyć virtŚwiat który pomoże ludziom zrozumieć skomplikowane koncepty. Edukacyjny.
* Eliza i postacie Graczy w młodości byli przyjaciółmi. Wspólnie radzili sobie z trudnymi rzeczami i potworami. Ale już wtedy Eliza zdradzała objawy Intensywności.
* Postacie Graczy zajmują się tematami detektywistycznymi itp. A Eliza poszła swoją drogą.

SERELTECH:

* Są w Technoparku, w coworku. Drzewa, kampus, "akademiki dla dorosłych". Standardowe biura plus ubikacje plus kuchnia.
* Projektują aplikację edukacyjną. Mają trzy awatary i jeden "ukryty". Ukryty to Alice - stworzony przez Konrada jako żart z mrocznej strony Elizy. Awatar, który bluzga i jest złośliwy.

CHRONOLOGIA:

* jak powyżej

PRZECIWNIK:

* Alice (alizE)

KTO CO ZROBIŁ ELIZIE (do kogo ma żal) :

| **Kto**                 | **Jaka akcja / relacja**              | **Jak zawinił / pomógł Elizie**    |
|-------------------------|---------------------------------------|------------------------------------|
| Bogdan, szef SerelTech  | pożyczył maszyny od Elizy             | Gdy Eliza rozpaczliwie potrzebowała pomocy finansowej, nie oddał i nie pomógł, by akcjonariusze byli zadowoleni. |
| Mariusz, sprzedawca     | podrzucał Elizie dzieci do opieki     | Po prostu zostawiał Elizie pod drzwiami. Eliza nie miała okazji niczego zrobić i nie miała serca by tam same zostały. |
| Ola, nowa LDev + sprzęt | fałszywa przyjaciółka                 | Pożyczyła od Elizy biżuterię i nie oddała. A potem gdy Eliza prosiła o sprawdzenie sprzętu nie zrobiła tego, powiedziała "OK". |

KOMU ELIZA POMAGA:

| **Kto**                | **Jaka akcja / relacja**             | **Jak zawinił / pomógł Elizie**    |
|------------------------|--------------------------------------|------------------------------------|
| Adrian, dev, dealer    | sprzedawał Elizie stymulanty         | Wcale. Adrian jest najlepszym przyjacielem Elizy. Ale po jej śmierci zrzucił policyjnie odkrytą pulę stymulantów, że to były jej. |
| Patryk, dev, junior    | używa sprzętu Elizy                  | Nie znali się. Ale Eliza prowadzi kod Patryka, budując ekosystem dalej. |
| Kasia, HR              | przyjaciółka                         | Pomagała Elizie w osobistych tematach. Zachęcała do tego by spróbować z chłopakiem. Zawsze stała po stronie Elizy. |

JAK ELIZA ZGINĘŁA?

* Testowane były nowe, silniejsze prototypy urządzeń.
* Eliza uszkodziła swój. Po prostu. Zdarza się.
    * Bo siedziała po nocy i pilnowała chorych dzieci Mariusza.
* Eliza, pełna dumy, chciała to ukryć i kupić element, ale nie miała pieniędzy. Więc kupiła tańszy.
    * Bo Bogdan nie oddał jej pieniędzy gdy prosiła.
    * Bo miała problemy zdrowotne wynikające z ciężkiej pracy i używania stymulantów.
* Eliza poprosiła Olę, by ta upewniła się, że jej sprzęt zadziała. To godzinka jej pracy, a Eliza musi się zająć innymi rzeczami.
* Ola była zajęta. Nie sprawdziła. W końcu co najgorszego może się stać?
* Eliza po kolejnej nocce by utrzymać terminy, zażywa kolejną dawkę stymulantów. 
* Eliza, na stymulantach, łączy się uszkodzoną stacją virt. Dostaje ataku epilepsji, sygnałów, sprzężeń. Sama w pokoju.
    * Normalny człowiek by kilka dni odchorowywał.
    * Ale Eliza była zanurzona za długo. Samotnie. Znaleziono ją martwą przy biurku.

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

* Alice zniszczy firmę. 
* Agregat "Alice - Eliza" powróci jako technologiczny amalgamat. "Czemu nie mogę mieć ciała?"
* Ola będzie musiała zmienić pracę. Całkowicie zmienić branżę. I wyjechać.
* Bogdan skończy pod mostem. Nie będzie w stanie nigdy niczego posiadać.
* Mariusz straci żonę i dzieci. Musi zacząć od nowa. Nie może się do nich zbliżyć.

### Fazy sesji

**FAZA 0**: Demonstracja dobrych czasów z Elizą

* Tonalnie
    * Optymizm, przyjaźń, kamraci
* Wiedza
    * Jesteśmy w "niskim sci-fi", mamy silne postacie które mogą dużo zrobić.
    * Młode postacie, 19-20 lat.
* Kontekst postaci i problemu
    * Demonstracja Intensywności Elizy. Zarówno jej ciepła, determinacji jak i jej mroku.
    * Eliza zrobiłaby dla przyjaciół wszystko.
* Implementacja
    * ?

**FAZA 1**: Eliza ratuje ludzi przed Duchem

* Postacie mają 25-26 lat. Eliza nic się nie zmieniła.
* Szef oddziału dziękuje za ich przybycie. To ciekawe, że centrala chciała właśnie ich (nie jest szczęśliwy). Powie listę 3 rzeczy jakie wskazują na obecność ducha:
    * Wypaczenia w Virt. Niekończące się światy prowadzące w... dziwne miejsca.

Moce Alice:

* Wiadomości, telefony komórkowe, niezbyt sprawne obrazy
* Poltergeist.
* Obecność w Virt. She is the queen there.
* Koszmary przez virt.

Agenda Alice wobec osób:

* **Bogdan**: "nigdy niczego nie będziesz miał"
* **Mariusz**: "nigdy nie będziesz wiedział czy Twoje dzieci są bezpieczne"
* **Ola**: "nigdy nie będziesz czuła się bezpieczna"
 
**FAZA 2**: Wiemy, czym jest duch. Teraz trzeba go zniszczyć.

Moce Alice:

* Amalgamat technologiczny
* Pancerz zewnętrzny

## Sesja - analiza
### Fiszki

* Bogdan Serel: szef oddziału SerelTech


***
***

### Scena Zero - impl

Zespół to grupa studentów dostarczających projekt - 6 osób. Niestety jeden z kolegów, Filip, nie zrobił tego, co powinien. Nie zrobił absolutnie niczego. Na spotkaniu zespołu Eliza powiedziała, że weźmie wszystko na siebie. Jest w stanie zrobić to sama w ciągu 3 dni. Inni jednak się o nią martwią. Zespół przekonuje ją, by się podzielić.

I udało im się, bo są kompetentni i nigdy Elizy nie zawiedli.

Eliza nie musiała siedzieć po nocach cały czas sama. Podzielili się pracą i im się udało. Eliza nie musiała być męczennicą ani bohaterką. Chciała jednak zemsty na Filipie. Jej pierwszy pomysł to było zatruć mu jedzenie albo poprosić kogoś, by go pobił. Zespół znowu skutecznie ją uspokoił i przywrócił do równowagi.

Okazało się, że Filip ma mało kompetentną dziewczynę. Jest głęboko zakochany, a cała ich grupa po prostu nie daje sobie rady. Filip pracuje dla tamtych i ma nadzieję, że zespół ten jeden raz mu wybaczy. Jednak Filip nie miał odwagi cywilnej, by powiedzieć tego przyjaciołom.

By zapobiec gniewowi Elizy, infiltrator skutecznie umieścił w prezentacji zespołu dziewczyny Filipa slajdy z narysowanymi w Paincie kwiatami i tekstem: „Kocham cię, Felicjo. Filip”.

Poczucie sprawiedliwości Elizy zostało zaspokojone. Nie jest osobą mroczną ani okrutną, ale jest bardzo intensywna i ma obsesję na punkcie kontroli.

### Sesja Właściwa - impl

8 lat później Zespół poszedł różnymi drogami. Zespół to teraz grupa konsultantów i najemników (biznes, małe egzorcyzmy, detektywi), a Eliza pracuje w firmie SerelTech jako ekspertka virt. 

Eliza nigdy nie prosi o pomoc. Fakt, że zespół dostał maila, w którym Eliza prosi o pomoc i informuje, że wszystko zostanie dobrze opłacone, jest sam w sobie bardzo dziwny. Do tego stopnia, że Cyfrowy Infiltrator próbuje ustalić, czy to naprawdę wiadomość od niej.  

TrZ+3:

* X: Wszystko wskazuje na to, że tak.
    * W tym mailu jest coś trochę dziwnego, ale jest to wiadomość od Elizy, napisana w sposób charakterystyczny dla niej.
    * Wygląda na to, że osoba pisząca maila ma dostęp do jej wiedzy.
    * Treść zawiera zarówno unikalną arogancję i poczucie wyższości Elizy, jak i jej troskę.

Zespół dotarł do SerelTech. Zostali zaprowadzeni przez ochroniarzy do szefa firmy, Bogdana. Infiltrator wycofał się i nie wszedł; zamiast tego postanowił rozejrzeć się. Chwilę potem dostał SMS-a od Elizy z prośbą, by poszedł z nią na taras widokowy.  

Zespół rozmawia z szefem. Bogdan powiedział, że nie do końca jest przyzwyczajony do współpracy z ludźmi o takim profilu jak oni, ale skoro centrala życzy sobie ich działania, to nie będzie stawał na drodze.  

Bogdan wyjaśnił, że dwa tygodnie temu zaczęły dziać się dziwne rzeczy. Mariusz (sprzedawca) dostał SMS-a od żony, że miała wypadek, a dzieci są w szpitalu. Jednak na miejscu okazało się, że wypadku nigdy nie było. Całkiem niedawno Mariusz wyszedł na rozmowę z klientem, a potem wrócił. Jego wygaszacz ekranu został zmieniony na zdjęcie jego dzieci w trudnej sytuacji i Bogdana robiącego zdjęcia zamiast pomóc. Jednocześnie Kasia, która była z nim w pomieszczeniu, przysięga, że nikt nie wchodził do pokoju.  

Gdy Bogdan robił demonstrację przed klientami, rzeka w virt "zgniła". Doszło do wyraźnej komplikacji. Nie udało się tego naprawić, a klienci odeszli niezadowoleni.

Bogdan nie wierzy w duchy. Wierzy w sabotaż. Przyciśnięty przez Profilerkę przyznał, że podejrzewa Konrada. Konrad, najlepszy programista w firmie, jest znany jako złośliwy dowcipniś. Gdy kiedyś Mariusz zabrał mu kanapkę, Konrad wsadził go do szpitala. Konrad ma wszystko gdzieś - zatrzymuje się, nie robi nikomu krzywdy, ale też nie pozwala sobą pomiatać.  

Korzystając z okazji, Infiltrator sprawdził, skąd pochodziły SMS-y - zarówno te do Mariusza, jak i te do Oli (które zawierały treści typu "Wiemy, co zrobiłaś" i "Patrzymy").

TrZ+3:

* V: Okazało się, że te wiadomości pochodziły ze środka firmy. Musiały zostać wysłane z komputerów w Technoparku.
* X: Niestety, nie udało się uzyskać dodatkowych informacji. To wszystko, co można było ustalić na razie.

Korzystając z okazji, Dekonspirator poszukał informacji o duchach i anomaliach. Czy są takie Anomalie w okolicy? Jakiegolwiek? Czy zna jakieś podobne? Tak - znalazł, takie Anomalie są powiązane ze śmiercią. To "duchy zemsty", mogą wystąpić w okolicy pola magicznego. I te Anomalie będą rosły w siłę z czasem.

Zainspirowana przez Infiltratora Profilerka spytała Bogdana czemu mówi o tym, że Konrad i Eliza "rywalizowali ze sobą", ale w czasie przeszłym. "Czy coś się zmieniło od tej pory"?

TrZ+3:

* X: "Praktycznie nic, poza tym, że Eliza nie żyje od 3 miesięcy". Bogdan patrzy na Profilerkę jak na złośliwą osobę. Jakby ona wiedziała. A ona nie wiedziała.

A Infiltrator jest z Elizą na dachu...

Infiltrator uznał, że Eliza - nawet jeśli jest "duchem" - to jednak nadal najpewniej będzie tą samą Elizą. Więc poprosił ją o pomoc w zrozumieniu tych wiadomości. Eliza z przyjemnością pomogła - nie dotknęła konsoli, ale poprowadziła Infiltratora przez proces.

Tp+3:

* X: Bez Elizy nie dałoby się tego zrobić. Eliza faktycznie pomogła, jest nadal wybitna.
* Vr: Jest dowód paranormalności. Wiadomości pojawiają się same.

Krzyk. Panika. Dekonspirator widzi Elizę, która pokazuje palcem w stronę ubikacji kobiecej i krzyczy "szybko, ratuj!". A tam - Ola, topi się w umywalce. COŚ ją trzyma za głowę, ale nic nie widać (my wiemy, że to ALICE). Dekonspirator szybko wyrywa ją za włosy z umywalki. Ola histeryzuje, oskarża w panice Dekonspiratora o próbę utopienia jej.

Pojawia się szef, pojawiają inni. Profilerka szybko tłumaczy, że to nie mógł być Dekonspirator. Po co? Plus, chronologia się nie zgadza.

Tp+3:

* V: Szef jest przekonany, że jednak nie była to próba utopienia Oli przez Dekonspiratora.

Korzystając z okazji, Dekonspirator zajmuje czas a Profilerka odciąga Olę na stronę. Próbuje od niej dowiedzieć się o co chodzi i co się dzieje. Co się stało z Elizą? Co między nią i Elizą? Dlaczego ktoś (potencjalnie Eliza) pragnie śmierci Oli?

Tp+3:

* Vr: Ola powiedziała, że Eliza wzięła za dużo srok za ogon, jak zwykle. Stąd wszystkie problemy.
* X: Ola bardzo płacze. Wszyscy uważają, że Profilerka jest bardzo okrutną osobą XD.
* X: Ola wymusiła na Profilerce, że Profilerka ją obroni przed Elizą czy czymkolwiek tam jest (duchem?)
* (poddany): Ola wszystko opowiedziała co wie.
    * Eliza się pomyliła. Zrobiła poważny błąd i jej Integrator się spalił a był ważny termin
    * Eliza poprosiła Olę o sprawdzenie części jaką Eliza kupiła, ale Ola była zajęta i zapomniała
    * Gdy Eliza spytała Olę - Ola powiedziała że sprawdziła.
    * "przecież Eliza miała pieniądze; nigdy nie oszczędzała i znała się na rzeczy"
    * Niestety, ta część była uszkodzona. Eliza dostała dawką 300% amplitudy emocjonalnej, zwłaszcza pod kątem emocji negatywnych.
        * A że była sama, to nikt jej nie odpiął.
        * To ją zabiło
    * I Ola żyje w poczuciu winy że zabiła Elizę...    

Rafał, chłopak Elizy jest prawdziwą osobą. I ma chorą córkę. Nie jest nieuleczalnie chora, ale wymaga to dużo pieniędzy. I Dekonspirator doszedł do tego jak wyglądała historia między Rafałem i Elizą, jak do tego doszło, zwłaszcza z pomocą Cyfrowego Infiltratora.

* Wpierw Eliza wsadziła sporo pieniędzy w firmę. Kupiła część maszyn virt.
* Potem Mariusz zaczął zostawiać jej swoje dzieci. Eliza nie chciała by stała się im krzywda więc dała sobie wejść na głowę
* Potem Eliza poznała Rafała. Rafał ma chorą córkę.
* Eliza zaczęła pakować kasę w córkę, nie w firmę.
* Eliza odkryła, że jest poważnie chora. Poprosiła szefa o zwrot kasy
    * Ale on myślał że to dla Rafała i nie chciał, by jej się stała krzywda. "Odda jak się zwrócą" - powiedział, bo przecież Eliza miałaby duży zwrot finansowy!
    * Eliza nie powiedziała, że ONA jest chora.
    * Eliza nie dożyła zwrotu kasy.
* By dorobić więcej, Eliza zaczęła kupować silniejsze stymulanty. Bardziej toksyczne. I dorwała kilka fuszek, dodatkowych robótek.
* Więc Eliza wszystkim pomogła finansowo. I na końcu - zabrakło pieniędzy dla niej.

Oki - ale skąd miała środki psychoaktywne? Skąd stymulanty? Dekonspirator skupił się na znalezieniu faktów.

Tr Z +3:

* X: Dekonspirator, technicznie, szuka narkotyków i info o tym. Zainteresuje się nim policja.
* V: Znalazł informacje co to było - narkotyki, środki stymulujące. Czyli faktycznie dość toksyczne środki 

Z tego wreszcie da się wywnioskować jak zginęła Eliza? Jaka kombinacja czynników?

* Była **sama w biurze**. Gdyby był ktokolwiek, mógłby ją odciąć.
* **Ola nie sprawdziła części**. Gdyby sprawdziła, znalazłaby problem. Ale samo w sobie to najwyżej kosztowałoby ją do tygodnia w szpitalu (szok & horror wynikający z kalibracji virt).
    * ...była zbyt zmęczona i biegała między Rafałem a **dziećmi Mariusza**.
* **Była na toksycznych środkach**. Jej organizm nie wytrzymał. Gdyby nie była tak przeciążona, skończyłaby w szpitalu.
    * ...**ale nie miała pieniędzy** i zarabiała na swoje zdrowie, wzięła drugą i trzecią pracę.

Czyli Eliza, która zawsze pomagała wszystkim nie dostała pomocy od nikogo komu pomogła i kto mógł jej pomóc.

Zostaje Konrad? Ale w sumie Eliza nie skupia się na Konradzie. Wiadomo z opowieści, że to była taka... jednostronna rywalizacja. Eliza rywalizowała z Konradem, który miał to wszystko gdzieś. I Konrad w którymś momencie, zirytowany już działaniami Elizy, stworzył w virt ALICE - zbudowany awatar na podstawie Elizy, który ma ją wyśmiewać. 

Więc Infiltrator z pomocą Elizy skupił się na dojściu do danych o Konradzie - co Konrad zbroił? Czy ALICE na niego poluje? (odpowiedź: nie; Eliza szanowała Konrada który zresztą nic w jej stronę nie robił)

Tp+3:

* X: Konrad wie, że Infiltrator grzebie w jego danych.
* X: Konrad staje przeciwko Infiltratorowi. Przyjdzie tu fizycznie.
* Vr: Konrad jest tym co stworzył "Alice". Zabawna wojna Konrad - Eliza.

Więc Konrad jest czysty. Ale to rywal. Wyszła ważna rzecz - jak tylko Eliza próbowała zniszczyć ALICE, Konrad ją odbudowywał i usprawniał, fortyfikował. Aż w końcu Eliza się poddała (po raz pierwszy w historii; to wtedy gdy już wiedziała że jest chora). Konrad wtedy schował ALICE w virt, ale jej nie skasował.

A teraz ALICE "opuściła" virt, polując na Olę (za niedopatrzenia), Bogdana (za nieoddanie pieniędzy) i Mariusza (za zaniedbywanie dzieci).

Tymczasem Profilerka przepytuje Adriana. Adrian, przepytany przez Profilerkę, powiedział jej prawdę. Żyje w poczuciu winy.

* Adrian od zawsze się narkotyzował trochę, by móc sobie poradzić. Wzmacniał się stymulantami.
* Był źródłem dla Elizy i innych.
* Gdy Eliza zginęła, była tu policja. Ale JAKOŚ się okazało, że ślady wskazywały że to nie Adrian a Eliza była dealerką.
    * to był pierwszy czyn ducha Elizy. Ochronić swojego przyjaciela.
* Adrian do dziś czuje, że to jego wina, że Eliza zginęła.

Profilerka przekonała go, że Eliza mu wybaczyła. Że go lubiła. Że to nie jego wina.

Tp+3:

* Vr: Ulga; Adrian zrozumiał, że to nie on zabił Elizę.

Dobrze - ostateczna konfrontacja. Czas na spotkanie na tarasie - Konrad oraz cały Zespół. Przy okazji, telefon do Oli - niech Ola idzie pod prysznic i się stamtąd nie rusza na następne kilkanaście godzin.

Konrad wyszedł na taras. Nie widzi Elizy, ale widzi resztę Zespołu. Zaczyna najeżdżać na Infiltratora, że jest szpiegiem i debilem. Dekonspirator zaczął Konradowi wyjaśniać, że Eliza jest duchem i jest prawdziwa. Że to ona stoi za wszystkim. Jakkolwiek brzmiał absurdalnie "the truth is out there", Konrad umiał połączyć fakty i po otrzymaniu wszystkiego _zrozumiał_. Fakt, że zaczął zauważać Elizę jedynie mu w tym pomógł. A tymczasem Infiltrator wziął Elizę na stronę, by nie wiedziała że nie żyje.

Konrad pomoże Zespołowi. Jeśli ALICE - jego kreacja - jest animowana przez Elizę jako anioł zemsty, Konrad zdecydował się do rozłożyć. Poszedł porozmawiać z Elizą. Przeprosił ją za wszystko. Powiedział, że nie będzie stał na drodze - niech Eliza zniszczy ALICE. To zostało wzmocnione przez cały Zespół.

Eliza z szerokim uśmiechem się zgodziła. 

Eliza bierze Integrator Infiltratora i uruchamia go. Wyłączy ALICE z poziomu virt.

Tr M +3Ob +3:

* Ob: Eliza rozpięła virt na taras. Wszyscy są w vircie. A po przeciwnej stronie rzeki - ALICE.
* (Infiltrator przypomina przeszłość - przypomina scenę "Filip x Felicja" w prezentacji, stawiając ściany dookoła virt. By przypomnieć Elizie czym była.) (+2Vg)
* V: Eliza ujawnia serce. Uderza w ALICE. Pokazuje czym była. ALICE is unwounding. Ale... okazuje się, że Eliza i ALICE (o czym Eliza nie wie) są tym samym bytem.
* V: ALICE zostaje zniszczona. Virt jest przejęty przez Elizę. Ale... Eliza i ALICE zaczynają się rozpadać razem, co powoduje panikę u Elizy
* V: Eliza, przytulona przez przyjaciół, rozprasza się w mniejszym cierpieniu i nie umiera samotnie - w odróżnieniu od śmierci w Integratorze...

Integrator Elizy jest lekko Skażony, ale w pozytywny sposób. Czyli jest pozytywny, nie negatywny; przynajmniej na razie. A Konrad pomoże rozwiązać sprawy Elizy, przekazać pieniądze Elizy na leczenie chorej córki Rafała itp...

Smutna historia dobiegła końca...

***
***

## Streszczenie

Trójka detektywów przybywa do SerelTech, by zbadać serię dziwnych zdarzeń w firmie technologicznej. Sabotaże systemów wirtualnych, przerażające wiadomości i rosnący chaos wskazują na coś nadprzyrodzonego. Na miejscu odkrywają, że za wszystkim stoi duch ich dawnej przyjaciółki, Elizy Derrist, genialnej, ale tragicznie zmarłej programistki. Pod postacią Alice, technologicznego bytu zrodzonego z żartów i rywalizacji, Eliza szuka zemsty na tych, którzy ją zawiedli.

## Progresja

* .

## Zasługi

* Klasa Cyfrowy Infiltrator: Zidentyfikował autentyczność wiadomości od Elizy oraz ustalił, że ich źródłem były systemy wewnętrzne SerelTech. Współpracował z duchem Elizy, która pomogła mu analizować anomalie technologiczne. Odegrał kluczową rolę w finale, wykorzystując swoje umiejętności do zniszczenia Alice w świecie virt. Jego współpraca z Elizą była dowodem na jej geniusz, nawet po śmierci.
* Klasa Profiler Dusz: Przesłuchiwała kluczowe postacie NPC, takie jak Ola, Adrian i Bogdan, wydobywając z nich informacje o ich relacjach z Elizą i zaniedbaniach, które przyczyniły się do jej śmierci. Skutecznie uspokajała Olię po próbie utopienia i odkryła, że kłamstwo Oli dotyczące sprawdzenia części doprowadziło do tragedii. Dzięki jej interwencjom bohaterowie zyskali pełniejszy obraz sytuacji.
* Klasa Poszukiwacz Konspiracji: Badał fizyczne dowody anomalii, takie jak źródło wiadomości i ślady narkotyków, które Eliza zażywała w ostatnich dniach życia. Był świadkiem najbardziej przerażających manifestacji Alice, w tym próby utopienia Oli. Jego dociekliwość i zdolność do analizowania powiązań pozwoliły zrozumieć, w jaki sposób różne czynniki doprowadziły do śmierci Elizy.
* Eliza Derrist: KIA; Nawet po śmierci była motorem wydarzeń, działając jako duch Alice, który wymierzał zemstę osobom odpowiedzialnym za jej tragedię. Pomogła bohaterom w walce z Alice, a jej ostateczne poświęcenie doprowadziło do zniszczenia amalgamatu i uwolnienia jej ducha. To ona wezwała tu Zespół.
* Bogdan Serel: Szef SerelTech, który nie oddał Elizie pieniędzy, co wpędziło ją w finansowe problemy. Był celem Alice, która manipulowała jego otoczeniem, by zniszczyć jego reputację. Mimo sceptycyzmu wobec duchów, współpracował z bohaterami, dostarczając kluczowych informacji.
* Konrad Kumis: Twórca Alice, początkowo jako żartu z Elizy, później jej rywal w pracy. W finale przyznał się do swoich błędów i przeprosił Elizę, wspierając bohaterów w ostatecznej konfrontacji. Jego wiedza pomogła w rozłożeniu Alice na części.
* Mariusz Gawrończyk: Zaniedbywał swoje dzieci, zostawiając je pod opieką Elizy, co dodatkowo ją obciążyło. Stał się jednym z celów Alice, która manipulowała jego strachem o bezpieczeństwo rodziny. Reprezentował ludzi, którzy korzystali z dobroci Elizy, nie dając nic w zamian.
* Ola Terierska: Bezpośrednio przyczyniła się do śmierci Elizy, zlekceważając jej prośbę o sprawdzenie sprzętu i kłamiąc, że to zrobiła. Była celem Alice, która próbowała ją zabić. Dzięki przesłuchaniu przez Profilerkę przyznała się do zaniedbania i poczuła ulgę, choć nadal nosiła ciężkie brzemię winy.
* Adrian Pykszor: Przyjaciel Elizy i dostawca stymulantów, które pomagały jej radzić sobie z obciążeniami pracy. Po jej śmierci Eliza jako duch ochroniła go przed oskarżeniami o handel narkotykami, co wywołało u niego poczucie winy. Jego współpraca z Profilerką pozwoliła mu zrozumieć, że Eliza mu wybaczyła.
* Patryk Urdowin: Korzystał z kodu i sprzętu Elizy i z jej wsparcia (ona jest pozytywnym Skażeniem które mu pomaga lepiej pisać), choć był przyjęty po jej śmierci.

## Frakcje

* .

## Fakty Lokalizacji

* Ecrestian: niewielka miejscowość niedaleko Cieniaszczytu, która ma technopark i zawiera sporo firm skupiających się na vircie; korzystając z dużego stężenia Alucis w okolicy.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Przelotyk Wschodni
                            1. Ecrestian
                                1. Technopark

## Czas

* Chronologia: Zmiażdżenie Inwazji Noctis
* Opóźnienie: 424
* Dni: 1

## Inne

.
