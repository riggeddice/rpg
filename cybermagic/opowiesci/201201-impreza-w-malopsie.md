## Metadane

* title: "Impreza w Małopsie"
* threads: unknown
* gm: żółw
* players: kić, darken

## Kontynuacja
### Kampanijna

* [201025 - Kraloth w parku Janor](201025-kraloth-w-parku-janor)

### Chronologiczna

* [201025 - Kraloth w parku Janor](201025-kraloth-w-parku-janor)

## Punkt zerowy

### Dark Past

.

### Opis sytuacji

.

### Postacie

* Daniel Terienak
    * dziąsłowiec i biedaartefaktor
    * kolekcjoner artefaktów, też mniej legalnych. Terminusi zajumali i czuje krzywdę.
    * agresywny, improwizujący, zero cierpliwości
* Karolina Terienak
    * pieści swój ścigacz nawet jak patrzą
    * świetna w walce kontaktowej ultrakrótkiej, jak bulterier
    * świetny mechanik i pilot

## Punkt zero

* Czarek / Cezary, domorosły egzorcysta pomógł swojej koleżance pozbyć się demona... i go przejął. Demon opętał Czarka.
* Czarek chce zrobić doom-metalową, pesymistyczną imprezę by się pozbyć demona rozpraszając go w obecnych.
* Chce w tym Czarkowi pomóc Rekin, niejaki DJ Babu. Ma pomysł jak zniewolić demona.
* W Czarka wpatrzony jest Andrzej, silny facet, trener. Andrzej chce być "jak Czarek". Demon przychodzi do niego w snach.
* Andrzej boi się tego co się dzieje w snach więc odsuwa się od swojej dziewczyny, Pauliny.
* Paulina miała romans z magiem, Izydorem, z Rekinów. Żonatym Izydorem (o czym ona nie wie). Prosi Izydora o pomoc.
* Izydor, nieszczęśliwy, prosi Postacie Graczy o pomoc.

## Misja właściwa

Izydor wysłał Zespół do Pauliny. Przyznał się do romansu. Pomogą mu.

Paulina pożaliła się Zespołowi. Powiedziała, że to poważny problem - to co Andrzej chce zrobić (impreza pesymistyczna) jest nielegalne. To może złamać jego życie i karierę. Karolinie na uboczu się przyznała płacząc, że Andrzej się od niej oddala, jakby jej uroda przestała na niego działać. Wyjaśniła też Karolinie ekosystem:

* Małopies jest miejscem, które zawsze było bardziej niezależne. Nigdy nie łyknęli "dominującej tyranii optymizmu" w Pustogorze.
* Jest tu pewnego typu "omerta". Nikt na nikogo nie powie i nie nada; Paulina wzywając osoby z zewnątrz ryzykuje dość sporo.
* (XXX: Paulinie coś się stanie, bo złamała omertę!)

Paulina wyjaśniła też wysokopoziomowo problem:

* Andrzej ma zorganizować imprezę na stadionie, który Andrzej stróżuje.
* Andrzej woli Cezarego od niej.
* Andrzej może strasznie zapłacić - a jak ona się od niego nie odsunie, też. A jej na Andrzeju zależy.

Daniel się wkurzył. No jak to, Andrzej lekceważy Paulinę. To nie może tak być, nieakceptowalne. Tak więc nowy plan - stadion i pogadać z Andrzejem. Polecieli tam z Karoliną, znaleźli Andrzeja i od razu Daniel zaczął od kopa w jaja i poprawienia w twarz.

Andrzej został prawidłowo ustawiony do sytuacji. Karolina tylko uniosła brwi.

Przyleciał starszy koleś, który nie przemyślał sprawy o imieniu Tadeusz - chronić Andrzeja. Daniel autorytatywnie kazał Andrzejowi odpowiadać na pytania a Tadeuszowi notować. W wyniku rozmowy, wszyscy uznali, że Zespół jest z mafii (Grzymościa). Andrzej, odpowiednio zmiękczony, wszystko wyznał:

* ma koszmary. Śmierć i cierpienie. Budzi się i prawie skrzywdził Paulinę. **Próbuje ją uratować, więc się od niej oddala.**
* Czarek jest egzorcystą. Jak Andrzej mu pomoże i zrobi tą imprezę, to koszmary odejdą.
* Andrzej będzie współpracował
* W okolicy jest jeszcze jeden Rekin, 'DJ Babu', który POMAGA (nie rozkazuje, pomaga) Czarkowi. Samo to zdziwiło Daniela i Karolinę.

Tadeusz (starszy koleś) to jest łasica i szczur. Zbiera informacje i zapisuje rzeczy bo tego sobie życzył Daniel.

Dobra, czas pojechać do motelu gdzie znajduje się Czarek. Trzeba jego przesłuchać. Karolina pozycjonowała się od strony okna, Daniel wchodzi centralnie do środka od frontu. Karolina widzi, jak Czarek kłóci się z "powietrzem", gada do powietrza. Gdy Daniel puka do drzwi, Czarek łapie za telefon i nie przerywając mówienia (teraz) do telefonu, wpuszcza Daniela.

Działania Karolinie uprzykrzyły dzieci. "NIECH SIĘ PANI DA PRZELECIEĆ". Karo wzięła i pokazała dzieciakom jak się fajnie lata. Dzieci <3 Karo. Karo dowiedziała się od nich, że: Babu się nie rządzi Czarkiem ale mu pomaga ORAZ że Babu nikogo nie lubi. A Czarka boją się wszystkie psy. Oki - to jest ciekawe.

Daniel tymczasem gada sobie z Czarkiem. Wpierw łagodnie, ale jak Czarek nie okazuje odpowiedniej pokory to Daniel wali mu gitarą w plecy i zaczyna go bić by ustawić go do porządku. (XXX: nadchodzi Babu). (V: Daniel zniszczył defensywy). W ogóle, dookoła Czarek ma mnóstwo jakichś symboli ochronnych, antydemonicznych... typowe przedmioty egzorcysty-amatora.

Karolina widzi Babu pierwsza. Wzbija ścigacz w powietrze i leci mu na spotkanie. Z zaskoczenia zderzyła swój piękny ścigacz z jego brzydką, funkcjonalną maszyną i przeciorała jego ścigacz przez ściany i jakiś słup. Ścigacz Karoliny jest uszkodzony, niesprawny - ale Babu jest ranny (rozwalił się ścigaczem). Karolina ze wściekłością rzuciła się na Babu i go zdemolowała, ogłuszając. Babu potrzebuje intensywnej terapii XD.

Czarek widząc to zlał się w spodnie.

Przybył policjant Franciszek i policja ogólnie; opieprzyli Karolinę. Dzieciaki w jej obronie: "PANI Z MAFII ZROBIŁA PORZĄDEK ZE ŚWINIEM!". Karolina, zakrwawiona, z uśmiechem: "przecież wszyscy wiemy, że mafii nie ma". Franciszek nie chce podpaść Grzymościowi ani Kajratowi; kiedyś noktian tępił, ale to nie te lata. Splunął na ziemię, ale jak tylko Karolina powiedziała, że Babu płaci i trzeba mu pomóc medycznie, cóż. Franciszek uznał że bezpieczniej jest się nie mieszać.

Gdy Babu został ewakuowany, Karolina dołączyła do Daniela a Daniel zaczął magicznie badać Czarka. Ten ma geas. Czarek zaczął bełkotać, że ona przychodzi w nocy i zaczął mieć fizyczną przemianę. Demon zaczął wyłazić. Teraz Andrzej wyje ze strachu - to demon z jego snu. Daniel i Karolina zostali solidnie ranni, ale udało się demona poważnie stłuc magicznie; kosztem Czarka (ciała i umysłu). Gdy demon chciał przeskoczyć na Andrzeja, Karolina zagnieździła go w kotwicy - którą okazał się być żeton z wózka, który Daniel wsadził demonowi w gardło. Żeton jest megaprzeklęty.

Demon contained.

Gdy FAKTYCZNIE przybył ktoś od Grzymościa zobaczyć "nie będziesz brał imienia mafii swojej nadaremno", Daniel i Karolina się z nim ułożyli - on dostanie ten dziwny, przeklęty żeton - ale trochę uporządkują rzeczy po tym co tu się stało.

A Daniel jest dumny - ten żeton to pierwszy świetny artefakt, który udało mu się zrobić. I on działa.

## Streszczenie

W Małopsie egzorcysta-amator zainfekowany demonem próbował zrobić doom-metalowy pesymistyczny koncert. To jest nielegalne (Pryzmat: tyrania optymizmu). Rekiny, które się pojawiły by to rozwiązać przeszły przez temat używając pięści - usunęły demona, zmieniły go w broń i oddały to Grzymościowcom (za to, że Wolny Uśmiech tu posprząta po nich). Ale skąd tu demon?

## Progresja

* Paulina Mordoch: Złamała Małopsową 'omertę' wzywając Rekiny; poniesie JAKIEŚ konsekwencje.

### Frakcji

* Wolny Uśmiech: uzyskali dziwny artefakt - żeton compellujący wsadzanie do ust, który zmienia osobę co to zrobi w demonicznego viciniusa - broń.

## Zasługi

* Karolina Terienak: Rekin. Miłośniczka swojego ścigacza i świetny pilot. Normalnie łagodna - ale brutalnie zdewastowała DJ Babu za uszkodzenie ścigacza.
* Daniel Terienak: Rekin. Dziąsłowiec i biedaartefaktor, który nie lubi jak ktoś lekceważy kobiety. Idzie prosto do celu pięścią i agresją. Stworzył "z demona" artefakt, który oddał Grzymościowcom za to, że oni posprzątają po Rekinach.
* Izydor Grumczewicz: Rekin, żonaty, nie potrafi utrzymać "końcówki" przy sobie. Zlecił Zespołowi by rozwiązali problem kochanki (Pauliny).
* Franciszek Zygmunt: policjant z Małopsa. Kiedyś wiarus, teraz próbuje utrzymać wszystko do kupy parę lat do emerytury. Bardzo odważny; nie bał się wyjść naprzeciw "magów Grzymościa". Tępi noktian jakby było 30 lat temu.
* Cezary Urmaszcz: egzorcysta-amator, który próbował usunąć demona z koleżanki. "Złapał" demona. Teraz próbuje doom-metalową kapelą się go pozbyć. Zdewastowany; wymaga poważnej terapii po starciu z Danielem i Karoliną.
* Andrzej Kuncerzyk: silny trener personalny; zainfekowany demonem odpychał od siebie swoją dziewczynę. Dostał wpiernicz od "mafii" i wszystko powiedział.
* Paulina Mordoch: dziewczyna Andrzeja i kochanka Izydora, miejscowa piękność. To, że Andrzej ją odpychał poważnie nadwątliło jej ego i poczucie seksapilu. Złamała Małopsową 'omertę' wzywając Rekiny; poniesie konsekwencje.
* Barnaba Burgacz: Tzw. 'DJ Babu', Rekin, dość agresywny. Chciał pomóc Cezaremu pozbyć się demona i go zniewolić; skończył rozbijając ścigacz i dostając w pysk od Karoliny, na intensywnej terapii XD.
* Tadeusz Łaśnic: łasica i szczur, 'joiner'; z radością zbiera notatki na swoich pobratymców by się podlizać mafii. Chciał ochronić Andrzeja przed dwoma Rekinami, więc jest odważny.

### Frakcji

* Wolny Uśmiech: zadziałali zgodnie z wolą Grzymościa; jak się okazało, że w Małopsie jest problem medyczno-mafijny, wkroczyli i przejęli temat by wyczyścić.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Powiat Jastrzębski
                        1. Małopies: miejsce, które zawsze było bardziej niezależne. Nigdy nie łyknęli "dominującej tyranii optymizmu" w Pustogorze. Jest tu 'omerta' - nie wzywa się osób z zewnątrz, a na pewno nie terminusów.
                            1. Hodowla psów
                            1. Stadion sportowy
                            1. Motel

## Czas

* Opóźnienie: 4
* Dni: 2

## Inne

Od MG: ten moment sesji w którym Andrzej się oddala od Pauliny i to mówi postaciom Graczy jest momentem, w którym Gracze byli zainteresowani samą sesją. Nic przedtem ich nie zainteresowało, szli z grzeczności. Od tego momentu - są zaciekawieni i idą dalej.
