## Metadane

* title: "Wszystkie sekrety Caralei"
* threads: naprawa-swiata-przez-bladawira
* motives: handel-zlym-towarem, przemyt
* gm: żółw
* players: wawrzus, zubov

## Kontynuacja
### Kampanijna

* [240110 - Wieczna wojna Bladawira](240110-wieczna-wojna-bladawira)

### Chronologiczna

* [240110 - Wieczna wojna Bladawira](240110-wieczna-wojna-bladawira)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA: 
    * Elfen Lied - Lilium
        * Pain, sorrow, torment and DEVASTATION
        * Mikiptur zaprojektowana jako pułapka na siły Bladawira, coś co zniszczy wszystko za czym Bladawir stoi
        * "Within a minute".
* Opowieść o (Theme and vision)
    * Kontratak Aureliona jest zaprojektowany w taki sposób, by przejąć Actazir - jednostkę Bladawira.
    * Na Caralei znajduje się odłamek Dewastatora - (ixiońskiego amalgamatu z Diakonki i innych magów), będący narzędziem masowego zniszczenia.
        * szczęśliwie, nie umie czarować poza małymi efektami
        * (kinetyczna osłona, wir defensywny, mechanoostrza, tysiące mechanicznych os)
    * SUKCES
        * Caralea zostanie zniszczona, coś z Dewastatora będzie przejęte
* Motive-split
    * handel-zlym-towarem: ukryty przemyt ludzi przez orilitów współpracujących z savaranami
    * przemyt: główny powód, czemu Actazir w ogóle działa w okolicy - ale uznał, że Caralea jako jednostka działająca lokalnie jest mniej niebezpieczna. Błędnie.
* Detale
    * Crisis source: 
        * HEART: "Aurelion przygotował pułapkę na statki Orbitera, które mają za mało mocy i nie są gotowe na walkę z... TYM"
        * VISIBLE: "Kolejny przemyt, najpewniej przemycają ludzi"
    * Delta future
        * DARK FUTURE:
            * chain 1: Actazir zniszczony lub przejęty
            * chain 2: Orbiter nic nie wie
        * LIGHT FUTURE:
            * chain 1: Actazir niszczy Caraleę
            * chain 2: Dewastator jest opanowany lub zneutralizowany
    * Dilemma: Actazir czy my?

### Co się stało i co wiemy

* Caralea jest jednostką modelowaną po Arcadalian; supply ship transportujący rzeczy pomiędzy bytami w kosmosie
* Actazir ma DWIE jednostki którymi musi się zająć

### Co się stanie (what will happen)

* F0: Ferrelith (model: Tezifeng)
    * Zespół sobie żartuje i robi inspekcję
    * Szukają czegoś poważnego - mają jakieś niewielkie sprawy (rzadkie leki) i mają rzadkie rośliny (!)
    * Kapitan Olaf Winiarz prosi o łaskę. Bartek chce dać im szansę, ale Damian protestuje - przemyt to poważna sprawa.
* F1: Wchodzimy na Caraleę
    * Część systemów średnio działa, mimo że statek jest dość luksusowy
    * Ukryta komora z przemytem ludzi, doskonale schowana
    * -> Znajdujemy Dewastatora i systemy ledwo działają.
    * Sceny:
        * próba przekupstwa, Caralea ma fajnych ludzi, "czemu Wy tu jesteście? O_O", nie zgadzają się parametry
    * Q:
        * czy KTOŚ zostanie zainfekowany?
        * czy uda się przetrwać?
* F2: The Devastator
    * Przebudzenie Dewastatora. 
    * Q:
        * Jak duże są straty?
        * Czy ostrzeże się bazę?
        * Czy uda się jakoś dać znać o problemie?
        * Czy Actazir będzie zainfekowana?
* CHAINS
    * .
* Overall
    * stakes
        * .
    * opponent
        * .
    * problem
        * .

## Sesja - analiza

### Fiszki

Zespół

* Kazimierz Darbik
    * OCEAN: (E- N+) "Cisza przed burzą jest najgorsza." | VALS: (Power, Achievement) "Tylko zwycięstwo liczy się." | DRIVE: "Odzyskać to, co straciłem."
* Filip Hanwir: neuromarine, ma neurosprzężenie i komputer w głowie. Nieśmiały, precyzyjny snajper. Podkochuje się w Alisie.
* Robert Zaan: wie więcej o komputerach niż ktokolwiek zapomniał. Lubi rozstawiać wieżyczki i sensory. Bob.
* Alisa Kaldien: advancer, ma mnóstwo głupich pomysłów. Lubi pokazywać ludziom że ona się zna na rzeczy a oni nie. Lojalna jak cholera.
* Damian Reklit: pragnie wielkich czynów, ale ogólnie nie jest zbyt odważny. Chce zaimponować Alisie.
* Bartek Chalir: zrezygnowany weteran, który nie wierzy w swój statek i cele. Uważa, że wszyscy są martwi.
* Oliwier Trenk: pilot, który kocha szybkie i ryzykowne manewry.
* 4 inni (Wiktor Derit)
* -> Wszyscy mają Lancery, przylecieli dwoma pintkami

Caralea:

* Marek Sivon: kapitan, ze światów Noctis. Nie lubi Orbitera. Zna swój statek na wylot, ale nie był dobry w walce.
* Jacek Ril: XO, charyzmatyczny negocjator i dyplomata. Kluczowy w delikatnych operacjach przemytniczych. Ma wiele kontaktów w różnych sektorach kosmosu.
* Ewa Terlin: była pilotka, teraz mechanik statku. Ma talent do naprawiania rzeczy i jest niezwykle lojalna wobec załogi. Poczucie humoru i optymizm.
* Tomasz Grel: były żołnierz, ma swojego starego cyber-psa na pokładzie. Ciężkie prace i ochrona.
* Kasia Lorne: specjalistka od komunikacji i szyfrowania. Nie spotyka się z ludźmi, jest po wypadku który ją oszpecił. (ONA STOI ZA DEWASTATOREM)
* Dexter Harmon: charyzmatyczny, lubi czytać książki

### Scena Zero - impl

Kapitan Olaf Rawen z Ferrelith.

Tp Z +3:

* V: Dokumenty - wyglądają na poprawne, niefałszowane i legitne.
    * Kapitan się uśmiecha lekko nerwowo
* Vr: Systemy statku pokazują, że coś się nie zgadza - obciążenie LIFESUPPORT jest za wysokie, energii za wysokie.
    * Ale... chcecie łapówkę, czy co?
    * W: "Albo nam powiesz o co chodzi albo kopiemy głębiej. Zwolniony, wydalony..."
    * Ale dałem łapówkę, w sensie, to nic ważnego. To są koty. Transportuję koty. Przemyca 4 verminusy.

Olaf pokazał miejsce gdzie są verminusy. Komu przewieźć. Pobić kapitana, do kogo ma przewieźć. Jak nie powie - zagrozić że czeka go spacer kosmiczny.

Tr Z+3:

* X: OPIERDOL. Pobił kolesia za przemyt 4 kotów. 
* (+3Og; plus STRACH, może odwiedzić rodzinę). V: Kapitan pękł. Powiedział wszystko:
    * KOMU ma przekazać Verminusy
    * OD KOGO odebrał
    * KTO z Orbitera współpracował (dlaczego przemyt w ogóle był możliwy)

Robert opowiada o stacji Saltomak:

* Był problem z potworem.
* Nie ma tam anty-anomalnych rzeczy.

Raport do kapitana, aresztowanie kolesia i niech kapitan podejmie decyzję.

15 minut później, siedzicie w brygu, kapitan Darbik Was odwiedził. Pomniejszy opieprz, po czym Was wypuścił po wyjaśnieniu.

* Tog, Hacker.
* Dobromir, Biurokrata.

### Sesja Właściwa - impl

Parę dni później. Actazir zbliża się do kolejnej jednostki - Caralea. Darbik podejrzewa niekompetencję.

Kapitan wita Was z lekkim niezadowoleniem. Dobromir szuka anomalii w dokumentach, matactw... -> to jest katastrofa. Bardzo skomplikowana sytuacja sprzętowa.

Dziecko opowiada o statku. A kapitan o historii statku.

Tp+2:

* V:
    * Ewa Terlin jest jego ulubionym człowiekiem.
    * Kasia Lorne, z nikim się nie spotyka. Bo strasznie wygląda
    * Tomasz Grel, piesek
    * Dexter Harmon, uczy
* Vr: 
    * Jacek Ril, pierwszy oficer, tata się z nim kłóci o "statek i o przyszłość"

* Kapitanie, wasz rdzeń AI jest mało wykorzystywany, wzbudziło ciekawość. Czemu nie korzystacie.
* Kapitan się krzywo uśmiechnął. Koszt energii i uszkodzony rdzeń TAI,.

Czy kapitan kręci?

Tr Z+2:

* V: najpewniej był sabotaż TAI.
    * Biurokrata wykłada karty na stół. "Strzelaliście się? Sabotaż? Przyczyny są mało prawdopodobne na idealną pracę statku".
    * (paragrafy, pogrozić - coś przewozicie, będą problemy, utrudnianie działań Orbitera to konsekwencje)
    * wydaje mi się że coś pan kręci, ukrywany sabotaż, małe wykorzystanie TAI... przemyt?
    * kapitan: (uśmiech) 
* Vr: 
    * Kapitan niechętnie ale zaakceptował że będzie współpracować.

* Robert -> TAI.
* Alisa, Filip -> manifest.

Biurokrata szuka informacji o różnych nazwiskach: kapitan, inne osoby...

* Kapitan jest byłym agentem światów Noctis.
* Kasia Lorne: kapitan ją przygarnął
* Tomasz: on był agentem Orbitera, ale się zapił i tu się odbudował.
* Jacek: człowiek majętny, z majętnej grupy, skupia się by statek finansowo działał

Tog -> Tomasz. "Powinniśmy sobie pomagać". Wypytać o co chodziło z konfliktem między 1 oficerem a kapitanem.

* V: Tomasz się wściekł.
    * "Gdy straciłem rodzinę, zostawili mnie. Nie byłem przydatnym narzędziem. Poświęcili mnie."
    * "Kapitan przygarnął na statek"
    * -> on jest lojalny dla statku, nie zrobiłby czegoś "złego".
* V: Tomasz wie, że i tak do tego dojdziecie, więc powie z nadzieją, że ZROZUMIECIE.
    * Noktiańskie TAI działają inaczej. Nasze są Ograniczone. Są zniewolone. Niewiele mogą. Ich - działają inaczej.
    * My mamy NOKTIAŃSKĄ TAI. Ona żyje. ALe jest niekompatybilna i nie działa najlepiej bo Kasia nie działa.
        * WAIT. Ale zgodnie z danymi Biurokraty Kasia powinna sobie poradzić.
    * Kasia od pół roku.
* Vz: Pierwszy oficer chciał zrobić ZMIANY w statku. Coś przesłać, coś transportować. Kapitan się nie zgadzał.
    * Kapitan się nie zgadzał, więc to musiało być coś nie tak.
    * Ania by CHCIAŁA mieć romans z kapitanem

Pierwszy oficer. Rozpytać. "Nasz komodor idzie na efekt, chcę wykazać efekt"

Tr (niepełny) Z +4:

* Vz: 
    * Jacek - "czy jest coś, co trzeba, sfałszować"
    * Statek ma potencjał. Może zarabiać. A kapitan... 
    * On dostarcza oskarżenia, dowody na lekkie rzeczy na kapitana. Mandat, nie zamknięcie. Duży mandat.
    * "Prosiłbym o rozeznanie w sprawie TAI. Sprawa najcięższego kalibru."
* X: 
    * Inni WIEDZĄ że Jacek by zdradził więc nie będą gadać z Jackiem.
* V: 
    * Jacek zostaje agentem Dobromira. Bo tu jest szansa na kasę i RoI.

Badanie rdzenia TAI.

Ex Z +4:

* X: TAI jest świadoma że jest skanowana i coś się dzieje
    * Robert zwodzi, zagaduje itp. Korzystając z niskiego rdzenia
* Vr: Tog widzi
    * Po pierwsze, ta TAI jest noktiańską TAI, żywą. TAI która była statkiem militarny.
    * TAI jest anomalna. Jest częściowo sprzężona z nieżywą żoną kapitana.
    * Kasia zrobiła kiepską robotę - wszystko wskazuje na to, że ona AKTYWNIE ograniczyła moc TAI.
    * BIA - to nie jest TAI, to Biologiczny Intelekt Autonomiczny. Zakazany i niebezpieczny.

Dobromir: zbiera haki i chce mieć rację. Chce awansować, chce zebrać akt oskarżenia, Roberta Zaana - zabezpieczyć, reakcja statku przed aresztowaniem kapitana, jak Actazir jest dostępny to oskarżyć i zamknąć.

Tog: nie chce dawać statku Jackowi, pochylić się nad tym by kapitan Marek zaufał Orbiterowi i współpracę z nim utrzymać. Bardziej przychylny dla naszej organizacji. Mamy Tomasza (były agent) - lojalny wobec załogi. Ale kto mataczy przy TAI?

Jak to się skończyło - oddali decyzję Bladawirowi. Komodor Bladawir (dowodzący kapitanem Actazir) postanowił, że Caralea zostanie jego jednostką; będzie współpracować. Pomoże ustabilizować anomalną TAI i pomoże zachować Caraleę aktualnej załodze. Tog trafi na Caraleę jako agent Bladawira.

## Streszczenie

Załoga Actazir zatrzymała przemyt verminusów na Saltomak w brutalny sposób, który jednak nie był problematyczny dla Orbitera - za co dostali opierdol od kapitana Actazir. Następnie przeszli do badań Caralei, która jest bardziej problematyczna niż normalna jednostka (problemy z TAI, problemy ze skanowaniem). Podczas rozwikływania kłopotliwej sytuacji Caralei Zespół doszedł do tego, że kapitan Caralei jest noktianinem unikającym Orbitera i mającym dobre serce; przygarnął sporo osób które miały problem. Niestety, są osoby na pokładzie Caralei skłonne go zdradzić. I - żona kapitana zintegrowała się z BIA jednostki. Bladawir dostając takie informacje pomógł Caralei - robiąc z niej swoją "sprzymierzoną jednostkę", z własnymi oczami i uszami.

## Fakty Lokalizacji

* Stacja przeładunkowa Saltomak: Był tam problem z potworem i nie ma tam anty-anomalnych rzeczy a biurokracja trwa, więc 'zaakceptowano' przemyt verminusów na Saltomak na wszelki wypadek

## Progresja

* .

## Zasługi

* Tog Worniacz: idzie koncyliacyjnie, dogaduje się z nieperfekcyjnymi załogami Ferrelith oraz Caralei; kończy na Caralei z ramienia Bladawira (i jako osoba kontrolująca TAI). 
* Dobromir Misiak: skutecznie wykorzystał swoje umiejętności biurokratyczne do odkrycia anomalii w dokumentach statku Caralea i ujawnienia sabotażu TAI (żywej TAI). Agresywny, pobił kapitana podejrzanego o przemyt.
* Olaf Rawen: kapitan Ferrelith; przemyca Verminusy na Stację Saltomak. Po pobiciu przez ludzi Darbika przyznał się do przemytu verminusów i podał pełną listę kanałów kontaktowych, ku utrapieniu Darbika (acz zadowoleniu Bladawira)
* Kazimierz Darbik: nie jest zadowolony z nadmiernej brutalności swojej załogi w sprawie przemytu; przemyt verminusów jest PRAWIDŁOWY choć nielegalny. Ale wyjaśnił co zrobili źle i pozwolił im działać po swojemu.
* Antoni Bladawir: gdy dostał problem Caralei - nie do końca prawidłowej jednostki z żywą BIA - włączył ją do swoich tajnych wspieranych jednostek i wprowadził tam Toga jako swojego człowieka.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Pierścień Zewnętrzny

## Czas

* Opóźnienie: 2
* Dni: 4
