
## Metadane

* title: "Przegrywy ratują przegrywy"
* threads: olaf-znajduje-dom, igrzyska-lemurczakow
* motives: okrutne-gry-moznych, niewolnicy-magow, magowie-bawia-sie-ludzmi, b-team, misja-survivalowa
* gm: kić
* players: żółw

## Kontynuacja
### Kampanijna

* [230618 - Reality show z zaskoczenia](230618-reality-show-z-zaskoczenia)

### Chronologiczna

* [230618 - Reality show z zaskoczenia](230618-reality-show-z-zaskoczenia)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ?
* CEL: 
    * ?

### Co się stało i co wiemy

* Kontekst
    * .
* Wydarzenia
    * .

### Co się stanie (what will happen)

?

### Sukces graczy (when you win)

* .

## Sesja - analiza
### Fiszki

* .

### Scena Zero - impl

.

### Sesja Właściwa - impl

Podczas podróży z powrotem w góry Olaf zaproponował opowiedzenie czym byli w przeszłości i czego się nauczyli. Jako przykład - opowiedział, że myślał że się stoczył po wojnie i widzi jak po wojnie może inne rzeczy jeszcze zrobić. Że wrogiem nie jest Astoria a potwory. I mogą razem coś zrobić. Sway public.

Ex Z +3 +5Or (wyzwanie od 'bogów' - jesteśmy godnie): 

* V: (podbicie): jakaś grupa publiki, ale nie tyle ile Olaf by chciał
* Vz: (manifestacja): wśród publiczności grupa która wspiera - którzy aktywnie chcą, by ten zespół wygrał
* Vr: (manifest): z perspektywy organizatorów ta grupa to czarny łabędź - AKCEPTOWALNE jest, by wszyscy przeżyli, że kogoś uratują. POZWOLĄ grać po naszemu.
* (+1Vg, Olaf chce sprawić, by ONI dali zespołowi jakiś zdewastowany zespół do uratowania i kosztu surowców) Vz: "bogowie" pójdą na to - ten odcinek będzie innego typu.

Zespół wraca w góry. Mapa:

|---------------------------------------------|
| tundra        | las deszczowy | sawanna     |
|---------------------------------------------|
| las           | pustynia      | pasmo       |
|  europejski   |               | górskie     |
|---------------------------------------------|
| tajga         | dżungla       | las         |
|               | nabrzeżna     | namorzynowy |
|---------------------------------------------|

Podczas budowania siedziby były patrole - szukamy materiałów, ale też śladów. Kilka dni zajęła konstrukcja, ale też z Romanem ustalili czy tryb patrolowy itp. I relatywnie wredne pułapki. Drugiego dnia Maks z Lily wracają z patrolu szybciej niż by się wydawało. "Coś jest dziwnego, ten teren..." (pokazał teren gdzie była polanka) "zmieniło się w mokradło. I gdzieś na środku trzęsawiska słychać wołanie o pomoc. Góra 2 osoby." Maja i Maks potwierdzili, że ten teren jest... trawersowalny, ale nie jest normalny.

* O: Tym się różnimy od potworów, że ratujemy ludzi przed potworami. Powinniśmy im pomóc.
* M: Byłem strażnikiem, wyciągam ludzi od niebezpieczeństwa.
* L: A co jak będą chcieli nas skrzywdzić?
* R (kładzie Lily rękę na ramieniu opiekuńczo)
* O: Musimy ich przekonać, że bardziej warto nam zaufać niż z nami walczyć.

Alan, Maks, Maja - jak się tam dostać i uratować, jakie rośliny, co warto wziąć. Grupa przekonywująca to: Maja, Lily i Olaf. Cel - wyjaśnić i udowodnić słowami, że sobie pomagamy wszyscy. Dziecko, niepraktyczna laska i noktianin. A jednak wszyscy współpracują. I im też pomogą. Wszyscy idą tam na pomoc, baza zostaje zapułapkowana jak cholera i:

* faza 1: przekonywujemy
* faza 2: ratujemy
* faza 3: powrót do bazy, gdzie wpierw bazę sprawdzi _scout_

Mega-konflikt. Wpierw faza 1: przekonywanie - bo nie musimy wchodzić głęboko. (+desperacja targetu, +Maja i Lily - nie tylko silni przetrwali, +formalne działania - możemy ich wintegrować w zespół) -> Tp +3

* V: (podbicie)
    * Olaf powiedział że pomogą, ale niech ich nie atakują; Lily przyznała się do "idziemy Wam pomóc, ale nie róbcie nam krzywdy"; Maja "pyta jakie rośliny widzą". Olaf wyjaśnia - przyjdą silni faceci żeby Was wyciągnąć. Ilu Was jest? Ile przygotować zestawów nosz itp? (jedna)
* X: odpowiedź brzmi "jedna" (podbicie), jest druga.
* V: (manifest) zapewnienie współpracy. Nie ma nic o których napastnik wie.

Maks i Alan mogą działać, z Lily jako lekka jednostka trudniejzatapialna. (-> Tr, +Z)

* Vz: Alan, Maks i Lily dotarli. Pojedynczy, bardzo odwodniony z oparzeniami koleś (i śladami odmrożeń), wygląda jakby właśnie wylazł z pustyni.
    * Biorą go na nosze, zbierają zasoby - są przy nim ciepłe ciuchy, dobre buty, saperka...
    * Ostatnie co on pamięta - był na pustyni.
* X: (manifestacja w drodze powrotnej do bazy - drugi przy bazie)
* V: z terenu mokradła Maja zbiera rośliny lecznicze, łagodzące... "won't cure but more comfortable".

Evac do bazy. Ostrożnie i wszystko. Dostajemy się do bazy i w jednej z pułapek - koleś. Poparzony od słońca i odmrożony. A teraz jeszcze ze złamaną nogą. Ten dla odmiany chce się bronić. Olaf zaznacza - mogą działać razem.

Antek is combative, Bogdan (ten na noszach) jest półprzytomny. Antek jest dobry w uprawie ziemniaków - średnio udany rolnik, też przegryw. Zaczęli od tundry, potem pustynia. Olaf pyta jak im poszło.

* ich zespół miał 'noktianina', ale coś nie pasującego (Roman ma snicker) - padł pierwszy, bo wszyscy za wszystkimi a on się stracił. To był księgowy.
* śnieżyca ich rozwaliła - nie znaleźli zasobów, pogoniło zwierzę, na pustyni zaczęli kolejni odpadać. Ta dwójka najbardziej wytrzymała, doszli najdalej ale nie mieli sił i czekali na śmierć.

Sytuacja jest kiepska ale stabilna. Nasz zespół ma wysokie morale. Pułapki są skuteczne.

Następny dzień, 4-5 rano. Kto stoi na warcie stoi. Nikt nie śpi. Rozlega się głos - 'nondescript', niosący się.

* GŁOS: "Uwaga zespoły. Rozgrzewka dobiegła końca. Czas na ostatni etap Waszej podróży. Bądźcie czujni, spodziewajcie się niespodziewanego, odliczanie się rozpoczęło." 

Każda osoba dostaje _proximity ping_. Na nadgarstku każdego jest opaska (jak zegarek). Nieprecyzyjny ekran radaru.

Jesteśmy w niedużej oazie nagle. XD. Uderza w nas ciepło, fala gorąca. Jesteśmy w niedużej oazie. Antek i Bogdan zaczynają lamentować. Ale to co zbudowaliśmy to jest. 

Proxi-ping mówi:

* blisko, kierunek na las europejski

Jest jeszcze jedna rzecz jaką Antek i Bogdan zobaczyli. Na pustyni stracili 2 osoby, ale zetknęli się z innym zespołem. Doszło do starcia. Weszli na pustynię z tundry, starcie (atak z zaskoczenia) ale jako że się pogubili to zobaczyli najazd z oddali. Czyli gdzieś tu są morderczy nomadzi.

Plan - idziemy w nocy do lasu europejskiego.

Antek i Bogdan (w kiepskim stanie) będą współpracować. Lily "powiedzieli, że możemy o coś poprosić. Może o coś by się schować? Jakąś zmyłkę?" Olaf "poprosimy by ich broń się zepsuła XD. To będzie śmieszne." Nagroda - materializacja manierki z wodą i napisane "dobrze kombinujecie, ale żywi stąd nie wyjdziecie". Czyli endgame, showdown na pustyni.

Mamy dwa pingi - jeden bardziej w kierunku na las europejski, drugi w kierunku na północ (las deszczowy)... i wiemy, że jeden z tych zespołów to napastnicy.

## Streszczenie

Ekipa przegrywów się ustabilizowała; zrobili bazę i nawet uratowali dwóch członków innego zespołu który ucierpiał na pustyni (jeden ze złamaną nogą, jeden ledwo żywy). Gdy już mieli plan poczekać, rzeczywistość się zmieniła i wszyscy trafili na pustynię. Wiedząc o tym, że jedna z grup to drapieżnicy pustynni, zespół przygotował się na starcie. Tamtych należy usunąć.

## Progresja

* .

## Zasługi

* Olaf Zuchwały: znając _genre_ tego reality show, atakuje samą strukturę i organizatorów wpływając na publiczność; konsekwentnie przekonuje wszystkich że warto ratować wszystkich. Poza napastnikami.
* Lily Sanarton: no-nonsense, jej obecność i słowa pomogły przekonać dwóch nieszczęśników z innego zespołu. Dobry zwiadowca.
* Roman Wyrkmycz: chroni całość bazy i zespołu przed bardziej szalonymi pomysłami Olafa, planuje jak przetrwać przeciwko rajderom pustynnym. Zastawił dobre pułapki.
* Alan Klart: z Maksem przeprowadził operację ratunkową; najsilniejszy fizycznie z ekipy
* Maks Ardyceń: z Alanem przeprowadził operację ratunkową; planuje jak przetrwać na pustyni. Pełni rolę medyka zespołowego.
* Maja Wurmramin: żywy dowód, że ten zespół sobie dobrze radzi (bo ona żyje). Dodatkowo, poznajdowała cenne zioła i rzeczy.
* Antoni Kmandir: z innego zespołu; wkradając się na teren tego zespołu złamał nogę w pułapkach. Kiedyś kiepski rolnik, nieufny wobec Zespołu, ale nie ma wyboru - bez nich nie przetrwa. Współpracuje.
* Bogdan Ubuddan: z innego zespołu; pustynia i mokradła prawie go zabiły. W większości nieprzytomny i słaby, ale Zespół mu pomaga i go ratuje.

## Frakcje

* Arbitrzy Rozwiązań Proxy: bardzo rozbawieni faktem, że zespół Olafa uważa death game za reality show. Nie przeszkadza im to czerpać z tego więcej pieniędzy klipując co lepsze teksty.
* Grupa Proludzka Aurum: pomagają zespołowi Olafa widząc, że ten zespół faktycznie próbuje uratować jak najwięcej ludzi. Zrzucają im przydatne dropy i dają dobre rady.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Lemurski
                            1. Wielka Arena Igrzysk

## Czas

* Opóźnienie: 1
* Dni: 3
