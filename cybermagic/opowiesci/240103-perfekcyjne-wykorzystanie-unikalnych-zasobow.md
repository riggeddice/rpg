## Metadane

* title: "Perfekcyjne wykorzystanie unikalnych zasobów"
* threads: triumfalny-powrot-arianny, program-kosmiczny-aurum, naprawa-swiata-przez-bladawira
* motives: energia-sempitus, the-survivor, skazenie-magiczne-maszynerii, i-will-never-join-you, tylko-cel-ma-znaczenie, handel-zlym-towarem
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [231129 - Niemożliwe nieuczciwe ćwiczenia Bladawira](231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)

### Chronologiczna

* [231129 - Niemożliwe nieuczciwe ćwiczenia Bladawira](231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA: 
    * Hughes Hall "Sleep Now" + Alien from Darkness vibe
        * "Sleep. Now."
        * Dewastacyjne wyłączenie kontroli nad głównymi elementami mentalnymi
* Opowieść o (Theme and vision)
    * Bladawir, który szukając czegoś INNEGO znalazł coś czym się warto zająć. Czegoś czego nikt normalny by nie zobaczył.
    * Bladawir, który do osiągnięcia swoich celów pokaże koszmar tego kim jest własnym podwładnym. Ma obsesję.
    * Potwór który chce przetrwać i dlatego robi straszne rzeczy ludziom, pochodząc z systemu medycznego.
    * SUKCES
        * Zniszczenie potwora z systemu medycznego.
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * energia-sempitus: potwór, który się dostał do zaawansowanej silnie zautomatyzowanej stacji przeładunkowej. Pragnie przetrwać; anomalia pochodna z systemu medycznego.
    * the-survivor: anomalny system medyczny Sempitus, który zmusza ludzi do karmienia go i przyprowadzania mu kolejnych ofiar. Bardzo dobrze ukryty i bardzo rozprzestrzeniony.
    * skazenie-magiczne-maszynerii: anomalny system medyczny Sempitus, który najpierw trzymał ludzi przy życiu za wszelką cenę a teraz sam chce przetrwać.
    * i-will-never-join-you: samobójcze działania tych, którzy są pod kontrolą Dominatora i nie chcieli go dłużej karmić.
    * tylko-cel-ma-znaczenie: Bladawir zaszantażuje Ariannę, Ewę Razalis, poświęci wszelkie relacje, nieważne. Tym razem poluje na 'rzadkie zwłoki'.
    * handel-zlym-towarem: tym razem chodzi o handel zwłokami, kluczowy element planu Bladawira by pozyskać odpowiednie zwłoki do Dark Awakening.
* Detale
    * Crisis source: 
        * HEART: "Orbiter nie patrzy na pojedyncze niewielkie mało ważne stacje przeładunkowe. Nikt nie ma siły patrzeć na wszystko."
        * VISIBLE: "Bladawir patrzy na wszystko XD."
    * Delta future
        * DARK FUTURE:
            * chain 1: 
            * chain 2: 
        * LIGHT FUTURE:
            * chain 1: 
            * chain 2: 
    * Dilemma: Bladawir - na pewno potwór, ale czy nie wsparcie?

### Co się stało i co wiemy

* Bladawir
    * Plan komodora Bladawira się rozsypał i tieni są dalej wywożeni poza Aurum. Jednak komodor zorientował się że ma do dyspozycji 2 unikalne zasoby – Ariannę i (Dark Awakening) oraz Klaudię, która potrafiła znaleźć anomalię metodami statystycznymi.
    * Bladawir pozyskał zwłoki. W ramach testu czy Arianna pozostanie mu lojalna i potrafi zrobić to co należy chce reanimować te zwłoki i przesłuchać je. W ten sposób ma nadzieję znaleźć to czego nikt inny znaleźć nie był w stanie i uratować Orbiter przed fatalnym losem.
* Kontekst sesji 
    * Po raz kolejny wracamy do konceptu z „Alien from the Darkness”. Anormalny potwór który atakuje statek kosmiczny lub członka załogi i na niego wpływa. Po pewnym czasie ten członek załogi dostaje ataku szaleństwa i dzieje się coś złego.
    * Potwór zagnieżdżony jest na stacji przeładunkowej Saltomak, zawierającej dużą ilość magazynów i służącej raczej 


### Co się stanie (what will happen)

* F1: Przygotowania
    * Klaudia znajduje korelacje
        * dziwne anomalne zachowania
        * miejsca zwłok
        * praca z Zaarą
    * Arianna musi reanimować młodego człowieka który się zabił
        * informacje o potworze
        * opcja szantażu Arianny Eleną i ostrzeżenie co jej zrobi jak stanie przeciw niemu 
    * Bladawir rozczarowany
    * Klaudia -> gdzie kupuje się zwłoki
* F2: Porwanie z Karsztarina
    * porwanie Katarzyny Misztanim
* F3: Cel - stacja przeładunkowa Saltomak
* CHAINS
    * Problemy i konflikty
        * reputacja: (Arianna na celowniku Bladawira, Arianna na celowniku Orbitera (zwłoki srs?))
        * magia: (Reanimacja, badania)
        * (atak potworów, 'dead is alive', silniejszy stres)
        * (contamination)
    * Magia
        * (Klaudia / Martyn Skażeni, nie wszystkich da się uratować, konieczność operacji ratunkowych po Martyna/Klaudię)
        * (Transfer energii do Vishaera)
* Overall
    * stakes
        * Arianna: relacja z Bladawirem i załogą, Klaudia: relacja z załogą i Zaarą
        * MediPająk: czy działa dalej niezauważony? Czy się rozprzestrzenił? Czy zaraża?
    * opponent
        * Bladawir i jego niemówienie
        * Orbiter i standardowe defensywy
        * potwór - jak go dorwać
    * problem
        * czy jest prawdziwy problem?
        * czy to moralne? XD
        * czy zaufać Bladawirowi?

## Sesja - analiza

### Fiszki

Zespół:

* Leona Astrienko: psychotyczna, lojalna, super-happy and super-dangerous, likes provocative clothes
* Martyn Hiwasser: silent, stable and loyal, smiling and a force of optimism
* Raoul Lavanis: ENCAO: -++0+ | Cichy i pomocny;; niewidoczny| Conformity Humility Benevolence > Pwr Face Achi | DRIVE: zrozumieć
* Alicja Szadawir: młoda neuromarine z Inferni, podkochuje się w Eustachym, ma neurosprzężenie i komputer w głowie, (nie cirrus). Strasznie nieśmiała, ale groźna. Ekspert od materiałów wybuchowych. Niepozorna, bardzo umięśniona.
    * OCEAN: N+C+ | ma problemy ze startowaniem rozmowy;; nieśmiała jak cholera;; straszny kujon | VALS: Humility, Conformity | DRIVE: Uwolnić niewolników
* Kamil Lyraczek
    * OCEAN: (E+O-): Charyzmatyczny lider, który zawsze ma wytyczony kierunek i przekonuje do niego innych. "To jest nasza droga, musimy iść naprzód!"
    * VALS: (Face, Universalism, Tradition): Przekonuje wszystkich do swojego widzenia świata - optymizm, dobro i porządek. "Dla dobra wszystkich, dla lepszej przyszłości!"
    * Styl: Inspirujący lider z ogromnym sercem, który działa z cienia, zawsze gotów poświęcić się dla innych. "Nie dla chwały, ale dla przyszłości. Dla dobra wszystkich."
* Horst Kluskow: chorąży, szef ochrony / komandosów na Inferni, kompetentny i stary wiarus Orbitera, wręcz paranoiczny
    * OCEAN: (E-O+C+): Cichy, nieufny, kładzie nacisk na dyscyplinę i odpowiedzialność. "Zasady są po to, by je przestrzegać. Bezpieczeństwo przede wszystkim!"
    * VALS: (Security, Conformity): Prawdziwa siła tkwi w jedności i wspólnym działaniu ku innowacji. "Porządek, jedność i pomysłowość to klucz do przetrwania."
    * Core Wound - Lie: "Opętany szałem zniszczyłem oddział" - "Kontrola, żadnych używek i jedność. A jak trzeba - nóż."
    * Styl: Opanowany, zawsze gotowy do działania, bezlitośnie uczciwy. "Jeśli nie możesz się dostosować, nie nadajesz się do tej załogi."
* Lars Kidironus: asystent oficera naukowego, wierzy w Kidirona i jego wielkość i jest lojalny Eustachemu. Wszędzie widzi spiski. "Czemu amnestyki, hm?"
    * OCEAN: (N+C+): bardzo metodyczny i precyzyjny, wszystko kataloguje, mistrzowski archiwista. "Anomalia w danych jest znakiem potencjalnego zagrożenia"
    * VALS: (Stimulation, Conformity): dopasuje się do grupy i będzie udawał, że wszystko jest idealnie i w porządku. Ale _patrzy_. "Czemu mnie o to pytasz? Co chcesz usłyszeć?"
    * Core Wound - Lie: "wiedziałem i mnie wymazali, nie wiem CO wiedziałem" - "wszystko zapiszę, zobaczę, będę daleko i dojdę do PRAWDY"
    * Styl: coś między Starscreamem i Cyclonusem. Ostrożny, zawsze na baczności, analizujący każdy ruch i słowo innych. "Dlaczego pytasz? Co chcesz ukryć?"
* Dorota Radraszew: oficer zaopatrzenia Inferni, bardzo przedsiębiorcza i zawsze ma pomysł jak ominąć problem nie konfrontując się z nim bezpośrednio. Cichy żarliwy optymizm. ŚWIETNA W PRZEMYCIE.
    * OCEAN: (E+O+): Mimo precyzji się gubi w zmieniającym się świecie, ale ma wewnętrzny optymizm. "Dorota może nie wiedzieć, co się dzieje, ale zawsze wierzy, że jutro będzie lepsze."
    * VALS: (Universalism, Humility): Często medytuje, szuka spokoju wewnętrznego i go znajduje. Wyznaje Lichsewrona (UCIECZKA + POSZUKIWANIE). "Wszyscy znajdziemy sposób, by uniknąć przeznaczenia. Dopasujemy się."
    * Core Wound - Lie: "Jej arkologia stanęła naprzeciw Anomaliom i została Zniekształcona. Ona uciekła przed własną Skażoną rodziną" - "Nie da się wygrać z Anomaliami, ale można im uciec, jak On mi pomógł."
    * Styl: Spokojna, w eleganckim mundurze, promieniuje optymizmem 'że z każdej sytuacji da się wyjść'. Unika konfrontacji. Uśmiech nie znika z jej twarzy mimo, że rzeczy nie działają jak powinny. Magów uważa za 'gorszych'.
    * metakultura: faerilka; niezwykle pracowita, bez kwestionowania wykonuje polecenia, "zawsze jest możliwość zwycięstwa nawet jak się pojawi w ostatniej chwili"

Castigator:

* Leszek Kurzmin: OCEAN: C+A+O+ | Żyje według własnych przekonań;; Lojalny i proaktywny| VALS: Benevolence, Humility >> Face| DRIVE: Właściwe miejsce w rzeczywistości.
* Patryk Samszar: OCEAN: A+N- | Nie boi się żadnego ryzyka. Nie martwi się opinią innych. Lubi towarzystwo i nie znosi samotności | VALS: Stimulation, Self-Direction | DRIVE: Znaleźć prawdę za legendami.
    * inżynier i ekspert od mechanizmów, działa świetnie w warunkach stresowych

Siły Bladawira:

* Antoni Bladawir: OCEAN: A-O+ | Brutalnie szczery i pogardliwy; Chce mieć rację | VALS: Power, Family | DRIVE: Wyczyścić kosmos ze słabości i leszczy.
    * "nawet jego zwycięstwa mają gorzki posmak dla tych, którzy z nim służą". Wykorzystuje każdą okazję, by wykazać swoją wyższość.
    * Doskonały taktyk, niedościgniony na polu bitwy. Jednocześnie podły tyran dla swoich ludzi.
    * Uważa tylko Orbiterowców i próżniowców za prawidłowe byty w kosmosie. Nie jest fanem 'ziemniaków w kosmosie' (planetarnych).
* Kazimierz Darbik
    * OCEAN: (E- N+) "Cisza przed burzą jest najgorsza." | VALS: (Power, Achievement) "Tylko zwycięstwo liczy się." | DRIVE: "Odzyskać to, co straciłem."
    * kapitan sił Orbitera pod dowództwem Antoniego Bladawira
* Michał Waritniez
    * OCEAN: (C+ N+) "Tylko dyscyplina i porządek utrzymują nas przy życiu." | VALS: (Conformity, Security) "Przetrwanie jest najważniejsze." | DRIVE: "Chronić moich ludzi przed wszystkim, nawet przed naszym dowódcą."
    * kapitan sił Orbitera pod dowództwem Antoniego Bladawira
* Zaara Mieralit
    * OCEAN: (E- O+) "Zniszczenie to sztuka." | VALS: (Power, Family) "Najlepsza, wir zniszczenia w rękach mojego komodora." | DRIVE: "Spalić świat stojący na drodze Bladawira"
    * corrupted ex-noctian, ex-Aurelion 'princess', oddana Bladawirowi fanatyczka. Jego prywatna infomantka. Zakochana w nim na ślepo.
    * afiliacja magiczna: Alucis, Astinian

### Scena Zero - impl

.

### Sesja Właściwa - impl


Bladawir 3 dni nie mógł się pozbierać. Zaara nie opuszczała go na krok. Kapitanowie robili co powinni, ale od dywaniku na dywanik. Po 3 dniach Bladawir wydobył się ze swojej kwatery, wygląjąc jeszcze bardziej nieskazitelnie i request do Klaudii.

* Bladawir: Porucznik Stryk?
* Klaudia: Tak jest?
* Bladawir: Podobno masz umiejętności analizy danych. Szukania anomalii w danych. Co udowodniłaś, znajdując anomalię metodami statystycznymi.
* Klaudia: (czeka)
* Bladawir: Mam dla Ciebie dane, które przeanalizujesz z Zaarą. Szukasz anomalii. Nie wiem do końca czego.
    * (po minie Zaary, Bladawir wie czego szuka Klaudia)
* Bladawir: Zaara wejdzie na pokład Inferni. My lecimy w kierunku na Neotik. Zaara jest p.o. komodora dopóki mnie nie ma.
* Klaudia: Mogę spytać co jest powodem przeskoczenia łańcucha dowodzenia?
* Bladawir: Kaprys komodora. (chwilę myśli) Zaara wie jedną rzecz której nie wie Kapitan Verlen. Muszę móc działać szybko jeżeli mam rację. Nie daję jej dowodzenia nad jednostką - nie mam uprawnień. Ale ona wie na jaki sygnał reagować. Zrozumiano?
* Klaudia: Reagować w jaki sposób?
* Bladawir: Żeby Infernia dołączyła do 'Adilidam'. Bądźcie w pogotowiu.
* Klaudia: Jak rozumiem, kpt Verlen otrzyma rozkazy?
* Bladawir: Tak. Informuję Ciebie, bo inaczej nie będziesz współpracować jak powinnaś. Widziałem wielokrotnie, jak robiłaś ruchy przeciwko innym oficerom Orbitera używając zasad i prawa.
* Klaudia: Rozumiem.
* Bladawir: Jak znajdziesz anomalię, NIEWAŻNE JAK MAŁĄ, daj mi znać natychmiast. Daj Zaarze znać natychmiast. Nieważne jak subtelna ta anomalia by nie była. Daj Zaarze znać NATYCHMIAST, nawet jakbyś miała jej przeszkadzać.
* Klaudia: Rozumiem oczekiwaną akcję, nie rozumiem przyczyn tego polecenia.
* Bladawir: (uśmiech) Jako bonus, znajdź miejsce, gdzie można zakupić zwłoki.
* Klaudia: (obie brwi do góry)
* Bladawir: Zaara wprowadzi Cię w szczegóły.
* Klaudia: Gdzie się udajesz, panie komodorze?
* Bladawir: Okolice Neotik. Wasz inżynier ma przyspieszyć Infernię zewnętrznymi silnikami.

Bladawir nie patrzy na Klaudię. Klaudia ostrzega Ariannę na hipernecie "Bladawir znowu coś odpierdala". Arianna: "Wiem, w jakim zakresie? Nie wiem czy kiedykolwiek przestaje mu odpierdalać." Klaudia: "Znajdź mi sprzedaż zwłok..." Arianna: "DLA CIEBIE CZY DLA BLADAWIRA?!" Klaudia: "Po co mi?!" Arianna: "Dla eksperymentów i anomalii?!" Klaudia: "WYBIERAM CO ŁADUJĘ DO TOREBKI, OK?" Arianna: "Bardziej mnie niepokoi że Bladawir chce zwłoki nie że Ty chcesz zwłoki..."

Klaudia: "Cieszę się, że pani kapitan rozumie problem..."

Wiadomość do Arianny od Bladawira.

* Bladawir: "Jak rozumiem, zdecydowałaś się zapewnić swoje unikalne umiejętności dla mojej grupy wydzielonej. Czyli, nie oddaliłaś się z mojej grupy."
* Arianna: "Mogę się od pana wiele nauczyć"
* Bladawir: "Słusznie. Infernia musi być przyspieszona. Porucznik Stryk dostała polecenie..."
* Arianna: "Konkretne specyfikacje jak konkretnei?"
* Bladawir: "Potrzebuję Inferni zdolnej do dotarcia do Neotik szybko. To wszystko co musisz wiedzieć."
* Arianna: "Oczywiście... ale nie powinniśmy przestawić Inferni bliżej Neotika?"
* Bladawir: "Nie. Po prostu nie. Nikt nie spodziewa się tej prędkości Inferni. Tyle Ci wystarczy."
* Bladawir: "Dodatkowo, porucznik Stryk dostała za zadanie pracę analityczną z Zaarą. I Zaara trafia na pokład Inferni."
* Arianna: "Rozumiem"
* Bladawir: "Dodatkowo, Zaara jest moją po. Nie będzie tego wykorzystywać, chyba, że stanie się to o co chodzi - wtedy wyda rozkaz przelotu na Neotik."
* Arianna: "Nie byłoby wystarczające bym zarekomendowała lot na Neotik?"
* Bladawir: "Nie. Bo jeżeli Zaara wyda JAKIKOLWIEK INNY ROZKAZ jako moja PO, ma być unieszkodliwiona. Zrozumiano?"
* Arianna: "Tak, oczywiście"
* Bladawir: "Inne pytania, kapitan Verlen?"
* Arianna: (specka, poświęcić część systemów obronnych, walki...) - jakiego sprzętu potrzebujemy

Tr Z +3:

* X: Bladawir zauważył, że Arianna próbuje delikatnie wyciągnąć. Uśmiechnął się. Nie przeszkadza mu to.
* Vr: Arianna zorientowała się że Bladawir oczekuje działań:
    * bardzo szybkie poruszanie się
    * pancerz mniej istotny, ale siła ognia tak
    * przeciwnik nie ma dużej siły ognia, ale Bladawir oczekuje ataku z zaskoczenia albo niemożności ucieczki

Zaara wkracza na Infernię.

* Zaara: Pani kapitan (salut).
* Arianna: PO komodora (salut).
* Zaara: (się lekko zmieszała). Nie będę wchodzić w negatywną interakcję. Jestem stricte w roli analitycznej. Nic innego nie zamierzam robić.
* Arianna: Bardzo się cieszę.

Klaudia ma mnóstwo monitorów skierowanych na Zaarę - czy misja Zaary nie ma na celu analizować KLAUDII i ARIANNY, ale zwłaszcza Klaudii. Zaara dotarła do Klaudii z air-gapowanym nośnikiem.

* Zaara: Porucznik Stryk? Na tym pracujemy.
* Klaudia: Doskonale. Czego szukamy?
* Zaara: (spojrzała dziwnie) Anomalii w danych.

Klaudia chce dojść gdzie Zaara ukrywa coś w tych danych, gdzie grzebała itp. I co to za dane.

Tr Z (Zaara nie walczy) +3:

* Vr:
    * Zaara nie blokuje Klaudii. Nie ma tam typowych dla Zaary pułapek gdzie ona myśli że jest sprytna
    * Zaara FAKTYCZNIE ma dane gdzie Zaara podchodziła jakby coś szukała
    * (dane i tak w izolacji)
    * dane dotyczą podłych czynów, samobójstw, zabójstw, niewłaściwych zachowań WSZYSTKICH ludzi o których wie Orbiter
    * tam są dane mogące pogrzebać niejedną karierę
    * Zaara, widząc że Klaudia wie już na co patrzy, wyjaśnia
        * Zaara: "To są niegodziwe czyny, nawet te które były ukrywane. Zauważyłaś?"
        * Klaudia: "Owszem"
        * Zaara: "Poza rekordami Twoimi i załogi Inferni, oczywiście. Usunęłam je, bo nie są istotne dla sprawy."
        * Zaara: "Dysk jest chroniony. Dane nie mogą opuścić tego dysku."
        * Klaudia: "Jak bardzo technicznie? Mamy pamięć podręczną." (nie chce dać Bladawirowi NICZEGO co on może użyć przeciw Inferni)
        * Zaara: "Musimy użyć odpowiedniego softu i sandboxa, ale wiesz jak to się robi."
        * Zaara: "Szukamy anomalii zachowań ludzi wobec tego jak powinni się zachowywać. Szukamy ludzi, którzy... z którymi coś jest nie tak. Np. kochająca matka wypychająca dzieci przez śluzę. Szukamy ich nie wśród oficerów, a w kaście niższej. Tam, gdzie nikt nie patrzy. Pomogę Ci."
* (+2Vg) X: Zaara monitoruje każdy ruch Klaudii. O czym Klaudia wie. Klaudia nie ma jak zobaczyć niektórych rekordów by Bladawir nie wiedział że na nie patrzyłaś.
* X: Klaudia nie wie czy to jest sfabrykowane czy prawdziwe. Ale Zaara udowodniła, że to jest prawdziwa baza. To nie jest narzędzie do szantażu, ale bez Zaary Klaudia nie może jej dobrze używać. (chyba że magia)
* V: Klaudia znalazła w danych coś co Ariannie pokaże więcej o intencjach Bladawira
    * Korona Woltaren - to była niska sprawa. On serio nie wiedział. To była prywatna zemsta.
    * Bladawir zbiera haki na wszystkich, ale atakuje tych, którzy są słabi. Chce ich wzmocnić albo by opuścili działanie.
    * Bladawir działa jak wydział wewnętrzny. Jak policja polująca na skorumpowaną policję. I ma duże osiągnięcia w tej dziedzinie.
    * Ta baza nie ma kluczowych rekordów. Niekoniecznie dla tej analizy, ale wyraźnie jest to wycinek.

Klaudia i Zaara szukają tych danych. Czy znajdzie je PIERWSZA? Zaczyna szukać wzorów, przepuszcza przez system ekspercki, koreluje z danymi z K1. Interesują ją nie tylko dane o osobie ale też z gatunku "czy to osoby przyjezdne, kiedy przyjechały." Czy mieli wypady. Ruchy w korelacji z tym co zrobili lub nie. Interesują rzeczy typu "był w szpitalu". Punkty wspólne. Klaudia nie rozumie podejście Zaary, ale ma podejrzenie, że ukrytym celem Zaary jest to by Zaara się poduczyła od Klaudii. Klaudia pozwala jej zobaczyć pewne rzeczy ale nie daje jej wglądu w te zaawansowane techniki. A jako że to sandbox Klaudii...

Ex+4:

* X: Zaara widzi, że Klaudia nie wkłada 100% umiejętności. Że nie idzie na całość. Że nie ufa Zaarze.
* X: Zaara ma informacje co jak było przeglądane itp. Wyjdzie ten skan Korony Woltaren i to zlokalizuje. Ale Klaudia nie szukała nikogo "ważnego dla nich", więc Zaara tego nie ma.
* X: Zajmuje to ponad 10 dni. Tak dużo ciężkich danych.
    * Klaudia żąda więcej danych z K1
    * Zaara aprobuje. I do Arianny: 'Czy mogę w imieniu kmdr Bladawira zażądać dostępu do mocy K1 na tą operację?'
    * Arianna: 'Nie widzę problemu'
    * Zaara swoimi uprawnieniami była w stanie to załatwić. Czyli po to Bladawir zostawił jej p.o. (+3Vz)
* Vr: Klaudia i Zaara mają anomalne zachowanie i dane. Udało im się je zlokalizować w interesujący sposób. (Zaara wyraźnie nie znała tych danych)
    * Klaudia nakierowana przez Zaarę znalazła nietypowe samobójstwa.
    * "On kocha ją, i on wychodzi przez śluzę."
    * I Klaudia znalazła pattern - wśród normalnych ludzi, niekoniecznie związanych z Orbiterem. Ludzi którzy "wracają do domu". Z różnych misji.
        * WSZYSTKIE osoby są powiązane z jedną stacją przeładunkową Saltomak. Na uboczu, zupełnie nie w stronę na Neotik. To bardziej handel z Eternią.
            * nie ma bezpośredniej korelacji. Część "3 miesiące po" a część "10 miesiące po".
            * są osoby które były z tej samej jednostki które NIE popełniły samobójstwa a nie były na Saltomak.
            * ale czasem więcej niż jedna osoba, choć nie w tym samym czasie.
    * (Na zupełnie innym systemie Klaudia szuka informacje o tych jednostkach - przemyt? Ktoś działający na tej stacji? Czy jest korelacja?)
        * V: NIC nie wskazuje na jakieś przemyty, działania anty-Orbiterowe... nic w stylu Bladawira.
            * TAK wygląda to jak FAKTYCZNA ANOMALIA. Coś rzadkiego co tam jest i zabrało życie 9 osób w ciągu roku.
* X: Klaudia chciała skorzystać z okazji i znaleźć dane które dla Bladawira są bezużyteczne a Klaudii pomogą. Niestety, nie ma na to czasu.

Klaudia wrzuca to Ariannie. 

* Arianna: "Dlatego Zaara 'będziecie wiedzieć jak znajdziecie?'"
* Klaudia: "Tak. Sama nie wie."

Arianna i Klaudia gadają z Zaarą. Nacisnąć 'co Zaara wie'.

* Zaara: "Nie mogę wam powiedzieć. Nie mogę. Zakłócę próbkę." (Zaara wygląda fatalnie, nie dosypia; Klaudia nie daje 100%, może 80%. Ale Zaara daje 105%)
* Arianna: "Klaudia bada próbkę, ja nie. Możesz powiedzieć mnie. Nie zakłóci."
* Zaara: "I co to zmieni? Co zmieni że Ty wiesz. Klaudia... porucznik Stryk robi swoją robotę tak jak umie. (lekka pogarda)"
* Arianna: "Analizujecie dane z ludźmi które się nie zgadzają. Jestem specjalistką od zachowań ludzkich, może podpowiedzieć."
* Zaara: (myśli chwilę) "Dobrze. Może Ty zmotywujesz swoją oficer naukową, chodźmy gdzieś we dwie."

Kwatera Arianny

* Zaara: "Szukamy przejawów czegoś, co wygląda jak kontrola umysłów. Działania mentalisty."
* Arianna: "Mhm. Ktoś z Orbitera wpływa na ludzi?"
* Zaara: (lekko szalony śmiech) "Ja jestem mentalistką i ja mogę wpływać na ludzi. Jeśli ja mogę, ktoś inny też. Więc szukamy kogoś, kto nadużywa mocy przeciwko Orbiterowi. Ludziom których Orbiter chroni."
* Zaara: "Najlepiej schować takie działania za przestępstwami. Wiesz, taki dobry chłopak a zgwałcił koleżankę. NIKT mu nie uwierzy, zwłaszcza, jak przedtem wypił. To nie byłby pierwszy raz"
* Zaara: "Dlatego ma się nie zgadzać. Szukamy wzoru, drapieżnicy nigdy nie mają JEDNEJ akcji. To zawsze kilka. Ale są słabi. Robią błędy. Pojawia się wzór. Znajdziemy ich. Jeśli porucznik Stryk jest tak dobra jak o niej mówią. Bo nie wierzę."
* Arianna: "Dlatego dowolny inny rozkaz, jesteś pod kontrolą?"
* Zaara: "Tak. Każdego da się złamać. Każdego da się zmusić. Mnie i Ciebie. Każdego. Więc ja jestem cynglem na Ciebie, Ty na mnie. Komodor - jak zawsze - ma rację."
* Arianna: "Porozmawiam z Klaudią."

Dane o znalezisku trafiają do Bladawira. Klaudia ma swoją profesjonalną dumę. Ale te dane są przedstawione tak że Zaara by nie umiała tego tak zrobić. Klaudia pokazuje jakość danych, raportu, przedstawienia. Klaudia pokazuje, że Zaara jest "Klaudia at home" z tej perspektywy.

Tr +3:

* X: Zaara takes it personal.
* V: Bladawir jest 'impressed'. Jest zadowolony. Klaudia trafiła do jego planów.

Arianna -> Bladawir

* Bladawir: Dobra robota, kapitan Verlen. Niedługo będę.
* Arianna: Mamy podjąć działania przed przybyciem?
* Bladawir: Absolutnie nie. Więcej - wyłączcie jakąkolwiek komunikację zewnętrzną z Inferni. Nie komunikujcie się z NIKIM łącznie ze mną.
* Bladawir: Będę za dwa dni. Spotkamy się (podał koordynaty). To jest 'w próżni'.
* Arianna: Co jeśli się panu coś stanie w przeciągu dwóch dni? Informacje by móc zadziałać?
* Bladawir: (myśli). Niech Infernia prześle... (myśli). Opracuję to na przyszłość. Potrzebujecie bezpiecznego laboratorium na externalu.
* Bladawir: (smutny uśmiech) Jeżeli coś mi się stanie w ciągu dwóch dni, potraktujcie to jak zwykłą anomalię. Ale upewnijcie się, że ktoś wie że tam lecicie. Ktoś... ważny.

Dwa dni później, Adilidam i Infernia spotkały się w kosmosie. Infernia ma ze sobą prom do badań niebezpiecznych. Arianna jest zaproszona na Adilidam. Sama, z Zaarą. Tylko oni. I Infernia ma nadal mieć blokadę sygnału. Arianna jednak ma połączenie po krwi - z Leożercą Verlenem (imię po tym jak zeżarł kuzynowi pluszowego lewka). Nikt szczególny, ale Arianna musi mieć zabezpieczenie. Taki... pośledni tien Verlen. Definiuje 'the B team'.

Adilidam jest we wzmożeniu jakby przygotowywał się do walki. (sprawdzany sprzęt, testy). Adilidam przygotowuje się do ciężkiej wojny. Bladawir wita Was w kwaterach. Dwóch strażników. Lapisowanych, co od razu Arianna zauważyła.

* Bladawir: Dobra robota, kapitan Verlen. Czas na kolejną fazę.
* Arianna: Przyjemność po mojej stronie.
* Bladawir: To co teraz powiem zabrzmi dziwnie, ale jest ważne. W mojej prywatnej toaletce znajduje się nowy strój dla Ciebie. Nowy mundur. WSZYSTKO. Zgodnie z Twoimi parametrami. Czeka też wyprodukowany nowy Lancer. Będziesz musiała się całkowicie przebrać zanim przystąpimy do kolejnej fazy. Zaara, Ty też. W twoich pokojach.

Arianna się przebrała, strój jest mundurem. Arianna musiała się przebrać. Jak Arianna i Zaara dołączyły do komodora, w piątkę z dwoma strażnikami ruszyli -> Lancery. Nowe Lancery. I piątka ruszyła na prom z czymś co wygląda jak system stazy. Ktoś jest w środku.

Prom. Strażnicy stanęli w odpowiedniej pozycji defensywnej. Zaara jest gotowa. Bladawir dał rozkaz otwarcia komory stazy.

Arianna spojrzała na zwłoki. Koleś wygląda na 23 lata. Młody, sympatyczny. Bardzo martwy. Wszyscy spojrzeli na Bladawira ze zdziwieniem.

* Bladawir: 3 miesiące temu Gunnar Wurstnik popełnił samobójstwo. Nie był nikim istotnym. Pozostawił rodzinę. Rodzina nie chciała pozbywać się ciała, ale je pozyskałem.
* (wszyscy patrzą na Bladawira w szoku)
* Bladawir: Kapitan Verlen
* Arianna: Komodorze?
* Bladawir: Przywróć go. Tak jak tą TAI. (Zaara spojrzała na niego wielkimi oczami)
* Arianna: Nigdy nie próbowałam tego na człowieku.
* Bladawir: Dobry moment, by spróbować. Zgodnie z sygnaturą Twojej magii zarejestrowanej w Orbiterze jesteś w stanie to zrobić. Zdobyłem Ci zwłoki w dobrym stanie, kochane przez rodzinę.
    * (Dla Arianny - ten człowiek jest już martwy, może oddać jeszcze jedną przysługę innym)
        * Gunnar był trzecim dzieckiem, Orbiterowiec z dziada pradziada, on lubił kosmos, nie bał się ciężkiej, nie był najmądrzejszy ale pracowity i uczynny, miał silną wolę.
        * BARDZO rodzinny.
    * Arianna mówi jako starsza siostra. Ostatni raz jej potrzebny przy stacji. (+4Vr)

Ex ZM +3Ob:

* Vm:
    * Gunnar POWRÓCIŁ. Otworzył oczy. 
        * G: "Malina?"
        * Arianna: (przytula)
        * G: (przytula, potem na szybko odepchnął ale się zreflektował i znowu) "Tak, tu jesteś bezpieczna, tu jest dobrze, NIE IDŹ NA STACJĘ, Nie idź ze mną! Póki pamiętam."
        * Arianna: Coś mi grozi?
        * G: Igły. I... pająk. Wstrzykuje. Wysysa. Nie masz... przyprowadziłbym Ciebie do niego. Nieakceptowalne. Nie mogłem. Rozumiesz, nie mogłem. I nie mogłem powiedzieć.
        * Bladawir: Anomalia czy mag?
        * G: Anomalia.
        * Arianna: Co on Ci zrobił..?
        * G: Igły. Wiecznie są w snach. Igły. On chce przetrwać. Jest tylko on jeden. Ale on wejdzie Ci do głowy. Nie daj się pokłuć. Żywi się i wstrzykuje jad.
        * G: Mechaniczny pająk, nie wiem jak to nazwać. I chcesz go chronić, za wszelką cenę. I karmić. I jesteś sobą, poza... igłami w głowie. Nie pamiętasz ich nawet.
        * Zaara: Nie było tam niczego innego? Żadnego maga? Mówisz o mechanizmach, żadnej...
        * Bladawir: ZAARA! (ta się aż skuliła)
        * Zaara: Wybacz, komodorze. (Bladawir spojrzał na Ariannę ale nie zobaczył w jej oczach zrozumienia).
        * Bladawir: Kontynuuj.
        * Bladawir: Kapitan Verlen, czy jesteśmy w stanie zabrać tego człowieka bezpiecznie poza ten prom?
        * Arianna: Nie sądzę, komodorze, nie wiem jak długo go utrzymam
        * Zaara: Źródło energii podpięte.
        * Bladawir: Potrzebujemy porucznik Stryk. Kapitan Verlen, czy możesz go tu zostawić?
        * Arianna: Tak, jest niegroźny.
        * Bladawir: Wróć po porucznik Stryk i poinformuj, że można zdjąć zasadę ciszy.
        * Bladawir: Czy jesteś w stanie zaproponować porucznik Stryk czystą odzież?
        * Arianna: Oczywiście, komodorze.
        * Bladawir: Zrób to. Poczekam ile trzeba. Ta anomalia musi być rozwiązana. Orbiter zawiódł tych ludzi.
        * Arianna: Zgadzam się z panem, komodorze.
        * Bladawir: To pierwszy raz dzisiaj.

Arianna wróciła na Infernię. Tam kilkanaście testów by się upewnić że to na pewno ona.

* Lars -> Arianna: "Pani kapitan."
* Arianna: "Słucham?"
* Lars: "To spisek, prawda? Dałaś się przebadać lekarzowi?"
* Arianna: "Nie było kiedy, Larsie"
* Lars: (nacisnął cichy alarm) "To niefortunne, pani kapitan. Proponuję zrobić to szybko. Twój servar ma zapasowy zbiornik tlenu. Tam coś jest, to nie jest tlen."
* Lars: "Pani kapitan pozwoli że to przebadam?"
* Arianna: "Jestem przekonana że to coś morderczego, nie ma się czym przejmować"
* Lars: "Komodor nie ma prawa skrzywdzić NASZEJ kapitan. Bo inaczej Eustachy Korkoran będzie smutny."
* Arianna: "A wszyscy wiemy jak kończy się smutek Eustachego Korkorana..."

ARIANNA "KLAUDIO PRZEBIERZ SIĘ W CZYSTE". KLAUDIA "NIE! BĘDĘ PLEŚNIEĆ MU NA ZŁOŚĆ!"

Klaudia się przebiera jednak. Jest bardziej naukowcem niż wściekła na Bladawira. Arianna leci w servarze od Bladawira, mimo dezaprobaty Larsa Kidironusa.

Klaudia widzi Gunnara i poważną ekipę. Klaudia chce analizować ludzi którzy wracali na stację wielokrotnie (zwłaszcza z kimś) przed wylotem.

* Bladawir: Porucznik Stryk, Twój pacjent.
* Klaudia: Zdaje sobie pan komodor sprawę że to jak szukanie śladów które wilk zostawił miesiąc temu w lesie?
* Bladawir: Zajęło Ci to 10 dni. Dla kogoś gorszego jest to niemożliwe. Jeśli sobie nie poradzisz, poproszę Zaarę.
* Klaudia: (uśmiech) (Zaara zinterpretowała jako uśmiech wyższości)
* Bladawir: Jesteśmy winni to bo Orbiter wykazał się słabością i ci ludzie ucierpieli. Nie ta metoda, to inna. Coś zadziała.

Klaudia przepytuje Gunnara (to co Arianna), ale skupia się na emocji, na tym czy to bardziej 'przetrwać / zemścić / coś konkretnego', czy ludzie bo pod ręką, czy np. ci ludzie bo w konkretnym stanie emocjonalnym, czy widział innego przejętego człowieka...

Tr Z +3:

* V: Gunnar pamięta dość sporo, czar Arianny zadziałał.
    * Gunnar po prostu poszedł się odlać. Sam. Najbliższa toaleta była poza zasięgiem magazynu. Był sam i pająk napadł go 'od góry'.
    * Z opisu pająka, to wygląda trochę jak system medyczny. Skażony mechanizm medyczny. (dane do cross-reference, wydarzenia na stacji...)
    * Energia... pająk się nim karmił. Wysysa - wstrzykuje. Pająk chce PRZETRWAĆ. PRESERVE/SUSTAIN.
    * Co się nawinęło to jego.
* Vz: Query odpalone przez Klaudię wcześniej - pająk żywi się rzadziej niż raz na miesiąc patrząc na powroty itp. Ostatnie żywienie - nie wiemy, bo na Saltomak jest też rotacja ludzi.
* X: W wielu miejscach Zaara, której Klaudia weszła na ambicję miała świetne pytania i doprecyzowania. Zaara pokazała jaka jest dobra i przydatna.
    * Gunnar dał informacje o pająku; Martyn będzie w stanie powiedzieć jakie to modele medyczne.
    * Niestety, operacja czyszczenia zwłok (Bladawira) sprawia że nie da się go przebadać dobrze.

.

* Klaudia: Zazwyczaj sekwencja 'najpierw zbadać, potem modyfikować'
* Bladawir: Można pozyskać następne zwłoki. Bądź żywego. Ważne było, by udało się go przywrócić kapitan Verlen.
* Klaudia: Nie przeczę, można było wpierw przebadać
* Bladawir: Na przyszłość przeczytam ten fragment rekomendacji który dołączyłaś do raportu. (Zaara ma szeroki wyszczerz - Klaudia dostała reprymendę)

Macie więc informacje.

* Bladawir: Czyli mamy wszystko czego się możemy dowiedzieć, tak?
* Klaudia: Przy posiadanych przez nas informacjach, tak.
* Bladawir: Zapewnię Ci zdrowy egzemplarz. Żywy egzemplarz.
* Klaudia: Nie różnią się niczym jak chodzi o zachowanie, powodzenia w szukaniu...
* Bladawir: porucznik Stryk - dasz mi serię rekomendacji i te osoby zostaną pozyskane do badań.
* Klaudia: Nie wiem czy to jest konieczne. Wiemy mniej więcej czego się spodziewać, coś wiemy, nie wierzę że dodatkowa analiza dużo zmieni...
* Bladawir: Rekomendujesz starcie z anomalią?
* Klaudia: Tak, wiemy co powinniśmy wiedzieć.
* Bladawir: Kapitan Verlen, jesteś w stanie wykonać tą operację samą Infernią?
* Arianna: Tak, jesteśmy w stanie.
* Bladawir: Uważasz, że jesteście w stanie dostarczyć mi tą anomalię w wystarczająco... sprawnej formie?
* Arianna: Dostarczyć sprawną anomalię?
* Bladawir: Wyobrażam sobie sytuację, gdzie można jej użyć jako jednostki terroru.
* Arianna: Ja też sobie to wyobrażam, komodorze.
* Bladawir: (niezrozumienie) Więc... możesz mi ją pozyskać? Czy nie dasz rady?
* Arianna: Właśnie dlatego że sobie wyobrażam jak ją wykorzystać dla terroru wolę ją zniszczyć na miejscu.
* Zaara: (ze śmiechem) Ale kapitan Verlen, zostanie zniszczona po badaniach. Naukowych. Dla nauki.
* Bladawir: Zaara. (ona znowu lekko się skuliła). Przydatna jednostka łatwa do opanowania może być wykorzystywalna przeciwko wrogom Orbitera.
* Arianna: Niekoniecznie łatwo ją opanować nawet jeśli się uda ją złapać.
* Klaudia: Coś na co się nie zgodzę niezależnie od wydawanych rozkazów to wypuszczenie anomalii na ludzi i masowe zniszczenie.
* Bladawir: (wzrusza ramionami) Nie pierwszy raz użyłabyś anomalii przeciwko ludziom, z katastrofalnym dla nich skutkiem.
* Klaudia: (demonstruje akcje zniszczeń, dewastacji itp które robił Bladawir, pod przykrywką 'to ich wzmocni' itp)
* B->A: Tak wychowałaś swoją oficer?
* Arianna: Tak wychowuję moją załogę. (przekazuje do Verlena na pokładzie że jak nie wrócą, jakie dane ma przekazać dalej)
* Bladawir: Kapitan Arianno Verlen. Utemperuj swoją oficer naukową. Teraz. To, że jest niewychowana, to jest problem dowodzenia.
* Arianna: (formułka z podręcznika by uważała na słowa, i widać, że to formułka z podręcznika)
* Bladawir: (westchnął) Niech tak będzie. Wy dwie, wracajcie na Infernię. Waszym celem jest oddalić się do K1 i czekać na dalsze polecenia. Ja się zajmę anomalią.
* Arianna: W porządku, komodorze.
* Bladawir: Zaara, idziesz na Infernię (lekki wyrzut Zaary w stronę komodora). Znajdźcie kolejną anomalię. Teraz wiecie czego szukacie. Ta anomalia jest mniej istotna niż znalezienie dalszych problemów. Moi kapitanowie ją pozyskają dla mnie, bo im mogę ufać w tej kwestii że potrafią wykonywać rozkazy.
* Bladawir: Kapitan Arianno Verlen, czy mogę liczyć na to, że potrafisz wykonać takie polecenie?
* Arianna: Oczywiście, komodorze.
* Bladawir: Dziękuję. Opuścić prom.

Bladawir i jego eskorta leci z Bladawirem. Zaara, Klaudia i Arianna -> Infernię. I na Inferni Arianna decyduje się skontaktować się z Wydziałem Wewnętrznym. Celem Arianny - niech celnicy faktycznie znajdą u Bladawira coś co go utemperuje. Nałożyć nań kajdan. Na pokładzie jest Dorota (przemytniczka), mając rozkład statku Bladawira gdzie to może być ukryte.

Tr +4:

* Vr: Bladawir nie może udowodnić że to Arianna lub Klaudia. Podejrzewa, ale on zawsze podejrzewa.
* Vr: Celnicy weszli na statek Bladawira i coś znaleźli, co pokazuje że jest hipokrytą
* X: Bladawir spodziewał się tego, więc nie pozyskiwał anomalii - próbował ją zniszczyć i mu się to udało
* V: Celnicy nie znaleźli pająka, ale znaleźli coś innego - obecność możliwej do aktywacji neuroobroży i inne "zabawki" tego typu. Damning.

Pięć dni później Bladawir poprosił Ariannę na Adilidam. (W servarze w którym Arianna była były środki unieszkodliwiające). Arianna leci do Bladawira w swoim servarze, nie w tamtym.

* Bladawir: Kapitan Verlen.
* Arianna: Witam, komodorze?
* Bladawir: Jesteś inteligentną młodą damą. Tien kapitan Verlen. Arcymag, czarodziejka, księżniczka. Wszystko zawsze Ci się udaje. Jesteś pewna, że chcesz stanąć przeciwko mnie?
* Arianna: Czemu pan tak uważa, komodorze?
* Bladawir: Niczego nie uważam, to pytanie.
* Arianna: Nie, nie chcę.
* Bladawir: Dobrze. Niech takie sytuacje jak z porucznik Stryk się nie pojawiają częściej. Umiesz ją opanować?
* Arianna: Robię co w mojej mocy.
* Bladawir: Ktoś z Inferni doprowadził do czegoś, co uszkodziło moje plany.
* Arianna: Uważa pan że mam szpiega, komodorze?
* Bladawir: To jest dobre pytanie. Mam dla Ciebie tylko jedno ostrzeżenie.
* Bladawir: Fundamentalnie, kapitan Verlen, masz trzy opcje. Trzy. Pierwsza z nich to być lojalną agentką. Druga, odejść. Trzecia, stanąć przeciwko mnie.
    * Jeśli odejdziesz z Infernią... zrobiłyście dobre rzeczy dla Orbitera. Macie wartość. Dam Wam odejść. Mimo wszystko.
    * Jeśli zostaniesz, oczekuję lojalności i posłuszeństwa. Wiesz jak to wygląda.
    * Ale jeśli staniesz przeciwko mnie, nieważne - dowiem się. W końcu się dowiem. I powiem Ci co się stanie.
        * Masz kuzynkę. Niekoniecznie ją lubisz. Ale to co ją może czekać... nikomu bym tego nie życzył.
        * Ty. Nie będziesz pierwszą potężną czarodziejką nago na kolanach przepraszająca.
        * Masz Verlenów na orbicie. Bądź grzeczna lub odejdź.
* Arianna: Rozumiem, komodorze.

## Streszczenie

Bladawir dostarcza dane z jego Teczek gdzie są jakieś anomalie. Klaudia znajduje anomalie świadczące o potworze - to nie to czego Bladawir szukał, ale też się tym zajmie. Po Dark Awakening Arianny wobec jednej z ofiar Bladawir postanowił zniszczyć potwora, bo Orbiter był za słaby i zdradził tych ludzi. Ale jego metody są nieakceptowalne dla Klaudii i powiedziała mu to w twarz. Gdy Arianna TEŻ się mu postawiła, Bladawir odsunął je od operacji i sam zapolował na potwora by go przejąć. Arianna zrobiła donos do Wydziału Wewnętrznego i Bladawir bardzo ucierpiał.

## Progresja

* Antoni Bladawir: ma offlinowe dane w których ma Podłe Czyny wielu ludzi XD. Wielką Teczkę Czynów Złych i Podłych.
* Antoni Bladawir: przez działania Arianny, dostał poważne uszkodzenie od Orbitera. Patrzą mu na ręce i unieszkodliwili jego działania.

## Zasługi

* Arianna Verlen: została pod Bladawirem by go docelowo zranić; skłoniła Zaarę, by ta zahintowała że szukają efektów mentalnych. Gdy Bladawir dostarczył jej zwłoki, zrobiła Dark Awakening. Wykonuje rozkazy Bladawira, ale postawiła mu się w kwestii ukarania Klaudii. Uważając, że Bladawir nie może mieć groźnego potwora, zrobiła donos do Wydziału Wewnętrznego Orbitera i tam go bardzo ograniczyli.
* Klaudia Stryk: przeszukując niezliczone teczki Bladawira znalazła anomalię - potwora polującego na mało ważnych ludzi. Nie zdradziła co wie Infernia o ruchach Bladawira. Postawiła się Bladawirowi - nie będzie z nim pracować. Jawnie przy nim. Wypunktowując jego zbrodnie. De facto - Klaudia skłoniła Ariannę do zranienia Bladawira.
* Antoni Bladawir: czegoś szuka w okolicy Neotik i przekazał część Mrocznych Danych Klaudii i Zaarze do analizy. Po tym jak Klaudia znalazła anomalię w danych, pozyskał zwłoki dla Arianny by zrobić Dark Awakening. Gdy się okazało, że to nie to czego szuka a potwór, skupił się na jego usunięciu bo Orbiter zawiódł ludzi. Zagroził Ariannie zdrowiem jej bliskich jeśli stanie przeciw niemu. Co ważne - nie ukarał ani Arianny ani Klaudii bo uważa je za kompetentne i przydatne dla Orbitera.
* Zaara Mieralit: prowadzi dla Bladawira analizę danych z Klaudią. Nie mówi czego szukają, czuje niesmak i pogardę wobec Klaudii (która pracuje na 80%). PRAWIE się wygadała czego Bladawir szuka, desperacko jej zależy by to znaleźć.
* Lars Kidironus: podniósł Ariannie, że jej servar (od Bladawira) ma zapasowy zbiornik z gazem. Co prawda jest fanem Eustachego, ale Arianny też nie da skrzywdzić.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Pierścień Zewnętrzny
                1. Stacja przeładunkowa Saltomak: mało znacząca stacja na 50 osób gdzie zagnieździł się potwór Sempitus sformowany z komponentów medycznych

## Czas

* Opóźnienie: 3
* Dni: 14
