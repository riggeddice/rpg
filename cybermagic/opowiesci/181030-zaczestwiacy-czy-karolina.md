## Metadane

* title:  "Zaczęstwiacy czy Karolina"
* threads: progildie-zaczestwa
* motives: gra-supreme-missionforce, trudne-decyzje-zyciowe, efemeryda-zrodzona-z-emocji
* gm: żółw
* players: jur, olo, lian, kirisu

## Kontynuacja
### Kampanijna

* [181205 - Jeden dokument nie w porę](181205-jeden-dokument-nie-w-pore)

### Chronologiczna

* [181205 - Jeden dokument nie w porę](181205-jeden-dokument-nie-w-pore)

## Projektowanie sesji

### Struktura sesji: Wyścig

* **Strona, potrzeba, sukces, porażka, sposób, silnik, breakpointy**
    * Echo Damiana i Karoliny
    * Konflikt wewnętrzny w Damianie - konieczność go rozwiązania
    * Damian odejdzie z gry w Supreme Missionforce
    * Damian będzie dalej grał w Supreme Missionforce
    * Straszenie, atakowanie - docelowo destrukcja możliwości grania w SupMis
    * "moc silnika": 1k6 / 5 pkt wpływu (tylko w przód)
    * Wyścig
        * 10: potrzebne będzie zastępstwo za Damiana
        * 15: porażka na półfinale w walce przeciw Ambitnym Orzeszkom
        * 20: utrata Damiana na stałe
* **Strona, potrzeba, sukces, porażka, forma gry**
    * Zespół postaci - Zaczęstwiacy
    * "moc silnika": 1k3 / 5 pkt wpływu (tylko w przód)
    * sukces z intencją wzmocnienia toru: +2/Typowy, +3/Trudny do wybranego obszaru
    * Wyścig
        * 10: Damian przynajmniej jeszcze zagra raz czy dwa
        * 16: Damian jest gwarantowany że będzie dalej grał
* **Czemu gracze muszą w to wejść?**
    * Bez Damiana nie mają drużyny w Supreme Missionforce
    * Efemeryczne Echa ich też atakują od pewnego momentu
* **Trigger?**
    * Wpada Damian i ścigają go Echa.
* **Okrążenia**
    * Brak. Ich funkcją są tory kierowane przez Graczy.

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) poprzednia sesja - zabawa z energiami magicznymi
* Ż: (element niepasujący) Echa w kształcie Damiana i Karoliny
* Ż: (przeszłość, kontekst) Nieużytki Staszka wykorzystane jako miejsce szczeniackich przysiąg

### Dark Future

1. Damian nie będzie już nigdy grał w Supreme Missionforce

Pogoda, otoczenie:

* Wszystko podczas jednej nocy, ładna pogoda

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

* Dawno temu Damian i Karolina przyzwyczaili Prąd Magiczny pod Nieużytkami Staszka do tego, że są traktowani jako "jedna osoba" magicznie.
* Damian ma miejsce na Nieużytkach gdzie robi "przysięgi" i traktuje je jak prywatną kapliczkę. Nie wie o tym, ale magia odpowiedziała na jego prośby.
* Damian jest średnim graczem; nie ma pracy i dorabia sobie kiepskim komiksem "Kosmiczny Robot". Wygrywa w Supreme Missionforce - magia mu pomaga.
* Niedawno Damian jest rozerwany między Karoliną i Missionforce. Nie wie co robić. Chce w sumie zerwać z graniem, ale nie wie jak powiedzieć to kumplom.
* Magia odpowiedziała. Magia sprowadziła dwa Echa - Echo Damiana i Echo Karoliny.

## Misja właściwa

W **Cyberszkole**, wieczorem. Dyrektor Kruszawiecki udostępnił Zaczęstwiakom granie, bo przecież nic złego nie robią (szkoła się regeneruje sama). Ćwiczą zatem SupMis zanim będzie turniej, ćwierćfinały. Wtem wpada przerażony Damian i prosi, by go schowali. ONI GO GONIĄ. Jacy oni, o co tu chodzi? Zespół próbuje dopytać jak to możliwe, co tu się dzieje - niestety, przepytanie Damiana się nie udaje i okazuje się, że Damian zapomniał powiedzieć kluczową rzecz - "oni" umieją przechodzić przez ścianę.

Imagine this – they have fortified the door. They have done everything right. And now, the Echoes are coming through the wall. And the team has nowhere to escape. 

Suddenly, they have noticed that the ghosts (echoes) are following Damian only. So Mariusz caught Damian and jumped out of the window - from the first floor. Fortunately, no one got hurt. The ghosts followed Mariusz and Kirisu decided this might be a good moment to see what happens if he throws a mouse at the ghosts.

The ghosts were more surprised by being thrown a mouse at them then usually expected; they sizzled and disappeared. The team got out of the school, entered the car and drove after still running Mariusz.

Damian started panicking – all is vain, someone wants to kill him, he needs to get out of the town. The team has managed to calm him down, he is safe with them… But what did she do that something is trying to get him. And what was that?

Damian has no idea.

So they went to Damian’s flat. They want to see if there are any clues. Going that direction, they started calming Damian again – he is safe with them, there is no need to skip the finals because of the girlfriend. Why didn’t he come to the training? Because he was with his girlfriend. Training is more important than girlfriends. The more problem – [failed conflict] - Damian is calmer, but Karolina heard that.

So now we have a fuming girl (Tsundere type) who has heard that Damian’s friends are telling him that they have priority over her. Worse, that the game has priority over her. The game she isn’t even playing and doesn’t like.

The team has decided to calm down Karolina, and to find some proofs of the ghosts they went back to the school (never getting to the flat). During the calming [conflict, failed] it happened that Karolina will not see a ghost. She can’t see them. And she will not let Damian out of her sight…

Okay, so it happened that Michał is a streamer. It was streaming the whole of her because he was playing – so the ghosts are on the video. However, Karolina does not see them. Not even on the video. And some people in the chat see the ghosts and some people don’t.

They started interrogating Damian what’s it all about what is that he is not telling them. He has broken down, started to explain with sobbing that he wants to end playing Supreme Missionforce; he is a failure as a human and he doesn’t even understand why he is winning in the game.

Karolina helped the team here - she started convincing Damian that he is not worthless but he simply doesn’t have a job and that she really cares for him and that – indeed – he should stop playing the game but in the same time he is not worthless. The team started convincing him that he is not worthless is that he completely and totally should play the game.

And that is when the ghosts came. Again.

Karolina doesn’t see them. The streaming is still on. Mariusz has done what he is usually doing here – he held the civilians (Damian and Karolina) and jumped through the window saving them. Fun fact – Karolina does not see the ghosts. At all. From her point of view, the bus driver madman has caught her and jumped through the window with her and Damian. Therefore, she screams appropriately.

Lia has noticed something interesting – the ghosts are visually similar to Damian and Karolina.

The ghosts acted differently this time – they cut off all the escape routes for the remaining three people inside the school. Kirisu, Lia and Michał are trapped in the room with the ghosts. Michał has decided it is his time to be a hero and (knowing they will disappear when touched) tackled the ghosts at the door to make a safe passage for the others. He was wrong with his assumptions, though.

He hugged Damian - or rather his ghost or echo. Damian’s face was very smiley and quite creepy. The rest of the team managed to escape but Michał had to survive a moment. And so he did. This time the bus was waiting for them - Mariusz brought his bus.

Okay so this is like, completely insane. Why does Damian see the ghost and Karolina doesn’t? Why do some people see the ghosts and others don’t? Lia started searching to find the common pattern between the people – only people playing supreme missionforce are able to see the ghosts. The other people don’t. And this time the ghosts were aiming at other players too – it was as if they wanted to stop Zaczęstwiacy to play the game at all.

In the same time, Kirisu interrogated Damian and Karolina. He remembered that Damian used to draw things having that type of face – Damian confirmed. Those were the „helper ghosts”, ones which are supposed to help people by stopping them doing wrong things. But that means that from the points of view of the ghosts - playing the game was a wrong thing. And Karolina doesn’t see the ghosts because she doesn’t play the game.

How to make a fuming, skeptical Karolina see the ghosts and make sure this is not some kind of an elaborate trick? Lia convinced her to play a single round of the game with her. So, back to the school again.

The broken mouse (by throwing at the ghosts) was already repaired and in its place; this gave the team a bit of head scratching, but was of no concern. So Lia and Karolina played a bit - and Karolina got addicted to the game. She really, really likes it.

Now Karolina wants to play in place of Damian. But she’s completely new dish. The team has managed to convince Karolina that she should not play in the finals – they will give her a club shirt and they will mentor her and all that.

And of course, ghosts arrived again. But this time Karolina sees them. Karolina believes that all of that was true. Mariusz was ready to catch the civilians and jump out of the window, but Kirisu asked the ghosts what do they really want.

Surprisingly, the ghosts answered. They want Damian to stop playing the game. The ghosts are not really „real”. They are echoes, echoes of Damian’s emotions. You can’t reason with them. But it was possible to extract some information from them – the team has managed to deduce that they are somehow connected to Damian’s internal conflict. Mariusz said that they are going to stop playing the ghosts disappeared.

Fine. Now to solve the case.

The team is decided that the real problem is that Damian doesn’t have a job and Karolina doesn’t really want to play with him. The second part – Karolina telling him not to play – has been solved already. Now, to find a job for Damian.

It took some work, but Lia with Kirisu have managed to find a job for Damian. He became a utility graphic designer. Someone who draws things for living.

They could return to playing and the ghosts have never returned.

**Finalne tory:**

* Tor Echa: 14 (Karolina zastępuje Damiana)
* Tor Graczy: 16 (sukces)
* Ż: 16 (16)
* Gracze: 5 (6)

**Epilog**:

.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |  11     |     16      |
| Kić           |   5     |      6      |

Czyli:

* (Ż): (5) Marlena Maja Leszczyńska, aka Wiewiórka musi rozwiązać ten problem by Zaczęstwiaków nie wycofano z gry.

## Streszczenie

Esportowa gildia Zaczęstwiaków ma pewien problem - na Damiana polują Eteryczne Echa. Kilku członków gildii zostało w to wplątanych; rzucili się na pomoc Damianowi by nie tracić kluczowego gracza. Okazało się, że w to wszystko zamieszany był wewnętrzny konflikt Damiana - dziewczyna czy gra oraz czy jest nieudacznikiem. Pomogli mu znaleźć pracę i wciągnęli jego dziewczynę w granie. W ten sposób wyegzorcyzmowali duchy... znajdując pracę.

## Progresja

* Mariusz Kozaczek: polubił adrenalinę. Zaczął pracować w filmach jako kaskader. Całkiem nieźle mu to wychodzi.
* Michał Krutkiwąs: zdobył niezły szacun za to patostreamowanie - nagle wszyscy krzyczą i Mariusz łapie Damiana i skacze przez okno. Epicki performance.
* Lia Sagabello: się zaprzyjaźniła z Karoliną; najchętniej grają razem. Biedny Damian rzadziej widzi dziewczynę niż Lia ;-).
* Kirisu Gero: zainteresował się tymi sprawami poważniej - został psychologiem esportu. Świetnie wspiera całą drużynę Zaczęstwiaków.
* Damian Podpalnik: ma pracę jako grafik użytkowy i nadal jest dobrym graczem w Supreme Missionforce.
* Karolina Erenit: polubiła Supreme Missionforce i z radością gra z Zaczęstwiakami - a zwłaszcza z Lią.
* Tadeusz Kruszawiecki: wieczorami pozwala wykorzystywać Cyberszkołę ludziom, którzy robią tam fajne rzeczy póki nie psują (a szkoła się sama regeneruje).

## Zasługi

* Mariusz Kozaczek: wygląda jak dres tytan, w rzeczywistości łagodny i sympatyczny kierowca autobusu. Mistrz szybkiego biegania i ewakuacji cywilów przez skakanie przez okno.
* Michał Krutkiwąs: streamer żądny chwały i właściciel sklepu z komputerami Komputrix, rzucił się na ducha by inni mogli uciec.
* Lia Sagabello: nie ma budynku, do którego się nie wkradnie (security tester). Świetna obserwatorka; wyczaiła powiązania duchy/Damian oraz znalazła Damianowi pracę.
* Kirisu Gero: masażysta i mistrz wyciągania informacji. Wyciągnął sporo informacji z Damiana i przekonał Eteryczne Echa do rozmowy.
* Damian Podpalnik: 32-letni rysownik kiepskich komiksów, członek Zaczęstwiaków i zapalony gracz w Supreme Missionforce. Przypadkiem wezwał Eteryczne Echa, które miały odebrać mu możliwości grania w SupMis.
* Karolina Erenit: młoda tsundere, studentka sieci i internetu. Uważa, że internet jest DZIWNY. Chce zniechęcić chłopaka (Damiana) do gry w Supreme Missionforce.

## Frakcje

* Zaczęstwiacy Multivirtu: drużyna e-sportu SupMis pozyskała nowego gracza, acz nooba: Karolina Erenit. Stracili weterana - Damian Podpalnik.
* Zaczęstwiacy Multivirtu: dyrektor Kruszawiecki wieczorami pozwala wykorzystywać Cyberszkołę wszystkim, którzy robią tam fajne rzeczy póki nie psują.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo:
                                1. Cyberszkoła: wiecznie regenerująca się kwatera główna Zaczęstwiaków; tam odbywa się większość turniejów esportowych w okolicy
                                1. Sklep Komputrix: niewielki sklep z komputerami wyspecjalizowany w obszarach esportu, własność Michała Krutkiwąsa
                                1. Nieużytki Staszka: gdzieś tam znajduje się tymczasowa "kapliczka" Damiana i Karoliny, która odpowiada magią na ich prośby i pragnienia
                                1. Osiedle Ptasie: mieszka tam Damian; miejsce zawsze spokojne, acz wyjątkowo nawiedzane przez Eteryczne Echa chcące by Damian przestał grać

## Czas

* Opóźnienie: 2
* Dni: 1

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

Ta sesja jest prototypem omówionego przez Vizza "startu z grubej rury". Niech sesja zaczyna się dupnięciem.

Plus, przerzucenie Graczom Wpływu i Wyścigu.

## Wykorzystana mechanika

1810
