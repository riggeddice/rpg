## Metadane

* title: "Supersupertajny plan Loreny"
* threads: rekiny-a-akademia, dysonans-diskordii
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [220222 - Płaszcz ochronny Mimozy](220222-plaszcz-ochronny-mimozy)
* [220101 - Karolina w Ciężkim Młocie](220101-karolina-w-ciezkim-mlocie)

### Chronologiczna

* [220222 - Płaszcz ochronny Mimozy](220222-plaszcz-ochronny-mimozy)

## Plan sesji
### Theme & Vision

* Ci, którzy przychodzą pomóc często są większym problemem niż wrogowie (Marsen -> Lorena)
* Ci, którzy szkodzą nie zawsze nienawidzą (Marsen -> Lorena)

### Co się wydarzyło KIEDYŚ

* Keira, agentka Ernesta, przechwyciła z pustego Apartamentu grupę kultystek Ośmiornicy. A przynajmniej tak to wygląda.
    * Keira + Ernest mają przerażającą opinię wśród Rekinów (Keira is a menace); nikomu to nie przeszkadza.
    * Wszystkie osoby przechwycił Ernest z Apartamentu, m.in. Melissę - "należącą" do Chevaleresse.
* W sprawę zamieszana jest najbardziej "samotna" elegancka dama Diakonów, Mimoza Elegancja Diakon, która nie ma NIC wspólnego z Karoliną.
    * Mimoza miała jakieś działania związane z kultystkami, a przynajmniej na to wszystko wskazuje. Typowe na Diakonkę, nietypowe na Mimozę.
* TAI Hestia jest Skażona ixionem - i jest sojuszniczką Marysi
    * Przeniesienie ixiońskie Marysia -> Hestia (przez kontakt z Marysią Hestia staje się bardziej jak Marysia).
    * Hestia jest aktywną sojuszniczką Marysi i chce, by Ernest x Marysia a nie Ernest x Amelia :3
* Sowińscy są niezadowoleni, uważają to za błąd - wyślą kuzyna Jeremiego by pomógł Marysi. ALE jeszcze się nie stałe
* Rekiny się dowiadują, że Marysia stoi za "państwem policyjnym" w Dzielnicy Rekinów
* Amelia Sowińska odzyskała częściowo kontakt z Aurum i Rekinami. Przez to, że chcieli się skonsultować z nią wrt Hestii itp. Przez Hestię.
    * To też jest forma ixiońska, o czym nikt nie ma pojęcia. Amelia nie jest ekspertem, nikt tu nie jest więc "ok, udało się połączyć". Nie wiedzą jak.
        * Czyli jeśli Amelia wejdzie w esuriit w jakimkolwiek stopniu, straci kontakt z Hestią / przez Hestię.0
* Pomniejsze waśnie Marysiowe
    * Liliana Bankierz (po stronie AMZ) nie może darować Marysi "zdrady". Ani Karolinie. Ani nikomu.
    * DJ Babu (po stronie Rekinów) nie może darować Marysi "zdrady" Stasia Arienika i oddania go "tyranizującej mamie".
        * I tego że robi z Rekinów państwo policyjne.
    * Santino Mysiokornik słusznie obawia się oskarżenia o nadmierne zainteresowanie młodymi damami (zwłaszcza Chevaleresse) i chwilowo unika Marysi.

### Co się wydarzyło TERAZ

* Lorena ogólnie ma problemy i nie daje rady, więc na pomoc przybywa jej młodszy (o rok) kuzyn, który ją UWIELBIA jako osobę - Marsen.
    * Marsen wierzy, że Lorena jest mistrzynią taktyki i ma niesamowicie silne zasady
    * Marsen chce pomóc Lorenie - będzie ją chronił i będzie jej siłą ognia
    * Marsen chce naprowadzić Lorenę na jej WIELKOŚĆ MILITARNĄ. A Lorena się go boi XD.
    * Marsen zamieszkał w Podwiercie, w apartamencie prywatnym
* Marsen stwierdza, że za wszystko co złe odpowiada ERNEST. Przez niego Lorena jest biedna.
    * Marsen rozpoczyna kampanię 'eliminacja wpływów Ernesta' używając swoich umiejętności
        * Ernest próbuje wzmacniać swoje wpływy w Podwiercie, Mimoza próbuje POMÓC w Podwiercie, Marsen sabotuje ruchy Ernesta
            * Wszystko się rozbiło m.in. o działania w okolicy zakładu recyklingowgo Owczarek...

### Co się stanie

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

Sprawa zaczęła się 10 dni temu - wtedy Marsen zmienił kody Żorża używając miragenta i zaczął sabotować biznesy.

### Scena Właściwa - impl

Dzień 1:

Karo stoi w Zakładzie Recyklingu Owczarek. Właściciel ją zaprosił, z honorami. To znaczy, że coś chce - na tyle Karo zna Owczarka. Owczarek poczęstował Karo i spytał "czy są prawnicy którzy tanio pomogą wyplątać się z bardzo złego kontraktu? Bo ten mag złamał słowo i dostarcza rzeczy których nie umiemy procesować. Koszty procesowania są niewarte tych zysków. Wiesz, gra bardzo... nie fair. Próbowałem się dogadać, ale nie chce słuchać."

Owczarek ma papiery, ma wszystko - i faktycznie, w papierach powinny być środki "z typowej budowy, recykling wspierania Dzielnicy Rekinów, odzyskiwanie materiałów itp." A dostają materiały bardziej niebezpieczne. I to jeszcze z Akademii Magicznej, odrzuty. 

Więc Owczarek po prostu nie chce brać na siebie tych rzeczy magicznych. Ale mag z Eterni po prostu nie akceptuje faktu, że kontrakt nie jest dotrzymywany a oni są mali. Dlatego pyta Karo - co można zrobić. Karo, ku zdziwieniu... Ernest?

* Karo: "Osobiście on czy ktoś inny?"
* Owczarek: "Nie on, oczywiście nie on"
* Karo: "To kto?"
* Owczarek: "Jakiś Żorż."
* Karo: O_O -> pogada z nimi, to DZIWNE.
* Owczarek: "Biznes, prosta sprawa. Ale magowie... ten Ernest miał dobrą opinię i tak nas załatwić..."
* Karo: "Spoko z niego chłop"
* Owczarek: "Może nie wie... /nadzieja"

Owczarek dostarczył Karo wszystkie dokumenty i dowody. Faktycznie, dowody są mocne.

Karo podbija do Żorża. Żorż cały wesoły. Karo "co on odpiernicza z tym śmietniskiem". Żorż nie rozumie. Karo "materiały niebezpieczne". Karo rzuca przykładami. Żorż prostestuje - nie nie nie nie, żadnego AMZ. On nic nie wie. Wszystko stąd, sanityzowane.

Karo widzi różnicę - Żorż gadał miesiąc temu, Owczarek mówi że przedwczoraj. Ktoś robi wała. Karo prowadzi Żorża do Owczarka. WTF. Żorż i Owczarek dyskutują o tym, spokojnie i kulturalnie. Wynika z tego, że:

* Owczarek "ale my nie radzimy sobie z tymi"
* Żorż "ale my wam tego nie wysyłamy; te materiały szły gdzie indziej, do innego biznesu, nie do was"
* Owczarek "no patrz!"
* Żorż "rozwiążę to, nie przejmuj się."
* Owczarek, śmielej "a opłaty za uszkodzenia, zużycie sprzętu itp..." <-- tu Karo patrzy krzywo. Żorż patrzy krzywo
* Owczarek:... no dobra...
* Żorż "powinieneś być eskalować do mnie"
* Owczarek: "eskalowałem. Na ten numer co mi dałeś. Na te komunikatory, papiery..."
* Żorż <patrzy>: to nie te numery, dawałem Ci inne
* Owczarek: "no ale te numery przyniosłeś sam... 10 dni temu"

Karo jest zdziwiona. Żorż mówi prawdę, on rzadko opuszcza Dzielnicę Rekinów. Ktoś tu się podszywa. I teraz pytanie - kto i czemu. I jakie szkody wyrządził. Żorż szybko sprawdził tą drugą firmę i jest wściekły - tamci dla odmiany nie dostawali tego co mieli, więc są na pustym przebiegu i nie zarabiają a mieli...

Żorż "dzięki Karo". Karo "znam i wiem że nie robisz ludzi w banana". Żorż "ale nic nikt nie powiedział... to zwiększy koszty biznesu, musimy monitorować wszystko za każdym razem." Żorż "poinformuję Ernesta, wyślemy na to Keirę..." Karo "niekoniecznie, hold it."

Karo przekonuje Żorża, że Ernest -> Keira to TYPOWY ruch i druga strona się będzie tego spodziewać. A brak audytu i działanie Karo + Żorż oznacza, że są w stanie działać niespodziewanie.

Tr (niepełny) Z (bo Karo to przyjaciółka) +3:

* Vz: Żorż to zrobi i zarekomenduje. Ale Ernest musi wiedzieć.
* Vz: Ernest nie będzie wiedział. Żorż + Karo robią to swoimi siłami.

Karo przekonuje Owczarka, by ten zrobił haję przez telefon. Tak jakby normalnie zrobił. Bo nie ten kontrakt, itp. żąda spotkania z "Żorżem". Dzięki temu będzie się dało złapać kolesia i dojść do tego o co chodzi. A Karo zastawia pułapkę. I prosi o pomoc Daniela. Który na to "ZNOWU eternijski koleś zmusza Cię do pracy, Karo...". Ale Daniel ma okazję komuś wpieprzyć, więc...

Owczarek opieprza fałszywego Żorża. Niech ten się pojawi, niczego się nie spodziewa itp.

Tr Z (lull; przeciwnik nie spodziewa się takiego działania) +2

* X: przeciwnik spodziewa się możliwej pułapki ZE STRONY ERNESTA I Z JEGO RUCHAMI.
* Xz: przeciwnik jest przygotowany i chce zastawić swoją pułapkę.
* V: i tak przyjdzie, ale przygotowany

Karo podbija do Arkadii Verlen. Nikt się jej nie spodziewa a Arkadia to force multiplier...

* Karo -> Arkadia "ktoś robi syf, Ernest podpisał umowy, umowy ogarnia Żorż, ktoś podszywa się za Żorża i podmienia na wysypisko niebezpieczne odpadki. A ja lubię Żorża. I ludzie są narażeni na magię."
* Arkadia "Eternia. Zawsze z nimi są problemy i muszę sprzątać."
* Karo "to nie ich wina dla odmiany -> przedstawia plan, tobie mogę zaufać że nie wygadasz i będzie miało sens"

Tp (niepełna; to nie siła Karo) Z (zaufanie do Karo) +3:

* V: Arkadia jest 'in'. Jak to Arkadia, oddaje się do dyspozycji dowódcy - tu, Karo.

Arkadia nie jest niestety dobrym taktykiem. Ona jest ekspertem od wślizgnięcia się i asasynacji. Karo i Daniel się zasadzają a Arkadia w kameleon suit się chowa w sposób prawie niemożliwy do znalezienia. Jej cel - eksterminacja oficerów. A potem - multiply force. A Żorż zostaje z tyłu. Zwłaszcza, że idzie do akcji Arkadia...

Dzień 3:

Pułapka jest zastawiona. I faktycznie "Żorż" się pojawił. Karo policzyła, że "Żorż" ma ze sobą 4 osoby. Jedną z nich jest niezbyt dobrze zamaskowany Marek Samszar. Na ten widok Arkadii zwęziły się oczy. First target. Po WYMOWNYM spojrzeniu prowodyra, wpierw "Żorż", potem Samszar.

Arkadia atakuje ZANIM "Żorż" jest w stanie coś zrobić.

Tr (z uwagi na klasę przewag) Z (kameleon suit) +2 +3O (Arkadia ZAWSZE walczy na ostro):

* O: Arkadia poszła na ostro, z nożami. Prosto w Marka. Marek jest zdjęty z akcji (a to Liliana XD)
* X: Nie jest to czysta operacja. Arkadii nie udało się dorwać "Żorża" - ten ma pancerz reaktywny. Atak Arkadii po prostu nie wszedł.
* O: "Żorż" nie spodziewał się brutalności i determinacji Arkadii. Normalna osoba jest ranna i się wycofa. Arkadia jest ranna, ale weszła z nożami. Przeciwnik - Żorż - ma LANCERA. Arkadia nie spenetrowała tego, ale zaskoczenie złamane. Arkadia jest RANNA, odbiła się od Lancera.

Zaskoczenie było ogromne. Lancer został przewrócony.

Karo wezwała ścigacz i włączyła harpun. Cel prosty - złapać Lancera, powlec za sobą; nie wstanie. A jak się w coś wbije, tym lepiej.

Tr (przewaga) Z (zaskoczenie + Arkadia + lancer leży) +3:

* Vz: Lancer jest zaharpunowany i nie jest w stanie nic sensownego robić. Karo zajmuje Lancera sobą.
* V: Ucieczka przeciwnika jest niemożliwa a jego siły są zdemoralizowane. Wyraźnie widać jak wygląda sytuacja.
* Vz: Lancer został zrzucony do jakiegoś dołu do zgniatania i opuszczono tłok. To znaczy, że Lancer jest tam uwięziony.

Tymczasem Daniel rzucił się na ludzi. Większość jest zdemoralizowana, ale nie wszyscy widzą i nie wszyscy zaczynają od demoralizacji.

Tr Z (demoralizacja) +3:

* Xz: walczą jak tygrysy. Daniel dostał kilka ciosów. Jest ich sporo, nie tylko 4; byli ukryci i wyskoczyli.
* V: Daniel dał radę unikać, manewrować, odskakiwać... by nie mogli mieć czystych ciosów. I grupa zaczęła znikać.
* V: Daniel odciągnął ogień od Arkadii, która jest przecież uszkodzona. A jak Lancer został pokonany to oni zaczęli uciekać.

Ranna Arkadia, kulejąca i potrzebująca glizdy podkuśtykała z szerokim uśmiechem. Potrzebuje kilku dni w gliździe, ale to była dobra bitwa. Karo aż pokręciła głową... Arkadia jest... special type of crazy. Ale - musiała usiąść na ścigaczu, bo jest za słaba. Mimo treningu, krew to krew... ale ma NIEPEWNĄ minę. To nie był Marek. To AMZka pod iluzją. I Arkadia wolała... wolała się nie pokazywać. Bo... nie wie co robić. Zaatakowała nożem AMZkę...

Jest tam solidnie ranna Liliana Bankierz. Która była zamaskowana jako Marek. Liliana potrzebuje pomocy medycznej, ale nie natychmiast; Arkadia wie co robi. Ale Arkadia zraniła "Marka" by zadać POWIĘKSZONY ból, bo ZNOWU jej podpadł, żąda od Marka czegoś LEPSZEGO. A Liliana nie przywykła do bólu czy Arkadii i płacze żałośnie. Daniel: "ja się nią zajmę". Karo patrzy podejrzliwie. Daniel "jest ranna, odwiozę ją do Sensacjusza". Arkadia "nie do AMZ?" Daniel "nie, to będzie polityczne. Sensacjusz." Arkadia też poleci z Danielem - tu się nie przyda.

Karo i Żorż. Plus Dobrze Zamknięty i Unieruchomiony Uszkodzony Lancer.

* Karo, po komunikatorze: "otwieraj puszkę bo będziemy musieli rozłupać"
* Marsen: "kim wy w ogóle jesteście? Nie jesteś eternijską świnią"
* Karo: "nie gadam z głosami. Sam jesteś świnią za robienie nam koło pióra. W moim domu! Będziesz mi zrzucał jakieś gówna"
* Marsen: "operacja jest wymierzona w Eternię, nie w Was!"
* Karo: "dzięki wielkie za zatruwanie okolicy /sarkazm. Wyłaź"
* Marsen: "przegrałem, jestem zmuszony się poddać. Oczekuję traktowania jeńców zgodnie z porozumieniami cieniaszczyckimi. Nie jesteś z Eterni więc zaakceptuję Twoje słowo"
* Karo: "Twoja koleżanka jedzie do medyka"
* Marsen: "czy mam Twoje słowo? Nie wiem z jakiego oddziału jesteś."
* Karo: "nie mam żadnego oddziału. Nie zrobię krzywdy Tobie, ale oczekuję reperacji."
* Marsen: "<zrezygnowany> oczywiście, doskonale rozumiem..."

Koleś jest przebrany w niepozorne ciuchy, ale zidentyfikował się jako Marsen Gwozdnik, zwiadowca trzeciej klasy Zjednoczonych Sił Aurum, na urlopie. Jest wyraźnie nieszczęśliwy. Jemu nic się nie stało. Jako, że się poddał - nie ucieka, nie walczy itp.

* Marsen: "Kim wy jesteście?" /z niedowierzaniem (wpierw Lorena, teraz on... a miał Lilianę)
* Karo: "Chodź, wyjaśnię potem" /prowadzi do terenu Rekinów; Żorż posprząta sprzęt po Marsenie i pomoże z odbudową.

W domu Karo. Ta pyta o co chodziło. Marsen pyta czy są Rekinami? TAK. Marsen... to czemu pomagają Eterni? Karo - co on wie o terenie. Marsen zrobił research. Karo wyłapała poważne odchylenie od rzeczywistości. W wyjaśnieniach Marsena Lorena jest taktykiem, niezłym taktykiem i do tego Eternia podkopuje Lorenę wykorzystując do tego Marysię. Lorena jest oskarżona o bycie niewojowniczką, o jakieś... zajmowanie się SZTUKĄ. A ona organizuje ruch oporu przeciwko Eterni.

* Karo: "czy ona Cię tu przysłała?"
* Marsen: "nie, działam sam, chciałem jej pomóc - nie spodziewałem się porażki więc myślałem, że będę w stanie uderzyć by dać jej możliwość realizacji jej planów"
* Karo: "jakich planów?"
* Marsen: "gdybym wiedział, nie powiedziałbym Ci. Ale nie wiem. Lorena jest jednym z najlepszych taktyków jakich znam"
* Karo: "laska co strzelała do mnie z rakietnicy bo była pod wpływem robala?"
* Marsen: "nie wiem nic o tej sytuacji. Pamiętaj jednak - nie wiesz czy ona to ROBIŁA czy próbowała WYMANEWROWAĆ. Nie jest przypadkiem, że pojawił się Eternianin i Sowińska zaczęła się doń zbliżać i od razu próbują Lorenę zdyskredytować."      /absolutne przekonanie
* Karo <frustracja - chce oneup it i nie wie jak bo to jest tak ABSURDALNE.>
* Karo: "jak chciałeś pomóc jej planowi jak go nie znasz?"
* Marsen: "wprowadzenie chaosu na teren przejmowany przez Eternię... nie wiem jak, ale to wykorzysta. Ona jest mistrzynią adaptacji."
* Karo: "jeśli tien Namertel próbuje przejąć tereny pierwsza mu wkopię a po mnie cały wężyk. Muszę powalczyć z bratem co zrobił wpierdol kumplom. Nie pomyślałeś że Sowińska może chcieć się zbliżyć by mieć na niego oko?"
* Marsen: "kosztem MOJEJ KUZYNKI. Gardzę polityką, nie jestem taki jak Ty"
* Karo: "zniszczyłam Twojego Lancera moim ścigaczem. TO JEST MOJA POLITYKA. Chcesz się bawić codziennie?"
* Marsen: "nie doceniałem Waszych umiejętności. Ja po prostu chcę by Lorena była traktowana jak na to zasługuje"
* Karo: "rozmawiałeś z nią? XD"
* Marsen: "nie, nie mogę jej znaleźć"
* Karo: "wystarczyło wejść na ten teren i ją zapytać"
* Marsen: "i zakłócić jej plan?"
* Karo: "i tak go zakłóciłeś. Nie wiesz jaki ma plan, może to było ważne miejsce."
* Marsen: "/myśli jedynie utrudniłem jej życie, może odsunąłem jej plan odparcia Eterni o sporo czasu..."
* Karo: "niepotrzebnie tu przybyłeś..."

Kić chce, by Marsen w wyniku rozmowy myślał, że Karo jest SEKRETNĄ POPLECZNICZKĄ LORENY. Karo na to nie wpadnie.

Tr (niepełny) Z (jego wiara w Lorenę) +4:

* Xz: Marsen jest przekonany, że Marysia i Ernest są w koalicji przeciwko Lorenie. Ma DOWÓD. (or so he thinks)
* X: Marsen będzie działał, by na pewno Marysia x Ernest nie zdobyli tego terenu. Zrobi to, co Lorena na pewno robi - chroni teren. (+1Vg)
* Vg: Marsen jest PRZEKONANY że rozumie rolę Karo i PLAN LORENY - ona wprowadza mnóstwo takich agentów jak Karo.

Doszli do porozumienia i mieli nawet dobry humor. Karo bo "ludzie nie mogą być tak głupi", Marsen bo "wreszcie rozumiem sytuację, nie mogę działać w ten sposób by nie psuć Lorenie".

Marsenowi humor się szybko popsuł, gdy dowiedział się, że Żorż sprzedał Lancera. Musiał opłacić te wszystkie biznesy itp...

MARSEN VOWS REVENGE!

## Streszczenie

Marsen Gwozdnik wszedł na teren by pomóc Lorenie (która nie wie) uważając Lorenę za najlepszego taktyka na świecie (którym nie jest). Wyedytował część kontraktów okolicznych firm z Eternią, by spowodować niechęć firm do Eterni (a Ernest próbuje pomóc by Mimoza nie była jedyną która pomaga). Karo zastawiła na Marsena pułapkę i wzięła jako wsparcie Arkadię. Marsen natomiast wziął Lancera i Lilianę. Skończyło się na rannej Arkadii, sprzedanym Lancerze, rannej Lilianie i tym, że Karo przekonała Marsenę że jest... agentką Loreny?

## Progresja

* Marsen Gwozdnik: wierzy, że Lorena Gwozdnik jest najlepszym strategiem i że Ernest x Marysia walczą przeciw Lorenie i próbują ją osłabić.
* Marsen Gwozdnik: chce zniszczyć sojusz Marysia x Ernest i nie dopuścić, by oni objęli ten teren w imię Aurum.
* Marsen Gwozdnik: wierzy, że Karolina Terienak jest agentką Loreny Gwozdnik.
* Lorena Gwozdnik: Marsen wierzy, że ona jest  najlepszym strategiem i że Ernest x Marysia walczą przeciw niej. Nie wie jak się z tego wyplątać.
* Arkadia Verlen: ranna w walce z Lancerem który miał reaktywny pancerz. Tydzień kuracji, trochę z glizdą Sensacjusza. ŁA~TWE~ZWYCIĘS~TWO (szeoki uśmiech)!
* Liliana Bankierz: solidnie ranna nożem Arkadii; dobre 2 tygodnie w gliździe Sensacjusza.

### Frakcji

* .

## Zasługi

* Karolina Terienak: by ochronić lokalny biznes przed problemami z kontraktem z Ernestem poszła do Żorża i doszła do tego, że ktoś się podle podszywa. Karo zastawiła pułapkę z pomocą Arkadii i Daniela, złapała Marsena Gwozdnika i miała zwis - koleś zupełnie źle widzi Lorenę (swoją kuzynkę). Wmówiła mu, że Karo jest agentką Loreny i Marsen ma nie szkodzić terenowi bo plan Loreny.
* Władysław Owczarek: podpisał kontrakt z Ernestem na recykling, ale dostaje złe części i boi się eskalować, więc poprosił o mediację Karolinę. Pomógł Karo zastawić pułapkę na prawdziwego sabotażystę - Marsena.
* Żorż d'Namertel: wrobiony przez Marsena w to, że niby Eternia próbuje zrujnować mały biznes podwiercki; współpracując z Karo zastawił pułapkę na Marsena i nie powiedział nic Ernestowi. Sprzedał Marsenowi Lancera.
* Daniel Terienak: pomógł siostrze zastawić pułapkę na osoby atakujące mały biznes podwiercki (bo siostra); pokonał kilku ludzi w walce wręcz, demoralizując ich.
* Arkadia Verlen: PROPER PSYCHO. By chronić mały biznes podwiercki z Karo, uderzyła nożem w "Marka" (to była Liliana) a potem zmierzyła się z Lancerem w reaktywnym pancerzu przewracając go. Ranna, ale "warto było".
* Liliana Bankierz: chciała uderzyć w Marysię i Ernesta (za Lorenę) i pomóc Marsenowi, acz nie wiedziała co Marsen robi i co planuje dla lokalnego biznesu (bo by nie pomogła). Zailuzjowała Marsenowego lancera i sama poszła jako Marek Samszar. Zaatakowana przez Arkadię nożem z zaskoczenia, skończyła w gliździe.
* Marsen Gwozdnik: pojawił się dyskretnie w Podwiercie i uznał, że Marysia x Ernest chcą przejąć teren (a on nienawidzi Eterni) i atakują Lorenę. Więc pozmieniał część kontraktów biznesowych między Ernestem i lokalnym biznesem by zastawić pułapkę na Eternię i wzmocnić Lorenę (podejrzewa że ona ma Chytry Plan). Arkadia pokonała jego Lancera, Karo z nim porozmawiała i wzięła do tymczasowej niewoli.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Kompleks Korporacyjny
                                    1. Zakład Recyklingu Owczarek: podpisał kontrakt z Ernestem ale Marsen wrzuca niewłaściwe typy rzeczy do recyklingu i puryfikacji. Marsen zastawił pułapkę ale Karo zastawiła kontrpułapkę. Marsen skrojony.

## Czas

* Opóźnienie: 5
* Dni: 4

## Konflikty

* 1 - Karo przekonuje Żorża, że Ernest -> Keira to TYPOWY ruch i druga strona się będzie tego spodziewać. A brak audytu i działanie Karo + Żorż oznacza, że są w stanie działać niespodziewanie.
    * Tr (niepełny) Z (bo Karo to przyjaciółka) +3
    * VzVz: Żorż odda plan Karo, działają dyskretnie bez Ernesta by odwrócić uwagę WROGA.
* 2 - Karo przekonuje Owczarka, by ten zrobił haję przez telefon. Zastawił pułapkę na WROGA. Zaatakują z zaskoczenia.
    * Tr Z (lull; przeciwnik nie spodziewa się takiego działania) +2
    * XXzV: przeciwnik przyjdzie, ale spodziewa się pułapki na pułapce.
* 3 - Karo podbija do Arkadii Verlen by ta pomogła w zastawieniu pułapki na maga antyludziowego. Nikt się jej nie spodziewa a Arkadia to force multiplier...
    * Tp (niepełna; to nie siła Karo) Z (zaufanie do Karo + zgodne z linią Arkadii) +3
    * V: Arkadia jest 'in'. Jak to Arkadia, oddaje się do dyspozycji dowódcy - tu, Karo.
* 4 - Pułapka jest zastawiona. I faktycznie "Żorż" się pojawił. Arkadia atakuje ZANIM "Żorż" jest w stanie coś zrobić.
    * Tr (z uwagi na klasę przewag) Z (kameleon suit) +2 +3O (Arkadia ZAWSZE walczy na ostro)
    * OXO: Arkadia na ostro z nożami (ciężko raniła Lilianę), "Żorż" nie spodziewał się ataku Arkadii w Lancerze, Arkadia ranna ale obaliła Lancera
* 5 - Karo wezwała ścigacz i włączyła harpun. Cel prosty - złapać Lancera, powlec za sobą; nie wstanie. A jak się w coś wbije, tym lepiej.
    * Tr (przewaga) Z (zaskoczenie + Arkadia + lancer leży) +3:
    * VzVVz: zaharpunowany Lancer, uwięziony w zgniatarce, siły Lancera zdemoralizowane
* 6 - Tymczasem Daniel rzucił się na ludzi. Większość jest zdemoralizowana, ale nie wszyscy widzą i nie wszyscy zaczynają od demoralizacji
    * Tr Z (demoralizacja) +3
    * XzVV: Daniel odciągnął ich od Arkadii, acz dostał kilka ciosów. Zmusił ich do ucieczki.
* 7 - Kić chce, by Marsen w wyniku rozmowy myślał, że Karo jest SEKRETNĄ POPLECZNICZKĄ LORENY. Karo na to nie wpadnie.
    * Tr (niepełny) Z (jego wiara w Lorenę) +4
    * XzXVg: Marsen przekonany, że Marysia x Ernest w koalicji z Loreną i działa przeciw nim. Będzie chronił teren. Ale uważa, że Karo i Lorena są w sojuszu XD

## Kto

.