## Metadane

* title: "Sympozjum Zniszczenia"
* threads: legenda-arianny
* gm: żółw
* players: kić, kapsel

## Kontynuacja
### Kampanijna

* [210106 - S.O.S z haremu](210106-sos-z-haremu)

### Chronologiczna

* [210106 - S.O.S z haremu](210106-sos-z-haremu)

## Punkt zerowy

### Dark Past

.

### Opis sytuacji

.

### Postacie

.

## Punkt zero

.

## Misja właściwa

Maria Naavas odwiedziła Eustachego. Powiedziała mu kim jest - pierwszy oficer Żelazka. Chce Arianna x Olgierd. I Eustachy jej w tym pomoże. Inaczej ona musi porwać Eustachego, by Arianna i Olgierd mogli stoczyć przeciwko sobie kolejny pojedynek. A może Arianna nie wygrać. Eustachy nie do końca wie jak adresować Marię - chciałby jej przysolić, ale nie wie czy to bezpieczne. Zgodził się jej pomóc bo NIE WIE co zrobić - pomoże Marii wprowadzić plan gdzie Arianna idzie do kapsuły by zamknęli się tam z Olgierdem.

Maria nie powiedziała mu, że zamknie Olgierda i Ariannę na 3 dni, uszkodzi life support (by było zimno i by mieli się przytulać). Cóż. Nie wyszło - Arianna użyła mocy arcymaga i by się nie przytulać podtrzymywała ciepło, jak padła z sił to Olgierd zadziałał i trzymał, by Arianna nie marzła jak śpi. Maria skończyła smutna; jej plany będą kontynuowane.

Maria dała w prezencie Eustachemu informację - Elena jest na niego zła, bo złamał serce Rozalii. WTF. Nie, Maria nic na to nie może poradzić. Maria obiecała Eustachemu, że jeszcze się do niego odezwie. Mistrzyni mimikry, leczenia i terminacji oddaliła się z urokiem, pozostawiając Eustachego w lekkiej (lol) konfuzji.

JEDNAK gdy Maria kombinowała nad związkiem Arianny i Olgierda, Eustachy, Martyn, Klaudia, Elena i Diana byli na Bakałarzu - zaproszeni do pomocy w zniszczeniu konkretnego materiału z Anomalii Kolapsu. Anomaliczny materiał, którego tak jakby "nie ma", niemożliwy do interakcji. Anomalia rozpraszająca i pochłaniająca. I jak do tego podejść?

Oczywiście, że Eustachy się ucieszył. On sobie z tym na 100% poradzi. Klaudia weszła w głębszą analizę (XV) i odkryła, że jest sposób by sprawić by to nie zniknęło i nie rozpraszało. Esuriit. Jak i dlaczego - cholera wie, ale jest coś takiego. A to jest jedna rzecz co do której Eustachy się **nie** posunie by to zrobić. Klaudia też nie.

Ale Eustachy wie, że Diana się do tego posunie - dla niego. Powiedział "jeśli tego nie zniszczę, skończę w habicie".

Diana w nocy znalazła miłośnika imprez i dobrej zabawy. Zaprowadziła go do laboratorium zespołu Inferni na Bakałarzu. Ekranowała laboratorium, rozebrała się i skrępowała młodzieńca. Potem się znowu ubrała ;-). Konflikt HEROICZNY:

* XXXXOOV:

Diana się nie powstrzymała. Dla Eustachego. Bardzo ciężko skrzywdziła kolesia; wywołała echo Esuriit, anomaliczne echo Esuriit, Skaziła laboratorium. Nie była dość silna by to osiągnąć, nawet Esuriit, więc szła dalej i dalej i dalej a jej Wzór się Skażał, Skażał i Skażał... a ona szła dalej i dalej...

Rano znalazł ją Eustachy. Wyciągnął ją z transu Esuriit (Tr): VVV - udało mu się ją przekonać by przerwała i by pozwoliła na pomoc lekarską sobie i jemu ("are you happy, my love?"), ale musiał ją pocałować. Namiętnie. Ona wygrała. Powiedziała mu, że będzie go mieć, Eustachy będzie jej. Będzie w jej złotej klatce. Zamknie go jako swojego sensei i mistrza... Eustachy był _creeped out_.

NATYCHMIAST wezwali Martyna, dyskretnie. Ten ma jeden z najtrudniejszych problemów. Pomóc facetowi i Dianie (ExM):

* VVV: wyczyszczone, naprawione
* XX: Diana 2 miesiące z akcji, koleś miesiąc

Martyn zrobił STRASZNY opierdol Eustachemu. To **on** dowodzi tą operacją. To **on** sprowokował Dianę. On musi się nią zajmować. Musi ją kierować. Jest za nią odpowiedzialny. Eustachy jest zaskoczony; Martyn nigdy tak nie opieprzał nikogo. Obiecał, że naprawi sprawę jak tylko Diana wróci. Martyn zauważył, że Dianie udało się wrócić tylko z uwagi na jej fanatyczne oddanie Eustachemu.

Tymczasem Klaudia skupia się na tym - kim był ten koleś? Co to za jeden? Jak go podłożyć?

* XXX: to jest arystokrata. Trochę degenerat, lubił zabawę; zna Dianę
* V: zakradli się, robili nieostrożny niszczycielski seks i doszło do problemu bo tam były badania Esuriit. To wypadek, nie działania świadome.
* X: koleś wyleciał. Zwolniony, wyrzucony z floty. A Diana dostaje reputację toksycznej; jest groźna dla innych i na imprezach
* V: Diana ma opierdol roku, kuratora i psychologa. 

Czyli skończyło się w miarę dobrze. Jeszcze Klaudia by zmniejszyć Skażenie zdecydowała się jakoś to zawrzeć i zamknąć - udało jej się (XXXVVVO) zrobić samoładującą się broń, która Skaża użytkownika i ma straszne własności. Ale udało się "to nie my".

Rozpełzła się korupcja Esuriit - Diana i Klaudia. W tym momencie Elena NATYCHMIAST ostrzegła innych na Bakałarzu (nie mówiąc skąd się to wzięło); nie udało jej się zatrzymać korupcji. Ale Bakałarz jest wypełniony ekspertami od nullifikacji i puryfikacji (Sympozjum). Elena i Zespół zostali odsunięci na bok; niech dzieci nie przeszkadzają. No i na Elenę spadło odium - to na pewno przez nią, biedaczka w końcu nie kontroluje magii. A Elena wie, że MUSI odwrócić uwagę od Diany w chwili obecnej, więc zagryza zęby (XXVXX)...

Dobra. Eustachy ma dość. Stracił Dianę, opierdol od Martyna, potraktowany jak plebs... MUSI wygrać i zniszczyć ten anomalny materiał. Używając źródła energii zrobionego przez Klaudię zmontował broń. (VVVOO). Poświęcenie + Nienawiść. Zasilane Esuriit działo strumieniowe, ręczne. Emiter Plagi Nienawiści. Broń przeklęta, zdolna do niszczenia i zarażania ludzi w promieniu plagą nienawiści. Ta broń pomogła zniszczyć anomalny materiał.

Honor uratowany. A przeklęta broń trafiła na Infernię.

## Streszczenie

Maria Naavas próbuje spiknąć Ariannę i Olgierda i do pomocy używa Eustachego. Później - Eustachy, Diana, Klaudia i Elena są na Sympozjum Zniszczenia OA Bakałarz. Próbują przebić się przez anomalną materię. Diana odkrywa słabość owej materii do Esuriit i się Skaża; Skażenie rozlewa się po Bakałarzu i Zespół dostaje opiernicz za niekompetencję (większość na Elenę). Zespół w gniewie, nienawiści i Esuriit fabrykuje Emiter Plagi Nienawiści - nową broń na pokład Inferni, piekielnie niebezpieczną...

## Progresja

* Elena Verlen: wszystkie niepowodzenia z Esuriit i z opanowaniem tego przez Zespół spadły na nią, mimo, że nie ma z tym nic wspólnego. Duży cios reputacyjny; sama z tego już nie wyjdzie.
* Elena Verlen: bardzo, bardzo rozczarowana Eustachym i jego podejściem do Diany. Nie zatrzymał jej. Nie opanował jej. I wszystko spada na Elenę. A mógł zadziałać.
* Diana Arłacz: dwa miesiące kuracji po Esuriit. W trakcie - psycholog. Potem - kurator. Plus zniszczona reputacja; persona non grata na imprezach. Ogólnie, tragedia. Ale wymusiła całus Eustachego. Warto było? XD
* OO Infernia: dostaje na pokład Emiter Plagi Nienawiści, działo sfabrykowane przez Zespół, które jest przeklęte i rozprzestrzenia nienawiść wśród osób w aurze - plus, skutecznie niszczy to co trafi.
* Eustachy Korkoran: reputacja "świetni w niszczeniu i destrukcji, ale idźcie na bok i nie psujcie więcej". Takie trochę dzieci od zniszczenia.
* Elena Verlen: reputacja "świetni w niszczeniu i destrukcji, ale idźcie na bok i nie psujcie więcej". Takie trochę dzieci od zniszczenia.
* Klaudia Stryk: reputacja "świetni w niszczeniu i destrukcji, ale idźcie na bok i nie psujcie więcej". Takie trochę dzieci od zniszczenia.
* Diana Arłacz: reputacja "świetni w niszczeniu i destrukcji, ale idźcie na bok i nie psujcie więcej". Takie trochę dzieci od zniszczenia.
* Martyn Hiwasser: reputacja "świetni w niszczeniu i destrukcji, ale idźcie na bok i nie psujcie więcej". Takie trochę dzieci od zniszczenia.

### Frakcji

* .

## Zasługi

* Klaudia Stryk: odkryła, że dziwny materiał z Anomalii Kolapsu wymaga Esuriit do działania; też, sformowała z Eustachym Emiter Plagi Nienawiści, samoładujący się.
* Eustachy Korkoran: nakłonił Dianę do zabawy Esuriit i przekroczenia granicy - po raz pierwszy ever. Potem wyciągnął Dianę z obsesji Esuriit. I z Klaudią zrobił Emiter Plagi Nienawiści.
* Elena Verlen: alias Mirokin; próbowała rozwiązać problem Skażenia Esuriit na Bakałarzu + ostrzegła innych; nie tylko jej się nie udało skoordynować Zespołu, co jeszcze spadło na nią że to ona winna.
* Diana Arłacz: by zaimponować Eustachemu za wszelką cenę weszła w głąb energii Esuriit; została przez Esuriit opętana i wyciągnął ją Eustachy. Ale wymusiła pocałunek. Poważnie skrzywdziła kogoś.
* Martyn Hiwasser: poskładał skrzywdzonego przez Dianę, poskładał samą Dianę, deeskalował sytuację i poważnie opieprzył Eustachego za to, że nie zajmuje się Dianą - a to on ją tu ściągnął z Aurum.
* Maria Naavas: Abzan (BWG). Poświęci Eustachego by Olgierd i Arianna byli na randce. Wpakowała ich do jednej kapsuły, uszkodziła life support by było zimno i wysłała na 3 dni ;-).
* Arianna Verlen: Maria Naavas wpakowała ją z Olgierdem na "randkę" w kapsule ratunkowej z zimnym life supportem. Użyła magii do ogrzania a jak nie miała sił, Olgierd przejął i trzymał energię.
* Olgierd Drongon: Maria Naavas wpakowała go z Arianną na "randkę" w kapsule ratunkowej z zimnym life supportem. Arianna grzała magią a jak nie miała sił, Olgierd przejął i trzymał energię.
* OA Bakałarz: statek naukowy między rodami Aurum; organizuje Sympozjum Zniszczenia gdzie rozpracowywali dziwny materiał z Anomalii Kolapsu. 

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 5
* Dni: 4

## Inne

.