## Metadane

* title: "Ostatni lot Valuvanok"
* threads: brak
* motives: the-savior, the-survivor, into-the-abyss, dark-scientist, dom-w-plomieniach, arogancja-mlodosci
* gm: żółw
* players: fox, kapsel

## Kontynuacja
### Kampanijna

* [240313 - Ostatni lot Valuvanok](240313-ostatni-lot-valuvanok)

### Chronologiczna

* [240313 - Ostatni lot Valuvanok](240313-ostatni-lot-valuvanok)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * Inspiracje
        * "Musimy zniszczyć statek? Jest inne wyjście?" - Nemesis spaceship creepypasta
        * Czy miałam rację, mistrzu? - pytanie z Ragnarok Online Anime
        * All is lost. There is no reason to fight. There is no hope.
    * Battle Beast - "Lost in Wars"
        * "How can the stars glow | Above this creepy show | Which lie to chose | To believe and use"
        * "We're waiting for the dawn | That will never show"
        * There is no hope. No good result. Nothing. Only uncaring stars and pointless deaths.
        * Plaga spawnowała Potwory - ukształtowane przez Marę i rozprzestrzeniane przez Marę
* Opowieść o (Theme and vision):
    * "Zarażony statek kosmiczny, z agentami którzy próbują uniemożliwić dotarcie do celu (bo nie da się tego powstrzymać). Mag próbował ich powstrzymać i stracił wzrok - ale wysmażył im pamięć."
        * Dwa viciniusy i dwóch magów. Jedyna populacja odporna na arborytów z Iorusa.
        * Pierwszy poziom: statek kosmiczny nie doleci do celu; ma przeciążenie silników, złe orbity i jest ogólnie uszkodzony.
        * Drugi poziom: statek kosmiczny jest zarażony; muszą uniknąć potworów by sobie poradzić ze stabilizacją statku.
        * Trzeci poziom: to oni doprowadzili do tego by statek nie dotarł do celu. Oni to zrobili, ale Jasper odebrał im pamięć - a oni zabrali mu oczy.
        * Czwarty poziom: oni są pierwotnymi wektorami zarazy. Oni są jedynymi odpornymi na zarazę, chyba, że ixion zaadaptuje. Oni sprowadzili TO na pokład. I nikt nie wie czemu.
    * Lokalizacja: statek 
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * the-savior: Jasper; mag, naukowiec, który doszedł do tego że wszyscy są skazani na śmierć. Chce doprowadzić do śmierci wszystkich i zniszczyć Valuvanok ZANIM terrorformy ich zniszczą.
    * the-survivor: Mara; mag, lekarka, która jest zniekształcona przez Plagę i pragnie przetrwać i doprowadzić do jej propagacji. W ten sposób znajdzie się rozwiązanie.
    * into-the-abyss: z każdym layerem nie wiadomo co robić, Valuvanok jest coraz bardziej skazany i nie ma dobrych rozwiązań.
    * dark-scientist: Mara nie zginie. Mara sprawi, że Valuvanok przetrwa i uda się im to wszystko osiągnąć. Nawet badając Plagę, zmieniając siebie i używając ludzi jako substratu.
    * dom-w-plomieniach: Valuvanok jest w złym stanie - Plaga, potwory, zły kurs, przeciążone silniki, niesprawne fabrykatory.
    * arogancja-mlodosci: Mara zadziałała wbrew Jasperowi (wbrew ojcu); jest w stanie uratować wszystkich od Plagi. Niestety, to zgubiło Valuvanok i postawiło ojca przeciwko córce. 
* O co grają Gracze?
    * Sukces: o decyzję - co robimy dalej.
    * Porażka: strata postaci (dołączą do Mary LUB śmierć), MG decyduje jak to się potoczy dalej, potencjalnie: zniszczenie statku
* O co gra MG?
    * Highlevel
        * Chcę ich zabić dla odmiany?
        * Chcę, by musieli ciężko walczyć i móc podjąć decyzję co robią.
    * Co uzyskać fabularnie
        * Chcę, by poznali Marę i Jaspera. Chcę, by Jasper został oddany Marze. 
        * Chcę, by zostali zahibernowani ALBO uciekli z tej jednostki.
* Default Future
    * Valuvanok się ustabilizuje i dotrze do JAKIEGOŚ celu. Mara da radę obudzić kogoś "wartościowego" by pomóc w stabilizacji jednostki, nieważne jakimi kosztami (ludzkimi i materiel).
* Dilemma: Co zrobią gracze i jak do tego doprowadzą.
* Crisis source: 
    * HEART: "Mara nie dopuści do śmierci wszystkich na pokładzie. Uratuje także swojego ojca. WSZYSTKICH."
    * VISIBLE: "Potwory, powietrze o złym kolorze, statek w złym stanie"
        
### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

* Valuvanok leciał z Iorusa w okolice Anomalii Kolapsu
* Na pokładzie Valuvanok, oprócz zwykłej załogi, są dwie osoby - Xavier i Ewelina. Oboje są zmienieni w broń biologiczną.
* Xavier i Ewelina rozprzestrzeniają Plagę.
* Identyfikuje to oficer naukowy będący pierwszym oficerem (Jasper Arimetus) oraz jego córka, medyk pokładowy (Mara Arimetus)
* Mara skutecznie leczy ludzi na pokładzie. Jasper zidentyfikował problem - Xaviera i Ewelinę, po czym usunął ich z pokładu Valuvanok.
* Mara, przekonana, że może im pomóc, ściągnęła kapsułę z Xavierem i Eweliną na pokładę i zaczęła pracę nad pomocą tej dwójce
* Xavier wbił w Marę 'mackę' i wstrzyknął w nią Plagę. Mara została Zarażona.
* Plaga się rozprzestrzenia, Jasper orientuje się, że coś jest bardzo nie tak. Widząc, że nie może już wiele zrobić i traci kontrolę nad mostkiem, postanowił sabotować statek.
* Jasper odizolował mostek i zaizolował wszystko co był w stanie, po czym ruszył do sabotażu reaktora i silników.
* Na Jaspera poluje Ewelina i Xavier.
* Podczas konfrontacji, Ewelina strzeliła laserem w Xaviera, oślepiając go. Ale jego czar Dotknął umysłów Xaviera i Eweliny, niszcząc ich programowanie i usuwając im stopniowo pamięć.
* Xavier i Ewelina wycofują się, tracąc dane, pamięć itp. Poluje na nich jeden z Potworów Plagi. Chowają się w chłodni, wysadzają potwora.

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

* F1: Valuvanok umiera. Red alerts, oni mają zdehermetyzowane scutiery i nic nie pamiętają
    * Komórka w jednym z magazynów; na zewnątrz biomasa na ścianach. Wyraźna eksplozja.
    * Komunikacja z Marą, która nie w pełni kontaktuje. Zdziwiona że jej nie pamiętają.
    * Silniki są zbyt gorące. Za wysokie promieniowanie. Zbyt niebezpieczne.
    * TAI nie do końca reaguje (walczy o przetrwanie statku), ale są jakieś komunikaty w systemie. Uszkodzone, nie mają pełni uprawnień (Ex+).
    * Pokład Ratunkowy oraz Przednia Część są zabarykadowane. Od Waszej strony..?
* F2: Jasper vs Mara
    * Znaleźli Jaspera (bliżej silników). Błaga, by nie robić mu krzywdy. Zniszczcie ten statek!
        * w medical da się pomóc mu zdobyć oczy (zdaniem Mary)
* F3: Zbliżamy się do Zarażenia. Musimy hibernować lub poddać się Marze.
    * 21:00 czasu rzeczywistego INIT.
* Overall
    * chains
        * energia i amunicja Zespołu: max 2 (po refuel)
        * pancerz Zespołu: max 2 (scutiery)
        * sprzęt Zespołu
        * uprawnienia Zespołu (1 / 3)
        * morale i statusy Zespołu
        * zdrowie Zespołu (halucynacje - choroba - corruption)
    * stakes
        * Co się stanie z Valuvanok?
        * Co się stanie z nimi?
        * Co z Jasperem i 
    * opponent
        * Drzewotwór (bardzo zwinny i cienki pajęczak używający broni tak jak oni)
        * Infector (bulbous monster wstrzykujący jad)
    * problem
        * Drzewotwory żylne są w różnych miejscach
        * Wszechobecny dziwny 'pyłek' w powietrzu (Plaga)
        * Valuvanok 
            * leci z zbyt dużym kosztem paliwa
            * ma pełen lockdown; prawie nic nie działa
            * ma poblokowane grodzie i nie wszędzie dociera energia
        * TAI jest zablokowana; nie da się jej odblokować (trzeba ręcznie)

## Sesja - analiza
### Fiszki

.

### Scena Zero - impl

brak

### Sesja Właściwa - impl

Nie do końca wiecie gdzie jesteście. Jesteście... chłodnia, od środka. I drzwi są wypaczone do środka (eksplozja). Jest tu sporo mięsa. Nie pamiętacie, jak znaleźliście się w chłodni. Jest podejrzanie cicho i z uwagi na klatkę Faradaya, nie ma połączenia z resztą statku. Aha, Wasze servary klasy Scutier są zdehermetyzowane i macie może 10 minut tlenu.

Xavier zauważył, że istnieją szczeliny, więc da się zobaczyć na zewnątrz. Teren wygląda jak dosłownie po eksplozji bomby. Na ścianie widzisz COŚ. Ale to się nie rusza i wygląda dziwnie.

Ewelina rzuciła kawałek mięsa z chłodni. Dobrej klasy stek kapitański plasnął na ziemię. Nic. Nic się nie rusza. Jest bezpiecznie.

Scutiery skutecznie rozwaliły wyjście z chłodni.

Co tam jest tam na ścianie - wygląda jak jakaś forma humanoida, ale ten humanoid ma zdecydowanie za dużo kończyn i wygląda jakby był zrobiony z czegoś roślinnego? Tak czy inaczej, jest bardzo mało biomasy i to cholerstwo jest wprasowane w ścianę. Wy przetrwaliście eksplozję z chłodni. On dostał bezpośrednie uderzenie. Ale co to jest i skąd się wzięło - nie wiadomo.

Jesteście w magazynach. Więc Xavier wziął się do roboty. Hermetyzacja Scutierów + tlen + zapewnił że to jakoś działa. Zajęło to ~15 min, z całym szukaniem.

Xavier zauważył też, że statek kosmiczny leci na pełnej mocy silników. Nie doleci do celu, zgubią orbitę transferową. Nie ma dość paliwa na takie numery. Czyli statek KONIECZNIE potrzebuje pomocy jak najszybciej.

Ewelina sprawdza - co tu się stało, jak to wyglądało, czym wabiliśmy... jest doświadczoną agentką bojową, jest w stanie sprawdzić co tu się działo i na co wskazują ślady.

Tp +3Oy +3:

* V: jak to wyglądało
    * na zewnątrz się ostrzeliwywaliście (zarówno karabinem laserowym jak i shotgunem), TEN POTWÓR próbował dogonić i się dostać
    * ktoś zrigował bombę - tą która potem wybuchał
    * jakoś znaleźliście się w chłodni i eksplozja tej bomby

Xavier spojrzał na panel kontrolny. Co tam ma - podstawowe dane i problemy (dostępne inżynierowi). Chce wejść na poziom admina. Większość dostępów powyłączana - przez Jaspera Arimetus, głównego naukowca i pierwszego oficera. ŻADEN z life supportów nie działa prawidłowo. Wszystkie są czerwone, wskazują na Skażenie Biologiczne.

Xavier i Ewelina wymienili spojrzenia. Oni oddychali skażonym powietrzem. Cholera wie co im się stało i czy już są zarażeni.

Xavier wywołuje - czy ktoś odpowiada?

* Mara: halo? Halo, ktoś tam jest?
* Xavier: zidentyfikuj się
* Mara: Mara Arimetus, lekarz, kim jesteś?
* Xavier: jesteśmy na pełnej mocy... czemu?
* Mara: coś jest nie w porządku. Bardzo. Ktoś sabotował statek. Jestem zamknięta w okolicach mostka.

Tr Z +2:

* XX: Mara informuje, że nie może dać Ci dostępów bo ona nie ma uprawnień, ale dałeś jej maksymalne uprawnienia a ona dała Wam uprawnienia jakie TWIERDZI że może.
    * drzwi, śluzy - 'ochrona', mówi, że kamery są wyłączone

Drzwi na mostek - perfekcyjna barykada. A Mara błaga o uwolnienie. Ale... coś jest nie tak i tam jest też podobno COŚ. Mara bardzo prosi, by jak najszybciej ją wyciągnąć. Coś słyszy - podobno coś jest na mostku. Ewelina ma pewne wątpliwości co tu się dzieje, ale Xavier naciska by jednak Marę jak najszybciej wyciągnąć.

Samo rozmontowywanie barykady zajmuje czas.

Tp+3+3Oy:

* X: rozmontowywanie barykady chwilę trwa, jakieś ~20 minut. Nie jest to tragedia, ale się przedostajecie.
* V: udało się rozmontować. Śluza jest aktywna.

Jak tylko udało się rozmontować barierę, Xavier i Ewelina przeszli przez śluzę. Dokładnie po drugiej stronie znajduje się straszliwy mackowy potwór, który czekał w zasadzce. Złapał Ewelinę by ją rozszarpać na kawałki. Ewelina próbuje się bronić używając swojej broni - zarówno karabinu laserowego jak i shotguna.

![Potwór, wizualizacja](mats/2403/240313-01-drzewostwor.png)

Tr +3: (oderwanie się)

* X: broń mało skuteczna, potwór bardzo silny; scutier Eweliny zostaje uszkodzony
* Vr: rzuciłaś się do tyłu - macki odpadają
* V: SUKCES. Zgodnie z rozkazem, Xavier zatrzasnął śluzę overridem. Udało się - macki są po tej stronie, potwór po tamtej, Ewelina jest wolna. Udało się Wam przynajmniej tymczasowo zatrzymać stwora - wszystkie macki są zniszczone.

Po drugiej stronie śluzy jest więcej pyłku niż po tej. (część z macek ruszała się KORZYSTNIEJ niż powinny z perspektywy Eweliny).

* Mara: Nie wiem co zrobiliście, ale w mojej okolicy jest cicho
* Ewelina: Gdzie się ukrywasz?
* Mara: W medycznym, zabarykadowana. Próbuję nie wychodzić.
* Ewelina: Co robiłaś?
* Mara: Plaga.
* Ewelina: (sprzedaje info o zarodnikach, stór z biomasy)
* Mara: Słuchajcie, ten statek nie doleci. Ja Wam mogę... dać kierunek, ale musicie coś z silnikami. Bo zginiemy. 

Ewelina... coś słyszy w jej głosie.

Tr +3 +3Oy:

* X: Potwory mają dostęp do nowej biomasy, są wzmocnione. A Mara może wyjść gdy chce.
* X: Ewelina sobie nic nie dała rady przypomnieć.

Zespół (Xavier i Ewelina) stwierdzili, że muszą zdobyć jakieś zasoby by móc zrobić coś z tymi silnikami z tyłu. Mara dała wszystkie potrzebne uprawnienia (sterowana przez Xaviera), ale nie zmienia to faktu, że nie mają dobrej broni do walki przeciwko temu co jest na tym statku. Ewelina zaproponowała znalezienie skafandrów by odhibernować jeszcze jakiegoś inżyniera; kogoś, kto pozwoli im działać w kilku miejscach jednocześnie i dla odmiany wie lub pamięta więcej od nich. 

Podczas szukania sprzętu i materiałów Ewelinie rzuciło się na oczy coś niepokojącego - ten stek kapitański który rzuciła na ziemię wcześniej zniknął. Nie ma go tam. Nie ma też resztek potwora na ścianie - tamta biomasa też zniknęła. I nie ma też żadnego mięsa w chłodni. Niepokojące.

Lokalizacja gdzie są uśpieni inżynierowie, konsultacja z Marą - POWINNI być zahibernowani bliżej Inżynierii, ale niektórzy są też zahibernowani w normalnych miejscach. Mara dostarczyła dane z komputerów statku - którzy inżynierowie są wartościowi do odblokowania itp.

Drzwi do hibernacji - są zaspawane, od tej strony. A te do lifesupport / medical są zabarykadowane i zaspawane. Hibernacja - zaspawana.

Ewelina decyduje się odspawać jedne z 6 drzwi – czyli 60 zahibernowanych osób. W środku nie ma żadnych potworów, ale atmosfera jest skażona. Ciężko będzie przesunąć odhibernowanego inżyniera bezpośrednio do skafandra, bo i tak po drodze ‘zarodniki' będą miały kontakt ze skórą.

Ewelina postanowiła wykorzystać spryskiwacze - po włączeniu spryskiwaczy na dużej mocy i odhibernowaniu jednego z inżynierów, wsadziła go bezpośrednio do skafandra, nie czekając aż przestanie wymiotować. Przy odrobinie szczęścia nie został zarażony. Zrobiła mu szybki briefing, prowadząc jego i Xaviera w kierunku na drzwi prowadzące do Inżynierii.

(Tymczasem drzwi nie zostały zablokowane; to sprawia, że pojedynczy potwór znajdujący się po tej stronie jest w stanie się tam dostać z pomocą Mary i przekształcić wszystkich zahibernowanych w Potwory Plagi)

Ross (inżynier) zna Ewelinę Kalwiert. Zna ją. Autoryzacja stymulantów, działa.

3Oy + 5X:

* Oy: Ewelina przypomniała sobie coś. Coś ważnego.
    * Gdy się ostrzeliwałaś z potworem w chłodni, to Ewelina zebrała szczątkową kinezą części do kupy. A Xavier spowodował wybuch.
        * Oboje macie szczątkowe aspekty magiczne
        * Macie visiat, nie jesteście ludźmi

Dlatego to zadziałało. Ani Ewelina, ani Xavier nie są ludźmi - to viciniusy. Dlatego są odporni na Plagę tak samo jak Jasper i Mara być powinni (Ewelina i Xavier nie wiedzą, że Mara jest zarażona).

Czas iść w kierunku na silniki. Tu nie ma barykady. Są zamknięte, nie mają uprawnień do wejścia (ale w sumie je mają dzięki Marze)

Droga w kierunku na silniki z przestraszonym Rossem... światła nie do końca działają. Też są zarodniki. Jest naprawdę ciepło. I scutier wysyła sygnały alarmowe, bo bardzo silne promieniowanie.

Są ślady potworów, ale te ślady są od "zniszczonych" potworów. Potwory wyraźnie są wrażliwe na promieniowanie radioaktywne. W sumie jest to logiczne - nie mają pancerza, nie mają łusek, to 'roślinno/śluzowe istoty'.

Silniki są bardzo mocne. Co gorsza, są przesterowane - działają ponad maksymalnego poziomu mocy. Promieniują i poziom promieniowania jest niemożliwy do wytrzymania dla scutiera. Naprawa tego typu silników, zrobienie czegoś jest praktycznie niemożliwe. Potrzebują jakiejś formy tarczy. W okolicach silników znajdują się te resztki innych inżynierów, zmienionych przez Plagę w potwory. Czyli wszyscy którzy mogli przeżyć w okolicy - zginęli.

Nie wszyscy.

W silnie ekranowanym miejscu, jedynym takim znajduje się jedna osoba w scutierze. Po lewej – światło apokalipsy (promieniowanie). Po prawej – to samo. A ta jedna nieszczęsna figurka siedzi w niszy awaryjnej, maksymalnie ekranowana, ale nie wiadomo jak długo przetrwa i ile radów już dostał. Wszystko wskazuje na to, że to Jasper.

Xavier i Ewelina budują ekran ochronny przed promieniowaniem z "płyt ekranujących pancerza", zapasowych z Inżynierii.

Tr Z +3:

* Vr: Xavier skutecznie zmontował ekran ochronny. Ten ekran powinien wytrzymać odpowiednio długo.

Ewelina przeszła do Jaspera przez światło promieniowania, chowając się za złożoną wielką tarczą. Scutier wysyła sygnały alarmowe, ale tarcza jest wystarczająco dobra. Co ciekawe, Jasper patrzy na Ewelinę, ale nie zareagował w żaden sposób, dalej się 'kiwa'. Jakby jej nie zauważył.

* Ewelina: Jasper, przysyła nas Mara, mówiła że gdzieś tu urwał się z Tobą kontakt
* Jasper: Nie! Boże, nie róbcie mi krzywdy! - WYCIĄGNĄŁ BROŃ I CELUJE KOŁO CIEBIE. (on Cię nie widzi)
* Ewelina: Nie jestem tu by Cię skrzywdzić, Mara chce Ci pomóc
* Jasper: (śmiech przez łzy)

Ewelina orientuje się że Jasper nie widzi. KIEDYŚ laserem wypaliła mu oczy. I żąda prawdy.

* Na statku pojawiła się plaga. Mara próbowała wyleczyć.
* Magów to nie dotyczyło, ale jej zależało. Bardzo. Wzięła pacjenta zero i on ją zaraził. Dał radę. Wstrzyknął jej PŁYN.
* Zabarykadowałem mostek, ale kaskadowo szło. Wy... wy nie chcieliście niszczyć statku. Więc... zatrzymaliście mnie
* I wiesz...

Plan Eweliny na teraz - uczynić resztę statku piekłem. Bierzemy jakiś kawałek silnie napromieniowanego metalu. Chcemy na naszej dużej tarczy napromieniować go BARDZIEJ. Od zewnątrz maksymalnie napromieniowana. I chcemy pręty do łap. Ale jednocześnie chcemy zabezpieczyć statek przed zniszczeniem.

Innymi słowy - jeśli normalna broń, którą mają nie działa dobrze na te potwory, ale większość metalu tutaj jest mocno napromieniowana, da się wykorzystać silnie napromieniowane rzeczy stąd jako broń przeciwko tym potworom. Po tym, jak oczyszczą mostek będą w stanie tu wrócić i doprowadzić silniki do prawidłowego działania.

* Ewelina: Wyciągnę Cię stąd
* Jasper: Nie, nie rób tego, daj mi tu umrzeć w spokoju. Jeżeli nie zniszczysz statku, nie chcę stać się tym czymś
* Ewelina: Wstawaj. (oferuje skuteczne zniszczenie statku - oba reaktory statku i biomasa nie zostanie); to że polecą za daleko nie znaczy że nikt nie spróbuje uratować
* Jasper: Dobrze, pomogę Ci. Nie chciałem... nie miałem nic wspólnego z tym co tu się stało. Nic. I nic nie mogłem zrobić. (prawdziwa gorycz)

Wróciłaś w zasięg komunikacji.

* Mara: Wszystko w porządku? (ma nieco dziwny głos)
* Ewelina: Mamy Jaspera, niestety jest usmażony
* Mara: NIE ŻYJE?!
* Ewelina: Mhm
* Mara: Co za strata...

Przygotowujemy broń radioaktywną. Nie chcemy odwlekać!

Tr +3:

* V: Mamy podstawową broń improwizowaną do walki wręcz dla 2 osób.
* V: Mamy kilka granatów odłamkowych; powinny pomóc. Odłamki - są radioaktywne. "bieda-granaty"

Czas na powrót systemów inżynieryjnych do obszarów magazynowych. Ewelina idzie przodem, wpierw stawiając przed sobą tarczę (radioaktywną), następnie podnieść ją do góry trochę, Xavier kulnie granat odłamkowy i Ewelina znowu zasłoni tarczą. Jeśli jest tam jakaś niemiła niespodzianka, powinno to rozwiązać problem.

Tr +4:

* V: Z GÓRY miały iść macki. Dotknęły i zasyczały. Tarcza poszła po granacie w przód osłaniając i odłamkowy rozwalił 2 kolejne bioformy. One - wszystkie - próbują się wycofać.

Ewelina widząc sytuację i widząc, że czas się kończy zdecydowała się zrobić bieg - taranem w kierunku na mostek. Ona z przodu, Jasper w środku i Xavier na plecach; powinni dać radę się przedostać. 

Ewelina jednak nie miała pojęcia, że po przeciwnej stronie znajduje się Mara, która dała rady skutecznie zarazić większość zahibernowany osób i uzyskać wszystkie potrzebne karabiny.

Nagle - z przodu (ze śluzy) i z tyłu (z zostawionych za sobą drzwi) wysunęły się pnącza i macki z karabinami. Jest ich dużo, są niecelne, ale nie ma to znaczenia. 30 karabinów. Ewelina NATYCHMIAST stawia tarczą z przodu, ale przeciwnik jest za szybki i jest ich za dużo...

Ex Z +3 +3Or (rany ale sukces):

* Or: Scutier Xaviera pocięty bronią, zdehermetyzowany i Xavier ranny, ALE osłonił atak z tyłu - rzucił się, silnie napromieniowany i zaczął machać bronią.
* V: Xavier wrzucił granat odłamkowy do drzwi z których przyszły mackii się cofnął. Eksplozja była znacząca. Xavier szybko overridował drzwi i je zamknął.
    * Tymczasem Ewelina spłaszczyła macki z przodu tarczą.

Jest nie najlepiej – Ewelina jest dobrym stanie, Jasper też, ale Xavier jest ciężko ranny i napromieniowany. Macki są ‘zranione’ i ‘w szoku’, ale wrócą do prawidłowego działania dosyć szybko. Ewelina szybko myśli - jeśli mają coś zrobić, muszą zrobić to teraz.

* Mara: Próbujesz mnie uwolnić?
* Ewelina: Wyciągniemy Cię
* Mara: Z moim martwym ojcem?
* Ewelina: Mówiłaś, że nie masz kamer
* Mara: Mówiłaś, że on nie żyje
* Mara: Nie rób tego - nie zabijaj nas wszystkich, łącznie z wami
* Ewelina: Przekonaj mnie
* Mara: Dobrze. Czy wiesz jak się zaraziłam? Czy powiedział Ci to?
* Ewelina: Przez kontakt
* Mara: Chciał Was wyrzucić przez śluzę. Chciałam Was uratować. Powiedział Ci to?
* Ewelina: Dalej mamy spójne cele. 
* Mara: Ewelino, Ty... technicznie Xavier, ale Wy byliście pacjentami zero. Wy wprowadziliście plagę na statek. Ja chcę Wam pomóc. Nie chcę umierać, nie chcę byśmy umierali. Możemy to kontrolować.

Mara wyjaśniła co i jak. Ewelina jednak zdaje sobie sprawę z jednej rzeczy – Mara mówi troszeczkę inaczej niż wcześniej. Mara myśli że to kontroluje, ale nie do końca to kontroluje. Gdyby było inaczej, nie proponowałaby, że „naprawi oczy ojcu i zmieni go w to czym ona jest”. To trzeba skończyć. Ale Mara nie może o tym wiedzieć. Ewelina więc sprzedała Marze historyjkę, że dopóki nie dowiedzą się co tu się dzieje nie da się wiele z tym zrobić. Dlatego muszą dostać się do głównego komputera (AI Core). „Zdrowa” Mara nigdy by tego nie kupiła...

Ewelina idzie w kierunku na AI Core. Mara nie blokuje. Uwierzyła :-).

AI Core. Ale muszą wejść bez radioaktywnych prętów, bo uszkodzą tkankę AI.

Jasper się podłącza do AI Core, Ewelina powiedziała Marze że "nie mamy informacji o naszym celu podróży, czemu zaatakowani itp - info o wyleczeniu". A tak naprawdę Jasper przygotowuje jednostkę do samozniszczenia. I przekonać TAI by pomogła - pokazuje dane, przesterowuje kanały itp. "Jedyni żywi na pokładzie mówią że trzeba zniszczyć"

Tr Z M +5Ob +3Vr:

* X: Xavier musi zostać, nie da rady się wycofać. Zbyt ciężko ranny. Bohaterska śmierć.
* V: TAI akceptuje samozniszczenie. Valuvanok wie, że to koniec. TAI zniszczy jednostkę.
* Xz: Ewelina wie, że Mara mówiła prawdę (zarówno Xavier jak i Ewelina są wektorem). Jesteście żywą bronią biologiczną. Uruchamia się programowanie "przetrwać za wszelką cenę". Zaklęcie Jaspera przestaje działać. Ewelina odzyska pamięć i wróci do bycia żywą bronią biologiczną, wektorem apokalipsy.
* X: TAI chce zabić wszystkich na pokładzie. To zbyt niebezpieczne.

Oba reaktory zaczynają się destabilizować. TAI uruchamia procedurę samozniszczenia, blokując możliwość ewakuacji kogokolwiek. Ewelina mogłaby uciec, ale nie robi tego. Nie chce być niewolnikiem. Wie, że kiedyś nie była tym czym się stała. Wie, że kiedyś była częścią tej załogi. Nie wie kto i dlaczego jej to zrobił, ale Ewelina nie zostanie niewolnikiem tego CZEGOŚ. Woli zginąć z przyjaciółmi.

* Mara: Co się dzieje?!
* Jasper: To koniec, córeczko.
* Ewelina: Możesz się spotkać z ojcem, ostatnia okazja
* Mara: Dlaczego! Możemy przegrać... mogliśmy... 
* Ewelina: Królowa Roju może tego nie czuć, ale nie chcę wracać do niewoli.

Ewelina wyjmuje karty i gra po raz ostatni z Xavierem.

Atomowa eksplozja.

Wszystko zniszczone.

## Streszczenie

Valuvanok staje w obliczu Plagi. Udało się ją opanować, kosztem dwójki członków załogi. Mara - lekarka - overriduje decyzję Jaspera i zostaje zarażona przez Plagę, ratując Ewelinę i Xaviera. Jasper próbuje sabotażu statku, by uniemożliwić dalsze rozprzestrzenianie się Plagi, a Mara, zmieniona przez Plagę, próbuje przetrwać. Ewelina i Xavier doprowadzili do samodestrukcji statku, by nikt więcej nie ucierpiał. Lepsza śmierć niż niewola i przekazanie tego DALEJ.

## Progresja

* .

## Zasługi

* Ewelina Kalwiert: waleczny agent Valuvanok (sabotaż + infiltracja + walka); znalazła sposób walki z Potworami Plagi, wykorzystując zręczność, taktykę, radioaktywność. Odkryła kluczową wiedzę o własnej odporności na Plagę oraz fakt że jest wektorem Plagi. Podjęła decyzję o zniszczeniu Valuvanok, by powstrzymać rozprzestrzenianie się Plagi. Nie będzie niewolnikiem.
* Xavier Kalwiert: techniczny agent Valuvanok (inżynier + hacker); zbudował ekran ochronny przed promieniowaniem, co było kluczowym krokiem do bezpiecznego dotarcia do Jaspera. W walce z potworami wykorzystał granaty odłamkowe z radioaktywnymi odłamkami, tworząc skuteczną broń przeciwko wrogom. W pułapce zastawionej przez Plagę uratował Jaspera, ale to go zdehermetyzowało i doprowadziło do jego śmierci.
* Jasper Arimetus: naukowiec Valuvanok i mag który stracił oczy; widząc katastrofalne rozprzestrzenianie Plagi zdecydował się zniszczyć Valuvanok by nikt inny nie ucierpiał. Zainicjował sabotaż i zniszczenie statku, ale nie był w stanie tego dokończyć. Pomógł TAI zrozumieć jak zła jest sytuacja.
* Mara Arimetus: biomantka Valuvanok, która pragnie przetrwać; przez cały czas dążyła do znalezienia lekarstwa na Plagę; z uwagi na arogancję młodości sama się zaraziła i stała się instrumentem destrukcji Valuvanok. Powstrzymana przez Zespół.
* OnS Valuvanok: fregata transportująca zahibernowanych ludzi na linii Iorus - Anomalia, gdzie doszło do wprowadzenia Plagi. Samozniszczenie po działaniach Zespołu.

## Frakcje

.

## Fakty Lokalizacji

* Transfer Iorus Anomalia: (Hohmann Transfer Orbit) jedna z optymalnych energetycznie (i silnikowo) metod transferu, korzystających z sytuacje gdy orbity się zgadzają, z cyklicznością 1/427 dni

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Transfer Iorus Anomalia

## Czas

* Chronologia: Zmiażdżenie Inwazji Noctis
* Opóźnienie: 147
* Dni: 1

## Inne
### Blueprint Valuvanok

1. Część przednia
    1. Mostek, Dowodzenie
        * Systemy nawigacyjne, sensory i komunikacyjne.
        * Command&Control – centralne miejsce kontroli zaawansowanych technologii statku.
    2. AI Core
        * Miejsce, w którym mieści się główna sztuczna inteligencja statku, zarządzająca i monitorująca wszystkie systemy.
    3. Pokład Życiowy
        * System Podtrzymywania Życia – utrzymujący odpowiednie warunki do życia na pokładzie.
        * Kwatery Załogi – miejsca do spania, relaksu i rekreacji dla załogi.
        * Medical - leczenie itp.
    4. Reaktor (zapasowy)
2. Część środkowa
    1. Pokład Zaopatrzenia
        * Magazyny Zaopatrzenia – przechowujące zapasy, części zamienne i inne niezbędne materiały.
        * Dropping Bay – sekcja do przesyłania zaopatrzenia na inne statki lub na powierzchnię planet.
    2. Pokład Hibernacyjny 1
        * 3 Węzły po 60 osób
    3. Pokład Hibernacyjny 2
        * 3 Węzły po 60 osób
    4. Pokład Ratunkowy
        * Medical (awaryjny)
        * System Podtrzymywania Życia, zapasowy
3. Część tylna
    1. Pokład Inżynieryjny
        * Advanced Engineering Suite – miejsce, w którym przeprowadzane są zaawansowane prace inżynieryjne i naprawy.
        * Systemy Kontroli Energetycznej – miejsce kontrolowania i dystrybucji energii do wszystkich systemów statku.
    2. Silniki
    3. Reaktory
    4. Doki Serwisowe
        * Dla przeprowadzania zewnętrznych napraw i utrzymania.
        * Support Drones – drony przeznaczone do wsparcia i napraw na zewnątrz statku.
        * Hangar Pintki
