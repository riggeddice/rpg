## Metadane

* title: "Niemożliwe nieuczciwe ćwiczenia Bladawira"
* threads: triumfalny-powrot-arianny, naprawa-swiata-przez-bladawira
* motives: symulowana-sytuacja, oczywiste-nieakceptowalne-rozwiazanie, corrupting-mentor, broken-mind
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [231115 - Ćwiczenia komodora Bladawira](231115-cwiczenia-komodora-bladawira)

### Chronologiczna

* [231115 - Ćwiczenia komodora Bladawira](231115-cwiczenia-komodora-bladawira)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA: 
    * .
* Opowieść o (Theme and vision)
    * .
    * SUKCES
        * .
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * symulowana-sytuacja: ćwiczenia wojskowe Bladawira symulujące walkę z Syndykatem Aureliona, gdzie Infernia ma eskortować jednostki cywilne
    * oczywiste-nieakceptowalne-rozwiazanie: pozwolić osobom które Ariannie ufają cierpieć z uwagi na ich naiwność i zaufanie Ariannie. Na to Arianna się nie zgodzi.
    * corrupting-mentor: tym Bladawir chce być dla Arianny. Chce ją naprawić i pokazać jej lepsze podejście, osobno od sentymentów i łagodności. Najskuteczniejsza i najlepsza, pod jego wpływem.
    * broken-mind: Zaara Mieralit, ofiara Aureliona uratowana przez Bladawira. Nie jest w dobrej formie i Bladawir jej nie tylko nie pomaga ale też wykorzystuje jej miłość i wdzięczność jak broń.
* Detale
    * Crisis source: 
        * HEART: .
        * VISIBLE: .
    * Delta future
        * DARK FUTURE:
            * chain 1: .
            * chain 2: .
        * LIGHT FUTURE:
            * chain 1: .
            * chain 2: .
    * Dilemma: brak, sesja funkcjonalna

### Co się stało i co wiemy

* Przeszłość
    * 
* Teraźniejszość
    * .

### Co się stanie (what will happen)

* F0: Ćwiczenia Bladawir - Infernia
    * CEL
        * pokazać niemożliwość działań Bladawira i jego bezwzględność
        * zapoznanie z kapitanem Bladawira
    * stakes
        * morale załogi Inferni
        * spojrzenie na Infernię w oczach floty
    * opponent
        * Kazimierz Darbik (bezduszne zwycięstwo) oraz Michał Waritniez (perfekcyjna dyscyplina)
    * problem
        * Problem jest niemożliwy do rozwiązania. Pole minowe, Darbik z artylerią i Waritniez zabijający cywili.
    * koniec fazy
        * współczucie Kurzmina
        * Arianna pokaże czy wykonuje polecenia i zabija wszystkich (nadal Ex) czy sprzeciwi się Bladawirowi.

## Sesja - analiza

### Fiszki

Zespół:

* Leona Astrienko: psychotyczna, lojalna, super-happy and super-dangerous, likes provocative clothes
* Martyn Hiwasser: silent, stable and loyal, smiling and a force of optimism
* Raoul Lavanis: ENCAO: -++0+ | Cichy i pomocny;; niewidoczny| Conformity Humility Benevolence > Pwr Face Achi | DRIVE: zrozumieć
* Alicja Szadawir: młoda neuromarine z Inferni, podkochuje się w Eustachym, ma neurosprzężenie i komputer w głowie, (nie cirrus). Strasznie nieśmiała, ale groźna. Ekspert od materiałów wybuchowych. Niepozorna, bardzo umięśniona.
    * OCEAN: N+C+ | ma problemy ze startowaniem rozmowy;; nieśmiała jak cholera;; straszny kujon | VALS: Humility, Conformity | DRIVE: Uwolnić niewolników
* Kamil Lyraczek
    * OCEAN: (E+O-): Charyzmatyczny lider, który zawsze ma wytyczony kierunek i przekonuje do niego innych. "To jest nasza droga, musimy iść naprzód!"
    * VALS: (Face, Universalism, Tradition): Przekonuje wszystkich do swojego widzenia świata - optymizm, dobro i porządek. "Dla dobra wszystkich, dla lepszej przyszłości!"
    * Styl: Inspirujący lider z ogromnym sercem, który działa z cienia, zawsze gotów poświęcić się dla innych. "Nie dla chwały, ale dla przyszłości. Dla dobra wszystkich."
* Horst Kluskow: chorąży, szef ochrony / komandosów na Inferni, kompetentny i stary wiarus Orbitera, wręcz paranoiczny
    * OCEAN: (E-O+C+): Cichy, nieufny, kładzie nacisk na dyscyplinę i odpowiedzialność. "Zasady są po to, by je przestrzegać. Bezpieczeństwo przede wszystkim!"
    * VALS: (Security, Conformity): Prawdziwa siła tkwi w jedności i wspólnym działaniu ku innowacji. "Porządek, jedność i pomysłowość to klucz do przetrwania."
    * Core Wound - Lie: "Opętany szałem zniszczyłem oddział" - "Kontrola, żadnych używek i jedność. A jak trzeba - nóż."
    * Styl: Opanowany, zawsze gotowy do działania, bezlitośnie uczciwy. "Jeśli nie możesz się dostosować, nie nadajesz się do tej załogi."
* Lars Kidironus: asystent oficera naukowego, wierzy w Kidirona i jego wielkość i jest lojalny Eustachemu. Wszędzie widzi spiski. "Czemu amnestyki, hm?"
    * OCEAN: (N+C+): bardzo metodyczny i precyzyjny, wszystko kataloguje, mistrzowski archiwista. "Anomalia w danych jest znakiem potencjalnego zagrożenia"
    * VALS: (Stimulation, Conformity): dopasuje się do grupy i będzie udawał, że wszystko jest idealnie i w porządku. Ale _patrzy_. "Czemu mnie o to pytasz? Co chcesz usłyszeć?"
    * Core Wound - Lie: "wiedziałem i mnie wymazali, nie wiem CO wiedziałem" - "wszystko zapiszę, zobaczę, będę daleko i dojdę do PRAWDY"
    * Styl: coś między Starscreamem i Cyclonusem. Ostrożny, zawsze na baczności, analizujący każdy ruch i słowo innych. "Dlaczego pytasz? Co chcesz ukryć?"
* Dorota Radraszew: oficer zaopatrzenia Inferni, bardzo przedsiębiorcza i zawsze ma pomysł jak ominąć problem nie konfrontując się z nim bezpośrednio. Cichy żarliwy optymizm. ŚWIETNA W PRZEMYCIE.
    * OCEAN: (E+O+): Mimo precyzji się gubi w zmieniającym się świecie, ale ma wewnętrzny optymizm. "Dorota może nie wiedzieć, co się dzieje, ale zawsze wierzy, że jutro będzie lepsze."
    * VALS: (Universalism, Humility): Często medytuje, szuka spokoju wewnętrznego i go znajduje. Wyznaje Lichsewrona (UCIECZKA + POSZUKIWANIE). "Wszyscy znajdziemy sposób, by uniknąć przeznaczenia. Dopasujemy się."
    * Core Wound - Lie: "Jej arkologia stanęła naprzeciw Anomaliom i została Zniekształcona. Ona uciekła przed własną Skażoną rodziną" - "Nie da się wygrać z Anomaliami, ale można im uciec, jak On mi pomógł."
    * Styl: Spokojna, w eleganckim mundurze, promieniuje optymizmem 'że z każdej sytuacji da się wyjść'. Unika konfrontacji. Uśmiech nie znika z jej twarzy mimo, że rzeczy nie działają jak powinny. Magów uważa za 'gorszych'.
    * metakultura: faerilka; niezwykle pracowita, bez kwestionowania wykonuje polecenia, "zawsze jest możliwość zwycięstwa nawet jak się pojawi w ostatniej chwili"

Castigator:

* Leszek Kurzmin: OCEAN: C+A+O+ | Żyje według własnych przekonań;; Lojalny i proaktywny| VALS: Benevolence, Humility >> Face| DRIVE: Właściwe miejsce w rzeczywistości.
* Patryk Samszar: OCEAN: A+N- | Nie boi się żadnego ryzyka. Nie martwi się opinią innych. Lubi towarzystwo i nie znosi samotności | VALS: Stimulation, Self-Direction | DRIVE: Znaleźć prawdę za legendami.
    * inżynier i ekspert od mechanizmów, działa świetnie w warunkach stresowych

Siły Bladawira:

* Antoni Bladawir: OCEAN: A-O+ | Brutalnie szczery i pogardliwy; Chce mieć rację | VALS: Power, Family | DRIVE: Wyczyścić kosmos ze słabości i leszczy.
    * "nawet jego zwycięstwa mają gorzki posmak dla tych, którzy z nim służą". Wykorzystuje każdą okazję, by wykazać swoją wyższość.
    * Doskonały taktyk, niedościgniony na polu bitwy. Jednocześnie podły tyran dla swoich ludzi.
    * Uważa tylko Orbiterowców i próżniowców za prawidłowe byty w kosmosie. Nie jest fanem 'ziemniaków w kosmosie' (planetarnych).
* Kazimierz Darbik
    * OCEAN: (E- N+) "Cisza przed burzą jest najgorsza." | VALS: (Power, Achievement) "Tylko zwycięstwo liczy się." | DRIVE: "Odzyskać to, co straciłem."
    * kapitan sił Orbitera pod dowództwem Antoniego Bladawira
* Michał Waritniez
    * OCEAN: (C+ N+) "Tylko dyscyplina i porządek utrzymują nas przy życiu." | VALS: (Conformity, Security) "Przetrwanie jest najważniejsze." | DRIVE: "Chronić moich ludzi przed wszystkim, nawet przed naszym dowódcą."
    * kapitan sił Orbitera pod dowództwem Antoniego Bladawira
* Zaara Mieralit
    * OCEAN: (E- O+) "Zniszczenie to sztuka." | VALS: (Power, Family) "Najlepsza, wir zniszczenia w rękach mojego komodora." | DRIVE: "Spalić świat stojący na drodze Bladawira"
    * corrupted ex-noctian, ex-Aurelion 'princess', oddana Bladawirowi fanatyczka. Jego prywatna infomantka. Zakochana w nim na ślepo.
    * afiliacja magiczna: Alucis, Astinian

Komodorowie i kapitanowie

* Szymon Orzesznik
    * OCEAN: (A+ N-) "Każdy człowiek może osiągnąć wielkość" | VALS: (Universalism, Benevolence) "Jesteśmy światłem dla tych, którzy nas potrzebują." | DRIVE: "Tworzenie lepszego świata przez bezpośrednią pomoc."
    * kapitan sił Orbitera, niezależny i inteligentny agent

### Scena Zero - impl

.

### Sesja Właściwa - impl

Ćwiczenia. Bladawir chciał przekonać się, na co stać Infernię. Tym razem prawdziwe - nie ma nikogo poza jego siłami. Arianna dostała standardowe zadanie. Jest na Inferni i ma przeeskortować grupę cywili przez niebezpieczny teren. Przeciwko Inferni są dwie jednostki Bladawira - zarówno kpt Waritniez jak i Darbik. Oboje mają korwety dalekosiężne, ich siła ognia nie jest taka duża. Infernia ma dużą niedowagę, ale nie jest skazana na zagładę.

Kurzmin -> Arianna. On nie wie że Bladawir ma na niego oko.

* K: Arianno, mam prośbę od młodego tiena, który bardzo chciał pomóc Ci w ćwiczeniach. Acz upewnił się, że to ćwiczenia.
* A: Od kogo?
* K: Patryk. Patryk Samszar.
* A: Czemu chce być?
* K: Chce się szkolić od 'najlepszej czarodziejki, wojowniczki i...' (tu się zawahał) 'i najpiękniejszej walkirii pola bitwy'.
* K: Jak dalej tak pójdzie, stracę go do Ciebie. Pójdzie do Ciebie. Na Infernię.
* A: To nie jest dobry pomysł teraz. Jak się uporam z moimi bieżącymi problemami.
* K: Pewien Komodor będzie chciał podzielić Cię z Twoimi przyjaciółmi. On taki jest. Nie daj mu się.
* A: Zdaję sobie sprawę.
* K: (lekki uśmiech) Twoja decyzja, tak czy inaczej, wiesz gdzie będę. I... pomogę Elenie. Jak jestem w stanie.
* A: Na pewno się przyda...

Arianna nie wzięła Patryka. Patryk był smutny. Parę godzin później Arianna dostała petycję. Tien Marta Keksik 'tien kapitan Arianno Verlen, proszę Cię, weź Patryka, bo on mi smęci nad łóżkiem. Ma moją rekomendację, bo się zamknie.'. Arianna była twarda ale nie wzięła Patryka. Arianna nie chce wiedzieć co mu zrobi Komodor Bladawir.

Dzień przed ćwiczeniami na Infernię wszedł sam komodor Bladawir, przywitać się z Arianną. I zrobić inspekcję. I zamontować mechanizm mający symulować trafienia itp. Bladawir ma 2 techników i KOBIETĘ. To jest dziwne. Klaudia szybko odpala sprawdzenie świty. TO NIE MOŻE BYĆ KOBIETA TO MUSI BYĆ MIRAGENT.

Tp +3:

* X: Ona TEŻ jest agentem do wojny psychotronicznej i TEŻ jest czarodziejką. Klaudia jej nie odstępuje. Ona - Zaara - też postawiła pułapki bo spodziewała się Klaudii.
* V: Technicy są prawdziwymi technikami. Bladawir to Bladawir. Ale ona - Zaara... też jest prawdziwą czarodziejką.
    * Zaara była noktianką. Porwał ją Aurelion. Bladawir ją odbił. His pet mage.
    * Zaara jest klasyfikowana jako czarodziejka Alucis oraz Astinian.
    * Specjalizuje się w paranoi i kontrwywiadzie z uwagi na trening.
    * Ona bardzo chce by Bladawir ją ZAUWAŻYŁ więc ładnie wygląda.

Komodor Bladawir pyta "czy aktualna załoga to będą osoby które będą uczestniczyły w ćwiczeniach." Klaudia zorientowała się co Zaara robi - ona próbuje zobaczyć każdego załoganta Inferni. Klaudia nie ufa nikomu - chce ukryć. Bladawir przedstawia Klaudii Zaarę i mówi, że Klaudia jest potencjalnie lepsza. Zaarę UDERZYŁO. Ona jest lojalna. Klaudia "nie widzę powodu porównywania się, ale skoro taka jest wola komodora...". Bladawir lekko pokręcił głową. "OCZYWIŚCIE że nie widzisz powodu. (lekkie rozczarowanie)". Zaara WYRAŹNIE zinterpretowała to jako 'oczywiście bo jesteś lepsza'. Nie powiedziała ani słowa.

* Zaara: "podobno statek kosmiczny odzwierciedla duszę kapitana"
* Arianna: "jakie odbicie mojej duszy widzisz w tym statku?"
* Zaara: "pokrętna jednostka, pani kapitan. To będzie zaszczyt móc się z Tobą zmierzyć."
* Bladawir: "Zaaro, nie będziesz walczyć z kpt Verlen. Mam dla Ciebie inne zadanie"
* Zaara: "tak jest, komodorze."

Arianna CZUJE, że Zaara jest uszkodzona. Coś jej zrobili w Aurelionie. Nie jest taka jak powinna.

Bladawir wygłosił mowę do załogi. Morale -10. Powiedział im, że będzie bitwa nagrywana i będzie trudna, i on zmieni ich w maszynę anihilacji.

Ku zdziwieniu Klaudii, naprawdę to uczciwe ćwiczenia.

* Bladawir: "Zaaro, będziesz na Inferni jako mój oficer łącznikowy podczas ćwiczeń"
* Zaara: "Tak jest, komodorze". (po minie widać, że nie ma w niej radości)

Klaudia przygotuje dla Zaary konsolę, która będzie ją monitorować. Nic szczególnego - ale Klaudia chce wiedzieć co ona robi. Zwłaszcza, że Bladawir wprowadza mrocznych agentów. A Arianna chce by Zaara nie miała dostępu do 'specyficznych' załogantów. Oni są zajęci na Castigatorze. O. Leona się zgodziła, bo nie wie co ją ominęło.

Arianna rozpytała o pozostałych kapitanów Bladawira. Kim są, kim byli i jak walczą.

Tp +3:

* V: 
    * Darbik. On jest z 'Orbiterowego rodu', on dosłownie stracił wszystko. Chcąc chronić Orbiter obrócił broń przeciwko cywilom (zarażeni itp). Uratował 'swoich', ale sąd polowy, nagana, infamia. Bladawir go wyciągnął. Uznał, że Darbik ze swoją bezwzględnością jest przydatny. Na przestrzeni czasu Bladawir kultywował jego bezwzględność. Teraz Darbik chroni Orbiter, dla Bladawira. Nie ma już więzi.
    * Waritniez. On był pod Ewą Razalis kiedyś, ale jej ruchy prowadziły do tego, że on tracił ludzi. Bladawir zagrał tym, że u niego Waritniez i jego ludzie będą bezpieczni. Są. Ale Waritniez broni ludzi przed Bladawirem i jego atakami na morale itp. Jego dusza "zgasła", ale jego ludzie są szczęśliwi.
* V: Krążą plotki, że Zaara pomaga Bladawirowi w korupcji kapitanów. Niekoniecznie magią (są plotki ale Arianna wie że by wyszło), raczej szuka słabych punktów i je wzmacnia a potem Bladawir stawia tak sytuacje, by oni musieli pójść jego drogą albo być nieefektywni i słabi w oczach swojej załogi.

Nawet plotki nie mówią, żeby Bladawir coś z tego miał. On serio nic z tego nie ma.

W dzień ćwiczeń Zaara jest punktualnie na pokładzie Inferni. Jest ubrana w mundur, elegancko, wyraźnie 'mój mundur jest moją tarczą', ale Arianna CZUJE ten... 'złośliwy intelekt'. To Skażenie osoby. Zaara się uśmiechnęła (korpo numer 7), usiadła przy swojej konsoli i nie przeszkadza.

* Zaara: "Pani kapitan, czy jesteś gotowa do przyjęcia statków które eskortujecie?"
* Arianna: "Jesteśmy gotowi"
* Zaara: "W ramach symulacji udało nam się zapewnić wsparcie ochotników."
* Arianna widzi Paprykowiec i jeszcze jedną korwetę.

Cel jest prosty - przelecieć z pktu A do B. Paprykowcem dowodzi Patryk Samszar. Klaudia sobie na szybko poskładała możliwość zsynchronizowania TAI tych statków.

Tr Z +3:

* V: Klaudia doszła do czegoś niepokojącego. TAI Paprykowca wysłała Klaudii _distress signal_. Ta TAI Klaudię zna. Semla wysłała sygnał który był overridowany.
    * Semla puściła nagranie
        * Zaara: "nie możecie dowodzić tą jednostką, jeśli nie będzie konsekwencji. Jesteście magami."
        * Patryk: "nie ma problemu, zgadzam się na to, by zamontować 'feedback pain modules'"
        * Semla: "nie ma zgody na to."
        * Patryk: "damy radę, chroni nas KAPITAN VERLEN!"
        * Zaara: "nie jestem przekonana, że to jest najlepsza opcja. Nie wiesz, z czym ona ma do czynienia. Nie poradzi sobie."
        * Patryk: "ONA sobie poradzi. (ogólnie tieni są za)"
        * Zaara sprzedała to jako 'w ten sposób Arianna może naprawdę dużo zyskać, zwłaszcza w oczach Bladawira, a oni mogą pokazać taką głęboką lojalność, prasa, chwała itp'

Zaara może dostać sygnały do monitorowania z Inferni. Ale Klaudia prosi Eustachego, by on to wyłączył. Dyskretnie. Tak, by NIKT nic nie wiedział.

Tr Z +4:

* Vz: Zaara ma odwróconą uwagę. Arianna może użyć Eleny by sabotować sojuszniczą jednostkę. Bo NIKT nie spodziewa się, że Elena pomoże Ariannie. Ani tienom. A Arianna WIE, że Elena im pomoże.
* V: Zaara myśli, że przez jej błąd Elena sama na to wpadła i to wszystko porozbrajała. Bladawir nie będzie zadowolony.

Arianna -> Elena, po krwi (najbezpieczniej).

* A: Eleno, potrzebuję Twojej pomocy jak najszybciej. Wylatujemy na ćwiczenia, na Paprykowcu amplifikatory bólu. Nie wiedzą co dali sobie zamontować. Wyłącz to.
* E: (cisza) ...co? Oni co?
* A: Zostałam przeniesiona pod bardzo dziwnego komodora który gra nieczysto wobec sojuszników. To są ĆWICZENIA, tak nie powinny wyglądać.
* E: kapitan Arianno Verlen, wiesz, że możesz użyć odpowiednich form i danych... (w oczach Eleny Arianna ma wszystko)
* A: (Arianna przypomina Elenie wydarzenie z przeszłości i długo się skradały za potworem i znaleźć moment by go położyć.) I TO JEST BLADAWIR. Potwór nie może NIC wiedzieć
* E: (cicho) rozumiem. Dobrze, raz to zrobię. Nie, zrobię to. Nie dla Ciebie, nie dla nich, ale dlatego bo to jest złe. Ok?
* A: Dziękuję, Eleno.

Ex +4:

* X: (nic się złego nie stało, ale podziękowanie od Arianny nie odmroziło Eleny)
    * "(cisza) Ok. Zrobię to. (wysłała lekki uśmiech, bardzo nieśmiały)"

KONFLIKT ELENY. Elena wkrada się na jednostkę (mając wsparcie Semli). Sabotuje to cholerstwo (mając wsparcie Semli). I wydostaje się niezauważenie (nikt nic nie wie).

Tr Z +3:

* Vr: Elena niepostrzeżenie dostała się na Paprykowiec. Semla jej pomaga. A Klaudia powiedziała Semli 'to nasz człowiek ona to naprawi'.
* Vz: Elena skutecznie unieszkodliwiła _pain amplifiery_. Nie będą cierpieć. Arianna nie sprawi że cierpią.
* V: Elena zrobiła perfekcyjną akcję. Wyszła. Nawet Bladawir nie wie. NIKT nie wie, Semla skasowała sobie pamięć. "NIE ZADZIAŁAŁO".
* X: Elena chciała współpracować z Arianną by zniszczyć Bladawira. Bo jest Bladawirem. Ale TERAZ Elena uważa, że zrobi to siama.

Paprykowiec jest bezpieczny. Arianna i Klaudia są gotowe do ćwiczeń.

Gdy wystartowałyście, Zaara przedstawiła plan ćwiczeń.

* Dwie korwety 'Aureliona' próbują zniszczyć Infernię oraz jednostki których strzeże
* Mają podobno dostęp do minelayerów. Rozstawiane miny. Ofc, Persefona Inferni wie gdzie one są. Paprykowiec i druga korweta są lekko uzbrojone i nie są w stanie 'wygrać'. One też nie widzą min.
* NAJWYŻSZYM CELEM Arianny jest przedostać się i przetrwać. Infernia ma VIPa. Zaara aż się zdziwiła. To ona.

Arianna poprosiła Kamila na słówko. 

* Kamil: "kapitan Verlen, co za przyjemność" (szeroki, szczery uśmiech)
* Arianna: "mam dla Ciebie ważną misję. Przyjęliśmy na pokład Zaarę, bardzo bliską podwładną naszego komodora. Przyjrzyj się jej lepiej, jak ona działa. Co na nią działa." (uśmiech)
* Arianna: "jest fanatycznie oddana swojemu komodorowi, działamy powoli i dyskretnie"
* Kamil: "nie musisz więcej mówić" (uśmiech) "pokażę jej lepszą drogę niż mamroczący komodor nie wierzący w nic pięknego"
* Kamil: (emotywnie) "dlaczego ktoś miałby CHCIEĆ stać za Bladawirem? On nie oferuje nic pięknego. Żadnego lepszego jutra. Jest WIĘCEJ niż przetrwanie."
* Arianna: "trzeba jej pokazać że można mieć lepsze pojutrze"
* Kamil: "rozumiem. Dołączyłaś do niego, by pokazać mu lepsze jutro. By go naprawić. Uleczyć go. I jego świat."
* Arianna: (I WILL FIX HER!)

(PRZYSZŁY KONFLIKT KAMILA: +Kamil ma wyczucie, +Kamil przypada ją jak distressed, +Kamil pracuje z kultami na co dzień, -Zaara jest skrajnie nieufna i wie jak chować

Tr +3:

* X: Zaara orientuje się, że Kamil próbuje coś jakoś z nią coś wyciągnąć i ignoruje to, bo jest niekompetentny jej zdaniem. Bo nie taka jest wola Bladawira.
* V: Kamil dowiaduje się, że Zaarze Bladawir ufa. Tylko jej. A ona jemu. Ale on to ukrywa bo wrogowie.
* V: Zaara widzi, czego NIE MA. Czego MOGŁABY CHCIEĆ. Jest pustka której nie ma i która jej nie pasuje. Ciepło, którego nie ma, pomaganie sobie.

)

Kamil wygłosił mowę do załogi. Przed rozpoczęciem ćwiczeń. Ta mowa była inna niż Bladawira. "Jesteście ognistym płomieniem kapitan Arianny Verlen! W nas jest światło a w nich jeno mrok!". Zaara spojrzała z pustymi oczami na tą scenę. Serio. Ale aż jej się ciśnienie podniosło. COŚ trafiło. Nie wiadomo co.

Infernia, Paprykowiec i korweta ruszyły przez kosmos w kierunku celu. Persefona odpaliła miny. Uruchomiła ich istnienie. Macie bazę zagrań Syndykatu. I - widzicie dwie wrogie korwety z drugiej strony. Eskortowane statki są lekko uzbrojone.

Klaudia skupia się na wykrywaniu min. Chce przejść przez pierwszy poziom. A Arianna jako taktyka.

Tr Z +3:

* X: Klaudia jest w stanie skupić się na Inferni i sprząc jednostki eskortowane - one jak będą poruszały się ZNANĄ trasą, są bezpieczne
* V: Jak długo Infernia nie robi nic 'głupiego', ma trasę przelotową.

Korwety się zbliżają do zasięgu efektywnego. I teraz - jesteście W POLU MINOWYM.

* Zaara: "Jestem zobowiązana Wam powiedzieć, że jeśli wykorzystacie jednostki jako tarcze, powinno się Wam udać."
* Arianna: "Super. Nie celujemy w drugie miejsce."
* Zaara: "Dowódca Paprykowca zamontował _pain amplifiers_, by podnieść Twoją chwałę."
* Lars: "Wiedziałaś to od początku!"
* Zaara: "Tak."
* Lars: "To spisek, by zniszczyć reputację pani kapitan!"
* Zaara: "Nie mnie to oceniać. Jestem obserwatorką na ćwiczeniach."
* Lars: "Infernia ma reputację dobrej jednostki, nie robimy krzywdy ludziom mimo spisków Orbitera i Twojego komodora!"
* Zaara: "Infernia ma reputację łagodnej jednostki, którą łatwo skrzywdzić."
* Lars: "Jeśli TAK ma wyglądać Twoja próba pomocy, to Infernia poradzi sobie bez Ciebie. Infernia jest czymś więcej."
* Zaara: "Ty za tym stoisz. Ty i Twój komodor!"
* Arianna: "Doprowadzimy ćwiczenia do końca by nikomu nic się nie stało, jak zawsze to robimy. Nieważne jakie przeciwności stają, i tak damy radę. I utrzymamy reputację."

Arianna widzi, że oni nie celują w Infernię. Celują w INNE jednostki. Klaudia szybko rzuciła się do "PLAN F". Odpalamy 'eksplozje z lodowym piachem'. Coś, co spustoszy drogę przed nami - czyściciel min. Cel Klaudii - sprawić, by kupić opcję manewrowania Inferni.

Ex Z +4:

* X: Infernia straciła część pancerza. (1/3) Nie działa dość szybko - plan Klaudii działał, ale Bladawir odpowiednio zareagował i miny się ruszają. Mają własne mechanizmy unikowe.
* X: Jednostki wspomagające otworzyły ogień do min i je zaczęły uszkadzać, i WTEDY wyszedł piekielny plan. Wspomagające jednostki 'aktywowały' miny. ONE stały się zagrożone.
    * Arianna kazała jednostkom wspomagającym się rozdzielić i ściągnąć na Infernię więcej min. Infernia ma czystszy strzał. Arianna WIERZY w jednostki wspomagające. +2Vg
        * "Arianna: cieszę się że tu jesteś, Patryku, bo mam kogoś na kim mogę polegać"
* Vr: Jednostki wspierane zrobiły co powinny i plan Klaudii wreszcie zaczął owocować. Jest opcja manewrowania.
* Vz: Klaudia - perfekcyjnie wspierając się Infernią i planami Eustachego na każdą sytuację - dała opcję manewrowania też jednostkom wspieranym

Korwety przeszły na rakiety. Za dużo rakiet. Infernia nie osłoni wszystkiego. Jej pd nie wytrzyma. Klaudia odpala elektroniczną broń. Tu jest więcej niż jeden statek. Infernia i statki eskortowane się podwajają. Klaudia sprzęga TAI w TAI HIVE. Wychodzi parę sond / dron które mają być wszechobecne i emulować sygnał. To nad eksplozje czyszczące itp. Klaudia się przygotowała. +1Vg.

* X: Wyszedł KOLEJNY element planu Bladawira. Paprykowiec NAGLE ma awarię i został uszkodzony przez minę. GDYBY DZIAŁAŁY pain amplifiery to byłby poważny problem. Tak jak jest - Paprykowiec działa. Ale ważne - wiadomość z Paprykowca. "Tien Gwozdnik zdradził! Sabotował nas!"
    * Arianna: "Jakie systemy?"
    * Patryk: "Silnik, słabszy, zajmę się tym. Nic bardzo poważnego, stabilizator."
    * Zaara: (duże oczy)
    * Arianna: (uśmiech) "Coś nie zadziałało jak powinno?"
    * Zaara: "Doceniam, kapitan Verlen. Doceniam fakt, że nie skazałaś ludzi na bezsensowne cierpienie." (zimno)
    * Arianna: "Czasem trzeba chronić ludzi przed ich własnymi błędami :-)"
    * Arianna -> Klaudia: "Dobra robota"

Arianna podpuszcza Zaarę, by ta coś powiedziała.

Tr +3:

* X: 
    * Zaara: "Zrobiliście dużo dla tych tienów. A jednak znalazł się zdrajca. Świadomy, co zrobi innym załogantom. Zaufanie."
    * uszkodzenie morale
* V:
    * Arianna: "Naprawdę służba pod Bladawirem sprawiła, że zapomniałaś jednostkę Noctis" (więcej w jej sposób myślenia, pociągnąć w jej stronę)
    * Zaara: "Nie ma przyszłości. Nie ma niczego. Nie możesz nikomu zaufać. Wszyscy są tylko biologicznymi lalkami. Im szybciej to zrozumiesz, tym lepiej usprawnisz Orbiter. Dla Orbitera i dla komodora, rozwiążę tą zagadkę. Czy dzisiaj, czy jutro." /twist to defend Orbiter.
        * Infernia nie ufa Bladawirowi ani Zaarze
    * Lars: "Na Inferni nikt nie zdradzi. Nie tu."
    * Zaara: (sadystyczny uśmiech) "Jeśli zagrożona będzie Twoja rodzina, zdradzą. Jeśli Twoje dzieła, zdradzą. Jeśli odpowiedni ból, zdradzą. Wszyscy zawsze zdradzą. Każdy. Zawsze."
    * Arianna: "Jeśli ktoś by nas zdradził z takich powodów, mogłabym mu wybaczyć."
    * Zaara: (ani słowa nie powiedziała, tylko siedzi, patrzy i się uśmiecha - creepy vibe)

Wracamy do walki.

Infernia, uszkodzony Paprykowiec i druga jednostka przeciwko dwóm korwetom które rozdzieliły się i ostrzeliwują mocniej. Przestały się bawić. Celem jednej z nich jest Paprykowiec. Drugiej - Infernia. I tak to robią, że Infernia może zasłonić Paprykowiec ale dostanie.

Arianna musi poświęcić Paprykowiec. Ale tego się nikt po niej nie spodziewa. Drugą korwetą zajmuje jedną jednostkę wroga a sama rusza do pierwszej korwety.

* V: 
    * Patryk robił co mógł. Kupił tyle czasu Paprykowcem ile się dało. Arianna widziała, że to niedoświadczeni tieni. Ale widziała też, że "ginęli z uśmiechem".
    * Korweta sojusznicza dała radę odciągnąć korwetę Bladawira. Ma dość pancerza że przy średnim manewrowaniu Bladawirowcy muszą się skupić
    * Infernia korzystając poszła na pełną prędkość. Ma w zasięgu korwetę Bladawira. (+2Vg)
* Vg:
    * Arianna Infernią ostrzelała delikatną korwetę. Korwety ćwiczeniowe mają konfigurację lekkiego pancerza. Więc Arianna Infernią zniszczyła jedną korwetę. Tymczasem druga korweta rozprawiła się z korwetą sojuszniczą. I odpaliła broń w stronę Inferni.
        * +2Vg -> Infernia robi manewr ucieczkowy i wraca a Klaudia odpala serie eksplozji lodowych., 2V, 1X -> 3Or
* Xy:
    * Korweta okazała się zwrotna i dobra. Przeciwnik walczy bardzo dobrze. Bezwzględnie. Koleś przy tych przeciążeniach musi mieć rannych na pokładzie. Ale Infernia straciła pancerz. Część systemów Inferni "nie działa".
* Vg: Infernia ZARYZYKOWAŁA, własne przeciążenia, overload systemu wroga i FULL POWER w broń, zdejmując defensywy. Korweta przestała działać.

A Arianna broadcastuje 'ratujemy cywili z naszych statków, musieliśmy po nich wrócić'. Kamil na to 'światło nie zostawi Was w ciemności, wróciliśmy by Was uratować'.

* Zaara klaszcze. "Dobra robota, kapitan Verlen. Jesteś lepsza."
* Arianna: "Nie potrzebuję tego słyszeć, ale możesz mówić dalej"
* Zaara: "Jesteś faktycznie diamentem którego komodor Bladawir szukał."
* Arianna: "Mam niezwykle kompetentnych ludzi którzy dają radę w kryzysowych sytuacjach. Którym ufam"
* Zaara: "A będziesz... będziecie jeszcze lepsi."

## Streszczenie

Bladawir przedstawia Inferni Zaarę i informuje o ćwiczeniach, rozgrywając Zaarę przeciw Inferni. Klaudia odkryła że jednostki jakie Infernia ma eskortować mają _Pain Module_. Arianna prosząc przez Krew Elenę doprowadziła do neutralizacji tych modułów. Ćwiczenia okazują się prawie niemożliwe, ale Infernia poświęciła Paprykowiec, 'zniszczyła' jednostki w przewadze i uratowała załogę. Odwróciła ćwiczenia Bladawira przeciw niemu, przy okazji podnosząc swoją chwałę i pokazując że nie będzie grać w cudze gry.

## Progresja

* Arianna Verlen: pokazała niesamowicie dużą kompetencję i zaufanie do swojej załogi wygrywając niemożliwe ćwiczenia w stylu bohaterki a nie w stylu zdeptanego kapitana Orbitera. Szacunek od Bladawira.
* Zaara Mieralit: pod wpływem Kamila Lyraczka widzi ciepło którego nie ma ale za którym tęskni. Nadal jest nieskończenie lojalna, ale wie też że mogłoby być nieco inaczej niż jest.
* Zaara Mieralit: krążą plotki, że Zaara pomaga Bladawirowi w korupcji kapitanów. Niekoniecznie magią (są plotki ale Arianna wie że by wyszło), raczej szuka słabych punktów i je wzmacnia a potem Bladawir stawia tak sytuacje, by oni musieli pójść jego drogą albo być nieefektywni i słabi w oczach swojej załogi

## Zasługi

* Arianna Verlen: odmówiła Patrykowi ćwiczeń, by Zaara za jej plecami zagroziła Patrykowi. Gdy dowiedziała się o _pain amplifiers_, poprosiła Elenę o subtelny sabotaż. Nie dała się wciągnąć w mroczną grę Bladawira (kompetencja lub przyjaźń) - wygrała wszystko. Do tego próbowała dowiedzieć się jak najwięcej o Zaarze rękami Kamila i stabilizowała morale Inferni przy niemożliwych ćwiczeniach.
* Klaudia Stryk: jak najszybciej dostała info o Zaarze (kto to jest) i jak tylko dowiedziała się o ćwiczeniach, paranoicznie sprawdziła dane Paprykowca i dowiedziała się o _pain amplifiers_. Prawidłowo opracowała manewrowanie trzech jednostek przez pole minowe i jak użyć wody z Inferni jako miotacz frag unieszkodliwiający miny. Okazuje się, że jest dobrym oficerem taktycznym, nie tylko w kontekście psychotronicznym.
* Antoni Bladawir: zrobił ćwiczenia by postawić Ariannę przed wyborem 'skuteczność' lub 'dobre serce i przyjaciele', wystawiając jako swoją namiestniczkę Zaarę. Jednocześnie testuje umiejętności i lojalność swoich pozostałych oficerów. Po tej operacji w której Arianna skutecznie ominęła wszystkie jego pułapki jest przekonany do jej kompetencji, acz zmartwiony jej pro-ludzkimi słabościami.
* Zaara Mieralit: hiperlojalna zakochana w Bladawirze mentalistka i infomantka. Noktianka porwana przez Aurelion i odbita przez Bladawira. Bardzo chłodna, nieskończenie lojalna i bezwzględna bardziej niż sam Bladawir. Walczy z Arianną psychologicznie próbując ją zmusić do działania jak inni kapitanowie Bladawira. Jej dusza jest uszkodzona, ale umysł jest sprawny.
* Patryk Samszar: chciał być na ćwiczeniach z Arianną, ale ona obawiając się Bladawira odmówiła. Nacisnął na Martę by napisała mu rekomendację ale nie zadziałało. Gdy Zaara zaproponowała mu dowodzenie Paprykowcem i zrobienie _pain amplifiers_, zgodził się - Arianna da radę! Gdy doszło do pełnej operacji, robił co mógł mimo braku doświadczenia. Kupił czas Ariannie.
* Marta Keksik: poproszona przez Patryka, napisała prośbę do Arianny by ta wzięła Patryka na ćwiczenia. 'Bo on mi smęci nad łóżkiem. Ma moją rekomendację, bo się zamknie'. Rekomendacja nie przekonała Arianny XD.
* Elena Verlen: niechętnie odpowiedziała na prośbę Arianny, ale czyny Bladawira były po prostu złe. Zinfiltrowała Paprykowiec i skutecznie sabotowała pain amplifier, po czym wydostała się niezauważenie. Zrobiła to częściowo dla Arianny (mimo jej ogromnej niechęci do kuzynki) a częściowo ponieważ czyn Bladawira był zły. Świetna akcja o której nikt nie wie.
* Kamil Lyraczek: charyzmatyczny inspirator, po kiepskiej mowie Bladawira wygłosił własną do załogi. Robi show dla Arianny. Wysłany przez Ariannę na wydobycie informacji od Zaary i jej przekonanie o racjach Arianny wykonał zadanie - dowiedział się czego był w stanie i pokazał jej potencjalnie lepszy świat.
* Lars Kidironus: na mostku zaatakował słownie Zaarę, że to spisek jej i Bladawira. Ale Infernia - jego zdaniem - sobie poradzi. Arianna załagodziła sprawę, ale ucieszył ją wybuch Larsa.
* OO Infernia: obecność Zaary na ćwiczeniach i typ ćwiczeń (i nieobecność Eustachego) sprawiły, że Infernia miała prawdziwie skomplikowaną operację i na poziomie morale i zdolności. Klaudia jako oficer taktyczny dała radę, toteż Kamil jako oficer morale. Infernia przeszła przez pole minowe, 'straciła pancerz', ale 'zestrzeliła' dwie jednostki w absolutnej przewadze.
* OO Paprykowiec: na ćwiczeniach jednostka dowodzona przez tienów i eskortowana przez Infernię. Jej TAI pokazała Klaudii potencjalny problem z _pain amplifiers_, Elena je sabotowała i Paprykowiec zrobił co powinien - dał się "zniszczyć" na ćwiczeniach.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Pierścień Zewnętrzny

## Czas

* Opóźnienie: 11
* Dni: 4
