## Metadane

* title: ""
* threads: brak
* motives: /
* gm: żółw
* players: anadia, kić, dzióbek

## Kontynuacja
### Kampanijna

* [Romeo, dyskretny instalator Supreme Missionforce](230523-romeo-dyskretny-instalator-supreme-missionforce)

### Chronologiczna

* .

## Plan sesji
### Projekt Wizji Sesji

* Opowieść o:
    * Inspiracja:
        * Fee Lion "Blood Sisters"
            * "Want to know I'm in my body | Want to know I'd feel the cut | Want to know I've found my sisters | Want to feel it in my blood"
            * KTOŚ szuka swojej siostry
        * Powerwolf "Sacramental Sister"
            * "Corpus for sin and to heaven the soul | Fallen for sin by the breaking of bread | Lamb on the altar and werewolf in bed"
            * ALE ona wybrała inną drogę
        * Trigun. Relacja Legato-to-Vash
        * Blood+. Relacja Diva-to-Saya
    * Theme & Message: 
        * "Jedynie prawdziwa miłość potrafi zmienić się w nieskończoną nienawiść."
            * _im bardziej kochały się kiedyś i wspierały się kiedyś, tym większe cierpienie związane ze zdradą_
        * "Jedna siostra bliźniaczka zdecydowała się podążać dawną drogą, druga wybrała nową ścieżkę. Jedna miała szczęście. Druga została odrzucona."
        * "Prawdziwa nienawiść nie polega na zabiciu drugiej osoby. Polega na zniszczeniu wszystkiego na czym drugiej osobie zależy."
            * _to sprawa OSOBISTA. Narażone są osoby bliskie jednej z sióstr._
        * "Czy istnieje odkupienie i nadzieja na lepsze jutro? Zemsta zawsze niszczy wszystkich."
            * _czy chcemy pomóc rehabilitować Siostrę, czy zostaje jedynie ją zniszczyć po tym co zrobiła?_
        * "Czy asymilacja jest zdradą?"
            * _rodzice Sióstr zginęli w okrutny sposób z ręki astorian. Ale jedna córka dostała miłość. Druga - nienawiść._
    * Jakie emocje?
        * FAZA 1: "Oki, dojdziemy do tego kto stoi za tymi przestępstwami; skuteczny przestępca."
        * FAZA 2: 
            * "Ona była noktiańską agentką?! Może to przeciwnik polityczny?"
            * "Ta Siostra jest NOKTIANKĄ, ale jest lokalna... przykład dobrej asymilacji"
        * FAZA 3: "O, to jest naprawdę doskonale zrobione. Ktoś jej naprawdę nienawidzi. It is personal."
        * FAZA 4: "Too far gone. No hope."
    * Lokalizacja: gdzieś na Astorii
* Po co ta sesja?
    * Jak wzbogaca świat
        * Pokaże tragedię na przykładzie sióstr ze światów Noctis; część się asymilowała, część traktowała asymilantów jako zdrajców.
    * Czemu jest ciekawa?
        * l1: Śledztwo: kto stoi za przestępstwami?
        * l2: Ale jeśli to nie ona to..?
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * the-desecrator: Andrea próbuje zniszczyć Beatę i wszystko co ona reprezentuje na poziomie komórkowym. Rodzinę. Reputację. Chce ją odrzuconą i pogardzaną. Chce Beatę, która CIERPI i TRACI IDEAŁY.
    * the-redeemer: Beata pragnie powrotu Andrei (siostry), chce pomóc Andrei stać się częścią ludzkości w nowym świecie.
    * heart-is-a-weakness: Beata próbuje pomóc Andrei, ale za to straci osoby, na których jej zależy. Andrea ZRANI Beatę w sposób nieodwracalny. "Nie będziesz mieć rodziny."
    * too-far-gone: Andrea zniszczyła swoje życie, zniszczyła tak wiele - tylko, by zemścić się za śmierć swoich rodziców. I zniszczy Beatę, bo ona wyraźnie zapomniała.
    * echa-inwazji-noctis: Andrea i Beata, dwie siostry rodziców zamordowanych przez Astorię. Beata wybrała wybaczenie i asymilację. Andrea wybrała zbrodnie wojenne.
    * deathwish: Andrea nie chce już żyć. Nie ma po co. Przed nią jest jedynie dalsza kaskada śmierci i zniszczenia. Pętla się zaciska.
* O co grają Gracze?
    * Sukces:
        * Beata ma do czego wrócić. Beata nie staje się potworem. Beata nie traci rodziny. Beata nie jest wygnana.
    * Porażka: 
        * Beata zabija Andreę i decyduje się zabić wszystkich noktian, którzy się nie zasymilowali.
* O co gra MG?
    * Highlevel
        * 
    * Co chcę uzyskać fabularnie
        * Beata traci rodzinę; Andrea ją brutalnie morduje.
* Default Future
    * Andrea zabija rodzinę Beaty
    * Beata niszczy Andreę w konfrontacji dwóch nie-do-końca-furii-mataris
* Dilemma: Czy da się jakkolwiek pomóc Andrei? Czy należy ją zniszczyć?
* Crisis source: 
    * HEART: ""
    * VISIBLE: "Andrea robi przestępstwa w imieniu Beaty"
        
### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

.

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

* Scena 0:
    * 
* Faza 1: Brutalne Przestępstwo
    * Beata była w Biolirt; w tym czasie Andrea włamała się do 
    * END: 
* Faza 2: Poszukiwanie zbrodniarza
    * .
    * END: to była Beata..?
* Faza 3: Życie Beaty się rozpada
    * Beata była kiedyś kandydatką na Furię
    * 
    * END: Andrea zabija rodzinę Beaty
* Faza 4: Ostateczna konfrontacja
    * .
    * END: Beata zabija Andreę i wraca do poprzedniego życia.

## Sesja - analiza
### Mapa Frakcji

.

### Potwory i blokady

* Furia Mataris
    * Odporność na promieniowanie, Skażenie, trucizny
    * Wzmocnione kości i gęstość mięśni - są silniejsze i szybsze
    * Wyszkolenie w używaniu większości broni
    * Umiejętność wytrzymania dużego bólu i presji

### Fiszki

* 

* Ślicznospad Verlen: tien Dun Latarnis; 31-letni elegancki miłośnik sztuki i kompetentny administrator. Elegancik, ale dobrze walczy. Pattern: (perfumowany arystokrata)
* Tiana Grolmik: podkochuje się w Mścizębie i zostaje przynętą; lojalna i naiwnie odważna. Pattern: (młoda Asuka Langley)
* Mściząb Verlen: Chce być Verlenem bojowym, chce wygrywać, nie do końca dba o ludzi. Ważne jest zwycięstwo.

### Scena Zero - impl

.

### Sesja Właściwa - impl



## Streszczenie


## Progresja

* . 

## Zasługi

* Elena Samszar: 
* AJA Szybka Strzała: 
* Robert Klejpor: eks-przemytnik, zaufany przez szefa policji 
* Andrea Sarkaes: siostra Beaty; 
* Beata Pirkacz: siostra Andrei; kiedyś prawie-Furia, teraz 
* Darek Pirkacz: mąż Beaty; szef startupowej firmy Drolit, specjalizującej się w dronach powszechnego użytku.
* Fryderyk Kanup: dyrektor firmy Ulkador, skupionej na dronach i psychotronice.
* Marian Labrador: szef ochrony firmy Biolirt; dostarcza Beacie leki dla Furii.
* Tadeusz Urkat: szef policji, zna i lubi Klejpora.

Inne:



## Frakcje

* .

## Fakty Lokalizacji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Karmazynowy Świt:
                                1. Zewnętrzny pierścień wygaszający
                                1. Świt Zewnętrzny: tam mieszkają normalni ludzie, m.in. rodzina Beaty
                            1. Karmazynowy Świt, okolice
                                1. Technopark Karmazyn
                                    1. Centrum Innowacji
                                    1. Kampus Edukacyjny
                                    1. Kompleks Rekreacyjny
                                    1. Strefa Handlowa
                                1. Kompleks Studencki
                                    1. Sektor Hotelowy
                                    1. Sektor Restauracyjny
                                1. Pole Eksperymentów
                                1. Biolirt: firma specjalizująca się w zaawansowanych lekach i biogenezie

## Czas

* Opóźnienie: 
* Dni: 

## Inne
#### Karmazynowy Świt

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Karmazynowy Świt: luksusowe miasteczko Samszarów; ekonomia oparta na usługach, ze szczególnym naciskiem na turystykę, wellness i rozwój osobisty
                                1. Centralny Park Harmonii
                                    1. Eko-spa
                                    1. Sztuczne Plaże
                                    1. Ogrody medytacyjne
                                    1. Pałac Harmonii: luksusowe miejsce odpoczynku i epicki hotel
                                1. Rezydencje
                                    1. Szkoły i sale treningowe
                                    1. Biblioteka
                                1. Zewnętrzny pierścień wygaszający: zapewnia ciszę i spokój
                                1. Kraina Winorośli: winnice, winiarnie, restauracje
                            1. Karmazynowy Świt, okolice
                                1. Centrum Danych Symulacji Zarządzania: zużywa dużo mocy dla utrzymania danych i symulacji managementu. Więcej maszyn niż ludzi. Tradycja dla tradycji?
                                1. Technopark Karmazyn
                                    1. Centrum Innowacji
                                    1. Kampus Edukacyjny
                                    1. Kompleks Rekreacyjny
                                    1. Strefa Handlowa
                                1. Kompleks Studencki
                                    1. Sektor Hotelowy
                                    1. Sektor Restauracyjny
                                1. Pole Eksperymentów

#### Robert Klejpor (graphviz)

digraph Character {
    
    // Core
    node [shape=rect]
    rankdir=LR;
    
    Centrum [label="ROBERT KLEJPOR\nDrakolita\nPrzemytnik,\nDetektyw\nWojownik", shape=ellipse, fontname="bold"];
    {
        rank=same;
        Cechy [label="Cechy,\nwłasności"];
        Wiedza [label="Co wie szczególnego\nJaką wiedzę dostarcza?"];
        Relacje [label="Unikalne Relacje\nZ kim się lubi?"];
        Zasoby [label="Unikalne Zasoby\nDo czego ma dostęp?"];
        Akcje [label="Akcje\nCo robi szczególnie dobrze?"];
    }

    // Łączenie węzłów
    Centrum -> Cechy;
    Centrum -> Wiedza;
    Centrum -> Relacje;
    Centrum -> Zasoby;
    Centrum -> Akcje;

    // Subcomponents for 'Cechy'
    CechyNode1 [label="Drakolita: zmodyfikowany genetycznie\nsilny i bardzo szybki"]
    CechyNode2 [label="Dużo lepsze zmysły i pamięć niż normalni ludzie"]
    CechyNode2 [label="Doskonale rozpoznaje szczegóły"]

    // Links for 'Cechy'
    Cechy -> CechyNode1
    Cechy -> CechyNode2
    
    // Subcomponents for 'Wiedza'
    WiedzaNode1 [label="Gdzie są najlepsze kryjówki? Gdzie można co i jak ukryć?"]
    WiedzaNode2 [label="Kto miał problemy z prawem / innymi słabościami?\nJak wpłynąć na różne osoby?"]
    WiedzaNode3 [label="Jak wyceniać różne rzeczy?\nCo powinno 'tu' być a co nie?"]
    WiedzaNode4 [label="Był w wojsku i walczył na froncie;\nwie jak zastawiać pułapki i ich unikać"]
    
    // Links for 'Wiedza'
    Wiedza -> WiedzaNode1
    Wiedza -> WiedzaNode2
    Wiedza -> WiedzaNode3
    Wiedza -> WiedzaNode4

    // Subcomponents for 'Relacje'
    RelacjeNode1 [label="Tadeusz Urkat: szef policji;\n(mają wspólną przeszłość i Urkat go szanuje)"]
    RelacjeNode2 [label="Fryderyk Kanup: dyrektor firmy Ulkador;\n(gdy Fryderyk wpadł w kłopoty, pomogłeś mu)"]
    RelacjeNode3 [label="Darek Pirkacz: szef startupowej firmy Drolit;\n(dobre serce i bardzo pomocny; pomaga sierotom)"]

    // Links for 'Relacje'
    Relacje -> RelacjeNode1
    Relacje -> RelacjeNode2
    Relacje -> RelacjeNode3

    // Subcomponents for 'Zasoby'
    ZasobyNode1 [label="Zawsze ma przy sobie ukrytą broń;\nnigdy nie jest zaskoczony"]
    ZasobyNode2 [label="Zna mnóstwo ludzi i większości w czymś jakoś pomógł;\nludzie go szanują i "]
    ZasobyNode3 [label="Coś, na czym innym zależy;\nzwykle umie innych przekupić"]
    ZasobyNode4 [label="Ma poukrywane dziwne rzeczy z czasów;\nburzliwej młodości, też nielegalne"]

    // Links for 'Zasoby'
    Zasoby -> ZasobyNode1
    Zasoby -> ZasobyNode2
    Zasoby -> ZasobyNode3
    Zasoby -> ZasobyNode4

    // Subcomponents for 'Akcje'
    AkcjeNode1 [label="Doskonale wyczuwa uczucia drugiej osoby;\nświetnie manipuluje ludźmi"]
    AkcjeNode2 [label="Odwraca uwagę, ściągając ją na siebie\nlub zrzucając gdzieś indziej"]
    AkcjeNode3 [label="Robi się całkowicie nieruchomy;\npraktycznie nie do zauważenia"]
    AkcjeNode4 [label="Znajduje kryjówki i przejścia;\nzawsze jest sposób COŚ zrobić"]

    // Links for 'Akcje'
    Akcje -> AkcjeNode1
    Akcje -> AkcjeNode2
    Akcje -> AkcjeNode3
    Akcje -> AkcjeNode4
}
