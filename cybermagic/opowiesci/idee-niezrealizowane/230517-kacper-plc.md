## Metadane

* title: ""
* threads: historia-eustachego, arkologia-nativis
* gm: żółw
* players: fox, kapsel

## Kontynuacja
### Kampanijna

* [230329 - Zdrada rozrywająca arkologię](230329-zdrada-rozrywajaca-arkologie)

### Chronologiczna

* [230329 - Zdrada rozrywająca arkologię](230329-zdrada-rozrywajaca-arkologie)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * .
* CEL: 
    * Bardzo wiele wymiarów celu. Bardzo.
        * Ardilla szuka maga Nihilusa.
        * Muszę pokazać obecność maga Esuriit kontrolującego Trianai i efekt jego działań
        * Kapsel potrzebuje godnej, uczciwej walki w mechanice walki z inicjatywą i wymiarami
        * Zbliżamy się do endgame.
            * Ktoś zauważy, że Infernia jest jedna a arkologia ma wiele różnych interesów - gaszenie pożarów
                * Foreshadowing Overlorda (używa Astinian / Vigilusa? Dark Infernia? Na pewno - cel to Infernia)
    * Więc
        * Wujek wychodzi ze szpitala i Infernia nie słucha jego poleceń
        * Ktoś kontroluje Trianai i Skorpiony mają przechlapane
        

### Co się stało i co wiemy

* Kontekst
    * .

### Co się stanie (what will happen)

* S1: Ardilla - ktoś rozpytuje o Infernię. To ktoś bardzo kompetentny (farighan recykler). Hunt. (OVERLORD)
    * czy ktoś dbający o Infernię należy do Farighanów?
    * Symbol czarnej świecy i czerwonego węża.
        * Kidiron jest zaniepokojony.
        * Wujek coś wie. Uważa, że nie powinno być problemu. Trzeba zostawić wiadomość - Infernia jest pod kontrolą. 
* S2: Ludzie ze Skorpionów coś wiedzą. 
    * Kornelia Lichitis, Anna Seiren: one wiedzą o:
        * sarderyckim crawlerze uciekającym od Trianai i farighanów
        * Magda Misteria Sarbanik dołączyła do Coruscatis (z wiłami i siłami Lirvint), niedaleko Trianai.
            * niewielka grupa najemników próbowała uderzyć w Coruscatis, dwóch magów to odparło
        * 
* S3: 

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Amara Zegarzec
    * ENCAO:  +0-+0 |Spontaniczna;;Niecierpliwa;;Pomocna| VALS: Self-direction, Achievement| DRIVE: Młodsza Siostra
    * styl: wesoła "młodsza siostra" próbująca zrobić wszystko pomocne
    * dusza hotelu: hospitality, majsterkowiczka, gotowanie, wiedza o lokalnym terenie
* Ludmiła Zegarzec: starsza siostra Amary
    * ENCAO:  00+-+ |Egocentryczny;;Oportunista;;Przewidujący, trudny do zaskoczenia| VALS: Stimulation, Power >> Humility| DRIVE: Ferengi
    * styl: przedsiębiorcza, zna swoje prawa
    * uwielbia hotel i teren kwiatów i menhirów
    * typowa reakcja: "wszystko dlatego, bo nie próbujesz zmaksymalizować korzyści"
* Franciszek Chartowiec: dziennikarz Paktu szukający sensacji za wszelką cenę
    * ENCAO:  00--+ |Zdradliwy;;Robi bałagan / problemy i nie sprząta po sobie| VALS: Universalism, Achievement| DRIVE: Wanderlust
    * styl: Donny Vermillion (SC2)
    * wykorzystuje mnóstwo dron i mechanizmów obserwacyjnych, chce zrobić z Karolinki atrakcję turystyczną
* .Świnka Karolinka: hybrydowy glukszwajn zagłady
    * akcje: "psotna, przeszkadzajka", "teleportuje się", "poluje na magię", "eksploduje i ranteleportuje pod wpływem stresu"
    * siły: "destabilizator magii", "ranteleportacja"
    * defensywy: "niezwykle antymagiczna", "ranteleportacja"
    * słabości: "to tylko świnia"
    * zachowania: "szuka magii i ją pożera", "ciekawska, szczurzy", "UWIELBIA kiszone marchewki"
    * Inne
        * Wyjątkowo głośna, potrafi śpiewać i uczy się odpowiednich dźwięków. Nauczyła się "Ka-ro-li-nus ko~chanie"
        * Wyjątkowo żądna uwagi i figlarna jak na glukszwajna. Co gorsza, strasznie ciekawska. Kocha duże wysokości.
        * Standardowo: teleportacja, pożeranie magii, uszkadzanie magii (+5Ob)
* .Lokacja: Kwiatowe Pole
    * populacja: "duchy opiekuńcze", "osoby szukające ukojenia"
    * teren: "nieskończony kwiatowy raj", "menhir centrujący energię magiczną"
    * hazard: "niewidoczny teren pod nogami", "pomniejsze magiczne efemerydy"
    * mutatory: "ogromna ilość otwartego terenu", "miejsce magicznych rytuałów"
* .Lokacja: Hotel Odpoczynek Pszczół
    * populacja: "rodzina właścicieli", "goście z różnych obszarów"
    * teren: "malownicza okolica", "starannie utrzymane ogrody", "niewielka ilość pokoi"
    * hazard: "wścibskie lokalne duchy", "plotki i ploteczki"
    * mutatory: "kursy i warsztaty robione przez właścicieli", "właściciele dbają o gości jak o rodzinę", "sezonowe wydarzenia"

### Scena Zero - impl

Amara - piękny poranek. Duchy uciekły z pola kwiatów XD. Są w hotelu. Chrumknięcie za oknem. Świnia patrzy na hotel podejrzliwie. Amara łapie za telefon i najbliższy rolnik "uciekła Ci świnia, Roman".
Świnia teleportowała się do pokoju. Karolinka się przestraszyła i teleportowała się poza pokój.
Śpiew świni z imieniem "Karolinus"...

### Sesja Właściwa - impl

Karolinus i Elena, spotkanie z Aleksandrem Samszarem. Bo centrala chciała byście z nim porozmawiali. Karolinus spytał, czy tam była Viorika i zostawiła dlań wiadomość. Aleksander powiedział, że tam była Elena Verlen i chyba ona dowodziła operacją. Trzy Verlenki - Arianna, Viorika, Elena Verlen.

Aleksander: "czy coś było nietypowe?". Ich pojazd ledwo trzymał się kupy, Elena (15-latka) próbowała go uwieść, chyba, lub zastraszyć (nie jest pewny). Dwie pozostałe Verlenki były przebrane za dźwiedzie. Ale holoprojektory były uszkodzone. Elena Verlen była dumna, że on się dał nabrać. I wpadły po to by rozwalić hybrydę oraz aparycję Vioriki w bikini. I wszystko zabrały do Verlenlandu. Aleksander nawet pomagał, bo tak jest lepiej dla Siewczyna.

Aleksander nie leci z Wami do Doliny Kwiatowej. Tam jest świnia. 20-30 km stąd. Elena pyta Aleksandra, czy pomoże przy świni jeśli będzie to niezbędne. Zdziwił się, że potrzebują pomocy przy ŚWINI skoro POKONALI LEMURCZAKA. On myśli, że pokonali Lemurczaka. Pogratulował im tego aktu, jest zadowolony.

Dolina Kwiatowa. I pojazd zastępczy. Z prostym TAI.

Docieracie do Doliny Kwiatowej. Dwa autokary turystyczne. Ludzie robią zdjęcia. Świnia na menhirze. Ktoś poumieszczał tabliczki "najlepszy pensjonat - Odpoczynek Pszczół", "Najlepsze zdjęcia świni, tylko u nas!". I stanowisko z łakociami dla świni. Dla Eleny na pierwszy rzut oka to paskudny problem, bo to glukszwajn.

* teleportuje
* eksploduje jak za duży stres
* niezwykle antymagiczna i destabilizuje magię i żywi się nią

Elena zapisuje tytuły do Karolinusa "to jest o zaprzyjaźnianiu się ze świnią". Karolinus "faceci się nie nadają do wychowania". Elena "szykuj się jak będziesz miał dzieci" XD. Ogólny plan - jak kontrolować glukszwajny.

Pojazd zastępczy jest elegancki, "rządowy". Więc od razu (2-3 minuty) pojawia się przy Was dziennikarz. I zaczyna nawijać do drony (filmuje). "panie i panowie, Samszarowie rozwiązują problem. Czy mogę mieć komentarz dla Paktu". Elena "bez komentarza". Chartowiec "słyszeliście o tym po raz pierwszy w Pakcie - tajemniczy Karolinus do którego śpiewa świnia!".

dane z ChatGPT4 ("co najbardziej lubi jeść świnia jako smakołyk"):

"
Świnie są zwierzętami wszystkożernymi i cieszą się wieloma różnymi pokarmami. Wielu hodowców i właścicieli zwierząt domowych zauważyło, że świnie szczególnie cieszą się następującymi "smakołykami":

Owoce: Świnie uwielbiają owoce, takie jak jabłka, gruszki, banany, melony czy arbuzy. Należy jednak zwracać uwagę, by podawać owoce w umiarkowanych ilościach, ponieważ są one bogate w cukry.

Warzywa: Warzywa, takie jak marchew, brokuły, ziemniaki czy dynia, są również bardzo lubiane przez świnie.

Zboża: Świnie lubią zboża, takie jak pszenica, kukurydza, owsy czy jęczmień.

Orzechy i nasiona: Orzechy i nasiona, takie jak orzechy włoskie, migdały czy nasiona słonecznika, są również atrakcyjne dla świń, ale podobnie jak owoce, powinny być podawane w umiarkowanych ilościach ze względu na wysoką zawartość tłuszczu.

Pokarmy zwierzęce: Świnie mogą także zjeść pewne rodzaje pokarmów zwierzęcych, takich jak robaki, larwy czy jaja.

Pamiętaj jednak, że każda świnia jest inna i to, co jedna świnia uwielbia, inna może zignorować.
"

Tien Samszarowie się kłócą KTO będzie karmił świnie. Przecież nie może ich nagrać Chartowiec XD. Karolinus "ja zapłacę za smakołyki Ty jej dasz". Elena "ja zapłacę i Ty jej dajesz". Karolinus "słyszałem że kobiety lubią jak się za nie płaci". Elena "to dokarm swoją świnię."

Elena idzie do Amary. Pyta co ta świnia lubi jeść. Amara odpowiada, że marchewka jest interesująca. Mówi, że ŚWINIA ATAKUJE DUCHY i uciekają. Elena "nie damy ducha na pożarcie, może lubić warzywka". Na to Chartowiec "słyszeliście po raz pierwszy, Samszarowie stają przeciwko świniom! Czy by pokonać świnię tien musi się taplać w błocie?"

Elena -> Karolinus: jabłko, marchewka. Wyprowadźmy tą świnię (na głos). Ja Cię asekuruję.

Karolinka zeskoczyła i zaczęła gonić duchy. Na pewno coś złapie. Elena nie może na to pozwolić. Rzuca jabłkiem w świnkę.

Tr (niepełny) +3: 

* X: doskonale zarejestrowane na filmie, SLOW MOTION
* V: jabłko trafia w świnkę. Świnka otrzepuje głowę. Świnka to zjada.
* Vr: świnka dostała kolejne jabłko i gruszkę a duchy zdążyły się wycofać
    * słychać komentarz Chartowca

Karolinus poprosił Chartowca o nagranie. Sprzeda mu newsa. Chartowiec dziękuje. Dał nagranie wiernemu oglądaczowi. Na świni jest wytatuowane "Karolinka kocha Karolinusa a Karolinus kocha Karolinkę".

Karolinus sugeruje Chartowcowi, że Elena jest jego szefową.

Tr (przewaga +lepsza opowieść) +3:

* V: Chartowiec przekierowuje uwagę na Elenę. Bo Karolinus to asystent.
* Vr: Chartowiec nie spyta Karolinusa o imię. Elena jest tą ważną dla niego i oglądaczy Paktu.

Chartowiec robi w tej chwili nagranie z szamanem, który narzeka co się tu dzieje. Amara poproszona o hotel daje znać siostrze - dwa pokoje. Amara "to się nie godzi". Ale ma zostać między nimi. OCZYWIŚCIE.

Elena i Karolinus decydują się na opuszczenie terenu i pojechanie do hotelu. Chartowiec "świnia 1 Samszarowie 0!!!".

Hotel jest zadbanym ładnym miejscem. Ludmiła sprzedała mu wyszywaną ręcznie świnię, drogo. Opowiedziała o tym, jak świnia uciekła po tym jak wbiły się w menhir. I magowie idą do pokojów. A Amara w tym czasie zwija stragan - jest ciekawa co się stanie. Ludmiła opieprzyła za to, że nie zarabia. Amara - że kasa jest tutaj.

Zanim zaklęcie - środek na glukszwajny.

Tr (przewaga Z: tien + teren, przewaga Z: Aleksander) +2 -> Tp +2:

* Vr: Aleksander Samszar dostarczy środka usypiającego na glukszwajny
* V: zrobione szybko - do 30 minut będzie środek usypiający, przyjedzie kurier.

Gdy czekają, Karolinus odbiera od Amary ręcznik i łakocie dla świni. Elena powiedziała, że może dopilnować by nikt im nie przeszkadzał i nikt nie wiedział o ich obecności. Zwłaszcza pan od Paktu. (oczywiście, on już tu jest XD). Zanim biedna Amara jest wygoniona - "ta świnia jest groźna, musiałam ją przeganiać od hotelowych przyjaciół, jednego straciliśmy". Elena "rozprawimy się ze świnią". Dla Amary wyraźnie ważne są te duchy. Elena "też zależy mi na dobrostanie naszych duchów", "jakiego ducha straciliście?". Amara "to był nasz stary opiekun hotelu. Kupił czas innym. Inni mogli się wycofać. Ona tu WSKOCZYŁA! Wszyscy myślą, że ta świnia jest słodka a to potwór." Elena pacą Amarą po ramieniu. Wyraża sobą zrozumienie i współczucie.

Karolinus zamyka drzwi, szuka kuriera - i zauważył drony dziennikarskie. Filmują. Okno i wszystko. I Karolinus przekazuje info Elenie, że drony podglądają. I tak - podjechał kurier, przekazał czarną paczkę i odjechał, typowo po żołniersku. I Karolinus wraca do Eleny. I zasłaniają zasłony. I wywalił Amarę XD.

Magowie rzucają wspólne zaklęcie mające zrobić nęcącą, uciekającą gruszkę energią duchową. Uciekinier do zabawy świni. W formie gruszki.

Tr (przewaga: znajomość glukszwajnów, glukszwajn lubi owoce) M +2 +3Ob:

* Xm: ŚWINIA JEST TUTAJ! Teleportowała się do nich. Okna rozsłonięte. A napromieniowani są Karolinus i Elena. Nie jabłko.
* V: WIEJEMY PRZED ŚWINIĄ. I ONA ZA NAMI!!!
* V: Niszczymy okna, drony, pół pokoju ALE nic się nie nagrało.
    * Tak, trzeba zapłacić
    * Pakt nie ma tego filmu jak wieją i wylizuje ich świnia
* Vz: Karolinka się teleportuje W ŚRODKU pojazdu, wylizuje Was, mizia się, jest najpoczciwszą świnką ever.
    * Potem zjadła jabłko usypiające i zasnęła.
    * WY czujecie się coraz bardziej zmęczeni i coraz słabsi

Karolinus dał dwóm żołnierzom z pierwszego garnizonu misję specjalną - zawieźć świnię do Verlenlandu i zrzucić w pierwszej wiosce.
Potem popatrzyli jak ich limuzyna pojechała w dal.

Powiedział wszystko Aleksandrowi. Aleksander nic nie powiedział. Nie wiedział co miał powiedzieć. "To chyba dobry ruch. Podłożyli Ci świnię, więc im oddałeś". Zrobi notę dyplomatyczną do Verlenów, bo duchy ucierpiały. A Pakt? "Elena Szamszar i jej wierny asystent pokonali świnię w Tajnym Planie Samszarów, podkładając świnię Verlenom. Oko za oko, świnia za świnię."

## Streszczenie

Karolinka, świnka podłożona przez Verlenów na Wielkie Kwiatowisko zaczęła polować na lokalne duchy a przedsiębiorcza Ludmiła z pobliskiego hotelu zorganizowała dziennikarza Paktu i okazję do zarobienia. Karolinus i Elena przebili się przez problematycznego dziennikarza Paktu, zwabili świnkę do pojazdu i uśpili oraz podrzucili ją (rękami żołnierzy) do Verlenlandu. A Strzała jest naprawiana.

## Progresja

* Karolinus Samszar: na wideo Paktu gdy zwalczali glukszwajna jako "asystent Eleny Samszar". Popularność wśród Paktu rośnie.
* Karolinus Samszar: zdaniem Verlenów, jest z nimi kwita jak chodzi o podłe rzeczy które się sobie robi.
* Viorika Verlen: zdaniem Verlenów, odpowiednio pomszczona za Karolinusa. Nic nie musi robić w sprawie tego co on robił do tej pory.
* Elena Samszar: na wideo Paktu gdy zwalczali glukszwajna jako "opiekunka duchów i obrończyni ich przed świnią". Popularność wśród Paktu rośnie.

### Frakcji

* .

## Zasługi

* Karolinus Samszar: kłóci się z Eleną kto zajmie się świnką, udaje asystenta Eleny (przed Paktem), uśpionego glukszwajna wysyła do Verlenlandu rękami dwóch żołnierzy.
* Elena Samszar: chciała uniknąć straty twarzy, ale bardziej chciała chronić duchy przed glukszwajnem. Unikała prasy, ale rzuciła w świnkę jabłkiem. Zniszczyła drony dziennikarza "przypadkiem".
* Amara Zegarzec: współwłaścicielka hotelu Odpoczynek Pszczół; starała się zrobić co może by chronić lokalne duchy i ekosystem. Mimo, że siostra próbowała zarobić. Przekonywała Elenę o współpracę.
* Aleksander Samszar: czyści Siewczyn do końca i wyjaśnił, co zrobiły Verlenki. Doszedł do tego, że były też na Kwiatowisku i wysłał tam Karolinusa. Dostarczył poproszony substancję do uśpienia glukszwajnów. Zadowolony z działań Karolinusa i Eleny - pokonali Lemurczaka (nie oni, ale on tego nie wie).
* Ludmiła Zegarzec: główna właścicielka hotelu Odpoczynek Pszczół. Sprzedała Karolinusowi wyszywaną świnię, ściągnęła tu dziennikarza i wycieczki... she's milking the glukszwajn to extremes.
* Franciszek Chartowiec: dziennikarz Paktu; znalazł temat glukszwajna wśród duchów na Kwiatowisku i siedział na tym temacie ile mógł, podnosząc Elenę Samszar i robiąc quizy jak był w stanie. Irytujący.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Wielkie Kwiatowisko
                                1. Menhir Centralny
                                1. Hotel Odpoczynek Pszczół: mały hotel rodzinny p.d. rodziny Zegarców (reprezentowanych przez Ludmiłę i Amarę)

## Czas

* Opóźnienie: 3
* Dni: 2

## OTHER
### Fakt Lokalizacji
#### Wielkie Kwiatowisko


1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Wielkie Kwiatowisko 
                                1. Menhir Centralny
                                1. Hotel Odpoczynek Pszczół

