## Metadane

* title: "Operacja Cognitio Nexus"
* threads: kidiron-zbawca-nativis
* motives: ?
* gm: żółw
* players: fox, kapsel

## Kontynuacja
### Kampanijna

* [230816 - Orbiter, Nihilus i ruch oporu w Nativis](230816-orbiter-nihilus-i-ruch-oporu-w-nativis)
* [230810 - Nie nazywasz się Janusz](230810-nie-nazywasz-sie-janusz)

### Chronologiczna

* [230816 - Orbiter, Nihilus i ruch oporu w Nativis](230816-orbiter-nihilus-i-ruch-oporu-w-nativis)

## Plan sesji
### Theme & Vision & Dilemma

![Plan sesji](mats/2308/230830-cognitio-nexus.svg)

* PIOSENKA: 
    * ""
* CEL: 
    * Highlv
        * 
    * Tactical
        * ?

### Co się stało i co wiemy

.

### Co się stanie (what will happen)

.

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Marcel Draglin: oficer Kidirona dowodzący Czarnymi Hełmami, dumny "pies Kidirona"
    * (ENCAO: --+00 |Bezbarwny, bez osobowości;;Bezkompromisowy i bezwzględny;;Skupiony na wyglądzie| VALS: Family (Kidiron), Achievement >> Hedonism, Humility| DRIVE: Lokalny społecznik)
    * "Nativis będzie bezpieczna. A jedynym bezpieczeństwem jest Kidiron."
    * Najwierniejszy z wiernych, wspiera Rafała Kidirona do śmierci a nawet potem. Jego jedyna lojalność jest wobec Rafała Kidirona. TAK, to jego natura a nie jakaś forma kontroli.
* Marzena Marius: była komodor Orbitera, która zdecydowała się na zbudowanie lepszego życia na Neikatis (ma ok. 15 doświadczonych osób); wie o farighanach
    * OCEAN: (C+A+): Emanuje spokojem i pewnością siebie, potrafi zjednać sobie tłumy swoją retoryką i charyzmą. "gdy z nią rozmawiasz, czujesz jej głęboką wiarę w jej przekonania"
    * VALS: (Power, Security): Jej zdaniem kontrakt społeczny ma stabilizować systemy "Każdy system jest tyle wart, ile stabilności może przynieść."
    * Core Wound - Lie: "brak zdecydowanego działania doprowadził już do tej samej tragedii" - "Moja wizja to jedyna droga do pokoju"
    * Styl: nie unika trudnych tematów, podaje je w przemyślany, logiczny sposób. Jej retoryka koncentruje się na potrzebie jedności i silnego kierownictwa
    * metakultura: Atarien: "prawdziwa siła leży w jedności pod właściwym kierownictwem"
* Gerard Savaripatel
    * OCEAN: (C+A-): Wybuchowy ALE cierpliwy, lodowo-ognisty, lojalny wobec załogi i rodziny. Nie wybacza wrogom. Tendencje do fokusowania na jednej rzeczy na raz
    * VALS: (Benevolence, Hedonism): "Honor i zaufanie są wszystkim. Ale dobrze też korzystać z życia"
    * Core Wound - Lie: "Wygrzebałem się z nicości, wbijając nóż w plecy przyjacielowi" - "Jedyną drogą jest wieczna ekspansja i plany alternatywne. Przegrana to śmierć."
    * Metakultura: Atarien: "Zawłaszczę wszystko, czego nie dam rady - spalę."
    * Chce: odzyskać córkę.
* Teresa Kritoriin
    * OCEAN: (C+A+): Perfekcjonistka z uśmiechem, zawsze dba o szczegóły. "Diabeł tkwi w szczegółach, ale i anioł też."
    * VALS: (Conformity, Tradition): "Trzymajmy się razem, jak zawsze to robiliśmy."
    * Core Wound - Lie: "?" - "?"
    * Styl: Dokładna w każdym zadaniu, zawsze zwraca uwagę na małe rzeczy, które mogą zrobić różnicę.

### Scena Zero - impl

.

### Sesja Właściwa - impl

## Streszczenie

## Progresja

* .

## Zasługi

* Ardilla Korkoran: 
* Eustachy Korkoran: 

* Ralf Tapszecz: 
* Gerard Savaripatel: 
* Teresa Kriitorin: córka, ostatnia aktywna; 
* Łucja Kriitorin: terrorform queen; 


* Marcel Draglin: blokuje ruchy Eustachego by zrzucić politykę publicznie na Ardillę i Kalię; jego zdaniem on oraz Eustachy są od 'krwawej roboty' i dziewczyny mają być symbolem czegoś lepszego. Lepszy politycznie niż się wydawało, bo nauczył się czegoś od Kidirona. Największy sojusznik Ardilli i Kalii w arkologii, mimo, że obie go nie lubią i nim gardzą. Infernia ma być niewinna. Z woli Kidirona.
* Kalia Awiter: konsultuje się z Ardillą w sprawie tego jak pokonać Marzenę Marius (z ruchu oporu). Jako regentka tymczasowo jest symbolem wszystkiego co dobre i piękne.
* Marzena Marius: niesamowicie dobry polityk i taktyk, eks-Orbiter i idealistka; zażądała wydania przez Infernię ludzi z Inferni co skrzywdzili niewinnych (eks=piratów) oraz Laurencjusza i Feliksa Kidironów. Spokojnie umacnia rząd dusz, fortyfikuje pozycje i pozycjonuje się jako sędzia. Ardilla nie jest w stanie jej łatwo politycznie wymanewrować.

## Frakcje

* Aurora Nativis: sformowana z ruchu oporu anty-Kidironowego i przejęta przez Marzenę Marius (kiedyś z Orbitera), zdobywa rząd dusz przeciw Inferni Nativis
* Infernia Nativis: niekwestionowani przywódcy Arkologii Nativis, acz spadają na nich zbrodnie Kidirona i te które popełnili (m.in. po działaniach Saviripatel)

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Nativis

## Czas

* Opóźnienie: 0
* Dni: 1
