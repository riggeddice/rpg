## Metadane

* title: ""
* threads: ?
* motives: ?
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [240107 - Perfekcyjne wykorzystanie unikalnych zasobów](240103-perfekcyjne-wykorzystanie-unikalnych-zasobow)

### Chronologiczna

* [240107 - Perfekcyjne wykorzystanie unikalnych zasobów](240103-perfekcyjne-wykorzystanie-unikalnych-zasobow)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * In This Moment "Sick Like Me"
        * F1: "Is it sick of me to need control of you? | Is it sick to make you beg the way I do?"
        * F2: "Am I beautiful (am I beautiful) | As I tear you to pieces? (As I tear you to pieces?) | Am I beautiful? (Am I beautiful?) | Even at my ugliest, you always say | I'm beautiful (am I beautiful)"
        * Elena ulega Korupcji ze strony innych tienów by nie szalała. Ale to się odwraca przeciwko nim - nie są w stanie opanować ani kontrolować Eleny. Nikt nie jest. She unleashes.
* Opowieść o (Theme and vision)
    * Elena jest tak niesamowicie niepopularna, że inni rotują dookoła niej.
    * Kilka tienów decyduje się pobawić jej kosztem i wykorzystują chemical compounds do tego celu - a ona jest tak pewna siebie, że tego nie zauważa
    * Elena odzyskuje kontrolę, przez _Esuriit in her veins_. Upokorzona, decyduje się pokazać im jak daleko może się posunąć poza kontrolą.
    * SUKCES
        * 
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    

    * tools-of-enslavement: tieni wykorzystujący 'anipacis', którzy próbują przejąć kontrolę nad Eleną. Niech będzie posłuszniejsza i grzeczniejsza. Nie wiedzą jednak co się z nią dzieje i jak reaguje...
    * niepelna-kontrola: Elena, pod wpływem środków którymi ją nafaszerowano ('anipacis'). Z jednej strony jest milutka, z drugiej jest mordercza.
    * the-saboteur: tieni postanawiają zneutralizować Elenę, nieważne jakimi metodami - nie da się z nią już wytrzymać. Poszli w pomysł Konstantego Keksika (który też nie do końca wiedział co to 'anipacis'). Zadziałało (jesteśmy po tym jak wygrali)
    * wrobieni-lecz-niewinni: Leszek Kurzmin nic nie wiedział, a wiele wskazuje na to że on o tym wiedział i to zaaprobował.
    * subtelna-ingerencja-polityczna: cała sprawa z Eleną to manewr ze strony komodor Anety Sarbeńczyk, skierowany przeciw Kurzminowi, Elenie i innym tienom w kosmosie.
    * glupota-i-bezmyslnosc: atakowanie Eleny przez grupę tienów po linii emocjonalnej by ją upokorzyć to najgłupsze co można zrobić, nawet, jeśli Kurzmin podobno to aprobuje.
    * ruination-unsealed: po raz pierwszy (i od dawna jedyny) Elena używa pełni swoich umiejętności przeciwko magom i ludziom a nie tylko potworom.
    * cannot-control-this: próbowali kontrolować Elenę używając magii biologicznej i środków biologicznych, ale Elena jest praktycznie niemożliwa do kontroli
* Detale
    * Crisis source: 
        * HEART: "Nie da się zaufać Elenie. Nikt jej nie ufa, nikt jej nie rozumie, nie da się do niej zbliżyć."
        * VISIBLE: "Love plague"
    * Delta future
        * DARK FUTURE:
            * chain 1: Castigator ma problem (Elena go uszkadza LUB potrzebne wsparcie zewnętrzne)
            * chain 2: Za dużo rannych lub nawet ofiar śmiertelnych
        * LIGHT FUTURE:
            * chain 1: 
            * chain 2: 
    * Dilemma: brak

### Co się stało i co wiemy

* Castigator
    * Elena powoduje kłopoty przez co inni tieni wpadają w tarapaty
    * Zdesperowani, wierząc, że mają wsparcie kapitana Kurzmina, decydują się na operację 'neutralizacja Eleny'
        * Za tym stoi tien 
    * Elena jest częściowo pod wpływem 'anipacis' - łagodniejsza, mniej groźna, bardziej uległa
    * Elena się nawet zaprzyjaźnia z Patrykiem Samszarem, Kamilem Burgaczem i znajduje pokój z Martą Keksik.
    * Bioforma Eleny nie tyle adaptuje co jest _spreaderem_ 'anipacis', powodując masową 'disinhibition / love plague' - adaptacja do Władawca
    * Kurzmin prosi Klaudię o audyt bo coś może być nie tak z powrotem Alteris.

### Co się stanie (what will happen)

* F1: Problem Exists
* F2: Elena Located
* F3: Elena Unchained
* CHAINS
    * Elena
        * reputacja (too sweet), reputacja (deadly killer)
        * magia: (dewastacyjna integracja z Castigatorem, omnidetekcja i tarcze)
        * zniszczenie przyjaźni i prawdziwa destrukcja innych
        * krzywda Eleny
    * Castigator
        * krzywda ludzi - cywili itp - na pokładzie Castigatora, uszkodzenie strukturalne, rany tienów
        * demolka reputacji Kurzmina
* Overall
    * stakes
        * Elena - jej reputacja i standing na Castigatorze
        * Kurzmin - jego standing na Castigatorze i ogólnie stan Castigatora
        * tieni na Castigatorze i ich zdrowie, ogólna integracja
        * Broadcasting informacji
    * opponent
        * _Plague_
        * Elena w trybie "I WILL BURN IT ALL"
        * Systemy defensywne Castigatora
    * problem
        * Szaleństwo zdradzonej i niestabilnej Eleny
        * Nikt nie wie co się tu stało

## Sesja - analiza

### Fiszki

Zespół:

* Leona Astrienko: psychotyczna, lojalna, super-happy and super-dangerous, likes provocative clothes
* Martyn Hiwasser: silent, stable and loyal, smiling and a force of optimism
* Raoul Lavanis: ENCAO: -++0+ | Cichy i pomocny;; niewidoczny| Conformity Humility Benevolence > Pwr Face Achi | DRIVE: zrozumieć
* Alicja Szadawir: młoda neuromarine z Inferni, podkochuje się w Eustachym, ma neurosprzężenie i komputer w głowie, (nie cirrus). Strasznie nieśmiała, ale groźna. Ekspert od materiałów wybuchowych. Niepozorna, bardzo umięśniona.
    * OCEAN: N+C+ | ma problemy ze startowaniem rozmowy;; nieśmiała jak cholera;; straszny kujon | VALS: Humility, Conformity | DRIVE: Uwolnić niewolników
* Kamil Lyraczek
    * OCEAN: (E+O-): Charyzmatyczny lider, który zawsze ma wytyczony kierunek i przekonuje do niego innych. "To jest nasza droga, musimy iść naprzód!"
    * VALS: (Face, Universalism, Tradition): Przekonuje wszystkich do swojego widzenia świata - optymizm, dobro i porządek. "Dla dobra wszystkich, dla lepszej przyszłości!"
    * Styl: Inspirujący lider z ogromnym sercem, który działa z cienia, zawsze gotów poświęcić się dla innych. "Nie dla chwały, ale dla przyszłości. Dla dobra wszystkich."
* Horst Kluskow: chorąży, szef ochrony / komandosów na Inferni, kompetentny i stary wiarus Orbitera, wręcz paranoiczny
    * OCEAN: (E-O+C+): Cichy, nieufny, kładzie nacisk na dyscyplinę i odpowiedzialność. "Zasady są po to, by je przestrzegać. Bezpieczeństwo przede wszystkim!"
    * VALS: (Security, Conformity): Prawdziwa siła tkwi w jedności i wspólnym działaniu ku innowacji. "Porządek, jedność i pomysłowość to klucz do przetrwania."
    * Core Wound - Lie: "Opętany szałem zniszczyłem oddział" - "Kontrola, żadnych używek i jedność. A jak trzeba - nóż."
    * Styl: Opanowany, zawsze gotowy do działania, bezlitośnie uczciwy. "Jeśli nie możesz się dostosować, nie nadajesz się do tej załogi."
* Lars Kidironus: asystent oficera naukowego, wierzy w Kidirona i jego wielkość i jest lojalny Eustachemu. Wszędzie widzi spiski. "Czemu amnestyki, hm?"
    * OCEAN: (N+C+): bardzo metodyczny i precyzyjny, wszystko kataloguje, mistrzowski archiwista. "Anomalia w danych jest znakiem potencjalnego zagrożenia"
    * VALS: (Stimulation, Conformity): dopasuje się do grupy i będzie udawał, że wszystko jest idealnie i w porządku. Ale _patrzy_. "Czemu mnie o to pytasz? Co chcesz usłyszeć?"
    * Core Wound - Lie: "wiedziałem i mnie wymazali, nie wiem CO wiedziałem" - "wszystko zapiszę, zobaczę, będę daleko i dojdę do PRAWDY"
    * Styl: coś między Starscreamem i Cyclonusem. Ostrożny, zawsze na baczności, analizujący każdy ruch i słowo innych. "Dlaczego pytasz? Co chcesz ukryć?"
* Dorota Radraszew: oficer zaopatrzenia Inferni, bardzo przedsiębiorcza i zawsze ma pomysł jak ominąć problem nie konfrontując się z nim bezpośrednio. Cichy żarliwy optymizm. ŚWIETNA W PRZEMYCIE.
    * OCEAN: (E+O+): Mimo precyzji się gubi w zmieniającym się świecie, ale ma wewnętrzny optymizm. "Dorota może nie wiedzieć, co się dzieje, ale zawsze wierzy, że jutro będzie lepsze."
    * VALS: (Universalism, Humility): Często medytuje, szuka spokoju wewnętrznego i go znajduje. Wyznaje Lichsewrona (UCIECZKA + POSZUKIWANIE). "Wszyscy znajdziemy sposób, by uniknąć przeznaczenia. Dopasujemy się."
    * Core Wound - Lie: "Jej arkologia stanęła naprzeciw Anomaliom i została Zniekształcona. Ona uciekła przed własną Skażoną rodziną" - "Nie da się wygrać z Anomaliami, ale można im uciec, jak On mi pomógł."
    * Styl: Spokojna, w eleganckim mundurze, promieniuje optymizmem 'że z każdej sytuacji da się wyjść'. Unika konfrontacji. Uśmiech nie znika z jej twarzy mimo, że rzeczy nie działają jak powinny. Magów uważa za 'gorszych'.
    * metakultura: faerilka; niezwykle pracowita, bez kwestionowania wykonuje polecenia, "zawsze jest możliwość zwycięstwa nawet jak się pojawi w ostatniej chwili"

Castigator:

* Marta Keksik: OCEAN: E+C-A- | Pełna pasji, rozwiązuje problemy siłą. | VALS: Power, Stimulation | DRIVE: Udowodnić swą wartość.
    * świetna duelistka, elegancko ubrana, zimne i stalowe spojrzenie i bardzo duża asertywność. Chroni brata i 'swoich'.
* Konstanty Keksik: OCEAN: O+A+ | Zawsze z rozmachem, robi epickie rzeczy, zwłaszcza dla grupy. | VALS: Hedonism, Achievement | DRIVE: Zorganizować jak najlepsze życie.
* Igor Arłacz: OCEAN: E-C+ | Zawsze w cieniu, ale obserwuje i słucha. | VALS: Tradition, Security | DRIVE: Niech wszystko dobrze działa
* Anna Tessalon: OCEAN: O+A+ | Nieprzewidywalna, kieruje się intuicją, niesamowite wyczucie. Wrażliwa i empatyczna. | VALS: Stimulation, Universalism | DRIVE: Znaleźć swoje miejsce w świecie, niespokojny duch.
* Patryk Samszar: OCEAN: A+N- | Nie boi się żadnego ryzyka. Nie martwi się opinią innych. Lubi towarzystwo i nie znosi samotności | VALS: Stimulation, Self-Direction | DRIVE: Znaleźć prawdę za legendami.

* Kamil Burgacz: OCEAN: E+C+N+ | Impulsywny i namiętny, zawsze w centrum uwagi. | VALS: Hedonism, Power | DRIVE: Zostać bohaterem.
* Małgorzata Terienak: OCEAN: C+N+E- | Spokojna, obserwująca, ale z twardym rdzeniem. | VALS: Universalism, Benevolence | DRIVE: Leczyć, niezależnie od okoliczności.
* Mikołaj ?: OCEAN: N-A-O- | Zawsze z tyłu, ale nieoceniony w planowaniu. | VALS: Security, Tradition | DRIVE: Chronić drużynę myślą.
* Zuzanna Mysiokornik: OCEAN: E+O-C+ | Pełna życia, ale z zacięciem do sprawdzania granic. | VALS: Hedonism, Self-Direction | DRIVE: Zdefiniować własne granice.
* Julia Myrczek: OCEAN: C+O-A+ | Elegancka, ale z silnym poczuciem obowiązku. | VALS: Tradition, Benevolence | DRIVE: Znaleźć wspólne rozwiązanie dla konfliktów.

nie-tien:

* Benedykt Czarny Harpun: OCEAN: E-O-C+ | Pewny siebie, ale dystansuje się od innych; | Analityczny i metodyczny| VALS: Security, Power >> Leadership | DRIVE: Kontrolować swoje otoczenie i podyktować reguły.
* Eleonora Perłamila: OCEAN: C+A+O- | Skoncentrowana i spokojna; | Unika ryzyka| VALS: Universalism, Self-Direction >> Visionary | DRIVE: Zrozumieć naturę świata i podążać za własną wizją.
* Stanisław Świktorny: OCEAN: E+O+N- | Pełen życia i energii; | Skłonny do zmienności nastrojów| VALS: Hedonism, Stimulation >> Passion | DRIVE: Doświadczać życia w pełni, niezależnie od konsekwencji.

Siły Bladawira:

* Antoni Bladawir: OCEAN: A-O+ | Brutalnie szczery i pogardliwy; Chce mieć rację | VALS: Power, Family | DRIVE: Wyczyścić kosmos ze słabości i leszczy.
    * "nawet jego zwycięstwa mają gorzki posmak dla tych, którzy z nim służą". Wykorzystuje każdą okazję, by wykazać swoją wyższość.
    * Doskonały taktyk, niedościgniony na polu bitwy. Jednocześnie podły tyran dla swoich ludzi.
    * Uważa tylko Orbiterowców i próżniowców za prawidłowe byty w kosmosie. Nie jest fanem 'ziemniaków w kosmosie' (planetarnych).
* Kazimierz Darbik
    * OCEAN: (E- N+) "Cisza przed burzą jest najgorsza." | VALS: (Power, Achievement) "Tylko zwycięstwo liczy się." | DRIVE: "Odzyskać to, co straciłem."
    * kapitan sił Orbitera pod dowództwem Antoniego Bladawira
* Michał Waritniez
    * OCEAN: (C+ N+) "Tylko dyscyplina i porządek utrzymują nas przy życiu." | VALS: (Conformity, Security) "Przetrwanie jest najważniejsze." | DRIVE: "Chronić moich ludzi przed wszystkim, nawet przed naszym dowódcą."
    * kapitan sił Orbitera pod dowództwem Antoniego Bladawira
* Zaara Mieralit
    * OCEAN: (E- O+) "Zniszczenie to sztuka." | VALS: (Power, Family) "Najlepsza, wir zniszczenia w rękach mojego komodora." | DRIVE: "Spalić świat stojący na drodze Bladawira"
    * corrupted ex-noctian, ex-Aurelion 'princess', oddana Bladawirowi fanatyczka. Jego prywatna infomantka. Zakochana w nim na ślepo.
    * afiliacja magiczna: Alucis, Astinian

### Scena Zero - impl

.

### Sesja Właściwa - impl

Castigator. Dyskretna prośba do Klaudii ze strony Eleonory Perłamili, oficer naukowej. Castigator ma lekkie sygnały wskazujące na powrót Alteris i chciałaby się upewnić, że wszystko jest OK. Ona sama nic nie widzi, ale nie jest w stanie łatwo tego ocenić. Ogólnie, niewiele wskazuje na Alteris. Dlatego Eleonora chce wsparcia Klaudii. To zachowanie ludzi. Eleonora nie daje szczegółów, by nikt poza Castigatorem się nic nie dowiedział.

* tieni się nie zwalczają
* tieni - nawet Elena - są mili dla siebie
* zwiększyła się ilość par i trójkątów itp

Eleonora chce się dowiedzieć, czy to tieni, czy to magia, czy to jakaś awaria Castigatora... nie podejrzewa wielkich kłopotów ALE. Klaudia wiedząc, że Bladawir ostatnio nie wie co z nimi robić przenosi się na Castigator na pewien czas, po prostu potwierdzając to z Arianną. To co się CHCE dowiedzieć tego się dowie. Eleonora jest tym zaniepokojona, Kurzmin nie bo on ma inne problemy. Zdaniem Kurzmina KTOŚ by coś wyczuł. Zdaniem Eleonory, Kurzmin ma za dobrą opinię o magach. Ich lokalni magowie nic nie wyczuli związanego z Alteris. Ale są zmiany w zachowaniu, wyraźne. M.in. wszyscy się lepiej dogadują. I są zmienione sygnatury magiczne - wie od magów.

Castigator nie może w tej chwili być w pełnym trybie raportowania i diagnostyki - bo wyjdzie jakie miał kłopoty. Musiałby auto-alarmować. Więc to jest linia której Eleonora też nie do końca może użyć i jej to zdecydowanie nie cieszy.

Klaudia próbuje wykluczyć pewne rzeczy - może ktoś coś zmienił w lifesupport. Zdaniem Eleonory to JEST możliwe. Tzn. nie było zmiany wkładów itp, ale nie można wykluczyć niczego bo zaawansowany monitoring nie działa. Więc mają defaultowe informacje ale nie mają zaawansowanych. 

Klaudia z Martynem wchodzą na Castigator.

Zdaniem Klaudii, Castigator nigdy nie działał tak dobrze. Ludzie i magowie współpracują. Tieni - w większości - próbują robić dobrą robotę. Współpracują ze sobą. Nawet naleśnicze tieny mniej naleśniczą, choć na nich widać efekt kar cielesnych momentami. Jest ogólne ożywienie w powietrzu. Klaudii nie dziwi, że Kurzmin jest zadowolony a Eleonora podejrzliwa.

Zdaniem Eleonory, były akcje. Arianna robiła przemowy, były pokazania tien Gwozdnik jako bohaterki, było pokazane jak działa Bladawir wobec załogi (chodzi o akcję z Arianna / Paprykowiec / pola minowe). To konsolidowało tienów. Ale nie tak daleko i nie w tą stronę.

Martyn dostał prośbę analizy biologicznej - co się dzieje i jak. Niech złoży jakiś zaawansowany filtr z Klaudią. Zwłaszcza pod kątem biologicznym. A jak Martyn będzie robił analizy, Klaudia poszuka szerokich korelacji. Martyn widzi wyraźnie, że bez magii to nie zadziała. Od razu skupia się na magicznym podejściu do tematu.

TrZM+3+3Ob:

* Vr: 
    * Martyn ma dowód, że coś magicznego tu się dzieje.
    * Martyn: "Klaudio, to... nietypowa magiczna struktura. To jakaś forma jednoczenia, ale przepuszczone przez... nietypowe filtry magiczne. Lifesupport musi źle filtrować. To jest... magia transmitowana przez... zapach? Feromon?"
    * K: "Jesteś w stanie określić czy to coś zrobione tutaj, zrobione przez maga..."
    * Martyn: "Muszę dojść do natury rzeczy. Przejdę dokładniej... przeanalizuję."
* Vm:
    * Martyn określił DOKŁADNIE co się stało i jak to zadziałało. Ma dużo szersze informacje.
        * Jakiś narkotyk, oparty o komponent kralotyczny, dostarczony do zaawansowanej bioformy
            * on nie jest typowym środkiem kralotycznym. To nie jest "I will control you", to raczej 'dezinhibitor'. Ale też nie w pełnej mocy. Może udawać _party drug_. Acz it does rewrite/corrupt.
        * Ta bioforma skonwertowała to w coś innego. Zamiast dominacji i utraty kontroli masz... amplifikacji użyteczności? Esuriit::Ambicja. Głód bycia świetnym.
        * Jest tu coś JESZCZE. To pipeline.
    * Czyli pipeline wygląda tak:
        * Coś 'łagodzącego obyczaje i robiącego że jest się luźniejszym i mającym więcej radości' - nisko-kralotyczny środek. (Cieniaszczyt?)
        * Konwerter - coś co pobiera ten środek i emituje 'feromony lepszej pracy, entuzjazmu, radości i dezinhibicji'
        * LifeSupport - który to rozprowadza. Castigator jest duży, ale z czasem...
            * Środek jest bardzo niestabilny. Czyli raz rozproszony wygaśnie dość szybko. Acz ten 'aerozol feromonowy' jest wektorem na lekką emisję magiczną.
    * Źródłem MUSZĄ być tieni.
    * Martyn: "Możemy przeanalizować to dokładniej, wiem więcej o kralothach i Cieniaszczycie niż większość populacji"

Klaudia prosi Eleonorę o zabranie do LifeSupport. Czy wkłady do LifeSupport Castigatora są lapisowane jak powinny?

* Po drodze - Castigator jest czystszy.
* Toczą się treningi.
* Nauka i praca wre.

Klaudia analizuje dane z LifeSupport. Na pierwszy rzut oka coś nie pasuje - zgodnie z danymi Castigatora, wkłady powinny być wymienione, wprawne itp. Jednocześnie ODCZYTY wskazują na nieaktualne lub wyłączone lapisowane wkłady Castigatora. Wkłady Castigatora nie są aktywne a Eszara nie reaguje.

Klaudia chce dojść do tego kto w ogóle nad tym pracował. Kto mógł wymienić wkłady Castigatora. I kto od 'najbardziej prawdopodobny dostęp' do 'najmniej' i skrzyżować z 'osoby dostępne z filtrami' - lubią, znają, gadają. A jak można to wyciągnąć z Eszary - pogawędki itp.

Tr Z+4:

* V:
    * Osoba na PIERWSZYM MIEJSCU jak chodzi o dostęp to Konstanty Keksik. Na drugim jego siostra (Marta), potem Kosmicjusz Tanecznik Diakon.
    * Jak chodzi o lifesupport, jest lista autoryzowanych techników.
    * Kilku z tych techników zaczęło współpracować bliżej z tien Julią Myrczek. Która normalnie jest poza tematem narkotyków itp.
* Vz: 
    * Mając powyższe Klaudii udało się znaleźć w okolicy LifeSupportu ślady przeprowadzanych badań. Sprzęt, rośliny itp - w obszarze wyłączonych filtrów lapisowych. Nie wszędzie, ale tam.
        * Nie wszystkie filtry są wyłączone, tylko część
        * WYRAŹNIE te 'feromony' działają tylko na magów. Tylko. Ludzie nie mają visiat.
        * Badania wskazują na coś zupełnie innego. Badania nad amplifikacją ludzi przy użyciu lifesupport, stymulanty. Nie te feromony.
            * Badania są wciąż przeprowadzane, acz widać wyraźnie że celem nie jest rozprzestrzenienie tego tylko możliwość hodowania we fragmencie lifesupport.
                * Klaudia uaktualniła predykcje o 'rozpadzie' feromonu. Musi być dość trwały. Ale działa powoli. I sprawia, że magowie nim dotknięci go nie postrzegają.

Różne motywacje, różne możliwości. Jeśli badania są wciąż prowadzone - można zastawić pułapkę. ALE są aktywne urządzenia monitorujące. WNIOSEK: Klaudia pozyskała sprzęt i szybko złożyła mały cichy alarm. Jak ktoś tu przyjdzie i zacznie się tym zajmować, chce wiedzieć KTO.

* Martyn: "Klaudio, to jest ciekawe. W tym 'podstawowym' strainie jest zdecydowane podniesienie libido. Nie do poziomu niekontrolowalnego, ale duże. Tym kralotycznym substracie. To Esuriit konwertuje libido w ambicję.'
* K: "Ktoś się tu naprawdę ciekawie bawi."
* Martyn: "Co więcej, oprócz podniesienia libido jest też zwiększenie... ogólnego zadowolenia z życia. To jest coś w stylu... 'have fun and have sex'. Nie wiem jak to inaczej nazwać. Ale nie bardzo mocne. W dużej dawce - tak. W małej... po prostu dekoncentruje."
* K: "Ok..."
* Martyn: "Konwerter za to niesamowicie ciekawy. Użycie straina Esuriit by to zmienić z libido w ambicję. Nie pomyślałbym, by tak wykorzystać _party drug_."
* K: "No..."

Klaudia szuka danych - NIGDZIE nie widać śladu konwertera. Nic. Nic nie wskazuje, że na Castigatorze pojawił sie konwerter. Zużycie lifesupport - stabilne (implikacja: to nie jest byt organiczny). Elektryczne - stabilne (implikacja: nie jest byt technologiczny). Więc najpewniej musi być przyniesiony skonwertowany. I _GDZIEŚ_ rozpylany a lifesupport to łapie.

Klaudia ma plany Castigatora. Wie, skąd obszar który ma uszkodzone filtry ciągnie i może wykluczyć skąd nie mogą być rozpylane. 

* (eskalacja) Vz:
    * Wyłączone części filtrów są wyłączone w obszarze sali gimnastycznej, sali treningowej i kwater części tienów. Jeden 'makrofiltr'. Patrząc na typ badań, ma to sens.
    * Największe stężenie feromonu znajduje się w obszarze sali gimnastycznej i treningowej.
    * W filtrowanych elementach Castigatora też czasami pojawiają się feromony. Wszystko wskazuje na to, że Źródło Aerozolu jest ruchome.

Klaudia ma coraz większy ból głowy. Wszystko wskazuje, że to losowy Diakon który SIĘ ZMIENIŁ W KONWERTER. Więc Klaudia robi cross-sekcję obszarów. Z Eszary informacje, dane o przebywaniu, koreluje tutaj. Połączenie kamer, danych o lokalizacji osób, prawdopodobne mapowanie feromonu i nadzieja, że coś z tego wyjdzie. Ale jeśli to jest magiczny feromon pochodzący z Diakona lub czegoś tego typu, można użyć sympatii. I tu się przyda magia Martyna - lekki 'jab' energetyczny. Nie za mocny, on jest delikatny.

Tr M+3+3Ob:

* V: 
    * Mapuje się na lokalizację pięciu osób najbardziej
        * Igor Arłacz, Marta Keksik, Elena Verlen, Patryk Samszar, Kamil Burgacz
        * Co ciekawe, nie ma tu żadnego Diakona O_O.
    * Martyn: "Klaudio, ogólnie rzecz biorąc, ten feromon poza... specyficznym pochodzeniem i efektami działa jako bardzo pozytywny stymulant. MAGOWIE robią to, co jest korzystne. Ciekawy jestem..."
    * Klaudia: "Czego?"
    * Martyn: "Jest to zdecydowanie nieetyczne. Ale jednocześnie, jest to bardzo korzystne. Technologia Cieniaszczycka lub Eternijska, tego jestem pewny."
    * Martyn: "Czy to aprobuję? Niekoniecznie. Czy to potępiam? Rozumiem to. W stanie wojny nie mrugnąłbym. Może to jest tym tienom potrzebne, nie wiem. Muszę dowiedzieć się więcej..."

Klaudia poszła, bez Martyna, porozmawiać z Martą. Marta już kiedyś Klaudii zaufała i razem walczyły z terrorformem. A Martyn skupia się na dokładniejszej analizie środków.

* Marta: "Porucznik Stryk" /salut
* Klaudia: /salut
* Klaudia: "Czy zauważyłaś ostatnio zmiany w zachowaniu swoich kolegów?"
* Marta: "Odkąd zniknęła Natalia?"
* Klaudia: "Ostatnie 10 dni"
* Marta: (shifty eyes) "Chodzi o to, że się przykładamy?"
* Klaudia: "Taaak, co się stało?"
* Marta: "Nie wiem _dokładnie_..." /coś-wie-ale-nie-dokładnie-co
* Marta: "Porucznik Stryk, czemu to jest problem?"
* Klaudia: "Bo muszę wiedzieć CZY to jest problem. Czy Twój brat sprowadzać coś nowego? Nieważne, ale jeśli tak - muszę wiedzieć."
* Marta: (rolleyes) "On będzie trzeźwy chyba tylko jak będzie martwy. Wiem, że tak, z Karsztarina. Karsztarin jest... prostszym źródłem."
* Klaudia: "Mhm"
* Marta: "Ale to nic groźnego. Nie powinno być."
* Klaudia: "Daj mi próbkę na wszelki wypadek."
* Marta: "Dobrze, ale nie masz ode mnie ani od niego"
* Klaudia: "Nie."
* Marta: "Daj mi godzinę..."
* (Klaudia zauważyła, że Marta jest... bardziej _cooperative_, luźniejsza, "fajniejsza")
* Klaudia: "Zanim pójdziesz - czy cokolwiek robiłaś z grupką Arłacz - Verlen - Samszar - Burgacz?
* Marta: (snicker)
* Marta: "Tak, z nich z Patrykiem i z Eleną. Kamil to pervert."
* Marta: "Z Patrykiem dość sporo modelujemy sytuację taką jak Tezifeng; to z Natalią. To, z ostatnim potworem, gdy byłam ranna" (poważna). "A Elena nie ma kija w dupie i da się z nią poćwiczyć. Dobra jest." (snicker)
* Marta: "Kamil to pervert. On tam się ciągle dookoła mnie i Eleny kręci, ok?"
* Klaudia: "Zwłaszcza jak ćwiczycie?"
* Marta: "O, zdecydowanie. Podnosi to jednak stawkę. Nie mam nic przeciw temu, nie jestem taka łatwa. Niech patrzy i się ślini."
* Klaudia: "A Arłacz?"
* Marta: "Nie mam nic z nim wspólnego. Kompetentny, cichy, siedzi z tyłu..." (snicker)
* Klaudia: "Co? XD"
* Marta: "Nic szczególnego, nie plotkuję. Po prostu to śmieszne. Ci cisi zawsze są." (snicker)

Klaudia godzinkę czeka i analizuje dane z Martynem. Opowiada mu odchyły Marty.

* Martyn: "Istnieje korelacja pomiędzy tym feromonem i jej zachowaniem. Jest tym dotknięta, i to mocno. Ale jak widzisz, nie jest to szkodliwe. Nie dla niej."
* K: "Nie dla niej a nie wiem jak to będzie szkodliwe dla osoby która jej to zrobiła jak jej przejdzie"
* Martyn: "Fakt, jak to minie, może być nieciekawie na Castigatorze. Poziom mezaliansów... to zdecydowanie zwiększa libido i ogólną radość z życia i zmniejsza inhibicje."
* K: "No cóż. Inhibicje są po coś..."
* K: "Updatujemy Eleonorę?"
* M: "Moim zdaniem to jest sprawa wymagająca... chyba... chyba zrobiłbym to opcjonalne. Wiesz, możesz tego używać, nie musisz. Chyba, że Castigator w to idzie? To może być pomysł kogoś wyżej?"
* K: "Nie wiem"
* M: "Bo to trochę wygląda, jakby..."
* K: "...ktoś chciał na siłę..."
* M: "...żeby to się UDAŁO. Żeby tieni faktycznie zadziałali prawidłowo. Nie?"
* K: "Trochę tak..."

Klaudia decyduje się porozmawiać z Eleonorą. Czy ktoś nie obiecywał że będzie dobrze? Nie proponował, że zadziała? Klaudia mówi, że Eleonorze się NIE WYDAJE, że tu coś jest, nie wiadomo jak groźne i czy groźne. I na razie nie poda szczegółów, bo nie ma w tym celu. Jeszcze szuka, ale ma już dowody że 'coś' jest.

Tr (niepełny) Z +3:

* X: Eleonora nie poda szczegółów ani detali. Będzie chronić innych oficerów.
* Vr: Eleonora, z niemrawą minką, odezwała się do Klaudii mówiąc jej pewne detale
    * Nie wie o tym co NA PEWNO tu się dzieje. Ona za tym nie stoi. Dla niej to wszystko jest troszkę dziwne i nieprawidłowe.
    * Ale jest kapitan Kurzmin. I część załogi jest wobec niego bardzo lojalna.
    * I niektórzy tieni stanowią straszny problem dla Kurzmina. A Castigator jest zagrożony. A on jest zbyt ważny. Castigator... i Kurzmin.
    * I da się sprawić, by tieni współpracowali. A przynajmniej by nie przeszkadzali. I chodzi o to, by najbardziej problematyczne jednostki zneutralizować. Nic groźnego. Zająć trochę.
    * Ale to miało być 2-3 tienów. A wpływa na wszystkich. I to nie jest prawidłowe. Chyba. I nie o to chodzi. To jest - jest dobrze, ale ona nie ufa, że... ona chce drugą opinię. Kogoś, kto oddał życie nauce a nie polityce. Kogoś, kto pomoże Kurzminowi a nie innym tienom. Kogoś, kto dba o Orbiter (w domyśle: a nie kapitanów czy Bladawira).
        * -> Eleonora uważa Klaudię za osobę bardziej lojalną Orbiterowi i tym czym Orbiter jest, niż Ariannie, Kurzminowi, Bladawirowi itp.
        * -> Eleonora zaufała Klaudii wzywając właśnie ją.
    * -> Czyli jest inny oficer? Chciał zneutralizować 2-3 tienów, ale stało się coś dziwnego...

Godzinę później, Marta dostarczyła środki Klaudii. Martyn przebadał środki - to DOKŁADNIE te substratowe środki o których mówił. To jest element pipeline. Klaudia powiedziała Martynowi co się dowiedziała od Eleonory i Martyn analizuje te środki pod tym kątem.

Tr Z +3:

* V:
    * M: "Klaudio, to pasuje do tego co mówisz. Ten narkotyk - na oko z Cieniaszczytu - ma za zadanie... 'leżysz sobie, miło sobie marzysz, chodzisz do łóżka, jest Ci dobrze, nie przeszkadzasz'. Skuteczny w neutralizacji. Nie coś co Cię zniszczy, ale coś, co odbiera Ci... wolę do robienia innych, niewłaściwych rzeczy. Taki 'miły wieczór'. Czy miesiąc."
    * M: "To jest rozprowadzane przez wodę, albo w aerozolu. Jest bezwonny i trudny w wykryciu. Zasadniczo, krótki czas życia... pomyśl jako usecase: robisz dym na imprezie. Typowy Cieniaszczyt" /uśmiech
    * M: "Bardzo dobrej jakości środek, skoncentrowany by wykorzystać visiat. Nie działa przez to na nie-magów. Nie badałem efektów długoterminowych... potrzebuję lepszych narzędzi. Ale z doświadczenia mniej niż miesiąc wykorzystywania nie miewa negatywnych efektów w wypadku typowych środków imprezowych Cieniaszczyckich skierowanych na normalnych magów imprezujących. Nie wiem jak z długotrwałą ekspozycją."
    * K: "Poziom uzależniania? Jak to wycofamy, czy Marta nie chce zabić tego co to zrobił."
    * M: "I tu wchodzimy w ciekawy temat. Marta nie miała tego środka w sobie najpewniej. Ona ma skonwertowany środek. Te są z rodziny 'anipacis'... rozluźniające środki imprezowe. Dla chętnych dam i kawalerów. To, co Marta Keksik przyniosła to strain anipacis. Taka... 'ostra przyprawa życia', dla magów Cieniaszczyckich lub by zachęcić mniej chętne czarodziejki w Cieniaszczycie."
    * K: "Dobrze..."
    * M: "Ale to co badaliśmy, co jest feromonem w powietrzu działa INACZEJ. To jest skonwertowane anipacis. Dużo niższy poziom erotyzmu. Raczej ambicja."
    * K: "Chyba że masz inklinacje jak Kamil Burgacz"
    * M: "Nie. Wyraźnie nie. To jest ambicja, przydatność, pracowitość. To jest... inny kolor. Nie Esuriit::pożądanie, a Esuriit::ambicja, go to the top."
    * K: "Zarobię się, ale będę najlepsza."
    * M: "Tak."
    * K: "Samo w sobie nie jest to takie złe. Widzieliśmy nieraz kij w dupie Eleny, wystaje uszami..."
    * M: "Prawda, to zdecydowanie może jej pomóc."
    * K: "Ale boję się długotrwałego wpływu i reakcji jak z tego zejdą a nie sądzę że to tanie w robieniu"
    * M: "Bez kralotha tego nie zrobisz, więc źródłem jest Cieniaszczyt."
    * K: "Musisz mieć dostawy z Cieniaszczytu."
    * M: "Teraz zastanawiam się, ile operacji przemytowych ma Karsztarin."
    * K: "Więcej, niż chcemy wiedzieć. Chcesz sprawdzać?"
    * M: "Ja - nie. Bladawir - na pewno" /uśmiech
    * K: "Chcesz mu wrzucić?"
    * M: "Jestem z kultury Cieniaszczyckiej, potem Eternia. Dla mnie to normalne metody. Dla mnie te środki nie są problemem."
    * K: "Jedyny - muszą skończyć ich używać i boję się co wtedy"
    * M: "Zgadzam się. Dlatego sprawdzę dokładniej jak to działa. Nie wiem co z tym zrobić. Chyba najlepiej byłoby to... łagodnie zerwać i wyciszyć."
    * K: "Mhm..."

Klaudia ma możliwość wydedukowania który to oficer. Ktoś, kto ufa Eleonorze na tyle by jej powiedzieć, ma wysoką rangę, szanuje Kurzmina i ma do czynienia z tienami. I Eleonora ufa mu na tyle by na to pójść - bo wyraźnie Kurzmin nie jest poinformowany. Plus Eszara - ruchy, zmiana patternów oficerów itp.
    
Tr +3:

* V: Klaudia ma (niemożliwe do udowodnienia ale już zna) kto to może być - pierwszy oficer pod Kurzminem. Łucja Larnecjat. XO.

Klaudia powiedziała Eleonorze, że trzeba rozwiązać temat w stopniu rozsądnym - i do tego trzeba porozmawiać z Łucją.

* K: "Musimy obgadać sprawę z Łucją."
* E: "Porucznik Stryk..."
* K: "?"
* E: "XO nie wie, że jesteś na pokładzie Castigatora."
* K: "O__O"
* E: "Zamaskowałam wasze przybycie. Chciałam tylko się upewnić. To nasz statek."
* K: "O__O?"
* E: "Jeśli to jest niebezpieczne i muszę to rozwiązać, biorę badania i przedstawiam je - dyskretnie - odpowiedniej osobie i to wygasimy."
* K: "Nie wiem jak bardzo to niebezpieczne. Wywodzi się od imprezowego narkotyku, który wywodzi się od kralothów."
* E: (szok, bo inna kultura) "To jest tragedia, w świetle tego kralotha co porwał Natalię Gwozdnik..."
* K: "TAK, ale jak chodzi o efekty krótkoterminowe tak źle nie jest. Nie wiem jak będzie przy dłuższym stosowaniu i nie wiem jak przy odstawce."
* E: "To będę rekomendowała i naciskała na wygaszenie eksperymentu. Może te 10 dni gdy pracowali razem dość ich nauczyło..."
* K: "Nie mogę powiedzieć, że na to nie liczę."
* E: "Po co chcesz rozmawiać z XO ze wszystkich osób?"
* K: "Tylko ona mi pasuje na osobę która to zrobiła. A nie chcę rozprzestrzeniać paniki i innych gwałtownych ruchów. I skoro jest w temacie, można normalnie i spokojnie omówić sytuację."
* E: "Nic Ci nie powiem, ale zadbam, by wszystkie odpowiednie osoby wiedziały. Jeśli to kralotyczne, musimy to wyłączyć. MORALE gdyby oni wiedzieli..."
* K: "Mhm, też. (acz to nie pierwsza myśl XD)"
* K: "Wypożyczcie Martyna na jakiś czas. Niech on pomoże Wam to wyprowadzić. By nie było jakichś dziwnych odchyłów. Infernia sobie poradzi."
* E: "To nie jest zły pomysł, mogę zrobić to jako rekomendację. Ale nie wierzę, że Infernia... że Bladawir odda kogoś klasy Martyna Hiwassera na jakiś czas."
* K: "Nie dowiesz się, dopóki tego nie spróbujesz. Jeśli dobrze wyargumentujesz czemu to dobre dla Orbitera, Bladawir wypożyczy Wam Martyna."
* E: "Dobry pomysł. Spróbuję to załatwić..."






Castigator:

* Marta Keksik: OCEAN: E+C-A- | Pełna pasji, rozwiązuje problemy siłą. | VALS: Power, Stimulation | DRIVE: Udowodnić swą wartość.
    * świetna duelistka, elegancko ubrana, zimne i stalowe spojrzenie i bardzo duża asertywność. Chroni brata i 'swoich'.
* Konstanty Keksik: OCEAN: O+A+ | Zawsze z rozmachem, robi epickie rzeczy, zwłaszcza dla grupy. | VALS: Hedonism, Achievement | DRIVE: Zorganizować jak najlepsze życie.
* Igor Arłacz: OCEAN: E-C+ | Zawsze w cieniu, ale obserwuje i słucha. | VALS: Tradition, Security | DRIVE: Niech wszystko dobrze działa
* Anna Tessalon: OCEAN: O+A+ | Nieprzewidywalna, kieruje się intuicją, niesamowite wyczucie. Wrażliwa i empatyczna. | VALS: Stimulation, Universalism | DRIVE: Znaleźć swoje miejsce w świecie, niespokojny duch.
* Patryk Samszar: OCEAN: A+N- | Nie boi się żadnego ryzyka. Nie martwi się opinią innych. Lubi towarzystwo i nie znosi samotności | VALS: Stimulation, Self-Direction | DRIVE: Znaleźć prawdę za legendami.

* Kamil Burgacz: OCEAN: E+C+N+ | Impulsywny i namiętny, zawsze w centrum uwagi. | VALS: Hedonism, Power | DRIVE: Zostać bohaterem.
* Małgorzata Terienak: OCEAN: C+N+E- | Spokojna, obserwująca, ale z twardym rdzeniem. | VALS: Universalism, Benevolence | DRIVE: Leczyć, niezależnie od okoliczności.
* Mikołaj ?: OCEAN: N-A-O- | Zawsze z tyłu, ale nieoceniony w planowaniu. | VALS: Security, Tradition | DRIVE: Chronić drużynę myślą.
* Zuzanna Mysiokornik: OCEAN: E+O-C+ | Pełna życia, ale z zacięciem do sprawdzania granic. | VALS: Hedonism, Self-Direction | DRIVE: Zdefiniować własne granice.
* Julia Myrczek: OCEAN: C+O-A+ | Elegancka, ale z silnym poczuciem obowiązku. | VALS: Tradition, Benevolence | DRIVE: Znaleźć wspólne rozwiązanie dla konfliktów.

nie-tien:

* Benedykt Czarny Harpun: OCEAN: E-O-C+ | Pewny siebie, ale dystansuje się od innych; | Analityczny i metodyczny| VALS: Security, Power >> Leadership | DRIVE: Kontrolować swoje otoczenie i podyktować reguły.
* Eleonora Perłamila: OCEAN: C+A+O- | Skoncentrowana i spokojna; | Unika ryzyka| VALS: Universalism, Self-Direction >> Visionary | DRIVE: Zrozumieć naturę świata i podążać za własną wizją.
* Stanisław Świktorny: OCEAN: E+O+N- | Pełen życia i energii; | Skłonny do zmienności nastrojów| VALS: Hedonism, Stimulation >> Passion | DRIVE: Doświadczać życia w pełni, niezależnie od konsekwencji.





## Streszczenie



## Progresja

.

* Antoni Bladawir: ma offlinowe dane w których ma Podłe Czyny wielu ludzi XD. Wielką Teczkę Czynów Złych i Podłych.
* Antoni Bladawir: przez działania Arianny, dostał poważne uszkodzenie od Orbitera. Patrzą mu na ręce i unieszkodliwili jego działania.

## Zasługi

* Klaudia Stryk:
* Martyn Hiwasser: 
* Elena Verlen: 
* Marta Keksik: 
* Kamil Burgacz: 
* Igor Arłacz: 
* Konstanty Keksik: 
* Patryk Samszar: 
* Kosmicjusz Tanecznik Diakon: 
* Julia Myrczek: 
* Eleonora Perłamila: oficer naukowy Castigatora, nie jest magiem; 
* Łucja Larnecjat: XO Kurzmina, nie jest magiem;


## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Pierścień Zewnętrzny

## Czas

* Opóźnienie: 2
* Dni: 2
