## Metadane

* title: "Pasożytnicze osy w Nativis"
* threads: historia-eustachego, arkologia-nativis, plagi-neikatis
* gm: żółw
* players: wikimet, anadia

## Kontynuacja
### Kampanijna

.

* [220712 - Pasożytnicze osy w Nativis](220712-pasozytnicze-osy-w-nativis)

### Chronologiczna

* [220712 - Pasożytnicze osy w Nativis](220712-pasozytnicze-osy-w-nativis)

## Plan sesji
### Co się wydarzyło

* .

### Co się stanie

* Cel Marcela: 
    * zdobywać pieniądze, dobrze handlować
    * ukryć swoje działania
* Cel :
    * 

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

.

## Streszczenie



## Progresja

.

### Frakcji

* .

## Zasługi

* Celina Lertys: 
* Jan Lertys: 
* Marcel Kidiron: 
* Laurencjusz Kidiron: 
* Tymon Korkoran: zapatrzony w Kidironów; 

* Serentina d'Remora: dedykowana inteligentna TAI połączona z szybkim ścigaczem klasy Remora; wyprowadziła miragenta Lycoris z kłopotów i opracowała plan jak uratować ludzi i jaki jest cel Pasożyta.
* Lycoris Kidiron: naukowiec i ekspert od bioinżynierii i terraformacji; oryginał spowodował Plagę pod wpływem Pasożyta. Miragent zrozumiał cel Pasożyta - rozprzestrzenić Pasożyta do innych arkologii - i współpracując z Rafałem uniemożliwił tą sytuację.
* Rafał Kidiron: bezwzględny szef ochrony Nativis należący do kasty rządzącej i kuzyn Lycoris; dobrze zarządza Nativis słuchając rad i zostawiając ekspertom działania. Opanował Pasożyta z pomocą miragenta Lycoris i pozyskał próbki Pasożyta jako broń biologiczną. 
* Jarlow Gurdacz: wybitny architekt i ekspert od piękna Nativis; ściągnął drzewa które okazały się być Pasożytem i umieszczając je by przetrwały niedaleko lifesupport zaczął Plagę.
* Damian Marlinczak: pomniejszy medyk; uczestniczył w reanimacji miragenta Lycoris. Stracił dwóch przyjaciół, dowiedział się o Pasożycie i powiedział Rafałowi o miragencie. Nikt ważny, acz zna sekret Nativis i nie chce "zniknąć".

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Nativis: klejnot Neikatis, jednocześnie chłodna i wymagająca perfekcji. Nie ma wojny domowej ale eksportuje żywność i bogaci się innymi arkologiami.
                        1. Stara Arkologia Wschodnia: opanowana przez Pasożyta Neikatis; sektory starsze i bardziej siermiężne niż reszta arkologii
                            1. Stare TechBunkry: w jednym z nich przebudzono miragenta Lycoris by poradzić sobie z infekcją Pasożyta
                            1. Ogrody Gurdacza: Gurdacz zamontował tam Pasożyta w formie drzew i niechcący doprowadził do Plagi.
                        1. Farmy Wschodnie: zainfekowane przez Pasożyta, nie doszło do eksportu. Dokładnie przeczyszczone przez miragenta Lycoris.

## Czas

* Opóźnienie: -44
* Dni: ?

## Konflikty

* 1 - 
    * 
    * 
* 2 - 
    * 
    * 
* 3 - 
    * 
    * 
* 4 - 
    * 
    * 
* 5 - 
    * 
    * 
* 6 - 
    * 
    * 
* 7 - 
    * 
    * 
* 8 - 
    * 
    * 
* 9 - 
    * 
    * 
* 10 -
    * 
    * 
* 11 -
    * 
    * 

## Kto

* Lycoris Kidiron, 57 lat (ze wspomaganiami drakolickimi dającymi jej efektywność 3x-latki) (objęta przez Wiki)
    * Wartości
        * TAK: Bezpieczeństwo, Osiągnięcia ("dzięki mnie ta arkologia będzie bezpieczna i osiągniemy najwyższy poziom eksportu żywności")
        * NIE: Konformizm ("nikt, kto próbował się dopasować do innych nie doprowadził do postępu")
    * Ocean
        * ENCAO: +-0+0 (optymistyczna, nieustraszona, lubi się dogadywać z innymi, nie patrzy na ryzyko)
    * Silniki:
        * TAK: Kapitan Ahab: polowanie na białego wieloryba do końca świata i jeszcze dalej, nieważne co przy tym stracimy. (tu: "zrobię najlepszą arkologię, lepszą dla ludzi niż planeta macierzysta")
    * Rola:
        * ekspert od bioinżynierii i terraformacji, pionier (zdolna do przeżycia w trudnym terenie)

* Rafał Kidiron
    * Wartości
        * TAK: Achievement, Power("wraz z sukcesami przychodzi władza, wraz z władzą musisz osiągać więcej sukcesów")
    * Ocean
        * ENCAO: 00+-0 (precyzyjny i dobrze wszystko ma zaplanowane, lubi się kłócić i nie zgadzać z innymi)
    * Silniki:
        * TAK: Duma i monument: to NASZE sukcesy są najlepsze i to co MY osiągnęliśmy jest najlepsze. Musimy wspierać NASZE dzieła i propagować ich chwałę.
    * Rola:
        * kuzyn Lycoris, security lord, w wiecznym konflikcie z Gurdaczem (security & money - beauty)
* Jarlow Gurdacz
    * Wartości
        * TAK: Hedonism, Face NIE: Conformism ("najpiękniejsza arkologia, klejnot Neikatis - i to nasza!")
    * Ocean
        * ENCAO: 000++ (kocha piękno i ma mnóstwo świetnych pomysłów)
    * Silniki:
        * TAK: Artystyczna dusza: Stworzyć piękno, pokazać je całemu światu lub wręcz przeciwnie, zachować dla godnej publiczności. Być docenionym.
    * Rola:
        * ekspert od roślinności, architektury i zdrowia arkologii, w wiecznym konflikcie z Rafałem Kidironem (security & money - beauty)

* 
    * Wartości
        * TAK: 
        * NIE: 
    * Ocean
        * ENCAO: 
    * Silniki:
        * TAK: 
    * Rola:
        * 

