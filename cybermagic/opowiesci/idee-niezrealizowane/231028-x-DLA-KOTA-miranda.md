## Metadane

* title: ""
* threads: planetoidy-kazimierza, sekret-mirandy-ceres
* motives: beznadziejny-szef, dont-mess-with-my-people, hostile-takeover, sabotaz-niespodziewany, koniecznosc-pelnej-dyskrecji, oszczedzanie-na-wszystkim
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [231027 - Planetoida bogatsza niż być powinna](231027-planetoida-bogatsza-niz-byc-powinna)

### Chronologiczna

* [231027 - Planetoida bogatsza niż być powinna](231027-planetoida-bogatsza-niz-byc-powinna)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * 
* Opowieść o (Theme and vision): 
    * Blakvelowcy mają Królową, ale w sumie może uda się tą jednostkę przekazać komuś innemu i oszukać 'tępą Ceres'
        * Próbują więc znaleźć warunki w których Królowa działa pod kimś innym.
        * Próbują wykonać misję górniczą pod dowództwem skeleton crew. A Królowa próbuje ich zniechęcić i pokazać wartość.
    * Królowa musi pokazać wartość, ale jednocześnie nie nadawać się pod kontrolę Blakvelowców.
    * SUKCES
        * Blakvelowcy zostawiają Królową w spokoju - niech PŁG dowodzą tym cholerstwem i niech to działa...
        * Królowa udowadnia, że jest bardziej wartościowa niż kosztowna
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * beznadziejny-szef: nowy szef nie jest kimś wybitnym. Jest pomysłowy, ale kiepski HRowo.
    * dont-mess-with-my-people: nowo wprowadzeni Blakvelowcy, górnicy itp. nie są fanami starego zespołu. Miranda kontratakuje.
    * hostile-takeover: wprowadzenie nowego szefostwa z ramienia Blakvela, próba oszczędności.
    * sabotaz-niespodziewany: Miranda musi sabotować i niszczyć morale 'napastników'.
    * koniecznosc-pelnej-dyskrecji: znowu dyskretne działania Mirandy - nikt nie może wiedziec o jej działaniach.
    * oszczedzanie-na-wszystkim: nowe szefostwo próbuje zmniejszyć koszty działania Królowej, nie tylko znaleźć dla niej miejsce.
* Detale
    * Crisis source:
        * HEART: "Blakvelowcy nie akceptują rzeczy których nie rozumieją i nie mogą kontrolować"
        * VISIBLE: "Wszystko musi być pod bezpośrednią kontrolą Blakvelowców i tak tanie jak to możliwe"
    * Delta future
        * DARK FUTURE:
            * chain 1: Blakvelowcy wpakują Królową na kiepskie misje
            * chain 2: Blakvelowcy wprowadzą docelowo na pokład zaufane osoby
            * chain 3: Blakvelowcy uważają, że Królowa jest jednostką Mardiusowców i PŁG mają w tamtym kierunku sympatię
        * LIGHT FUTURE: "PŁG mają swobodę używania Królowej"
    * Dilemma: brak, sesja funkcjonalna

### Co się stało i co wiemy

* Przeszłość
    * .
* Teraźniejszość
    * .

### Co się stanie (what will happen)

* F0: _Pierwsza akcja górnicza_
    * INSTANCES
        * Juliusz i Łucjan się żrą, Prokop próbuje deeskalować, widać kto tu rządzi
        * Gotard 'nie potrzebujemy tyle płacić za Waszą ochronę', Kazimierz 'płacicie tanio i cieszcie się że tak tanio'
            * Znowu nie działają działka. Nie da się ich zamontować na Królowej? Coś jest jak zawsze nie tak
        * Jakaś akcja że sprzęt się popsuje i zaraz górnik będzie ranny (co zrobi Królowa?)
* F1: _Podejrzanie dobry asteroid_
    * stakes:
        * czy Królowa pozostanie w ukryciu czy będzie musiała zabić ludzi?
        * czy ktoś poza szkieletową załogą PŁG wróci bezpiecznie?
        * czy uda się pozyskać jakąś kasę? czy uda się NIE wziąć jajek NieGórnika?
    * opponent: 
        * NieGórnik. Nie jest prawidłowo postrzegany; każdy widzi go jako tego kim powinien być
            * potwór musi pożreć kilka osób, a potem chce wrócić z ekipą
        * Blakvelowcy. Wierzą w swoją percepcję, nie do końca rozumieją co się tu dzieje
    * problem:
        * wszyscy myślą, że górników było N-1, a było N; tylko Miranda postrzega prawdę
        * Miranda musi pozostać w ukryciu
    * koniec fazy:
        * zostajemy / wracamy (z potworem / bez)
    * INSTANCES
        * Mikołaj i Bartek kłócą się o plugawość Królowej ('organiczna papka do jedzenia'). Seweryn broni Królowej. Łucjan ma wątpliwości.
        * przebudzenie NieGórnika, Miranda zauważa, że jednej osoby nie ma a widzi inną
            * (tam są dane z tego co się stało z górnikiem; jajko weń weszło)
        * NieGórnik poluje na innego delikwenta i go zabija, nikt nic nie zauważa
        * kłótnie
            * Grzesiek próbuje ukraść więcej kasy dla siebie, szuka co lepszych kawałków
            * Mikołaj zaczyna dramę bez powodu, np. 'Bartek co nam Blakvelowcy skoro możemy mieć kasę'
            * Bartek idzie w stronę konieczności odbudowy i poszerzenia bazy górniczej
            * Prokop próbuje stabilizować, Łucjan unika i robi swoje, Gotard 'trzeba było zainwestować więcej, patrz tą planetoidę, nawet sensory dobrze nie czytają'
    * DARK: 
        * chain 1: Dodatkowe, ogromne ofiary śmiertelne (poza 2 osobami)
        * chain 2: wracamy z jajkiem NieGórnika
        * chain 3: Królowa jest wykryta i musi zabić bo ktoś wie
        * chain 4: wracamy z NieGórnikiem
        * chain 5: wracamy bez niczego (kasa)

## Sesja - analiza

### Fiszki

Zespół:

* Prokop Umarkon: 
    * rola: kapitan, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: extravert, agreeable (outgoing, enthusiastic, trusting, empathetic)
    * values: power + conformity
    * wants: pragnie zostać KIMŚ, wydobyć się z bagna
* Łucjan Torwold: 
    * rola: administrator / logistyka, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: neurotic, conscientous (very worried, scared of cults, very organized)
    * values: security + conformity
    * wants: przesądny; boi się duchów i kultu. Narzeka. Chce bezpiecznego, prostego życia
* Gotard Kicjusz: 
    * rola: technical guy, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: open, low-neuro, low-consc (extra stable, spontaneous, eccentric)
    * values: self-direction
    * wants: przygód, odzyskać rodzinę
* Seweryn Grzęźlik:
    * rola: psychotronik, blakvelowiec
    * personality: neuro, low-extra, consc (workaholic, obsesyjny, cichy, skłonności do wybuchania, umiłowanie porządku)
    * values: self-direction, self-enhancement
    * wants: zrozumieć sekret Mirandy, Blakvel forever, ciszy i rozwoju

Blakvelowcy i górnicy ("nowe dowodzenie"):

* Grzegorz Fabutownik: górnik w przestrzeni Blakvela
    * OCEAN: E+A- |Zawsze szuka adrenalinki i kasy;; Nie dba o innych, może kraść | VALS: Power, Hedonism | DRIVE: Kasa i wszystkie laski jego
* Mikołaj Resztkowiec: górnik w przestrzeni Blakvela
    * OCEAN: E+N- |Nie kłania się nikomu;; Ciekawski;; Oczytany, ceni wiedzę dla wiedzy| VALS: Hedonism, Achievement| DRIVE: Drama
* Bartek Burbundow: górnik w przestrzeni Blakvela
    * OCEAN: A+O+ |Mało energii;; Kreatywny, tworzy wiecznie coś nowego;; Pomocny, starający się wesprzeć innych| VALS: Face, Tradition| DRIVE: Odbudowa i odnowa
* Juliusz Cieślawok:
    * OCEAN: A-O- |Fatalne żarty;; Antagonizuje;; Ciemięży ludzi| VALS: Stimulation, Universalism | DRIVE: Niska zabawa i ciemiężenie bliźniego
* Kazimierz Zamglis:
    * OCEAN: A-N+ |Żyje w swoim świecie i ma wszystko gdzieś;; Dość agresywny;; Unika hałasu | VALS: Security, Self-Direction | DRIVE: moja korporacja jest najlepsza
* Bogdan Wagdamor:
    * OCEAN: A-C+ |Brutalny i przeważający;; Plany i checklisty;; Żąda wydajności;; Centuś | VALS: Achievement (optimization), Family (Blakvel) | DRIVE: wykorzystać WSZYSTKO (wyznawca Vigilusa)
* Salina Artamea:
    * OCEAN: C+O+ |Pracowita i cierpliwa;; Ma chore pomysły;; Cicha z natury | VALS: Tradition (gildia inżynierów), Benevolence (Kazimierz) | DRIVE: burning hate towards _non-Kazimierzan_
* .5 innych górników

#### Strony

* Górnicy w Domenie Ukojenia i mały biznes: 
    * CZEGO: zachować niezależność, handel itp. Przetrwać. Zarobić.
    * JAK: sami się zbroją
    * OFERTA: standardowa oferta terenowa, plotki itp.
    * KTOŚ: Elsa Kułak (przełożona, trzyma twardą ręką), Antoni Czuwram (górnik, solidny stateczek i niezły sprzęt - dużo wie), Kamil Kantor (szuka ojca Leona który odkrył sekret Mirnas i zaginął), Bruno Baran (mag eternijski i założyciel; Strachy to destabilizacja Blakvela przy obecności lapizytu), Kara Szamun (młódka z rodziny Szamun, pilot i nawigator transportowca na Asimear.)
* Przemytnicy Blakvela: 
    * CZEGO: zdobyć prymat w Domenie Ukojenia
    * JAK: zmiażdżyć Mardiusa, potem przejąć Talio i zestrzelić bazę Barana.
    * OFERTA: świetne rozeznanie w pasie, największa siła ognia, wiedza o SWOICH skrytkach, wsparcie piratów etnomempleksu Fareil (synthesis oathbound pro-virt, pro-numerologia)
    * KTOŚ: Ernest Blakvel (arystokrata eternijski)
* Przemytnicy Mardiusa: 
    * CZEGO: zregenerować siły, odzyskać Tamarę, ukryć się przed Blakvelem
    * JAK: w ciągłej defensywie, ufortyfikowanie Sarnin, operacje militarne anty-Blakvel
    * OFERTA: wiedza o Miragencie i Aleksandrii, świetne rozeznanie w pasie, wiedza o SWOICH skrytkach, znajomości wśród lokalnych.
    * KTOŚ: Tamara Mardius (eks-komandos noctis, Alexandria), Deneb Ira (regeneruje komandosów po sprawie z Aleksandrią)
* Taliaci: (mieszkańcy Talio)
    * CZEGO: rzeczy wysokotechnologiczne
    * JAK: handel, współpraca
    * OFERTA: mają dostęp do WSZYSTKICH frakcji i są wiecznie neutralni, reaktory termojądrowe na podstawie deuteru i trytu na Talio.
* Strachy
    * CZEGO: Brak woli. Reakcja. Zniszczyć wszystko.
    * JAK: flota inwazyjna
    * OFERTA: zniszczenie, targetowane lub nie.

### Scena Zero - impl

.

### Sesja Właściwa - impl

.

## Streszczenie

.

## Progresja

* .

## Zasługi

* Miranda Ceres: 
* SC Królowa Przygód: 
* Prokop Umarkon: 
* Łucjan Torwold: 
* Gotard Kicjusz: 
* Seweryn Grzęźlik: 


* Mikołaj Resztkowiec: lubi zaczynać dramę i antagonizować np. Kazimierza. Poza tym - wystarczająco kompetentny i nic wielkiego nie zrobił.
* Bartek Burbundow: nie jest fanem Blakvelowców, nie uważa, że powinno się tyle się im płacić za ochronę. Zwolennik czystości - bardzo obrzydzony tym, że Królową trzeba karmić organiczną papką. Jednocześnie - uczciwy górnik.
* Juliusz Cieślawok: żołnierz Blakvelowców, który chciałby jak największej ilości ludzi zabrać kasę i ich pociemiężyć. Dowodzi operacją bo nikt inny (Kazimierz) nie chce. Gdyby nie Miranda, zostałby Zmieniony przez Alteris.
* Kazimierz Zamglis: czytający erotyki żołnierz Blakvelowców, gardzący górnikami których ma chronić, ale kompetentny i nie jest sadystyczny. Łatwo się irytuje i tnie kasę górników, ale jeśli wszystko działa prawidłowo to się nie przypieprza.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Pas Teliriański
                1. Planetoidy Kazimierza
                    1. Domena Ukojenia

## Czas

* Opóźnienie: 6
* Dni: ?

## OTHER
