## Metadane

* title: "X"
* threads: rekiny-a-akademia
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [211228 - Akt, o którym Marysia nie wie](211228-akt-o-ktorym-marysia-nie-wie)

### Chronologiczna

* [211228 - Akt, o którym Marysia nie wie](211228-akt-o-ktorym-marysia-nie-wie)

## Plan sesji

### Theme & Vision

* Action & Adventure.
* What is pleasant is dangerous.
* Popularność i korozja czy to, co powinno być zrobione i wrogowie?

### Ważne postacie + agendy

* Chevaleresse: agentka, która próbuje ekstraktować Melissę z Dzielnicy Rekinów dla Alana. Chce odzyskać Melissę.
* Durszenko, Melissa: sympatyczna dziewczyna, która jest głęboko posunięta w Kulcie Ośmiornicy. Chce przekonać wszystkich do nowej, lepszej kultury.
* Mysiokornik, Santino: chce promować kulturę Mielissy. Nie wie co to jest i co ona oznacza. Ale się świetnie sprzedaje i jest tam seks.

### Co się wydarzyło

* Przez brak Administratorki systemy obronne były długotrwale wyłączane / hackowane. Korzystali z tego zarówno Chevaleresse jak i Santino.
    * Santino sprowadzał sobie młode dziewczęta; zrobił z tego "bezpieczną bazę" i był otoczony podziwem. Niewielkim kosztem ;-).
        * M.in. znalazł i ściągnął poszukiwaną przez prawo Melissę Durszenko z Kultu Ośmiornicy i eks-przyjaciółkę Chevaleresse 
    * Nieświadomy tego wszystkiego Babu ściągnął Stasia Arienika na 24h, by postraszyć Urszulę. Staś nie chce mieszkać z rodzicami - chce być Rekinem.
    * Chevaleresse wiedząc o działaniach Santino zinfiltrowała Rekiny. Szuka Stasia dla Alana i Melissę dla siebie.
        * Santino przyuważył Chevaleresse myśląc, że to jedna z jego dziewczyn. Chevaleresse znalazła "sprawcę". Zdecydowała się na ewakuację i powiedzenie o wszystkim Alanowi.
            * Znalazła Melissę... nie znalazła Stasia.
* przed Marysią NADAL stoją trzy pytania:
    * Co robimy z tym, że biją Torszeckiego :-(.
        * I jak to zrobić żeby nie było, że biją go bo Marysia pozwala bo woli Ernesta? 
        * Lub że Marysia go chroni więc go kocha?
    * Jak to zrobić, by Justynian oddał władzę lub by Marysia ją przejęła?
    * Myrczek x Sabina - co z tym robić?

### Sukces graczy

* Ekstrakcja Melissy. Znalezienie Melissy. Wyciągnięcie Melissy. Nie skonfliktowanie się ze wszystkimi.
* Ta kultura jest super atrakcyjna dla Rekinów. M.in. dla Daniela.

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Podwiert. Bar Ciężki Młot. Karolina jest jednym z nielicznych Rekinów, który tam bywa. Głośne miejsce, można się napić, dość obrazoburcze, ale dosyć spokojne. Atmosfera typu "pijemy" a nie "bijemy się". Zmęczeni ludzie po ciężkiej pracy. Karo jest tam zaakceptowana jako "swoja".

## Streszczenie



## Progresja

* .

### Frakcji

* .

## Zasługi

* Karolina Terienak: 
* Diana Tevalier: 
* Melissa Durszenko: 


* Rafał Torszecki: 
* Keira Amarco d'Namertel: zabójczyni wysłana w roli infiltratorki.
* Ernest Namertel: 
* Napoleon Bankierz: 
* Liliana Bankierz: 
* Daniel Terienak: 

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Kompleks Korporacyjny
                                    1. Bar Ciężki Młot



                                1. Dzielnica Luksusu Rekinów: 
                                    1. Fortyfikacje Rolanda
                                    1. Serce Luksusu
                                        1. Lecznica Rannej Rybki
                                        1. Apartamentowce Elity
                                        1. Fontanna Królewska
                                        1. Kawiarenka Relaks
                                        1. Arena Amelii
                                    1. Obrzeża Biedy
                                        1. Hotel Milord
                                        1. Stadion Lotników
                                        1. Domy Ubóstwa
                                        1. Stajnia Rumaków
                                    1. Sektor Brudu i Nudy
                                        1. Komputerownia
                                        1. Skrytki Czereśniaka
                                        1. Magitrownia Pogardy
                                        1. Konwerter Magielektryczny


                                1. Dzielnica Luksusu Rekinów: jeden wieczór bez prądu, bo Elektrownia Szarpien odcięła prąd.
                                    1. Serce Luksusu
                                        1. Arena Amelii: odbywała się tam ostra walka Daniel - Franek. Ale wtedy zgasł prąd.
                                    1. Sektor Brudu i Nudy
                                        1. Komputerownia: miejsce rezydencji Hestii; Hestia przekazała info Marysi i ustawiła ją jako Oficjalnego Przedstawiciela Aurum.
                                1. Kompleks Korporacyjny
                                    1. Elektrownia Węglowa Szarpien: kiedyś Amelia podpisała z nimi świetną umowę; przejęta przez Arieników po bankructwie. Szarpien odcięli prąd Rekinom.
                                    1. Dystrybutor Prądu Ozitek: Marysia podpisała z nimi umowę na prąd dla Rekinów.
                            1. Czółenko
                                1. Generatory Keriltorn: transformacja magii w energię elektryczną; pod kontrolą ludzi Grzymościa. Mimo starań Natalii Tessalon, nie podpisali umowy o dostawę prądu Rekinom.

## Czas

* Opóźnienie: 2
* Dni: ?

## Konflikty

* 1 - 
    * 
    * 
* 2 - 
    * 
    * 
* 3 - 
    * 
    * 
* 4 - 
    * 
    * 
* 5 - 
    * 
    * 
* 6 - 
    * 
    * 
