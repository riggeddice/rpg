## Metadane

* title: "Nadintensyfikacja Exemplis"
* threads: brak
* motives: tien-na-wlosciach, energia-exemplis, dark-temptation, dziwna-zmiana-zachowania
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [240815 - Weterynarz i Verlen w Przelotyku](240815-weterynarz-i-verlen-w-przelotyku)

### Chronologiczna

* [240815 - Weterynarz i Verlen w Przelotyku](240815-weterynarz-i-verlen-w-przelotyku)

## Plan sesji
### Projekt Wizji Sesji

.

* PIOSENKA WIODĄCA: 
    * Muse "Resistance":
        * (It could be wrong, could be wrong) To let our hearts ignite | (It could be wrong, could be wrong) Are we digging a hole? | (It could be wrong, could be wrong) This is out of control
    * Inspiracje
        * Malenia, blade of Miquella
     * Uczucie dominujące
        * "To jest TO co grozi wszystkim tienom i magom..."
        * "Jak mu pomóc i nie zniszczyć wszystkiego?"
* Opowieść o (Theme and vision):
    * "Tienowie są kochani i Aurum to cudowne miejsce, ale warto bać się tienów, nieważne jak dobrzy i pomocni by nie byli"
        * Cichoświst KIEDYŚ i TERAZ. Sanktuarium Kwiatów KIEDYŚ i TERAZ.
    * Cichoświst Verlen pokonał bardzo potężnego potwora i uratował dużo ludzi. Niestety, zaczerpnął za dużo energii i poszedł za daleko w kierunku na Exemplis. To go Skaziło.
        * "Nie pozwalacie sobie na osiągnięcie swojego pełnego potencjału. Pomogę Wam."
        * "Zdradzacie Verlenów i ideały Verlenów zostawiając niepotrzebne, marnotrawne elementy ludzkiej natury"
        * Decado Lodowaty Zabójca, Wrath z FMA, Ra's Al Ghul z (batman x żółwie ninja)
        * personality: Decado; silent Exemplar; monk
        * REQUIREMENT: pokazać go kiedyś a potem pokazać co się z nim stało. Zbudować "on jest fajny" -> "och nie".
    * Lokalizacja: Sanktuarium Kwiatów
* Po co ta sesja?
    * Jak wzbogaca świat
        * Pokazuje bardzo fajnego maga, Cichośwista
    * Czemu jest ciekawa?
        * Pokazuje jak działa przeciążona magia
    * Jakie emocje?
        * "Jak mu pomóc?"
        * "Nawet on upadł i ciągnie swoich ludzi za sobą..."
* Motive-split
    * tien-na-wlosciach: operacja Arianny i Vioriki w Verlenlandzie, by wyczyścić pomniejsze problemy na terenie Verlenlandu; coś co robią tieni by pomóc swoim
    * energia-exemplis: Cichoświst Verlen poszedł za głęboko w Exemplis, co sprawiło, że stał się mniej osobą a bardziej exemplarem, przez co cierpią inni dookoła
    * dark-temptation: utracić swoje człowieczeństwo i pójść głębiej w moc magiczną, stać się Exemplarem energii. Cichoświst "nigdy nie czuł się lepiej"...
    * dziwna-zmiana-zachowania: Cichoświst Verlen - z kompetencji i skupieniu na wnętrzu stał się Exemplarem Exemplis.
    * local-team-decent: Cichoświst zebrał grupę kompetentnych agentów i część z nich zgłosiło niepokojącą zmianę zachowania swojego tiena. Pomogą tien Verlen.
    * lost-my-way: przez Skażenie Exemplis Cichoświst skupia się na perfekcji i ochronie swoich a nie na ochronie WSZYSTKICH swoich ludzi. Więc "słabsi upadną".
    * misja-ratunkowa: cała sesja to z jednej strony próba uratowania miejsca przed Cichoświstem a z drugiej eksploracja co się tu dzieje.
    * ochrona-za-wszelka-cene: Cichoświst ochroni ludzkość i ludzi za wszelką cenę. Poszedł za daleko walcząc z potworem. Potem jego Skażenie zmieniło jego percepcję.
    * radykalizacja-postepujaca: dotyczy ludzi Cichośwista. Część idzie za swoim tienem, część staje przeciwko niemu. Verlenlandzki Klasztor Miecza jest skłócony.
    * ruination-unsealed: Cichoświst. Nawet ostrzeże, że nie będzie się powstrzymywał, bo Arianna i Viorika muszą osiągnąć swój pełen potencjał.
    * the-inquisitor: Cichoświst Verlen, Skażony przez własną sentisieć i Exemplis. WSZYSCY będą mieć pełną ludzką perfekcję. Ochrona wszystkich wymaga ekstremum. A jak nie chcesz tam iść, nie jesteś osobą.
    * bardzo-grozne-potwory: verlenlandowa splitharpia ("Brisela"-class), na którą poluje Cichoświst i która zadaje mu upiorne rany
    * hartowanie-verlenow: Arianna i Viorika mają to rozwiązać dla Brunhildy, mimo, że przez to może więcej osób ucierpieć. Uczenie się jest tego warte.
* O co grają Gracze?
    * Sukces:
        * Uratować tyle ile się da z Sanktuarium Kwiatów
            * piękne kwiaty -> utylitarne rośliny; nie potrzebujemy piękna
            * miejsce gdzie ludzie chronieni przez tiena -> każdy walczy i broni się sam; słabi są kosztem
            * człowieczeństwo i prawo do błędów i rozrywki -> perfekcja formy i duszy; każdy ma swoją rolę i będzie ją pełnić
            * ochrona Sanktuarium -> wygrana dla Exemplis; odblokowanie potęgi Arianny i Vioriki (constant assault)
    * Porażka: 
        * Działały za wolno i Sanktuarium umiera
* O co gra MG?
    * Highlevel
        * Pokazanie zniszczenia Cichośwista wynikającego z przedawkowania Exemplis
    * Co chcę uzyskać fabularnie
        * Uszkodzenie Sanktuarium
        * 
* Default Future
    * 
* Dilemma:
    * ?
* Crisis source: 
    * HEART: "Cichoświst próbował działać samemu, z całych sił, chronić... i przegiął"
    * VISIBLE: "Sanktuarium Kwiatów In Extremis"
        
### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

* Cichoświst poluje na splitharpię w okolicach Przelotyku i prosi o wsparcie (bo to krzywdzi ludzi)
* Cichoświst pokonany, ale uratowali ludzi


### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

* Faza 1: Walka z splitharpią alucis
    * "Brisela" (Lerissa-class, alucis + praecis, groźny przeciwnik)
        * z Cichoświstem używającym shotguna, miecza i tarczy, niezwykłej prędkości i techniki
        * Cichoświst chroni 3 ludzi; jego pięcioosobowy oddział ledwo daje radę. On chroni wszystkich.
        * ona używa: MINDCONTROL, SPLIT, CLONE STRIKE! itp
* Faza 2: Odpoczynek w Sanktuarium Kwiatów
    * zasadzenie własnych kwiatów (herbalist Kamil)
    * zmierzenie się ze szczytem strachu (historian Maja)
    * pokazanie żołnierzom jak wyciągnąć młodych z lasu mgieł (kapral Wojmił)
    * porozmawiać z rannymi i chorymi w szpitalu (lekarz Brunon)
    * teatrzyk (dzieci dla tienów, pokazują walkę ze smokiem ze Smoczej Góry, z zabawkami)
* Faza 3: Brunhilda
    * 
* Faza 4: Sanktuarium, Upadłe
* Faza 5: Ucieczka od Cichośwista
* Overall
    * chains:
        * ?
    * stakes:
        * śmierć cywili
    * opponent
        * toksyczna sentisieć; atmosfera
    * problem
        * ludzie cierpią

## Sesja - analiza
### Mapa Frakcji

.

### Potwory i blokady

* teren
* fanatyzm

### Fiszki

.

### Scena Zero - impl

Jakieś pół roku temu. Arianna i Viorika w północnej części Verlenlandu. I dostają sygnał SOS od Verlena - z okolic Przelotyka, czyli safari. Wiadomość czytelna "Cichoświst Verlen, niebezpieczny przeciwnik, osłaniam ludzi, nie utrzymam. Wsparcie Verlena - potrzebne."

A+V szybko się zbierają i lecą. Maksymalna prędkością sensowną (nic się nie stanie); ścigacz z założeniem że Verlen ma broń. Szybkie pytanie o dobry skrót do miejscowych Verlenlandczyków i jest informacja - +P.

Tr P +3:

* V: tak szybko że nic się nie stało
    * po drodze Wasz ścigacz mija 8-osobą grupkę Verlenlandrynków, uzbrojonych, zmierzających ostrożnie w tą samą stronę. Idą wesprzeć Cichośwista.
    * dostarczyli Ariannie i Viorice elementy różnorodnego sprzętu który może im się przydać. Działko automatyczne - wielkokalibrowe działko rozstawne.
    * zgodnie z rozkazami Arianny mają zabezpieczyć bezpieczny teren i odwrót.

Ścigacz wyskakuje zza skał i wyżyn tylko po to by wpaść na Verlena ostrzeliwuje przeciwko... anielicom? A jeden z chronionych ludzi przez Verlena próbuje strzelić mu w plecy.

Arianna robi drift ścigaczem, Viorika zeskakuje na kolesia który strzela do Cichośwista by go unieszkodliwić; Cichoświst chroni 10-15 ludzi. "Karawanę". A Arianna rozstawia działko.

* V: 
    * Viorika rzuciła się na delikwenta, delikatnie go zmiażdżyła serwopancerzem (no lethal intent, ale jak coś się złamie to nie płaczemy) i zdjęła go z Cichośwista
    * Arianna się rzuciła i rozstawiła działko automatyczne.
   
Anielice przygotowują się do ataku. Są w kręgu otaczającym Cichośwista i Viorikę. Arianna jest troszkę dalej. Lokalsi są częściowo w walce, częściowo oszołomieni.

Arianna się SKUPIA ze snajperką. Chce widzieć patterny. Działko ma strzelić i ma za zadanie pokazać PRAWDZIWOŚĆ przeciwników.

Tp +4:

* Xz: Gdy działko wystrzeliło, kilka...naście? anielic skupiło się na nowym zagrożeniu. Lecą w stronę działka. Nie zmniejsza to presji z Cichośwista i Vioriki, ale podnosi dla Arianny i działka.
* V: Arianna widzi coś... niepokojącego. Tych anielic jest mniej - to się zgadza - to są iluzyjne klony. Większość z tego to ułudy. Ale Arianna nie jest pewna czy wie gdzie są oryginalne anielice.
* X: Anielice słusznie uznały działko za główne zagrożenie. Skupiły się na JEGO ZNISZCZENIU. Działko je widzi - a przynajmniej lepiej.

Viorika próbuje skrzyknąć i zebrać do kupy lokalsów. Wytrącić ich ze stuporu. Zmobilizować do ognia zaporowego. Atakuje Alucis - siłę Anielic. Idzie "jako sierżant".

Tr Z +3:

* Xz: Widząc sytuację, Anielice skupiają się na wzmocnieniu ataku. Przestały udawać, że są nie-tam gdzie są i zintensyfikowały atak. Wyraźnie widać, że traktowały Cichośwista jako potencjalną przynętę. "Przyprowadzi ludzi". Ale Cichoświt okazał się być silniejszy - został ranny, dość ciężko ranny, z "niewidzialnego miejsca" - ale sam ściął niewidzialną Anielicę.
    * Arianna zorientowała się, że przeciwnik najpewniej nie może utrzymywać w tym momencie aż tyle iluzji i mocy i skupiła się na przekierowaniu feedów co podniosła Viorika. Viorika: +P
* Vz: Viorika otrząsnęła ludzi i ich zmobilizowała. Widok padającego ale wstającego Cichośwista pomógł. Ogień zaporowy, co sprawia, że Anielice mają duży problem - nie mogą się dobrze zbliżyć. WIĘC ich uwaga kieruje się ku Ariannie.
* ARIANNA CZMYCHNĘŁA NA DRZEWO! W konary drzewa. Przeciwnik tam do niej nie dotrze. (+P do puli; nie ma łatwego celu i Anielice wolą się wycofać)
* V: Anielice się wycofują. Nie ma tu dla nich niczego.

Cichoświst się obraca do Arianny i Vioriki. Lekki uśmiech. Mowa Cichośwista była krótka i treściwa - o jedności, współpracy i tym że razem ludzie chronią się przed potworami. Ludzie byli zdziwieni że mowa taka... krótka i mało kwiecista, ale W SUMIE CICHOŚWIST MOŻE BYĆ ZMĘCZONY. Nadal - jest larger niż life.

Jak tylko się udało problem rozwiązać, jego dźwiedź - Ponury Bicz - zaczął go łajać. Że znowu za dużo wrzucił na siebie. Że znowu próbował zrobić samemu.

Ponury Bicz spytał Ariannę o jej stan matrymonialny i dokucza Cichoświstowi że potrzebuje narzeczonej. Arianna się świetnie bawi a Cichoświst - czerwieni. Z jego perspektywy, Verlenowie powinni dążyć do piękna i kwiatów a nie tylko jak najlepszej walki.

Arianna się wprosiła do Sanktuarium Kwiatów. Viorika zabezpieczyła Anielicę. Teraz jest przekonana, że musi coś z tym zrobić XD. Dźwiedź zbierze truchło Anielicy - co z tym zrobić i jak do tego podejść. Bo potwór nie może przetrwać. W Przelotyku byłby niebezpieczny.

Sanktuarium Kwiatów, unikalne miejsce w Verlenlandzie. Dotarcie tam to trzeba się przebić przez typowy Verlenland - wzgórza, niebezpieczeństwa itp. Ale macie oddział, macie 3 Verlenóœ i jesteście "na swoich ziemiach". Nie jest to przyjemna trasa, ale to Wasz dom. Typowa trasa.

Cichoświst jak tylko jest w stanie próbuje się ruszać sam itp. Dźwiedź "popisujesz się przed Arianną!" Cichoświst odpuścił z pójściem "pokonam, dam radę" bo nie chce wyjść na osobę popisującą się przed Arianną. O to dźwiedziowi chodziło.

Viorika jest zaskoczona jego podejściem. Skąd pomysł na piękno, kwiaty - i lokacja Sanktuarium Kwiatów? XD. Nieco nie-Verlenowe.

* Viorika: Sanktuarium Kwiatów? Nietypowa nazwa i niestandardowa lokalizacja. Twój pomysł czy było?
* Cichoświst: Vioriko, (chwila ciszy bo nie wie jak to powiedzieć), jesteśmy istotami Exemplis. Szukamy najlepszego w naszej naturze. Jeśli ludzie... ludzie potrzebują piękna, potrzebują miejsca wartego ochrony. To jest w naszej naturze. Perfekcja to nie tylko walka. Jest piękno w walce, ale jest też piękno poza nią.
* Arianna: Twój pomysł z ogrodem czy u was rodzinne?
* Cichoświst: To miejsce było przede mną. Ono było zaniedbane. Miało potencjał. I...
* Arianna: wziąłeś pod skrzydła i rozwinąłeś by kwiaty mogły rozkwitnąć?
* Cichoświst: Verlenowie kojarzą się z walką i głośnymi przechwałkami. Ja chcę pokazać, że możemy też działać ciszą i pięknem. To też Exemplis i Praecis. To jest zgodne z sentisiecią i sentisieć... ona Odpowiada. Ona z tym rezonuje. To miejsce takie powinno być.

Arianna ma złą (EVIL!) strategię. Dopytać go o to SKĄD POMYSŁ a jak będzie kręcił to oprowadzi po ogrodzie, miło czas, odwraca uwagę - i wydobyć informacje. A on ma słabość do dziewczyn i piękna.

Tr Z +3:

* Vr: Cichoświst nie jest osobą, która dużo mówi i opowiada o przeszłości. Ale Arianna ma swoje niewieści triki. A on nie ma odporności.
    * Cichoświst się przyznał, że jak był młody, to jego rodzice mieli jako przyjaciół Diakonów
    * Cichoświst miał... romans (tu się zaczerwienił) z trzema Diakonkami. Jednocześnie. To było... trudne.
    * Cichoświst pojechał za Diakonkami. Tam... nauczył się wielu rzeczy których się nie spodziewał (tu nie wchodził w detale)
    * One powiedziały, żę nie przyjadą do niego, bo tu jest smutno potwornie itp. Przekonały go, że piękno jest ważne.
    * On to wyniósł.
    * TO MIEJSCE jego wybrało. A on uznał, że jego obowiązkiem jest to dać sentisieci.
   
DŹWIEDŹ oprowadza Ariannę i Viorikę. "Spektakl Teatralny dla VIPów".

Teatr. Jest ładnym, funkcjonalnym budynkiem. Stoi tam historyczka Maja - dzieci zrobiły spektakl dla swojego tiena a dźwiedź wpisał Ariannę i Viorikę na listę. Dzieci zrobiły spektakl o Smoczej Jamie i Verlenach ratujących ludzi przed Ostatnim Smokiem, bardzo kwieciście. High cringe level, lots of fun.

Viorika poszła się siłować z dźwiedziem na arenie. Dźwiedź jest dobrej klasy wojownikiem - tam są wojownicy którzy ćwiczą. Dźwiedź poszedł w siłę, Viorika "mmm, ładne futerko, używasz olejków?"

Tp +3:

* V:
    * Dźwiedź DOSTAŁ, zdestabilizował się lekko: 'moje futerko lepiej wygląda niż Twoje włosy, bardziej zadbane'
    * Viorika: "bo moje włosy są świeżo po walce z aniołkiem, misiaczku"
    * Dźwiedź: "dobra, pewna Verlenka chce zobaczyć czemu mówią o mnie Ponury Bicz."
* V: 
    * Viorika krąży dookoła dźwiedzia by wyczuć styl i skąd jest
    * Ten dźwiedź jest z domeny Atraksjusza Verlena - styl Atraksjusza to jest "ból jest tylko ceną za zwycięstwo"
* V: 
    * Viorika fintami i manewrami wyczuła jego ruchy
    * Dźwiedź manipuluje odległością. Jego celem jest użyć łańcucha jako dywersji ALBO kontrolować ruchy Vioriki
    * Viorika poszła w "złagodniałeś i zrobiłeś się mięciutki jak futerko" by go sprowokować i wyciągnąć jego intencje (+1V)
* V: 
    * Dźwiedź o dziwo się trochę rozluźnił. Ostrzejszy w walce, ale nie da się tauntować.
    
Tr +4: 

* Vr: Viorika przeskakuje jak o tyczka jak on robi sweep po ziemi; ona jest na jego plecach, wybija się i robi dystans.

Viorika samą bitwą udowodniła, że jest wartościową Verlenką. Dysydenci, ludzie którym to nie pasuje - mogą do Vioriki podbić.

Chwilę powalczyli, po czym dźwiedź w końcu Viorikę złapał, ale miała bardzo duże sukcesy w samej walce.
    
A Arianna chce zobaczyć czym to miejsce się różni od Verlenlandu. Czym jest to miejsce, jak tu objawia się Praecis Exemplis:

* Każda PARA energii magicznych ma 'facet'
    * Verlenland "Marcinozaura" to jest 'facet': BOAST, EPICKOŚĆ, SYMBOLICZNOŚĆ (jestem exemplarem / symbolem)
    * Verlenland "Adelicji" idzie mocniej w kontrolę. Kontrola Która Chroni.
    * Verlenland "Cichośwista" idzie w stronę Exemplis jako WYOSTRZENIE NATURALNEGO PIĘKNA, SYMBOL a Praecis ZAPEWNIENIE TEGO

Podleczony Cichoświst i Arianna SADZĄ KWIAT.

Cichoświst "cieszę się, że zdecydowałaś się zasadzić piękny kwiat w tym miejscu". 
Arianna "najlepiej Was zrozumiem jeśli zrobię to co Wy zwykle robicie, sadzenie kwiatów będzie krokiem w dobrą stronę. Coś co nie zrobiłam nigdzie w Verlenlandzie. Tak wyobrażałam sobie Verlenland ale takiego miejsca nie było"
Cichoświst "to MY decydujemy jaki będzie Verlenland i gdzie będzie. Sentisieć odpowie na nasze żądanie. Są miejsca z takim potencjałem."
Arianna "to miejsce jest piękne ale trzeba wybierać wojny. Można się skupić na takim miejscu, ale też pomagać ludziom zagrożonym przez potwory. Nie na wszystko jest czas. Cieszę się, że są Verlenowie, którzy to robią. Mogło źle zabrzmieć, lepiej brzmiało w mojej głowie... chciałam powiedzieć że chcę to mieć u siebie, ale presja ze strony rodziny sprawia, że nie mogłam i nie było czasu. I cieszę się, że ktoś inny miał czas."
Cichoświst "Pamiętaj, że jesteś Verlenką. NIKT nie powie Ci co masz robić. TY decydujesz że ich słuchasz. Naprawdę. Rozumiem presję. Naprawdę oni Cię kontrolują, bo Ty im na to pozwalasz. I tylko w tym zakresie."
Arianna "Mogę odrzucić wpływy ale pomogę mniejszej ilości osób"
Cichoświst "Prawda, masz rację."
Arianna "Cena, którą płacę za możliwości"
Cichoświst "Zawsze jesteś tu mile widziana /z uśmiechem. Bo Verlenland jest miejscem, o który warto walczyć"
Arianna "przyjadę"

Viorika ma okazję pogawędzić z ludźmi. Oni widząc ją, jako "normalną" tienkę, nie boją się jej - i ci, którym mniej pasuje tutejszy tien będą uważali, że ona jest "bezpieczna". Viorika uważa, że to jest naprawdę fajne miejsce (choć nie dla niej), ale szuka zagrożenia dla terenu czy stwory. Polityka, ktoś-coś. Tak samo interesuje ją ogólne poczucie czy ludzie MUSZĄ tu być. Chcą czy nie?

Tp +3:

* V: 
    * zagrożeniem jest to, że to jest JEDEN tien i to introspektywny. Ludzie to widzą. Oni sobie swojego tiena chwalą, ale jest JEDEN i Sanktuarium umrze.
    * to miejsce JEST dość ascetyczne. Więc - "turyści" nie mają czego tu szukać. Ale jest za ładne i za "nie-verleńskie"
        * sentisieć to akceptuje, ale to miejsce jest na odludziu więc nic z nią nie walczy
    * przez to, że Cichoszept nie do końca jest szanowany wysoko, on nie liczy na pomoc. On próbuje wszystko wygrać samemu, bo nie ma nadziei że mu pomogą.
    * boją się, że jest sam. To jest zawsze zagrożenie.
    * dźwiedź przypałętał się, został i pilnuje tiena. "lojalny jak dźwiedź". I prostolinijnie próbuje znaleźć mu partnerkę.







* Faza 2: Odpoczynek w Sanktuarium Kwiatów
    * zasadzenie własnych kwiatów (herbalist Kamil)
    * zmierzenie się ze szczytem strachu (historian Maja)
    * pokazanie żołnierzom jak wyciągnąć młodych z lasu mgieł (kapral Wojmił)
    * porozmawiać z rannymi i chorymi w szpitalu (lekarz Brunon)
    * teatrzyk (dzieci dla tienów, pokazują walkę ze smokiem ze Smoczej Góry, z zabawkami)
    
    
    



Arianna proponuje atakować po skrzydłach. 



### Sesja Właściwa - impl



## Streszczenie



## Progresja

.

* Arianna Verlen: znana jako element Problematycznej Parki z Vioriką
* Viorika Verlen: znana jako element Problematycznej Parki z Arianną

## Zasługi

.

* Arianna Verlen: zamiast zwalczać Wielobija, weszła z nim w sojusz i przekonała do współpracy. Potem nawiązała dialog z Czesławem Blakenbauerem i dyplomatycznie rozwiązały sprawę, zrzucając winę na Tyraniusza zamiast tępić weterynarza który po swojemu próbował ratować miasteczko.
* Viorika Verlen: pozyskuje informacje od ludzi Gadobija, potem zrobiła dobry zwiad w Serpiniek i wykryła obecność Blakenbauera. Udaje szorstką i ostrą w rozmowie by ułatwić Ariannie.
* Gadobij Verlen: usłyszał o potworze i przybył ratować ludzi w Serpiniek; niestety, poszedł "heroicznie" i starł się z Czesławem Blakenbauerem chcącym tego samego. Skończył zatruty na kiblu z amnestykami. Bardzo sympatyczny młody Verlen.
* Wielobij Verlen: przybył pomścić brata z oddziałem gotowym do walki, jednak wykazał się rozsądkiem, akceptując plan Arianny i Vioriki. Gdy już widzi, że nikt nie nastaje na honor brata, pomocny i życzliwy innym Verlenkom, choć nie przepuści Blakenbauerowi.
* Czesław Blakenbauer: weterynarz przekonany o okrucieństwie Verlenów; gdy Gadobij się pojawił to wzmocnił Hydrę by ją wyleczyć i pomóc. Podał Gadobijowi antybiotyki-amnestyki (niestety); na szczęście, posłuchał Arianny i Vioriki, dogadał się z nimi i uniknął Krwawej Zemsty ze strony Wielobija.
* Ewelina Mistowir: lekarka-verlenlandrynka w Krwawirogu; zdiagnozowała stan Gadobija i odkryła, że zatrucie ma magiczne podłoże, co skierowało śledztwo na właściwe tory. Wykryła, że Gadobij i jego ludzie mieli pomoc medyczną. Zafascynowana Arianną; fangirl, mimo, że ma 53 lata.
* Grażyn Ozdamrin: członek starszyzny Serpiniek w Przelotyku; nieufny wobec Verlenek, ale pomógł utrzymać spokój w Serpiniek. Zależy mu na mieście i na spokoju w nim. Inteligentny.
* Tyraniusz Blakenbauer: desygnowany przez Blakenbauerów kozioł ofiarny na krzywdy Verlenów; świetnie się bawi jako Wielki Zły. Tym razem wziął winę za zatrucie Gadobija.

## Frakcje

* .

## Fakty Lokalizacji

* Sanktuarium Kwiatów: w Verlenlandzie, niedaleko Przelotyka; 


## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. Sanktuarium Kwiatów
                                1. Klasztor Miecza
                                1. Fortiwioska
                                    1. Szpital Opieki
                            1. Sanktuarium Kwiatów, okolice
                                1. Las Mgieł
                                1. Szczyt Strachu

## Czas

* Opóźnienie: 4
* Dni: ?

## Inne
