## Metadane

* title: "Potworny servar Verlenów"
* threads: legendy-verlenlandu
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [230426 - Mściząb, zabójca arachnoziemskich jaszczurów](230426-mscizab-zabojca-arachnoziemskich-jaszczurow)

### Chronologiczna

* [230426 - Mściząb, zabójca arachnoziemskich jaszczurów](230426-mscizab-zabojca-arachnoziemskich-jaszczurow)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * .
* CEL: 
    * .

### Co się stało i co wiemy

* Kontekst
    * .

### Co się stanie (what will happen)

* S1: .
* S2: .
* S3: .
* S4: .

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* .POTWÓR TAR-LANCER
    * PIERWOTNIE: 
        * 
    * SKILLSET
        * PHASE 1
            * 
        * PHASE 2
        * PHASE 3
        * PHASE 4

    

### Scena Zero - impl

.

### Sesja Właściwa - impl



## Streszczenie



## Progresja

* .

### Frakcji

* .

## Zasługi

* Arianna Verlen: 
* Viorika Verlen: 
* Marcinozaur Verlen: 
* Ula Blakenbauer: 

* Mściząb Verlen: 
* Atraksiusz Verlen: 



* Fantazjusz Verlen: 41 lat, 
* Aleksander Samszar: 36 lat, 
* Apollo Verlen: w tle współpracował z Vioriką i Arianną, by ukryć sytuację z Eleną. Wziął na siebie sprawę z podłożeniem świni i ze wtopieniem Tymka Samszara w menhir. Jako 'mszczenie się za Viorikę'.
* Marcinozaur Verlen: ma teraz 27 lat. SZPIEGATOR. Troszczy się o Elenę i o Verlenów; chce by Apollo przeprosił Elenę, chce pokazać Verlenom siłę Blakenbauerów (stąd Ula i jej płaszczki), chce pokazać Uli siłę Verlenów (stąd dźwiedzie, Arianna i Viorika) oraz chce rozwiązać Szeptomandrę. A wszystko to w swoim uroczym stylu GŁOŚNIEJ ZNACZY LEPIEJ.
* Ula Blakenbauer: ma teraz 22 lata. Arogancka mistrzyni płaszczek i sentiinfuzji. Współpracuje z Marcinozaurem szukając śladów Szeptomandry. Gdy Viorika zaproponowała jej wyzwanie, poszła w mimiki (jaskinia jest JEDNYM mimikiem). Pokonana przez verleńską taktykę i dźwiedzie, czego by się NIGDY nie spodziewała. Acz zaimponowała siłą ognia i magii.
* Apollo Verlen: jest święcie przekonany, że Blakenbauerowie chcieli skrzywdzić Elenę; wziął na siebie ogień reputacyjny by chronić Elenę. To spowodowało konflikt z Marcinozaurem (który to konflikt Marcinozaur wygrywa dzięki Uli). Ściągnął Ariannę i Viorikę do rozwiązania sprawy. Główny dyplomata Verlenów XD.
* Elena Verlen: (nieobecna) gdy zbliżała się do Szeptomandry, zaczęła mieć halucynacje i krzyżówki tego co było naprawdę z Szeptami. Gdyby nie Apollo, skrzywdziłaby swoich żołnierzy. Dlatego Apollo ją odesłał.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                           1. Arachnoziem: słynie ze złóż minerałów, niestabilności i wyjątkowo podłych podziemnych potworów; ma garnizon 50 żołnierzy na 20k populacji rozsianych po górzystym terenie.
                                1. Kopalnia
                                1. Bar Łeb Jaszczura

## Czas

* Opóźnienie: 11
* Dni: ?


