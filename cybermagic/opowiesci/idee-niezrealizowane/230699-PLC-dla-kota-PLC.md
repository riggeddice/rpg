## Metadane

* title: ""
* threads: historia-klaudii, historia-martyna, grupa-wydzielona-serbinius
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [230530 - Ziarno Kuratorów na Karnaxianie](230530-ziarno-kuratorow-na-karnaxianie)

### Chronologiczna

* [230530 - Ziarno Kuratorów na Karnaxianie](230530-ziarno-kuratorow-na-karnaxianie)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * 
* CEL: 
    * 

### Co się stało i co wiemy

* Kontekst
    * Tnakraz był kiedyś jednostką zaopatrzeniową, dostarczającą rakiety. Przerobiony został w _prospector_ badający byty w okolicy Anomalii Kolapsu i ekstraktujący główne rzeczy
    * Wpadł w Anomalię Przestrzenną. Wpadł w pętlę. Daleko od szlaków, poważnie uszkodzony. Mało zasobów. Usypiamy ludzi w kwaterach do czegoś najbliższego hibernacji co mamy.
        * Torpor medyczny przez rekonstrukcję LifeSupport i odpowiednie podpięcie.
    * Tnakraz wysyła SOS, ale nie ma nadziei.
* Problemy
    * Tnakraz jest częściowo w Anomalii Przestrzennej. Ani tu, ani tam.
        * -> Serbinius musi wyciągnąć Tnakraz z Anomalii
        * X: wciąga naszych na pokład
    * Tnakraz jest zainfekowany Anomalią
        * przestrzeń
            * X: jesteś nie tam gdzie powinieneś
            * X: częściowa integracja z Tnakrazem
        * poważne uszkodzenia
            * X: wbicie się w Tnakraz, spadek na dolny pokład
        * pętla koszmarów
            * noktiańscy żołnierze (echo)
            * X: strzelają i to z zaskoczenia
    * Tnakraz ma załogę której część da się uratować
        * znaleźć i wyciągnąć


### Co się stanie (what will happen)

* S1: Karnaxian jest poza swoją trasą, ma wyłączone silniki. (są rozsypane Ziarna jako miny) (poziom energożerności jest absurdalny, max. 10h do wykończenia wszystkich baterii)
    * Semla chce wysłać killware do Persefony by Karnaxian mógł ją zainfekować Ziarnem
    * Gdzieś na pokładzie jest Aleksandria (technicznie, repurposed AI Core / life support)

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* .Rafał Klapidor
    * (ENCAO: +-00-) |Szuka pieniędzy tam gdzie inni widzą śmieci;; niezwykle optymistyczny, TAK: Universalism, Hedonism >> Face | DRIVE: Hardholder)
    * kapitan Karnaxian (owner, pilot) faeril: skupienie na przychodach i miłość do sprzętu
    * "Dla innych to tylko złom. Dla nas - dla nas to prawdziwe bogactwo!", "Jest jedna droga - przez złom do złota!"
    * styl: wesoły, chciwy dla obcych acz życzliwy dla swoich
* .Weronika 
    * (ENCAO:  00++-) |Nudna i przewidywalna;;Cierpliwa, jak pająk w sieci;;Życzliwa| VALS: Security >> Conformity| DRIVE: Starsza Siostra Statku
* .Karnaxian
    * Zainfekowany Ziarnem Kuratorów
    * broń: killware, "rozproszone kawałki złomu (nanoziarna)" dookoła, Ferrantos-class anti-Lancer

### Scena Zero - impl

.

### Sesja Właściwa - impl



## Streszczenie



## Progresja

* .

### Frakcji

* .

## Zasługi

* Klaudia Stryk: 
* Martyn Hiwasser:
* Fabian Korneliusz: 
* Anastazy Termann: 
* Helmut Szczypacz: 
* TAI Selena d'Tnakraz: 
* SC Tnakraz: kiedyś zaopatrzeniowa jednostka noktiańska, 

* Kurator Sarkamair: rozłożył Ziarna Kuratorów (zbiór fabrykatorów) i przejął Karnaxian, gdzie się zrekonstruował. Zaatakował też Serbiniusa, ale został odparty. Magia Klaudii PRAWIE dała mu skille i możliwości Malictrix, ale jego plan został odparty i zestrzelony. Okazał się być niesamowicie niebezpiecznym przeciwnikiem - dobrze planuje, atakuje z zaskoczenia i lubi porywać ludzi, adaptować ich jednostki i odlecieć do bazy.
* Helmut Szczypacz: (NIEOBECNY), siedzi na Kontrolerze Pierwszym i dostaje opiernicz za POTENCJALNE ZABICIE ANASTAZEGO. Miranda nie wybacza ;-).
* OO Serbinius: odparł atak Kuratora Sarkamaira, ma lakko uszkodzone systemy komunikacyjne. Musiał zestrzelić przechwyconego Karnaxiana, nie był w stanie niestety więcej zrobić.
* SC Karnaxian: KIA. Wpierw przejęty przez Kuratorów (Semla + lifesupport -> Alexandria + Kurator Sarkamair), potem wektor do przejęcia Serbiniusa, potem zestrzelony przez Serbiniusa jak nie dało się uratować załogi.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 11
* Dni: ?

## OTHER
### Statek "Tnakraz", prospector

Pokład Główny/Komora Rakiet: Główne miejsce składowania i obsługi pocisków rakietowych. Po przebudowie przekształcone w obszar gromadzenia próbek z asteroid.

Sekcja Maszyn/Wiertnice: Zawiera główne silniki statku i systemy napędowe. Po przebudowie wyposażone w duże, mocne wiertnice i inny sprzęt do górnictwa.

Komora Przetwarzania/Laboratorium: Pierwotnie służyła do obsługi i utrzymania pocisków, teraz jest wykorzystywana jako laboratorium do przetwarzania i analizy próbek mineralnych.

Mostek/Sensory i Skanery: Znajdują się tu systemy sterowania statku, systemy nawigacyjne i komunikacyjne. Po przebudowie dodano systemy skanowania do identyfikacji cennych minerałów.

Kwatery Załogi: Miejsca do spania, relaksu i rekreacji dla załogi. Mogą istnieć osobne kabiny dla różnych członków załogi lub wspólne pomieszczenie.

Kantyna: Miejsce, gdzie załoga przygotowuje i spożywa posiłki. Jest tu także miejsce na odpoczynek i rekreację.

Magazyn: Miejsce, gdzie przechowywane są zapasowe części, narzędzia, sprzęt do prospekcji oraz inne niezbędne materiały.

Mechaniczne Ramiona/Dźwig Ładunkowy: Zastosowane do obsługi pocisków rakietowych, teraz służą do gromadzenia próbek z asteroid.

Doki Serwisowe: Miejsce, gdzie mogą być przeprowadzane naprawy zewnętrzne, utrzymanie mechanicznych ramion i innych systemów na zewnątrz statku.

System Wsparcia Życia (Life Support): Odpowiada za utrzymanie odpowiednich warunków życiowych na statku.

Jądro AI (AI Core): Centrum obliczeniowe statku, kontrolujące większość systemów, zarówno w trakcie misji wojskowych, jak i operacji poszukiwawczych.
