## Metadane

* title: "X"
* threads: salvagerzy-lohalian, rehabilitacja-eleny-samszar
* motives: fish-out-of-water, fish-in-water, casual-cruelty, karne-podle-zadanie, konflikt-wewnatrzzespolowy, the-gatekeeper
* gm: żółw
* players: anadia, kić

## Kontynuacja
### Kampanijna

* [231024 - Rozbierany poker do zwalczania Interis](231024-rozbierany-poker-do-zwalczania-interis)

### Chronologiczna

* [231024 - Rozbierany poker do zwalczania Interis](231024-rozbierany-poker-do-zwalczania-interis)

## Plan sesji
## Plan sesji
### Theme & Vision

* PIOSENKA: 
    * Asking Alexandria - When Everyday's The Weekend
        * "I can't see straight anymore, just got kicked out the fucking door! | It's all easy, it's all easy! | That's how we fucking roll!"
* Opowieść o (Theme and vision): 
    * "Orbiter, normalnie dumny i pełen energii stracił motywację. Dowodzenie idzie od góry a tu dowodzenie nie idzie od góry."
    * Nikt nie wierzy w te misje i w te działania. Z jakiegoś powodu, ta grupa Orbitera zgasła. Ale misja musi być wykonana.
    * Desperacka zabawa by ukryć strach, brak nadziei i "czemu tu jesteśmy"
    * SUKCES
        * zbudowanie zespołu, identyfikacja problemu
* Motive-split
    * fish-out-of-water: (-> Elena). Co można? Czego nie można? Co jest akceptowalne? Jak daleko się posunąć? Jak nie wypaść?
    * fish-in-water: (-> Estella). Jak nawigować TEN zespół, dokąd się posunąć? Jak ich pozyskać? Jak zintegrować Elenę?
    * casual-cruelty: (Jarek: traktowanie Maurycego i tienek, bo MOŻNA), (Tomasz: ignorowanie martwych noktian), (dowodzenie: żołnierze mają robić robotę mimo morale i strachu)
    * karne-podle-zadanie: infiltracja zniszczonej jednostki i jej wyczyszczenie
    * konflikt-wewnatrzzespolowy: (Jarek, Tomasz) mają cele, Paweł trzyma w kupie śmiechem, Maurycy skupia się na sekretach Harmonii
    * the-gatekeeper: noktiańskie echo, chroniące Oculi Vespae. Odpędza istoty Interis, wenapnitów oraz Orbiter. Zmienia Ocula Vespae w niebezpieczną jednostkę.
* Detale
    * Crisis source: 
        * HEART: "niezdolność do akceptacji tienek, niechęć do bycia w tym miejscu, to wszystko jest bez sensu i niebezpieczne" 
        * VISIBLE: "niska motywacja, niska energia, niechęć do robienia czegokolwiek, chęć do zabawy, narzekanie na dowództwo"
    * Delta future
        * DARK FUTURE: "zespół się rozpada, nie będą działać razem, pocięci i w złej formie"
        * LIGHT FUTURE: "zespół się ukonstytuował"
    * Dilemma: "How far to go in order to make that stuff work"

### Co się stało i co wiemy

* Zamierzchła Przeszłość
    * irrelevant
* Przeszłość
    * Maurycy przegrał zakład, ma zaprosić tienkę
    * Odblokowali jednostkę noktiańską, nazywa się 'Osculi Vespae'. Nie ma tam nic interesującego. Trzeba wyczyścić. Nie jest groźna
        * ZNOWU nie działa komunikacja
        * ZNOWU burza śnieżna
* Teraźniejszość
    * .
* Frakcje
    * tym razem każda jednostka reprezentuje własną frakcję; patrz na osoby
* w okolicy bardzo dużo jednostek Orbitera i Noctis jest rozbitych
* grupa czyszcząca z ramienia Orbitera, z Astralną Flarą - mają podczyścić teren; baza to Lohalian

### Co się stanie (what will happen)

* Faza 0: _feast_
    * .
* Faza 1: _droga do celu_
    * stakes:
        * jaką opinię mają żołnierze o obu tienkach
            * jak daleko tienki się posuną (Maurycy ma jedną rozebrać w grze w karty, Pawełek będzie oszukiwał)
            * jak skończy się pojedynek z Cyprianem. Jarek bierze to na siebie, Cyprian nic nie ma do niego.
        * czy sprawa zaeskaluje wyżej?
    * opponent: 
        * karciarze: Jarek (brutalna niechęć do tienek), Maurycy (nieśmiały przegrał zakład)
    * problem:
        * brak nadziei, chęć gry tym co mamy, impreza, demoralizacja. Nie chcą być żołnierzami na tym terenie.
    * koniec fazy:
        * gra karciana się kończy
* Faza 2: _misja sprzątania Osculi Vespae_
    * stakes:
        * żołnierze czy przyjaciele?
        * demonstracja kompetencji i przydatności
    * opponent: 
        * burza, złe warunki, fatalny teren, jednostka w złym stanie
        * Tomasz chce pieniądz
    * problem:
        * to samo co wyżej.
        * kto podłożył te zwłoki?
    * koniec fazy:
        * czysta jednostka.
* SN: spotkanie z inżynierem - nic nie działa, wykryli jednostkę w zimnie. Może tam coś jest.
* SN: wejście na uszkodzoną noktiańską jednostkę, szabrujemy

.
### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki
#### 1. Grupa wydzielona Lohalian

SZTAB

* Mikołaj Larnecjat: dowódca grupy wydzielonej sprzątającej Przelotyk Północny; stracił bliskich podczas wojny
    * OCEAN: (E-O-): zamknięty w sobie, rzetelny, spokojny i dobry w powtarzalnych operacjach. "Nie powinienem był przetrwać wojny. Jeszcze Orbiterowi jednak się przydam."
    * VALS: (Conformism(Duty), Humility): jak coś ma zrobić, zrobi i dostarczy. "Nieważne czy mi się podoba, czy nie. Jestem elementem maszyny Orbitera."
    * Core Wound - Lie: "Wszystko, co kochałem zginęło i zostało zniszczone. Jestem sam." - "Nie przywiązuj się. Jest tylko misja."
    * Styl: Spokojny, absolutnie nie inspirujący i cichy miłośnik muzyki klasycznej pochłonięty swoimi myślami, ale wykonujący każde polecenie rzetelnie i sensownie.
    * metakultura: Atarien: "dowódca każe, ja wykonuję. Na tym polega hierarchia. Tak się buduje coś większego niż my sami."
* Rachela Brześniak: czarodziejka Orbitera (katalistka i technomantka), dołączona do pomocy w dekontaminacji terenu; na uboczu, bo jest "dziwna"
    * OCEAN: (N+C+): niezwykle ostrożna z czarowaniem i anomaliami, niewiele mówi i raczej trzyma się sama ze sobą.
    * VALS: (Achievement, Universalism): Skupia się dokładnie na zadaniu, by je wykonać może pracować z każdym. "Dekontaminacja to moje zadanie. Zajmijcie się resztą."
    * Core Wound - Lie: "Współpracowała z żywymi TAI i na jej oczach Rzieza zniszczył jej przyjaciół; myśli że to jej wina" - "Magia MUSI być kontrolowana. Wszystko co anomalne musi być reglamentowane."
    * Styl: Cicha, pełna wewnętrznego ognia. Skupia się na dekontaminacji z żarliwością godną faerilki.
    * metakultura: Faeril: "ciężką pracą da się rozwiązać każdy problem - to tylko kwestia cierpliwości i determinacji"
* Irek Korniczew: główny inżynier wysłany przez Flarę by zająć się obsługą bazy tymczasowej
    * OCEAN: (C+O+): tysiące drobnych pomysłów i usprawnień, poszukiwanie nowego i wszystko przy poszanowaniu istniejącej maszyny. "Wszystko da się usprawnić i zrobić lepiej. Dojdziemy do tego jak."
    * VALS: (Stimulation, Achievement): coś nowego, coś ciekawego, coś rozwijającego. A nie gówno na patyku. "Te drobne powtarzające się błędy powinna rozwiązać TAI."
    * Core Wound - Lie: "Zawsze inni dostawali wszystko co ciekawe a ja tylko maintenance, mimo że to moja specjalizacja" - "Te wszystkie tematy maintenance to misja karna; nie mogę brać na siebie"
    * Styl: Wiecznie zirytowany, ale pomocny. Narzeka na sprzęt i zadanie. Jednocześnie - jeden z lepszych inżynierów.
    * metakultura: Atarien: "jeśli nie dam rady się wykazać, rozpłynę się w tłumie szarych twarzy"
* Antoni Paklinos: chorąży odpowiedzialny za integrację z ramienia grupy wydzielonej Salvagerów Lohalian
    * OCEAN: (A-C+): niezwykle cierpliwy i nieźle współpracuje z ludźmi, jednocześnie dość agresywny; lubi dawać ludziom szansę i ich testować. "Paklinos to swój chłop; nieco zmierzły i testujący, ale daje radę"
    * VALS: (Security, Benevolence): niezależnie czego nie dostaje i czym się nie zajmuje, kluczowa dla niego jest pomoc innym i zadbanie o ochronę. "On nawet do kibla wchodzi z nożem"
    * Core Wound - Lie: "Zrobiłem to, co należało zrobić i zostałem wygnany na planetę" - "Nic co robimy nie ma znaczenia, ale możemy pomagać w najbliższej okolicy. Od myślenia jest dowództwo."
    * Styl: Dość zaczepny ale pozytywny facet; dużo testuje i zawsze jest 'in your face'. Zaufany i daje radę.
    * metakultura: Atarien: "Oddział jest tyle warty ile jego wytrenowanie i spójność z rozkazami. Co nie jest testowane i szkolone, to się nie dzieje."

OPERACYJKA

* Tomasz Afagrel: kapral grupy wydzielonej pod dowództwem Larnecjata; były przemytnik
    * OCEAN: (E+O+): nie boi się ryzyka i zawsze pierwszy do szydery czy okazji. "Jeśli jest zysk, warto spróbować."
    * VALS: (Hedonism, Power): pierwszy szuka okazji do dorobienia czy dobrej zabawy. Straszny hustler. "Nikt nie dostanie za darmo niczego. Trzeba chcieć i działać."
    * Core Wound - Lie: "Może jestem z bogatego domu, ale wojna zniszczyła wszystko co mieliśmy" - "KAŻDĄ okazję trzeba wykorzystać, WSZYSTKIEGO spróbować - by nie żałować."
    * Styl: Głośny, pełen energii, uśmiech drwiący i szyderczy. Zawsze szuka sposobu na zabawę czy okazję. Lojalny wobec "swoich". Kolekcjonuje fanty.
    * metakultura: Atarien: "moja rodzina miała wszystko i wszystko straciła. Moja kolej odbudowania potęgi rodziny."
* Jarosław Mirelski: szeregowy, ekspert od walki w zwarciu; wygląda jak kupa mięśni o świńskich oczkach
    * OCEAN: (E+C-): skłonny do ryzyka, łatwo się denerwuje, impulsywny, ale też bardzo lojalny. "Z Jarkiem lepiej nie wchodzić w konflikt, chyba że masz ochotę na siniaka."
    * VALS: (Achievement, Tradition): duma z własnych umiejętności w walce. Tradycyjnie patrzy na honor i wyzwania. "Prawdziwi faceci rozwiązują problemy walką, a ich przełożeni ich uruchamiają"
    * Core Wound - Lie: "W wyniku scamu moja rodzina została bez niczego i tylko wojsko było dla mnie nadzieją" - "Pokaż, że warto na ciebie stawiać, bądź świetnym żołnierzem."
    * Styl: Krótki temperament, zawsze gotów do wyzwania. Przeciwników mierzy wzrokiem, ale też ma w sobie pewien urok, który przyciąga innych. Przyjazny dla "plebsu".
    * metakultura: Atarien: "Ludzie są ludźmi. Trzymamy się razem. Każdemu normalsowi warto pomóc."
* Paweł Lawarczak: szeregowy, znany z niezwykłej zręczności i brawurowej gry w karty; advancer
    * OCEAN: (E+N-): Charyzmatyczny, gadatliwy i uwielbia być w centrum uwagi. Działa zanim pomyśli, ryzykant. "Paweł zawsze ma jakąś sztuczkę, żart czy pomysł. Nieważne jak źle jest, Paweł rozchmurzy."
    * VALS: (Hedonism, Security): Żyje dla chwili, uwielbia emocje i ryzyko, ale dba by nie spotkała go krzywda. Małe oszustwa to kolejny sposób na świetną zabawę. "Najlepszy do zabawy, ale go pilnuj..."
    * Core Wound - Lie: "Jako dziecko byłem złodziejem i akrobatą; jak pili to bili" - "Jeśli wszyscy się śmieją i postrzegają mnie za niegroźnego, jestem bezpieczny"
    * Styl: Wiecznie uśmiechnięty, zwinny jak kot, zawsze ma gotowy dowcip na ustach. Ma sztuczki karciane, nic nie bierze na poważnie i ogólnie co się nie stanie - jest radość.
    * metakultura: Atarien: "To są moi ludzie. Tylko mnie wolno ich oszukiwać."
* Maurycy Derwisz: szeregowy, technik i miłośnik historii Harmonii Diakon
    * OCEAN: (N+C+): Nieśmiały, skoncentrowany na badaniach i technologii, cierpliwy cichy i dokładny. "Zna się na rzeczach, o których inni nawet nie słyszeli. Ale musisz się dobrze przyjrzeć, żeby go zauważyć."
    * VALS: (Achievement, Self-Direction): Zbiera wiedzę o Harmonii Diakon; nieskończenie układający 'puzzle' jej życia. "Tajemnice Harmonii to wyzwanie które przezwyciężę."
    * Core Wound - Lie: "Jego rodzina musiała uciekać z Aurum z uwagi na powiązanie z Harmonią Diakon" - "Jeśli zrozumiem o co chodziło z Harmonią, zrozumiem sekret mojej rodziny"
    * Styl: Praktyczne ubrania, do swobodnej pracy z różnymi urządzeniami i dyskrecji. Mimo nieśmiałości wobec kobiet, jeden z lepszych strzelców i ekspertów od sprzętu. Przystojny chłopaczek.

#### 2. Siły Samszarów

* Cyprian Mirisztalnik: chorąży Aurum, kompetentny w walce i wierny towarzysz Eleny w świecie Orbitera
    * OCEAN (E+ A-): pełen życiowej energii i charyzmy, opiekuńczy ALE arogancki i roszczeniowy. "Kto ma moc, ma obowiązek. Kto ma moc, tego słuchają."
    * VALS: (Conformity, Tradition): głęboko wierzy w znaczenie hierarchii i lojalności. Wierzy też w odpowiednie wyglądanie i zachowywanie się. "Krew mówi sama za siebie."
    * Core Wound - Lie: "Nie będąc arystokratą, wszyscy mnie ignorowali" - "Zapewnię odpowiednią opiekę i kontrolę nad innymi, dzięki czemu mnie nie odrzucą i nie zostawią."
    * Styl: Opiekuńczy i dominujący jednocześnie, kłótliwy ale elegancki. Patrzy na świat przez pryzmat hierarchii i formalnej kontroli.
* Adelajda Kalmiris
    * Core Wound - Lie: "tylko mnie udało się uciec i tylko mnie udało się rodowi Eleny uratować." - "ród Samszar jest inny - to jedyny szlachetny, dobry ród; oni nie pozwalają na zło"

### Scena Zero - impl

.

### Sesja Właściwa - impl

4 dni później. Wielka feta i żywność. Dużo roślinnych smacznych dań. Nawet migające światła Lohalian nie bolą tak bardzo. Ogólnie - ludzie się bawią, mimo strat w jedzeniu. Opowiadają, po raz pierwszy się CIESZĄ. Jest odrobina radości. No - poza lokalną czarodziejką (Rachela). Kapitan KAZAŁ jej przyjść na imprezę więc przyszła. Ale się nie socjalizuje, siedzi z boku i je smętnie glony. Taka... czarna magiczna chmura.

Żołnierze wygłaszają przemowy. Zwykle idiotyczne. Grają, żartują, jedzą - normalna cisza przed burzą. Kapitan zaznaczył, że to dzięki tien porucznik Samszar mogą pojeść. Spojrzał wymownie na Estellę. Po Estelli spływa to jak po kaczce. Kapitan lekko wzruszył ramionami i zarzucił dalszą rywalizację między tienkami. Warto było spróbować.

Tym samym, Elena stała się nie-całkowicie-niepopularną osobą.

Elena zobaczyła jak żołnierz i Cyprian stają.

* Cyprian: "Powiedz to jeszcze raz, a wyzwę Cię na honorowy pojedynek"
* Losowy Żołnierz: "Powiedziałem, że Twoja ukochana tienka nie umie obierać ziemniaków!"
* Cyprian: "JAK ŚMIESZ!"
* Losowy Żołnierz: "No są fakty. Mamy zdjęcie oprawione w ramce tego ziemniaka!"
* (rechot)
* Cyprian: "STAŃ I WALCZ!"
* Elena: "CYPRIAN! JA NIE UMIEM OBIERAĆ ZIEMNIAKÓW! TO STRASZNA PRAWDA JAKIEJ NIE UKRYJEMY!"
* Losowy Żołnierz: "No widzisz Cyprian? Nawet Twoja tienka to rozumie. Ty też zrozum. Siad."
* Estella: (obejmuje Cypriana ramieniem) "Chodź"
* (gwizdy) i "mnie też dotknij mała!" na co Estella "jakbyś się tydzień mył to może!"
* Cyprian: "tien chorąża Gwozdnik?"
* Estella: "tu jestem chorąża. Po drugie, musisz się tego oduczyć. Naprawdę. Jeśli nie chcesz zrobić swojej pracodawczyni utrudniać życia, graj jak ona. A ona chce być członkiem zespołu. Twoje hurdur jej nie pomoże."
* Cyprian: "tien chorąża Gwozdnik. Słucham poleceń JEDYNIE tien porucznik Eleny Samszar. Przyjąłem radę."
* Estella: "jeśli wolisz usłyszeć ją z jej ust, proszę bardzo"
* Es -> El: (hiper) "Elena? (przekazanie co powiedziała), Cyprian nie uznaje porad od byle chorążych, sama musisz to powiedzieć, nadmuchał się okropnie jak ropucha i chciałabym szpilką, ale mi się nie chce. Wyjmij mu kij z dupy."

Cyprian, elegancko stoi, wyraźnie widać te kocie ruchy wojownika. Cyprian WYRAŹNIE jest szybszy i silniejszy niż żołnierz Orbitera. Odżywiony, wyszkolony do walki, plus nie jest próżniowcem. Ale już jeden podchodzi do niego i to z oddziału Eleny. Jarek. Koleś o twarzy wieprza. Żołnierze się ożywiają.

* Elena: (ręka na ramieniu Cypriana) "Nie umiem obierać ziemniaków. Skoro chcecie pojedynku, wybiorę broń. Broń, która nas poróżniła. OBIERACZKA! Kto szybciej i ładniej obierze! Cyprianie, możesz uratować mój honor obierając ziemniaki. Jarek, wierzę, że będziesz godnym przeciwnikiem!" i do sali "O-BIE-RA-CZKA-ZIE-MNIA-KI!" i Estella się włącza. Adelajda się włącza. "Ale bym pooglądała :>"

Widząc na twarzy Jarka absolutne zniesmaczenie i głęboki smutek na twarzy Cypriana podchwycili.

Tp+3:

* X: Jarek i Cyprian tego tak nie zostawią. Oni to muszą PO MĘSKU rozwiązać. Cicho.
* V: Presja społeczna działa. Ku ich wielkiej niechęci, pojedynek na ziemniory. JAROMIR KUCHARZ przyniósł dwa wielkie wory. Jarek "ja pierdolę...". Cyprian "ale tien porucznik..."
    * a Adelajda na całym regulatorze, jeden guziczek mniej, ZIE!MNIA!KI! jakby była na disco.
* X: część ziemniaków jest pognita i kiepska. To smutne przypomnienie Interis. Udało się zdeeskalować, ale nie ma efektu morale który powinien być.
* X: Jarek i Cyprian będą obiektami śmiechu żołnierzy. To prowadzi do ich zapiekłej rywalizacji.
* Vr: histeryczna zabawa, krótkie wytchnienie od Interis, ryk sprawił, że aż kapitan wyszedł. Paweł zrobił DJ PABLO. Adelajda mu pomaga. Zrobili KOCIOŁ.

PULA ZIEMNIAKÓW: 8V (Cyprian) 12X (Jarek). 2Vr 2Xr -> za krew i poświęcenie. (len: 3)

* Vr: Cyprian się zaciął, skaleczył, on NIE DBA. Jest zadanie - wykona. "Tien porucznik życzyła sobie obrony honoru - obronię!"
    * gwizdy z sali, ale mimo "zakochał się..." jest jakiś szacunek.
* V: Cyprian robi ziemniaki szybko i sprawnie. "nie jestem tienem, ja wiem czym jest praca. Jestem w rolnictwie od zawsze! Wy jesteście z kosmosu, wy nie wiecie co to DOBRY ZIEMNIAK!"
    * Jarek na początku zlekceważył i nagle ma przed sobą dużo do nadrobienia (+1Xr)
* V: Jarek dobrze obiera. Ale Cyprian - eleganciutki, w nieskazitelnym mundurze, pobrudził się, obświnił, ale OBRAŁ. "prawdziwe rozkazy pochodzą od tien porucznik. Ona jest godna Waszego szacunku, czego nie rozumiecie!"

Jak się zacina i świni, Estella krąży dookoła i rzuca hasła z gatunku mającego im pokazać że za arogancją jest też kompetencja i prawdziwe oddanie. Ma kij w dupie ale ten kij może wyjąć i nim walczyć, choć śmierdzi.

Tr Z (akcja) +3:

* X: Cyprian nie jest JEDNYM Z NICH i zdecydowanie PODKOCHUJE SIĘ W ELENIE.
* Vr: jest taki lekki szacunek do kompetencji i oddania.
* V: "nie drażnimy go, on po prostu TAKI JEST, zakochany oddany pies" w ogólnym rozumieniu

Pokpiwania z Jarka, Cyprian któremu Adelajda podniosła rękę (zakrwawioną) i zrobiła odpowiednią siarę, jeszcze bardziej rozśmieszając ludzi. Jarek wstał i się oddalił. Jest WŚCIEKŁY na Elenę, Cypriana i ekipę. 

* Elena: "Cyprianie, bardzo dobrze sobie poradziłeś."
* Cyprian: "tien porucznik, chronię Twój honor jak zażądałaś. Nie mogłem przegrać!"
* Elena: "Jestem Ci bardzo wdzięczna za Twoją postawę. Przy okazji proszę Cię, wyciągnij kij z dupy (ale powiedziane ładniej)"
* Elena: "Chcemy nawiązać dobre znajomości, bo problem Interis. Konieczność współpracy z Orbiterem tutaj. Gdy się rzucasz na rzeczy jak żołnierskie żarty z ziemniaków... a musimy współpracować. Umówmy się - jak zagrożona / urażona, dam Ci znać. Co Ty na to?"
* Cyprian: "Twoje życzenie jest moim rozkazem, tien porucznik. (wyraźnie niezadowolony) Czyli jak rozumiem mam... ich ignorować."
* Elena: "Śmieszne przyczepki? Ignoruj. Proszę. Może Ci się uda z nimi zakolegować lekko. To nie rozkaz, to... cicha nadzieja"
* Cyprian: "(bardzo ciężkie westchnięcie) Tien porucznik... wykonam polecenie. Ale wiedz, że czuję ból fizyczny i psychiczny. Ty wśród takich małpoludów..."
* Elena: "Nie patrz na nich jak na małpoludów a jak na ludzi. Miej ludzkie podejście."
* Cyprian: "tien porucznik, będę próbował patrzeć na nich jak na inteligentne istoty ludzkie, nieważne jak trudnym by to nie było"

Adelajda złapała Elenę.

* Adelajda: "tien porucznik, czy mogę na słówko?"
* Adelajda: "wielu żołnierzy jest samotnych. Nie chodzi mi tylko o partnerów romantycznych a po prostu o samotność. Kapitan powinien się tym zająć. Nie zajął się."
* Elena: "nie mają rodzin?"
* Adelajda: "nie mają kontaktu z orbitą. Są odcięci. Interis."
* Elena: "dzięki. A coś więcej wiesz lub proponujesz? Kto ma rodziny, kto nie..."

Elena idzie porozmawiać z kapitanem Larnecjatem. Mówi o głębokiej samotności i niepotrzebnych waśniach. O tym, że trzeba coś z tym zrobić. Kapitan uśmiechnął się do ELeny. W tle - muzyka klasyczna.

* Larnecjat: "Tien porucznik, czy masz jakieś propozycje?"
* Elena: "Mam! Trzy rodzaje rozrywek! Kotki, pieski, gejsze."
    * Elena: Kotki i pieski dla osób które chcą poprzytulać zwierzątka, samotność, by MIELI PO CO ŻYĆ.
    * Larnecjat: Utrzymanie kotów i psów będzie kosztowne i niebezpieczne na tym terenie. Dla nich. Mamy nie w pełni przejęty wrak statku kosmicznego. Nie wiem czy zapewnimy im bezpieczeństwo jak się rozbiegną.
    * Elena: Pacyfikatory i Verminusy?
    * Larnecjat: Nie rozbiegną się w śnieg? Poradzą sobie tutaj? Nieczęsto... warto spróbować, ale ktoś musi się nimi opiekować.
    * Elena: Oraz 2-3 wolontariuszki od kotów które umieją się obronić :-)
    * Larnecjat: Zgadzam się - jeżeli weźmiesz odpowiedzialność za nich. Nie mogę tego zrobić.
    * Elena: Ma pan inny pomysł na morale?
    * Larnecjat: Morale mają wystarczające by wykonać zadanie. Nie powiedziałem, że mnie to CIESZY w jakiej są sytuacji.

Elenie zostało zadzwonić do Samszarów. Wybrała Aleksandra Samszara - on ROZUMIE na czym polega problem i zna się na wojskowości.

* Aleksander: Tien Eleno?
* Elena: Tien Aleksandrze?
* Elena: Potrzebujemy dziewczyny, koty, psy i verminusy
* Aleksander: (uniesiona brew)
* Elena: (wyjaśnia Interis i problem z morale - stąd dziewczyny psy i koty, tam opieka. A dziewczyny przeszkolone w walce.)

Aleksander wyciąga z Eleny krok po kroku wszystko co ważne i istotne. Pomaga Elenie sformułować jej własny _case_. Pokazuje, jak należy zrobić zapytanie do Rady. Aleksander pomaga Elenie sformułować request do rodu. I powiedział, że będzie sponsorował ten request w Radzie.

Elena -> Estella o rekomendację. Tłumaczy o Karolinusie, że jest skończonym chujem i najebał. Skoro może pomóc - pomoże. 

Elena chce dostać co następuje:

* wyszkolone Pacyfikatory które nie polegną do Interis
* zwierzątka od morale do przytulania
* kilka dziewczyn, które umieją walczyć i chcą przygód wśród żołnierzy
* utrzymania dostaw żywności i innych takich rzeczy
* Elena dostaje wotum zaufania

Ex: -nie ufają jej, -nie chcą płacić kasy. +Aleksander. +Elena ma rekomendację innej tienki (Estelli), +Samszarowie mogą zyskać na współpracy z Orbiterem reputacyjnie, +Elena przekonała kapitana do rekomendacji, +Elena jest do odratowania

Tr Z+4:

* V: utrzymanie żywności i tego co jest
* V: wotum zaufania Samszarów do Eleny ("klątwa Karolinusa" jest zdjęta)
* X: więżniowie: nie wszystkie dziewczyny które trafią MIAŁY wybór. Politycznie dostały się też osoby 'niewłaściwe' których chcieli się pozbyć.
* XX: Rada na tym etapie nic więcej nie ma. Niech Elena sobie poradzi i GDY SOBIE PORADZI może wrócić. Na razie - więcej nie dostanie z Rodu. Już dużo dostała.

TBC

## Streszczenie



## Progresja

.

## Zasługi

.

* Elena Samszar: .
* Estella Gwozdnik: .
* Jarosław Mirelski: .
* Cyprian Mirisztalnik: .
* Adelajda Kalmiris: .
* Mikołaj Larnecjat: .

* Tomasz Afagrel: zaprojektował plan jak obejrzeć nieubrane tienki - gra w strip poker, Maurycy wprowadzi sprawę, Paweł będzie oszukiwał. Tylko, że Elena oszukiwała lepiej (magią). Potraktował sprawę z humorem.
* Paweł Lawarczak: świetnie oszukuje w karty (acz Elena lepiej) i robi 'sztuczki magiczne'. Najweselszy z zespołu. Przyjął przegraną z pokorą i szerokim uśmiechem.
* Maurycy Derwisz: prześliczny chłopiec, wstydzi się rozmowy z Eleną. Wciągnął ją do gry w karty na polecenie Tomasza. Też na polecenie zaproponował grę w strip poker. 
* Jaromir Gaburon: kucharz Lohaliana; ponury i zrezygnowany wielki brodaty facet. Wysyła Elenę, Cypriana i Elenę do obierania ziemniaków, pokazał Elenie składzik ziemniaków i już NIGDY nie da Elenie ziemniaka do obierania.


## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, W
                    1. Góry Hallarmeng
                        1. Przyprzelotyk
                            1. Korony Cmentarne
                                1. Ruina Lohalian
                                1. Dolina Krosadasza

## Czas

* Opóźnienie: 1
* Dni: 2
