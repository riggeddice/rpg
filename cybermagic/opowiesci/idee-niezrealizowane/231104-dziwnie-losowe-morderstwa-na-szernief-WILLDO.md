## Metadane

* title: "Dziwnie losowe morderstwa na Szernief"
* threads: agencja-lux-umbrarum, problemy-con-szernief
* motives: glupota-i-bezmyslnosc, jestem-ponad-prawem, niszczycielski-efekt-motyla, starcia-kultur, manifestacja-ducha, koniecznosc-pelnej-dyskrecji, 
* gm: żółw
* players: kić, yobolyin

## Kontynuacja
### Kampanijna

* [230923 - Ciemność pożerająca Arcadalian](230923-ciemnosc-pozerajaca-arcadalian)

### Chronologiczna

* [230923 - Ciemność pożerająca Arcadalian](230923-ciemnosc-pozerajaca-arcadalian)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * Metric - "For Kicks"
        * "Why'd I have to be such trouble to please? | It wouldn't be me, oh if it was easy, if it was easy | Such trouble to please | It wouldn't be easy, if it was me"
        * "Why'd I have to go and break what can't be fixed?"
* Opowieść o (Theme and vision): 
    * "Głupie prostesty i głupie ruchy w miejscu o podniesionym polu magicznym kończą się katastrofalnie"
    * CON Szernief z przyczyn finansowych (Savaranie) sprowadza wyznawców Adaptacji (Saitaera) bo oni płacą dużo i chcą mieszkać na uboczu.
        * Głupie protesty przed tym doprowadziły do śmierci młodej kobiety.
        * Manifestacja kobiety się mści i będzie się mściła póki to się nie skończy.
    * SUKCES
        * Exoneration tych, którzy siedzą 'za niewinność'
        * Rozwiązanie problemu ducha. Egzorcyzm? Wysiedlenie eks-protestujących by byli poza zasięgiem ducha?
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * glupota-i-bezmyslnosc: protesty młodych którzy nigdy nie musieli pracować doprowadziły do niepotrzebnej śmierci i utrudnień CON co przyspieszyło degenerację
    * jestem-ponad-prawem: młodzi protestujący nie tylko nie czują się winni - biurokracja ukrywa ich działania
    * niszczycielski-efekt-motyla: protest -> śmierć -> konieczność zwiększenia przychodów -> więcej saitaerowców -> pole magiczne -> duch -> morderstwa -> eskalacja...
    * starcia-kultur: inwestorzy szukający RoI KONTRA savaranie i oszczędność KONTRA kultyści adaptacji płacący i szukający kultu KONTRA młodziaki inwestorów które nie splamiły się pracą ale chcą coś zrobić
    * manifestacja-ducha: duch martwej młodej matki która 'pragnie' zemsty na tych którzy doprowadzili do jej śmierci
    * koniecznosc-pelnej-dyskrecji: ukrywamy informację o magii przed szerszą publiką; modus operandi Lux Umbrarum
* Detale
    * Crisis source: 
        * HEART: "sin (grzech): doprowadzenie do śmierci młodej kobiety i ukrycie tego pod płaszczykiem biurokracji"
        * VISIBLE: "pozornie losowe morderstwa osób powiązanych z tym przez osoby blisko tej trasy"
    * Delta future
        * DARK FUTURE:
            * chain 1: ZADOŚĆUCZYNIENIE: mordercy są winni i przekonani o swej winie. W oczach prawa i w oczach publiki.
            * chain 2: MORDERSTWA: wszyscy protestujący i kilka innych osób wymrze. Duch rośnie w siłę, będzie zabijał dalej.
            * chain 3: PRAWDA: wiedza o magii wyjdzie na jaw, konieczność aplikowania masowych amnestyków.
            * chain 4: STARCIA: konflikty między grupami (inwestorzy - savaranie - kultyści) będą eskalowały.
        * LIGHT FUTURE: "Protestujący 'wyemigrują', duch wygaśnie, starcia osłabione 'to nie ich wina', mordercy dostaną szansę i sztuczne wyjaśnienie"
    * Dilemma: co z Mordercami? Co z Protestującymi? Jak to uzasadnić czy naprawić?

### Co się stało i co wiemy

* Przeszłość
    * CON Szernief została zaprojektowana jako stacja sprzedająca Irianium. Jest to miejsce o podniesionej ochronności elmag (lokalizacja: przy Sebirialis).
    * Okazuje się, że populacja która zainteresowała się tymi warunkami to byli Savaranie.
        * I inwestorzy są w klopsie. Dla savaran nadmiar energii to radość. Nie chcą kupować seks-androidów ani niczego. Pełna oszczędność.
            * Savaranie to NAJGORZEJ z perspektywy korporacji i inwestycji XD
        * Inwestorzy żądają zwrotu
    * Zarządcy CON Szernief szukają kogoś kto będzie tam chciał żyć i najlepiej za to jeszcze zapłaci
        * Stanęło na ściągnięciu Kultystów Adaptacji. Ale oni nie są jacyś super niebezpieczni, sami tak mówią ;-).
    * W wyniku tego zaczęły się protesty błękitnej młodzieży (dzieciaki Inwestorów), która po nudzie związanej z SAVARANAMI ma jeszcze mieć GŁUPICH KULTYSTÓW.
        * Protesty to m.in. blokowanie ważnej infrastruktury transportowej, wandalizm itp.
        * W wyniku protestów w karetce umarła młoda kobieta w ciąży. Nazwijmy ją Anissa.
        * Nikt z tego powodu nie ucierpiał. Młodzi są 'bezpieczni'.
    * Kultyści zamieszkali na CON Szernief.
* Teraźniejszość
    * W wyniku zmian pola magicznego (savaranie: oszczędność, kultyści: Adaptacja > Optymalizacja jako aspekt) doszło do polaryzacji umożliwiającej pojawienie się Ducha
        * bo dla ludzi śmierć Anissy było formatywne, do dzisiaj przychodzą tam jako do 'symbolu tego co złego się stało' i 'symbolu głupiej błękitnej młodzieży'
    * Duch opętuje randomowych ludzi by eksterminować 'błękitnych' odpowiedzialnych za jej śmierć
    * Pierwszą opętaną osobą był mąż Anissy, którego posadzili do więzienia. Oczywiście.
    * Jako, że ilość MORDERSTW dramatycznie wzrosła i nikt nie wie czemu, Lux Umbrarum to chce sprawdzić

### Co się stanie (what will happen)

,

* F0: _Pierwsza akcja górnicza_
    * INSTANCES
        * Juliusz i Łucjan się żrą, Prokop próbuje deeskalować, widać kto tu rządzi
        * Gotard 'nie potrzebujemy tyle płacić za Waszą ochronę', Kazimierz 'płacicie tanio i cieszcie się że tak tanio'
            * Znowu nie działają działka. Nie da się ich zamontować na Królowej? Coś jest jak zawsze nie tak
        * Jakaś akcja że sprzęt się popsuje i zaraz górnik będzie ranny (co zrobi Królowa?)
* F1: _Podejrzanie dobry asteroid_
    * stakes:
        * czy Królowa pozostanie w ukryciu czy będzie musiała zabić ludzi?
        * czy ktoś poza szkieletową załogą PŁG wróci bezpiecznie?
        * czy uda się pozyskać jakąś kasę? czy uda się NIE wziąć jajek NieGórnika?
    * opponent: 
        * NieGórnik. Nie jest prawidłowo postrzegany; każdy widzi go jako tego kim powinien być
            * potwór musi pożreć kilka osób, a potem chce wrócić z ekipą
        * Blakvelowcy. Wierzą w swoją percepcję, nie do końca rozumieją co się tu dzieje
    * problem:
        * wszyscy myślą, że górników było N-1, a było N; tylko Miranda postrzega prawdę
        * Miranda musi pozostać w ukryciu
    * koniec fazy:
        * zostajemy / wracamy (z potworem / bez)
    * INSTANCES
        * Mikołaj i Bartek kłócą się o plugawość Królowej ('organiczna papka do jedzenia'). Seweryn broni Królowej. Łucjan ma wątpliwości.
        * przebudzenie NieGórnika, Miranda zauważa, że jednej osoby nie ma a widzi inną
            * (tam są dane z tego co się stało z górnikiem; jajko weń weszło)
        * NieGórnik poluje na innego delikwenta i go zabija, nikt nic nie zauważa
        * kłótnie
            * Grzesiek próbuje ukraść więcej kasy dla siebie, szuka co lepszych kawałków
            * Mikołaj zaczyna dramę bez powodu, np. 'Bartek co nam Blakvelowcy skoro możemy mieć kasę'
            * Bartek idzie w stronę konieczności odbudowy i poszerzenia bazy górniczej
            * Prokop próbuje stabilizować, Łucjan unika i robi swoje, Gotard 'trzeba było zainwestować więcej, patrz tą planetoidę, nawet sensory dobrze nie czytają'
    * DARK: 
        * chain 1: Dodatkowe, ogromne ofiary śmiertelne (poza 2 osobami)
        * chain 2: wracamy z jajkiem NieGórnika
        * chain 3: Królowa jest wykryta i musi zabić bo ktoś wie
        * chain 4: wracamy z NieGórnikiem
        * chain 5: wracamy bez niczego (kasa)

## Sesja - analiza

### Fiszki

,

Zespół:

* Prokop Umarkon: 
    * rola: kapitan, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: extravert, agreeable (outgoing, enthusiastic, trusting, empathetic)
    * values: power + conformity
    * wants: pragnie zostać KIMŚ, wydobyć się z bagna
* Łucjan Torwold: 
    * rola: administrator / logistyka, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: neurotic, conscientous (very worried, scared of cults, very organized)
    * values: security + conformity
    * wants: przesądny; boi się duchów i kultu. Narzeka. Chce bezpiecznego, prostego życia
* Gotard Kicjusz: 
    * rola: technical guy, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: open, low-neuro, low-consc (extra stable, spontaneous, eccentric)
    * values: self-direction
    * wants: przygód, odzyskać rodzinę
* Seweryn Grzęźlik:
    * rola: psychotronik, blakvelowiec
    * personality: neuro, low-extra, consc (workaholic, obsesyjny, cichy, skłonności do wybuchania, umiłowanie porządku)
    * values: self-direction, self-enhancement
    * wants: zrozumieć sekret Mirandy, Blakvel forever, ciszy i rozwoju

Blakvelowcy i górnicy:

* Grzegorz Fabutownik: górnik w przestrzeni Blakvela
    * OCEAN: E+A- |Zawsze szuka adrenalinki i kasy;; Nie dba o innych, może kraść | VALS: Power, Hedonism | DRIVE: Kasa i wszystkie laski jego
* Mikołaj Resztkowiec: górnik w przestrzeni Blakvela
    * OCEAN: E+N- |Nie kłania się nikomu;; Ciekawski;; Oczytany, ceni wiedzę dla wiedzy| VALS: Hedonism, Achievement| DRIVE: Drama
* Bartek Burbundow: górnik w przestrzeni Blakvela
    * OCEAN: A+O+ |Mało energii;; Kreatywny, tworzy wiecznie coś nowego;; Pomocny, starający się wesprzeć innych| VALS: Face, Tradition| DRIVE: Odbudowa i odnowa
* Juliusz Cieślawok:
    * OCEAN: A-O- |Fatalne żarty;; Antagonizuje;; Ciemięży ludzi| VALS: Stimulation, Universalism | DRIVE: Niska zabawa i ciemiężenie bliźniego
* Kazimierz Zamglis:
    * OCEAN: A-N+ |Żyje w swoim świecie i ma wszystko gdzieś;; Dość agresywny;; Unika hałasu | VALS: Security, Self-Direction | DRIVE: moja korporacja jest najlepsza
* .5 innych górników

#### Strony

,

* Górnicy w Domenie Ukojenia i mały biznes: 
    * CZEGO: zachować niezależność, handel itp. Przetrwać. Zarobić.
    * JAK: sami się zbroją
    * OFERTA: standardowa oferta terenowa, plotki itp.
    * KTOŚ: Elsa Kułak (przełożona, trzyma twardą ręką), Antoni Czuwram (górnik, solidny stateczek i niezły sprzęt - dużo wie), Kamil Kantor (szuka ojca Leona który odkrył sekret Mirnas i zaginął), Bruno Baran (mag eternijski i założyciel; Strachy to destabilizacja Blakvela przy obecności lapizytu), Kara Szamun (młódka z rodziny Szamun, pilot i nawigator transportowca na Asimear.)
* Przemytnicy Blakvela: 
    * CZEGO: zdobyć prymat w Domenie Ukojenia
    * JAK: zmiażdżyć Mardiusa, potem przejąć Talio i zestrzelić bazę Barana.
    * OFERTA: świetne rozeznanie w pasie, największa siła ognia, wiedza o SWOICH skrytkach, wsparcie piratów etnomempleksu Fareil (synthesis oathbound pro-virt, pro-numerologia)
    * KTOŚ: Ernest Blakvel (arystokrata eternijski)
* Przemytnicy Mardiusa: 
    * CZEGO: zregenerować siły, odzyskać Tamarę, ukryć się przed Blakvelem
    * JAK: w ciągłej defensywie, ufortyfikowanie Sarnin, operacje militarne anty-Blakvel
    * OFERTA: wiedza o Miragencie i Aleksandrii, świetne rozeznanie w pasie, wiedza o SWOICH skrytkach, znajomości wśród lokalnych.
    * KTOŚ: Tamara Mardius (eks-komandos noctis, Alexandria), Deneb Ira (regeneruje komandosów po sprawie z Aleksandrią)
* Taliaci: (mieszkańcy Talio)
    * CZEGO: rzeczy wysokotechnologiczne
    * JAK: handel, współpraca
    * OFERTA: mają dostęp do WSZYSTKICH frakcji i są wiecznie neutralni, reaktory termojądrowe na podstawie deuteru i trytu na Talio.
* Strachy
    * CZEGO: Brak woli. Reakcja. Zniszczyć wszystko.
    * JAK: flota inwazyjna
    * OFERTA: zniszczenie, targetowane lub nie.

### Scena Zero - impl

.

### Sesja Właściwa - impl

.

## Streszczenie

.

## Progresja

* .

## Zasługi

* Salma Bluszcz: 
* Iwo Bretonis: 

.

* Salma Bluszcz: Agentka Lux Umbrarum, advancer; uspokajała kapitanów obu jednostek bo 'nie wiemy z czym mamy do czynienia'. Żądała uratowania wszystkich. Opracowała sposób rozwiązania problemów z Umbrą przez wprowadzenie światła wszędzie.
* Iwo Bretonis: Agent Lux Umbrarum, inżynier bojowy; kluczowy w negocjacjach i jak tylko Umbra przejęła TAI Arcadaliana to ewakuował ludzi używając dron i kazał zniszczyć AK Arcadalian.
* Lester Martz: koordynator Agencji Lux Umbrarum, wygląda jak mumia; przesyła zespół Agentów do akcji i zapewnia im odpowiednie uprawnienia i wsparcie logistyczne. Dał też kontekst.
* Livia Sertiano: kapitan Arcadaliana; kiedyś naukowiec, szybko zorientowała się, że coś się tu dzieje dziwnego i staje po stronie Agencji. Nie zrobiła żadnych poważnych błędów, po prostu nie wiedziała o magii i dała zainfekować Arcadalian przez Zakon. 

* OLU Luminarius: jednostka Agencji Lux Umbrarum; szybka korweta fabrykacyjna i badawcza, 17 osób załogi. Tym razem - rozwiązuje problem statków dotkniętych Umbrą przez Zakon i magię.

## Frakcje

.

* Agencja Lux Umbrarum: przechwycili agenta Zakonu, którego Umbra zmieniła w istotę cienia przy okazji ratowania Arcadalian itp. Misja bez zarzutu.
* Zakon Nocnej Prawdy: grupa niezależnych kultystów-uciekinierów wbiła się na Arcadalian i wprowadziła Umbrę. Zostali pokonani przez Lux Umbrarum.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Kalmantis: noktiański sektor pełny dobrobytu, gdzie nie ma może żadnej dobrej planety gdzie da się mieszkać, ale wykorzystywane są arkologie i CON.
            1. Sebirialis: planeta typu Merkurego, eksporter Irianium; dość niezbadana.
                1. CON Szernief: 

## Czas

* Opóźnienie: 13
* Dni: 8
