## Metadane

* title: "Syrenopajęczak politycznie skomplikowany"
* threads: mscizab-rywal-arianny
* motives: the-rival, the-hunter, gry-polityczne, local-team-decent, aurum-wasn-wewnatrzrodowa
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [210306 - Wiktoriata](210306-wiktoriata)

### Chronologiczna

* [210311 - Studenci u Verlenów](210311-studenci-u-verlenow)

## Plan sesji
### Projekt Wizji Sesji

Sesja powstała jako Scena Zero NASTĘPNEJ sesji (ale się fajnie rozwinęła), więc ma bardzo ograniczony zestaw Motywów i Inspiracji.

* Opowieść o (Theme and vision):
    * "Dziecko, nigdy dość dobre, naciśnięte za mocno, próbuje wygrać szacunek i miłość. Ale nie daje rady."
    * "The child who receives no warmth will burn the village just to feel its warmth"
    * Lokalizacja: Verlenland
* Po co ta sesja?
    * Jak wzbogaca świat
        * Pokaże politykę Verlenlandu
        * Pokaże pomocnych Verlenów, którzy chcą pomóc - i tych, którzy trzymają się okrutnych reguł Verlenlandu, starej daty.
    * Czemu jest ciekawa?
        * Pokazuje Ariannie i Viorice ich własne lustro
    * Jakie emocje?
        * "Mściząb jest dalej głupi, ale w miarę spoko się rozwinął"
        * "The guy simply can't win, can he?"
        * "Zachowujesz się jak dziecko..."
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * the-rival: Mściząb jest tym dla Arianny. Chce być lepszy od niej za wszelką cenę, nawet, jeśli straci ludzi. Jest zaślepiony rywalizacją i byciem lepszym.
    * the-hunter: klasyczny pajęczo-syrenowy drapieżnik w Verlenlandzie w jaskiniach Dun Latarnis. Z jednej strony ma thrill of the hunt, z drugiej chce przetrwać i się najeść.
    * gry-polityczne: mimo, że Mściząb chce pomóc Ślicznospadowi, ten wzywa Ariannę. Bo jeśli Mściząb pomoże, jest duże ryzyko, że Dun Latarnis wejdzie w inną strefę wpływów.
    * local-team-decent: Dun Latarnis jest dobrze zorganizowane, ma dobrą ekipę i sytuacja jest - po konsultacji z Mścizębem - opanowana i pod kontrolą. Czas rozwalić potwora.
    * aurum-wasn-wewnatrzrodowa: Verlen-on-Verlen violence. Normalni ludzie rozmawiają, Verlenowie idą na ostrza, WIĘC wezwano Mścizęba a POTEM Ariannę by Arianna wygrała.
* O co grają Gracze?
    * Sukces:
        * To Scena Zero. Nieopracowane.
    * Porażka: 
        * To Scena Zero. Nieopracowane.
* O co gra MG?
    * Highlevel
        * Chcę pokazać im różne strony Verlenów.
    * Co chcę uzyskać fabularnie
        * Mściząb jest podle potraktowany przez politykę.
        * Mściząb jest kompetentny, ale nadal zapalony przeciw Ariannie.
* Default Future
    * ?
* Dilemma: Pogłaskać Mścizęba CZY zrobić dobry ruch polityczny?
* Crisis source: 
    * HEART: "Problemy polityczne Ślicznospada"
    * VISIBLE: "Mściząb nie zniszczył potwora"
        
### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

.

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

To ta sesja XD:

* Scena Zero (30 min)
    * jakieś pół roku temu, stalkują potwora
    * jest tam Mściząb, kompetentny
        * tracking - trapping - executing
    * -> pokaż bezduszność Mścizęba
        * piękny, elegancki, ale wykorzystuje podkochującą dziewczynę jako bait
    * -> pokaż bezwzględność Mścizęba
        * ludzie jako bait, gotowy do zadawania ran
    * -> pokaż drive Mścizęba
        * ważniejszy sukces niż żeby A+V zniszczyły potwora
    * -> pokaż że jest groźny i kompetentny

## Sesja - analiza
### Mapa Frakcji

.

### Potwory i blokady

* Podły Pajęczak Syrenowy
    * akcje: "wstrzykuję śluz", "rozkładam sieć", "punktowo niszczę pancerz śluzem", "przyzywanie ofiary śpiewem"
    * siły: "silny i żrący śluz", "niezwykle wytrzymała sieć", "labirynt korytarzy"
    * defensywy: "ekstremalna odporność toksyczna", "labirynt korytarzy", "nieruchomy jest niewidoczny"
    * słabości: "wrażliwy na wysokie temperatury", "wszystko jest łatwopalne i wybucha"
    * zachowania: "rozpinanie pułapek", "unieruchamianie ofiar", "przyzywanie ofiary śpiewem"

### Fiszki

* Ślicznospad Verlen: tien Dun Latarnis; 31-letni elegancki miłośnik sztuki i kompetentny administrator. Elegancik, ale dobrze walczy. Pattern: (perfumowany arystokrata)
* Tiana Grolmik: podkochuje się w Mścizębie i zostaje przynętą; lojalna i naiwnie odważna. Pattern: (młoda Asuka Langley)
* Mściząb Verlen: Chce być Verlenem bojowym, chce wygrywać, nie do końca dba o ludzi. Ważne jest zwycięstwo.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Arianna dostała prośbę. Od Ślicznospada Verlena, który jest tienem Dun Latarnis. Ma problem z potworem i czy Arianna i Viorika mogą pomóc. Dyskretnie. Próbował sobie z potworem poradzić samemu, ale potwór zabija ludzi a Ślicznospadowi na jego ludziach zależy.

* Ślicznospad umie walczyć. Ale on jest dobrym administratorem. On nie lubi tracić ludzi. On dba o swoich ludzi.
* Dun Latarnis jest miejscem robiącym śpiwory i tekstylia... 1300 osób. Zwykłe miejsce. Ale Ślicznospad dba o swoich ludzi.
* Potwór sprawia, że ludzie wchodzą do jaskiń i znikają. To podobno jakaś forma Syreny.

Viorika dostała pozytywny sygnał z sentisieci jak się zbliżyła do miasta - obie są mile widziane. Dotarły do Dun Latarnis. Wtedy Viorice odbija się w sentisieci silna negatywna emocja, która zanika (gdy się pojawiły w polu widzenia). I potem - radość innej osoby. Viorika zauważa, że w Dun Latarnis sentisieć jest bardzo czuła na tym terenie, jest dobrze zadbana i ogólnie przyjazna.

Spotyka się z Arianną i Vioriką (dalej: A+V) Ślicznospad.

* Ślicznospad: Tien Arianno, tien Vioriko, jaka to radość móc Was zobaczyć (uśmiech)
* Arianna: Miło być zaproszoną w tak piękną okolicę. Szkoda że są problemy
* Ślicznospad: Są. I... nie chcę tracić ludzi bez sensu

Viorika widzi, że ludzie są dobrze rozmieszczeni - w trójkach, widząc się krzyżowo, obserwując jaskinie itp. Dobra robota.

* Viorika: Widzę, że dopracowałeś rozłożenie żołnierzy.
* Ślicznospad: (uśmiech) To nie ja, to tien Mściząb.
* Arianna: tien Mściząb jest na miejscu?
* Ślicznospad: tak, ale zawiódł. Bo ludzie dalej znikają.
* Arianna: czym się zajmuje?
* Ślicznospad: poluje na potwora (z niezadowoleniem)
* Viorika: rozumiem, że nie upolował?
* Ślicznospad: tak, nie dał rady. Naprawdę. Oczekiwałem więcej.

Ślicznospad jest BARDZO zadowolony z Waszej obecności. Mniej z Mścizęba. I najpewniej od Mścizęba poszło uczucie niezadowolenia w stronę Arianny i Vioriki. Co ciekawe - Mściząb się prawidłowo rozłożył - ma kwaterę, ze swoimi ludźmi, pilnuje by nikt nie zniknął przypadkiem. Mściząb uważa, że jako mag jest w stanie sobie z tym poradzić i na niego ten efekt nie zadziała. Chyba ma rację.

Viorika próbuje ocenić rozkład planu Mścizęba - jak to wygląda.

Tp +2:

* Vr: Mściząb monitoruje wszystkie linie, połączenia, szlaki... nie do końca widzicie JAK ludzie znikają jak Mściząb zrobił taki układ. Może da się, ale jak?
    * wszystkie wyjścia z jaskiń są monitorowane co najmniej przez trzy osoby
    * o dziwo, nie ma sieci sensorów ani zaawansowanego sprzętu; Mściząb zalewa problem większą ilością ludzi

To zwróciło uwagę Arianny.

* Arianna: Ludzie dalej znikają?
* Ślicznospad: Arianno, teraz nie. Ale znikali. Teraz on ich zatrzymuje.
* Arianna: Czyli na razie jego zabezpieczenia są skuteczne?
* Ślicznospad: Może. Aktualna strategia nie jest pokonana. Ale za jego czasów wchodzili do jaskini. Trzech ludzi straciliśmy przez to (nie udaje smutku)
* Arianna: (wygląda jak błędy braku doświadczenia - aktualny system powinien działać).
* Arianna: nie wygląda na działania złośliwe
* Ślicznospad: nie, ale od Verlenów oczekuje się kompetencji...
* Arianna: tak czy inaczej jest postęp wobec tego co robił wcześniej
* Ślicznospad: prawda, ale ludzie giną.

Arianna szuka wiadomości, danych, informacji itp. Skąd i co się dzieje. Jak ludzie znikali. Co tu się działo. Viorika zagłębiła się w sentisieci, Arianna w opiniach żołnierzy i tym co mówią miejscowi.

* X: Mściząb jest w stanie przyjść, wie o ich obecności.

Arianna i Viorika siedzą w centrum i analizują dane, Ślicznospad przynosi im dobry trunek i jedzenie. Ciekawe informacje z danych:

* Zniknęło 7 osób. 4 zanim Ślicznospad wezwał Mścizęba.
* Potem zniknęły 3 osoby za czasów Mścizęba. ALE zniknęłoby 5 kolejnych, tylko systemy ich złapały
* Wyraźnie to atak hipnotyczno-soniczny. Śpiew. Syrena. SYRENA z jaskiń.
* Wyraźnie to atak oportunistyczny. "Kto jest pod ręką koło odpowiedniej dziury".
* Są dziury i jest... "kot na drzewie poluje coś W POBLIŻU". Nie widać mrocznej agendy - naturalny łowca. 
* Czyli jakiś potwór z Verlenlandu, jak wiele. Ale ten jest sprytniejszy.

Mściząb wpada do centrum dowodzenia i od razu z pyskiem, niezadowolony.

* Mściząb: "Co wy tu robicie... co one tu robią?"
* Viorika: Dobra robota z rozstawieniem żołnierzy. (szczera pochwała)
* Mściząb: (zignorował Viorikę)
* Ślicznospad: Ty nie dałeś rady. One mają sukcesy.
* Mściząb: Prawie dorwałem tego potwora.
* Ślicznospad: Nie mogę pozwolić na to, by więcej ludzi zginęło. Plus, każdy dzień kosztuje.
* Mściząb: Musisz zapłacić by mieć bezpieczeństwo
* Ślicznospad: Tien Arianna i tien Viorika są odpowiednio wyposażone i są w stanie zrobić to najpewniej taniej. Szybciej. Z mniejszym ryzykiem dla ludzi.
* Arianna: Patrząc na aktualne zabezpieczenia ryzyko dla ludzi jest minimalne; wstrzymamy się z interwencją (łagodzi sytuację, nie chce wchodzić na odcisk Mścizębowi)
* Ślicznospad: Nie, proszę, nie wstrzymujcie się (lekko przerażony).
* Arianna: Obiekt jest odpowiednio zabezpieczony, ludziom nie grozi szczególne niebezpieczeństwo. Acz potrzebujemy sporo czasu by się rozeznać w sytuacji, Mściząb zrobił odpowiedni zwiad i wie więcej o potworze.
* Ślicznospad: Zwiad był zrobiony moimi ludźmi, dostaniecie wszystkie dane... (wyraźnie widać, że on Mścizębowi nie ufa, nie ufa w jego kompetencje i bardzo chce by to A+V zrobiły)
* Ślicznospad: Tien Mścizębie, masz jakiś plan jak pokonać tego potwora?
* Mściząb: Trzeba tam wejść, wprowadzić przynętę, rozwalić potwora. Tyle.
* Viorika: Co wiesz o potworze?
* Mściząb: (niechętnie) Śpiewa, przyzywa, nie działa na magów. Nie znam kształtu, potrafi poruszać się w małych zakamarkach. A jaskinie tutaj to...
* Mściząb: Tak czy inaczej, mamy dwie możliwości. Poczekamy, aż nie znajdzie nikogo i sobie pójdzie lub tam wejdziemy ryzykując ludzi
* Ślicznospad: Poczekamy? Nie wiemy czy go zniszczymy w ten sposób, tylko zrzucimy problem na inną miejscowość
* Mściząb: To musimy wysłać ludzi. Musisz mieć przynętę.
* Ślicznospad: (błagalnie patrzy na A+V) Musicie mieć lepszy pomysł...

Jaskinie są zamapowane. Łączą się w kompleksy jaskiń. I tak, stwór MOŻE przejść gdzieś indziej. Czyli przeczekanie czy głodzenie potwora nie jest rozwiązaniem. Potwór po prostu pójdzie gdzieś indziej szkodzić komuś innemu. TEŻ na terenie pod kontrolą Ślicznospada.

* Viorika: Zgadzam się, że musimy to jakoś zniszczyć. Przeczekanie jest dobre dla tego miejsca, ale nie dla Verlenlandu.
* Ślicznospad: Dziękuję Ci.
* Mściząb: Nie masz sposobu jak to zrobić nie wysyłając ludzi z przynętą.
* Viorika: Powiedziałeś jedną istotną rzecz. Ta rzecz nie wpływa na magów bardzo, mamy dość siły by sobie z tym poradzić. A przynęta... zawsze znajdzie się ochotnik. Jeśli będzie potrzebna.
* Ślicznospad: Na pewno znajdzie się ochotnik, zgromadzę jakichś ochotników, to Verlenland (to będą prawdziwi ochotnicy, Ślicznospad nie jest osobą źle traktującą ludzi)

Arianna ma plan:

* Trzeba zrobić lukę w zabezpieczeniach
* Trzeba rozmieścić sensory - motion scanners
* Trzeba podpiąć to wszystko do sentisieci
* -> I w ten sposób uda się "wystawić" potwora - będzie tam gdzie go chcecie.

Ślicznospad zapisuje ten plan krok po kroku. 

* Ślicznospad: Arianno, mogę mieć prośbę? Wiem, że zabrzmi dziwnie.
* Arianna: Słucham, możesz...
* Ślicznospad: Z przyczyn... turystycznych i politycznych, możesz udzielić autografu na tym planie? Do muzeum wielkich sukcesów. Gdy się uda.

Mściząb odwrócił na pięcie i wyszedł. Viorika go rozumie. JEJ też nikt nie poprosił o autograf XD. Ale jej to szczególnie nie przeszkadza.

* Arianna: Nie mów, że ściągnąłeś mnie tu by zrobić atrakcję turystyczną z polowania na potwora
* Ślicznospad: Nie poświęciłbym ludzi. Gdyby Mściząb dał radę, to bym Cię nie prosił o pomoc. Ale - jeśli mogę podnieść poziom turyzmu zerowym kosztem i z korzyścią dla regionu i chwały dla Ciebie, kto traci? (uśmiech)
* Ślicznospad: Arianno, jesteś... w idealnym miejscu, że tak powiem. Jesteś młoda, ładna, kompetentna. Zrobisz słodką minkę, trzymasz głowę potwora, plus trzy do turyzmu. A ludzie będą pamiętali i się odwdzięczą.
* Ślicznospad: Jestem pod presją polityczną dołączenia do bardziej... izolacjonistycznych frakcji, jeśli wiesz co mam na myśli. A wprowadzając do muzeum Ciebie pokazuję, że to nie takie proste. A nie jestem zwolennikiem tego, żeby tylko ci silni mogli przetrwać na terenach Verlenlandu.
* Arianna: Sensowne, dostaniesz autograf.

Dlatego jemu się opłaca, by to ARIANNA pomogła. Arianna, nie Viorika. "Sorry Mściząb". Więc - Mściząb powinien być tym, który pomoże, ale to przesunie politycznie ten teren. Gdyby Mściząb pomógł, inne siły polityczne zadziałają. Więc - o dziwo - Mściząb (kompetentny, w odróżnieniu od ostatniej sytuacji) jest ofiarą polityki. Tego by się Arianna nie spodziewała.

Czyli sytuacja - Mściząb nie poradził sobie z ocaleniem wszystkim (nieuczciwie byłoby oczekiwać tego od niego po prawdzie) - ale zrobił dobrą robotę. Potwór nie ma jak atakować.

Skoro WIEMY że Mściząb zrobił dobrą robotę, Viorika ma propozycję - Potwór zauważy, że nie może pozwolić sobie na włażenie w osłony. To nie musi być głęboki intelekt, ale jest to drapieżnik. Zdaniem Vioriki - może prosta pułapka. W **nam** znanym miejscu osłabiamy pokrycie, by mógł wejść i atakujemy. Nie potrzeba wielkiej przynęty. Ale stwór nie wychodzi. Pułapkę trzeba zastawić w środku jaskini. Trzeba tam to zrobić.

To jest byt jaskiniowy, najpewniej mamy do czynienia z pajęczakiem, który porusza się po powierzchni, albo śluzoślimak. Coś co chodzi po wszystkich ścianach. Nie latające. Jest tam potwór, który jest bytem sonicznym. Potwór, który lokalizuje (wzrokiem?) ofiarę, potem z KONKRETNYCH MIEJSC wysyła atak soniczny. Wzrok w jaskini jest mało przydatny. Czyżby on oceniał dźwiękiem? Może da się zrobić efekt "ktoś wszedł do jaskini".

Wszystko wskazuje na JAKĄŚ formę robaka / dużego owada. Co może nosić? Najpewniej niesie ciała i są krople krwi. Nie ma masy ani zawieszenia jak niedźwiedź. Jako, że żaden servar nie został nigdy zaatakowany, najpewniej nie widzi / się boi.

Arianna znajduje jakąś drobno wyglądającą żołnierkę. Requisition: scutier na miarę i za kilka godzin będzie mogła zrobić pułapkę. Ta dziewczyna ma gwiazdki w oczach.

Arianna i Viorika przygotowują plany. Do Arianny przychodzi inna żołnierka. Ta dla odmiany jest wyższa, solidnie zbudowana, umięśniona. Sensowna verlenlandrynka.

* kapral Tiana Grolmik: Tien Verlen, jesteś... osobą ograniczonej moralności.
* (ogólny szok dookoła)
* Arianna: ...słucham?
* Tiana: jesteś, tien Verlen, osobą ograniczonej moralności. Tien Mściząb Verlen PRAWIE dotarł do rozwiązania. A Ty masz zamiar ZNOWU zabrać całą chwałę dla siebie?
* Arianna: skąd wniosek, że robię to znowu?
* Tiana: (myśli chwilę) bo tego typu zachowanie to wzór a nie pojedyncza akcja.
* Arianna: jeśli to wzór w moim zachowaniu, to jedna akcja więcej tego nie zmieni. Nie wiem skąd wysnułaś ten wzór.
* Tiana: (spojrzenie bardzo urażone) Tien Verlen - Mściząb Verlen - jest bardzo wartościowym agentem i Ty niszczysz jego potencjał.
* Arianna: ...Emmanuelle?
* Tiana: słucham, tien Verlen? (wyraźnie nie rozumie). Kapral Tiana Grolmik.
* Arianna: Przepraszam, musiałam Cię z kimś pomylić. Pewnie z dziewczyną która tak bardzo wierzyła w umiejętności Mścizęba że zeszła do jaskini i uwolniła potwory. Lojalność względem przełożonego bardzo się chwali, ale działanie za jego plecami może mieć dla niego i dla Ciebie fatalne konsekwencje. To nie jest groźba. Nie jestem by odbierać mu chwałę. Ale oboje jesteśmy Verlenami. Jestem wezwana do pomocy, nie zamierzam stąd uciekać bo Mścizębowi zrobiło się smutno.
* Tiana: (krew do twarzy) (zacisnęła zęby)
* Arianna: Nie prosiłam o ten przydział, nie wiedziałam że się tu znajduje. Przykro mi że tak wyszło.
* Tiana: Ale zamierzasz, tien Verlen, SAMA rozwiązać ten problem z kuzynką a Mściząb który WSZYSTKO PRZYGOTOWAŁ zostanie w kurzu. Dobrze rozumiem?
* Viorika: Mściząb jest mile widziany jeśli postanowi przytargać tu swój tyłek. Wyparował stąd nie próbując planować ani rozmawiać wspólnie, nie bierze udziału, sam się wykluczył.
* Tiana: Rozumiem, tien Verlen. Spróbuję... doprowadzić do współpracy pomiędzy szanownymi magami rodu. (niechętnie, ale rozumie sytuację)

A+V są w stanie przygotować pułapkę na wieczór, używając tej drobnej agentki (Serry Panirik). Oczywiście, Mściząb się nie pojawił, najpewniej jest emo gdzieś indziej.

Dwie godziny później.

Arianna zlokalizowała Mścizęba w jednym z jego namiotów operacyjnych. Nie jest sam. Arianna idzie do niego.

* Mściząb: ...wtedy przygotujemy... (widzi Ariannę), słucham, tien Verlen, niektórzy z nas są zajęci (Arianna widzi, jak Tiana lekko się krzywi)
* Arianna: Był u mnie od pana posłaniec, sugerując że powinnyśmy zmienić podejście do sytuacji, przemyślałam i stwierdziłam, że możemy wszyscy skorzystać na współpracy
* Mściząb: nie wysyłałem posłańca (Tiana się jeszcze bardziej krzywi - mina typu "nie chciałam spieprzyć a wyszło jak zwykle"). Czyli potrzebujecie mojej pomocy? I prosicie o nią?
* Arianna: Prosimy o Twoją pomoc, Mścizębie. Jesteś na miejscu, Twoi ludzie to obstawiali.
* Mściząb: (SZOK)
* Mściząb: Jeśli... jeśli faktycznie potrzebujecie mojej pomocy i faktycznie dzięki temu eee... dacie radę... nie stracimy nikogo, pomogę Wam.
* Tiana: (wyraźne westchnięcie ulgi)
* Arianna: Może masz lepszy plan, przyda nam się Twoja opinia.
* Mściząb: Dobrze, pójdę z Wami. Zespół, wróćcie do trasy patrolowej, plan zawieszony. (Arianna jest PEWNA że coś realizował XD)

Arianna dostaje po hipernecie "SYTUACJA DELIKATNA NIE OBRAŻAJ MŚCIZĘBA!". Viorika nie rozumie czemu ma go nie obrażać skoro nie zamierzała. Viorika jeszcze nie wie.

Cała trójka siedzi w systemie planowania. Ślicznospad przyniósł kolejne posiłki, jedzenie itp. Dobrze traktuje Mścizęba. Głupia sprawa - nic do niego nie ma, ale polityka.

Mściząb komentuje Wasz plan:

* Mściząb: potwór najpewniej nie zaatakuje. Popatrz, chcesz mieć pułapkę, tak? Taką, wiesz gdzie potwór jest, tak?
* Mściząb: no to... czemu nie zrobisz tak - Tiana jako przynęta, związane nogi, żeby nie mogła iść. I ręce. I w jaskini trzymasz tą pułapkę. Na nie wiem, zdalnym sterowaniu.
* Mściząb: potwór jest, "o, smaczna Tiana" (widzicie że on nie patrzy na Tianę jak na OSOBĘ)...
* Arianna: jeśli ofiara się nie porusza, nie zainteresuje się.
* Mściząb: hm... (myśli). To jej nie wiążmy. Niech się zbliża do jaskini. Zanim się zbliży, wysadzisz go. Najwyżej będzie ranna.
* Viorika: zdajesz sobie sprawę, że szafowanie zdrowiem żołnierzy - zwłaszcza ochotników - nie robi dobrze na oddział?
* Arianna: zwłaszcza jak potrzebujesz do kolejnej akcji. Żołnierza szkoli się długo i potrzebuje być doświadczony. By nic mu się nie działo
* Mściząb: tak, ale masz 2 minuty czasu reakcji. Czy coś. Zdążysz. Tiana nie zginie. Nawet nie powinna być ranna. 
* Viorika: nic nie wiemy o stworze, więc... jeśli to zwykły pajęczak nie będzie problemu. Ale nie musi tak być. To coś działało dłuższą chwilę i źle mu nie było.
* Mściząb: nie będzie polować na scutiery. Tiana w scutierze była daleko przede mną, ja ją osłaniałem i nic się nie działo.
* Viorika: i dlaczego będzie w personalnym pancerzu
* Mściząb: Tiana się zgodzi. Jest świetnym żołnierzem.

Pułapka działa tak:

* Robimy lukę w obronie. Wiemy, że z dwóch wejść z jaskini da się pewien obszar zaatakować. Luka wygląda jak przypadkowa.
* Tiana jest umieszczona żeby prała mundur w rzece czy coś. Coś ma robić. W narażonym obszarze. Tiana ma personal armour, ale nie servar.
* Tiana jest mocno monitorowana z naszej strony.

Rozmowy były typowe dla nerdów militarnych. "użyjemy gatlinga", "nie, działa burzącego". Aż w końcu Ślicznospad zaproponował minę na 'remote control' - gdy Tiana jest pod wpływem pieśni, idzie w stronę na jaskinię i ktoś naciśnie guzik i mina eksploduje. Wszyscy analizujący sytuację taktyczną spojrzeli na Ślicznospada ze smutkiem. Verlenowie robią rzeczy Verlenowe i kochają Verlenową broń i rozważania o typie uzbrojenia.

* Mściząb: "Jeśli Tiana jest moją żołnierką, to ja chcę nacisnąć guzik. Jestem jej to winien."
* Ślicznospad: "tien Mścizębie, chcesz zabić potwora."
* Mściząb: "Tak. Jestem tu od początku i chcę go zabić. Ale..." (walczy z sobą) "...ale.... możecie patrzeć". (chciał powiedzieć coś innego)
* Viorika: (lekko rozczarowane spojrzenie)
* Arianna: "Szczerze mówiąc, Mścizębie, mi personalnie nie zależy by zyskać rozgłos z tej sprawy. Ślicznospad poprosił mnie, bo jego miejscowość zyska na walorach turystycznych, ekonomii - szersze, pozytywne efekty niż ja dostanę bonusy reputacyjne. A Tobie zależy na tym by dostać reputację za zabicie potwora. Satysfakcjonowałoby Cię, gdybyśmy... wytropię Ci innego potwora którego zabijesz by się wykazać? Odpowiednio niebezpiecznego i w odpowiednich warunkach?"
* Mściząb: (nie wie co powiedzieć)
* Arianna: Nie mam też nic przeciwko by wezwać Cię do moich terenów gdy coś się będzie działo.

Arianna chce, by z własnej woli ustąpił z zabijania tego potwora i nie chcemy pogłębiać konfliktu.

Tr Z +3:

* Vr: odstąpi od rozwalenia tego potwora.
* Vr: nie ma zaognienia tego konfliktu.
* V: "tym razem tak musiało być, tak jest dobrze, niech będzie"

Mściząb jest wyraźnie niezadowolony. To widać. Ale odstąpił. I nie chce niczego od Arianny ani Vioriki. Przegrał przez politykę i widzi, że po raz KOLEJNY czegokolwiek nie zrobi to polityka faworyzuje Ariannę Verlen nad niego.

* Mściząb: Wpadłbym na ten pomysł z miną bez Was. (z takim lekkim... takie ostrze bez ostrza)
* Ślicznospad: Nie mam wątpliwości, tien Mścizębie. Po prostu polityka wymaga polityki.

Niech jest ktoś, kto widzi Mścizęba w dobrym świetle.

* X: Mściząb wychodzi z założenia, że SKORO Arianna i Ślicznospad faktycznie są skłonni współpracować z nim, GDYBY był lepszy, szybszy, zdolniejszy - byłby w stanie uzyskać chwałę oraz glorię jaka jest mu należna.
* Vz: Ślicznospad powiedział Mścizębowi, że bardzo docenia itp. Po prostu problem polityczny. 
    * -> DOCELOWO ta dwójka będzie w stanie się jakoś zaprzyjaźnić, jakkolwiek to nie brzmi. Mimo, że na papierze nie powinni być przyjaciółmi.

(Potwór zginął od miny. To faktycznie był pajęczak. Plan się udał)

Wielka impreza. Jak na miasto 1300 osób, przeznaczona Ariannie. A Mściząb siedzi na boku w cieniu bawiąc się nożem. Jest mroczny. Ale dziewczyny biorą go do tańca. 

## Streszczenie

Arianna i Viorika, wezwane przez Ślicznospada do Dun Latarnis w temacie walki z potworem napotykają na problem polityczny - Mściząb już tam jest i Ślicznospad nie może pozwolić, by Mściząb potwora pokonał (bo Ślicznospad i Dun Latarnis wpadną do innej strefy wpływów Verlenów). Mściząb bardzo agresywnie reaguje na obecność Arianny i Vioriki, ale Arianna go udobruchała i trzej Verlenowie współpracowali by zniszczyć potwora. Wszyscy wygrali - tylko Mściząb jest przekonany, że musi bardziej się starać, by osiągnąć szacunek ze strony Rodu.

## Progresja

* Mściząb Verlen: zaprzyjaźnił się ze Ślicznospadem Verlenem, co jest zabawne, bo niewiele ich łączy - inne charaktery, motywacje, podejścia.
* Mściząb Verlen: wychodzi z założenia, że SKORO Arianna i Ślicznospad z nim współpracują, GDYBY był lepszy, szybszy, zdolniejszy - byłby w stanie uzyskać chwałę oraz glorię jaka jest mu należna.
* Ślicznospad Verlen: zaprzyjaźnił się ze Mścizębem Verlenem, co jest zabawne, bo niewiele ich łączy - inne charaktery, motywacje, podejścia.

## Zasługi

* Arianna Verlen: przekonała Mścizęba do współpracy wyjaśniając, że problem jest natury politycznej a nie kompetencyjnej; podpisała autograf na planie pokonania potwora w Dun Latarnis
* Viorika Verlen: zaprojektowała skuteczną pułapkę na potwora wykorzystując swoją wiedzę o sentisieci i technologii; nie zgodziła się na przeczekanie by potwór przeszedł gdzieś indziej
* Ślicznospad Verlen: tien Dun Latarnis; 31-letni elegancki miłośnik sztuki i kompetentny administrator. By chronić Dun Latarnis przed wpływami frakcji izolacjonistycznej Verlenów, wezwał Ariannę i Viorikę by to Arianna zabiła potwora a nie Mściząb.
* Mściząb Verlen: przybył pokonać potwora w Dun Latarnis. Stracił 3 osoby zanim doszedł do tego jak zabezpieczyć teren; niski exp. Jego lokalna wiedza i zrozumienie zachowań potwora były nieocenione w pokonaniu syrenopajęczaka. Nabrał kompetencji od ostatniego razu.
* Tiana Grolmik: kapral; broniła dzielnie honoru Mścizęba przed Arianną, odważnie służyła jako przynęta dla potwora, co pozwoliło na bezpośrednie zaangażowanie i pokonanie zagrożenia.
* Serra Panirik: otrzymała Scutiera od Arianny; mała drobna żołnierka-verlenlandrynka z Dun Latarnis, ochotniczka do większości dzikich operacji.

## Frakcje

* .

## Fakty Lokalizacji

* Dun Latarnis: w Verlenland, stosunkowo bezpieczne miejsce, chyba, że pojawi się coś szczególnie podłego, monitoruje teren dookoła i słynie z dobrej klasy tekstyliów.
* Dun Latarnis: import: wysokie technologie, magitech, żywność, dobra wszelakie, sensory
* Dun Latarnis: eksport: tekstylia, ubrania, płótna, śpiwory wojskowe, intel (na bazie sensorów itp), hodowla podwodna, odpoczynek
* Dun Latarnis: dostęp do bezpiecznych źródeł podziemnych, niewielka lokalizacja, około 1300 osób
* Dun Latarnis: ważny aspekt: 'Labirynt Korytarzy'; są tu jaskinie i szczeliny
* Dun Latarnis: pojawił się tam podły pajęczy potwór przyzywający śpiewem ofiary; Mściząb pomógł containować, ale ściągnięto tam też Ariannę i Viorikę.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. Dun Latarnis
                                1. Strona Podgórska
                                    1. Podziemne źródła
                                    1. Fabryka tekstyliów
                                    1. Latarnia sensorów
                                1. Strona Wskalna
                                    1. Obszar mieszkalny
                                    1. Targowisko
                                    1. Systemy obronne

## Czas

* Opóźnienie: -44
* Dni: 3

## Inne
