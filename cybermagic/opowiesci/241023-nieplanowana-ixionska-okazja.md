## Metadane

* title: "Nieplanowana ixiońska okazja"
* threads: brak
* motives: the-expander, the-distractor, petla-magia-emocje, potwor-asymilator, baza-ledwo-sprawna, body-horror, przemyt, zlamanie-procedury-negatywne
* gm: żółw
* players: til, vivian, edward, adam

## Kontynuacja
### Kampanijna

* [241030 - Arkologia Królowej Pająków](241030-arkologia-krolowej-pajakow)

### Chronologiczna

* [241030 - Arkologia Królowej Pająków](241030-arkologia-krolowej-pajakow)

## Plan sesji
### Projekt Wizji Sesji

**WAŻNE**: Kontekst sesji i okolicy oraz Agendy itp DOKŁADNIE opisane w [Thread 'serce-planetoidy-kabanek'](/threads/serce-planetoidy-kabanek). Tam też znajduje się rozkład populacji itp.

* INSPIRACJE
    * PIOSENKA WIODĄCA
        * [Nemesea "The Taker"](https://www.youtube.com/watch?v=l0NBv8gWDJ4)
        * "As he tried to move | His body froze, he never realized | A higher power took him by surprise | Told him his life would end | He didn't understand"
        * Ixient przejmuje kontrolę, dominuje, zamienia-w-siebie.
    * Inspiracje inne
        * Alien from the Darkness; [zmieniony prefektiss ixioński](https://ez.riggeddice.pl/ez-world/energies-magic/ixion/#5-typowe-manifestacje-tej-energii) pełni tą rolę.
* Opowieść o (Theme and Vision):
    * "_Chciwość w świecie Anomalii prowadzi do niebezpieczeństw przekraczających możliwości. Ostrożność ma znaczenie._"
        * Advancerzy weszli do wraku w poszukiwaniu wartościowych rzeczy. Znaleźli prefektiss.
        * Kapitan SC Naetika nie musi przemycać niebezpiecznych rzeczy, ale to robi. TAI przez to nie ma pełnego raportowania.
        * Kapitan SC Naetika i Kapitan OO Kastor nie powinni się spotykać w celach handlu, "pozyskania czegoś małego i miłego" itp. Ale to robią.
    * "_Procedury istnieją z pewnych powodów. Procedury są po to, by wszyscy byli bezpieczni, nawet jak to niewygodne._"
        * OO Kastor nie powinien się łączyć ze SC Naetika śluzą w śluzę. (to pozwala na kaskadę infekcji).
        * OO Kastor nie powinien robić niezapowiedzialnych ćwiczeń bez zgody Centrali (to pozwala na narażenie rekrutów).
        * OO Kastor nie powinien używać CZTERECH magów na CZTERDZIEŚCI osób (to zagraża stabilności magicznej okolicy).
    * "_Anomalie magiczne są bardzo niebezpieczne a magowie nie zawsze są tylko korzystni._"
        * Prefektiss jest w stanie skutecznie zmasakrować większość obu statków jeśli się nie spodziewają co się dzieje.
        * Magowie będą destabilizować rzeczywistość swoją magią, pod kątem Pętli Magia-Emocje.
    * Lokalizacja: Okolice Neikatis
* Motive-split
    * the-expander: neo-prefektiss ixioński próbuje zainfekować jak najwięcej ofiar i próbuje się rozprzestrzenić, pozostając niezauważonym. Docelowo spróbuje zrobić Terror Ship.
    * the-distractor: kapitan OO Kastor; naprawdę próbuje pozyskać cenne przedmioty z SC Naetika. Aktywnie odwraca uwagę że tam coś MOŻE się dziać.
    * petla-magia-emocje: za mała populacja ludzka na obu statkach na czterech magów; im bardziej ludzie są przestraszeni i im bardziej Pryzmat jest niekorzystny, tym bardziej rzeczywistość zaczyna destabilizować.
    * potwor-asymilator: neo-prefektiss; suppressuje swoje ofiary zmuszając je do działania zgodnie ze swoją wolą. Integruje się z ich układem nerwowym oraz z AI Core.
    * baza-ledwo-sprawna: SC Naetika; statek jest starego typu, niemały transportowiec z większością systemów ledwo działających. Idealny do przemytu.
    * body-horror: neo-prefektiss ixioński jest w stanie zrobić 'force corruption' swojej ofiary by przekształcić ją w godnego i zdolnego do walki terrorforma.
    * przemyt: SC Naetika przemyca dobry towar, który bardzo cieszy kapitana OO Kastor - kapitan OO Kastor chce swoją działkę / porcję.
    * zlamanie-procedur-szkodliwe: wpierw kapitan Naetiki wszedł na podejrzany wrak a potem kapitan Kastora złamał procedurę by pomóc przyjacielowi; to sumarycznie doprowadziło do śmierci kapitanów i części załogi z ręki potwora.
* O co grają Gracze?
    * Sukces:
        * Uratować jak najwięcej ludzi z OO Kastor i SC Naetika.
        * Usunąć prefektissa, najlepiej na stałe.
        * Odkryć, że kapitan OO Kastor dorabia sobie na haraczach przemytników.
    * Porażka: 
        * Kapitan OO Kastor umrze, gdy weźmie kapitana SC Naetika na stronę, zostanie nowym nośnikiem prefektissa.
        * SC Naetika zostanie zniszczona a ludzie na pokładzie - łącznie z niewinnymi - umrą.
        * Prefektiss przedostanie się przez OO Kastor dalej.
        * Za duża populacja marines zostanie zainfekowana przez prefektissa.
* O co gra MG?
    * Highlevel
        * W sumie gram na pełnej mocy prefektissa. Chcę, by się mu udało uciec. Ale prefektiss nie wie z czym ma do czynienia.
        * Chcę im pokazać rozpadanie się rzeczywistości przy nadmiarze magii. Strach staje się rzeczywistością.
        * PRIMARY: zaimplementować Theme & Vision, pokazać magię, pokazać błędy jak nie ma procedur.
    * Co uzyskać fabularnie
        * Zasymilować: kapitana OO Kastor, zniszczyć SC Naetika, zabić sporo marines i zrobić showdown między magami i prefektissem.
* Agendy
    * Prefektiss: chce przetrwać i się wydostać, zdobyć lepszy statek. Stworzyć Terror Ship.
    * Kapitan OO Kastor: chce ukryć wszystko o swojej obecności, chce zapewnić sobie bezpieczeństwo i wpływy.
    * Ludzie: chcą przetrwać i opuścić ten koszmar. Nie wiedzą co się dzieje i chcą przetrwać.

### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

KONTEKST:

* 

CHRONOLOGIA:

* 

PRZECIWNIK:

* 

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

Faza 0: TUTORIAL

* Tonalnie
    * Wchodzimy na niezidentyfikowany wrak; tu jest niebezpiecznie
* Wiedza
    * Nie ma biomasy. Nie ma żadnych ludzi, statek w dobrej formie.
    * Jest to jednostka z której da się wydobyć sporo wartościowych rzeczy.
* Kontekst postaci i problemu
    * Chcę pokazać _efekt_ działania prefektissa. Statek jest czysty, ale DZIWNY.
        * Łatwo się włamać, łatwo się dostać wszędzie, da się znaleźć różne ciekawe rzeczy.
            * Moduł pamięci (z AI Core), Zapasy żywności + wody + materiel itp, dzienniki załogi, zdjęcia, rzeczy osobiste
        * Statek jest "jak nawiedzony", ale są ślady np. barykad, zniszczone komunikatory
    * Chcę zrobić tutorial mechaniki
* Implementacja
    * Ładownia jest zablokowana; trzeba odblokować i przetransportować zapasy
    * AI Core jest zablokowany; zabarykadowany z zewnątrz
    * Statek ma powyłączane wszystko; działa tylko najbardziej podstawowy system z reaktorem

Faza 1 (20:00 - 21:30): PRZYBYCIE

* Zaczynamy od wyjaśnienia kontekstu "przylecieliśmy, kapitan nie pozwala zignorować, nie ma promu tylko dokowanie, Naetika się zgadza na ćwiczenia i pomoc".
* koty (pacyfikatory) ROZPACZLIWIE próbują uciec; nie da się.
* marines się rozdzielają i robią patrol na statku; neuronauta ma TRAGEDIĘ - coś poszło źle
* infekcja jednego marine - lokalna ślicznotka (Karla) poszła na bok z jednym z marine i neo-prefektiss go dorwał
* plotki, że nie wszystko jest tu w porządku; były straty i problemy
* comms "co odpierdoliło Twoim ludziom?"

| Kto                 | 20:00 - 20:45         | 20:45 - 21:30              |
|---------------------|-----------------------|----------------------------|
| **The Expander**    | nic                   | infekcja jednego marine    |
| **The Distractor**  | rozkaz, wejście, koty | patrol, neurofail          |
| **Humans**          | nic                   | plotki że coś jest nie tak |
| **Magic**           | nic                   | nic?                       |


Faza 2 (21:30 - 23:00): KASKADA INFEKCJI

* Paula as diversion and infector; poluje na ludzi jako designated terrorform; ma wskazać skrytkę
* 2 marines sprowadzają Purchawczyka do Szamockiego

| Kto                 | 21:30 - 22:15         | 22:15 - 23:00               |
|---------------------|-----------------------|-----------------------------|
| **The Expander**    | Paula, evolution      | Paula, artifact fight       |
| **The Distractor**  | summon captain        | INFECT Kastor captain       |
| **Humans**          | dwie osoby zniknęły   | marines szukają zaginionych |
| **Magic**           |  "strach wszędzie"    | Neika atakuje; terrorwąż    |

Faza 3 (22:00 - 23:00): ENDGAME

* Rozkazy 
    * 'zniszczcie silniki i zostawcie innym jednostkom'
    * 'odizolujcie ich w kapsułach ratunkowych'
    * 'wykonajcie ewakuację, by statek był bezpieczny, wyślemy wiadomości'

| Kto                 | 22:00                     | 22:30                      |
|---------------------|---------------------------|----------------------------|
| **The Expander**    | hunt Róża (new body)      | ? |
| **The Distractor**  | rozkazy na Kastorze       | ? |
| **Humans**          | panika, riot; dont leave  | ? |
| **Magic**           | poświęcamy Neikę i załogę | ? |

## Sesja - analiza
### Fiszki
#### Orbiter, OO Kastor, 30 rekrutów, 27 załogi i marines, plus 4 magów

* Paweł Szamocki: kapitan OO Kastor
    * zbiera haracz od różnych przemytników i ogólnie wie jak się ustawić by szybko trafić na emeryturę.
    * Sybaryta: lubi luksus, lubi piękne rzeczy i nie lubi ich nie mieć; jest rozgoryczony faktem, że nie ma tego wszystkiego co mu się należy.
* Cezary Żertniś: pierwszy oficer OO Kastor
    * wykonuje rozkazy, jest lojalny Orbiterowi oraz kapitanowi. 
    * Perfekcja Człowieczeństwa: nie jest fanem rzeczy, które zmieniają ludzką naturę; brzydzi się nimi. Nie lubi magów ("pasożytnictwo ich ciała").
* Róża Leszczynienka: Oficer komunikacyjna OO Kastor
    * niezłej klasy neuronautka i oficer komunikacyjna. Kiedyś idealistka, ale się przesunęła.
    * Oportunista: stoi po stronie, która pozwoli jej jak najwięcej zarobić czy osiągnąć. Tymczasowo lojalna kapitanowi, bo może zarobić więcej.
* OO Kastor: fregata dalekosiężna, 27 osób załogi i 30 transportowanych rekrutów + 4 magów gdy ściągnął ją anulowany sygnał SOS i robiła operację ratunkowo-ćwiczenia.

#### Cywile, SC Naetika, 40 załogantów

* Kasjan Purchawczyk: Kapitan SC Naetika
    * przemytnik, szarmancki i elegancki, nie dba o reguły czy procedury. Liczy się dlań jedynie Naetika, nic innego.
    * Pragnie Więcej: kieruje nim chciwość oraz pragnienie wyrwania się z tego wszystkiego. Chciałby móc zapewnić sobie, żonie i załodze lepsze jutro.
* Kamila Szekiewicz: medyk SC Naetika
    * zainfekowana przez prefektissa
    * (Ratchet): zgryźliwa, wiecznie niezadowolona
* Paula Barzik: załogantka SC Naetika
    * zainfekowana przez prefektissa
    * (PoisonIvy): erotic++, cechy wzmocnione
* Marian Pręgowiec: advancer SC Naetika
    * zainfekowany przez prefektissa
    * Oportunista: "kazali iść, poszedłem. Reguły nie mają sensu. A teraz oni nie żyją."
* Damian Kłęczywo: neuronauta SC Naetika
    * (Blackarachnia): skonfliktowany; wie, że coś jest nie tak ale też wie, że musi siedzieć cicho. Nienawidzi siebie i innych.

#### Postacie Graczy

* Isa Całujek: Terminuska + Salvager
* Lucjusz Jastrzębiec: Lekarz + Arystokrata Aurum
* Wanda Dola: As Pilotażu, Komendant, 30 lat
    * Podporządkowała swoje życie jednemu marzeniu - pracować na pokładzie statku
    * Ubiegała się o wszelkie możliwe stypendia, uczyła się i ćwiczyła pozwalając by życie mijało ją bokiem, ale dopieła swojego celu - została nawigatorką i pilotem. 
    * Choć mineło kilka lat od pierwszej misji, gdy widzi przed sobą szeroką perspektywę kosmosu, wie, że było warto bo ten widok. 
    * W skrócie: zdeterminowana i ciekawa (wszech)świata, ale bywa zbyt skupiona na celu.
* Astian Kider: Pionier, "Kapłan"
    * Jego zakon zajmuje się szkoleniem swych Kapłanów jako Przewodników i Pionierów (patrz archetyp)
    * Podąża wiernie za głosem swej Pani i jakimś cudem odnajduje właściwą drogę w dowolnym terenie.

### Scena Zero - impl

Trójka najlepszych do tego celu osób z Naetiki, advancer, marine i inżynier, zostali wysłani promem na znajdującą się niedaleko jednostkę. Ten statek wygląda jak wrak kosmiczny; wygląda na nieaktywny, ale na pierwszy rzut oka nie wygląda na to, żeby był poważnie uszkodzony. Procedury mówią, że najbezpieczniej poinformować Orbiter. Jednocześnie procedury nie nakarmią takiego statku jak Naetika. 

Skanujemy jako Advancerzy + Inżynier.

ExZ+3:

* V: Statek jest w trybie uśpienia. Ogólnie jest sprawny. TAI wygląda na aktywną.
    * Statek wygląda sprawnie.

Są to bardzo nietypowe odczyty. Po obejrzeniu statku wszystko wygląda prawidłowo. TAI jest w trybie... maintenance? Trzeba nawiązać kontakt. 

* Inżynier: "Tu Naetika, jesteśmy przyjaźnie nastawieni, odbiór".
* TAI nie odpowiada
* Polecenie: "RAPORT"
* TAI się obudziła.
    * "(seria sygnałów, bełkot, szum)"
    * "(SOS, SOS)"
    * "(seria sygnałów, bełkot, szum)"

Po dokładnym przeanalizowaniu sygnałów coś jest bardzo nie w porządku.  


* Vz: Z sygnałami z TAI jest coś cholernie nie tak.
    * TAI wysyłała coś nie tak.
    * Przebił się inny sygnał. "zero osób na pokładzie".
    * Status jest JEDNOCZEŚNIE: ludzie w krio, krytyczna sytuacja. Oraz ZERO OSÓB NA POKŁADZIE!!!

Nie można jednocześnie mieć zero osób na pokładzie oraz osób w komorach kriogenicznych, którzy potrzebują natychmiastowej pomocy medycznej. Procedury mówią, że należy wezwać jednostki w tym wyspecjalizowane. ale jeśli ludzie potrzebują pomocy...  

Advancerzy dają rekomendację "wchodzimy".

### Sesja Właściwa - impl

Zespół próbuje się dostać na Neikatis; Orbiter wysłał ich akurat lecącym tam statkiem, Kastorem. Kapitan może nie jest najlepszy, ale to nadal Orbiter. Jakkolwiek zespołowi się nie podoba kapitan, to jednak są po prostu wartościowym ładunkiem. 

OO Kastor zmienia kurs. Pierwszy oficer nie lubiący magów, Cezary Żertniś, wszedł wściekły do mesy. Zapytany przez magów odpowiedział "Sygnał SOS" z jednostki cywilnej, "bo musimy zmienić kurs". Żertniś nie chce powiedzieć o co chodzi. Zespół przekonuje Żertnisia, że są po tej samej stronie. I mogą nałożyć problemy na kapitana Kastora (Pawła Szamockiego).

"Zatajanie przed nami nie jest w Twoim interesie bo się możemy przydać"

Tr +3:

* X: Gorętsza sytuacja; "spisek przeciw kapitanowi, XO vs kapitan"
* X: Pierwszy oficer jeszcze bardziej nie lubi magów niż kiedyś. GARDZI. I dostał wpierdol i brak szacunku z załogi.
    * "Sygnał SOS, zmieniliśmy kurs, sygnał SOS i info od pana, sygnał."
* Vr: Pierwszy oficer się przyznał
    * "Kapitan przedkłada personalne obowiązki". 'Naetika'.
    * "Oni się lubią".

Od razu pytania – dlaczego Kastor zmienił kurs? Mają dość delta-v, ale to nie jest zgodne z regułami. SOS z jednostki cywilnej. Oni nie mają obowiązku pomóc, zwłaszcza nie jeśli przewożą około 20 nieopierzonych, niedoświadczonych marines. I zwłaszcza z uwagi na niestabilne okno transferowe. A już w ogóle z uwagi na to, że sygnał SOS był aktywny a potem zaniknął. Ale - kapitan podjął decyzję by to zrobić i nikt nie ma nic do powiedzenia.

Lecimy więc na Naetikę - statek cywilny, który zrobił SOS.

Dotarliście w pobliże Naetiki. Zespół (czterech magów) nie ma wiele do roboty.

Kapitan Paweł Szamocki zaczął od: "marines do odprawy".

* "Jednak nic złego się nie działo, fałszywy alarm"
* "Straciliśmy okno transferowe na kilka godzin"
* "Więc idźcie na Naetikę, pomóżcie im tam, zrobimy ćwiczenia dzięki uprzejmości Naetiki".

Zespół jest niepewny co z tym zrobić; kapitan technicznie nie robi nic złego, ale nie powinno tak być. Chce kilka godzin kupić. Co, będą znowu kapitanowie grać w karty? Niestety, Kastorowi się nie spieszy i nie musi się spieszyć; ich misja nie wymaga szybkiego działania ani szybkich mechanizmów funkcjonowania. Zespół nie zamierza się jednak w nic głupiego angażować.

* "Zioła prozdrowotne" dla magów :3, na kilka godzin.
* Zespół spożywa ziółka Lucjusza; "nalewka prozdrowotna".

Róża Leszczynienka - Oficer komunikacyjna OO Kastor - wpada pół godziny później do pokoju gdzie Zespół raczy się ziółkami. "Czy jest lekarz! Kto jest lekarzem!". Okazuje się, że jeden z marines wpadł w katatonię po neuro-integracji z TAI Naetiki. To się nie zdarza. Marine nie miał tak silnego mechanizmu sprzęgającego by mogło pokonać barierę defensyw... więc... magia? I dlatego absolutnie niezbędny jest mag. I dlatego Róża poprosiła Lucjusza o pomoc. Tam coś się dzieje...

Prom. Korytarz już jest. "Zerwać połączenie, żadnego połączenia TAI."

Prom - lecicie. PACYFIKATORY WIEJĄ NA PROM!

Wewnętrzny obieg: 

Patryk Kamicki, dowódca marines (p.o.).

* Czy oprócz osoby która ma problemy ktoś miał kłopot?
* A ktoś miał kłopot?

5 minut później - Patryk zbiera ludzi. Zniknął Olgierd.

Otwierasz się na Eter.

TrM+3:

* V: 
    * bardzo podniesione pole magiczne
    * TAI, i TAI jest Skażona Ixionem

Lucjusz biegnie do AI Core. Isa osłania. Wanda "kwarantanna".

Wanda przejmuje dowodzenie nad marines. Nie ma nikogo wyższego rangą. Komendant. Marines stoją przy promie, czekają, pilnują się krzyżowo.

Tr Z+3:

* X: Coś Już Się Stało. Nie wszyscy marines wrócą.
* Vr: Jest 36 aktywnych marines. Pilnują się, słuchają Cię itp.
* Vr: Mamy 6 zespołów funkcjonalnych marines p.k. Wandy
* V: Prawdziwe informacje od marines
    * Marian połączył się ze Rdzeniem i padł. -> SOS do Kastora i Róża przybiegła do Lucjusza.
    * Polecenie tutaj: "pomóc, znaleźć problemy, ponaprawiać przecieki, "słuchajcie trzeba się wykazać".
    * Z nimi rozmawiał kapitan i że problemy z AI Core. "Błędny sygnał SOS, nie trzeba nic robić". Marian mu odradzał. To nie oni SOS. To TAI. 
    * Z PLOTEK wynikało, że SOS był niedługo po spotkaniu wraku.
        * Wrak "Elisandra". Że stracili tam dwie osoby. Jednemu udało się uciec. "Tam był potwór, zjadł wszystkich".
        * Advancer w ambulatorium.
    * Kapitan wolał zbadać itp.
    * PLOTKA "sygnał SOS był po to, by Naetika mógł powiedzieć kapitanowi Kastora że jest niebezpieczny wrak."

ROZKAZ: zebrać wszystkich do mesy.

Ambulatorium. Kilka osób. Medyk, advancer, Marian.

* Lucjusz po kątach rozstawia.
* Medyk
* "Macie problem, jesteśmy magami"

Sprzęt tutaj jest kiepski, przeterminowany i FATALNY. Lucjusz przeszukuje stan mentalny Mariana; EEG itp.

Tr Z +4 +3Og:

* Vz: Marian świeci się jak choinka. Jego własny mózg go przeciąża. Coś z niego czerpie jak cholera.
* -> Tp+4 -3Og, V: Marian śpi jak suseł. Wyłączony mózg.

Lucjusz jak patolog. Mówi do komunikatora co robi, co z tego wynika.

Równocześnie badamy.

Tr Z+3 + 3og:

* X: Bardzo skomplikowany system, duża koncentracja. Bardzo się trzeba skupić.
* Vz: System wygląda na czysty. Są próby shackowania, ale nieudane. Jest bezpieczny.
* Vr: Agenda: "PRZEŻYĆ, POSZERZYĆ". Zimnym, dużym intelektem. Ixioński byt BYŁ 

Advancer atakuje Isę.

* Vz: NIE jest zdehermetyzowany pancerz, ale jest uszkodzony
* X: Isa ma USZKODZONY serwopancerz, nie jest zdehermetyzowany
* V: Isa ROZCHLAPAŁA mózg ixiońskiego terrorforma na ścianie z półobrotu.

Medyk wymiotuje gdzieś w kącie.

Lucjusz starannie zmywa z pancerza Isy jakiekolwiek kawałki terrorforma. Żeby nie doszło do tego, że cokolwiek i ktokolwiek zostanie zarażony.

* Kwarantanna w laboratorium

Kierowane badanie.

"Czy ciało terrorforma jest hazardem, czy jest DZIAŁAJĄCĄ materią lub zintegrować się ze statkiem".

Tr ZM +3 +3Ob:

* Ob: Naergetyzowałeś byty ixiońskie energią. Są SILNIEJSZE.
* Vm:
    * Strumień Exemplis przeszedł przez to, korzystając z podniesionego pola magicznego to poszło za tym. Strumień komunikacyjno/informacyjno/poznawczy poszedł po korytarzach. MAPUJĄC byty ixiońskie.
* Vg: 
    * Rozświetliły się ixiońskie byty w Eterze dla Lucjusza. 

Advancer jest biedny. W nim jest ślad po... czymś. Prefektiss.

Isa obserwuje, zastrzeliła "medyka".

Wiadomość.

Mamy zdecydowaną większość załogi i większość marines.

Tr Z+3:

* X: To nie ten prom. Prom musiał się zmienić.
* X: Jeden oddział marines jest Zarażony.
    * Kapitan Szamocki kazał jednej grupce marines znaleźć kapitana.

W załodze Naetiki jest 6/20 zainfekowanych osób. A z marines 2 jest zainfekowanych.

* Vz: Przekonał o tym ixientów, że to WŁAŚCIWA osoba.

Isa - na pokładzie promu. Z ixientami. Niczego się nie spodziewają. Isa czeka z rozhermetyzowaniem statku aż nie będzie w polu widzenia żadnego okienka.

Wanda manewruje promem; niech NIKT nie wie.

Ex Z+4:

* X: Ixienty WIEDZĄ co się dzieje.
* Vz: Udało się rozrzucić niewidocznie ciała.

OO Kastor oddala się od promu. Kastor strzela ostrzegawczym w stronę promu.

Uspokajanie Róży przez Astiana. (Róża widzi że to nie jest typowa sytuacja i nie chce umierać i są magami). Ona przyszła do nas. KAPITAN powiedział że coś jest nie tak. I kolejny krok. PLUS ratunek Mariana.

TrZ+4:

* X: Róża nie zrobi AKCJI PRZECIWKO kapitanowi, ale może... COŚ zrobić.
* V: Róża "przypadkiem" wprowadzi TAI w pewną formę pętli logicznej przez co Kastor nie będzie TRAFIAŁ.
* Vz: Róża zamaskuje ich w AI Core. Macie dyskretne wejście.

Ekipa, szturm.

Załoga jest czysta. Przebiliście się do kapitana. Doszło do infekcji - mamy dwa ixienty.

BEZ KONFLIKTU - eksterminacja.

I sprawdzenie one-by-one. A następnie wezwanie SOS.

Jeden Pacyfikator był zainfekowany. Został zestrzelony.

## Streszczenie

Podczas transferu na Neikatis kapitan OO Kastor zdecydował się skorzystać z uprzejmości SC Naetika i zażyć luksusów niezgodnie z procedurami. Ale tym razem na Naetice było coś poważnego - infekcja statku przez neo-prefektissa. Doświadczony Zespół Orbitera wszedł, opanował sytuację i usunął prefektissa, który zdążył już niestety zarazić obu kapitanów i medyków. Naetika została oflagowana jako problem i zostanie wyczyszczona przez specjalistów a Kastor leci dalej na Neikatis, jak powinien.

## Progresja

* .

## Zasługi

* Lucjusz Jastrzębiec: działa jak patolog; wykrył obecność ixionu i rozświetlił wszystkie ixienty do łatwej eksterminacji. Przekonał też Różę do współpracy - jeśli nie zatrzymają ixientów, Kastor zostanie zniszczony.
* Isa Całujek: zniszczyła servarem ixienta który zagrażał Lucjuszowi, potem zdehermetyzowała prom by zniszczyć pozostałe ixienty. A na końcu - egzekucja kapitana.
* Wanda Dola: przejmuje dowodzenie nad marines i dramatycznie ogranicza ruchy prefektissa. Manewrując promem zniszczyła ixienty na pokładzie promu; gdyby nie sieć prefektissa (ixienty wiedzą co wiedzą inne ixienty) to by jej operacja była bezbłędna.
* Astian Kider: znalazł najlepsze możliwe ścieżki i drogi by móc kontrolować ruchy prefektissa. Przekonał też ixienty (udając że nie wie) do pójścia za jego planem i wejścia na prom (na ich śmierć).
* Patryk Kamicki: niedoświadczony marine, pełniący obowiązki dowódcy na pokładzie Naetiki. Z przyjemnością oddał dowodzenie Wandzie. Ma potencjał na kompetentnego, po prostu nie ma doświadczenia.
* Róża Leszczynienka: neuronautka z Kastora, która nie chce stawać przeciw kapitanowi ale dała się przekonać dowodom i przesłankom Zespołu z pokładu Naetiki. W końcu nie zestrzeliła promu mimo żądań kapitana (już ixienta).

## Frakcje

* .

## Fakty Lokalizacji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Transfer Astoria - Neikatis

## Czas

* Opóźnienie: 22
* Dni: 1

## Inne

.