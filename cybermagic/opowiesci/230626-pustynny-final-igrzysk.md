## Metadane

* title: "Pustynny finał igrzysk"
* threads: olaf-znajduje-dom, igrzyska-lemurczakow
* motives: okrutne-gry-moznych, echa-inwazji-noctis, niewolnicy-magow, magowie-bawia-sie-ludzmi, misja-survivalowa
* gm: kić
* players: żółw

## Kontynuacja
### Kampanijna

* [230625 - Przegrywy ratują przegrywy](230625-przegrywy-ratuja-przegrywy)

### Chronologiczna

* [230625 - Przegrywy ratują przegrywy](230625-przegrywy-ratuja-przegrywy)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ?
* CEL: 
    * ?

### Co się stało i co wiemy

* Kontekst
    * .
* Wydarzenia
    * .

### Co się stanie (what will happen)

?

### Sukces graczy (when you win)

* .

## Sesja - analiza
### Fiszki

* .

### Scena Zero - impl

.

### Sesja Właściwa - impl

Olaf zaproponował - albo poproszą o "wszystko się zepsuje" albo "DESZCZ ALIGATORÓW" (miny wszystkich były wtf). Jak my się spodziewamy to mamy przewagę. Też - wiemy gdzie mniej więcej napadli Antka i Bogdana. Ale nie ma dużo danych. Nie wiemy który zespół jest który.

Olaf zaproponował Antkowi, żeby on został przynętą. Ma złamaną nogę, w walce się nie przyda, ale da się wymanewrować przeciwnika. Roman zrobi morderczą pułapkę i jeśli wrogowie są naprawdę groźni, to my mając przewagę sił bojowych ich zmiażdżymy. Widząc, że Antek się waha, Olaf ma inną propozycję. "Samotny noktianin rzucony na pożarcie". Olaf umie walczyć, choć nie jest dostosowany do warunków Astorii. "Zostawić noktianina samego na pustyni na śmierć" to coś, co wrogowie łykną. A jak się zamontuje pułapkę czy dwie, to Olaf może kogoś rozwalić. (Olaf robi ruch heroiczny i pełen dramy - to on).

NAGLE w kieszeni Olafa coś się pojawiło. Nieduża tubka do otwarcia. Kawałeczek papieru. "Twoje nowe zadanie - ocal pobratymca". Olaf spojrzał w górę - "spełnię to zadanie". Jeden z pingów na opasce Olafa zmienił ton.

Olaf -> Zespół. "Trudny problem. Nie jestem jedynym noktianinem w okolicy. Wiem, że jest noktianin, którego też mogę uratować. Jeśli to jeden 'z moich', mogę go przekonać. Jeśli to dziecko, nie mogę go zostawić. Jeśli to psychopata, chcę go zabić samemu. Astoria nie zasłużyła na psychopatycznych dekadian. To nie jest wojna Noctis - Astoria..."

* Maja: "myślisz, że jesteśmy w stanie..?"
* Olaf: "popatrz na Bogdana. Jemu mogliśmy pomóc. Ale jeśli czegoś nie zrobimy, umrze. Każda nowa osoba zwiększa szanse przetrwania."
* Roman: "potrzebujemy zwiadu - nie możemy siedzieć na ślepo"
* Maks: "to nie jest moja silna strona, ale mogę spróbować pójść"
* Olaf: "a nie lepiej wieczorem?"
* Roman: "jak wszyscy będą się ruszać?"
* Olaf: "Maks nie może iść sam, jakby coś mu się stało to po nim. Słuchajcie, skoro ja jestem pułapką, to może pójdę z Maksem jak się będę poruszał w tym terenie?"
* Roman: "tak samo jak wchodziłeś na tą diunę"
* Olaf: "Czyli źle..."
* Roman: "to ja pójdę"
* Olaf: (informuje w którą stronę jest grupa z pingiem). (Powiedział o planie - jeśli tam jest noktianin, jest w stanie przeczytać).

Nowy plan:

* Lily, Maja, Olaf, Antek i Bogdan są jak prawie pełen zespół. To jest wiarygodne. ONI są pułapką (pomysł Lily). Lily i Olaf to siła ognia lokalna. Olaf wygląda na najgroźniejszego. Lily ma truciznę.
* Tymczasem, Alan, Roman i Maks są prawdziwym młotem rażącym. To powinno dać duże szanse na to, by Zespół uznano za słaby i jednocześnie by Olaf mógł wykryć noktianina.
* My robimy "pomocy, potrzebujemy pomocy". SOS.

I jest prośba "by to o co poprosili inni się popsuło jak próbują użyć tego przeciw nam". Uczciwa walka, max(drama)...

Niestety, dostajemy oba zespoły w tym samym czasie XD. Nie lookoutujemy za bardzo. Rozstawiliśmy KIEPSKIE i DOŚĆ WIDOCZNE pułapki. "Nie mamy siły". Lepsze dookoła siebie - tam będziemy walczyć.

Zapada wieczór, upał mniej wściekły. A my wyglądamy dość niegroźnie.

konflikt init: Ex (przeciwnik ma Mistrzostwo) ale my mamy +Skalę oraz +pułapki których nawet my nie wiedzieliśmy że się przydadzą. -> Tr +3.

Tr+3 (8X6V3Vr):

* O: wchodzi inna strona. Są DWA zespoły i my.
    * V (12): (podbicie) nie będziemy zaskoczeni - pułapka aktywowana
    * V (9): (manifest) dokładnie JEDEN gościu próbuje się tu przekraść. Roman daje sygnał typu 'scout' niż 'killer'.

.

* O: "Jesteś w stanie samemu się wydostać, czy potrzebujesz pomocy?"
* Infiltrator: rozcina linę nożem. Wielki smutek Zespołu.
* O: "Chodź do ognia, ogrzej się".
* Infiltrator chowa nóż, nie odrzuca, chowa.

Infiltrator jest świetny w walce. Ale nie walczy. Rozmowa. Olaf próbuje przekonać go do sojuszu. Powiedział mu, na czym polega problem:

* są w reality show
* jak wyglądała mapa
* jakie rzeczy skonstruowali i gdzie byli
* że widzieli ślady masakry - jest zespół morderczy
    * (oni też je znaleźli -> jest dowód, że Olaf mówi prawdę)
* jeśli są ranni, niech uwzględni

KONFLIKT: Ex Z (podejście + planowanie) +3 (11X 3V 3Vz 4):

* V (18): Antek się wygadał Infiltratorowi, że on i Bogdan nie są z tego zespołu. Ale to jest dowodem, że sojusz jest PRAWDZIWY.
    * To jest zespół bez noktianina
    * Olaf: "nieważne w jakim stanie jesteś Ty i reszta zespołu. Sojusz się liczy. Albo wszystkich musimy zabić albo się sprzymierzyć. Albo zabijesz Lily, Maję, Bogdana, Antka i mnie albo się sprzymierzymy."
        Na imię Infiltrator ma Jan.
* X (1): Wrogowie odcinają drogę ucieczki. Nie możemy my się wycofać, potrwa moment zanim wsparcie dotrze, Infiltrator nie ma jak uciec.
* X (2): Zespół Jana nie udzieli wsparcia. Jest skrajnie kompetentny, ale JEDYNY kompetentny. Mają 'w miarę obronić'.
    * Atak PRZEKONAŁ Infiltratora.

Widzimy, że przodem idzie jeden, za nim parę kroków idzie dwóch. Atak Przeciwnika. Mają lepszą broń (o to poprosili). Jest ciemno. 

* Olaf: "Ave Noctis! Saitaer nas nie złamał, Wy nas nie złamiecie!" (po noktiańsku, potem astoriańsku). Pierwszy się zawahał.
* Olaf: "Z jakiej jesteś jednostki! Powiedz, żebym mógł powiedzieć Twoim, że ich zdradziłeś!"
* (Lily cicho szepcze Infiltratorowi o planie Olafa. Infiltrator cicho facepalmuje.)
* Noktianin: (gorzki śmiech)(po astoriańsku) "A co, magiem jesteś? Z duchami będziesz gadał?"
* Olaf: "Moja jednostka nie żyje. Pozostałem jej wierny. Ty - zdradziłeś."
* Olaf: "Nie przybyliśmy mordować astorian tylko zniszczyć boga. Mój dowódca sam wbiłby Ci nóż w serce za to co zrobiłeś. Ci ludzie - pomogli mi przetrwać w obcym biomie. Nikogo nie musieliśmy zabijać. My kontra magia. My kontra świat. Ave Noctis! A Ty?!"
* Olaf: "Dzieci, starsze osoby, ranni - a Ty?"

Olaf robi skomplikowany konflikt - konflikt otwarty otwierający sytuację. Tr +3. (8X 6V 3Vr)

* X (3): (podbicie) HIDDEN MOVEMENT
* V (9): (podbicie) Noktianin (dalej po astoriańsku), idzie i przestaje wymijać proste pułapki. "a ja jestem zmęczony... zmęczony tym wszystkim... walką, wojną... po co to wszystko..."
    * (+1Vg) "Jeśli chcesz, by się skończyło, wbij mi nóż w serce" (Olaf wie że się rozpadnie). "Skończy się tak czy inaczej. Ave Noctis!"
* V (10): Gdy się zbliża, nie ma broni. Nie przeszkadza mu podejść i robi ruch jakby atakował. ŁAPIE (bezbronny) Olafa i cicho po noktiańsku (jeszcze trójka z tyłu) i w mordę.
    * Olaf się zatacza. Mówi "Zatem wybrałeś, takiś pewny że nie masz broni?!". Tauntuje innych - "może któryś z Twoich psów będzie w stanie ze mną walczyć?"
* Vr (17): (podbicie) Tych dwóch fokusuje na Olafie, trzech i dwóch jest rozdzielonych.
* X (9): can't taunt, trzeba wiać na pułapki XD.
* X (1): ta dwójka spowalnia, tamta trójka się zbliża do Zespołu.

Teatr bojowy. Bloody war. Roman i ekipa z pleców tych trzech (8 -> V).

* V (1): (podbicie) Olaf wprowadził dwóch na pułapkę, jeden z nich będzie uszkodzony.
* V (8): (manifest) Nowy noktianin (Armadion) złapał przeciwnika i Olaf go zdjął włócznią. "świetna robota"
* V (12): (eskalacja) Widząc, że stracili trzech "tak nagle", wrogowie przez MOMENT zastygają. Nie zbliżyli się do Infiltratora. To wystarczyło, by Roman jednego zabił włócznią.

Olaf: "macie jedną szansę się poddać". Rzucili broń. Olaf -> Antek "ci ludzie zabili Twoich ludzi. Będziesz taki jak oni, czy oddasz ich los w ręce publiczności?" Antek powiedział głośno, że nie chce ich zabijać. Olaf się zwraca do publiczności. "Ci ludzie są źli, ale naszym wrogiem jest magia nie oni, i się nam poddali. Proszę o łaskę dla nich, użyjcie ich inaczej."

...już nie ma nocy. Nagle. Niebo się zrobiło 'flying marquee', 'congratulations, you win'. Dwójka napastników już znika. Dostajemy wiadomość "it IS highly irregular, ale niech tak będzie. Waszym życzeniem jest ocalenie reszty uczestników..."

Nie zabierają pamięci. Nie ma po co. Olaf się budzi wykopany jeszcze gdzieś indziej XD. Ostatni akt złośliwości organizatorów - wypieprzają noktianina 'na prowincję Aurum', do Szczelińca. Z Lily. Bez drugiego noktianina.

## Streszczenie

Olaf proponuje nietypową strategię - udajemy, że potrzebujemy pomocy. Zespół ma surowce. Grupa dzieli się na "młot rażący" (ukryty) i "ofiary" (słaba grupa przy ogniu). Olaf dostaje subtask uratowania pobratymca. Plan jest niezły, ale oba zespoły pojawiają się jednocześnie. Gdy Infiltrator zostaje przekonany, że Zespół jest 'pomocny', pojawiają się wrogowie. Odpowiednio dzieląc ich na subzespoły i mając szczęście (i wsparcie ich noktianina) udaje się Zespołowi zabić 4/6 i zmusić dwóch do poddania się. Gra się skończyła i udało się uratować więcej ludzi niż kiedykolwiek. Olaf i Lily są przeniesieni do Szczelińca.

## Progresja

* Olaf Zuchwały: trafił do Szczelińca z Lily Sanarton metodą teleportacji. W przyszłości on i Lily zostaną rodziną.
* Lily Sanarton: trafiła do Szczelińca z Olafem Zuchwałym metodą teleportacji. W przyszłości ona i Olaf zostaną rodziną.

## Zasługi

* Olaf Zuchwały: zaproponował plan pułapki 3+5, rozmawiał z noktianinem i obrócił go na stronę Zespołu, pomógł przekonać Infiltratora do współpracy i wprowadził przeciwników w pułapkę. Po zwycięstwie apelował, by dać szansę nawet tym 'morderczym psychopatom'. Mimo braku szczególnie dobrych umiejętności w jakiejkolwiek dziedzinie jego optymizm i podejście sprawiło, że udało im się wygrać.
* Lily Sanarton: wymyśliła, by pułapką była ona i Maja, co potem zmieniło się w plan 3+5. Mimo młodego wieku, godna zaufania. Przeniesiona z Olafem do Szczelińca gdy wygrali.
* Roman Wyrkmycz: z Alanem i Maksem schowali się by Olaf i reszta zostali pułapką. Skutecznie z zaskoczenia, od pleców zabił jednego z morderczych napastników włócznią, co zniszczyło ich morale.
* Alan Klart: z Romanem i Maksem schowali się by Olaf i reszta zostali pułapką - a oni młotem rażącym.
* Maks Ardyceń: z Alanem i Romanem schowali się by Olaf i reszta zostali pułapką - a oni młotem rażącym.
* Maja Wurmramin: jako element pułapki głośno woła SOS; jakkolwiek w walce nieprzydatna, ale świetnie udowadnia że to nie jest pułapka.
* Antoni Kmandir: ze złamaną nogą nieprzydatny bojowo, ale dzięki jego obecności dało się łatwiej 'wkręcić', że Olaf ma pełen zespół i to nie jest pułapka.
* Bogdan Ubuddan: nadal nieprzytomny, nadal nic nie może zrobić. Ale przeżył. Dzięki jego obecności dało się łatwiej 'wkręcić', że Olaf ma pełen zespół i to nie jest pułapka.
* Jan Firatiel: Infiltrator, zostawiony w bardzo trudnej sytuacji - jedyny z zespołu był w stanie sobie poradzić. Czuł się odpowiedzialny za swój zespół i zakradł się do zespołu Olafa by zobaczyć kto to. Wszedł z nimi w sojusz gdy zaatakował morderczy zespół.
* Rafał Armadion: noktianin wykorzystany w grach Aurum, szybko zdominowany przez swój zespół. Służył jako mięso armatnie i minesweeper. Gdy skonfrontował się z Olafem, ostrzegł go cicho mimo trudnej sytuacji i pomógł mu pokonać swoich 'panów'.

## Frakcje

* Arbitrzy Rozwiązań Proxy: zaakceptowali z rozbawieniem Wielkie Uratowanie Wszystkich jako życzenie zespołu Olafa, po czym zaczęli kolejną edycję _death game_...
* Grupa Proludzka Aurum: ktoś stąd wykupił Olafa i Lily, by ich przenieść do Szczelińca. Uznali, że warto im pomóc po tym co widzieli. Architekci love story Olafa.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Lemurski
                            1. Wielka Arena Igrzysk

## Czas

* Opóźnienie: 1
* Dni: 3


## OTHER
### Biomy

Desert:
 - Dangers:
    Extreme heat and dehydration.
    Limited water sources.
    Sandstorms and high winds.
    Venomous snakes and scorpions.
    Lack of vegetation and food sources.

 - Supporting Factors:
  Cacti and succulent plants for a potential source of water.
  Use of sand and rocks for constructing protective structures.
  Desert vegetation as a source of food and medicinal plants.
  Animal migration patterns for potential hunting opportunities.
  Clear skies and sunlight for solar energy and navigation.

Arctic Tundra:
 - Dangers:
    Extreme cold and hypothermia.
    Limited food sources.
    Blizzards and strong winds.
    Polar bears and other predatory animals.
    Frozen terrain and potential for falling through ice.

 - Supporting Factors:
  Presence of cold-adapted animals like caribou and arctic foxes for potential food sources.
  Use of ice and snow for constructing insulated shelters.
  Frozen lakes and rivers for access to water sources.
  Availability of mosses and lichens as edible vegetation.
  Presence of driftwood for firewood and building materials.

Rainforest:
 - Dangers:
    High humidity and risk of fungal infections.
    Dense vegetation and limited visibility.
    Venomous snakes, spiders, and insects.
    Flash floods and heavy rainfall.
    Lack of clear water sources.

 - Supporting Factors:
  Abundant rainfall and water sources, such as rivers and waterfalls.
  Diverse plant life for food sources, including fruits, nuts, and tubers.
  Availability of medicinal plants for treating illnesses and injuries.
  Natural materials like vines and bamboo for constructing shelters and tools.
  Animal diversity for potential hunting and fishing opportunities.

Mountain Range:
 - Dangers:
    Thin air and altitude sickness.
    Harsh weather conditions and avalanches.
    Steep and treacherous terrain.
    Limited food sources.
    Wildlife encounters, including bears and mountain lions.

 - Supporting Factors:
  Natural rock formations and caves for shelter.
  Mountain streams and lakes for freshwater sources.
  Availability of alpine plants and berries for food.
  Wildlife, such as mountain goats and birds, for potential hunting.
  Natural resources like rocks and branches for constructing tools and weapons.

Coastal Jungle:
 - Dangers:
    Dense vegetation and limited mobility.
    Saltwater exposure and dehydration.
    Dangerous marine life, such as sharks and jellyfish.
    Flash floods and high tides.
    Limited food sources.

 - Supporting Factors:
  Abundance of marine life for fishing and gathering seafood.
  Mangrove trees for building shelters and boats.
  Availability of coconut trees for food, water, and materials.
  Natural landmarks, such as cliffs and rock formations, for navigation.
  Use of driftwood and bamboo for constructing tools and traps.

Savannah:
 - Dangers:
    High temperatures and risk of dehydration.
    Limited water sources.
    Predatory animals, such as lions and hyenas.
    Thorny bushes and grasses.
    Lack of shelter and exposure to elements.

 - Supporting Factors:
  Grasses and shrubs for potential food sources.
  Presence of waterholes and rivers for drinking and bathing.
  Acacia trees for shade and potential food sources.
  Animal herds for potential hunting and scavenging opportunities.
  Availability of rocks and sticks for constructing weapons and tools.

Taiga (Boreal Forest):
 - Dangers:
    Harsh winters and freezing temperatures.
    Limited food sources.
    Deep snow and difficult travel conditions.
  Dense Undergrowth and Fallen Trees
  Wildlife Encounters: The taiga is home to various wildlife species, including bears, wolves, and moose
 - Supporting Factors:  
  Coniferous trees for shelter and building materials.
  Abundance of berries, nuts, and edible plant roots.
  Forest animals, such as deer and rabbits, for potential hunting.
  Presence of freshwater lakes and streams for drinking and fishing.
  Availability of tree bark and sap for fire starting and medicinal purposes.
  
North european-style forrest:
 - Dangers:
    Cold temperatures and exposure to harsh winters.
    Dense vegetation and limited visibility.
    Wild animals, including bears, wolves, and boars.
    Lack of food sources during certain seasons.
    Potential for getting lost in the dense forest.
 - Supporting Factors:  
    Variety of tree species for shelter, firewood, and materials.
    Edible plants, mushrooms, and berries for food sources
    Abundance of small game, such as rabbits and squirrels, for potential hunting.
    Natural water sources, such as streams and creeks, for drinking and fishing.
    Availability of rocks and fallen branches for constructing tools and weapons.
  
  
Mangrove Forest:
 - Dangers:
    Dense and Intricate Root Systems
    Saltwater Exposure
    Limited Visibility
    Venomous Snakes and Marine Life
    Extreme Weather Events: tropical storms, hurricanes, and tidal surges

 - Supporting Factors:
    Abundance of Marine Life
    Availability of Freshwater
    Edible Plants and Fruits
    Natural Building Materials
    Navigation Aids: