## Metadane

* title: "Czarodziejka z woli Saitaera"
* threads: pieknotka-kaplanka-saitaera, saitaer-bog-primusa
* motives: niekompetentna-pomoc, magowie-pomagaja-ludziom, magowie-eksploatuja-ludzi, dotyk-saitaera, kara-nieproporcjonalna, dark-scientist, forced-transformation
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [190127 - Ixioński transorganik](190127-ixionski-transorganik)
* [190113 - Chrońmy Karolinę przed uczniami](190113-chronmy-karoline-przed-uczniami)

### Chronologiczna

* [190127 - Ixioński transorganik](190127-ixionski-transorganik)

## Projektowanie sesji

### Pytania sesji

* .

### Struktura sesji: Frakcje

* Kornel: przekonać Minerwę, by pojechała z nim
* Kasjopea: zobaczyć koszmar Minerwy
* Adela: pomóc Karolinie

DARK FUTURE: Wszyscy mają katastrofalne problemy

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Future

1. Karolina Erenit przypadkowo robi poważną krzywdę Wojtkowi
2. Wojtek traci magiczną moc
3. Sławek przypadkowo robi poważną krzywdę Wojtkowi
4. Adela jest uznana za winną tego co się stało

### Dark Future

1. Karolina jest uznana za istotę złą / Saitaera.
2. Adela jest uznana za winną
3. Sławek krzywdzi Wojtka i jest winny

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (20:00)

_Zaczęstwo, Osiedle Ptasie - w domu Minerwy_

Watch Pięknotki na Karolinie Erenit się odpalił. Wojtek poszedł po raz kolejny zabawić się z Karoliną. Pięknotka nagrywa ten widok. Wtem, gdy Karolina jest już na kolanach i z nieco mniejszą ilością ubrań, do pomieszczenia wpada NINJA! Ninja atakuje Wojtka pięścią. Gdy Wojtek zaczyna rzucać czar, ninja oblewa go eliksirem. Wojtek traci przytomność. Ninja chce wyczyścić pamięć, ale zapomniał zabrać czegoś na czyszczenie pamięci. Zwiał.

_Zaczęstwo, Osiedle Ptasie - w domu Karoliny_

Pięknotka wpadła do pomieszczenia by naprawić błąd i wezwała Tymona. Zmroziło ją, gdy wyczuła cień energii Saitaera. Energia zanika. Szybko ściągnęła Minerwę - ta rzuciła zaklęcie (TrM) i wyszło, że energia Saitaera pochodzi od Wojtka. Saitaer coś zrobił, najpewniej przeprowadza kolejny chory eksperyment.

Wpada ninja. A tam Pięknotka i Minerwa. Ninja szybko odpala eliksir z ostrym, ciężkim dymem. Pięknotka uruchamia power suit by go zatrzymać. (TpZ). Power suitowy sukces - Pięknotka zdewastowała część pokoju, ale ninja jest nieprzytomny. ONE HIT.

Pięknotka otwiera okno i obadywuje ninję. NIESPODZIANKA - Sławek z Czerwonych Myszy! A teraz leży nieprzytomny. Wpada na ten teren Tymon. Rzucił lekką iluzję - niech ludzie nie widzą dymu. Tymon obudził Sławka okrutnym wiadrem z lodowatą wodą. Szybko wyciągnęli, że Adela zrobiła mu eliksir a Sławek to zrobił, bo nikt nie chciał pomóc dziewczynie. Co to za eliksir? Coś, co zakłóca czarowanie. Przez parę dni gdy będzie chciał czarować to będzie czuł ten sam typ strachu co czuła Karolina.

Pięknotka poprosiła Tymona, by dobie poszedł. By jego tu nie było. Tymon powiedział, że może mieć przez to problemy jeśli to spora sprawa. Pięknotka zauważyła, że sprawa nie jest spora. Tymon się zgodził - poszedł sobie, ale zapowiedział Sławkowi, że jego (Tymona) tu nigdy nie było. Pięknotka powiedziała Sławkowi, żeby nie narażał Adeli (bo taki eliksir wyjdzie), po czym wypuściła go na wolność. Niech idzie i nie grzeszy więcej.

Minerwa nadal jest zaniepokojona. Na pewno wyczuła Jego energię dotykającą zarówno Karolinę jak i Wojtka. Pięknotka przekonała Minerwę, by ta spróbowała jednak użyć swojej mocy by zrozumieć co się stało. (TrM:S). Minerwa jest BARDZIEJ zaniepokojona po 30 minutach badań. ON przestanie być magiem. ONA zacznie. I nie jest to "puste kanały magiczne". Saitaer całkowicie przesuwa poziom energii magicznej z jednej osoby na drugą. Oraz wiedzę o magii i jak jej używać.

Jest to lekka katastrofa.

Pięknotka powinna to zgłosić. Ale zgłoszenie zrobi krzywdę wielu osobom - najmniej samemu Wojciechowi, ale Tymonowi, Sławkowi, Adeli, Pięknotce...

**Scena**: (21:00)

_Zaczęstwo, Osiedle Ptasie - w domu Karoliny_

Pięknotka zdecydowała się porozmawiać z Saitaerem...

Saitaer powiedział jej, że próbuje przenieść kanały i magię z maga na człowieka i zobaczyć, czy jest w stanie to zrobić bez efektów ubocznych. Wojtek należy do niego - Saitaer go uratował przed ixiońską transorganizacją. Dodatkowo, Wojtek i Karolina mają potężny kanał emocjonalny, oparty na pożądaniu i przerażeniu. Energia eliksiru Adeli oraz trochę jego energii i eksperyment gotowy.

Saitaer zaproponował Pięknotce kilka rozwiązań - on jest skłonny zainfekować Wojtka energią ixiońską i zmienić Wojtka w terrorforma. W ten sposób można ukryć co on zrobił by Wojtek mógł "legalnie" być pozbawiony mocy. Wystarczy, że Wojtek trafi na bagno - tam nikt się nie zdziwi, że wpadł w ręce Saitaera...

Pięknotce się to wszystko bardzo nie podoba...

Dobrze, Pięknotka ma Plan. Wie, że chce, by Karolina została magiem - niech będzie Mausem, bo dzięki temu będzie odporna na działania Saitaera i chroniona przed innymi magami. Wojtek straci moc magiczną - trudno, zasłużył sobie. A teraz trzeba tak wykonspirować, by to się stało.

Pięknotka musi chwilę poczekać. Ale wpierw - sprawa z Wojtkiem...

Minerwa i Pięknotka, zmęczone i zaziajane, wloką Wojtka do Cyberszkoły...

_Zaczęstwo, Cyberszkoła_

Pięknotka wie, że Saitaer potrzebuje jedynie energii. Użyła mocy Saitaera by zmienić Wojtka (6,3,6=S). Jakimś cudem Pięknotce udało się przekierować energię Saitaera w Wojtka bez Skażenia niczego dookoła. Łącznie z sobą. Przy okazji, Pięknotka miała zimne ciarki, że umie coś takiego zrobić...

Obudziła Wojtka, obsobaczyła go od góry do dołu i kazała wracać do domu. Sam Wojtek nie zdziwił się że jest w Cyberszkole - umysł jest już dotknięty przez Władcę Rekonstrukcji.

(pauza 60 min)

**Scena**: (22:38)

_Pustogor, Barbakan_

Trzy dni później. Karolina odkryła w sobie moc magiczną, co Pięknotka zauważyła. Karolina nie jest protomagiem - Saitaer przekazał jej wszystkie podstawowe zasady świata magów tak, jak zna je Wojtek. Karolina zachowuje się ostrożnie, z nieśmiałością, acz asertywnie.

Akurat Pięknotka miała poważną akcję - coś musiała zrobić na zlecenie Pustogoru, najpewniej papiery, ale TERAZ. I wtedy do Karoliny przyszedł inny uczeń Szkoły Magów. Ale Karolina sama jest czarodziejką...

Pięknotka ma zamiar obejrzeć show - co Karolina zrobi do obronienia się. Pięknotka odrzuca wszystko związane z papierami i leci do Zaczęstwa, by zapobiec katastrofie.

Czarodziej zaczął od dominacji. Karolina jest podatna, ale wtedy wkroczyły zabezpieczenia Saitaera. Karolina złamała zaklęcie i odpowiedziała najlepiej jak umiała, w strachu. "Spłoń". Pięknotka wpada na scenę, widząc maga w agonii i przerażoną Karolinę, która próbuje gasić go wodą. Pięknotka próbuje natychmiast ugasić go sprzętem terminuskim (9,3,3=SS). Udało się, ale Karolina wyskoczyła przez okno. Uciekła w miasto.

Na to przyleciał Tymon. Zajmie się rannym magiem. Pięknotka - za Karoliną. Wzięła taksówkę by być szybszą. I pierwsza - przed Karoliną - dotarła na miejsce. Do Cyberszkoły.

_Zaczęstwo, Cyberszkoła_

Pięknotka próbuje Karolinę przechwycić, zanim ta trafi do Cyberszkoły. Sukces - Karolina tam nie dotarła; Pięknotka ma ją wcześniej (Łt). Karolina jest spłoszona - czemu ktoś wszedł do jej domu i kazał jej się rozbierać? Co tu się dzieje? Pięknotka ma zatem nie takie łatwe zadanie.

Pięknotka zachodzi drogę Karolinie tak, by nie wyglądać bardzo groźnie. Karolina się zatrzymała; jest zmęczona, dyszy. Pięknotka się jej ujawniła i zaczęła z nią ostrożnie rozmawiać. Uspokoić ją. (Tp:S). Pięknotka ma szatański plan - ściągnąć Minerwę, by ta neuronautycznie zbadała czy wszystko w porządku. A jednocześnie, Karla będzie na pewno chciała przepuścić Karolinę przez Mausa.

Pojawiła się Minerwa. Przeskanowała Karolinę (TpM:S) i powiedziała Pięknotce, że Karolina jest arcydziełem. Ma wbudowane mechanizmy mentalne odrzucające negatywne wpływy wszelkiego typu. Świetnie adaptuje się do energii magicznych. Jej umysł radzi sobie z nieprawdopodobnym stresem. Doskonała konstrukcja mentalna. A do tego, elegancko stopiła się stara wiedza z nowym przeszczepem Saitaera - dla Karoliny, ma to wszystko sens - tyle razy czarowano na nią w polu magicznym, że wykształciła sobie energię. I nie ma śladu Saitaera.

Teraz Karla na pewno będzie chciała sprawdzić Karolinę pod kątem Minerwy/Saitaera.

Karolina, przerażona, poddała się terminusom bez walki.

Wpływ:

* Ż: 13
* K: 1

**Sprawdzenie Torów** (21:05):

2 Wpływu -> 50% +1 do toru. Tory:

* Karolina jako problem: 4 -> XV: magowie w szkole nie chcą jej przyjąć przez historię; w odróżnieniu od Bankierzy.
* Sławek krzywdzi Wojtka: niemożliwe
* Hold Saitaera na Karolinie: niemożliwe
* Adela uznana za osobę winną: 4 -> VX: skojarzyli sprawę Karoliny z Adelą. Śledztwo w sprawie Adeli.
* Minerwa jako zagrożenie: 4 -> XX

**Epilog** (21:10)

* Wojtek uruchomił w sobie terrorforma kilka dni później. Bez problemu został pokonany. Niestety, stracił moc magiczną.
* Karolina Erenit została wyczyszczona przez Karradraela do zera. Nie było możliwości nadania jej matrycy Mausa, ale dało się usunąć wszelkie wpływy Saitaera.
* Saitaer nie dostał swojej wymarzonej agentki, acz dowiedział się sporo o działaniu magów i ludzi.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
|               |         |             |

Czyli:

* (K): Pięknotka potrafi Skażać energią Saitaera.

## Streszczenie

Wojtek Kurczynos ZNOWU dobierał się do Karoliny Erenit. Przez machinacje Sławka i Adeli Saitaer dokonał transfuzji mocy magicznej od Wojtka do Karoliny. Pięknotka drastycznie ukrywała wszystkie wpływy wszystkich osób, by ucierpiał tylko winny Wojtek. Nawet współpracowała z Saitaerem, by terroformizować Wojtka. Wszystko, by Karolina zachowała moc magiczną. ALE - doprowadziła do tego, że spojrzeli na Karolinę Mausowie. W ten sposób Karradrael uniemożliwił hold Saitaera na Karolinie.

## Progresja

* Pięknotka Diakon: potrafi Skażać energią Saitaera. Niestety dla niej, umie przesyłać ixiońską energię.
* Karolina Erenit: stała się czarodziejką mocą Saitaera; odebrała moc Wojtkowi Kurczynosowi.
* Karolina Erenit: mocą Karradraela wyczyszczona z wpływów Saitaera - Władca Rekonstrukcji nie ma na niej holdu.
* Karolina Erenit: nielubiana przez wielu magów zwłaszcza w szkole z uwagi na korelację z jej uzyskaniem mocy i utratą mocy przez Wojtka.
* Wojtek Kurczynos: utracił moc magiczną. Jego moc trafiła do Karoliny Erenit z woli Saitaera.
* Saitaer: dowiedział się sporo odnośnie wykorzystania energii ixiońskiej do transfuzji energii magicznej od maga do człowieka. 
* Adela Kirys: sporo z tematów powiązanych z utratą przez Wojtka mocy zostało jej przypisane. Jest formalne śledztwo w jej kierunku.
* Saitaer: wszystkie plany w uzyskaniu nowej agentki (Karoliny Erenit) się nie powiodły. Trudno, nie dostał czego chciał.

## Zasługi

* Pięknotka Diakon: ukrywała wpływ Saitaera na Wojtka chroniąc: Adelę, Sławka, Karolinę i Minerwę. Wyintrygowała zmianę Wojtka w terrorforma by "stracił magię".
* Minerwa Metalia: główny detektor Saitaera i energii ixiońskich. Dodatkowo, przez to, że dotknęła energią Karoliny, dała pretekst Karli by ona sprawdziła Karolinę Karradraelem.
* Karolina Erenit: przestała być ofiarą. Gdy stała się czarodziejką, zaczęła działać inaczej - pomiędzy strachem a agresją. Dotknął ją Karradrael, by wyczyścić Saitaera.
* Wojtek Kurczynos: poszedł do Karoliny o jeden raz za dużo. Pobity przez ninję, zdestabilizowano mu energię, zmieniono w terrorforma i stracił moc magiczną na rzecz Karoliny.
* Sławomir Muczarek: ninja który na prośbę Adeli pomógł Karolinie i zdestabilizował energię magiczną Wojtka - dzięki czemu mógł zadziałać Saitaer.
* Tymon Grubosz: terminus który prawie wpadł w katastrofalne kłopoty, bo ukrył działanie Sławka i Adeli wobec Wojtka. Potem osłaniał Minerwę i Pięknotkę. O dziwo, przeszło.
* Saitaer: robił eksperyment w transfuzji energii maga do człowieka. Aktywnie pomagał Pięknotce w maskowaniu tego eksperymentu. Niestety, nie dostał tego co chciał.
* Adela Kirys: bardzo jej zależało by pomóc Karolinie Erenit. Przekonała Sławka i dała mu eliksir. Czyli - drugi raz zrobiła ten sam błąd, eliksir na nieznany target.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Akademia Magii, kampus
                                    1. Budynek Centralny: miejsce faktycznej manifestacji terroforma który kiedyś był Wojtkiem - bez problemu pokonany.
                                1. Cyberszkoła: tam Pięknotka i Minerwa zaniosły Wojtka i Skaziły go w terrorforma.
                                1. Osiedle Ptasie: gdy Wojtek przyszedł odwiedzić Karolinę, tam doszło do walki Wojtek - Sławek i tam doszło do ixiońskiego transferu mocy

## Czas

* Opóźnienie: 2
* Dni: 4
