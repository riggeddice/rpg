## Metadane

* title: "Osopokalipsa Wiktora"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [190406 - Bardzo kosztowne łzy](190406-bardzo-kosztowne-lzy)

### Chronologiczna

* [190406 - Bardzo kosztowne łzy](190406-bardzo-kosztowne-lzy)

## Budowa sesji

Pytania:

1. Czy Wiktor da radę doprowadzić do dramatycznej redukcji pieniędzy Sensoplex? (5)
1. Poziom paniki w okolicy (5)
1. Czy Pięknotka znajdzie bazę Wiktora? (3)
1. Zdrowie Pięknotki (3)

Wizja:

* Wiktor nie darował Sensoplex działań związanych z wiłami.

Sceny:

* Scena 1: Pięknotka pod Podwiertem (osoSkażeńcy)
* Scena 2: Chaos w Sensoplex

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena 1: Gdy uderzyły osy**

Pięknotka znajduje się pod Podwiertem, w obszarze jeszcze należącym do Podwiertu ale już nie centrum miasta. Wszyscy terminusi są zajęci jakimiś szerokimi atakami Skażeńców i nie wiadomo skąd się to wszystko wzięło. Szpital Pustogorski działa na podwójnej mocy. Więc - Pięknotka jest. Parę domków, ciężarówka Sensus (???) i cisza. A Pięknotka jest w zapasowym pustogorskim power suicie. Jedyne co słychać to bzyczenie owadów. W domku coś się ruszyło.

Pięknotka zajmuje miejsce z osłoniętymi plecami, ma też dwa konstruminusy do pomocy. Nie - był SOS z ciężarówki i koniec sygnału. Pięknotka jest na pozycji strzeleckiej a konstruminus ma spojrzeć do ciężarówki. Konstruminus podszedł do ciężarówki i w środku napotkał człowieka, wyraźnie uszkodzonego przez moc magiczną. Skażeniec rzucił się na konstruminusa który użył programowania by otworzyć ogień. W ostatnim momencie Pięknotka go powstrzymała by ogłuszył a nie zabił (Łt:11,3,2=S). Udało się, konstruminus unieszkodliwił jedynie ofiarę.

I konstruminus znalazł dziwne gniazdo os. A na człowieku jest ślad po ukąszeniu przez osę czy dwie. Pięknotka zaspawała ciężarówkę i poszła dalej. Jeden konstruminus został pilnując tego nieszczęśnika. Pięknotka poszła dalej; wzięła drugiego konstruminusa. Dom jest zamknięty; zabarykadowany. Pięknotka woła "obrona terytorialna". A w innym domu jest jeszcze ktoś. Pięknotka zauważyła, że ma 4-5 Skażonych ludzi i jeszcze do tego są owady na wolności... super. Jak to rozwiązać?

Pięknotka wezwała wsparcie. Dostała odpowiedź, że nie jest jedynym terminusem który potrzebuje wsparcia - wszystko ogniskuje się dookoła ciężarówek Sensus. Szpital Pustogorski poinformował, że to jest odwracalne; przygotowują serum.

Pięknotka zabarykadowała wszystkie domy w okolicy. Ludzie mają z nich nie wyjść. Zaraz potem wykorzystała gniazdo - chce wyprodukować to co je wabi. Zaklęcie. Niech tu przylecą i zostaną w ciężarówce. Najlepsza możliwa puszka i najlepszy możliwy wabik - przy gnieździe. Kosmetyki dla os ;-). (TrM:12,3,6=SM). Pięknotce zajęło to STRASZNIE dużo czasu i było STRASZNIE męczące; musiała zwalczyć energię Trzęsawiska. Poczuła dotyk Wiktora Sataraila... to on stoi za tą apokalipsą.

Do wieczora Pięknotka to opanowała i rozesłała recepturkę. Pustogor wysłał lekarza by ten zajął się ofiarami a Pięknotka pilnowała, by nie stała mu się krzywda. Sporo terminusów było zajętych tą akcją... Pięknotka jest pewna że to dywersja. I trzeba znaleźć o co chodzi.

**Scena 2: Powstrzymać Apokalipsę**

Pięknotka poprosiła Atenę, by ta sprawdziła dane historyczne okolicy. Gdzie był Wiktor, co robił? Sama próbowała dojść do tego o co Wiktorowi chodzi; nie jest głupia i zna Wiktora lepiej niż ktokolwiek inny. (Tr:P). Udało jej się dojść do tego o co chodzi Wiktorowi - Wiktor chce zniszczyć Sensus. I zaczął działać ostro przeciwko temu. Wiktor ZNISZCZY Sensus. Zmieni Sensoplex w swój dom.

Pięknotka ma dylemat - pomóc mu odepchnąć Sensus czy zatrzymać Wiktora. Wiktor będzie walczył dopóki Sensus nie wyczerpie zasobów finansowych - a czegokolwiek nie zrobił, to będzie tam, w Sensoplex...

Pięknotka w nocy pobiegła na Trzęsawisko. Musi się spotkać z Wiktorem Satarailem. Wysłała do niego sygnał przez lokalne bioformy. Ona wie, że Trzęsawisko odpowie na jej prośbę do niego. I Wiktor faktycznie się pojawił. Spytał ją, czy docenia to piękno. Pięknotka uznała, że docenia intencję, ale nie metody. Wiktor zauważył, że poprzednio był łagodny a Pięknotka go odparła. Nie miał wyboru, podniósł stawkę.

Pięknotka ma nowy pomysł - niech Wiktor podłoży dowody do Sensus. Dowody, które sprawią, że wszystko wskazuje na to że osy to ich sprawka. Wiktor powiedział wyraźnie - on nie widzi powodu by przerywać teraz. Teraz wszystko wygrał. Pięknotka odwróciła dyskusję - ona nie wchodzi na bagno niszczyć je dlatego bo ludzie cierpią. Niech on nie idzie do ludzi niszczyć ich bo bagno jest zagrożone. To wymaga ludzkiego podejścia, nie podejścia bagnistego. I ona to rozwiąże. (Tp:S).

Wiktor zgodził się z argumentacją Pięknotki. Może niech będzie tak jak ona chce, jemu też nie zależy na rozlewie krwi. Powiedział co zrobił - zatruł CAŁE jedzenie w Sensoplex. Larwami. One się wyklują, przejmą kontrolę nad swoimi celami i najpewniej rozwalą to wszystko w drobny mak. Pięknotka jeszcze jest w stanie to powstrzymać jeśli chce - ale Sensoplex w tym momencie nie zostanie przez nic zniszczony. Trzeba będzie znaleźć inny sposób.

Pięknotka ma plan. Skontaktować się z Kasjopeą. Niech ma reporterkę. Dodatkowo, zebrać oddział terminusów którzy zatrzymają problem. Wcześniej przygotować odtrutkę. I ma na to wszystko kilka godzin. I jest noc. Wiktor jej sprzyja - na prośbę Pięknotki dał jej próbkę os z bagna. Złożył KILKA próbek, żeby Pięknotka miała "dowody" że ktoś coś z osami robił. I zamaskował swoją aurę. (TpM:12,3,3=SM). Jako Efekt Skażenia - wygląda to jakby ktoś od Wiktora kupił takowe osy. Innymi słowy, wersja "ktoś próbował kupić" jest plausible. A jako że nikt Wiktora nie kojarzy z byciem podstępnym w ten sposób... taaak. Plan wygląda możliwie.

**Scena 3: Zebrać Drużynę**

Pięknotka przypomniała sobie o Kasjopei Maus. Normalnie unika tej świrki, ale tym razem Kasjopea jej się przyda. Pięknotka obudziła Kasjopeę i powiedziała jej, że jeśli jej imię KIEDYKOLWIEK wypłynie jako źródło, to koniec. Kasjopea się zgodziła. Pięknotka zaproponowała jej inną historyjkę - niech to KASJOPEA jest pierwotnym źródłem informacji. Wiadomo, że Sensus i Rexpapier walczą ze sobą; więc Kasjopea poszła nocą do Sensoplexu posznupać. A że wpadła na wybuch apokalipsy...

Kasjopea przerwała Pięknotce - to znaczy, że... że ona musi pozwolić na wybuch apokalipsy? Cała szczęśliwa. Pięknotka jeszcze raz przypomniała sobie czemu nie lubi pracować z Kasjopeą i czemu nie lubi Kasjopei - ale tym razem nie ma opcji. Ma sens. (Tp:S). Sukces. Pięknotce udało się z Kasjopeą złożyć coś, co wygląda bardzo wiarygodnie. Ona ma antidotum (to, co złożył Lucjusz w Pustogorze) i dowody (to, co dostarczył jej Wiktor). No i ma Kasjopeę która jest prawidłowym wyzwalaczem apokalipsy. Zostaje tylko czekać.

Chętnie by wezwała wsparcie wcześniej, ale... tia, jak? Wezwała Alana i powiedziała, że Kasjopea jest gdzieś tam i coś robi. Czy może być w pogotowiu jakby co. Alan ciężko westchnął i się zgodził. Rozumie co Pięknotka ma na myśli mówiąc o niestabilnej Kasjopei.

**Scena 4: Kontrolowana Apokalipsa**

Pięknotka jest tam na miejscu i czeka na ową zapowiedzianą przez Wiktora apokalipsę. Jest tam też Kasjopea która zawzięcie czeka by nagrywać. I apokalipsa się zaczęła - ludzie i magowie ulegli Skażeniu. Energia magiczna zaczęła wypływać spod kontroli. Jest źle. A Pięknotka nadkłada drogi by przyjść od strony bagna. Rozmiar utraty kontroli Pięknotkę zaskoczył - magia zaczęła szaleć.

Pięknotka i Kasjopea są jedynymi czarodziejkami które mogą coś zrobić żeby nikomu nic się nie stało. Kasjopea zeżarła więcej środków wspomagających niż to możliwe - to ją uodporniło przed osami i działaniami Wiktora, ale sprawi, że będzie miała najgorsze zejście EVER. Zdaniem Pięknotki - biedny Blakenbauer. Zdaniem Kasjopei - robi film "superbohater". Pięknotka wkracza od razu - natychmiast ogłasza alarm i wzywa m.in. Alana oraz ratuje kogo się da.

Pięknotka wybiera konkretną salę i odpala bombę wabikową. Chce pozbyć się os - niech to się nie rozlezie. (TrM+3:13,3,6=SS). Skonfliktowanie: Kasjopea jest bohaterką. Serio, ratuje ludzi, pilnuje tego wszystkiego, pomaga... SHE IS THE HERO. Aż Alan spojrzał "damn... przydała się". Kasjopea jest bardziej przydatna niż terminusi, aczkolwiek płaci za to koszmarną cenę. Plus destrukcja własności postępuje.

Przy tak przygotowanej sprawie udało się zatrzymać sprawę. Sensoplex jest uszkodzony, ale sprawa jest opanowana. A Kasjopeę zabrali do szpitala - organizm nie wytrzymał.

**Scena 5: Echo polityczne**

Pięknotka jest na debriefingu. Stare dobre "skąd wie, co wie...". Przedstawiła dowody, pokazała co i jak. Innymi słowy, pokazała fałszywe dowody i spróbowała przekonać, że to wina Sensus, choć trochę. (Tp+2:S). Pięknotka łgarz sprawiła, że jej wersja stała się niepodważalną. Sensus musi zapłacić (+1 tor redukcji kasy). Pięknotka chce ich wystawić Rexpapier na strzał, a najlepiej jeśli kompleks przejęłoby Luxuritias (którego członkiem jest Kasjopea).

Pięknotka musi jeszcze odpowiedzieć na pytanie - czy jest możliwe, że to wszystko jest intrygą Luxuritias i dlatego Kasjopea była na miejscu. Pięknotka powiedziała, że mało prawdopodobne.

TEST SENSOPLEX:

* za zachowaniem: upór, chęć odkrycia prawdy
* przeciw zachowaniu: osy, Wiktor, ślady, koszty, sporo wrogów, mało korzyści

(2,3,6=P). Sensus opuści Sensoplex. Wiktor Satarail osiągnął to, czego chciał - a dzięki Pięknotce nie doszło do apokalipsy.

Tory:

1. Czy Wiktor da radę doprowadzić do dramatycznej redukcji pieniędzy Sensoplex? (5): 3: KONIEC
1. Poziom paniki w okolicy (5): 2: pewna panika

Wpływ:

* Ż: .
* Zespół: .

**Sprawdzenie Torów** ():

2 Wpływu -> 50% +1 do toru. Tory:

* .

**Epilog**:

* Sensus odrzucił kontrolę nad Sensoplex
* Wiktor Satarail nadal poluje na Sensus, ale nie ma czego atakować

## Streszczenie

Wiktor zdecydował się zemścić w imieniu wił. Zaprojektował osy które robiły krzywdę magom i ludziom - wpierw użył ich jako dywersję, potem zaraził jedzenie w Sensoplex. Pięknotka dekontaminując okolicę odkryła plan; przekonała go, że można bezkrwawo usunąć stąd Sensus. I to zrobiła - z pomocą Kasjopei by ta zrobiła reportaż o "Osopokalipsie", po czym sfabrykowała dowody z pomocą Wiktora. W ten sposób Sensus opuścił teren i nikt szczególnie nie ucierpiał.

## Progresja

* .

### Frakcji

* Sensus: stracili kontrolę nad Sensoplex w Podwiercie; oddali ten teren, chwilowo nikt go nie używa.

## Zasługi

* Pięknotka Diakon: chroniła ludzi, Wiktora oraz Sensus - negocjacjami i akcją bezpośrednią. Minimalizowała szkody; nie dało się niczego tu wygrać.
* Wiktor Satarail: pokazał pełnię mocy tworząc osy by zniszczyć Sensoplex. Pięknotka przekonała go, że można ich wrobić i usunąć politycznie. Osiągnął cel.
* Kasjopea Maus: dziennikarka, chciała sfilmować apokalipsę Sensoplex; osłoniła Pięknotkę biorąc na siebie źródło pierwotne i wyszła na mega bohaterkę w Sensopleksie.
* Alan Bartozol: wsparcie Pięknotki. Jego wiedza o bagnie przydała się opanowując apokalipsę Sensoplexu.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert: pod Podwiertem doszło do dywersji - pierwsza fala bardzo niefortunnych os Wiktora
                                1. Sensoplex: w wyniku ukrytej wojny Wiktora i Sensusa o to miejsce, Sensoplex nie jest przez nikogo wykorzystywany
                        1. Trzęsawisko Zjawosztup: rozmowa Wiktora i Pięknotki oraz podstawowy biolab Wiktora

## Czas

* Opóźnienie: 3
* Dni: 2
