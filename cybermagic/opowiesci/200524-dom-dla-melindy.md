## Metadane

* title: "Dom dla Melindy"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [200510 - Tajna baza Orbitera](200510-tajna-baza-orbitera)

### Chronologiczna

* [200510 - Tajna baza Orbitera](200510-tajna-baza-orbitera)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* .

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

* .

## Punkt zero

.

## Misja właściwa

Pięknotka dostała prośbę. Trzewń znalazł coś, co może im pomóc w relacjach z Aurum. A dokładniej - zniknęła arystokratka. Rodzice się o nią martwią i zrobili oficjalny request. Jakieś kilka miesięcy temu zniknęła; request pojawił się 3 dni temu. I nikt się tego nie podjął; może wynikać to z tego, że ród nie jest specjalnie potężny. To zadanie faktycznie pomoże w relacjach z Aurum - to jest szczególnie istotne. Wcześniej nikt nie wiedział gdzie dziewczyna jest.

Wiadomo, że tu jest bo jeden z Rekinów Melindę rozpoznał. Dziewczyna nazywa się Melinda Teilert. Nie jest czarodziejką. Trzewń zrobił krótką analizę - działa m.in. w klubie podwierckim o nazwie "Arkadia", jako performerka kabaretu. Ludzie zadają jej pytania a ona odpowiada jak taka stereotypowa nic nie wiedząca o życiu arystokratka. Nie wygląda na szczególnie porwaną, zdaniem Trzewnia - może jej zmienili pamięć lub uciekła z domu. Tak czy inaczej, Aurum chce ją z powrotem a ona jest obywatelką Aurum, nie Szczelińca. Pustogor zaaprobował ten request - więc któryś terminus to powinien zrobić.

Pięknotka chce znaleźć Melindę. Proponuje Trzewniowi pójście razem do kabaretu. Trzewń się przestraszył - to miejsce GANGÓW. Tam jest GANG REKINÓW. Oni chodzą w łańcuchach; on jest niezłym magiem, ale kiepsko walczy. WOW, Pięknotka nie miała pojęcia o tej stronie Trzewnia. GANGI! W Podwiercie :D.

Pięknotka zdobyła bilety i wieczorem Trzewń i Pięknotka poszli obejrzeć występ. Sama Arkadia im się spodobała - przepych w stylu Aurum, ale jest tam chaos i naturalna dzikość. Pięknotka i Trzewń przysłuchują się odpowiedziom Melindy - wygląda na to, że ona naprawdę nie ma pojęcia o tym co się dzieje na świecie. Pięknotka na sali zobaczyła jednego z fighterów Rekinów, Franka Bulteriera. Gość po prostu lubi się bić.

PIęknotka poszła z Trzewniem na zaplecze, porozmawiać z Melindą. Tam jest wejście wyżej, do mieszkania Diany - tam Diana ją zatrzymała i powiedziała, że Melinda jest zmęczona. Piękntoka powiedziała że jest terminuską i chce porozmawiać o jej rodzinie. Diana to powtórzyła, Trzewń wyczuł, że Diana włączyła komunikator swoimi systemami. Pięknotka zauważyła, że Melinda nie musiała ostrzegać Melindy; Diana powiedziała, że sprowadzi Pięknotce Melindę na dół za 10-15 minut. Pięknotka uznała, że Melinda po ostrzeżeniu JUŻ zdążyła uciec...

Trzewń po drodze zrobił research na temat Melindy. Faktycznie, Melinda ma plany małżeńskie; uciekła z uwagi na to, że nie chce.

Po 15 minutach Melinda i Diana zeszły ubrane w idiotyczne peruki, ubrane tak samo. Pięknotkę uderzyło coś ciekawego. Melinda jest przebrana za Dianę przebraną za Melindę i vice versa. Czyli każda z nich jest tą którą jest, ale jeśli nie masz dobrej detekcji, skilli terminusa itp to możesz się pomylić.

Pięknotka powiedziała Melindzie, że rodzina jej szuka i dała nagrodę za przyprowadzenie jej z powrotem. Diana zauważyła, że to mała nagroda i terminusi się nie fatygują czymś takim. Plus, to nie w stylu Szczelińca. Melinda jest wdzięczna, że Pięknotka ją ostrzegła. Ta ją zatrzymała - zdaniem Pięknotki, Melinda ma wybór. Może zaprzeć się kierpcami. Pięknotka nie chce tłuc się z 2x-latką. Pięknotka sobie pójdzie i przyjdą inni.

Zdaniem Diany jeśli to nie są terminusi, to da się coś na to poradzić. Można wsadzić ich do dołu z myszami - a wszyscy się boją myszy.

Pięknotka pokazała Dianie i Melindzie stronę ze zgłoszeniem. Diana powiedziała, że to niewłaściwe i czy Pięknotka się z tym zgadza. Pięknotka unika odpowiedzi. Trzewń powiedział, że nie wiadomo czy ktoś nie namieszał Melindzie w głowie magią. A jest obywatelką Aurum. Diana spytała Pięknotkę, czy ta ma zamiar "porwać" Melindę. Pięknotka powiedziała, że niekoniecznie. Powiedziała Dianie, że chciałaby pomóc, ale ważne jest też to, że rodzina musi być usatysfakcjonowana.

Innymi słowy, "notka na JIRZE terminusów musi być zamknięta".

Diana powiedziała Pięknotce, że nie chce musieć chować Melindy swoimi metodami. Pomoże jej mafia Grzymościa, jeśli Diana poprosi - ale Diana nie chce się z nimi wiązać. Pomoże kuzyn Diany, jeśli Diana poprosi - ale to jest dokładnie to o co mu chodzi. Diana walczy o Melindę, ale nie chce stracić swojej duszy przy okazji. Nie chce stracić tego co ma i co robi.

Ale Pięknotka wpadła na fajny pomysł - wpakować Melindę Chevaleresse i Alanowi! To pomoże ustabilizować sytuację między tą dwójką i odciągnie ich myśli od ponurych spraw. Melinda jest teraz jak piesek którym trzeba się opiekować.

Diana, spytana czy Melinda w CZYMKOLWIEK jest dobra, odpowiedziała - tak. Nie jest zbyt mądra, nie rozumie wiele, ale za to tworzy piękne opowieści, potrafi opowieściami porwać publiczność czy słuchaczy. No i nie ma w Melindzie wiele zła. Plus, bardzo potrafi rozbawić, jest rozbrajająca i świetnie rozładowuje napięcie.

Yup, idealna do Alana :D. A Alan ma trzy pokoje w mieszkaniu :D.

Następnego dnia Pięknotka poszła odwiedzić Chevaleresse w szkole. A raczej - poza szkołą. Opowiedziała jej o Melindzie, która potrzebuje pomocy. Taka trochę rozpieszczona arystokratka, która jeszcze potrzebuje pomocy. I tylko ktoś taki jak Chevaleresse czy Alan mogą jej pomóc! A Chevaleresse ma okazję dać policzek rodowi magicznemu.

* VVV: Chevaleresse spotka się z Melindą, uzna, że trzeba jej pomóc i przekona Alana. A Alan nie będzie miał siły ani w sumie woli odmówić.
* XX: MNÓSTWO konfliktów, problemów, komplikacji w domostwie Alana. Na które Alan nie poradzi - bo nie ma siły. Ale dziewczyny obie chcą dobrze.

Cóż, przynajmniej Alan będzie wiedział że żyje.

EPILOG:

* Pięknotka zgłosiła znalezienie Melindy. Na prośbę o odzyskanie, Laura (studentka Tukana) zgłosiła, że jasne z przyjemnością - jak wydadzą z Aurum Małmałaza. Pięknotka spojrzała na Laurę z uznaniem za to. Tukan też spojrzał na Laurę z uznaniem - tu ich interesy są wspólne.
* Alan nie ma spokoju w domu.
* Siły Aurum wysłały najpewniej jakiś strikeforce by odzyskać dziewczynę. Do Alana. To nie mogło skończyć się dla sił napastników dobrze...

## Streszczenie

Rodzina chce Melindę z powrotem, Melinda nie chce wracać. Pięknotka szuka Melindy i znalazła ją w Arkadii. Gdy poznała sytuację, we współpracy z Dianą wsadziła Melindę Alanowi przy użyciu Chevaleresse. Wszyscy są zadowoleni, choć Melinda musi opuścić Podwiert - ale wreszcie jest bezpieczna.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: zaczęła od szukania zaginionej Melindy, skończyła na współpracowaniu z Melindą by ta nie wracała do domu; wsadziła Melindę Alanowi dla poprawienia humoru.
* Mariusz Trzewń: nigdy nie był w Podwiercie bo to "miasto gangów" a on nie umie walczyć. Pięknotka wzięła go na kabaret w Arkadii. Zainicjował szukanie Melindy.
* Melinda Teilert: nie jest szczególnie mądra, ale podobno jest świetna w opowieściach; Pięknotka zaczęła na nią polować by... dać jej dom u Alana, z Chevaleresse.
* Franek Bulterier: jeden z gangu Rekinów z Podwiertu; szczególnie zaprzyjaźiony z Dianą. Gość chodzi w łańcuchach (ozdoba) i lubi się bić, hobbystycznie.
* Diana Lemurczak: broniła Melindę jak ognia przed terminuską, przed rodziną. Jednocześnie - nie chciała związać się z mafią ani wysługiwać arystokratom. Wygrała. Wierzy w dobro terminusów.
* Diana Tevalier: Pięknotka ją przekonała, że jest taka Melinda która potrzebuje opieki i ma przechlapane. Sama Chevaleresse przekonała Alana że tak ma być. Alan nie podejrzewa Pięknotki.
* Laura Tesinik: Tukan chciał chronić Melindę przed Aurum. Pięknotka też. Laura całkowicie z własnej inicjatywy zablokowała ekstradycję Melindy i dostała podziękowania od obu.
* Gerwazy Lemurczak: chciał pokazać Dianie gdzie jej miejsce; wszędzie silny pożre słabego i nadał temat rodzinie Melindy. Ale Diana skutecznie zawalczyła o Melindę nie tracąc duszy.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Klub Arkadia

## Czas

* Opóźnienie: 2
* Dni: 3
