## Metadane

* title: "Wojna o Arkologię Nativis - nowa regentka"
* threads: kidiron-zbawca-nativis, regentka-arkologii-nativis, neikatianska-gloria-saviripatel
* motives: wojna-domowa, masowy-atak-mentalny, pozornie-w-kajdanach, tools-of-corruption, dotyk-nihilusa, perfect-vision, birutan
* gm: żółw
* players: fox, kapsel

## Kontynuacja
### Kampanijna

* [230628 - Wojna o arkologię Nativis - konsolidacja sił](230628-wojna-o-arkologie-nativis-konsolidacja-sil)

### Chronologiczna

* [230628 - Wojna o arkologię Nativis - konsolidacja sił](230628-wojna-o-arkologie-nativis-konsolidacja-sil)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * .
* CEL: 
    * Highlv
        * Ardilla szuka maga Nihilusa.
        * Muszę pokazać obecność maga Esuriit kontrolującego Trianai i efekt jego działań
        * Kapsel potrzebuje godnej, uczciwej walki w mechanice walki z inicjatywą i wymiarami
        * KTO przejmie kontrolę nad arkologią Nativis - Laurencjusz i Tymon czy Infernia?
    * Tactical
        * ?

### Co się stało i co wiemy

* Frakcje Arkologii
    * **Laurencjusz i Tymon + Izabella + Lobrak (LITL)**
        * impuls: take over, destroy Infernia's reputation, use powers to maneuever
            * mentalna: infernianie atakują arkologię, ludzie przechodzą na stronę Laurencjusza
            * polityczna: infernia zniszczyła ich ludzi, infernia nie ochroniła, Kidiron to potwór
            * Infernia nie ma surowców
        * slogan: "Kidiron musi zapłacić za zbrodnie!"
    * **Karol Lertys: 73-letni "dziadek" Celiny i Jana; kiedyś kapitan aspiriańskiego statku OA Erozja Ego + Celina + Janek**
        * impuls: fortyfikacja, tępienie Birutan, tępienie Hełmów, pozyskać surowce
            * kolejne tereny
        * slogan: "Wpierw ochrona, zwalczanie Birutan, odbudujemy naszą arkologię; rewolucja? XD".
    * **Szczury Arkologii**
        * impuls: loot, fortify, hide
            * loot, attack Lertys
        * slogan: "it is our time"
    * **Birutanie**
        * impuls: destroy and plunder
            * destroy and disassemble, force of nature
        * slogan: "DEVOUR"
    * **Rebelianci**
        * impuls: atak na LITL i Infernię
            * atak z zaskoczenia
        * slogan: "KIDIRON MUST DIE! Infernia must die!!!"
    * **Hełmy Draglina**
        * impuls: retake with iron fist. Destroy ALL opposition. Purge their ranks.
        * slogan: "Tylko Kidiron. PRAWDZIWY Kidiron."

### Co się stanie (what will happen)

* F1: NeoNativis KiKo pokazują że Infernia to potwory
    * ATAK MORALE
        * pokazanie że infernianie (piraci) zabijają cywili i niszczą dla niszczenia
        * puszczają rzeczy w radiowęźle, pokazując zbrodnie Kidirona i zapisy z Inferni (prawdziwe i sfałszowane; CES Mineralis, CES Purdont, dziewczynka trianai)
    * PRZEJĘCIE TERENU
        * 1) oddziały Hełmów p.d. KiKo atakują Farmy Wschodnie, przejmując Engineering (i przejmując znajdujące się tam osoby z Inferni itp. robiąc dodatkowe zapisy wizualne) "Nadchodzi nowe! Stare zostanie zniszczone! To co stare, przegrywa!"
        * 2) KiKo atakują Lertysów. "Kidiron to potwór, dajmy szansę KiKo!"
    * INCIDENTAL
        * ruch oporu przeciw Inferni i stający za KiKo. Ścierają się o najważniejsze obiekty, atakując często szaleńczo; Wujek musi ich eksterminować
    * PRIMARY
        * Izabela poluje na Ardillę, bo tylko ona może zabić Eustachego.
    * Wujek "gra w szachy - albo my usuniemy zniszczymy ich sztab, oni nasz albo Arkologia umrze..."
* SN: Ardilla napotyka Izabellę; atak mentalny i obrona Ralfa. "Nie pamiętam już jak nazywa się mój statek, nie pamiętam twarzy mamy..."
* SN: Wujek spróbuje uratować syna. Ale Tymon się wysadzi, zabierając Wujka ze sobą.

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Marcel Draglin: oficer Kidirona dowodzący Czarnymi Hełmami, dumny "pies Kidirona"
    * (ENCAO: --+00 |Bezbarwny, bez osobowości;;Bezkompromisowy i bezwzględny;;Skupiony na wyglądzie| VALS: Family (Kidiron), Achievement >> Hedonism, Humility| DRIVE: Lokalny społecznik)
    * "Nativis będzie bezpieczna. A jedynym bezpieczeństwem jest Kidiron."
    * Najwierniejszy z wiernych, wspiera Rafała Kidirona do śmierci a nawet potem. Jego jedyna lojalność jest wobec Rafała Kidirona. TAK, to jego natura a nie jakaś forma kontroli.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Ardilla - bardzo źle by było, gdyby Kidiron przyskoczył do władzy. Więc ONA weźmie władzę w imieniu Kidirona. Generalnie, plan jest taki by pogadać z Kalią, wyżalić i coś chce, hook na Kalię, potem przekonać Eustachego że on dostanie statki i i tak jest główną siłą militarną, poplecznicy Kidirona - Ardilla działa w imieniu Kidirona (najlepsza osoba do sterowania, uspokoi rebelię, 'użyteczna marionetka'). A potem przekona się Kidirona że Ardilla to najlepsza osoba na to stanowisko. W ten sposób Kidiron robi trochę mniej chorych rzeczy, dalej rządzi ALE nalot Kidironowy gaśnie.

Lobrak używa, jak się okazuje z danych Kidirona:

* może jest częścią Syndykatu? Agent zewnętrzny?
* niesamowicie silnych narkotyków uzależniających, też kralotycznych
* egzotycznych piękności (Izabela w obroży)
* neuroobroży, też warianty łamiące umysł
* POTENCJALNIE jest magiem? Były w jego pobliżu efekty magii mentalnej

Ardilla -> Kalia (ranna, ale przytomna, w skrzydle medycznym).

* A: Witaj, Kalio. Jak się czujesz?
* K: Żyję. Połowa rzeczy nie działa. W szpitalu.
* A: Trzeba Cię przenieść na Infernię jak dalej tak pójdzie
* K: Na Infernię.
* A: A jak ktoś spróbuje Cię przechwycić?
* K: Nie jestem kluczową częścią arkologii jak inżyneria, wiesz, nie zdradzę Arkologii. Więc niech próbują, zmarnują czas i siły.
* A: Nie o to chodzi. Muszę wprost że musimy Cię ratować? XD
* K: Masz rację. Oczywiście, Infernia to dobry pomysł. (lekki uśmiech) Czyli jednak Eustachego rozpraszam.
* A: No co Ty, jak Eustachy siądzie za sterami władzy, trudno go wyciągnąć.
* K: Masz rację.
* A+R eskortują Kalię na Infernię.
* A: Po tym wszystkim co się działo ostatnio mam mętlik w głowie. DOkopałam się do pewnych informacji od Kidirona, nie podoba mi się wszystko co tam czytam... Kidiron u władzy to najlepsze co nas spotkało, widać ile się posypało jak jest wyłączony, ale z drugiej strony... to nie jest lektura jaką nie chcę czytać jako osiągnięcia Arkologii... zaczęłam się zastanawiać czy nie da się obejść... wiesz, jakoś kontrolować co robi Kidiron. Na coś wpadłam, pomyślałam, że ja mogę oficjalnie przejąć władzę, Kidiron w cieniu, on zarządza i trochę go kontrolujemy...
* K: Czyli zamiast Laurencjusz i Tymon, Ty oraz Kidiron w tle, tak? I... ja lub (ostatni-żywy-Kidiron-nie-będący-Lycoris)? Czyli... w sumie postulaty... tej dwójki. Ale inaczej /myśli
* A: Ich postulaty nie są złe, nie widziałaś z kim się zadają. (opowiada o Lobraku)
* K: nie on...
* A: Właśnie o to chodzi. Podłożył im dziewczynę.
* K: (wzdrygnęła się)
* A: Kuzyn nie jest złą osobą, nie wiem jak drugi Kidiron, szansa wyjść na dobre ale nie z nim za plecami.

Kalii zależy na Kidironie, ale też uważa, że niekontrolowany Kidiron jest niebezpieczny. Kalia POMOŻE przekonać Kidirona do planu.

Kalia, Ralf oraz Ardilla BEZ LANCERÓW ewakuują się na Infernię. Ale jak?

SYTUACJA TAKTYCZNA DLA EUSTACHEGO:

* radiowęzeł konsekwentnie nadaje ZBRODNIE WOJENNE INFERNI.
    * -> niektóre kluczowe komponenty (inżynieria, lifesupport itp) są atakowane przez szaleńców.
* część piratów robi ataki terrorystyczne niszcząc losowo, co też nagłaśniane przez radiowęzeł
* oddziały "Hełmów" p.d. KiKo komasują siły koło Farm Wschodnich
* Wujek ostrzega, że sztab będzie zaatakowany i musi uderzyć w sztab wroga
* bardzo blisko, w radiowęźle jest centrala Lobraka. I on chce Kalię. ALE NIE WIE ŻE ONA TAM JEST.

Plan Eustachego - dwóch wymęczonych medyków ma przetransportować 'martwych' Ralfa, Ardillę i Kalię w czarnych workach na Infernię. Więcej trupów.

Tp +3:

* X (podbicie): siły Tymona wiedzą o działaniach Inferni, więc BLOKUJĄ Infernię w ten sposób.
* V (podbicie): Zespół dostanie się na Infernię
* X (manifest): POZA TRUPAMI IDZIE BOMBA
* V (manifest): Zespół SZYBKO dostanie się na Infernię.

Ardilla - ona ma unikalne umiejętności percepcji. Tr Z+ 3:

* V: Ardilla zna Tymona. To nie pasuje. Nic z tego nie pasuje. Chyba coś słyszałaś. CHYBA. 
* V: Ardilla skutecznie przekazała 'uwaga alert trupowy, coś jest nie tak'
* Vr: CAŁKOWICIE zneutralizowany plan KiKo

Okazało się, że w trupach były bomby. Infernia by została zniszczona.
Jeszcze raz Tymon -> Eustachy:

* T: Kuzynie, możemy połączyć siły, naprawdę. Dostaniesz wszystko czego chcesz.
* E: Więc przyrzeknij mi wierność
* T: To ja będę na szczycie, nie Ty. 
* E: Nie mamy o czym rozmawiać
* T: Masz rację, nie mamy...

Infernia -> Eustachy:

* I: (obraz Twojego zniszczonego Lancera. Po chwili pokazuje obraz Eustachego w Lancerze. I pokazuje generator Memoriam)
* E: NOT THIS TIME INFERNIA. "Na razie rozwiążę problem samemu, żeby pokonać własne ograniczenia i wspiąć się na wyżyny by być godnym"
* I: Godność nie ma znaczenia. Ograniczenia nie mają znaczenia. Nic nie ma znaczenia.
* R: Jeśli nie mają znaczenia, nie musimy ich usuwać
* I: Generatory Memoriam ulegną awarii. To tylko kwestia czasu. Entropia zawsze wygrywa. Rok, 10, 100...

Eustachy poprosił Draglina i Wujka o pójście i ze swoimi superkomandosami złapali radiowęzeł. A sam Eustachy skupia się na obronie Farm lub Inżynierii.

Tr Z +3:

* Vr (p): uda się wbić do radiowęzła
* Vz (m): uda się z relatywnie małymi stratami
* X (p): Izabella jest na miejscu, nikt się nie spodziewa jej jako maga i działa dyskretnie, ma maskę iluzji

Udało się Draglinowi i Wujkowi wbić do radiowęzła. Nie udało im się dorwać Laurencjusza - spierdolił w jakąś dziurę. Ale mają Izabellę.

Eustachy broni INŻYNIERII. Przeciwnicy mają ~10 scutierów. Atakujących jest sporo a na pierwszej linii piraci z Inferni. Eustachy ma to gdzieś. WALI W SWOICH, ale po nogach. "Nasi są pod wpływem uroku!". Próbujemy obronić inżynierię. Eustachy przygotowuje obronę tak, by wyglądało jakby zostali zaskoczeni. Chodzi o dewastację masową.

Tr Z +4 +3Og (MAULER DAMAGE):

* V (p): uda się utrzymać Inżynierię (na razie ze znacznymi stratami)
* V (m): Inżynieria utrzymana, małe straty, wysokie morale Inferni (mimo strzelania do swoich)
* Vr (e): jedna duża siła wroga została zniszczona. Zajście od tyłu. Eustachy dostaje cichy tytuł "Eustachy Pierwszy Straszny i Krwawy"
    * "Kto staje przeciwko Eustachemu, ginie"
* V (e): To staje się tytułem Eustachego na stałe. Krwawy Lord Eustachy. Demoralizacja przeciwników.
    * "Tam nie ma nienawiści, złości, tylko łagodna irytacja że jest przeszkoda."

Czas ułożyć z Kalią odpowiednią propagandę. Eustachy się łączy z nimi. Przygotowane kamery. Nagranie z totalnej dewastacji by puścić. Ardilla: te sceny jak wchodzą KiKo na nieuzbrojonych ludzi, próbują poskładać do kupy, próbują rozstawiać karty i zmasakrowani. Kalia idzie stabilizacją, łagodnością i optymizmem. Kalia próbuje zaprowadzić pokój w arkologii i przekazać dowodzenie Ardilli jako regentce.

Tr +3:

* V: Ludzie ogólnie stają za Kalią i jej słowami, oni TEŻ nie chcą wojny, jest uspokojenie
* X (p): Sporo ludzi wierzy, że to nie może być Infernia ani nikt z Inferni ani ze starego porządku
* Vr (p): Sporo osób chce stać za Ardillą, ale nie ma masy krytycznej
* X (m): Masa krytyczna stoi po stronie KALII ze wszystkich osób. Oni chcą ją za tymczasową regentkę. (+2Vg)
* Vr: Ardilla co prawda tymczasowo przekazała głosy Kalii, ale będzie walczyć w przyszłości z nią o włądzę w arkologii. Czyli "która laska będzie słupem Kidirona".

Ardilla i Ralf idą do radiowęzła. Radiowęzeł. Ekipa tam zarządza i zajmuje. Wujek i Draglin dyskutują ostro o strategiach. Wujek chce uratować Tymona, Draglin "dobry zdrajca to martwy zdrajca". Ardilla podaje koc Izabelli, ona przyjmuje go, ale nie do końca wie co zrobić. Nie ma instrukcji. Ralf patrzy na nią podejrzliwie.

ATAK MAGICZNY.

Ex +2 +3Ob +5Og:

* X (p): Ardilla jest wyłączona z akcji
    * Ardilla usiadła koło Izabelii, kocyk, rozmowa, co u kuzyna
    * I: "Jak długo Eustachy żyje, tak długo ta wojna się nie skończy"
    * A: "Gdzieś się musieli przenieść"
    * I: "Jeżeli chcemy powstrzymać zniszczenia, Eustachy musi zginąć. Tylko Ty możesz go zabić."
* Og: (JESZCZE odraczam o jeden krok)
    * Ralf przejął kontrolę magicznie
* X (m): Ardilla jest rekonfigurowana...

Potężny atak entropii. Zdarcie energii z Ardilli. Izabella ma wydrapane oczy (przez siebie). Powtarza w katatonii "Nie ma niczego. Żadnej nadziei. Nikogo innego. Nie ma narkotyków. Nie ma ukojenia. Nie ma nic. Nic."

Ralf stoi w kącie i się nie rusza. Po chwili się przewrócił. I wstał, acz ostrożnie. Gdy Ardilla pyta czy wszystko w porządku i co się stało, NAJPIERW jej nie poznał, ale po chwili odpowiedział "nie pamiętam rodziców. Nie pamiętam statku. Ty nie będziesz zapomniana." Ardilla udaje, że nic nie zrozumiała i "WOW! Ona była niesamowicie silną czarodziejką, coś jej się niezwykle spieprzyło!"

Ralf popatrzył na Ardillę dziwnie i nieco wytrącony z równowagi powiedział "tak, tak to prawda..."

## Streszczenie

Ardilla ma plan przejęcia kontroli nad Arkologią, w tle Kidiron przez nią kontrolowany. To też neutralizuje główne ataki L&T, że Kidiron taki zły. Z pomocą Kalii (ewakuowanej ze skrzydła medycznego) zbudowała linię propagandową i po odzyskaniu Radiowęzła, Kalia nadała wiadomość pokoju i pojednania. Eustachy zmiażdżył główne siły L&T koło Engineering, Kalia JAKIMŚ CUDEM została regentką przez przypadek a czarodziejka Syndykatu, Izabella została zmiażdżona w imię Nihilusa przez Ralfa chroniącego Ardillę.

## Progresja

* Eustachy Korkoran: dostał reputację 'krwawego sprawiedliwego lorda'. "Tam nie ma nienawiści, złości, tylko łagodna irytacja że jest przeszkoda." Zabija WSZYSTKO co staje mu na drodze i nic innego.
* Ardilla Korkoran: ma potencjał przejęcia kontroli nad Arkologią Nativis w swoim imieniu, a w tle Kidiron.
* Kalia Awiter: przypadkowo została tymczasową regentką Arkologii Nativis, stoi za nią Ardilla i Infernia. A docelowo - Rafał Kidiron.

## Zasługi

* Ardilla Korkoran: doszła, że Lobrak jest aktywnym agentem Syndykatu i za wszystkim stoi mag; chce przejąć władzę nad Arkologią i współpracuje z Kalią. Ewakuowała Kalię i wpadła pod zaklęcie czarodziejki Syndykatu; zanim ta przeformatowała Ardillę, Ralf uratował Ardillę niszcząc umysł czarodziejki Syndykatu w imię Nihilusa.
* Eustachy Korkoran: wymyślił sposób ewakuowania Ardilli i Kalii przez worki na ciała na Infernię, zmiażdżył główne siły L&T oraz pozbawił ich maga. De facto - militarnie kontroluje Arkologię a Kalia ma rząd dusz.
* Kalia Awiter: mimo ran, została ewakuowana na Infernię i tam zrobiła akcję propagandową stając za Ardillą. Poszło jej niespodziewanie dobrze i to ONA została tymczasową Regentką Arkologii.
* Bartłomiej Korkoran: z Draglinem wbił się i zdobył Radiowęzeł. Chce uratować syna, Tymona. 
* Marcel Draglin: z Wujkiem wbił się i zdobył Radiowęzeł. Chce zniszczenia wszystkich zdrajców.
* Tobiasz Lobrak: okazało się, że jest aktywnym agentem Syndykatu i najpewniej on stoi za Infiltratorem i atakiem na Arkologię Nativis. Traci kontrolę nad Arkologią i atakiem. Stracił maga Syndykatu.
* Izabella Saviripatel: agentka Syndykatu, 'złapana' przez Wujka i Draglina, spróbowała gambitu mającego przeprogramować Ardillę do zabicia Eustachego. Jej umysł zniszczył Nihilus/Ralf w obronie Ardilli. Praktycznie KIA.
* Ralf Tapszecz: cień Ardilli, ochronił ją przed atakiem mentalnym przepisującym ją wzywając energię Nihilusa niszcząc umysł Izabelli. Jego ostatnią wartościową kotwicą jest Ardilla.
* OO Infernia: najbezpieczniejsze miejsce w okolicach Arkologii Nativis. Będące anomalią Nihilusa kuszącą Eustachego do zdjęcia kolejnych generatorów memoriam.

## Frakcje

* Infernia Nativis: przejmuje kontrolę nad Arkologią Nativis używając brutalnej pięści, terroru i łagodności Kalii XD.
* NeoNativis KiKo: tracą poszczególne elementy i kontrolę, stracili maga, główny obszar itp. Sytuacja dla nich robi się desperacka.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Nativis
                        1. Poziom 2 - Niższy Środkowy
                            1. Południe
                                1. Engineering: miejsce straszliwej walki pomiędzy siłami Inferni a atakiem KiKo. Eustachy zmasakrował napastników z niewielkimi uszkodzeniami strukturalnymi i małymi stratami.
                        1. Poziom 3 - Górny Środkowy
                            1. Południe
                                1. Medical: tam była Kalia, kluczowa dla Lobraka. Ewakuowana przez 'worki z ciałami'.
                        1. Poziom 4 - Górny
                            1. Wschód
                                1. Radiowęzeł: zmiażdżony przez siły Draglina i Wujka; baza KiKo została zdobyta, łącznie z Izabellą (co sprawiło, że Engineering poszedł tak dobrze)

## Czas

* Opóźnienie: 0
* Dni: 1
