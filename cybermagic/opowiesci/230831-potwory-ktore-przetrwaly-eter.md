## Metadane

* title: "Potwory, które przetrwały Eter"
* threads: brak
* motives: dreszczowiec, body-horror, lokalne-mity-i-legendy, naprawa-bledow-przeszlosci, pekniecie-kolejnej-pieczeci, stalam-sie-potworem, starcia-kultur, potwor-lewiatan, echa-inwazji-noctis
* gm: żółw
* players: kić, flamerog

## Kontynuacja
### Kampanijna

* [Potwory, które przetrwały Eter](230831-potwory-ktore-przetrwaly-eter)

### Chronologiczna

* [230201 - Wyłączone Generatory Memoriam Inferni](230201-wylaczone-generatory-memoriam-inferni)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * "Delain - Creatures"
        * Our stories all've been told
        * But you survived, you are the night
        * When your back's against the wall, who will catch you when you fall...
    * "Equilibrium - Eternal Destination"
* THEME & VISION:
    * There are nightmares greater than nightmare...
    * Magic always wins
* CEL: 
    * Highlv
        * ?
    * Tactical
        * ?

### Co się stało i co wiemy

Gracze:
    
* Scena zero:
    * Kić: Esaria: komandoska Coruscatis
    * Flame: Rosenkrat: komandos Coruscatis
* Reszta scen: 
    * Kić: Amelia, pajęczy terrorform z dziesiątkami broni, ATV (inżynier, była magiem)
    * Flame: Dawid, "Skażony Miragent", "mimik" (lekarz)
* Sojusznicy: 
    * "Braciszek" ('bronić DIANĘ!', entropik-class , kiedyś: brat Amelii )
    * "Mateusz" ('corrupted Perceptor', wężoform z laserem, kiedyś: snajper, syn Alberta)

Historia:

* Travistas był uszkodzoną jednostką próbującą się ukryć, zapolował na niego 'Śmiech Sępa'
* Travistas włączył Silnik i porwał ze sobą Śmiech; rozbili się na Neikatis
    * Obie jednostki przeszły przez to agonalnie
        * Dowódca Sępa, Albert, stał się Upiorem Neikatis
* Diana użyła mocy by zregenerować tych których mogła
    * Wołanie z Sępa jest dla niej za mocna. Ona i Kamil poszli w tamtą stronę. Zresetowała tych co mogła.
        * Diana zostawiła to co mogła - projekcję. Ale potrzebny jest sprzęt którego nie ma tu - jest na Sępie lub w Stacji Damnos.

### Co się stanie (what will happen)

* Cel strategiczny: złożony
    * Albert i Diana: "Dokończyć dzieła. Zniszczyć WSZYSTKO co pochodzi z Noctis."
    * Diana, kiedyś: "Chronić ludzi. Tu jest coś strasznego!"
        * flashback: "musimy chronić... ludzi... przed potworami..." (gdy jej piękna twarz zaczyna spływać a pod nią pojawiają się kable)
        * flashback: "PAJĄK poluje na trianai, łapie je i wyciąga z nich biopapkę do asymilacji przez Zespół"
    * Stacja Damnos: "Ustabilizować się, rozwinąć, przetrwać"
        * flashback: MIMIK trzymał niebezpieczny teren, by dzieci mogły przejść podczas awarii
        * flashback: MIMIK uciekał ze zwłokami
    * Oddział Coruscatis: "Zniszczyć potwory i przejąć kontrolę dla ludzi!"
* Lokacja:
    * Objęta martwa arkologia
    * Martwa Pustynia Neikatis
* Sceny
    * S0: infiltracja by odzyskać dzieci
        * jedno nie żyje, drugie w klatce (pajęczak spali rękę, braciszek próbuje zabić)
    * S1: potwory
        * znowu ten sam sen z JEJ twarzą
        * brak sprzętu, zasobów, biomasy - trianai? zwłoki?
        * nie mamy od niej wiadomości
        * ciągnie Mateusza do ruiny Sępa
        * podobno 
    * Sn
        * atak Arkologii na Sępa
        * atak Sępa na Arkologię (mechanical swarm, trianai swarm)

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki
#### 1. Oryginalne siły Stacji Damnatios (noktianie)

* Aeva Salrian: doświadczony inżynier Travistasa, chce uratować kogo się da i uciec z tego miejsca. Tymczasowo dowodzi.
    * OCEAN: (N-C-): Skoncentrowana, perfekcjonistka, skupia się na szczegółach i ma tendencję do bycia zamkniętą w sobie.
    * VALS: (Achievement, Security): "Jedynym sposobem, by przetrwać, jest naprawa tego statku. Musimy dać z siebie wszystko."
    * Core Wound - Lie: "Nie wiem co robić a Potwory zabiły moje dziecko!!!" - "Twarda dykatura jest jedynym rozwiązaniem problemu."
    * Styl: Skupiona, zawsze z datapadem w ręku; chce zrozumieć i naprawić uszkodzenia.
* Simeon Rejter: Lekarz pokładowy
    * OCEAN: (A+O+): Empatyczny, otwarty, zawsze gotowy do niesienia pomocy. Ma dużo dobrych pomysłów
    * VALS: (Benevolence, Universalism): "Każde życie jest cenne i każda forma ma sens, musimy pomagać sobie nawzajem."
    * Core Wound - Lie: "Gdyby nie moja chęć pomocy, nie rozbilibyśmy się tu" - "Zostawię dowodzenie tym, którzy się na tym lepiej znają"
    * Styl: Opiekuńczy, zawsze z apteczką, szybko reaguje na potrzeby innych. Widać w nim wieczne poczucie winy.
    * Metakultura: Klarkartianin: "Dopóki jest życie, jest nadzieja. Nieważne jak jest źle."
* Lurius Kasimir: Specjalista ds. komunikacji
    * OCEAN: (N+C-): Nieśmiały i impulsywny, ale kiedy przychodzi co do czego to robi robotę dobrze
    * VALS: (Conformism, Security): "Wpierw ufortyfikujmy to co mamy, zabezpieczmy się przed potworami"
    * Core Wound - Lie: "Miałem szansę ostrzec ich wcześniej, ale się wahałem." - "Jesteśmy zgubieni, nie ma nadziei"
    * Styl: "żywy trup"
* Karol La Viris: Cyberwspomagany oficer wojskowy
    * OCEAN: (A-E+): Zdecydowany i agresywny. Maskuje rozpacz agresją. Deathwish.
    * VALS: (Power, Security): "Ochronię wszystkich. Jakoś. Nie wiem jak. Dowolna broń."
    * Core Wound - Lie: "Potwory niszczą mój świat a ja jestem bezradny!" - "SZTUCZNY OPTYMIZM I ZNISZCZENIE POTWÓR!"
    * Styl: "Wszystko będzie dobrze /otwarty śmiech"
    * Metakultura: Atarien ("wszyscy polegają na mnie a ja dowodzę... tym co mam.")
* Aurelia Manalis: zajmuje się dziećmi, kiedyś naukowiec (astrobiolog)
    * OCEAN: (E+A+): przyjazna, pozytywna i zawsze miło z nią pogawędzić. Symbol światła.
    * VALS: (Universalism, Benevolence): wierzy w wartość wszystkich i symbol pozytywnego jutro
    * Core Wound - Lie: "straciłam moc magiczną, a teraz naukę" - "dam dzieciom lepsze jutro"
    * Ciepła, cierpliwa, zawsze z uśmiechem na twarzy. Ma talent do przekazywania skomplikowanych koncepcji w prosty i zrozumiały sposób
    * Metakultura: Klarkartianin

#### 2. Nowe nabytki Stacji Damnatios (noktianie)

* Kalista Luminis: czarodziejka płomieni, savaranka, CES Coruscatis
    * (ENCAO:  -0+0- |Robi co każą;;Nie znosi być w centrum uwagi| VALS: Family, Security >> Stimulation| DRIVE: Ujawnić prawdę o Trianai)
    * "Te wszystkie zniszczenia, te śmierci - to Wasza wina!!!"
* Kratos Coruscatis: wspomagany komandos noktiański; posiada wyrzutnię rakiet i servar klasy Fulmen, dekadianin, CES Coruscatis
    * (ENCAO:  --+00 |Kocha silny stres;;Introspektywny;;Pracowity| VALS: Benevolence, Conformity >> Family| DRIVE: Zwyciężyć w debacie o współpracy z Neikatis)
    * "Od początku mówiłem, że ta współpraca z Nativis się nie uda. Noctis powinna współpracować z noktianami."

#### 3. Potwory

* Diana Nałęcznik:
    * OCEAN: (A+E+): Niezwykle empatyczna, o ciepłym sercu, gotowa poświęcić się dla innych.
    * VALS: (Benevolence, Universalism): "Dla moich przyjaciół zrobię wszystko. Oni są moją rodziną."
    * Core Wound - Lie: "Uratowali mnie. Pomogli mi w potrzebie. A potem umierali - jeden za drugim" - "Nigdy więcej nie zostawię nikogo w potrzebie, poświęcę WSZYSTKO"
    * Styl: Jej oczy zawsze patrzą uważnie na otoczenie, szukając sposobu, by pomóc.
    * Metakultura: Sybrianin: "Współczucie i troska o innych to nasza największa siła. Przyjaźń jest wieczna."

### Scena Zero - impl

Awangarda z Coruscatis, duża siła ognia. Wczoraj potwory napadły i porwały dwójkę dzieci. Z serca arkologii.

Rosenkrat podbija do Karola. Jak to się stało.

* LV: "Nie wiem. Nie mam pojęcia. One wpadły i wypadły."
* LV: dał wizję na potwory
    * cztery, bez MIMIKA

Rosenkrat zbiera dane o potworach by Kalista mogła powiedzieć jak to ruszyć.

Tp +3:

* Vr: masz wystarczające dane
    * WSZYSTKIE potwory są magiczne
    * WSZYSTKIE potwory są na szkielecie ludzi (były ludźmi)

Lokalsi powiedzieli ci o wraku:

* wrak jest kawałkiem astoriańskiej jednostki "Śmiech Sępa".
* Śmiech Sępa ich napadł. Lokalsi nie są jednostką wojskową. Travistas skoczył.
    * mostek Śmiechu Sępa jest oddalony 10 km od potworów. One nie są TAM. Nie są tutaj.
* Lokalsi opłakują dzieci, bo one posłużą jako "baterie i żywność"
    * "LaViris: potworów było 5, zostało 4. Za duże straty."
    * "Nie poświęcę snajpera by zabił dzieciaki by nie cierpiały. Dzieci jako części zamienne."

Rosenkrat zaproponował - infiltracja dwóch osób. LV proponuje by tego nie robić. Rosenkrat jest za tym. 

* Niech LV zrobi dywersję
* Rosenkrat spróbuje z Esarią się przekraść i dostać do Głowy Sępa.

Plan jest dobry - dywersja z jednej strony robiona przez lokalsów, Zespół się zakrada. P: (Druga Strona nie posiada detektorów zaawansowanych)

Tp +3:

* X: dywersja była bardzo skuteczna, są ranni "po obu stronach" i wy musieliście działać cicho
* X: odciąć wszelkie komunikacje do momentu powrotu - nikt od Was nic nie usłyszy
* X: bardzo ciężko uszkodziliście jednego potwora ale dotarliście. Dotarliście "w ogniu". Trzeba było go zgubić.

Dotarliście do celu...

### Sesja Właściwa - impl

"Musimy chronić ludzi przed potworami!". Diana. Spłynęła jej twarz. 3 dni stracił Dawid. Mateusz (wąż) "wszystko w porządku?".

* Amelia: OK
* Mateusz: Straciliśmy Kamila. I Dianę.
* Amelia: ?
* Mateusz: Kamil zbliżył się do Mostka, tam gdzie zniknęła Diana.

Dwie siły. Coruscatis (nas zmiażdży).

Musimy przedstawić się arkologii jako nie-źli. Czy jest widoczna ta choroba? W martwym ciele - lekarz, który robi badania na sprzęcie to wykryje. Amelia to unieczynniła - EMP.

1. Jesteście potworami
2. Lokalna arkologia nie ma jak Was pokonać
3. Coruscatis ma siłę by Was pokonać
4. Centrala - tam coś się stało i Wy nie możecie się tam zbliżyć. Damian próbował zatrzymać Infiltratorów i ZNIKNĄŁ. Uległ Asymilacji przez Centralę.
5. Diana zabezpieczyła Waszą piątkę
    1. Diana zostawiła projekcję, ale nie ma komponentu
6. Macie niedomiar sprzętu. Sojusz z Arkologią - nie ma problemu
7. Dianie coś się stało a Wy nie do końca pamiętacie co się dzieje

Mimik wchodzi w głowę Braciszka. Dowiedzieć się czego nie wiedzą. "Jak próbowaliśmy się skontaktować z Arkologią i czemu nie wypaliło". Diana na pewno by próbowała. Na pewno. Czemu się nie udało? Nawet że Diana była SOBĄ. Co ona twierdziła po próbach.

Tr P +3 +3Or (pamięć Braciszka):

* Or: Braciszek przypomni sobie Amelię i co się stało z Dianą (na koniec, uspokoić)
* V:
    * Diana była bardzo, bardzo słaba. Ten czar jej bardzo zaszkodził. Ona "mówiła do powietrza".
    * Wszyscy zdestabilizowani
    * Nie próbowaliście się skontaktować z Arkologią
    * Nie ma nic wskazującego na okrucieństwo z Waszej strony wobec arkologii
    * SCENA: Mimik chroni, Pająk naprawia uszkodzone fragmenty arkologii
* Vz:
    * Macie to. Macie to ponagrywane.
* Vr: 
    * Braciszek widział zniknięcie Diany. 
        * "wojna jest skończona", "nie chcę tego robić", "Ty nie przeżyłeś, ja tak..."
        * WSZYSCY mieli podobne efekty. Diana wysmażyła Wasze systemy. ALe nie zdążyła wszystkich.
        * DIana poszła tam. Z dwoma osobami.
        * -> Diana stała się częścią Centrali. Już nie jest sobą.
        * Centrala nie miała siły. Ale infiltracja wzmocniła Centralę.
* Vp: Braciszek dostaje część funkcji wyższego rzędu, ale nie buduje nowej pamięci.
* Or: Mimik ma dostęp do sygnałów które słyszy Braciszek
* Vz: Braciszek generuje pamięć długoterminową

Braciszek zawył żałośnie. Przypomniał sobie że Amelia jest siostrą.

* Mimik: "Jak mamy dojść?"
* Diana: "Nigdy Was nie zawiodłam i nie zawiodę. Przysięgam, przyjdę i Was uratuję!" /Diana z przeświadczeniem
    * Diana była żoną Mimika

WŁAŚNIE! Astorianie zajęli opuszczoną Arkologię. Czyli Arkologia była opuszczona i noktianie ją zajęli.

.

1. Chcemy stworzyć wiarygodny sygnał od żołnierza "infiltracja była błędem, nie zbliżajcie się tam, silna magia"
2. Chcemy współpracować z lokalsami

Mimik spożywa zwłoki Rosenkrata. Od razu pojawia się pamięć, echa, pojawia się jego rodzina...

* Diana: "To byliśmy my..."

Mimika i Rosenkrata łączy strata. Sytuacja. Nadawanie sygnału SOS siądzie bardziej. Nie jest odrealniony. +P. Korzystając z pamięci, zwłok itp chcesz nadać sygnał który powie Coruscatis "byliśmy w awangardzie, mieliśmy sprawdzić, nie zbliżajcie się, silna magia, nie ratujcie." Plus "zabierzcie cywili ZANIM to coś zwiększy moc nimi, WIDZIAŁEM TO, dla nas nie ma już nadziei, powstrzymam to jak mogę...". Plus - "widziałem że ci z wraku z tym walczą". Plus dobrze ubrane. "Dla nas nie ma już nadziei, tu jest więcej co się wydaje, zabierzcie stąd lokalnych noktian"

1. nie zbliżajcie się i nie ratujcie mnie: P/M
2. potwory walczą z tym co tam jest, to byli ludzie: S
3. zabierzcie stąd cywili: P/M

Tp+4+3Ob:

* Vr: p: wejść w Rosenkrata i przekierować jego, jego uczucia do rodziny którą chce chronić itp. Nikt nie ma wątpliwości że to Rosenkrat.
* Vr: m: masz gwarancję, że Coruscatis nie będzie się tam zbliżać.
* V: p: Coruscatis akceptują to, że byliście ludźmi i z tym walczycie.
* V: m: Corustatis jest otwarte na komunikację z Waszej strony ORAZ zostawi Wam sprzęt by Was wspomóc w walce z TYM co zabiło ich ludzi
* Vr: ex: Corustatis docelowo wchodzi na "otwarty sojusz" z "potworami". Oni aktywnie próbują, byście przetrwali i Wy ich aktywnie chronicie, acz się nie spotykacie ale komunikujecie.
* V: s: Coruscatis ewakuuje stąd cywili. Ta arkologia jak była martwa tak martwa pozostanie.

.

* Wiadomość od Diany: "Wiesz, że zawsze Cię znajdę. Obiecałam. Nie zostawię Cię samego. Do śmierci a nawet potem. Kocham Cię."
* MIMIK to ignoruje. Wie, że TA Diana która była by była z niego dumna.

Byliście skupieni na konstrukcji sygnału. NIE MA BRATA. Ani dziecka. Od razu lokalizujesz. Braciszek niesie dziecko w kierunku na Centralę. (był bratem Amelii)

* Amelia: "Chodź, potrzebuję Twojej pomocy."
* Braciszek: "Chronię Amelię. Diana powiedziała, że ochronię Amelię. Zniszczę potwora. To dziecko ochroni Amelię."
* Amelia: "To dziecko ochroni Amelię jeśli będziesz... (flashback: jako dziecko rzucił się na starszych chłopców którzy dokuczali Amelii)"
* Amelia: "Braciszku... wróć do mnie. Potrzebuję Cię tutaj. Tu i teraz. Boję się."
* Braciszek: "Siostrzyczko... obronię Cię. Jak kiedyś."
* Amelia: "Bez Ciebie obok mnie tracę siebie..." (+P)

Tp +4:

* V: p: wróci z dzieckim
* V: m: Braciszek nie zostawi Amelii. Będzie z nią.
* V: ex: WIE że jest uszkodzony i WIE, że musi Amelii słuchać, bo sam nie jest w pełni sił umysłowych

Braciszek, najpotężniejszy, wraca z przerażonym chłopcem bez ręki.

Chłopczyk ma "złamane morale", ma wszystko gdzieś (bo przekroczył element paniki). Mimik siada przy chłopcu i tłumaczy:

* Chcieliśmy zwrócić, ale problem że Braciszek zbyt chętny i pomylił kierunki bo nie jest zbyt mądry
* "Byłeś chory i..." (Mateusz zaproponował: 'zmieniałeś się we mnie') "...nie chcieliśmy by spotkało to Ciebie i innych w Arkologii. Tylko Ty jesteś w stanie nam pomóc" (jesteś w stanie pomóc) "Jesteś dzielny i możesz wszystkich uratować"
* To przez komunikator by przekazał dowódcy

Co chcemy:

1. By posłuchał i nie uciekł -> By dało się wyleczyć traumę XD -> By nie było traumy
2. ......................... -> By przekazał co się stało
3. ......................... -> By przekazał komunikator
4. ......................... -> "Uwiarygadnia to wiadomość do i od Coruscatis"

Tr P (nagranie + demonstracja) +3:

* V: posłucha i nie ucieknie. Obecność potworów, i to takich co MÓWIĄ... będzie grzeczny
* Xz: jest grupa cywili dla których to wszystko jest fabrykacją i fake news. Bo potwory CHCĄ ich zabić i wyciągnąć stąd.
* Vz: uwiarygodnienie wiadomość do i od Coruscatis
* Vr: dzieciak zrozumiał prawidłowo co mu się stało i dlaczego.

Dziecko będzie miało straszną traumę po wszystkim. 

Dziecko zrobi Wam "warm introduction" do Arkologii. A jednocześnie Coruscatis też potwierdza Arkologii, że to co mówicie jest prawdą.

Mimik przekrada się z dzieckiem do Arkologii. By dostać sie do wewnętrznych części i tam wejść do pokoju szefa ochrony. I będzie grał byłym dowódcą. WĄŻ ROBI DYWERSJĘ POJAWIA SIĘ I ZNIKA! Wąż poszedł na spacer.

Tp +3:

* X: Wąż Mateusz został lekko ranny, nie przywykł do pokazywania się. Nic poważnego, Amelia naprawi.
* V: p: Wślizgnęliście się do arkologii niezauważenie
* V: m: W pokoju szefa ochrony. Też niezauważenie

Karol nie śpi. Monitoruje "Poważny Atak Mateusza". Otwierają się drzwi, cicho, i wchodzi chłopczyk wraz z Mimikiem w formie bloba. Karol się odwraca i STAJE. Oczy -> broń.

* Chłopiec: "Panie Karolu, to ja. Wróciłem. I nie mam ręki."
* Karol: "Nic Ci nie powiem."
* Chłopec podaje urządzenie Karolowi: "To pozwoli się skomunikować. Będzie można z nim rozmawiać. Powiedział żebym dał."
* Karol (ostrożnie, z prędkością lodowca bierze kom i go włożył)

.

* Mimik jako poprzedni dowódca: "witaj stary druhu, ostatnio widzieliśmy się na wyścigach holopsów. Trójka zdecydowanie świetna, ale widzimy się... w innych okolicznościach. Zdecydowanie nie wyglądam tak dobrze jak trójka co wygrała wtedy."
* Karol: "(szczęka przy podłodze)"
* Mimik: "Rozumiem, że to jak wyglądam może odstraszać ale...  wiesz co, (zerka w lustro), myślisz że chciałem być TYM? Nie mogę iść do wanny i wyciągnąć korka bo mnie zassie jak tego studenta, pamiętasz..."
* Karol: "komendant Stefan..? O___O"
* Mimik: "tak... wybacz, chciałbym trochę normalności, widzisz czym się stałem?"
* DIANA: "naprawdę się starałam..."
* Mimik: (do niej) "wiem..."
* Mimik: "Stałem się tym czymś a i tak jestem szczęśliwcem. Nasze chłopaki są czymś gorszym, co najgorsze, zatracili swój umysł. Niebawem spotka to wszystkich tu. Starałem się by nasi mieli wszystko czego potrzebowali..."
* Karol: "Odkryłeś, czemu arkologia była pusta? To jest powód? To nie astorianie? To tu było wcześniej?" /szok "myślałem, że ONI to zrobili"
* Mimik: "Powiem Ci więcej, to COŚ zrobiło to samo astorianom. Pamiętasz te stwory, co przychodziły? Myślałem że chciały zrobić krzywdę?"
* Karol: "Jednego właśnie postrzeliliśmy, mamy go w krzyżowym ogniu."
* Mimik: "Odwołaj ludzi. Może się przydać. Bitwa o kolonię acz staramy się by nie dotknęła Twoich... naszych. Do niedawna moich ludzi". (puszcza to + narracja - tu uratowaliśmy, źle interpretowaliśmy +P)

P: +narracja, +potwory MOGŁYBY rozpieprzyć a nie zrobiły, +opuszczona arkologia, +dziecko i dane medyczne, N: --to jest zbyt dzikie by uwierzył i przecież giną ludzie itp.

Tr P +3:

* Vz: LV będzie współpracować i będzie bardzo aktywnie dążył do współpracy
* Vr: LV uwierzył XD. Wierzy, że Mimik to komendant Stefan, jego stary kolega
* Xz: BĘDZIE zaczyn wojny domowej, bo część osób boi się zostać jedzeniem dla potworów
* V: Wojna domowa rozwiązana, _martial law_, ludzie pod kontrolą itp.

PODSUMUJMY:

* Coruscatis się pojawi by ewakuować i będą współpracować z Potworami. Acz unikają.
* Arkologia boi się Potworów ale do nich nie strzela. I współpracuje. Acz unika.
* Potwory mają miejsce w okolicach "poszerzonego Coruscatis"
* Diana obiecała, że Cię znajdzie.

## Streszczenie

Travistas był uszkodzoną jednostką próbującą się ukryć, zapolował na niego 'Śmiech Sępa'. Jednostki przeszły przez Eter i się rozbiły na Neikatis. Członkowie 'Śmiechu' bez Generatorów Memoriam ulegli strasznej transformacji. Noktiański Travistas objął 'arkologię' Damnos, post-astoriańskie terrorformy ze 'Śmiechu' zaczęły chronić noktian. Niestety, COŚ jest w okolicach Damnos; Diana, czarodziejka, uległa i dołączyła do Mroku. Gdy pojawiły się niedobitki Coruscatis, Terrorformy weszły z nimi w sojusz i wszystkie siły - noktianie z Coruscatis, noktianie z Damnos i resztki terrorformów oddaliły się w poszukiwaniu lepszego jutra. Diana (corrupted) mentalnie odezwała się do męża, że wróci po niego i go uratuje...

## Progresja

* .

## Zasługi

* Amelia Mardiblon: pajęczy terrorform z dziesiątkami broni, ATV (inżynier, była magiem); gdy Braciszek uległ zewowi, poprosiła by do niej wrócił. Udało jej się odzyskać brata. Trzyma wszystkich w miarę sensownej formie i dyskretnie naprawia Damnos jak nikt nie patrzy.
* Dawid Nałęcznik: Skażony Miragent, 'mimik' (lekarz); po wyczyszczeniu przez Dianę dowiedział się od Braciszka czym był i jakie miał relacje z Dianą (która jest na stałym połączeniu z jego głową). Przekradł się z dzieckiem do Damnos, udał poprzedniego komendanta (Stefana) i doprowadził do sojuszu między wszystkimi stronami.
* Rosenkrat Amiribasit: komandos Coruscatis; dowiedziawszy się że Potwory porwały dzieci z arkologii udał się z Esarią w infiltrację resztek Śmiechu Sępa. Udało mu się... KIA.
* Esaria Mirtalis: komandoska Coruscatis; dowiedziawszy się że Potwory porwały dzieci z arkologii udała się z Rosenkratem w infiltrację resztek Śmiechu Sępa. Miała pecha - nie zginęła, dołączyła do czegokolwiek tam jest, przekształcona...
* Bartek Mardiblon: 'Braciszek' ('bronić DIANĘ!', entropik-class, kiedyś: brat Amelii; uszkodzony umysł, lojalny Amelii i chce ją chronić. Gdy opętał go zew, wrócił do Amelii - lojalność silniejsza niż zew Diany i Centrali.
* Mateusz Owisiec: 'corrupted Perceptor', wężoform z laserem, kiedyś: snajper, syn Alberta (kapitana); skutecznie robi dywersję dla reszty Zespołu by Mimik mógł się wślizgnąć do arkologii. Ogólnie optymistyczny, acz z czarnym humorem.
* Diana Nałęcznik: potężny terrorform, czarodziejka. Uratowała wszystkich, zmieniając ich w terrorformy. Nie chciała dołączyć do Centrali, ale zew był silniejszy od niej. Corrupted, głos w głowie Dawida - chce swojego męża z powrotem i wierzy, że uratuje 'swoich' i 'skończy misję'...
* Karol La Viris: cyberwspomagany oficer wojskowy z Travistas, nadrabia miną; próbuje twardo robić sojusze by pozbyć się potworów, ale dał się przekonać Mimikowi (wierzy, że ten jest poprzednim komendantem stacji Damnos). Po prawdzie, nie ma siły ognia na pokonanie potworów; ważniejsze jest przetrwanie...

## Frakcje

* Custodes Coruscatis: dołączają do nich resztki Travistas oraz Potwory powstałe z astoriańskich sił 'Sępa Uśmiechu'. Szukają miejsca gdzie mogą przetrwać; za mało zasobów do ilości gąb.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Quintal
                    1. Stacja Damnos: zwana 'przeklętą', niepełna arkologia, coś złego znajduje się pod nią lub w jej pobliżu.

## Czas

* Opóźnienie: 27
* Dni: 4


## OTHER
### Stacja Damnos


1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Stacja Damnos
                        1. Poziom 1 - Dolny
                            1. Północ
                                1. Sektor Porządkowy
                            1. Wschód
                                1. Farmy Wschodnie
                            1. Południe
                                1. Processing
                                1. Magazyny
                            1. Zachód
                                1. Medical
                                1. Life Support
                        1. Poziom 2 - Środkowy
                            1. Południe
                                1. Engineering
                                1. Power Core
                            1. Południe i Wschód
                                1. Centrum Kultury i Rozrywki
                                1. Centrum Dzieci
                            1. Północ
                                1. Bazar i sklepy
                        1. Poziom 3 - Górny
                            1. Centrala dowodzenia
                            1. Komunikacja
