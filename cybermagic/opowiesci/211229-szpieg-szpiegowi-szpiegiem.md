## Metadane

* title: "Szpieg szpiegowi szpiegiem"
* threads: historia-rolanda
* gm: kić
* players: żółw, fox

## Kontynuacja
### Kampanijna

* [211229 - Szpieg szpiegowi szpiegiem](211229-szpieg-szpiegowi-szpiegiem)

### Chronologiczna

* [211124 - Prototypowa Nereida Natalii](211124-prototypowa-nereida-natalii)

## Sesja właściwa

### Postacie

* Berenika - Agentka specjalna. Szpieg, jej zadaniem jest zdobyć - jak i Roland - wykonać szpiegostwo przemysłowe na terenie Stoczni Neotik. Plany, dziwne energie itp. Dowiedzieć się interesujących rzeczy dla mocodawców. Oboje znacie się na robocie. Nie znają się jeszcze. Opcje frakcji: Cieniaszczyt (miasto szemranych interesów, rozpusty, mafii, gangów). Inna frakcja: Eternia, poza arkinem. TO JEST TO. Nadal chcę pracować dla Eterni, ale nie mam połączenia z arkinem. Specjalistka od zamków, włamów itp. Technik.

### Scena Zero - impl

.

### Scena Właściwa - impl

(Roland kiepsko się skrada. Nie jest w tym dobry.)

Berenika przybyła do Stoczni jakiś czas temu. Nikt jej nie zarejestrował. W jakiś sposób się zabunkrowała. Dodała się do logów - "czyli zawsze tu była". Trochę czasu zajęło zorientowanie się w terenie, kogo unikać... kolegów z security. Stocznia nie jest super strzeżona. Ale jak już jest się w środku, można w miarę sensownie się poruszać. Udało jej się zlokalizować miejsce, które jest dość ciekawe. Miejsce, gdzie są informacje gdzie przechowywane są różnego rodzaju plany. To jest to po co tu przybyła. Więc Berenika zdecydowanie chce się tam wybrać. Jest w tej stoczni obszar strzeżony BARDZO mocno, fizyczny dostęp do serwera itp. I jest też obszar tyci mniej strzeżony, tam są inne plany stoczni.

Berenika przybyła, bo tu się dzieją projekty eksperymentalne, dziwne. Jak się ustawić na tej stoczni? => Berenika ustawia się jako maintenance. Ludzie chodzący i przykręcający rzeczy, prace itp. Drobne prace, logistyka, mechanik i złota rączka. Jak się tam dostaje? Mikroalarmy w budynku, w różnych drzwiach. Problem z drzwiami, czy wszystko OK.

Jako kto się pojawia Roland, jak się pojawia, jak się wbija. 

Dziennikarka i performerka (Estella Evans, z Neikatis) z Valentiny i Roland jako boytoy. Perfekcyjnie wygląda i niewiele robi. Estella ma zachcianki. Diwa. Zlokalizował interesujący teren i miejsca. Roland nadaje Estelli odpowiednio ładnego chłopaka i jako on idzie w tamten region. Bada, ogląda jak ten teren wygląda... przedostaje się przy użyciu fałszywej biometryki. I teczek.

Berenika robi dywersję drzwiową i chce być wysłana do tych konkretnych drzwi. (test szczęścia: XXX). Roland nadał kolesia Estelli, zakręcił by dostać jego biometryki. Udaje się pewnym krokiem (mam prawo tu być) do owych drzwi. Wchodzi do pomieszczenia, całkowicie na bezczela. Są tam terminale komputerowe, forma segregatorów. Są też mniejsze drzwi, zamknięte. Sam pokój to połączenie _cubicle_ z terminalami komputerowymi. Roland jest dobry w tematach komputerowych i biurokratycznych - używa disruptor Hestii i szuka informacji odnośnie tematów anomalnych. Nie. Czegoś o osobach znających się na anomaliach. Crosssekcja z miejscami i dossier.

TrZ (info o ludziach) +3:

* Xz: ODROCZONY.
* X: PRZERWANY KONFLIKT. 

Berenika dociera na spokojnie, po kolei, sprawdzając różne drzwi - do interesujących drzwi. Drzwi się otwierają. Ostatnią rzeczą jakiej Berenika by się spodziewała jest koleś w środku który pracuje nad czymś. Roland jedzie jako hardcorowy HRowiec który znalazł COŚ NIE TAK i ma gdzieś zabezpieczenia. Berenika próbuje wygnać HRowca by robić swoje.

* "Moja droga! Nie ruszam się stąd póki nie dowiem się że coś jest tu nie tak w rekordach!"

Berenika blokuje drzwi. Niech nikt nie może wejść. Berenika potem podchodzi by potraktować go czymś chemicznym, powiesić mu się na ramieniu. "Wyciek gazu".

* Vr: Roland uniknął ataku i oddalił się na zasięg. Ma wyższy poziom bezpieczeństwa. "ATAKUJESZ HRA?!"
* O: "Uwaga, nieautoryzowany dostęp." I światła alarmowe.

Berenika podchodzi do kompa i próbuje otworzyć drzwi. Roland zachowuje się jako bardzo, bardzo wkurzający koleś z HRu który NIC NIE ZROBIŁ. Berenika zauważyła na monitorze ikonę cichego alarmu. Była odpalona jak Berenika wchodziła. To nie jest typowe. Nigdy. To ma za zadanie dyskretnie dać znać pracownikom, że coś jest nie w porządku. A to co zapisano w alarmie to "nieautoryzowany dostę...". Wiadomość jest ucięta. I w tym momencie słyszą ryk alarmu. Który prawie natychmiast jest odcięty. I rozlega się ogłoszenie przez systemy stoczni "tu gwardia ochrony Orbitera. Przejęliśmy kontrolę nad stocznią. Tej stoczni grozi atak. Będziemy jej bronić. Wszyscy pracownicy mają udać się do hangaru 3D. Kto tego nie zrobi zostanie uznany za szpiega i odpowiednio potraktowani. Macie... 10 minut."

* "No otwórz te drzwi byśmy mogli tam iść!" - Roland do Bereniki

Drzwi się otwierają.

* "Co zrobiłaś? Czemu włączyłaś alarm? Co spieprzyłaś?" - Roland do Bereniki, odsuwa, szuka informacji odnośnie Gwardii Orbitera. Nic nie ma. Berenika odsuwa Rolanda, siada, szuka kodów - nie do końca ma to sens. Hestia wykrywa intruzję. Obcy ludzie są na pokładzie, ale nie do końca rozpoznaje ich jako wrogich. Nieautoryzowani ale nie do końca wrodzy. Hestia jest przez disruptor bezużyteczna...

Berenika widzi, że na stację weszło około 50 osób. Kolejne ogłoszenie przez interkom "Rozsyłam moich ludzi. Ostatnia szansa by z nimi pójść. Kto nie pójdzie z moimi ludźmi będzie traktowany jako szpieg. Nadchodzi atak i będziemy bronić tej stoczni."

Roland do Bereniki, że Hestia działa dziwnie (chowając disruptor za sobą). Berenika próbuje faktycznie uszkodzić / zniekształcić drzwi. Żeby ich keycardy nie działały / działały dziwnie. I niech Berenika seeduje za drzwiami różnymi poukrywanych ludzi - ale ich tam nie ma. A trzeci rząd - jak się poruszają i w jakich grupach.

Czyli Berenika próbuje zapewnić trzy cele.

ExZ (disruptor) +3:

* V: fałszywe sygnały
* Vz: możemy ich zatrzasnąć. Nie spodziewają się kto jak i gdzie. 
* X: disruptowaliśmy Hestię. Oni szukają disrupcji Hestii. Roland pokazuje że disruptował Hestię i wyjaśnia że jest z sił specjalnych Orbitera. Wyjaśnia co i jak a Berenika udaje że nie wie o co chodzi. +1Vg.

Berenika próbuje używać przejść technicznych, obliczeń - gdzie może znajdować się jakaś niewielka grupka. I wie które przejścia są sensowne.

TrZ (Berenika jest technikiem i ma uprawnienia i wie które przejścia są sensowne) + 3:

* Vz: Uniknęliśmy innych grupek. Znaleźliśmy niewielką grupkę niedaleko pokoju w którym byliśmy. 3 osoby. Kompetentni ludzie. Kryją boki, patrzą na boki itp. Berenika atakuje roombą (+1Vg) by oni musieli coś zrobić.
* V: Sukces. Odwrócona uwaga.

Roland atakuje z zaskoczenia przy użyciu magii. Odwrócona uwaga. TrZM+3. Atak z potężnym łukiem elektrycznym.

* X: Ktoś zdążył się zorientować i zwrócić w tą stronę. Są naprawdę kompetentni. Roland puszcza łuk elektryczny + atakuje zmniejszając profil. +Vg
* V: Usmażone komunikatory + podgłuszyć. Oszołomić. Wciąż niebezpieczni. +Vg. Wyrywa pałkę, energetyzuje, bardzo agresywny _stance_.
* V: Nie są w stanie stanowić poważnego zagrożenia.
* V: Roland ich zbił. Jednego za mocno - ciężko ranny, interwencja lekarska. Wszyscy zgłuszeni.

Z Bereniką wciągnęli ich do środka. "Dobra robota. Utrzymałaś nerwy jak prawdziwy agent".

Roland zaczyna od tortur psychologicznych. Niby potwierdza ich historię ale idzie full psycho.

TrZ (pokonani, historia trzyma się kupy itp) +3: 

* V: on jest przerażony. "Nie uda wam się przejąć tej stacji!". Jest z Orbitera... Roland "jasna cholera! czemu nie powiedzieliście! (błąd kom na linii oficerskiej)
* V: Roland udowodnił, że jest po tej samej stronie. Chcę chronić stację przed zagrożeniem zewnętrznym. Rozwiązuje go.

Berenika wsadza rannego w medbay. Wymaga pomocy, ale nic krytycznego. Roland powiedział, że ma coś do sprawdzenia i załatwienia. Niech on idzie do swojego dowódcy i powie co i jak - a Roland musi coś sprawdzić.

* V: Wzbudził podejrzenia, tamci widzą, że coś jest nie tak, ale ogólnie "ok".

Roland prowadzi Berenikę do miejsca z disruptorem. Tam są te specjalnie drzwi. Roland chce, by Berenika przedostała się na drugą stronę - coś jest nie tak na tej stacji i trzeba dojść do tego o co chodzi. Berenika "wow, mam agenta Orbitera i wszystko robi za mnie". Berenika włamuje się do pokoju z autoryzacją "Agenta Orbitera". A jak byli powaleni strażnicy Berenika podkradła uszkodzone komunikatory strażników. Kody autoryzacyjne, wszystko.

Berenika włamuje się do pomieszczenia.

TrZ (dziwne karty i kody) M +3:

* Omv: stałe połączenie z komputerem póki się bardzo nie oddali od niego. Połączenie przez warkocz. Zostają jej ślady, echo itp. Disruptor inkorporowany w Hestię - Roland nie może go wyłączyć.
* Om: Berenika nie jest kompatybilna z arkinem... a Berenika ma skłonności ixiońskie. Berenika wyczuwa coś na kształt świadomości na obrzeżu własnej świadomości. Nie wie CO TO, ale to tam JEST. (proto-prototypy Nereidy). I jest samotne. Ta Hestia potrafi nie do końca świadomie SAMA się disruptować w kluczowych momentach i kluczowych kwestiach. Ona jest świadoma. Może zdecydować się samej nie zauważyć czegoś co może pomóc. Ona może na wpół świadomie nie zarejestrować czegoś co musiałaby zrobić. Jest świadoma. Przeniesienie ixiońskie.
* V: Wjazd do pomieszczenia.

"Dobra robota. A teraz sprawdź czy wszystko jest prawidłowe" - Roland odwraca uwagę od Bereniki a sam bezczelnie ściąga informacje których szuka. Berenika się TAK CIESZY że ten "agent Orbitera" nie wie XD.

Roland i Berenika, całkowicie nieświadomi, zajmują się swoimi tematami i rzeczami. Oboje wykonali swoje zadanie. Roland podkłada bombę katalityczną wskazującą na to, że coś tu zrobił inny mag, po czym się oddalamy. Berenika ślicznie dziękuje.

Berenika dostała to co chciała. Głupio byłoby teraz wypaść z roli technika, który jest grzeczny.

Roland idzie do tej bomby katalitycznej. Zostawia sprzęt, odpowiednią sztuczną krew itp. Rzeczy, które służą do takich spraw. I tak wybuch większość zniszczy. Sam zostaje w gaciach. Następnie - kilka uderzeń w twarz, sam sobie. Uderzenie o ściany. By były ślady. Potem, krzesło + lina, związuję się, w jakiejś szafie. Locker szkolny. Szatnia techników. Damska. Plus się posikać - +5 do wiarygodności. Potem płacz. No i zostaje tam i grzecznie czeka. Czyli - ofiara ostatnia. A kto był "kocącym"? - osoby, które wcześniej były zatrudnione do kocenia przez innego kochanka Estelli (Rolanda). I czeka aż wybuchnie bomba i zatrze ślady.

Berenika bierze po najbliższych Orbiterowców - jakie złe rzeczy opowiedział agent, co się dzieje, że tam coś niebezpiecznego, MUSZĄ PÓJŚĆ TO SPRAWDZIĆ. Będą ją przepytywać? Trzyma się "mówił że jest z Orbitera i kazał jej robić rzeczy i na jej oczach położył 5 gości NIE DYSKUTUJE SIĘ Z TAKIMI!"

Przeszło. Operacja udana.

## Streszczenie

Zarówno Roland i Berenika zostali wysłani na stocznię Neotik - całkowicie niezależnie - przez swoich pracodawców, by dowiedzieć się, jakie projekty są aktualnie przeprowadzane przez Orbiter. Każde ze swoją legendą, zabrali się do pracy. Przeżyli napaść Gwardii Ochrony Orbitera, która zaatakowała stację, aby obronić ją przed napaścią z zewnątrz. Oboje osiągnęli swój cel wykradając dane. Przy okazji Berenika ożywiła Hestię stacji (nie zamierzone), a Roland nabawił się zainteresowania anomaliami.

## Progresja

* Roland Sowiński: zainteresowanie anomaliami w wyniku tego, co znalazł w zdobytych materiałach
* Berenika Roldan: szacun za dobrze zrobioną akcję i informacę o innym agencie - choć nie byli w stanie go namierzyć

### Frakcji

* .

## Zasługi

* Roland Sowiński: szpieg z Aurum jako boytoy Estelli Evans z Valentiny; fatalnie się skrada czy włamuje, ale jest szybki, groźny w walce i jest mistrzem aktorstwa. Zarządzał ryzykiem, walczył z intruzami i ogólnie tak zamieszał, że nikt o nic go nie podejrzewa. Zdobył informacje, których szukał.
* Berenika Roldan: szpieg z Eterni w Neotik jako technik; specjalistka od zamków, włamów itp. Ukryła przed Rolandem fakt, że jest magiem. Do samego końca wierzył, że Berenika jest po prostu zwykłym technikiem. Zdobyła informacje, których szukała. Niechcący ożywiła Hestię stacji.
* Estella Evans: dziennikarka i performerka oryginalnie z Neikatis, teraz z Valentiny; Roland był pod przykrywką jej BoyToy w stoczni Neotik.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Pierścień Zewnętrzny
                1. Stocznia Neotik
 
## Czas

* Opóźnienie: -411
* Dni: 4

## Konflikty

* 1 - Roland jest dobry w tematach komputerowych i biurokratycznych - używa disruptor Hestii i szuka informacji odnośnie tematów anomalnych. Nie. Czegoś o osobach znających się na anomaliach.
    * TrZ (info o ludziach) +3
    * XzX: Odroczony + przerwany konflikt. Nie zdążył.
* 2 - Berenika potem podchodzi by potraktować go czymś chemicznym, powiesić się Rolandowi na ramieniu. "Wyciek gazu".
    * Tr+2
    * VrO: Roland uniknął ataku i oddalił się na zasięg. Ma wyższy poziom bezpieczeństwa. "ATAKUJESZ HRA?!" + "Uwaga, nieautoryzowany dostęp." I światła alarmowe.
* 3 - Berenika próbuje faktycznie uszkodzić / zniekształcić drzwi. Żeby ich keycardy nie działały / działały dziwnie. I seeduje za drzwiami niby poukrywanych ludzi.
    * ExZ (disruptor) +3
    * VVzX: fałszywe sygnały, możliwość ich podzielenia / pogrupowania. Nie spodziewają się. Hestia disruptowana, szukają tej disrupcji.
* 4 - Berenika próbuje używać przejść technicznych, obliczeń - gdzie może znajdować się jakaś niewielka grupka. I wie które przejścia są sensowne.
    * TrZ (Berenika jest technikiem i ma uprawnienia i wie które przejścia są sensowne) + 3
    * VzV: sukces, znaleziona grupka + odwrócona uwaga
* 5 - Roland atakuje z zaskoczenia przy użyciu magii. Odwrócona uwaga. Atak z potężnym łukiem elektrycznym.
    * TrZM+3
    * XVVV: Ktoś się zorientował, ale usmażone komunikatory + unieszkodliwieni + pobici.
* 6 - Roland zaczyna od tortur psychologicznych. Niby potwierdza ich historię ale idzie full psycho.
    * TrZ (pokonani, historia trzyma się kupy itp) +3
    * VVV: koleś jest z Orbitera, Roland udowodnił, że z tej samej strony, podejrzenie, coś nie tak ale ogólnie OK
* 7 - Berenika włamuje się do pokoju z autoryzacją "Agenta Orbitera".
    * TrZ (dziwne karty i kody) M +3
    * OmvOmV: stałe połączenie z komputerem, disruptor w Hestii, Berenika niekompatybilna z arkinem; Hestia umie autodisruptować, wjazd do pomieszczenia
