## Metadane

* title: "Tajemnicze Tunele Sebirialis"
* threads: agencja-lux-umbrarum, problemy-con-szernief
* motives: ukryta-instalacja-wojskowa, lokalne-mity-i-legendy, protomag-nieswiadomy, znikajacy-ludzie, ujawnic-zlo-publicznie, tools-of-enslavement, manhunt, teren-skazony, energia-alteris, faceci-w-czerni
* gm: żółw
* players: kić, flamerog

## Kontynuacja
### Kampanijna

* [231202 - Protest przed kultem na Szernief](231202-protest-przed-kultem-na-szernief)

### Chronologiczna

* [231202 - Protest przed kultem na Szernief](231202-protest-przed-kultem-na-szernief)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA: 
    * ?
* Opowieść o (Theme and vision): 
    * "Jeśli gonimy za pieniędzmi nawet kosztem moralności, życia i zasad - czasem coś się może całkowicie posypać gdy Magia odpowie na uczucia i pragnienie zemsty"
    * Konieczność pozyskania pieniędzy prowadzi do katastrofalnych decyzji i współpracy z NavirMed - firmą specjalizującą się w bioaugmentacji i lekach, która ma zamieszania
        * NavirMed prowadzą badania nad ludźmi co prowadzi do protomaga Alteris który ucieka wraz z grupą innych (robią fałszywe tunele)
        * Protomag pragnie zemsty na NavirMed. NavirMed pragnie odzyskać swojego człowieka i go zabić lub zbadać.
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * ukryta-instalacja-wojskowa: na Sebirialis znajduje się połączenie więzienia oraz badań na ludziach, za które płaci koncert NavirMed i z których czerpie
    * lokalne-mity-i-legendy: zdaniem drakolitów, na Sebirialis dzieje się coś magicznego i anomalnego (de facto, to sprawiło, że pojawia się protomag)
    * protomag-nieswiadomy: ...co prowadzi do tego, że w Jaskiniach Sebirialis pojawia się protomag. Więzień, na którym były robione eksperymenty, z afinicją do Alteris. Większe emocje -> większa moc.
    * znikajacy-ludzie: tymczasem grupa bogatych INWESTORÓW chciała zwiedzić kopalnię. Byli uzbrojeni i wyposażeni, ale alteris ich rozproszyło.
    * ujawnic-zlo-publicznie: wśród tych inwestorów jest Kalista Surilik, która się zamaskowała by odkryć sekret NavirMed. Ofc też zaginęła. Agentka INNEJ organizacji.
    * tools-of-enslavement: neuro-kontrolowani więźniowie zostali wysłani do złapania i usunięcia protomaga. Są w trójkach, poruszają się w podziemiach. "Lokalne Potwory"
    * manhunt: neuro-kontrolowani więźniowie zostali wysłani do złapania i usunięcia protomaga. Są w trójkach, poruszają się w podziemiach. "Lokalne Potwory"
    * teren-skazony: teren i ziemia się PRZEMIESZCZA, tunele alteris; główny problem to nie tyle siły militarne a połączenie tego i paniki protomaga z podziemi
    * energia-alteris: wpływa na przekształcenia przestrzeni, NieTunele (Brama) i czasem przeniesienia mentalne - osoby są kimś innym niż myślą że są.
    * faceci-w-czerni: nasza Agencja Lux Umbrarum, ale tym razem nikt im niczego nie mówi. Myślą że jest magia a wszyscy inni wierzą, że magii nie ma.
* Detale
    * Crisis source: 
        * HEART: "sin (grzech): tajne więzienie połączone z eksperymentami na ludziach SPAWNOWAŁO protomaga"
        * VISIBLE: "znikają ludzie, wyższy poziom paniki niż powinien być, ryzyko że kopalnie metali będą narażone"
    * Delta future
        * DARK FUTURE:
            * chain 1: SEE NO EVIL: nic nie wyjdzie na jaw, proceder będzie kontynuowany, więzienie i badania są dalej, NavirMed nie ucierpi w żaden sposób
            * chain 2: EXECUTION: inwestorzy, uwzględniając Kalistę zostaną zabici przez albo łowców albo przez więźniów. Eryk skutecznie dojdzie do tego że krwią MOŻE zniszczyć Więzienie.
            * chain 3: PRAWDA: wiedza o magii wyjdzie na jaw, konieczność aplikowania masowych amnestyków.
        * LIGHT FUTURE: 
            * chain 1: NavirMed musi wycofać działania i siły z terenu, odpowiednie osoby po stronie CON ucierpią
            * chain 2: Uda się uratować część ludzi, ewakuować itp.
            * chain 3: Nikt jak o magii nie wiedział tak nie wie
    * Dilemma: "iść za ZADANIEM czy iść za tym co NALEŻY?"

### Co się stało i co wiemy

* Kontekst stacji
    * CON Szernief została zaprojektowana jako stacja sprzedająca Irianium. Jest to miejsce o podniesionej ochronności elmag (lokalizacja: przy Sebirialis).
    * Okazuje się, że populacja która zainteresowała się tymi warunkami to byli Savaranie.
        * I inwestorzy są w klopsie. Dla savaran nadmiar energii to radość. Nie chcą kupować seks-androidów ani niczego. Pełna oszczędność.
            * Savaranie to NAJGORZEJ z perspektywy korporacji i inwestycji XD
        * Inwestorzy żądają zwrotu
    * Zarządcy CON Szernief szukają kogoś kto będzie tam chciał żyć i najlepiej za to jeszcze zapłaci
        * Stanęło na ściągnięciu Kultystów Adaptacji. Ale oni nie są jacyś super niebezpieczni, sami tak mówią ;-).
* Przeszłość
    * Konieczność podniesienia pieniędzy i zadowoleniu inwestorów. Jeden z nich, z NavirMed, zaproponował deal
        * Zbudowano na Sebirialis perfekcyjne więzienie. Tam są badania na ludziach itp.
        * Pojawiły się plotki o konieczności radzenia sobie z dziwnymi rzeczami - stąd te statki. Firma Sinharis.
    * Kalista bada Sinharis i NavirMed, zbliża się do prawdy
* Teraźniejszość
    * W wyniku zmian ludzi i badań obudził się protomag, któremu Alteris pomaga _przesunąć tunele_.
    * Protomag uciekł dwa tygodnie temu, porywając poszczególnych ludzi jako 'potwór ze ścian'.
    * Próbowali to rozwiązać po swojej stronie, ale się nie udało, bo Alteris zrobiło Bramę przestrzenną. Protomag porwał inwestorów idących na badania.
* Ślady
    * Część osób na stacji wie o dziwnych statkach z różnych firm które lecą w kierunku Sebirialis, gdzie "nic nie ma"
    * Kalista jest osobą rozpoznawaną wśród wielu drakolitów (którzy stanowią większość zaadaptowanych górników)
    * W jaskini znajduje się RESZTKA więźnia. Ale kto to?


### Co się stanie (what will happen)

* Siły
    * Eryk próbuje zapolować na ludzi i pozyskać ich do Krwawego Rytuału by zniszczyć Więzienie
    * Kalista próbuje go przekonać że to zły pomysł
    * Górnicy się boją, ale szefostwo ich zmusza do działania
    * Więzienie wysyła agentów by usunąć Eryka
* F0: CON Szernief
    * desc lokacji, desc sytuacji
* F1: Sebirialis, kopalnia
    * stakes:
        * ogromne spowolnienie i odcięcie
        * odkrycie obecności magii
        * odkrycie śladu INNEGO więźnia
    * opponent:
        * alteris: jako przekształcenie tras
        * alteris: czy górnik stał się więźniem
        * sama mordercza kopalnia
            * "_ten teren jest niestabilny_" - pozornie teren wygląda na normalny, ale się kruszy. Nie spadnij.
            * "_cicho, bo coś się zawali_" - wszystko jest kruche i delikatne, trzeba uważać z hałasami.
            * "_nienaturalny teren_" - kąty i ogólnie miejsce nie ma sensu, nie powinno działać
            * "_uwaga na wyładowania_" - elektryczność iskruje, grożąc ogromnymi zniszczeniami
    * problem:
        * kim są ci ludzie
        * skąd się wzięli i jak to się stało
        * jak się dowiedzieć co wiedzą i nie zdradzić
    * SCENES:
        * "górnik jest więźniem" (przeniesienie alteris)
        * "tego tunelu nie było"
        * "oki... te tereny nie mają sensu"
* F3: Więzienie
    * stakes
    * opponent
        * Więzienie
            * "To miejsce jest nie tylko więzieniem dla ciał, ale i umysłów. Kto wie, jakie tajemnice skrywa w swoich głębinach."
            * "nigdzie nie uciekniesz" - systemy zabezpieczeń są nieprzeniknione.
            * "wszędzie oczy" - wszechobecne systemy nadzoru.
            * "forteca pod ziemią" - budowla wytrzymała na wszelkie próby ucieczki.
            * "strzeżone tajemnice" - więzienie skrywa więcej niż tylko więźniów.
        * Wpływ Alteris na Więzienie
    * problem
    * SCENES
* F4: Podziemne Imperium Alteris Eryka
    * "they cannot die, yet they do not live"

## Sesja - analiza

### Fiszki

Luminarius

* Lester Martz, dowódca operacji

CON Szernief:

* Janvir Krassus
    * OCEAN: (O+ C+) | "Przez współpracę i innowacje budujemy lepszą przyszłość." (Locke) | VALS: (Tradition, Security) | DRIVE: "Przekształcenie Szernief w zaawansowaną kolonię."
    * on jest odpowiedzialny za współpracę z NavirMed
* Larkus Talvinir
    * OCEAN: A-O- | "Żartuję nawet w złych sytuacjach; prowokuję i nie boję się konfliktów" | VALS: Stimulation, Universalism | DRIVE: "Zabawa kosztem innych to moja rozrywka"
    * główny inżynier w sztabie
* Marta Sarilit
    * OCEAN: (N+ O+) | "Jeśli nie będziemy dość szybcy, zostaniemy zniszczeni." | VALS: (Benevolence, Power) | DRIVE: "Naprawa i lecznie"
    * dowodzi więzieniem NavirMed; to jej operacja
* Felina Amatanir
    * OCEAN: (N- C+) | "Nieustraszona i niezależna, wyróżnia się determinacją i profesjonalizmem" | VALS: (Power, Security) | DRIVE: "Porządek i sprawiedliwość, ponad kontrakt"
    * drakolitka o zwiększonych parametrach; wywalili ją z zarządu i z dowodzenia ochroną po tej operacji
* Kalista Surilik
    * OCEAN: (A+E+) | "Odważna i zdecydowana, w gorącej siarce kąpana" | VALS: (Benevolence, Achievement) | DRIVE: "Prawda MUSI wyjść na jaw"
    * już z nią Agencja miała do czynienia...

Więźniowie:

* Eryk Kawanicz
    * OCEAN: (A- C-) | "W moim świecie, nie ma miejsca na litość." | VALS: (Power, Self-Direction) | DRIVE: "PODDAŁEM SIĘ. Zniszczę NavirMed."
    * Protomag Alteris

Randomy (drakolici):

* Ignatius Sozyliw
    * OCEAN: O+A- | "Świat to szereg wyzwań do pokonania; rzadko zważam na emocje innych" | VALS: Achievement, Power | DRIVE: "Moc to moje przeznaczenie, a walka - droga do niej"
* Zygfryd Ogriss
    * OCEAN: C+E- | "Skupiam się na celach, nie ulegam emocjom; moje plany są zawsze starannie przemyślane" | VALS: Self-Direction, Security | DRIVE: "W pogoni za doskonałością, moim przewodnikiem jest dyscyplina"
* Helissa Zarimis
    * OCEAN: N+O- | "Intensywnie reaguję na świat wokół, często odczuwam niepokój; kieruję się logiką, nie emocjami" | VALS: Universalism, Tradition | DRIVE: "Przez wiedzę i tradycję odnajduję spokój w chaosie"

Randomy (savaranie):

* Markus Palavius
    * OCEAN: E+A+ | "Uwielbiam grupowe przeglądanie żwiru" | VALS: Stimulation, Hedonism | DRIVE: "Znam te jaskinie jak własną kieszeń"
* Julia Zepunit
    * OCEAN: C-N+ | "Unikam konfliktów i chowam się w ciszę" | VALS: Benevolence, Universalism | DRIVE: "Ludzkie formy da się naprawić"
* Taddeus Zeler
    * OCEAN: A-C+ | "Praktyczne rozwiązania. Istotne." | VALS: Tradition, Security | DRIVE: "Kolonia. Lepsze jutro."

Randomy (inne):

* Arkadiusz:
    * OCEAN: E+A- | "Nigdy nie przegap okazji do przygody i zysku; innych traktuj jako narzędzia" | VALS: Power, Hedonism | DRIVE: "Żyję dla bogactwa i przyjemności"

* Rafał:
    * OCEAN: A+O+ | "Zawsze pełen energii do tworzenia; pomocny i kreatywny w poszukiwaniu nowych rozwiązań" | VALS: Face, Tradition | DRIVE: "Innowacja i odrodzenie to moja droga"
* Witold:
    * OCEAN: A-N+ | "Żyję w swoim świecie, niezależny od innych; ostra natura, unikam zgiełku" | VALS: Security, Self-Direction | DRIVE: "Moja korporacja to moja twierdza i duma"

#### Strony

* Eryk
    * CZEGO: zniszczyć więzienie Krwawym Rytuałem
    * JAK: alteris, przemieszczenia, pozyskanie ludzi, rytuał
* Kalista
    * CZEGO: naprawmy to słowami, pomóżmy wszystkim
    * JAK: negocjacje, działania...
* CON Szernief
    * CZEGO: naprawić sprawę, pozbyć się Agencji
    * JAK: współpraca z Agencją ale też usuwanie niepotrzebnych rzeczy i ukrywanie zbędnych danych
    * OFERTA: pełna pomoc
    * KTOŚ: Janvir, Felina
* NavirMed
    * CZEGO: naprawić sytuację, ukryć
    * JAK: neurocommando
    * OFERTA: standardowa oferta terenowa, plotki itp.
    * KTOŚ:


### Scena Zero - impl

.

### Sesja Właściwa - impl

Sebirialis. CON dookoła Sebirialis. PODOBNO na Sebirialis dzieje się coś złego. Podobno to magia. Lokalsi wierzą w magię. To jest problem. Jakieś osoby podobno znikają w kopalni. I nie wracają. I nie ma śladu ani ciał. Więc - to jest moment by Was tam wysłać. Macie amnestyki (nie są idealne). TAI Crystal jest symulowana jako rudowłosa dziewczyna w niebieskim stroju.
 
Luminarius znajduje się niedaleko Szernief. Jak widać - Szernief jako stacja jest dość nietypowa.

* Szernief eksportuje energię w formie baterii
* Farmy słoneczne
* Kopalnie, górnictwo tylko na planecie.

Sigwald proponuje dowiedzieć się śladu od rodzin.

Wasza jednostka dokujecie na Szernief, macie komitet powitalny. Sigwald - jestem zachwycony jak licznie nas przywitano, jakie luksusy.

Tp (dominacja) +3:

* Vr: Strażnik od razu zaczął pytać
    * str1: "czy to dlatego, że inwestorzy zniknęli? To znaczy, te szychy?"
    * str2: "no jasne, normalni ludzie znikają, nawet jak to te psy (savaranie), to nic się nie dzieje. Ale zniknie inwestor, to od razu Agencja."
    * Sigwald: "jak zniknie jeden wysoki cel... ale zniknął jeden i przysłużył się wszystkim, szansa że zanikną wszelkie problemy... można powiedzieć że inwestor najlepszą rzecz ;-)"
    * str1: "szefie, wiem jak jest, mi nie zniknęli. Na razie znikają tylko psy i gady."
    * Sigwald: "niech lepiej nie znikają ludzie... chcemy powstrzymać. Ludzie na górze nie lubią mówić szczegółów a tacy jak Wy wiedzą więcej"
    * str1: "szefie, no tak, no, znikają tylko psy i gady..."
    * str2: "savaranie i drakolici. Miałeś już opierdol."
    * str1: "przepraszam najmnocniej, albo ci co patrzą w żwir i nie gadają albo kultyści..."
* V: Strażnik Anton Ci pozbiera wszystkie informacje, z kim gadać, ciepłe powitania itp.

Riana - jak prowadzą do sztabu - stacja jest w dobrej formie. Ale jest... tu trochę farby zabrakło. Nie ma RDZY ale nie ma też WYKOŃCZEŃ. Tej stacji się nie przelewa. Nie jest może bogata ale nie cierpi na ubóstwo. Z perspektywy strukturalnej - mogłabyś ją sabotować. Kamery powinny działać. I oni wyraźnie nie spodziewają się problemów. (docelowo: Riana -> ich monitoring i prześledzić tych co zniknęli na planecie w kopalniach).

5 osób. 

* Larkus: "zabrakło wam ludzi? Znaczy, problemów?"
* Sigwald: A nie wam ludzi?
* Larkus: "to jest... mieliśmy pewne przypadki, prawda. Przepraszam, jesteście... robimy budżetowanie" /ma kwaśną minę.
* Janvir: "przepraszam za kolegę. Tak, mamy pewne problemy. Ale... wszystko da się wyjaśnić."
* Larkus: "no właśnie nie, nie da się"
* Janvir: "Larkus, kupiłeś zbyt tanie sensory."
* Larkus: "Właśnie NIE. Powinny działać. Mają skalę elmag."
* Janvir: "Dwóch potencjalnych inwestorów... i pięciu górników... straciliśmy w kopalni."
* Sigwald: "Czemu sensory były za tanie? Nie będę pytał ktogoś kto kupił?"
* Janvir: "Bo nie wykryły... bo nie radzą sobie z tunelami. Wykopany na 10"
* Larkus: "Janvir, chłopie, siedzę na temacie sensorów i planet blisko gwiazd... od zawsze. Te sensory działają. To jest 10 metrów. Ale... to był sabotaż. To jedyne wyjaśnienie."
* Sigwald: "Sabotaż, czyj?"
* Janvir i Larkus spojrzeli zdziwieni (wtf?) "Nie wiem, nikt nie ma powodu."
    * (Feliny nigdzie nie ma, nie ma jej w radzie a powinna być)
* Sigwald: Kto jest szefem ochrony?
* Janvir: Tak, oczywiście, dobry pomysł. Ignatius tu obecny (cyborg-drakolita).
* Sigwald: Od dawna tu jesteś?
* Ignatius: Niestety, dopiero tydzień.
* Sigwald: Czemu?
* Ignatius: Moja poprzedniczka powiedziała, że nie może spełniać dalej litery kontraktu i ja wszedłem na to miejsce.
* Janvir: Co było dobrym wyborem
* Ignatius: Pragnę zaznaczyć, że inwestorów straciłem WCZORAJ. /niezadowolenie Tak więc nie jestem pewien, czy wymiana Feliny na mnie była dobrą opcją.
* Sigwald: Jest na planecie?
* Ignatius: Poszła zbadać kopalnię. Dałem jej cztery osoby do ochrony.
* Sigwald: Miała biuro? Można znaleźć rzeczy jakie mogą pomóc znaleźć inwestorów?
* Ignatius: Oczywiście, jeżeli chcesz mieć dostęp do jej oficjalnych dokumentów, możesz je otrzymać. Póki ona żyje nie mogę dać dostępu do prywatnych dokumentów
* Larkus: Miejmy nadzieję, że przeżyje długo zatem /krzywy uśmiech
* Ignatius: (ostro) Co to ma znaczyć?

Sigwald chce się dowiedzieć o kontekście żartu

Tr Z +3:

* V: Oni powiedzą to co jest do powiedzenia, kontekst itp.
    * Ignatius: "Felina nigdy by nie zrezygnowała ze stanowiska jakby nie uznała że czyni zło w ten sposób, lub że jest zablokowana." (on tego nie rozumie)
    * Larkus: "Nigdy nie ma pieniędzy na zrobienie więcej niż minimum. Nie są to idealne sensory, ale NA MNIE tego nie wsadzicie, po prostu."
    * Janvir: "Ci inwestorzy to... inwestorzy w ogóle w tej stacji to ogólnie nadzieja. Wyobraź sobie - robisz stację..." (opowieść o Szernief)

.

* Wpierw savaranie, potem drakolici, TERAZ inwestor od turyzmu i zniknęli. W kopalni.

Trzy tygodnie temu pierwszy górnik zniknął. Czy nikt nie zrobił dochodzenia...? Felina coś badała. Ale wychodziły jej głupoty.

* Larkus: mówiłem, że musimy przetestować czy TAI działa.
* Janvir: TAI działa. Nie stać nas na testy
* Larkus: stać nas
* Janvir: NIE.

Jest tu pewna... konfuzja.

* Felina, szefowa ochrony, która przyłożyła się do górnika i jest w kopalni. Nie ma z nią kontaktu, ale od miesiąca nie da się skomunikować wiarygodnie z planetą. Podobno nic nie jest zagłuszane.
* Inwestorzy zniknęli i którzy dostali ludzi do ochrony. Ci ludzie też zniknęli. Poza jedną osobą która jest w areszcie.
* Miesiąc temu: problemy z komunikacją. Trzy tygodnie temu: pierwszy górnik znika. Przez 3 tygodnie: zniknęło 5 górników. Wczoraj: zniknęło 2 inwestorów ORAZ 4/5 strażników inwestorów.
    * łącznie z problemami z komunikacją zaczęły się problemy z sensorami
* Tydzień temu Felina zrezygnowała ze stanowiska zgodnie z literą kontraktu (co tam jest?).
* Larkus mówi, że wszystko powinno działać i jest COŚ NIE TAK
* Janvir mówi, że wszystko jest w parametrach. Wszystko to uszkodzenia sprzętu.
* Ignatius dodaje, że taka była wola Saitaera.

Riana idzie do biura Feliny. Ignatius jej pomoże. Sam jest zaniepokojony tą sytuacją. Riana chce przeanalizować dane i sprawdzić, czy on nie chce sabotować danych. **Niektóre** dane które ma Felina są zakodowane. Felina nie może szyfrować danych i dokumentów przed Radą, ale mogła maskować. (Ignatius nie przeszkadza, nie konfliktuje). Z dobrych wieści - nikt nie PRÓBOWAŁ zmienić tych danych, ale ktoś je PRZESZUKIWAŁ.

Ex +3:

* X: zajmuje to paskudnie dużo czasu (ale nie blokując Felinę)
* X: Felina zamaskowała wszystko bardzo skutecznie. Nie da się dojść co DOKŁADNIE miała na myśli.
* Vr: Felina dobrze zamaskowała dane. Ale z pomocą Luminariusa macie pewne dane
    * Felina skupiała się m.in. na instrukcjach do TAI. A dokładniej, czego ma NIE raportować.
    * ZAKRES badań Feliny sięgają 2 lat wstecz. Pobieżnie zaczęły się jakiś rok temu a w ostatnim czasie BARDZO się nasiliły - zrezygnowała.
        * Oraz - sensory, komunikacja itp. Acz nie z planetą.

Czyli coś jest ukryte. Sigwald -> koleś w areszcie, co przetrwał. Czemu w areszcie:

* gdy była grupa ratunkowa, on zaatakował tych co go ratowali i nie rozpoznał imienia w ogóle. I krzyczał "nie bierzcie mnie tam, nie róbcie mi tego".
* badania z kamerek... cóż, jego kamerka pokazała, że on został z tyłu. Korytarz kończył się ślepą skałą. Ale on mówił, że tam była wielka pieczara i ją widział.
    * ratownicy sprawdzili - to jest lita skała
* strasznie bał się i łkał przed "doktor Anielą". Nie ma nikogo takiego. To mu już przeszło.

Sigwald przesłuchuje Leona. Leon przerażony jak usłyszał o kopalni "NIE IDŹ TAM!". Sigwald "rozkaz to rozkaz". Leon "no tak...".

* Sigwald: Goście co nie byli nie WIEDZĄ co tam było. TY WIDZIAŁEŚ. Tobie mogę zaufać. Nie takim pierdolcom
* L: "Czerwona glina... to nie ma sensu."

Tp +3 (bo agencja + wyszkolenie):

* V: Leon tłumaczy i opowiada _Alteris_: pomieszanie korytarzy i przeniesienie mentalne
* V: Leon (ON JEST Z ZEWNĄTRZ) opisał jako "ludzi połączonych ze stalagmitowymi jelitami"
    * to są ci górnicy co zniknęli
    * ten koleś, ten bez tlenu - tego człowieka nie było na tej stacji
    * ta laska wygląda jak KALISTA! (dziennikarka)
    * nie było nikogo takiego jak doktor Aniela czy ludzie w klatkach itp - tamtych ludzi nie było na tej stacji

Sigwald chce przesłuchać różne osoby które MOGŁY widzieć -> Tr Z

* V: Jeden z dokerów się Sigwaldowi PRZYZNAŁ.
    * Doker: Ale nikt nie będzie o tym wiedział (stres)
    * Doker: Byłem... zrobiłem zakład. Mam wśród znajomych taką laskę z arystokracji.
    * Doker: Ona była... chciała coś fajnego. Więc do jednego z tych statków... TYCH statków... poszedłem na tyły. Załadować. Coś małego ukraść. I tam były takie klatki jak mówisz. Ten statek był pusty, w klatkach nie było, on wracał do planet centralnych. Więc... więc ja to widziałem. Co więcej, potem się okazało... (obniżył głos)... TAI nie miała w pamięci że ten statek tu był. To był statek-widmo. Jeden z tych ukrytych.
* V: 
    * Doker: Statek to był taki transportowiec. To była taka dalekosiężna korweta. I to było dziwne, bo co, jacht?
    * Doker: Są takie statki, one nigdy nie dokują na stacji, ale one skądś wracają. Przecież nie z kopalni. 
    * Doker: Ktoś by wiedział o klatkach w kopalniach.
    * Doker: Statek nazywał się Vespae Obscurum. Wiem bo patrzyłem. Tabliczka. Wyglądał jak jacht inwestorów. A był czymś innym.
    * Wszystko wskazuje na to, że to, co widział Leon to nie dotyczy TYCH LUDZI Z TEJ STACJI ale coś jest nie tak.

Sigwald robi analizę kultystów. Co tu się dzieje. Co wiedzą / o czym myślą kultyści. Przy pomocy Crystal skanujemy całość monitoringu

Tp Z+3:

* Vz: Faktycznie, kultyści tu są. Wyłapać kilka fragmentów ciekawszych akcji, słów itp
    * Kultyści
        * Kultyści się modlą do Saitaera błagając o OCHRONĘ przed TYM co jest w kopalni
        * Dla nich to coś strasznego
    * Savaranie
        * Savaranie zbierają sie, wymieniają się informacjami
        * Przestraszeni
* V: To co ciekawe - monitoring pokazał, że KALISTA dołączyła do inwestorów. Ona się tam wślizgnęła, jako "lokalna osoba która ich oprowadzi".
* V: Kalista. Oczywiście. Wieczny problem dla agencji.
    * Kalista przeszukiwała TAI pod kątem "niezidentyfikowanych statków kosmicznych". Oraz lokalizacji na Sebirialis. Ale TAI jest "ślepa", więc nie mogła znaleźć
    * Kalista szukała czegoś w kopalniach - jeszcze ZANIM do czegokolwiek doszło. Jak i informacji o budowie placówek.
    * Ona się wkręciła do inwestorów by coś zbadać.

Hackujemy prywatne dane Kalisty. W odróżnieniu od Feliny, Kalista nie ma takiego dobrego opsec.

Tr+3:

* X: Mimo wsparcia Luminariusa wyjdzie na to, że Agencja grzebała w jej danych.
* V: 
    * Kalista zrobiła serię danych, analiz itp do Wielkiego Raportu Swojego Życia
        * Gdzieś tu jest sekretna baza o której nikt nie wie.
        * Ktoś na Cylindrze o niej wie, Rada postanowiła ją zbudować.
        * Niecały miesiąc temu: baza musi być w okolicach kopalni. Tylko to wyjaśnia "problemy z czujnikami" oraz zniknięcie górników.
    * Luminarius postawił taktyczna TAI która ma wysadzić dane Kalisty jakby były groźne.
    * Kalista doszła do nazwy firmy - NavirMed. Tej co buduje bazę. Bo nagle NavirMed płaci więcej Cylindrowi niż wynika to z potrzeb.
        * 2 lata temu zaczęła się inwestycja
        * i wtedy pojawiają się te dziwne statki

Dużo danych, trzeba puścić do Centrali. Nazwa statku, NavirMed itp.

Tr Z +3:

* Vr: 
    * Statek "Vespae Obscurum"
        * Statek "Vespae Obscurum" który widział doker - ta jednostka FAKTYCZNIE jest powiązana ze spółką ze spółką... z NavirMed.
        * Ten statek zwykle przewozi ludzi chorych. Nie powinien mieć klatek, raczej 3-4 biovaty.
        * Ten statek - zgodnie z danymi - FAKTYCZNIE tu przyleciał i faktycznie tu był.
    * Potwierdzone jest z danych z Centrali
        * Tu podróżowały 2-3 jednostki tego typu. I PODOBNO to robiły albo wycieczkowo albo by coś przewieźć itp.
    * 4->2 lata temu była tu robiona pewna konstrukcja. Były ciężkie pojazdy itp. Więc coś faktycznie się działo. Ale nie w polu widzenia Cylindra.
    * Nie mając współpracy z TAI nie dałoby się tego ukryć przed kolonią. Czyli ktoś w Radzie wie i za tym stoi.

Riana przeszukuje TAI pod kątem instrukcji. Ma uprawnienia, więc ma główny 'override'. Luminarius pomaga.

Tr Z + 3:

* X: ten ruch będzie JAWNY dla wszystkich członków Rady
* Vz: TAK, doszło do ukrycia. Są instrukcje. Są dokładne instrukcje dla TAI łącznie z override.
    * Tamta rada podjęła tą decyzję, Janvir jest jej aktualnym przewodniczącym. Feliny nie było w radzie. Janvir i Larkus byli wtedy w Radzie. Ignatius nie ma pojęcia o niczym.
    * Dojście do tej prawdy usunęło Felinę

Sigwald puszcza plotkę wśród kultystów, że to ktoś w Radzie jest odpowiedzialny za to co w kopalni, ale Agencja się tym zajmuje - że JAKBY CO to kultyści pociągną za spust.

Tr +4:

* Vr: Kultyści będą 'w odwodzie'. Stanowią też mechanizm nacisku na Radę.

Spotkanie z Radą. Rada jest zestresowana całą sytuacją. Ale będzie to miła rozmowa. Chodzi o to by dowiedzieć się ile można wyciągnąć ORAZ skłonić do ataku jeśli mają inklinacje.

* R: Co to ma znaczyć... przekraczacie uprawnienia... - i Janvir i Larkus są niezadowoleni
* Sigwald: (ten projekt) (nie mówi nazwy)
* R: Próbują rżnąć głupa.
    * FAKT FAKT FAKT

Tr Z +3:

* Vz: JEŚLI mają inklinacje do ataku to zaatakują i macie przewagę lub NIE zaatakujecie i wiecie że nie mają.
* Vr: (p) DOWIADUJEMY się rzeczy.
    * L: Ach PROJEKT, ten projekt... ale co on ma do rzeczy? Przecież to jest kilka tysięcy kilometrów stąd? Nie ma tam nigdzie kopalni.
    * J: Projekt. Jak ten projekt ma się do tematu? Nie widzę powiązania.
    * Sigwald: Fakty o projekcie
    * L: Czekaj, ale to pasuje - patrz, to nie moje czujniki... to zmiany w TAI!
    * J: Larkus. LOKALNE czujniki mają problem. Nie tylko te Twoje z tej bazy. Ale NADAL nie ma to powiązania.
    * (wyraźnie oni NIE DODALI projektu do tej sytuacji - to nie jest powiązane)
    * L: Vespae Obscurum
* Xz: Oni nie wszystko kontrolują tak jak myślą że kontrolują. (hidden movement)
* X: Oni nie wiedzą dokładnie co tam jest. Wiedzą to co im powiedziano.
    * L: Vespae Obscurum
        * klatki itp -> oni byli zdziwieni
        * J: Moment, klatki? Jakie klatki? To miały być badania medyczne. Podziemna baza na testy biologiczne. Dużo energii i 100% pewność.
        * L: No i kopalnie, w sensie, mają swoje. Nie są powiązane z naszymi, za daleko.
        * Sigwald: Witalne zasoby są oszczędzane a blacksite są 'expendable'. Nie przypominacie, nic nie wydaje się dziwne, modyfikowanie TAI...
* X: Oni NAPRAWDĘ wierzyli w to, że ta firma mówi prawdę.
    * Mogą być sensownymi administratorami, ale politycznie z nich dupy. Firma ich przekręciła.
    * ALE mają kontakt do szefowej placówki. Można się z nią POŁĄCZYĆ. Nie mogą jej ściągnąć.
    * Tamta firma NAPRAWDĘ nie interesowała się tym terenem. Coś się zmieniło miesiąc temu.
    * 5 lat była współpraca, 2 ostatnie lata placówka działała i nie było kłopotów. Coś się musiało zmienić.
    * Oni nadal nie wierzą w magię. I NADAL są przekonani, że najwyżej to był sabotaż.
* V: Janvir oraz Larkus w ramach tego problemu będą współpracować z Agencją na 100% ALE żądają immunitetu.
    * Blacksite dowodzi Marta Sarilit, wysoka dyrektorka w NavirMed. Zgodnie z informacjami, dowodzi grupą eksperymentalną i augmentacjami. Podobno "chce przegonić Astorię i Saitaera"
    * doktor "Aniela" to "Aniela Teriens". Prawa ręka Marty Sarilit. 

Komunikacja z Martą, dyrektorką NavirMed. Sigwald mówi o Anieli Teriens i problemach w kopalniach. Aniela mogłaby pociągnąć nie tylko siebie i kopalnię ale i blacksite. Marta nie rozumie czemu ONI - Agencja Umbrarum Lux - się z nią kontaktują. Przekupstwa itp. są ok. Tu Riana pokazuje co podpadło pod nos, było znaną katastrofą ale wyjaśnia magię. "Jak na razie sytuacja rozwija się w tą stronę". 

* Marta: "Dobrze, rozumiem czym się zajmujecie. Czemu uważacie że w blacksite jest coś związanego z magią? Nie tego szukam, nie od tego blacksite jest."
* Riana: "To pani znalazła. Na to wszystko wskazuje"
* Marta: (chwilę myśli)
* Sigwald: "Warto jak SPECJALIŚCI się zajmują, i jak rzeczy mogą ściągnąć na dno mogą też sprawić żę rozwiną skrzydła. Ścisła kontrola osób które WIEDZĄ z czym mają do czynienia w miejscu w którym WYSTĘPUJE zjawisko - lepsze niż ignorowanie strat, flota kosztuje. Jest to tabelka."
* Marta: "Nie ścisła kontrola a współpraca."
* Marta: "Aniela wpadła na coś głupiego? Czego potrzebujecie ode mnie?"
* Sigwald: "Wszystkiego co nam pomoże."
* Riana: "Na ile pani ufa Anieli?"
* Marta: "Aniela jest oddana swojej pracy."
* Marta: "Aniela eksperymentuje z neurokontrolą i augmentacją. Chce stworzyć idealne maszyny do zabijania, nie idealnych żołnierzy. Oczywiście, ma samych ochotników."
* Marta: "To więźniowie, zwykle skazani na śmierć. Mogą posłużyć nauce, zamiast ginąć."

Kluczem jest osadzenie człowieka w blacksite, ale negocjacje będą "wyżej". Plus odzyskać Kalistę.

* Marta: "Aniela ma absolutny zakaz zbliżania się czy interakcji. Nie ma nawet czym. Jeżeli Aniela ma Waszą dziennikarkę, to jutro Aniela nie dowodzi tą stacją. Stanie się... obiektem badawczym. I wie o tym."

Marta ma się skontaktować z Agencją i powiedzieć słowo kluczowe by zrozumieć prawdę. Aniela... zobaczymy. I teraz - w zależności od tego jakim typem osoby jest Aniela, można wykorzystać co się mogło stać. Więc musimy się dowiedzieć co sprowokowało tą sytuację...

TYMCZASEM - na stacji jest radość. Coś się zmieniło, coś się stało. TAI przekazała - z kopalni w stronę CON leci niewielka jednostka, (6 górników). Podobno to Felina i Kalista. Luminarius przechwytuje je na kwarantannę. Felina próbuje manewrować, unikać, ale nie ma szans. Luminarius je przechwycił. I zamknął na kwarantannie. Kwarantanna jest z powodzeniem.

NOTY PROTESTACYJNE. Zbliżają się pomruki rozruchów.

* Sigwald: "Powinno zależyć Wam na bezpieczeństwie! Nie przyniosą czegoś na stację! WŁAŚNIE DALIŚMY WAM CZAS NA ZORGANIZOWANIE PRZYJĘCIA DLA WYZWOLICIELKI!"

Tr +3:

* X: "Tam jest coś potencjalnie groźnego! Nie lecimy tam!" - ogólny PROTEST, nie lecą, jeśli coś groźnego itp.
* V: Nie ma rozruchów, jest wysokie niezadowolenie, ale jest OK. Agencja JESZCZE trzyma rzeczy pod kontrolą.
* V: Rada nie jest zbyt nieszczęśliwa bo rozumie sytuację.

Wszyscy smutni, ale sytuacja zrozumiała.

Dane z Luminariusa mówią:

* Felina jest zdrowa. Jest Dotknięta przez Alteris, ale słabo. FELINA NIE CHCE WSPÓŁPRACOWAĆ.
* Kalista jest Skażona przez Alteris, ale jest odwracalna. Da się ją naprawić na Luminariusie. Felina wyraźnie ją jakoś wyciągnęła...

Sigwald zdalnie łączy się z Feliną. Drakolitka nie jest szczęśliwa

* Felina: Agencie. Co to ma znaczyć. Potrzebowałam wsparcia.
* Sigwald: Najlepsza osoba jaka... najlepiej przerobiona osoba, peak efficiency... niepotrzebne posiłki. W tym czasie więcej mogłem tutaj.
* Felina: Agencie. Problem jest na stacji, zgadzam się, tyle mogę powiedzieć. Ale ludzie do uratowania byli w kopalni. Nie chodzi o to, że Ty. Ale nikt.
* Sigwald: A Ty?
* Felina: Nie wiedziałam o Was, poczekałabym. Macie lepszy sprzęt, w odróżnieniu od naszych czujników Wasze działają.
* Sigwald: Wiesz jak jest z tą stacją, nie ma informacji. Dopytywać o szefie ochrony.
* Felina: JUŻ mnie wymazali?
* Sigwald: Nie próbuj wyrzucić go przed śluzę... wytrzyma.
* Felina: Ignatius jest... najlepszą opcją po mnie. Ale jest ufny. Nieważne. Udało wam się kogoś uratować? Widziałam tam dziesiątki osób.
* Sigwald: Wydaje mi się... wróciłaś z wszystkimi. Nie wiesz? Aktualnie udaje nam się doprowadzić by kolejne osoby nie zginęły w gazach. Pod wrażeniem... jesteś świetnie wyekwipowana.
* Felina: Agencie. Mam skafander. Próżniowo osłonięty. JAKIE GAZY? Kalista... /wątpliwość... Kalista nie miała skafandra... Dobrze. Coś jest nie tak. Ale to nie są gazy.
* Felina: Mieliśmy sabotowane skafandry..? Z zatrutym powietrzem? Ale czemu? (oczy się zapaliły) O tym chcesz rozmawiać? Dlatego tu jesteście? Dlatego ta kwarantanna..?
* Sigwald: Wrócimy do tego (disconnect)
* Felina: (NIE!!!)

(godzinę później)

* Sigwald: Musiałem coś wyjaśnić na stacji...
* Felina: Oczywiście, będę współpracowała.
* Sigwald: Powiedz jak misja, czujniki, ale pod... poznać jak się dostałaś, jak wydobyłaś...
* Felina: (zrobiła głupią minę) Po prawdzie, nie do końca wiem. Nie jest to coś z czego jestem dumna ale obecność skażonego powietrza dużo tłumaczy.
* Sigwald: Dalej?
* Felina: Podam Ci fakty.
* Sigwald: Dokładnie Twoją wersję, bez faktów. Fakty wywnioskujemy.
* Felina: Interesujesz się moimi _halucynacjami_? (niedowierzanie agentki porządkowej)
* Sigwald: Oczywiście

Tr Z +3:

* X: 
    * Felina: "nie rozumiem, dlaczego halucynacje pokrywały się z czujnikami... czy dlatego chcesz wiedzieć"
    * Sigwald: "wiesz że nie możesz mieć za dużo informacji gdy można je poukładać i odfiltrować"
    * Felina: "rozumiem. Zasady, reguły i procedury."
* X: Felina DOMYŚLIŁA SIĘ, że to była prawda a nie fikcja. Nie umie dodać 2+2, ale wie, że to jest fakt, więc powie Ci prawdę.

Felina opowiedziała o Pieczarze Alteris. Szok.

Marta przekazała dane z systemu jak działa blacksite by można było przeanalizować.

Tr Z +3:

* Vz:
    * Blacksite ogólnie trzyma się zasad których nie rozumie np. regularne spryskiwanie ludzi wodą
    * Blacksite robi różnego rodzaju broń biologiczna, modyfikacje itp.
    * Blacksite zawiera też neuroobroże
    * Blacksite robi też badania nad chorobami
    * Zgodnie z procedurami, to NIE POWINNO mieć miejsca.

Ale. Protomag. Koleś którego widział Leon i Felina był Astorianinem. Wiedział o magiem. I to się stało.

OPERACJA: EWAKUACJA. 3 miesiące kopalnie są nieczynne "bo wyciek gazu sabotaż i inne takie". Marta kazała zawiesić blacksite - ogromne koszty, ale to łyknie, bo ważniejsza reputacja. I faktycznie po 3 miesiącach Alteris wymarło z głodu.

Nowa modyfikacja procedury: żadnych Astorian.

## Streszczenie

W tunelach kopalni pod CON Szernief znikają ludzie. Agencja dochodzi do tego, że winny jest protomag Dotknięty Alteris, który uciekł z tajnej (dla populacji Szernief) placówki NavirMed gdzie prowadzi się badania na ludziach. Agencja doszła do łańcucha logicznego i odkryła prawdę; by to rozwiązać, ewakuowali kopalnię i placówkę NavirMed na 3 miesiące by protomag umarł z głodu.

## Progresja

* .

## Zasługi

* Klasa Dyplomata: Sigwald Asidar z Lux Umbrarum; rozmawiając z dokerami dowiaduje się o blackships, z Radą o problemach, łapie powiązania kto-z-kim-kiedy i ogólnie robi świetną robotę śledczą. Praktycznie fraternizując się ze wszystkimi doszedł do tego że nikt nic nie wie, ale zapewnił ich współpracę.
* Klasa Sabotażysta: Riana Sanesset z Lux Umbrarum; doszła do tego, że stacja jest finansowo zaniedbana i z pomocą Luminariusa wbiła się do systemów stacji. Dobra w zastraszaniu, skłoniła Martę z NavirMed do współpracy. Decyduje o ewakuacji stacji.
* Felina Amatanir: nieustraszona; jak tylko doszła do tego że nie może zrobić Dobra będąc w radzie i zablokowana przez kontrakt, szybko opuściła kontrakt. Poszła do tuneli Sebiralis szukać Kalisty i inwestorów. Spotkawszy Alteris, udało jej się ewakuować z Kalistą. Doszła do współpracy z NavirMed i jej się to nie podoba. Nie chce współpracować z Agencją, ale nie ma wyjścia.
* Ignatius Sozyliw: drakolita, zastępca Feliny. Ochrona, bardzo sensowny ale nie ma w nim podstępu ani politycznego intelektu. Cybernetycznie wzmocniony. Lojalny wobec Feliny oraz stacji, pomaga Agencji bo wierzy, że w ten sposób odkryją szybciej jak to rozwiązać.
* Kalista Surilik: dziennikarka badająca znikanie górników na Sebiralisie; zeszła z potencjalnymi inwestorami do kopalni robić badania i została porwana przez Eryka i Alteris. W jakiś sposób przetrwała, uratowana przez Felinę. Jej zniknięcie było dużym utrapieniem dla Rady. 
* Larkus Talvinir: radny i inżynier, obwiniony o kupowanie kiepskiego sprzętu (bo sensory przy alteris nie dają sensownej odpowiedzi). Wie o umowie z NavirMed, ale to nie jej linia. Żąda immunitetu, szuka przyczyn i jest bardzo zestresowany. Zaradny, potrafi z minimalnych pieniędzy wyciągnąć coś co zadziała. Wieczne zwierzę w radzie XD.
* Janvir Krassus: elegancki radny, to on stoi za otwarciem Sebiralis dla różnych firm i agencji, w formie ukrytej (przez co zrezygnowała Felina). Chce dobra dla stacji, acz niekoniecznie dla aktualnej populacji stacji. Dba o stację a nie o osoby na niej.
* Marta Sarilit: dyrektor NavirMed; bezwzględnie mistrzowska w polityce, angażuje się w eksperymenty z neurokontrolą i biomodyfikacją. Bardzo pragmatyczna, chce być gotowa na następną wojnę z Astorią. Decyduje się współpracować z Agencją.
* Aniela Kafantelas: agentka NavirMed oddelegowana do placówki w Sebirialis, 'doktor Aniela'. Dowodzi placówką. Jest w takim stopniu strażnikiem jak więźniem. Rządzi tą placówką i się jej boją; bardzo kompetentna ale psychicznie nie do końca.
* Leon Hurmniow: strażnik inwestorów, który jako jedyny nie zniknął. Dotknięty Alteris, widział rzeczy jakich nie umiał zrozumieć. Nie chciał opuścić aresztu, tam się czuł bezpiecznie.
* Eryk Kawanicz: astoriański więzień, na którym eksperymentował NavirMed na Sebirialis. Protomag. Dotknięty Alteris, dotarł do kopalni na Sebirialis. Super-niebezpieczny więzień powodujący wszystkie problemy i planujący zniszczyć bazę NavirMed używając krwi porwanych ludzi; agencja doprowadziła do ewakuacji Sebirialis i umarł z głodu i pragnienia. KIA.
* OLU Luminarius: główne repozytorium danych Agencji i taktyczna TAI która pomogła w hackowaniu danych Kalisty a przedtem Feliny.

## Frakcje

* Agencja Lux Umbrarum: uzyskuje wsparcie NavirMed, a co najmniej placówki na Sebirialis; wprowadza tam praktykanta.
* NavirMed Sebirialis: współpracuje z Lux Umbrarum, dostaje praktykanta oraz aktywnie wspiera. Dowodzi dyrektor Marta Sarilit. Bada więźniów i produkuje stymulanty itp.
* Con Szernief: cylindryczna kolonia musiała zatrzymać pewne działania kopalniowe na 3 miesiące, co spowodowało dużą stratę finansową i duże niezadowolenie społeczne.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Kalmantis: noktiański sektor pełny dobrobytu, gdzie nie ma może żadnej dobrej planety gdzie da się mieszkać, ale wykorzystywane są arkologie i CON.
            1. Sebirialis: planeta w stylu Merkurego
                1. Krater Ablardius
                    1. Kopalnie Ablardius: znikają tam ludzie; pojawił się link Alteris od bazy NavirMed do tych kopalni. Felina poruszając się w tym terenie uratowała Kalistę.
                1. Płaskowyż Zaitrus: parę tysięcy kilometrów od Ablardiusa, w obszarze niewidocznym przez Con Szernief
                    1. Ukryta Baza NavirMed: prowadzone są tam badania na ludziach i nad neurokontrolą na więźniach; jeden z nich - protomag - uciekł.
            1. Sebirialis, orbita
                1. CON Szernief

## Czas

* Opóźnienie: 398
* Dni: 4
