## Metadane

* title: "Chevaleresse"
* threads: alan-opiekun-ucisnionych
* motives: niechetnie-spelniana-rola, spotlight-na-postac, virtual-world, zrzucenie-winy-na-innych, milosc-szczenieca, subtelna-slodka-manipulacja, rozmrazanie-krolowej-lodu
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [190210 - Minerwa i Kwiaty Nadziei](190210-minerwa-i-kwiaty-nadziei)
* [181101 - Wojna o uczciwe półfinały](181101-wojna-o-uczciwe-polfinaly)

### Chronologiczna

* [190210 - Minerwa i Kwiaty Nadziei](190210-minerwa-i-kwiaty-nadziei)

## Projektowanie sesji

### Dark Past

* Alan stworzył gildię Elisquid w multivirt. Ale zabanował go Yyizdath dzięki Pięknotce. Alan stracił kontrolę nad gildią.
* Ktoś inny przejął kontrolę nad Elisquid i prowadzi ją w niewłaściwą stronę. Diana się mu postawiła i przegrała.
* Diana dotarła na Astorię do Alana dowiedzieć się co się stało.
* Starcie na linii Marlena - Diana - Karolina

### Dekompozycja pytaniami

* Czy Alan zostanie odbanowany?
* Czy Alan pozbędzie się (kłopotliwej) Diany?
* Czy Alan i Marlena wejdą w wojnę?
* Czy Alan i Pięknotka wejdą w wojnę?

### Struktura sesji: Frakcje

* Alan: chce odzyskać swoją gildię.
* Alan: chce pozbyć się kłopotliwej Diany.
* Diana: chce odzyskać Alana i sprawić, by on Coś z Tym Zrobił.
* Marlena: nie chce mieć z tym wszystkim nic wspólnego.

### Dark Future

1. Alan nie zostanie odbanowany - przejmie konto Diany.
2. Diana opuści Alana, ale będzie kręcić się koło Zaczęstwa.
3. Alan i Marlena wejdą w wojnę - Marlena doprowadziła do jego zabanowania.

### Pierwsza Scena

Akcja w Cyberszkole - Tymon vs Marlena, Diana i wezwanie Pięknotki

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (21:00)

Pięknotka ma szczękę u ziemi. Została wezwana jako awaryjny terminus do ciężkiej akcji w Cyberszkole. A tam - stojący Tymon w oporządzeniu bojowym, dwie zaszokowane dziewczyny (Wiewiórka i Karolina) oraz skulona zapłakana... piętnastolatka? Sytuacja jest napięta - Tymon sobie z tym NIE poradzi. Marlena powiedziała, że została zaatakowana. Tymon, że został wezwany na ciężką akcję i zaatakował... Diana, że on ją uderzył.

Pięknotka nie poznaje Diany. Ona jest poza systemami Pustogorskimi. Czyli nie jest z Sojuszu Letejskiego. Próba załagodzenia sytuacji (Tp:9,3,3=P). Sytuacja się zaostrzyła. Tymon wyłączył power suit, Karolina machinalnie wezwała energię by zrozumieć co tu się stało - sweep energii. Diana zaczęła rzucać swoje zaklęcie. Marlena schowała się za Tymona. Tymon zrobił jedyną rzecz jaką mógł - opanował siłowo Dianę i Karolinę. Karolina w szoku, Diana przerażona i też w szoku.

Diana w panice wysłała sygnał SOS na hipernecie "Voidcarver, ratunku!!!". Tymon wysłał na ten sam sygnał "terminus na miejscu, spokojnie...".

Pięknotka przyjrzała się Dianie. Jest ranna. Nic poważnego, ale bolesna rana. Na oko jest młoda. Pięknotka i Tymon zaczęli robić perimeter przed ludźmi. (Tp:7,3,4=SS). Udało im się opanować perimeter. Diana zaczęła dyskretnie odpełzać, ranna, ale... to nie konflikt. Terminusi (Pięknotka, Tymon) ją opanują od ręki. Cała reszta magów i ludzi jest opanowana.

I na to wpadł Alan. Cały zaskoczony. Nie wie co się dzieje. Diana się jeszcze bardziej skuliła. Zaczęła szeptać "Diana, nazwa kodowa Chevaleresse, ratunku, Voidcarver" w pętli. Alana jakby raził piorun - nie rozpoznał Diany, ale rozpoznał "Chevaleresse". Nie wiedział, że jest DZIECKIEM. Od razu napadł słownie na Tymona - jak on mógł uderzyć w cywila! Włączył power suit. Tymon odpowiedział włączając swój power suit, że sytuacja tego wymagała.

Pięknotka ZNOWU musi deeskalować. Marlena weszła między terminusów i spytała łamiącym się głosem "strzelicie"? Diana wtula się w nogę power suita ze strachu. A Pięknotka OPIERDALA ICH JAK PSY (Tp:11,3,3=S). Udało się jej deeskalować tym razem.

Alan zignorował wszystkich. Poprosił Tymona i Pięknotkę, by mu zostawili Chevaleresse. On się ją zajmie i będzie za nią odpowiadał. Będzie do ich dyspozycji. Ale ona nie jest z Astorii, jest dzieckiem i jemu ufa. Pięknotka zażądała od niego dostępności do Diany. Alan się zgodził. Pięknotka nigdy nie widziała Alana tak wstrząśniętego. Nikt nigdy nie widział...

Alan opuścił to miejsce ze swoją Chevaleresse. Tymon spojrzał na Pięknotkę z bezradnością. Marlena rozpoznała "Voidcarver" - dowódca gildii Elisquid. Ale... ale czemu niby ONA go zabanowała? Co miała wspólnego? Fakt, Voidcarver nie pojawia się i był sygnał, że on oszukiwał co poważnie nadwątliło morale i działanie gildii. Pięknotka przypomniała sobie, że aby uratować Zaczęstwiaków ONA zabanowała Alana. Nie wiedziała, że Alan gra - nie wyglądał...

Wiewiórka jest załamana. Czuje się winna wielkiego zła.

Pięknotka zabroniła komukolwiek - a zwłaszcza Wiewiórce i Karolinie - robić cokolwiek w tej kwestii. Dziewczyny się zgodziły nie dotykać niczego. Pięknotka poprosiła Tymona by ten przeprosił Karolinę. Też to zrobił. I Pięknotka kazała Wiewiórce i Karolinie iść do Szkoły Magów i nie wpakować się w żadne kłopoty. (6,3,4=P). Wiewiórka powiedziała, że nie zrobi nic głupiego (ale zrobi...).

**Scena**: (21:50)

Następny dzień. W mieszkaniu Alana. Pięknotka aż była zaskoczona...

Pięknotka zobaczyła coś ciekawego. Nasza mała, słodka Diana jest przerażona - ale przestylizowała się (aktorstwo, urok, makijaż) na 14-15 lat. W rzeczywistości ma więcej. Jest przerażona, ale nie aż TAK. A Alan 100% to łyknął. Tymon szczur to zostawił Pięknotce. Nie chciał eskalować z Alanem...

Diana powiedziała, że wyleciała z Elisquid. Elisquid po odejściu Alana wpadło w ręce kogoś innego - i ten ktoś próbuje obrócić gildię w coś mrocznego. Alan zupełnie nie widzi tej delikatnej manipulacji - Pięknotka ją pokazuje po hipernecie (Tr:8,3,6=S). Alan wpadł w gniew. Spokojnie i chłodno kazał Chevaleresse wyjaśnić wszystko, bez słowa kłamstwa, bo ją wyrzuci i nigdy nie chce jej znać.

Chevaleresse wpadła w panikę. Powiedziała wszystko - Elisquid wpadła w złe ręce, ona wyleciała, nie ma nikogo, nie ma rodziny online, przybyła tu z nadzieją że go zobaczy i wszystko się ułoży. Przyleciała z Anathi na gapę. Nie zrobiła researcha. Niech on jej nie wyrzuca, nic innego już nie ma...

A tak w ogóle ma 16 lat. Tylko młodo wygląda.

Alan pozwolił jej na razie zostać. Powiedział, że musi odblokować swoje konto. I znajdzie osobę odpowiedzialną za tego bana i sprawi, że ta osoba zapłaci taką wielokrotność cierpienia jak to co zostało zadane jego kochanej gildii.

Diana jeszcze się przyznała, że jest szczurem ulicznym. Ale świetnie gotuje. Pięknotka uświadomiła Alana po hipernecie, że ona się w nim podkochuje i patrzy nań jak na wielkiego herosa. Alan zrobił się biały. Zwłaszcza, gdy Diana powiedziała, że jej specyficzny hipernet pozwala na to, by Alan połączył się do multivirtu jej kontem.

Alan powiedział, że nawet wystarał się o referencje u Karli. Nawet to yyizdis odrzucił...

Pięknotka ma poważny problem. Jak sprawić, by ALan nie dowiedział się, że to Pięknotka XD. Nie przeszkadza jej wina na Marlenę, ale Pięknotka... to by było niefortunne.

"Pięknotka wrzuciła Marlenę pod autobus". Powiedziała Alanowi, że to Marlena stoi za tym, że on został zabanowany. Alan się jeszcze bardziej zdenerwował - to, że nie chciała to nie jest argument. Jest skłonny skrzywdzić Marlenę - ale nie w realu. Pięknotka próbuje go przekonać żeby nie pogarszał swojej sprawy przed Yyizdathem, żeby faktycznie nie skrzywdził (spalona ziemia) Marleny. (Tr:6,3,6=S).

Alan jest twardo przeciwnikiem Marleny, ale nie ma zamiaru używać pełni artylerii czy spalonej ziemi. Pięknotka odetchnęła z ulgą...

Wpływ:

* Ż: 5
* K: 5

**Sprawdzenie Torów** ():

2 Wpływu -> 50% +1 do toru. Tory:

* 

**Epilog** (22:56)

* Alan rozpoczął wojnę z Marleną, ale na ograniczonym poziomie. Jest strasznym przeciwnikiem ale nie personalnym wrogiem.
* Diana, aka Chevaleresse, zostaje z Alanem. Na długo. Ku wielkiemu utrapieniu terminusa. (2 Kić): Diana załatwiła, że on jest jej prawnym opiekunem.
* Alan nigdy nie dowie się o roli Pięknotki. Wierzy w wyłączną winę Marleny (która się przyznała i wzięła winę na siebie).

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
|               |         |             |

Czyli:

* (K): 

## Streszczenie

Diana, aka Chevaleresse przybyła do Cyberszkoły szukać Alana. Postraszyła Marlenę i skończyła zaatakowana przez Tymona. Alan się zaopiekował swoją członkinią gildii; okazało się, że jego gildia (Elisquid) wpadła w złe ręce. Teraz Alan rozpaczliwie szuka sposobu na to by się odbanować; tymczasowo Chevaleresse zostaje u Alana... KTÓRY STAŁ SIĘ JEJ PRAWNYM OPIEKUNEM. Aha, Alan wie, że to wina Marleny że go zabanowali (a tak naprawdę jest Pięknotki).

## Progresja

* Diana Tevalier: Alan stał się jej prawnym opiekunem.
* Alan Bartozol: zostaje prawnym opiekunem Diany Tevalier, aka Chevaleresse.
* Alan Bartozol: zimny gniew wobec Marleny Mai Leszczyńskiej, co do której myśli, że go zabanowała.
* Alan Bartozol: eskalacja z Tymonem Gruboszem; serio się nie lubią. A fakt że on skrzywdził Dianę nie pomógł.

## Frakcje

* EliSquid Supremus: gildia w multivirt; wpadła w niepowołane zdaniem Diany i Tymona ręce. Przesunęła się w kierunku na PK.

## Zasługi

* Pięknotka Diakon: rozpaczliwie deeskaluje wszystkie problemy między Alanem i Tymonem, między Marleną/Karoliną i Dianą. Potem nieźle się bawi kosztem Alana gdy do niego wprowadza się Chevaleresse.
* Tymon Grubosz: wezwał Pięknotkę na wszelki wypadek - i była potrzebna. Sklupał Dianę i Karolinę. Nie umiał deeskalować z dziewczynami i wolał się wycofać i oddać jej problem.
* Karolina Erenit: w Cyberszkole zaatakowana przez Dianę, potem pchnięta na ziemię przez Tymona gdy ów chciał przerwać jej czar. Ogólnie, nie jej dzień ;-).
* Marlena Maja Leszczyńska: cel ataku Diany (Chevaleresse). Doszła do tego, że jest powiązana z zabanowaniem Alana. Przyznała się i przeprosiła. Teraz Alan jest na nią zły.
* Diana Tevalier: w grze "Chevaleresse", support, 16-latka a wygląda na 13. Przybyła na Astorię z Anathi by odnaleźć Alana, zaatakowała Marlenę, wpadła na Tymona i w końcu udało jej się odnaleźć Alana.
* Alan Bartozol: w grze "Voidcarver", lider EliSquid. Powadził się z Tymonem o Chevaleresse i ją ku swemu utrapieniu przygarnął. Załatwiła go i został jej opiekunem prawnym.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Cyberszkoła: miejsce "walki" Diany z Marleną i Karoliną, przerwanej brutalnie przez Tymona.
                            1. Pustogor:
                                1. Miasteczko: mieszka tam Alan Bartozol. I teraz, dodatkowo, też Diana Tevalier.

## Czas

* Opóźnienie: 2
* Dni: 2
