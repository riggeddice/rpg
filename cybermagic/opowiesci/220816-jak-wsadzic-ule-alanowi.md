## Metadane

* title: "Jak wsadzić Ulę Alanowi?"
* threads: rekiny-a-akademia
* gm: żółw
* players: kić, anadia

## Kontynuacja
### Kampanijna

* [220802 - Gdy prawnik przyjdzie po Rekiny](220802-gdy-prawnik-przyjdzie-po-rekiny)
* [220814 - Stary kot a Trzęsawisko](220814-stary-kot-a-trzesawisko)
* [220819 - Tank as a love letter](220819-tank-as-a-love-letter)

### Chronologiczna

* [220802 - Gdy prawnik przyjdzie po Rekiny](220802-gdy-prawnik-przyjdzie-po-rekiny)

## Plan sesji
### Theme & Vision

* Ci, którzy przychodzą pomóc często są większym problemem niż wrogowie i nie zawsze nienawidzą (Marsen -> Lorena)
* W którymś momencie trzeba podjąć decyzje i działać z konsekwencjami swoich działań (Hipolit -> Marysia)
* Sojusznik jest wrogiem, wróg sojusznikiem i unlikely alliance (Marysia - Amelia - Ernest - Torszecki)

### Co się wydarzyło KIEDYŚ

* Keira, agentka Ernesta, przechwyciła z pustego Apartamentu grupę kultystek Ośmiornicy. A przynajmniej tak to wygląda.
    * Keira + Ernest mają przerażającą opinię wśród Rekinów (Keira is a menace); nikomu to nie przeszkadza.
    * Wszystkie osoby przechwycił Ernest z Apartamentu, m.in. Melissę - "należącą" do Chevaleresse.
* W sprawę zamieszana jest najbardziej "samotna" elegancka dama Diakonów, Mimoza Elegancja Diakon, która nie ma NIC wspólnego z Karoliną.
    * Mimoza miała jakieś działania związane z kultystkami, a przynajmniej na to wszystko wskazuje. Typowe na Diakonkę, nietypowe na Mimozę.
* TAI Hestia jest Skażona ixionem - i jest sojuszniczką Marysi
    * Przeniesienie ixiońskie Marysia -> Hestia (przez kontakt z Marysią Hestia staje się bardziej jak Marysia).
    * Hestia jest aktywną sojuszniczką Marysi i chce, by Ernest x Marysia a nie Ernest x Amelia :3
* Sowińscy są niezadowoleni, uważają to za błąd - wyślą kuzyna Jeremiego by pomógł Marysi. ALE jeszcze się nie stało
* Rekiny się dowiadują, że Marysia stoi za "państwem policyjnym" w Dzielnicy Rekinów
* Amelia Sowińska odzyskała częściowo kontakt z Aurum i Rekinami. Przez to, że chcieli się skonsultować z nią wrt Hestii itp. Przez Hestię.
    * To też jest forma ixiońska, o czym nikt nie ma pojęcia. Amelia nie jest ekspertem, nikt tu nie jest więc "ok, udało się połączyć". Nie wiedzą jak.
        * Czyli jeśli Amelia wejdzie w esuriit w jakimkolwiek stopniu, straci kontakt z Hestią / przez Hestię.0
* Pomniejsze waśnie Marysiowe
    * Liliana Bankierz (po stronie AMZ) nie może darować Marysi "zdrady". Ani Karolinie. Ani nikomu.
    * DJ Babu (po stronie Rekinów) nie może darować Marysi "zdrady" Stasia Arienika i oddania go "tyranizującej mamie".
        * I tego że robi z Rekinów państwo policyjne.
    * Santino Mysiokornik słusznie obawia się oskarżenia o nadmierne zainteresowanie młodymi damami (zwłaszcza Chevaleresse) i chwilowo unika Marysi.
* Liliana Bankierz trafiła do szpitala, do larwy Blakenbauera.
* Lorena ma wsparcie - Marsena Gwozdnika - który wierzy w jej dobroć i kompetencję. Ona ofc musi udawać.

### Co się wydarzyło TERAZ

* .

### Co się stanie

* Hestia ostrzeże Marysię, że coś tu zaczyna się dziać ze strony Amelii
* Prawnik Hipolit żąda by Rekiny były kontrolowane. Ernest też. Ernest nie pozwoli by jakiś PRAWNIK go ograniczał.
    * nazwiskiem Ernesta i Eternią podobno straszy się lokalny biznes w Majkłapcu. Robota Karoliny i Daniela.
* Pojawi się Jeremi i będzie robił audyt; Amelii to nie pasuje.
    * plotka o akcie Marysi.
* Marsen żąda oddania Loreny z rąk podłej Marysi.
* "Wojna" Mimoza - Ernest

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Marysia naprawdę, ale naprawdę chciałaby mieć komponenty do TAI Hestii zanim przybędzie Jeremi. By Hestia mogła to poukrywać, by Jeremi nie miał dostępu do tych wszystkich rzeczy. I te rzeczy powinny już dawno być - sensory, subkomponenty, eksperci lokalni - ale nic z tego się nie dzieje. Tak jakby ktoś sabotował jej próby.

Marysia ma dość. Nacisnąć na Hestię - KTO BLOKUJE TE KOMPONENTY. Hestia sprawdziła pobieżnie - Mimoza. Ktoś, czego Hestia się nie spodziewała. Mimoza wpływa na wewnętrzne kontrakty Aurum w taki sposób, by te komponenty miały opóźnienia. Mimoza powoduje, że przetargi nie są wygrywane przez pachołów Marysi. Mimoza aktywnie sprawia, że tych części nie ma.

Niestety, jeśli sabotaż Mimozy nie zostanie usunięty, Jeremi będzie osobą, która dostanie w pełni aktywną Hestię i będzie mógł gadać z ekspertami. To sprawia, że Jeremi BĘDZIE WIEDZIAŁ co Hestia może mu dać. A jeśli Marysia rozwiąże problem z Mimozą i częściami, to wtedy ZDĄŻY ZANIM Jeremi się pojawi i będzie się dało to wszystko ukryć przed nim. I Marysia będzie miała oczy na Dzielnicę Rekinów a Jeremi nie!

Zanim jednak się udało jej zająć tematem Hestii, przybyła Karo. Szybko i bezwzględnie. Ignorując wszystkich. Chce porozmawiać i porozmawiać z nią TERAZ. Marysia jest nieco zdziwiona.

* K: Laska, słuchaj. Jesteś Sowińska, ważna i w ogóle. Zrób tak by Ulka była przypięta do Alana.
* M: WTF?
* K: No, terminuska. Zmień jej szefa.
* M: Pod kim jest? Tukanem?
* K: Jakimś dupkiem. Gabriel Ursus. Terminus z Aurum /z pogardą
* M: A czemu? Czemu pod Alana, czemu Ula? I dalej jesteś na nią zła czy nie?
* K: Powiedzmy, że ma pewne... pomniejsze zalety.
* M: Czyli nie za karę?
* K: Zależy kogo spytasz.
* M: Mogę spytać Uli
* K: Powie Ci, że w nagrodę.
* M: Ty uważasz, że za karę?
* K: Uważam, że zależy od niej - ale wytargała futrzaka z tarapatów więc musi siedzieć na granicy Trzęsawiska.
* M: JAKIEGO FUTRZAKA? CZEMU FUTRZAK Z TRZĘSAWISKA? I co ma do tego Wiktor Satarail?
* K: O ile wiem nic.
* M: Futrzak.
* K: Vistermin. Anty-anomalny kot. Czy to jest WAŻNE? Fajny futrzak, szukał właścicieli którzy zginęli na Trzęsawisku...
* M: Co Ty robiłaś na Trzęsawisku?
* K: Nie byłam tam, znalazłam półmartwego futrzaka i go odniosłam... (tu randomowe chaotyczne zdania Karo wyjaśniające kiepsko co się stało i o co chodziło)

Czyli jest kot. Kot jest niebezpieczny. Karo go znalazła. Była na granicach Trzęsawiska, bo tam może polatać "bezpiecznie". Marysia chciała zrobić pomniejszy OPR, ale nie może - ona osobiście poszła do Sataraila... i NIE, to nie kot Olgi. Marysia pyta czemu nie przekazać SUPERNIEBEZPIECZNEGO kota Oldze? Karo: bo kot ma nowego człowieka i to Ula, Ula go.

* M: Wiesz że są nielegalne?
* K: Nie tam gdzie jest. To granica Trzęsawiska. To że STAŁ się nielegalny nie znaczy że BYŁ nielegalny kilkanaście lat temu. Żyje. Utłuczesz kota?
* M: Myślałam o przekazaniu Oldze a nie o utłuczeniu! /obrażona
* K: Chcesz to przekonuj Olgę. Ja wolę by Ula dostała Alana który jej wyjaśni jak radzić sobie z tym kotem na tej farmie.
* M: JAKA FARMA?!
* K: Farma na granicy Trzęsawiska
* M: Czyja?
* K: Już niczyja bo właściciele zginęli na Trzęsawisku
* M: Jak zginęli?
* K: Trzęsawisko ich zabiło
* M: Nie kot?
* K: Nie. Kot próbował ich ratować i sam prawie zginął
* M: I Ula jest teraz na tej farmie z kotkiem?
* K: Tak. Ula jest teraz na tej farmie z kotkiem.
* M: ...nie wiem czy chcę mieć Ulę na sumieniu...
* K: Kot Uli nic nie zrobi bo Ula to jest człowiek tego kota.
* M: Ale Olga ma dobre miejsce na koty. Jak Ula będzie chodzić do pracy?
* K: DLATEGO Ula ma dostać Alana, który ma dać jej zadanie pilnowania farmy i kota a nie papierków jak wsadził ją cholerny Gabriel.
* K: Dobra, spróbuję POWOLI /i wyjaśnia. Lepiej. Nie najlepiej. Kiepsko. Ale się stara i mniej więcej dobrze wyjaśnia.

...

* M: Czy Alan się na to zgodzi?
* K: Nie pytałam. Ale jest bardzo obowiązkowy.
* M: Chcesz go WROBIĆ w opiekę nad Ulą.
* K: OCZYWIŚCIE! Lepsze to niż dawać laskę która nie znosi Aurum do gościa co liże buty Aurum.
* K: NIE CHCĘ, by Ula coś wiedziała i łączyła to ze mną. Chcę JEJ pomóc.
* M: Chcesz JEJ pomóc czy pomóc KOTU?
* K: OBA. Jak pomogę jej, pomogę kotu i nie da się zeżreć, będzie bezpieczniejsza.
* M: Jak będzie w Pustogorze będzie bezpieczniejsza.
* K: Ale nie będzie bo ma kota.

Marysia NIE wrobi Ulę w życie z kotem na pustkowiu XD. Są granice okrucieństwa. Jak Ula chce to tak, ale może Karo się chce jej pozbyć? Karo na to, że zostawiłaby ją na papierach pod Gabrielem jakby chciała się jej pozbyć. Tu Marysia jest stanowcza.

* K: Chcesz - porozmawiaj z Ulą. Ale ja nie mam z tym nic wspólnego.

Czyli Karo chce, by Alan dostał Ulę. Koniec. I w ten sposób Ula zajmie się kotem. Marysia ŻĄDA porozmawiania z Ulą. Da się wrobić Alana, spoko, ale nie Ulę.

Karo pyta Ulę na hipernecie czy ta woli być pod Gabrielem czy kimś innym. Ula jest nieufna - nie rozumie gdzie jest haczyk. Karo mówi że chce pomóc kotu a Ula jest jedyną opcją. Ula potwierdziła - Gabriel nie nadaje się na jej opiekuna. Nic się nie nauczy. Ula z przyjemnością mieszka na fortifarmie, ale się boi - chciałaby mieć wsparcie. Niekoniecznie kogoś ze sobą (jej jest dobrze z samotnością), chce wiedzieć, że Trzęsawisko lub fortifarma jej nie dorwą jeśli już ma tu być.

Czyli - Marysia widzi, że przeniesienie Uli pod Alana (przed czym Alan będzie bronił się WSZYSTKIMI kończynami!) jest optymalnym rozwiązaniem.

Marysia informuje Karo, że jak się zacznie coś pieprzyć to KARO ma pomóc. Np. kot zje człowiek lub Tukan straci szacun u terminusów. Albo nogę.

Marysia kontaktuje się z Tukanem.

* T: tien Sowińska, co za zaszczyt. Jak mogę pomóc? /z uśmiechem
* M: terminusie, czy byłaby opcja zmiany przełożonego dla subterminuski Miłkowicz?
* T: opcja jest zawsze
* M: Najlepiej na terminusa Bartozola
* T: (szeroki uśmiech) oczywiście, że jest taka możliwość. Czy jakiś konkretny pretekst, czy mam zrobić po swojemu?
* M: Dowolnie, ale tak, by im nie zaszkodzić
* T: (mniejszy uśmiech) oczywiście. (chwila ciszy) Czy jest coś co powinienem wiedzieć by Twoje cele były spełnione?
* M: Wszystkie koty Uli mają przeżyć. A jest tam vistermin.
* T: (przyklejony uśmiech) tien Sowińska, zastanów się ponownie. Subterminuska może zginąć do vistermina.
* M: Dostałam informację, że kot został oswojony przez subterminuskę a ona wyraziła chęć zajęcia się tym i mieszkania na fortifarmie.
* T: Subterminuski są jak dzieci. Dużo chcą, nic nie osiągają i robią sobie krzywdę. Czy NA PEWNO bierzesz na SIEBIE ryzyko, że może tej subterminusce stać się krzywda?
* M: Behawiorysta dla vistermina?
* T: Kontrpropozycja: przekaż ją pode mnie. Ja zadbam o to, by poradziła sobie z visterminem a potem przekażę ją terminusowi Bartozolowi.

Tak. Przy całym czarnym sercu Tukana i całym oportunizmie Tukana Tukan się autentycznie przestraszył o to, co spotka subterminuskę Urszulę Miłkowicz. Uważa pomysł Marysi za IDIOTYCZNY i SADYSTYCZNY, ale emerytura to emerytura. Ale po raz pierwszy Tukan zaczął się zastanawiać, czy emerytura jest warta tak czarnego sumienia. BO NIE WIE, ŻE WSZYSCY CHCĄ LASCE POMÓC!

Marysia powiedziała, że się odezwie, musi PRZEMYŚLEĆ. Tukan odetchnął z ulgą. Marysia skierowała się do Karo. Przecież Alan, Tukan... to nie powinno być trudne by jej to wyjaśnić. BO MARYSIA NIE WIE O TAI. BO KARO JEJ NIE POWIEDZIAŁA...

* M: Karo? Sprawa o którą mi mówiłaś - 2 miesiące pierwsze Ula pod Tukanem?
* K: Tą śluzowatą łajzą? Oooo nie.
* M: Dlaczego?
* K: Bo jest śluzowatą obślizgłą łajzą.
* M: (wykłada Tukanowe zalety)
* K: Ale Tukan się nie zna na Trzęsawisku a Alan się zna. I o to chodzi bo Ula mieszka NA GRANICY TRZĘSAWISKA. O ile wiem, Alan jest ostatnim żyjącym terminusem który wie coś o Trzęsawisku.
* M: Pod Alanem, ale Tukan zapewnia obsadę behawiorystów itp?
* K: Ula ceni sobie prywatność i spokój. Wsadzisz bandę cholernych ludzi, i to od tej śluzowatej łajzy...
* M: Czy Ty chcesz ZABIĆ Ulę?
* K: Nie, nic jej nie grozi. Z mojej strony. A miejscówka jest dość bezpieczna.
* M: ALE MA DWA KOTY! (przedstawia mnóstwo o kotach. Karo odpłynęła w 15% rozmowy. Marysia zauważyła.)
* M: Karo, mieszkasz u siebie na kwadracie czy gdzie... tam... się mieszka. Wyobraź sobie że masz taki ścigacz jak ten co zabili Ernestowi. I mieszkacie razem. I... nagle... każą Ci się przeprowadzić do domu ścigacza. I są tam też ścigacze Skażone... (Marysia powiedziała to dużo ładniej)
* K: Rozumiem pojęcie TERYTORIALNOŚCI, ok?
* M: Nie skończyłam! Każą Ci się z tym małym ścigaczem przeprowadzić się do Skażonego ścigacza i Skażony ścigacz próbuje Ci zabić ścigacz który miałaś całe życie. I na tym polega terytorialność kotów.
* K: TEN ARGUMENT JEST INWALIDĄ!
* M: Ale czy broniłabyś ścigacza który miałaś tyle lat?
* K: .........jak już powiedziałam, to porównanie jest inwalidą.........
* M: Ale unikasz odpowiedzi.
* K: .........
* M: Ktoś musi nauczyć Ulę jak radzić sobie z kotem
* K: Obślizgła łajza się nie zna!
* M: On zapłaci za behawiorystów
* K: Zrobią tłum! Bez sensu!
* M: Karo... o co Ci chodzi...

Minęło DUŻO czasu. W końcu dziewczynom udało się dojść do względnego porozumienia:

* Ula dostaje Pawła Szprotkę i Ignacego Myrczka.
    * Paweł pomoże z opanowaniem vistermina
    * Miłym efektem ubocznym będzie to, że Myrczek może się zakochać w Uli a nie w Sabinie Kazitan XD
* Ula trafia pod Alana Bartozola.
* Marysia WIE, że Karo coś ukrywa. I nie chce nikomu powiedzieć. I Karo to powiedziała. I Ula to wie.
    * CZYLI KARO I ULA MAJĄ SEKRETY PRZED MARYSIĄ!
    * I Tukan nie może wiedzieć.
    * I Karo uważa, że Marysi to nie zaszkodzi w przyszłości. A przynajmniej nie powinno.

Marysia ma tylko jeden problem. Jak się NIE WYRĄBAĆ na audycie jak się to wszystko zrobi... i powiedziała Karo, że ma problem. Niedługo audyt. I Marysia boi się, że jak zrobi coś głupiego to może mieć straszne negatywne konsekwencje - np. mogą ją odwołać do Aurum i wydać za jakiegoś podłego starca. Ale Marysia może teraz COŚ zrobić i może pomóc i działać.

Karo wspina się na szczyt dyplomacji: "jeśli oślizgła łajza jest opiekunem Uli, może coś wyjść i zaszkodzić. Jeśli Alan - nie. Ale nie chcesz by Alan wiedział że to Marysia bo on się zemści. I zemści się strasznie."

Marysia wpadła na INNY pomysł - jeśli Alan jest tego typu osobą to MOŻE PRZEKIEROWAĆ JEGO GNIEW NA MIMOZĘ. Usunąć rywalkę. Pokonać złą osobę. Ale czy Mimoza zasłużyła na to? I czy Karolina by to wybaczyła? I nawet jakby Marysi się upiekło, czy Marysia jest osobą, która by to zrobiła?

Dobrze - Mimoza. Dlaczego Mimoza sabotuje konsekwentnie plany Marysi? Zwłaszcza to teraz? Zwłaszcza, jak ma audyt na karku i Rekiny już doceniły, że Marysia przecież jest potrzebna do kontrolowania Ernesta? Czemu Mimoza jej w tym AKTYWNIE przeszkadza - zawsze była politycznie pasywna? Gdyby nie Mimoza, plany Marysi by się zrealizowały... a Ernest byłby przeszczęśliwy, gdyby Mimoza zniknęła mu z drogi. Bo Mimoza wyraźnie buduje kapitał społeczny w okolicy. Jest jedną z nielicznych Rekinów która jest naprawdę lubiana przez lokalnych.

Mistrzowska, subtelna królowa polityki - MIMOZA DIAKON! Ale Marysia ją pokona!

Jak Marysia może dowiedzieć się, czemu Mimoza blokuje jej transporty? Najprościej - pójść jej zapytać. I to zdecydowała się zrobić XD. Mimoza przyjęła Marysię ze spokojną elegancją. 100% uprzejmości.

Marysia poprosiła Mimozę o szczerą rozmowę, acz z zachowaniem pełnej uprzejmości (całkowicie bez cienia ironii). Mimoza się zgodziła, z pełnią uprzejmością i bez cienia ironii. Marysia zaczęła, że martwi się, że Mimoza jest wrabiana w to że części i czujniki itp. Więc chce ostrzec Mimozę że ktoś ją potencjalnie wrabia zanim Marysia zajmie się sprawą przy użyciu potęgi Sowińskich. Ale wszystko uprzejmie. I szczerze.

Mimoza odpowiedziała podobnie uprzejmie i podobnie szczerze. TAK ONA SERIO SABOTUJE. I nie robi tego ze złośliwości. Marysia popija herbatkę podobnie jak Mimoza.

* Ma: "Czy zechcesz mi powiedzieć, dlaczego podjęłaś taką decyzję?" /herbatka
* Mi: "Ależ oczywiście, szlachetna koleżanko" /herbatka "Brakuje podzespołów dla okolicznych firm dekontaminacyjnych"
* Ma (do siebie, myśli): Co? Ona jest pierdolnięta...
* Ma (na głos): "Czy, szlachetna koleżanko, możesz wyjaśnić dokładniej?"

Mimoza wyjaśniła. Podczas wielu operacji skupiła się na czyszczeniu niektórych obszarów szczególnie dotkniętych po wojnie. Obszarów z bardziej niebezpiecznymi anomaliami. Zapłaciła częściowo agentom z Aurum i częściowo osobom lokalnym, plus zawarła korzystne kontrakty dla lokalnych przedsiębiorstw. Niestety, sprzęt który Marysia potrzebuje konkuruje ze sprzętem który potrzebny jest do pracy na tym terenie.

Czyli ruchy Marysi, zdaniem Mimozy, sprawiają, że lokalne firmy nie są w stanie wypełniać kontraktów i płacą kary umowne. Więc Mimoza zaczęła osłabiać i sabotować działania Marysi, by lokalne firmy mogły kupować części i mieć dostęp do specjalistów.

Marysia (w duchu do siebie): "ona rzeczywiście jest jebanym świętym...". Marysia chce się dowiedzieć co ona chce i na czym jej zależy. Czy udaje czy mówi serio.

Tr +3:

* V: TAK. Mimoza święcie w to wierzy. Mimoza próbuje być najlepszą arystokratką Aurum jaką powinna być każda arystokratka. Exemplar. Żywy przykład. To najstraszniejsza rzecz jaką Marysia w życiu widziała. Arystokratka która chce pomóc i wierzy w to, że jej pieniądze mają wspierać okolicę.

Ona nie gra w politykę. Ona taka jest. I nagle Marysi się przykro zrobiło. Mogła ją wrobić, ale... wrobić TO? To aż smutno. Ona osobiście podnosi opinię o Rekinach w okolicy. Ale to znaczy, że Ernest się myli - podejrzewa Mimozę o wszystko co najgorsze a ona po prostu robi swoje i go ignoruje. A zwalcza go, gdy Ernest wchodzi w szkodę osób, które Mimoza uznała za potrzebne do ochrony.

Tak, Mimoza nie ufa Eterni. To widać...

## Streszczenie

Części do Hestii nadal nie dotarły do Marysi - okazuje się, że Mimoza ją sabotuje. Więc jest ryzyko, że części do Hestii dojdą i Jeremi Sowiński będzie miał dostęp do pełni mocy Hestii a Marysia nie XD. Dodatkowo, Karo poprosiła Marysię by ta wsadziła Ulę Alanowi, ale nie chcą zabijać kota. Ale JAK wsadzić Alanowi Ulę (z kotem) jako uczennicę, skoro vistermin jest morderczy, zbliża się audyt Marysi a w Tukanie obudził się terminus (i nie chce ryzykować Uli)? I do tego Alan nie może się dowiedzieć? Do tego okazuje się że części do Hestii konkurują z normalnymi częściami potrzebnymi na tym terenie i DLATEGO Mimoza - paladynka cholerna - blokuje Marysię...

## Progresja

* Mimoza Diakon: TAK, ona naprawdę nie gra w politykę. Próbuje być najlepszą arystokratką Aurum jaką powinna być każda arystokratka. Exemplar. Żywy przykład. I Marysia to wie.

### Frakcji

* .

## Zasługi

* Marysia Sowińska: skomplikowany problem - Mimoza blokuje części do Hestii bo ratuje region, Karo prosi o wsadzenie Uli Alanowi ale audyt i vistermin... Marysia zbiera dane i się nieco miota.
* Karolina Terienak: po raz pierwszy w życiu poprosiła o coś Marysię - niech wsadzi Ulę Alanowi. Chroni informację o TAI i chroni fortifarmę przed Tukanem. Ostro kłóci się z Marysią XD.
* Urszula Miłkowicz: zdecydowanie woli życie w samotności na fortifarmie (choć chce być nieco bezpieczniejsza) niż w blokowisku. Ratuje swojego kota przez visterminem Rozbójnikiem.
* Mimoza Diakon: blokuje rozbudowę TAI Hestia bo region potrzebuje tych części bardziej; porozmawiała z Marysią i pokazała, że ona nie udaje - ona taka jest
* Tomasz Tukan: jakkolwiek spełnia prośby Marysi, zmartwiła go prośba by wsadzić Ulę (z visterminem!) pod Alana bo się o Ulę boi. Może gardzić subterminuskami, ale nie chce ich krzywdy - więc się postawił Marysi.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Dzielnica Luksusu Rekinów

## Czas

* Opóźnienie: 1
* Dni: 1

## Konflikty

* 1 - Marysia chce się dowiedzieć co Mimoza chce i na czym jej zależy. Czy udaje czy mówi serio. Czemu sabotuje ruchy Marysi.
    * Tr +3
    * * V: TAK. Mimoza święcie w to wierzy. Mimoza próbuje być najlepszą arystokratką Aurum jaką powinna być każda arystokratka. Exemplar. Żywy przykład.
* 2 - 
    * 
    * 
* 3 - 
    * 
    * 
* 4 - 
    * 
    * 
* 5 - 
    * 
    * 
* 6 - 
    * 
    * 
* 7 - 
    * 
    * 
* 8 - 
    * 
    * 
    * 
* 9 - 
    * 
    * 
* 10 - 
    * 
    * 

## Kto

