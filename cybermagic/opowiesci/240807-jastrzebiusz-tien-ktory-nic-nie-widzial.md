## Metadane

* title: "Jastrzębiusz, tien, który nic nie widział"
* threads: mscizab-rywal-arianny
* motives: the-addicted, gift-is-anthrax, dziwna-zmiana-zachowania, pulapka-bardzo-skomplikowana, zawisc-mordercza, potwor-spawner, koniecznosc-pelnej-dyskrecji, ratowanie-dzieciaka
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [240515 - SOS z Rinosates](240515-sos-z-rinosates)

### Chronologiczna

* [240515 - SOS z Rinosates](240515-sos-z-rinosates)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * Lydia the Bard "Isabella's Villain Song": 
        * "Stand upstraight, stick it through | Uphold the family that hurts and harms you and asks too much of you"
        * "How could you when I was just a child | Support the family don't stray display obey you must be perfect"
        * "what could you possibly expect was gonna happen when you made me | tend to a plant with poison and expect a flower are you crazy?"
        * Kluczem jest **smutek** oraz przekroczenie "breaking point"
    * Inspiracje
        * "Black Swan" (film), o perfekcji i obsesji perfekcji i jak daleko się można posunąć
        * Vinerva @ Shadows of the Forbidden Gods
            * Vinerva to bogini, która daje bogactwo, zdrowie i potęgę, skażając serca i prowadząc do zniszczenia
            * Vinerva, technicznie, jest **the-corruptor**. Czyli ona pragnie Cię wzmocnić by Cię usidlić.
            * Złapała i usidliła Mścizęba
* Opowieść o (Theme and vision):
    * "Dziecko, nigdy dość dobre, naciśnięte za mocno, próbuje wygrać szacunek i miłość. Ale nie daje rady."
    * "The child who receives no warmth will burn the village just to feel its warmth"
        * Mściząb bardzo pragnął być jak Arianna czy Viorika czy inni wartościowi członkowie Verlenów. Ale nie jest w stanie.
        * 2 lata temu Mściząb natrafił na Ariannę gdy robił coś bardzo głupiego i ona go upokorzyła.
        * Mściząb miał pecha natrafić na istotę [Interis/Esuriit]. Coś, co "daje" mu moc w zamian za ofiary i odbiera mu człowieczeństwo.
        * Mściząb pragnie osiągnąć swój poziom kompetencji i umiejętności. Pragnie być JAK ARIANNA. Nie ma po co żyć jako śmieć.
    * Lokalizacja: Verlenland
* Po co ta sesja?
    * Jak wzbogaca świat
        * Pokaże, jak Verlenowie traktują "słabe" jednostki
        * Pokaże wysoki poziom autonomii w rodzie Verlen
        * Pokaże pomocnych Verlenów, którzy chcą pomóc - i tych, którzy trzymają się okrutnych reguł Verlenlandu, starej daty.
    * Czemu jest ciekawa?
        * Pokazuje Ariannie i Viorice ich własne lustro
        * Dylemat: uratować OFIARY młodego Verlena czy samego Verlena czy spróbować uratować wszystko ryzykując utratę wszystkiego?
    * Jakie emocje?
        * "Mściząb znowu coś odpierniczył..." -> "...co on zrobił... czemu..."
            * Od lekkiej frustracji do pełnego szoku - Fallen Verlen
            * I zrozumienie DLACZEGO upadł. I swojej roli w tym upadku.
        * "Coś tu jest poważnie nie tak, ale damy radę" -> "CZEMU NIE WEZWAŁYŚMY POMOCY!"
            * Od przekonania o możliwościach do uzmysłowienia sobie z CZYM walczą
            * Dlaczego Verlen zarządzający tym terenem nic nie robi i go nie ma?!
        * "Nie chcę musieć wybierać Miasteczko / Mściząb!"
            * prawdziwa pokusa związana z Darami Leniperii - I can get it all
            * dylemat. Mściząb w końcu mógł być 'sensowny', ale nie miał szczęścia
        * "Rozwalę tego potwora!!! Za Mścizęba!!!"
            * widząc, co Leniperia zrobiła Mścizębowi.
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * the-addicted: Mściząb Verlen. Jego narkotykiem jest potęga i akceptacja Rodu, jest możliwość upokorzenia Arianny i udowodnienia, że jest lepszy od niej. Coraz bardziej traci wszystko by stać się jak ona. Unchained.
    * gift-is-anthrax: Leniperia (kontrolowana przez Lycię) zostawia podarunki i możliwości wzmocnienia się; te podarunki wzmacniają, ale kosztem powolnego uzależnienia od Leniperii. To wzmacnia Mścizęba i Karasiożera i tworzy kult.
    * dziwna-zmiana-zachowania: Mściząb zrobił się kompetentny oraz bardzo agresywny wobec Arianny; rywalizuje z nią i może nie daje rady, ale jest bliżej niż powinien być. Jest też okrutniejszy.
    * pulapka-bardzo-skomplikowana: Lycia (i Leniperia) szuka następnego agenta, Mściząb zasugerował (nieświadomie) Ariannę. Cała sesja jest pułapką na Ariannę i Viorikę.
    * zawisc-mordercza: Mściząb wobec Arianny - jego skaza sprawiła, że Leniperia miała otwarcie i mogła Mścizęba Dotknąć.
    * potwor-spawner: Leniperia jest w stanie wykorzystać część swojej energii by móc spawnować inne potwory.
    * koniecznosc-pelnej-dyskrecji: Arianna i Viorika działają na terenie innego Verlena, któremu nie zależy tak na ludziach w swojej domenie. A one chcą zatrzymać Mścizęba jak on robi głupoty.
    * ratowanie-dzieciaka: Karasiożer Verlen jest zapatrzony w Mścizęba, jest powiązany z siłami na tym terenie. Mściząb przysposabia go do dołączenia do Leniperii.
* O co grają Gracze?
    * Sukces:
        * Uratować Mścizęba
        * Uratować Miasteczko
        * Karasiożer jest bezpieczny przed Leniperią
        * Odegnać Leniperię z Verlenlandu
    * Porażka: 
        * W służbie Leniperii
        * Śmierć zarówno Mścizęba jak i Miasteczka
        * Karasiożer przyjął dar Leniperii
* O co gra MG?
    * Highlevel
        * Chcę pokazać im słabe i mroczne strony Verlenów. Te fajne już znają.
    * Co chcę uzyskać fabularnie
        * Spróbuję dołączyć je do Leniperii. Niech się Skażą.
            * Używając Interis zabiorę im umiejętność ostrzeżenia o Leniperii.
        * Karasiożer dołącza do Leniperii.
        * Zarówno Miasteczko jak i Mściząb są zniszczone.
        * Nie są w stanie ostrzec nikogo przed Leniperią; ona nadal tu jest i się żywi.
        * Wszyscy są przekonani, że to wszystko jest winą Arianny - tak jak Elena.
        * Pojawia się Mroczny Oddział Leniperii.
* Default Future
    * Miasteczko zostanie zjedzone
    * Mściząb wciągnie pod kontrolę Leniperii Karasiożera Verlena
    * Leniperia będzie w stanie rozprzestrzenić się na kolejne miejsca; ma niewielki, dobrze ukryty oddział
* Dilemma: Mściząb CZY Miasteczko? Wezwać pomoc CZY samemu?
* Crisis source: 
    * HEART: "The child who receives no warmth will burn the village just to feel its warmth"
    * VISIBLE: "Znikają ludzie w niedalekiej okolicy"
        
### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

* Mściząb napotkał Leniperię na jednej z innych operacji. Jego głód pokonania Arianny sprawił, że Leniperia znalazła okazję.
* Mściząb przyjął kilka Darów Leniperii niszcząc tworzone przez nią istoty, dzięki czemu jego reputacja zaczęła rosnąć.
* Leniperia przybyła do Rinosates; zaczęła rozpuszczać macki. Mściząb - nieświadomy - wspiera Leniperię w zniszczeniu Rinosates.
* Karasiożer przybył z rozkazu Jastrzębiusza, niech się uczy od lepszych od siebie.
* Ludzie znikają, Mściząb zwalcza Spawnera, ale za wolno i nieskutecznie
* Leniperia skupia się na ludziach próbujących opuścić to miejsce
* Część ludzi wie o tym że "emigranci nie dotarli", ale Karasiożer ani Mściząb nic z tym nie robią
* Palaos decyduje się na ryzykowną wyprawę - niech Viorika lub Arianna pomogą...

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

* "The Spawner" - wprowadzenie potwora, pierwsza sesja
    * Faza 2: "Spawner falls" (75 min) <- Mściząb 
        * Tu celem Mścizęba jest skłonienie Karasiożera do wzięcia Darów Leniperii.
            * s1: brak nadziei, starcie co i jak (konieczność deeskalacji)
            * s2: starcia z oddziałem potworów (wzięcie tabletek)
                * hulk, kamienniak
            * s3: porwani ludzie - Mściząb i Karasiożer chcą wejść odzyskać (z sukcesem!)
                * decadia invictus na pierwszej linii
        * Operacja wykorzystania ludzi by zlokalizować i zniszczyć Spawnera
            * s3: 
        * Uda się, ale straty...
        * Mściząb chce zniszczyć Dekadię Invictus, infiltracja + wysunąć ich na pierwszy ogień
        * -> aktywnie graj o zniszczenie miasteczka i porywanie ludzi przez Spawnera
        * -> aktywnie graj o to, by Mściząb zniszczył Spawnera
        * -> aktywnie graj o to, by Karasiożer pozyskał Dar Leniperii
        * -> aktywnie graj o śmierć cywili i Dekadię Invictus CHYBA że Gracze użyją Darów
        * -> pokaż, że to NIE KONIEC problemu 
            * (nie zgadza się ilość osób znikniętych)
            * (nie zgadzają się opowieści tych co zostali)
            * (potwór, który został)
* "The Mastermind" - wprowadzenie Leniperii, druga sesja
    * Interludium: to-co-będzie-potrzebne-do-demonstracji
    * Faza 3: "Konspiracja narasta"
        * ? 
    * Faza 4: "Mastermind ujawniony, wojna domowa"
        * ?
* Overall
    * chains:
        * energia i zasoby Zespołu: max 2
        * morale Zespołu: max 2
        * Dusza Karasiożera: (pragnę Daru - Mściząb ma rację - (Mściząb jest super) - Arianna jest super - można zostawić to innym)
        * To wszystko wina A+V: ((nie) - w sumie bo tu są - tak - gdyby nie one byłoby dobrze)
        * Służki Leniperii: ((nie) - wzmocnienie działa - muszę mieć więcej - upadłe Verlenki)
        * rozpad Rinosates: (pokój - (starcia) - przelew krwi - interwencja tiena - wojna domowa)
        * Armia Leniperii: (brak - byle co - (przydatna) - oddział - spora - legion)
        * problemy polityczne: ((brak) - czemu to są A+V - czemu szkodzą A+V - wygnanie stąd - wojna domowa)
    * stakes:
        * śmierć Mścizęba, śmierć Karasiożera, Dar Karasiożera
        * reputacja Arianny i Vioriki, problemy polityczne
        * rozprzestrzenienie się sił Leniperii
        * śmierć Rinosates, zniszczenia w Rinosates
    * opponent
        * Mściząb, Spawner, Leniperia
    * problem
        * Leniperia jest nie do ruszenia
        * Mściząb jest 'za daleko' i jest złamany przez Verlenów (a potem przez Leniperię)

## Sesja - analiza
### Mapa Frakcji

* Jastrzębiusz Verlen (Lokalny tien Verlen)
    * potężny politycznie izolacjonista, pro-darwinistyczny; ludzie to zasób
    * oneliner: "jeśli sobie nie poradzą sami, będę interweniował - ale zapłacą"
    * eksport: (irrelevant)
    * import: (irrelevant)
    * zasoby: silna siła ognia, magitech
    * agenda: 'stal hartuje się w ogniu', 'za wszystko płacisz'
    * cechy: 'piekło było na Ziemi, on stał na górze', skrajny militarysta
* Kult Kła Zemsty Rinosates (około 300 osób): 
    * pro-leniperiański kult stworzony przez Mścizęba Verlena
    * oneliner: "Siła i determinacja ugasi Twoje cierpienie"
    * eksport: bezpieczeństwo, szkolenia bojowe, broń, morale, usługi (patrol, fanatyczna walka)
    * import: ludzie (okolica), Dary Leniperii (Leniperia)
    * zasoby: oddział, fanatyzm, broń, znajomość lokalsów i terenu
    * agenda: ekspansja, ochrona okolicy
    * cechy: edgy warriors, fanatical
* Rinosates Decadia Invictus (około 80 osób):
    * (będzie ciekawiej, jeśli to miasteczko z populacją postnoktiańską; pasuje do przyszłej historii Arianny)
    * dekadiańska grupa samopomocowa dążąca do rozwiązania co tu się dzieje
    * oneliner: "noktianie powinni trzymać się noktian"
    * eksport: bezpieczeństwo, wiedza o anomaliach, wiedza o terenie, hideouts
    * import: sprzęt, intel, material, medykamenty
    * agenda: co tu się dzieje?, ochrona okolicy, ochrona swoich
    * cechy: bardzo nieufni, klanowcy dekadiańscy, anty-magowie, anty-verlenowie
* Rinosates Lojaliści Verleńscy (około 900 osób):
    * lokalni ludzie nadal wierzący w Verlenów i w ich pomoc
    * oneliner: "pomagali nam a my im; to nowa sytuacja"
    * eksport: (irrelevant)
    * import: (irrelevant)
    * zasoby: dostęp do większości miejsc i rzeczy w Miasteczku
    * agenda: ekspansja, przetrwanie, wspieranie Verlenów
    * cechy: pozytywnie nastawieni, przetrwamy piekło, populacja
* Rinosates Proemigranci (około 700 osób):
    * lokalni ludzie uważający, że Verlenowie zawiedli
    * oneliner: "mają jedno zadanie - a i tak zawiedli"
    * eksport: główne źródło ofiar dla Potworów
* Unaffiliated (około 4k osób)

### Potwory i blokady

* Jednostka Oddziału Spawnera
    * akcje: "wolno przeładowujący się strzał", "ranię szponem", "udaję kamień"
    * siły: "niewidoczny bez ruchu", "bardzo silny", "spojrzenie Koszmaru"
    * defensywy: "bardzo ciężki pancerz"
    * słabości: "wolny i toporny", "niezgrabny", "wolno strzela", "mało celny"
    * zachowania: "atakuję frontalnie", "atakuję z zaskoczenia"
* Spawner
    * akcje: "wystrzeliwuję Sieć Rozpaczy", "zamykam ofiarę w kokonie", "atakuję pnączomacką", "Oczy Terroru", "Chowam się w ścianie", "spawnuję Wojownika"
    * siły: "atak z tysiąca stron", "corrupting touch", "nieprawdopodobnie silny"
    * defensywy: "macki zasłaniają skałą", "nieciągły byt roślinny", "Interis chroni przed żywiołami"
    * słabości: "wolny", "nieciągły i ma jądro"
    * zachowania: "otacza przeciwnika", "corruption over destruction", "atakuje spod ziemi"
* Labirynt Korytarzy
    * akcje: "zagubienie", "nie da się manewrować", "za ciasno by przejść", "atak od tyłu", "szczelina"
    * siły: "ryzyko zawału", "odcięcie od sojuszników", "spowolnienie ruchów bo ciasno", "fałszywe sygnały sensorów"
    * słabości: "stałe punkty orientacyjne"

Inne - jak się manifestują Dary Leniperii?

* Dziwne Zwoje, wzmacniające użytkownika
* Dziwne Żywe Tatuaże
* Stymulanty (w strzykawkach)
* -> ważne: nie działają, jeśli ich nie weźmiesz aktywnie.

### Fiszki

* Jastrzębiusz Verlen: Zimny, pragmatyczny, darwinista, skupiający się na eksperymentach i militariach. Pattern: (Shockwave IDW)
* Karasiożer Verlen: Optymistyczny i zaczepny aspirujący wojownik, 16 lat, Exemplis+Alucis. Pattern: (Powerglide G1)
* Palaos Kregner: Stary zwiadowca, przekradł się wierząc, że Viorika pomoże. Noktianin, klarkartianin. Pattern: (Westley 'as you wish')
* Tiberius Skarn: członek Decadii Invictus; silny wojownik, acz już stary. Poświęci się za przyjaciół. Pattern: (stary Schwarzenegger)
* Nariad Fargelin: członek Decadii Invictus; łowca nagród, świetny sabotażysta, szlachetny i bystry. Pattern: (commander Ashley Lynx @ Solty Rei)
* Julia Strzałowik: ciekawska i odważna, wierzy we wspieranie Verlenów. Pattern: (Hermiona Granger)
* Zara Skarn: młoda, ambitna inżynierka, śmiała i kreatywna, pro-Verleńska. Pattern: (Kaylee Frye z "Firefly")
* Szymon Garwok: żarliwy i cichy, 'moja wiara w Verlenów moim sukcesem', Kieł Zemsty. Pattern: (mnich z 'Rogue Squardron')
* Leon Oszakowski: cichy specjalista od walki z potworami. Pattern: (Menasor G1 'power to destroy')
* Kasia Mribkit: infiltratorka, chroniąca Karasiożera, zawsze w okolicy. Pattern: (Ravage IDW)
* Eryk Tewatrik: arogancki naukowiec i wynalazca, który 'odkrył' Dary Leniperii. Pattern: (Brainstorm IDW)
* Mściząb Verlen: magia przede wszystkim bojowa i działania rozproszone (delegacja). Ekspresja: kontrola, link (Exemplis, Praecis).
* Symfonides Verlen: soniczny Verlen z wielkim sercem (Exemplis, Alteris). Pattern: (Michaelangelo+Blaster G1)
* Gwiazdodeszcz Verlen: AoE, masowy deszcz destrukcji, kochający efektowne rzeczy. (Exemplis, Praecis). Pattern: (Archer FateStayNight + dramatics)
* Techniczuś Verlen: kocha high-tech, konstruktor, precyzyjny miłośnik technologii. (Exemplis, Praecis). Pattern: (Donatello + lubi mieć wrażenie u dziewczyn)

### Scena Zero - impl

Wejście do tej sesji - co już wiemy:

* Jako, że Elena pokazała słabość w oczach jastrzębi Verlenlandu, Arianna ma presję społeczną by pokazać że jest GODNA.
* Do Arianny i Vioriki dotarł zwiadowca (Palaos) z Dekadii Invictus
* Jastrzębiusz Diakon jest super-jastrzębiem; Mściząb i Karasiożer próbują rozwiązać problem potwora na terenie Rinosates
* Jastrzębiusz wierzy, że Arianna i Mściząb to dwójka kochanków; nie patrzył na ten teren. Sentisieć nie WIDZI niektórych rzeczy.
* Mściząb zachowuje się jak nie on: triggery, wzmocnienie?, odciął Karasiożera z sentisieci. A na terenie jest potwór działający jak strateg.
* Mściząb buduje coraz silniejszą frakcję Kłów Zemsty, silnie oddanych mu agentów.
* Ludzie znikają i pojawiają się siły i oddziały Potwora. A Jastrzębiusz niczego nie widział.
* Arianna i Viorika mają plan (jeszcze nie zrealizowany)
    * CZEMU: czy to nie pułapka? czy Mściząb jest sobą?
    * Viorika przeładowuje Mścizęba sygnałami (by go przeciążyć i by wyłączył triggery na sentisieci)
    * Arianna kontaktuje się z Decadią Invictus
* Mściząb będzie ignorował większość triggerów które da się sensownie ukryć i zamaskować. Jego zdaniem większość to fałszywe pozytywy. No i ta cholerna impreza Verlenów - RÓŻNI SZUKAJĄ GŁUPICH RZECZY.
* Tiberius jest przekonany, że Mściząb wystawia Dekadię Invictus na eksterminację i próbuje budować swoje siły
* Tiberius znalazł (po radach Arianny i na co patrzeć) serię zarażonych agentów. Z 80 Decadia Invictus zarażonych jest 11. Są spenetrowani.
* Zgodnie z pomysłami Arianny i Vioriki, Jastrzębiusz nie wykazał się kompetencją tiena rodu.
* Karasiożer gardzi Eleną i nie chce być tak bezużyteczny jak ona; jest młodym zaciekłym idealistą.

### Sesja Właściwa - impl

Dekadia Invictus nie ma pojęcia o agentach, którzy zniknęli. Z ich perspektywy ci ludzie zniknęli. O Palaosie też nie wiedzą; dopóki Arianna nie powiedziała Tiberiusowi. 

Arianna i Viorika szuka informacji o życiu, co robił, o związkach - kim jest Karasiożer, jakie miał sukcesy, jak z przyjaciółmi, czy stawiał na swoim.

* Karasiożer jest żądny umiejętności i chwały, ale umiejętności > chwały. Chce być dobry, a potem znany. Młodzieńczy idealizm.
* Karasiożer jest porywczy. Chce pokazać. Chce... pomóc. On się aż rwie. Taki młody chłopak, który pragnie być przydatny. I ma taki romantyczny zapał do dziewczyny.
    * romantyczny, młody idealista
* Karasiożer na _liniach komunikacyjnych sentisieci Verlenów_ wielokrotnie demonstrował jawnie, że Elena jest głupia jak but, bo jest SŁABA oraz że DAŁA SIĘ WYROLOWAĆ.
    * jest osobą, która jak coś uważa, ZARAZ TO POWIE.
    * boi się, by nie być tak bezużytecznym jak Elena. By odciąć się od smrodu porażki.

Dekadia Invictus dostarczyła informacje odnośnie ruchów Potwora i Mścizęba. I Viorika z Arianną oraz Tiberiusem próbują dojść do tego jak wygląda CEL Przeciwnika. Czemu on to robi? Te ruchy - co one pokazują? Czy pomagają Mścizębowi? I na bazie ruchów Mścizęba co on może myśleć.

Tr Z +3:

* X: wyraźnie ruchy Mścizęba uwzględniają aspekt epickości. Gorszy strateg uzna, że tam jest geniusz. Np. Karasiożer uzna, że tam jest geniusz.
    * -> z perspektywy Karasiożera Mściząb jest genialny
* X: ruchy przeciwników są tego typu, że nie da się nic Mściząbowi udowodnić. Ma prawo robić takie ruchy. To nie jest niekompetencja. (to nadal wskazanie na wzmocnienie)
* DARMO:
    * przeciwnik pojawił się przed Mścizębem. Mściząb nie mógł być inicjatorem. Ale jeśli ktoś jest za Mścizębem, to tamta osoba mogła być inicjatorem
    * potwór ciemięży Mścizęba. Robi dobre ruchy. Mściząb gdyby chciał zrobić to dobrze, wykazać się - zrobiłby inaczej. Mściząb był UPOKORZONY przez potwora.
        * Mściząb nawet stracił nad sobą panowanie
    * przeciwnik wygląda na to, że próbuje pozyskać zasoby. Poluje na ludzi. I atakuje też pod ziemią. CHYBA może przesuwać ziemię.
    * nic nie wskazuje na to, że potwór się ogranicza
* V: Arianna i Viorika miały do czynienia z Mścizębem. Viorika zna dobrze taktykę Verlenów. A Tiberius uczestniczył w wielu walkach i ma inne podejście, nieskażone magią.
    * potwór wyraźnie próbuje maksymalizować presję emocjonalną na Mścizęba. Ruchy potwora mają złamać Mścizęba.
    * potwór redukuje profil - nie chce, by Jastrzębiusz się pojawił.
    * Arianna jest maksymalizacją presji - Mściząb MUSI zejść do potwora. Ale to pokazuje 'demoniczne okrucieństwo' potwora, to jest coś innego.
    * Mściząb robi naprawdę dobre ruchy. One mają sens. Ale wyraźnie nie martwi go los ludzi, którzy opuszczają ten teren. To nie są "jego ludzie".
    * potwór zagraża Mścizębowi ale NIE PRÓBUJE GO ZRANIĆ. (subtelne)

Dedukcja: najpewniej mają do czynienia z Alteris-Interis. Ale to by znaczyło, że gnębienie Mścizęba jest strategią PRAGMATYCZNĄ. Skąd jednak wiedział o Ariannie i Mścizębie? Nie ma tu Esuriit... nic nie jest zrozumiałe. Nadal coś się nie zgadza, ale CHYBA jesteśmy dalej. Czyli przeciwnik MUSI zrobić ruch, przy imprezie i braku jedzenia.

A Palaos - który uciekł do Arianny i wezwał, czy jest wzmocniony? Czy jest zmieniony? Co mu się stało? W JAKI SPOSÓB? Jaka energia mogła zostawić takie ślady. Dane ze szpitala.

* Palaos był dotknięty przez Interis.
* Interis nie było precyzyjne. To nie był ruch wybitnego maga, to jest "z siekierą do układu nerwowego"...

Arianna szuka obiecującego lekarza i JEGO pyta. +2Vg.

* V: Palaos był wzmocniony. Przez Esuriit. To bardzo subtelne. Bez tego mógłby nie dotrzeć. Obsesyjna próba ratowania swoich ludzi.
    * czyli najpewniej mamy DWA byty. Alteris-Interis oraz Interis-Esuriit. Nie jeden byt. 

Nemesis przechwycił Palaosa by wykorzystać a byt który podjudza doładował go. Czyli Nemesis jest potrzebna by mieć wiedzę... ale może Esuriit mieć kontrolę nad terenem. Podpięte do sentisieci? Najpewniej są tam DWA byty.

Ale komu Mściząb podpadł na odcisk? Dojdziemy do przypadkowych ofiar jego poczynań? Czy wysłał komuś żonę na linię frontu i ona nie wróciła? Co tu się stało - NIKT nie zrobiłby czegoś takiego przecież...

Czy mamy kontakt do Emmanuelle? Czy ona będzie na biężąco? Na PEWNO będzie na bieżąco i jest daleko od Mścizęba. Emmanuelle jest KATEGORIĄ KONFLIKTU badającego Mścizęba.

* Emmanuelle: tien Verlen, słucham, jestem do dyspozycji! (odpowiednio przestraszona) Ja nic nie zrobiłam!
* Arianna: w tym momencie nie chodzi o Ciebie. Sprawa nie cierpiąca zwłoki. Życie Mścizęba jest zagrożone, ktoś mu zagraża.
* Emmanuelle: ktoś chce zrobić krzywdę Mścizębowi? Ale dlaczego?
* Arianna: mogło się zdarzyć, że PRZYPADKOWO ktoś został skrzywdzony, ktoś bliski tej osobie która się chce zemścić na Mścizębie. Nie wszyscy potrafią zrozumieć że potrzebne są poświęcenia. Ktoś chce go upokorzyć a potem brutalnie zabić.
* Emmanuelle: zrobię wszystko, by pomóc. Mściząb... nie może stać mu się krzywda. Jest mi pisany.

Emmanuelle ma ten sam aspekt problemu co "kochankowie Eleny" - Elena tak jak Mściząb przyzywają do siebie osoby płci przeciwnej i to nie normalne osoby. Emmanuelle nie jest normalna. Więc jest stalkerką Mścizęba. INTENCYJNIE: szukamy osoby, która może za tym stać. Po członkach oddziału którzy zginęli i mieli bliskich. Cross-reference z ruchami Mścizęba i akcjami Mścizęba. Rozkazami Mścizęba i jego zaniechaniami.

Emmanuelle: "Czy mogę mieć dostęp do sentisieci? Czy mogę mieć dostęp do czegoś _więcej_?". Arianna daje dostęp Emmanuelle do jakiegoś 15-latka Verlenów. Niech ten się przyda. A ona dzięki temu może powiedzieć więcej.

Tr +3:

* X: Emmanuelle wykorzystuje zainteresowanie Arianny, by móc zyskać zasoby, możliwości, by nie być traktowaną jako "ta czarodziejka". Jest ważniejsza niż to. ALE nie działa wbrew Verlenom.
* X: Emmanuelle ma ambicje stania się Verlenką, albo Verlen-adjacent. Chce być TIENKĄ a nie czarodziejką. Chce jako tienka być równa Mścizębowi. (w sentisieci Verlenów są triggery na Esuriit +1Vg)
* Vg: Emmanuelle ma coś:
    * Mściząb ma przegrywający ciąg. Nie jest dość dobrze. Arianna jest "lepsza". On jest porównywany z Eleną.
    * Mściząb miał operację w okolicach jednego miasteczka. Ona wyglądała tak, że on nie wygrywał. Ale wszedł, bardzo ryzykowna operacja i wyszedł i wygrał. 
        * Rozdzielił się z zespołem. Też operacja była podziemna.
    * Od czasu tej operacji jest coraz lepszy, rzuca się na coraz trudniejsze akcje. Jest korelacja.
    * Tiana dalej z nim jest. Tam też była. Ona też się radykalizuje, była jego kultystką
            * Tiana na początku jest w formie "nie rób tak, nie warto tak ryzykować"
            * Od pewnego momentu, koło miesiąca po jego operacji, Tiana staje za nim. ON ma rację.
            * On nie ma NIKOGO koło siebie który mówi "nie rób tak"

Chyba Mściząb spotkał Mroczną Muzę. Najdoskonalszą dziewczynę. Taką, która chce by mu się udało. Nieważne jakim kosztem. To faktycznie wygląda jak pułapka na Ariannę - celem jest upokorzenie Arianny. Czyli wszystko wskazuje na to, że Muza wykorzystuje Esuriit. Nemesis - nie.

* V: Emmanuelle jest PRZEKONANA z całego serca że Arianna próbuje uratować Mścizęba. Emmanuelle wspiera Ariannę. Nie będzie obracać się przeciw niej. Pomóc najlepiej Mścizębowi może tak, że faktycznie wspiera Ariannę. NIE Emmanuelle nie zna jeszcze plotek o Arianna x Mściząb. Emmanuelle jest spuszczona z łańcucha i może eksplorować na własną rękę, ma drobne wsparcie i ma pomóc Ariannie pomóc Mścizębowi.

Chronologia:

* WPIERW mamy akcję gdzie Arianna pomogła Mścizębowi i Mściząb zdobył przyjaciela. (syrenopajęczak)
* POTEM Mściząb ma wpierdol. Aż w końcu napotyka na Muzę.
* POTEM (od czasów Muzy) Mściząb buduje kult, poszerza, wychodzi mu. Tiana staje się kultystką.
* TERAZ pojawia się przeciwnik godny Mścizęba. Lepszy. I Muza tu jest.
* NAJPEWNIEJ Muza aktywnie ściągnęła Ariannę.

Muza najpewniej jest magiem rodu Sylver...

* Mściząb buduje kult. Kult który zrobi to, czego Mściząb chce.
* Chce ściągnąć Mścizęba i ludzi. Kradnie ludzi i Mściząb "kradnie ludzi".
    * Potwór nie używa ludzi chyba do niczego...
    * ...ale kamienne istoty które napotykamy mają 'tkankę', więc POTENCJALNIE ludzie mogą być substratem. 1 człowiek -> kilka kamienniaków?

No nieźle...

Arianna skupia się znowu na Emmanuelle - czy przybył z nim ktoś dodatkowy? Czy przybył z Muzą? Kim potencjalnie jest Muza? Czy ktoś widział go z Muzą - czy Muza istnieje? Czy Mściząb coś rozmawiał i się mogła wymsknąć? Mamy nagrania z sentisieci bo Jastrzębiusz autoryzował "kłótnię kochanków".

Hr (ona jest superdaleko, poograniczana itp)

* P_2: stalkerka + kontakt do piętnastolatka Verlenów (cała baza) -> Ex.
* P_1: nagrania z sentisieci. -> +P
* P_1: dekadia invictus robiła research i o Karasiożerze i o Mścizębie. -> +P
* +4 bo przedni pomysł

-> Tr+4:

* V: 
    * Muza: Arianna dostaje potwierdzenie od Emmanuelle o istnieniu tajemniczej kobiety. Ona nigdy nie jest "widoczna", ale Mściząb o niej mówił. I ma od niej dary pomagające mu w walce z Arianną. Jest bardzo silnie skupiony, by być "lepszy niż Arianna". 
    * Nemesis: Arianna dowiaduje się, że istnieje potwór. Bardzo niebezpieczny. I Mściząb wraz z Muzą polują na niego. Robią wszystko by go unieczynnić i usunąć. To nie jest pierwszy raz gdy Mściząb z CZYMŚ walczy tego typu. I Nemesis próbuje zniszczyć psychicznie Mścizęba.
* X: 
    * Emmanuelle jest "lojalna i pomocna" Verlenlandowi (Mścizębowi i Ariannie) w oczach dzieciaków którzy jej pomagają
* Vr:
    * Emmanuelle dotarła do wiadomości, że Mściząb ma na sobie potwora za którym stoi Arianna, tak, jak pewien Blakenbauer został przeklęty. I ten potwór to działania Arianny by go na zawsze zniszczyć i ośmieszyć. Bo Arianna nie pozwoli, by ktoś mógł ją przyćmić. Nigdy. 
    * A Muza ma coś do Arianny - to personalna sprawa.
    * Mściząb i Nemesis walczą dłużej - to nie jest raz.
    * Ten ptaszek to jest to w co wierzy Mściząb. NIGDY nie zaakceptuje wsparcia Arianny. On MUSI udowodnić, że to jej dzieło.

Viorika zbiera dowody na to, że tu się dzieje coś więcej niż "kłótnia kochanków". Te dowody mogą posłużyć do tego, żeby Jastrzębiusz wziął to na poważnie i wsparł wysiłki Vioriki i Arianny. Odblokowanie Jastrzębiusza - jeśli same to super, ale jak nie - to gorzej. A przy okazji zbudować obraz sytuacji.

baza: Tr (dużo się dzieje) +P (różnorodność - D.I., sentisieć jako źródło informacji, uprawnienia od Jastrzębiusza). -> 

Tr Z +3.

* X: Jastrzębiusz po otrzymaniu dowodów potraktuje to śmiertelnie poważnie. Zajmie się tym osobiście. Dziewczynki na bok. Spieprzył i nie spieprzy bardziej.
* V: Mamy dowody i jakoś "co tu się dzieje"
    * Mamy dowody, że skalniaczki są na substracie ludzi
    * Mamy dowody, że Mściząb to wie, ale nic z tym nie robi
    * Mamy dowody, że ludzie znikają i Decadia Invictus robi co może - ale może niewiele
* X: Zbieranie dowodów zajmuje DUŻO czasu. Tak samo jak Arianna jest bardzo zajęta tak samo Viorika ciężko pracuje. Mamy dowody, ale to trwa, by były odpowiedniej klasy
    * Viorika miała je wcześniej, ale nie ufała, triple checks itp. Po prostu w tak trudnej sytuacji niech Jastrzębiusz ich nie może podważyc.
* DARMO: Karasiożer został "przekazany" przez Jastrzębiusza. Karasiożer był "praktykantem" z którym nie było co robić - niech się uczy od Mścizęba. To jest PRAWDA. To nie jest tak, że on jest podmieniony itp. To jest tien Karasiożer Verlen. Jego agendą jest się uczyć od lepszych. Nie robił żadnych nietypowych akcji... Karasiożer nie wykonał żadnych ruchów wskazujących na to, że jest agentem czymkolwiek. To jest poczciwy Verlen.

Viorika chce porozmawiać z Karasiożerem. Do tego musisz się połączyć hipernetem. Adres jest oczywisty.

* Viorika: "Cześć kuzynie, Viorika z tej strony"
* Karasiożer: "Tien Viorika Verlen, miło mi, Karasiożer z tej strony"
* Viorika: "Tak, wiem"
* Karasiożer: "(cisza)"
* Viorika: "Jest jedna rzecz w której możesz mi pomóc jakbyś chciał"
* Karasiożer: "Oczywiście, tien Vioriko, z przyjemnością pomogę, w czym mogę?"
* Viorika: "Próbuję zrozumieć co tu się dzieje... możemy się spotkać?"
* Karasiożer: "Ojej, ale nie wiem jak daleko jesteś. Jesteś w domu?"
* Viorika: "Martwi mnie, że na terenie Rinosates giną ludzie i chciałam się dowiedzieć co się dzieje. Jesteś w pobliżu, masz rękę na pulsie..."
* Karasiożer: "Ale tien Vioriko, skąd wiesz co się tu dzieje? To mogą być zakłócenia."
* Viorika: "To trochę nie po verleńsku (wyrzut)"
* Karasiożer: "Ale CO nie po verleńsku wszystko po verleńsku!"
* Viorika: "Że ludzie giną?"
* Karasiożer: "Ale nie wiemy czy giną, Vioriko! Masz nieprawdziwe informacje!"
* Viorika: "To mi je skoryguj."
* Karasiożer: "Muszę zebrać fakty! Nie mogę z głowy."
* Viorika: "Nie rozpędzaj się tak... zacznijmy z innego punktu. Przyszedłeś się uczyć. Czego się nauczyłeś odkąd tu jestem?"
* Karasiożer: "(z dumą) Rozlokowywania oddziałów, szukania potwora, skanowania podziemi, przekierowanie sentisieci, wspomagań bojowych (...wymienia dalej...)"
* Viorika: "Umiejętności czysto techniczne, a czy wiesz czemu to robiłeś?"
* Karasiożer: "Oczywiście, polujemy na potwora (emocja: jak ona może tego nie rozumieć). Polujemy na potwora więc polujemy na potwora. A polujemy, bo jesteśmy Verlenami. Polujemy na potwory. MŚCIZĄB JEST TAKI SUPER!"

Viorika idzie w "miałeś zajęcia z X, jak masz popatrzeć na plan skoro go nie znasz". Idzie też - popatrz na agendę tien Jastrzębiusza "co tu się dzieje", że może być niezadowolony. 

Tr Z (bo powiedziała, że przyszli ludzie prosić o pomoc) +3:

* X: Karasiożer ufa Mścizębowi z uwagi na to co dostał i współpracę. Karasiożer naprawdę dużo dostał - wiedzy, autonomii itp.
* X: Lojalności wobec Mścizęba nie ruszymy. Karasiożer wierzy, że Mściząb ma najlepszy możliwy plan i agendę.
* V: Świat według Karasiożera, czyli co on widzi:
    * Tu jest potwór, groźniejszy i bardziej nietypowy niż inne. Jastrzębiusz akceptuje to, by go tu zniszczyć. Ale z pewnych przyczyn (Blakenbauerowie), udaje że nic nie wie.
        * To jest potwór BLAKENBAUERÓW i on się wyrwał spod kontroli i poszło nie tak itp. I Jastrzębiusz próbuje to zrobić.
    * Potwór jest bardzo groźny. Trzeba na niego uważać. Dlatego wymaga specjalnych środków. Ale nie wiadomo, czy ludzie umierają. Potencjalnie da się ich ratować. Dlatego Jastrzębiusz w to poszedł.
    * Sprawa jest super dyskretna. Dlatego są ograniczenia sentisieci i plausible deniability.
    * Mściząb nie chciał mu tego powiedzieć - Karasiożer zeń to wyciągnął.
    * Są frakcje wewnątrz Verlenów, którzy mogą chcieć CHRONIĆ potwora czy coś - kwestia polityczna.
* V: Mściząb jest przekonany, że Karasiożer był przepytany przez Viorikę ale nie powiedział nic ważnego. Więc - może działać spokojnie.

Arianna ma Jastrzębiusza. I chce go dobrze wykorzystać.

* Arianna: "...dokopałyśmy się do trudnej sytuacji, zostawiamy lordowi decyzję co należy zrobić. I więcej ofiar niż to konieczne... (i dowody)"
* Jastrzębiusz: "(SZOK, aż widać) Tien Arianno, dziękuję Ci za tą informację. Możecie... jesteście zaproszone na ucztę do mojego zamku, ja... ja będę chwilę zajęty."
* Arianna: "rozumiem że chce pan rozwiązać to sam, ale Mściząb zabudował siły i zobaczy, że się poruszyłeś w jego kierunku - ryzykowne akcje. Dodatkowi agenci?"
* Jastrzębiusz: "Doceniam propozycję. Będziecie w zamku, będziecie w odwodach."
* Arianna: "Rozwiąże pan sytuację z Mścizębem i potworem, jest tu nieznany mag - Muza, który steruje Mścizębem. Może próbowac się wymknąć gdy sytuacja zrobi się gorąca. Lepiej jak monitorujemy. Czymkolwiek jest ten potwór, wielkie szkody. Okoliczne tereny. Dotyczy to również mnie jako księżniczki Verlenlandu."
* Jastrzębiusz: "Wielkie szkody... księżniczka... (myśli)"
* Arianna: "Oficjalnie nigdy nas tu nie było."
* Jastrzębiusz: "(uśmiech) Tego nie uda się ukryć przy takiej ilości śmierci, ale widzę..."
* Arianna: "W takim razie nasz udział już niewiele zmienia"

Tr Z (zależy mu na reputacji, ale na ludziach bardziej, mimo wszystko - zainwestował w noktian, dał im szansę; nie chce, by tak to się skończyło) +3:

* V: Nie wyłączy Was z akcji
    * Jastrzębiusz: "Ja naprawię ten teren i zajmę się Mścizębem, Karasiożerem itp. Ja zajmę się Verlenami. Wy zajmijcie się tajemniczym magiem - odblokuję Wam poszerzenie sentisieci i będę w odwodach."
    * Arianna: "Mściząb musi dostać nauczkę, ale musi być linia rehabilitacji. On ma lojalnych ludzi. Może być związane z jego magią. Możemy mieć kult Mścizęba - niech będzie miał okazję zostać wartościowy. A jest jeszcze młody i ma prawo popełnić błędy - wiele jego akcji było sensownych. Ale desperacko się wykazuje, nie dopuszczał, że musi chronić ludzi bo wykazanie się było tak ważniejsze."
    * Arianna: "Twoja wersja wydarzeń jest prawdziwą wersją, ale chcę chronić moją reputację - pogorszy mi się reputacja jak PRZEZE MNIE będzie pogrzebany Mściząb. To przysługa dla mnie"
* X: Jastrzębiusz powie Mścizębowi, że tylko dzięki Ariannie ma jakąś nadzieję. To sprawia, że Mściząb faktycznie zawdzięcza Ariannie. To sprawia, że Arianna okazała się być "nemesis" dla Mścizęba. Ona za tym stała. Od samego początku. I o to chodziło. Mściząb poszedł za daleko, chcąc udowodnić - i dlatego Arianna go złapała. 
* V: Jastrzębiusz będzie łagodny wobec Mścizęba - będzie tam opcja rehabilitacji. Ma prawo wyjść na prostą.

## Streszczenie

Arianna i Viorika kontynuują śledztwo w sprawie dziwnego zachowania Mścizęba i zniknięć ludzi w Rinosates. Odkrywają, że Mściząb podejmuje działania niezgodne z jego wcześniejszym charakterem, a w jego otoczeniu rośnie kult oddanych mu zwolenników. Ponadto, część członków Decadii Invictus okazuje się być zainfiltrowana przez wrogą siłę związaną z Interis i Esuriit. Uruchamiają Emmanuelle, zwolenniczkę Mścizęba, która dostarcza dodatkowych informacji. Okazuje się, że Mściząb mógł po jednej z porażek spotkać tajemniczą Mroczną Muzę, która go manipuluje i wzmacnia jego moce, prowadząc go do radykalnych działań i konfliktu z Arianną. Tiana, jego dotychczasowa towarzyszka, również ulega wpływowi Muzy i staje się jego oddaną zwolenniczką.

Arianna kontaktuje się z Emmanuelle, stalkerką Mścizęba, która potwierdza istnienie tajemniczej Mrocznej Muzy manipulującej Mścizębem. Muza najprawdopodobniej jest magiem z rodu Sylver i dąży do zniszczenia Arianny poprzez wykorzystanie Mścizęba. Emmanuelle dostarcza informacji, że Mściząb wierzy, iż Arianna stoi za jego nieszczęściami i potworem, z którym walczy. Potwór lub Muza próbują zniszczyć Mścizęba i Arianna jest środkiem. Ale dlaczego?

Viorika próbuje przekonać Karasiożera, młodego idealistę i ucznia Mścizęba, o niebezpieczeństwie, ale jego lojalność wobec mentora uniemożliwia zmianę stanowiska. Arianna przedstawia dowody Jastrzębiuszowi, który postanawia zająć się sprawą osobiście, ale zgadza się, by Arianna i Viorika zajęły się tajemniczym magiem. Arianna podkreśla potrzebę rehabilitacji Mścizęba i chce uniknąć jego całkowitego upadku, jednocześnie chroniąc swoją reputację.

Jastrzębiusz planuje łagodniejsze podejście do Mścizęba, dając mu szansę na odkupienie. Arianna i Viorika mają za zadanie zneutralizować wpływ Muzy i rozwiązać problem potwora. 

## Progresja

* .

## Zasługi

* Arianna Verlen: we współpracy z Tiberiusem Skarnem zidentyfikowała zinfiltrowanych agentów (11 z 80) w szeregach Decadii Invictus. Wykorzystała Emmanuelle by odkryć, że Mściząb jest manipulowany przez Mroczną Muzę, prawdopodobnie maga z rodu Sylver. Przedstawiła dowody Jastrzębiuszowi, przekonując go do działania i wynegocjowała łagodne podejście do Mścizęba, podkreślając potrzebę jego rehabilitacji i ochronę własnej reputacji.
* Viorika Verlen: zebrała dowody na działania Mściząba i obecność Muzy, które przekazała Jastrzębiuszowi. próbowała przekonać Karasiożera o zagrożeniu, choć jego lojalność wobec Mściząba pozostała niezachwiana.
* Mściząb Verlen: nadmiernie wykorzystując sentisieć, odcinając Karasiożera i będąc pod wpływem Muzy. Wierzy, że Arianna stoi za jego nieszczęściami i potworem, z którym walczy; że Arianna ZNOWU chroni jakiegoś Blakenbauera.
* Karasiożer Verlen: pod okiem Mściząba nauczył się wielu umiejętności wojskowych, w tym rozlokowywania oddziałów, skanowania podziemi i manipulacji sentisiecią. Pozostał lojalny wobec Mściząba, mimo prób Vioriki przekonania go o zagrożeniu.
* Jastrzębiusz Verlen: Początkowo nieświadomy powagi sytuacji, po otrzymaniu dowodów od Arianny i Vioriki potraktował sprawę poważnie; zadziała osobiście jako tien Rodu, by zatrzymać Mścizęba przed krzywdzeniem jego ludzi.
* Emmanuelle Gęsiawiec: stalkerka i adoratorka Mścizęba. Pomogła Ariannie, dostarczając informacji o zachowaniu Mścizęba i potencjalnie kiedy ten spotkał się z Mroczną Muzą. Potwierdziła, że Mściząb wierzy, iż Arianna stoi za jego nieszczęściami i potworem. Chce pomóc Ariannie, wierząc, że to pomoże Mścizębowi. Uzyskała dostęp do zasobów Verlenów, by zdobyć więcej informacji.
* Tiberius Skarn: dostarcza cennych informacji na temat sytuacji w Rinosates i oddał Decadię Invictus pod kontrolę Vioriki. Podjął działania mające na celu zabezpieczenie Decadii Invictus przed infiltracją.
* Tiana Grolmik: Początkowo zakochana w Mścizębie i służąca jako jego przynęta. Po pewnym czasie stała się oddaną zwolenniczką Mścizęba, w pełni popierając jego radykalne działania. Pod wpływem Muzy, dodatkowo wzmocniła pozycję Mścizęba i jego izolację od głosów sprzeciwu.
* Lycia Sylver: Mroczna Muza. Spotkała Mścizęba po jednej z jego porażek, wzmacniając jego moce i wpływając na niego w kierunku radykalnych działań. Dąży do zniszczenia Arianny, manipulując Mścizębem i stawiając ich przeciwko sobie.

## Frakcje

* .

## Fakty Lokalizacji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. Rinosates
                                1. Poziom podziemny
                                    1. Kompleks Mieszkalny Alfa: są tam trzy kompleksy, mniej więcej po 2k osób każdy
                                        1. Mieszkania
                                        1. Strefa relaksu
                                        1. Magazyny i schowki
                                        1. Strefa Zarządcza Życiem: mikro-reaktor, centrala, ciepło, wentylacja, recykling wody...
                                    1. Sektor ekonomiczny
                                    1. Sektor medyczny
                                    1. Inżynieria
                                    1. Reaktor główny
                                1. Poziom naziemny
                                    1. System obronny
                                    1. Windy i wentylacje
                                    1. Odnawialna energia
                                    1. Domy mieszkalne: nie wszyscy są noktianami i nie wszyscy chcą tak żyć
                                    1. Strefa Handlowa
                                    1. Strefa Produkcyjna


## Czas

* Opóźnienie: 1
* Dni: 2

## Inne

