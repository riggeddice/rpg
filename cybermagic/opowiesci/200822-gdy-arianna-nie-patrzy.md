## Metadane

* title: "Gdy Arianna nie patrzy"
* threads: legenda-arianny
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [200819 - "Sekrety Orbitera": historia prawdziwa](200819-sekrety-orbitera-historia-prawdziwa)

### Chronologiczna

* [200819 - "Sekrety Orbitera": historia prawdziwa](200819-sekrety-orbitera-historia-prawdziwa)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* ?

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Punkt zero

* .

## Misja właściwa

Następnego dnia, po Wielkim Wystąpieniu Leony, na którym Leona skroiła Tadeusza z Eterni. Ku wielkiej żałości Tadeusza i radości Eleny, Tadeusz będzie musiał zostawić Elenę w spokoju. A do Klaudii przyszedł Martyn.

Martyn zauważył, że Tadeusz użył energii Esuriit. A oni niedawno byli na Krypcie - tam też było Esuriit. Martyn chciałby zdobyć wiedzę, jak eternianie radzą sobie z rozpraszaniem energii Esuriit - znając Ariannę, tego będzie więcej. Martyn wie dokładnie jak jest oficjalnie w Eterni, czyli rozpraszanie energii po ludziach - ale nie wie, jak jest w rzeczywistości. Klaudia zgadza się z jego rozumowaniem. Oficjalnie - co wie i Klaudia i Martyn - chodzi o rozproszenie energii po wielu osobach.

Martyn powiedział, że jest dziewczyna. Nazywa się Sylwia Milarcz. Sylwia jest jedną ze świty Tadeusza - więc do niej po fali zwrotnej powinno przyjść jakieś uderzenie wtórne. A dzięki badaniom Klaudii i silnemu skanowaniu Martyn i Klaudia wiedzą, jak silnie Skażony Esuriit został Tadeusz przy uruchomieniu Simulacrum. Więc skanując Sylwię mogą dostać więcej informacji.

Gorzej, że Sylwia nie może być oficjalnie przeskanowana przez ludzi poza świtą Tadeusza. Martyn nie ma uprawnień by coś z tym zrobić. Ona nie jest do końca w linii Orbitera. Klaudia zaproponowała, by Sylwia zachorowała - Martyn powiedział, że to rozważał, ale ktoś z Eterni by ją badał.

Sylwia nie jest agentem Orbitera, jest w świcie Tadeusza. To jest pewien problem. Eternia wie, gdzie Sylwia się znajduje - słudzy Tadeusza i szlachty Eterni mają chipy, jak psy. Martyn podejrzewa, że to jest powiązanego z niewolnictwem. Ale Sylwia Martyna lubi ;-). Martyn wywiedział się też czegoś o Tadeuszu - jest to arystokrata Eterni mający korzenie w arystokracji Aurum. Niezbyt znaczący ród Aurum, ale w Eterni jest silniejszy. Emigrował z Aurum do Eterni.

Eternianie nie traktują Tadeusza w 100% jak swojego. Jest "ich", ale trochę na uboczu.

Pierwsze, co Klaudia próbuje się dowiedzieć i zrobić to sprawdzić w medycznych systemach jak mocno faktycznie Skażony jest Tadeusz. Jak bardzo "dostał" - lub jak bardzo nie dostał. Klaudia ma wysokopoziomowe wyobrażenie jak ten mechanizm działa, Martyn polega na tym co jest opisane i są w stanie dowiedzieć się czy ta procedura pomogła. Klaudia porówna swoje (niezbyt legalne) wyniki z wynikami medycznymi "na miejscu". Plus, Klaudia dokładnie wie co zrobiła Arianna. Bo spytała.

Trudna sprawa (TrZ), ale nic niemożliwego dla Klaudii.

* V: Klaudia zauważyła coś ciekawego. Tadeusz jest BARDZIEJ Skażony niż powinien być. Tak jakby naturalne / wyuczone / rytualne mechanizmy zupełnie nie zadziałały. Ta liczba świadczy o tym, że był Esuriit napromieniowany dużo bardziej niż był - a liczby Klaudii rzadko kłamią.
* V: Tadeusz badał się przed samym rytuałem. Klaudii nie zajęło dużo czasu, by znaleźć pierwotne liczby. Okazuje się, że te pierwotne liczby są dużo niższe. Czyli - ALBO dostał dużo większą dawkę Esuriit niż Klaudia policzyła (mało prawdopodobne), albo rytuał rozpraszający nie zadziałał, albo technologia Kirasjerów zrobiła coś bardzo złego albo dostał większą dawkę Esuriit po tym jak Arianna go wyczyściła.

Podczas walki Klaudia widziała, że na jego świtę miał wpływ atak. Czyli Esuriit się w nich rozpraszało. Na bazie tego, co już wykryła jest w stanie określić ile faktycznie jednostek Esuriit powinno trafić do osób obecnych na sali. Jako, że część jego świty też musiała być badana, Klaudia poszła dalej za tymi informacjami. Co my tam mamy:

* V: Na bazie liczb które są w świcie Tadeusza, Klaudia wydedukowała jednoznacznie - rozpraszało się PRAWIE tak jak powinno w modelach. Tak jakby coś blokowało pełne rozproszenie. Czyli zdaniem Klaudii coś dostarcza mu Esuriit. Tu jest jakaś forma sabotażu lub próby zabójstwa. I dla odmiany - to nie my.
* XX: Działania Klaudii zostały zauważone. Zarówno oficjalnie jak i przez AI Asystentów.
* X: Wszyscy myślą, że Klaudia działa z rozkazu Arianny. To Arianna kazała Klaudii sznupać, bo Klaudia przecież sama nie robi takich rzeczy. Arianna dostanie kolejny opierdol.

Klaudia ma tu ciekawe pytanie - w jakiej frakcji jest Sylwia. Martyn powiedział wyraźnie - nie ma pojęcia. Nie miał okazji z nią bardziej porozmawiać. Tak, ona jest nim zainteresowana, ale nigdy nie zostali sam na sam - Martyn ma Reputację. Nie miał okazji. Ale na bazie tego jak z nią szybko rozmawiał, ona raczej lubi Tadeusza. Uważa go za bardziej zacnego z lordów Eterni.

Klaudia przygotowała się do znalezienia jakichś imprez, fajnych okoliczności, czegoś, gdzie Martyn będzie w stanie porozmawiać z Sylwią. I wzmocniła tą imprezę, bo "Arianna stawia". Zrobiła co była w stanie, by tylko pojawiła się tam Sylwia i jakieś elementy świty, plus, żeby Martyn miał szansę urwać Sylwię.

* V: Martyn ma okazję porozmawiać z Sylwią bez świadków.
* X: Arianna pokazuje się, że próbuje żałośnie podkupić - przeprosić, rozwiązać... w taki parweniuszowski sposób, sypiąc kasą.
* X: Martyn będzie miał do czynienia z kilkoma silnymi pachołkami.
* X: Martyn pokazał, dlaczego jest LEKARZEM WOJSKOWYM podczas walki, przez co zrobiły się poważniejsze kłopoty (dla Arianny). Użył magii by wykręcić bioformę ludzką...
* X: Martyn przypadkowo pokazał reputację godną Leony. Biowarp. Napadający go ludzie potrzebują pomocy lekarza i mają DŁUGO koszmary senne...

Martyn myślał, że napastnicy polowali na Sylwię. Zdjęły mu się ograniczniki i - jako mistrz bioformacji - zmienił im strukturę w coś, nad czym pracował niedawno. Pierwszy, instynktowny czar. Jednocześnie, nagrany - Martyn będzie miał reputację klasy "lekarz, jak Leona".

Martyn spróbował wyciągnąć jak najwięcej z Sylwii w tym czasie. Wszystko było po jego stronie - nawet to, że on przecież uratował i obronił ją przed napastnikami.

* X: Sposób, w jaki Martyn ich pokonał magicznie przeraził Sylwię. Raczej się będzie go bardzo bała.
* X: Sylwia jest z tych gadatliwych. "Sława i chwała" Martyna będą eskalować i będą szerokie.
* X: Mimo tego wszystkiego, to Sylwię bardzo kręci. Ona jest z tych, które pragną takiego strasznego faceta. He has a groupie.
* X: "Groupie" będzie mówić, że on jej wszystko kazał zrobić. "On mi kazał, jest taki straszny, co mogłam zrobić?"
* V: Martyn dowiedział się od Sylwi, że Tadeusz jest dość łagodnym lordem Eterni. To ogólnie poczciwy mag. Bardzo ambitny, honorny, dumny. Nie do końca zgadza się z zasadami Eterni w rozumieniu czystego "master - cattle", ale dla mocy akceptuje tą relację. Acz - nie zachowuje się tak wobec podwładnych.
* V: Martyn kazał jej dać się mu zbadać. Sylwia, "przerażona" się zgodziła. Miała nadzieję na inną formę badania - ale Martyn ją zbadał. Martyn wykrył, że Sylwia ma "właściwy" poziom rozproszenia energii. To punkt odniesienia. To pozwoliło Martynowi jednoznacznie dać odpowiedź Klaudii - jedna osoba w świcie ma coś co sprawia, że w Tadeuszu odkłada się energia. Czyli Tadeusz ma wroga. Albo ktoś w świcie jest świadomym wrogiem albo ktoś wykorzystuje członka świty. Co przy podejściu Eterni jest możliwe.
* V: Na bazie tego wszystkiego Martyn dał radę określić jak można pomóc Tadeusza podleczyć. Plus, określił sposób działania tej "trucizny" w świcie - ona wypacza umysł Tadeusza. Przesuwa go szybciej, bardziej w kierunku Esuriit. A dokładniej, eroduje jego moralność. Sprawia, że Tadeusz zaakceptuje zwyczaje i podejście Esuriit szybciej. Czyli ktoś truje Tadeusza przez kogoś w świcie Tadeusza, by ten jak najszybciej upadł.

Klaudia po usłyszeniu tego wszystkiego zainteresowała się samym Tadeuszem. W jaki sposób Tadeusz doszedł do władzy w Eterni. Martyn odpowiedział co Sylwia mu powiedziała i co sam dał radę zebrać z plotek - Tadeusz był silnym magiem Aurum, ale związanie z sentisiecią oraz ogólna moralność jego rodu mu nie odpowiadała. Tadeusz jest troszkę bardziej ambitny i żądny władzy i mocy, też nad innymi. Ale Eternia dla odmiany jest jeszcze bardziej w tą stronę niż Tadeusz - więc mag, któremu nie odpowiadało Aurum znalazł się w Eterni która mu też nie odpowiadała. Ale z drugiej strony.

Tadeusz wszedł w pakt krwi z pomniejszym arystokratą. Jego moc i jego umysł były silniejsze - Tadeusz wiedział, że silniejszy mag prędzej czy później zamieni się miejscem ze słabszym. I w ten sposób przeszedł przez kilka "poziomów" do góry, spychając magów którzy byli nad nim pod siebie jako swoich wasali, odziedziczywszy ich służących, niewolników i wszystko. Władza mu się bardzo podobała, ale niektórych rzeczy po prostu nie chciał robić.

I teraz ktoś sprawia, że Tadeusz upadnie. Ten upadek zajmie jeszcze co najmniej pół roku - ale się stanie. To długoterminowy plan. I nie jest osobą, która chce upadku Tadeusza Sylwia. Martyn ją przebadał i upewnił się, że ona jest czysta - świadomie i nieświadomie.

Co więcej - Klaudię zdziwiło, że na Kontrolerze Pierwszym w laboratoriach i medvatach w których był Tadeusz, że to nie wyszło. Albo ktoś jest przekupiony, albo zwyczajnie tamci magowie i specjaliści olewają ten temat. Lub nie wiedzą zbyt dużo o Esuriit... lub o Eterni.

Klaudia ma Pomysł. Chce, by Tadeusz z tego wyszedł. I był choć trochę wdzięczny Ariannie.

Klaudia nie jest najsubtelniejszą osobą na Kontrolerze. Zdecydowała się uderzyć bezpośrednio do Tadeusza. Niech on zobaczy co ona wykryła i niech da jej zgodę. Bo jak nie - będzie martwy i już.

Klaudia wzięła Martyna. Trzeba iść do Tadeusza. On jest lekarzem, więc przejdzie przez dalsze perymetry niż ona. I tak trzeba przebić się przez "systemy obronne biurokracji", ale dla Klaudii to nic dziwnego ani szczególnie trudnego.

* V: Klaudii udało się z Martynem wejść do skrzydła medycznego
* V: Dostali się do Tadeusza, gdy jest świadomy i aktywny
* V: I nikt o tym nie wie (poza samym Tadeuszem, Klaudią i Martynem). Są logi, ale nikt ich nie szuka.

Klaudia porozmawiała z Tadeuszem. Ku jej wielkiej żałości, Tadeusz nie okazał się być tytanem intelektu. Jest potężnym wojownikiem, szlachcicem czystej krwi, ale procent składany go przerasta. Tadeusz wierzy święcie swojemu lekarzowi w świcie. Klaudia pokazała mu, że projekcja jest taka, że za niecały rok upadnie przez Esuriit; Tadeusz nie ma jednak potencjału intelektualnego by zrozumieć dowód... Klaudia po raz pierwszy naprawdę nie wie co robić.

Klaudia powiedziała, że Tadeusz nie musi jej wierzyć. Ale najpewniej lekarz Tadeusza chce go skrzywdzić ALBO ktoś go wykorzystuje. Klaudia zaproponowała konsylium innych lekarzy. A na Kontrolerze tego jest sporo.

* V: Lekarze (dyskretnie sprowadzeni przez Klaudię) potwierdzili daty i to, że lekarz Tadeusza wyraźnie nie mówi mu wszystkiego. Klaudia znalazła failure mode - Tadeusz był leczony i badany tylko przez lekarza w swojej świcie. To nie Kontroler Pierwszy zawiódł - to Tadeusz dał takie a nie inne rozkazy.
* X: Przyszli smutni panowie z ochrony i zwinęli Martyna do kicia. Martyn przeprosił Klaudię i poszedł; na szczęście by nie było failure mode, inni lekarze pomagają Leonie. Martyn nie musi cały czas przy niej być...

Klaudia powiedziała Tadeuszowi, że Martyn poświęcił się by go ratować.

Tadeusz uwierzył Klaudii. A dokładniej - jak wszyscy mu mówią że coś jest nie tak, to coś jest nie tak. Podziękował Klaudii bardzo. Powiedział, że zajmie się tą sprawą - to już teraz kwestia eterniańskiego podejścia itp. On musi to rozwiązać. Klaudia powiedziała, że jakkolwiek docenia podziękowania, ale chciałaby zrozumieć jak użyć innych do rozproszenia brzemia Esuriit. Tadeusz dał Klaudii odpowiednie holokryształy - niech sama zobaczy jak to działa.

Po przejrzeniu przez Klaudię - ciężkie do replikacji, jakkolwiek rytuał nie byłby dla niej szczególnie trudny. Nieakceptowalny ORAZ ryzykowny. Ale - przynajmniej wie więcej. Martyn może i jest w kiciu, ale to nic czego Arianna dla niego nie rozwiąże :D.

## Streszczenie

Klaudia i Martyn mieli dość tego, że nie wiedzą nic o tematach związanych z Esuriit a ostatnio mają z tym ciągle do czynienia. Podczas dojścia do tego jaką dawkę dostał Tadeusz gdy stworzył Simulacrum przypadkowo odkryli konspirację - lekarz Tadeusza próbował go (długoterminowo) zniszczyć. Po drodze reputacja Arianny ucierpiała a Martyn trafił do aresztu za okrutne użycie magii wobec thugów. Tadeusz jest jednak bardzo wdzięczny Ariannie która o niczym nie wie :-).

## Progresja

* Arianna Verlen: KOLEJNY opiernicz od admiralicji. Tym razem za imprezę i zachowanie Martyna.
* Arianna Verlen: dzięki manipulacjom Klaudii wrobiona w to, że żałośnie próbuje podkupić się u arystokracji Eterni i przeprosić za to z Eleną. Nic o tym nie wie.
* Arianna Verlen: Tadeusz Ursus dużo jej zawdzięcza dzięki Klaudii - wierzy, że Arianna pilnowała by on nie zginął.
* Martyn Hiwasser: po odruchowej obronie na Kontrolerze Pierwszym gdzie wypaczył 3 thugów dostał opinię okrutnego biomaga Inferni, godnego Leony.
* Martyn Hiwasser: ma psychofankę - Sylwię Milarcz - ze świty Tadeusza Ursusa. Sylwia się go boi i to ją strasznie kręci.

### Frakcji

* .

## Zasługi

* Klaudia Stryk: dotarła do intrygi podwładnego Tadeusza który chciał go Upaść przez Esuriit. Na kilkanaście sposobów wkręciła Ariannę, że to jej wina XD.
* Martyn Hiwasser: zainteresowany Esuriit by chronić Infernię, wpakował się w intrygę Eterni. Użył mocy i skręcił trzech napastników bioformicznie. Skończył w brygu.
* Sylwia Milarcz: ludzki członek świty Tadeusza Ursusa; przerażona mocą i bezwzględnością Martyna (XD). Zostaje jego psychofanką. Połączona mocą z Tadeuszem.
* Tadeusz Ursus: arystokrata kiedyś Aurum, teraz Eterni. Niezbyt bystry acz potężny; nie rozumie co Klaudia próbuje mu pokazać odnośnie jego Skażenia. Jego własny lekarz zatruwał go Esuriit, by Tadeusz upadł.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy

## Czas

* Opóźnienie: 1
* Dni: 2
