## Metadane

* title: "Operacja: spotkać się z Dmitrim"
* threads: pronoktianska-mafia-kajrata, furia-mataris-agentka-mafii
* motives: teren-morderczy, echa-inwazji-noctis, manhunt, infiltracja-dyskretna
* gm: żółw
* players: kić, fox

## Kontynuacja
### Kampanijna

* [230906 - Operacja: mag dla Symlotosu](230906-operacja-mag-dla-symlotosu)

### Chronologiczna

* [230906 - Operacja: mag dla Symlotosu](230906-operacja-mag-dla-symlotosu)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ?
* Struktura: 
    * ?
* CEL: 
    * MG
    * Drużynowy
* THEME
    * 


### Co się stało i co wiemy

* Dane
    * Kajrat wie, kto stoi za porwaniem Furii - Grzymość
    * Kajrat chce pozyskać maga, w końcu, by odzyskać Aynę
    * KONKLUZJA: porywamy maga

### Co się stanie (what will happen)

* F1: _Przetrwanie w Lesie i znalezienie Dmitriego_
    * stakes:
        * uda się zachować ciszę, nikt nie zginie, nikt nie zna Furii
        * Dmitri nie będzie miał kłopotów
    * opponent: 
        * Arnulf, zwiadowcy, psy 
        * patrol terminusów
        * Las Natywny
    * problem:
        * Pułapka Arnulfa ("bez nazwisk, chodźcie TAM")
        * Las jest groźny
            * wildlife predators (bear) and aggressive flora
            * getting lost
            * unstable treacherous terrain, falling branches
            * POST-WAR anomalies, złom post-noktiański i post-astoriański
            * auto-działka roboty patrolowe, "Żukowce"
        * Psy Arnulfa polują (tor: 3 (TRACK) - 7 (GOT EM))
            * advanced sensors (Lancer)
            * czterech żołnierzy i Arnulf + 3 robotyczne psy; obława (znaleźli paczkę kontaktu z Dmitrim)
        * Terminuskie patrole
            * pułapki terminuskie
        * Trzeba dostać się do zakopanego komunikatora z Dmitrim
            * Dmitri ich szuka w swojej Narveen; na tym terenie jest dekontaminacja
    * koniec:
        * znalezienie Furii LUB znalezienie Dmitriego (ze śladem, bez śladu)
* F2: _Dotarcie do Zaczęstwa, do Przytułku Cichych Gwiazd_
    * stakes:
        * Dmitri wpadnie w kłopoty
        * Furie ściągną kłopoty na noktian
        * Grzymościowcy dorwą Furie
    * scenery:
        * klatki (nie z noktianami i na dziewczyny)
        * złomiarska chata w lesie
    * opponent: 
        * Grzymościowcy, reprezentowani przez Ksawerego Kleszcza (człowiek, chce 'noktiańskie laseczki') ("SKĄD MASZ LEKI! PRACUJESZ SAM?")
    * problem:
        * Grzymościowcy skrzywdzą Dmitra i jego żonę, Petrę
        * dotrzeć do Podwiertu
        * pociąg i Podwiert
            * potwory na trasie Podwiert - Zaczęstwo; potrzebny jest pociąg
            * "jesteś malutką noktianką; zrobisz czego chcę i nikt się nie dowie" w pociągu

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Xavera Sirtas
    * OCEAN: (C+A-): żywy ogień, jej dusza jest pełna pasji, a jej słowa ostrzem. "Kto siedzi cicho, tego spotyka wszystko co najgorsze"
    * VALS: (Security, Tradition): wszystko odnosi do tego co już widziała i co było, działa dość tradycyjnie. "Wszystko już było i się stało"
    * Core Wound - Lie: "Przeze to, że dużo mówiłam i szperałam, moi rodzice się rozwiedli" - "Będę walczyć i ognistą walką udowodnię, że warto być razem!"
    * Styl: Zawsze gotowa do walki. Agresywna i asertywna. Dobrze wybiera walki. Jej upór często wpędza ją w kłopoty.
* Dmitri Karpov: astoriański sojusznik Kajrata chcący zniszczenia Grzymościa
    * OCEAN (O- C+): very hands-on and methodical, gruff and silent. "Keep your friends close, but your enemies closer."
    * VALS: (Tradition, Power): strength and control became paramount after his loss. While vengeance burns in his heart, he's not careless. "Strength allows you to shape the world."
    * Core Wound - Lie: "straciłem to co kochałem najbardziej przez mafię" - "jeśli ich zniszczę, znajdę lepsze życie"
    * Styl: relentless and gritty, willing to work with anyone; sometimes too trusting of those who promise aid against Grzymość
    * Loss: Wolny Uśmiech framed Dmitri's family in a major scandal, causing them to lose their standing in society
    * Work: Zwiadowca i Identyfikator zagrożeń; ma dobry pojazd (Narveen) i porusza się dobrze w trudnych warunkach

### Scena Zero - impl

.

### Sesja Właściwa - impl

Las. Udało Wam się odbiec od terenu niebezpiecznego. I faktycznie, przeleciał patrol - rutynowy - i nic nie znalazł. Kajrat dał lokalizacje "zakopanych i ukrytych".

Podróżujecie po "bezpieczniejszej" stronie, zauważyłyście czemu jest bezpieczniejsze - zauważyłyście w oddali coś na kształt dużego mechanicznego żuka (średnica 1m). To cholerstwo jest uzbrojone i to cholerstwo czyści teren ze Skażenia. Trzeba znaleźć komunikator.

ORIENTACJA:

Tp Z +3:

* Vr: udało się wytyczyć trasę. Wiecie gdzie jesteście i jak dojść do komunikatora
* (-Z) V: dotarcie do komunikatora

Udało się Furiom bezpiecznie ominąć jakieś niedźwiedziopodobne stworzenia, trochę poruszając się koło drzew, trochę przechodząc gałęziami i omijając jakiś wrak. Dotarłyście do zakopanego komunikatora. Połączenie.

* Amanda: (zaczyna protokołem gadania) Komunikator 87, co mamy robić
* GŁOS: Żadnych imion, żadnych lokalizacji. Rozumiesz?
* Amanda: oczywiście
* GŁOS: Dobrze. Jesteście po tej stronie?
* Amanda: jesteśmy przy komunikatorze
* GŁOS: (westchnięcie) wiecie, gdzie jest taka zrujnowana polanka z dużym kamieniem, takim jak stół?
* Amanda: wiemy, nie.
* GŁOS: (westchnięcie) nie mogę do Was przyjść, bo ten komunikator jest spalony, rozumiecie.
* Amanda: (ubiera rozmowę w hasło-odzew, tak inteligentnie)
* GŁOS: sorki mała, nie doszło do mnie hasło. Wasi ludzie nie dotarli. Nie powiem Ci dobrego hasła.
* Amanda: poszukamy tej polanki
* GŁOS: nie mogę powiedzieć Wam gdzie jest... (irytacja)... zachowajacie ostrożność.
* Amanda: taki plan

Xavera ma plan - wyszczurzyć z dwóch części złomów rzeczy, które po połączeniu będą ODPOWIEDNIO niebezpieczne i może się tym zająć Żukowiec. W ten sposób pojazd dekontaminujący będzie miał pretekst by tu przyjechać. A to jest wiadomo - na tym terenie większość ludzi nie chce jeździć, Dmitri się nie boi. On bierze roboty których nikt nie chce robić.

Poruszając się po lesie i unikając dron i żukowców Xavera i Amanda szukają... odpowiednio wygodnych złomów. Niedaleko jest rozwalony Hovertank. Częściowo już zarośnięty anomalnym lasem.

Żukowce są jednostkami mało inteligentnymi, to TAI lv 2 (subturingowa). Xavera proponuje by ZAKLINOWAĆ żukowca. To cholerstwo wyśle sygnał S.O.S i Dmitri wie, żeby przyjechać.

Dwie fazy działania:

1. Przygotowanie Złomu by zawalił się na Żukowca
    * wymiar czasu
    * jak mamy rozwalony hovertank (serio rozwalony) - zrzucić na Żukowca by się zaklinował. Mały ładunek wybuchowy.
2. Wabimy Żukowca do złomu
    * coś z tego komunikatora, np. baterie (lubią bycie dziurawione, kopniaki itp) - to powinno skusić Żukowca
    * Xavera będzie biegła z niestabilną baterią, potem wrzuci ją do złomu hovertanka

Przygotowujemy pułapkę. (kompetencje podstawowe, nie pełne, brak dobrego sprzętu)

* Ex Z +3: (nie interesuje nas presja czasowa)
    * sukces: mamy faktycznie pułapkę
    * sukces: wystarczająco dyskretne
    * .
    * Xavera nie ma źródeł energii, ale używa tego co ma by TROSZKĘ ułatwić sobie wpinając się w istniejące aktuatory: +1Vr
    * V: pułapka się udała.
    * X: hidden movement (robo-pies wykrył sytuację)
    * Xz: dowód ingerencji noktian i nie jest dyskretne

Xavera dzielnie przygotowała pułapkę z pomocą Amandy. Czołg nazywał się "Nucleus Ignis". Jest martwy. W środku jest tkanka organiczna. Wykona swoją robotę po raz ostatni. 

Czas na destabilizację baterii i ściąganie Żukowca w pułapkę.

Tr Z +3:

* bateria jest atrakcyjną przynętą
* żukowiec zanęcony i jest gdzie chcecie
* żukowiec nie jest zniszczony
* .
* Vz: Xavera i Amanda zdestabilizowały baterię. Ta bateria w tej chwili 'świeci' energią. Jest niebezpieczna, ale wiecie jak używać
* (dziewczyny mają duży dystans od tego cholerstwa, mają izolujące ciuchy, bateria jest na 'sznurku')
* V: Żukowiec szczurzył, ale Xavera z 'wędką' zanęciła. Żukowiec wtuptał do środka.
* (dziewczyny odpalają ładunki)
* X: nęcenie Żukowca zajęło czas. Manewrowanie, trasa itp.
* V: pułapka zastawiona. Żukowiec unieruchomiony. Uszkodzony. Wysyła sygnał SOS.

Amanda zastawia pułapkę. Jest w trybie paranoi, nie wierzy, że przyjdzie Dmitri. Amanda zastawia pułapkę w tym terenie. Zasadza się w miejscu w którym się może zasadzić.

Tr Z +3:

* X: "ktoś tu jest". Przeciwnik wykrył że ktoś tam jest.
* X: Amanda musiała się schować w "szerszeniokrzewach" - ma wysypkę i jest poparzona, ale funkcjonalna
* V: Amanda znalazła przeciwnika PIERWSZA. To jakaś synteza mechanicznego lamparta i psa, detektor-tracker. 
* (+3Vg Amanda snajperzy) Lampart został zestrzelony.

Amanda jest w bąblach i innych problemach. Oki - musimy uciekać. Robimy WIELKĄ EKSPLOZJĘ używając hovertanka itp. Wysadzamy stash. Wszystko co jest w stashu. I korzystając z osłony eksplozji - w las. Szybko. Ważne, by zgubić. (potrzebna manifestacja)

Tr +2:

* X: szybka ucieczka po lesie, też po drzewach, powoduje, że są pocięte i poranione. Nie jest STATUS, nikt nie spodziewa się że Furie mogą biec w taki sposób. Wyglądają jak 'biedne, poranione dziewczątka'
* X: po drugiej stronie jest jeden Lancer, na szczęście bez mecholampartów; nie ma innego wyjścia, trzeba jednego ominąć.
* V: (p) zgubienie tymczasowe śladu; zakopały się w błocie i marzną. Niestety, Lancer jest niedaleko.
    * Lancer wyraźnie próbuje ZŁAPAĆ, nie zabić.
    * Lancer włączył głośnik
        * "Agenci Noctis. Wojna się skończyła. Nie ma powodu kontynuować wojny. Wyjdźcie i złóżcie broń. Zostaniecie procesowani. Jeśli nie zrobiliście żadnych zbrodni wojennych, zostaniecie potraktowani uczciwie. Tu nic na Was nie czeka a z uwagi na magię i inne formy anomalnego promieniowania stanie się Wam coś strasznego. Część astorian na Was poluje. My nie. Ale nie możemy pozwolić, by uzbrojeni partyzanci działali na naszym terenie. Nie jesteśmy terminusami pustogorskimi. Jesteśmy prywatnym oddziałem paramilitarnym. Proponujemy wam lepsze jutro. Potencjalnie z nami. Ale złóżcie broń i wyjdźcie. Jest dla Was szansa na Astorii. Pomóżcie nam ratować Wasze dzieci."
        * (po chwili) "Przechwyciliśmy Wasz poprzedni komunikat. To ze mną rozmawiałaś. Nikt nie przyjedzie. Po co się męczyć. Tylko więcej cierpienia. Nawet jak nam uciekniecie - przyjdą inni. Z nami macie JAKĄŚ przyszłość."
* X: jesteście w miejscu szkodliwym dla ludzkiego organizmu. Kombinezony są przeżarte. Wysypka, alergia, popromienne, przeziębienie. Przydatna pomoc medyczna. NIe jest niezbędna jeszcze, ale... normalny człoiwiek już nie żyje.

Elektroniczny wibrogwizdek. Coś, co wydaje bardzo drażniące dźwięki. Zwykle stosowany jest na psy. Xavera drżącymi łapkami moduluje sygnał o innej fali. DRAŻNI NIEDŹWIEDZIA DŹWIĘKIEM który ma wskazywać na inną bestię. Niech lancer skupi się na potworze. +2Vg

* V: "to był naturalny dźwięk" - nikt nie wie, że tu są noktianki
* V: potwór się zirytował. Ryknął i zaczął się zbliżać. Lancer nie zaczął walczyć - zaczął omijać. A stwór idzie.

Furie ZGUBIŁY ich. W końcu. I nie do końca wiedzą gdzie są.

15 minut później, Amanda na drzewie zauważyła charakterystyczny kształt łazika. Jedzie W OKOLICE ale nie bezpośrednio do spętanego żukowca. A Furie są w stanie, że pokona je tutejszy teren... Xavera jako wabik, Amanda jako ubezpieczenie na snajperce. Łazik się zatrzymał widząc (zauważalną) Xaverę.

* X: ratuje mnie pan w piękną pogodę
* D: i tak jutro będzie padał deszcz (bardzo Kajratowe hasło)
* D: (obniżył broń) tylko Ty przeżyłaś? Właź. (Amanda wychodzi po znaku Xavery)
* A: (lekko sceptyczna)
* D: właź. Zanim nas coś zobaczy.
* D: naprawię żukowca i jedziemy. (patrzy sceptycznie) apteczka z tyłu. Przebierzcie się tam.

Amanda się przebiera, opisuje tych co na nie polowali

* A: kto to, co to?
* D: nie mafia. Jacyś inni. Koleś z wydziału wewnętrznego /splunął. Wyciąga noktian. On też walczy z mafią.
* A: i?
* D: wyglądam na noktianina? Nie wiem. Ale nie słyszałem niczego złego.
* A: ok.
* D: macie leki dla Petry?
* X: mamy je
* D: naprawię żukowca, u mnie w domu dostaniecie kobiece ciuchy. I coś lepszego medycznie.
* A: co TY myślisz o mafii?
* D: kastracja i gotowanie w gorącej wodzie. KAŻDEGO członka mafii. Nawet tych co są 10 minut. Czemu pytasz?
* A: by zrozumieć co robią i lokalną sytuację, czym zasłużyli sobie na taką niechęć...
* D: mała, część ludzi ich lubi. Ja nie. Oni mieszkają w mieście. Ja mieszkam w lesie.
* X: lubi, za co?
* D: po tym jak weszliście to mafia pomogła w odbudowie. Sprowadzała rzadkie rzeczy. Dawała nadzieję. Dzięki.
* D: miałem Wam pomóc, nie rozmawiać, tak?
* A: rozmowa jest formą pomocy, rozumiemy...
* D: w Zaczęstwie jest przytułek. Nazywa się Ciche Gwiazdy. Czy coś. Tam są też noktianie. Nikt nie pyta. Tam traficie. Nie wyróżniacie się.
* A: możemy działać po swojemu?
* D: tam są ci którzy udają że nie są z Noctis. Tam próbują nowego życia. Będziecie pasować. Ale możemy zrobić inaczej. Acz co, do kopalni się was nie wyśle /sceptycznie
* A: nie wiemy dość by się rozpłynąć
* D: są ludzie w Zaczęstwie, pomogą
* Amanda: przytułek to dobry start, potrzebujemy wejść w pobliże mafii
* Dmitri: nie wiem czego chcesz. Możesz się zatrudnić jako kurtyzana
* A: jest to opcja
* D: też nie lubisz mafii
* A: nieszczególnie
* D: są groźni, ale to tylko ludzie. Są słabsi niż terminusi. Ale terminusi nie dbają o nas. Walczą z potworami a nie z potworami w masce ludzi.

Na wieczór, łazik wrócił do chatki Dmitriego. Dmitri tłumaczy Furiom, że: 

* terminusi są za słabi, nie mogą wszystkiego trzymać
* nie zwalczają mafii tak jak by mogli
* wojna -> Paradygmat -> ogromne problemy na linii anomalii itp

Dmitri wie dużo dużo więcej niż powinien i odpowiada o magii. On mieszka na uboczu. Ciężko się dostać bez łazika. I z zewnątrz wyraźnie wygląda jak złom. Ale od środka - lepiej. Skrzydło medyczne. Wyraźnie Dmitri był bogaty. I on sam ma "fortifarmę". 

Żona, Petra, jest osobą rozpromienioną. CHoroba zniszczyła jej wzrok, zniekształciła ją itp. Amanda i Xavera rozmawiając z rozszczebiotaną Petrą łapią patterny mowy. Petra powie kilka rzeczy:

* tu nie zaglądają terminusi. Tu czasem zagląda mafia. Dmitri nic na to nie może poradzić. Najpewniej Dmitri od mafii dostaje leki dla niej
* oni nie mają gości. Nie odkąd rodzina Dmitriego straciła reputację. Od TEGO czasu.
* Dmitri to złoty człowiek, unika młodych dziewczyn, bo przypominają mu córkę
* Petra skłoniła Dmitriego do wyjęcia filmów o Pustogorze
* Terminusi oprócz mafii mają też kulturę lokalną jako problem. I tu się trochę zwalczają.

Petra była straszną plotkarą. Xavera chce poznać ludzi dookoła których się obracała.

* Z opowieści Petry, ona jest przedstawicielką "starej krwi, starych pieniędzy".
* Dmitri i Petra to był sojusz dwóch rodów arystokratycznych - Petra opowiadała o przepychu ich rezydencji w Zaczęstwie.
* Skandal - 10 lat temu - doprowadził do przejęcia przez mafię "wszystkiego" i Dmitri się przeniósł TUTAJ. Daleko od wszystkiego. Nikt nie pomógł.
* Ich córka. Chciała pomścić rodziców. I zatrudniła się w mafii. Chciała ZEMŚCIĆ się. Ale zniknęła.

PRZEPRASZAM, jest ktoś kto czasem odwiedza Petrę. Ten przesympatyczny delikwent co poluje na noktian by im pomóc. On zwłaszcza szuka dzieci by je ukryć przed mafią. On szuka zbrodniarzy (task 1), ale faktycznie próbuje pomóc noktianom którzy "wpadli". On jest "na emeryturze", zwolniony z Orbitera? Albo z Aurum? Ma na imię Arnulf...

## Streszczenie

Furie po przejściu przez Mur nawiązały kontakt z Dmitrim (a to był Arnulf). Zostawiły sygnał, ale też zastawiły pułapkę - i skutecznie ominęły 5 servarów, acz się obie pochorowały i zapłaciły zdrowiem. Wyślizgnąwszy się z pułapki, napotkały Dmitriego i dotarły do jego fortifarmy, gdzie poznały też jego żonę, Petrę.

## Progresja

* .

## Zasługi

* Amanda Kajrat: połączyła się z 'Dmitrim', ale nie dała mu swojej pozycji; gdy wyskoczył na nią polujący technolampart, zestrzeliła go snajperką. Poraniona i w bąblach, ale uniknęła Arnulfa i dotarła z Xaverą do Dmitriego.
* Xavera Sirtas: ściągnęła żukowca i dała znać 'Dmitriemu' gdzie mniej więcej są; gdy już polowały na nie Lancery to ściągnęła potwora odpowiednimi dźwiękami. Gdy napotkały Dmitriego, ona była centrum dyplomacji ;-).
* Arnulf Poważny: vigilante z Aurum; ze swoją grupką poluje na noktian i próbuje ich odstawić do bezpiecznego miejsca LUB ukarać za zbrodnie wojenne. Polował na Amandę i Xaverę, ale mimo świetnie zastawionej pułapki i sprzętu nie poradził sobie z ich determinacją i tym że są homo superior. They suffered more than he expected ;-).
* Dmitri Karpov: astoriański sojusznik Kajrata; Wolny Uśmiech zabrał mu wszystko łącznie z córką. Mieszka na uboczu, w fortifarmie. Bardzo kocha żonę. Pomógł Xaverze i Amandzie, bo zwalczają Grzymościa.
* Petra Karpov: kiedyś arystokratka, oszpecona przez chorobę i ślepa; przyjazna Furiom i pogodzona z losem. Uczy Furie jak zachowywać się jak lokals i opowiedziała im historię Karpovów.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Powiat Przymurski
                                1. Las Przymurski
                                1. Fortifarma Karpovska

## Czas

* Opóźnienie: 0
* Dni: 2

## Specjalne

* .

## OTHER

.