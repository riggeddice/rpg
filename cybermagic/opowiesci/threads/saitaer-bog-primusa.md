# Thread
## Metadane

* Nazwa: Saitaer, bóg Primusa

## Krótki opis

Saitaer, Władca Adaptacji pochodzi z innego świata, zniszczonego przez Esuriit. Saitaer stopniowo adaptuje się do Primusa, stając się siłą pozytywną, acz niebezpieczną.

## Opis

Saitaer, Władca Adaptacji pochodzi z innego świata, zniszczonego przez Esuriit. Saitaer stopniowo adaptuje się do Primusa, stając się siłą pozytywną, acz niebezpieczną. Na początku Saitaer był bóstwem pasożytniczym, próbującym przekształcić Primus w Ixion, jednak z czasem zaczął obejmować Aspekt Adaptacji na Primusie i dopasowywać się do potrzeb ludzkości. W ramach tego wątku pokaż dokładnie to - jak na początku Saitaer stanowi zagrożenie, jak się przekształca i jak agresywnymi transformacjami nabiera zrozumienia o Primusie, specyfice Primusa i potrzebie Primusa.

## Spoilers
