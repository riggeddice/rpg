# Thread
## Metadane

* Nazwa: Iluzja Kontroli Anomalii Kolapsu

## Krótki opis

Anomalia Kolapsu jest potężną anomalią Alteris-Praecis. Myślimy, że możemy z niej korzystać, a jednak regularnie sprowadza zagładę.

## Opis

"Myślisz, że ją rozumiesz, ale nie wiesz co zaraz się stanie...".

Anomalia Kolapsu jest potężną anomalią Alteris-Praecis. Myślimy, że możemy z niej korzystać, a jednak regularnie sprowadza zagładę.

Ten Wątek opowiada o zmaganiach ludzi przeciwko Anomalii Kolapsu i próbie "udomowienia" tej Anomalii. Ludzie myślą, że są w stanie ją kontrolować, że są w stanie sobie z nią poradzić - w praktyce, Anomalia Kolapsu jest niesamowicie niszczycielską i niebezpieczną siłą. Gdy tylko czujemy się pewnie, Anomalia Kolapsu pokazuje nam, gdzie jest naprawdę nasze miejsce...

Zwykle ten wątek pokazuje ludzi którzy próbują coś zrobić, coś osiągnąć, zarobić dzięki Anomalii Kolapsu - i którzy w którymś momencie zostają przez Anomalię Kolapsu zniszczeni.

## Spoilers

### 1. Wydarzenia

* brak

### 2. Makroplany

* brak

### 3. Postacie

* brak