# Thread
## Metadane

* Nazwa: Samotna Maja w strasznym Aurum

## Krótki opis

Tien Maja Samszar, samotna, pod kloszem, bez przyjaciół. Jak i dlaczego uciekła na Orbiter?

## Opis

Tien Maja Samszar, samotna, pod kloszem, bez przyjaciół. Jak i dlaczego uciekła na Orbiter? W tym wątku skupiamy się na Mai Samszar, "ta piosenka nie o tym, o nie, jak czarnemu kotkowi jest źle". Pokazujemy jak jej życie pod kloszem, propozycja _arranged marriage_, rodzice którzy nie wiedzieli lepiej i ogólny koszmar Aurum sprawił, że Maja opuściła Aurum by zostać kompetentną nawigatorką Orbitera. 

## Spoilers

Ten wątek pokazuje CAŁĄ drogę Mai opuszczającej AURUM. Łącznie z czasem gdy jest na Orbiterze i pojawiają się węzły decyzyjne rozdzierające ją z Aurum.

Sesje w większości idą w następującą stronę:

* Pokazanie, jak Mai jest źle i nikogo nie ma
* Pokazanie, że Maja próbuje być dorosła i mimo wszystko "coś" zrobić
* Skrzywdzenie Mai z uwagi na zdrady i plany Aurum

Innymi słowy, Maja jako... punkt centralny pokazania wszystkiego co złe w Aurum.
