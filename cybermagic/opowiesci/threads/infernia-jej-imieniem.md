# Thread
## Metadane

* Nazwa: Infernia jej imieniem

## Krótki opis

Anomalia Kosmiczna Infernia. Dziwny, brzydki i niezwykle skuteczny statek. Czym jest i jak się objawia jej inność?

## Opis

Anomalia Kosmiczna Infernia. Dziwny, brzydki i niezwykle skuteczny statek. Czym jest i jak się objawia jej inność? W tym wątku skupiamy się na zmianach Inferni lub jej działaniach pokazujących jej odmienność. Pokazujemy jak Infernia się zmienia z czasem i jak niszczy życie wszystkich, którzy są z nią powiązani.

## Spoilers

Wola Interis. Wola Nihilusa.
