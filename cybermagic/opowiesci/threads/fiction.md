# Thread
## Metadane

* Nazwa: Fiction

## Krótki opis

Odcinek serialu, coś w fikcji. Coś, co się nie wydarzyło ale ma wpływ kulturowy.

## Opis

Odcinek serialu, coś w fikcji. Coś, co się nie wydarzyło ale ma wpływ kulturowy. Zasługi czy Progresje, jeśli są przypisane, mają prefix FICTION: xxx. Daty i chronologia są powiązane z tym kiedy odcinek wyszedł.

## Spoilers
