# Thread
## Metadane

* Nazwa: Tarcza Nox Aegis

## Krótki opis

Nox Aegis, tajemnicza ukryta arkologia noktiańska na Neikatis. Wielu o niej słyszało, mało kto wie, gdzie jest. Ten wątek eksploruje machinacje Nox Aegis i poznanie sekretu Tarczy.

## Opis

Nox Aegis, tajemnicza ukryta arkologia noktiańska na Neikatis. Wielu o niej słyszało, mało kto wie, gdzie jest. Ten wątek eksploruje machinacje Nox Aegis i poznanie sekretu Tarczy. W ramach tego wątku pokaż działania Nox Aegis oraz ruchy, które prowadzą do odkrycia sekretów Nox Aegis. Pokaż osoby rekrutowane przez Nox Aegis, agendy Nox Aegis i działania wzmacniające Nox Aegis.

## Spoilers

Nox Aegis współpracują z wolnymi TAI, ukryte są właśnie pod tą egidą. Analogia: Druga Fundacja.
