# Thread
## Metadane

* Nazwa: Cena Nox Ignis

## Krótki opis

Dżin Esuriit - Nox Ignis, Korweta z Anomalii Kolapsu. Spełni Twoje życzenie. Ale co Ci zabierze?

## Opis

Dżin Esuriit - Nox Ignis, Korweta z Anomalii Kolapsu. Spełni Twoje życzenie. Ale co Ci zabierze? Ten wątek pokazuje desperację noktian chętnych do używania tak straszliwej jednostki, wzrost w mocy Nox Ignis i stare dobre stwierdzenie ‘nic nie jest za darmo’. Pokazuje to też że niektórych potworów kosmicznych nie da się zniszczyć - ale da się jej zatrzymać.

## Spoilers
