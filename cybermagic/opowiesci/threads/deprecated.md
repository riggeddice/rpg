# Thread
## Metadane

* Nazwa: Deprecated

## Krótki opis

Legendy i opowieści, rzeczy które się wydarzyły ale nie są w 100% prawdą.

## Opis

"Legendy i opowieści", rzeczy które się wydarzyły ale rzeczywistość się przesunęła. W jakiejś formie te rzeczy się stały, te postacie coś tego typu zrobiły, ale nie traktujemy tych opowieści jako "kanonicznych". Te sesje dalej mają znaczenie, ale niekoniecznie Progresje czy Zasługi czy nawet wydarzenia odbyły się w 100% tak jak napisano w raporcie.

## Spoilers
