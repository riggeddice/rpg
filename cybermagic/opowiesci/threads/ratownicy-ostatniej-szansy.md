# Thread
## Metadane

* Nazwa: Ratownicy Ostatniej Szansy

## Krótki opis

Grupka bardzo kompetentnych noktian została przeniesiona do bazy daleko od ludzkości by pełnić straceńczą misję ratowników astoriańskich.

## Opis

Ten wątek opowiada o grupie noktian którzy stracili wszystko, trafili pod niedoświadczonego dowódcę z Astorii by ratować jednostki mające problemy w kosmosie. Każdy z noktian ma swoje własne problemy, nie są akceptowani przez astorian i muszą jakoś sobie radzić. Często problemem są mniej akcje ratunkowe a bardziej sojusznicy z którymi muszą współpracować.

## Spoilers
