# Thread
## Metadane

* Nazwa: Progildie Zaczęstwa

## Krótki opis

Zaczęstwo to hotspot wielu gildii i gier MultiVirt. Ten wątek pokazuje czemu i jak to się stało.

## Opis

Zaczęstwo to hotspot wielu gildii i gier MultiVirt. Ten wątek pokazuje czemu i jak to się stało. W ramach tego wątku pokazujemy jak obecność Cyberszkoły i AMZ daje elementy niezbędne do konstrukcji progildii i akademii progildii. Jak powojenny sprzęt daje opcje eksperymentów i testowania. I jak obecność Rekinów z Aurum jedynie motywowała zaczęstwiaków do stania się lepszym. Prawdziwy tygiel frakcji, osobowości + zasoby.

## Spoilers
