# Thread
## Metadane

* Nazwa: Córka Morlana

## Krótki opis

Nataniel Morlan rozpaczliwie próbujący odzyskać córkę i Antonella Temaris próbująca odzyskać siebie.

## Opis

Nataniel Morlan rozpaczliwie próbujący odzyskać córkę i Antonella Temaris próbująca odzyskać siebie. Eternijski tien, rządzący żelazną ręką, próbujący odzyskać córkę, którą stracił. Ten wątek skupia się na wpływach i mafijnej ośmiornicy w rękach Morlana, który nie akceptuje utraty córki nawet jeśli ona nie chce mieć z nim nic wspólnego. Morlan zawsze szuka córki a ona zawsze przed nim ucieka.

## Spoilers

* Init_rl: 210810-porwanie-na-gwiezdnym-motylu
* O czym
    * Nataniel Morlan rozpaczliwie próbujący odzyskać córkę i Antonella Temaris próbująca odzyskać siebie
    * Lokalizacja: głównie Eternia / kosmos
* Theme
    * Complete Monster, Pure of Heart
        * Caramon & Raistlin "taka wielka nienawiść... taka wielka miłość"
        * Caduceus Beetlestone & Demmi Beetlestone "like father, like daughter"
