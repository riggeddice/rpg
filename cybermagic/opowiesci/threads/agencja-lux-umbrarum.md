# Thread
## Metadane

* Nazwa: Agencja Lux Umbrarum

## Krótki opis

Noktiańskie "FBI w kosmosie", Agencja Lux Umbrarum zajmuje się usuwaniem anomalii i ukrywaniem magii przed populacją.

## Opis

Noktiańskie "FBI w kosmosie", Agencja Lux Umbrarum zajmuje się usuwaniem anomalii i ukrywaniem magii przed populacją. Lux Umbrarum jest oficjalną agencją do spraw dziwnych i nietypowych, psującej się technologii, szalejących nanomaszyn i zjawisk niewyjaśnionych. W rzeczywistości, ukrywają obecność magii i anomalii przed ludźmi i zatrzymują zmiany Paradygmatu. Niech światy Noctis będą światami noktiańskimi, nie to co światy innych sektorów.

Lux Umbrarum posiada dobrze wyposażone siły, amnestyki oraz uprawnienia i współpracę lokalnej społeczności. Jednocześnie, Agencja nie próbuje nikomu robić krzywdy.

Ich głównymi przeciwnikami jest Kult 'Zakon Nocnej Prawdy' oraz poszukiwacze prawdy pozarządowi. Czasem Syndykat Aureliona (którzy są wszędzie...)

## Spoilers
