# Thread
## Metadane

* Nazwa: Autonomia Neotik

## Krótki opis

Stocznia Neotik pod kontrolą Szarjanów chce być czymś osobnym od Orbitera. Chcą tworzyć piękną, perfekcyjną broń i wspierać Astorię, Neikatis i Orbiter, nie będąc od nikogo zależnymi.

## Opis

Stocznia Neotik pod kontrolą Szarjanów chce być czymś osobnym od Orbitera. Chcą tworzyć piękną, perfekcyjną broń i wspierać Astorię, Neikatis i Orbiter, nie będąc od nikogo zależnymi. Ten wątek skupia się na różnego rodzaju skomplikowanych eksperymentacj Neotik i coraz bardziej zdesperowanych ruchach i akcjach mających im dać niezależność i wolność.

## Spoilers
