# Thread
## Metadane

* Nazwa: Agenci Arkologii Araknis

## Krótki opis

Zniszczona Arkologia Araknis na Neikatis zawiera wiele mrocznych sekretów. Orbiter planuje jej odbudowę, ale czy to dobry pomysł?

## Opis

Zniszczona Arkologia Araknis na Neikatis zawiera wiele mrocznych sekretów. Orbiter planuje jej odbudowę, ale czy to dobry pomysł?

Arkologia Araknis jest zrujnowaną, w większości zasypaną strukturą częściowo naziemną i częściowo podziemną. Jest uznawana za bardzo niebezpieczne miejsce, zamieszkana przez birutanów i "kto tu wejdzie, nie wróci". Orbiter pragnie poznać sekrety Araknis - Arkologii, do której podobno borutanowie porywają ludzi.

## Spoilers

### 1. Wydarzenia

* Chain: "Arkologia Królowej Pająków"
    * Mamy tu Wolną TAI, która pała nienawiścią do ludzi i pragnie zarówno się uwolnić jak i pozbyć ludzi z Neikatis. Lub trzymać ich w farmie. Ona jest sławną "Królową Pająków".
    * Mamy tu różne birutany, które są wzmacniane Energią i przyprowadzają dalszych ludzi do Araknis.

### 2. Makroplany

* Araknis jest nadal częściowo żywa. Jest tu Autonomiczna TAI próbująca wykorzystać birutany jako swoich agentów.
* Araknis ma częste Burze Magiczne; kolejny powód, czemu jest "przeklęta".

### 3. Postacie




