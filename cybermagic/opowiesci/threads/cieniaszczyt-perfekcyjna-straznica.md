# Thread
## Metadane

* Nazwa: Cieniaszczyt, perfekcyjna strażnica

## Krótki opis

Jeszcze przed Finis Vitae Cieniaszczyt stał jako strażnica, chroniąc Sojusz przed CZYMŚ. Obcość kulturowa i obecność kralothów jest niefajna, ale konieczna. Czemu?

## Opis

Jeszcze przed Finis Vitae Cieniaszczyt stał jako strażnica, chroniąc Sojusz przed CZYMŚ. Obcość kulturowa i obecność kralothów jest niefajna, ale konieczna. Czemu? Ten wątek skupia się na Cieniaszczycie jako mechanizmie defensywnym przed wieloma zagrożeniami które zagrażają Sojusz Letejski. Ten wątek właśnie służy do odkrywania tajemnic Cieniaszczytu, wielu zakazanych, sekretnych miejsc i mroku kryjącego się w labiryncie pod Cieniaszczytem.

## Spoilers
