# Thread
## Metadane

* Nazwa: Spawner godny Verlenlandu

## Krótki opis

W krainie potworów i legend powstaje Wielka Arena, Verlenowie kontra Potwory...

## Opis

W krainie potworów i legend powstaje Wielka Arena, Verlenowie kontra Potwory... Ten wątek skupia się na próbach opanowania Arachnoziemu i konflikcie pomiędzy naturalnymi bogactwami i harmonią tego miejsca a potencjalną Wielką Areną, miejscu, które zmieni Verlenów w najdoskonalszych wojowników.

## Spoilers

Działania frakcji Krwawe Ciernie Verlenów. Aktualny kierunek - Arachnoziem.
