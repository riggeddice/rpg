# Thread
## Metadane

* Nazwa: Reintegracja Minerwy Metalii

## Krótki opis

Wybitna ekspertka od konstrukcji broni, Minerwa Diakon, zginęła w Szczelinie i wróciła jako ixioński terrorform. Jak Minerwa wróci do społeczeństwa i co ją czeka?

## Opis

Wybitna ekspertka od konstrukcji broni, Minerwa Diakon, zginęła w Szczelinie i wróciła jako ixioński terrorform. Jak Minerwa wróci do społeczeństwa i co ją czeka? Ten wątek skupia się na losie Minerwy po jej 'śmierci' i próbach znalezienia przez nią nowego życia po tym, jak stała się niesamowicie niebezpieczną czarodziejką ixiońską.

## Spoilers
