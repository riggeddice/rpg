# Thread
## Metadane

* Nazwa: Olga, regentka Czarnopalca

## Krótki opis

Olga Myszeczka, spokojna introwertyczna czarodziejka objęła Czarnopalec - martwe miejsce - jako swój dom gdzie zajmuje się viciniusami. Jakoś została ważnym graczem w okolicy, choć nie wie jak i czemu.

## Opis

Olga Myszeczka, spokojna introwertyczna czarodziejka objęła Czarnopalec - martwe miejsce - jako swój dom gdzie zajmuje się viciniusami. Jakoś została ważnym graczem w okolicy, choć nie wie jak i czemu. W ramach tego wątku skupiamy się na interakcjach Olgi ze 'światem normalnym' i pokazujemy jej ścieżkę dojścia do bycia "potężną czarodziejką". Pokażmy jej interakcje z Wiktorem Satarailem, ich sojusz i przyjaźń. Pokażmy ich działania jako mechanizm tworzący subksięstwo Zjawosztup. Pokażmy miłość do viciniusów i obcy, nieludzki aspekt niedaleko ludzkich ziem Szczelińca.

## Spoilers
