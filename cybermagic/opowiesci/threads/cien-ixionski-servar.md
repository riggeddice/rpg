# Thread
## Metadane

* Nazwa: Cień, ixioński servar

## Krótki opis

Cień. Ixioński symbiotyczny servar. Nieokiełznany straszliwy Smok, który zniszczy wszystko. Historia Cienia od powstania do zniszczenia.

## Opis

Cień. Ixioński symbiotyczny servar. Nieokiełznany straszliwy Smok, który zniszczy wszystko. Historia Cienia od powstania w laboratoriach Orbiter Aurora przy pomocy Minerwy Metalii do zniszczenia w ramionach Pięknotki Diakon jako narzędzie Saitaera. Pokaż niszczycielskość Cienia i jego niezwykłą przydatność i adaptację.

## Spoilers
