# Thread
## Metadane

* Nazwa: Przeznaczenie Eleny Verlen

## Krótki opis

Elena zaczęła jako bardzo obiecująca arcymag Verlenów, lubiana przez kuzynki. Jak to się stało, że stała się Spustoszeniem?

## Opis

Elena zaczęła jako bardzo obiecująca arcymag Verlenów, lubiana przez kuzynki. Jak to się stało, że stała się Spustoszeniem? Ten wątek skupia się na kluczowych elementach w życiu Eleny i jej decyzjach, które scementowały jej przeznaczenie jako Koordynator Spustoszenia na Orbiterze. Wbrew nazwie, tego "przeznaczenia" dało się uniknąć, gdyby Elena podjęła w kluczowych momentach inne decyzje...

## Spoilers
