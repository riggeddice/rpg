# Thread
## Metadane

* Nazwa: Tysiące wojen Liliany

## Krótki opis

Liliana Bankierz otwiera front za frontem. Zygmunt Zając, Marysia Sowińska itp. Dlaczego?

## Opis

Liliana Bankierz otwiera front za frontem. Zygmunt Zając, Marysia Sowińska itp. Dlaczego? Ten wątek skupia się na sercu Liliany. Atakuje, bo uważa, że jest atakowana. Straciła swój ukochany wzór, więc nie wierzy w to, że przed nią jest coś wartościowego i próbuje czystą determinacją skompensować swe słabości. Mściwa jak cholera, odważna do bólu. W tym wątku zatem skupiamy się na znalezieniu przyszłości dla Liliany. Czym się stanie. Gdzie znajdzie 'dom'. I wszystkie fronty które otwiera i przeciwnicy jacy są przed nią.

## Spoilers
