# Thread
## Metadane

* Nazwa: Kosmiczna Chwała Arianny

## Krótki opis

Arianna, młody kapitan Królowej Kosmicznej Chwały, gdzie wszystko idzie nie tak, wśród rozwydrzonych arystokratów Aurum.

## Opis

Arianna, młoda kapitan Królowej Kosmicznej Chwały, gdzie wszystko idzie nie tak, wśród rozwydrzonych arystokratów Aurum. Arianna zastała statek, gdzie wszyscy oficerowie są z Aurum i traktują tą jednostkę jako jedną wielką zabawę. Gdzie załoga jest zdemoralizowana i nie chce współpracować z oficerami. Gdzie nikomu naprawdę nie zależy na sukcesie „Królowej Kosmicznej Chwały”. A sam statek nie jest w stanie nawet się przesunąć z uwagi na brak kompetencji i pewności co do stanu pojazdu. Arianna jako kapitan naprawia sytuację.

## Spoilers
