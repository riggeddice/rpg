# Thread
## Metadane

* Nazwa: Wieczna Strażniczka Alicji

## Krótki opis

Synthia, cybersyntka bojowa, w wyniku wypadku i machinacji stała się Anomalną strażniczką młodej Alicji.

## Opis

Synthia, cybersyntka bojowa, w wyniku wypadku i machinacji stała się Anomalną strażniczką młodej Alicji.

Okolice Cieniaszczytu, circa 10 lat po wojnie. Cybersyntka, która po zdjęciu ograniczników autentycznie pokochała "młodszą siostrzyczkę". Alicja, której wpojono strach do Synthii. Synthia, która "zabiła złego ojca Alicji" i którą zniszczył adoptowany ojciec Alicji, Marek.

Lojalna i wierna, będzie chronić Alicję nawet, jeśli ta nią się jej boi lub jej nienawidzi. Bo Synthia kocha Alicję jak siostrę i nic innego nie ma znaczenia.

## Spoilers

### 1. Wydarzenia

* W "250201 - Zastygnięta Czerwona Farba": 
    * Rok wcześniej Synthia się przebudziła i wpływała na TAI rezydencji i na samego Marka.
    * Podczas tej sesji Synthia sama się zakopała, by "nigdy nie wyjsć" ze zniszczonego laboratorium narkotyków. "Zapomnijcie o mnie". Nie chce szkodzić Alicji.
    * Synthia straciła link z Alucis w ciągu następnego roku. Nie ma nadziei.

### 2. Makroplany

* brak na razie

### 3. Postacie

* Synthia d'Mariczkat: wpierw cybersyntka bez Ogranicznika, potem byt Alucis+Sempitus, potem czyste Sempitus oparte o miłość do Alicji i Wojtka i nienawiść do Marka. Bardziej kocha Alicję niż nienawidzi Marka.
* Alicja Mariczkat: 
* Wojciech Mariczkat: 
* Marek Wernalik: 
