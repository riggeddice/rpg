# Thread
## Metadane

* Nazwa: Dziewczyna i Pies

## Krótki opis

Głód animujący dziewczynę próbującą odzyskać rodzinę. Lojalność psa, który chroni ją mimo energii magicznych.

## Opis

Głód animujący dziewczynę próbującą odzyskać rodzinę. Lojalność psa, który chroni ją mimo energii magicznych.  Ten wątek skupia się na nieszczęsnej Patrycji, która nieświadomie użyła magii krwi by przywrócić ojca. Nawet jeśli ojciec nie wrócił prawdziwy, wierny pies Patrycji, Lucek, chroni swoją panią przed anomaliami i konsekwencjami jej czynów. Patrycja pragnie rodziny, Lucek pragnie szczęścia Patrycji a Esuriit zawsze wygrywa...

## Spoilers
