# Thread
## Metadane

* Nazwa: Grupa Wydzielona Serbinius

## Krótki opis

Korweta Orbitera pierwszego ratunku Serbinius jest na froncie typowych sytuacji z którymi ma do czynienia Orbiter przy Astorii.

## Opis

Korweta Orbitera pierwszego ratunku Serbinius jest na froncie typowych sytuacji z którymi ma do czynienia Orbiter przy Astorii. W tym wątku skupiamy się na dość kompetentnym, acz niedoświadczonym dowódcy, pokazaniem z czym Orbiter ma do czynienia przy Astorii oraz rozbudowujemy los Serbiniusa jako jednostki.

## Spoilers

