# Thread
## Metadane

* Nazwa: Tragedia Trzeciego Raju

## Krótki opis

Trzeci Raj jako miasteczko jest formą neutralizacji i containowania noktian na Astorii. Jest jednocześnie poletkiem eksperymentalnym Admirał Termii z Orbitera.

## Opis

Trzeci Raj jako miasteczko jest formą neutralizacji i containowania noktian na Astorii. Jest jednocześnie poletkiem eksperymentalnym Admirał Termii z Orbitera. W ramach tego wątku skupiamy się na pokazaniu eksperymentów Admirał Termii, zarówno w obszarze Ataienne jak i samego Trzeciego Raju. W końcu jest to TRZECI Raj a Termia słynie raczej z prób zrozumienia i traktowania ludzi jak zasoby a nie ze sprawiania, by nieszczęsnym noktianom z Trzeciego Raju żyło się dobrze.

## Spoilers
