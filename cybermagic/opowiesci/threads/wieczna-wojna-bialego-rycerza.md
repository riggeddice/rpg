# Thread
## Metadane

* Nazwa: Wieczna Wojna Białego Rycerza

## Krótki opis

Kornel Garn, ekstremista Orbitera i jeden z najlepszych komandosów nigdy nie skończył wojny. Dotknięty przez Saitaera, kontynuuje walkę choć niekoniecznie pamięta dlaczego i dla kogo.

## Opis

Kornel Garn, ekstremista Orbitera i jeden z najlepszych komandosów nigdy nie skończył wojny. Dotknięty przez Saitaera, kontynuuje walkę choć niekoniecznie pamięta dlaczego i dla kogo. W ramach tego wątku skupiamy się na działach Kornela i jego próbach zbudowania lepszego świata w swoim rozumieniu, na jego próbach zniszczenia wszelkiego zła, zrozumienia natury Saitaera, planety i zniszczenia wszelkich nieprawości. Jak i replikacji technologii i bioform mających naprawić świat.

## Spoilers
