# Thread
## Metadane

* Nazwa: Kidiron, zbawca Nativis

## Krótki opis

Arkologia Nativis była kiedyś na granicy upadku. Rafał Kidiron zrobił sporo mrocznych rzeczy by stała się wielka.

## Opis

Arkologia Nativis była kiedyś na granicy upadku. Rafał Kidiron zrobił sporo mrocznych rzeczy by stała się wielka. Ten wątek skupia się na wzroście potęgi Arkologii Nativis i na wszystkich zbrodniach, które Rafał Kidiron musiał popełnić. Ten wątek też pokazuje Kidirona jako zło konieczne Arkologii. Bez niego Arkologia Nativis upadnie albo będzie strasznie słaba. "Oddałem wszystko dla tej arkologii..."

## Spoilers
