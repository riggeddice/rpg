# Thread
## Metadane

* Nazwa: Turniej SupremeMissionforce, edycja 41

## Krótki opis

41 edycja turnieju Supreme Missionforce w której uczestniczą zawodnicy z całej Astorii – a nawet Orbitera. Plotki mówią, że najlepsi będą mogli iść w gwiazdy.

## Opis

41 edycja turnieju Supreme Missionforce w której uczestniczą zawodnicy z całej Astorii – a nawet Orbitera. Plotki mówią, że najlepsi będą mogli iść w gwiazdy. Ten wątek skupia się na różnego rodzaju próbach i działaniach zwycięstwa w turnieju przez różne osoby, na konfliktach czy wziąć przyjaciela czy lepiej zdobyć silniejszego sojusznika i na różnego rodzaju próbach i staraniach dookoła tego turnieju. 

## Spoilers
