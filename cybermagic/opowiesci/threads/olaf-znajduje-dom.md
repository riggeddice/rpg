# Thread
## Metadane

* Nazwa: Olaf znajduje dom.

## Krótki opis

Olaf Zuchwały, Noktianin, który stoczył się do rynsztoka, zostaje porwany i wykorzystany w survival reality show Lemurczaków.

## Opis

Ród Lemurczak organizuje igrzyska będące połączeniem survivalowego reality show z hunger games. Jest to dla nich sposób na pokojowe rozwiązanie konfliktów między magami różnych rodów.
Przy użyciu senti sieci tworzona jest arena, która podda uczestników testowi. Zespoły składane są z życiowych przegrywów - ludzi, którzy się stoczyli, którzy nie mają ochrony i których nikt nie będzie szukał. 
Przed rozpoczęciem igrzysk każdy może sobie "zamówić" członków zespołu. Można również zapłacić za dorzucenie do zespołu przeciwników osoby, która mu zaszkodzi.
W trakcie trwania można wykupić bonusy i przeszkody dla drużyn. Odbywają się też zakłady.

Drużyny zrzucane są każda w innym obszarze areny z minimalną wiedzą o tym, co się dzieje.
Dostają informację, że zespół, który pokona wszystkie inne wygrywa i otrzymują najbardziej podstawowe narzędzia do survivalu. 
Reszta to kwestia ich wysiłków, umiejętności i tego, jak dobre przedstawienie zrobią.
A na sam koniec publiczność może wykupić dla ulubionych lub znienawidzonych uczestników los, czyli co ma się z nimi dalej stać.

Olaf, choć dorzucony do zespołu jako "ten bez sensu", skutecznie integruje grupę i udaje mu się doprowadzić do bardzo nietypowej edycji igrzysk - takiej, gdzie przeżyło więcej niż planowano. Choć nie obyło się bez walki i strat w ludziach, zespół Olafa przetrwał.

## Spoilers

Arena igrzysk

|Tundra|Las deszczowy|Sawanna|
|Las europejski|Pustynia|Pasmo górskie|
|Tajga|Dżungla (nadbrzeżna)| Las namorzynowy|

Zespoły:
* A
* B
* C
* D
* E (zespół gracza)

Ścieżki obrane przez zespoły:
* A: Tundra -> Pustynia (tu padli do **D**)
* B: Las deszczowy -> Tundra -> Las europejski (tu spotkali się z D, padła połowa); połączyli siły z **D**
* C: Sawanna -> Pasmo górskie -> Pustynia
* D: Dżungla nadbrzeżna -> Pustynia (tu wykończyli A, stracili jednego) -> Las europejski (tu spotkali się z **B**, stracili dwójkę); połączyli siły z **B**
* E: Pasmo górskie -> Las namorzynowy -> Pasmo górskie
* F: Połączenie **B** i **D**

