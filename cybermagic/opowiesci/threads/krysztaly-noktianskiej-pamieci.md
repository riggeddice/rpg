# Thread
## Metadane

* Nazwa: Kryształy noktiańskiej pamięci

## Krótki opis

Eliza Ira stała się arcymagiem i jej wola została dostosowana do woli Noctis Eternia. Eliza wiecznie walczy o zapamiętanie Noctis... ale czym Noctis jest?

## Opis

Eliza Ira stała się arcymagiem i jej wola została dostosowana do woli Noctis Eternia. Eliza wiecznie walczy o zapamiętanie Noctis... ale czym Noctis jest? Eliza, Skażona ponad swoją osobowość, niesamowicie niebezpieczna skupia się na Zapamiętaniu i Krystalizacji Noctis. Uratowaniu wszystkich noktian. Zbudowaniu nieśmiertelnej Kryształowej Fortecy. W tym wątku skupiamy się na jej planie na ratunek noktian, na tym co próbuje osiągnąć używając swoich kryształów i jak próbuje zbudować swoim Skażonym umysłem nowe, lepsze życie dla noktian.

## Spoilers
