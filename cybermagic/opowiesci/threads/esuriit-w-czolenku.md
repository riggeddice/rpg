# Thread
## Metadane

* Nazwa: Esuriit w Czółenku

## Krótki opis

W niewielkim miasteczku Czółenku są stare bunkry, w których dominuje Esuriit.

## Opis

W niewielkim miasteczku Czółenku są stare bunkry, w których dominuje Esuriit. Ten wątek skupia się na próbach opanowania Esuriit, wylewaniu się Esuriit z Czółenka oraz na próbach zatrzymania wpływu Esuriit na cały region. „Zawsze musi być jeden Strażnik...”

## Spoilers

