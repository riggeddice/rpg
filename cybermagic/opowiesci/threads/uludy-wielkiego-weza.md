# Thread
## Metadane

* Nazwa: Ułudy Wielkiego Węża

## Krótki opis

SC Wielki Wąż wlatuje na teren anomalicznego Morza Ułud, napotykając rzeczy straszne i dziwne.

## Opis

SC Wielki Wąż wlatuje na teren anomalicznego Morza Ułud, napotykając rzeczy straszne i dziwne. Ten wątek skupia się na rozgrywaniu załogi przeciwko sobie, na działaniu tajemniczych potworów w Morzu Ułud i stanowi prawdziwie paranoiczno-intrygancko-dreszczykowy koktajl.

## Spoilers

