# Thread
## Metadane

* Nazwa: Wojna o duszę Szczelińca

## Krótki opis

Szczeliniec jest tyglem wielu frakcji i kultur, pod parasolem Pustogoru. Każda frakcja próbuje jednak zdobyć rząd dusz po swojemu.

## Opis

Szczeliniec jest tyglem wielu frakcji i kultur, pod parasolem Pustogoru. Każda frakcja próbuje jednak zdobyć rząd dusz po swojemu. Ten wątek skupia się właśnie na próbach przejęcia rządu dusz w okolicach Pustogoru i w Szczelińcu. Między Grzymościem, Kajratem, wpływom Cieniaszczytu, Kręgiem Ośmiornicy, Czerwonymi Myszami, Rekinami i wieloma innymi.

## Spoilers
