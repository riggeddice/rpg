# Thread
## Metadane

* Nazwa: Karolinus (heart) Viorika

## Krótki opis

Przez pewien czas Karolinus 'podrywał' Viorikę i atakował Verlenland...

## Opis

Przez pewien czas Karolinus 'podrywał' Viorikę i atakował Verlenland... a przynajmniej tak to wyglądało. Ten wątek zajmuje się tematem akcji i reakcji - jak Karolinus 'podrywa' Viorikę, jak Verlenowie odpowiadają na te podrywy i jak wszystko eskaluje w najbardziej absurdalny sposób.

## Spoilers
