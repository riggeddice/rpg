# Thread
## Metadane

* Nazwa: Pronoktiańska mafia Kajrata

## Krótki opis

Ernest Kajrat, noktiański oficer skupił cały swój intelekt i zasoby by pomóc pobratymcom. Założył mafię mającą ich ratować.

## Opis

Ernest Kajrat, noktiański oficer skupił cały swój intelekt i zasoby by pomóc pobratymcom. Założył mafię mającą ich ratować. Ten wątek skupia się na rozbudowie potęgi mafii, na pokazaniu jej bezwzględności wobec astorian i na pokazaniu jak daleko się posuwa by ratować noktian. Pieniądze mają mniejsze znaczenie niż uratowanie jak największej ilości noktian i zbudowanie im lepszego świata.

## Spoilers
