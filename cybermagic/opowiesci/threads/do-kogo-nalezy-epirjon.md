# Thread
## Metadane

* Nazwa: Do kogo należy Epirjon?

## Krótki opis

Anomalna Stacja Epirjon, powiązana z Aurum oraz Szczelińcem, jest bardzo atrakcyjna dla wielu frakcji. I różne frakcje próbują przejąć nad nią kontrolę.

## Opis

Anomalna Stacja Epirjon, powiązana z Aurum oraz Szczelińcem, jest bardzo atrakcyjna dla wielu frakcji. I różne frakcje próbują przejąć nad nią kontrolę. Epirjon jest zawieszony pomiędzy trzema siłami - Orbiter (który pragnie pozyskać anomalną stację), Aurum (które próbuje utrzymać stację) i Szczeliniec (gdzie Epirjon ma główne działania z jakiegoś anomalnego powodu). Ten wątek skupia się właśnie na analizie sił wpływających na Epirjona i na budowaniu sojuszy przez próbujący spełniać swoją misję badawczą i wspierającą Epirjon.

## Spoilers
