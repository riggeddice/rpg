# Thread
## Metadane

* Nazwa: Igrzyska Lemurczaków

## Krótki opis

Na Wielkiej Arenie Lemurczaków grupa przegrywów została rzucona by grupa arystokratów różnych rodów mogli rozwiązać konflikt.

## Opis

Na Wielkiej Arenie Lemurczaków grupa porwanych przegrywów z różnych stron została rzucona w konflikt między sobą z podziałem na grupy, by arystokraci różnych rodów mogli rozwiązać konflikt między sobą. W tym wszystkim jest grupa biomów (dla 'pionków') i zakłady (dla 'arystokratów').

## Spoilers
