# Thread
## Metadane

* Nazwa: Pięknotka, kapłanka Saitaera

## Krótki opis

Terminuska Pustogoru miała pecha napotkać na swojej drodze Boga Adaptacji, który postanowił zrobić z niej swoją kapłankę.

## Opis

Terminuska Pustogoru miała pecha napotkać na swojej drodze Boga Adaptacji, który postanowił zrobić z niej swoją kapłankę. W ramach tego wątku eksplorujemy jak Saitaer doprowadził do upadku terminuski i jak się mu postawiła po raz ostatni.

## Spoilers
