# Thread
## Metadane

* Nazwa: Magiczne szczury podwierckie

## Krótki opis

W Podwiercie pojawiły się dziwne magiczne szczury.  Mimo że są bardziej uciążliwe niż groźne, powoduje to problemy międzyludzkie.

## Opis

W Podwiercie pojawiły się dziwne magiczne szczury.  Mimo że są bardziej uciążliwe niż groźne, powoduje to problemy międzyludzkie. Mimo że szczury zostały przypadkowo stworzone przez Rekiny, wina spada na AMZ. Ten wątek skupia się na tym, jak obecność całkiem niegroźnych szczurów rozpala napięcia pomiędzy różnymi frakcjami Szczelińca oraz magami i ludźmi.

## Spoilers
