# Thread
## Metadane

* Nazwa: Triumfalny Powrót Arianny

## Krótki opis

Arianna, bohaterka, została Dotknięta przez Kryptę. Teraz wraca po machinacjach Lyraczka, jako cicha i skuteczna oficer Orbitera.

## Opis

Arianna, bohaterka, została Dotknięta przez Kryptę. Teraz wraca po machinacjach Lyraczka, jako cicha i skuteczna oficer Orbitera. Arianna odbudowuje znajomości z Leszkiem Kurzminem i wykonuje działania dookoła sił Orbitera używając Inferni jako swojego statku flagowego. "Złoty czas" działań Arianny - w ramach działań z Orbiterem mediuje między poszczególnymi frakcjami pokazując, że było warto dać jej szansę.

## Spoilers
