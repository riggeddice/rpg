# Thread
## Metadane

* Nazwa: Serce Planetoidy Kabanek

## Krótki opis

Planetoida Kabanek, w Libracji Lirańskiej, niedaleko Anomalii Kolapsu, próbuje być alternatywą dla Nonariona Nadziei. Ale sama ma ciekawe problemy...

## Opis

Planetoida Kabanek, w Libracji Lirańskiej, niedaleko Anomalii Kolapsu, próbuje być alternatywą dla Nonariona Nadziei. Ale sama ma ciekawe problemy.

Kabanek i okoliczne sprzężone planetoidy tworzą sojusz, które próbuje się wspierać i chronić swoich. Jednocześnie populacja jest zdecydowanie za mała na sensowny sojusz, jak i są tu siły zewnętrzne na ów sojusz działające. Z perspektywy metakultur, wiemy o dominujących grupach:

* Savaranach, którzy bez problemu dopasowują się do życia w tych warunkach; są mniej skrajni niż zwykle
* Dekadianach, którzy chcą chronić swojego nowego domu po zdradzie światów noktiańskich
* Atarienach, którzy szukają szczęścia i możliwości poradzenia sobie
* Orilianach, którzy próbują przekonać wszystkich do swojego podejścia i do integracji
* Faerilach, dla których jest to kontynuacja dalszych działań górniczych; nie przejmują się warunkami.
* Klarkartianie, uczciwi postnoktiańscy optymiści, którzy robią co mogą i tęsknią za lepszym wczoraj. Próbują ZROZUMIEĆ co się stało.

I o dominujących frakcjach:

* Karepin, frakcja sojuszu trzech planetoid: Kabanek, Rebison i Pinvadon. Chcą zrobić tu ośrodek władzy, pod kontrolą triady planetoid.
* Aurelion, który znajduje okazję by wejść na teren Libracji Lirańskiej przez Kabanek.
* Orbiter, który próbuje uniemożliwić pojawienie się tu Aureliona i bardzo groźnych Anomalii.
* Listar, którzy stanowią Noctis Returned, pragnący zbudować nowe, lepsze, niekoniecznie wrogie Astorii Noctis; spójni z oddziałem Alarius.
* Doloryci, noktiani idący w ślady Valerii Dolor, pragnący nowej, lepszej wojny między Astorią i Noctis. Ekstremiści.
* Fretyści, którzy raczej idą w niezależność i w luźne związki a nie w jakieś bardziej skomplikowane operacje.

Tymczasowo Kabanek jest pod kontrolą Karepin, acz z silnym wpływem Fretystów.

## Spoilers

### 1. Wydarzenia

* Chain: "Osiemnaście Oczu"
    * Dwa lata przed sesją "Osiemnaście Oczu" Aurelion wysłał tu potężną jednostkę ukrywającą się w Anomalii Kolapsu i mającą za zadanie przejąć, doradzać i wzmocnić wpływy Aureliona na Kabanku.

### 2. Makroplany

* The Factory
    * Natalia wie, uciekła na Orbiter; udało jej się umknąć.
    * Lodowiec wie, szuka

### 3. Postacie




