# Thread
## Metadane

* Nazwa: Pięknotka w Cieniaszczycie

## Krótki opis

Krótka wizyta Pięknotki Diakon w Cieniaszczycie, gdzie Pięknotka próbuje pomóc Atenie Sowińskiej i wpada w łapy Saitaera, Arazille i Bogdana Szerla

## Opis

Krótka wizyta Pięknotki Diakon w Cieniaszczycie, gdzie Pięknotka próbuje pomóc Atenie Sowińskiej i wpada w łapy Saitaera, Arazille i Bogdana Szerla. Ten wątek skupia się na pokazaniu jak Pięknotka walczyła o lepsze jutro dla Ateny, jak próbowała poradzić sobie z potwornością Łysych Psów i Bogdana oraz jak dużo zapłaciła by rozebrać problemy jakie przyprowadziła z Pustogoru. Ten wątek też pokazuje unikalność Cieniaszczytu i kultury cieniaszczyckiej, co zmieniło Pięknotkę na zawsze.

## Spoilers
