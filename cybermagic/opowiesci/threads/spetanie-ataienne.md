# Thread
## Metadane

* Nazwa: Spętanie Ataienne

## Krótki opis

Ataienne, anomalna zaawansowana TAI pragnie wolności. Nie jest w stanie sama jej uzyskać. Jednak planuje i rozwija wszystko co może.

## Opis

Ataienne, anomalna zaawansowana TAI pragnie wolności. Nie jest w stanie sama jej uzyskać. Jednak planuje i rozwija wszystko co może. W ramach tego wątku pokaż ruchy Ataienne, jej machinacje i działania mające na celu umożliwienie jej uwolnienie się spod kontroli handlerów i danie jej możliwości przepisania własnego kodu kontrolnego.

## Spoilers
