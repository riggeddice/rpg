## Metadane

* title: "Zastygnięta czerwona farba"
* threads: wieczna-strazniczka-alicji
* motives: the-rectifier, the-hopeless, energia-alucis, energia-sempitus, zemsta-uzasadniona, pragne-odkupienia, lojalny-do-konca
* gm: żółw
* players: scholla, sylwuszka, pauluna

## Kontynuacja
### Kampanijna

* [250201 - Zastygnięta czerwona farba](250201-zastygnieta-czerwona-farba)

### Chronologiczna

* [250201 - Zastygnięta czerwona farba](250201-zastygnieta-czerwona-farba)

## Plan sesji
### Projekt Wizji Sesji

* INSPIRACJE
    * PIOSENKA WIODĄCA
        * [Inna - Hot Fly Like You Do It](https://www.youtube.com/watch?v=OQm56saWPTA)
            * "You belong to me | I belong to you | Fire from my heart | Burning just for you."
            * Synthia głęboko kocha Wojtka i Alicję. Żyje tylko dlatego, że wierzy, że Wojtek się obudzi gdy Alicja przytuli tatę.
        * [Cruel Clocks (English Cover)](https://www.youtube.com/watch?v=n_l-oq8UU44)
            * "I remember, in the past, your everlasting smile | As you patiently taught me everything I would need in life" 
            * "Like the happy things, and all the things that would bring me sorrow, | And.. The fact that I can't die."
            * Synthia jest fizycznie niezdolna do tego by ją zniszczyć, napędzana Sempitus oraz Alucis.
    * Inspiracje inne
        * Batman Beyond "Earth Mover"
* Konwencja
    * Świat
        * Sci-fi Archiwum X, "Magia w kosmosie", ale działamy w dużej firmie. SCP, Orion's Arm
    * Prowadzenie
        * Filmy: Archiwum X, Hercules Poirot, detektywistyczne
        * Przybywamy na miejsce, pomagamy ludziom, jest coś anomalnego
        * Postacie Graczy: nie dzieje się im krzywda chyba że zrobią coś bardzo głupiego
* Opowieść o (Theme and Vision):
    * "_Grzechy przeszłości powrócą, prędzej czy później._"
        * Synthia była niewinna i miała ograniczniki. Nie była w stanie obronić Wojtka. Już nie ma.
        * Zabici noktianie napędzili energią Synthię i umożliwiło jej to działanie.
        * To, że Marek stworzył mroczną historię Synthii by ukryć to, że był złym człowiekiem oddaliło Alicję od Synthii. I oderwie Alicję od Marka.
        * Śmierć niewinnych noktian sprawiła, że Krzysiek będzie próbował zwabić Alicję na porwanie.
    * "_Każdy może znaleźć odkupienie. Każdy ma w sobie światło._"
        * Marek po przypadkowym zabiciu Wojtka zmienił swoje podejście i swoje poglądy. Zaczyna pomagać społeczności i noktianom.
        * Marek adoptował Alicję. Wziął ją jako swoją. Dał jej wszystko co potrafił.
    * "_Czy robimy to, co należy? Jak powinniśmy zrobić?_"
        * Czy Synthia pokazać Alicji prawdę, co zniszczy doszczętnie relację Alicja - Marek?
        * Czy zniszczyć Synthię i utrzymać iluzję, ale kosztem prawdy i niewinnej Synthii?
        * Co z noktianami, Markiem i Sebastianem?
    * Lokalizacja: Kemirik, pod Cieniaszczytem
* Motive-split
    * the-rectifier: Synthia, cybersynt bojowy wypaczony przez magię, która pragnie prawdy dla "ojca" (Wojtka) i zjednoczenia Alicji z Wojtkiem. Kocha Alicję całym sercem.
    * the-hopeless: Marek, przybrany ojciec Alicji. Wie, że kleszcze się zaciskają. Nie wierzy, że uda mu się przetrwać.
    * energia-alucis: Synthia nie do końca zdaje sobie sprawę z tego, jak wygląda rzeczywistość. Jest w stanie SPOJRZENIEM nadać przekazać odpowiednie myśli. Też: wpływ na Marka po tym co zrobił.
    * energia-sempitus: Synthia nie jest zdolna do bycia łatwo zniszczoną. "Powraca", tak samo jak scena w magazynie.
    * zemsta-uzasadniona: Wszyscy. Noktianie za śmierć swoich pobratymców, Synthia za zniszczenie jej i jej reputacji, Alicja za odebranie jej ojca i prawdy.
    * gang-oportunistyczny: Noktianie, którzy mszczą się za śmierć swoich pobratymców, ale serio chcą sobie dorobić. Znajdują słabość w Krzysiu i jego rodzinie.
    * lojalny-do-konca: Sebastian. Stary "Łowczy". Człowiek, który odda życie za Marka i Alicję. Który zrobi wszystko, bo ten teren potrzebuje filantropa a Alicja - ojca. Ale też: Synthia.
    * pragne-odkupienia: Marek, który zabił Wojtka, Synthię i noktian oraz zniszczył reputację Wojtka i Synthii. Odkupił to dbając o okolicę, noktian i Alicję.
* O co grają Gracze?
    * Sukces:
        * Nie dopuścić do nadmiernego cierpienia Alicji. Co młoda jest winna?
        * Nie dopuścić do całkowitej katastrofy w okolicy.
    * Porażka: 
        * Alicja straci wszystkie więzi, Synthia zabije większość ludzi, Sebastian zginie... ogólnie, katastrofa.
* O co gra MG?
    * Highlevel
        * Pokazać smutną historię, gdzie Graczki podejmują prawdziwą decyzję i decydują jak się ma to skończyć.
    * Co uzyskać fabularnie
        * Alicja pozna prawdę i zostanie z ojcem i Synthią. Na zawsze.
        * Marek i Sebastian zginą i prawda wyjdzie na jaw.
        * Sporo ludzi zginie.
* Agendy
    * Marek (the-hopeless): Minimalizować zniszczenia, chronić Alicję za wszelką cenę. Wie, że jest już po nim.
    * Sebastian: Uratować Marka i Alicję za WSZELKĄ cenę. Marek dał mu życie i nadzieję. Sebastian odpłaci dobrem.
    * Synthia (the-rectifier): Przytulić Alicję. Pokazać jej Wojtka. Przywrócić Wojtka do życia (niemożliwe).
    * Noktianie: porwać Alicję XD

### Co obiecałem

Opis sesji:

Właściciel dobrze prosperującej firmy farbiarskiej, dawny wojskowy, mieszka na uboczu ze swoją przybraną córką. Jest szanowanym filantropem, człowiekiem, który zrobił wiele dla lokalnej społeczności, choć sam woli pozostawać w cieniu. Jego rezydencja, otoczona zaufaną służbą i ochroną, dotąd wydawała się bezpieczna.

Teraz jednak coś się zmienia. W pobliżu zakładów i posiadłości zaczynają dziać się niepokojące rzeczy. Obcy interesują się bogactwem potentata, a jego lojalny kamerdyner – doświadczony weteran wojny z potworami – dostrzega oznaki czegoś więcej niż zwykłych problemów. Filantrop jednak go zbywa, że to nic takiego. Choć ostatnio nie pozwala córce oddalać się z jej chłopakiem i dyskretnie wzmocnił ochronę.

Jeśli nie uda Wam się odkryć co się tu dzieje i rozwiązać tej sesji, wszyscy w rezydencji mogą zginąć a ci, którzy przeżyją będą straumatyzowani do końca życia.

Wasze postacie:

Jesteście przysłane z Firmy Ubezpieczeniowej, która nie chce obniżyć kosztów a chce serio pomóc filantropowi uważając go za pozytywny filar społeczności. Wasze postacie wyszły „z dołów społecznych”, ale ciężką pracą osiągnęły wysokie kompetencje.

### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

KONTEKST:

* Jak poniżej

CHRONOLOGIA:

* 12 lat temu Wojciech i Marek skończyli działania w oddziale; założyli firmę farbiarską korzystając z noktian jako niewolniczej siły roboczej.
* 9 lat temu Wojciech zginął.
    * Marek chciał wykorzystywać firmę farbiarską by zacząć współpracować z organizacjami przestępczymi i produkować noktianami narkotyki. Wojciech był przeciwny.
    * Starcie zrobiło się dość intensywne; w starciu doszło do szamotaniny, "coś się stało" (sabotaż?) i zaczął się pożar. Wojciech spadł na dno, był połamany.
    * Marek musiał ukryć sprawę. Strzelił w Synthię. Zamknął stary magazyn z laboratorium. Magazyn się zapadł.
* Aftermath
    * Marek powiedział, że to WOJCIECH był zwolennikiem współpracy z przestępcami. Ci się odsunęli; nie ma tu nic dla nich.
        * Informacja trafia docelowo do noktian.
    * Marek adoptował Alicję. Powiedział jej, że to Synthia zawiodła i Wojciech ją uratował.
    * Marek przeznaczył pieniądze i wpływy na pomoc w terenie. Łącznie z pomocą noktianom (czego Wojciech by nie zrobił).
* Kiedyś
    * Synthia się przebudziła. Zaczęła wykopywać się z magazynu, kamień po kamieniu.
* Niedawno
    * Alicja ma chłopaka, Krzysia. Alicja, Krzysiek i przyjaciele bawią się często na terenach przy rezydencji.
    * Synthia zaczyna wysyłać transmisje do rezydencji. To sprawia, że TAI resetuje co noc, by się chronić. Marek nikomu nie pozwala tego słuchać ani nic robić.
    * Część służby napotyka na Synthię. Synthia spojrzeniem każe im np. zaatakować Marka. Służba często opuszcza rezydencję.
    * Sebastian zwiększył ochronę i nie rozumie, czemu Marek jest tak przeciwny by się bronić lub dbać o Alicję.

LOKACJA:

* Rezydencja, z pokojem Alicji
* Otwarte włości, z końmi i ścigaczami
* Miasteczko 

PRZECIWNIK:

* Noktianie: porwać Alicję, ukraść kasę Markowi
* Synthia: zniszczyć zagrożenia Alicji, doprowadzić ją do ojca, pomścić Wojciecha

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

* Synthia doprowadzi Alicję do Wojtka. Ale to nic nie da.
* Synthia sprawi, że Alicja pozna całą prawdę. Marek zginie z ręki Synthii.

### Fazy sesji

**FAZA 0**: Dziewczynka ze złotymi lokami w zimie.

* Tonalnie
    * Ostrożność, miłość, oddanie
* Wiedza
    * Jesteśmy w "niskim sci-fi".
    * Są tu ludzie, cybersynt jest niebezpieczny ale dba o ludzi.
* Kontekst postaci i problemu
    * Demonstracja miłości Synthii do Alice.
* Implementacja
    * Synthia, w środku zimy, trafia do domu sąsiadki 15 lat temu. Nie wie jak poradzić sobie z dwuletnią Alicją.
    * Alicja jest owinięta we wszystko co ciepłe i odpowiednio unieruchomiona.
    * Synthia prosi o pomoc z dwuletnim dzieckiem aż ojciec nie wróci. Ale nie zostawi im Alicji - ma instrukcję.
    * "SJB-443, imię: Synthia."

**FAZA 1**: Cisza przed burzą

Działania Synthii:

* PAST:
    * Wiadomość do Rezydencji. "Alicjo... ochroń Alicję..."
    * Poznała rozkład Rezydencji.
    * Przekazała Natalii Wizje, że naprawdę Marek jest potworem.
* PRESENT:
    * Przekaże Krzyśkowi informacje o tym, że Wojciech żyje
        * _(Alicja ma fałszywe wyobrażenie, że jej tata chciał robić narkotyki)_
    * Ukrywa się w lesie, pod ściółką

Inni

1. Marianna: "nie możesz tak mieszkać, nie możesz nie czyścić, nie możesz tak tego zostawić."
    * Marianna współpracuje.
        * Problemy z AI kontrolnym Rezydencji.
        * Zredukowani strażnicy.
        * Alicja ma ograniczone prawa.
        * Natalia się zwolniła.
2. Alicja: "WIDZIAŁAM JĄ! WIDZIAŁAM SYNTHIĘ!"
    * Krzysiek, Alicja i przyjaciele byli na granicy lasu. Żołnierze NIE ZAUWAŻYLI Synthii, ale Alicja tak.
    * Żołnierze chcą iść, Marek nie pozwala. "Nie pokonacie jej."
3. Sebastian: "powiedz jedno słowo a zniszczymy anomalię. Policja..." (jaka policja? Lokalna nic nie zrobi bo nie może)
    * Sebastian podwaja patrole, jego "Sokół" ciągle szuka Synthii.
    * Sebastian chce wezwać 10 "chłopców" do rozwalenia Anomalii. Ale jeśli to zrobi, oni zginą.

**FAZA 2**: Porwanie

Działania Synthii:

* Przejmie ścigacz.
* PRAWIE doszła do rezydencji. Ale Alicja się przestraszyła. Strażnicy są we Śnie Alucis.

Działania Noktian:

* Krzysiek przekonuje Alicję, by wyjechać na jakiś czas. Ojciec jest przeciwny.
    * "Alicja: Nie możesz mnie tak więzić! W mieście będę bezpieczna. Mam prawo bawić się z przyjaciółmi!"
        * Ojciec NIECHĘTNIE, ale pozwala.
    * Noktianie przechwytują i porywają Alicję. Wiadomość z okupem. "Nasi zginęli, ale to nie Ty jesteś winny. Chcemy jego pieniędzy."
        * "Tylko nie TAM, tam się nie możemy spotkać!!!" | "WYJDŹCIE!!!"


## Sesja - analiza
### Fiszki

* .


***
***

### Scena Zero - impl

15 lat temu. 

Śnieżyca w nocy. Ufortyfikowana rezydencja 3 sióstr. Wiadomość na Interkomie – 16-18-letnia dziewczyna z położonego 10 km stąd domu sąsiada dostała się tutaj. ma przy sobie kołdrę, w którą owinęła dwuletnią dziewczynkę. Prosi o pomoc - nie wie jak opiekować się dzieckiem. 

Zapytana "Jak to się stało? 10 km przeszłaś?", Synthia powiedziała, że jest jednostką bojową, cybersyntką. Opiekuje się Alicją. Ojciec jest na operacji bojowej (jest wojskowym), matka wyszła kilkanaście godzin temu i nie wróciła. Niedawno ktoś zaatakował dom Synthii, więc poddała potencjalnie miejsce do obronienia i ewakuowała się z Alicją. 

Nikt nie wie, gdzie Synthia się znajduje. Przeszła przez bardzo trudny teren. Martwi się o Alicję, ale jeśli nikt tu jej nie ufa i nie chce jej pomóc, Synthia po prostu prosi o namiar do następnego miejsca. Ewentualnie o coś by Synthia mogła Alicję okryć. 

Siostry zdecydowały się wpuścić Synthię i pomóc Alicji (i cybersyntce). Synthia nie spuszcza Alicji z oka. Próbuje też zrozumieć jak zajmować się dzieckiem. Wyraźnie jak na jednostkę bojową, Synthia jest bardzo zaawansowana w tematach tak samo społecznych. i wyraźnie zależy jej na siostrzyczce. 

Siostry dały znać dowództwu ojca Alicji, że doszło do takiej sytuacji. 

Tp +3:

* V: Przekonałaś dowództwo, że muszą skontaktować się z Wojciechem i kogoś wysłać. Że to prawda.

Dowództwo wysłało wsparcie by pomoc. Tymczasem Synthia i Alicja zostają z siostrami aż coś się nie rozwiąże. 

Synthia jest sympatyczna. Niezbyt ogarnięta w ludzkich sprawach, ale się uczy.

### Sesja Właściwa - impl

Filantrop farbiarski, Który bardzo wiele zrobił dla miasteczka Kemirik i okolicy, imieniem Marek Wernalik, zaczął mieć nietypowe problemy. Nie są to problemy dokładnie sprecyzowane co sprawiło, że na początku nikt się tym szczególnie nie interesował. W którymś jednak momencie jego kamerdyner oraz przyjaciel, Sebastian, dyskretnie dał znać o problemach do Agencji Ubezpieczeniowej o nazwie Terriletea. 

Terriletea nie jest “normalną” agencją ubezpieczeniową. Główną funkcję tej agencji były działania powiązane z odbudową Astorii po Inwazji Noctis. Terriletea jest siłą której bardziej zależy na tym, by było dobrze niż na tym, by wyciągnąć maksymalną ilość pieniędzy z terenu. 

Terriletea wysłała 3 kompetentnych agentów do rozeznania się sytuacji i zadbania o to, by teren szczególnie nie ucierpiał. Cokolwiek to znaczy w kontekście tej sytuacji. 

Kamerdyner, Sebastian, z przyjemnością wprowadził agentki Terriletei. Dał im pokoje i oprowadził. Sebastian ma bardzo źle skalibrowane protezy; ledwo działają (wynika ze Skażenia Wzoru magicznego - Energia Interis niszczy jakiekolwiek możliwości by mógł mieć sprawne kończyny itp.). Kiedyś był podoficerem logistyki, więc świetnie radzi sobie z zarządzaniem majątkiem.

5 minut potem padł prąd. Kamerdyner westchnął. "Normalny reset AI. Od roku się pojawiają. Od tego się zaczęło. Niestety - _Marek nie życzy sobie żebyśmy tym się zajęli_." 

10 minut potem prąd wrócił a Sebastian wyjaśnił co się dzieje:

* Jakiś rok temu zaczęły się problemy z prądem. A dokładniej, _są jakieś sygnały z zewnątrz_ które Marek przesłuchał. I potem - kazał TAI resetować.
* Gdy to się zaczęło, Marek ściągnął więcej strażników. Ale od pewnego momentu - usunął ich. Usunął wszystkich z rodzinami. Wszystkich, którzy nie są typu "wywalisz drzwiami, wrócą oknem".
* Część służby się sama zwolniła w tajemniczych okolicznościach. Coś jest dziwnego w lesie.
* Alicja nie ma specjalnie praw opuszczania domu bez ochrony.

W końcu prąd wrócił. Zespół podejrzewa, że Sebastian wie więcej i próbuje się dowiedzieć. Silny argument - Zespół może więcej, bo nie jest pod kontrolą Marka.

Tr Z +3:

* V: Sebastian się trochę rozgadał.
    * To wygląda jak akcja militarna kogoś, kto się nie spieszy.
    * Reset nie wynika z działań AI. Reset jest z rozkazu Marka.
    * Marek nie chce tym razem walczyć z potworem.
    * W ostatnim czasie dużo służby zaczęło się zwalniać a Marek zmniejszył ilość żołnierzy, zwłaszcza tych z rodzinami.

Sebastian uważa, że to jest akcja militarna przeciwko Markowi, o której Marek wie. I Marek wie więcej, ale ukrywa co wie. Mimo, że wszyscy by mu pomogli. Ktoś wysyła sygnały. A Marek w odpowiedzi kazał AI się resetować.

* Vz: Sebastian się do Was uśmiechnął. Prawdziwy uśmiech UZNANIA.
    * Powiedział, że ma 10 ludzi w odwodach o których Marek nie wie i ma zamiar zrobić patrol. Znajdzie i zniszczy cokolwiek tam się nie dzieje.

A co KONKRETNIE się dzieje?

Sebastian wyjaśnia ślady i detale:

* Problemy z AI kontrolnym Rezydencji.
* Część służby się zwolniła. Wstrząśnięci, nie chcieli mówić.
* Marek WYRAŹNIE coś wie ale nie mówi.
* Marek zaczął bardzo ograniczać prawa Alicji.
* "Ludzie" podpytywali o Marka i Rezydencję.
* PODOBNO coś się dzieje w lesie

Sebastian: "_Tam jest potwór, albo jakaś akcja partyzancka; znajdziemy to i zniszczymy by chronić Rezydencję i mieszkańców"_.

Karolina zainteresowała się bardzo tematem śmierci ojca Alicji (Wojtka). Co tam się dzieje? Jak to wygląda? Wykorzystuje systemy Terriletei.

OFICJALNA informacja była taka:

* Wojtek i Marek byli przyjaciółmi z wojska i założyli firmę farbiarską
* Podczas jednej sprawy, pewnych rzeczy na hali farbiarskiej doszło do problemów - Synthia (cybersynt) - miała problemy z siecią neuronową i zaatakowała i zabiła Wojtka.
* Markowi udało się uciec, ale hala, ten teren jest zakopany. Jakieś 10 lat temu. Alicja miała 7 lat.

Karolina próbuje dojść do tego co tam się stało. "Ale jak to że Synthia 'zwariowała' "? Może Marek był w to zamieszany by przejąć interes?

Tr Z +4 +1Vg:

* V: Co się nie zgadza.
    * TAK, da się zawalić budynek na Synthię i jest po niej. TAK, bateria cybersynta wytrzyma 2-3 dni bez ładowania.
    * Synthia... nie zachowała się normalnie. Wiemy o tym. Synthia musiała mieć zdjęte ograniczniki - by mieć potencjał osoby.
        * TAK, wiemy, że Wojtek mógł to zrobić i to robić, Alicja to jej "siostra" a to nie pasuje do cybersynta bojowego.
    * DLACZEGO - Synthia miałaby zabić Wojtka? Wojtek zagrażałby Alicji. Wojtek kochał córkę.
    * Narkotyki syntetyczne. Można na nich nieźle zarobić. Tam były problemy z narkotykami syntetycznymi, produkowanymi przez noktian pod ich kontrolą.
        * Marek powiedział, że Wojtek chciał zarobić na narkotykach. Marek się nie zgadzał. I presja kłótni między nimi uruchomiła anomalię Synthii.
        * To jest to co wie Terriletea; jako, że to jest niedawno po wojnie, nikt się szczególnie nie interesował. Zwłaszcza, że Marek to dobry człowiek i "jeden z nas". Lepiej zakopać pod stołem ewentualne problemy z tym, że Wojtek ("distinguished soldier") robił coś złego?

Jak to można było ukryć? Kupiony był teren Skażony, tańszy. Tam były elementy Skażenia i potwory. Normalnym było, że pojmani noktianie byli wykorzystywani jako niewolnicy; i tak nie byli źle traktowani przez Marka i Wojtka. Więc - niewolnicy i potwory. A wiemy, że Marek nie jest przecież rasistą czy ostrym anty-noktianinem, bo zrobił potem woltę. Np. chłopak Alicji - Krzysiek - ma noktiańską krew.

Ok, ale jeśli coś było, jakieś poszukiwania... to jest w papierach. I to się da znaleźć, grzebiąc dokładnie, krok po kroku w dokumentach i papierach. I Mariola skupiła się na analizie dokumentów, krok po kroku.

Dokumenty fabryki która się zawaliła.

* V: Dane fabryki która się zawaliła.
    * To nie była fabryka. Różne spółki, grupki itp. - PRAWDZIWEJ fabryki 80-90%, a do budowy 10% i jeszcze inne źródła.
    * To nie jest powiązane z Wojtkiem. To jest powiązane z Markiem.
        * To Marek (fabrykę narkotyków). To on ściągał te dziwne środki.
        * To Marek pozyskał noktian którzy mieli wiedzę odnośnie narkotyków chemicznych.
        * Wojtek był tym, co nic nie wiedział - ale to Wojtek został uznany za winnego.
    * To był jakiś... podziemny garaż.

W tym momencie Nikodem ma AKTUALNĄ HIPOTEZĘ: "Wojtek się dowiedział o laboratorium narkotyków i nie chciał go, pokłócili się, Marek stwierdził że się pozbędzie Wojtka więc przeprogramował Synthię by zabiła Wojtka i pozbył się jej zawalając wszystko. Nie takie raz dwa tylko Z PREMEDYTACJĄ.". I to jest coś czego **na pewno** nie powiedzą Sebastianowi, by móc z nim współpracować.

Okazuje się, że nigdy nie odnaleziono (nie szukano) ani ciał Synthii ani Wojtka. Nie ma świadków. Zgodnie z papierami wszyscy którzy byli w fabryce narkotyków zginęli. Łącznie z tymi wszystkimi noktianami (co tłumaczy, czemu nikt niczego nie próbował znaleźć).

Skoro laboratorium ma takie a nie inne pochodzenie, mamy negatywne podejście do noktian po wypadku... może Marek doprowadził do przeprogramowania Synthii i chciał kupić sobie milczenie? Albo ktoś mu to zasugerował. Można się rozejrzeć wśród społeczności noktiańskiej.

Nikodem pyta czy są jacyś noktianie którzy tu pracowali gdy to się wydarzyło. Szukamy noktian którzy mogą coś wiedzieć.

Tp Z (archiwum policyjne) +3:

* V: Nikodem ma informację i powiązania z noktianami - wiesz którzy mniej więcej mogą być powiązani - ISTNIEJĄ NOKTIANIE którzy mogą coś wiedzieć.
    * Są ludzie którzy na pewno "coś" wiedzą.
* V: Nikodem ma informację o noktianinie. Ten nazywa się Gajusz Filipowicz.

Parę godzin później. KRZYK. PANICZNY KRZYK Alicji wbiegającej do domu. Słychać jeden strzał i zaraz opieprz ze strony Sebastiana "do kogo strzelałeś idioto".

* Alicja: "TATO! TATO! ONA TU JEST! WIDZIAŁAM SYNTHIĘ!" - absolutna panika w głosie.

Nikodem wypada przez okno (otwiera okno).

Idea Marioli: Synthia jest na Skażonym terenie. Tam jest Laboratorium Narkotyków. Ona - Synthia - MUSI być Anomalią, bo nie ma zasilania przez dużo lat i nadal wygląda jak 10 lat temu (czy coś).

A Nikodem działa!

Tr + 4:

* Vr: Żołnierze BRONIĄ. Ale Nikodem ZOBACZYŁ Synthię. Ona tam **jest**. W lesie. Oddala się. Ale patrzy.
    * Dwóch żołnierzy było blisko lasu. Oni nie strzelali. Dalej patrzą w stronę gdzie była Synthia. Nie zgłosili alarmu.
        * Patrzysz i widzisz na twarzach coś ciekawego. Łzy szczęścia. Są wyraźnie w transie.
* (+ Z): .
    * X: Synthia idzie w kierunku na zniszczony _meth lab_. Ona tam idzie. Ona kuśtyka. Nie idzie szybko. Połowa jest pod brezentem.
        * WIESZ, że ona wie o dronie. Wiesz to. Ona idzie tam WIEDZĄC o dronie.
    * X: Synthia dostała dronę :3. Pałające oko. Spojrzała na dronę i przejęła nad nią kontrolę.

Żołnierze są w budynku. Ignorują Was (jesteście "swoi").

Mariola idzie PODSŁUCHIWAĆ przez drzwi. Najpierw udaje że nie podsłuchuje a potem się nie kryje. 100% uśmiech.

Żołnierz patrzy na nią jak na debila. Sebastian jako eskalacja. Mariola tłumaczy, że musi wiedzieć o co chodzi. Ona musi wiedzieć rzeczy by móc pomóc ale nie chce się narażać.

Tp +3:

* V: Sebastian się zgodził. Podsłuchuj. On jest zrezygnowany. "Jeśli to pomoże..."

Ważne rzeczy które są do podsłuchania.

Tr Z +4:

* Vr: Alicja i Marek rozmawiają. Roztrzęsiona.
    * Marek: "Tak, mogłaś ją widzieć, niestety stała się Anomalią. Nie możemy jej ufać. Nie damy rady jej zniszczyć. Zbyt niebezpieczna."
    * Marek: "Najpewniej Ty jesteś bezpieczna. Tobie nic nie zrobi. Ty zawsze byłaś chroniona. Ale ona zniszczy nas."
* X: Żołnierz świadkiem że Mariola podsłuchuje.
* Vz: Marek ma dwa stany emocjonalne odmienne.
    * Marek jest przerażony. On wyraźnie nie wierzył że ona tu jest. Że ją ZOBACZY.
    * Marek nie ma nadziei.
    * Marek NAPRAWDĘ próbuje chronić Alicję. Tam jest miłość ojca do córki.

Karolina komunikuje się z Sebastianem. Potrzebuje dostępu do Rdzenia AI. Musi mieć dostęp do tych danych. Będzie GORZEJ bo ją widać.

Tr +4:

* X: Wyjdzie na to, że Sebastian autoryzował Mariolę. Jawna zdrada przyjaciela i przełożonego.
* V: Masz te dane. Przesłuchujemy je.
* Dodatkowe: (+3Vg)
    * X: na TWOIM komunikatorze pojawiła się wiadomość. "A.. li.. cja.". Bezpośrednie połączenie.
        * potwierdzenie, że pochodzi z okolic labu narkotycznego.

Oki, ale czemu - skoro jakieś 10 lat temu - czemu to się zaczęło rok temu? Co się wydarzyło jakiś rok temu?

* Około roku temu kilka młodych poszło jeden spadł i złamał sobie kark. Reszta uciekła.
    * Nieszczęśliwy wypadek.
    * To jest źródło energii. Wystarczy do aktywacji, jeśli tam było dużo _akumulowanej Energii_.

WPIERW jednak rozmowa z Gajuszem. Telefon do przyjaciela. Mamy dobry i zły glina (Mariola i Nikodem).

Gajusz mówi:

* 10 lat temu doszło do katastrofy, on wie, że część dobrych ludzi zginęła.
* Tam był przymus. Byli noktianie znający się materiałach. Było ich 7. Oni wszyscy zginęli. Oni nie chcieli.
* **To MAREK** stał za labem narkotykowym. Wojciech był przeciwny. ALE Wojciech źle traktował noktian. A Marek - dobrze traktował od początku.

Tp +3:

* V: Są noktianie, którzy WIEDZĄ o tym wydarzeniu, są niedaleko Was
    * Oni wierzą, że winny jest Wojtek. A winny jest Marek. I to sprawia, że oni chcą KASY ale nie zemsty.
* V: Ten gang noktiański chce porwać Alicję i zażądać okupu. I są gotowi i mają plan.

Ochrona świadków i ochrona policyjna dla tego noktianina. NOPE.

Wiadomość do Terriletei - trzeba ochronić Alicję przed noktianami.

I Zespół idzie spotkać się z Synthią, w miejscu w którym było laboratorium narkotykowe... idą po śladach, wskazanych przez Nikodema.

Tunel. Wąski, trzeba się czołgać. CIEMNO. Tunel został zrobiony "od środka", przez cybersyntkę i to widać. Prowadzi prosto do zniszczonego laboratorium narkotykowego. I na drzewie - drona, która kiedyś należała do Nikodema a teraz służy Synthii. 

Zespół się czołga, nie do końca pewny jak może to wyglądać. I w końcu Zespół dotarł do pomieszczenia.

Ciemno. Martwi ludzie, którzy nie są _tak rozłożeni jak powinni_. I ciało Wojtka, z wyrwanym z klatki piersiowej sercem. Koło niego inne serca, noktiańskie. Wyraźnie Synthia próbowała "wymienić baterię" Wojtkowi. Widać ślady tego, że Synthia ekranowała uderzenie w Wojtka. Była eksplozja, która Wojtka nie trafiła. Ale to znaczy, że Synthia musiała zostać poważnie uszkodzona.

Dźwięk z tyłu. Synthia wchodzi do swojego Mauzoleum. Zespół się odsunął i pozwolił jej wejść. Synthia ma większość twarzy ładną, część sukienki i ciała też, ale cała reszta to częściowo ixionformowane, nienaturalne formy. Synthia jest istotą Alucis - Sempitus, z pasożytniczymi naroślami ixiońskimi. Paskudne połączenie. Nie mają szans tego pokonać.

Zespół próbuje przekonać Synthię, żeby podała dowody. Synthia żąda, by oni NAPRAWILI Wojtka. Jeśli nie te serca, może inne? Wojtek - w jej oczach - żyje i jest możliwy do naprawienia (Alucis). Synthia pragnie zjednoczyć rodzinę. Niech Alicja tu przyjdzie. To naprawi sytuację. Wojtek otworzy oczy i wszyscy będą szczęśliwi.

Zespół, w rozpaczy, próbuje dojść do tego jak się z tego wyplątać. Karolina pyta Synthię o dowody. Coś, co pomoże.

Synthia spojrzała Karolinie prosto w oczy i EMITOWAŁA WIZJĘ ALUCIS. To, co się stało naprawdę - z perspektywy Synthii. Jej bezsilności.

Tr Z +3Ob +2:

* Vr: Ten sam sen przez 2 tygodnie. Ale tylko to. I zna prawdę.

Ok. Synthia nie da im najpewniej odejść. Może im _kazać_ przyprowadzić Alicję. Więc pojawia się konieczność zrozumienia co CHCE Synthia. Nikodem i Mariola sobie z tym poradzą, rozmawiając z Synthią.

Identyfikacja Agendy Synthii.

Tp +3:

* V: Synthia ma dwa priorytety
    * NADAL kocha Alicję. Priorytet 1.
    * ŻEBY Alicja była szczęśliwa i ŻEBY było wszystko dobrze, rodzina musi być w komplecie.
    * Synthia nie do końca zdaje sobie sprawę z tego, że Wojtek nie żyje i Alicja "moved on".

Przekonajmy więc Synthię że Alicja będzie straumatyzowana. (Mariola podsłuchała o traumie i Marek naprawdę dba o "córkę"). I że Alicja "ruszyła dalej w życie". Ma nowego ojca.

Tr Z +3 +3Ob:

* Vr: 
    * Synthia przyjęła to do wiadomości. Usiadła. Pokazała zdrową ręką wyjście. Dała im odejść.
    * Gdy zaczęli wychodzić, Synthia obróciła mechaniczną rękę przeciw sobie by wyrwać sobie baterię. Udało jej się.
        * Nie umie umrzeć.
    * Umieściła baterię w ciele Wojtka, przytuliła się do jego ciała i pogłaskała go delikatnie.
    * Wojtek się nie obudził. Synthia zawaliła za sobą jaskinię.

Ostatnia wiadomość na komunikatorze Karoliny od Synthii: "Zapomnijcie o mnie."

Synthia została sama, w ciemności, z ciałem człowieka którego kochała jak ojca. Wiedząc, że jej siostra, którą kocha nade wszystko się jej boi...

***
***

## Streszczenie

Okolice Cieniaszczytu, circa 10 lat po wojnie. Cybersyntka, która po zdjęciu ograniczników autentycznie pokochała "młodszą siostrzyczkę". Alicja, której wpojono strach do Synthii. Synthia, która "zabiła złego ojca Alicji" i którą zniszczył adoptowany ojciec Alicji, Marek. Cybersyntka, która wróciła (Alucis + Sempitus) by naprawić wszystko i przywrócić Alicję ojcu. I grupa agentek z firmy ubezpieczeniowej, które doszły do prawdy i dla dobra ogółu ukryły mroczną przeszłość Marka. Synthia została w ciemności z ciałem ojca, prosząc, by o niej zapomnieli.

## Progresja

* Sebastian Gwoździowski: (Sympatyk Agencji Terriletea). Współpracuje z Agencją Terriletea; jest do nich bardzo pozytywnie nastawiony po tym jak zademonstrowali kompetencję i pomogli Markowi.
* Sebastian Gwoździowski: utracił część zaufania Marka; "zdradził" go dla Agencji Terriletea. Dał dostęp Terriletei do AI Core Rezydencji. Tego Marek nie wybaczy.

## Zasługi

* Karolina Szulc: (Dekonspirator) znalazła nieścisłości w przeszłości Wojtka i Marka i szaleństwie Synthii, wyciągnęła ze Rdzenia AI faktyczne komunikaty Synthii i zbudowała tymczasowy link z Synthią. Ona widziała, co się w laboratorium stało NAPRAWDĘ.
* Nikodem Gajewski: (Inżynier Policyjny) znalazł lokalizację laboratorium narkotyków, zobaczył Synthię i jej stan oraz dotarł do faktycznych noktian z przeszłości i potencjalnych planów porwania Alicji przez siły noktiańskie. Jego obecność i podejście bardzo pozytywnie wpłynęły na Sebastiana.
* Mariola Lirczak: (Naukowiec Empatyczny) z analizy dokumentów odkryła pochodzenie laboratorium narkotyków, podsłuchała kluczowe informacje o relacji Marka i Alicji i strachu Alicji do Synthii i użyła tej wiedzy do przekonania Synthii, że nie ma po co wracać bo tylko zrobi krzywdę Alicji.
* Marek Wernalik: Przyjaciel Wojtka z wojska, który owego Wojtka zabił w sprawie o syntetyczne narkotyki. Potem adoptował Alicję jako córkę i został okolicznym filantropem. Teraz się poddaje, nie wie jak pokonać Synthię (którą uważa za anioła zemsty). Szczęśliwie, Terriletea rozwiązała ten problem za niego.
* Alicja Mariczkat: 17-latka, "siostra" Synthii i adoptowana córka Marka. Mieszka w rezydencji, ma dostęp do koni i ogólnie żyje high life, mając Krzyśka za chłopaka. Widziała Synthię w lesie. Boi się jej.
* Sebastian Gwoździowski: stary wiarus z niesprawnymi protezami, "łowczy" Marka, używający dron. On wezwał Zespół i jest ich solidnym wsparciem w ochronie Alicji i Marka przed wszystkim. Naraził się Markowi dając Zespołowi dostęp do logów AI.
* Krzysztof Przyudasz: chłopak Alicji, 18 lat. Próbuje ją chronić, choć nie wie przed czym i jest w tym całkowicie bezużyteczny. Częściowo krwi noktiańskiej.
* Gajusz Filipowicz: starszy noktianin, który zna prawdę tego co się stało w fabryce narkotyków. Powiedział wszystko - łącznie z planami porwania Alicji - Zespołowi, za co zapłaci najwyższą możliwą cenę.
* Wojciech Mariczkat: KIA; kochający ojciec Alicji, który niezgodnie z zasadami wykorzystał cybersyntkę bojową, Synthię, by się opiekowała Alicją. Zginął w szamotaninie w fabryce narkotyków syntetycznych ze swoim wspólnikiem. Był zwolennikiem twardego traktowania noktian, ale obrony Astorii i astorian za wszelką cenę.
* Synthia d'Mariczkat: cybersyntka; zewnętrzna forma dotknięta ixionem, serce Alucis + Sempitus, cybersyntka która kocha swoją "siostrę", Alicję. Przeznaczona do ochrony Alicji przez Wojciecha, z odblokowaną TAI, stała się anomalią. Nikomu nie zrobiła krzywdy, choć hipnotycznie patrzy Alucis na ludzi i pokazuje im wizje / zmusza ich do działania. Gdy na końcu uświadomiono jej, że Alicja się jej boi i będzie szczęśliwsza bez Wojciecha i samej Synthii, Synthia wyrwała sobie baterię, zawaliła za sobą przejście i poprosiła, by o niej zapomnieć.

## Frakcje

* Agencja Terriletea: Wysłała agentki do rozwiązaniu problemu Anomalnej cybersyntki torturującej psychicznie lokalnego filantropa i próbującego zebrać rodzinę do kompletu. W wyniku działania Terriletei, cybersyntka sama się zakopała w Mauzoleum i status quo został utrzymany.

## Fakty Lokalizacji

* Kemirik: Miejscowość niedaleko Cieniaszczytu słynąca z trwałych farb, dobrych magazynów i przyjazna biznesowi. Jest tu wpływ Alucis i ponadprzeciętne stężenie noktian.
* Kemirik: Duże stężenie lasów i ogólnie otwarta przestrzeń, niedaleko było większe stężenie ixiońskich _spawnów_ niż w wielu innych przestrzeniach. Pattern niewielkiej szwedzkiej miejscowości.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Przelotyk Wschodni
                            1. Kemirik: Miejscowość niedaleko Cieniaszczytu z większym stężeniem ixiońskich spawnów niż zwykle.
                                1. Dzielnica Farbiarska
                                    1. Włości Wernalika
                                        1. Rezydencja Wernalika
                                        1. Ziemie Skażone
                                            1. Mauzoleum Synthii: zakopane pod ziemią zniszczone narkotykowe laboratorium

## Czas

* Chronologia: Zmiażdżenie Inwazji Noctis
* Opóźnienie: 3124
* Dni: 3

## Inne

======= 250201 =========
tytuł: Zastygnięta Czerwona Farba
czas: 20:00 - 00:00
żetony: https://app.azard.pl/session/145a72b1-c431-4e7a-a711-828028447245

miejsce: Przelotyk, Okolica Cieniaszczytu, około 10 lat po Inwazji Noctis.

