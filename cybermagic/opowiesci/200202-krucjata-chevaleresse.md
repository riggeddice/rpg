## Metadane

* title: "Krucjata Chevaleresse"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [191201 - Ukradziony entropik](191201-ukradziony-entropik)

### Chronologiczna

* [191201 - Ukradziony entropik](191201-ukradziony-entropik)

## Budowa sesji

### Stan aktualny

* Chevaleresse szuka siły. Szuka mocy. Dotarła do klubu eks-terminusa; tam rośnie w siłę.
* Jednocześnie Chevaleresse chce pomóc przyjaciółce w jej planach. Dołączyła do Liberatis.
* Chevaleresse uzbroiła Liberatis w sprzęt Alana do walki przed ciemiężycielami.
* Eks-Terminus próbuje wzmocnić swoje siły kosztem Liberatis.
* Chevaleresse wraz z przyjaciółką i siłą ognia Alana + Liberatis zdobywa od Grzymościa jednostki bojowe klasy Serratus i seksboty.

### Dark Future

* Liberatis Shall Fall (as slaves)
* Chevaleresse Shall Fall (go too far)
* Ofiary śmiertelne

### Pytania

1. 

### Dominujące uczucia

* 

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Pięknotka, jak to Pięknotka, skazana na raporty. Dostała nieformalny komunikat od Tukana - terminusa z którym się nie szanują szczególnie. Powiedział Pięknotce, że zgodnie z jego wiedzą, Alan nie wrócił - nadal jest zdjęty z akcji. Pięknotka chłodno potwierdziła. Tukan powiedział, że wczoraj doszło do ataku na jedną z ukrytych baz Grzymościa. I część sygnatur używanych broni wskazuje na broń wyprodukowaną przez Alana. Jedno wygląda na eksperymentalne działo strumieniowe. Tukan podejrzewa Pięknotkę - to coś czego ona by użyła by odwrócić uwagę od siebie. Pięknotka twierdzi, że to zdecydowanie nie ona i nie ma z tym nic wspólnego. Co więcej, Tukan powiedział, że w szpitalu w Jastrząbcu dwóch cywili poparzonych działem strumieniowym Alana. Pięknotka powiedziała, że nadal nic o tym nie wie. Tukan spytał - Pięknotka nie ma nic przeciw temu by on się temu przyjrzał? 

Pięknotka powiedziała Tukanowi, że ma większe szanse i możliwości dojść do sedna niż on (Tp). Niech jej to zostawi. Tukan się zgodził. Niechętnie, ale woli by Pięknotka to wzięła; on powie Grzymościowi, że za tym nie stoi Pustogor i będą poszukiwania w tej dziedzinie. Że Pustogor nic nie wie. Tukan powiedział Pięknotce jeszcze jedną rzecz - it was no fight. Baza składająca się z 9 ludzi i 2 magów nie miała żadnych szans. Everything got obliterated. Żadnych faktów, żadnych dowodów. TAI bazy zostało anihilowane. Ludzie niewiele widzieli...

Pięknotka jest zaskoczona. Jak to możliwe? Grzymość nie ma samych idiotów, choć intelektualiści rzadko wybierają przestępczą ścieżkę. Pięknotka poprosiła Tukana o jakieś dodatkowe informacje - ten przekazał co ma. Pięknotka widzi, że atak był zaplanowany przez świetnej klasy stratega. Nic dziwnego, że Tukan podejrzewał Alana. Tukan powiedział, że Grzymościowi wykradziono kilka maszyn klasy Serratus oraz kilka złożonych, ale nie uruchomionych seksbotów. Ta baza nie była militarna, tam się składało sprzęt. Serratusy były w konfiguracji 'pleasant pain'. Miały być strażnikami ważnych osób. Nie wiadomo też kto powiedział o istnieniu tajnej bazy...

Jeden z mafiozów był przesłuchany przez napastnika. Potem dostał terminus-grade neurotoksynę by zapomniał wszystko. Jako, że w chwili obecnej Grzymość wierzy, że to Pustogor - to nie próbował wejść w szczegóły.

Wysokopoziomowo, Pięknotka chce znaleźć punkt zaczepienia.

Pięknotka udała się do Kerainy, w Jastrzębcu. Niech jedną ręką Tukan uspokaja Grzymościa, drugą niech Keraina zniszczy kompleks Itaron.

WSTECZNIE, ponad tydzień temu, Pięknotka poprosiła Minerwę by ta wskrzesiła projekt 'Malictrix'. TAI, której funkcją jest niszczenie TAI oraz baz. Minerwa nie była przekonana, ale Pięknotka była przekonywująca. Kompleks Itaron jest zbyt niebezpieczny. Tak więc - Minerwa wskrzesiła 'Malictrix'. (Ex_Z_M_5EN: VVXXX). Skażony ixionem pasożyt TAI. V: skuteczna, V: utajona, X: malicious, X: nie da się jej kontrolować, X: szpital i kompleks muszą być zniszczone; cholerstwo się zagnieździ. 

AKTUALNIE, Keraina dostała od Pięknotki zadanie. Dostać się do szpitala, przesłuchać mafiozo i wprowadzić 'Malictrix' do Itaron. (Tr_Z_5EN: XVV). V: weszła i wyszła bezpiecznie, V: niezauważona, V: przesłuchanie mafiozo skuteczne chemikaliami, X: spada wina na Tukana, V: nie ma korelacji ani nic nie widać że Keraina x Pustogor x Malictrix; wszystko wyszło na Tukana jako wektor problemów (Malictrix nie jest skorelowana z niczym).

Pięknotka się naprawdę, naprawdę cieszy. Acz nie wie, jak strasznego potwora wpuściła Minerwa do Itaron.

Dane z przesłuchania mafioza:

* mafiozo nie wie dokładnie kto go przesłuchiwał
* mafiozo był pytany o to, dlaczego siły Grzymościa atakowały podziemne koncerty i co się dzieje z dziewczynami
* mafiozo nic o tym nie wie. To nie oni. Jest jeszcze inna siła
* mafiozo był pytany ogólnie o różnego rodzaju ataki i manewry, to był atak ODWETOWY ale nie trafili
* ok, były porywane dziewczyny, były rzeczy z tym związane - ale nie tu, nie tak; to zwykle chodziło o Cieniaszczyt
* mafiozo wie coś o jakimś 'partnerze' Grzymościa działającym niedaleko - Pięknotka powiązała to z tą zaginioną dziewczyną od Ataienne

Z tego wynika, że porwana dziewczyna, fanka Ataienne, była porwana NA ZLECENIE. To nie była kwestia wysłania jej do Cieniaszczytu. To było coś innego. To znaczy, że Grzymość tu był egzekutorem, ktoś z nim współpracował i komuś zależało na tej KONKRETNEJ dziewczynie. To zmienia postać rzeczy - tamta akcja nie jest zakończona.

Pięknotka przypomniała sobie - jest dwóch cywili poparzonych bronią Alana. To jest powiązanie albo z Tajemniczą Grupą (Liberatis) albo z Tajemniczą Siłą Porywającą Dziewczyny (eks-terminus). Ma na szczęście nagranie z akcji przeciwko bazie Grzymościa; przesłała Trzewniowi to nagranie. Niech Trzewń oceni jak wygląda sprawa (Ex_Z): V: zdaniem Trzewnia, operacją dowodzi TAI poziomu 3, wyżej niż Persefona. Czyli - Orbiter? Tak czy inaczej, TAI 3 stopnia to nieprawdopodobna siła, przekracza nawet Persefonę w Jastrzębcu.

Pięknotka bardzo, bardzo nie chce narażać Mariusza. Podziękowała mu bardzo. On jest gryzipiórkiem - nie powinien narażać się bardziej. Ten taktyk i hacker już jej bardzo pomógł a wejście w szkodę TAI poziomu 3... nie. Nie bez Minerwy.

Pięknotka wie, że tylko Minerwa i Talia są w stanie zrobić coś takiego. Udała się więc do Talii - nie podejrzewa Minerwy.

Pięknotka zadała Talii pytanie - czy w ostatnim czasie udostępniła komuś TAI 3 generacji. Talia się uśmiechnęła. To uśmiech typu sweet summer child. Zaczęła wyjaśniać:

* TAI 3 jest za duża np. na kompleks Itaron czy Tiamenat.
* TAI 3 dodatkowo nie jest możliwa do tak po prostu zbudowania nawet przez Talię. Musi mieć za dużo by nad tym pracować. Da się to obejść - adaptogenem w wypadku BIA czy ixionem czy innymi takimi siłami - ale nadal, problem "za duży". No i to nie jest pełne TAI 3.
* Nie, Talia nie budowała TAI 3 poziomu. Nie w ostatnich 5 latach.

Talia się zdziwiła - czy Pięknotka ma TAI 3 poziomu i problem z tym powiązany? Pięknotka spytała jak skonfundować TAI 3. Talia powiedziała, że nie do końca wie - zależy czego chce TAI 3. Pięknotka tego nie wie. Talia zaznaczyła - Pięknotka nie ma do czynienia z TAI 3 stopnia, nie typową. Nie ma miejsca. Barbakan wykryłby krążownik w zasięgu czy coś tego typu. Pięknotka powiedziała, że ma do czynienia z czymś atakującym mafię.

Talia powiedziała Pięknotce, że może spojrzeć i zobaczyć czy nie dowie się czegoś cennego i interesującego. Pięknotka poprosiła, by Talia była ostrożna. Talia będzie ostrożna ;-).

I teraz dopiero Pięknotka jest w kropce. O co tu chodzi.

Czas porozmawiać z Damianem Orionem.

* Damian powiedział, że nie mają technologii na TAI 3 z orbity.
* Damian nie ma pojęcia odnośnie działań żadnej TAI 3 Orbitera w okolicy; Keraina to wspomagana TAI 2 i nic nie wykryła.
* Damian spojrzy na sprawę; Pięknotka nalega, by przekazał informacje (Tr: V: Ataienne jest potencjalna, ma więcej niż 1 placówkę na Astorii. X: Keraina monitoruje Ataienne i jej działania)

Damian powiedział, że Keraina spojrzy na Ataienne - jak zachowuje się na planecie itp. Nie zabiera wsparcia Kerainy Pięknotce; po prostu dał jej dodatkowe polecenia. Niech Keraina monitoruje czy Ataienne nadal działa zgodnie z programowaniem. Jak Damian zauważył, nie ma wprawy w TAI 3+. Nie wie, jakie są efekty uboczne i ewolucyjne takich TAI.

Pięknotka się nie ucieszyła słysząc to, co usłyszała. Niech zatem Keraina pójdzie przepytać cywili porażonych bronią Alana.

Tr_Z_5E (VXXV): V: wchodzi i wychodzi całkowicie niezauważona, V: wyciąga to co się miała dowiedzieć, X: Malictrix nadwątla defensywy Kerainy (ale nic nie może), X: Ataienne doszła do tego kim są ci "cywile".

Co dowiedziała się Pięknotka od tych "cywili":

* Należą do ORGANIZACJI skupionej dookoła Autoklubu Piękna, pod Mariuszem Kardamaczem.
* Jest grupa która jest nastawiona przeciwko kulturze Astorii - skupiają się na złych rzeczach. Dlatego Kardamacz próbuje ich deprogramować.
* Oni są "good guys" walczącymi ze Skażeniem Pryzmatycznym ze strony grupy Liberatis.
* (Pięknotka wie) Mariusz był terminusem. Upadły terminus. Ma negatywną historię z Pięknotką - spanikował i JEGO dorwał kraloth.
* Całkiem niedawno Liberatis uzyskało upiorną siłę ognia. I genialnego taktyka. Kardamaczowcy dostali WPIERDOL. Gdyby nie eks-terminus, byliby anihilowani.

I Pięknotka doszła mniej więcej do zarysowanych frakcji. Kardamaczowcy, którzy chronią kulturę Astorii przeciwko Liberatis, którzy... robią coś innego. I którym najwyraźniej pomaga... Ataienne? To jest ten problematyczny fragment. Czemu Ataienne. Co ona z tego ma.

Pięknotka ruszyła do dokumentów. Co to za bronie Alana. Skąd się wzięły. Stwierdziła, że sama je znajdzie i sama do tego dojdzie - na wszelki wypadek. Pięknotka nie lubi pracy z dokumentami, ale zna Alana. Mają wspólną historię. I on ma bardzo idiosynkratyczny zapis broni. A Pięknotka to zna, bo jest najbliżej Alana ze wszystkich terminusów (poza Ksenią, o dziwo). (Tp: V). Pięknotka doszła do tego:

* Wiadomo, z którego składu to się wzięło. Zniknęło tydzień temu. Stare, dawno nie zmieniane kody Alana. Praktycznie grupa zdobyła broń od Alana.
* Tam nie było superciężkich broni; raczej mniej udane eksperymenty. Raczej mniej bezpieczne. Ktoś kto na ślepo by to brał zrobiłby sobie krzywdę.
* Wszystkie ślady wskazują na Chevaleresse. I to uzasadnia obecność Ataienne - one się przyjaźnią.
* Chevaleresse zna dobrze Alana. Nie musiała hackować. Wie, który sprzęt jest jak groźny.

Pięknotka doszła do tego o co chodzi. Wie kolejną rzecz. Chevaleresse nie wierzy w to, że ktokolwiek pomści Alana. Nie ma informacji, jakie są działania w tle. Ale Ataienne... ona może doprowadzić do egzekucji zabójcy. To na pewno skierowało Chevaleresse do Ataienne. A Ataienne szukała informacji o swojej zaginionej fance. I coś sprawiło, że obie połączyły siły przeciwko Mateuszowi Kardamaczowi...

Pięknotka zasugerowała Karli pomyślenie nad uruchomieniem własnej Eszary T3 w Pustogorze. Tak na wszelki wypadek.

## Streszczenie

Chevaleresse wykradła broń Alana i uzbroiła Liberitias, gdzie działa też Ataienne przeciw Kardamaczowi, Grzymościowi i krzywdzicielowi Alana. Pięknotka doszła do serca problemu i materii, acz potrzebowała wsparcia Orbitera - dokładniej, Kerainy. Dzięki Kerainie wprowadziła Malictrix do Kompleksu Itaran. Teraz to tylko kwestia czasu aż Grzymość zostanie zniszczony.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: rozplątuje upiorną sieć zarzuconą przez Ataienne i Chevaleresse przeciwko Grzymościowi, Kardamaczowi oraz krzywdzicielowi Alana. Zmartwiona niestabilnością Ataienne.
* Tomasz Tukan: chroni interesy mafii przeciw Pustogorowi. Ale nie chce szkody terminusów. Zwrócił się do Pięknotki; Malictrix spadnie na niego jako jego "zasługa". Nie ma za co.
* Mariusz Trzewń: nerd taktyki i gryzipiórek; doszedł do tego, że nagrania wskazują, że napastnikiem bazy Grzymościa jest TAI poziomu ponad 3. Pięknotce jest zimno.
* Minerwa Metalia: nieco zaniepokojona, ale sformowała dla Pięknotki anty-TAI klasy Malictrix do zniszczenia kompleksu Itaran. Poszło jej za dobrze.
* Talia Aegis: nie stoi za działaniami przeciw Pustogorowi ani Grzymościowi tym razem; wyjaśniła Pięknotce wymagania na TAI 3. Przyjrzy się "dziwnej TAI" w okolicy.
* Malictrix d'Itaran: ixioński pasożyt TAI, sformowana przez Minerwę do zniszczenia kompleksu Itaran. Wygra i wsadzi Grzymościa za kratki. Okrutna, poza kontrolą, pożera wszystko.
* Diana Tevalier: zrozpaczona sytuacją z Alanem, zrobiła coś niewłaściwego - uzbroiła jego bronią Liberitias oraz weszła w sojusz z Ataienne. Najpewniej to nie koniec problemów.
* Ataienne: TAI powyżej 3 stopnia. Działa w tle - taktyk oraz commander, ale niekoniecznie mistrzyni zdobywania informacji. Jedyna TAI 3.5 w okolicy Szczelińca. Sformowała 'Liberitas'
* Mateusz Kardamacz: zauważył pewne problemy z kulturą Liberitias i odpowiedział własną kulturą. Ma coś wspólnego z porywaniem dziewczyn? Niepokojące; do sprawdzenia.
* Keraina d'Orion: miragent płynnie poruszający się po patrolowanym obszarze szpitala w Jastrząbcu. Przesłuchuje wszystko. Korozja defensyw spowodowana przez Malictrix którą umieściła.
* Damian Orion: oddał Kerainę do pokonania Grzymościa; zmartwiony niestabilnością Ataienne. Oddelegował Kerainę do monitorowania Ataienne.
 
## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Jastrzębski
                            1. Kalbark
                                1. Escape Room Lustereczko: centrala Liberatis; tutaj dowodzi Ataienne grupą osób, zdobyła też seksboty i Serratusy.
                            1. Jastrząbiec, okolice
                                1. TechBunkier Sarrat: zdobyty przez Liberatis; kiedyś jedna z wielu baz Grzymościa, słabo broniona.
                                1. Klinika Iglica: perfekcyjnie chroniony i patrolowany szpital, po którym swobodnie porusza się miragent Orbitera.
                                    1. Kompleks Itaran: główna baza Grzymościa, zainfekowana dzięki Kerainie przez anty-TAI Malictrix.
                        1. Powiat Pustogorski
                            1. Zaczęstwo

## Czas

* Opóźnienie: 9
* Dni: 3
