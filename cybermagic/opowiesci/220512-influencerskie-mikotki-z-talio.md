## Metadane

* title: "Influencerskie Mikotki z Talio"
* threads: planetoidy-kazimierza
* gm: żółw
* players: kić, wikimet, fox

## Kontynuacja
### Kampanijna

* [220512 - Influencerskie Mikotki z Talio](220512-influencerskie-mikotki-z-talio)

### Chronologiczna

* [220329 - Młodociani i pirat na Królowej](220329-mlodociani-i-pirat-na-krolowej)

## Plan sesji
### Co się wydarzyło
#### Układ Talio

* Aspekty:
    * Lodowe Tunele
    * Żywność dominująca to wodorosty zwane 'wodnym ryżem'. I grzyby.
    * Bardzo mocno monitorowany, dużo mechanizmów
    * Obecny archeotech i anomalne technologie
* Struktura:
    * Populacja: 2300
        * Operations & Administration: 210
        * Extraction & Manufacturing: 641
        * Medical: 83
        * Science: 11
        * Maintenance: 132
        * Leisure: 211
        * Social Services: 21
        * Transportation: 122
        * Training: 17
        * Life Support: 27
        * Computer & TAI & Communications: 48
        * Engineering: 118
        * Supply: 133
        * Security: 45
    * Bogactwo: wysokie, stechnicyzowana
    * Eksport: baterie energetyczne, wodny ryż, lód / woda, substraty asteroidowe
    * Import: luxuries, fun, robots, people, technologies
    * Manufacturing: baterie, jedzenie, lód / woda
    * Kultura: 
        * dominanta: kult technologii, kult ciężkiej pracy, raczej utylitarnie, "nudne miejsce", "wszystko da się naprawić"
        * wyjątek: Ogniki; korzystają z korzystnych warunków Talio i z wysokiego stechnicyzowania, anarchiści
    * Rozrywki
        * siłownie, arena (walki robotów)
        * Restauracja 'Prawdziwek'
        * przede wszystkim virt
    * Zasilanie
        * elektrownie termojądrowe chłodzone lodem / wodą
    * Żywność
        * 'wodny ryż', konkretne miejsca gdzie to zielsko rośnie
        * woda -> konwertery
    * Life support => archeotech + w głównych miejscach dominujące nowoczesne maszyny
    * Comms => okablowanie + virt central
    * TAI => bardzo przeładowana Hestia, która nie ma mocy obliczeniowej na wszystko
    * Starport => large
    * Infrastructure => high-tech, decaying oraz osobno archeotech. Oraz sporo robotów wspierających.
    * Access & Security => sieć sensorów, spiętych w Hestii

#### Co się stało

### Co się stanie
#### Bezpośrednio przed

* Jeden z Ogników, fan Mikotek, wysłał wiadomość do Aurusa. Podczas występu na arenie Ogniki zaatakują i porwą Mikotki
    * w rzeczywistości celem jest porwanie tylko Mirelli - Erwin chce ją zjednoczyć z ojcem
* Tytus chce zrobić uderzenie wyprzedzające; nie ma pojęcia, czemu sensory i systemy nie widzą Ogników. Przecież Hestia powinna dać radę!
* TYMCZASEM Mirella chce skorzystać z obecności Elwiry i przekonać ją by ich zabrała z Talio gdzieś do szerokiej galaktyki

#### Czego chcą siły

* Mirella (socjopatka): expand. Take power. Move away where music is better.
* Aurus (mikotki): wesprzeć Talio. Wesprzeć Mikotki. Towar eksportowy - mikrovirt.
* Petro (MONSTER): "uratować" Mirellę. Zniszczyć Ogniki. Nauczyć Karę śpiewać.
* Tytus (security): zmiażdżyć Ogniki, trzymać kontrolę nad Talio
* Nicola (engineering): odkryć plan Tytusa na zniszczenie Mikotek; ochronić Ogniki
* Erwin (ognik): przywrócić Mirellę ojcu, zatrzymać RAMPAGE Tytusa, nie uszkodzić siostry

#### Dark Future

* Mirella Karczak, główna mikotka, zostanie Master Influencer
    * I się rozprzestrzeni na cały świat, opuści Talio.
    * I podporządkuje sobie Kamila
* Aurus Czarmadon, "ojciec" mikotek zginie
* Wszystkie Ogniki zginą
* Prawda nigdy nie wyjdzie na jaw

### Sukces graczy

* Decyzja co z Mirellą

## Sesja właściwa
### Scena Zero - impl

Elwira podchodzi do starportu Talio swoim Statkiem Widmem. Tam jest młody szarogęszący się oficer który nie chce, by Elwira po prostu mogła lądować. Chce ją przenieść gdzieś by czekała w jakieś nieważne miejsce i by grzecznie czekała. Wyraźnie nie chce mu się robić swojej roboty.

Elwira próbuje dowiedzieć się czemu on jest tak nieelegancki. O co mu chodzi. Grzecznie ale zdecydowanie. Czy wszystko dobrze. Ma papiery w porządku.

TrZ+3:

* X: "a dlaczego arystokratka rodu Piscernik tu przylatuje" - sceptyczny. Elwira: "no, jestem tu na zwiedzanie. Nie muszę siedzieć przy biurku."
* V: oficer jest skonfundowany, przekazał głos dowódcy - Tytusowi. Tytus jest bardziej chętny.
* V: Tytus po krótkiej rozmowie pozwolił na lądowanie na "zwykłym" pasie. Elwira "nic się nie stało, arystokracja powinna czuć granice".

PÓŁ ROKU WCZEŚNIEJ

Lodowy tunel planetoidy Talio. Kara biegnie sprawdzić czemu Jacek krzyczy. Tam - naćpany Ognik co chce Jackowi zrobić krzywdę. Kara krzyczy i prowokuje - po czym zwiewa. Prowokuje, po śliskiej posadzce. 

TrZ+5O+3

* X: jesteś szybka, ale on jest większy. Ma wprawę. Odciął Karę.
* V: Kara wślizgnęła się gdzieś gdzie on nie wejdzie. 
* X: Jacek próbuje uciec, ale koleś wyjął broń. Wycelował w Jacka i Jacek ma paraliż.

SZCZUR ZESKAKUJE Z RAMIENIA KARY NA GŁOWĘ KOLESIA BY GO ODCIĄGNĄĆ W STRONĘ ZŁA KTÓRE WYCZUŁ! Zło ma zapach gnijącego ciała i metalu.

* Xz: Koleś przestraszył się i strzelił w Jacka. Jacek jest ranny. Krzyczy.
* V: Koleś się wkurzył i leci za szczurem. KRZYCZY! ZŁO GO ZŁAPAŁO.

Kara biegnie do Jacka. Ciągnie. Jacek krzyczy. Szczur wraca na ramię. (Tymczasem ostatni komponent - organiczny - został dostarczony. Nanomaszyny archeotech są w stanie zregenerować eks-ojca Mireli. Jakoś. Więc potwór się budzi - nie ma do końca swojego ciała, jest częściowo mechaniczny częściowo organiczny, niewiele pamięta ale słyszy głos dziewczyny... córki?)

Chrapliwy, bulgoczący głos... "Mirela..?" KARA KRZYCZY DO JACKA CHODŹ! 

Kara chce, by byt GADAŁ i chce wyciągnąć Jacka stąd. Ewakuacja. Powiedziała, że nie jest Mirelą.

TrZ+3:

* V: Udało Karze się odciągnąć Jacka...
* V: Kara jest w stanie ciągnąć Jacka i wołać o pomoc i w końcu się uda. Nikt im nie wierzy.

(Potwór orientuje się że to nie ona. Orientuje się że coś się stało. Jeszcze niewiele pamięta, ale nanitki pracują. Nowy cel - znaleźć i uratować córkę.)

### Sesja właściwa - impl

TERAZ

Po lądowaniu wystawiono bal, ucztę na cześć Elwiry. Czemu odwracają jej uwagę? Czemu tyle uwagi dla niej? Elwira podsłuchuje.

TrZ+3:

* V: "myślisz, że jej reputacja jest prawdziwa?" "jest detektywem" | "podobno rozwiązała zagadkę OSOBIŚCIE"
* V: Elwira się dowiaduje - dwie sprawy: prywatny problem managera Mikotek i problem Tytusa - ten jest przekonany że ktoś ukrywa Ogniki

Elwira podchodzi do Aurusa, managera Mikotek. Pyta co się dzieje i jak może pomóc. 

* (+2V) V: Aurus jest zaskoczony. Bierze do gabinetu. Tłumaczy, że są problemy

Do gabinetu wpada Mirela "czy pozwolisz nam odlecieć ze sobą?" Aurus się nie zgadza. Miejsce Mireli jest na Talio - tu jest jej dom, tu może pomóc. A w ogóle niech nie przeszkadza dorosłym.

* X: Coś MIGNĘŁO w Mireli, w jej charakterze. Smutne spojrzenie Mireli. Nie widać psychopatki. Mirela odesłana do pokoju.

Aurus puścił nagranie Ognika wysłane anonimowo - "ktoś chce porwać Mikotki, sam byłem kiedyś ojcem." I to Aurusa martwi. Zbliża się wystąpienie Mikotek i ważne jest to, by nic złego się nie stało z influencerami...

ELWIRA IDZIE POZNAĆ MIKOTKI.

-> Kara teraz.

Od pewnego czasu Ogniki znikają. Wiadomo, że Tytus to sadysta i idiota. Wiadomo, że koleś robi polowania na ludzi. Antonina chce się spotkać z Karą. Jacek tam jest. Nie chce z nikim rozmawiać. Jacek odmawia zgaszenia światła i nic nie mówi "wszyscy jesteśmy zgubieni".

Kara przemknęła się do skrzydła medycznego. Jacek desperacko próbuje ją przekonać, że nic się nie dzieje, nic nie widział.

Kara konfliktuje Jacka - ExZ+4:

* V: "powiedział, że byłem z tym co śpiewasz więc mnie nie zabije ale nie mogę Ci powiedzieć nie mogę". "Jacek jak mam chronić jak nie wiem przed czym"
* (+2Vg) Vz: był z Robertem i Robert nie żyje. I potwór pozwolił mu odejść. I zabija Ogniki. I dlatego nie gaszę światła. 
* (+2Vg -1X) X: przychodzi ranny strażnik do skrzydła medyczne.
* V: Jacek zdeutramatyzowany. ZROZUMIAŁ. Potwór nie przyjdzie bo mógł go zjeść wcześniej.

Szczur się chowa pod prześcieradła i szczurzy i węszy - gdzie jest źródło ZŁA. Bo czuje ten sam zapach co wtedy, gdy Potwór się obudził...

ExZ+3+2Vg:

* V: Szczur ma bezpieczną pozycję.
* X: Strażnik dostrzegł i będzie polował na szczura.
* X: Antonina "jak mogłaś przynieść SZCZURA" a szczur się bawi w chowanego.
* X: SZCZUR SIĘ EWAKUUJE.
* V: Szczur znalazł problem - robot sprzątający. Tam jest ludzkie OKO. Skażone.
    * szczur OBSIKAŁ robota, by zmusić ludzi do znalezienia tego oka. Bo dezynfekcja itp. Potem - zwiewa.
* (+2Vg) X: Tytus aresztuje Karę i Jacka w zamieszaniu.

TYMCZASEM ELWIRA.

Tytus zaprowadził Elwirę do "aresztu" gdzie są dwa DZIECIAKI. 

* "GDYBY ELWIRA BYŁA TAK DOBRA TO BY WIEDZIAŁA CO TU SIĘ DZIEJE!"
* Tytus jest skonfundowany
* "Dam wam ciepłe jedzenie jeśli powiesz mi co się dzieje"
* "NIE WYDAMY NASZYCH LUDZI!"
* "Co się dzieje? Czemu wszystko kręci się wokół waszej grupy? Jesteście groźni?"
* "ZADAJESZ SIĘ Z POTWOREM! ZABIJA NASZYCH LUDZI! TORTURUJE ICH! WYSYŁALI IM NAGRANIA! NASI LUDZIE NIE WRACAJĄ! Wiadomo, że nie wracają."
* "Wiecie gdzie trafiają ci co znikają?"
* "PEWNIE DO LOCHÓW NIE WIEM GDZIE ICH CHOWA!"
* "Zapytaj go trzyma się z nim"
* "Myślisz że sprawca Ci wyjaśni?"
* "Nie bo trzymasz się z potworem"

Jacek w końcu Jacek powiedział jak wygląda sytuacja (przekupiony wodorostami na ciepło z odrobiną soli):

* Taliaci to pracowici nudni ludzie
* Ogniki to pracowici ciekawi ludzie, chcą być poza obszarem wpływów Tytusa. To wagabundzi z całej okolicy.

...

* Kara, pomożesz mi?
* W czym niby?
* Zobaczenie pełnej perspektywy.
* Jak nas stąd wyprowadzisz, możemy porozmawiać
* A nie uciekniesz?
* Skąd mam mieć pewność że nas wypuścicie?
* Mogę dać Ci jedzenie, ubranie, ugoszczę na moim statku

LUKSUS ZA POMOC!

Elwira -> Tytus, że dzieciaki trzeba uwolnić i umyć. Tytus zatrzymuje Elwirę, wpada jak jeszcze są dzieciaki. Powiedział, że ten robot sprzątający ma części bio i mecha. Jest przestraszony. Jacek BĘDZIE WSPÓŁPRACOWAŁ słysząc to - to znaczy, że Tytus nie jest powiązany z potworem. Kara nieco bardziej sceptyczna, ale też może pomóc... bo Jacek. Elwira rozkłada szmaciany dywan i każe im się umyć na statku...

Jacek jest nieszczęśliwy. WYKĄPANY PIES. A Kara jest naburmuszona, ale czysta. Elwira "prędzej czy później będą wyglądały jak wcześniej". Elwira dała Karze w miarę wygodną sukienkę z cekinami. Falbanki. Kara "myślisz że się da przerobić bym mogła w tym występować"? "I TAK CI NIC NIE POWIEM O NASZYCH LUDZIACH ZABIJECIE ICH!" ale z uśmiechem i radością bo strój. Elwira "nie chcę im nic robić".

Kara tłumaczy Elwirze o potworze. Potwór jest groźny.

* Pół roku temu pojawił się po raz pierwszy
* Jacek ostatnio go natknął i przyjaciela, Roberta rozszarpał.
* Nie wiem jak wygląda, duży, straszny, mięsny...
* Zabija bo..? Jest potworem.
* Zabija Ogniki
* Potwór puścił Jacka bo widział go z Karą
* Kara nie widziała potwora, ale go słyszała
* "Tacy jak Ty rzadko tu przylatują. Dziwnie mówisz"

Elwira + Kara -> Mikotki

Elwira przepytuje Kamila o sytuację

* X: Kamil zaczął dobrze -> Kara. Myśli że Kara to arystokratka XD. "JA TEŻ ŚPIEWAM" Zaprezentuję. I śpiewają razem. "NADAJECIE SIĘ IDEALNIE DO NAGRANIA PŁYTY"
* V: Kamil o planach Mireli - wylecieć, sławni i bogaci, jest kolizja Aurus - Mirela. Aurus chce iść po linii "pieniądze dla planetoidy". Mirela Kamila zdominowała.
* (+1Vg) Vz: Mirela obsesyjnie buduje najlepsze Mikotki w galaktyce. Mirela NIE jest siostrą. Ojciec Mireli zginął w niefortunnych okolicznościach jakieś 2 lata temu.

Elwira + Kara -> Mirela

Elwira ma teorię, że Kara ma powiązania z potworem. Może zrobić krzywdę. Czy Mirela nie ma skłonności psychopatycznych?

ExZ+2

* X: maska grzecznej, ułożonej dziewczynki
* +(3Vg): V: "nie ma potworów bo nie ma magii". Ona coś wie. Ona coś zrobiła. NIE MOŻE BYĆ POTWORÓW.
* X: Ta maska jest nie do wyczucia dla innych. Ona jest za dobra. Nie dba o nikogo, nie dba o nic. Ma tylko cele.
* +(2Vg): Vz: "ja mam lepszą sukienkę od Ciebie". "Kara to Ognik, śpi ze szczurem. A Twoja maska nie jest doskonała". Mirela powiedziała, że to jej były ojciec. Ograniczał ją więc musiał zginąć. Aurus teraz ją ogranicza. Ona nie przywołała potwora. Ona nie wie nic o potworze. Wie, że to jej ojciec.
* X: Nikt nie uwierzy w nic złego na temat Mireli.

Jest compel na Elwirze. Wywieźć Mirelę.

Elwira + Kara szukają potwora. Znalazły Szczura. Szczur w końcu przyszedł. "Szukaj potwór". 

TrZ+3+2Vg:

* Vz: Szczur znalazł potwór.
* X: Potwór nie da się zajść.
* V: Jest opcja wycofania się.
* X: Potwór zauważył szczur.
* V: Szczur zauważył potwora i nie dał się trafić. potwór: "Nie jestem głodny. Zmykaj"

Ok, wiemy gdzie jest ten potwór. Potwór jest za ścianą. Elwira rzuciła okruchem lodu w drzwi by zwrócić uwagę potwora. Potwór wyrzucił pojedynczą czaszkę Roberta. Czaszka wygląda STRASZNIE. Kara i Elwira wieją. Szczur chce kawałek czaszki do szpitala...

* V: SZCZUROWI SIĘ UDAŁO!

Elwira prowadzi Mirelę do potwora. I zobaczymy co dalej...

(Co dalej? Mirela wypłakała, że za wszystkim stał Aurus i on ją krzywdzi. Doszło do tego, że potwór zabił Aurusa i został zniszczony przez siły Tytusa. A Mirela i zrozpaczony Kamil opuścili Talio i zaczęli swoją nową karierę wśród gwiazd...)

(co się naprawdę stało?

* Mirela chciała być influencerką
* Ojciec nie chciał by była, chciał by uczciwie pracowała
* Mirela powiedziała że on ją krzywdzi. "Zabili" go. Wrzucili gdzieś. Nanitki z archeotech go utrzymały przy życiu
* Gdy Kara / szczur wprowadzili naćpanego Ognika w tamten obszar, nanitki miały tkanki i przywróciły ojca do życia
* Ojciec chce chronić Mirelę, nie wiedząc, że ona wszystko zaaranżowała...
)

## Streszczenie

Elwira Piscernik przybyła na Talio w odwiedziny i zaczęła rozwiązywać tajemniczą sprawę znikających Ogników i potwora polującego na Mirellę z Mikotek. Pośrodku starć Taliatów z Ognikami okazuje się, że Mirela doprowadziła do śmierci swego ojca który powrócił jako potwór. Elwira ją wywiozła z Talio, wraz z zabraniem stąd Kary i przekazaniem jej na Asimear.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Elwira Piscernik: detektyw, arystokratka i neurokomandoska. Bogata Piscerniczka, która trochę cosplayuje detektywa. Udało jej się dojść do problemów na Talio - jest tam nanitkowy potwór - i doprowadziła do spotkania Mireli z jej ojcem. Osłabiła ostry konflikt między Ognikami i Taliatami, po czym wywiozła Mikotki z Talio.
* Kara Prazdnik: 13 lat, ratując przyjaciela (Jacka) przed naćpanym Ognikiem przypadkiem reanimowała ojca Mireli; ma inteligentnego szczura. Doszła do tego, że to Mirela jest przyczyną zła na Talio. Lojalna Ognikom, zadziorna, nic nie powie. Chce śpiewać!
* Jacek Ożgor: 14 lat, przyjaciel Kary na Talio. Wpierw Kara go uratowała gdy brutalny Ognik chciał go skrzywdzić (co uruchomiło Potwora). Potem widział potwora i zamknął się w skrzydle medycznym. Powiedział Elwirze wszystko za luksusy.
* Mirella Czarmadon: mikotka; jej nieskończony głód sławy i władzy sprawił, że doprowadziła kłamstwami do śmierci swego ojca a POTEM do śmierci swego przybranego ojca używając nanitkowego echa jej własnego ojca. Przekonała Elwirę, by ta wzięła ją w świat. By mogła zostać influencerką Talio na całą Domenę Ukojenia.
* Kamil Czarmadon: mikotek; cichy śpiewak i influencer, który dał się zdominować Mireli. Chciałby mieć dobre życie i pomagać innym; jest bardzo pasywnym elementem Mikotek.
* Aurus Czarmadon: manager mikotek; po śmierci ojca Mireli ją zaadoptował i chce zrobić z niej gwiazdę. Dał jej i Kamilowi wszystko. Mirela napuściła nań potwora (kiedyś: ojca) i tak skończyła się kariera managera... KIA.
* Tytus Ramkon: dowódca ochrony Talio; udaje brutalnego psychopatę; w praktyce jest łagodniejszy niż się zdaje po prostu ta strategia działa. Zaangażował Elwirę do rozwiązania "kto chce porwać mikotki" i "czemu znikają Ogniki".
* Antonina Terkin: medical Talio; silnie i dyskretnie współpracuje z Ognikami przeciwko ochronie Talio i cicho podpowiada Karze, że Jacek jest w skrzydle medycznym i nie chce wyjść. Wściekła na Karę, bo ta wzięła szczura do medical.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Pas Teliriański
                1. Planetoidy Kazimierza
                    1. Domena Ukojenia
                        1. Planetoida Talio: główna planetoida dostarczająca wodę i baterie energii do Domeny Ukojenia. Lodowa z archeotechem, bardzo zimna. Utylitarne, nudne miejsce z kultem ciężkiej pracy gdzie pojawiła się szansa na stworzenie nowej influencerki - i pojawił się nanitkowy potwór. Dwie kultury - Taliaci i Ogniki. ~2300 osób.

## Czas

* Opóźnienie: -1591
* Dni: 6

## Konflikty

* 1 - Młody szarogęszący się oficer nie chce, by Elwira po prostu mogła lądować na Talio. Elwira przekonuje, że może.
    * TrZ+3
    * XVV: Elwira mówi że zwiedza i musiała się wyspowiadać z celów, dostała namiar na Tytusa i Tytus pozwolił lądować
* 2 - Lodowy tunel planetoidy Talio. Kara biegnie sprawdzić czemu Jacek krzyczy. Kara krzyczy i prowokuje okrutnika - po czym zwiewa. Prowokuje, po śliskiej posadzce. 
    * TrZ+5O+3
    * XVX: szybka, ale on jest większy. Odciął Karę. Kara się wślizgnęła w zaułek. Koleś wyjął broń w stronę Jacka.
    * XzV: Koleś postrzelił Jacka a szczur tauntował kolesia by ten pobiegł w stronę ZŁA.
* 3 - Kara chce, by byt GADAŁ i chce wyciągnąć Jacka stąd. Ewakuacja. Powiedziała, że nie jest Mirelą.
    * TrZ+3
    * VV: Udało się Karze odciągnąć Jacka do cywilizacji i jakkolwiek nikt im nie wierzy to jednak wszyscy są bezpieczni.
* 4 - Wystawiono bal, ucztę na cześć Elwiry. Czemu odwracają jej uwagę? Elwira podsłuchuje.
    * TrZ+3
    * VVVX: wierzą w epickość Elwiry i mają nadzieję że pomoże; Elwira wzięła Aurusa na bok i dowiedziała się o próbach porwania Mikotek. Nie dojrzała psychopatki w Mireli.
* 5 - Antonina chce się spotkać z Karą. Jacek tam jest. Nie chce z nikim rozmawiać. Jacek odmawia zgaszenia światła i nic nie mówi "wszyscy jesteśmy zgubieni". Jacek próbuje ją przekonać NIC SIĘ NIE DZIEJE. Kara konfliktuje.
    * ExZ+4
    * VVzXV: Jacek straumatyzowany bo WIDZIAŁ potwora który chciał by Kara śpiewała. Potwór dał mu odejść i zabija Ogniki. Do skrzydła medycznego przychodzi ranny strażnik. Jacek ZROZUMIAŁ - stwór MÓGŁ zabić a nie chciał.
* 6 - Szczur się chowa pod prześcieradła i szczurzy i węszy - gdzie jest źródło ZŁA.
    * ExZ+3+2Vg
    * VXXXVX: Szczur ma bezpieczną pozycję, ale widzi go strażnik. Antonina ZŁA na Karę za przyniesienie szczura który wieje. Szczur zna zagrożenie - robot sprzątający. Więc obsikał. Tytus aresztuje Jacka i Karę. 
* 7 - Elwira + Kara -> Mikotki. Jak wygląda sytuacja?
    * TrZ+2
    * XVVz: Kamil myśli że Kara to arystokratka i pośpiewali. CRIIINGE. Kamil opowiedział o planach Mireli i widać, że Mirela ma obsesję na punkcie najlepszych Mikotek w okolicy.
* 8 - Elwira ma teorię, że Kara ma powiązania z potworem. Może zrobić krzywdę. A czy Mirela dla odmiany nie ma skłonności psychopatycznych?
    * ExZ+2
    * XVXVzX: perfekcyjna maska słodkiej dziewczynki, Mirela coś wie, nikt nie wierzy w zło Mireli, Mirela powiedziała o "byłym" ojcu i że to chyba on. Elwira ma compel - wywieźć Mirelę na Asimear.
* 9 - Elwira + Kara szukają potwora. Znalazły Szczura. Szczur w końcu przyszedł. "Szukaj potwór". 
    * TrZ+3+2Vg
    * VzXVXV: szczur się dobrze pozycjonował i znalazł potwora w dobry sposób. Potwór odstraszył czaszką Roberta Elwirę i Karę. Szczur kawałek czaszki do szpitala...

## Kto tu jest

### Postacie graczy

* Elwira Piscernik (objęta przez Wiki)
    * rola: 
        * DETEKTYW: znajdowanie śladów, tropienie oraz orientowanie się co i jak. Przesłuchiwanie i lokalizowanie co nie działa.
        * NEUROSPRZĘŻONY KOMANDOS: perfekcyjna pamięć, niesamowity refleks i szybkie poruszanie się. Aha, i ukryta broń wszczepiona w obie ręce.
        * LWICA SALONOWA: poruszanie się wśród możnych tego świata, plotkowanie i intrygi to jej silna strona.
        * WŁAŚCICIELKA KONTRAPUNKTU GROZY: posiada własny krótkodystansowy jacht kosmiczny o dobrych skanerach i niezłej sile ognia
    * osobowość: 
        * ENCAO: 0--++
        * Nie planuje długoterminowo, żyje chwilą
        * Nie ma absolutnie żadnych barier czy granicy osobistej
        * Opiekuńcza, skłonna do ochrony innych
        * Tolerancyjna, skłonna do akceptowania wszystkiego, co nie robi bezpośredniej krzywdy
    * wartości: 
        * Osiągnięcia: chcę pokonać każde wyzwanie. Chcę być lepsza niż byłam wczoraj.
        * Dobrodziejstwo: chcę pomagać innym, chcę sprawić, by wszystkim jak najlepiej się wiodło.
        * NIE Konformizm: nie zależy mi na tym, żeby wszyscy się dopasowali. Więcej, im więcej różnorodności i irracjonalności tym lepiej!
    * pragnie: 
        * NIE NUDZIĆ SIĘ! Jeśli jest jakaś ciekawa sprawa, należy to rozwiązać.
        * Jeśli już gdzieś jestem, to mogę się do czegoś przydać i innym pomóc.
        * Jestem bogata. Nie wszyscy tak mają. Mogę się pochylić nad maluczkimi.

* Kara Prazdnik (objęta przez Fox)
    * rola: delinquet teen, aspiruje do bycia administratorem i/lub idolką, 13
        * WYCHUDZONA TRZYNASTOLATKA: cicha, bezszelestna, nikt nie bierze jej poważnie. Umie o siebie zadbać. Prześlizgnie się i skryje.
        * SIEROTA PLANETOIDY TALIO: zna tunele i tajemnice lodowej planetoidy Talio. Zna ludzi w podziemiach.
        * ASPIRUJĄCA IDOLKA: chciałaby być sławną virtidolką, ale śpiewa żebrakom i gangom. Jest ogólnie lubiana.
    * personality: conscientous, extraversion (zorganizowana, metodyczna, wie co robić, bardzo głośna i kocha śpiewać)
        * ENCAO: +-++0
        * Pełny energii, żywy wulkan
        * Nie ma blokad, nie przejmuje się że coś pójdzie nie tak
        * Świetnie zorganizowany; zawsze wie co, gdzie i kiedy
        * Przyjacielski, łatwo nawiązujący znajomości
    * values:
        * TAK: Dobrodziejstwo (pomagać innym), Hedonizm (komfort, luksus i fajne rzeczy. I brak zimna.), Tradition:Religion (ktoś nade mną czuwa, utrzymać harmonię tego co jest i działa)
        * NIE: Security (jest skłonna zaryzykować, co jej się może stać? Ludzie fundamentalnie są dobrzy)
    * wants: dowodzić innymi, wykazać się w sytuacji kryzysowej
        * żeby nie było jej zimno...
        * żeby nie krzywdzić osób które lubi. W sumie nikogo.
        * zostać virtidolką. Lub administratorką. 
        * Dowodzić kimś. Być kimś WAŻNYM. Wykazać się.

* Opętany Szczur Płci Żeńskiej (objęty przez Kić)
    * rola: 
        * SZCZUR OCHRONNY I PRZYJACIELSKI: szczur chroniący Karę, inteligentny, "z laboratorium".
    * osobowość: 
        * ?
    * wartości: 
        * ?
    * pragnie: 
        * chronić Karę
        * prawości. Sprawiedliwości.

### Taliaci

* Aurus Czarmadon: 
    * rola: influencer, władca MiKotek
    * personality: 
        * ENCAO: +---0
        * Ma skłonności do gniewu i wybuchania
        * Spokojny, bez pośpiechu; flegmatyczny
        * Tendencje do podkradania różnych drobiazgów
        * Ambitny, chętny, by się wykazać
    * values: 
        * T: Tradition, Self-Direction, Security
        * N: Power
    * wants: 
        * lepszego życia dla dzieci
        * odkupienia winy
* Kamil Czarmadon
    * rola: influencer, child prodigy (16)

* Mirella Karczak (Czarmadon)
    * rola: influencer, socjopatka, child prodigy (16)
    * personality: +-+-+; urocza socjopatka
    * values: POWER! FACE!
    * wants: the best there is.

* Tytus Ramkon, kapitan sił ochronnych
    * rola: main security, kapitan sił ochronnych
    * personality: 
        * ENCAO: -+0-0
        * Nie dba o potrzeby innych, tylko o swoje
        * Krytyczny, szuka dziury w całym
    * values: 
        * T: Conformity, Power, Security
        * N: Hedonism
    * wants: 
        * pozbyć się Ogników, porządek, zadowolić Piscerników
        * Pokój przez tyranię

* Antonina Terkin, main medical
    * rola: medical expert, kiedyś Ognik
    * personality: 
        * ENCAO: -00-+
        * Powściągliwy, skryty i wyważony
        * Skupiony przede wszystkim na pieniądzach i korzyściach materialnych
        * Subtelny, zostawia podpowiedzi zamiast mówić wprost
    * values: 
        * T: Family (Talio, Ognik), Achievement, Face
        * N: Universalism, Hedonism
    * wants: 
        * Preserver: "this planet is dying..." - TO jest ważne i piękne i niewidoczne!
        * Towarzystwo: czy przyjaciele, czy zwierzak - ale chce być w swojej Grupie

* Nicola Burkin, engineering
    * rola: engineering, współczująca, przeciwnik polityczny Tytusa
    * personality: 
        * ENCAO: 00+--
        * Nudny i przewidywalny; wiadomo dokładnie jak się zachowa
        * Nigdy nie wiadomo, co jest prawdą a co maską
        * Pedantyczny, z ogromną dbałością o szczegóły
    * values: 
        * T: Benevolence, Humility, Self-direction
        * N: Universalism
    * wants: 
        * Ochraniać słabszych
        * Odkrycie konspiracji

* Erwin Infernalismus (Terkin), Ognik
    * rola: ognik, trzyma bandę twardo, brat Antoniny z niej dumny
    * personality:
        * ENCAO: 0+00-
        * Nietolerancyjny, musi być tak jak uważa
        * Cyniczny, patrzy na świat z goryczą
    * values:
        * T: self-direction, power, family
        * N: humility
    * wants:
        * ratować siostrę, wspierać
        * chcę wiedzieć WSZYSTKO o Talio
