## Metadane

* title: "Samszarowie, Lemurczak i fortel Strzały"
* threads: brak
* motives: dark-scientist, echa-inwazji-noctis
* gm: żółw
* players: kić, anadia, kamilinux

## Kontynuacja
### Kampanijna

* [230411 - Egzorcysta z Sanktuarium](230411-egzorcysta-z-sanktuarium)

### Chronologiczna

* [230411 - Egzorcysta z Sanktuarium](230411-egzorcysta-z-sanktuarium)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: Umbra et Imago "Endorphine" ("The shouts of the father | So yelling and agonizing")
* CEL: heroizm graczy?

### Co się stało i co wiemy

* Kontekst
    * tien 

### Co się stanie (what will happen)

.

### Sukces graczy (when you win)

* rozwiązanie problemu

## Sesja - analiza
### Fiszki

* Elena Samszar: czarodziejka origami i kinezy, <-- objęta przez Anadię
    * Archiwistka, biurokracja, prawo i dokumenty
    * Jej papier potrafi zrobić krzywdę
    * W wolnym czasie robię rzeczy z papieru, wieże, łabędzie itp.
    * A teraz, ku wielkiemu smutkowi, pomagam stanąć na nogi kuzynowi Karolinusowi >.>
* .Lancatim, lokacja
    * niedaleko potężne gniazdo Thunderbirda (phasing, minor-port, call lightning)
    * jaskinie zamieszkałe przez wilkoszczury

### Scena Zero - impl

.

### Sesja Właściwa - impl

Strzało - leciałaś z resztką energii, ledwo sprawne systemy ale obecność grzmotoptaka to nawet Ty wykryjesz. Mało da się wykryć przez grzmotoptaki, więc da się tu bezpiecznie schować. Strzała osiada dość blisko, drona zwiadowcza jako oczy po uszkodzonej stronie. Lewa strona Strzały celuje w jaskinię. I jaskinia jest "czysta". Czyli udało się przycupnąć w miarę bezpiecznie. Strzała wyrzuca dronę jako awaryjny system ostrzegania i przechodzi w tryb obniżonej mocy - system autorepair.


Dane o wilkoszczurach.

Karolinus do Irka odnośnie Sanktuarium. Pyta o Siewczyn. "Wrócisz do swoich jak rozwiążemy". A Elena "ściągnąłeś potwora a ludzie co umierali u nas to co? (zimno)". Irek się aż zapluł. "Wy jesteście pieprzoną arystokracją magiczną. Was ci ludzie nie obchodzą. Nic". Elena "trzeba było powiedzieć a nie podrzucać" (intencja: że jest wkurzona i naraził ludzi i duchy i śmierci ducha spichlerza). Irek się zaśmiał paskudnie "tak jasne TIENOWACI interesowaliby się ludźmi. Powiedziałbym to byście mnie zamknęli."

Irek kontynuuje "widzieliście Sanktuarium - tam ile magów różnych. Wszyscy niszczyli, pasożytują. Wy nie lepsi, porwaliście mnie. Oni zginą. Przez was." Karolinus: "może próbujesz ratować swoją skórę". Irek "COŚ MI GROZI?" Karolinus "bez Ciebie świat zginie."

Karolinus spytał czemu on to zrobił, czemu on zostawił ducha w Siewczynie. Irek odpowiedział

* było niewielkie miasteczko które miało problem z duchem (Hybryda)
* on nie umiał tego zrobił ale musiał ratować
* dlatego zaniósł do tienów którzy się na tym powinni znać
* on jest przekonany, że żaden mag z Aurum by nie pomógł ludziom

Karolinus "może jak jedno miasteczko naprawimy to pomożemy". Irek "jasne, bo będzie wam się chciało pomóc". Karolinus opowiada Irkowi trochę o swoim życiu, jak pomagał ludziom. Próbuje trochę otworzyć umysł Irka.

Ex Z (Karolinus ma gadane) +3:

* Vr: Irek uważa, że MOŻE niektórzy tieni faktycznie coś chcą zrobić by pomóc, ale nie sądzi że to Wy (po Sanktuarium).
* Vz: Irek wie, że nie ma szans, więc jest skłonny z Wami współpracować. Nie będzie próbował np. zabić we śnie. Będzie jakoś próbował pomóc, walczyć ze stworem
    * Irek faktycznie jest kiepski, mało skilla, słaba moc, ale serce tam gdzie powinno. Tylko kiepskie strategie.
    * Irek opowiedział jak kiedyś próbował współpracować z Aurum i torturowali go.
        * On się strasznie boi magów Aurum. Nie ma w nim myśli że może wygrać.

Elena ma dużą wrogość do Irka. Nie chce ocieplać.

Drona Strzały wykryła sporo małych stworzeń UCIEKAJĄCYCH od Was. Strzała ostrzega swoich. Coś się zbliża. Coś groźnego. I zbliżają się... dwójka wykończonych nastolatków. Zapłakanych. Uciekają. Biegą _dalej_. Nastolatki są ubrane w 'do-it-yourself'. Lokalsi. Na tym terenie są lokalsi. Przebiegną i NIE ZAUWAŻĄ. Karolinus chce ich wpuścić i zanęcić do jaskini. Sygnał z głośników Strzały "jeśli potrzebujecie pomocy zapraszamy do jaskini"

Ex +3Vr +5Og:

* V: dzieciaki się zawahały, ale biegną do jaskini
* Og: pomiędzy jaskinią i nimi pojawił się stwór. Wygląda jak szczurowilk. RZUCIŁA się na dzieciaki, ale odbiła. Walczy ze sobą. Nie chce walczyć.
    * Oczy potwora są ludzkie i pełne łez

Karolinus osłania Elenę. Elena rzuca czar by unieruchomić stwora. Z plecaczka wyciąga karteczkę papieru. Zrobić taką spiralkę do spętania.

Tr M Z (przewaga, druga strona nic nie może) +3 +3Ob:

* VmVr: Elena rozmontowała kartkę.

Dzieci ZAMARŁY. Nie ruszają się. Praktycznie nie oddychają. Absolutny strach, jeden z nich się posikał.

* Elena: "spokojnie, nie zrobimy wam krzywdy. Wygląda jakby miała ludzkie oczy. Znacie? Możecie wydawać dźwięki i oddychać."
* Chłopaczek szepnął: "mama". Strzała retransmitowała.
* Drugi chłopaczek rozgląda się dyskretnie szukając broni.
* Chłopaczek: "Nie jesteście z Aurum, prawda?". "Mag z Aurum zmienił mamę."
* Karolinus odłożył broń i podchodzi do matki. Chce ją spuryfikować.

Elena próbuje zobaczyć co się stało w warstwie astralnej.

* Matka, członek rodziny. Tu gdzieś jest wioska. Oni się chowają po jaskiniach, trudno ich wykryć.
* Czarna chmura. Coś co wygląda (dla tej matki) jak superciężki czołg. Nowoczesna konstrukcja.
* Mag Aurum, z dwoma pięknymi dziewczynami, uzbrojonymi. Zły.
* Mag wstrzykuje różnym ludziom rzeczy.
* Jej dzieci uciekają. Ona blokuje maga. Spowalnia go.
* On jej coś wstrzyknał. Ona ma je mu przynieść. Bawi go to.
* tien rodu Lemurczak.
* Ona nie jest zmieniona tylko fizycznie. Emocjonalnie też.

Elena doszła, że to jakiś eksperyment. Tu na serio ten czar - ten eliksir - ma też własności "raportujące". Badanie na próbce kontrolnej. Elena i Karolinus dyskutują co z tym robić. Strzała przekierowuje z autorepair na diagnostykę. "Systemy uzbrojenia działają, drona bojowa ma manewry".

Karolinus chce zwiększyć szansę na odczarowanie. Nie patrzy na konsekwencje. 

Tr Z (Irek który jest puryfikować) +3 M +3Ob:

* Ob: dodajemy kolor energii magicznej Verlenów. Więc Lemurczak będzie przestraszony. Będzie ostrożniejszy.
* X: Lemurczak dostaje pełną diagnostykę magiczną. Tą co chciał. Wie, że w akcji puryfikacji uczestniczyli: tajemniczy Verlen, kiepski puryfikator i biomanta.
* Vz: IREK DAJE RADĘ. On wie jak w to cholerstwo uderzyć.
    * "ktoś dał praktykanta na tą operację więc to jest ciemniejszy Verlen"
* V: Matka zregenerowana. Dzieci w płacz i pełne uściski.

.

* Karolinus: "tacy źli magowie"?
* Irek patrzy takim nędznym, wykończonym spojrzeniem "może się myliłem, może Ty jesteś jedynym nie-złym magiem Aurum"
* Karolinus: "może"

.

Matka doszła do siebie po 15 minutach. Pocałowała sygnet. Dzieci też. Matka powiedziała kilka ważnych faktów:

* Szlachetni magowie uratujcie wioskę, tam jest zły mag! Nazywa się Jonatan Lemurczak, przyznał się!
* On robi straszne rzeczy, coś sprawdza na tych ludziach
* Ma taki duży straszny czołg i kilku żołnierzy

(Elena Samszar - co wiemy o Lemurczakach? Poza tym że drama)

Tr +3:

* V: 
    * Cztery lata temu Jonatan miał 27 lat. Zasłynął w Pakcie z tego, że zalecał się do tien Mirażji Diakon. I skończył nago na balkonie. Dostał wpiernicz i ogólnie, nie było za dobrze. Pełne upokorzenie. On nawet płakał.
    * Jonatan jest lekarzem. Ale on używa skilli specyficznie. On wypacza bioformy. Szuka perfekcyjnej lojalności. Zwłaszcza wobec ładnych dziewczyn. Zwłaszcza wobec Diakonek.
    * Jonatan ma swój cel i bada populację.

Emocjonalny wybuch Irka - czemu jeśli jesteście magami Aurum nie pomożecie? Nie jest lepszy od Was. Ja bym pomógł.

Karolinus i Elena decydują się przechować matkę z dziećmi i poczekać na wsparcie. Strzała wysyła Karolinusowi analizę bojową. Ogólnie - kiepsko. Strzałę martwi, że skoro gościu chce patrzeć jak ludzie się męczą, ma coś, co obserwuje matkę z dziećmi. Strzała ryzykuje poświęcenie ostatniej drony by się dowiedzieć, na niskim pułapie, by przechytrzyć wrogi czołg.

Tr (inicjatywa) Vz (zaskoczenie) +3:

* Vz: drona Strzały skutecznie zobaczyła to co trzeba.
    * "czołg" to jest jednostka WSPARCIA klasy Stegozaur. Badawczo-militarna, przewozi do 5 osób.
    * jest jeden mag w servarze, dwie ładne gwardzistki, w pancerzach i jeden asystent.
    * Lemurczak jest gotowy do natychmiastowego odwrotu
* Xz: Stegozaur wykrył dronę.
    * próba przejęcia drony ze strony Stegozaura.
* X: Stegozaur dał radę wejść do systemów drony (inicjatywa). Strzała podrabia kody i ślady Verlenów.
* (+3Vg) V: Stegozaur ma "dowód" na obecność Verlenów i że wiedzą co się dzieje. Stegozaur naciska celem walki psychotronicznej.
    * Stegozaur chce spalić dronę. Przeładowanie psychotroniczne i zastąpienie pamięci pamięcią fikcyjną.
    * Strzała chce wejść w system Stegozaura i weń uderzyć.
        * +3Or -3Vg
* X: Dane drony są zastąpione. Drona należy do Stegozaura ale wróci - z fikcyjnymi sygnałami.
* X: Stegozaur WIE, że tylko kupił trochę czasu. Verlenowie się zorientują.
    * Strzała chce przekierować dronę gdzieś indziej, by Verlenowie myśleli że jest gdzieś indziej. A tam Strzała jej zrobi 'factory reset'.
* Vz: Strzale się to udało.

Stegozaur na pewno ostrzegł tien Lemurczaka. Lemurczak na pewno jest zaniepokojony.

Strzała zgasła. Po prostu wszystkie nie-niezbędne systemy umarły. Pełna energia na psychotronikę i taktykę. Strzała używa drony bojowej by przestraszyć Stegozaura. Nawiązać z nim kontakt i "pokazać mu", że jeśli się ewakuują szybko to mają szansę uciec, else, po nich.

Ex 3Vz (Strzała jest cholerną noktiańską droną militarną) 3Vg (Stegozaur jest zaniepokojony) +3: -> Tr +3 +3Or (przeciążenie):

* "ćwiczenia Verlenów z grzmotoptakami. Lepiej spieprzaj." +3Vg (plausible i scary)
    * Vg: Stegozaur jest poważnie zaniepokojony. MAULER DAMAGE.
    * V: Stegozaur ma przekroczenie zarządzania ryzykiem. Rozpoczyna procedurę ewakuacji.
        * "nieważne czy to prawda, on jest tylko jednostką wsparcia. Jeśli tu są Verlenowie albo coś co może takie rzeczy robić - lepiej się ewakuować"

Lemurczak posłuchał Stegozaura, bo słucha go z perspektywy taktycznej. Strzała zostawia dronę na skraju widzenia Stegozaura, by ten wiedział że jest pod ciągłą obserwacją. Lemurczak chce wziąć osobę z wioski. Strzała wysyła "nie zgadzam się". 

* Stegozaur przejął inicjatywę:
    * "To nie jest Twój teren. Jakim prawem decydujesz."
    * Strzała -> Elena o prawach.
        * +3Vv. Nie grozi siłą. Ale podaje prawa.
            * Vv: ODZYSKAŁAŚ inicjatywę, Vv
            * Vr: Stegozaur zaakceptował rozumowanie, jest wytrącony. Lemurczak niechętnie ale zostawił dziewczynę i się pakują na Stegozaura. Bez pośpiechu, ale konsekwentnie.

Stegozaur faktycznie się ewakuował. Drona podąża niedaleko (bo interferencje itp), po czym wraca. Strzała wróciła do działania.

* Irek "zupełnie się nie spodziewałem (szok) Jak to zrobiłeś, tien Samszar?"
* Karolinus "chcesz pomóc, idź po dronę z ustawieniami"
* Dzieciak "ja pójdę, magu. Dziękujemy. Naprawdę. Mam prezent. (broszka zrobiona z kości wilkoszczura)"

.

* Cztery godziny później pojawiło się wsparcie. Najbardziej irytujący mag jakiego można zobaczyć. Roland Samszar.
* Strzała wysyła Rolandowi raport o wydarzeniach tutaj, szczególnie podnosząc rolę Irka. Wystawia laurkę. Podkreśla "na pewno żeby go nie zabili".
    * Tak jak dawno temu wysłała w raporcie że Siewczyn ma problemy.

.

Elena natychmiast działa na Irka - jedyny sposób by Cię nie poświęcili, nie zniszczyli (powiedziała mu po co jest), duch nastawiony na niego. Jeśli chce uratować wioskę i uratować skórę i nie chce problemów ze starszyzną przyszedł po to i WEZWAŁ po to bo zrobił haniebny czyn i chce odpowiedzieć za czyny i prosi o pomoc.

1. on nie jest poświęcony
2. my jako bohaterowie bo poprosiliśmy o pomoc
3. on chce ocalić Siewczyn więc prosi o pomoc i wybaczenie

Tr Vz (przewaga tego że to jest tak bezczelne że niespodziewane) +4:

* V: Irek jest przekonany że to jest bezpieczne
* X: Irek ma inicjatywę -> emocje
* "Sanktuarium. Zapewnij proszę, że Sanktuarium będzie bezpieczne. Że ktoś się nim zajmie."
    * (+3Vg Elena deklaruje że mu powie) X: Elena jest zobowiązana by znaleźć KOGOŚ kto powie że się tym zajmie.
    * (oddaję inicjatywę)
* Elena się WSTAWI za Irkiem, że on się opiekował, był dobry - więc niech Irek idzie za jej planem (+3Vg)
    * Vr: Irek faktycznie idzie za planem Eleny
    * X: "Elena chciała dobrze, plan był chyba dobry (pomóc Siewczynowi, Sanktuarium Kazitan itp), pomysł był dobry, ale Elena to Elena, wymamrotała, więc musi nad tym popracować.
        * NA SZCZĘŚCIE ROLAND BYŁ W POBLIŻU I WSZYSTKO JAK ZAWSZE NAPRAWIŁ. Ale - warto oddać, że Karolinus i Elena od początku chcieli dobrze!

Na miejscu dowiedzieliście się, że nie ma konieczności zabijania Irka, bo Verlenowie ukradli potwora. Viorika ukradła potwora XD. 2 dni temu.

## Streszczenie

Strzała w ruinie, ale dała radę dotrzeć do w miarę bezpiecznego miejsca pod grzmotoptakami. Gdy dwójka nastolatków na które poluje ich zmieniona przez Lemurczaka matka się pojawili blisko, Elena ją unieruchomiła a Karolinus przekształcił w normalną formę. Acz Paradoksem wysłał sygnaturę Verlenopodobną. Gdy Karolinus i Elena się kłócą czy pomóc czy czekać, Strzała pojedynczą droną wymanewrowała Stegozaur-class support hovertank i przestraszyła Lemurczaka hintując, że Verlenowie polujący na ptaki są w pobliżu. Elena zmanipulowała Irka, więc E+K wyszli na osoby pozytywne które chcą dobrze, acz nie zawsze mają idealne plany (bo są młodzi). A Roland zajmie się Sanktuarium Kazitan.

## Progresja

* Irek Kraczownik: straszna trauma i strach przed magami Aurum. Boi się ich i nie chce współpracować. Kiedyś był torturowany przez Blakenbauerów.

## Zasługi

* AJA Szybka Strzała: mimo połowy systemów zniszczonych, dotarła do bezpiecznej jaskini niedaleko grzmotoptaków. Używając okoliczności i inteligencji w tak marnym stanie przestraszyła Lemurczaka (jego hovertank), że tu są Verlenowie i mają złe intencje. A potem zdała pełen raport Rolandowi Samszarowi i zgasła. Shutdown + autorepair.
* Elena Samszar: duża wrogość do egzorcysty Irka; nie chce ocieplać stosunków. Unieruchomiła magią przekształconą przez Lemurczaka matkę nastolatków. Zmanipulowała Irka, by ten powiedział że Elena i Karolinus są po właściwej stronie i on nie był porwany tylko ich potrzebował. Dzięki temu Samszarowie wyszli na bohaterów (acz jeszcze nieudolnych bo młodych) a nie na potwory z Aurum XD.
* Karolinus Samszar: próbuje zrozumieć Irka i jego motywacje; naprawił magią przekształconą przez Lemurczaka matkę nastolatków. W ten sposób pokazuje Irkowi, że może nie jest całkiem zły. Irek mu zaufał. Karolinusowi dziękują w okolicach Lancatim; tam jest bohaterem (mimo, że to Strzała zrobiła robotę).
* Irek Kraczownik: próbuje przekonać Samszarów, że Aurum jest z dupy i on próbuje naprawdę chronić ludzi. Opowiedział, jak kiedyś Blakenbauerowie go krzywdzili. Zależy mu na Sanktuarium. Przekonał (compel) Elenę, by ona wstawiła się za Sanktuarium Kazitan i za to wsparł jej cele (by Elena i Karolinus wyszli na bohaterów a nie porywaczy).
* Jonatan Lemurczak: robi jakieś dziwne eksperymenty na mieszkańcach Lancatim, w ukryciu przed innymi magami. Czegoś szuka, chciał porwać jedną dziewczynę, ale wolał opuścić ten teren. Skupiony na bezpieczeństwie bardziej niż na wyniku eksperymentów. Przekonany przez swego Stegozaura, że w okolicy jest banda szalonych Verlenów.
* Roland Samszar: prawdziwy paladyn, kiedyś towarzysz Strzały. Chce pomóc i Sanktuarium Kazitan i lokalnym ludziom, więc Elena i Irek nie muszą go szczególnie przekonywać.

### Frakcje

* Tien Lemurczak: nierzadko wysyłają agentów poza swoje tereny by coś sprawdzać czy pozyskać wiedzę i osoby. Tym razem magiem "poza terenem" był Jonatan Lemurczak. Ogólnie, Lemurczakowie unikają Verlenów, bo z nimi nie da się negocjować.

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Przelotyk Zachodni Dziki
                            1. Lancatim, okolice
                            1. Lancatim

## Czas

* Opóźnienie: 1
* Dni: 2

## OTHER

## OTHER
### Fakt Lokalizacji
#### Miasto Lancatim

Dane:

* Nazwa: Lancatim
* Lokalizacja: 

Fakt:

* niedaleko potężne gniazdo Thunderbirda (phasing, minor-port, call lightning)
* jaskinie zamieszkałe przez wilkoszczury
* rozbitkowie ze światów Noctis

.
