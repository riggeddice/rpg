## Metadane

* title: "Kajdan na Moktara"
* threads: pieknotka-w-cieniaszczycie
* motives: pojedynek-rywali, echa-inwazji-noctis
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [181125 - Swaty w cieniu potwora](181125-swaty-w-cieniu-potwora)

### Chronologiczna

* [181125 - Swaty w cieniu potwora](181125-swaty-w-cieniu-potwora)

## Projektowanie sesji

### Struktura sesji: Eksploracja

* **Scena, Niestabilność, Opozycja, Trigger**
    * Waleria i Pięknotka mają firefight na snajperkach
    * Waleria chce usunąć zagrożenie Pięknotki i vice versa
    * Waleria Cyklon: swarm ghost sniper
    * start sesji

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. ...

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (11:11)

_Skalny Labirynt_.

Pięknotka skulona za jakąś skałą, wszędzie pełgające światła i cienie. Teren wyglądający jak złożony przez jakiegoś pająka tkającego sieć z kamieni. Jedyny dźwięk jaki słychać to ciche szumienie morderczych dron. I gdzieś tam jest Waleria, polująca na Pięknotkę.

TOR: 10/10 silnik 1k3 na akcję.

Pięknotka szła na akcję. Ma przy sobie całość kosmetycznego sprzętu, tego kluczowego. Pięknotka buduje szybko silny środek, po którym Walerię powinna zacząć boleć głowa, silny i okrutny zapach. Pięknotka przywykła - plus, ma power suit. Szybkie strzelenie do rzutki z toksyną w powietrzu i błyskawiczna zmiana miejsca pobytu. (Porażka). Pięknotka dała radę rozpylić i się przesunąć; poczuła sygnaturę teleportacji i walnęła jetpackiem w sufit, unikając wiązki lasera. Jetpack jest martwy, ale power suit działa.

Waleria wie, gdzie jest Pięknotka i Pięknotka nie ma jetpacka.

"Musiałaś być bohaterką. Musiałaś tu przyjść przeciwko Moktarowi. Nie Ty pierwsza. Nie pokonasz mnie. Znaj swoje miejsce".

To nie jest pułapka - Waleria musiała rozpaczliwie bronić sekretu Moktara. Ale teraz tu jest, wraz z Pięknotką. Większość dron Walerii nie potrafiło spenetrować pancerza Pięknotki; to najlepszy dowód jej nieprzygotowania. Gorzej, że Pięknotka jest w złej pozycji.

Pięknotka zdecydowała się na zaklęcie o dość katastroficznej mocy. Jako źródło energii wykorzystała reaktor power suita - chce zrobić potężną burzę pod ziemią. Waleria bez power suita i ze swarmem będzie miała dużo trudniejszą sytuację niż Pięknotka, której power suit zaabsorbuje ewentualne wyładowania. Potężny magiczny test trudny. (MTr+1: 11,3,5=S). Pięknotka zaczęła wypalać power suitowy reaktor, ze wszystkimi zapasami - ale dookoła rozpętało się elektryczne piekło. Waleria próbuje się rozpaczliwie bronić przed wyładowaniami (6,3,4=P). Waleria jest ranna i lekko ogłuszona; musi się ewakuować. A Pięknotka ma nowy ruch...

Pięknotka wie gdzie jest Vault Moktara w tym przeklętym labiryncie; Waleria jest blokadą stojącą jej na drodze by tam się dostać. Pięknotka zmieniła strategię - thousand cuts. Tak pokona Walerię. Wzięła więc stinkbombę i rzuciła lobem prosto w Walerię, parabolą. Tymczasem Waleria odpaliła laser by zrzucić na Pięknotkę kawały sufitu. Jednoczesne działanie.

(-2 Wpływu Kić: Waleria do końca sesji nie ma wsparcia Łysych Psów ani przyjaciół. Żadnego inteligentnego.)

* Atak Pięknotki: (Typowy) Sukces. Waleria musi opuścić swoją pozycję i jest bardzo osłabiona.
* Unik Pięknotki: (Typowy) Sukces Skonfliktowany. Power Suit jest martwy po tej scenie.

Pięknotka na ziemi, w zdewastowanym power suit. Waleria na ziemi, wyraźnie ranna w nogę i ostro kaszle. Pięknotka ma szansę nie być trafioną (większą niż to brzmi z tego zdania) mimo, że Waleria ma zdecydowaną przewagę tego że musi tylko nacisnąć na spust.

* tor-pięknotki: 0, 3, 1, 4, 
* tor-walerii: 2, 0, 2, 

Pięknotka wie, że czas jest po jej stronie.

* "Poczekamy... na posiłki. Nie ruszaj się..." - Waleria, łapiąc powietrze przy kaszlu.
* "Dobrze, poczekamy..." - Pięknotka, spokojnie. ONA nie jest ani obita ani ranna. I ma pancerz.

Pięknotka spróbowała wyglądać na jak najmniej groźną - odsunęła broń i spytała Walerię co Moktar na nią ma. Bo przecież ona sama by nie współpracowała z kimś takim. Waleria się tylko uśmiechnęła krzywo. On przyszedł ją uratować gdy była najsłabsza. A Pięknotka miała Lilię - i Lilia sama opuściła Nukleon, wróciła do domu w hańbie. Pięknotka, jak wszyscy, wykorzystuje magów jak narzędzie. Ale ona (Waleria) przynajmniej przez Moktara jest narzędziem polerowanym i zadbanym.

Chwilę później eksplodowały kosmetyki w powietrzu po trafieniu błyskawicą. Power Suit to przyjął i padł. Waleria, cóż. Mocniejszy cios, bo nie ma pancerza. Silniej ranna i nieprzytomna.

Wpływ:

* Żółw: 4
* Kić: 1 (3)

**Scena**: (12:08)

_Skalny Labirynt_.

Waleria jest nieprzytomna. Wygląda żałośnie krucho. Pięknotka wypełzła z power suita, choć chwilę jej to zajęło. Podeszła do Walerii i użyła zaklęcia skanującego mentalnie - jak dostać się do tego Vaulta Moktara. (MTp). Waleria WIE, że Pięknotka zrobiła nie shallow scan a deep scan - że mogła zdobyć wszystkie informacje. Wie, że Pięknotka się nie powstrzymywała. A Skonfliktowanie Sukcesu sprawia, że Waleria jest bardzo zafascynowana i zauroczona Pięknotką. A Pięknotce też Waleria bardzo zaimponowała; sprzężenie mentalne. Waleria ma podniesioną amplitudę emocji przy Pięknotce.

Pięknotka zobaczyła oficer wojskową na statku. Statek atakujący Astorię, kilka lat temu. Flotę. I ją - Walerię - pozostawioną przez sojuszników na śmierć. Waleria by umarła - zero zaufania, czegokolwiek, traktowana jak wróg, gdyby nie Moktar, który wziął ją do siebie. Stąd płonąca wręcz nienawiść Walerii do Orbitera Pierwszego i do swojej byłej frakcji, którzy ją zostawili. Lojalna, kompetentna, wierna. Nie jest terminusem, ale jest świetnym żołnierzem. I jest skrajnie samotna - poza Moktarem i Łysymi Psami. I Waleria wie, że teraz Pięknotka to wie...

Pięknotka poznała też sekret Moktara który Waleria zna - on wzmacnia się krwią swoich Łysych Psów, łącznie z krwią Walerii. Co tydzień robi sobie transfuzje od żywych magów. Destabilizuje to jego Wzór, ale upiornie go wzmacnia. Użył do tego też krwi Lilii - a to oznacza, że prawnie zrobił coś strasznego, bo Lilia nie jest obywatelką Cieniaszczytu. I okazuje się, że Moktarowi naprawdę po swojemu zależy na Łysych Psach i na tym, co się stanie z jego oddziałem. Mógłby 'make a statement' i ich powybijać, ale nie chce tego robić.

* Łyse Psy bardzo cenią Moktara. Część się go boi i chce odejść, ale wielu chce z nim zostać bo im pomógł jak Walerii
* Ceni sobie obiekty o mrocznej historii, broń i rzeczy niebezpieczne. Ceni też siłę, ale jak widzi zagrożenie, zniszczy przeciwnika.

Pięknotka wlecze ze sobą Walerię. She is taking her time. (Tp+1:6,3,4=P). Niestety, stan Walerii się pogarsza, zdecydowanie. Nie można jej po prostu tak przetransportować - potrzebne są jakieś zdecydowane środki. Pięknotka bierze dwa karabiny, jakaś forma ciuchów i robi tobogan. (Łt:S). Uda się przetransportować Walerię.

(Ż: -5 Wpływ: atak Nojrepów)

Trzy Nojrepy. Idą prosto na Pięknotkę. I Pięknotka dostała echo informacji od Walerii - Astoria odparła jej sojuszników właśnie przy użyciu zmilitaryzowanych nojrepów. Pięknotka zaczerpnęła wiedzy Walerii - jak ominąć lub usunąć te istoty. Safe passage to the base. (MTp:10,3,3=SS). Waleria przejęła kontrolę nad ciałami 3 nojrepów - swarm queen is back. Ale pozostałe Nojrepy wiedzą o odcięciu 3 dron od sieci i przygotowują się do ataku. Zgodnie z wiedzą Walerii, one chcą **zdobyć Walerię żywą** acz Waleria nie wie czemu.

Waleria się obudziła. Powiedziała, że będzie musiała wpuścić Pięknotkę, bo inaczej ta umrze. Sama Waleria jest bardzo ciężko ranna, lekka maligna - operuje z poziomu nojrepów bardziej niż swojego ciała. Wprowadziła Pięknotkę do bazy i dała się wsadzić w medvat. Powiedziała Pięknotce, że ta baza zostanie zmieciona przez nojrepy. Poprosiła, by Pięknotka wsadziła ją w augmenter - tam Waleria będzie w stanie zostać wzmocniona i ustabilizowana by móc obronić tą bazę. Pięknotka to zrobiła.

Pięknotka ma 10 minut sama w bazie. SZCZURZY. Pięknotka szuka świetnego dowodu, czegoś, co może użyć przeciwko Moktarowi by tego móc zniszczyć. (Tp+2: 7,3,4=SS). Pięknotka ma swój dowód - ale Moktar wie o tym, że Pięknotka ma i wie, że to wina Walerii po tej sesji.

Pięknotka przeszła przez portal z dowodem. Przedostała się przez linię nieskoordynowanych nojrepów i dostała się do wsparcia - ekstrakt z Walerii (Tp+4:11,3,3=SS). Przeszła, ale ranna. Solidnie ranna. Wymaga pomocy medycznej, ale dotarła.

**Scena**: (13:21)

_Cieniaszczyt, Mrowisko_.

Pięknotka obudziła się w Mrowisku, zabandażowana. Paręnaście minut była nieprzytomna. Pietro wyjaśnił, że przeciwnik używał środków obezwładniających - nie próbowali jej zabić, próbowali ją pojmać. Pietro ściągnął Wiolettę i Pięknotka zaczęła wszystko tłumaczyć.

Wioletta i Pietro się rozeszli. Trzeba znaleźć ekipę do ataku na bazę, która jest potem do rozszabrowania. Brzmi dobrze - acz wymaga odpowiednio ciężkiego oddziału. Aha, Pięknotka nie może iść z nimi - środki nojrepów wymagają neutralizacji a Amadeusz chce je przebadać. Pięknotka nalegała, ale nic z tego nie było. Niestety.

Wpływ:

* Żółw: 6 (11)
* Kić: 5 (7)

**Epilog**: (13:35)

* Moktar pokazał Pięknotce strefy wpływów. Pokazał gdzie ją zniszczy a gdzie ją ominie. Nie celuje w atakowanie Pięknotki.
* Pięknotka ma dowody, które może nie zniszczą Moktara, ale zniszczą jego plany i rozwiążą Łyse Psy.
* Dziwny oddział nojrepów został zniszczony przez Walerię i wsparcie.
* Waleria została przetransportowana do Nukleona na leczenie.
* Waleria zdążyła zniszczyć wszystko co miała zniszczyć przed przybyciem wsparcia.
* Moktar jest bardzo, bardzo niezadowolony z wyników działania Walerii.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |   4     |    11       |
| Kić           |   1     |    10       |

Czyli:

* (K): Wbijam klin w oddział Moktara - niech ci co chcą odejść, niech Moktar ma więcej z nimi zachodu. Obserwowalnie - Pięknotka katalizatorem. (2)
* (K): Oddział wsparcia zdobył części dużej ilości nietypowych nojrepów (2), od razu poszły do Sowińskiego (1), są tam też modele których nikt nigdy nie widział łącznie z obezwładniającymi (2).
* (Ż): Moktar i tylko Moktar chwilowo wie, czemu te nojrepy działają tak jak działają - i Pięknotka wie od Walerii że on wie (2).

## Streszczenie

Pięnotka próbowała osłonić się przed potencjalnym atakiem Moktara i poszła szukać jakiejś jego słabości - i spotkała się w pojedynku snajperów z Walerią Cyklon. Wygrała dzięki lepszemu sprzętowi (power suit), acz skończyła w złym stanie. Wycofała się i Walerię przed atakiem dziwnych nojrepów i wybudziła Walerię; dostała swój dowód, przeskanowała pamięć Walerii i uratowała bazę Moktara przed zniszczeniem przez te dziwne nojrepy. Ogólnie, sukces.

## Progresja

* Pięknotka Diakon: Moktar i ona nie są już w stanie otwartej wojny. Pięknotka ma hak na Moktara - może zniszczyć Łyse Psy i osłabić jego plany.
* Pięknotka Diakon: ma dostęp do ech pamięci Walerii z różnych momentów, czasów i punktów przeszłości.
* Moktar Gradon: ma ogromną wiedzę o dziwnych nojrepach i całej tej sprawie z Orbiterem Pierwszym
* Moktar Gradon: wie, że Pięknotka ma dowód mogący poważnie uszkodzić jak nie zniszczyć Łyse Psy. Musi działać przeciw niej ostrożniej.
* Moktar Gradon: Podzielił się z Pięknotką strefami wpływów; jak długo nie wchodzą sobie w drogę, Pięknotka jest bezpieczna.
* Waleria Cyklon: ciężko ranna, wymaga regeneracji i w niełaskach Moktara z uwagi na fail, utratę bazy i dowód jaki ma Pięknotka mogący zniszczyć Psy.

## Frakcje

* Łyse Psy Moktara: powoli się zaczyna struktura rozpadać przez działania Pięknotki - jest ona katalizatorem rozbijania Łysych Psów
* Ostrze Szkarłatu Cieniaszczytu: dostają ogromną pulę nietypowych, dziwnych nojrepów do analizy i badań

## Zasługi

* Pięknotka Diakon: pokonała w pojedynku snajperskim Walerię, uzyskała dowód mogący pogrążyć Moktara oraz wyszła na zero - nie ma groźnych wrogów. Pełen sukces.
* Waleria Cyklon: wiecznie lojalna Moktarowi; chroniła jego bazę przed Pięknotką a potem weszła z nią w sojusz przed nacierającymi nojrepami.
* Pietro Dwarczan: głównodowodzący grupy organizujący plan uratowania Walerii przed dziwnymi nojrepami. Rozkradli bazę Moktarowi XD.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Przelotyk Wschodni
                            1. Cieniaszczyt
                                1. Mrowisko: tam w końcu skończyła ranna i ledwo przytomna Pięknotka, zatrzymana z rozkazu Amadeusza Sowińskiego po uratowaniu Walerii.
                1. Sojusz Letejski, NW
                    1. Ruiniec
                        1. Skalny Labirynt: znajduje się tam baza Moktara. Znajdowała się. Zniszczona przez atakujące nojrepy i grupę ratującą Walerię i kontrolującą te nojrepy.

## Czas

* Opóźnienie: 1
* Dni: 1

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* Analiza sesji eksploracyjnej.
* Założenie jest takie:
    * Eksploracja to zbiór scen nie powiązanych wątkiem (lub zawierających pojedyczny wątek), gdzie są Sytuacje.
    * Sytuacja wymaga podjęcia decyzji przez Gracza i zagrania o to - dzięki czemu zbudujemy świat
    * Zaczynamy od chronologii ŚRODKOWEJ. Jak do tego doszliśmy?
* Żółw ma moc Wpływu na (3).

## Wykorzystana mechanika

1811
