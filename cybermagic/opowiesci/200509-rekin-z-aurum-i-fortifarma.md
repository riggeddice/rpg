## Metadane

* title: "Rekin z Aurum i fortifarma"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [200425 - Infiltrator poluje na TAI Minerwy](200425-inflitrator-poluje-na-tai-minerwy)

### Chronologiczna

* [200425 - Infiltrator poluje na TAI Minerwy](200425-inflitrator-poluje-na-tai-minerwy)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* .

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

* Latające Rekiny mają wrogie podejście do pewnej fortifarmy zarządzanej przez Artura Kołczonda.
* Rekiny skupiły się i zdobyły hodują superciężkiego potwora.
* Dowiedziała się o tym Sabina Kazitan; sama im pomogła i dostarczyła odpowiedniego mutującego rytuału.
* Oczywiście, Sabina natychmiast przekazała informacje Pięknotce.
* ALE na bazie tego co Gabriel wie, ta akurat podgrupa Rekinów wspiera Lilię w sprawie powiązaną z Małmałazem

## Punkt zero

.

## Misja właściwa

Cztery dni później. Do Zaczęstwa wrócił już Tymon. Lucjusz został na miejscu.

A Pięknotkę, ku jej ogromnemu zdziwieniu, do Górskiej Szalupy zaprosiła Sabina Kazitan. Pięknotka poszła. Sabina czeka na Pięknotkę w kącie, w miejscu nie na podsłuchiwanie. Nadal jest paranoiczna, ale Pięknotka mówi, że to nie jest ryzyko. Sabina zatem powiedziała co wie.

* Skontaktowała się z nią Natalia Tessalon, jest to czarodziejka z Aurum. Zażądała, by Sabina podała jej informacje o rytuale jak wyhodować potwora w polu magicznym Trzęsawiska
* To jest rytuał przyspieszający i augmentujący. Wzmocni potwora; sam sposób jest zależny od tego, z jakiego typu został wyhodowany
* Sabina przekazała Pięknotce kryształ z sygnaturą energii tego potwora
* Natalia Tessalon jest bardzo DUMNĄ czarodziejką. Ale nie jest poza polityką; mogła chcieć komuś przekazać lub się zemścić

Pięknotka podziękowała Sabinie - nie musiała. Sabina zauważyła, że Pięknotka takie dała jej wytyczne. Jest lojalna (w domyśle: bo nie ma wyjścia).

Posiadając sygnaturę Trzęsawiska Pięknotka chciała określić GDZIE na Trzęsawisku ma to miejsce. Ale do tego celu potrzebuje najlepiej Alana. Ale Alana nie ma - zostaje Gabriel. Arystokrata - katalista - terminus z przyjemnością spotkał się z terminuską w gabinecie Pięknotki. Dowiedział się - ku swojemu zdziwieniu - że w różnych miejscach Trzęsawisko ma różne sygnatury.

Gdy Gabriel dowiedział się, że Natalia Tessalon, to powiedział że ona jest SOJUSZNICZKĄ. Zarówno sojuszniczką rodu Ursus jak i pomaga w akcji przeciwko Małmałazowi. Dobra sojuszniczka (TrZ+2) i ich rody są w przyjaźni. Pięknotka doszła do czegoś głębiej - Natalia była dziewczyną Gabriela. Teraz jest eks. Gabriel ją zdradzał i Natalia z nim zerwała. BARDZO zasadnicza i honorowa czarodziejka, trochę starej daty. Cała rodzina dobrze dba o majątek i dba o ludzi / magów w swoich włościach, ale jest bardzo konserwatywna.

Jej brat zapowiedział Gabrielowi że jak tylko będzie Natalię nachodził to on wyzwie Gabriela na pojedynek. A Gabriel nie chciał robić krzywdy nikomu z rodu Tessalon. Między innymi dlatego uciekł na prowincję (Szczeliniec) i został terminusem. Przy okazji Gabriel sprzedał Pięknotce co w Aurum mówi się o Szczelińcu (to trochę jak opinia o Rosji w internetach w realu). Pięknotka was not amused. Ale to też tłumaczy niektóre pytania niektórych klientek z Aurum...

Pięknotka nie chce pieprzyć się z arystokracją Aurum. Już Lilia była denerwująca (kiedyś), Gabriel jest chlubnym wyjątkiem a Sabina powoduje dreszcze swoją bezwzględnością i ilością nienawiści. Jeszcze jakaś Natalia to już za dużo. Pięknotka i Gabriel wzięli awiana i oblecą Trzęsawisko z oddali by znaleźć w których to okolicach. Poprosiła Erwina też o pomoc - nie ma wielu lepszych od niego katalistów (TrM)

* V: Znaleźli obszar - to okolice Czemerty. Przynajmniej w tamtych terenach Pięknotka ma prawo działać...
* V: Znalazła lokalizację dokładniejszą. Niedaleko pewnej fortifarmy. Dostała sygnał IFF "zidentyfikuj się!"
* XX: ...ale Gabriela magia uszkodziła odpowiedź. Systemy IFF wystrzeliły.

Zestrzelenie? (Tr): 

* X: Awian jest uszkodzony, ale Gabriel bezpiecznie sprowadził go na ziemię.

Z fortifarmy wyszła niewielka grupa robotów bojowych. Pięknotka wysłała sygnał terminusa. Roboty przestały atakować. Wycofały się.

Z fortifarmy wyszedł jeden z właścicieli, Kołczond. Starszy gość kiedyś był terminusem, ale zostawił tą robotę (po tym jak już nie mógł) i wziął fortifarmę. Plus, się ożenił. Zaprosił Zespół do środka, pomylił ich z "gówniarzami w ścigaczach" (zaklęcie Gabriela).

Kołczond zapytany przez Pięknotkę czy miał jakieś problemy w ostatnim czasie się uśmiechnął i powiedział, że skutecznie wszystkie rozwiązuje. Miał problem z "gówniarzami" - grupa młodych magów chcących się wykazać, Latające Rekiny. Kołczond hodował m.in. latające istoty a Rekiny na nie polowały, bo to fajna zabawa. Kołczond ich ostrzegał że nie wolno - ale zignorowali. Więc wziął i złapał dwóch Rekinów i ich strasznie upokorzył, nagrał to i wysłał na wideo do reszty.

Od tej pory Rekiny raczej zostawiają go w spokoju, są ostrożniejsi. Czasami atakują z zaskoczenia, ale fortifarma wytrzymuje. Kołczond liczy, że niedługo znajdzie sobie kolejnego Rekina i go pojma. Tym razem Rekin zapłaci.

Pięknotka znalazła korelację - po tym jak Kołczond upokorzył te dwa Rekiny, następnego dnia Natalia zażądała od Sabiny rytuału. Czyli jest zdecydowana korelacja.

Kołczond z przyjemnością oprowadził Pięknotkę po fortifarmie. Z ogromną przyjemnością. Pięknotka zapoznała się z pięcioma typami bioform które tu są hodowane. Całkiem przydatne stworzenia, jedno z nich jest często stosowane przez Tukana (bo to jest bioforma) jako broń.

Pięknotka ślicznie podziękowała. Ma co pokazać Sabinie, by dowiedzieć się, co rytuał zrobi z tym stworem.

Sabina próbuje do tego dojść. Ale nie jest to dla niej łatwe - nie jest ekspertem od biomancji a od zakazanych rytuałów. Pięknotka zaproponowała wsparcie kogoś innego (Kreacjusza), ale Sabina odmówiła. Nie umie pracować z innymi. Pięknotka machnęła ręką. Sabina jej nic nie pomoże. Przynajmniej ma parę dni zanim potwór się wyhoduje. Sabina powiedziała tylko jeszcze, że rytuał który przekazała miał cechy inkarnacji. Potwór będzie gotowy za 2-3 dni. Ale może być odpalony wcześniej, tylko będzie wtedy niekompletny.

Pięknotka zabrała całą wesołą bandą do Pustogoru. Dostała opierdol za uszkodzonego awiana. Michasiewicz zaznaczył, że:

* NON STOP Pięknotka dewastuje power suity.
* A TERAZ ZNOWU AWIAN.

Michasiewicz żąda, by Pięknotka rzuciła się na papierkową robotę. Trzy dni. Michasiewicz chce mieć parę dni spokoju bez destrukcji cennego i wartościowego sprzętu. Chce, by Pięknotka to robiła, bo NIE SZANUJE. Pięknotka próbuje uzasadnić, ŻE SZANUJE. Szanuje jego pracę, jego sprzęt, jego osobę itp. ALE MA PECHA! I GABRIEL NIE UMIE LATAĆ!!! (tego nie powiedziała)

* XXXX: Artur Michasiewicz jest PRZEKONANY, że Pięknotka nie szanuje sprzętu. Ale rozumie, że jest terminuską i czasem coś się może popsuć. ALE ONA NIE DBA!
* V: KTOŚ musi zrobić dokumenty. Tzn. niekoniecznie Pięknotka indywidualnie. Ale mają być. I mogą być dopiero po akcji.

Pięknotka zdecydowała, że nie ma zamiaru bawić się w politykę. Ma zamiar znaleźć i zniszczyć potwora. Potem - mając dowody - dorwie osobę "sterującą" potworem. Wzięła więc Gabriela, samochód cywilny (Gabriel prowadzi) i pojechali OSTROŻNIE do fortifarmy. Tam powiedziała Arturowi Kołczondowi, że będzie atak potwora. I ona chce przekazać "pilota" do Barbakanu. Zapytany przez Pięknotkę ile magów bojowych ma, powiedział, że jest sam. A jego magia jest już słaba. Niestety.

Pięknotka zdecydowała się poczekać te dwa dni.

Późnym wieczorem dwa dni później potwór zaatakował. Dla Pięknotki to jest primetime... i 100% się tego spodziewała. Potwór jest szybki. 2.5metrowy, power suit, działo energetyczne. Fortifarma nie może go trafić. Fortifarma nie przegrywa, ale nie jest w stanie wygrać. A potwór niszczy placementy fortifarmy.

Pięknotka, na dachu, ze snajperką. Synchronizuje się z działkami fortifarmy by nie zostać wykrytą. (TrMZ+2)

* V: Niewykryta
* V: Zestrzelenie broni przeciwnika
* V: Przeciwnik się nie wycofa; zbyt zaangażowany, za dużo na szali
* X: Uszkodzona farma (niszczone działka)
* X: Pięknotka więcej nie zdziała snajperką

Pięknotka poczuła głód Cienia. Cień z radością jej pomoże - włączył się.

Gabriel został w środku fortifarmy; pomaga by nic się nie wbiło do środka (jakieś mniejsze istoty).

Pięknotka/Cień wykorzystują efekt zaskoczenia. Chcą zniszczyć power pack na potworze. Potwór bez power suita będzie stosunkowo niegroźny. A sam potwór nie wie o Cieniu. (TrZ+2)

* VZ: Power pack zniszczony; potwór nie może używać niczego energetycznego
* V: Potwór nie ma power suita, zostanie odrzucony
* X: Cień sprzęgł Esuriit/Ixion --> uderza w pilota
* V: Potwór NIE WIE że Pięknotka i Cień tu są

Pięknotka stwierdziła, że najlepszym sposobem unieruchomienia przeciwnika jest odcięcie nóg i rąk. A potem spożycie części ciała by znaleźć pilota. Cień wskazuje na plecy, pazury rąk w ręce a nóg w nogi i niechirurgicznie oraz brutalnie oderwać niepotrzebne (wszystkie) kończyny. Cień approves. Potwór nic nie wie i nie wie z czym walczy. (TrZ+3)

* V: Potwór nie może strącić Cienia
* X: Pilot dostał pełen feedback loop
* V: Potwór nie skrzywdzi Cienia (flawless)
* V: Potwór nie ma kończyn
* V: Nikt w fortifarmie nawet nie wykrył Cienia. Po prostu COŚ zabiło potwora.

Cień ma ślad. Pięknotka leci w kierunku na pilota. (Tr)

* X: Erozja ixiońska spowodowała szok magiczny u pilota.
* X: Potrzebuje natychmiastowej pomocy medycznej.
* X: Są nieodwracalne zmiany związane zarówno z interakcją z Cieniem jak i z rytuałem.
* V: Znaleziony przez Cienia.

Pięknotka próbuje usadzić Cienia, który chce SKOSZTOWAĆ celu (Tp) - VXV. Udało jej się usadzić i wyłączyć Cienia. Wyskoczyła na medyków, nago, po czym wystrzeliła distress signal. Niech ktoś chłopaka ratuje - a ona go w stazę. "Czemu ona jest taka dziwna że lata nago po lesie - no, prowincja".

Karetka szybko przyleciała.

EPILOG:

* Okazało się, że pilotem potwora był ów brat Natalii. Gabriel jest załamany. To był jego kumpel.
* Delikwent potrzebuje kilku miesięcy rehabilitacji. Magicznie nie wróci do pełnej formy już nigdy.
* Sabina się uśmiechnęła zimno. Jeden mniej. Jeszcze sporo musi zniszczyć.

## Streszczenie

Sabina Kazitan ostrzegła Pięknotkę, że Natalia Tessalon z Aurum zażądała od niej niebezpiecznego rytuału do hodowania potwora. Pięknotka doszła do tego z sygnatury energii magicznej, że problem jest w okolicy Studni Irrydiańskiej; tam jest też fortifarma w której stary były-terminus tępi Latające Rekiny. I faktycznie - arystokraci z Aurum stworzyli potwora (krwawego) a Pięknotka Cieniem go zniszczyła. Niestety, brat Natalii został ciężko porażony i nigdy nie będzie już taki jak kiedyś. Aha, kwatermistrz opieprza Pięknotkę.

## Progresja

* Pięknotka Diakon: podpadła Arturowi Michasiewiczowi, kwatermistrzowi Aurum. Nie szanuje sprzętu i go uszkadza a nie musi.
* Artur Michasiewicz: podpadła mu Pięknotka Diakon. Nie szanuje sprzętu i go uszkadza a nie musi.
* Tadeusz Tessalon: utracił część kontroli nad magią i część władzy nad ciałem. Nigdy nie będzie jak był kiedyś.
* Gabriel Ursus: eks Natalii Tessalon. Lubi ją i jej brata. Niechętnie walczy / działa przeciwko nim. Kiedyś ją zdradził i z nim zerwała, potem uciekł "na prowincję".
* Natalia Tessalon: eks Gabriela Ursusa. Pokłóciła się z nim solidnie i go rzuciła.

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: 
* Sabina Kazitan: z przyjemnością przekazała niebezpieczny rytuał Tessalonom i ostrzegła o tym Pięknotkę. Z przyjemnością patrzyła na zniszczenie Tadeusza Tessalona i ból Natalii.
* Natalia Tessalon: dla brata zażądała od Sabiny Kazitan niebezpieczny rytuał. Dostała go, przekazała bratu. Teraz ma wyrzuty sumienia - jej brat jest "zniszczony".
* Gabriel Ursus: źródło informacji o arystokracji Aurum i jak myślą o "prowincji". Jako katalista, pomógł Pięknotce znaleźć miejsce związane z Krwawym Potworem. Załamany losem Tadeusza Tessalona.
* Erwin Galilien: najdoskonalszy katalista; pomógł Gabrielowi znaleźć, że Krwawy Potwór budowany jest w okolicy fortifarmy Irrydii.
* Artur Kołczond: kiedyś terminus, ale stary i stracił większość mocy. Teraz dowodzi fortifarmą Irrydią. Daje radę. Bardzo lubi terminusów i stare opowieści. Ożenił się, ma też dzieci.
* Artur Michasiewicz: kwatermistrz Barbakanu w Pustogorze. Opieprzył Pięknotkę za niszczenie sprzętu bez sensu i kazał jej robić dokumenty aż się nauczy. Nie lubi jej.
* Tadeusz Tessalon: chciał pomścić despekt spowodowany przez Kołczonda; zintegrował się z Krwawym Potworem i po tym jak posiekał go Cień Pięknotki, został ciężko porażony.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Czemerta, okolice
                                1. Fortifarma Irrydia: czerpiąca ze Studni Irrydiańskiej fortifarma budująca bojowe bioformy dla Pustogoru; m.in. dla Tukana.
                                1. Studnia Irrydiańska: miejsce głębokiego Skażenia połączonego z Trzęsawiskiem; gdzieś tam hodowany był Krwawy Potwór.
                            1. Pustogor
                                1. Knajpa Górska Szalupa: najbezpieczniejsze miejsce spotkań z Pięknotką jakie zna Sabina Kazitan (nie wie o gabinecie Pięknotki)
                                1. Gabinet Pięknotki: faktycznie najbezpieczniejsze miejsce spotkań. Pięknotka x Gabriel.

## Czas

* Opóźnienie: 4
* Dni: 5
