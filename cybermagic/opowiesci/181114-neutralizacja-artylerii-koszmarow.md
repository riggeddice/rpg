## Metadane

* title: "Neutralizacja artylerii koszmarów"
* threads: wojna-o-dusze-szczelinca
* motives: magowie-eksploatuja-ludzi, vicinius-pozyteczny, zemsta-uzasadniona, konstrukcja-superbroni
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [181101 - Wojna o uczciwe półfinały](181101-wojna-o-uczciwe-polfinaly)

### Chronologiczna

* [181101 - Wojna o uczciwe półfinały](181101-wojna-o-uczciwe-polfinaly)

## Projektowanie sesji

### Struktura sesji: Wyścig

* **Strona, potrzeba, sukces, porażka, sposób, silnik, breakpointy**
    * Wiktor Satarail, uzyskujący morderczą broń
    * powstrzymać ciągłe ataki na Trzęsawisko
    * ma ciężką broń - Upiorną Skrzynię
    * niczego nie osiągnął, musi naładować
    * osobiste działanie z bioformami
    * 1k3 / 3, skok na 3
    * Tor:
        * 3: Wiktor naładował artefakt (vol 1) na Nieużytkach
        * 6: Wiktor zaatakował okoliczne tereny przy użyciu bioform i zalał teren siłami bojowymi
        * 9: Wiktor wycofał się na Trzęsawisko z artefaktem
        * 12: Wiktor jest na wzmocnionej pozycji na Trzęsawisku
        * 15: Wiktor ma uzbrojoną broń artyleryjską
        * 18: Wiktor Satarail jest bezpieczny; nie da się doń dostać

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) 
* Ż: (przeszłość, kontekst) 

### Dark Future

1. Na Trzęsawisku powstanie śmiertelnie niebezpieczna broń artyleryjska, zagrażająca Zaczęstwu i awianom

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: Katastrofa w Mecce (21:00)

Pięknotka dotarła do Mekki Wolności. Grupa przerażonych anarchistów, wyraźnie była tu impreza. Coś się stało. Anormalnie niski poziom energii magicznej i ogólna panika i konfuzja. Z dobrych wieści - Tymon jest w drodze. Pięknotka łapie osobę która próbuje coś tu ogarnąć i organizować - uczestnika imprezy (animatora) imieniem Karol - i pyta co się dzieje. Pięknotka nie przeszkadza - odchodzi.

Pięknotka napotkała osobę, której się nie spodziewała. Felicję - ze szkoły magów. Nie panikuje, próbuje jakoś pomóc. Nieporadnie. Kieruje ludźmi, uspokaja ich ręcznie. Pięknotka wzmocniła jej umiejętności uspokajania ludzi - niech Felicja sobie z tym dobrze radzi. (TpM+2:9-3-3=S). Ludzie zaczęli się uspokajać; potęga Pięknotki działa prawidłowo. Acz Pięknotka czuje coś dziwnego - tak jakby coś wysysało energię jej zaklęcia. Ale za słabo, to raczej echo.

Ludzie uspokojeni, Felicja próbowała rozpłynąć się w tłumie by Pięknotka jej nie zauważyła. Oczywiście, Pięknotka złapała ją za rękę. Pięknotka dopytuje Felicję o co chodzi, co się stało. Felicja chce powiedzieć, ale nie umie (Tp+2:9,3,3=S). Wyjaśniła Pięknotce co następuje:

* energia magiczna: wpierw eksplodowała, dużo jej było, potem implodowała; zniknęła, coś ją wyssało
* pomiędzy pojawieniem i zniknęciem energii pojawił się Pnączoszpon przerażając ludzi i tak jakby coś wzbudziło panikę
* jednocześnie panika została osłabiona jak zniknęła energia. Coś zadbało by ludzie nie porobili sobie krzywdy
* Pnączoszpon zniknął, ale to nie była iluzja. Felicja jest pewna, że był prawdziwy. Nikogo nie skrzywdził co jest dziwne

Pięknotka kazała Felicji zająć się ludźmi. Sama zapoluje na pnączoszpona...

Pięknotka wie czego szukać by śledzić pnączoszpona. Świeże ślady i łatwe dojście. (Tp+1:10-3-3=SS). Dotarła do Nieużytków - tam faktycznie dojrzała pnączoszpona, oraz maga w brązowych szatach który ładuje przez żywe magitrownie jakiś artefakt. Pnączoszpon wyraźnie go pilnuje. Pięknotka zna bagna (Tp:9-3-3=S). Wiktor Satarail. Terror Zjawosztupa. Pięknotka pingnęła Tymona, po czym strzał w powietrze krzycząc "odwołaj go" widząc, jak Pnączoszpon atakuje.

Satarail krzyknął, że Pięknotka ma zwiewać. Pięknotka odpaliła power suit oraz odpaliła bombę perfumową - niech pnączoszpon się wycofa. (Tp+Z:12-3-2=S). Pnączoszpon wycofał się blisko Wiktora sycząc. Pięknotka w power suicie wyszła bezpieczniej i ostrożniej na widok. Wiktor schował artefakt. Zapytany co robi, odpowiedział, że naładował artefakt. Nie ma czasu odpowiadać na jej inne pytania - ona w jego oczach jedynie kupuje czas.

* "Tacy jak Ty nie dają prezentów, w dodatku takich" - Pięknotka, patrząc na artefakt służący do przerażenia i paniki
* "Ależ zostawiłem. Kilka groźnych bioform, które będą skutecznie siać terror i zniszczeni" - Wiktor z uśmiechem

Pięknotka dowiedziała się, że Wiktor ma dość tego, że magowie drażnią jego dom (Trzęsawisko) i ma zamiar Coś Z Tym Zrobić. Pięknotka chce od niego dwóch rzeczy: "odwołaj bioformy!" oraz "współpracuj a nie walcz". Drugie zignorował. Na pierwsze powiedział, że się zgadza - pod warunkiem, że ma spokojne przejście do domu. Zbyt dużo terminusi na niego polowali. Pięknotka nie widzi jak ma go zatrzymać; niech ma to przejście - ale za to niech Wiktor powie po co mu ten artefakt (Tp:7,3,4=SS).

Wiktor wyjaśnił swój plan - wyekstraktował strach i terror wielu ludzi. Wzmacniane magią Trzęsawiska będzie w stanie zbudować działo artyleryjskie. Powystrzela magów, którzy chodzą po jego domu - po jego Trzęsawisku. Niech zostawią go w spokoju. Jego i energię jego domu.

Terminusi uznali, że Pięknotka dając mu _safe passage_ zrobiła duży błąd.

Wynik:

* Tor Porażki: 1+3

Wpływ:

* Żółw: 7
* Kić: 1

**Scena**: Briefing w Barbakanie (22:00)

Terminusi zarzucili Pięknotce, że była bezradna na spotkaniu z Wiktorem. Ona zauważyła, że miała przeciwko sobie maga i pnączoszpona. To była jej decyzja że wydobyła przynajmniej informacje na temat tego co się dzieje. Inny terminus potwierdził - Pięknotka dowiedziała się jaki plan ma Wiktor. Na pytanie jakie Pięknotka ma plany, ta odpowiedziała że jeszcze nie wie. Terminusi ją zapewnili, że pomogą w miarę możliwości.

Tymon powiedział Pięknotce, że jest pewien plan - jest możliwość użycia działa orbitalnego Epirjona i zestrzelić tą artylerię. Więc jest plan B.

Pięknotka poprosiła katalistów o sprawdzenie ile mocy zdążył zebrać Wiktor Satarail w swoim artefakcie - chce mieć informacje jak groźne jest to, co on robi. (9,3,3=S). Jak tragicznie będzie jak się nie uda? Kataliści powiedzieli - niezbyt. To jest broń, dalekosiężna artyleria. Może być precyzyjna. Może zestrzelić jakiegoś awiana czy dwa. Nie jest w stanie jednak zniszczyć niczego bardzo ufortyfikowanego. Nawet ładowane energią Trzęsawiska nie może to strzelać dość często. Innymi słowy - powstanie czegoś takiego nie będzie absolutną katastrofą.

Pięknotka ma Plan. Przygotowała zbiór bomb aromatycznych, afrodyzjaki itp. Też zrobiła research na jego temat - kim był przed Skażeniem rodu. Doszła do tego - Wiktor Satarail kocha sztukę, a w szczególności muzea i muzykę. Biokonstruktor i kustosz muzeum, kustosz pamięci swego rodu. (12,3,2=SS;szybciej idzie). Cały czas próbuje uratować ród Satarail przed magami i znaleźć dla nich miejsce, gdzie będą bezpieczni. Między innymi dlatego osiadł na Trzęsawisku i nie pozwoli na to, by Trzęsawisko przestało być Wylęgarnią.

Plus, Pięknotka poznała też jego inne preferencje.

Pięknotka poszukała czegoś co może służyć jak dobrej klasy pozytywka z muzyką zgodną z jego upodobaniami. Odpowiednik 1-2 płyt. Zneutralizuje artefakt i artylerię używając miłości ^_^. No i dostała instruktaż od katalistów jak sobie z czymś takim radzić pryzmatycznie.

Wynik:

* Tor Porażki: 12

Wpływ:

* Żółw: 9
* Kić: 2

**Scena**: Operacja 'Neutralizacja' (22:30)

Trzęsawisko Zjawosztup.

Pięknotka poszła go znaleźć; jak to ona, bardzo ostrożnie; z power suitem (Tr:10,3,5=SS). Dotarła do Sataraila, acz z uszkodzonym power suitem. Wiktor przywitał ją uprzejmie acz lekko cynicznie w swoim biolabie. Zobaczyła jego księgozbiór - bardzo imponujący. To samo z samym biolabem organicznym. Pięknotka jest pierwszą czarodziejką, która zobaczyła faktyczną potęgę tego co ma Wiktor Satarail. Jest on zdecydowanie niedoceniony przez siły Pustogoru.

Więc zdecydowała się go uwieść i Skazić Pryzmatycznie artefakt z wszystkim co przygotowała. (Tr+3+Z:12,3,4=SS). Wiktor się zorientował, ale było za późno. Naprawdę się długo śmiał; Pięknotka zneutralizowała jego broń własnym ciałem i poświęceniem. Pięknotka zauważyła, że jako Diakonka, nie było to duże poświęcenie. Satarail w dobrym humorze odprowadził ją na skraj bagna. Jeszcze się spotkają.

Wynik:

* Tor Porażki: 14

Wpływ:

* Żółw: 13
* Kić: 3

**Epilog**:

* Skrzynia Duchów ma inny kolor emocjonalny; trafia do kogoś innego (ranteleport).
* Wiktor Satarail decyduje się nie atakować Zaczęstwa na razie tylko monitorować sytuację.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |  13     |     13      |
| Kić           |   5     |      5      |

Czyli:

* (K): Wiktor Satarail ma 'soft spot' do Pięknotki. (2)
* (K): -

## Streszczenie

Wiktor Satarail uderzył z Trzęsawiska. Zdecydował się zrobić Artylerię Koszmarów bazując na strachu ludzi z Mekki Wolności w Zaczęstwie by zeń zbudować artylerię na Trzęstawisku. Pięknotka starła się z nim dwa razy i dała mu odejść spokojnie, by tylko wejść na Trzęsawisko, uwieść Wiktora i zneutralizować Pryzmat artylerii. Wiktor docenił jej plan; pozwolił jej odejść i obiecał, że jeszcze poczeka z eliminacją magów Zaczęstwa.

## Progresja

* Pięknotka Diakon: Wiktor Satarail ma do niej lekką słabość.
* Pięknotka Diakon: uważana przez wielu terminusów za zdecydowanie niepoważną - dała odejść Satarailowi a potem strasznie ryzykowała chcąc rozmontować Artylerię Koszmarów.
* Wiktor Satarail: ma lekką słabość do Pięknotki. Soft spot. Pięknotka nie jest kill on sight.

## Zasługi

* Pięknotka Diakon: Wiktor Satarail miał wszystkie możliwe przewagi, więc "dała mu wygrać" - a potem go uwiodła i zneutralizowała koszmarną moc jego artylerii słodyczą.
* Wiktor Satarail: zirytowany tym, że magowie ingerują w Trzęsawisko Zjawosztup zdecydował się zrobić artylerię koszmarów. Artyleria została zneutralizowana przez słodką Pięknotkę w bardzo sprytny sposób.
* Felicja Melitniek: nie panikuje, próbuje pomóc ludziom na Nieużytkach Staszka straszonych przez Pnączoszpona, acz nieporadnie. Kieruje ludźmi, uspokaja ich. Powiedziała Pięknotce co zauważyła.
* Tymon Grubosz: wsparł Pięknotkę w przejęciu kontrolę nad Nieużytkami Staszka atakowanymi przez Pnączoszpona (który tylko ich straszył)

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Barbakan: miejsce poważnego spotkania i opieprzania Pięknotki przez innych terminusów
                            1. Zaczęstwo
                                1. Mekka Wolności: miejsce pojawienia się pnączoszpona i miejsce, gdzie Wiktor Satarail zebrał koszmary i strach wraz z energią magiczną
                                1. Nieużytki Staszka: Wiktor wykorzystał to miejsce do ładowania artefaktu energią z żywych magitrowni; prawie walka z Pięknotką.
                        1. Trzęsawisko Zjawosztup
                            1. Laboratorium W Drzewie: przesuwający się, ruchomy dom i biolab Wiktora Sataraila; ma tam ogromną kolekcję książek i materiałów

## Czas

* Opóźnienie: 1
* Dni: 1

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* Start z Grubej Rury 4
* Szybki wyścig z 1 torem

## Wykorzystana mechanika

1810
