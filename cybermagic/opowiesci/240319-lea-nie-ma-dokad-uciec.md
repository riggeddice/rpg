## Metadane

* title: "Lea nie ma dokąd uciec"
* threads: rekiny-a-akademia
* motives: the-inquisitor, the-warden, the-chainbreaker, the-pastmaster, heart-is-a-weakness, slipping-through-my-fingers, szczur-zapedzony-do-kata, teen-drama, polityczna-okazja
* gm: żółw
* players: anadia, kić

## Kontynuacja
### Kampanijna

* [240305 - Lea, Strażniczka Lasu](240305-lea-strazniczka-lasu)

### Chronologiczna

* [240305 - Lea, Strażniczka Lasu](240305-lea-strazniczka-lasu)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * Inspiracje
        * Ragnarok Online "czy miałam rację, mistrzu?"
    * L'ame Immortelle: "The truth behind"
        * "On the edge of this world | In the darkness of your soul | The only hold you have | Are your beliefs, that conquer all"
        * Starcie woli i starcie pomiędzy głębokimi wierzeniami. 
* Opowieść o (Theme and vision):
    * "Lea - strażniczka ludzi przed PRAWDZIWYM ZŁEM, która chroni ich większymi horrorami. Bo dookoła są złe rzeczy i lepiej ich nie przyzywać..."
    * "Wszyscy są spętani przez swoje oczekiwania i każdy może zrobić tylko to co może zrobić. Nie da się oszukać przeznaczenia, wszyscy zrobią to co muszą zrobić. Ekstremizm po obu stronach."
        * Napoleon Bankierz: AMZ, czarno-białe spojrzenie na magów krzywdzących ludzi i bawiący się na terenach Lasu. Wierzy w winę Rekinów, ale szuka Jednego Sprawiedliwego Rekina. Dla Triany.
        * Lea Samszar: Rekin, winna tego wszystkiego i zaszczuta. ZNOWU samotna. ZNOWU zdradzona. Zostaje jej jedynie godność. Tego jej nie odbiorą.
        * Marek Samszar: Rekin, "dajcie Lei spokój!" Ona może się zmienić. Ona może nie chciała zrobić krzywdy.
    * Lokalizacja: Podwiert, okolice (las)
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * the-inquisitor: Napoleon Bankierz; próbuje dojść do tego jak to się stało, że faktycznie pojawiła się tu tego typu anomalia. Próbuje zniszczyć to, co zagraża wszystkim i doprowadzić do sprawiedliwości.
    * the-warden: potwór stworzony przez Leę Samszar, bardzo niebezpieczny złomoform (XD) który ulega coraz większemu Skażeniu z czasem. Odpycha wszystkich z Lasu Trzęsawnego.
    * the-chainbreaker: Marek Samszar, który chce uwolnić Napoleona z jego ekstremizmu i Leę z jej szaleństwa. Dlaczego Rekiny nie mogą współpracować z AMZ?
    * the-pastmaster: Arkadia Verlen. Jest jak było. AMZ nie współpracują z Rekinami. Lea nie ma szansy na ewolucję. Marek zawiódł. Musi być tak jak było, nie da się tego zmienić. Magowie niszczą potwory i potwornych magów.
    * heart-is-a-weakness: kolejny raz Lea ma potwierdzenie, że powinna trzymać się tylko swoich spraw i swoich metod. Gdy próbuje pomóc, wszystko obraca się w pył.
    * slipping-through-my-fingers: Lea ma całkowity BSOD. Nie wie co robić, nie wie gdzie się oddalić. Może tylko lecieć... _tam_. Gdziekolwiek _tam_ nie jest.
    * szczur-zapedzony-do-kata: Lea. Nie ma innej opcji. Jest już poza swoim pokojem, gotowa by wyjechać na zawsze i pojechać GDZIEŚ. Zostawiła golema na swoim miejscu.
    * teen-drama: Lea (która nie chce zapłacić za to co zrobiła) - wszyscy inni (którzy są na Leę źli). Ale też 'wydamy Leę'?
    * polityczna-okazja: można mocno zintegrować Rekiny przeciwko temu co zrobiła Lea, bo tu akurat nikt nie będzie za nią stał. Lea jest winna.
* O co gra MG?
    * Lea zostaje wygnana, lub aresztowana i zamknięta. Marek Samszar obraca się przeciwko innym Rekinom.
    * Jak największe zniszczenia na obrzeżach Podwiertu - EfemeHorror Lei się manifestuje.
* Default Future
    * brak. Serio.
* Dilemma: Prawda i dobre stosunki z AMZ (Napoleon)? Wydanie Lei?
* Crisis source: 
    * HEART: "Lea musi pozostać nieznana, więc ukrywa swoją naturę"
    * VISIBLE: "EfemeHorror."
        
### Co się stało i co wiemy

* Bogdan Gwiazdocisz, stary 'archeolog' próbujący rozwiązać stare zagadki dotyczące m.in. jego przytułka
    * Poszukiwacze historycznych zagadek, AMZ + Rekiny
        * Detektywi umarłych - zaginęły dzieciaki, ale też noktianie
        * co tam się znajduje, jak to wygląda
* Lea postawiła EfemeHorror blokujący. Jednocześnie Lea miała kłopot z Ernestem.
* Co się stało gdy ekipa Bogdana spotkała się z EfemeHorrorem
    * Bogdan robi wyprawę by znaleźć ślady zaginionych i pomóc rodzinom dowiedzieć się co i jak.
    * Triana i Mariusz poszli z ramienia AMZ, Marek i Malena poszli z ramienia Rekinów. Marek chciał poprawić status Rekinów. Plus - może pomóc to pomoże. Malena - dla historii.
    * Poszli na pola i szukali duchów, robili operacje, dowiedzieli się czegoś nawet. Bogdan NAPRAWDĘ szuka historii.
    * NAGLE znikąd pojawił się potwór. I był za mocny. Po prostu.
* Gdy wyszło na jaw, że to wszystko Lea (przed Marysią), Lea nie chce powtórki z tego co było gdy ją wygnali do Rekinów. Sama odejdzie.
* Lea postawiła na swoim miejscu golema (w jej pancerzu bojowym), sama się spakowała i wyjechała.

### Co się stanie (what will happen)

* F1: Dwa oddziały
    * Ernest 'jesteś pewna że to nie ktoś z Twoich'? -> Marysia
    * Napoleon dołącza do oddziału polującego na EfemeHorror. Trzeba znaleźć winnego.
        * Oddział to: Arkadia, Karo, Daniel, Barnaba Burgacz, Franek Bulterier, Marek Samszar
    * Walka i polowanie na EfemeHorror. To ciężka bitwa.
    * Marysia dostaje prośbę od Marka, by znalazła w Hestii czy nie ma śladów. Znajdą tego Rekina jak to Rekin.
* F2: To wszystko Lea.
    * Scutier Lei (golem) na miejscu Lei
    * Marek próbuje być rozjemcą, wściekła Arkadia i Napoleon
* Overall
    * chains
        * Lea poza zasięgiem: (BrokenSoul - Ekstremalny - Heroiczny - out of range)
        * Budowa nienawiści do Lei: (idiotka - w łańcuchy - ostracyzm + kontrola)
        * Poziom zniszczeń: (niski - materiałowe - ludzkie - apokalipsa)
        * Reputacja Marysi: (kiepsko kontroluje - wcale nie kontroluje - wtf przez nią bo Lea to Rekin)
    * stakes
        * poziom reputacji Marysi
        * los Lwi
    * opponent
        * Lea vs Napoleon (zależy od strony graczy)
        * To Co Śpi Pod Lasem
        * EfemeHorror Lei, który uległ Skażeniu
    * problem
        * human drama XD
        * niektórych rzeczy nie powinno się badać

## Sesja - analiza
### Fiszki

* Lea Samszar: czarodziejka, źródło problemów, 24
    * ENCAO:  +-+-0 |Arogancja i poczucie wyższości;;Kłótliwa;;Królowa lodu;;Overextending| VALS: Face, Universalism >> Hedonism| DRIVE: Deal with pests
    * styl: Adelicia (Rental Magica)
    * przewaga: ogromna siła magii - Horrory (efemerydy strachu i koszmarów na podstawie sacrifice)
* Malena Barandis: czarodziejka, winggirl Lei, 25
    * ENCAO:  000-+ |Wyrachowana;;Proponuje niekonwencjonalne, dziwne i często śmieszne rozwiązania| VALS: Security, Tradition >> Family| DRIVE: Impress the superior / senpai
    * styl: młoda Asuka ale z nastawieniem na swoje korzyści / Blaster TF G1
    * przewaga: czarodziejka soniczna, elegancka arystokratka, BARDZO dobre zmysły (ród Barandis)
    * bardzo zainteresowana historią i rzeczami historycznymi

### Scena Zero - impl

brak

### Sesja Właściwa - impl

Marysia zrobiła dobrą robotę, jest zadowolona, wszystko przeszło. Oni poszli rozbrajać potwora, Marysia jest szczęśliwa.

* Ernest: Marysiu, jesteś pewna, że ten potwór to nie od Was? (luźne pytanie)
* Marysia: Skąd ten pomysł?
* Ernest: Bo skąd by się to wzięło? Potężny byt...
* Marysia: Jest to teren, na którym to nie jest dziwne, powojenny teren...
* Ernest: Właśnie DLATEGO że to powojenny teren uważam, że to ktoś od Was. Samo z siebie się nie uruchomi. A ja jestem z Eterni.
* Marysia: Może... nie wiem. Może ktoś nieświadomie uruchomił starą pułapkę, zobaczymy jak to rozwalą i co będzie dalej. Chciałabym oczyścić ten teren. Razem to wyczyśćmy.
* Ernest: Chwalebne, Marysiu. Myślisz... dobrze, spróbuję dojść do tego czy to ktoś od Was. Dowiem Ci się (mówi to jako mag który chce pomóc i mu zależy).
* Marysia: Jak to zrobisz?
* Ernest: Poszukam typów energii, pozbieram ślady i spróbuję znaleźć maga. Użycie tego typu typu rytuału... (coś ukrywa)
* Marysia: Tego typu czyli JAKIEGO? (jaki Ty jesteś mądry i męski...)
* Ernest: (20% więcej powietrza) Podejrzewam Leę.
* Marysia: Leę? Dlaczego?
* Ernest: Po pierwsze, śmieje się z Eternijskich zwyczajów. Po drugie, ona jako jedyna używa krwawych rytuałów.
* Marysia: Gdzie widzisz korelację?
* Ernest: Bo ten potwór - jak powiedzieli ta... Malena, i Patryk (z lekkim niesmakiem) - miał echo Krwi i czegoś starego. A nikt inny od Was nie jest tak tępy.
* Marysia: Poczekaj aż dostaniemy potwora
* Ernest: Lepsze próbki, sprytnie.
* Marysia: Dawno się w sumie nie widzieliśmy, zajmijmy się _good thing_...
* Ernest: Oki, masz rację, możemy poczekać.

Tymczasem, Karo. Do Waszego Zespołu dołączył AMZowiec.

* Arkadia: idź sobie.
* Napoleon: nie, tien Verlen. To też moja sprawa.
* Arkadia: będziesz nas spowalniał (singsong)
* Napoleon: Ten potwór skądś się wziął. Zranił Trianę oraz niewinnego człowieka. I maga, który chciał poderwać Trianę na pomoc, ale to inna kwestia. To moje miejsce, by pomóc.
* Arkadia: ... (zanim coś powie)
* Karo: widzisz, kogo mamy. Co Ty wniesiesz?
* Napoleon: Znajomość terenu? Ja jestem lokalny. Znajomość LEGEND? Wiedzę DETEKTYWA?
* Karolina: Ok, wchodzisz.
* Arkadia: (mina jakby zjadła spleśniałego szczura)
* Napoleon: (patrzy na Arkadię) ktoś tego stwora wezwał. Po prostu. Sam się nie mógł tu pojawić. To mógł być przypadek, to mógł być ktoś od nas...
* Babu: Ale myślisz, że to ktoś od nas, tak? (agresywnie)
* Arkadia: Wiesz, ma prawo tak uważać.
* Babu: (prychnął)
* Bulterier: (uśmiech) Rozwalimy potwora, potem rozwalimy maga który zrobił potwora.
* Napoleon: A ja pomogę Wam go znaleźć. Ten potwór zagraża ludziom w okolicy.
* Daniel: Ja też jestem detektywem. 
* Napoleon: Wpadłbyś na to, by szukać maga? Czy uznałbyś, że zadanie skończone gdy potwór znaleziony.
* Daniel: Touche. Ale skontaktuję się z Marysią - niech sprawdzi rejestry. Czy ktoś od nas to w ogóle mógł być.
* Karolina: Nie czekamy. Zaczynamy szukać.
* Napoleon: Dobrze. Zacznę poszukiwania.
* Karolina: Jedna rzecz. To mój zespół. Moje zasady.
* Napoleon: Akceptuję. Nie chcę Wam przeszkadzać. Chcę po prostu dojść i ukarać winnego - bo podejrzewam, po prawdzie, jakiegoś... dzikiego maga. Jakiś Paradoks. Nie Was. To się zdarzało.
* Babu: (prychnął, ale łagodniej)

Napoleon i Daniel zaczynają szukać potwora. Przy okazji, Marek tłumaczy z czym mieli do czynienia. Napoleon słucha i zadaje techniczne pytania. Daniel też. Napoleon i Daniel trasują teren. Co się dzieje. Jakie dane. Jak to wygląda.

Tr +2:

* X: Widzą ślady. Ale potwora nie ma. Potwór jest wyraźnie nieciągły.
* V: Są w stanie to wydobyć po tym jak zrobili reenactment (archeologiczne rzeczy)
* X: Potwór ich zaskoczył.

Błoto, pnącza i mechanizmy sformułowały takiego... pajęczaka, amalgamat, który zaatakował jednocześnie z kilku stron. Arkadia JUŻ jest w akcji.

Tr +4 +5Or: (rule: zawsze 5Or)

* V: Arkadii udało się odciągnąć ogień - jej noże nie są dość mocne; nie jest w stanie uszkodzić amalgamatu, ale jest w stanie osłonić detektywów.
    * ale TERAZ stwór wygląda inaczej.
    * +Vg -> Karo ostrzega, używa incendiary grenade
* Or: Stwór jest trafiony. I wygląda na to, że granat działa - wypala drewno, pnącza itp. Stwór "nadął się" i eksplodował - zewnętrzna bariera. Babu został trafiony i jego ścigacz płonie. Babu walnął ścigaczem w COŚ i wypadł z pojazdu, tocząc się po ziemi.
    * Daniel go przechwycił i złapał - ZANIM kolec go przebił na wylot.
* X: Stwór jest amalgamacją jakichś mechanizmów powojennych; Karo widzi co najmniej jeden ścigacz, co najmniej jeden czołg... pokręcone mechanizmy.
    * Za to sięga do ścigacza Babu by go 'inkorporować'
* Vm: Bulterier "OH HELL NO!" i annihilating blast magią, wypalając swoje palce. (+3Vm +3Ob). Energia Interis ZRANIŁA potwora, amalgamat się cofnął. Ale stoi. Ale nie zbliża się do Babu.
* Or: Arkadia "OSŁANIAJ MNIE, KARO!" i atakuje. Karo działa prawidłowo - ale amalgamat zaatakował spod ziemi, serią ciosów i szponów raniąc Arkadię i łamiąc jej tarcze. Arkadia się przewraca.
* Vr: Karo NIE TYLKO szybkim lotem łapie ranną Arkadię ale też wystawiła cholernego potwora. Jest słabość.
* X: Napoleon i Marek wystrzelili jednocześnie. Weszli sobie w szkodę. Amalgamat się odwrócił i słabość zniknęła, po czym skupił się na Napoleonie. Który wyjął granat. STARY granat.
    * wygląda jak coś, co się zabiera z AMZ i nie mówi profesorom
    * Karo rzuca ranną Arkadię Danielowi i przygotowuje czar który ma przejąć kontrolę nad martwymi mechanizmami. "SERVE ONE LAST TIME DAMN YOU!"
* Vm: Zaklęcie Karo skutecznie na MOMENT unieruchomiło potwora.
    * Bulterier walnął jeszcze jedną wiązką anihilacji. Zrobił dziurę w potworze. Napoleon rzucił granat.

Monster is no more. Anomalia została zniszczona. Zapadła cisza. (Daniel złapał Arkadię)

* Karo: "To było... trudniejsze niż myślałam..."
* Napoleon: Cholera... (przerażony)
* Babu: Dzięki za uratowanie ścigacza.
* Bulterier: Spoko. (założył rękawiczki)
* Arkadia: (cisza, lodowata, mordercza cisza) (jest zła i skupiona na upolowaniu sprawcy)
* Marek: ta bitwa była inna. Potwór był inny.
* Karolina: mów
* Marek: ten potwór miał inny kształt i inne umiejętności, nie z tym walczyliśmy, to by nas zmasakrowało
* Napoleon: nikt z Was tego nie mógł zrobić. To jest coś... coś co się obudziło.
* Daniel: poszukajmy mimo wszystko, jeśli ktoś to zrobił chcemy wiedzieć.
* Karolina: tak
* Napoleon: zabezpieczymy próbki. Ale musimy użyć magii. Nie jesteśmy w stanie... jeśli się coś spieprzy, prawda?
* Karolina: ...po to tu jesteśmy by to dostać. Ale tu się przyda Marysia.
* Napoleon: poproszę Ekaterinę i Laurę jako wsparcie ogniowe.
* Daniel: niekoniecznie. Zabiorą nam tę sprawę.
* Karolina: przyjdą, zabiorą i będą udawać że nic nam nie mówią dla naszego dobra
* Napoleon: może kogoś z AMZ, mamy kilku magów bojowych
* Karolina: przeżyliby tu? (szczere pytanie)
* Napoleon: ...nie.
* Karolina: to nie

Marysia miała południe życia. Super zabawa + _thrill emocji_. Zrelaksowana, zadowolona a Ernest też ma w głowie inne rzeczy niż polowanie na Leę. I jecie sobie jakieś smaczne rzeczy, zrobione przez Żorża, są SUPER. W szlafroczkach itp. Lub bez. I Marysia ma kontakt od Karo.

* Karolina: Marysia, potrzebuję wsparcia terminusa. Gabriel Ursus. Na wczoraj.
* Marysia: Co się stało?
* Karolina: Rozumiesz na wczoraj czy nie?
* Marysia: Może być w żołnierskich słowach, co się stało?
* Karolina: Wielki rozpierdol, dawaj Gabriela i NIKOGO INNEGO. Powiedz mu że sprawa Rodu.
* Marysia: (myśli czy nie można wykorzystać tego by ocieplić wizerunek Ernesta) Potwór związany z Magią Krwi?
* Karolina: (wzdycha i się opanowuje) Tak. Możesz...
* Marysia: Ernest jest bliżej, też może wam pomóc. Rozważasz czy bardzo nie?
* Karolina: WHATEVER. Ale ma być szybko i kompetentne. (wysyła namiary)

Marysia do Ernesta, szybko, ubierając się.

* Marysia: musisz się ubrać szybko. Asekurujemy Rekiny. Pomożesz?
* Ernest: (z uśmiechem paladyna) oczywiście, skarbie. Jeśli jest im potrzebny dobry mag, z przyjemnością pomogę.

3 minuty później, lecą. Ernest i sześciu żołnierzy, w tym Keira. Uzbrojeni po zęby. Karo była poinformowana (i poinformowała resztę).

* Arkadia: (przewróciła oczami)
* Babu: ślicznotek się znalazł. Zaś ukradnie chwałę.
* Napoleon: zamiast terminusa bierzemy tiena Eterni? Jesteś pewna, że AMZ nie wystarczy?
* Marek: Napoleon, nie rozumiesz. Tien Sowińska LUBI tiena Eterni. Bardzo lubi. Nie wychodzi mu z łóżka.
* Napoleon: jesteście zgubieni.
* Arkadia: nieważne, mamy dość siły ognia.
* Daniel: słuchajcie, on nie jest do kitu. Dobrze walczy. Da radę.
* Babu: też wchodzisz mu do łóżka?
* Daniel: (WPIERDOL RANNEMU BABU)
* Arkadia: (oddzieliła ich nożem) potem. Teraz chcę go aktywnego.
* Babu: dzięki! (zdziwiony)
* Arkadia: nada się na przynętę
* Babu: ... (aż nic nie powiedział)
* Daniel: (parsknął śmiechem)
* Napoleon: czy... czy Ty... zdeeskalowałaś to? (szok)
* Arkadia: (zdziwiona) w sumie (zaczerwieniła się) (odwróciła się od nich)
* Napoleon: (szczęka przy ziemi)

Ernest przybył, po 7 min. Rozstawione patrole, wszyscy gotowi, nawet Keira jest ubrana.

* Karolina: (stawia sytuację, co się stało i jak wygląda sprawa)
* Ernest: dobrze, będę Was asekurował.

Napoleon i Daniel, asekurowani, osłonięci rzucają zaklęcie ekstrakcji.

Tr Z M +3 +3Ob:

* Ob: energia _poszła_ w chory sposób. COŚ ją zasysa. Ernest przygotowuje się do działania, cały Zespół też.
* X: energia poszła _gdzieś indziej_. Asekuracja nie jest konieczna. COŚ ją wchłonęło...
* V: WIEMY, że na bazie zebranych próbek da się znaleźć kto za tym stoi. NIE WIEMY KTO, ale wiemy że się dowiemy.

...Ernest wymienił spojrzenie z Danielem i Napoleonem. Arkadia bierze jakieś narkotyki bojowe. Widząc minę wszystkich

* Arkadia: chyba... chyba nie myślicie, że tylko Wy się będziecie dobrze bawić? (ale widać, że to jest taki uśmiech lekko wisielczy)
* Ernest: słuchajcie, powiem szczerze - czy aż tak zależy nam na tym kto to zrobił? Bo... bo tu coś się dzieje.
* Karolina: cholera wie kto to zrobił, trzeba to rozpierdolić
* Daniel: Karo, ta energia poszła GDZIEŚ.
* Karolina: i dlatego trzeba to rozpieprzyć
* Napoleon: nie wiemy GDZIE. Nie idziemy w głąb lasu teraz.
* Karolina: nie, nie idziemy. Bardziej mnie martwi że to coś znajdzie kogoś innego.
* Ernest: ...cholera. (dotarło do niego)
* Karolina: nie, nie chcę iść do pieprzonego lasu, ale jak nie chcemy by ktoś tu padł nieprzygotowany...
* Karolina: ok, nie przepadamy za terminusami, ze wzajemnością, ale może to jest ta pora?
* Napoleon: tak, to ta pora.
* Karolina: jedna rzecz. Ja tam idę.
* Napoleon: poczekaj. 
* Karolina: nie sama, ale mnie nie wykluczą.
* Napoleon: ten las jest niebezpieczny. Procedura mówi, że teraz nie czarujemy i się wycofujemy. Serio. Większość fortifarm ma potężne defensywy. Są terminusi na patrolach. My tylko możemy wyrządzić szkody, tak jak jesteśmy.
* Daniel: Napoleon, mamy dalej połączenie magiczne, MOŻEMY dojść do tego o co chodzi. (widząc minę Napoleona) jeśli tam jest jakiś zły mag, że tak powiem, to mamy coś dla terminusów. Nie wykluczą nas.
* Daniel: Karo?
* Karolina: nie, to nie ta chwila. Tym razem nie. Nawet jak bufoniaści terminusi nas wykluczą... to nadal nie. Zbyt niebezpieczne.
* Napoleon: masz rację. (myśli cicho) Może jest przestrzeń do współpracy AMZ i Rekiny...

Zespół fortyfikuje pozycję, Marysia łączy się z Gabrielem Ursusem.

* Gabriel: tien Sowińska, co za przyjemność
* Marysia: terminusie Gabrielu, miło Cię słyszeć. Kontaktuję się z Tobą w delikatnej sprawie.
* Gabriel: zamieniam się w słuch i nie drążę
* Marysia: (nakreśliła sytuację - co się stało, co się dzieje itp), czy możesz pomóc? I oczywiście między nami? I nie informować o tym Pustogoru?
* Gabriel: tien Sowińska, sprawdzę co tam się dzieje. Przygotuję siły, praktykantów, wezmę ich na ćwiczenia.
* Marysia: (powiedziała jakie są szkody, nic nie ukrywa poza Leą)
* Gabriel: przyjąłem, zajmiemy się tym i nie wykluczymy Ciebie z pętli (szarmancki ukłon)
* Marysia: dziękuję

Karo, Napoleon i Daniel zostają. Arkadia, Babu, Bulterier się nie nadają do walki. Arkadia protestowała, ale to nic nie dało. Karo kopnęła Arkadię w kostkę i Arkadia zasyczała. Karo użyła to jako dowód by wycofać Arkadię.

Marysia zostaje. Marysia jako jedyna wie, że to robota Lei. I że się będzie dało dowiedzieć.

Marysia poszła porozmawiać z Leą. Lea jest... w ceremonialnej zbroi. Z przyłbicą i wszystkim. 

* Marysia: Czemu jesteś w zbroi?
* Lea: Tien Sowińska, jestem w domu, więc jestem w tym stroju w którym mi wygodnie
* Marysia: I dobrze. Moja droga. Rozumiem, że możesz po domu chodzić ubrana jak chcesz, ale zdejmij kapelusz z głowy albo przyłbicę, oczekuję, że zastosujesz się do tego.
* Lea: tien Sowińska, odmawiam wykonania tego polecenia, mając pełen szacunek
* Marysia: Dlaczego?
* Lea: Bo nie jestem w stanie tego zrobić. Jest to zapieczętowane. (ona nie wiedziała że przyjdziesz)
* Marysia: Ty to zrobiłaś sobie sama czy ktoś Ci to zrobił?
* Lea: Jest to coś, czego w danym momencie pragnęłam. Minie za parę godzin.
* Marysia: Potwór, który miał chronić... to co ma chronić, zrobił TO (pokazuje Lei rejestr walki)
* Lea: (nic nie mówi, jej puste oczy w przyłbicy, nic nie widać XD - NIE RUSZA SIĘ SZCZEGÓLNIE). Lea nie ma ruchu "normalnego". Stoi jak w serwopancerzu.

Marysia -> Hestia

* Marysia: Przeszukaj w bibliotece o co chodzi z Leą, ona sama czy ktoś ją zaczarował?
* Hestia: Tien Sowińska, pamiętasz te usprawnienia do kamer, do psychotroniki, to wszystko co miałam dostać a nie dostałam?
* Marysia: Przepraszam, Hestio... dla naszego wspólnego dobra... czyli nic nie jesteś w stanie pomóc?
* Hestia: Tego nie powiedziałam.

Hestia zaczyna szukać danych o Lei

Tr +3:

* V: Hestia wysłała sygnał alarmowy do Marysi. Lea nie jest na terenie Enklawy Rekinów.

Marysia - WŚCIEKŁA - opuściła mieszkanie Lei. I łapie ją na hipernecie.

* Lea: Tien Sowińska? To zaszczyt dostać od Ciebie wiadomość.
* Marysia: Leo, możemy się spotkać? Może przyjdę do Ciebie do domu.
* Lea: Przed chwilą byłaś u mnie w domu, z pełnią szacunku.
* Marysia: Ale myślę, że nie rozmawiałam z Tobą. Gdzie jesteś?
* Lea: Rozmawiałaś ze mną.
* Marysia: Widziałaś te obrazy które Ci pokazywałam?
* Lea: Ubolewam nad tym, ale tak.
* Marysia: Jak nie wrócisz teraz do domu, nawet ja nie mam jak Ci pomóc
* Lea: Tien Sowińska, nie oczekuję pomocy z Twojej strony. Masz jakiś plan, zrealizuj go, osiągnij korzyści, ja nie będę Ci przeszkadzać.
* Marysia: Chcesz dostać od nich wszystkich wpierdol?
* Lea: To takie nieleganckie słowo.
* Marysia: To opisuje co się stanie. Nie masz też nic przeciwko temu by się dowiedzieli jak naraziłaś na niebezpieczeństwo swojego przyjaciela bo się śmiali że jest Twoim przyjacielem?
* Lea: Zakładałam, że już wszyscy wiedzą, bo mogłaś to wykorzystać. 
* Marysia: Nie, nikt nie wie.
* Lea: Nie mam przyjaciół, tien Sowińska, a już na pewno nie wśród ludzi. Gdybyś go spytała, sam potwierdzi.
* Marysia: Ale może potwierdzić, że przyjaźniliście się wcześniej i bardzo Ci ufał, nie przypuszczał że mu zrobisz krzywdę. To nadszarpnęłoby nasze stosunki z ludźmi, z Pustogorem. Ród nie będzie zadowolony.
* Lea: Tien Sowińska, przy całej pełni szacunku mam GDZIEŚ to co Ród chce. Ode mnie, od Ciebie, od kogokolwiek.
* Marysia: Co spowodowało tę zmianę w Twoim podejściu?
* Lea: Tien Sowińska, nie czeka mnie tam nic dobrego i nie mam po co wracać do Rekinów.
* Marysia: Jak nie wrócisz, to Cię znajdę i złapię. To nie zostawi Ci żadnego wyboru i w sprawie będą terminusi. Zostaniesz znaleziona. Dokonujesz teraz życiowego wyboru i to nie jest sprawa tylko Rekinów i Aurum. To ostatnie minuty, w których możesz podjąć decyzję o powrocie. I dalszej przyszłości.
* Lea: Tien Sowińska, nie rozumiem (lekko jej się głos załamał).
* Marysia: Potwór był tak potężny, że ta grupa ze wsparciem Ernesta był tak potężny że potrzebował wsparcia terminusów. Obecnie jesteś uciekinierką, która porzuciła Aurum, Rekiny i jesteś uciekinierką za którą nikt się nie wstawi. Wiesz jak działa magia krwi. Zostawiasz ślady. Masz 3 minuty na podjęcie decyzji.
* Lea: Tien Sowińska. Decyzji o czym.
* Marysia: Wracasz do swojego domu czy będziesz się pałętać. Myślałas, że nie rozpoznasz konstruktu zamiast Ciebie?
* Lea: Czyli chcesz... czyli, tien Sowińska, albo wrócę, albo... (cisza) rzucisz przeciwko mnie terminusów, tak?
* Marysia: Ja nawet nie muszę ich rzucać przeciwko Tobie. Sami się zorientują. Wszyscy Cię będą szukać. A jak widzę...
* Lea: Podjęłam decyzję, wracam. (Marysia jest w stanie wyczuć głęboką rozpacz - Lea nie widzi rozwiązania)
    * Lea wie w jak bardzo beznadziejnej sytuacji jest

Marysia powiedziała Lei, że nie wraca na stracenie. Że spróbuje ją osłonić, ale będzie nieprzyjemnie i poniesie karę. Ale - Lea ma słuchać Marysi, być jej lojalną, nie czarować bez konsultacji, nie robić tego typu numerów i pamiętać, że Marysia jej pomogła.

Tr Z (Lea jest w sytuacji beznadziejnej i wie o tym) +3:

* X: Lea fundamentalnie wierzy, że Marysia to robi, bo ma z tego korzyści, ale Lea to rozumie. "Ona jest słaba, Marysia to wykorzystała, to jest najlepsze co można dostać"
* X: Wszyscy są wściekli na Leę za to co zrobiła. WŚCIEKLI. Chcą uczciwej zemsty i mają do tego prawo, bo z ich perspektywy wygląda to jak samowolka okrutnej arystokratki - a Lea NIGDY nie powie słowa w swojej obronie które zdradzi co ma w sercu. Woli cierpieć jako "okrutna arystokratka" niż przyznać się, że chciała pomóc.
* Vz: Lea pójdzie w plan Marysi. Wszystko co Marysia zażądała, Lea zrobi.
    * nie będzie czarować bez zezwolenia
    * będzie lojalna Marysi i będzie pamiętać że jej pomogła
    * będzie słuchać Marysi
    * (bo Marysia jest silniejsza i może wszystko i okazała łaskę bo Lea jest w jej planach przydatnym pionkiem)
* Vr: Marysia nacisnęła.
    * Marysia: Moja droga Leo, nie uwierzysz mi, że robię to ze względu na Twoje dobro. Ale dlaczego wolisz dostać wpierdol zanim powiedzieć, że możesz komuś pomóc. Co tak Cię skrzywdziło? (głos z wyrazem troski)
    * Lea: Jeśli komuś spróbuję pomóc, będzie na mnie liczył. Może mi zaufać. Straci wszystko. Zostanie zniszczony. Lepiej, by nigdy mi nie zaufał, niż żebym musiała skrzywdzić go później, w bezpiecznych warunkach.
    * Marysia: Możesz złamać jego zaufanie?
    * Lea: Nie, chodzi o to, co stało się Michałowi. Zaufał mi, a potem... potem go poświęciłam w rytuale. Gdyby nigdy mi nie zaufał, nigdy by go to nie spotkało.
    * Marysia: Czemu musisz krzywdzić wszystkich którzy Ci zaufają? Kto Ci każe?
    * Lea: Bo jeśli tego nie zrobię, to KIEDYŚ będzie moment próby i WTEDY ich zniszczę. Lepiej - wcześniej, kiedy są w stanie kontrolować ból. Kiedy nic nie tracą.
    * Marysia: Czemu tak musi być? Czemu ktoś Cię poddaje próbie?
    * Lea: Tien Sowińska. Bo każdego da się złamać. Mnie też.
    * Marysia: Ktoś Cię próbował złamać?
    * Lea: Nie. Ktoś mnie zawiódł. I byłam sama. I nie wygrałam. I nikt po mnie nie wrócił. I nie życzę tego nikomu.
    * Lea: (kontynuuje) Mogliśmy zniszczyć tego ducha. Ale uciekli. I nie wrócili. Nawet jak byłam sama, nawet... po tym czasie, nikt nie wrócił.
    * Lea: Ale ja wróciłam. Sama, tien Sowińska. Tak jak Ty. Sama, z planami i działaniami, i pionkami.
    * (Marysia się dowiedziała jeszcze kilku detali - ani duchy Lei nie pomogły ani inni magowie. Jej moc uległa uszkodzeniu. Jej Wzór uległ uszkodzeniu. I wtedy Lea po raz pierwszy zaczęła używać Horrorów.)

Lea wraca. Marysia planuje.

## Streszczenie

EfemeHorror Lei uległ Skażeniu przez To-Co-Jest-W-Lesie-Trzęsawnym. Zespół Karo + Napoleon starli się z tym potworem i dostając ciężkie rany wśród Rekinów dali radę zniszczyć amalgamat. Marysia konfrontuje się z Leą, ale okazuje się, że ta uciekła. Marysia zaznacza, że przecież może wezwać terminusów. Zastrasza Leę - obiecuje jej ratunek, ale pod warunkiem pełnej lojalności i współpracy, na co Lea się zgadza. Lea wraca, złamana, przekonana, że wraca do piekła.

## Progresja

* Barnaba Burgacz: walcząc ze Skażonym EfemeHorrorem Lei jego ścigacz został ciężko uszkodzony a on sam jest poturbowany. Parę dni w gliździe mu pomoże.
* Arkadia Verlen: walcząc ze Skażonym EfemeHorrorem Lei została zaatakowana spod ziemi i ciężko pocięta ostrzami. Niezdolna do dalszej walki. Tydzień w gliździe jej pomoże.
* Franek Bulterier: walcząc ze Skażonym EfemeHorrorem Lei odpalił kilka anihilacyjnych wiązek, co popaliło mu palce. 3 dni w gliździe i będzie zdrowy.
* Lea Samszar: nie jest w stanie opierać się Marysi; poddała się. Będzie jej słuchać, będzie jej lojalna, nie będzie czarować. Ale zachowa godność.
* Lea Samszar: wierzy, że Marysia jej pomaga bo chce mieć z niej posłusznego pionka. Lea będzie grać w tą grę, bo ją rozumie i w sumie nie ma wyjścia.

## Zasługi

* Marysia Sowińska: skutecznie przekonała Leę do powrotu i wymusiła współpracę, obiecując ochronę; zapewniła Ernesta a potem Gabriela, by wspierał grupę po pokonaniu EfemeHorrora.
* Karolina Terienak: dowodziła zespołem w konfrontacji z EfemeHorrorem, umiejętnie koordynując działania i wykorzystując zarówno taktykę, jak i magię, by zminimalizować zagrożenie i ostatecznie pokonać przeciwnika. Przekonała Daniela, by jednak zaniechali dalszych poszukiwań. Włączyła Napoleona w grupę.
* Lea Samszar: widząc że wszystko staje przeciw niej, wycofała się i zdecydowała odjechać. Gdy Marysia ją zaszantażowała, wróciła do Rekinów. I tak nie ma jak wygrać. Zachowuje godność, nie skarży się i przyjmuje los. Powiedziała Marysi o wydarzeniu z przeszłości które ją tak zmieniło.
* Ernest Namertel: użył swoich umiejętności magicznych, by wspierać grupę w starciu z EfemeHorrorem, a także przyczynił się do zabezpieczenia terenu po walce
* Daniel Terienak: pomógł w analizie i śledztwie dotyczącym źródła EfemeHorroru, wspierając grupę swoim doświadczeniem detektywistycznym. Uratował Babu i Arkadię przed ciosami EfemeHorrora
* Arkadia Verlen: aktywnie uczestniczyła w walce z potworem, odciągając ogień i chroniąc innych, mimo że została ciężko ranna; prycha na obecność Napoleona i Ernesta, ale akceptuje dowodzenie Karo.
* Marek Samszar: walczył z EfemeHorrorem, nawet jeśli mało skutecznie. Kluczowa wiadomość - on wie, że Horror się zmienił. Chce bliższej integracji AMZ i Rekinów.
* Barnaba Burgacz: prycha na Napoleona a zwłaszcza Ernesta, ale walczy przeciw EfemeHorrorowi. Dostał najmocniej, na początku, ale wyciągnął go Daniel.
* Franek Bulterier: działał jako artyleria magiczna, raniąc siebie by SMAŻYĆ EfemeHorrora, kosztem swego zdrowia. Powstrzymał EfemeHorror od zdobycia ścigacza Babu.
* Napoleon Bankierz: aktywnie uczestniczył w walce, ale również zaproponował racjonalne podejście do badania i rozwiązania problemu EfemeHorroru. Jest tu by poznać prawdę i pomóc z ramienia AMZ.
* Gabriel Ursus: na prośbę Marysi szybko zareagował, organizując dyskretnie wsparcie terminusów i zabezpieczenie EfemeHorrora, nie informując o niczym Pustogoru.

## Frakcje

* .

## Fakty Lokalizacji

.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Dzielnica Luksusu Rekinów
                                1. Las Trzęsawny

## Czas

* Opóźnienie: 1
* Dni: 1
