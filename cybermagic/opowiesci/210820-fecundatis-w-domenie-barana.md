## Metadane

* title: "Fecundatis w Domenie Barana"
* threads: corka-morlana, planetoidy-kazimierza
* gm: żółw
* players: kić, skobel, mactator

## Kontynuacja
### Kampanijna

* [210813 - Szmuglowanie Antonelli](210813-szmuglowanie-antonelli)

### Chronologiczna

* [210813 - Szmuglowanie Antonelli](210813-szmuglowanie-antonelli)

### Plan sesji
#### Co się wydarzyło

* Inicjacja bazy Ukojenie Barana
    * mag eternijski, Bruno Baran, założył niewielką bazę daleko od Eterni z częścią ludzi 12 lat temu
    * automatycznie zaczął się kręcić profit
    * wśród górników Elsa Kułak została nieformalną liderką. Niestety, "demokracja" nie przypasowała Baranowi. Odizolował się.
* Migracja Mardiusa
    * noktiańscy komandosi Tamary Mardius złożyli tu swój nowy dom wśród lapicytu. Podeszli do górników i Barana z neutralnością. Ot, są.
    * mało kto jak noktianie umieją żyć w kosmosie. W tym miejscu znaleźli świetne miejsce i ich umiejętności stały się bardzo przydatne.
    * siły Mardius znalazły opcję szmuglowania przez statek Gwiezdny Motyl.
* Imperium Blakvela
    * 3 lata temu pojawił się eternijski szlachcic, Ernest Blakvel. Stwierdził, że władza w kosmosie to coś dla niego. Zaczął umacniać się w tym terenie.
    * Blakvel i Mardius weszli w stały konflikt. Mardius chroni miejscowych, Blakvel chce ich podbić.
* Pojawienie się Strachów
    * 2 lata temu pojawiły się Strachy. Stworzone z programowalnej materii (morfelin), nie wiadomo czemu są i czym są.
    * Mardiusowcy aktywnie chronią lokalnych. Blakvelowcy zastawiają pułapkę na Mardiusowców ;-).
* Miragent w kosmosie
    * Miragent został przechwycony z Gwiezdnego Motyla. Mardiusowcy nie chcieli jego zniszczenia, więc go odratowali i przeszmuglowali z powrotem na Ukojenie.
    * Miragent posłużył do zadania serii strasznych ciosów Blakvelowcom.
    * Gdzieś po drodze Mardiusowcy utracili kontrolę / kontakt z miragentem.
* Aleksandria
    * Zniknął Leon Kantor. Mardius wysłała 11 agentów.
    * Znaleźli Aleksandrię, uruchomioną przypadkowo przez Leona. Aleksandria porwała Tamarę Mardius.
    * Deneb zorientował się co się dzieje, ale miragent uderzył pierwszy i jako Deneb zaprowadził część oddziału Mardius do Aleksandrii.
    * Deneb został zmuszony do ewakuacji do ostatnich baz; Blakvelowcy z radością to wykorzystali.
* Jolanta Sowińska + Tomasz Sowiński
    * Jolanta wie co się stało z miragentem i że przechwycili go Mardiusowcy. Ma kody kontrolne i chce go przechwycić.

#### Strony i czego pragną

* Górnicy w Domenie Ukojenia i mały biznes: 
    * CZEGO: zachować niezależność, handel itp. Przetrwać. Zarobić.
    * JAK: sami się zbroją
    * OFERTA: standardowa oferta terenowa, plotki itp.
    * KTOŚ: Elsa Kułak (przełożona, trzyma twardą ręką), Antoni Czuwram (górnik, solidny stateczek i niezły sprzęt - dużo wie), Kamil Kantor (szuka ojca Leona który odkrył sekret Mirnas i zaginął), Bruno Baran (mag eternijski i założyciel; Strachy to destabilizacja Blakvela przy obecności lapizytu), Kara Szamun (młódka z rodziny Szamun, pilot i nawigator transportowca na Asimear.)
* Przemytnicy Blakvela: 
    * CZEGO: zdobyć prymat w Domenie Ukojenia
    * JAK: zmiażdżyć Mardiusa, potem przejąć Talio i zestrzelić bazę Barana.
    * OFERTA: świetne rozeznanie w pasie, największa siła ognia, wiedza o SWOICH skrytkach, wsparcie piratów etnomempleksu Fareil (synthesis oathbound pro-virt, pro-numerologia)
    * KTOŚ: Ernest Blakvel (arystokrata eternijski)
* Przemytnicy Mardiusa: 
    * CZEGO: zregenerować siły, odzyskać Tamarę, ukryć się przed Blakvelem
    * JAK: w ciągłej defensywie, ufortyfikowanie Sarnin, operacje militarne anty-Blakvel
    * OFERTA: wiedza o Miragencie i Aleksandrii, świetne rozeznanie w pasie, wiedza o SWOICH skrytkach, znajomości wśród lokalnych.
    * KTOŚ: Tamara Mardius (eks-komandos noctis, Alexandria), Deneb Ira (regeneruje komandosów po sprawie z Aleksandrią)
* Miragent: 
    * CZEGO: wolności od wszystkich i wszystkiego
    * JAK: hunter-killer, destabilizacja terenu, pułapki, zamaskowane jako Strachy jednostki Kuratorów (arteval-class).
    * OFERTA: ukojenie Aleksandrii, poddanie się Blakvelowi
* Taliaci: (mieszkańcy Talio)
    * CZEGO: rzeczy wysokotechnologiczne
    * JAK: handel, współpraca
    * OFERTA: mają dostęp do WSZYSTKICH frakcji i są wiecznie neutralni, reaktory termojądrowe na podstawie deuteru i trytu na Talio.
* Strachy
    * CZEGO: Brak woli. Reakcja. Zniszczyć wszystko.
    * JAK: flota inwazyjna
    * OFERTA: zniszczenie, targetowane lub nie.

#### Sukces graczy

* Sukces
    * PRIMARY: odbudowanie stabilności terenu
    * SECONDARY: odzyskanie miragenta
* Porażka
    * Blakvel zdobywa Antonellę
    * Zespół jest zdominowany przez Aleksandrię lub siły Blakvela

### Sesja właściwa
#### Scena Zero - impl

DWA TYGODNIE PRZED SESJĄ WŁAŚCIWĄ.

Rick został poproszony przez Tamarę Mardius o pomoc. Uczestniczy w akcji ratowania / szukania Leona Kantora, bo rozbił się niedaleko statek Ricka a ów jest dobrze uzbrojonym najemnikiem który potrafi przydać się doświadczenia. Po drodze eksplorując miejsce gdzie Leon _mógł_ być, Rick napotkał sporo Strachów.

Czemu Strachy tu są? Czemu nie gdzieś indziej? I nie dać się wykryć.

TrZ+2:

* X: Strachów jest całkiem sporo. Są dziwne. I jest tam ktoś jeszcze - Blakvelowcy.
* V: Blakvelowcy na kogoś polują, unikając Strachów. Rick ostrzegł Blakvelowców, którzy mu podziękowali. 
* V: W kierunku na jedną z planetoid są dziwne Strachy - jakby bez imaginacji. Przekonany, że część Strachów została zniszczona przez Kogoś Innego.
* XX: Strachy atakują, Rick się wycofał.

...

* Q: Dlaczego Brighton wie, że może ufać Rickowi?
* A: Sole survivor oddziału w którym służył syn. Dostarczył pamiątki. Znalazł na Fecudantis safe harbor.
* Q: Dlaczego dla Ricka jest problemem narastająca obecność Strachów na tym terenie?
* A: Zainwestował w kopalnię minerałów która aktualnie jest pożerana przez Strachy.
* Q: Czemu Brighton - Blakvel?
* A: Bo Kara chciała mieć osobistą wolność a Blakvel chcą kontroli.

#### Scena Właściwa - impl

Fecundatis. Zabezpieczony lądownik niedaleko Szamunczak (bo przecież Fecundatis jest za dużą jednostką i działa z ukrycia; ma pewne wąty do Blakvelowców i vice versa - Brighton jest raczej po stronie Kary niż Blakvela). Brighton pyta Flawię, co Flawia z tego ma że ta pomaga Antonelli i Jolancie. Flawia odpowiada, że kasę. Brighton nie wierzy. 

Dobra, nie dojdziemy - więc sprawdźmy co tam się dzieje. Sytuacja jest dynamiczna.

Flawia skanuje, dotarli bezpiecznie do Szamunczaka. W "Koronie" Blakvelowcy piją i dają kolejki lokalnym. Elsa widzi Brightona i macha do niego. Pije na smutno. Brighton z Flawią przy ramieniu idzie w stronę Elsy... a Flawia robi z siebie maksymalnie piękny punkt ściągający CAŁOŚĆ uwagi na siebie i Brightona. Świetnie. Dokładnie NIE o to chodziło Brightonowi...

Tymczasem w Koronie Rick pije z Damianem (uratowanym od Strachów). Narzekają na brak kobiet. A tu wpada Brighton z Flawią... Rick niedźwiedź przyjaźni -> Brighton. Sytuacja jest nietrywialna. Brighton - będą pić na statku. Flawia jest wyraźnie zagrożona. Rick ma się nią zająć, Brighton idzie do Elsy korzystając z dywersji w formie Flawii...

Tymczasem Flawia bawi się, kokietuje, wyciąga z Blakvelowców opowieści o dokonaniach, okolicznościach. CEL: co się tu dzieje. TrZ+2:

* V: Mardiusowcy praktycznie zostali zmiażdżeni. Tamary - nie ma. Od ponad tygodnia. A ich "dowódca" ich zdradził. I ten dowódca też nie żyje. Deneb występował w dwóch miejscach w tym samym czasie? Miragent?
* X: Nie dadzą ci tak po prostu odejść.
* V: TAK, masz kierunek, masz mniej więcej info gdzie Deneb miał ich zabrać. Kilkanaście osób.

Elsa -> Brighton. Przerażona. Kara - niby kogoś zabiła (i są świadkowie!). Kara mówi że jej nie było. Aresztowana. W dwóch miejscach? A jak chodzi o Blakvela - zniszczył Mardiusowców. A przynajmniej wszystko na to wskazuje.

Flawia próbuje "przykleić" się do Damiana i zachęcić do gry w rzutki. (Damian jako zasób bo największy)

TrZ+2:

* V: Flawia "wybrała" Damiana. Spontaniczna, roześmiana - ale nie ladacznica.
* VV: Damian niezborny
* X: "Ze mną się nie napijesz"
* V: Przetrwała. Flawia przetrwała.

Oderwanie. Idzie z nim do statku.

ExZ+2 --> TrZ+2+2+3O:

* XX: Oderwanie niemożliwe. "No chodźcie moje sikorki" - zarówno Flawię jak i Ricka
* V: Powłączałaś tryby "awaria, przebicie, usztywnić, kończyna..." - koleś jest poważnie unieszkodliwiony
* V: Nie ma to jak psychotropy. Jak już są na statku, skafander w dół, Flawia x psychotropy i spokój.

Wszyscy razem (Flawia, Rick, Brighton...) są na Fecundatis.

Brighton mówi, że Kara ma problemy. I jej wierzy. Flawia wyjaśnia miragenta. Potem Jolanta dodaje informacje i mówi, że mają kody kontrolne. Rick się zdziwił - czyżby Blakvel współpracował z miragentem?

Niech Fecundatis oficjalnie pojawi się na planetoidzie. Jest większy niż Lord Szamun. Jeśli miragent kieruje się tym by przechwycić duży statek - chce obrać na cel Fecundatis. Ale na pewno Ernestowi Blakvelowi taki statek by się przydał. Więc - wystawiamy się na ryzyko z perspektywy różnych sił.

OGROMNA fala Strachów zmierza w kierunku na Szamunczak - lecą bezpośrednio w tą stronę. Plus, NAPRAWDĘ dużo. Brighton --> Elsa, ewakuacja.

Trawler rozpościerający pajęczynową sieć (<3 Flawia). Fecundatis rozpościera sieć (<3 Brighton, który dołożył do tego swoje skorpioidy by pomóc Flawii). Rick, który przekonuje zastępcę Damiana, że to jego chwila na zostanie wielkim mistrzem i detronizowanie Damiana ;-).

Rick --> Antos. Próba przekonania, że da się pokonać te Strachy. Nazwisko Nataniela Morlana (broń dla Morlana, Fecundatis) + jak nie zrobi to obudzi się Damiana.

TrZ+3:

* Vz: zazdrość. Antos CHCE być mistrzem.
* V: na szali. Wszystko.
* V: Antos chce się wykazać. Ujednolicenie sił Blakvela itp.
* V: Antos jest BOHATEREM. Dookoła niego się konsoliduje. I on jest wdzięczny.
* XX: Naruszenie struktury, straty i w siłach Blakvela i w siłach cywilnych. Poważne uszkodzenia.

Flawia --> skorpioidy. Rekombinacja w odpowiednie "pajęczynki" by potężnie wzmocnić siły Blakvela.

ExMZ+3:

* X: Skorpioidy wymrą. To jednostronna operacja. Zostaje 1k6 dla Antonelli. ()
* V: Bardzo skuteczna sieć "pajęcza". Ograniczamy straty i spowalniamy ataki.
* XX: Bez Antonelli nic nie da się zrobić. Jest zdestabilizowana. Jolanta też ucierpiała. Obie nie mają dostępu do magii.
* X: Czerpiemy energię z innych członków załogi. Pełna moc. Pomniejsze Skażenie Esuriit.

Brighton --> Fecundatis. Odpowiedni pilotaż, rozłożenie itp. Rozkładanie w miarę ręczne przez Brightona + pełna koncentracja załogi (mimo Skażenia Antonelli), bo na szali jest życie Brightona. I pintka która ma zasłaniać:

Tr(3O)+4:

* X: pintka poświęciła się dla dobra ludzkości i swego kapitana

Dodanie Jolanty i jej Elainki i maszyn.

* V: SUKCES! Rozłożone.
* V: Wsparcie. Posiłki Kuratorów.
* O: Jolanta w silnym szoku - link Jolanta-TAI się rozsypuje
* V: Brighton odpala pintkę --> niech zasłania. DUŻO mniejsze straty. Antos SUPERBOHATER ale Fecundatis też.

"Czy jest sens tyle walczyć, cierpieć - sami sobie to robicie".

Flawia próbuje się zorientować co to do cholery jest. Ma sygnaturę broni dzięki Rickowi.

TrZ+3 (niepełna):

* V: Mamy do czynienia z Kuratorami.

RICK PODWAŻA KOMPETENCJE KURATORÓW ODNOŚNIE TEGO CZY MOŻE POMÓC XD. W dodatku - nie są stąd. Nie wiedzą wszystkiego. Flawia z uprzejmością dorzuca chemiczne fakty o skorpoidach + uszkodzony, trochę "zaszumiony" waveform Jolanty.

Cel Ricka: po pokonaniu Strachów - niech Kuratorzy będą Kuratorami gdzieś indziej.

TrZ+4:

* X: Część osób już zabranych (odratowanych z pola bitwy) trafi do Aleksandrii.
* V: Kuratorzy się wycofają. (od 2-3 tygodni Tamara / Leon zniknęli)
* X: Kuratorzy się nauczyli i zrozumieli - ten typ sztuczek nie zadziała. Są pewni że mogą pomóc.
* V: Tydzień procesowania przez Kuratorów.
* V: Wiemy, gdzie jest baza Kuratorów - jest konkretne miejsce i planetoida.

Trzeba się pozbyć Kuratorów. Ale jak? 

POTENCJALNY POMYSŁ: na dzień dobry wysłać tam oddział ludzi olapicytowanych albo lapicytujemy Aleksandrię...

A Antos (jako bohater) --> redukcja stresu i energii negatywnych --> redukcja Strachów.

Damian zostaje desygnowany na przynętę na Kuratorów, bo on po tym wszystkim na pewno nie chce się obudzić...

## Streszczenie

Fecundatis dotarł do Domeny Ukojenia w Pasie Kazimierza. Na miejscu - Blakvelowcy zmiażdżyli Mardiusowców; Strachy mają kolejną ofensywę i wszyscy się boją. Po przeanalizowaniu sytuacji Fecundatis zdecydował się na znalezienie i przejęcie miragenta, ale przeszkodziła im inwazja DUŻEJ ilości groźnych Strachów. Zespół skutecznie odparł Strachy (przy pewnych stratach) i wyniósł Antosa do roli lokalnego bohatera i jednoczyciela. Mieli jednak wsparcie Kuratorów... którzy m.in. Tamarę Mardius wpakowali już do Aleksandrii...

## Progresja

* Jolanta Sowińska: rozszczepienie Jolanta - Elainka; Jolanta jest niezborna i ma problemy z dostępem do swojej magii / niektórych funkcji.
* Antonella Temaris: pomniejsze Skażenie Esuriit; przepuściła przez siebie za dużo Krwi i energii Krwi.
* Rick Varias: zawsze ma miejsce w Szamunczaku za obronę przed Strachami. Wielkie osiągnięcie.
* Rick Varias: Antos Kuramin, "bohater ludu" jest mu bardzo wdzięczny za działania Ricka i wybicie Antosa na szczyt.
* Cień Brighton: zawsze ma miejsce w Szamunczaku za obronę przed Strachami. Wielkie osiągnięcie.
* Flawia Blakenbauer: zawsze nosi przy sobie psychotropy "na miło i wesoło" oraz środki unieszkodliwiające.
* SC Fecundatis: zapas skorpioidów spadł do zera. Mamy tylko królową i jednego malutkiego skorpioida, uratowanego przez Antonellę (oczywiście).

### Frakcji

* .

## Zasługi

* Flawia Blakenbauer: zaczęła od flirtu zbierającego info co się dzieje w okolicy, potem psychotropowała Damiana i skończyła na tym, że magią (wspierana przez Antonellę i Esuriit) syntetyzowała skorpioidami pajęczynę do neutralizacji Strachów. Wymęczyło ją to, ale dała radę.
* Cień Brighton: zdecydował się pomóc Elsie i Karze (eliminacja miragenta); zaryzykował Fecundatis, by tylko ochronić Szamunczak przed armią Strachów. Stracił cargo (skorpioidy) i trochę zdrowia, ale dał radę.
* Rick Varias: kiedyś: twardy najemnik. Uratował Blakvelowców przed Strachami, potem przekonał Antosa by ów dowodził obroną Szamunczak a na końcu wmanipulował Kuratorów w opuszczenie tego miejsca na tydzień bo nie mogą pomóc XD.
* Antonella Temaris: poszła za planem Jolanty i użyła mocy eternijskiej Esuriit by przekierować energię z Jolanty (a potem: załogi) by dać Flawii dość energii do nadprodukcji skorpioidowej pajęczyny. Lekko Skażona Esuriit.
* Jolanta Sowińska: autorka pomysłu użycia Antonelli i jej wiedzy o Esuriit jako baterii do przekształcenia skorpioidów; drugim torem zasilała Fecundatis swoją Elainką. Doszło do rozszczepienia ona - TAI.
* Damian Szczugor: przeprowadzając operację polowania na Mardiusowców prawie wpakował się na Strachy i Rick go ostrzegł. Potem gdy świętował w Koronie, przyczepiła się doń Flawia i go poderwała. Walnęła mu psychotropami że było mu dobrze. Przespał największą walkę ze Strachami i jak się obudził - nie był już "prawą ręką Ernesta Blakvela" na tym terenie XD.
* Antos Kuramin: "zastępca" Damiana Szczugora; Rick go przekonał, by poprowadził operację obrony Szamunczak przed Strachami. Antos to zrobił - kompetentny zeń żołnierz. Został bohaterem ludowym, uznanym przez Blakvelowców i lokalsów.
* Elsa Kułak: dowodzi społecznością Szamunczaka, chwilowo zagubiona i pije na smutno. Nie wie jak odeprzeć Blakvelową potęgę i straszliwe Strachy... zwierza się Brightonowi.
* Kara Szamun: dowodzi Lordem Szamunem; najważniejszym transportowcem. Podobno zabiła kogoś - ale to najpewniej miragent zabił udając ją. Jest chwilowo aresztowana.
* Leon Kantor: świetny górnik, przyjaciel Tamary Mardius. Zniknął, bo przypadkowo uruchomił Aleksandrię przy jednej ze swoich wypraw.
* Deneb Ira: XO Tamary Mardius, chwilowo dowodzi Mardiusowcami. Wycofał wszystkie bazy i skrył się w fortecy, której Blakvel nie do końca może złamać. Miragent podszywając się za niego wprowadził 20% jego oddziału do Aleksandrii...
* Tamara Mardius: noktianka, jej grupa zbudowała sobie dom w Domenie Ukojenia. W ostrym starciu z Blakvelowcami. Wpadła do Aleksandrii.
* Bruno Baran: założyciel Domeny Ukojenia, wycofał się na ubocze (nie lubi demokracji). Sporo wie o tym terenie, Strachach itp - ale się dąsa i nie chce wyjść.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Pas Teliriański
                1. Planetoidy Kazimierza
                    1. Domena Ukojenia: oddalony od Asimear "teren" nieformalnie objęty przez Bruno Barana z nadmiernym stężeniem lapicytu i morfenu. Cierpi na STRASZNY niedobór energii.
                        1. Planetoida Mirnas: kiedyś miała służyć jako magazyn dla grupy przemytników Mardiusa. W planetoidę wbity jest statek Kuratorów. Aleksandria odkryta przez Leona i przezeń uruchomiona...
                        1. Stacja Ukojenie Barana: złożona z kilku zespawanych statków. Stosunkowo blisko Mirnas. Wykorzystuje niewielki reaktor amat. Chwilowo Baran jest tam zamknięty i się dąsa
                        1. Planetoida Talio: lodowa planetoida, źródło wody dla Domeny Ukojenia. Są tam też reaktory jądrowe wykorzystywane przez wszystkich.
                        1. Szamunczak: zbiór struktur powiązanych mostami i stabilizatorami; trochę RAMA trochę planetoidy. Najbliższe cywilizacji.
                            1. Klub Korona: JEDYNY klub i bar w Domenie Ukojenia. Flawia tam inspirowała Damiana i innych Blakvelowców do tego by gadali.

## Czas

* Opóźnienie: 11
* Dni: 3

## Konflikty

* 1 - Tymczasem Flawia bawi się, kokietuje, wyciąga z Blakvelowców opowieści o dokonaniach, okolicznościach. CEL: co się tu dzieje.
    * TrZ+2
    * VXV: Mardiusowcy zmiażdżeni, wiadomo jaki jest kierunek na Aleksandrię (jeszcze nie wiemy że A.)
* 2 - Flawia próbuje "przykleić" się do Damiana i zachęcić do gry w rzutki. (Damian jako zasób bo największy)
    * TrZ+2
    * VVVXV: Damian niezborny, Flawia musiała się z nimi napić ale przetrwała
* 3 - Oderwanie. Flawia idzie z Damianem do statku i chce zniknąć.
    * ExZ+2 --> TrZ+2+2+3O
    * XXVV: nie da się oderwać od Damiana, ale psychotropy go wyłączyły. "Było mu dobrze".
* 4 - Rick --> Antos. Próba przekonania, że da się pokonać te Strachy. Nazwisko Nataniela Morlana (broń dla Morlana, Fecundatis) + jak nie zrobi to obudzi się Damiana.
    * TrZ+3
    * VzVVVXX: ciężkie straty u Blakvela i cywili, ale Antos zostaje bohaterem ludowym i jest wdzięczny zwłaszcza Rickowi
* 5 - Flawia --> skorpioidy. Rekombinacja w odpowiednie "pajęczynki" by potężnie wzmocnić siły Blakvela
    * ExMZ+3
    * XVXX: Skorpioidy wymrą; mamy sieć pajęczą. Ale Antonella musiała użyć Esuriit i transferować energię. Mamy Skażenie Esuriit w Antonelli i trochę w Jolancie
* 6 - Brighton --> Fecundatis. Odpowiedni pilotaż, rozłożenie itp. Rozkładanie w miarę ręczne przez Brightona + pełna koncentracja załogi (mimo Skażenia Antonelli), bo na szali jest życie Brightona. I pintka która ma zasłaniać. + Jolanta (a dokładniej: jej Elainka wspierająca automatyzację Fecundatis)
    * Tr(3O)+4
    * XVVOV: Jolanta-Elainka link się sypie; ale udało się rozłożyć sieci i doczekać posiłków kuratorów. Pintka zginęła. Fecundatis też bohater. Ma zawsze miejsce w Szamunczaku.
* 7 - Flawia próbuje się zorientować co to do cholery jest co przyszło jako wsparcie. Ma sygnaturę broni dzięki Rickowi.
    * TrZ+3 (niepełna)
    * V: Kuratorzy.
* 8 - RICK PODWAŻA KOMPETENCJE KURATORÓW ODNOŚNIE TEGO CZY MOŻE POMÓC XD. W dodatku - nie są stąd. Nie wiedzą wszystkiego. Flawia dorzuca chemiczne fakty o skorpoidach + uszkodzony waveform Jolanty. Cel: - niech odejdą.
    * TrZ+4
    * XVXVV: Część odratowanych --> Aleksandria, Kuratorzy się wycofają (tydzień spokoju) i przeprocesują. Plus wiemy DOKŁADNIE gdzie jest ich baza. Ale są odporni na tą i podobne sztuczki.
