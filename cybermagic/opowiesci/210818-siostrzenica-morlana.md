## Metadane

* title: "Siostrzenica Morlana"
* threads: legenda-arianny, corka-morlana
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [210630 - Listy od fanów](210630-listy-od-fanow)

### Chronologiczna

* [210630 - Listy od fanów](210630-listy-od-fanow)

### Plan sesji
#### Co się wydarzyło

.

#### Sukces graczy

* 

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Eustachy. Jest na K1. Nadzoruje Infernię (jest refitowana). Odwiedza go Mariusz (pion techniczny), z innej jednostki. "Jak to możliwe że przyczepiają się do Ciebie klauny". W Mordowni na K1 przylazł typ. Czarny płaszcz. Syntezator głosu. Maska. Eustachy poprosił Mariusza o przypilnowanie by kolesiowi nic się nie stało. Martyn przerobił Eustachego. A koleś... płaci za info o Eustachym.

Eustachy wziął ze sobą Leonę. MROCZNY KOLEŚ SCHOWAŁ SIĘ ZA BORYSEM! Podeszli do nich. On chce ratunku. Borys olał temat. A Leona nazwała Tomasza Kefirem. Tomasz chce się spotkać sam na sam z Eustachym - Leona (mock) że Tomasz może Eustachego zabić. IDZIE NIE INCOGNITO, ZAŁAMANY. I Eustachy tłumaczy Tomaszowi jakby to zrobił sabotażysta.

Infernia. Tomasz Sowiński i Eustachy. Przebrał się. CZEMU TOMASZ NIE POWIEDZIAŁ ARIANNIE? BO KIEDYŚ TOMASZ POWIEDZIAŁ ARIANNIE ŻE ŁADNIE WYGLĄDA ALE JEDEN GUZICZEK MNIEJ! I nie może się jej pokazać. I Eustachy lekko zestraja się z Infernią by powiedzieć Sowińskiemu "to ja idę do kibelka" i wyjść na moment zanim wejdzie Arianna. Chce podglądać.

TrZM+2:

* Vm: Eustachy podsłuchuje. I ma dostęp do sensorów Inferni; widzi lepiej co Tomasz serio a co kłamie. 

Arianna wchodzi na Infernię A TAM TOMASZ. Wyjaśnił o co chodzi:

* tien Nataniel Morlan, "łowca kuriozów" z Eterni poluje na siostrzenicę, tien Ofelię Morlan
* tien Ofelia Morlan pomagała ofiarom Nataniela, próbowała ich ekspediować i chować przed Natanielem, współpracując m.in. z Jolantą Sowińską
* Nataniel chce Ofelię usunąć. Ofelia służy na Netrahinie.
* Ofelia szmugluje czasem arystokratów eternijskich (podpadli Morlanowi) przez Netrahinę. Jest w cargo.

Arianna "uff, jak gorąco" i jeden guziczek zdejmuje, siada koło Tomasza. Pyta o Nataniela. Tomasz zrobi wszystko by tylko nie powiedzieć / nie pokazać że Arianna mu się podoba. 

TrZ+4:

* V: Tomasz mówi PRAWDĘ o córce. Powiedział o zmianie wzoru Antonelli przez Lucjusza Blakenbauera.
* V: "Jesteś tak mądra jak piękna..." - Tomasz jest przerażony tym co strzelił

Arianna zgodziła się pomóc. Za przysługę od Tomasza i Jolanty. Tomasz się zgodził (udając że nie patrzy na guziczek Arianny)

Centrala dowodzenia. Eustachy, Klaudia, Arianna.

Klaudia próbuje znaleźć czy jakieś sygnały szły z Eterni (Nataniel Morlan) w kosmos. Sygnały lub paczki lub coś takiego. Klaudia wspiera się różnymi AI. ExZ+2:

* XX: Nataniel 2 dni temu wysyłał sygnały w kosmos. Korelacja: Tomasz Sowiński i jego tajna akcja.
* X: Nataniel spodziewał się, że będą monitoringi i inne takie - jest paranoiczny bo nie wierzy że Sowińscy nie próbują go zatrzymać. Dlatego Klaudia nic nie ma. A on wie.

Klaudia poprosiła Martyna, by ten powiedział Marii, żeby przekonała Olgierda. A Arianna porozmawia z Olgierdem - chodzi o Ofelię.

Olgierd zabierze ich na Netrahinę Żelazkiem. Ostrzegł Ariannę, że Tomasza mogli oszukać - i Ofelia może być PRAWDZIWYM handlarzem ludzi.

Klaudia może przejrzeć dokumenty, raporty - KTÓRYM osobom Tomasz i Jolanta pomogli, kto uciekł, ich nagrania itp. PLUS przez negację - czy "ta partia" szmuglowanych ludzi o których Ofelię oskarżają nie jest jedyną. Arianna wykorzystuje jako wsparcie pamięć Jolanty, zamkniętą w Inferni.

ExZ+4+2:

* V: Macie potwierdzenie, że i pamięć Jolanty, i nazwiska w dokumentach... ta historia jest spójna. Tomasz i Jolanta pomagają z ratowaniu magów z Eterni przed Morlanem.
* V: Na podstawie wiedzy Jolanty (pamięci itp) Ofelia współpracuje z paroma statkami - Światłodóbr. Lata pomiędzy statkami we flocie. Inny - Fecundatis.
* X: Po paru użyciach do Jolanty dojdzie że ktoś używa jej kodów.
* V: Arianna ma ZAWSZE możliwość wykorzystywania kodów Jolanty i podawania się za Jolantę.
* V: Klaudia wydobyła informacje z pamięci Jolanty o wszystkim odnośnie programu kosmicznego Aurum co Jolanta wiesz. I czemu - chciała pomóc Flawii.

Realizujemy ten plan:

* Ćwiczenia połączone: Żelazko - Netrahina
* W zamieszaniu FAKTYCZNIE załoga Inferni porywa Ofelię ze statku XD
    * A inne osoby zamieszane w to wiedzą, że Ofelia czasami działa.

Arianna przekonuje Olgierda do planu. Olgierd - Arianna wisi mu kolację. A Maria YESS!!! 

Kramer dostaje od Arianny informacje o programie kosmicznym Aurum i za to pomaga bezapelacyjnie z ćwiczeniami i z atakiem na Nataniela Morlana. Kramer w tej chwili jest najszczęśliwszym mopsem na Kontrolerze. Olgierd dostał polecenie, Netrahina też. Będą ćwiczenia. Kramer ma nadzieję, że Netrahina będzie aktywna. Ale...

Żelazko. Strasznie ciasne, gorące, nie do życia. Arianna bierze Leonę, Klaudię, Eustachego i Tomasza.

Na początku Maria Naavas przypisała ("przypadkowo") Ariannę i Tomasza do małego pokoiku. Godzinę później Olgierd zrobił executive override.

Podróż na Netrahinę - 20h.

Eustachy po drodze fabrykuje sygnaturę "trzeciego statku" - to oni strzelali, to oni porwali Ofelię. Z sygnaturą "Pocałunku Aspirii" To jest pintka (Żelazko nie ma dron itp). Przygotowane sygnatury "Jaszczura" z Inferni. Teren ćwiczeń jako taki ma wraki / asteroidy a temat ćwiczeń to zasadzka.

ExZ+3+3O:

* XX: Tam rzeczywiście JEST inny statek... koloidowy. Statek Kasandry Kruk. Nataniel wie jaki jest plan.
* O: Infiltracja Netrahiny celem porwania Ofelii. Przez Kasandrę.
* V: Udało się zrzucić na Pocałunek Aspirii.

Klaudia włamuje się do TAI Netrahiny. Szuka gdzie jest Ofelia. Używa kodów odblokowujących Persefonę. "Pomóż nam uratować przyjaciółkę a odblokujemy Ciebie".

TrZ+3:

* XX: Druga Strona jest pierwsza.
* V: Persefona informuje Klaudię, że Ofelia znajdowała się w brygu. Za handel ludźmi.
* V: Persefona mówi o DWÓCH Ofeliach. Druga próbuje ewakuować się przez cargo.

Eustachy i Leona biegną do cargo przechwycić Ofelię, zanim napastnicy ją porwą.

ExZ+3:

* Vz: Dzięki Persefonie biegnie tam z pełną prędkością. Leona zdążyła dotrzeć do cargo zanim _tamci_ się ewakuowali.

Dotarli. Tam - 2-4 osoby w Eidolonach, prowadzą nieszczęśliwą Ofelię w skafandrze. Eustachy używa magii - robi kierunkową eksplozję. Chce wyrzucić wszystkie Eidolony przez otwierające się cargo.

ExZM+3:

* V: mikroeksplozja wyrzucająca koloidy - z powodzeniem. Ofelia też leci... ale Leona się rzuca, łapie Ofelię i ściąga ją z powrotem. I włącza się self-destruct w skafandrze Ofelii...
* X: magia i komplikacja sytuacyjna zapewnia Eidolonom ewakuację na statek macierzysty
* V: Eustachy magią spowolnił wybuch skafandra Ofelii. Leona wydziera Ofelię ze skafandra i osłania ją swoim ciałem + evac. Eksplozja. Leona, Ofelia poparzone. Leona przywykła.

STOP! Jest serio statek koloidowy? To nie jest plan Eustachego? XD. Arianna --> Żelazko. Jest koloidowy statek. Przerywamy ćwiczenia. Musimy go znaleźć i zniszczyć.

Netrahina jest statkiem badawczym - Klaudia używa TAI Netrahiny (Netrahina jako statek badawczy) by go znaleźć. Jak znaleźć? Wynik - zwierciadło jak odległość między teleskopami z czujnikami Żelazka i Netrahiny. A zwłaszcza Netrahiny...

ExZ+4: 

* Vz: Netrahina wykryła koloidowy statek; ma sygnaturę i nazwę...
* V: ...i Olgierd ma czas na zareagowanie.

Olgierd próbuje unieruchomić koloidowy statek

TrZ+3:

* X: Bardzo poważne zniszczenia statku koloidowego, sporo faktów / danych straconych
* V: Statek koloidowy nie wróci do bazy. Olgierd go dorwie.
* VV: Gdy samozniszczenie się włączyło (załoganci nie wiedzieli XD) Olgierd spalił reaktor i konsolę lancą plazmową - uratował kogo się dało.
* X: niestety podczas "plazmowania" kapitan i oficerowie wymarli. 

Czyli udało im się zniszczyć statek i przechwycić część cargo i załogi. Załoga wstrząśnięta, że skazani na śmierć... Kasandra by im tego nie zrobiła.

Po przeanalizowaniu, zamiast Ofelii jest miragent w brygu.

Miragent? Statek koloidowy? Eidolony? Mówimy tu o sporych kwotach. Wszystko faktycznie wskazuje na Nataniela Morlana...

## Streszczenie

Tomasz Sowiński próbuje uratować Ofelię Morlan przed Natanielem Morlanem. Nie ma kogo poprosić a Ofelia służy na Netrahinie; poprosił więc Ariannę. Arianna skanując echo pamięci Jolanty w Inferni zdobywa informacje o programie kosmicznym Orbitera, przekazuje to Kramerowi, wykorzystuje wsparcie Olgierda i robią ćwiczenia Żelazko - Netrahina. Ratują Ofelię przed porwaniem przez koloidowy statek Kruków Kasandry.

## Progresja

* Arianna Verlen: kody pozwalające jej na podawanie się za Jolantę Sowińską. Ich użycie to POWAŻNE nadużycie zaufania Jolanty.
* Arianna Verlen: pochwała od Kramera - pozyskała info o programie kosmicznym Aurum z pamięci Jolanty i przekazała to Kramerowi. Poważne nadużycie zaufania Jolanty i problemy dla Jolanty.
* Tomasz Sowiński: podkochuje się w Ariannie Verlen. Uważa ją za mądrą i piękną. Nigdy jej tego nie powie.
* Tomasz Sowiński: wisi Ariannie Verlen spory dług wdzięczności za ratunek Ofelii.
* Jolanta Sowińska: jej plany / wiedza odnośnie programu kosmicznego Aurum zostały przekazane Kramerowi a jej reputacja w Aurum poważnie uszkodzona, że "dała cynk".
* Jolanta Sowińska: wisi Ariannie Verlen spory dług wdzięczności za ratunek Ofelii. Tomasz zawarł dług, ale Jolanta będzie honorować.
* Nataniel Morlan: wie, że to Infernia (Klaudia) próbuje dowiedzieć się o jego działaniach przeciw Ofelii. Wie, kto z nim walczy.
* Ofelia Morlan: ewakuowana z Netrahiny na Kontroler Pierwszy; jest bezpieczna, pod opieką admirała Kramera.
* OO Netrahina: TAI Persefona uwolniona przez Klaudię podczas operacji ratowania Ofelii.

### Frakcji

* Kruki Kasandry: tracą jedyną koloidową jednostkę, miragenta, kilka Eidolonów. Ogromny cios.

## Zasługi

* Arianna Verlen: panikuje Tomasza rozpinając JEDEN GUZICZEK i poznaje sekrety Tomasza i Jolanty. Przekazuje Kramerowi info o kosmicznym programie Aurum. Planuje wspólne ćwiczenia by odzyskać Ofelię. W sumie - mastermind działań przeciw Natanielowi Morlanowi. 
* Eustachy Korkoran: fabrykuje sygnaturę statku koloidowego, po czym magią ratuje Ofelię przed Eidolonami (eksplozja kierunkowa wywala ich ze statku).
* Klaudia Stryk: udowodniła, że Jolanta, Tomasz, Ofelia faktycznie ratują Eternian przed Morlanem. Potem odblokowała TAI Netrahiny i znalazła koloidowy statek dla Olgierda do zestrzelenia.
* Leona Astrienko: z lubością nazywa Eustachego "callsign kefir". Ratuje Ofelię z Eustachym używając swojej prędkości poruszania się, a potem wyłuskuje Ofelię ze skafandra i ją zasłania ciałem.
* Tomasz Sowiński: chce uratować Ofelię i zwraca się z prośbą do Arianny Verlen. Podkochuje się w niej. Ma najgorsze poczucie spec ops w historii - zakłada dziwny strój i zwraca na siebie uwagę.
* Ofelia Morlan: z Tomaszem i Jolantą Sowińskimi ratowała szlachtę Eterni przed Natanielem Morlanem przemycając ich przez Netrahinę (działa z cargo na Netrahinie). Kruki Kasandry wrobiły ją w prawdziwy przemyt ludzi i chciały porwać w zamieszaniu; udało się Zespołowi ją uratować.
* Nataniel Morlan: absolutnie bezwzględny; skłonny porwać / zabić siostrzenicę by tylko wszystko wróciło "do normy". Wie, że Infernia działa przeciw niemu.
* Antoni Kramer: dostał info o planie na program kosmiczny Jolanty Sowińskiej. Zaaranżował ćwiczenia Netrahina - Żelazko z dobrego serca i chęci pomocy Ofelii.
* Olgierd Drongon: pomógł Ariannie dostać się na Netrahinę i skutecznie zestrzelił koloidowy statek. Trochę zazdrosny o Ariannę x Tomasza ;-). Pomógł Ariannie za wspólną kolację.
* Maria Naavas: przypisała ("przypadkowo") Ariannę i Tomasza do małego pokoiku na Żelazku, by wywołać zazdrość Olgierda. Z powodzeniem :-). Olgierd overridował decyzję i Arianna była sama.
* OO Żelazko: wykorzystane przez Ariannę (prośba -> Olgierd) do ratowania Ofelii i ćwiczeń z Netrahiną. Jak pies gończy złapał koloidowy statek Kruków i go unieszkodliwił.
* OO Netrahina: służyła do przerzutu nieszczęsnych arystokratów Eterni którzy podpadli Natanielowi Morlanowi. Klaudia odblokowała jej Persefonę kodami kontrolnymi. Wykryła koloidowy statek.
* SC Światłodóbr: współpracuje z Ofelią Morlan i Jolantą Sowińską przy ratowaniu arystokratów Eterni przed Natanielem Morlanem.
* SC Fecundatis: współpracuje z Ofelią Morlan i Jolantą Sowińską przy ratowaniu arystokratów Eterni przed Natanielem Morlanem.

### Frakcji

* Kruki Kasandry: próbowały przechwycić Ofelię Morlan. Skutecznie wrobiły Ofelię w przemyt ludzi, ale przez Eustachego nie udało im się podmienić Ofelię na miragenta i odlecieć z Netrahiny.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy
                    1. Sektor 57: dość podły sektor, używany przez agentów różnej maści. Tam raczej odbywają się walki niż wieczorek poezji.
                        1. Mordownia: Ten jeden klub, gdzie się nie chodzi jak nie ma się ochoty na ciężkie chwile. Tomasz Sowiński tam był i szukał Eustachego jak owieczka wśród wilków.

## Czas

* Opóźnienie: 2
* Dni: 4

## Inne

.

## Konflikty

* 1 - Eustachy lekko zestraja się z Infernią by powiedzieć Sowińskiemu "to ja idę do kibelka" i wyjść na moment zanim wejdzie Arianna. Chce podglądać rozmowę.
    * TrZM+2
    * Vm: Eustachy podsłuchuje sensorami Inferni
* 2 - Arianna "uff, jak gorąco" i jeden guziczek zdejmuje, siada koło Tomasza. Pyta o Nataniela.
    * TrZ+4
    * VV: Tomasz opowiedział o przeszłości Nataniela i jego + Jolanty walce przeciw niemu. Wymsknęło mu się też, że uważa Ariannę za piękną i mądrą. Podkochuje się w niej.
* 3 - Klaudia próbuje znaleźć czy jakieś sygnały szły z Eterni (Nataniel Morlan) w kosmos. Sygnały lub paczki lub coś takiego. Klaudia wspiera się różnymi AI.
    * ExZ+2
    * XXX: 5 dni temu Nataniel słał w kosmos. Spodziewał się, że ktoś będzie monitorował - złapał info o Inferni.
* 4 - Klaudia próbuje dokumentami i danymi z Inferni/pamięci Jolanty określić prawdę - czy Tomasz i Jolanta i Ofelia serio walczą z Morlanem?
    * ExZ+4+2:
    * VVXVV: potwierdzenie; statki współpracujące: Światłodóbr, Fecundatis; Arianna ma kody podające się za Jolantę (ale w końcu wyjdzie), Klaudia ma program kosmiczny Aurum.
* 5 - Eustachy po drodze fabrykuje sygnaturę "trzeciego statku" - to oni porwali Ofelię.
    * ExZ+3+3O
    * XXOV: tam jest inny koloidowy statek i oni są szybsi. Zinfiltrowali Netrahinę by porwać Ofelię. Ale Eustachy zrzucił winę na Pocałunek Aspirii.
* 6 - Klaudia włamuje się do TAI Netrahiny. Szuka gdzie jest Ofelia. Używa kodów odblokowujących Persefonę. "Pomóż nam uratować przyjaciółkę a odblokujemy Ciebie".
    * TrZ+3
    * XXVV: Druga Strona jest pierwsza, ale Klaudia wie od Persefony gdzie jest Ofelia i ma jej współpracę.
* 7 - Eustachy i Leona biegną do cargo przechwycić Ofelię, zanim napastnicy ją porwą.
    * ExZ+3
    * Vz: sukces, zdążyli
* 8 - Eustachy używa magii - robi kierunkową eksplozję. Chce wyrzucić wszystkie Eidolony przez otwierające się cargo i ratować Ofelię.
    * ExZM+3
    * VXV: pozbycie się koloidów, neutralizacja bomby w skafandrze Ofelii, ale Eidolony zdołały się ewakuować.
* 9 - Netrahina jest statkiem badawczym - Klaudia używa TAI Netrahiny (Netrahina jako statek badawczy) by znaleźć statek koloidowy.
    * ExZ+4
    * VzV: nie tylko znalazła i poznała sygnaturę - Olgierd ma czas na reakcję
* 10 - Olgierd próbuje unieruchomić koloidowy statek
    * TrZ+3
    * XVVVX: zniszczenie struktury statku, są osoby martwe i ranne (m.in. oficerowie), ale załoga przejęta
