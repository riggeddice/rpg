## Metadane

* title: "Dziwne Strachy w Morzu Ułud"
* threads: uludy-wielkiego-weza
* gm: żółw
* players: kić, fox

## Kontynuacja
### Kampanijna

* [220420 - Samobójstwo kapitana Wielkiego Węża](220420-samobojstwo-kapitana-wielkiego-weza)

### Chronologiczna

* [220420 - Samobójstwo kapitana Wielkiego Węża](220420-samobojstwo-kapitana-wielkiego-weza)

## Plan sesji
### Co się wydarzyło
#### Co się wydarzyło w tym terenie?

* Inicjacja bazy Ukojenie Barana
    * mag eternijski, Bruno Baran, założył niewielką bazę daleko od Eterni z częścią ludzi 12 lat temu
    * automatycznie zaczął się kręcić profit
    * wśród górników Elsa Kułak została nieformalną liderką. Niestety, "demokracja" nie przypasowała Baranowi. Odizolował się.
* Migracja Mardiusa
    * noktiańscy komandosi Tamary Mardius złożyli tu swój nowy dom wśród lapicytu. Podeszli do górników i Barana z neutralnością. Ot, są.
    * mało kto jak noktianie umieją żyć w kosmosie. W tym miejscu znaleźli świetne miejsce i ich umiejętności stały się bardzo przydatne.
    * siły Mardius znalazły opcję szmuglowania przez statek Gwiezdny Motyl.
* Imperium Blakvela
    * 3 lata temu pojawił się eternijski szlachcic, Ernest Blakvel. Stwierdził, że władza w kosmosie to coś dla niego. Zaczął umacniać się w tym terenie.
    * Blakvel i Mardius weszli w stały konflikt. Mardius chroni miejscowych, Blakvel chce ich podbić.
* Pojawienie się Strachów
    * 2 lata temu pojawiły się Strachy. Stworzone z programowalnej materii (morfelin), nie wiadomo czemu są i czym są.
    * Mardiusowcy aktywnie chronią lokalnych.

.

* Zespół dał radę kupić lokalizację dziwnego sygnału - znajduje się tam Tajemniczy Derelict Ship. Kapitan podjął decyzję, by Wąż poleciał.
    * Ten okręt zawiera Hipermutator Emisyjny (anomalia), dość sprawną TAI, interesujący ładunek biologiczny oraz neuroobroże Syndykatu Aureliona.
    * Była informacja o niszczeniu morale przez różnego rodzaju zagrożenia i problemy
* Zatrudniono advancera - Berdysza. Nie wiedzą kim Berdysz jest. Gaulron-advancer z wystarczająco dobrym sprzętem.
* Wąż odleciał w kierunku na Derelict Ship.

#### Strony i czego pragną

* Alan 
    * jest agentem Syndykatu Aureliona. Ceni siłę, bezpieczeństwo i hedonizm, ale nieważne jak go patrzą i pomoc innym. Niecierpliwy i praktyczny, motywowany nagrodami i beztroski. Inspektor sanitarny i elektronik. Chce zwiększyć wpływ Syndykatu. Pragnie znaleźć derelict ship i sprzedać znajdujące się tam Obroże Neurokontroli Blakvelowcom. 
    * CEL: sprzedać obroże.

* Pola 
    * najważniejsze są jej osiągnięcia, niezależność biznesowa oraz czysta przyjemność. Niewrażliwa na stres i praktycznie nie motywowana niczym, woli się zgodzić z drugą stroną niż się pieprzyć. Szuka mistycyzmu i rzeczy nadnaturalnych, pragnie zostawić na boku ten bezwartościowy świat. Ewentualnie - komfortowego życia. Badaczka szukająca wśród Strachów iskry bogów. 
    * CEL: interakcja i integracja z absolutem.

* Maja 
    * nienawidzi osób fałszywych. Tradycyjne podejście i akumulacja mocy sprawiają, że Maja osiągnęła niemały poziom znaczenia. Wszystko pragnie zawłaszczyć i rządzić dominacją i terrorem - by wreszcie wszyscy żyli w prawdzie. Jednocześnie znajduje piękno w gospodarce roślin i czystości ekosystemu. Robi za lekarza i kontrolę biologiczną Węża. 
    * CEL: przejęcie władzy

* Kornelia 
    * pragnie się dopasować do grupy i z radością buduje wspólną przyszłość nieważne czy dla ludzi czy AI. Przyjmuje każdego, nieważne z jakiej frakcji. Nie dba o swoje zadowolenie czy emocje. "Dobra mama" grupy. Zajmuje się załadunkiem i sprzedażą dóbr. 
    * CEL: pragnie Wojciecha (jakiegoś Blakvelowca) jako swojego soulmate.

* Filip 
    * jest skupiony na dokonaniach, pokorze i tradycji. Nie dba o stymulację czy pomaganie innym. Złośliwe i wścibskie bydlę, ale niesamowicie pracowity. Kiedyś już zdradził; nie powtórzy się to ponownie - poprzysiągł dbać i chronić interesy. 
    * CEL: zdobyć najlepsze możliwe narzędzia dla Wielkiego Węża.

* Antoni 
    * zna swoje miejsce w rzeczywistości. Skupia się na zabezpieczeniu Wielkiego Węża i na tym by nikomu na pokładzie nic się nie stało. Uratowany z niewoli piratów, pochodzi z zupełnie innego miejsca. Jest jak ryba w wodzie, ale jest świetnym inżynierem i konstruktorem dron, zwłaszcza badawczych. Nienawidzi piratów w każdej formie. 
    * CEL: wrócić do domu. Chwilowo: zabezpieczać Węża.

* Lila 
    * nie dba o tradycję ani bezpieczeństwo - ale skupia się na sukcesach, autonomii i stymulacji. Zostaw ją w spokoju i sumienna (choć nieprzyjemna w obyciu) Lila zapewni by wszystko zadziałało. Appraiser, neurosprzężona wbrew swojej woli i ucieka w używki by nie czuć wiecznego szeptania uszkodzonego AI. Główny pilot i serce maszyny. Wyjątkowo drażnią ją małe głupoty i "szczury w piwnicy" ;-). 
    * CEL: usuwać przeszkody, zapewnić, by coś takiego nigdy nikogo innego nie spotkało.

* Berdysz 
    * jest straszliwym kapitanem piratów. Dołączył do załogi jako advancer. Jest gaulronem, dewastatorem i świetnym planerem. Ceni sobie władzę i przyjemność; nie ma nic przeciw by się podzielić ;-). Jest w stanie pełnić prawie każdą rolę.
    * CEL: skorzystać z okazji, wrócić do bycia kapitanem piratów. Kiedyś.

### Co się stanie

* Morale graczy będzie dewastowane przez halucynacje, Strachy
* Spotkanie ze Strachami
* Inny Derelict Ship który jest przez coś spustoszony i przeżarty
    * rozpada się
* emisja anomalna na pokładzie Węża 

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Sesja właściwa - impl

Lila pilotuje dzielnie Wielkiego Węża. Lecimy coraz głębiej terytorium Strachów w Morzu Ułudy. Gdzieś tam w tamtych okolicach znajduje się Derelict Ship. I potencjalna zasadzka piratów. Dowodzi Kornelia, która zna się na rzeczy i po działaniach Mai znowu zintegrowała ludzi.

Wlecieliście 2 dni w głąb.

Lila siłą Węża zauważa większą ilość Strachów. Nadmierną ilość. W mniej więcej ich wektorze. Zwykle obecność Strachów jest przyciągana przez jakieś jednostki czy zagrożenia. Lila próbuje ominąć problem. I szuka wzorów - co się dzieje.

TrZ+3+2Or:

* Or: zabójca memetyczny zamknięty w AI prowadzi statek dalej. Lila "zwisła" na moment.
* Vz: Lila zorientowała się co się dzieje. Jesteście wśród Strachów. Już jesteście niedaleko nich. Strachy przyciągnęła boja. Boja nadaje sygnał... Lila przenosi statek w low power.

Lila -> all: Kornelio, podejdź na mostek. Mamy trochę problem. Kornelia ma minę "THOSE WHO KNOW". Lila pokazuje boję - a na niej ludzkie ciało. I boja jest dość słaba i uszkodzona. Strachy nie wchodzą z nią w interakcję. Boja odwraca uwagę, ALE dopóki boja tam jest to Strachy nie odejdą. Trzeba się pozbyć boi jakoś...

Maja -> Pola, że czy jest jakiś wzór tych strachów. Pola analizuje przebłyski które były zanim wyłączono - tak. Ośmiornica, rekin, delfin. Istoty wodne. Rzeczy związane z wodą.

* Pola: "jesteśmy w stanie dostać się na boję. Wpiąć i dowiedzieć się skąd pochodzi."
* Maja: "jak bardzo to ryzykowne?"
* Pola: "bardzo"

.

* Antoni: "ale czemu wleciałaś w sam środek strachów?"
* Lila: "bo było czysto"
* Antoni: "Strachy zachodzą nas jak drapieżniki teraz?!"

To dziwne, że Strachy nie zachowują się jak zwykle. Pola -> czemu Strachy zachowują się tak a nie inaczej.

TrZ+3:

* V: Boja, lub coś związanego z boją przyciągają uwagę Strachów w formie "hipnotycznej".
* X: Maja i Pola może są w stanie zaproponować dryf, ale wymaga to by coś zmieniło się w boi.
* V: Da się zrobić tak, by Strachy skupiły się na boi a nie na Was. Co więcej, inne Strachy nie reagują na tą boję tak jak te.

A Berdysz się zbiesił. Nie chce tam wyjść. Kornelia próbuje go przekonać, ale on twierdzi że za mało mu płacą i nie jest to tego warte. Kornelia proponuje że zapłaci więcej, że to dla załogi a Berdysz "nie dbam, nieważne, nie stać Cię". Wyraźnie Kornelia sobie z tym nie radzi.

Maja poszła twardo - potrzebny jest do zrobienia czegoś z boją. Berdysz skonfrontował twardo - Maja nic nie ryzykuje a oni zostawią go w kosmosie. Kornelia aż się skuliła. Maja zaproponowała, żeby wziął Pamiątkowy Pistolet, odznaka jak pomogli komuś z Aurum. Coś ważnego dla kapitana i dla załogi. Pola i Kornelia zaprotestowały. Maja nacisnęła - to tylko kawałek metalu a Berdysz z tym przecież wróci.

Berdysz zaakceptował i chyba lekko mrugnął do Mai.

Maja wzięła Kornelię na szukanie pistoletu, część załogi krzyczy na Berdysza, który ma to gdzieś i nieźle się bawi. Kornelia jest przygnębiona - ona normalnie jest nie do ruszenia emocjonalnie CHYBA że nie dogaduje się załoga. Maja buduje, żeby Kornelia Mai ufała.

TrZ+3:

* X: Pola uważa, że Berdysz aktywnie podminowuje Kornelię. Nie jest głupia.
* Vz: Kornelia zna Maję długo. Wierzy, że Mai może ufać i Maja zna się na rzeczy. Że w trudnych sytuacjach ona jest najlepsza na Wężu.
* V: Kornelia chce zrobić z Mai pierwszego oficera.
* V: Kornelia AKTYWNIE chroni honor Mai.

Berdysz przygotowuje się do kosmicznego spaceru. Po raz kolejny sprawdza sprzęt. Niski profil silników korekcyjnych, to musi być cichy, delikatny lot.

TrZ+3:

* X: Berdysz będzie musiał użyć pistoletu. Pistolet tam zostanie.
* Vz: Dobry sprzęt i dobre działanie. Berdysz zapewni, że Wąż może odlecieć.
* Vz: Wybitny advancer, udało mu się pobrać istotne próbki.

Berdysz próbuje nie pokazać po sobie emocji, ale jest zaniepokojony po powrocie. Maja pyta, czy wszystko poszło gładko. Nie. Musiał użyć tego pistoletu. NIE BYŁ NAŁADOWANY.

* X: Załoga po zimnym wybuchu Berdysza jest przekonana, że coś z nim jest nie tak, że to jakiś niebezpieczny koleś, nie advancer. Antoni i Filip dojdą do tego kim jest Berdysz.

Berdysz wyjaśnił o co chodzi. Tam był "undead". Plakietka w śluzie. Zaatakował go, więc Berdysz wbił mu pistolet w oko i wyrwał plakietkę, "wyrwał mu serce". I wypchnął w kosmos.

Maja próbuje uspokoić załogę - jest po ich stronę i niech tak lepiej zostanie.

TrZ+2:

* X: Kornelia była potrzebna do ich uspokojenia; Maja musiała mieć wsparcie
* X: Maja nie była w stanie ich uspokoić. Ale Kornelia uspokoiła.
* X: Kornelia jest NIEZBĘDNA na tej jednostce. Tylko ona może uspokoić, ukoić i utrzymać. Maja może tylko zaszkodzić.

Alan i Berdysz o czymś długo rozmawiali, Berdysz oddalił się od załogi, poszedł do sprzętu.

Lila jest w stanie wreszcie wyprowadzić statek spośród Strachów. Wiedząc, w jakim stanie jest Berdysz (chodzi o specjalny "sok" który gaulrony muszą brać) Lila założyła na Berdysza monitoring. I pierwsze wyniki tego monitoringu pokazały, że Berdysz nie jest w stanie mieć dość soku na całą drogę.

Lila -> Maja o soku dla gaulrona. Maja się zasępiła.

Pola i Maja zaczynają badania próbek które zdobył Berdysz a Lila i Alan zaczynają badania sygnałów elektronicznych.

Pola i Maja:

Tr+2:

* XX: to na pewno nie jest Strach. To jest tkanka ludzka, ale zmieniona. Faktycznie "undead" brzmi... możliwie? Ciało było "sprawne", ale nie do końca. Jakiś magimod.
* V: nie trzeba tego utylizować. Jest bezpieczne. Nie jest to zaraźliwe. Ale warto zachować w bezpiecznym miejscu - a plakietka wskazuje na to, że koleś pochodził z jakiegoś statku.

Lila i Alan próbują dojść do tego co w tej boi:

TrZ+2:

* V: to pochodzi ze statku pirackiego. Był tam sygnał SOS. Jest jakiś komunikat, ale wymaga odtworzenia.
* V:
    * statek piracki wysłał rozpaczliwe SOS
    * doszło do przebicia na pokładzie
    * atakują ich Deep Ones. W skafandrach i wszystkim.
    * w imię Seilii uratujcie nas! Mamy łupy i wszystko.

Alan: "o cholera - widziałaś to? Możemy przejąć statek piracki i go sprzedać? *_* "
Lila: "może będzie mnie stać na wymianę AI na coś sensownego"
Alan: "weź, chodźmy w głąb danych - ile piratów tam może być, co to za jednostka itp"

* Vz: .
    * Nazwa jednostki "Okrutna Wrona". To dość dobrze elektronicznie ukryty statek. Na kilkanaście piratów. Małe cargo, nieźle uzbrojony.
    * Ta jednostka nie słynie z nadmiaru okrucieństwa. Haracz, "oddawajcie i lecimy". Nie z tych "LOL BUMBUMBUM".
    * Czasem ta jednostka z uwagi na swoje działania mogła pojawiać się w bardziej cywilowanych terenach, np. Asimear. Zwłasza że nie było dowodów...
        * ...ma adaptujący transponder. Może udawać że jest czymś innym.
        * często próbuje się maskować jako "Plugawy Jaszczur"

Alan jest zainteresowany przechwyceniem transpondera.

Lila -> Maja: "chcesz adaptujący transponder? Jak uratujemy piratów, uratujemy ich transponder". Lila streściła Mai to co znaleźli. Przekonała, że może być warto spróbować. A Maja ma przekonać teraz Kornelię. Mimo potencjalnych problemów i ryzyk. Boja jest kilka godzin drogi stąd.

Kornelia -> Maja: "możemy uratować kilku piratów?". Kornelia jest sceptyczna. Pola OCZYWIŚCIE jest za. Bardzo za. Zdaniem Poli to najlepsza rzecz na świecie - zombie nie istnieją. Zombie są dowodem na istnienie bogów. Maja -> Kornelia "aresztujemy ich". Kornelia jest całkowicie zagubiona. Nie wie co z tym robić...

Maja proponuje -> nadłóżmy kilka godzin i zobaczmy jak wygląda sytuacja. W najgorszym wypadku straciliśmy kilka godzin. Może coś się wycofało i są jacyś ocalali, pojedynczy.

TrZ+3:

* X: Kornelia czuje się bardzo niepewnie. Nie wie co z tym wszystkim robić i sytuacja ją przerasta.
* X: Kornelia decyduje się tam lecieć i uratować kogo się da, bo po prostu TAK TRZEBA. I załoga mimo, że nie zawsze się zgadza, ceni ją za to.
* V: Maja staje się podporą dla Kornelii. Maja podawała Kornelii fakty nie narzucała jej swego zdania. Maja wspiera a nie podkopuje Kornelii.

I NA TO WCHODZI Antoni i Filip do Mai i Kornelii: "BERDYSZ JEST KAPITANÓW PIRATÓW! KRWAWYCH ROZPRUWACZY BERDYSZA CZY JAKOŚ TAK!" 

Kornelia ma minę zbitego pudla. Ma blank. Maja też ma blank. Antoni: "MUSIMY GO ZABIĆ ZANIM SIĘ ZORIENTUJE!" Maja: "jak?"

Antoni ma pełną panikę. ON JEST KANIBALEM. Pełna panika. Kornelia ma blank. Maja walnęła Antoniego w pysk - nie może się zorientować. Jest kapitanem piratów ale jedynym advancerem. Nie dołączy do nich. I nie chcemy by ich pozabijał. Z drugiej strony - sprzedał informację... najpewniej nie jest z nimi w zmowie bo czemu miałby ostrzec? Najlepiej jakby nic nie powiedział. Więc... jest po naszej stronie? Chce czegoś od derelictu... Kornelia -> nie możemy zdecydować o NIE leceniu na derelict. Maja, wiem że to niebezpieczne... czy możesz wybadać jakie są jego plany? Ciebie poważa lub słucha.

Dla Mai w świetle tego co usłyszała zachowanie Berdysza jest zupełnie inne. Groźniejsze.

Maja przejmuje kontrolę nad innymi. Antoni panikuje, Filip nie utrzyma. Kornelia, cóż.

ExZ+4

* X: Antoni unika Berdysza za wszelką cenę. Boi się go i nie chce mieć nic wspólnego. Obsesyjnie go śledzi.
* V: Sekret będzie zachowany a Maja jest strongmanem. Jej pozycja "ważnej osoby w kryzysie" jest w tej trójce utwierdzona.

Lila, nieświadoma tego, podbija do kajuty Berdysza gdy ten tam jest. Zastała go jak on ćwiczy. Jest słabszy niż powinien być. I powiedział że jego 'skrzynka medyczna' jest w rzeczywistości czymś co go zasila. Pogadali o Deep One którego znalazł Berdysz.

Antoni przyniósł Mai bombę. Coś cennego. -> Berdysz na 100% jest kanibalem. Bo inaczej nie jest w stanie zapewniać niektóych hormonów i mechanizmów. Musi też dostęp mieć do apteczki i innych medykamentów. Czyli mogą go zabić - zatruć odpowiednie rzeczy. Zginie. Ale nie wiadomo kiedy i jak szybko.

Maja powiedziała, że by nie ryzykowała.

Antoni smutny.

Antoni ma nowy plan - wysłać go jako advancera jako statek piracki i spieprzać.

ALE JAK TO! BERDYSZ CHYBA NIE ZEŻARŁ NAM MARTWEGO KAPITANA?!

Maja ma niesamowicie dobrą linię przekonywania - najpewniej powiedział Alanowi, na pewno coś chce od tego derelictu i nie chce by załoga coś wiedziała bo nie chce im robić krzywdy

TrZ (wszyscy chcą w to wierzyć) +4:

* V: Maja dowodzi tą ekipą w sprawie Berdysza. Będą się jej słuchać.
* V: Maja faktycznie nieoficjalnie dowodzi. Bo jak Kornelia się z nią nie zgadza to przez ryzyko Berdysza Kornelia zmieni zdanie na Mai.
* V: Zrobi się z tego nawyk. Jak nawet Berdysz opuści statek, Maja przejęła dowodzenie niepisane. A ta trójka stoi za nią murem.

## Streszczenie

Z uwagi na kolizje i niekompatybilności między AI i umysłem Lili, ta wpakowała Węża prosto w środek dziwnych Strachów orbitujących dookoła boi. Berdysz - advancer - wyleciał do boi (acz skorzystał z okazji by podnieść pozycję Mai vs Kornelii). Zniszczył tam "nieumarłego", po czym wrócił z boją. Okazuje się, że został zniszczony statek piracki - i Berdysz chce ratować co się da. Konflikt na Wężu. Wyszło szydło z worka - Berdysz to kapitan piratów. Ale po naszej stronie. Decyzja - WPIERW piraci POTEM derelict...

## Progresja

* Maja Kormoran: nie ma opcji, by mieć serca załogi Wielkiego Węża. Jest najlepszym pierwszym oficerem której można ufać w sprawach kryzysowych, ale nie będzie nigdy lubiana.

### Frakcji

* .

## Zasługi

* Maja Kormoran: udało jej się przejąć nieformalnie władzę dzięki pomocy Berdysza i dobrego wykorzystania kryzysu. Skutecznie wykorzystała kryzys do przejęcia władzy. Opanowała załogę - nie zdradzamy że wiemy kim jest Berdysz i bezpieczniej jest współpracować. I tak, ratujemy piratów z Okrutnej Wrony.
* Lila Cziras: z uwagi na problemy z integracją z AI wpakowała wszystkich w paskudne miejsce między Strachami, ale wyprowadziła stamtąd statek. Doszła do tego, że Berdysz - gaulron - ma specjalne potrzeby i próbuje (skutecznie) z nim wejść w jakąś formę nieformalnego sojuszu przeciwko światu. Odszyfrowała z Alanem sygnały z boi "Okrutnej Wrony".
* Alan Falkam: wraz z Lilą dochodzą do tego, co z tą boją - pochodzi ze statku pirackiego ("Okrutnej Wrony") i odszyfrowali sygnały. 
* Pola Mornak: stoi silnie we frakcji pro-Kornelii. Doszła do tego jak działają Strachy i jak użyć Berdysza by dało się ominąć Strachy. Niezły naukowiec.
* Kornelia Sanoros: wymanewrowana politycznie przez Berdysza i emocjonalnie przez Maję, praktycznie oddała Mai dowodzenie. Sytuacja ją przerasta. ALE - zapewni, żeby piraci zostali uratowani bo to właściwa rzecz do zrobienia. Skutecznie uspokaja załogę i dba o to, by wszystko działało prawidłowo - na swoją zgubę polityczną.
* Filip Gościc: pomógł Antoniemu dojść do tego kim jest Berdysz. Zaniepokojony, bo go tauntował często. Chwilowo z boku, nie wie jak się sprawa potoczy.
* Antoni Krutacz: z Filipem doszedł do tego kim jest Berdysz - morderczy kapitan piratów. Wymyślał serię planów jak się Berdysza pozbyć ze statku - bez szans. Cała ta sytuacja zmieniła go w trwożliwe stworzonko.
* Berdysz Rozdzieracz: jako advancer doleciał do boi i rozszarpał swoim kombinezonem kalcynita. Jakkolwiek załoga Węża odkryła kim jest i są osoby aktywnie go zwalczające, zapewnił sobie na Wężu stronnictwo silnie stojące za nim.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Pas Teliriański
                1. Planetoidy Kazimierza
                    1. Morze Ułud: pojawiają się tu bardzo nietypowe i niestabilne Strachy, wyglądają inaczej niż typowe formy. Bardzo ciężko się nawiguje i porusza po tym terenie.

## Czas

* Opóźnienie: 3
* Dni: 2

## Konflikty

* 1 - Zwykle obecność Strachów jest przyciągana przez jakieś jednostki czy zagrożenia. Lila próbuje ominąć problem. I szuka wzorów - co się dzieje.
    * TrZ+3+2Or
    * OrVz: Lila "zwisła" i Wąż jest otoczony przez Strachy. Nietypowe. Przeniosła statek w low power.
* 2 - To dziwne, że Strachy nie zachowują się jak zwykle. Pola -> czemu Strachy zachowują się tak a nie inaczej.
    * TrZ+3
    * VXV: boja przyciąga Strachy w sposób "hipnotyczny", Maja i Pola mogą zaproponować dryf. Da się ominąć Strachy ale trzeba użyć advancera.
* 3 - Maja wzięła Kornelię na szukanie pistoletu kapitana, Maja buduje narrację, żeby Kornelia Mai ufała.
    * TrZ+3
    * XVzVV: Pola uważa, że Berdysz podminowuje Kornelię ale Kornelia robi z Mai pierwszego oficera i aktywnie chroni jej honor.
* 4 - Berdysz przygotowuje się do kosmicznego spaceru. Po raz kolejny sprawdza sprzęt. Niski profil silników korekcyjnych, to musi być cichy, delikatny lot.
    * TrZ+3
    * XVzVz: Berdysz jest świetnym advancerem; stracił pamiątkowy pistolet ale zdobył próbki i mogą odlecieć.
* 5 - Maja próbuje uspokoić załogę - Berdysz jest po ich stronę i niech tak lepiej zostanie.
    * TrZ+2
    * XXX: Kornelia ich uspokaja, Maja nie ma jak, Kornelia jest niezbędna - Maja nie będzie dobra w trzymaniu serc załogi.
* 6 - Pola i Maja zaczynają badania próbek które zdobył Berdysz a Lila i Alan zaczynają badania sygnałów elektronicznych.
    * Tr+2
    * XXV: kalcynit nie jest Strachem; to zmieniona tkanka ludzka. Ciało było "sprawne", ale nie do końca. Jakiś magimod. Nie jest zaraźliwe.
* 7 - Lila i Alan próbują dojść do tego co w tej boi:
    * TrZ+2
    * VVVz: SOS ze statku pirackiego, informacja co się stało i o co chodzi + info czym jest Okrutna Wrona.
* 8 - Maja proponuje -> nadłóżmy kilka godzin i zobaczmy jak wygląda sytuacja. W najgorszym wypadku straciliśmy kilka godzin. Może coś się wycofało i są jacyś ocalali, pojedynczy.
    * TrZ+2
    * XXV: Maja staje się podporą dla Kornelii, która chce ratować ludzi za wszelką cenę.
* 9 - Dla Mai w świetle tego co usłyszała zachowanie Berdysza jest zupełnie inne. Groźniejsze. Maja przejmuje kontrolę i opanowuje załogę.
    * ExZ+4
    * XV: Antoni się boi Berdysza i unika za wszelką cenę, ale sekret jest zachowany i pozycja Mai utwierdzona.
* 10 - Maja ma niesamowicie dobrą linię przekonywania - najpewniej powiedział Alanowi, na pewno coś chce od tego derelictu i nie chce by załoga coś wiedziała bo nie chce im robić krzywdy
    * TrZ (wszyscy chcą w to wierzyć) +4:
    * VVV: Maja ma dowodzenieniepisane i ma Kornelię i Antoniego za sobą. I dowodzi w sprawie Berdysza.
