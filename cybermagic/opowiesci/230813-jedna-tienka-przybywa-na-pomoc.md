## Metadane

* title: "Jedna tienka przybywa na pomoc"
* threads: salvagerzy-lohalian
* motives: anomalia-rzadka, fish-in-water, magowie-pomagaja-ludziom, naprawa-wizerunku-grupy, rytual-doroslosci
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

230808-naiwny-tien-i-straszny-potwor-INCOMPLETE

* [230726 - Korkoran płaci cenę za Nativis](230726-korkoran-placi-cene-za-nativis)

### Chronologiczna

* [230808 - Nauczmy młodego tiena jak żyć](230808-nauczmy-mlodego-tiena-jak-zyc)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ""
* THEME & VISION:
    * ?
* Cel strategiczny:
    * Pokazać, że ten Orbiter tutaj to gorszy sort
* Cele poboczne
    * Ukonstytuować podstawowe postacie

### Co się stało i co wiemy

* w okolicy bardzo dużo jednostek Orbitera i Noctis jest rozbitych
* grupa czyszcząca z ramienia Orbitera, z Astralną Flarą - mają podczyścić teren; baza to Lohalian

### Co się stanie (what will happen)

* S1: spotkanie z dowódcą (wyraźnie zmaltretowany życiem, nie chce tu być)
* S2: gdzie będziemy spać?
* SN: żołnierze traktują ją raczej jak niebezpieczne jajko; książeczka 'jak traktować tienkę'

* SN: spotkanie z inżynierem - nic nie działa, wykryli jednostkę w zimnie. Może tam coś jest.
* SN: wejście na uszkodzoną noktiańską jednostkę, szabrujemy

* SN: odśnieżanie 


### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Mikołaj Larnecjat: dowódca grupy wydzielonej sprzątającej Przelotyk Północny; stracił bliskich podczas wojny
    * OCEAN: (E-O-): zamknięty w sobie, rzetelny, spokojny i dobry w powtarzalnych operacjach. "Nie powinienem był przetrwać wojny. Jeszcze Orbiterowi jednak się przydam."
    * VALS: (Conformism(Duty), Humility): jak coś ma zrobić, zrobi i dostarczy. "Nieważne czy mi się podoba, czy nie. Jestem elementem maszyny Orbitera."
    * Core Wound - Lie: "Wszystko, co kochałem zginęło i zostało zniszczone. Jestem sam." - "Nie przywiązuj się. Jest tylko misja."
    * Styl: Spokojny, absolutnie nie inspirujący i cichy miłośnik muzyki klasycznej pochłonięty swoimi myślami, ale wykonujący każde polecenie rzetelnie i sensownie.
    * metakultura: Atarien: "dowódca każe, ja wykonuję. Na tym polega hierarchia. Tak się buduje coś większego niż my sami."
* Rachela Brześniak: czarodziejka Orbitera (katalistka i technomantka), dołączona do pomocy w dekontaminacji terenu; na uboczu, bo jest "dziwna"
    * OCEAN: (N+C+): niezwykle ostrożna z czarowaniem i anomaliami, niewiele mówi i raczej trzyma się sama ze sobą.
    * VALS: (Achievement, Universalism): Skupia się dokładnie na zadaniu, by je wykonać może pracować z każdym. "Dekontaminacja to moje zadanie. Zajmijcie się resztą."
    * Core Wound - Lie: "Współpracowała z żywymi TAI i na jej oczach Rzieza zniszczył jej przyjaciół; myśli że to jej wina" - "Magia MUSI być kontrolowana. Wszystko co anomalne musi być reglamentowane."
    * Styl: Cicha, pełna wewnętrznego ognia. Skupia się na dekontaminacji z żarliwością godną faerilki.
    * metakultura: Faeril: "ciężką pracą da się rozwiązać każdy problem - to tylko kwestia cierpliwości i determinacji"
* Antoni Paklinos: chorąży odpowiedzialny za integrację z ramienia grupy wydzielonej Salvagerów Lohalian
    * OCEAN: (A-C+): niezwykle cierpliwy i nieźle współpracuje z ludźmi, jednocześnie dość agresywny; lubi dawać ludziom szansę i ich testować. "Paklinos to swój chłop; nieco zmierzły i testujący, ale daje radę"
    * VALS: (Security, Benevolence): niezależnie czego nie dostaje i czym się nie zajmuje, kluczowa dla niego jest pomoc innym i zadbanie o ochronę. "On nawet do kibla wchodzi z nożem"
    * Core Wound - Lie: "Zrobiłem to, co należało zrobić i zostałem wygnany na planetę" - "Nic co robimy nie ma znaczenia, ale możemy pomagać w najbliższej okolicy. Od myślenia jest dowództwo."
    * Styl: Dość zaczepny ale pozytywny facet; dużo testuje i zawsze jest 'in your face'. Zaufany i daje radę.
    * metakultura: Atarien: "Oddział jest tyle warty ile jego wytrenowanie i spójność z rozkazami. Co nie jest testowane i szkolone, to się nie dzieje."
* Tomasz Afagrel: kapral grupy wydzielonej pod dowództwem Larnecjata; były przemytnik
    * OCEAN: (E+O+): nie boi się ryzyka i zawsze pierwszy do szydery czy okazji. "Jeśli jest zysk, warto spróbować."
    * VALS: (Hedonism, Power): pierwszy szuka okazji do dorobienia czy dobrej zabawy. Straszny hustler. "Nikt nie dostanie za darmo niczego. Trzeba chcieć i działać."
    * Core Wound - Lie: "Może jestem z bogatego domu, ale wojna zniszczyła wszystko co mieliśmy" - "KAŻDĄ okazję trzeba wykorzystać, WSZYSTKIEGO spróbować - by nie żałować."
    * Styl: Głośny, pełen energii, uśmiech drwiący i szyderczy. Zawsze szuka sposobu na zabawę czy okazję. Lojalny wobec "swoich". Kolekcjonuje fanty.
    * metakultura: Atarien: "moja rodzina miała wszystko i wszystko straciła. Moja kolej odbudowania potęgi rodziny."
* Irek Korniczew: główny inżynier wysłany przez Flarę by zająć się obsługą bazy tymczasowej
    * OCEAN: (C+O+): tysiące drobnych pomysłów i usprawnień, poszukiwanie nowego i wszystko przy poszanowaniu istniejącej maszyny. "Wszystko da się usprawnić i zrobić lepiej. Dojdziemy do tego jak."
    * VALS: (Stimulation, Achievement): coś nowego, coś ciekawego, coś rozwijającego. A nie gówno na patyku. "Te drobne powtarzające się błędy powinna rozwiązać TAI."
    * Core Wound - Lie: "Zawsze inni dostawali wszystko co ciekawe a ja tylko maintenance, mimo że to moja specjalizacja" - "Te wszystkie tematy maintenance to misja karna; nie mogę brać na siebie"
    * Styl: Wiecznie zirytowany, ale pomocny. Narzeka na sprzęt i zadanie. Jednocześnie - jeden z lepszych inżynierów.
    * metakultura: Atarien: "jeśli nie dam rady się wykazać, rozpłynę się w tłumie szarych twarzy"

### Scena Zero - impl

.

### Sesja Właściwa - impl

Estella bez większych problemów dotarła do bazy Orbitera - była wysłana w opancerzonej jednostce, do tego z eskortującymi jednostkami defensywnymi.

* Serafin: "Estello, jesteś PEWNA że nie chcesz żadnej świty? Żadnych żołnierzy?"
* Estella: "Absolutnie"
* Serafin: "Ród dostarczy Ci komandosów jakbyś potrzebowała, wesprze defensywy Orbitera."
* Estella: "Jeśli będzie to potrzebne, to o to poproszę."
* Serafin: "Keep in touch, shortie /z sympatią"
* Estella: "Zrobi się."

Estella wleciała w obszar IFF placówki Orbitera. Od razu oczywiście autonegocjacje (iden, co robimy, podejście itp). Estella wzmocniła temperaturę w Lancerze i opuściła trap. Przed nią - zimne górskie tereny. I częściowo wbity w skałę Lohalian - kwatera Orbitera. Pada śnieg. Oczywiście. Estella została przechwycona przez żołnierza.

* Przemek: "tien Gwozdnik, jak mniemam?"
* Estella: "w rzeczy samej"
* Przemek: "mam Cię odprowadzić do tymczasowej kwatery. Żebyś mogła zrzucić... (patrzy na nią)... pakunki."
* Estella: (uśmiech) (ma 1 torbę) "to będzie krótkie, a potem?"
* Przemek: "Dowódca chce z Tobą porozmawiać. Komendant Larnecjat."

To nie potrwało długo. Estella została zaprowadzona na mostek Lohaliana. Lohalian ogólnie jest zrujnowany, ale żołnierze czyszczą a w środku działa jakoś life-support. Lohalian był supply shipem, więc miał powierzchnię. Dowódca - komendant Larnecjat - czeka na Estellę.

* Mikołaj: "tien Gwozdnik, cieszę się z obecności sojuszniczki" /completely flat tone
* Estella: "Komendancie, chorąża Gwozdnik melduje się jako sojuszniczka z sił Aurum i rekomenduję włączenie mnie do łańcucha dowodzenia Orbitera"
* Mikołaj: (podniósł oczy i spojrzał na Estellę) "Chorąża Gwozdnik..." (z lekkim zainteresowaniem) "...masz rejestr dokonań?"
* Estella: (podaje rejestr) (M. czyta z zainteresowaniem)
* Mikołaj: "nie jesteś zielona i zasłużyłaś na swój stopień. Nie tego się nie spodziewałem po tienie Aurum."
* Estella: "są tienowie i są tienowie."
* Mikołaj: "dowódco."
* Estella: "dowódco."
* Mikołaj: (lekki uśmiech) /nadal jest taki... flatline, ale już mniej.
* Mikołaj: "szeregowy Matrewniak się zajmie Twoją... integracją. Potrzebujesz czegoś, idziesz do niego."
* Estella: "zauważyłam, że moja kwatera nie jest w koszarach"
* Mikołaj: "jesteś jedyną kobietą na pokładzie jednostki poza porucznik Brześniak. Plus, jesteś... sojuszniczką, a nie natywna."
* Estella: "jeśli mam być przydatna, muszę dobrze współpracować z żołnierzami."
* Mikołaj: "co proponujesz?"
* Estella: "jeśli w koszarach nie ma miejsca..."
* Mikołaj: "jest, chorąża Gwozdnik. Nie mamy koszar. Mamy tymczasowe koszary - sfabrykowane prycze wewnątrz jednej ze zniszczonych ładowni."
* Estella: "nie widzę problemu"
* Mikołaj: "więc ja też nie widzę problemu. Zmieni się, dasz mi znać."

Chorąży Paklinos będzie odpowiedzialny za integrację Estelli i zajmowaniem się jej działaniami. Paklinos ma koło 40. Wyraźnie jest weteranem.

* Estella: /salut
* Paklinos: tien Estella Gwozdnik?
* Estella: tak jest. Chorąża Gwozdnik.
* Paklinos: Chorąża Gwozdnik. Świetnie. (uśmiech) Dostałem polecenie przenieść Cię do koszar?
* Estella: na moją prośbę.
* Paklinos: Nie będę honorował Twojego stopnia. Nie jesteś w strukturach Orbitera. Nie dam Ci ludzi do dowodzenia. Czy to zrozumiane?
* Estella: tak jest.
* Paklinos: /lekko się rozpogodził
* Paklinos: czyli jesteś lokalnym... przewodnikiem? Masz dodać kontekstu do jednostek nie-Orbitera z którymi się spotkamy?
* Estella: Orbiter poprosił ród Gwozdnik o wsparcie. Nie mam więcej szczegółów.
* Paklinos: i wysłali Ciebie. W czym możesz się przydać?
* Estella: (skrócony raport: historia, analiza wzorów, taktyka, walka, magia ognia)
* Paklinos: co do magii, nie. Nie póki nie porozmawiasz z porucznik Brześniak. Nie była szczęśliwa, że będzie tu tienka. (podkreślił)
* Estella: jestem w stanie to zrozumieć.
* Paklinos: jak chodzi o wzory. Jesteś dobra we wzorach, tak?
* Estella: tak uważam i to wynika z badań
* Paklinos: dobra. Tam jest szmata, trzeba przetrzeć stołówkę. Zgodnie ze wzorem.

Estella próbuje zrobić coś, co pokaże nieprawdziwy wzór ale się wyplącze.

Tr Z +3:

* V: Estella znalazła NIEPRAWDZIWY wzór, ale taki który tam akurat jest ("co piąte krzesło jest źle odstawione bez jednego i w lewym pionie wszystkie stoły mają smugi")
    * -> pokazuje Paklinosowi trochę poczucie humoru, trochę kompetencję a trochę potencjalne 'malicious compliance' - 'fuck with me, I will fuck with you'
* X: Estella będzie to robić jakieś 15 minut bo Paklinos przyciśnie
* Vz: Paklinos po 15 minutach zmienił zdanie. Estella szoruje, nikt nie jest w Lancerze, Paklinos wie co chciał
    * -> Paklinos ma +1 punkt szacunku dla Estelli

Estella zgarnia rzeczy z tymczasowego, zanosi do odpowiedniej pryczy. Na wejściu Estella widzi jakieś 30 chłopa. Wszyscy na nią PATRZĄ. A ona jest _immaculate_ i nawet się nie pobrudziła. Paklinos przedstawił 

* "dobra owieczki..."
* "to jest CHORĄŻA Gwozdnik. Traktujecie jak chorążą."

Z sali: "panie chorąży, czy książeczka obowiązuje?" Odpowiedź: "częściowo." Zrobił kwaśną minę, że ktoś pyta o KSIĄŻECZKĘ.

* Estella: "czy mogę się zapoznać z instrukcją obsługi tienów?"
* Paklinos: "dostaniesz egzemplarz na wypadek gdyby pojawił się jakiś tien."
* Estella: "dziękuję"

Paklinos: (enumerował 4 żołnierzy i Estellę) - jak się Gwozdnik rozłoży, to idziecie odśnieżać. (Orbiterowcy nie wykazują się zapałem, ale idą). Paklinos: "kluczem są sensory, działka itp."

Estella była gotowa w ciągu 5 minut. Wzięła swój ciepły kombinezon, Orbiterowcy też mają ciepłe stroje i poszli odśnieżać. Estella pracuje tak, by 'pull her weight', ale też się nie popisuje i nie używa magii. Estella ma dobre buty i dobry sprzęt, Orbiter też nie oszczędza i mimo kiepskiej pogody dzielnie odśnieżają statek.

Tr +3:

* Vr: Estella robi dobrą robotę, żołnierze widzą, że jest "swoja".

Jeden z żołnierzy do Estelli:

* Tomasz: Ty jesteś tienką, chorąża?
* Estella: Owszem
* Tomasz: Czemu nie użyjesz magii by to odśnieżyć? Nie te umiejętności? /curious
* Estella: (matter-of-factly) Władam magią ognia. Jednocześnie używanie magii na terenie jakiego nie znasz - a właśnie tu przyjechałam - jest ryzykowne. To jak chlać bimber nie wiadomo kto go zrobił.
* Tomasz: (śmiech) Tomasz jestem.
* Estella: Estella.

Nie robią pokazu siły. Nie ufają sobie jeszcze, ale Tomasz jest nieco bardziej pro-Estellowy niż był. A jest to wygadany koleś, to co on mówi jutro pół wojska mówi. Estella docenia, że Paklinos to dobrze złożył.

* X: Estella zaproponowała nieco inny podział pracy, żołnierze są ogólnie za, acz nie ma przyspieszenia. Ot, they humored her. Nie żeby wiedzieli lepiej.
* X: poszło im trochę wolniej, ale żołnierze tego nie zauważyli. Paklinos i sztab szybciej, ale nie ma to znaczenia - tienka jakoś się wdraża i ogólnie jest spoko.

Estella jak tylko kończy z przerzucaniem śniegu to idzie do Racheli Brześniak. Rachela siedzi w zaawansowanym (na oko) laboratorium zrobionym z byle czego. Nie do końca wiadomo co i jak. Wygląda 'a bit superstitious'. Rachela spojrzała na Estellę. Estella salutuje i "chorąża Estella Gwozdnik, proszę o chwilę rozmowy na tematy magiczne".

* Rachela: spocznij. Słucham. /patrzy spokojnie
* Estella: Ty znasz ten teren, ja nie. Z czym się wiąże czarowanie tutaj, co jest niebezpieczne, jak działać, na co uważać, czego nie robić? Jak nie być głupią?
* Rachela: osobiście rekomenduję w ogóle nie czarować
* Estella: (zaskoczenie)
* Rachela: są tu anomalie. Są tu dziwne zachowania. Jak zauważył porucznik Korniczew, nic nie działa prawidłowo.
* Estella: porucznik Korniczew?
* Rachela: główny inżynier. Uważa to za jakąś formę sabotażu. Ja uważam to za działania anomalne.
* Estella: rozumiem. Bardziej terenowo, aura... (Estella NIE CZUJE żadnej anomalii aury)
* Rachela: nie wiem. Rekomenduję nie czarowanie jeśli to nie jest absolutnie niezbędne. Dopóki nie zrozumiemy co się dzieje.
* Estella: nie wyczuwam żadnej aury.
* Rachela: nie mam twardych dowodów na anomalię rzeczywistości dookoła. Mam dowody pośrednie. Zaklęcie nie zrobi Ci krzywdy. Aż zrobi.
* Estella: jakieś przykłady? (dowody?)
* Rachela: nie rzuciłam ani jednego zaklęcia odkąd tu jestem, więc nie mam.
* Estella: to skąd przekonanie?
* Rachela: dostroiłam się do aury. Jak nie czarujesz, zaczynasz... wyczuwać. Jeszcze nie znalazłam źródła ani typu. Ale rzeczy nie działają jak powinny i miejscowi uważają miejsce za nawiedzone.
* Estella: ktoś tu żyje?
* Rachela: nie. Lokalni górale, boronici. 
* Estella: (propozycja pomocy w badaniach, analizie - by to wykryć)
* Rachela: jesteś tienką. Jesteś... delikatna. Przyszłaś bawić się w żołnierza. (powiedziała to z lekko zagubionym zainteresowaniem, there was no malice, raczej dziecięce zadziwienie)
* Estella: (zwęża oczy) Gdybyś nie miała wyższego stopnia i gdyby nie naruszało to łańcucha dowodzenia, tu i teraz wyzwałabym Cię na pojedynek by zademonstrować. (podkreśla że to kwestia honoru)
* Rachela: (NIE DOTARŁO do niej co powiedziała Estella) Oczywiście, z przyjemnością mogę ja wyzwać Cię na pojedynek, byle bez magii, bo się dostrajam.
* Estella: Widzę, że nadajemy na zupełnie różnych częstotliwościach. Chcesz to wyzwij. Ale chodzi tylko o to... nie przyszłam się tu bawić. Przyszłam tu pomóc. (zimno)
* Rachela: (zamrugała) W takim razie przepraszam (całkowicie nie zrozumiała; robi to bo chyba powinna). Czyli rozumiem, że nie chcesz pojedynku tylko to była figura retoryczna.
* Estella: Nie dążę do pojedynku. Nie o to mi chodzi.
* Rachela: I chcesz mi pomóc znaleźć problem. Naprawdę pomóc.
* Estella: Po to tu jestem. Po co inaczej kolejny mag na tym terenie?
* Rachela: (myśli)
* Estella: Nie prosiłaś o wsparcie?
* Rachela: Ja? Nie, ja nie. Ja prosiłam o stymulanty.
* Estella: Mag na stymulantach to kiepski pomysł.
* Rachela: Nie czaruję.
* Estella: To nic nie zmienia. (Estella WIDZI ogrom pracy zrobionej przez Rachelę)
* Rachela: Dobrze. Jest taka jednostka, zniszczona jednostka, jest w skale. Czy w lodzie. Wejdź tam i "poczuj". Nazwaliśmy ją Skalniak-7. Nie wiemy co to. Noktiańska.
* Estella: Zamierzasz złożyć zapotrzebowanie na taką operację? Zaplanować?
* Rachela: (na jej twarzy po raz PIERWSZY pojawiła się irytacja) Ogarnij to z komendantem jeśli naprawdę chcesz mi pomóc. Jestem jedyną osobą odpowiedzialną za dekontaminację na tej operacji. I żadne detektory nie pokazują Skażenia. Ale ono tu JEST. To sprawia, że wyglądam na paranoiczkę albo idiotkę. Jednocześnie jeśli to jest tak ukryte i tak skuteczne, to mamy do czynienia z czymś groźnym. Znajdę to statystycznie. Jeśli nie dam rady, dostroję się. Jeśli możesz mi pomóc przez przeskanowanie Skalniak-7, super. Jeśli nie, po prostu nie przeszkadzaj i nie czaruj.
* Estella: Doskonale. Mam tylko jedno pytanie. Kiedy ostatnio spałaś dłużej niż 10 minut?
* Rachela: Wysypiam się zgodnie z regulaminem i przydziałem. Dostrojenie mnie destabilizuje. Zbliżam się. (ogień w środku)
* Rachela: (widząc niezrozumienie na twarzy Estelli) Przeczytaj raporty. Wszystko dokładnie opisałam. Masz pytania - zadaj je wtedy. Odpowiem.

Estella próbuje przeczytać raporty i zrozumieć co się dzieje z Rachelą. Czy jest coraz bardziej paranoiczna. Czy jest nadal stabilna intelektualnie i emocjonalnie. Bo Estella bardzo - BARDZO - nie chce mieć niestabilnego maga. Estella szuka odchyleń, podstawowe zrozumienie sytuacji.

Ex Z (mastery) +3:

* Vz: Estella analizuje raporty i zauważa, że Rachela **przeprowadziła analizę prawidłowo**. Pozornie wszystko jest w porządku. Ale statystyczne uszkodzenia, niesprawne radary, rozbijane statki itp. wskazują, że tu coś JEST nie tak. Astralna Flara i sprzęt z niej wysyłany mówi, że wszystko w porządku. Ale nie jest w porządku. Bo tylko tu jest inaczej. Tak samo detektory. Tu **jest** coś anomalnego. I tego nie widać. Fundamentalnie, Rachela rzeczywiście coś znalazła i **nikt** tego nie widzi.
* X: Wszyscy "wiedzą", że coś jest tu nie tak, ale Rachela ani Estella nie mają do końca jak przekonać. Tzn. wszyscy fundamentalnie ufają, że Rachela ma rację. Ale nie widzą niebezpieczeństwa które widzi Rachela.
* X: Estella nie ma dostępu do głębszej analizy Racheli bez wsparcia danych z dowództwa. Acz, faktycznie, wygląda na bardziej niestabilną niż w dokumentach. Ale jej raporty są dalej sprawne. Jej umysł działa.

Nawet Estella to wie. Rachela nie używa visiatu jako filtr przez to, że nie czaruje. Przez to staje się bardziej wrażliwa na to, co odbiera. Sama podlega tym energiom, cokolwiek to jest.

Estella podbija do Paklinosa. To ważna sprawa. Sam na sam.

* Paklinos: Co mała, chcesz do domu? /śmiech (on mówi to przyjacielsko a nie by dokuczyć)
* Estella: (prych). Myślę, że jeszcze tutaj zostanę.
* Paklinos: To co chcesz?
* Estella: Pogadać w spokoju.
* Paklinos: (pokazał ręką że są w małym pomieszczeniu i nikogo nie ma)
* Estella: Porucznik Brześniak.
* Paklinos: _She does make an entrance, doesn't she._
* Estella: Wierzę jej.
* Paklinos: (zamrugał) Jesteś magiem. Ty coś czujesz? (poważnie)
* Estella: Nie, ale to nie jest dziwne.
* Paklinos: Wyjaśnij, sorki, ale magia to nie moja domena.
* Estella: Upraszczając. Jak strzelasz z pistoletu na kule to potem śmierdzisz prochem, tak? Jak używasz magii, to trochę nią nasiąkasz. To trochę... przytępiają Ci się na nią zmysły. Jak palaczowi. Mają gorszy węch. Jak przestajesz czarować, Twój 'węch' się wyostrza. (Z punktu widzenia Estelli TAKIE ZŁE PORÓWNANIE). Jednocześnie... jak nasiąkasz magią, to jesteś jej pełny. Jak nasiąknięta gąbka. Ona nie wchłonie. Ale gąbka która nie jest nasiąknięta będzie wciągać. Czyli. Przestając czarować wyczulasz zmysł magiczny ale otwierasz się na to co jest dookoła...
* Paklinos: ...a ona powiedziała, że tu jest jakaś dziwna energia. Czyli nią nasiąka.
* Estella: Kiedy ostatnio porządnie spała?
* Paklinos: Ja nie mam danych... (krótki check)... kwatermistrz mówi, że śpi przydziałowe 8 godzin. Ona zawsze pilnuje procedur.
* Estella: Wygląda jakby nie spała i zachowuje się... to jej normalne zachowanie?
* Paklinos: Wiesz, Rachela jest... _dziwna_.
* Estella: Zawsze taka była czy TERAZ taka jest?

Tp +3:

* V: Paklinos identyfikuje, że Rachela to jest Rachela
    * trzyma się reguł, ciężko pracuje, unika czarowania, ma ten wewnętrzny ogień, dokładna i ostrożna, taka... nieżyciowa.
    * ma swoją teorię i ją udowodni lub obali
    * te SUPER RAPORTY to 100% Rachela
    * Rachela monitoruje TEŻ swój stan i pisze go w raportach do dowództwa nic nie ukrywając łącznie z libido itp.
* V: I jednocześnie widzi, że jest z nią coś nie tak
    * nie wysypia się, a Rachela zawsze bardzo o to dba
    * jakby... zapominała. Rachela nie zapomina. To nie jest przemęczenie.
    * ogólnie jest słabsza, "mniejsza". Mniej niż zwykle.

Estella kontynuuje.

* Estella: jeśli na nią wpływa ta energia - czymkolwiek ona nie jest - to nie wiemy jak to się dalej rozwinie. I co się dalej będzie działo. Wolałabym że lokalny puryfikator traci kontrolę. (Estella lekko zbladła) Chciałabym wysłać te dane do analizy po swojej stronie.
* Paklinos: to będzie wymagało dyskusji z Komendantem. To sensowny oficer. Ale czemu nie Orbiter a Aurum?
* Estella: bo mój ród ma doświadczenie z magią bojową a tu jest teren na którym działo się dużo starć.
    * plus, Gwozdnikowie specjalizują się w miragentach. Proces transferu, przenikania itp jest im dobrze znany. Mogą mieć dobre rekomendacje.
* Paklinos: pogadam z głównym lekarzem, złożymy coś dla Komendanta. Pomożesz z raportem.
* Estella: oczywiście
* Paklinos: (patrzy na Estelle serio) Słuchaj, to nie jest żart. Nie wiem czy LUBISZ pisać raporty. Mamy TAI która Ci pomoże. To ważne, byś potraktowała to na serio. Nie wyglądasz na śmieszkę, ale ja lubię testować ludzi. Teraz Cię nie testuję. Rozumiesz to?
* Estella: Rozumiem. Gdybym nie uważała tego za poważne, to bym nie przyszła.

Pisanie raportu i przekonywanie Komendanta. Estella wykorzystuje nowe TAI tej jednostki, Maię. Ta Maia jest tu dwa tygodnie, to niedużo. Przez te dwa tygodnie Rachela nie rzuciła ani jednego zaklęcia (co Paklinos potwierdził jest u niej normalne). Estella opiera się o badania jej rodu (do których ma dostęp), obserwacje własne i próbuje przekonać go, by Coś Z Tym Zrobił oraz by dał dane jej rodowi do analizy.

BAZA: Ex. +Estella daje radę, (+risk assessment) (-bo dane wyjdą poza Orbiter)

Tr Z +4:

* X: Rachela dowie się, że to przez Estellę. Będzie wściekła.
* X: Komendant poprosi o dane Gwozdników na ten temat i przekaże to do Orbitera. (Estellę to nie boli, nie ma problemów a Gwozdnikowie to najwierniejsi sojusznicy Orbitera)
* V: Komendant potraktował to poważnie. Dane poszły na Orbiter łącznie z rekomendacjami Komendanta o tym, co się dzieje itp.
    * -> zwrotka będzie 'Rachela ma natychmiast rzucić zaklęcie, to rozkaz'
        * Rachela nie doceni
        * Dostanie bezpośredni rozkaz i go wykona.
            * I jej się poprawi długoterminowo
* X: 
    * Komendant i Paklinos WIEDZĄ. Oni patrzą na Estellę pozytywnie.
    * Sprawa jest utajona, więc nikt inny nie dowiedział się o przydatności Estelli.

Estella poprosiła kuzyna, by ten poszukał czy wiadomo im coś dziwnego o tym terenie. Kuzyn obiecał, że skompiluje te informacje.

To - te parametry i komponenty - sprawiły, że mag rodu Samszar byłby przydatny. Bo oni takie rzeczy wykrywają...

## Streszczenie

Tien Estella Gwozdnik dotarła do tymczasowej bazy Orbitera i bardzo próbowała pokazać się jako żołnierz a nie tienka. Robiła minimalną ilość problemu, odśnieżała z żołnierzami, nie żąda żadnych udogodnień. A potem odkryła, że lokalna czarodziejka Orbitera - dość DZIWNA - próbuje zrozumieć jaka tu jest energia. Zgodnie z danymi statystycznymi tu FAKTYCZNIE jest coś nie tak. Niestety, jeśli Rachela się integruje z dziwną energią, ona ją Skaża. Estella ostrzegła Orbiter, kazali Racheli rzucić czar. Rachela jest wściekła na Estellę. Ale - Rachela przetrwa. A Estella włączyła ród Gwozdników w akcję. Po analizie sytuacji, przydałby się kompetentny mag rodu Samszar. Bo normalnie nic nie da się wykryć..?

## Progresja

* Estella Gwozdnik: Salvagerzy Lohalian, oddział Orbitera ją chłodno zaakceptował; udowodniła swoją przydatność i wartość.
* Rachela Brześniak: lekko Skażona energiami Finis Vitae, po rzuceniu zaklęcia się wyfiltruje sama. WŚCIEKŁA na Estellę.

## Zasługi

* Estella Gwozdnik: chorąża i tienka, próbuje się zintegrować z Orbiterem i jej się to udaje - nie jest ponad odśnieżanie czy uczciwą pracę; nie chce czarować i zakłócać eksperymentu Racheli, ale jak wykryła w patternie dokumentacji że tu coś jest nie tak i Rachela najpewniej ulega Skażeniu, zaeskalowała do dowództwa Orbitera.
* Mikołaj Larnecjat: komendant Salvagerów Lohalian, nieco nieufny wobec tienów i bardzo pro-Orbiter; nieco zrezygnowany i melancholijny, ale robi robotę wystarczająco dobrze.
* Antoni Paklinos: chorąży,  odpowiedzialny za integrację Estelli, lubi testować; dał Estelli wpierw zadanie przetrzeć stołówkę, potem odśnieżać - by ją przetestować. Zdała test. Gdy Estella wykryła niewłaściwość mentalną w czarodziejce Orbitera, Paklinos stanął za Estellą by razem udało się im pomóc Racheli.
* Tomasz Afagrel: kapral, wesoły człowiek który jako pierwszy porozmawiał z tienką (a nie musiał). Pogadał z nią o magii i odśnieżaniu. Ogólnie, wesoły pozytywny koleś.
* Rachela Brześniak: chorąża i czarodziejka; jest bardzo dziwna i nie ma kontekstu społecznego; niezwykle pracowita i próbuje nie czarować jak może uniknąć. Bardzo dobra w pisaniu raportów i dokumentów, niesamowicie precyzyjna, unika czarowania. Przez to że nie czaruje Skaża się lokalnymi energiami - to sprawia, że ma pewne dysfunkcje. Ale jak dostała rozkaz 'rzuć czar', rzuciła. Wściekła na Estellę.
* Serafin Gwozdnik: tien i kuzyn Estelli, komunikuje się z nią i wspiera jej działania wzmacniające Orbiter. Pozytywny młody mag, LUBI Estellę i ją wspiera. Wziął na siebie badania tego terenu.

## Frakcje

* Salvagerzy Lohalian: grupa wydzielona z Astralnej Flary p.d. Larnecjata która ma za zadanie salvagować co się da i unieczynnić niebezpieczne technologie Orbitera i Noctis w Górach Hallarmeng. Dołączyła do nich tymczasowo chorąża tien Estella Gwozdnik z Aurum. Są nieufni, ale Estella szybko pokazała swoją wartość.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, W
                    1. Góry Hallarmeng: 'góry niespokojnego snu', interesujące miejsce gdzie cywilizacja nie sięga i pogoda jest zawsze paskudna
                        1. Przyprzelotyk: ogólnie elektronika zawodzi, entropia dominuje, magia działa nieco inaczej i wszystko się psuje
                            1. Korony Cmentarne: seria szczytów w kształcie korony, gdzie rozbiła się nadzwyczajnie duża ilość jednostek podczas wojny
                                1. Ruina Lohalian: rozbita jednostka Lohalian, która stała się elementem Korony; jest tam tymczasowa baza Orbitera pod kontrolą Astralnej Flary
                                1. Dolina Krosadasza: wyjście z Koron Cmentarnych na dalsze obszary rozbitych jednostek

## Czas

* Opóźnienie: -15
* Dni: 5
