## Metadane

* title: "Figurka a kopie zapasowe"
* threads: dzien-z-zycia-terminusa
* gm: żółw
* players: xivara, irphis moltrus, cyris

## Kontynuacja
### Kampanijna

* [200507 - Anomalna figurka Żabboga](200507-anomalna-figurka-zabboga)

### Chronologiczna

* [200507 - Anomalna figurka Żabboga](200507-anomalna-figurka-zabboga)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* .

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

* Cyris obejmuje Araneę Diakon
* Xivara obejmuje Gerwazego Lemurczaka
* Irphis obejmuje Mariusza Grabarza (na sesji: 'Marysia Grabarz').

### Opis sytuacji

Tukan chce zniszczyć Nocne Niebo Kajrata przez zniszczenie kopii zapasowych danych używając Anomalicznej Figurki Żabboga. Przekazał to Gerwazemu Lemurczakowi, który zebrał sobie idealną ekipę.

## Punkt zero

.

## Misja właściwa

Zespół ma zatem figurkę Żabiego Bóstwa, która zniszczy dokumenty kopii zapasowych Nocnego Nieba. To pozwoli Tukanowi się zemścić na Kajracie (ale nie podniesie go w oczach Grzymościa bo Grzymość nie będzie wiedział) - Nocne Niebo upadnie zanim stanie się potężne i Grzymość będzie miał praktyczny monopol na mafię w Szczelińcu.

Jak jednak doprowadzić do tego, by ktoś odpalił ta figurkę - ktoś kto nie wie co ona robi i nie wie jak działa?

Gerwazy zaproponował, że doskonałym słupem - lub narzędziem do zdobycia informacji o Nocnym Niebie będzie jego "lokalna kuzynka", czyli Diana. Zatem Mariusz poszedł porozmawiać z Dianą. Ma to sens, bo kiedyś już Mariusz trochę pomógł Dianie - Diana mu trochę ufa.

Diana przywitała Mariusza z uśmiechem. Mariusz roztoczył piękne wizje o tym, jak to może dla Diany zrobić piękną reklamę, dać jej coś wartościowego - ale Diana zadała proste pytanie: co on od niej chce. Mariusz powiedział, że ma problem z mafią i musi zadziałać w jakiś sposób przeciwko nim. Czy Diana może podpowiedzieć mu kogoś z Nocnego Nieba. Diana próbowała przekonać Mariusza że to BARDZO zły pomysł - Kajrat i jego ludzie są super niebezpieczni - ale Mariusz powiedział że i tak jest już w kłopotach, to kwestia "uda mi się" lub "padnę".

* V: Diana pomoże, acz nie bardzo mocno
* XX: Mariusz zdradzi Dianę
* X: Konieczność ukrycia udziału Mariusza, bo Diana będzie mogła powiedzieć że to on

Diana dała Mariuszowi namiar na kilku gości z Nocnego Nieba którzy od czasu do czasu piją. Takie 4-5 osób. Jako, że między mafiami jest zimna (nie gorąca) wojna, nikt nikomu nie robi poważnej krzywdy i nikt nikogo nie zabija. W końcu wszyscy byli jeszcze niedawno kumplami.

Następny krok należał do Aranei. Użyła swojej częściowej zmiennokształtności i poszła do podziemnego klubu "Napartar", gdzie - jak pokazały drony Mariusza - znajdował się jeden z tych pięciu gości z Nocnego Nieba. Użyła swojego seksapilu, by zakręcić się koło tych mafiozów i dosypała im troszkę afrodyzjaka do drinków. Niewielki problem, nie w "Napartarze". Tak więc Aranea wyciągnęła mafiozów i poszła z nimi do Sensoplexu, tam gdzie jest koloseum.

* VVV: Oni się tego nie spodziewają i pójdą z Araneą. Nie wiedzą, że Aranea to Aranea, myślą że to jakaś śliczna Diakonka
* XO: będzie potrzebna natychmiastowa pomoc medyczna dla jednego z ludzi Nocnego Nieba jak już tam dotrą; jeden ma alergię na te środki

Tymczasem reszta Zespołu (Gerwazy, Mariusz) dotarli do Sensoplexu. Tam - gra świateł. Gang Technożerców robi tam sobie męską imprezę - siłowanie się, zapasy, wrestling. Czyli Aranea mówiąca o imprezie w Koloseum... przez przypadek mówiła prawdę.

Nic to, Gerwazy postanowił, że przejdą nie do Koloseum a w inne miejsce Sensoplexu, tam gdzie jest "labirynt" - poniszczone budynki tworzące świetny kształt jak na poligonie. Przekazał sygnał Aranei by ta odpowiednio ich przesunęła, by poszli nie na Koloseum a konkretną ścieżką - zastawi się pułapkę.

Więc pięciu silnych kolesi idzie z Araneą na imprezę, a tu zasadzka - przygotowane wcześniej przez Araneę bomby gazowe rozrzucone przez Gerwazego. Mafiozi, będąc ludźmi i nie spodziewając się tego - padli na ziemię. Ten jeden zaczyna wpadać w tryb drgawek i zbliżać się do śmierci.

Aranea chce go ratować. Gerwazy chce, by Mariusz zaczął czytać jego pamięć najpierw używając infiltratorów. Śmierć nie ma dla niego znaczenia. Aranea próbuje protestować, ale nie jest w stanie nic z tym zrobić. Bezsensowna, głupia śmierć.

Skanowanie umysłów wykonane przez Mariusza dało cenne odpowiedzi:

* VVV: mają lokalizację kopii zapasowych. To firma HardBack, komercyjna, niepowiązana z żadną z mafii. W Dolinie Biurowej w Podwiercie.
* XXX: siły Kajrata wiedzą, że jest jakaś Trzecia Strona. Ktoś za tym stoi, ktoś zaatakował i doprowadził do śmierci ich człowieka.
* V: Mariusz zaszumił wszystko skutecznie. Nie ma dowodów. Jest ZA DUŻO dowodów o WSZYSTKICH - a Niebo nie ma czasu ani sił się tym teraz zająć.

Dobrze. Mają informacje gdzie odpalić figurkę, ale komu ją ukraść i gdzie ma być? Gerwazy się zaśmiał - ma genialny pomysł jak to zrobić. Wystarczy wykorzystać słabość Diany - to, że jest ufna i wierzy ludziom - i użyć tego przeciwko niej. Niech ktoś ukradnie figurkę Dianie. To będzie idealne.

Ale kto? I tu Gerwazy też ma podły pomysł. Wśród Rekinów jest Feliks Keksik, niejaki "Pożeracz". Gerwazy od czasu do czasu, jak Keksik wejdzie mu w drogę, to zdewastuje łajzowatego Rekina. Niech zatem on będzie tym co wprowadza cały plan w życie. A bądźmy szczerzy, Pożeracz nie jest szczególnie bystry i inteligentny - za to chce mieć powodzenie u dziewczyn...

Tak więc znowu Mariusz idzie porozmawiać z Dianą. Po krótkiej rozmowie Diana jest w humorze - faktycznie, Mariusz znalazł dla niej perfekcyjnego arystokratycznego łosia (Pożeracza). Jeśli ktoś taki przyjdzie do Diany i będzie zostawiać u niej pieniądze... nieźle.

Mariusz poprosił Dianę o przysługę. Niech ona przechowa jego ważną, cenną figurkę w czasie gdy on idzie negocjować z mafią. Diana jest zmartwiona, ale przechowa - w sejfie.

...tego Mariusz nie przewidział. Nie wierzy w to, że Pożeracz poradzi sobie z sejfem...

AHA! Mariusz poprosił o przechowanie też innych "cennych" dokumentów, obiektywów, kontrolerów dron i innych takich. Figurka jest ważna, ale nie TAK ważna. Diana się zgodziła - schowa najcenniejsze rzeczy w sejfie a figurkę w bezpiecznym miejscu na zaplecze.

KATASTROFA OMINIĘTA.

Tymczasem Aranea, z nową formą (jeszcze inną, ale też ponętną) poszła porozmawiać z Pożeraczem. Przedstawiła mu się jako była dziewczyna Gerwazego - Gerwazy ją rzucił bo się nią znudził i ona teraz pragnie zemsty. Szukała kogoś kompetentnego i znalazła. Pożeracz się MEGA ucieszył a na zwiększenie jego uwagi zdecydowanie pomógł afrodyzjak wrzucony przez Araneę do jego szklaneczki, gdy Pożeracz nie patrzył.

Aranea splotła dość wiarygodną opowieść (dla kogoś kto nie zna dobrze Gerwazego ani Diany) - Diana ukradła Gerwazemu bardzo ważną figurkę i schowała ją w swoim klubie, Arkadia. Ta figurka ma kompromitujące sekrety na temat Gerwazego, ale żeby je wydobyć, trzeba uruchomić figurkę przy firmie HardBack. Pożeracz, jedynie patrząc w dekolt Aranei (i nieświadomy jej pajęczej natury) jedynie kiwał głową. Zakochał się.

* VVVVV: Zakochany w jednej z form Aranei Pożeracz ściąga kumpli i robią WJAZD. Wykonają misję od początku do końca i Pożeracz NIGDY nie znajdzie Aranei.
* XXX: Niestety, Pożeraczowi trzeba będzie pomóc; jest BARDZIEJ kompetetny oraz MNIEJ kompetentny niż się wydawało, jednocześnie.

Tak więc dwie noce później Zespół monitoruje sytuację. Pożeracz zebrał kumpli i będą się włamywać do Arkadii. Ale nad Arkadią mieszka Diana... samo w sobie nie będzie to wielką tragedią (bo bądźmy szczerzy, Pożeracz z kumplami będą w stanie wbić się do Diany i ją pokonać) ale Mariusz swoimi kamerkami podsłuchał, że wszyscy napastnicy są przekonani, że figurka jest w sejfie. I będą próbowali UKRAŚĆ SEJF.

A w sejfie nie ma figurki.

KATASTROFA POTENCJALNA.

Nowy plan Gerwazego - wyciągnąć Dianę. Na szybko sprawdził odpytując swoich przydupasów, czy w pobliżu gdzieś jest jakaś koleżanka Diany - tak, w klubie Pierwiosnek bawi się Melinda Teilert; całkowicie bezwartościowa arystokratka nie władająca magią. Gerwazy pojechał do tego klubu swoim ścigaczem, wyizolował Melindę i ją zastraszył. Płacząca Melinda zadzwoniła do Diany, że jeśli ta się nie pojawi to Melinda nie wyjdzie nigdy z klubu - tak Gerwazy jej powiedział.

Diana to odebrała. I teraz Diana się boi, bardzo. Wie, jakiego typu osobą jest Gerwazy. Wie, że jeśli ściągnie policję to będzie katastrofa (nie teraz a kiedyś).

* VVV: Diana przyjdzie, nie robiąc problemów. Nie ma jak wygrać w tej sytuacji.
* XX: ALE jednak Diana coś wymyśliła - wkręca Gerwazego w groźne zachowania, inni stają w obronie Melindy i Diany, Gerwazy daje w mordę i trafia do aresztu.

Tak więc Diana uratowała Melindę przed Gerwazym który nie chciał robić Melindzie krzywdy. Gerwazy wygrał wszystko (choć jedna noc w areszcie jest bardzo denerwująca). Ale - co ważne - Diana nie jest w stanie przeszkodzić Pożeraczowi. Mimo, że Diana powiąże bezpośrednio Gerwazego i Mariusza z tematem kradzieży figurki to nikt jej nie uwierzy - bo co niby zrobili? Jakie Diana ma dowody? A Nocne Niebo nie będzie miało czasu ani siły się tym zajmować.

Ostatnie co zostało to Mariusz włamujący się do Diany by otworzyć sejf, wyjąć cenne rzeczy, wsadzić tam Figurkę, zmienić hasło na "1234" i się oddalić. Potem Mariusz miał przyjemność obserwowania, jak towarzysze Pożeracza kombinują z sejfem a Pożeracz mówi, jak to Diana jest głupia i z głupiego rodu - sprawdził "1234" i miał rację. Wszyscy się z Diany śmieją.

Mariusz mógł przekazać to nagranie Gerwazemu. Gerwazy by docenił. Ale Mariusz nie chce tego Dianie robić, mimo wszystko... bez przesady.

A Pożeracz? Triumfalnie wszedł do HardBack, odpalił figurkę. NIBY nic się nie stało. Wyproszono go stamtąd, ale kopie zostały zniszczone.

Podejrzewam że do dzisiaj Pożeracz szuka swojej ukochanej, byłej dziewczyny Gerwazego...

Mariusz pomógł Pożeraczowi, by reputacyjnie ten nie został całkowicie zniszczony. Przy okazji dostając wdzięczność Pożeracza.

## Streszczenie

Tukan chce zniszczyć kopie zapasowe Nocnego Nieba. Gerwazy Lemurczak to zrobił - znaleźli osobę której się ukradnie (Diana), osobę która ukradnie (Pożeracz) i lokalizację kopii zapasowej (HardBack w Dolinie Biurowej). Manipulując za kulisami udało im się to wszystko osiągnąć, acz jeden gość od Kajrata nie żyje, Pożeracz jest zakochany w nieistniejącej dziewczynie a Diana wie, że Mariusz i Gerwazy za tym wszystkim stali.

## Progresja

* Diana Lemurczak: wie, że Gerwazy Lemurczak i Mariusz Grabarz (którego uważała za przyjaciela) doprowadzili do okradzenia Arkadii i jej mieszkania. Połączyła to z Żabbogiem, ale nie ma dowodów i nikt jej nie słucha. Aha, wie, że to "Pożeracz" był egzekutorem.
* Feliks Keksik: wyprztykał się z surowców, zrobił sobie wroga w Dianie, stracił sporo reputacji i gdyby nie Mariusz Grabarz, rodzice kazaliby mu wracać do domu w hańbie.

## Zasługi

* Gerwazy Lemurczak: wyreżyserował akcję zniszczenia kopii zapasowych Nocnego Nieba używając Figurki Żabboga. Doprowadził do śmierci jednego z nieważnych ludzi Kajrata, bo tak.
* Mariusz Grabarz: zdradził Dianę stając z Gerwazym; przekonał ją by mu pomogła i zrobił z niej "cel" kradzieży figurki Żabboga. Plus, jednostka śledząco-czytająca pamięć.
* Aranea Diakon: wabik; zwabiła mafiozów Kajrata na czytanie pamięci a potem wkręciła Pożeracza. Liberalnie korzysta z afrodyzjaków i innych środków. Nie chciała niczyjej śmierci.
* Diana Lemurczak: osoba przechowująca Figurkę Żabboga dla Mariusza, potem wywabiona przez Gerwazego z domu. Jedyne co osiągnęła to wsadzić Gerwazego na 1 dzień do aresztu dzięki czemu odkryła, że Mariusz też stał za tym wszystkim.
* Melinda Teilert: bawiła się w klubie Pierwiosnek. Wpadł tam Gerwazy Lemurczak i wykorzystał ją jako przynętę na Dianę. Przerażona Gerwazym.
* Feliks Keksik: "Pożeracz". Ten, który faktycznie ukradł figurkę Żabboga i ją uruchomił w HardBack. Wyprztykał się ze środków by skończyć jako pożyteczny idiota - i wie o tym.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Osiedle Leszczynowe
                                    1. Klub Arkadia: nad nim jest mieszkanie Diany gdzie jest sejf. Zarówno do klubu jak i do mieszkania włamał się Pożeracz z ekipą; okradli jej sejf.
                                1. Magazyny sprzętu ciężkiego
                                    1. Klub Napartar: podziemny klub gdzie miłośnicy nielegalnych klimatów mogą się dobrze bawić. Napierniczanka jest na porządku dziennym, też orgie.
                                1. Sensoplex
                                    1. Koloseum: gdzie miejsce ma typowa męska impreza Technożerców: wrestling, zapasy, nikomu nie dzieje się poważna krzywda
                                    1. Labirynt: porujnowane budynki, z przejściami podziemnymi i pasażami; obszar przypominający labirynt. Tam Gerwazy poznał lokalizację backupów Nocnego Nieba.
                                1. Dolina Biurowa: jest tam firma HardBack, trzymająca backupy Nocnego Nieba. A raczej: były backupy; Pożeracz je zniszczył figurką Żabboga.

## Czas

* Opóźnienie: 17
* Dni: 7
