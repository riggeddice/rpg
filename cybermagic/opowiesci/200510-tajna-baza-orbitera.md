## Metadane

* title: "Tajna baza Orbitera?"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [200509 - Rekin z Aurum i fortifarma](200509-rekin-z-aurum-i-fortifarma)

### Chronologiczna

* [200509 - Rekin z Aurum i fortifarma](200509-rekin-z-aurum-i-fortifarma)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* .

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

* Gabriel poprosił o kilkudniowy urlop by spotkać się z Natalią; uważa, że Natalia robi katastrofalny błąd próbując zdobyć wsparcie Grzymościa
* Kallista dewastuje Gabriela i wstrzykuje mu środki wskazujące na to że to atak ludzi Grzymościa - ale maskuje to jako atak grupy Rekinów
* Natalia podkupuje grupę najemną która ma wrócić do Aurum.
* Kallista obiecuje pomoc Natalii w zamian za tymczasową bazę dla Grzymościa na terenie Aurum. Z rozkazu Talaranda maskuje ślady.

## Punkt zero

.

## Misja właściwa

Pięknotka nie zostawiła Gabriela samego z tymi wszystkimi cholernymi dokumentami. Próbowała trochę pomóc. Gabriel - ku największemu zdziwieniu i urażeniu Pięknotki - Gabriel postanowił się wykręcić z dokumentów. Powiedział, że ma coś bardzo ważnego do zrobienia i sprawdzenia. Nigdy jeszcze się nie wykręcał, choć też dokumentów nie lubi. Załatwił zastępstwo - ku ogromnemu zaskoczeniu Pięknotki, załatwił Laurę. Studentkę Tukana...

Pięknotka nie wnikała - nie ma w zwyczaju wnikać w prywatne sprawy Gabriela i wraz z Laurą skupiła się na dokumentach. A Laura jest w tym niezła. 

Dwa dni później Pięknotka dostała alarm - Gabriel jest w Szpitalu Terminuskim. Terminus down. Gdzieś poszedł bez wsparcia - i ktoś go pozamiatał.

Gabriel jest już przytomny, ale bardzo słaby. Przy Gabrielu jest pewna dama, odwiedza go. Pięknotka zdecydowała się poczekać, ale dama nie chce wychodzić. Pięknotka poczekała jeszcze 15 minut, aż owa dama sobie poszła. Natalia Tessalon. Poszła, przedstawiła się Pięknotce i poszła do swojego brata... chyba.

Pięknotka weszła do Gabriela. Ten się lekko uśmiechnął. Pokiwał głową - kto by pomyślał że NATALIA przyjdzie się z nim spotkać. Myślał, że ona raczej go spróbuje skrzywdzić, a była bardzo zmartwiona obecnością Gabriela w szpitalu i czy nic mu nie jest. Gabriel przyznał się Pięknotce, że nie pamięta NICZEGO po wyjściu z Centrali - nie wie czemu stracił przytomność, kto go pobił (został pokonany, nie zbrutalizowany a fachowo pokonany). Ktoś wyczyścił mu pamięć. Najpewniej - jak pokazują dane lekarskie - wyczyszczono ją farmaceutycznie. To nie był neuronauta, to były środki magiczne.

Pięknotka zna tego typu środki - to terminuskie środki, wymagające autoryzacji itp. Są też dostępne w Cieniaszczycie.

Gabriel pamięta, że na pewno coś zostawił na kompach pustogorskich. Ale nie pamięta co.

Pięknotka wyraźnie widzi, że celem była minimalizacja szkód i zniszczeń u Gabriela. Młody terminus wyjdzie za parę dni, nic na stałe. Po prostu na coś się wpakował... wiedząc, że Gabriela coś zainspirowało w dokumentach, z cholernie ciężkim sercem stwierdziła, że musi przejrzeć dokumenty... coś znalazł...

Pięknotka usłyszała męski krzyk. Potem wyładowanie magiczne, powstrzymane przez autokatalityczne systemy szpitala. Pobiegła tam natychmiast - na ziemi leży zakrwawiony Myrczek; dostał szpicrutą w twarz. Nad nim Sabina, z krwawiącą szpicrutą i patrzącą na Myrczka i na Natalię Tessalon. Pięknotka usłyszała od ochroniarza, że Sabina zaatakowała Myrczka i chciała rzucić nań zaklęcie. Myrczek poszedł do lekarza, Sabinę wzięła Pięknotka do aresztu.

Zapytana, Sabina powiedziała co się stało - Natalia ją napadła z uwagi na rytuał który Sabina odradzała. Ignacy się uniósł i chciał bronić Sabinę. Sabina go zdzieliła i chciała rzucić czar przeciw autokatalitycznym systemom (co nie miało szans zadziałać). Pięknotka powiedziała Sabinie, że zrobiła najgłupszą chyba rzecz którą mogła. Sabina odpowiedziała, że w ten sposób Natalia nie uzna, że Myrczek stoi po stronie Sabiny i jest sens w niego bić. Jest bezpieczny. A jeśli przez to trafi do więzienia na jakiś czas... straszne.

Sabina poprosiła Pięknotkę, by ta zdjęła z Sabiny Myrczka. Ona jest niebezpieczna. Niech Myrczek wróci do swojego świata grzybów.

Pięknotka poszła do Natalii, do szpitala. Chciała poznać jej wersję. Natalia nie chciała powiedzieć, bo przecież Sabina powiedziała, ale Pięknotka nalega. Natalia powiedziała, że ona chciała porozmawiać z Sabiną, sprawa się zaogniła, jakiś lokalny mag zaczął się rzucać i Sabina go usadziła. Nie powinna była, bo nie uwzględniła kontekstu kulturowego - nie jest w Aurum. Pięknotka ma wszystko co potrzebuje, pro forma.

Czas na Myrczka. Pięknotka powiedziała mu, żeby zostawił Sabinę w spokoju. Myrczek zrobił potężną pasję, że Sabina jest niezrozumiana i można ją uratować. Może pochodzi do szkoły? Pięknotka się nie zgodziła i kazała mu Sabinę zostawić. A potem skontaktowała się z Teresą Mieralit, nauczycielką z Akademii Magii. Powiedziała jej o tym jak to szlachetny Ignacy Myrczek znalazł dobro w Sabinie Kazitan i obie kobiety pośmiały się z Myrczka. Teresa obiecała, że zajmie się tym i Myrczek nie będzie miał na Sabinę czasu.

Pięknotka wychodząc ze szpitala zderzyła się jeszcze z Aliną Anakondą. Ta zwróciła się do Pięknotki. Alina jest wściekła - Natalia chce przenieść swojego brata do Aurum. Alina uważa, że to jest głupie, bo nie da się porównać klasy Szpitala Terminuskiego z jakąś prowincją Aurum. Plus, no nie ma opcji że Natalia ściągnie kogoś klasy Blakenbauera czy Anakondy. To jest "prowincja na forpoczcie". Zamiast kłócić się z Natalią, Pięknotka poprosiła Laurę o wsparcie - niech Laura znajdzie kruczki prawne i opóźnienia. Laura się uśmiechnęła, z przyjemnością wykorzysta swoje talenty do uratowania życia. (ExZ+3)

* V: Natalia nie jest w stanie przesunąć brata przez następne dwa tygodnie. To wystarczy na pierwszą stabilizację i regenerację.
* V: Laura poszła głębiej. Pokazała WSZYSTKIM - tu jest prawo. Tu jest porządek. Tu, w Pustogorze, nikt nie może "robić widzimisię", nawet arystokracja. I nawet nie próbuj odbijać...
* X: Natalia jest wroga Pustogorowi. Bo chcą jej brata krzywdzić i ją ciemiężyć.
* X: Tukan będzie niezadowolony z Laury, bo podpadła ZARÓWNO Aurum JAK I Kalliście...

Pięknotka chciała sama poszukać papierów. Ale nie ma siły. Poprosiła Trzewnia o pomoc. Plik papierów i butelka wina... Trzewń pomoże Pięknotce... ale to wymaga dobrej kolacji u Olafa, w Górskiej Szalupie... (TrZ+2)

* XXXX: Przesiedzieli bardzo dużo czasu - dwa dni. Namęczyli się solidnie. Ale okazuje się, że jakiekolwiek dane Gabriel zostawił, zostawił je po prostu ŹLE. Przecenił ich...
* V: ALE Trzewń znalazł coś istotnego - Gabrielowi nie podobały się sygnatury pola magicznego w okolicach fortifarmy. Coś mu nie pasowało, więc wziął awiana i tam poleciał.

Pięknotka wie, że Gabriel wziął awiana. Poszła do kogoś, kto NIE jest kwatermistrzem. Poprosiła o zapis logów - gdzie awian latał. Jako, że Pustogor jest strasznie biurokratyczny, to Pięknotka wie który to awian. Na widok dekoltu Pięknotki młodzian podał terminusce zapis logów awiana - awian wylądował niedaleko fortifarmy. Tam będą jakieś informacje.

Pięknotka potrzebuje katalisty. Na szczęście, znalazła jednego. Tymon jest znowu w Zaczęstwie...

Tymon wypytał Pięknotkę o rany Gabriela. Okazuje się, że najpewniej był bez power suita. Zdaniem Tymona to znaczy że nie szukał aury - poszedł na randkę. Nie ma powodu, by mag poszedł na niebezpieczny teren bez power suita.

Zgodnie z danymi z raportów Gabriel MIAŁ power suit; ale ten power suit nie został użyty. Czyli ktoś zaatakował go za szybko?

Pięknotka i Tymon polecieli awianem w okolice gdzie Gabriel dostał wpiernicz. Niedaleko fortifarmy. Pięknotka skupiła energię i zaczęła magicznie szukać, gdzie Gabriel dostał wpiernicz. Jako, że była otwarta krew i ona ma Gabrielową krew, jest w stanie dość bezpiecznie to zrobić. (TrMZ+3e)

* XXX: Energia Skażenia, fantomy Studni Irrydiańskiej... krzywdzą Pięknotkę. Ona szukając asymiluje tą energię i jest chorsza.
* V: Ale znalazła miejsce.
* V: I wyczuła inne zapachy. Inne osoby. Jeden z nich jest całkowicie nowy i nienaturalny. Ten typ zapachu Pięknotka czuła dwa razy - gdy miała do czynienia z Emulatorką.
* V: A drugi zapach który poznaje... kojarzy jej się z Natalią Tessalon.

To by też tłumaczyło, czemu Natalia odwiedziła go w szpitalu. Żeby się upewnić, że nic mu się nie stało... to niestety co ma Pięknotka nie tylko nie nadaje się na żadną formę dowodu ale też co gorsza jest słabe.

Tymon mówi, że aura jest lekko zdestabilizowana. Coś tu jest nie tak w tej okolicy. Subtelnie nie tak. Tak więc Pięknotka poszła z Tymonem na fortifarmę i poprosiła Kołczonda, by ten udostępnił Tymonowi sensory. Pięknotka uzasadniła tym, że terminus w pobliżu został ciężko pobity. Kołczond się oczywiście zgodził. (ExZ+2)

* V: Tymon odkrył, że sensory na fortifarmie są skalibrowane by niektórych rzeczy NIE widzieć. I to znalazł Gabriel
* V: Tymon odkrył, że gdzieś w okolicy Studni Irrydiańskiej znajduje się ukryta, zamaskowana baza.
* X: ONI wiedzą, że ktoś tu kombinował i grzebał. Że ktoś szukał bazy.

Gdy Pięknotka była w drodze na Trzęsawisko, dostała wiadomość od Tymona. Natychmiast Pięknotka nie jedzie na Trzęsawisko; to nie ma sensu. Obejrzała awianem widok z powietrza. Niestety, żadna baza nie została odkryta ani możliwa do łatwego odkrycia.

Pięknotka ma plan. Tymon w pełni zahermetyzowanym entropiku i Pięknotka. Tymon na low powered broadcast: "wiemy że tu jesteście, wyjdź, Emulatorko, albo Cię znajdę". Po 5 minutach faktycznie na otwartą przestrzeń wyszła Emulatorka.

Emulatorka powiedziała, że jej celem nie jest robienie nikomu krzywdy. Dyskutowała z Pięknotką i wyjaśniła, że nazywa się Kallista. Jest "wolną Emulatorką", z innej frakcji Orbitera. Czemu pomaga Natalii? To daje jej wejście w Aurum. Nie powie czym zajmuje się ta baza, ale jest tu od lat. Pięknotka widzi sytuację, nie chce faktycznie walczyć przeciwko Orbiterowi - zwłaszcza, że Kallista wie o Cieniu.

Chwilowo się zgodziły na to, że Pięknotka odsunie się od tematu bazy. Ale Pięknotka nie powiedziała, że uszkodziła plan Kallisty - Laura zajmie się tym, żeby szybko tereniu nie opuścił Tadeusz; to byłoby lekko niefortunne ;-). No i Pięknotka myśli co dalej z tym robić:

* jakie są relacje Kołczonda i Kallisty - wiedzą o sobie?
* czemu Orbiter ma tu bazę i czemu to baza silna medycznie? Czego szukają?
* czy Natalia jest w stanie pomóc Tadeuszowi bardziej?

No i najważniejsze - czy/co powiedzieć Karli?

## Streszczenie

Natalia Tessalon chce przetransportować rannego brata do Aurum, ale manewr zablokowała Laura (terminuska-prawniczka) na prośbę Pięknotki. Natalia i Sabina się ścięły, przez co ucierpiał Myrczek a Sabina trafiła do aresztu. Teresa Mieralit zapewni, że Myrczek nie ma głowy do Sabiny. Pięknotka z Tymonem poszukali kto ciężko zmiażdżył Gabriela; udało się im znaleźć tajną bazę Orbitera w której jest Emulatorka, która się do tego przyznała.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: katalizator: wyłączyła relację Myrczek - Kazitan, odnalazła tajną bazę Irrydius przy użyciu Tymona oraz porozmawiała z tamtejszą Emulatorką.
* Gabriel Ursus: zauważył różnice między aurami, spotkał się z Natalią przy Fortifarmie Irrydii i został sklupany przez Kallistę. Stracił o tym pamięć i skończył w Szpitalu Terminuskim.
* Laura Tesinik: miała okazję użyć swoich zaawansowanych skilli prawnych by stawić czoło Aurum i WYGRAĆ - nie przeniosą Tadeusza. Prawo jest jej bronią. Pomogła Pięknotce :D.
* Natalia Tessalon: zrozpaczona przez stan brata; nawiązała kontakt z bazą Irrydius i postanowiła przenieść brata do Aurum. Wini Sabinę oraz jest wroga Szczelińcowi za wszystko.
* Sabina Kazitan: skonfliktowana arystokratka Aurum. Skonfrontowana z Natalią przyjmowała na chłodno aż Myrczek się wtrącił. By go chronić przed Natalią, zraniła go. Aresztowana.
* Ignacy Myrczek: stanął w obronie Sabiny przed Natalią, za co Sabina walnęła mu szpicrutą w twarz. Wyraźnie się w Sabinie podkochuje i chce ją "uratować od jej złej natury".
* Teresa Mieralit: poproszona przez Pięknotkę, by skupić się na Myrczku. On ma trochę za dużo czasu i podkochuje się w Sabinie Kazitan, co do niczego nie prowadzi. Obiecała, że go od niej odsunie.
* Alina Anakonda: główny lekarz w Szpitalu Terminuskim (odkąd zabrakło Blakenbauera); nie zgadza się, by Tadeusz Tessalon opuścił szpital, bo to mu zagrozi. Włączyła w akcję Pięknotkę.
* Mariusz Trzewń: przegrzebywał się przez dokumenty Gabriela, gdzie był ślad dla Pięknotki. Gabriel zdecydowanie ich przecenił - nic nie znalazł. Ale pomógł nakierować na awiana.
* Tymon Grubosz: tym razem jego groźne oblicze, kataliza i technomancja się przydały - wykrył, że czujniki na autofarmie Kołczonda mają NIE znaleźć bazy w Studni Irrydiańskiej.
* Kallista Exolon: emulatorka Grzymościa. Przekonana że jest wolną emulatorką Orbitera z frakcji Orbitera a nie Grzymościa (tymczasowe działanie Talaranda) załatwiła z Pięknotką, że ta zostawi ich bazę w spokoju, przynajmniej na razie.
* Talarand d'Irrydius: BIA wspomagająca Grzymościa, jego specjalnej bazy. Koordynator Kallisty; tym razem załadował Kallistę w wiedzę, że jest agentką Orbitera a nie Grzymościa na negocjacje z Pięknotką.
* Artur Kołczond: ma jakieś relacje z Bazą Irrydius? Pomaga terminusom i udostępnia im swoje sensory i systemy, ale po której naprawdę jest stronie?

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Czemerta, okolice
                                1. Fortifarma Irrydia: specjalnie ma tak skonfigurowane sensory, by nie dało się znaleźć Bazy Irrydius.
                                1. Studnia Irrydiańska: znajduje się gdzieś w okolicy czyjaś baza; jest specjalnie maskowana.
                                1. Baza Irrydius: operowana przez Talaranda i Kallistę; została wykryta przypadkiem przez Tymona.
                            1. Pustogor
                                1. Rdzeń
                                    1. Szpital Terminuski: wpierw Natalia Tessalon odwiedziła tam brata i Gabriela, potem Sabina Kazitan zraniła tam Myrczka. Miejsce DRAMY.
                                1. Eksterior
                                    1. Miasteczko
                                        1. Knajpa Górska Szalupa: najbezpieczniejsze miejsce spotkań z Pięknotką jakie zna Sabina Kazitan (nie wie o gabinecie Pięknotki)
                                

## Czas

* Opóźnienie: 3
* Dni: 4
