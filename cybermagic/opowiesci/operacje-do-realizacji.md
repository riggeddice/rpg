# Operacje

===========================================================================

## Planetoida Kabanek, kontynuacja
### Pełny motive-split

* the-inquisitor: Gabriel Lodowiec, wchodzi w głąb intrygi przy Kabanku szukając anomalnych krzyżówek, noktiańskich spisków i walczący, by Orbiter dalej miał możliwości działania.
* the-facade: Tadeus Melrok próbuje ukryć to, że stopniowo przejmuje planetoidę współpracując z Syndykatem; nie jest fanem Orbitera. Pokazuje się jako dobrotliwy bogaty dżentelmen.
* the-doomed: Laudus Kronitor pozyskuje ludzi, by stworzyć imperium 'biorobotów'. Kronitor zawsze zatrudnia. Zawsze dobrze płaci.
* anomalia-oswojona: tak Kronitor myśli, że znaleziona przez niego Anomalia robi bezpiecznych ludzi. Nie robi. Na to właśnie poluje Gabriel Lodowiec.
* dark-technology: wykorzystywane w tym terenie byty i jednostki anomalne, które nie skończą się dobrze, jak np. korweta Wolrath, dowodzona przez zdesperowanych anty-Kronitorowców.
* corrupting-mentor: w pewien sposób Warząkiewicz (lekarz, marine) wobec młodego kapitana, ucząc go, by ten dobrze bawił się życiem.
* energia-praecis: wtłoczenie człowieka do niewłaściwego schematu by ciężko pracował; "anomalia oswojona". To jest Twoje miejsce w egzystencji, pełne zniewolenie.
* kocenie-slabszych: Warząkiewicz tak traktuje advancerkę noktiańską, bawi się jej cierpieniem bo naprawdę nienawidzi noktian.
* szkolenie-dzieciaka: w pewien sposób zarządzanie dowódcą Tezremont (Pawilończykem) przez Zespół
* zasoby-ograniczone: poważny problem, przed którym stoi Lodowiec. Potrzebuje sprzętu i jednostki wsparcia, nie ma nic.

### Frakcje Kabanka

* KaRePin: 
* Syndykat Aureliona: 
* Orbiter: 
* Listar: 
* Dolorius: 
* Fretyści: 


* Kamil Drobiakis
    * Zapisano w Gwiazdach: będzie walczył o to, o co należy walczyć. Będzie dzielnie wspierał.
    * BARDZO duże poczucie sprawiedliwości.
* Marek Osadowski
    * Pionier: Uwielbia pracować w trudnych warunkach, jest specjalistą od naprawiania wszystkiego przy minimalnych zasobach. Ma nieustępliwą determinację.
    * Dotknięty Lacrimor: Przygnieciony brutalnością przestrzeni kosmicznej, zaczyna poszukiwać ekstremalnych, nielegalnych środków, by przetrwać.
* Iwan Beresin
    * Opiekuńczy Mentor: Zawsze służy radą i wsparciem młodszym członkom załogi, zapewniając im spokojne miejsce do rozwoju.
* Julia Worokiewicz
    * Optymistka w Koszmarze: Szuka pozytywów nawet w najgorszych sytuacjach, próbując odnaleźć piękno i sens.
* Emilia Stawicka
    * Po przegranym wyroku, jej dusza zgasła. Chłodna i odpycha wszystkich.
* Adam Trybicki
    * Fatalista: Przekonany, że przeznaczenie jest nieuniknione.


### Kompletna Wizja tej Sesji

* PIOSENKA WIODĄCA: 
    * Inspiracje
        * "Osiemnaście Oczu"
        * "Gdy Rzeczywistość Pękła, Brama Lirańska stała się Anomalią Kolapsu"
        * Ofiary z ludzi, tu: z dziewczyn, by z anomalii Unumens uzyskać Tancerki i Pracowników.    
    * [Narcotic Thrust - "I like it"](https://www.youtube.com/watch?v=kq1iZ9cWeAI)
        * "I hate the treadmill everyday | I hate the mundane things they say | The boredom sets in nine to five | At night that's when I come alive"
        * "I like it when we go to extremes | I like it when we enter my dreams | I like it when I feel your touch | I like it so much"
        * Bardziej ambitne osoby w okolicach Anomalii Kolapsu chcą korzystać z okazji, chcą korzystać z życia.
        * Ludzie jako zasoby i marzenia ludzkie. Ludzkie cierpienie. Ludzka ambicja. Czasem białkowe ciała są najlepszą opcją.
* Opowieść o (Theme and vision):
    * "Ambitni ludzie pozyskują pieniądze gdzie się da i jak się da, często używając 'zasobów ludzkich' - jedynych, które mają".
        * Natalia dołączyła do sił Orbitera, bo nie widziała lepszej nadziei dla siebie. Nie chciała zostać Tancereczką. A nie wierzy, że Orbiter coś z tym zrobi.
        * SC Patriaklest jest jednostką, która wie jak dostać się koło Anomalnych Korwet w Anomalii, by dotrzeć do miejsca Transformacji.
        * Ludzie często sprzedają dziewczyny, bo można z nich zdobyć akceptowalne pieniądze. Zwłaszcza, jak się jakieś znajdzie. Zwłaszcza noktianki.
    * "Ambitni ludzie o dobrym sercu często dużo ryzykują"
        * SC Woldran jest korwetą, która próbuje uratować dziewczyny - tam trafiła siostra właściciela Woldran.
        * Porucznik Pawilończyk robi ryzykowne ruchy, których nie powinien robić. Lodowiec mu nie pozwolił.
    * Lokalizacja: Okolice Anomalii Kolapsu, Libracja Lirańska.
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * the-inquisitor: Gabriel Lodowiec, wchodzi w głąb intrygi przy Kabanku szukając anomalnych krzyżówek, noktiańskich spisków i walczący, by Orbiter dalej miał możliwości działania.
    * energia-praecis: wtłoczenie człowieka do niewłaściwego schematu by ciężko pracował; "anomalia oswojona". To jest Twoje miejsce w egzystencji, pełne zniewolenie.
    * zasoby-ludzkie-przeksztalcone: wykorzystywanie ludzi przez anomalizowanie ich, transformacja ludzi w 'bioroboty', co wyskoczyło przy górnikach na planetoidzie LLP-112.
    * bunt-maszyn: zanomalizowani ludzie (bardziej golemy) zaczęli niszczyć wszystko dookoła.
    * zasoby-ograniczone: poważny problem, przed którym stoi Lodowiec. Potrzebuje sprzętu i jednostki wsparcia, nie ma nic.
    * starcia-kultur: pomiędzy praktycznie wojskowym OO Geisterjager Lodowca i luźnym acz wciąż kompetentnym OO Tezremont Pawilończyka.
* O co grają Gracze?
    * Sukces:
        * Uratować dziewczyny z "Obfitego Wieprza".
        * Zatrzymać anomalną korwetę Wolrath, zanim ta skrzywdzi Tezremont.
    * Porażka: 
        * Poważne uszkodzenia Tezremont
        * Krzywda Natalii
* O co gra MG?
    * Highlevel
        * Setting jest "CHARACTER-FIRST". Celem jest pokazanie kilku ciekawych postaci i lokalizacji.
        * PRIMARY: chcę, by zapoznali się z lokacją, z kilkoma postaciami i zrozumieli Anomalie oraz ten teren.
        * Cel: krótka sesja, w wyniku której gracze zapoznają się z postaciami, miejscem itp.
        * Czyli ta sesja to SCENA ZERO, ale szersza.
    * Co uzyskać fabularnie
        * Lodowiec OPIERDALA Pawilończyka w kosmos i jeszcze dalej
        * Pawilończyk oddala się na bok, do Illuminy Dorsz
        * Zespół zapoznaje się z pragnieniem posiadania stabilnych, bezpiecznych, "anomalnych" pracowników (wstęp do Aureliona)
        * Zespół widzi stosunek do Natalii i do innych noktian
* Agendy
    * Gabriel Lodowiec (the-inquisitor)
        * wyplenić negatywne problemy z anomaliami i innymi siłami na tym terenie
    * Tadeus Melrok (the-facade)
        * corrupted by Aurelion, przejął dowodzenie (nieoficjalnie) nad Kabankiem
        * magnanimous, daje więcej igrzysk i radości ludziom
    * Laudus Kronitor (the-doomed)
        * Anomalia ma w nim swoje szpony; nie zgodzi się na to, by umrzeć. Karmi ją ludźmi.
        * Noktianin, który stwierdził, że idzie swoją drogą
* Default Future
    * Wolrath zostaje zniszczona, ale Tezremont jest bardzo uszkodzony
    * Dziewczyny giną
    * Natalia jest obwiniona o większość rzeczy
* Dilemma: "Czy posuwamy się za daleko? Czy Lodowiec powinien iść za wielorybem?"
* Crisis source: 
    * HEART: "."
    * VISIBLE: "."
* Emocje, w które celujemy
    * Część 1: ""
        * ?
    * Część 2: "Ekspansja"
        * ?
    * Część 3: "Rozwiązanie"
        * ?


| Kto                 | 1 (s0 + 20 min)   | 2 (s0 + 40 min)            | 3 (s0 + 60 min)         | 4 (s0 + 80 min)  |
|---------------------|-------------------|----------------------------|-------------------------|------------------|
| **Mimik, Ralena**   | sama słodycz (drenuje ojca) | poluje na inżyniera | robi "nieudany atak" (wina) | atakuje na serio |
| **Kultysta**        | monitoruje Mimika | wystawia Mawirowców        | zbiera środki do Rytuału | robi Rytuał      |
| **Mawir**           | nic               | terroryzowanie Mawirowców  | poszukiwanie prawdy     | atak na Ralenę   |
| **Ojciec, Serven**  | nic               | obrona Raleny              | ukrywanie śladów        | zagrożenie Alissie |
| **Macocha, Alissa** | nic               | prawda do Kalisty          | prawda do Mawira        | nic              |


Faza 2: Planetoida Kabanek

* WHERE SUPPLIES oraz rzeczy mapujące na przemyt ludzi
* Illumina i kapitan
* Warząkiewicz opowiada o złej noktiańsce i Lodowiec jako LODOWA DUPA
* SIGNAL: natychmiast dowiedzcie się o Obfitym Wieprzu


===========================================================================
===========================================================================















## SESJE

### 1. Vespiravit - plany Samszarów do stworzenia revenanta. "The ghost fights again"

* MG
    * Stworzyć strasznego revenanta
* Drużynowy
    * Uratować porwanych ludzi
    * "Nigdy więcej oni nie skrzywdzą naszych"

### Co się stało i co wiemy

* Mała podgrupka Boronitów napadła na niewielką osadę, zabijając kilkanaście osób i porywając kilkanaście innych
* tien Samszar reanimował Upiora, będącego syntezą zmarłych oraz bohaterki rodu Samszar z czasów wojny
    * Program 'Vespiravit'




## Sceny

### Wpływ Arianny na Arkadię Verlen (okolice 211117-porwany-trismegistos )	<-- DONE 50%

2 tygodnie po  :

* Arkadia Verlen pisze list od fanów do Izabeli Zarantel "mniej Eustachego, mniej Klaudii, więcej Arianny"
* Arkadia największa fanka Sekretów Orbitera i wierzy w to co tam jest XD

...

* Fanfiki, kiepskie, z Arianną jako Mary Sue. Pisze je Arkadia.

### Smutny sojusz Loreny i Torszeckiego. I ochrona Arkadii

* Lorena jest sama, słaba, połamana i nie ma przyjaciół
    * acz odwiedzi ją Daniel Terienak z sympatii; jego jednak Lorena odepchnie
* Na Torszeckiego polują
* I będzie ich chronić... **Arkadia**

### Karolina słyszy o ścigaczu?

* Eksperymentalny ścigacz z Aurum z inną formą neurosprzężenia przez sentisieć.
* Neurosprzęg chce zdominować maga. Jest strasznie męczący fizycznie i psychicznie.

### Kampania „Wyleczone Czółenko” (dla Kić)

1.	Potwór Esuriit w przeszłości - coś strasznego, sesja dla terminusow.	<-- irrelevant
2.	Próba wyleczenia Czółenka przez 2-3 magów z Eterni. Porażka. Trzech Skażonych magów. Dyrektor szkoły umiera chroniąc dzieciaki przed jednym z dzieciaków. 		: DONE
3.	Aktualna chronologia
	1.	Trzeba odbić Alicje z Pustogoru i ja schować w Czolenku
	2.	Trzeba pozyskać Jolantę Kopiec i dać jej info ze tu coś sie działo
	3.	Trzeba zbudować link z Trzesawiskiem
	4.	Rytuał: Jolanta, transfer Ernest, Amanda, Alicja —- Pieknotka
        1.	Przy użyciu Saitaera, który jest Władca Adaptacji
	    2.	Zrobić „pole neutralizacyjne dwustronne”, zlinkować Saitaera z Czółenkiem przez Trzęsawisko 
	    3.	Zatrzymać / odciągnąć uwagę terminusow
	    4.	Pieknotka TRZYMA Saitaera
	    5.	Lucjusz ratuje tamta trójkę 

### Okolice Anomalia Kosmiczna Infernia

* ( Tivr ma zniszczonego miragenta na pokładzie. Pokłosie Sowińska - Morlan. Wygląda jak martwa Arianna. )	
* Elena chce wziąć Infernię i polecieć sprawdzić coś dziwnego co wykryła na poligonie. Nie chce być z innymi.           WONTDO
* Tymczasowa załoga Inferni - przemówić do nich
* Stocznia chce wsparcia Klaudii i Eustachego. Mają różne prototypowe jednostki.
* Hestia Stoczni wspiera napastników.
* Siły z Neikatis chcą pozyskać prototypy. Program kosmiczny Neikatis. Że z Plugawego Jaszczura niby.

* Starcia między "kultystami", "noktianami" i miłośnikami lolitek.


* POTENCJALNIE
    * Atak na Stocznię Neotik.
    * Siły z Neikatis chcą pozyskać prototypy. Program kosmiczny Neikatis. Że z Plugawego Jaszczura niby.
    * Elena obejmuje drugą Nereidę, za radą Adama.

* Mechanicy, jeden z nich w formie ixiońskiej
* "Mamy ixiońską anomalię kosmiczną. Szept Saitaera!"
    * kultyści Saitaera -> Infernia?
* Kult Arianny na pokładzie Inferni vs pozostali.
    * Kamil aktywnie proponuje, by znaleźć "zagubione dusze" i ich skonwertować na Infernię
    * "Nikt nie opuści Inferni"
        * Noktianie chcą na Tivr, część noktian pro-Infernia.
* Ixioński terrorform, złożony z Koruptora Ixiońskiego przez Infernię
* Koloidowe jednostki - Elena chce ich odszukać Tivrem? Ale nie prowokować potwora?

### Anadia, na 211221

* Chevaleresse, dziewczyny, autodefensywy.
* Santino ma swój "shock podcast".
* Alan chce to rozwalić.

Czyli do problemów z:

* Torszeckim
* Ernest - Rekiny
* Justynian "pragnący" władzy
* Myrczek x Sabina

dochodzą Marysi jeszcze problemy:

* Kult Ośmiornicy x Santino Mysiokornik
* Chevaleresse? (ale na pewno ród Arieników i Rekiny x Arieniki)
* Oficjalna Administratorka Aurum i jedna niesforna Hestia
