## Wyjaśnienie

Ustalenie faktycznej chronologii Arianny z datami i wszystkim.

## Dotyczy

* Postacie
    * Arianna Verlen, Klaudia Stryk, Eustachy Korkoran
    * Elena Verlen
* Lokalizacja
    * Verlenland
    * Kosmos
* Frakcje
    * Mafia
    * Serafina
    * Rekiny
    * Terminusi / Pustogor

## Sesje, wątki, linie chronologiczne

* ?

## Analiza

Arianna zaczyna jako arystokratka w Verlenlandzie, mamy informacje do momentu kryzysu z Eleną.

* Sentiobsesja, Studenci u Verlenów, Lustrzane odbicie Eleny [16, 20, 20] (0092, 0095-08-05 (lub 97))

Przechodzimy na chronologię KLAUDII i MARTYNA:

* Chory piesek na statku Luxuritias  <-- tu Klaudia jest poza Infernią, jest z Martynem na Serbiniusie

ANALIZA ARIANNY:

* Nocna Krypta i Bohaterka <-- pierwszy raz jak o Ariannie WIEMY. Jest na Perle Nadziei (statek arystokratki z Aurum)
    * czy to jej pierwsza jednostka?
    * czy to jej przydzielona / z Aurum?
    * przed tą sesją miała inne udane sesje na Perle. Bo jest BOHATERKĄ. Czyli wcześniej była BOHATERKĄ.
        * co zrobiła?


Arianna
    Dziecko
    Arystokratka Verlenów               wiemy ma 20 lat na końcu
    ?
    Trafia na Orbiter                                   23 lata?
        pierwszy beznadziejny statek
    ?
    Zostaje Bohaterką ???                               27 lat
        * "nie tylko skuteczna i ma zwycięstwa i coś dużego"
        * "ale i 'Aurum can into space'"
    ?
    _Chory piesek na statku Luxuritias_
    **Infernia i Martyn Hiwasser**                                          <-- przestrukturyzować
        * Arianna pomaga Martynowi więc zaskarbia sympatię Klaudii
        * Ale to nie jest Infernia
        * Ale wtedy potem zmieniamy Janet Ewron na Ariannę
        * v2: czyli zamiast tej sesji potrzebujemy sesji gdzie Arianna pomaga Martynowi.
    ?
    Nocna Krypta i Bohaterka                            30 lat
        * 'Skażenie' Arianny i doprowadziła do Dyshonoru
    ?
    Niesława / Błąd / Dyshonor                          30
    ?
    Klaudia wyciąga Ariannę z niesławy przez Kamila     31
    ?
    Infernia i nowy początek                            31
    ?
    ...
    Teraz - anomalna Infernia           Arianna ma 33-34 lata





PROBLEMATYCZNA SESJA: Infernia i Martyn



