## Metadane

* title: "Ostatnia misja Tarna"
* threads: nemesis-pieknotki
* gm: żółw
* players: onyks, olo, tomek_taktyk

## Kontynuacja
### Kampanijna

* [190827 - Rozpaczliwe ratowanie Bii](190827-rozpaczliwe-ratowanie-bii)

### Chronologiczna

* [190827 - Rozpaczliwe ratowanie Bii](190827-rozpaczliwe-ratowanie-bii)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

brak

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

* Wiktor poproszony przez Pięknotkę ma zniszczyć Tarna.
* Talia wzięła Tarna do Kompleksu Tiamenat; tam siły Kajrata mają duże wpływy.

## Misja właściwa - opis SESJI PRZY STOLE

### Wstęp / kontekst

To była bardzo interesująca sesja, zdecydowanie warta zrobienia. Miałem trzy osoby - gdyby było pięć, sesja by się nie udała. Pojawiło się kilka problemów i kilka zaskakujących pozytywnych efektów ubocznych; spróbuję wszystkie pokazać.

Zacznijmy od tego czemu ta sesja jest trudna - ma następujące wymagania:

Kontekst świata (tylko to co jest niezbędne):

* Gracze muszą rozumieć, że Astoria i Noctis są w stanie zimnej wojny. Tarn, którym grają, jest uważany za wroga prawie wszystkich.
* Gracze muszą rozumieć, że Tarna się wszyscy boją - jest przepakowany (master hacker, master tactician)
* Gracze muszą rozumieć relację między Biologicznymi AI (Tarn) a Technicznymi AI (Hestia) - i co z tego wynika
* Gracze muszą rozumieć, że Tarn może stać się syntianem (jeszcze bardziej przepakowana istota), ale traci osobowość i staje się potworem

Cholera, ludzie którzy ze mną grali w EZ (Maciek, Karol, Vizz, Voldy, Gosia) - nie mają dość tego kontekstu by podejmować decyzje w tej sesji w chwili obecnej. A co mówić o nowych?

Ludzkie / motywacyjne:

* Gracze **muszą chcieć** pomóc Talii jako Tarn
* Grupa osób gra **jedną** postacią, która jest przepakowanym mózgiem w słoiku.
* Gracze grając "bojowym mózgiem inwazyjnym z Noctis" muszą chcieć nie zrobić krzywdy nikomu z Astorii - nikomu, kto chce ich zniszczyć.
* Gracze już na starcie wiedzą, że Tarn umrze. Tak więc grają o uratowanie Talii ("lol po co") lub o abstrakcyjne "niech Astoria zobaczy że BIA może być dobra" ("lol, zróbmy OP bekę")

Techniczne:

* Jest to bardzo, bardzo szybka sesja akcji o ostrych torach i licznikach w nieznanej im mechanice intencyjnej / z żetonami
* Jest to sesja która wygląda na IDEALNIE zaprojektowaną na bekę
* Jest to sesja z dwoma godzinami nudnej ekspozycji

Innymi słowy, wyzwanie jak cholera...

### Kto przyszedł

Dostałem trzy osoby

* Olo (grał ze mną 1-2 razy), nie zna kontekstu Astoria/Noctis ale zna kontekst Inwazji (zawsze coś). Zna mechanikę.
* Krystyna (grała ze mną 10+ razy), nie zna dość tego kontekstu i nie lubi sesji akcji (oczywiście), ale zna mechanikę.
* Tomek (nie grał ze mną), przyszedł specjalnie zagrać w EZ, bo ostatnio grał z Pawłem Figlem i mu się bardzo podobało. Kocha sesje taktyczne, z tego co widzę.

Populacja jak na tak eksperymentalną sesję bardzo udana. Co prawda gracze nie grali nigdy ze sobą razem wcześniej, ale klasyfikuję ich wszystkich jako "kategoria A" - wiem, że będzie dobrze i będzie się działo. Acz próbka z perspektywy generalizacji wydarzeń z tej sesji jest skażona; to, co będę opisywał nie jest "zawsze tak jest i to jedyna prawda" a "w tym wypadku tak było bo gracze dopisali".

Tak na szybko? Sesja była jedną z moich lepszych, nie w kategorii "sesja z osobami nieznanymi" a "sesje w ogóle" z perspektywy pędu.

### Wstęp / wejście do sesji

Na początku zawsze są pewne problemy:

* jak podejść do mechaniki
* jak grać z innymi (spotlight itp) z tym MG
* jak zacząć "coś robić"
* świat i ekspozycja
* jak Maciek zauważył, gracze się boją "że coś spieprzą".

Ta sesja zaczęła się w taki sposób, że powiedziałem im o tym, że są na polu kukurydzy i szukają zakopanego statku kosmicznego pod ziemią - Olo wziął postać Talii (a raczej, dostał ją :p) a Tomek dostał postać Anonimowego Górnika. Wyjaśniłem że to jest tutorial mechaniki z zaskryptowanym końcem (Tarn jest wykopany) ale gramy o koszt dla Talii. Po zbudowaniu szybkiego kontekstu "Górnik wisi Talii, Talia chce wykopać Tarna" ruszyliśmy do sceny zero.

W wyniku - oni zaczęli mnie pytać o elementy świata; dowiedzieli się, że Tarn jest stworzony przez Talię, to jest jednostka biowojenna, że Górnik jest z Astorii, relacja Astoria - Noctis. Te wszystkie rzeczy wyszły z ich pytań, nie z mojej ekspozycji. Na bazie tego po 4 konfliktach wykopali Tarna. Pytam na końcu co zrobi Górnik (w końcu Astorianin) - Gracze ustalili, że spróbuje zniszczyć Tarna tu i teraz. Przypomniałem im o skrypcie - oni potem GRAJĄ Tarnem. Pokazałem, że Tarn uruchomił rozbity statek, Talia nie pozwoliła mu skrzywdzić Górnika i Tarn jej posłuchał. Ale Tarn jest ciężko ranny - to jest jedna z praprzyczyn tego że jest skazany na śmierć. Graczom się spodobała ta interpretacja. Powiedziałem, że to jest 3 lata temu.

Tu minęło 15-20 minut na całość. I mam pęd. WAŻNE: mimo, że mam 2 "postacie", trzy osoby dają pomysły.

Potem druga scena zero - tutaj Krystyna objęła Pedra, Olo dostał Talię a Tomek dostał Tarna. Tu moim celem jest im pokazanie jak nieprawdopodobnie Tarn jest przepakowany.

Umieściłem graczy pół rokiem przed "aktualną sytuacją", pod niewielkim miasteczkiem Jagnięcino. Pedro to jest mafiozo, kiedyś z floty inwazyjnej Noctis ale teraz w półświatku. Talia, Tarn i Pedro robią infiltrację na budynek kontrolowany przez mafię. Dziwne jest to, że Talia kazała zaatakować i zniszczyć BIA.

Podkreśliłem kontrast - ta sama Talia która ratowała każde życie teraz chce zniszczyć inne BIA.Spytany czemu, wyjaśniłem im Syntiana; relacja typu "przepakowany, asymiluje, ale już nie jest osobą - i Talia chce go zniszczyć". Drugi kontrast - Pedro z mafii atakuje mafię w ukryciu. Zapytany, wyjaśniłem ponownie - mafia próbowała wskrzesić znalezioną BIA i przez przypadek (używając adaptogenu) stworzyła syntiana. Po odpowiedzi na kilka pytań powiązanych z kontekstem i z taktyką gracze zdecydowali się na działanie.

W tym momencie spytałem Krystynę o jej akcję. Ona powiedziała, że nie rozumie w ogóle tej sesji. Po krótkiej rozmowie wszystkich graczy doszedłem do tego o co chodzi:

* Olo, Tomek się świetnie bawili - dla nich to była sesja taktyczna i akcji. Ekspozycja przez fakty, działanie i popyt - to było dla nich super. Poznawali świat przez działanie, tylko to co potrzebowali.
* Krystyna rozumiała konflikty, ale one zupełnie jej nie interesowały. Ona chciała konflikty dotyczące natury ludzkiej i psychiki; taktyka i "tło" jej nie interesowały bo nie było tam ludzi.

Wyjaśniłem w skrócie, że w pełnej sesji będzie już inaczej, ale najpierw potrzebne im są wszystkie składowe by móc ją rozpocząć. Inni gracze zupełnie nie rozumieli o co jej chodziło i w tym momencie uznali to za "ale o co jej chodzi" - szybko przedstawiłem tą scenę z perspektywy konfliktów międzyludzkich (i międzysłoikowych) tak by powiązać ich wszystkich w jeden Shared Imagined Space. Świetnym pretekstem dla mnie było pytanie Krystyny "ale czemu Talia po prostu nie zostawi tematu Władzom, po co ona w ten temat się pakuje i co tu robi Pedro".

* Władze i Mafia się raczej zwalczają. Innymi słowy, Mafia i Pedro nie mają powodu iść do Władz. Władze wykorzystają to jako pretekst.
* Talia i Tarn nie pójdą do Władz, bo Władze zniszczą Tarna (wyszło w scenie zero #1). Tak więc tu nic nie będzie.
* Talia nie pójdzie do Mafii, bo Mafia może uznać, że wykorzystają Syntiana jako "broń którą da się kontrolować".
* Tak więc pozostają tylko Tarn, Talia i Pedro.

Usatysfakcjonowana, acz z lekką rezygnacją, Krystyna poszła za sesją dalej. Zespół bardzo elegancko rozwiązał sprawę - Tomek sterujący Tarnem (ukrytym jako robotyczny, włochaty pies) chciał wpierw znaleźć w której stodole znajduje się syntian, ale konflikt im nieco nie wyszedł - syntian znalazł Tarna. Gracze odwrócili to jako "Tarn będzie dywersją", Tarn shackował jakąś strażnicę z dronami i pokazał syntianowi jaki jest groźny. Syntian chce zasymilować Tarna, wysłał tam wszystkie siły, Pedro się zakradł i używając neutralizatora Talii zniszczył to cholerstwo.

To powyżej wszystko było interpretacją oraz generacją scen wykonaną przez graczy. Przy okazji dodaliśmy następujące fakty do ekspozycji:

* Tarn był przez Talię budzony tylko w wypadkach tego typu; jest wychowany przez Talię do niezabijania.
* Tarn jest BIA lv3, syntian był lv1. Tarn robił kółeczka dookoła syntiana a syntian przejął kontrolę na 3 ludźmi, magiem i kilkunastoma kotami, psami i szczurami.
* Transformacja w syntiana jest nieodwracalna i potrzebny jest adaptogen kralotyczny by tą transformację dokonać.
* Tarn jest groźny jak cholera
* Syntian jest groźniejszy niż BIA, ale Tarn (klasa 3) jest silniejszy niż syntian klasy 1.
* Pedro jest skuteczny, dość lojalny i daje radę. Lubi Talię, ale już nie identyfikuje się z Noctis.
* Talia współpracuje z mafią, bo nie ma wyjścia by uratować Tarna.
* Pedro dostał powerup w mafii bo zrobił Udaną Akcję niezłej klasy.

Przy okazji doszło do innej zabawnej sytuacji. Olo chciał użyć Talii by się zakraść i uratować syntiana ("Talia ratuje wszelkie życie"). Przyciągnąłem informację z kontekstu i powiedziałem, że syntian jest nieodwracalny - to jest klątwa boga Saitaera. Jako, że Olo (i Krystyna) kojarzy ten temat, to mu to wystarczyło; wycofał akcję, bo Talia by tego nie zrobiła (jego słowa). Tomek po prostu założył to jako niezmiennik. W ten sposób ustawiliśmy wszystkie ważne fakty odnośnie sytuacji. I mogliśmy zacząć sesję właściwą.

Na wyjściu tej sceny nie stało się nic ciekawego.

Do tego momentu minęło 30-40 minut od rozpoczęcia sesji. Gracze (poza Krystyną) są zaangażowani w konflikty i postacie a Krystyna jest zainteresowana tematem, ale jeszcze trochę z boku.

### Sesja właściwa

W poprzedniej scenie mieli Tarna o niesamowitej mocy i sile. W tej zacząłem od czegoś pięknego.

"Jesteś w kompleksie do badań bytów niebezpiecznych. Niedawno przebudzono Cię, bo kilku pijanych mafiozów chciało posłuchać jakie dowcipy zna mózg w słoiku. Tym razem przebudziłeś się sam, z innego powodu - Twoje wewnętrzne czujniki wykryły, że w powietrzu jest skażenie biologiczne. Jakiś wirus czy coś. W pomieszczeniu oprócz Ciebie jest czterech pijanych, śpiących mafiozów. Jesteś mózgiem w słoiku na platformie i nic nie możesz."

Mina absolutnego kontrastowego upodlenia graczy była niesamowita. Przeskok od "Tarn niszczyciel światów" do "Tarn mózg w słoiku a inni pijani lub śpią" był silny. Tomek spytał, czy NIC nie da się zrobić? Powiedziałem, że niedaleko leży "łazik" Tarna (takie pajęcze nogi + manipulatory), ale w chwili obecnej podstawowy biovat Tarna ma tylko mikrofon.

Gracze od razu poszli za ciosem - opierniczyli jednego z tych przysypiających mafiozów. Zrobiłem show pijackim głosem, jak to "no ale nie wolno mi cię tu wsadzić na łazik bo tylko Talia może wsadzić Cię na łazik...". Porządnej klasy konflikt i zaraz Tarn jest na łaziku. I - zaskoczenie - Tomek od razu strzelił "dobra, hackuję ten komputer. To jest biolab, nie? To na pewno można odciąć tlen. Jakiś obieg zamknięty. Oczyścić powietrze!". A Krystyna: "co to jest za wirus? Trzeba go zbadać.".

Ze spokojem powiedziałem zanim zaczęli się kłócić o kolejność, że Tarn jest w stanie robić te dwie rzeczy jednocześnie - i wiele, wiele innych. Gracze zauważyli znowu jaki jest silny. Wpierw rozstrzygnąłem analizę wirusa - mutagenny, zmienia ludzi w potwory. A potem - odciąłem powietrze. Co prawda wirus to nie "gaz" i teoretycznie mutacja mogła iść dalej, ale poszedłem za fajnymi pomysłami graczy ;-).

Jednocześnie dało mi to pretekst do wprowadzenia TAI Hestia - AI Kompleksu Badań Niebezpiecznych Tiamenat. Ale - znowu grając na kompetencji Tarna - pokazałem, jak Tarn skutecznie porusza się w tych systemach i Hestia - STRAŻNICZA AI - nie ma pojęcia że intruz - niedostosowany do tych systemów - porusza się w nich skutecznie. Olo i Tomek byli bardzo zadowoleni z tej siły; Krystyna zainteresowała się Hestią i potencjałem na negocjacje i współpracę. Już wiedziałem, że będzie dobrze.

Przypomniałem graczom, że Tarn jest skazany na śmierć na końcu sesji. Ale - ALE - jest tu gdzieś Talia którą musi uratować. W tym miejscu gracze - wszyscy - wzięli to jako swój cel, tu nie było dyskusji. Byli strasznie zaangażowani w relację Tarn/Talia. Powiedziałem im też, że Tarn wie że wszyscy się go boją. Dla niego za późno, ale może się mu uda poprawić relację Astorian z wszystkimi istotami typu BIA. Czyli może pomóc Talii w przyszłości i innym istotom tej samej klasy. Zwłaszcza Krystyna przyjęła to za cel który trzeba zrobić, ale inni gracze też go wzięli jako godny dla siebie.

Chcę zaznaczyć - w tym momencie sesja już działa perfekcyjnie. Gracze grają mózgiem w słoiku, wczuli się zarówno w sytuację (zgodnie z założeniami) jak i w samego Tarna (niespodziewane O_o) - do tego stopnia, że nie chcieli zmieniać postaci na inną i zaczęli mówić "ok, to ja robię X! To znaczy, er, Tarn robi...". W pewnym momencie olali temat i zaczęli deklarować w pierwszej osobie, często budując krzyżowo na swoich pomysłach. Nawet nie musiałem kontrolować spotlighta.

Zanim minęła pierwsza godzina sesji, sytuacja wygląda tak:

* Wyłączając powietrze, budując w biolab antyciała i uniemożliwiając komukolwiek śmierć przejęli kontrolę nad pierwszym budynkiem (tym, gdzie są).
* Wykorzystują mafioza naukowca Eustachego (którego wymyślili) do gadania, bo wszyscy się boją Tarna
* Powiedziałem im o adaptogenie kralotycznym. Mimo, że brzmi to epicko jako powerup, nie chcą tej mocy bo Talia byłaby załamana a Tarn by tego nie zrobił poza ostatecznością (serio).
* Zorientowali się, że monitoring nie działa; tzn. pokazuje nieprawdę
* Skontaktowali się z Pedrem (na ich pytanie "gdzie jest Pedro" powiedziałem "w budynku gdzie Talia" - duża radość na twarzach graczy)

Komunikują się z Pedro a on jest zarażony więc pijany. Gracze cali niezadowoleni, i Tomek "ŻOŁNIERZU BACZNOŚĆ!". Podchwycili, zaczęli odwoływać się do jego przeszłości i zmusili go trudnym konfliktem do udania się do biolab gdzie jest Talia. Jednocześnie przesłali mu plany antidotum na wirusa (które Tarn opracował) by Talia mogła w biolabie coś z tym zrobić i ich wszystkich ponaprawiać.

Czyli - mamy 1h20 minut i gracze mają tzw. "dobrą pozycję". Stan zrobił się stabilny - Pedro i Talia są "w miarę bezpieczni", ufortyfikowani, sam Tarn jest bezpieczny i kontroluje jeden budynek. Czyli mamy wojnę pozycyjną. Czas to zmienić - mój ruch (kontratak).

**Chciałem** zobaczyć czy gracze użyją adaptogenu kralotycznego pod presją, więc Pedro zaraportował Tarnowi, że tamten budynek jest w pełni skażony - łącznie z life support (źródłem skażenia). Mają 40 minut systemu podtrzymywania życia w biolabie, Pedro, Talia i 2 osoby są uzbrojeni, ale skażeńcy w tamtym budynku są już potworami, są uzbrojeni i przygotowują się do zdobycia broni ciężkiej by złamać szczelność biolabu i zainfekować ich wszystkich.

Gdy gracze spróbowali połączyć się z drugim budynkiem i przejąć nad nim kontrolę, wykryła ich Hestia (konflikt). Natychmiast kaskadowo odciąłem im CAŁOŚĆ kontroli nad systemem; Hestia zaatakowała potężną baterią wirusów i memów by rozciąć Tarna, ale gracze powołali Talię jako wsparcie (psychotroniczka) (konflikt). Tak więc Tarnowi nic się nie stało, ale stracił kontrolę. Tak więc stan aktualny:

* Pedro, Talia itp są bardzo zagrożeni - zarówno czas jak i skażeńcy
* Tarn jest z 8-9 osobami w oczyszczonym przez siebie budynku, ale kontroluje go Hestia
* Hestia nie wie o zagrożeniu; uważa, że wszystko jest winą Tarna.

W tym momencie byłem pewny, że wezmą adaptogen. Tymczasem, Krystyna rozpoczęła negocjacje. Po skomplikowanej dyskusji o tym, czy AI w tym świecie są żywe czy zaprogramowane, poszerzyła się ekspozycja świata:

* AI mogą być typu 'subturing', 'superturing'. Te pierwsze to "general AI" bez osobowości, te drugie mają osobowość
* Talia walczy od dawna o prawa zarówno dla TAI jak i dla BIA, tych superturingowych; żadne nie mają praw cywilnych i są uważane za narzędzia
* Technicznie, mamy niewolnictwo AI ;-)
* Hestia jest superturingiem tak jak Tarn, ale Hestia to Typ 2 a Tarn to Typ 3. To jak rower vs ferrari.

To, co ważne - wszystko to co tu piszę **Gracze rozumieli i wykorzystywali jako modyfikatory konfliktów**. Tak więc ekspozycja przez akcję jest niesamowitym narzędziem.

Gracze próbowali shackować system, ale powiedziałem, że zajmie to ~30 minut dyskretnie - nie mają tyle. Jawnie 2 minuty, ale Hestia wyśle SOS do terminusów i ci wpadną i zrobią rąbanko biednemu Tarnowi i wszystkiemu tutaj. Zostaje przekonać Hestię.

Ale w jaki sposób przekonać przestraszoną TAI poziom 2, że BIA poziom 3 jest czymś czemu Hestia ma zaufać i wpuścić z powrotem w systemy?

Gracze wpadli na ciekawy pomysł:

* Pokazali Hestii to, co widzi Pedro kamerką
* Pokazali Hestii wynik badań nad wirusem i kontr-wirusem
* Powiedzieli Hestii, że ta może wysłać opóźnioną wiadomość do terminusów
* Powiedzieli Hestii, że jeśli jej monitoring nie działa to jej podstawowa funkcja - ochrona - jest narażona.

Hestia ich ostrzegła, że opóźniona wiadomość poszła po czym zgodziła się na wpuszczenie ich w systemy po przejrzeniu dowodów. Sama jest zaskoczona - jakim cudem byty biologiczne są w stanie wpłynąć na jej podsystemy technologiczne? Jeden z graczy zasugerował, że to kwestia potencjalnego skażonego hackera; oczywiście, poszedłem za tym.

Tak więc Tarn z pomocą Hestii zniszczyli ewentualne wirusy i hackmasterstwo które blokowało systemy. Odzyskali widzenie na wszystkie systemy. I pokazał się niefortunny obraz - grupy skażeńców, zagrożona Talia, jeden Stwór kontrolujący oraz coś co jest praktycznie niewidoczne i niewykrywalne; obserwator lub zabójca.

Gracze mieli ten sam pomysł - zdetoksyfikować skażeńców używając powietrza, ale nie ze mną te numery. Oni są w pełni skażeni - mają różne biotypy. Padają do nieprzytomności w różnych momentach, więc nie da się wszystkich odciąć i jednocześnie nikogo nie zabić. Gracze mają mały zgryz.

...przez chwilę. Za chwilę wykorzystują pomysły związane z grodziami i polami siłowymi. Hestia odwraca uwagę i wydaje dziwne dźwięki i sygnały rozpraszając grupę skażeńców i odcina grodzie, Tarn koordynuje działania Pedra by ten łapał nieprzytomnych skażeńców a Talia w stroju typu hazmat ma ich wyleczyć. Najlepsza biomantka i psychotroniczka w okolicy... całkowicie tego nie przewidziałem. Innymi słowy, zamiast przeprowadzać i eskortować Talię, gracze zbudowali dookoła niej bezpieczną przestrzeń i wrócili do koordynacji i kontroli terenu. Sesja trwa 2h15.

Gracze zaczęli się zastanawiać jak pozbyć się Stwora. Zastraszyli go; w końcu powstał z maga a Tarn pokazał mu co potrafi i że przejął kontrolę nad wszystkim. Tak bardzo bossfight :D. Gracze kazali mu się podpiąć do podsystemu i Tarn rozpoczął hackowanie stwora... krytyczna porażka.

Ooo, tak.

Mam swoją "śmierć Tarna". Pokazałem im połączenie z Trzęsawiskiem i powiedziałem o Wiktorze Satarailu, władcy Trzęsawiska. Władca bioform przygotował specjalną formę trucizny i Tarn został tym zaatakowany. (na tym etapie nie zauważyłem że w sumie nie wiem jak trucizna ma przejść przez barierę technologiczną, ale gracze w sumie też nie - wynik konfliktu był dla nich wystarczająco święty). Tarn ma dwa dni życia maksimum. Ale wynegocjowali możliwość porozmawiania z Wiktorem. Oczywiście, że jestem za - "villain cackle" jest czymś co zawsze chciałem zrobić.

Teraz - Krystyna kojarzy Wiktora. Pamięta, że jest eleganckim dżentelmenem, "jaszczur z bagien". Ale nie widziała go jeszcze od tej mroczniejszej strony, którą teraz mogłem użyć:

* Wiktor pogratulował Tarnowi, że ów przejął kontrolę nad kompleksem Tiamenat - wyraźnie jest tak groźny jak mu mówiono
* Wiktor powiedział, że Tarn został otruty bo pewna terminuska go o to poprosiła. I przyniosła dobre wino oraz ładnie poprosiła. (miny graczy... bezcenne)
    * (Ale jak to! Zabijasz mnie, bo... poprosiła?) (Hej, ma ładny uśmiech, sam rozumiesz... jesteś mózgiem w słoiku)
* Wiktor ogólnie rzecz biorąc potwierdził - cała akcja była tylko po to by zniszczyć Tarna. Na pytanie "a co jeśli syntian?" odpowiedział "i tak nie żyjesz, mi wystarczy."

Klincz. Nasza historia się pięknie kończy...

NIE! Gracze zaczynają negocjować niezmiennik.

Niezmiennik to niezmiennik, nie ulega negocjacjom. Nawet jeśli obiecują chipsy następnym razem (serio, to się stało). Ale zbudowali naprawdę piękną pozycję negocjacyjną:

* Tarn nikogo nie zabił. Próbował wszystkich uratować i się mu udało
* Tarn pokazał Wiktorowi Hestię - dogadał się nawet z TAI. Nie liczy się?
* Tarn chce tylko by wszyscy żyli bezpiecznie. Jest biowojennym mózgiem, ale przecież walczy o bezpieczeństwo dla wszystkich. Nigdy nic nikomu nie zrobił.
* Tarn się podda Wiktorowi, odda mu część wiedzy. Ale nie chce umierać, nie chce by Talia płakała.

I jestem w kropce. Zgodnie z motywacją Wiktora co najmniej część z tego powinno działać. Ale jestem spętany niezmiennikiem - Tarn jest martwy (wynik konfliktu innego zespołu graczy).

Stanęło na tym, że przeszedłem przez logikę. Celem zabicia Tarna jest usunięcie go z wszystkich sesji na Astorii - ale może da sie to zrobić inaczej. W końcu ustaliłem, że da się. Tarn po prostu musi iść w kosmos. Gdzieś poza Astorię. Wtedy zadziała - wszystkie konflikty są jednocześnie prawdziwe. "O mózgu w słoiku który poleciał na księżyc" i inne takie.

Gracze byli przeszczęśliwi i dumni z siebie. Sesja była bardzo szybka, wartka i miała sporo akcji, kombinowania i planowania.

### Co zadziałało

* Ogromna ilość ekspozycji wygenerowana przez akcję. Serio - OGROMNA. Ekspozycja przez akcję forever.
* Gracze mnie pytali o świat. To nie ja im robiłem ekspozycję, to oni mnie pytali.
* Myślałem, że powinienem był zadać pytanie czemu Górnik chce pomóc Talii (jak ona mu pomogła). Ale bez kontekstu w scenie zero by nie zadziałało.
* Gracze którzy na początku chcieli zabić Tarna "bo świat" pod koniec sesji walczyli o niego nawet chipsami
* Gracze wczuli się w Tarna
* Gracze byli zachwyceni możliwościami i potęgą Tarna. Mieli naprawdę potężną postać i to czuli - a jednocześnie czuli ból tego "leżę na platformie i mam tylko głośnik"
* Wszyscy gracze weszli w sesję. Przyjęli cele Tarna za swoje.
* Wszystkie tłumaczenia i trzymanie kontekstu było możliwe dzięki temu, że cały czas rysowałem im mapę w czasie rzeczywistym (diagram połączeń). Gdy coś pokazywałem lub tłumaczyłem lub gdy oni coś mówili, dodawałem ten byt na mapę.

Dominująca technika tej sesji to PĘD. Ani przez moment nie było chwili spokoju - ciągle się działo. Ten pęd wygenerował immersję oraz sprawiał, że przerzucali się między sobą.

Dlatego ta sesja by nie zadziałała dla 5-6 graczy. Nie byłoby pędu.

## Misja właściwa - istotne nowe koncepty

* Syntian: BIA + adaptogen kralotyczny = syntian; wieczny rozrost i głód
* Tiamenat: silny wpływ Grzymości i/lub Kajrata w Kompleksie Tiamenat w Zaczęstwie
* Tiamenat: potężnie niedoinwestowany; świetne miejsce z niedostatecznymi systemami obronnymi
* TAI Hestia: potężna strażnicza i naukowa superturingowa TAI; lojalna Astorii nade wszystko
* Wiktor Satarail: bez problemu potrafi dostać się do Tiamenat

**Epilog**:

* Tarn dołączy na krótki czas do Wiktora; tam się nażywi i zregeneruje
* Tarn opuści Trzęsawisko i wróci w kosmos - tam może / powinien robić coś korzystnego
* Tarn nie stanie się syntianem

## Streszczenie

Wiktor Satarail poproszony przez Pięknotkę skupił się na zniszczeniu Tarna. Zaatakował Tiamenat subtelnie, skażeniem biologicznym i hackerem uszkodził percepcję TAI Hestii. Tarn się obudził i odparł atak Wiktora; w wyniku ten go Zatruł. Ale Tarn wynegocjował życie - bo nikomu nie zrobił krzywdy. Wiktor się na to zgodził, jeśli Tarn opuści ten teren raz na zawsze - poleci w kosmos.

## Progresja

* Wiktor Satarail: zdobywa sporo wiedzy noktiańskiej dzięki współpracy z BIA Tarn.
* BIA Tarn: zregenerowany; spróbuje uciec w kosmos.
* TAI Hestia Tiamenat: zrozumiała, jak bardzo jej sytuacja jest beznadziejna - bez czujników, bez pieniędzy, bez szacunku. Plus - przerażona Satarailem i Tarnem.

### Frakcji

* .

## Zasługi

* Talia Aegis: robiła wszystko by uratować Tarna i... w sumie się udało. Odkażała skażeńców Wiktora, budowała antidotum itp.
* BIA Tarn: pełnił rolę koordynatora taktycznego, naukowca, jednostki walki, dywersji oraz negocjatora. Pokazał potęgę BIA poziom 3. Nikogo nie skrzywdził.
* Eustachy Mrownik: naukowiec astoriański; powiązany z mafią. Uratowany przez Tarna przed skażeniem, współpracował z nim mimo oporów ze strachu przed alternatywą.
* Pedro Ronfak: eks-noktianin, który dołączył do mafii. Ma słabość do Talii; ogólnie, dba o swoją skórę ale dobrze chroni Talię.
* Hestia d'Tiamenat: strażnicza i naukowa TAI Tiamenat, superturing; dla ochrony Tiamenat potrafiła współpracować z Tarnem. Przerażona możliwościami BIA lv3.
* Wiktor Satarail: na prośbę Pięknotki uderzył w Tiamenat by skażeniem biologicznym zniszczyć Tarna. Gdy Tarn przejął kompleks, Wiktor go Zatruł - ale darował życie.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Kompleks Tiamenat: w którym, jak się okazuje, ma niemałe wpływy Kajrat i jego siły; zaatakowany przez Wiktora Sataraila.
                                    1. Centrum Dowodzenia: główny budynek Tiamenat, w którym nie stało się absolutnie nic złego; odłamek Hestii stamtąd nie miał pojęcia o niczym.
                                    1. Centralny Biolab: miejsce gdzie była Talia i które było najsilniej zaatakowane przez Wiktora. Całkiem skażone, ale to naprawiono.
                                    1. Budynek Wołkowca: miejsce, gdzie Tarn się znajdował i obudził i odcinając tlen itp. odzyskał kontrolę nad wszystkim.

## Czas

* Opóźnienie: 6
* Dni: 1
