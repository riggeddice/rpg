## Metadane

* title: "Wrobieni detektywi"
* threads: deprecated
* gm: kić
* players: random

## Kontynuacja
### Kampanijna

* [190906 - Wypadek w Kramamczu](190906-wypadek-w-kramamczu)

### Chronologiczna

* [190906 - Wypadek w Kramamczu](190906-wypadek-w-kramamczu)

## Punkt zerowy

### Postacie

* Dobby Kmic - oficer marynarki wojennej, zwolniony dyscyplinarnie za odmowę wykonania niehonorowego rozkazu
* Serafina - kiedyś w wywiadzie, potem w agencji rządowej. Pragnie porządku. Działa raczej z dystansu
* Jonatan - mechanik motocyklista. Gra w klubie na bębnach. Złota rączka, bezpośredni.
* Dora - lekarz, medycyna plastyczna (wszczepy) Pragnie znaleźć bezpieczny kąt. Zaborcza i konfliktowa.
* Ksenia - terminuska

## Misja właściwa

W nocy napadnięto na oddział korporacji Luksuritias.
W wyniku ataku część magazynów wyleciała w powietrze.
Terminusi złapali napastników - zwykłe mięśnie do wynajęcia.
Polecenie zajęcia się tą sprawą otrzymała Ksenia (która już wtedy miała za dużo na głowie)

Wszystkie ślady wskazują na niewielką agencję detektywistyczną, która dotąd miała nieskazitelną reputację.
Ślady są niby w porządku, ale nieco zbyt oczywiste. Ksenia ma inne problemy na głowie...
A oni nie dość, że twierdzą, że nie mają pojęcia, o co chodzi, to jeszcze mają alibi... Niezbyt dobre, ale zawsze.
Pili razem świętując kasę, którą po ciężkiej przeprawie w końcu odzyskali od klienta.
Ksenia, Wiele się nie zastanawiając, postawiła detektywom ultimatum. Mają tydzień na wyjaśnienie sprawy. W końcu to ich praca. Niech wyjaśnią sprawę, bo inaczej ona, Ksenia, będzie musiała się tym zająć... A tego by nie chcieli.
Ksenia wyraźnie dała do zrozumienia, że mają jej nie zawracać głowy dopóki nie rozwiążą sprawy.

Poprosili o prawo przesłuchania napastników - dostali zgodę. Dostali też nagrania z monitoringu Luksuritias.
Wszystko wskazuje na to, że Dobby osobiście brał udział w ataku.

Postanowili przesłuchać osiłków. Nieco przekombinowali (najpierw do celi osiłków wsadzili Dobbiego -  że niby złapany, a potem wprowadzili przebraną za niego Dorę...) ale cel osiągnęli. Osiłki są przerażeni i wszystko powiedzą... choć nie wiedzą za wiele.

Jednak potwierdzili, że był z nimi "Dobby", a za akcję zapłaciła im "Dora".
No cóż... ktoś ewidentnie ich wrabia.
W dodatku okazało się, że zbiry miały "zgubić" w Luksuritiasie pieczątkę ich agencji.
Jak najbardziej prawdziwą - zginęła im kilka miesięcy temu, gdy Dobby rzucił nią w ociągającego się z zapłatą klienta.
Skojarzyli, że tym klientem był Grzegorz Stypor, manager średniego szczebla w Protogórze, kompanii górniczej z Pustogoru.
To z nim dochodzili się o kasę. Ale przecież w końcu zapłacił...
A sama pieczątka to za mało, żeby go oskarżyć.
Wyciągnęli od zbirów, że mieli dostać drugą połowę zapłaty po robocie. Bez problemu dowiedzieli się gdzie i jak.

W czasie, gdy Dobby, Dora i Serafina przesłuchiwali zbirów, Jonatan udał się do knajpy Olafa. pogadać ze swoim kumplem, który pracował w Protogórze.

Dowiedział się, że niedawno Protogór dostał lukratywny kontrakt z Luksuritiasu, ale jednocześnie, że pojawiły się plotki o tym, że kontrakt niemal padł z powodu scysji pomiędzy jednym z managerów a szefem oddziału Luksuritiasu.

Ok. Czyli mają kolejne poszlaki, silne, ale wciąż nie są to dowody...

Postanowili złapać zleceniodawcę w umówionym miejscu zapłaty.
Złapany i przesłuchany mężczyna powiedział, że jemu to śmierdziało i sam zrobił małe dochodzenie.

## Streszczenie

Luxuritias używa prywatnej agencji detektywistycznej jako kozła ofiarnego by coś ukryć. Plotki mówią, że niedawno Protogór dostał lukratywny kontrakt z Luksuritiasu, ale jednocześnie, że pojawiły się plotki o tym, że kontrakt niemal padł z powodu scysji pomiędzy jednym z managerów a szefem oddziału Luksuritiasu.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Dobby Kmic: właściciel agencji detektywistycznej w Pustogorze, wrobionej że niby wysadzili magazyny Luxuritias w okolicy. Pod ultimatum Ksenii jego ekipa zajęła się czyszczeniem swego imienia.
* Ksenia Kirallen: główny straszak; dała detektywom ultimatum, że jeśli oni są wrabiani to oni mają to rozwiązać (gorszy dzień Esuriit). Przeciążona pracą.
* Olaf Zuchwały: właściciel Górskiej Szalupy, zapewnił detektywom miejsce na rozmowę z przedstawicielami Luxuritias na terenie.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Eksterior
                                    1. Miasteczko
                                        1. Knajpa Górska Szalupa
                            1. Pustogor, okolice
                                1. Firma Protogór

## Czas

* Opóźnienie: -114
* Dni: 8






