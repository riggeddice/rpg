## Metadane

* title: "Pojedynek: Akademia - Rekiny"
* threads: rekiny-a-akademia
* gm: żółw
* players: kić, darken

## Kontynuacja
### Kampanijna

* [201006 - Dezinhibitor dla Sabiny](201006-dezinhibitor-dla-sabiny)

### Chronologiczna

* [201006 - Dezinhibitor dla Sabiny](201006-dezinhibitor-dla-sabiny)

## Punkt zerowy

### Dark Past

.

### Opis sytuacji

.

### Postacie

.

## Scena zerowa

MIESIĄC TEMU.

Teresa Mieralit i Aleksander Bemucik lecą w kierunku Nieużytków Staszka; dostali informację od Napoleona Bankierza, że doszło do "ustawki" między Rekinami i studentami AMZ (info przekazał im Napoleon Bankierz). I faktycznie, poziom energii z Nieużytków rośnie. Na miejscu kilkunastu magów rzucających zaklęcia przeciwko sobie, część na ścigaczach a część to klasyczni studenci AMZ.

Aleksander rzucił flashbangi; chce odwrócić ich uwagę i rozłożyć sprawę. (XOVXXX) - na ich widok część AMZów pouciekała, moc wyrwała się spod kontroli i zaczęła się stabilizować efemeryda, paru studentów zostało ogłuszonych a Rekiny się wycofały bez dowodów, koordynowane przez Kacpra Bankierza. Ale studenci - ci co zostali aktywni - oddali się pod dowodzenie Aleksandra.

Teresa natychmiast zaczęła stabilizować to cholerstwo; używa fokusu pryzmatycznego AMZ, symbolu szkoły. Zna to miejsce, to ulubione miejsce ustawkowe pomiędzy tymi grupami chuliganów. (VXVXm): Teresa wpierw zdestabilizowała efemerydę, ale wola i czysta nienawiść Liliany sprawiła, że część efemerydy przykleiła się do jednego z tamtejszych ścigaczy (Kacpra Bankierza). I niestety nie udało się tego ukryć przed terminusem - Tymon Grubosz wie, że znowu AMZ szaleli...

Oczywiście ze strony AMZ prowodyrką jest Liliana Bankierz... kto inny? Aleksander jednak broni studentów AZM - "Rekiny napadły, bo przecież na naszym terenie". Teresa tylko ciężko westchnęła...

## Misja właściwa

Liliana Bankierz zorganizowała zebranie grupy studentów AMZ. Na tym zebraniu, w Audytorium, przedstawiła dwie osoby: Myrczka, któremu arystokratka złamała serce (Sabina Kazitan ofc, nie mająca relacji z Rekinami) oraz Stellę Armadion, szulerkę, której nie tylko nie zapłacono za wygraną ale też potraktowali ją alergizatorem i ma teraz wysypkę. Co proponuje Liliana? KRUCJATĘ. Niedługo będzie rajd Rekinów, wyścig ścigaczy w Sensoplex. Liliana chce im sabotować wyścig. Niech będzie haniebny i wysłać to potem do ich rodzin.

Nie podoba się to ani Julii (która ma niemało klientów wśród Rekinów na naprawę i tuningowanie ścigaczy) ani Robertowi (który jest głównym dealerem do Rekinów; współpracuje też z Grzymościowcami). Robert powiedział, że otwarta walka jest kiepskim pomysłem - lepiej pokonać ich w uczciwej walce. (VOXVV) - Liliana nie jest przekonana, ale hojna oferta Roberta, że zdobędzie ścigacz i zajmie się projektem przekonała innych. Liliana uzna to jako poprawne TYLKO jeśli wygrają, ale wszyscy inni uznają niezależnie od okoliczności.

I tak problem przekształcenia otwartej wojny w zawody między Rekinami i studentami AMZ spadł w 100% na Roberta i Julię.

Po opracowaniu planu trzeba zrobić co następuje:

1. przekonać Rekiny że to warto zrobić
2. pozyskać ścigacz
3. pozyskać pilota, najlepiej z AMZ
4. zwiększyć szanse pilota na zwycięstwo (tuning ścigacza, info o trasie i przeciwnikach...)

Julii i Robertowi przeszło przez głowę kilka osób: Karolina Erenit (czysta moc magiczna), Diana Tevalier (integracja z psychotroniką i AI), ale najlepszym wyborem jest jednak Napoleon Bankierz. Napoleon ma bardzo paladyńskie, uczciwe podejście, nie jest może arystokratą Aurum (nie ma sprzężenia z sentisiecią) ale był uczony na jednego i kocha militaria. Robert przekonał go dość prosto: po pierwsze, może utrzeć nosa Lilianie, po drugie - może być bohaterem który zatrzyma ciągle eskalujące wojny między Rekinami i AMZ.

Z perspektywy przekonywania Teresy do tego by dała dostęp do starych, świetnych ścigaczy wojskowych w artefaktorium mają: +3V (Napoleon), +2V (szansa zamknięcia konfliktu), Teresa jest z Dare Shiver (+2V), deeskalacja Liliany i szansa na nie zniszczenie bo Julia (+2V). Robert dostał propozycję od Julii, by zebrał podpisy od innych studenciaków, ale on powiedział że woli utopić się w szambie. Czyli: 9V5X do 5.

Teresa Mieralit podeszła z lekkim rozbawieniem i zadowoleniem do inicjatywy AMZ. (VVXXVVXV). Zgodziła się, by Julia i Napoleon wybrali odpowiednio pasujący ścigacz, ale w 100% odpowiada za wynik Napoleon. Napoleon spojrzał prosto w oczy Teresie i powiedział, że nie ma problemu - weźmie to na siebie. Teresa powiedziała, że życzyłaby sobie by skorzystali z pomocy Liliany, Stelli albo Ignacego. Julia i Robert się skwapliwie zgodzili - wiedząc, że tą prośbę Teresy całkowicie zignorują.

Dobrze. Mają pilota i dostęp do ścigacza. Ale trzeba jeszcze przekonać Rekiny. Można pójść do Kacpra Bankierza (trudniej) albo do częściej współpracującego z nimi i bardziej zgodnego kulturowo Justyniana (łatwiej). Wybrali Justyniana - nawet jak nie wyłączy to konfliktu Liliana - Kacper, ale przynajmniej rozwiąże makroproblem AMZ - Rekiny.

Justynian Diakon przyjął Roberta i Julię ciepło. Gdy usłyszał od Roberta, że jest ryzyko, że skończą się zapasy "fajnych środków psychoaktywnych" przez te bitwy i ustawki to się serio zmartwił. Julia i Robert zaproponowali mu turniej - niech mag AMZ trafi na wyścig Rekinów i w ten sposób połączyć siły w pewien sposób. Zbliżyć do siebie dwie zwalczające się grupy. Justynian chce być paladynem, czymś lepszym na tym terenie niż tylko tępym łobuziakiem - pomoże Zespołowi. Wspólnie opracowali jak przekonać inne Rekiny, nie tylko te pod jego "kontrolą":

(XV): oczywiście, Kacper i Liliana są nieprzekonywalni; ale to też bitwa wewnątrzrodowa. (VV): uznanie. Rekiny są tu trochę na zesłaniu, bo nie wiadomo co z nimi robić. W ten sposób jakby oficjalne siły Szczelińca uznały, że mają znaczenie. Więc to też działa. (VV): to nie jednorazowo, to powtarzalna struktura, która też Rekinom otwiera nowe, ciekawe drzwi. A dodatkowo (V) pozycja Justyniana i podejście "nie rozwydrzona szlachta a porządni strażnicy" też pójdzie w górę.

Robert i Julia mają więc pewność, że to się odbędzie. Ale jak Napoleon ma wygrać?

Robert pokręcił się na imprezie Rekinów by zdobyć informacje o trasie i o tym na kogo należy uważać; dowiedział się o kilku ambitnych łebkach które KONIECZNIE chcą pokazać co i jak. Robert zapewnił też sobie, że dwóch Rekinów na torze zadba o to, by Napoleonowi nie stała się żadna poważna krzywda. Mimo, że mógł zrobić z Liliany kozła ofiarnego i więcej uzyskać, Robert zdecydował się tego nie robić. Nie ma z nią kosy a cel nie uświęca środków.

Przed Julią stał nie mniej ciekawy problem - który ścigacz z Artefaktorium wybrać. Pierwszy typ Napoleona - szybki ścigacz kurierski - odpadł; trasa w Sensoplex jest bardzo trudna i składa się z pułapek i ruin. Najlepszy byłby ścigacz noktiański; po chwili wahania, Julia odrzuciła go - zwycięstwo nie jest warte tego, by przekonywać Napoleona do używania noktiańskiego pojazdu... no i ewentualnej pogardy ze strony Rekinów.

Po zastanowieniu Julia wybrała stary astoriański ścigacz manewrujący. Ta instancja jest dość sprawna, ścigacz trzyma się kupy; uczestniczył w walce, ale nawet ma w miarę sprawną psychotronikę. Julia jednak zdecydowała się skorzystać z tego pojazdu - jest to niebezpieczne, ale TAI bojowa plus solidny ścigacz powinny dać Napoleonowi potężną przewagę. (Ex: VVX). Julia uruchomiła ścigacz, który przedstawił się jako Remor 340D. Ścigacz ma echo swoich ostatnich walk i podbojów; Julia "przekonała" go, że to rywalizacja między oddziałami. Niestety, to jej uruchomienie itp. sprawiło, że psychotronika ścigacza jest na stałe taka... dziwna. Julia stwierdziła, że nikt nie zamierza go nigdy już włączać - jej to pasuje :3.

Więc pula projektowa: 15V, 3O, 9X. Czas na Wielki Wyścig.

Na samym początku wyścigu Napoleon pokazał co umie - wysforował się na sam początek (VV). Dzięki swoim skillom i researchowi był w stanie bezproblemowo zrobić porządne wrażenie. Utrzymał to dalej (VX), ale go niestety wyprzedzili. Z uwagi na rangę i wydarzenie i media Rekiny się przekonały, że warto (VV). Ale Napoleon naciskał dalej - trafił na podium! Niestety, w wyniku tego ścigacz został bardzo ciężko pouszkadzany (XX); Teresa zdecydowanie nie doceni. Ale (V) ścigacz "przypomniał" sobie wojnę. Wraz z nim, w Sensoplexie - silnie napromieniowanym - ruszyła energia i wśród widowni pojawiły się echa.

Braterstwo. Walka. Próba odparcia wroga. Umierający ścigacz. Rekiny poczuły, że przecież oni są potomkami tych co walczyli o Astorię; a teraz walczą bezsensownie z innymi astorianami z AMZ? To bez sensu. Tak daleko doszło, że aż zdecydowali się zafundować tor w Podwiercie - coś, co pokaże że są godni bycia potomkami tamtych wojowników...

A Remor 340D wrócił do Artefaktorium AZM. Najpewniej będzie podreperowany. Tak czy inaczej, jego trud skończony. Przydał się po raz ostatni.

## Streszczenie

Kolejna ustawka między Rekinami a uczniami AMZ mogłaby się skończyć bardzo źle, więc grupa uczniów spróbowała wprowadzić sposób rozwiązywania konfliktu przez turnieje między Rekinami i AMZ. Nie tylko im się to udało - użycie starego ścigacza wojskowego Remor 340D i echo emocji wojen noktiańskich sprawiło, że Rekiny się unormowały i zainwestowano w budowę Toru Wyścigowego Pamięci w Podwiercie.

## Progresja

* Kacper Bankierz: do jego ścigacza połączyło się echo 'ustawki' i efemerydy. Ścigacz robi czasem "swoje rzeczy".
* Napoleon Bankierz: nie ma sprzężenia z sentisiecią Bankierz, ale był uczony na arystokratę Aurum.

### Frakcji

* Latające Rekiny - Paladyni: idą do góry wobec innych frakcji Rekinów; po tym wyścigu, obecności duchów wojskowych itp. Ich podejście "mamy coś do zrobienia" tu jest wzmocnione.

## Zasługi

* Robert Pakiszon: cel nie uświęca środków; nie zrobił z Liliany kozła ofiarnego. Dealer z AMZ dla Rekinów, współpracuje z Grzymościem. Przekonał Napoleona do pilotażu ścigacza w Turnieju Rekinów oraz zebrał dlań wszelkie potrzebne informacje o trasie i przeciwnikach by mógł skończyć na podium.
* Julia Kardolin: technomantka i psychotroniczka; uczennica Akademii Magicznej Zaczęstwa. Postawiła ścigacz Remor340D i pomogła Napoleonowi wygrać turniej Rekinów. Rekiny często u niej zamawiają tuning ścigaczy i ich naprawę, specjalizuje się w ścigaczach.
* Aleksander Bemucik: woźny-militarysta w Akademii Magicznej Zaczęstwa; staje twardo po stronie studentów AMZ. Nosi flashbangi itp. Kiedyś wojownik Zjednoczenia Letyckiego.
* Teresa Mieralit: miesiąc temu katalitycznie rozproszyła efemerydę złożoną przez bitwę studentów AMZ vs Rekiny. Zgodziła się by Napoleon pożyczył ścigacz z artefaktorium.
* Liliana Bankierz: prowodyrka i podżegaczka bitew między Rekinami i studentami AMZ. Spróbowała zapalić studentów do ostrzejszej wojny, ale Robert i Julia zmienili to w zawody.
* Stella Armadion: czarodziejka AMZ noktiańskiego pochodzenia; szulerka; zarabia na grach hazardowych. Cicha. Tu: wygrała z Rekinami i zamiast jej zapłacić, potraktowali ją proszkiem alergicznym. Liliana używa jej jako przykładu że trzeba tępić Rekiny.
* Ignacy Myrczek: Liliana używa tego, że Sabina z nim "zerwała" jako przykładu tego że trzeba tępić Rekiny.
* Napoleon Bankierz: podczas ustawki ostrzegł nauczycieli o efemerydzie; miłośnik historii i wojskowości, tu: ŚWIETNY pilot ścigacza który trafił na podium walcząc z Rekinami.
* Kacper Bankierz: wieczny przeciwnik Liliany - Rekin, który gardzi podejściem Liliany i uważa ją za impostora w rodzie. Zwykle robił przeciw niej ustawki. Ma małą frakcję Rekinów.
* Justynian Diakon: zdeptany po sprawie z Kazitan; postawił na współpracę z Akademią Magiczną Zaczęstwa i na turniej zamiast ustawek; wygrał na tym sporo pozycji i dobrej woli.
* Remor 340D: stary astoriański ścigacz; sprzężony psychotronicznie z czasem wojny noktiańskiej. Posłużył jako pojazd Napoleona Bankierza i pokazał Rekinom czym jest honor.

### Frakcji

* Latające Rekiny - Paladyni: ich pozycja idzie solidnie w górę - Tor Wyścigowy Pamięci oraz fakt, że to Justynian stał za tym by się to udało.
* Latające Rekiny - Mistrzowie: są zainteresowani Torem Wyścigowym Pamięci i ogólnie rozumianymi wyścigami ścigaczy. 
* Uczniowie Akademii Magicznej Zaczęstwa: zamiast (jak do tej pory) robić ustawki, zaczęli w ustrukturyzowany sposób (turnieje) rozwiązywać konflikty.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Sensoplex: miejsce w którym Rekiny się ścigały na turnieju; m.in. ścigał się tam też Napoleon na Remorze 340D, przed widownią.
                                1. Tor Wyścigowy Pamięci: za trzy miesiące powstanie; tego dnia zainicjowano jego konstrukcję.
                            1. Zaczęstwo
                                1. Nieużytki Staszka: ulubione miejsce ustawek między Latającymi Rekinami a uczniami AMZ.
                                1. Akademia Magii, kampus
                                    1. Audytorium: miejsce spotkań i przemów studentów; Liliana próbowała rozniecić ogień AMZ vs Rekiny. Na szczęście zmieniono to w zawody.
                                    1. Artefaktorium: miejsce gdzie znajdują się stare i mniej bezpieczne artefakty w AMZ. Julia pozyskała stamtąd wojenny ścigacz. Trochę muzeum.

## Czas

* Opóźnienie: 5
* Dni: 8

## Inne

Opracowanie sesji:

Cel:

* Niskie stawki. Niech gracze mają okazję się wykazać kompetencją i umiejętnościami.
* Przedstawienie Rekinów jako "uczniów na wymianie studenckiej" i huliganów.
* Przedstawienie uczniów Akademii Magicznej jako potencjalną bombę, która może wybuchnąć.
* Działania pomiędzy Akademią a Rekinami są napięte i mogą wybuchnąć - pokazanie odłamka.

Główna linia:

* Ostre, potencjalnie krwawe ustawki pomiędzy Rekinami i magami Akademii.
* Próba przekształcenia zimnej wojny w coś, co będzie po prostu potężnym i solidnym turniejem.
* Operacją dowodzi: jedyna i niepowtarzalna Liliana Bankierz :D.
* Cel: wprowadzić Zespół na Efemeryczne Wyścigi Ścigaczy w Sensoplex.

Godne sceny:

* Scena zero - nauczyciele muszą rozproszyć potencjalnie efemeryczny stan bitwy magicznej pomiędzy Rekinami i uczniami AM.
* Scena jeden - Stella Armadion z ostrą wysypką. Liliana podżega wszystkich do Wielkiej Wojny z Rekinami. Aranea próbuje deeskalować.
* Wyścig pomiędzy championami Akademii a Rekinami, zwłaszcza mistrzem - Kacprem Bankierzem (ród Liliany i Napoleona).

