## Metadane

* title: "Ćwiczenia komodora Bladawira"
* threads: triumfalny-powrot-arianny, naprawa-swiata-przez-bladawira
* motives: beznadziejny-szef, zrzucenie-winy-na-innych, niedoceniany-wierny-pies, bitter-vet, pojedynek-rywali, manhunt
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [231109 - Komodor Bladawir i Korona Woltaren](231109-komodor-bladawir-i-korona-woltaren)

### Chronologiczna

* [231109 - Komodor Bladawir i Korona Woltaren](231109-komodor-bladawir-i-korona-woltaren)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA: 
    * Nemesea "Believe"
        Razalis chce pokazać Bladawirowi co i jak.
* Opowieść o (Theme and vision)
    * .
    * .
    * SUKCES
        * .
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * beznadziejny-szef: komodor Bladawir jest źródłem cierpienia dla swoich podwładnych, ale jest po prostu świetny w tym co robi. I niski.
    * zrzucenie-winy-na-innych: próba zrzucenia winy z nieudanych ćwiczeń na komodora Bladawira jako pomniejsza zemsta i próba wyplątania Inferni.
    * niedoceniany-wierny-pies: kapitan Waritniez, który cierpi z powodu działań Bladawira ale go wspiera. Osłania swoich ludzi.
    * bitter-vet: kapitan Darbik, który miał dobry pomysł (zakłócenia min), ale mu się nie udało, Orbiter jego poświęcił nie chcąc poświęcać nikogo bardzo wartościowego.
    * pojedynek-rywali: Bladawir (perfekcyjne plany taktyczne) vs Razalis (empatia oraz traktowanie dobre ludzi). Ta dwójka jest w wiecznej wojnie ideologicznej.
    * manhunt: asymetryczne ćwiczenia; Bladawir ma się wbić na Karsztarin i wydobyć z niego dane 'Syndykatu'. Osoba na pokładzie Karsztarina jest tą na którą polujemy.
* Detale
    * Crisis source: 
        * HEART: "Bladawir który nacisnął Razalis by była lepsza. Razalis która próbuje pokazać mu że jest dość dobra."
        * VISIBLE: ""
    * Delta future
        * DARK FUTURE:
            * chain 1: .
            * chain 2: .
            * chain 3: .
        * LIGHT FUTURE:
            * chain 1: .
            * chain 2: .
            * chain 3: .
    * Dilemma: brak, sesja funkcjonalna

### Co się stało i co wiemy

* Przeszłość
    * Aneta, naciśnięta przez komodora, poszła by przetransportować coś z okolic Iorusa w okolice Neikatis. Dużo pieniędzy. Złapał ją Prefektiss
    * 'Aneta' rozłożyła nanofar w 'Sadzie' (Sad jest miejscem od zawsze w rękach rodziny Waltaren)
* Teraźniejszość
    * .

### Co się stanie (what will happen)

* Infernia jako jednostka bojowa odwracająca uwagę. A dokładniej 'Paprykowiec'.
* ...

## Sesja - analiza

### Fiszki

Zespół:

* Leona Astrienko: psychotyczna, lojalna, super-happy and super-dangerous, likes provocative clothes
* Martyn Hiwasser: silent, stable and loyal, smiling and a force of optimism
* Raoul Lavanis: ENCAO: -++0+ | Cichy i pomocny;; niewidoczny| Conformity Humility Benevolence > Pwr Face Achi | DRIVE: zrozumieć
* Alicja Szadawir: młoda neuromarine z Inferni, podkochuje się w Eustachym, ma neurosprzężenie i komputer w głowie, (nie cirrus). Strasznie nieśmiała, ale groźna. Ekspert od materiałów wybuchowych. Niepozorna, bardzo umięśniona.
    * OCEAN: N+C+ | ma problemy ze startowaniem rozmowy;; nieśmiała jak cholera;; straszny kujon | VALS: Humility, Conformity | DRIVE: Uwolnić niewolników
* Kamil Lyraczek
    * OCEAN: (E+O-): Charyzmatyczny lider, który zawsze ma wytyczony kierunek i przekonuje do niego innych. "To jest nasza droga, musimy iść naprzód!"
    * VALS: (Face, Universalism, Tradition): Przekonuje wszystkich do swojego widzenia świata - optymizm, dobro i porządek. "Dla dobra wszystkich, dla lepszej przyszłości!"
    * Styl: Inspirujący lider z ogromnym sercem, który działa z cienia, zawsze gotów poświęcić się dla innych. "Nie dla chwały, ale dla przyszłości. Dla dobra wszystkich."
* Horst Kluskow: chorąży, szef ochrony / komandosów na Inferni, kompetentny i stary wiarus Orbitera, wręcz paranoiczny
    * OCEAN: (E-O+C+): Cichy, nieufny, kładzie nacisk na dyscyplinę i odpowiedzialność. "Zasady są po to, by je przestrzegać. Bezpieczeństwo przede wszystkim!"
    * VALS: (Security, Conformity): Prawdziwa siła tkwi w jedności i wspólnym działaniu ku innowacji. "Porządek, jedność i pomysłowość to klucz do przetrwania."
    * Core Wound - Lie: "Opętany szałem zniszczyłem oddział" - "Kontrola, żadnych używek i jedność. A jak trzeba - nóż."
    * Styl: Opanowany, zawsze gotowy do działania, bezlitośnie uczciwy. "Jeśli nie możesz się dostosować, nie nadajesz się do tej załogi."
* Lars Kidironus: asystent oficera naukowego, wierzy w Kidirona i jego wielkość i jest lojalny Eustachemu. Wszędzie widzi spiski. "Czemu amnestyki, hm?"
    * OCEAN: (N+C+): bardzo metodyczny i precyzyjny, wszystko kataloguje, mistrzowski archiwista. "Anomalia w danych jest znakiem potencjalnego zagrożenia"
    * VALS: (Stimulation, Conformity): dopasuje się do grupy i będzie udawał, że wszystko jest idealnie i w porządku. Ale _patrzy_. "Czemu mnie o to pytasz? Co chcesz usłyszeć?"
    * Core Wound - Lie: "wiedziałem i mnie wymazali, nie wiem CO wiedziałem" - "wszystko zapiszę, zobaczę, będę daleko i dojdę do PRAWDY"
    * Styl: coś między Starscreamem i Cyclonusem. Ostrożny, zawsze na baczności, analizujący każdy ruch i słowo innych. "Dlaczego pytasz? Co chcesz ukryć?"
* Dorota Radraszew: oficer zaopatrzenia Inferni, bardzo przedsiębiorcza i zawsze ma pomysł jak ominąć problem nie konfrontując się z nim bezpośrednio. Cichy żarliwy optymizm. ŚWIETNA W PRZEMYCIE.
    * OCEAN: (E+O+): Mimo precyzji się gubi w zmieniającym się świecie, ale ma wewnętrzny optymizm. "Dorota może nie wiedzieć, co się dzieje, ale zawsze wierzy, że jutro będzie lepsze."
    * VALS: (Universalism, Humility): Często medytuje, szuka spokoju wewnętrznego i go znajduje. Wyznaje Lichsewrona (UCIECZKA + POSZUKIWANIE). "Wszyscy znajdziemy sposób, by uniknąć przeznaczenia. Dopasujemy się."
    * Core Wound - Lie: "Jej arkologia stanęła naprzeciw Anomaliom i została Zniekształcona. Ona uciekła przed własną Skażoną rodziną" - "Nie da się wygrać z Anomaliami, ale można im uciec, jak On mi pomógł."
    * Styl: Spokojna, w eleganckim mundurze, promieniuje optymizmem 'że z każdej sytuacji da się wyjść'. Unika konfrontacji. Uśmiech nie znika z jej twarzy mimo, że rzeczy nie działają jak powinny. Magów uważa za 'gorszych'.
    * metakultura: faerilka; niezwykle pracowita, bez kwestionowania wykonuje polecenia, "zawsze jest możliwość zwycięstwa nawet jak się pojawi w ostatniej chwili"

Castigator:

* Leszek Kurzmin: OCEAN: C+A+O+ | Żyje według własnych przekonań;; Lojalny i proaktywny| VALS: Benevolence, Humility >> Face| DRIVE: Właściwe miejsce w rzeczywistości.

Komodorowie i kapitanowie:

* Antoni Bladawir: OCEAN: A-O+ | Brutalnie szczery i pogardliwy; Chce mieć rację | VALS: Power, Family | DRIVE: Wyczyścić kosmos ze słabości i leszczy.
    * "nawet jego zwycięstwa mają gorzki posmak dla tych, którzy z nim służą". Wykorzystuje każdą okazję, by wykazać swoją wyższość.
    * Doskonały taktyk, niedościgniony na polu bitwy. Jednocześnie podły tyran dla swoich ludzi.
    * Uważa tylko Orbiterowców i próżniowców za prawidłowe byty w kosmosie. Nie jest fanem 'ziemniaków w kosmosie' (planetarnych).
* Kazimierz Darbik
    * OCEAN: (E- N+) "Cisza przed burzą jest najgorsza." | VALS: (Power, Achievement) "Tylko zwycięstwo liczy się." | DRIVE: "Odzyskać to, co straciłem."
    * kapitan sił Orbitera pod dowództwem Antoniego Bladawira
* Michał Waritniez
    * OCEAN: (C+ N+) "Tylko dyscyplina i porządek utrzymują nas przy życiu." | VALS: (Conformity, Security) "Przetrwanie jest najważniejsze." | DRIVE: "Chronić moich ludzi przed wszystkim, nawet przed naszym dowódcą."
    * kapitan sił Orbitera pod dowództwem Antoniego Bladawira
* Ewa Razalis
    * OCEAN: (A+ E+) "Lepsza jest siła serca niż siła broni." | VALS: (Benevolence, Hedonism) "Radość i prosperity, zwłaszcza w kosmosie." | DRIVE: "Empatia ma miejsce w kosmosie."
    * kiedyś pod Bladawirem, teraz niezależny komodor chcąca udowodnić Bladawirowi, że jego podejście jest błędne

### Scena Zero - impl

Dwa dni po powrocie z akcji, Kurzmin do Arianny.

* Kurzmin: Było ciężej niż się spodziewałem, gorzej niż z kralothem.
* Arianna: nie mieliśmy Inferni by zbadać sprawę
* Kurzmin: Patryk jest w dobrej formie, zgłosił się na ochotnika do kolejnej akcji z Tobą.
* Arianna: cieszę się że przynajmniej on jest zadowolony. Powiedział czemu chce udział w czymś takim?
* Kurzmin: Powiedział, że widział prawdziwego... oficera w akcji. Nie chciał mnie obrazić (lekki uśmiech), ale zrobiłaś wrażenie. Patrzy na Ciebie... "wow, TAK wyglądają PRAWDZIWE akcje".
* Arianna: a wszyscy OSHITOSHITOSHIT.
* Kurzmin: Patryk i Marta - ale zwłaszcza Patryk - zostali bohaterami w oczach innych tienów. Jednocześnie kilka osób myśli czy Orbiter jest dla nich.
* Arianna: może i lepiej... jeżeli zauważają że nie pasują... nie zrobią krzywdy innym i sobie
* Kurzmin: zrobiłaś wrażenie - /lekki uśmiech, po chwili powaga - Marta dalej w szpitalu. Została zraniona przez terrorforma i wdała się infekcja ixiońska. Próba Adaptacji. Na szczęście, nic poważnego.
* Kurzmin: przy okazji, część personelu medycznego chce zbadać Martę - okazja infekcji ixiońskiej na magu.
* Kurzmin: Marta z nikim nie rozmawia o tym co tam się stało. Ale jest stabilna. Za to Patryk... raport z walki wypłynął na Castigator, tak Ci powiem.
* Arianna: z dużymi szczegółami?
* Kurzmin: nie, o dziwo - nie. Nie było koloryzowania. Ale to sprawia, że Wy macie też... inaczej na Was patrzą. Chyba (lekki śmiech) zostałaś baseline Orbitera w oczach tienów.
* Arianna: przylatywać z kontrolą by poustawiać ;-)

Arianna chce się dowiedzieć jakie polecenie wydał wobec ludzi ocalonych (czy tamtej jednostki)? Cóż, Klaudia musi szukać po raportach i dokumentach i komunikatach.

Tr Z (Bladawira nie lubią, ludzie gadają i narzekają) +3 (Klaudia chciałaby doprowadzić do końca a ONA TAM JEST):

* X: Klaudia przesłała dane (ale bez fragmentu z Dark Awakening by Ariannie reputacji nie zniszczyć)
* V: 
    * Bladawir wsadził Wojciecha i Lidię do biovatów by ich naprawić. A resztę deportował. Nie poczekał. Deportował OJCA bez córki (w biovacie).
    * Wydał polecenia w kierunku na tą jednostkę ixiońską, nie zostawił tego samego.
* X: CAŁE dane z walki. Paweł, Marta - wyjdą na "sensownych tienów" w oczach Orbitera. A Arianna będzie SCARY. Nauczyła się dużo od Lodowca.
* Vz: Bladawir wysłał jednostki mające OSŁANIAĆ jednostkę ixiońską by nic nie zrobiła głupiego (jeśli coś robi zastrzelcie to). A potem - jednostki "naukowe".

Czyli, o dziwo, sytuacja opanowana i pod kontrolą.

### Sesja Właściwa - impl

Dwa dni później. Eustachy ZNOWU coś robi na Castigatorze. Wszyscy podejrzewają, że Leona sabotuje Castigator by Eustachy go naprawiał. Zwłaszcza z uwagi na "złośliwość" tych uszkodzeń i użycie Alicji. Kurzmin też zadowolony, bo mimo wszystko sabotaże są minimalne a Castigator MUSI być naprawiony. Trzech tienów wróciło na planetę. Wracali Tucznikiem. "MAMO ZABIERZ MNIE STĄD!".

Leona (dla jaj) i Kamil (na serio) zmontowali "Arianny słowo na dzień". Patryk poprosił o kopię. Kamil dał mu z radością, Leona też, acz z innych powodów. Kurzmin z radością powiedział o tym Ariannie. Marta czuje się coraz lepiej, ale nadal musi tam leżej.

Prognozy Lidii są takie, że jakiś miesiąc+ będzie leżeć w biovacie. Wojciech - tylko dwa tygodnie. Lidia i Wojciech są na stacji Atropos.

* Kurzmin: (zdziwiony) Dałem Ci Paprykowiec ale na chwilę. A teraz komodor Bladawir zażądał Twojej obecności i Paprykowca. Powiesz mi co się dzieje?
* Arianna: Nie wiem, nie dostałam informacji jeszcze... Infernia JEST sprawna. Ma prawo zażądać Twojego statku?
* Kurzmin: W sumie, nie... zwłaszcza nie tego. (zdziwiony). Porozmawiam z nim.
* Arianna: Nie chciałabym Cię pozbawić sprawnej jednostki
* Kurzmin: Nie jest to NAJLEPSZA jednostka, ale nie tak, nie rozumiem tej prośby
* Arianna: Niebezpieczna akcja to lepiej stracić jednostkę ze śmieciowego Aurum niż z Orbitera?
* Kurzmin: Ale Infernia jest z Neikatis, też nie jest dumną jednostką...
* Arianna: Tak...

TERAZ, W TEJ CHWILI rozkazy od Bladawira. (wszystkie potrzebne sigile) komodor Arianny dał dostęp do Arianny jak długo nie jest na Inferni. Bladawir chce Ariannę na ćwiczenia. I dał listę parametrów.

* Arianna może wziąć załogę na Paprykowiec.
* Arianna będzie uczestniczyć w ataku na Karsztarin.
    * 3 lekkie korwety (o określonych parametrach i tonażu) symbolizujące Orbiter. Karsztarin będzie uzbrojony i wyposażony jako forteca Aureliona.
    * Karsztarinem dowodzi komodor Ewa Razalis.
        * Komodor Ewa Razalis jest SYMPATYCZNĄ kobietą. "Lepsza jest siła serca niż siła broni". Załoga ją lubi. A Ewa stoi za załogą.
        * Plotki mówi (podparte raportami), że Ewa Razalis była kapitanem pod Bladawirem
            * Plotki mówią, że oni się ścinali. Podobno Razalis próbowała naprostować Bladawira. A Bladawir próbował wychować Razalis.
            * Plotki też mówią, że byli parą. I poszło bardzo bardzo na ostre.

Dla obu komodorów to BARDZO ważna sprawa. Podobno (plotki) po drodze szły obelgi przy tych ćwiczeniach, co daje bardzo interesującą SCENERIĘ.

Arianna - ku wielkiemu szokowi - widzi, że w rozkazie od Bladawira jest klauzula. "MOŻESZ odmówić tych rozkazów. Nie są w pełni oficjalne i formalne, choć są legalne. Ale PROSZĘ o Twoją obecność."

* Podobno komodor Bladawir powiedział, że wystarczą mu trzy jego jednostki i niech Ewa to sobie wsadzi cokolwiek potrzebuje, on nie potrzebuje nic więcej.
* Na to Ewa Razalis powiedziała, że Bladawir może dobrać więcej. Jeśli poprosi i oni się zgodzą.
* Bladawir powiedział, że ona może mieć wyprzedzenie, dowolne siły, WHATEVER jeśli tak. ON ma sekretną broń. Swój umysł. Coś, czego ona nigdy nie zrozumie. Bo nie ma.
* Razalis na to, że JEJ żołnierze wykonują polecenia bo im ZALEŻY na Orbiterze. A on nie ma przyjaciół.

Czas porozmawiać z komodorem...

* Bladawir: Kapitan Verlen, cieszę się, że jednak zdecydowałaś się na dołączenie do grupy uderzeniowej "Życie jest ciężkie".
* Arianna: Jak nazywa się nasz przeciwnik?
* Bladawir: Gdyby była szczera, nazwałaby "Pałace i salony", jak jakiś śmieć z Aurum. Ale nie wiem jak się nazywa.
* Arianna: Proponuję "Beztroskie wakacje"
* Bladawir: (uśmiechnął się). Dobry wybór. Plan jest prosty. Mam dwie jednostki. Jedna będzie szybka. Druga będzie osłaniać. Ty jesteś w Paprykowcu.
* Arianna: Jakie zalety ma Paprykowiec?
* Bladawir: Jest bezwartościowy. To jest zaleta. Ty wchodzisz na pokład jednostki Paprykowcem, z innego kąta. Ty przechwycisz dane. 

Karsztarin jest jak baza. Bladawir atakuje tą bazę, chce zdobyć dokumenty. Dwie korwety mają sprawić, że cała uwaga pójdzie na nich. A Paprykowiec, ogólnie kiepski (ale szybki) ma zrobić insercję Arianny i kogo-tam-trzeba. TAK jak kiedyś Arianna zrobiła to Królową też na Karsztarin. Baldawir zakłada, że Razalis skupi się na nim i jego siłach. A nie na Ariannie. Arianna, tymczasem, ma INNĄ misję. Nie ma zdobyć dokumentów. Ma doprowadzić do "uszkodzenia reaktora". Nie jest odpowiednio uzbrojona, ale jest arcymagiem. To znaczy, że to się obroni. I wtedy - on użyje swoich sił commando i wbije się na Karsztarin. (uśmiechnął się): nigdy w planach nie było powiedziane, że nie mogę użyć transportowca komandosów. A jak Karsztarin będzie ciemny, wlecą.

* Bladawir: Dasz radę? Nie jest to łatwa operacja, ale widziałem co potrafisz. Diament wśród śmieci.
* Arianna: Nie powinno być problemu, panie komodorze
* Bladawir: Wybierz członków załogi. Twoich. Nie mogę dać Ci moich z uwagi na pewne... komplikacje (myśli o zakładzie)

Arianna jednak CHCE to wygrać. Podgryzanie Bladawira nie jest czymś do czego Arianna chce się zniżać. A Arianna nie była nigdy mściwa.

Klaudia próbuje dowiedzieć się dokumentami itp. co wie Ewa Razalis i jaki ma plan. I niech Dorota dowie się co Ewa albo jej oficerowie załatwiali.

Dorota Radraszew. Ona ROZUMIE (jako że działa dla arcymaga). Gdy Arianna dała jej rozkazy by dowiedziała się o sytuacji, Dorota podeszła do tematu z pełną sumiennością. Zaczynając od wizyty na Karsztarinie. Bo jeszcze nikt nie wie o Bladawirze.

Ex (hidden movement) Z (Dorota zna wszystkich i wszędzie + Przemytnik) +4 (Arianna oficjalnie nic nie wie):

* X: Nie da się dowiedzieć DOKŁADNIE na czym i jak ściąga dane Ewa Razalis, ale Ewa przygotuje się na coś większego. Nie tylko nie zlekceważyła. Traktuje to jak LIFE OR DEATH. Łącznie z security.
* X: Dorota została pojmana, pobita i zwrócona. Nic poważnego jej się nie stało; to raczej ludzie Ewy Razalis z własnej inicjatywy.
* Vr: Dorota, przy wszystkich problemach, dowiedziała się dwóch rzeczy
    * że ktoś wyciekł informacje, że Arianna - arcymag Arianna Verlen - będzie PRAWDZIWYM celem insercji.
    * że Razalis WIE o Ariannie, ale oficjalnie nie wie
    * Ewa na serio podejrzewa Paprykowiec jako najważniejszą jednostkę.
    * Ewa Razalis ściąga siły, środki, metody - ściągnęła jednostkę typu drone-carrier.

Klaudia uzupełnia dane od Doroty. Co Ewa widzi, jakie ma plany i jak działa.

Tr (bo bonus od i Klaudii i Doroty) +3:

* X: Klaudia zebrała TAKŻE dane które Bladawir już znał i znalazł. To co znalazła trzeba Bladawirowi pokazać. Klaudia "pozornie nie dołożyła staranności", ale Bladawir wiedział więcej niż się wydawało.
* X: Klaudia miała przeciwko sobie triggery pozakładane przez ludzi oraz TAI ponad poziom dostępny Ewie Razalis. Wiele osób chce porażki Bladawira. Triggery poodpalane.
* V: Widać wyraźnie, że Ewa Razalis jest gotowa "na wszystko". Klaudia ma różne obrazy, plany, komponenty - Bladawir nie ma JAK zrobić sensownej insercji. Tego się NIE DA zrobić.
* Vr: Dzięki danym od Dorocie, wyprzedzeniach i sile różnych rzeczy - Klaudia ma dane o krecie. To jedna z bardziej zaufanych jednostek (grup) pod Ewą Razalis która ma link do technicznych u Bladawira. Kret jest bardzo blisko Bladawira. Bardzo. To adiutant czy coś. Ten adiutant ma dziewczynę i ONA miała poważny problem u Bladawira. Bo nie była dość dobra i zawalała sprawy.

Arianna idzie porozmawiać z Bladawirem. Czas na dyskusję. Ma dużo.

* Bladawir: Kapitan Verlen.
* Arianna: Komodorze, kazałam mojemu człowiekowi pogrzebać w planach przeciwnika i choć nie jestem zaskoczona ale plan różni się od przedstawionego; część informacji wyciekło i uwagę przeciwnika na Paprykowiec by wykonać zupełnie inny atak. Najczęściej zdarzało mi się przy pracy z Bolzą że mamy takie asy w zanadrzu.
* Bladawir: (patrzy na Ariannę świńskimi oczkami)

Ex Z (Arianna wszystko złapała) +4:

* X: Bladawir jest zaskoczony i WIDZI że Arianna widzi że jest zaskoczony.
* X: Bladawir zauważa, że Arianna próbuje go skłonić / manipulować itp. ALE Bladawir, w jego umyśle, jest to OK, bo tego się spodziewa po innych. 
    * Ma +1 do obrony na Arianny manipulację ALE ceni jej kompetencję.
* V: Bladawir zdecydował się PODZIELIĆ PRAWDZIWYM planem z Arianną (przy założeniu że powie mu o adiutancie)
    * Bladawir: nieźle. Tak, Ewa Razalis ma jedną dużą słabość, która jest tak oczywista, że nie ma jej w dokumentach Twojej biurokratki. Bo jest zbyt oczywista. Ewa Razalis ufa ludziom i uważa, że ja jestem łatwy do zdradzenia.
    * Bladawir: adiutant jest po mojej stronie tak jak jej elitarny oddział. Nie byli tani. Ale są moi.
    * Bladawir: celem ćwiczenia jest to, żeby Ewa Razalis, WIEDZĄC że wszystko w jej rękach, że wygrała sympatią ludzi - zobaczyła, że jej najbardziej zaufani ludzie stają przeciw niej. Pracuję nad tym od dwóch lat. Ja zbudowałem ten oddział.
    * Bladawir: ich celem nie jest dostać się do reaktora. Ich celem jest porwanie Ewy Razalis. Wszystko inne to dywersja. Mam na pokładzie Karsztarina moje siły. Tak, jak Orbiter by to zrobił naprawdę. Bolza pierwszy dowodził tego typu operacją.
    * Bladawir: (szeroki uśmiech) Ewa Razalis nigdy już nie zaufa żadnemu żołnierzowi i zrozumie jak wygląda świat w jakim żyjemy.
    * Arianna: Dlaczego jest pan pewien lojalności tych ludzi?
    * Bladawir: Standard. Pieniądze, rodziny i mam dostęp do ich rodzin. I oni o tym wiedzą. I wiedzą, że jeśli KTOKOLWIEK zawiedzie, to WSZYSCY ucierpią.
* X: Bladawir będzie chciał Ariannę pod siebie. Arianna będzie pod Bladawirem.
* Vz: Bladawir NIE UWIERZY że Arianna może się obrócić przeciwko niemu, nie ma powodów ani podstaw a jest na tyle rozsądna że rozumie świat. Choć troszkę.

Arianna uważa ten plan za chory XD. Dyskretnie skontaktowała się z Kurzminem. 

* Arianna: "Słuchaj, mój komodor ma bardzo nieuczciwy plan poradzenia sobie ze swoją byłą. Potrzebuję, by ktoś z zewnątrz pchnął dziewczynę w odpowiednią stronę. Niech media na to patrzą. Cokolwiek się dzieje - zarejestrowane i odpowiedzialni poniosą konsekwencje. Byłbyś w stanie jej podpowiedzieć że chcesz mieć prasę?"
* Kurzmin: "Dobrze, ale... na pewno?"
* Arianna: "Na pewno. Możesz powiedzieć że moja największa fanka zainteresowana, media, itp. Bohaterka bierze udział w tak dofinansowanych ćwiczeniach."
* Arianna: "To przestało być ćwiczeniami jak komodor 2 lata wsadził i czasu i pieniędzy i życia tych ludzi by tylko skrzywdzić byłą."

Kurzmin próbuje przekonać Ewę Razalis że to ważne i nie zdradzić źródła i jej pomóc i wygrać.

Ex (+P: Ewa ufa ludziom, +P: Kurzmin ma detale mimo że nie mówi) +3 -> Tr +3:

* Vr: Ewa weźmie to na serio, to znaczy, że będą media itp.
* XX: Kurzmin zdeklarował się jednoznacznie przeciwko Bladawirowi i Bladawir o tym wie. (+1Vg)
* X: Ewa doszła do tego, że Arianna jej chce pomóc. Bladawir też. Ale Bladawir w to nie wierzy. To znaczy, że Bladawir MYŚLI, że Kurzmin działa MYŚLĄC czego Arianna by chciała. Kolejny dowód jego tezy. Bladawir pokaże Kurzminowi, że świat jest okrutny. (+1Vg)
* Vr: Przez prasę MIMO zwycięstwa Bladawira Bladawir dostał.

EPILOG:

* Faktycznie - Bladawir porwał Ewę jej lojalnymi ludźmi, co ją straumatyzowało. Ona im ufała. 
* Prasa uznała to nie za myśl taktyczną a za coś chorego i koszmarnego. Jak on mógł. "Czy my jesteśmy noktianami?!"
    * Obiecująca reporterka, Izabela Zarantel, zrobiła wielki reportaż o Bladawirze i jego brudnych metodach i podejściach.
    * Iza osiągnąła coś niemożliwego - w wywiadzie z Bladawirem doprowadziła, że powiedział "Chciałem Ewie pomóc i pokazać jej słabości. Ma wszystko poza rozumem. Ma serce a nie rozum. Coś ją zniszczy." i zestawiła to z sytuacją Ewy która wpada tymczasowo w lekką paranoję. "Coś ją zniszczy, byłeś to ty".
* Ogólnie, "wszyscy przegrali", duży koszt niepotrzebny ćwiczeń, Bladawir dostał w oczach opinii publicznej, Razalis wyszła na ofiarę oraz na nieudane koszty i dostała emocjonalnie.
    * Nie jest to coś, co Orbiter kiedykolwiek chciał zobaczyć.

Z jedynych dobrych informacji, Infernia dostała przydział pod komodora Bladawira. Z tych lepszych informacji, Bladawir musi tymczasowo działać delikatniej. Bo na niego patrzą.

## Streszczenie

Komodorzy Bladawir i Razalis mają historię i przy wspólnych ćwiczeniach zrobili wszystko by to drugie nie wygrało. Bladawir ściągnął Ariannę jako dywersję, ale okazało się że to był plan wewnątrz planu. Gdy Klaudia doszła do tego, że atak na Karsztarin jest niemożliwy, Arianna wkradła się w łaski Bladawira i poznała plan - porwać Ewę Razalis jej lojalnymi ludźmi. Arianna poszła za planem (prosząc Kurzmina by ostrzegł Razalis); Bladawirowi Arianna się spodobała do tego stopnia, że dodał ją do swoich sił. Też dlatego, bo Kurzmin stanął przeciw niemu a Bladawir nie wybacza.

## Progresja

* Antoni Bladawir: przez Izę, Bladawir spotyka się z publicznym potępieniem, a Razalis postrzegana jest jako ofiara.
* Ewa Razalis: przez Izę jest postrzegana jako uczciwa ofiara Bladawira; wpada w lekką paranoję, ale Bladawirowi nie udało się jej zniszczyć.
* Leszek Kurzmin: zdeklarował się jednoznacznie przeciwko Bladawirowi i Bladawir o tym wie.
* OO Infernia: trafia pod kontrolę komodora Bladawira
* Arianna Verlen: trafia pod kontrolę komodora Bladawira

## Zasługi

* Arianna Verlen: dzięki blefowaniu, przymilaniu się i danym Klaudii zadowoliła komodora Bladawira który powiedział jej swój plan. Poprosiła Kurzmina by pomógł zneutralizować zniszczenie komodor Razalis. Była więcej niż tylko dywersją w rękach Bladawira. 
* Klaudia Stryk: na podstawie danych zebranych przez Dorotę doszła do tego jaki jest PRAWDZIWY plan Bladawira. Znalazła kreta. Doszła też, że Bladawir nie ma szans na sensowną insercję i że on o tym wie. Dzięki niej Arianna miała możliwość prawidłowego działania.
* Antoni Bladawir: były kochanek i przełożony kmdr Ewy Razalis, przez kilka lat podkładał jej ludzi do elitarnej gwardii by ostatecznie ją ZNISZCZYĆ i pokazać jej że każdego da się kupić. Ściągnął do siebie Ariannę jako dywersję, ale jak wykazała się skillsetem to dołączył ją bo kompetentna. Absolutnie okrutny. Jego słabością jest przekonanie o jego intelekcie i absolutny brak sympatii do ludzi.
* Ewa Razalis: komodor, kochanka Bladawira i jego była podkomendna. 'Lepsza jest siła serca niż siła broni' chyba że chodzi o Bladawira. Weszła z nim w konflikt by tylko udowodnić mu że nie jest na tyle inteligentny i skuteczny, ale jej własna gwardia ją skrzywdziła. Posłuchała Kurzmina, i nie wyszła całkowicie na minus. Jej słabością jest jej wiara w ludzi.
* Izabela Zarantel: zrobiła wielki reportaż o Bladawirze i pokazała Orbiter i starcie między Ewą Razalis a Antonim Bladawirem w najgorszym możliwym świetle.
* Leszek Kurzmin: dąży do udowodnienia błędów w podejściu Bladawira; chce pomóc Ewie Razalis bo NIE ZGADZA SIĘ z wartościami Bladawira. Wspiera Ariannę.
* Dorota Radraszew: lekko zrezygnowana (wie że pracuje dla magów i nie da się nic zrobić z Infernią) oficer zapoatrzeniowa Inferni; gdy miała poznać plan Razalis, odwiedziła Karsztarin i co prawda została ciężko pobita ale doszła do tego, że ktoś 'leakował' plan Bladawira co dało Klaudii i Ariannie kluczowe fakty.
* OO Karsztarin: przekształcony w 'fortecę Aureliona', zmieniony w obiekt niemożliwy do sforsowania. Bladawir nie próbował - porwał komodor Razalis inaczej.
* OO Paprykowiec: znów okręt flagowy Arianny, spełnił się w tej roli doskonale, nie robiąc nic wielkiego XD

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Pierścień Zewnętrzny
                1. Stacja Medyczna Atropos: lokalizacja przypadków ciężkich, w czym ofiar Korony Woltaren (terrorforma)

## Czas

* Opóźnienie: 5
* Dni: 7
