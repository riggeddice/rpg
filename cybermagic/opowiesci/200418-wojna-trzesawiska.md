## Metadane

* title: "Wojna Trzęsawiska"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [200417 - Nawoływanie Trzęsawiska](200417-nawolywanie-trzesawiska)

### Chronologiczna

* [200417 - Nawoływanie Trzęsawiska](200417-nawolywanie-trzesawiska)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* Kontroler Pierwszy robi bombardowanie orbitalne Trzęsawiska
* Sporo ludzi umiera

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Punkt zero

.

## Misja właściwa

Dwa dni później, noc.

Pięknotka dostaje sygnał alarmowy. Terminusi w Czemercie wysłali sygnał SOS. Atak nieznanych magów? Czegoś. Pięknotka wypada z łóżka i dostaje informację od Gabriela że jej pomoże. Zaspanego Gabriela. Gabriel informuje Pięknotkę, że sporo punktów się zapaliło tej nocy. Wszystkie mają część wspólną - są graniczne z Trzęsawiskiem.

Pięknotka leci w tamte okolice i ściąga raport sytuacyjny. Nie wygląda to fajnie - siły atakujące Czemertę są skupione na fabrykatorach sprzętu ciężkiego i magitechów. Do tej pory Trzęsawisko było dla Czemerty tłem; źródłem w miarę bezpiecznej energii. A teraz wyraźnie te dziwne siły atakujące mentalnie i sonicznie skupiają się na pozyskaniu magitechów. Standardowy układ pnączoszponów i innych trzęsawiskowych bytów - z dodatkiem nowej bioformy. Jeszcze nie została zidentyfikowana; terminusi są w defensywnie. Terminusi ratują cywilów - soniczno-mentalne ataki tych "syren" sprawiają, że ludzie chcą do nich iść.

Terminusi przez to się bardziej skupiają na ratowaniu ludzi niż czymkolwiek innym. Pięknotka dostała informację, że część istot zabiera magitechy na Trzęsawisko. Pięknotka odpowiedziała, że prawie tam jest.

Pięknotka z Gabrielem wpadli awianem na miejsce - prosto na flankę bioform. Pięknotka skupiła się na stworzeniu najpotężniejszej kuli feromonowej i zawieszenie jej dookoła awiana - chce wyciągnąć wszystkie cholerne bioformy i skupić je na awianie, niech przerwą operację. A Gabriel jej katalitycznie pomaga. MUSI im się udać!

ExZM+2: (11v11: XV): 

* V: Pięknotka odblokowała terminusów; kupiła im czas na ratowanie ludzi
* Vm: Pięknotka ściągnęła bioformy w swoją stronę
* X: nowa bioforma jest z tym niekompatybilna; nienawidzi tego zapachu
* V: Pięknotka zatrzymała działania innych bioform; wszystkie ściągają do awiana

Gabriel pilotuje awiana; jest pod ostrzałem sonicznym i mentalnym. Pięknotka otwiera ogień ze snajperki by nie mogły łatwo trafić ani się przymierzyć. TrZ (6+2v8):

* V: awian nadal ściąga potwory
* X: awian jest uszkodzony; ma ograniczony lot (uszkodzone paliwo)
* V: awian jest kontrolowany
* V: Gabriel może potwory ściągać

Nie jest to sytuacja całkowicie idealna, bo ma uszkodzone paliwo, ale kupuje czas w którym terminusi robią swoje. Pięknotka zauważyła w co strzela - nowe bioformy, pajęczokształtne, wzorowane na Sabinie Kazitan. Pięknotka doszła też do tego, że atak mentalny to albo wabienie albo - jeśli ma być niszczycielski - nienawiść.

Pięknotka zauważyła też, że nie ma bardzo wielu tych bioform w grupie. Pięknotka i Gabriel kupują czas, by terminusi dali radę odzyskać kontrolę:

* X: magitechy dostały się na Trzęsawisko
* X: bioformy w większości się wycofały bezpiecznie
* X: awian jest zmuszony do crashlandingu
* V: Pięknotka kupiła dość czasu by terminusi odzyskali.

Gabriel jest ranny, solidnie. Pięknotce nic nie jest. Nawet zadraśnięcia. Terminusi dotarli do Gabriela i Pięknotki na czas. Powiedzieli, że Trzęsawisko zawsze było spokojne... a tu taki atak, skoordynowany. Terminusi - oczywiście - winią Wiktora Sataraila. Ale dlaczego? Kto go sprowokował, czym i po co? Pięknotka zmilczała; nie chce niczego mówić.

Pięknotka i Gabriel wrócili zwykłym łazikiem do Pustogoru - tym razem Pięknotka prowadzi. Gabriel jest nadal w szoku. Pięknotka nie - widziała już kiedyś jak Trzęsawisko powstało do walki. Wtedy zostało zatrzymane przez bombardowanie orbitalne Kontrolera Pierwszego...

Jak tylko wrócili do Pustogoru, Pięknotka sprawdziła sytuację w Barbakanie. Tak - złapali "żywcem" jedną z nowych bioform. Trzewń potwierdził - wzorowana na Sabinie Kazitan, co daje +100 nienawiści wszystkich terminusów do tej arystokratki...

Potwierdziła się jeszcze jedna informacja - nowe bioformy ściągają LUDZI na Trzęsawisko. Karla ustawiła alarm czerwony; ludziom nie wolno tam się zbliżać. Ale kilkadziesiąt osób już zostało "porwanych". Zdaniem naukowców z Laboratorium Senetis, nowa bioforma jest "klonem" lub "droną" bardziej zaawansowanej bioformy - i tamta bioforma może mieć większy intelekt. Ale drony nie.

Karla poprosiła Pięknotkę do siebie. Powiedziała, że już poinformowała Kontroler Pierwszy o potrzebę bombardowania z orbity. Ostatnio gdy Kontroler strzelał to magiczne pole pękło i Pustogor się musiał regenerować. Karla chce zlokalizować bioformę bazowaną na Sabinie Kazitan i zestrzelenie jej Kontrolerem Pierwszym; jeśli będzie strzelać na ślepo to niestety może być bardzo źle z polem magicznym.

Pięknotka zauważyła, że jeśli inkarnacja jest bazowana na Sabinie Kazitan, może mieć jej wady. Poprosiła Karlę o autoryzację do użycia Senetis - może uda się jej to cholerstwo oćpać. Plus, porozmawia z Wiktorem Satarailem. Karla wyraziła zgodę.

Po rozmowie z Karlą Pięknotka udała się do Szpitala Terminuskiego, spotkać się z Sabiną. Sabina powiedziała Pięknotce, że Myrczek czyta jej opowieści optymistycznej czarodziejki. Na pytanie Pieknotki czy ta ma wziąć Myrczkowi książkę, Sabina powiedziała że nie. Póki jej nie miał opowiadał jej o grzybach. Przez godziny. Szkoda, że się już jej nie boi. Jemu jest JEJ żal. Ze wszystkich ludzi i magów. Jej. (Ok, bardzo ciężko dostała przez odpięcie jej od Trzęsawiska).

Zapytana jaka jest jej słabość Sabina odpowiedziała że magowie Aurum - i Krwawy Lotos. To jest silny halucynogenny narkotyk, działający tylko na magów, powodujący niesamowite halucynacje i wizje. Sabina nie umie mu się oprzeć.

Pięknotka wzięła najcięższy sprzęt jaki kiedykolwiek miała i poszła odwiedzić Sataraila na Trzęsawisku...

Pięknotka jest w pancerzu bojowym; towarzyszy jej dwóch terminusów mających robić dywersję. Pięknotka spytała co oni Karli zrobili. Zobaczyła tylko krzywe uśmiechy. Trudno, nic się nie poradzi. Pięknotka wbija się jak najbliżej miejsca, gdzie można czasem spotkać Wiktora. Deep raid.

(do wysłania sygnału do Wiktora, Pięknotka potrzebuje 8 * V. By dotrzeć jak najbliżej, może dostać do VVVVV. Dla Pięknotki to Trudny Zasobowy. Pięknotka wysyła dronę przodem które ma za zadanie odciągnąć jak najwięcej stworów - daje jej to +2).

Na początku idzie w miarę dobrze - drona skutecznie odciągnęła sporo potworów. Acz Trzęsawisko pulsuje energią; Pięknotka porusza się wolniej i ostrożniej. Terminusi też poruszają się niepewnie. Pięknotka zauważa zupełnie inne roślinne Skażenie. Trzęsawisko ożyło inaczej. (VV). Pięknotka omija wszystkie potencjalne zagrożenia - drona niestety przestała działać i potwory ruszyły. Terminusi zaczęli odciągać potwory od Pięknotki, która idzie w głąb (XV). Terminusi są głośni, ale wycofują się (lekko panicznie) w kierunku na granicę. Pięknotka wchodzi jeszcze głębiej, korzystając z tego, że jej energia jest niewidoczna. (V). Dotarła w miarę bezpiecznie, choć bardzo ostrożnie do Laboratorium W Drzewie - chatki Wiktora Sataraila (V).

Dookoła Pięknotki zaczęły wić się pnączowęże i pnączoszpony. Wiktora wyraźnie nie ma. Pięknotka potrzebuje czasu. Pięknotka używa magii, by okryć się energią Trzęsawiska, by wyglądać jak ono, by potwory Trzęsawiska nie zobaczyły jej tutaj i żeby wysłać Wiktorowi sygnał, że ona tu jest i czeka (TrMZ: V). Pnączoszpon podpełzł do Pięknotki by ją powąchać; nie wyczuł w niej obcego Trzęsawisku maga (V). Chatka ją rozpoznała i pnączoszpon poczuł tą akceptację; nie miał z tym kłopotu. (X) Nic dziwnego, bo zostawiła power suit za sobą; nie ma na sobie pancerza, może liczyć na Cień. Odrzuciła go po drodze mając nadzieję, że może do niego wrócić. (V) I faktycznie, czekała w chatce Wiktora aż on powrócił, w nietypowej nawet jak na nią harmonii z Trzęsawiskiem.

Po prowrocie, Wiktor uniósł brew i lekko zdziwiony zrobił Pięknotce herbatę. Pięknotka z przyjemnością ją przyjęła. Wiktor zauważył, że coś się wydarzyło. Pięknotka potwierdziła - Trzęsawisko przypuściło atak, i to niejeden. Wiktor potwierdził - Trzęsawisko szuka. Jak ją znajdzie, to się zakończy. Pięknotka spytała po co sprzęt ciężki i magitechy. Wiktor odpowiedział - nauczyło się. Magowie tego używają. Więc Trzęsawisko może próbować spoić bioformy z technomagią.

Pięknotce się wcale ta odpowiedź nie spodobała.

Wiktor poprosił Pięknotkę o oddanie Sabiny. Jej uszkodzona wersja weszła w Trzęsawisko. Jest niesprawna. Trzęsawisko się chce jej nauczyć i nie potrafi. Pięknotka powiedziała, że Sabina jest zbyt przerażająca by stać się częścią Trzęsawiska. Wiktor zauważył, że Pustogor nie ma większego wyboru. Pięknotka powiedziała, że można usunąć ją z zasięgu. Po namyśle, Wiktor powiedział, że to zadziała.

Pięknotka nie zgadza się, by oddać jej krew Wiktorowi. Ta czarodziejka jest zbyt niebezpieczna. Nie chce tak stabilizować Trzęsawiska. Wiktor powiedział jej, że sam może ją przecież złapać poza Trzęsawiskiem. Pięknotka powiedziała, że to zły pomysł. Zależy jej na Wiktorze - nie chce, by on się zintegrował z tym czym jest Sabina. Wiktor nie ma z tym problemu. Ona jest silnym Wzorem, poradzi sobie.

Tak czy inaczej, stanęli na tym, że się nie zgadzają. Wiktor spytał, czemu Pięknotka tu jest. Pięknotka prosi Wiktora o to, by jednak nie zmieniać Trzęsawiska przy użyciu Sabiny. Ona potrzebuje czegoś innego - zarówno Sabina, jak i Pustogor. Pięknotka weszła tu w harmonii, nie robiąc nikomu i niczemu krzywdy - to się u Wiktora liczy (TrZ+2).

* V: Wiktor zgadza się, że nie musi polować na Sabinę. Ale potrzebuje innej istoty która pomoże Trzęsawisku na dokończenie nowej bioformy. Pięknotka mu w tym pomoże.
* X: Wiktor jest absolutnie przekonany, że nowa bioforma musi powstać.
* X: Pięknotka sama czuje imbalans; jest to potrzebne, by bioforma powstała i Trzęsawisko będzie szukać aż znajdzie sposób - nawet jeśli stworzy Lewiatana.
* V: Wiktor nie potrzebuje Elizy Iry, potrzebuje komponentów magów, istot, i sam dojdzie do tego jakich i w jakiej kolejności.
* X: Trzęsawisko się NIE uspokoi, dopóki nowa bioforma nie powstanie
* X: Karla nie odpuści bombardowania orbitalnego póki Trzęsawisko się nie uspokoi.
* V: Wiktor zgodził się pomóc z ewolucją by trochę lepiej ukierunkować moce mentalne nowej bioformy.

Wiktor powiedział Pięknotce, że będzie w stanie rozpocząć pracę, jeśli ta dostarczy mu trochę krwi czarodziejki którą zna pod imieniem Serafina Ira. Bo to zawiera matrycę rodu Ira oraz zawiera możliwości osłabienia Trzęsawiska pod kątem Sabiny Kazitan.

Wiktor zapowiedział też Pięknotce ze spokojem, że jeśli choć jeden strzał artyleryjski z Kontrolera Pierwszego trafi w Trzęsawisko, mamy gorącą wojnę między nim i jego siłami a Szczelińcem. To nie jest wojna - zdaniem Wiktora - którą Pustogor może wygrać. Niekoniecznie przegra, ale nie wygra. Jeśli Karli się to nie podoba, niech pomoże Pięknotce i pomoże dostarczyć Serafinę Wiktorowi.

Pięknotka opuszczała Trzęsawisko, ale wyczuła... coś dziwnego. Emanację. Pięknotka poczuła TO, co stanowi TEGO potwora. Matkę Roju. Prototyp. Przez głowę Pięknotki przeszło: może próbować ją sama zabić, może próbować przekazać koordynaty do Karli (i Kontrolera Pierwszego), ale dla Pięknotki ważniejsze jednak są relacje z Wiktorem... chyba.

Pięknotka zdecydowała się podejść do nowej bioformy i ją zobaczyć. Ma w końcu pozwolenie Wiktora na bycie na Trzęsawisku. (TrZ: 9v8: VV). Pięknotka podeszła nową bioformę. Nowa bioforma wygląda jak skrzyżówanie Sabiny z driadą i pajęczakiem. Wyraźnie jest częściowo zrobiona z drzewa. Ale nie da się pomylić co jest oryginałem - cechy charakterystyczne po prostu tam są. Istota dogląda jaj. Pnącza jej już pomagają. Jej fizyczne adaptacje pomagają jej w poruszaniu się po Trzęsawisku. Pnączoszpony na nią nie polują. Jest to istota Trzęsawiska. Jednocześnie Pięknotka wyczuwa Umysł tej istoty - ta istota faktycznie ma formę magii mentalnej, ale pasywnej. A magia idzie w kierunku na przyzywanie (jak syreny) i koszmary (nienawiść). Po prostu idealnie...

Pięknotka wycofuje się z Trzęsawiska - od razu idzie do Karli. Regentka Pustogoru czeka na raport. Pięknotka powiedziała jej, że Wiktor uważa bombardowanie orbitalne za akt otwartej wojny i trzeba rozwiązać to inaczej. Karla nie jest szczęśliwa. Szczęśliwie, Pięknotka załatwiła, że Wiktor nie będzie polował na Sabinę - ale oczekuje krwi Serafiny Iry. Karla spojrzała na Pięknotkę lekko dziwnie. Serafina nienawidzi Pustogoru i jest w Enklawach. Karla musiałaby wysłać raiding party.

(TrZ+3)

* V: Karla wyśle raiding party do Enklaw i pozyska Serafinę.
* V: Karla nie zbombarduje Trzęsawiska z orbity; wpierw pozwoli planowi Pięknotki i Wiktora się zmaterializować.
* X: Jednak przez to wszystko będą straty, nawet ofiary. Trzęsawisko jest aktywne. I niestety, nie ma dość sił terminuskich by to wszystko utrzymać.

A Pięknotka będzie zajechana przez następne cztery dni...

## Streszczenie

Trzęsawisko odpowiedziało na atak tworząc nową bioformę na bazie Sabiny Kazitan; zaczęło przyciągać ludzi do siebie. Z uwagi na wysokie niebezpieczeństwo Karla chciała zbombardować Trzęsawisko z orbity, jak kiedyś. Pięknotka zrobiła kanał negocjacyjny Pustogor - Wiktor Satarail; Wiktor pomoże Pięknotce uspokoić Trzęsawisko, ale nie będzie bombardowania z orbity. Plus, Pustogor pomoże mu sformować nową bioformę...

## Progresja

* Sabina Kazitan: stała się dużo bardziej rozpoznawalna w Szczelińcu przez anomalie Trzęsawiska. BARDZO nielubiana przez to.

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: wpierw broniła Czemerty przed bioformami Trzęsawiska (przez kłąb feromonów ściągających) a potem przekradła się po Trzęsawisku by negocjować z Wiktorem Satarailem.
* Gabriel Ursus: walczył w obronie Czemerty przed bioformami Trzęsawiska; został ranny jako pilot awiana.
* Karla Mrozik: w niefortunnym położeniu - chce chronić ludzi (bombardowanie z orbity) i chce chronić teren (nie dotykać Trzęsawiska). Decyduje o negocjacji z Satarailem.
* Ignacy Myrczek: przestał się bać Sabiny Kazitan; gdy ona leży z nim w szpitalu to czyta jej książki i opowiada o grzybach.
* Sabina Kazitan: leży w szpitalu z Ignacym Myrczkiem; próbuje nie dać się zwariować od choroby i od opowieści o grzybach.
* Wiktor Satarail: zapowiedział wojnę jeśli będzie bombardowanie Trzęsawiska z orbity, pomoże uspokoić Trzęsawisko jeśli Pustogor pomoże mu ze zbudowaniem bioformy na bazie Sabiny.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Czemerta: atak bioform z Trzęsawiska celem zdobycia sprzętu ciężkiego (traktory itp) odparty przez 2 lokalnych terminusów, Pięknotkę i Gabriela
                        1. Trzęsawisko Zjawosztup: uaktywnione i naergetyzowane, próbuje stworzyć nową bioformę na bazie Sabiny Kazitan; poluje na ludzi
                            1. Laboratorium W Drzewie: nadal rozpoznaje Pięknotkę Diakon; znalazła tam schronienie niż Wiktor wrócił

## Czas

* Opóźnienie: 2
* Dni: 5
