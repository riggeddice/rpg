## Metadane

* title: "Kiepski altruistyczny terroryzm"
* threads: triumfalny-powrot-arianny
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [200106 - Infernia i Martyn Hiwasser](200106-infernia-martyn-hiwasser)
* [200115 - Pech komodora Ogryza](200115-pech-komodora-ogryza)
* [180701 - Dwa rejsy w potrzebie](180701-dwa-rejsy-w-potrzebie)
* [220720 - Infernia taksówką dla Lycoris](220720-infernia-taksowka-dla-lycoris)

### Chronologiczna

* [200106 - Infernia i Martyn Hiwasser](200106-infernia-martyn-hiwasser)

## Uczucie sesji

* .

## Punkt zerowy

.

### Postacie

.

## Misja właściwa

### Scena zero

Królowa Chmur jest luksusowym statkiem pasażerskim Luxuritiasu. Podczas jednego ze standardowych, spokojnych dni okazało się ku zdziwieniu oficera łącznościowego, że coś się stało na pokładzie Kryształowym - tam, gdzie normalnie nie ma wstępu. Okazało się też, że to PERSEFONA tego statku stanęła przeciwko Królowej Chmur. Jest to sytuacja praktycznie niemożliwa; psychotronik dyskretnie doszedł do tego, że na statku pojawił się tajemniczny napastnik. Coś przebiło się militarnie przez osłony i tam jest ktoś kto rozbija to co tam się dzieje.

Martauron przybył na Królową Chmur, wraz z Emilią, ratując ludzi do których strzelają noble.

Łącznościowiec dał radę uszkodzić Persefonę oraz zniszczyć mapy eteryczne; Królowa nie skoczy nigdzie dalej. Ale Persefona obróciła się przeciw statkowi. Emilia jakoś przejęła kontrolę nad statkiem - ale JAK?

### Sesja właściwa

Niedaleko Królowej Chmur znajdowała się Infernia. Infernia jest statkiem zwiadowczym i naukowym - jest szybka i ma koloidowy pancerz. Infernia zbliżyła się do Królowej Chmur maksymalnie niezauważona i wprowadziła na statek trójkę agentów - Klaudię, Natalię i Eustachego. W pogotowiu czeka Martyn a Arianna jest gotowa przebić przez pancerz Królowej by tylko ratować swoich magów. Tak więc akcja jest bezpieczniejsza niż się wydaje (hahaha, nie jest)

* Zespół na Królowej jest ukryty, nie da się ich łatwo znaleźć
* Wylądowali w sektorze ogrodów; tam była ostra walka i ogrody są martwe
* Co prawda udało się Klaudii podmienić rekordy że oni mają prawo tam być, ale wszyscy mają być w kajutach. Persefona nie powinna o nich wiedzieć.
* Są ślady walk i ruchu oporu. Są patrole. Ogólnie, ciężka sprawa.

Eustachy przygotował przedtem odpowiednie narzędzie - bomba dysruptywna. Jego zadaniem było po prostu poważnie uszkodzić TAI by Persefona straciła wizję kontroli nad statkiem. Bardzo trudno walczyć przeciwko statkowi wraz z aktywną militarną TAI, zwłaszcza jak na pokładzie jest tajemnicza terrorystka.

* Persefona jest disruptowana i oślepiona. Nie kontroluje statku.
* Królowa jest w trybie 'clam mode'. Bunkier.
* Ruch oporu strzela się z robotami Persefony.
* Aktywne patrole sił Persefony.

Dobrze, udało się. Statek jest w ostrej fazie bojowej - ale nie rozwiązuje to problemu. Szczęśliwie, z lekko szaloną Persefoną da się coś zrobić; Klaudia użyła potężnego rytuału mającego na celu połączyć Natalię z Persefoną, nawiązanie rozmowy by Natalia mogła coś się dowiedzieć i coś zrobić z tym wszystkim.

* link Persefona - Natalia się udał.
* w linku jest też Emilia. Persefona - Emilia - Natalia.
* Emilia ani Natalia nie mają jak się wycofać z Persefony. A Persefona ma 10h życia, potem ostatecznie umrze zabierając dziewczyny ze sobą

Z rozmowy Natalia wyciągnęła sporo ciekawych informacji:

* Emilia **nie** jest dominującą siłą w tym związku. To Persefona ją pociesza i pomaga. Emilia nie do końca wie co robi.
* Persefona ukrywa swoją śmierć przed Emilią.
* Persefona blokuje przejęcie kontroli przez Emilię, by nie dopuścić do śmierci ludzi ani nobli. Persefona, zgodnie z programowaniem, CHRONI.
* Bogacze robili krzywdę ludziom o których nie wiedziała Persefona; Persefona stanęła po stronie Emilii.
* Tak więc jak na terroryzm to troszkę nie wyszło tak jak Luxuritias myślał
* Persefona chciała skoczyć gdzieś przez Bramę; Emilia jest KOS i wszyscy ludzie też będą. A teraz nie skoczy, nie ma jak.

Dodatkowo, przez przypadek lub specjalnie, Natalia stworzyła nowy strain Persefony; strain, który jest lojalny Luxuritiasowi i samej Natalii (grupie ratunkowej). Przekonała Emilię, żeby ta się wstrzymała z próbami zemsty czy zniszczenia nobli. Zamiast tego - jak wszystkich stąd wydostać czy uratować. I nagle sama Natalia jest skonfliktowana - stoi po stronie Emilii, ale jednocześnie nie może dopuścić do śmierci tych wszystkich ludzi i magów!

Po pewnym kombinowaniu Natalia, Eustachy i Klaudia sformowali nowy plan. Wrobić Persefonę 2 w to, że udało się wszystkich napastników i ludzi (łącznie z Emilią) zastrzelić. Niech Persefona 2 sama jest tą która strzela. Persefona 1 jest na poziomie rozpadu mentalnego, nie kontaktuje już w ogóle. Eustachy przeszedł się po Królowej Chmur i przesterował działka tak, by Persefona 2 myślała, że wszystko zniszczyła bezbłędnie. Arianna zapewniła odpowiednie sygnatury z pomocą Klaudii a Natalia przekonała nowo stworzoną TAI do tego planu.

Emilia pomogła z ewakuacją ludzi i udało się zamaskować ucieczkę Emilii i ludzi.

PRAWIE.

Bo zanim do tego doszło, trzeba rozmontować Natalię i Emilię z Persefony 1. Martyn był siłą, która to zrobiła - przyleciał na Królową Chmur za zgodą obu TAI i pomógł Natalii. Ale Emilia się wycofała; uciekła do ogrodów. Nie chce zostawiać Persefony 1 samej i strasznie, strasznie nie ufa Zespołowi ani nikomu. Persefona 1 się rozpada, Emilia ma w niej przyjaciółkę i nie pozwoli by ta sama umarła. Chroniona przez Martaurona, cóż - jest to problematyczne.

Klaudia odpaliła najtrudniejszy rytuał w historii. Niech Persefona 1 się zintegruje z Martauronem. Niech dzięki temu Emilia nie zginie. I jej się udało! Martauron nabył strategiczne i taktyczne umiejętności Persefony. Trudno wyobrazić sobie coś groźniejszego.

I oni wyruszyli z tymi wszystkimi ludźmi jednym ze statków nobli...

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Emilia z Martauronem zdobyli statek kosmiczny Luxuritiasu, Królową Chmur. Infernia - zamaskowany statek - pomogła go odzyskać Luxuritiasowi. Twist polega na tym, że załoga Inferni POMOGŁA Emilii ukryć wszystkich ludzi i ich ewakuować - by Luxuritias niczego nie wiedział. Sercem Infernia jest za Emilią, profesjonalnie pomaga Luxuritiasowi.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Klaudia Stryk: badacz otchłani (wreszcie!); przeprowadziła dwa chore rytuały, linkując Natalię z Persefoną i integrując umierającą Persefonę z Martauronem.
* Eustachy Korkoran: inżynier zniszczenia; wpierw uszkodził Persefonę a potem doprowadził do tego, że Persefona 2 myślała, że zniszczyła napastników.
* Natalia Miszryk: inspirator; przekonała Emilię i Persefonę do spokojnego rozwiązania konfliktu a potem DRUGĄ Persefonę do swojego planu. Ryzykowała życiem.
* Arianna Verlen: dowódca Inferni, zapewniła efektowne zniszczenie "roboczego statku" by Persefona 2 na Królowej Chmur była przekonana o wykonaniu zadania.
* Emilia Kariamon: zintegrowana z Martauronem była Emulatorka; przeprowadziła atak terrorystyczny na Królową Chmur by ratować ludzi. Udało jej się, dzięki załodze Inferni.
* Martauron Attylla: gladiator i aparycja Emilii; zintegrowany z Persefoną uzyskał jej umiejętności taktyczne i strategiczne.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Kosmos
                1. Brama Trzypływów: gdzie zatrzymała się Królowa Chmur Luxuritiasu po interwencji Emilii i Martaurona by ratować ludzi.

## Czas

* Opóźnienie: 74
* Dni: 2

## Inne

### Co warto zapamiętać

Stopnie trudności. Trudne i Ekstremalne, ani jednego Typowego. Stąd tak złożone wyniki. Świetnie wyszły aspekty eksploracji.
