## Wyjaśnienie

Dokument jest jedynym NIEJAWNYM dokumentem jaki mamy. To próba zrozumienia co się tu dzieje przez Żółwia.

## Dotyczy

* Lokalizacja
    * Szczeliniec
* Frakcje
    * Mafia
    * Serafina
    * Rekiny
    * Terminusi / Pustogor

## Sesje, wątki, linie chronologiczne

* START: 200202-krucjata-chevaleresse
    * tutaj pojawiła się Malictrix i to zniszczyło Grzymościa.
* Marysia Sowińska: 220802-gdy-prawnik-przyjdzie-po-rekiny
* Mimoza Diakon: 220809-20-razy-za-duzo-esuriit

## 1. Wątek - Dysonans Diskordii

### 1.1. Sytuacja - przesłanki

* Marsen się pojawił, by wywołać chaos i podnieść Lorenę. Ale dlaczego uważa, że Lorena jest zagrożona? KTO mu dał ten pomysł?
    * Marsen nie lubi Eterni, ale skąd to zacietrzewienie?
* Ktoś wykorzystał plan Marsena, by Flara Astorii się pojawiła. Ale KTO chce wydobyć skorodowanego przez Esuriit hovertanka klasy Carcharien?
    * Iwona podkochuje się w Danielu, ale czemu uważa, że poradzi sobie z Carcharienem?! I to Esuriitowym?!
* Ktoś zachęca Rekiny do destabilizacji i zachowań niebezpiecznych z perspektywy osób dookoła. Ale PO CO PRAWNIK? On nie ma szansy wygrać. To może tylko osłabić Marysię.
    * Ok, to co stało się Lilianie rozwścieczyło AMZ, ale prawnik nie ma szans wygrać.
* Silna destabilizacja rzeczy dookoła Rekinów (kult kralotyczny?!)
    * Ok, Rekiny sobie szaleją, ale skąd dostęp do takich rzeczy i spraw??

### 1.2. Analiza

Ktoś zyskuje na chaosie i destabilizacji wszystkiego. Odpowiedź jest jednoznaczna - Serafina Ira.

* Pustogor i Orbiter zatrzymały ją przed ekspedycją ratującą jej przyjaciół przed Pacyfiką.
    * Pustogor musi zostać zmieniony. Aktualna formuła "only the obedient survive" się nie sprawdza - ona dowodem.
* Pustogor zmiażdżył siłę, która kontrolowała Serafinę - Grzymościa (po upadku Kajrata).
    * Jest luka w przywództwie i w systemach.
* Rekiny krzywdzą lokalną ludność a Pustogor to ignoruje (bo nie ma sił i chce sojuszu z Aurum).
    * Sytuacja z Pacyfiką się powtarza...

Serafina jest idealistką chorego typu. Chce zniszczyć to, co istnieje by na tego miejscu zbudować coś lepszego.

Jakie siły ma Serafina?

* Mistrzyni PR i żądlących słów. Wie, co powiedzieć.
* Niewielki podzbiór mafii; część mafiozów zorganizowała się dookoła Serafiny która ma soniczne moce i kontroluje umysły.
* Disgruntled people, disgruntled Sharks, disgruntled terminus.

Co najbardziej staje na drodze Serafiny?

* Tymon Grubisz. Lokalny terminus, który monitoruje sytuację z pomocą ukochanej Eszary. Niemożliwy do pokonania, ale da się zaatakować go społecznie.
* Alan Bartozol. Terminus artyleryjski, zaczął interesować się Rekinami widząc, że tam coś się da sensownego zrobić z nimi.
* Mimoza Diakon. Próbuje pomóc terenowi, ale przez to jedynie umacnia struktury władzy i sprawia, że 

Ale co stanowi wsparcie taktyczne Serafiny? Na razie nie wiem. Ktoś z Syndykatu Aureliona?

## 2. Wątek - Poświęcenie Amelii

### 2.1. Sytuacja - przesłanki

* Amelia Sowińska była kochana przez Rekiny i znienawidzona przez miejscowych. Duchy pomordowanych tworzące mandragorę oskarżyły Marysię o bycie Amelią. <-- 210824-mandragora-nienawidzi-rekinow
    * Amelia jest super-administratorką i wzięła wszystko na siebie by działało, optymalizując 
    * Widząc eskalujący konflikt Morlan - Jolanta Sowińska wprowadziła siebie na dwór Morlana by neutralizować Jolantę. Bo działania Jolanty źle rzutują na ród Sowińskich, ale też po to, by zrozumieć gdzie zrobiła błąd z Rolandem. Jak mogłaby to naprawić. I jak zemścić się na Oliwii.

Rdzeń informacji z "210921-przybycie-rekina-z-eterni":

MARYSIA -> LUCJAN

"Hej, udało ci się coś ustalić?" Lucjan szukał, dyskretnie, co tu się stało.

TrZ+3+3O (on się dowie):

* Vz: Lucjan dowiedział się, że za tym stoi Amelia bo jest zaufany z rodu.
* X: Amelia wie, że Lucjan pytał dla Marysi.
* V: Eternianin nazywa się Ernest Namertel, jest dość "niegroźny" z charakteru. Jest powiązany z Morlanem pośrednio. Jest młodym magiem, którego Morlan swego czasu mentorował, więc jest niezły w walce.
* Xz: Amelia uważa, że tego typu przeszukiwania mogą zakłócić delikatny balans. Jest wściekła na Marysię że wysłała "gorylowatego Lucka" na przeszpiegi.
* V: Amelia próbuje deeskalować problemy spowodowane przez Jolantę. 
    * Zaproponowała wymianę (żeby lepiej się wszyscy zrozumieli) - ona jedzie do Eterni, do Morlana. A on przyśle kogoś do Aurum. 
    * I Eternia przysłała - ale ku zdziwieniu wszystkich - na prowincję. Morlan chciał, by Ernest trafił nie do Aurum a do Szczelińca. Nikt nie wie czemu.
    * Aurum uważa, że Ernest jest wysłany po to, by coś mu się stało. By Morlan miał pretekst. Czyli - lepiej, by nic mu się nie stało.
    * Wśród Sowińskich są osoby, którym nie przeszkadza, że Amelii coś się stanie ;-). Więc może utrata Ernesta na prowincji i utrata Amelii w Eterni nie jest taka zła ;-).

AMELIA żąda kontaktu z MARYSIĄ. Złotowłosa Amelia, z lokami, NIGDY nie jest uśmiechnięta. Marysia z przyjemnością porozmawia z Amelią...

* Marysia wyszła od "mogliśmy zrobić mu krzywdę"
* Amelia się uśmiechnęła "spróbujcie :-)"
* Marysia: "Amelio, dzięki Tobie się dużo nauczyliśmy. Szuka Cię Serafina, wiesz, ludobójstwo parę lat temu..."
* Amelia blankuje. "A, tamto. Mogliby już sobie darować. To było dawno."

Co Marysia może się dowiedzieć czego Amelia NIE CHCE jej powiedzieć (clues, secrets):

ExZ+4:

* V: Co może Amelię ZRANIĆ: Ernest nie ma pojęcia jakim ziółkiem jest Amelia. Morlan też tego nie wie. Jakby Ernest się dowiedział, że Amelia jest odpowiedzialna za coś takiego, to może być "ciekawiej".
* (+3Or za szantaż) 
* XXz: Amelia robi coś ważnego dla rodu. Marysia jest "lol rekin". Amelia ma delikatną pozycję, więc lepiej niech Marysia nie robi głupot. Bo może uszkodzić Amelię - i wyjątkowo uszkodzi to tak samo ród i za to Marysi nie podziękują.
* X: Marysia jest osobą, która musi zapewnić że sekret Amelii nie wyjdzie przed Ernestem.
    * + Amelia nie wiedziała, że Marysi nie powiedzieli o tym, że tu przybędzie Ernest. Amelia wysłała sygnał. Marysia miała wiedzieć tydzień temu.
* (+2Vz na zachętę) V: Amelia była osobą, która przekonała Ernesta, by przybył tu, na prowincję. Bała się, że:
    * Ernest zobaczy jakie jest Aurum i wzgardzi jej rodem bardzo mocno.
    * Ernestowi coś się stanie. Albo bo "krzywdę Amelii" albo "Eternia jest zła".
    * Np. że Jolka go porwie, coś mu zrobi, przeczyta pamięć... w queście vs Morlanowi
* V: Amelii **personalnie zależy** na Erneście. I on ją też lubi. To potencjalny mezalians (dla Amelii). To może jej zrobić straszną krzywdę.
* X: Amelia zadba o to personalnie (z sukcesem), by Marysia odpowiadała za bezpieczeństwo Ernesta na tym terenie przed Sowińskimi. W końcu jest jedną z nich ;-).
    * + Amelii wymsknęło się. Ona wzięła to na siebie z mandragorą. Ale ona tego nie zrobiła. Amelia chroniła kogoś.

Amelia kocha Ernesta. Ale straciła go przez machinacje Marysi.

### 2.2. Analiza

Amelia Sowińska zawsze grała drugie skrzypce. Nigdy nie była najlepsza, nigdy nie była doceniana. Jak jeden raz miała coś pięknego - Ernesta - musiała priorytetyzować Jolantę Sowińską i jej chore machinacje. Ernesta przekierowała z Aurum (gdzie coś mogłoby się mu stać) do Rekinów (gdzie jest bezpieczny). I nawet TAM straciła go. Przez chore machinacje Marysi.

Amelia. Chce. Ernesta. Poza tym, że teraz to już serio nienawidzi Marysi.

* Torszecki się chce pozbyć Ernesta a Amelia Marysi. Oboje chcą rozerwać ten związek. Naturalny sojusz Amelia - Torszecki.

## 3. Wątek - Kocioł Rekinów

### 3.1. Sytuacja - przesłanki

z 220802-gdy-prawnik-przyjdzie-po-rekiny:

* Keira, agentka Ernesta, przechwyciła z pustego Apartamentu grupę kultystek Ośmiornicy. A przynajmniej tak to wygląda.
    * Keira + Ernest mają przerażającą opinię wśród Rekinów (Keira is a menace); nikomu to nie przeszkadza.
    * Wszystkie osoby przechwycił Ernest z Apartamentu, m.in. Melissę - "należącą" do Chevaleresse.
* W sprawę zamieszana jest najbardziej "samotna" elegancka dama Diakonów, Mimoza Elegancja Diakon, która nie ma NIC wspólnego z Karoliną.
    * Mimoza miała jakieś działania związane z kultystkami, a przynajmniej na to wszystko wskazuje. Typowe na Diakonkę, nietypowe na Mimozę.
* TAI Hestia jest Skażona ixionem - i jest sojuszniczką Marysi
    * Przeniesienie ixiońskie Marysia -> Hestia (przez kontakt z Marysią Hestia staje się bardziej jak Marysia).
    * Hestia jest aktywną sojuszniczką Marysi i chce, by Ernest x Marysia a nie Ernest x Amelia :3
* Sowińscy są niezadowoleni, uważają to za błąd - wyślą kuzyna Jeremiego by pomógł Marysi. ALE jeszcze się nie stało
* Rekiny się dowiadują, że Marysia stoi za "państwem policyjnym" w Dzielnicy Rekinów
* Amelia Sowińska odzyskała częściowo kontakt z Aurum i Rekinami. Przez to, że chcieli się skonsultować z nią wrt Hestii itp. Przez Hestię.
    * To też jest forma ixiońska, o czym nikt nie ma pojęcia. Amelia nie jest ekspertem, nikt tu nie jest więc "ok, udało się połączyć". Nie wiedzą jak.
        * Czyli jeśli Amelia wejdzie w esuriit w jakimkolwiek stopniu, straci kontakt z Hestią / przez Hestię.0
* Pomniejsze waśnie Marysiowe
    * Liliana Bankierz (po stronie AMZ) nie może darować Marysi "zdrady". Ani Karolinie. Ani nikomu.
    * DJ Babu (po stronie Rekinów) nie może darować Marysi "zdrady" Stasia Arienika i oddania go "tyranizującej mamie".
        * I tego że robi z Rekinów państwo policyjne.
    * Santino Mysiokornik słusznie obawia się oskarżenia o nadmierne zainteresowanie młodymi damami (zwłaszcza Chevaleresse) i chwilowo unika Marysi.
* Liliana Bankierz trafiła do szpitala, do larwy Blakenbauera.
* Lorena ma wsparcie - Marsena Gwozdnika - który wierzy w jej dobroć i kompetencję. Ona ofc musi udawać.

### 3.2. Analiza

Mamy kilka frakcji Rekinów:

* Paladyni
    * Justynian Diakon + Mimoza Diakon
    * Ogólnie uważają, że Rekiny powinny być źródłem siły i dobra na tym terenie
    * Zwykle preferują podejście Amelii nad Marysi
    * Są bardzo przeciwni Eterni; stoją za Tradycją Aurum
* Anarchiści
    * DJ Babu, Santino Mysiokornik, Franek Bulterier, Daniel Terienak...
    * Ogólnie uważają, że Rekiny powinny robić co chcą i nic nie powinno ich ograniczać
* Anty-Eternici
    * Santino Mysiokornik, Lorena Gwozdnik, Mimoza Diakon...
    * Ogólnie uważają, że Eternia stanowi problem i Ernest nie powinien tu działać
* Lojaliści Korony
    * -> Amelia lub Jeremi. Nie Marysia.