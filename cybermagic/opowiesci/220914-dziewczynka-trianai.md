## Metadane

* title: "Dziewczynka Trianai"
* threads: historia-eustachego, arkologia-nativis, plagi-neikatis, zbrodnie-kidirona
* gm: żółw
* players: fox, kapsel

## Kontynuacja
### Kampanijna

* [220831 - Czarne Hełmy i Robaki](220831-czarne-helmy-i-robaki)

### Chronologiczna

* [220831 - Czarne Hełmy i Robaki](220831-czarne-helmy-i-robaki)

## Plan sesji
### Theme & Vision

* "I did not want to be a monster!"
    * Nature always wins
    * Rewrite overpowers nature

### Co się wydarzyło KIEDYŚ

* Nativis była dość ubogą arkologią; ale 24 lat temu zaczęła zarabiać jedzeniem i się rozbudowywać
    * W ten sposób ród Kidiron doszedł do władzy

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

Czarne Hełmy wpadły i uderzyły w Robaki. Robaki zostały zmiażdżone.

Dziadek i Wujek podejrzewają Tymona, zostało kilka śladów wskazujących na kompetentną osobę a Tymon po prostu pasuje do wzoru. Fakt, że chce się przypodobać Kidironom jedynie zwiększa. Kidironowie też to zauważyli i jako, że Hełmy zmiażdżyły Robaki to Tymon został "ozłocony" - przesunięty na zupełnie inną pozycję. A im wyżej idzie Tymon wśród Kidironów tym bardziej oddala się od wujka (czyli swojego ojca).

* Ktoś robi chore hybrydy ludzi i Trianai (xenoplague)
    * Kara kiedyś była osobą, teraz jest drapieżnikiem
    * Odkąd zniknęły Robaki, Kara zaczęła pobierać jedynie jedzenie przez mindwarp strażników pilnujących jedzenia
    * Strażnik popełnił samobójstwo, ale udało się dojść do tego, że tam jest Kult. 

### Co się stanie (what will happen)

* S00: -> Fox "zostawisz jedzenie w korytarzach"?
* S00: "co, strachasz się?" -> zniknięcie Rafała
* S01: briefing Zespołu. Trzeba otworzyć przejście niedostępnymi terenami, tam jest Rafał i kultyści...

### Sukces graczy (when you win)

* Karina zostanie zlokalizowana i usunięta
* Pocałunek nie zostanie rozprzestrzeniony za bardzo
* Znalezienie techbunkrów
* Uratowanie nastolatka (Rafała Kumnika)
    * Widziano go PO zniknięciu
    * Ale... nie ma go

## Sesja właściwa
### Wydarzenia przedsesjowe

Karina jest nastolatką (14 lat). Jest protomagiem, która została przekształcona w trianai i jej Wzór to zaakceptował. Uciekła. Dotarła do Arkologii Nativis i zagnieździła się w tunelach Starej Arkologii.

Karina nie do końca pamięta kim jest, ma też nowe niebezpieczne instynkty Trianai. Szuka swojej rodziny i Pocałunkiem infekuje ludzi budując w nich nieskończoną lojalność wobec siebie. W ten sposób udało jej się wzbudzić lojalność kilku Robaków.

Wtedy Robaki zostały rozbite. Karinie zostało jedynie dostawać w prezencie jedzenie od swojego Agenta z zewnątrz; niestety, ów popełnił samobójstwo gdy tylko namierzyły go Czarne Hełmy...

### Scena Zero - impl

2 tygodnie po sprawie z Robakami. Stanisław resocjalizuje Ardillę do pracy z dziećmi. Stanisław wymyślił, że Ardilla zostanie przedszkolanką. Ardilla jest lekko sceptyczna wobec tego pomysłu, ale co jej zostaje XD. Nagle:

* S: Ardillo, czy mogłabyś... wziąć tą paczuszkę i zostawić ją w tunelach tam gdzie Ci powiem?
* A: Kult? Miałeś zrezygnować...
* S: Nie, głupia sprawa. To przesąd. W środku jest jedzenie. (dobrze zbalansowane potrawy)
* S: Duch... tak, duch się musi odżywiać.

Gdy mówił o Duchu mówił bardziej prawdziwie niż gdy o reszcie Robaków. Okazuje się, że Stanisław wierzy w to, że w Starej Arkologii jest duch XD.

* A: Jeśli jest niebezpieczeństwo, może mnie pan uprzedzić? Sam pan mówił żebym nosa nie w niebezpieczne sprawy.

Tp (niepełna) Z (on nie może Ty możesz i Ty pomogłaś) +3:

* X: nawet Robaki nie do końca uważają że on w tej kwestii wie co się dzieje
* V: duch arkologii
    * Stanisław opowiedział historię - znikały nam kanapki
    * widział dziewczynę. 15 lat. Zostawia jej jedzenie

(TAK - Stanisław ją widział i zostawił jej jedzenie. Karina nie wiedziała co z tym zrobić; nie pokazała mu się, ale zapamiętała jego zapach. Chciała go bliżej poznać, ale nie chciała mu się pokazywać. Za to zaczęła robić sobie makijaż - trochę brudu na twarzy, jak pamiętała niewyraźnie swoją mamę)

Ardilla zaniesie jedzenie Duchowi mimo kordonu. Prześlizgnąć:

Tp Z (on nie może Ty możesz i Ty pomogłaś) +3:

* V: prześlizgnęłaś się między kordonem (2 emerytów) i wpełzłaś do tuneli
* X: zajęło upokarzająco dużo czasu znalezienie właściwego miejsca. Serio. Powinnaś wiedzieć. (Karina obserwuje Ardillę ale zostawia ją w spokoju)
    * opieprz #7 - mogło coś Ci się stać, byłabyś dobrą przedszkolanką
* V: Ardilla się upewniła - nie umie znaleźć ducha. I czas do Stanisława.

...

TEMPKA. Masz na imię Maciek. 17 lat i chce się wykazać. Więc pozbierać sprzęt po Czarnych Hełmach i Robakach.

Idą do pułapki gdzie część Hełmów padła.

Tr+2:

* X: droga do pułapki jest odcięta - by się tam dostać, trzeba przez rury z drugiej strony (+2Vg)
* V: udało Ci się przedostać po tych rurach. Rzuciła się w oczy ciekawostka - coś na kształt... pudełko po jedzeniu?
* V: udało się zerknąć przez drzwi - widzisz na ziemi leżącego jednego kolegę a drugiego nie ma. Zniknął. I ruch, gdzieś w oddali. Szybki.

Maciek nie ma wyjścia. Patrzy czy kolega jest ranny. Jest nieprzytomny, ugryziony. Maciek stwierdza że ucieczka jest ważniejsza +2Vg.

* V: Maćkowi udało się uciec i zaalarmować emerytów z kompanii ochroniarskiej Bulterier...

(Karina zobaczyła w jednym z tych chłopaków swojego brata, więc go porwała a drugiego ukąsiła - nikt nie chce być sam, więc Karina chciała dwóch przyjaciół razem. Zobaczyła Maćka, ale skoro nie został z nimi to nie jest najlepszym przyjacielem więc pozwoliła mu odejść.)

### Sesja Właściwa - impl

Dwie godziny później Ardilla i Eustachy zostali wezwani przez Bartłomieja. "W tunelu starej arkologii zniknęła dwójka dzieciaków". Dzieciak zniknął 7h po tym jak Ardilla zostawiła jedzenie. Co ciekawe, zgodnie z tym gdzie chłopak mówił że znalazł jedzenie to Ardilla ma dowód że ktoś PRZENIÓSŁ jedzenie gdzieś indziej.

Zapytana przez wujka dlaczego bywa w tamtych tunelach, Ardilla powiedziała "Dokarmiamy ducha by nas nie porwał" (że niby dzieciaki mają takie plotki) - mina wujka "to najgłupsza rzecz jaką słyszałem". Wujek mówi, że podejrzewa Robaki. Nie pasuje to do nich, ale... co innego? To o dokarmianiu jednak coś mu przypomniało - wujek jednak powiedział "odkąd Robaki zniknęły monitorujemy jedzenie itp. Normalne. Pięć dni temu... nie tak. Strażnik noszący jedzenie do Starej Arkologii chyba był z kultu i się zastrzelił przy próbie aresztowania". (to był agent Kariny)

Ośmiu potencjalnych Robaków nie wróciło do Arkologii - mogą być tam na dole. Inny, groźniejszy kult?

Tak czy inaczej, Ardilla i Eustachy zdecydowali się dzieciaki znaleźć przed zmontowaniem grup poszukiwawczych.

JAK CHCECIE ZNALEŹĆ DZIECIAKI.

Eustachy ma prosty pomysł - seria czujników działających jak system ostrzegania przed tsunami. Każda boja robi sygnał (pik), algorytm przetwarza wysokość fali. Coś takiego, że każdy czujnik szuka prędkość, kierunki, rozmiar. Czyli nie tylko czujnik ruchu... rozszerzająca się sieć pokazująca otoczenie.

TrZ (sprzęt Inferni) +3:

* X: sieć czujników w tamtym terenie będzie dość... nieprecyzyjna. Daje dobry kierunek i mniej więcej informację, ale nie jest dokładne.
* V: Znajdzie i poinformuje

Ekipa bez kłopotu dotarli do miejsca gdzie zniknęły dzieciaki. Nic nie wygląda bardzo nietypowo, choć... są ślady. Historia się MOŻE trzymać kupy, zwłaszcza ślad sosu na jednej z rur gdzie miało być opakowanie po jedzeniu. Idą razem, nie rozdzielają się i rozprowadzają czujniki.

* V: są ślady, są sygnały. Są dwie grupy sygnałów - pojedynczy sygnał (gdzieś na obrzeżach) oraz dużo sygnałów (6+) gdzieś "tam".

(Karina śledzi Zespół - widzi Ardillę i poznaje zapach Stanisława i tą, co przyniosła jeść. To sprawia, że jest mniej ostrożna. Pozostałe 6 osób to jej "rodzina" - Karina ich zainfekowała i ma kogoś do kogo się może przytulać)

Czas na pułapkę Eustachego - nie lubi jak ktoś go ściga i śledzi. Gdzieś w korytarzach jest śluza. Dla zasady - nie ma przejścia wentylacją, bo ma oddzielać jedną część bazy od drugiej. Rozstawiamy czujniki i w samej śluzie pułapka - zatrzasnąć w środku to co ich ściga. Granat do puszki, rękojeść odpadająca poza puszką, poza puszkę, drugi koniec - PYK, nie da się zabezpieczyć i wybucha. Hukowo-błyskowy granat by ogłuszyć przeciwnika. (+3Vg)

* V: udało się. Pułapka trafiła i oszołomiła Karinę.

(Z perspektywy Kariny - zeskoczyła, przechodzi i błysk i huk i fala uderzeniowa. Szybko wstaje) Eustachy szarżuje i widząc nieludzką bioformę Kariny kolbą w... NIE! STRZELA! Obok. (Karina robi desperacki manewr unikowy dostając ciężką ranę, ale unikając śmierci)

* V: Przeciwnik jest ciężko ranny. Pocisk rozerwał jej bok. Człowiek tego nie przeżyje. Ona odskoczyła częściowo i ciężko ranna. Krzyknęła "MAMO!!!" jak Eustachy przyłożył do strzału... nie jest jej w głowie atak, chce uciekać
* Vz: Eustachy ma dobrą broń. Ciężko ranił stwora w nogi i ręce, by ten miał szok i nie mógł nic zrobić. Krzyk z agonii. Stwór jest niegroźny. Zamknięty w worku. Stary but w pysku by nie pogryzła i lecimy.

Ardilla znajduje drogę. Zapach krwi itp -> zawroty głowy. Istota powoduje Skażenie i Zauroczenie samym zapachem. Eustachy zostawia za sobą miny a zamiast odłamków jest sól. Nieletalne ale bolesne.

Tr Z (miny Eustachego) +5Og (sukcesy ale miny Eustachego) +3:

* V: Ardilli uda się zgubić przeciwników. I w sumie wszystko.
* X: Sprawa nie została całkowicie wyciszona. Szmata spoko, ale to wygląda jak płacząca dziewczyna w worku.
* Og: SUKCES ale terror. "Infernia to potwory".

Do Kidironów: "Złapaliśmy potwora". Biolab wziął potwora się nim zająć. Ardilla oddała Kidironom dziewczynkę, nie wujkowi.

EPILOG:

* Ten byt jest sformowany od dłuższego czasu z dziewczynki. Z protomaga. To synteza protomaga i trianai.
* Dziewczynka nie pochodzi z tej arkologii. Ona jest tu od kilku lat.
* Ona ma umiejętność dominacji swoją trucizną. Szukała rodziców i rodzeństwa.
* Nie będzie nigdy normalna; nie wiadomo co z nią zrobić. Kidironowie ją zniknęli.
* Ona nie zrobiła nikomu krzywdy - instynkty wygrywały, ale nie chciała.

KARINA NIE JEST STĄD.

Było 6 grup poszukiwawczych w kanałach.

## Streszczenie

Po zniszczeniu Robaków okazało się, że w tunelach Starej Arkologii zniknęło dwóch nastolatków. Ardilla i Eustachy poszli znaleźć owych nastolatków - faktycznie, coś tam jest. Eustachy zastawił pułapkę i prawie zabił pietnastolatkę zmienioną w Trianai. Udało im się wydobyć ją i dostarczyć Kidironom, choć wykazali się dużą bezwzględnością. Większość zdominowanych przez dziewczynkę ludzi udało się uratować. Ale kto jej to zrobił i czemu?

## Progresja

* Eustachy Korkoran: opinia niesamowicie bezwzględnego. Nieważne co - jest zadanie to wykona. Wzbudza strach oraz szacunek w Arkologii Nativis.

### Frakcji

* .

## Zasługi

* Eustachy Korkoran: po przyjęciu zadania odnalezienia dwójki zaginionych nastolatków przejął dowodzenie, rozstrzelał piętnastolatkę Trianai (z trudem się powstrzymał by jej nie zabić) i odniósł do Kidironów z Ardillą. Wykazał się absolutną skutecznością i zimną bezwzględnością.
* Ardilla Korkoran: w imieniu Stanisława zaniosła jedzenie dla "Ducha" Arkologii, potem znalazła gdzie ów "Duch" się znajduje (piętnastolatka trianai) i oddała ją Kidironom.
* Stanisław Uczantor: ex-social worker; karmił "Ducha" arkologii i próbuje resocjalizować Ardillę, by ta została przedszkolanką.
* Karina Nezerin: piętnastoletnia protomag-trianai; szukała miłości i rodziny infekując ludzi. Zaciekawiona Ardillą weszła w pułapkę Eustachego i została ciężko ranna i ze strasznym bólem. Mindwarp; jej krew i pocałunki uzależniają. Skonfliktowana między naturą Trianai i ludzką pamięcią.
* Bartłomiej Korkoran: wujek poprosił Eustachego i Ardillę, by oni rozwiązali problem znikających nastolatków w Starej Arkologii. Nie wierzy w Ducha Arkologii.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Nativis
                        1. Poziom 1 - Dolny
                            1. Północ - Stara Arkologia:
                                1. Stare Wejście Północne: nieużywane; tamtędy Agent Kariny nosił Karinie i jej "rodzinie" jedzenie z magazynów
                                1. Blokhaus F: nieużywany, ale aktywny; tam znajdowała się Karina i jej "rodzina".

## Czas

* Opóźnienie: 14
* Dni: 1

## Konflikty

* 1 - Ardilla przekonuje Stanisława by powiedział jej prawdę o Duchu i czemu chce by ona łaziła po korytarzach
    * Tp (niepełna) Z (on nie może Ty możesz i Ty pomogłaś) +3
    * XV: nikt Stanisławowi nie wierzy, ale powiedział Ardilli prawdę
* 2 - Ardilla zaniesie jedzenie Duchowi mimo kordonu. Prześlizgnie się koło starych ochroniarzy
    * Tp Z (on nie może Ty możesz i Ty pomogłaś) +3
    * VXV: prześlizgnęła, ale zostawiła nie zauważywszy Kariny, wróciła i dostała opieprz od Stanisława że ryzykowała
* 3 - Nastolatek Maciek idzie do pułapki gdzie część Hełmów padła.
    * Tr+2
    * XVVV: odcięta droga do pułapki WIĘC rury z drugiej strony, przejście, znalazł pudełko po jedzeniu i widząc nieprzytomnego przyjaciela - zwiał. COŚ JEST.
* 4 - Eustachy ma prosty pomysł znalezienia wroga - seria czujników działających jak system ostrzegania przed tsunami.
    * TrZ (sprzęt Inferni) +3
    * XVV: nie jest dokładny, daje poglądówkę, są ślady i sygnały
* 5 - Czas na pułapkę Eustachego - nie lubi jak ktoś go ściga i śledzi. Gdzieś w korytarzach jest śluza. Zaatakować z zaskoczenia i złapać napastnika.
    * TrZ (sprzęt Inferni) +3
    * VVVz: sukces - oszołomienie Kariny, potem jej postrzelenie, ciężka rana i unieszkodliwienie. A mógł zabić.
* 6 - Eustachy zostawia za sobą miny a zamiast odłamków jest sól. Nieletalne ale bolesne. Dyskretne wycofanie zanim ktoś przyjdzie.
    * Tr Z (miny Eustachego) +5Og (sukcesy ale miny Eustachego) +3
    * VXOg: Ardilli uda się zgubić przeciwników, ale nie wyszło z wyciszeniem. "Eustachy to potwór".
* 7 - 
    * 
    * 
* 8 - 
    * 
    * 
* 9 - 
    * 
    * 
* 10 -
    * 
    * 
