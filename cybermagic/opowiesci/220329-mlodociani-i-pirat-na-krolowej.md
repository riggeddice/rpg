## Metadane

* title: "Młodociani i pirat na Królowej"
* threads: planetoidy-kazimierza, sekret-mirandy-ceres
* gm: żółw
* players: kić, anadia

## Kontynuacja
### Kampanijna

* [231027 - Planetoida bogatsza niż być powinna](231027-planetoida-bogatsza-niz-byc-powinna)
* [220512 - Influencerskie Mikotki z Talio](220512-influencerskie-mikotki-z-talio)
* [220525 - Dzieci z Arinkarii odpychają piratów](220525-dzieci-z-arinkarii-odpychaja-piratow)

### Chronologiczna

* [231027 - Planetoida bogatsza niż być powinna](231027-planetoida-bogatsza-niz-byc-powinna)

## Plan sesji
### Co się wydarzyło
#### Co się wydarzyło w tym terenie?

* Inicjacja bazy Ukojenie Barana
    * mag eternijski, Bruno Baran, założył niewielką bazę daleko od Eterni z częścią ludzi 12 lat temu
    * automatycznie zaczął się kręcić profit
    * wśród górników Elsa Kułak została nieformalną liderką. Niestety, "demokracja" nie przypasowała Baranowi. Odizolował się.
* Migracja Mardiusa
    * noktiańscy komandosi Tamary Mardius złożyli tu swój nowy dom wśród lapicytu. Podeszli do górników i Barana z neutralnością. Ot, są.
    * mało kto jak noktianie umieją żyć w kosmosie. W tym miejscu znaleźli świetne miejsce i ich umiejętności stały się bardzo przydatne.
    * siły Mardius znalazły opcję szmuglowania przez statek Gwiezdny Motyl.
* Imperium Blakvela
    * 3 lata temu pojawił się eternijski szlachcic, Ernest Blakvel. Stwierdził, że władza w kosmosie to coś dla niego. Zaczął umacniać się w tym terenie.
    * Blakvel i Mardius weszli w stały konflikt. Mardius chroni miejscowych, Blakvel chce ich podbić.
* Pojawienie się Strachów
    * 2 lata temu pojawiły się Strachy. Stworzone z programowalnej materii (morfelin), nie wiadomo czemu są i czym są.
    * Mardiusowcy aktywnie chronią lokalnych.

#### Strony i czego pragną

* Górnicy w Domenie Ukojenia i mały biznes: 
    * CZEGO: zachować niezależność, handel itp. Przetrwać. Zarobić.
    * JAK: sami się zbroją
    * OFERTA: standardowa oferta terenowa, plotki itp.
    * KTOŚ: Elsa Kułak (przełożona, trzyma twardą ręką), Antoni Czuwram (górnik, solidny stateczek i niezły sprzęt - dużo wie), Kamil Kantor (szuka ojca Leona który odkrył sekret Mirnas i zaginął), Bruno Baran (mag eternijski i założyciel; Strachy to destabilizacja Blakvela przy obecności lapizytu), Kara Szamun (młódka z rodziny Szamun, pilot i nawigator transportowca na Asimear.)
* Przemytnicy Blakvela: 
    * CZEGO: zdobyć prymat w Domenie Ukojenia
    * JAK: zmiażdżyć Mardiusa, potem przejąć Talio i zestrzelić bazę Barana.
    * OFERTA: świetne rozeznanie w pasie, największa siła ognia, wiedza o SWOICH skrytkach, wsparcie piratów etnomempleksu Fareil (synthesis oathbound pro-virt, pro-numerologia)
    * KTOŚ: Ernest Blakvel (arystokrata eternijski)
* Przemytnicy Mardiusa: 
    * CZEGO: zregenerować siły, odzyskać Tamarę, ukryć się przed Blakvelem
    * JAK: w ciągłej defensywie, ufortyfikowanie Sarnin, operacje militarne anty-Blakvel
    * OFERTA: wiedza o Miragencie i Aleksandrii, świetne rozeznanie w pasie, wiedza o SWOICH skrytkach, znajomości wśród lokalnych.
    * KTOŚ: Tamara Mardius (eks-komandos noctis, Alexandria), Deneb Ira (regeneruje komandosów po sprawie z Aleksandrią)
* Taliaci: (mieszkańcy Talio)
    * CZEGO: rzeczy wysokotechnologiczne
    * JAK: handel, współpraca
    * OFERTA: mają dostęp do WSZYSTKICH frakcji i są wiecznie neutralni, reaktory termojądrowe na podstawie deuteru i trytu na Talio.
* Strachy
    * CZEGO: Brak woli. Reakcja. Zniszczyć wszystko.
    * JAK: flota inwazyjna
    * OFERTA: zniszczenie, targetowane lub nie.

### Co się stanie

* .

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

* Po 10x dniach Królowa Przygód została doprowadzona do działania przez blakvelowców
    * Na pokład trafił jako czwarty członek załogi psychotronik, Seweryn Grzęźlik.
* Królowa poleciała w pierwszy rejs na Asimear, sprzedać loot. Z sukcesem
* Anna Szrakt, z Szkoły Resocjalizacji Jednostek Imienia Seelii, wzięła trójkę swych podopiecznych by zapoznali się z Pracą.
    * SRJIS (w skrócie: Irys) zapłaciła za najtańszą jednostkę która dotarła do planetoidy Asimear i okolic. To była Królowa.
* W ten sposób Królowa ma na pokładzie grupę deliquents + Ania. Nikt nie wie kto i co.
    * Prokop, kapitan statku, trochę flirtował z Anną. Anna, ze swoim 5x-letnim uśmiechem, go skutecznie zniechęciła. Za duży gołodupiec dla niej.
    * Kara chciała flirtować z Prokopem, ale Anna to ucięła.
* Po kilku dniach drogi przez Pas Kazimierza młodzież zaczęła być zniechęcona i znudzona...

### Sesja właściwa - impl

Dzień 1:

Gotard pełni rolę mechanika. Miał kilka fajnych narzędzi. Miranda zauważyła, że jedna z nastolatek, Romana, zwinęła jedno z narzędzi Gotardowi. Małe, dyskretne, łatwe do schowania. Nic niebezpiecznego ale ma jakąś tam wartość. Miranda traktuje to jako pożyczkę i nic z tym nie robi ;-).

Dzień 2:

Dzień / noc nie występują na statku kosmicznym, są sztuczne wachty. Dzieciaki są w tylnej części statku z Anną. Żeby było w miarę spokojnie i żeby dzieciaki mogły się czymś zająć, Kara zrobiła karaoke. Naprawdę fajnie śpiewa. Ogólnie, dzieciaki się cieszą, coś się dzieje, jest zabawa...

Seweryn po raz kolejny próbuje zrozumieć Sekrety Mirandy. Nie jest w stanie shackować ani złamać TAI. Jest coraz bardziej sfrustrowany. I mruczy "niech te wrzaski świni się wreszcie skończy". W końcu z frustacji wstał i idzie opieprzyć Karę. Kara ma łzy w oczach. Bartek łapie za nóż. Anna reaguje. "Spierdalaj od moich dzieci". Seweryn ma szczękę przy podłodze. Popatrzył, zamrugał, odwrócił się i wyszedł przy śmiechu dzieciaków.

Seweryn poszedł się poskarżyć kapitanowi. Kapitan, filozoficznie "są naszymi gośćmi. Płacą." Seweryn "za mało..." Wściekły Seweryn nie ma gdzie iść. A Kara zdwoiła karaoke, są tańce itp. Prokop ogląda sobie na monitorze i się dobrze bawi. Gotard szuka swojego narzędzia i nie pomyśli nawet, że Romana mogła je ukraść. Seweryn wbija się do AI Core i Miranda go wkręca, że "na pewno coś znajdzie".

Anna pedagogicznie do Bartka, bo ten się opanował. Bartek robi postępy - A MÓGŁ ZABIĆ.

Innymi słowy, dzień jak co dzień ;-).

Dzień 3: 

Zbliżamy się Domeny Barana - miejsca, gdzie Królowa ma wracać i gdzie mają teren obejrzeć dzieciaki. Gotard dalej nieszczęśliwy, nie ma narzędzia. Pyta Seweryna "nie brałeś mojego induktora? Nie wiesz gdzie jest?" Seweryn "na pewno dzieciaki ukradły". Gotard "nonsens, skąd one tutaj? Po co im to?"

Miranda, udając zepsutą Ceres - "induktor jest w kabinie Romany TAM". Seweryn rozpromieniony. Gotard WTF. Seweryn "idź z tym do kapitana". Gotard "wolę sam, tak po cichu, co dzieciakom będziemy coś robić". Seweryn "i tak powiem moim, jakie aniołki tu sprowadzamy". Gotard "dobra, powiem kapitanowi..." i Gotard niechętnie poszedł do kapitana.

Prokop chce to po cichu. Przyszedł do Anny. Czy można zrobić tak, by to się "znalazło"? Na stacji jakby coś takiego zrobiła to mogą ją przez luk wywalić. Mogą ją za to zabić nawet, rozumiesz? Anna rozumie. Niech Prokop sobie idzie, ona to rozwiąże.

Anna poszła do Romany. Siada obok Romki, kładzie rękę na ramieniu i "wszystko, co zabrałaś". Dodaje, że to nie tak jak na Asimear. Tu za kradzieże ucinają ręce i wyrzucają w kosmos. Szkoda żywić więźnia. Anna robi terror++.

Tr Z (reputacja Anny) +3:

* X: Romana jest PRZERAŻONA życiem na granicy. "Ale jak to wyrzucą za kradzież".
* Vr: Romka przyznaje się do podprowadzenia playboyów dla Bartka. I do induktora plazmowego ("to co czerwone i świeci").
    * Bartek spłonił raka. "To miało być TECHNICZNE". Anna opanowuje się, by nie było widać - nie chce pokazać śmiechu.
* V: 
    * Anna: "Komu zwinęłaś te playboye?"
    * Romka: "One były przy księgach, przy pieniądzach, tam gdzie dane z administracji". KASY nie wzięła, bo to zawsze widać. 2/13 numery playboya - nie zobaczą

Za takie zachowanie Anna oczekuje, że dzieciaki zrobią coś na rzecz statku. Posprzątać, drobne naprawy, inwentaryzacja. W czym mogą pomóc, w czym odciążyć?

Anna -> dzieciaki ORAZ dzieciaki -> Seweryn

Ex Z (bo się starają) +3:

* X: dzieciaki są na Annę złe. Nie chcą robić tego wszystkiego. W końcu ROMKA zawiniła.
* X: dzieciaki są wściekłe na Romę. Dlaczego WSZYSCY mają pracować? Anna: głośne zachowanie + ich przyszłość. 
* X: dzieciaki będą próbować, ale średnio im wychodzi. Nie są tak dobre. Niewiele wniosą ale próbują. +Vg
* X: kapitan musiał dzieciaki zatrzymać, bo wyrządziły straszne szkody. Nic poważnego, ale wtf. Marnotrawstwo zasobów. +Vg
* X: nic to nie dało, spięcia Prokop - Seweryn. Seweryn jest wściekły na Prokopa że preferuje dzieci nad statek i załogę. ALE nie nada tematu na planetoidzie; są ważniejsze problemy. Np. Prokop.

Prokop jest zwolennikiem tego, by dzieciaki miały więcej szans. Chce im pomóc. Seweryn - "bo to blakvelowcy płacą". MIRANDA się dyskretnie włączyła - doprowadziła do tego, że niby dzieciaki naprawiły coś. Seweryn nie ma innej opcji - musi przyjąć fakty. Seweryn to łyknął. Dobra. Niech jest druga szansa. NADAL zaraportuje, że Prokop traktuje statek jak jego własność a nie własność blakvelowców, ale MOŻE Prokop miał tu TROCHĘ racji. Nic ani słowa o dzieciach.

Miranda robi eksperyment. Niech dzieci myślą, że coś naprawiły. Czy zainteresują się teorią która za tym stoi? Czy pójdą w głąb?

Tr (niepełna) Z (bo masz dużo ekspozycji) +3:

* Vr: Kara bardzo mocno się zaangażowała (tam gdzie widzi że ktoś to chwali), potem Bartek (tam gdzie pomoże). Romka mniej.

Dzień 5:

RANEK:

Załoga już jakoś bardziej do dzieciaków, choć Seweryn dalej chmurny. Romka -> Anna "czemu on nas tak nie lubi? Jego nawet nie okradłam". Anna odpowiedziała "on nikogo nie lubi. Lubi ciszę i spokój. A my mu przeszkadzamy :3".

POŁUDNIE:

Romka i Kara chichoczą. Miranda podgląda. Romka i Kara zrobiły serię śmiesznych historyjek jak to Seweryn x TAI Ceres. Very explicit - Kara umie też rysować. Miranda nic nie powiedziała, ale zapisała sobie w buforze...

WIECZÓR:

Skanery dalekiego zasięgu zobaczyły niewielką jednostkę, na oko uzbrojony statek górniczy, ostrzeliwany przez trzy Strachy. Miranda pokazuje info Prokopowi. Kapitan daje rozkaz - pomagamy. Łucjan - "nam nie pomogli". Kapitan - "ale my pomożemy". Seweryn zaproponował by puścić to dzieciakom (złośliwie). Kapitan nie poczuł złośliwości. Jasne, dobry pomysł.

GOTARD GŁÓWNY ARTYLERZYSTA. Miranda zobaczyła jak celuje. OMG... Poprawiła.

Miranda dowodzi Królową, udając, że dowodzi naprawdę załoga. Próbuje osłonić jednostkę górniczą i zestrzelić Strachy (i nie dać się trafić). Aha, i sprawić, by wszyscy myśleli że to ONI mają kontrolę nad jednostką.

Tr Z (bo ma przewagę, jest naprawiona) +2:

* Vz: iluzja kontroli zapewniona. No jak, Ceres?
* X: niestety, trafiona została też jednostka górnicza. 
* V: Strachy unieszkodliwione. Nie skrzywdzą Mirandy i Królowej.

Jednostka górnicza wysłała sygnał S.O.S. Prokop zaakceptował. Łucjan "kapitanie, to niebezpieczne. To na pewno pułapka. Wpadną i nas okradną, bo Gotard ich trafił." Gotard: "Wcale nie trafiłem. Tylko trochę... i przez przypadek". Prokop: "Nieważne, nie zostawimy ich tu samych, nie możemy." Seweryn: "Możemy. Wezwiemy inną jednostkę." Prokop: "A jak się pojawi więcej Strachów?" I taka cisza.

Bartek "pani Aniu, ale im... pomogą, prawda?" Anna "mam nadzieję, że tak". Anna próbuje więcej złapać na temat tej jednostki.

Tr Z (bo komandos + zna jednostki górnicze) +2:

* V: jednostka górnicza wygląda trochę dziwnie - jest ciężko uzbrojona jak na jednostkę górniczą i ma mocny napęd. "Wojownicza" i dalekosiężna, przez to mniejsze cargo, mniej dron itp.
* V: jednostka górnicza jest uszkodzona. Najpewniej faktycznie potrzebują pomocy. Napęd działa, ale niezbyt dobrze powalczy. Wyraźnie jednostka uczestniczyła w wielu walkach.
* X: jeśli Anna nie przyzna kim była, nikt jej nie uwierzy
* Vz: Anna zna dobre podejście i kąty (jak to zrobić by nie mogli Królowej zaatakować działami). Jednocześnie Anna wie, że w okolicy działają jednostki pirackie, które np. kradną cargo. To wygląda na taką jednostkę. Lub na eskortowca.

Anna dopowiada "W sumie - nie. Nie mam nadziei. Wiem, że im pomogą." Po czym idzie porozmawiać na mostek.

Cała załoga patrzy ze zdziwieniem na Annę. WTF ona tu robi. Seweryn, złośliwie "dzieciaczki się przestraszyły i trzeba wyłączyć?". Anna "tylko tego że nie chcesz pomóc osobom w potrzebie." i do Prokopa - "podleć TAK I TAK, nie mogą nas zaatakować". Miranda - o, ona wytyczyłaby inny, słabszy. Od razu Anna dodaje, że to albo pirat albo eskortowiec.

Łucjan i Seweryn "ABSOLUTNIE nie pomagamy." Seweryn "porwą nasze kobiety i je sprzedadzą!" Anna, zimny śmiech "nie martw się, ochronimy Cię." Prokop jest niepewny, boi się piratów wyraźnie. Anna rzuca - jest byłą opsec na Asimear. Uczestniczyła w niejednej pacyfikacji.

Prokop - "dobra, jak to robimy by im pomóc?" - i wszyscy patrzą na Annę. Anna układa plan. Ale patrzy na uzbrojenie, mają lekkie. Nie mają ani jednego servara. Same pancerze i skafandry. Nie są mocno uzbrojeni. A tamta jednostka jest potencjalnie jednostką piracką; można nacelować działa i zestrzelić.

Anna proponuje kontakt z tamtą jednostką. Połączyła się z nimi. Odebrała 3x-letnia kobieta. Na oko, lekko ranna.

* Helena: "Helena Banbadan. Potrzebujemy pomocy." - wygląda na atrakcyjną, ale twardą. Widać że na mostku nie jest dobrze - "Czemu nas ostrzelaliści?"
* Anna: "Walczyliście ze Strachami"
* "Nie z Wami"
* "Ostrzelaliście nas!"
* "Moja droga" - Anna wchodzi w tryb nauczycielki - "Coś bardzo dużo marudzisz jak na osobę której uratowaliśmy dupę i dowodzisz statkiem pirackim"
* "Nie dowodzę statkiem pirackim!" - stres w głosie

Anna się zna na resocjalizacji. Ona wie. Ona wie jak z nimi mówić.

Ex (Helena jest MISTRZEM oszustwa, ale Anna pracowała z nastolatkami...) Z (bo dużo wie) +3:

* X: przeciwnik widzi, że WY WIECIE i musi działać. Miranda wzmacnia ekran by Anna widziała mimikę. Królowa ma SŁABE sensory, ale ok. +1Vg.
* X: przeciwnik zadziałał korzystając ze słabości systemów Królowej. Anna mówi co widzi - pokazuje, że zna się na rzeczy +1Vg.
* V: Anna widzi, że Helena OGÓLNIE mówi prawdę, ale kłamie - nie ona dowodzi. Ona jest frontem. I widzi, że ich jednostka jest w gorszym stanie niż się wydawało. Królowa może ich zestrzelić. Helena jest zdesperowana. Nie mają dużo kart, ale coś knują. To są piraci. Anna informuje ich - wie, że to piraci. ŻĄDAJĄ manifestu statku w czasie rzeczywistym i WTEDY się zobaczy. +2Vg.
* Vg: 
    * Helena jest zrezygnowana. Spuściła głowę. Statek piracki spróbował namierzyć Królową, ale nie jest w stanie. Natychmiast przestał próbować, ale ANNA WIDZIAŁA.
    * Helena "wysyłam manifest".

Anna -> Seweryn "zajmiesz się tym?". To jest robota dla Seweryna wspieranego przez "bezwartościową Ceres" w jego oczach. Miranda ma kocią minkę. A Anna jest uśmiechnięta, bo dzieciaki piszczą z radości, że WŁAŚNIE ŁAPIĄ PIRATÓW DO NIEWOLI.

Seweryn MYŚLI, że robi to sam. Miranda robi to z jego pomocą.

Tr Z (bo jednak coś Seweryn pomaga i wie na co patrzeć) +3:

* V: Oczywiście, manifest jest nieprawdziwy. BARDZO DOBRZE sfałszowany. Seweryn -> Anna. Anna -> Helena. I nowy manifest.
* Xz: Seweryn zorientował się, że TAI jest zbyt pomocna. Działa lepiej niż powinna. WTF? Jak to możliwe?
* X: Tylko jeden fakt udało się schować. Dużo cennych danych - ale jeden fakt zniknął. Skutecznie przeoczony bo tam jest mistrz biurokracji. Seweryn się wściekł - przekierował większość mocy na obliczenia. Wszystko niekluczowe. Światła przygasły. +2Vg
* V: Na statku jest sześciu piratów. Mają 2 servary klasy Lancer. Nie najlepsze, ale dość by zrobić insercję. Jest stabilny feed - niemożliwe oszukanie że np. ktoś jest w servarze czy coś. Mają sprzęt i broń. Na statku jest też lokalizacja bazy z niewolnikami.
* V: Zgodnie z manifestem jest sześciu piratów. Zgodnie z przeszłymi misjami zwykle jest siedmiu. Jednego nie ma. Jest za to więcej łupu. Seweryn i Miranda mają jednoznaczną odpowiedź - nie ma kapitana, ta jednostka jest pod Berdyszem Rozdzieraczem. Zgodnie z manifestem kapitan BYŁ na początku podróży.

Seweryn -> niech poleci Helenie obniżenie ciśnienia i uśpienie na tamtej jednostce. Królowa - niech przygotuje ładownię i w ładowni niech obniży poziom tlenu. Nie jest to ŁATWE, ale jest MOŻLIWE. Seweryn będzie dumny, że dobrze ustawił Ceres ;-).

Anna informuje Helenę o tym czego żąda - wszyscy są wyłączeni przez life support. A ona ich przeniesie (załogą). Czujniki potwierdzają, że się udało. I Gotard + Łucjan, klnąc na czym świat stoi (na Annę), przetargują tych ludzi z jednostki pirackiej do ładowni.

MIRANDA! Widzisz na sensorach kogoś w ŚRODKU statku. Zbliża się do Łucjana. Jest uzbrojony. Następne będą dzieci. To nie jest homo sapiens. To gaulron. Miranda natychmiast spuszcza grodzie - odcinając Łucjana + Berdysza w jednym pomieszczeniu i wszystkich innych poza tym terenem.

* Miranda: "masz 10 sekund na poddanie się albo pomieszczenie wypełnię gazem". Łucjan zakwilił jak świnia.
* Berdysz: "macie CHWILĘ, albo wysadzę oba statki"

Na pierwszy rzut oka wszystko wskazuje na miny. Cholera...

Miranda komunikuje się z dziećmi. Udaje głos Anny. Wielokrotnie widziała patterny mowy Anny. Mają zająć Berdysza. ALE JAK? Poproście, by nie wysadzał statku. Dzieciaki wyraźnie są zestresowane. Miranda zestawia połączenie - rzuca hasłem, by nikomu nie mówili, bo kapitan się nie zgadza a to desperacka sprawa. I to kupuje czas reszcie.

Dzieciaki opóźniają Berdysza na tyle, by on był zaskoczony i nie wysadził statku.

Ex Z (pełne zaskoczenie + szczerość) +3:

* X: Romka pytała Berdysza jak się zostaje piratem, czemu wybrał tą drogę. Berdysz ich zacharyzmował. Pokazał im, że to jest potencjalna droga. Oni mają formę syndromu sztokholmskiego.
* V: Kara płakała o swoją przyszłość, pokazała sztukę. Bartek chciał chronić dziewczyny. Łucjan nawet się popłakał, nie chce umierać. Dzieciaki kupiły czas kapitanowi, Gotardowi i Annie by ci byli w stanie sprawdzić / rozwiązać miny. Dzieci kontynuują "myśmy chcieli Wam pomóc, ktoś na nas czeka..." +Vg
* X: Berdysz wyjaśnił dzieciom - nie chcieli pomóc. To KRÓLOWA zaatakowała. Nie piraci. Piraci nie mieli na celu atakować. Piraci chcieli przetrwać. Królowa nie była jego celem. On chce negocjować. Nawet teraz to KRÓLOWA próbuje działać przeciwko piratom a piraci królowej i dzieciom nic nie zrobili. On jest moralnie wyżej. Łucjan żyje. (dzieci chcą uwolnienia piratów i pomocy piratom). Dzieci zaczynają opowiadać o PANI ANNIE. Tej honorowej, komandosce, specops, od resocjalizacji. +1Vg.
* Vg: Berdysz nie chce wysadzać tej jednostki. Nie chce krzywdzić ani dzieci ani Anny. Ale JEGO statek nie wpadnie w niczyje ręce. Choć - jak powiedział dzieciom - ważniejsze jest dla niego jego życie i wolność niż jego statek.
* X: Berdysz pokazał dzieciom - jeśli Anna jest tak dobra, resocjalizuje i chce pomóc - to niech popatrzą: KRÓLOWA zaatakowała pierwsza, KRÓLOWA może ich puścić, nic złego się nie stało. On nie chce ich atakować. On nawet ich nie wysadza. Ale Anna jeśli go zaatakuje to znaczy, że jest tym, co się o Berdyszu mówi. Ale ON mógł ich wysadzić a tego nie zrobił. a ONA mogła uratować i wolała zabić. I dlatego ma Łucjana - dzięki temu jeszcze żyje. A więc - czym się Anna różni od niego? Hipokryzją. Might makes right. Dzieci są silniejsze od Anny, bo mając wybór zdecydowały że powinien żyć i powinno się pomóc. I dlatego ich nie zabije. JEŚLI Anna zabije Berdysza, dzieci jej nie uwierzą. On miał rację. +1Vg
* X: Dzieciom BARDZO zależy na przetrwaniu Berdysza. Są skłonne nieść jego legendę itp. Przekonały go (pośrednio), że jak przeżyją to on faktycznie dostanie "swoją zemstę". Berdysz wyłączył miny. Powiedział im o tym (na tej jednostce). Przekonały go do tego i jest skłonny im zaufać. Zaufać Annie i temu, że faktycznie KRÓLOWA chciała dobrze. Ich ruch wobec załogi i Anny.

Dzieciaki takie dumne i radosne kontaktują się z Anną. Kara aż szczebiocze. "Pani Anno! Wyłączył miny na tej jednostce! Możemy się dogadać!" (Gotard potwierdził będąc na zewnątrz - miny przy Królowej są nieaktywne). Berdysz będzie negocjował. Dzieci TERAZ Annę wielbią pod niebiosa - to co mówiła, czego ich uczyła jest PRAWDĄ. Przekonali KAPITANA PIRATÓW! Anna ma rację!

* Prokop: "możemy się z nim dogadać!"
* Seweryn: "to może być trik"
* Prokop: "nie poświęcę przyjaciela. Nigdy."

Seweryn twierdzi, że zniszczenie bandy Berdysza jest warte śmierci Łucjana. Prokop i Gotard się absolutnie nie zgadzają - to przyjaciel. A faktycznie nie polują na piratów. Więc czemu mieliby w tym momencie mieć wybierać pokonanie piratów nad życie przyjaciela, który jest z nimi od zawsze. Zdaniem Seweryna - bo ci piraci są winni ogromnej ilości cierpienia. Ci są naprawdę źli. Ale Prokop i Gotard nie mogą odrzucić Łucjana...

PROKOP I GOTARD idą w skafandrach do magazynu, biorą nieprzytomnego Wojciecha (psychotronika i eksperta od sprzętu eks-Orbitera z załogi Berdysza) i do medbay. Tam podaje się mu stymulant. Wojciech się budzi. Szok (związany). I chcemy wsparcia o rozbrajaniu min - on się na tym zna, bo miny analizował i składał. Anna 100% lodowaty uśmiech. Ma pomóc rozbroić miny - bo nie chce umierać. Ma wesprzeć Gotarda. Anna nie torturuje. Na razie.

Tr Z (bo nie chce umierać) +2:

* X: Będzie chciał wolności tylko dla siebie. Nie jest SUPER SZKODLIWY. Jest arogancki i dobry. +1Vg
* V: Postara się pomóc. Poda kody, pomoże rozbroić... zdalnie.
* X: Na pewno nie pójdzie ręcznie rozbrajać min itp.
* X: Będzie żądał / potrzebował gwarancji bezpieczeństwa dla Heleny i tylko Heleny. (+Vg)
    * Jego zdaniem Helena jest ofiarą, jest medykiem, była porwana i albo współpracuje albo zginie. On ją lubi.
    * Więc wolność dla Heleny. Ona raczej zamiast złych rzeczy topiła życie w narkotykach.
* V: Pod warunkiem, że Berdysz i Galahad zginą. => NOPE. Nie ma tego wymogu.

Anna na to idzie. Wolność dla Heleny + Wojciech nie musi ryzykować życiem => on dołączy do załogi. 

GOTARD Z POMOCĄ WOJCIECHA ROZMINOWUJĄ jednostkę piracką:

Tr (a nie ekstremalny bo Wojciech) +3:

* V: Gotard dał radę rozminować jednostkę tak, że nie zostanie zniszczona
* XX: Gotard potrzebuje interwencji medycznej asap +1Vg
* X: Tylko Berdysz może uratować Gotarda LUB Gotard zginie.

Nowe pytanie - Gotard ORAZ Łucjan ORAZ dzieci wierzą / kochają Annę czy śmierć Berdysza?

EPILOG:

* Berdysz został uwolniony. Odstawiony do pomniejszej bazy, z podstawowym sprzętem.
* Statek piracki, nieuszkodzony (bo Berdysz nie wysadził min) został przechwycony. Blakvel wypłacił pryzowe.
* Wojciech dołączył do załogi Królowej. Helena też (przynajmniej tymczasowo). Helena się boi cienia. Wojciech próbuje ją uspokoić, ale sam się boi Berdysza.
* Berdysz obiecał, że nie będzie się mścił na nikim z kim są dzieci.
* Dzieci są przekonane, że Anna jest PRAWDĄ.
* Galahad miał "wypadek" i wyleciał w kosmos bez skafandra.
* Łucjan i Gotard zostali uratowani.

## Streszczenie

Królowa Przygód przeszła przez Asimear i zebrała 3 młodocianych przestępców i Annę, by młodzi obejrzeli życie do którego aspirują (zwykle górników w Pasie). Załoga i dzieciaki nie współpracowały najlepiej, aż Anna nacisnęła. Po drodze Królowa napotkała statek piracki w opałach, pomogła zestrzelić Strachy i przejęła statek piracki. Niestety, kapitan (Berdysz) wślizgnął się na Królową. Owszem - stracił statek i większość załogi, ale odszedł wolno. A Królowa dostała nowych członków załogi i pryzowe za jednostkę piracką.

## Progresja

* Prokop Umarkon: POWAŻNY OPIERDOL od blakvelowców, bo przedkłada jakieś dzieciaki nad finanse Blakvela i swoją załogę.
* SC Królowa Przygód: dostaje do załogi nowego psychotronika (Wojciech Kaznodzieja) i medyka (Helena Banbadan). Ale najpewniej opuści ją Seweryn Grzęźlik.
* Bartek Wudrak: przekonany, że Anna ma rację - dobro i pomaganie mają znaczenie. Przekonany, że Berdysz Rozdzieracz też jest dobrą osobą. Uczestniczył w negocjacjach z nim.
* Romana Kundel: przekonana, że Anna ma rację - dobro i pomaganie mają znaczenie. Przekonana, że Berdysz Rozdzieracz też jest dobrą osobą. Uczestniczyła w negocjacjach z nim.
* Kara Prazdnik: przekonana, że Anna ma rację - dobro i pomaganie mają znaczenie. Przekonana, że Berdysz Rozdzieracz też jest dobrą osobą. Uczestniczyła w negocjacjach z nim.

### Frakcji

* Rozdzieracze Berdysza: praktycznie zniszczeni; zostało 2 + Berdysz i nie mają statku. Ale są na wolności i gotowi do 

## Zasługi

* Miranda Ceres: nigdy się nie zdradziła, choć to ona steruje Królową Przygód. Uratowała statek przed Berdyszem, potem udając Annę połączyła z nim dzieci. Niewidzialne wsparcie.
* Anna Szrakt: dla ratowania resocjalizacji dzieci uwolniła pirata (Berdysza). Taktycznie odkryła że to statek piracki i zmusiła Helenę do poddania jednostki pirackiej.
* Prokop Umarkon: postawił się Sewerynowi w sprawie dzieci; mają mieć szansę. Chce pomóc zagrożonej jednostce (nawet jak piraci). Nie odrzuci życia Łucjana nawet jak to zabije groźnych piratów.
* Łucjan Torwold: bardzo nie chciał pomagać tajemniczej jednostce... miał rację bo to piraci. Wzięty za zakładnika przez Berdysza, prawie zginął.
* Gotard Kicjusz: próbuje ukryć problemy z dzieciakami tak, by nie było oficjalnie. Potem strzelał (kiepsko, ale Miranda strzelała dobrze) i rozbrajał miny (na szczęście nie musiał poważnie).
* Seweryn Grzęźlik: zgryźliwy, kochający ciszę i nie znoszący dzieci. Zdekodował manifest jednostki pirackiej i miał sporo dobrych rad. Coś podejrzewa o Mirandzie.
* Bartek Wudrak: chroni "swoje" dziewczyny nożem, postawił się nawet Berdyszowi. Mało mówi, opiekuńczy. Zainteresował się maszynami pod wpływem Mirandy.
* Romana Kundel: ukradła narzędzie i playboye dla Bartka. Wślizguje się w różne miejsca. Wyciszona, ale zadaje trafne pytania Berdyszowi - czemu ścieżka pirata.
* Kara Prazdnik: miłośniczka karaoke wchodząca w szkodę Sewerynowi. Próbowała rozwiązać z Berdyszem sytuację kryzysową - prosiła by nie zniszczył jej przyszłości.
* Berdysz Rozdzieracz: gaulron i kapitan piratów z Pasa Kazimierza. Zinfiltrował Królową Przygód jako ostatni z grupy. Wymanewrował dzieciaki i doprowadził do tego że mając jedynie jednego zakładnika i będąc na nie swoim terenie NADAL odszedł żywy i zdrowy z dwoma członkami załogi. Strasznie niebezpieczny i ciałem i umysłem. Gra ostro.
* Wojciech Kaznodzieja: pseudo "profesor"; próbował ukrywać manifest piratów ale pokonała go Miranda i Seweryn. Ostatecznie pomógł rozmontować miny i przyłączył się do załogi Królowej Przygód. Zażądał wolności dla Heleny.
* Helena Banbadan: pseudo "Medyk"; udawała kapitana piratów by kupić czas Berdyszowi oraz próbowała wymanewrować Annę. Bez powodzenia.
* SC Królowa Przygód: naprawiona przez blakvelowców, przeprowadziła maiden voyage przez Asimear i po zestrzeleniu kilku Strachów uczestniczyła w akcji destrukcji piratów Berdysza.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Pas Teliriański
                1. Planetoidy Kazimierza
                    1. Domena Ukojenia
                    1. Planetoida Asimear
                        1. Planetoida właściwa
                            1. Kompleks mieszkalny CK14
                                1. Dom Poprawczy Irys: Szkoła Resocjalizacji Jednostek Imienia Seelii; stamtąd pochodzi Anna oraz jej podopieczni

## Czas

* Opóźnienie: 58
* Dni: 11

## Konflikty

* 1 - Anna poszła do Romany. Siada obok Romki, kładzie rękę na ramieniu i "wszystko, co zabrałaś". Anna robi terror++.
    * Tr Z (reputacja Anny) +3
    * XVrV: Romana przerażona konsekwencjami, przyznała się i do wkradania i kradzieży. Odda.
* 2 - Anna oczekuje, że dzieciaki zrobią coś na rzecz statku. Posprzątać, drobne naprawy, inwentaryzacja. W czym mogą pomóc, w czym odciążyć?
    * Ex Z (bo się starają) +3:
    * XXXXX: dzieciaki złe na Annę i na siebie; wyrządziły straszne szkody na Królowej; spięcie Prokop - Seweryn kończące się poważnym opierdolem.
* 3 - Miranda robi eksperyment. Niech dzieci myślą, że coś naprawiły. Czy zainteresują się teorią która za tym stoi? Czy pójdą w głąb?
    * Tr (niepełna) Z (bo masz dużo ekspozycji) +3:
    * V: Kara bardzo mocno się zaangażowała (tam gdzie widzi że ktoś to chwali), potem Bartek (tam gdzie pomoże). Romka mniej.
* 4 - Miranda udaje, że dowodzi załoga. Próbuje osłonić jednostkę górniczą i zestrzelić Strachy. Aha, i sprawić, by wszyscy myśleli że to ONI mają kontrolę nad jednostką.
    * Tr Z (bo ma przewagę, jest naprawiona) +2
    * VzXV: Strachy unieszkodliwione, jednostka górnicza trafiona, iluzja kontroli jest
* 5 - Anna próbuje więcej złapać na temat tej jednostki górniczej.
    * Tr Z (bo komandos + zna jednostki górnicze) +2
    * VVXVz: to najpewniej jednostka piracka? jest uszkodzona; to nie pułapka. I mamy kąt natarcia jak być bezpieczni. Ale Anna musi przyznać kim była.
* 6 - Anna się zna na resocjalizacji. Ona wie. Ona wie jak mówić z piratami i się dowiedzieć.
    * Ex (Helena jest MISTRZEM oszustwa, ale Anna pracowała z nastolatkami...) Z (bo dużo wie) +3
    * XXVVg: Berdysz wkrada się kosmicznym spacerem blisko Królowej i minuje obie jednostki; Anna wymusiła manifest i poddanie się piratów na statku.
* 7 - Seweryn analizuje manifest wspierany przez "bezwartościową Ceres". Miranda ukrywa swoje działanie i łamie manifest.
    * Tr Z (bo jednak coś Seweryn pomaga i wie na co patrzeć) +3
    * VXzXVV: dobrze sfałszowany manifest, będzie nowy; Seweryn widzi moc TAI (trochę); ukryty fakt o Berdyszu i on się wkradł; mają manifest i info i zagrożenia.
* 8 - Dzieciaki opóźniają Berdysza na tyle, by on był zaskoczony i nie wysadził statku.
    * Ex Z (pełne zaskoczenie + szczerość) +3:
    * XVXVgXX: Dzieci spowolniły Berdysza i skłoniły go do wyłączenia min; on jednak przekonał je, że Królowa zaatakowała i Anna ma wybór - moralność czy siła. Dzieciom zależy na Berdyszu.
* 9 - Wojciech się budzi. Szok (związany). I chcemy wsparcia w rozbrajaniu min
    * Tr Z (bo nie chce umierać) +2
    * XVXXV: dołączy do załogi z Heleną i potrzebuje gwarancji bezpieczeństwa dla obu; podpowie jak rozbroić miny ale nie leci ryzykować sam
* 10 - GOTARD Z POMOCĄ WOJCIECHA ROZMINOWUJĄ jednostkę piracką
    * Tr (a nie ekstremalny bo Wojciech) +3
    * VXXX: Gotard rozminowuje, ale jakby nie układ z Berdyszem to by wyparował.

## Kto tu jest
### Załoga Królowej

* Prokop Umarkon: 
    * rola: kapitan, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: extravert, agreeable (outgoing, enthusiastic, trusting, empathetic)
    * values: power + conformity
    * wants: pragnie zostać KIMŚ, wydobyć się z bagna
* Łucjan Torwold: 
    * rola: administrator / logistyka, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: neurotic, conscientous (very worried, scared of cults, very organized)
    * values: security + conformity
    * wants: przesądny; boi się duchów i kultu. Narzeka. Chce bezpiecznego, prostego życia
* Gotard Kicjusz: 
    * rola: technical guy, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: open, low-neuro, low-consc (extra stable, spontaneous, eccentric)
    * values: self-direction
    * wants: przygód, odzyskać rodzinę
* Seweryn Grzęźlik:
    * rola: psychotronik, blakvelowiec
    * personality: neuro, low-extra, consc (workaholic, obsesyjny, cichy, skłonności do wybuchania, umiłowanie porządku)
    * values: self-direction, self-enhancement
    * wants: zrozumieć sekret Mirandy, Blakvel forever, ciszy i rozwoju

### Pasażerowie

* Anna Szrakt
    * rola: wojownik (multi-weapon), "opiekunka do dzieci", nauczycielka życia w społeczeństwie
    * personality: low-open, low-extra, low-neuro (praktyczna, raczej cicha ALE JAK POWIE, bardzo stabilna, taka "zimna zakonnica, sucz z piekła")
    * values: community / tradition, benevolence (społeczeństwo, trzymamy się razem, pomagamy sobie BO MASAKRA BĘDZIE)
    * wants: pomóc osobom dookoła, dać drugą szansę, chronić "swoje" dzieciaki
* Bartek Wudrak
    * rola: delinquet teen, nożownik, aspiruje do bycia górnikiem, 17
    * personality: low-agree, low-cons, low-extra, open (spontaniczny, cichy, nie planuje, 0->100 i atak)
    * values: security, community
    * wants: nikt mnie nie skrzywdzi, wolność od wszystkiego, chronić swoich, Romana ;-)
* Romana Kundel
    * rola: delinquet teen, złodziejka, aspiruje do bycia górnikiem, 16
    * personality: agreeable, open, low-extra (wycofana, ufna, otwarta i chętna próbowania innych rzeczy)
    * values: hedonism, self-direction
    * wants: nikt mi nie będzie rozkazywał, kasa na virt lub kryształy, lepsze życie
* Kara Prazdnik
    * rola: delinquet teen, aspiruje do bycia administratorem i/lub idolką, 17
    * personality: conscientous, extraversion (zorganizowana, metodyczna, wie co robić, bardzo głośna i kocha śpiewać)
    * values: benevolence, tradition (religion)
    * wants: dowodzić innymi, wykazać się w sytuacji kryzysowej

### Piraci

* Berdysz Rozdzieracz
    * rola: pirat-gaulron, dowódca; dewastator, terror unit
    * personality: low-consc, extra, low-agree (choleryczny, entuzjastyczny, spontaniczny, wrogi i niechętny do współpracy)
    * values: hedonism + power
    * wants: BOW BEFORE ME! Prestiż i podziw. Złamię wszystkich. Poczucie władzy.
* Wojciech Kaznodzieja "Profesor"
    * rola: pirat, hacker, psychotronik, były neuropilot Orbitera
    * personality: open, low-neuro (arogancki, otwarty na możliwości, lubi gadać o anomaliach i zbiera opowieści o duchach)
    * values: power, security
    * wants: JESTEM EKSPERTEM, chwała i prestiż
* Galahad Fetysz "Paladyn"
    * rola: pirat, wojownik, sadysta / tortury
    * personality: conscientous, open (artysta sadyzmu, dużo się oblizuje)
    * values: hedonism
    * wants: rape, torment
* Helena Banbadan "Medyk"
    * rola: pirat, medyk, wojownik
    * personality: neuro, low-extra, agreeable (raczej cicha, odwraca oczy, raczej empatyczna)
    * values: security (self>others), hedonism (narcotics)
    * wants: odkupienie, bezpieczeństwo, być sama daleko od tego wszystkiego
