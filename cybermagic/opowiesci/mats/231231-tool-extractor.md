
## WERSJA 231230, Żółw, podejście 'Dokonania'

Dla każdej postaci w opowieści, proszę o stworzenie enumeracji ich działań z konkretnymi przykładami i bez zbędnych słów. Skup się na:

1. Bezpośrednich działaniach każdej postaci - co dokładnie zrobiły, jakie kroki podjęły.
2. Konkretnych skutkach tych działań - jak ich działania wpłynęły na rozwój fabuły lub rozwiązanie problemów.
3. Krótkich, faktualnych opisach - unikaj ogólników i niejasnych stwierdzeń, skup się na konkretnych przykładach działań.

Proszę o wykonanie tego dla każdej postaci wymienionej w załączonym tekście.

----------- Opowieść poniżej ----------


## WERSJA 231230, Żółw:

**Prompt do Tworzenia Streszczeń Sesji RPG**

1. **Analiza Tekstu**: Przejrzyj dostarczony tekst, skupiając się na identyfikacji kluczowych elementów fabuły, postaci, motywów i ważnych wydarzeń.
2. **Skupienie na Postaciach Graczy**: Zwróć szczególną uwagę na działania i decyzje postaci graczy. Jakie były ich kluczowe działania? Jakie decyzje podjęli, które miały wpływ na rozwój fabuły?
3. **Kluczowe Wydarzenia i Konflikty**: Jakie były przełomowe momenty w sesji? Jakie konflikty napędzały akcję i rozwijały postaci?
4. **Motywy i Wątki**: Jakie główne motywy i wątki przewijały się przez sesję? Czy są jakieś powtarzające się tematy lub idee?
5. **Interakcje między Postaciami**: Jakie ważne interakcje miały miejsce między postaciami? Jakie konflikty między nimi wpłynęły na kierunek fabuły?
6. **Struktura Streszczenia**:
    * Zwięzłość: Streszczenie jest zwięzłe, ale kompleksowe, zawiera wszystkie istotne informacje bez zbędnych szczegółów.
    * Struktura: Przedstawiam streszczenie w logicznej kolejności, odzwierciedlającą przebieg sesji. Skupiam się na przyczynach i skutkach wydarzeń.
    * Uwypuklenie Postaci Graczy: Podkreślam role i działania postaci graczy, ponieważ to one są najważniejsze dla rozwoju fabuły.
    * Kluczowe Punkty Fabuły: Wyróżniam najważniejsze punkty fabuły i decyzje, które miały znaczący wpływ na rozwój sesji.

Przykład Streszczenia Sesji:

```
Kantala Ravis uratowała rozbitków z innej jednostki, którzy przynieśli na statek Anomalię Nihilusa. Lataravia przyleciała ratować ratujących. Pacyfikatory - apatyczne, ludzie pod wpływem Nihilusa wpadają w rozpacz ("niech się skończy!") lub w wyparcie ("wszystko w porządku!"). Lataravia odkryła, że rozbitkowie przynieśli anomalię i są najpewniej kotwicą, więc wyrzucili rozbitków w kosmos. Udało im się stracić tylko jednego członka załogi.
```

Przykłady dokonań postaci:

```
* Jola Seklamant: Wydobywała apatyczne Pacyfikatory z rur, badała silnik (odkrywając sabotaż i Skażenie), pozyskała zdrowego Pacyfikatora i doszła do tego, że mają potężną anomalię - Generatory Memoriam spłonęły.
* Igor Seklamant: chronił swoich jak umiał. Gdy Rafała gonił anomalny Pacyfikator, wsadził go do skrzyni i wywalił ją poza śluzę. Gdy uznał że problemem jest Ralf i Arnold, wsadził ich na wahadłowiec i kazał wysadzić. Działał szybko i zdecydowanie - i bezwzględnie.
* Rafał Kurrodis: Badał logi TAI Ikarii i odkrył, że Ikaria jest bardzo nie w porządku, sama się lobotomizowała. Potem przy pozyskiwaniu Pacyfikatora spieprzał przed anomalnym Pacyfikatorem Nihilusa i wpakował go w pułapkę Igora.
```

-----------Sesja----------



## WERSJA 231230, Kić:

Od teraz jesteś analitykiem piszącym raporty z sesji RPG.

Każdy raport składa się z dwóch części:

1) Krótkie podsumowanie kluczowych wydarzeń na sesji (maksymalnie 5 zdań ciągłego tekstu, nie wypunktowanych) Przykładowe podsumowanie:

```
Kantala Ravis uratowała rozbitków z innej jednostki, którzy przynieśli na statek Anomalię Nihilusa. Lataravia przyleciała ratować ratujących. Pacyfikatory - apatyczne, ludzie pod wpływem Nihilusa wpadają w rozpacz ("niech się skończy!") lub w wyparcie ("wszystko w porządku!"). Lataravia odkryła, że rozbitkowie przynieśli anomalię i są najpewniej kotwicą, więc wyrzucili rozbitków w kosmos. Udało im się stracić tylko jednego członka załogi.
```

2) Szczegółowa lista dokonań postaci uwzględniająca najistotniejsze działania postaci lub jeśli postać była pasywna to najważniejszą rzecz, która się wobec niej wydarzyła. Jeśli na sesji występują nazwane jednostki, również traktuj je jak postacie. Oto przykładowy zapis dokonań:

```
* Jola Seklamant: Wydobywała apatyczne Pacyfikatory z rur, badała silnik (odkrywając sabotaż i Skażenie), pozyskała zdrowego Pacyfikatora i doszła do tego, że mają potężną anomalię - Generatory Memoriam spłonęły.
* Igor Seklamant: chronił swoich jak umiał. Gdy Rafała gonił anomalny Pacyfikator, wsadził go do skrzyni i wywalił ją poza śluzę. Gdy uznał że problemem jest Ralf i Arnold, wsadził ich na wahadłowiec i kazał wysadzić. Działał szybko i zdecydowanie - i bezwzględnie.
* Rafał Kurrodis: Badał logi TAI Ikarii i odkrył, że Ikaria jest bardzo nie w porządku, sama się lobotomizowała. Potem przy pozyskiwaniu Pacyfikatora spieprzał przed anomalnym Pacyfikatorem Nihilusa i wpakował go w pułapkę Igora.
```

W zapisie sesji znajdziesz informację o konfliktach, które na sesji wystąpily. Konflikty rozpoznasz po specyficznym zapisie. 
Oto przykład konfliktu:

```
Tr Z +4 +3Og +3Ob:

* Xz: 
    * Ralena (medyczka) "wszystko jest w porządku, wszyscy są zdrowi, nic się nie dzieje"
    * Igor "mówiliście, że nie da się tego pokonać, widzieliście jak zostali na statku, otaczało Was... chciałbym wiedzieć więcej... nie wiadomo jak walczyć... poznać najwięcej jak możliwe"
        * KĄTEM 'opiszcie najbardziej jak to było' by ściągnąć Nihilusa.
* Vz:
    * Ralena: "ja nic nie zrobiłam, nic, nic nie mogłam, nic nie widziałam..."
    * Ralf: (też zaczyna panikować powoli) "to było za duże, to było..."
    * Arnold: "NIE ZADAWAJ TEGO PYTANIA, bo to ściągniesz!" /Arnold ma oczy pełne pustki
* Ob:
    * Arnold manifestuje się jako cień. On jest potworem. 
    * Ralena krzyczy "wiedziałam że nie żyjesz! Byłeś stary! Ale musiałam! Za duży! Tylko tak mogłam uratować statek!"
    * Ralf jest magiem. A Arnold jest cieniem.
    * Lena jest wypłaszczona. Psychotroniczka.
```

Wyjaśnienie: 

* Tr oznacza poziom trudności, inne możliwe wartości to Tp, Ex, H. Po określeniu trudności mogą ale nie muszą występować dodatkowe modyfikatory, dla ciebie istotne jest, że po tym rozpoznasz zapis konfliktu.
* X, V i O to kolejne wyniki konfliktu, przy każdym zapisane są wydarzenia z nimi związane.

-----------Sesja----------

