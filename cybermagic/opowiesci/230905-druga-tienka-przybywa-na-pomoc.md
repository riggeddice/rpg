## Metadane

* title: "Druga tienka przybywa na pomoc"
* threads: salvagerzy-lohalian, rehabilitacja-eleny-samszar
* motives: fish-out-of-water, fish-in-water, gry-polityczne, rodzina-trzyma-sie-razem
* gm: żółw
* players: anadia

## Kontynuacja
### Kampanijna

* [230813 - Jedna tienka przybywa na pomoc](230813-jedna-tienka-przybywa-na-pomoc)
* [230808 - Nauczmy młodego tiena jak żyć](230808-nauczmy-mlodego-tiena-jak-zyc)

### Chronologiczna

* [230808 - Nauczmy młodego tiena jak żyć](230808-nauczmy-mlodego-tiena-jak-zyc)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ""
* THEME & VISION:
    * ?
* Cel strategiczny:
    * Pokazać, że ten Orbiter tutaj to gorszy sort
* Cele poboczne
    * Ukonstytuować podstawowe postacie

### Co się stało i co wiemy

* w okolicy bardzo dużo jednostek Orbitera i Noctis jest rozbitych
* grupa czyszcząca z ramienia Orbitera, z Astralną Flarą - mają podczyścić teren; baza to Lohalian

### Co się stanie (what will happen)

* F1: Samszarowie
    * "Idziesz do wojska" 
    * Próby awansu jej na porucznika, by miała jakkolwiek dobrze
    * Cyprian + świta
* F2: Orbiter na Lohalian
    * Gdzie będziemy spać? + rozmagiczniły się rzeczy.
    * Jest jakaś dziwna aura zdaniem 
    * Coś jest nie tak z fabrykatorami - jedzenie jest po prostu FATALNE
    * Spotkanie z dowódcą, Larnecjatem
    * Estella i Elena, synchronizacja
* F3: Excavation zniszczonej noktiańskiej jednostki
    * Oddział 10+3
    * Tomasz szabruje zwłoki


* S1: spotkanie z dowódcą (wyraźnie zmaltretowany życiem, nie chce tu być)
* S2: gdzie będziemy spać?
* SN: żołnierze traktują ją raczej jak niebezpieczne jajko; książeczka 'jak traktować tienkę'

* SN: spotkanie z inżynierem - nic nie działa, wykryli jednostkę w zimnie. Może tam coś jest.
* SN: wejście na uszkodzoną noktiańską jednostkę, szabrujemy

* SN: odśnieżanie 


### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki
#### 1. Grupa wydzielona Lohalian

* Mikołaj Larnecjat: dowódca grupy wydzielonej sprzątającej Przelotyk Północny; stracił bliskich podczas wojny
    * OCEAN: (E-O-): zamknięty w sobie, rzetelny, spokojny i dobry w powtarzalnych operacjach. "Nie powinienem był przetrwać wojny. Jeszcze Orbiterowi jednak się przydam."
    * VALS: (Conformism(Duty), Humility): jak coś ma zrobić, zrobi i dostarczy. "Nieważne czy mi się podoba, czy nie. Jestem elementem maszyny Orbitera."
    * Core Wound - Lie: "Wszystko, co kochałem zginęło i zostało zniszczone. Jestem sam." - "Nie przywiązuj się. Jest tylko misja."
    * Styl: Spokojny, absolutnie nie inspirujący i cichy miłośnik muzyki klasycznej pochłonięty swoimi myślami, ale wykonujący każde polecenie rzetelnie i sensownie.
    * metakultura: Atarien: "dowódca każe, ja wykonuję. Na tym polega hierarchia. Tak się buduje coś większego niż my sami."
* Rachela Brześniak: czarodziejka Orbitera (katalistka i technomantka), dołączona do pomocy w dekontaminacji terenu; na uboczu, bo jest "dziwna"
    * OCEAN: (N+C+): niezwykle ostrożna z czarowaniem i anomaliami, niewiele mówi i raczej trzyma się sama ze sobą.
    * VALS: (Achievement, Universalism): Skupia się dokładnie na zadaniu, by je wykonać może pracować z każdym. "Dekontaminacja to moje zadanie. Zajmijcie się resztą."
    * Core Wound - Lie: "Współpracowała z żywymi TAI i na jej oczach Rzieza zniszczył jej przyjaciół; myśli że to jej wina" - "Magia MUSI być kontrolowana. Wszystko co anomalne musi być reglamentowane."
    * Styl: Cicha, pełna wewnętrznego ognia. Skupia się na dekontaminacji z żarliwością godną faerilki.
    * metakultura: Faeril: "ciężką pracą da się rozwiązać każdy problem - to tylko kwestia cierpliwości i determinacji"
* Antoni Paklinos: chorąży odpowiedzialny za integrację z ramienia grupy wydzielonej Salvagerów Lohalian
    * OCEAN: (A-C+): niezwykle cierpliwy i nieźle współpracuje z ludźmi, jednocześnie dość agresywny; lubi dawać ludziom szansę i ich testować. "Paklinos to swój chłop; nieco zmierzły i testujący, ale daje radę"
    * VALS: (Security, Benevolence): niezależnie czego nie dostaje i czym się nie zajmuje, kluczowa dla niego jest pomoc innym i zadbanie o ochronę. "On nawet do kibla wchodzi z nożem"
    * Core Wound - Lie: "Zrobiłem to, co należało zrobić i zostałem wygnany na planetę" - "Nic co robimy nie ma znaczenia, ale możemy pomagać w najbliższej okolicy. Od myślenia jest dowództwo."
    * Styl: Dość zaczepny ale pozytywny facet; dużo testuje i zawsze jest 'in your face'. Zaufany i daje radę.
    * metakultura: Atarien: "Oddział jest tyle warty ile jego wytrenowanie i spójność z rozkazami. Co nie jest testowane i szkolone, to się nie dzieje."
* Tomasz Afagrel: kapral grupy wydzielonej pod dowództwem Larnecjata; były przemytnik
    * OCEAN: (E+O+): nie boi się ryzyka i zawsze pierwszy do szydery czy okazji. "Jeśli jest zysk, warto spróbować."
    * VALS: (Hedonism, Power): pierwszy szuka okazji do dorobienia czy dobrej zabawy. Straszny hustler. "Nikt nie dostanie za darmo niczego. Trzeba chcieć i działać."
    * Core Wound - Lie: "Może jestem z bogatego domu, ale wojna zniszczyła wszystko co mieliśmy" - "KAŻDĄ okazję trzeba wykorzystać, WSZYSTKIEGO spróbować - by nie żałować."
    * Styl: Głośny, pełen energii, uśmiech drwiący i szyderczy. Zawsze szuka sposobu na zabawę czy okazję. Lojalny wobec "swoich". Kolekcjonuje fanty.
    * metakultura: Atarien: "moja rodzina miała wszystko i wszystko straciła. Moja kolej odbudowania potęgi rodziny."
* Irek Korniczew: główny inżynier wysłany przez Flarę by zająć się obsługą bazy tymczasowej
    * OCEAN: (C+O+): tysiące drobnych pomysłów i usprawnień, poszukiwanie nowego i wszystko przy poszanowaniu istniejącej maszyny. "Wszystko da się usprawnić i zrobić lepiej. Dojdziemy do tego jak."
    * VALS: (Stimulation, Achievement): coś nowego, coś ciekawego, coś rozwijającego. A nie gówno na patyku. "Te drobne powtarzające się błędy powinna rozwiązać TAI."
    * Core Wound - Lie: "Zawsze inni dostawali wszystko co ciekawe a ja tylko maintenance, mimo że to moja specjalizacja" - "Te wszystkie tematy maintenance to misja karna; nie mogę brać na siebie"
    * Styl: Wiecznie zirytowany, ale pomocny. Narzeka na sprzęt i zadanie. Jednocześnie - jeden z lepszych inżynierów.
    * metakultura: Atarien: "jeśli nie dam rady się wykazać, rozpłynę się w tłumie szarych twarzy"

#### 2. Siły Samszarów

* Cyprian Mirisztalnik: chorąży Aurum, kompetentny w walce i wierny towarzysz Eleny w świecie Orbitera
    * OCEAN (E+ A-): pełen życiowej energii i charyzmy, opiekuńczy ALE arogancki i roszczeniowy. "Kto ma moc, ma obowiązek. Kto ma moc, tego słuchają."
    * VALS: (Conformity, Tradition): głęboko wierzy w znaczenie hierarchii i lojalności. Wierzy też w odpowiednie wyglądanie i zachowywanie się. "Krew mówi sama za siebie."
    * Core Wound - Lie: "Nie będąc arystokratą, wszyscy mnie ignorowali" - "Zapewnię odpowiednią opiekę i kontrolę nad innymi, dzięki czemu mnie nie odrzucą i nie zostawią."
    * Styl: Opiekuńczy i dominujący jednocześnie, kłótliwy ale elegancki. Patrzy na świat przez pryzmat hierarchii i formalnej kontroli.
* Adelajda Kalmiris
    * Core Wound - Lie: "tylko mnie udało się uciec i tylko mnie udało się rodowi Eleny uratować." - "ród Samszar jest inny - to jedyny szlachetny, dobry ród; oni nie pozwalają na zło"

### Scena Zero - impl

.

### Sesja Właściwa - impl

Zenobia Samszar, matka Eleny. 

Elena jest w swoim skrzydle w posiadłości. Układa książki, zajmuje się swoimi tematami - ostatnie dwa tygodnie wszyscy dali jej spokój. Do momentu, jak przyszła do Eleny matka. 

* Zenobia: "Elenko, mam dla Ciebie złą wiadomość" /matka ma zaczerwienione oczy
* Elena: "Mhm? Płakałaś."
* Zenobia: "Elenko, Orbiter poszukuje oficera łącznikowego z Samszarów. Kogoś, kto pomoże zrozumieć co się dzieje w górach, poza Aurum."
* Elena: "M...hm?"
* Zenobia: "Zostałaś wyznaczona na ochotniczkę."
* Elena: "Ochotniczy przymus?"
* Zenobia: "Broniliśmy Cię jak mogliśmy. Robiliśmy..."
* Elena: "Rozumiem, że się nie udało?"
* Zenobia: "Wynegocjowałam tyle, że awansują Cię na porucznika."
    * "żebyś miała jakiś szacunek w Orbiterze, żeby było Ci jakkolwiek dobrze" (Elena nie zna się na niczym)
* Elena: "Mamo, nie byłam w wojsku i nie wiem jak się robi"
* Zenobia: "No nie będą Tobą pomiatali, podobno każą... ZIEMNIAKI obierać! Jakby służby nie było!"
* Elena: "M...hm, ok"
* Zenobia: "Będziesz miała świtę, kucharza, własny oddział... dostaniesz naszych ludzi!"
* Elena: "Mamo, dlaczego kogoś z Samszarów, jest problem z duchami?"
* Zenobia: "Tak, podobno tak. Podobno potrzebny jest ktoś, kto ma wiedzę i potrafi radzić sobie z dziwnymi aurami. To jest bliskie duchów."
* Elena: "I ze WSZYSTKICH Samszarów MNIE wybrali? Interesujące."
* Zenobia: "Nasz ród... nie szanuje Orbitera tak, jak powinien. Uznali to za okazję pozbycia się potencjalnego problemu."
* Elena: "Czyli mnie?"
* Zenobia: "Najpierw planowali wysłać Karolinusa. Potem Floriana. Ale padło na Ciebie."
* Elena: "Czemu nie oni? Karolinus, dobra, wiemy. Nie ma tam dziewczyn."
* Zenobia: "Nie, bo tam Karolinus się nie przyda, w ogóle. Tam potrzebny jest ktoś, kto ma wiedzę o duchach, o aurach, ktoś, kto poradzi sobie z nieznanym. Ty jesteś najlepsza z tej trójki."
* Elena: "Mamo... czy chcesz... widzę, że coś wiesz, chcesz mi coś powiedzieć? Jest coś, co powinnam wiedzieć, czemu Orbiter powinien być przez nas bardziej doceniony?"

Tp (ona jest słabsza, naprawdę zależy jej na Elenie PLUS Elena mówi że się boi co działa mocno na Mamę która chce uspokoić) +3

* Vz: (s) Zenobia powiedziała więcej na temat tego co się dzieje niż można się spodziewać po niej
    * ona wie, że jako porucznik nic to nie da Elenie. Ale przy okazji Elena dostaje chorążego ze sobą. Kompetentnego. Imieniem Cyprian.
    * ród chciał, by awansowała na porucznika, bo nie wyśle 'non-combatant', źle by ród wyglądał (ich zdaniem)
        * zdaniem Zenobii, Orbiter ma to gdzieś, Elena nie będzie walczyć.
    * Orbiter NAPRAWDĘ potrzebuje Samszara. I Elena została wybrana jako najbardziej kompetentna. Z tej trójki. Acz nadal to nie to.
        * pewne osoby chciały ukarać Floriana, Karolinusa i Elenę. I one wygrały politycznie.
        * Florian dałby się zabić. Po prostu. A Karolinus jest tam bezużyteczny. A w Elenie nie ma bohaterskiej kości, nie jest głupia więc najwyżej będzie bezużyteczna.
            * A dla Samszarów nie ma znaczenia czy Orbiter zadowolony czy nie. Aktualni Samszarowie zajęci są Aurum, tematami tutaj. Nie obchodzi ich Orbiter.
* X: Elena dostanie: mundur, świtę, kucharza, WSZYSTKO. W końcu jak wysyłają ją Samszarowie to wysyłają ją godnie. I to byli... sojusznicy Eleny... magiczne namioty by ciepło było... aha, i stopień porucznika.
* Vr: Mama się przyznała. Ona kiedyś walczyła z siłami Orbitera jako sojuszniczka, jako tien Samszar. W wojnie przeciwko Saitaerowi.
    * była we wsparciu i logistyce, zajmowała się m.in. informacjami, odpytywaniem duchów - nie była na polu bitwy, ale była w siłach wsparcia. Jako ochotniczka.
    * rady:
        * "Elenko, oni są... nieokrzesani i nie patrzą na hierarchię, ale to nie są źli ludzie."
        * "zdaniem Mamy, Orbiter jest niedoceniany bo wojna z Saitaerem się skończyła, ale Orbiter DALEJ WALCZY. Walczy ze wszystkim co zagraża Astorii. I teraz potrzebują pomocy."
            * (przyczyna płaczu mamy): ONI potrzebują pomocy, to znaczy, że jest coś nie tak. Mają mało magów. I wysyłają ELENĘ. ELENĘ, niebojową. Na obcy teren.
        * "Obierz te ziemniaki. Nie zachowuj się jak lepsza niż oni. Będą Cię bronić."
        * "Jeśli mówią NIE IDŹ TAM to nie idź tam"
        * "Orbiter jest lojalny swoim. Jeśli pomagasz Orbiterowi, jeśli jesteś im lojalna, jesteś swoja."
* Vz: Elena poprosiła Mamę o list rekomendacyjny który mówi PRAWDĘ.
    * że nie umie walczyć, ale może pomóc w tych i tych obszarach.
    * że nie jest najlepszą kandydatką, ale jest najlepsza jaką Samszarowie dadzą
    * jest chętna by się uczyć i ma dużą wiedzę i jest dobra w wiedzy i zbieraniu danych i wyciąganiu z nich wniosków
* V: Na orbicie, na Astralnej Flarze jest ktoś kto kiedyś pracował z Zenobią. Koleś pamięta Zenobię jako "słodkie, przestraszone ale robi robotę". Więc ma SYMPATIĘ do Eleny. I da znać dowódcy bazy, że Elenie warto dać szansę.

Mama robiła co mogła, ale niewiele mogła. Nikt się nie wstawiał za Eleną i Karolinusem. Nikt ważny. Tylko rodzina. 

Elena na szybko czyta wszystko o Orbiterze co może. Wie też w jaki teren jadą. "Góry Hallarmeng". To jest "morze sargassowe". Tam podczas wojny nadzwyczaj dużo jednostek się rozbiło. Z perspektywy statystycznej za dużo statków się rozbiło. Dlatego tam jest Orbiter. Ma zbadać i pozyskać swoje i noktiańskie technologie które nie mogą wpaść w niepowołane miejsce. Elena dostaje odpowiednio ciepłe i przydatne rzeczy. Mimo, że jej ludzie mówili że ją wyposażą, Elena na podstawie książek itp. zarządziła sama co na pewno ma być. Plus pionierski sprzęt rejestrujący - taki najwyższej klasy, dla saperów.

Elena chce mieć wpływ na to co dostanie. I jest skłonna postawić na szalę podkładkę - TAKIE warunki, potrzeba TAKICH osób a gruby kucharz zamarznie.

Tp (bo Zenobia mocno wspiera Elenę, Elena zapowiedziała, że JEŚLI dostanie tylko to co chce, spoko, będzie grzeczna i da radę. Ale jak jej wpakują - powie że to Ród i zniszczy reputację) +3:

* V (p): Elena dostanie możliwość odrzucenia świty itp, pod warunkiem że zachowuje się tam dobrze i robi im dobrze.
* V (m): Dobrzy, doświadczenie Samszarowie w takich sprawach pomogli skombinować Elenie odpowiedni sprzęt.
* V (x): Elena dostanie servar klasy Chevalier (nie musi nim prowadzić) oraz komandoskę do pomocy, jako agentkę i asystentkę. Nazywa się Adelajda.
    * w Chevalierze: "Elainka-class, ale lepsza"

Spotkanie z Cyprianem. Chorąży Samszarów. Elegant, to widać. Cyprian jest opiekuńczy i jest dobry w walce i przystojny."

* Cyprian: "Tien Samszar, melduję się, by Ci służyć."
* Elena: "Dzień dobry, Cyprianie. Żebyśmy się komunikowali jak na Orbiterze."
* Cyprian: "Tak jest, tien porucznik!" (widząc minę Eleny) "jesteś godna obu tytułów. Noś je z dumą."
* Elena: (mindfuck) "Czemu masz o mnie takie dobre zdanie?"
* Cyprian: "Tien porucznik Samszar! Wasza matka uratowała mojego ojca. Moja kolej, by spłacić dług. Zgłosiłem się na ochotnika. Jesteś przy mnie bezpieczna, tien porucznik Eleno Samszar" /uśmiech amanta
* Elena: (wyciąga rękę) Cieszę się na naszą współpracę.
* Cyprian: (złapał, uścisnął) Ja też, tien porucznik Samszar.

Adelajda wygląda tak jak powinna - wygląda bardzo seksownie. Ale nie z ubrania, a z rysów twarzy, figury itp. Bardzo ładna, ponętna dziewczyna. Acz Elena widzi w jej oczach, że jest starsza niż jej ciało pokazuje. Odpowiednio niepozorna, ale odpowiednio ładna.

Z wielkiej świty Eleny zostały:

* Jedna Adelajda, jako pokojówka / agentka / ochrona (prawdziwa)
* Jeden Cyprian, jako ochrona i chorąży (ten jawny)
* Mnóstwo sprzętu do przetrwania w górach i rzeczy duchozwiązanych.

Mama do Eleny:

* Zenobia: "Pamiętaj, skarbie. Dzwoń, komunikuj się, jak czegoś potrzebujesz, daj znać. Pomożemy."
* Elena: "Dobrze, mamo"
* Zenobia: (chciała coś powiedzieć, ale nie powiedziała) (dystans między nimi jest za duży)
* Elena: (podeszła, dotknęła pozytywnie; nie przytul, ale uspokajający dotyk)
* Zenobia: (przytuliła Elenę, mocno, ale puściła) "Powodzenia, córeczko"
* Elena: (nie pokazała na twarzy 'DOTYKA MNIE!!!', jest profesjonalistką)

Wielki, czarny, pancerny pojazd BEZ godnej nazwy czeka. Orbiter wysłał po oficera łącznikowego SWOJĄ jednostkę.

Elena usadziła się w pojeździe, 5 żołnierzy i jeden pilot. Wszyscy uzbrojeni, wszyscy w lancerach. Wrażenie "WTF" i siły ognia. Nowoczesny sprzęt, żołnierze szczurzą. Ale przez hełmy nie widać ich min.

* Elena: Ile tu lecieliście?
* chorąży: tien Samszar, dwie godziny
* Cyprian: panie chorąży, tien porucznik Samszar ma stopień tien porucznika.
* chorąży: przepraszam, tien porucznik. /lekki sarkazm
* Cyprian: (zadowolony z siebie)
* chorąży: dobrze, startujemy.
* Elena: chcecie rozprostować kości? Herbatę albo coś? Pozwiedzać? Możemy polecieć za godzinę, jeśli chorąży i żołnierze wyrażają taką chęć.
* chorąży: dziękujemy za propozycję, tien porucznik, ale niestety, musimy lecieć. Dowodzenie wzywa. Orbiter nie śpi, więc my też nie będziemy.
* Elena: jednak pozostańmy przy 'tien'.

Dolecieliście, po 2h. Droga jest spoko. Pojazd jest... ma zapach. Zapach niemytego oddziału. Ale jednostka jest solidna. Żołnierze gapią się na Elenę i Adelajdę, myśląc, że nie widać. Elena udaje że nie widzi. Cyprian nie zauważa. Żołnierze komunikują się na jakichś wewnętrznych komunikatorach, więc nic nie słychać. Nie są wrodzy. Po prostu nie wiedzą co robić i chorąży dał im najpewniej rozkazy typu 'morda w kubeł'.

Elena dowiedziała się - dane z Chevaliera - że pojazd ma IFF i identyfikator. Nazywa się "Pojazd VI". Po prostu wow. To najpewniej był czyjś żart.

Na miejscu, jesteście w rozbitej jednostce - Lohalian. Ciekawe - czemu cztery działka pilnujące Lohalian nie są auto tylko mają 4 biedaków. Sam Lohalian jest oświetlony (jak każda placówka). Ale niska widoczność. Jest dzień i mało widać. Jak wielki smog, ale to jest plugawa pogoda. Zaprowadzili Elenę do dowódcy bazy. Ten - komendant Mikołaj Larnecjat - siedzi w nie dość jasnym jak na gusta Eleny pomieszczeniu, które jest nieskazitelnie czyste. Ma mapy, wykresy itp. Co ciekawe, sporo jest analogowych. Rzuciło się Elenie w uszy, że jest cicha muzyka klasyczna. Dobiega z radyjka. Trochę starszy facet.

* Larnecjat: tien Samszar, cieszę się, że Cię widzę /głos bez entuzjazmu (a on jeszcze nie wie!)
* Elena: majorze, ja również. Cieszę się na naszą wspólną współpracę. I żebyśmy zaczęli współpracę mając pełen obraz sytuacji... (daje kopertę) proszę.
* Larnecjat: (czyta szybko, ale 5 minut czyta a Elena stoi) tien Samszar...
* Elena: majorze...
* Larnecjat: poradzisz sobie. Miałaś do czynienia z jakimiś... niedogodnościami?
* Elena: w sensie... w życiu czy w podróży? Podróż była w porządku. W życiu... kuzyn? Co pan major ma na myśli?
* Larnecjat: na przykład, wykrwawiasz się w okopie i nie możesz się ruszyć 3 godziny. I kapie na Ciebie jakiś śluz.
* Elena: em... melduję, panie majorze, że nigdy nie byłam w okopie i nie wykrwawiałam się. A potwory (takie i takie) znam.
* Elena: panie majorze, czy mogę mówić swobodnie?
* Larnecjat: możesz...
* Elena: panie majorze, książki, wiedza z książek, dostęp do źródeł, strategiczne działania, łączenie kropek, duchy, magia - to obszar w którym możemy działać. Dowiemy się, czemu nie działa u Was dobrze elektronika, bo widzę że wiele rzeczy jest analogowa. Z chęcią się nauczę biegania, strzelania i walczenia.
* Larnecjat: tien Samszar, jestem... (myśli jak to powiedzieć) masz wartość dla tej jednostki. Potencjalnie. Mówisz prawdę, zauważasz rzeczy oczywiste i zgodnie z listem się przydasz. Chorąży Paklinos się zajmie Twoją integracją.
* Elena: dobrze. Jestem zainteresowana pomocą z tymi aurami i duchami.
* Larnecjat: doceniam zapał młodości (powiedział wolno i beznamiętnie). Wpierw umieść swoich podwładnych, zapoznaj się z tien Gwozdnik i ona powie Ci do czego doszliśmy.
* Elena: (wychodząc) potrafię też obierać ziemniaki
* Larnecjat: (na twarzy komendanta pojawił się CIEŃ uśmiechu)

## Streszczenie

Rodzice Eleny chronili ją przed 'wygnaniem' do wspierania Orbitera, ale nie udało im się. Orbiter potrzebuje Samszara, Elena się nadaje i dostała od mamy list polecający - bo na Flarze jest ktoś kto kojarzy Zenobię Samszar. Elena jedzie w Góry Hallarmeng. To jest "morze sargassowe". Elena dostanie dobry sprzęt od doświadczonych Samszarów, Chevaliera i Cypriana z Adelajdą. A na miejscu Elena zrobiła dobre wrażenie na komendancie Mikołaju Larnecjacie. Elena się ziemniaków nie boi. Czas zacząć nowe życie.

## Progresja

* Elena Samszar: wsparcie Adelajdy, Cypriana oraz własny Chevalier. I sprzęt do przetrwania w paskudnych warunkach górskich.

## Zasługi

* Elena Samszar: przekonała wszystkich, by jednak nie dawali jej nadmiernych niepotrzebnych rzeczy i pozytywnie podeszła do tematu 'wygnania w góry'. Ma wsparcie matki i list polecający.
* Zenobia Samszar: matka Eleny; wyjaśniła jej sytuację w jakiej się znajduje, kto stoi za Eleną i kto przeciw. Dała Elenie list polecający i wyjaśniła, że ona kiedyś walczyła z siłami Saitaera współpracując z Orbiterem. Zrobiła wszystko co mogła.
* Cyprian Mirisztalnik: chorąży Samszarów, elegancki i opiekuńczy. Dołącza do Eleny by spłacić dług z powodu uratowania jego ojca przez matkę Eleny. Już na starcie podpadł mówiąc o Elenie 'tien porucznik'. Narwany, ale chce pomóc.
* Adelajda Kalmiris: bardzo atrakcyjna, ale jednocześnie doświadczona osoba. Pokojówka i doświadczona agentka i komandoska. Podróżuje chronić Elenę.
* Mikołaj Larnecjat: major, którego ucieszyło proste podejście Eleny do tematu - ma zapał, nie zna się, ale można dać Elenie szansę.

## Frakcje

* Salvagerzy Lohalian: wzbogacili się o tien Elenę Samszar i kontynuują wydobycie i analizę poszczególnych okolicznych artefaktów.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, W
                    1. Góry Hallarmeng
                        1. Przyprzelotyk
                            1. Korony Cmentarne
                                1. Ruina Lohalian
                                1. Dolina Krosadasza

## Czas

* Opóźnienie: 12
* Dni: 2
