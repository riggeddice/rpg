## Metadane

* title: "Dekralotyzacja Asimear"
* threads: legenda-arianny
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [210317 - Arianna podbija Asimear](210317-arianna-podbija-asimear)

### Chronologiczna

* [210317 - Arianna podbija Asimear](210317-arianna-podbija-asimear)

## Punkt zerowy

### Dark Past

.

### Opis sytuacji

Infernia udaje Goldariona, by przetransportować tajną przesyłkę Sowińskich do Jolanty Sowińskiej na planetoidzie Asimear. Tomasz Sowiński na Inferni okazuje się być dużym dzieckiem, łatwo jest wmanipulowany w otwarcie paczki - jest tam nietypowy antynanitkowy Entropik (którego psuje Elena przez błędną integrację). W tle wszystkiego - próba zdobycia niezależnej od Orbitera floty przez Aurum i jeden znikający advancer Kramera na Asimear...

"Płetwal Błękitny" ma na pieńku z "Goldarionem"; by nie złamać konspiracji, Klaudia potraktowała Płetwala Bazyliszkiem. Tymczasem pojawiła się wiadomość, że Arianna chce zniszczyć Asimear i żąda wydania Sowińskiej. Prawdziwa Arianna złamała konspirę, skontaktowała się z Jolantą Sowińską, po czym ją porwała. Okazało się, że Jolanta jest kontrolowana przez kralotycznego pasożyta...

Tematy do rozwiązania:

1. Advancer Kramera --> odzyskać
2. Płetwal Błękitny --> popcorn
3. Tirakal | "Arianna" --> pozbyć się problemu reputacyjnego
4. Sowińscy --> wtf
5. Tomasz i "Monisia" --> wtf
6. kraloth --> ...?

### Postacie

## Punkt zero

.

## Misja właściwa

Pierwsze co Arianna kazała zrobić - zbliżyć Pandorę do Asimear. Jeśli będzie potrzebowała AI-killera lub czegoś superciężkiego, to chce mieć taką możliwość.

Kamil pilnuje Tomasza Sowińskiego w celi. Na polecenie Arianny przekonał go do tego, że jeśli Tomasz chce by nikomu nic się nie stało, musi współpracować.

TrZ+3:

* V: bojaźń Tomasza. Arianna to BICZ BOŻY. Tomasz nie odważy się działać jawnie przeciwko niej.
* X: "Monisia" to Arianna, więc Tomasz wierzy we wszystko co złe się o Ariannie mówi. Jest więc na pokładzie dowodzonym przez potwora.
* VV: Tomasz będzie w pełni współpracował z Arianną. Ze strachu co się stanie wszystkim jeśli odmówi.

Tymczasem Arianna zbiera informacje na temat tego co tu się dzieje. I Tirakal dowodzony przez "Ariannę" zabił już, jak się okazuje, kilkanaście osób - tylko na oczach Arianny (ci górnicy). Ludzi, którzy byli pod wpływem kralotha, ale ludzi. Więc Tirakal na pewno nie chce współpracować z kralothem, to osobna siła.

Ok, zadanie to zadanie. Trzeba dojść do tego, gdzie jest advancer i naprawić sytuację.

Przede wszystkim, Arianna musi mieć wejście na Lazarin - tam powinny być rekordy odnośnie wejść wyjść i wszystkich takich rzeczy. By to osiągnąć, poprosiła ładnie (terrorem) o pomoc Tomasza Sowińskiego. Tomasz zauważył, że on niewiele może. Fakt, ale jest Sowiński. Jolanta była tu przed nim pierwsza. Może Tomaszowi uda się przekonać odpowiednie osoby, że warto z Arianną współpracować... w końcu Arianna jest arcymagiem i _wiadomo_, co ona może zrobić.

Tomasz zdecydował się spróbować przekonać Mariusza Tubalona, że stacja Lazarin zyska ze współpracy z nim i z "Goldarionem"...

Tr+2:

* XV: Mariusz jest podejrzliwy. Ale ok, Tomasz odziedziczył prawa Jolanty, w końcu FAKTYCZNIE przybył tu po niej i dla niej. Nawet Jolanta kiedyś mówiła, że ma prawo tu być.
* V: Tomasz przekonał Mariusza, że jest OK. Może "Goldarion" nie jest statkiem godnym zaufania, ale przecież można ufać JEMU.

Tymczasem Martyn wraz z Klaudią pracują nad czymś co sprawi, że efekty kralotha będą zakłócone. W końcu PORWALI JOLANTĘ. To znaczy, że mają dostęp do próbek kralotha. To, że kraloth podmienił jej Governora na swoje bioformy jedynie sprawia, że mają dostęp do taumotypu i genotypu tego egzemplarza kralotha. A Klaudia i Martyn mają wszystkie narzędzia potrzebne by zrealizować ALERGIZATOR kralotha.

ExMZ+3:

* X: Niestety, trzeba wstrzyknąć to człowiekowi / magowi w ciało. I jest nieprzyjemny.
* V: Efekt alergiczny - da się wykryć wpływ kralotha, acz jest to kosztowne i bolesne dla osoby pod wpływem owych środków.

Klaudia przechwyciła informację, że Lazarin wysłało kuriera na Orbiter z sygnałem SOS. Coś złego się tu dzieje, coś złego zrobili Sowińscy i Tirakal "oszalał" z Arianną na pokładzie. Normalnie byłoby to zabawne, bo Arianna jest faktycznie na Orbiterze... ale tym razem Arianna _faktycznie_ jest w okolicy Asimear i wysoka szansa, że ktoś uzna, że to jest naprawdę Arianna. Więc Arianna kazała po prostu Klaudii przełączyć sygnał - dodać kurierowi informację "opanowałam sytuację" oraz "wiecie co, sekrety orbitera to był głupi pomysł >.>".

Done.

Mając wszystkie potrzebne dostępy na Lazarin, Klaudia obudziła w sobie znowu duszę biurokratki. Znaleźć wszystko na temat naszego advancera co da się znaleźć. 

TrZ+2:

* V: Advancera nie ma na stacji
* V: Jakieś 10 dni temu advancer faktycznie tu był; wraz z Jolantą badali tunele na planetoidzie (możliwości Tirakala)
    * w ramach firmy FuturMin; jakieś 30 osób + sprzęt, wiemy gdzie są.

Arianna z ciężkim sercem zdecydowała się na coś strasznego. Wyciągnęła ciężką artylerię. CEKINY. Nadaje broadcast używając Inferni; nie ma co się ukrywać, skoro niby tu i tak jest. Że jest tu kraloth i ktoś wyciera sobie nią gębę. I że muszą współpracować przeciw kralothowi. Ona współpracuje nawet z Sowińskimi a wiadomo jak jest między nimi. Wiedziała o problemach, więc przybyła z ramienia Orbitera. Kto jest z nią?

ExZ+4:

* X: Uwierzyli, że Arianna taka JEST. Cekiny, duma, wszystko XD. Jak w Sekretach Orbitera.
* V: Będą współpracować z legendarną Arianną Verlen.
* X: Tirakal się przestraszył.
* V: Będzie dyscyplina, bo są przekonani o kralocie.

Czas na potężną koordynację. Lazarin jest dokładnie przebadany i sprawdzony Alergizatorem. Gdy nie ma tam już agentów kralotha, Eustachy przejmuje koordynację taktyczną. 20 komandosów Verlenów pod dowództwem Arianny oraz siły porządkowe Lazarinu, wszystko pod groźbą potężnych dział Inferni. Lockdown - nikt nie przyleci, nikt nie odleci.

Ogłoszenie kto ma kiedy przyjść na badania. Jak nie przychodzi - wjazd ciężkiej piechoty ;-).

Eustachy dowodzi strategią, Klaudia logistyką. Próbują odpowiednio zawęzić grupy osób by na pewno nikt im się nie wymknął. 

ExZ+3:

* XX: Tirakal się ewakuował; uciekł na Płetwala Błękitnego (mówiąc im że ma rzeczy jakie mogą im pomóc itp i przejął kontrolę nad statkiem w końcu cywilnym).
* O: Brutalność - kraloth kazał jednej firemce bronić się do końca. Verlenowie zmasakrowali wszystkich nie tracąc nikogo (27 martwych osób, 11 rannych).
* X: Opinia Krwawej Lady Verlen na Asimear. I Krwawy Lord Sowiński. Opowieści o parze brutalnych morderców z arystokracji masakrujących ofiary kralotha.
* V: Dwa statki cywilne opanowane przez komandosów Verlenów by wyciąć tam kralotyczne siły
* O: Kolejnych kilkadziesiąt rannych i martwych przy eksterminacji i łamaniu sił kralotycznych. Kraloth uciekł do podziemia.

Eustachy jest zadowolony z wyniku działań - po stronie Inferni są może ranni ale nikt nie zginął. Jednak Elena opieprzyła go od góry na dół. Nie byłby w stanie zrobić tego lepiej? Sprawić, że mniej osób zginie? Uratować więcej żyć? Eustachy podszedł do tego planu taktycznego jak do zagadki logicznej - a nie jak do sytuacji gdzie może ratować cenne ludzkie życia. Eustachy dalej jest zadowolony ze swoich sukcesów, ale... mniej. I inaczej. Mógł zrobić więcej...

Dobra. Eustachy zdecydował się porozmawiać z Martyną i przeprosić ją za tą sytuację. Martyna powiedziała, że nie ma problemu - Semla się niedługo sama uruchomi. Eustachy stwierdził, że to niemożliwe.

* E: "Wiem, że się pomyliłaś..."
* M: "Nie, za 1-2h Semla będzie aktywna. Ty widocznie nie wiesz co zrobiliście."

Martyna wyraźnie jest zestresowana. Coś się dzieje. A Semla nie mogła wrócić do działania. A na planetoidzie nie znaleźli Tirakala. Infernia omiotła sensorami Płetwala - statek nie ma żadnych, ale to żadnych możliwości anty-ekranowania jakie mają okręty wojskowe. I Klaudia ma ping. Tirakal jest na pokładzie.

Morrigan d'Tirakal jest na pokładzie i przejmuje kontrolę nad Płetwalem Błękitnym.

Dobra. Potrzebne jest coś co dawno nie było stosowane. Desant. A dokładniej - desant Eleny na Płetwala korzystając z tego, że jest advancerką. Ale to nie będzie takie proste - koloidowy Eidolon nie ma żadnych szans w pokonaniu Tirakala. W sumie _nic_ co ma Infernia nie jest w stanie sensownie pokonać Tirakala. A jest ryzyko, że jeśli użyją dział Inferni, nie będą w stanie sensownie nikogo uratować bo Tirakal może zniszczyć Płetwala.

Ale jest plan. Elena ma umiejętność synchronizacji z sentisiecią i łączyła się kiedyś już z Entropikiem Jolanty zaprojektowanym do walki z nanitkami. Więc Arianna musi użyć fal mentalnych Jolanty by "silniej autoryzować" Elenę do używania Entropika. Dzięki temu Elena ma coś co musi znaleźć - Eidolon -> Entropik i zniszczenie Tirakala.

Więc Arianna wyciąga broń atomową - cekiny. Znowu. By wzmocnić kult Arianny na Inferni.

Arianna próbuje przenieść fale mentalne Jolanty na Elenę, by Elena mogła hybrydowymi falami mentalnymi (swoimi i Jolanty) przejąć kontrolę nad Entropikiem Jolanty.

ExZ+4

* X: Tomasz jest przekonany, że wszyscy zginą a Arianna chce skrzywdzić Jolantę i jego.
* V: jest wavepattern. Elena jest w stanie kontrolować Entropik.

Eustachy niezauważenie desantuje Elenę na pokład Płetwala. I ostrzeliwuje okolicę działkami by zrobić potężną dywersję.

TrMZ+3

* V: Elena niezauważenie jest umieszczona na Płetwalu
* V: dywersja sprawia, że Elenie łatwiej operować na pokładzie Płetwala

Elena infiltruje cholernego Płetwala by znaleźć Entropika i usunąć Tirakala

ExZ+4:

* X: Elena została na moment wykryta, ale się ukryła znowu. Ale Tirakal jej szuka.
* V: Elena ma problem - jest tu aktywny Tirakal i aktywnie jej szuka innymi jednostkami, ale zniknęła i odwróciła uwagę od ludzi.
* V: W ciągu 20 minut Elena dyskretnie znalazła Entropika
* X: niestety, nie udało się Elenie zaskoczyć Tirakala. Zobaczył, jak Elena opuszcza Eidolona i obejmuje Entropika
* V: Elena objęła Entropika i rozwaliła niedalekie nanitkowe jednostki
* X: Tirakal wymanewrował Elenę; ona chce chronić ludzi, on próbuje ją usunąć...
* X: ...i wystrzelił Elenę ze statku robiąc dekompresję pewnego pomieszczenia
* V: Elena podpięła Entropika do Tirakala psychotronicznie. Take THAT.

Mając połączenie i Elenę w kosmosie, Malictrix strzeliła podłym Bazyliszkiem. Morrigan d'Tirakal przestała istnieć. Entropik też umarł.

## Streszczenie

Wiedząc, że Jolanta jest pod wpływem kralotha Infernia sfabrykowała kralotyczny alergizator. Arianna zdradziła się Aesimar i powiedziała, że ma zamiar zniszczyć kralotha z ramienia Orbitera. Udało się kralotha zmiażdżyć i zepchnąć do podziemi Asimear, choć ze sporymi stratami ludzkimi; tymczasem Tirakal uciekł na Płetwala. Elena zinfiltrowała Płetwala, ale nie mogła zniszczyć Tirakala nie krzywdząc ludzi. Destrukcję Tirakala Infernia zostawiła więc Malictrix.

## Progresja

* Arianna Verlen: na Asimear ma opinię "dokładnie taka jak w Sekretach Orbitera". Z cekinami, dumą i wszystkim
* Arianna Verlen: Opinia "Krwawa Lady Verlen" na Asimear po akcji Eustachego i Klaudii z twardym, krwawym odpieraniem kralotha.
* Tomasz Sowiński: Opinia "Krwawy Lord Sowiński" na Asimear po akcji Eustachego i Klaudii z twardym, krwawym odpieraniem kralotha.

### Frakcji

* .

## Zasługi

* Arianna Verlen: manifestuje cekiny - przekonała Lazarin do współpracy przy niszczeniu kralotha (jako "ta z Sekretów Orbitera"), magią przeniosła fale mentalne z Jolanty na Eleny by dać jej możliwość przejęcia Entropika Joli.
* Eustachy Korkoran: taktycznie dowodzi operacją 'dekralotyzacja Asimear', używając sił porządkowych Lazarin i sił Inferni. Potem desantuje Elenę w Eidolonie na Płetwala, z dywersją odwracającą uwagę Morrigan.
* Klaudia Stryk: z Martynem złożyła alergizator kralotha na podstawie próbek Jolanty, znalazła w komputerach Lazarin brak advancera a potem z Eustachym pracowała logistycznie nad zapewnieniem, że kraloth zostanie odparty.
* Kamil Lyraczek: skutecznie nastraszył Tomasza Sowińskiego, żeby ten współpracował z Arianną. Potem - ustawił kult Arianny, by wzmocnić jej moc przy łączeniu brainwave Jolanty i Eleny (by oszukać Entropik).
* Tomasz Sowiński: przerażony Arianną jak cholera, współpracuje z nią by nikt nie ucierpiał. Przekonał Mariusza Tubalona, by ten dał mu uprawnienia takie jak Jolancie.
* Elena Verlen: opieprzyła Eustachego że w ramach działań na Asimear nie optymalizował przeżywalności ludzi. Potem infiltruje Płetwala, przejęła Entropika i wdała się w "taniec" z Tirakalem. Ten grożąc ludziom wymanewrował ją i skończyła w kosmosie - ale podpięła Tirakal do Entropika dając czysty strzał Malictrix.
* Martyn Hiwasser: wraz z Klaudią bardzo skutecznie złożył środek do alergizacji kralotha używając próbek z Jolanty Sowińskiej i wsadzonego w nią pasożyta (w roli Governora).
* Mariusz Tubalon: agent komunikacyjny na Lazarin; nieufny że Tomasz Sowiński faktycznie nie działa pod presją, ale przekazał mu uprawnienia Jolanty. Tomasz go przekonał. Potem - współpracuje z Infernią w ramach dekralotyzacji.
* Llarnagraht: kraloth na Asimear; próbował konfrontacji z siłami Lazarin i Inferni, ale jego agenci zostali zmasakrowani. Oddał chwilowo pozycję i się schował gdzieś w tunelach Asimear.
* Martyna Bianistek: przekazała Eustachemu sygnał, że na Płetwalu jest bardzo źle w dyskretny sposób, ryzykując życie sporej ilości członków załogi. Ale Tirakal był za straszny.
* Malictrix d'Pandora: szczęśliwa jak świnia w błocie; na zlecenie Arianny zrobiła AI-kill i przechodząc przez Entropika Jolanty ("Eleny") trafiła Bazyliszkiem w Tirakala wysmażając Morrigan d'Tirakal. I Entropika też ;-).
* SCA Płetwal Błękitny: miejsce bitwy pomiędzy Entropikiem Eleny i Morrigan d'Tirakal. Szczęśliwie, nie został bardzo poważnie uszkodzony a potem Eustachy i ekipa Inferni pomogli go trochę zreperować.

### Frakcji

* Ród Sowińskich: ich program kosmiczny został uszkodzony przez stratę eksperymentów Jolanty.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Pas Teliriański
                1. Planetoidy Kazimierza
                    1. Planetoida Asimear: znajduje się tam gdzieś ukryty kraloth Llarnagraht; mackami opętywał powoli planetoidę, ale został odparty krwawo przez Infernię i zmuszony do schowania się.
                        1. Stacja Lazarin: centrum dowodzenia akcji antykralotycznej z ramienia Inferni. Zainstalowane nowe zabezpieczenia i detektory antykralotyczne + serum Martyna.

## Czas

* Opóźnienie: 1
* Dni: 9

## Konflikty

* 1 - Kamil pilnuje Tomasza Sowińskiego w celi. Na polecenie Arianny przekonał go do tego, że jeśli Tomasz chce by nikomu nic się nie stało, musi współpracować.
    * TrZ+3
    * VXVV: Tomasz się boi Arianny i będzie z nią współpracował ze strachem przed tym co się stanie z innymi. Ale uważa ją za potwora.
* 2 - Tomasz zdecydował się spróbować przekonać Mariusza Tubalona, że stacja Lazarin zyska ze współpracy z nim i z "Goldarionem"...
    * Tr+2
    * XVV: Mariusz jest podejrzliwy, ale Tomasz "odziedziczył" po Jolancie wszystkie te prawa
* 3 - Klaudia i Martyn mają wszystkie narzędzia potrzebne by zrealizować ALERGIZATOR kralotha. Środek wykrywający osoby pod wpływem TEGO kralotha
    * ExMZ+3
    * XV: Niestety, trzeba to wstrzyknąć w ciało i efekt nieprzyjemny, ale jest efekt alergiczny dla osób pod wpływem kralotha
* 4 - Klaudia obudziła w sobie znowu duszę biurokratki. Znaleźć wszystko na temat naszego advancera co da się znaleźć. Ma dostęp do Lazarin.
    * TrZ+2
    * VV: Advancera nie ma na stacji, działał z Jolantą Sowińską badać tunele na planetoidzie. Czyli jest w łapkach kralotha.
* 5 - Arianna nadaje broadcast używając Inferni; wiedziała o problemach z kralothem, więc przybyła z ramienia Orbitera. Kto jest z nią?
    * ExZ+4
    * XVXV: Uwierzyli że Arianna TAKA JEST z cekinami, dumą i jak w Sekretach. Tirakal się przestraszył i spieprza. Ale - lokalsi będą współpracować i będzie dyscyplina.
* 6 - Znalezienie i odcięcie kralotha; Eustachy dowodzi strategią, Klaudia logistyką. Próbują odpowiednio zawęzić grupy osób by na pewno nikt im się nie wymknął. 
    * ExZ+3
    * XXOXVO: Tirakal evac, okropna brutalność Verlenów, opinia Arianny i Tomasza "krwawi lordowie", kraloth zamknięty gdzieś na planetoidzie
* 7 - Arianna próbuje przenieść fale mentalne Jolanty na Elenę, by Elena mogła hybrydowymi falami mentalnymi (swoimi i Jolanty) przejąć kontrolę nad Entropikiem Jolanty.
    * ExZ+4
    * XV: Tomasz jest przekonany, że Arianna chce go skrzywdzić (i Jolantę). Ale jest wavepattern, Elena może kontrolować cudzy Entropik.
* 8 - Eustachy niezauważenie desantuje Elenę na pokład Płetwala. I ostrzeliwuje okolicę działkami by zrobić potężną dywersję.
    * TrMZ+3
    * VV: Elena Niezauważenie umieszczona na Płetwalu + dywersja daje jej możliwość cichego poruszania się po Płetwalu
* 9 - Elena infiltruje cholernego Płetwala by znaleźć Entropika i usunąć Tirakala
    * ExZ+4
    * XVVXVXXV: Elena wykryta, ale przeskoczyła na Entropika. Bitwa na Płetwalu, by nie narażać ludzi Elena wymanewrowana i wystrzelona w kosmos, ale zlinkowała Tirakala i Entropika - to wystarczyło Malictrix.

## Inne
### Projekt sesji
#### Procedure

* Overarching Theme:
    * gracze mają "kierowanie" (Vision & Purpose); wiedzą gdzie iść i po co tam idą
    * gracze są bardziej zaangażowani w historię, bo widzą, gdzie ich decyzje prowadzą
    * MG wie, które sesje wprowadzać i które usuwać
    * MG wie, w którą stronę stawiać adwersariat
* Achronologia:
    * przez przeskakiwanie do przodu nieważnych wydarzeń oczekuję, że każda sesja jest powiązana z kontekstem (gracze widzą jak się to rozwija)
    * przez cofanie się i uzupełnianie luk w przeszłości oczekuję, że brakujące MG i graczom luki uzupełnimy takimi działaniami
    * przez działanie w przeszłości gracze czują silniejszą relację z postaciami i światem, silniejsza budowa wspólnej historii.
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * graczom bardziej zależy na postaciach i ich powodzeniu
    * więcej potencjalnych wątków i rzeczy które możemy odrzucić przy krzyżykach
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * gracze widzą jak wpływają na świat. Wpływ jest "ich" - stąd tak tragiczne jest gdy "ich" rzeczy są corrupted / destroyed.

#### Result

* Overarching Theme:
    * V&P: sojusz z Sowińskimi przeciwko Krypcie? Pokazanie, że Arianna jest prawdziwa i całkiem fajna?
    * "Arianna Verlen nie istnieje, jest tylko aktorką": Sowińscy nie wierzą w Ariannę. A Arianna jest tu potrzebna.
    * "Lojalna TAI": TAI Morrigan osłoni Jolantę. Ale nie chce umierać, więc udaje Ariannę.
    * "Too far away, so far away...": Morrigan, która zniszczy Asimear. Jolanta, która ma Governor.
    * adwersariat: "ukojenie w mrocznym świecie": Kult Ośmiornicy, rozprzestrzeniający macki.
* Achronologia:
    * N/A.
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * Tomasz Sowiński: pragnie uratować wszystkich, ale nie zna świata. Jest _creepy_
    * Jolanta Sowińska: czarodziejka z niestabilną magią, która ma Governor by zostać homo superior. Wierzy w technologię nad magię.
    * Adam Kurczak: advancer Kramera, który dołączył do Kultu Ośmiornicy.
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * Ośmiornica: chce zdobyć statek kosmiczny "Płetwal Błękitny" Martyny Bianistki i użyć go, by zdobyć inne 

#### Scenes

* Scena 0: pokazanie potęgi Jolanty podczas jednej z gier (quiz + działanie) --> Jolanta jest niesamowicie skuteczna, wszystko pamięta. Neurosprzężenie + governor.
* Scena 1: na orbicie, Martyna Bianistek chce pieniędzy od "Leszerta". Blef, że strzeli. Ale wyśle oddział szturmowy.
* Scena 2: "Arianna": "porwę i zniszczę Jolantę Sowińską". "Zniszczę planetoidę, którą ta Sowińska splugawiła swą obecnością".
