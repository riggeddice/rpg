## Metadane

* title: "Duch nieopiekuńczy"
* threads: deprecated, kult-ośmiornicy
* gm: żółw
* players: mactator, LisaMelisa

## Kontynuacja
### Kampanijna

* [200617 Duch Nieopiekunczy](200617-duch-nieopiekunczy)

### Chronologiczna

* [200617 Duch Nieopiekunczy](200617-duch-nieopiekunczy)

### Scena Zero - impl

.

### Sesja Właściwa - impl

-> pdf.

## Streszczenie

Miasto Żerżuch ma ducha opiekuńczego, który ostatnio zaczął zwalczać ludzi. Wezwano tam Apirakit - rodzinną firmę egzorcystyczną - by naprawili lub usunęli ducha. Okazało się, że pola strasznie śmierdzącej rzeżuchy są inhibitorem strasznie potężnego kralotha śpiącego pod jeziorem. Gdy w miasteczku pojawił się kult kralotyczny, zaczęli pracować nad regeneracją kralotha i dlatego duch opiekuńczy zaczął walczyć z miasteczkiem. Kult został rozbity, kraloth śpi dalej, większość kultu się rozpierzchła w las.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Borys Apirakit: Apirakit - rodzinna firma egzorcystyczna w Drzewcu; zrobił festyn wiary z fajerwerkami i bombę wiary spuszczoną do jeziora.
* Merida Apirakit: Apirakit - rodzinna firma egzorcystyczna w Drzewcu; przejęła kontrolę nad walnym zebraniem i skierowała gniew miasteczka przeciw kultystom kralotha.
* kraloth Asplaghamirnar: uśpiony pod jeziorem w Żerżuchu, wpływa na jezioro dając aspekty lecznicze i mutacyjne. Uśpiony ponownie bombą wiary.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Drzewiec
                        1. Wielkorolnia
                            1. Żerżuch
                                1. Jezioro: pod nim śpi kraloth dając aspekty lecznicze i mutacyjne; uśpiony ponownie bombą wiary
                                1. Gospodarstwa
                                1. Pola uprawne: zawsze ma kilka pól z rzeżuchą, by uśpić kralotha

## Czas

* Chronologia: Zmiażdżenie Inwazji Noctis
* Opóźnienie: 3999
* Dni: 3
