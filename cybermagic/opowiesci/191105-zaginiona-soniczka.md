## Metadane

* title: "Zaginiona soniczka"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [191103 - Kontrpolowanie Pięknotki - pułapka](191103-kontrpolowanie-pieknotki-pulapka)

### Chronologiczna

* [191103 - Kontrpolowanie Pięknotki - pułapka](191103-kontrpolowanie-pieknotki-pulapka)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

brak

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

Ogólnie:

* Mateusz Kardamacz był terminusem.
* Mateusz miał spotkanie z kralothem na akcji z Pięknotką; nie nadawał się do tego - chciał uratować siebie, ale wpadł pod wpływ kralotyczny.
* Mateusz wyszedł z Domu Weterana. Jest to jedyny terminus, który walczył z Ksenią. Znalazł sobie miejsce jako autotrener fitness w Kalbarku.
* Mateusz potrzebuje kralotycznych środków oraz dziewczyn; pragnie zrobić swojego Pygmaliona.

Szczególnie:

* Mateusz kupuje dziewczyny od ludzi Grzymościa oraz z Cieniaszczytu.
* Mateuszowi wyjątkowo spodobała się Mariola - soniczka Ataienne. Porwał ją i wykorzystał w swoim klubie. Odesłał ją do "korekty" do Cieniaszczytu przez ludzi Grzymościa.
* Ataienne odezwała się do jedynej terminuski jaką zna w okolicach Pustogoru, by ta pomogła odnaleźć jej soniczkę.

Dane właściwe:

* Ataienne powiedziała o zniknięciu Marioli
* Mariola nie jest jedyną, która zniknęła; w Miasteczku się mówi o różnych rzeczach

## Misja właściwa

Pięknotka przygotowała się do swojego pierwszego od dawna dnia urlopu. Nie to, że chciała mieć urlop. Karla jej to silnie zasugerowała. Ktoś ma się zająć Chevaleresse, zdaniem Karli - bo ktoś musi. Ale zanim do tego doszło, Pięknotka dostała dalekosiężną prośbę o kontakt, z sił Orbitera. Ataienne.

Sygnał Ataienne jest słaby, ale wyraźny. Jej znajoma zniknęła. Jej sygnał zniknął z linii hipernetu. Nie ma jej w grze. Nie ma jej nigdzie. Przestała istnieć. Zdaniem Ataienne, Mariola prosiła ją o kontakt i zniknęła. A nie robiła niczego złego, ot, 23-letnia fanka. Mieszka w Zaczęstwie, jest bardzo zainteresowana różnego rodzaju muzyką, śpiewaniem... Ataienne uczyła ją śpiewać. Mariola Tralment jest dość 'fierce', potrafi walczyć, zawsze staje w obronie słabszych - Ataienne boi się, że stało jej się coś złego.

Pięknotka poprosiła Ataienne o listę znajomych i przyjaciół. Ataienne przekazała kilka imion i nazwisk - to, co wie. Pięknotka zauważyła, że Ataienne dużo wie odnośnie swojej fanki. To aż ciekawe. Ale Pięknotka odłożyła tą myśl na potem. Pięknotka stwierdziła, że musi się przejść po chałupie Marioli i poszukać śladów.

Bez żadnego problemu Pięknotce udało się dostać do mieszkania Marioli. Mieszkanie jest zamknięte, mieszka na osiedlu Ptasim. Pięknotka nie jest w stanie znaleźć Marioli w hipernecie. Ale jako, że Mariola jest mieszkanką Szczelińca to zawsze można użyć potęgi Barbakanu. Mariusz Trzewń do akcji. Faktycznie, nawet potęgą Barbakanu Mariusz nie jest w stanie jej znaleźć. Ale jest w stanie puścić potężny sygnał detekcyjny - pulse Barbakanu. Normalnie potrzebny jest numer sprawy, ale może się uda... (TrZ: SS).

Mariusz wpadł w kłopoty. Ten impuls przeszkadzał w pewnym śledztwie na którym zależało Karli. Ale Pięknotka dostała kilka ważnych informacji:

* Połączenie z hipernetem zostało wyłączone przy użyciu terminus-grade technology. To implikuje trzy możliwe siły: Kajrat, Grzymość, Barbakan.
* Pięknotka ma ostatnie ruchy dziewczyny. Sygnał zniknął ostatecznie w okolicach Nieużytków Staszka. Pięknotka mniej więcej wie, gdzie.
* Pięknotka ma zamapowane ogólnie kroki dziewczyny; Mariusz przekazał jej dokładne informacje odnośnie ruchów cywila.
* Coś, co jest anomalią w ruchach dziewczyny - cztery dni z rzędu jeździła do Kalbarka. Wcześniej tego nie robiła.
* Zdaniem Ataienne, kontakt z Mariolą był utrudniony od przedwczoraj (dwa dni temu). Gorsze łącze, nie w sosie...

Pięknotka natychmiast poprosiła Tymona o pomoc. Tymon nie odmówił, pojawił się dość szybko. Pięknotka poprosiła Tymona o możliwość wejścia do środka; Tymon otworzył drzwi przeładowując zamek (po czym zablokuje wejście). Pięknotka dalej szuka za śladami - ściąga też Erwina (najlepszego katalistę, jakiego widział świat zdaniem Pięknotki). Erwin szybko spytał o co chodzi - Pięknotka chce znaleźć wszystkie anomalie, dziwne rzeczy. Coś, czego się Erwin by nie spodziewał. A sama Pięknotka szuka za tematami dziewczyńskimi. Co razem wyprowadzili (SS):

* Erwin wykrył coś niespodziewanego - od dwóch dni (wtedy gdy Ataienne powiedziała że coś jest nie tak) aura magiczna dziewczyny była uszkodzona. She was suppressed.
* Dziewczyna walczyła, cierpiała, próbowała wysyłać sygnały, ale przegrywała. Zupełnie nie była kontrolować swojej energii ani swoich ruchów, co się odbiło w aurze.
* Pięknotka natomiast znalazła ulotkę klubu fitness "Piękno" w Kalbarku.
* Erwin powiedział, że słyszał plotki w Miasteczku. Nie o Marioli, ale o tym, że podobno czasami przez okolicę przewożone są dziewczyny - czy to ludzkie, czy czarodziejki. Traktował to jako plotkę, bo sensory Pustogoru by to wykryły (co Pięknotka wie, że niekoniecznie)...
* Pięknotka znalazła jeszcze coś ciekawego - coś, co wskazuje na... konspirację muzyczną, cokolwiek to nie jest. Konspiracja muzyczna powiązana z TAI w której centrum znajduje się Mariola i kilka innych osób (Pięknotka zapamiętała kto).

Pięknotce wszystko wskazuje na to, że źródłem akcji może być Grzymość. Teraz, jak nie ma Kajrata, Grzymość nie ma balansu. Pięknotka jest praktycznie pewna, że Grzymość to zrobił. Pięknotka zebrała z chaty rzeczy typu włosy, rzeczy, które można wykorzystać. Personal belongings. Żeby było. Pięknotka wspomagana mocą Erwina szuka, gdzie w chwili obecnej może znajdować się biedna Mariola. Też proximity tracker. Pięknotka chce ją jak najszybciej zlokalizować. (TrMZ+4:17,3,5=SM). Udało się - ale Skażenie sprawiło, że wszyscy wyglądają jak Mariola. Erwin, Pięknotka i Tymon...

Tymon natychmiast włączył power suit. Superciężki servar Entropik się uruchomił. "Mariolka" prawie nie utrzymała się w pancerzu. Tymon jest nieszczęśliwy. Erwin chichocze. Nutka chichocze. Pięknotka udaje powagę.

Pięknotka ma do wyboru ratować dziewczynę lub odzyskać tajemniczy sprzęt. Ważniejsza jest dziewczyna. Sprzęt może w końcu być gdzieś w pobliżu dziewczyny. Dziewczyna znajduje się gdzieś w bunkrach w okolicach Esuriit; w Czółenku. Pięknotka wie dokładnie w których bunkrach. Nie wzięła Erwina (combat matter). Tymon... Tymon jest dobrym rozwiązaniem. Entropik Tymona to servar zdolny do walki i pokonania Cienia. Zwłaszcza w rękach szturmowego paladyna którym jest Tymon.

Całe rozproszenie miejsc, wszystkie te technologie i działania - wskazują zdecydowanie na coś większego niż tylko pojedynczy agent. Ale celem Pięknotki jest w tej chwili uratowanie Marioli. Pięknotka i Tymon poszli na bój.

Tymon na zewnątrz. Pięknotka idzie pierwsza. [właśnie tory przestały działać] Korzystając, że wygląda jak Mariola, Pięknotka idzie tam jak Mariola - ma nadzieję, że ktoś ją złapie. Zbliżyła się do bunkra. Od razu uderzyła ją fala emocjonalna. To połączenie rozpaczy Esuriit, pragnienia bycia posłuszną oraz radości. Próba przeładowania umysłu. Pięknotka ma systemy terminuskie; (Tp:S). Pięknotka powstrzymała się; Cień niebezpiecznie się poruszył, ale Pięknotka go kontroluje.

Złapali i zaciągnęli Pięknotkę do innych dziewczyn. Jest ich sześć - czterech uzbrojonych, jeden oficer i jeden lekarz. Dziewczyn jest pięć, są łamane używając Esuriit i przeładowaniem emocjonalnym. Pięknotka dała się tam zaciągnąć i zaczęła syntetyzować usypiające środki chemicznie swoim organizmem.

Zdjęła ich bez walki.

Tor: 10

* (10) Nie uda się przyskrzynić nikogo ważnego od Grzymościa
* (15) Transport do Cieniaszczytu (nie uda się znaleźć trasy przerzutowej)
* (20) Dotarła do Cieniaszczytu (trzeba zrobić akcję ratunkową tam)
* (25) Mariola jest stracona; zniknęła w otchłani Cieniaszczytu

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Ataienne poprosiła Pięknotkę o znalezienie zaginionej fanki. Pięknotka zaczęła śledztwo i doszła do tego, że Mariola została porwana przez gang przerzucający dziewczyny do Cieniaszczytu. Erwin słyszał takie plotki. Pięknotka zlokalizowała i usunęła komórkę tego gangu; śledztwo pokazało na autoklub fitness jako jedno interesujące miejsce. Najpewniej nie to jest miejscem "zła", ale warto sprawdzić.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: posłuchała prośby Ataienne, nie wierząc o co chodzi - i rozwiązała ring transportujący dziewczyny do Cieniaszczytu. Dzięki temu mają ślad na oficera Grzymościa.
* Ataienne: przestraszyła się o Mariolę (swoją soniczkę która zaginęła) i udała się z prośbą o pomoc do Pięknotki. Przypadkowo, Ataienne wykryła gang przerzutowy dziewczyn do Cieniaszczytu.
* Mariola Tralment: soniczka Ataienne. Poszła do autoklubu fitness i następne co pamięta to - spotkała się z ludźmi Grzymościa którzy zaczęli ją łamać by przetransportować na "fazę drugą".
* Mariusz Trzewń: na prośbę Pięknotki sprawdził silnym sygnałem obecność Marioli, przez co rozwalił Karli jedno tajne śledztwo. ALE - uratowali pięć dziewczyn. Więc w sumie Karla go nie zabije.
* Tymon Grubosz: dewastująca maszyna zniszczenia, który przez Efekt Skażenia wygląda jak słodka Mariolka. Nieszczęśliwy, ale poszedł szturmować.
* Erwin Galilien: najlepszy katalista. Wykrył po starej aurze co było nie tak z Mariolą oraz pomógł Pięknotce rozwikłać zagadkę. Gdy potrzebny jest arcykatalista, Erwin jest na miejscu.
* Mateusz Kardamacz: właściciel autoklubu fitness w Kalbarku.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Eksterior
                                    1. Miasteczko: pojawiają się tu plotki o gangu przerzucającym dziewczyny do Cieniaszczytu.
                            1. Zaczęstwo
                                1. Osiedle Ptasie: tu mieszka Mariola, soniczka Ataienne.
                                1. Nieużytki Staszka: tutaj siły Grzymościa mają gdzieś bazę, dzięki której wyłączają hipernet metodą terminus-grade.
                            1. Czółenko
                                1. Bunkry: tutaj "korygowano" dziewczyny do transportu do Cieniaszczytu.
                        1. Powiat Jastrzębski
                            1. Kalbark
                                1. Autoklub Piękna: Mariola tam poszła, spotkała się z Mateuszem i ucierpiała. Potem wpadła w ręce Grzymościa na "korektę".

## Czas

* Opóźnienie: 1
* Dni: 1
