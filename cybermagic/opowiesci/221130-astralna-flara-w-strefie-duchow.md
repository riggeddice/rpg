## Metadane

* title: "Astralna Flara w strefie duchów"
* threads: historia-arianny
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [221123 - Egzotyczna Piękność na Astralnej Flarze](221123-egzotyczna-pieknosc-na-astralnej-flarze)

### Chronologiczna

* [221123 - Egzotyczna Piękność na Astralnej Flarze](221123-egzotyczna-pieknosc-na-astralnej-flarze)

## Plan sesji

### Fiszki
#### 1. Astralna Flara (55 osób max)
##### 1.1. Dowodzenie

* Arianna Verlen: kapitan
* Daria Czarnewik: engineering officer, (4 inż pod nią)
* Alezja Dumorin: eks-kapitan, Orbiter, 
    * ENCAO:  +0--0 |Amoralna, skuteczna| VALS: Hedonism, Face, Power| DRIVE: Wędrowny mistrz Ryu
* Władawiec Diakon: pierwszy oficer, tien, (p.o. Stefana) 
    * ENCAO:  +-0-0 |Intrygancki;;Żywy wulkan| VALS: Hedonism, Self-direction| DRIVE: Follow My Dreams, Korupcja anioła
* Grażyna Burgacz: logistyka i sprzęt (officer), tien (3 osoby pod nią) 
    * ENCAO:  -0+-- |Powściągliwa i 'nudna';;Ascetyczna| VALS: Humility, Tradition| DRIVE: Starszy Brat
* Arnulf Perikas: fabrykacja i produkcja (officer), tien (2 inż pod nim) 
    * ENCAO:  0-0-- |Hardheaded;;Napuszony| VALS: Tradition, Family| DRIVE: Wzbudzenie zachwytu
* Maja Samszar: comms officer, tien (p.o. Klarysy jak K. nie może) 
    * ENCAO:  0+-0- |Moralizatorska| VALS: Stimulation, Conformity >> Power| DRIVE: Długi
* Kajetan Kircznik: medical officer (5 pod nim) (czarny, afro, paw, augmentacje bio)
    * ENCAO:  +-0+0 |Anarchistyczny;;Stabilny emocjonalnie;;Z poczuciem humoru| VALS: Self-direction >> Power| DRIVE: Jestem unikalny

##### 1.2. Operacja

* Klarysa Jirnik: artillery, (p.o. Mai jak Maja nie może) 
    * ENCAO:  00+-- |Wymagający;;Małostkowy| VALS: Tradition, Conformity|DRIVE: Sprawiedliwość
* Tomasz Ruppok: starszy mat (13 osób + 2 starszych)
    * ENCAO:  +0--- |Mało punktualny;;Kłótliwy;;W przejaskrawiony sposób okazuje uczucia| VALS: Hedonism >> Achievement, Family| DRIVE: Apokalipta
* Rufus Warkoczyk: starszy mat (13 osób + 2 starszych)
    * ENCAO:  0-+-0 |Zarozumiały;;Bezkompromisowy, niemożliwy do zatrzymania;;Zorganizowany| VALS: Self-direction, Power| DRIVE: Odkrycie konspiracji ("ktoś nas sabotuje")
* Marcelina Trzęsiel: inżynier syntezy (pod Arnulfem), pod opieką tien Terienaka
    * ENCAO:  0+0-+ |Małostkowa;;Dramatic shifts in mood;;Różnorodność form| VALS: Hedonism, Achievement >> Security| DRIVE: "Co za tą górą"
* Elena Verlen: pilot / advancer

##### 1.3. Infiltracja / Starcie

* Gerwazy Kircznik: sierżant marine, Orbiter
    * ENCAO:  +0+-0 |Głośny;;Honorowy| VALS: Humility, Face| DRIVE: Lokalny społecznik
* Erwin Pies: kapral marine, Orbiter
    * ENCAO:  -0-+- |Skryty;;Wiecznie zagubiony;;Praktyczny| VALS: Tradition, Stimulation >> Power| DRIVE: Ochraniać słabszych
* Szymon Wanad: marine, Orbiter
* Marcel Kulgard: marine, Orbiter, stary znajomy Arianny
    * ENCAO:  000-+ |Demotywuje innych;;Innowacyjny| VALS: Achievement, Face| DRIVE: Zbudować legacy
* Tomasz Dojnicz: marine, Orbiter
    * ENCAO:  --0+0 |Nie znosi być w centrum uwagi;;Dusza towarzystwa| VALS: Conformity, Hedonism >> Power| DRIVE: Arlekin Maytag
* Szczepan Myrczek: advancer, tien, seilita,
    * ENCAO:  --+0- |Stabilny emocjonalnie;;Zawsze bardzo zajęty| VALS: Face, Achievement, Hedonism >> Self-direction| DRIVE: Zasady są święte
* Mariusz Bulterier: advancer, tien, seilita, sybrianin 
    * ENCAO:  0-+-- |Prostolinijny i otwarty;;Nie kłania się nikomu;;Uroczysty i poważny| VALS: Humility, Tradition| DRIVE: Nawracanie
* Hubert Kerwelenios: advancer, sarderyta
    * ENCAO:  -0+-0 |Kontemplacyjny, refleksyjny;;Kompleks paladyna| VALS: Humility, Tradition >> Stimulation| DRIVE: Odbudowa i odnowa

##### 1.4. Other

* Ellarina Samarintael: Egzotyczna Piękność
    * ENCAO:  +-00+ | Spontaniczna;; Chipper;; | VALS: Power, Hedonism >> Family | DRIVE: Wyciągnąć kogoś z bagna, dobrze wyjść za mąż

#### 2. Athamarein, korweta

* Gabriel Lodowiec: komodor-in-training
    * ENCAO: 0+-+0 |Spokojna fasada;;Kontrolowany przez emocje | VALS: Tradition, Security >> Power, Face| DRIVE: Procedury, poprawność systemu, kaganek oświaty.
* Leszek Kurzmin: dowódca jednostki
    * ENCAO: 0-+++ |
* Alan Nierkamin: eks-lokalny przewodnik, advancer
    * ENCAO: ++-0- |Nietolerancyjny, musi być tak jak uważa;;Dokładnie przemyśli wszystko zanim coś powie| VALS: Power, Achievement >> Tradition| DRIVE: wprowadzi Orbiter na Nonarion

#### 3. Nonarion Nadziei (stacja cywilna)

* Leo Kasztop: sprzedawca sekretów na Nonarionie (atarienin)
    * ENCAO:  0-+00 | Intrygancki, polityka;;Nie odracza| VALS: Self-direction >> Stimulation, Tradition| DRIVE: Wygrać w rywalizacji)
* .Franciszek, enforcer Nonariona (atarienin)
    * ENCAO:  +00-- | Lubi rutynę;;Uszczypliwy i zgryźliwy;;Lubi wyzwania | VALS: Achievement >> Security| DRIVE: Komfortowe życie
* .Wojciech, agent Aureliona (faeril)
    * ENCAO:  ---+0 |Bezbarwny, przezroczysty;;Niemożliwy do ruszenia| VALS: Power, Humility >> Tradition | DRIVE: Supremacja Aureliona

#### 4. Domina Lucis

* Sarian Xadaar: pierwszy oficer i dowódca Dominy Lucis (dekadianin) 
    * ENCAO:  0-+00 |Niemożliwy do zatrzymania;;Lojalny i oddany grupie| VALS: Benevolence, Tradition >> Face| DRIVE: Niezłomna Forteca
    * "Victorious, to the end!!!"
* Kirea Rialirat: advancer Dominy Lucis (savaranka)
    * ENCAO:  --+00 |Odporna na stres;;Introspektywna| VALS: Humility, Conformity | DRIVE: Potrzeba samotności i ciszy; problemy z ludźmi i towarzystwem
* Axel Nargan: zainfekowany kapitan Dominy Lucis (dekadianin)
    * ENCAO:  00+-- |Agresywny;; Bezpośredni| VALS: Security, Power | DRIVE: Zwyciężyć w sporze
* Akara Marilinias: inżynier i dziewczyna Franka Mgrota z Orbitera; zainfekowana (żywa broń)
    * ENCAO:  +0--- |Spontaniczna;;Lubi rutynę| VALS: Achievement >> Stimulation| DRIVE: "The Modern End"
    * "Będziecie cierpieć jak cierpię ja"
* 3*serpentis
* 200*"cywil"

### Theme & Vision

* Kampania
    * Problemy kulturowe; Orbiter nie respektuje tego co wypracował Nonarion
        * Ratowanie zniszczonego statku - TAI też potrzebuje pomocy (TAI Mirtaela d'Hadiah Emas)
        * Wyjście na pokład Nonariona kończy się... źle
    * Daria nie ma łatwego powrotu
    * Władawiec corruptuje Elenę
* Ta sesja
    * Domina Noctis - ostatnia ukryta baza Orbitera w terenie
    * Tristania "Let's celebrate the modern end" + "Aphelion"

### Co się wydarzyło KIEDYŚ

* Domina Lucis, support ship noctis dała radę schować się na planetoidzie TKO-4271
    * Sarian Xadaar przejął dowodzenie by utrzymać i wesprzeć siły Noctis; zaprojektował operację "ghost ship"
    * Trzech serpentisów zostało uśpionych, łącznie z wszystkimi ludźmi w fatalnym stanie - w tym ich kapitan (broń biologiczna)
* Domina Lucis dała radę ufortyfikować pozycję i zapewnić dość działań z fissili, wody itp. Ma grazery i blueprinty.
* Z biegiem czasu inne jednostki przestały latać w tym terenie, bo "duchy".
    * Domina Lucis zaczęła polować na statki niewolnicze. Zawsze ta sama procedura - 1 serpentis na pokład, eksterminacja, uwolnienie niewolników, statek -> TKO-4231.

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

* Gabriel Lodowiec nie chce pozwolić na to, by jakaś dziwna anomalia była na tym terenie a on nic nie wie. 

### Co się stanie (what will happen)

* S0_1: Daria wysłana by rozpytać i dowiedzieć się więcej o Strefie Duchów i planetoidzie TKO-4271
    * dobre surowce ale nie idealne; wszyscy się boją
* S0_1: Arianna opanowuje lodową anomalię na planetoidzie Kazmirian
* S1: Elena jest w stanie złapać i przechwycić Kireę
* S1: stan jednostki "Miraż Obfitości": 
    * TAI uśpiona, banki danych wycięte, wszystko zabrane, ślady śmierci na pokładzie ale nikt wśród niewolników
    * sfałszowane sygnały ludzi strzelających do siebie i robiących sobie krzywdę po śpiewie syreny
* S1: planetoidy są grazerowane
* S1: "duchy"
    * creepy message: "nie zbliżajcie się, Kravaen -> wszystko w porządku, możecie przyjść"
    * "nieumarły" synt na planetoidzie, nagi człowiek który się uśmiecha
    * holoprojekcje ludzi wychodzących w kosmos po śpiewie syren zapisane w bankach "Miraża Obfitości"
* SN: kontratak Dominy Lucis
    * infekcja i atak bronią biologiczną (żywy agent), za czym idzie dwóch serpentisów i trzech innych agentów
    * eksplozje poukrywane w skałach powodujące flak
    * salwa rakietowa

### Sukces graczy (when you win)

* przetrwanie i wycofanie się

## Sesja właściwa
### Wydarzenia przedsesjowe

.

### SPECJALNE ZASADY SESJI

.

### Scena Zero - impl

Po opanowaniu planetoidy Kazmirian (baza Orbitera) komodor poprosił do siebie Ariannę.

* G: kapitan Verlen, czy wierzysz w duchy?
* A: niespecjalnie, komodorze - acz dziwne rzeczy mogą się pojawić w świecie anomalnym
* G: czy widzisz cokolwiek co może zagrozić naszym planom na tym terenie jeśli wyczyścimy strefę 'nieumarłych' i udostępnimy ją lokalsom.
* A: niespecjalnie
* G: to będzie nasz następny ruch. Żadna jednostka w okolicy nie ma siły ognia Athamarein ani tylu marines. Poradzimy sobie, bo my jesteśmy Orbiterem a oni nie
* A: nic ponadprzeciętnie dziwnego
* G: wycofamy się wtedy. Przygotuj plan jak możesz się przydać. Opracuj, jak możemy zbadać strefę śmierci. Jeśli da się wysłać Twoją agentkę i dowiedzieć więcej na temat strefy śmierci, to może być to dobry pomysł...

Daria podbija do Arianny, niedługo po sprawie z Hadiah Emas.

* D: pani kapitan, mam sugestię
* A: tak, Dario?
* D: może wezmę wahadłowiec, weźmiemy ludzi i jak mi łażą po stacji to nie mogę naprawiać uszkodzeń a zawsze możemy wysłać nowy wahadłowiec po właściciela i przyda się parę rzeczy zdobyć ze stacji
* A: sensowne by nie siedzieli tu nieproduktywnie

Odstawiamy Hadiah Emas i wysyłamy na Nonarion Darię. I dostaje pintkę i Szymona Wanada do pomocy. Darii wszyscy są niesamowicie wdzięczni - i to widać. Darii i Orbiterowi. 

* Kapitan nieco narzeknął Darii "ech, taki urobek stracony... ten komodor nie rozumie, prawda?" 
* Daria: "nie rozumie"
* Kapitan: "no ale jak ich uwolnię, to co oni zrobią?"
* Daria: "ja to rozumiem"
* Kapitan: "i tak taniej, niż gdybym miał zapłacić za te naprawy, miałem szczęście" /poweselał 
* Daria: "no nie?"

Daria będąc na Nonarionie załatwia wszystko co trzeba. Ale przy okazji ma załatwić info o strefie śmierci. Przede wszystkim, Daria idzie do Leo.

* L: "Jesteś wcześniej niż myślałem. Podoba wam się planetoida?"
* D: "Odprowadziliśmy Hadiah Emas, naprawiliśmy go - wpadł w anomalię" /streszcza
* L: "Isigtand zniszczony?"
* D: "W znacznym stopniu"
* L: "Są winni pieniądze niemiłych ludziom... może Orbiter chce ich kupić?"
* D: "Nasz komodor by nie zrozumiał."
* L: "To może mu nie mówmy że widziałem noktianina na tej stacji."
* D: "Ja mu nie powiem"
* D: (podrzuca Leo próbki rzeczy na handel)
* L: "Macie... potrzebujecie przede wszystkim irianium, jako paliwo?"
* D: "Tak, tego szukamy"
* L: "Zapewnię Wam dobry kanał"
* D: "Strefa śmierci?"

Leo ma pewne informacje, ale Daria musi je filtrować. Leo ma sporo, jest brokerem - NIE ZBLIŻAJCIE SIĘ TAM. To niebezpieczne. Leo opowiedział mniej więcej co wie:

* Strefa Śmierci jest dookoła planetoidy TKO-4271, 15 min. impulsowo od Kazmirian.
* CZASAMI wlatują tam statki i nic się nie dzieje. Ale te statki widzą duchy.
    * widzą echa przeszłości. Dziwne komunikaty. Jeden statek widział śmierć innego - "Miraż Obfitości" - ludzie WYSZLI w próżnię bez skafandra. Podobno jest śpiew syreny.
    * są tam wraki statków, na TKO-4271. Co ciekawe, to statki niewolnicze. Hipoteza taka, że duchy wzywają serca ludzi, którzy pragną wolności.
    * Daria próbuje jako laik ustalić więcej o tym co to może być, jak to działa...

Tr Z (Leo Darię lubi) +3:

* V: Leo powiedział Darii:
    * Wszystkie te rzeczy mogły być sfabrykowane. Kupił nagrania by się upewnić.
    * Miraż Obfitości zniknął WCZEŚNIEJ. 2 tygodnie wcześniej. Mógł mieć dość zasobów - ale mógł nie mieć. Lub nawet coś tego typu.
    * Nagrania to nagrania. Wszystko da się... zrobić
    * "jeśli ktoś TAK się stara, to on nie ma zamiaru wchodzić im w szkodę. To Anomalia Kolapsu, są inne planetoidy."
* V: Leo jest skłonny Darii odsprzedać nagrania po uczciwym koszcie po którym je kupił. Daria zrozumiała jak zobaczyła koszt. To było DROGIE.
    * Daria wydała na to kasę XD

Daria jeszcze powiedziała Leo coś ważnego - "ostrzeż innych dyskretnie. Niech nie pozwolą ludziom z Orbitera ładować się na statki. Nasz komodor nie lubi TAI.". Leo się zasępił "Orbiter niewoli TAI? Znowu?" Daria "nie wiem czy niewoli, ale ten konkretny komodor ma coś przeciwko naszym TAI." Leo "dobrze, ostrzegę wszystkie jednostki. Miałem nadzieję, że Orbiterowi przeszło. No ale jest powód czemu Ogden i Nastia opuścili Orbiter..."

### Sesja Właściwa - impl

Frank Mgrot jest na Flarze. Jest większa i ma celę.

Daria w końcu wróciła i Arianna dostała raport. I pliki. I seria plotek które Leo zebrał. Wanad wzruszył ramionami - nic się nie działo. Musiał jednego kolesia pobić, ale to było normalne. "Pani kapitan, lubię tą stację" - Wanad.

Arianna chce, by Maja przeanalizowała nagrania i wyłowiła anomalne sygnały. Coś właściwego dla tej anomalii.

Ex (dobrze złożone, Maja jest 'nowa') Z (potęga psychotroniki Persefony + Maja szuka bardzo dokładnie) +3:

* X: Maja nie może dostać JASNEJ sygnatury z danych
* X: Arianna Mai może uwierzyć, ale nikt inny nie da rady - nie ma dowodów.
* X: dane wyglądają na bezwartościowe i SĄ OFICJALNIE bezwartościowe - tylko, że Maja będąc z Aurum ma inne techniki. Orbiter jest ROZCZAROWANY że Maja znalazła takie bezsensowne głupoty.
    * Maja: "Leo zapewnił nam dane z czujników i statystyki różnych jednostek. I te jednostki które miały 'dobre' dane miały na statku oficerów którzy byli noktianami LUB TAI mające wyższy poziom psychotroniczny. Ale nieoficjalnie. Nie chodzi o "pochodzenie stricte noktiańskie" a o "sympatyków noktiańskich"
    * Maja: Inne statki też się wydostały. Wszystkie dwa. I nigdy nie wleciały głęboko. One się odstraszyły przez te nagrania itp.
* Vz: Maja ma WIĘCEJ wniosków
    * statki niewolnicze NIGDY nie wleciały w ten teren. Trasa 3/4 była w innych miejscach. Czwarty leciał blisko i on był pierwszym zniszczonym.
    * predyktory analityczne mówią o niskim prawdopodobieństwie teorii Mai. Poniżej 10%.
    * plotki o DUCHACH pochodzą ze statków noktiańskich. Plotki o ZOMBIE ze statków nienoktiańskich

I Arianna wie, że jak takie dowody przedstawi Lodowcowi to on spojrzy na nią jak na idiotkę. Czyżbyśmy mieli do czynienia ze źle poblokowanymi neikatiańskimi TAI które są przejmowane przez coś? Maja ma gwiazdki w oczach - WALKA Z NOKTIANAMI, WROGAMI LUDZKOŚCI!

Arianna prosi Maję, by ta zachowała dyskrecję. Chce je zweryfikować zanim trafią do ogólnej wiadomości.

Czas odwiedzić komodora Lodowca

* A: "Mam pomysł, może mieć pan pewne obiekcje co do niego. Mój łącznościowiec wykrył, że tam może być sygnał, wirus który wpływa na neikatiańskie TAI mocno - sprowadzanie załóg na niebezpieczne tory. Dobrze by to było zweryfikować; mamy jedną TAI tego typu (Isigtand). Można ją aktywować i zobaczyć czy zadziała. Śpiew Syreny."
* G: "Nie spodziewałem się, że śpiew syreny ma jakiekolwiek znaczenie"
* A: "Może mieć. Wszystkie statki które uciekły o tym mówiły. WSZYSCY mówią to samo. Coś się pojawiło. Pytanie jaką ma naturę. Niebepieczny czy dodatkowy element."
* G: "Warto spróbować, nie tracimy. Reanimuj TAI Isigtand. Zrobimy ten eksperyment, potem pomyślimy co dalej. Zamontujemy to TAI jako doczepkę do Athamarein. Nie wyślę jednostki wsparcia przodem."
* A: "Możemy oszukać TAI; niech myśli że ma kontrolę nad statkiem i wysłało output jak postąpiło"
* G: "Podoba mi się ten pomysł. Zróbmy to ostrożnie."

Arianna chce całokształt skonsultować z Darią.

* A: "Musimy reanimować TAI Isigtand by znaleźć syreni śpiew i musisz ją reanimować. Możemy być w stanie ją odczepić i przetransportować na Nonarion. Może da się to zrobić by komodor nie zauważył. Więc ta TAI należała do Twojej przyjaciółki, jest martwa i może zostać martwa ale jak ją wskrzesisz i użyjemy ją do zebranych danych masz szansę by mogła być przemontowana na kolejny statek."
* D: "Mm /dezaprobata i nieufność"
* A: "I tak ktoś to zrobi dla mnie. Ktoś inny zrobi jej większą krzywdę. Nie chcę tego."
* D: "Ok... czy komodor wie, że ta TAI ma być ożywiona?"
* A: "Tak"
* D: "Więc potem chce by nie była żywa."
* A: "Nie da się złożyć obejścia by się wydawała martwa?"
* D: "Nie jestem psychotronikiem."
* D: "Dobrze, zrobię to" /Daria ma PLAN.

Daria nie tylko naprawia TAI, ale do tego wszystkiego chce zamontować selfdestruct switch, by wyglądało, jakby TAI pod wpływem sygnałów po prostu przestała działać. Martwa TAI nie będzie Ograniczona. Daria współpracuje z tą TAI. A TAI nazywa się "Zarralea". Nie jest może specjalnie bystra jak na TAI, ale ładnie śpiewa.

Tr Z (Daria współpracuje z Zarraleą) +3:

* X: koszt naprawy i regeneracji jest widoczny. Isigtand dostał wpiernicz.
* X: Zarralea została uszkodzona. Nie ma pełni mocy psychotronicznej. Jest głupsza.
* X: Zarralea niestety nie nadaje się do reanimacji.

Arianna wie, że Daria się starała. Ale Lodowiec ma podejrzenia, że Daria go sabotowała.

Plan B Arianny - robimy odbiornik z martwej Zarralei oraz z resztek Isigtand. Przynajmniej coś się uda zrobić. 

Tr Z (dobrej klasy części + destruktywność) +3:

* X: Ellarina będzie nieszczęśliwa. Ona lubiła Zarraleę. A to jest babranie się w jej zwłokach. Dobrze, WIE jak jest, ale to jak inżynieria na piesku domowym.
* X: Duża strata części Isigtand. Po prostu niekompatybilność sprzętu, niedoświadczenie inżynierów... to nie są eksperci tego terenu a tak się starają że robią błędy.
* X: Udało się zrobić odbiornik. Ten odbiornik nie jest idealny, da się go wykryć jako zrobiony ze zniszczonej TAI. Ale DZIAŁA. 'Vlad Palownik leci Cię wykryć'

WRACAMY DO LODOWCA. 

* G: "Widzę, że jednak udało się to złożyć."
* A: "Generalnie, tak. Acz moi inżynierowie twierdzą, że ma dziwne sygnatury. Każdy wie że to martwa TAI."
* G: "Recykling. Noktianie to lubią."
* A: "Nie wiem co z ludźmi z tego sektora."
* G: /silna irytacja "Co. Proponujesz. To TWÓJ inżynier."
* A: "Nie trzymajmy tego na naszej jednostce. Namierzy się to to wiadomo o co chodzi."
* G: "Czyli na pintce? Wiesz, że KAŻDY wie, że tylko my tam polecimy. Kapitan Verlen, to oczywiste że to my i nasza robota. Nie zamaskujemy tego. Chyba, że użyjemy magii..."

Arianna używa swojej magii. Chce "ożywić" tą TAI melodią Ellariny chcąc by ta mogła złapać, zrozumieć i przekazać sygnały. A TAI i Ellarina były przywiązane do śpiewu.

Tr Z (sympatia Ellariny) M +3Ob +3:

* Vm: śpiew Ellariny faktycznie zarezonował z Zarraleą.
    * Zarralea uruchomiła holoprojekcję. Budowała wcześniej obraz na podstawie Ellariny tylko bez drakolickich zmian - ale teraz wygląda jak cyber-Ellarina, gdzie pomiędzy mechanizmem a ciałem coś _się rusza_. Ma puste oczy. TAI też wie że nie żyje XD.
    * Zarralea funkcjonuje. Ona wokalizuje, ale MY WIEMY co ona chce powiedzieć. Ellarina jest BIAŁA. 
    * Zarralea wydaje KOLEJNY kojący dźwięk do Ellariny.
    * Arianna stworzyła nekrotechniczną syrenę.

Załoga Flary jest w szoku. Lekarz mówi "no, tego jeszcze nie widziałem. Czego takiego nie widziałem. Po prostu nie." Ellarina ma łzy na policzkach, dla niej to jest... _niewłaściwe_, ale poprosiła Zarraleę, by ta zrobiła dla niej tą jedną ostatnią rzecz. Kajetan podszedł do Ellariny "nie martw się, opowiemy o niej dobrą pieśń" i poklepał ją po ramieniu.

Konflikt na MORALE. Załoga ma do czynienia z czymś STRASZNYM. To arystokraci Aurum. Niech Ellarina + Zarralea śpiewają, żeby pokazać Zarraleę jako uroczą.

Tr Z (Ellarina show) +3

* V: zdecydowana większość załogi jest OK z tym. Przejdzie im po tej sesji.
* V: creepy i strange, ale przejdzie im. Przywykli do _Arianny z rodu Verlen_. I tu pomagają też dziwactwa Eleny.
* V: Ci, co uważają TAI za żywe istoty dalej mają z tym problem, ale wierzą, że Arianna zrobiła to przez przypadek i nie chciała.

Arianna dostaje "+5 do perswazji". Ona nie tylko ma stalowy tyłek. Ona też ma stalową magię. SHE IS SCARY! I +5 miłości od Wanada.

* G: "Nie... nie spodziewałem się tego, po prawdzie." /szok "kapitan Verlen... Ty to kontrolujesz? I _to_ kontrolujesz?" /z odrazą na Zarraleę
* A: "Też spodziewałam się trochę innego efektu. Nie próbowałam jeszcze działać na TAI nigdy."
* G: "Musimy... dobrze, musimy wykorzystać _to_ żeby znaleźć o co chodzi. Z _tym_ jesteśmy w stanie zdominować i złamać strefę śmierci."

Flara i Athamarein poleciały w kierunku Strefy Śmierci, czyli TKO-4271, zostawiając niechronioną bazę.

Zbliżyliście się do Strefy Śmierci. Zarralea się ożywiła. Przesyła sygnały na Athamarein, które potem lecą na Flarę. Maja patrzy lekko zdziwiona "pani kapitan, wszystkie sygnały OK. Są sygnały straszące, ale... one są ok."

Jest potwierdzenie teorii, że coś odstrasza od tego terenu. Daria chce porównać to co dostaje Persefona i Zarralea. Szuka różnic, niezgodności... Maja się tym zajmuje, żeby to wykryć dla Darii.

Tr Z (nikt nie spodziewa się anomalnej TAI) +3:

* X: Persefona ma problemy ze spięciem się z Zarraleą. Po prostu. Nie umie czytać tego sygnału - nie "rozumie" kanału neikatiańskiej TAI.
* X: Maja nie ma do tego kompetencji. Przekroczyliśmy jej umiejętności.
* V: na szczęście po przekierowaniu zapytania na Athamarein udało się odkodować różnicę
    * różnica działa na kanałach zablokowanych przez Orbiter. Czyli nie każda nTAI to dostanie - tylko te "odblokowane"
    * to nie jest nic niszczycielskiego. To ostrzeżenie "uwaga, hazard memetyczny dla Twojej załogi, unikaj tego miejsca." I to sprawia, że TAI znajdują wymówkę.
* X: psychotronik TEŻ nie poradził sobie z jeszcze jednym sygnałem, ale on jest na częstotliwościach stosowanych przez TAI i BIA noktiańskie

Macie pewność, że Waszym jednostek ze strony psychotronicznej teraz nic nie grozi. Ale nie ma nic wskazującego na obecność anomalii memetycznej. Kurzmin kontaktuje się z Arianną:

* L: Kapitan Verlen, są w okolicznych planetoidach ślady wydobycia. Są tu jakieś jednostki typu grazer.
* A: Mamy zbadać co jest wydobywane?
* D: Trochę spektrometrów itp.

Daria puszcza spektrometry - standardowo: woda, metal, fissile. Jeśli jest to ostrzeżenie, to ktoś się upewnia że ma to dla siebie. Albo ktoś chce by nikt mu nie przeszkadzał. Detektor materiałów na samej planetoidzie TKO-4271. Co tam jest. To jest spora planetoida. Produkujemy sondy / boje i wysyłamy - chcemy znaleźć potencjalną anomalię. I wysyłamy ze wzmocnioną sygnaturą.

Tr Z (fabrykator) +3:

* Vr: udało się znaleźć dowód - **nie ma** oczywistych anomalii ani reakcji
* V: udało się bojom znaleźć zniszczony statek niewolniczy plus kilka innych. Ale ten "pierwszy" jest jakby... "na widoku". On aż krzyczy "EKSPLORUJ MNIE!"
    * udało się zlokalizować humanoidalną istotę która się szybko przemieszcza.

Arianna jest zdecydowana przechwycić tą żywą istotę. Wysyła sarderytę. Hubert Kerwelenios. On z RADOŚCIĄ wszedł w "torpedę abordażową" i został wystrzelony. Plus kilka torped by zablokować ucieczkę.

Tr Z (zaskoczenie + sarderyta) +3:

* X: podczas insercji zniszczona jest droga powrotu.
* V: Hubert PRZECHWYCIŁ przeciwnika.
    * próbuje się schować, zmniejszyć swój profil. I NAGLE HUBERT. Pierwsza reakcja to ucieczka, ale Hubert ma Lancera. Przeciwnik ma tylko skafander, co ciekawe: nie strzela.
* Vz: przeciwnik próbował, naprawdę, zna teren i jest zwinny. Ale Hubert przechwycił. Przeciwnik przestał się miotać.
* Vr: Elena błyskawicznie szybko przesunęła Flarę i Klarysa wystrzeliła nanowłókno. Hubert złapał i poleciał.
    * detektory wykryły rozbłysk energii w dwóch miejscach, ale on szybko zgasł

Hubert ma jeńca. Daria - TRIPLE PARANOIA, co to za rozbłysk, czy nie ma infekcji. Elena jest gotowa do błyskawicznego przesunięcia Flary i to robi asap by oddalić się od planetoidy.

Athamarein -> Arianny

* G: Kapitan Verlen, macie JEŃCA!
* A: Tak, mamy
* G: To będzie trudne, zostaw go w kosmosie i zsyntetyzuj pojemnik. Może jest wybuchowy.
* A: Najpewniej wybuchłby naszemu człowiekowi w twarz
* G: Proszę wykonać swoje polecenie.
* A: Tak jest, komodorze...

ARNULF SYNTETYZUJE. 90 minut później... gdy Arianna już WIE że jeśli tam ktoś jest to jest ostrzeżony... NIE!

Arianna się nie zgadza. Transferowe, nie mały lifesupport. Ma być szybko i wystarczająco solidnie. 15 minut.

Tr Z (blueprint na szybko od Darii) +4:

* V: jest kontener. Jest niestabilny (nie podziała długo), ale wystarczy na kilka godzin

Jeniec i Hubert trafili do kontenera. Wysłanie medyka, Dolnicza i całej świty to małego kontenerka. Daria podpina się na obwodzie inżynierskim i patrzy co się dzieje.

* Dojnicz: "Jesteśmy siłami Orbitera. Poddaj się i zdejmij hełm. Jest tu atmosfera."
* Jeniec się nie rusza.

Arianna wycofuje ludzi. Poszedł robot. 5 minut później. Jeniec przestraszył się robota. SERIO. Próbuje się wpłaszczyć w ścianę. A po chwili jeniec przeszedł w pozycję do walki. Nie jest może najwprawniejsza, ale jednak. Jeniec ma łatany skafander noktiański. Kobiecy łatany skafander noktiański.

* V: Jeniec RZUCIŁA na piłę tarczową. Ale robot zwinnie ją złapał i unieruchomił. Za ręce i nogi. Pilnuje by sobie nie mogła zrobić krzywdy. Ona ZNOWU wyłączyła mięśnie.

Robot ją wyjął ze skafandra, przebadał, upewnił się, że "nic jej nie jest", że nie jest żywą bombą. Faktycznie, NIE JEST. Nie jest chora, nie ma... niczego. Jest blada, wychudzona, delikatnej konstrukcji, "wystarczająco zadbana". Wygląda na przestraszoną, ale zrezygnowaną. Nie powiedziała ani jednego słowa.

G -> A:

* G: Musimy ją przesłuchać. Mam koktajl chemiczny.
* A: Koktajl chemiczny? Nie lepiej wysłać do niej Ellarinę? Że jesteśmy z Nonariona? 
* G: Marine już powiedział... /rezygnacja To noktianka. Noktianie są niebezpieczni. Jeśli noktianie robią coś z lokalnymi TAI, musimy to wiedzieć szybko.
* A: Tylko je ostrzegają
* G: Nie, to jest to, co _TO_ zgłosiło. Nie wiemy jaka jest prawda.
* A: Ufam mojej magii, może nie pracowałam z moim typem, ale wiem jak funkcjonują typy...
* G: /uśmiech - Przepraszam Cię, masz rację. Niesłusznie obwiniałem... _to_. Zróbmy inaczej - pokażmy noktiance to co stworzyłaś. To jest to co ją czeka jak nie będzie współpracować. I i tak będzie współpracować. A tak ma decyzję co nam powie. Jeśli jest rozsądna, będzie współpracować jak zobaczy co zrobiłaś.
* A: Ona jest w podobnym wieku jak ja, nie pamięta wojny. Nie wszystkich nas dotknęła wojna.
* G: Sugerujesz, że nie miała jak zabić kogokolwiek. Bo jest za młoda. Ty nie zabiłaś ani jednego noktianina. Ja też nie.

Arianna bardzo nie chce, by on torturował niewinną noktiankę.

Tr Z (fundamentalnie on NIE CHCE tego robić) +3:

* X: Gabriel zobaczył TAJNĄ BAZĘ NOKTIAŃSKĄ. I on ma siły by to rozwiązać. Bo oni się nie spodziewają. MUSI wiedzieć czy to jest prawda.
* V: Gabriel... nie lubi robić krzywdy. Jeśli są lepsze metody... ale Arianna na razie ma fatalny success rate w przesłuchaniach... czyli chce przesłuchać ją na Athamarein.
* Arianna ma ludzi z Aurum z niewolnikami noktiańskimi. Lepiej mogą podejść: X: Gabriel chce być w pętli.
* X: Gabriel uważa, że noktianie muszą odpowiedzieć przed Orbiterem lub Astorią. Jej los jest przesądzony - ona jest w rękach Orbitera jako jeniec wojenny.
* Xz: Gabriel przesłucha ją na Athamarein. Niestety.

Athamarein. Przestraszona noktianka. W ciasnych, jasnych korytarzach. Więcej ludzi niż zwykle. Noktianka próbuje nie panikować i jej na razie wychodzi. O dziwo, nie wydała żadnego dźwięku, jeszcze - poza piśnięciem, ale nie mówi.

Gabriel próbuje być łagodny. Jest jak "dobry wujek Stalin", nie wychodzi mu. A noktianka jest w podniesionej grawitacji.

* G: "Powiedz, nie zabiłaś nigdy człowieka, prawda?"
* noktianka: O_O
* G: "Nic Ci nie grozi z naszej strony, jeśli powiesz nam prawdę"
* noktianka: ..?
* G: "Umiesz mówić? Rozumiesz mnie?"
* noktianka kiwnęła głową na tak, ale tak lekko
* G: "pani kapitan, proszę kontynuować."
* noktianka ma taką samą minę jak do tej pory
* A: (charakterystyczne, że noktianka nie ma nic charakterystycznego)
* A: dostaliśmy informacje o anomalii memetycznej i musieliśmy się upewnić.
* noktianka ma minę bez wyrazu
* G -> A (na kom): chemikalia czy groźba? /rezygnacja
* A -> G: Ellarina będzie dobra
* G -> A: dobrze. Ellarina it is.
* G: masz dwie możliwości. Jeśli powiesz jak masz na imię, możesz wybrać czy pójdziesz z nią czy zostaniesz ze mną. Na tamtym statku masz więcej miejsca. I mniej strasznych ludzi.
* noktianka myśli
* G: i potencjał na niższą grawitację.
* noktianka myśli chwilkę "Kirea Rialirat"
* G: tutaj czy niższa grawitacja?
* Kirea: prosiłabym o niższą grawitację
* G: dobrze. Pani kapitan, jeniec należy do pani.
* Kirea: (BLANK FACE na to "jeniec")
* Arianna: (dyskretny facepalm)

Arianna leci w wahadłowcu na Flarę. Kirea rozpoznaje przestrzeń, patrzy tęsknie za planetoidą ale nie walczy itp.

* Arianna, mruczy by Kirea słyszała "nawet ja nie miałam okazji walczyć w tej wojnie, a co dopiero ta dziewczyna"
* Kirea: brak reakcji, ale wyraźnie myśli

Flara. Na wejściu Dojnicz (bo musi być ktoś). Arianna wycofała Psa itp. NIKT NIE MA TU BYĆ. "Nie patrzymy na prawdziwą noktiankę!!!", niech Ellarina śpiewa w mesie. Arianna chce, by Kirea usłyszała pieśń Ellariny nie w języku używanym przez Orbiter. Kirea ZNOWU myśli, ale chyba nie jest już tak załamana. Arianna prowadzi Kireę do salki treningowej i ustawiła 0.5G, by Kirea czuła się komfortowo. Kirea się lekko uśmiechnęła i powiedziała "dziękuję".

Kirea usiadła w kącie i po prostu patrzy w przestrzeń. Widać, że jest skupiona; jakaś forma medytacji. Na planetoidzie ona leżała i patrzyła w gwiazdy. Po chwili się wyrwała i z zaciekawieniem się rozgląda. Bo nie widziała nic takiego. Arianna -> Aurumowców (by od niewolników noktiańskich co i jak) -> Maja.

Maja naprawdę się stara. Ale teraz przechodzi siebie. Bo Maja "poczciwa noktianka, niegroźna, głodna, chudziutka, i tak będą ją torturować niech jej smakuje".

Tr Z (niewolnicy) +3:

* X: Kirea nie jest przyzwyczajona do noktiańskiego jedzenia. Widocznie nie mają sprawnych noktiańskich life support.
* V: Kirei może nie będzie smakować (bo jest za ostre / za mocne MIMO że dla Mai jest za łagodne) ale nic jej nie będzie i doceni.

Kirea została nakarmiona. Arianna zauważyła minę "auć za ostre", ale Kirea nic nie powiedziała tylko zjadła. I popiła wodą. Zdziwił ją smak wody. Jest czysta. Czyli nie mają bardzo dobrych warunków. Noktianka usiadła i zwróciła się ku kątowi - jest przestymulowana.

* G -> A: "Czy myślisz, że zauważą jej brak?"
* A -> G: "Myślę, że zauważyli jak zniknęła."
* G -> A: "Poinformuję ich, że ona żyje i nie stała jej się krzywda. Że nie mamy w zwyczaju krzywdzić niewinnych."

Gabriel wysłał informację na planetoidę, że mają Keirę, żyje, nie jest ranna, jest nakarmiona, jest trzymana w 0.5G, nie stała jej się krzywda bo Orbiter nie ma w zwyczaju krzywdzić niewinnych, więc niech się o nią nie martwią. Planetoida, jak to planetoida, nie odpowiedziała.

"Arianna - właścicielka DZIKIEJ NOKTIANKI! Takiej nieoswojonej."

TYMCZASEM KEIRA. Arianna przygasza światło i mówi ciszej.

* A -> K: "Czy jest jakieś realne zagrożenie w tym sektorze"
* K -> A: (oszczędne odwrócenie się w stronę A., mokre policzki) "Wy"
* A -> K: "to miejsce nazywa się strefą śmierci zanim tu przylecieliśmy. Czemu?"
* K -> A: "nie wiem" (po chwili) "odstraszamy"
* A -> K: "co się stało ze statkami które nie wróciły?"
* K -> A: "nie powiem"
* A -> K: "jeśli nie wiemy że potrzebują ratunku, musimy interweniować"
* K -> A: (puzzled) "ratunku?"
* A -> K: "statki giną w strefie śmierci więc potrzebują ratunku. Taki jest wniosek z naszej perspektywy."
* K -> A: (cicho) "szkoda" /łzy /smutek

Arianna idzie dalej w tą linię rozumowania. Są tu bo tu są plotki że strefa zabija ludzi. Są tu bo statki zaginęły. Orbiter chce ich ratować.

Tr +3:

* (ona nie wie o istnieniu magii i w nią nie wierzy)
* Vr: ona jest przekonana, że to oznacza, że ktoś zginie. Albo wszyscy na tej planetoidzie albo Wy wszyscy. I ona widzi śmierć i zagładę. Nie jest przekonana, że planetoida jest w stanie ich pokonać.
    * nie ma innego rozwiązania. To jest koniec.
    * Wasze przybycie zniszczyło wszystko. Jej świat właśnie umarł. Orbiter ich znalazł.
* V: wierzy w swoje dowództwo. Wszystko rozwiązali, to też jakoś rozwiążą. _Ona już jest martwa_ w jej oczach, ale oni jeszcze mają szansę.
    * to że ona tu jest nie znaczy że oni jej też nie zabiją
    * są tam nie tylko savaranie
    * oni wiedzą, że wojna się skończyła i oni mają świadomość polityczną na tym terenie; nie wiedzieli że Orbiter się pojawił. Myśleli, że mają jeszcze czas.
* Vr: Kirea wie, że ta baza próbuje ratować noktian. Ich celem nie jest walka z Orbiterem. 
    * Kirea nie wie jak duży Orbiter jest, ona myśli że Orbiter ma "50 statków".
    * Kirea jest advancerem. Ona jest w tym dobra - ma minimalny ślad.
    * wyszła na powierzchnię, bo za dużo ludzi. Chciała popatrzeć na gwiazdy. Ona lubi samotność.
    * nic jej nie grozi -> nie ma "niekontrolowanych" problemów.
* V: Kirea wie, że jest tam co najmniej jedna osoba z Orbitera. Wolna. Chciał być noktianinem. To groźny wojownik.     (tego Arianna NIE powie komodorowi)
    * oni skutecznie przechwycili / zniszczyli statki niewolnicze.
    * były próby ataku na "duchy". Nikt nie przeżył.
    * nie mają sprawnego dużego statku kosmicznego, ale Domina Lucis była statkiem wsparcia
    * Kirea nie wie o unifikacji sprzętu - oni mają low profile, to duchy. Nie handlują z nikim.
    * dowodzi niejaki Sarian Xadaar

## Streszczenie

Planetoida Kazmirian została przejęta przez Orbiter. Daria ostrzegła Nonarion (Leo), że Orbiter nadal poluje na TAI i zdobyła informację o 'strefie duchów'. Z Mają doszły do tego, że 'strefa duchów' jest świetnym maskowaniem; Lodowiec wyprowadził tam Orbiter a Arianna stworzyła nekroTAI z Zarralei d'Isigtand. Używając advancera przechwycili samotną savarankę (której Lodowiec nie umie przesłuchać) i stanęło na tym, że 'strefa duchów' jest bazą noktiańską zbudowaną dookoła jednostki wsparcia, 'Domina Lucis'. Lodowiec chce doprowadzić noktian do sprawiedliwości Orbitera, Arianna i Daria są skonfliktowane. Mają jeńca - Kireę, lokalną młodą savarankę.

## Progresja

* Gabriel Lodowiec: podejrzewa, że Daria Czarnewik sabotuje plan Arianny z reanimacją TAI bo jest sympatyczką TAI. Zero zaufania do Darii.
* Arianna Verlen: opinia ABSOLUTNIE bezwzględnej i strasznej wśród załogi Athamarein i Astralnej Flary po mrocznym wskrzeszeniu TAI Zarralea. 'Live or die, you shall serve'.
* Ellarina Samarintael: bardzo nieszczęśliwa; lubiła Zarraleę, która została zmieniona w nekroTAI. W szoku - Zarralea została 'dark awakened' i przesunęła lojalność w Ariannę, acz rozpoznaje swoją dawną koleżankę (Ellarinę). Ellarina bardzo dobrze ukrywa swoje emocje; it is what it is, ale reewaluowała wszystko co wie o Orbiterze. Obwinia siebie za stworzenie nekroTAI i mroczne wskrzeszenie Zarralei. Nie chce po śmierci służyć Ariannie. Będzie grzeczna i zrobi co Arianna każe.
* Daria Czarnewik: Arianna wie, że Daria się starała w odbudowie Zarralei. Ale Lodowiec ma podejrzenia, że Daria go sabotowała.
* OO Astralna Flara: straciła trochę materiałów na próbę odbudowy Zarralei i pracy nad Isigtand a potem 'klatka' na Kireę dla Lodowca; ma jakieś 85% pełnego capacity.

### Frakcji

* Orbiter: w okolicach Obłoka Lirańskiego dostaje opinię bardzo skutecznego i bezwzględnego - wskrzeszenie TAI Zarralea, Ograniczanie innych TAI, walka z duchami...

## Zasługi

* Arianna Verlen: pozyskała informacje o tym, że coś może wpływać na TAI i może Strefa Duchów to noktianie. Stworzyła nekroTAI Zarraleę wywołując grozę w części załogi. Kazała przechwycić jeńca (Kireę) i zatrzymała Lodowca przed okrutnym traktowaniem savaranki. Przesłuchała ją wyjątkowo delikatnie i poznała prawdę o Strefie Duchów. Skuteczna w przekonywaniu Lodowca do zachowywania się "dobrze" a nie "maksymalnie skutecznie".
* Daria Czarnewik: ostrzegła Leo przed tym, że Orbiter COŚ robi z tymi neikatiańskimi TAI. Pozyskała dane o Strefie Duchów. Próbowała reanimować TAI Zarraleę z Isigtand; niestety, nie udało jej się - zbyt zniszczona (bardzo wbrew sobie, nie pozwoli na jej Ograniczenie). Przerażona powstaniem nekroTAI.
* Leo Kasztop: przekazał Darii informacje o Strefie Duchów i planetoidzie TKO-4271; po tym jak Daria ostrzegła go o działaniach Orbitera wobec neikatiańskich TAI przekazał info gdzie trzeba.
* Szymon Wanad: osłaniał Darię, gdy ta na Nonarionie negocjowała z Leo. Pobił jednego człowieka i ma wysokie morale.
* Maja Samszar: dekodowała dane dostarczone przez Darię; doszła do tego, że wszystko wskazuje na sojusz noktian i TAI? Niestety, za słaba - nie ma na to dowodów. Ma gwiazdki w oczach - potencjalnie walczą z NOKTIANAMI WROGAMI LUDZKOŚCI! Zachowała dla Arianny dyskrecję o danych. Zrobiła potrawę dla noktianki Kirei; nieco za mocną, ale jej doświadczenia z niewolnikami noktiańskimi się sprawdza.
* Gabriel Lodowiec: podszedł do rozbierania Strefy Duchów metodycznie i ostrożnie. Single-minded w celu doprowadzenia noktian do Pax Orbiter. Zupełnie nie nadaje się do przesłuchiwania niegroźnych savarańskich noktianek. Skonfliktowany między "noktianie muszą być zniszczeni" a "nie bądź okrutny, Pax Orbiter". Nie jest przeciwny używania koktajlu chemicznego do przesłuchania Kirei, ale Arianna go przekonała.
* Ellarina Samarintael: na polecenie Arianny śpiewała 'martwej' Zarralei co posłużyło do jej 'mrocznego wskrzeszenia' przez Ariannę. W szoku, obwinia się, bo przyczyniła się do tej reanimacji. Potem wraz z nekroZarraleą zrobiła koncert, co ją mocniej straumatyzowało, ale podniosło morale załogi.
* Hubert Kerwelenios: insercja na TKO-4271, przechwycenie Kirei i ewakuacja nanowłóknem. Doskonała operacja advancerska.
* Elena Verlen: płynnie przesunęła Flarę by Klara ewakuowała Huberta nanowłóknem. Świetnie pilotuje.
* Klarysa Jirnik: precyzyjny strzał nanowłóknem, skuteczna ewakuacja Huberta i Kirei.
* Kirea Rialirat: advancer Dominy Lucis (savaranka) i dziewczyna w wieku Eleny; przechwycona przez Huberta z planetoidy TKO-4271. Bardzo cicha savaranka przyzwyczajona do 0.5g. Nieświadomie, powiedziała bardzo dużo Ariannie o Dominie Lucis i ekosystemie Strefy Duchów. Przerażona, mimo, że traktowana dobrze (do poziomu próby samobójstwa). Uważa Orbiter za tych, co sprowadzą koniec jej świata. Albo Orbiter zginie albo baza noktiańska zginie.
* Arnulf Perikas: syntetyzuje klatki kosmiczne i urządzenia do przesłuchiwania "na szybko".
* Tomasz Dojnicz: jego łagodność wyjątkowo się przydała do zarządzania przerażoną savaranką (jeńcem).
* Kajetan Kircznik: upewnił się, że Kirea nie jest niebezpieczna, że nie jest chora i nie zagrozi Flarze.
* NekroTAI Zarralea: kiedyś TAI d'Isigtand, niezbyt bystra, ale ładnie Ellarinie śpiewała. Teraz - nekroTAI, ożywiona przez Ariannę i zmieniona w detektor fal działających na neikatiańskie TAI. Mądrzejsza, komunikuje się muzyką i w ten sposób przesyła emocje, anomalna istota próbująca ukoić Ellarinę i wykonać misję dla Arianny - ku przerażeniu Ellariny.
* OO Athamarein: udało się użyć Persi i Zarralei do zrozumienia co się dzieje w Strefie Duchów.
* OO Astralna Flara: jako jednostka wsparcia skutecznie fabrykowała drony do badania Strefy Duchów a potem klatki na Kireę (savarankę) 

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Libracja Lirańska
                1. Anomalia Kolapsu, orbita
                    1. Strefa Upiorów Orbitera
                        1. Planetoida Kazmirian: 
                        1. Planetoida Lodowca: jeszcze znana jako TKO-4271; 15 minut od planetoidy Kazmirian, miejsce, gdzie znajdują się "duchy". W tej planetoidzie znajduje się noktiański statek wsparcia, Domina Lucis.

## Czas

* Opóźnienie: 20
* Dni: 3

## Konflikty

* 1 - Leo ma pewne informacje, ale Daria musi je filtrować. Leo ma sporo, jest brokerem - NIE ZBLIŻAJCIE SIĘ TAM. To niebezpieczne. Leo opowiedział mniej więcej co wie:
    * Tr Z (Leo Darię lubi) +3
    * VV: Leo odsprzedał Darii dane o Strefie Śmierci i powiedział co zebrał
* 2 - Arianna chce, by Maja przeanalizowała nagrania i wyłowiła anomalne sygnały. Coś właściwego dla tej anomalii.
    * Ex (dobrze złożone, Maja jest 'nowa') Z (potęga psychotroniki Persefony + Maja szuka bardzo dokładnie) +3
    * XXXVz: Maja nie ma sygnatury, tylko Arianna jej wierzy, brak dowodów - ale są wnioski. Statki niewolnicze ucierpiały i to wygląda jak sojusz TAI i noktian? Jak chodzi o "duchy".
* 3 - Daria nie tylko naprawia TAI, ale do tego wszystkiego chce zamontować selfdestruct switch, by wyglądało, jakby TAI pod wpływem sygnałów po prostu przestała działać. Martwa TAI nie będzie Ograniczona.
    * Tr Z (Daria współpracuje z Zarraleą) +3
    * XXX: Duża strata zasobów i porażka. Zarralea zbyt zniszczona.
* 4 - Plan B Arianny - Daria robi odbiornik z martwej Zarralei oraz z resztek Isigtand. Przynajmniej coś się uda zrobić.
    * Tr Z (dobrej klasy części + destruktywność) +3
    * XXX: udało się zrobić odbiornik i on ma sygnaturę że zrobiona z martwej TAI, ale niestety duży koszt i niestabilność części
* 5 - Arianna używa swojej magii. Chce "ożywić" tą TAI melodią Ellariny chcąc by ta mogła złapać, zrozumieć i przekazać sygnały. A TAI i Ellarina były przywiązane do śpiewu.
    * Tr Z (sympatia Ellariny) M +3Ob +3
    * Vm: stworzenie detektora z Zarralei i reanimacja jej w nekroTAI
* 6 - Konflikt na MORALE. Załoga ma do czynienia z czymś STRASZNYM. To arystokraci Aurum. Niech Ellarina + Zarralea śpiewają, żeby pokazać Zarraleę jako uroczą.
    * Tr Z (Ellarina show) +3
    * VVV: Arianna się wybroniła że to przypadek, ludzie creeped out
* 7 - Jest potwierdzenie teorii, że coś odstrasza od tego terenu. Maja szuka różnic, niezgodności...
    * Tr Z (nikt nie spodziewa się anomalnej TAI) +3
    * XXVX: Persi nie spina się z Zarraleą i Maja nie ma skilli, ale na Athamarein skille są. Mamy sygnał noktiańskich częstotliwości odstraszających.
* 8 - Produkujemy sondy / boje i wysyłamy - chcemy znaleźć potencjalną anomalię. I wysyłamy ze wzmocnioną sygnaturą.
    * Tr Z (fabrykator) +3
    * VV: nie ma oczywistych anomalii, są ślady że ktoś tu jest i coś robi; potencjalna pułapka PLUS wykrycie uciekającej Kirei Rialirat
* 9 - Arianna jest zdecydowana przechwycić tą żywą istotę. Wysyła sarderytę. Hubert Kerwelenios. On z RADOŚCIĄ wszedł w "torpedę abordażową" i został wystrzelony.
    * Tr Z (zaskoczenie + sarderyta) +3
    * XVVzVr: przy insercji zniszczona droga powrotu, Hubert przechwycił Kireę, Hubert unieszkodliwił Kireę i Elena przesunęła Flarę by Klarysa mogła wystrzelić nanowłókno; udany evac.
* 10 - Arianna się nie zgadza. Transferowe, nie mały lifesupport. Ma być szybko i wystarczająco solidnie. 15 minut.
    * Tr Z (blueprint na szybko od Darii) +4
    * V: jest kontener. Jest niestabilny (nie podziała długo), ale wystarczy na kilka godzin
* 11 - Jeniec ma łatany skafander noktiański. Kobiecy łatany skafander noktiański. Daria zapewnia, by Kirei (jeńcowi) nic się nie stało mimo robota.
    * Tr (wysoka kontrola acz zaskoczenie) +2
    * V: Jeniec RZUCIŁA na piłę tarczową. Ale robot zwinnie ją złapał i unieruchomił. Za ręce i nogi. Pilnuje by sobie nie mogła zrobić krzywdy. 
* 12 - Arianna bardzo nie chce, by on torturował niewinną noktiankę.
    * Tr Z (fundamentalnie on NIE CHCE tego robić) +3
    * XVXXz: Gabriel nie chce jej robić krzywdy, ale musi poznać prawdę o BAZIE NOCTIS! Przesłucha ją i jej los jest przesądzony.
* 13 - Maja naprawdę się stara dokarmić Kireę. Bo "poczciwa noktianka, niegroźna, głodna, chudziutka, i tak będą ją torturować niech jej smakuje".
    * Tr Z (niewolnicy) +3
    * XV: Kirea nie jest przyzwyczajona do noktiańskiego jedzenia, jest za mocne, ale nic jej nie będzie i doceni gest
* 14 - Arianna idzie dalej w tą linię rozumowania. Są tu bo tu są plotki że strefa zabija ludzi. Są tu bo statki zaginęły. Orbiter chce ich ratować. Przesłuchuje Kireę.
    * Tr +3
    * VrVVrV: dowiedziała się dużo o tym co wie Kirea, dużo o Dominie Lucis, dużo o samej bazie.
