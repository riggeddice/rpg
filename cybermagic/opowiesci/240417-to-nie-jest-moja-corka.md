## Metadane

* title: "To nie jest moja córka"
* threads: problemy-con-szernief
* motives: the-addicted, the-truthseeker, the-protector, the-aspirant, the-hunter, casual-cruelty, dyskretny-stalker, dziwna-zmiana-zachowania, energia-interis, energia-ixion, konspiracja-pozorna, lokalne-mity-i-legendy, nieoczekiwana-interakcja-magiczna, pozornie-w-kajdanach, rekonstrukcja-zycia, stopniowe-tracenie-siebie, trudne-decyzje-zyciowe, arystokratka-niesforna
* gm: żółw
* players: johny, szymon, lawroj

## Kontynuacja
### Kampanijna

* [240320 - Seryjne samobójstwa na Szernief](240320-seryjne-samobojstwa-na-szernief)

### Chronologiczna

* [240320 - Seryjne samobójstwa na Szernief](240320-seryjne-samobojstwa-na-szernief)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * Inspiracje
        * ["The thing in the basement is getting better at mimicking people"](https://www.youtube.com/watch?v=3lX59lKV0oA)
            * Mimik zastąpił nastolatkę.
                * -> Wpasowuje się w topos "mała dziewczynka w horrorze jest straszna"
                * -> Gracze będą próbowali uratować dziewczynkę
            * Macocha dziewczynki nie poznała dziewczynki i trafiła do psychiatryka.
                * -> "No jak to Macocha NIE POZNAŁA? Musi coś być nie tak."
                * -> WIĘC: Macocha miała w przeszłości epizod psychotyczny; nie WIEMY czy dziewczynka jest mimikiem czy nie. 
                * -> ...ale wszystko wskazuje na to, że tak.
            * Na to nałóżmy jeszcze _Slay the Princess_  
                * mamy Mimika, który niekoniecznie chce niszczyć i zabijać. Ot, taka jego natura. 
                * Zrost energii _Ixion_ (adaptacja przez poświęcenie) i _Interis_ (koniec wszystkiego)
    * Sonata Arctica "Full Moon"
        * "Someone's at the door | Understanding, too demanding | Can this be wrong? | It's love that is not ending"
        * "She should not lock the open door | (Run away! Run away! Run away!) | Full moon is on the sky and he's not a man anymore"
        * "Knock on the door and scream that is soon ending | Mess on the floor again"
        * Ojciec: "Raleno, będziesz bezpieczna"
        * Mimik: "Nie mogę z Wami zostać, nie mogę tu być! Nie mogę i nie chcę! Nie Wy, nie z Wami!"
* Opowieść o (Theme and vision):
    * "Czy jesteśmy tym czym się urodziliśmy, czy możemy być czymś innym?"
        * Mimik jest bronią i "testem ixiońskim" dla Kultu - ale pragnie być osobą
    * "Gdzie możni walczą, tam ludzie cierpią"
        * Kultysta vs Mawir jako walka o duszę Kultu
        * Serven (Ojciec) źle traktuje ludzi, Ralena robi co chce - a teraz walczą o resztkę tego co mają
        * Alissa (Macocha) - trophy wife, ale ona NAPRAWDĘ polubiła Ralenę. 
    * Lokalizacja: Stacja CON Szernief
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * the-addicted: Ralena / Doppelganger; istota ixion-interis; pożarła Ralenę i zastąpiła jej miejsce. Skonfliktowana; nie chce być potworem i robić krzywdy bliskim, ale MUSI się żywić ludźmi i ich rozpaczą.
    * the-truthseeker: Alissa, Macocha Raleny; świadoma tego, że Ralena nie jest jej córką (bo pożarł ją Doppelganger), próbuje dowiedzieć się co się stało z jej córką, czym jest "Ralena" i ujawnić całość prawdy.
    * the-protector: Serven, ojciec Raleny; jego córka i żona już dość wycierpiały na tej przeklętej stacji, trzeba udzielić im maksymalnej pomocy jak to możliwe i nie pozwolić by ktoś je skrzywdził.
    * the-aspirant: Kultysta Saitaera spoza Stacji, który chce przejąć kontrolę nad Kultem na Szernief i do tego celu wprowadził Doppelgangera na Stację. Ale wszystko poszło nie tak.
    * the-hunter: Mawir Hong, uniemożliwiający Kultyście spoza Stacji przejęcie kontroli nad Szernief. Chce znaleźć i pożreć lub pokonać swoich rywali. Jest _apex predator_.
    * casual-cruelty: Serven jest osobą dość twardą z natury, nadużywającą władzy i traktującą ludzi źle. Ale kocha swoją rodzinę. Servenowi nie przeszkadza, że ktoś jest głodny - byle jemu było wygodnie.
    * dyskretny-stalker: Sposób działania Doppelgangera; izoluje swoją ofiarę, szepcze do niej, bawi się głosami, próbuje ją wykończyć fizycznie i psychicznie by fundamentalnie ją Pożreć.
    * dziwna-zmiana-zachowania: Gdy Doppelganger przejął ciało Raleny, zachowywał się identycznie do niej. Paradoksalnie, im bardziej jest skonfliktowany tym bardziej NIE JEST Raleną a czymś nowym.
    * energia-interis: im bardziej zdesperowana i załamana jest ofiara, tym lepiej Doppelganger wchodzi do jej głowy i tym lepiej nią manipuluje. Wtedy też się żywi. Czyli - aplikacja rozpaczy i braku nadziei.
    * energia-ixion: Doppelganger jest w stanie perfekcyjnie się przekształcić zarówno fizycznie jak i psychicznie do swoich ofiar, ma też formę bojową.
    * konspiracja-pozorna: wszystko wskazuje na perfekcyjny plan wielu zaawansowanych sił, ale tak naprawdę Doppelganger nie wie co robi i czego chce i to tylko ruchy emergentnych agentów. Ralena jest sama. Tak samo Kultysta.
    * lokalne-mity-i-legendy: Drakolici Mawira Honga mają legendy o Doppelgangerze - perfekcyjnym łowcy-zabójcy który poluje na "ludzi słabych" (ulegających rozpaczy). Oczywiście, nasz Doppelganger jest inny.
    * nieoczekiwana-interakcja-magiczna: Doppelganger miał pomóc komuś przejąć kontrolę nad Mawirem i Mawirowcami, ale z uwagi na specyfikę tej stacji Doppelganger... chce walczyć ze swoją naturą.
    * pozornie-w-kajdanach: wydaje się, że Doppelganger/Ralena jest pod ciągłym monitoringiem ojca i kontrolą. W praktyce, ma swobodę ruchu na Stacji - potrafi poruszać się tak by Ojciec nie wiedział.
    * rekonstrukcja-zycia: jeden z wariantów tego czego pragnie Doppelganger/Ralena. Co prawda nie jest "Raleną", ale chce życia w swojej rodzinie, razem.
    * stopniowe-tracenie-siebie: zarówno Doppelganger staje się "mniej" sobą jak i echo Raleny staje się "mniej" sobą. Powstaje nowa istota, która nie wie czym jest i czego pragnie.
    * trudne-decyzje-zyciowe: jak rozwiązać trójkąt Ojciec - Macocha - Doppelganger? Dla nich wszystkich to trudny problem.
    * arystokratka-niesforna: Ralena była osobą, która robiła co chciała i gdzie chciała. Nie dało się jej upilnować ani zatrzymać. Dlatego była IDEALNĄ ofiarą dla doppelgangera.
* O co grają Gracze?
    * Sukces:
        * Zniszczyć, uratować lub wygnać Mimika.
        * Pomóc Mawirowi w utrzymaniu Kultu lub osłabić jego pozycję.
    * Porażka: 
        * Kultysta używa Mimika by przejąć kontrolę nad Kultem
        * Mimik niezależnie podejmuje decyzję co robić ze swoim życiem
        * Mawir pożera Mimika żywcem i się jeszcze bardziej wzmacnia ixiońsko.
* O co gra MG?
    * Highlevel
        * Setting jest "STORY-FIRST". Niski stopień trudności, dużo ludzi, kiepskie warunki.
        * PRIMARY: chcę, by określili los Mimika / Raleny
        * SECONDARY: chcę, by sami określili po czyjej stronie chcą stanąć - Kultysta, Mawir?
        * Chcę, by określili jak zniszczyć lub uratować różne siły
    * Co uzyskać fabularnie
        * Mawir lub Kultysta przejmie kontrolę nad Kultem. Mawir spożyje Mimika.
        * Kultysta odprawi rytuał, by przejąć kontrolę nad Mimikiem
        * Mimik zabije Ojca (nie chce, ale nie ma możliwości) i serię ludzi
        * Ojciec doprowadzi do tego, że Macocha zostanie w izolatce
* Agendy
    * Mawir (the-hunter)
        * widzi, że coś jest nie tak i szuka źródła. Nie wie kto mu stoi na drodze.
        * poluje na Mimika, by go pokonać i zjeść (serio). Nie wie co tu jest, ale wie że coś.
    * Kultysta (the-aspirant)
        * czeka na to aż Mimik dotrze do Mawira i nie rozumie, czemu Mimik nic nie robi.
    * Mimik (the-addicted)
        * niby kojarzy jakie ma zadanie, ale się _zmienił_.
        * najpierw jest pasywny i wysysa Ojca, ale po pewnym czasie chce polować na Mawirowców
            * bo misja i bo nie chce krzywdzić swoich
    * Ojciec, Serven (the-protector) się skupia na osłanianiu Mimika oraz Matki, ale jest coraz słabszy i paranoiczny (wpływ Mimika)
    * Macocha, Alissa (the-truthseeker) walczy o prawdę; docelowo może skierować Mawira w stronę Mimika.
* Default Future
    * Ralena zabija kilka osób na Stacji. Kultysta dociera do Raleny i przejmuje nad nią kontrolę. Mawir zabija Ralenę.
* Dilemma: Czy "Ralena" może mieć normalne życie jako to czym jest?
* Crisis source: 
    * HEART: "Ten Kult Saitaera wymaga lepszego przywódcy"
    * VISIBLE: "Ludzie znikają, pożarci przez Mimika"
* Emocje, w które celujemy
    * Część 1: "Czy na Stacji Jest Mimik?"
        * Oki - jest potwór, jest dobrze zakamuflowany, możemy to rozwiązać... czy jest?
        * Czy Macocha ma rację? Czy Mimik to Mimik, czy zwykła dziewczyna? A może ona nic nie wie?
        * Co do tego ma cholerny kult Saitaera działający w okolicy?
        * ...ale ***jowa rodzina...
    * Część 2: "Ekspansja"
        * TAK! Mawir może pomóc to znaleźć i zniszczyć!
        * Wait... ona nie chce walczyć? Nie chce być potworem?
        * NIE! Mawir może to rozwalić! XD
    * Część 3: "Rozwiązanie"
        * Ona... co z tym wszystkim robimy?!

### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

KONTEKST:

* Zgodny z CHRONOLOGIĄ

CHRONOLOGIA:

* 2 miesiące temu na Stacji pojawił się Kultysta, który przybył jako bogaty przedsiębiorca
    * Kultysta jest szczerym wyznawcą Saitaera, uważa lokalny kult za nie dość godny Jego mocy.
* 45 dni temu temu na Stacji pojawił się Serven, Alissa i ich rozpieszczona córka, Ralena
    * Oni są bogaci i ustawieni politycznie, chcieli zobaczyć stan kopalni na Serbiniusie
    * Ralena była nie do życia, chciała być WSZĘDZIE i WSZYSTKO zwiedzać
* Mniej więcej wtedy Kultysta stworzył Mimika, by przejąć kontrolę nad Kultem Saitaera
* 2 tygodnie temu Serven i Ralena już zrobili niezłe bydło. Źle się zachowywali wobec ludzi itp.
    * Ralena miała mieć badania na obecność pewnych patogenów na Stacji, odmówiła, potem podobno BARDZO ŹLE SIĘ CZUŁA
* Tydzień temu Ralenę dorwał Mimik.
* 2 dni temu Alissa zorientowała się, że Mimik to nie Ralena.

PRZECIWNIK:

Mimik

* Zmienia wygląd, kontroluje głosy, im bardziej ktoś jest w rozpaczy tym lepiej czyta tej osobie w myślach
* Ma formę bojową
* Żywi się ludźmi - albo musi mieć ludzi dotkniętych Rozpaczą, albo ich po prostu musi zjeść

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

Faza 0: TUTORIAL

* Tonalnie
    * Zagadka połączona z decyzją moralną
    * Trochę wesołości, trochę ostrych akcji
* Wiedza o świecie
    * Drakolici i Savaranie
    * Kult Saitaera, skupiony na Adaptacji
* Kontekst postaci i problemu
    * Chcę pokazać, jak działa Mimik, ale nie pokazując, że Ralena jest Mimikiem.
    * Chcę pokazać, że w tle jest _coś jeszcze_ (konflikt: Kultysta, Mawir)
* Implementacja
    * Savaranin, Drakolita jako grupa agentów ochrony Stacji 
        * próbują USPOKOIĆ i POMÓC 3 drakolitom znaleźć zaginionego towarzysza
    * Poruszanie się po TRUDNYM TERENIE
        * Znajdą drakolitę który wygląda jakby umarł DAWNO TEMU; jego ciało nie jest kompletne
        * Drakolici COŚ PODEJRZEWAJĄ, legendy o Pożeraczu

Faza 1:

* "Biedna Matka", 
* "O, Ralena jest mimikiem", "O, Ralena się żywi i zabija ludzi", "O, Ralena jest całkiem sympatyczna" 
* "Mawir nam pomoże?", "Kultysta tu jest... gdzieś"

| Kto                 | 1 (s0 + 20 min)   | 2 (s0 + 40 min)            | 3 (s0 + 60 min)         | 4 (s0 + 80 min)  |
|---------------------|-------------------|----------------------------|-------------------------|------------------|
| **Mimik, Ralena**   | sama słodycz (drenuje ojca) | poluje na inżyniera | robi "nieudany atak" (wina) | atakuje na serio |
| **Kultysta**        | monitoruje Mimika | wystawia Mawirowców        | zbiera środki do Rytuału | robi Rytuał      |
| **Mawir**           | nic               | terroryzowanie Mawirowców  | poszukiwanie prawdy     | atak na Ralenę   |
| **Ojciec, Serven**  | nic               | obrona Raleny              | ukrywanie śladów        | zagrożenie Alissie |
| **Macocha, Alissa** | nic               | prawda do Kalisty          | prawda do Mawira        | nic              |

Faza 2:

* "O SHIT! Mawir poluje na Ralenę", "Mimik zabije Ojca"
* Rozwiązanie i wykonanie sesji, podsumowanie

| Kto                 | 1 (s0 + 100 min)  | 2 (s0 + 120 min)       | 3 (s0 + 140 min)   |
|---------------------|-------------------|------------------------|--------------------|
| **Mimik, Ralena**   | poluje na randoma | pod kontrolą Kultysty  | Mawir vs Ralena    |
| **Kultysta**        | zwabia Ralenę     | przejmuje kontrolę nad Raleną | Mawir vs Ralena    |
| **Mawir**           | idzie do aresztu  | uwalnia się z aresztu  | Mawir vs Ralena    |
| **Ojciec, Serven**  | uwolnienie Alissy | zabicie Alissy         | zrzucenie winy na Mawira |
| **Macocha, Alissa** | zbieranie faktów  | KIA                    | nic                |

Overall

* chains
    * Zawarte w Fazach powyżej
* stakes
    * stan Raleny / Mimika
    * kto będzie dowodził Kultem?
* opponent
    * Ralena, Mimik
    * Mawir Hong LUB Kultysta
* problem
    * .

## Sesja - analiza
### Fiszki

.

### Scena Zero - impl
#### Jak sesja została opisana ludziom

Matka 16-latki została przymusowo umieszczona w szpitalu na Stacji Kosmicznej z uwagi na jej histeryczną reakcję. Jej zdaniem - jej córka nie jest jej córką, "TO COŚ" to nie ona.

Zgodnie z tym, co Wasza sojuszniczka - czarodziejka (jedyna na stacji) – uważa, ta matka może mieć rację. Nic na to nie wskazuje, ale na tej Stacji nie jest to pierwsza nadnaturalna rzecz, która się zdarzyła. Czarodziejka nie jest jednak pewna; nie ma dostępu do córki a ta jest chroniona przez swojego ojca.

Na tej Stacji Kosmicznej działa magia. Praktycznie nikt o magii nie wie i wiedzieć nie może, bo dojdzie do katastrofy. Na Stacji jest też lokalna czarodziejka (o której nikt poza Wami nie wie). Lokalne służby rozpatrują problem "normalnie" – Wy jesteście jedynymi którzy o magii wiedzą.

Jesteście grupą lokalnych mieszkańców Stacji (np. inżynier środowiskowy, celnik, księgowy). Macie przyjaciół na Stacji, koneksje i zasoby. Waszym zadaniem jest odkryć prawdę – czy córka jest sobą, czy jest potworem? Czy matka jest ofiarą magii, czy może ma rację? Następnie musicie znaleźć problem powiązany z anomalią magiczną i go odpowiednio rozwiązać.

Jeśli Wam się nie uda, sporo niewinnych ludzi zginie, bardzo cierpiąc.

Powodzenia.

#### Implementacja właściwa

Obszar Stacji kosmicznej Szernief, który miał poważne awarie po tym, jak w wyniku pojawienia się potwora Unumens został wysadzony przez biosynty i Agencję Lux Umbrarum jest naprawiany i przebudowywany. 

W tym obszarze zniknął jeden drakolita. To mawirowiec - i inni mawirowcy pomagają szukać. 

3 oficerów ochrony przybyło pomóc znaleźć zaginionego człowieka.

TrZ+2:

* X: to 1 drakolita wezwał wsparcie (snitches get stitches)
* X: szukają, nie chcą współpracować
* X: rift mawirowcy - Stacja jest naprawdę duża
* V: wiemy, gdzie drakolita zniknął z kamer - nigdy nie opuścił tego terenu, jest na kamerach
* V: drona znalazła zmumifikowane zwłoki - drakolita zniknął kilka godzin temu, ale zgodnie ze stanem ciała - kilka dni temu?
* V: Folgor został aresztowany (dla jego własnego bezpieczeństwa), opowiedział legendę o Pożeraczu

Lokalni mawirowcy nie przyjęli ochrony z wielką sympatią. Do tego stopnia, że aż ochrona zaczęła się zastanawiać kto ich wezwał. Okazało się, że to był jeden z mawirowców – Folgor - zaadaptowany bardziej do badań naukowych niż pracy fizycznej.

Inni mawirowcy spojrzeli na niego dość złośliwie („snitches get stitches”), ale Folgor decyduje się współpracować z ochroną – mimo, że napięcie na Stacji jest podniesione pomiędzy mawirowcami i resztą stacji.

Ochrona ściągnęła drony poszukiwawcze po tym jak potwierdziła na kamerach, że faktycznie drakolita stąd nie wyszedł. Przy użyciu dron udało im się znaleźć zmumifikowane zwłoki - wszystko wyglądało na to, że drakolita zginął dużo wcześniej niż zginął (co nie ma sensu). Zwłoki zostały też znalezione w miejscu, w którym drakolita miałby problemy się dostać, wysoko pod sufitem. Były częściowo sprasowane i wciągnięte przez bardzo silną istotę.

Folgor jest przerażony - opowiedział ochronie (czego nie powinien był robić zgodnie z Doktryną Kultu), że istnieją legendy o Pożeraczu - istocie, która zastępuje nie dość godną ofiarę. Wielki Test Saitaera. 

Temat został potraktowany poważnie, acz Pożeracz nie. Temat trafił na biurko Feliny, skąd dorwała się do niego Czarodziejka.

### Sesja Właściwa - impl

Czarodziejka zajmowała się tym sama. Ale po usłyszeniu sytuacji z Alissą, że "coś zastąpiło jej córkę", Czarodziejka uruchomiła Zespół - trzeba na to spojrzeć, a ona nie może by nie pokazać że istnieje. 

Naukowiec zaczął od analizy zwłok, z pomocą Celnika (który się na tym trochę zna). Potwierdzili hipotezę Czarodziejki - są tam wyraźne ślady energii Interis. Ale oprócz tego da się zobaczyć echo perfekcji adaptacyjnej – ixion. Czyli jest tam też coś nowego.

* X: Jest wiadomość, że są zainteresowani zwłokami (Ragasz i Ralena / Mimik się dowiedzą)
* V: Energie: Interis, Ixion. Zwłoki nie mają mózgu i części układu nerwowego. Przeciwnik jest zmiennokształtny


Najwięcej powinna wiedzieć Alissa, ale nikt nie brał jej na poważnie. Inżynier poszedł się z nią spotkać, powiedział jej, że on jej wierzy. Alissa jest wyraźnie wytrącona z równowagi, całkowicie zdestabilizowana. Boi się rozmawiać bez kamer. Opowiedziała, że jej przybrana córka, Ralena, zachowuje się momentami inaczej. Gdy Alissa na nią nie patrzy bezpośrednio, Ralena ma nieprawdopodobnie dużo ostrych zębów. Ralena czasami się uśmiecha bawiąc się nożem patrząc na swoją macochę.

Alissa jest pewna, że cokolwiek stało się Ralenie, teraz jest to istota niebezpieczna i nie jest prawdziwą Raleną.

TrZ+2:

* V: Alissa powiedziała o Ralenie
    * różne głosy, ma ostre zęby jak się na nią nie patrzy, WIE co Alissa wie (jest w jej głowie)
    * to są cechy spójne z Pożeraczem
    * Alissa miała w przeszłości problemy psychiczne i epizod psychotyczny. Ralena wykorzystała to przeciwko niej.
* V: Analiza danych z kamer pokazuje
    * "Ralena" wchodzi na teren rumowiska z innymi ludźmi
    * 10 drakolitów wchodzi
    * wychodzi "Ralena"
    * 9 drakolitów wychodzi
    * -> Ralena mogła zabić i zjeść drakolitę

Inżynier upewnił się, że idą w dobrym kierunku - sprawdził nagrania z kamer. Faktycznie, Ralena zawsze była osobą bardzo niesforną, robiła co chciała. I istnieje punkt w czasie, w którym Ralena była na rumowisku w tym samym czasie co zniknął ów drakolita.

Ale jak udowodnić, że to nie jest ta sama dziewczynka, która przybyła na Stację?

Anara z przyjemnością odpowiedziała na te pytania, zawsze lubi słyszeć jak savaranin wchodzi w głąb mitów Saitaera.

* Pożeracz nie jest istotą czysto Saitaera. Jest to potwór często traktowany jako wielki test. Ogólnie, musi być przyzwany w rytuale i ktoś musi umrzeć by można było stworzyć Pożeracza. 
    * Ktoś musiał wezwać Pożeracza. To nie jest istota Saitaera, to jest coś _innego_, Wielki Test. Nikt tu nawet nie wie jak wezwać Pożeracza
    * -> to implikuje, że jest tu ktoś jeszcze. Ktoś nowy. Nowy Kultysta Saitaera nie bojący się okrutnych rytuałów
* Zdaniem Mawira, Pożeracz w tym momencie i w tym miejscu nie ma sensu. On jest głęboko oddany Jego Ekscelencji, a dodatkowo nikt nie ma pojęcia jak dokładnie tworzy się Pożeracza.
    * Mawir konsekwentnie szuka Nowego Kultystę Saitaera oraz potwora.
* Mawir zdecydował się znaleźć i pożreć Pożeracza. Jeżeli to jest test, on zda ten test.
* Pattern "głosy + niszczenie duszy i nadziei + zjadanie układu nerwowego" pasuje. Mamy Pożeracza.

Jak znaleźć zmiennokształtnego potwora, który potrafi przyjąć dowolną formę, im bardziej człowiek jest zdesperowany tym bardziej wchodzi mu w głowę i żywi się ludzkim układem nerwowym i rozpaczą?
Ktoś musiał zginąć by mógł pojawić się Pożeracz. Trzeba znaleźć Pacjenta Zero. Kto zginął pierwszy? Jaki drakolita z Kultu Saitaera był pierwszym który zginął, gdzie i kiedy?

Szczęśliwie Celnik posiada przyjaciela w ochronie, który uwielbia patrzeć na kamery jak na reality show. Jeżeli ktokolwiek jest w stanie znaleźć odpowiedź na te pytania to będzie to on.

Tr Z+2:

* V: Galian Truft; on był pierwszym. Po drodze było jeszcze 2 drakolitów a potem Ralena
* V: 40 dni temu Galian stanął przeciw Mawirowi; 45 dni temu Ralena dotarła na Stację. Czyli _Ralena była Raleną przez pewien czas_
* X: Kult jest zainteresowany Celnikiem. Mawir będzie chciał z nim porozmawiać.

Dzięki informacjom z kamer udało się zlokalizować najbardziej prawdopodobną lokalizację i moment śmierć Raleny. Ralena najprawdopodobniej zginęła gdzieś na terenach o mniejszym pokryciu kamerami. Jak można znaleźć na tego typu stacji zmumifikowane ciało dziewczynki?

Inżynier zwrócił się do Mary Czuk, z Teraquid. Jeżeli ktokolwiek ma dostęp do metod śledzenia i tropienia, będą to byli najemnicy z Teraquid. Mara wie więcej o magii, niż ktokolwiek by się spodziewał; gdy Inżynier wyjaśnił jej że szukają zmumifikowanego ciała w stylu (demonstracja ciała drakolity z kostnicy), Mara zaproponowała wykorzystanie sprzętu śledzącego, trackerów, zwiadowców Teraquid oraz psów. Mara - wiedząc że szukają dziecka (16-latki) - jest skłonna użyć większe środki niż by się wydawało. 

Ex Z +4:

* V: Ralena zginęła tydzień temu, poznali lokalizację w odległości 15 minut od ostatniego nagrania z kamer
* V: Znaleźli miejsce rytuału stworzenia Pożeracza, jak i znaleźli ciało Raleny

Siły Teraquid w połączeniu z danymi z kamer zlokalizowały ostatniej chwile Raleny. Nie było to bardzo trudne - wpierw obserwowali, jak na przestrzeni 3-4 dni Ralena jest coraz bardziej zrozpaczona na kamerach. Jak gaśnie. Jak przestaje być radosną dziewczyną. Jak nie ma nadziei. Widzieli też, jak w pewnym momencie zdeptana życiem Ralena idzie w okolicach mniej monitorowanych przez kamery. Nagle coś usłyszała. Zaciekawiona, poszła w tamtą stronę.

15 minut później uśmiechnięta Ralena, z lekkimi podskokami wraca do części arystokratycznej Stacji. To już nie jest ona.

Mając te informacje i mając kilkanaście osób ze sprzętem, Mara przyniosła zmumifikowane, podzielone na kawałeczki ciało Raleny. Mara miała tylko jedną prośbę do Inżyniera - cokolwiek tam się nie znajduje, ona chce zabić to pierwsza. Inżynier się zgodził.

Podczas wszystkich tych operacji, Naukowiec dostał wiadomość od Czarodziejki – Pożeracz zaatakował ponownie. I mu się nie udało. Zawiódł. Mimo, że powinien był w stanie bez żadnego problemu zabić swoją ofiarę. Naukowiec poszedł za ciosem - musi się spotkać z ofiarą.

Niedoszłą ofiarą okazał się być drakolita imieniem Bormon Gronak. Jeżeli chodzi o poziom intelektu, Bormon nie jest jednym z najmądrzejszych. Bormon opowiedział, jak to od pewnego czasu rzeczy mu nie wychodziły i ogólnie miał kiepski humor. Gdy przechodził koło rumowiska, usłyszał płacz swojego młodszego syna. Szybko pobiegł zobaczyć co się dzieje i wtedy zza pleców coś się na niego rzuciło.

Bormon próbował się wyrywać i próbował walczyć, ale nie miał szans. Nie wiedział, dlaczego jeszcze żyje - przeciwnik byłby w stanie zabić go, gdyby chciał - po prostu bawił się losem Bormona. Gdy drakolita zaczął płakać i stracił resztę nadziei, podczas szamotaniny na ziemię wypadł medalionik zrobiony przez jego syna. Ot, nic wielkiego – dziewięciolatek + zdjęcie + niewprawne używanie drukarki 3d.

Wtedy Bormon usłyszał głos swojego syna w uszach. „Zamknij oczy i licz do 5. Potem wynoś się stąd. Jeśli otworzysz oczy lub spojrzysz za siebie, nie żyjesz.” Bormon powiedział, że liczył aż do 10. 

...czemu on przeżył?

* V: zlitował się

Naukowiec analizował różne hipotezy. Może Bormon miał za niską wiarę? Za wysoką? Może nie stracił nadziei? Ale po przeanalizowaniu danych tylko jedna możliwość miała sens – Pożeracz się **zlitował** nad Bormonem.

Coś, czego istota interis-ixion po prostu nie potrafi zrobić.

Naukowiec głębiej analizuje sytuację. Wszystko wskazuje na to, że Pożeracz powoli staje się osobą, ale nadal biologicznie jest tym czym jest. Czymkolwiek Pożeracz nie był, jakikolwiek nie miał agendy, nie chce być tym czym jest.

Zespół uznał, że może to jest okazja do uratowania Potwora. Może nie trzeba go zabijać? Może jest możliwość wyciągnięcia Raleny z Pożeracza? Do tego celu jednak potrzeba spotkać się z Raleną. Więc wszyscy – poza Celnikiem (nadal na dywaniku) - poszli porozmawiać z Raleną.

Ojciec, gdy usłyszał, że oni chcą pomóc Ralenie, pozwolił im się spotkać z dziewczyną. Ralena wie o tym co oni wiedzą – jest Pożeraczem, jest w ich głowach - więc nie ukrywa swojej natury. Ale ona pokazuje wyraźnie - jest zmęczona. Jest zagubiona. Nie wie do końca czym jest. Nie wierzy, że można jej pomóc. Nie chce robić nikomu krzywdy, ale jest tak strasznie głodna.

Zespół zaproponował jej próbę użycia magii by ją naprawić. Ralena jest bardzo sceptycznie nastawiona do tego planu, ale zespół informuje ją, że na stacji jest Czarodziejka. A energia Alteris, energia Nie-Rzeczywistości, jest w stanie robić dziwne rzeczy.

Ralena się zgadza, jeśli jej ojcu nic się nie stanie. Ona tak bardzo nie chce być tym czym jest.

Ex M +3 +5Ob +5Or (poświęcenie ojca):

* Vm: Ralena/Pożeracz stanie się "osobą". Będzie mogła żyć nie zabijając innych.
* Or: Krew Ojca i sam Ojciec posłużyli jako paliwo do zmiany Raleny. Umarł z uśmiechem, chroniąc swoją córkę.
    * Ralena dostała jego formę jako nową do której może mimikować. 
    * Ralena ma umiejętności, ale jest nową istotą - na ixion-interis weszły komponenty fidetis/esuriit-alteris
        * Ralena jest czymś NOWYM
* Om: W tym samym czasie w innym miejscu Kultysta przygotowywał Rytuał mający przejąć kontrolę nad Raleną.
    * interakcja Rytuałów -> Kultysta uzyskał JAKIEŚ moce i umiejętności. Traktuje to jako znak od Saitaera.

Nadal nie rozwiązuje to problemu, że Mawir chce pożreć Ralenę. Celnik z nim rozmawia (w końcu jest na dywaniku) – wytłumaczył, że Ralena nie chce walczyć i nie jest chętna do walki. Że jest tu gdzieś Kultysta, który stworzył Ralenę i który pragnie zniszczyć lub przejąć Kult. On jest prawdziwym problemem. Przedstawia Pożeracza jako istotę niegodną uwagi a Kultystę jako prawdziwe zagrożenie.

Tp+4:

* V: Mawir gardzi Pożeraczem. Da jej spokój. CZARODZIEJKA go zniszczyła? (Mawir wie kim ona jest)
* X: Mawir bierze sprawę Kultysty osobiście. Jego ma zamiar zjeść. To on jest testem Saitaera, nie Pożeracz
* X: Rywalizacja - Mawir i Kult nie będą współpracować ze Stacją, ale mogą rywalizować z Czarodziejką; zmiana Raleny jest dowodem, że Saitaer pragnie ich udziału. 

Czas się zmierzyć z Kultystą...

## Streszczenie

Na CON Szernief pojawił się Mimik/Pożeracz, istota ixion-interis. Zastąpił Ralenę Karwist i polował na ludzi. Został stworzony przez Kultystę Saitaera który przybył z zewnątrz, by przejąć kontrolę nad lokalnym kultem. Alissa, macocha Raleny, zorientowała się, że coś jest nie tak z jej przybraną córką i rozpoczęła śledztwo, które miało na celu odkrycie prawdy o tym, co się z nią stało. Mimik zniszczył życie Alissie, ale jednak zaczął być zmęczony swoją naturą, nie chciał być tylko potworem. Gdy Zespół dotarł do Mimika, Czarodziejka przeprowadziła Rytuał który miał Mimika zmienić w osobę. Ostatecznie, z pomocą magii i poświęcenia ojca Raleny, Mimik przekształcił się w nową formę - osobę zdolną do życia bez zabijania. Ale Kultysta został wzmocniony i problem nie został jeszcze rozwiązany.

## Progresja

* Ralena Karwist: kiedyś 16-latka, potem Pożeracz Ixion-Interis, teraz nowy byt, przekształcony w rytuale przez Kalistę (Alteris-Esuriit/Fidetis) z poświęceniem swojego ojca (z własnej woli).
* Mara Czuk: chce zniszczyć Ralenę / Pożeracza. Nie akceptuje istnienia tego typu anomalii / potwora, zwłaszcza na tej stacji. Jest bardzo antypotworowa i antyanomalna.
* Ragasz Morkol: dzięki Rytuałowi Czarodziejki dostał (przypadkowo) moce magiczne (ixion-alteris), co uważa za Znak od Saitaera.

## Zasługi

* Klasa Inżynier Unifikator: spotkał się z Alissą i dowiedział się czym jest Pożeracz; potem zwrócił się do Mary Czuk z Teraquid i dotarł z jej pomocą do zmumifikowanego ciała oryginalnej Raleny.
* Klasa Savaranin Celnik: dzięki przyjacielowi w ochronie doszedł do znalezienia jak powstał Pożeracz; potem przekonał Mawira, że Ralena/Pożeracz nie jest godna walki i prawdziwym wrogiem jest Kultysta.
* Klasa Savaranin Naukowiec: analiza zwłok umożliwiła dojścia do natury Pożeracza; potem gdy Pożeracz nie zabił ofiary doszedł do tego co się dzieje - Pożeracz staje się osobą a nie bytem z legend.
* Folgor Dubuk: drakolita-mawirowiec chętny do współpracy ze Stacją; współpracował z ochroną i powiedział im legendę o Pożeraczu mimo ryzyka personalnego ("snitches get stitches"). Aresztowany dla własnego bezpieczeństwa.
* Mawir Hong: brutalnie spacyfikował przestraszonych mawirowców i szuka Pożeracza by go pokonać i pożreć. Zmienił cel - znaleźć Kultystę i w imię Saitaera osiągnąć mistrzostwo na Stacji.
* Serwent Karwist: okrutny i pogardliwy przedsiębiorca z trophy wife i córką; kochał jednak swoją córkę na tyle, by chronić Ralenę NAWET jak zastąpił ją Pożeracz, bo "ona tam jest". Oddał dla niej życie, by ją naprawić.
* Alissa Karwist: trophy wife, kochająca Ralenę jak własne dziecko. Jako pierwsza zrozumiała i podjęła działania, by ujawnić prawdę o zastąpieniu jej córki przez Mimika. Zamknięta w _psych ward_, próbowała uratować Ralenę za wszelką cenę mówiąc wszystkim o Ralenie.
* Ralena Karwist: wpierw rozpieszczona 16-latka, potem pożarta przez mimika/Pożeracza i polowała na ludzi by ich zjadać. Z uwagi na interakcje magiczne nie chciała być tym czym jest i w końcu współpracowała z Zespołem, by stać się osobą. 
* Sulut Kridalir: nieszczęśliwy główny lekarz Szernief, który nie rozumie co się tu stało - zwłoki nie powinny być zmumifikowane i wyssane ze wszystkiego przydatnego.
* Anara Grabnik: szefowa komórki kultu Saitaera stojąca nad Celnikiem; opowiedziała mu legendy o Pożeraczu i wysłała na dywanik do Mawira.
* Kalista Surilik: Czarodziejka; skojarzyła Pożeracza Saitaerowców z problemem Alissy ("to nie moja córka"). Przeprowadziła Rytuał który zmienił Ralenę z Pożeracza w osobę oraz nadał Kultyście moc magiczną.
* Ragasz Morkol: Aspirant Kultu; uważa Mawira i ten Kult za niegodne. Wpierw obrócił Galiana przeciw Kultowi, potem zeń stworzył Pożeracza. Dzięki rytuałowi Czarodziejki uzyskał moce magiczne.
* Galian Truft: drakolita, który uwierzył w Lepszy Kult i stanął naprzeciwko Mawira. Dostał dewastujący wpiernicz od Mawira. Potem oddał się rytuałowi Ragasza by stworzyć Pożeracza. KIA.
* Mirt-17: savarański oficer ochrony traktujący kamery jak reality show; dzięki niemu udało się dojść do momentu śmierci Raleny i kto był Pacjentem Zero tworzącym Pożeracza.
* Mara Czuk: na prośbę Inżyniera dostarczyła kluczowe wsparcie logistyczne i technologiczne z Teraquid (trackery, psy), które umożliwiło odnalezienie i zabezpieczenie ciała Raleny.

## Frakcje

* Anomaliści Szernief: skutecznie zlokalizowali Pożeracza/Mimika/Ralenę i przekształcili ją w osobę. Napuścili Mawira na Kultystę.
* Con Szernief Mawirowcy: kult Saitaera posiada legendy o Pożeraczu; o tą frakcję rozgrywa się walka między Mawirem i Ragaszem. 
* Con Szernief Teraquid: wysłali kilkanaście osób by pomóc Anomalistom znaleźć ciało Raleny. Psy, tracker-drony i zwiadowcy pomogli znaleźć ciało. Przygotowują się do walki z anomaliami.

## Fakty Lokalizacji

* CON Szernief: fragment uszkodzony w historii z Unumens jest naprawiany; tam pojawił się Mimik / Pożeracz, by polować na ludzi.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Kalmantis
            1. Sebirialis, orbita
                1. CON Szernief

## Czas

* Opóźnienie: 64
* Dni: 2
