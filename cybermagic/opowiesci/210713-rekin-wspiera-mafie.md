## Metadane

* title: "Rekin wspiera mafię"
* threads: rekiny-a-akademia
* gm: żółw
* players: anadia, kić

## Kontynuacja
### Kampanijna

* [210622 - Verlenka na grzybkach](210622-verlenka-na-grzybkach)

### Chronologiczna

* [210622 - Verlenka na grzybkach](210622-verlenka-na-grzybkach)

### Plan sesji
#### Co się wydarzyło

.

#### Sukces graczy

* .

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Gdy Marysia leciała po Julię (ta się wprosiła do Marysi na chatę by wyżerać sałatki i wypijać mleko. I wszystko.) do Zaczęstwa, musiała, jak zawsze, przelecieć nad Lasem. I gdy spokojnie sobie leciała, dostała wiadomość na hipernecie od kogoś na ziemi. "Ląduj, to ważne".

A na ziemi Gabriel Ursus. Uczeń terminusa, arystokrata. Złote loki.

Gabriel wyjaśnił Marysi następujący problem:

* Jakiś "genialny" Rekin wpakował się w przemyt. Na terenie Dzielnicy Rekinów jest magazyn.
* Powiązanie z jedną z mafii - albo Kajrata albo Grzymościa (najpewniej Kajrata)
* Gabriel za kilka dni (6?) będzie robił operację eliminacji problemu - przechwycenia.
* Gabriel nie chce, by Rekinom i relacjom Szczeliniec - Aurum stała się krzywda.
* Typ towarów to albo technologia noktiańska albo chemikalia i narkotyki
* Gabriel wie dzięki pracy operacyjnej wykonanej przez terminusów: śledzenie, drony...
* Dyskrecja jest wskazana, bo będzie kłopot. Duży.

Marysia przyjęła smutne zadanie Gabriela. Poleciała do Julii i stamtąd prosto do swojego Epickiego Apartamentu w Podwiercie i w skrócie wyjaśniła Julii problem, nie mówiąc słowa o spotkaniu Gabriela.

Jak się dowiedzieć? Rafał Torszecki, człowiek-plotka.

Marysia zaprosiła do siebie ("oczekuję Cię za godzinę i się nie spóźnij") Rafała Torszeckiego. Torszecki pojawił się szybko. Z radością.

Marysia powiedziała Rafałowi, że ktoś szarga dobre imię Rekinów. Przemyty i konszachty z mafią. Rafał jeszcze "nie czai" dobrego imienia Rekinów, ale ok. Marysia sobie NIE ŻYCZY dobrego imienia Rekinów by było szargane. Marysia żąda, by Rafał znalazł tajny magazyn z CZYMŚ. Rzeczy z przemytu. Coś co może im zaszkodzić. Lub nazwisko właściciela magazynu. Aha, ale nikomu ani słowa.

Rafał przyjął do wiadomości. Marysia kazała "na jutro". Rafał zrobi co może.

Rafał szuka Tajnego Magazynu dla Marysi (Ex+2):

* V: ma dla Marysi nazwisko - Kacper Bankierz.
* X: dostał pomniejszy aspekt wpierdolu. Ale zwiał.
* V: udało mu się ujść w miarę nienaruszonym

Julia poszła naprawić czyjś ścigacz (Franka Bulteriera). Franka może teraz tu nie być, ale jego ścigacz jest. Julia widzi, jak Rafał rozpaczliwie ucieka przed _czymś_, zanosząc się kaszlem. Nie umie na tym nawet skupić dobrze wzroku. Jest to jakaś forma anomalii. Na oko, fizyczna. Są na Stadionie Lotników.

Julia próbuje opanować chory ścigacz Franka. Trzeba uratować Rafała od anomalii! Julia używa linek, wiat i MIRV (jak trzeba).

ExZ+3:

* V: Julia używając ścigacza Franka rzuciła się w stronę anomalii; podebrała go i leci w górę by obejrzeć co się stało.
* XX: Anomalia zanika, zniknęła, rozpłynęła się. Julia widzi, że to coś bardzo dziwnego. Niespecjalnie chce tego szukać. To teren i problem Marysi.

Julia zaczyna zapominać informacje o anomalii. Natychmiast wysyła wiadomość tekstową do Marysi. Rafał też wybełkotał nazwisko które Julia wysłała.

15 minut później Julia spokojnie naprawiała ścigacz jakby nic się nie wydarzyło a Rafał stał nad nią i podpowiadał jak Julia ma wykonywać swoją pracę (nie znając się na tym i irytując Julię).

Marysia ma nazwisko i informację o chorej, dziwnej anomalii. A nazwisko to Kacper Bankierz.

Marysia zapowiedziała się u Kacpra. Ten z przyjemnością ją zobaczy. Przybyła doń.

Marysia powiedziała mu, że wie o magazynie i wie o przemycie. Kacper rzekł, że "przemyt" to brzydkie słowo na "handel", plotki często mówią za dużo a on nie ma powodu niczego mówić Marysi. To jego domena, jego teren, jego moc i możliwości a ona ma swoje. Zostają w pozytywnych stosunkach, ale Kacper nie zamierza się dzielić.

Marysia naciska - ona WIE, że akcja jest spalona. Nie chodzi o dolę. Ona chce go ratować. Kacper się uśmiechnął przyjaźnie. Niczego mniej się nie spodziewał od tien Sowińskiej. Ale nie da jej przejąć jego kanałów.

Marysia zauważyła - czy Kacper woli wylądować w Kazamatach pod Barbakanem? Kacper powiedział, że przecież nie on ma zamiar to robić osobiście. Najwyżej ktoś inny ucierpi. Tak jak Marysia ma swojego Toszeckiego, tak on ma swoich.

Kacper obiecał Marysi, że nie zostanie złapany i postara się, by nikt nie został złapany. Podziękował jej za troskę i zmodyfikuje operację w taki sposób, by jeśli jest spalona do niczego nie doszło.

Przed Marysią stoi trudna decyzja. Co Marysia chce zrobić? W sumie - zdecydowała. Marysia uznała, że chciałaby uniemożliwić Kacprowi działania z mafią. Ona może działać z mafią, Kacper nie. To są środki, wpływy, możliwości. Ale tak trzeba to zrobić, by Pustogor nie miał pojęcia że Marysia jest powiązana z mafią, zwłaszcza mafią Kajrata. Czyli potrzebny jest potencjalnie frontman. Toszecki odpada.

Marysia wpadła na pomysł. Przecież jest Arkadia Verlen. Która siedzi i gnije w kiciu bo była odurzona (środkami Myrczka). Ale mafia Kajrata ma różne chemikalia. Kto powiedział, że to np. nie było NAPRAWDĘ środkami Kacpra Bankierza? Więc gdyby Arkadia miała (fałszywe) ślady i gdyby miała chęć zatrzymać Kacpra a Marysia jedynie dostarczy jej pewnych środków czy planów... to Arkadia mogłaby się zemścić :3.

I byłoby spoko :3.

A to może sprawić, że mafia zacznie stwierdzać, że Kacper nie jest spoko ;-).

Marysia udała się do Uli. Trzeba uwolnić Arkadię. Ula miała nadzieję, że jednak terminusi mogą wejść tam legalnie. Marysia stwierdziła, że niestety nie da się tego zrobić w ten sposób bo terminusi nie aresztują szefa za tym stojącego a tylko rekinie płotki. 

Marysia powiedziała też Uli, że ona DOMYŚLIŁA się że jest taka akcja. I jak się troszkę upiła to się wygadała. Ula nie doceniła. "Innymi słowy, będziemy ośmieszeni przez TWOJĄ słabość, tien Sowińska?". Marysia zauważyła, że nie musiała przychodzić. Ula się zgodziła z tym stwierdzeniem.

"Czemu jako Sowińska nie powiesz im wszystkim "nie pieprzymy się z mafią?"". Marysia zauważyła, że to tak nie działa. Ula powiedziała wyraźnie - nie może po prostu iść do terminusów i powiedzieć im "ej akcja spalona wypuśćcie Verlenkę". Ona jest uczennicą. NIkt jej nie posłucha. Musi mieć coś więcej.

Marysia na podstawie swojego researchu wie, że jeden z terminusów, Tomasz Tukan, ma osobiste zatargi z mafią Kajrata. Więc może jej się uda przekonać Tukana. Zwłaszcza, że Tukan kiedyś był zainteresowany współpracą z Sowińskimi. Powiedzmy szczerze - lubi luksusy, blichtr i ładne kobiety. Tukan ma standardy które wymagają PIENIĘDZY. A jest terminusem. Nie ma dużo pieniędzy.

Ula nie będzie dziewczyną na posyłki dla Marysi. Nie będzie przeszkadzać terminusowi (Tukanowi) i mówić mu, że Sowińska uważa, że powinni porozmawiać. Marysia nawet nie prosi. Tr+2.

* V: Ula pójdzie do Tukana.
* X: Ula pójdzie tylko wtedy, jeśli Marysia poprosi Tukana.

Ula rzuciła Marysi Stosunkowo Złe Spojrzenie, nie jest dziewczynką na posyłki, ale zależy jej na tej operacji...

Wieczorem skontaktował się z Marysią Tomasz Tukan. Marysia wyjaśniła, że przypadkiem spaliła się akcja terminusów. Tukan zrozumiał "przypadkowo", czyli to plan Marysi Sowińskiej. I ona chce Arkadii na wolności. I to co może mieć z tego Tukan - prztyczek w nos mafii Kajrata i coś dla niego. Tukan powiedział, że jest zainteresowany weekendem z pewną młodą damą w luksusowym hotelu Sowińskich w Aurum. Marysia powiedziała Tukanowi, że Arkadia nie może wiedzieć że ona jest uwolniona przez Sowińską. Tukan powiedział, że intrygi Aurum go mniej interesują niż korzyści Aurum.

Marysia udaje się do siebie i wzywa Rafała Torszeckiego. Torszecki pojawia się u swojej pani. Marysia ma do niego sprawę. Wymagającą intelektu. Marysia pyta, czy on się chce zemścić? On patrzy na nią i nie wie jakiej odpowiedzi szukać. Ale Marysia go naprowadziła. Chce, by Torszecki sfabrykował dowody by Arkadia myślała że to Kacper Bankierz stoi za tym, że ona była odurzona. Torszecki się ucieszył, że się może zemścić na obu (Arkadia go nożem a anomalia rozwaliła mu płaszcz).

Torszecki składa fałszywe dowody i je tak podkłada, by nikt się nie domyślił: TrZ+3

* V: Arkadia to łyknęła
* X: Bankierz dotrze do Torszeckiego
* V: Arkadia jest odporna na dowody. W jej świecie to była faktycznie robota Kacpra Bankierza
* Vz: Dzięki machinacjom Torszeckiego Kacper Bankierz nie dojdzie do Marysi. Może podejrzewać, ale nie znajdzie dowodów.

Marysia siada na wygodnym fotelu, zakłada nogę na nogę i obserwuje co będzie dalej...

## Streszczenie

Gabriel Ursus powiedział Marysi, że jakiś Rekin uczestniczy w przemycie nielegalnych rzeczy w Podwiercie, na terenie eksterytorialnym. I będzie rajd i przechwycą te Rekiny. Marysia dotarła do tego, że są tajne magazyny (chronione przez anomalię) pod kontrolą Kacpra Bankierza. Rozmowa z Kacprem nic nie dała, więc Marysia doprowadziła do uwolnienia Arkadii z więzienia i zrobiła fałszywe ślady - to Kacper stał za jej odurzeniem. Plus, Marysia odbudowała kontakt z Tukanem...

## Progresja

* Arkadia Verlen: uwolniona z Kazamatów dzięki Marysi (o czym nie wie); święcie przekonana, że Kacper Bankierz jest odpowiedzialny za jej otrucie (dzięki Marysi ofc). Odporna na dowody.
* Kacper Bankierz: wrobiony przez Marysię w to, że niby to on otruł Arkadię. A jest niewinny.
* Rafał Torszecki: Kacper Bankierz uznał, że Torszecki to jego osobisty wróg za poszczucie go Arkadią.

### Frakcji

* .

## Zasługi

* Marysia Sowińska: władczyni marionetek; Torszeckim znalazła Kacpra Bankierza, gdy ów nie chciał przestać współpracować z mafią to używając Uli dotarła do Tukana, uwolniła nim Arkadię i poszczuła nią Kacpra.
* Julia Kardolin: uratowała Torszeckiego przed anomalią mentalną używając ścigacza Franka Bulteriera nad którym pracowała. Uciekła z nim wysoko w powietrze i zanim zapomniała o anomalii zawiadomiła Marysię.
* Rafał Torszecki: wpierw działał jako detektyw (wykrył skrytkę Kacpra Bankierza x mafii) a potem jako szpieg (podłożył dowody świadczące że to Kacper otruł Arkadię). Lojalny Marysi.
* Kacper Bankierz: współpracuje z mafią Kajrata by być KIMŚ na tym terenie i nie tylko. Nie chciał się dzielić z Marysią i używa kurierów (agentów) by sam nie ucierpieć. Bezpośrednia kolizja z praworządną Arkadią.
* Urszula Miłkowicz: rozczarowana, że nie przyskrzyni żadnego Rekina. Uważa, że Rekiny pozwalają sobie na zdecydowanie za dużo. Ściągnęła Marysi Tukana - jeśli to może uratować akcję... ale się postawiła. Marysia musiała poprosić.
* Tomasz Tukan: pomógł Marysi Sowińskiej uwolnić Arkadię by rozwiązać problemy Rekinów w świecie Rekinów, za co dostał dobry weekend z piękną damą w luksusowych hotelach Sowińskich w Aurum.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Las Trzęsawny: Gabriel spotkał się tam dyskretnie z Marysią i przekazał jej info o Rekinie Współpracującym z Mafią.
                                1. Dzielnica Luksusu Rekinów
                                    1. Serce Luksusu
                                        1. Apartamentowce Elity: Kacper Bankierz ma tam swoje małe imperium, tak samo jak Marysia
                                    1. Obrzeża Biedy
                                        1. Stadion Lotników: miejsce żałosnej ucieczki Torszeckiego przed anomalią mentalną; uratowała go tam Julia 
                                    1. Sektor Brudu i Nudy
                                        1. Skrytki Czereśniaka: znajduje się tam tajny magazyn przerzutowy mafii Kajrata we współpracy z Kacprem Bankierzem

## Czas

* Opóźnienie: 6
* Dni: 3

## Konflikty

* 1 - Rafał szuka Tajnego Magazynu dla Marysi na terenie Podwiertu. Stary sposób - chodzi i zadaje pytania.
    * Ex+2
    * VXV: Ma nazwisko - Kacper Bankierz, ale został sponiewierany prez anomalię. Uciekł jednak.
* 2 - Julia próbuje opanować chory ścigacz Franka. Trzeba uratować Rafała od anomalii! Julia używa linek, wiat i MIRV (jak trzeba).
    * ExZ+3
    * VXX: Julia porwała Rafała ścigaczem Franka, ale anomalia mentalna wpływa na nią; ona zaczyna zapominać. Wysyła info Marysi ASAP.
* 3 - Marysia przekonuje Ulę by ta powiedziała Tukanowi o co chodzi. Ula nie jest dziewczyną na posyłki dla Marysi - Marysia nawet nie prosi!
    * Tr+2
    * VX: Ula pójdzie do Tukana bo akcja jest ważna, ale pójdzie tylko wtedy jak Marysia poprosi Tukana.
* 4 - Torszecki składa fałszywe dowody i je tak podkłada, by nikt się nie domyślił. I by Arkadia rozwiązała za Marysię problem z Kacprem.
    * TrZ+3
    * VXVVz: Arkadia to łyknęła; w jej świecie SERIO Kacper Bankierz ją otruł. Ale Bankierz dotrze do Torszeckiego (choć nie do Marysi).

## Inne
### Projekt sesji

#### Procedure

* Overarching Theme:
    * gracze mają "kierowanie" (Vision & Purpose); wiedzą gdzie iść i po co tam idą
* Achronologia: x
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * graczom bardziej zależy na postaciach i ich powodzeniu
    * więcej potencjalnych wątków i rzeczy które możemy odrzucić przy krzyżykach
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * gracze widzą jak wpływają na świat. Wpływ jest "ich" - stąd tak tragiczne jest gdy "ich" rzeczy są corrupted / destroyed.

#### Result

* Overarching Theme:
    * Theme & Feel: 
        * Detektive vs głupota ludzka
    * V&P:
        * Ratujemy durnia XD
    * adwersariat: 
        * Kacper Bankierz: próbuje zdobyć potęgę i władzę na tym terenie
        * Ksawery Wojnicki: próbuje zrobić kanał przerzutowy
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * Role i postacie
        * Rekin, akumulujący zasoby dla swojej przyjemności i mocy (potęga + samosterowność): Kacper Bankierz
        * Kajratowiec, zwiększający swoje wpływy korumpując Rekina: Ksawery Wojnicki (chemik Kajrata, "bombshell")
        * Terminus, ratujący młodego Rekina: Gabriel Ursus
* Co się działo w przeszłości
    * Kacper pragnął być wszechpotężny; Pustogor stoi mu na drodze, niewiele osób go słucha
    * Ksawery dostarczył mu potężnej noktiańskiej technologii i środków
    * Kacper zaczyna być przerażony tym co może ta technologia zrobić
    * Ksawery go skorumpował. Nie musi już szantażować.
    * Kacper składuje niebezpieczne, groźne przedmioty tu, w eksterytorialnym miejscu
    * Kacper prowadzi handel z tego miejsca. Cieniaszczyt -- Aurum -- Miasteczko
    * Terminusi wiedzą o problemie, ale nie wiedzą dokładnie jak dochodzi do przemytu
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * Kacper wpadnie całkowicie pod kontrolę Ksawerego albo zostanie aresztowany przez Gabriela
