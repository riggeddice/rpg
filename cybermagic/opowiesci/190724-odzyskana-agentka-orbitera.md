## Metadane

* title: "Odzyskana agentka Orbitera"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [190721 - Kirasjerka najgorszym detektywem](190721-kirasjerka-najgorszym-detektywem)

### Chronologiczna

* [190721 - Kirasjerka najgorszym detektywem](190721-kirasjerka-najgorszym-detektywem)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

#### Poprzednia

* Ohglarhalg porwał niedoświadczoną agentkę Orbitera testując potwora - Hlarwagha.
* Agentka Orbitera była kiedyś PORWANA przez Orbiter; nędzny szczur miejski z okolic Aurum.
* Znajdująca się na wakacjach Mirela zareagowała natychmiast. Była tu bo szukała autografu Serafiny.

#### Teraz

* Starzy przyjaciele Pięknotki porwali "Emulatorkę". Ale to nie Emulatorka - to zwykła agentka.
* Agentka nie chce wracać do Orbitera; jest uzależniona od kralotha.
* Orbiter nie ma jak odzyskać agentki dzisiaj. Ale da radę w przyszłości.

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Mirela została pod Cieniaszczytem, w Przejściaku. Została wyposażona w awaryjny servar, ale nie klasy Calibris. Smoku, ratuj - Mirela zaczęła robić research na temat prywatnego życia Aidy. A Pięknotka w tym czasie znalazła się w Szkarłatnym Szepcie, by tam spotkać się z Julią. Pięknotka powiedziała Julii, że w Pustogorze porwano dziewczynę. Ta nie skojarzyła - pomoże. Ale gdy Pięknotka podała okoliczności, Julia drgnęła (Tp: KS). Julia powiedziała, że przecież porwali Emulatorkę. Pięknotka zauważyła, że nie. Nie porwali Emulatorki. To była zwykła dziewczyna.

Julia powiedziała Pięknotce całą historię - próbowali uwolnić Emulatorkę. Dlatego porwali Emulatorkę. Nie wiedzieli, że jest tam druga agentka. I teraz Julia nie rozumie, czemu Aida jest w Nukleonie. Narzędzie do tego służące - Hlarwagh - zostało zaprojektowane przy pomocy Julii do eliminacji Emulatorki. Nie ma POJĘCIA co się stanie, jeśli Hlarwagh złapie maga...

Pięknotka załamała się lekko. Poszła porozmawiać z Aidą - do Nukleona. Tam spotkała się z Mirelą Niecień. Ona pomaga Aidzie. Lekarka uśmiechnęła się do Pięknotki - poznała. Powiedziała Pięknotce, że Aida miała bardzo ciężkie Skażenie kralotyczne; ale jest już w dobrym stanie. Niedługo (pojutrze) wypiszą ją do domu. Pięknotka poszła porozmawiać z Aidą; ta jednak jest w totalnie rozmemłanym stanie. Pięknotka MUSI ją złamać by czegoś się dowiedzieć, ale nie ma możliwości...

Pięknotka zatem zdecydowała się skontaktować ze starym znajomym z Orbitera, opuszczając Cieniaszczyt, z Hotelu Pirat. Adam Szarjan. Szarjan roztoczył przed Pięknotką opowieść, że na orbicie znajdują się statki, derelicty itp. I Orbiter próbuje dalej znajdować te "cywilizacje" i je reintegrować. Oni skupiają się na kosmosie, nie na planecie. W jednym z takich statków znaleźli superoptymistyczną Aidę. Z agorafobią. Aida bała się kosmosu, więc Orbiter załatwił jej przejście na Astorię. No i Aida trochę podróżuje, bardzo chciała zobaczyć świat. Nie jest ważna, niewiele wie, nie jest agentką. Jest cywilem Orbitera.

Pięknotka chce płakać. Tu wszystko co mogło pójść nie tak poszło nie tak.

Pięknotka wróciła do Julii. Przekonała ją, by Julia nie skupiała się na uwalnianiu Emulatorek teraz. Julia zobowiązała się, że wszelkie działania związane z łapaniem Emulatorek na terenie Pustogoru przejdą przez Pięknotkę. Bo - co nie mówić - Julia nie ma pojęcia o działaniach Pustogoru. Julia pomoże Pięknotce. Przyznała się, że zaraziła swoimi pomysłami BARDZO entuzjastycznego kralotha - Ohglarhalga. Twórca broni; jest zainteresowany Aidą z uwagi na jej unikalną przeszłość. Jakoś on z nią rezonuje. Tak czy inaczej, pojutrze Aida wychodzi. I Julia ma pomysł - porwijmy ją od kralotha. Pięknotka jest JESZCZE bardziej załamana.

Pięknotka idzie porozmawiać z Mirelą...

Pięknotka wyjaśniła Mireli całą sytuację. To jest cywil Orbitera. A Ohglarhalg ją za bardzo lubi. I ona (Pięknotka) chce dostać tego cywila w swoje ręce - niech Orbiterka wróci do domu. Mirela rozumie problem; Pięknotka nie ma pojęcia jak to rozwiązać, prawda? Mirela ma dwa rozwiązania. Albo próba porwania (Cieniaszczyt pomoże), albo pojedynek. Jej champion (albo ona) kontra champion kralotha. Pięknotka się zgodziła - ma dobry pomysł.

Pięknotka wraca do Hotelu Pirat niedaleko Cieniaszczytu, do Mireli. Ku jej zdziwieniu, Mirela rozmawia z jakimś jegomościem. Przedstawiła go Pięknotce jako przemytnika; kogoś, kto potrafi przejść przez niebezpieczne obszary Skażenia Cieniaszczyckiego i może porwać "cargo". Pięknotka go wywaliła; powiedziała Mireli, że aby odzyskać Aidę, Pięknotce potrzebny jest champion. A Moktar - jej champion - jako jedyną walutę zaakceptuje walkę przeciwko epickiemu przeciwnikowi. Tu: Mireli. Mirela się z przyjemnością zgodziła na walkę - nie będzie się powstrzymywać. Pięknotka w sercu dalej stawia na Moktara.

Pięknotka podbiła do Łysych Psów. Przyniosła mu przegryzki dla "watahy". Powiedziała Moktarowi, że potrzebuje jego siły w odzyskaniu cywila od kralotha. Moktar nie jest zainteresowany. Pięknotka powiedziała, że płaci walką z Kirasjerem. Moktar powiedział, że Pięknotka może ustawić walkę.

Pięknotka rzuciła wyzwanie kralothowi. Faktycznie, walka na arenie się odbyła. Championi kralotha nie mieli żadnych szans ze wściekłym Psem - Moktarem.

Aida wróciła do Mireli. Pięknotka ją z przyjemnością odprowadziła. Aida jest ciągle na haju kralotycznym; Mirela jest przestraszona, ale jej przeszło - Emulatorka. Spytała czy Aidzie przejdzie. Tak, przejdzie.

Mirela stoczyła walkę z Moktarem. Zraniła go dość poważnie nawet, ale nie miała szans. Mimo, że walczyła na pełnej mocy, nie była w stanie pokonać furii Moktara. Nawet Kirasjerka. A Pięknotka patrzyła z ustami TAK - takiej walki jeszcze nie widziała.

Pięknotka zapobiegła silnej eskalacji między dwoma miastami, z którą nawet nie miała nic wspólnego... Just terminus things.

**Sprawdzenie Torów** ():

* skomplikowane.

**Epilog**:

* .

## Streszczenie

Aidę porwała frakcja Cieniaszczytu zainspirowana przez Julię - by ratować Emulatorkę. Ale Aida, jak się okazało, jest nędznym cywilem i to takim uratowanym z kosmosu. Pięknotka wygrała z championem kralotha przez zanęcenie Moktara - a zwabiła go dając mu walkę z Mirelą. Ogólnie, wszyscy są zadowoleni..?

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: rozwiązała problem polityczny; zinfiltrowała Cieniaszczyt, wkręciła Mirelę do walki z Moktarem i vice versa oraz ostatecznie odbiła Aidę... cudzymi siłami.
* Julia Morwisz: opracowała plan uratowania Emulatorki a porwała Aidę. Cóż. Pomogła Pięknotce odkręcić sprawę, ale nie do końca jej wyszło. 
* Aida Serenit: nie agentka a zwykły cywil Orbitera; całkowicie niegroźna. Uratowana z kapsuły w kosmosie. Skażona kralotycznie, wygrana przez championa Pięknotki od kralotha.
* Mirela Orion: ściągnęła dyskretnie Damiana, znalazła przemytników oraz dowiedziała się kim jest naprawdę Aida. By ratować Aidę stoczyła walkę z Moktarem - i przegrała.
* Mirela Niecień: zajmuje się Aidą; powiedziała Pięknotce co zrobić - wygrać na arenie z championem kralotha lub porwać Aidę. 
* Adam Szarjan: powiedział Pięknotce, że Aida nie jest przecież jakąś elitką czy nikim ważnym. Uratowana z kapsuły w kosmosie.
* Moktar Gradon: champion Pięknotki do walki z kralotycznym championem przekupiony walką z Kirasjerką. Pokonał wszystko z czym walczył i się doskonale bawił.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Przelotyk Wschodni
                            1. Przejściak: miasteczko niedaleko Cieniaszczytu; poczekalnia dla tych, którzy nie mogą/nie chcą do Cieniaszczytu wejść
                                1. Hotel Pirat: OGROMNY kompleks hotelowy; tam znajdowała się Mirela, Damian (w ukryciu) i Pięknotka
                            1. Cieniaszczyt
                                1. Knajpka Szkarłatny Szept: standardowe już miejsce spotkań Pięknotki ze wszystkimi; tu z Julią
                                1. Kompleks Nukleon: w którym przetrzymywana była Aida celem regeneracji pokralotycznej

## Czas

* Opóźnienie: 1
* Dni: 3
