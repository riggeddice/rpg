## Metadane

* title: "Test z etyki"
* threads: rekiny-a-akademia
* gm: żółw
* players: kić, fox

## Kontynuacja
### Kampanijna

* [200311 - Wygrany kontrakt](200311-wygrany-kontrakt)

### Chronologiczna

* [200311 - Wygrany kontrakt](200311-wygrany-kontrakt)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* .

### Pytania

1. .

### Dominujące uczucia

* .

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

The ethics exam. This is one of the more stressful times for students. Fortunately, Teresa Mieralit knows how to make those things work. As a „Dare Shiver” member, she tends to put very difficult situations in front of the students and see how they solve it.

After all, they are supposed to learn some civic duty as the Magi of Letian Alliance.

## Punkt zero

.

## Misja właściwa

Our heroines are two magicians close to the end of the education at magical Academy. One of them (Berenika) is an expert on art materialization, she’s tired of school already and wants to have all of that finished. The other (Aniela) is a doctor who is able to fetch anything she needs. She is a shadow, hard to notice yet all seeing.

And Teresa asked the girls to come to her room at magical Academy. She told them they will not be able to partake in a test – there are problems in the school and some litigation might be in order. The girls fortunately took the bait and asked Teresa what is it about. They knew Teresa was planning something, but there was no sensible way out.

Teresa explained that Liliana was doing something dangerous, something which would require some ethical correcting. Yet she did not tell what was it about. Girls managed to pull out of Teresa what the teacher expected them to do – Teresa wanted the girls to make it so that no one gets harmed and the school’s reputation goes unsullied. Also, Teresa accepted that if all goes perfectly well, this shall be qualified as passing ethics exam.

In other words, super cool.

Aniela ran her intelligence network (people telling her things because she kept them in the best) two information what the hell was Liliana doing. She has heard a very over the top and underwhelming tale in the same time – „Zygmunt Zając”, the company making canned food has infiltrated the magical Academy. The food the students are eating has been corrupted and made from unnatural sources. In other words, lots of smoke, not much fire.

But Liliana has taken Myrczek, a local mushroom and fungus expert. She has caused him to help her do something to the canned food, to make some experiments. Also, Liliana has access to the laboratory with dangerous reagents and experiments. How come Liliana has access? Well, Teresa gave it to her. Such a surprise…

How to fight fire the most efficient way? Usually you would use a plasma bolt or some other natural calamity. Therefore Berenika decided to go meet Liliana’s cousin, Napoleon. That guy connected with Liliana is a calamity and a natural disaster.

Napoleon was training at an arena, surrounded by female fans. He wasn’t really interested in them, or rather, he tried not to show he was interested in them. Berenika used her materializing magic and she created a homunculus to lure the fans away from Napoleon. Wasn’t the easiest thing, but she succeeded - to the point some of those fans became rabid psychos. Well, some collateral damage.

Napoleon remained alone, a bit sad, but quite impressed with Berenika’s power. Berenika explained to Napoleon that Liliana took poor Myrczek and is doing unspeakable things with him, which might endanger his graduation. That pulled a string. Napoleon decided he has to help the students save Myrczek; Liliana is a danger to well, everything.

In the meantime, Aniela started to search for information how to piss Liliana off. There are things which irritate Liliana and those things can distract her from doing awkward things with canned food.

And Aniela found some of those things – Liliana is quite vain. She likes looking very well. And she absolutely hates having random pictures taken of her, especially when she doesn’t look perfect. So, that was enough for Aniela to mastermind a plan to create an imp - one which is supposed to snap a picture while Liliana is sleeping or not really in the best state, then put those pictures in random locations.

Well, for that to happen, to create such an imp, the girls need access to the same dangerous laboratory Liliana has. Teresa gladly gave them her approval. After all, it is their problem now.

Fortunately, or unfortunately, Berenika has managed to create an imp. The imp is much more powerful than Liliana. He will be able to evade the sorceress anytime. Unfortunately, it is way too powerful – it is potentially self regenerating and indestructible. No one can control it. It is not a danger, but it is a horrible nuisance. The only good thing is that it is bound to the school and school’s campus. It cannot leave this particular place. But Liliana will have a nightmarish time from now.

Just ethics exam.

But their main goal was not just to create an imp to annoy Liliana, but also to save Myrczek from the sorceress. So Berenika created a fungus – and unnatural, strange fungus to lure Myrczek in the night from his room to have a chat. And they asked Napoleon to come with them.

In the early night (earlier than night, but later than evening) the sorceresses positioned themselves in a trap with a fungus. Of course, they have managed to lure Myrczek away from his room – fungi do that. To slight consternation of both Napoleon and our heroines, some of Napoleon’s fans were also here, trying to remain unnoticed in the bushes.

When the fungi master expert magician arrived on the scene, he got grilled by everyone present. He told them everything, as Myrczek cracks really badly under stress. Liliana was making some experiments in the special wing of the Academy, the one which was not used. Some of the canned food formed a cocoon. Overall, she got quite paranoid about the „Zygmunt Zając” company.

Wow. So there is something about it. But to be honest, our heroines simply want to have nothing to do with it – they want to pass the exam and leave all of this behind.

So they went to meet Teresa. The teacher was quite sleepy, that she told the students that she could solve it, by calling proper authorities and asked the magicians what would a truly ethical mage do in this situation.

Neither Berenika nor Aniela are stupid. They knew this means they have to solve it. On their own.

Ah, Liliana is absolutely sure that this imp thing is created by the evil processed food company „Zygmunt Zając”, who is her personal nemesis. So the problem has not been solved but just displaced. She won’t bother Myrczek, though.

Teresa would never allow them to enter the forbidden wing of the Academy, but Napoleon knows how to enter that place, to great astonishment of all the students. „Bad boy Napoleon” is completely not something anyone would ever expect him to do. Well, he does know how to get there and he helps our heroines get to that wing.

And in the chamber where Liliana started performing her experiment something is there. Some kind of cocoon has bursted and there are some strange insects; they look very much like dangerous creatures on Trzęsawisko Zjawosztup. That implies Liliana was right after all! But her being right does not help our heroines pass the ethics exam.

Napoleon took the fire on himself to protect the girls. He got bitten several times, quite badly. But not a single insect has escaped, everything has been contained by the team.

And during the escape Napoleon was left behind, caught by a janitor.

Ethics.

## Streszczenie

Liliana eskalowała swoją krucjatę przeciw "Zygmuntowi Zającowi", włączając do działania Ignacego Myrczka. Teresa Mieralit, nauczycielka m.in. etyki, zrobiła z tego przypadku egzamin dla dwóch uczennic kończących już swoją naukę w Akademii Magii. Skończyło się na stworzeniu anomalnego impa robiącego zdjęć śpiącej Lilianie i jeszcze większym podgrzaniu atmosfery. Ale - konserwy ZZ faktycznie posiadają dziwne substraty.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Aniela Kark: AMZ; pozyskała sporo informacji na temat Liliany, jej sił i słabości. Dodatkowo jako lekarka ustabilizowała Napoleona pokąsanego przez dziwne rzeczy.
* Berenika Wrążowiec: AMZ; Zmaterializowała homunkulusa Napoleona, a potem impa który gnębi szkołę magów. Dość bezwzględna w swoich celach; byle mieć tą durną etykę za sobą.
* Teresa Mieralit: Nauczycielka etyki ORAZ agentka Dare Shiver. Wpierw dostarczyła Lilianie narzędzia do robienia problemów a potem poszczuła ją dwoma uczennicami. I nic nie musiała robić.
* Liliana Bankierz: AMZ; przekonana o tym że "Zygmunt Zając" ją prześladuje i wysyła na nią agentów. Zdekomponowała ich konserwy i wyhodowała z nich straszne rzeczy przy pomocy Myrczka. Imp od zjęć ją prześladuje.
* Napoleon Bankierz: AMZ; podrywa dziewczyny na arenie ćwicząc, ale jak przychodzi temat Liliany to przekierował się na nią by chronić Myrczka. Chronił Zespół przed insektami; solidnie pokąsany.
* Ignacy Myrczek: AMZ; zgnębiony przez Lilianę, próbuje pomóc jej testując konserwy "Zygmunta Zająca". Bardzo pasywna rola - ona każe, on się boi i jej słucha.


## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Akademia Magii, kampus
                                    1. Budynek Centralny
                                        1. Skrzydło Loris: "zakazane", nieużywane skrzydło AMZ. Liliana tu robiła eksperymenty hodując straszne rzeczy z konserw Zygmunta Zająca.
                                    1. Arena Treningowa

## Czas

* Opóźnienie: 6
* Dni: 2
