## Metadane

* title: "Na statku jest potwór. Uratuj mnie."
* threads: ciche-ostrza-orbitera
* motives: the-protector, the-devastator, the-savior, nic-sie-nie-zmienilem, body-horror, dark-technology, energia-ixion, handel-zlym-towarem, ixionska-synteza, nie-na-to-sie-pisalismy, pozornie-w-kajdanach, lost-my-way, zdrada-w-rodzinie, echo-dawnej-osoby
* gm: żółw
* players: magdawrr, igor

## Kontynuacja
### Kampanijna

* [240321 - Na statku jest potwór. Uratuj mnie.](240321-na-statku-jest-potwor-uratuj-mnie)

### Chronologiczna

* [240321 - Na statku jest potwór. Uratuj mnie.](240321-na-statku-jest-potwor-uratuj-mnie)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * Inspiracje
        * Slay the Princess or the world ends. "I never wanted to hurt you"
        * Nova "It's a trap! They want to kill you"!
        * Alien in the Darkness: Metroglia.
        * IRIA: Puttobayah i idea z Zeiramem.
    * Epica "Our Destiny"
        * "So many days we've spent together | Trying to get ahead with our dreams"
        * "I won't give up, we'll fight to win | To move along from where we'd been | I'll sing this song for you again | ... | We're so much stronger than before"
        * Allarea jest blisko, ma dowody. Nie pozwoli sobie na to że zginie teraz. Walczy z nieskończoną determinacją.
        * Kapitan jest w stanie zapewnić zdrowie i pieniądze dla planetoidy z której pochodzi. Nie zatrzyma się. Walczy z nieskończoną determinacją.
* Opowieść o (Theme and vision):
    * "Orbiter wysłał agentkę by znaleźć niebezpieczny i przemyt ludzi i anomalii Saitaera. Gdy agentka została wykryta, potraktowali ją jak zagrożenie. Ale ixion sprawił, że może działać."
    * "Kapitan Hofjan próbuje pomóc swojemu domowi i swojej rodzinie. Nieważne co jest potrzebne i jak daleko trzeba się posunąć. Oni potrzebują go i jego podejścia."
    * Clash of wills. Kapitan Hofjan i Allarea. Oboje jako immovable force.
        * Allarea, agentka Orbitera, wysłana by rozwiązać problem przemytu niebezpiecznych elementów kultu Saitaera.
        * poziom 1a: Allarea była świetną sojuszniczką Zespołu i lojalną przyjaciółką.
        * poziom 1b: Kapitan jest dobrym człowiekiem i zawsze zapewniał, by statek miał się jak najlepiej.
        * poziom 2a: to kapitan i 2 innych członków załogi załatwili Allareaę. SHE SUFFERED.
        * poziom 2b: Allarea stała się potworem
        * poziom 3a: są na jednostce przemytniczej, niebezpiecznej. Cargo tego statku stanowi zagrożenie
        * poziom 3b: są tu też ludzie tacy jak oni - dobrzy i niewinni. Ale Allarea tego już nie widzi
        * poziom 3c: Allarea sama w sobie traci siebie, nawet, jeśli tego nie zauważa
    * Lokalizacja: statek kosmiczny
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * the-protector: Kapitan Hofjan pragnie zapewnić zdrowie, dobro i bezpieczeństwo SWOIM LUDZIOM. Do tego potrzebuje pieniędzy i środków. Nienawidzi zdrady i dlatego zareagował tak ostro na to, że Allarea to agentka Orbitera.
    * the-devastator: spawn Allarei; istota ixiońska pragnąca wykorzystać pełnię biomasy do maksymalizacji potęgi Saitaera. Potrzebuje artefaktu - rdzenia Saitaera do pojawienia się. Nie chce zabijać przyjaciół; 5Og by uniknąć walki.
    * the-savior: sama Allarea. Nadal chce pomóc Orbiterowi. Nadal chce powstrzymać ixion. Nadal chce ratować przyjaciół. Nadal wierzy, że oni ją uratują.  
    * nic-sie-nie-zmienilem: Allarea/Ixion zamknięta w biovat. Nadal wierzy, że może coś z tym zrobić. Że mogą ją uratować. Ale jak ją zostawią, to ich zniszczy. Ixion wygrywa.
    * body-horror: Allarea/Ixion, Allarea/Devastator. Ogólnie, ixion pokazuje jak piękna Allarea staje się terrorformem i traci wszystko co ją definiowało.
    * dark-technology: przemycane ixiońskie leki i ixiońskie artefakty; one prowadzą do apokaliptycznych scen na pokładzie statku
    * energia-ixion: terrorformizacja Allarei, nieludzki umysł połączony z ludzkimi aspektami. Straszliwa amalgamacja.
    * handel-zlym-towarem: handel niewolnikami i super-wartościowymi i super-niebezpiecznymi ixiońskimi artefaktami - tworami kultu Saitaera
    * ixionska-synteza: ixion uratował życie Allarei i umożliwił jej działanie. Jednocześnie ixion zniszczył samą Allareę.
    * nie-na-to-sie-pisalismy: większość załogi statku nie ma pojęcia o tym co naprawdę przewożą. Nie zasłużyli na śmierć i los który ich może czekać.
    * pozornie-w-kajdanach: Allarea jest zamknięta w biovacie niewolniczym, ale kontroluje sytuację. A raczej jej ixiońska część kontroluje sytuację. Sama Allarea nic nie wie.
    * lost-my-way: to co Hofjan zrobił Allarei, to co chciał zrobić transportując ixiońskie artefakty... to nie jest to co by zrobił w normalnych okolicznościach. Ale chce ratować swój dom.
    * zdrada-w-rodzinie: Zdaniem Hofjana, Allarea go zdradziła. Zdaniem Allarei, Hofjan zdradził wszystkich biorąc to zlecenie.
    * echo-dawnej-osoby: Allarea nie uległa pełnej transformacji ixiońskiej; jeszcze tam jest. Jeszcze coś zostało. Allarea aktywnie prosi o to, by wykonali jej misję do końca.
* O co grają Gracze?
    * Sukces:
        * Dostarczyć cargo czy nie? Pomóc Kapitanowi Hofjanowi czy nie?
        * Zniszczyć Allareę czy nie?
        * Statek w ogóle przetrwa?
    * Porażka: 
        * strata postaci (amalgamacja LUB śmierć)
        * MG decyduje jak to się potoczy dalej
* O co gra MG?
    * Highlevel
        * Chcę, by musieli ciężko walczyć i móc podjąć decyzję co robią.
        * Chcę pokazać im koszmar ixionu i trudne decyzje
        * Potencjalnie, chcę zrobić Ixion Outbreak w miejscu docelowym. Jeśli Allarea padnie w fazie 1.
    * Co uzyskać fabularnie
        * Chcę, by poznali Hofjana i Allareę. Chcę by polubili postacie.
        * Chcę, by mieli problem z Allareą
        * Chcę, by dużo osób zginęło
* Default Future
    * nieokreślone z uwagi na Fazę 1.
    * mogą zginąć w Fazie 1, czyli 'za wcześnie'.
* Dilemma: Co zrobią gracze - zwłaszcza z ixiońskimi artefaktami - i jak do tego doprowadzą.
* Crisis source: 
    * HEART: "Dwie uparte osoby, każda z silną agendą"
    * VISIBLE: "Przebudzenie w niewłaściwym momencie"
        
### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

* Allarea jest agentką Orbitera. Egzotyczną Pięknością. Uroczą przyjaciółką, która jest miła dla wszystkich.
* Allarea szuka przemytu bardzo niebezpiecznych artefaktów Saitaera. Działa w okolicy Anomalia - Iorus.
* Allarea znalazła pośród transportowanych bioform i ludzi bardzo niebezpieczne artefakty Saitaerowskie
* Kapitan Hofjan ją przyłapał. Allarea chciała go przekonać, ale została zaatakowana, uderzona w tył głowy.
* Hofjan, Tiberius ją poważnie pobili i uszkodzili, Kasjopea ją wsadziła do systemu podtrzymywania życia.
* Allarea jest Skażona ixiońsko. She is inactive, but her Spawn shall reawake.
* Pięć dni później, Allarea Zaadaptowała. Spawn się przebudził (ręka Allarei). Poszedł polować na 
* Allarea wysyła wiadomość do śpiących przyjaciół. "Na statku jest potwór. Uratuj mnie."

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

* F0: "Pokazanie głównych problemów"
    * 3 miesiące temu
    * Allarea pomaga w uwolnieniu Kasjopei, silent alarm; jest w magazynie gdzie grają w karty, jest tam kontakt który wie o Kasjopei.
    * -> pokaż Allareę jako szybką i śliczną nożowniczkę, która nie boi się wejść do paszczy lwa.
    * -> pokaż Allareę jako zdolną do zaufania Zespołowi i kapitanowi, nie boi się zostać postrzelona (ma lekką regenerację, naprawi się)
    * -> pokaż pozytywne relacje Allarei i Tiberiusa.
    * cel: złapać kolesia który jest kontaktem. Hofjan go przekupi.
    * Kasjopea ma być sprzedana przez handlarzy niewolnikami. Nie da się tego łatwo rozwalić. Hofjan ją wykupi.
    * -> pokaż, że kapitan dba o załogę - zapłaci z własnej kieszeni by pomóc każdemu
* F1: "Przebudzenie"
    * "Na statku jest potwór. Uratuj mnie."
    * Manifestacja -> Allarea atakuje Kasjopeę w skrzydle medycznym; zabierze jej rękę, MOŻE da radę ją zabić.
    * Kapitan proponuje odciąć skrzydła statku by to nie mogło tu przyjść. Tiberius uważa, że trzeba na to zapolować.
        * Szukamy potwora zanim zrobi komuś krzywdę - wire amalgamate with slime, porusza się w różnych subsystemach?
    * Spawn próbuje znaleźć ixiońskie artefakty i użyć ich w połączeniu z biovatem
        * -> FORM DEVASTATOR
    * ataki mentalne, przypominające wydarzenia z kapitanem Hofjanem i z Allareą, toteż z komputerami
    * Devastator próbuje dostać się do AI Core i połączyć się ze statkiem
        * -> UWAGA IXIOŃSKI ARTEFAKT
* F2: "Decyzja"
    * Kapitan - wiemy co zrobił i czemu
    * Allarea - wiemy że jest ixiońskim terrorformem (BARDZO groźnym)
    * So what do you do?
* Overall
    * chains
        * energia i amunicja Zespołu: max 2 (po refuel)
        * pancerz Zespołu: max 2 (scutiery)
        * sprzęt Zespołu
        * status Załogi (1/4); hibernowanej i nazwanej
        * status Statku (1/4)
    * stakes
        * Co się stanie z Takerigo? Ixionform?
        * Co się stanie z nimi?
        * Co z Allareą i Hofjanem?
    * opponent
        * Spawn, potem Devastator
    * problem
        * Spawn jest odporny na większość ataków
        * Takerigo
            * ma kryjówki
            * kapitan blokuje rzeczy przed / informacje

## Sesja - analiza
### Fiszki

* Allarea Verimentalis: agentka Orbitera szukająca przemytu niebezpiecznych artefaktów i Egzotyczna Piękność, pattern: wczesna Maytag
* Markus Hofjan: kapitan, pattern: Henry Gloval, z nieodłączną fajką
* Tiberius Orgon: brutalny i lojalny oficer, pattern: The Mountain; mało mówi, lubi miażdżyć wrogów rękoma
* Kasjopea Tiris: medyczka i badaczka, dekadianka, kompetentna i sympatyczna. Militarystka. Pattern: Deanna Troy.
* Samun Tiris: dekadiański pierwszy oficer, świetny w walce i dystyngowany. Pattern: Razorclaw G1.
* Jonatan Worot: lekko sadystyczny arystokrata, żołnierz. Pattern: Vicious
* Miron Worot: zajmuje się lifesupport, studiuje zacięcie broń biologiczną; pattern: Passionate Student
* Edgar Valtin: oficer łączności; uwielbia gry, często grywa z Allareą; pattern: Riddler
* Liwia Worot: inżynier, kocha patrzeć w gwiazdy i nie chce spotykać ludzi; pattern: Emotionless Doll
* Calia Tillard: starsza już psychotroniczka statku i od ładowni i logistyki; pattern: Beachcomber G1 (-> łagodność i dokumentacja)

### Scena Zero - impl

![Baza na planetoidzie Parkazis](mats/2403/240321-01-colony-on-moon.png)

4 miesiące temu.

Podczas jednego z rutynowych odpoczynków, Kasjopea, medyczka załogi zniknęła. Do akcji została wysłana Allarea – Egzotyczna Piękność - która powiedziała, że odnajdzie przyjaciółkę. Zespół długo czekał w niepokoju, ale w końcu pojawił się sygnał od Allarei. Sygnał wskazywał na to, że Allarea nie może swobodnie rozmawiać. Udało się ją zlokalizować w jednym z magazynów Stacji Przeładunkowej Parkazis, gdzie jej zdecydowanie nie powinno być. Nikogo nie powinno być. Jest na transponderze.

Więc Allarea jest w jakimś magazynie, wysłała sygnał - zlokalizowała cel (osobę która wie o Kasjopei lub jej lokalizacji). 

Operacją wyciągnięcia Allarei dowodzi najbardziej wojskowo doświadczony członek załogi, Andrzej - dekadiański inżynier. Wspiera go drakolicka przemytniczka, Rei.

Przemytniczka Rei doskonale zna ten obszar. Kiedyś w ramach działań Takerigo trzeba było załadować potencjalnie niebezpieczny towar na pokład statku, więc zeszła do magazynów i dokładnie je sprawdziła. Tak więc wie, jak dostać się do tych magazynów i jak są chronione - nie mają co prawda specjalnych metod zabezpieczenia, ale są pod kontrolą kamer a sama stacja Parkazis jest dość paranoiczna z uwagi na swoje znaczenie i równe siły działające dookoła.

Zgodnie z triangulacją lokalizacji Allarei (połknęła swój transponder), ta powinna znajdować się w jednym z bardziej oddalonych magazynów; tam akurat Rei nie robiła nic szczególnego.

Inżynier Andrzej dobrze korzysta z wiedzy Rei; wie też, że lokalna TAI jest zwykle zajęta wszystkim innym i jest przeciążona. Wykorzystał więc sprzęt Takerigo by się sensownie połączyć z lokalnymi kamerami i podglądać to co widzą. Nie jest to najprostsze, ale jako że nie ma zamiaru w nic ingerować, TAI nie postawiła na to żadnych sensownych zabezpieczeń.

Tr Z +3:

* V: Oglądasz tak jak kamery. 
    * W środku tego magazynu kamery nie ma
    * Istnieje ścieżka niemonitorowana - da się przedostać do tego magazynu bo nikt nie będzie wiedział (ta ścieżka jest specjalna - to nietypowy magazyn)
    * (+2Vg) czy Przemytnik coś wie?
* V: Przemytnik wie. Słyszałaś plotki
    * Wiesz, że gdzieś tutaj jest handel niewolnikami. Gdzieś są rozmowy - niewolnicy, broń biologiczna itp. 
    * Stacja Parkazis WIE, ale udaje że nie wie.

Jest to dosyć interesujące - wszystko wskazuje na to, że ktoś specjalnie przygotował ścieżkę przyjścia do tego magazynu tak, by nikt nie widział kto wchodzi i kto wychodzi. Kamery pokazują nawet ślady w pyle - czyli ten magazyn jest miejscem tajnych spotkań. I Andrzej wie dokładnie jak to zrobić by mogli tam wyjść niezauważenie.

Zgodnie z wiedzą Rei, ma to sens. W okolicy działa potężna grupa przestępcza ‘Ypraznir’; jakkolwiek właściciele Parkazis nie są połączeni z żadną organizacją przestępczą, ale bezpieczniej jest pozwolić Ypraznir robić to na co mają ochotę. Tak jest bezpieczniej i Parkazis będzie dużo szczęśliwsza.

Gdy te informacje dotarły do kapitana Hofjana, ten zasugerował, że wbiją się tam w czwórkę. On, Tiberius, Rei i Andrzej. Problem polega na tym, że kapitan jest bezużyteczny w tego typu operacjach a Tiberius, jakkolwiek bardzo groźny, ma zbyt duże tendencje do zadawania bólu i działania impulsywnego. Rei i Andrzej przekonali kapitana by jednak oni byli jedynymi którzy wchodzą do akcji. Kapitan się zgodził, niechętnie, dając dostęp do dużej ilości zasobów finansowych - praktycznie większość tego na co stać Takerigo w chwili obecnej.

Jeśli odzyska bezpiecznie Kasjopeę i Allareę, będą wszyscy szczęśliwi. Pieniądze są mniej istotne. Liczy się dobro załogi. A trzeba kapitanowi oddać, że zawsze potrafi znaleźć dodatkowe źródło funduszy; Takerigo pod tym względem jest bardzo szczęśliwą jednostką, gdzie nawet noktianie współpracują z próżniowcami połączonymi kulturowo i historycznie z Astorią. Mimo wojny która trwa w innych miejscach.

Krok - czekamy - krok, czekamy. Zgodnie z patternem monitoringu. I dotarli do drzwi z panelem. Inżynier się włąmuje do magazynu. 

TrZ+3:

* Vr: możecie wejść.
* V: drzwi nie zaraportowały że są otwarte

Rei i Andrzej przeszli przez podwójne śluzy. 

Są niezauważeni, co lekko zdziwiło Andrzeja. Spodziewałby się, że niezależnie od tego jak silni przeciwnicy są na tym terenie, będą jednak bardziej ostrożni - acz darowanemu koniowi nie patrzy się w zęby. Przed nimi staje wielka hala, wypełniona biovatami, hibernatorami w których znajdują się ludzie, dziwnymi artefaktami i przedmiotami a nawet biofabryki - odpowiednio syntetyzowane istoty na podstawie DNA ludzi, służące do przechowywania broni biologicznej lub leków.

Zdecydowanie są na terenie Ypraznir.

Andrzej i Rei słyszą głosy z oddali. Głośne. Podkręcają dźwięk w swoich Scutierach i poznają głos Allarei. Oprócz tego słyszą kilka innych osób, co najmniej 2 jak nie 5. Allarea bardzo ostro negocjuje, podaje się za wysłanniczkę Syndykatu Aureliona i jest w tym całkiem wiarygodna. Druga strona jest zdecydowanie niepewna.

* X: Rozmowa zrobiła się ostrzejsza. Widać, że Allarea nie ma dużo czasu i powoli musi coś podać - Wam się czas kończy
* V: Allarea powiedziała coś głupiego, gdzieś się pomyliła, ALE DRUGA STRONA NIE ZAUWAŻYŁA. To dziwne.
* V: Przemytniczka WIDZI dlaczego Allarea tak naciska
    * Druga strona negocjuje niepewnie. Nie czują się tak silni.
    * Druga strona zupełnie nie zna tego terenu. A jeżeli to lokalna grupa przestępcza to powinni.

![Allarea, na mostku](mats/2403/240321-02-allarea.png)

Andrzej wysłał do Allarei sygnał świadczący o tym, że są na miejscu. Allarea elegancko zmieniła temat rozmowy, w formie ‘przecież nikt z waszej piątki nie chce mi nic zrobić w tej chwili; tak, wasza trójka w Scutierach jest w stanie mnie rozstrzelać wiązkami laserowymi, a wasza dwójka - nawet jak nieuzbrojona - ma przewagę liczebną, ale za mną stoi Syndykat ich nie chcecie wiedzieć co oni robią neuroobrożami ludziom’.

Jest to niesamowite jak dużo Allarea się musiała przygotować by móc udawać agentkę Syndykatu; detale trzymają się kupy. Niestety, Allarea nie jest świetna w przemycaniu rzeczy i to widać – Rei aż się skrzywiła słysząc jeden czy 2 błędy. Za to co Rei zdziwiło – przeciwnicy tego nie zauważyli.

Przedstawicieli organizacji Ypraznir - działający na tym terenie od dawna, znający lepiej teren niż sama Rei - nie zauważyli oczywistego błędu, który zrobiła Allarea. To nie jest możliwe. A Rei ma dobre wyczucie ludzi, oni traktują Allareę jakby była prawdziwym zagrożeniem.

Czyżby to nie byli prawdziwi agenci Ypraznir? Dokładnie tą wiadomość Rei wysłała do Allarei, która natychmiast ją wykorzystała.

* X: Allarea wyszła z pełnym uśmiechem. "Zrobiłam test i nie jesteście z Ypraznir". Oni na to - "Ty nie jesteś z Syndykatu".
* X: Allarea spróbowała się dowiedzieć gdzie jest Kasjopea. Niestety, nie tylko jej nie powiedzieli, ale ją osaczają.

Mimo, że przeciwników jest 3 i to w Scutierach, Allarea nie straciła zimnej krwi i głośno informuje co się dzieje. Robi tak dużo hałasu, że Andrzej i Rei skutecznie mogą zajść z 2 stron przeciwników. Celują w głowy swoimi działkami laserowymi i otwierają ogień z zaskoczenia.

Tp +3 +5Or (rany) +5Og (zniszczenia):

* V: Dwa scutiery dostały z broni laserowej. Jeden ma spalone czujniki. Ciężko uszkodzony. Drugi nie żyje.
* (koniec zaskoczenia +5Or) Inżynier strzela w ostatniego scutiera. Niestety, jeden ze scutierów zmiażdżył przedramię Allarei.
* Xz: Inżynier zablokował uderzenie bronią, jak był w stanie, na pancerz, system uszkodzony ale przetrwał cios. USZKODZONY scutier, trzyma się, jest hermetyczny, ale nie wytrzyma ciosu.
* X: Scutier Inżyniera dobrze na pancerz. Scutier dehermetyzowany, uszkodzony. Tracisz przytomność.
* Or: Allarea spróbowała COŚ zrobić by odwrócić uwagę. Zapłaciła za to. Nie jest zdolna do działania. Zdjęta z akcji. +2Vg.
* V: Udało się Przemytnikowi trafić w głowę. Scutier jest zniszczony. KIA.

Bitwa potoczyła się w dość spodziewany sposób - Andrzej i Rei skutecznie zniszczyli 2 scutiery wrogów (jednego oślepili, drugi zginął na miejscu). Niestety trzeci przeciwnik złapał Allareę za ramię, miażdżąc ją, i wykorzystał ją jako żywą tarczę. Andrzej znalazł się w fatalnej sytuacji - jeśli zrobi unik, własność Ypraznir zostanie uszkodzona lub zniszczona i Takerigo będzie mieć absolutnie przechlapane. Jeśli wystrzeli, może zranić Allareę. Tak więc Andrzej wykorzystał sytuację, przyjął uderzenia na scutier z nadzieją, że Rei będzie mieć lepszą pozycję i pomoże Allarei.

Scutier Andrzeja został uszkodzony a sam Andrzej stracił przytomność; scutier wytrzymał kilka strzałów, ale nawet dobrze wykorzystywany pancerz ma swoje granice.

Allarea, pokonując ból (cecha drakolitki, ale też coś co wskazuje siłę woli samej Allarei), zaczęła atakować hełm Scutiera swojego oprawcy. Atakujący drugą ręką mocno uderzył i odrzucił Allareę, łamiąc jej co najmniej kilka żeber i zdejmując ją z akcji. Ale to kupiło czas Rei, która była w stanie zestrzelić tego scutiera.

Rei złapała dwóch rozbiegających się unieszkodliwionych ludzi a oślepiony scutier został unieszkodliwiony. Stymulantami doprowadziło się Andrzeja do działania, acz był w kiepskim stanie.

Trzeba przesłuchać napastników. O co chodzi.

Tp Z +2:

* X: Jeden z tej trójki (nic mu się nie stało) jest powiązany; jest chłopakiem córki jednego z bossów. I boss "pozwolił" na to, by zrobił "coś przy okazji"
    * Delikwent proponuje: "nie było spotkania, dacie nam trochę kasy i się rozejdziemy, nie spotkaliśmy się".
    * Przemytniczka pyta o Kasjopeę: złapali Kasjopeę, jest na sprzedaż. Jest w tym magazynie. Skąd wiedzieli? Bo Kasjopea ma długi, i oni o tym wiedzieli, długi sprzedane za niewielką wartość.
* V: Nazywa się Robert. Robert chciałby być KIMŚ, chce osiągnąć. Chciałby coś przynieść i załatwić.
* V: Robert CHCE z Wami współpracować.
    * "widzę, że to nieporozumienie"
    * załatwił z mafii wsparcie.

Faktycznie okazało się, że Robert (pomniejszy gangster z Ypraznir i ‘zięć’ jednego z oficerów organizacji) działał na własną rękę. Dyskretnie był wspierany przez Oficera, ale miał nic nie spieprzyć. A spieprzył. Zespół skutecznie przekonał go do tego, że mogą współpracować - pomogą z pieniędzmi (30% tego, co kapitan myślał że to będzie kosztować) i zapewnili sobie trwałego sojusznika, który im pomoże w każdej sytuacji, w której Ypraznir może im pomóc.

Do tego udało się odzyskać Kasjopeę i wprowadzić Allareę do szpitala na Parkazis zanim stało jej się coś nieodwracalnego. Allarea, niestety, długo nie odzyska pełni władzy w uszkodzonej ręce; nie przy poziomie medycznym w tej okolicy. Ale po 1-2 miesiącach rehabilitacji będzie w dobrym stanie, jak nowa. Poza tą ręką. To potrwa rok. Mimo jej drakolickiej struktury.

Kapitan się bardzo ucieszył, że odzyskał Allareę i Kasjopeę. Miał jednak wątpliwości co do typu operacji - nie dało się zrobić tego inaczej? Naprawdę Allarea musiała tak ucierpieć? Było warto? Wyraźnie kapitan ma ogromne wyrzuty sumienia - on autoryzował tę operację. On pozwolił Allarei ryzykować. Ale to był jej pomysł.

Tak czy inaczej - duży sukces.

### Sesja Właściwa - impl

Wszechobecny sygnał alarmowy. Ciemność a potem światło. Andrzej i Rei wyczołgują się z hibernatorów, wymiotują płyny hibernacyjne i wracają im zmysły. Jest ciemno - nikt na nich nie czeka, powinni być dalej w podróży kosmicznej. Czeka tylko wiadomość, od Allarei, sprzed chwili. „Na pokładzie jest potwór. Uratuj mnie.”

Co tu się do cholery dzieje? Szybko się ubierają, Andrzej korzystając z okazji wysyła wiadomość do Mirona.

* Miron: Andrzej? Cz... czemu Ty jesteś aktywny?
* Andrzej: Chciałem Cię spytać?
* Miron: Czekaj, przyjdę tam. Tylko Ty?
* Rei: Nie, ja też
* Andrzej: Wskazania życiowe wszystkich na statku ok? Allarei też?
* Miron: (westchnął)... Allarea była chora. Coś jej się stało. Kasjopea się nią zajęła. Uśpili ją.

Zgodnie z tym co mówi Miron, Allarea była uśpiona 5 dni temu. Ale wiadomość dostali przed chwilą. To nie jest możliwe, coś się nie trzyma kupy. Andrzej zdecydował się sprawdzić, gdzie NA PEWNO znajduje się Allarea. Podobno w biovacie skrzydle medycznym. Andrzej szuka informacje czy ona tam jest.

Tr Z +4:

* Vr: Dane z kamer pokazują, że ona tam jest. ALE. To jest prezentowanie sygnału.
* X: Czy wiemy kto? Nie.
    * (FLASHBACK: kiedyś Allarea pomogła stworzyć odwrócenie uwagi - był zagrożony, statek, abordaż, Allarea ściąga uwagę i wrogowie w kosmos)
    * Rozkaz bezpośredni kapitana. Kapitan wydał ten rozkaz, osobiście. I na jego życzenie postawiona jest pułapka. I on wie.
* (+Z) X: Czy tu jest jakiś potwór? Coś jest nie tak? Jesteś w stanie to dostać, ale PÓŹNO. Wpierw - kapitan do Was dotrze.
* V: Zablokowane drzwi. Miron nie dojdzie, kapitan nie dojdzie. Ale jest dość czasu.

Allarea wielokrotnie pomogła załodze, Andrzej tego tak nie zostawi. Nie, jeśli wysłała wiadomość sos i jeśli się obudził w tak dziwny sposób. Kamera w medycznym monitorująca stan Allarei bezpośrednio wskazuje na to, że ona tam jest - ale ta kamera jest ustawiona by to pokazywać. Jej tam może nie być. Więcej, na pewno jej tam nie ma. A kto wydał rozkaz ukrycia tych danych? Sam kapitan.

Co tu się dzieje? Andrzej natychmiast uruchomił mechanizm maksymalnego skanowania wszystkich kamer i czujników na statku. Allarea mówiła, że gdzieś tu jest potwór. Czy miała rację? Niestety, wszystkie systemy kamer i czujników są - z rozkazu kapitana - ustawione by nie pokazywać prawdy. Andrzej musi się przebijać. By nikt mu nie przeszkadzał, natychmiast zablokował wszystkie drzwi.

Miron podszedł pod drzwi i zaczął pukać. Nie może wejść. Andrzej go ignoruje. Po chwili do Mirona dołączył kapitan, wraz z pierwszym oficerem. Andrzej szybko patrzy na kamerze - kapitan wygląda ‘normalnie’, nie wygląda jakoś dziwnie. Pierwszy oficer jest w scutierze, w konfiguracji bojowej, osłania kapitana - na wypadek, gdyby kapitan był w stanie wejść do środka i Andrzej zrobił coś głupiego.

Andrzej szybko przechwycił też wiadomość o tym, jak to Miron informuje Kasjopeę, że Andrzej obudził się psychotyczny i żeby ona coś z tym zrobiła. Kasjopea jest zaskoczona - ostatni raz Andrzej obudził się psychotyczny 3 lata temu. Moment - czemu Andrzej w ogóle się obudził?

Drugi wątek który Andrzej monitoruje to rozmowa kapitana z pierwszym oficerem. Oficer próbuje przekonać kapitana by ten uważał. Kapitan twierdzi, że to jakieś nieporozumienie i zaraz uda się przekonać Andrzeja do otwarcia drzwi. Nie trzeba robić niczego szczególnego.

JEST! Andrzej znalazł coś dziwnego na kamerach i czujnikach. W okolicach inżynierii były różnego rodzaju przedmioty elektroniczne, które potem zniknęły, mimo że nikogo tam nie było. Life Support pokazuje troszeczkę podniesiony poziom zużycia. I czujniki wskazują na to, że COŚ zbliża się w kierunku Kasjopei. Do systemu medycznego.

* Vz: Inżynier otworzył drzwi (drugie) i TAI ma nic nie mówi. Kapitan i Samun po jednej stronie.

Andrzej i Rei nie mają pojęcia co tu się dzieje, ale coś jest bardzo nie w porządku. Andrzej zostawił zablokowane drzwi po stronie, po której jest kapitan, ale otworzył drzwi z drugiej strony. Poprosił ładnie TAI, by ta niczego z tym nie zrobiła i nikogo nie informowała, co się w pełni udało. Po czym zabrali broń i wybiegli w kierunku na Kasjopeę.

Teraz - oni nie powinni mieć broni. Ale Andrzej zwykle chowa różne ‘przydatne rzeczy’ (broń) w różnych miejscach. Taka jego natura, paranoika dekadiańskiego. A Rei wiedząc, że będzie spała w tym samym czasie co Andrzej, też schowała mały pistolet elektrołukowy w okolicach skafandrów. Tak więc nie powinni być uzbrojeni w swoich skafandrach - nikt normalny by nie był – ale Andrzej ma wibroostrze i elektrołukowy pistolet a Rei ma elektrołukowy pistolet. I gnają, przechodząc przez śluzy (chowając informacje) w stronę Kasjopei.

Tr +3:

* X:
    * (FLASHBACK: Tiberius kiedyś zaginął. Wpakował się w kłopoty. Kapitan zaryzykował cargo zrobił ekspedycję i potem przycisnął statek, zwiększając koszty, ale Tiberiusa się wyciągnęło)
    * POTWÓR zdąży zrobić RUCH.
* X: Kapitan dowiaduje się gdzie idziecie. TAI nie ukryła. Ekipa będzie tam zmierzać, ale po Was.
* Vr: Zdążycie do Kasjopei.

Kasjopea w systemie medycznym przygotowywała różnego rodzaju rzeczy i nagle otwierają się drzwi i wpada ekipa – Andrzej, uzbrojony (i podobno obudził się psychotyczny) a za nim Rei. Gdy Andrzej zrobił krok w stronę Kasjopei (by ją szybko stąd wyciągnąć ZANIM coś tu wejdzie), ta zrobiła krok wstecz. I z systemów wentylacyjnych wystrzelił w jej stronę ‘szlam’, amalgamat techorganiczny, podejrzanie wyglądający jak bardzo długa dłoń. Amalgamat złapał Kasjopeę za dłoń i natychmiast wślizgnął jej się pod skórę, przechodząc od strony kości w kierunku na ciało. Kasjopea wrzasnęła.

Andrzej natychmiast użył wibroostrza i odciął amalgamat od źródła. To, co już znajduje się na Kasjopei zignorowało rozcięcie i pełznie dalej. Rei natychmiast zakłada opaskę uciskową. Amalgamat ją asymiluje i pełznie dalej.

Andrzej używa łuku elektrycznego i rani 'pasikonika' (ten fragment amalgamatu, który został na ziemi). TYMCZASOWO jest ogłuszony, ale działa dalej. Rei odcina rękę Kasjopei - ale amalgamat już wniknął w jej ciało.

Wszystkie byty integrują się w jedną całość. W oczach Kasjopei gaśnie życie, jest pożerana i rekonstruowana od środka.

Tr +2:

* X: Zawiązałaś - to przeciska się przez.
* X: Tazer trafił w pasikonika. Ten byt się rozpłynął - śluz + elementy mechaniczne
* (+1Vg) X: Odcięłaś rękę. Szybki cios, szybkie uderzenie.
    * ALE widzisz. To już w nią wniknęło.
* (+1Vg) Rei wrzuca byt do hibernatora. Szybko go zatrzasnęłaś.
    * Hibernator zaczyna rzucać kody błędów. Ten amalgamat zaczyna łączyć hibernator ze sobą. Co gorsza, to będzie GROŹNIEJSZE jak z niego wyjdzie.
* Vg: Andrzej zdejmuje limity i MAKSYMALNA ENERGIA (alert TAI "WARNING POTENCJALNY POŻAR"). 
    * Hibernator, wszystkie te rzeczy stanęły w płomieniach

W skrzydle medycznym zawsze znajduje się hibernator. Rei natychmiast łapie to, co zostało z Kasjopei i unikając dotknięcia amalgamatu wpycha ‘to’ do środka, dbając, by wszystkie elementy amalgamatu znalazły się w hibernatorze. Andrzej zamyka hibernator, przygotowując się do uruchomienia przyspieszonej procedury zamrażania.

Oboje patrzą z przerażeniem, jak amalgamat zaczyna asymilować hibernator. Lecą czerwone kody błędów. Andrzej zmienia plan - destabilizuje wszystkie mechanizmy ochronne, zdejmuje wszystkie limity i puszcza zdecydowanie za dużą moc po kablach mimo protestów TAI, pokazując ‘to co widać’ jako argument. Rei zgarnia tyle leków, ile jest w stanie i oboje wypadają poza pomieszczenie medyczne, zanim dochodzi do straszliwego pożaru, niszczącego całe pomieszczenie medyczne wraz z amalgamatem.

Na to wpada kapitan, pierwszy oficer i Liwia. Liwia bez słowa hermetyzuje skrzydło medyczne, odcina wszystkie przepływy i wysysa atmosferę by zatrzymać pożar. Pierwszy oficer celuje w Andrzeja i w Rei. Kapitan jest przerażony. Co tu się stało! 

Andrzej overriduje rozkaz kapitana (co nie jest trudne, bo dla TAI aktualna sytuacja jest kryzysowa i rozkazy o niepodawaniu prawdy zostaną zignorowane) i szybko puszcza nagranie strumieniem do pozostałych obecnych tu członków załogi. Pierwszy oficer natychmiast przestaje celować w Andrzeja i skupia się na monitoringu otoczenia oraz tego co zostało w medycznym. Liwia odsuwa się od wszystkich kratek wentylacyjnych. Kapitan siada na ziemi ukrywając twarz w dłoniach.

Andrzej konfrontuje się z kapitanem - czemu ten zablokował obraz z kamer i czujników? CZEMU on to zrobił?

Tp +3:

* Vr: 
    * Andrzej: Odkąd się obudziłem, dziwne blokady wzniesione przez kapitana. Ostatnia dotyczyła blokady nagrania. Pierwsza, którą widziałem dotyczyła tego, że obraz na miejsce gdzie powinna być Allarea jest podmieniony. Rozumiem, że kapitanowi nic o tym nie wiadomo
    * Kapitan: Nie... to... tak, zrobiłem blokady, zrobiłem... ale... (szok) (przyznał się). Wiem, ale co to było?! TAI, zdejmij blokady... 
    * Kapitan: Możemy porozmawiać sami...?

Kapitan wziął Rei i Andrzeja na bok, do jednej z kwater mieszkalnych.

Kapitan zaczął wyjaśniać, że Allarea miała bardzo ciężki atak psychotyczny. Zaatakowała ich (jego, Kasjopeę i Tiberiusa). Pokazał nawet siniaki na torsie jako dowód; Allarea potrafi mocno uderzać ;-). Niestety, podczas walki Allarea została dużo bardziej ranna niż się wydawało. Allarea jest pewien sposób maskotką tego statku. To sprawiło, że kapitan podjął decyzję by dla utrzymania morale zahibernować Allareę w innym biovacie i ukryć to przed wszystkimi. Powiedzieć, że ona po prostu była chora. A Kasjopea jej pomagała, nadzorując dane z systemu medycznego i czasem odwiedzając i sprawdzając jak to działa.

Oki, ale gdzie ten biovat?

Kapitan nie miał wielu możliwości. Na Takerigo jest bardzo dużo skrytek i potencjalnych pomieszczeń; ten statek jest troszeczkę ‘złożony’ z innych jednostek, nie ma dobrej struktury. Wiadomo o tym – Rei sama pomagała kapitanowi przygotować miejsca na przemyt. Ale to o czym ani Andrzej ani Rei nie wiedzieli - kapitan zainstalował w niektórych z tych ‘skrytek’ miejsce gdzie może znaleźć się biovat.

To znajduje się w Inżynierii. Dokładnie tam, gdzie po raz pierwszy Andrzej zlokalizował obecność potwora.

Świetnie.

Andrzej jest dekadianinem - jednym z tych którzy walczyli z ixiońskimi terrorformami z ramienia światów Noctis. To niepokojąco przypomina anomalię ixiońską. Z doświadczenia najlepiej walczyć z tego typu rzeczami niszcząc biomasę, ogólnie niszcząc materię. To cholerstwo się skutecznie adaptuje do wszystkiego.

Mimo ogólnych protestów kapitana (Andrzej nie ma zamiaru go teraz słuchać; nie w tej sytuacji), Andrzej z inżynierii pozyskuje coś co teoretycznie nie jest bronią, ale zadziała idealnie – pochodnia plazmowa. Coś, co normalnie służy do spawania elementów kadłuba. Coś, co zupełnie nie powinno być używane wewnątrz aktywnego statku kosmicznego. Coś, co nada się perfekcyjnie do walki z ixiońskim terrorformem. 

Rei w życiu nie może dostać czegoś takiego, bo zniszczy pół statku przez przypadek. Więc Andrzej składa dla niej broń chemiczną - są różne baterie i ‘rzeczy’ z niebezpiecznymi kwasami i substancjami. Póki nie wiadomo, gdzie znajduje się potencjalny terrorform, Andrzej składa dla Rei tego typu broń do kupy, jednocześnie osłaniając główne wejście do kryjówki przy Inżynierii.

Komunikat: "nie chciałam nikomu zrobić krzywdy... (wiadomość od Allarei)"

Komunikat świadczy o tym, że Allarea może żyć. Może jeszcze tam być. Rei jest skłonna pójść sprawdzić - jest cicha i umie być niezauważona. Andrzej uważa, że muszą wpierw mieć gotową broń. Poszli na kompromis – Rei wejdzie tam bardzo ostrożnie i zostawi sobie możliwość ucieczki a Andrzej będzie ją osłaniał z miejsca, w którym się znajduje. Oboje widzieli co stało się z Kasjopeą, więc nie musieli informować się, że jeśli Rei będzie Dotknięta przez Amalgamat, to Andrzej ją spali pochodnią plazmową zanim Rei stanie się śmiertelnym zagrożeniem.

Andrzej osłania Rei (wejście do kryjówki), która przekrada się przez bardzo trudny teren.

Tam często nie ma miejsca by nawet dobrze się odwrócić; Rei zauważa 2 biovaty w załamaniach konstrukcji. W obu są normalni ludzie; nic nie wskazuje na to by byli amalgamowani. Ale jednocześnie Rei słyszy bicie potężnego serca. Dziwnego serca.

Komunikat: "Na statku jest potwór. Uratuj mnie. Proszę". Od Allarei.

Rei wysyła wiadomość do Allarei, typu „jestem”. Z drugiej strony słychać dźwięk metalowych butów na pokładzie. Pojawiła się Allarea – a raczej to, co z niej zostało. Ixioński terrorform, gdzie z Allarei została jedynie twarz. Byt nie zachowuje się fizycznie tak jak Allarea; posiada grację jej ruchów, ale ma niesamowite wygięcia stawów. Rei zastyga, by nie zostać zauważoną. ‘Allarea’ patrzy na nią pustymi oczami, po czym powoli monitoruje cały teren.

![Allarea, terrorform](mats/2403/240321-03-allarea.png)

Tr Z +3:

* V: pozostała niewidoczna, niewykryta przez Allareę
* Vr: Kupiłaś czas rozmawiając z Allareą
* V: Prawda od Allarei

Z rozmowy między Rei i Allareą wynikało, że Allarea nie zorientowała się, że stała się terrorformem. Ona myśli, że dalej jest w biovacie i komunikują się przez komunikator (fakt, że to nie byłoby możliwe pokazuje, że Allarea już nie do końca jest ‘z nami’). 

* Allarea: "Wiedziałam, że jeśli was obudzę, to przyjdziecie po mnie. Zawsze przychodziliście po mnie. Przepraszam, że tak późno. Przepraszam, że nie zdążyłam wcześniej."

Allarea wyjaśniła, że jest agentką Orbitera. Została wysłana do zatrzymania przemytu bardzo niebezpiecznej broni i ludzi. Ale najważniejsza jest ta broń. To są artefakty ixiońskie, które są zdolne do zarażania ludzi. Znalazła, że kapitan przemycał artefakty ixiońskie oraz ludzi; niestety, została przyłapana, bo nie zauważyła pułapki.

Tiberius, kapitan i Kasjopea się skonfrontowali z Allareą w tym miejscu. Allarea próbowała ich przekonać, że artefakty ixiońskie są zbyt niebezpieczne. Że mogą zrobić coś strasznego. Kapitan powiedział, że mogą iść porozmawiać o tym w innym miejscu - i wtedy Allarea została zaatakowana.

Walczyła dzielnie przeciwko swoim przyjaciołom, ale Tiberius jest po prostu silniejszy od niej i wyłamał jej obie ręce. Kasjopea zatrzymała ich i powiedziała że należy ją uśpić w biovacie i potem się to rozwiąże.

* Allarea (nieświadoma tego czym jest): "Proszę, musisz mi uwierzyć. Ixiońskie artefakty są śmiertelnie niebezpieczne. Zmieniają ludzi w potwory. Zabierają wszystko na czym ci zależy, zmieniając cię w bezwzględną maszynę do zabijania. One nie mogą dotrzeć na planetoidę. Pomóż mi. Proszę."

Patrząc na dziwne kamienne serce przy ramieniu Allarei, Rei wydedukowała co się stało - podczas starcia z Tiberiusem i kapitanem Allarea musiała zostać zraniona jednym z ixiońskich artefaktów. Po wsadzeniu jej do biovatu, zaczęła się ixiońska synteza. To by pasowało - jej ciało wygląda jak połączenie biovatu oraz jej dawnej urody.

Allarea oddaliła się w kierunku miejsca, z którego wyszła. Rei zauważyła, że nie ma lewej ręki. Czyli amalgamat, który zabił Kasjopeę był pierwotnie ręką Allarei. To nie jest dobra wiadomość, bo to pokazuje jak niesamowicie niebezpieczna stała się dawna przyjaciółka. I nie wiadomo w jakim stopniu ona nawet kontroluje swoje ciało i jak długo dalej będzie sobą.

Rei ma plan. Rozmawia z Allareą tak długo jak jest w stanie, wspominając dawne czasy, obiecując jej, że zapewni, żeby żaden z artefaktów nigdy nie wydostał się na światło dzienne i odwraca jej uwagę. Tymczasem Andrzej i Liwia hermetyzują kryjówkę, by wykonać manewr ostateczny – wypalić to wszystko do białego metalu.

Allarea albo nie zwróciła na to uwagi, albo nie zależało jej. Plan się udał. Zginęła, rozmawiając z Rei. Nie walczyła.

* (+Vr) Vr: Spokojnie rozmawiacie, uspokajasz Allareę i mówisz, że to się uda.

Rei jest jedyną osobą, która wie co tu się stało i jak to wyglądało. Wyjaśniła kapitanowi, co naprawdę zrobił. Wyjaśniła, że to był ixioński terrorform. Że ta walka, którą Allarea stoczyła z Tiberiusem spowodowała jej zranienie i to zmieniło ją w terrorforma. Allarea do końca nie chciała robić krzywdy załodze - chciała zatrzymać te artefakty.

Z pomocą kapitana (który wie co, gdzie się znajduje), usunęli ixiońskie artefakty przez śluzę, w kosmos. Kapitan miał ogromne poczucie winy - podkochiwał się w Allarei (dlatego ta zdrada zabolała tak mocno) a przez częściową utratę kontroli nie tylko ją stracił, ale też straciłby cały statek, gdyby Allarea nie obudziła Rei i Andrzeja.

Jak tylko dotarli do najbliższego bezpiecznego miejsca, Rei oraz Andrzej opuścili Takerigo. Nie tylko oni. Nie chcą więcej służyć na statku, gdzie ich przyjaciółkę spotkał taki los. Za to, że próbowała wszystkich - łagodnie i konsekwentnie - chronić do końca.

Komentarz wspomagający:

* z perspektywy tego jak szybko jest w stanie podłączać się w dalszym ciągu do ai, przy jednoczesnym fakcie, że by otrzymać pomoc, musielibyśmy leciec przez kolejne 2 tygodnie po nią, stwierdziłam, że to zbyt ryzykowne, a sama Allarea z charakteru poświęcając się dla innych sama chciałaby, gdyby miała pełną świadomość, by ją unicestwić
* ostatecznie w pokojowych warunkach podczas postoju przeprowadziliśmy dyskusję z resztą załogi, ujawniając te fakty, by sami zdecydowali, co z tym zrobią, jednocześnie zawierając układ z kapitanem, że nie będzie już przemycać tak niebezpiecznych rzeczy tylko np. leki
no i moja postać odeszła, nie wiem jak Twoja xD z załogi
* zostawiliśmy zabezpieczenie przez podgląd ai do tego, co dzieje się na statku i kontrakt z jednym z ludzi, którzy zostali z kapitanem, ale byli z nami blisko, żeby na odległość móc weryfikować wywiązywanie się kapitana z obietnicy
* być może kapitan przez wyrzuty sumienia potęgowane uczuciem, jakim darzył Allareę, będzie sam dążył do zaprzestania tworzenia/przemytu broni biologicznych itd. wśród swoich kontaktów, let's find out xD

## Streszczenie

Allarea - kochana Egzotyczna Piękność i ukryta agentka Orbitera wielokrotnie ryzykująca życie dla załogi Takerigo, z którą to załogą się bardzo zżyła. Allarea - przekształcona w ixioński terrorform, z agendą pożarcia Takerigo z winy kapitana i części załogi. Andrzej i Rei, obudzeni przez wiadomość Allarei, stawili czoła jej Skażonej formie i wyszli cało, niszcząc Allareę i wypełniając jej misję do końca - ixiońskie niebezpieczne byty nie będą transportowane. Kapitan próbował wszystko zatuszować, ale Andrzej i Rei to ujawnili i opuścili jednostkę. 

## Progresja

* Andrzej Marzalis: opuścił Takerigo po tym, jak Allarea zginęła. Zachowanie kapitana nie jest honorowe. Ciężko być z kapitanem, który zataja faktyczne przeznaczenie załogi.
* Rei Hasiren: opuściła Takerigo po tym, jak Allarea zginęła. Allarea do końca była kochana, a tak jej podziękowano.
* Samun Tiris: opuścił Takerigo po tym, jak Allarea zginęła. Kasjopea była jego ukochaną, a do tego - jako dekadianin - nie uważa tego za właściwe.
* SC Takerigo: doszczętnie zniszczona seria skrytek przy Inżynierii (wypalone do białości) i medical (pożar). Ogólnie w dobrym stanie, choć stracił kilku członków załogi i jest dużo uboższy, z gorszym morale.

## Zasługi

* Andrzej Marzalis: inżynier + advancer + combatant; przejął kontrolę nad kamerami wpierw by wyciągnąć Allareę a potem na Takerigo. Świetny strzelec i dobrze podchodzi do walki. Zawsze gdy hibernuje chowa gdzieś broń; dzięki temu skonfrontował się z terrorformem który pożarł Kasjopeę uzbrojony i doprowadził do pożaru używając systemu hibernacji. Potem - zdobył broń i zniszczył terrorforma, którym stała się Allarea.
* Rei Hasiren: przemytnik + dyplomata + combatant; skutecznie wprowadziła Zespół do magazynów i dogadała się z Robertem z Ypraznir. Weszła do 'pieczary lwa' (tam gdzie jest terrorform) by uratować Allareę jak się da, wiedząc, jakie jest ryzyko. Rozmawiała z Allareą-terrorformem, by odwrócić jej uwagę i by nie umierała samotnie. Kontynuowała jej misję usunięcia ixionu z Takerigo.
* Allarea Verimentalis: zawsze pomocna załodze, odważna i waleczna; zatrzymała scutier przed zranieniem Rei, choć prawie zginęła. Wykryła przemyt ixiońskich artefaktów na Takerigo i została przyłapana. Pobita przez Tiberiusa, połamana i zraniona przez ixioński artefakt, zmieniła się w terrorforma. Obudziła Andrzeja i Rei (bo oni zawsze przyjdą po nią) i dzięki temu uratowała - ostatnim ruchem - Takerigo. KIA, od pochodni plazmowej Andrzeja. Nie walczyła.
* Markus Hofjan: gdy członek jego załogi wpadł w kłopoty, zawsze próbował mu pomóc nieważne co się nie działo. Jednak gdy Allarea, w której się podkochiwał, wykryła przemyt ixiońskich artefaktów i odkrył że jest agentką Orbitera, zamknął ją w biovacie (po pobiciu). Niestety, Allarea stała się terrorformem a Markus nie będzie już spał dobrze w nocy.
* Kasjopea Tiris: wpadła w kłopoty i została złapana by sprzedać; wtedy Allarea ją wyciągnęła z Andrzejem i Rei. Próbowała ochronić Allareę gdy 'zdradziła' przed brutalnością Tiberiusa; zginęła amalgamowana przez ixiońskiego terrorforma (spawn Allarei). KIA.
* Samun Tiris: próbował przekonać kapitana, by ten nie narażał się na problemy z 'psychotycznie obudzonym Andrzejem'. Solidne dekadiańskie podejście, w scutierze i gotowy na walkę. Pierwszy oficer Takerigo.
* Miron Worot: przyjaciel Andrzeja, powiedział mu że wszystko powinno być OK i nie powinien być obudzony. Jak Andrzej wyłączył systemy, poprosił Kasjopeę, by ta zrobiła środki anty-psychotyczne.
* Liwia Worot: jej szybkie reakcje pomogły w kontrolowaniu szkód spowodowanych przez pożar w skrzydle medycznym; nadal z nikim nie chce rozmawiać i unika wszystkich.
* Robert Farkatak: młody agent Ypraznir, porwał Kasjopeę by ją sprzedać za plecami Ypraznir. To sprawiło że poznał Allareę a potem skroili go Andrzej i Rei. Zawarł sojusz z Takerigo, wyszedł na tym lepiej niż mógł (acz stracił 3 ludzi).
* SC Takerigo: bardzo specyficzna struktura z dużą ilością szczelin; podczas przemytu ixiońskich artefaktów Allarea została zraniona i zmieniła się w terrorforma. W walce z terrroformem uległ serii uszkodzeń.

## Frakcje

* Ypraznir: znacząca grupa przestępcza działająca w okolicy Iorusa; mają magazyn na planetoidzie Parkazis gdzie mają przemyt rzeczy niebezpiecznych, ixiońskich i handel ludźmi. Ich agenci mają więcej swobody niż to bezpieczne.

## Fakty Lokalizacji

* Planetoida Parkazis: pełni funkcję ważnego węzła handlowego i logistycznego w okolicach Iorusa; w lepszym stanie niż większość, ma też dobrej klasy skrzydło medyczne na stacji Parkazis (i przeciążoną TAI; działa tam Hestia).
* Planetoida Parkazis: arena dla różnorodnych działań przestępczych, w tym handlu niewolnikami i niebezpiecznymi artefaktami; nie chcą współpracować z Ypraznir, ale nie mają szczególnego wyjścia

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Transfer Iorus Anomalia
            1. Iorus, Pierścień
                1. Planetoida Parkazis
                    1. Stacja Parkazis
                        1. Sektor przeładunkowy
                            1. Stacja przeładunkowa
                            1. Magazyny

## Czas

* Chronologia: Zmiażdżenie Inwazji Noctis
* Opóźnienie: 19
* Dni: 1

## Inne
### Blueprint Takerigo

1. Część przednia
    1. Mostek, Dowodzenie
        * Systemy nawigacyjne, sensory i komunikacyjne.
        * Command&Control – centralne miejsce kontroli zaawansowanych technologii statku.
    2. AI Core
        * Miejsce, w którym mieści się główna sztuczna inteligencja statku, zarządzająca i monitorująca wszystkie systemy.
    3. Pokład Życiowy
        * System Podtrzymywania Życia – utrzymujący odpowiednie warunki do życia na pokładzie.
        * Kwatery Załogi – miejsca do spania, relaksu i rekreacji dla załogi.
        * Medical - leczenie itp.
    4. Reaktor (zapasowy)
2. Część środkowa
    1. Pokład Zaopatrzenia
        * Magazyny Zaopatrzenia – przechowujące zapasy, części zamienne i inne niezbędne materiały.
        * Dropping Bay – sekcja do przesyłania zaopatrzenia na inne statki lub na powierzchnię planet.
    2. Pokład Hibernacyjny 1
        * 2 Węzły po 60 osób
    3. Pokład Hibernacyjny 2
        * 2 Węzły na zapasowe bioformy
    4. Pokład Ratunkowy
        * Medical (awaryjny)
        * System Podtrzymywania Życia, zapasowy
3. Część tylna
    1. Pokład Inżynieryjny
        * Advanced Engineering Suite – miejsce, w którym przeprowadzane są zaawansowane prace inżynieryjne i naprawy.
        * Systemy Kontroli Energetycznej – miejsce kontrolowania i dystrybucji energii do wszystkich systemów statku.
    2. Silniki
    3. Reaktory
    4. Doki Serwisowe
        * Dla przeprowadzania zewnętrznych napraw i utrzymania.
