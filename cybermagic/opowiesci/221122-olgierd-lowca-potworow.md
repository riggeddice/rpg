## Metadane

* title: "Olgierd, łowca potworów"
* threads: historia-olgierda
* gm: kić
* players: żółw

## Kontynuacja
### Kampanijna

* [221122 - Olgierd, łowca potworów](221122-olgierd-lowca-potworow)

### Chronologiczna

* [220921 - Kapitan Verlen i Królowa Kosmicznej Chwały](220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)

## Plan sesji
### Theme & Vision

* ?

### Co się wydarzyło KIEDYŚ

.

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

.

### Co się stanie (what will happen)
?

### Sukces graczy (when you win)

* .

## Sesja właściwa

### Wydarzenia przedsesjowe

.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Kartaliana - dość spora stacja kosmiczna (klasy RAMA), nie ma frakcji która ją wyraźnie kontroluje. Są oczywiście siły porządkowe, ale nie są dominujące. Jest balans tak jakby a nie silna dominacja. Crime pays, unless you go very extreme.

Olgierd jest chwilowo drifterem. Poluje na niebezpiecznych noktian; ma 23 lata. I na oportunistów próbujących skrzywdzić Orbiter. Już teraz jego umiejętności walki są bardzo wysokie, nawet nie używając magii.

Na stacji jest od niedawna - słyszał plotki o serpentisie, który przeżył, próbuje dojść do tego czy to prawda i czy ów serpentis stanowi zagrożenie.

Olgierd poszedł do knajpy, wypił troszkę alkoholu, pograł w bilard. Potem wraca. Nie jest to mocno oświetlona stacja, Olgierd wybiera takie ciekawsze obszary - nie bo chce się ukrywać czy coś; po prostu chce przygody i zobaczyć w jakim stanie jest stacja. Idzie sobie takim korytarzem i z bocznego korytarza gniewny / przerażony krzyk kobiety. Olgierd idzie w tamtą stronę. Gdy skręca za róg - dwóch kolesi zainteresowanych dziewczyną, szamoczą się z nią.

Olgierd idzie szybko w ich stronę. "Koledzy, zastanowiłbym się nad waszym następnym ruchem." Jeden się szamocze, drugi się odwraca. "Popatrz popatrz, bohater się znalazł. Wynocha mi stąd.". Olgierd się uśmiecha "Może i jestem Twoim bohaterem, ale tym bardziej powinieneś mnie posłuchać." Ręka przeciwnika wędruje do pasa.

Olgierd rzuca weń manierką, skraca dystans i wali nim o ścianę.

Tr +4 +3Or:

* V: skrócony dystans, uderzony o ścianę. 
* Vr: przeciwnik szoknięty.
* Vr: przeciwnik rozbrojony (tymczasowo). Miał tazer..?

Olgierd leniwie tazerem, w tego drugiego celuje, "porozmawiamy?". Ten drugi "TY SKUR...!!!" i tak jak się szarpie z laską tak rzuca w Olgierda czymś z alejki i próbuje skrócić dystans.

* X: laska skończy temat wolna. Olgierd jej nie zatrzyma / nie zablokuje. Coś co oni robili dało Olgierdowi wytyczne, że ona musi "być wolna". Olgierd ODEBRAŁ WRAŻENIE że próbowali ją zgwałcić.
* V: koleś jest złapany i zaprzyjaźnił się ze ścianą. Oboje są wstrząśnięci (shaken).

Olgierd pozwala im leżeć. Odpływają. Skupia się na damie. Ona cała przestrzaszona, na ziemi. Drży. Olgierd podaje jej rękę z galanterią, ale trzyma na zasięg. Ona "dziękuje". Olgierd "w porządku?" Ona "teraz już tak". Olgierd omiata ją wzrokiem. Roughness, na oko scavenger / najemniczka. Olgierd pyta "o co chodziło?" Ona "chcieli mi wszystko zabrać". Ona "chodźmy stąd", chce iść. Olgierd odprowadza ją do przestrzeni bardziej publicznej, ale wpierw pytanie "co robiłaś w tych zaułkach?". Ona "wracałam do przyjaciela".

* Olgierd: "odprowadzić Cię?"
* ona: "wystarczy (niezbyt publiczny)"
* Olgierd: "Olgierd jestem. Jak do Ciebie mówić?"
* Alicja: "Alicja."
* Olgierd: "Daleki strzał - słyszałaś plotki o potencjalnym serpentisie w okolicy? Albo innych niebezpiecznych noktianach?"

Tr (niepełny) Z (bo pomógł) +3:

* X: musi przejść przez podły teren. ONA ma tam przejście, ale ON nie.
* X: blokada - Olgierd nie może nic zrobić z tym co mu teraz powie
* X: Olgierd nie ma jej jako aliantki - stracił status że pomógł. Ona widzi nadmiar idealizmu i niepolitycznego myślenia. On po prostu... to nie to.
    * Alicja: "Lutus nie jest żadnym serpentisem."
    * Olgierd: "może nie jest - ale czy jest niebezpiecznym noktianinem? Wojna się skończyła."
    * Alicja: (błysk w oczach, żarliwość). "Lutus nie jest bardziej niebezpieczny niż Ty."
    * Olgierd: "jestem niebezpieczny. Ale ja jestem w prawie. Jeśli Lutus nie robi krzywdy obywatelom tego sektora, I don't care. Jedna krzywda - I'm on his ass."
    * Alicji zależy na Lutusie, bardzo, jest skłonna go bronić. Zrywa się i ucieka.
    * Olgierd za nią woła: "Jeśli chcesz mu pomóc, współpracujcie ze mną. Albo Lutus i Ty dołączycie do mnie, albo skończy się takedownem. Jeden człowiek kontra Orbiter." Nie próbuje jej zatrzymać.

Olgierd wraca do swojego terenu. Pojawia się kilka osób, pięciu. "Można wkopać Orbiterowi". Wychodzi dwóch przed Olgierda i się mu pokazują. Olgierd się rozgląda, czterech widzi. Na razie namierzają i oceniają reakcję.

* Olgierd: "W czym mogę Wam pomóc? Zbieracie grzyby?"
* Jeden: (Paskudnie się uśmiecha) "Zbieramy takie purchawki jak Ty, żeby nam korytarze nie zarastały."
* Olgierd: "Tak bez żadnych pytań? Nic Was nie interesuje? Nie chcecie kasy?" (zdziwiony)
* Jeden: "To Ty masz problem, kasę sobie zabierzemy" (gdy odpowiada, Olgierd atakuje)

Olgierd szarżuje z berserkerskim rykiem w kierunku jednego. Adrenalinowy spike. Cel - skrzywdzić. 1v4 to nie kwestia zabawy, zwłaszcza że Olgierd ma tylko broń improwizowaną. Cel - błyskawicznie jednego unieszkodliwić i sprawić, by oni myśleli, że jeśli nie grają w grę Olgierda to on zabije. INTIMIDATION.

Tr Z (bioforma) +2 (bo trudno) + 3Or:

* Vr: delikwent dopadnięty, w szoku, nie jest wyłączony ale cios go oszałamia. Cios w splot - wymioty.
* V: NIEOBLICZALNY Olgierd. Ten sponiewierany jest grzeczny. Reszta niepewna - co dalej. Zbliżają się.

Olgierd: "jeden krok i jest Was o jednego mniej". Zawahali się, chwila niepewności. Trzymając gościa Olgierd robi krok w ich kierunku. Pozycjonują się dobrze. Nie cofają się. Olgierd łamie rękę przeciwnikowi i czeka na skowyt. Po czym Olgierd bierze baseballa tego delikwenta i ATAKUJE! Rzuca baseballem w drugiego i próbuje schwytać kolejnego przeciwnika.

* (+1Vg +2Or): V: baseball nie trafił w sojusznika, ale dwóch udało się rozdzielić. I KOLEJNY nieszczęśnik jest w łapach Olgierda. Wisi za gardło. Próbuje z pięści w Olgierda, ale solidna basia go zniechęciła. Olgierd pozycjonuje go jako zakładnika i zwraca się ku trójce.

Olgierd: "Rozmawiamy, czy wybieracie kończynę?"

DARMOWY SUKCES - uciekają.

Olgierd, manierka z wodą. Wykorzystuje ubranie (nie swoje) i trochę wodą na twarz by nieco pomóc. "Porozmawiamy, czy wybierasz kończynę?". Koleś zaczął płakać. On już nie chce. Olgierd, konwersacyjnie "ja bym porozmawiał"

* O: "Olgierd. Z kim mam przyjemność?"
* M: "Maciek."
* O: "Maciek, czemu mnie zaatakowaliście?"
* M: "Bo Orbiter nie ma tu czego szukać."
* O: "Nikt Wam nie... chcieliście się... w piątkę na komandosa? Bez EMP? Nie neutralizując biotoksyn bojowych? Nierozsądnie."
* M: (koleś odpełza)
* O: "Zgodzisz się, że to w sumie Wasza wina?" (konwersacyjnie)
* M: Tak, tak, oczywiście.
* O: "Ostatnie pytanie - co wiecie o Lutusie? Taki z Noctis? Słuchaj - ja do Was nic nie mam. Zero urazy. Wzięliście kastety na strzelaninę. Spoko. Zdarza się. A Lutus?"
* M: (defiance, nie jest kapusiem)
* O: "Powiem tak - poluję na potwory. Jeśli Lutus jest potworem, jest mój. Poluje na astorian. Jeśli nie jest potworem, nie zrobię mu krzywdy, ale porozmawiam. Mam gdzieś noktian, poluję na potwory."
* M: mówi
    * Potwierdza istnienie Lutusa. Jest to ktoś w półświatku. Zmienia laski jak rękawiczki, więc Alicja może tam pasować.
    * Nie ma mrocznych plotek na jego temat. Nie wchodzi się mu w drogę jak nie trzeba, ale nie po trupach do celu.
    * Nic nie może powiedzieć o Alicji.
    * Uśmiał się - jeśli Olgierd sklepał dwóch z tazerami, najpewniej policję.
* O: "To niefortunne, że Twój kolega... jak się zwie? (Marcin) upadł i złamał rękę a Ty ratowałeś go i uderzyłeś się w twarz. Na szczęście przechodziłem. A mam dobre serce. Chodź, bierzemy Marcina i idziemy do medycznego."
* M: "Czemu?"
* O: "Dobra próba. Plus, byliście w niewłaściwym momencie w niewłaściwym czasie. Dawno nie miałem treningu. Ale serio - nie zaczepiajcie Orbitera. Nie wszyscy mają tak łagodne serca jak ja."

Autentycznie Olgierd ich weźmie i pokryje koszty medyczne. Bo czemu nie, i tak nie ma co robić z kasą.

* O: "Maciek, mam prośbę. Możesz powiedzieć 'nie'. Czy jest jakiś dzieciak któremu można zapłacić by sprawdził czy policja nie siedzi mi na chacie?"
* M: (banan). 

Olgierd po zastanowieniu poszedł bezpośrednio na komisariat policji. Nie ukryje się i tak, nie ma po co, a oni nie aresztują Orbiterowca. Nie, jak dojdą do nich plotki. Bo nie wiadomo co zrobi. Trafił na policjanta, który ogarnia - ale Olgierd udaje, że nie wie o co chodzi. Po prostu policjanci są u niego w domu, więc sam przyszedł sprawdzić w czym może pomóc.

Olgierd posiedział w sali, w samotności chwilę. Potem zaproszony do następnego pomieszczenia, featureless jak poprzednie. Znowu czeka. Olgierd patrzy jak się stąd wydostać, słabsze nity itp. Olgierd został upgradowany do trudniejszego pokoju żeby z niego uciec, co doceniło jego ego. Do pomieszczenia starszy koleś w dobrej formie, koło 40-tki. Zero broni. 

* A: "Pan Drongon, jak rozumiem."
* O: "W rzeczy samej, panie oficerze. Z kim mam przyjemność?"
* A: "Detektyw Altair."
* O: "W czym mogę pomóc?"
* A: "Może pan zacząć od podania mi powodu dlaczego nie powinienem pana aresztować za pobicie dwóch funkcjonariuszy i utrudniania czynności służbowych"

Olgierd powiedział szczerze - poluje na potwory. Najchętniej z Orbitera lub Noctis, ale każdego ludzkiego potwora z przyjemnością dorwie. I dlatego jest tu, szukał serpentisa a znalazł Lutusa. Jeśli to tylko przestępca, Olgierd nic do niego nie ma. Jeśli to potwór - z przyjemnością. 

* A: "Ci oficerowie których pan pobił próbowali przejąć albo środki albo już zdobyte pieniądze za sprzedaż podłych narkotyków, uzależniają jak cholera."
* O: "Zepsułem, więc naprawię. Mam po drodze."
* A: "Niech i tak będzie."
* O: "Kim jest ta dama? I kim jest ten Lutus?"
* A: "Średnia lokalna ryba. Ostatnio trochę wypłynął, idzie dynamicznie do góry. A kobieta? Jakaś nowa. Zaczniemy od nowa procedury przygotowania..."
* O: "Niekoniecznie. Co o nich wiecie? Gdzie są? Jak to wygląda? Jestem szturmowcem i spoza stacji. Zrobię swoje, wywalicie mnie, życie potoczy się dalej."
* A: "Lutus to tylko średnia ryba. Grube ryby są niedostępne. On nigdy nie dotknął prawdziwego syfu, ale się orientuje. Musi, skoro coraz wyżej idzie"
* O: "Czyli on jest środkiem. I nie ma TWARDYCH dowodów, że on ma coś z tym wspólnego? Są mocne poszlaki?"
* A: "Gdybyśmy mieli twarde dowody..."
* O: "To byście je podłożyli. Lub nie macie siły ognia i nie chcecie otwartej konfrontacji."
* A: "Może to standardowa procedura operacji na Orbiterze, my utrzymujemy wszystko w dobrym stanie i nie dążyć po trupach do celu."
* O: "To nie jest procedura Orbitera. Ale gdy walczysz z potworami, używasz ich technik. Inaczej zbyt wiele niewinnych umiera."
* O: "Rozumiem, że tu chcemy działać bardziej zgodnie z zasadami."
* A: "Na tej stacji obowiązuje prawo. Nie jesteśmy opresorami i nimi nie będziemy."
* O: "Rozumiem, dostosuję się. Ale w takim razie po co Wam jestem? By pójść i pogadać?"
* A: "Trzeba mu pokazać, że zawsze jest ktoś groźniejszy i trzeba działać zgodnie z regułami."
* O: "Rozumiem, popsułem, naprawię. I tak chcę porozmawiać z Lutusem. Poproszę od razu by z Panem porozmawiał. Gdzie go znajdę?"
* A: "Nie mamy oczu 24/7, ale zwykle w okolicach Ciemnicy"
* O: "Byłem tam dziś."
* A: "Nietypowe podejście do rozwiązania konfliktów, tudzież ich kończenia"
* O: "Nikt nie zginął i nikomu nic nie groziło. Musiałem walczyć na serio, bo nie pokonam pięciu samemu."
* O: "Jakie są ZWYCZAJE na stacji? Raczej ludzie giną, ranni? Czy jest raczej łagodnie?"
* A: "Pański wybryk będzie zapisany w historii."

Olgierd idzie do sklepu z bibelotami. Szuka pamiątek. Coś ciężkiego i dobrego jako broń miotana. Ale co kupi turysta i przepłaci. Biorę kilka. Nie jest to dobra broń, ale nie wygląda.

Olgierd idzie do Ciemnicy. Chce znaleźć Lutusa i z nim porozmawiać. Dość spora przestrzeń do przeszukania, są też knajpy i stragany. Idę szukać pamiątek. Serio szuka pamiątek które jednocześnie mogą być CIEKAWE albo SŁUŻYĆ JAKO BROŃ. No i - jak skontaktować się z Lutusem? Jest mu w końcu winny jedną rozmowę. De facto, Olgierd jest wabikiem na siebie samego.

Olgierd poczuł nóż na plecach. Ktoś go zaszedł i zaskoczył. Olgierd kontynuuje kupowanie pamiątek - gdyby ktoś chciał pchnąć, pchnąłby. "Chcesz cokolwiek - idziemy." Olgierd się zgadza, że plakietka poczeka. Chce się spotkać z Lutusem.

Nożownik wyprowadza Olgierda w mniej znany teren. I nie odpowiada Olgierdowi na pytanie o imię. Olgierd się zgubił. Są tu inne rzeczy - zaułki, też ślepe. Czyli Olgierd ma okazję na miejsce, gdzie może rzucić zaklęcia. Olgierd spytał nożownika "po co Ci ten nóż, i tak idę a nie goniłem Alicji." Olgierd decyduje się zaufać Alicji - że "Lutus nie jest potworem". I tłumaczy nożownikowi co robił w przeszłości. Jakie osoby złapał. Skupia się na potworach. Pokazuje, że WIE że na tej stacji są noktianie i on ma to gdzieś. Jego celem jest potwór, nie człowiek. I on próbuje przekonać, że są po tej samej stronie.

Ex +3:

* Vr: thug "nie ma co z Olgierdem walczyć" więc z tym nożem tak bez serca. On WIE, że Olgierd nie uderzy jeśli nie jest zagrożony. Wycofał nóż.
    * "thug Borys."
* (+Z, integrity): Jak odprowadzają (po rozmowie z Lutusem), Borys jest bardziej 'hovery' type, jest blisko. Jest delikatny SYGNAŁ że on jest agentem Drugiej Strony. I że Borysowi nie do końca podoba się to co się dzieje na planecie.

W końcu Olgierd spotkał się z Lutusem. Na dzień dobry - Lutus jest noktianinem. Ocenia reakcję Olgierda - Olgierd AUTENTYCZNIE ma to gdzieś i to widać. Borys wprowadza Olgierda.

* L: "Szukałeś mnie. Znalazłeś mnie. Czego chcesz?"
* O: (ta sama śpiewka o potworach)
* O: "Jeśli jesteś potworem, informuję Cię o wrogości i wracam z nożem. Jeśli jesteś na DRODZE do potwora, chcę Ci pomóc znaleźć inną ścieżkę. Else, chcę byś mi podpowiedział czy znasz jakieś potwory i jak nie potrzebujesz pomocy, idę sobie" /szczerość
* L: (mierzy wzrokiem) "everybody out." (ma sporą obstawę, z pomieszczenia)
* O: (kiwa mądrze głową)
* L: "Na tej stacji nie ma potwórów"
* O: "Powiedziałbym, że jestem rozczarowany, ale bym skłamał"
* L: "Ale możesz mi pomóc"
* O: (słucha)
* L: "Chcę odnaleźć mojego... brata. Myslisz że czemu prowadzę tą operację? Chcę awansować na planetę. Porwali go. I na pewno mają tu swoich agentów"
* O: "Czyli to musi wyglądać że się nie dogadaliśmy. Ja na Ciebie krzyczę, Ty na mnie. Trzaskam drzwiami. I montuję operację."
* O: "Masz coś co mi się może przydać?" /zebrał /towarzysz jest GDZIEŚ na planecie, używany jako gladiator / zabawka / prey. GDZIEŚ. Ale żyje bo jeszcze żyje.
    * walczymy z kartelem
* ...
* O: "Admirał płaci za tą operację. Po prostu o tym jeszcze nie wie."
* L: "Rozpozna Cię po nazwie statku 'Alivia Nocturna'. Nie służyliśmy na nim."

Olgierd się uśmiechnął. "A teraz mnie skrzycz i wywal. I spróbuj pogadać z detektywem." A ja się tym zajmę. Odezwę się w ciągu (CZAS). Jak się nie uda, załóż, że przegrałem.

## Streszczenie

Olgierd szuka 'potworów' - noktian, astorian, orbiterowców, którzy muszą zostać usunięci - jako blackops agent. Na Kartalianie znalazł plotki o serpentisie, ale te plotki się nie spełniły. Natomiast przypadkiem pomógł Lutusowi Saaranowi, pobił ludzi stojących mu na drodze i sprzymierzył się z Lutusem by odzyskać jego 'brata' z niewoli na Neikatis, gdzie - jak się okazuje - są 'potwory' których Olgierd szuka.

## Progresja

* Olgierd Drongon: zrobił PIEKIELNE wrażenie na lokalsach bazy XXX. Złe, bo pokrzywdził w samoobronie. Dobre, bo pomógł.

### Frakcji

* .

## Zasługi

* Olgierd Drongon: młodszy (23 lata), jeszcze nie tak rozsądny, poluje na niebezpiecznych ludzi i magów ('potwory'), zwłaszcza noktiańskich i Orbiterian, w ramach black ops z ramienia Orbitera. Szukał 'potwora' na Kartalianie, znalazł noktianina Lutusa Saraana (po drodze obijając wszystko na jego drodze) i wszedł z nim w sojusz - pomoże odzyskać 'brata' Lutusa Saraana z niewoli na Neikatis. Bardzo niebezpieczny; pozornie łagodny niedźwiedź, ale przechodzi 0-100 w moment. Niesamowicie wręcz szczery i bezpośredni.
* Alicja Kirnan: agentka Lutusa Saraana, jego kurierka. Uratowana przez Olgierda, powiedziała mu o Lutusie.
* Lutus Saraan: noktianin, który pnie się wśród przestępczości by móc uratować swojego 'brata'. Ich wspólnym hasłem jest Alivia Nocturna. Zaufał Olgierdowi Drongonowi, by ten pomógł odzyskać jego brata.
* Borys Uprakocz: nożownik pracujący pod Lutusem Saraanem; dał się przekonać Olgierdowi że lepiej nie wyciągać nań noża. Najpewniej pracuje dla potężnego syndykatu na planecie ale ich nie chce wspierać i nie podoba mu się co tam się dzieje.
* Jakub Altair: detektyw działający maksymalnie zgodnie z prawem. Martwi się obecnością narkotyków i szybkim pójściu w górę Lutusa Saraana; włączył do współpracy Olgierda by mieć większe szanse.
* Maciek Kwaśnica: przestępca z Kartaliany; chciał z kolegami skroić Olgierda Drongona, skończyło się na ciężkim pobiciu. Ale w sumie Olgierd spoko chłop, popytał i nawet pomógł medycznie.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Dwupunkt Cztery: orbita od 1.4 AU do 2.4 AU gdzie znajduje się pas asteroid pomiędzy Neikatis i Gletser
                1. Kartaliana: stacja kosmiczna typu RAMA; kilkanaście km średnicy acz większość nieużywane; 
                    1. Ciemnica: dzielnica Kartaliany gdzie kiedyś wydobywano fissile i która teraz należy do lokalnej przestępczości

## Czas

* Opóźnienie: -1400
* Dni: 5

## Konflikty

* 1 - Olgierd rzuca weń manierką, skraca dystans i wali nim o ścianę. DRUGI przeciwnik rzuca w Olgierda czymś z alejki i próbuje skrócić dystans.
    * Tr +4 +3Or
    * VVrVr: skrócony dystans, szoknięty, rozbrojony przeciwnik
    * XV: laska skończy wolna, Olgierd jej nie zatrzyma; koleś złapany i unieszkodliwiony o ścianę
* 2 - Olgierd: "Daleki strzał - słyszałaś plotki o potencjalnym serpentisie w okolicy? Albo innych niebezpiecznych noktianach?"
    * Tr (niepełny) Z (bo pomógł) +3
    * XXX: Olgierd ją wypuści, ona ma przejście przez podły teren (on nie), nie ma jej jako aliantki. ALE poznał imię "Lutus"
* 3 - Olgierd szarżuje z berserkerskim rykiem w kierunku jednego. Adrenalinowy spike. Cel - skrzywdzić. 1v4 to nie kwestia zabawy.
    * Tr Z (bioforma) +2 (bo trudno) + 3Or
    * VrV: delikwent dopadnięty, w szoku, oszołomiony; Olgierd jest nieobliczalny i inni niepewni.
    * (+1Vg +2Or): V: Olgierd eskaluje i dopada drugiego. Inni uciekają.
* 4 - Olgierd tłumaczy nożownikowi co robił w przeszłości. Jakie osoby złapał. WIE że na stacji są noktianie i on ma to gdzieś. Jego celem jest potwór, nie człowiek.
    * Ex +3: thug "nie ma co z Olgierdem walczyć" więc z tym nożem tak bez serca. Powiedział też imię - Borys.
    * (+Z, integrity) Vr: Jest SYGNAŁ że Borys nożownik jest agentem Drugiej Strony. I że Borysowi nie do końca podoba się to co się dzieje na planecie.
    * 
* 5 - 
    * 
    * 
* 6 - 
    * 
    * 
* 7 - 
    * 
    * 
* 8 - 
    * 
    * 
* 9 - 
    * 
    * 
* 10 -
    * 
    * 

