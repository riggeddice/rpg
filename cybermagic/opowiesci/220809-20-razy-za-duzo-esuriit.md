## Metadane

* title: "20 razy za dużo Esuriit"
* threads: rekiny-a-akademia, dysonans-diskordii
* gm: żółw
* players: kić, sola, fireseed

## Kontynuacja
### Kampanijna

* [220730 - Supersupertajny plan Loreny](220730-supersupertajny-plan-loreny)

### Chronologiczna

* [220730 - Supersupertajny plan Loreny](220730-supersupertajny-plan-loreny)

## Plan sesji
### Theme & Vision

* Exploration -> how does the world look like? Maximize the visible components of the world while not overloading players
* HIDDEN, because they show motivation of other parties; look at the next Story in the sequence, 220819)

### Co się wydarzyło KIEDYŚ (what happened in the PAST LONG AGO)

* HIDDEN MOVEMENT (look at the next Story in the sequence, 220819)

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

* Mimosa Diakon, an Aurum aristocrat (therefore, mage), is trying to help local businesses and the locals as an Aurum aristocrat should.
    * -> she sends groups of agents left and right to help important parts of the region
* One of the mercenary Aurum groups Mimosa has requested was a group made of Mike, Thalia and Isabel; they have been sent to one of the magic purification places to help with some issues
    * They have no idea what the issues even are
    * This is one of Mimosa's tests. Will they make it?
    * However, there are THINGS happening there which are way over poor mercenaries' head

(look for character sheets / profiles / hints in ENGLISH_NOTES at the end of this file)

ENCODING:

* Thalia (T took over this character) -> Talia Mikrit; Brawler
* Mike (Fireseed took over character) -> Michał Czwardon; Scout
* Isabel (Sola took over character) -> Izabela Szentor; Sage
* Red, Green, Blue types of magical energy / contamination -> irrelevant energies
* Yellow type of energy -> Esuriit energy (look at ENGLISH_NOTES in the bottom)

### Co się stanie (what will happen)

* The war machine shall probably rise and devastate, as war machines do ;-). Or Esuriit corruption shall corrupt someone powerful. Or whatever.

### Sukces graczy (when you win)

* Lower levels of devastation
* Mimosa will not be harmed (socially) and she can still help the region
* People will not be harmed. Well, not _many_ people will be harmed.
* Political issues (Aurum <-> Szczeliniec (Chasm Region) will not be massacred)

## Sesja właściwa
### Scena Zero - impl

N/A

### Sesja Właściwa - impl

Thalia, Mike, Isabel (denoted as: TMY / Team later) were sent to the largest private magical decontamination and purification plant nearby, named the Sunflower. The Sunflower is quite fortified and it usually doesn't have much problems - neither with authorities nor with the performing of decontamination and purification itself. However, lately there have been some problems - thus the Team was sent there to investigate and help.

However, the locals do not really like aristocrats from Aurum, therefore the Team has decided to go into the local watering hole to extract relevant information. Isabel, the sage, decided to ask several questions to understand the current situation. She pretended she is looking for work (with some other people, ie: the Team) and asked the workers if this - the Sunflower - is a good place to join. 

Isabel wants to learn about manager and the state of the plant from the workers in Heavy Hammer watering hole.

Tr Z (one of us) +2:

* VVV: 
    * yeah, their presence would really help; they are swamped with work but the equipment is falling apart because of those *** from Aurum
    * the manager blamed Aurum directly - there should have been "normal" transports but there are transports with Esuriit energy, WAY too much Esuriit energy and way too much transports. About 20% of transports contained Esuriit energy including Esuriit manifestations, so their sensors are burning out
        * they are using sensors able to detect Esuriit energy, but those sensors are burning out when they do. So it is important that transports have good manifests
        * they have a special Esuriit decontamination device, but it is extremely time-consuming. So they have a choice: either put everything into the Esuriit decontaminator or scan stuff and risk burning out sensors
        * the contract assumed 1% of transports having Esuriit levels. In reality, about 20% of transports of one particular TYPE have that.
            * this type looks like an old war machine. SUCKS. We don't want Esuriit-corrupted war machines, no sir.
    * the Esuriit-corrupted parts need to have one source (from Isabel's knowledge, as the pattern fits). But where and why?
    * Manager is trying his best, but because of Aurum he cannot protect the workers
        * due to some _strange cultural reason_ the workers believe the manager and stand by him, even if they are the ones to suffer
            * manager says that the sensors etc would be replaceable, but the Aurum Enclave (Irx: Sharks' District) decided to upgrade all the sensors asap (no reason to tbh). So the plant can't buy those sensors, as they have been bought out by Sharks' District (Aurum Enclave)
            * Aurum promised the 1% Esuriit rate and they received 20% Esuriit rate
            * wait, manager tells THAT to people? XD

So yeah, cultural issues. Doesn't change the fact that actually something is happening here. But there is no way for them to learn more from the watering hole; time to join the crews of Sunflower.

**5 days later**, in Sunflower plant.

Mike is working at the facility, near sensors. He is twined and has cybernetic augmentations. He noticed that the incoming transport is a bit strange; it is definitely corrupted. And probably it is undergoing the manifestation, which kind of makes no sense, because there is not enough incoming energy.

The problem is – Mike is the only person seeing this from the distance; how the hell can Mike convince all the workers around that this place is dangerous without dropping cover? Mike quickly spun an elaborate lie – he has always wanted to be an agent of a terminus but failed, he was quite good at it, but there was that matter with the daughter. You know.

To be honest nobody does know “what about that daughter”, but if an agent of a terminus tells you there is a problem with an incoming transport it is wise to run away.

Tr +3:

* X: Whose daughter was it? This kind of became interesting and important to many people.
* V: The facility has been cleared; people decided that Mike seems to know what he's doing.
* V: People decided to obey Mike's commands. He became SUCH A HERO.

Okay – we have an anomalous transport with extreme readings of corrupted Esuriit energy and a half-conscious driver. Who probably does not really know who he is and what is happening right now. Fortunately, we also have a solution. Thalia. The paranoid, explosives loving, always prepared and trigger happy demolition expert of the team. expecting there might be an issue with one of the transports, she has mined the main roads. Super illegal, super dangerous, she doesn't care.

Thalia tries to perform a multi-stage explosion and save the driver and destroy the anomalous construct.

Tr Z (heavy explosions) +2:

* X: Sadly, the driver got heavily wounded during the explosion. Nothing terminal yet, but unfortunate.
* V: Multi-staged explosion did its job; the car and the trailer separated. The driver with the car managed to land into the ditch.
* V: The manifestation is undergoing. Some electronic devices in the facility managed to capture the... recording? It is looped and multi-voiced, as if the Ephemera was in the internal conflict with itself. "PROTECT - HIDE" vs "PROTECT - DESTROY MONSTER".
* Vz: That was a beautiful, heavy bomb. Thalia has detonated a chain of explosives directly on the path of an anomalous trailer. The trailer has been destroyed.
* Vz: Thalia has proven to be an expert; even under the circumstances nobody died. Of course there was property damage, but the driver survived even if he needed a lot of medical assistance.

The funny thing is - nobody really knows who planted explosives. From their point of view the only person who has actually done something was Mike. People started flocking around Mike asking questions – who he is, how did he know, when did he manage to plant all those explosives. Mike wasn't really prepared to answer those questions – he had tried to keep low profile, however an Ephemera successfully forced him to blow the cover.

So Mike has decided to do what he was not really used to.she decided to tell the truth, rather the most palatable lie. But one closest to truth possible.

He explained that he comes from Aurum. He needs to act silently, in disguise, because the local Aurum Enclave makes people really dislike his faction (here, he received some confirming nods).

Mike has also said that the person who hired him is Mimosa Diakon, who is trying to help people.when there was that problem with daughter and stuff, she had helped him.

However that elicited a reaction from the plant supervisor, Carl. Carl blurted, that Mimosa is the person responsible for the contract which brings the corrupted shipments in the first place. But this makes absolutely no sense for the Team. Mimosa's intentions have always been quite transparent, and she is operating with many different teams trying to help this region. Why would she do that?

Isabel decided to make a move – she wants to see the shipping manifests. If they are counterfeit then she will be able to determine and show that to people as a proof. Let's be honest – after everything that transpired the manager had no other way than to allow Isabel to look into documents. Otherwise he would look like a person having something to hide what would shatter his carefully cultivated face.

Isabel analyzes a manifest trying to extract information where did stuff come from, find proof etc.

Tr Z (many manifests, full cooperation) +3: 

* X: This is, sadly, just circumstantial evidence. Inadmissible.
* X: People will not believe the results; even if the team understands what is happening they won't be believed.
* X: The Other Side is aware that there is someone around interested in this topic.
* X: It REALLY, REALLY looks like Mimosa. There is an extremely good forger, one who really knows his stuff.and who knows the administrative stuff around. So Mimosa will have severe backlash even with Mike's praises (as in – she screwed something up and now sent the Team to correct her mistakes; or that is some kind of Aurum internal political kerfuffle).
* (free): Isabel has managed to find the origin of the parts - she knows from which sector those parts come from. Several manifests confirm that sector and also Isabel knows, that Mimosa has a team working there. So this makes sense. (+3Vg for tempting)
* V: The war machine could not have originated in that region; the region which Mimosa's team is trying to scrub is near the town of Czółenko ("Canoe") and Mimosa put a slightly untested team there. In other words, that place should be quite safe. And the war machine needed to come closer to several kilometres either towards the Marsh or towards the Corrupted Bunkers, were a massacre occurred during the war.
    * In other words, it looks like someone has found a corrupted war machine and is trying to add some of its parts to this region or to the transport originating from this region. But that would mean that the other team might be somehow connected to this, because the manifest enumerates the exact amount of entities which are coming to this plant. So – either some there is bribed or there should be differences in the manifest. If all the manifests are identical on both sides (leaving, arriving) then we are dealing with some kind of COMMANDO FORGERS.
    * But the reasons and an actual truth still elude our team. They know where to look, though.

Isabel has also realised one thing – if we are dealing with some kind of corrupted war machine having a dissonance between subpersonalities, that means the entity was probably originally twined. The corrupted parts delivered to this plant looked like some kind of advanced combat unit. There were not many advanced combat units in this region neither during the war nor after. That means it could be possible to trace what units used to be here in the past and find one with sufficiently advanced AI that it could have been corrupted by Esuriit.

This would point towards Persephone class AI. Persephone is usually created using human brain patterns but they are being slightly lobotomised in order to make them fit for the purpose. However, this a I is simply too large in size to be put in a small unit; it usually is used in units no smaller than spaceship corvettes.

That would point towards some kind of drone-controlling hovertank or heavy track-based devastator tanks. And those units, corrupted by Esuriit, would be greatly unfortunate.

## Streszczenie

Fortified privately-owned decontamination-purification plant Sunflower has a bad time. Esuriit-corrupted shipments are being delivered to them and they can't do much about it. Because the problems started to be visible, Mimosa has sent an untested Team there to discreetly help Sunflower out. The Team has managed to find out that there are Esuriit manifestations, that we are probably dealing with some kind of heavy AI-twined tank whose parts are arriving and that Mimosa is the one being blamed by perfect forgery. Aha, and the parts needed for Sunflower's operational capacity are being consumed by the Sharks' District where Mimosa lives because of "some dumb idea of a random dumb aristocrat". To Be Continued.

## Progresja

* Mimoza Diakon: receives backlash because "it's her stuff which is corrupted by Esuriit" and "her political shit is damaging the local region". She won't be happy.
* Michał Kabarniec: the best recognized stealth operator. Local hero of Sunflower plant. People are interested in his "because of that issue with daughter, you know".

### Frakcji

* .

## Zasługi

* Talia Mikrit: Thalia; Brawler; wanted to stop a truck manifesting Esuriit and did that with explosives saving driver's life. She REALLY likes blowing stuff up.
* Izabela Selentik: Isabel; Sage; got information from the workers at watering hole about the problems plaguing Sunflower plant and later managed to extract information from manifests - although there is an amazing forger, she managed to piece stuff together and infer a Persephone-assisted tank. Ah, and she knows where to go ;-).
* Michał Kabarniec: Mike; Scout; managed to spread the people so Esuriit-manifesting truck won't damage them and spun a lie upon a lie upon a lie... he became a local hero of sorts and his problem "because of daughter, you know" became a public secret.
* Mimoza Diakon: operates in Szczeliniec trying to show people how an Aurum aristocrat should act, pouring money into the region and improving the lives of people here. This time she has hired the group of Aurum-based mercenary agents (or freelancers, it's the same) so they can solve the strange problems troubling Sunflower Decontamination-Purification plant.
* Karol Walgoryn: the supervisor in a Sunflower Decontamination-Purification plant, close to the workers and likes to talk a lot. Mike gave him no choice but to deliver the manifests for analysis; fortunately, he won't have any problems related to this.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Dzielnica Luksusu Rekinów: the Sharks' District Aurum Enclave is buying out all the sensors (including Esuriit-scanning ones) to the Sunflower plant's chagrin
                                1. Kompleks Korporacyjny
                                    1. Bar Ciężki Młot: encoded as 'Heavy Hammer'; a popular blue-collar place where Isabel has managed to get information on some problems troubling Sunflower plant.
                            1. Podwiert, okolice
                                1. Oczyszczalnia Słonecznik: encoded as 'Sunflower'; fortified private decontamination-purification plant; 
                            1. Czółenko, okolice: somewhere there is a place decontaminated by Mimosa's team which is being used to drop Esuriit-corrupted Persephone-based land unit.

## Czas

* Opóźnienie: -4
* Dni: 6

## Konflikty

N/A, impossible to capture really this time because of environmental hazards (lack of computer)

## Kto
### Aurum team:

* BRAWLER Thalia <-- T
    * aspects (incomplete list): heavy machinery, explosives, close combat, heavy weapons, intimidation, unbreakable, cool head, strong like ox
* SAGE Isabel   <-- Sola
    * aspects (incomplete list): computers, documents, social / friendly, heraldry, local knowledge, respected
* SCOUT Michał  <-- Fireseed
    * aspects (incomplete list): sniper, lying, devious, stealth, master of wilderness, twined, Eidolon power suit (stealth suit)

### Aurum :

## ENGLISH NOTES

### Energy types

* Esuriit Energy (encoded: YELLOW)
    * Primary: HUNGER; unquenchable ambition, corrosion of soul. Exploitation without end. Obsession; hunger which, satiated, will create more hunger. Addiction.
    * Secondary: HATRED; "what others have I cannot, the relief I cannot get. Pain and suffering which always flares and pushes me and I need to DEVOUR and THEY ARE THE CAUSE OF IT!"
    * Song representation: 
        * Umbra et Imago "Endorphin": "In a state of total isolation The suffering decides my behaviour The bloodlust begins" 
        * Luca Turilli "Black Rose": "Red rose so attractive bleeding hurting feelings in this state of hate your colours turning black"
    * "horrific mass of organic beings melded together, and every one of those beings is pulling and clawing away eternally, trying to escape the pain and pursue even the smallest pleasure or consumption to distract themselves from the agony of their horrible existence" <-- Path of Exile; good representation of its aspects
    * Esuriit is not just hate or hunger. Esuriit energy HATES YOU. It is un-life.
* Ixion Energy (encoded: PINK)
    * Primary: ADAPTATION; ever-changing magical world will defeat you unless you change to make it work. Shed everything inefficient and irrelevant - friends, love, your own body.
    * Secondary: SACRIFICE; nothing is free. You need to give up what you used to be and love. "You" tomorrow wouldn't understand "You" yesterday. Drop humanity for efficiency.
    * You did not have to deal with Ixion energy yet; it is related to The Marsh.

Esuriit >> Ixion. Technically, Esuriit > most energies, with the exception of Interis (PRIMARY: entropy SECONDARY: helplesness). TECHNICALLY every other energy represents a human facet, a way for humans to cope with the eventual victory of entropy. If you want me to, I will explain.
