## Metadane

* title: "Kto wrobił Alana"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [190626 - Upadek Enklawy Floris](190626-upadek-enklawy-floris)

### Chronologiczna

* [190626 - Upadek Enklawy Floris](190626-upadek-enklawy-floris)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

brak

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Do Pięknotki odezwał się bardzo nieszczęśliwy Tymon. Talia zgłosiła formalną, poważną skargę na Alana. Czy Alan faktycznie coś jej zrobił? Podobno Talia ma dowody, ale Tymon nie może przejąć dowodów nie przyjmując skargi. Ogólnie, jest tu pewien problem... a że ona chyba ufa Pięknotce, to może ona to sprawdzi?

Talia spotkała się z Pięknotką; opowiedziała, że Alan był ją przesłuchać odnośnie AI w Cyberszkole. Jedno z nich faktycznie było jej (nie BIA, AI); ale Alan na odchodnym zniszczył jej AI. Poranił i i poniszczył; dla Talii to jak poranienie zwierząt domowych. Pięknotka spróbowała wybadać Talię; Talia mówi to, co uważa za prawdę. Więcej, ma dowody świadczące, że mówi prawdę - dowody urządzenia scramblującego AI. I Pięknotka jest dość przekonana, że Talia nie wycofa swoich skarg - raczej eskaluje. Bolesna sprawa.

Pięknotka poprosiła Talię o to, by ta położyła dowody do rzadko używanego pomieszczenia - a Pięknotka je zasealuje. Talia się zgodziła, ale powiedziała, że nie chce by kiedykolwiek ktoś stamtąd dowody wykradł. Pięknotka się zdziwiła, ale zgodziła na te warunki...

Pięknotka poszła spotkać się z Chevaleresse, po jej szkole. Chevaleresse jest dość ostrożna; szuka potencjalnego zagrożenia, ale tak casualowo. Pięknotka próbowała ją troszkę dokładniej wyczaić; Chevaleresse wyraźnie oczekuje zagrożenia z zewnątrz, oraz jest to zagrożenie typu bojowego. Np. ktoś będzie chciał ją porwać. I trzyma się blisko... Karoliny Erenit. Pięknotka wzięła Chevaleresse na bok. Wpierw small talk; potem zdecydowała się dopytać o co chodzi z tymi AI. Chevaleresse się ucieszyła - Talia jest całkiem fajna, robi zacne AI do gier komputerowych.

Czyli - czego się boi? Chevaleresse dostała pytanie z zaskoczenia. Niechętnie odpowie (Tr:P); dowiedziała się, że Alan będzie miał problemy przez tamten artefakt. Powiedziała, że kiedyś frustrowała się strasznie walcząc z AI i Alan zrobił dla niej taką zabawkę. Nie chciała tego używać i nieuczciwie wygrywać; Alan się ucieszył i schował zabawkę do szafy. A Chevaleresse kiedyś potem to zabrała i zaniosła do szkoły; chciała się komuś pochwalić. Ech...

Ale to znaczy, że Talii ktoś to podłożył. Nie Alan.

Pięknotka powiedziała Chevaleresse, że jeśli ta powie swoją historię Talii, może się uda zdjąć z niej gniew na Alana. Chevaleresse się zgodziła. Acz jest... zmieszana. Pięknotka dopytała - Chevaleresse wie, że Talia jest noktianką. A przecież Chevaleresse nie ma rodziny przez Noktis...

Pięknotka zaprowadziła Chevaleresse do Talii. Chevaleresse rozpoznała ten artefakt. Młoda czarodziejka się bardzo pokajała. Nie wytrzymała jednak; strzeliła, że "Noktis zabrało moich rodziców i rodzinę. Mam tylko Alana. Jego też chcesz mi zabrać za mój błąd?!" Talia powiedziała, że w tym świetle - nie. Bo wreszcie Chevaleresse powiedziała coś prawdziwego. Talia zaproponowała Chevaleresse, że wszystko wycofa, jeśli Chevaleresse odwiedzi ją kilka razy po 2h by się nauczyć historii. Tu wbiła się Pięknotka - wykluczone. Nie bez zgody Alana a Chevaleresse jest niepełnoletnia.

Talia wycofała swój pomysł. Anuluje wszystkie problemy. Powiedziała Chevaleresse, że współczuje jej straty.

Oki - Pięknotka odwozi Chevaleresse do domu. Mówi jej, że musi wiedzieć kto to ukradł. Chevaleresse nie chce dać Pięknotce imion - Pięknotka tego potrzebuje, bo to jest konspiracja przeciw terminusowi, to jest poważniejsze i to się powtórzy. Chevaleresse podała kilka imion, z wielkim smutkiem. M.in. Liliana Bankierz, Napoleon Bankierz i Karolina Erenit. Jedno z tej trójki to osoba, która najpewniej to ukradła Chevaleresse.

Pięknotka przesłuchała wszystkich tych magów. Przyznała się... Karolina Erenit. Powiedziała, że jakiś mag z Aurum chce wrobić Chevaleresse i zrobić jej krzywdę. Zdaniem Karoliny, Chevaleresse ma bogatego stalkera z Aurum. Więc Karolina ukradła to czego ten mag chciał, by łatwiej było jemu zastawić sidła na Chevaleresse - a Karolinie na osobę atakującą.

Pięknotka uznała, że plan Karoliny jest niezły; ale zarówno Karolina jak i Diana potrzebują przyjaciół...

Oki - czas użyć Chevaleresse jako przynęty na tajemniczego napastnika. Sama Diana jest bardzo przybita tym, że to Karolina ją zdradziła. Ale otrząsnęła się jak pies. Pięknotka wciągnęła do asekuracji Tymona. Jest gotowa do przynęty.

Pięknotka złożyła "historię". Chevaleresse idzie do Talii; niech pułapka będzie na Nieużytkach Staszka. I faktycznie, Pięknotce udało się doprowadzić do tego, że totalnie zaskoczyli napastników Chevaleresse.

Napastnicy - 3 magów i dwóch ochroniarzy. Magowie grali z Chevaleresse w tą samą grę; Chevaleresse zabrała ich złoto. Czarodziejka potwierdziła - w ogóle zachowuje się bardzo twardo. W końcu jeden nie wytrzymał - uderzył Chevaleresse i ją przewrócił. Na to weszła Pięknotka z Tymonem... i zrobili wielką opierniczankę zagłady.

Młodzi magowie z Aurum byli przerażeni wjazdem terminusów a Chevaleresse się z nich po prostu śmiała gdy Alan i Pięknotka robili im z tyłka jesień średniowiecza. Magowie Aurum próbowali zarzucić Chevaleresse, że ta się rozbiera w virt i kradnie złoto. Chevaleresse potwierdziła ze śmiechem. Tak czy inaczej, Pięknotka zabrała całą piątkę do Aurum, do aresztowania.

Czego sesja NIE pokazała:

* Potem Alan i Chevaleresse mieli chyba najbardziej epicką przekrzykiwankę. On, że ona rozbiera się w virt. I w ogóle. Ona, że to jej życie i nie robi nic złego. On, że ona zagraża jego karierze. Ona, że on wpierw musiałby mieć karierę a nie "siedzi i słucha pustki" majstrując nad różnymi dziwnymi broniami.
* Chevaleresse wyzwała Karolinę na pojedynek magiczny. Jest to najgłupszy sposób walki z Karoliną. Pojedynek zakończył się śmiesznie - żadna z nich nie trafiła w tarczę przeciwniczki trzy razy. Obie za bardzo się szanują i czują się winne by chciały wygrać w tym starciu.

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Ktoś wrabia Alana. Okazało się, że to nie Alan jest celem - to Chevaleresse. Podpadła kilku magom z Aurum w vircie i stwierdzili że ją nastraszą - ale najpierw muszą odsunąć Alana na bok. Pięknotka z Tymonem zastawili pułapkę współpracując z Chevaleresse jako przynętą. Udało się - magowie Aurum wpadli. Przy okazji - Karolina pomagała magom Aurum bo uznała, że to pomoże terminusom i Chevaleresse najbardziej. Karolina bowiem nienawidzi arystokracji magów.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: pogodziła Chevaleresse i Talię, osłoniła Alana przed skargą oraz zastawiła skuteczną pułapkę na Wojciecha Zermanna.
* Alan Bartozol: nic nie wie, że był wrobiony. Ale potem się dowiaduje, że Chevaleresse zwinęła artefakt oraz że zdenerwowała magów Aurum. Skończyło się na gorącej kłótni.
* Diana Tevalier: chciała się pochwalić artefaktem który posłużył do wrobienia Alana. Bardzo przeprosiła Talię i posłużyła jako przynętę przeciwko magom Aurum dla Pięknotki.
* Talia Aegis: chciała poważnie uszkodzić Alana za scrambler AI, ale jako, że to było podłożone... odpuściła. Nie będzie atakować niewinnego.
* Karolina Erenit: zwinęła Chevaleresse artefakt Alana i oddała go magom Aurum by wpakować ich w katastrofalne kłopoty.
* Wojciech Zermann: główny wróg Chevaleresse; pochodzi z Aurum. Tym razem próbował ją nastraszyć i wrobić Alana - ale zostało to zneutralizowane przez Pięknotkę.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo

## Czas

* Opóźnienie: 3
* Dni: 2
