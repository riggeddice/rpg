## Metadane

* title: "Esuriit w sercu Alicji"
* threads: esuriit-w-czolenku
* gm: żółw
* players: onyks, strus

## Kontynuacja
### Kampanijna

* [190530 - Porwana foodfluencerka](190530-porwana-foodfluencerka)

### Chronologiczna

* [190530 - Porwana foodfluencerka](190530-porwana-foodfluencerka)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. Alicja / Eliza

### Wizja

* .

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Alicję, po tym wszystkim, pożera nienawiść do Elizy i wszystkiego innego. Teoretycznie jest wszystko w porządku; w praktyce, Esuriit coraz silnie zagnieżdża się w młodej czarodziejce.

Eliza poprosiła dwóch magów z Szkoły Magii o to, by pomogli Alicji. Z nią jest coś nie tak. Jednocześnie, powiedziała im, że boi się stalkera. Ktoś ją śledzi. To jest dziwne - ale nie chciała mówić sama; Zespół ją przekonał, by pozwoliła sobie pomóc. Stalker będzie wieczorem, wygląda jak takie dziwne oko. Ale przecież to nie jest możliwe.

Tymczasowo - do szkoły magów. Tam, Alicji dokuczają uczniowie, a dokładniej Rafał. Chce po plecach Alicji wejść do popularności. Alicja nie zachowuje się jak zwykle; nie ma w niej agresji czy silnych uczuć. Jest taka trochę... pusta. Zespół poszedł do jej obrony (słownie), aż w końcu Alicja nie wytrzymała i uderzyła Rafała. Zespół jednak przekonał Rafała, że to nie jest dobra droga; niech raczej Alicji pomoże - więcej na tym zyska.

Alicja ma zero energii. Wróciła z dywaniku. Nie ma w niej tej energicznej i antypatycznej czarodziejki którą kiedyś była.

Dobrze, kryzys rozwiązany. Czas na wieczór. Czas znaleźć cholernego stalkera.

Udało im się znaleźć stalkera. To skrzydłochłept; vicinius. Stworzony przez kogoś, ale przez kogo i po co? By odpowiedzieć na to pytanie, Zespół wpierw uśpił Elizę i jej rodziców a potem zastawili pułapkę na skrzydłochłepta. Udało się - złapali go i pokazali go Tymonowi. Teraz terminus widział, że istnieje faktyczny problem / przeciwnik. Jeśli tak, to on się tym zajmie. Jednocześnie Zespół doszedł do tego, że to Alicja stworzyła tego skrzydłochłepta przeciw Elizie. Alicja nie wiedziała, że to zrobiła; to nie ona. Esuriit się w niej zagnieździło.

Jak pokonać mrok? Trzy dni później wzięli Alicję na imprezę na Mekkę Wolności. Alicja nie chce mieć nic wspólnego z Elizą już nigdy, ale OK.

Tam Alicję chciało porwać trzech thugów; Zespół zmanipulował ich że nikogo nie widzieli i powiązali tych ludzi z siłami Kajrata. Auć.

Znowu sprawa wróciła do Tymona - trzeba Alicję wywieźć. Jest powiązana z Esuriit. Alicja nie chce, ale w końcu została przekonana - jest zagrożeniem dla swojej rodziny, dla Elizy i ogólnie. No a w Pustogorze można pomóc jej przy użyciu m.in. Lucjusza Blakenbauera. W końcu się zgodziła...

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Alicja Kiermacz została wyleczona, ale Esuriit do niej wróciło. Prawie skrzywdziła swoją eks-przyjaciółkę (Elizę) i prawie porwał ją Kajrat. Zaczęła gasnąć i tracić energię. W końcu jednak została przekonana wbrew sobie do pojechania do Pustogoru - tam mogą jej pomóc. Oby.

## Progresja

* Alicja Kiermacz: niechętnie, ale wywieziona z Zaczęstwa do Pustogoru. Tam można jej pomoc z Esuriit.

### Frakcji

* .

## Zasługi

* Alicja Kiermacz: wydawało się, że się wyleczyła, ale Esuriit ją pochłonęło. W końcu skończyła w Pustogorze po tym, jak zainteresował się nią Kajrat z uwagi na jej unikalne umiejętności.
* Eliza Farnorz: chciała rozpaczliwie uratować relację z Alicją. Niemożliwe. Za to energia Esuriit Alicji prawie ją zabiła powołując skrzydłochłepta. Zespół ją uratował.
* Rafał Muczor: uczeń Szkoły Magów; chciał na plecach Alicji wejść do sławy i poważania. Nie udało mu się - zaczął jej pomagać by zostać ważnym i istotnym magiem.
* Tymon Grubosz: terminus, który zatroszczył się o los Alicji Kiermacz. Wywiózł ją do Pustogoru po upewnieniu się, że w Alicji jakoś zagnieździło się Esuriit.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Akademia Magii, kampus: Alicja chodziła do AMZ jako aspirująca czarodziejka; tam poprztykała się z Rafałem i trafiła na dywanik.
                                1. Mekka Wolności: miejsce, gdzie prawie Alicję złapały siły Kajrata podczas niezłej imprezy.


## Czas

* Opóźnienie: 10
* Dni: 2
