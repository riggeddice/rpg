## Metadane

* title: "Plaga jamników"
* threads: krysztaly-noktianskiej-pamieci, deprecated
* motives: nuisance-pests, zaraza-powoli-infekujaca, biznesowa-okazja
* gm: żółw
* players: anna, pawel_f

## Kontynuacja
### Kampanijna

* [190217 - Chevaleresse](190217-chevaleresse)

### Chronologiczna

* [190217 - Chevaleresse](190217-chevaleresse)

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Alsa jest zainteresowana różnymi ciekawostkami i rzeczami typu artefakty, więc przekonała Czerwone Myszy w Podwiercie, by oni pozwolili jej przebadać interesujący tunel w kopalni Terposzy. Myszy się zgodziły i Alsa zeszła w podziemia z dwoma górnikami.

Jednocześnie w okolicy pojawiła się wyjątkowo irytująca plaga - wszędzie łażą jamniki i wyją "auuwuwuwuwu". Niestety, są też tam jamniki w kopalni. Łażą i wyją. Górnicy chcieli je rozwalić, ale Alsa ich zatrzymała - nie wiadomo co się stanie. Oki. I dotarli w końcu do tajemniczej ściany w kopalni - jest to ściana lodu, ale to dziwny lud. Alsa zna się na różnych starych artefaktach. To jest system zabezpieczeń. Alsa wezwała wsparcie - Tomka.

Alsa i Tomek spróbowali użyć magii by dostać się do środka dziwnego korytarza; udało im się, ale uruchomili system zabezpieczeń. Teraz oni są w środku (jako "swój"), lecz na zewnątrz mamy sześć Zarażonych Jamników. Te jamniki mają zdolność infekcji dalej, "chaos in the crystal". A nasz Zespół jest tu uwięziony. Świetnie.

Alsa poeksperymentowała z konsolą. Udało jej się skontaktować z kimś - z Elizą. Eliza Ira, bardzo niebezpieczna czarodziejka floty inwazyjnej z czasów wojny. Tomek przekonał Elizę, żeby ona jednak powiedziała im jak to wyłączyć. Eliza się zgodziła - ale za cenę tego że otrzyma kilka niebezpiecznych przedmiotów z tego TechBunkra - no i zwłoki swojego zmarłego podwładnego.

Zespół przygotował sabotaż. Niech Eliza nie dostanie aktywnych i niebezpiecznych rzeczy którymi mogłaby zrobić coś bardzo złego. Udało im się (SS), acz jako konsekwencja Eliza dostała też dwóch górników (nie chciała). No cóż, sabotowanie transpondera i teleportera przez laików nigdy nie działa idealnie. Ale przynajmniej Eliza nie ma broni ciężkiego kalibru.

Oki, mają złoto, mają sprzęt - teraz pytanie o co chodzi z tymi cholernymi jamnikami. Czas odnieść się do poprzedniej kategorii wiedzy. Albo to wina Olgi albo Wiktora Sataraila. Wszyscy stawiają na Wiktora, więc warto porozmawiać z Olgą.

Olga wyraźnie coś kręciła. W końcu przyciśnięta przez Tomka (SS) powiedziała - to ona spuściła na to miejsce plagę jamników. One dają jej spokój, ludzie nie zajmują się dokuczaniem Oldze. Gdyby nie to, Wiktor spuściłby na to miejsce groźną, niebezpieczną i brutalną plagę. Oki.

Jakie są warunki Olgi? Niech Czerwone Myszy stąd odejdą. I nie wrócą. Tomek wyszukał inną grupę - Towarzystwo Historyków Pustogoru i dyskretnie poinformował Pustogor o sprawie. Niech Czerwone Myszy zostawią to miejsce i wezmą inne. Ale niech nie kontrolują trójkąta Podwiert - Żarnia i Czarnopalec.

Tymczasem przez energie magiczne na tym terenie pojawiło się nowe zjawisko - "Wyjący Zew". Jamniki od czasu do czasu się pojawiają. Nowy typ stabilnej efemerydy na tym terenie - jakby było mało problemów.

Jeden plus - Czerwone Myszy nie mają już problemów finansowych. A Olga ma spokój - nikt jej już nie będzie atakował.

Wyszła też ciekawa sprawa z przeszłości:

* Eliza dowodziła oddziałem Terroru. Jej zadaniem był terror, chaos oraz panika wśród ludności.
* Celem wojny było usunięcie Saitaera z Astorii.
* Eliza zawiodła - nie doprowadziła do 'genocide'. Nie chciała mordować cywili. Terror to nie eksterminacja.

Wpływ:

* Ż: .
* Zespół: .

**Sprawdzenie Torów** ():

2 Wpływu -> 50% +1 do toru. Tory:

* .

**Epilog**

* 

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
|               |         |             |

Czyli:

* (K): .

## Streszczenie

By pozbyć się problemów z Czerwonymi Myszami raz na zawsze, Olga zrobiła Plagę Jamników (która przerodziła się w "Wyjący Zew"). Tymczasem archeolog zeszła w podziemia kopalni Terposzy i natrafiła na nieaktywne zabezpieczenia z czasów Wojny. Uruchomiła nową plagę i współpracując z Elizą Irą rozmontowała ją. Potem rozwiązali problem Olgi usuwając Myszy z tego terenu. Olga nie ma już kłopotów z tępiącymi ją magami.

## Progresja

* .

## Zasługi

* Alsa Fryta: archeolog "indiana jones". Odkryła stary kompleks żołnierza Elizy Iry i sabotowała kilka niefortunnych rzeczy.
* Tomek Żuchwiacz: wynegocjował z Elizą Irą usunięcie krystalicznej plagi oraz oddanie jej zwłok. Potem dotarł do prawdy od Olgi.
* Eliza Ira: chciała zdobyć stare systemy bojowe. Zamiast tego dostała je zsabotowane plus dwóch górników (czego nie chciała). I jedno ciało martwego kompana.
* Olga Myszeczka: zesłała Plagę Jamników na Podwiert by ludzie dali jej spokój. No i dali - stworzyła Wyjący Zew.

## Frakcje

* Czerwone Myszy Pustogorskie: tracą kontrolę nad terenem Podwiertu i Czarnopalca, ale nie mają już problemów finansowych. Scale down, ale są bezpieczniejsze i silniejsze.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert: w okolicy od tej pory działa "Wyjący Zew" - plaga jamników
                                1. Kopalnia Terposzy: gdzie znaleziono stary system zabezpieczeń z czasów wojny; już usunięty przez Alsę.
                            1. Czarnopalec
                                1. Pusta Wieś: gdzie Olga zrobiła Plagę Jamników by usunąć problem Czerwonych Myszy.

## Czas

* Opóźnienie: 1
* Dni: 1
