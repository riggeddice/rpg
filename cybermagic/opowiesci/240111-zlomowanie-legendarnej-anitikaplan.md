## Metadane

* title: "Złomowanie legendarnej Anitikaplan"
* threads: agencja-lux-umbrarum
* motives: biznesowa-okazja, potwor-scooby-doo, wojna-domowa, energia-sempitus, energia-alteris, lojalnosc-syntetycznych-intelektow, dlugi-kupione, konflikt-wewnatrzzespolowy, oszczedzanie-na-wszystkim, radykalizacja-postepujaca, skazenie-magiczne-maszynerii, starcia-kultur, the-pastmaster
* gm: żółw
* players: szary, klusarz, owcen, sidvs

## Kontynuacja
### Kampanijna

* [240111 - Złomowanie legendarnej Anitikaplan](240111-zlomowanie-legendarnej-anitikaplan)

### Chronologiczna

* [240111 - Złomowanie legendarnej Anitikaplan](240111-zlomowanie-legendarnej-anitikaplan)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * Final Light - "In the Void"
* Opowieść o (Theme and vision): 
    * Wojna nigdy się nie kończy.
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * biznesowa-okazja: grupa salvagerów zakupiła prawa do salvagowania starego statku kosmicznego "Anitikaplan". To ich ostatni strzał na przetrwanie. Ich statek to "Varilen".
    * potwor-scooby-doo: grupa nielegalnych TAI żyje na "Anitikaplan". Próbują sprawić, by wszyscy myśleli, że tu jest coś anomalnego / potwór by zostawili ich w spokoju.
    * wojna-domowa: Anitikaplan był jednostką, która ucierpiała w wyniku Wojny Deoriańskiej - wtedy został zniszczony, w największym ogniu walki i nienawiści. Echo nie zapomniało.
    * energia-sempitus: wojna NIGDY się nie kończy. Duchy chcą kontynuować Wojnę Deoriańską, odtworzyć echo tego co się tu działo przed tymi wszystkimi latami.
    * energia-alteris: Past Will Become The Present. Przywrócenie przeszłości w określonych miejscach, czy przez halucynacje czy akcje.
    * lojalnosc-syntetycznych-intelektow: te synty, które pamiętają te czasy - dla nich wojna się skończyła. One nie chcą kontynuować wojen. One chcą ratować wszystkich ludzi nieważne co się nie dzieje.
    * dlugi-kupione: część osób, które tu są i pracują nie pracują z własnej woli. Pracują tu bo muszą.
    * konflikt-wewnatrzzespolowy: sporo subfirm i subfrakcji działających z ramienia salvagerów z Varilen; część osób nie chce tu być, część uważa ten statek za pomnik.
    * oszczedzanie-na-wszystkim: nie pomaga fakt, że Varilen potrzebuje pieniędzy i rozłożenie Anitikaplan na kawałki jest dla nich po prostu ważną okazją. Kiepski sprzęt i awarie.
    * radykalizacja-postepujaca: im więcej krwi, im więcej śmierci, im więcej WIEDZY o Wojnie Deoriańskiej - tym większa szansa że historia się powtórzy
    * skazenie-magiczne-maszynerii: część elementów tej jednostki nie działa normalnie a chodzi na magii. Trzeba ewakuować statek i usunąć źródło Skażenia.
    * starcia-kultur: zatrudniający to klarkartianie. Część kontraktorów to savaranie a część to przesądni faerile.
    * the-pastmaster: Duchy działające na Anitikaplan; pragną odtworzenia wojny deoriańskiej przy użyciu obecnych ludzi.
* Detale
    * Crisis source: 
        * HEART: ""
        * VISIBLE: ""
    * Delta future
        * DARK FUTURE:
        * LIGHT FUTURE: 
            * chain 1: .
    * Dilemma: brak

### Co się stało i co wiemy

* Anitikaplan - statek generacyjny - sprzedawał nowe zaawansowane TAI i działał niezależnie jako jednostka
    * Podczas Wojny Deoriańskiej grupa fanatyków wyrżnęła załogę
    * Anitikaplan obrócił się przeciwko nim; wszyscy zginęli a Anitikaplan poleciał jako wrak kosmiczny
* Anitikaplan został odkryty i odnaleziony; kupili prawa do salvagingu ludzie z Varilen
    * Trzy frakcje Varilen:
        * Drakolici rządzący jednostką; oni PRAGNĄ pieniędzy i nadziei dla siebie (Wiktorianie)
            * Wiktor Worl (Megatron)
            * Twaróg Worl (Starscream)
            * Mirea Worl (Soundwave, oficer naukowy - kontraktowo zobligowana)
        * Faerile, przesądni którzy nie chcą tu być ale ich długi są wykupione (Kompania Straceńców), oni PRAGNĄ odkupienia długów i wolności
            * Antos Urak (Broken Kenobi)
            * Hanna Pireus (Starry-Eyed Novice) 
        * Savaranie, którym to pasuje (3 firmy: Sal-17, Sal-219, Sal-441), oni PRAGNĄ spokojnego zarobienia dla rodziny
            * Mia-117
            * Tal-93
    * Na Anitikaplan są trzy inne frakcje:
        * 'Anitikaplan Jeden': potężny AI Core, próbujący dalej chronić ludzi tym co pozostaje. On PRAGNIE by ludzie przetrwali, nie zależy mu na przetrwaniu.
        * 'Ekosystem': zbiór 'wolnych TAI', oni PRAGNĄ by ludzie sobie poszli lub wymarli. Najlepiej poszli, ale odbudowują działa Anitikaplan.
            * "chichotka zielona"
            * "wesoły tygrysek"
            * "mroczny groźny dino"
        * 'Duchy': niech przeszłość stanie się teraźniejszością. Kontynuacja LUB koniec Wojny Deoriańskiej.

### Co się stanie (what will happen)

* F0: TUTORIAL
    * Grupa salvagerów wbija się do obszaru z przodu statku, zbierają porozrzucane rzeczy po pokojach
    * TAI robią anomalie w komunikatorach. 'Wycie wiatru', 'oooOOooo', zamykające się drzwi (resztka energii)
    * Jeden zainfekowany
* F1: ???
    * Pojawia się Perez (Faeril, uciekł, widział jak zabili żonę kapitana)
    * KTO (łańcuchy)
        * Wiktorianie: 
            * chain: (salvage, salvage MORE, wchodzimy głębiej, zabijamy swoich (kontrakt), insanity, deorian)
        * Separatyści (kontrola Deoriańska): 
            * chain: (zbrojenie, sabotaż, atak z zaskoczenia, fortyfikacja, spread the corruption)
        * Ekosystem:
            * chain: (straszymy, firefight, naprawiamy działka, infekujemy Varilen)
* Overall
    * stakes
        * Alteris i Sempitus zmieniają statek; wojna powraca, brat vs brat
        * wiedza o magii
        * śmierć Varilen
    * opponent
        * Duchy
        * Wolne TAI
    * problem
        * wojna deoriańska
        * ludzie sobie nie ufają
        * za dużo niegodnych śmierci, za duże napromieniowanie, brak wody

## Sesja - analiza

### Fiszki

.

#### Strony

.

### Scena Zero - impl

Grupa złomiarzy została wysłana na pokład Anitikaplan, by rozłożyć na kawałki tą legendarną jednostkę. Anitikaplan od kilku lat jest nieaktywna; jest ofiarą Wojny Deoriańskiej.

Brygadzista widzi jak jeden ze złomiarzy nie chce pracować nad wyciąganiem rzeczy z sejfu. Zapytany, złomiarz odpowiada że to problem tego że to Anitikaplan. Okradanie nawet zmarłych ludzi z Anitikaplan jest czymś po prostu złym z jego punktu widzenia. Zdaniem brygadzisty trzeba to zrobić - wszyscy są martwi i potrzebna jest kasa.

Po krótkiej rozmowie Brygadzista odesłał tego złomiarza i jeszcze jednego ochotnika do innego miejsca. Niech oni zajmą się salvageowaniem innych rzeczy.

Po jakichś 10 minutach na komunikatorze słychać „ooooOOOooOOOoo”, jak Duch z kreskówki. Wszyscy patrzą na siebie jak na jakieś żarty. Ale chwilę potem – przypominam, że Anitikaplan nie ma zasilania ani grawitacji - na moment zapala się jedno ze świateł.

To już jest niepokojące. A 2 odesłane osoby nie odpowiadają na komunikatorze. Brygadzista idzie z jeszcze 2 osobami zobaczyć co tam się dzieje. Napotyka na jednego ze złomiarzy który pochodnią plazmową zabija drugiego i pastwi się nad jego zwłokami.

TrZ+3:

* X, Vr, X: morderca ucieka. Jeden ranny, jeden martwy, brygadzista i kilka osób, oni uciekli na statek.

Brygadzista próbuje unieszkodliwić agresora, ale ten w jakiś sposób - mimo próżni - zorientował się że zbliżają się do niego ludzie. Agresor skutecznie ciężko zranił jeszcze jednego salvagera zanim został obezwładniony przez Brygadzistę. Nie posiadając broni, agresor uciekł w głąb statku.

I dokładnie takie nagranie otrzymała Agencja, która wysłała Luminarius na miejsce.

* Szary: Hacker
* klusarz: Biurokrata
* owcen: Sabotażysta
* sidvs: Oficer Naukowy

### Sesja Właściwa - impl

Gdy Luminarius zbliżał się do obu jednostek, dostał sygnał z automatycznej kapsuły ratunkowej. Luminarius podjął kapsułę z Varilen; tam znajduje się delikwent w stazie. Zespół zdecydował się otworzyć kapsułę w bezpiecznym miejscu i wziąć delikwenta, który znajdował się w kapsule na badania przez oficera naukowego. Tymczasem haker połączył się z samą kapsułą by wydobyć jak najwięcej informacji.

Skan kapsuły:

Tp+2:

* X: Wszystko wygląda prawidłowo. Kwiat nie wskazuje na nadmiar magii na zewnątrz kapsuły.

Bezpieczne otwarcie kapsuły. Każdy ma lapisowy naszyjnik. Kapsuła została otwarta, w środku jest jeden smętny koleś. Nie wygląda muskularnie. Kwiat zareagował, ale nie bardzo mocno. Oficer bierze delikwenta do badań. Bierzemy delikwenta na badania - jakie Skażenie, co tu się dzieje.

Tr +3 +3Ob:

* Ob: Oficer Naukowy bada delikwenta i doszło do Manifestacji magicznej.
* Ob: Hacker połączył się z kapsułą by ją zbadać.
* X: Jad antymagiczny ZABIŁ delikwenta, ale masz już pętlę; już zaczyna rosnąć w nieskończoność
* X: Dwóch asystentów jest na Anitikaplan.
* Vr: Delikwent wyleciał poza Luminarius. TO JEST ALTERIS.
* V: 
    * Aktualna sytuacja na statku macierzystym: "Perez". Perez się bał i chciał uciec za wszelką cenę, ogólnie nikt nie wierzy.
    * Perez wyraźnie widział jak ktoś zabija żonę kapitana w zimnej krwi.
* (-1Ob+2Vg) V: 
    * Perez widział nie kapitana Varilen, Perez widział śmierć żony kapitana Anitikaplan.

Niestety w wyniku badań doszło do manifestacji energii magicznej i delikwent imieniem Perez umarł. W jego miejscu pojawił się rozszerzający się portal Alteris; Perez kapsuła oraz Anitikaplan zaczęli być jednym bytem a brzuch Pereza stał się portalem. 2 asystentów wpadło przez portal na Anitikaplan; sabotażystka skutecznie wzięła i wyrzuciła resztki Pereza za śluzę, gnając przez Luminarius.

Tymczasem haker dowiedział się jak wygląda sytuacja na zarówno Anitikaplan jak i Varilen; przez manifestację Alteris haker zhakował wszystkie 3 byty jednocześnie. Perez widział jak ktoś zabija żonę kapitana i wpadł w taki poziomie chaosu i paniki że zdecydował się uciec z Varilen.

W czym gdy biurokratka spojrzała na sytuację, wyszło wyraźnie, że osoba która zginęła była klarkartianką. Kapitan też. A przecież kapitan (i żona) z Varilen to drakolici. Gdy biurokratka spojrzała w głąb posiadanych danych, okazało się że to co Perez widział to były wydarzenia z przeszłości Anitikaplan, z czasów wojny deoriańskiej.

Lecimy na Varilen. Varilen odpowiada na zapytanie. Lecimy promem.

Biurokrata: "był distress call, była kapsuła, nie żyje, wyjaśnień".

Gdy tylko kapitan – Wiktor Worl - dowiedział się o śmierci Pereza, przeszła mu bojowa atmosfera i zaczął współpracować. Powiedział prawdę o Perezie, ale nadal nie do końca chce współpracować z Agencją.

* Perez okradał rzeczy, zwłoki itp.
* Perez to taka gnida, ale pracowita. On robił za trzech, a kradł za 20%, to się opłacał.

Szczęśliwie (nieszczęśliwie?) inny drakolita, Twaróg Worl, z przyjemnością dogryza szefowi. 

Twaróg zaznacza, że gdyby szef zainwestował w dobrą kapsułę to potencjalnie Perez by przeżył (zespół wie, że kapsuła była dobra). Okazuje się, że ogólnie sprzęt jest kiepskiej jakości. Twaróg powiedział że wie gdzie Perez chował fanty i wszyscy byli zdziwieni że nie wziął ich ze sobą gdy uciekł przez kapsułę.

Szukamy czegoś magicznego w skrytce Pereza. Twaróg "z perspektywy obszarów inżynieryjnych co złomowali, grupka Pereza tam poszła (Krypta). 

Oficer naukowy skanuje gdzie są jakieś potencjalnie rzeczy ukradzione przez Pereza, Skażone.

Tr Z +3:

* Vr: znalazłeś - Twaróg jest Skażony. Tu coś było, w tej rurze. Ale tego nie ma. I chyba Twaróg to ma.
    * Twaróg dał Wam taką piękną inskrypcję tygrysa z napisem "podaruj każdemu dziecku szeroki uśmiech".
* Vz: Luminarius skanuje Anitikaplan
    * Anitikaplan ma lekkie sygnatury energii (jakieś reaktory działają)
    * Anitikaplan ma aktywne działa anty-asteroidowe i te działa celują w Varilen
    * Około 20 osób jest na pokładzie Anitikaplan. 2 grupy po 6 osób najpewniej złomują. I jedna grupa 10 osób GDZIEŚ. Skupiona.

To jest ten moment kiedy biurokratka zdecydowała się wskazać historię Anitikaplan, łącznie z tymi aspektami historii których normalni ludzie nie wiedzą. Anitikaplan chciała być jednostką neutralną w Wojnie Deoriańskiej, ale została zaatakowana przez grupę randomowych fanatyków. Nie do końca wiadomo co stało się na Anitikaplan, wiadomo jednak że nikt nie uszedł z życiem.

Czym Anitikaplan była? Był to statek wielkości krążownika, 400+ metrów i 10+ pokładów, jednostka generacyjna (rodziny tam mieszkały) skupiające się na tworzeniem doskonałych bajek i kreskówek dla dzieci, zwłaszcza dookoła maskotki tygryska. Stąd zresztą nazwa jednostki – „Pomnik tygrysa”. Tygrys miał praktycznie kultową reputację wśród dzieci noktiańskich; tragedia Anitikaplan była jednym z smutniejszych punktów wojny.

Na podstawie danych ze strumieni, anomali oraz dedukcji Zespół doszedł do wniosku że mają do czynienia z kombinacją 2 energii: Alteris i Sempitus. 

Problem polega na tym że jeżeli Sempitus pragnie kontynuacji lub powrotu do Wojny Deoriańskiej a Alteris faktycznie zmienia rzeczywistość, mamy sytuację w której przeszłość zaczyna nadpisywać teraźniejszość. I trzeba koniecznie zmniejszyć ilość osób na pokładzie statku. Więc trzeba porozmawiać z kapitanem (który absolutnie nie chce przerwać wydobycia i złomowania, bo go zwyczajnie nie stać).

Kapitan. "Ale... nie mogę przerwać złomowania. Nie stać mnie, jeden dzień bez pracy? To są kontraktorzy."

Co pomogło? Fakt, że cholerny Anitikaplan ma sprawne działo KTÓRE CELUJE W Varilen. I jak dostanie dość energi...

Tr Z +3:

* V: Kapitan pójdzie za planem ewakuacji
* Vr: Kapitan nie będzie szczególnie marudził ani nie będzie robił problemów. Działka nacelowane.
    * on odda ludzi Agencji do badań
    * "K: czyli znajdziecie, tych, morderców?"
    * "Taki mamy zamiar."

Agencja opłaca mu rekomensatę. Koleś jest waszym najlepszym przyjacielem. On BĘDZIE współpracował. 12 osób wróciło, 10+ zostało.

Chcemy dowiedzieć się o co chodzi - co z tą dziesiątką osób i co tu się w ogóle dzieje:

* Wiecie o kombinacji Alteris + Sempitus
* Macie dane ze wszystkich serwopancerzy w zasięgu - możecie przekierować dane też z tych na Anitikaplan, choć zakłócone
* Haker agreguje ze wszystkich źródeł i super-strumień + Oficer Naukowy
* Biurokrata procesuje z perspektywy historii.

Tr Z +4:

* Vz: Mamy strumień, dane są
* Vr: Mamy wyselekcjonowane elementy
* Vr: Mamy sukces

Mamy kilka nietypowych rzeczy. Część jest magiczna a część nie.

* Ludzie, którzy są "tam", oni odtwarzają wojnę deoriańską - oni są "w przeszłości"
* Ludzie, którzy zginęli stworzyli zasilanie. I część statku się odbudowała. Jak w przeszłości.
* Są pewne komponenty statku które są aktywne od dawna - zanim pojawili się złomiarze, są powiązane z Generatorem AI.
* Świadome AI próbują udawać duchy i wystraszyć ludzi żeby stąd poszli.
* Te świadome AI resztką sił odbudowują jedno działo.
* Jeden asystent skończył jako karma dla magii, ma ładne jelita.
* Drugi asystent natomiast trafił do krypty. I tam jest zamrożony. Bo to jakoś jeszcze działa. AI go uratowały.

Mając te konkretne informacje, Sabotażystka ruszyła zniszczyć resztę śladów AI. Niech chociaż jedno zagrożenie, niech chociaż jeden problem zostanie zniszczony raz na zawsze.

Tr Z+3:

* X: AI są czujne i trudno się do nich podkraść
* X: AI mają jedną aktywną maszynę bojową
* V: Sabotażystka ma dane z Luminariusa - wiesz o 1 reaktor i 2 baterie.
* (+2Vg) Vz: Sabotażystka z półobrotu
* Vg: Neutralizacja baterii i TAI zakończona z powodzeniem

Sabotażystka leci na reaktor. "Nie, nie, nie, przepraszamy, prosimy...". Nic to nie dało - wszystkie baterie i reaktor AI zostały zniszczone. Problem zneutralizowany. A więc zostaje nam tylko jeden problem - Źródło Skażenia. Ale gdzie?

Krypta. Krypta reprezentuje starych rysowników, ludzi którzy stworzyli koncept tygryska, wszystko co było piękne i stanowiło o duszy tej jednostki. A jednocześnie zgodnie z danymi ze strumieni, tam doszło do największego zbezczeszczenia z perspektywy fanatyków którzy zaatakowali tą jednostkę – czyli tam mogło dojść do czegoś złego. A fakt że Perez ukradł inskrypcję wziętą z Krypty mogło jedynie rozognić sytuację.

Zespół idzie w stronę Krypty. Im bardziej zbliżacie się, tym bardziej statek jest nowoczesny. Chociaż 'nowoczesny' nie jest właściwym słowem. Coraz bardziej tracą kontakt z Luminariusem, zamiast iść „tam” idą „nie-tam”. Wchodzą w głąb Alteris, w nie-rzeczywistość. Wchodzą nie do krypty jaką ona jest a do krypty jaką ona powinna być zgodnie z ideą ludzi którzy tu mieszkali. Nie-rzeczywistość. Zespół zdecydowanie nie chce wejść z tej strony.

Pojawiły się nowy pomysł. Przebić się do krypty idąc od ‘góry’, od pokładu wyżej. Wycofali się z pętli Alteris, nie idą za energią myśli i uczuć, nie idziecie za 'melodią uczuć ludzi', idziecie 'inną drogą', od góry. 

Tr +3 +3Ob:

* Ob: Teraz KOLOR anomalii się zmienił. Sempitus. Statek zaczyna żywić się servarami
* X: Serwopancerze dołączyły do mechanizmu Wieczności Sempitus (ratowalny Zespół)
* (full power pancerza do palnika) +3Vg: X: (tam był asystent) -> Asystent nie do końca jest tym czym był.
* X: Masa krytyczna energii - cała Anitikaplan zaczyna się przekształcać
* V: Udało się wypalić drogę do Alteris.

Poziom energii magicznej jest za wysoki. Anitikaplan próbowała się obudzić, próbowała powrócić niezależnie od typu energii – czy przez Wojnę czy przez Tygryska i Radość. By móc zregenerować jednostkę, potrzebny jest materiał oraz energia. Tak więc serwopancerze zaczęły gasnąć energetycznie. Zespół przekierował całość energii do wypalania dziury w kierunku na Kryptę i jedynie jedna osoba – Sabotażystka - miała się powstrzymać przed zużywaniem energii. Ona musi wejść do krypty i rozwiązać problem.

To co sabotażystka zastała na miejscu to koszmar. Fanatycy którzy zaatakowali Anitikaplan musieli poczuć się zdradzeni przez statek który był twórcą maskotek ich dzieciństwa tak samo. Doszło tam do rytualnych mordów, bezczeszczenia zarówno zwłok jak i żywych osób oraz największych zbrodni wojennych jakich ci ludzie potrafili sobie wyobrazić.

Alteris przekształciło to w bardzo komiksową, graficzną reprezentację. To spowodowało, że efekt był dość upiorny. I wojna oraz podłe czyny były odtwarzane w pętli w nieskończoność w tamtym miejscu. Tygrysek – strażnik stworzony z asystenta - tylko stał i patrzył. Sabotażystka nie ma łatwego ataku ani możliwości działania.

Tr Z +3 +3Ob, 2X => 2Xm:

* X: Wystrzeliłaś w morderców, dałaś radę nawet kilku zabić, ale po chwili wstali - ZNOWU pętla działa. Tygrys patrzy.
* V: Przytuliła Tygryska. Tygrysek poczuł większą moc. Skierowany przeciw Wojnie.
* X: Tygrysek obrócił się w stronę i zaatakował. Jest za szybki. Czujesz krew w ustach. Przebił przez serwopancerz - ciężko ranna, więc problem.
* X: Gasnę
* (+Vm) Vm: anomalia jest w Krypcie.

Sabotażystka zaczęła od próby walki z ‘nieśmiertelnymi fanatykami’. Ale ich niszczenie nic nie dawało, nic nie powodowało. Po zastanowieniu się, sabotażystka spróbowała wykorzystać aspekt uśmiechu i radości ze strony oryginalnego tygryska. Przytuliła go, przez co dostarczyła mu trochę energii. Skierowała go do walki przeciwko fanatycznym anomaliom. Gdy tygrysek odwrócił uwagę od sabotażystki, spróbowała zestrzelić ścianki krypty by uwolnić znajdujących się tam rysowników i mędrców.

Nieszczęśliwie w samym sercu Alteris tygrysek okazał się być za szybki. Zakwalifikował sabotażystę jako jedną z defilerek. Sabotażystka poczuła krew w ustach - tygrysek przebił ją szponem na wylot. Na podłodze rozpryskało się konfetti. Sabotażystka praktycznie umiera, ale skupia całą swoją energię, całą swoją siłę - by wolą i nadzieją zamknąć anomalię tutaj. Niech to stąd się nie wydostanie.

Magia odpowiedziała na jej krew i na jej żądanie. Anomalia została containowana w Krypcie. To uwolniło resztę zespołu i umożliwiło oddziałowi z Luminariusa uratowanie reszty zespołu oraz Sabotażystki.

Ludzie lecą na ratunek. Tr+3:

* Vr: udało się uratować wyłączone serwopancerze
* X: są pewne straty w populacji
* X: z 20 osób 11 zginęło w operacji ratunkowej

Nikt kto był Dotknięty przez anomalię z Varilena nie przeżył.

Jesteście na Luminariusie. Do kapitana -> wycinamy Kryptę z Anitikaplan.

Tr Z+3:

* Vr: Udało się wyciąć Kryptę z Anitikaplan. Jednosta zaczyna stabilizować się na sensorach.

Luminarius odpala baterię nuklearek by zniszczyć resztkę anomalii...

## Streszczenie

Anitikaplan – legendarna jednostka od bajek z Tygryskiem została zniszczona podczas Wojny Deoriańskiej przez grupę fanatyków. Gdy zostały kupione prawa do zezłomowania Anitikaplan i gdy zaczęły dziać się anomalne rzeczy na pokładzie, Luminarius przybył, by usunąć anomalię. Anitikaplan próbowała przywrócić przeszłość do teraźniejszości; Sempitus i Alteris wspólnie kontynuowały zarówno Wojnę Deoriańską jak i ‘podaruj wszystkim dzieciom radość’. Agencji – mimo dużych strat w załodze – udało się wyczyścić Anitikaplan i zachować sekret magii przed populacją, zgodnie z misją.

## Progresja

* .

## Zasługi

* Klasa Hacker: skutecznie połączył się z kapsułą ratunkową (i pośrednio z umysłem Pereza i Anitikaplan); potem zagregował dane ze wszystkich serwopancerzy i danych statku tworząc superstrumień.
* Klasa Biurokrata: znała historię Anitikaplan i prawidłowo ją wykorzystała do zrozumienia co się tu dzieje i co jest teraźniejszością i co przeszłością; do tego skłoniła kapitana Wiktora Worla do współpracy.
* Klasa Sabotażysta: zniszczyła wolne TAI na pokładzie Anitikaplan a potem skutecznie rozmontowała Anomalię Alteris-Sempitus w Krypcie, czerpiąc z energii magicznej dookoła.
* Klasa Oficer Naukowy: zbadał Pereza i jakkolwiek doszło do Manifestacji, zrozumiał z jakimi energiami ma do czynienia. Stworzył detektory krewetkowe i pomógł w filtracji superstrumienia danych hackera.
* Wiktor Worl: kapitan SN Varilen, bardzo próbuje zezłomować Anitikaplan; jedna z ostatnich szans i okazji by przetrwać biznesowo. Współpracuje z Agencją zwłaszcza po tym jak dostał rekompensatę finansową.
* Twaróg Worl: kompetentny oficer Wiktora, choć go nie lubi (i zmienił imię po zakładzie). Podkrada, jest niskim człowiekiem, ale nie zdradzi szefa. Współpracuje z Agencją by dokopać szefowi.
* SN Varilen: statek salvagerski, z laserem dekombinacyjnym i sporą załogą. Podjął się zezłomowania Anitikaplan; na jego pokładzie jest kilka firm mających za zadanie maksymalizować zasoby i pieniądz.
* SN Anitikaplan: legendarny statek rysowników Tygryska, wagabunda kosmiczny. Podczas Wojny Deoriańskiej napadnięty przez fanatyków którzy go zniszczyli. Teraz - jednostka anomalizująca (Sempitus-Alteris). Zneutralizowany przez Agencję.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Kalmantis

## Czas

* Chronologia: Wojna Deoriańska
* Opóźnienie: 3424
* Dni: 4

