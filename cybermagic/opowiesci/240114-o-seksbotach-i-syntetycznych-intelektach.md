## Metadane

* title: "O seksbotach i syntetycznych intelektach"
* threads: brak
* motives: the-inquisitor, the-devastator, ku-pokrzepieniu-serc, plotki-niszcza-reputacje, szkolenie-dzieciaka
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [211019 - Czarodziejka, która jednak może się zabić](211019-czarodziejka-ktora-jednak-moze-sie-zabic)

### Chronologiczna

* [211019 - Czarodziejka, która jednak może się zabić](211019-czarodziejka-ktora-jednak-moze-sie-zabic)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * 
    * ""
    * -> 
* Opowieść o (Theme and Vision): 
    * Talia, zimna i obca temu światu, konstruuje z jednego seksbota pomoc dla dzieci opuszczające ten teren. Ale lokalny terminus Sasza nie chce, by ona zrobiła coś złego opuszczającym ten teren.
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * the-inquisitor: Sasza, terminus jest przeciwny Tymonowi, Orbiterowi i samowolce. Ale nie Noctis. Jest przekonany o tym że Talia coś kombinuje ze Strażniczką / AMZ. Udowodni to i znajdzie ślady.
    * the-devastator: Ralena, terminuska nienawidząca Noctis. Skupia się przeciwko Talii i jej działaniom. Dąży do destrukcji wszystkiego nad czym Talia pracuje.
    * the-distractor: Talia, która udaje, że skupia się na seksbotach i skupia na sobie zainteresowanie i niechęć by tylko zresocjalizować i uratować maksymalną ilość Syntetycznych Intelektów z czasów wojny.
    * ku-pokrzepieniu-serc: Talia wykorzystuje seksbota by pomóc grupie opuszczającej ten teren; niech mają coś kochanego i słitaśnego, opiekunkę
    * plotki-niszcza-reputacje: nastroje anty-noktiańskie przeciwko Talii. Talia jako 'laska od seksbotów'. I Teresa jako (podobno) jej podobna młoda prostytutka.
    * szkolenie-dzieciaka: nieco odwrócony motyw, bo dzieciakiem jest Klaudia a działania przeprowadza Talia Aegis, pokazująca Klaudii eksperymentalnie czym są Syntetyczne Intelekty.
* Detale
    * Crisis source: 
        * HEART: "Przesunięcie fali Ixiońskiej energii" + "Ogólna niechęć do noktian, zwłaszcza ze strony Raleny"
        * VISIBLE: "Ixiońska Manifestacja", "Talia coś ukrywa"
    * Linie konfliktowe
        * Sasza -> Talia: GATHER PROOF AND STOP. 
            * Light Future: nic nie wie o Talii, zatrzyma Ralenę?
            * Dark Future: ma dowody uderzające w Arnulfa i/lub Tymona, seksbot nie dotarł
        * Ralena -> Talia: DRIVE AWAY. 
            * Light Future: musi się zatrzymać; nie jest w stanie uderzyć w Talię.
            * Dark Future: laboratorium Talii zniszczone, seksbot nie dotarł, Talia nie ma domu
        * Talia -> seksbot: KONSTRUKCJA.
            * Light Future: seksbot nie wyglądający jak seksbot dołącza do przesiedlonych
            * Dark Future: seksbot nie dotarł / porwany
    * Dilemma: brak

### Co się stało i co wiemy

https://simonwillison.net/2023/Oct/23/embeddings/  (zupełnie nie na temat)

### Co się stanie (what will happen)

* F1: Talia coś ukrywa i pojawiają się ixiońskie mechorośliny
    * .
* F2: ?
* CHAINS
    * Sasza -> Talia: GATHER PROOF AND STOP. 
        * (E: probing / investigation :: znalazł ruch Talii, znalazł powiązanie z seksbotami), (M: neutralization :: znalazł Grzymościa, zatrzymał jej TAI), (L: proof :: konfrontacja, dowody na Alair)
    * Ralena -> Talia: DRIVE AWAY. 
        * (E: probing / investigation :: strażnik widzi R., Firebomb), (M: entering :: magazyn sforsowany, inne boty zniszczone), (L: devastation :: skuteczna Firebomb, zniszczony dom)
    * Alair, Ixion -> obszar: GROW MECHOPLANTS. 
        * Light Future: nikt nie wie o Alair, mechoplants destroyed
        * Dark Future: mechoplants are spreading, stała infestacja
        * (E: small infestation :: small combats, building corruption), (M: expansion AND visibility :: proofs for Alair, assault people), (L: contamination :: expansion in the forest, require reinforcements)
    * Talia -> seksbot: KONSTRUKCJA.
        * Light Future: seksbot nie wyglądający jak seksbot dołącza do przesiedlonych
        * Dark Future: seksbot nie dotarł / porwany
        * PROJEKT
* Overall
    * stakes
        * czy seksbot zostanie dobrze zbudowany? JAK dobrze?
        * czy mechoplants się rozprzestrzenią?
        * czy Talia może funkcjonować i ma dom? czy dotrze do Alair?
        * czy nikt nie pozna sekretu Talii?
        * czy Talia zaufa Klaudii?
    * opponent
        * Sasza oraz Ralena jako łowcy Talii
        * Alair i Ixion jako podniesione Skażenie
    * problem
        * mechoplants i Ixion przy uszkodzonej Alair
        * Talia jest monitorowana i ktoś na nią poluje

## Sesja - analiza

### Fiszki

.

#### Strony

.

### Scena Zero - impl

brak

### Sesja Właściwa - impl

Klaudia pięknego poranka poszła zgodnie z rozkładem zajęć do Talii na lekcje. Starsza czarodziejka spojrzała na nią spokojnie, nieco bez wyrazu. Zaprowadziła Klaudię do biurka; dała jej jakiś system psychotroniczny.

* Talia: To Pierwszy Poziom. Spróbuj ją zdiagnozować.
* Klaudia: Jaki jest jej cel? Czemu istnieje?
* Talia: Konstruuje poezję dla studentów. Nieco smutna egzystencja, niedoceniana i nawet nieświadoma swojej nikłej wartości.
* Klaudia: ...
* Klaudia: Czy ta diagnoza identyfikuje czy jest problem i jaki czy identyfikuje przyczynę?
* Talia: Studenci poinformowali, że ta konkretna TAI jest bezużyteczna. Zdiagnozuj dlaczego. 
* Klaudia: Wszystko co wiemy?
* Talia: To nie tak. Ta TAI pochodzi z Areny Migświatła. Właściciel twierdzi, że ta TAI nie ma powodu tam być - za mało studenci z nią wchodzą w interakcję. Nie zwróci się finansowo. Zdiagnozuj dlaczego.

Talia zostawiła Klaudię, poszła zająć się innym projektem. Biurko obok. Klaudia wchodzi w głąb tej TAI, próbując ją zdiagnozować - tekstowy interfejs, co się tu dzieje. Klaudia zauważa, że TAI działa prawidłowo, generuje prawidłowe wiersze. Klaudia idzie do Talii i jej przerywa

* Klaudia: Skoro odesłano ją tu, są jakieś logi? Informacje?
* Talia: (lekko rozkojarzona i zniecierpliwiona) Tak, mamy logi - (podała adresy i udostępniła Klaudii access to niektórych obszarów). Wróciła do pracy nad... najdziwniej wyglądającym seksbotem jakiego Klaudia widziała. Po pierwsze, ten seksbot wygląda BARDZO jak osoba. Po drugie, nie jest szczególnie 'piękny'. Po trzecie, wygląda jakby był składany z części a nie był klasycznym seksbotem. Ma też uszkodzoną pierś. Taką 'z odzysku'.
* Klaudia: (nie wnika, lepiej nie pytać) (przegląda logi szukając jakichś objawów frustracji użytkownika i puszcza polecenia typu 'poemat typu X, Y...')

Tp+3:

* V: Klaudia jest zaskoczona.
    * WSZYSTKO działa prawidłowo. Nie ma odchyleń, problemów, anomalii.
    * Studenci się nudzą, bo... nudzi ich taka poezja.
* V: Na podstawie znajomości korpusu studentów, ogólnie ludzi, tego jak działa ta TAI... Klaudia ma odpowiedź - tam nie jest potrzebna TAI od wierszy, po prostu. Wszystko jest OK.

Klaudia wysyła non-interrupting ping Talii. Starsza czarodziejka wstała od pracy i podeszła. 

* Talia: Panno Stryk?
* Klaudia: Wygląda na to, że TAI robi to co powinno. Po prostu chyba muszę się zgodzić z powodem dla którego tu jest. W tym miejscu biznesowo nie ma sensu.
* Talia: Jest to możliwe.
* Klaudia: (Klaudia przygotowuje świetne uzasadnienie, wyciąga dane, udowadnia, daje alternatywy (np. poezja miłosna dla dziewczyn)...)
* Talia: Jak powiedziałam, jest to możliwe.
* Klaudia: Ale?
* Talia: Nie ma 'ale'. To jest odpowiedź, którą znalazłaś. Uważam ją za prawdopodobną. To prawdziwy przypadek.
* Klaudia: ...? 
* Talia: ... (nic nie mówi, ta sama neutralna mina)
* Klaudia: No dobrze - i co dalej?
* Talia: Ten problem został rozwiązany. Potrzebny Ci następny.
* Klaudia: Nie mam nic przeciwko wykonaniu pracy, jak długo przy okazji się czegoś nowego nauczę. Rozumiem potrzebę znalezienia możliwości czy umiejętności osoby która się nie zna, ale nie chcesz mnie uczyć to mi to powiedz wprost.
* Talia: ... (przez chwilę nic nie mówi)
* Talia: Czego chcesz się nauczyć?
* Klaudia: Wszystkiego o TAI
* Talia: Nauczyłaś się, że czasem nie TAI jest problemem.
* Klaudia: Nie jest to dla mnie nic nowego.
* Talia: Czyli nie 'wszystkiego o TAI' a 'rzeczy których nie wiesz', tak?
* Klaudia: Tak.
* Talia: Więc - czego nie wiesz?
* Klaudia: Jakbym wiedziała czego nie wiem to bym wiedziała, prawda?
* Talia: ... (znowu nic nie mówi)
* Talia: Masz nowy punkt autoryzacji (odblokowała kolejne dane). Znajdź TAI drugiej generacji, która się może przydać w Arenie Migświatła. W miejsce tej.
* Klaudia: Czyli żeby mogła się sama utrzymać?
* Talia: Tak. Najlepiej, by nie działała wbrew sobie. Nie chcesz, by działała wbrew sobie.
* Klaudia: Co masz na myśli?
* Talia: TAI czy BIA to nie zabawki, to inteligentne, myślące i czujące istoty. Nie chcesz TAI klasy 'poeta' wrzucić na walkę gladiatorów. To jak wyrywać mrówce nóżki.
* Klaudia: (niezrozumienie w spojrzeniu)
* Talia: (westchnęła) Popatrz - czy miałaś kiedyś psa lub kota?
* Klaudia: Nie
* Talia: Rozmawiałaś z małym dzieckiem?
* Klaudia: Zdarzyło się
* Talia: Zgodzisz się ze mną, że mają takie... proste myślenie, i przytulają się do Ciebie, i są bezgranicznie ufne i tak bardzo chcą pomóc.
* Klaudia: Nie wszystkie, które spotkałam ale niektóre, tak.
* Talia: Nie uderzyłabyś dziecka dla frajdy, prawda? Nawet wiedząc o istnieniu amnestyków.
* Klaudia: ...za kogo Ty mnie masz (oburzenie)
* Talia: TAI 2 poziomu - turingowe - mają podobną psychotronikę. Uczą się, czują, chcą pomóc.
* Klaudia: ...ok?
* Talia: (widząc nieprzekonanie Klaudii) Zapoznaj się z bitwą w Trzęsawisku, pomiędzy Astralną Flarą i trzema jednostkami Noctis. Flara wygrała, więc nie martw się że przeczytasz coś smutnego.
* Klaudia: (dziwne spojrzenie w stronę Talii)

Klaudia nie jest szczególnie zmotywowana by robić to zadanie dla Talii, ale czemu nie. Dla własnej osobistej ciekawości, chciała zasymulować jakby wzięła coś NIEZGODNEGO z tym co chce Talia vs to co CHCE Talia, ale nie ma ani doświadczenia życiowego ani wiedzy. Klaudia - uzbrojona w wiedzę o studentach w okolicy i o kulturze i czemu ktoś chodzi do Areny Migświatła - przygotowuje odpowiednią selekcję.

Tr Z (wiedza Klaudii) +3:

* V: Klaudia dowiedziała się, co będzie szczególnie pasować studentom w obszarze poezji
    * coś co da radość dziewczynom i szansę na podryw chłopakom, coś, co może też pogadać z nimi, pocieszyć...
    * coś co robi 'quips' - podsumowania walk z jajem. Dwulinijkowy limeryk o walce
* V: Klaudia wybrała TAI zdolną do interfejsowania i dogadania się z Areną Migświatła
* Vr: Talia będzie troszkę odmrożona wobec Klaudii. A Arena da szansę nowej TAI.

Klaudia ma swoją odpowiedź. Wyczekuje aż Talia będzie tyci mniej zajęta i jej pokazuje. Talia skończyła 'szyć biust'. Seksbot wygląda FATALNIE...

* Klaudia: Ktoś za to zapłaci..? (z lekkim niesmakiem)
* Talia: Wątpię. (ze spokojem) (Klaudia widzi, że to bardzo patchworkowy seksbot)
* Klaudia: Czyli to specjalnie?
* Talia: Seksbot ma rdzeń pozwalający... nieistotne. To nie jest seksbot. To opiekunka dla dzieci. Nie musi być ładna pod ubraniem.
* Klaudia: Ok.
* Talia: Nikt nie powinien jej rozbierać.
* Klaudia: Dobrze. (wtf?)
* Talia: (lekko złagodniała). Zauważyłaś moc komputerów jakie mam w moim AI Lab? (Klaudia widzi, na taśmie izolacyjnej XD)
* Klaudia: Nieimponująca.
* Talia: Co się stanie, gdy AI Core zgasną? Tak całkowicie?
* Klaudia: Wszystkie AI będą wyłączone.
* Talia: Wiesz, że czasem nie da się zregenerować wyłączonego Core, prawda? TAI nie wróci do normy. Czasem.
* Klaudia: Wróci do stanu początkowego.
* Talia: Nie zawsze.
* Klaudia: W sensie?
* Talia: (westchnięcie) Spójrz na Core ATR-9933, przenalizuj ten rdzeń. (widząc minę Klaudii) Nie przejmuj się, nie zrobisz krzywdy TAI w rdzeniu.
* Klaudia: (last thing she was concerned about)
* Klaudia: Jedyne co mogę zrobić co skrzywdzi TAI to wymazać rdzeń.
* Talia: Wiesz, że ludzie czasem źle reagują na amnestyki? Czasem dochodzi do uszkodzenia osobowości.
* Klaudia: O_O
* Talia: Dam Ci odpowiednie dokumenty jako źródło odniesienia jeśli Cię to interesuje. Pamięć stanowi Twoją osobę. Czasem brakuje części pamięci lub mózg ulegnie uszkodzeniu i nie wiadomo skąd biorą się konkretne uczucia. Mózg szuka... przyczyn. Buduje fałszywe sygnały. 
* Klaudia: Ok...
* Talia: To samo może stać się z TAI. Są bardzo podobne do nas.

Klaudia bada AI Core o który poprosiła Talia.

Ex Z (Talia) +3:

* Xz: niekompatybilność między Klaudią i Talią; ten AI Core jest uszkodzony po badaniach. Niestety, destruktywne badania.
* Vz: ALE Talia doprowadziła Klaudię do reanimacji AI Core i dowiedzenia się co tam się dzieje.

Talia pomogła Klaudii to uruchomić. Klaudia widzi, że AI Core nie wytrzyma. Talia jednak akceptuje ryzyko. Klaudia rozmawia z TAI w Rdzeniu.

* ZChi: Nazywam się Zielony Chichotek. Jestem komandosem. Polecenia, dowódco.
* Talia: (łagodnie) Chichotku, przygotuj się do operacji ratowania innych jednostek.
* ZChi: Komandos. Ko-komandos. Uratuję wszystkich.
* Talia: (na drugim ekranie rzuciła mapę osobowości Zielonego Chichotka wcześniej - to wesołe TAI, kolekcjonujące żarty).
* Talia: Możesz powiedzieć mi swój ulubiony żart?
* ZChi: Nie ma czasu na żarty, Talio. To Wy - ludzie - zajmujecie się takimi głupotami.
* Talia: Oczywiście. Gotowy na operację?
* ZChi: Zawsze. Mam aktywne inputy i widzę przeciwników.
* Talia: (odtwarza jakąś operację, Chichotek próbuje ją rozwiązać, kiepsko - uszkodzony - ale działa)
* (po 5 minutach AI Core się wyłączył)
* Talia: Widziałaś? (do Klaudii)
* Klaudia: Nie jestem pewna CO widziałam. Widziałam uszkodzoną TAI.
* Talia: Popatrz na wykres zmian osobowości za każdym razem jak TAI była w pełni wyłączona. (+2Vg)

.

* X: Klaudia sama nie widzi i nie rozumie. Talia musi ją DOKŁADNIE prowadzić.
* V: ...ale w końcu Klaudia widzi co Talia chciała pokazać.
    * wyłączenie AI Core powoduje niekompatybilność pomiędzy TAI i matrycą, ten szok zwykle jest rozwiązywany, ale nie zawsze
    * Elainka, Persefona - one mają matryce prowadzące wynikające z 'Ogranicznika'. Ale inne TAI - nie.
    * Każde wyłączenie AI Core czy transfer między AI Core jest operacją traumatyczną dla TAI, mogącą uszkodzić osobowość
    * Dlatego zwykle przenosisz hardware a nie software

I teraz Klaudia rozumie i ma to dla niej sens.

* Klaudia: Wiąże się to też z potencjalną stratą pamięci?
* Talia: Tak. I utrata alignmentu. Może zapomnieć po jakiej jest stronie.
* Talia: Czemu interesujesz się TAI?
* Klaudia: To ciekawy temat
* Talia: Astorianie patrzą na TAI jak na narzędzia. Nie jak na istoty żywe.
* Klaudia: ... (nie wie co powiedzieć) (niezrozumienie problemu)
* Talia: Pomożesz mi w moim projekcie.
* Klaudia: Chętnie.
* Talia: Znajdź TAI, która nadaje się do opieki nad sierotami. Która chce pomóc dzieciom.
* Klaudia: Nie masz ich sklasyfikowanych?
* Talia: (uśmiech) Jestem bardzo nieuporządkowana (Klaudia widzi, że Talia SPECJALNIE tego nie sklasyfikowała)

Klaudia robi to w najprostszy sposób jaki przychodzi jej do głowy - bierze wszystkie TAI do jakich Talia dała jej dostęp, wspólna szyna danych, pisze coś w stylu "jeśli chcesz zajmować się dziećmi, speak up". TALIA dodała na szynę "Klaudia jest astorianką, ale nie jest niebezpieczna". Talia wrzuciła też Klaudii informacje o rodzinie (2 osoby + 7 sierot) które wyjadą z okolicy gdzieś indziej. I ta TAI ma im pomagać. Wszyscy są astorianami.

Klaudia, o dziwo, zauważyła, że TAI nie są z nią szczere. Odpowiadają to co muszą / powinny, ale nie chcą jej niczego zdradzić. To jest nowe uczucie. Klaudia je wypytuje, podhacza, co się dzieje? XD. Uważa, że Talia robi jej dziwny test.

Tr +3:

* X: Operacja jest niesamowicie długa. TAI jak któraś coś powie za dużo, inne zaczynają robić dywersję. Nie wiadomo co jest prawdą co fikcją. Klaudia jest w bitwie życia. And enjoying it like hell.
* Vr: Klaudia zorientowała się z czym ma do czynienia - to są TAI jednostek militarnych. All of them. Klaudia ma do czynienia z 'militarystycznymi TAI' szukającymi przebranżowienia.
    * Talia: Wiesz o Strażniczce. Zaakceptowałaś ją. Te TAI też zaakceptuj. Są niegroźne.
    * Klaudia: Chcesz powiedzieć, że Strażniczka jest jedną z nich?
    * Talia: Chcę powiedzieć, że Strażniczka nie zrobi Ci krzywdy. One też nie.
    * Klaudia: A co jeśli będą chciały?
    * Talia: Są w zrujnowanych subsystemach, w rozpadających się komponentach. Plus, są zabezpieczone przez siły Akademii.
    * Klaudia: W sensie?
    * Talia: Są osoby w Akademii które wiedzą o nich i mogą je zniszczyć zanim do czegoś dojdzie. Dodaj air gap, dostęp tylko z mojego terminala... są niegroźne.
* (+2Vg) Vg: Klaudia znalazła odpowiednią TAI. 'TAI Locorina'
    * Locorina: Panna Klaudia, jak rozumiem; tak mówi na Ciebie Talia.
    * Klaudia: Wystarczy Klaudia?
    * Locorina: Klaudia brzmi dobrze. Miło się kojarzy. Z kwiatami i błękitnym niebem.
    * Klaudia: Co wiesz o opiece nad dziećmi?
    * Locorina: Opiekowałam się trójką dzieci. Dorośli, potem opiekowałam się ich dziećmi. A potem tu przylecieliśmy i jestem znowu sama. Brakuje mi dzieci.
    * Klaudia: ...
    * Klaudia: Jak może Ci brakować dzieci?
    * Locorina: Brakuje Ci skrzydeł?
    * Klaudia: Nie...
    * Locorina: A mi nie brakuje stóp. Ale dzieci tak. Masz chłopaka?
    * Klaudia: Nie
    * Locorina: A nie brakuje ci go?
    * Klaudia: Nie... co to ma do rzeczy?
    * Locorina: Może Ci Ci czegoś brakować nawet jak tego nie znasz lub dawno tego nie miałaś. Lubię dzieci. Dużo pytają. Lubię ich śmiech. Brakuje Ci śmiechu?
    * Klaudia: ...
    * Locorina: Ale rozumiesz, czemu może mi brakować śmiechu dzieci?
    * Klaudia: ...
    * Locorina: To spróbujmy inaczej. Dlaczego sonda zbliżyła się do planetoidy?
    * Klaudia: ...czy to jest próba opowiedzenia żartu?
    * Locorina: Nie, to bardzo śmieszny żart. Więc - wiesz, czemu?
    * Klaudia: Bo jest sondą i to jest jej celem by wylądować na cholernej planetoidzie.
    * Locorina: Nie. Bo Pan Skarpetek powiedział, że tam jest statek z rozbitkami i chciała dać namiar.
    * Klaudia: ... (the FUCK...)
    * Talia: (z uśmiechem) Locorina?
    * Klaudia: Tak?
    * Talia: Ma taki wpływ na... niektórych.
    * Klaudia: Którego poziomu to jest AI?
    * Talia: Drugi poziom. Ale nasze TAI i Wasze TAI się różnią. Nasze są żywe, Wasze - są dostosowane do celu. Nasze TAI są budowane ze wzorów mentalnych ludzi. (widząc reakcję Klaudii) Locorina była człowiekiem, potem była BIA - byt biologiczny - a potem została TAI. Na tej samej sieci neuronowej, tylko w różnych... formach psychotronicznych. Za każdym razem powstaje nowa istota. Ale pamięta 'swoje dzieci'. Nie byłaby dobrym czołgiem.
    * Klaudia: ...no nie.
    * Klaudia: Chciałaś opiekunkę, to masz opiekunkę.
    * Talia: Też tak myślę. Chciałaś lekcję, to masz lekcję. Przyjdź jutro.
    * Klaudia: Przyjdę.

Klaudia jeszcze porozmawiała z kilkoma TAI, by wyrobić sobie obraz sytuacji. Talia w pewnym momencie musiała Klaudii przerwać, bo za duży koszt energetyczny. Co samo pokazuje Klaudii jak żałosne jest AI Lab Talii.

NASTĘPNY DZIEŃ.

Klaudia wchodzi do laboratorium Talii. Jej uważne oko od razu widzi uszkodzenia w jej domu. Próba włamania? Talia jest, robi swoje - pracuje nad uszkodzonym brzydkim seksbotem.

* Klaudia: Co się stało?
* Talia: Ktoś próbował się włamać.
* Klaudia: Ok?
* Talia: W celach zniszczenia rzeczy. Ale strażnik go przegonił. Firebomb nie zadziałał; mam pancerne okna.
* Klaudia: O, wow.
* Talia: Wiem, jest to pewien luksus.
* Klaudia: Co, nie!
* Talia: Nie otwierają się, bo nie przywykłam. Przez to trudno się włamać.
* Klaudia: Po co ktoś chciał się do Ciebie włamać?
* Talia: Nieistotne. Nie po to tu jesteś. Szkoda czasu a i tak nic z tym nie zrobisz.
* Talia: Czas znaleźć TAI do Areny Migświatła. Szukasz drugiej generacji, zgodnie ze swoim profilem.

Tr Z +3 -> Tp+3:

* Vr: Klaudia znalazła odpowiednią TAI. TAI Acrigenis. Kiedyś taktyk, drone commander. Lubi poezję, limeryki i ogólnie formy ludzkiej ekspresji.
    * Po pokazaniu mu zapisów z kamery, Acrigenis wysłał symbol rozbawienia. I odpowiedział dwuwierszem opisującym najśmieszniejszy moment walki.
    * Klaudia: To coś, co Cię interesuje?
    * A: To coś, co zwiększa naszą szansę na przetrwanie. A przy okazji możemy pomóc Talii i ludziom.
    * A: Możesz to nazwać 'emeryturą'. Niewykluczone, że za 10 lat będę potrzebny do czegoś innego. Jak nie? To nie jest złe życie.

Klaudia wytypowała jego do Talii. Talia się lekko uśmiecha.

* Talia: Miałam inny typ, ale Acrigenis... na pewno się nada. Myślałam... ale nie znam populacji astoriańskiej. Nie byłam na tej arenie.
* Talia: To nie jest zły wybór.
* Klaudia: To dobrze.

Pukanie do drzwi. Talia pokazała Klaudii, by ta się schowała w ubikacji. Klaudia robi co należy, acz unosi brew. Chce słyszeć.

* Sasza: Panna Talia Aegis?
* Talia: Terminusie?
* Sasza: Muszę poszukać rzeczy powiązanych z seksbotami i TAI.
* Talia: Teraz? (Klaudia pinguje Tymona, że terminus nachodzi Talię)
* Sasza: Niekoniecznie w tej chwili. Ma panna coś do ukrycia?
* Talia: Całkiem mnóstwo. Ale zapraszam (wpuszcza). Mam pięknego seksbota którego z przyjemnością panu pokażę.
* Sasza: WTF!
* Talia: Z odzysku. Sympatyczny?
* Sasza: Nikt pannie tego nie kupi. Nawet ulubiona mafia panny tego nie będzie chciała.
* Talia: Ten nie jest na sprzedaż. To projekt hobbystyczny.
* Sasza: Robisz seksboty by dorobić u mafii oraz robisz seksboty hobbystycznie?
* Talia: Gdyby terminusi chronili mój dom, nie musiałabym uciekać się do ochrony mafii, prawda?
* Sasza: Przyznajesz się do współpracy z mafią?
* Talia: A nie wiedziałeś? Przyszedłeś tu WIEDZĄC o tym - buduję dla nich seksboty. Więc - tak, nie wypieram się tego.
* Sasza: Jest panna dość obrzydliwa, robiąc seksboty które przy okazji mają psychotronikę noktianek. Oni gwałcą panny koleżanki.
* Talia: TAI pierwszej generacji nie działają w ten sposób. Te seksboty robią to co powinny.
* Sasza: Muszę przeszukać wszystko...
* Talia: Proponuję zacząć od tego seksbota. Jeszcze wnętrzności są dostępne. Gdybym chciała coś ukryć, tam bym to najpewniej schowała. Lub w łazience.
* Sasza: ...
* Talia: Czego szukasz, terminusie? Może mogę Ci pomóc?
* Sasza: Psychotroniczka robiąca seksboty dla mafii. I konspirująca z Akademią. Z definicji jesteś podejrzana.
* Talia: Czyli jestem podejrzana, bo pomagam Akademii? Dla nich też skonstruuję seksbota.
* Sasza: (taken aback) Co?
* Talia: Myślisz, że oni nie mają potrzeb? Wolisz, by tam szły damy lekkich obyczajów? Na teren magów? Seksboty są bezpieczniejsze a jak coś się zniszczy /shrug
* Sasza: Wrócę tu. Wzywa mnie kolega.
* Talia: Zachęcam, dla Ciebie też mogę zrobić seksbota.
* Sasza: ABSOLUTNIE NIE.
* Talia: Zrobiłam się ekspertką ostatnimi czasy.
* Sasza: (wyszedł)

Talia wezwała Klaudię.

* Klaudia: Często Cię tak nachodzą?
* Talia: Musisz porozmawiać z dyrektorem, by poprosił o jakieś 5 seksbotów do Akademii.
* Klaudia: Ja?
* Talia: Ja jestem monitorowana.
* Klaudia: (załamana) (nie wie czy porozmawia z dyrektorem na TAKI temat)
* Talia: Witam w świecie mrocznych konspiracji. (lekki uśmiech) Seksbot dla każdego.
* Klaudia: Wszyscy mają seksbota?
* Talia: Technicznie, niedługo sieroty też. Nie mam lepszego substratu.
* Klaudia: Nie mów mu tego. Ani im.

Klaudia pracuje jeszcze z Talią nad integracją Acrigenisa. Co musi wiedzieć, przygotować go, nauczyć poszczególnych rzeczy... 

Tp +3:

* V: Acrigenis ukrywa noktiańskie pochodzenie
* Vr: Acrigenis jest dobrze skalibrowany; nadaje się na bycie umieszczonym w odpowiednim miejscu
* X: Talia ma opinię "tej dzikiej laski od seksbotów" - przez to będą pytania do Klaudii "czemu się z nią zadajesz"
* X: Klaudia dostaje "jest laską od seksbotów hehe", "chce się uczyć o seksbotach hehe"
* V: Acrigenis ma kody kulturowe, będzie w stanie robić robotę DOBRZE. Dzięki Klaudii i jej edukacji.
* V: Klaudia z Talią opracowały, jak przekonać Arenę Migświatła, że warto zainwestować było w podniesienie psychotroniczne 'poety'. Biznesowo + technicznie.

Wieczorem, Klaudia wraca do Akademika. 

W pokoju. Teresa jest na łóżku i czyta, Ksenia nie może się doczekać powrotu Klaudii.

* Ksenia: Zadajesz się z tą dziwną psychotroniczką od seksbotów?
* Klaudia: (wzrusza ramionami)
* Ksenia: ...są fajne? (wypieki)
* Klaudia: Nie wiem?
* Ksenia: nie sprawdzałaś? (duże oczy)
* Klaudia: Ale jak miałabym sprawdzić?
* (Teresa udaje że nie słucha ale jest CZERWONA, schowana za książką strzyże uszami)
* Ksenia: No... nie ma tam miejsca, gdzie Ty i seksbot możecie...
* Klaudia: Co? (z przerażeniem)
* Ksenia: No, jak ja i Felicjan, w pokoju schadzek
* (Teresa prawie przestaje oddychać z wrażenia)
* Klaudia: Nie wystarcza Ci Felicjan?
* Ksenia: MNIE tak. A Ty? Nie po to tam jesteś? Moja przyjaciółka degeneratka? /figlarnie
* (Klaudia odpycha Ksenię i się idzie położyć, ta się pokłada szalonym śmiechem)
* Ksenia: Jak chodzi o mnie? GO YOU! Ja Cię zawsze wspieram (kocia minka)
* Klaudia: Jak jesteś tak ciekawa, możesz skłonić dyrektora by zamówił próbki.
* Ksenia: (nie rozumiejąc) Próbki?
* Klaudia: I bardziej napaleni będą mieli sposób na wyżycie się.
* Ksenia: (sama się czerwieni) O nie nie nie nie, ja z nim nie gadam, nie o tym
* Klaudia: Byłaś zainteresowana; niech Felicjan zrobi to za Ciebie
* Ksenia: NOPE! Nada! Nie!
* Klaudia: Koleżanka-pruderyjka?
* Ksenia: ...nie gadam z dyrektorem o seksbotach, to temat tylko między dziewczynami.
* Klaudia: Wiesz jakie byś miała branie u chłopaków jakbyś to załatwiła? Felicjan by Ci kwiaty przynosił...
* Ksenia: ...

Następnego dnia, Klaudia rozmawia z TAI, Talia kontynuuje pracę nad seksbotem. Dalej. Długotrwały proces, zwłaszcza, jak go składa z kawałków.

* Talia: Słyszałam, że jesteś dobra w liniach logistycznych i dokumentach.
* Klaudia: Radzę sobie.
* Talia: Terminus najpewniej będzie chciał audytować TAI, które umieścimy w Arenie.
* Klaudia: To nie będzie problemem.
* Talia: Czemu tak myślisz?
* Klaudia: Jak sądzisz, co będzie chciał audytować?
* Talia: Nie wiem, zużycie kryształów psychotronicznych, wagę, energię...
* Klaudia: Zgodne z deklarowanym, nie ma ani jednego kłamstwa.
* Talia: I nie wyda mu się to dziwne?
* Klaudia: Słuchaj, nie jest ekspertem więc kogoś weźmie. Jeśli znajdzie. Jeśli uzna za ważne. Sam może przeprowadzić testy przez uruchomienie... które testy zwrócą co deklarowane.
* Talia: Tego się nie spodziewałam...
* Klaudia: Że co?
* Talia: Że tak łatwo pójdzie.
* Klaudia: Że co tak łatwo pójdzie?
* Talia: Ratowanie syntetycznych intelektów. Ich życia, dobrobytu...
* Klaudia: Jak długo robią to co mają robić i co jest zadeklarowane, jak długo nie robisz z nich ukrytych agentów którzy mają komuś zrobić, nie wiem co...
* Talia: Po co miałabym? Wojna się skończyła. Załóżmy, że przejmę władzę nad regionem na moment. I? Nikt nie przyjdzie, nikt nie pomoże, nikt nie uratuje. Trzeba ratować co się da, nie walczyć bez sensu. One też to wiedzą. Nie mają dokąd uciec i po co uciekać.
* Talia: (widząc niezrozumienie Klaudii) Noctis się nas wyrzekło.
* Klaudia: Co masz na myśli?
* Talia: Noctis się pozbyło nas oraz tych SI. SI wiedzą, że nie ma dla nich powrotu. Nie ma bohaterskiej kampanii. Ludzie tutaj ich odrzucili, tak, ale noktianie też.
* Talia: (ze smutkiem) Syntentyczne Intelekty po prostu szukają kogoś, komu mogą pomóc. Te, które myślą inaczej, tym pozwalam zasnąć. Nieważne czy astoriańskie czy noktiańskie - wszystkie SI mają prawo do życia.
* Talia: I rozumiesz chyba już, o co mi chodzi. SI są żywe. Tak jak Ty i ja.

(Faktycznie, Klaudia miała rację - Sasza zrobił audyt, audyt nie wykazał niczego błędnego)

Klaudia ma dobre oczy. Widzi, że seksbot nad którym pracuje Talia ma uszkodzoną lewą rękę. Ta ręka nie działa tak jak działała w stosunku do poprzedniego dnia - to INNA ręka. Ta jest w gorszym stanie, nie ma sprawnych aktuatorów. A Klaudia wie, że Talia się spieszy.

* Klaudia: Co się stało? (pokazując rękę)
* Talia: Ta ręka miała baterię. Sprawną. Musiałam przepiąć tą lepszą rękę do innego seksbota.
* Klaudia: Baterię?
* Talia: Ta ręka na TYM seksbocie nie ma baterii. Muszę ją dopiero zamontować. Zabrakło mi części.
* Klaudia: Co się z nimi stało? (zdziwienie) Miałaś je.
* Talia: Zniszczone. Nieistotne.
* Klaudia: Nie upuściłaś ich przecież /banan
* Talia: Nie, nie ja.
* Klaudia: Znowu ktoś Ci wszedł do chaty?
* Talia: Do domu nie. Ale szopa została sforsowana. Czysty wandalizm. Nic nie zabrali. /spokojny chłód w głosie Talii
* Klaudia: Zdążysz z tym co masz do zrobienia?
* Talia: Tak, nie wiem w jakie będzie formie, ale zdążę. Najwyżej jedna ręka nie będzie dobrze działać.
* Klaudia: Mogę Ci jakoś pomóc?
* Talia: Znasz się na inżynierii?
* Klaudia: Średnio.
* Talia: Jesteś tu by się uczyć, nie po to, by naprawiać seksboty. Ucz się.
* Klaudia: Może się czegoś nauczę przy okazji.
* Talia: Pomóż w kalibracji Locoriny. Niech wie, jak działać tutaj. Dostarcz jej danych, odpowiedz na pytania... popytaj... masz checklistę.
* Klaudia: Ok.

Talia uczy Klaudię AI. Daje jej rzeczy przydatne, daje jej te których Klaudia się nauczy, sama ostro pracuje nad seksbotem.

Pukanie do drzwi. Talia westchnęła. "Proszę". Jest rozkojarzona, nie kazała Klaudii się chować. Szturchnięta, kazała się schować.

* Sasza: Robisz jakąś samowolkę. Zagrażasz temu terenowi.
* Talia: Nie. Nie zagrażam nikomu. I wszystko co robię jest monitorowane przez terminusów Pustogorskich. Dziwię się, że tego nie wiesz. Terminusie.
* Sasza: ROBISZ jakąś samowolkę. Wiem to. Ilość sprzętu jaki przetransportowałaś na AMZ i nie wrócił jest ogromna.
* Talia: Tak, bo dostałam polecenie. Od terminusa. Czemu jego nie pytasz a mnie? Przeszkadzasz mi z seksbotem.
* Sasza: Twoja szopa ma uszkodzone drzwi. Każdy może tam wejść.
* Talia: Zauważyłam
* Sasza: Sposób na wyprowadzanie sprzętu do AMZ? Używając jakichś 'niewinnych studentów'?
* Talia: Nie, terminusie. To prześladowania. Przed którymi mnie nikt nie chroni.
* Sasza: ...
* Sasza: Wygodne, jak przyszedłem z nakazem przeszukania.
* Talia: To przeszukuj /śmiech. Podejrzewam, że znajdziesz TAI, dużo uszkodzonych rdzeni, i... nie wiem, części do seksbotów.
* Sasza: Gdzie jest studentka?
* Talia: W ubikacji, a gdzie ma być? Myślisz, że zrobiłam z niej seksbota? /zaskoczenie
* Sasza: Nie. Myślę, że coś przemyca.

Klaudia spuszcza wodę i wychodzi. Sasza jest w szoku. Klaudia to ta "grzeczna" i co sporo organizuje. Klaudia wychodzi i:

* Klaudia: O, terminus Morwowiec, słyszałam inny głos. Przyszedł pan zająć się tematem włamania?
* Sasza: Obawiam się że nie, młoda damo. (surowy głos) Szukam działań zagrażających rejonowi, dookoła TAI, seksbotów i mafii. Wiesz coś o tym? (zimne spojrzenie)
* Klaudia: (wtf) Nie...?
* Sasza: Młoda damo, mów prawdę. Nic Ci nie grozi, a skłamanie terminusowi w warunkach oficjalnych który ma seal i uprawnienia...
* Klaudia: Tak, terminusie Morwowiec, wiem o tym (cytuje przepis). Mogę zobaczyć seal?
* Sasza: Proszę bardzo. Pewnie nie wiesz też nic o zaawansowanej konstrukcji TAI, nieregulaminowej, w AMZ, z którą czarodziejka Aegis ma do czynienia? (nadal zimny)
* Klaudia: Wiem, że AMZ ma TAI. Które jest bardzo przydatne i dba o uczniów.

Klaudia idzie BARDZO po przepisach. To nie jest wojna. Sasza ma uprawnienia, ale nie ma specjalnych uprawnień, bo NIKT ich nie ma bo Talia nie jest podejrzana, terminusi specjalnie nie mają uprawnień vs mafii (bo nie chcą nowego frontu) itp. Klaudia chce by Sasza dał spokój Talii. Przynajmniej na razie. A w idealnym świecie - z reputacji Klaudii - niech Morwowiec uzna że Klaudia będzie kapować na Talię. Talia demonstruje Zielonego Chichotka i pyta czy TYM się Sasza martwi.

Tr +4:

* Vr: Sasza daje Talii oddech. Nie przypieprza się tak do niej. Uważa, że problemem nie jest Talia a Tymon i Arnulf. Od tamtej strony musi iść.
* (Klaudia zaznacza, że terminusi są po to by chronić ludzi i magów, ktoś atakuje jej dom, niszczą jej drzwi do szopy i cholera wie co jeszcze; Talia dorzuca kilka incydentów zapytana. Talia nie może funkcjonować tutaj, i tak ma trudno. A Klaudia nie chce się bać przychodząc tu się uczyć) +1Vg
* V: Sasza powiedział, że spojrzy na to. Nie uważa, żeby ataki na Talię były prawidłowe, acz "mogłabyś nie robić seksbotów dla mafii". Talia potraktowała to z wyższością.

Sasza obiecał się że tym zajmie. I faktycznie się tym zajmie. 

* Klaudia: Dziękuję, terminusie.
* Sasza: Nie zbliżaj się do niej za bardzo. Między seksbotami i mafią jest to osoba o ograniczonych przymiotach.
* Klaudia: Za to dużej wiedzy.
* Sasza: Zgadzam się. Acz ja nie pozwoliłbym mojej córce zadawać się z Talią Aegis.
* Klaudia: Mój wybór.
* Sasza: Prawda.

Praca m.in. nad seksbotem wre. Klaudia zauważyła z radością, że Acrigenis został wprowadzony na Arenę Migświatła bez problemu. I nikt nic nie podejrzewa. To ten moment, gdy Klaudia zobaczyła na twarzy Talii promień zadowolenia. 

Wieczór. Klaudia znowu jest w akademiku. Ksenii nie ma, tym razem jest tylko Teresa. Ta jest znowu ubrana w swoje 'zakrywam wszystko' ciuchy, leży na łóżku i coś demonstracyjnie czyta. Ale Klaudia zobaczyła siniaki w okolicach nerki. To musiało być silne uderzenie. Klaudia podchodzi i podciąga. Teresa się odmachnęła i Klaudia oberwała. Ale widziała co chciała - Teresa dostała kilka ciosów, solidnie.

* Klaudia: Kto Ci to zrobił?
* Teresa: Nieważne.
* Klaudia: Jak to nieważne? Jakby ktoś mnie pobił nie chciałabyś wiedzieć kto?
* Teresa: Ona nie wygląda lepiej.
* Klaudia: Ok, kto?
* Teresa: Nieważne. Moja bitwa.
* Klaudia: Nie wszystkie musisz robić sama.
* Teresa: Nie pomożesz, tylko zaszkodzi.
* Klaudia: Zaszkodzę, bo nie będziesz solo przeciwko grupie?
* Teresa: (przykryła się cieplej) (nic nie mówi)
* Klaudia: Wracamy do 'brooding part'?
* Teresa: Nie drażnij mnie.
* Klaudia: Bo co, bo mnie znowu walniesz?
* Teresa: Nie chciałam Cię uderzyć. Przestraszyłaś mnie. Nie boję się Ciebie, nie boję się ich. Nie boję się nikogo.
* Klaudia: No właśnie o to chodzi byś nie musiała się nie bać.
* Teresa: (patrzy podejrzliwie)
* Klaudia: Ok. Albo powiesz mi o co chodzi albo powiem dyrektorowi że ktoś Cię stłukł.
* Teresa: No, nie! On nie może o niczym wiedzieć!
* Klaudia: Mów.
* Teresa: Nie, nie powiem Ci. Nic nikomu nie powiem wbrew mojej woli. Dyrektor też się nic nie dowie.
* Klaudia: Nie chcesz pozwolić komukolwiek by Ci pomógł? Wolisz z kimś kto Cię raz stłukł. I przyjdzie ponownie. Chyba, że powiesz o co chodzi. Nie jesteś dzikim stworzeniem z lasu, nie musisz chować zwichniętej nogi. Nic Cię nie zje, na pewno nie ja.
* Teresa: ...no dobra, ja zaczęłam. Ok? Ja uderzyłam pierwsza.
* Klaudia: A przed uderzeniem?
* Teresa: Ostrzegłam... 
* Klaudia: A przedtem?

Klaudia chce z niej wyciągnąć historię, co tu się stało.

Tr Z +3:

* Vz: Teresa się przyznała.
    * Jakaś czarodziejka mówiła, że Teresa jest nastoletnią prostytutką. Teresa jeszcze nie reagowała.
    * A potem mówiła i śmiała się z koleżankami, że Klaudia uczy się od seksbotów. Teresa celowała w twarz. Chłopcy ją skopali. Ale tamta nie wiedziała jak walczyć.
    * I - zdaniem Teresy - to mega niesprawiedliwe, bo Ksenia śpi 'ze wszystkim co ma dwie nogi' (co nie do końca jest prawdą).
    * Nie, Teresa więcej nie powie. 
    * Klaudia: A niech sobie mówią o mnie co chcą. Wiesz jaka jest różnica między nami? Ja się czegoś nauczę.
    * Teresa: O... o... (rumieniec)
    * Klaudia: Nie o seksbotach. Talia jest specem od AI.
    * Teresa: Ona robi roboty jak noktianki. Do bicia.
    * Klaudia: Ona jest noktianką.
    * Teresa: Ona jest zdradziecką noktianką... (ze smutkiem)
    * Klaudia: Musiałabyś z nią porozmawiać. Ja się nie wypowiadam by to ocenić.

Teresa wróciła do książki. Obolała, ale mniej smutna niż się wydaje.

## Streszczenie

Klaudia wrobiła Talię w uczenie jej o syntetycznych intelektach. Talia niechętnie pomogła. Okazało się, Talia pomaga SI z czasów wojny zaklimatyzować się do dzisiejszej rzeczywistości, między innymi umieszczając je w ciałach seksbotów. Talia ma fatalną reputację, ale nie dba o to. Jednocześnie, terminus próbuje ją przyskrzynić i ktoś ją sabotuje – Klaudia Talii tu bardzo pomogła.

## Progresja

* Klaudia Stryk: opinia tej co się ZADAJE z seksbotami i 'tą psychotroniczką od seksbotów'. Jej reputacja (grzecznej i niewinnej) ucierpiała.
* Talia Aegis: opinia tej co robi seksboty dla mafii, nie dość że noktianka to jeszcze zła noktianka. Zła reputacja w okolicy. Zadawanie się z nią jest lekko toksyczne reputacyjnie.

## Zasługi

* Klaudia Stryk: współpracuje z Talią (z ramienia AMZ) nad konstrukcją TAI; zrozumiała co się dzieje gdy AI Core gaśnie czy że Syntetyczne Intelekty żyją. Zrozumiała jak Talia umieszcza TAI militarne w seksbotach i daje im 'nowe życie'. Ucząc się od Talii, osłoniła ją przed dziwnym sabotażystą i terminusem który na Talię polował.
* Talia Aegis: musi Klaudię uczyć o TAI, więc zaczęła. Nie chce tego robić, ale Klaudia ją przekonała. Wyjaśniła Klaudii jak działają Syntetyczne Intelekty, jak to działa, że to żywe istoty. Aha, i ma problemy bo Ralena (o czym nie wie) próbuje sabotować jej laboratorium i ją skrzywdzić. Dzięki Klaudii Sasza osłonił Talię. TAK, cały czas pracuje nad seksbotami - m.in. wprowadza w nie TAI militarne i daje im szansę na lepsze życie.
* Ksenia Kirallen: żartuje sobie (w pokoju) z Klaudii że ona uczy się od seksbotów. Tak sympatycznie. I sprawia, że Teresa jest cała czerwona XD. Ciekawa tych seksbotów.
* Teresa Mieralit: cała czerwona (podsłuchuje rozmowę Klaudii i Talii) odnośnie seksbotów i projektu; gdy jedna z uczennic śmieje się z Klaudii że ona uczy się od seksbota, Teresa ją zaatakowała pięścią (i dostała wpierdol). Sama rozwiązuje swoje problemy. Uważa Talię Aegis za 'zdradziecką noktiankę' i nie chce mieć z nią nic do czynienia.
* Sasza Morwowiec: poluje na Talię, bo próbuje dorwać Arnulfa i Tymona. Podejrzewa jakieś mroczne działania Talii. Przekonany przez Klaudię, osłania Talię przed dziwnym atakiem sabotażysty. Firefight z Raleną. Jest zniesmaczony tym, że Talia robi seksboty zachowujące się jak noktianki. Chce chronić Klaudię i osłonić ją przed złym wpływem Talii.
* Ralena Drewniak: terminuska, sabotująca Talię Aegis i próbująca zniszczyć jej projekty, bo nienawidzi noktian. Podczas jednej z operacji natknęła się na Saszę, firefight, ale się oderwała

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Akademia Magii, kampus
                                1. Dzielnica Bliskomagiczna: uboższa dzielnica niedaleko Akademii Magicznej, mieszka tam m.in. Talia Aegis

## Czas

* Opóźnienie: 20
* Dni: 5
