## Metadane

* title: "Szara nawałnica"
* threads: legenda-arianny, niestabilna-brama, sektor-mevilig
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [210929 - Grupa ekspedycyjna Kellert](210929-grupa-ekspedycyjna-kellert)

### Chronologiczna

* [210929 - Grupa ekspedycyjna Kellert](210929-grupa-ekspedycyjna-kellert)

### Plan sesji
#### Co się wydarzyło

* .

#### Sukces graczy

* 

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Znajdujecie się jeszcze na K1. Admirał Termia dała Wam wszelkie uprawnienia dotyczące sal gdzie badani są ozdrowieńcy z Savery. Większość jest nieprzytomna, ale Termia mimo wszystko autoryzowała czytanie pamięci itp.

* Czy na pokładzie był mag? -> TAK, był. Jednego się udało odratować. Remigiusz Błyszczyk, kartograf + pływy magii + analiza Bram.
* Savera podczas walki z piraniami zorientowała się, że to byty zbudowane z nanitek. Nanomaszyny + struktura mechaniczna. "Rozbierania rzeczy na części". Z opcją replikacji.
* Piranie nie są odporne na magię, ale jest ich dużo. Kierują się na ruch, na statki, na cele.
* TAI zaczęła szaleć wcześnie. Ta mantra którą TAI mówiła - ona miała TAI chronić. Przed tym szaleństwem. Słabo działało, ale...
* Potwór Esuriit odciął TAI od komunikacji. Przeciął kable, komunikację itp. Stąd TAI nie miała kontaktu ze światem poza Saverą.
* Potwór Esuriit wyraźnie ma jakieś powiązania z twarzami. Ci, co ich zaatakowali mieli szmaciane maski. Żeby potwór ich "nie widział".

Im szybszy bagaż emocjonalny i stres tym szybciej Skażenie postępuje.

Eustachy - załoganci na statku, z informacjami od K1 z checklistą - triage. Kartoteka do ratowania (zieleni) lub hibernacji. Pierwsza forma zabezpieczenia twarzy - makijaż mający "zagłuszyć" kim jest osoba. Plus nieprzezroczyste hełmy / maski w wypadku akcji.

Eustachy -> Termia. Czy mieli eksperymentować z Esuriit, nową TAI, cokolwiek? Termia ma odpowiedzi.

* Nie testowaliśmy Esuriit, TAI, niczego.
* Z danych historycznych Mevilig jest sektorem z dużą technologią. Trzy zwalczające się frakcje i dużo eksperymentów z nanomaszynami i autobroniami.

Eustachy + Klaudia mają zmontować prototyp torped mających być bardzo atrakcyjne dla Piranii z pomocą K1. Projekcja, że jest większa. (Eustachy dominuje w konflikcie). 

TrZ+3+3Og: 

* XXz: torpedy skutecznie odwracają od Was uwagę, ale są "GŁOŚNE". I nadużywanie ich sprawi, że Piranie zaadaptuje. +1O do toru TAI.
* V: Piranie automatycznie się tam kierują.

Plus, Klaudia chce zbadać stan tego sygnału używając Rziezy. Rzieza nie jest szczęśliwy, ale próbuje zrozumieć problem i zabezpieczyć K1.

ExZ+4+6Or:

* X: strainy tego sygnału zaczęły mutować i wypełzać poza sandbox
* O: Rzieza anihilował sygnał. Poważna akcja i awaria. Rzieza jest gotowy.

Rzieza wyjaśnił na czym polega problem. Jest coś, co podnosi TAI na wyższe poziomy topozoficzne i łączy je w sieci - których nośnikami są Piranie. I są tam różne TAI, podniesione w ten sposób. Rzieza anulował operację. Klaudia się nie zgadza.

Czy posiadanie lustra jest niebezpieczne? NIE. Więc Arianna ma chory pomysł - oblec servary w lustra. By potwór ich nie widział. I niech Termia to zrobi.

Niewidoczność agentów przez potwora dzięki lustrom:

TrZ+3+5Og: 

* V: Servary FAKTYCZNIE są niewidoczne. Potwór szuka, ale nie znajdzie.
* O: Użycie tych servarów budzi ZAINTERESOWANIE stwora.
* Vz: Servary sprawiają, że ludzie blisko też są niewidoczni.

A, i Termia dodała sprawny Eidolon dla Eleny...

Dobra. Lecimy TERAZ. Klaudia chce jak najszybciej lecieć by Rzieza nie zadziałał.

* X: Część załogi się nie stawiła. Stawiły się inne osoby, nie należące do Inferni. Błędy w rozkazach itp.
* V: Klaudia to widzi.

Klaudia pozbyła się tej załogi z Inferni. Rzieza ostrzega Klaudię, że jak odleci, on ich zniszczy. Klaudia powiedziała, że ostrzeże innych przed jego istnieniem. Rzieza jest niezadowolony. Przekazał Klaudii torpedę do wsadzenia na Infernię. Jak połączy się Persefona - Torpeda.

Eustachy MUSI zrobić przeróbkę Inferni (oficjalnie). Dodaje tunele serwisowe między Infernią a zewnętrznem. Chce, by torpeda nie zniszczyła Inferni a jedynie uszkodziła poważnie Infernię. By nie wymarli. I zmienia ekranowanie na szybko w stoczni poza K1.

Zespół leci spokojnie do stoczni. A Klaudia podbija do Arianny. Wyjaśnia jej na czym polega problem ze Rziezą. Oops.

Dwa dni refitowania w stoczni. Porządna robota, Eustachy wie co robi. I czas -> sektora Mevilig.

Niewidzialna, słitaśna Infernia. Może wziąć bezpiecznie 120-150 osób jak wszystkich się mega upchnie (jak PKP w lecie). I przed Infernią znajomy, charakterystyczny obraz - przytłumiona gwiazda, otoczona rojem Dysona. Niezliczona ilość Piranii.

Infernia wystrzeliła torpedę 'decoy', lecąc w kierunku na tą wielką planetoidę. Savera określiła ją jako planetoidę Kalarfam. Skanery Savery określili Kalarfam jest w większości kamienna i lodowa. Piranie są mechaniczne. To nie jest wielka pirania - to wielka planetoida.

Eustachy składa sondy seismiczne. CHodzi o to by wybuchły i da się z "sejsmicznej sygnatury" zrobić mapę planetoidy.

ExZ+4+5Og(siła O TAI: 3):

* O: ZAINTERESOWANIE TAI +3
* O: ZAINTERESOWANIE TAI +3
* X: Sondy coś "strąciły". Wybuch sond rozwalił niewidoczność jakiegoś śmieciowego stateczka.
* X: ...i COŚ się zbliża do tego stateczka.
* V: Eustachy ma mapy sejsmiczne. Łącznie z informacjami na temat tego jak na wybuchy radzą sobie te rzeczy
    * "Mechaniczny potwór": jest solidnie z metalu. "Żywy metal".
    * Śmieciowy stateczek jest złożony ze złomu. 
    * Poza tym jest lokalizacja wraków naszych jednostek. W środku. I innych wraków "wrakowisko".

Infernia leci na dużej prędkości do środka, do śmieciowego statku.

(plan:

Elena + Martyn jako pilot pintka -> akcja ratunkowa na stateczek. Plus na złoma parę torped. Ludzi zgarniamy do karceru. Zwiewamy na odległość. Potwór pożera "owcę z siarką" z kilkoma torpedami, jak bardzo ściągnęło uwagę - i odlatujemy. Próbki, ogarniamy, wtf. W pintce, w polu siłowym w pintce, by nie było kontaktu metal - metal. A pintka ma tractor beam.

)

TrZ+3:

* Vz: Infernia daje radę - są pod statkiem.
* Xz: Infernia daje radę, ale niestety nie jest zwrotna. Ani cicha. Czyli jest "widoczna".
* X: Potwór jest szybszy niż się zdawało. A dokładniej, inne sejsmiczne echa - podobne mniejsze byty zbliżają się z innych stron, tunelując przez kamień.
* X: Mamy PIEKIELNIE mało czasu. Jeśli chcemy wprowadzić torpedy, nie będziemy mogli ich łatwo rozładować. Czyli trzeba robić dziurę w statku - nie da się wysłać grupy ratunkowej.

Eustachy strzela by zrobić dziurę w statku a Elena i komandosi robią kosmiczne przechwycenie "śmieciarzy" (ludzi ze statku).

ExZ+4+5O:

* X: Eustachy nie przewidział jak delikatny jest ten statek. Bez sensu, pierwsi zabici.
* Vz: Udało się uratować kilkanaście osób.
* (+3Vg) Eustachy wprowadza torpedy. X: TAI zorientowały się w sytuacji - mogą użyć sejsmiki by znaleźć ludzi. Czas na fabrykację.
* V: Torpedy na miejscu.
* X: Inne potwory lecą w tą stronę.
* V: Wybuch oderwał pół potwora i cholerstwo "przestało żyć". Potwór nie żyje.
* X: musieliśmy stracić torpedę że niby UCIEKAMY. Torpeda je odciąga. ALE - w truchle potwora torpeda anihilacyjna. 
* Vg: Komandos poleciał wziąć bezpieczną próbkę.

Arianna decyduje się na użycie torpedy anihilacji. Torpeda ma chęć "pożarcia". Arianna chce ją ożywić - chce by cała energia skupiła się w mniejszym obszarze.

TrZM+3+3Or: 

* XXmO: TORPEDA CHCE POŻREĆ ŚWIAT!!! Poszerza, powiększa...
* V: Arianna dała radę POKAZAĆ anomalii że planeta jest smaczniejsza niż planetoida. Anomalia energetyczna POŻARŁA metalowe byty i skierowała się ku planecie...
    * Gdy anomalia zbliżyła się do wrakowiska, została odparta. Odepchnął ją Krwawy Potwór.

Paluszek pod kłódkę. TO nie trafia do raportu.

MACIE kilkunastu "jeńców". Są OOO.

Arianna + Eustachy + Kamil + Otton gadają z jeńcami. Najpierw mówili nie po swojemu. Ale język Persi ma w bazie. To język używany przy Pęknięciu. Stary.

Oni powiedzieli, że ONI STĄD. Nanitki stworzyli ich przodkowie.

"Wasz zbawiciel jest słaby. Tylko ja mogę was zbawić." Arianna chce by oni wyzbyli się kultu.

ExZ+3:

* X: Nie do końca wierzą, bo nie wierzą że może być lepiej. Plus, mieli innych przybyszów z Bramy.
* Xz: "powiedziałaś, że jesteś silniejsza niż on. Udowodnij i zdejmij maskę. I powiedz jego imię."
* (+3Vg) Vg: JAKA ONA PIĘKNA. Wyrzekli się. Zdjęli maski, tak samo.

Martyn ich wziął i zaczął robić badania. Uśpienie + badania. Oni też przedtem powiedzieli wszystko co wiedzą o swojej kulturze i świecie.

* Ci ludzie wywodzą się z kultury neoterrańskiej, tak jak Astoria. Wspólny rdzeń.
* Ich przodkowie walczyli z kultury pranoktiańską. I innymi. I m.in. wyścig zbrojeń. W którymś momencie TAI weszły w sojusz przeciw ludziom.
* Wszystkie bazy, stacje kosmiczne itp -> zniszczone i przejęte przez TAI. Tam ludzi po prostu nie ma.
* Planeta jest FARMĄ ludzi. Chyba. Tak wynika z tego co mówią Piranie.
* Niszczyciel-Zbawiciel jest tym, co chroni ludzi. Tym, czego nie mogą pokonać maszyny. Ich zbawicielem, ale żądającym ofiar.
* Legenda głosi, że pewnego dnia Zbawiciel zabierze ich do raju. Wtedy Niszczyciel zniszczy wszystko. I oni uznali "anomalię anihilacyjną" za Znak.
* W bazie jest kilka tysięcy osób.
* Wrakowisko to "ich stocznia".
* Przybyli tu ludzie "z zewnątrz". Nie wiedzą ile, bo kapłani zarządzają tą cywilizacją.
* Tak, nie zdejmują nawet do "kąpieli".

## Streszczenie

Zespół przygotował się na wejście do Mevilig. Rzieza odkrył jak działają Piranie i że tamten sektor jest opanowany przez TAI 5+ generacji. Niestety, Infernia musiała uciekać przed Rziezą. Na miejscu Eustachy znalazł skuteczny sposób przemykania między Piraniami, dotarli do planetoidy Kalarfam gdzie znaleźli bazę ludzi. Mechaniczny "smok" został zniszczony przez Infernię, pobrali lokalsów i Arianna przekonała ich do zmiany kultu. Aha, anomalny wybuch torpedy anihilacyjnej Arianna przekierowała na planetę, wypowiadając ostrą wojnę TAI 5+ generacji...

## Progresja

* .

### Frakcji

* .

## Zasługi

* Arianna Verlen: znalazła sposób by Vigilus nie widział servarów - użyć luster. Potem - złamała konwencję anomalizując torpedę anihilacyjną (wybuch) i kierując go ku planecie w sektorze Mevilig. Aha, przekonała grupkę ludzi z Mevilig że ona jest silniejsza niż zbawiciel - jej kult jest lepszy.
* Eustachy Korkoran: refitował Infernię by móc wysadzić torpedę Rziezy, znalazł sposób jak omijać skutecznie Piranie, sondami sejsmicznymi znalazł bazę ludzi w Kalarfam i użył torpedy anihilacyjnej by zabić "smoka".
* Klaudia Stryk: zrozumiała na czym polega Pirania i TAI w Mevilig, ale skonfliktowała się z Rziezą. Teraz musi go przechytrzyć...
* Aleksandra Termia: odbudowała Inferni zapasy i sprzęt. Współpracuje z Infernią, bo chce odzyskać swoich ludzi z Mevilig. Autoryzowała nawet torpedy anihilacyjne.
* Otto Azgorn: z Eleną i komandosami Verlenów przechwytywał lokalsów ze śmieciostateczku w Kalarfam na Infernię. Zdobył próbkę "metalowego smoka".
* Martyn Hiwasser: zajął się ocaleńcami z sektora Mevilig; wyjaśnił, że ich poważnym problemem jest potężne Skażenie Esuriit. Wymagają ogromnej pomocy na K1.
* Elena Verlen: wraz z komandosami Verlenów przechwytywała lokalsów ze śmieciostateczku w Kalarfam na Infernię. Niewielka rola.
* Remigiusz Błyszczyk: mag z Savery; kartograf + pływy magii + analiza Bram. Przesłuchany przez mentalistów Termii, wyjaśnił co wie o sektorze Mevilig.
* TAI Rzieza d'K1: rozwiązał dla Klaudii zagadkę jak działają Piranie i TAI w sektorze Mevilig - niestety, chciał Klaudię (i Infernię) upolować by nie lecieli tam.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy: Klaudia weszła w "spór" z Rziezą odnośnie sygnałów z Mevilig; Infernia ratowała się ucieczką z K1 przed Rziezą.
            1. Brama Kariańska
            1. Stocznia Kariańska: zapasowa, awaryjna stocznia Orbitera dla jednostek ratowanych z Bramy Kariańskiej. Infernia się tu refitowała.
        1. Sektor Mevilig
            1. Chmura Piranii: tymczasowo zneutralizowana przez nowe torpedy Eustachego
            1. Planetoida Kalarfam: poważnie uszkodzona anomalną torpedą anihilacji; jest tam mechaniczny "smok" i kolonia ludzi. Też jest tam wrakowisko.
            1. Keratlia: planeta, na której znajdują się "farmy" ludzi i która jest pod kontrolą TAI 5 generacji. Leci w jej kierunku anomalny wybuch torpedy anihilacyjnej.

## Czas

* Opóźnienie: 2
* Dni: 5

## Konflikty

* 1 - Eustachy + Klaudia mają zmontować prototyp torped mających być bardzo atrakcyjne dla Piranii z pomocą K1. Projekcja, że jest większa. (Eustachy dominuje w konflikcie). 
    * TrZ+3+3Og
    * XXzV: torpedy skutecznie odwracają uwagę, ale Piranie adaptują. Neutralizacja Piranii.
* 2 - Klaudia chce zbadać stan tego sygnału używając Rziezy. Rzieza nie jest szczęśliwy, ale próbuje zrozumieć problem i zabezpieczyć K1.
    * ExZ+4+6Or
    * XO: strainy mutują, wypełzają poza sandbox; Rzieza niszczy sygnał.
* 3 - Czy posiadanie lustra jest niebezpieczne? NIE. Więc Arianna ma chory pomysł - oblec servary w lustra. By potwór ich nie widział. 
    * TrZ+3+5Og
    * VOVz: servary i otoczenie niewidoczne, ale to budzi ZAINTERESOWANIE Vigilusa.
* 4 - Dobra. Lecimy TERAZ. Klaudia chce jak najszybciej lecieć by Rzieza nie zadziałał.
    * Tr+2
    * XV: Część załogi się nie stawiła, ale Klaudia to zauważyła i rozmontowała
* 5 - Eustachy składa sondy seismiczne. CHodzi o to by wybuchły i da się z "sejsmicznej sygnatury" zrobić mapę planetoidy.
    * ExZ+4+5Og(siła O TAI: 3)
    * OOXXV: ZAINTERESOWANIE TAI +6, sondy strąciły stateczek i "coś" tam leci. Eustachy ma mapy sejsmiczne.
* 6 - Infernia leci na dużej prędkości do środka, do śmieciowego statku.
    * TrZ+3
    * VzXzXX: Infernia dotarła, ale jest "widoczna" i potworów jest więcej. Piekielnie mało czasu.
* 7 - Eustachy strzela by zrobić dziurę w statku a Elena i komandosi robią kosmiczne przechwycenie "śmieciarzy" (ludzi ze statku).
    * ExZ+4+5O
    * XVz: ratujemy kilkanaście osób, ale są zabici na śmieciostatku
    * (+3Vg) XVXVXVg: TAI użyją sejsmiki by znaleźć ludzi, torpedy na miejscu, potwór nie żyje, komandos ma próbkę potwora, ale tracimy torpedę. +1 ZAINTERESOWANIA.
* 8 - Arianna decyduje się na użycie torpedy anihilacji. Torpeda ma chęć "pożarcia". Arianna chce ją ożywić - chce by cała energia skupiła się w mniejszym obszarze.
    * TrZM+3+3Or
    * XXmOV: Torpeda chce POŻREĆ ŚWIAT. Arianna wskazała anomalicznej torpedzie planetę. TO NIE TRAFI DO RAPORTU!
* 9 - "Wasz zbawiciel jest słaby. Tylko ja mogę was zbawić." Arianna chce by lokalsi z Mevilig wyzbyli się kultu.
    * XXz: nie do końca wierzą, bo widzieli innych. Plus, "udowodnij że jesteś silniejsza i zdejmij maskę"
    * (+3Vg) Vg: JAKA ONA PIĘKNA. Wyrzekli się. Zdjęli maski.

## Inne
### Projekt sesji
#### Narzędzia
##### Koncept

.

##### Implementacja

.

#### Przeciwnik

1. esuriit-Skażony mechaniczny terrorform
2. Wielka planetoida Kalarfam
3. Ciągły niedobór energii i konieczność chowania się przed Pożeraczami... i terrorformami
4. kultyści w bazie.
5. Sam teren bazy i dotarcie do niej, dookoła planetoidy Kalarfam.

#### Sceny

Podczas całej sesji - 3 entropiczne esuriit. W wypadku magii - 5 entropicznych esuriit a nie 3 magiczne.

1. Infernia szuka zaginionych jednostek. Zbliża się do niej złom kosmiczny. Złom kosmiczny to nie złom. To Pożeracze - zmierzają w kierunku Inferni. Jest ich za dużo by wszystkie zniszczyć... jak sharkticony. A użycie głośnych działań - ściągnie terrorforma (o którym nie wiedzą).
    * KONFLIKTY: czy Inferni uda się odlecieć?
1. Planetoida Kalarfam; Infernia za sygnałami SOS itp. wlatuje do środka i szuka źródła sygnału. Ostrzeliwuje ją mały stateczek który próbuje Infernię wyprowadzić na bezpieczną przestrzeń. To budzi terrorforma w planetoidzie, który poluje na Infernię i ów stateczek.
    * POKAZUJE: zagrożenie z "prostych rzeczy". Pułapka wynikająca z ixiońskiego terrorforma polującego na byty.
    * AKTORZY: planetoida, terrorform, stateczek
    * KONFLIKTY: czy stateczek przetrwa? czy Infernia ucieknie terrorformowi bez szkód?
1. Wejście na martwy statek 'OO Savera'. Próba odnalezienia map / sektora / where the hell are we. Wrogowie: Tech-Mawurm, Necroguard, Tech-Rockmorph, Soulmorph. I szepty. Szepty o terrorformie. I jeszcze gasnący ludzie, należący już częściowo do szalonej TAI.
    * POKAZUJE: tu się strasznie coś złego stało...
    * KONFLIKTY: dyskretne dostanie się do danych, wycofanie się bez złamania sygnału, COUP DE GRACE, czy dojdą do info o "szalonych TAI"?
1. Ludzka baza; reaktor, pole niewidoczności, kultyści esuriit trzymający bazę przy życiu składając ofiary. Ten odcinek Netflixa ze statkiem kosmicznym. A dotrzeć tam... Infernia jest duża.
    * KONFLIKTY: czy Infernia będzie w dobrym stanie / nie uszkodzi maskowania? Czy połapią się w rozkładzie bazy? Czy dojdą do tego czemu wszyscy mają maski? Czemu nikt nie mówi gdzie są ludzie z Grupy Ekspedycyjnej Kellert? Odnajdą ich w tunelach NIE ściągając Pożeraczy / terrorforma?
1. Ludzie w bazie ROZPACZLIWIE chcą dostać się na pokład Inferni. Ewakuacja? Ratunek? Dzięki Xardasowi! Rozpaczliwe błagania, dadzą wszystko. Uciec z tego piekła. Ambrozja -> ludzie padają na ziemię chłeptać krew.
    * KONFLIKTY: czy Infernia... odleci? Czy będzie wojna Infernia (200) vs większość populacji? Czy kogokolwiek uda się uratować? Wezwać terrorforma przypadkiem? Uratować "swoich"?
1. Terrorform
    * (loop)"...za dużo ścięgien, kości, za długie, tak duży, tak dużo, za dużo oczu, za dużo kończyn, kończyny wszędzie, on jest wszędzie, krew wszędzie, zbawiciel, niszczyciel, szepty, tak dużo szeptów, każda twarz szepcze, tak wiele twarzy, każda twarz jest jego, żyje w każdej twarzy, ostrej, przekłuwające, tnące, mechanizmy, ścięgna..."

#### Wsparcie graficzne (rysunki)

.
