## Metadane

* title: "Reality show z zaskoczenia"
* threads: olaf-znajduje-dom, igrzyska-lemurczakow
* motives: okrutne-gry-moznych, echa-inwazji-noctis, niewolnicy-magow, magowie-bawia-sie-ludzmi, b-team, misja-survivalowa
* gm: kić
* players: żółw

## Kontynuacja
### Kampanijna

* [230618 - Reality show z zaskoczenia](230618-reality-show-z-zaskoczenia)

### Chronologiczna

* [230618 - Reality show z zaskoczenia](230618-reality-show-z-zaskoczenia)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ?
* CEL: 
    * ?

### Co się stało i co wiemy

* Kontekst
    * .
* Wydarzenia
    * .

### Co się stanie (what will happen)

?

### Sukces graczy (when you win)

* .

## Sesja - analiza
### Fiszki

* .

### Scena Zero - impl

.

### Sesja Właściwa - impl

Ostatnią rzeczą jaką Olaf pamięta jest knajpa. Ostatnia rzecz po tym jak obudziło go chłodne powietrze. CHŁODNE. Leży na twardej skale, nie wygląda to na bar. Olaf otwiera oczy - przyciąga jego wzrok nieduże ognisko. Drugie - DALEKO widzi, to nie ściana. (I nie ma knajpy). I dookoła ogniska porozrzucane jest jeszcze 5 osób. Zaczynają się powoli ruszać.

Olafowi wszystko działa, acz nie może w pełni wstać - są na półce skalnej. Nie chce zlecieć z tego zbocza. A ostatnie co Olaf pamięta - pił w knajpie. Ale po skupieniu - wie rzeczy o jakich nie miał pojęcia. "Zadanie - przetrwać, last team standing wins." "Możecie użyć wszystkiego co znajdziecie / zdobędziecie, wszystkie chwyty dozwolone, nikt nie będzie Was szukał". "Opuszczenie TEGO terenu równa się śmierć." "Zwycięski zespół - każdy członek zespołu ma jedno życzenie."

Olaf widzi, że ludzie dookoła są różni - nie wszyscy są z Noctis. 2 laski i 3 facetów. Z Olafem - sześć osób.

Przy ognisku kompas, nóż i saperka. Sztuk jeden. Olaf ocenia ludzi - każdy ma moment zagubienia w momencie obudzenia, chwila "coś wiemy". Jedna laska drobna i zwinna, raczej młoda, chuda (Lily). Jeden facet koło 30 z manierą wojskową, acz jest zmęczony życiem (Roman). Jeden bardzo żylasty gość, szczupły żylasty i silny (Alan). Druga kobietka jest... księgową (Maja)? na oko - praca biurowa. Trzeci koleś - jak tylko się ocknął i pozbierał to pierwsze co zrobił, sprawdził ogień (Maks). Powiedział "nie ma tu za dużo drewna".

Wszyscy wyglądają jak przegrywy. Nikt nic nie pamięta i nie wie.

Maks się zna na rzeczy - jest strażnikiem parkowym od 5 lat. Roman sięga po kompas. Ponuro "zakładając że jesteśmy tam gdzie jesteśmy, półkula globu to cholernie duże miejsce". Olaf z uśmiechem: "wpierw przetrwajmy i ustabilizujmy przyczółek, potem zaplanujemy co dalej. Wyglądasz na fachowca." Roman jest sceptyczny wobec Olafa.

Czyli trzeba zejść w dół. Asekuracja ręczna. Kilka osób bierze ogień, rozkładamy ognisko by wziąć drewno. Maks tu rządzi, ogarnia. Wykombinował że jak obtłucze się skały to może nie miska ale ogniotransporter.

Tr +3:

* Vr: uda się zejść do przeniżenia między szczytami; zaczyna się roślinność

Maja zaproponowała zrobienie liny z gałęzi itp. Roman pilnuje, Maks, Olaf i Lily próbują przeszukać zagrożenia i przydatnych rzeczy. Roman pilnuje "bazy", Olaf pilnuje "away party". Maks zaznacza, że to są rośliny żywiczne. Olaf się uśmiecha i nie udaje że rozumie. Maks zaznacza, że to znaczy że się będzie lepiej palić.

(+przewaga: wiedza Maksa i ogólna uważność) -> TrZ+3

* Xz: (podbicie) kozioł górski. Okrzyk Maksa, poślizgnął i prawie spadł i kozioł nadstawia się na Maksa, by go pogonić. Maks się powoli wycofuje. Olaf próbuje przejąć uwagę na siebie.
* X: Maks MUSIAŁ odskoczyć z drogi; trzeba go NATYCHMIAST ratować. Kozioł wieje koło Olafa XD. Olaf leci i szczupakiem - wyciągać Maksa. 
* X: Nie da się wyciągnąć Maksa, bo się zsuwa, więc Olaf i Lily zdejmują kurtki i używają ich jako lin dla Maksa. Kurtki będą mocno uszkodzone, ale Maks nie zsuwa się.
    * Maks jest wyciągnięty.

Olaf -> Lily "szybka reakcja, dobra robota."

* Vr: Olaf nie czuje się dobrze w tym terenie, widzi coś swojskiego. Przebłysk czerwonego koloru. ZDANIEM OLAFA CZERWIEŃ W NATURZE NIE WYSTĘPUJE co szybko pokazuję Maksowi (który szybko wyprowadził Olafa z błędu). Ale "to nietypowe".

To plecak. Przechwytywalny. Przez cały czas jak szukali Roman nie znalazł śladów ludzi, nie znalazł żadnych większych śladów. Zasadniczo - brak cywilizacji. Ktoś "zostawił" przysłonięty plecak bez szans znaleźć. Przypadkiem byśmy na to nie wpadli. Olaf próbuje dojść do plecaka i go przeszukać / przenalizować z resztą. Wpierw pułapki potem wynik - co w środku.

Olaf BEZ PROBLEMU znalazł pułapkę. Taką terminalną, ale łatwą do ominięcia.

* V: Lily chciała rzucić się do plecaka, ale Olaf NATYCHMIAST zimnym głosem "STOP". Na ten typ głosu Lily zamarła w półkroku. Olaf natychmiast przeprosił.
    * Lily i Maks są raczej przekonani, że Olaf chce dobrze.

Olaf próbuje rozbroić tą pułapkę. To potencjalny zasób - broń. Igła medyczna która coś wstrzykuje. Plus pułapka ma części. I can disassemble it. Olaf używa kurtki do wsparcia w rozbrajaniu. Odzyskał igłę medyczną i inne substraty. A w plecaku były: lina, płachta (brezent, duża). Możemy zrobić namiot.

Olaf używa swoich skilli śledczych i inspiracyjnych. Wyprowadza: nie tylko krzywdzą, ale też pomagają. To gra. Ale jak to gra, możemy wygrać. Budowa zespołu i morale. Olaf zaznacza, że ON się nie zna na naturze, czyli nie powinien dowodzić. Ale podniósł rolę wszystkich innych - Romana, Lily, Mai, Maksa. I jeszcze Olaf ma tu Mistrzostwo!

TpZ+3:

* V: (podbicie) Podniesione morale, wierzą że jesteśmy w stanie sobie poradzić.
* X: Roman się zepnie z Olafem
* Vz: (manifestacja) Mamy sprawny zespół.

.

* Roman (gniew i żal w oczach) (zgorzkniały komentarz): "(Przez takich jak Ty) wyleciałem z wojska."
* Olaf: "Nie przeze mnie. To niemożliwe. Nawet nie jestem z tej planety. Moi bracia strzelili mi w plecy" (pokazując bliznę na sercu)

.

* Xz: Roman widzi w Olafie żołnierza (i chce pogadać) ale też przyczynę cierpienia. Będzie współpracował, będzie się opiekował resztą ale Olaf jest pierwszy do odstrzału.

Olaf opowiedział, że był śledczym noktiańskim, dostał od "swoich" w plecy. Nie ma nic, pił, obudził się tu. I może jest przegrywem, ale pomoże. I wierzy, że sobie poradzą. Lily "miałam _robotę_ w domu i... miałam tam wejść i nic nie pamiętam". Czyli to szczur uliczny. Alan: "Cieniaszczyt?" Lily: "mhm". Zespół zaczyna się łączyć. Maja "ja z Aurum, ostatnio siedziałam w bibliotece". Maks: "jak mnie wykopali z parku w Drzewcu, to trochę łażę po świecie szukając czegoś... _i znalazłem... lub coś znalazło mnie_". Alan: "ja w sumie, trochę tu trochę tam. Ostatnio w klubie wspinaczkowym pracowałem, kiepsko mi szło bo się trochę boję (lęk wysokości)". Roman może podejrzliwy, ale upewnił się że Lily, Maks > Olaf są cali i nie trzeba opatrywać.

Maks kierował jak z tych substratów zrobić schronienie. Olaf składał. Olaf + Roman ustalają warty, gdzie Roman jest najlepszy. (Roman + Maja), (Olaf + Maks bo się Olaf nauczy czegoś), (Lily szczur uliczny + Alan).

Dla Olafa nie jest naturalne, że na planecie _wszystko jest_. Myśli o wodzie, jedzeniu itp.

Olaf opuścił namiot i poszedł szukać kamer i urządzeń rejestrujących. Nie był w stanie ich znaleźć, więc wybrał losowy potencjalnie rejestrujący kamień i wygłosił mu EPICKĄ mowę, jak to "większa korzyść dramatyczna dla oglądających jeśli uda się nie zabijać innych zespołów a zjednoczyć je pod flagą ludzkości", że Lily "może nie być szczurem ulicznym a nauczyć się super rzeczy". Powiedział to odpowiednio efektownie i dramatycznie, nie udając że jest zafascynowany planetą. DO TEGO powiedział że w sumie jest im wdzięczny. Jeszcze wczoraj gnił w rynsztoku, a dziś ma znowu kogoś kogo może chronić i pomóc. A, i ma interesujące opowieści noktiańskie.

W międzyczasie jak się szykują, Lily coś z Maksem gada i w którymś momencie bierze nóż i urzyna sobie warkocz. Olaf nie rozumie. Lily "dobrze splecione włosy są mocne, można zrobić pułapkę". Olaf na ochotnika do pomocy w craftingu.

(kość szczęścia Tr+2: VXXV, hidden movement. I dealbreaker: X -> V)

Maks jest kompetentny, dobrze ustawił obóz. (była mowa, było sporo dobrych ruchów). Olaf nie wie na co patrzeć. Raz się osypały kamienie, za jakiś czas coś zachrobotało. Maks "spokojnie, to tylko gałęzie na wietrze". W którymś momencie - Maks "uważaj" i się rzuca by przewrócić Olafa, w płachtę wpada niewielkie stadko zwierząt po czym się obala, przekotłowuje i spieprza.

Reszta nocy minęła spokojnie, co nie oznacza, że wszyscy są wyspani...

GDY JEST STADKO, Olaf ma zaostrzony kij + kamienie. Nóż i saperka -> Maks. Więc Olaf atakuje.

(+Olaf is on alert and can fight -nie ma pojęcia co i jak +Maks widzi co robi Olaf i wspiera +to nieduże stworzenia)

Tr +3:

* X: na pewno nie na miejscu; może się wykrwawić, ale nie tu
* V: (podbicie) WYKRWAWI SIĘ, ale niekoniecznie jest odzyskiwalne
* V: (manifestacja) dotrzemy do zwierza acz niekoniecznie to proste
* Vr: (wyższa manifestacja) Olaf SKUTECZNIE zadał ranę zwierzęciu. Szybko padnie, acz nie tu. He is deadly. Albo miał szczęście XD.
    * to stresuje Romana. To jest DOBRE że Olaf jest po naszej stronie, ale "cholera."
    * brezent jest cały ochlapany krwią; to ważne gdyby był jakiś większy drapieżnik

Maks chce ograniczyć cierpienie zwierzaka. Olaf zauważył, że zwierzak dostał bardzo ciężką ranę (po ilości krwi). Maks się zgadza - nie śledzimy w nocy.

DZIEŃ.

Maks i Roman idą po padłego zwierza. Olaf zostaje z zespołem pilnować by nikomu nic się tu nie stało. Wrócą ze zwierzyną, na widok zwierza Maja marszczy brwi "TUTAJ?! Trochę nie ma sensu, jesteśmy za wysoko." Olaf: "To nie są aligatory?" Z Maksa uszło powietrze. (takimi pytaniami Olaf zapewnia odpowiedni efekt dramatyczny). Maks mówi, że mogą zostać lub się ruszyć. Mając zwierza, da się zrobić racje żywnościowe. Maks chce nazbierać rzeczy by upiec i by wieszać paski mięsa na gałęzi. ALE trzeba stąd iść. Drapieżniki.

Gdy wszyscy pracują, Roman "schodząc ze zbocza szliśmy na zachód, zakładam że nie chcemy pod górę - mamy do wyboru N, W, S". Olaf potwierdza - na pewno są na Astorii. Ta sama półkula co zwykle. I nie widać śladów cywilizacji. Olaf nie zauważył nigdzie żadnych śladów wskazujących na inne grupy takie jak oni.

Maks próbuje ocenić sytuację która trasa jest najlepsza. (+zna się na rzeczy -nie wie gdzie jest +Maja zagubiona że część roślin i zwierzy widzi, ale niektóre nie pasują do biomu, i możliwość monitowania terenu)

Tr Z +2:

* Xz: (hidden movement) - zajmuje sporo czasu
* V: (podbicie) Maks nie wybierze złej trasy
* V: (manifestacja) wspólna konferencja -> Maks wybierze dobrą trasę, na S.

Olaf i Roman pilnują, Roman z tyłu Maks z przodu. Olaf asekuruje liną. Jak na łańcuch górski, jest to cholernie krótkie. Po większej części dnia następuje drastyczna zmiana otoczenia. Teren się wypłaszcza i zaczynają widzieć coraz więcej wody, zupełnie inny biom. Oczom zespołu - fajne wystające z wody korzenie. Maja "LAS NAMORZYNOWY?!". Olaf: "teraz jestem PEWIEN że tu mogą być aligatory." Maks: "chyba?" Maja: "Tak! I pijawki i komary i inne straszne rzeczy."

Olaf wyprowadza logikę - to niemożliwe. Roman i Lily wymieniają spojrzenia. Lily "jak głupi możesz być? To oczywiste, że magowie się bawią." Olaf: "magia tak może?" Lily "to po nas..." Olaf "nie, nie po nas. Każdy coś wie i to seria dalszych wyzwań". Olaf pokazuje im, że warto działać survivalowo i funkcjonować dalej. Warto być w programie. Dadzą radę.

Woda - w większości - powinna być słodka. Olaf proponuje rozglądać się po drzewach i innych roślinach szukając znaków zrobionych przez człowieka - tam może coś być.

Olaf przypadkowo wdepnął do wody i złapał pijawki. Maks pomógł je usunąć.

W którymś momencie Lily pokazuje palcem. Na drzewie jest przyczepiona (jak?!) strzałka pokazująca do góry. Zlewa się z tłem - bez szukania się nie uda znaleźć. Alan "ja to zrobię" - wejście do góry. Nietypowa wspinaczka, ale Alan się zna. Wie co robi, nie zleciał. Sam szczyt drzewa, pokazuje ręką mniej więcej kierunek. Alan "tam jest coś co wygląda jak chata. Nie ma dymu, nie wygląda szczególnie dobrze, ale jest."

Potencjał na potykacze, coś z góry, miny, granaty itp. Tu wreszcie Olaf może się przydać. Jest czujny z uwagi na potencjalne pułapki. Oprócz tego warto szukać śladów innych grup. Idziemy w stronę na chatkę. Owady tną. Czasem trzeba przejść przez kanał, w niedalekiej odległości dźwięk jakby coś wpadało do wody. Maja "tu nawet mogą być aligatory >.>".

Chatka w zasięgu wzroku, biedny Alan wchodził na drzewo kilka razy by utrzymać kierunek. Olaf szpera, szuka.

Tr Z +2:

* V: (podbicie) Do chatki jest ścieżka, Olaf i Maks widzą że jest potencjalnie mocno zapułapkowane
* (+Vg za podejście i użycie kamieni): Xz: zbyt niebezpieczne jest pozyskanie zasobów z tych pułapek
* V: (manifestacja) Maks tego nie zauważył, ale Olaf tak - wrażenie że ktoś zaznaczył bezpieczną ścieżkę między pułapkami, ale bardzo delikatnie
    * i testowanie kamieniami to potwierdza
* V: Olaf demonstruje pułapki i bezpieczne przejście. Zespół widzi, że KAŻDY jest potrzebny i "normalnie" z tego nie wyjdą. Potrzebne jest inne podejście.

Chatka. Olaf skanuje jak chatka działa - czy ktoś tu mieszkał, jak to wygląda itp.

* X: Olaf nie zna się na tym czy ktoś mieszkał czy nie. Nie wiadomo - prop czy nie. Wiedza Lily nie pomoże. Brak kompetencji.
* (+Przewaga Lily - śledczy i szczur) przeszukujemy, a bardziej 'outdoorsy' ekipa pomaga w ocenie co można scroungować. V: Lily nauczona doświadczeniem z plecakiem pokazuje Olafowi nie "piwnicę" ale skrytkę u góry. Z paczką, zgrabnie zapakowaną. Nie pasującą do chatki i całokształtu. Umieszczona by na pierwszy rzut oka nie dało się jej zobaczyć ale nie wymaga ogromnej kompetencji do szukania.
* PO RAZ PIERWSZY Olaf jest w swoim żywiole. Hiperkompetencja. Ma zbiór danych. Jest dobrym śledczym i ekspertem od ludzkiej natury. Chce określić jak DZIAŁA ten system
    * nie jest w stanie na bazie tego co ma. Za mało wydarzeń. WYDAJE się że jeśli się staramy to będzie ale część tego nie spełnia.

Olaf otwiera paczkę. W paczce: 

* całkiem dobrze wyposażona apteczka
* niezamakalne zapałki
* 'MRE' - Military Rations. (Olaf i Roman wracają myślami. Oczywiście, o smaku ryby z ryżem. OCZYWIŚCIE.)
* Maks się cieszy na podręcznik survivalu
* coś co wygląda jak mapa
* złożona kartka papieru. List.
    * "Wierzymy w inny świat, taki, w którym to co Wam zrobiono jest nieakceptowalne. Użyjcie tego co dostaliście by pomóc sobie i innym ale uważajcie - organizatorom nie spodoba się Wasze podejście."
    * (coś między hunger games i survivor reality show)
    * "Jesteście jedynym zespołem który jeszcze nikogo nie stracił"
* cieplejsze ubranie
* filtrowanie wody
* igła z nićmi...

Lily i Maja są przestraszone. Olaf: "to jest doskonała wiadomość. Po raz pierwszy rozumiemy sytuację. Jeśli ją rozumiemy, możemy przetrwać!" Widząc reakcje innych (zwłaszcza na info że zespoły tracą ludzi), Olaf kontynuuje "współpracujemy, oni też są kompetentni, mają zgrane grupy, ale my przetrwaliśmy bo walczymy razem."

Olaf kontynuuje wyjaśnienia:

* Zespoły mają nieokreśloną ilość. To zwiększa dramę. "Zdradzi, dołączy, nóż w plecy" itp. Więc - możemy dołączyć ludzi do naszego zespołu.
* Widzowie zasłużyli na show. To sprawia, że możemy dać im to show. Większy show -> większa szansa że przetrwamy.
* Ostrożność, pomaganie innym itp PLUS wysokie show sprawia, że możemy nie tylko my przetrwać ale też uratować innych.
    * Sam fakt, że inni próbują nas zabić a inni próbują nas uratować sprawia, że nasz zespół staje się ciekawy!
* Mapa jest podzielona na 9 sektorów. My zaczęliśmy w paśmie górskim, jesteśmy w lesie namorzynowym. -> Zespół: KTÓRY sektor dla nas jest najlepszy (kompetencyjnie)?
    * NIE: pustynia, CHYBA: pasmo górskie albo las namorzynowy. MOŻE: las deszczowy, tajga & tundra, sawanna (duże drapieżniki), TAK: las europejski.
        * PRZEJŚCIE: dżungla -> tajga -> las euro | góry -> pustynia -> las | dżungla -> pustynia -> las | "wszystko poza pustynią".

(Olaf wie, że this is a slight chance. Ale jeśli jego zespół jest zmotywowany, działa sprawnie i wierzy w to że może przetrwać, zmniejszamy ofiary.)

Plan Olafa - jak podejść do tego.

* sharvestować chatkę i rzeczy w lesie namorzynowym - liny, tykwy, "rzeczy które przydadzą się w górach".
* zostawić w chatce przerysowaną mapę i "chcecie pomocy - chodźcie w góry".
* w górach zbudować bazę. Mamy sprzęt, mamy możłiwości. First aid. Oprócz tego - ludzie lubią oglądać crafting, we will craft.
* w górach zbudować: "drugą bazę" gdzie zostawiamy ślady obecności by wiedzieć że ktoś tam wszedł.
* niedaleko naszej bazy miejsce, gdzie da się nas zajść. THIS SHALL BE TRAPPED. Możliwie nieletalnie.
* aha - jeśli znajdziemy inny zespół, mówicie że "ten noktianin był z noktiańskiego zespołu który wymarł i go uratowaliśmy".
* VISION: mamy bezpieczne schronienie korzystając z wiedzy i skilli i zasobów. Lepsze góry niż las euro gdzie nie ma jak dojść XD.

Olaf wyjaśnia jedną rzecz. Mowę. "Wszyscy kontra magia i chore rzeczywistości". Ludzie i magowie razem. Noktianie i Astoria razem. Mowy Olafa + "drama" - INNE zespoły dostaną hinta gdzie jest zespół Olafa i że warto tu przyjść.

RUCH INNYCH ZESPOŁÓW:

* (50-50) draw 6: (XXXXXV). "Populacja graczy zmniejsza się"

(zakłady: kto zginie PIERWSZY, Maja czy Olaf?)

## Streszczenie

Grupa porwanych przegrywów życiowych obudziła się na nieznanym terenie, w kształcie planszy 3x3, gdzie wraz z innymi grupami przegrywów mieli rywalizować o to, który zespół przetrwa najdłużej. Olaf ustabilizował swój zespół na poziomie morale i jako dekadianin filozoficznie akceptuje trudny czas. Domyślił się, że to reality show i próbował wpłynąć na widzów. Zespół zszedł z gór do lasu namorzynowego, ale zdecydowali się wrócić w góry i uratować kogo się da...

## Progresja

* .

## Zasługi

* Olaf Zuchwały: zapijaczony stoczony dekadianin, porwany do reality show. O dziwo, integruje zespół i podnosi mu morale zamiast zginąć w piątej minucie. Uratował sporo osób przed błędami i śmiercią, po czym zrobił z nich zgrany oddział. Wpadł na reality show i kombinował jak przekonać magów by wszyscy przetrwali. Zaadaptował się i próbuje wszystkich uratować.
* Lily Sanarton: Cieniaszczycki szczur uliczny; prawie wpadła w pułapkę a potem wyciągnęła z Olafem Maksa z przepaści. Szybko się uczy i umie robić pułapki z włosów. Bardzo cicha, ale podoba jej się współpraca.
* Roman Wyrkmycz: Astorianin, eks-Orbiter; pilnuje bezpieczeństwa zespołu i patrzy nieufnie na Olafa. Wziął dowodzenie taktyczne i działał w najbardziej niebezpiecznych sytuacjach (warta, padły zwierz).
* Alan Klart: Drifter; pomagał przy zejściu z gór i wspinaczce; wziął na siebie asekurację i chodzenie po drzewach. Cichy i kompetentny koleś.
* Maks Ardyceń: były strażnik parku w Drzewcu; ekspertyza w tropieniu, zna się na górach i survivalu; prawie spadł w przepaść wyciągnięty przez Olafa i Lily. Usunął pijawki z Olafa i Mai więcej niż raz.
* Maja Wurmramin: Aurum, botanistka uninterested in everything BUT. Ekspert od jadalności i roślin, kula u nogi przy wszystkim innym. W każdym z dwóch biomów znała się na roślinach i widziała, że coś nie pasuje.

## Frakcje

* Tien Lemurczak: udostępniają swoje ziemie by różne rody mogły rozwiązywać spory przez walki gladiatorów na arenie. Też dla protestujących aktywistów z niewiadomych przyczyn.
* Arbitrzy Rozwiązań Proxy: grupa Lemurczaków którzy rozwiązują problemy na wielkiej arenie survivalowej i porywają ludzi, zarabiając na sporach i zakładach. 
* Grupa Proludzka Aurum: grupa aktywistów Aurum protestująca przeciwko eksploatacji ludzi i skupiająca się na pomocy ludziom. Tym razem zrobili protest w Lemurii, ze wszystkich miejsc, przeciw grom survivalowym.

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Lemurski
                            1. Wielka Arena Igrzysk

## Czas

* Chronologia: Zmiażdżenie Inwazji Noctis
* Opóźnienie: 455
* Dni: 5
