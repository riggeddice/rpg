## Metadane

* title: "Rozpaczliwe ratowanie BII"
* threads: wojna-o-dusze-szczelinca
* motives: zrzucenie-winy-na-innych, wziecie-winy-na-siebie, ratunek-za-wszelka-cene
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [190119 - Skorpipedy królewskiego xirathira](190119-skorpipedy-krolewskiego-xirathira)

### Chronologiczna

* [190119 - Skorpipedy królewskiego xirathira](190119-skorpipedy-krolewskiego-xirathira)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

brak

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Pięknotka jest w Barbakanie w Pustogorze. Widziała na nagraniu - kilka pnączoszponów i szturmowych viciniusów plus kilka najpewniej przejętych ludzi weszło do kompleksu Tiamenat i przechwyciło sporo środków, po czym - najpewniej - wycofali się na Trzęsawisko. To, co Pięknotce tu bardzo nie pasuje - Satarail jest skuteczny ale nie jest taktykiem. A tutaj wszystko wskazuje na to, że to bardzo taktyczny manewr. Pięknotka poprosiła o wsparcie Mariusza Trzewnia, support z Barbakanu. To wygląda jak neurosprzężona taktyka noktiańska; używając noktiańskich Bii. (pkty: II)

Pięknotka wie jedno. Cokolwiek się nie dzieje, Satarail nie potrafi korzystać z noktiańskich bii. Coś tu jest nie tak.

Pięknotka poszła na "miejsce zbrodni" - Tiamenat. Wraz z Tymonem. Tymon jest katalistą i technomantą; uznał za dziwne że wszystkie rzeczy elektroniczne są zniszczone. A na wejściu do Tiamenat? Nie ma śladów pazurów. Zupełnie, jakby zostały otwarte. Bia przejęła kontrolę..?

Pięknotka wypytuje Tymona o co chodzi i jak (Tp:S). Co widział. Tymon widział coś ciekawego - pnączoszpony miały iniekcje; może w nich zagnieżdżono Bię lub Bie. Ludzie też. Co więcej, Tymon rzucił szrapnel; ma próbki krwi tych ludzi. Te próbki trafiły do Senetis. Więc jutro będą wyniki scrossowane z populacją znaną Pustogorowi. Co więcej, Pięknotka przeanalizowała obraz pnączoszponów; to nie są głębinowe (z głębi bagna) pnączoszpony; to obrzeżowe. Co więcej, jest ich kilka, ale nie są to silne pnączoszpony. (pkty: III)

Takie pnączoszpony raczej nie pokonałyby zabezpieczeń Tiamenat; gdyby nie to, że BIA wyłączyła zabezpieczenia. Więc Pięknotka ma pewien model - jest też pewna, że to nie Wiktor - bo Wiktor nie zrobiłby czegoś w takim stylu.

Pięknotka uważa, że wszystko co się dało zrobić zostało zrobione. Idzie do Barbakanu - czas popracować z dokumentami. Ładnie uśmiechnęła się do Mariusza - niech znajdzie informacje o BIA dla niej. (Tr:KS). Pkty: (IIIIII)

* ukradziono rzeczy potrzebne BIA do działania, do życia. Są tam surowce na dobry miesiąc.
* ukradziono rzeczy które pozwolą BIA na regenerację. To wskazuje, że BIA jest uszkodzona - nie ma pełnej mocy.
* BIA tym się różni od TAI że nie zrobi replikacji. Tak więc musiała być transplantowana w tamte istoty. Ale to oznacza, że mamy eksperta od BIA. Wysokiej klasy.
* środki ukradzione crossowane z BIA wskazują na BIA 3 generacji. To jest klasa Command Module. Lotniskowiec na lądzie; PIĄTEJ użyto do pacyfikacji Finis Vitae.

Następnego dnia Pięknotka dostała wyniki z Senetis. Zidentyfikowano jednego maga. To Marek Puszczok - człowiek Grzymościa.

Oki - jak najszybciej znaleźć BIA w pnączoszponach. Tracking, zanim dojdzie do usunięcia dowodów. (Tr:P). Pkty: (IIIIIII). Dwie osoby redukują do zera plazmą zwłoki pnączoszponów. Pięknotka ich wyśledziła. Pięknotka atakuje z zaskoczenia - nie spodziewają się jej tutaj.

Pięknotka jest skłonna opanować tych dwóch magów. Zaklęcie na wiązkę plazmy; są ranni i nie mogą uciec. Wysadziła ich. Leżą, zwijają się i cierpią. Pięknotka zgasiła z nich ogień; nie są ciężko ranni, ale ból uniemożliwi im czarowanie. Powiedzieli, że Pięknotka jest psychopatką; oni mieli prostą robotę. Sławek im ją zlecił za pieniądze. Mieli spalić zabite pnączoszpony. Pięknotka odstawiła ich do więzienia w Pustogorze; tam ich opatrzą i dowiedzą się wszystkiego (spoiler: nic nie ma). Dostali wpierdol za niewinność.

Sławomir Niejamnik, z Wolnych Ptaków im to zlecił. Pięknotka wie o gościu; często pełni rolę słupa. Płacisz mu za to, że nie zadaje pytań.

Pięknotka z Tymonem poszli do domku Talii; ekspertki z Noctis. Przed domkiem są twinowane narzędzia. Tymon zapukał, w pełnym power suicie. Talia otworzyła. Jest zdziwiona i lekko przestraszona. Spytała w czym może pomóc. Tymon powiedział, że ma się natychmiast przyznać. Talia przyznała się do tego, że sprzedała subturingową BIA do Cyberszkoły, by dało się poćwiczyć z wyspecjalizowanym przeciwnikiem w grze w Supreme Missionforce. Tymon jest zirytowany.

Tymon i Pięknotka wzięli Talię w krzyżowy ogień. Wyszło coś - Talia jest zbyt dobrze przygotowana. Coś wie. Nie wiadomo co. Za dużo przypadków, przykładów. Nie doszli do tego jednak co ona wie; pojawił się nowy aktor. Ernest Kajrat. Spytał ją, czemu sadystycznie skrzywdziła jego ludzi - plus, czemu nadużyła władzy. Niczego nie zrobili. Nie są winni. Pięknotka powiedziała, że to atak terrorystyczny i ON za tym stoi - ona była zmuszona. Może nawet Grzymość nie wie...

Kajrat spojrzał na nią i był ostro zaskoczony. Ona na niego. Kajrat spytał, czy wszystko powiedziała. Ona spojrzała przestraszona - i Kajrat strzelił. Pięknotka ją zasłoniła, ale Talia została ranna. Kajrat ujawnił Power Suit i odskoczył do lasu. "Catch me if you can". Pięknotka używa zaklęcia by go zamknąć w kopule z Ziemi. Dała radę. Kajrat się zaśmiał. Spytał Pięknotkę, czego ona chce teraz. Bo jak go zamknie, to Talia nie żyje. Chyba, że Pięknotka jest skłonna negocjować o życie zdrajczyni.

Cholerny Kajrat.

Pięknotka czuje, że coś jest bardzo nie tak. Kajrat zachowuje się bardzo nietypowo. (Tr: KrS). Kajrat wziął winę na siebie - powiedział, że zmusił ją do zrobienia BIA 2.5 którą chciał wykorzystać przeciwko Orbiterowi. Pięknotka to nagrała. Powiedział, że ona za dużo wie - zdrajczyni musi zginąć bo jak raz powiedziała, to teraz zawsze będzie mówić.

Pięknotka przekonała Kajrata (któremu to pasuje), że nie zamierza przesłuchać Talii. Nikt jej nie przesłucha. W takim razie - Kajrat nie będzie jej zabijał. Kajrat trafi do więzienia; Ernestowi się to bardzo nie podoba, ale jego cel jest osiągnięty.

Pięknotka jednak ma psikus, a nawet figiel. Po wszystkim udała się na Trzęsawisko. Spotkać się z Wiktorem. Powiedziała, że Kajrat go wrobił. Że był atak na laboratorium i użyto jego dzieci. Pięknotka bardzo prosiła, by Satarail oddał niepotrzebne mu chemikalia. Te, które go interesują - zachowa dla siebie tak czy inaczej. (Tr+2=R,SS). Satarail skrzywdzi sporo ludzi Kajrata i Grzymościa. Skrzywdzi też Talię - Talia próbowała chronić BIĘ przed bioformami Sataraila.

Sprawa załatwiona. BIA będzie zniszczona do końca tygodnia.

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Talia próbowała utrzymać przy życiu BIA 3 generacji. Nie mając surowców, użyła tej BIA do zdobycia rzeczy odżywczych z Tiamenat - zrzucając winę na Wiktora Sataraila. Pięknotka poszła za śladem i gdy dotarła do Talii, Kajrat wziął na siebie winę (za co Pięknotka go aresztowała). Następnie poprosiła Wiktora Sataraila by ten ochronił swoje dobre imię - i zniszczył śmiertelnie niebezpieczną BIA.

## Progresja

* Ernest Kajrat: poszedł do więzienia by chronić Talię Aegis przed jej działaniami. Nie przeszkadza mu to w zupełności.

## Zasługi

* Pięknotka Diakon: nie uwierzyła w winę Wiktora Sataraila; zlokalizowała, że to wina BIA i przesłuchała Talię. Potem złapała Kajrata (serio) i załatwiła z Wiktorem Satarailem, że BIA będzie przezeń zniszczona.
* Mariusz Trzewń: taktyk, wsparcie, hacker z Barbakanu; często współpracuje z Pięknotką. Zidentyfikował dla Pięknotki, że pnączoszpony używały noktiańskiej taktyki.
* Tymon Grubosz: pomaga Pięknotce rozwiązać problem "Wiktora" (czyli BIA) w Zaczęstwie. Osłania Pięknotkę gdy idą do Talii Aegis.
* Marek Puszczok: mag Grzymościa; twinowany z BIA. Zaatakował dla BIA Tiamenat i ukradł rzeczy potrzebne dla BIA do przetrwania. Też schował BIA w miejscu kontrolowanym przez Grzymościa.
* Sławomir Niejadek: słup z Wolnych Ptaków; płacisz mu za to, że nie zadaje pytań jak się mu coś zleca. Tym razem użyty przez Talię by ukryć ślady BIA w pnączoszponach.
* Talia Aegis: rozpaczliwie chciała uratować BIA 3 generacji; udawała, że to Wiktor Satarail zaatakował Tiamenat - a to była ona. Uratowana przed więzieniem przez Ernesta Kajrata.
* Ernest Kajrat: całkowicie nieświadomy sprawy wpakował się w kiepskiej klasy intrygę Talii. Wziął winę na siebie, dał się złapać i poszedł dla niej do więzienia na pewien czas; uzyskał jej wsparcie za to.
* BIA Tarn: BIA 3 poziomu; którą za wszelką cenę próbuje uratować Talia. Na tej sesji wielki nieobecny - przedmiot, nie podmiot.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Kompleks Tiamenat: miejsce ataku pnączoszponów kontrolowanych przez BIA; Talia zdobyła trochę surowców i Wiktor Satarail jeszcze więcej...

## Czas

* Opóźnienie: 3
* Dni: 3
