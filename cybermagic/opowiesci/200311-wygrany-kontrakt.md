## Metadane

* title: "Wygrany kontrakt"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić, darken, fox

## Kontynuacja
### Kampanijna

* [190804 - Niespodziewany wpływ Aidy](190804-niespodziewany-wplyw-aidy)

### Chronologiczna

* [190804 - Niespodziewany wpływ Aidy](190804-niespodziewany-wplyw-aidy)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* .

### Pytania

1. .

### Dominujące uczucia

* .

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

Ernest Kajrat nadal zniknięty. Amanda zresztą też.

## Punkt zero

.

## Misja właściwa

Niewielka firma "Zygmunt Zając" zajmująca się konserwami i paszą dla zwierząt oraz interesujących viciniusów stanęła w obliczu dość nietypowego sukcesu. Dagmara dowiedziała się od weterynarza, Joachima, że wygrali kontrakt. Problem polega na tym, że nigdy nikt do żadnego kontraktu nie startował. A kontrakt jest do Grzymościa - Grzymość i jego ludzie będą bardzo, bardzo nieszczęśliwi jeśli kontrakt nie dojdzie do skutku lub coś pójdzie nie tak.

No dobrze, ale "Zając" nigdy nie aplikował do żadnego kontraktu dla Grzymościa. Tak, mieli pozytywne doświadczenia z Grzymościem i Grzymościowcy im ufają - kiedyś ktoś skrewił dostawy jedzenia i "Zając" rozwiązał problem, ale ta aktualna sytuacja jest dość dziwna. Kto zatem im to zrobił?

Dagmara która ma kontakty w półświatku ruszyła w kierunku rozpoznania bojem - kto wie coś o ich kontrakcie?

* Chodzi o jedzenie dla jakiejś bardzo niebezpiecznej istoty. Coś szczególnie groźnego, bardzo ważna jest dyskrecja.
* Swego czasu siły Grzymościa się rozbiły i zostały solidnie poranione przez tą istotę; trafiły do szpitala.
* Osoby które coś wiedzą na ten temat więcej: Lucjusz Blakenbauer, Minerwa Metalia, ktoś w kompleksie Tiamenat.
* Obecność Minerwy jest bardzo dziwna na tej liście - co ma mistrzyni psychotroniki z tym wspólnego? I to władająca ixiońską energią?
* Niestety, okazało się, że "poinformowali" Grzymościa o swoich kompetencjach - teraz zupełnie nie mogą przegrać.

Na szczęście Grzymość nie wie, że oni nie wiedzą. No i cała ta sprawa zrobiła się wielokrotnie dziwniejsza.

Ziemowit Zięba, badacz otchłani i... szczególny, powiedzmy, ekspert od spraw dziwnych (oraz używania Trzęsawiska i jego energii do produkcji żywności) skupił się na tematyce dziwnego stwora. To niemożliwe, żeby nikt nic nie widział czy nie wiedział. Jeśli jest sporo mafiozów, musi być sporo informacji. Tak więc zaczął szperać po różnych rejestrach, kamerach, artykułach, dokumentach. To jest Pustogor - da się takie rzeczy znaleźć.

* faktycznie, udało mu się zobaczyć kiepskiej jakości obraz - to jest istota odludzka. Jakaś forma Skażeńca. Zmasakrowała grupę Grzymościowców, nawet z magami.
* potwór miał bazę na Nieużytkach Staszka. Wszyscy zranieni przez potwora zostali odizolowani - jakaś forma zarazy?
* faktycznie, jest powiązanie z energią ixiońską.

Osoba, która będzie sporo wiedziała na ten temat to Lucjusz Blakenbauer. Ale jak się do niego dostać? Jeleń kiedyś terminował u Blakenbauera; jest szansa, że mają nadal niezłe kontakty. I faktycznie, Lucjusz zaprosił do siebie Jelenia i dwójkę magów. Podczas rozmowy z Lucjuszem, ten powiedział troszkę więcej:

* problem jest rzeczywisty; mają do czynienia z viciniusem pochodzenia ixiońsko-ludzkiego z maga
* podpowiedział jak doprowadzić do tego, by dożywić tą nieszczęsną istotę
* wyraźnie Lucjusz próbuje pomóc

Ziemowit przeszedł się po mieszkaniu Lucjusza szukając czegoś dziwnego i nietypowego co może zwinąć do swojej kolekcji - i udało mu się zaszabrować jakiś dziwny eksperyment łączący energię ixiońską z esuriit. To nie ma prawa działać i to ogólnie jest śmiertelnie niebezpieczne i łamie wszelkie zasady "co komu gdzie i jak wolno". Niestety, Lucjusz to zauważył, jak Ziemowit szabruje. Z twarzy lekarza zniknął uśmiech i zmienił pozycję na bardziej "nie wyjdziecie stąd". Uprzejmie zaznaczył, że może wezwać deathkommando; czemu ma tego nie zrobić?

Zespół głupi nie jest - jeśli Lucjusz pracuje nad takimi energiami poza laboratorium Pustogoru, to ma coś wspólnego z Ernestem Kajratem. Zaczęli tłumaczyć Lucjuszowi problem ponownie - mają coś do zrobienia dla Grzymościa i nie wiedzą co. Teraz wiedzą więcej, ale są w sumie po tej samej stronie co Blakenbauer. Czy mogą współpracować? Lucjusz dał się przekonać - między innymi dlatego, że mają dla niego coś co mogą zrobić i mu pomóc.

* Lucjusz ma Ernesta Kajrata i próbuje go wyleczyć i utrzymać przy życiu; walka z Saitaerem bardzo poważnie go uszkodziła i Ernest nie ma sił
* Amanda, ukochana "córka" Kajrata została Skażona przez Saitaera i stała się narzędziem egzekucji Kajrata
* Naprawienie Amandy Kajrat pomogłoby Lucjuszowi naprawić Ernesta Kajrata
* Ernest jako Strażnik Esuriit utrzymuje region w stanie stabilnym
* Grzymość ma Amandę; tym potworem jest ixiońska, Skażona wersja Amandy Kajrat
* Jednak w oczach Blakenbauera, Jeleń go zdradził. Został wyczyszczony z pamięci o spotkaniu przez Blakenbauera i porzucony.

Lucjusz ma plan - jeśli Zespół ma dostarczyć żywność Amandzie, może pomóc wraz z Minerwą opracować coś co pozwoli się Amandzie od Grzymościa wyrwać. A jak do tego doda się odrobinę krwi Ziemowita, to przy okazji Amanda będzie nakierowana na Ziemowita - będzie dążyła do jego pożarcia i infekcji. Ziemowit się "bardzo ucieszył", ale to chyba jedyne sensowne wyjście.

Tymczasem Dagmara dostała dziwny sygnał - podobno Jeleń z magazynów firmowych wyciągnął "odświeżane" konserwy służące do stołówki Akademii Magicznej w Zaczęstwie i gdzieś z nimi poszedł. Tyle, że Jeleń jest z nimi. Tak więc to ktoś inny. Dagmara natychmiast wróciła na miejsce i zaczęła szukać kto to był i kto to mógł zrobić...

Oczywiście, doszła do tego. Liliana Bankierz - kto inny. Liliana, która próbuje udowodnić, że jedzenie do stołówki studenckiej jest niewłaściwej jakości i Trzeba Coś Z Tym Zrobić. Dagmara pozwoliła jej na to, by Liliana zrobiła atesty, ale... podmieniła puszki.

* Liliana została skompromitowana (poza niektórymi studentami)
* Liliana nie ma dowodów
* ale Liliana zna prawdę; "Zając" ma wroga na całe życie. Liliana wzięła to bardzo osobiście.

A tymczasem, po paru dniach pracy "Zającowi" udało się sformować odpowiednie jedzenie z pomocą Lucjusza i Minerwy. Do tego stopnia, że Grzymość dalej uważa, że "Zając" to najlepsi ludzie od niewłaściwego, dziwnego i podejrzanego jedzenia. I nie skojarzy ucieczki Amandy z ich jedzeniem. (to, że ma Pięknotkę i Mallictrix na głowie mu nie pomoże)

## Streszczenie

Ernest Kajrat jest w stanie śmiertelnym; Lucjusz i Minerwa trzymają go przy życiu. Tymczasem firma dostarczająca jedzenie "Zygmunt Zając" wygrała przetarg (na który nie startowała) odnośnie jedzenia dla zixionizowanej Amandy Kajrat w rękach Grzymościa. Współpraca Blakenbauera i "Zająca" doprowadza do uwolnienia Amandy i kupienia szansy Ernestowi...

## Progresja

* .

### Frakcji

* .

## Zasługi

* Lucjusz Blakenbauer: próbuje utrzymać przy życiu Ernesta Kajrata i opracowuje z magami z "Zająca" jak wyciągnąć zixionizowaną Amandę Kajrat z rąk Grzymościa jedzeniem
* Minerwa Metalia: współkonspiratorka z Lucjuszem Blakenbauerem, skupiła się na użyciu energii ixiońskich do pomocy Kajratowi - zarówno Ernestowi jak i Amandzie
* Ernest Kajrat: śmiertelnie ranny, utrzymywany przez Lucjusza przy życiu. Najpewniej gdzieś w Rezydencji Blakenbauerów
* Amanda Kajrat: Skażona energiami ixiońskimi przez Saitaera, wpadła w ręce Grzymościa, acz poniszczyła mu mafiozów solidnie
* Dagmara Doberman: odnalazła 
* Ziemowit Zięba: badacz otchłani "Zająca" o lepkich rękach. Pomógł w wyciągnięciu Amandy Kajrat z rąk Grzymościa.
* Liliana Bankierz: chciała udowodnić, że jedzenie firmy "Zygmunt Zając" jest niejadalne (mimo że trafia na stołówkę); włamała się i zebrała dowody. Upokorzona na wizji, stała się wrogiem "Zająca" na całe życie.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor, okolice
                                1. Rezydencja Blakenbauerów : miejsce, w którym mieszka i eksperymentuje Lucjusz Blakenbauer; tym razem nad próbą uratowania Kajrata
                            1. Zaczęstwo
                                1. Nieużytki Staszka : gdzie znalazła schronienie zixionizowana Amanda Kajrat

## Czas

* Opóźnienie: 18
* Dni: 3
