## Metadane

* title: "Adaptacja Azalii"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [200616 - Bardzo straszna mysz](200616-bardzo-straszna-mysz)

### Chronologiczna

* [200616 - Bardzo straszna mysz](200616-bardzo-straszna-mysz)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* .

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

* .

### Opis sytuacji

* .

## Punkt zero

* .

## Misja właściwa

Pięknotka, jak to ona, znalazła się w Barbakanie przed Karlą.

I okazało się, że Karla ma dla niej zadanie. Karla powiedziała, że od czterech dni Orbiter postawił na Trzęsawisku swoją Enklawę - Enklawę Aster. Celem Enklawy jest szkolenie nowej hiperadaptującej TAI klasy Azalia, co najmniej TAI 3 - wyjątkowo mała i wyjątkowo skuteczna w walce. Karla chce, by nie skończyło się to ich katastrofą - więc dostali oficera do spraw Trzęsawiska - Pięknotkę. Pięknotka poprosiła o Alana jako wsparcie, Karla zauważyła, że Alan jest ciągle ranny - a w dodatku trzeba eskortować Minerwę.

Pięknotka powiedziała, że Alan jest absolutnym ekspertem od broni - on przejdzie przez Trzęsawisko. Karla spytała, czy jest w stanie poradzić sobie nawet w sytuacji z tak zaawansowaną rekombinacją nowej eksperymentalnej TAI. Pięknotka uznała, że Minerwa + Alan sobie poradzą... ale Pięknotka nie odważy się na takie eksperymenty. Lepiej ona + Minerwa. Pięknotka boi się o Alana.

Karla też nie chce narażać Alana. Plus, Pięknotka ma dowód - Karla CARES.

Oczywiście, by było trudniej, Minerwa udaje eksperta od Trzęsawiska; nie może być przecież ekspertem od TAI.

Dropship zrzucił Pięknotkę i Minerwę w superopancerzonym kontenerze prosto do Enklawy Aster. Gdy kontener spadał, został otoczony przez Azalię plazmą. Neutralizacja bioagentów. Konrad jej pogratulował działania.

Nanoswarmowy system defensywny, autorekonfigurowalna baza. Do tego centralny, duży AI Core. Konrad przywitał się z Pięknotką i Minerwą; Azalia jednak natychmiast otoczyła obie damy kopułą defensywną. (Tr: XX). Azalia skutecznie zamknęła terminuskę i Minerwę, w bardzo precyzyjny i szybki sposób, też korzystając z slavowania Pięknotkowego power suita. Konrad się zdziwił. Azalia uznała, że obie damy są anomalne - Trzęsawisko dobrało się do nich przed nią.

Pięknotka jest zirytowana jak cholera. Żąda, by Konrad wypuścił Pięknotkę i Minerwę. Konrad nieco zgłupiał. Powiedział, że spróbuje - ale Azalia ma nad nim priorytet. Musi na tej misji, bo on może być Skażony a ona nie. Konrad poszedł przekonywać Azalię; Pięknotka bezczelnie podsłuchuje.

Konrad przekonuje Azalię, ale ta jest nieubłagana. Żąda od Pustogoru informacji o wzorze obu czarodziejek by mogła porównać. Konrad twierdzi, że nie może tego zrobić. Azalia uznała, że defensywna adaptacja jest ważniejsza niż dobre relacje z Pustogorem. Konrad poprosił Azalię, by tego nie robiła - jest czymś więcej niż tylko maszyną, niech przekroczy swoją paranoiczną adaptację. Azalia powiedziała, że to na nią nie zadziała - ale niech Karla potwierdzi że to one po otrzymaniu sygnału o wzorze. Wtedy Azalia je wypuści.

Godzinę później. Azalia wypuściła obie czarodziejki. Minerwa jest w paskudnym humorze, ale trzyma fason. Pięknotka nie lepszym. "Najpierw nas chcieli spalić, potem zamknęli w pudełku". A Azalia nalegała na dokładne badanie. Konrad bronił czci czarodziejek...

Minerwa zauważyła, że Azalia to wyjątkowo paranoiczna TAI. To chyba dobrze, ale nie dla nich.

Trzęsawisko uderzyło. Pięknotka wyczuwa zbliżający się atak wcześniej. Jest to atak o dewastującej mocy. Pięknotka próbuje ocenić stan ducha Trzęsawisko (Tr: V):

* V: Trzęsawisko **czeka**. Zasadza się. Ma dewastacyjną moc, ale to nie jest atak.
* V: Pięknotka wyczuła stratega w tle - Wiktor Satarail wspomagany wiedzą Sabiny Kazitan.
* Pięknotka czuje, że Wiktor planuje, by Enklawa nie miała pojęcia, że to jest element planu - Azalia wierzy, że broni.
* Co więcej, Trzęsawisko używa UPIORNEJ ilości energii na te dywersje. Coś, czego Wiktor nigdy by nie zrobił. Ale Sabina mogłaby...
* XX: Wiktor dotknął umysłu Pięknotki. Poczuł też obecność Minerwy.

Wiktor wysłał Pięknotce sygnał typu "Masz nowe zabawki. Chwilowo nie jestem w stanie ich pokonać. Ale będę w stanie - nie tylko to pokonać ale też WYGRAĆ". Pięknotka wysłała mu sygnał, że nie ma co do tego wątpliwości. To był sygnał z gatunku "Chciałabym im wybić z głowy pomysły odnośnie Enklawy, ale niekoniecznie kosztem ich życia". Wiktor zauważył, że nie umrą. Tu, na Trzęsawisku, nie umrą.

Atak był typu "furia Trzęsawiska". Azalia elegancko i subtelnie przepuściło energię przez swoje bariery by jedynie zamknąć wszystko potem. Bezproblemowo. A Minerwa ocenia AI i ma jeden wniosek do Pięknotki - to AI nie jest TAI. To jest mag. Zdaniem Minerwy to jest najgłupsza rzecz jaką Orbiter zrobił do tej pory. Jakaś forma cyborga. Były takie pomysły, ale wszystkie zawiodły.

Ostatni atak Trzęsawiska - ten energią magiczną - jest czymś nowym, bardzo marnotrawiącym energię i bezsensownym. Były tam też inne ataki, ale ten szczególnie zwrócił uwagę Pięknotki, bo Wiktor musiał go osobiście przeprowadzić. To poza wzorem działania Trzęsawiska. Czyli jakiegokolwiek planu Wiktor nie ma, będzie używał m.in. tego typu ataku / energii.

Minerwa zaczęła szukać drugiego dna w ataku Wiktora. (Tr3e:)

* V: Minerwa wyczuła coś niebezpiecznego. W ataku Wiktora są delikatne strandy energii ixiońskiej (!) oraz elementy rytuałów Sabiny (!). Azalia nic nie wyczuła. Minerwa twierdzi, że NIC to nie robi. Ale.
* To na pewno był test czy Azalia to wyczuje - i nie wyczuła.
* V: Minerwa wyczuła lokalizację. Wie, gdzie jest repozytorium tego dziwnego rytuału. Wie, skąd Wiktor dowodzi tą operacją. Ale jeszcze nie wie, czego chce.

Wszystkie ataki Wiktora są oszustwem. Wiktor na coś czeka, sam osłabia ataki Trzęsawiska. Marnuje energię na inne rzeczy. Czegoś chce - ale czego. A Azalia nie ma pojęcia, że walczy z inteligentnym, subtelnym przeciwnikiem... a Pięknotka niekoniecznie chce Azalii ułatwiać.

Pięknotkę czekają negocjacje z Wiktorem... poszła na zwiad na Trzęsawisko - wie, że jak ON tu jest, to ma bezpieczną drogę. Prawie. Ale wpierw - spotkanie z Konradem. Niech Konrad zrobi listę zasobów dla Enklawy. Ona pomoże je zdobyć. I niech lepiej Konrad opracuje jakąś drogę ewakuacji inną niż Castigator; nie chce przecież dowalić Pustogorowi.

Patrząc na poziom naenergetyzowania Trzęsawiska, Pięknotka nie idzie w servarze. Bierze snajperkę, lekki strój i idzie sama. I Pięknotka idzie w Trzęsawisko. Idzie, z ostatnią radą Minerwy - zdaniem Minerwy użycie energii ixiońskiej przez Sataraila może świadczyć o próbie zasymilowania Azalii.

Pięknotka idzie znaleźć Wiktora - lekko ubrana, syntetyzując odpowiednie zapachy, ze snajperką. (Tr(3)):

* XX: Musiała się przedzierać. Ostre kolce, szukające jej, trujące - ale Pięknotka syntetyzowała antidotum.
* VVV: Dotarła do Wiktora. Acz w lekko podartym stroju i pokrwawiona.

Wiktor ucieszył się, że Pięknotka przyszła. Spodziewał się. Pięknotka powiedziała Wiktorowi, że nie powiedziała Orbiterowi o nim, co zdecydowanie utrudni pracę TAI. Wiktor potwierdził. Pięknotka zażądała bezpiecznego przejścia dla siebie i Minerwy. Wiktor się zgodził, acz powiedział, że jego uwaga będzie TUTAJ - nie może jej zagwarantować bezpieczeństwa. Ale może pomóc im się wydostać STĄD.

* "Oni chcieli zabrać coś naszego. My zabierzemy im coś należącego do nich". - Wiktor
* "To jest moje Trzęsawisko... to nasze. Nie ich. Baza tutaj jest zła." - Pięknotka

Wiktor powiedział Pięknotce, że jego celem jest zabrać coś pięknego. Ma zamiar zniszczyć Castigator - orbitalną artylerię Orbitera. I Pięknotka może patrzeć, jak Wiktor to robi - nie wie nawet jak to zatrzymać. A co gorsza, na pewnym poziomie Pięknotka się z Wiktorem zgadza.

Pięknotka jednak BARDZO nie chce zniszczenia Castigatora. Niech Wiktor nie zniszczy go! Niech go PRZEJMIE! To trudniejsze i to jest "True Fear". (Ex:)

* X: Pięknotka wraca TUŻ PO ataku Wiktora. Jest plausible - wróciła najszybciej jak się dało, ale TAI nie może skorelować ataku z obecnością Pięknotki - bo jej nie ma.
* V: Wiktor się zgadza. Przejmie Castigator, nie będzie go niszczyć. Chyba, że nie uda się przejąć - wtedy ciężko uszkodzi.
* V: Wiktor nie chce nanoswarmów ani Azalii. Nie zasymiluje jej. Skazi ją.

Wiktor zaprosił Pięknotkę na herbatkę. Pięknotka zostanie z nim do rana, pooglądają piękno.

Rano.

Wiktor uruchomił moc Trzęsawiska. Atak silniejszy niż wszystkie do tej pory. Z wplecionym wątkiem energetycznym Ixionu. Cień chce atakować Wiktora, Pięknotka blokuje (Tr).

* X: Pięknotka musiała zranić SIEBIE by Cień pochłeptał
* V: Ale utrzymała nad knąbrnym servarem kontrolę

Wiktor wysłał serię fal w kierunku Azalii. Enklawy.

* "Zaadaptuj się do tego, moja piękna..." - Wiktor, do Azalii - "Moja pieśń dotknie Twojej duszy. Twojej prawdziwej duszy."

Wiktor pozwolił Pięknotce odejść. Pomógł jej w trasie, kontrolując anomalie. Powiedział, że Azalia najpewniej zniszczy tą Enklawę, ale to jeszcze potrwa.

Pięknotka wróciła. Od razu, na wejściu - NIE BĘDZIE W PUDEŁKU. Więc siedzi na zewnątrz. Konrad przekonuje Azalię, a Minerwa, przestraszona, do Pięknotki. Minerwa powiedziała, że był atak - Azalia powtórzyła tą samą sztuczkę, ale tym razem wchłonęła energię ixiońską. Z Azalią jest coś nie tak. Minerwa wyjaśniła, że Azalia jest imprintem - doprowadzonym do perfekcji tym, czym była Minerwa. I to imprintem żywego maga. Maga, który pełni rolę TAI.

Pięknotka podsłuchując rozmowę z Azalią widzi, co się jej stało. Totalna paranoja. Wszędzie widzi wrogów, wszędzie Skażenie Trzęsawiska. Subtelny atak Wiktora uderzył w jej siłę - jak to zwykle Wiktor...

Azalia wymusiła pudełko, Pięknotka freedom of movement. Minerwa zatrzymała Azalię. Nanitki nie działają. Azalia nie ma nad nimi pełnej kontroli. Azalia powiedziała, że Minerwa się myli - czy też jest Skażona? Rosnąca paranoja Azalii sprawiła, że zaczyna postrzegać wszystkich jako Skażonych. Konrad uruchamia sygnał ewakuacji - zatrzymuje go Pięknotka. Nie mogą lecieć z powietrza. Muszą na piechotę. Azalia wykorzystuje mgłę nanitkową - ona przesunie bazę jak ruchoma forteca. Powolna ale skuteczna. To jedyny, bezpieczny sposób.

Pięknotka i Minerwa opuszczają Mobile Fortress Aster. A ludzie są gotowi na spieprzanie. Nie... to nie ma sensu.

Pięknotka konfliktuje Azalię. Niech Azalia pełznie jedną drogą, ona weźmie ludzi i z nimi ewakuuje się inną drogą. Wtedy Azalia odciągnie wszystko na siebie. Azalia jest paranoiczna, jest coraz bardziej uszkodzona, ale widzi sensowność (Tr)

* X: By to osiągnąć, Minerwa ujawniła, że jest ekspertem psychotroniki. Wyjaśniła Azalii, że ta nie żyje. Konrad jest w szoku.
* V: Azalia się zgadza. Ona w jedną, Pięknotka z resztą w drugą.

Minerwa, poproszona przez Pięknotkę, przejęła część nanitek Azalii - zsyntetyzuje środki wydzielane przez Pięknotkę by dać wszystkim kamuflaż.

* V: Minerwa zrobiła wszystkim Pięknotkowy "krem" ochronny - ekran ochronny.
* V: Minerwa nanomaszynami zrobiła też serie dywersji dynamicznych bazowanych na Azalii. Sporo nanomaszyn zwinęła. Pięknotka zobaczyła jej potęgę u szczytu mocy.

Czas na ewakuację wszystkich. (TrMZ+3)

* X: W oddali Azalia zginęła. Strumień kwasu, toksyn spod ziemi, gejzer. Wielokrotne osłabiania nanitek, wyczerpywanie jej rezerw. Azalia długo walczyła, ale nie dość długo.
* VV: Dotarcie do celu.
* X: Padły nanomaszynowe dywersje
* V: Minerwa, Pięknotka, Konrad dotarli bez perma-uszkodzeń
* V: Naukowcy, część rannych, ale dotarli bez dużych strat. (80%)
* X: Zapach i syntetyki Pięknotki padły
* X: Część żołnierzy zginęło w walce. Pnączoszpony, osy, sama woda, ziemia. Trzęsawisko obróciło się jako aktywna Anomalia.
* V: 80% żołnierzy, część ciężko rannych, ale dotarli do celu.
* V: Z naukowców, jakimś cudem, ale wszyscy przeżyli.

Ekipa Orbitera jest ciężko straumatyzowana. Ale większość przetrwała. Poza Azalią - ale los hiperadaptowanej imprintowanej AI był przesądzaony...

## Streszczenie

Orbiter, frakcja NeoMil, wystawili na Trzęsawisko bazę nanitkową której celem jest uczenie odludzkiej TAI klasy Azalia. Pięknotka i Minerwa poszły jako wsparcie z Pustogoru. Okazało się, że Wiktor pozwala TAI Azalii na wiarę, że adaptuje się do Trzęsawiska by przejąć nad nią kontrolę. Pięknotka wynegocjowała bezpieczną ewakuację - ale Wiktor powiedział, że zniszczy Castigator. Pięknotka nie wie jak, ale OK. Nie wygra tego. Grunt, że wszyscy są bezpieczni.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: przeprowadziła ekspedycję Orbitera przez Trzęsawisko z pomocą Minerwy; wcześniej przenegocjowała z Wiktorem żeby im bardzo nie przeszkadzał.
* Karla Mrozik: z ciężkim sercem, wysłała Pięknotkę i Minerwę na ratowanie Orbitera - nie chciała przed Trzęsawiskiem ujawniać Minerwy, ale nie ma wyjścia.
* Minerwa Metalia: z Pięknotką na tajnej pustogorskiej operacji ratowania Orbitera/NeoMil przed Trzęsawiskiem; miała też wykryć co jest dziwnego z TAI Azalia. Używając energii ixiońskiej przejęła kontrolę nad nanitkami Azalii by wszystkich wyprowadzić.
* Konrad Wączak: brat Rozalii (której Cieniem jest Azalia d'Alkaris), agent NeoMil, frakcji Orbitera. Dowodził nanoswarmową bazą na Trzęsawisku. Uratowany przez Pięknotkę. Przekonał siostrę, że jednak da się coś z tym zrobić.
* Azalia d'Alkaris: TAI stworzone z czarodziejki, frakcja Orbitera NeoMil. Wiktor Satarail ją Skaził energią ixiońską i doprowadził do zniszczenia kopii.
* Wiktor Satarail: Orbiter próbował wejść na jego teren, na jego Trzęsawisko by wyszkolić Azalię d'Alkaris. On użył ixiońskiej energii do Skażenia oryginalnej Azalii na Alkarisie. 

## Plany

* Wiktor Satarail: zainteresowany Minerwą Metalią. W końcu też kontroluje moc ixiońską w jakimś stopniu.

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Trzęsawisko Zjawosztup

## Czas

* Opóźnienie: 5
* Dni: 8
