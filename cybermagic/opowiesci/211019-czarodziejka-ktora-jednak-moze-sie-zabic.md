## Metadane

* title: "Czarodziejka, która jednak może się zabić"
* threads: mlodosc-klaudii
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [211017 - Nastolatka w bieliźnie na dachu w burzy](211017-nastolatka-w-bieliznie-na-dachu-w-burzy)

### Chronologiczna

* [211017 - Nastolatka w bieliźnie na dachu w burzy](211017-nastolatka-w-bieliznie-na-dachu-w-burzy)

### Plan sesji
#### Co się wydarzyło, historycznie

* W okolicach Trzęsawiska Zjawosztup pojawił się oficer Saitaera. Naprzeciwko niemu stanął Wiktor Satarail - i stał się częścią Trzęsawiska.
* Noctis oddelegowało 'ON Tamrail' z eskortą by zapewnił bezpieczeństwo tego obszaru. Tamrail został Skażony mocą Saitaera i rozpadł się na terenie zwanym w przyszłości Nieużytkami Staszka.
* Siły Noctis znalazły się w krzyżowym ogniu między mocą Saitaera i Astorii. Ich bazą stało się Czółenko (stąd Esuriit w przyszłości), główne walki toczyły się o obszar Nieużytków Staszka i elementów Lasu Trzęsawnego.
* Aż Noctis zostało pokonane. Wycofali się, biedni, na Trzęsawisko Zjawosztup - gdzie już rekonstruował się Wiktor Satarail.
* Doszło do masakry...
* Teraz jesteśmy 711 dni po oficjalnym zakończeniu wojny i zmiażdżeniu sił noktiańskich. 
* Ciężko uszkodzona jednostka 'koordynacyjno-dowódcza' Ślepacz została podstawą areny migświatła. Nadmierna moc psychotroniczna na potrzeby.

#### Co się wydarzyło, na tej sesji

.

#### Sukces graczy

* powstrzymać napastników przed zniszczeniem domku Talii
* uratować Dom Noktiańskich Weteranów i potencjalnie Agostino na którego poluje Ralena (?)

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Klaudia, Ksenia i Teresa mają swój nowy pokój. Pokój jest większy, przestronniejszy i ogólnie rzecz biorąc jest _fajny_ - widać, że Arnulf zadziałał w tle. Ksenia nie wie o co chodzi, Klaudia się cieszy, Teresa jeśli coś pomyślała to zachowała to dla siebie.

Klaudia zauważyła, że Teresa praktycznie nie opuszcza kampusu - dokładniej, Teresa chodzi tylko na uczelnię i do pokoju. Klaudii to nie przeszkadza, choć Ksenia by chciała trochę inaczej. To sprawiło, że ich poprzedni pokój w akademiku stał się Centralą Planowania dla Ksenii i Klaudii. A czasem Felicjana. Woźny nic nie zauważył a jeśli zauważył, zdecydował się nie widzieć. Klaudia zauważyła od razu, że Teresa nie ma dużo ubrań, strojów itp. Owszem, ma - takie, jakie na pewno kupiłby ktoś taki jak Arnulf.

Klaudia i Ksenia stwierdziły, że tak nie może być. Wzięły Teresę i wyciągnęły ją do ciucholandów. Teresa się bardzo, bardzo opierała - Klaudia by ją zostawiła, ale Ksenia się nie zgadza. Teresa nie może tak działać. Nie może tak żyć. Ma się socjalizować. Wszyscy są potrzaskani przez wojnę. Dzieci też. Więc nikt piętnastolatce nic złego nie powie.

Tr+3:

* X: Teresa się dąsa. Opiera się. W końcu, z lekkim wstydem, powiedziała, że jej nie stać. Ksenia i Klaudia zaproponowały że jej kupią. Teresa się ABSOLUTNIE nie zgodziła. Sama zapłaci.
* V: Teresa pójdzie z nimi... ale gdy będzie miała pieniądze.
* XX: Teresa absolutnie nie chce pieniędzy pożyczyć, nie chce ich dostać, wymsknęło jej się że nawet od Arnulfa nie chciała - to co nosi to są ubrania jego córki. Dlatego nie ma nic nowego i dlatego tak o nie dba.

Klaudia i Ksenia są lekko zirytowane i bardzo rozczarowane, ale Teresa jest nieustępliwa. Spytana o co chodzi Teresa powiedziała wprost - jej ubrania są dobre, są funkcjonalne, ona przywykła do życia w niedostatku przedmiotów. PRAWIE powiedziała coś odnośnie tego jak się żyło w domu, ale się szybko powstrzymała. Ogólnie - z jej relacji przywykła do małej ilości miejsca, ogromnych wrażeń i dużo ciężkiej pracy. Więc tego tu się nie boi. Ale nie chce nic od nikogo pożyczać, bo jeszcze ta osoba umrze i nie będzie wiedziała komu oddać.

Ksenia spojrzała na Teresę z ogromnym współczuciem. Ta prychnęła. Klaudia "tak szybko Ci tu nie wymrzemy". Teresa "mam nadzieję", nie orientując się, co właśnie powiedziała. Ksenia, rozważnie, nie drążyła tematu...

Ksenia poprosiła Klaudię o pomoc - czy może pomóc jej sformować petycję. Dom Weteranów Noctis. Gdzieś, gdzie ci wszyscy ranni, nie nadający się do walki noktianie mogą być. Ci, którzy nie mogą nawet mocniej pracować. Tak, jak jest Zamek Weteranów. Gdzie? NIE W PUSTOGORZE. Myślała o Podwiercie lub Zaczęstwie, ale chyba Podwiert lepszy. Ksenia powiedziała wszystko co wymyśliła - najchętniej miałaby noktian w Zamku tak jak astorian, ale na to nikt nie pójdzie i będzie to złe także i dla nich. Wie, że w Podwiercie też są pustostany.

Klaudia od razu robi research odnośnie tego typu miejsca. Gdzie możnaby umieścić weteranów noktiańskich? 

TrZ+3:

* V: jest takie miejsce. Zniszczony kompleks przemysłowy należący do nieaktywnej już firmy. Wymaga remontu i jest w złym stanie; położony w "bezpiecznym miejscu", noktianie nie mają sensownych celów stamtąd a inne firmy mają zabezpieczenia.
* V: bardzo profesjonalna analiza i raport, z danymi historycznymi itp. Klaudia wpierniczyła się w dane operacyjne Zamku Weteranów i przygotowała proto-plan.

Piszemy petycję. Ale sama petycja bez zaplecza (popularność ze strony ludzi itp) nie będzie tak silna jeszcze. Tak więc Klaudia napisała petycję, ale wyjaśniła Ksenii, że wpierw ta musi się pobawić w promocję pomysłu. Ksenia jest lekko przestraszona KAMPANIĄ PUBLICZNĄ, ale zgodziła się. Teresa strzyże uszami, ale nic nie mówi - udaje, że coś czyta.

NASTĘPNY DZIEŃ.

Nawet Klaudia zauważyła, że Teresa jest niesamowicie nie w sosie. Mina typu "I kill you". Praktycznie wróciła do pokoju dosyć późno, nie po zajęciach. Gdy Teresa próbuje się schować na swoim łóżku, Klaudia pyta "co się stało". Teresa, jak to Teresa, odpowiada "nic". Klaudia pyta, co ją zdenerwowało. Teresa, że Klaudia nie zrozumie. Klaudia naciska. Teresa zauważa - nie są nawet koleżankami, co tu mówić o przyjaciółkach. Klaudia zauważyła, że są w tym samym pokoju (i nie ukrywa tego, że to stwierdzenie Teresy ją zabolało).

"Nie ma nic co możesz zrobić by mi pomóc. Po prostu niektórzy kolesie z AMZ są całkowicie z dupy."

Klaudia przepytuje Teresę o co chodzi. Ta nie do końca chce powiedzieć, ale trochę chce.

Tp (niepełny) Z+3:

* V: była grupa chłopaków. Pytali mnie za ile się puszczam. Powiedziałam, że ich nie stać. I wtedy spytali, czy widziałam nowe seksboty. Te, które są noktiankami. Które można bić i są bardzo realistyczne.
    * K: Ale nie wkopałaś im?
    * T: Nie umiem.

Klaudia wie, że to wyciekło. Ktoś musiał to wyciec - to, o co podejrzewali Teresę nie powinno być do publicznej wiadomości. Więc KTOŚ musiał to wyciec. Ale kto i czemu? Klaudia próbuje Teresę uspokoić, że są ludzie, którzy są dziwni... i tacy są wszędzie. Teresa powiedziała "wiem. Nic nie zrobisz i nic nie możesz. Mówiłam.". Klaudia - kwestia dyskusyjna. Teresa aż się poderwała - "nic nie rób. Stanie ci się krzywda.".

Teresa rzuciła jeszcze "ciekawe czy też by się tak uśmiechali gdyby mi rozcinali brzuch i patrzyli jak krzyczę".

Teresa NIE POWIE Klaudii kto to. Boi się o Klaudię.

Klaudia przekierowuje uwagę Teresy. Widzi, że Teresa czyta książki i ćwiczenia praktyczne odnośnie negamagii. Stwierdziła, że Teresie potrzebny jest prywatny korepetytor...

Klaudia jeszcze tego wieczoru spotkała się z Arnulfem. Ale jeszcze PRZEDTEM spytała o weteranów w Zamku Weteranów. Czego mogliby uczyć, czy chcą, czy chcą kontaktu z młodzieżą... i tacy nie anty-noktiańscy ;-).

Klaudia zaproponowała Arnulfowi że można zdobyć kadrę z Zamku Weteranów. Są to ludzie, którzy często się nudzą i mogą działać, chcieliby i choć aktywnie nie mogą, to jednak są w stanie. Problem z dojazdem? Użyje się pociągu. Jak inaczej się nie da, niech część zamieszka w tym miejscu. A to pozwoli na korepetycje tam gdzie trzeba. I Klaudia tu wspomniała o Teresie - której przydałyby się korepetycje z negamagii. Arnulf spytał, czy Klaudia już ma kilka nazwisk. Klaudia potwierdziła - od Ksenii i sprawdziła ich sama, ich kariery itp. Nie szukała nikogo z czasów wojny, raczej przed lub weterani, którzy w wojnie nie uczestniczyli. Weterani terminusów a nie wojskowych (damn shame, że nasi terminusi musieli walczyć).

Ex (ukryte rekordy, nie wszystko widać) Z (wiedza Ksenii + zaufanie) +4:

* X: prześlizgną się też 2 osoby, których Klaudia nie chciałaby tu mieć. Nie wszystko było widać + problem polityczny jak weteran CHCIAŁ pomóc.
* X: Klaudia ma kolejną straszną organizację na głowie, co dramatycznie zmniejsza jej czas. Arnulf po prostu nie ma jak jej pomóc, sam jest zarobiony.
* V: operacja się udała - Klaudia nie tylko przekonała Arnulfa ale też złożyła prospekt dzięki któremu program będzie uruchomiony.
* (+3Vg): Vz: i będzie tam dobry korepetytor dla Teresy i Teresa będzie wiedzieć, że to Klaudia + Arnulf zrobili dla niej.

W ten sposób, o dziwo, Klaudia poważnie przyczyniła się do usprawnienia wczesnych kadr AMZ by pierwsze generacje radziły sobie dobrze.

Wieczorem, po powrocie od Arnulfa, Klaudia zabrała się za poszukiwanie źródeł wycieku. Skąd uczniowie mogli wiedzieć o podejrzeniach wobec Teresy? Ślady logowania do systemu, do bazy itp. Klaudia szuka głęboko. Brak Strażniczki utrudnia i ułatwia.

Są ślady, ale świetnie ukryte i świetnie maskujące.

Ex (niesamowita kompetencja + czas) Z (Klaudia zna te systemy) +4 (czas, znajomość, uprawnienia itp):

* X: zasiedziała się za długo. Rano będzie nieprzytomna.
* V: ma informacje kto to. To Trzewń zdobył te informacje. Klaudia bez problemu rozpoznała Trzewnia po mistrzowskich patternach - sami się wymieniają często.

Klaudia zrzuca to na dysk i zaspana leci do Talii.

ACHRONOLOGICZNIE, DWA TYGODNIE DO PRZODU!

Mariusz Trzewń zatrzymał Klaudię, która nawet go nie zauważyła. Klepnął ją mocno po plecach. "Au!" Trzewń zaznaczył "wybitna organizatorka, ale nie zorganizowała nikogo by sobie pomóc". Trzewń powiedział Klaudii, że ona już sobie nie radzi. Widać to po tym co się dzieje na zajęciach. On jej pomoże. Klaudia ma wątpliwości czy on naprawdę chce. Trzewń powiedział wyraźnie - nieważne. Ale ona jest jego przyjaciółką. Najbliższą przyjaciółką. I robi cholernie ważne rzeczy. On od niej przejmie, pomoże jej i będzie dobrze. Klaudia znowu wróci do formy. "That's what SHOULD be."

Klaudia -> Trzewń, konflikt na przekazanie. Chodzi o to ile i jakich fuckupów po drodze będzie.

TrZ+2:

* Xz: Klaudia przekazuje Trzewniowi, kończy się czas, zepsuł się transport dla weteranów. Regeneracja awarii itp. Ogólnie, fuckup wizerunkowy i nie tylko w pierwszym dniu działania!
* V: Trzewń skutecznie odciąża Klaudię.
* X: Niestety, część z tych tematów jest też w rękach osób niepożądanych jak np. mafii. Chodzi o to, że jest szersze visibility na ruchy.
* V: Trzewń zbudował SIEĆ OSÓB które mogą przejmować rzeczy. Podwykonawców. Niekoniecznie najbardziej kompetentnych, ale good enough. Mniejsze tematy można zlecać.

Jedyne, co Klaudia zostawiła tylko dla siebie to terminusi. Wszystko inne działa "jakoś". Jest good enough, niektóre rzeczy nawet lepiej niż za czasów Klaudii - bo więcej osób..

POWRÓT DO WŁAŚCIWEJ CHRONOLOGII!

NASTĘPNY DZIEŃ.

Następnego ranka Klaudia idzie po kolejne lekcje do Talii. Klaudia wyszła bardzo wcześnie, wykończona i zaspana jak cholera. Ale nie może się spóźnić do Talii, bo wszystko musi być odpowiednio dyskretne. I docierając do Talii Klaudia zobaczyła ślady krwi. Walki, niedaleko domku Talii. Normalnie by zdziwiło Klaudię, ale teraz jest podwójnie ostrożna. Niedaleko domku Talii widzi takiego byczka. Koleś spojrzał na Klaudię i ją zignorował. Sprząta - ustawia krzesło, czyści ślady walki. Wyraźnie wojskowy albo były wojskowy.

Są boczne drzwi do domku Talii. Klaudia weszła do Talii więc bocznymi drzwiami...

Talia robi herbatkę.

* Co tam się stało? - Klaudia
* Nieprzyjemna grupa podburzonych astorian spotkała się z Karganem i jego przyjaciółmi.
* Kto to Kargan?
* Sympatyczny pan na zewnątrz - Talia, bez cienia sympatii - nie dzieje się tu nic, czym się musisz przejmować.

Talia uczy Klaudię używania podstawowych biostrainów. Tego dnia Klaudia jest lekko rozproszona - zmęczenie + wydarzenia. Gdy Talia zaprosiła Klaudię do warsztatu, Klaudia zobaczyła tam nieaktywnego seksbota. Klaudia zdziwiona. Talia powiedziała "seksbot". Widząc minę Klaudii - "jesteś za młoda by Ci wyjaśniać". Klaudia na to "jeśli ma się zachowywać jak noktianka, idę się upić". Talia wyraźnie uniosła brwi. "Jesteś za młoda by się upijać. Plus, czarodziejce nie wypada.". Talia dodała "chodzi o BARDZO mroczne fantazje". To moment, w którym Klaudia przestaje drążyć temat...

Klaudia wywnętrzyła się Talii o swoim problemie z terminusami - "czysto hipotetycznie, gdyby było dwóch takich żołnierzy". Czyli - Sasza poluje na Tymona. Sasza uważa, że Tymon jest powiązany z mafią. I Klaudia chciałaby jakoś sprawić, by to zaniknęło. W idealnej sytuacji Tymon zrobi coś przeciwko mafii, ale nie może to być udawane...

Talia poważnie się zastanawia nad problemem. Na razie nie widzi rozwiązania...

Wieczorem Klaudia idzie opieprzyć Trzewnia.

* Mariusz, jak mogłeś? - Klaudia
* E? - Trzewń
* Czemu rozpowiadasz zapisy z rekordów straży akademika? - Klaudia
* Harmless shit - Trzewń
* Nieprawda! Plus, to moja współlokatorka.
* Co? Od kiedy?
* 3-4 dni
* To skąd miałem wiedzieć?
* Za rzadko nas odwiedzasz.
* ...boję się Ksenii...
* Na poważnie - Teresa jest sierotą, ma zdupione rzeczy w życiu. I już jej pytali za ile się puszcza...

Trzewń się zasępił. Nie do końca o to mu chodziło. Klaudia dodała, że chcą ją z Ksenią zintegrować a to pomaga. Trzewń powiedział, że przynajmniej to jest dowód, że nie jest noktianką. Zauważył, że to niszczy plotkę, że Teresa jest noktianką - i tą plotkę udało mu się rozłożyć. Klaudia się zdziwiła - skąd taka plotka? Trzewń - krążyła plotka i chciał ją unieszkodliwić, coś musiał dać. Sporo płacili za dowód na to, że jest noktianką. Klaudia nie wie kto. Trzewń - Maryla (dziewczyna z wyższego roku). Więc chciałem położyć temu kres. Lub zdobyć nagrodę.

Ale udało się Trzewniowi zdobyć pewną kwotę jak chodzi o Teresę. Zdobyłem informacje medyczne na jej temat. Ma założony _compel_ - nie jest w stanie się zabić. Maryla to chciała. Trzewń powiedział, że nie przekazał jej nic więcej na temat Teresy.

Klaudia nie jest może bardzo społecznie dostosowana, ale widzi co to znaczy - Maryla może znęcać się nad Teresą do woli.

Trzewń twierdzi, że na AMZ jest jakiś noktianin. Maryla podejrzewała Teresę. I zdaniem Trzewnia ich patriotycznym obowiązkiem jest znaleźć tego noktianina i go wydalić. Klaudia dyskretnie westchnęła...

Klaudii w sumie coś nie pasuje. Jeśli Teresa ma _compel_, nie byłaby w stanie siedzieć na dachu. Więc ten compel musiałby być bardzo słaby. Albo Teresa - negamag - już go rozłożyła. Ale ten temat jest sam w sobie bardzo interesujący...

Jeszcze tego wieczoru Klaudia idzie do Arnulfa. Ten compel - geas - ją zmartwił.

Klaudia NADAL jest niewyspana i biedna. Idzie do Arnulfa złachana. Spytała go, czy to prawda, że Teresa ma geas zatrzymujący ją przed śmiercią? Arnulf się zasępił. Nie chce zdradzać sekretów Teresy nikomu. Powiedziała Klaudii coś takiego? Klaudia wie, że nie jest w stanie kłamać w tej kwestii. Arnulf poprosił, by powiedziała co wie. Klaudia powiedziała mu prawdę i powiedziała, że wie od Trzewnia. Arnulf powiedział Klaudii coś ciekawego:

* w papierach jest napisane, że Arnulf nałożył na Teresę ten geas by nie zrobiła sobie krzywdy po śmierci rodziców.
* naprawdę to geas nałożył na Teresę ojciec. W tym samym celu. By ją zmusić do nauki negamagii.
* tak, dobrze jej idzie. Ale Teresa - zdaniem Arnulfa - już się nie zabije. Już nie teraz. Wtedy? Tak. Teraz? Nie. Więc jej ojciec miał rację.

Klaudia powiedziała, że jest potencjalnie potrzeba zwrócenia uwagi czy ktoś się nad nią nie znęca. Arnulf powiedział, że nie jest w stanie jej pilnować. Zostawia to Klaudii i Ksenii. A raczej Ksenii...

## Streszczenie

Nastroje antynoktiańskie się nasilają. Pojawiły się seksboty do bicia, w kształcie noktian i noktianek (co strasznie zestresowało Teresę). Klaudia zaproponowała Arnulfowi powiększenie kadry o Weteranów i przejmując to prawie się zarżnęła (uratował ją Trzewń). Maryla szuka informacji o Teresie - szuka ukrytego noktianina na AMZ, ale Trzewń wykluczył Teresę dzięki dokumentom sformowanym przez Arnulfa. Klaudia i Ksenia pracują nad petycją, by stworzyć Dom Weteranów Noctis.

## Progresja

* Klaudia Stryk: następne dwa tygodnie ma przechlapane. Za dużo pracy organizacyjnej - aż Trzewń ją odciąży.
* Teresa Mieralit: miała na sobie geas uniemożliwiający jej zabicie się, założony przez jej ojca przed śmiercią. Geas już nie działa, rozproszony przez jej negamagię.
* Teresa Mieralit: nie ma pieniędzy, nie pożyczy i dlatego uważa swoje ciuchy za praktyczne. Arnulf dał jej ubrania po córce i nie kupiła nic nowego.
* Talia Aegis: ma opiekę i ochronę jakichś typów (mafii) - chronią ją przed napaścią i demonstrantami. Zajmuje się seksbotami.

### Frakcji

* Akademia Magii Zaczęstwa: AMZ dostanie kadry z Zamku Weteranów w Pustogorze. 5 osób bardzo pomocnych, 2 których Arnulf i Klaudia woleliby tu nie mieć.

## Zasługi

* Klaudia Stryk: przekonała Arnulfa, by zasilił kadrę AMZ weteranami; wzięła za dużo na siebie i gdyby nie Trzewń, wypaliłaby się. Socjalizuje Teresę. Wykryła, że Trzewń grzebał po systemach.
* Teresa Mieralit: powoli się oswaja z Klaudią i Ksenią, przez co je odpycha. Przestraszona tym, że pojawiły się seksboty "skrzywdź noktiankę". Świetna w negamagii, ma naturalny talent; rozbiła nałożony na nią geas.
* Ksenia Kirallen: z Klaudią budują petycję by weterani noktiańscy też mieli Dom Weterana. Nieskutecznie socjalizuje Teresę - ta nie chce iść na zakupy bez pieniędzy.
* Arnulf Poważny: opiekuje się Teresą; dał jej ubrania po córce. Zgodził się na plan Klaudii by zapewnić AMZ kadrę ze strony Weteranów. Wyraźnie przeładowany.
* Mariusz Trzewń: wziął na siebie część roboty Klaudii, po czym zrobił sieć pomocników - w ten sposób odpowiedzialność nie leży na samej Klaudii i wszyscy są w stanie działać. Szukał ukrytego noktianina w systemach i wykluczył Teresę jako noktiankę (co jest całkiem zabawne). Chce usunąć noktian z AMZ.
* Talia Aegis: ochraniana przez jakichś typów (mafię), konfiguruje seksboty na "mroczniejsze przyjemności".
* Maryla Koternik: z jakichś przyczyn skupiała się na noktianach i na Teresie. Płaci za informacje. Studentka AMZ ostatniego roku, dwa lata starsza od Klaudii.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Akademia Magii, kampus
                                    1. Akademik
                                    1. Domek dyrektora

## Czas

* Opóźnienie: 5
* Dni: 4

## Konflikty

* 1 - Klaudia i Ksenia stwierdziły, że tak nie może być. Chciały wziąć Teresę i wyciągnąć ją do ciucholandów.
    * Tr+3
    * XVXX: Teresa nie ma pieniędzy, nie pożyczy i nie ma niczego prawie swojego. Jak zarobi, pójdzie.
* 2 - Klaudia od razu robi research odnośnie tego typu miejsca. Gdzie można by umieścić weteranów noktiańskich? 
    * TrZ+3
    * VV: Klaudia ma takie miejsce (Podwiert) i zrobiła piękną, profesjonalną analizę i raport łącznie z oddziaływaniem.
* 3 - Klaudia przepytuje Teresę o co chodzi, czemu ta tak zestresowana. Ta nie do końca chce powiedzieć, ale trochę chce.
    * Tp (niepełny) Z+3
    * V: Teresa powiedziała o seksbotach "noktianek" które można bić
* 4 - Klaudia zaproponowała Arnulfowi że można zdobyć kadrę z Zamku Weteranów.
    * Ex (ukryte rekordy, nie wszystko widać) Z (wiedza Ksenii + zaufanie) +4
    * XXV (+3Vg) Vz: 2 niewłaściwe osoby; Klaudia nie ma czasu na nic; Klaudia przekonała Arnulfa i Pustogor; będzie tam dobry korepetytor dla Teresy
* 5 - Wieczorem, po powrocie od Arnulfa, Klaudia zabrała się za poszukiwanie źródeł wycieku. Skąd uczniowie mogli wiedzieć o podejrzeniach wobec Teresy?
    * Ex (niesamowita kompetencja + czas) Z (Klaudia zna te systemy) +4 (czas, znajomość, uprawnienia itp)
    * XV: rano nieprzytomna ale wie kto to - Trzewń
* 6 - Klaudia -> Trzewń, konflikt na przekazanie roboty. Chodzi o to ile i jakich fuckupów po drodze będzie.
    * TrZ+2
    * XzVXV: fuckupów kilka zwłaszcza koło weteranów; Trzewń odciąża Klaudię i robi siatkę osób; mafia i niepożądane osoby mają visibility 

## Projekt sesji

* Overarching Theme:
    * Theme & Feel: 
        * .
    * V&P:
        * .
    * adwersariat: 
        * .
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * Role i postacie
        * Olaf Zuchwały -> chce być łącznikiem między Miasteczkiem + Kryształowym Smokiem a Astorią.
        * Ksenia & Felicjan -> chcą normalizacji Noctis - Astoria.
        * Sasza & Protazy -> chcą ujawnić prawdę o Strażniczce i zdradzie Tymona i Arnulfa
        * Ralena Drewniak -> chce zniszczyć wszystkich noktian. Nie może się to tak skończyć. Terminuska.
        * Mariusz Trzewń -> bardzo antynoktiański; napadli i jeszcze to wszystko.
        * Teresa Mieralit -> na krawędzi dachu, na granicy śmierci podczas burzy.
* Co się działo w przeszłości
    * 
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * Sasza dojdzie do tego że Strażniczka jest anomalna.
    * Ksenia i Felicjan --> zdrajcy.

Dane projektowe:

* "27 dni po poprzedniej"
* Ksenia -> Klaudia, „Zamek Weteranów dla noktian”
* problem przekonania że tymi weteranami też trzeba się zająć.
* Petycje, działania, Ksenia na froncie a na banku Klaudia
* Duże wsparcie Felicjana - jeśli im nie pomożemy, w czym jesteśmy lepsi?
* Strażniczka Alair jest niestabilna - za mało energii, za dużo żądamy, pobiera Skażoną energię… Klaudia pełni rolę ukrywacza gdy Tymon i Talia próbują jakoś to skorygować.
* Felicjan -> Klaudia: niech Augustino dostanie jakieś podstawowe miejsce i sprzęty w okolicach Lasu. Sam tam zginie. A próbuje pomagać. 
* KONIEC SESJI: podpalenie domu noktiańskich weteranów. Ksenia wyciąga weteranów, nie osoby które podpaliły. Felicjan jej pomaga. 
* Oddział Kryształowy Smok (noktianski) w okolicy Pacyfiki. Zajęli ten teren. Pustogor nic nie robi - nie ma na to sił.
* Tymon ma problemy z lokalnym terminusem, Saszą Morwowcem. Sasza uważa, że Tymon ma swoją agendę i ogólnie nie działa na korzyść Pustogoru. Sasza robi query. Klaudia to zauważa.
* Ogólna niechęć astorian do noktian. Próby linczu (Felicjan x Ksenia świadkami, oni próbują temu zapobiec i zostają pobici).

Sceny:

* Ksenia chce by Klaudia jej pomogła napisać petycję o Dom Noktiańskich Weteranów.
* Strażniczka jest niestabilna. Drony atakują studentów (Arnulf przedstawia to jako ćwiczenia ). 
    * M.in. Polują na Teresę Mieralit. 15-letnią czarodziejkę mieszkającą chyba na terenie akademii.
* Problemy z integracją Strażniczki i jej komponentów. A z drugiej strony węszący terminus Sasza wraz z ichnim psychotronikiem, Protazym.
* Ralena chce zniszczenia Domu Weteranów Noctis, podburza ludzi.

Dramatis personae (na potem):

* Protazy Tukan: psychotronik lojalny Saszy; 
* Felicjan Szarak: pragnie normalizacji Noctis - Astoria; 
* Ralena Drewniak: terminuska z nienawiścią ku Noctis;
* Ksenia Kirallen: pragnie Domu Weteranów dla Noctis; 
 