## Metadane

* title: "To nie pułapka na Nereidę..."
* threads: legenda-arianny, dotyk-saitaera, spustoszenie
* gm: żółw
* players: fox, kapsel, kić

## Kontynuacja
### Kampanijna

* [211222 - Kult Saitaera w Neotik](211222-kult-saitaera-w-neotik)

### Chronologiczna

* [211222 - Kult Saitaera w Neotik](211222-kult-saitaera-w-neotik)

## Plan sesji
### Co się wydarzyło

* ?

### Co się stanie

* _patrz operacje_
* Red Gauna x Beast <-- Natalia
* Infernia "SOS!!!" <-- Natalia -> Gauna
* Atak memetyczny
* Śmieci na poligonie

### Sukces graczy

* Inteligentna ewakuacja ludzi?
* Zatrzymanie kaskady Spustoszenia?
* Jakie jest źródło energii ixionu?
* pająkostwór ixioński
* blokada payloadu memetycznego do Inferni / Diany.

## Sesja właściwa
### Scena Zero - impl

* Zasoby:
    * Elena ma Nereidę, uzbrojoną
    * Macie uprawnienia na ćwiczenia
    * Gdzieś na poligonie są koloidowe korwety, jesteście w stanie je wykryć
    * Adam Szarjan i Kasandra Destrukcja Diakon są po Waszej stronie
    * Mamy Tivr (nadaje się do walki) i Infernię (nie do końca nadaje się do walki).
    * Na poligonie są śmieci kosmiczne, cele itp.

### Scena Właściwa - impl

Idea - przechwycić koloidową korwetę i sprawdzić co to jest.

Klaudia idzie do Marii. Mówi, że potrzebny jej Martyn. Klaudia martwi się Eleną - chce jej pomóc, nie chce jej skrzywdzić. Maria powiedziała, że Elena jest bardzo niestabilna, ale się utrzymuje. Z perspektywy morale gorzej, ale z tym Elena powinna sobie poradzić (Maria powiedziała to głosem osoby nie rozumiejącej morale). Maria zauważyła - Eustachy jest stabilny, dobrze połączony z Infernią. Zastanowiła się - złożyć feromony do pomocy Elenie. Musi przebadać jej Wzór. 

Maria ma krew Eleny. Z Klaudią przeprowadzają badania. Jak zrobić feromony. 

ExZ (komponenty krwi Eleny, Eustachego itp) M+3

* O: Energia badań -> ixion. Ta energia została wchłonięta przez kokon Natalii. (ELENA JEST WYCZUWALNA). Większe zasilanie i stabilizacja kokonu Natalii.
* X: Pierwsze zbudowane feromony będą miały nieprzyjemny zapach dla wszystkich którzy nie są Eleną. Tivr będzie śmierdzieć. Plus potrzebna jest do tego próbka Eustachego.
* Vz: Udało się złożyć pierwsze feromony. One będą uspokajać Elenę. Spowolnią jej atrofię. Utrzymają ją stabilniejszą emocjonalnie.

Klaudia i Adam składają antydatowaną pilną prośbę o 10 mechaników do pomocy w Stoczni Neotik z uwagi na braki w personelu i konieczność działania z mniej poufnymi komponentami.

Tr+3:

* X: Neotik ma już pewne ograniczenia. Za dużo żąda, za mało dostarcza. Więc nie wyślą najlepszych najpilniej.
* V: Ale wyślą tak, by byli w ciągu 4 dni. Za 4 dni będzie 10 wystarczjąco kompetentnych mechaników.
* V: To będą mechanicy nie powodujący dodatkowych kłopotów. O dziwo, przeszło to bez kłopotu.

W tej chwili Infernia w tej chwili dostaje elementy mechaniczne. Do naprawy statku. Beztwarzy próbują coś z tym zrobić i powoli to idzie. Ale Infernia regeneruje się szybciej jeśli dostanie źródła energii, komponenty organiczne, odpady komunalne, elementy Nereidy, krew...

Diana powiedziała - ona próbuje zrobić, by dziecko miało umiejętności JEJ (Diany), Eustachego i prawie wszystkich donorów energii. Arianna poprosiła Stocznię o statek wsparcia (tankowiec) do ich doku, by móc zasilić Infernię - bo to pomoże Natalii docelowo. Arianna + Eustachy się tym zajmują.

Arianna przekonuje, że muszą dostać tankowiec.

TrZ+3:

* X: Jedyny tankowiec jaki dostaną to tankowiec dostosowany do Nereid. Plus, wiemy, że Skażenie nie dostało się do Nereid, więc ten jest spoko.
* V: I dostaną ten tankowiec.

Ma wystarczającą moc. Infernia może działać na dużej mocy. Nie wyczerpuje resztek energii. 

Eustachy dostosowuje tankowiec do Inferni. Złączki, transformatory itp. Żeby Infernia nie zeżarła za dużo itp. Plus uodpornić obie strony

TrZ+3:

* V: Sukces. Udało się, będzie działać płynnie. Ale Eustachy zauważył coś ciekawego - tankowiec jest dostosowany do TRZECH Nereid. A były tylko dwie.

Czas zaplanować "ćwiczenia". Takie, by upolować koloidowy statek Nereidą lub Tivrem. I przetestować przypadkiem działo anty-ixiońskie. Poligon działa na naszą korzyść, bo pełno odpadków. Przeciwnik się ukrywa tak, że ma podgląd. A my z grubsza wiemy gdzie są, możemy korzystać z sensorów stacji. Eustachy robi wspólne skanowanie - czujniki Inferni, czujniki stacji, pełny pogląd na rzeczy dookoła, żadna jednostka niezaplątana. Żadnych obserwatorów postronnych. Bezpiecznie strzelamy w odpadki, bo się da. A "przypadkiem" strzelamy w ukryty koloidowiec. I nadajemy na otwartym kanale "przestrzeń przeskanowana, mamy cel, to te odpadki". Podajemy błędne sygnały. Nereida jest zwrotna, Tivr do niej strzela. Tivr "ma zestrzelić" Nereidę "ślepakami". A jak Elena wie gdzie ma lecieć, to poleci.

Faza 1: lokalizowanie statków koloidowych w dzień ćwiczeń. Nowa broń, nie wiemy jak działa, skanujemy czy ktoś się nie pałęta. Infernia też jest na poligonie. Oficjalne sensorami stacji (tam gdzie na pewno ich nie ma) a prawdziwe Infernia tam, gdzie stacja nie dociera. Stacja ma zawęzić a Infernia znaleźć. Koloidowe statki mają wiedzieć GDZIE MA ICH NIE BYĆ. I mają wiedzieć, że tam będzie Nereida, która jest podatna przez nich na potencjalne przechwycenie.

Próbki pobrane przez Klaudię Klaudia bada pod kątem samej Nereidy i kokonu. Czy to co wiemy o ixionie, Saitaerze itp sprawia, że wiemy dlaczego Natalia straciła kontrolę?

Ex (chore energie, przeszłość) Z (próbki) M (jej moc) +3:

* O: badania Klaudii spowodowały, że energie "rozpętały się" poza same próbki. Ale energie zostały przez Dianę? lub przez kokon? przekierowane i kokon to wchłonął. ZNOWU.
* V: Nereida nie przejęła Natalii "po prostu". Było coś, co wzmocniło ten efekt. Coś zdestabilizowało link Natalia - Nereida. Zewnątrz. Memetic payload. Pozytywne wsparcie.

Klaudia łączy się z danymi historycznymi - gdzie znajdowały się korwety gdy walczyli z Nereidą? Co interesuje korwety?

ExZ (backdoor) +3:

* X: Musicie podpiąć Infernię / Dianę by zadziałała lepiej. By wzmocnić sygnał i moc przetwarzania danych.
* Xz: Diana i Hestia zalewają się ogromną ilością informacji. Hestia nie lubi Diany. HIDDEN MOVEMENT.
* X: Macie informacje o celach korwet - korwety faktycznie polują na informacje o Nereidzie. Korwety bardzo interesują się bytami ixiońskimi. Memetic payload.

Diana jest szczęśliwa i dumna. Ma wszystkie informacje o Nereidach jakich mogą potrzebować. Wykazała proaktywność - dla Eustachego.

Faza przeprowadzenia ćwiczeń:

* Cel: zdobyć koloidową korwetę by zrozumieć czego chcą. Ze świadomością, że gdzieś jest trzecia strona, która może chcieć przejąć Infernię.
* Klaudia buduje coś, co chroni Infernię przed tym payloadem.

Arianna - chcemy zablokować sygnał idący w ELENĘ. "Wszyscy umarli przez Ciebie". Żeby ona się nie zdestabilizowała. Bo będzie kłopot. (jak się pojawi, wsteczne). A Maria, cichutko, udała się do Eustachego. Ma dla Nereidy dyspozytor feromonowy. Eustachy -> Arianna, niech ona zwoła dowództwo, testujemy broń itp. By Elena tam była. A on wmontuje dyspozytor feromonów w chorej ixiońskiej jednostce. Arianna jest za. Robimy to. Ale nie "dowództwo" a spotkanie w Tivrze - nowe dowództwo Tivra. Noktianie itp. Lutus dowodzi, Elena pierwszy oficer.

Eustachy infiltruje Nereidę. Ma kody dostępu, ma sprzęt, ma uprawnienia, ma wszystko. Ale Nereida jest jednostką inną. Eustachy z pomocą Diany (i jej wiedzą) wmontowuje w Nereidę dystrybutor feromonów.

TrZ (Diana i jej szeroka wiedza o Nereidach) +3:

* V: Zamontowany dyspozytor, nawet się nie spocił.
* Vz: Elena nawet nie wie, że ktoś majstrował i dystrybutor ma autoregenerację. Nie "na zawsze", bo brakuje rzadkich środków, ale trwa dłużej
* V: Łatwo można dodawać feromony do Nereidy.

Kto jest gdzie podczas operacji "ćwiczenia".

* Elena -> Nereida
* Eustachy -> Tivr
* Adam -> Tivr (pilot zamiast Eleny)
* Arianna -> Infernia
* Klaudia -> Infernia
* Maria -> Infernia

Arianna -> Tivr. Morale. Kohezja. Większość noktian na Tivrze + osoby bojące się Inferni (większość to noktianie). "Chcieliście Tivr, możecie mieć. Flagowa jednostka Noctis. Ludzkość vs anomalie, nie zaiwedziecie tym razem". A Elena - bo wiedzą co ona umie + komunikacja z kuzynką.

TrZ+4:

* VVzVz: walczą dla Arianny tak jak by walczyli dla Noctis. Kapitan Lutus jest zaakceptowany. Elena zaakceptowana. Spójna jednostka, a Lutus ma zamiar zrobić z Tivru "jednostkę flagową". TAK, łykną, że Tivr śmierdzi feromonami dla Eleny.

Operacja "ćwiczenia".

Eustachy steruje operacją z Tivr. Planuje ćwiczenia bezbłędnie. Spodziewając się pułapki. Bo tu MUSI być pułapka.

ExZ (wsparcie, czujniki, plan) M (integracja z Infernią + wzmocnienie obliczeń) +4 + 3Vg (wzmocnienie Inferni) +3Og (korozja Inferni)

* Vm: Eustachy płynnie i perfekcyjnie przeprowadził manewry. Wie, gdzie są koloidy.
* Vm: Eustachy chciał strzelić Tivrem. Infernia zestrzeliła jedną z korwet; Elena leci w kierunku drugiej.
* X: Wszystkie jednostki są na swoich pozycjach. Nie ma opcji łatwej ewakuacji.
* Xz: Jesteśmy w obszarze częściowo zaminowanym. Jest tu coś jeszcze.
* V: To nie jest pułapka na Nereidę. To jest pułapka na Infernię. Infernia jest zagrożona i jest w tej chwili na pozycji niebezpiecznej. Klaudia wykrywa obecność trzeciej Nereidy. W kosmosie. Jest gdzieś tu. Tivr UDAJE że leci by pułapka zadziałała
* V: Pułapka odpalona, ale macie wszystko w rezerwie.

Diana jest zamrożona. Killware. Ściągnęła przy Nereidzie. Eustachy ma ten sam poziom zamrożenia. Eustachy "Diano, jak się nie ockniesz, oddam Cię Elenie!". (+1Vv)

* X: Operacja trochę potrwa
* Om: Diana jest zamrożona. Ale Eustachy jest Infernią. Diana i główny kontroler są w pętli, ale Eustachy tu działa. Kokon przerasta Infernię. Nereida 3 się ruszyła.

Pierwszy pomysł - wysadzenie pokładu, odstrzelenie tego. Arianna ma inny pomysł - użycie Esuriit z kultu, i wysłanie fali "zgnić" to wszystko. Rozstawić kultystów dookoła strefy, zrobić z tego barierę.

ExMZ+3+5Om+3Og:

* Om: Energia Esuriit wylała się z kultystów. Zalewa dolne pokłady. Arianna eskaluje energię.
* X: Ci kultyści nie żyją. Pojawiają się manifestacje Esuriit na pokładzie. Diana wytrącona i wróciła do siebie, Eustachy też. Infernia ma koszmarne uszkodzenia.
* Om: Całość energii przepływa do jaja. Infernia jako jednostka zaczyna umierać. Szok i wstrząs. Esuriit jest wszędzie. Burza magiczna. 
* O: zniszczenia są dużo mniejsze niż się wydaje. Twój kult - Iza - coś robi, by to zatrzymać. Iza próbuje to wciągnąć w siebie.

Klaudia wysadza niższe deki. To cholerstwo "odpada" od Inferni. Nereida 3 NIE CHCE lecieć w tym kierunku. Ale pomiędzy Tivrem i jakąś formą przyciągania... jajo i Nereida się sprzęgły.

Eustachy - Diana jest w głębokim szoku. Diana się rozpada. Mentalnie, psychicznie, energetycznie. Eustachy bierze najpotężniejszy stymulant jaki istnieje. Łączy się z Infernią i odciąża Dianę jak tylko jest w stanie. Ratuje Klaudię, Ariannę, kluczowe postacie i części załogi. Akceptujemy zniszczenia Inferni ale kluczowe komponenty mają być. Wyłączamy co się da.

HrMZ (stymulant) +4 +3Vg (integracja + poświęcanie)

* Vz: Uda się utrzymać szkielet Inferni i załogi.
* O: część energii przekierowała się na Tivr. Energia zalała Tivr i tu też się dzieją rzeczy. +2Vg
* Vg: Diana wyjdzie bez szkód psychicznych. Infernia wróci do formy szybciej. Mniejsze straty w załodze. Jest... lepiej. Eustachy przeżyje tylko dzięki Marii.

Klaudia używa fragmentów Nereidy. Wszystko w kulkę. Wszystko przekierowane w Izę (która sobie to robi). Klaudia przekierowuje energię w Izę. Klaudia pełni rolę super-katalisty. Ale Klaudia nie zaryzykuje Skażenia Arianny. Nie w tej chwili. Nie w tej sytuacji. 

HrMZ (anomalie Klaudii) +4 + 3Vg (za działania Eustachego) + 3Vg (za działania Eleny)

* Xm: Iza + kilkanaście osób z kultu byli potrzebni do opanowania energii.
* V: udało się containować Skażenie. Infernia nie będzie zniszczona / bardzo poważnie Skażona. Nie stanie się Serenitem. Klaudia usunęła kody kontrolne Saitaera z Inferni.
* X: Elena jest zdjęta z akcji. Ratowała kogo się da, ratowała załogę, wszystkich itp - i padła w boju. Ale żyje. "Jej Skażenie większe nie grozi".
* V: ogromna część załogi jest możliwa do uratowania. Maria ma co robić.

Maria Naavas dwoi się i troi, by używając wszystkich swoich zdolności utrzymać jak najwięcej załogantów Inferni.

ExZ (wsparcie medyczne + przejęła dowodzenie) M + 3Vg (Elena, Leona itp) +2:

* V: Marii udało się pomóc sporej ilości osób
* Xz: ci, którzy jej pomagali ucierpieli najmocniej. 2-3 miesiące regeneracji.
* Vm: Marii udało się pomóc 85% osób, których dało się uratować. Plus, zapewnia wysokie morale.
* X: Maria przekierowywała wszystkie rany, szkody, zniszczenia itp na osobach w śluzie, m.in. na Izę. Maria ma opinię "POTWORA".

Arianna "spójrzcie do czego to wszystko doprowadziło. Pozbywamy się potworów. Pozbyć się zła z Inferni". Wpływ na kult. Odzyskanie kontroli nad kultem, pozbycie się figurek.

ExZ (Iza i sytuacji) +4:

* V: Kult skierowany przeciwko potworom i anomaliom. Pro-ludzki kult. Ludzie vs anomalie. Arianna stawia Ottona jako tego co OD POCZĄTKU nie powinniśmy. Po wojskowemu. +2Vg.
* X: Mamy sekty. Sekta "Vigilusa=Nihilusa" i sekta Arianny.
* V: Kult dominujący to kult Arianny. To jest dominanta. Ale nadal kontroluje go Kamil i inni prorocy a nie Arianna. Otton jest PRAWDZIWYM arcykapłanem Arianny +Vg
* X: Kamil nie żyje. Otton "arcykapłan" kultu Arianny (nie chce i nie wierzy w Ariannę).

Lutus dowodzi obroną Tivra przed anomaliami i magią. Nie do końca wierzy w magię. Noktianie. Arianna może sobie wierzyć w magię, ale bądźmy szczerzy - to nie do końca to. Adam przejmuje dowodzenie jako "ekspert od nanomaszyn i technologii ixiońskich i mistrz technobełkotu".

TrZM+3:

* XX: podstawowe linie obrony zostały złamane i Tivr też wymaga naprawy, ma niewielkie straty w ludziach. Giną nie-noktianie bo wiedzą co to Esuriit i się boją. Noktianie nie wiedzą co to Esuriit więc nie giną.
* X: Adam i Kasandra Destrukcja przekierowali energię używając dział Tivra i przekierowali całość energii do tamtej zdobytej koloidowej korwety.
* O: "Jajko", czyli Natalia pozbywa się tej energii. Natalia podpina się do Tivru, wchłania energię i to jest wystarczające by się obudziła. Po czym, niestabilna jak cholera, odlatuje. A Tivr stracił energię.


## Streszczenie

Plan - dorwanie tajemniczych koloidowych korwet. Wiemy, że to pułapka, więc zróbmy kontr-pułapkę. Ale okazało się, że to nie pułapka na Nereidę a na Infernię. Standardowe badania wprowadziły w Infernię (Dianę) memetic payload i Diana prawie wpadła pod kontrolę Saitaera - odrzucenie kokonu z Natalią sprawiło, że udało się ciężko uszkodzoną Infernię uratować. Zwalczając ixion Arianna sięgnęła do Esuriit przez Elenę; Infernia by zginęła gdyby nie samobójcza akcja Izabeli i Kamila. Elena wchłonęła tyle energii ile była w stanie, przekraczając swój Wzór. ALE - Infernia przetrwała. Przy okazji, Natalia się wykluła. Ani osoba ani statek kosmiczny - mordercza anomalia kosmiczna.

## Progresja

* Arianna Verlen: przejęła kontrolę nad kultem i pozycjonowała Ottona na arcykapłana po śmierci Kamila.
* Eustachy Korkoran: ciężko ranny, sprzężony z Infernią. Przeżył tylko dzięki niesamowitej skuteczności i bezwzględności Marii Naavas.
* Maria Naavas: z uwagi na swoje bezwględne, spokojne triagowanie i przekierowywanie energii została uznana za potwora przez członków Inferni. Maria spokojnie to akceptuje.
* Elena Verlen: zniszczony Wzór, pomiędzy ixionem i esuriit. Dwa potężne ryczące oceany a pomiędzy nimi jedna mała osobowość Eleny.
* Elena Verlen: perfekcyjny pilot Nereidy. Perfekcyjna anomalna integracja z pojazdami. Jej eidolon i ona stanowią jedność.

### Frakcji

* .

## Zasługi

* Arianna Verlen: zbudowała z noktiańskiego Tivra perfekcyjną maszynę wojskową, po czym opanowała kult. Niestety z uwagi na problemy z magią w obliczu Saitaera uwolniła Esuriit i prawie zniszczyła Infernię. 
* Eustachy Korkoran: zintegrował się z zamrożoną Infernią (payload memetyczny) by uratować Dianę przed korupcją Saitaera. Ciężko ranny, ale udało mu się. Ratował życie WSZYSTKICH ryzykując swoim oraz Infernią. Opracował genialny plan, ale naprzeciw niemu stanął osobiście Saitaer.
* Klaudia Stryk: Desperacko próbując ratować Infernię działała jako super-katalistka przepuściła przez siebie więcej energii niż powinna. By ratować Infernię i wszystkich innych przekierowała energię w Izabelę, niszcząc ją.
* Maria Naavas: z Klaudią złożyła feromony by spowolnić destabilizację Eleny; ixiońska energia wpłynęła do kokonu Natalii. Cichy MVP operacji - dzięki jej działaniom medycznym i stabilizacyjnym (magiczne i nie) udało się uratować większość załogi przed strasznym losem.
* Elena Verlen: w kryzysowej i krytycznej sytuacji przekierowywała energie w lewą i prawą pomagając Izie, Klaudii i Kamilowi. Jej wzór uległ uszkodzeniu w sposób nieosiągalny. Poświęciła się dla Inferni i przyjaciół. ŻYJE ALE.
* Izabela Zarantel: widząc że Infernia umiera i energie przechodzą poza kontrolę przekierowała (z pomocą Klaudii) całość energii na siebie jak kiedyś Martyn. Po czym opuściła Infernię. Bez skafandra. KIA.
* Kamil Lyraczek: oddał życie, by tylko Infernia i kult mogły przetrwać atak Saitaera. Utrzymał memetycznie wiarę w Ariannę, w integralność Inferni. Wierzył nawet gdy miażdżyły go wrota. Nie miał lekkiej śmierci. KIA.
* Lutus Amerin: trwały kapitan Tivr; obronił Tivr przed anomaliami i magią. Tivr i Lutus mają nieskończoną wiarę w Ariannę i jej moralność.
* Natalia Aradin: wykluła się, jako synteza energii Eleny, Arianny, Esuriit, Ixionu i kilku innych kanałów. Stała się anomalią kosmiczną na chassisie Nereidy.
* Saitaer: zaplanował przechwycenie Inferni, Eleny, lub Arianny. Co najmniej Natalii. Co prawda prawie zniszczył te wszystkie jednostki, ale nie udało mu się uzyskać ani jednego agenta. Nawet Natalia jest wolną istotą.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Pierścień Zewnętrzny
                1. Stocznia Neotik
                1. Poligon Stoczni Neotik
 
## Czas

* Opóźnienie: 3
* Dni: 5

## Konflikty

* 1 - Maria ma krew Eleny. Z Klaudią przeprowadzają badania. Jak zrobić feromony do pomocy Elenie
    * ExZM+3 OXVz
    *  Energia badań wchłonięta przez kokon Natalii. Większe zasilanie i stabilizacja kokonu Natalii. Tivr będzie śmierdzieć. Feromony udane
* 2 - Antydatowana pilna prośba o 10 mechaników do pomocy w Stoczni Neotik 
    * Tr+3 XVV
    * Nie wyślą najlepszych najpilniej, le wyślą tak, by byli w ciągu 4 dni. To będą mechanicy nie powodujący dodatkowych kłopotów.
* 3 - Arianna przekonuje, że muszą dostać tankowiec.
    * TrZ+3 XV
    * Jedyny tankowiec jaki dostaną to tankowiec dostosowany do Nereid, ale go dostaną
* 4 - Eustachy dostosowuje tankowiec do Inferni.
    * TrZ+3 VOV
    * Będzie działać płynnie. Tankowiec jest dostosowany do TRZECH Nereid. Energie "rozpętały się" poza same próbki i kokon to wchłonął. Coś zdestabilizowało link Natalia - Nereida. Zewnątrz. Memetic payload. Pozytywne wsparcie.
* 5 - Klaudia łączy się z danymi historycznymi - gdzie znajdowały się korwety gdy walczyli z Nereidą? Co interesuje korwety?
    * ExZ (backdoor) +3 XXzX
    * Musicie podpiąć Infernię / Dianę by zadziałała lepiej, Diana i Hestia zalewają się ogromną ilością informacji. Hestia nie lubi Diany. HIDDEN MOVEMENT. Macie informacje o celach korwet - korwety faktycznie polują na informacje o Nereidzie. Korwety bardzo interesują się bytami ixiońskimi. Memetic payload
* 6 - Eustachy infiltruje Nereidę.
    * TrZ (Diana i jej szeroka wiedza o Nereidach) +3 VVzV
    * Zamontowany dyspozytor, nawet się nie spocił. Elena nawet nie wie, że ktoś majstrował i dystrybutor ma autoregenerację. Łatwo można dodawać feromony do Nereidy.
* 7 - Mowa inspiracyjna Arianny
    * TrZ+4 VVzVz
    * Walczą dla Arianny tak jak by walczyli dla Noctis. Kapitan Lutus jest zaakceptowany. Elena zaakceptowana. Spójna jednostka, a Lutus ma zamiar zrobić z Tivru "jednostkę flagową". TAK, łykną, że Tivr śmierdzi feromonami dla Eleny.
* 8 - Operacja "ćwiczenia"
    * ExZ (wsparcie, czujniki, plan) M (integracja z Infernią + wzmocnienie obliczeń) +4 + 3Vg (wzmocnienie Inferni) +3Og (korozja Inferni)  VmVmXXzVV
    * Eustachy wie, gdzie są koloidy. Eustachy chciał strzelić Tivrem. Infernia zestrzeliła jedną z korwet; Elena leci w kierunku drugiej. Wszystkie jednostki są na swoich pozycjach. Nie ma opcji łatwej ewakuacji. Jesteśmy w obszarze częściowo zaminowanym. Jest tu coś jeszcze. To jest pułapka na Infernię. Pułapka odpalona, ale macie wszystko w rezerwie.
* 9 - Eustachy budzi Dianę ze stuporu "Diano, jak się nie ockniesz, oddam Cię Elenie!". (+1Vv)
    * XOm
    * Operacja trochę potrwa. Diana jest zamrożona. Ale Eustachy jest Infernią. Diana i główny kontroler są w pętli, ale Eustachy tu działa. Kokon przerasta Infernię. Nereida 3 się ruszyła.
* 10 - Arianna: użycie Esuriit z kultu, i wysłanie fali "zgnić" to wszystko. Rozstawić kultystów dookoła strefy, zrobić z tego barierę.
    * ExMZ+3+5Om+3Og OmXOmO
    * Energia Esuriit wylała się z kultystów. Kultyści nie żyją. Manifestacje Esuriit na pokładzie. Diana wytrącona i wróciła do siebie, Eustachy też. Infernia ma koszmarne uszkodzenia. Całość energii przepływa do jaja. Burza magiczna. Iza próbuje to wciągnąć w siebie.
* 11 - Stabilizacja. Diana w szoku. Eustachy przejmuje i ratuje co się da
    * HrMZ (stymulant) +4 +3Vg (integracja + poświęcanie) VzOVg
    * Uda się utrzymać szkielet Inferni i załogi. Część energii przekierowała się na Tivr. Tu też się dzieją rzeczy. +2Vg.  Diana wyjdzie bez szkód psychicznych. Infernia wróci do formy szybciej. Eustachy przeżyje tylko dzięki Marii.
* 12 - Klaudia używa fragmentów Nereidy. Wszystko w kulkę. Wszystko przekierowane w Izę (która sobie to robi). Klaudia przekierowuje energię w Izę. Klaudia pełni rolę super-katalisty.
    * HrMZ (anomalie Klaudii) +4 + 3Vg (za działania Eustachego) + 3Vg (za działania Eleny) XmVXV
    * Iza + kilkanaście osób z kultu byli potrzebni do opanowania energii. Udało się containować Skażenie. Klaudia usunęła kody kontrolne Saitaera z Inferni. Elena jest zdjęta z akcji. Ogromna część załogi jest możliwa do uratowania. 
* 13 - Maria ratuje. 
    * ExZ (wsparcie medyczne + przejęła dowodzenie) M + 3Vg (Elena, Leona itp) +2 VXzVmX
    * Marii udało się pomóc sporej ilości osób. Ci, którzy jej pomagali ucierpieli najmocniej. Marii udało się pomóc 85% osób, których dało się uratować. Plus, zapewnia wysokie morale. Maria ma opinię "POTWORA".
* 14 - Arianna: Odzyskanie kontroli nad kultem, pozbycie się figurek.
    * ExZ (Iza i sytuacji) +4 VXVX
    * Kult skierowany przeciwko potworom i anomaliom.+2Vg. Mamy sekty. Sekta "Vigilusa=Nihilusa" i sekta Arianny. Kult dominujący to kult Arianny. Otton jest PRAWDZIWYM arcykapłanem Arianny +Vg. Kamil nie żyje. 
* 15 - Lutus dowodzi obroną Tivra przed anomaliami i magią.
    * TrZM+3 XXXO
    * Podstawowe linie obrony zostały złamane i Tivr też wymaga naprawy, ma niewielkie straty w ludziach. Giną nie-noktianie bo wiedzą co to Esuriit i się boją. Noktianie nie wiedzą co to Esuriit więc nie giną. Adam i Kasandra Destrukcja przekierowali energię używając dział Tivra i przekierowali całość energii do tamtej zdobytej koloidowej korwety. "Jajko", czyli Natalia pozbywa się tej energii. Natalia podpina się do Tivru, wchłania energię i to jest wystarczające by się obudziła. Po czym, niestabilna jak cholera, odlatuje. A Tivr stracił energię.
