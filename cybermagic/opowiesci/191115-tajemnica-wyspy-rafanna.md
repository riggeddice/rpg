## Metadane

* title: "Tajemnica Wyspy Rafanna"
* threads: unknown
* gm: kić
* players: voldy, vizzdoom, żółw

## Kontynuacja
### Kampanijna

* [191115 - Tajemnica wyspy Rafanna](191115-tajemnica-wyspy-rafanna)

### Chronologiczna

* [191115 - Tajemnica wyspy Rafanna](191115-tajemnica-wyspy-rafanna)

## Punkt zerowy

Jedna z frakcji Orbitera przeprowadza eksperyment badający wpływ długotrwałej ekspozycji na Eter Nieskończony. Wszystko idzie dobrze.

Aby zapewnić sobie silną wiarę, zadbali o dość prymitywne warunki życia, stworzyli kult Eteru i zapewnili, że zawsze mają w plemieniu swojego agenta - szamana. Pozwolili też, aby ściągać co jakiś czas losowe statki - w końcu "Eter dostarcza". Plemię, choć mocno wierzące, jest raczej mało agresywne. Tolerują obecność na wyspie "Heretyków" - rozbitków z łodzi, którzy chcą uciec z wyspy. Jedyny regularny akt agresji to niszczenie wszelkich łodzi, jakie Heretycy zbudują. Często taki akt jest rytuałem przejścia dla młodych mężczyzn. Nie przeszkadza to rozbitkom próbować budować kolejnych łodzi - nie wiedzą, że badacze Orbitera zawsze powiedzą szamanowi, gdzie jest kolejna łódź. Czasem któryś z Heretyków traci nadzieję na opuszczenie wyspy i jest z radością przyjmowany w szeregi Plemienia.

Ludzie na wyspie (magowie są wyłapywani, opłacani i odsyłani do domu żeby nie zakłócali eksperymentu) wykazują objawy wpływu Eteru - choć nie są magami, są w stanie wymyślać i skutecznie używać rytuałów.

### Postacie

* .

## Misja właściwa

### Scena zero

Młody chłopak przechodzi przez inicjację. Ma za zadanie zniszczyć statek heretyków. Nie chodzi o zabicie ludzi tylko o zniszczenie statku.

Poszedł do szamana i poprosił o rytuał przyzwania potwora. Dostał go. Zebrał kolegów i poszli tańczyć na klifie. Oczywiście, heretycy zauważyli rozpalone jako część rytuału ognisko i zanim udało się dokończyć rytuał ja klifie pojawiła się dziewczyna heretyków. Jeden z chłopców odciągnął ją od rytuału, aby koledzy mogli go dokończyć.

Udało im się, choć przypłacili rozproszenie się lekkimi obrażeniami. Statek został zniszczony przez pomniejszego krakena, przyzwanego rytuałem z Eteru.

### Sesja właściwa

Syrena, dobrze zadbany statek handlowy należący do pomniejszej pustogorskiej frakcji został złapany w potężną burzę magiczną na Eterze Nieskończonym. Andrij wziął sprawy w swoje ręce i pokierował statkiem by zapobiec jego zniszczeniu. Szczepan skupił się na zarządzaniu - niech mechanicy są w pogotowiu itp. Ale to dzięki Andrijowi Syrena wyszła z tego bez szwanku. I okazuje się, że jakaś siła próbuje aktywnie ich wciągnąć. Siła, której nie są w stanie pokonać.

Zrobili więc efekt procy. Popłynęli tam, z ogromną prędkością, wierząc w umiejętności Andrija oraz w stabilność Syreny. I udało się, acz napęd spłonął. Są uwięzieni na wyspie pośrodku Eteru. Znając sytuację, nie mają co liczyć na ratunek. Ale coś ich tu ściągnęło - trzeba się tym zająć.

Ekspedycja w głąb lądu. Przygotowany przez Szczepana rytualnie kompas łączący energię burzy z energią wyspy by znaleźć punkt (a dokładniej, by pomóc Andrijowi znaleźć punkt) i Przygotowana Ekspedycja przez Ezrę i ruszyli w głąb, przez dżunglę. Bez problemu dotarli do jaskini, gdzie miała miejsce manifestacja tej energii. Weszli tam i Andrij skupił się na tym, by odnaleźć co się naprawdę stało, jak sytuacja wyglądała. Niestety, wszystko poszło horrendalnie nie tak:

* udało im się dojść do tego, że rytuał był powiązany z głodem i nadzieją i nie miał ich zabić
* udało się dojść, że najpewniej nie był to rytuał robiony przez magów. Przez ludzi? Ciekawe.
* niestety, woda zaczęła się podnosić; gdyby nie Ezra, Andrij by utonął.
* Druga Strona zorientowała się, że tu ktoś jest.

Tak więc nasza Ekspedycja wyszła sobie kaszląc, marznąc, topiąc się - a na lądzie ludzie z włóczniami i szaman (starring: Karol Rotmistrz) krzyczący "zbezcześciliście naszą świętą jaskinię". Tu na przód wysunął się Szczepan mając nadzieję kupić czas by Ezra mógł ich wszystkich pobić jak trzeba. Prawie się udało:

* Szczepan wyczuł, że szaman jest magiem. Na pewno reagował na słowa "Orbiter" i "Aurum"
* fanatycy się mega oburzyli na Szczepana i Ekspedycję - stąd się nie da odjechać ani odpłynąć, nie ma świata poza tą wyspą
* opowieść o exploitach drużyny na skorupach żółwi morskich (bo nie ma świata poza tym) zbudowały szacun - jeśli Ezra pokona w pojedynku ich woja
* Szczepan wygadał się, że jest magiem.

No dobrze, Ezra wychodzi z ich wybranym wojem, zażywa narkotyki bojowe, zaczął walkę. Mimo, że nie miało być na śmierć - Ezra nie powstrzymał się i po wielu ciosach które dostał zarezonował z Eterem i de facto zmienił swojego oponenta w krwawą miazgę. Teraz się ich BOJĄ oraz NIENAWIDZĄ ale też CZUJĄ SZACUNEK. Zapewniwszy sobie spokój od miejscowych, Zespół wrócił na pokład Syreny.

Tam czekali na nich "Heretycy". To są ludzie, którzy chcieli opuścić wyspę, ale nie są w stanie - miejscowi regularnie niszczą ich statki, acz ich samych nie zabijają. Raz na jakiś czas ktoś z Heretyków ma dość i traci nadzieję, przechodzi do plemienia - jest akceptowany. Plemię wierzy - próbuje uwierzyć - że nie ma świata poza wyspą oraz, że Eter Dostarcza Wszystkiego Co Potrzebne. Heretycy nie chcą odrzucić prawdy i chcą odejść.

Tu Ezra uznał, że tak nie może być, oni tu nie zostaną i jak trzeba, to wszystkim się wpierdoli. Ale należy opuścić tą wyspę. By to osiągnąć, Szczepan wprowadził heretyków w 'faith frenzy' - teraz mu wierzą, że to jest możliwe. Da się opuścić. Uda się. W ten sposób mają potężną broń do walki z Szamanem jakby było potrzebne, pryzmatycznie nie są całkiem do kitu. Ezra zgromadził ekspedycję, dodał tam heretyków kilku i poszli do Centrum Wyspy. Bo z Centrum się nie wraca. Ale, jak to Ezra zauważył, mają maga (Szczepana - tylko on jest jawny). Plus, mają siłę ognia i sprawną Syrenę. Niemobilną, ale z DZIAŁAMI.

W nocy Szczepan musiał się wysikać. Gdy poszedł robić swoje, gdy skończył, zastał go Szaman. Powiedział, że oni wchodzą w poważny ekperyment Orbitera i jest mega dużo kłopotów z tym. Oni wyekspediują Szczepana z powrotem do domu, ale wszyscy inni muszą zostać (bo tylko Szczepan jest magiem). Szczepan chciał uratować sytuację i nic nie zdradzić. Zablefował - nie, to SZAMAN wchodzi w bardzo ważne śledztwo kombinowanych sił Aurum oraz Orbitera wobec korupcji pewnego szlacheckiego rodu, i dlatego oni mają biorobota (chyba nie myślą, że Ezra to człowiek...) i w ogóle. Szaman zdębiał. Co z tym zrobić. Nikt nie może opuścić Wyspy, bo wszystko pójdzie w cholerę - a tu jest kwestia skomplikowanego śledztwa.

Zaczynają się długie i skomplikowane dyskusje polityczne, z różnymi frakcjami, kto za co płaci (a Szczepan bez grosza przy duszy i nie wie jak się wyplątać, będzie lewarował odszkodowanie by jakoś coś...) gdy przyszedł Ezra i wpierdolił szamanowi z zaskoczenia. Dyplomacja poszła w cholerę.

Przesłuchany pod wpływem drugów szaman powiedział że baza jest silnie chroniona, i w ogóle. Ezra kombinował jak tam się wbić, ale Szczepan miał inny pomysł. Opracowali plan z Andrijem - jeśli mają kontrolę nad plemieniem (bo Szaman; łatwo się przebrać) i nad Heretykami (bo wierzą w Zespół), to da się obrócić Eter Nieskończony przeciwko placówce Orbitera. W ten sposób wszystkie ślady znikną i oni mogą przejąć kontrolę nad wyspą.

Ezra jest za. Ci ludzie ucierpieli przez Orbiter. Tak nie może być. Andrij jest za - władza i w sumie możliwości. Szczepan - chce ratować ludzi i nie być jak jego siostra (Sabina). Dobra, próbują to zrobić.

Poszło KATASTROFALNIE źle (przegrany heroik):

* wielu ludzi z Heretyków i Plemiennych zginęło. Ich krew wzmocniła rytuał i Skaziła Eter.
* Skażenie i "dupnięcie" stworzyło tu _Dark Beacon_ - statki będą czasem się tu pojawiać - chcą lub nie chcą. Plus, może zobaczyć to jakiś morski potwór
* Erupcja Eteru zniszczyła bazę Orbitera w formie lawy. Nikt, kto tam był nie miał szans przeżyć a portal się rozerwał. Wszyscy zginęli (Skażenie++)
* z uwagi na zagrożenia, ściągnięty został tu mały patrol - oni przejęli kontrolę nad wyspą (a nie nasz Zespół)
* Ezra, Szczepan i Andrij dostali reputację Jonaszy. Idzie za nimi nieszczęście
* ALE - ludzie przetrwali, załoga Syreny też oraz eksperyment Orbitera się zakończył - i nikt nie ma pojęcia co zrobił Zespół.

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Orbiter przeprowadzał eksperyment nad wpływ długotrwałej ekspozycji na Eter na ludzi. Przez przypadek ściągnęli na wyspę statek handlowy z trzema magami. Jakkolwiek Orbiter chciał dobrze - odesłać magów i kontynuować eksperyment, ale magowie chcieli przejąć władzę oraz nie być niemoralni - w związku z tym przeprowadzili rytuał, spieprzyli go i wszyscy w bazie Orbitera zginęli. Na szczęście, magowie zatarli ślady, ale są uwięzieni na wyspie...

## Progresja

* .

### Frakcji

* .

## Zasługi

* Ezra Czarnoręki: wachtmistrz na Syrenie; doprowadził do tego, że zamiast opuścić Wyspę Rafanna Zespół próbował przejąć nad nią kontrolę. Bezwzględny ale lojalny.
* Andrij Zarębski: eteronawigator na Syrenie; główny 'detektor' i genialny nawigator, utrzymał Syrenę wbrew straszliwej burzy. Katalista, który przeprowadził apokaliptyczny rytuał.
* Szczepan Kazitan: bard na Syrenie; tak bardzo bullshitował bo chciał udowodnić, że nie jest tak zły jak jego siostra, że doprowadził do śmierci wielu magów i ludzi Orbitera.
* Karol Rotmistrz: szaman i mag Orbitera; uczestniczył w Eksperymencie i jako jedyny z Orbitera przeżył. Mimo, że stał się przyczyną zniszczenia tej bazy nie wie, że to jego sojusznicy zrobili.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Eter Nieskończony
            1. Mare Quixos
                1. Wyspa Rafanna
                    1. Pierwsza Osada: najlepiej rozbudowana wioska w otoczeniu z ruin statków; tam dowodzi Szaman. A raczej, dowodził.
                    1. Dżungla Zmienna: wielka, ciągle zmieniająca się dżungla która nie jest niebezpieczna dla dużej uzbrojonej grupy osób - zwykle.
                    1. Centrum Wyspy: tu był eksperyment Orbitera. Teraz pozostało tylko wiecznie żywe jezioro lawy w wyniku nie dogasającego rytuału.

## Czas

* Chronologia: Aktualna chronologia
* Opóźnienie: 44
* Dni: 5
