## Metadane

* title: "Wieczna Wojna Bladawira"
* threads: triumfalny-powrot-arianny, program-kosmiczny-aurum, naprawa-swiata-przez-bladawira
* motives: slipping-through-my-fingers, pakt-z-diablem, sinful-hedonism, mroczna-konspiracja, broken-mind
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [240107 - Narkotyczna pacyfikacja tienów na Castigatorze](240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)

### Chronologiczna

* [240107 - Narkotyczna pacyfikacja tienów na Castigatorze](240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA: 
    * brak
* Opowieść o (Theme and vision)
    * Zagubiony Bladawir szuka w różnych miejscach - czasem dobrze, czasem źle. Nie zawsze wie co się dzieje, rozbity całkowicie.
    * Jest jakiś dziwny Kult skupiający się na wykorzystywaniu swojej pozycji na Orbiterze, korzystając ze słabości kultury Atarienów.
    * Bladawir - jako potwór - jest idealny do eksterminacji tego typu potworów. Są rzeczy których nie robi i nie rozumie i gardzi.
    * SUKCES
        * Arianna wpakuje Władawca na Infernię (oraz Zaarę) i zdejmie oczy Bladawira z Inferni i z siebie.
        * Bladawir będzie na tyle zajęty że da spokój Inferni - znajdzie swoją Mroczną Konspirację.
        * Uda się przyskrzynić to co stoi za Talią Miraris bez większego ryzyka dla Arianny i Klaudii.
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * slipping-through-my-fingers: wpierw sprawa z przemytem tienów, teraz sprawa ze zniszczeniem okrętu z kralothem. Bladawir rozpaczliwie szuka CZEGOKOLWIEK, śladów, dowodów, że miał rację.
    * pakt-z-diablem: Arianna ściąga na pokład Inferni Władawca, by rozpoczął pracę nad Zaarą. Zaara musi być zrobiona miła i przyjemna by móc wbić się do planów Bladawira.
    * sinful-hedonism: kapitan Dormand Miraris, wykorzystujący SC Karonotris jako bezpieczne miejsce zabawy z dziewczętami używając potwora.
    * mroczna-konspiracja: Bladawir szuka konspiracji Syndykatu Aureliona na Orbiterze, ale kto miałby za nią stać i dlaczego? Szuka wykorzystywania 'mniej ważnych' ludzi Orbitera, kraloth...
    * broken-mind: Zaara. Okazuje się, że Aurelion ją zrekonstruował. Ona nie jest w dobrej formie i nigdy nie będzie - Bladawir ją 'adoptował'.
* Detale
    * Crisis source: 
        * HEART: "Bladawir jest całkowicie zagubiony przez to co się dzieje - błędy częściowo są Konspiracją a częściowo Arianną" oraz "There are forces in Orbiter which want to exploit"
        * VISIBLE: "Bladawir skupia się na statystycznej analizie anomalii Orbitera, jak Wydział Wewnętrzny, znalazł Talię Miraris. Właśnie - czy ma wsparcie Wydziału?"
    * Delta future
        * DARK FUTURE:
            * chain 1: BLAME: zrzucone na Bladawira, że on w sumie nie wie o co chodzi i nic sensownego nie robi i nie określił (zwłaszcza kraloth)
            * chain 2: WARNING: nie tylko nic się nie uda zrobić by zablokować działania Mirarisa, ale do tego jeszcze Miraris jest ostrzeżony i Kult się rozprzestrzenia
            * chain 3: THE EYE: Bladawir monitoruje ruchy Inferni dokładniej niż by ktokolwiek chciał
        * LIGHT FUTURE:
            * chain 1: BLAME: Bladawir udowodnił, że tam się coś złego stało, ma możliwość działań mimo problemów. Więcej - są próby ZABICIA Bladawira jako ryzyko.
            * chain 2: WARNING: Miraris pojmany żywcem / zabity. Łącznie z jego istotą.
            * chain 3: THE EYE: Bladawir jest tak zajęty walką z działaniami na Orbiterze, że nie ma czasu na Infernię
    * Dilemma: Bladawir - na pewno potwór, ale czy nie wsparcie?

### Co się stało i co wiemy

* Okazuje się, że 'SC Wirmerin' był jednostką przejętą przez kralotha - ta jednostka została zniszczona przez kapitana Ernesta Bankierza.
    * Bladawir nie wierzy. To niemożliwe. Tam nie mógł być kraloth. On nie mógł być zniszczony.
    * -> wysyła Infernię
        * szukaj o Erneście Bankierzu!!!
        * szukaj śladów!!! Dark Awaken!!! AWAKEN THAT KRALOTH!!!
    * -> gdzie jest Natalia Gwozdnik...
* Bladawir idzie z innej strony. Od strony kapitana Ernesta Bankierza
    * Ernest ma swoje cechy
        * jest strasznym kobieciarzem, też wobec swoich podwładnych (no-no dla Bladawira)
        * jego XO jest bardzo kompetentna; nazywa się Talia Miraris; to ona sukcesywnie zapewnia baterię miłych dam dla Ernesta (Bladawir: coś jej zrobił!)
            * (na serio to kpt. Dormand Miraris, brat Talii zrobił to własnej siostrze)
                * Talia ma szpiegować Bankierza i sprawiać, by jego życie było cudowne (Bladawir: DLACZEGO?)
    * Dormand Miraris jest jedną z osób odpowiadającą za zapewnianie narkotyków imprezowych; handluje z Aurum, rozprowadza przez Karsztarin i stacje przeładunkowe
        * jego jednostką jest fregata 'Piscatrix', dobrze uzbrojona i szybka jednostka, bardzo dobrze doinwestowana
        * może się bezpiecznie schować w Aurum, ma tam przyjaciół i sojuszników
* Cel Arianny - pozyskanie Władawca przez Ariannę jeśli to możliwe ;-)
    * Pozyskanie Zaary na pokład Inferni
    * Pozyskanie Władawca przez Infernię
    * Zdjęcie oczu Bladawira z Inferni
* Kontekst sesji 
    * Tym razem nie "Alien from the Darkness", a "Omega Supreme / Constructicons"
    * Potwór znajduje się w odpowiednim statku kosmicznym, niezauważony

### Co się stanie (what will happen)

* F1: Wiadomość o zniszczeniu kralotycznego statku przez kpt Ernesta Bankierza
    * Bladawir nie wierzy. Bladawir konsultuje się ze swoimi kapitanami.
    * Infernia -> ruina jednostki
        * szukaj o Erneście Bankierzu!!!
        * szukaj śladów!!! Dark Awaken!!! AWAKEN THAT KRALOTH!!!
        * gdzie jest Natalia Gwozdnik jeśli tu jest kraloth?
    * Infernia -> info o Erneście Bankierzu
        * wskaźnik na Talię Miraris (pozyskuje dziewczyny dla kapitana Bankierza)
* F2: Życie personalne kapitana Bankierza
    * Ernest ma swoje cechy
        * jest strasznym kobieciarzem, też wobec swoich podwładnych (no-no dla Bladawira)
        * jego XO jest bardzo kompetentna; nazywa się Talia Miraris; to ona sukcesywnie zapewnia baterię miłych dam dla Ernesta (Bladawir: coś jej zrobił!)
            * (na serio to kpt. Dormand Miraris, brat Talii zrobił to własnej siostrze)
                * Talia ma szpiegować Bankierza i sprawiać, by jego życie było cudowne (Bladawir: DLACZEGO?)
    * Dormand Miraris jest jedną z osób odpowiadającą za zapewnianie narkotyków imprezowych; handluje z Aurum, rozprowadza przez Karsztarin i stacje przeładunkowe
        * jego jednostką jest fregata 'Piscatrix', dobrze uzbrojona i szybka jednostka, bardzo dobrze doinwestowana
        * może się bezpiecznie schować w Aurum, ma tam przyjaciół i sojuszników
* CHAINS
    * Problemy i konflikty
        * reputacja: (Arianna na celowniku Bladawira, Arianna na celowniku Orbitera (zwłoki srs?))
        * magia: (Reanimacja, badania)
        * (atak potworów, 'dead is alive', silniejszy stres)
        * (contamination)
    * Magia
        * (Klaudia / Martyn Skażeni, nie wszystkich da się uratować, konieczność operacji ratunkowych po Martyna/Klaudię)
        * (Transfer energii do Vishaera)
* Overall
    * stakes
        * Arianna: relacja z Bladawirem, relacja z Bankierzem
        * Arianna: ściągnięcie Władawca i zatrzymanie Zaary
        * Bladawir: czy patrzy na Ariannę dokładnie? 
        * Bladawir: czy znajdzie prawdziwy spisek?
        * Kult: czy znajdzie Mroczny Kult? czy dojdzie do Talii Miraris? Do Dormanda Mirarisa?
        * Potwór: czy dorwie Zespół? czy ucieknie?
    * opponent
        * Bladawir i jego niemówienie, jego paranoja, jego poszukiwanie mroku gdzie go nie ma, jego niewiara we wszystko
        * Potwór (interis/alucis), usuwający Znaczenia i sprawiający, że ofiara jest zniewolona
        * Kapitan Dormand Miraris.
    * problem
        * czy zaufać Bladawirowi? Jak daleko pójść za jego planami?
        * jak dobić się do informacji o Mirarisie?

## Sesja - analiza

### Fiszki

Zespół:

* Leona Astrienko: psychotyczna, lojalna, super-happy and super-dangerous, likes provocative clothes
* Martyn Hiwasser: silent, stable and loyal, smiling and a force of optimism
* Raoul Lavanis: ENCAO: -++0+ | Cichy i pomocny;; niewidoczny| Conformity Humility Benevolence > Pwr Face Achi | DRIVE: zrozumieć
* Alicja Szadawir: młoda neuromarine z Inferni, podkochuje się w Eustachym, ma neurosprzężenie i komputer w głowie, (nie cirrus). Strasznie nieśmiała, ale groźna. Ekspert od materiałów wybuchowych. Niepozorna, bardzo umięśniona.
    * OCEAN: N+C+ | ma problemy ze startowaniem rozmowy;; nieśmiała jak cholera;; straszny kujon | VALS: Humility, Conformity | DRIVE: Uwolnić niewolników
* Kamil Lyraczek
    * OCEAN: (E+O-): Charyzmatyczny lider, który zawsze ma wytyczony kierunek i przekonuje do niego innych. "To jest nasza droga, musimy iść naprzód!"
    * VALS: (Face, Universalism, Tradition): Przekonuje wszystkich do swojego widzenia świata - optymizm, dobro i porządek. "Dla dobra wszystkich, dla lepszej przyszłości!"
    * Styl: Inspirujący lider z ogromnym sercem, który działa z cienia, zawsze gotów poświęcić się dla innych. "Nie dla chwały, ale dla przyszłości. Dla dobra wszystkich."
* Horst Kluskow: chorąży, szef ochrony / komandosów na Inferni, kompetentny i stary wiarus Orbitera, wręcz paranoiczny
    * OCEAN: (E-O+C+): Cichy, nieufny, kładzie nacisk na dyscyplinę i odpowiedzialność. "Zasady są po to, by je przestrzegać. Bezpieczeństwo przede wszystkim!"
    * VALS: (Security, Conformity): Prawdziwa siła tkwi w jedności i wspólnym działaniu ku innowacji. "Porządek, jedność i pomysłowość to klucz do przetrwania."
    * Core Wound - Lie: "Opętany szałem zniszczyłem oddział" - "Kontrola, żadnych używek i jedność. A jak trzeba - nóż."
    * Styl: Opanowany, zawsze gotowy do działania, bezlitośnie uczciwy. "Jeśli nie możesz się dostosować, nie nadajesz się do tej załogi."
* Lars Kidironus: asystent oficera naukowego, wierzy w Kidirona i jego wielkość i jest lojalny Eustachemu. Wszędzie widzi spiski. "Czemu amnestyki, hm?"
    * OCEAN: (N+C+): bardzo metodyczny i precyzyjny, wszystko kataloguje, mistrzowski archiwista. "Anomalia w danych jest znakiem potencjalnego zagrożenia"
    * VALS: (Stimulation, Conformity): dopasuje się do grupy i będzie udawał, że wszystko jest idealnie i w porządku. Ale _patrzy_. "Czemu mnie o to pytasz? Co chcesz usłyszeć?"
    * Core Wound - Lie: "wiedziałem i mnie wymazali, nie wiem CO wiedziałem" - "wszystko zapiszę, zobaczę, będę daleko i dojdę do PRAWDY"
    * Styl: coś między Starscreamem i Cyclonusem. Ostrożny, zawsze na baczności, analizujący każdy ruch i słowo innych. "Dlaczego pytasz? Co chcesz ukryć?"
* Dorota Radraszew: oficer zaopatrzenia Inferni, bardzo przedsiębiorcza i zawsze ma pomysł jak ominąć problem nie konfrontując się z nim bezpośrednio. Cichy żarliwy optymizm. ŚWIETNA W PRZEMYCIE.
    * OCEAN: (E+O+): Mimo precyzji się gubi w zmieniającym się świecie, ale ma wewnętrzny optymizm. "Dorota może nie wiedzieć, co się dzieje, ale zawsze wierzy, że jutro będzie lepsze."
    * VALS: (Universalism, Humility): Często medytuje, szuka spokoju wewnętrznego i go znajduje. Wyznaje Lichsewrona (UCIECZKA + POSZUKIWANIE). "Wszyscy znajdziemy sposób, by uniknąć przeznaczenia. Dopasujemy się."
    * Core Wound - Lie: "Jej arkologia stanęła naprzeciw Anomaliom i została Zniekształcona. Ona uciekła przed własną Skażoną rodziną" - "Nie da się wygrać z Anomaliami, ale można im uciec, jak On mi pomógł."
    * Styl: Spokojna, w eleganckim mundurze, promieniuje optymizmem 'że z każdej sytuacji da się wyjść'. Unika konfrontacji. Uśmiech nie znika z jej twarzy mimo, że rzeczy nie działają jak powinny. Magów uważa za 'gorszych'.
    * metakultura: faerilka; niezwykle pracowita, bez kwestionowania wykonuje polecenia, "zawsze jest możliwość zwycięstwa nawet jak się pojawi w ostatniej chwili"

Castigator:

* Leszek Kurzmin: OCEAN: C+A+O+ | Żyje według własnych przekonań;; Lojalny i proaktywny| VALS: Benevolence, Humility >> Face| DRIVE: Właściwe miejsce w rzeczywistości.
* Patryk Samszar: OCEAN: A+N- | Nie boi się żadnego ryzyka. Nie martwi się opinią innych. Lubi towarzystwo i nie znosi samotności | VALS: Stimulation, Self-Direction | DRIVE: Znaleźć prawdę za legendami.
    * inżynier i ekspert od mechanizmów, działa świetnie w warunkach stresowych

Siły Bladawira:

* Antoni Bladawir: OCEAN: A-O+ | Brutalnie szczery i pogardliwy; Chce mieć rację | VALS: Power, Family | DRIVE: Wyczyścić kosmos ze słabości i leszczy.
    * "nawet jego zwycięstwa mają gorzki posmak dla tych, którzy z nim służą". Wykorzystuje każdą okazję, by wykazać swoją wyższość.
    * Doskonały taktyk, niedościgniony na polu bitwy. Jednocześnie podły tyran dla swoich ludzi.
    * Uważa tylko Orbiterowców i próżniowców za prawidłowe byty w kosmosie. Nie jest fanem 'ziemniaków w kosmosie' (planetarnych).
* Kazimierz Darbik
    * OCEAN: (E- N+) "Cisza przed burzą jest najgorsza." | VALS: (Power, Achievement) "Tylko zwycięstwo liczy się." | DRIVE: "Odzyskać to, co straciłem."
    * kapitan sił Orbitera pod dowództwem Antoniego Bladawira
* Michał Waritniez
    * OCEAN: (C+ N+) "Tylko dyscyplina i porządek utrzymują nas przy życiu." | VALS: (Conformity, Security) "Przetrwanie jest najważniejsze." | DRIVE: "Chronić moich ludzi przed wszystkim, nawet przed naszym dowódcą."
    * kapitan sił Orbitera pod dowództwem Antoniego Bladawira
* Zaara Mieralit
    * OCEAN: (E- O+) "Zniszczenie to sztuka." | VALS: (Power, Family) "Najlepsza, wir zniszczenia w rękach mojego komodora." | DRIVE: "Spalić świat stojący na drodze Bladawira"
    * corrupted ex-noctian, ex-Aurelion 'princess', oddana Bladawirowi fanatyczka. Jego prywatna infomantka. Zakochana w nim na ślepo.
    * afiliacja magiczna: Alucis, Astinian

Siły inne:

* Władawiec Diakon: tien, corruptor of ladies, duelist, second-in-command na statku kosmicznym (XO)
    * ENCAO: (E+C+) | intrygancki;; kontrolowany wulkan;; pająk w sieci | VALS: Hedonism, Power | DRIVE: Perfekcja Zaufania
    * styl: nieskazitelny oficer, delikatny uśmiech, niezmiennie pewny. Gabriel Durindal (Gundam Seed Destiny)
    * piosenka wiodąca: Epica "Obsessive Devotion"

### Scena Zero - impl

.

### Sesja Właściwa - impl

PLAN:

Arianna ma dwie rzeczy do zrobienia: przemycić Władawca i obniżyć severity misji. By przemycić Władawca, trzeba zrobić rotację w składzie Inferni. Pewną roszadę. Nowy komodor, nowa roszada, zostają osoby które chcą tak działać. Komodor powiedział, że mam kontrolować ludzi więc stosuję się do jego polecenia.

Druga rzecz - jak to zrobić, by Bladawir nie był skupiony? Niech ma zadanie które mu się spodoba i nie będzie nieakceptowalne. Pociągnąć nasze kontakty, by przed Bladawirem pojawiło się opportunity które mu się podoba. Coś podsunąć co stwierdzi że to jego pomysł. Coś korzystnego. Dla niego lub Orbitera. Musimy zrobić coś co mu pokaże że go słuchamy...

DZIAŁANIE:

Arianna siedziała, robiła plany jakieś 3-4, analizowała jak to ruszyć. Po drodze Klaudia i Martyn byli na Castigatorze, Klaudia wróciła. Sama. Tam coś się stało i Martyn jest potrzebny, przynajmniej tymczasowo. Klaudia się nie chwaliła. Ale nie było żadnych protestów z Castigatora, wszyscy udają że jest dobrze. Co ciekawe - Kurzmin chyba nie zauważył że Klaudia i Martyn tam byli, a przynajmniej nic nie mówił. No nic, zdarza się.

Arianna chce przekonać Bladawira do tych zmian. Jej argumenty są jak następuje:

* Chce dalej z nim współpracować i chce się dostosować do jego podejścia do jego członków floty
* Chce przeorganizować Infernię by działać
* Chce wyeliminować osoby które mogą przeszkadzać
* Chce posprzątać śmieci na swoim podwórku.
* Jeśli ktoś konspiruje przeciw Bladawirowi i Ariannie, przemieszanie sprawia że traci grunt który ułożył

Bladawir:

* Bladawir: Klaudia Stryk zostaje? Jest przydatnym narzędziem.
* Arianna: Zastanawiałam się nad nią; jest przydatniejsza gdy ją mam niż gdzieś indziej
* Bladawir: Dobrze. Tak zrobimy. Ona ma pewną wartość. Wiem jak jej użyć. 
* Bladawir: (dodatkowa mikrogroźba: "pamiętaj że to Twoja ostatnia szansa", "jeśli ktoś z Twojej załogi konspiruje przeciw mnie, powiedz, nie Twoja wina.")

Ex Z +4:

* X: Bladawir przejrzy wszystkie dossier i udzieli rekomendacji
* X: Nie wszystkie osoby które Arianna by chciała dostanie i dostanie osoby jakich by nie chciała; wsadzi kogoś
* Xz: Rotacja będzie dopiero po tej sesji i Arianna musi sprzedać mu Władawca.

Arianna - "Władawiec ma bardzo szerokie kontakty, można znaleźć kogoś kogo Bladawir szuka, bez dostępu normalnego". Władawiec może zauroczyć, dopytać, dowiedzieć się itp. Wyciągnąć itp. Nowe świetne narzędzie w ręku Bladawira.

* Vr: Bladawir udostępni Ariannie Władawca. Acz po jego uśmieszku Arianna widzi, że podejrzewa, że Arianna chce go dla siebie. (Arianna się zaczerwieniła)
    * Bladawir: "Dobrze, jeśli to jest potrzebne by Infernia działała jak powinna, nie dbam o to co tam się dzieje. Tylko uważaj. Infernia jest MOJA, nie JEGO."
    * Arianna: "Oczywiście, komodorze."

Arianna spokojnie wróciła na Infernię. I na K1 - tam powinien być Władawiec. Jest pomiędzy przydziałami. Klaudia <3.

Władawiec wygląda... bardziej epicko niż kiedyś. Rekruter roku. Koło niego 4 zawstydzone dziewczątka. Uśmiechnął się na minę Arianny. Podziękował dziewczynom (i zapłacił), zniknęły, jedna zrobiła mu ukradkiem zdjęcie, udał że nie zauważył. Władawiec zaprosił Ariannę do jednego 'boxów ciszy'.

* Władawiec: Kapitan Verlen, jeszcze piękniejsza niż ostatnio gdy Cię widziałem.
* Arianna: Porucznik Diakon, jeszcze lepiej wyglądasz niż jak się spotkaliśmy.
* Arianna: Jesteś pomiędzy przydziałami. Mam do Ciebie dużą prośbę.
* Władawiec: Prośbę, między... (zawiesił głos), słucham :-).
* Arianna: Mam potwora do upolowania i potrzebuję pomocy. 
* Władawiec: Zwykle jestem proszony w innych aktywnościach niż polowanie na potwory.
* Arianna: Wszystko zależy z jakimi potworami mamy do czynienia.
* Władawiec: Jak na razie żadna z moich przyjaciółek nie narzeka na potwory (mrugnął).
* Arianna: Ja narzekam - nie możesz mówić że żadna. Wracając. 
* Arianna: Potrzebuję się dobrać do skóry jednego komodora we flocie
* Władawiec: Komodor ma tylko jeden mundur, zdjąć go nie jest tak trudno. Skóra... jest łatwo dostępna.
* Arianna: Zdziwiłbyś się.
* Władawiec: Próbowałaś tej metody? Mówię - dobrze wyglądasz. Mogę dać Ci parę trików lub poćwiczyć z Tobą, jeśli nie jesteś pewna. (bez złośliwości)
* Arianna: Nie jestem pewna że jestem w stanie tak do niego podejść. Jest jedna dziewczyna której PRAWIE ufa i nawet ona nie zdjęła z niego munduru.
* Władawiec: (spoważniał na sekundę) Jesteś PEWNA że nie dała rady?
* Arianna: Jestem pewna
* Władawiec: (znowu uśmiech) kapitan Verlen, KAŻDY, absolutnie KAŻDY z kimś lub czymś spać musi. Nie ma innej opcji.
* Arianna: Z kim stał komodor Bolza to z wężami
* Władawiec: (szeroki uśmiech) Prawda, ale powiem Ci, że komodor miał bardziej... interesujące życie uczuciowe niż byś pomyślała.
* Arianna: Nie wątpię, była populacja której się podobał
* Władawiec: Czyli szukasz u mnie konsultacji jak uwieść komodora, tak?
* Arianna: Potrzebuję pomocy w uwiedzeniu jego współporacowniczki
* Władawiec: Czyli... Ty chcesz uwieść jego współpracowniczkę.
* Arianna: Nie mam takich kompetencji
* Władawiec: Wszystko da się wyćwiczyć. Tym się nie przejmuj. Jeśli Ci się podoba, to wiesz, da się załatwić. Jak nie, a jest Ci to potrzebne, to wyobrażaj sobie inną damę. Nauczę Cię.
* Arianna: Pamiętasz Alezję?
* Władawiec: Oczywiście
* Arianna: Pamiętasz jak na Ciebie patrzyła w szczycie Twoich podbojów?
* Władawiec: W szczycie mojej władzy.
* Arianna: Dziewczyna która tak patrzy na niego jak Alezja na Ciebie. I nie weszła do łóżka.
* Władawiec: O. I tą dziewczynę chcesz uwieść? 
* Arianna: Tak.
* Władawiec: (myśli chwilę) Masz jakiekolwiek doświadczenie?
* Arianna: Myślisz że miałam czas?
* Władawiec: Arianno, zawsze się znajdzie czas, masz potencjalnie dużo zainteresowanych osób. Ja bym też nie pogardził.
* Arianna: Pamiętasz Leonę?
* Władawiec: LEONA jest Twoją przyzwoitką?
* Arianna: Leona zajmuje 90% mojej uwagi w czasie wolnym. Ale mam jeszcze pierwszego oficera.
* Władawiec: I nikt z nich nie odpowiada na Twoje "subtelne" sygnały że chcesz iść z nimi do łóżka?
* Arianna: Nieistotne. W sensie... nie próbowałam. Nie o to chodzi.
* Władawiec: (z mądrą miną) Nie możesz wspiąć się na niedostępną górę póki na mały prosty pagórek.
* Arianna: Mogę zaproponować Eustachemu nawet na Twoich oczach. Nie w tą stronę. Chciałabym byś dołączył do Inferni i zajął się tym osobiście.
* Władawiec: Żebym ja pomógł Tobie uwieść ową damę.
* Arianna: Żebyś Ty dla mnie uwiódł ową damę.
* Władawiec: (minka niewinna) Arianno, przecież wyraźnie powiedziałaś mi za czasów Alezji a potem Eleny... że nie powinienem, że robię dziewczynom krzywdę. I teraz chcesz, bym skrzywdził dziewczynę dla Ciebie?
* Arianna: Jakbyś przestał praktykować bo Ci powiedziałam
* Władawiec: Nie przestałem. Ale teraz to Twoja prośba, prawda?
* Arianna: Tak
* Władawiec: Zgoda. Zależy Ci na tym. I podejrzewam, że to będzie dobra zabawa. Ale - nie wszystkie ruchy które zrobię będą dla Ciebie zrozumiałe. Rozumiesz to.
* Arianna: Podejrzewam.
* Władawiec: I mimo wszystko chcesz mnie na pokładzie Inferni.
* Arianna: Tak.
* Władawiec: Ale będę potrzebował zapłaty, wiesz o tym. (sympatyczny uśmiech)
* Arianna: Czego konkretnie chcesz?
* Władawiec: Pójdź ze mną na kawę i ciastko. Na K1. Teraz. To nie randka. To... spotkanie starych przyjaciół. (szeroki uśmiech)
* Arianna: Jest to chyba cena którą mogę zapłacić.
* Władawiec: Cieszę się, pani kapitan /ukłon

(to może być przypadek gdy lekarstwo jest gorsze od choroby). Klaudia zajmie się przesunięciem Władawca na Infernię. (od przyszłej sesji)

Arianna i Władawiec są na pierwszej randce Arianny na Orbiterze. Buduje miłe wspomnienia na K1 z Arianną. I - Arianna dostaje wiadomość od Bladawira. Oczywiście. "Wszyscy kapitanowie proszeni na Adilidam NATYCHMIAST. Critical." Arianna przeprasza Władawca i musi się zbierać. Władawiec się uśmiechnął "spotkamy się niedługo". Klaudia w tym czasie skupia się ludźmi, Klaudia zarządza, robi to co Bladawir robi dla siebie. Ludzi którzy wchodzą na pokład. Bo - Bladawir w pobliżu to paranoja++. Klaudia ma też wiedzę o Władawcu od Arianny - widzi co to za ziółko. Widzi, że Arianna nie ma szans. MENTAL NOTE TO SELF: ściągnąć Martyna jak się zacznie gorzej. SECOND NOTE: ustawić na TAI trigger.

Klaudia zapuszcza search - dyskretny - szuka jakiegoś rodzaju problemów które chciałaby by odwracały uwagę Bladawira. Takie od których Klaudia się nie porzyga że Bladawir na tym się skupia i Infernia idzie w tą stronę. Zasadniczo - podwójna paranoja - jest przekonana, że Bladawir będzie chciał żeby Zaara spróbowała monitorować Klaudię w jakiś sposób, więc Klaudia sobie zrobi wyszukiwanie monitoringu Zaary by móc mu coś podkładać. Czyli - Klaudia przygotowuje się na wojnę zewnętrzną i wewnętrzną.

Arianna dociera na Adilidam. Tam są wszyscy dwaj kapitanowie Bladawira, Zaara, Bladawir oraz dwóch strażników Bladawira (cirrusy?). Bladawir wygląda na bardziej chorego niż kiedykolwiek. Blada cera, zimny pot.

* Bladawir: Wczoraj... wczoraj statek o nazwie Wirmerin został zniszczony. Ten statek miał kralotha na pokładzie. Zniszczył go kapitan Ernest Bankierz z sił Orbitera.
* Darbik: Czyli koniec operacji, panie komodorze?
* Bladawir: Gdzie jest Natalia Gwozdnik? 
* Darbik: Nie wiem, panie komodorze.
* Bladawir: Ernest Bankierz zniszczyłby statek na którym jest kraloth? Przypadkiem go znalazł i zniszczył? I to jeszcze śmieć z Aurum, który został kapitanem Orbitera?
* Bladawir: Najprawdopodobniej to jakaś zmyłka. To nie był prawdziwy kraloth. Albo to był fragment kralotha. Musimy to sprawdzić, kraloth NIE POWINIEN być zniszczony tam i wtedy. To nie ma sensu.
* Arianna: (zauważyła spojrzenie jakie rzucili sobie kapitanowie)
* Darbik: Panie komodorze, to jest możliwe. Orbiter patroluje ten teren. Kraloth nie zna tego, mógł zrobić błąd.
* Bladawir: Natalia Gwozdnik się nie odnalazła. Kraloth nie handluje niewolnikami. Może zginęła w eksplozji. Tak czy inaczej - flota leci tam zająć się tym. Pytania?
* (cisza)
* Bladawir: Zaara, na Infernię. Znajdźmie mi informacje o Erneście Bankierzu. Kapitan Verlen?
* Arianna: Komodorze?
* Bladawir: Jesteś w stanie wykonać tą operację załogą którą masz?
* Arianna: Tak, komodorze.
* Darbik: Potwierdzam, komodorze. Infernia jest w stanie wykonywać operacje.
* Bladawir: (rzucił mu chłodne spojrzenie "nie mieszaj się jak nie rozumiesz"). Cieszy mnie Pańska rekomendacja, kapitanie Darbik. Jest pan dobrym znawcą charakteru.
* Arianna: konkretne polecenia co robić?
* Bladawir: Oni dostali instrukcje. Chcesz otrzymać jakieś dodatkowe polecenia? (lekko zdziwiony)
* Arianna: Tak, panie komodorze. Infernia nie ma wymienionej załogi, ale mogę sprawnie działać tym co mam.
* Bladawir: Zostawcie nas samych. (pozostali kapitanowie poszli). Zaara, Ty też. (Zaara wyszła, patrząc tęsknie za Bladawirem)
* Bladawir: (usiadł) (myśli) (strasznie długo myśli)
* Bladawir: Kapitan Verlen, czy uważasz, że jeśli tam jest ciało Natalii Gwozdnik, to jesteś w stanie ją Przywrócić i przepytać by inna czarodziejka Aurum nie ucierpiała tak samo?
* Arianna: Jeśli ciało nie będzie Skażone, potencjalnie tak. Jeśli była pod wpływem kralotha, boję się wpakować w nią magię.
* Bladawir: Ernest Bankierz nie mógł zniszczyć kralotycznego statku. To nie ma sensu. To jest ślepa kura i ziarno.
* Bladawir: Najpewniej to jest dywersja. Musimy to udowodnić. Dlaczego Ernest Bankierz. Najpewniej...
* Bladawir: Czy jesteście w stanie swoją magią wykryć czy kraloth zniszczony był CAŁY czy spawnował potomka?
* Arianna: Nie wiem czy jestem w stanie.
* Bladawir: To wszystko nie ma sensu, kapitan Verlen. Dobrze. Szukajcie o Erneście Bankierzu. Czemu on. I gdzie ten kraloth może być. Szukasz igły w stogu siana.
* Arianna: Panie komodorze, co panu nie pasuje? Bankierze są ambitnym rodem i... nawet jeśli kapitan nie jest najlepszy, to kapitan mógł tego dokonać.
* Bladawir: Nie mogę Ci powiedzieć. Ale... (pokręcił głową ze smutkiem)...
* Arianna: Może dodatkowe polecenia jak przez Aurum zaciągnąć języka?
* Bladawir: (ożywił się). Kapitan Verlen, ten kraloth... opracuję dla Ciebie te pytania. (nigdy Arianna nie widziała go tak zagubionego, autentycznie jego model się rozsypał)
* Bladawir: Niestety, musisz mi zaufać. Coś jest nie tak z kapitanem Bankierzem i kraloth NIE ZOSTAŁ zniszczony. Nie mógł być.
* Arianna: Rozumiem komodorze, jeśli jest taka szansa...
* Bladawir: Jest coś, co możesz dowiedzieć się z Aurum. A dokładniej - ile narkotyków imprezowych było przetransportowanych przez Karsztarin w tym roku. Nie pytam 'kto'. Pytam 'ile'.
* Arianna: Pyta pan czy bardzo dużo?
* Bladawir: Nie mogę powiedzieć Ci czemu pytam. Ale jeśli nie dostaniemy odpowiedzi... jeśli nie jesteś agentką i nie dostaniemy odpowiedzi i nie rozwiążemy ich, więcej tienów ucierpi jak Natalia Gwozdnik. Jeśli jesteś agentką, to się dowiem.

Infernia. Lecą w stronę ruin jednostki. Klaudia kombinuje żeby poszukać czegoś na temat Ernesta Bankierza. Coś ciekawego. Bo nawet jak Bladawir jest paranoikiem... to jednak coś musi wiedzieć. I Klaudia ma Zaarę pod ręką. Ma Wielką Księgę Złych Czynów Bladawira. Plus wejścia do baz K1. Klaudia szuka wszystkiego - od plotek po tematy medyczne. A Arianna prosi Kamila by kultystów posłał po barach i plotkowali. Zwłaszcza, że sporo kultystów zostaje na K1. Czyli część Kultystów Arianny jest wysłana na MISJĘ. Plotki i zbieranie informacji.

Ex (+kultyści na misji, +dostępy do baz itp, +hiperkompetencja Klaudii i Zaara); Klaudia podchodzi jak do naukowego researchu i kieruje Zaarą. Zaara jest uległa. Robi co powinna. Jest smutna. Klaudia się cieszy, bo Zaara wreszcie nie przeszkadza i się słucha; nie ma dla niej znaczenia Zaara by to zauważyć -> **Tr Z +4**:

* Xz: Klaudia nie wierzyła że Zaara współpracuje. To spowodowało, że przypadkowo obie się sabotowały i zdaniem Zaary Klaudia zrobiła to specjalnie.
    * Zaara: porucznik Stryk, to jest ważniejsze niż Twoje niskie uczucia wobec komodora i mnie.
    * Klaudia: jeśli już to niska opinia, ale widzę że jednak słuchałaś co mówiłam
    * Zaara: więc jeśli dostosowałam się do Ciebie, to rób pracę uczciwie i dobrze.
    * Klaudia: zawsze robię uczciwie i dobrze
    * Zaara: stać Cię na więcej. Jesteś lepsza niż demonstrujesz. Ale używasz tego tylko do gier politycznych.
    * Klaudia: (parska śmiechem) spytaj dowolną osobę na statku
    * Zaara: (niezrozumienie) ...Infernia ma najlepszego komodora we flocie... czemu to wszystko... tak nie działa...
* X: Klaudia i Zaara nie mogą współpracować razem. Po prostu. To nie działa. Są minusem sama na siebie. Jedna w prawo, druga w lewo. Nawet jak obie się starają, po prostu nie są w stanie.
    * Klaudia: jest jeszcze jeden sposób by to zrobić. To nie działa. Ja robię swoje, Ty swoje.
    * Zaara: ale komodor chciał byśmy pracowały razem. (przez łzy wściekłości)
    * Klaudia: czy jakby komodor kazał Ci się przebić przez ścianę rozgotowanym spaghetti...
    * Zaara: (przerwała Klaudii) gdyby kazał mi zabić Ciebie a potem siebie, nie zawahałabym się pięciu sekund. On mnie uratował. On ma moje życie w garści. Ja bez niego jestem martwa. Gorszy los niż śmierć. (oddanie i uduchowienie)
    * Klaudia: wiesz co, Twój wybór. Ale jest różnica między 'robię bo ma sens' a 'ktoś Ci kazał' i to ważna różnica. Chcesz - rób co uważasz, porównamy wyniki i skompilujemy. Nie chcesz - zostaw mnie w spokoju.
    * Zaara: (myśli) jeśli pani kapitan wyda mi rozkaz nie współpracowania z Tobą tylko działania niezależnie, będę działała niezależnie. Tego chcesz?
    * Klaudia: chcę byś w tym wypadku nie wymuszała współpracy której obie widzimy że nie diząła
    * Zaara: to pani kapitan musi wydać mi ten rozkaz. Ja nie mogę jej zaproponować by go wydała.
    * K->A (po hipernecie): ZDEJMIJ MI JĄ Z GŁOWY, TO NIE DZIAŁA, ONA NIE CHCE IŚĆ.
    * Arianna: Zaaro, chodź mi pomóc z pewnymi danymi
    * Zaara: Tak jest, pani kapitan. (Zaara siedzi z Arianną i pracuje nad danymi Arianny. Mimo pełni frustracji i złości próbuje być profesjonalna. Ale - powstrzymuje łzy)
* X: Mimo, że Klaudia się bardzo starała itp, to jak Zaary zabrakło, zaczęło działać. Zaara i Bladawir mają odczucie, że Klaudia nie współpracuje z Zaarą i tyle. Klaudia BĘDZIE działać, ale sama. I w tą stronę Bladawir będzie to przesuwał. To sprawia, że Bladawir jest ZALEŻNY od Klaudii i wie o tym. I bardzo mu się to nie podoba. Bo uważa Klaudię za bardzo prawdopodobnego agenta.
    * Klaudia poznaje ciekawostki.
    * PIERWSZA: Ernest Bankierz jest kobieciarzem.
    * DRUGA: jego pierwsza oficer, Talia Miraris wymienia mu sukcesywnie załogę na damy chętne awansu.
    * TRZECIA: Talia ma brata, który jest kapitanem Orbitera - Dormand Miraris.
    * (Zaara udostępniła) Dormand Miraris jest zamieszany w narkotyki imprezowe i jest bardzo popularny.
        * Bladawir próbował go ugryźć, ale nic nie znalazł.
    * Wszystko wskazuje na to, że patterny Ernesta Bankierza się nie zmieniły.
* Vz: 
    * Natomiast Talia Miraris - jej zachowanie się zmieniło rok temu. Od wtedy zaczęła Bankierzowi dostarczać młode damy na pokład.
    * Statek Bankierza ma poniżej przeciętny poziom kompetencji. Niżej niż 50%, bo ma zbyt ładną załogę oficerską
    * Talia ZAWSZE była kompetentnym oficerem. Podobno: 
        * Talia jest w nim zakochana.
        * Talia ukrywa coś w taki sposób (np. inklinacje pana kapitana).
        * Talia chroni SIEBIE (żeby Bankierz nie skupiał się na niej).
        * Talia NIE ŻĄDA PRZENIESIENIA.

Flota Bladawira dociera do ruin Wirmerina. Tam znajdują się dwa inne statki Orbitera.

* Bladawir: Kapitan Verlen?
* Arianna: Komodorze?
* Bladawir: Czy jesteś w stanie zapewnić, żebyśmy mogli wejść do tych ruin. 
* Arianna: Badania, sytuacja.
* Bladawir: Chodzi mi z perspektywy formalno-prawnej. Nie będę wydawał rozkazów Twoim oficerom. Ale musimy znaleźć ślad kadet Gwozdnik i dowiedzieć się o kralocie co się da. To wymaga dobrej klasy oficera naukowego. Mam kilku, ale Ty masz czarodziejkę.
* Arianna: Zajmę się tym, komodorze.
* Klaudia: Mam cokolwiek czym mogę wesprzeć to żądanie?
* Arianna: Nie, komodor Bladawir nie ma niczego...
* Klaudia: Przygotuję papiery by przeszły przez załogę Bankierza. By salutował jak Bladawir przedstawi papiery.

Tr Z +3 (Bladawira nikt nie lubi i nikt go nie chce, ON utrudnił stopień trudności):

* Vz: Dokumenty Klaudii przeszły inspekcję. Wirmerin jest otwarty.
* Xz: Bladawir jest bardziej zdesperowany niż się wydaje. Nie robi błędów JESZCZE, ale już posuwa się za daleko.
* V: Dokumenty Klaudii są 100% rock solid. I Bladawir wie, że przez Klaudię.

Arianna i Klaudia są zaproszone na Adilidam. Bladawir. Klaudia - nienaganny mundur i idzie jak w środek strefy wojny. Wita ich porucznik Marian Witaczek, XO Bladawira.

* Witaczek: Tien Kapitan Verlen, czarodziejko porucznik Stryk?
* Witaczek: Komodor poprosił, byście przeszły do strefy dekontaminacyjnej. Insercja na jednostkę będzie wymagała specjalnych servarów.
* Arianna: (łapie Zaarę i jej to demonstruje)
* Zaara: (deer-in-the-headlights) Faktycznie, pani kapitan. Nie zauważyłam. Zmienię plan - zażądam dwóch oddziałów marines.
* Arianna: Brzmi lepiej. Mogę zaoferować oddział od nas.
* Zaara: Macie tę cirruskę (Leonę), prawda? To dobry pomysł.

40 marines, 4 magów: (Klaudia, Samuel, Zaara, Arianna) i NIE Bladawir. Klaudia przy użyciu marines ściąga odpowiednie czujniki. Statek jest po dekompresji. Jest rozwalony. Nie ma bezpiecznych miejsc. Jest posiekany pociskami i bronią energetyczną. To był statek cywilny, który został celem statku Orbitera. Wyraźnie widać - ktoś spanikował i strzelali jak już nie musieli. Zniszczenia Wirmerina są dużo większe niż powinny. Nie wygląda to na świadome działanie, raczej na OMG OMG OMG KRALOTH OMG OMG OMG. (zdaniem Bladawira to spisek, ale tak to wygląda - MIAŁO NIE BYĆ ŚLADÓW NIE?)

Tkanka kralotyczna na pierwszy rzut oka jest martwa. Tu zginął kraloth albo kraloth zostawił zewnętrzną powłokę. Ale tu coś kralotycznego było. To był statek bardziej typu 'pułapka' niż typu 'zestrzelę Cię'.

* Bladawir: Kapitan Verlen, Zaara i czarodziej Fanszakt zdecydowali się zostać ochotnikami by wzmocnić Twoje możliwości i możliwości agentki Stryk.
* Samuel: Pani kapitan, jestem dobry w ekranowaniu i czyszczeniu energii magicznych; będę w stanie Wam zapewnić osłonę.
* Arianna: Rozumiem, co mamy zrobić?
* B (priv) -> A: czy jesteś w stanie animować kralotha?
* Arianna: nie zrobię tego, komodorze, nawet jeśli pan tego chce. Za duże zagrożenie. To nie jest kwestia lojalności, reanimacja kralotha to żywy kraloth. Z arcymagiem?
* B (nie złości się, przetwarza): czyli to jest Twoja granica możliwości? Uważasz, że nie możesz reanimować go lojalnego?
* Arianna: Tak, energia mnie Skazi. Nie zostanę taka sama.
* Bladawir: mimo ekranowania i wsparcia?
* Arianna: komodorze, to jest kraloth.
* Bladawir: dobrze, wycofuję polecenie.
* Bladawir: (głośno): Waszym celem jest znaleźć jak najwięcej dowodów na to, że tu BYŁ kraloth. Pełen kraloth. Lub że go nie było. Znaleźć ślady kadet Gwozdnik. Znaleźć ślady Skażenia kralotycznego.
* Klaudia: (na priv do Arianny) mam przygotowaną krew tien Gwozdnika, na taką sytuację /kocia minka

Arianna jest wiodąca (jako arcymag), prowadzona przez Klaudię jako oficer naukową. Samuel ekranuje jak może, Zaara jest baterią. WSZYSCY mają specjalne servary Bladawira dla magów (unieszkodliwić, wyłączyć). Marines chronią, ale oprócz ochrony też mają Was na celowniku.

Ex Z M +5Ob +4:

* Xz: Nadmiar energii magicznej, wymogi, Sympatia, Esuriit, rezonans z kralothem - Zaara jest wyczerpana magicznie. Ona potrzebuje tydzień regeneracji i odkażenia potem. Jest do niczego.
* Ob: (hidden) (kraloth reforming)
* Vr:
    * jest tu CIEŃ Natalii; to znaczy, że kraloth ją kiedyś Dotknął. Ale za delikatny. Czyli jej nie wchłonął.
    * to co nie żyje to był pełnoprawny kraloth. Faktycznie kapitan Bankierz rozwalił kralotha.
        * (Arianna: ktoś podrzucił kapitanowi Bankierzowi kralotha do rozwalenia)
        * kapitan Bankierz walił jak w bęben, FULL POWER, a statek wyraźnie był statkiem kralotycznym
* (Arianna przepuszcza przez Zaarę sygnał emocjonalny +Vg) X: Zaara dostała mentalną falą. _Zaara ma flasback_ 
* Vr: Arianna ma ECHO tego co się tu działo
    * Załoga kralotycznego statku nie spodziewała się ataku. Oni nic nie wiedzieli
    * Załoga kralotycznego statku była w "typowej operacji", zmieniali jednostkę na inną. Kraloth chciał zmienić jednostkę. ZNOWU.
    * Kraloth nie celował napaść na inną jednostkę - chciał się przenieść do 'friendly unit'
* V: Arianna dostała SYGNAŁ
    * Kraloth NIE BYŁ na szczycie. Kraloth był pod kontrolą LUDZI. Słuchał ich, bo to dawało mu większe korzyści.
    * Natalia była wzięta przez nich. Kraloth ją zniewolił, oddał ją ludziom. Za INNE rzeczy.
    * Arianna ogólnie chciałaby się bardzo wykąpać.
    * KRALOTH JEST ŚRODKIEM A NIE OVERLORDEM
* (Arianna chce użyć mocy kralotycznych +1Vg) X: (podsumowując)
    * Kraloth się reformuje
    * Zaara ma pełen flashback, "jest w Aurelionie"
    * Samuel pragnie regeneracji kralotha - wpadł pod kontrolę
* V:
    * Arianna chce się dowiedzieć WIĘCEJ o Zaarze.
        * Noktianka, kochająca rodzina, wojna deoriańska, wygnanie.
        * Syndykat - przechwycił ją. Straszne, straszne rzeczy. Obudził w niej magię. I od razu eksperymenty na wzorze. Zaara straciła umysł i duszę zyskując magię.
        * Bladawir ją wydobył. Bladawir próbował ją naprawić. Orbiter zrobił co mógł. Przepięli 'obwody Syndykatu' na lojalność wobec Bladawira. Bo on się nią opiekował.
        * Bladawir stracił żonę. Do Syndykatu. Z zemsty za Zaarę. Bladawir stracił rodzinę do Syndykatu.
        * Bladawir WSZĘDZIE i ZAWSZE szuka Syndykatu. Za rodzinę i za Zaarę.
            * Musiał sam zabić część rodziny pod kontrolą Syndykatu. Potencjalnie zabił kogoś NIE pod kontrolą jako 'pre-mercy kill'
            * M.in. dlatego jest tak nielubiany i odsunięty na bok
        * **Bladawir szuka Syndykatu Aureliona na Orbiterze** z całą swoją paranoją
        * Bladawir poluje na własną rodzinę, która stała się agentami Syndykatu.
        * Bladawir śpi z Zaarą. Oczywiście że tak. Ale ona 
        * I stąd łzy - jeśli Zaara nie zadowala Bladawira - to Zaara czuje cierpienie i rozpacz
    * Arianna użyła starych "Obwodów Aureliona" w Zaarze by zablokować jej możliwość powiedzenia co jej zostało zrobione i co jej się stało (teraz, tutaj).
* Vz
    * Arianna używa energii kralotycznej i mocy arcymaga i BATERII i traumy Zaary i tego wszystkiego i niszczy jej Wzór.
    * Arianna zostawia Wzór Zaary otwarty; da się ją naprawić
    * To czego Orbiter nie był w stanie zrobić - to Ariannie i kralothowi się udało
    * Zaara uzyskała możliwość zdradzenia Bladawira
    * Zaara jest możliwa do rekonstrukcji i można jej pomóc

Zaara jest nieprzytomna i ma flashback. Samuel jest pod kontrolą kralotyczną. Arianna jest osłabiona i ledwo się trzyma na nogach oraz jest _BRUDNA_ w środku. Klaudia jest w najlepszej formie.

Bladawir naciska czerwony guzik i neutralizuje wszystkich magów swoimi servarami...

## Streszczenie

Arianna planowała ściągnięcie Władawca i przy jego pomocy uwiedzenie Zaary by zniszczyć Bladawira. Z pomocą Klaudii załatwiła transfer Władawca na Infernię (i pewną roszadę załogi). Gdy okazało się, że kraloth został zniszczony przez kapitana Ernesta Bankierza, Bladawir jest jeszcze bardziej zagubiony. Przekierował swoją flotę by tam wejść i dowiedzieć się wszystkiego o konspiracji ("bo to niemożliwe"). Gdy Arianna połączyła się z tkanką kralotyczną z Klaudią, Zaarą i Samuelem udało jej się odkryć że kraloth współpracuje z większą organizacją (która chyba się go pozbyła XD). Co więcej - udało jej się ZROZUMIEĆ przeszłość Zaary i Bladawira. Bladawir stracił wszystko przez Syndykat Aureliona i walczy z nimi dalej, konspiracja w konspiracji. Ariannie udało się użyć kralotycznej mocy by Zdestabilizować Zaarę - niech jest możliwa do naprawienia, rekonstruowała ją integrując ją z tkanką kralotyczną i wyłączyła Zaarze możliwość powiedzenia o tym że cokolwiek jej się stało.

Czyli celem Bladawira jest Syndykat Aureliona. Siła, na którą Bladawir poluje obsesyjnie. Każdy może być członkiem Aureliona. I nagle Arianna nie chce już tak bardzo niszczyć Bladawira i kontrolować Zaary...

## Progresja

* Arianna Verlen: rozprzestrzeniła agentów Kamila na Kontrolerze Pierwszym; chce mieć agentów w różnych miejscach
* Klaudia Stryk: nie jest w stanie współpracować z Zaarą Mieralit. Po prostu ich sposoby myślenia i patterny są zbyt niekompatybilne.
* Zaara Mieralit: nie jest w stanie współpracować z Klaudią Stryk. Po prostu ich sposoby myślenia i patterny są zbyt niekompatybilne.
* Zaara Mieralit: jej Wzór jest zdestabilizowany przez kralotyczną infuzję (działania Arianny) i nie jest w stanie nikomu o tym powiedzieć że coś się z nią stało lub dzieje.
* Zaara Mieralit: prawdziwa nienawiść do Klaudii i do Arianny. Częściowo nie wie czemu (kraloth violation), częściowo: dlaczego one są o tyle od niej lepsze. Dlaczego one nie chcą służyć Bladawirowi jak powinny? Czemu ONA nie jest dość dobra?
* Antoni Bladawir: całkowicie zagubiony pomiędzy dziesiątkami konspiracji i planów Orbitera. Bierze narkotyki, by się wzmocnić i by być niestrawnym dla magii. He Will Not Serve Arianna After Death.

## Zasługi

* Arianna Verlen: poprosiła Władawca, by dołączył do Inferni i uwiódł dla niej Zaarę, przekonała Bladawira że reanimacja kralotha to fatalny pomysł, zdjęła Zaarę z głowy Klaudii i na Skażonej kralotycznie jednostce poznała sekrety kralotha jako wiodący mag w kwartecie, po czym wstrzyknęła w Zaarę adaptogen kralotyczny by ją naprawić po poznaniu jej sekretów. Czuje się brudna, ale wreszcie poznała sekret Bladawira i Zaary.
* Klaudia Stryk: zapewniła Władawca na Infernię, skupia się na dekoncentracji Bladawira (by nie patrzył na Infernię), znajduje ciekawe rzeczy o Erneście Bankierzu i zupełnie nie jest w stanie pracować z Zaarą, nawet jak ta się stara. Niekompatybilność mentalna. Zrobiła perfekcyjne papiery, by Bladawir miał wejście na Skażoną kralotycznie jednostkę SC Wirmerin.
* Antoni Bladawir: grasping at straws. Próbuje łagodniej podejść do Inferni; tak zdewastowany, że aż PRAWIE powiedział Ariannie coś czego nie chciał. I tak pokazał kilka śladów które Arianna może znaleźć. Zaczyna robić błędy - chciał użyć magów bez obstawy ludzi, chciał Reanimować kralotha by go przesłuchać. Ale coś znalazł i dookoła kralotha i działań Ernesta Bankierza. Pytanie - co.
* Zaara Mieralit: smutna, bo Bladawirowi się rozsypuje plan. Pracuje z całych sił chcąc współpracować z Klaudią, ale nie umie - przez resentymenty i skillset. Nie jest dość dobra. Gdy pomaga Ariannie użyć mocy na tkankę kralotyczną, Arianna się wbija w jej rdzeń i ją edytuje kralotycznie i wydobywa z niej wiedzę. Zaara CZUJE że coś jest nie tak ale nie wie co.
* Samuel Fanszakt: mag służący za puryfikatora i bariery. W projekcie ekstrakcji danych o kralocie, uległ Skażeniu kralotycznemu. Bladawir go shutdownował gazem z customowego Lancera zanim coś się stało ;-).
* Władawiec Diakon: dzięki machinacjom Klaudii, jest dostępny na Infernię. W swoim stylu podrywa Ariannę i z przyjemnością dołączy do niej na Inferni pomóc jej z Zaarą.
* Marian Witaczek: XO Bladawira, na Adilidam. Bladawir wysłał go do komunikacji z Arianną i Klaudią by niepotrzebnie nie antagonizować.
* Ernest Bankierz: mało znaczący i ważny kapitan Orbitera, pies na kobiety (z danych Klaudii). Jego mostek jest jak mostek z anime - same ładne dziewczyny. ROZWALIŁ KRALOTHA czego nie może przeboleć Bladawir.
* Talia Miraris: XO Ernesta Bankierza, podobno sprowadza mu dziewczyny na mostek z anime ;-). Kiedyś modelowa oficer, rok temu się to zmieniło i stała się bardziej pro-Bankierzowa (dane Klaudii).
* Dormand Miraris: (nieobecny) kapitan Orbitera i przyjaciel Ernesta Bankierza; ogólnie (z danych Klaudii) ma jakieś związki z narkotykami imprezowymi.
* Kazimierz Darbik: nie boi się dyskutować z Bladawirem i nawet się wstawił za Arianną przed Bladawirem (nie znając kontekstu historii jej i Bladawira)

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Pierścień Zewnętrzny

## Czas

* Opóźnienie: 3
* Dni: 3
