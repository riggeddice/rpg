## Metadane

* title: "Ixioński Transorganik"
* threads: pieknotka-kaplanka-saitaera, wojna-o-dusze-szczelinca, saitaer-bog-primusa
* motives: ixionska-synteza, dotyk-saitaera, niepelna-kontrola, gift-is-anthrax, przeciwnik-terrorform, perfect-vision, sojusznik-bardzo-dziwny, nieoczekiwana-interakcja-magiczna
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [190120 - Nowa Minerwa w nowym świecie](190120-nowa-minerwa-w-nowym-swiecie)

### Chronologiczna

* [190120 - Nowa Minerwa w nowym świecie](190120-nowa-minerwa-w-nowym-swiecie)

## Projektowanie sesji

### Pytania sesji

* .

### Struktura sesji: Frakcje

* Kornel: przekonać Minerwę, by pojechała z nim
* Kasjopea: zobaczyć koszmar Minerwy
* Minerwa: pomóc uczniowi
* Liliana: pomóc uczniowi

DARK FUTURE: Wszyscy mają katastrofalne problemy

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. Wojtek Kurczynos robi krzywdę Karolinie Erenit
2. Wojtek potrzebuje pomocy Lucjusza Blakenbauera - i potrzebuje pomocy Saitaera
3. Minerwa zostaje okrzyknięta potworem

### Tor Bossa

1. Trzy poziomy fazy 1
2. Trzy poziomy fazy 2
3. Trzy poziomy fazy 3

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (19:30)

_Pustogor, Gabinet Pięknotki_

Pięknotka została wyrwana ze snu przez sygnał Saitaera. Bóg rozkazał jej się przebudzić. Powiedział Pięknotce, że Minerwa użyła czegoś z martwego świata i przekształciła maga w terrorforma. Ale w niewłaściwego terrorforma - w coś, co jest ze sobą niekompatybilne. Coś, co zadaje sobie ból i nienawidzi siebie. Wyjaśnił, że jego świat umarł w wyniku wojny - a to coś właśnie pochodzi z czasów tamtej wojny. Nie jest to coś co zagraża Astorii, ale zabije tego maga docelowo. (7,3,6=S). Pięknotka zobaczyła tą scenę - w Cyberszkole Minerwa zmieniła maga w terrorforma jakimś dziwnym artefaktem.

Pięknotka szybko strzeliła sygnał do Minerwy po hipernecie. Faktycznie, Minerwa wraca z Cyberszkoły do domu, spokojna, jak gdyby nigdy nic. Pięknotka wciąga w sprawę Erwina. Wysłała go do Cyberszkoły; sama poszła szybko do Minerwy. Próbuje ją przechwycić, zanim ta wróci do domu. Bez kłopotu ją przechwyciła.

Minerwa. Pięknotka od razu zauważyła, że Minerwa nie łże - pomyliły jej się światy. Jest Karolina Erenit, którą wykorzystują magowie ze Szkoły Magów, więc Minerwa chciała zapewnić, że to się nie będzie działo. Użyła Ixiońskiej Klątwy - ale nie jesteśmy w Ixionie. Jesteśmy na Primusie. Minerwa nadal nie rozróżnia rzeczywistości. Pięknotka widzi, że to "małe nieporozumienie" może być koszmarnym problemem, ale przynajmniej Minerwa nie jest jakaś sadystyczna czy zła. Pięknotka poprosiła Minerwę, by ta poszła z nią.

Tymczasem Erwin robi skan katalityczny w Cyberszkole. (TrM: 10,3,5=S). Udało mu się zlokalizować wszystkie informacje i ślady. Na to weszły Pięknotka i Minerwa. Erwin wyjaśnił - ten mag się zintegrował z komputerem i jest w ciągłym bólu i agonii. Wierzy, że jak zabije konkretną osobę to ból minie. Pięknotka szybko skojarzyła - Karolinę Erenit. Muszą się spieszyć...

Pięknotka strzeliła sygnał do Nataniela Bankierza. "Awaken". Dowiedziała się od niego jaki jest adres Karoliny. Nie wyjaśnia po co jej - powiedziała, że ma się nie interesować. Magowie odpalili power suity i polecieli (niosąc Minerwę) do celu.

**Scena**: (20:00)

_Zaczęstwo, Sypialnia Szczelińca, Osiedle Ptasie_

Magowie dotarli przed Wojtkiem. Karolina jeszcze śpi. Pięknotka poinformowała Minerwę, że ta robi za wabik. Robią scenkę w stylu "dwa power suity wyciągają dziewczynę z okna i spierniczają" - by Wojtek nawet nie sprawdził. (Tp:9,3,3=P). Odciągnęli, ale nie docenili prędkości Wojtka. Wojtek wrzasnął pełen agonii jako terrorform, po czym strzelił laserem prosto w Erwina i Pięknotkę by ich zestrzelić. Pięknotka odpala zasób "servary". (Tr:10,3,5=S). Wygląda, jakby ich trafił - mają przewagę. A na serio, oni kontrolują sytuację.

FIGHT ZOOM IN

Pięknotka zostawiła Erwinowi osłonę Minerwy. Wojtek driftował na swoich kołonogach, po czym strzelił wiązką lasera. Wojtek jest w agonii, wieczne cierpienie przez niekompatybilność energii Ixiońskich z Primusem. (Tp:9,3,3=S). Erwin wskoczył z Minerwą za jakiś śmietnik i w ten sposób uniknął wiązki. Pięknotka wyskoczyła w górę z latarni, choć latarnia wyparowała w laserze.

* "To nie laser! To czysta, korupcyjna moc magiczna! Cała jego energia w to jest przekierowana!" - Erwin
* "Atena! Pomóż!" - Pięknotka - "Przenieś mnie i tego potwora na Trzęsawisko!"

(Teleport: 15,5,12=SS). Udało się - Atena i Kirył przenieśli ich zanim doszło do większych zniszczeń. A Erwin i Minerwa muszą pogadać z Tymonem - nie ma dowodów!

FIGHT ZOOM OUT

**Scena**: (20:27)

_Trzęsawisko Zjawosztup, Głodna Ziemia_

FIGHT ZOOM IN

Zasoby: power suit

Pięknotka wzbiła się w powietrze unikając Głodnej Ziemi. Jej przeciwnik nie ma tego szczęścia - ugrzązł w bagnie. Pięknotka strzela w niego tranquilizerem. (Tr:10,3,5=S). +status "mniej boli, śpiący". Wojtek wrzasnął głośno, w tej samej agonii. Strzelił w siebie własnym działem i się przemutował - ma łapki i nogi. Wyskoczył prosto na drzewo i je okrążył. Pięknotka nie ma go w polu widzenia.

Pięknotka natychmiast zaczęła przywoływać pnączoszpony swoimi chemikaliami (Tp:9,3,3=S). Za moment się kilka powinno pojawić i odwrócić uwagę Wojtka od Pięknotki. Tymczasem jednak Wojtek wychylił się zza drzewa i wystrzelił laserem. (Tp:9,3,3=SS). Lekko uszkodzony power suit - Pięknotka zrobiła unik, ale nie spodziewała się tak szybkiego manewru. Szczęśliwie, pojawiły się pnączoszpony. Skupiły uwagę na Wojtku - a on na nich.

Pięknotka rzuciła zaklęcie - coś rozbijającego struktury mechaniczne, korozję, bez dotykania elementu bio. Ma dzięki temu zamiar obłupić Wojtka ze skorupy by osłabić mu siłę ognia i go wykończyć energegycznie. (TpM:12,3,2=S). Pięknotce udało się wprowadzić go w szok magiczny. Wojtek wpadł do bagna i zaczął regenerować. Nie jest w stanie nic innego zrobić - stracił przytomność. Pnączoszpony zadały mu też solidne rany.

FIGHT ZOOM OUT

Pięknotka rzuciła zaklęcie przyzywające Wiktora Sataraila. (TpM:12,3,2=S). Wiktor pojawił się stosunkowo szybko. Uspokoił pnączoszpony; Pięknotka wyłowiła dzieciaka z bagna. Wiktor spojrzał na Wojtka - jest autentycznie zaskoczony jego stanem. Nie widział nigdy czegoś takiego. Nie wie do końca jak mu pomóc... do jego laboratorium.

_Trzęsawisko Zjawosztup, Laboratorium Wiktora Sataraila_

Wiktor spróbował pierwszych środków separacji (7,3,6=S). Udało mu się, acz chłopak niczego nie będzie pamiętać. Wiktor stwierdził, że jest to prostsze niż się obawiał - niech Pięknotka go stąd weźmie. I niech nie bawi się z tematami dotyczącymi bóstw...

Wpływ:

* Ż: 12
* K: 3

**Sprawdzenie Torów** (21:05):

2 Wpływu -> 50% +1 do toru. Tory:

* Zmiany w Wojtku: 2 -> X
* Nieufność wobec Minerwy: 4 -> XX
* Niskie morale Minerwy: 4 -> VV
* Hold Saitaera na Pięknotce: 2 -> X

**Epilog** (21:10)

* Zadziwiające, ale nikt nie skojarzył problemów z Minerwą. Tymon puścił to płazem, gdy dowiedział się o tym, że Pięknotka się tym zajmuje.
* Saitaer pomógł Pięknotce, ale nie udało mu się wzmocnić jej lojalności wobec siebie. Still too creepy.
* Minerwa wie, że spieprzyła. Bardzo nie ufa swojemu podejściu i swojej magii. Czuje się wyizolowana.
* Wojtek nie zmieni swojego podejścia - dalej robi to, co robi i tak jak robi. Ale Minerwa nie odważy się mu przeszkadzać.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |   12    |             |
| Kić           |   6     |             |

Czyli:

* (K): Minerwa znajdzie oparcie w Tymonie (2). A on wie, co przeszła i jej współczuje i chce jej integracji (1).
* (K): Wiktor Satarail opracowuje formuły radzące sobie z ixiońską transorganiką. Chce być w stanie to wyłączać (2).
* (K): Pięknotka postawiła monitoring na Karolinie Erenit, by nagrywać jak magowie się na niej znęcają. (1)

## Streszczenie

Minerwa próbowała pomóc Karolinie Erenit, ale niestety użyła artefaktu ixiońskiego i przekształciła wrednego Wojtka Kurczynosa w transorganika. Pięknotka z pomocą Epirjona przeteleportowała siebie i Wojtka na Trzęsawisko i tam z pomocą Wiktora Sataraila odwrócili tą transorganizację. Ten konkretny problem udało się dyskretnie rozwiązać.

## Progresja

* Minerwa Metalia: jej morale sięgnęło dna. Nie ufa swojej magii i swoim umiejętnościom już w ogóle. Nie czuje się częścią tego świata.
* Wojtek Kurczynos: naładowany energią ixiońską; przez to jest do niego dostęp przez moc Saitaera.
* Wiktor Satarail: opracowuje sposób odwrócenia ixiońskiej transorganizacji; częściowo już mu się to udało.
* Saitaer: planuje wykorzystać ixiońską energię rezydualną na Wojtku Kurczynosie.

## Frakcje

* .

## Zasługi

* Pięknotka Diakon: wezwana przez Saitaera do odwrócenia ixiońskiej transorganizacji, stoczyła ciężką walkę z terrorformem (Wojtkiem). Z pomocą Wiktora, odwróciła to.
* Minerwa Metalia: bardzo zależy jej na pomocy Karolinie Erenit, więc użyła transorganizacji ixiońskiej na Wojtka. Niestety. Potem odwracała z pomocą Pięknotki.
* Saitaer: zgłosił się do Pięknotki - zmusił ją do naprawy ixiońskiego transorganika który był Wojtkiem. Czemu? Bo to maladaptacja.
* Karolina Erenit: stały target Wojtka Kurczynosa i kilku innych magów Szkoły Magów w Zaczęstwie. Jako, że jest człowiekiem, nic nie pamięta.
* Wojtek Kurczynos: stał się terrorformem przez błędne zaklęcie Minerwy. Chciał zniszczyć Karolinę Erenit, ale został powstrzymany przez Pięknotkę.
* Erwin Galilien: katalista Pięknotki i dywersja by odwrócić uwagę ixiońskiego transorganika.
* Napoleon Bankierz: obudzony przez Pięknotkę w środku nocy, natychmiast jej odpowiedział na pytanie gdzie mieszka Karolina Erenit.
* Wiktor Satarail: pomógł Pięknotce na Trzęsawisku by nie stała się nadmierna krzywda Wojtkowi; odwrócił jego transorganizację.
* Kirył Najłalmin: mistrz teleportacji; dał radę przenieść jedynie Wojtka (terrorforma) jak i Pięknotkę na Głodną Ziemię.
* Tymon Grubosz: lubi Pięknotkę i nie chce jej robić problemów; puścił płazem ewentualne problemy z Minerwą i Erwinem.

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Cyberszkoła: tam Minerwa rzuciła ixiońskie zaklęcie na Wojtka i tam Erwin szukał terrorforma ixiońskiego.
                                1. Osiedle Ptasie: mieszkanie Karoliny Erenit oraz miejsce pierwszego starcia z terrorformem ixiońskim.
                        1. Trzęsawisko Zjawosztup
                            1. Głodna Ziemia: miejsce ostatecznej walki pomiędzy Pięknotką a terrorformem ixiońskim. Pięknotka wygrała ;-).
                            1. Laboratorium W Drzewie: baza Wiktora Sataraila; tam Wiktor odwrócił transorganizację Wojtka.

## Czas

* Opóźnienie: 3
* Dni: 1
