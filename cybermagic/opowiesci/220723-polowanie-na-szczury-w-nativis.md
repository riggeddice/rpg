## Metadane

* title: "Polowanie na szczury w Nativis"
* threads: historia-eustachego, arkologia-nativis, plagi-neikatis
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [220723 - Polowanie na szczury w Nativis](220723-polowanie-na-szczury-w-nativis)

### Chronologiczna

* [220720 - Infernia taksówką dla Lycoris](220720-infernia-taksowka-dla-lycoris)

## Plan sesji
### Co się wydarzyło

* .

### Co się stanie

.

* Cel przemytników: ukryć skrytkę PONOWNIE. Pozbyć się dzieciaków?
* Cel Janka: kasa na przetrwanie.
* Sceny:
    * s0: Zdobycie kontraktu na szczury
    * s1: Wkradanie się do starych części arkologii
    * s2: Znalezienie przypadkowe przemytników i uniknięcie załogi 'Erozji Ego'

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

Janek i Celina są bardzo młodzi, poniżej 16 roku życia. To dzieciaki. Mieszkają sami z dziadkiem (Karol) i dziadek ich nauczył wielu rzeczy, poza tym że dał wikt i opierunek. Widać, że dziadek ma mnóstwo doświadczenia i opowieści, ale ich nie mówi. Celina jest ciekawa, ale więcej wojuje z dziadkiem niż będzie pytać. Np. dziadek jest zwolennikiem zachowania podstawowej bioformy, Celina i Janek poszli za efektywnością i trochę modą. Janek próbuje trzymać pokój między Celiną i dziadkiem, ale Celina jest dużo inteligentniejsza.

Niestety, dziadek zużywa zapasy jakie zgromadził podczas życia. To znaczy, że - mimo, że dziadek mówi "nie martwcie się" to Janek się martwi co będzie w przyszłości. Celina się nieszczególnie przejmuje. Ale w którymś momencie Janek wrócił zadowolony i odciągnął Celinę na stronę. Ma POMYSŁ. Dowiedział się, że jest możliwość dorobienia sobie przez polowanie na szczury. Oczywiście, dla dorosłych. Ale Janek ma pomysł - można pokryć obszar mniej używany arkologii, z tuneli o których opowiadał dziadek. Tam, gdzie Celina i Janek łazili gdy byli młodsi. Tam są szczury i oni znają tajne przejścia. Więc mogą rozłożyć trutki w tamtym terenie i jednocześnie zapewnić sobie trochę kasy. Celinie się pomysł spodobał.

Jedyny problem - musimy przekonać administratora który się tym zajmuje (Marcel Kidiron) że możemy to zrobić. Np. można zapisać się jako dziadek! (Celinie już się "spodobał" pomysł Janka - 73-letni dziadek nie będzie czołgał się po tunelach). Jak więc przekonać?

Zdaniem Celiny - NIE włączamy w to dziadka. Biorą kogoś tuż powyżej granicy minimalnej i idą z nim na podział kasy. Jak się udziela - po równo, jak nie - odpalą coś. Słupem Celiny został Tymon Korkoran. Celina WIE, że Tymon podkochuje się w koleżance, która go zostawiła, bo Tymon był mało użyteczny dla arkologii. Celina wie też, że Tymon chce być "jak wujek Bartłomiej" (patriarcha Inferni). Tymon nie nadaje się na Infernię - jeszcze - więc czyn w formie polowania na szczury mógłby być czymś, co przekona Bartłomieja do Tymona. Więc - trochę kasy na randkę + bycie przydatnym.

Uzbrojona w tą wiedzę, Celina odwiedziła Tymona w barze "Śrubka z Masła". Tymon podryguje na parkiecie, z holoprojekcją dziewczyny i dwoma innymi kumplami. Wszyscy zarywają do tej samej holodziewczyna. Tymon się rozweselił na widok Celiny - dziewczęce zainteresowanie daje mu radość. Celina bierze go na bok i zagaja o planie. Celina streściła plan - trzeba rozłożyć trutkę w arkologii. Ona z Jankiem weźmie te podłe obszary a jak Tymon się przyłoży, dostanie swoją część. Jak słup, dostanie 5%. Tymon nie lubi brudnej roboty - i dobrze, bo Celina nie ma ochoty by on coś robił.

TrZ+4:

* V: Tymon się zgodzi na plan. Teraz tylko kwestia detali.
* V: Tymon zgodzi się na 5%. W końcu nic nie robi poza rozdziałem próbek i trutki na szczury i załatwiania spraw z Marcelem (adminem).
* X: Tymon dostał całuska w policzek i zdanie typu "jaki ty jesteś fajny, Tymonie" (Celina miała 'minor ick factor', ale spoko)
* Vz: Tymon runs interference z dziadkiem

Tymon, o dziwo, załatwił wszystko rzeczowo i na czas i już po 3 dniach Janek i Celina byli właścicielami pozwolenia, trutek, sprzętu i terenu. A Tymon siedzi na tyłku i zbiera 5% dywidendy. I nikt nie jest niezadowolony.

### Scena Właściwa - impl

Janek i Celina są przy tzw. Starej Arkologii Wschodniej. To obszar arkologii, który jest używany, ale jest bardziej zmechanizowany. Mniej ludzi, więcej maszyn, bardziej poniszczony. Jest tam sporo starych TechBunkrów, które były wykorzystywane przy budowie arkologii. Tam są przez to różne elementy inżynieryjne, mechaniczne - to raczej przetwórcza część arkologii. Łącznie z przetwarzaniem żywności.

Przed Jankiem i Celiną co najmniej jeden dzień ciężkiej, uczciwej pracy z rozkładaniem trutek i detektorów po tych fragmentach arkologii. Celina naszkicowała plan, jak to najsensowniej rozmieścić (wspierając się wiedzą o szczurach i wsparciem Prometeusa; oczywiście Prometeusowi Celina mówi, że robi to dla Tymona). I po planie uznali że pierwszy dzień poświęcą rozkładając trutkę i detektory w najłatwiejszych miejscach, nie używając swojej wiedzy o tunelach i skrytkach - by odstraszyć innych od tego miejsca i że "ktoś już tu rozkłada".

Tr Z (Prometeus + dwie osoby + plan Celiny) +2:

* Xz: więcej osób uznało, że to dobre miejsce na to, by rozkładać pułapki itp. Ktoś monitorował zapytania do Prometeusa. Lub Prometeus komuś powiedział. Tak czy inaczej - nie fair.
* X: konfrontacja z Laurencjuszem Kidironem.

Gdy Janek i Celina rozkładali próbki, Tymon się z nimi skomunikował. Cały niezadowolony. "Ej, popsuliście próbki, każą mi zapłacić kary jeśli to się nie naprawi. A jak się nie naprawi, to będziecie Wy płacić.". Celina w klopsie - ona i Janek robią to dobrze. Janek się cofnął sprawdzić próbki o których mówił Tymon. Ktoś zatruł trutkę. Wiedząc, że rzeczy da się wykryć, poszedł dalej, szukać po swoich innych próbkach - i tam napotkał zbirów.

Tr +3:

* V: Janek dał radę uciec
* X: Janek dostał 1-2 ciosy, ale nic poważnego
* V: Janek zgubił zbirów

Janek spotkał się z Celiną. Wygląda jakby dostał w pysk z basi. Otrząsnął się i zaczął sapać - szybko zwiewał. Wyjaśnił. Na tym terenie jest Laurentius Kidiron. Celina się wściekła - zastawiają pułapkę. Chce mieć fotkę jak oni psują ich próbki. Janek nie jest przekonany, czy to najlepszy pomysł. Celina - NIE BĘDĄ MI PSUĆ. Moje, uczciwie złożone. Janek nie do końca chętnie, ale się zgodził. Celina zwykle wygrywa te argumenty. Wyjaśnił, że Laurentius współpracuje z trzema innymi chłopakami, dwóch z nich to kumple Tymona. Celina - pfft.

Celina myślała żeby nasłać na nich Tymona, ale zmieniła zdanie. Tymon jest zbyt leniwy i woli nie mieć roboty niż potencjalnie stracić kumpli. Zamiast tego zadziała po swojemu.

Janek, z lekką niechęcią, zaproponował Celinie wykorzystanie niektórych access portów; mogą wykorzystać różne spraye skunksowe itp. Glitter bomb. Żeby Laurencjusz i ekipa nie zbliżyli się do Celiny i niego. A Celina ma umiejętności jak zmontować takie rzeczy a w tunelach ma dość wszystkiego co jej potrzebne, musi objąć tylko odpowiedni tech bunker. I - niespodzianka - dzięki dziadkowi ma dostęp do wielu i awaryjne kody. Dziadek był inżynierem arkologii, więc je zna.

Janek zdobywa dla Celiny spray skunksowy - rzygosmród przylegający do skóry. Janek umie się poruszać po podłych terenach i mimo zapominalstwa jest świetny w orientacji w terenie. Więc on wysłany. 

TrZ+3:

* V: Janek zdobył i pozyskał dla Celiny taki spray, z ich starych zapasów
* V: Celina zastawia pułapkę - jak się zbliżą do trutki to Celina to odpali.
* V: Nie mają DOWODÓW kto to był, nikt nic nie złapał / nie zobaczył

Jakie wrażenie wywarło to na tej ekipie (konsekwencje):

TrZ+4:

* X: Laurencjusz chce się mścić
* V: Musieli się ewakuować, wycofać, odkaszleć... ogólnie - zniechęceni do szkodzenia Zespołowi na tej akcji
* Vz: Chłopaki uważają "oki, fair game, to DZIWNE dzieciaki i lepiej z nimi nie zadzierać" - krótkoterminowo. Oni pogonili Janka i im popsuli, dostali skunksem, fair game.

Po tym, jak Zespół odstraszył rywali, Celina i Janek przyspieszyli z rozkładaniem tego cholerstwa. POWRÓT DO PULI:

Tr Z (Prometeus + dwie osoby + plan Celiny) +2 -2X (bo kontynuacja):

* Xz: Mimo starań i dobrego rozkładania po prostu nie nadążają. Szkody, które im wyrządzono są za duże. Cały dzień się starali i zamiast zarobić, stracili.

Celina i Janek wracają w MINOROWYCH nastrojach do pokoju. A opieprz ze strony Tymona nie pomógł. Tymon dodatkowo dowiedział się, co spotkało jego przyjaciół i "zamiast zajmować się tym to jeszcze niszczycie mi reputację! I tępicie moich kumpli! WIERZYŁEM CI CELINO!". A Celina na to "Spierniczaj. Jutro będzie lepiej. Trzeba było pilnować kumpli. Wszystko by rozpieprzyli i nic byś nie zarobił." Tymon "Miało być łatwo i bez problemu."...

Janek i Celina poszli spać. Następnego dnia zrobią to LEPIEJ. Janek "słuchaj, może pogadajmy z dziadkiem, pomoże wytyczyć najlepszą trasę?" Celina kategorycznie odrzuciła ten plan - czy Janek chce wysłuchiwać? Jak na razie to nic nie osiągnęli. Janek się zgodził z Celiną. Po prostu wolałby mieć jego wsparcie. Celina westchnęła. Ona też, ale to ONI się w to wpakowali... są w końcu prawie dorośli ;-).

Następnego dnia Janek i Celina zamiast iść do szkoły stwierdzili, że wykorzystają znajomość tuneli i arkologii. Tam za nimi nikt nie pójdzie, nikt im nie zaszkodzi i - co ważniejsze - tam będzie więcej pieniędzy z tych trutek i sygnałów. Najtrudniejszy był początek - przekraść się do tych tuneli mimo, że powinni być w szkole. Ale nie jest to dla nich duży problem.

TrZ (nie pierwszy raz) +3:

* X: muszą użyć innego tunelu; stracili jakąś godzinkę
* Vz: wiedzą jak się poruszają straże, roboty itp. Wiedzą, jak się schować i prześlizgnąć

Tunel jest w stosunkowo kiepskiej jakości, ale da się nim przedostać wszędzie gdzie trzeba. Nie znaczy że się rozpada - jest brudny, nieduży i był przeznaczony dla małych robotów a nie dla ludzi. Ale nasz Zespół to mali ludzie. Zaczęli więc rozkładać trutki na szczury w specjalnych tunelach, by tymi tunelami pokryć wszystko. A oni się praktycznie nie gubią w serwisowych tunelach arkologii - dziadek im kazał nauczyć się tego na pamięć. Taki jego dziwny nawyk.

TrZ (znajomość tuneli + Prometeus + plan) +4:

* V: uda się tyle porozmieszczać, żeby wyjść na zero
* V: uda się tyle porozmieszczać, że nawet Tymon będzie zadowolony a i Zespół coś ugrał.
* Xz: musieli iść nietypowymi, dziwnymi drogami i pouszkadzały się im ubrania
* V: udało się. Następne 5 dni (bo tyle operacja) będzie udanym rozmieszczaniem trutki na szczury i dorobią sobie godne grosiwo

Coś rzuciło się Celinie w oczy, poruszając się po tych dziwnych tunelach. Pewien tunel powinien być opuszczony, brudny itp. Ale wyraźnie ktoś tam BYŁ. Człowiek. Ktoś się tam poruszał i to całkiem niedawno. W tunelach które były "ich" (Celiny i Janka), bo dziadek o nich mówił.

* V: Celinie i Jankowi udało się znaleźć obejścia, by móc zlokalizować gdzie / jak poruszał się tamten człowiek. Wszystko wskazywało, że objął jeden z nieaktywnych techbunkrów. Ale nie było go w środku, więc Celina - wbrew radom Janka - zdecydowała się zobaczyć co tam jest.

W techbunkrze Celina znalazła mały komputer, trochę sprzętu, papierów itp. Rzeczy wskazujących na infiltrację / advancera. Small nest, small base. Celina korzystając z tego że i tak ma rękawice i zarówno ona jak i Janek są zamaskowani przejrzała niektóre z tych rzeczy. Uderzyło ją to, że znalazła zdjęcie dziadka z czasów jak był młodszy. Dziadek jest w dziwnym mundurze, Celina go nie rozpoznaje, Janek też. Jest na mostku jakiegoś statku? Celina zrobiła dokładne fotki, po czym spierniczają - im mniejsze, bardziej kiepskie tunele tym lepiej. I im mniej śladów.

* Vz: bardzo ciężka trasa. Ale udało im się wydostać, idąc też wyjątkowo kiepskimi tunelami, za wąskimi, z fragmentami stale uczęszczanych... by advancer nie mógł ich znaleźć no matter what. Wiedzą, że się wydostali i ich nie znajdzie.

Celina mówi Jankowi, że idzie z tym do dziadka. Bo to jest coś bardzo niepokojącego. I faktycznie, poszła.

Dziadek siedzi w swoim slumsowatym kącie i pali elektryczne cygaro. Janek idzie z Celiną. Dziadek uśmiechnął się na widok młodych... po czym marsowa chmura. Celina "nakrzyczysz na nas później". Wyciąga nośnik i pokazuje dziadkowi zdjęcie jego w mundurze. Dziadek... spochmurniał. "Skąd to masz?" "Znalazłam w tunelach". "To zdjęcie jest niebezpieczne...".

Dziadek próbuje wyciągnąć wszystko ale nie ukrywa, że się martwi. Celina próbuje wyciągnąć z dziadka jak najwięcej. Janek patrzy z dużymi oczami. Dziadek przyznał, że to zdjęcie jest zdjęciem z innej arkologii, dowodził pewnym statkiem kosmicznym. I w tej arkologii nie byłoby to przyjęte dobrze. CELINA MA GWIAZDKI W OCZACH. TEGO SIĘ NIE SPODZIEWAŁA. Dziadek widzi co palnął i zamknął się znowu. Celina chce podnieść mu "nie martw się dziadku, na pewno ten ktoś nas nie znajdzie". Dziadek się nastroszył.

Dziadek chce skonfrontować się z advancerem i wysłać dzieci do kolegi. Janek i Celina stanowczo protestują. Celina ze zdjęcia przeczytała nazwę statku na czapce "Erozja Ego". Casualowo wspomniała, że może sprawdzić nazwę statku z Prometeusem. Dziadek spochmurniał jeszcze bardziej, jeśli to możliwe.

Tr+3:

* X: Dziadek jest zdeterminowany, by dzieci zdjąć z linii ognia nieważne co się nie stanie
* V: Dziadek opowiedział, że dowodził jednostką "Erozja Ego". Statek obronny arkologii, wraz z rodzicami Celiny i Janka. Chronili arkologię o nazwie Aspiria.
    * Aspiria była chroniona przez kilka jednostek. Erozja była najsilniejszym ze statków.
    * Zanim doszło do zniszczenia Aspirii Dziadek został otruty. Erozja nie była w stanie walczyć.
* X: Laurencjusz przygotował "shock group" - chce tu wejść i zrobić rozróbę. Za krzywdę którą mu zrobiono.
* V: Dziadek wyjaśnił, że Erozja jest jednostką autonomiczną, jest sterowana przez bardzo zaawansowaną TAI o imieniu EMILIA.
    * Tylko on jako kapitan mógł powiedzieć kto ma prawo używania Emilii i Erozji. Póki nie odda dowodzenia, Erozja jest nieaktywna.
    * Dziadek został otruty przez swoją ZAŁOGĘ. To była zdrada. Dlatego Erozja jest nieaktywna.
    * Lojaliści próbowali mnie wyciągnąć. Wtedy Wasi rodzice zginęli. Z ręki swoich pobratymców. Wasi rodzice byli po mojej stronie.
    * --> ktoś próbuje go znaleźć by przekazać dowodzenie nad Emilią i Erozją. I może wykorzystać Was by mnie do tego zmusić.

Celina ze 100% pewnością "nie wie o nas". Dziadek westchnął jak stary człowiek którym jest. "Nikt nie może się dowiedzieć."

Celina "to co chcesz zrobić z tym advancerem?" Dziadek "jeszcze nie wiem". Janek jest w szoku. Pierwszy raz dziadek powiedział, że czegoś nie wie. Celina trochę też, ale nie pokaże po sobie. Dziadek "ale rozwiążę ten problem :-). Jesteście bezpiecz...". Celina "pomożemy Ci". Dziadek rzucił Mroczne Spojrzenie. Janek potwierdził.

I wtedy metalowa rura uderzyła w szybę. Laurencjusz. Dziadek nie wie co i jak - ale próbuje ochronić swoich i przed chwilą był w swojej przeszłości. Wyciąga broń i przygotowuje się do walki. Celina "nie mógł nas znaleźć!" i szybko biegnie sprawdzić na monitoringu (dziadek ma super zaawansowany monitoring, teraz Celina wie czemu).

Ex Z (ma kody itp) +3:

* V: Celina jakimś cudem dojrzała charakterystyczne smrodliwe ślady na Laurencjuszu. WIE, że to nie advancer a Laurencjusz i koledzy. Krzyknęła do dziadka "to Laurencjusz Kidiron, nie strzelaj!". A dziadek przygotowuje się do strzelania przez ŚCIANĘ. Ze swojej PROTEZY! Celina trąca go w rękę (mając szczękę gdzieś w okolicach podłogi)...

Dziadek strzelił. Ale spudłował. Laurencjusz i ekipa widząc strzał spierdolili! Po prostu SPIERDALAMY!

Tr Z (OMG OMG OMG SZCZELALI!!!) +4:

* Vz: Laurencjusz będzie UNIKAŁ Celiny i Janka. Nienawidzi ich ale nie chce mieć z nimi nic wspólnego. WTF co za rodzina.
* XX: Laurencjusz to zgłosi i dziadek będzie miał problemy. Strzelał do ludzi, może ma problemy z głową. Ogólnie, głośniej się zrobi.

Celina i Janek powiedzą wszystko dziadkowi.

EPILOG (prawdopodobny):

* Dziadek nie zajął się advancerem osobiście. Za stary. Zamiast tego wszedł w sojusz z Bartłomiejem Korkoranem. I oddał mu Emilię i Erozję. Żeby ta arkologia nie miała takich problemów jak Aspiria.
    * W zamian za to Celina i Janek są bezpieczni i są traktowani jak Korkoranowie przez Bartłomieja i cały klan Korkoranów.
    * I poza Celiną, Jankiem i Bartłomiejem nikt nie może nic wiedzieć.

## Streszczenie

Młodzi Lertysi - Celina i Janek - próbowali sobie dorobić polując na szczury w Nativis. Skonfliktowali się z Laurencjuszem Kidironem (i go sponiewierali skunksem), ale przez to musieli użyć wiedzy o kanałach serwisowych Nativis. To sprawiło że znaleźli ślady advancera polującego na ich dziadka. I to sprawiło, że poznali prawdę o swoim dziedzictwie - ich dziadek nie jest ich dziadkiem a oni pochodzą z martwej arkologii Aspiria.

## Progresja

* Celina Lertys: ogólna reputacja 'dziwnej' w Nativis; nikt nie chce z nią zadzierać. Wie, że pochodzi z Aspirii.
* Jan Lertys: ogólna reputacja 'dziwnego' w Nativis; nikt nie chce z nim zadzierać. Wie, że pochodzi z Aspirii.

### Frakcji

* .

## Zasługi

* Celina Lertys: przekonała Tymona do wzięcia szczurzego zlecenia jako słup, zastawiła pułapkę na Laurencjusza, przeszukała rzeczy niebezpiecznego advancera i wyciągnęła z dziadka info o przeszłości. Aha, uratowała wandali przed dziadkiem strzelającym przez ścianę. A, i jest aspirianką.
* Jan Lertys: brat Celiny; świetnie porusza się po tunelach arkologii i umie pozyskać różne rzeczy (np. spray skunksowy). Raczej słucha się siostry, bardzo związany z dziadkiem.
* Karol Lertys: 73-letni "dziadek" Celiny i Jana; kiedyś kapitan aspiriańskiego statku OA Erozja Ego. Gdy został zdradzony i otruty, lojaliści go wydobyli ze statku i został "dziadkiem" gdy prawdziwi rodzice zginęli. Potem został inżynierem Nativis i się ukrywa. Nadal jest jedynym zdolnym do aktywacji Erozji Ego.
* Tymon Korkoran: lekkoduch rozdarty między wujem Bartłomiejem i Laurencjuszem Kidironem (15); został słupem dla Celiny odnośnie polowania na szczury, bo chce zaimponować swojej eks-dziewczynie że robi coś przydatnego (i zarobić za nic).
* Laurencjusz Kidiron: mściwy młody (16) Kidiron próbujący wygrać "eksterminację szczurów" z Lertysami a potem zwandalizować slumsowy dom Lertysów. Zmienił zdanie po tym jak został OSTRZELANY.
* Emilia d'Erozja: superzaawansowany android p.o. TAI jednostki OA Erozja Ego. Lojalna Karolowi Lertysowi, oddana docelowo Bartłomiejowi Korkoranowi. Nieobecna.
* OA Erozja Ego: kiedyś główny okręt defensywny arkologii Aspiria dowodzony przez Karola Lertysa. Teraz nieaktywny, GDZIEŚ. Sterowany przez androida, Emilia d'Erozja. Nieobecny.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Nativis
                        1. Poziom 1 - Dolny
                            1. Zachód
                                1. Stare TechBunkry: jeden z nich został objęty przez advancera polującego na Karola Lertysa. Zostało to przypadkowo odkryte przez Janka i Celinę.
                        1. Poziom 2 - Niższy Środkowy
                            1. Wschód
                                1. Centrum Kultury i Rozrywki
                                    1. Bar Śrubka z Masła: da się tam tańczyć z holodziewczynami; ulubione miejsce zabawy młodzieży z Nativis.


## Czas

* Opóźnienie: -2091
* Dni: 9

## Konflikty

* 1 - Celina przekonuje Tymona, by on wziął zlecenie usuwanie szczurów i za nicnierobienie dostanie 5% i chwałę swojej eks-dziewczyny.
    * TrZ+4
    * VVXVz: Tymon zgodzi się na plan, zgodzi się na 5% za nic, osłoni przed dziadkiem i dostanie całuska w policzek
* 2 - Celina naszkicowała plan, jak najsensowniej rozmieścić trutkę na szczury itp by zarobić jak najwięcej
    * Tr Z (Prometeus + dwie osoby + plan Celiny) +2
    * XzX: jest konkurencja, konfrontacja z Laurencjuszem
* 3 - Janek poszedł dalej szukać po swoich innych próbkach - i tam napotkał zbirów Laurencjusza. Zwiał i zgubił.
    * Tr+3
    * VXV: Dostał ze dwa ciosy w twarz, ale zwiał i zgubił
* 4 - Janek zdobywa dla Celiny spray skunksowy - rzygosmród przylegający do skóry. Janek umie się poruszać po podłych terenach i mimo zapominalstwa jest świetny w orientacji w terenie. Więc on wysłany.
    * TrZ+3
    * VVV: Janek pozyskał spray, Celina zastawia pułapkę, nikt nie ma dowodów na nic
* 5 - Efekt pułapki Celiny
    * TrZ+4
    * XVVz: Laurencjusz chce się mścić, ale dla reszty 'fair game'.
* 6 - Po tym, jak Zespół odstraszył rywali, Celina i Janek przyspieszyli z rozkładaniem tego cholerstwa. POWRÓT DO PULI.
    * Tr Z (Prometeus + dwie osoby + plan Celiny) +2 -2X (bo kontynuacja)
    * Xz: Mimo starań i dobrego rozkładania po prostu nie nadążają. Szkody, które im wyrządzono są za duże. Cały dzień się starali i zamiast zarobić, stracili.
* 7 - Następnego dnia Janek i Celina zamiast iść do szkoły stwierdzili, że wykorzystają znajomość tuneli i arkologii. Przekraść się do tych tuneli mimo, że powinni być w szkole.
    * TrZ (nie pierwszy raz) +3
    * XVz: inny tunel, stracili godzinę; ale dali radę.
* 8 - Zaczęli więc rozkładać trutki na szczury w specjalnych tunelach, by tymi tunelami pokryć wszystko.
    * TrZ (znajomość tuneli + Prometeus + plan) +4
    * VVXzV: dorobili sobie solidnie, Tymon zadowolony, ale pouszkadzały im się ubrania i ciężka droga i praca
    * VV: znaleźli drogę do techbunkra gdzie jest dziwny advancer polujący na ich dziadka; udało im się PERFEKCYJNIE wydostać
* 9 - Celina wyciąga od dziadka informacje o przeszłości używając zdjęcia i "mogę to sprawdzić" (strachu dziadka że to wyjdzie)
    * Tr+3
    * XVXV: dziadek zdejmie dzieci z linii ognia, opowiedział o Erozji Ego i zdradzie, pojawi się Laurencjusz wandalizować.
* 10 - Celina zatrzymuje dziadka przed przypadkowym zastrzeleniem Laurencjusza (którego pomylił z wrogim advancerem)
    * Ex Z (ma kody monitoringu itp) +3
    * V: Celina zauważyła, że to Laurencjusz; zatrzymuje dziadka m.in. odpychając broń.
* 11 - Dziadek strzelił. Ale spudłował. Laurencjusz i ekipa widząc strzał spierdolili! Po prostu SPIERDALAMY!
    * Tr Z (OMG OMG OMG SZCZELALI!!!) +4
    * VzXX: Laurencjusz nienawidzi Celiny i Janka, ale ich unika. Dziadek ma problemy, strzelał do ludzi itp.

## Kto
### Arkologia Nativis

* Celina Lertys, młoda objęta przez Kić
    * Ocean: ENCAO:  --+-0
        * Stanowczy i silny, zdecydowany
        * Skryty; zachowujący swoją prywatność dla siebie
        * Nigdy nie wiadomo, co jest prawdą a co maską
        * Praworządny, uznaje prawo za najwyższą wartość
    * Wartości:
        * TAK: Universalism, TAK: Tradition, TAK: Security, NIE: Power, NIE: Humility
    * Silnik:
        * TAK: Harmonia naturalna: balans między technologią i naturą, utrzymanie cudów natury. Lasy Amazonii.
        * NIE: Awans za wszelką cenę: albo lepsza firma albo wojna o stanowisko z innymi. To stanowisko będzie moje.
    * Rola: science / bio officer + medical. Młoda podkochująca się w Eustachym :D. Złota, płonąca skóra z błękitnymi włosami, może robić wyładowania
* Jan Lertys
    * Ocean: ENCAO: -0-0+
        * Zapominalski; non stop trzeba coś przypomnieć
        * Obserwator; raczej stoi z boku i patrzy niż działa bezpośrednio
        * Łatwo wywrzeć na tej osobie wrażenie
    * Wartości:
        * TAK: Humility, TAK: Universalism, TAK: Self-direction, NIE: Achievement, NIE: Security
    * Silnik:
        * TAK: Pokój: Kapuleti i Monteki MUSZĄ żyć w pokoju. Unikajmy starć, idźmy w pacyfizm. Wygaszanie konfliktów, harmonia.
        * TAK: Ratunek przed chorą miłością: TYLKO NIE Celina -> Eustachy. Pls.
    * Rola: First Mate (siłowy / ładownia); chce zejść do bazy bo tu pracuje jego ukochana Daria

* Tymon Korkoran
    * Ocean: ENCAO:  +--00
        * Jeżeli czegoś chce, weźmie to dla siebie; zazdrosny i zawistny
        * Niecierpliwy, chce TERAZ i nie będzie czekać
        * Egzaltowany, w przejaskrawiony sposób okazuje uczucia
    * Wartości:
        * TAK: Universalism, TAK: Stimulation, TAK: Tradition, NIE: Face, NIE: Humility
    * Silnik:
        * TAK: Być jak idol: iść w ślady swojego Idola i Mentora. Stać się tym czym idol się stał.
        * TAK: Wrócić do ukochanej: byłem głupi i ją straciłem. Zrobię wszystko, by móc do niej wrócić.
* Marcel Kidiron, główny agent ds. odszczurzania i pomniejszy administrator
    * Ocean: ENCAO:  ---+0
        * Nie kłania się nikomu, z podniesioną głową
        * Powściągliwy, skryty i wyważony
        * Nadużywa różnych używek (narkotyki, alkohol)
        * Pacyfistyczny, pokojowo nastawiony
    * Wartości:
        * TAK: Tradition, TAK: Humility, TAK: Conformity, NIE: Stimulation, NIE: Security
    * Silnik:
        * TAK: Złamać kogoś, zdominować: pokazać komuś że to JA naprawdę mam władzę i beze mnie tamta osoba nie ma niczego i nic nie znaczy.
        * TAK: Złota klatka Lauriego: jest mi bardzo dobrze, ale pragnę wolności! Chcę latać wolno! Skonfliktowanie między światami.
        * NIE: Być jak idol: iść w ślady swojego Idola i Mentora. Stać się tym czym idol się stał.
* Karol Lertys
    * Ocean: ENCAO:  -00-+
        * Zrzędliwy i kłótliwy
        * Skryty; zachowujący swoją prywatność dla siebie
        * Chętny do eksperymentowania i testowania nowych rzeczy
    * Wartości:
        * TAK: Achievement, TAK: Family, TAK: Security, NIE: Stimulation, NIE: Self-direction
    * Silnik: (wszystko by tylko pomóc swoim wnukom; KIEDYŚ przemytnik potem inżynier arkologii i zna tajne skrytki)
        * TAK: Herold Baltazara: zwiększać wpływ i moc kogoś innego, powiększać ich wpływy.
        * NIE: Do the right thing: niezależnie od tego co jest wygodne czy tanie, własne zasady i własny kompas moralny są najważniejsze. By spojrzeć w lustro.
* Laurencjusz Kidiron
    * Ocean: ENCAO:  +0---
        * Nie planuje długoterminowo, żyje chwilą
        * Nudny. Nie ma nic ciekawego do powiedzenia i niezbyt się czymkolwiek interesuje.
    * Wartości:
        * TAK: Face, TAK: Power, TAK: Hedonism, NIE: Achievement
    * Silnik:
        * TAK: Przejąć władzę nad ogranizacją: ten ród / ta firma będą MOJE. Podbiję je, zmanipuluję lub doprowadzę do tego pokojowo.

* Karmelian Farczok, agent Erozji
    * Ocean: ENCAO:  +-0-0
        * Doskonale radzi sobie pod wpływem silnego stresu
        * Opryskliwy, bezceremonialny i obcesowy
        * Z poczuciem humoru, lubi żartować
    * Wartości:
        * TAK: Face, TAK: Security, TAK: Power, NIE: Universalism
    * Silnik:
        * TAK: Pozycja w Grupie: zbudować swoją pozycję i autorytet w swojej rodzinie / klanie. Podnieść SWOJĄ pozycję w SWOJEJ grupie.
        * TAK: Godslayer: pokonać bestię / zniszczyć anomalię zdecydowanie przekraczającą jakiekolwiek możliwości. Pokonać niepokonane. (pokonać )
        * NIE: Niezłomna Forteca: A man of dignity and virtue. Nikt nie zmusi mnie do powiedzenia kłamstwa. Mogę stać sam przeciwko wszystkim. Incorruptible. I shall not fall.


### Infernia

* Bartłomiej Korkoran, wuj i twarda łapka rządząca Infernią.
    * Ocean: ENCAO:  +-000
        * Bezkompromisowy, nieustępliwy, niemożliwy do zatrzymania
        * Ciekawski, wszystko chce wiedzieć i wszędzie wsadzić nos
    * Wartości:
        * TAK: Family, TAK: Self-direction, TAK: Achievement, NIE: Tradition, NIE: Humility
    * Silnik:
        * TAK: Inkwizytor: niszczenie fake news, publiczne pokazywanie hipokryzji, ujawnianie bolesnych prawd
        * TAK: Kolekcjoner: kolekcjonuje coś rzadkiego ()
    * Rola: wuj, kapitan Inferni
* Emilia d'Infernia
    * "żona" Bartłomieja. Żywy arsenał. Egzekutor Bartłomieja. (Emilia -> "Egzekucja MILItarnA")

## Układ arkologii Nativis

* Aspekty:
    * Bardzo dużo roślin, drzew itp. Piękne roślinne miejsce.
    * Bardzo spokojna arkologia, silnie stechnicyzowana i sprawna.
    * Dominanta: faeril > drakolici, ale próba balansowania. Niskie stężenie AI.
    * BIA jako centralna jednostka dowodząca, imieniem "Prometeus"
* Struktura:
    * Wielkość: 4.74 km kwadratowych, trzy poziomy
    * Populacja: 11900 (Stroszek)
        * Operations & Administration: 1100
        * Extraction & Manufacturing: 3213
        * Medical: 355
        * Science: 360
        * Maintenance: 700
        * Leisure: 1000
        * Social Services: 240
        * Transportation: 800
        * Training: 229
        * Life Support: 500
        * Computer & TAI & Communications: 500
        * Engineering: 500
        * Supply / Food: 1200
        * Security: 300
    * Bogactwo: wysokie. Technologia + piękna roślinność.
    * Eksport: żywność (przede wszystkim), komponenty konstrukcyjne, hightech, terraformacja
    * Import: luxuries, prime materials, water, terraforming devices, weapons
    * Manufacturing: jedzenie, konstrukcja, terraformacja
    * Kultura: 
        * dominanta: piękno, duma z tego co osiągnęli, duma z tego że nie ma wojny domowej faeril/drakolici
        * wyjątek: brak.
    * Przestrzenie
        * rozrywki: parki, natura, zdrowe ciało - zdrowy duch
        * inne: hotele, laboratoria terraformacyjne, restauracje...
    * Zasilanie
        * solar + microfusion
    * Żywność
        * różnorodna; to miejsce jest źródłem żywności
        * przede wszystkim roślinna, ale też zawiera elementy zwierzęce (te najczęściej z importu)
    * Life support => terraformacja + filtracja
    * Comms => biomechaniczne okablowanie + normalne sieci
    * TAI => BIA wysokiego stopnia imieniem Prometeus
    * Starport => large
    * Infrastructure => high-tech, dużo półautonomicznych TAI, wszystko nadrzędne przez Prometeusa. Świetny transport publiczny. 
    * Access & Security => arkologia z silnymi blast walls, silnymi liniami dostępowymi.

Historia: 

* w trakcie wojny domowej sprzedawała żywność na 2 fronty co spowodowało znaczne bogactwo ale też częściową niechęć innych arkologii
* obecnie prowadzi ekspansje na struktury zrujnowane/opuszczone po wojnie żeby całkowicie się uniezależnić od dostaw z poza planety
