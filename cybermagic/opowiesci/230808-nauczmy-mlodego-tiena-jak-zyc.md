## Metadane

* title: "Nauczmy młodego tiena jak żyć"
* threads: brak
* motives: magowie-bawia-sie-ludzmi, potwor-scooby-doo, wychowywanie-dzieciaka, subtelna-ingerencja-maga, symulowana-sytuacja, teren-gdzie-rzadzi-arystokracja, casual-cruelty
* gm: żółw
* players: kić, anadia, kamilinux

## Kontynuacja
### Kampanijna

* [230715 - Amanda konsoliduje Złoty Cień](230715-amanda-konsoliduje-zloty-cien)
* [230711 - Zablokowana sentisieć w krainie makaronu](230711-zablokowana-sentisiec-w-krainie-makaronu)

### Chronologiczna

* [230715 - Amanda konsoliduje Złoty Cień](230715-amanda-konsoliduje-zloty-cien)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ?
* Struktura: 
    * ?
* CEL: 
    * MG
    * Drużynowy

### Co się stało i co wiemy

* Klan Samszarów Milterius, do którego należy Elena i Karolinus, ma młodzika który zachowuje się niegodnie jak na tiena.
    * Armin
        * 'wszystkie laski jego', 'mogę działać bez niczyjej pomocy', 'jestem tienem wszystko mogę'
        * --> niech oczytana Elena wymyśli sposób, jak naprawić młodego
    * Florian
        * chce pomóc Karolinusowi i Elenie. Niech będą wartościowymi tienami.
        * chce ich też wsocjalizować

### Co się stanie (what will happen)

* F1: .
* F2: .

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Armin Samszar
    * OCEAN: (E+N+C-): głośny i zapalczywy, przyciąga uwagę wszystkich rozkazującymi gestami. Wszystko czego pragnie ma być jego. "Kto nie żąda, nie ma dość sił by móc żądać."
    * VALS: (Face, Hedonism): skupia się na tym by traktowano go jak księcia i udawaniu dobrotliwego. "Jeśli czegoś pragnę, dostanę to, dobry człowieku. Spraw, bym pragnął Ci pomóc."
    * Core Wound - Lie: "Myśleli, że nie jestem tienem to mnie odrzucili" - "Zasługuję na to, aby być w centrum uwagi! Jak mi nie dadzą, wezmę sam!"
    * styl: extremely confident and prideful, acting in a dignified manner and constantly giving the impression that he looks down upon others.
* Wiktor Blakenbauer
    * OCEAN: (C+O-): cierpliwy z charakterem lodowca, monotematyczny i monomaniczny, "Trochę cierpliwości i po mojemu będzie."
    * VALS: (Humility, Universalism): nikt nie jest specjalny, wszyscy podlegają tym samym regułom, "Jak jest, tak było i będzie. Kolejna iteracja tego samego cyklu."
    * Core Wound - Lie: "monomaniczny Blakenbauer alchemik" - "wzmocnię alchemię i udowodnię wszystkim, że alchemią osiągniesz wszystko"
    * styl: beznamiętny alchemik, "_I make elixirs. That's what I do. When I do this elixir I will make another one._"

### Scena Zero - impl

.

### Sesja Właściwa - impl

Elena należy do klanu Samszarów Milterus. Ma starszego kuzyna, Floriana. Florian się rządzi, ale zależy mu na rodzinie. Florian poprosił Elenę na rozmowę.

* F: Eleno, dobrze widzieć Cię w dobrym zdrowiu
* E: Ciebie również, Florianie
* F: Podobno zrobiłaś coś... niewłaściwego w krainie makaronu? 
* E: E... nie wiem co masz na myśli, Florianie. Możesz być bardziej precyzyjny?
* F: (wymienili się spojrzeniami z Eleną)
* F: (westchnięcie) Eleno, Ty i Karolinus... dobrze. Mam prośbę. Od rodu.
* F: Młody Armin (z klanu). Ma... wymaga wychowania.
* E: I ja mam go wychować?
* F: Tak. Armin ma tendencje do robienia wszystkiego samemu, otaczania się młodymi dziewczynami i nadużywania władzy. Chcemy, by Armin był bardziej... proklanowy, by zobaczył wartość u ludzi i powściągnął swoje instynkty.
* E: Ok...
* F: Armin przyjedzie do Triticatusa. Ty będziesz w pobliżu. On nie wie że tam jesteś. Zrób coś, by Armin robił to co powinien. Naucz go.
* E: Wychowanie ale nie wprost. Nie niańka?
* F: Nie, masz go nauczyć dlaczego działanie samemu, w oderwaniu, nadużywanie dziewczyn itp. jest szkodliwe.
* E: Co ja z tego będę miała?
* F: Eleno. Jesteś tienką. To nie jest pytanie 'co masz z tego mieć'. To Twój obowiązek.
* E: Źle się wyraziłam. Czy w związku z tym obowiązkiem mogę liczyć na wsparcie Twoje i rodu przy założeniu mojej biblioteki?
* F: Oczywiście. W chwili, w której wykażesz wartość. Eleno, działasz sama. Nie dbasz o innych. Czemu ktoś miałby Ci pomóc? Komu pomogłaś? Komu na Tobie zależy? Kto by płakał, gdybyś zginęła?
* F: Eleno, jeśli się przyłożysz, znajdziesz miejsce, UDOWODNISZ że Ci zależy, włożysz energię, to Ci pomogę. Ale musisz COŚ zrobić.
* E: Robię więcej niż kuzyn Armin!
* F: Oczywiście, dlatego TY MASZ GO NAUCZYĆ JAK ŻYĆ. Bo jesteś lepsza. To nie to że nic nie robisz. Ale nie spełniasz swojego potencjału.
* E: Mogę liczyć na jakieś wsparcie?
* F: Tak, dam Ci wsparcie jak potrzebujesz. Ukryjemy Cię w sentisieci, i Twoje zaklęcia też.

Elena -> Amanda, że trzeba wychować młodego. Amanda zauważyła, że dzieci jej nie interesują, ale jeśli Elena potrzebuje... 

Triticatus. ZANIM pojawił się młody Armin. Elena tłumaczy Amandzie plan:

* zorganizować panienki
* Armin poluje na panienki
* 4 smutnych panów, łapią Armina "TY JĄ ZABIŁEŚ!" i żeby wszystkie członki mu zostały ale by się nauczył pokory i Elena wpada i go ratuje

CEL TO:

* Twój klan Ci pomoże
    * REALIZOWANE PRZEZ: Elena wpada i ratuje
* ludzie mają wartość i nie można ich lekceważyć ani nadużywać
    * REALIZOWANE PRZEZ: zainteresował się dziewczyną, przespał się z nią i bracia go potraktowali źle a ona była na dragach
* nie wszystko rozwiążesz samemu
    * REALIZOWANE PRZEZ: była na dragach i potrzebuje pomocy i bracia szukają a sentisieć nie zadziałała bo był naćpany

Zdaniem Amandy - czemu chłopak miałby kogoś poprosić o pomoc? Najpewniej oleje laskę - to tylko człowiek. ALE! Zależy mu na jego twarzy, chce być podziwianym tienem. Jest "SZLACHETNYM KSIĘCIEM" i żąda szacunku. Koleś nie jest fundamentalnie zły, ale jest leniwy i nie szanuje ludzi. Chce się bawić i zależy mu na dobrej zabawie, reputacji itp. Elena patrzy na Amandę, która patrzy na Elenę obojętnie.

Elena ma nowy plan - zamiast mnie ratującej niech uratują go ludzie. Ludzie dobrzy, ludzie źli. To bardziej pomoże.

FINALNY PLAN:

1. Amanda pozyskuje od Bilgemenera ładną dziewczynę do podłożenia Arminowi.
2. Elena przygotowuje POTWORA i labirynt z potworem. Coś co jest adekwatnie niebezpieczne. Zainspirowane makaronem. Coś co wygląda jakby chłopak namieszał.
    * Drobinki papieru powciskać w makaron. Niech makaron rządzi. MAKARONOWY POTWÓR
3. Amanda pozyskuje odpowiednie dragi. Dama od Bilgemenera podaje dragi dyskretnie Arminowi. Chodzi o to, by rozwalić mu percepcję magii i uwiarygodnić że sentisieć nie działa.
4. Laska podpuszcza Armina, by on poszedł rozwalić stwora (niegroźny makaronostwór). Armin rzuca zaklęcie, ma 'Paradoks' i się pojawia MEGAMAKARONOTWÓR pod kontrolą Eleny. Armin uważa, że Z JEGO WINY potwór tam jest.
5. Potwór robi wpierdol Arminowi. Armin i laska uciekają. Więc teraz musi to na serio naprawić.

.

* A -> Bilgemener: potrzebuję rozwiązać tien Samszar rozwiązać problem.
* B: czego tien Samszar potrzebuje.
* A: wystrzałowa laska zdolna do manipulowania nastolatkiem zgodnie z zadanymi parametrami
* B: brunetka czy blondynka?
* A: młoda, ładna, wygląda na naiwną i wszystko łyka.
* B: oczywiście. Nadio, załatw. Na jutro.

Elena robi labirynt z potworem. Wybrała nieużywane magazyny w północnej części poza Triticatusem. I tam Elena używa magii i sentisieci by sformować odpowiednio podłego i potężnego MAKARONOPOTWORA. Ma być wystarczająco przekonywujący by chłopak uwierzył że to on spowodował problem. I problem - sam nie da rady. (+Elena ma czas i sentisieć +Elena ma substraty i wie o chłopaku)

Tr M +3 +3Ob:

* V (p) Vr (m): POTWÓR ISTNIEJE I JEST GROŹNY
* X (p): POTWÓR będzie wolny, uwolniony spod kontroli Eleny; trzeba go uczciwie pokonać
* Vm: Petra ani nikt nie jest w stanie dowiedzieć się o pochodzeniu potwora

Elena łączy się z Wiktorem po hipernecie.

* W: czego? /beznamiętnie
* E: Wiktorze, mam sprawę, nie będę ściemniać, chodzi o zabawę
* W: potrzebujesz kupić trucizny? Narkotyków?
* E: potrzebuję wpuścić kogoś na Twój teren, kogoś kto się nauczy czym jest prawdziwe zagrożenie. Ale nie żeby się coś stało a żeby go uratował człowiek
* W: to nie dotyczy eliksirów, nie jestem zainteresowany
* E: ależ to dotyczy eliksirów
* W: on będzie je kupował?
* E: on nie będzie nic wiedział, ale jest ktoś kto może je kupować i może też dawać Ci nowe składniki, dostęp do rynku zbytu
* W: (zamrugał żabimi oczami powoli) czyli ktoś CHCE kupić eliksiry?
* E: Tak, ktoś chce kupić eliksiry
* W: dobrze, ile i jakie?
* E: a ile masz na magazynie?
* W: (uśmiechnął) zależy czego
* E: wszystkiego
* W: mam syntezatory, mam inne narzędzia... kilkaset litrów, DROGICH.
* E: chodzi mi o długoplanową transakcję
* W: ktoś, kto kupuje eliksiry i je dystrybuuje?
* E: mam kogoś ze Złotego Cienia. Skontaktuję Was, jeśli możesz być dla nich przydatny. To nie jest jednorazowa akcja. 

.

Tr Z +4:

* V (p): warto dać jej szansę
* V (m): pójdzie w roli partnera
* V (e): pójdzie w roli PETENTA; z punktu widzenia Wiktora się mega opłaca współpracować z mafią i to jest dla niego okazja, Wiktor się musi wykazać
* V (e1): Wiktor jest zainteresowany byciem wsparciem dla Eleny, ona jest znacząca i nie tylko w kontekście mafii

Wasz sukces polega na tym:

* Młody tien (Armin) będzie mieć określone obserwacje
    * Twój klan Ci pomoże
    * ludzie mają wartość i nie można ich lekceważyć ani nadużywać
    * nie wszystko rozwiążesz samemu
* Wy pozostajecie nieodkryci. "Nikt" (Armin) nie wie, że Wy w tym uczestniczyliście

Zasoby:

* Jesteście ukrytymi w sentisieci tienami na terenie. Plus macie Amandę.
* Amanda sprowadziła młodą ładną dziewczynę ze Złotego Cienia która będzie współpracować. Ma Pomniejsze Skille Mafijne.
* Elena pozyskała wsparcie Wiktora Blakenbauera, handlując kontaktem z mafią. Wiktor jest tienem Blakenbauerów, alchemikiem, ma płaszczki. I chce się wykazać. Przed Amandą.
    * Wiktor jest zainteresowany współpracą z Eleną
* Elena sformowała MAKARONOWEGO POTWORA. Nad którym zaraz straciła kontrolę. 

Karolinus robi to co zwykle (fraternizuje się z plebsem w knajpie i ma dwie panienki na ramionach). Wchodzi kuzyn Florian.

* F: Karolinusie, dobrze widzieć Cię w dobrym zdrowiu.
* K: Zamieniam się w słuch
* F: Kuzyn Armin. Wymaga wychowania. Wysłałem Elenę. Wychowuje go. (nie dopowiedział, ale SPOJRZAŁ na Karolinusa znacząco)
* K: Ok.
* F: (czeka chwilę) 
* K: (też czeka)
* F: Armin ma tendencje do robienia wszystkiego samemu, otaczania się młodymi dziewczynami i nadużywania władzy. Widzisz... pewien wzór?
* K: (patrzy na swoje panienki) (patrzy na siebie). No... tak. Co rodzina, to rodzina.
* F: Dokładnie. Dlatego może gdy poprosiłem Elenę, nie był to mój najlepszy pomysł. Pomożesz jej? (insynuuje, że jest podobieństo między tym opisem i Eleną)
* K: (wzruszam ramionami). Czego ode mnie oczekujesz? Czy przyszedłeś się napić i pożalić
* F: Nie, prawdziwy tien się nie żali i nie zapija smutków. Przyszedłem poprosić Cię, byś pomógł Elenie w wychowaniu Armina. I zaopiekował się Eleną.
* F: Może się nie domyślasz, ale to moja próba głębszej... integracji Eleny z... (myśli chwilę) kompetentnym, wrażliwym i skutecznym tienem. Z naszego rodu.
* K: (mówi o mnie? XD) Mówisz o mnie?
* F: Wszyscy inni są zajęci.
* K: (tak jak ja...) Gdzie oni są?
* F: Triticatus. Miasto makaronu. Elena już tam coś robi.
* K: Wiem. Byłem tam. Kiedyś. No dobra... coś w zamian?
* F: Kuzynie. Czasami ród potrzebuje ochotnika. Będziesz tym ochotnikiem. 
* K: Wisisz mi przysługę?
* F: Jesteś tienem. To nie jest pytanie 'co masz z tego mieć'. To Twój obowiązek. (śmiertelnie poważnie)
* K: A nie rodziców?
* F: Rodzice Armina nie są magami. Nie mają mocy magicznej.
* F: Karolinusie. Elena zadała dokładnie to samo pytanie.
* K: Co rodzina to rodzina.
* F: Dostaniesz wsparcie jakiego potrzebujesz. Po prostu POPROŚ.
* K: Chciałbym coś w stylu Strzały.
* F: (patrzy pusto) Ostatnią super-jednostkę ZGUBIŁEŚ.
* K: Zmeiniła właściciela. Gdzieś w warsztacie.
* K: To koszty, tylko kto nic nie robi nie popełnia błędów. Koszty operacyjne.
* F: (ciężkie westchnięcie). Karolinusie, masz te koszty, bo działasz z natury sam. Nie dbasz o innych.
* K: Nie sam. Z Elena. A wcześniej inne dziewczyny.
* F: Czemu ktoś miałby Ci pomóc? Komu pomogłeś? Komu na Tobie zależy? Kto by płakał, gdybyś zginął? Mi na Tobie zależy.
* K: (wymienia akcje dla Samszarów - Maja itp., powołuje się na ojca Mai)
* F: ...
* F: Dam Ci wsparcie jakiego potrzebujesz. Ukryjemy Cię w sentisieci, i Twoje zaklęcia też. Wychowajcie Armina i... po prostu go wychowajcie. I pamiętajcie - ród Wam pomoże.
* K: Ok.

(Florian ma powoli dość. TEŻ dostał zadanie socjalizacji Karolinusa i Eleny. Ma dość.)

Niedługo, Eleno. 

* F -> E: Nie potrzebowałaś wsparcia?
* E: Dobrze rozgrywam wszystko strategicznie.
* F: Czyli NIC nie robisz?
* E: Korzystam z zasobów jakie mamy na miejscu. Florianie, mamy akcję przygotowaną. Jest jakiś problem. Chcesz wziąć udział?
* F: Nie. Wysyłam Ci Karolinusa.
* E: Dobrze. Są dziewczyny jakimi może się zająć.
* F: Eleno. 
* E: Tak Florianie?
* F: (mina rozczarowania).
* E: Nie ma co się martwić, Armin zostanie wychowany.
* E: Florianie, w Twoim wieku nie należy się stresować. Im później w wieku, tym gorzej ze zdrowiem. Damy radę.
* F: Dobrze, tylko... żeby nie zginął, dobrze?!
* E: A muszą mu zostać wszystkie kończyny?
* F: TAK!!!
* E: Taki żarcik, haha.
* F: ... (rozerwał połączenie)

Karolinusie. Ty, Elena i Amanda jesteście w lesie. Macie epicki szałas. Takiego szałasu epickiego... przenośny elegancki domek. Myśl techniki Samszarów. A Armin będzie jutro lub pojutrze. Jest z nimi Kleopatra - bardzo pasująca młoda dama. Taka, by chciał się wykazać. Patrząc na Karolinusa i Elenę, czeka. Karolinus zasugerował Elenie, by zrobiła kolację Kleopatrze. Karolinus chce "położyć Kleopatrę do snu". Karolinus jest zmęczony. Elena daje Karolinusowi w łeb.

* E: Nie musisz wypoczywać z Kleopatrą
* K: Mamy jedno posłanie
* A: (patrzy znudzonymi oczami na scenę)
* E: (Po dzisiejszej nocy zrobię z Amandą dodatkowy deal)

Elena pokazuje gdzie jej pokój, gdzie jest jedzenie i idzie spać. Kleopatra patrzy z lekkim niepokojem na Amandę. Elena (idź spać sama), Karolinus (idź spać ze mną). Amanda patrzy na Karolinusa.

* A: To nie on Cię interesuje. Chyba że tak, Twój wybór.
* Kl: (robi słodką minkę) (efektownie mdleje)
* K: Elena, przez Ciebie bo zamieszałaś. Zanieś ją do pokoju!
* E: (wychodzi, patrzy, przykrywa kocykiem Kleopatrę, mówi 'dobrych snów' i wraca)
* E -> A: Może robić co chce. Nie musi iść do łóżka. Celem misja wychowywania Armina i nie spierdolić. Reszta, róbcie co chcecie. Wiesz, że mamy INNE ważne cele (handel narkotykami)
* A: Oczywiście.
* E -> K: Tam masz bar. Nie ruszaj jej.
* K: Ok. (podnoszę ręce do góry). Zanieś ją do łóżka.
* E: Idź do pokoju, przelewituję
* K: (idzie do łóżka i włąćza hologramy)
* E -> Kl: Możesz przelewitować do sypialni
* Kl: (taktycznie udaje że jest nieprzytomna)
* A: (siedzi i czeka i patrzy)
* Kl: (obudziła się po 10 min)
* A: Jednego chłopaczka trzeba wychować, masz go dobrze podpuścić. Żaden z tej dwójki. Jest magiem. (daje plan) I będzie potwór, ale potwór jest udawany.
* Kl: Rozumiem i jestem posłuszna.
* A: Jak wszystko dobrze pójdzie, jeden mag choć odrobinę dobrze będzie traktował ludzi
* Kl: Mam to szczęście, że magowie w tych okolicach dobrze traktują ludzi

PLAN:

* (1): Kleopatra podaje Arminowi dragi. Macie te narkotyki. 
* (2): Kleopatra podpuszcza Armina, by on poszedł rozwalić stwora. Na dragach ma mieć "Paradoks".
* (3): Starcie z makaronotworem. Armin ma przegrać i uciekać. Na teren Blakenbauerów.               <-- niezdefiniowany ale zadziała
    * ma uciec na teren Blakenbaerów
* (4): Tam przechwycą go potwory Wiktora                            <-- ok
* (5): Uratuje go człowiek i odprowadzi na teren Samszarów          <-- ok
    * Wiktor podstawi człowieka, starego blaken-trapera
* (6): Potwór którego trzeba pokonać, więc poprosi o pomoc          <-- słaby punkt ale może zadziałać
    * Elena lub Karolinus?
    * bo plotki o strasznym potworze i plama na honorze 
    * żeby poprosił Was o pomoc:
        * SKĄD WIE że jesteście -> (właśnie przyjechali na rocznicę jakiejś akcji)
        * CZEMU WY -> (bo są)
    * ryzyko: laski zobaczą że spieprzy; sztuczny dziennikarz Paktu

Przybywa Armin. Wy widzicie wszystko na sentisieci, Armin Was nie widzi. Armin ma cztery strażniczki, śliczniuteńkie. Ale dobrze uzbrojone. I jednego starego kaprala. Ten kapral wygląda jak dupa psa, ale jest w eleganckim mundurze i patrzy kaprawymi oczami. Armin wyraźnie się go nie słucha. Kapral mówi Arminowi jak ma być. Armin "kapralu Maskiewnik, jesteś tylko kapralem. JA jestem TIENEM.". Maskiewnik: "tien Samszar. Jesteś żywym tienem. I tak pozostanie." Kapral wygląda, jakby był tu za karę.

Armin wychodzi na plac, dziewczyny rozkładają mu scenę, Kapral facepalmuje. Armin przemawia do "tłuszczy"

* A: Tien Armin Samszar zaszczycił Waszą niewielką osadę swoją obecnością.
* (ogólne grzeczne oklaski)
* A: Zabawiajcie mnie, proszę.
* (z tłumu wychodzą jacyś PRowcy)

Operacja "KLEOPATRA". Cel - podać Arminowi narkotyki.

* Kl -> A: pani kapral, wszystkie parametry bez zmian?
* A -> K, E: Trzeba sobie poradzić ze świtą. Pozbyć się ich.

Karolinus dał Amandzie SIGIL Samszara. Operacja wewnętrzna itp. Barman ma być tym co poda narkotyki Kleopatrze oraz Arminowi. Kleopatrze nie trzeba, ale mniej failmodów.

Amanda, dyskretnie, do barmana. Pokazuje sigil od Samszara. "Pojawi się Armin z taką damą. Podaj im TO. To zlecenie od Samszara" Barman ma minę nieszczęśnika. On nie chce być między dwoma Samszarami.

* Barman: "Ale agentko, on też jest Samszarem."
* Amanda: "On (zimne spojrzenie) jest tym SŁABYM Samszarem. On jest Samszarem, który nie ma właściwych znajomości. On jest tym Samszarem, który nie może dostać wsparcia Złotego Cienia na każde żądanie."
* Barman: (przełknął ślinę) "Agentko, mam rodzinę... nie chcę stawać między dwóch tienów"
* Amanda: (łapie go martwymi oczami z czasów wojny) "Masz rodzinę i nie chcesz stawać przeciwko tej dwójce."

Tp +3:

* X: Barman jest przerażony i kapral będzie w stanie zobaczyć że coś jest nie tak choć nie wie co
* X: Barman potrzebuje pewnej demonstracji
    * Amanda wyjmuje ucho wroga płci żeńskiej. "Chcesz by kolejne było Twojej żony?"
* Vr: Barman zrobi co powinien. Jeden cholera lojalny Barman Samszarom.
* V: Barman nigdy nikomu nic nie powie.
* X: Amanda porywa rodzinę Barmana do skończenia operacji. Trafią do jakiegoś starego magazynu gwarantując dobre zachowanie.

Kleopatra, zachętka. Tak jak zaproponował Karolinus, Kleopatra przechodzi bokiem, odpowiednio zaseedowana danymi by Armin ją zobaczył i się nią zainteresował. (nie konfliktuję zainteresowania). Armin skinął i jedna strażniczka podprowadziła mu Kleopatrę. Kleopatra rozmawia z Arminem, ostro z nim flirtując (nie konfliktuję).

Kleopatra bez problemu ściągnie go do baru. Ofc strażniczki i kapral idą z nimi. Karolinus przygotuje zaklęcie - niech wygląda, jakby kapral poślizgnął się na schodach i potrzaskał sobie nogi i walnął się w głowę. Zaklęcie jest czarem biologicznym.

Tr M +3Ob +2:

* Vr: kapral będzie potrzaskany - uszkodzone nogi i walnął się w głowę.
* Ob: zostaje aura emanacji magicznej, wykrywalna. Verlenów.
    * wskazuje na to, jakby to VERLENOWIE próbowali rozpalić starcia Samszar - Blakenbauer
* V: Armin się nie zorientuje. Zdaniem Armina, stary kapral jest po prostu niekompetentny, poślizgnął się. LOL!

Celina (jedna z ochroniarek): "Wujku!" widząc starego żołnierza. Nie wiemy skąd Celina ma urodę, ale nie po wujku. Armin: "stary kapral to nawet chodzić nie umie (nieukrywana pogarda)". Celina: "proszę, tien, pójdę z nim do szpitala!" Armin: "dobrze, ten jeden raz ZEZWALAM. Znaj moją łaskawość." Kleopatra: "zaprawdę, łaskawy jesteś, tien Samszar" Celina odprowadziła kaprala. Nosze. Nie ma problemu, Armin nie popatrzył. Do Kleopatry: "chodź, moja piękna, chodź." I idą do baru. (+1 punkt Mroku Samszarów)

Narkotyki podane bez problemu.

Kleopatra flirtuje z Arminem. Są tam też trzy strażniczki, **bardzo** zadowolone że mogą być strażniczkami a nie zabawiaczkami Arminów.

Karolinus bez problemu znajduje gdzieś ciężko pracującego chłopa od makaronu. Korzysta z tego że jest dyskretnie. Zaklęciem przenosi narkotyki berserkerskie prosto w ciało chłopa, a potem wzmacnia jego cechy fizyczne by zrobić z niego pakera. "Żywy czołg". By strażniczki się nim zajęły.

Tr Z M +3 +3Ob:

* X: Zostają ślady pozwalające w przyszłości komuś (nie Arminowi) wykryć naturę pierwotną zaklęcia.
* V: Delikwent ma w sobie odpowiednie środki berserkerskie. Walczy aż padnie.
* X: (p) Delikwent będzie potrzebował długotrwałej (2 tygodnie) wizyty w szpitalu.
    * w DOBRYM szpitalu magów to byłoby krótko, ale on jest zwykłym cywilem
* Vm: (m) Delikwent faktycznie zajmie sobą strażniczki. Będzie niebezpieczny, groźny, ale w sposób który Armin będzie lekceważył. Armin wyśle na delikwenta strażniczki.
    * wina spada na Verlena. Wszystko wskazuje, że to był ruch VERLENA a nie SAMSZARA.
    * +1 punkt mroku

Kleopatra ślicznie prosiła Armina, by on ochronił Triticatus przed Straszliwą Bestią. Armin wysłał strażniczki. Śmieje się z ich niekompetencji, bo: one gonią "stwora", "stwór" unika, manewruje, popchnie...

* Ar->Kl: "Popatrz, o piękna z czym mam do czynieia. Głupota. Niekompetencja. Żałość. Ale przynajmniej są ładne."
* Kl: "Oczywiście, tien (z zalotnym uśmiechem)"

Kleopatra próbuje podpuścić Armina by poszedł załatwić makaronostwora. "Skoro one są tak niekompetetne, tien, proszę o pomoc! Będą się wstydzić, bo Ty jesteś w stanie a one nie."

Ex Z +3Og +3:

* X: Armin decyduje, że woli pójść z Kleopatrą do łóżka. I ona nie ma nic do gadania.
* X: Armin nie pójdzie sam. Zabierze Celinę od wujka. (nie ma nic do udowodnienia ludziom) (-3Og) (+1Vg dragi)
* Vz: Armin pójdzie. Kleopatra ładnie prosi, jest miła i usłuchana, pomoże jej. I pokaże swoim strażniczkom jak działa PRAWDZIWY tien Samszar. Żadnych kaprali. Weźmie Celinę ze sobą.

Kleopatra prowadzi Armina oraz Celinę do miejsca, gdzie jest MAKARONOTWÓR. Amanda, Elena, Karolinus są już AKTYWNI. Kleopatra prowadzi Armina, który ma już lepszy humor. Tak, idą wieczorem. Tak, Celina zaproponowała pójście rano. Armin powiedział patrząc na Kleopatrę, że "to nie ma znaczenia, poradzę sobie". I Celina nie ma nic do gadania.

Armin CHCIAŁ iść pierwszy, Celina się mu postawiła i powiedziała że ona idzie pierwsza. Armin się zgodził. Amanda zastawiła pułapkę kłusowniczą, by Celina była unieszkodliwiona i nie mogła wejść z Arminem. Amanda zastawia ją w taki sposób, by Celina stała się przynętą na stwora.

Tr +3 +3Or:

* Or: (p) Amanda zastawiła dobrą pułapkę, nie przywykła do ludzi w "cywilnych" pancerzach. Krew trysnęła. Celina wrzeszczy. 
    * Stwór wypełza z podziemi.
* Vr: Armin chce chronić Celinę. O dziwo. Mimo, że "nią gardzi", ale wychodzi przed Celinę, krzyczy do Kleopatry "zajmij się nią" i przygotowuje magię. OCZYWIŚCIE tak jak pokazał plan, narkotyki itp - Armin myśli że miał Paradoks.
* X: stwór łamie rękę Arminowi
    * +1 punkt WTF SAMSZAR CO TO ZA WYCHOWANIE
* Vr: Armin krzyczy z bólu, płacze i ucieka.

Stwór go nie goni. Stwór patrzy na Celinę. Stwór oblizuje się makaronem. 

* X: Stwór rzuca się na Kleopatrę, Amanda ją zasłania swoim servarem i wchodzi w walkę bezpośrednią ze stworem. Jej servar nie daje rady, jest uszkodzony, ale się trzyma
* AMANDA chce wziąć makaronostwora, posiekać, złapać i wrzucić do strumienia.
    * -wszystkie Or
    * +3Vz (servar) +2Vr (Amandę i jej skillset walki) -- 2 V.
        * V: Amanda dała radę odciągnąć stwora od Celiny i od Kleopatry
        * V: Amanda wskoczyła ze stworem do wody. Zniszczyła go. Servar zdehermetyzowany (Lancer).
    
Amanda zgarnie dziewczyny. Elena podaje narkotyki Celinie, Amanda doprowadzi do tego, by Celina dostała pomoc medyczną. I zajmie się Kleopatrą, która jest biała i hiperwentyluje. Odrobina bólu od Amandy i Kleopatra wzięła się w garść.

Młodziak ucieka na bagna Blakenbauerów. Nie widzi innej drogi.

Wiktor Blakenbauer przeprowadza swoją porcję Mrocznego Planu.

Tr Z +3:

* X: Blakenbauer widząc, że młody Samszar jest w złym stanie (złamana łapka) założył, że TAK MA BYĆ i trzeba młodego ciorać bardziej. Poszła krew. Tnie młodego jak w rzeźni.
    * dodatkowe punkty TERRORU
* Vr: Armin NIE MA NADZIEI. "ZABIJCIE MNIE PLS!"
* Vz: Traper, stary doświadczony bagnołaz (ranger) wyciągnął młodego maga, dał mu środki na detox, opatrzył mu rany, opierdolił jak burą sukę za niekompetencję "A CO TY ROBISZ DEBILU! CHCESZ ZGINĄĆ?!"
* X: Zrzucenie winy na Floriana.
* X: MIMO że to wina Floriana, NADAL Zespół dostaje solidny opierdol.
* Vr: ...ale młody Armin się nauczy, że ludzie to coś więcej niż "głupie laski do łóżka"

Sytuacja jest dziwna:

* Armin jest półprzytomny i w złym stanie (dzięki, Wiktor...)
* Potwór nie żyje, bo Amanda go zabiła.
* Armin NIE MA JAK i NIE MA PO CO wzywać pomocy. <-- cel nieosiągnięty

Cele były:

* Twój klan Ci pomoże   <-- nieosiągnięty
* ludzie mają wartość i nie można ich lekceważyć ani nadużywać  <-- osiągnięty
* nie wszystko rozwiążesz samemu    <-- osiągnięty

Dwa z trzech celi osiągnięte. Mogło być gorzej ;-).

Podsumujmy:

* Jeden kapral, solidnie ranny. Celina (bratanica) nie ma nogi (dzięki, Amanda). Biedak chłop też w szpitalu.
* Armin ma inne podejście do ludzi, jest straumatyzowany, rehabilitacja itp.
* Florian ma WPIERDOL. Zespół też ale mniejszy.
* Zespół ma opinię "Dość Mrocznych Samszarów". To jest akceptowalne.
* Elena nawiązała linię z Wiktorem Blakenbauerem i zlinkowała go z mafią (Amanda)
* Kleopatra jest straumatyzowana i ma dla Bilgemenera dowód, że Amanda współpracuje z GROŹNYMI Samszarami.

## Streszczenie

Florian chciał dać szansę Elenie i Karolinusowi i zrobił operację w której E+K mają wychować młodego Armina z pomocą rodu. Jednak E+K zadziałali w sposób pokazujący że są chętni do lekceważącego krzywdzenia ludzi i jakkolwiek osiągnęli cel z Arminem, oni sami "zawiedli". Florian dostał wpierdol. Amanda i Elena nawiązały pozytywne kontakty z Wiktorem Blakenbauerem.

## Progresja

* Amanda Kajrat: ma opinię, że współpracuje z najmroczniejszymi z mrocznych Samszarów. Bilgemener się jej boi. Złoty Cień nie stanie jej na drodze. Jednocześnie w oczach Złotego Cienia, broni swoich ludzi nawet jeśli Samszarowie ich poświęcają.
* Amanda Kajrat: dzięki Elenie Samszar ma kontakt z Wiktorem Blakenbauerem, który uważa ją za bardzo potężną i wpływową agentką mafii. 
* Elena Samszar: zbudowała silny sojusz z Wiktorem Blakenbauerem; Wiktor jest zainteresowany byciem wsparciem dla Eleny, ona jest znacząca i nie tylko w kontekście mafii
* Elena Samszar: Samszarowie stwierdzili, że nie dba o ród. Wykonuje polecenia, ale nie ma tam 'serca'. Zdaniem Samszarów nie zależy jej na nikim poza niej samej.
* Karolinus Samszar: Samszarowie stwierdzili, że nie dba o ród. Wykonuje polecenia, ale nie ma tam 'serca'. Zdaniem Samszarów nie zależy mu na nikim poza nim samym.
* Florian Samszar: ma potężny WPIERDOL od rodu za niekompetencję, narażenie Armina, narażenie okolicy itp. Wierzył w Karolinusa i Elenę, ale naprawdę przez niego wszyscy (ludzie, Armin) ucierpieli.
* Tadeusz Maskiewnik: ranny przez działania Karolinusa (magia); uszkodzone nogi i walnął się w głowę.
* Celina Maskiewnik: ranna; wprowadzona na pułapkę która odcięła jej nogę i PRAWIE zabita przez makaronotwora gdyby nie Amanda a POTEM amnestyki i szpital. Miesiąc w szpitalu.
* Armin Samszar: ciężkimi ranami, złamaniem ręki itp. nauczył się, że ludzie mają wartość i nie można ich lekceważyć ani nadużywać oraz że nie wszystko rozwiążesz samemu.

## Zasługi

* Amanda Kajrat: pozyskała od Bilgemenera ładną dziewczynę do pułapkowania Armina oraz dzięki Elenie nawiązała silny link z Wiktorem Blakenbauerem. Zastraszyła barmana pokazując zmumifikowane kobiece ucho (które nosi dla takich chwil). Ciężko zraniła Celinę pułapką (przypadkowo, nie doceniła słabości servara) ale potem poszła ją uratować przed makaronowym potworem jak żaden z magów nie uznał tego za istotne. Chroni "swoich".
* Elena Samszar: mercenary (co mam z tego mieć) a nie pomoc rodowi; opracowała (z Amandą) plan jak przerazić młodego Armina oraz sama uznała, że nie będzie współpracować do tego celu z innymi Samszarami. Zrobiła Makaronowego Potwora, zbudowała sojusz z Wiktorem Blakenbauerem (nadając mu Amandę Kajrat jako mafię za nagrodę) i nie dała Karolinusowi zwinąć do łóżka Kleopatry. Gdy pojawił się Karolinus, zaprzestała aktywnych działań.
* Karolinus Samszar: mercenary (co mam z tego mieć) a nie pomoc rodowi; pojawił się później by pomóc Elenie, chciał "położyć Kleopatrę do snu", ale Elena mu nie dała. Bez problemu magią biologiczną wpierw zranił kaprala a potem zmienił niewinnego człowieka w berserkera by pozbyć się strażniczek Armina. Bezwzględny, nie dba o swoich ludzi (podwładnych Samszarów).
* Florian Samszar: 27-letni tien wierzący szczerze w to, że Karolinus i Elena mają ogromny potencjał; zrobił operację "nauczmy młodego tiena jak żyć" by oni się przekonali że mają wsparcie rodu. Padło katastrofalnie, nie tylko Armin został ranny i Karolinus i Elena skrzywdzili zwykłych ludzi ale i Florian wyszedł na idiotę i naiwniaka.
* Rufus Bilgemener: na żądanie Amandy dostarczył jej Egzotyczną Piękność do wpłynięcia na Armina. Dowiedział się jak okrutnymi Samszarami są ci, z którymi pracuje Amanda. Nie stanie przeciw Amandzie.
* Celina Maskiewnik: śliczna bratanica kaprala Maskiewnika w ochronie Armina Samszara; jako jedyna poszła z Arminem i Kleopatrą na makaronowego potwora, postawiła się że ONA wchodzi pierwsza, wpadła w pułapkę i prawie zginęła. Bardzo bliska wujkowi.
* Tadeusz Maskiewnik: 57-letni kapral i weteran; trafił za karę by chronić młodego Armina i swoją Celinę. Lekko paranoiczny, próbuje Armina wychowywać co nie wychodzi a Armin 'eggs him on'. Karolinus zaklęciem go z zaskoczenia rozwalił i Tadeusz wyszedł na niezgrabnego głupca.
* Wiktor Blakenbauer: 22, ekspert od środków psychoaktywnych; ku wielkiemu zdziwieniu skontaktowała się z nim Elena i skontaktowała go z mafią Kajrata. Ale za to pomógł postraszyć młodego Armina Samszara używając płaszczek i uratował go starym traperem (człowiekiem).
* Kleopatra Trusiek: egzotyczna drakolicka piękność Bilgemenera; podała narkotyki Arminowi zgodnie z planem, podpuściła Armina do polowania na makaronotwora i prawie ucierpiała - gdyby nie Amanda. Przerażona i posłuszna.
* Armin Samszar: 16-letni 'książę', którego trzeba nauczyć życia; ma tendencje do robienia wszystkiego samemu, otaczania się młodymi dziewczynami i nadużywania władzy. Ma cztery strażniczki, śliczniuteńkie i nadużywa władzy wobec nich itp. Poszedł z jedną z nich podpuszczony przez Kleopatrę, spotkawszy się z POTWOREM ten złamał Arminowi rękę. Żałośnie uciekał, zostawił dziewczyny na śmierć i Blakenbauerskie stwory go pocięły. W końcu uratował go traper (ranger) po stronie Blakenbauerów. Nauczył się, że ludzie to COŚ WIĘCEJ.

## Frakcje

* Klan Samszarów Milterius: przeprowadza operację wychowania Armina, Karolinusa i Eleny rękami Floriana. Nie udało się, Florian nie jest w stanie tego zrobić. Zamiast tego Armin został ranny, Florian dostał opierdol a na Elenie i Karolinusie tymczasowo... ech.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Triticatus, północ
                                1. Stare magazyny: nieużywane od czasu niewyspecyfikowanych problemów z Blakenbauerami. Tu zmontowano Mroczny Plan wprowadzenia makaronowego potwora.
                            1. Triticatus
                                1. Makaroniarnia (NW)
                                    1. Pola Pszenicy
                        1. Powiat Blakenbauer
                            1. Gęstwina Duchów: przy granicy z Samszarami, teren bardzo pełny podłej roślinności i bardzo niebezpieczny dla duchów

## Czas

* Opóźnienie: 2
* Dni: 9

## Specjalne

* .

## OTHER
### Fakt Lokalizacji
#### Triticatus

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Triticatus: miasteczko niedaleko Mirkali słynące przede wszystkim z produkcji świetnego tradycyjnego makaronu; niedaleko granicy z Blakenbauerami; około 11k osób
                                1. Makaroniarnia (NW)
                                    1. Strumień Pszeniczny
                                    1. Systemy Irygacyjne
                                    1. Wielki Młyn: miejsce, gdzie ziarna pszenicy są mielone na mąkę.
                                    1. Semolinatorium: tu m.in. przez mieszanie tworzy się ciasto do makaronu. Maszyny, sprzęt itp.
                                    1. Pola Pszenicy: tu znajdują się rozległe pola upraw pszenicy, niezbędne do produkcji makaronu.
                                    1. Formatornia: tu są maszyny do formowania makaronu
                                    1. Wielka Suszarnia: tu się suszy makaron, duża otwarta przestrzeń
                                1. Pszenicznik (SE)
                                    1. Magazyny: tu są pakowalnie i magazyny
                                    1. Dworzec Maglev
                                    1. Obszar administracyjny
                                    1. Rezydencje mieszkalne
                            1. Triticatus, północ: 
                                1. Dopływ Strumienia Pszenicznego
                                1. Stare magazyny: nieużywane od czasu niewyspecyfikowanych problemów z Blakenbauerami. 

                                