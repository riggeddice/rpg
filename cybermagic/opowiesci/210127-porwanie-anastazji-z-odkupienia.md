## Metadane

* title: "Porwanie Anastazji z Odkupienia"
* threads: legenda-arianny
* gm: żółw
* players: kić, kapsel, fox

## Kontynuacja
### Kampanijna

* [201230 - Pułapka z Anastazji](201230-pulapka-z-anastazji)

### Chronologiczna

* [201230 - Pułapka z Anastazji](201230-pulapka-z-anastazji)

## Punkt zerowy

### Dark Past

.

### Opis sytuacji

Przypomnienie sytuacji:

* Arianna jest **osobiście odpowiedzialna** przed Sowińskimi za oddanie im "Anastazji"
* Infernia ma trochę czasu zanim odda im "Anastazję"
* Na Infernię przyjdą pretorianie mający pilnować Anastazję
* Trzeba będzie wypuścić Anastazję by wyszła poza biovat; biovat jest nieprzenaszalny
* Martyn jest święcie przekonany, że to jest prawdziwa Anastazja. Wszyscy w to wierzą - poza Arianną.

### Postacie

.

## Punkt zero

.

## Misja właściwa

Pretorianie Sowińskich w pełnych servarach antyśrodowiskowych chcą przyjść i zobaczyć Anastazję. Wchodzą na pokład Inferni. Arianna kupuje czas - niech Kamil Lyraczek zaprowadzi ich, ale niech się trochę zgubi (pokaże dorobek panienki Anastazji na Inferni). Udało się. Zamknęli się na Inferni w miejscu z anomalią; parę godzin zajmie im wydostanie się.

Tymczasem Diana zaproponowała Klaudii zrobienie obrzydliwej anomalii, takiej, by ci pretorianie nie mogli i nie chcieli. Coś, co jest fałszywym miejscem gdzie jest Anastazja. Za zgodą Arianny, poszły na to - Klaudia zrobiła maksymalnie plugawą anomalię; pomiędzy halucynacjami i podłymi rejestrami. Tak więc gdy już pretorianie zostali wypuszczeni przez Eustachego, poszli pilnować Anastazji (do miejsca, gdzie jej nie ma). Za drzwiami jest chora anomalia, oni pilnują. A tam słyszą np. jak Martyn wyciąga z niej robactwo i inne takie.

* VVV: udało się wyłączyć pretorian.
* XX: im się to śni. Co nocy. Anomalia jest koszmarna i wpływa na nich. A Elena wie, co zrobiła Klaudia z Dianą.

Elena podniosła to na kłótni z Arianną - to są ludzie. To są oddani, lojalni żołnierze. Co ona im zrobiła, czym się różni od arystokracji z Castigatora? Arianna powiedziała, że ona dowodzi i to jest ważne - Elena **musi** to zaakceptować. Nie ma innego wyjścia. A potem Martyn im pomoże. (VVX). Skrajnie niezadowolona Elena zaakceptowała tą decyzję. Warunkowo.

Arianna, Klaudia i Martyn skupili się na badaniu Anastazji - co jej ta Krypta zrobiła. Czym jest ta fałszywa Anastazja? (VVX):

* Po pierwsze, sztuczna Anastazja ma pamięć oryginalnej. Wierzy, że jest Anastazją.
* Jest kompetentna. Nadal ta sama Anastazja, ale "naprawiona" przez Kryptę.
* Ma bardzo osłabioną moc magiczną. Praktycznie nie ma tej mocy, ale ma potencjał połączenia z sentisiecią.
* W pewien sposób jest połączona z Anomalią Kolapsu.

I tu jest klops. Martyn już ogłosił, że to prawdziwa Anastazja. Zadziałanie tak, by musiał to odwołać zniszczy jego reputację. Arianna dba o swojego Martyna - nie dopuści do tego. Nie wobec jednego z najbardziej kompetentnych i zrównoważonych członków Inferni.

Dobra. Nie mogą oddać "Anastazji" Sowińskim:

* przejmie kontrolę nad sentisiecią?
* zdobędzie wpływy polityczne?
* da moc Nocnej Krypcie / Helenie?

Innymi słowy, Anastazję należy usunąć. I tylko Klaudia i Eustachy mogą wiedzieć, co podejrzewa Arianna. Po prostu Eustachy i Klaudia są odpowiednio amoralni. Więc ta operacja musi być skierowana też przeciwko załodze Inferni. Krypta miała dobry plan - ale Zespół może sformować lepszy.

Plan działa w taki sposób:

* Klaudia znajduje statek piracki w okolicy. Superpotężny / superinteligentny. Taki, który wyjątkowo nie cierpi Aurum.
* Arianna i Eustachy z "Anastazją" wchodzą na pokład statku Aurum
* Arianna i Eustachy zapewniają, że uda się Inferni (zamaskowanej jako statek piracki) porwać "Anastazję"
* Teraz to wina Sowińskich, nie Inferni.

Dobrze. Klaudia skupiła się na znalezieniu odpowiedniego statku pirackiego. Udało jej się znaleźć (VVVXX) statek o nazwie "Plugawy Jaszczur". Niestety, "Jaszczur" wie, że ktoś się za nich zamaskował by zadziałać przeciw Sowińskim. I Jaszczur będzie szukał.

Martyn porozmawiał z Pretorianami - dowiedzieć się co wiedzą o Odkupieniu (statek, na który ma przejść Anastazja). Jego umiejętności słuchania bardzo pomogły, Pretorianie powiedzieli mu kilka niepokojących rzeczy (VVX):

* Oni się trochę boją być na OA Odkupienie
* Statek ma całkowicie cybernetyczną / zcyborgizowaną załogę
* Podobno TAI się boją przebywać na Odkupieniu, Odkupienie ma inny model kontroli

Dla Arianny to bardzo niepokojące. Statek do zadań specjalnych kontrolowany przez Sowińskich? Coś całkowicie poza kontrolą i wiedzą Orbitera? Niedobrze.

Tak czy inaczej, Arianna zostawiła jako dowódcę Inferni... Klaudię. Nie miała wyjścia:

* Martyn i Elena nie mogą nic wiedzieć, muszą być zajęci
* Diana nie nadaje się; plus, perfekcyjna dywersja dla Eleny
* Pretorianie idą z "Anastazją", Eustachym i Arianną.

Eustachy zdecydował się na odwrócenie uwagi Eleny i Diany w taki sposób, że dał im sprzeczne rozkazy które wchodzą im w kolizję. (VVX). Elena **się zorientowała**, ale Diana nie daje sobie przemówić do rozsądku - "przecież Eustachy-sama nie zrobiłby czegoś tak głupiego". Na odpowiedź Eleny, że głupie działania Eustachego to norma doszło do kłótni. Między tym, Eleną naprawiającą jakąś usterkę którą Eustachy odpowiednio wysadził i tym, że Martyn jest zajęty jakimś superskomplikowanym badaniem (rozkaz Arianny) załoga Inferni została zneutralizowana przez Ariannę.

W końcu czas na realizację planu. Eustachy i Arianna z Anastazją i Pretorianami wchodzą na pokład OA Odkupienie.

Klaudia na Inferni instaluje anomalię - niech Persefona symuluje kapitana SP Plugawy Jaszczur - Rufusa Niegnata. (XXV): udało się. Za bardzo troszkę ;-).

Tymczasem Arianna czuje się na Odkupieniu bardzo, bardzo źle. Jest arystokratką. Czuje, że Odkupienie to _zły_ statek:

* jest powiązany sentisiecią. Ktoś przeniósł sentisieć na statek typu Odkupienie!
* rozpoznała jednego kolesia, który jest w załodze Odkupienia. Ten koleś kiedyś próbował zabić Anastazję. Teraz jest zcyborgizowanym elementem sentisieci Odkupienia.
* czuje myśli i sygnały typu "zabijcie mnie". Cały statek cierpi.

Teraz Arianna rozumie, skąd ten statek ma nazwę "Odkupienie". Ci, którzy zawiedli Sowińskich mogą po raz ostatni odkupić swoje zbrodnie... podłe.

Eustachy tymczasem zauważył, że statek jest zaprojektowany do walki w taki sposób, by wypełnić całość przestrzeni płynem (zwiększyć zwrotność i możliwość robienia przyspieszeń). Innymi słowy, Eustachy nie jest pewny, czy są w stanie Infernią to zniszczyć i pokonać.

Arianna nie wie do końca jak poradzić sobie z tym statkiem. "Anastazja" jest przerażona - czuje to, że to jest niebezpieczne i czuje, że to jest po prostu złe. Ale "Anastazja" ślepo ufa Ariannie ("być jak Elena"). Tak więc Arianna poprosiła Anastazję, by ta połączyła się z sentisiecią i przejęła nad nią kontrolę (w nadziei, że uda się użyć Krypty). Anastazja poprosiła Ariannę, by ta wydała jej rozkaz. Arianna dała jej rozkaz ("Dobrze, rekrucie Anastazjo") i Anastazja połączyła się z sentisiecią.

* O: połączenie przeszło przez Kryptę. Potężna fala "naprawiająca" z Krypty przeszła przez Anastazję w Odkupienie.
* XX: Anastazja w absolutnej panice. A Dariusz - to, co z niego zostało - poczuło jej obecność.
* VV: CHAOS. Rozerwanie sygnału. Skonfliktowana sentisieć. OA "Odkupienie" jest niesterowny.

Na to przylatuje Infernia, która udaje "Plugawego Jaszczura". Nie musi wiele robić, "Odkupienie" jest niesprawne i niesterowne, chwilowo. Ale Persefona, pardon, Rufus chce niewolników (XD).

Na statku jest chaos. Anastazja nie panikuje, ale by chciała. Arianna zauważa ze smutkiem, że ta wersja Anastazji jest mniej irytująca i fajniejsza niż oryginał.

Eustachy został wysłany przez Ariannę, by znalazł słaby punkt Odkupienia. By Infernia mogła uratować właściwe osoby. I Eustachy natknął się na Dariusza Krantaka; silnie zcyborgizowanego maga z wieloma "przyłączami", który został mieszanką kapitana statku i TAI i cierpi, bardzo cierpi, zdradził Anastazję kiedyś i teraz jest skazany na wieczne cierpienie i dowodzenie Odkupieniem i niech ona mu wybaczy...

ANASTAZJA WRZASNĘŁA W PANICE.

Eustachy kontrolowanie używał magii do robienia eksplozji, by uniemożliwić Dariuszowi... czy temu co z niego zostało zbliżyć się do Arianny i Anastazji a Arianna wysłała sygnał do Klaudii. Ta używając "Rufusa" odstrzeliła fragment Odkupienia, wyrzucając w kosmos Ariannę, Anastazję i Eustachego. Infernia ich przybrała. Mimo rozkazu Arianny by Infernia zniszczyła Odkupienie, Rufus odmówił. Chce niewolników. I tak Odkupienie dało radę wrócić do Sowińskich, oderwać się od Inferni...

Anastazja wróciła do biovatu, wierząc, że będzie miała małe badania.
Elena i Martyn nic nie wiedzą.
Sowińscy myślą, że to był "Plugawy Jaszczur". Uszkodzenia i killware Krypty zakłóciły psychotronikę "Odkupienia".

Arianna powiedziała, że udało się to zrobić w taki sposób, że Jaszczur porwał Anastazję - a ona ukryła siebie i Eustachego. Ale odnajdą Jaszczura! Słowo!

Gdzie trafiła Anastazja? Gdzieś ukryta na Kontrolerze Pierwszym, w jednym z "martwych biovatów", wiecznie śpiąc - o czym nie wie nikt...

A pretorianie co mieli strzec Anastazji? Zawiedli ją. Zatem dołączyli do załogi Odkupienia.

## Streszczenie

Arianna, Eustachy i Klaudia oddali "Anastazję" na OA Odkupienie tylko po to, by ją stamtąd porwać. Nawet załoga Inferni o tym nie miała pojęcia. Okazało się, że Odkupienie jest sentisieciowanym specjalnym statkiem Aurum, gdzie ci co narazili się Sowińskim mają szansę na "odkupienie".

## Progresja

* Elena Verlen: WIE, że Eustachy próbował skłócić ją i Dianę. Nie wie, czemu. Też zachowanie Arianny i Klaudii wobec Pretorian Anastazji ją zdrażniło. Ogólnie, silny cios w relacje z dowództwem Inferni.
* Anastazja Sowińska Dwa: śpi wiecznym snem w biovacie na Kontrolerze Pierwszym, o czym nikt nie wie
* SP Plugawy Jaszczur: Sowińscy go nienawidzą; szukają go by odzyskać swoją ukochaną Anastazję. A Jaszczur jest niewinny.
* OA Odkupienie: dostał killware w formie Nocnej Krypty (XD), potem uszkodzony przez Infernię. Zregenerował i odleciał do Sowińskich, acz wymaga naprawy.
* OO Infernia: ma reputację "psującej się", dzięki czemu nikt nie podejrzewa że pokonała OA Odkupienie.

### Frakcji

* Ród Sowińskich: stracili Anastazję i mają serio na pieńku z piratami, zwłaszcza z Plugawym Jaszczurem.

## Zasługi

* Klaudia Stryk: stworzyła z Dianą plugawą anomalię odwracającą pretorian a potem objęła dowodzenie Infernią, by uratować Ariannę, Eustachego i Anastazję bis z Odkupienia.
* Eustachy Korkoran: sabotował Infernię by odwrócić uwagę Eleny, skłócał Elenę i Dianę by nie wiedziały o akcji odbijania Anastazji a potem magią (wybuchów) odpychał Dariusza by kupić Klaudii czas.
* Arianna Verlen: podjęła decyzję o odbiciu sztucznej Anastazji tak, by jej załoga nie ucierpiała. Pozwala Dianie i Klaudii bawić się pretorianami, choć szkoda jej trochę relacji z Eleną. Czuje sentisieć na Odkupieniu i popchnęła Anastazję do zrobienia tego, co konieczne - połączenia z tą siecią.
* Elena Verlen: alias Mirokin; nie zgadza się na złe traktowanie pretorian Anastazji. Nie zgadza się na skłócanie jej z Dianą przez Eustachego. Protestowała do Arianny. Odsunięta z akcji, bo zbyt paladyńska.
* Martyn Hiwasser: dopytał pretorian Anastazji odnośnie Odkupienia i pomógł im zregenerować się trochę po anomalii Klaudii/Diany. Odsunięty od głównej akcji - zbyt "dobry".
* Diana Arłacz: wpadła na pomysł chorej anomalii by odsunąć pretorian. A potem posłużyła jako dywersja dla Eleny. Ogólnie - perfekcyjna Eris.
* Anastazja Sowińska Dwa: lepsza wersja Anastazji, połączyła Kryptę z sentisiecią Odkupienia i wygenerowała falę leczniczą, dezorganizując Kryptę. Skończyła w biovacie. Szanuje Ariannę i Elenę.
* Rufus Niegnat: groźny dowódca Plugawego Jaszczura; tu symulowany przez Persefonę przez anomalię Klaudii. Lubi niewolników, zwłaszcza płci żeńskiej.
* Dariusz Krantak: przekształcony przez Sowińskich w cybernetycznego kapitana Odkupienia / technohorror / TAI. Żałuje i cierpi. Powstrzymany przez Eustachego przed złapaniem Anastazji.
* OA Odkupienie: sentisprzężony statek z cyberzałogą, pod dowództwem Sowińskich. Miał przewieźć Anastazję do Aurum, ale został zaatakowany przez killware Krypty i przez Infernię (o czym nie wie).
* OO Infernia: pod dowodzeniem Klaudii wykonała operację "odbijmy Anastazję od Sowińskich, z Odkupienia". Ma (słuszną) reputację psującej się.
* SP Plugawy Jaszczur: bardzo groźny statek piracki; Infernia zamaskowała się jako oni by porwać Anastazję. Słyną z brawurowych akcji.

### Frakcji

* Ród Sowińskich: wysłali OA Odkupienie po Anastazję. Stracili ją, niby przez "Jaszczura". Okazuje się, że Odkupienie jest statkiem "karnym" Sowińskich...

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 2
* Dni: 1

## Inne

.