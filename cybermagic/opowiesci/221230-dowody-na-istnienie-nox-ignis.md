## Metadane

* title: "Dowody na istnienie Nox Ignis"
* threads: historia-talii, cena-nox-ignis, wojna-deorianska, ciche-ostrza-orbitera
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [221230 - Dowody na istnienie Nox Ignis](221230-dowody-na-istnienie-nox-ignis)

### Chronologiczna

* [221230 - Dowody na istnienie Nox Ignis](221230-dowody-na-istnienie-nox-ignis)

## Plan sesji

### Fiszki

#### 1. OO Loricatus, ciężka fregata

* Talia Derwisz: agentka insercji
* Franz Szczypiornik: kapitan Loricatus, weteran Orbitera (5x) szukający synekur (atarien)
    * ENCAO:  -0+-- |Nie ma głowy w chmurach, trzeba ciężko pracować;;Nie ufa nikomu;;Niezwykle surowy| VALS: Hedonism, Security >> Conformity| DRIVE: Pokój przez tyranię
    * "Pax Orbiter jest najważniejszy", "Jeśli wiedzą co jest dla nich dobre, będą współpracować" | "nie rozumiesz sytuacji, noktianinie" (nie używa imion tylko tytułów)
* Medea Sowińska: oficer łącznościowy Loricatus, psychotroniczka i młodziutka czarodziejka (18?)
    * ENCAO:  0-+-0 |Nie przejmuje się niczym; Formalna i zimna| VALS: Face, Hedonism >> Benevolence | DRIVE: corrupted hunger, slaves
    * "Jak sobie kapitan życzy", "Z przyjemnością przesłucham jeńców"
* Dominik Łarnisz: cyberwspomagany marine który kocha rysować (atarien)
    * ENCAO:  0--0+ |Surowy, wymagający;;Spontaniczny| VALS: Self-direction, Family >> Hedonism| DRIVE: Zapobiec cierpieniu (rozerwana rodzina)
* Wawrzyn Rewemis

#### 2. CON Ratio Spei 
##### 2.1. Grupa Ozariat

* Tristan Ozariat: (dekadianin) kiedyś oficer; potem użył Nox Ignis i "próbuje odbudować Elwirę" (jego córka to była Tatiana - ciemne włosy, blada cera)
    * ENCAO: MANIC!!! | Zimna taktyka;; Głodna dusza i nie kontaktuje| Power, Benevolence > Humility | DRIVE: odbudować / znaleźć Tatianę
    * "to jedna z nich! To moja ukochana Elwira."
* Sankor Drawas: (dekadianin) zimny enforcer Tristana, cyberwspomagany i lojalny, zna go "od zawsze"
    * ENCAO:  -0+-- |Płytki;;Skromny i unikający pokus ciała;;Nie dba o to co inni czują| VALS: Power, Self-direction >> Benevolence| DRIVE: Corrupted contagion
    * "To mój kapitan. Uratował mi życie."
* Dolor Formido: (??); noktiański mag koszmarów i wypaczania umysłów, evil incarnate, he HURTS people. BOMBSHELL.

##### 2.2. Grupa Wąż Pożera Światło

* Lithian Amarantis: (drakolita) władca kultu i narkotyków
    * ENCAO:  --00+ |Szczery; uczciwy;;Kapryśny i marzycielski| VALS: Power, Stimulation >> Tradition| DRIVE: Pokój
    * "Tego jeszcze nie próbowaliśmy, prawda? By zaćpać populację i zagłuszyć ich ból?"

##### 2.3. Orbiter

* Katrina Komczirp: (atarienka); agentka Orbitera, dodana do haremu Tristana
* Szymon Gurczam: corrupt cop; korzysta z Ozariat i sobie "wyrywa" noktianki by je "chronić".
    * ENCAO:  +0-00 |Niszczycielski;;Dowcipny, Błyskotliwy| VALS: Self-direction, Hedonism >> Benevolence, Humility| DRIVE: Rana ego, obsesyjna
* Aletia Nix: noktianka, uciekła pod opiekę Szymona przed siłami Tristana
    * ENCAO:  0+--- |Wszystko robi na ostatnią chwilę;;Zrezygnowana, wszędzie widzi ciemność| VALS: Self-direction, Security >> Benevolence, Achievement| DRIVE: Ukryć sekret (zna Lokację)

##### 2.4. Orbiter

* Raoul Lavanis: (dekadianin); noktiański snajper, advancer, detektyw.
    * ENCAO: -++0+ | Cichy i pomocny;; niewidoczny| Conformity Humility Benevolence > Pwr Face Achi | DRIVE: zrozumieć

#### 2. Frakcja z Dominy Lucis

* Sarian Xadaar: pierwszy oficer i dowódca Dominy Lucis (dekadianin) 
    * ENCAO:  0-+00 |Niemożliwy do zatrzymania;;Lojalny i oddany grupie| VALS: Benevolence, Tradition >> Face| DRIVE: Niezłomna Forteca
    * "Victorious, to the end!!!"
* Tristan Rialirat: wujek Kirei, lekarz (savaranin); ciężko poraniony kiedyś, powymieniane na prymitywne implanty. Ledwo ludzka forma.
    * ENCAO:  0-+-0 |Brutalny, bezwzględny;;Solidny, twardy;;Zawsze wie co, gdzie i kiedy| VALS: Face, Conformity >> Humility, Hedonism| DRIVE: Dominacja
    * "Uratuję Cię, Kireo, moja kochana iskierko - jak nie dał rady Twój ojciec"
* 3*serpentis
* 200*"cywil"

### Theme & Vision

* Kampania
    * Nightmare of Nox Ignis - is it real? What is it?
    * Baronowie noktiańscy - nie dopuśćmy by na tym terenie pojawiło się coś złego dla Orbitera
    * Konstytuowanie Anomalii Kolapsu - Orbiter, Noctis, starcia, GANGSTERSTWO
* Ta sesja
    * Nox Ignis niszczy użytkownika; Tristan has a corrupted soul

### Co się wydarzyło KIEDYŚ

* Jesteśmy dość świeżo po wojnie noktiańskiej.
* Talia jest wysłana, by łapać niewłaściwych Orbiterowców, noktian, przestępców.
    * Talia udaje bounty hunterkę.
* Trzeba się dowiedzieć czy Nox Ignis to prawda czy legenda.

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

* Orbiter zniszczył TAI i wszelkie funkcje defensywne Ratio Spei.
    * sytuację przejęły mniejsze lub większe gangi i grupy
    * raczej nikt nie robi krzywdy głównym agentom sterującym Ratio Spei
* Ratio Spei ma ogromną ilość osób przybywających i odchodzących
* Stacja Con Ratio Spei jest rozdzierana przez główne ideologie
    * "przeczekać i zmiażdżyć Orbiter", "Pax Orbiter", "żyć w spokoju", "kralotyczny kult", "każdy przetrwa na własną rękę" i wiele innych
    * wszystko jest na skraju walki ze wszystkim, dodajmy niewielką ilość surowców
* LOKALNIE wobec tej sesji: dwie główne siły "Zdradzieckie noktianki" oraz "Jad zwycięstwa"

### Co się stanie (what will happen)

* S0: Katrina Komczirp próbuje uciec i zostawić informacje dla Orbitera odnośnie CON Ratio Spei i Nox Ignis.
* S1: Tristan ma obsesję na punkcie swojej córki. WIE coś o Nox Ignis. Franz - "zinfiltruj, znajdź; ja zdobędę wsparcie Orbitera i się doń dostaniemy."
* SN: Ludzie ustawiają się w kolejce po narkotyki, by zapomnieć. There is no hope.
* SN: Ludzie wiedzą, że Tristan porywa dziewczyny. Ale one chcą z nim zostać; może je chce chronić? Ogólnie Tristan nie jest bardzo groźny.
* SN: Aletia była na celowniku Tristana; na pewno ją znajdzie. To tylko pytanie kiedy Tristan zaoferuje Gurczamowi dość by on Aletię oddał.

### Sukces graczy (when you win)

* uratować Katrinę zanim jej umysł zostanie zniszczony
* złapać żywcem Tristana
* przechwycić biznes Tristana

## Sesja właściwa
### Wydarzenia przedsesjowe

.

### SPECJALNE ZASADY SESJI

.

### Scena Zero - impl

Jesteś uzbrojona. Jesteś agentką Orbitera. Na imię masz Katrina. Masz ważne informacje które udowadniają, że ON wie o Nox Ignis. Ale on jest szalony, on nie jest normalny. Uciekłaś. Teraz tylko dotrzeć do czegoś Orbiterokształtnego lub chociaż przekazać wiadomość. Jesteś w "nocy" (symulowanej) na CON Ratio Spei; jest tam posterunek Orbitera w centralnym miejscu. Ratio Spei ledwo działa, bo wyłączyli TAI. Systemy są sprawne, ALE. Jesteś w Brzuchowisku; Orbiter - niewielka grupa - jest w Rdzeniu. I pomiędzy Wami są gangi. Ogólnie? Przechlapane.

Stacja jest rozwarstwiona społecznie. Orbiter powinien przejąć kontrolę, ale tego nie zrobił bo nie ma siły. Nikt z Noctis kontroli nie przejął. Jeszcze.

Ale jest dowód na to, że Nox Ignis istnieje i jest strasznym zagrożeniem. Więc jeśli uda się przekazać dane do Orbitera, ktoś Cię uratuje. Sęk w tym, że _oni_ znają stację lepiej i są lepiej wyposażeni.

Katrina ma pomysł. Chce wyjść na zewnątrz w skafandrze, znaleźć antenę oraz wysłać sygnał. Czyli musi się dostać przez śluzę. A jest z Orbitera - ma specjalne kody i ma wejście gdzie inni nie wiedzą. Katrina się skrada.

Tr Z (przewaga + uprawnienia) +2:

* Vz: Katrinie udało się szybko i niezauważenie przedostać w okolicę śluzy. Są tam skafandry. Teraz by tylko kody działały...
* Vz: Katrina skutecznie się przebrała (długa suknia -> skafander) i szybko opuściła stację. Katrina jest świetna w łączności i sygnałach - da radę dostać się do anteny.
* Vr: Katrina się wpięła i wysłała sygnał. Informacje kim jest (jawne) i że należy przesłać do Orbitera, bardzo istotne. Plus zaszyfrowane dane o Nox Ignis. Plus prośba o pomoc.

I póki starczy zasobów skafandra - znaleźć inne przejście, zmienić strój i spróbować wtopić się w teren. Może uda się nawet WRÓCIĆ. Lub schować gdzieś.

* Vz: Katrina wróciła na stację, zanim ktokolwiek ją zauważył że zniknęła. Teraz zostało jej tylko się rozmyć na stacji. Szybko musi dostać się na elementy Orbitera...

(Katrina przechwycona przez Xadaara)

### Sesja Właściwa - impl

Niecałe dwa dni później, Talia jest na pokładzie Loricatus - ciężkiej fregaty szturmowej Orbitera. Talia jest agentem insercji, infiltratorem. A przynajmniej na tej misji. Wspierać ją będzie mięsień - przesympatyczny Dominik. Dominik który jest cyberwspomaganym marine, który kocha rysować i jest ogólnie bardzo sympatyczny. A w walce, auć. Talia jest w stanie mu dorównać tylko dzięki jej wspomaganiu, ale Dominik nie walczy na pełnej mocy; ma ogranicznik berserkerski.

Po raz kolejny z Dominikiem przesłuchali wiadomość od Katriny:

* Nox Ignis istnieje! To jakaś superbroń noktiańska; anomalna! I Tristan Ozariat miał z nią do czynienia, wie gdzie ona jest!
* On jest szalony, ten koleś szuka swojej córki. On PRZERABIA dziewczyny w swoją córkę! Wiem, że Orbiter mnie nie autoryzował na tą akcję, ale weźcie mnie stąd! Ma mentalistę czy coś!
* Macie mapę polityczną którą zebrałam; nie można na nikim polegać, nawet na Orbiterze na tej stacji. Stacja jest przepełniona noktianami... a oni nas nienawidzą.
* Znajdźcie i zniszczie Nox Ignis!

Dominik -> Medea, łagodnie: "jesteś pewna, że wszystko dobrze zdekodowałaś?". Medea, zimno i formalnie: "panie kapralu, wszystko co dało się zdekodować zostało zdekodowane. Nic więcej tam nie ma. Mamy mapę polityczną i mogę pomóc ją czytać; Aurum nauczyło mnie jak na to patrzeć.". Medea SOWIŃSKA. 18 lat na oko. W praktyce, Dominik się przyznał, podejrzewa, że jest młodsza. Jak na tak młodą damę jest absolutnie socjopatyczna. Zabiła co najmniej 1 osobę strzelając jej w tył głowy, co Dominika napawa pewną odrazą. Shouldn't go that way...

"Nox Ignis" jest terminem, który pojawiał się wielokrotnie w zeznaniach jeńców wyższych rangą. To podobno ciężki myśliwiec dalekosiężny lub lekka korweta; jednostka pośrednia, zdolna do niesamowitych przyspieszeń i mająca upiorną siłę ognia. JAKIM CUDEM korweta ma dużą siłę ognia - tego nie umieją powiedzieć. Wiele wskazuje, że Nox Ignis jest jednostką anomalną. To co wiadomo - jedynie jedna osoba jest zdolna do sprzężenia z Nox Ignis; to jednostka jednoosobowa. Noktianie twierdzą, że była próba uruchomienia Nox Ignis, ale pilot po prostu umarł. Sprzężenie go zabiło.

Orbiter nie wierzył, że Nox Ignis jest prawdziwą jednostką. Za dużo sprzecznych informacji na temat. Acz po Finis Vitae niewiele Orbiter zaskoczyło. Ale Katrina do końca wierzyła, że Nox Ignis jest prawdą i zrobiła własną misję znaleźć prawdę. I ta misja doprowadziła ją do CON Ratio Spei.

I teraz Loricatus ma za zadanie uratować niesforną infiltratorkę. I przechwycić Nox Ignis - jeśli naprawdę istnieje.

Kapitan Franz Szczypiornik został oddelegowany do zajęcia się tą sprawą. Sam się zgłosił na ochotnika - zawdzięcza coś Katrinie, czego nie chce powiedzieć. Franz nie jest szczęśliwy, że komodor oddelegował JEDNĄ ciężką fregatę, wolałby mieć coś więcej (zwłaszcza, jeśli choć połowa plotek o Nox Ignis jest prawdą). Ale co tam. "Pax Orbiter jest najważniejszy". Więc poleciał.

Główny medyk statku, Wawrzyn Rewemis, jest osobą "przypisaną" do Loricatus. On i Loricatus się świetnie znają i lubią. Łącznie z umiejętnością radzenia sobie ze specyfiką TAI Loricatus, która ma swoje quirki charakteru. Ta Persefona ma pewne słabości i lubi z Wawrzynem oglądać i komentować różne operacje cybernetyzacji. Potrafią grać w karty i się spierać czy ludzka czy cybernetyczna forma są lepsze. Wawrzyn silnie optuje za ludzką, mimo, że ma koło 30% mechanizacji. Wojna. Ale ten twardy BASTARD nie zostawiłby swojej "Lorci" w potrzebie i poleciał.

Odprawa. Loricatus leci do celu a Franz informuje Talię i Dominika o swoim planie.

* F: Problem jest prosty. Jest noktianin, który ma wiedzę na temat Nox Ignis i który ma moją przyjaciółkę. Talio, zinfiltruj go i zlokalizuj gdzie się znajduje. Ja pozyskam wsparcie Orbitera na stacji i uderzymy i go zmiażdżymy. Dominik będzie Cię osłaniał.
* T: Zgodnie z informacją od agentki nie można w pełni ufać siłom Orbitera na tej stacji.
* F: Będę posiłkował się Medeą by im przypomnieć jak zachowuje się agent Orbitera. Medea jest młodą damą o wielu umiejętnościach które powinny ich przerazić. (mówi o torturach i magii)
* W: Panie kapitanie, czy to rozsądne? Loricatus to jedna fregata a naprzeciw nas jest stacja noktiańska, było nie było, oraz nasze osłabione siły.
* F: Bez obaw. Nasza wola i determinacja są zdecydowanie wyższe niż ich. Moim celem nie jest robienie krzywdy noktianom. Chcę zachować Pax Orbiter na stacji. I mam uprawnienia by noktianie oraz Orbiter zrobił to, czego sobie życzę.
* T: Skoro tak, czemu nie możemy tam pójść i zażądać wydania agentki?
* F: Od Orbitera byśmy mogli. Od noktian? Noktianie POTRZEBUJĄ zobaczyć siłę. Lub infiltracji. Katrina sama powiedziała, że noktianin który wie o Nox Ignis jest szalony. Szalony noktianin nie wyda Katriny. Jeśli on ją ma. Nie jestem zwolennikiem pospiesznych ruchów; wolę, byś Ty, Talio, zorientowała się w sytuacji zanim będę rozlewał niepotrzebną krew noktiańską.
* F: Medea będzie odpowiedzialna za komunikację między Tobą a Loricatus. Nie zawiedzie, mamy coś czego noktianie się nie spodziewają - mamy hipernet kompatybilny z Twoją strukturą. Medea jest arystokratką Aurum i może tymczasowo podpiąć Ciebie do swojej sentisieci.
* T: (spięcie) to może być trudniejsze niż się wydaje. (Franz czeka) (Talia nie kontynuuje) (Franz dalej czeka).
* T: pozwoli pan kapitanie że skonsultuję to z czarodziejką Sowińską na osobności.
* F: Jak powiedziałem, temat będzie obsłużony przez chorążą Sowińską. Więc to jej odpowiedzialność byś Ty była usatysfakcjonowana z kontaktu. W czym jeśli stracę kontakt, będę skłonny użyć oddziału szturmowego by Cię wyciągnąć, Talio.
* T: zrozumiałe.
* F: Nox Ignis niekoniecznie jest prawdą a nie mam w zwyczaju tracić ludzi bez sensu.

Medea, potem już, spojrzała na Talię _inaczej_.

* M: Chorąża Derwisz, jesteś tien? /chłodne acz zdziwione spytanie
* T: TECHNICZNIE rzecz ujmując, nie. Ale mam dość Krwi by to mógł być problem.
* M: Sentisieć?
* T: Tak.
* M: Rozumiem. (zamyślenie). Może mi się uda zsynchronizować z istniejącą sentisiecią.
* T: Byłoby to przydatne.
* M: Czy mogę prosić o trochę krwi, chorąża Derwisz? Tylko w celach synchronizacji sentisieci.
* T: Zrobimy to razem. Ty nadzorujesz i robisz, ja patrzę.
* M: Jak chorąża Derwisz sobie życzy (lekki ukłon)(lekki uśmiech).

Medea próbuje synchronizować sentisieci używając swojej magii i swych umiejętności:

Ex Z (pomoc Talii + wiedza o rodzie i szczególna wiedza o krwi) M (niezbędna) +2 + 3Ob:

* Vz: prowadzona przez Talię i mając... szczególne zainteresowania hematologiczne... Medea zsynchronizowała ich sentisieci.
    * Talia czuje jak jej sentisieć się obudziła. Czuje obecność Medei, ale jest od niej odcięta. Wie, że Medea ma to samo.
* Vz: Medea jest niesamowicie precyzyjna a Talia jest wdzięczna (gracious). Medea z przyjemnością to zrobiła, nawet moment w którym musiała troszkę krwi Talii wypić. Lekki uśmiech Medei. Creep ze strony Talii.
    * Medea ORAZ TALIA są w stanie uruchomić połączenie sentisiecią dużo częściej i łatwiej. Plus to SENTIPOŁĄCZENIE. Nawet Orbiter nie ma tego jak wykryć. Mind-to-mind comms.

Medea dygnęła elegancko przed Talią. "Jak mniemam, chorąża jest zadowolona.". Talia dygnęła tak samo, w stylu Aurum. Medei brwi powędrowały do góry.

* M: (myśli chwilę) Nie spodziewałam się, że na takiej misji spotka mnie... przyjemność cywilizacji... (szczery uśmiech, z rezerwą, ale zawsze). Brakowało mi trochę.
* T: Nie jestem może magiem, ale z przyjemnością choć w ten sposób wrócę do domu.
* M: Nie każdy mag jest warty uwagi i nie każdy nie-mag jest nieistotny (uśmiech). Jest powód czemu nie jestem w Aurum.
* T: Rozumiem.

Medea po raz pierwszy powiedziała coś od siebie i o sobie komukolwiek. Talia uznała to za pierwszy mały sukces misji. Przez resztę drogi Talia i Medea się sychronizują, sprawdzają jak połączenie działa itp. Docierają się.

Jednocześnie Talia czyta informacje o córce Tristana. Córka Tristana nazywała się Tatiana. Była praktycznie do niego identyczna; przez to nie była najśliczniejsza. Noktianie wierzą, że Tatiana zginęła podczas wojny. Ale on nie chce w to uwierzyć. Tristan próbował Tatianę wyciągnąć, potencjalnie mógł on ją zabić... znowu - Tristan, kiedyś kapitan korwety, bardzo dobrze traktujący żołnierzy itp. A teraz... nie wiadomo. Plus - Talia i Dominik potrzebują celów na bountyhuntowanie. A jeden z celów jest podobny do Tristana.

Loricatus dociera z powodzeniem do CON Ratio Spei. Jeszcze w drodze przygotował niewielki myśliwiec "Dajnakar"; ta jednostka będzie statkiem Talii oraz Dominika. Zostaną bounty hunterami i mają pełną swobodę działania. Czas odnaleźć zagubioną agentkę i dowiedzieć się prawdy o Nox Ignis. W tym czasie Franz spróbuje zrobić porządek z garnizorem Orbitera.

**Stacja CON Ratio Spei** do której dolecieliście myśliwcem.

Talia dawno już nie widziała tak PESYMISTYCZNEJ stacji. Stacja jest częściowo aktywna - brak Eszary nie pomaga. Stacja _jakoś_ działa dzięki siłom Orbitera plus lokalnym majstrom, ale nie działa monitoring, powietrze śmierdzi itp. Stacja przez to wygląda bardziej jak zamieszkane metro niż faktyczna stacja. Co więcej, da się wyczuć ducha powszechnej rezygnacji. Na tej stacji nie ma nadziei - to nawet wchodzi w ryzyko Paradygmatu.

TERAZ Talia rozumie, czemu Orbiter pozwala gangsterom rządzić. Bo to bezpieczniejsze. Daje jakąś energię. Coś, co nie jest rezygnacją i desperacją. Walczą o swoje i nie ma Kolapsu Paradygmatycznego. W świetle około 100 Orbiterowców i 30-35k noktian... auć.

Talia widzi że ten stan jest niestabilny. WIDZI, jak Aurelioni szepczą w uszy noktianom. WIDZI, jak kultyści oferują nadzieję w objęciach kralothów. Widzi jak część ludzi przekonuje noktian, że jak się sprzedadzą w niewolę, to ich życie będzie lepsze - a na pewno uratują ich przed tymi złymi z Orbitera. Innymi słowy, sępy się rzuciły. A Tristan ma ogłoszenie "chcesz spotkać wesołą, optymistyczną i niesforną noktiankę? Zajrzyj do nas ;-)".

"Bounty hunterzy" pierwsze co robią to idą do baru. Po drodze zaczepiła Was kobieta. Wyraźnie noktianka.

* Nk: "Szanowni państwo, jesteście łowcami nagród?"
* D: "To my" (bravado) "A dobrze płacisz?"
* Nk: "Niestety, mogę tylko odwołać się do Waszych sumień."
* D: "My nie mamy sumień (śmiech). Kasa lub z drogi."
* T: "Chyba że wiesz o kimś o kim jest coś wyznaczone?"
* Nk: "Nie, ale ona będzie cenna. Ona ma jakąś wiedzę, lub znaczenie, ale porwał ją Orbiter. Podobno Orbiter nie ma niewolników!"
* (INNY NOKTIANIN): (fizycznie odciąga kobietę) "Przepraszam, szlachetni państwo. Koleżanka jest nietrzeźwa. Nie ma żadnych problemów."
* D: "Nie ma kasy? Nie ma dyskusji."
* T: "Masz 5 minut."
* D: "Ej! Puść ją (do innego noktianina). Moja koleżanka chce posłuchać. Get lost."

Noktianka zaczęła opowiadać, jak Aletia - dziewczyna w lepszym stroju i wyraźnie wiedząca lub mająca więcej - próbowała się chować przed Orbiterem. Ale Orbiter ją porwał. Tutejsi. Wszystkie młode dziewczyny postawili w szeregu i choć Aletia próbowała wyglądać jak normalna to jednak została wybrana. Trzy dziewczyny zostały zabrane na zmarnowanie do Orbitera. Najładniejsze. I ona nie może się na to zgodzić - Orbiter NIE MOŻE tak robić. (righteousness). Jedna z tych trzech sama chciała, bo to prostsze życie, ale dwie pozostałe zostały zmuszone. Jako "łup wojenny". Noktianka pokręciła głową z ogniem.

* Talia -> Medea: "Posłuchaj tego, to może się przydać."
* M: "Przekażę kapitanowi. Zarekomenduję wyczyszczenie problemu noktianek. Zwłaszcza jeśli jedna z nich jest 'inna'. Zarekomenduję mu, żebym mogła ją przesłuchać, więc się wszystkiego dowiemy."

W drodze do baru wiele innych incydentów nie było. Talia i Dominik momentami ustępowali z drogi grupkom; słyszeli też liczne plotki po prostu idąc:

* morderstwa. Rozwiązywanie starych noctis vs noctis.
* ktoś zmarł ze strachu. Nie pierwszy raz. Jest tu potwór.
* Xadaar powróci i przejmie kontrolę nad bazą, po czym wszystkich ewakuuje i zderzy bazę z Astorią.
* ...itp.

Talia filtruje, ale na tym etapie nie ma danych. Większość z tego nie ma dużo podstaw. Talia dzięki bateriom irianium w dużych ilościach ma dość środków płatniczych by móc sobie na wiele pozwolić. Jak każdy bounty hunter, Talia i Dominik mają sprzęt na barter. 

W barze też nic szczególnego się nie zdarzyło. Ot, prośby, czy nie mogliby kogoś zabić czy sklepać, jakieś mniejsze rzeczy... wyraźnie nie rozumieją jak to działa. Talia korzysta z okazji - szuka sygnałów Katriny i zostawia ślady DLA Katriny by ona wiedziała że się może spotkać czy jak i kiedy. Druga rzecz? Pretekst by znaleźć Tristana. AHA, Tristan wyznaczył nagrodę za Katrinę. Niewielką, ot, uciekinierka. Ale jest.

TrZ (maska + desperacja) +3:

* Vr: Młody noktianin szuka kogoś kto mu pomoże wbić się do Tristana by wyciągnąć jego siostrę. Odda się w niewolę, bo nic innego nie ma. Niewola za wyjęcie siostry. Albo odpracuje.
    * Siostra chciała dorobić sobie jako dama lekkich obyczajów. Nie miała wyjścia, nie miała innych kompetencji tutaj. Nie na tej stacji. On nie chciał, ale co może.
    * Siostra w końcu nie wróciła a jak on poszedł został zawrócony. Umie walczyć, ale nie ma broni (rozbrojony) plus jak zginie to siostrze nie pomoże
        * Orbiter ma to gdzieś. Nawet u Orbitera był!
* V: Katrina się nie pojawiła na Orbiterze. Ale nie ma jej Tristan; też jej szukał. Ktoś _inny_ ją ma albo się świetnie schowała. Ale kto i czemu? Nie wiadomo.

Talia widzi, że wiele osób w barze i okolicy siedzi, leży... z lekkim uśmiechem. Do Talii też podszedł mężczyzna w szarym stroju. Taki lepszy szmatex. Kapłan.

* K: "Siostro, jesteś nowa na stacji?"
* T: "(mierzy spojrzeniem) Jak ja jestem Twoją siostrą..."
* K: "(lekki uśmiech) Rozumiem. Szerokiej drogi. (odchodzi)" /ma symbol węża pożerającego światło

Talia i Dominik uznali, że insercja zakończona. Czas skupić się na wykonaniu zadania...

## Streszczenie

Stacja CON Ratio Spei jest wypełniona resztkami złamanych noktian i aż ocieka pesymizmem i brakiem nadziei. Gdzieś na stacji Katrina Komczirp odnalazła sekret anomalnej superbroni Noctis, Nox Ignis i wpadła w kłopoty. Załoga Loricatus ma zadanie wyciągnąć Katrinę i poznać sekret Nox Ignis. Talia i Dominik zinfiltrowali Ratio Spei i się zorientowali w sytuacji. Jako łowcy nagród mają pierwszy plan - poszukać Katriny u Tristana Ozariata.

## Progresja

* Talia Derwisz: zintegrowana sentisiecią z Medeą Sowińską, dzięki czemu mają dwustronną komunikację i może coś jeszcze.

### Frakcji

* .

## Zasługi

* Talia Derwisz: cyberwspomagana infiltratorka wysokiej klasy z ukrytymi implantami; nie ma mocy magicznej tylko echo sentisieci. Dowodzi suboperacją infiltracji Ratio Spei i znalezienia Katriny Komczirp (czyt. dowodzi Dominikiem).
* Dominik Łarnisz: przesympatyczny cyberkomandos który kocha rysować; ma ogranicznik berserkerski. Wspiera Talię jako ko-infiltrator. On wygląda groźnie, ona całkowicie nie.
* Medea Sowińska: 18-letnia czarodziejka, ekspert komunikacji Loricatus, zimna i formalna z nastawieniem do dominacji i bezduszności. Zabiła co najmniej 1 osobę strzelając jej w tył głowy. Zintegrowała swoją sentisieć z resztką sentisieci Talii dając im perfekcyjną komunikację.
* Katrina Komczirp: przeprowadziła samodzielną operację mającą dowiedzieć się o lokalizacji Nox Ignis i o tym czym jest Nox Ignis. Wymanewrowała siły Tristana i wysłała sygnał poruszając się po pancerzu Ratio Spei. Ale wpadła w czyjeś ręce.
* Sarian Xadaar: znajduje się na Ratio Spei; przechwycił Katrinę Komczirp. Jego aktualne plany są nieznane; jest poszukiwany przez Orbiter.
* Tristan Ozariat: kiedyś oficer noktiański; wie o Nox Ignis, jest szalony. Miał u siebie Katrinę Komczirp jako ukrytą agentkę Orbitera. Podobno szuka swojej córki i PRZERABIA dziewczyny w swoją córkę.
* Tatiana Ozariat: córka Tristana, nie wiadomo co się z nią stało. Noktianie wierzą, że zginęła podczas wojny.
* Franz Szczypiornik: kapitan Loricatus; zgłosił się na ochotnika bo zawdzięcza coś Katrinie. Chłodny styl wypowiedzi, nie jest fanem noktian ale wierzy w prawidłowy Orbiter.
* Wawrzyn Rewemis: medyk Loricatus; blisko zaprzyjaźniony z Persefoną d'Loricatus. Podobno ta TAI jest jego dziewczyną.
* Persefona d'Loricatus: z natury cicha i zdyscyplinowana, lubi z Wawrzynem oglądać i komentować różne operacje cybernetyzacji.
* Aletia Nix: podobno porwana przez Orbiter noktianka, bo jest ładna, jako łup wojenny?
* OO Loricatus: ciężka fregata szturmowa Orbitera p.d. Franza Szczypiornika, w dobrym stanie i zdolna do działania autonomicznego bez jednostek wsparcia.
* AK Nox Ignis: pierwszy raz Orbiter się o niej dowiaduje jako o 'anomalnej superbroni noktiańskiej'. Nieobecna na sesji.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Libracja Lirańska
                1. Anomalia Kolapsu, orbita
                    1. CON Ratio Spei: noktiańsko-astoriańska stacja klasy ONeill Colony z 35k osób, z wojną domową między gangami i bez głównej TAI    
                        1. Brzuchowisko: przepełniony fragment cylindra, gorszej jakości
                        1. Rdzeń: najlepiej chroniony i kontrolny system Kolonii; tam są szkieletowe siły Orbitera

## Czas

* Chronologia: Zmiażdżenie Inwazji Noctis
* Opóźnienie: 24
* Dni: 4
