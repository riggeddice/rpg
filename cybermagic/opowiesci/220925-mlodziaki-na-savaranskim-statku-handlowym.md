## Metadane

* title: "Młodziaki na Savarańskim statku handlowym"
* threads: historia-klaudii, historia-martyna, grupa-wydzielona-serbinius
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [220716 - Chory piesek na statku Luxuritias](220716-chory-piesek-na-statku-luxuritias)

### Chronologiczna

* [220716 - Chory piesek na statku Luxuritias](220716-chory-piesek-na-statku-luxuritias)

## Plan sesji
### Theme & Vision

* Savaranie spotykają ambitnych Atarienów
* "Chcesz niezależności, ale nie rozumiesz czym niezależność naprawdę jest"
* Rebuild the cruiser, not for war... for home.
* Martyn naprawdę, naprawdę nie jest wolny od echa Noctis

### Co się wydarzyło

* Na 'Hektorze 17' zachorował pilot i zastępca pilota. Savaranie musieli zaakceptować lokalsów z K1 (zanim dotrą do Valentiny)
  * Perdius nie ma z tym kłopotu, bo docelowo przejmie kontrolę biosynt Xartel (przy współpracy z Semlą) - ale to absolutnie zakazane
  * Klaudiusz i Mariola podpisali wszystkie papiery - ale nie zdradzili kim naprawdę są i że szukają przygód
* Nie dogadują się ze sobą, niespodzianka XD. H17 ma zimno, zero przyjemności i nie wolno zużywać energii
  * Ma magigen na poziomie agonalnym i wycieki, ale ma sześć kotów (Pacyfikatorów)
  * Generator i paliwo są niewielkie, lecą dużo wolniej niż to konieczne (ale dużo taniej)
* Klaudiusz i Mariola nie chcą być dłużej na tej jednostce...
* H17 przewozi części nadające się do innej, większej jednostki - szczególnie interesujący jest uszkodzony reaktor 'Kalaris 41'

### Co się stanie

* S1: znaleźć H17 (setup), podejrzenie porwania. Porusza się dziwnie. (setup)
* S2: H17 to dziwna jednostka (setup)
* S3: Klaudiusz i Mariola chcą opuścić ten statek ale wszystko zgodnie z umową (twist)
* S4: Biosynt Xartel, nienaturalna jednostka, noctis (payoff)
* S5: 'Kalaris 41' -> inna jednostka? (twist / payoff)

### Sukces graczy

* Dobre pytanie. Klaudia rozwiąże sesję.

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Fabian Korneliusz poprosił do briefing roomu Klaudię. "Mamy poważny problem - Syndykat porwał obywateli Orbitera!" Wyraźnie nie wie co robić i jest pod wpływem silnych emocji. Klaudia "ojej". Fabian "próbowałem ekstrapolować gdzie ta jednostka może się znaleźć, ale nie umiem..."

Klaudia pyta o szczegóły. Fabian wyjaśnia:

* Jednostka nazywa się 'Hektor 17'.
* 'H 17' na pokład wziął? Porwał? Dwie osoby - w czym osobę powiązaną z Orbiterem. I zniknął.
* Nie ma go tam gdzie powinien być gdyby normalnie leciał. Fabian nie umie go znaleźć na skanerach (to tłumaczy Klaudii czemu zmienili kurs)
* ...i wszystko wskazuje na to, że to nie jest oficjalna misja

Klaudia pyta o większe szczegóły...

* Syn MENTORA Fabiana, Klaudiusz, został zatrudniony jako pilot 'H 17'. I zniknął.
  * Klaudiusz nie miał opuszczać K1. To sprawa rodzinna. Klaudiusz tak jakby uciekł z domu. Ale jest pełnoletni...
    * Klaudiusz i jego ojciec się nie pokłócili; najpewniej Klaudiusz z dziewczyną. Która też zniknęła. Ale ona jest nieważna.
  * 'H 17' miał wszystkie papiery w porządku. To normalna jednostka, na oko. Zrujnowana.
    * Zniknął, bo nie ma go na radarach! 

Na tym etapie Klaudia rozpytuje dyskretnie okoliczne statki używając sigila Fabiana. Fabian jako kapitan tej jednostki może się dopytywać. A Klaudia ma to gdzieś, zwłaszcza że Fabian i tak ją poprosił.

Tr Z (dane jednostki) +2:

* V: 'H 17' był widziany przez różne jednostki; leciał prawidłowym kursem. Ale upiornie wolno. Jakby na 'jałowym'.
  * Zakładając ten kurs powinien być 2h Twoją prędkością w TAMTĄ stronę (ich jakieś 10h).
  * Co więcej, ich sygnatura będzie obniżona - niskie echo elektroniczne itp.

Zdaniem Fabiana to podejrzane. Zdaniem Klaudii ...ok? Ale niech będzie. Polecą tam.

I faktycznie, 2h później, da się wykryć słabiutki sygnał. To normalny sygnał, transponder, wszystko jak powinno być - tylko jednostka ma bardzo niskie echo i jest praktycznie powyłączana. Fabian -> Klaudia "musimy mieć powód wejścia i zobaczenia w jakim stanie są Klaudiusz i Mariola".

Jak są blisko Klaudia zarzuca pełny skan. Szuka coś dziwnego.

Tr +3:

* V: COŚ dziwnego? XD
  * poziom temperatury jest obniżony. Jest tam chłodno, po prostu.
  * poziom promieniowania magigeneratora? Podniesiony; potencjał efemeryd.
  * poziom energii wypromieniowywanej? Prawie Zero. Zero rozrywki, światła ciemne...
  * statek wygląda na patchwork. Trzyma się kupy, ale naprawdę nie wygląda.

Klaudia wyciąga dziwne promieniowanie magiczne jako podstawę i łączy to z odpowiednim kodeksem po czym formułuje formalną prośbę wejścia na pokład celem inspekcji. Przedstawia to kapitanowi acz ostrzega że to nie jest mocna podstawa. Kapitan jest 100% za - wisi swojemu mentorowi dużo.

Kapitan idzie za ciosem - chce inspekcji

Tr Z (bo jest podstawa nawet jak słaba) +2:

* V: Fabian poważnie nalega na inspekcję. Głos z drugiej strony spokojnie nie chce się zgodzić. Fabian przedkłada papiery. Po chwili głos się spokojnie zgadza. Ale proszą, by Fabian nie marnował energii - niech przylecą w skafandrach itp.
* V: Fabian się NIE ZGADZA. To jest INSPEKCJA. Jak mają wyliczone - mają bufory? Mają. "Przyjmijcie nas prawidłowo" - nie żeby oszczędzać energię itp. 

Fabian chce Klaudię na pokład 'Hektora 17'. I jednego strzelca. Trzy osoby - jak długo zgodnie z procedurami. Właścicielem statku jest Perdius Aximar i jest kapitanem tej jednostki (i tak, słynie z małomówności i spokoju). Klaudia - przewidując potencjalne problemy - zostawia sygnał Martynowi.

H17: jest tam niejasno. Powietrze jest bezpieczne, ale nie pachnie ładnie. Zero ozdób. Bardzo... utylitarny i ponury widok. Kapitan powitał ich osobiście. Klaudia chce się przejść po statku - czy wszystko działa i to że ma podniesioną energię to coś ryzykownego i czy przykręcili do minimum.

Klaudia rozgląda się, towarzyszy jej nastolatek. Wykryła obecność zaczynającej się anomalii... która po chwili zniknęła. Po chwili przebiegł koło niej Pacyfikator. Coś złowił i jest szczęśliwy. Na twarzy chłopaka pojawił się cień uśmiechu (typu: "kot :D"), który szybko zniknął jak tylko zorientował się że Klaudia widzi.

W rosterze jest SZEŚĆ Pacyfikatorów. To nie jest tania sprawa. To pasuje - obniżenie kosztu magigeneratorów itp, wprowadzenie Pacyfikatorów... acz nie jest to normalne działanie. Tak samo chłopak potwierdził - minimalna moc silnika, generatorów, prądu... nie przewożą towarów delikatnych i time-limited, więc mogą lecieć powoli i oszczędnie. Chłopak też mówi krótkimi zdaniami i zachowuje pokerową minę.

Klaudia rozmawia z chłopakiem jak z niepewnym AI; chce wyrobić sobie opinię czy o coś warto się martwić czy to po prostu dziwny statek.

Tr (pełny bo jak AI) Z (do tego przywykł) +2:

* X: oni odbierają inspekcję Orbitera jako wrogą operację. Orbiter próbuje coś im zrobić. Dlatego ogromna rezerwa. chłopak nikomu łącznie z Klaudią nie ufa, ale ojciec kazał mu rozmawiać by się uczył.
* X: Fabian MUSI wydobyć Klaudiusza i Mariolę bo oszaleją.
* V: Klaudia rzeczowym podejściem wyprowadziła coś ciekawego
  * to jest statek savarański. Ten typ tak ma. Wszystko jest bezpieczne acz KAŻDY NORMALNY CZŁOWIEK by oszalał.
  * oni trzymają się razem. Jest 'rodzina' czyli ten statek i wszyscy inni
  * niestety, nasz pilot jest chory. Dlatego musieliśmy zatrudnić tymczasowego - aż dolecą do Valentiny. Nic poważnego, wyjdzie ale musi przeleżeć.
    * TAK pilot ma lepsze warunki.

Fabian spotkał się z Klaudią. "Musimy ich wyciągnąć." Klaudia "nieważne że to noktianie - zostawisz statek bez pilota?" Fabian "wynegocjujmy lepsze warunki..." Klaudia "nam nie wolno wtrącać się w handel."

Fabian ma NOWY POMYSŁ. Zatrzymać ten statek by ewakuować ludzi. Klaudia - ale są tu Pacyfikatory... a pilot będzie zagrożony. Fabian - i dlatego chcę wycofać pilota. Dobra. Klaudio, sprawdź papiery Pacyfikatorów.

Klaudia wie też jedną rzecz - jeśli to savaranie, to nawet jak mają jednego i tylko jednego pilota z papierami to wszyscy sobie poradzą w jakimś stopniu. Klaudia więc o to spytała kapitana. On powiedział, że każdy sobie w jakimś stopniu poradzi, więc poradzą sobie z unikalnym problemem. 'Hektor 13' jest czymś co sobie poradzi.

Klaudia robi audyt rostera i transportowanych rzeczy.

TrZ +2:

* V: W całej załodze jest jedna osoba, która nie ma uprawnień NA NIC. Nawet dzieciak ma jakieś uprawnienia. Ale Xartel nie ma żadnych (dorosły). I z loga wynika, że pełni rolę mechanika i advancera i pilota i i i... ale nie ma żadnych uprawnień.
* V: Jedno z urządzeń które ma duże zużycie energii to komora ładowania biosyntów. Nie ma żadnej informacji o biosyntach na pokładzie w manifeście itp - bo zwłaszcza noktiańskie biosynty są super nielegalne.
* V: Wszystkie rzeczy które są kupowane, sprzedawane i transportowane itp. Klaudia z pomocą mechanika Helmuta potrafi zrozumieć i umiejscowić. Ale coś, czego na pewno się nie uda sprzedać - uszkodzony reaktor 'Kalaris 41'. Służący zwykle na noktiańskich krążownikach. Wśród wszystkich transportowanych rzeczy są też elementy noktiańskiego krążownika. Coś, czego na pewno się nie sprzeda.

Klaudia nie chce w to wchodzić i nie chce gadać ani z Martynem (który jest bardzo antynoktiański) ani z Fabianem (który bardzo chce coś znaleźć). Ale może dyskretnie o tym porozmawiać z kapitanem Perdiusem. Klaudia zostawia ślady w papierach (tak samo zakopane jako dead man's hand) ale chce z Perdiusem porozmawiać.

Perdius powiedział Klaudii wyraźnie - dla niego 'Hektor 17' to dom. Ten reaktor trafi do Pasa Kazimierza. Po co mu krążownik bojowy? On po prostu chce wrócić do domu. Cichego, chłodnego.

Klaudia powiedziała Perdiusowi, że usunie zapis o generatorze i biosyncie. Wyjaśniła sytuację - biosynt byłby problemem. Ale dlatego musi spytać - czemu ładowarka biosynta tyle ciągnie.

Tr (niepełny) Z (udowodniła chęć współpracy) +3:

* V: Odpowiedź: "Ładowarka biosynta zużywa dużo energii bo jest tam przepięcie i jest podpięta do komponentów pomagających choremu pilotowi.". It's plausible, ale Klaudia wie, że się profil energii nie zgadza. Widząc nieprzekonanie Klaudii dodał "poza obszarem Orbitera jest niemało biosyntów. A ta jednostka potrafi im pomóc. Dlatego muszę to mieć aktywne - w innym wypadku wyłączenie mogłoby poważnie coś zepsuć." (i to jest prawda - ale Klaudia jest przekonana, że na pokładzie jest biosynt.)
  * Klaudia: "Cokolwiek jest na pokładzie - nigdy koło Orbitera i zero problemów Orbiterowi."
  * Kapitan: "Nie byłoby nas tu gdyby nie choroba pilota i Wasze regulacje. Gdybyśmy mieli biosynta, traktowalibyśmy go jak Pacyfikatory."

Kapitan aż spytał czemu robią inspekcję - nigdy nie robił problemów i nie miał problemów. Klaudia powiedziała wyraźnie "ci, których zatrudniłeś na K1 tu nie pasują." Kapitan odpowiedział "jak zapewnię na statku światło i warunki Waszych statków - będzie dobrze?" Klaudia "nie wiem".

Klaudia powiedziała kapitanowi "oddaj tą dwójkę - powiedz, że KTOŚ INNY jest tym kto jest chory. Skasuj informację o zatrudnieniu tych dwóch i leć swoją drogą". Kapitan się na to zgodził. I Klaudia zapewniła, by Klaudiusz nie dostał kasy za bycie pilotem.

Fabian "NIE WIEM JAK TO ZROBIŁAŚ ALE TO BYŁO ŚWIETNE!"

Martyn do teraz nie wie, że to była noktiańska jednostka. Nikt mu nie powie. Klaudia ma ten mały sekrecik.

## Streszczenie

Młody Orbiterowiec wraz z dziewczyną zatrudnili się na savarańskiej jednostce SC Hektor 17. Ojciec owego Orbiterowca jest mentorem Fabiana Korneliusza, więc Zespół udał się wyciągać tą dwójkę. Klaudia doszła do tego, że to neonoktiańska jednostka i współpracując z kapitanem tej jednostki wyciągnęła młodych oraz ukryła niegroźne naruszenia na 'Hektorze 13' które Fabian by wykorzystał.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Klaudia Stryk: spotkała się z savarańską jednostką cywilną neonoktiańską i unikając Martyna rozwiązała problem; doszła do tego, że ta jednostka jest dziwna ale nie jest groźna i dogadała się z kapitanem 'Hektora 13' - on po prostu chce odbudować swój dom.
* Fabian Korneliusz: syn jego mentora wpakował się na statek cywilny z dziewczyną i 'zniknął'. Fabian lokalizuje (Klaudią) jednostkę cywilną 'Hektor 13' i robi wszystko, by wyciągnąć te osoby. Jest skłonny nawet zadziałać niezgodnie z celami Orbitera. I nie zauważył że 'Hektor 13' to jednostka neonoktiańska.
* Martyn Hiwasser: nadal nie jest wolny od echa Noctis i wojny z Noctis; był obecny, ale Klaudia skutecznie mu nic nie powiedziała.
* Klaudiusz Zanęcik: syn oficera Orbitera; chciał uciec z dziewczyną i przeżyć przygodę ale wpakował się na jednostkę savarańską Hektor 13. Wyciągnęła go Klaudia i Fabian.
* Perdius Aximar: kapitan 'Hektora 17'; savaranin. Z uwagi na chorobę pilota zatrudnił młodych Orbiterowców i wpadł w tarapaty przez inspekcję Orbitera i różnice kulturowe. Szczęśliwie dogadał się z Klaudią i po oddaniu jej tych Orbiterowców może kontynuować odbudowę jednostki domowej.
* Dawid Aximar: 16-latek syn kapitana na 'Hektorze 17'; savaranin. Nieufny Orbiterowi, ale już wystarczająco kompetentny by oprowadzać Klaudię i nie doprowadzić do katastrofy. Opiekuje się sześcioma pacyfikatorami.
* OO Serbinius: przekierowany przez kapitana Fabiana by uratować zaginiętego syna jego mentora.
* SC Hektor 17: savarański statek handlowy; przez chorobę pilota trafił do K1 i zatrudnili tymczasowo rozpieszczonych ludzi niezdolnych do działania na savarańskiej jednostce. Ma biosynta, pacyfikatory, operuje na zerowej energii i przewozi części by odbudować jednostkę 'domową'.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 17
* Dni: 1

## Konflikty

* 1 - Klaudia rozpytuje dyskretnie okoliczne statki używając sigila Fabiana.
    * Tr Z (dane jednostki) +2
    * V: ta jednostka leci upiornie powoli na zerowej energii
* 2 - Jak są blisko Klaudia zarzuca pełny skan. Szuka coś dziwnego na 'H 13'
    * Tr +3
    * V: znalazła sporo dziwnych rzeczy
* 3 - Klaudia wyciąga dziwne promieniowanie magiczne jako podstawę i łączy to z odpowiednim kodeksem -> formalna prośba inspekcji.
    * Tr Z (bo jest podstawa nawet jak słaba) +2
    * VV: tak inspekcja, nie nie będą oszczędzać energii
* 4 - Klaudia rozmawia z chłopakiem jak z niepewnym AI; chce wyrobić sobie opinię czy o coś warto się martwić czy to po prostu dziwny statek
    * Tr (pełny bo jak AI) Z (do tego przywykł) +2
    * XXV: inspekcja Orbitera to dla nich wroga operacja a Fabian MUSI wydobyć Klaudiusza i Mariolę ale Klaudia zrozumiała sytuację
* 5 - Klaudia robi audyt rostera załogi i transportowanych rzeczy
    * TrZ +2
    * VVV: roster jest ok, ale mają nielegalnego biosynta i reaktor do krążownika noktiańskiego
* 6 - Klaudia rozmawia z Perdiusem - o co chodzi i powiedziała co robiła i planuje
    * Tr (niepełny) Z (udowodniła chęć współpracy) +3
    * V: dogadali się. On po prostu chce odbudować swój dom. Odda Klaudii Klaudiusza itp.
* 7 - 
    * 
    * 
* 8 - 
    * 
    * 
* 9 - 
    * 
    * 
* 10 -
    * 
    * 
* 11 -
    * 
    * 

## Kto

### Hektor 17

* **Name**: Perdius, kapitan PLUS Savaranin
    * Ocean: ENCAO:  +-0-0
        * Doskonale radzi sobie pod wpływem silnego stresu
        * Opryskliwy, bezceremonialny i obcesowy
        * Z poczuciem humoru, lubi żartować (wobec bliskich)
    * Wartości:
        * TAK: Face, TAK: Family, TAK: Power, NIE: Universalism, NIE: Security
    * Silnik:
        * TAK: Niezłomna Forteca: A man of dignity and virtue. Nikt nie zmusi mnie do powiedzenia kłamstwa. Mogę stać sam przeciwko wszystkim. Incorruptible. I shall not fall.
        * TAK: Godslayer: pokonać bestię / zniszczyć anomalię zdecydowanie przekraczającą jakiekolwiek możliwości. Pokonać niepokonane.
        * NIE: Pozycja w Grupie: zbudować swoją pozycję i autorytet w swojej rodzinie / klanie. Podnieść SWOJĄ pozycję w SWOJEJ grupie.
* **Name**: Dawid, syn i inżynier Hektora 17
    * Ocean: ENCAO:  0+0-0
        * Zdradliwy; do sukcesu przez nóż w plecach sojusznika
        * Boi się konkretnej rzeczy i unika tej rzeczy za wszelką cenę
    * Wartości: 
        * TAK: Self-direction, TAK: Tradition, TAK: Stimulation, NIE: Face, NIE: Achievement
    * Silnik:
        * TAK: Preserver: "this planet is dying..." - TO jest ważne i piękne i niewidoczne! Muszę mieć pomoc byśmy wszyscy TEGO nie stracili!
        * TAK: Opus Magnum: stworzyć wielkie dzieło, zrobić coś najdoskonalszego czego niekoniecznie zrozumieją inni.
* **Name**: Klaudiusz ORAZ Mariola (jako gestalt), piloci / aktenirzy
    * Ocean: ENCAO:  +--00
        * Jeżeli czegoś chce, weźmie to dla siebie; zazdrosny i zawistny
        * Niecierpliwy, chce TERAZ i nie będzie czekać
        * Egzaltowany, w przejaskrawiony sposób okazuje uczucia
    * Wartości:
        * TAK: Achievement, TAK: Power, TAK: Hedonism, NIE: Tradition, NIE: Family
    * Silnik:
        * TAK: Drama: Kapuleti i Monteki nigdy nie będą żyć w pokoju. Moja grupa przyjaciół też nie ;-). Niech COŚ SIĘ DZIEJE! Paliwo do grupy i plotek.
        * TAK: Daredevil: Takie życie jak mają jest nudne i bezsensowne. Niech się obudzą, niech mnie podziwiają. Niech widzą co można! Muszę działać na krawędzi.

