## Metadane

* title: "Stacja na planecie widmo"
* threads: brak
* motives: ghost-town
* gm: kić
* players: alex, aki, luk, fil

## Kontynuacja
### Kampanijna

* 

### Chronologiczna

* 

## Plan sesji

### Projekt Wizji Sesji

Po wizycie Inferni, Zona Tres ponownie zapadła w sen. Musiała ze względu na braki w żywności.
Kiedy próbujący oszczędzić handlarze przypadkiem przechodzą przez jej bramę, muszą odkryć co się stało i zdecydować, co zrobić w sytuacji, w jakiej się znaleźli.

### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

#### Jak sesja została opisana ludziom

Jesteście wolnymi handlarzami, którzy muszą sami radzić sobie z niebezpieczeństwami, z którymi wiążą się podróże w kosmosie.
Dotychczas mieliście dość dużo szczęścia - nie przewoziliście materiałów dość cennych, by zainteresowali się wami piraci, nie zrobiliście nic, co zwróciłoby na was uwagę wielkich tego świata.
Kiedy opuszczaliście ostatni port, nie udało wam się zrobić biznesu życia, ale mieliście dość, by dotrzeć do następnej stacji. By zaoszczędzić, zamiast zapłacić normalną cenę za przejście przez magiczną bramę pomiędzy sektorami, wasz agent zdobył dla was mapę eteryczną, mającą taniej - i równie bezpiecznie - doprowadzić was do celu. Wspólnie uznaliście, że warto z niej skorzystać - w końcu opłaty są wysokie jak dla drobnego statku kupieckiego. Załadowaliście więc towary i ruszyliście w drogę.
Niestety, mapa doprowadziła was nie tam, gdzie się spodziewaliście i nie macie pojęcia jak wrócić...

### Scena Zero
W scenie zero gracze grają sztuczną inteligencją Noktiańskiego statku, na którego pokład wdzierają się Astorianie. 
Astorianie myśleli, że udało im się wyłączyć AI, ale nie spodziewali się, że Noktianie nie ogranicząją swoich AI.
Udało im się z łatwością pokonać przeciwników i ocalić swój statek...
Konflikt 
Tr +2
X - przeciwnicy podejrzewają, że coś jest nie tak
V - wciągnęli przeciwników w środek statku, gdzie łatwiej będzie się z nimi rozprawić
X - przeciwnicy dostaną posiłki
V - nie będzie poważnych ofiar w żołnierzach
V - uda im się ostatecznie całkowicie usunąć abordaż (ale nikogo nie złapią)

#### Implementacja właściwa

Statek kupiecki nazywa się Januszex
Postacie graczy:

**Arnold** - ochroniarz / ops (myśl: Alfred z Batmana)
W czym postać jest świetna?
•	Ochrona statku i załogi, pułapkami i siłą ognia
•	Uspokaja ludzi i łagodzi konflikty prowadząc do pokojowego rozwiązania
•	Świetnie walczy, poradzi sobie z większością bezpośrednich zagrożeń
Typowe akcje
•	Chcesz kogoś do czegoś przekonać lub uspokoić
o	„Na pewno dojdziemy do porozumienia”
	Wypracuj akceptowalne rozwiązanie.
	Zrozum, o co chodzi drugiej stronie
•	Chcesz chronić załogę
o	„Mojej załodze nic się nie stanie”
	Osłoń zagrożoną osobę
	Odwróć uwagę przeciwnika
•	Chcesz kogoś pokonać w walce
o	„Na Twoim miejscu zrobiłbym inaczej…”
	Pokonaj kogoś w walce wręcz
	Zademonstruj swoją siłę
	Ostrzeliwuj się
Jakie ma zasoby?
•	Dostęp do broni. Różnorodnej, ciężkiej, dziwnej.
•	Elegancki, nienaganny mundur.

**Echo** - medyk / detektyw (myśl: Leonard „Bones” Mc Coy ze Star Treka lub John Watson z Sherlocka)
W czym postać jest świetna?
•	Szybko orientuje się w sytuacjach niewyjaśnionych i dziwnych
•	Potrafi badać osoby, przedmioty i miejsca by wyciągać wnioski
•	Na podstawie posiadanych danych potrafi opracować jak coś zneutralizować
•	Załogowy kucharz
Typowe akcje
•	Diagnozujesz osobę bądź badasz ślady
o	„Przyjrzyjmy się temu, co tu się dzieje…”
	Przeanalizuj ślady / dowody i odrzuć niemożliwe hipotezy
o	„Po wykluczeniu niemożliwego, zostaje nam tylko jedno…”
	Na podstawie zebranych danych, znajdź rzeczy, które nie pasują
o	„Dobrze, co tu się stało…”
	Określ, co wydarzyło się w danym miejscu na podstawie śladów
	Co nie pasuje w danej sytuacji? (na podstawie hipotezy)
•	Określasz co najpewniej się stanie / trzeba zrobić 
o	„To jest grypa. On musi leżeć w łóżku.”
	Dowiedz się, jak pomóc komuś w trudnej sytuacji zdrowotnej
o	„Wytrzymaj jeszcze trochę. To Ci pomoże”
	Podaj stymulanty, które mają wzmocnić konkretne cechy.
o	„Na bazie tego co wiemy, najpewniej…”
	Możesz przewidzieć na podstawie hipotezy i danych co się stanie dalej
	Możesz ekstrapolować (wiemy A, B, C -> najpewniej następnym krokiem jest D)
•	Zauważasz rzeczy, które działają INACZEJ niż powinny
o	„Moment, nasze AI zwykle nikomu nie pyskuje”
	Znasz się na wzorach. To „coś” jest niezgodne ze wzorem.

Jakie ma zasoby?
•	Ambulatorium i zapasy medyczne na statku 

**Elio** - inżynier (myśl: Q z Jamesa Bonda)
W czym postać jest świetna?
•	W trudnym terenie i wśród mechanizmów czuje się jak w domu
•	Projektuje, konstruuje i wykorzystuje maszyny i drony
•	Pilotuje i steruje większością maszyn i pojazdów
Typowe akcje
•	Zarządzasz flotą konfigurowalnych dron
o	„To co, chcecie mieć tam oczy?”
	Wysyła drony mające monitorować sytuację
	Wysyła drony w miejsca, gdzie człowiek nie wejdzie
o	„Musicie coś naprawić przy reaktorze?”
	Przekazuje przedmioty używając dron
	Wykorzystuje drony do naprawy czy działania w miejscach niebezpiecznych
•	Przebudowujesz i naprawiasz mechanizmy i sprzęt
o	„To w sumie by się nadało, trzeba tylko trochę…”
	Chcieliście mieć miotacz gazu z gaśnicy? To macie.
	Budujesz bariery, spawasz drzwi, uniemożliwiasz ‘innym’ wejście
	Budujesz pułapki czy przydatne przedmioty z ‘normalnych rzeczy’
o	„Dziura – zaspawana.”
	Naprawiasz awarie i uszkodzone mechaniczne rzeczy
o	„To maleństwo stać na więcej”
	Jesteś w stanie wyciągnąć WIĘCEJ z istniejącego sprzętu, urządzeń i pojazdów
Jakie ma zasoby
•	Drony, dostosowane do konkretnego celu i mogące wykonać drobne prace
•	Podręczny sprzęt pozwalający na przebudowę przedmiotów i urządzeń
•	Skanery, detektory i narzędzia do obróbki strukturalnej

**Zenek** - handlarz / przemytnik (myśl: Han Solo)
W czym postać jest świetna?
•	Ocena wartości przedmiotów, ich historia
•	Włamania, prześlizgiwanie się, skrytki
•	Ukrywanie kontrabandy
•	Negocjacje
Typowe akcje
•	Opowiedz o przedmiocie
o	„To jest amulet Inków…”
	Określ pochodzenie i historię danego przedmiotu 
•	Pokonujesz stojące na drodze zamki mechaniczne lub elektroniczne
o	„Każdy mógł tu wejść.”
	Uzyskaj dostęp do strzeżonego miejsca
•	Chcesz wynegocjować najlepsze możliwe warunki
o	 „To się opłaci nam wszystkim”
	Przekonaj innych, że warto z Tobą pracować
•	Chcesz ukryć lub odkryć kontrabandę
o	„Tu nikt by nie szukał”
	Odnajdź zamaskowane i ukryte zasoby lub miejsca
Jakie ma zasoby?
•	Poukrywana na pokładzie statku kontrabanda (w granicach rozsądku)


### Sesja Właściwa - impl

Postacie ocknęły się na podłodze mostka swojego statku. Ostanie, co pamiętają to ryk alarmów i to, że zaraz mieli zginąć... A jednak żyją.
Czujniki - a przynajmniej to, co z nich zostało, pokazują, że na zewnątrz statku panuje... atmosfera o_O? Ostatnio lecieli w kosmosie...
Kiedy wyszli ze statku odkryli, że są w ogromnym hangarze. Inżynier wraz z przemytnikiem postanowili dobrać się do widocznego na ścianie terminala i spróbować dowiedzieć się, co się dzieje.
Tr +2
X - tu nie poznają całej historii
V - są w podwodnej bazie, ich statek znajduje się w kapsule ratunkowej, która wyciągnęła go z wody i ocaliła przed zgnieceniem. Baza ma kilka poziomów, może będą  w stanie dowiedzieć się czegoś więcej niżej.

Statek jest bardzo uszkodzony, inżynier nie ma materiałów żeby go naprawić. W dodatku, silniki są zalane. Ogólnie, kicha.

Postanowili pójść na zwiady. Kapitan zostaje na statku.
Odkryli, że korytarz, na który wyszli jest ciemny i zimny, ale szybko zapaliło się łagodne światło i temperatura podniosła się do przyjemnej.
Korytarz poprowadził ich po łuku. Znaleźli coś, co wyglądało jak toalety, ale tabliczka była w dziwnym języku... Handlarzowi kojarzył się trochę z noktiańskim, ale nie do końca... 
Następnie odkryli wejście do dużej hali. Okazał się to być taras widokowy, za którego oknem zapaliły się światła, gdy tylko tam weszli. Mieli możliwość obejrzeć pływającego pod wodą młodego lewiatana i... bramę, przez którą przybyli.
Inżynier zauważył, że taras jest wypakowany różnego rodzaju instrumentami. Dwa z nich wyglądały trochę jak podczerwień, ale jeden pokazywał samą bestię, drugi zaś bestię i coś, co wyglądało jak wyciek z bramy.
Znaleźli również magazyny, ale niestety, nie było tam niczego bardzo przydatnego w naprawach.

Idąc dalej po tym samym łuku, wrócili do drzwi swojego hangaru, znajdując jeszcze wcześniej coś, co wyglądało na windę.

Przez chwilę debatowali, co robić. Czemu nie widać żywego ducha? Gdzie jest AI? 
Postanowili przeszukać niższy poziom.
Kiedy zeszli na drugi poziom, za nimi z wnęk zaczeły wychodzić małe robociki wyglądające trochę jak odkurzacze. Podążają za nimi.

Pierwszym, na co się natknęli to kafeteria / mesa. Całkowicie ogołocona z wszystkiego, co biologiczne. 

Arnold z Echo zabrali się za przeszukanie tego poziomu.

Tr Z +2
V - znaleźli rzeczy osobiste ludzi. To wygląda, jakby ludzie tu byli, a potem... ich nie było.
X - Echo zajrzała pod stół w jednym z pomieszczeń i znalazła słownik obrazkowy. Niestety, nie zauważyła roomby, która wciągnęła większość jej włosów, po czym uciekła do bazy.
X - Echo z Arnoldem zdołali jeszcze znaleźć kartę dostępową, ale uruchomili systemy obronne - stracą dostęp do znacznej części tego poziomu.

W tym czasie Elio i Zenek dobierają się do konsoli na niższym poziomie.

Tr Z +3
Vz - stan AI: jest prąd, ale AI jest... głodna? To nie ma sensu, ale na to wychodzi.
X - wiedzą, gdzie idzie "jedzenie", ale w najeździe roomb na statek tracą zapasy 
+1Vg - gracze wpadają na pomysł "dokarmienia" AI swoim toi toiem - na bezrybiu...
Vz - uda im się wybudzić AI

AI Zony Tres budzi się na niskim (ale już nie krytycznym) poziomie żywności. Gdyby teraz wróciła do hibernacji, ma znów kilka ładnych lat, przedtem zostały jej najwyżej dwa...
W pierwszym momencie odzywa się w dziwnym języku (to stary Noktiański)
Kiedy jedno z nich prosi o rozmowę w ich języku - AI to robi, choć z nieco dziwnym akcentem (Klaudia plus Noktiański...)
Po rozmowie z AI doszli do porozumienia - oni chcą naprawić swój statek, ale AI z przyjemnością pozwoli im tu wrócić i rozwinąć tutaj bazę handlową. 
Z przyjemnością pomoże im w naprawie statku, ale w tej chwili jest to niemożliwe - fabrykatory są uszkodzone.
Postanowili też dokarmić AI częścią cargo (wieźli żywność), żeby nie groziła jej śmierć głodowa.

Inżynier zabrał się za naprawę.

Tr Z +3
Vz - dzięki pomocy AI i swojego warsztatu na statku, udało mu się przeprowadzić podstawowe naprawy
V - fabrykatory są w pełni naprawione i podziałają przez jakiś czas
V - nawiązanie pozytywnej relacji z AI (dotąd traktowała ich jako tymczasowych gości, teraz jest nastawiona bardziej przyjaźnie)

Następnie zabrali się za naprawę statku
Tr Z +3
X - muszą naruszyć zasoby bazy (konieczne będzie uzupełnienie zapasu materii)
V - udaje się osuszyć i naprawić silniki Januszexa
V - kadłub jest znów sprawny (wciąż brzydki, ale się nie rozleci)
V - zasilanie jest znów sprawne.

Potem AI buduje dla nich mapę eteryczną, by mogli wrócić (nie mają pojęcia, że to coś dziwnego)

Tr +4
V - mapa działa
X - po powrocie wzbudzą nadmierne zainteresowanie  (szemrani i takie tam)
V - mapa ma 100% na powrót do bazy i kilka wykorzystań
X - będą musieli sprzedać statek

AI pozwoliła im również przeszukać swoje korytarze za rzeczami na handel. Szaber mocno
Tr +4
V - uda im się zrekompensować cargo i uzykać niewielki zysk
X - wzbudzą zainteresowanie możnych
X - ktoś zaginie w Eterze korzystając z kopii ich mapy - cios w ich reputację



## Streszczenie

Przez bramę przy uśpionej z głodu Zonie Tres przeszedł Januszex, pechowy statek pomniejszych handlarzy. Odkrywszy pustą bazę bez śladu substancji biologicznych, stopniowo doszli do tego, co się stało i dokarmili głodującą BIA. Ta, po tym jak naprawili dla niej fabrykatory stworzyła dla nich coś bardzo rzadkiego - kilkakrotnego użytku mapę eteryczną, która na pewno doprowadzi ich z powrotem. Postanowili spróbować uczynić z Zony Tres nowy hub handlowy.

## Progresja


## Zasługi


## Frakcje


## Fakty Lokalizacji


## Lokalizacje


## Czas

* Opóźnienie: 
* Dni: 
