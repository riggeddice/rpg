## Metadane

* title: "Orbiter, Nihilus i ruch oporu w Nativis"
* threads: kidiron-zbawca-nativis, neikatianska-gloria-saviripatel
* motives: rekonstrukcja-ruiny, machinacje-syndykatu-aureliona, symbol-lepszego-jutra, sojusznik-bardzo-dziwny, gry-polityczne, system-moloch, birutan
* gm: żółw
* players: fox, kapsel

## Kontynuacja
### Kampanijna

* [230726 - Korkoran płaci cenę za Nativis](230726-korkoran-placi-cene-za-nativis)

### Chronologiczna

* [230726 - Korkoran płaci cenę za Nativis](230726-korkoran-placi-cene-za-nativis)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * "Sound Holic - Requiem For A Nightmare"
* CEL: 
    * Highlv
        * Poukładanie przyszłości - jak rozwiążemy problem z Arkologią
    * Tactical
        * ?

### Co się stało i co wiemy

* Frakcje Arkologii
    * **Lobrak**
        * impuls: wyjść z tego cało, scorruptować Eustachego, przejąć kontrolę lub się wycofać
            * Oddział bojowy do ataku na farmy
            * Neuroobroże
            * Siły Syndykatu zdolne do zniszczenia Arkologii, łącznie z zamaskowaną artylerią w piaskach
        * ograniczenia:
            * jeśli tego nie rozwiąże, jest po nim
    * **Lertysowie**
        * impuls: naprawa szkód po Kidironie
    * **Regentka Kalia**
        * impuls: integrate and rebuild, ochrona arkologii
    * **Rebelianci**
        * impuls: atak na LITL i Infernię
            * atak z zaskoczenia
        * slogan: "KIDIRON MUST DIE! Infernia must die!!!"
    * **Hełmy Draglina**
        * impuls: odzyskać Kidirona
        * slogan: "Tylko Kidiron. PRAWDZIWY Kidiron."

### Co się stanie (what will happen)

* Cel strategiczny: "Jak się pozbyć Syndykatu z Arkologii?"
    * Kalia rozważa sojusz, "Kidiron nas uratuje".
* Cele poboczne
    * Zabezpieczyć Arkologię (handel, części, naprawa)
    * Ustabilizować Arkologię (ludzie są w gniewie, Kidiron wtf)
    * Infiltratorzy Syndykatu w Arkologii
    * Usunąć grupkę "Piratów" polujących na rzeczy opuszczające Nativis
* F1:
    * Ludzie żądają wydania Laurencjusza i Feliksa (16)
    * Ludzie są podburzani przeciwko Inferni (bo piraci). Chcą ukarania winnych.
    * Infernia rekomenduje Izabelę
    * Kalia nie wie czy nie sojuszować z Lobrakiem bo Kidiron uratuje i Arkologia przetrwa
    * Draglin chce uderzyć żelazną pięścią, Marius chce zniszczyć Hełmy

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Marcel Draglin: oficer Kidirona dowodzący Czarnymi Hełmami, dumny "pies Kidirona"
    * (ENCAO: --+00 |Bezbarwny, bez osobowości;;Bezkompromisowy i bezwzględny;;Skupiony na wyglądzie| VALS: Family (Kidiron), Achievement >> Hedonism, Humility| DRIVE: Lokalny społecznik)
    * "Nativis będzie bezpieczna. A jedynym bezpieczeństwem jest Kidiron."
    * Najwierniejszy z wiernych, wspiera Rafała Kidirona do śmierci a nawet potem. Jego jedyna lojalność jest wobec Rafała Kidirona. TAK, to jego natura a nie jakaś forma kontroli.
* Marzena Marius: była komodor Orbitera, która zdecydowała się na zbudowanie lepszego życia na Neikatis (ma ok. 15 doświadczonych osób); wie o birutanach
    * OCEAN: (C+A+): Emanuje spokojem i pewnością siebie, potrafi zjednać sobie tłumy swoją retoryką i charyzmą. "gdy z nią rozmawiasz, czujesz jej głęboką wiarę w jej przekonania"
    * VALS: (Power, Security): Jej zdaniem kontrakt społeczny ma stabilizować systemy "Każdy system jest tyle wart, ile stabilności może przynieść."
    * Core Wound - Lie: "brak zdecydowanego działania doprowadził już do tej samej tragedii" - "Moja wizja to jedyna droga do pokoju"
    * Styl: nie unika trudnych tematów, podaje je w przemyślany, logiczny sposób. Jej retoryka koncentruje się na potrzebie jedności i silnego kierownictwa
    * metakultura: Atarien: "prawdziwa siła leży w jedności pod właściwym kierownictwem"

### Scena Zero - impl

.

### Sesja Właściwa - impl

Co wiemy:

* Arkologia Nativis jest w złej formie. Trzeba szybko zmontować jakieś elementy wsparcia.
* Syndykat Aureliona jest niedaleko, na orbicie. Mają carrier-class.
* Ruch Oporu przeciw Kidironowi obraca się przeciw Inferni
* Mamy Laurencjusza, którego można wydać ludziom.

Niedługo przylecą na negocjacje. Ardilla proponuje, żeby proponować czy nie zrobić czegoś ZANIM przylecą. Jej pomysłem jest pokazanie Syndykatowi że ta Arkologia jest Skażona Nihilusem i nie warto jej wchłaniać. A że Infernia jest Skażona Nihilusem, mamy Ralfa... można zrobić grę/serial horrorowe. Nagle widzą tego jednego typa co nie zachowuje się normalnie. CULT IN THE TOWN.

Ale czy Izabella będzie potrzebna? Zdaniem Eustachego - warto ją pokazać jako Skażoną. Pokazać jej Pustkę. Ardilli nie podoba się ten pomysł czy plan, ale bardzo się podoba Eustachemu. Za to Eustachy zrzuca na Ardillę ciężar władzy. Wyszło, że Eustachy jest krwawy. Więc Eustachy chce zostać WŚCIEKŁYM PSEM Ardilli. Ardilla przytula drzewa, a jeśli coś jest bardzo nie tak to jest Eustachy.

Ostatni raport nt Izabelli od Lobraka 

* Izabella wysłała "jestem, dezorganizuję itp"
* Eustachy z Lobrakiem "a, zniszczony umysł"

Oki, zostawiamy "kult Nihilusa" jako pomysł przestraszenie Syndykatu. Na potem.

Kalia prosi o pomoc Ardillę. O konsultacje.

* Kalia: Mamy problem. Ruch oporu się... rozruszał.
* Ardilla: Aha?
* Kalia: Żądają wydania Laurencjusza Kidirona, Feliksa Kidirona oraz tych agentów Inferni którzy krzywdzili ludzi. Chcą ich osądzić.
* Ardilla: Osądzić jak zwykle się osądza ludzi któryz nie pasują?
* Kalia: Nie wykluczam. Mają naprawdę, NAPRAWDĘ charyzmatycznego dowódcę. To nowa osoba, nie wiem skąd ją mają. Ale nie wskazuje na Aureliona. Nazywa się Marzena Marius.
* Ardilla: Dane Kidirona o Marzenie
    * jest to była komodor Orbitera
    * opuściła Orbiter po skandalu (nie do końca wiadomo, JEJ zdaniem została zdradzona przez Orbiter)
    * dane Kidirona: jest idealistką, ale która nie boi się twardej ręki
    * w przeszłości była oficerem taktycznym

Eustchy bez problemu może ściągnąć Lycoris Kidiron (nie ściągamy). (Draglin zmiażdżyć ruch oporu Hełmami). Idziemy w tą stronę - odwracamy uwagę Draglina Kidironem i "ma pilnować" na Inferni.

* Draglin: komendancie, czemu chciałeś się ze mną widzieć?
* Eustachy: musisz iść robić rzeczy
* Draglin: czyli co? Kogo zabijamy?
* Eustachy: nikogo. Pierwsza sprawa - przekazuję władzę siostrze
* Draglin: chyba Cię popierdoliło
* Eustachy: nie
* Draglin: (tłumaczy jak dziecku) ona ma najlepszą opinię. Jeśli Ty trzymasz władzę, to ona jest czysta i rządzisz zza jej pleców. I czekamy na Rafała Kidirona aż wróci.
* Draglin: ona jest jedyną czystą z rodu.
* Eustachy: Nie dostaje pełni władzy tylko... Ardilla dostanie ustawodawczą a ja wykonawczą
* Draglin: (spojrzał tak dziwnie) wiesz, na Twoim miejscu ja bym ją zamknął w szafce. Wszystko robimy sami, potem 'wygodnie' wraca komendant Kidiron, wyciąga Ardillę i my dostajemy po łapkach, nie wiem, 2 dni więzienia i wszystko wraca do normy i ona jest czystą naiwną wiewióreczką jak do tej pory
* Eustachy: to wersja ostateczna. Ardilla ma sumienie. My go nie mamy.
* Draglin: gdyby komendant Kidiron potrzebował bym miał sumienie to by mi je przydzielił. Nie potrzebujemy sumienia.
* Eustachy: jak będą zbrodnie, spadną na nas. Ardilla przecież nie zrobiłaby takich rzeczy. A ja muszę mieć czas i możliwości działania.
* Draglin: rozmawiałeś z Ardillą i Kalią i się zgodziły?
* Eustachy: Kalia nic nie wie, Ardilla jest za.

Tr Z+3:

* X: Draglin mówi 'nie'. Infernia ma być niewinna. Z woli Kidirona.
* X: Draglin chce wziąć na siebie i na Hełmy wszystkie zbrodnie. Eustachy nie może działać swoimi siłami.
* X: Draglin nie da odwrócić swojej uwagi. Masz Draglina cały czas. Bo nie jest pewny czy Wola Kidirona jest wykonywana prawidłowo.
* V: Draglin WIE, że Syndykat chciał zabić Kidirona w związku z tym NIGDY nie stanie po ich stronie.
    * jak długo Infernia walczy z Syndykatem, tak długo Draglin jest w sojuszu

Zgodnie z danymi zebranymi przez Infernię, Carrier zaatakował więcej niż tylko to jedno miejsce. Jakieś CESy też były zaatakowane przez Syndykat. Nie są zajęci tylko Nativis...

Są trzy opcje:

1. Niech Syndykat zostanie odstraszony lub zniszczony           <-- wybór Ardilli oraz Eustachy
    1. strategia na Nihilusa
    2. strategia na Orbiter         <-- bezpieczniejsza strategia
2. Niech Syndykat wspiera arkologię
3. Niech Syndykat zostawi arkologię ale tępi wszystko inne

W danych Kidirona jest jednoznaczna wiadomość. Jest placówka, całkiem niedaleko, placówka badawcza `Cognitio Nexus`. I tam znajduje się system do dalekosiężnej komunikacji. Jest to dobrze broniona baza. Według danych Inferni był atak na tą bazę ze strony Syndykatu. Nieudany.

Co możemy zrobić?

1. Cognitio Nexus - tam powinien być sygnał
2. Izabella - to jest plan B

Ardilla kontaktuje się z Marzeną Marius. Jest w eleganckim mundurze. Wie jak usiąść i jak wyglądać. Ale też ma odpowiednie blizny.

* Marzena: ko-regentka Ardilla Korkoran. Jedyna, która nie ma krwi na rękach.
* Ardilla: pani Marzena Marius. Osoba, która przywłaszczyła sobie powstającą rebelię.
* Marzena: niczego nie przywłaszczałam. Poprosili mnie o pomoc.
* Ardilla: rozumiem. Chcę się dogadać. Sytuacja robi się gorsza cały czas. Słyszałam jakie macie cele - wydanie ludzi którzy zawinili w ostatnim czasie. Co chcecie z nimi zrobić?
* Marzena: chcemy ich osądzić, acz zdajemy sobie sprawę z sytuacji
* Ardilla: rozumiem Laurencjusz, rzucał się przez ostatni czas. Rozumiem załogantów Inferni kontrolowanych... ale czemu Feliks?
* Marzena: dlaczego wpływał na Prometeusa? Czemu działał niezgodnie z prawami? Ustanowionymi przez własnego wujka?
* Ardilla: miała pani kiedyś 16 lat? Nie chciała by dziewczyna popełniła samobójstwo
* Marzena: w takim razie zostanie osądzony uczciwie i nic mu się nie stanie
* Ardilla: kto będzie prowadził sąd?
* Marzena: ja
* Ardilla: co tak po prostu? XD
* Marzena: tak
* Marzena: za długo Kidironowie byli bezkarni. Każdemu sprawiedliwość, ale to nie znaczy lincz
* Ardilla: na jakiej podstawie?
* Marzena: (mina urażonej niewinności) Ależ skąd, prawa arkologii. Tej, w której jesteśmy. Nie uzurpuję sobie ustawodawstwa.
* Ardilla: masz wykształcenie prawnicze?
* Marzena: mam prawnika. Niejednego.
* Marzena: czemu Ty się boisz sprawiedliwości arkologii? Jeśli nic nie zrobili, sąd ich oczyści

MM jest bardzo przeciwna Syndykatowi. I wie, że za zdradę jest kara śmierci. I wie, że Kidiron nie zabiłby Laurencjusza, dałby mu drugą szansę.

* Marzena: chyba, że uważasz, że Infernia jest ponad prawem? Twój wujek uważał inaczej. Był jedną z NAJLEPSZYCH osób w tej arkologii.
* Ardilla: gdybyśmy się nie musieli z waszym powstaniem, wujek by dalej mógł
* Marzena: naprawdę współczuję. Ale jego nie ma. I ktoś musi zająć jego miejsce. Jako sumienie tej arkologii. Jako nadzieja.
* Ardilla: bardzo fortunną rolę sobie wybrałaś. Gdzie sąd?
* Marzena: (dała sensowną lokalizację na uboczu która nie jest zniszczona i może być chroniona; jakiś 'supermarket')
* Marzena: wiem, że nie jesteście tak nierozsądni by próbować zniszczyć ruch oporu militarnie. Wysłałoby to dobry sygnał. Ludzie DALEJ wierzą w Infernię. A ja pokażę, czym NAPRAWDĘ jesteście. Albo jesteście tacy jak chciał Wasz wujek, albo tacy jak Kidiron.
* Ardilla: skonsultuję to z regentką
* Marzena: dziękuję za audiencję (lekki uśmiech). Byłaś po naszej stronie. Zrób to, co najlepsze dla arkologii.

Ruch Oporu chce wygrać arkologię, nie zniszczyć Infernię. Ich ruchy są w formie 'fortyfikacja, rząd dusz itp'. Ona kontynuuje to co zaczął Laurencjusz ale robi to niemilitarnie. W końcu WSZYSCY mieli dość Kidirona.

## Streszczenie

Przed Nativis problem - nie są w stanie zniszczyć ani odeprzeć Syndykatu. Więc pozostaje im wezwać Orbiter, ale jak? Albo przez Izabellę Skażoną Nihilusem albo przez Cognitio Nexus (gdzie jest komunikacja). A tymczasem Marzena Marius, kiedyś z Orbitera przejęła dowodzenie nad ruchem oporu anty-Kidironowym i niestety rozgrywa rewelacyjnie politykę. Draglin chroni za wszelką cenę reputację Ardilli i samą Ardillę, też przeciw Eustachemu XD.

## Progresja

* .

## Zasługi

* Ardilla Korkoran: jej pierwszy plan - udawać kult Nihilusa - sama złamała (zbyt niebezpieczne). Próbowała wybadać ruchy Aurory Nativis i rozmawiała z Marzeną Marius, ale okazało się, że eks-komodor Orbitera jest po prostu politycznie LEPSZA od Ardilli. Ardilla odroczyła działania ruchu oporu.
* Eustachy Korkoran: chce zrzucić władzę ustawodawczą i dowodzenie na Ardillę (czemu sprzeciwia się Draglin) i chce by ona była jego sumieniem. W końcu podjął decyzję - odpychamy Syndykat, zdobywamy wsparcie Orbitera.
* Marcel Draglin: blokuje ruchy Eustachego by zrzucić politykę publicznie na Ardillę i Kalię; jego zdaniem on oraz Eustachy są od 'krwawej roboty' i dziewczyny mają być symbolem czegoś lepszego. Lepszy politycznie niż się wydawało, bo nauczył się czegoś od Kidirona. Największy sojusznik Ardilli i Kalii w arkologii, mimo, że obie go nie lubią i nim gardzą. Infernia ma być niewinna. Z woli Kidirona.
* Kalia Awiter: konsultuje się z Ardillą w sprawie tego jak pokonać Marzenę Marius (z ruchu oporu). Jako regentka tymczasowo jest symbolem wszystkiego co dobre i piękne.
* Marzena Marius: niesamowicie dobry polityk i taktyk, eks-Orbiter i idealistka; zażądała wydania przez Infernię ludzi z Inferni co skrzywdzili niewinnych (eks=piratów) oraz Laurencjusza i Feliksa Kidironów. Spokojnie umacnia rząd dusz, fortyfikuje pozycje i pozycjonuje się jako sędzia. Ardilla nie jest w stanie jej łatwo politycznie wymanewrować.

## Frakcje

* Aurora Nativis: sformowana z ruchu oporu anty-Kidironowego i przejęta przez Marzenę Marius (kiedyś z Orbitera), zdobywa rząd dusz przeciw Inferni Nativis
* Infernia Nativis: niekwestionowani przywódcy Arkologii Nativis, acz spadają na nich zbrodnie Kidirona i te które popełnili (m.in. po działaniach Saviripatel)

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Nativis

## Czas

* Opóźnienie: 0
* Dni: 1
