## Metadane

* title: "Waśń o ryby w Majkłapcu"
* threads: rekiny-a-akademia
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [211123 - Odbudowa według Justyniana](211123-odbudowa-wedlug-justyniana)

### Chronologiczna

* [211123 - Odbudowa według Justyniana](211123-odbudowa-wedlug-justyniana)

## Plan sesji

### Theme & Vision

* Zmęczenie tym wszystkim
    * "Mafia też jest zmęczona i chce na emeryturę"
* Daniel się nie poddaje, chce proaktywnie znaleźć sposób na odżywienie sentisieci

### Ważne postacie

* ?

### Co się wydarzyło

* Stella chce przejść na emeryturę. Chce mieć przychody. Trafia różne biznesy, niewielkie, by mieć niewielką spokojną sumkę. Wybrała Majkłapiec i kociarnię Zawtrak.
* Zawtrak nie chcieli z nią współpracować, więc uwolniła kilka kotów i im dała agresor. Poszły ryby i gęsi.
* Farma Krecik podejrzewa Myriad jako kolejną odsłonę tego, jakie mają być proporcje w paprykarzu.
* Daniel szuka jak pomóc sentisieci Terienak. Znalazł potencjalnie kogoś na AMZ, ale by się przypodobać - chce pomóc rozwiązać plotki z rybami w Majkłapcu. Słyszy o "noktianach chcących by Orbiter chciał żyć wegetariańsko". Ale bardzo nie chcą w to mieszać terminusów bo ci mają dość... poza Ksenią. Więc on to rozwiąże...
* ...ale potrzebuje wsparcia Karo. Gdzie dwa ścigacze tam nie jeden.

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Daniel odwiedził Karolinę. Poprosił siorkę o pomoc. "Pomożesz mi znaleźć mordercę ryb?". Wyjaśnił - jest taki Majkłapiec. I tam ktoś zabił ryby. Na jednej z farm ryb. Podobno to waśń noktiańsko-astoriańska czy coś. Daniel jest sfrustrowany, brzmi to głupio. A od początku:

* Jest taka laska. Na AMZ. Ona może coś wiedzieć o sentisieci. Jakieś inne techniki. Sposoby.
* Przejmuje się tym co się dzieje w Majkłapcu.
* Chcę jej pomóc by powiedziała. Pomogła. Może coś da się zrobić. Może.

Daniel nie jest przekonany, że to pomoże. Ale nie ma lepszego pomysłu. AMZ jest jego pomysłem - tam ktoś MUSI móc coś wiedzieć. To w końcu Akademia Magii. Więc Daniel mówi, że chce rozwiązać problem z zabijanymi rybami, ale sam nie wie jak. Karolina zauważyła, że się nie zna na rybach. Daniel - jest w stanie podłożyć czujki, pułapki, zrobić coś. Nie mają tam magów szczególnie. Jest jeden konstruminus. Daniel i Karolina to force multiplier. Więc coś może się wymyśli. A dzięki temu - nawet jak się nie uda - on ma pretekst by ta laska mu pomogła.

Co na razie Daniel wie:

* Doszło do masakry ryb w stawie, chronionym.
* Zginęło też sporo gęsi. Wszystko na jednej farmie.
* Ta laska jest zmartwiona, bo uwielbia paprykarz stamtąd. Podobno tam są dwie farmy - jedna wege (noktiańska), druga ryby i gęsi (astoriańska). I one się wiecznie kłócą która więcej składników ma dać
* I to jest jej ulubiony paprykarz - Paprykarz Z Majkłapca. I martwi się, że to się totalnie spieprzy po tej eksterminacji.
* Lokalsi nie chcą tego eskalować, bo terminusi są już zirytowani waśniami lokalsów. Jest tylko jedna terminuska która się tym interesują a jej nie chcą - Ksenia Kirallen.
    * Nie chcą Ksenii, bo była tam raz 3 miesiące temu i odechciało im się waśni na długo... rozwiązała problem, bardzo brutalnie.

Daniel powiedział, że ma kontakt do laski która jest córką kolesia któremu wymarły ryby. Ale jakkolwiek może próbować samemu, uważa, że Karo jest tu bardzo przydatna. On zajmie się laską, Karo będzie mózgiem operacji. Karo pyta - czyli znaleźć przyczynę? Daniel potwierdził. Spotka się w trójkę z tą laską.

Majkłapiec. 12 km od Zaczęstwa. Niewielka mieścina. Daniel i Karolina na farmie, z nimi Genowefa Krecik - córka i następczyni potęgi Krecików z Majkłapca. 

Genowefa mówi co wie i co uważa.

* To na pewno robota "noktian". Zawsze się kłócą o to jaki będzie skład konserw. Jeśli nie ma ryb i gęsi (a sporo jest poranionych i pozabijanych), to konserwy będą słabe. A noktianie mieli nadurodzaj i co zrobią ze swoimi grzybami i ryżem?
* Wszystko wskazuje na to, że to noktianie, bo oni mają kilka dobrze wytresowanych kotów bojowych. Rasa to kot-pacyfikator. Od Pacyfiki. Szkolone do obrony, eksportowane na statki kosmiczne. Niedaleko jest hodowla, kociarnia Zawtrak. A rany na gęsiach i rybach wskazuje na atak kotami. A jej ojciec przyjaźni się z Iwanem Zawtrakiem - Iwan nie zrobiłby im tego.
* Mają kamery itp. Na kamerach faktycznie widać koty. To są pacyfikatory. Karo spytała - czy te koty są jakkolwiek powiązane z noktianami? To te same? Genowefa nie wie. Nie odwiedza noktian. Chwilowo Genowefa zainwestowała w maga rolniczego, kogoś, kto leczy ranne zwierzaki. Na szczęście znalazła kogoś taniego. Jak Karo chce się coś dowiedzieć i upewnić się że to koty, może z nim pogadać.

Ku wielkiemu zdziwieniu Karo, tym "magiem rolniczym" okazał się być Paweł Szprotka. AMZ, protegowany Olgi Myszeczki. Faktycznie, opatruje, dba, podlecza ryby i gęsi.

Szprotka się przestraszył widząc Karolinę. Karo widzi, że on się naprawdę przejmuje cierpieniem zwierząt. Zdaniem Szprotki to NIE MOŻE być robota noktian - wie, że oni mają koty, jako weterynarz był u nich już raz a Ignacy czasem kupuje od nich grzyby. Karo się zorientowała, że przecież on widział te koty. Pokazała Szprotce nagranie i on uznał, że coś jest nie tak. Coś jest nie tak na tym nagraniu.

Paweł Szprotka próbuje dojść do tego co jest nie tak z tymi kotami na nagraniu

TrZ+2:

* V: Paweł pokazuje Karo niektóre ruchy tych kotów. "On się nie bawi. On chce zabić. Chce zranić. Ten kot jest opętany nienawiścią. Koty tak nie mają. Co im zrobili?"

Paweł jest oburzony i przestraszony. Pacyfikatory służą do tego, by np. na statkach kosmicznych polować na małe anomalie. To bardzo waleczne koty. Żaden szczur nie ma szans. Ale nie są _nienawistne_. Ktoś bardzo zaryzykował, bo te koty są po prostu niebezpieczne. "Możesz mi znaleźć jednego z tych kotów? Chciałbym go przebadać...". Karo słusznie zauważyła - atak był dwa dni temu a ona NIE TROPI KOTÓW. Karo pyta, czy Paweł zna hodowców z Zawtraka? Nie. Oni mają innego weterynarza. Droższego.

Karo spytała Pawła, czy znalazł jakieś futro. Nie szukał. Karo na podstawie nagrania wie, gdzie były koty i gdzie walczyły - wyjaśniła Danielowi problem i poprosiła, by ten znalazł kocie futro. Daniel spojrzał na siorkę ze spojrzeniem "odpierniczyło Ci, siorka". Ona zauważyła, że to był jego pomysł. Daniel, lepszy w tropieniu niż Karo, westchnął i zabrał się do szukania kociego futra...

ExZM+2:

* Xz: Daniel szukając futra szedł za śladami. Przeszedł przez teren gdzie były koty. Ale kot przejdzie nad stawem z delikatną osłoną, Daniel nie. PLUM!
* X: Opierdol od Genowefy, że dewastuje mienie itp. Daniel się broni, że szuka kociego futra. Opierdol numer dwa.
* Vz: Daniel znalazł kocie futro. Ma próbki dla Karo i ma nadzieję, że to było tego warte...

Dla Karo było. Ma futro i okazję pośmiania się z brata, który bierze na siebie ogień Genowefy i próbuje ją uspokoić. Ona chce by płacił, on bardzo nie chce.

Karo mając futro idzie do Pawła Szprotki - co może powiedzieć jej o tym kocie? Czym był nafaszerowany? Albo jak go znaleźć? Koci skaner. Paweł się zastanowił; sam nie ma na to możliwości. Ale Karo zauważyła, że Daniel tak. Daniel to seeker, Paweł to weterynarz. Razem mogą to zrobić. Zbudować koci skaner, radar znajdujący czterołapczaki typu pacyfikator tępiące ryby.

Paweł i Daniel (który się uwolnił od Genowefy) składają czar razem. Daniel artefaktyzuje detektor w podrdzewiałej podkowie, Paweł dostarcza "jądra" detektora.

TrZM+3, 5O (5 nie 3, bo nie są kompatybilni ze sobą):

* X: Genowefa jest święcie przekonana, że Daniel nic nie umie. Jej zdanie nie będzie łatwe do zmiany. Mózgiem jest Karo a skillem Paweł. Daniel po prostu wygląda.
* Vz: Dzięki kociej sierści udało się zbudować perfekcyjny detektor kotów. Oczywiście, najwięcej kotów pokazuje w kociarni. Ale pokazuje też koty w lesie, na niedalekim terenie.
* Vm: Paweł i Daniel sformowali jeszcze perfekcyjny wabik, zamknięty w bardzo sparszywiałej myszce z jednym okiem z guzika. Jest to ŚWIETNY wabik na koty tej klasy.

Karo ma wabik na koty. Ma lokalizację kotów. Teraz potrzebuje odpowiednich klatek. "Jak mocne pazury to ma?" "Nie przebije się przez ścigacz czy przyczepę, przebije się przez skórę czy kombinezon".

Czas pożyczyć klatki od hodowcy kotów. I nie tylko. Ale to dobry pretekst ;-).

Karo i Daniel (Paweł wraca do opatrywania ryb i gęsi - w końcu zlecenie) udają się do Zawtraka. Po chwili rozmontowywania zabezpieczeń (pełna moc aktywna) wpuścił ją Iwan. Osobiście. Wow, ale zabezpieczenia. Iwan jest nieufny ale życzliwy. Gdy Karo gadała z Iwanem, Daniel strzelił "to Twoje koty uciekły, tak?", za co zarobił sójkę w żebra od Karo (nieukrywaną). Iwan się przyznał. Zapłaci im jak znajdą koty i wyłapią. Spytał, czy gdzieś koty problemy zrobiły. Tak. Iwan jest sfrustrowany. 

Daniel próbuje ocenić, czy Iwan udaje czy naprawdę przejmuje się tą sytuacją. O co tu chodzi? Daniel by się dowiedzieć generuje presję.

Tr+2+3O (presja - sukces, ALE):

* X: Daniel przesadził z presją i krzykami i wszedł w obszar Iwana, zagrażając mu. Iwan odepchnął go solidnie na ścianę. Daniel aż się zapalił do walki, Karo go uspokoiła.
* V: Daniel wie, że Iwan mówi prawdę. Coś ukrywa, ale NIE CHCIAŁ by koty wyrządziły komuś krzywdę i MARTWI SIĘ i o sąsiadów i o koty. Cokolwiek się stało, Daniel tego nie chciał

Karo do Iwana: "słuchaj, wiem jak to zabrzmi, ale podpadłeś komuś ostatnio?". Czujne oko Karo wykryło sigile z lapisu na ścianach. Zabezpieczenie przed magią. Karo wie, że mało skuteczne. Iwan "jesteś terminuską?" Daniel się roześmiał, Karo też. Karo: "jak chcesz, możesz dostać Ksenię". Daniel wyjaśnił "jesteśmy najlepszymi Rekinami w okolicy. Robimy co chcemy."

Iwan westchnął. "Te szczeniaki z Aurum?" Daniel się nastroszył. Karo "może i szczeniaki, ale wiedz że te sigile są bezużyteczne". I powiedziała "nara". Iwan "a klatki chcecie?" Karo "nie chcesz ze szczeniakami, poradzimy sobie inaczej. Więcej kombinowania, ale..."

TrZ+3+3O (presja - sukces, ALE):

* Vz: Iwan: "kurde, wy nie pracujecie z mafią?" z takim zdziwieniem, wyraźnie jest zagubiony.
    * Karo: "po jakiego grzyba?"
    * Iwan: "tak słyszałem"
    * Karo: "a ja słyszałam, że koty żrą marchewkę" - Iwan się zapluł z oburzenia
    * Iwan: "czyli wy ani z terminusami ani z mafią"
    * Karo: "ani, ani"
    * Iwan: "a macie bezpieczne miejsce?" - wyraźnie zafrasowany
    * Karo: "mamy bezpieczne i przed terminusami i mafią"
    * Iwan: "dobra. Znajdźcie koty, weźcie te klatki, macie ID chipów, wróćcie, pogadamy. Zapłacę."
    * Karo: "są szkolone?"
    * Iwan: "nie wiem co im zrobili i jak długo to będzie trwało. Jak co, są szkolone. Nauczę was na szybko komend."
* Xz: Karo za cholerę nie złapała komend. Po prostu to nie działa jak Karo.
* X: Daniel też nie złapał komend. Te komendy po prostu są totalnie od czapy.

Karo spojrzała na Daniela jak Iwan wyjaśnia, dostała to samo spojrzenie, Iwan dostał to samo spojrzenie i zrozumiał. Machnął ręką. "Dobra, macie ID chipów. Macie mój skaner. Znajdźcie i złapcie." Karo - "daj jednego z tych co zostały". Zadziałało, Iwan kociaka odwołał.

Karo zaproponowała Danielowi - łączą siły. Nagrać (złapać komendę) Iwana, by kot zostawił wabik. Nawet jak nie zadziała, może pomóc.

TpZM+3:

* V: Nagrana komenda Iwana, solidny artefakt.
* X: Komenda wali z siłą DUŻEJ ILOŚCI DECYBELI.
* Vm: ...ale działa. To plus wabik zdobędzie futrzaki.

Czyli Karo i Daniel mają: klatki, lokalizację, wabik, stopper kotów, ścigacze. Czas zapolować na futra.

TrZ+4+3Vg (reprezentują absolutną przewagę):

* V: udało im się wyłapać koty - wszystkie żyjące
* V: ...jeszcze tego dnia
* V: te, które by nie przeżyły da się jeszcze uratować - a Paweł jest pod ręką. A Iwan widzi, że dzięki Rekinom udało się te koty poratować. Z 12 kotów uratować się da aż 10.
* V: Iwan jest im wdzięczny i zaufał. Powie wszystko.

Iwan za namową Karo ściągnął Szprotkę. Niech Paweł pomoże kotom. Sam też pomaga kotom, zna się na rzeczy. I zaczął wyjaśniać Karo i Danielowi co tu się dzieje.

* Jest mafia. Mafia chce od niego haracz. Oczywiście powiedział im co o tym myśli - Kajrat i Grzymość są w końcu złamani.
* Niestety, mafia ma magów. I tego on się nie spodziewał. Przyszli na ostro. Wykradli koty i je zatruli.
* Iwan nie chce wzywać terminusów, bo oni mafii nie są w stanie zniszczyć a on tu zostanie i ma rodzinę.
* Iwan poważnie rozważa opłacanie się mafii. Nie wie jak to zrobić inaczej. Cieszy się tylko, że nikomu nic poważnego się nie stało i odzyskał tyle kotów ile mógł. On je serio lubi.

Karo powiedziała jak wyszedł problem. Pokazała nagranie. Iwan powiedział, że on pójdzie do Krecików, przeprosi i załagodzi. Karo nie czuje się na siłach, więc cieszy się z decyzji Iwana.

Daniel: "Ale mafia przecież tak nie robi. Oni nie zajmowali się małymi biznesami." Karo zauważyła, że futrzaki są stosunkowo niebezpieczne. Wolałaby znaleźć tego kto to zrobił. Czy Iwan ma jakieś nagrania? Iwan potwierdził, ma. Ale przeciwnik jest kompetentny. Nie dała się nagrać. To starsza kobieta. Głos, postura. Ona dowodzi.

Karo -> Daniel, po hipernecie: "nie pasuje do mafii?" Daniel na to, że mafia którą ON zna ma trochę większe ambicje. To może być jakaś mała odnoga mafii. Ale nie sądzi, by to była cała mafia. Czy takie siły jak te z którymi zwykle Daniel ma do czynienia. Karo się nie babrze w mafię.

Daniel: "Myślisz, że tu można użyć tej, Sowińskiej?" Karo ma inny pomysł - Torszecki. On jest bardziej w mafię, w informacje itp. Torszecki może pomóc rozwiązać ten problem.

Czyli czas na Torszeckiego.

"Dla siebie bym nie prosiła, ale możesz pomóc. Jeśli jesteś zainteresowany, daj znać.". Torszecki szybko odpowiedział: "tien Terienak, jak mogę pomóc?". Karo daje mu kontekst. Chce wiedzieć co mafia robi na tym terenie i czemu się doczepiają do lokalnych biznesów.

Torszecki się wyraźnie zasępił. Karo chce Nazwiska, lokalizacje, działanie, co tu mafia robi. Torszecki zainwestował sporo środków i uruchomił sporo przysług. Mafia nie jest jego siłą.

Ex+3:

* X: Torszecki nie dał rady znaleźć niczego bez dodatkowych śladów
* X: Spalił kilka przysług na marne. Mafia to po prostu nie jego poziom
* X: Mafia się nim zainteresowała. Czemu się interesuje?

Torszecki -> Karo: "przykro mi, tien Terienak. Nie mam szczegółów. Czy masz cokolwiek?". Karo powiedziała, że tylko to co ma. Torszecki pyta co Karo zrobili. Zdrażnili ją. Torszecki pyta, co oni chcą zrobić. Karo - że okup. Torszecki zaproponował transponder. Wpadną na to. Ale może da się wymyśleć coś lepszego.

Karo -> Iwan. Pyta czy się czepiali. Jeszcze nie. Kwota jest racjonalna, coś, co Iwan jak zapłaci to bardzo nie ucierpi. Mniejsza niż Karo by się spodziewała. Karo pyta, czy koty nadają się do tropienia. Iwan potwierdził. Nie są mistrzowskie ale są spoko. Karo zaproponowała wrzucenie wabika do walizki z pieniędzmi tak jakby zrobił to kot. W końcu przecież to że kot wrzuca coś do walizki to się regularnie zdarza.

Karo i Daniel próbują przekonać Iwana do planu.

TrZ+2:

* Xz: Karo się dorzuca do okupu. Będzie biedniejsza jak się nie uda odzyskać.
* X: Iwan się bardzo martwi. Mafia. Najchętniej zapłaciłby okup i spokój, czyli Karo i Daniel muszą jego i rodzinę jakoś osłonić.
* V: Sukces. Iwan na to pójdzie.

Torszecki zaproponował, by Karo i Daniel straszyli Iwana Ernestem - że odda ludzi do Eterni czy coś. Zły Eternianin. I to jest coś w co mafia uwierzy. Że Iwan miał prawo się bać. Karo miała wątpliwości, że to dobry pomysł na starcie, ale Daniel i Torszecki ją przekonali. Koszt dla Marysi i Ernesta nie jest duży.

Czas na wsparcie bojowe. Karolina kontaktuje się z Arkadią. Jest okazja komuś pomóc i pobić złych ludzi. Arkadia spytała prosto - Karo zna Arkadię. Będzie to warte jej czasu? Karolina potwierdziła. Arkadia powiedziała, że przyjedzie. Gdy się pojawiła, Karolina powiedziała Arkadii - potrzebuje potencjalnej siły ognia i ubezpieczenia. Arkadia się uśmiechnęła szeroko. Z przyjemnością pomoże.

Arkadia pojawiła się w lekkim camo-suit, paletą noży, farba na twarz i włosy itp. Wyraźnie wzięła to na serio. Karo wyjaśnia wysokopoziomowy cel - niech Pustogor zrobił porządek z tym i paroma innymi grupami mafii. Głównie po to, by lokalny biznes nie miał niepotrzebnych zatargów z mafią. Ale nie chce ściągać tu szczególnie mocno terminusów - nie są tu szczególnie popularni. Więc sytuacja tak, by terminusi nie mieli po co tu się pojawiać. Ale by musieli rozwiązać sprawę.

Arkadia rozumie to jako "mają być ranni, ma być głośno, nie da się wyciszyć, nie jest to wina miejscowych". Aha, niech te pieniądze są odzyskane. Arkadia rozumie - dyskretny ops. Cicho, do celu, atak z zaskoczenia.

Faktycznie - dwa dni po pojawieniu się tam Karo pojawiło się żądanie okupu. Iwan zapłacił i poinformował Zespół. Arkadia, Daniel, Karo i Torszecki są gotowi. Jak tylko minęła odpowiednia ilość czasu (pół dnia), Iwan dostarczył kota Arkadii (ona jakoś sobie z nim radzi; jakoś - ale Karo i Damian są beznadziejni a Torszecki boi się kota, ku uciesze Arkadii).

Daniel tropi cholernym kotem drogę do celu.

TrZ+2:

* XX: Daniel dał radę zlokalizować kryjówkę... odpalając alarm.
* V: Daniel zorientował się, że jest cichy alarm. Mają lokację.

Arkadia stwierdziła prostą rzecz - nie ma możliwości zrobienia tego cicho i dyskretnie. Ona odciągnie uwagę mafii. Dywersja. Karo się aż zdziwiła - to jest głupi pomysł. Arkadia potwierdziła, ale nie widzi innej lepszej opcji w tej sytuacji. I zaatakowała. A Karo jest jej runaway.

ExZ (Karo + kamuflaż Arkadii + jej monumentalnie ryzykowny plan) +3:

* Vz: Arkadia zaatakowała. Dwa granaty. Ściągnęła uwagę. Okrzyk bojowy "Aurum! Sowińscy!" (Karo ma XD). Gdy 4 osoby wypadły z budynku, Arkadię przechwyciła Karo i odleciały. 

Dwa pojazdy ścigają Karo.

TrZ (wszyscy znają okolicę, ale Karo ma po prostu lepszy ścigacz) +3 +3Oy:

* V: Karo i Arkadia wycofały się.
* V: Karo się z nimi bawi a Arkadia ich ostrzeliwuje (nie ma szans trafić), tak jak oni nie mają szans trafić Arkadii

TYMCZASEM DANIEL próbuje się włamać, zdobyć fakty i podłożyć twarde narkotyki (od Torszeckiego):

TrZ (dywersja) +3:

* X: Daniel został zauważony; gdy podkładał i podmieniał to zobaczył przeciwnika. Czarodziejka weń strzeliła, ale Daniel spierniczył
* V: Daniel dokonał podmiany i zwinął dowody.
* V: Daniel zwiał bez szkody; przechwyciła w locie go Karo
* (+3Vg) Vz: Karolina odleciała na pełnej mocy ścigacza. Nie przechwycili jej. Udało się Rekinom się wycofać.

Torszecki docenił łup. Jest w stanie z tym pracować, coś wydobyć. Torszecki zauważył, że mafia bardzo nie będzie się chciała do tego przyznać - trzy Rekiny ich napadły i rozbiły? Torszecki na podstawie tego że Daniel widział twarz przeciwnika i tego co wydobył zlokalizował kim jest czarodziejka mafii - niejaka Stella Amakirin. Ponad50letnia czarodziejka, z mafią związana od zawsze, przedtem noktianka.

Arkadia powiedziała, że przekaże to Ksenii. To powinno rozwiązać problem.

Karo doceniła. Ona nie chce mieć do czynienia z terminusami jak nie musi. I to powinno odzyskać kasę Iwanowi... i jej. A Ksenia jak pozna tożsamość i dowie się co się stało to nie odpuści. I faktycznie, Karo odzyska kasę. Za 2 tygodnie.

## Streszczenie

Daniel chciał rozwiązać problem w Majkłapcu, gdzie koty poraniły ryby. Okazało się, że to wina mafii, której właściciel kociarni nie chce płacić okupu. Karolina zebrała drużynę, uratowali zatrute wściekłością koty i zaatakowali siedzibę małego oddziałka mafii. Po zdobyciu dowodów (i ucieczce) przekazali temat Ksenii.

## Progresja

* Karolina Terienak: w Majkłapcu jest lubiana przez Iwana Zawtraka z Kociarni i ma jego zaufanie.
* Daniel Terienak: w Majkłapcu jest lubiany przez Iwana Zawtraka z Kociarni i ma jego zaufanie a Genowefa z Farmy Krecik nim gardzi.
* Rafał Torszecki: próbował znaleźć info o mafii i ich działaniach w kierunku lokalnych biznesów i mafia się zirytowała. Niech się nie interesuje. Ma problem z mafią.
* Ernest Namertel: jego nazwiskiem i Eternią podobno straszy się lokalny biznes w Majkłapcu. Robota Karoliny i Daniela.

### Frakcji

* .

## Zasługi

* Karolina Terienak: uznała, że chce pomóc Iwanowi i rozwiązać problem lokalnego oddziału mafii; zebrała drużynę (Torszecki i Arkadia), po wyłapaniu kotów z Danielem manewrowała ścigaczem by uratować od mafii Arkadię i ewakuować Daniela.
* Daniel Terienak: chce pomóc domowej sentisieci, więc zainteresował się pomocą morderców ryb w Majkłapcu (by się zbliżyć do czarodziejki z AMZ). Złożył czar z Pawłem Szprotką - artefakt szukający kotów. Doszedł do tego, że Iwan Zawtrak nie mówi całej prawdy; wszedł na twardo i jakkolwiek dostał cios, to prawda wyszła na jaw. Włamał się skutecznie do mafii i podłożył im narkotyki.
* Ksenia Kirallen: oddelegowywana do wszystkich małych waśni pomiędzy firmami, bo jest absolutnie przerażająca i jeśli to coś nieważnego, NIKT JEJ NIE CHCE. 3 mc temu rozwiązała problem z Paprykarzem Z Majkłapca. A teraz pojawił się inny i jej nie wezwano. ALE! Jak tylko dostała dowody, że działa tam mafia (Stella Amarkirin) to ruszyła do działania.
* Genowefa Krecik: 22 lata, następczyni potęgi Krecików z Majkłapca; przekonana że morderstwo ryb i gęsi to wina "noktian" z wegefarmy. Nie ma najlepszej opinii o Danielu, mimo, że on dał radę.
* Paweł Szprotka: zajmuje się leczeniem ryb i gęsi w Majkłapcu z ran kotów-pacyfikatorów (z ramienia farmy Krecik). Z samego nagrania zauważył, że koty były czymś naćpane - ktoś im coś podał. Karo poprowadziła go by z Danielem stworzył detektor kotów po sierści.
* Iwan Zawtrak: 44 lata; właściciel kociarni i miłośnik tych zwierzaków. Nie chce się opłacać mafii, ale gdy mafia wypuściła część jego kotów i je podtruła by były agresywne, zmienił trochę zdanie. Zaufał Karo i Danielowi, powiedział im o co chodzi. Na uboczu, uczciwy, ale trudny w obejściu.
* Rafał Torszecki: nie znalazł dla Karo info o mafii i tylko podpadł mafii, ale zaproponował transponder co Karo przekształciła w wabik na koty. Dostarczył kompromitujące twarde narkotyki Danielowi, by ten mógł podłożyć je mafii. Pomógł Karo w manipulowaniu mafii.
* Arkadia Verlen: gdy usłyszała, że może bić i pomóc komuś to dołączyła do Karo. Gdy Daniel przypadkiem odpalił cichy alarm, zaatakowała mafię FRONTALNIE - i Karo ją przechwyciła ścigaczem. Dobra dywersja. Potem przekazała Ksenii info o Stelli z mafii.
* Stella Amakirin: eks-noktiańska czarodziejka, eks-mafia Kajrata (57 lat); ma kilka osób i szuka małych biznesów by dawały jej niewielki haracz. Szuka emerytury. Niestety, napotkała na ambitne Rekiny i na jej małą komórkę Karolina i Arkadia napuściły KSENIĘ!

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Majkłapiec: niewielka mieścina z jednym konstruminusem defensywnym; 12 km od Zaczęstwa. Słyną przede wszystkim z produkcji paprykarza eksportowanego na Orbiter.
                                1. Kociarnia Zawtrak: zajmują się kotami anty-anomalnymi rasy pacyfikator; eksport na orbitę, do większych jednostek itp. Iwan (szef) nie chciał opłacać się mafii, więc mafia wpadła, kilkanaście kotów uwolniła i je podtruła.
                                1. Wegefarma Myriad: eks-noktianin ją prowadzi; grzyby i ryż. Do paprykarza. 
                                1. Farma Krecik: prowadzone przez astorianina od zawsze; ryby i gęsi. Do paprykarza. Córka szefa Genowefa jest anty-noktiańska. Tu naćpane przez mafię koty-pacyfikatory wpadły zmasakrować sporo ryb i gęsi.
                                1. Zakład Paprykarski Majkłapiec: podsycają wiecznie konflikt między Myriad i Krecik, bo to obniża ceny. Eksporter Paprykarza Majkłapiec na Orbiter.

## Czas

* Opóźnienie: -1
* Dni: 5

## Konflikty

* 1 - Paweł Szprotka próbuje dojść do tego co jest nie tak z tymi kotami na nagraniu
    * TrZ+2
    * V: Paweł pokazuje Karo niektóre ruchy tych kotów. "On się nie bawi. On chce zabić. Chce zranić. Ten kot jest opętany nienawiścią. Koty tak nie mają. Co im zrobili?"
* 2 - Daniel, lepszy w tropieniu niż Karo, westchnął i zabrał się do szukania kociego futra...
    * ExZM+2:
    * XzXVz: Daniel wpadł do stawu uszkadzając nawierzchnię i dostał opierdol za dewastację mienia. Ale znalazł cholerne kocie futro...
* 3 - Paweł i Daniel (który się uwolnił od Genowefy) składają czar razem. Daniel artefaktyzuje detektor w podrdzewiałej podkowie, Paweł dostarcza "jądra" detektora.
    * TrZM+3, 5O (5 nie 3, bo nie są kompatybilni ze sobą)
    * XVzVm: mają perfekcyjny detektor kotów i perfekcyjny wabik. Ale Genowefa uważa, że Daniel nic nie umie i po co tu jest? Disdain.
* 4 - Daniel próbuje ocenić, czy Iwan udaje czy naprawdę przejmuje się tą sytuacją. O co tu chodzi? Daniel by się dowiedzieć generuje presję.
    * Tr+2+3O (presja - sukces, ALE)
    * XV: Daniel przesadził z presją i został walnięty w ścianę. Ale widzi, że Iwan mówi prawdę acz niecałą. Ale zależy mu na innych lokalsach i nie chciał krzywdy ani ich ani kotów
* 5 - Karo "nie chcesz ze szczeniakami, poradzimy sobie inaczej. Więcej kombinowania, ale..." -> Iwan ma współpracować.
    * TrZ+3+3O (presja - sukces, ALE)
    * VzXzX: Iwan powiedział im wszystko i chce ich nauczyć sterowania kotami. Nope. Daniel ani Karo NIE UMIEJĄ W KOCIE KOMENDY.
* 6 - Karo zaproponowała Danielowi - łączą siły. Nagrać (złapać komendę) Iwana, by kot zostawił wabik. Nawet jak nie zadziała, może pomóc.
    * TpZM+3
    * VXVm: komenda działa, z dużą ilością decybeli (niestety), ale działa. To i wabik zadziała na futrzaki.
* 7 - Czyli Karo i Daniel mają: klatki, lokalizację, wabik, stopper kotów, ścigacze. Czas zapolować na futra.
    * TrZ+4+3Vg (reprezentują absolutną przewagę)
    * VVVV: udało im się wszystkie wyłapać, nawet te co miały małe szanse. Ściągnięty Szprotka co je wyratuje. A Iwan lubi te dwa Rekinki.
* 8 - Torszecki się wyraźnie zasępił. Karo chce Nazwiska, lokalizacje, działanie, co tu mafia robi. Torszecki zainwestował sporo środków i uruchomił sporo przysług. Mafia nie jest jego siłą.
    * Ex+3
    * XXX: Torszecki nie dał rady, spalił kilka przysług i mafia się nim interesuje.
* 9 - Karo i Daniel próbują przekonać Iwana do planu podłożenia mafii fałszywego okupu
    * TrZ+2
    * XzXV: Iwan potrzebuje zapewnienia że mafia się na nim nie zemści a Karo dołożyła kasy do okupu. Ale - przekonali Iwana
* 10 - Daniel tropi cholernym kotem drogę do ukrytej bazy mafii.
    * TrZ+2
    * XXV: Daniel dał radę zlokalizować kryjówkę... odpalając cichy alarm. Ale wie gdzie są i wie o alarmie.
* 11 - Arkadia stwierdziła prostą rzecz - nie ma możliwości zrobienia tego cicho i dyskretnie. Ona odciągnie uwagę mafii. Dywersja. 
    * ExZ (Karo + kamuflaż Arkadii + jej monumentalnie ryzykowny plan) +3
    * Vz: Arkadia zaatakowała. Dwa granaty. Ściągnęła uwagę. Okrzyk bojowy "Aurum! Sowińscy!" (Karo ma XD). Gdy 4 osoby wypadły z budynku, Arkadię przechwyciła Karo i odleciały. 
* 12 - Dwa pojazdy ścigają Karo. Ona próbuje kupić czas Danielowi.
    * TrZ (wszyscy znają okolicę, ale Karo ma po prostu lepszy ścigacz) +3 +3Oy:
    * VV: Sukces na obu liniach
* 13 - TYMCZASEM DANIEL próbuje się włamać, zdobyć fakty i podłożyć twarde narkotyki (od Torszeckiego)
    * TrZ (dywersja) +3
    * XVV(+3Vz)Vz: Daniel zauważony, ale podmienił i zwinął dowody. Zwiał bez szkody dzięki Karo i się wycofali bezpiecznie.
