## Metadane

* title: "Kapitan Verlen i Królowa Kosmicznej Chwały"
* threads: kosmiczna-chwala-arianny
* motives: aurum-program-kosmiczny, rekonstrukcja-ruiny
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [210324 - Lustrzane odbicie Eleny](210324-lustrzane-odbicie-eleny)
* [221113 - Ailira, niezależna handlarka wodą](221113-ailira-niezalezna-handlarka-woda)

### Chronologiczna

* [210324 - Lustrzane odbicie Eleny](210324-lustrzane-odbicie-eleny)

## Plan sesji
### Theme & Vision

* Królowa Kosmicznej Chwały, support ship

### Co się wydarzyło KIEDYŚ

* .

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

.

### Co się stanie (what will happen)

* S00: 
* S01: Alezja Dumorin, półnaga, wyrzucana przez marine bo pojawia się Arianna (Szymon Wanad).
* S0N: gra w karty, przegrał (Jan Kirpin) cnotę przyjaciółki (Medea Szarprak) do marine. Nie odważy się odmówić BO LEONA.
* S0N: statek nie jest w pełni sterowny a trzeba doprowadzić go do działania.
* S0N: Władawiec jest wściekły że nie dowodzi. Powinien być dowódcą. Czemu Arianna a nie on?
* S0N: XXX mówi Ariannie "nie". Nie czuje się zobowiązany do robienia czegokolwiek. I tak Arianna rozłoży nogi.
* S0N: Puryści Seilii chcą pozbyć się Leony i Irkana + Triaca. Leona ich chroni.
    * 3 ze statku 7 z Aurum, 2 osoby z Aurum to advancerzy (tien Szczepan Myrczek + tien Mariusz Bulterier)
    * tak, robią też sabotaż przeciw Leonie
* S0N: Oficer łącznościowy (tien Maja Samszar) nie przekazuje Ariannie niczego
    * Inspekcja, Arianna nie jest gotowa
* S0N: przerażony Tomasz Dojnicz (marine) ma zatrzymać bójkę między ludźmi - "Aurum" vs "załoga"

.

* Pierwszy oficer - Władawiec Diakon - podrywa na lewo i prawo bawiąc się hormonami.
    * W medycznym jest tien Klaudiusz Terienak który chce zdobyć uznanie Władawca, ale boi się Leony.
* Aurum podzieliło się na frakcje i próbują wygrywać między sobą.
* Leona Astrienko jest tu na wakacjach - ale też ma uniemożliwić śmierć kogokolwiek czy jednostki.
    * 2 sarderytów, Irkan i Triac nie dają się jej ale też w sumie nikomu skrzywdzić.
* Leona + 5 marines opanowuje tą 40-osobową jednostkę.
* STRASZNA nieufność Orbitera do Aurum
* Klarysa Jirnik, artylerzystka i odpowiedzialna za broń
* tien Grażyna Burgacz, logistyka i sprzęt
* tien Arnulf Perikas, fabrykacja i produkcja - on jest bardzo za Arianną i najbardziej rozczarowany Alezją

### Sukces graczy (when you win)

* .

## Sesja właściwa
### Wydarzenia przedsesjowe

.

### Scena Zero - impl

Arianna dostała tą jednostkę "za karę" - zrobiła COŚ co KOMUŚ podpadło. Arianna dostała jednostkę o nazwie "Królowa Kosmicznej Chwały". To nie jest jednostka FLOTY ORBITERA. To jest jednostka która ma NIEZNANE pochodzenie. Na pokładzie nie ma ani jednego oficera ORBITERA stricte. Aurum itp. mają decyzyjność i poprzedni kapitan zażyczył sobie żeby NA PEWNO nie pojawił się żaden "sztywny oficer Orbitera" by tam działać. Orbiter przydzielił tylko 6 marines by statek się nie rozleciał. Marines są dowodzeni przez Ernesta Psa. Który jest KAPRALEM. Jednostka ma koło 50 osób. Większość to Aurum ale nie tylko...

I tam pojawia się Arianna jako OFICER AURUM ALE NAPRAWDĘ ORBITERA. Jest wysłana by "rozwiązać problem". To jest jednostka WSPARCIA. Ta jednostka jest szczytem polityki - Aurum can into space. A Orbiter się na to zgodził bo polityka.

Arianna dostała Darię - engineering. Ta jest kompetentna. Aha, i WSZYSCY marines przesłani przez Orbiter na Królową Kosmicznej Chwały są tam za karę albo karnie albo mają problemy osobowościowe.

### Sesja Właściwa - impl

Pinasa którą przylatuje Arianna jest z tego "dobrego" Orbitera - z K1. Arianna dostała proste zadanie. Sprawić, by podczas ćwiczeń Królowa dała radę przesłać advancerów na pokład jednostki której pomaga. Nie udało się to 3 ostatnie razy. Raz Królowa się ZDERZYŁA z jednostką, raz advancer "przeleciał" a raz Królowa nawet nie wystartowała. Co więcej, Królowa ma niesamowitą ilość "ran wymagających hospitalizacji" i ludzie opuszczają pokład nie wracając tam. Plotka mówi, że jeden z marines "uczynnie pomaga" ochotnikom.

"Królowa" uczy synów i córki Astorii prawdziwego życia bez dyscypliny. I niektórych marines.

Pintka dotarła do Królowej. Przynajmniej z zew. wygląda dobrze. KAPITAN ARIANNA VERLEN jest w stanie dostać się na pokład. Pintka się otwiera. I tam - MARINE TRZYMA NIEZBYT UBRANĄ PANIĄ KAPITAN. Arianna wyskakuje na marine i go podcina. Koleś się tego nie spodziewa. Przewrócił się.

Tp+3:

* V: marine na ziemi
* V: pani kapitan złapana
* V: Arianna ma EFEKTOWNE wejście
* X: Daria nie wie o co chodzi ale tazuje marine. (+2Vg)
* V: WIDAĆ, że będzie inaczej. Marine nie lubi Arianny i Darii, ALE jest szacunek.

"To ja zaraz wrócę i się ubiorę! Może chodźmy do pinasy!" Arianna w szoku. Jak ona może chcieć zostawić statek XD. Więc... do jej kwater. Kapral wpadł i zdębiał. Ale dostał rozkaz zajęcia się podopiecznym...

KWATERY kapitan Alezji Dumorin są... niegodne kapitan. Jakby miała OnlyFans. BARDZO próbuje na Ariannę nie patrzeć i niczego nie mówić. Powiedziała "ten statek Cię zniszczy. Uciekaj." - tyle z siebie wydusiła.

Dała wskazówki:

* Marines wykonują polecenia... ale nie wszystkie. I nie wszyscy. Nie zrobią krzywdy i ochronią... ale nie będą proaktywni.
* Masz... purystów Seilii. I masz zmodyfikowanych. Oni się zwalczają.
* Masz Aurum. I wszystkie małe frakcje Aurum. Oni... nie patrzą na stopnie a na tytuły.
* Oni robią co chcą. I jak nie użyjesz marines nie będą Cię słuchać. A marines KRZYWDZĄ.
* Masz dwóch sarderytów. Trzymam ich w karcerze by im nic się nie stało.
* "Co się stało z tym marine?"
    * CZERWONA. Oficer łącznościowy mi nie powiedział że będziesz i... była zajęta.
* Kapral boi się części podwładnych.

Jeśli ona opuści tą jednostkę, jest skończona. Nie w tym stanie. Arianna zaproponowała jej by została. To nie jest pierwszy przydział Alezji - ale podpadła. I wierzyła, że sobie poradzi. Nie poradziła sobie.

Arianna ma unikalny brak wiedzy dookoła słabości ludzkiej natury. Więc nie radzi sobie z wyczytaniem tego dookoła Alezji jak pyta o 1 oficera.

Tr (niepełny) Z (bo Alezja bardzo się stara) +2:

* Vz: pierwszy oficer to Władawiec Diakon. On chce być kapitanem. Ona jest w nim rozkochana.
    * on ją scorruptował tak, że poodcinał troszeczkę, wydarzenia sprawiały, że ona nie miała wsparcia.
    * był zaskoczony tym, że Arianna została kapitanem. Myślał, że jego awansują.
* Wsparcie:
    * Arianna znajdzie wsparcie u kaprala marines i OGÓLNIE u marines, choć nie wszyscy ją zaakceptują.
    * Sarderyci nie wiedzą, że to anomalne zachowanie. Oni się adaptują cokolwiek się nie dzieje.
    * Część załogi chce normalnego statku i chce być z czegoś dumna.
    * Wyznawcy Seilli będą wspierać każdego kto wspiera Seillię.
        * Wszyscy advancerzy to wyznawcy Seilii. (tien Szczepan Myrczek + tien Mariusz Bulterier)

Arianna spojrzała ze smutkiem. Od razu pierwszy problem - jeśli Alezja tu zostanie, GDZIE? Ona oczywiście chciałaby z Władawcem. Co nie wchodzi w grę jeśli ma się rehabilitować. Arianna zatem ściąga niższego stopniem oficera o którym Alezja powiedziała że ma sens (tien Grażyna Burgacz) i z nią siadła do rozlokowywania ludzi.

Tr Z(Grażyna zna się na rzeczy i chce pomóc) +3:

* X: wśród załogi Alezja jest traktowana jako minus przez "Orbiterowców" - czemu ją trzymasz, czemu dajesz szansę?
    * Alezja zostaje DRUGIM oficerem a Władawiec PIERWSZYM
* V: udało się tak rozlokować, by wszyscy byli odpowiednio zadowoleni po statusie
* V: nie da się przyczepić. Po prostu.
    * ludzie lokowali "jak chcieli", Grażyna nie miała przebicia. Teraz dostała rozkaz.
* X: w załodze Grażyna jest "tą co się podlizuje nowej kapitan"
* X: opinia: to GRAŻYNA za tym wszystkim stoi. Podlizać się i załatwić sprawę. Bo ona chce być nowym adjutantem. Arianna trzyma Alezję bo LUBI takie jak ona.

Oki - czas na dalsze działania. 

Tymczasem Daria przejmuje inżynierię po drobnej lasce, która chciała być bohaterką Orbitera i jej przeszło. Wróciła na łąki Aurum czy coś bawić się w żołnierza. A Daria patrzy na dane. Rekordy pokazują, że statek jest sprawny. WSZYSTKO działa. 100% wskazań działa poprawnie. Daria "ten statek zaraz się rozsypie".

Czujniki są spięte na krótko. Nie ma informacji o tym jak działa statek. Czterech inżynierów patrzy na Darię markotnymi oczkami. Daria patrzy na termometr który oszukuje o 5 stopni. "Serio?" Inżynier do Darii "taki rozkaz - pani kapitan powiedziała że jak zobaczy choć jedno wskazanie nieperfekcyjne to będzie problem...". Daria "to teraz odwracamy wszystkie spięte rzeczy. Chcecie zginąć?" Jeden z inżynierów pokazał, jak w systemach dalekiego zasięgu monitorują stan krytycznych systemów.

Daria na ich oczach naprawia czujnik i żąda, by wszyscy naprawiali. Inżynierowie "ale oficer Dumorin tu ciągle jest...". Na to Daria "ale kapitan się zmienił".

Tr Z (inżynierowie CHCĄ) +2:

* V: inżynierowie odbudują kluczowe systemy i puszczą na ekran
* X: część rzeczy, zasobów itp jest porozkradana i posprzedawana
* V: odbudują wszystkie detektory systemów
* Vz: morale inżynierów jest wysokie. Wreszcie robią coś sensownego.

Bardzo szybko Daria dostała pierwsze informacje - ten statek potrzebuje gruntownych napraw od środka. Wszystko może sam sfabrykować. Ale póki tego się nie zrobi, nie poleci. I nie ma materiału - ktoś sprzedał lub rozkradł, np. część materiału pozsła na tor do wyścigu psów mechanicznych. Rozkaz Darii - po zrobieniu tego z sensorami (2 dni) to porozkładać tor psów itp. (7 dni).

Rozmontowanie toru psów będzie niepopularną decyzją. Ale Daria ma to gdzieś - chce naprawić statek. Jeśli ktoś narzeka na to że jego pies ma żyć, spoko. Ale przestanie mu działać prysznic w kajucie.

Za 14 dni kolejna edycja ćwiczeń. Daria ma wątpliwości czy zdąży dostarczyć...

Arianna wzywa do siebie kaprala Erwina Psa. Kapral ma pokerową minę. Ale będzie słuchał. Przeprosił, bo nie chciał tak upokorzyć Alezji. Złożył BARDZO dokładny raport łącznie ze swoimi failami i Arianna ma dostęp do dużej ilości 'malpractice':

* przybywanie niewłaściwych jednostek
* destrukcja maszyny do kawy dla załogi by mieć przeszkodę w wyścigach psów
* wykorzystanie arystokraty Aurum jako miotłę przez marine Astrienko
    * większość marines jest tu karnie. Leona Astrienko się zgłosiła na ochotnika.

W ogóle z raportu wynika, że jest dwóch strasznych troublemakerów - Szymon Wanad (oczy zabójcy, LUBI zadawnie cierpienia). Drugim jest neurosprzężona Leona Astrienko. Kapral się przyznał, że ona nie do końca słucha poleceń.

"Wyścigi psów są zarezerwowane dla arystokracji Aurum nieważne czy są oficerami czy nie". Leona czasem ostrzeliwuje mechaniczne psy. To pokazuje czemu tak bardzo brakuje psów i materiału czasem.

Arianna -> kapral: musimy zaniedbać statek i zrobić awarie. Muszą zacząć dbać o statek bo ich wygoda od tego zależy. Kapralowi zapaliły się oczy. "Pozwoli pani kapitan, że zarekomenduję szeregową Astrienko do pilnowania miejsc gdzie może dojść do hipotetycznego sabotażu". I kapral wyjaśni Leonie sytuację. Arianna się zgodziła.

Arianna najpierw robi spotkanie na mostku dla wszystkich ważnych arystokratów i oficerów. Arianna chce poznać swoich oficerów... Cała ekipa oficerów nie sprawia wrażenia tego co Arianna chciała zobaczyć. Arianna robi MOWĘ a w tym czasie inżynierowie operację SABOTAŻ. Daria poprosiła o informację kto jest najbardziej zapsiony - tam awarie będą za wysokie. 

Arianna ma następujące cele z tej mowy:

* "będę rekomendować kompetentnych a kompetentnych będę własnymi oczami oceniać a stan aktualny to wina lat i zaniedbań i niemożności dogadania"
* "to jedyny statek kosmiczny Aurum - pokazujemy Orbiterowi że nawet nie umiemy wylecieć z doku. Pokażmy, czym Aurum może być"
* "Aurum zainwestowało w ten statek reputację i pieniądze a my robimy wyścigi psów na orbicie - może pocztówka i rodzinom z ćwiczeń?"
* zepchnąć na bok winę Alezji - nie mówię JAWNIE że to jej wina bo jest dużo czynników które się na to złożyły
* pokazać swoje szersze ambicje Diakonowi - fotel kapitana może się zwolić za jakiś czas
* zachęcić oficerów do tego, by działali razem dla jednostki

ORBITER NAJSKUTECZNIEJ NA ŚWIECIE SABOTOWAŁ PROGRAM KOSMICZNY AURUM - DAŁ IM STATEK I DAWAŁ MŁODE DZIEWCZYNY JAKO KAPITANÓW.

Tr Z (Arianna jest jednym z nich i dla każdego coś ma) +3:

* V: oficerowie ku dobru wspólnemu - statek ma zacząć działać. ZROZUMIELI co się tu działo.
* V: Władawiec jest przekonany że warto współpracować z Arianną bo to mu się opłaci. Niekoniecznie "za darmo", ale nie będzie jej sabotował.
* X: Załoga nie zaakceptuje Alezji. Nie po tym co ona zrobiła. Nie nadaje się ich zdaniem na kapitana czy oficera.
* V: Załoga nie zamierza mówić o Alezji nikomu poza tą jednostką. Oni jej nie zaakceptują, ale nie chcą jej zniszczyć.
* X: Władawiec BĘDZIE WSPÓŁPRACOWAŁ z Arianną. Za Alezję.   <-- Arianna się nie zgadza, będziemy mieli negocjacje POTEM.
* Vz: Niezależnie od okoliczności Władawiec zostawi temat Alezji na tej jednostce. Faile Alezji nie opuszczą tej jednostki i tych ludzi. Ma szansę na przyszłość.

Arianna ma pomysł - NIECH ZABRAKNIE CIEPŁEJ WODY POD PRYSZNICAMI. Niech Daria zacznie wprowadzać mini-awarie. Daria sabotażystka. Niech jest maksymalnie niewygodnie. Statek zaczyna umierać. Wygody mogą iść w cholerę. ARIANNA LUBI TO - CZUJE SIĘ JAK W DOMU.

Tp Z (Leona pilnuje i czuwa) +4 (mowa Arianny i działania i Leona ma szansę zastraszać i wszystkie krytyczne giną):

* VXV: obniżona popularność Darii i Arianny bo dyskomfort jak cholera ("zimna woda hartuje charakter pytajcie Ariannę"), ludzie ZMOTYWOWANI do pracy bo tak się nie da żyć.
    * Sarderyci? Sarderyci nie rozumieją o co chodzi. Nadal jest super wygodnie. Nawet jest lepiej bo nie trzeba się kąpać.

## Streszczenie

Królowa Kosmicznej Chwały to najbardziej dysfunkcyjna jednostka pod kontrolą Orbitera - służy do łamania karier obiecujących oficerów i sabotażu programu kosmicznego Aurum (oba z woli sił specjalnych Aurum). Trafiła na królową Arianna i zaczęła robić porządek. Poprzednią panią kapitan zdecydowała się odratować, współpracując z Darią z inżynierii rozmontować wyścigi psów i stwierdziła, że doprowadzi do tego, by Królowa zaczęła skutecznie działać podczas ćwiczeń. Aha, tu poznała się z Leoną Astrienko ;-).

## Progresja

* Grażyna Burgacz: opinia na "Królowej" że podlizuje się Ariannie, ale jest bardzo kompetentna w logistyce.
* Alezja Dumorin: załoga "Królowej" uważa ją za silny minus i obniża status Arianny. Ale jej reputacja nie wyjdzie poza Królową.

* OO Królowa Kosmicznej Chwały: jeszcze 5 dni zajmie przebudowywanie torów i sensorów itp by doszła do jakiegokolwiek funkcjonowania.

### Frakcji

* .

## Zasługi

* Arianna Verlen: 22-23 lata; została kapitan Królowej Kosmicznej Chwały (jednostki 'Aurum' pod kontrolą Orbitera). Dała szansę poprzedniej kapitan (Alezji) mimo, że ta jest w beznadziejnym stanie i pokonała marine. Wprowadziła dyskomfort na Królowej i pokazała innym oficerom, że Siły Specjalne Orbitera sabotują Królową by program kosmiczny Aurum się nie mógł odbyć.
* Daria Czarnewik: w cywilnej części Kontrolera Pierwszego, przydzielona na Królową jako chief engineer. Przejęła kontrolę nad inżynierią, natychmiast kazała odbudować to co ważne, podjęła niepopularne decyzje (m.in. rozmontowanie toru do wyścigu psów).
* Alezja Dumorin: poprzednia kapitan Królowej; pod wpływem Władawca praktycznie założyła OnlyFans. Arianna dała jej szansę i zdegenerowała do drugiego oficera. Mimo złamanego morale i braku wiary w siebie pomaga Ariannie jak może.
* Władawiec Diakon: pierwszy oficer Królowej; zdominował i scorruptował Alezję i 'wszystkie spódniczki jego'. Zdecydował się nie sabotować Arianny jeśli ona mu nie przeszkadza. Nie chce oddać holdu na Alezji - jego zabawka.
* Erwin Pies: kapral i dowódca marines na Królowej; stanął po stronie Arianny. Zbiera bardzo dokładny raport i wspiera Ariannę jak tylko ten Orbiterowiec potrafi. Dość kompetentny, choć nie miał czym i z kim pracować póki nie miał Arianny.
* Leona Astrienko: marine na Królowej; wszyscy się jej boją. Chroni ludzi i dobrze się bawi tępiąc tienowatych. Niesterowalna. Jej pomysłem było upokorzenie Alezji przez Szymona Wanada. Pilnuje, by inżynierowie mogli odciąć ciepłą wodę (z woli Arianny).
* Szymon Wanad: marine na Królowej; oczy zabójcy, LUBI zadawnie cierpienia. Wyciągnął Alezję półnagą przed oblicze Arianny by ją upokorzyć i dostał od Darii i Arianny.
* Klarysa Jirnik: artylerzystka i odpowiedzialna za broń na Królowej; chwilowo nie ma czym strzelać. Z ciekawością patrzy co się będzie działo.
* Grażyna Burgacz: kompetentna logistyk Królowej i tien; pomogła Ariannie rozlokować ludzi. Uważana za najlepszą oficer Królowej przez załogę.
* Arnulf Perikas: fabrykacja i produkcja na Królowej - on jest bardzo za Arianną i najbardziej rozczarowany Alezją; tien.
* Klaudiusz Terienak: oficer medyczny Królowej i tien; współpracuje z Władawcem i jedyną osobą której naprawdę się boi jest Leona Astrienko.
* Maja Samszar: oficer łącznościowy Królowej i tien; nie przekazała informacji o przybyciu Arianny Alezji. Stoi lojalnie za Władawcem.
* OO Królowa Kosmicznej Chwały: najbardziej dysfunkcyjna jednostka pod kontrolą Orbitera - służy do łamania karier obiecujących oficerów i sabotażu programu kosmicznego Aurum. Arianna przejęła nad tą jednostką dowodzenie.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 973
* Dni: 6

## Konflikty

* 1 - MARINE TRZYMA NIEZBYT UBRANĄ PANIĄ KAPITAN. Arianna wyskakuje na marine i go podcina. Koleś się tego nie spodziewa. Przewrócił się.
    * Tp+3
    * VVVXV: Arianna ma efektowne wejście - łapie kapitan, powala marine, Daria tazuje marine a tamten marine nie lubi tych dwóch ale ok.
* 2 - Arianna ma unikalny brak wiedzy dookoła słabości ludzkiej natury. Więc nie radzi sobie z wyczytaniem tego dookoła Alezji jak pyta o pierwszego oficera
    * Tr (niepełny) Z (bo Alezja bardzo się stara) +2
    * VzV: Alezja powiedziała co wie a nawet czego nie wie; Arianna ma info o romansie z Władawcem i gdzie znajdzie wsparcie
* 3 - Arianna zatem ściąga oficera o którym Alezja powiedziała że ma sens (tien Grażyna Burgacz) i z nią siadła do rozlokowywania ludzi.
    * Tr Z(Grażyna zna się na rzeczy i chce pomóc) +3
    * XVVXX: Alezja jest traktowana jako minus, Grażyna się "podlizuje pani kapitan" i ona za tym stoi. Udało się porozlokowywać.
* 4 - Daria na ich oczach naprawia czujnik i żąda, by wszyscy naprawiali. Inżynierowie "ale oficer Dumorin tu ciągle jest...".
    * Tr Z (inżynierowie CHCĄ) +2
    * VXVVz: inżynierowie odbudowują kluczowe systemy i detektory i morale rośnie. Część rzeczy rozkradziona i zmarnowana.
* 5 - Arianna robi MOWĘ do swoich oficerów.
    * Tr Z (Arianna jest jednym z nich i dla każdego coś ma) +3
    * VVXVXVz: statek zacznie działać, Władawiec jej nie sabotuje ALE nie pomaga bez Alezji. Załoga nie zaakceptuje Alezji, ale zakres - ten statek.
* 6 - Arianna ma pomysł - NIECH ZABRAKNIE CIEPŁEJ WODY POD PRYSZNICAMI. Niech Daria zacznie wprowadzać mini-awarie.
    * Tp Z (Leona pilnuje i czuwa) +4 (mowa Arianny i działania i Leona ma szansę zastraszać i wszystkie krytyczne giną)
    * VXV: Obniżona popularność Darii i Arianny bo dyskomfort.
* 7 - 
    * 
    * 
* 8 - 
    * 
    * 
* 9 - 
    * 
    * 
* 10 -
    * 
    * 
