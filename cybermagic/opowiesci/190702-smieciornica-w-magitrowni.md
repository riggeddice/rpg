## Metadane

* title: "Śmieciornica w magitrowni"
* threads: narodziny-skorpiona, dziewczyna-i-pies
* gm: żółw
* players: kić, sabrina

## Kontynuacja
### Kampanijna

* [190704 - Magitrownia Finis Vitae](190704-magitrownia-finis-vitae)

### Chronologiczna

* [190704 - Magitrownia Finis Vitae](190704-magitrownia-finis-vitae)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

* .

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Jan Waczar poprosił Alię i Klarę o rozwiązanie problemu ryczącego potwora w okolicy starej, nieaktywnej magitrowni. Czemu mu ten problem przeszkadza?

1. Rozmagicznia nawozy; nie da się sensownie uprawiać niektórych roślin
2. Jan boi się o syna, Irka; on czasem lata gdzieś w tamte okolice

To trzeba przesłuchać dzieciaka; może coś wie? Okazało się, że Irek biega w tamtych terenach bo chodzi ze swoją koleżanką, Patrycją. Jest w sumie jedynym przyjacielem Patrycji a ona tam często się przemieszcza. Alia i Klara są zniesmaczone; Patrycja to córka zapijaczonego degenerata (kiedyś drzeworyt, mąż inżynier magitrowni która zginęła w Erupcji Finis Vitae pięć lat temu). No nic, trzeba z Patrycją porozmawiać.

Patrycja mieszka w ruderze - jej dom jest zniszczony i zdewastowany. Nie chce wpuścić Alii i Klary; sama do nich wyszła z nimi porozmawiać. Usiadła na ławeczce. Jest wyraźnie niezbyt zadbana, acz jest rezolutną czternastolatką. Jest nieprzyjaźnie nastawiona do dorosłych, ale odpowiedziała na pytania, naciśnięta:

1. Chodzi z Irkiem bo go lubi. Uważa go za przyjaciela.
2. Nie boi się potwora, bo jej pies - Lucek - ją ochroni.
3. Potwór to śmieciornica; konsekwentnie rośnie i jest coraz większa i groźniejsza.
4. Trzy tygodnie temu się ta śmieciornica tam pojawiła.
5. Miesiąc temu magitrownia została uruchomiona. Kto ją uruchomił? No, ona.

Oki, czyli dało się uruchomić magitrownię. Czyli w jakiejś formie magitrownia się odbudowała. Trzeba się tam przejść. Klara i Alia poszły w tamtym kierunku - coś ich śledzi. Klara podpatrzyła i znalazła dziwnego viciniusa na podstawie psa. Nie jest to istota Esuriit, ale jest napędzany jakąś formą energii. Eteryczny pies na bazie wilczura. Oddaliły się - wilczur nie robi wrogich ruchów, tylko patrzy.

Alia ściągnęła śmieciornicę w magitrowni, Klara ją zintegrowała z mechanizmami. Śmieciornica jest sprzężona i unieruchomiona przez tydzień. Alia tymczasem postawiła dwóch strażników; niech młoda nie uruchomi magitrowni ponownie. I faktycznie się udało, uniemożliwili Patrycji uruchomienie magitrowni.

W nocy Anomalie zniszczyły pietruszkę Waczarowi. Nie można jej jeść (Skażenie). W związku z tym następnej nocy Alia spali mu pole by nikt jej nie jadł XD.

Parę dni później Waczar poprosił do siebie czarodziejki ponownie. Zaczął biadolić że pietruszka, że nawozy nie działają, magitrownia nadal psuje, a potwór dalej ryczy. Przy okazji Klara zauważyła ślady krwi przy framudze okna. Zdecydowały się pójść porozmawiać z Irkiem ponownie.

Chłopak jest załamany, przerażony i ma wydrenowaną energię. Klara przyjrzała się Irkowi uważnie - jest lekko dotknięty energią Esuriit. Emanacja Magii Krwi. Przyciśnięty, wyśpiewał wszystko - powiedział że Patrycja była cała przerażona i zapłakana, że jej ojciec potrzebuje pomocy, że magitrownia nie działa - i ona musi zabić królika. Więc on trzyma ona zabija. On powiedział że on to zrobi by ona nie musiała. I zabił królika ale się lekko skaleczył. Część energii poszła z niego.

Oki. To już nie jest śmieszne. Trzeba zatrzymać Patrycję - ale o co chodzi? Czarodziejki poczekały, aż Patrycja pójdzie na targ z psem i poszły odwiedzić ojca.

Ojciec Patrycji jest epicki, w świetnym stanie, uśmiechnięty i w ogóle. Jednocześnie są w obszarze Efemerydy. Krótkie badanie - ojciec Patrycji nie żyje od miesiąca czy coś. Patrycja włączyła magitrownię by trzymać go przy życiu, by stabilizować efemerydę. Ale efektem ubocznym jest potencjalne Skażenie pryzmatem... no i potencjalne ściągnięcie energii Esuriit.

Patrycja wróciła z upiornym psem, Luckiem. Pies jest jej lojalny i wierny; jest echem efemerycznym, Skażeńcem, ale lojalnym dziewczynce. Patrycja się boi - nie chce być odebrana przez MOPS, nie chce tracić ojca którego kocha. Czarodziejki przekonały efemerycznego ojca by powiedział Patrycji jakie są konsekwencje. On sam powiedział Patrycji, że nie żyje, że jest potworem i prędzej czy później zrobi jej krzywdę.

Patrycja w rozpaczy uciekła z Luckiem. A czarodziejki rozmontowały efemerydę.

**Sprawdzenie Torów** ():

* skomplikowane.

**Epilog**:

* .

## Streszczenie

Magitrownia Maczkowiecka została uruchomiona przez czternastoletnią Patrycję by wskrzesić jej ojca. Czarodziejki odkryły problem i deeskalowały problem. Patrycja uciekła (by nie zabrał jej MOPS) a sytuacja w Maczkowcu została opanowana.

## Progresja

* Patrycja Radniak: 14latka; po raz pierwszy (i jak obiecała sobie, ostatni) użyła magii krwi do dostarczenia ojcu energii by powrócił
* Patrycja Radniak: ma psa Lucka; jest to Skażeniec jej niesamowicie oddany i lojalny

### Frakcji

* .

## Zasługi

* Klara Orkaczyn: lokalny szczur i lekarz w Maczkowcu; leczy, wykrywa i używa magii by dojść do prawdy odnośnie śmieciornicy, Patrycji i efemerydy.
* Alia Naszemba: wojowniczka i firebrand w Maczkowcu; siłą i dominacją przebija się przez nastolatków i dorosłuch by rozwiązać problem Patrycji używającej magii krwi.
* Patrycja Radniak: czternastolatka, która walczyła o swojego ojca po jego śmierci - wpierw magitrownią, potem magią krwi. Za wszelką cenę. Uciekła, gdy okazało się że przegrała.
* Przemysław Grumcz: hodowca psów który pięć lat temu dał Patrycji psa Lucka - by nie było jej szkoda, by nie była sama z ojcem po wydarzeniach w magitrowni. Żal mu było.
* Jan Waczar: rolnik zmartwiony stanem swoich pól i śmieciornicą w magitrowni. No i synem, który włóczy się po tych niebezpiecznych terenach.
* Irek Waczar: chłopak (13) który przyjaźnił się z Patrycją; użył magii krwi by pomóc jej ojcu i został dotknięty Esuriit. Dał sobie spokój z Patrycją i magią.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Maczkowiec
                            1. Magitrownia Stu Maków: odbudowana, włączana i wyłączana przez Patrycję by utrzymywać ojca w nieżyciu; zalęgła się tam Śmieciornica
                            1. Gospodarstwa Przyleśne: m.in. tam był dom Patrycji. Doszło do pojawienia się efemerydy - jej ojciec już nie żył a Patrycja chciała wierzyć
                            1. Gospodarstwa Przydrożne: m.in. gospodarstwo Waczarów. W pewien sposób centrum dowodzenia Zespołu - Klary i Alii.

## Czas

* Opóźnienie: 1799
* Dni: 3
