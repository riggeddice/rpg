## Metadane

* title: "Wojna o Arkologię Nativis - konsolidacja sił"
* threads: kidiron-zbawca-nativis, neikatianska-gloria-saviripatel
* motives: zdrada-w-rodzinie, wojna-domowa, integracja-rozbitej-frakcji, pomagamy-diablu, birutan
* gm: żółw
* players: fox, kapsel

## Kontynuacja
### Kampanijna

* [230621 - Infiltrator ucieka a arkologia płonie](230621-infiltrator-ucieka-a-arkologia-plonie)

### Chronologiczna

* [230621 - Infiltrator ucieka a arkologia płonie](230621-infiltrator-ucieka-a-arkologia-plonie)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * .
* CEL: 
    * Highlv
        * Ardilla szuka maga Nihilusa.
        * Muszę pokazać obecność maga Esuriit kontrolującego Trianai i efekt jego działań
        * Kapsel potrzebuje godnej, uczciwej walki w mechanice walki z inicjatywą i wymiarami
        * KTO przejmie kontrolę nad arkologią Nativis - Laurencjusz i Tymon czy Infernia?
    * Tactical
        * ?

### Co się stało i co wiemy

* Frakcje Arkologii
    * **Laurencjusz i Tymon + Izabella + Lobrak (LITL)**
        * impuls: take over, destroy Infernia's reputation, use powers to maneuever
            * mentalna: infernianie atakują arkologię, ludzie przechodzą na stronę Laurencjusza
            * polityczna: infernia zniszczyła ich ludzi, infernia nie ochroniła, Kidiron to potwór
            * Infernia nie ma surowców
        * slogan: "Kidiron musi zapłacić za zbrodnie!"
    * **Karol Lertys: 73-letni "dziadek" Celiny i Jana; kiedyś kapitan aspiriańskiego statku OA Erozja Ego + Celina + Janek**
        * impuls: fortyfikacja, tępienie Birutan, tępienie Hełmów, pozyskać surowce
            * kolejne tereny
        * slogan: "Wpierw ochrona, zwalczanie Birutan, odbudujemy naszą arkologię; rewolucja? XD".
    * **Szczury Arkologii**
        * impuls: loot, fortify, hide
            * loot, attack Lertys
        * slogan: "it is our time"
    * **Birutanie**
        * impuls: destroy and plunder
            * destroy and disassemble, force of nature
        * slogan: "DEVOUR"
    * **Rebelianci**
        * impuls: atak na LITL i Infernię
            * atak z zaskoczenia
        * slogan: "KIDIRON MUST DIE! Infernia must die!!!"
    * **Hełmy Draglina**
        * impuls: retake with iron fist. Destroy ALL opposition. Purge their ranks.
        * slogan: "Tylko Kidiron. PRAWDZIWY Kidiron."

### Co się stanie (what will happen)

* .S1: Ardilla utrzymuje Kidirona
* .S2: Eustachy ratuje Kalię jak bohater

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Marcel Draglin: oficer Kidirona dowodzący Czarnymi Hełmami, dumny "pies Kidirona"
    * (ENCAO: --+00 |Bezbarwny, bez osobowości;;Bezkompromisowy i bezwzględny;;Skupiony na wyglądzie| VALS: Family (Kidiron), Achievement >> Hedonism, Humility| DRIVE: Lokalny społecznik)
    * "Nativis będzie bezpieczna. A jedynym bezpieczeństwem jest Kidiron."
    * Najwierniejszy z wiernych, wspiera Rafała Kidirona do śmierci a nawet potem. Jego jedyna lojalność jest wobec Rafała Kidirona. TAK, to jego natura a nie jakaś forma kontroli.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Arkologia jest w tej chwili bardziej po stronie Laurencjusza niż Wujka.

1. Laurencjusz się pokazał się a Wujek nie.
2. Mowa Laurencjusza i Tymona
    * "Rafał Kidiron był zbrodniarzem" - i dowody.
    * "Infernia zawiodła. Nie obronili nas. Więcej - oddali naszych ludzi na pożarcie." (klip Eustachy oddający ludzi Infiltratorowi)
    * "Dlatego czas na nowe - jak wcześniej zbrodniczym skrzydłem Arkologii zarządzali Kidiron i Korkoran, tak teraz Kidiron i Korkoran ją uratują"

.

* Eustachy: "Eks-Piraci mają zdjąć Szczury z ulic. Najlepiej niech się podda. Pacyfikujemy."
    * Ardilla: "zostaw ludzi w arkologii. Nie bądź jak Kidiron!"
* Ardilla: "Eustachy powinien porozmawiać z Draglinem. Przejąć władzę w imieniu Kidirona."
* Ardilla -> zdobyć dane o Birutanach. (Ardilla - ranny Ralf jest jej cieniem)

Eustachy. Bez problemu dociera do Draglina. Hełmy reagują nerwowo ale Eustachego puszczają. Draglin:

* Draglin: Jeśli przyszedłeś wspierać swojego żałosnego kuzyna i żałosnego Kidirona, to... to Cię nie znam
* Draglin: Możesz być arogancki, ale nie jesteś idiotą. Jesteś dalej lojalny Rafałowi Kidironowi?
* Eustachy: Nie jestem lojalny Kidironowi a arkologii.
* Draglin: (podniósł przykrywkę hełmu by E. zobaczył że się uśmiechnął) To jest to samo. Kidiron jest arkologią.
* Eustachy: Uratowaliśmy Kidirona. Załoga Inferni nie dała dupy. Jest ukryty w bezpiecznym miejscu - Infiltrator może spróbować naprawić robotę.
* Draglin: (szeroki uśmiech) Wiedziałem, że to oddanie tych kolesi to była dywersja.
* Eustachy: Powiedzmy... granie na czas i pozbycie się Infiltratora. Druga sprawa - na dowód: to są dane od Kidirona. Specjalnie na tą sytuację.

Tp +3:

* V: (p): Draglin jest przekonany, że Eustachy mówi prawdę.
* X: (p): Draglin będzie współpracował. Z Eustachym a nie z wujkiem.
* V: (m): Draglin BĘDZIE współpracował z pełnych sił z Eustachym.

(-> Tr +Z)

* V: Eustachy wspierany przez Draglina i Wujka wygląda na największą frakcję o największym potencjale na ustabilizowanie Arkologii.
    * (duży bonus do przekonania Lertysów)
    * (przypina WSZYSTKIE dalsze zbrodnie Eustachemu)

Ardilla. Łączy się z Infernią by zbadać Birutan. W SUMIE SZARE OSTRZE JEST BEZPIECZNIEJSZE XD. I ma część danych Kidirona!!! A JA MAM KODY KIDIRONA!!!

* Tr +3:
    * V: Ardilla ma enumerację Czarnych Hełmów o potencjale Birutan
    * (+Z - Eustachy dodaje informacje o tym którzy Birutanie) Vz: Ardilla wie DOKŁADNIE którymi się zająć i na których skupić. Lista Czarnych Hełmów którzy ulegli Transformacji.

Szczury ścierają się z piratami. Szczury próbują szczurzyć, okradać itp a piraci próbują ich spacyfikować.

Tr (połowa Xr: ranne Szczury połowa Xg: propaganda Laurencjusza) Z +3Og (mroczna strona):

* XgXg: (ZAŁOGA INFERNI) "Masakruje obywateli Arkologii którzy uciekają przed Birutanami. Załoga Inferni nie dba o ludzi"
* V (p): Szczury nie dały rady wyrządzić dużych zniszczeń
* Xr: (p): CZĘŚĆ PIRATÓW poszła robić krzywdę ludziom.
    * Oni byli przesłuchani. Oni nie chcieli krzywdy. A JEDNAK TO ZROBILI.
* Xr: (m): HIDDEN MOVEMENT. Dark Seeds magią mentalną w piratach.
* (+2Vg) V (m): Szczury spacyfikowane. Odechciało im się lootować.

Eustachy jest zaskoczony ruchem załogantów Inferni (eks-piratów). Eustachy --> wiadomość od kuzyna Tymona.

* Tymon: Eustachy, kuzynie, opanowujesz arkologię?
* Eustachy: No.
* Tymon: Czemu my w ogóle walczymy? Ty masz Infernię, my mamy Arkologię, Kidiron jest zbrodniarzem...
* Eustachy: My?
* Tymon: Ja i Laurencjusz. Czemu do nas nie dołączysz? Czemu WSZYSCY nie dołączycie? Razem uratujemy Arkologię.
* Eustachy: Z reguły słabszy dołącza do silniejszego, by zagwarantować bezpieczeństwo. Jak chcesz sojuszu, Wy podporządkujecie się mi a nie ja Wam. A czemu myślisz że Kidiron nie żyje?
* Tymon: Bo... wysadzili go? Tak skutecznie? W sensie - takiego wybuchu by nie przeżył. A jego miragenty zostały zniszczone.
* Eustachy: Jak myślisz że by zareagował jak żyje a Wy odpierdzielacie manianę?
* Tymon: Eustachy, to nie ma znaczenia jeśli przejmiemy kontrolę. Z TObą. Kiedyś był Kidiron + Wujek. Teraz - będizemy my + Ty. Kidiron nie dowodził wujkiem. My nie chcemy dowodzić Tobą.
* Eustachy: Nie chcecie mną dowodzić, kto kieruje Wami? (z całym szacunkiem nie wpadlibyście na to, w trakcie ataku Infiltratora to nie Wasz poziom, przerasta Wasze możliwości poznawcze)

.

Tr Z (znasz Tymka, skille taktyczne, byłeś tam) +3:

* V: Eustachy - Tymek oraz Laurencjusz zadziałali szybciej niż wujek. Niż Ty. Oni działali jakby się spodziewali ataku Infiltratora.
* V: Tymek i Laurencjusz NAPRAWDĘ wierzą, że Eustachy i Ardilla mogą do nich dołączyć. Że się podzielą. Że będzie dobrze. ALE są bardziej chętni władzy niż utrzymania A+E w rodzinie. Zrobią wszystko by Was nie krzywdzić, ale np. infamia, więzienie, spokornieli... to jest ok.
* X: Oni są przekonani, że Lobrak jest _niewielkim kosztem_. Nie kochają go, ale jest OK.
    * Eustachy: czemu współpracujecie z Lobrakiem
    * Tymon: (zdziwiony) ma pieniądze, ma dziewczyny, ma oddział... czemu nie? Chce tylko pewnych uprawnień i możliwości, ale dalej my dowodzimy. A zna się na tym, doradzał Kidironowi.
    
Celina dyskutuje z Ardillą.

* Celina: Ardillo? Podobno... EUSTACHY dowodzi siłami arkologii w imieniu Kidirona? I Ty mu pomagasz?
* Ardilla: Można tak powiedzieć... nie mogę powiedzieć bym była bardzo szczęśliwa z tgo powodu
* Celina: Ale czemu wspieracie KIDIRONA?
* Ardilla: Bo bałagan jaki zrobił w arkologii tylko on może powstrzymać. Uwierz, wiem co mówię
* Celina: Nie Ty pierwsza to mówisz.
* Ardilla: Kto wcześniej tak mówił?
* Celina: Tymon Korkoran /z jadem
* Celina: Eustachy to Kidiron 2.0
* Ardilla: Wujek nie chce. To kto?
* Celina: Mój dziadek..? Wujek mu ufa. On też nie chce, ale... był kapitanem statku, to się... liczy. ALE NIE KIDIRON!
* Celina: Na szczęście Kidiron nie żyje!
* Ardilla: No... nie do końca...
* Celina: (straciła serce)
* Ardilla: Arkologia potrzebuje silnego przywódcy który ją poskłada. Kidiron ma wady ale on jest gotowy zmienić władzę jak się znajdzie ktoś lepszy. Z tym co mamy możemy go zdjąć w dowolnym momencie. Musimy mieć czas na lepszą opcję. Ktoś na szybko to Tymon u władzy.
* Celina: (małe załamanie) tylko nie Kidiron... Kidiron... nie zdejmiemy go, nie da się go zdjąć. NIE DA SIE! Bo się nie dało i nie będzie dało. On jest... ponadczasowym plugastwem.
* Ardilla: Nie stawaj po stornie Kidirona, ale stań po stronie naszych ludzi. Przeciw Birutanom.

Tr Z +3:

* V: (p) Lertysowie pomogą zwalczyć Birutan i zapewnić bezpieczny port, ale to wszystko.
* X: (p) Link Celina + Janek - Ardilla + Eustachy jest rozerwany.
* X: (m) Ardilla jest przez Lertysów (frakcję) zakwalifikowana jako lojalistka Kidirona tak jak Eustachy i Draglin.
* Vz: (m) Ardilla dostaje wsparcie Lertysów.

.

* Birutanie są tępieni
* Szczury są opanowane i złamane
* Infernia ma fatalną reputację, ale to działa
* Część załogi Inferni jest "szalona". Z jakiegoś powodu krzywdzą ludzi
* Lertysowie i Draglin wspierają Infernię.
    * Ale Lertysowie się odmontowali od Inferni - nie wspierają Eustachego mimo że będą wspierać Wujka

## Streszczenie

W Nativis panuje wojna domowa - Laurencjusz i Tymon próbują przejąć władzę jako 'nowi lepsi Kidiron + Korkoran'. Eustachy i Ardilla konsolidują lojalistów arkologii - Lertysów, Draglina, Szczury - tworząc potężną siłę pro-Rafał Kidiron. Gdy Laurencjusz i Tymon wspomagani przez Lobraka próbują Eustachego przekonać do współpracy, Eustachy odrzuca. Oddziały Inferni z jakiegoś powodu krzywdzą cywili (co dziwi Eustachego i martwi Ardillę) i zbrodnie są dobrze nagłośnione, spadając na ręce Eustachego. Birutanie są odparci. Szczury są złamane. I wiemy, że L+T współpracowali z Infiltratorem...

## Progresja

* Ardilla Korkoran: oficjalnie i powszechnie rozpoznawana jako sympatyczka i sojuszniczka Rafała Kidirona
* Eustachy Korkoran: oficjalnie i powszechnie rozpoznawany jako sympatyk i sojusznik Rafała Kidirona
* Eustachy Korkoran: buduje się coraz większa opinia, że jest to brutalny i niebezpieczny dowódca (w większości robota Laurencjusza który maksymalizuje czyny Eustachego).
* Jan Lertys: wspiera Bartłomieja Korkorana i tylko jego z rodziny Korkoranów. Wszyscy inni są jego zdaniem lojalistami Kidirona.
* Celina Lertys: wspiera Bartłomieja Korkorana i tylko jego z rodziny Korkoranów. Wszyscy inni są jej zdaniem lojalistami Kidirona.
* Karol Lertys: wspiera Bartłomieja Korkorana i tylko jego z rodziny Korkoranów. Wszyscy inni są jego zdaniem lojalistami Kidirona.

## Zasługi

* Ardilla Korkoran: używając Szarego Ostrza ma listę Birutan; przekonała Lertysów do pomocy przeciw nim. Próbuje chronić Szczury Arkologii przed brutalnością Eustachego i jego piratów, ale nadaremno. Eustachy zrobił co chciał. Poświęca personalną reputację dla jutrzejszej żywej i sprawnej arkologii. Uważa, że Rafał Kidiron jest jedynym co faktycznie utrzyma arkologię Nativis...
* Eustachy Korkoran: buduje sojusze dla Kidirona by arkologia odzyskała siłę; dla Draglina wziął na siebie dowodzenie (zamiast wujka). Przegrywa wojnę propagandową z Laurencjuszem i Tymonem, wychodzi na brutalnego potwora w stylu Kidirona. Jest jednak lojalny wujkowi i arkologii.
* Laurencjusz Kidiron: Polityczna propaganda i 'nowy lepszy Kidiron'. Przejął radiowęzeł by kontrolować sygnał. Szczuje na Infernię i na Rafała Kidirona. O dziwo, bardzo wiele ludzi mu ufa.
* Tymon Korkoran: zdradził Infernię i dołączył do Laurencjusza. Zależy mu na przekonaniu Eustachego do sojuszu. Wierzy, że Lobrak to tylko doradca i dał się zaślepić Izabelli.
* Bartłomiej Korkoran: zajmuje główne komponenty Arkologii by zapewnić jej przetrwanie (i oba statki - Infernię i Szare Ostrze). Gdy Draglin obiecał współpracę pod Eustachym, oficjalnie oddał się do dyspozycji Eustachego. Ale robi swoje.
* Karol Lertys: mimo ogromnej nieufności i niechęci do Kidirona, przekonał swoich ludzi że muszą współpracować z Infernią. To ważniejsze - muszą uratować Arkologię przed kimś GORSZYM (Lobrakiem).
* Celina Lertys: czuje się zdradzona przez Ardillę i nie przyjmuje jej argumentów - Rafał Kidiron MUSI zostać odsunięty od władzy. Dziadek przekonał ją, że muszą pomóc Ardilli, ale Celina rozerwała przyjaźń z Ardillą.
* Marcel Draglin: najwierniejszy człowiek Rafała Kidirona. Szef Czarnych Hełmów. Jak tylko dowiedział się że Eustachy osłonił Kidirona i chce mu oddać kontrolę, Marcel w pełni poszedł za Eustachym.
* Tobiasz Lobrak: wie, że Bartłomiej Korkoran nim gardzi, więc połączył siły z Laurencjuszem i Tymonem, oddając im Izabellę do zabawy. CHYBA ma coś wspólnego z Infiltratorem. On lub L+T.
* Izabella Saviripatel: piękna egzotyczna piękność (17?) pod kontrolą Lobraka, oddana przez Lobraka Tymonowi i Laurencjuszowi; nie wygląda na szczególnie szczęśliwą.
* Ralf Tapszecz: ranny i niewidoczny; jest cieniem Ardilli.

## Frakcje

* Infernia Nativis: by dostać wsparcie Draglina w ratowaniu arkologii, Wujek oddał się pod komendę Eustachego i Eustachy jest siłą dominującą w Nativis. Tymon się odłączył. O dziwo, część piratów obraca się przeciwko ludziom co robi straszny cios reputacyjny Eustachemu i samej Inferni Nativis.
* Szczury Arkologii Nativis: pomiędzy Czarnymi Hełmami, Infernią i Birutanami. Próby lootowania skończyły się ostrym potrzaskaniem, ranami i rozbiciem. Szczury przez jakiś tydzień liżą rany i przestają być tymczasowo spójną siłą.
* Czarne Hełmy Nativis: dowodzi nimi Marcel Draglin. Ścisły sojusz z Eustachym, do poziomu oddania się mu pod dowodzenie. Impuls: odbić arkologię żelazną pięścią. Jedynym przełożonym jest PRAWDZIWY Kidiron.
* Birutanie Dystryktu Glairen: ich siły w ramach Nativis zostały zmiażdżone w pył przez kombinację Lertysów oraz Hełmów Draglina.
* NeoNativis KiKo: Tymon i Laurencjusz tworzą tą frakcję z Lobrakiem, częścią Hełmów i dodatkowymi siłami; chcą przejąć kontrolę i obrzydzić Kidirona. Wyraźnie wiedzieli o Infiltratorze. Opanowali radiowęzeł w Nativis i próbują politycznie przejąć Nativis.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Nativis
                        1. Poziom 1 - Dolny
                            1. Północ - Stara Arkologia: tymczasowo są tam Szczury, Birutanie oraz Celina i Janek. I Kidiron.
                                1. Stare Wejście Północne
                                1. Blokhaus E
                                1. Blokhaus F
                                    1. Szczurowisko
                            1. Wschód
                                1. Farmy Wschodnie
                            1. Południe: objęte przez Lertysów w sojuszu ze Szczepanem Falernikiem
                                1. Processing
                                1. Stacje Skorpionów
                                1. Magazyny
                            1. Zachód
                                1. Stare TechBunkry
                                1. Centrala Prometeusa
                        1. Poziom 2 - Niższy Środkowy
                            1. Południe
                                1. Engineering
                                1. Power Core
                            1. Wschód
                                1. Centrum Kultury i Rozrywki
                                    1. Bar Śrubka z Masła
                                    1. Stacja holosymulacji
                                    1. Muzeum Kidirona
                                    1. Ogrody Gurdacza
                            1. Północ
                                1. Bazar i sklepy
                            1. Zachód: Całkowicie pod kontrolą Draglina; tam też jest Lycoris Kidiron
                                1. Koszary Hełmów
                                1. Sektor Porządkowy
                        1. Poziom 3 - Górny Środkowy
                            1. Wschód
                                1. Dzielnica Luksusu: płonie, regularnie atakowana przez Szczury
                                    1. Ambasadorka Ukojenia
                                    1. Stacja holosymulacji
                                    1. Ogrody Wiecznej Zieleni
                            1. Południe
                                1. Medical
                                1. Life Support
                        1. Poziom 4 - Górny
                            1. Wschód
                                1. Obserwatorium Astronomiczne
                                1. Centrala dowodzenia: zniszczona przez Infiltratora
                                1. Radiowęzeł: pod kontrolą Laurencjusza
                            1. Zachód
                                1. Tereny Sportowe
                        1. Poziom 5 - Szczyt
                            1. Północ
                                1. Port kosmiczny: pod kontrolą Wujka

## Czas

* Opóźnienie: 1
* Dni: 1
