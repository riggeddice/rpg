## Metadane

* title: "EtAur - dziwnie ważny węzeł"
* threads: legenda-arianny
* gm: żółw
* players: kapsel, kić

## Kontynuacja
### Kampanijna

* [220610 - Ratujemy porywaczy Eleny](220610-ratujemy-porywaczy-eleny)

### Chronologiczna

* [220610 - Ratujemy porywaczy Eleny](220610-ratujemy-porywaczy-eleny)

## Plan sesji
### Co się wydarzyło

* .

### Co się stanie

* Maria ostrzega, że Termia ma jakieś konszachty powiązane z Syndykatem Aureliona
* Syndykat Aureliona spróbuje przechwycić Katorikaton (statek Neikatis)
* Adrianna Lemari splugawi napastników
* Całość statku zostanie zniszczona / przejęta; tylko Adrianna przetrwa

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Kontroler Pierwszy. Mechanicy Inferni. Diana -> Eustachy. Chce kilku przystojnych mechaników. Eyecandy. Eustachy -> "podanie w 3 egzemplarzach". "Załatwisz mi tych przystojnych mechaników? Szkoda by tych co mamy żeby coś im się miało stać..."

Eustachy poprosił dyskretnie Klaudię o 3 brzydkich ale kompetentnych mechaników...

TrZ+3:

* Vz: będą kompetentni i właściwi.
* V: Klaudii udało się dojść do tego, że Roland Sowiński próbował wsadzić jej człowieka.
* V: Roland nie wie że Klaudia i Eustachy wiedzą.

Maria x Eustachy: "Termia współpracuje z Syndykatem Aureliona, wystawia im jednostkę pomocniczą - potrzebna mi pomoc". Maria wyjaśniła, że Termia coś planuje i ona musi dojść do tego o co chodzi - to niezgodne z działaniami Orbitera.

Eustachy -> adm. Termia "czy to prawda że współpracujesz z Syndykatem i wystawiasz jej jednostkę pomocniczą?" Eustachy nigdy nie był zbyt dyskretny a ten manewr pozwoli na jednoznaczne wyczajenie o co chodzi z działaniami Termii. Wredna? Tak. Zdrajczyni? Nie.

Termia żąda, by Eustachy ją odwiedził w gabinecie. I faktycznie, Eustachy odwiedza Termię. Po wyjaśnieniu Eustachego, że wie o wszystkim Termia się przyznaje - bardzo nie lubi Syndykatu bo wchodzi na jej teren i skrzywdzili stację Atropos. Eustachy ściąga Klaudię na spotkanie - nie chce rozmawiać z Termią bez niej, bo ona i tak wszystkiego się dowie. W końcu Klaudia, nie?

Termia wyjaśnia im swoją nową zabawkę. Neuroviatoribus (viatoribus == passenger (lat)). Czarny śluz, infekcja, pasożyt znajdujący się w ŚRODKU osoby i sterujący osobą. Nadrzędny byt nad osobą. Termia chce użyć swojego neuroviatoribusa, by ten przeszedł przez niektóre osoby i dotarł do ważnego miejsca w Syndykacie.

Sytuacja:

* Jest jednostka cywilna, jakoś uzbrojona, którą Termia wystawiła. Na tej jednostce jest m.in. neuroviatoribus.
* Ta jednostka cywilna jest atrakcyjna z perspektywy Syndykatu. Termia zadbała o to - neuroviatoribus jest wewnątrz wartościowej osoby i są tam inne wartościowe osoby.
* Syndykat zaatakuje tą jednostkę. Zwie się ona Katorikaton.
* Cel: wprowadzić neuroviatoribusa do kapitana statku Aureliona.

Termia chce docelowo odnaleźć koloidową bazę Aureliona na astoriańskim księżycu (Elang). Podejrzewa, że gdzieś tam jest baza Syndykatu, ale nie ma pojęcia gdzie. Skoro Eustachy już zainteresował się sprawą, niech zatem Termii pomoże z wprowadzeniem neuroviatoribusa - Damian Orion zajmie się czymś innym.

Eustachy ma jednak inny plan: Infernia nie pomoże z agentem Termii. On się zajmie szukaniem koloidowej bazy inną ścieżką. Jej agent (neuroviatoribus) działa dobrze w rękach Termii i Oriona, niech robi to po swojemu. Infernia jednak ma kontakty na EtAur Zwycięskiej i jeśli koloidowa baza Aureliona jest na Elang to Eustachy może zadziałać przez ten księżyc. Plus, przemytnicy. A Syndykat jest nastawiony na dyskretne działania więc to może pomóc.

Ale po co? Termia nie widzi korzyści ze współpracy z EtAur. A przynajmniej do momentu aż Klaudia jej uzmysławia, że EtAur jest wejściem na planetę - i Syndykat może chcieć odciąć Orbiter od Astorii. I tu jest faktycznie ryzyko. A co sprawia, że Eustachy się nadaje do tego? Eustachy jest dziedzicem jednej z arkologii Neikatis, z sojuszu 'konkretna arkologia Neikatis' - 'Orbiter'.

Eustachy ustawia z Termią, że ona zapewni odpowiednie środki i mechanizmy by pomóc bazie EtAur. Termia zauważa, że przecież Orbiter sabotuje postępy EtAur. Klaudia na to: przybliżone wejścia, bazy, relacje, jakie handle ekonomiczne, powiązania, linki, co możemy stracić, ścieżki zaopatrzenia. Klaudia podnosi, że Aurelion może chcieć dostęp do planety i odciąć Orbiter.

Termia zmieniła zdanie. Przekierowała Eustachego i Klaudię na rozeznanie stacji księżycowej i zapewnienie, że Aurelion tam się nie wbije. Bo faktycznie, Orbiter nie może być odcięty od planety. A w tym czasie neuroviatoribus ma sprawić, że Aurelion skrzywdzi EtAur i odepchnie EtAur od potencjalnej współpracy z Syndykatem Aureliona.

Eustachy DODATKOWO gra na siebie. Zapewnić Inferni wszystko co bezpieczne - wejście w sojusz z EtAur na wypadek jakby były jakieś problemy z Orbiterem.

Klaudia na podstawie wiedzy o EtAur buduje listę rzeczy tego co EtAur będzie potrzebować. Pokazać, że Orbiter wie i im zależy:

ExZ(byliśmy) +4 (bo wbijali się do systemów) +5Og (Termia chce to osiągnąć):

* X: EtAur są nieufni wobec Orbitera - Orbiter działał przeciwko nim
* X: EtAur niekoniecznie chce się wiązać - ma przemytników, ma planetę, ma samodzielność
* X: Termia ma złą reputację - EtAur są szczególnie nieufni wobec niej (a ona jest tą która chce współpracować).
* X: Aurelion już do nich dotarł i oni ufają bardziej Aurelionowi niż Orbiterowi
* V: EtAur jest przekonana do tego, że Infernia chce z nimi współpracować i jest po ich stronie.
* Vz: EtAur są chętni do współpracy z admirał Termią - ona jest INNA niż reszta Orbitera (bo to prawda)

Infernia dociera do EtAur. Parczak jest wdzięczny Eustachemu i Klaudii BEZPOŚREDNIO. Parczak powiedział, że z przyjemnością zapewni Inferni bezpieczny port w EtAur - nawet zapewni ochotników z planety. Niekoniecznie bardzo kompetentnych, ale chętnych. A on zapewni jakieś miejsce naprawcze dla Inferni. Półautomatyczna stacja na orbicie geostacjonarnej. Tam się zadokuje pintki, personel naprawczy - coś dla Inferni. Winda kosmiczna z konta Orbitera (admirał Termia).

## Streszczenie

Maria zainteresowała się planem adm. Termii wskazującym na to, że ta zdradza Orbiter z Syndykatem Aureliona. Eustachy (do którego przyszła) zapytał Termię wprost i doszedł do tego, że Termia szuka koloidowej bazy Aureliona na księżycu Elang swymi bezdusznymi metodami. Eustachy i Klaudia wskazali Termii, że baza EtAur jest potencjalnie punktem kontaktowym Syndykatu z Astorią i tu jest ryzyko. Eustachy doprowadził do kontaktu Termia - EtAur, ale też gra na współpracę Inferni i EtAur.

## Progresja

* Roland Sowiński: wpakował na Infernię kilku kompetentnych i brzydkich mechaników. Nie wie, że Klaudia i Eustachy wiedzą, że to jego ludzie.
* Aleksandra Termia: weszła w sojusz z EtAur Zwycięską, acz ogromnym kosztem reputacyjnym.
* Eustachy Korkoran: 

### Frakcji

* OO Infernia: ma na pokładzie kilku kompetentnych i brzydkich mechaników przesłanych przez Rolanda Sowińskiego; ale Klaudia i Eustachy wiedzą którzy to.
* EtAur Zwycięska: współpracuje z admirał Termią ORAZ z Syndykatem Aureliona, rozgrywając te frakcje przeciwko sobie. Dąży do niezależności.

## Zasługi

* Eustachy Korkoran: gdy Maria przyszła doń z konspiracją Termia - Aurelion, poszedł do Termii bezpośrednio. Powiedział jej, że nie pójdzie za jej planem; lepiej znaleźć koloidową bazę Aureliona używając przemytników i jego środków z bazy EtAur. Zbudował sojusz Infernia - EtAur i gra pod siebie i pod Infernię.
* Klaudia Stryk: zaproszona przez Eustachego do konspiracji Termii wskazała jak istotna jest baza EtAur dla Orbitera i Syndykatu. Potem przeanalizowała czego EtAur pragnie by sojusz EtAur z Infernią był jak najlepszy (i by jak najbardziej pomóc sojuszowi EtAur - Termia).
* Maria Naavas: podejrzewa adm. Termię o grę przeciwko Orbiterowi i za Syndykatem Aureliona; przyszła z tym do Eustachego i chyba zaczęła sojusz Termia - EtAur?
* Aleksandra Termia: wielopoziomowy plan polegający na infiltracji statku Aureliona przez neuroviatoribusa; przekonana przez Eustachego i Klaudię, że konieczny jest sojusz Orbitera z EtAur. Zaczęła wspierać EtAur środkami Orbitera, łamiąc zasadę "Orbiter to jedyna siła dostępu do planety".

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 2
* Dni: 2

## Konflikty

* 1 - Eustachy poprosił dyskretnie Klaudię o 3 brzydkich ale kompetentnych mechaników...
    * TrZ+3
    * VzVV: mamy kompetentnych mechaników, agentów Rolanda - ale wiemy o tym
* 2 - Klaudia na podstawie wiedzy o EtAur buduje listę rzeczy tego co EtAur będzie potrzebować. Pokazać, że Orbiter wie i im zależy
    * ExZ(byliśmy) +4 (bo wbijali się do systemów) +5Og (Termia chce to osiągnąć)
    * XXXXVVz: EtAur chce być samodzielna i nie ufa Orbiterowi; woli Syndykat. Zła reputacja Termii + Aurelion do nich dotarł. ALE współpracują z Infernią i... Termią a nie Orbiterem.
