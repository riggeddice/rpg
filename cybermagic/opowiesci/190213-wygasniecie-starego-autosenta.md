## Metadane

* title: "Wygaśnięcie starego autosenta"
* threads: brak
* motives: lojalnosc-syntetycznych-intelektow, stara-jednostka-bojowa, efemeryda-z-przeszlosci
* gm: żółw
* players: kić, kapsel, pawel_f

## Kontynuacja
### Kampanijna

* [190213 - Herbata, grzyby i mimik](190208-herbata-grzyby-i-mimik)

### Chronologiczna

* [190213 - Herbata, grzyby i mimik](190208-herbata-grzyby-i-mimik)

## Projektowanie sesji

### Past

1. Jadwiga wpadła do dziury i uruchomiła uszkodzonego autosenta; konstruminus ją ratuje.
2. Autosent łapie Jadwigę i jedzie z nią do Skałopływu

### Dark Future

1. Autosent jest zniszczony
2. Jadwiga nie żyje
3. Baltazar dostaje medal i pochwałę
4. Skałopływ jest ciężko uszkodzony

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (20:50)

_Skałopływ, Strażnica_

Powtarzam PAST:

1. Jadwiga wpadła do dziury i uruchomiła uszkodzonego autosenta; konstruminus ją ratuje.
2. Autosent łapie Jadwigę i jedzie z nią do Skałopływu

W czasie rzeczywistym:

Atena ostrzegła Pięknotkę. Coś dużego się zbliża, coś, co wykrył Epirjon. Ona conscriptowała magów w okolicy - i niestety też Baltazara. Zbliża się stary automatyczny sentinel, autosent. Placówka nie ma z nim żadnych szans. Ludność cywilna będzie wysłana do pieczar.

Tchórz Baltazar chce walczyć. Ma zakazaną broń - Skaża psychotronikę, niszczy pancerz i powoduje Skażenie. Pięknotka weszła w konflikt z Baltazarem. (Tr:SS). Udało się, ale autosent jest już tutaj. Za dużo walki z własnym sojusznikiem (Baltazarem), za mało walki przeciwko autosentowi... no i okazało się, że autosent przejął kontrolę nad jednym konstruminusem który towarzyszył Jadwidze (?!).

Autosent szuka ludzi, wraz z konstruminusem. Ale nie jest niebezpieczny - sam nie atakuje. Nie otwiera ognia, jedynie monitoruje teren i czegoś wyraźnie szuka. A na pokładzie starej maszyny umiera Jadwiga, konsekwentnie.

Przybył Dariusz i shackował komunikację z autosentem. Dowiedział się o co chodzi i co się dzieje.

* Autosent ma ciężko uszkodzoną psychotronikę. Nie wie co się dzieje. Nie rozróżnia przyjaciół od wrogów i nie atakuje pierwszy.
* Autosent próbuje uratować Jadwigę, ale nie do końca wie jak to zrobić. Szuka najpewniej lekarza czy apteczki.
* Nie ma kontaktu z psychotroniką tej jednostki. Aha, jak on wybuchnie...

Rafał poszedł jako przynęta z wtyczką do autosenta. Pięknotka go zacharakteryzowała. Gdy Rafał wyszedł w pole widzenia, "wrogi konstruminus" strzelił mu w plecy. Autosent wystrzelił rakietę i konstruminus wyparował (Tp:12,3,2=SS). Autowar wysłał swojego konstruminusa i Rafał trafił na pokład autosenta.

Szczęśliwie, Rafał wziął ze sobą apteczkę a Baltazar został kontaktem komunikacyjnym pomiędzy lekarzem (w pieczarach) a Rafałem. Rafał bez szczególnych umiejętności dzięki pokierowaniu przez lekarza i serio dobrej klasy apteczce dał radę uratować i ustabilizować Jadwigę.

Gdy autosent wykrył, że Jadwiga i Rafał są zdrowi to zaczął odjeżdżać z miasta. Nowy plan - niech oni się katapultują ze środka autosenta, z pozycji kapitana. Poszli więc tam, przy całkowitym ignorowaniu ze strony autosenta. Jednak zauważył, że część psychotroniki kojarzy panią kapitan - odpytuje ich co się dzieje. Przy rozmowie (gdzie Antoni rozmawiał przez Rafała) wyszło, że niejaka Teresa Tevalier dowodziła autosentem. Nie chciała, nie była oficerem bojowym a logistycznym - ale KTOŚ musiał.

Czyżby Teresa się sprzęgła z autosentem? Niestety, okazało się, że to tylko pętla. Echo. Nie ma tam Teresy, jest jej echo, efemeryda.

Przeprowadzili Teresę łagodnie przez procedurę wyłączenia silników i autosent się wyłączył. Pięknotka natychmiast przekonała Antoniego i Rafała, by oni przekazali autosenta pod kontrolę Ateny - a na pewno pod Cieniaszczyt, by pozbyć się politycznie problemów z destabilizacją sytuacji w regionie. I się udało.

I za to wszystko Baltazar dostał nagrodę i order. Serio...

Wpływ:

* Ż: 21 (11)
* KKP: 4

**Sprawdzenie Torów** ():

2 Wpływu -> 50% +1 do toru. Tory:

1. Autosent jest zniszczony
2. Jadwiga nie żyje
3. Baltazar dostaje medal i pochwałę
4. Skałopływ jest ciężko uszkodzony

**Epilog** ()

* by nie destabilizować sytuacji politycznej tutaj, autosent jest wysłany do Cieniaszczytu
* Baltazar dostaje order
* Baltazar docenia wpływ Postaci Graczy i jest im wdzięczny

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
|               |         |             |

Czyli:

* .

## Streszczenie

Stary autosent się jakoś zregenerował by uratować Jadwigę przed śmiercią. Zagroził Skałopływowi, szukając medyka dla Jadwigi. Zespołowi udało się postawić Jadwigę na nogi i wyprowadzić autosenta poza Skałopływ, po czym przeprowadzono echo Teresy Tevalier przez procedurę dezaktywacji autosenta. Docelowo, autosent trafił do Cieniaszczytu.

## Progresja

* .

## Zasługi

* Pięknotka Diakon: przełożona operacji neutralizacji autosenta. Ku swojemu wielkiemu zdziwieniu, lokalne siły były kompetentne. Przekazała autosenta Cieniaszczytowi.
* Antoni Kotomin: koordynator i taktyk; wydobywał dużo danych o przeszłości autosenta. Chciał uratować Teresę i nie był szczęśliwy, że się nie dało.
* Rafał Bobowiec: mięśniak i żołnierz; przyjął ranę by wejść na autosenta i uleczył Jadwigę ZUPEŁNIE SIĘ NA TYM NIE ZNAJĄC. Nawiązał kontakt z echem Teresy Tevalier.
* Jadwiga Pszarnik: szukając nowego miejsca na pieczarę z grzybami wpakowała się w ciężką ranę. Autosent ją uratował a Rafał postawił na nogi.
* Dariusz Bankierz: okazało się, że posiada umiejętności hackerskie. Nawiązał kontakt z autosentem i dowiedział się co jest grane. 
* Baltazar Rączniak: tchórz a nie terminus. Ale w sumie chciał wysadzić autosenta zakazaną bronią. Opóźnił zespół i... skończył z orderem
* Atena Sowińska: zlokalizowała autosenta oraz przekazała temat Pięknotce.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, SW
                    1. Granica Anomalii
                        1. Skałopływ: w okolicach Skałopływu pojawił się śmiertelnie niebezpieczny autosent. Trafił do Cieniaszczytu po neutralizacji.

## Czas

* Opóźnienie: 2
* Dni: 1
