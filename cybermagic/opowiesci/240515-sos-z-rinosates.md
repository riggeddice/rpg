## Metadane

* title: "SOS z Rinosates"
* threads: mscizab-rywal-arianny
* motives: the-corruptor, the-addicted, dziwna-zmiana-zachowania, hartowanie-verlenow, kult-mroczny, koniecznosc-pelnej-dyskrecji, ratowanie-dzieciaka
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [210324 - Lustrzane odbicie Eleny](210324-lustrzane-odbicie-eleny)
* [240801 - Bohaterska śmierć Wężomysza](240801-bohaterska-smierc-wezomysza)

### Chronologiczna

* [210324 - Lustrzane odbicie Eleny](210324-lustrzane-odbicie-eleny)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * Lydia the Bard "Isabella's Villain Song": 
        * "Stand upstraight, stick it through | Uphold the family that hurts and harms you and asks too much of you"
        * "How could you when I was just a child | Support the family don't stray display obey you must be perfect"
        * "what could you possibly expect was gonna happen when you made me | tend to a plant with poison and expect a flower are you crazy?"
        * Kluczem jest **smutek** oraz przekroczenie "breaking point"
    * Inspiracje
        * "Black Swan" (film), o perfekcji i obsesji perfekcji i jak daleko się można posunąć
        * Vinerva @ Shadows of the Forbidden Gods
            * Vinerva to bogini, która daje bogactwo, zdrowie i potęgę, skażając serca i prowadząc do zniszczenia
            * Vinerva, technicznie, jest **the-corruptor**. Czyli ona pragnie Cię wzmocnić by Cię usidlić.
            * Złapała i usidliła Mścizęba
* Opowieść o (Theme and vision):
    * "Dziecko, nigdy dość dobre, naciśnięte za mocno, próbuje wygrać szacunek i miłość. Ale nie daje rady."
    * "The child who receives no warmth will burn the village just to feel its warmth"
        * Mściząb bardzo pragnął być jak Arianna czy Viorika czy inni wartościowi członkowie Verlenów. Ale nie jest w stanie.
        * 2 lata temu Mściząb natrafił na Ariannę gdy robił coś bardzo głupiego i ona go upokorzyła.
        * Mściząb miał pecha natrafić na istotę [Interis/Esuriit]. Coś, co "daje" mu moc w zamian za ofiary i odbiera mu człowieczeństwo.
        * Mściząb pragnie osiągnąć swój poziom kompetencji i umiejętności. Pragnie być JAK ARIANNA. Nie ma po co żyć jako śmieć.
    * Lokalizacja: Verlenland
* Po co ta sesja?
    * Jak wzbogaca świat
        * Pokaże, jak Verlenowie traktują "słabe" jednostki
        * Pokaże wysoki poziom autonomii w rodzie Verlen
        * Pokaże pomocnych Verlenów, którzy chcą pomóc - i tych, którzy trzymają się okrutnych reguł Verlenlandu, starej daty.
    * Czemu jest ciekawa?
        * Pokazuje Ariannie i Viorice ich własne lustro
        * Dylemat: uratować OFIARY młodego Verlena czy samego Verlena czy spróbować uratować wszystko ryzykując utratę wszystkiego?
    * Jakie emocje?
        * "Mściząb znowu coś odpierniczył..." -> "...co on zrobił... czemu..."
            * Od lekkiej frustracji do pełnego szoku - Fallen Verlen
            * I zrozumienie DLACZEGO upadł. I swojej roli w tym upadku.
        * "Coś tu jest poważnie nie tak, ale damy radę" -> "CZEMU NIE WEZWAŁYŚMY POMOCY!"
            * Od przekonania o możliwościach do uzmysłowienia sobie z CZYM walczą
            * Dlaczego Verlen zarządzający tym terenem nic nie robi i go nie ma?!
        * "Nie chcę musieć wybierać Miasteczko / Mściząb!"
            * prawdziwa pokusa związana z Darami Leniperii - I can get it all
            * dylemat. Mściząb w końcu mógł być 'sensowny', ale nie miał szczęścia
        * "Rozwalę tego potwora!!! Za Mścizęba!!!"
            * widząc, co Leniperia zrobiła Mścizębowi.
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * the-corruptor: Leniperia, istota Esuriit/Interis. Stopniowo pozbawia Cię wszystkiego, wzmacniając Cię darami, jeśli je przyjmiesz. Wzmacnia Twoją obsesję, zabierając wszystko inne. Wzmacnia Cię krwawymi ofiarami. Tym razem dorwała Mścizęba, młodego Verlena, który rozpaczliwie pragnął pokazać Ariannie że jest lepszy od niej
    * the-addicted: Mściząb Verlen. Jego narkotykiem jest potęga i akceptacja Rodu, jest możliwość upokorzenia Arianny i udowodnienia, że jest lepszy od niej. Coraz bardziej traci wszystko by stać się jak ona. Unchained.
    * dziwna-zmiana-zachowania: Mściząb zrobił się kompetentny oraz bardzo agresywny wobec Arianny; rywalizuje z nią i może nie daje rady, ale jest bliżej niż powinien być. Jest też okrutniejszy.
    * hartowanie-verlenow: tu widać, co się dzieje, gdy 'słaby niegodny Verlen' napotyka siłę, która próbuje z tego skorzystać. Jak łatwo 'słabi Verlenowie' przechodzą na Drugą Stronę. Wyraźna ich słabość.
    * kult-mroczny: stworzony sztucznie, przez Leniperię, ale zawsze; więcej osób należy do sił Potwora i więcej osób próbuje być lepszymi i płacą więcej i więcej. Część z nich 'nawrócił' Mściząb. Część przyjęło dary.
    * koniecznosc-pelnej-dyskrecji: Arianna i Viorika działają na terenie innego Verlena, któremu nie zależy tak na ludziach w swojej domenie. A one chcą zatrzymać Mścizęba jak on robi głupoty.
    * ratowanie-dzieciaka: Karasiożer Verlen jest zapatrzony w Mścizęba, jest powiązany z siłami na tym terenie. Mściząb przysposabia go do dołączenia do Leniperii.
* O co grają Gracze?
    * Sukces:
        * Uratować Mścizęba
        * Uratować Miasteczko
        * Karasiożer jest bezpieczny przed Leniperią
        * Odegnać Leniperię z Verlenlandu
    * Porażka: 
        * W służbie Leniperii
        * Śmierć zarówno Mścizęba jak i Miasteczka
        * Karasiożer przyjął dar Leniperii
* O co gra MG?
    * Highlevel
        * Chcę pokazać im słabe i mroczne strony Verlenów. Te fajne już znają.
    * Co chcę uzyskać fabularnie
        * Spróbuję dołączyć je do Leniperii. Niech się Skażą.
            * Używając Interis zabiorę im umiejętność ostrzeżenia o Leniperii.
        * Karasiożer dołącza do Leniperii.
        * Zarówno Miasteczko jak i Mściząb są zniszczone.
        * Nie są w stanie ostrzec nikogo przed Leniperią; ona nadal tu jest i się żywi.
        * Wszyscy są przekonani, że to wszystko jest winą Arianny - tak jak Elena.
        * Pojawia się Mroczny Oddział Leniperii.
* Default Future
    * Miasteczko zostanie zjedzone
    * Mściząb wciągnie pod kontrolę Leniperii Karasiożera Verlena
    * Leniperia będzie w stanie rozprzestrzenić się na kolejne miejsca; ma niewielki, dobrze ukryty oddział
* Dilemma: Mściząb CZY Miasteczko? Wezwać pomoc CZY samemu?
* Crisis source: 
    * HEART: "The child who receives no warmth will burn the village just to feel its warmth"
    * VISIBLE: "Znikają ludzie w niedalekiej okolicy"
        
### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

* Mściząb napotkał Leniperię na jednej z innych operacji. Jego głód pokonania Arianny sprawił, że Leniperia znalazła okazję.
* Mściząb przyjął kilka Darów Leniperii niszcząc tworzone przez nią istoty, dzięki czemu jego reputacja zaczęła rosnąć.
* Leniperia przybyła do Rinosates; zaczęła rozpuszczać macki. Mściząb - nieświadomy - wspiera Leniperię w zniszczeniu Rinosates.
* Karasiożer przybył z rozkazu Jastrzębiusza, niech się uczy od lepszych od siebie.
* Ludzie znikają, Mściząb zwalcza Spawnera, ale za wolno i nieskutecznie
* Leniperia skupia się na ludziach próbujących opuścić to miejsce
* Część ludzi wie o tym że "emigranci nie dotarli", ale Karasiożer ani Mściząb nic z tym nie robią
* Palaos decyduje się na ryzykowną wyprawę - niech Viorika lub Arianna pomogą...

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

* "The Spawner" - wprowadzenie potwora, pierwsza sesja
    * Faza 1: "Darkness Grows" (90 min)
        * Zespół dowiaduje się o problemie, bo Mściząb. Podobno pod kontrolą.
        * -> wprowadź odważnego pro-Verlena, mimo wszystko (ten delikwent jest na Twoim celowniku)
        * A+V są dyskretnie wprowadzone i przemycone? Ukryta 'baza'?
            * pokazanie Dekadia Invictus, Kłów, ogólnie starć i rozpadającej się tkanki społecznej
            * pokazanie, że ludzie znikają i lokalny Verlen + Mściząb sobie nie radzą
        * Ogólnie, budowa kontekstu okolicy
    * Faza 2: "Spawner falls" (75 min) <- Mściząb 
        * Tu celem Mścizęba jest skłonienie Karasiożera do wzięcia Darów Leniperii.
        * Operacja wykorzystania ludzi by zlokalizować i zniszczyć Spawnera
        * Uda się, ale straty...
        * Mściząb chce zniszczyć Dekadię Invictus, infiltracja + wysunąć ich na pierwszy ogień
        * -> aktywnie graj o zniszczenie miasteczka i porywanie ludzi przez Spawnera
        * -> aktywnie graj o to, by Mściząb zniszczył Spawnera
        * -> aktywnie graj o to, by Karasiożer pozyskał Dar Leniperii
        * -> aktywnie graj o śmierć cywili i Dekadię Invictus CHYBA że Gracze użyją Darów
        * -> pokaż, że to NIE KONIEC problemu 
            * (nie zgadza się ilość osób znikniętych)
            * (nie zgadzają się opowieści tych co zostali)
            * (potwór, który został)
* "The Mastermind" - wprowadzenie Leniperii, druga sesja
    * Interludium: to-co-będzie-potrzebne-do-demonstracji
    * Faza 3: "Konspiracja narasta"
        * ? 
    * Faza 4: "Mastermind ujawniony, wojna domowa"
        * ?
* Overall
    * chains:
        * energia i zasoby Zespołu: max 2
        * morale Zespołu: max 2
        * Dusza Karasiożera: (pragnę Daru - Mściząb ma rację - (Mściząb jest super) - Arianna jest super - można zostawić to innym)
        * To wszystko wina A+V: ((nie) - w sumie bo tu są - tak - gdyby nie one byłoby dobrze)
        * Służki Leniperii: ((nie) - wzmocnienie działa - muszę mieć więcej - upadłe Verlenki)
        * rozpad Rinosates: (pokój - (starcia) - przelew krwi - interwencja tiena - wojna domowa)
        * Armia Leniperii: (brak - byle co - (przydatna) - oddział - spora - legion)
        * problemy polityczne: ((brak) - czemu to są A+V - czemu szkodzą A+V - wygnanie stąd - wojna domowa)
    * stakes:
        * śmierć Mścizęba, śmierć Karasiożera, Dar Karasiożera
        * reputacja Arianny i Vioriki, problemy polityczne
        * rozprzestrzenienie się sił Leniperii
        * śmierć Rinosates, zniszczenia w Rinosates
    * opponent
        * Mściząb, Spawner, Leniperia
    * problem
        * Leniperia jest nie do ruszenia
        * Mściząb jest 'za daleko' i jest złamany przez Verlenów (a potem przez Leniperię)

## Sesja - analiza
### Mapa Frakcji

* Jastrzębiusz Verlen (Lokalny tien Verlen)
    * potężny politycznie izolacjonista, pro-darwinistyczny; ludzie to zasób
    * oneliner: "jeśli sobie nie poradzą sami, będę interweniował - ale zapłacą"
    * eksport: (irrelevant)
    * import: (irrelevant)
    * zasoby: silna siła ognia, magitech
    * agenda: 'stal hartuje się w ogniu', 'za wszystko płacisz'
    * cechy: 'piekło było na Ziemi, on stał na górze', skrajny militarysta
* Kult Kła Zemsty Rinosates (około 300 osób): 
    * pro-leniperiański kult stworzony przez Mścizęba Verlena
    * oneliner: "Siła i determinacja ugasi Twoje cierpienie"
    * eksport: bezpieczeństwo, szkolenia bojowe, broń, morale, usługi (patrol, fanatyczna walka)
    * import: ludzie (okolica), Dary Leniperii (Leniperia)
    * zasoby: oddział, fanatyzm, broń, znajomość lokalsów i terenu
    * agenda: ekspansja, ochrona okolicy
    * cechy: edgy warriors, fanatical
* Rinosates Decadia Invictus (około 80 osób):
    * (będzie ciekawiej, jeśli to miasteczko z populacją postnoktiańską; pasuje do przyszłej historii Arianny)
    * dekadiańska grupa samopomocowa dążąca do rozwiązania co tu się dzieje
    * oneliner: "noktianie powinni trzymać się noktian"
    * eksport: bezpieczeństwo, wiedza o anomaliach, wiedza o terenie, hideouts
    * import: sprzęt, intel, material, medykamenty
    * agenda: co tu się dzieje?, ochrona okolicy, ochrona swoich
    * cechy: bardzo nieufni, klanowcy dekadiańscy, anty-magowie, anty-verlenowie
* Rinosates Lojaliści Verleńscy (około 900 osób):
    * lokalni ludzie nadal wierzący w Verlenów i w ich pomoc
    * oneliner: "pomagali nam a my im; to nowa sytuacja"
    * eksport: (irrelevant)
    * import: (irrelevant)
    * zasoby: dostęp do większości miejsc i rzeczy w Miasteczku
    * agenda: ekspansja, przetrwanie, wspieranie Verlenów
    * cechy: pozytywnie nastawieni, przetrwamy piekło, populacja
* Rinosates Proemigranci (około 700 osób):
    * lokalni ludzie uważający, że Verlenowie zawiedli
    * oneliner: "mają jedno zadanie - a i tak zawiedli"
    * eksport: główne źródło ofiar dla Potworów
* Unaffiliated (około 4k osób)

### Potwory i blokady

* Jednostka Oddziału Spawnera
    * akcje: "wolno przeładowujący się strzał", "ranię szponem", "udaję kamień"
    * siły: "niewidoczny bez ruchu", "bardzo silny", "spojrzenie Koszmaru"
    * defensywy: "bardzo ciężki pancerz"
    * słabości: "wolny i toporny", "niezgrabny", "wolno strzela", "mało celny"
    * zachowania: "atakuję frontalnie", "atakuję z zaskoczenia"
* Spawner
    * akcje: "wystrzeliwuję Sieć Rozpaczy", "zamykam ofiarę w kokonie", "atakuję pnączomacką", "Oczy Terroru", "Chowam się w ścianie", "spawnuję Wojownika"
    * siły: "atak z tysiąca stron", "corrupting touch", "nieprawdopodobnie silny"
    * defensywy: "macki zasłaniają skałą", "nieciągły byt roślinny", "Interis chroni przed żywiołami"
    * słabości: "wolny", "nieciągły i ma jądro"
    * zachowania: "otacza przeciwnika", "corruption over destruction", "atakuje spod ziemi"
* Labirynt Korytarzy
    * akcje: "zagubienie", "nie da się manewrować", "za ciasno by przejść", "atak od tyłu", "szczelina"
    * siły: "ryzyko zawału", "odcięcie od sojuszników", "spowolnienie ruchów bo ciasno", "fałszywe sygnały sensorów"
    * słabości: "stałe punkty orientacyjne"

Inne - jak się manifestują Dary Leniperii?

* Dziwne Zwoje, wzmacniające użytkownika
* Dziwne Żywe Tatuaże
* Stymulanty (w strzykawkach)
* -> ważne: nie działają, jeśli ich nie weźmiesz aktywnie.

### Fiszki

* Jastrzębiusz Verlen: Zimny, pragmatyczny, darwinista, skupiający się na eksperymentach i militariach. Pattern: (Shockwave IDW)
* Karasiożer Verlen: Optymistyczny i zaczepny aspirujący wojownik, 16 lat, Exemplis+Alucis. Pattern: (Powerglide G1)
* Palaos Kregner: Stary zwiadowca, przekradł się wierząc, że Viorika pomoże. Noktianin, klarkartianin. Pattern: (Westley 'as you wish')
* Tiberius Skarn: członek Decadii Invictus; silny wojownik, acz już stary. Poświęci się za przyjaciół. Pattern: (stary Schwarzenegger)
* Nariad Fargelin: członek Decadii Invictus; łowca nagród, świetny sabotażysta, szlachetny i bystry. Pattern: (commander Ashley Lynx @ Solty Rei)
* Julia Strzałowik: ciekawska i odważna, wierzy we wspieranie Verlenów. Pattern: (Hermiona Granger)
* Zara Skarn: młoda, ambitna inżynierka, śmiała i kreatywna, pro-Verleńska. Pattern: (Kaylee Frye z "Firefly")
* Szymon Garwok: żarliwy i cichy, 'moja wiara w Verlenów moim sukcesem', Kieł Zemsty. Pattern: (mnich z 'Rogue Squardron')
* Leon Oszakowski: cichy specjalista od walki z potworami. Pattern: (Menasor G1 'power to destroy')
* Kasia Mribkit: infiltratorka, chroniąca Karasiożera, zawsze w okolicy. Pattern: (Ravage IDW)
* Eryk Tewatrik: arogancki naukowiec i wynalazca, który 'odkrył' Dary Leniperii. Pattern: (Brainstorm IDW)
* Mściząb Verlen: Chce być Verlenem bojowym, chce wygrywać, nie do końca dba o ludzi. Ważne jest zwycięstwo.

### Scena Zero - impl

.

### Sesja Właściwa - impl

W oczach wielu jastrzębi Verlendandu Elena pokazała słabość. Była nie dość silna, ale Skaziła swój wzór. I to buduje pewną presję na Ariannę. Z perspektywy Jastrzębi Blakenbauerowie skrzywdzili Elenę a ona - słaba - poszła dalej. Jastrzębie PATRZĄ. Bo czy Arianna jest GODNA?

Arianna i Viorika robią... Verlenowe rzeczy. W jakiejś mieścinie. Viorika najpewniej robi jakiś trening, Arianna podbudowuje morale oraz sznupie. Pokazuje jak zachowuje się godna Verlenlandrynka. Dla Arianny to zdecydowanie za duża presja, (mimo że rodzice tego od niej nie chcą), ale... no i po sprawie z Eleną. Ciężko.

Wieczór. Jakiś bar w mieścinie. Kilku żołnierzy, kilka normalnych osób, Arianna 100% elegancji, Viorika 100% Vioriki... ogólnie, jest standard.

Do Vioriki zwiadowca.

* Zwiadowca: "Tien Verlen, znaleźliśmy... rannego człowieka." (z lekkim zawahaniem)
* Viorika: "Dlaczego to warte raportu?" (zdziwiona)
* Zwiadowca: "Jest... jest noktianinem. I szuka tien Arianny Verlen." (lekkie zawahanie) "Podejrzewam, że chce ją zaatakować, ale... jest za słaby. I..."
* Viorika: "Nie byłby godnym wyzwaniem?"
* Zwiadowca: (z ulgą) "Tak. A nie chcę odbierać tien Ariannie Verlen możliwości walnięcia noktianinowi, więc nie wiem co robić"

Zwiadowca ma taką minę mopsa patrząc na Viorikę. Jak nie zepsuć potencjalnie fajnej niespodzianki i starcia i nie podpaść Ariannie. Viorika zdecydowała się pójść spotkać w stodole (bo tam jest noktianin) z tajemniczym rannym noktianinem. Zwiadowca z nim nie rozmawiał. Noktianin próbował się przekraść koło zwiadowcy, ale nie udało mu się. Ma gorączkę i jest ranny. W złym stanie. Zwiadowca wysoko ocenia skill noktianina, z jego punktu widzenia Światy Noctis wysłały całkiem spoko zabójcę przeciwko księżniczce Ariannie. Ale dzicz Verlenlandu... tak.

Viorika nawet Ariannie nie mówi co się dzieje XD. 

Noktianin jest w stodole, ma prowizoryczny zestaw medyczny. Na oko ma jakieś 40-50 lat, co znaczy, że jest SUPER STARY. Widać że został zatruty, spotkał się z potworem, roślinność... dostał full wypas Verlenlandu. Ale koleś jest opalony. I patrząc na niego - koleś co najmniej parę lat jest na planecie. Noktianin spojrzał na Viorikę i się uśmiechnął (zadowolony uśmiech).

* Palaos: Tien Arianna Verlen?
* Viorika: Nie, ale blisko. Czego chcesz od Arianny?
* Palaos: Sprawiedliwości. Podły Verlen niszczy miasto w którym mieszkam. Potrzebny mi polityk. Potrzebna mi jedna prawa Verlenka. Taka, która POMAGA ludziom.
* Viorika: Pasuje do całkiem dużej ilości Verlenów.
* Palaos: Nazywam się Palaos Kregner.
* Viorika: Viorika.
* Palaos: (ulga na twarzy) Czyli dotarłem do celu. Czyli miasto będzie bezpieczne.
* Viorika: Dobrze by było, byś powiedział coś więcej.
* Palaos: Pochodzę z Rinosates. Eks-Kolonia noktiańska.
* Viorika: SKORO JESTEŚ Z TEGO TERENU, CZEMU DAŁEŚ SIĘ TAK ZEŻREĆ?!
* Palaos: 40 kilometrów. Brak zasobów. Brak przygotowania. Ucieczka z Rinosates, w kierunku na Was.
* Viorika: Dlaczego uciekałeś? Czemu ucieczka była konieczna?
* Palaos: Potwór pustoszy nasze miasto, Jastrzębiusz Verlen zrzucił temat na Karasiożera Verlena a głównym operatorem jest Mściząb Verlen.
* Palaos: Mściząb nie dba o populację. Karasiożer (Palaos splunął) i Mściząb skupiają się na walce z potworem i na nie wzywaniu posiłków. To nie pierwszy raz z tego co wiem.
* Palaos: Potwór jest groźny, Mściząb i Karasiożer go nie pokonają.
* Viorika: Czemu tak uważasz?
* Palaos: Jestem członkiem Decadii Invictus Rinosates. Jesteśmy eks-żołnierzami. Broniliśmy Rinosates. Od zawsze. Tien Jastrzębiusz Verlen (niesmak) pragnie jak najsilniejszej populacji. Więc walczymy sami. Ale ten potwór, ten jest inny. Działa w tunelach. Tworzy inne potwory. Mściząb i Karasiożer... oni budują oddziały, robią plany, manewry, ale Potwór jest od nich lepszy. Sprytniejszy. Jeden potwór przechytrzył dwóch Verlenów. 
* Viorika: Nie pierwszy raz; zdarza się.
* Palaos: Jastrzębiusz odciął dostęp do sentisieci. 
* Viorika: Skąd wiesz?
* Palaos: Słyszałem rozmowy tienów. Nie umieją zlokalizować potwora. Dzięki temu tienowie są w stanie zrobić... lepszą walkę.
* Palaos: Część ludzi z Rinosates próbuje uciec. Ale potwór rozstawił agentów. Ludzie nie zawsze docierają tam, gdzie próbują uciec.
* Palaos: I dlatego mam nadzieję, że tien Arianna Verlen, księżniczka Verlenlandu, będzie w stanie wpłynąć na Jastrzębiusza, Mścizęba i Karasiożera. Bo ludzie giną. A Verlenland jest winny ludziom ochronę.
* Palaos: Z trzech ludzi którzy szukali pomocy... jestem pierwszy, prawda?
* Viorika: Jesteś pierwszy o którym wiem. Jak uciekłeś i dlaczego Tobie się udało a im nie?
* Palaos: Decadia Invictus zrobiła potężne zamieszki. W ogniu zamieszek wymknęło się trzech najbardziej doświadczonych zwiadowców. Mamy tunele.

Viorika przesłuchuje Palaosa pod kątem tego, co on wie o potworze. A Arianna chce się upewnić, że gość mówi całą prawdę. Że jest przekonany, że nie robi głupich błędów. Że ktoś nie wszczepił mu głupich wspomnień.

Tp +3:

* X: Wyraźnie Karasiożer jest zapatrzony na Mścizęba. SENPAI TEACH ME. I wyraźnie Mściząb jest kompetentny - rozwalił dużo tych potworów, uratował sporo ludzi, z populacji Rinosates buduje sobie też lojalistów. Mściząb WYGLĄDA jakby miał mieć rzeczy pod kontrolą. Ale na razie nie ma. Karasiożer nie był przekonany do tak... drastycznych rozwiązań. Ale Mściząb przekonuje go, że to jest optimum. Co pasuje Jastrzębiuszowi. Jastrzębiusz wspiera Mścizęba i jego strategię. Ale potwór jest INNY niż do tej pory. Potwór - zdaniem Palaosa - jest bytem wojskowym a nie zwykłym potworem.
* V: Palaos NIE CHCE kłamać, ale coś MOŻE być nie tak. Są tam pewne nieścisłości. Są tam... tam coś faktycznie może być nie tak. Palaos wierzy w to co mówi, ale niektóre z tych rzeczy nie trzymają się kupy perfekcyjnie.
* (utrudnienie klasy konfliktu, Tp->Tr); plus wypytuje o to samo ale inaczej, szuka nieścisłości, +1Vr. A jeśli działanie trzeciej strony, Viorika monitoruje potencjalny atak na Ariannę.
* V: Arianna doszła do błędów poznawczych, do tego... co jest nie tak. Problem polega na tym, że on WIERZY że się wydostał z miasteczka. Nawet umie powiedzieć jak. Ale to nie zadziała. Wydostałby się.
    * To były niespójności typu "jadłem spaghetti" -> "jadłem spaghetti pałeczkami". Nie ma sensu. Żaden mag, żadna osoba nie skłamałaby w ten sposób. Żadna osoba która rozumie co mówi nie powiedziałaby w ten sposób. On na poziomie poznawczym nie rejestruje tego błędu.
    * Myśl "znaleźć Ariannę" to jego myśl. Ale myśl "potrzebujemy pomocy" najpewniej jest jego, ale coś go NIE zatrzymało jak próbował zdobyć pomoc.

Viorika jest aż zaskoczona. To jest dziwne. Karasiożer. Nie był jastrzębiem, nie był izolacjonistą, nie był we frakcji "wiecznej wojny".

* Karasiożer jest 16-letnim wojownikiem. Aspirujący. Zaczepny. Chce osiągnięć.
* Karasiożer nie jest anty-ludziom. Karasiożer dba o swoich, nie chce wielkich czynów JEŚLI muszą ucierpieć JEGO ludzie.
* Mściząb jest dla niego WOW IDEAŁ. Charyzma, kompetencja, "tak musi być, młody".

Co Mściząb zrobił ostatnio?

* W ciągu ostatnich 2 miesięcy, Mściząb zaczął być CORAZ LEPSZY - coraz więcej potworów, zagrożeń, problemów... kompetencja mu rośnie.
* Rośnie i bezwzględność i kompetencja.
* Mściząb NIE MA zasobów Arianny i Vioriki, więc musi działać siłami lokalnymi... a Mściząb CHCE WYGRAĆ. Chce być lepszy niż Arianna - ta obsesja mu się wzmocniła.

Palaos potwierdził - mieszkańcy Rinosates poprosili Jastrzębiusza o pomoc. Jastrzębiusz dostarczył Mścizęba. Zapytany przez Viorikę, czy poprosili po raz drugi gdy Mściząb sobie nie radzi - nie. Nie poprosili ponownie. Rinosates zaczyna się dzielić. Część mieszkańców bardzo wspiera Mścizęba.

Kim jest Jastrzębiusz?

* Wierzy w Wielką Wieczną Wojnę. Wojna nigdy się nie kończy i potrzebni są żołnierze oraz sprzęt.
* Jego zdaniem słabi powinni zginąć albo opuścić Verlenland. Tylko przydatne jednostki.
* Ceni sobie Rinosates. Nie chce zniszczenia Rinosates. Ale jeśli uzna je za "zbyt miękkie" - należy je "przeczyścić".
* TO JEST MAG, który mógłby coś zrobić by przestestować młodych Verlenów.
    * To jest mag, który nie chciałby śmierci za dużej populacji Verlenlandu.
* "Wojna nie jest najgorszym co się zdarzyło Verlenlandowi. To pokój zniszczy Verlenland."
* Nigdy nie doprowadziłby do śmierci Verlena. Ani innego cennego zasobu.

Oficjalne relacje Mściząb - Arianna? Dzięki Marcinozaurowi - "chcieli być kochankami albo byli kochankami". Zapytany Palaos powiedział, że z jego wiedzy relacje Arianna - Mściząb są takie:

* Pragnęli być razem, ale im nie pozwolono
* Arianna wybrała obowiązek a Mściząb walkę z potworami
* Piszą do siebie listy
* (m.in dlatego Palaos wybrał Ariannę)

Zdaniem A+V, nie mogą tam wjechać jawnie z ciężkimi siłami. Mściząb będzie miał chore ruchy, bo nie chce źle wypaść. Arianna decyduje się połączyć z Jastrzębiuszem. Niech on ukryje Ariannę i Viorikę w sentisieci. Arianna ma zamiar apelować do poczucia humoru Jastrzębiusza oraz do rywalizacji.

* A: (idzie na dystyngowaną elegancką księżniczkę Verlenlandu ustawić kolesia, pokaże Jastrzębiuszowi jaka jest) Szanowny tienie Jastrzębiuszu! Miejscowi mieszkańcy zwrócili się ze mną z prośbą o pomoc i nie mogę ich tak zostawić. Oddelegowany do tego mag rodowy nie był w stanie zająć się tym problemem odpowiednio. Zwracam się z prośbą o zezwolenie na interwencję w tej sprawie. Chcemy to rozstrzygnąć jako Verlenowie między sobą. (technicznie nie musi ALE bo polityka)
* J: Młoda Arianno, twierdzisz, że Mściząb nie jest w stanie tego rozwiązać? Że zawodzi?
* A: Ewentualne rozwiązanie jakie dostarczy Mściząb po długim czasie będzie obejmowało pozbycie się wartościowych członków społeczeństwa
* J: Nie jest tak, że patrzysz, gdzie jest Mściząb i szukasz z nim zwady? (okrutny uśmiech)
* A: Mściząb nie jest na tyle wartościową osobą, bym poświęcała mu uwagę. Na pewno nie ganiałabym po całym Verlenlandzie.
* J: Skąd wiesz, że nie daje rady?
* A: Przybył do mnie posłaniec z Rinosates, proszący mnie o pomoc.
* J: Opuścił Rinosates? Przybył do Ciebie osobiście (lekka irytacja)?
* A: Jeśli Mściząb założył blokady, nie były skuteczne.

(+ jemu nie zależy czy wiesz), (- nie chce, by ktoś widział co on myśli zwłaszcza z innej frakcji), (+ Arianna to zauważyła i ma prawo wiedzieć za wyższą kompetencję)

Tr Z+3:

* X: Jastrzębiusz jest tą jedną osobą, która wierzy, że to jest kłótnia kochanków. Arianna chce pokazać Mścizębowi że jest jego godna. (czego Jastrzębiusz nie rozumie bo jest lepsza ale ok, ma poczucie niższości...)
* X: Jastrzębiusz zapewnia sobie, że ta sytuacja będzie nagłośniona. JEŚLI Arianna skroi Mścizęba, będą o tym wiedzieć. Jeśli Mściząb pokona Ariannę, będzie to głośne. Czyli - będą grali ostro.
* "Cała sprawa się rozwleka, więcej doświadczonych mieszkańców może zrobić własną akcję. Stracił dwóch doświadczonych zwiadowców. To były elitarne jednostki. Jedna przeszła do mnie" +1Vg
* V: Jastrzębiusz obieca, że Was dobrze ukryje. Mściząb się nie dowie że Ariannę tam ma.
* (czy to jest test? -> Tp) Vz:
    * Jastrzębiusz się zmartwił stratą doświadczonych noktian. On ich nie ma. Co więcej, on ich nie widzi w sentisieci. Są martwi. POWIEDZIAŁ Wam to.
    * To nie jest test. On nie planował niczego. On nie odcinał. On jest WŚCIEKŁY na Mścizęba. Tak bardzo spieprzyć.
    * ALE Arianna to naprawi. Lepsza kochanka utrzyma słabszego kochanka na smyczy.
    * "Mściząb traci wszystkie dobre relacje dzięki swojej słabości i działaniom Arianny, by musiał zostać z Arianną" -> i to Jastrzębiusz rozumie i docenia.
* Vr: Jastrzębiusz powiedział, że nie zablokował sentisieci Mścizębowi ani Karasiożerowi.
    * Albo Palaos ma fałszywe dane, albo Mściząb oszukał Karasiożera odnośnie sentisieci.

W Barbakanie Jastrzębiusza są Arianna i Viorika, wiedzą to co wiedzieć mają. Jastrzębiusz jest przekonany, że Arianna i Viorika z Mścizębem i Karasiożerem naprawią sprawę. Jastrzębiusz ma wystarczająco dobrą opinię o Ariannie i lepszą o Viorice - wierzy że będzie dobrze.

Arianna robi płomienną mowę: "ZNAM TĄ OSOBĘ NIE JESTEŚMY BLISKO I NIE odciąłby od sieci, tam może być coś strasznego i groźnego i ja uratuję magów rodowych!" (+1Vr)

* V: Jastrzębiusz sprawdził w sentisieci kilka rzeczy dla Arianny, bo bawi go gra kochanków. Jastrzębiusz nie jest najbardziej kompetentny w sentisieci.
    * czy ktoś zablokował Mścizęba i Karasiożera? -> nie, ale Mściząb zablokował Karasiożera. Jastrzębiusz: "no tak, robi młodemu test (dobrze robi, ale stracił ludzi więc źle)"
        * Zamysł dobry wykonanie jak zawsze. Jastrzębiusz ma pomniejszy aspekt pogardy do Mścizęba.
    * czy Mściząb ma triggery, queries itp na sentisieci? -> bardzo dużo. Mściząb wykorzystuje sentisieć w sposób godny szacunku. Viorika-class a nie Mściząb-class.
        * dużo "czy są tu inni Verlenowie", badanie ruchów populacji, robienie ruchów sentisiecią by przekierowywać potwory z pkt X do Y...
        * bardzo dużo triggerów na temat sentisieci, pytań do sentisieci itp. Do tego wszystkiego ma triggery na Ariannę, Viorikę i wielu innych Verlenów.
    * czy są luki i problemy widoczne przez sentisieć? -> MOŻE. Tak duży jest "hałas" w sentisieci, że coś można przeoczyć. To może być błąd Mścizęba - tak bardzo robi ruchy, że traci pasywną detekcję. Ale jest coś co WSKAZUJE na obecność Interis - osoby, które zniknęły z sentisieci są niewidoczne.
    * Palaos jest widoczny w sentisieci. Nie został ukryty przez Interis. 

Z perspektywy Verlenów Elena była "tą słabą" - zniszczyli ją Blakenbauerowie. Ale tam przynajmniej byli Blakenbauerowie. A tu? Mściząb nie ma przeciwko sobie Blakenbauerów. Więc co, wpakowałby się w coś sam? "Aż tak głupi nie jest" zdaniem Verlenów. Ale Arianna - która kojarzy problem Eleny - jest wyczulona. Ona wie. Ona wie, że może być gorzej niż się wydaje.

STOP. Analiza sytuacji:

* Mściząb zachowuje się jak nie on
    * postawił triggery, nie w jego stylu
    * jest na powerupie jakiegoś typu
    * udaje, że nie ma dostępu do sentisieci
    * odciął Karasiożera z sentisieci
* Na terenie jest potwór, który działa jak strateg
* Zwiadowca się dostał do Arianny i Vioriki i coś / ktoś go wypuścił
* Jastrzębiusz jest po stronie Arianny i Vioriki

Co najbardziej w tym martwi:

* dziwne zachowanie Mścizęba
* znikające postacie
* obecność Interis
* potencjalny trzeci gracz
    * jaka agenda?
* czy to nie jest pułapka
* POTWÓR STRATEG

Co musimy rozwiązać najpierw:

1. czy to nie jest pułapka?
2. czy Mściząb jest sobą?
    * strategia: przeładować triggerami, by go przeciążyć i by je wyłączył

PLAN:

* Arianna kontaktuje się z Decadią Invictus
* Viorika przeładowuje Mścizęba sygnałami

Arianna do Jastrzębiusza: "zaprezentujesz epickie czołgi osobom zainteresowanym, z naszej frakcji i zrobisz dystrakcję dla Mścizęba, który nauczy się, że nie należy przeciążać się sentisiecią."

* (+przewaga) Vg: Jastrzębiusz się zgodzi, dostarczy kilka czołgów na prezentację i będzie impreza na terenie "na tyle bliskim, by to Mścizęba wkurzało ale nie na terenie Rinosates". Tak zwana "impreza na pograniczu" gdzie "straszymy post-noktian czołgami" (oficjalnie, bo nieoficjalnie nikt tego nie chce). Więc - będzie impreza, Arianna sponsoruje, Jastrzębiusz się zgadza i desygnuje miejsce, Mściząb będzie smutny

(Mściząb ma już Niedowagę z sentisiecią)

Tymczasem Viorika robi serię chorych sygnałów i triggerów w sentisieci. Cel - niech Mściząb zacznie mieć luki, nie zauważać. Niech Arianna i Viorika mogą się tam sensownie prześlizgiwać, robić wygodne query i dowiadywać się o rzeczywistości nie wpakowując się na Mścizęba. Viorika robi to subtelnie, długoterminowo. Jej celem jest trzymanie się kupy, irytowanie Mścizęba i ogólnie - rozwalenie sytuacji by nie ufał sentisieci.

Ex. Mimo wszystko triggery itp. ALE: Mściząb jest zdestabilizowany bo impreza będzie (+). Viorika jest ekspertką od sentisieci (+), Viorika robi to subtelnie i długoterminowo (+) -> TrZ+3.

* V: macie swobodne ruchy na tym terenie, Mściząb w ogóle nie myśli o Was, bardziej martwi się imprezą. Zirytowany i zagubiony, nie ma problemu
* Vr: Mściząb będzie ignorował większość triggerów które da się sensownie ukryć i zamaskować. Jego zdaniem większość to fałszywe pozytywy. No i ta cholerna impreza - RÓŻNI SZUKAJĄ GŁUPICH RZECZY.

Te rzeczy + Decadia Invictus to około 3 dni.

Arianna po rozstartowaniu imprezy łączy się z Decadią Invictus.

* Tiberius: tien Verlen, czyli Palaos dotarł do celu.
* Arianna: tak, udało mu się. Moją obecność w tajemnicy. Nie wiemy z czym mamy do czynienia. Pseudonimy operacyjne.
* Tiberius: To ja proponuję, że będę... "kukurydzą". Tobie proponuję "jaskółkę".
* Arianna: pasuje.
* Tiberius: miasto jest ze sobą skłócone. "Kiełek" (Mściząb) zbiera ludzi i chroni miasto skutecznie, ale ludzie którzy opuszczają ten teren by uciec gdzieś indziej znikają.
    * (przez imprezę, ludzie uciekają w stronę imprezy z Verlenami, co sprawia, że nie będą już znikać)
* Tiberius: nie jestem przekonany, czy ludzie uciekali i dlatego znikają, czy niektórzy nie znikali z domów. 2-3 osoby na dzień. To dużo. Wiem, że rodziny uciekały. Nie wiem, czy wszyscy co zniknęli próbowali uciec. Jednocześnie, Rinosates przygotowuje się do wojny z potworem, pod dowodzeniem "Małego Kiełka".
    * (Kult Kła Zemsty Rinosates, tak Mściząb nazwał swoją frakcję - około 300 osób)
    * Oni między innymi "modlą się" do niego, wzmacniając go

Viorika oszacowuje - jak mocny jest potencjalny wpływ kultu na Mścizęba? Jakieś 2 tygodnie i kult ciągle rośnie w siłę. Wzmocnienie Mścizęba samo w sobie jest większe niż wynika z tego kultu. To może być czynnik, ale nie jest wyłączny. Co ciekawe - udało się udowodnić (nota bene, Tianie), że kultyści znikają statystycznie rzadziej. Mściząb jest za dobry jak na siebie. Zdecydowanie za dobry. Brzmi jak _Exemplis_.

Arianna rozmawiając z nim oszacowuje, czy jego głowa jest OK. Czy coś na niego wpływało - nie. Nic na to nie wskazuje. Tiberius, współdowodzący Dekadią Invictus, on jest "czysty". On ogólnie unika magii.

Arianna go przesłuchuje. Chce się dowiedzieć, co wie i jak to działa.

Tp+3:

* V: 
    * Tiberius jest przekonany, że Mściząb wystawia Dekadię Invictus w najgorszym miejscu (blisko potworów, na froncie) - i niestety chyba ma rację. Ta frakcja Mścizębowi nie leży. Nie robi tego by eksterminować. Ale jeśli potrzebny mu cywil, to od nich. Tak samo kultyści źle traktują Decadię Invictus.
    * Zdaniem Tiberiusa, wielu kultystów ogólnie jest "lepsza" niż kiedyś. Kompetencja im się w pewnym stopniu udzieliła. Nie aż tak mocno, ale. I im rośnie wiara i fanatyzm do Mścizęba. Ci ludzie nie patrzą na poprzednie więcy. Chwała Mścizębowi!.
    * Ogólnie, potwory zabijają i znikają ludzi. Nie tak bardzo, by kłopotać Jastrzębiusza. Ale dość, by ludzie się stresowali.
    * Kultyści to mieszanka. Większość astorian i mieszanych. Większość mieszkała na powierzchni.
    * Potwierdzenie - znikają przede wszystkim ludzie, których "później zauważysz że zniknęli"

Z taktycznego punktu widzenia - jeśli potwór potrzebuje dostępu do ludzi, to Mściząb przegrywa wojnę i tego nie widzi. Mimo, że nie robi głupich ruchów i nie wchodzi w podziemia. Co gorsza, Decadia Invictus oraz INNE siły mogą mieć zarażonych agentów - agenci mogą służyć Potworowi. Tiberius dostaje zadanie - znaleźć zarażonych.

Tr Z +3:

* Vr: Tiberius znalazł (po radach Arianny i na co patrzeć) serię zarażonych agentów. Z 80 Decadia Invictus zarażonych jest 11. Są spenetrowani.
    * na razie oni jeszcze tego nie wiedzą
    * mamy zakres KTO i GDZIE w D.I. jest pod kontrolą Arianny.

## Streszczenie

Arianna i Viorika spotykają rannego noktianina który ich szukał, Palaosa Kregnera. Palaos prosi o pomoc przeciwko Mścizębowi i Karasiożerowi Verlenom, oskarżając ich o zaniedbanie w walce z potężnym potworem niszczącym Rinosates. W trakcie śledztwa Arianna i Viorika zauważają nieścisłości w opowieści Palaosa; działa tu trzeci gracz, prawdopodobnie powiązany z Interis. Potem kontaktują się z Jastrzębiuszem Verlenem, który zgadza się pomóc uważając to za kłótnię kochanków. A+V organizują dywersję, przeciążając sentisieć Mścizęba poprzez serię sygnałów i triggerów, aby odwrócić jego uwagę i przeprowadzić własne dochodzenie. Odkrywają, że Decadia Invictus została zinfiltrowana przez zarażonych agentów, a Mściząb zachowuje się nietypowo, co sugeruje, że może być pod wpływem zewnętrznej siły.

## Progresja

* .

## Zasługi

* Arianna Verlen: skutecznie przekonała Jastrzębiusza do wsparcia i ukrycia jej obecności, organizując dywersję w postaci prezentacji czołgów. Nawiązała współpracę z Decadią Invictus, zidentyfikowała potencjalne zagrożenia i zleciła Tiberiusowi Skarnowi wykrycie zainfekowanych agentów.
* Viorika Verlen: wykryła niespójności w opowieści zwiadowcy (Palaosa). Przeładowała sentisieć, destabilizując działania Mściząba i umożliwiając swobodne poruszanie się sobie i Ariannie
* Mściząb Verlen: ostatnio bardzo zwiększył swoją skuteczność w walce z potworami, zyskując poparcie lokalnej ludności. Utworzył Kult Kła Zemsty w Rinosates; ale zachowuje się dziwnie, odciął Karasiożera od sentisieci i nadmierne wykorzystuje triggery sentisieci
* Karasiożer Verlen: młody aspirujący tien, pod silnym wpływem Mściząba. Został odcięty od sentisieci przez Mściząba, co ograniczyło jego zdolność do komunikacji i koordynacji. Lojalny Mścizębowi, nie kwestionuje
* Jastrzębiusz Verlen: Tien tego terenu; początkowo wspierał strategię Mściząba, ale po informacji od Arianny o zaginionych zwiadowcach zaczął kwestionować jego działania. Ukrywa Ariannę i Viorikę, organizując prezentację czołgów jako dywersję.
* Palaos Kregner: ranny i wyczerpany, przekradł się przez niebezpieczny teren, aby dotrzeć do Arianny i Vioriki. Dostarczył im informacji o sytuacji w Rinosates, choć jego opowieść zawierała niespójności
* Tiberius Skarn: współdowodzący Decadią Invictus, wykazał się lojalnością i profesjonalizmem. Na prośbę Arianny podjął się zadania zidentyfikowania zainfekowanych agentów w swojej organizacji

## Frakcje

* .

## Fakty Lokalizacji

* Rinosates: miasto Verlenlandu złożone pierwotnie trochę na bazie arkologii noktiańskiej, z poziomem podziemnym (mieszkalnym) i nadziemnym (produkującym żywność). To się zmieniło, bo są tam nie tylko noktianie.
* Rinosates: ma bardzo dużą redundancję i na dole nie mieszka już tyle ludzi co kiedyś z uwagi na ochronę Verlenów; bardzo samowystarczalne miejsce, ale lojalne

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. Rinosates
                                1. Poziom podziemny
                                    1. Kompleks Mieszkalny Alfa: są tam trzy kompleksy, mniej więcej po 2k osób każdy
                                        1. Mieszkania
                                        1. Strefa relaksu
                                        1. Magazyny i schowki
                                        1. Strefa Zarządcza Życiem: mikro-reaktor, centrala, ciepło, wentylacja, recykling wody...
                                    1. Sektor ekonomiczny
                                    1. Sektor medyczny
                                    1. Inżynieria
                                    1. Reaktor główny
                                1. Poziom naziemny
                                    1. System obronny
                                    1. Windy i wentylacje
                                    1. Odnawialna energia
                                    1. Domy mieszkalne: nie wszyscy są noktianami i nie wszyscy chcą tak żyć
                                    1. Strefa Handlowa
                                    1. Strefa Produkcyjna


## Czas

* Opóźnienie: 17
* Dni: 5

## Inne
