## Metadane

* title: "Pocałunek Aspirii"
* threads: legenda-arianny
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [201118 - Anastazja - bohaterką?](201118-anastazja-bohaterka)

### Chronologiczna

* [201118 - Anastazja - bohaterką?](201118-anastazja-bohaterka)

## Punkt zerowy

### Dark Past

.

### Opis sytuacji

Statek Koloidowy poluje na Ariannę. Nie wie, że Arianna dowodzi Infernią - i nie wie, że Infernia ma zaawansowane sensory.
Arianna próbuje zniszczyć statek koloidowy, zanim on zniszczy ją.

### Postacie

.

## Punkt zero

.

## Misja właściwa

Anastazja zamknęła się w swojej kajucie na Inferni i desperuje. Nikt na serio nie chce jej przeszkadzać. Najpewniej zamknęła się, bo kocha się w Eustachym i desperuje, bo nie może i nie powinna i to jest straszne.

Arianna i Eustachy doprowadzili statek do jako takiego porządku po sprawie z koloidowcem. Tymczasem - Klaudia dostaje priorytetową informację z K1. Próbuje to sprawdzić i zbadać (XX), ale Persefona zareagowała na kody i włączyła połączenie. To Juliusz Sowiński, "paladyn Sowińskich". Chce natychmiast rozmawiać z Eustachym. Klaudia powołała się na paragrafy i przekierowała sygnał do Arianny.

Arianna dzielnie odpiera ataki wściekłego Juliusza Sowińskiego. On chce Anastazję z powrotem, TERAZ. Arianna się nią "opiekuje" i jak to się skończyło? Plebs Eustachy, zrujnowana reputacja Anastazji i ogólnie tragedia. Arianna tłumaczy, że Martyn założył kwarantannę na Inferni. Juliusz pyta, czy Anastazji coś grozi. Arianna po namyśle, że nie - bo naprawdę, naprawdę nie chce mieć Sowińskiego na ogonie.

Juliusz chce rozmawiać z Anastazją. Arianna inteligentnie, że musi się Anastazja przygotować, ona ją zapyta. Juliusz wiedząc, że Anastazja może mieć fochy poczekał. A Arianna zsyntetyzowała ulubione ciastka Anastazji i poszła ją odwiedzić.

Anastazja wpuściła Ariannę, przekupiona ciastkami. Jest zapłakana - zbłaźniła się, nie ma powrotu, zostaje tylko dołączenie do sił wojskowych i bohaterska śmierć w chwale jak Elena. Arianna wyjaśniła Anastazji, że nie jest tak źle i powiedziała, że Juliusz chce z nią rozmawiać. Anastazja MU TAK POWIE. ONA MU POWIE ŻE NIE WRACA Z NIM. BO JEST PADALCEM! O!

Anastazja doprowadziła się do porządku i zażądała połączenia z Juliuszem. (VzOX). Poszło... nie tak jak Anastazja myślała. I nie tak jak Arianna myślała.

Juliusz zaczął od opieprzania Anastazji - zrobiła hańbę rodowi. Ale on przyleci ją z tego wyciągnąć, jakoś to rozwiążą. Dla rodu i dla dziadka. Anastazja już się łamała - Juliusz jest bardzo charyzmatyczny - ale Klaudia rzuciła na ekran Anastazji wizerunek Eleny pod life supportem, gdy Juliusz mówił o "rolą pomniejszych arystokratów jest chronić nas" i gdy powiedział, że Elena nie może być 'role model' dla Anastazji.

Anastazja zaczęła od tego, że Infernia nie jest taka jak Juliusz myśli, że tu są ciągłe zagrożenia, że ona się ciągle uczy i że jest Skażenie i anomalie. Ona zostanie żołnierzem jak Elena. Chce do wojska, bo w rodzie i tak niczego nie osiągnie. Do tego pojawił się też Upiór Kwiatu Wiśni - doprowadzając Juliusza do załamania. Anastazja zaczęła mówić wszystko źle, ale z serca - Sowińscy jej nie potrzebują, tu może zrobić wiele dobrego i odkupić swój honor... ogólnie, straszne. Arianna jest z niej dumna. Elena też by była.

Juliusz przepiął się na komunikację z Arianną. Przyśle jej wsparcie - Okręt Aurum "Zguba Tytanów". Jeśli ten statek koloidowy sprawia, że Infernia ma problem z anomalią, to "Zguba" pomoże Inferni w wyciągnięciu z tego Anastazji. Arianna się zgodziła.

Statek dotrze do pozycji Arianny za jakieś 3 dni. Kolejne 2 dni Klaudia pracowicie korzystała z baz Inferni i z Persefony - czym jest ten cholerny koloidowy statek który ich napadł? O co tu chodzi?

* Taki koloidowy statek to klasa "Terrozaur". Natywny dla szczepu Fareil z planety Neikatis.
* Fareil nie mają własnej floty, wierzą w Orbiter. Ale mają statki handlowe, które lubią być atakowane przez piratów. Stąd koloidowe Terrozaury.
* Fareil mają bardzo dobre relacje z Orbiterem. Nie ma powodu, czemu Terrozaur miałby atakować statki Orbitera, acz Klaudia wykryła, że w przeszłości niejeden statek Orbitera miał smutne spotkanie z Terrozaurem - ale Fareil mówili, że to nie ich.
* Klaudia drąży głębiej - ma to. Ten statek zwie się "Pocałunek Aspirii". I to jest statek, który Fareil zgubili. A dokładniej, pochodzi z martwej arkologii na Neikatis, Aspirii.

Martwa arokologia? O co tutaj chodzi?

Okazało się, że jakieś 10 lat temu Aspiria była miejscem wojny pomiędzy Drakolitami i Fareil. Drakolici puścili broń biologiczną, swój wirus. Fareil weszli w power suity i zaczęli eksterminować wszystkich. Orbiter wszedł pomiędzy jako siły pokojowe; gdy się wycofał, Drakolici zeksterminowali Fareil na Aspirii. Aspiria jako arkologia została zniszczona, w tej chwili jest martwa i nie działa tam stabilny ekosystem. Był jeden Terrozaur w Aspirii; ten Terrozaur zniknął. Najpewniej nasz terrorystyczny statek koloidowy to właśnie tamten Terrozaur.

Klaudia, wiedząc już, że ten statek najpewniej wie gdzie była Infernia - zmieniła szyfry i linie komunikacyjne. Po co mu za bardzo ułatwiać?

Tymczasem Eustachy planuje jak zastawić pułapkę na "Pocałunek Aspirii", bo w tej chwili to "my albo on". I wraz z Arianną i Klaudią opracowali plan.

1. Zanęcić - jesteśmy przynętą. Niech do nas przyleci.
2. Dać się trafić, ale udajemy, że jesteśmy mniejszą jednostką (to nas nie zniszczy strumieniowym działem).
3. Szybko zregenerować Infernię i zestrzelić drania.

Faktyczny plan został przekształcony w taki sposób: Infernia będzie miała inny transponder jako Q-Ship; będzie udawać OO "Blask Aurum", statek, który przewozi materiały i części na Neikatis. Ale po drodze do Neikatis nasz "Blask Aurum" został zaatakowany przez piratów i/lub anomalie; eskorta została z tyłu a sam Blask został zmuszony do ratunkowego skoku w kierunku na Neikatis i wzywa SOS. To powinno skłonić Pocałunek Aspirii do zaatakowania - nieczęsto ma taką okazję. Co więcej, jest też możliwość, by znajdująca się dzień drogi stąd Zguba Tytanów zaczęła strzelać - udając, że jest walka między piratami i eskortą.

Do tego Infernia przelatuje przez śmieci kosmiczne - zmniejsza szansę na wykrycie. A Eustachy przebudowuje część Inferni; niech fizycznie wygląda bardziej jak Blask Aurum. Do tego wzmacnia osłony koło reaktora i silników, robiąc efekt "reakcji łańcuchowej". Niech widać po Inferni, że dostała bardzo cieżki strzał.

Oki, wszystko zrobili. Czas na przeprowadzenie akcji.

* Przynęta: 9v5 1entropii
* Detekcja: 7v5 1entropii
* Strzał: 10v5 1entropii
* Regeneracja i kontrstrzał: 10v5 1entropii

**Przynęta**: Parziarz nie jest głupcem (XXV). Leci przygotowany, jego wspierająca anomalna stacja jest bliżej niż się wydaje. Eustachy by ściągnąć w pułapkę Parziarza musiał dodatkowo uszkodzić Infernię. Nic szczególnie ważnego, ale naprawiający na K1 inżynierowie nie docenią.

**Detekcja**: (XVVO). Klaudia wykryła zbliżający się Pocałunek Aspirii - i coś jeszcze. Jedną z bój detekcyjnych zniszczyło coś anomalicznego. Klaudia wie, że zbliża się coś _jeszcze_. Na domiar złego, dwa małe patrolowce Fareil zbliżają się ratować przyjazną jednostkę z Orbitera...

**Strzał**: (VVX). Systemy Inferni lekko uszkodzone, ale statek jest aktywny. (X): poważniejsze uszkodzenia w układzie antygrawitacyjnym, który lekko migocze. Nic bardzo poważnego, ale... jednak Infernia nie wyszła z tego suchym silnikiem.

Do Inferni odezwał się Parziarz. Klaudia natychmiast przełączyła obraz Arianny jako kapitan Kinga Czerw, dowódca Blasku Aurum. Parziarz jest ubrany w mundur Orbitera, jest schludny i zadbany. Arianna próbuje dowiedzieć się czegoś od Parziarza, ale on odpowiada - poczekajcie aż przejmie was większa jednostka, wtedy porozmawiamy. Nie stanie wam się krzywda.

Klaudia powiązała szybko informacje o większej jednostce z anomalicznym statkiem zbliżającym się w kierunku Inferni.

**Kontrstrzał**: Eustachy włączył systemy Inferni i wystrzelił wszystkim co Infernia ma w Pocałunek Aspirii. Po drodze puszczając jedną torpedę dużej mocy. (VVV). Eustachy zniszczył reaktor Pocałunku; statek jest martwy. Śmierć około 30% załogi na pokładzie Pocałunku, a na pewno agent pełniący rolę TAI spłonął od feedbacku.

Parziarz siedzi tam, jak nie do końca świadomy tego co się właśnie stało. Mówi "zabiliście sporo dobrych ludzi". Arianna każe mu się poddać, Parziarz mówi, że chyba nie ma wyboru. Ale anomalny statek się zbliża. Arianna każe mu odwołać anomalię, lub wystrzeli i ich dobije. Parziarz mówi, że nie kontroluje Katry. Nic nie kontroluje Katry. Ale niech Arianna natychmiast odwoła patrolowce Fareil, bo zginą.

Klaudia natychmiast wysyła do nich sygnał. A monitory Inferni zaczynają szumieć. Pojawiają się głosy i szepty i krzyki. Anomalia Kosmiczna Wyjec zaczyna się manifestować - kiedyś noktiańska stacja medyczno-naprawcza wspierająca Aspirię, po masakrze uległa anomalizacji. Dowódca: Katra Igneus.

Arianna nie chce zostawić Pocałunku Aspirii. Jeśli to zrobi, całe ich działanie będzie na marne. Nie chce też ich niszczyć. Po chwili namysłu, użyła swojej mocy i wysłała Sygnał.

Arianna wezwała Nocną Kryptę...

## Streszczenie

Juliusz Sowiński zażądał oddania Anastazji, ale ona nie chce wracać - jej reputacja i stan są w ruinie. Anastazja chce być jak Elena, ku zgryzocie Juliusza. Tymczasem Arianna zastawiła pułapkę na koloidowy statek - "Pocałunek Aspirii" - i go zestrzeliła. Po czym wezwała Nocną Kryptę by zdjąć z nich klątwę miłości i naprawić co się da z "Pocałunku" i AK Wyjec.

## Progresja

* OO Infernia: lekko uszkodzona; ma migoczące pole grawitacyjne i lekko słabsze stabilizatory. Ogólnie, operacyjna.

### Frakcji

* .

## Zasługi

* Arianna Verlen: nawigowała polityczne wody między rodem Verlen a Juliuszem Sowińskim odnośnie Anastazji. Wezwała Kryptę by naprawić AK "Wyjec".
* Eustachy Korkoran: przebudował (konstrukcja) Infernię by wyglądała jak Blask Aurum. Potem - zestrzelił Pocałunek Aspirii.
* Klaudia Stryk: oficer komunikacyjny; by przekonać Anastazję do postawienia się Juliuszowi pokazała na monitorze Elenę pod lifesupportem. Manipuluje Anastazją.
* Anastazja Sowińska: załamana tym, że po ostatnim zrobiła pośmiewisko swojemu rodowi. Zakochana w Eustachym. Chce wstąpić do wojska i postawiła się Juliuszowi.
* Juliusz Sowiński: chce odzyskać Anastazję, jest przerażony tym, że ona chce wstąpić do armii. Wysłał do pomocy Anastazji OA "Zguba Tytanów".
* Donald Parziarz: kiedyś jeden z oficerów sił pokojowych Orbitera w arkologii Aspirii, teraz kapitan piratów. Stracił pojazd i czść załogi do Inferni.
* Katra Igneus: noktiańska przełożona stacji medyczno-naprawczej w Aspirii; na przestrzeni wojen etnicznych stała się rdzeniem AK Wyjec.
* OO Infernia: QShip; udawała OO Blask Aurum. Lekko uszkodzona, ale pułapkując zniszczyła Pocałunek Aspirii.
* AK Wyjec: kiedyś noktiańska stacja medyczno naprawcza, anomalia sprzężona z Pocałunkiem Aspirii. Dostarcza Pocałunkowi koloidu, "żywi" się jeńcami.
* AK Nocna Krypta: ściągnięta przez Ariannę Verlen, by zdjęła z Inferni klątwę miłości i naprawiła Pocałunek Aspirii i AK Wyjec.
* OA Zguba Tytanów: potężny krążownik Aurum pod dowództwem Sowińskich, wysłany na pomoc Inferni by uwolnić Anastazję od klątwy miłości.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis: słabo sterrarformowana planeta typu 'Mars', z arkologiami. Można powiedzieć, w stanie wiecznej walki etnicznej. Nie mają floty bojowej.
                1. Dystrykt Quintal
                    1. Arkologia Aspiria: doszło tam kiedyś do starć między Fareil i Drakolitami; siły Orbitera jako rozjemcy. Atm: martwa arkologia.

## Czas

* Opóźnienie: 1
* Dni: 3

## Inne

Pierwszy raz pokazaliśmy:

* Terrozaur: jednostki koloidowe typu Pocałunek Aspirii
* Planeta Neikatis, z etnomempleksem Fareil (synthesis oathbound pro-virt, pro-numerologia) i Drakolici (xenoplague amazon, wpływ kultu ośmiornicy)
* Przeszłość: Orbiter nie zawsze działał jak powinien; czasem wbijał się tam gdzie nie trzeba i powodowało to problemy

Przetestowaliśmy:

* Wieloetapowy mechanizm sprawdzania wyniku (buduj pulę, draw 3).