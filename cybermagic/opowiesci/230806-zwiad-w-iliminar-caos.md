## Metadane

* title: "Zwiad w Iliminar Caos"
* threads: pronoktianska-mafia-kajrata, furia-mataris-agentka-mafii
* motives: teren-morderczy, echa-inwazji-noctis, symbol-lepszego-jutra, zrzucenie-winy-na-innych, integracja-rozbitej-frakcji
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [230730 - Skażone schronienie w fortecy](230730-skazone-schronienie-w-fortecy)

### Chronologiczna

* [230730 - Skażone schronienie w fortecy](230730-skazone-schronienie-w-fortecy)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ?
* Struktura: 
    * ?
* CEL: 
    * MG
    * Drużynowy
* THEME
    * Kajrat mówi 'never again' - będzie walczył o noktian


### Co się stało i co wiemy

* Tymczasowa baza w zniszczonym miasteczku Kalterweiser
    * a dokładniej, w tunelach pod miasteczkiem
    * części pochodzą ze skanibalizowanych jednostek
* Konieczność udowodnienia Symlotosowi, że warto oddać mu Furie (operacja Kajrata)
    * -> bezpośrednie uderzenie w Czarne Czaszki i oddanie części z nich Symlotosowi
    * -> udanie się do Czarnych Czaszek 
* Konieczność zdobycia części medycznych (operacja Amandy)
    * jednostka noktiańska Iliminar Caos; niestety, mocno Skażona
    * potem -> Las Pusty do dekontaminacji (jak DOKŁADNIE się odkaża?)
    * potem powrót

### Co się stanie (what will happen)

* F1: Kajrat wyjaśnia plan 
    * Kajrat wyjaśnia plan (on -> Czaszki, ona -> Iliminar Caos)
    * 'Bunt' noktian, Kajrat wyjaśnia, że to jedyna droga
    * Amanda się zapoznaje ze stanem bazy
    * Kajrat prezentuje Amandzie (hover APC 'Quispis') i pięciu żołnierzy (6 osób)
        * pierwotnie jednostka zwiadowczej insercji
    * savarański medyk zajmuje się Xaverą
* F2: droga do Iliminar Caos
    * teren + omijamy niektóre potwory
    * dokuczamy Amandzie bo słodka dziewczynka
* F3: infiltracja Iliminar Caos
    * ?
* F4: powrót
    * poluje na nas latający stwór

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Xavera Sirtas
    * OCEAN: (C+A-): żywy ogień, jej dusza jest pełna pasji, a jej słowa ostrzem. "Kto siedzi cicho, tego spotyka wszystko co najgorsze"
    * VALS: (Security, Tradition): wszystko odnosi do tego co już widziała i co było, działa dość tradycyjnie. "Wszystko już było i się stało"
    * Core Wound - Lie: "Przeze to, że dużo mówiłam i szperałam, moi rodzice się rozwiedli" - "Będę walczyć i ognistą walką udowodnię, że warto być razem!"
    * Styl: Zawsze gotowa do walki. Agresywna i asertywna. Dobrze wybiera walki. Jej upór często wpędza ją w kłopoty.
* Isaura Velaska
    * OCEAN: (E+A+): uspokajający głos w zespole, zawsze znajduje chwile na uśmiech i dowcip. "Nieważne co się dzieje, trzymaj fason i się uśmiechaj"
    * VALS: (Harmony, Benevolence): "Zespół to jak rodzina. Musimy być dla siebie wsparciem."
    * Core Wound - Lie: "Rodzina mnie porzuciła, ale Furie adoptowały" - "Jeśli zrobię dla Was rodzinę, wszyscy zawsze będziemy razem bez kłótni i sporów"
    * Styl: Ostoja spokoju wśród chaosu, rozładowuje napięcia i tworzy poczucie wspólnoty i jedności; dystyngowana, zachowuje się jak arystokratka
* Caelia Calaris: savarańska lekarka Kajrata
    * OCEAN: (E-N+A+): wyjątkowo nieruchoma i ostrożna społecznie NAWET jak na savarankę, ale empatyczna i przyjacielska. Przytulna. "Oszczędzajmy wszystko."
    * VALS: (Harmony, Achievement): robi co może tym co ma i dba o morale jak umie. "Razem sobie poradzimy."
    * Core Wound - Lie: "Calaris, macierzysta jednostka umarła przez brak zasobów" - "Jeśli będziemy oszczędzać na wszystkim, przetrwamy! Nie kosztem załogi, nigdy!"
    * Styl: Całkowicie nieruchoma, unika bycia pod słońcem, mało mówi i bardzo ciężko pracuje. Wrażliwa, widać ślady łez. Słaba fizycznie z nieprzystosowania planetarnego.
* Dragan Halatis: klarkartiański główny inżynier Kajrata (57 lat) 
    * OCEAN: (O+N-A+): ciekawy świata i wszystkiego, radosny z uwagi na problemy, empatyczny wobec załogi. Nie poddaje się. "Inżynieria Noctis pokonała planetoidy, pokona i to."
    * VALS: (Face, SelfDirection): lubi eksperymentować i testować różne rzeczy; bardzo ważny jest dlań szacunek. "Wszystko jest możliwe, trzeba tylko spróbować kilkanaście razy".
    * Core Wound - Lie: "Gdyby nie ja, dalej bylibyśmy w światach Noctis" - "Nieważne ile naprawię, nigdy nie naprawię zniszczenia naszej przyszłości"
    * Styl: Praktyczny, z chłopięcym entuzjazmem, 'przygarnia znajdki'. Ciekawy nowości i co da się zrobić. Jak długo jest traktowany z szacunkiem.
* Lestral Kirmanik: pilot Quispis
    * OCEAN: (E+A+C-): Uwielbia przygody i często działa impulsywnie. Emocje nosi na wierzchu i łatwo się dostosowuje. "Melduję, że wysadziliśmy ten most, bo była impreza!"
    * VALS: (Stimulation, Face): Wesołek ekipy który dba o to, by być postrzegany jako wesołek. Lubi jak coś się dzieje. "Panie sierżancie, melduję, że da się zrobić z tej sytuacji coś zabawnego."
    * Core Wound - Lie: "Wychowałem się na ulicach, nikt mi nie pomógł" - "Jeśli się nie będę pokazywał i nie będę widoczny, znowu mnie zostawią; marketing, baby".
    * Styl: Bezceremonialny, głośny i pełen energii. Ubiera się niekonwencjonalnie. Robi dowcipy, ale ogólnie jest lubiany i dba o swoich.
* Leon Varkas: żołnierz p.d. Kajrata
    * OCEAN: (E+N-O+): Zbyt pewny siebie. Agresywny wobec przeciwników i ma agresywne plany. Mało czym się przejmuje.
    * VALS: (Hedonism, Achievement): Doświadczony, mnóstwo trenuje i próbuje być lepszy od innych. "Jestem najlepszy - w łóżku i na polu bitwy".
    * Styl: Dobrze ubrany elegancik wizualnie, ale jak otworzy usta - sprośny. Świetny infiltrator, doskonale robi nożem i w walce wręcz
* Alaric Rakkeir: kapral p.d. Kajrata
    * OCEAN: (O-E+N-A+): Naturalny przywódca, silny, zdecydowany, z dystansem do siebie.
    * VALS: (Responsibility, Security): Bardzo dba o swoją drużynę i jest odpowiedzialny za jej działania.
    * Styl: Solidny, z autorytetem, często z cygarem w ustach. Inspiruje innych do działania.
* Nero Parmaion: szef salvagerów Danuiaian
    * OCEAN: (E-N-): Zdystansowany, rzadko okazuje uczucia. Ma wewnętrzne poczucie sprawiedliwości. "Astoria to planeta. Ludzie pokonują planety."
    * VALS: (Security, Self-Direction): Głównym celem jest przetrwanie, ale nie pozwoli, by niewinni cierpieli jeśli może zapobiec. "Nie za wszelką cenę. Nie za tą cenę."

### Scena Zero - impl

.

### Sesja Właściwa - impl

Amanda wyszła z prowizorycznego biovatu, w dużo lepszej formie. Jest ciemno, nawet jak dla niej. W ciemności wyłoniła się postać w kitlu medycznym.

* Caelia: "Furio, status?"
* Amanda: (podaje status)
* Caelia: (mrugnęła potwierdzająco) (pokazała oczami drzwi) (Amanda widzi Xaverę w podobnym biovacie; ta jest w złym stanie)
* Amanda: (spojrzenie z intencją pytania do Caelii)
* Caelia: (lekki uśmiech, więc będzie dobrze)

Amanda idzie sobie przez drzwi, tylko założyła jakieś lekki pancerz. Pancerz jest wyraźnie patchworkiem, ale pasuje do Amandy. Lekarka nieruchoma została przy monitorach. Savaranka.

Amandę dobiegły głosy. Niezadowolenie. Praktycznie kłótnia.

* Garzin: Komendancie Kajrat, z całym szacunkiem, zmarnowaliśmy mnóstwo zasobów by przechwycić cztery dziewczynki. Zanim je naprawimy, będą straszne koszty. Zamiast zabezpieczyć pozycję.
* Kajrat: Poruczniku Garzin, Furie są noktiankami. Nie zostawiamy swoich. Żaden noktianin nie będzie zostawiony na śmierć i niewolę TUTAJ.

(w oczach Amandy: Garzin wygląda na weterana, więcej - wygląda na oficera taktycznego; Kajrat natomiast jest oficerem logistycznym)

* Garzin: Kajrat, jesteś komendantem bo jesteś najwyższy stopniem, ale my nawet nie jesteśmy z tych samych jednostek. Nie dam Ci wytracić ostatnich noktian jakich mamy. To zabije wszystkich.
* Kajrat: Garzin, możemy ich uratować oraz uważam, że te Furie są kluczem do naszego przetrwania na tej planecie. Co widzisz jak patrzysz do magazynów?
* Garzin: Mamy sprzęt, ale słaby. Mamy problemy z wodą, jedzeniem, wszystkim.
* Kajrat: Humor me, kto wie jak to pozyskać? Skąd ktokolwiek może pozyskać cokolwiek?
* Garzin: Miejscowi mają tą wiedzę
* Kajrat: Dlatego Furie są kluczem. To infiltratorki. Musimy podpiąć się pod Szczeliniec. Nie przetrwamy na tych terenach z grupą noktian, a na pewno nie przetrwają savaranie.

Amanda wchodzi na totalnego lokalsa (pełne aktorstwo) do sali gdzie jest Kajrat i Garzin. Amanda czeka na reakcje. Garzin NATYCHMIAST pistoletem celuje w Amandę.

* Amanda: (po noktiańsku) Komendancie? Melduję się na służbę.
* Kajrat: Amanda, druga aktywna Furia. Twoje dwie siostry są w medvatach.
* Amanda: Xaverę widziałam. (do Garzina) poruczniku Garzin, to nie będzie konieczne
* Kajrat: (z uśmiechem) Dlatego Furie są kluczem do uratowania noktian.
* Garzin: (schował broń) Tak jest, komendancie. (niezadowolony, ale akceptuje)
* Amanda: rozumiem, że powątpiewa pan w wartość bojową Furii?
* Garzin: Nie, agentko. Powątpiewam w sensowność palenia większości zasobów na ratunek pojedynczych osób. W tym Furii.
* Kajrat: Odzyskamy te zasoby. Nie odzyskamy martwych lub zniewolonych noktian. (do obu) Noktianie potrzebują czegoś więcej niż tylko przetrwanie. Potrzebują nadziei. Potrzebują walczyć o coś. Potrzebują lepszego jutra. Przetrwać i wymrzeć na tych pustkowiach potrafi każdy. My jesteśmy uosobieniem Noctis. (mówi to z przekonaniem)
* Garzin: Tak jest, komendancie. Uważam to za błąd, ale ja bym nie wyciągnął mnie i trzech żołnierzy z jednostki, a Ty tak. Więc... zaufam Ci. Znowu. Po prostu nie idź za daleko, proszę.
* Kajrat: Nie zawiodę zaufania, jakim obdarzyło mnie Noctis. Wyciągnę nas wszystkich z tego bagna. 
* Kajrat: (do Amandy) Garzin i Isaura zajmą się wyciągnięciem Ayny i Leiry. A dokładniej, udobruchaniem Symlotosu. Uderzą w Czaszki.
* Amanda: przepraszam komendancie, nie zostały nam inne opcje, żadnej innej nie byłam w stanie wymyśleć
* Kajrat: są tam dwie Furie nadające się do uratowania i zasilenia naszych sił, spisałaś się dobrze. Dla Ciebie mam inne zadanie.
* Amanda: (opowiedziała o Lucii i jak została złapana)
* Kajrat: z tym związana jest Twoja misja. Nie tak daleko znajduje się rozbity _supply ship_ Iliminar Caos. Leży tam od ponad miesiąca. Iliminar Caos ma bardzo potężny mechanizm nadawczo-odbiorczy. Twoim celem jest pomóc doprowadzić do niego inżyniera, by ów inżynier nadał odpowiedni sygnał przy użyciu systemów Iliminar Caos. Oraz wydobądźcie co się da z tej jednostki.
* Amanda: jak duży jest oddział?
* Kajrat: w sumie sześć osób. W tym pilot oraz inżynier. Oboje są zdolni do walki. Jesteście w zmodyfikowanym APC Quispis.
* Amanda: (lekko odwraca się do Garzina, ale pytanie do Kajrata) Kto dowodzi?
* Kajrat: (do Garzina) kogo rekomendujesz w takiej operacji?
* Garzin: Alaric. Alaric Rakkeir. On sobie poradzi i opanuje sytuację. Plus, będzie używał unikalnych umiejętności Furii a nie puści ją na pierwszą linię. (spojrzał na Amandę) Będzie Cię testował, ale będzie Cię słuchał.
* Amanda: bardzo dobrze.
* Garzin: Furio, udaj się do... inżynierii. Tam dobierz sprzęt.
* Kajrat: trzeci korytarz od zawału w prawo.

Faktycznie, są w podziemiach; są tu słabe światła, ogólnie zapach pleśni... nie jest to idealna baza. Ale jest. I wygląda dość bezpiecznie, zwłaszcza z dwójkami patrolujących żołnierzy. Amanda przestaje wyglądać lokalnie, to fatalny pomysł w takich okolicznościach.

Amanda dotarła do "inżynierii". Tam jest silny, postawny, wysoki i umięśniony koleś.

* Dragan: Oi, nie widziałem Cię tu.
* Amanda: (spokojnym, cichym głosem) Komendant Kajrat powiedział że mam tu pobrać sprzęt jakiego potrzebuję
* Dragan: Dragan jestem. (dał rękę do uciśnięcia) (widać entuzjazm). Ty jesteś tą sławną Furią?
* Amanda: Nic nie wiem o 'sławną', ale Furią na pewno.
* Dragan: Dobra, tu masz sprzęt. Wybierz coś. Mamy mało amunicji do niektórych części, ale wy się znacie na rzeczy. Z tego co wiem - zero servarów na tą operację, tak?
* Amanda: Nie znam parametrów. Ale jest to bardzo prawdopodobne przez możliwość wykrycia sygnatury energetycznej.
* Dragan: Jak co, mamy cztery Culicydy.
* Amanda: Jak z pakowaniem tego do Quispisa?
* Dragan: Culicydy są dobre na otwartej przestrzeni, ale... jedna osoba mniej jeśli weźmiecie Culicydy.
* Amanda: Zatem nie. (przegląda lekkie pancerze i wybiera coś dobrego do niej)

Dragan pokazał jej Quispis. To jest oryginalnie hoverscout, ale zmodyfikowany i zamaskowany jeszcze mocniej, łącznie z jakimiś glonami itp. na powierzchni. To jest dokładnie jednostka insercji. Szybka, średniodystansowa, wysoka manewrowność. Czyli Zespół ma: w sumie 6 osób z pilotem, z czego jeden inżynier. Cztery jednostki dedykowane bojowe, w tym Amanda.

* Amanda: Ty z nami lecisz? Czy nie?
* Dragan: Tak, ale nie jestem najlepszy w walce. Jestem inżynierem, ale cholernie dobrym. (z uśmiechem) Dawno nie byłem w splugawionej jednostce; a to pasuje do sygnatury Iliminar Caos.
* Amanda: Nie ma znaczenia.

* Amanda: "Będziesz miał problem zmieścić się z servarem i resztą z nas, ale jesteś inżynierem, możesz coś być w stanie złożyć"
* Dragan: "Masz rację; co myślisz że robię od pewnego czasu? Kanibalizuję jednego Culicyda by złożyć inżynieryjną jednostkę."

Tr Z +3:

* V: udało mu się złożyć coś, co ma sens i się przyda
* Vz: udało mu się pokazać że jest cholernie dobry; to naprawdę jest sensowna struktura. Amanda uśmiechnęła się lekko z szacunkiem.

Chwilę potem wpadła grupa żołnierzy, cztery osoby. Amanda ma na sobie lekki pancerz, nóż, snajperka, shotgun. Amanda idzie lekko z założeniem, że jak trzeba się ostrzeliwywać, fall back. Scout & Scavenge > Fight.

Dowódca od razu spojrzeniem przyhaczył Amandę. A Amanda ocenia go po swojemu.

* Alaric: Ty jesteś Furią?
* Amanda: Amanda.
* Leon: (szept, ale sceniczny) Ona jest za ładna na bycie Furią.
* Amanda: (odwraca się do Leona) (encyklopedycznie) Dobry wygląd jest elementem szkolenia Furii, bardzo przydatny do akcji, nie ma możliwości by być zbyt ładną.
* (ogólny śmiech, Amanda nie wie o co chodzi)
* Alaric: (spokojnie) Spokój. Nie mamy intel, nie możemy go pozyskać, my jesteśmy intel.
* Lestral: intel-igentni inaczej, kapralu?
* Alaric: (ignorując) Musimy wejść do Iliminar Caos, przebić się, wyjść. To oznacza konfigurację krótką. Musimy też się tam bezpiecznie dostać. To konfiguracja długa. (patrząc na Amandę) Furia dobrze wybrała.
* Lestral: panie kapralu, minigun?
* Alaric: Furio, Twoja snajperka jest ppanc?
* Amanda: 'Amanda' może być. I przy odpowiedniej amunicji, tak. Nie znając parametrów misji wzięłam niewielki zapas tego typu.
* Alaric: Skonfiguruj się przeciwpancernie. Ty i ja zajmiemy się celami ciężkimi. Ty masz daleki zasięg.
* Amanda: Tak jest.
* Lestral: przeszła do drugiej bazy, kapralu, już możesz ją po imieniu tytułować. Ale wiesz, tylko na daleko.
* Amanda (z brakiem zrozumienia): masz drugą bazę?
* (ogólny śmiech)
* Alaric: dobra, spakujcie się, mamy plan, mniej śmieszkowania...
* (Amanda patrzy z brakiem zrozumienia na Alarica)

W ciągu godziny wyruszyli. Mogli szybciej, ale to nie miało znaczenia - musieli ustawić ops, drogę, mapę, plany itp. A Amanda poprosiła Dragana by wyjaśnił jej możliwości pojazdu itp. Ku jej żałości - jeśli wyjdzie poza obrys pojazdu, może być wykryta. Ale może w sytuacji alarmowej się wychylić i strzelać. Niewygodne, wymaga zaufania do zespołu, ale da radę. A okiem Amandy żołnierze są dość doświadczeni. Nie są to rekruci. A Alaric jest weteranem i to widać.

Amanda podbiła do najsłabszego fizycznie żołnierza. Symulowane ćwiczenia gdzie on musi ją utrzymać jak ona coś robi. BEZ SZANS. Jedyna osoba jaka może solo utrzymać Amandę to inżynier Dragan. Ten koleś to bestia. MIMO jej pancerza itp. A jeszcze na to ma Culicyd, więc on może Amandę nosić.

W Quispisie jest ciasno. Pilot ma miejsce (bo musi), Zespół się gnieździ z tyłu i jeden żołnierz narzeka. ZAWSZE ktoś narzeka. 

* Pilot, Lestral, żartuje: "Oj nie narzekaj, sam bym się przytulił do Amandy"
* Janne: "Ja się przytulam do cholernego niedźwiedzia"
* Dragan: (filozoficznie) "Opancerzonego niedźwiedzia"
* Lestral: "To kto się przytula do Amandy? Leon?"
* Leon: "Yyyup."
* (ogólne smutki i protesty)
* Alaric: "To wasza towarzyszka broni. Trochę szacunku."
* Leon: (szeptem) "Masz ochotę na odrobinę... szacunku?"
* Amanda: (patrzy na Leona intensywnie z zębatym uśmiechem) "Jak wrócimy z misji, dam Ci szansę zapracować na mój szacunek"
* (gwizdy)
* Alaric: (znudzonym głosem) "Dobrze panienki... i Ty Furio... (poprawiając się) skupcie się na misji..."
* Leon: (się oblizał) "Stoi."
* Lestral: "Jaaasne, teraz to Ci stoi, poczekaj jak cię przeciora"
* Alaric (ma minę jakby chciał dowodzić jakimkolwiek innym oddziałem)...
* Dragan: dziewczyny są spoko. Ale opowiem Wam o takiej fajnej cewce... (i zaczął opowiadać)
* Alaric: (ciągnie Dragana za język)
* Lestral: ale kapralu...
* Alaric: słuchaj, może się przydać. Będziesz miał potem z tego test. Dla Ciebie - pojedynczego wyboru. Może dostaniesz statystyczne punkty.

Lestral próbuje manewrować przeciwko skomplikowanemu terenowi, między potworami i żeby nie zostać zauważonym przez żadną z frakcji.

Tr Z +3:

* V (p): Lestral skutecznie wymanewrował kilka dziwnych rzeczy; oddział to widział.
* Vz (m): Lestral doprowadza Quispis na miejsce bez żadnych komplikacji; wyraźnie Dragan i mechanizmy zadziałały

.

* Amanda: (cicho, do Lestrala) Dobra robota
* Lestral: Dzięki. Widzisz Leon? Teraz ja mam u niej szanse. (jedno spojrzenie Alarica) Zamykam się, panie kapralu. Przepraszam, Furio. (Amanda przyjmuje skinięciem głowy)

Lestral NAGLE zmienia lokalizację hoverAPC. Chowa się za wzgórzem.

* Lestral: "Panie kapralu melduję, ludzie. Na oko... kilkanaście. I mają... lamy?"
* Amanda: (cicho) Kapralu, czy mam się udać na zwiad?
* Alaric: Poczekaj. Dragan, mamy coś w tym rzęchu by dać nam wideo? Kamera, sonda?
* Dragan: ...rzęchu... (autentycznie ciężka uraza) Masz jedną zwiadowczą dronę, ale ma krótki zasięg bo ją stracimy. 2-3 minuty.
* Alaric: Lestral, puść to. (do Amandy) wolę Cię na snajperce jeśli będziesz potrzebna.

Tp +3:

* VV (p-m): Drona złapała kluczowe dane.
    * Wyglądają na salvagerów; są uzbrojeni i poruszają się dobrze. Mają perymetr obronny. Chronią się zarówno przed zewnętrzem jak i statkiem.
    * Mają sprzęt cięższy i transportują rzeczy zwierzętami. Duże parzystokopytne.
    * 17 osób. Z czego co najmniej połowa uzbrojona.
    * wyglądają na doświadczonych w walce, ale nie mają szkolenia wojskowego.
    * to NIE są Czarne Czaszki. To lokalni salvagerzy, rozbili tu obóz; na oko, tydzień temu czy coś.
    * część rzeczy już wydobyli z jednostki, ale dużo mniej niż Amanda by się spodziewała.
    * są tam osoby obu płci, ale problem w tym, że jest ich mało więc wszyscy się znają

Przeczekanie nic nie da.

* Leon: Kapralu, z zaskoczenia zdejmiemy...
* Amanda: Nie dość.
* Leon: Serio, Furio, mamy broń dalekosiężną, pięć strzałów - pięć trupów. 10 osób zdejmiemy zanim się zorientują, że cokolwiek się stało. Rozbijemy ich.
* Alaric: Zakładając, że chcemy ich zabijać.
* Leon: To astorianie
* Alaric: Nie wiem jak Ci to powiedzieć, ale astorianie są źródłem jedzenia. I nie mówię o kanibalizmie.
* Leon: Próbują się włamać na noktiańską jednostkę.
* Amanda: Wrak który leży od ponad miesiąca i nikt go nie objął. Nie zrobiłbyś tego samego na jego miejscu?
* Leon: Zrobiłbym, ale teraz ja tu jestem.
* Alaric: Więc ich nie zabijamy. A przynajmniej - nie bez analizy innych możliwości. Dragan? Furia?
* Lestral: A czemu nigdy mnie nie pytasz o zdanie?
* Alaric: Test jednokrotnego wyboru, Lestral. Chyba że ktoś ma coś dobrego do dodania?
* Dragan: Handel? Możemy pomóc im wejść, a oni pomogą nam wydobyć kluczowe rzeczy i wysłać sygnał. Możemy też nawiązać pierwszy sojusz.
* Alaric: Jeśli do nich po prostu pójdziemy, oni nas ostrzelają.

...

* V: Sonda podleciała bliżej by ich podsłuchać. Mimo protestów Dragana, bo ona na taki dystans może już być 'stracona'. Ale doleciała, podsłuchała i wróciła. Ku zdziwieniu Dragana.
    * Wśród 21 salvagerów 4 to niewolnicy. Niewolnicy idą pierwsi.
    * Salvagerzy są 'raiding party' pobliskiej Enklawy, która próbuje objąć ten statek i go rozszabrować.
    * Statek jest Skażony. W środku są "mutasy". Szabrowanie jest wolną operacją
    * Salvagerzy boją się Czarnych Czaszek i Symlotosu. I zawsze szukają kobiet.

Amanda przedstawia plan:

* Podszywamy się pod Czarne Czaszki
* Powodujemy panikę wynikającą z obecności Czarnych Czaszek (a wiemy że to wyścig z czasem aż Czaszki się zainteresują)
* Chcemy ich porządnie wystraszyć i porwać niewolnika
* Jako, że mają cztery psy, ale psy chodzą w parach na warcie; jest słabość. Brak wojskowego wyszkolenia.
* CZYLI: porywamy patrol. Lub dwa. Bez psów.

Grupa wydzielona kombinuje.

* Alaric: Jeśli porwiemy trzy osoby... zmieńmy to.
* Alaric: Porywamy osoby, gdy osiągniemy trzy, wyjdzie Dragan z czarną czaszką i powie, że jeśli zostawią łupy to oddadzą porwanych. I niech się zmywają.
* Amanda: (patrzy na Dragana, czy on w to umie)
* Dragan: Zrobię tak: 'ludzie! jeśli chcecie odzyskać swych pobratymców...'
* Amanda: (przerywając Leonowi) (przysuwa się do Dragana) (zimnym głosem) macie dwie opcje - odejdziesz stąd ze swoimi ludźmi lub zginiesz (z nieruchomą twarzą)
* Leon: chyba się zakochałem. (uśmiech) dajmy Furii to zrobić, ona jest straszniejsza niż Dragan. Małe, słodkie a uwierzę że ma kolekcję oderżniętych jaj.
* Lestral: byle twoich nie dodała do kolekcji. Szkoda by była jakbyś sikał na siedząco.
* Alaric: postanowione. Porywamy 3 osoby, żądamy zostawienia jednego niewolnika i opuszczenia terenu. Że niby "praktykanci ćwiczyli" i Furia, jako przełożona, puszcza wolno.
* Leon: dobra. Ja jestem dobry w skradaniu i robocie nożem.
* Lestral: aaa, dlatego czasem zamoczysz.
* Amanda: (spojrzenie).
* Alaric: Lestral. Zamknij się.
* Lestral: Zamykam się, panie kapralu.
* Amanda: zanim wprowadzimy ten plan w życie, odrobina charakteryzacji.

Czas zrobić z nich dobrze wyglądające Czarne Czaszki - jako, że C.C. są doświadczone i _camo_, to nie jest tak trudne skoro Amanda ich widziała. A oni nie mają jednolitych strojów. Amanda przesprayowuje Culicyd, obrysowuje to (Dragan patrzy na nią jakby zgwałciła mu szczeniaczka). Amanda dowodzi operacją maskowania, zespół robi co powinien.

Tp Z +3:

* Vz (p): Nie wyglądają jak noktiańska grupa wydzielona
* Vz (m): Wyglądają jak Czarne Czaszki
* X (p): Dwie osoby mają podejście 'lepsza śmierć niż niewola u Czaszek' a nie wierzą że Czaszki ich puszczą.
* V (e): FULL TERROR. Czaszki. Mają dużo większe możliwości, bo Czaszek się wszyscy boją.

Dobrze schowany Zespół monitorujący ruchy patroli przy użyciu noktowizorów i jednej smutnej drony czeka, aż dwuosobowy patrol będzie poza zasięgiem widzenia kogokolwiek. Leon i Amanda wysłani, by przechwycić tą dwójkę.

Tr Z +4:

* Vr: Patrol został unieszkodliwiony
* V: Patrol "zniknął".

(Leon i Amanda się podkradli i jak tylko patrolujący salvagerowie ich minęli, Leon i Amanda zaatakowali z zaskoczenia; oboje unieszkodliwili swoje cele; Leon z większą brutalnością, ale oboje przeżyją). Związać w baleronik, zakneblować, ewakuować. Leon nie wierzy, że Amanda jest w stanie zanieść na plecach jednego kolesia sama. Po prostu tego nigdy nie przewidział. LIKE WHAT.

* V: gdy jeden z patroli poszedł szukać znikniętego salvagera a drugi przejął pilnowanie, Amanda i Leon mający wsparcie nokto itp. wkradli się do obozu i porwali wartownika, który na moment oddalił się odlać. Terror rośnie.

Zdaniem Amandy, terror jest wysoki. Naprawdę wysoki.

* Alaric: "Teraz - snajperką strzelamy w ognisko. Raz. By im pokazać. I porozmawiamy sobie z nimi."

Próba całkowitego zdominowania salvagerów, sterroryzowania ich, że jesteśmy Czarnymi Czaszkami. (+FULL TERROR, +trzech zniknęło, +heavy firepower i zaskoczenie, --Czaszki zabijają od ręki, +bo nie mają wyboru)

Tp+4:

* V (p): Wychodzi Amanda ucharakteryzowana i oznajmia, że mają opuścić ten teren, dostaną z powrotem swoich ludzi ale mają zostawić niewolnika. Salvagerzy nie chcą walczyć.
    * Na tym etapie: opuszczą teren
* V (m): Znaleźli rzeczy. Amanda żąda części tych rzeczy ORAZ jednego niewolnika.
* Vr (e): Amanda dostaje intel co tam jest
    * dziwne przerośnięte rośliny, owady, mutanci, byty stworzone z ciał, zrujnowana jednostka...
    * salvagerzy odejdą i będą bali się wrócić a Zespół może bezpiecznie wrócić i mieć korzyści
* V: (e+): Amanda chce iść o krok DALEJ
    * salvagerzy mają tu ZOSTAĆ. Tam jest coś co 'Czaszki' chcą. I salvagerzy to dla nich zdobędą.
    * ale 'Czaszki' są łaskawe. Część lootu trafi do salvagerów
    * salvagerzy, całkowicie sterroryzowani, bo Amanda obiecała, że jeśli zrobią inaczej, ich Enklawa zostanie zniszczona. I gorzej.

Z jednym niewolnikiem i częścią sprzętu - zwłaszcza jakimiś zapasowymi medycznymi - Quispis wraca do bazy.

* Alaric: Nieźle. Cholernie nieźle.
* Lestral: Gdzie diabeł nie może, tam babę pośle.
* Amanda: (patrzy spokojnym, równym spojrzeniem)
* Leon: Nie wysłaliśmy sygnału, ale teraz mamy intel.
* Dragan: Inaczej przygotuję sprzęt.
* Amanda: Potrzebujemy możliwości transportu.
* Dragan: I jakiejś formy _environmental_. Chociaż _rebreather_. Byliśmy na to niegotowi.
* Amanda: Jednym z celów był zwiad, prawda? Parametry się nie zmieniły. Dostosujemy się do wyników i informacji.
* Alaric: Zobaczymy, co komendant z tym zrobi. Mam wrażenie, że wymyśli coś chytrego.

## Streszczenie

Kajrat przedstawił nową misję - ratujemy noktian i podpinamy się pod Sojusz Letejski. Wysłał Amandę i Quispis, by wysłali sygnał używając dawno rozbitego _supply ship_ Iliminar Caos. Grupa wydzielona Quispis wykryła, że tam są salvagerzy - udało im się zastraszyć ich (udając że są Czarnymi Czaszkami) i zmusić do współpracy. Mają też jednego niewolnika salvagerów jako intel dla Kajrata.

## Progresja

* .

## Zasługi

* Amanda Kajrat: zero poczucia humoru, co sprawiło pewną konfuzję w grupie wydzielonej Quispis; jakoś się zintegrowała z grupą noktiańską. Akceptuje erotyczne żarty na swój temat, akceptuje brak profesjonalizmu przez stosunkowo niskie morale, zaproponowała plan przebrania się za Czaszki by zastraszyć salvagerów i porwała kilku wartowników salvagerów z pomocą Leona.
* Caelia Calaris: lekarka utrzymująca Amandę i Xaverę w najlepszym stanie jak potrafi. Praktycznie się nie rusza, creepy nawet jak na savarankę.
* Ernest Kajrat: major logistyki oddziałów szturmowych Noctis; przejął kontrolę nad siłami Noctis na Astorii (co wyszło z dyskusji z Garzinem). Symbol lepszego jutra dla noktian. Kazał zabezpieczyć intel i zasoby z rozbitej jednostki Iliminar Caos. Dla niego Furie - infiltratorki - są kluczem do integracji i wbicia się w struktury Astorii.
* Edmund Garzin: porucznik i oficer taktyczny, ale z innej jednostki niż Kajrat; chce kontynuować wojnę na Astorii i przegrupować się. Słucha poleceń Kajrata, choć nie rozumie, czemu dla Kajrata Furie są kluczem. Dowodzi pionem personalnym i taktycznym niewielkich sił Kajrata.
* Isaura Velaska: wraz z Garzinem wykonuje operację pomocy Symlotosowi, by Ayna i Leira były bezpieczne.
* Dragan Halatis: główny inżynier Kajrata; przebudował servar Culicyd by mieć inżynierski. Przekształcił Quispis, by był hoverAPC a nie tylko jednostką zwiadowczą. Wygląda jak niedźwiedź, ma chłopięcy entuzjazm. FATALNY w zastraszaniu. Zdecydowanie woli handlować z Enklawami niż po prostu zabijać ludzi.
* Lestral Kirmanik: śmieszek, próbujący żartować z Amandy i jej przyszłego pójścia do łóżka z Leonem, ale ogólnie dobry pilot Quispisa. Ogólnie jest opinia, że między dwójką jego uszu jest prześwit ;-).
* Leon Varkas: cały czas próbuje flirtować z Amandą, ale jest kompetentny; wraz z Amandą porwali kilku wartowników. Infiltrator. Ma opinię absolutnego kobieciarza.
* Alaric Rakkeir: nie czuje się komfortowo dowodząc zespołem gdzie jest kobieta (Amanda), ale dobrze prowadzi operację. Trzyma morale i nie chce zabijać salvagerów. Słucha pomysłów Amandy. Wierzy w Kajrata i Garzina.

## Frakcje

* Nocne Niebo: Kajrat ściera się z Garzinem o to, jaka jest przyszłość Nocnego Nieba - militaryści na terenie Granicy Anomalii czy wbicie się w Sojusz Letejski w jakiś sposób
* Nocne Niebo: mają dostęp do hoverAPC klasy Quispis; 6 osób (lub 4 w servarach), dość dobre maskowanie
* Enklawa Planganis: wysłała zespół scavengerów by zdobyli materiały z Iliminar Caos; Zespół ich zastraszył i zrzucił winę na Czaszki
* Czarne Czaszki Enklaw: tym razem całkowicie niewinni, ale zostali wykorzystani jako zastraszanie Enklawy Planganis przez Amandę Kajrat

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, SW
                    1. Granica Anomalii
                        1. Ruina miasteczka Kalterweiser: pod nim jest tymczasowa baza Kajrata / Nocnego Nieba
                        1. Ruina Iliminar Caos: scavengowana przez salvagerów Enklawy Planganis; starcie z Nocnym Niebem udającym Czarne Czaszki

## Czas

* Opóźnienie: 1
* Dni: 2

## Specjalne

* .

## OTHER

.