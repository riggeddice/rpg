## Metadane

* title: "Piękna Diakonka i rytuał nirwany kóz"
* threads: brak
* motives: helena-trojanska, magowie-ignoruja-obowiazki, dobra-impreza
* gm: żółw
* players: kamilinux, anadia, kić

## Kontynuacja
### Kampanijna

* [230523 - Romeo, dyskretny instalator Supreme Missionforce](230523-romeo-dyskretny-instalator-supreme-missionforce)

### Chronologiczna

* [230523 - Romeo, dyskretny instalator Supreme Missionforce](230523-romeo-dyskretny-instalator-supreme-missionforce)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * ?
* CEL: 
    * 
    * Działania
        * Elena: duchy, historia bazy, 
        * Karolinus: Maja, sekrety mroczne, 

### Co się stało i co wiemy

* Kontekst
    * .
* Wydarzenia
    * Itria przyniosła kwiaty Lilii Anandobao (błogość, euforia + eksplozja). Herbert się nimi zajmuje...

### Co się stanie (what will happen)

* S1: To Herbert. On miał coś wspólnego z tymi duchami.
* SN: 
    * Kallista uważa, że Herbert coś kręci z Itrią. A Itria jest szpiegiem.
    * Kozy odnalazły drogę do przejścia w Inną Fazę Astralną XD. 
        * RAMPAGE!
    * Dźwiedź nie chce Itrii. Maks tak bo jest słodka. Kalista nie. Herbert bardzo tak.
        * Maks nie wie jak się pozbyć Itrii.
    * Itria chce potężny rytuał na imprezie by ZROZUMIEĆ i wzmocnić Lilie Anandobao.
        * Przyjdzie Potwór z Verlenlandu a wszyscy _crippled_


### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* Impresja Ignicja Incydencja Diakon
    * ENCAO:  +--0+ | dramatic, spontaneous, liberty | VALS: Self-direction, Hedonism, Stimulation | DRIVE: Przygody!!!
    * styl: BR; dramatic, flamboyant, spontaneous, sensitive, "I lead you follow"
    * "wszystko jest jednym wielkim doświadczeniem, rytuałem przejścia, przeprowadzę Cię przez to!"
    * "ROZUMIEM co się tu dzieje" (fiksacja na kolejnej dziwnej teorii)
    * magia: "astralne wizje i mieszanie rzeczywistości"; "amplifikacja tego co jest"
    * ruch: "wchodzi, wszystko zmienia i pozostawia za sobą pożogę"
    * piękna brunetka
* Herbert Samszar
    * ENCAO:  0-0++ | Nie ma blokad;; Uparty jak osioł | VALS: Benevolence, Hedonism >> Self-Direction | DRIVE: Tajemnica Wszechświata
    * styl: GB; peaceful, silent, cycle-focused, nurturing | tradition, community, faith, nature
    * magia: "ten, który śpiewa roślinom"
    * ruch: "ten byt to nie było coś czemu powinno się było dać szansę"
* .Wir Os Astralnych
    * Akcje: 'psioniczne uderzenie', 'chmara chaosu', 'psychiczne przeszywanie', 'absorpcja energii', 'powodowanie halucynacji', 'burza owadów'
    * Defensywy: 'chmara tarcz' (życie) (3), zdolność do rozpraszania i rekonfigurowania formy, ekstremalnie szybki, trudny do zranienia z powodu roju
    * Słabości: 'wrażliwość na zakłócenia psychiczne', 'potrzeba stałego dostarczania energii psionicznej do utrzymania jednolitej formy'

### Scena Zero - impl

Elena dała znać Viorice, że Romeo spieprzył operację (co zrobił i jak spieprzył) i się zbliżył do Mai. Wydźwięk: "niech on nie robi krzywdy Mai i niech sobie krzywdy nie zrobi."

Niestety, zarówno Albert (ojciec Mai) jak i nowy Samszar, Nataniel, nie dopuścili Was do bazy. "Nie ma żadnej bazy", "nic się nie dzieje" i silny sygnał "słuchajcie, imprezy są TAM".

Ślady "roślinne" wskazały na Herberta Samszara. Bardzo poczciwego maga.

### Sesja Właściwa - impl

Fort Tawalizer. Wielki baner "impreza roku nadchodzi". Jest też rezygnacja na twarzach żołnierzy - zwłaszcza widząc Was.

* Itria Diakon -> potencjalnie niebezpieczne jądro chaosu i szaleństwa; gdzie ona tam kulty. Ale jest słodka.
    * Impresja Ignicja Incydencja Diakon
* Herbert Samszar -> ekspert od roślin, który ma coś wspólnego z podziemną bazą.
* Maks Samszar -> lokalny dowódca Fortu Tawalizer.

Żołnierze nie są szczęśliwi z powodu imprezy i patrząc na ich "linie rozmów", nie cieszy ich Itria.

Tr Z (+Elena) +3:

* X: o Itrii są OPOWIEŚCI. Chyba próbuje założyć kult wśród łabędzi. Lub kóz. Nie wiadomo dokładnie
    * "niech kozy próbują odnaleźć się w absolucie"
    * tak. Żołnierze są oddelegowani do nauki kóz jak wejść w absolut.
    * żołnierze byli zmuszeni do parad, akrobacji... by Itrię zabawić. Maks chciał jej zaimponować.
    * Itria NIE JEST szpiegiem. Ona NIE MA złych intencji. Ona jest inkarnacją chaosu.
    * (Maks się dowie o nowych Samszarach na terenie)
* (+1Vr) Elena współczująca poza, bibliotekarka, -> V
    * Itria skupiła się na kozach. Ona SERIO uważa, że im jest potrzebna siła wyższa.
    * Itria ma nowy projekt. Coś związanego z imprezą i wspólnym uduchowieniem.
    * Maks i Herbert współpracują z Itrią.
        * Itria, wyszkolona na dworze Diakonów, czaruje wdziękiem ogólną populację.
    * Macie PEWNOŚĆ, że Itria nie używała: eliksirów miłości, magii, itp. To nie w jej stylu
* X: Żołnierze powiedzieli z pełnym przekonaniem, że "najlepszy konsultant do spraw wojskowości, którego ma Maks" protestował przeciwko Itrii i został aresztowany. Powiedzieli prawdę. Ale nie całą.
    * Zaznaczyli też jego egzotyczną urodę i nie uległ nawet czarowi Itrii.
    * Żołnierze tęsknią za konsultantem. Wolą jego niż Maksa. Mają niskie morale.
    * W dokumentach nie ma informacji o konsultancie. Ktoś "dyskretny".

Herbert jest w okolicach północnej części F.T. W okolicach pastwiska dla kóz.

Na miejscu - Herbert (lekko łysawy, broda, bez wąsa), ubrany w eleganckie nieczęsto noszone szaty i towarzyszy mu piękna brunetka. I oboje z werwą coś tłumaczą kozie. A Herbert podaje rośliny do jedzenia. Okoliczni żołnierze wyglądają nieszczęśliwie, oni monitorują teren ale od czasu do czasu brunetka patrzy na nich i żołnierz wraca do dyskusji z kozą.

Karolinus i Herbert dyskutują dyskretnie o strojach (bo Itrii zależy by się przebrał w coś pstrokatego by kozy osiągnęły nirwanę XD). Herbert liczy na orgię. Karolinus jest... rozsądniejszy - do Itrii jest kolejka.

Karolinus dał Herbertowi słowo, że się przebierze - Herbert poszedł do Strzały. W trójkę (a naprawdę w czwórkę) konferują.

* H: "Kolega zniknął?"
* K: "Cede, nie wiem czy pamiętasz, nie odzywa się. Co wiesz, widywałeś się z nim"
* H: "Nie powinieneś dawać słowa jak nie dotrzymasz."
* K: "Szukam, nie "znajdę""
* H: "Nie wiem gdzie jest, ale wiem gdzie MOŻE być. Poszukam."
* E: "Herbercie..."
* H: "Ty też się musisz przebrać, wiesz o tym? Itria Ci pożyczy jakieś stroje dla dziewczyn"
* H: "Wiesz, grunt by kozy Cię nie widziały."
* E: "Karmazynowy Świt. Piwnica. Dziwna baza. Duchy i potwory. Ślady wskazują na Twoją obecność."
* H: (blank) "Nie wiem o czym mówisz. Piłaś coś?"
* E: "Nie wiesz o czym mówię, ktoś się podszywa. Zgłosić wyżej by się zajęli?"
* H: "Nie zgłaszaj wyżej. Nie chcesz tego. Naprawdę. Słuchaj - to nie na teraz. Teraz mam ważniejsze rzeczy. Cede zniknął!" /radość że ma ważniejszy powód.

Elena i Karolinus chcą przyskrzynić Herberta, by coś im powiedział, by był już ślad.

Tr (+Elena)(-Herbert jest zajęty Itrią)(+Elena może pomóc Itrii w nirwanie kóz) +3:

* V: (podbicie) 
    * Elena chce informacje o tych bazach. 
    * Herbert: "słuchaj, czego nie wiesz, nie zrobi Ci krzywdy"
    * E: "Mogę udawać że dalej nie wiem, zostanie między nami tutaj"
    * H: "Nie możemy rozmawiać o tym na terenie naszego powiatu. Nie, gdy sentisieć słyszy."
* X: (podbicie)
    * Herbert: "Eleno, jesteś dziewczyną. Wiesz, jak myślą dziewczyny. Pomóż mi zaciągnąć Itrię do łóżka."
    * E: "Chcesz publikacje 'jak uwieść Diakonkę?'"
    * (Strzała, dyskretnie) -> drukuje informacje na co ostatnio poleciała Itria, 3 miesiące wstecz.
* X: (manifestacja)
    * Elena będzie próbować doprowadzić do Herbert x Itria.
* Vz: (manifestacja)
    * Herbert: "WSZYSTKO wam powiem. Skończmy sprawę z Itrią, rozdzielimy się, dostaniecie karteczką, taką szpiegowską i się spotkamy na terenie innej sentisieci i wszystko Wam powiem, ok? Nie róbcie nic sami, tu są... mroczne sekrety."

Elena jest zajęta przeszukiwaniem informacji o nirwanie kóz. I duchowej inkarnacji kóz. Czy whatever.

* Ex (+Itria i jej badania) (+Elena ma tu mistrzostwo) (+już wykonana spora robota) ->
* Tr Z +2:
    * X: wszystkie rytuały tego typu są _dziwne_. Tzn. "Pakt robi reportaż"-level dziwne. Wbrew pozorom to co robi Itria nie jest idiotyczne.
    * V: (podbicie)
        * Elena ma rytuał, który jest dość kłopotliwy i wymaga jej obecności.
    * Vz: Herbert uważa, że Elena jest mistrzynią głębokiego intelektu i Sherlockiem Holmesem naszych czasów.

Elena ma dość, by spełnić obietnicę, którą dała Hubertowi. Hubert się cieszy.

* H: "Tylko Ty, z Twoją wiedzą i umiejętnością korelowania faktów mogłaś znaleźć to, co znalazłaś w Karmazynowym Świcie. Zrobili wszystko dobrze - zamaskowali sygnatury, zrobili maskowania, ale nie przewidzieli Twego intelektu i umiejętności agregacji informacji."
    
Dobra. Czas odwiedzić Maksa.

Maks Samszar jest przygnębiony. On jest w servarze (paradnym, nie operacyjnym, ale co z tego).

* M: "Przyjechaliście na imprezę? Sporo magów ma przyjechać."
* K: "Nie, dowiedzieć się kilku informacji o znajomym co zaginął."
* M: "Tutaj?" /zdenerwowany "Mogę coś zrobić?"
* K: "Nie, Herbert ma te informacje i zauważyliśmy baner, rytuały na kozach"
* M: "Tak..."
* K: "Kiepskie morale wojska..."
* M: "CZEMU kiepskie morale wojska?"
* K: "Bo wojsko zajmuje się kozami a nie obronnością."
* K: "Czemu wojsko nie może wrócić do obowiązków?"
* M: "To są ćwiczenia wydzielone. Ja je wydzieliłem. Oni chronią naszego szanownego gościa. I kozy."
* K: "A czemu jesteś przygnębiony?"
* M: "Nie jestem."

Tr Z +3:

* X: (podbicie)
    * Strzała monitoruje Itrię, by łapać styl jej mówienia, zachowywania się, jej obraz, manieryzmy itp. by stworzyć "kopię" Itrii i nałożyć to na jakiegoś biosynta którego ściągnie Maks.
* Xz: (manifestacja)
    * Maks jest bardzo przygnębiony. Itria go nie potrzebuje tak jak Herberta. On nie ma u niej dużych szans. (oczywiście, się myli). W takim razie Elena staje na wysokości zadania - po tym, jak pierwszy rytuał przeprowadzi ona dla Herberta to potem Elena nauczy Maksa tego rytuału. Maks będzie zapewniał kozy w nirwanie. I ma duże szanse zaliczyć.
    * --> Maks jest chętny z Wami współpracować.

MUTACJA PULI: Tp+3

* V: Maks obiecuje, że wojsko wróci do swoich obowiązków. Ale nie może wypuścić konsultanta. On... on ma zły wpływ na Itrię... Ale możecie go odwiedzić i przekonać go by zachowywał się WŁAŚCIWIE.
* V: Maks, bardzo zrezygnowany, zgadza się uwolnić konsultanta. Konsultant ma być w barakach. Konsultant nie ma zbliżać się do Maksa ani kóz. I nie może robić obchodu terenu. Nigdzie, gdzie wchodzi w potencjalną interakcję z Itrią. Ale -> morale pójdzie w górę.

Konsultant jest w areszcie wojskowym. Karolinus i Elena uwalniają go osobiście. Nazywa się Łagodne Słowo.

* ŁS: "Dobra robota, żołnierze. Wiedziałem, że niekompetencja opuści ten teren."
* ŁS: "Jeśli Diakonka tu jest, to nadal nie powinno wpływać na obronność tego miejsca."
* K: "Dlatego zadbaliśmy, by władza Cię wypuściła."

Dźwiedź udał się robić dźwiedziowe, żołnierskie rzeczy. A Wam zostaje - Itria, Herbert i Maks.

Plan wygląda w taki sposób:

1. Mówimy Itrii, że potrzebujemy odblokować potencjał Herberta, który wydaje się być dobrym kandydatem na szamana kozonirwannego. Więc Itria musi się z nim przespać, bo to odblokuje potencjał.
2. Mówimy Itrii, że Herbert ma za mały potencjał. Może próbować dalej, ale proponujemy również Maksa. Więc Itria musi się przespać z Maksem.
3. Mówimy Itrii, że Maks się nadaje i Elena uczy Maksa jak skutecznie w nirwanę kóz. TAK, Maks musi przebrać się za kozę.
4. Rytuał się odbędzie i wszyscy będą szczęśliwi. Zwłaszcza kozy, bo osiągną nirwanę.

Karolinus przekonuje Itrię odnośnie planu w zaadaptowanej formie.

* Tr (Itria nie ma nic przeciwko) (+Itrii zależy na nirwanie kóz) (+Karolinus poświęca się, by jego potencjał też był sprawdzony, przez co demonstruje oddanie Sprawie) +3 +5Oy -> 
* Tp +3 +5Oy:
    * X: (podbicie) Itria podejrzewa coś nietypowego w tym planie
    * Oy: Karolinus pójdzie do łóżka z Itrią.
    * Oy: Karolinus pójdzie PIERWSZY. Maks i Herbert są zazdrośni.
    * V: (podbicie) Itria akceptuje plan
    * V: (manifestacja) plan zostanie zrealizowany.

Pozostaje nam jedynie nauczyć Maksa jak prowadzić kozy do nirwany. Elena go nauczy. Maks przebiera się za kozę a Elena prowadzi go jak żyć.

(-Elena nie ma cierpliwości do debili i głupot) (+Elena ma czas i nic się nie spieprzy)

Tr +2:

* Vr: (podbicie) 
    * Maks potrafi prowadzić kozy do nirwany. Ale za każdym razem coś się dzieje nie tak, pieprzy itp.

Elena nie ma cierpliwości. Elena powiedziała, że go nauczyła. Elena uznała, że nie będzie się tym więcej zajmować bo nie chce patrzeć na kolesia przebranego za kozę.

Karolinus i Elena opuścili teren ZANIM Maks przeprowadził Rytuał Prowadzenia Kóz Do Nirwany. Tak będzie bezpieczniej. I bezpieczniej dla reputacji - gdyby tu byli, mogłoby ich uderzyć rykoszetem.

Karolinus zgłasza do centrali co robi Maks (który to Maks ma cel pójść do łóżka z Diakonką). Czyli: destrukcja odporności, osłabienie morale, przebiera się za kozy i robi głupie rytuały...

## Streszczenie

Itria Diakon ma kolejny projekt - nirwana kóz u Samszarów. Jej obecność spowodowała chaos w Forcie Tawalizer. Karolinus i Elena S. dotarli tam by dowiedzieć się o tajnych podziemnych bazach Samszarów od Herberta, ale musieli wpierw rozwiązać problem z Itrią. W końcu doprowadzili do rytuału kóz (dzięki Elenie i jej researchowi) i trochę naprawili sytuację. Czas spotkać się z Herbertem poza terenem sentisieci i poznać sekrety badań nad duchami.

## Progresja

* Elena Samszar: wysłała do Vioriki informację o tym, że Romeo spieprzył operację. Romeo powiedział Viorice, że Elena S. sobie z nim nie radziła. Wniosek Verlenów: Elena jest 'słaba' i 'irytująca'.
* Romeo Verlen: dostał opieprz od Vioriki za to, że Elena S. wysłała do niej "omg zawiódł operację". Nie dlatego, że zawiódł operację. Dlatego, że R. pokazał tak dużą słabość że się E.S. odważyła napisać.
* Maja Samszar: Romeo poinformował ją o działaniach Eleny S. i że on nie ma zamiaru się więcej zadawać z jej 'crazy bloodline'.
* Herbert Samszar: ma tymczasowo ważną roślinę - lilię anandobao (błogość, euforia + eksplozja) którą pilnuje i rozwija dla Itrii Diakon (ofc).
* Herbert Samszar: uważa, że Elena Samszar jest Sherlockiem Holmesem naszych czasów; "wylogiczniła" lokalizację tajnej bazy.
* Maks Samszar: dostał solidny opieprz od rodu, bo Karolinus na niego nadał. Czemu? Bo Maks zagraża terenowi jaki ma chronić przez cholerną Itrię i jest 'zbyt thirsty'.
* Maks Samszar: to, że przebrał się za kozę by pójść do łóżka z Itrią Diakon, potem przeprowadził rytuał i ściągnął potwora NIE zostało mu zapomniane przez żołnierzy i w okolicy.

## Zasługi

* Karolinus Samszar: podbija do Itrii i przekonuje ją o planie - "musi się przespać z nim, Herbertem i Maksem by zrobić nirwanę kóz". Sam nie wierzy jak bardzo ta sprawa eskalowała i eksplodowała. Jak zawsze, ma sympatię prostych żołnierzy. Skupił się nie tylko na tajnych bazach ale też by pomóc w okolicy.
* Elena Samszar: znalazła rytuał nirwany kóz szperając po bibliotekach, ale nie poszukała bardzo głęboko by nie musieć się przyznawać kolegom z biblioteki. Potem nauczyła Maksa tego rytuału, ale nie zadbała o dokładność - bo to i tak będzie tylko raz czy dwa razy a nie będzie się upokarzać. Nie chce patrzeć na kolesia przebranego za kozę.
* AJA Szybka Strzała: zbiera informacje na co ostatnio 'poleciała' Itria, stanowi bezpieczne miejsce rozmowy z Herbertem i monitoruje + patternuje zachowania Itrii by zrobić jej projekcję dla Maksa.
* Impresja Ignicja Incydencja Diakon: jako 'Itria' (23) Diakonka. Tym razem wpadła na pomysł rytuału nirwany kóz, bo Samszarowie mają kozy na pożarcie dla potworów i ona chce im umilić życie. Ona skupia się na kozach, ale Herbert i Maks przez nią szaleją i robią głupoty. Skończyło się na tym, że się przespała z Karolinusem, Herbertem i Maksem (w sekwencji) i jakkolwiek doszła do tego że to plan Karolinusa i Eleny, nie przeszkadzało jej to zupełnie. Rytuał nirwany kóz został osiągnięty.
* Maks Samszar: (22); całkowicie zakręcony przez Itrię; dla niej zorganizował imprezę, zniszczył morale żołnierzy, aresztował dźwiedzia i ogólnie nie zachował się jakby się spodziewać po dowódcy bazy. Nauczony przez Elenę, przeprowadził rytuał nirwany kóz przebierając się za kozę. Na tej sesji - żałosny.
* Herbert Samszar: (26); całkowicie zakręcony przez Itrię; coś wie o tajnych bazach, ale nie chce na terenie sentisieci Samszarów o tym opowiadać. Uważa Elenę za wybitnie inteligentną. Dzięki Karolinusowi i Elenie Itria poszła z nim do łóżka, za co jest im niezmiernie wdzięczny.
* Dźwiedź Łagodne Słowo: łącznikowy dźwiedź Verlenów, który zaskarbił sobie ogromny szacunek u żołnierzy w Forcie Tawalizer; przez to, że mówi niedyplomatycznie i ostro Itria miała łzy w oczach więc Maks go aresztował. Dźwiedź potraktował to jako rozkaz i siedzi w celi (z której może wyjść XD). Uwolniony przez Karolinusa i Elenę.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Fort Tawalizer
                                1. Wzgórza Potworów (N)
                                    1. Farmy kóz: z funkcją odwracania uwagi potworów; tam przeprowadzany jest rytuał nirwany kóz
                                1. Obserwatorium Potworów i Fort (E)
                                    1. Koszary garnizonu
                                    1. Areszt: przetrzymuje się tam dźwiedzia. Dźwiedź może wyjść, ale daje się przetrzymać.

## Czas

* Opóźnienie: 4
* Dni: 3

## OTHER
### Fakt Lokalizacji
#### Fort Tawalizer

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Fort Tawalizer: niewielkie miasteczko na granicach z Verlenlandem (na północy)
                                1. Wzgórza Potworów (N)
                                    1. Obserwatoria potworów
                                    1. Baterie defensywne
                                    1. Farmy kóz: z funkcją odwracania uwagi potworów
                                1. Centrum Miasta (C)
                                    1. Ratusz
                                    1. Park miejski
                                    1. Bazar rękodzieła
                                    1. Luksusowa dzielnica mieszkalna
                                        1. Rezydencje
                                        1. Ekskluzywne restauracje
                                        1. Spa i centrum relaksu
                                        1. Jezioro Fortowe
                                1. Obserwatorium Potworów i Fort (E)
                                    1. Laboratoria badawcze
                                    1. Biura klubu potworologów
                                    1. Koszary garnizonu
                                1. Dzielnica mieszkaniowa (S)
                                    1. Domy mieszkalne
                                    1. Sklepy i centra handlowe
                                    1. Szkoły i placówki oświatowe
                                1. Dzielnica przemysłowa i rolnicza (W)
                                    1. Magazyny i warsztaty
                                    1. Farmy i pola uprawne
                                    1. Młyny i zakłady przetwórcze
