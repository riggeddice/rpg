## Metadane

* title: "Lojalna Zdrajczyni"
* threads: dzien-z-zycia-terminusa
* gm: żółw
* players: varether, monika

## Kontynuacja
### Kampanijna

* [180817 - Protomag z Trzęsawisk](180817-protomag-z-trzesawisk)

### Chronologiczna

* [180817 - Protomag z Trzęsawisk](180817-protomag-z-trzesawisk)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* Czy Felicja poważnie ucierpi i wyleci na zbity pysk?
* Czy niewinna firma dostanie wpiernicz?

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

* Jan Łowicz: Jason Hunt: objęty i zaprojektowany przez Pitera.
* Natasza Aniel: Nesta Allen: objęta i zaprojektowana przez Monikę

### Opis sytuacji

Why does the Big Tom help Phantasmagoria? Because in the past he needed some plasters and bandages and phantasmagoria sold him things which were able to heal his wounds.

The favorite company of Big Tom is Curara - it is his most beloved masterpiece.

In the past, Felicia has managed to save the company. She has managed to recover a relic which was dangerous. Catholic was used by the competition to smear Phantasmagoria.

Why need Katja break up with Tom (or v.v.)? Because Katja has managed to find a better team - flying sharks. Tom’s gang was simply not what Katja wanted (with her links with Aurum et al).

Why is Curara close to bankruptcy? Big Tom thinks there is a saboteur and he put out a reward.

Why is a woman stalking Jason? She is an aristocrat from Aurum, in her rebel phase, 21. Her name is Melinda. She wants Jason to hire her... or get into his pants.

Why is TomStone endangered? Tom tries to govern; what he wants is not just extraction of money from the businesses under his wings, he wants to improve this area. A second, internal faction inside TomStone wants to extract as much money as possible and make sure they are stronger than flying sharks.

How far is Tom’s secret admirer going to go? Well, she is willing to put Tom’s enemies in jail. He isn’t willing to kill, but any type of incapacitation is okay.

## Punkt zero

During a normal day of sales, one of the customers started looking around including one of the vases. To everyone’s horror, when the customer put the vase down, a snake left it. Or – was trying to, because Jason rapidly walked towards the vase and put it upside down to hide the snake.

But some people were ranting that it is dangerous - there is a snake, it could have hurt someone – Nesta tries to explain that the snake was harmless but after everything she had to give some 5% discount to the loudest complainers. Where did the snake come from? Nobody knows. Anomaly?

## Misja właściwa

A normal day in an occult shop. But this day there are many people from Tom’s gang entering and looking around the shop. They are not willing to buy anything, they are trying to find something or someone. Nesta asked one of those thugs, Czesiek, what they are looking for. He answered – they are looking for Felicia and Felicia tends to hang out here.

Well, she is not here now, Nesta tried to get information what happened – Felicia works for the same guy, Tom, like Czesiek. Czesiek explained – there was an important package and Felicia stole the package from two Tom’s guys, directly and in plain sight.

Now this doesn’t sound like Felicia Nesta knows. Felicia may be many things, but she is fiercely loyal to Tom and to her friends. Czesiek confirmed, but he has his orders. Can his people look around? Sure – Nesta asked Jason to look at them. And indeed, Jason caught one of Czesiek’s guys trying to steal a spoon. An expensive spoon, to add.

When Jason confronted Czesiek, Czesiek got really angry. She explained that maybe Tom is a bit to soft on his men. They will deal with this. Czesiek said goodbye and they left the shop.

When they left, Nesta phoned Felicia. What the hell happened?

To Nesta’s surprise, Felicia answered. She is on the line, but she sounds a bit high-pitched and feverish. Nesta knows that Felicia doesn’t do drugs, like ever. So Nesta really, really tried to convince Felicia to come meet her. As in – Nesta let Felicia set the terms of the meeting. And Felicia had some terms: they go to the Warehouses (flying sharks territory), they go unarmed, only they come. Not a single additional person. Ah, and they come at night.

During the night they start to go for a meeting, but Jason noticed that they are being followed by Melinda, his stalker. He sighed and went to meet her - but before he went, Kinga asked him a small figurine, whatever it was.

Melinda – of course – wanted to go with Jason on an adventure. Jason told Melinda, that he has two extremely important quests and needs Melinda’s help. Melinda’s eyes lit hearing that and Jason gave her the figurine and sent her to the Sensoplex; to find a random person and give that person figurine (of course, he did not tell her that it’s a fools errand). 

Melinda complied. Little did Jason know that people following the Team followed Melinda instead. And Melinda was so zealous in completing the quest, she has managed to get into trouble in Sensoplex. With the Technovore gang. Oh well.

The team has managed to slip through the sharks not-really-patrols. And indeed, Felicia is waiting in one of the warehouses. She is sweaty, feverish, she has some kind of long awkward gloves completely do not suit her style. She also has a manic grin, explaining about the mask, the gift and the sharks.

Wait, what?

Nesta engaged her in a conversation trying to determine what really is Felicia talking about. Felicia explained, although with difficulty, that someone has sent a package to Tom. When she was nearby people who were bringing Tom the package, she FELT it. She felt the package whisper to her. The whispers were too strong, but they were the danger to Tom.

She had to steal the package. And inside the package there was a mask. But this mask wasn’t a mask anymore - it morphed, changed - it became gloves, her gloves, her beautiful gloves.

Nesta was able to deduce what is she dealing with – a symbiotic mimic. Jason had an idea to engage Felicia and rip the mimic off her, but Nesta had a different idea. She phoned Tom, fully aware that Felicia is fanatically loyal to him, especially now.

Nesta has managed to convince Tom to come alone (he was tailed though by his crewmember) and that they three will come with Felicia and without anyone else (they were tailed too, by a Shark). Nesta also explained to Tom that Felicia is feeling horrible - she is under influence of something; she’s hexed.

While traveling, Jason noticed, that they are being followed by a Shark. He split from the group and lured a Shark from the rest. While trying to lose the Shark, he noticed Tom – and noticed, that Tom is being tailed too. So Jason made the most logical thing – he sic a shark onto Tom’s tail and vice versa.

Tom, unaware of all that, got intercepted by Jason. Jason convinced Tom to follow him before cops come; they went to the safe house where Felicia was already waiting.

The discussion between Tom and Felicia was both fruitful and sad. (XVXVXXV). Firstly, Tom ordered Felicia to take off the gloves – she has stolen from him. She got all defensive and jumpy, cheerfully, she explained that she really was trying to save him. Helen said that Felicia shouldn’t really mind passing gloves to Nesta for analysis. It took Felicia all of her willpower but she managed to take the gloves off, breaking the physical connection with the mimic

Nesta and Jason tries to convince Felicia that she can drop the gloves, that she doesn’t need them. They tried to explain to Felicia that if she drops the gloves, if she revokes them, then she will be able to help, because an enemy will never see it coming. They played Felicia’s paranoia to help both Felicia and Tom. Felicia did manage to cast the gloves away, therefore putting mimic in a psychic shock and severing the connection.

But that made Felicia’s hysteric. Jason tried to conform her but he just got punched in the nose. This managed to break Felicia’s unnatural state, she apologized and lost consciousness. Nesta burned the gloves before mimic recovered.

So, Tom will have some problems because if he doesn’t punish Felicia then the other faction in his gang will think he is soft. But he doesn’t care as much. And an enemy thinks that they have won - unaware that the team is slowly closing on them.

## Streszczenie

Felicja - lojalna przynętka Dużego Toma - okradła owego Toma. Zespół się z nią spotkał i odkrył, że Felicja jest pod wpływem mimika symbiotycznego. Z pomocą Dużego Toma dali radę Felicję przekonać, żeby odrzuciła mimika. Niestety, pozycja Toma została nadwątlona - oraz nadal nie wiadomo kto wysłał mu mimika. Podwiert przygotowuje się na starcie gangów - Latających Rekinów (wsparcie Aurum), TomStone (lokalni) i Technożerców (militaryści i antypostępowcy).

## Progresja

* .

### Frakcji

* .

## Zasługi

* Jan Łowicz: przekrada się po uliczkach Podwiertu i ma własną stalkerkę. Skutecznie wykrył ogon swój i Dużego Toma.
* Natasza Aniel: głos łączący Felicję z Tomem, osoba negocjująca i dbająca o swój sklepik okultystyczny. Doszła do tego, że mają do czynienia z mimikiem symbiotycznym.
* Kinga Kruk: zza kulis przygotowuje różnego rodzaju przedmioty którymi odwraca uwagę lub przekupuje niewłaściwe osoby.
* Felicja Melitniek: poczuła mimika symbiotycznego, ukradła go z przesyłki do Toma, ale lojalność zwyciężyła - odrzuciła mimika symbiotycznego, acz kosztem zdrowia.
* Duży Tom: szef lokalnego gangu Podwierckiego, który wybrał lojalność Felicji nad utrzymanie twardej kontroli nad swoim gangiem. Ktoś próbuje go zdjąć.
* Katja Nowik: powiązana z Aurum, kiedyś kochanka Dużego Toma. Teraz ma powiązania z Latającymi Rekinami.
* Melinda Teilert: młoda bogaczka z Aurum (nie czarodziejka), która chce dołączyć do Janka i być groźnym łowcą potworów. Lub ludzi. Nieważne - chce być groźna

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Osiedle Leszczynowe
                                    1. Sklep z reliktami Fantasmagoria: niewielki sklepik prowadzony przez ludzi; pomagała tam Felicja z ramienia Dużego Toma.
                                1. Magazyny sprzętu ciężkiego: miejsce, gdzie Skażona mimikiem symbiotycznym Felicja spotkała się z Zespołem; tam działają Latające Rekiny.
                                1. Kompleks Korporacyjny
                                    1. Chemiczna firma Kurara: należąca do Dużego Toma firma zajmująca się chemią, alkoholem czy psychotropami.

## Czas

* Opóźnienie: -50
* Dni: 3
