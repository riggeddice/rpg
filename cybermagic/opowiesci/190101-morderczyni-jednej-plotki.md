## Metadane

* title: "Morderczyni-jednej-plotki"
* threads: brak
* motives: plotki-niszcza-reputacje, publiczny-pojedynek, dont-mess-with-my-people
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [181230 - Uwięzienie Saitaera](181230-uwiezienie-saitaera)

### Chronologiczna

* [181230 - Uwięzienie Saitaera](181230-uwiezienie-saitaera)

## Projektowanie sesji

### Struktura sesji: Eksperymentalna eksploracja / generacja

* **Scena, Konsekwencja, Niestabilność, Opozycja, Trigger, Pytania, Pytanie sesji**
    * Otwarcie salonu Pięknotki i zmiana samej terminuski doprowadziła do redukcji popularności salonu Adeli
    * Adela jest zmuszona do innych, mniej bezpiecznych metod by utrzymać swój biznes
    * Adela walczy o utrzymanie klientów mimo dekoniunktury wywołanej istnieniem Pięknotki
    * Działania pasywne Pięknotki
    * Pytania
        * Czy Adela zachowa wystarczającą ilość klientów by przetrwać?
        * Czy Adela zwróci się w kierunku mafii?
        * Czy Adela zaatakuje Pięknotkę bezpośrednio
    * Czy biznes Adeli przetrwa?

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. ...

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (21:25)

_Pustogor_

Pięknotka ma swój gabinet, Afrodytę. Ma zdecydowanie więcej klientów niż przywykła. Nie martwi jej to w zupełności. Pięknotki celem nie jest zniszczenie gabinetu Adeli, ale jest jej celem zdecydowanie zwiększyć potęgę jej gabinetu i zadowolenie klientów. Po pierwsze, Pięknotka działa więcej osobiście - po sprawie z Saitaerem to jej dobrze zrobi a i Karla będzie zadowolona. Erwin też pomaga Pięknotce, choć bardziej na zasadzie "być i pomagać" a nie "być przydatnym".

Wieczorem, gdy Pięknotka szła do swojego mieszkania, spotkał ją nieznany jej mag. Zakapturzony, w płaszczu, zaczął pytać Pięknotkę, czy jej gabinet zawiera "mroczne drżenie i sekrety Cieniaszczytu". Terminuska zgłupiała - sięgnęła po niego do hipernecie by dowiedzieć się kto to do cholery jest. (Tp:7,3,4=SS). "Gościu" się aż "szarpnął" - Pięknotka zrobiła brutalny skan. Wykryła kto to. To młoda, lekko wyegzaltowana panienka ze szkoły magów, która naczytała się "Siedmiu Twarzy Mroku". Gorzej, NIE JEST UCZENNICĄ. To nauczycielka...

Teresa Mieralit, nauczycielka w szkole magów. Pięknotka podchodzi do kuszenia - niech Teresa nie ucieka, niech zobaczy sytuację. Pięknotka uderzyła w tony "Cieniaszczyt, miasto ekskluzywnego luksusu". Pięknotka zdecydowała się dojść do tego czemu Teresa tu jest. Zaczęła opowiadać Teresie historie o Cieniaszczycie. I chce dowiedzieć się wszystkiego - o co tutaj chodzi. (Tr: 7,3,6=P,P,SS). Pięknotka dowiedziała się wszystkiego.

* Teresa jest powiązana z Dare Shiver oraz z Czerwonymi Myszami
* Teresa chce się dowiedzieć, jak bardzo epickim, nietypowym miejscem jest gabinet Pięknotki - czy jest to "odłamek Cieniaszczytu w Pustogorze"
* Pięknotka widzi, że na coś takiego jest niesamowity popyt. Zwłaszcza wśród Miasteczkowców.
* Teresa jest nauczycielką i rekruterką w szkole dla magów.
* Teresa ORAZ Czerwone Myszy uznały, że gabinet Pięknotki to jest **THE** place to be. A Pięknotka powinna dołączyć do Czerwonych Myszy.
* Alan Bartozol dał zdecydowane weto i został przegłosowany. A Pięknotka nie ma nic do gadania. Co więcej, to doprowadziło do odepchnięcia z Myszy Adeli Kirys.

Wow, dużo tego... Pięknotka stała się popularna.

Oki. Myszy mogą chcieć Pięknotkę, ale Pięknotka chce swobody i rozbudowania swojego gabinetu. To jest jej biznes. A skoro jest na to popyt... Pięknotka idzie w tamtą stronę. Dodatkowo, to ALAN powiedział o Cieniaszczycie - Pięknotka może chcieć mu podziękować za nową popularność ;-).

Teresa jest już przekonana, że Zna Prawdę. Przydała się w Myszach i miała swój aspekt Dare Shiver.

Pięknotka znalazła sprytny plan - stworzyła tonę niedomówień, by zaapelować do jej natury Dare Shiver. "Can you take it"? Niech Teresa sama się przekona, jak bardzo Mroczne Drżenie Cieniaszczytu się pojawi w jej gabinecie. Nikt w tym mieście (Pustogorze) się nie podpiął do podejścia Cieniaszczytu, a Pięknotce ONO PASUJE. Plus, to bezpieczniejsze dla samego Pustogoru. Celem Pięknotki paradoksalnie NIE JEST uruchomienie Teresy. Jej celem jest SCHŁODZENIE Teresy i przekierowanie jej. Skanalizować jej entuzjazm. (Tp:S). Teresa nie stanowi problemu dla Pięknotki. Przynajmniej coś. A Pięknotka ma darmową reklamę.

Koniec sceny. Pytania:

* Czy Myszy są absolutnie przekonane, że Pięknotka robi coś megaepickiego i trzeba w to wejść? (5,3,5) -> (8,3,5)
* Czy Adela jest przekonana, że przez Pięknotkę wszystkie jej plany i nadzieje poszły w ruinę? (5,3,5) -> (6,3,5)
* Czy ogólna opinia o gabinecie Pięknotki przesunie się w "miejsce zepsucia Cieniaszczytem" całkowicie niezależnie od Pięknotki (6,3,7)

Wynik:

* Myszy: TAK (S,S). Myszy są przekonane, że Pięknotka robi The Next Thing. Może zakłócić operację rekrutacji a nawet założyć własną frakcję - zbiera przecież osoby takie jak Erwin czy Adam Szarjan, a MOŻE NAWET ATENA. Tak więc dla bezpieczeństwa, ktoś musi być "cieniem" Pięknotki i Pięknotka ma dołączyć do Myszy. Padło na razie na Teresę.
* Adela: TAK (S,S). Adeli zależało, by mieć jako klientów Myszy, wycelowała w to jako w niszę. I teraz zostało to rozerwane przez Pięknotkę. Zdaniem Adeli, Pięknotka robi to 100% specjalnie. Adela jest jeszcze bardziej przekonana, że musi się Pięknotce postawić.
* Opinia: TAK (S,S). WPŁYW (Remis). W związku z tym m.in. Adela rozpuściła odpowiednie plotki i działania - gabinet i idea planów Pięknotki zostały nazwane wpływem Cieniaszczytu. Niektórzy terminusi, mniej przyjaźni Pięknotce, to podchwycili. Opinia się rozchodzi. Pięknotka może to jeszcze całkowicie zatrzymać.

Wpływ:

* Ż: 4
* K: 0 (1)

**Scena**: (22:18)

_Pustogor_

Pustogor to dosyć nudne miasto. Mnóstwo osób, zwłaszcza Pustogorskich dam jest spragnione czegokolwiek, np. egzotyki (najlepszym dowodem Dare Shiver oraz Teresa). Tak więc Pięknotka zaczęła oferować coś nowego, niewinnie - "umaluj się według mody różnych miejsc". Wyglądaj jak ktoś skąd indziej. Pięknotka współpracuje ze znajomym krawcem, Aleksandrem Iczakiem. W ten sposób można mieć odpowiednie stroje, ale dostosowane lokalnie. Pięknotka zamówiła u niego kilka strojów i dziewczyny w jej gabinecie zaczęły obsługiwać tak ubrane. (+1 hype).

Do Pięknotki przyszła jedna z jej pracownic, Gabriela Moriasz. Poprosiła o urlop - jej chłopak jest bardzo przeciwny pracy Gabrieli w miejscu rozpusty i zła. JAKIEJ ROZPUSTY?! Pięknotka usłyszała o plotkach i nie ucieszyła się z tego powodu. Powiedziała Gabrieli, by ta zaprosiła chłopaka. Niech sam się przekona.

Chłopak przyszedł szukać ROZPUSTY I ZŁA - ale ich nie ma. Pięknotka mu się przez pewien czas nie pokazuje, by miał swobodę szukania. Po czym, gdy miał chwilę, (-5W), do salonu weszło dwóch typów z Miasteczka, łowców nagród. Zaczęli pytać o emulatory kralothów; dziewczyny coraz bardziej czerwone. Gdy Pięknotka wyszła z nimi porozmawiać, by kontrolować sytuację - tych dwóch zaproponowało jej kupno pudełka z magią erotyczną ze Szczeliny. Pięknotka chłodno odmówiła. Nie jest zainteresowana.

Pięknotka zorientowała się, że przez te plotki i nie tylko tego typu wizyty będzie miała częściej. Pięknotka poprosiła o informację kto rozpuszcza takie plotki. Oni nie chcą jej powiedzieć. Ok. Niech Erwin się dowie...

Aha, ale po tym spotkaniu chłopak Gabrieli jest lekko wstrząśnięty tym wszystkim. Pięknotka dała Gabrieli urlop, ale po raz pierwszy miała problemy z przekonaniem człowieka do czegoś. Jej gabinet jest zagrożony... Pięknotka skupia się na tym, by organicznie przeciwdziałać plotkom - niech jej pracownice i znajomi przeciwdziałają (+4S) (Tr: SSZ). Pięknotce się to uda - ale będzie musiała skonfrontować się i z Adelą i pokonać widowiskowo kogoś w walce. Ktoś musi zobaczyc konsekwencje.

Po godzinie Pięknotka dostała wiadomość od Erwina. Karol Szurnak. Łowca nagród z Miasteczka. Pięknotka nie ma z nim żadnych układów ani niczego.

_Pustogor, Miasteczko, Górska Szalupa_

Olaf ciepło powitał Pięknotkę. Pokazał jej jej własne zdjęcia i zauważył, że Pięknotce ktoś próbuje dodać trzeci aspekt - terminuska, kosmetyczka i... burdelmama? Pięknotka powiedziała, że nie jest aż taką kliszą. Olaf potwierdził. Jemu też się to nie podoba. Cóż, Pięknotka jest "higher life gal", nie przychodzi tu często - a przynajmniej, nie jej znajomi poza Erwinem.

Pięknotka powiedziała, że chce sklupać takiego jednego dupka. I chce zrobić show. Olaf zaproponował Salę Pojedynkową z włączeniem zasad "koniec bitwy przy śmierci". Pięknotka powiedziała, że z radością przyjmie. Olaf z radością to rozreklamuje jako pojedynek zemsty. Pięknotka doceniła. Poprosiła Olafa o kiepski kufel (co się rozpadnie jak rozbije komuś na głowie) i żeby ten napełnił go czymś nędznym i cuchnącym. Olaf nie chciał się zgodzić przez wzgląd na innych gości, Pięknotka zmieniła zdanie.

Pięknotka zrobiła krótki hipernetowy research na temat Karola. Pięknotka chce, by to się PONIOSŁO, by Karol został upokorzony i zdyskredytowany. Pięknotka chce, by nikt tu nie odważył się już opowiadać na jej temat takich plotek. Show must go on. Wykorzystała dane terminusów i poszła na pełną artylerię - chce, by świat zamarzł. Nikt nie odważy się nigdy stawić przeciwko niej. (Tr:10,3,5=SZ). Zasobowy Sukces. Znalazła coś super - gdy trochę popił kiedyś to dobierał się do nastolatek, poniżej 16 roku życia. Był nawet skłonny użyć magii. To było jakieś 10 lat temu, potem już tak nie robił, ale jest to coś co go zniszczy w oczach wielu ludzi.

Pięknotka podeszła do niego w Szalupie i głośno ogłosiła jego przeszłe czyny. Powiedziała, że on próbuje z niej zrobić burdelmamę i zniszczyć jej reputację, a przecież to że jest z rodu Diakonów nie oznacza, że dostarczy mu nastolatki. I wyjaśniła co miała na myśli. I wyzwała go na oficjalny pojedynek. Gość nie wytrzymał (ze strachu) i i tak próbował Pięknotkę uderzyć - (Tp:SS). Wkurzyła go niemożebnie; niestety, nie dała rady uniknąć wszystkich ciosów. Wzięła więc przykład z Walerii - zadała mu upiornie bolesne obrażenia.

Pięknotka wywarła aurę strachu. To, co Waleria zrobiła w Cieniaszczycie było straszne tam - ale w Pustogorze to jest więcej niż straszne. Zwłaszcza, że o Pięknotce nigdy nikt tak nie myślał. She is the cute one ;-). Olaf ich rozdzielił (nie trzeba było, ale bał się o Karola). Pięknotka z uśmiechem opuściła Szalupę.

_Pustogor, Gabinet Adeli_

Pięknotka poszła do Adeli bezpośrednio. Powiedziała, że przyszła bezpośrednio. Opisała to, co zrobiła w karczmie. Te plotki nie są tym, co ona kiedykolwiek robiła czy kogokolwiek nauczyła. Czyste, bezwzględne zastraszanie - jeśli Adela odważy się działać plotkami przeciwko Pięknotce, stanie się coś strasznego. (Tp:9,3,3=SS). Sukces. Adela się BOI tej nowej Pięnotki. Nie odważy się stawić jej czoła czy działać przeciwko jej biznesowi. Nie tak. To już nie jest kwestia rywalizacji, Adela zrozumiała, że to nie ta sama liga.

Adela przeprosiła i powiedziała że musi wrócić do klientów. Pięknotka ZOBACZYŁA, że Adela zrozumiała. Uśmiechnęła się zimno i wyszła.

_Pustogor, Barbakan_

Pięknotka zbiera wszystkie informacje na temat Karola i jego stylu walki. Trenuje walkę przeciwko temu stylowi (i nie tylko). Pięknotka WYGRA tą bitwę i to tak, by nikt nie odważył się więcej stanąć na drodze jej biznesu. Nie w ten sposób. Więc ćwiczy z Tymonem i innymi terminusami.

I Pięknotka znalazła sposób jak z nim walczyć. Walka wręcz bez power suitów. Strój Pięknotki do walki - minimalny. Ma wyglądać groźnie. Seksownie i groźnie. Jemu ma to pomieszać, a widownia ma zobaczyć tygrysicę. Pięknotka pomyślała o Saitaerze. Byłby z niej dumny.

_Pustogor, Miasteczko, Górska Szalupa, dwa dni później_

Sala Pojedynków. Widownia jest pod wrażeniem. Karol zaczyna od boastowania i dissowania Pięknotki. Pięknotce tylko w to graj. Obraziła jego męskość. Uderzyła w niego całą złośliwością i jadowitością, chcąc go wyprowadzić z równowagi i zmieszać. (Tp+1: 10,3,3=SS). Skonfliktowanie polega na tym, że Pięknotka poszła za daleko - uderzyła w niego tak jak by zrobiła to Waleria, nie Pięknotka. Ludzie widzą w Pięknotce terminuskę, i to ranną terminuskę a nie kosmetyczkę.

Wszedł z Pięknotką w walkę w zwarciu. Nie jest to siła Pięknotki, ale nie jest to też siła Karola. Pięknotka chce go zniszczyć - więc zaakceptowała niekorzystną dla siebie walkę. Teraz wzięła przykład z Moktara. (Tp:8,3,4=S). Pięknotka NIE poszła w stylu Moktara (kontrolowane łamanie części ciała). Wpierw złamała coś małego (palec). Jeśli walczy dalej, łamie kolejną część (łokieć). Jeśli walczy dalej...

Karol poddał się po trzecim złamaniu. Zaczął płakać. Tyle wystarczyło. I teraz Pięknotka zmusiła go do publicznego odszczekania wszystkiego co powiedział na jej temat. (Tp:9,3,3=P). Pięknotka zmusiła płaczącego i błagającego maga do odszczekania, ale z perspektywy innych magów wyglądało to tak, jak Pięknotka patrząca na Walerię czy Moktara w pierwszym tygodniu pobytu w Cieniaszczycie...

Pytania:

* Czy Pięknotka wygenerowała ogromny hype na swój gabinet? (7,3,4 = SS)
* Czy Pięknotce udało się zdusić plotki (9,3,4 = autosukces)
* Czy Pięknotka sprawiła, że Myszy są jeszcze bardziej nią zainteresowane? (8,3,4 = S)
* Czy Pięknotka zdobyła reputację "córki Saitaera"? (7,3,5=SS)

Wyniki:

* Gabinet (SS): Tak, zdecydowanie tak. Acz niekoniecznie tylko taki jak chciała. Wiele osób typu Moktara czy Walerii też się zainteresowało gabinetem. Pięknotka została też swego rodzaju fetish fuel dla niektórego typu masochistów.
* Plotki: (S): Nikt się nie odważy.
* Zainteresowanie Myszy (R): Tak, bardzo są zainteresowani. Ale nie chcą jej podpaść, boją się, co może się stać po tym co było teraz.
* Reputacja: (SS): Pięknotka ma reputację straszliwej, ale tylko jako przeciwnika. Do. Not. Cross. Her.

Wpływ:

* Ż: 7 (12)
* K: 4 (6)

**Epilog**

* Plotki na temat gabinetu Pięknotki się skończyły.
* Karla wzięła Pięknotkę na dywanik. Pięknotka nie przepraszała.
* Moktar wysłał Pięknotce wyrazy zadowolenia. Psy są zadowolone z jej ewolucji.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |    4    |     12      |
| Kić           |    7    |     9       |

Czyli:

* (K): 
* (Ż): (3) Karol jest wrogiem Pięknotki. Będzie chciał się zemścić. Ale nie wie jak.

## Streszczenie

Na temat Pięknotki i jej salonu zaczęto rozpuszczać nieprzyjemne plotki. Pięknotka zlokalizowała jedno ze źródeł i je pokazowo zniszczyła, zmuszając maga do przepraszania i płaczu na kolanach. Dodatkowo, Czerwone Myszy oraz Dare Shiver zaczęli interesować się Pięknotką i jej salonem. A sama Pięknotka przecięła "największą nemesis" Adeli Kirys.

## Progresja

* Adela Kirys: jest przerażona Pięknotką. Boi się stanąć przeciwko niej. Nie zrobi już tego. Widzi, że to nie ta liga. Przestała mówić o "największej nemesis".
* Pięknotka Diakon: w Pustogorze ma reputację osoby której nie wolno stanąć na drodze, bo go zniszczy. Zrobi krzywdę na kilku poziomach.
* Karol Szurnak: upokorzony przez Pięknotkę; wyszło, że lubił nastolatki oraz musiał na kolanach płacząc przepraszać i odszczekiwać plotki. Wróg Pięknotki.

## Zasługi

* Pięknotka Diakon: gdy uderzono w reputację jej salonu i w jej pracowników, zrobiła kontratak, pokazówkę, i zniszczyła wszelkie plotki na swój temat. Strachem.
* Teresa Mieralit: nauczycielka w AMZ. Bardzo (zbyt) zainteresowana Mrocznymi Drżeniami Cieniaszczytu, cokolwiek to jest. Agentka Czerwonych Myszy i agentka Dare Shiver. Zafascynowana Cieniaszczytem i Cieniaszczycką kulturą.
* Aleksander Iczak: krawiec współpracujący z Pięknotką w sprawie egzotycznych strojów z różnych miejsc. Pomaga jej w dodaniu kolorytu do strojów pań w Pustogorze.
* Karol Szurnak: łowca nagród z Pustogoru, który rozpuszczał plotki o Pięknotce i jej "brudnym gabinecie". Został upokorzony a jego reputacja zniszczona przez Pięknotkę.
* Olaf Zuchwały: właściciel Góskiej Szalupy. Ma topór. Stylizuje się na kuriozum godne Pustogoru. Fajny facet. Pomógł Pięknotce znaleźć Szurnaka i zrobić pojedynek.
* Erwin Galilien: znalazł dla Pięknotki informacje o Karolu Szurnaku, który rozpuszczał plotki.
* Alan Bartozol: zawetował akces Pięknotki do Czerwonych Myszy i został przegłosowany; jest wściekły na Czerwone Myszy, bo wybierają Pięknotkę nad niego

### Frakcji

* Dare Shiver: Teresa Mieralit jest agentką Dare Shiver w Pustogorze; to poszukiwacze emocji
* Czerwone Myszy Pustogorskie: Teresa Mieralit jest agentką Czerwonych Myszy w szkole magów w Zaczęstwie. Służy jako nauczycielka i rekruterka.
* Czerwone Myszy Pustogorskie: chcą zdobyć Pięknotkę Diakon, z uwagi na jej nową popularność i fajność - nawet wbrew Alanowi Bartozolowi.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Gabinet Pięknotki: miejsce, dookoła którego złożyła się cała historia plotek. Pięknotka dodała tam salon masażu (nie erotycznego).
                                1. Knajpa Górska Szalupa: dość rozległa gospoda w Pustogorze. Główna w Miasteczku. Zawiera dobrej klasy Salę Pojedynkową do rozwiązywania waśni.

## Czas

* Opóźnienie: 4
* Dni: 4

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* nic

## Wykorzystana mechanika

1811
