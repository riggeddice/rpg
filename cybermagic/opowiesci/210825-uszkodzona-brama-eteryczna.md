## Metadane

* title: "Uszkodzona Brama Eteryczna"
* threads: legenda-arianny, niestabilna-brama
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [210616 - Nieudana infiltracja Inferni](210616-nieudana-infiltracja-inferni)

### Chronologiczna

* [210616 - Nieudana infiltracja Inferni](210616-nieudana-infiltracja-inferni)

### Plan sesji
#### Co się wydarzyło

.

#### Sukces graczy

* 

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Trzy dni po tym, jak Flawia stała się częścią załogi. Ogólnie, najczęściej pomaga Martynowi.

Infernia, w pełni w nowym opancerzeniu i w tarczach antymagicznych. Infernia wyrusza w serce Anomalii Kolapsu... tyle, że następnego dnia w kosmosie przechwycił ją koloidowy statek. Niobe, statek sił specjalnych Orbitera.

Medea nawiązuje kontakt. Powiedziała, że jest sytuacja skomplikowana - zniknął statek naprawiający Bramę Eteryczną, nazywa się Mfumo. Okazało się, że Infernia nie był jedynym statkiem, który zaginął w tamtej Bramie - ale Infernia była pierwszym, który wrócił... i Medea chce, by Infernia to sprawdziła (bo ma ekranowanie). Medea zajmie się przykrywką.

Medea ostrzega Ariannę - jeśli coś jest dziwnego z tą Bramą, koniecznie niech się skonsultuje z Medeą. Teoretycznie da się zniszczyć Bramę - i to może zdestabilizować gwiazdę. Tego nie chcemy.

Medea: Brama może mieć... pasożyty. Czym dokładnie pasożyty są - Medea nie wie. Skąd się wzięły? Classified. Wyjątkowo nie była to wina Inferni - to, że Infernia się zgubiła było WYNIKIEM pasożytów a nie ich przyczyną.

Zadaniem Inferni według Medei jest:

* jeśli coś wyleciało z Bramy, pew pew.
* jeśli są pasożyty w Bramie, nie dotykać.
* jeśli Mfumo jest aktywnym statkiem, użyć mechanizmów Mfumo by obniżyć niestabilność Bramy
* zorientować się co do cholery się dzieje
* NIE niszczyć pasożytów w Bramie.
* uratować kogo się da, ale nie kosztem pasożytów z Bramy lub większej destabilizacji
* nie stabilizować Bramy innymi metodami niż Mfumo.

Arianna poszła w pełną paladynkę. Ratujemy wszystkich. Żeby sprowokować Medeę do gadania. TrZ+3:

* X: Medea przekazała na pokład komisarza. Gilbert Bloch. Jest tu by doradzać, oczywiście.
* V: Medea powiedziała problem - jeśli Brama się w pełni ustabilizuje, wypuści to, co jest w środku "zamknięte" i nigdy nie dotarło.
* Vz: Wojna noktiańska. Noktianie przegrali, bo nie wszystkie ich jednostki doleciały. M.in. nie doleciał ich superpancernik. Spatium Gelida. W związku z tym dbają o populację pasożytów w Bramie. Bo tak duża jednostka wymaga odpowiednio stabilnej Bramy.

Arianna -> Flawia, niech Flawia wydobędzie informacje z Blocha. Flawia ostro protestuje, bo nie może działać przeciw Orbiterowi. Orbiter przecież nie ma intryg. Arianna zauważyła, że przecież była akcja z tymi dziećmi i Eternią. To zadziałało na Flawię. Bo to, co Flawię dotknęło - nie widzi jako intrygę Orbitera. Dla niej - personalna rana zadana przez Elenę Verlen i Aurum.

ExZ+3:

* V: Flawia wbrew sobie ale to zrobi. Jest wstrząśnięta tym co Arianna powiedziała, ale to są fakty którym nie zaprzeczy.
* XX: Flawia zaraportuje, że to zrobiła. Że wyciąga informacje od Blocha. Ale nie powie, że Arianna jej kazała.

Koniecznie - KLAUDIA MUSI PRZECHWYCIĆ TEN RAPORT.

Flawia jest na potem. Gdyby trzeba było Blocha do czegoś przekonać.

Docieracie do Bramy. I jest coś cholernie nie tak. Sensory Inferni pokazują wysoki poziom energii. Brama jest niestabilna. Coś jest nie tak.

W kierunku na Infernię lecą 2-3 efemerydy. Jest z nimi coś nie tak. Zachowują się jak jednostki wojskowe. Lecą w kierunku Inferni w szyku noktiańskim. Te jednostki to "trójkąt" noktiański - dwie jednostki na front, jedna z tyłu. Przednie to "tanki", tylny to "dps".

Gdy "wygraliśmy" wojnę z Noctis, siły specjalne zrobiły to co zwykle - odłowiły co lepszych / bardziej doświadczonych dowódców noktiańskich i "pozyskały" informacje na temat ich taktyki i działań, uzbrojeń. Czyli Eustachy ma sporo wiedzy.

Propozycja - Infernia --> kosmiczny złom, wyłączamy silniki, MY TEŻ JESTEŚMY ZŁOM LOL. +obserwacja przeciwników. Plus - Eustachy włącza pełne ekranowanie Inferni. Po raz pierwszy ostry test bojowy.

TrZ+3:

* X: Infernia w trybie ukrytym ŻRE ENERGIĘ JAK CHOLERA. Nie wytrzyma długo.
* Vz: Infernia jest w stanie się skutecznie ukryć przed efemerydami.
* VVz: Infernia gdy jest ekranowana to jest niewidoczna przed wszystkimi bytami taumicznymi.

Infernia zlokalizowała Mfumo. Jest blisko Bramy i energia przeskakuje między tymi dwoma bytami. Jest trochę jak drzazga. Coś poszło nie tak - część struktury Bramy jest uszkodzona. Jeden z węzłów wspierających Bramy jest uszkodzony.

I są tu wraki jeszcze innych jednostek. Żadna Orbitera. Jedna większa. Największa to będzie spora fregata, nie kodowana jako Orbiterowa.

Eustachy zaproponował, by ukryć Infernię jako statek noktiański. Z perspektywy Arianny to jej problem. Trzeba odpowiednio wzmocnić aurę noktian żeby Infernia "czuła się" noktiańsko i obniżyć aurę Orbitera. Wtedy efemerydy potraktują Infernię jako "swoją".

Arianna znowu idzie w Alivię Nocturnę. W końcu tą znają i tą już byli.

TrMZ+3+5O:

* O: Echo Alivii Nocturny. Infernia ma echo Krypty. Częściowo jest Kryptą, częściowo Infernią. (+3V)
* V: Infernia jest wykrywana jako Alivia Nocturna przez efemerydy i Bramę. Brama "czuje" Noctis.
* O: Infernia ma link ze statkami uwięzionymi w Bramie. I Krypta też. A one Dotknęły Infernię.
* V: Część efemerycznych jednostek jest głodna i poluje na Infernię. Nie są "noktiańsko-świadome". Ale inne efemerydy wiedząc że Alivia jest statkiem medycznym będą jej chronić.

Eustachy zmniejsza moc ekranowania i lecą --> Mfumo. Jeśli jakaś jednostka się zainteresuje Infernią, włączy się ekranowanie. Lawirowanie, by nie obciążać generatorów Inferni i poruszać się bezpiecznie. Kontrola ekranu ochronnego.

TrZ+3+3O:

* X: Jedna efemeryczna "głodna" jednostka zainteresowała się Infernią (ale zgubiona) więc Mfumo.
* V: Udało się tym planem bezpiecznie dolecieć do Mfumo.
* OX: "Diana" przytuliła się do Eustachego i wystrzeliła z dział. Zniszczyła tamtą efemerydę. Pojawiły się 2-3 kolejne zainteresowane. A "Diana" cała szczęśliwa. "Strzelamy? :-)"
* V: Eustachy ściąga INNE efemeryczne jednostki do obrony "Alivii".
* X: Elena przychodzi ratować Eustachego przed Dianą.

Elena ATAKUJE Dianę, zanim ta powie coś więcej niż "ja całowałam Eustachego a Ty tylko chciałaś". Bo to niebezpieczna linia jest.

TrM+3+5O:

* V: Elena wygnała Dianę. "Czy to nie jest piękne, że dziewczyny o Ciebie walczą? Pomogę Ci sprawić, by była Twoja..."

Elena wycofała się czerwona jak piwonia po poinformowaniu Eustachego, że przyszła go ratować. I tylko dlatego.

Wreszcie - Infernia i Mfumo. Bloch wie, co jest nie tak i co się głównie zepsuło - jeden z węzłów Bramy jest bardzo mocno uszkodzony. Coś się z nim... zderzyło? Jakby jakaś jednostka walnęła w ten węzeł.

Klaudia analizując jednostki dookoła próbuje dojść do tego co tu się spieprzyło. Co walnęło w ten węzeł? Wrzuca dane w kompa, niech Persefona na to spojrzy. A Bloch niech uzupełnia info na bieżąco jak jest w stanie.

TrZ+3:

* V: Klaudia ma odtworzony stan - jedna z tych jednostek zniszonych, z tych wraków musiała walnąć w węzeł i uszkodzić Bramę. Klaudia wysyła dronę by dowiedzieć się więcej o tej jednostce.
* V: Ta jednostka walnęła w Bramę bo była ostrzelana.
* XV: Są jednostki które mają ludzi. Da się ich uratować. Jest ultra mało czasu.

Arianna chce uratować wszystkich. Nie ma jak. Więc - channelluje Kryptę. MACROSS STYLE! Arianna == Lynn Minmei. A dokładniej... KRYPTA jako Lynn Minmei inkarnując Ariannę - wysyłając rozkazy i imperatywy do jednostek noktiańskich.

ExMZ+4+5O:

* Vz: Efemerydy zaczęły pełnić rolę jednostek pomocniczych Krypty. Efemerydy ratują "normalsów" i zwożą ich na Infernię.
* VV: Efemerydy z żądania Krypty wyciągnęły ludzi z Bramy i z Mfumo i wszędzie wszystkich co dało się uratować a Bloch ma szczękę w okolicach podłogi.
* O: Krypta zaczyna się materializować. Pojawiła się w potrzebie...
* V: Arianna wysyła na Kryptę impuls emocjonalny. Infernia nie chce być Kryptą. To kupuje czas. Krypta przetwarza efemerydy, Bramę, wszystko - a to nie pomogło.

EUSTACHY! Wszystko, co Infernia ma w silnikach. Infernia była gotowa na działania w Anomalii Kolapsu. Więc faktycznie ma ekstra zapasy paliwa. PALIMY PALIWO! Żyłujemy silniki na zasadzie "nitro". Tyle paliwa, że część niespalonego paliwa --> wraz z ciągiem poza statek. Ekstra wybuchy poza statkiem. A z tyłu głowy - wiem że będzie w serwisie :3. Katujemy silnik :3.

ExZ+4 -> TrZ+4 (ale gwarantowane uszkodzenie silnika):

* X: Silniki w absolutnej ruinie. Infernia ma prędkość do 20%. I jest zwrotna jak Tucznik.
* X: Detonacja innych jednostek dla odrzutu które tu są jeszcze bardziej uszkodziły Bramę.
* V: Infernia na nitro oddaliła się od Krypty. Infernia ma czerwone alarmy w silnikach.

Niobe przechwytuje Infernię.

Medea dostaje raport od Blocha, jest zaskoczona sukcesem. Ściągnęła już wcześniej jednostki medyczne i inne. Infernia do doków? Nope. Jak tylko Krypta zniknie, Infernia wraca na front - trzeba naprawić cholerną Bramę.

## Streszczenie

Infernia została przechwycona przez Medeę lecąc na Anomalię Kolapsu i przekierowana na uszkodzoną Bramę Eteryczną (gdzie, jak się okazuje, są pasożyty i flota noktiańska która nie doleciała). Na miejscu Infernia uniknęła efemeryd i channelując Kryptę uratowała wszystkich efemerydami. Gdy Krypta pojawiła się na serio, Infernia zwiała paląc silniki. Statki noktiańskie w Bramie mają tether na Infernię.

## Progresja

* OO Infernia: perfekcyjnie zamaskowana przed bytami taumicznymi, acz to kosztuje strasznie dużo reaktora jak maskowanie jest włączone. Straciła bycie Q-Ship póki to ma.
* OO Infernia: silniki mają 20% mocy (uszkodzenie przez Eustachego). Manewrowność Tucznika... i nie ma szans na drydock...
* ON Spatium Gelida: ma tether na Infernię po stronie Sektora Astoriańskiego. Ma skonfundowany sygnał - Infernia? Alivia Nocturna? Ale ma i się obudził.

### Frakcji

* .

## Zasługi

* Arianna Verlen: abstrahując od przekonania Medei do wyznania że w Bramie zamknięty jest Spatium Gelida, channelowała Nocną Kryptę w zdestabilizowaną Bramę. Dzięki temu uratowała efemerydami WSZYSTKICH, nawet tych, którzy częściowo byli już w Bramie.
* Eustachy Korkoran: świadek tego jak "Diana" i Elena o niego walczyły; autor pomysłu, by Infernię zamaskować jako noktiański statek by efemerydy Infernię wspierały. Po raz pierwszy UNIKAŁ walki i modulował sygnaturę ekranowania tak, by efemerydy nie zbliżały się do Inferni.
* Klaudia Stryk: używając systemów skanujących Inferni i dron odkryła których ludzi z wraków da się jeszcze uratować - na przestrzeni wszystkiego dookoła Bramy.
* Medea Sowińska: nie mając nikogo kto może pomóc w naprawieniu Eterycznej Bramy zrekrutowała Infernię (bo ekranowanie). Musiała wyznać prawdę o pasożytach Bramy i przyczynach tego, że Infernię "porwało" przez Bramę całkiem niedawno.
* Elena Verlen: widząc "Dianę" "napastującą" Eustachego, zaatakowała ją by chronić Eustachego. OCZYWIŚCIE tylko dlatego i wcale się nie przejmowała czy coś Eustachemu nie jest lub nie robi.
* Diana d'Infernia: powołana przez efekt Paradoksu Eustachego, nacisnęła za niego spust (którego nie chciał naciskać) i spowodowała kłopoty z efemerydami. Rozwiana przez Elenę. Obiecała Eustachemu, że Elena i Eustachy będą razem.
* Flawia Blakenbauer: dostała polecenie od Arianny by oczarować Blocha. Odmówiła, bo Orbiter nie ma intryg przeciw Orbiterowi. Arianna przykładami ją złamała do współpracy.
* Gilbert Bloch: niby "komisarz" sił specjalnych by Infernia nie spieprzyła tematu z Bramą, ale dość przyjemny i nieproblematyczny. Pod niesamowitym wrażeniem tego, jak Infernia rozwiązała problem efemeryd i uratowała wszystkich.
* OO Infernia: świetnie ekranowana przed anomalną energią, uciekła Krypcie na przesterowanych silnikach przez Eustachego.
* OO Mfumo: zaawansowana jednostka do naprawiania Bram Eterycznych, ekranowana. Nie dość w wypadku tak niestabilnej Bramy. 
* ON Spatium Gelida: Superpancernik noktiański, który nie dotarł na pole bitwy, uwięziony pomiędzy Bramami przez pasożyty sił specjalnych Orbitera. Nadal tam jest.
* AK Nocna Krypta: wezwana przez Ariannę PRZYPADKIEM, wykorzystana świadomie do uratowania kogo się da channelując Kryptę w Bramę i kontrolując efemerydy. Potem jak się zmaterializowała na serio, Infernia jej zwiała.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Brama Kariańska: zdestabilizowana przez rozbicie się o nią innej jednostki. W środku są "uwięzione" jednostki noktiańskie z czasów wojny. Opasożytowana (w czym te pasożyty są wprowadzone przez siły specjalne Orbitera)

## Czas

* Opóźnienie: 3
* Dni: 3

## Inne

.

## Konflikty

* 1 - Arianna -> Flawia, niech Flawia wydobędzie informacje z Blocha. Flawia ostro protestuje, bo nie może działać przeciw Orbiterowi. Orbiter przecież nie ma intryg.
    * ExZ+3
    * VXX: Flawia zrobi to wbrew sobie, ale sama na siebie zaraportuje że zrobiła coś takiego.
* 2 - Propozycja - Infernia --> kosmiczny złom, wyłączamy silniki, MY TEŻ JESTEŚMY ZŁOM LOL. Plus - Eustachy włącza pełne ekranowanie Inferni. Niewidzialność przed efemerydami.
    * TrZ+3
    * XVzVVz: Infernia żre energię jak cholera (niedługo przetrwa), ale jest niewidzialna przed wszystkimi bytami taumicznymi.
* 3 - Eustachy zaproponował, by ukryć Infernię jako statek noktiański. Z perspektywy Arianny to jej problem. Wtedy efemerydy potraktują Infernię jako "swoją". Jako Alivia Nocturna.
    * TrMZ+3+5O
    * O: (+3V) Echo Krypty, częściowa integracja z Kryptą
    * VOV: Statki Noktiańskie w Bramie Dotknęły Infernię (tether), efemerydy "noktiańskie" bronią Infernię przed innymi biorąc ją za Alivię.
* 4 - Eustachy zmniejsza moc ekranowania i lecą --> Mfumo. Jeśli jakaś jednostka się zainteresuje Infernią, włączy się ekranowanie. Balansuje ekranowaniem.
    * TrZ+3+3O
    * XVOCVX: Jedna efemeryczna głodna jednostka --> Mfumo; doleciał, "Diana" powołana i działem niszczy efemerydę, Eustachy ściąga inne efemerydy do obrony Inferni, Elena idzie chronić.
* 5 - Elena ATAKUJE Dianę, zanim ta powie coś więcej niż "ja całowałam Eustachego a Ty tylko chciałaś". Bo to niebezpieczna linia jest.
    * TrM+3+5O
    * V: Elena wygnała Dianę, ale Diana jej dokuczyła słownie.
* 6 - Klaudia analizując jednostki dookoła próbuje dojść do tego co tu się spieprzyło. Co walnęło w ten węzeł?
    * TrZ+3
    * VVXV: Jedna ze zniszczonych jednostek była ostrzeliwywana i walnęła w węzeł fizycznie. Są jednostki co mają ludzi, ale ultra mało czasu na ratunek.
* 7 - Arianna chce uratować wszystkich. Nie ma jak. Więc - channelluje Kryptę. MACROSS STYLE! Arianna == Lynn Minmei. A dokładniej... KRYPTA jako Lynn Minmei inkarnując Ariannę.
    * ExMZ+4+5O
    * VzVVOV: Efemerydy podpięte do Krypty wyciągają ludzi z Bramy i Mfumo; Krypta się zmaterializowała. Arianna - disrupt by kupić czas.
* 8 - EUSTACHY! Wszystko, co Infernia ma w silnikach. NITRO!!! Infernia była gotowa na działania w Anomalii Kolapsu. Więc faktycznie ma ekstra zapasy paliwa. PALIMY PALIWO!
    * ExZ+4 -> TrZ+4 (ale gwarantowane uszkodzenie silnika)
    * XXV: Silniki w absolutnej ruinie i głębsze uszkodzenie Bramy, ale Infernia się oddaliła i zwiała Krypcie z ludźmi na pokładzie.
