## Metadane

* title: "Protest przed kultem na Szernief"
* threads: agencja-lux-umbrarum, problemy-con-szernief
* motives: energia-esuriit, kult-mroczny, manifestacja-ducha, faceci-w-czerni, bardzo-irytujacy-reporter, glupota-i-bezmyslnosc, jestem-ponad-prawem, stopniowe-tracenie-siebie, opetanie-przez-istote, niszczycielski-efekt-motyla
* gm: żółw
* players: kić, ukasz

## Kontynuacja
### Kampanijna

* [231202 - Protest przed kultem na Szernief](231202-protest-przed-kultem-na-szernief)

### Chronologiczna

* [231202 - Protest przed kultem na Szernief](231202-protest-przed-kultem-na-szernief)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA: 
    * Metric - "For Kicks"
        * "Why'd I have to be such trouble to please? | It wouldn't be me, oh if it was easy, if it was easy | Such trouble to please | It wouldn't be easy, if it was me"
        * "Why'd I have to go and break what can't be fixed?"
        * Wpierw winni są arystokraci. Ale potem - Frederico. Jeden błędny ruch.
        * Studium korozji duszy przez opętanie i obsesję. Korozja dostała manifestację i próbuje propagować.
            * Infinite Hunger - Esuriit.
* Opowieść o (Theme and vision): 
    * "Głupi protest młodych ludzi w nieodpowiedni sposób doprowadził do śmierci Wioli i powrócił Revenant."
    * Frederico opuścił Kult, by mieć lepsze życie, z Wiolą. Oddalili się tak daleko jak mogli. 3 lata później - szansa na dziecko.
    * Protest doprowadził do zamieszek, gdy ludzie chcieli dostać się do pracy. Strażnicy brutalni. Wiola jechała do szpitala. Umarła.
    * Po pojawieniu się Kultu Saitaera podnosi się pole magiczne.
    * Revenant wraca pomścić Wiolę i Frederico. Frederico chce, by ona mogła odejść. Gdy widzi, że nie jest w stanie, oddaje jej życie.
    * SUKCES
        * Exoneration tych, którzy siedzą 'za niewinność'
        * Rozwiązanie problemu ducha. Egzorcyzm? Wysiedlenie eks-protestujących by byli poza zasięgiem ducha?
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * energia-esuriit: głównym przeciwnikiem jest manifestacja Esuriit w formie Revenant i konwersji jednej z arystokratek w Wendigo.
    * kult-mroczny: Frederico był kiedyś członkiem Zakonu, chciał ich opuścić z ukochaną Wiolą. Gdy Wiola zginęła, próbował powstrzymać jej powrót (który zapoczątkował).
    * manifestacja-ducha: Esurient pragnący pomścić miłość-która-nie-była Frederico i Wioli. Powoduje morderstwa.
    * faceci-w-czerni: Agencja Lux Umbrarum, ukrywa i - co ważniejsze - zapewnia, że problem zaniknie. Mimo, że nikt nie widzi związku.
    * bardzo-irytujacy-reporter: Kalista tym razem bardzo skutecznie pokazuje że to proponuje Agencja i Rada ogólnie nie ma sensu.
    * glupota-i-bezmyslnosc: protesty młodych arystokratów, dzieci inwestorów i bogaczy przeciwko sprowadzeniu PRZEZ RODZICÓW kultu, prowadzące do śmierci normalsów.
    * jestem-ponad-prawem: młodym arystokratom wszystko było wolno, nikt nie pomógł Frederico ani normalsom ze stacji.
    * stopniowe-tracenie-siebie: Aerina Cavalis; kiedyś prospołeczna młoda arystokratka zmienia się w Wendigo przez Dotyk Esurienta (a ZMIENIŁA swoje życie po tym wypadku i próbuje udawać inaczej)
    * opetanie-przez-istote: wszyscy mordercy byli losowymi ofiarami. Ale wszystkie ofiary pochodziły z grupy protestującej lub blokującej Frederico przed ratowaniem Wioli.
    * niszczycielski-efekt-motyla: protest -> śmierć -> konieczność zwiększenia przychodów -> więcej saitaerowców -> pole magiczne -> duch -> morderstwa -> eskalacja...
* Detale
    * Crisis source: 
        * HEART: "sin: chęć protestów nie dbając o lokalnych ludzi i arogancja arystokracji prowadzi do śmierci jedynej dziewczyny którą ON kochał"
        * VISIBLE: "dziwnie losowe morderstwa"
    * Delta future
        * DARK FUTURE:
            * chain 1: ZADOŚĆUCZYNIENIE: mordercy są winni i przekonani o swej winie. W oczach prawa i w oczach publiki.
            * chain 2: MORDERSTWA: wszyscy protestujący i kilka innych osób wymrze. Duch rośnie w siłę, będzie zabijał dalej.
            * chain 3: PRAWDA: wiedza o magii wyjdzie na jaw, konieczność aplikowania masowych amnestyków.
            * chain 4: STARCIA: konflikty między grupami (inwestorzy - savaranie - kultyści) będą eskalowały.
        * LIGHT FUTURE: "Protestujący 'wyemigrują', duch wygaśnie, starcia osłabione 'to nie ich wina', mordercy dostaną szansę i sztuczne wyjaśnienie"
    * Dilemma: co z Mordercami? Co z Protestującymi? Jak to uzasadnić czy naprawić?

### Co się stało i co wiemy

* Kontekst stacji
    * CON Szernief została zaprojektowana jako stacja sprzedająca Irianium. Jest to miejsce o podniesionej ochronności elmag (lokalizacja: przy Sebirialis).
    * Okazuje się, że populacja która zainteresowała się tymi warunkami to byli Savaranie.
        * I inwestorzy są w klopsie. Dla savaran nadmiar energii to radość. Nie chcą kupować seks-androidów ani niczego. Pełna oszczędność.
            * Savaranie to NAJGORZEJ z perspektywy korporacji i inwestycji XD
        * Inwestorzy żądają zwrotu.
* Przeszłość
    * Zarządcy CON Szernief szukają kogoś kto będzie tam chciał żyć i najlepiej za to jeszcze zapłaci
        * Stanęło na ściągnięciu Kultystów Adaptacji. Ale oni nie są jacyś super niebezpieczni, sami tak mówią ;-).
    * 5 lat temu
        * W wyniku tego zaczęły się protesty błękitnej młodzieży (dzieciaki Inwestorów), która po nudzie związanej z SAVARANAMI ma jeszcze mieć GŁUPICH KULTYSTÓW.
            * Protesty to m.in. blokowanie ważnej infrastruktury transportowej, wandalizm itp.
            * W wyniku protestów umarła młoda kobieta w ciąży. Wiola.
            * Nikt z tego powodu nie ucierpiał. Młodzi są 'bezpieczni'.
    * 4 lata temu
        * Drakolici zostali sprowadzeni i faktycznie nie są bardzo groźni.
        * Frederico regularnie odwiedza miejsce śmierci swojej żony i obiecuje jej że coś się zmieni.
            * Rytualnie wbił sobie w miejscu jej śmierci nóż w serce, ale nie trafił i przeżył.
                * -> Esurient
        * Arystokraci próbują jeszcze coś robić przeciw drakolitom, ale nie do końca mają pomysł jak.
    * 1 rok temu
        * Podniesione pole magiczne. Esurient manifests.
    * pół roku temu
        * Frederico robi atak (scena zero)
* Teraźniejszość
    * W wyniku zmian pola magicznego (savaranie: oszczędność, kultyści: Adaptacja > Optymalizacja jako aspekt) doszło do polaryzacji umożliwiającej pojawienie się Revenanta
        * bo dla wielu ludzi śmierć Wioli była formatywna, do dzisiaj przychodzą tam jako do 'symbolu tego co złego się stało' i 'symbolu głupiej błękitnej młodzieży'
    * Duch opętuje randomowych ludzi by eksterminować 'błękitnych' odpowiedzialnych za jej śmierć
    * Pierwszą opętaną osobą był mąż Wioli, Frederico, którego posadzili do więzienia. Oczywiście.
    * Jako, że ilość MORDERSTW dramatycznie wzrosła i nikt nie wie czemu i to nie ma sensu, Lux Umbrarum to chce sprawdzić

### Co się stanie (what will happen)

* Siły
    * Esurient: Eksterminacja wszystkich powiązanych arystokratów oraz przekształcenie Aeriny w wendigo
    * Prawdoposzukiwacze: co się tu dzieje i co za tym stoi
    * Kult Sprawiedliwości: never forget, nie taką stację nam obiecano
    * Arystokraci: zmienić świat na lepsze bawiąc się tym
    * Drakolici Mawira: okazja zarobienia na arystokratach ale jesteśmy niewinni
    * Savaranie: nas to nie dotyczy, to dowód, że powinniśmy się trzymać razem
* F0: Frederico, atak terroru
    * celem jest zabicie jednej arystokratki i rozchlapanie krwi maksymalnie szeroko
    * jest inżynierem, na promenadzie
* F1: Stacja, normalne dni
    * Data
        * "To wina tych cholernych kultystów - zaczęli sprzedawać narkotyki i była nawet SPRAWA z Cassianem"
        * morderstwa
            * "Frederico, savaranin, strażnik, 3 drakolitów jest w więzieniu" : 5 morderstw arystokratów oraz 2 arystokratów popełniło samobójstwo w szale
            * 10 osób 'zniknęło' (Vanessa, wierna służka Aeriny i zabójczyni porywa dla niej ludzi a potem prowadzi przekupując strażników).
        * "Kultyści są tu od 4 lat, protesty były 5 lat temu, morderstwa zaczęły się pół roku temu"
        * "Ludzie znikają i ktoś wie jak ukryć ich zniknięcia"
    * stakes
        * Czy arystokraci zaczną umierać? Uda się kogoś uratować?
        * Czy aresztowani zostaną niewinni drakolici Mawira?
    * opponent
        * Esurient, który próbuje zniszczyć arystokratów i zmienić Aerinę
        * Wszyscy 'na szczycie' chronią arystokrację
    * problem
        * są osoby, które wiedzą, że Aerina 'jest chora', ale jej przejdzie - Vanessa jej pomaga (jej ojciec ją osłoni)
        * nikt nie chce powiedzieć prawdy; tylko Kalista zbiera ją do kupy
        * zabójcy to 'losowi ludzie', tak jakby coś się bardzo dziwnego tu działo
        * wszyscy wierzą, że to porcja skażonych narkotyków Mawira. Ale Mawir ma dowody, że nie sprzedawał i one tak nie robią.
    * ESCALATION
        * Arystokraci
            * Arystokraci -> Mawir, po środki i imprezę, Security chroni, Poszukiwacze przesłuchują
            * Vanessa kogoś porywa, Aerina pożera ciało. Mawirowcy znajdują, wezwane Security...
            * Arystokraci się bawią i żądają imprezy podczas protestów
        * Stacja
            * Security -> Mawir, aresztowanie, walka i Mawir wycofuje się w głąb
            * 
        * Esurient
            * Esurient zmusza arystokratę do znęcania się nad służką. Nikt nie reaguje.
            * Podniesienie brutalności Security i zwarcia Mawira z innymi

## Sesja - analiza

### Fiszki

Luminarius

* Lester Martz, dowódca operacji

CON Szernief - rada i inwestorzy:

* Janvir Krassus: OCEAN: (O+ C+) | "Przez współpracę i innowacje budujemy lepszą przyszłość." (Locke) | VALS: (Tradition, Security) | DRIVE: "Przekształcenie Szernief w zaawansowaną kolonię."
* Larkus Talvinir: OCEAN: A-O- | "Żartuję nawet w złych sytuacjach; prowokuję i nie boję się konfliktów" | VALS: Stimulation, Universalism | DRIVE: "Zabawa kosztem innych to moja rozrywka"
* Klaudiusz Widar: OCEAN: (N+C+) | "Bezwzględny i strategiczny, skupiony na zyskach; emocje nie mają miejsca w biznesie" | VALS: (Power, Achievement) | DRIVE: "Monopol przy Sebirialis"
* Damian Orczakin: OCEAN: (E+O+) | "Zawsze jest coś o czym nie pomyśleliśmy i zawsze można próbować." | VALS: (Power, Stimulation) | DRIVE: "Jak najszybszy zwrot z inwestycji"
* Estril Cavalis: OCEAN: (N-A+) | "Zrobią to, co jest powinni zrobić gdy się im pokaże konsekwencje." | VALS: (Benevolence, Tradition) | DRIVE: "Najlepszy sposób na zwiększenie władzy to indoktrynacja młodych"

CON Szernief - Liniowe siły: 

* Mateo Glodiran: OCEAN: (N+C+) | "Surowy, zdecydowany dowódca, który nie toleruje nieposłuszeństwa i chaosu" | VALS: (Security, Power) | DRIVE: "Porządek i kontrola"
    * on dowodził operacją ochrony 'błękitnych' przed konsekwencjami. Szef ochrony z ramienia Stacji. (Ulisses Feral)
* Kalista Surilik: OCEAN: (A+E+) | "Odważna i zdecydowana, w gorącej siarce kąpana" | VALS: (Benevolence, Achievement) | DRIVE: "Prawda MUSI wyjść na jaw"
* Felina Amatanir: OCEAN: (N- C+) | "Nieustraszona i niezależna, wyróżnia się determinacją i profesjonalizmem" | VALS: (Power, Security) | DRIVE: "Porządek i sprawiedliwość, ponad kontrakt"
* Ignatius Sozyliw: OCEAN: O+A- | "Świat to szereg wyzwań do pokonania; rzadko zważam na emocje innych" | VALS: Achievement, Power | DRIVE: "Moc to moje przeznaczenie, a walka - droga do niej"
* Mawir Hong: OCEAN: (E+A-) | "Wygra ten kto posunie się najdalej i poświęci najwięcej" | VALS: (Power, Security) | DRIVE: "Lepiej być królem w błocie niż niewolnikiem w raju"
* Teriman Skarun: OCEAN: (E+A+) | "Harmonia wymaga poświęcenia i wiary." | VALS: (Benevolence, Universalism) | DRIVE: "Wszystko stanie się jednością w Saitaerze, wszystko to tylko kroki"
* Wit-421: OCEAN: (A+ C+) | "Nieważne jak dobrze robisz pracę, jeśli nikt nie rozumie korzyści" | VALS: (Benevolence, Universalism) | DRIVE: "Harmonijny rozwój stacji, wszystkich obecnych"


Randomy (drakolici):

* Zygfryd Ogriss
    * OCEAN: C+E- | "Skupiam się na celach, nie ulegam emocjom; moje plany są zawsze starannie przemyślane" | VALS: Self-Direction, Security | DRIVE: "W pogoni za doskonałością, moim przewodnikiem jest dyscyplina"
* Helissa Zarimis
    * OCEAN: N+O- | "Intensywnie reaguję na świat wokół, często odczuwam niepokój; kieruję się logiką, nie emocjami" | VALS: Universalism, Tradition | DRIVE: "Przez wiedzę i tradycję odnajduję spokój w chaosie"

Randomy (savaranie):

* Markus Palavius
    * OCEAN: E+A+ | "Uwielbiam grupowe przeglądanie żwiru" | VALS: Stimulation, Hedonism | DRIVE: "Znam te jaskinie jak własną kieszeń"
* Julia Zepunit
    * OCEAN: C-N+ | "Unikam konfliktów i chowam się w ciszę" | VALS: Benevolence, Universalism | DRIVE: "Ludzkie formy da się naprawić"
* Taddeus Zeler
    * OCEAN: A-C+ | "Praktyczne rozwiązania. Istotne." | VALS: Tradition, Security | DRIVE: "Kolonia. Lepsze jutro."

Randomy (inne):

* Arkadiusz:
    * OCEAN: E+A- | "Nigdy nie przegap okazji do przygody i zysku; innych traktuj jako narzędzia" | VALS: Power, Hedonism | DRIVE: "Żyję dla bogactwa i przyjemności"
* Rafał:
    * OCEAN: A+O+ | "Zawsze pełen energii do tworzenia; pomocny i kreatywny w poszukiwaniu nowych rozwiązań" | VALS: Face, Tradition | DRIVE: "Innowacja i odrodzenie to moja droga"
* Witold:
    * OCEAN: A-N+ | "Żyję w swoim świecie, niezależny od innych; ostra natura, unikam zgiełku" | VALS: Security, Self-Direction | DRIVE: "Moja korporacja to moja twierdza i duma"

#### Strony

* Esurient
    * CZEGO: Eksterminacja wszystkich powiązanych arystokratów oraz przekształcenie Aeriny w wendigo
    * JAK: Opętywanie przyszłych morderców, zwiększanie krwawych czynów, szał arystokratów
* Prawdoposzukiwacze
    * KTO: Kalista, Felina...
    * CZEGO: Dojść do tego co się tu naprawdę dzieje i co za tym stoi
    * JAK: Przesłuchania, zbieranie danych, określenie co i jak
* Kult Sprawiedliwości
    * KTO: populacja zwykłych ludzi, naciskanych przez arystokrację
    * CZEGO: "Niech się boją", "niech nie mają nieskończonych praw"
    * JAK: Przypominanie co i jak się stało, ulotki
* Arystokraci
    * KTO: Aerina, Cassian, Tobiasz, Maximus, Livia, Larisa, Anastazja, 
    * CZEGO: Zmienić świat na lepsze, zrobić różnicę, świetnie się bawić przy okazji
    * JAK: Protesty, płomienne mowy, 'my jesteśmy ważniejsi to robimy co uważamy za słuszne'
* Drakolici Mawira
    * CZEGO: Nie dać się wpakować że to oni; serio nie mają powodu
    * JAK: Redukować kontakt z arystokratami ale NADAL sprzedawać im niewłaściwe środki i specyfiki
* Savaranie
    * CZEGO: Robić swoje, robić porządnie, nie dać się wciągnąć ALE chronić stację-dom

### Scena Zero - impl

Obserwacja na monitorze ataku przez Zespół.

Frederico wpierw przekonuje kolegę, by ten zagadał strażnika i dziewczyny proponując im coś fajnego na sprzedaż. Ubrał to jako ZAKŁAD - odważy lub nie odważ.

Tr Z +2:

* Vz: Z uwagi na długoletnią znajomość i przyjaźń koleś podszedł i zagaduje strażnika
* X: Kumpel będzie ciężko spałowany (wiemy jak odwrócił)
* Vz: Strażnik jest zajęty pałowaniem kumpla i nie patrzy na Frederico.
* X: Kumpel trafi do szpitala, ciężko potraktowany

Frederico używa ukrytego w koszu na śmieci drona. Cel - zabić. Ładunek wybuchowy.

* Vr: Dron eksplodował, arystokratka zginęła. Wybuch ochlapał inne dziewczyny które zaczęły krzyczeć.
* V: Frederico spokojnie się wycofał, poszedł tam gdzie chciał, spokojnym krokiem gdy strażnicy zaczęli się zbierać itp.

...na mostku skończyliście nagranie. Wiadomość od Luminariusa: "Frederico Zyklas, inżynier, osoba dość spokojna na co dzień została znaleziona przy osi, przy tramwajach. Żył. Nie bronił się i oddał się w ręce policji". Potem zginęło jeszcze 5 arystokratów. Za każdym razem mordercą była osoba dziwna. Raz był to strażnik arystokraty. Raz, był to sprzedawca w sklepie w którym ten coś kupował. Piąty arystokrata sam rozszczelnił skafander. To sprawia, że podejrzewamy anomalie. To sprawia, że Wy macie zadanie.

To działo się pół roku temu.

### Sesja Właściwa - impl

Luminarius - zbliża się do CON Szernief. Biurokrata chce zobaczyć jak w przeszłości to wyglądało - bazy danych, niepowiązane zabójstwa / samobójstwa itp.

Tp +3:

* V: Masz pewną ciekawą chronologię tej stacji:
    * Stacja była zbudowana przez konsorcjum inwestorów. Stacja światła przy Sebiralis.
    * Stacja to populacyjnie piekło. 60% populacji stacji to savaranie. 
    * Ściągnięto dodatkową populację która chciała zapłacić. Drakolici. Drakolici - kultyści boga Adaptacji. 25%.
    * 15% procent "innej"
    * Arystokraci próbowali się pozbyć kultystów i savaran.
    * Bóg Adaptacji - Saitaer
* V: Linia polityczna
    * Drakolici podzielili się na dwie grupy
        * Mawir Hong: dobra zabawa, korzystanie z miejsca które jest, pędzimy bimber -- arystokraci
        * Teriman: to wszystko to małe rzeczy, trzeba się kulcie, adaptacji
    * Savaranie chcą jak najlepiej dla stacji.
    * Poszukiwacze Prawdy: lokalna dziennikarka, lokalna agentka ochrony. Oni próbują zrozumieć co tu się dzieje
    * Arystokraci: chcą Czynić Wielkie Rzeczy i bawić się dobrze. Zgodnie z plotkami, zachowują się... niegodnie. Są chronieni przez Stację.
    * Kult Sprawiedliwości: 'zwykłych ludzi' którzy są uciemiężeni przez arystokratów i szefostwo stacji
        * JAK TO STACJA ROBI KRZYWDĘ LUDZIOM I STACJA TEZ
        * -> zniknęło 10 osób. Zniknęli i nikt nie wie gdzie
        * część ludzi tu sprowadzonych nie chciała tu być

A Frederico? Po zabiciu arystokratki zadał sobie ranę i wisiał przy Osi.

-> TrZ+3

* X: Kalista dowiaduje się że ktoś szpera w dokumentach i O CO CHODZI.
* X: Kalista jest przekonana, że coś wiecie. Wie to na 100%.

Biurokrata dostaje wiadomość od Kalisty, prosi o rozmowę. Rozmowa.

* Kalista: Agencja Lux Umbrarum, co za niespodzianka. Nie wiedziałam... co Wy w sumie robicie?
* Biurokrata: Witam serdecznie, Michael z tej strony. Badamy sprawę korupcji w sprawie wędkarstwa. Potrzebujemy pomocy.
* Kalista: MOJEJ pomocy w sprawie WĘDKARSTWA?
* Biurokrata: Ważna osoba na stacji. Słyszałem że gromadzi pani informacje.
* Kalista: Wszystko opisuję w artykułach. ...niezrozumiałe przypadki hipnozy są.
* Biurokrata: Coś więcej? Nie zdążyłem przeczytać.
* Kalista: Mam fana, niesamowite. Ale tak, oczywiście, włączę tylko nagrywanie, dobrze?
* Biurokrata: Proszę uprzejmie.
* Kalista: Osoby, które podobno zabiły... nie tak. Oni zabili tych arystokratów, ale są niewinni. Oni są niewinni. A przynajmniej... 
* Biurokrata: Coś więcej?
* Kalista: Savaranin próbował popełnić samobójstwo, bo 'jest uszkodzony'. Drakolici nie wiedzą czemu to zrobili. A Frederico nie chciał ze mną rozmawiać.
* Biurokrata: Savaranin jest uszkodzony?
* Kalista: Nie wiem. On tak twierdzi. Podejrzewam, że... dlaczego ktoś chciałby zabijać TYCH ludzi? Rozumiem. WIEM że się tym interesujesz.
* Biurokrata: Wie pani, interesują nas wszystkie sytuacje jakie mogą być powiązane z waszą sprawą, posłucham i się dowiem. Ma pani możliwość i dostęp, wie pani jakie są powiązania... nie wiem co jeszcze może mi się przydać.
* Kalista: (słodki uśmiech) Drogi agencie. Możemy się wymienić. Chcę pewne agregacje. ALE. Muszę mieć uprawnienia. Załatw mi te uprawnienia - a dostaniesz ode mnie co wiem.

(w między czasie Hacker wbija się Kaliście na kompa).

Tr Z+3:

* X: Kalista WIE że tam się wbiliście i poznaliście jej dane (+1 pkt niegodziwości)
* V: Haker dostaje odpowiedzi
    * Kalista odkryła już, że MORDERCY są całkowicie niepowiązani. To - dosłownie - randomy.
    * Ale, zabici arystokraci są powiązani ze sobą. Jest lista 10-15 osób wśród arystokratów i 5 osób jest z tej listy.
        * Ta lista jest powiązana z zamieszkami z 5 lat temu, z czasów Wielkich Protestów przed sprowadzaniem kultystów na tą stację
        * Arystokraci robili blokady.
            * "Nie chcemy kultów"
            * "Tylko normalni ludzie"
            * (rodzice i możni arystokratów podejmowali decyzje, arystokraci blokowali ludzi pracy)
            * wtedy zaczęły się pierwsze starcia między ludźmi i arystokratami
        * Rok później sprowadzono kultystów, ta radykalna grupka próbowała im obrzydzić itp, ale kultyści się zaadaptowali.
        * Dwa lata później wszystko się rozmyło
        * I NAGLE pięć lat później zaczynają się morderstwa.
* V: Haker daje jej dostęp do Luminariusa i publicznych danych. "Oferta krótkoterminowa". Moc obliczeniowa i "publiczne dane Agencji". Bo jest pomocna i potrzebna Wam jej pomoc.

.

* Kalista: (jej twarz robi się purpurowa.) "Agencie, przełamywanie zabezpieczeń i to... jestem jak adwokat, z nazwiskami, ze wszystkim! Ja Was... obsmaruję jak tabloid!"
* Kalista: (oferta) (łagodnieje)
* Biurokrata: "Nie wiem o czym mówisz"
* Kalista: (wyłącza dyktafon) "Mogłeś poprosić." (chwilę myśli) "Aha, podejrzewasz ICH. Dlatego nie mogłeś poprosić. Nie może trafić na dyktafon" (tworzy własną konspirację w głowie)
* Kalista: "Wy nie szukacie wędkarstwa. To przykrywka. Chcecie znaleźć odpowiedzialnych za morderstwa. Na samym szczycie tej stacji."
* Biurokrata: "Nie potwierdzam, nie zaprzeczam, to tylko pani teoria." (uśmiech)
* Kalista: (głęboki oddech) "Dobra, pomogę Wam. Wyprowadzimy to. Spotkajmy się w inżynierii. Tam jest głośno i ciepło. Za 2h. Przyprowadzę koleżankę, weźmiemy co mamy."

Biurokrata buduje legalny case by móc porozmawiać z Frederico.

Tp+3:

* X: Frederico nie chce się z Wami widzieć. Nie jest to problem dla Agencji, bo nikt go nie lubi.
* Vr: Strażnicy powiedzieli wyraźnie 'jeśli chcesz się pan spotkać z tym dziwakiem, proszę bardzo'
* X: Strażnicy mają ZAKAZ zostawienia go samego przez Radę. On ma cierpieć.
* V: Jestem z Agencji. To ważne. Dziękuję Wam ale musicie opuścić to miejsce i wyłączyć kamery.

Jesteś w więzieniu, w sali spotkań z Frederico. On ma kaftan bezpieczeństwa. Strażnik "dla jego bezpieczeństwa, nie pańskiego." Frederico, z bladym uśmiechem "ależ drogi Józefie, nic ani agentowi ani mnie się nie stanie." Józef na to "nie możemy dać Ci się zabić". I faktycznie, Frederico nie dosypia itp. Strażnik wychodzi, Frederico uśmiecha się do niego słodko.

* Frederico: Agencja. Nie wiem kim jesteście. Nie chcę z Tobą rozmawiać.
* Biurokrata: Jest to dla Twojego dobra, mamy dużą władzę, możemy Ci pomóc.
* Frederico: Nie możecie mi pomóc. Nie przywrócicie jej do życia. Nie pomścicie jej śmierci.
* Biurokrata: Czyjej?
* Frederico: (zimny, okrutny uśmiech) Nawet nie wiesz z czym masz do czynienia.
* Biurokrata: Nie wiem, więc potrzebuję Twojej pomocy. A pomsta... da się coś z tym zrobić, więcej informacji.
* Frederico: Wróć do mnie, gdy będziesz wiedział, KOGO chcę pomścić.

Hacker szuka danych u Kalisty - o czym Frederico mówi.

Tr Z+3:

* V: Podczas jednego z protestów żona Frederico - w ciąży - jechała do szpitala.
    * W zamieszkach została trafiona w głowę. Ani ona ani dziecko nie przeżyło.
        * Jej imię było Wiola
    * Frederico to całkowicie złamało. On nie opuszczał miejsca jej śmierci. To było przy osi.
    * Frederico próbował rok po pojawieniu się Kultu popełnić samobójstwo. Ale nie trafił w serce i przeżył.
* Vr: 
    * Frederico mówił, że "ona go uratowała". Że to miało być "ich nowe życie". "Wpadł w złe towarzystwo i robił straszne rzeczy, ale ONA go wyciągnęła"
    * Dla niego ta bezsilność, że nikt nie został ukarany... nieszczęśliwy wypadek... wtedy jeszcze Kalisty tu nie było.
    * Frederico regularnie upuszczał krew na miejscu jej śmierci.
    * Frederico i Wiola chcieli "nowego życia tutaj". Oni nie lubili arystokratów, ale ich nie nienawidzili. Ale Wiola MUSIAŁA jechać do szpitala. I zginęła przez ten protest.
* XX: Luminarius jest za daleko i nie ma dostępu. Przeklęta Kalista za mocno ssie. A obiecaliście. Przycinamy ją.
    * Frederico był powiązany z jednym z mrocznych kultów przed spotkaniem Wioli. Był bieda-okultystą.

Wracamy do Biurokraty.

* Biurokrata: Chodzi o Wiolę i dziecko?
* Frederico: (Zimny uśmiech) A jednak się przygotowaliście. Tak. Tak... tak... (złamany człowiek)
* Biurokrata: Wiola?
* Frederico: Tak... była najlepszym... najlepszym co istniało. Była aniołem... 6 miesiąc. Były komplikacje. Ale przez chciwość... musieliśmy jechać.
    * płomień w oczach, czysta nienawiść
* Biurokrata: Na kim chcesz się zemścić?
* Frederico: Ja? Nie chcę. Chcę, by ona... chcę by ona wróciła. Ale to nie jest możliwe. Nie da się wrócić.
* Biurokrata: Na kim chcesz się zemścić?
* Frederico: (potrząsnął głową). Oni wszyscy. Wszyscy, przez których ona nie żyje. Ten protest. Oni wszyscy muszą cierpieć. Ale ona musi spać. Nie chcę, by wróciła. Nie tak.
* Biurokrata: Dlatego zdecydowałeś się ją zabić?
* Frederico: Miałem nadzieję, że ona zaśnie. Że się nie obudzi. Że Wiola nie wróci. To mój błąd. Ja zrobiłem błąd. Powinienem był zabić wszystkich arystokratów. Ale nie tak, nie to co zrobiłem. Chwila słabości.
* Frederico: Możesz to zatrzymać. Ale musisz mnie zabić.
* Biurokrata: Ale co zatrzymać?
* Frederico: Wiolę.
* Biurokrata: Wiola przekonała Cię do zabójstwa?
* Frederico: Jeśli Ci powiem. Zatrzymasz ją i sprawiedliwość się nie stanie. Ale ja nie chcę, by ona musiała zabijać. Nie chcę by WIOLA musiała zabijać.
* Frederico: Jak chcesz ją zatrzymać? Jak sprawić, by Wiola zasnęła? Ja nie wiem. Próbowałem ją zatrzymać, naprawdę.
* Biurokrata: Dlatego puszczałeś krew?
* Frederico: Tak. Nie pochówku a śmierci. Tam to robiłem. By... by mogła spać, pić moją krew, nie szukała swojej. Mówię - zrobiłem błąd, straszny błąd. Chciałem się zabić. Nie udało mi się. I wtedy ją USŁYSZAŁEM. Nie dała mi odejść. Powiedziała, że pomści moje cierpienie. Nie chciałem. Ja... to nie byłby pierwszy człowiek, którego szyję przegryzłem zębami. Ten smak... ale nie ONA. Ona tego nie zna. Ona nie ma tego znać. Nie Wiola.

Samobójstwo, chęć zemsty, krew i śmierć. Biurokrata wyciąga od Frederico co on robił z tym okultyzmem. Głód, chciwość. Zemsta.

Ex Z+3:

* V: "Opowiedz mi co robiliście". Frederico opowiada.
    * Wyobraź sobie życie w niewoli. To nie jest tak że byliśmy niewolnikami. Ale nic nie możesz. Wszędzie komputery, kamery, obserwacja, zasady. I - masz być TAKI a nie inny.
    * Z kolegami, znaleźliśmy sposób. Księga, podpowiedzi, krew czyni Cię większym, pijesz cudzą krew. A potem - karmisz się cudzym bólem.
    * Rośniesz. Wpierw robiliśmy to sobie. Potem - innym. Oni... byli pod nami.
    * I ona. Wiola. Ona wzięła mnie stamtąd. Bo oni.. oni zaczęli się zmieniać. Nie chodzi o kanibalizm, to normalne.
    * "Wiola nie może pożerać!"
    * ESURIIT.
* (+1Vg: zaoferowanie samozniszczenia) X: Frederico Ci powie, jeśli przestaną go torturować (compel)
* Vz: Frederico powiedział co zrobił.
    * Powiedziałem że to moja wina. Nie wiedziałem, że wróci. Że może. Nie da się wrócić. NIE DA SIĘ.
    * Pokazałęm jej tych arystokratów. Wszystkich z zdjęć. Mówiłem o nich, opowiadałem o nich jej. Nawet otaczałem ich kontury swoją krwią. Jak mogłem znaleźć DNA tych ludzi... łączyłem z krwią i jej dawałem. Żeby wiedziała kto ją zabił.
    * "Oprócz arystokratów, inni?" -> F: "Nie, oni nie są winni. Oni nie. Tylko arystokraci. Oni są winni. Wiola nie krzywdzi niewinnych. Nie może."
    * Ja rozmawiam z Wiolą, ona mnie odwiedza. Zawsze jest przy mnie. Mój anioł. Ale nie lubię, jak ma krew na rękach. Ona ma takie piękne ręce. Nie chcę widzieć krwi na rękach i twarzy.
        * "Ja ją widzę. Ja ją czuję. Ona jest ze mną."

Korzystając z dokumentów i innych rzeczy - po co były te protesty. ZNAMY wersję oficjalną, ale jak było? Może coś pod spodem?

Tr Z +3:

* V: Protesty FAKTYCZNIE były tym czym się wydawało. Dzieciaki robiły protest bo nie muszą pracować bo chcą Coś Zrobić i bo mogą.
    * To jest okazja "look at me!"
    * Po to, bo to jest "coś do zrobienia"
    * Przetasowania sił w grupach. Normalna gra o status.
        * Estril Cavalis jest ojcem Aeriny Cavalis.
            * Aerina jest dziewczyną dość łagodną, pomocną, angażuje się itp.
            * "Jako arystokratka muszę robić WIĘCEJ"

Hacker ma nowy pomysł jak to rozwiązać. Każdy z tych arystokratów ma coś co może zrobić, coś prestiżowego, coś co ich podnosi w karierze. Coś, co ONI chcą zrobić i ich opiekunowie uznają za wartościowe dla nich. Jak ich opiekunowie nie uznają za super-wartościowe to można troszkę pomóc. I to jest kwestia znalezienia takich 'placementów' w miejscach wspieranych przez Agencję. Biurokrata.

Tr Z+4:

* Vr: Większość arystokratów na to idzie i ten temat jest załatwiony (6 osób/10)
* X: Kalista zrobiła link w głowie między 'eksport arystokratów' a 'obecność Agencji' a 'Frederico'.
* Xz: Kalista nie ufa Agencji. WIE że coś zrobiliście i nie wie co. Ale obiecaliście i nie powiedzieliście.
* Vz: Wszyscy arystokraci poza jedną osobą zostaną przesunięci. Tą osobą jest właśnie Aerina. Ojciec się nie zgadza.
    * (oferta jest MEGA; jest DOPASOWANA do tej dziewczyny)

Hacker wykorzystuje Luminariusa, sprzęga z TAI stacji, używasz danych ze stacji ze WSZYSTKICH zarejestrowanych ruchów Frederico. Z tego co znalazła na jego temat Kalista. Wszystko co mówi. Moduły analizujące stan mentalny. Eliminuje wszystko 'nie do naprawy'. I wyciąga anomalne ruchy Frederico by znaleźć offeringi.

Ex Z +4:

* X: Kalista już WIE że robiliście ciężką operację choć nic nie może udowodnić. (+1 pkt niegodziwości)
* X: Trzeba będzie wysłać sporo agentów / lokalną populacją / dron
    * "zagrożenie biologiczne, czyścimy biologicznie". Wysyłamy savaran na to XD. I Hacker chce to zneutralizować lapisem.
    * Kalista ma +1 do plotek.
* Vz: UDAŁO się z pomocą Luminariusa. Zajmie to 36h, ale savarański rój znajdzie to w odpowiednim miejscu - to jest w jednym z miejsc gdzie inżynierowie przy -2 poziomie.
    * Nie ma ŚLADU po znikniętych ludziach. (+3Vg)
* Vz: Nie udało się znaleźć znikniętych ludzi, ale savaranie znaleźli ślady utylizacji ludzkich ciał.
    * Ktoś tych ludzi zabił i... najpewniej zjadł. Próby ukrycia, usunięcia. CAŁKOWICIE w innym obszarze.
* Vg: dwie kobiety. Jedna wspomagana. Druga - Aerina.

Biurokrata, widząc to, zdecydował się na kontakt z Aeriną. DELIKATNIE dał do zrozumienia, ze wiesz o jej zainteresowaniu kulinarnym i że możecie pomóc. Niech przyjdzie na Luminarius. I rozmowa w śluzie.

* Aerina (blada i szczupła, wychudzona): Agencie Michaelu, to przyjemność się z Tobą spotkał
* Biurokrata: Aerino, dla mnie też przyjemność. Niech ta rozmowa jest przyjemna, pomóżmy sobie. Czy jesteś może głodna?
* Aerina: Nie, dziękuję (z wyższością). Niezdrowo wygląda.
* Biurokrata: Czy w ostatnim czasie mierzyłaś się z nasilonym głodem, pragnieniem, dążeniem do zaspokojenia wewnętrznych pragnień które niekoniecznie są moralne? (puszcza oczko)
* Aerina: Przejdźmy do rzeczy. Wiem, że chcesz bym zmieniła miejsce. Jestem skłonna się przeprowadzić. Czy o to chodzi w tej rozmowie?
* Biurokrata: Przejdę do rzeczy. Namierzyliśmy i mamy dowody, że ukrywasz coś. Przed całym światem. Podzielisz się?
* Aerina: (unosząc brew) Że wbrew plotkom nie jestem dziewicą? Proszę.
* Biurokrata: Przechodząc do sedna (co znaleźli, szczątki i nagrania z kamer)
* Aerina: (Krew odpłynęła jej z twarzy. Usiadła z hiperwentylacją. Próbuje się opanować)
* Aerina: ...(przez łzy) czego chcesz, ja nie chciałam i nie chcę. Ale NIE MOGĘ.
* H: Czego nie możesz?
* Aerina: Zapach... nie wiem jak to powiedzieć, to NIE JESTEM JA. NIE CHCĘ tego robić.
* Biurokrata: Od kiedy się zaczęło?
* Aerina: Trzy miesiące temu. Nagle... (mega wstyd) byłam w medycznym. I... obryzłam palec z kostnicy. Vanessa to ukryła. Moja... agentka.
    * Aerina NAGLE złapała ten głód. Nie wie czemu. Najpierw "to nie była ona", ale potem to już była ona.
    * Aerina zaczęła karierę kanibala od zjedzenia części swojego przyjaciela z protestu.
    * Aerina zaczyna pamiętać rzeczy których nie może pamiętać. Np. ciążę.

"Możemy Ci pomóc, ale pójdziesz z nami. U nas będziesz miała specjalistyczną pomoc. Bez pytań, bez niczego. Jesteśmy od takich tematów ekspertami."

Aerina się zgadza. Ona nienawidzi tego czym się staje. I pomoże przekonać Radę, by Frederico nie był torturowany.

Tr Z +3:

* V: Frederico będzie wydany na Luminarius i on, Aerina i Vanessa odlecą w siną dal.

## Streszczenie

Seria tajemniczych morderstw arystokratów na Szernief. Pierwszym sprawcą był Frederico, inżynier, Agencja odkrywa, że morderstwa wydają się być spowodowane przez anomalię Esuriit, która wpływa na zachowania ofiar. Kluczem do prawdy było przesłuchanie Frederico (kiedyś kultysty) - jego żona i nienarodzone dziecko zginęły podczas protestu arystokratów. Okazało się, że jego żona próbuje inkarnować się w Aerinie. Rozwiązaniem było rozlokowanie arystokratów poza stację i wyleczenie Aeriny, zabierając ją do Agencji.

## Progresja

* Aerina Cavalis: Skażona przez Anomalię Esuriit; potrzebuje ludzkiego mięsa. Wyciągnięta przez Agencję, by jej pomóc.
* Kalista Surilik: miała dostęp do Luminariusa, jego publicznych banków danych i mocy obliczeniowej co mogła doskonale wykorzystać. I to zrobiła.
* Kalista Surilik: nie ufa Agencji. Wie o linkach Agencji z dziwnymi rzeczami jakie się tu dzieją. Wie o ciężkiej operacji Agencji i że są dziwni.

## Zasługi

* Klasa Biurokrata: odkrył ważne elementy historii Szernief, zaproponował współpracę Kaliście, wydobył prawdę od Frederico (rozmowa) i skoordynował operację ewakuacji arystokratów.
* Klasa Hacker: włamała się do danych Kalisty i znalazła z jej pomocą dane o arystokratach, zidentyfikowała anomalne zachowania Frederico i przygotowała możliwość wartościowej ewakuacji arystokratów (by rozproszyć anomalię przez redukcję masy krytycznej energii).
* Frederico Zyklas: kiedyś kultysta, wyciągnęła go żona. Przy protestach zginęła jego żona i dziecko. Próbował ich przywrócić w rozpaczy i sprawił, że pojawiła się Anomalia. Idąc za jej podszeptem zabił arystokratkę za pomocą zdalnie sterowanego drona z ładunkiem wybuchowym. Próbował popełnić samobójstwo, ale złapała go policja. Wyjaśnił wszystko Agencji, gdy był przesłuchany.
* Kalista Surilik: świetnie złapała Agencję za tematy anomalne; przekazała informacje Zespołowi o tym co wie (historia, Frederico, działania) i za to dostała wsparcie Luminariusa do prowadzenia swoich badań i śledztwa. Tak czy inaczej, jej chęć pomocy jest większa niż jej nieufność. Acz nie jest traktowana poważnie przez Agencję.
* Aerina Cavalis: arystokratka z Szernief; płomienna miłośniczka 'dobra' zmieniająca się w wendigo przez Esuriit. Kiedyś protestowała przeciwko ściąganiu drakolitów na Szernief. Odkąd manifestowała się Anomalia, Aerina ma niekontrolowany głód kanibalistyczny. Jej pierwszą ofiarą była znajoma z protestów. Zniknięcia ludzi wykonuje jej biosyntka, Vanessa, by Aerina nie była głodna. Współpracuje z Agencją by się wyleczyć.
* Vanessa d'Cavalis: biosyntka bojowa Aeriny; pomagała Aerinie w ukryciu jej nowej natury (kanibalistycznej) oraz porywała dla niej ludzi. Nieskończenie lojalna.
* OLU Luminarius: dostęp do jego danych był kluczowym mechanizmem przekupienia Kalisty; ponadto pomógł w analizie danych związanych z ruchami Frederico i wzmacniał moc lokalnej TAI. 

## Frakcje

* Con Szernief: cylindryczna kolonia musiała zatrzymać pewne działania kopalniowe na 3 miesiące, co spowodowało dużą stratę finansową i duże niezadowolenie społeczne.
* Con Szernief Querenci: poszukiwacze prawdy (nazwani: Felina i Kalista); próbują dojść do tego co tu się dzieje i Kalista dała radę wykroić własne źródło danych dzięki Luminariusowi.
* Con Szernief Arystokraci: duża ich część rozproszona przez działania Agencji Lux Umbrarum. Poważnie osłabiona frakcja.
* Con Szernief Mawirowcy: korzystają z okazji by zarobić na arystokratach i dobrze sobie pojeść. Idą głębiej w kierunku na 'grupę niezależną'
* Con Szernief Justarianie: żądają sprawiedliwości dla Stacji, są skupieni przeciwko Radzie i siłom dowodzącym Con Szernief. Nie taką stację im obiecano, więc czemu jest tak źle?

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Kalmantis: noktiański sektor pełny dobrobytu, gdzie nie ma może żadnej dobrej planety gdzie da się mieszkać, ale wykorzystywane są arkologie i CON.
            1. Sebirialis, orbita
                1. CON Szernief

## Czas

* Chronologia: Aktualna chronologia
* Opóźnienie: -2109
* Dni: 4
