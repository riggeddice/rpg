## Metadane

* title: "Marysiowa Hestia Rekinów"
* threads: rekiny-a-akademia
* gm: żółw
* players: kić, anadia

## Kontynuacja
### Kampanijna

* [211228 - Akt, o którym Marysia nie wie](211228-akt-o-ktorym-marysia-nie-wie)

### Chronologiczna

* [211228 - Akt, o którym Marysia nie wie](211228-akt-o-ktorym-marysia-nie-wie)

## Plan sesji

### Theme & Vision

* Wolność (Melissa, Babu, Liliana) vs Bezpieczeństwo (pozbycie się kultu)
    * Liliana to wzmacnia i stawia jako podstawę
* Jak daleko posunie się Santino dla utrzymania swojej reputacji?
    * Prestiż vs Konformizm
    
### Ważne postacie + agendy

.

### Co się wydarzyło

.

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Do Marysi trafił nowy potencjalny numer "Plotek Royalsów" Karola Pustaka. A dokładniej, trafił do niej przez wiadomość - Pustak napisał artykuł i pyta, czy Marysia chce się ustosunkować. A w numerze seria wywiadów, opowieści i zdjęć ulotek na temat "Marysia Sowińska - podliz Eterni". Autorka - Liliana Bankierz. "Marysia, wróg wszelkiej wolności". Ogólnie, Marysia ma ubaw, ale nie do końca. Liliana nie ma może najbardziej gadane ani najlżejszego pióra, ale Pustak pomógł napisać kąśliwy artykuł. Aha, jest tam też wywiad z "tajemniczym dżentelmenem (Marysia poznała po charakterze mowy że to DJ Babu) który powiedział, że Marysia wydała w niewolę młodego arystokratę lokalnego." (bo Karo oddała Stasia Arienika mamie)

Marysia się nie przejmowała aktem. Nie przejmuje się też "Plotkami Royalsów".

Tymczasem Marysia kontaktuje się z Hestią. Pyta ją - Tinder dla Myrczka. Hestia spojrzała na Marysię ponuro. Tak. Łypnęła okiem z monitora. "Tien Sowińska, wykonam każde żądanie. Nawet jeśli niemożliwe." Hestia zadała proste pytanie - jak daleko może się posunąć? By znaleźć Myrczkowi dziewczynę? "Jakie mam autoryzacje i ile mogę wydać?". Marysia spojrzała na Hestię "zrób research". Hestia przyjęła... i zaproponowała kilka opcji:

1. Zakup młodej damy z Eterni. Lub z sierocińca. Lub od mafii. Plus pensja.
2. Miragent.
3. Pensja. Dla młodej damy z Aurum.

Hestia rekomenduje wariant 1. Wyjdzie taniej. I będzie przekonywująco. Marysia poprosiła o opcję 4. Hestia spytała co jest nie tak z miragentem? "Może się kapnąć że to nie człowiek". Hestia się zastanowiła. Marysia: "znasz Myrczka. Powiem Ci, czemu szukam...". Hestia na to: "Nie znam Myrczka. Jest poza zakresem Dzielnicy Rekinów. Jest uczniem AMZ. Nie jest dla mnie interesujący. Mogę się nim zainteresować, jeśli taka Twoja wola, ale jedyne co wiem to jego ogólną niekompetencję w każdym zakresie nie będącym grzybami."

Marysia powiedziała, że Myrczek zakochał się w Sabinie Kazitan. Hestia stwierdziła, że to nie ta liga. Hestia ma GDZIEŚ moralność Sabiny. Hestia dostała od Marysi praktycznie niemożliwe zadanie - znaleźć Myrczkowi dziewczynę, której nie trzeba kupować, importować i która nie jest miragentem.

Hestia stwierdziła, że potrzebna jej większa moc psychotroniczna. Ale się tym zajmie. ZNAJDZIE dziewczynę dla Myrczka.

Ex (nie zna Myrczka + to ciężki przypadek) +2:

* V: Hestia zamigotała. System uległ częściowemu obciążeniu. Hestia pracuje. Będzie odpowiedź (na tej sesji).

Tymczasem, Karolina ma wiadomość hipernetową. Od Chevaleresse. "To pomożesz mi wyciągnąć Melissę?" Karo "the fuck you're talking about? XD". Chevaleresse, ze spokojem "koleżanka? Kult Ośmiornicy? U Was? Porwiemy ją?" Karo: "od biedy. Masz dowody?" Chevaleresse: "nie ma upodobania do dziwnych czapek... dlatego tak trudno ją wykryć. Ale WIEM że jest w Kulcie. Widziałam ją tam."

Karo pomoże, ale grają jak Karo chce. Chevaleresse się zgadza. Ch: "Kult u Was byłby groźny.". Karo też nie chce kultu, ale Chevaleresse nie słynie ze szczerości plus ma kiepskie dowody. Tzn. nie ma.

Karo -> Daniel. "Jest sprawa. Nie mam dowodów. Ale gdzieś tu jest kult ośmiornicy." Daniel się ucieszył myśląć o dziewczynach a Karo zripostowała o ssaniu macki. Daniel się wzdrygnął. Karo przekonała Daniela, że kraloth to nieuczciwa konkurencja i jednak trzeba dziewczyny ratować bo wymrą. Daniel z cholernie ciężkim sercem myśli co dalej. Powiedział, że znalazł Melissę - jest u Santino.

Tr (bo jest facetem i lubi Santino) Z (pomożemy rodowi, pomożemy niewinnej dziewczynie) +3:

* X: Daniel powie Karo, ale nie utrzyma tego w sekrecie przed Santino. Tzn. że ON się interesuje Melissą.
* V: Daniel powiedział wszystko Karo. W jednym z bogatych apartamentów jest Melissa i kilka lasek. Kilka jest też w innym apartamencie. I nie wie, by Santino chodził do tego innego. Daniel udaje że nie wie. Ale wie.

Karo udaje się do Sensacjusza. Ten powiedział - daj ofiarę, będzie glizda. Nie wie w 100% jak to ruszyć, ale z glizdą da radę.

Marysia natychmiast zażądała, by zamówić dla Hestii:

* wzmocnienie psychotroniczne
* dodatkowe drony
* wszystkie-inne-rzeczy by mogły działać poprawnie.

Marysia chce mieć dobry monitoring terenu. Hestia powiedziała, że ma dla niej nagranie od swojej poprzedniczki. Czy Marysia chce je odsłuchać.

Amelia. "Administratorko." - pogardliwie - "widzę, że próbujesz wzmocnić możliwość monitorowania Rekinów. Ostrzegam Cię przed tym, bo Rekiny tego nie lubią. Państwo policyjne i te sprawy. Ale proszę bardzo, rób co chcesz. Ty tu dowodzisz. Ja nie musiałam takich rzeczy robić i wszystko działało perfekcyjnie" - Pogardliwy uśmiech i zniknęła.

Marysia kazała Hestii zamówić wszystko co trzeba do zwiększenia mocy. Sukcesywnie, na przestrzeni 2 miesięcy. I niech się pojawia. By nikt się nie zorientował.

ExZ (Amelia i to jak ona nauczyła Hestię) +2:

* X: Hestia jest dużo lepsza w ukrywaniu danych niż powinna (lekkie Skażenie ixiońskie przez Trzęsawisko)
* V: Dojdzie do tego, że Hestia będzie miała wzmocnienie jakie Marysia chce.
* X: Sowińscy są niezadowoleni, uważają to za błąd - wyślą kuzyna Jeremiego by pomógł Marysi.
* zostaje otwarte, czy Sowińscy uznają to za błąd i czy Rekiny się dowiedzą.
* V: Hestia za to polubiła Marysię. Marysia jej coś dała. Marysi zależy na tym, by Hestia była silna i skuteczna.
* V: Hestia AKTYWNIE sprawia, by Ernest zobaczył, że Marysia jest lepsza niż Amelia.
* Vz: Hestia jest AKTYWNYM sojusznikiem Marysi. Przeniesienie ixiońskie Marysia -> Hestia (przez kontakt z Marysią Hestia staje się bardziej jak Marysia).
* X: Rekiny się dowiedzą, że Marysia stoi za "państwem policyjnym" w Dzielnicy Rekinów
* Xz: Amelia Sowińska odzyskała częściowo kontakt z Aurum i Rekinami. Przez to, że chcieli się skonsultować z nią wrt Hestii itp.

Marysia i Hestia miały owocny okres. TYMCZASEM Karo. -> Ernest, Keira. Karo wyjaśniła Ernestowi problem.

Ernest stwierdził, że ma bardzo prosty plan. Nie ma tam nikogo, do kogo należy Apartament. Więc on robi ćwiczenia - Keira vs oddział. I Keira wpada tam pierwsza. Znajduje orgię kralotyczną (lub nie) i atakuje pierwsza, koniecznie raniąc 1-2 osoby. Potem "ojej, pomyliłam się, idziemy do lekarza" i zajmuje się tym Sensacjusz.

Pytanie brzmi - KTÓRY apartament? Są DWA, gdzie znajdują się niezidentyfikowane osoby. Jeden z nich jest pod bezpośrednią opieką tien Mimozy Diakon. Drugi - niby nikogo tam nie ma. I zgodnie z tym co mówi Hestia, apartament Mimozy jest na 100% legitny. Tylko ludzie Mimozy się tam kręcą normalnie. Ale Mimoza Elegancja tam rezyduje - to jej apartament.

Plan Ernesta zadziałał - Keira wpadła, nie działo się nic zdrożnego, ale były feromony kralotyczne. Keira doprowadziła do serii ran i szkód. Sensacjusz miał zabawę z glizdą.

Reputacja Ernesta / Keiry u Rekinów:

ExZ (kult ośmiornicy wykryty) +4 (Karo bierze na siebie że ona wybrała miejsce):

* Vz: rozmyło się. Keira is a menace, ale się rozmyło.
* X: Mimoza jest zaniepokojona. Musi zadziałać, by Ernest nie zrobił wjazdu na jej apartament.
* X: Arkadia też wraca, zrobić porządek z Keirą i sprawdzić WTF kult ośmiornicy (Marysia dowodzi, ma wszędzie oczy i tu KULT? HMMM...). 

## Streszczenie

Konflikt Liliana - Marysia się zaostrza. Marysia skupia się na wzmocnieniu Hestii i uzyskaniu kontroli nad Rekinami - przejęła Hestię jako sojuszniczkę. Chevaleresse poprosiła Karolinę o uratowanie Melissy i Karo faktycznie pomogła - Ernest wysłał Keirę i odbili Melissę. To sprawiło, że Mimoza Diakon poczuła się zagrożona i weszła do akcji. Jakby tego było mało, Sowińscy wysyłają Jeremiego by opanował kuzynkę.

## Progresja

* Marysia Sowińska: Rekiny widzą, że Marysia stoi za "państwem policyjnym" wśród Rekinów. 
* Hestia d'Rekiny: polubiła Marysię i jest jej AKTYWNYM sojusznikiem, wspiera ją w celach -> Ernest x Marysia. 
* Hestia d'Rekiny: będzie wzmocniona siłami Sowińskich. Jest wzmocniona ixiońsko i Skażona ixiońsko. Lepsza i inteligentniejsza niż się wydaje.
* Hestia d'Rekiny: SUPERTRUDNE ZADANIE - znaleźć dla Myrczka dziewczynę, która nie jest Sabiną Kazitan. Opłata? Zakup z Eterni? Miragent?
* Amelia Sowińska: odzyskała częściowo kontakt z Rekinami i Aurum (bo konsultowali się wrt Hestii itp)
* Mimoza Elegancja Diakon: silnie zaniepokojona działaniami Ernesta wrt wjazdu na apartament czy jego samowolą. Mimoza przeszła na tryb bojowy / defensywny.
* Keira Amarco d'Namertel: Arkadia Verlen wraca zrobić z nią porządek. Ernest zbyt się szarogęsi.

### Frakcji

* .

## Zasługi

* Marysia Sowińska: zamawia wzmocnienia dla Hestii, znajduje dziewczynę dla Myrczka i doprowadza do silnej korupcji Hestii - Hestia jest jej przyjaciółką i sojuszniczką teraz.
* Karolina Terienak: pod namową Chevaleresse zwalcza Kult Ośmiornicy wśród Rekinów; zlokalizowała gdzie jest Melissa (u Santino) i współpracując z Ernestem przechwyciła Melissę.
* Hestia d'Rekiny: weszła w ścisłą współpracę z Marysią. Szuka dziewczyny dla Myrczka. Zostaje solidnie wzmocniona.
* Jeremi Sowiński: wysłany przez Sowińskich na teren Rekinów z uwagi na to, jak bardzo Sowińscy są niezadowoleni z działań Marysi. W DRODZE przez pewien czas.
* Liliana Bankierz: twórca strasznych ulotek "Marysia Sowińska wróg wszelkiej wolności i podliz". Zrobiła artykuł w "Plotkach Royalsów" Pustaka.
* Diana Tevalier: nie ustąpiła w sprawie Melissy; doprowadziła do tego że Karolina przechwyciła Melissę i rozbiła Kult Ośmiornicy. Rękoma ETERNI.
* Ernest Namertel: zaplanował, przekonany przez Karo, odbicie Melissy z potencjalnego kultu jako ćwiczenia. Wysłał jedną Keirę.
* Keira Amarco d'Namertel: podczas "ćwiczeń" zaplanowanych przez Ernesta by wyciągnąć "przypadkiem" Mimozę wpadła, załatwiła kilku magów, pokonała feromony kralotyczne i wyciągnęła Melissę.


### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Akademia Magii, kampus
                                    1. Arena Treningowa
                                    1. Akademik
                                    1. Las Trzęsawny
                            1. Podwiert
                                1. Dzielnica Luksusu Rekinów
                                    1. Fortyfikacje Rolanda
                                    1. Serce Luksusu
                                        1. Lecznica Rannej Rybki
                                        1. Apartamentowce Elity
                                        1. Fontanna Królewska
                                        1. Kawiarenka Relaks
                                        1. Arena Amelii
                                    1. Obrzeża Biedy
                                        1. Hotel Milord
                                        1. Stadion Lotników
                                        1. Domy Ubóstwa
                                        1. Stajnia Rumaków
                                    1. Sektor Brudu i Nudy
                                        1. Komputerownia
                                        1. Skrytki Czereśniaka
                                        1. Magitrownia Pogardy
                                        1. Konwerter Magielektryczny

## Czas

* Opóźnienie: 4
* Dni: 3

## Konflikty

* 1 - Hestia szuka dziewczyny dla Myrczka
    * Ex (nie zna Myrczka + to ciężki przypadek) +2 V
    * Hestia da odpowiedź
* 2 - Karo przekonuje Daniela, że kult macki jest zły i niech powie, co wie lub poszuka jak nie wie
    * Tr (bo jest facetem i lubi Santino) Z (pomożemy rodowi, pomożemy niewinnej dziewczynie) +3 XV
    * Daniel powie Karo, ale Santino się dowie, że Daniel się interesuje. Daniel powiedział wszystko Karo. W jednym z bogatych apartamentów jest Melissa i kilka lasek. Kilka jest też w innym apartamencie. I nie wie, by Santino chodził do tego innego. Daniel udaje że nie wie. Ale wie.
* 3 - Marysia dyskretnie zamawia wzmocnienia dla Hestii
    * ExZ (Amelia i to jak ona nauczyła Hestię) +2 XVXVVVzXXz
    * Hestia dużo lepsza niż powinna być w ukrywaniu (lekkie skażenie ixiońskie. Hestia będzie miała wzmocnienie jakie Marysia chce.Sowińscy są niezadowoleni, uważają to za błąd - wyślą kuzyna Jeremiego by pomógł Marysi. Hestia polubiła Marysię. Hestia AKTYWNIE sprawia, by Ernest zobaczył, że Marysia jest lepsza niż Amelia. Hestia jest AKTYWNYM sojusznikiem Marysi. Rekiny się dowiedzą, że Marysia stoi za "państwem policyjnym" w Dzielnicy Rekinów. Amelia Sowińska odzyskała częściowo kontakt z Aurum i Rekinami.
* 4 - Ćwiczenia Ernesta -> Reputacja Ernesta / Keiry u Rekinów:
    * ExZ (kult ośmiornicy wykryty) +4 (Karo bierze na siebie że ona wybrała miejsce) VzXX
    * Keira is a menace, ale się rozmyło. Mimoza jest zaniepokojona. Musi zadziałać, by Ernest nie zrobił wjazdu na jej apartament. Arkadia wraca, zrobić porządek z Keirą i sprawdzić WTF kult ośmiornicy 