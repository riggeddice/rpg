## Metadane

* title: "Aleksandria Arkologii Jabłuszko"
* threads: brak
* motives: the-collector, the-aspirant, birutan, kurator-aleksandrii
* gm: żółw
* players: dimzard, popy, izabela_od_gier

## Kontynuacja
### Kampanijna

* [241219 - Aleksandria Arkologii Jabłuszko](241219-aleksandria-arkologii-jabluszko)

### Chronologiczna

* [241219 - Aleksandria Arkologii Jabłuszko](241219-aleksandria-arkologii-jabluszko)

## Plan sesji
### Projekt Wizji Sesji

* INSPIRACJE
    * PIOSENKA WIODĄCA
        * [The Princess (Princess of Ice Remix)](https://www.youtube.com/watch?v=IjTtCwoFta0)
            * "She is a princess and her heart is made of ice | But there is no coldness in her soul | She tries to heal my eternal pain" 
            * "She is a princess in a world so full of tears | But there is no bitterness in her words | She takes my hand to show me love"
            * Edmund Lichter, kiedyś salvager a teraz Kurator. "Wszyscy muszą zostać uratowani".
            * Birutan, agent Aleksandrii, który jest Kuratorem a nie birutanem.
    * Inspiracje inne
        * brak
* Opowieść o (Theme and Vision):
    * "_Pod powierzchnią Neikatis można znaleźć różne rzeczy - mniej lub bardziej niebezpieczne i wartościowe..._"
        * Lichter był salvagerem; znalazł Aleksandrię. On jako jedyny przeżył i teraz został lekarzem.
    * "_Magia sprawia, że stajesz się czymś MNIEJ. Nie jesteś już osobą. Jesteś wyrazicielem idei._"
        * Lichter, Kurator, nie jest w stanie nawet się bronić.
        * Mimo, że optimum byłoby zachować ciszę i się nie ujawniać, Aleksandria będzie chronić ludzi.
    * Lokalizacja: Neikatis, nieznany Dystrykt
* Motive-split
    * the-collector: Aleksandria d'Jabłuszko. Jej celem jest złapanie i danie Ukojenia wszystkim ludziom, którzy mieszkają w Arkologii. Działa inteligentnie, przez agentów.
    * the-aspirant: Kevlaryci próbują przejąć władzę nad Arkologią. 
    * birutan: zarówno ci prawdziwi, stojący na drodze salvagerom jak i ten sztuczny, Kurator kontrolowany przez Aleksandrię.
    * kurator-aleksandrii: Aleksandria d'Jabłuszko oraz birutan, który kiedyś był salvagerem a został lekarzem.
* O co grają Gracze?
    * Sukces:
        * Rozwiązać Aleksandrię
    * Porażka: 
        * Sporo ludzi którzy są z Wami powiązani w Arkologii ucierpi lub straci majątek.
        * Jeśli zadziałacie źle, stracicie reputację i majątek. Możecie zostać pariasami.
        * Ludzie będą dalej znikali. Mogą być jakieś zamieszki, ale Arkologia NIE zostanie zniszczona.
        * Wy zawsze możecie uciec do innej Arkologii i zacząć od nowa, zostawiając tu wszystko i wszystkich.
* O co gra MG?
    * Highlevel
        * ?
    * Co uzyskać fabularnie
        * Kevlaryci przejmują Arkologię korzystając z Birutana
* Agendy
    * .

### Co obiecałem

#### TLDR

KONTEKST SESJI:

- **Planeta Neikatis**: Terraformowana planeta przypominający Marsa, z zimnymi piaskami, burzami magicznymi i "cyberzombie" zwanymi birutanami.
- **Społeczeństwo**: Ludzie żyją w arkologiach (kopułowe miasta, np. Wasza to Jabłuszko – populacja ~8,000), unikając niebezpiecznej powierzchni.
- **Postaci Graczy**: Salvagerzy - zgrana grupa poszukiwaczy przemierzająca pustynie pojazdem (Skorpionem), działająca na rzecz Jabłuszka, często wspierająca lokalną społeczność.
- **Ostatnie wydarzenia w Jabłuszku**:
  - Powrót graczy do Arkologii; panika po zawaleniu się fragmentu kopuły.
  - Świadkowie donoszą o obecności infiltrującego Arkologię birutana, co odbiega od ich zwykłych zachowań.
  - Obrona arkologii osłabiona, a ludzie przerażeni.
  - Ostatnio zaginęło kilka osób, w tym bliska postaciom osoba.

Domyślna Przyszłość (nic nie zrobicie):

- Znajomi i bliscy w Jabłuszku ucierpią lub stracą majątek.  
- Zniknięcia mieszkańców będą się nasilać.  
- Mogą wybuchnąć zamieszki, choć arkologia jako całość przetrwa.  
- Ryzyko utraty reputacji i majątku. Możecie stać się pariasami.  
- Macie opcję ucieczki do innej arkologii, ale wiąże się to z porzuceniem dotychczasowego życia i relacji.

Wasze postacie:

- **Inspiracja**: Załoga z **Alien 4** lub **Firefly** - barwna, kompetentna grupa zróżnicowanych osobowości.  
- **Rola w załodze**: Każda postać ma konkretną funkcję w operacjach salvagerskich (np. technik, zwiadowca, mechanik).
- **Kompetencje**:
  - Postacie są zdolne do walki i przetrwania w trudnych warunkach.
  - Kompetentne w swoim fachu – osoby nieprzydatne nie miałyby miejsca w grupie.
- **Charakter**:
  - Postacie, którym gracze chcą kibicować.  
  - Brak odpychających cech, jak przemoc wobec bezbronnych czy jawna agresja wobec silniejszych.
  - Nie są głupie i nie zachowują się głupio, ale mogą mieć wady.
- **Motywacje**:
  - Zależy im na innych członkach Skorpiona i na ich Skorpionie.
  - Powiązania w Arkologii Jabłuszko - znajomi, przyjaciele lub rodzina. To ich dodatkowa motywacja.
- **Imiona**: W stylu polskim lub włoskim, pasujące do świata.

Detale - Konwencja, Styl Prowadzenia, Kontrakt - w wątku.

#### 1. Opis sesji:

**Kontekst lokacji i Pozycjonowanie postaci**:

Planeta Neikatis (jak Mars w drugim stadium terraformacji), planeta zimnych piasków, strasznych burz piaskowych i magicznych, powierzchnię której przemierzają cyberzombie zwani birutanami. Ludzie mieszkają w arkologiach - dość samowystarczalnych miastach pod kopułami - i prosperują unikając wychodzenia na niebezpieczną powierzchnię Neikatis.

Wy jesteście jedną z grupek salvagerów Neikatis - zgraną grupką jeżdżącą pustynnym pojazdem klasy Skorpion - luźno powiązanymi z arkologią Jabłuszko (ok. 8_000 populacji). Przemierzacie okolice Jabłuszka by znaleźć wartościowe rzeczy pojawiające się po burzy magicznej, które są skupowane przez Jabłuszko. Macie tu znajomych, przyjaciół i niejednokrotnie udzielaliście pomocy jak i ją otrzymywaliście. Życie jako pionier wymaga współpracy z innymi, nawet, jeśli się niekoniecznie trzeba lubić.

Jest to twarde, brudne, niebezpieczne życie - ale jesteście wolni i jesteście władcami swojego losu.

Kilkadziesiąt kilometrów od Jabłuszka odkryto gniazdo birutan, w którym znajdują się cenne przedmioty i anomalie. Niestety, dwie grupki salvagerów próbowały już dostać się do tych anomalii i zapłacili za to życiem. Ich Skorpiony zostały przejęte przez birutan i stanowią teraz zagrożenie. Chcielibyście się dobrać do tego gniazda, ale nie do końca macie jak. Rosnąca siła birutan w okolicy martwi starszyznę Jabłuszka, ale nie ma ani siły ognia ani pomysłów jak to rozwiązać.

**Kontekst konkretnych wątków sesji**:

Gdy wracaliście z jednej z akcji, wróciliście do Arkologii ogarniętej paniką. Fragment Arkologii się zawalił i świadkowie przysięgają, że widzieli birutana w Arkologii Jabłuszko, który najpewniej stał za tą katastrofą. Arkologia chwilowo straciła jedno ze swoich działek obronnych i jest zagrożona potencjalnym atakiem.

Nikomu nie pasuje to do typowego działania birutan; zwykle nie mają tak zaawansowanych planów. Jednak ludzie są przerażeni a fakty świadczą, że coś niewłaściwego się dzieje w Arkologii. Przykładowo, kilka osób zniknęło w tajemniczych okolicznościach; jedną z tych osób znaliście.

Mimo, że siły porządkowe Arkologii Jabłuszko zwykle są kompetentne, zdecydowaliście się wziąć sprawy w swoje ręce. Tym bardziej, że sprawa dotyczy bezpośrednio bliskich Wam osób.

#### 2. Parametry techniczne sesji:

* **Czas sesji**: 210 minut, z możliwością przedłużenia do 240 minut.
* **Ilość graczy**: 2-5.
* **Struktura sesji**
    * Sesja jest dość otwarta, bo to miejsce jest żywe i dużo się tam dzieje.
    * Ja zakładam, że spróbujecie dojść do tego, co się dzieje z tym birutanem (bo dotyczy to Waszego domu oraz: zniknięcia + świadkowie dotyczą ludzi których znacie), ale nie będę naciskał.
        * Wtedy jednak Waszą odpowiedzialnością jest stworzenie własnego wątku lub pójście za innym wątkiem. 
            * Każda sesja to film a film musi być "o czymś". Nie jest akceptowalna "eksploracja mająca za cel tylko eksplorację".
            * Wątek oznacza: "Świat teraz wygląda TAK. Jeśli wygram, zmieni się TAK".
        * Przykładowe jawne Wątki Sesji w chwili obecnej: "co się dzieje w Jabłuszku", "tajemniczy birutan", "pozyskanie większej ilości zasobów", "pomoc komuś wartemu pomocy", "pognębić rywala", "potężne gniazdo birutan".
    * Gwarantuję **co najmniej** 20 nazwanych NPC o różnych osobowościach. Obecność nazwanego NPCa nie oznacza, że jest ważny. Jest sporo wątków. Wszystko czego nie zrobicie odbędzie się "w tle".
* **Domyślna Przyszłość - co jeśli nic nie zrobicie lub zawiedziecie**
    * Sporo ludzi którzy są z Wami powiązani w Arkologii ucierpi lub straci majątek.
    * Jeśli zadziałacie źle, stracicie reputację i majątek. Możecie zostać pariasami.
    * Ludzie będą dalej znikali. Mogą być jakieś zamieszki, ale Arkologia NIE zostanie zniszczona.
    * Wy zawsze możecie uciec do innej Arkologii i zacząć od nowa, zostawiając tu wszystko i wszystkich.

#### 3. Konwencja

Typ: **Neikatiańska Wola Przetrwania, podtyp: otwarte śledztwo**

* **Świat**: Eteryczne Zaćmienie, sci-fi + magia. Jesteście na Neikatis, planecie lodowatych piasków i burz magicznych.
* **Typ sesji**: 
    * Śledztwo z kilkoma poziomami, ale w odróżnieniu od śledztwa nie daję Wam gwarancji na dojście do prawdy. 
    * Macie wiele wątków, co podniesiecie to pozyskacie. Rekomenduję temat birutana, ale nie będę Was zmuszał.
    * Więcej śledzenia, infiltracji, rozmawiania i negocjacji niż walki. Ale starcia z birutanami są możliwe. Rzadsze starcia z ludźmi.
* **Media podobne do sesji**
    * Agregacja: Archiwum X, S.T.A.L.K.E.R., Firefly
    * Z tym wyjątkiem, że tu raczej nikt nie ginie. Za mała populacja ludzka i ludzie są zbyt wartościowi.
* **Letalność i Pozycjonowanie**: 
    * Wasze postacie nie zginą i szczególnie nie ucierpią jeśli nie zrobią nic głupiego.
    * Jesteście małymi rybkami w średnim jeziorze, ale macie dobrą reputację i nie macie wrogów. Rywali - tak. Wrogów - nie. Podchodzi się do Was pozytywnie.
    * Jeśli komuś niewłaściwemu **naprawdę** podpadniecie, nie dojdzie do walki. Podłożą bombę pod Waszego Skorpiona i zginiecie bez możliwości reakcji.
        * W małych, dość samowystarczalnych społecznościach jednostki szkodliwe dla społeczności są zwykle reedukowane trotylem.
    * Ludzie są wartościowym zasobem. Jeśli kogoś zabijecie / okaleczycie bez zgody odpowiednich sił lub nie w samoobronie, patrz punkt powyżej.
* **Styl prowadzenia**
    * Gramy intencyjnie. Póki wszyscy nie rozumiemy co akcja ma zrobić w fikcji, akcja nie wejdzie do fikcji.
        * Nie da się "przypadkiem" zepsuć sobie sesji czy postaci; to musi być świadoma decyzja.
    * Będę Wam przerywał, jeśli nie rozumiem do czego dane działanie / opis ma prowadzić. 
        * Każde słowo Gracza / MG ma prowadzić do czegoś. Nie ma "pustych słów"; jeśli nie wiem po co coś jest powiedziane, będę pytał.
    * Będę Was ostrzegał, jeśli Wasze ruchy są niezgodne z zasadami EZ lub Jabłuszka (bo postacie wiedzą).
        * Pozwolę Wam zrobić co chcecie, ale będę bezwzględnie egzekwował logikę świata i Wasze pozycjonowanie.
        * Jeśli np. wpakujecie się w niefortunną sytuację poprzednimi decyzjami, możecie skończyć w sytuacji, w której (X) zaczną przesuwać fabułę w stronę dla Was mało akceptowalną. 
            * Wniosek - nie wpakujcie się w niefortunną sytuację poprzednimi decyzjami. Będę próbował Was osaczyć.
    * Jeśli uważam deklarację za nierealistyczną w kontekście, będę pytał "pokaż mi ciąg logiczny który prowadzi do tego, że TO jest możliwe dla tych postaci w tej sytuacji"
    * **Logika świata i fikcji ma pierwszeństwo nad 'rule of cool'**

#### 4. Wasze Postacie

* Myślcie jak o: załoga statku w **Alien 4**, załoga statku **Firefly**. Barwna gromadka, kompetentna, nie zawsze idealna, nie jesteście głodni ale nie żyjecie w dobrobycie.
    * Każda postać "coś robi" w operacji salvagerskiej na Skorpionie (nie musicie DOKŁADNIE wiedzieć co, ale nie możecie mieć postaci która tam się nie nadaje). 
    * Każda postać jest uzbrojona i umie walczyć.
    * Postacie winny być kompetentne (niekompetentne by zostały w Arkologii lub zginęły).
    * Postacie winny być takie, byśmy my chcieli im kibicować.
    * Żadna postać nie może wzbudzać odrazy innych osób siedzących przy stole (np. nie ma opcji "gram kimś kto lubi znęcać się nad dziećmi").
* Osoby, które _przetrwałyby_ w takich warunkach społecznych i kulturowych jakie podałem.
    * Czyli ktoś kto losowo "daje w mordę" losowym napotkanym osobom odpada.
    * Czyli jeśli coś Wam nie pasuje, raczej podkopujecie ich dyskretnie niż jawnie zaczepiacie silniejszych od siebie.
* Postacie mają imiona w konwencji polskiej lub włoskiej.
* Każdej postaci zależy na innych postaciach i Waszym Skorpionie. Inaczej te postacie opuściłyby tą grupę i przeszłyby do innego Skorpiona.
* Każda postać ma znajomości, przyjaźnie lub rodzinę w Arkologii Jabłuszko. Macie powiązanie z różnymi osobami w Jabłuszku.

#### 5. Kontrakt i zasady gry

Zasady:

* Nie musicie nic wiedzieć o świecie, Skorpionach, operacjach złomiarskich itp. Nic nie musicie w ogóle wiedzieć.
* Jeśli zrobicie nietragiczne postacie i gracie zgodnie z Konwencją, jesteście w stanie wygrać i nic ekstra nie musicie.

Triggery:

* Triggery:
    * niewolnictwo, lodowa pustynia, głód, traktowanie ludzi jak zasoby, drapieżny kapitalizm, transformacja ludzi wbrew ich woli, organizacje przestępcze, aspekty kanibalizmu, magia, recykling, fizyka i logika
* Wykluczenia (NIE BĘDZIE tych rzeczy poniżej na sesji):
    * cierpienie zwierząt, cierpienie nieletnich, tematy erotyczne, nic-z-Triggerów-powyżej aplikowanego do Postaci Graczy bez woli Graczy, dokładnych opisów powyższego; raczej suche fakty.
* Oczywiście jest możliwość dodania własnych Triggerów i wyłączeń. Jeśli o czymś nie pomyśleliście i pojawi się na sesji, przerwijcie i dodamy nowe Wykluczenie.

Inne zasady:

1. Na sesji "podsłuchiwanie" (pasywne uczestnictwo nie będąc w roli Gracza) występuje wtedy i tylko wtedy jeśli:
    1. mam entuzjastyczną akceptację wszystkich graczy. Nie "no ok" a "chcę by to się stało", bez jakiejkolwiek presji zewnętrznej.
    2. osoby podsłuchujące nie mają kamer i są wyciszone.
2. Jeśli dowolna osoba przy stole czuje się niekomfortowo **z jakiegokolwiek powodu**, ma prawo przerwać sesję. Pozostałe osoby przy stole winny przyjąć to ze zrozumieniem i wspólnie rozwiążemy sprawę. 
    1. Coś ważnego dla jednej osoby nie może być uznane za błahe przez nikogo.
3. Gracze "pasywni" (nie wykonujący proaktywnych akcji) mogą się liczyć z tym, że w którymś momencie MG przekieruje na nich spotlight i wymusi przeprowadzenie akcji pod kątem intencji.
4.  Wspólnie budujemy opowieść; MG nie wie jak sesja się skończy.
5.  Szanujemy wszystkich Graczy i KAŻDY Gracz ma miejsce na zmianę rzeczywistości. Nie przeszkadzamy sobie.
6.  System intencyjny - nie pytam "co robisz" a "co chcesz osiągnąć". Puste akcje nie wchodzą do gry.
7.  Gracze pchają akcję, Mistrz Gry reaguje na działania Graczy. 
8.  Wyłączone PvP (poza intencyjnym), postacie Graczy (zwykle) nie giną, ogólnie mało "walki".
9. Każdy konflikt, który nie był negocjowany przez Graczy jest uznany za **entuzjastycznie przyjęty** przez wszystkich Graczy.
    1. Gracz, którego postać nie jest w konflikcie ma prawo podnieść, że dany (X) jest dlań nieakceptowalny.


### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

KONTEKST:

.

CHRONOLOGIA:

1. 20 lat temu znika Skorpion z Edmundem i Dawidem Rekińczykiem. Cała ekipa napotkała w piaskach Neikatis niesprawną Aleksandrię i Aleksandria ich porwała i zasymilowała - poza Edmundem, który został Kuratorem.
2. Edmund wraca do Arkologii, zostaje lekarzem. Dodaje ludzi (umierających i chętnych) do Aleksandrii używając swojego Skorpiona-Widmo.
3. Starszyzna się dowiaduje, akceptuje, bo Edmund jest coraz bardziej kompetentny i pomocny. Starszyzna nawet naprawia Skorpiona-Widmo, by proceder mógł działać dalej i wycisza sygnały od obywateli którzy coś wiedzą.
4. Po latach, Aleksandria konstruuje nie-birutana z Dawida Rekińczyka, kiedyś przyjaciela Edmunda; Dawid ma pomagać w transporcie ludzi.
5. Dochodzi do katastrofy konstrukcyjnej, Dawid ratuje grupę losowych ludzi w Arkologii.
6. 5 lat temu w okolicy 09bd7 (Dziewiąta Beta Delta Dziewięć) odkryto potężne gniazdo birutan.
7. 2 lata temu ściągnięto tu Kevlarytów. Grupa najemników ściągnięta, by zniszczyć 09bd7. Nie udało się, zostali złamani. Zostali jako defensywa i nie do końca mają jak się wycofać.
8. Wchodzą postacie Graczy

PRZECIWNIK:

* Kevlaryci, próbujący przejąć władzę i dochody z Jabłuszka korzystając z sytuacji
* Doktor Lichter, wzmacniający Aleksandrię by nikt nie umierał

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

FAZA 0: TUTORIAL

* Tonalnie
    * Możliwość pozyskania rzeczy
    * Niebezpieczeństwo, risk-reward
* Wiedza
    * Pokazać birutan i niebezpieczeństwa z piasków Neikatis.
    * Pokazać czemu birutanie są tak niesamowicie niebezpieczni.
    * Pokazać, że dwa Skorpiony sobie radzą, każdy chroni każdego.
    * Pokazać, że da się pozyskać wartościowe anomalie i zamknąć je w pudełku.
* Kontekst postaci i problemu
    * Każda postać jest w stanie robić różne rzeczy; od badania, szukania po walkę.
    * Każdy pomaga każdemu, każdy liczy na każdego.
    * "Szaman ostrzegał, że tu może być bardziej niebezpiecznie".
* Implementacja
    * Prosta anomalia; jedna osoba odwraca uwagę, druga zamyka w pudełku próżniowym.
    * Atak birutan spod piasków.
    * Pokaż: dwa Skorpiony, Birutanie wychodzący z piasku, jak trudno ich zabić i jak są groźni.

FAZA 1: OTWARTA sesja

Strony:

* **The Collector** nie dopuszcza do śmierci ludzi i pomaga komu jest w stanie. Pragnie pozyskać jak najwięcej ludzi do Aleksandrii.
* **The Protector** nie ma aktywnych Ruchów. Ratuje ludzi i im pomaga przed krzywdą jak może. Slave to the Alexandria.
* **The Aspirant** jest tu definiowany przez Kevlarytów. Próbują przejąć kontrolę nad Jabłuszkiem.
* **The Survivor** to lokalna starszyzna. Uważają Aleksandrię za wartościową i dyskretnie pomagają Lichterowi.

Akcje:

nieistotne

## Sesja - analiza
### Fiszki
#### Skorpion Waternis

* .Kamil Waternis:
    * salvager Waternis (owner, analityk / radar / hacker) faeril: łagodny, ale pracowity; zawsze ryzykuje dla innych
    * Zbieracz Rozgwiazd
* .Zofia Waternis: 
    * (owner, pilot / heavy stuff / appraisal ) faeril: skupienie na przychodach i miłość do sprzętu | Lancer z pochodnią plazmową
    * "Wszystko da się kontrolować lub spalić"
* .Michał Kardok: 
    * salvager Waternis (hired muscle / engineer) faeril: robota czeka.
    * "Jak masz czas gadać to masz czas pracować"; twardy, nie okazuje słabości
* .Czesiek Myszopalczasty: salvager Serien (hired muscle / combat) faeril: transformacje są złe
    * "Nienawidzę tych wszystkich anomalicznych gówien..."; pomocny, dobrą rękę poda

### Scena Zero - impl

Rowena, Aurora, Betta.

Rowena -> skupia się na homo superior, dlatego prowadzi badania na birutanach. Ich grupa nazywa się "Księżniczki", sam Skorpion nazywa się "Wasza Wysokość".

Dwa współpracujące Skorpiony z anomalią. Po tym jak Rowena wysłała latającego drona, zobaczyła ukrytych birutan pod piaskami gdy Zofia robiła salvage.

Tr Z +2:

* Vz: Skorpion z Bettą bierze ogień na siebie.
* X: Informacja przyjdzie nieco później.
* V: BIRUTANY!
* X: Uszkodzenie pancerza Skorpiona
* Vg: Zofia spacyfikowała i zneutralizowała anomalię.
* X: Poważnie uszkodzony Skorpion - łopata.
* Vr: Zofia złapała się, utrzymała i ZJECHALIŚCIE ze wzgórza birutan.

Sukces w ewakuacji Zofii.

* Kamil: Dzięki, Anomalia jest Wasza. Wielkie dzięki. Przepraszamy.

Ze wzgórza wyskakują birutany i Was gonią. Ale są za wolne i udało się uniknąć pułapki.

Macie przyjaciół.

### Sesja Właściwa - impl
#### PYTANIA GENERATYWNE

* Betta, która frakcja chce Cię zrekrutować najbardziej?
    * **Kevlaryci**, najemnicy którzy zaatakowali gniazdo Birutan i zostali rozbici; liżą tu rany i myślą co dalej?
    * **Starszyzna**, która chciałaby byś dołączyła do sił obronnych arkologii?
    * **Bazar Jabłuszko**, który próbuje utrzymać pozycję w świetle zbliżających się Kevlaryckich zapędów?
* Rowena, z kim masz najlepsze relacje?
    * **Amanda Hizeryk**, doświadczona i sceptyczna lekarka Kevlarytów (najemników)?
    * **Edmund Lichter**, PRASTARY i łagodny lekarz który jest tu od zawsze i o którym są plotki że jest magiem?
    * **Nataniel Purczycki**, młody i lekko zestresowany miłośnik nietypowych sekretów?
* Aurora, kto Ci pomoże w trudnej sytuacji?
    * **Szaman Maksym Oblator**; dziwny człowiek który unika chodzenia do lekarza ale ma częste rady które się czasem sprawdzają?
    * **Szczury Arkologii**, uboższa grupa okolicznych ludzi której niejednokrotnie pomagałaś i która pomoże Tobie?
    * **Erwin Rolnicki**, delikatny mechanik, który się w Tobie podkochuje (nie ma szans), ale ma nadzieję, że przynajmniej zostaniecie przyjaciółmi?
* Betta, kogo znacie wśród Szczurów Arkologii? Pomogli Wam czy Wy pomogliście im?
* Rowena, co sprawia, że macie dostęp do logów i dokumentów Arkologii? Jak to się stało? Modeluj po świecie rzeczywistym.
* Aurora, co sprawia że Michał Kardok (który wszystkim gardzi) lubi Was? Co zrobiliście, że serio czuje że ma wobec Was dług?

ZACZYNAMY SESJĘ:

Skorpion ma:

* Kevlaryci: status POZYTYWNY (1)
* Szczury Arkologii: status POZYTYWNY (1)
* Skorpiony: status POZYTYWNY (1)
* Bazar Jabłuszko: status NEUTRALNY (0)

Zasoby:

* Jest uszkodzony
* Anomalie niebezpieczne

Problemy:

* Birutan w mieście, "poranił" Szczury.
* Skorpion ma ładunek, komu go damy?

IMPLEMENTACJA:

* Betta ma wsparcie Kevlarytów.
* Rowena ma relacje z Natanielem.
* Aurorze pomogą Szczury.
* Jadwiga pomoże Betcie; jest ze starszyzny, bo straciła córkę.
* Rowena jest na tyle inteligentna że ma znajomości; dlatego ma dostęp do kodów i logów.
* Mogliśmy zdobyć COŚ bardzo ważnego dla Michała. Michał miał wypadek i musiał mieć nową mechaniczną kończynę z birutana. Nowy mechanizm, choroba.

#### Działamy

2 mc później. Macie wartościowe anomalie, uszkodzonego Skorpiona.

Czekamy na Jadwigę.

Jadwiga, rozmowa z mentorką. 

Aurora szuka informacji o birutanie.

Tp +3:

* V: To nie jest sabotaż. To cholerstwo przegniło - awaria. To nie jest sabotaż.
* X: ZDRAJCZYNI LUDZKIEJ RASY
* Vr: Próbki dla Roweny.
* X: Szczury Arkologii "też" są ZDRAJCAMI LUDZKIEJ RASY
    * Birutan uratował i uciekł.

Troszkę gorsze części itp.

Rowena bada próbki. Czy to jest birutan? Czy działa jak birutan?

TrZ+3:

* V: To jest "biologicznie" - martwe ciało. To jest coś innego. To jest... naukowiec który to zrobił?
* Vr: .
    * To jest byt stworzony. Ale wymaga to nieprawdopobnej wiedzy.
    * Reverse engineering birutana.
    * Kto to BYŁ - ten człowiek BYŁ KIEDYŚ osobą z jednego ze Skorpionów. Nazywał się Dawid Rekińczyk. Zniknął 20 lat temu.

Betta sypia z jakimś randomowym kevlarytą, ma do niej słabość. Jan Szakczyński. Spotkali się, są rano, "co wiesz".

Tr Z +3:

* Vr: Jan jest wysokim oficerem Kevlarytów i ma słabość do Betty
    * Birutan im spadł z nieba.
    * Sami chcą się dowiedzieć - stan wojenny itp.
    * Nie wpadli na to. 
    * Nie mają nic z tym wspólnego.
* Ogólnie ludzie znikają. Ale nie ci którzy są łatwi do uratowania.
* Ktoś znika, są protesty, wszyscy to ignorują.

WIEMY że znikają ludzie. Są śmiertelnie chorzy. Ktoś / coś tworzy coś na wzór birutana, sterowane przez tą osobę..? Albo ratuje ludzi? Naszym celem będzie dowiedzenie się gdzie trafiają te osoby? Nie sądzę że są porywane.

Niech Rowena powie czy są porywane. Bo jak nie są - same się zgłaszają?

Szaman Maksym Oblator. Co wie szaman?

* X: Kevlaryci wiedzą, że trzeba działać szybko bo birutan zniknie XD
* X: Szaman twierdzi, że 20 lat Skorpion czeka. I trzeba jechać z szamanem. By przebłagać

KONFLIKT ZAWIESZONY

Nataniel. Co się dowiemy od niego?

* Jest jeden birutan. Tylko jeden.
* Gdzie znikają chore osoby, gdzie oddają...

Tr Z +2:

* V: Ludzie którzy znikają znikają w Krematorium. 
    * Nie ma logów. Nie ma informacji.
    * Osoby ŻYWE są wysłane do Krematorium i znikają.
* V: 
    * Nataniel uważa, że Edmund jest MAGIEM. Chodzącą perfekcją medyczną.
* Vr: 
    * Nataniel twierdzi, że Edmund jest coraz lepszy odkąd został lekarzem, jakieś 15-17 lat temu.
    * Nataniel potwierdza, że wieczna szczęśliwość.

Skorpion, który 20 lat temu zniknął nazywał się Wesoły Kram. Dawid Rekińczyk. Aurora go widziała.

Do kogo należy?

Tp +3:

* Vr: Ten Skorpion należy do Arkologii. Dokładniej, jest pod kontrolą Starszyzny. Ani jedna osoba nie używała tego Skorpiona.

Bibi, Jadwiga.

* "Skorpion, coś wspólnego".

Tr Z +3:

* V:
    * Jadwiga się przyznała, powiedziała o Aleksandrii. I o tym jak Starszyzna wspiera Aleksandrię w nadziei na lepsze jutro.

Aleksandria shall rise.

## Streszczenie

Grupa salvagerek wróciła do Arkologii Jabłuszko, ogarniętej paniką. Fragment Arkologii się zawalił i nie-birutan uratował ludzi przed katastrofą. Okazało się, że ten nie-birutan to jest Kurator stworzony z jednego z salvagerów lata temu; Arkologia współpracuje z Aleksandrią i salvagerki zaakceptowały ten fakt i zaczęły to aktywnie wspierać. W końcu Aleksandria jest lepsza niż śmierć w piaskach Neikatis. Jednocześnie grupa eks-najemników - Kevlaryci - próbują przejąć kontrolę nad Jabłuszkiem i gdyby wiedzieli, zwalczaliby z całych sił Arkologię.

## Progresja

* .

## Zasługi

* Aurora Werniel: salvager szczur inżynier; potwierdziła brak sabotażu w przypadku birutana (tylko zmęczenie materiału) i skierowała grupę na trop Starszyzny oraz ich potencjalnych powiązań ze Skorpionem.
* Betta Merkawiec: salvagerka i wojowniczka; dzięki relacji z Janem dostarczyła informacji, że Kevlaryci nie stoją za powstaniem birutana. Dowodziła zespołem.
* Rowena Ferket: salvagerka i badaczka birutan; jej badania dostarczyły kluczowych informacji o naturze bytu i pochodzeniu birutana, co skierowało grupę na trop Dawida Rekińczyka oraz Starszyzny Arkologii.
* Edmund Lichter: PRASTARY i łagodny lekarz który jest tu od zawsze; naprawdę jest Głównym Kuratorem Aleksandrii rozbitej niedaleko Arkologii Jabłuszko. To on rozbudowuje potęgę Aleksandrii i Arkologii i jest głównym "negocjatorem" z Arkologią w zakresie w jakim jest w stanie.
* Jan Szakczyński: oficer kevlarytów który sypia z Bettą; tłumaczy jej, że Kevlaryci planują stan wojenny. Ten "birutan" spadł im z nieba.
* Nataniel Purczycki: młody i lekko zestresowany miłośnik teorii spiskowych; podejrzewa Lichtera o bycie magiem (stary, potężny, coraz lepszy, za dużo wie) i dzieli się tym z Roweną.
* Jadwiga Verinn: członkini Starszyzny Arkologii Jabłuszko i mentorka Betty; osłania Lichtera i wspiera Opętanego Skorpiona. Dzięki niej Aleksandria rośnie w siłę.
* Kamil Waternis: salvager; dąży do współpracy z innymi Skorpionami i współpracując z Zespołem udało mu się uratować swoją żonę. Oddał całość łupu Zespołowi, bo było warto.
* Zofia Waternis: salvagerka; główna ekstraktorka która wpakowała się na niebezpiecznych birutan. Z pomocą Zespołu udało jej się wydostać i przechwycić łup.
* Maksym Oblator: szaman; bardzo przeciwny łamaniu starych zwyczajów Neikatis. Wie, że Opętany Skorpion został zniewolony przez Kuratora i chce Skorpiona przebłagać (i uwolnić) swoją śmiercią. Nie może nic zrobić.
* Dawid Rekińczyk: kiedyś przyjaciel Edmunda z tego samego Skorpiona; Aleksandria przekształciła go w bojowego Kuratora. Uratował życie wielu ludzi i prowadził ich do Aleksandrii.

## Frakcje

* .

## Fakty Lokalizacji

* Arkologia Jabłuszko: współpracuje z bardzo ciężko uszkodzoną Aleksandrią Kuratorów reprezentowaną przez Edmunda Lichtera; mniej problemów z birutanami i lepsze warunki za to, że ludzie nie giną.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Korowiec
                    1. Arkologia Jabłuszko

## Czas

* Chronologia: Zmiażdżenie Inwazji Noctis
* Opóźnienie: 4727
* Dni: 3

## Inne
### Arkologia

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Korowiec
                    1. Arkologia Jabłuszko
                        1. Zewnętrzna Tarcza
                        1. Poziom jeden
                            1. Blockhausy mieszkalne
                            1. System Defensywny
                            1. Galeria Widokowa
                            1. Jedna Jabłonka
                        1. Poziom zero
                            1. Stacje Przeładunkowe
                            1. Magazyny
                        1. Poziom (-1) Mieszkalny
                            1. Szyb centralny
                            1. NW
                                1. Magazyny wewnętrzne
                                1. System obronny
                                1. Sprzęt ciężki, kopalniany
                            1. NE
                                1. Medical
                                1. Rebreathing
                                1. Administracja
                                1. Sale reprezentacyjne
                            1. S
                                1. Sektor mieszkalny
                                    1. Przedszkole
                                    1. Stołówki
                                1. Centrum Rozrywki
                                1. Park
                                1. Farmy Grzybów
                        1. Poziom (-2) Industrialny
                            1. NW
                                1. Inżynieria
                                1. Fabrykacja
                                1. Reprocesor Śmieci
                                1. Magazyny
                                1. Reaktor
                            1. NE
                                1. Centrum Dowodzenia
                                1. Rdzeń AI
                                1. Podtrzymywanie życia
                            1. S    
                                1. Szklarnie podziemne
                                1. Stacje badawcze
