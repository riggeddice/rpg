## Metadane

* title: "Nie taki bezkarny młody tien"
* threads: historia-rolanda
* gm: kić
* players: żółw

## Kontynuacja
### Kampanijna

* [211212 - Nie taki bezkarny młody tien](211212-nie-taki-bezkarny-mlody-tien)

### Chronologiczna

* [211120 - Glizda która leczy](211120-glizda-ktora-leczy)

## Plan sesji
### Co się wydarzyło

* Jest frakcja, która ma relacje z Aurum. Magowie Aurum tam bywają i bywali nadal. Duża kohezja.
* Przez całe lata kropla po kropli - zwłaszcza młodzi magowie są problemem. Lecą na podryw, na używanie itp. I traktują ludzi jak śmiecie. Użyć i nara.
* Poleciał tam Lemurczak (Alojzy). Kiedyś. Był kroplą która przelała czarę. Zrobił to po Lemurczakowemu.
* Ta frakcja wynajęła Stalową Kompanię. Po to, by zwrócić uwagę Aurum i by pokazać, że muszą opanować swoich ogierów.

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Młody Roland. Zakochany w Klarze. Miłość, która nie ma prawa mieć miejsca - i on o tym wie. Kwartalny zlot młodych tienów jest jedną z nielicznych okazji by móc porozmawiać i nikt się nie dowie. Potrzymać się za ręce bez wzbudzania podejrzeń. On - paladyn. Ona - ma zajmować się bronią. Czy coś. Roland nie wnikał. Jeden z wielu zlotów Rolanda, pierwszy Klary. Roland wie, czego się spodziewać.

Wielki bankiet, wielka hala, wszyscy porozsadzani, Roland gdzieś wysoko przodu stołu - Sowiński, paladyn, te sprawy. Nie najważniejszy (są inni Sowińscy, są starsi itp) ale już wystarczająco istotny. Klara gdzieś tam daleko z tyłu, ale mają połączenie hipernetowe. Roland tylko sygnały typu "piiisk", przestraszona, nie bardzo wie co robić. Roland widzi swoich kuzynów - starszych i młodszych - obsadzonych laskami. A dookoła Rolanda (paladyna) sami faceci.

To jest smutek. Na szczęście jest Klara. Ale zainteresowanie dziewcząt też by się przydało. A tu sami młodzi pytający o np. taktyki użycia servarów... albo by opowiedział jak rozwiązał problem na treningu. I typ, którego Roland nie lubi - włazidupcy "OCH SENSEI OPOWIEDZ WIĘCEJ, MISTRZU!". Lubi w ustach dziewczyn. Nie lubi w ustach facetów. Bardzo.

Roland chce ułatwić Klarze wejście w towarzystwo. Jest w końcu PALADYNEM. Hipernetowe szepty - niech jej łatwo wejść w towarzystwo, coś co lubi. ALE niech wie, że to dzięki niemu a inni nie wiedzą, że dostała pomoc.

TrZ (Roland rozumie sytuacje i nikt od Klary nie spodziewa się biegłości) +2: 

* X: Za dobrze poszło. Będą od niej oczekiwać więcej, niż ona umie.
* X: Nie będzie lubiana przez większość. Odrzucona. Bo tryharduje.
* X: Bardzo kiepsko zaczyna - opinia takiej, co próbuje zdobyć pomoc w czymś TAKIM.

Roland nie wziął pod uwagę różnicy poziomów między nimi. Z frustracją obserwuje, jak Klara wpakowuje się w problem za problemem. I zamiast mieć eleganckie wejście, staje się pośmiewiskiem. Roland nie może zadziałać subtelnie, więc idzie tam bezpośrednio. Bierze Klarę i jakieś dwie inne dziewczyny, by "zademonstrować" coś chłopakom odnośnie broni i działania. I tak będzie gadał, by Klara mogła wykazać się jako czarodziejka rodu Gwozdnik - że na broni się zna. W ten sposób może nie uratuje jej reputacji wśród dziewczyn, ale Klara PRZYNAJMNIEJ będzie mieć chłopaków z którymi może gadać.

* V: Udało się, chłopaki zwłaszcza zainteresowani wojskowością zobaczyli w niej jakąś wartość, okazję do rozmowy.
* V: Klara może nie złapie kontaktu z dziewczynami na linii dziewczyńskiej, ale przynajmniej może pogadać z chłopakami o taktyce. Będzie miło spędzać czas.
* X: Parę doświadczonych lasek się zorientowała co się dzieje. Sztuczki Rolanda nie wyszły. Co najmniej jedna Sowińska widzi co się dzieje i odpowiednio zadziała.

Żeby nie było - Roland JEST zazdrosny. Planował coś innego, ale nie zostawi swojej UKOCHANEJ samej na pastwę "podłych kur". Nie faworyzował żadnej z tych trzech dziewczyn, ale wybrał je tak, by Klara na ich tle błyszczała. "Przypadkiem".

Wielkie żarcie doszło do końca. Czas na tańce. Roland nieźle tańczy - jest wyszkolony we wszystkich cnotach rodu. Klara gorzej. Roland wykorzystał to, że jest obok - "za to, że tak dobrze ich bawiła" da jej taniec. Przedstawia to jako zaszczyt dla oszukania innych. Roland próbuje zrobić, żeby Klara się wystarczająco dobrze bawiła.

Tr+2, bo to trudne warunki i trudny taniec a Klara jest zestresowana:

* X: Klara bumped into someone. ALOJZY LEMURCZAK. Wyżej w hierarchi niż Roland i starszy.

Normalnie Lemurczak nie opierdalałby Sowińskiego, ale tu można. Lemurczak "jak śmiesz" do Klary. Klara przestraszona. Roland przejmuje kontrolę - "przepraszam, tien. Mój błąd. Ja prowadziłem partnerkę."

* V: Roland NIE CHCE, by Lemurczak skupiał się na Klarze. Więc przekierował skutecznie ogień na siebie.
* V: "Ten Sowiński lepiej radzi sobie na polu walki niż w tańcu." -> Roland przeprasza, ale "don't escalate". Roland dostaje pogardliwe prychnięcie i Lemurczak przesuwa się gdzieś indziej do tańca.
* V: Roland uspokoił Klarę. Klara rozpływa się w ramionach Rolanda. Roland sprawia, że Klara bawi się dobrze.

Roland widzi wszystkich otoczonych osobami płci przeciwnej i się dobrze bawiących. A on - z facetami. Widząc jak bardziej doświadczone, lepsze wojowniczki się świetnie bawią stwierdza, że ON IM ZAIMPONUJE. Idzie na imponowanie bardziej doświadczonym i lepszym od siebie. Chodzi o to, by się pokazać też tym mniej. Może laski podejdą...

Ex+3:

* V: wszyscy są pod wrażeniem skilli Rolanda i jego paladyństwa. Wojowniczki w wieku Klary - "wow O_O". Starsze / podobne do Rolanda - "chłopie jesteś dobry, ale nie umiesz wybierać swoich bitew - musisz się douczyć".

Roland wolałby więcej... ale jest good enough. Musi zostać to, co jest. Resztę wieczoru spełnił w miłym towarzystwie, godnie broniąc honoru Sowińskich. Potwierdziło się to, co mówił Alojzemu Lemurczakowi - w walce jest groźny, w tańcu mniej ;-).

Późny wieczór. Roland chciał się wymknąć z Klarą popatrzeć w gwiazdy, pobyć z nią. Ale Klara jest zmęczona, oraz zastraszona. Ok, Roland MOŻE dałby radę ją wyciągnąć, ale to byłoby wbrew niej. Klara chce być tą grzeczną (nie rozumiejąc, że trochę wypada z grupy i jeszcze bardziej potwierdza się jej opinia słitaśnego nerda broni XD). Roland... on nie chce iść spać. Chciałby coś WIĘCEJ, ale musi być paladynem. Więc... zdecydował się iść na arenę. Popatrzeć na gry wojenne. Na pojedynki. Na walki grupowe. Najlepiej, jeśli jest jakaś kobieca drużyna i może popatrzeć - oczywiście, dla nabrania skilla.

Impreza zwolniła - środek nocy. Wielu tienów się szprycuje, Roland nie - więcej, bierze awaryjne detoksyfikatory (jakby coś wypił czy coś). Będzie zmęczony, ale jego ciało jego świątynią (niestety). Na sali jest też Alojzy, obstawia zakłady - ale Roland ignoruje. Roland ogląda jakąś kobiecą drużynę, 2v2, same laski :D.

Nagle - dźwięk. Seria z broni maszynowej w powietrze. Roland przypada i się chowa by być niewidoczny. Pełen servar, Lancer. Ten zwraca uwagę wszystkich wystrzałem. Oprócz niego kilka więcej jednostek. Spędzają wszystkich w jedno miejsce, na otwartą przestrzeń. Są kompetentni. Nie widać opcji czarowania (servar strzela w dłoń). Nie widząc innego wyjścia, Roland pozbywa się liberii i szybko smaruje się błotem. Niech nie wiedzą kim jest. Niech wygląda jak ofiara na arenie. Ktoś kto dostał wpierdol. Roland szybko do Klary - znaleźli ją? Tak. Więc niech się SŁUCHA, SŁUCHA POLECEŃ, nie będzie bohaterska itp. Ma być grzeczna. To nic jej się nie stanie.

Jak znaleźli Rolanda, on poszedł z nimi, bez żadnego oporu. Dostał wiadomość od Nikczemniczki Diakon. Zebrali większość na hali bankietowej. Na razie chyba nikomu nic się nie stało. Sowińscy - lepsi, bardziej doświadczeni - próbują opanować grupę by nie było paniki. Roland, zamaskowany, jest stoicki i nie przejmuje dowodzenia. KRÓL SPRYTU. I tu i po stronie Niki - "nie próbujcie nic głupiego to nic złego Wam się nie stanie". Roland idzie według procedury - zawsze można wykupić arystokratów.

Roland zorientował się, że hipernet jest zakłócony. O dziwo, ma link TYLKO do Nikczemniczki i Klary (emocjonalne nastawienie). Plus jak ktoś lokalnie się drze, da się zareagować. Pytanie, czy napastnicy nie mają czytania hipernetu (prawa inspektora). Starsi Sowińscy idą wg "trzymamy się procedury". Roland policzył z pomocą Nikczemniczki, że najpewniej jest koło 100 napastników. Z czego 10 servarów. Duży oddział. Niestety, to SĄ profesjonaliści - Roland nie widzi jak to pokonać.

Twarzą w stronę servara. Jakiś Mysiokornik, Samszarówna, Alojzy Lemurczak - wezwani. Zabrani z pomieszczenia. Z drugiego pomieszczenia - Klara, Torszecka, Wkrąż.

Roland NATYCHMIAST wchodzi na hipernet. Uspokaja Klarę. I chce informacji co się dzieje.

ExZ (zaufanie) +3

* Vz: udało się ją uspokoić. Klara nie zrobi nic głupiego, nie zacznie hiperwentylować, uciekać - nie skrzywdzi się.
* Xz: Druga Strona orientuje się, że to NIEMOŻLIWE że taka Klara się tak zachowuje. Ktoś jej mówi co ma robić.
* X: Klara pod presją się przyzna, że to jej ukochany. Roland. Ich związek wyszedł na jaw.
* X: Związek Rolanda z Klarą oficjalnie będzie zniszczony przez Sowińskich. To jest mezalians i to widać. Plus... inne laski nie patrzą na Rolanda, bo jest zajęty. Zabierają ważnego Lemurczaka a on się laską przejmuje.

Po wyprowadzeniu ich wszystkich, Roland zauważa że napastnicy zaczynają się wycofywać. Niki przekazuje Rolandowi, że mają kilka pojazdów, do dwóch dwie grupki magów. I z tymi dwoma pojechało po servarze. Te pojazdy ruszają w długą - Niki nie może ocenić gdzie. Roland widzi, że ci pilnujący coraz bardziej się przetasowują by móc powoli się wycofać. Gaz usypiający.

Tyle, że Roland jest - jako jedyny chyba - na detoksach bez środków odurzających. Więc dość szybko dojdzie do siebie. Szybciej niż powinien, zwłaszcza, że oni nie chcą zrobić krzywdy.

Roland się ocknął. Statrep: większość śpi, część totalnie zamulonych, część ruszać ale nie idzie. I nie można dawać detoksów bo pogorszę - brak wiedzy medycznej. Roland więc zajmuje się dyspozytorem automatycznych apteczek - niech jakiś Blakenbauer / Diakon / coś medycznego się tym zajmie.

Roland szuka zwycięzcę zawodów z najszybszego ścigacza. Do medyka - potrzebny jest ten zwycięzca na pełnej mocy. Wyjaśnia medykowi o co chodzi - porwano grupę magów. Trzeba asap poinformować o tym Centralę. Ktoś musi tam pojechać. On jest najszybszy. To jest różnica między "oni będą uratowani / wykupieni" a "oni znikną". Więc - budzimy kolesia.

ExZ+2:

* XXXVz: Mamy _jakiegoś_ pilota. Lata dość szybko. Starsi Sowińscy chcą władować mu raport, więc spowalniają. WNIOSEK: Centrala potrzebuje by spowolnić uciekinierów.

Roland przemawia przez megafon. Szuka miragentów. Mówi, kto jest porwany i że pomoc w drodze ale za wolno. Klara Gwozdnik. Że z miragentami dadzą radę ich uratować.

ExZ (wynik popisywania się wcześniej) +2:

* X: najlepsi z najlepszych są nieaktywni lub wolą poczekać
* V: Roland ma wsparcie - Niki, 2 miragenty, 3 innych magów różnych rodów.
* (+3Vg) Roland korzysta z tego, że już ma wsparcie - próbuje wciągnąć więcej. X: za dużo. Niekompetentni, ale pełni wielkiego wkurwu.
* X: Roland próbuje wybrać grupę. Zajmuje dłużej niż chce. Więc plan B: rozdziela na zespół szturmowy i resztę. Resztę -> gdzieś gdzie nie przeszkadzają.
* X: Grupa bezużyteczna się szybko zorientowała. Są wściekli na Rolanda.

Roland ma swoją grupę, choć nie ma relacji z ŻADNYM rodem - nawet ze swoimi (odrzucił też Sowińskich). Ma kilkunastu magów i dwa miragenty. Rekwizycja - najlepszy sprzęt jaki tu jest. Roland poprosił swoich o to, że jak ktoś jest technomantą - slave other stuff. Chcemy mieć DUŻĄ SIŁĘ, a przynajmniej jej iluzję.

Roland się domyśla, że oni będą szybko jechać w obszar gdzie jest pięć dzikich sieci jednocześnie. Tam mogli schować jednostkę lub przez niego przejechać. Oni są zamaskowani więc muszą ruszać się wolniej. Roland prowadzi swoją "armadę" za nimi. Najpierw ZNALEŹĆ i DOGONIĆ. Ale już teraz Roland zaczyna kombinować. Jakaś groźna jednostka najemników - Lanca Rosomaka. Ekipa, z którą nikt nie chce zwykle walczyć, bo są zawzięci jak cholera.

Grupa Rolanda próbuje PRZEGONIĆ przeciwników, z założeniem, że oni jadą na tamten teren. Miragenty uczą się kształtu i zachowania kluczowych osób z Lancy Rosomaka. A nasi iluzjoniści i technomanci dynamicznie przekształcają to czym się tam dostajemy by maksymalnie podobnie wyglądało do jednostek Lancy Rosomaka.

Adaptacja sprzętu przez technomantów. Niech wygląda jak Lanca Rosomaka i niech jest szybszy. KOSZT NIE MA ZNACZENIA. Sowińscy zapłacą.

ExZ (grupa, morale, sprzęt) M+2:

* X: Nie da się zbalansować pomiędzy tym jak to wygląda a prędkością.
* X: Magowie się zaharowywali; są ranni, zmęczeni itp.
* X: Nie przegonimy. Nie ma szans.
* X: Nie dogonimy. Tamci dotrą do obszaru ZANIM do czegoś dojdzie. NAPSULI SPRZĘTU NA NIC. Bez zysku.

Roland jest przestraszony. Poszarpany. Chce ratować Klarę. Wszystko mu się posypało. A Druga Strona dotarła do obszaru dzikiej sieci.

Gdy Grupa Rolanda tam dotarła, Roland ma nowy plan. Wcale nie lepszy. Integracja z dziką sentisiecią przez WSZYSTKICH. Niech nikt tam nie może przetrwać. Jeśli Druga Strona ma tam statek, niech będzie zniszczony. Jeśli nie dotarli do niego, niech muszą się wycofać. Destabilizacja i przebudzenie dzikiej sieci.

ExZ (wiele magów różnych rodów + idziemy tam gdzie sieć CHCE iść) M+2:

* X: Sieć wypełza; część sprzętu Grupy Rolanda została pożarta
* Vz: Dzika sentisieć uszkadza pojazd z Klarą; ten pojazd nie jest w stanie uciec, więc Klara + ekipa są na dzikim terenie
* X: Przeciwnik zdąży się ewakuować zanim pojawi się Centrala. Wzięli ze sobą pojazd z Alojzym.
* Vm: Dzika sentisieć chroni pojazd z Klarą i tylko ten obszar. Nikt nie może tego miejsca opuścić. Czyli - będą jeńcy. Ale trzech tienów jest uratowanych.

Roland dostał OPIERDOL. Politycznie uznano, że dlatego chronił ten pojazd, bo Klara. Zdaniem większości powinien był wybrać ten drugi. Roland nie miał nad tym kontroli, ale nic nie powiedział. Związek został rozerwany. Ale przynajmniej udało mu się uratować trzech tienów, choć ogromnym kosztem finansowym dla Swawolnika. Plus, przejęli jeńców - to samo w sobie miało dla Aurum jakąś wartość.

Jeńcy się po prostu poddali, nazwa jednostki, kontraktu i czekali na negocjacje. Jednostka to "Stalowa Kompania", bardzo kompetentni, nie są okrutni, są przygotowani. Nie są tani. A ci nie wiedzą kto ich wynajął - mamy trepów. Nie wiedzieli nawet kogo porwali.

## Streszczenie

Roland x Klara, w Swawolniku. Żywe imprezy i swawole młodych tienów Aurum zakończyły się atakiem Stalowej Kompanii, która porwała 6 magów. Roland zebrał grupę by ich odbić, nie dogonił i odniósł mniejszy sukces - ale wpływając na dziką sentisieć uratowali ukochaną Klarę Rolanda przed porwaniem (i 2 innych magów) i Aurum ma jeńców.

## Progresja

* Roland Sowiński: zmuszony do zerwania tajnego związku z Klarą Gwozdnik przez centralę Sowińskich. Jako posłuszny paladyn, wypełnił rozkaz. Ma w końcu 16 lat.
* Roland Sowiński: narobił sobie wrogów w Aurum we WSZYSTKICH rodach, łącznie ze swoim - odrzucił tych, co uznał za niekompetentnych i też nie dogonił porywaczy. Acz uratował 3 magów.

### Frakcji

* Lemurczak: czyny Alojzego Lemurczaka zostały nagłośnione. Lemurczakowie mają wpierdol reputacyjny poza Aurum.

## Zasługi

* Roland Sowiński: 16 lat. Próbuje być paladynem, choć trochę nie ma serca. Ma relację z Klarą Gwozdnik. Słucha się poleceń porywaczy, uspokaja i stabilizuje, ale jak tylko opuścili miejsce ataku to organizuje grupę pościgową. Gdy nie wyszło dogonienie, użyli dzikiej sentisieci do zatrzymania choć części napastników i ukochanej Klary. Popisuje się jak cholera.
* Klara Gwozdnik: 14 lat. Ukochana Rolanda (ze wzajemnością), pierwszy raz na zlocie. Trochę nerd broni, inne laski jej nie lubią bo tryharduje, chłopaki dzięki Rolandowi ją lubią, bo fajnie gada o broni. Losowo wybrana jako +1 zakładnik przez Stalową Kompanię. Opanowała nerwy na wodzy dzięki Rolandowi. Kiepska społecznie (na tym etapie).
* Alojzy Lemurczak: 17 lat. Wyżej w hierarchii niż Roland Sowiński. Swego czasu narozrabiał poza Aurum, więc wysłano za nim Stalową Kompanię. Koleś szuka guza, ale nie z Rolandem. Porwany przez Stalową Kompanię wraz z dwoma innymi magami.
* Nikczemniczka Diakon: 16 lat, dobrze walczy, lubi się z Rolandem Sowińskim. Jego zdaniem - za ładna. Jej zdaniem - za sztywny i to nie tam gdzie trzeba. Dla przyjaciół - Niki. Koordynuje z Rolandem pościg za porywaczami Klary. Wierzy w Rolanda bardziej niż on sam w siebie. Lubi go, więc go trolluje - nigdy z żadną nie pójdzie do łóżka. Cheeky.

### Frakcji

* Stalowa Kompania: bardzo kompetentni, nie są okrutni, są przygotowani. Nie są tani. Najemnicy operujący przede wszystkim w Sojuszu Letejskim. Tu: wysłani by porwać młodego Alojzego Lemurczaka. Udało im się, acz w obszarze Dzikiej Sentisieci musieli zostawić jeden APC z 3 jeńcami (magami) i kilkoma żołnierzami. Sprzęt i żołnierze do wykupu.

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Pięciokąt Ichtis
                            1. Powiat Pentalis
                                1. Swawolnik: miejsce wypoczynkowe, które pełni rolę "szaleństwa" dla młodych lordów Aurum. Miejsce swawoli. Przygotowane dla młodych magów Aurum, atrakcje dla osób poniżej 20 roku życia. Tym razem ma tam miejsce zlot młodych lord-in-waiting.
                            1. Powiat Dzikiej Sieci: gdzieś tam ukryty był statek Stalowej Kompanii który posłużył do ewakuacji ich inwazji na Swawolnik i porwania trzech tienów.

## Czas

* Opóźnienie: -2561
* Dni: 3

## Konflikty

* 1 - Roland chce ułatwić Klarze wejście w towarzystwo. Hipernetowe szepty - niech jej łatwo wejść w towarzystwo. Niech wie, że to dzięki niemu a inni nie wiedzą.
    * TrZ (Roland rozumie sytuacje i nikt od Klary nie spodziewa się biegłości) +2
    * XXX: młoda tryharduje, kiepsko zaczyna, odrzucona.
* 2 - Roland nie może zadziałać subtelnie, więc idzie tam bezpośrednio. Bierze Klarę i jakieś dwie inne dziewczyny, by "zademonstrować" coś chłopakom odnośnie broni.
    * TrZ (Roland rozumie sytuacje i nikt od Klary nie spodziewa się biegłości) +2
    * VVX: Udało się, Klara ma kontakt z chłopakami (nawet jak nie dziewczynami), ale laski z Aurum widzą i ten związek nie przetrwa - trzeba go rozbić
* 3 - Czas na tańce. Roland nieźle tańczy - jest wyszkolony we wszystkich cnotach rodu. Roland próbuje zrobić, żeby Klara się wystarczająco dobrze bawiła.
    * Tr+2, bo to trudne warunki i trudny taniec a Klara jest zestresowana:
    * XVVV: Klara bumped into ALOJZY LEMURCZAK; Roland przekierował na siebie, przeprosił ale nie ma eskalacji, uspokoił Klarę.
* 4 - Roland chce zaimponować wojowniczkom i innym. Jest dobry w walce.
    * Ex+3
    * V: są pod wrażeniem skilli Rolanda i jego paladyństwa. młode - "wow O_O". Starsze - "chłopie jesteś dobry, ale nie umiesz wybierać swoich bitew.
* 5 - Roland NATYCHMIAST wchodzi na hipernet. Uspokaja Klarę. I chce informacji co się dzieje (przy porwaniu).
    * ExZ (zaufanie) +3
    * VzXzXX: udało się uspokoić, Klara nic głupiego nie zrobi; Druga Strona wie o Rolandzie, Klara powiedziała o ukochanym, związek zniszczony przez Sowińskich (mezalians)
* 6 - Roland szuka zwycięzcę zawodów z najszybszego ścigacza. Budzimy kolesia.
    * ExZ+2
    * XXXVz: Mamy _jakiegoś_ pilota. Lata dość szybko. Starsi Sowińscy chcą władować mu raport, więc spowalniają. WNIOSEK: Centrala potrzebuje by spowolnić uciekinierów.
* 7 - Roland przemawia przez megafon. Szuka miragentów. Mówi, kto jest porwany i że pomoc w drodze ale za wolno. Klara Gwozdnik. Że z miragentami dadzą radę ich uratować.
    * ExZ (wynik popisywania się wcześniej) +2:
    * XV (+3VG) XXX: najlepsi nieaktywni; Roland ma wsparcie; ma ZA DUŻE wsparcie; oddziela na dwie grupy użyteczną i bezwartościową; bezwartościowi zorientowali się i wściekli
* 8 - Adaptacja sprzętu przez technomantów. Niech wygląda jak Lanca Rosomaka i niech jest szybszy. KOSZT NIE MA ZNACZENIA. Sowińscy zapłacą.
    * ExZ (grupa, morale, sprzęt) M+2:
    * XXXX: Nope. Nie dogonią, sprzęt uszkodzony, zaharowali się itp. Wrogowie dotarli do Dzikiej Sentisieci.
* 9 - Roland ma nowy plan. Wcale nie lepszy. Integracja z dziką sentisiecią przez WSZYSTKICH. Niech nikt tam nie może przetrwać.
    * ExZ (wiele magów różnych rodów + idziemy tam gdzie sieć CHCE iść) M+2
    * XVzXVm: część sprzętu Rolanda pożarta; pojazd z Klarą uratowany; przeciwnik zdołał się ewakuować; dzika sieć chroni pojazd z Klarą.
