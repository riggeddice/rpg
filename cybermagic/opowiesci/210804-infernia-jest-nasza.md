## Metadane

* title: "Infernia jest nasza!"
* threads: legenda-arianny
* gm: żółw
* players: fox, kapsel, kić

## Kontynuacja
### Kampanijna

* [210728 - W cieniu Nocnej Krypty](210728-w-cieniu-nocnej-krypty)

### Chronologiczna

* [210728 - W cieniu Nocnej Krypty](210728-w-cieniu-nocnej-krypty)

### Plan sesji
#### Co się wydarzyło

* Skrajny odłam sił Termii pod dowództwem komodora Traffala chce uderzyć w Eternię przy użyciu Inferni.
* Korzystając z okazji odłam eternijskich sił tien Żaranda też chce przejąć Infernię i ją Przebudzić.

#### Sukces graczy

* .

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Arianna odwiedza hangary. Jest z nią Franciszek Maszkiet - delikwent zajmujący się przydziałami. Statek "Samotność Gwiazd". Jest TANI zarówno w utrzymaniu jak i naprawie. Decyzją admiralicji Infernia nie będzie naprawiana i będzie oddana na złom. Samotność Gwiazd potrzebuje 1/4 załogi Inferni.

Ariannę zdecydowanie zdziwił fakt, że Infernia TERAZ ma zostać rozebrana. Jest stosunkowo trzymająca się kupy. Naprawa niekoniecznie jest tak droga.

Konsekwencje zmiany statku:

* Infernia i Samotność to inne typy jednostek.
* Samotność potrzebuje 25% załogi którą ma Infernia.
* Samotność jest jednostką "lepszą".

"Mamy Infernię opanowaną jak Sebix w latach 90 miał golfa dwójkę po tuningu i możemy zrobić z tym wszystko. Musimy mieć Infernię naszą." - Kacper.

Klaudia przygotowuje potężny raport. Składa informacje ile kosztuje Infernia i jakie są koszty ale też jakie analogiczne działania Infernia rozwiązuje. Zaraz potem dodała: jak analogiczne statki do Samotności sobie radzą na tych akcjach. To nie jest tak że Infernia jest tak droga w utrzymaniu a Admiralicja nie stosuje jej tak jak powinna. Plus Orbiter ma za dużo gruchotów. Infernia jest okrętem do zadań specjalnych a one są drogie. Bo fachowcy do zadań specjalnych są drodzy. A poziom wyszkolenia załogi jest taki że powinno się im płacić więcej o 75%. Ale z uwagi na sympatii do statku zgadzają się pracować "za grosze".

Klaudia bierze od Arianny kody dostępu. Potrzebuje szerszych uprawnień.

ExZ+3:

* V: Raport jest raportem zagłady. Admiralicja wygląda jak idioci jeśli umiesz to czytać.
* XX: Jeśli raport wycieknie do prasy to Admiralicja będzie BARDZO niezadowolona z działań Arianny i na całą Infernię.
* V: Jeśli raport wycieknie do prasy to będzie wizerunkowa katastrofa dla Orbitera. Serio problematyczna, zwłaszcza w świetle Sekretów Orbitera.

Eustachy się przeszedł w okolicach hangarów Alicantis. Nad Infernią toczone są prace. Czyli ktoś coś tam robi. Eustachy chce sabotować. Infernia jako jedyny statek jest chroniona. Kilka osób z żandarmerii.

Eustachy poprosił Leonę, by ta weszła na Infernię. Bo potrzebuje odwrócić uwagę. Eustachy mówi, że żandarmi pilnują Inferni. A nie powinna. Leona się SKRAJNIE oburzyła, że Infernia na złom a Ariannie nie powiedzieli. Eustachy informuje Leonę że to się może dla niej źle skończyć więzieniem. Ta się uśmiechnęła. Eustachy szybko przeszkolił Kamila by ten blokował wejście. "Nie da się tego otworzyć, patrzcie."

Leona idzie na Infernię jakby nigdy nic. Żandarmi próbują ją powstrzymać. A Eustachy wywołuje mały pożar na boku - by był dym. 

ExZ+3:

* X: Faktycznie, Leona trafi do więzienia na tydzień
* V: Eustachy się wślizgnie i ma okazję do sabotażu
* X: Eustachy łazi po kanałach serwisowych Inferni i nie wychodzi, ale sabotuje w trakcie.
* X: Leona dokonała bardzo poważnych obrażeń i uszkodzeń; musieli ją zestrzelić. Działała na ostro, jako neurosprzężony komandos.
* V: Eustachy sabotażem skutecznie unieszkodliwia jakichkolwiek planów by nie mieli.

Eustachy korzysta z okazji że Infernia ma super czujniki. Chce mieć dokładną wizję kto jest gdzie na Inferni. Modyfikuje na szybko czujniki. Rzut izometryczny. Nawet może czarować bo jest przesiąknięte echem Eustachego.

TrZ+3 -> TpMZ+3:

* V: Eustachy ma pełen podgląd na Infernię i kto jest gdzie.
* O: Emulacja neurosprzężenia w kontekście tej sesji. Eustachy - Infernia. Eustachy ma RELACJĘ z Infernią. Aha, plus Diana.
* V: Eustachy jest magicznie neurosprzężony z Infernią i tylko z Infernią przez jej anomalne komponenty. Infernia jest "jego ciałem i oczami".

Klaudia szybko sprawdziła pod kim są ci żandarmowie. Pod... admirał Termią? Ale to nie pasuje. Termia nie działa w ten sposób. Arianna odwiedza admirał Termię. Ma pretekst - przeprosić że Leona zmasakrowała jej żandarmerię. Termia potwierdziła; ona tego nie autoryzowała tego z Infernią. Jest za tym, by Arianna po prostu była w stanie odzyskać Infernię.

Arianna próbuje od Termii się dowiedzieć więcej na temat tego komodora.

Tr+2:

* XX: Traffal skutecznie wykonał ruch polityczny - Infernia jest za droga. Jest taka opinia. Koszty rzędu krążownika a nie fregaty. Termia się nieźle uśmiała, że zamiast iść z tym do Kramera to Arianna zaczęła bunt.
* V: Nazwisko komodora - Artur Traffal. Arianna nie wie o nim wiele.
* V: Traffal jest bardziej... radykalnym komodorem. Bardzo pro-wiedzowo. Rozłożyć Infernię na wiedzę. Zezłomować a potem ukraść i się dowiedzieć. Co więcej, Traffal ostro gra budżetem. Ryzykownie.

Termia poinformowała Ariannę, że ta wyraźnie próbuje się dobrze bawić buntując. Jeśli Arianna myśli że jest jedna Leona na K1, że jej siły na K1 mogą cokolwiek K1 zrobić to Arianna się myli. Arianna może hasać, bo Termia ją chroni. Było to dość ciekawe dla Arianny, ale Termia wyraźnie mówi prawdę. Aha, zdaniem Termii, Arianna powinna iść z tym do Kramera a nie próbować się buntować; to, że coś jest decyzją Admiralicji nie oznacza że admirał podjął taką decyzję osobiście. K1 składa się z biurokracji. 

Arianna wyciągnęła z tego dwie obserwacje:

* Kramer dużo bardziej chroni swoich agentów niż Termia. Termii nie przeszkadzało wrzucenie Traffala pod autobus. "Tylko silni przetrwają".
* Termia Ariannę chroni. Tego się Arianna nie spodziewała.

Arianna poszła więc do admirała Kramera. Pokazała, że Infernia ma być złomowana. Kramer powiedział że coś jest nie tak. Nie żądał tego. Spytał Ariannę czy ma odwrócić rozkaz? TAK! Kramer odwrócił; powiedział, że tydzień Infernia jest "niczyja". Ale nie ma tak że nie wolno mieć do niej dostępu. To normalna sprawa. Arianna podziękowała i zdecydowała sie nie robić nic. Niech Eustachy będzie żywą pułapką.

Mija 15h.

Tymczasem Eustachy. Aparycja Diany jest jeszcze bardziej nie do wytrzymania niż sama Diana. Ktoś wszedł na pokład. Podobno ekipa remontowa. Nawet są ubrani jak ekipa remontowa. Infernia nie identyfikuje ich jako remontowców dopasowanych do Inferni. Szukają czegoś anomalnego, anomalnego statku. Eustachy ich zamyka "zidentyfikuj się i podaj numer karty pokładowej". Koleś zaczął mówić o tym, że przybył "uwolnić Bestię zamkniętą w kajdanach". Eustachy puścił zraszacze i gaz usypiający.

TrZ+3:

* V: Unieszkodliwieni. Z pomocą Diany.
* X: "Ćśś... odpowiadaj na pytania". Diana wbiła nóż w napastnika i się oblizała. Anomalia Inferni poczuła smak.
* V: Przesłuchanie. "Po prostu nie chciałem by arcymag trzymał Cię na smyczy. Jesteś bytem energetycznym, musisz być wolna".
* X: Diana ma problemy by się powstrzymać. Już z trzech chłepcze. Anomalia Inferni zaczyna negatywnie wpływać na Eustachego. Ten widok jak anomalna Diana z radością wylizuje ranę swojej ofiary... a Eustachy to _czuje_...

Eustachy powiedział "NIE PRZESZEDŁEŚ PRÓBY ODEJDŹ NIEGODNY!" i go wywalił. Naprawdę nie chce się skazić. Diana CHCE kolesia, Eustachy powiedział żeby nie przeszkadzała. Może tańczyć dla niego. Diana... zaczęła się rozbierać....... VEIL.

Arianna: "Infernio, przybywam Cię spętać bo wyrywasz się spod kontroli". Eustachy udaje, że się uwolnił, Eustachy wybuchy i lasery i "znowu jest spętana". Gandalf YOU SHALL NOT PASS. Klaudia to filmuje. Cel - by nikt nie pomyślał że może kontrolować Infernię.

TrZ+3:

* V: Maciek jest na kolanach. Zobaczył Potęgę Arcymaga.
* Vz: "Tylko Arianna może dowodzić Infernią a Eustachy może być jej mechanikiem". Wybija z głowy.
* V: Maciek pęka. Wszystko powie. Wszystko.
    * Koleś jest eternianinem. Wierzy w wyższość bytów efemerycznych, magicznych nad ludzkie.
* "Spójrz na mnie. JA jestem istotą wyższą."
    * X: ...i rozgłosi. Będzie budował kult arcymagiń.
    * O: Diana wyszła z Inferni. Jest dowodem efemeryczności i anomalności Inferni. Klaudia ma WTF. Elena będzie miała PYTANIA.
    * V: ON W TO UWIERZY. Wierzy że Arianna i Eliza są najwyższymi formami egzystencji.
* On dowiedział się tego dzięki komodorowi Traffalowi. Traffal potrzebował pieniędzy więc sprzedał informacje o Inferni - nawet do tego umożliwił Maćkowi na wejście na pokład.

"Termia? Watch me..." - Arianna.

Arianna chce użyć PRAWDZIWEJ magii a nie udawanej. Puryfikacja efemerydy Diany. Wyczyszczenie z Esuriit. "Chodźmy razem na pokład, do Eustachego". Arianna przygotowuje się do zaskakującej puryfikacji. "Mówił o Tobie gdy Ciebie nie było. Elena była zła bo wspominał o Tobie gdy był przy niej."

TrZM+2:

* Vz: Potężna wiązka puryfikacyjna. "Diana" będzie oczyszczona. Infernia ani Eustachy nie zostaną Skażeni.
* V: Diana nic nie zdąży zrobić, Infernia też.

"Diana" się rozpłynęła w Inferni. Ale wróci.

KLAUDIA JEST MŚCIWA.

Klaudia wpada w virt. Zawsze utrzymywała przyjazne stosunki z TAI na K1. Nic super, ale tu coś podrzuci, pomoże... więc Klaudia chce doprowadzić do tego by wszystkie akcje jakie koleś chce podjąć miały strajk włoski ze strony TAI. Losowe sprawdzenie ksiąg? Na niego. Losowa kontrola? On ma kontrolę. Chodzi i gra w kasynach? To ma niższe prawdopodobieństwa :3. WSZYSTKO co Klaudia jest w stanie zrobić co sprawi że trudniej mu na K1 zdobyć kasę. A jak się uda - przemycić w papiery znacznik "zwróć na niego uwagę". Termia jak wszyscy ma swój sztab, Klaudia chce zrobić by on dostał kilka kolejnych misji typu "wożenie świń do Anomalii Kolapsu". Brudna i paskudna robota.

ExZ+3:

* V: Sukces. Koleś nie będzie miał łatwo w życiu na K1.
* X: Dojdzie do tego, że to kontratak z Inferni.

EPILOG:

Ariannę odwiedza komodor Traffal. Pogratulował jej usunięcie maga z Eterni. Arianna "to nie Twoja zasługa". Traffal powiedział, że polował na kultystów z Eterni i jednego chciał złapać przez Infernię. Nie spodziewał się, że Arianna przesunie go na swojego kultystę XD. Powiedział jej, że to niewłaściwe. Arianna powiedziała, że nie powinien atakować Inferni.

## Streszczenie

Ku wielkiemu niezadowoleniu Zespołu, Infernia miała im być odebrana i złomowana. Mieli dostać nowy tańszy statek. Zespół odkrył że za tym stoi komodor Traffal z sił Termii który przy użyciu Inferni chciał złapać w potrzask arystokratę eternijskiego który był kultystą magii. Zespół odzyskał Infernię, przesunął kultystę w kierunku na Ariannę i uciemiężył komodora Traffala - będzie miał pecha na K1.

## Progresja

* Arianna Verlen: tien Maciej Żarand z Eterni został jej bezwzględnym kultystą. Jeśli magia jest bytem wyższym a Arianna ją kontroluje to arcymagowie są godni wyznawania...
* Artur Traffal: ma skrajnego pecha na K1 dzięki Klaudii Stryk. W kasynie ma gorsze prawdopodobieństwa, jego wybierają do kontroli itp. Innymi słowy, chce unikać K1 jak to tylko możliwe.
* Eustachy Korkoran: ma możliwość magią "neurosprzężenia" z Infernią przez anomalny rdzeń Inferni. 
* OO Infernia: opinia bardzo drogiego statku, na poziomie krążownika a nie fregaty dzięki komodorowi Traffalowi.
* OO Infernia: opinia - tylko Arianna Verlen może kontrolować Infernię. Ten statek jest po prostu zbyt "chory" i anomalny.
* Leona Astrienko: następny tydzień spędza na zmianę w więzieniu i w medvacie po akcji z masakrą żandarmów pilnujących Infernię.

### Frakcji

* .

## Zasługi

* Arianna Verlen: dowiedziała się od Termii kto stoi za odbieraniem jej Inferni, po czym zmieniła tien Żaranda w swojego kultystę i puryfikowała aparycję Diany Arłacz na Inferni.
* Eustachy Korkoran: wkradł się na Infernię by nikt mu jej nie zabrał, po czym przejął nad nią kontrolę i się z nią magicznie neurosprzęgł, po czym złapał w pułapkę kultystę magii - eternianina tien Żaranda.
* Klaudia Stryk: wpierw stworzyła raport pokazujący Admiralicji że Infernia robi inne akcje i jest droga ale superskuteczna... a potem zemściła się za Infernię robiąc komodorowi Traffalowi strajk włoski ze strony TAI na K1.
* Diana d'Infernia: psychotyczna aparycja Diany Arłacz; wróciła wezwana przez Eustachego magią Paradoksu. Zraniła Żaranda infiltrującego Infernię i z radością wylizywała jego ranę. Potem tańczyła dla Eustachego. Arianna ją "egzorcyzmowała" magią. Ale wróci :-).
* Leona Astrienko: wykorzystana jako psychotyczna broń odwracająca uwagę od Inferni by Eustachy mógł się wkraść na Infernię mimo żandarmów. Ciężko raniła żandarmów, w końcu zestrzelona trafiła do więzienia.
* Franciszek Maszkiet: oficer Orbitera na K1 zajmujący się provisioningiem. Poinformował Ariannę z zadowoleniem, że ma dla niej nową jednostkę w miejscu Inferni decyzją Admiralicji. Ofc Arianna zaczęła działania odzyskujące Infernię.
* Aleksandra Termia: nie chroni swoich komodorów, trzyma się zasady "najsilniejszy przetrwa", ale daje im dużo wyższą autonomię. Chroni Ariannę na K1 pozwalając jej "się buntować" - dlatego żandarmeria nie ruszyła przeciw niej i załodze Inferni.
* Antoni Kramer: ucieszył się, że dla odmiany Arianna przyszła do niego rozwiązać problem "złomowania Inferni" zamiast robić własne plany (nie przyszła). Odwrócił rozkaz złomowania i oddał Ariannie Infernię.
* Maciej Żarand: eternijski arystokrata wierzący w magię jako byt najwyższy. Chciał "uwolnić anomalność z Inferni". Arianna pokazała mu moc arcymaga i Maciej został kultystą Arianny i Elizy Iry...
* Artur Traffal: komodor Termii; zastawił pułapkę na Żaranda przy użyciu Inferni (i sobie dorobił). Wymanewrował Ariannę z Inferni, po czym chciał zapolować na kultystę magii z Eterni na K1. Niestety dla niego, wszystko poszło źle a kultystę oswoiła Arianna.
* OO Samotność Gwiazd: szybka i solidna dalekosiężna jednostka która miała być Ariannie dostarczona zamiast Inferni. Tania. 25% załogi. Arianna wzgardziła tą jednostką - chce Infernię z powrotem XD.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy

## Czas

* Opóźnienie: 15
* Dni: 3

## Konflikty

* 1 - Klaudia przygotowuje potężny raport. Składa informacje ile kosztuje Infernia i jakie są koszty ale też jakie analogiczne działania Infernia rozwiązuje. Infernia MUSI do nich wrócić. Raport pokazujący znaczenie Inferni dla Admiralicji.
    * ExZ+3
    * VXXV: Raport jest raportem zagłady, jeśli wycieknie - katastrofa wizerunkowa dla Orbitera. Silna rzecz. Ale: jeśli wycieknie do prasy to Admiralicja będzie WŚCIEKŁA na Ariannę.
* 2 - Eustachy poprosił Leonę, by ta weszła na Infernię. Bo potrzebuje odwrócić uwagę. Leona zatem odwraca uwagę brutalnie (jak to ona)
    * ExZ+3
    * XVXXV: Leona ciężko pobiła żandarmów i dała czas Eustachemu, ten się wślizgnął na Infernię i łazi kanałami technicznymi. No i Leona trafia na tydzień do więzienia...
* 3 - Eustachy korzysta z okazji że Infernia ma super czujniki. Chce mieć dokładną wizję kto jest gdzie na Inferni. Modyfikuje na szybko czujniki. Rzut izometryczny. Nawet może czarować bo jest przesiąknięte echem Eustachego.
    * TrZ+3 -> TpMZ+3
    * VOV: pełen podgląd na Infernię i wieczne neurosprzężenie przez anomalię z Infernią; ale wraca efemeryda Diany.
* 4 - Arianna próbuje od Termii się dowiedzieć więcej na temat tego komodora który próbuje odciąć Ariannę od Inferni.
    * Tr+2
    * XXVV: Traffal skutecznie zrobił ruch polityczny że Infernia jest droga; ale Arianna dowiedziała się kim jest Traffal.
* 5 - Infernię infiltruje "ekipa remontowa". Eustachy ich zamyka i unieszkodliwia i przesłuchuje.
    * TrZ+3
    * VXVX: Unieszkodliwieni z pomocą Diany, która zaczyna entuzjastycznie wylizywać krew (ranę którą zadała nożem). Koleś powiedział prawdę Eustachemu ale Diana nie może się powstrzymać z chłeptaniem...
* 6 - Arianna: "Infernio, przybywam Cię spętać bo wyrywasz się spod kontroli". Cel - by nikt nie pomyślał że może kontrolować Infernię.
    * TrZ+3
    * VVzV: Żarand na kolanach przed mocą arcymaga, zostaje kultystą arcymagów i wszystko wyśpiewał
    * (doprecyzowanie) XOV: rozgłosi, buduje kult arcymagini, Diana widoczna na K1, Maciek wierzy w najwyższą formę egzystencji
* 7 - Arianna puryfikuje efemerydę Diany
    * TrZM+2
    * VVz: pełen sukces, "Diana" oczyszczona i odesłana.
* 8 - KLAUDIA JEST MŚCIWA. Chce doprowadzić do tego by wszystkie akcje jakie Traffal chce podjąć miały strajk włoski ze strony TAI. Losowe sprawdzenie ksiąg? Na niego.
    * ExZ+3
    * VX: Sukces. Komodor Traffal ma ciężko na K1, ale dojdzie do tego że to Klaudia XD.

## Inne
### Projekt sesji
#### Procedure

* Overarching Theme:
    * gracze mają "kierowanie" (Vision & Purpose); wiedzą gdzie iść i po co tam idą
    * gracze są bardziej zaangażowani w historię, bo widzą, gdzie ich decyzje prowadzą
    * MG wie, które sesje wprowadzać i które usuwać i w którą stronę stawiać adwersariat
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * graczom bardziej zależy na postaciach i ich powodzeniu
    * więcej potencjalnych wątków i rzeczy które możemy odrzucić przy krzyżykach
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * gracze widzą jak wpływają na świat. Wpływ jest "ich" - stąd tak tragiczne jest gdy "ich" rzeczy są corrupted / destroyed.

#### Result

* Overarching Theme:
    * V&P:
        * 
    * adwersariat: 
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * .
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * .

#### Scenes

* Scena 0: 
* Scena: 

