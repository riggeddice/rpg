## Metadane

* title: "Dla swych marzeń, warto!"
* threads: agencja-lux-umbrarum, problemy-con-szernief
* motives: energia-esuriit, faceci-w-czerni, the-seeker, the-revenant, the-protector, black-technology, dark-temptation, petla-magia-emocje, wrobieni-lecz-niewinni
* gm: żółw
* players: kamilelf, michal, zuzia

## Kontynuacja
### Kampanijna

* [231221 - Pan Skarpetek i odratowany ogród](231221-pan-skarpetek-i-odratowany-ogrod)

### Chronologiczna

* [231221 - Pan Skarpetek i odratowany ogród](231221-pan-skarpetek-i-odratowany-ogrod)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * Seesaw - ".Hack//Sign-Obsession"
        * "How come I must know | Where obsession needs to go? | How come I must know | Where the passion hides its feelings?"
* Opowieść o (Theme and vision): 
    * "Dążenie do spełniania marzeń - obsesja na tym punkcie - skończy się katastrofą. Niezależnie od celu. Było warto?"
        * Ojciec, chroniący córkę przed magią - by tylko ją alienować od siebie za wszelką cenę
        * Arystokrata, pragnący integracji biosyntów - by patrzeć jak jego biosyntka umiera chroniąc go przed tłumem
        * Oficer Teraquid, nienawidząca biosyntów i chroniąca Teraquid - pragnie zniszczyć biosynty by ratować 
        * Popularyzatorka, pragnąca dobra dla stacji. Czy jej śmierć jest warta lepszej stacji?
        * Dziennikarka wykolejona ze świata który zna, pragnąca ZROZUMIEĆ.
        * Poszukiwacz, pragnący uratować za wszelką cenę swojego syna, żyje tylko dla złudzeń i niszczy ludzkie życia dążąc do marzeń.
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * energia-esuriit: nieskończona obsesja Pierścienia, która sprawia że ludzie się spalają pragnąc spełnić swoje marzenia.
    * the-seeker: zarówno Pierścień, szukający maga by go Aktywować jak i Poszukiwacz, próbujący odnaleźć pierścień jako pierwszy.
    * faceci-w-czerni: Agencja Lux Umbrarum, znowu. Tym razem próbują przechwycić Pierścień i deeskalować konflikty związane z Obsesją Esuriit na Stacji.
    * the-revenant: Maia Sakiran (Teraquid), która przetrwała operację eksterminacji biosyntów ale jej ukochany zginął. Od tej pory zrobi WSZYSTKO by zniszczyć biosynty. Augmented Human.
    * the-protector: Estril Cavalis, który pod wpływem Pierścienia chce zamknąć Aerinę w złotej klatce. Nic złego jej się nie stanie. Magia NIE wróci. Oraz Vanessa - ochroni Artura i Aerinę.
    * black-technology: wykorzystywane przez Poszukiwacza kontrolowalne anomalie Alteris: jedna do Mimikry jedna do Teleportacji.
    * dark-temptation: Pierścień oferuje spełnienie marzeń - ale zabiera Ci życie i energię.
    * petla-magia-emocje: Estril Cavalis idzie FULL PATRICK ZALA. Aerina celuje w Agentów z pistoletu. Narastająca obsesja - silniejsze emocje -> większa pętla i wzmocnienie.
    * wrobieni-lecz-niewinni: Mawirowcy. Wycofali się w głąb Stacji, nie są już częścią większej grupy. Są poza prawem i Felina nie chce ich atakować.
* Dilemma: hurt or help?
* Detale
    * Crisis source: 
        * HEART: "Pierścień, zamieniający Obsesję w Energię"
        * VISIBLE: "Ludzie giną i dzieją się dziwne rzeczy - osiągają szczyt swoich możliwości"
    * Agendas (integrują Dark Future)
        * Artur Tavit: pomóc biosyntom, zintegrować je. ENDGAME(F1): stoi na środku, płomienna mowa, aspekty prawne, 'zastrzelony przez biosynta'.
        * Maia Sakiran (Teraquid): The Revenant; zniszczyć biosynty. ENDGAME(F1): wzmocniona Pierścieniem zabija Vanessę i Artura.
        * Estril Cavalis: The Protector; uratować Aerinę, uratować Stację przed magią, full lockdown. ENDGAME(F2): Patrick Zara (szalone rozkazy, pełna kontrola)
        * Pierścień: The Seeker; szuka protomaga do Aktywacji (Sia / Kalista). ENDGAME(F3): Sia/Kalista zakłada na palec Pierścień i się Aktywuje.
        * Poszukiwacz: The Seeker; szuka Pierścienia przebijając się przez ludzi na lewo i prawo. ENDGAME(F3): odstrzeliwuje Pierścień z palca Czarodziejki i go zakłada.
        * Aerina Cavalis: uratować Ojca, uratować Stację przez popularyzację. ENDGAME(F4): stoi z pistoletem i celuje w Agentów by dostać Pierścień (życie za popularyzację)

### Co się stało i co wiemy

Mamy do czynienia z kolejną sytuacją, gdzie wszyscy chcą dobrze, ale z uwagi na to jak działa Pierścień nie kończy się to tak dobrze. Marzenia zmieniają się w obsesje.

Droga Pierścienia (przed sesją):

* Darek Markin (Turysta): zginął na Arenie pokonany przez Mawirowca (tydzień temu)
* Fenters Dakk (Mawirowiec): zginął walcząc z biosyntami (3 dni temu)
* Vanessa (biosynt): zaniosła go nieświadomie do terenów kontrolnych (3 dni temu); TRANSFER
* Hans Fruht (admin): WCZORAJ umierał z uwagi na przeciążenie - pisał tom poezji (wczoraj)
* Maia Sakiran (Teraquid): analizowała ofiary biosynta. 
    * Starła się z Poszukiwaczem; Teraquid odegnał Poszukiwacza. AKTYWNA

Droga Pierścienia (sesja):

* Estril Cavalis: spotkał się z Maią odnośnie biosyntów na prośbę Aeriny; AKTYWNY
* Artur Tavit (Arystokrata): spotykał się z Maią by wyjaśnić że biosynt jest niewinny; AKTYWNY
* Ochroniarz Estrila, zabity przez Poszukiwacza; ale pierścień trafił do...
* Mawirowiec, próbujący przejąć kontrolę nad gangiem Mawira
* Kalista, odnajdująca Pierścień
* Poszukiwacz

### Co się stanie (what will happen)

* F0: TUTORIAL
    * "Biosynty muszą zostać zniszczone" - dane z kamery Mai
    * Zespół gra agentami Teraquid. Polują na Vanessę, która porusza się w korytarzach prowadzących w okolice terenów Rady
    * Konflikt: 
    * -> Poszukiwacz z Alteris, Vanessa, ładunek EMP uszkadzający sporo sprzętu, Vanessa miała coś do podsłuchiwania i dronę
* F1: Artur i Maia
* F2: Estril i lockdown
* F3: Kalista i Poszukiwacz
* SUKCES
    * Pozyskanie i neutralizacja Pierścienia
    * Neutralizacja Poszukiwacza
* Overall
    * chains
        * .
    * stakes
        * .
    * opponent
        * .
    * problem
        * .
    * key scenes
        * .

## Sesja - analiza
### Fiszki

.

#### Strony

.

### Scena Zero - impl

W scenie Zero zespół gra grupą agentów ochrony skupiających się na polowaniu na pojedynczego biosynta. Dociera do nich informacja, że biosynt najprawdopodobniej próbuje się dostać w okolice zamieszkałe przez członków Rady.

Agenci używają detektorów ruchu w połączeniu z detektorami ciepła i lokalizują biosynta, który ma konkretny cel i próbuje się dostać w tereny Rady. Biosynt (Vanessa) jest monitorowany - nie ma pojęcia, że jest śledzona przez ochronę. Ochrona monitoruje jej ruchy i zauważa, że Vanessa próbuje nawiązać kontakt z kimś w jednym z sekretnych pokojów. Dokładniej: z Aeriną.

Mimo rozkazów mówiących o konieczności eliminacji biosyntów, zespół decyduje się na to, by niczego złego nie robić. Vanessa kiedyś służyła Aerinie, nigdy nie robiła jej krzywdy. Tak więc nawiązanie kontaktu między tą dwójką nie stanowi szczególnego problemu.

Parę minut później znikąd pojawił się koleś w szarym płaszczu. Szmaciarz. Zaatakował Vanessę kataną. Vanessa wykonała kilka manewrów unikowych, strzeliła w szmaciarza i on znowu zniknął. Teleportacja na podstawie Alteris.

Dokładnie to zostało wysłane Agencji przez Felinę.

Tp +3

* X: ochrona nie zniszczy Vanessy
* V: mamy sposób monitorowania lokalizacji Vanessy
* V: Vanessa nie wie że jest monitorowana
* V: poznaliśmy agendę Vanessy i jej cel tutaj
* V: komunikacja przechwycona między Vanessą i Aeriną

### Sesja Właściwa - impl

Tydzień później, Agencja dociera na Stację.

Czyli mamy serię ludzi którzy w tajemniczy sposób ucierpieli przez efekty magiczne. 

* Młody chłopak, który chciał zaimponować swojej dziewczynie, wyszedł na arenę bić się z drakolitą, solidnie go pobił (ku wielkiej radości innych drakolitów), po czym zginął – ‘spalił się wewnętrznie’.
* Koleś pracujący w biurze, który zamknął się na kilka dni i w 3 dni napisał tom poezji, znaleziono go umierającego i lekarzowi nie udało się go odratować.

Na to wszystko nakładamy Szmaciarza - teleportującego się człowieka o nieznanych umiejętnościach i agendzie. Felina zrobiła podstawowe badania - udało jej się określić, że Szmaciarz nie jest członkiem załogi stacji. To kompletnie nowa osoba, turysta.

Oficer naukowy decyduje się połączyć fakty i dane by wyciągnąć informacje o kolejności, wspierany przez dane od hakera. Co się działo w jakiej kolejności?

Tr Z+3:

* V: Kolejność osób o jakich wiedzą:
    * chłopak z areny (Malik) - drakolita który go pokonał (Stefan) - Vanesssa (i szmaciarz) - biurokrata z tomikiem poezji
* X: Kalista się uruchamia i robi Dark Movement
* X: Estril skupia się na córce, ALE info: Stefan walczył świetnie
* V: Energia Esuriit

Łącząc dane od hakera z analizą oficera naukowego udało im się dojść do jakiejś logicznej kolejności wydarzeń:
Pierwszą osobą, o której wiemy, że została ‘zarażona’ jest chłopak który chciał pochwalić się dziewczynie. Skopał gladiatora na arenie. Potem ten gladiator poszedł polować na biosynty, uszkodził Vanessę. Vanessa nie ma mechanizmów naprawienia się na tej stacji. Vanessa poszła do Aeriny, tam napotkała Szmaciarza (scena zero). Nie dotarła do Aeriny; jeszcze bardziej uszkodzona przez szmaciarza schowała się z jednym z biur by przeczekać. Parę godzin później opuściła to biuro, po czym parę godzin później do biura wszedł biurokrata. To była kolejna osoba zarażona i ostatnia o której wiemy.

Dużo wskazuje na to, że Vanessa zaraża i została zarażona. Ale nic jej się szczególnego nie stało - co pasuje, bo magia nie działa na biosynty w ten sam sposób jak na ludzi. Czyli Vanessa może być czysta.

(w tle: tymczasem Kalista zaczęła łączyć fakty i informacje, dowiadując się dużo więcej o naturze drugiej strony; dowiedziawszy się że agencja wróciła na stację, zdecydowała się działać przed nimi)

(w tle: tymczasem Estril jest już mocno dotknięty przez Pierścień; ogranicza jakikolwiek dostęp do swojej córki i zapewnia, żeby stacja była maksymalnie bezpieczna)

Po przeanalizowaniu dodatkowych danych, jest jednoznaczny wynik - gladiator walcząc z Vanessą walczył za dobrze. A Szmaciarz doskonale wie gdzie znajduje się źródło anomalii. 

Agenci mieli bardzo dobry pomysł - porozmawiać z rodziną pierwszego chłopaka, ale z uwagi na to że wszyscy Agenci są zajęci ten pomysł nie został zrealizowany.

(kontynuacja)

* X: Pierścień zmienia ofiarę
* Vz: Namiot i prawo do badania
* X: Jonatan zabija ochroniarza
* V: Estril współpracuje (a Aerina jest czysta)
    * niestety tydzień zajmie wyczyszczenie / wyleczenie wszystkich
* V: Aerina współpracuje, ALE Vanessa odmiawia współpray (chroni Artura)
* (poddanie konfliktu, sami tam pójdą)

Mając dostęp do odpowiedniej ilości zwłok osób zniszczonych przez magię oraz wiedząc jak ta magia działa, oficer naukowy określił z czym mają do czynienia – energia Esuriit. Tym razem manifestuje się jako ambicja, nieposkromiona chęć osiągnięcia celu kończąca się śmiercią użytkownika.

Agenci uważają, że jest możliwość zatrzymania tego, ale muszą móc skomunikować się z Aeriną. Uważają, że ona może być zarażona przez Vanessę. A nawet jak nie, ona powinna wiedzieć dużo więcej na ten temat zwłaszcza że miała do czynienia z tym typem energii w przeszłości (była zarażona Esuriit i agencja jej pomogła). Jej ojciec jednak (Estril) nie pozwala na dostęp do dziewczyny.

Gdy zespół rozmawiał bezpośrednio z radą, oficer naukowy przyniósł (krewetkowy detektor). Były tam dość zabawne sytuacje – Larkus bał się zostać ugryziony przez krewetko-pijawkę, ale wszyscy poddali się badaniom. Okazało się, że Estril jest zarażony. To pomaga zrozumieć dlaczego tak bardzo chroni swoją córkę.

Udało się przekonać go do tego, by jednak pozwolił zespołowi porozmawiać z córką (Aeriną). Muszą postawić namiot (decontaminant), przeprowadzić procedurę odkażającą i wtedy będzie taka możliwość. Udało im się nawet przekonać go do tego, by poszedł z nimi na Luminarius - bo w ten sposób będzie w stanie uratować swoją córkę, bo jak jego uda się odkazić, ona nie zostanie zarażona.

Aerina przyjęła agencję bardzo ciepło. Powiedziała o czym rozmawiała z Vanessą -Vanessa chciała ją chronić za wszelką cenę, bo znowu dzieje się coś bardzo dziwnego w okolicy. Chciała zawiązać stałe połączenie audio. Vanessa stale monitoruje okolicę i chroni na zmianę Aerinę oraz Artura.

Gdy agencja powiedziała Aerinie jak wygląda sytuacja i jak bardzo jest niebezpieczna - że znowu mają do czynienia z energią typu Esuriit – Aerina zdecydowała się współpracować z pełni sił na pełni umiejętności. Poprosiła audio Vanessę o przybycie; Vanessa powiedziała jednak, że Artur jest zagrożony. Musi go chronić. Potem może przyjść. Jako, że zdaniem zespołu Artur jest kolejnym potencjalnym nośnikiem, Sabotażysta został wysłany by porwać (uratować) Artura oraz Vanessę.

Sabotażysta podchodzi do problemu porwania / uratowania Artura:

* V: hacker gasi światło, Artur jest uratowany od asasyna
* X: Vanessa zniszczona przez asasyna
* V: Artur, Aerina, zniszczona Vanessa, Estril => Luminarius
* X: Maia jest Esurientem; randomowe ostrzeliwanie się, ma ofiary śmiertelne i ranne
* X: Anomalia w rękach Kalisty
* V: Maia unieszkodliwiona

Artur robi ogromne wystąpienie w parku, który najbardziej zbliżony jest do sceny. Z perspektywy sabotażysty? Idealne miejsce na zastawienie pułapki lub na zabicie kogoś. Sabotażysta i haker gaszą światło i sabotażysta natychmiast przechwytuje Artura, podejrzewając, że Artur jest zarażony i będzie jakiś problem. O dziwo - strzał z karabinu szturmowego. Sabotażysta osłania Artura ale zanim zdążył coś z tym zrobić, asasyn atakuje ponownie.

Tym razem do akcji ruszyła Vanessa. Rzuciła się na zabójcę, próbując zasłonić sobą Artura. Ciężko uszkodzona już wcześniej, nie była w stanie nic innego zrobić. Zabójca zniszczył Vanessę do poziomu niemożliwego do naprawy. Ale Vanessa skutecznie kupiła czas sabotażyście na wyciągnięcie Artura. Widząc sytuację, Hacker natychmiast ewakuowała Aerinę.

Sabotażysta zaczął ostrzeliwać się z zabójcą; zabójca nie dbał o to kto będzie ranny, więc seria poszła po tłumie i sporo osób zostało rannych a kilka zginęło. W końcu sobota wyjście udało się trafić w oba ramiona zabójcy i zabójca – Maia - też trafiła w ręce agencji.

Hackerka przekonuje Kalistę do pomocy:

Tr (niepełny) + 3:

* V: Kalista pomoże - mówi o pierścieniu i co się dowiedziała
* (+3V): V: Są dowody na istnienie pierścienia na rannych ORAZ Kalista się wstrzyma bo problemy

Widząc sytuację, Hackerka kontaktuje się z Kalistą. Czego nikt nie wie, Kalista ma na palcu Pierścień. To sprawia, że Kalista jest jeszcze bardziej chętna pomocy Agencji niż zwykle. Poproszona Kalista wyjaśnia jak wygląda sytuacja po tym jak jej troszkę posłodzono i pokazano jak sytuacja jest niebezpieczna dla ludzi (bo są zabici i ranni).

Kalista wyjaśniła, że na bazie jej obserwacji wszystko jest powiązane z dziwnym pierścieniem. To co nie pasowało do żadnego wzoru to był właśnie ten pierścień - wszyscy którzy raz coś mieli do czynienia z tą energią mieli taki pierścień na palcu. Ale nie wtedy kiedy był robiony problem a wtedy kiedy faktycznie doprowadzało do problemu z Esuriit.

To by wyjaśniało w jaki sposób Vanessa przekazała pierścień biurokracie - gdy była ukryta w pokoju, pierścień musiał się zsunąć. I faktycznie, po badaniach ciał osób okazało się, na palcu był ślad po pierścieniu. Ale Estril, przepytany przez Zespół, powiedział że nie kojarzy żadnego pierścienia. 

Faktycznie - wszystko wskazuje na to że pierścień jest niewidoczny metodami elektronicznymi (kamery itp.) oraz posiadacz go nie widzi. Ale wszyscy inni tak.

Nagroda za informacje o pierścieniu, Estril ogłasza wspierany przez Radę

Ex Z+3:

* X: Jonatan (Szmaciarz) też wie o pierścieniu i lokacji
* X: Jonatan da radę odciąć palec Kaliście
* V: Info od gangmana

Zespół postanowił wykorzystać fakt, że mają wsparcie Rady. Estril ogłosił nagrodę za pojawienie się pierścienia - wszelkie informacje na jego temat. Z uwagi na to, że Mawirowcy nie są fanami rady ani dowodzenia stacją, niekoniecznie chcieli pomagać. Ale jeden gangster zdecydował się pomóc z uwagi na możliwości nagrody personalnej.

Skąd traktował się z Agencją i powiedział, że jeżeli zabiorą go z tej stacji, dadzą mu roczny obiad oparty na mięsie oraz rok mieszkania w którym ma jeden pokój i kibel dla siebie, to powie wszystko co wie łącznie z lokalizacją tego pierścienia. Dla agencji jest to niski koszt.

Pierścień znajduje się na palcu Kalisty. Czas się przejść do inżynierii ludzi Mawira. A tam – sam Mawir Hong, 2 drakolitów i Kalista, pracująca nad czymś. Mawir podchodzi do agencji agresywnie, nie chce im pomóc i chce by sobie poszli. Kalista przekonuje go, że to tym razem poważna sprawa i nie zamierzają robić nic złego. 

Hackerka, która już dobrze współpracuje z Kalistą, rozmawia z nią i tłumaczy o pierścieniu. Kalista nie widzi i nie postrzega pierścienia. Mawir jest poważnie zdziwiony - on pierścień widzi. Kalista nie. Mawir mówi, że Kalista chyba żartuje. Kalista jest coraz bardziej zagubiona, co Mawir postrzega.

Czyli ktoś krzywdzi Kalistę. I to nie jest agencja.

Mawir jest już poważnie zmodyfikowany - pożarł jakąś anomalię w przeszłości. Chce chronić Kalistę.

Tp +3: 

* V: Mawir pomoże Agencji dorwać Jonatana, jeśli może pożreć rękę Jonatana na surowo w imię Saitaera

Znikąd pojawia się Szmaciarz i atakuje Kalistę kataną. Kalista głośno krzyczy - jej palec, na którym jest pierścień pada na ziemię. Mawir rzuca się na Szmaciarza (Jonatana) i próbuje rozerwać go gołymi rękami. Szmaciarz ma wsparcie Alteris, ale Mawir jest zasilany ixionem i jest brutalną maszyną zagłady w walce. Szmaciarz rani go kataną, Mawir wbija zęby w jego ramię i odgryza kawał mięśnia. Jonatan teleportuje się w szoku, Mawir w szale bojowym wstaje, unosi głowę i zaczyna wypijać krew. Po chwili widzi w jakim stanie jest Kalista, bierze ją do lekarza. Jonatan poczeka – Mawir się zregeneruje, Jonatan nie...

Hackerka podchodzi i próbuje podnieść pierścień i wsadzić go do lapisowanego pudełka. Pierścień jednak wysyła do niej podszepty - pokazuje jej, że może spełnić jej marzenia.

Tr Z+2:

* Vz: Pierścień w pudełku
* V: Mawir odniesie Kalistę do lekarza zamiast się pieprzyć z nimi czy wchodzić w głąb ixionu

Tymczasem Oficer Naukowy na Luminariusie próbuje naprawić Estrila i znaleźć sposób jak pomóc innym ofiarom Esuriit.

Tr +3 +3Ob:

* X: Aerina robi coś głupiego
* V: Da się uratować narzędziami Luminariusa, ale nie będzie to miłe ani przyjemne

Czyli sytuacja jest pod kontrolą. Oficer Naukowy dowiedział się też jaka była rola Mai w tym wszystkim - ona zaraziła się pierścieniem, gdy badała u biurokraty czy biosynt stał za jego śmiercią. A Maia obsesyjnie nienawidzi biosyntów.

Gdy reszta agencji wróciła na Luminarius z pierścieniem, Aerina zrobiła coś głupiego. Wycelowała pistoletem w agentów i poprosiła ich by oddali jej pierścień. Agenci widzą, że ona nie do końca wie, jak celować - ręce jej się trzęsą i najpewniej będzie bała się strzelić. Może po prostu nie mieć wprawy.

Zapytali Aerinę łagodnie, po co jej ten pierścień; czy nie widzi ile zła on wyrządził. Aerina odpowiedziała przez łzy, że to jedyny sposób by mogła ściągnąć więcej ludzi na stację. A im więcej ludzi tym mniejsza szansa że jej tata ucierpi ponownie. Vanessa nie żyje. Jej tata jest ciężko ranny. Ona coś musi zrobić.

Agenci wyjaśnili jej, że to spowodowałoby jeszcze większy smutek ojca. Udajmy że nic głupiego się nie stało i niech odda broń. Oddała, ku wielkiemu zdziwieniu agentów. Aerina nie wie co robić.

Ale faktycznie szkoda, że Vanessa została zniszczona i zostało tylko 6 biosyntów. Sabotażysta zaproponował, by zrobić pomnik dla biosyntki. Niech Kalista rozpropaguje (odpowiednio zmodyfikowane nagranie przez hakera) które pokazuje, że Vanessa próbowała obronić wszystkich ludzi przed szalonym zabójcą. I w ten sposób zginęła jako bohaterka.

Połączone siły Agencji oraz Kalisty sprawiają, że pojawia się możliwość integracji biosyntów z resztą stacji.

Tr Z +3:

* V: nagranie sprawia, że poprawia się opinia biosyntów
* V: biosynty zostaną zintegrowane z resztą stacji na przestrzeni pół roku

A Kalista straciła palec i została przez pierścień Aktywowana.

## Streszczenie

Na Stacji pojawił się pierścień Esuriit, który sprawia, że ludzie potrafią poświęcić energię do spełnienia obsesyjnych marzeń kosztem życia tych ludzi. Za pierścieniem przybył Szmaciarz, próbujący dodać pierścień do swojej kolekcji artefaktów. Zespołowi udało się zatrzymać Skażenie i uratować członka rady - stracili jednak Vanessę (biosyntkę) oraz Kalista została Aktywowana. Za to zespół zintegrował biosynty z resztą stacji.

## Progresja

* Kalista Surilik: trwale straciła palec, ale pierścień Esuriit Aktywował jej moce protomaga. Od teraz musi nosić rękawiczkę - jej palec NIGDY nie zadziała.
* Mawir Hong: dotknięty przez ixion, stał się bardzo niebezpiecznym wojownikiem; pożera żywcem anomalie i to go wzmacnia w imię Saitaera
* Vanessa d'Cavalis: uznana za bohaterską biosyntkę stacji; w jej imieniu zbudowano pomnik integracji biosyntów i ludzi na Szernief

## Zasługi

* Klasa Sabotażysta: uratował Artura i ostrzelał Maię; zaproponował pomnik dla Vanessy by zintegrować biosynty z ludźmi
* Klasa Oficer Naukowy: uratował Estrila, wyciągnął kolejność wydarzeń i kto-kiedy, opracował odpowiedni detektor i upewnił się, że problem zniknął
* Klasa Hacker: poza standardową integracją strumieni danych, przekonała Kalistę do współpracy i containowała niebezpieczny Pierścień
* Kalista Surilik: doszła do tego, że za wszystkim stoi tajemniczy Pierścień; współpracuje z Agencją, bo to mniejsze zło. Straciła palec do Jonatana i nosiła Pierścień, co ją Aktywowało.
* Aerina Cavalis: wpierw chroni ją Vanessa, potem ojciec. Straciła Vanessę. Celuje pistoletem w Agenta, by oddali jej pierścień; z jego pomocą może uratować stację, nawet kosztem swego życia, by jej ojciec nie ucierpiał. Przekonana by oddała broń; wszyscy udają że nic się nie stało.
* Mawir Hong: chroni Kalistę, ale jak Agencja przekonała go że jej coś jest, to pomógł Agencji. Walczył jak równy z równym z Jonatanem i wyżarł mu fragment barku. Niebezpieczny.
* Vanessa d'Cavalis: po pokonaniu mawirowca Esuriit który ją uszkodził poszła do Aeriny by ją ostrzec - tam uszkodził ją Jonatan. Chroni Artura i osłoniła go przed Maią (zabójczynią). Przyjęła jej pocisk na siebie i została zniszczona. KIA. W jej imieniu zbudowano pomnik i zrobiono integrację biosyntów i Szernief.
* Felina Amatanir: wezwała Agencję rozpoznając Alteris w nagraniach o Szmaciarzu. Silnie przekonywała Estrila do współpracy z Agencją. Sprawdziła i dostarczała dossier ludzi na stacji. Kompetentna i skuteczna, acz w roli wsparcia.
* Estril Cavalis: zarażony przez Pierścień, próbuje rozpaczliwie osłonić Aerinę (i zamknął ją w pokoju). Współpracuje z Agencją, bo to jedyny sposób by ratować córkę. Wyleczony przez Luminariusa.
* Larkus Talvinir: nalega, by wszyscy przebadali się przez Agencję i współpracowali i NADAL dogryza Estrilowi, że jego córka została kanibalem. Mdleje, gdy liże go krewetkowy detektor.
* Artur Tavit: NIE dotknięty przez Esuriit, robi gorące apele i mowy by zintegrować biosynty. Maia chciała go zabić, lecz Agencja go uratowała. Jego marzenie spełnione - biosynty zintegrowane. Ale jego lojalna Vanessa została zniszczona.
* Maia Sakiran: fanatycznie nienawidząca biosyntów agentka Teraquid, opętana przez Pierścień zniszczyła Vanessę, zabiła kilka osób ze Stacji i została zabrana przez Luminarius na leczenie.

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Kalmantis
            1. Sebirialis, orbita
                1. CON Szernief

## Czas

* Opóźnienie: 191
* Dni: 2
