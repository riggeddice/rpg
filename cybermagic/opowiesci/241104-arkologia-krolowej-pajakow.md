## Metadane

* title: "Arkologia Królowej Pająków"
* threads: agenci-arkologii-araknis
* motives: the-resurrector, the-hunter, burza-magiczna, birutan, ghost-town, petla-magia-emocje, kaskadujaca-fala-emocji, dobry-uczynek-sie-msci, misja-ratunkowa, misja-survivalowa, potwor-z-horroru, lokalne-mity-i-legendy, energia-interis, energia-lacrimor
* gm: żółw
* players: kić, fox

## Kontynuacja
### Kampanijna

* [241027 - Szafirianin znalazł starą minę](241027-szafirianin-znalazl-stara-mine)

### Chronologiczna

* [241027 - Szafirianin znalazł starą minę](241027-szafirianin-znalazl-stara-mine)

## Plan sesji
### Projekt Wizji Sesji

* INSPIRACJE
    * PIOSENKA WIODĄCA
        * [Digital Daggers - The Devil Within](https://www.youtube.com/watch?v=O3UuqCN1sQs)
        * "I'll keep quiet | You won't even know I'm here | You won't suspect a thing"
        * "I made myself at home | In the cobwebs and the lies | I'm learning all your tricks | I can hurt you from inside"
        * "I'll be here | When you think you're all alone | Seeping through the cracks | I'm the poison in your bones..."
        * Reprezentuje Królową Pająków. Reprezentuje jej Agendę, jej istnienie
    * Inspiracje inne
        * "Network Effect", Martha Wells. "Główny" przeciwnik.
* Opowieść o (Theme and Vision):
    * "_Orbiter próbuje uratować wszystkich. Ale nawet Orbiter nie wie o wszystkim na Neikatis..._"
        * Burza i legenda Araknis stają się antytezą wszystkiego w co Orbiter wierzy.
    * "_Czym jest siła w obliczu żywego koszmaru?_"
        * Królowa Pająków jest niesamowicie niebezpieczną istotą, której w Burzy Magicznej chyba nie da się pokonać samą siłą ognia
        * W obliczu tego typu potwora jedyne co da się zrobić - morale, ochrona?
        * Ludzkość w obliczu niemożliwego.
    * "_Czy mamy prawo spróbować przetrwać wzmacniając lub tworząc Królową Pająków?_"
        * Im dłużej jesteśmy w Arkologii Araknis, tym groźniejsza staje się Królowa Pająków. I tym bardziej przerażeni są neikatianie.
        * Mamy prawo przeżyć? Zabicie neikatian i silne morale Orbitera powinno pomóc...
    * Lokalizacja: Neikatis
* Motive-split
    * the-resurrector: sztuczna Agenda, wywołana przez wierzenia neikatian i próbująca przywrócić legendarną Królową Pająków do życia w Araknis. Istotę z legend, która nigdy nie istniała.
    * the-hunter: Królowa Pająków. Poluje, próbuje usunąć delikatne osoby. Wykorzystuje birutanów do odwrócenia uwagi i "zniknięcia" ludzi.
    * burza-magiczna: potężne wyładowanie na Neikatis, które uderzyło w ruinę arkologii Araknis i doprowadziło do materializacji koszmarów znajdujących się tam ludzi.
    * birutan: horda birutanów traktująca Arkologię Araknis jako swoją bazę i sprowadzająca ludzi do Araknis. Jedna z przyczyn legendy o Królowej Pająków. Większość nieaktywna.
    * ghost-town: Arkologia Araknis, częściowo zniszczona i opuszczona, uszkodzone miejsce pełne wspomnień. Miejsce schronienia przed burzą magiczną gdzie porwano ludzi.
    * kaskadujaca-fala-emocji: zarówno lokalsi jak i agenci Orbitera zaczynają się coraz bardziej stresować. Oni plus pole magiczne zaczynają "sprowadzać" Królową Pająków.
    * petla-magia-emocje: rzeczywistość materializuje Legendę Królowej Pająków. Od najbardziej podstawowego oddziału aż do pełnej Królowej Pająków.
    * dobry-uczynek-sie-msci: Orbiter próbuje uratować grupę ludzi porwaną przez birutanów do Arkologii Araknis. Przetrwają tam burzę magiczną. Ale nie wiedzą, z czym mają do czynienia...
    * misja-ratunkowa: Orbiter próbuje uratować grupę ludzi porwaną przez birutanów do Arkologii Araknis. I niestety zbliża się burza magiczna.
    * misja-survivalowa: mimo potęgi sprzętu Orbitera i wyszkolenia trzeba przetrwać burzę magiczną w Arkologii Araknis. I to jest katastrofa, między materializacją emocji, efemerydami, birutanami i samym miejscem.
    * potwor-z-horroru: Manifestacja "Królowa Pająków" arkologii Araknis. Nie jest prawdziwą istotą, jest manifestacją strachów i legend.
    * lokalne-mity-i-legendy: legenda o Królowej Pająków, przerażającym potworze Arkologii Araknis, która dla neikatian jest najgorszym złem w okolicy.
    * energia-interis: Koniec Wszystkiego, koniec snów o potędze Orbitera. Orbiter jest niczym na Neikatis, stając naprzeciw Legendarnej Królowej Pająków podczas Burzy Magicznej
    * energia-lacrimor: Mroczna Intensyfikacja, brutalna prawda o nicości i maksymalizacja przeżyć; Arkologia Araknis złamie wszystkich. Groteska, brud oraz jesteśmy tylko mięsem. Atomizacja i izolacja, mierzenie się z koszmarami.
* O co grają Gracze?
    * Sukces:
        * Uratować jak najwięcej neikatian (21 osób)
        * Pozbyć się Królowej Pająków
        * Przetrwać jako siły Orbitera (11 osób)
    * Porażka: 
        * Manifestacja Królowej Pająków
        * Ratunkowa ewakuacja agentów Orbitera przez pilota 
* O co gra MG?
    * Highlevel
        * Pokazać "Przeklętą Arkologię Araknis".
        * Pokazać, że Orbiter z całym high-tech nie jest w stanie pokonać burzy magicznej. Może tylko przetrwać.
        * Obudzić Królową Pająków.
    * Co uzyskać fabularnie
        * Zabić wszystkich poza potencjalnie Erykiem, Sylwią i Annabelle
        * Doprowadzić do manifestacji Królowej Pająków
        * Doprowadzić do przebudzenia Królowej Pająków
        * Częściowo zreanimować Arkologię Araknis
* Agendy
    * The Resurrector: chce przywrócić Królową Pająków.
    * The Hunter: chce zabijać, krok po kroku, ludzi. Chce siać terror i eksterminować. Jak Predator z "Predatora".
    * Neikatianie: chcą przetrwać, znajdują się w miejscu legend i koszmarów w ich pojęciu.
    * Orbiter: chce uratować ludzi. Jest przekonany o swojej sile ognia
    * Birutan: zasilić Królową Pająków, pozyskać jak najwięcej biomasy i technologii.

### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

KONTEKST:

* .

CHRONOLOGIA:

* .

PRZECIWNIK:

* .

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

Faza 0 (19:30 - 20:00): KRĄŻOWNIK EVENTARES

* Tonalnie
    * optymizm, kompetencja, camaradery
* Wiedza
    * Postacie są różne ale ogólnie chcą dobrze dla Orbitera
    * Zaznajomić z ciężkim krążownikiem Eventares i Grupą Wydzieloną 
* Kontekst postaci i problemu
    * Pokazać Fox kluczowe osoby
* Implementacja
    * Eryk składa dla Sylwii małe działko na kapiszony; bo może
    * Gabriel podrywa Annabelle niemiłosiernie
    * Darek informuje, że koniecznie muszą pomóc miejscowym; jest niebezpieczna arkologia birutanów
        * Tristan "to tylko neikatianie"; Darek "bez nich Orbiter tutaj nie ma długoterminowych szans powodzenia"
    * Feliks eggs Lareth. Lareth ignores.

#### Faza 1 (20:00 - 21:00): PRZED BURZĄ

Tonalnie: Nadzieja, operacja wojskowa

SPLIT 3:

1. Dowódca + noktianie, neikatianie
2. Tristan + Feliks + Elizawieta + Mateusz
3. Dziewczyny, Eryk, Marcin, Marian Czeresiński

Co znajdzie Zespół:

* kot: 
    * bardzo trudny teren plus poukrywane birutany
    * są ślady że coś inteligentnego tu jest i działa
    * ślady, że arkologia zginęła przez wojnę domową?
    * BARDZO dużo birutanów, ale większość nieaktywna.
* fox: Echa częstych burz magicznych
    * nie ma śladów "legendarnej królowej pająków" ani super anomalii?
    * relacja magia - birutan; tu musi gdzieś być źródło energii magicznej

.

* Holograficzne Dzienniki Mieszkańców
* Dane z systemów komputerowych
* Ratujemy neikatian
    * **Borys Karganow**
        * jest w recyklitorium; jeszcze żyje, jest ranny i chronią go 3 birutany. Dwóch innych już Rekonstruktor przerobił.
    * "Zbliża się burza magiczna; przeczekamy"
    * Farma żywności; pod ziemią
        * sporo ciał, gdzie znajdują się dziwne grzyby i inne porosty?
        * **dwie nieprzytomne, ale żywe osoby**.
    * **Olga**
        * jest odizolowana; "dziwny birutan" ją zamknął poza dostępem. Ale włączyła SOS.
        * trudno się przedostać przez rozpadający się labirynt korytarzy, ale jest tam. To zajmie chwilę.
        * birutany próbują się do niej dostać ale nie mają jak

Spotkanie Zespołu. Burza magiczna. Przetrwamy w Arkologii. Fortyfikujemy perymetr.

#### Faza 2 (21:00 - 22:00): BURZA I PAJĄKI

Tonalnie: Interis (bezsilność), Lacrimor (groteska)

* Atak birutanów; perymetr nie jest dość osłonięty i burza zaczyna zbliżać się do arkologii.
* Znika Wojciech. Po prostu był i nie ma.
* Konieczność zejścia w głąb arkologii; ale tam teren jest gorszy i nie da się osłaniać
    * zakamuflowani Recyklerzy wyskakują, próbując złapać i rozerwać, atakując szponami i mechanicznymi szczękami "ze ścian i gruzu"
    * pod wpływem ciężaru korytarze zaczynają się rozpadać; spadamy w dół. Lub coś na nas spada.
    * atak ze strony osoby która kiedyś była sojusznikiem; Rekonstruktor ją zdążył zrekonstruować.
* Pojawia się nietypowy Birutan; szybszy, lepszy i "myślący taktycznie"
    * WPIERW wysadza Skorpiony
    * POTEM podkłada ładunki do zabicia Zespołu
* Pojawiają się w głębi symbole pająków i sigile wskazujące na obecność "Królowej Pająków"
    * Słychać szepty
        * "Wiedzieliście, że tu jestem. A jednak przyszliście. Witajcie w pajęczynie."
        * "Moje dzieci są tak bardzo głodne..."
            * spawn cyberspiders
        * "SZAFIRIANIN UMARŁ PRZEZ TO, ŻE WYSZŁAŚ ZA PÓŹNO W PRZESTRZEŃ..."
        * "Sparaliżowany... pająki wchodzą przez usta... będzie żył długo..."
    * Znikają osoby; SHE HUNTS
    * **BARDZO rośnie panika**
        * próba samobójstwa?
* Burza powoduje "znajome efekty"
    * gęsta, czerwonawa mgła. To chmura pełna cząsteczek rdzy i magicznego skażenia,
    * silne zakłócenia elektromagnetyczne. Przerywają one łączność, zaburzają sensory i powodują problemy z napędem
    * dust cloud; ludzie się duszą, SHE HUNTS
    * "what's the matter..." -> staje, dusi się, krew z oczu i ust... DARK EMERGENCE
* Zespół się broni
    * Eryk przygotowuje perymetr, działka i rekonstrukcję terenu
    * Tristan "ścigamy się w zabijaniu". RATATATA.
    * Lareth on-alert. Skanuje teren.
    * Feliks "Orbiter rozwali waszą królową pająków, zobaczycie"
    * Elizawieta "Dobra, KIM jest Królowa Pająków, powiedzcie mi co wiecie o niej"
    * Mateusz "Supplies, medical"
* Neikatianie panikują
    * Marian Czeresiński "Ona tu jest, nie możemy zejść w głąb!"
    * Borys Karganow "Wsadzi w nas pająki i pożywimy jej dzieci; ten dziwny birutan..."
    * Dymitr Śledziewski "Spierdalaj, SPIERDALAJ i zamknij mordę, rozumiesz!?"

#### Faza 3 (22:00 - 23:00): SHE IS HERE

* sama arkologia zmienia część ruin w kończyny pająka atakującego z zaskoczenia
* 'wskrzeszenie' magiczne części birutan, zwłaszcza po odłamkowym pocisku
* rana się nie goi, raczej pokrywa się technopająkami wychodzącymi "od środka". HORRIBLE DEATH.
* SHE STALKS, SHE HUNTS, she is visible
* SHE KILLS (dziwny Birutan)
* SHE PULLS PEOPLE THROUGH WALLS. Agonalne krzyki.
* Eryk / Darek się orientują co się dzieje. Zabić wszystkich?

## Sesja - analiza
### Fiszki
#### Orbiter; grupa ma 15 osób z Orbitera

* Eryk Komczirp: inżynier + solutor, agent Orbitera
    * Bohater Pozytywny: zawsze na froncie, zawsze pomocny, każdemu poda rękę.
* Gabriel Tabacki: najlepszy pilot Orbitera w okolicach Neikatis (swoim zdaniem); ma opancerzony prom "Emmanuelle"; 10 osób (atmosfera - kosmos) 
    * As Pilotażu: doskonały pilot, niezwykle odważny i precyzyjny, ŻYJE dla latania w trudnych warunkach
    * Zuchwały Chwalipięta: nieprawdopodobnie optymistyczny, flirtuje z każdą spódniczką, przekonany o swojej epickości
* Darek Artycki: pro-neikatiański dowódca operacji Orbitera, porucznik, pro-integracja Orbiter - Neikatis
    * Niezależny Wizjoner: zmienia świat po swojemu; jest zdecydowany podejmować ambitne ryzykowne decyzje i nie boi się trudnych rzeczy
* Tristan Karbińczyk
    * Królowa Kliki: Orbiter i tylko Orbiter poradzi sobie z każdym zagrożeniem z jakim się spotkamy. Testuje innych.
    * Perfekcyjna Forma: przystojny, silny i wygląda jak exemplar Orbitera. Lubi walkę wręcz i ciężką broń.
* Feliks Pasikonik
    * Ognista Pasja: bardzo wrażliwy na nastroje; silnie rywalizujący. Głośny i obecny wszędzie. Pragnie strzelać do birutan!
* Elizawieta Barwiczak
    * Ciekawski Eksplorator: rzadko działa na planecie, jest to okazja na coś niesamowicie ciekawego.
* Mateusz Drożdżołebski
    * Broken: nie chce być na wygnaniu Neikatis; tu nie ma niczego ciekawego dla Orbitera
    * Medyk.
* Gaius Venisen: post-noktiański, dekadiański Orbiterowiec.
    * Szkiełko i oko: da się opanować Neikatis, lokalne mity i legendy są mało istotne. Da się współpracować z neikatianami.
* Lareth Karirian: post-noktiański, dekadiański Orbiterowiec, wieczny cień Gaiusa.
    * Mroczny Samotnik: mało mówi, nie wdaje się w rozmowy ani kłótnie. Wykonuje polecenia i chroni - zwłaszcza Gaiusa.
    * Weteran: najwyższej klasy eksterminator, najwyższa klasa kompetencji.
    * Cyberprotezy: 30% jego ciała stanowią zaawansowanej klasy mechanizmy i cybernetyka.
* Marcin Psiamęczok: naukowiec Orbitera, próbuje zrozumieć Neikatis
    * Żelazna Rutyna: krok po kroku, wie jak podejść, metodą naukową
* OO Eventares: ciężki krążownik, 650 załogantów, wydzielony do zapewnienia interesów Orbitera na Neikatis. Trzon grupy wydzielonej "Neikatis C7".

#### Neikatianie z Salvagera Czereśnia (nie widzieli, ale fajna nazwa) (5 osób)

* Jolka Czeresińska: dowódca 
    * Optymistyczny: "Jesteśmy w stanie uratować naszych ludzi!"
* Marian Czeresiński
    * Lękliwy: "To bardzo zły pomysł, ONA tu jest"
* Ulryk Czeresiński
    * Przesądny: "Ona tu jest. Ona jest zawsze, wszędzie i nas zniszczy. Tych ludzi nie da się uratować."
* Nataniel Marczkin
    * Niszczycielski: "Nareszcie mamy siłę to wszystko rozwalić i uratować naszych ludzi!"

#### Neikatianie, porwani (11 żywych)

* Larisa Zdanowicz
    * Nawigatorka Skorpiona; boi się Burzy Magicznej
* Dymitr Śledziewski
    * Racjonalny, nie odzywa się pierwszy
    * Przemytnik; boi się panicznie Królowej Pająków
* Borys Karganow 
    * Ekspert od lokalnych legend; boi się że NIE umrą
* Leonard Gurczow
    * Zrezygnowany, "wszystko to tylko pył w kosmosie." 
* Valius Amarenit
    * Nieustępliwy i brutalny
    * Kiedyś z oddziału Dolor; próbował znaleźć bezpieczne życie na Neikatis
* Olga Ondycka
    * Drapieżna Negocjatorka: Ekspertka od negocjacji, która zawsze wychodzi na swoje, zwłaszcza z trudnymi sojusznikami.

### Scena Zero - impl

Krążownik Eventares. To jednostka Orbitera nad Neikatis.

Annabelle ma pecha. Spędza czas z członkami załogi i zaczepia ją Gabriel. Chce z nią polecieć przy Neikatis swoim pancernym promem. Wpadło jeszcze kilka osób; Annabelle planuje zrobienie czegoś więcej.

Sylwia rzeźbi w drewnie. Eryk, z filuternym uśmiechem. Ma coś dla niej. Pudełko zaręczynowe. "Nie wyjdę za Ciebie", strzelił raka. Nie to. Armata.

Darek odmówił prośbie Annabelle. Powiedział, że arkologia.

* Tristan: Pomagamy miejscowym. Marnujemy czas.
* Sylwia: Pomóc zawsze warto
* Annabelle: Kontakt z miejscowymi, ściągnąć różne rzeczy z planety..? przydają się osobiste kontakty
* Tristan: Annabelle, Ty jesteś z Orbitera, Ty nie rozumiesz; jesteś osobą wysokiej klasy.
* Feliks: Moment, nie mogą wszystkich rozwalić, musimy postrzelać do birutan!
* Tristan: (przewrócił oczami)
* Annabelle: Zakładnicy, wielka bomba nie załatwi sprawy. A ludzie to osobiste kontakty i ludzie pomogą. Np. alkohol na imprezy.
* Annabelle: Ja bym sprawdziła alkohol z Neikatis

Darek tłumaczy:

* Jest Arkologia, o nazwie... w sumie nie wiem. Miejscowi nazywają ją Arkologią Araknis.
* Parę h temu birutanie porwali grupę neikatian -> Araknis
* Podobno oni żyją. Podobno... birutanie nie zabijają na miejscu. Potrzebują ich żywych.
* Połączymy siły ze Skorpionem Neikatiańskim. Czysty sweep. Ratujemy kogo się da i wychodzimy.
* Zbliża się burza magiczna. Spieszymy się.

### Sesja Właściwa - impl

Neikatis. Zbliżacie się do Arkologii. Jak tylko promy wylądowały, natychmiast podjechał Skorpion. Ze Skorpiona wyszła nieprawdopodobnie zapiaszczona i od oleju laska w pancerzu, bez hermetyzacji. Jolka Czeresińska.

To twardzi salvagerowie. Jolka i jeszcze jeden się trzymają dobrze, z morale itp. Ale dwóch pozostałych... nie.

Nataniel wszedł tu kiedyś. Informacja o miejscu.

* Darek: Słuchajcie, musimy wejść. Albo się wycofać. Moim zdaniem - wchodzimy. Dzielę nas na 3 grupy.
* Annabelle: Zweryfikujmy przynajmniej jednego birutana - kiedy martwi, kiedy nie i w co celować.
* Darek: Nie schodzimy na dół. 2 grupy sweep Twoja osłania Ciebie.
* Annabelle: Sensowne
* Darek: Dostaniesz jednego neikatianina. Preferencje? Wasze zadanie to ZROZUMIEĆ. My stanowimy pięść i perymetr.
* Sylwia (priv): nie chcemy przesądnego. Emocje... i to się może stać prawdą. Skazi próbkę. On musi zostać z dużą grupą.
* Marcin (priv): słuchajcie, ja chcę z Wami; będziemy grupą naukowo-badawczą.
* Annabelle: Chcę Marcina, Tristana oraz Nataniela. Zbadamy i dowiemy się wszystkiego.

Skaner geologiczny Annabelle - szuka metalu, szuka poukrywanych birutan.

Tr Z+3:

* V: Co najmniej 30 punktów w ziemi, na ścianach, w arkologii itp. Co najmniej 100 nieaktywnych birutan.

Sylwia bierze dronę i uniwersalną droną zwiadowczą próbuje z różnymi bodźcami sprawdzić z czym ma do czynienia. Czy - a jeśli tak to na co reagują. Co je obudzi. I skanery jakości powietrza.

Tr Z+3:

* X: Słyszycie krzyk. Ale daleko. Zachrypnięty krzyk człowieka, który dawno nie miał wody. Paniczny.
    * To pułapka; to najpewniej birutan ściągający ekipę ratunkową
    * Annabelle: ostrzega innych o tym.
    * Darek: to znaczy, że nas wykryli. Że ktoś wie, że jesteśmy.

Drona znalazła coś interesującego w jednym z blokhausów. Obok 3 nieaktywnych birutanów jest... datapad. Po prostu. Stary datapad. Zdaniem Annabelle MUSI być z podziemi - bo jest datapad i MUSI być z podziemi, bo inaczej Nataniel itp. by go ukradł.

Drona ich nie uruchamia. To dobra wiadomość. Też drona leci ostrożnie, wchodzi głębiej do blockhausu by zweryfikowała co się dzieje.

* X: Detektory drony są przytępione, nie jest w stanie zlokalizować niektórych rzeczy.
* X: Sylwia orientuje się, że pole magiczne arkologii jest specyficzne. Jest tu specyficzna aura. MUSI być blisko ludzi; zasięg drony jest 10 metrów. Nadal nie jest nic, ale nie da się nic wypuścić. Ogólnie, mechanizmy, komunikatory... daleko -> kiepsko.
    * Sylwia: informuje o czujnikach i problemach. Podniesione pole magiczne i mechanizmy nie działają.
    * Eryk: dzięki. To trochę tłumaczy.

Sylwia wysforowuje się do przodu, ściąga dronę do wejścia i przestawia w tryb 'psa pasterza', krąży dookoła Sylwii i reaguje w zasięgu działania. Ma ostrzegać. +1Vg. Sylwia szuka aktywacji birutanów i szuka śladów porwanych.

* Vr: Sylwia się musiała trochę wysforować. Znalazła ślady - w kurzu. Przemieszczanie jest w miarę świeże. Są ślady ciągnięcia, ale też szamotaniny. I prowadzą do takich specyficznych tuneli. W dół. Są ślady prowadzące w dół. Tam są ludzie... a krew nawet jeśli zastygnięta to nie jest stara.
* V: Sylwia znalazła coś jeszcze - zintegrowane pojazdy z dużymi kołami, szybkie. Raiderzy do wyjeżdżania i polowania. Szybkie pojazdy, uzbrojone, z ciałem, np. dwie głowy dodane do pojazdu. Wszystko nieaktywne. Ruchy - "przemieszczanie się" - wyglądają na "typowo birutańskie". Nie ma tam "wybitnego intelektu", choć może być mastermind.

Sylwia przygotowuje ładunki wybuchowe do wysadzenia do "pojazdów birutan". Nie jesteśmy w stanie określić ich zasięgu, bardzo ostrożnie się zbliża droną + magnes by zaminować te pojazdy. Magnetycznie przytwierdza. I akceptuje że to potrwa chwilę czasu. (+1Vg)

* X: Komunikacja się rozsypuje. Zespoły mogą liczyć tylko na to co poustalały wcześniej.
* V: Najszybsze jednostki zostały zaminowane. Łącznie z pełnoprawnym Skorpionem. Był tu jeden uszkodzony Skorpion, który ma np. szczelinę zapchaną ciałem. To zostało zaminowane specjalnie mocno.

Nataniel opowiedział, pytany, o Królowej Pająków.

* jej słowa sprowadzają pająki
* łapie sparaliżowanych ludzi i robi z nich inkubatory pająków
* piękna kobieta / cybernetyczny pająk.

Słychać strzały. Tutejsze birutany jak leżały tak leżą. Annabelle znajduje jednego birutana który jest dalej niż inni. Nieaktywny, w szczelinie, dobrze "wciśnięty". Annabelle robi trasę odwrotu, Tristan przygotowuje broń.

Pręcik + odrobina syntetycznej bioskóry z bandażem i pułapka na birutana gotowa. Annabelle przechodzi z pułapką "no zrób coś" a Tristan, Marcin i Mateusz przygotowują się do złapania birutana jak się wychyli. I skan Annabelle ile kończyn - ma dwie ręce, obie zakończone szponami; 40% ciała to organika. Rozkładająca się organika.

Annabelle ostrożnie przeprowadza operację z prętem. 

Tp +3:

* X: Z kijkiem, NO NIE REAGUJE. Jest nieaktywny jak trup. Nie uruchamia się. Trzeba go WYJĄĆ. Ale nie wiadomo czy CZEKA czy jest nieaktywny.

Blokujemy go przy użyciu prętów (by nie mógł skoczyć), Tristan rozwala skałę i przechwytujemy.

* X: Tristan rozwalił skałę. Huk nieziemski w ciszy martwej arkologii. Birutan wypadł jak lalka na ziemię.

Tristan spojrzał "SEEERIOUSLY". A motion detector się zaświecił.

5 birutanów szarżuje na Zespół minus Sylwia, 3 na Sylwię

Sylwia włącza wsteczny, szybko biegnie, bierze karabin precyzyjny i strzela.

Tr Z+3:

* V: WYRAŹNIE próbowali odciąć Sylwię. Sylwia się nie dała, wymanewrowała i była szybsza; teraz oni gonią Sylwię biegnącą do zespołu.
* X: Annabelle ma dobry pomysł - wskoczyć na domek, zrobić nest defensywny. Tristan "co robisz!" Mateusz "zaufaj jej!" i skoczyli. I plan jest dobry. Ale domek - dach się zawalił, nie wytrzymał nacisku. Tristan tam na dole został sam z machinegunem i tą piątką. Nataniela złapał Marcin i skoczył z nim.
* V: Tristan otworzył ogień z broni maszynowej. Zaczął ich po prostu kosić. Nie ściął WSZYSTKICH, ale poważnie ich pouszkadzał i sam skoczył do tyłu. Zostały dwa uszkodzone birutany.
* Annabelle "przypadkiem" rozdeptała nieaktywnych i się pozbierała. Annabelle i Marcin są aktywni. Marcin puścił Nataniela "cholera". Wypada przez drzwi i strzela
* Vz: Annabelle wypada przez drzwi i rozstrzeliwuje jednego, który PRAWIE dobiegł do Tristana. Marcin użył silnika skokowego i serią chciał odepchnąć ale niewiele zrobił. Ale - birutan się przewrócił; Marcin trafił w nogę. 
* V: Sylwia wymanewrowała, postawiła ich koło siebie, po czym zostawiła im w prezencie granat odłamkowy. Zniszczone.
* Tristan rozdeptał ostatniego.

Mateusz się pozbierał, Nataniel też.

W tej chwili nie ma więcej birutanów które są zainteresowane, a przynajmniej nic na to nie wygląda.

* Tristan: "Co to miało być? Serio, nie tego spodziewałbym się po weteranach"
* Annabelle: Lepsza pozycja strzelecka? nie spodziewałabym się dachu.
* Tristan: Nie o to chodzi, rozstrzelalibyśmy, to tylko te, birutany. No ile trzeba. Pomysł miałaś niezły ale serio? Po to mamy broń, nie?
* Annabelle: (smutna minka zbitego psa)
* Tristan: No kurde nie, jesteś na to za dobra, każdy się myli, tak? Po prostu... ech. Dobra.

Sylwia się skupia. Coś jej nie pasuje. Czy się zmieniła aura?

ExZ+3:

* X: Wszystko wskazuje na to, że nic się nie zmieniło. Ale nadal czujesz się obserwowana. Czujesz intelekt. Coś... coś widziałaś, ale co?

.

* Tristan: "Z taką piękną laską o ciele pięknej kobiety i głosie syreny to bym się spotkał..."
* Annabelle: "Ja Ci nie wystarczam?"
* Tristan: "100% spłonił się"

WIESZ co nie pasuje Sylwii. Gdy uciekała przed tą trójką, PRAWIE trafił ją pocisk snajperski. "Za osłonę". Przeciwnik strzelił i się przeniósł. Skan, sweep - nic, żaden detektor nie pokazuje przeciwnika. I jeszcze coś zagłusza sygnał. Jedyne co dobre, przeciwnik też nie może namierzyć...

Przeciwnik strzelał do Sylwii z okolic jednego z uszkodzonych blockhausów prowadzących do jednego z tuneli.

* Komunikator: "Tu Feliks... <zakłócenia>... pod ziemią. Mam człowieka. Potrzebuję wsparcia. <zakłócenia>"

Annabelle próbuje określić co jest grane. Czy to może być Feliks.

Tr +3:

* Vr: Annabelle doszła do tego - to JEST Feliks. Co więcej, uszkodzenia sygnałów wskazują na to, że jest pod ziemią. Po głosie nie jest przestraszony. Raczej... "wtf happened".

Annabelle próbuje wzmocnić sygnał. Używa kabli, zapasowe części itp. Są tu resztki sieci komunikacyjnej arkologii i Annabelle spróbuje się podpiąć by powzmacniać.

Tr +3 +Z:

* X: Sygnał jest mocny i świetnie się niesie. WSZYSCY słyszą Annabelle. Ale sojusznicy nic nie mogą powiedzieć.
* X: Przeciwnicy są zaalertowani. Mogą odciąć Zespół.
* V: Połączenie z Feliksem. Czyste połączenie. I mniej więcej XYZ po sile sygnału.

.

* Feliks: "Czy ktoś mnie słyszy? Wiecie..."
* Annabelle: "Feliks"
* Feliks: "Poznałem po głosie, Annabelle... jesteś jak anioł. Wpadłem w dziurę!"
* Annabelle: "Gdzie jest reszta grupy?"
* Feliks: "Nie wiem. Wpadłem w dziurę. Strzelaliśmy się, i... wpadłem w dziurę. Zsunąłem się, jak zsypem. Ok, szli za mną - strzelałem, za dużo, musiałem się oderwać. Nie działa mi kompas... potem usłyszałem głosy. I mam człowieka. Ale nie mogę się do niego dostać. Jest za ścianą. Serio. Annabelle, wiem jak to brzmi, ale to naprawdę prawda i naprawdę ja. Jestem..."
* Annabelle: "Gdzie schowałeś ukradzione majtki Elizawiety?"
* Feliks: "Moment! Moment! Po pierwsze, ja ich nie ukradłem. To był Tristan. Ja mu tylko schowałem! Po drugie... (podał lokalizację)"
* Tristan (cicho): "Zapierdolę skurwysyna. Wpierw kradnie majtki a potem zwala na mnie..."
* Annabelle: "Rozliczycie się jak go wyciągniemy"
* Annabelle: "Co widziałeś ostatnio jak spadłeś?"
* Feliks: "W jednym z blockhausów była dziura. Spadłem. Zsunąłem się, potem uciekałem. WIEM gdzie jestem. To był... to był jak zsyp."

.

* Feliks: "Za ścianą jest kobieta, przedstawiła się jako Olga Ondycka, z Neikatis; podobno "dziwny birutan" ją porwał. I zamknął tu by inne birutany nie mogły się do niej dorwać. Drapały w ścianę."
* Annabelle: "Nie brzmi to absurdalnie"
* Feliks: "To królowa pająków, tak? Ona zamknęła tą Olgę?"

90 minut do burzy magicznej.

Annabelle robi SZYBKI skan pozyskanych zwłok nieaktywnego birutana.

Tr Z +3 +3Og:

* Og: Annabelle SZYBKO grzebie w zwłokach birutana i nagle... nekrotkanka się uaktywniła
* V: Tristan złapał Annabelle i ją odciągnął; potem otworzył ogień z machinegun
    * Annabelle: birutan NIE MIAŁ żadnej ingerencji, "typowy" lokalny birutan. A jednak wstał.
    * ten birutan był "martwy". Nie miał dość procesów życiowych.

.

* Tristan: odetnę części ciała.
* Annabelle: Zwiążmy kolejnego.
* Sylwia: Mamy 90-120 minut na burzę magiczną.
* Annabelle: nie możemy swoich zostawić, musimy mieć więcej informacji
* Sylwia: Pchamy się jak strażak z nędzną psikawką do budynku który płonie
* Nataniel: MOMENT! Ale... ale mieliście uratować naszych ludzi, nie?
* Tristan: Zamknij się, Ciebie nikt nie pyta. Siedź cicho jak dorośli rozmawiają.
* Sylwia: Martwi nikogo nie uratujemy. Mamy bardzo mało czasu na cokolwiek.
* Annabelle: Jak się dokopiemy do reszty, ufortyfikujemy się na czas burzy. Budynki są odporne na burzę magiczną.

Eskalacja konfliktu: 

* V: Im bliżej burza magiczna, tym bardziej aktywne są birutany. Ten birutan był "wyłączony", za niski poziom energii. One działają cyklicznie.

.

* Annabelle: dobrze - burza, kogo się da i ewakuujemy
* Sylwia: obiecaliśmy że zrobimy co jesteśmy w stanie i to zrobimy

Sylwia próbuje Nataniela uspokoić, "nie wszyscy są jak Tristan".

Tr Z+3:

* X: Nataniel: (cicha rozpacz) (niesmak) Oby się udało... (Tristan po prostu za mocne wrażenie)

Sylwia przeprowadza oddział do tunelu i bada zagrożenia po drodze

Tr Z+3:

* V: Sylwia doprowadziła zespół bezpiecznie do tunelu; snajper albo nie wie o nich albo go tu już nie ma

Tunel wygląda ŹLE. Niestabilnie, Co gorsza, jest przekształcony magicznie... ludzie wierzą w Królową Pająków. Taka... nora. Z dziurami. Nadal metalowy, nadal nieco niestabilny, ale powinien prowadzić do celu. Sylwia bardzo ostrożnie przesuwa się do przodu, by kupić czas innym by się tam mogli dostać.

* Vr: Sylwia wszystkich prowadzi w miarę bezpiecznie. Zauważacie w tych szczelinach czasem jest nieaktywny birutan.

Komunikator COŚ ŁAPIE. Annabelle chce wyostrzyć sygnał i chce złapać co się dzieje. W takim razie troszkę wzmacniasz sygnał - słyszy przebitki głosu Darka i Feliksa. Są w różnych miejscach i nic nie słychać. Ale jest jedna sylaba plus szum.

Zgodnie z raportem Olgi, która panikuje ale mniej bo Feliks ją uspokoił:

* Nad nią jest niezbyt stabilny sufit (jej zdaniem); jest... ma szczeliny, ale ona się stresuje
* Z Waszej strony powinna być chyba ściana, ale coś pękło, są załomy, i słyszy Feliksa
* Birutan przyszedł i ją zamknął od DRUGIEJ strony. Zawalił metal by ją odciąć. Ona jest "w klatce".
    * Widziała, że birutan potrafi użyć dźwigni itp by otworzyć to przejście.

Sylwia puszcza dronę przodem; chce sprawdzić co się dzieje w tym wąskim tunelu. (+3Vg)

* Xz: Drona widzi drogę prowadzącą do Feliksa; trzeba w jednym miejscu się troszkę postarać. Niestety, na Feliksa - na oko - poluje dwóch birutanów których on nie widzi. Ukryci ale aktywni. Czekają, aż się odwróci lub zrobi coś głupiego. Trzeba działać SZYBKO. (plus pułapka)

Sylwia wpada do dziury, iskrzy serwopancerz, wylądowała, rzuciła się do przodu z pistoletem by zlokalizować birutany i WTEDY z góry, ze szczeliny spada na nią inny birutan.

* V: Sylwia odstrzeliła tego co spada na nią. Piękny strzał, prosto w łączenie udowe; odrzut + uszkodzenie poważne birutana, Sylwia do tyłu.

Tristan poleciał za Sylwią. Jest bitwa, to poleci. Przygotował broń i...

* X: UTKNĄŁ. Źle poleciał.
* Vz: Tristan się szamocze, nie pomaga
    * Annabelle: "Przestań się szamotać na chwilę!"
    * Tristan: "..."
    * Annabelle go podważa lekko, niewielka siła i poleciał w dół.
* V: Sylwia bronią w pierwszego, drugiego odpycha droną; przesuwa jego pęd. Zestrzelony jeden, drugi 'uderzony', a po chwili pocisk Tristana usunął tego drugiego.

Zespół połączony.

Olga (przejście przez zrujnowany teren) wygląda na to, że jest z nią dobrze.

Darek jest z Erykiem, Jolką, Gaiusem i Larethem. 

* Darek: Oddział do raportu... (zmęczony głos)
* Annabelle: Pod ziemią mamy Feliksa, grupa w komplecie
* Darek: Idziemy do Was i ewakuujemy się. 
* Annabelle: (podaje pozycję)
* Darek: Mam problem. Nasze detektory wykryły dwie żywe osoby, głębiej. Poradzicie sobie tam?
* Sylwia: Ciężki sprzęt potrzebny
* Annabelle: jedna żywa w zawalonej sekcji
* Darek: (ucieszył się) Wiem, że trzeci zespół; grupa Elizawiety uratowała kogoś. Idą na powierzchnię, do pktu zbornego.
* Sylwia: (mówi do Eryka o sytuacji że jest tu snajper, analogicznie)
* Eryk: Przyjąłem, zabezpieczę ścianę.

Annabelle ostrzegła, że wejście od drugiej strony do Olgi może być zaminowane.

Chwilę potem pojawiła się ekipa. Dariusz, Gaius, Lareth i Eryk. Plus Jolka. Wyraźnie widać, że doszło do wymiany po drodze.

Do burzy została godzina. Eryk patrzy na rumowisko i kręci głową przecząco lekko.

Darek chce wydobyć - znaleźć i wydobyć - dwie osoby które są gdzieś "niżej". Tu przyda mu się Annabelle oraz Sylwia. Eryk - jeśli ma mieć jakiekolwiek szanse zdążyć - musi mieć pochodnię Sylwii i np. Tristana do pomocy. Plus trzeba się stąd wydostać.

* Darek: szansa wyciągnięcia Olgi?
* Eryk: W 45 minut? Niewielka. Bardzo niewielka.
* Darek: Annabelle, da się ufortyfikować przed burzą?
* Annabelle: fatalna fortyfikacja tutaj... i coś może przejść...
* Sylwia: już się przedostało, chciało nas zjeść
* Darek: Znajdziemy niżej jakąś drogę do fortyfikacji i przetrwamy burzę tutaj.
* Annabelle: ewakuować i wrócić po burzy?
* Darek: Oni już nie będą żyli. Lub co gorsza, będą żyli
* Annabelle: nic nie zrobimy podczas burzy. Nie wydobędziemy teraz to...
* Sylwia: siedzenie w burzy jest najgorszym, co możemy zrobić dobrowolnie

Sylwia i Annabelle przekonują Darka, że jednak trzeba się ewakuować. Powinien zdawać sobie sprawę z tego, w co idzie. Sylwia opowiada o najgorszych akcjach jakie zna magicznych i podkreśla, że burza magiczna jest straszniejsza. "Jeśli tu zostaniemy, to zostaniemy. Potencjalnie ZOSTANIEMY." Annabelle jeszcze bardziej zależy by się stąd wycofać - wszyscy z załogi muszą przeżyć. I motywuje to też tym że jeśli tu zginiemy, nie uratujemy nikogo. A do tego... serwopancerz Orbitera + Orbiterowiec daje pięknego birutana.

Tp +4:

* V: Darek zrobi to, co uważają. Dlatego, że to jedyna sensowna opcja.
* X: Zorientowali się wszyscy, że nie da się uratować nikogo jeśli chcemy wyjść. Morale spadło. To ważne z uwagi na Pryzmat przed burzą.
* Vr: Udało się podnieść relacje między tymi salvagerami i okolicznymi salvagerami a Orbiterem, mimo wszystko. Bo Orbiter próbował. Ale nawet Orbiter nie może z burzą magiczną - i to rozumieją.

Próbujemy przepalić się do Olgi. Próbujemy się do niej przebić. Idziemy SZYBKO, więc mniej bezpiecznie. Eryk robi co może, pomaga mu Sylwia i Tristan a Annabelle opracowuje jak do tego sensownie podejść. Jakie kroki, jakimi sposobami... akceptując, że Olga się pokaleczy że ją wyjmujemy.

Sylwia jest też wyczulona na różne rzeczy - aktywnie próbuje wyczuć czy coś dziwnego się nie zaczyna dziać. Wchodzi paranoja. I już w tym momencie podajemy Oldze linę - wyciągniemy "na siłę" jak się inaczej nie da.

Ex+3+3Or:

* X: Olga będzie ciężko poraniona, jeśli przetrwa
* V: Jest "wąskie" przejście do Olgi
* Or: Dwóch agentów Orbitera - Eryk oraz Gaius - zostało RANNYCH. Solidnie rannych. Połamani, ale stabilni. Dehermetyzacja ("nic co tu jest nie pokona środków Orbitera na Eventares"). Ale - Olga jest wyciągnięta. Bo było załamanie.

W TYM CZASIE Annabelle robi badania terenu. Też ma dronę, ma sprzęt, rozstawia i próbuje wydobyć co się da. Cel - w głąb bazy. Przekrój geologiczny. Co tu się dzieje. Co to jest. Jakie pomieszczenia POWINNY być, czy jest tam dużo złych rzeczy.

Jest tu też terminal.

Tr Z+3:

* X: Annabelle złapała na komunikatorze jakiś sygnał SPOZA Waszej grupy. Skupia się na tym jako na głównym i jedynym.
* V: Annabelle dała radę złapać jeszcze coś - sygnaturę energii. Pod spodem arkologii jest sprawny reaktor.
    * To znaczy, że ABSOLUTNIE nie można zrobić bombardowania orbitalnego. Ta arkologia ma reaktor nuklearny. Jest źródło energii - ale nie u góry. Czyli mamy dowód na MAGIĘ oraz ENERGIĘ. Coś tam jest i jest to coś innego.
* Vz: Annabelle określiła główny kolor Arkologii Araknis i to nie ucieszyło.
    * Lacrimor + Interis.

Mamy Olgę. Mamy błyskawiczną pomoc medyczną. Mamy Annabelle która doszła co się dzieje.

I nagle - z sufitu - ze szczeliny - na ziemię spada granat.

Tr Z (Sylwia ma paranoję) +3:

* V: Sylwia WIDZI, jak jeszcze na poziomie sufitu przez szczelinę w dół będzie spadał granat. Ten jest czasowy, starszego typu, ale mocny.
* V: Sylwia podskoczyła na serwomotorach, lekko złapała granat i przekierowała go natychmiast w inną szczelinę gdzieś "na boku". Granat tam poleciał. I "padnij".

Granat był odłamkowy, ostry szrapnel, ale odłamkowy. Nikomu nie stała się krzywda; Orbiter w trybie bojowym.

* Darek: "Wycofujemy się"

Annabelle idzie pogardą na głos jako idolka by zdeprymować przeciwnika "Widzę, że jak zwykły wtorek z granatami" a Sylwia idzie filmikami szkoleniowymi i "dzieci, nie róbcie tego w domu" i Perfekcyjny Żołnierz Orbitera. Scenka. "Sylwia, łap mój" itp. Chodzi o efekt.

Tp +4:

* X: flashbang się odbił, spadł do Zespołu i odpalił. Servary wyciemniły szybki, więc nie ma efektu. WTF! 
* V: sukces. Przeciwnik ma dość. Odgoniony.

Czas na ewakuację. Mamy rannych, Jola + Olga + Nataniel są ślepi. Olga + Eryk + Gaius potrzebują pomocy i transport jest wolny. No i potencjalne pułapki. Ale jest 15 minut - rezerwa czasowa. Sylwia przodem na zwiadzie, wyszukuje rzeczy. I Sylwia laseruje dronami Eryka wszystko co podejrzane. Nie ma sensu czekać. 

Tr Z (agresywne drony usuwające pułapki) +3 +3Og (aktywacja birutanów):

* X: Idzie powoli. Niestety wolniej niż chcecie. Nie macie rezerwy; pilot musiał podlecieć bliżej arkologii. (nie ma ryzyka, bo zaminowaliście wcześniej)
* Og: Atak birutanów z różnych stron. Tyle, że jest cięższa ekipa.
* Vz: Wydobiliście się z tuneli. Jesteście już pod osłoną arkologii.

Natychmiastowa detonacja wszystkiego co było zaminowane. 

* X: Snajper wystrzelił. Jest gdzieś ukryty, nie do końca wiadomo gdzie, dał radę ciężko postrzelić dowódcę oddziału (Darka).

Widzicie zbliżającego się Skorpiona. Skorpiona Czereśnię. Włączył pełne światła i z jednego okna Elizawieta otwiera ogień maszynowy a z drugiego Ulryk (koleś ze Skorpiona).

Skorpion zasłania oddział przed ogniem snajpera.

Wleczemy rannych i do Skorpiona. A reszta łapie Skorpiona ręcznie. +3Vv

* Vg: Skorpion z maksymalną Skorpionią prędkością wyjeżdża, a przez osłonę podwójne działko promu.

Burza jest za blisko. Skorpion nie da rady odjechać; nie zdąży.

Ewakuacja wszystkich na Eventares używając trzech promów.

## Streszczenie

Zespół Orbitera wyruszył na Neikatis, aby zbadać Arkologię Araknis oraz uratować potencjalnych ocalałych. Na miejscu odkryli nieaktywnych birutan, porzucone technologie, wpływ energii Lacrimor i Interis, oraz dowody walk i porwań. Udało się zneutralizować część birutan, uratować Feliksa Pasikonika i Olgę Ondycką, oraz uniknąć zasadzek przeciwnika, mimo nadciągającej burzy magicznej oraz działającego tu snajpera (?). Misja zakończyła się ewakuacją drużyny i ocalonych, choć kosztem obrażeń dwóch członków załogi i pozostawienia Skorpiona Jolki. Kluczowym odkryciem był aktywny reaktor nuklearny w podziemiach Arkologii.

## Progresja

* .

## Zasługi

* Sylwia Mazur: Przeprowadziła drużynę bezpiecznie przez niebezpieczne tunele Arkologii, unikając aktywacji większości birutanów. Zidentyfikowała i zneutralizowała kilka potencjalnych zagrożeń, w tym zbliżający się granat odłamkowy, który mogła przekierować w bezpieczne miejsce. Aktywnie wykorzystywała drony zwiadowcze do zabezpieczania terenu i eliminowania pułapek, co znacznie zwiększyło szanse drużyny na przetrwanie.
* Annabelle Magnolia: Wzmocniła sygnał komunikacyjny, umożliwiając kontakt z Feliksem Pasikonikiem oraz określenie jego pozycji. Przeprowadziła badania magicznej aury Arkologii i zidentyfikowała energię Lacrimor i Interis jako kluczowe wpływy. Uczestniczyła w ratowaniu Olgi Ondyckiej, wskazując najlepsze metody przebicia się przez zniszczone struktury.
* Eryk Komczirp: Mimo odniesienia poważnych obrażeń, zachował zimną krew i umożliwił skuteczne zakończenie akcji ratunkowej wyciągając Olgę zanim dojdzie do katastrofy strukturalnej Arkologii. Przygotowywał perymetr wokół zespołu, by zminimalizować ryzyko ataku birutanów.
* Tristan Karbińczyk: Brał udział w walce z birutanami, skutecznie eliminując kilka jednostek dzięki precyzyjnemu ostrzałowi z karabinu maszynowego. Pomagał Sylwii w operacji neutralizacji przeciwników w tunelach, choć chwilowo utknął podczas jednej z akcji. Nieprzyjemny i głośny, ale skrajnie lojalny Orbiterowi; wszystkimi innymi gardzi. Doskonała siła ognia i wyczucie bojowe.
* Feliks Pasikonik: Wpadł w pułapkę i się zgubił; w końcu nawiązał kontakt radiowy z drużyną mimo zaciemnienia komunikacji i przekazał informacje o swojej sytuacji oraz obecności Olgi Ondyckiej. Chronił Olgę przed birutanami, utrzymując pozycję, mimo że nie miał dostępu do wsparcia ani środków ewakuacji.
* Darek Artycki: Kierował drużyną podczas eksploracji Arkologii, podejmując trudne decyzje dotyczące podziału sił i priorytetów. Walczył o uratowanie wszystkich potencjalnych ocalałych, nawet kosztem ryzyka dla siebie i drużyny. Został ciężko ranny podczas akcji, ale jego działania znacząco wpłynęły na morale i relacje z lokalnymi salvagerami.
* Elizawieta Barwiczak: Osłaniała drużynę podczas ostatecznej ewakuacji przy pomocy Skorpiona Czereśni. Zapewniała wsparcie ogniowe z pokładu Skorpiona, co umożliwiło drużynie bezpieczny odwrót pod presją snajpera.
* Gabriel Tabacki: podrywający Annabelle świetny pilot, który stanął na wysokości zadania mimo zbliżającej się burzy magicznej.
* Mateusz Drożdżołebski: Wspierał Annabelle i Tristana w zabezpieczaniu oraz eliminacji aktywnych birutanów, skutecznie utrzymując drużynę w bezpiecznej pozycji podczas walk.
* Marcin Psiamęczok: Wsparcie ogniowe i osłona Zespołu w trudnych momentach. Kompetentny agent Orbitera.
* Jolka Czeresińska: Współpracowała z Orbiterem, pomagając zrozumieć specyfikę magicznej aury Neikatis i jej wpływ na technologie. Brała udział w ewakuacji, osłaniając drużynę w krytycznych momentach przy pomocy Skorpiona. Optymizm i profesjonalizm.
* Nataniel Marczkin: Służył jako lokalny (acz zrezygnowany i niechętny) przewodnik, dostarczając drużynie wiedzy o strukturze Arkologii oraz wierzeniach związanych z Królową Pająków.
* Ulryk Czeresiński: Obecny na akcji; wraz z Elizawietą osłaniał ze Skorpiona ogniem zaporowym ewakuujący się Zespół z Arkologii.
* Olga Ondycka: Ocalała dzięki akcji ratunkowej drużyny Orbitera, dostarczając kluczowych informacji o strukturze Arkologii i działaniach birutanów. Współpracowała z Feliksem, z trudem utrzymując spokój, przekazując dane o swojej sytuacji.

## Frakcje

* brak

## Fakty Lokalizacji

* brak

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Korowiec
                    1. Arkologia Araknis
                        1. Zewnętrzna Tarcza
                        1. Poziom jeden
                            1. Blockhausy mieszkalne
                            1. System Defensywny
                        1. Poziom zero
                            1. Tunele Pajęcze
                            1. Stacje Przeładunkowe
                            1. Magazyny
## Czas

* Opóźnienie: 44
* Dni: 2

## Inne

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Korowiec
                    1. Arkologia Araknis
                        1. Zewnętrzna Tarcza
                        1. Poziom jeden
                            1. Blockhausy mieszkalne
                            1. System Defensywny
                        1. Poziom zero
                            1. Tunele Pajęcze
                            1. Stacje Przeładunkowe
                            1. Magazyny
                        1. Poziom (-1) Mieszkalny
                            1. Szyb centralny
                            1. NW
                                1. Magazyny wewnętrzne
                                1. System obronny
                                1. Sprzęt ciężki, kopalniany
                            1. NE
                                1. Medical
                                1. Rebreathing
                                1. Administracja
                            1. S
                                1. Sektor mieszkalny
                                    1. Przedszkole
                                    1. Stołówki
                                1. Centrum Rozrywki
                                1. Park
                        1. Poziom (-2) Industrialny
                            1. NW
                                1. Inżynieria
                                1. Fabrykacja
                                1. Reprocesor Śmieci
                                1. Magazyny
                                1. Reaktor
                            1. NE
                                1. Centrum Dowodzenia
                                1. Rdzeń AI
                                1. Podtrzymywanie życia
                            1. S    
                                1. Szklarnie podziemne
                                1. Stacje badawcze
                        1. Poziom (-3) Badań Niebezpiecznych
                            1. Śluza defensywna
                            1. Biolab
                            1. Magitech
                            1. Anomaliczne

### Podsumowanie inne

Drużyna z Orbitera wyruszyła na misję ratunkową do jednej z Arkologii na Neikatis, gdzie ich zadaniem było odzyskanie ocalałych i zbadanie zagrożenia birutanami w regionie. Po lądowaniu zostali przywitani przez lokalną salvagerkę Jolkę Czeresińską, która zaoferowała wsparcie, ale drużyna szybko zauważyła napiętą sytuację wśród jej ludzi. Na miejscu odkryto ślady walk, porwań oraz potężne anomalie magiczne wpływające na mechanikę i komunikację.

Eksploracja Arkologii: Drużyna podzieliła się na mniejsze grupy, aby przeszukać teren. Annabelle Magnolia wykorzystała swoje drony i skanery do zlokalizowania zarówno aktywnych birutanów, jak i nieaktywnego sprzętu, co pozwoliło określić potencjalne zagrożenia. Sylwia Mazur, jako zwiadowca, zdołała odkryć ślady walk, zaminować pojazdy birutanów, a także wymanewrować wrogów próbujących ją odciąć.

Konflikt z birutanami: Podczas eksploracji drużyna napotkała aktywne birutany, które zaatakowały ich z różnych stron. Tristan Karbińczyk, wraz z innymi członkami drużyny, skutecznie zneutralizował zagrożenie, choć intensywność walk odsłoniła napięcia wewnątrz grupy, szczególnie między nim a Annabelle.

Ratunek Feliksa i Olgi: Annabelle, dzięki swojej wiedzy technicznej, nawiązała kontakt z Feliksem Pasikonikiem, który został uwięziony w podziemnych tunelach. Okazało się, że Feliks znalazł kobietę – Olgę Ondycką – która twierdziła, że została zamknięta przez birutana w celu ochrony przed innymi. Drużyna, mimo coraz bliższej burzy magicznej, zdecydowała się na szybkie działania, by ich uratować.

Ewakuacja: W obliczu nadciągającej burzy magicznej drużyna musiała podjąć trudną decyzję o ewakuacji. Dzięki pracy Sylwii, Annabelle, Tristana i innych udało się wydostać rannych oraz ocalić Olgę, choć kosztem ciężkich obrażeń Eryka Komczirpa i Gaiusa Tabackiego. Ewakuacja została wsparta przez Jolkę i jej Skorpiona „Czereśnię,” który osłaniał drużynę przed snajperem.

Podsumowanie: Misja zakończyła się sukcesem w zakresie ratowania życia i zdobycia danych o Arkologii, choć morale drużyny ucierpiało z powodu strat i presji czasu. Annabelle odkryła, że Arkologia skrywa aktywny reaktor nuklearny, a dominujące energie Lacrimor i Interis czynią miejsce jeszcze bardziej niebezpiecznym. Drużyna ewakuowała się na pokład Eventares, zyskując nowe wyzwania do rozważenia w przyszłych działaniach.
