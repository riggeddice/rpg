## Metadane

* title: "Tank as a love letter"
* threads: rekiny-a-akademia
* gm: żółw
* players: kić, sola, fireseed

## Kontynuacja
### Kampanijna

* [220809 - 20 razy za dużo Esuriit](220809-20-razy-za-duzo-esuriit)

### Chronologiczna

* [220809 - 20 razy za dużo Esuriit](220809-20-razy-za-duzo-esuriit)

## Plan sesji
### Theme & Vision

* Exploration -> how does the world look like? Maximize the visible components of the world while not overloading players
* Young puppy love, causing sorrows and devastation
* Too much power for such unstable individuals, so easy to corrupt and manipulate
* Esuriit always wins.

### Co się wydarzyło KIEDYŚ

* Four years after the war... (canonically: about 25 years ago)
    * The "Astorian Flare" hovertank of Carcharein class has gotten corrupted by Esuriit during the astorian-noctian war.
    * "Astorian Flare" got deactivated and was supposed to be dismantled; however, tien Arienik decided that this is a good, safe relic.
    * When a powerful anomalous Esuriit monster emerged from Czółenko, local forces were not really able to face that
    * Arek Terienak woke "Astorian Flare". The hovertank objected feeling the corruption, however, mission needed to be completed
    * "Astorian Flare" managed to take on a monster, but the corruption became so much deeper...
        * TAI Maia is more resistant to corruption, Arkadiusz however...
        * Maia commanded the gestalt to go deeper into the Marsh on full speed, in order to drown there.
            * With success. "Astorian Flare" was lost and Marsh energy (ixion overload) kept it dormant...

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

* PREVIOUS STORY
    * Mimosa Diakon, an Aurum aristocrat (therefore, mage), is trying to help local businesses and the locals as an Aurum aristocrat should.
        * -> she sends groups of agents left and right to help important parts of the region
    * One of the mercenary Aurum groups Mimosa has requested was a group made of Mike, Thalia and Isabel; they have been sent to one of the magic purification places to help with some issues
        * They have no idea what the issues even are
        * This is one of Mimosa's tests. Will they make it?
        * However, there are THINGS happening there which are way over poor mercenaries' head
* THIS STORY
    * Iwona Perikas likes Daniel Terienak. Seriously, she LIKES him ;-).
    * She believes she has a tool able to extract Esuriit from "Astorian Flare" and move it to some small parts with a cult
        * WHY: She believes she can bury "Astorian Flare" and Arkadiusz with honors. And get information to get closer to Daniel.
            * WHY: She is misguided; she got deceived by a Shadowy Figure (Seraphine)
        * HOW: small cult, about 15 people chanting and some excavators
            * Some of those people come from Żarnia, some are hers (which means the transport comes )
    * Iwona disposes of the corrupted parts using the excavation site controlled by Henryk Wkrąż; he hopes Iwona will look at him favorably
        * Iwona told Henryk, that they are doing it for Daniel. Henryk is 100% for it.
        * Henryk has no idea how corrupted this shit it.
        * Mimoza wanted Henryk to have a chance at redemption. He just failed.
            * -> this will cause: 
                * rift Marsen -> Iwona
                * rift Mimoza -> Henryk


(look for character sheets / profiles / hints in ENGLISH_NOTES at the end of this file)

ENCODING:

* Thalia (T took over this character) -> Talia Mikrit; Brawler
* Mike (Fireseed took over character) -> Michał Czwardon; Scout
* Isabel (Sola took over character) -> Izabela Szentor; Sage
* Red, Green, Blue types of magical energy / contamination -> irrelevant energies
* Yellow type of energy -> Esuriit energy (look at ENGLISH_NOTES in the bottom)

### Co się stanie (what will happen)

* The war machine shall probably rise and devastate, as war machines do ;-). Or Esuriit corruption shall corrupt someone powerful. Or whatever.

### Sukces graczy (when you win)

* Lower levels of devastation
* Mimosa will not be harmed (socially) and she can still help the region
* People will not be harmed. Well, not _many_ people will be harmed.
* Political issues (Aurum <-> Szczeliniec (Chasm Region) will not be massacred)

## Sesja właściwa
### Scena Zero - impl

N/A

### Sesja Właściwa - impl

Team knows:

* where is the site the truck originated from   
    * the truck DID come from the site. But we don't know where the corrupted parts came from.
        * they are present in the manifest.
            * the manifest shows right AMOUNT of parts but not the YELLOW parts
* the Maia-class AI-twined was a machine which is corrupted
* a twined machine: person + AI as a single yellow energy (Esuriit) construct
* problems with sensors come from Sharks' District - "a stupid aristocrat"
* whoever is involved is REALLY good in forging and **Mimosa** is involved
    * manifest comes from Mimosa's site

So Mimosa needs to be informed about the results of the investigation. But before - driver can be interrogated :D. Most important question - when did the yellow parts get in the truck.

The driver is not in best shape, he is wounded and has a head injury. + decontamination used for machines, not people. Isabel is the one who talks, she can easily combine facts and sweet-talk. And Isabel KNOWS the manifest is forged.

Hr Z (wants to help us, saved his life) +4:

* "what happened to the load" V: so, the guy says he is innocent. He is surprised you believe he is innocent. He explained that he knew the parts were kind of iffy this time. The yellow parts were loaded at the site. All of them. So the site had those parts.
* "you knew they are iffy, did anyone else know?" Vr: no mam, the parts might be yellow but were weak, hard to detect but now, then, in truck, that chanting and it was strong and it was strange. So people did not know when they were loading the thing, but the part BECAME deep yellow after. But the mage had to know. He is one of those DUMB ARISTOCRATS. "He knew, he set us all up".

There is one thing which does not make sense - why would anyone activate Esuriit early? Not in the purification plant? Not in the plant? There is malicious intent and it is INEFFICIENT! Or it is stupid not malicious - if you activate it in the truck only the driver can be harmed.

Why activate it to begin with?

We KNOW those are forgeries. And the mage there is her employee. So... we need to check in with Mimosa. Or ask people on the site if they are receiving shipments elsewhere? We don't know if the site is the origin or stuff is smuggled into the site.

The Team is moving to the decontamination site and in the meantime they compose the report and request authorization to audit if needed. If they refuse to talk. There is a request "who is the mage working there". History, anything else.

Basically, Mimosa contacted you when you were in the transit. And she looks troubled. What she said:

* Apologies for Esuriit. She did not expect that.
* The guy who is there is named Henry. Party guy (florida surfer), stupid ideas... he reached to Mimosa first, about a month ago, because he wanted to do sth useful for a change. She gave the the safest site in existence. You can't fail this.
    * he is NOT in charge. A supervisor can overrule him (named Ariel).
* Henry WILL cooperate with you. Otherwise, Mimosa's wrath will be terrible.
* She wants you to interrogate and THEN to fall back and she will send magi against Esuriit.

Mike doesn't really want to be a hero; he wants to clear things up with Isabel and Thalia that he took credit because otherwise too complicated, you want to change it - tell.

So they are traveling in the armoured hovervan. Mike's instinct is that Henry was taken advantage of, not being a guilty party. So chat with Henry, get information from him and if he is resisting then intimidate him. He may be helpful ;-).

People at the Site know about the truck; they got some hatephones "what the hell did you send us", so there is no reason to go too fast; let's just go there safely. So you have managed to get to the site with no problems. Arrived officially, they are AUDITORS.

Workers want to show the way to the supervisor; Mike acts as the bodyguard and stays with the workers, Thalia might be needed as intimidation agent. Ariel wants to meet them BEFORE they meet with Henry - he wants to be the first. He wanted workers to get you there.

Ariel. A bit older, bulky guy, local, furious AND scared. He wants to lay into you.

* he doesn't care who you are, auditors or Aurum or not, but he did NOT sign for having a good man harmed
* he thinks he is not in charge because there were papers and he talked with Mimosa and she put Henry in charge (phone)
* Thalia informed him that it was not Mimosa and papers might have been forged.
* He knows Henry has a girlfriend and that is fishy.

Thalia wants his full cooperation. "You just abstained from this site and a man almost died".

HrZ (he was in charge and got scammed) +3:

* X: after everything is done he wants to quit; he wants to have nothing to do with Aurum anymore.
* V: he will cooperate with the Team. Completely.
* Xz: when he is going to quit he is going to be all "stupid Sharks, even if good Mimosa"
* Vz: the guy is all "I was spying and gathering stuff on the girlfriend"
    * the girlfriend is bringing stuff from offsite; it LOOKED safe, but...
    * the manifest was proper and Henry said Mimosa ordered it so
    * the girlfriend has a different site. The site is located in dangerous places. Ariel personally doesn't know where the site is, but PEOPLE ARE TALKING that she employs some people from Żarnia.
    * the girlfriend's name is Iwona (Yvonne) Perikas
* X: this is one of those things which make people doubt Mimosa's intentions.
    * "so she is still one of them" -> as potential political attack from other Sharks.

Thalia and Isabel decided to go to Henry. They want to butter him up (Isabel, technically). Can he tell her what they are up to?

Henry is sad. Gloomy. Not perking up. Tense. Isabel was like "we will make a report, but maybe it is not you, we can see". Add honey. Henry perked up. He expected being scolded or something, he did not expect someone being nice to him.

Hrd Z (he is in hell + you are nice) + 4:

* X: He is extremely loyal to GF. Will take blame.
* V: How long he knows her? How did meet and stuff. Relations.
    * He REALLY wanted to turn his life around, after parties, doing stuff. Maybe there is a reason why Mimosa does her stuff. Maybe this is something worth it. He really wanted to be useful.
    * But then SHE appeared. She was interested in some oldschool tech. There is one other Shark, their colleague, called Daniel. And he is kinda scary. 
    * She was like "there is something Daniel's bloodline left behind, old story. And Daniel would really appreciate if we found it for him. And you can help".
    * MY LIFE IS TURNED!
    * She was giving him some low-danger parts. And they go for purification plant. THAT IS WHAT PLANT IS FOR.
    * He heard there ARE some manifestations and it makes no sense. So he was wary of the LAST part sent. But he had to, because girlfriends.
    * The girlfriend approached him, knowing he's working on this site and she said in order to extract the THING for Daniel they need to purify something.
* V: Oh, right, we have a forger, a damn good one. I think a forger was working with mafia at one point. Mimosa wouldn't have authorised it, so we kinda had to make it work. They both knew about forgery, it was HIS PERFECT IDEA. /proud
* V: He is talking a LOT, showing how wise he is etc. But - what Mike realized after a fact - the forger did everything the forger could to be found. So the forger is a THIRD party, not the girl, not Henry. AND - this looks more of **mafia** operation than Shark operation. With Sharks being the ones to execute _some_ shadowy plan here.

Time for Mike to shine. What Mike is next planning to do - establish a sting operation which gathers Henry, Daniel and Yvonne together. So Henry doesn't reveal anything. Is it possible for Mike to gather evidence and manipulation behind the scenes so three of them are here confronted with each other?

PLAN: push (both by words and evidence) that Daniel manipulated Yvonne to cause this, and make Henry show up and impress girlfriend he can stand up to Daniel BECAUSE DANIEL SET YVONNE UP. THis would not be possible. BUT! Henry actually has feelings towards Yvonne, Isabel managed to build up a LOT of trust AND Mike is a master manipulator.

Ex Z (Isabel + goodwill + Henry has a VERY bad time) +4 +5Or (Henry goes too far):

* X: Mimosa rejects Henry. He completely burned out all good will.
* Or: Henry will manage to lure Daniel and Yvonne (BUT they decide where it happens UNLESS escalate) AND Daniel is really, really pissed.
* V: Henry will get Daniel and Yvonne where Mike needs them

The meeting will happen in Ruins of Sensoplex in Podwiert. Large ruin.

So - the Team gets there FIRST. So they can set up recording and possible explosives if needed. And the Team is there and the Team is waiting for first people to arrive.

Basically:

* how they respond to each other
* if Daniel is involved
* to know what they are talking and how is Yvonne going to deal with this situation
* Henry is patsy and he will confront

Isabel is giving Henry an earpiece. Mike will tell him what to say. Because Henry wants to KNOW THE TRUTH about Yvonne x Daniel and the darkness HOW DANIEL USED ESURIIT TO FORCE YVONNE TO BETRAY HIM!

Hr +3 +3Or (Daniel) +3Og (Yvonne) +3Op (Henry):

* Og: so they meet there. Daniel "wtf". Esuriit. And YVONNE IS LIKE I LOVE YOU!
* Op: HENRY LOVES YVONNE! DANIEL IS LIKE I AM NOT KILLING HIM.
* V: Daniel: "YOU PIECES OF SHIT WHAT DID YOU DO!"
    * Yvonne is like: "there is an old hovertank here! Named Astorian Flare! And it was during the war! I mean after! And your uncle used it! And I found it! And I can purify it!"
    * Daniel: "YOU SICK PUPPY! My uncle died a hero. Now he'd return like a monster!"
    * Yvonne: "there is a ritual, we can use sympathy to drain Esuriit..."
    * Daniel: "won't work, you're stupid"
    * Daniel: "1v2. Come at me. Will be fun - stupidity needs to be punished"
* Vr: Daniel is like "how the hell did you find Astorian Flare? How the hell did you have the ritual? HOW did you have the stupid idea you can drain Esuriit?"
    * Yvonne speaks words, Daniel doesn't get it, but Mike does - Yvonne has been influenced and it is someone _else_. Some kind of magical influence. And it does NOT look just like the mafia.
* Op: Henry lost it. He attacked Daniel. With fists. Not magic. HE DESTROYED EVERYTHING IN HIS LIFE AND NOW HE CAN HIT THE BASTARD! HENRY IS CRYING!
* Op: Henry tries to use magic in panic! Daniel SWIFTLY stunned him.
    * We can't tell him about tank

They quickly write a note. "TANK IS THERE! GO FOR IT! FRIENDS!" Or rather, Isabel writes that note, not me.

Tp Z +2:

* V: The note contains all the relevant info and is persuasive.

Mike gets to Daniel's hovershark and places a note.

Tr Z (Yvonne +3Oy) +2:

* V: Note was attached to the hoverbike.

OFFICIALLY, YOU HAZ SUCCESS.

## Streszczenie

The truck infected with Esuriit led to Mimosa's site. Henryk (Shark) was supposed to be a support, but he told he took over - because of Iwona Perikas, his GF. The team unveiled that Iwona was the one to throw in the Esuriit-infected stuff and she was trying to use Henryk to get to Daniel whom she loves. The Team turned it around and cooperated with Henryk to lure Daniel and Iwona. But... Daniel was not the problematic one - when he heard of 'Astorian Flare' (esuriit-hovertank) he ordered other Sharks to stop everything. There are some mafia aspects here too... fortunately, Mimosa's agents were undetected.

## Progresja

* Henryk Wkrąż: Mimoza Diakon gave him another chance and he blew it for Iwona Perikas. There will be no more chances - he cost Mimoza way too much.
* Mimoza Diakon: strong reduction of reputation because of Henryk and Iwona. Ariel Kubunczak leaves her. She lost the operation, even if she helped the region.
* Daniel Terienak: broke all contact with Iwona Perikas; she might love him, but she is despicable - hurting Henryk Wkrąż and reanimating Flara Astorii...

### Frakcji

* .

## Zasługi

* Talia Mikrit: Mimosa's agent; intimidated Ariel and made him talk for his own good.
* Izabela Selentik: Mimosa's agent; by series of discussions got to Henryk and revealed he has a girlfriend; she made everything work by good discussions and detecting what Astorian Flare is.
* Michał Kabarniec: Mimosa's agent; planned a sting operation which revealed link Iwona - Daniel - Henryk. Found out that mafia is doing something here, which is worrying.
* Ariel Kubunczak: administrator of Mimosa's site near Czółenko. Henryk Wkrąż forged Mimosa's documents so he didn't know he should be in charge. Worked with Team to solve it, but he wants to have NOTHING to do with Aurum and Aurum intrigues anymore. Mimosa lost him as an agent.
* Henryk Wkrąż: Rekin; Shark; wanted to turn his life around and Mimosa gave him a second chance. Unfortunately, Iwona Perikas (and his infatuation) made it so he turned away from Mimosa and started forging stuff with mafia's help. All for Iwona. Then, manipulated by Michał, he confronted Daniel Terienak over Esuriit and Iwona, with expected result - got beat up.
* Iwona Perikas: Rekin; Shark; she found Flara Astorii and tried to reanimate it and decontaminate it for Daniel, to impress him. She wooed Henryk Wkrąż and inserted some Esuriit-parts to different transports. She was the driver, but she was under some form of mindwarp by a third party. And she worked with mafia.
* Daniel Terienak: Rekin; Shark; Henryk summoned him to confront him about Iwona Perikas. He was unaware of AU Flara Astorii being found and stopped them from doing stupid stuff and unearthing the Esuriit anomaly. He beat Henryk up and broke all contact with Iwona.
* Mimoza Diakon: Rekin; Shark; when she heart the Team is dealing with Esuriit she ordered them to stop because she did not want them to get hurt. She cares about her people. Also, wanted to give a chance to Henryk Wkrąż, who blew it. Mimosa won't trust him again.
* AU Flara Astorii: MEMORY. Anomalous Unit, drone-equipped hovertank of Carcharein class, Maia-equipped; lies somewhere near the Swamp and is completely corrupted. Iwona Perikas tried to reawaken it.
* Arkadiusz Terienak: MEMORY. In the past, he linked with contaminated Flara Astorii to stop monsters. In process they became a monster and Flara drowned in the Swamp in order to not be dangerous.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert, okolice
                                1. Oczyszczalnia Słonecznik
                            1. Czółenko, okolice: safer areas are decontaminated by Mimosa's sites
                                1. Las Trzęsawny: somewhere close to the Swamp itself lies dormant Esuriit-infected AU Flara Astorii. 

## Czas

* Opóźnienie: 1
* Dni: 1

## Konflikty

* 1 - The driver is not in best shape, he is wounded and has a head injury. + decontamination used for machines, not people. Isabel is the one who talks, she can easily combine facts and sweet-talk.
    * Hr Z (wants to help us, saved his life) +4
    * VV: guy is innocent, he believes it is aristocrats from Aurum
* 2 - Thalia wants Ariel's full cooperation. "You just abstained from this site and a man almost died".
    * HrZ (he was in charge and got scammed) +3
    * XVXzVzX: he wants to leave Aurum stuff behind, but will cooperate and will moan and people will doubt Mimosa's intentions. Even if he cooperated and said a lot.
* 3 - Henry is sad. Gloomy. Not perking up. Tense. Isabel was like "we will make a report, but maybe it is not you, we can see". Add honey. Henry perked up. He expected being scolded or something, he did not expect someone being nice to him.
    * Hrd Z (he is in hell + you are nice) + 4
    * XVVV: super loyal to GF and takes blame, but doesnt really KNOW her and they have a mafia-related forger. There is a 3rd party, mafia?
* 4 - PLAN: push (both by words and evidence) that Daniel manipulated Yvonne to cause this, and make Henry show up and impress girlfriend he can stand up to Daniel BECAUSE DANIEL SET YVONNE UP.
    * Ex Z (Isabel + goodwill + Henry has a VERY bad time) +4 +5Or (Henry goes too far)
    * XOrV: Mimosa rejects Henry - there is no third chance, Henry will piss Daniel off and Henry will get Daniel and Yvonne where Mike needs them
* 5 - Isabel is giving Henry an earpiece. Mike will tell him what to say. Because Henry wants to KNOW THE TRUTH about Yvonne x Daniel and HOW DANIEL USED ESURIIT TO FORCE YVONNE TO BETRAY HIM!
    * Hr +3 +3Or (Daniel) +3Og (Yvonne) +3Op (Henry)
    * OgOpVVrOpOp: teen drama, in short.
* 6 - They quickly write a note. "TANK IS THERE! GO FOR IT! FRIENDS!" Or rather, Isabel writes that note, not me.
    * TpZ+2
    * V: persuasive, all info there
* 7 - Mike gets to Daniel's hovershark and places a note.
    * Tr Z (Yvonne +3Oy) +2
    * attached to hoverbike
* 8 - 
    * 
    * 
* 9 - 
    * 
    * 
* 10 - 
    * 
    * 
* 11 - 
    * 
    * 

## Kto
### Aurum team:

* BRAWLER Thalia <-- T
    * aspects (incomplete list): heavy machinery, explosives, close combat, heavy weapons, intimidation, unbreakable, cool head, strong like ox
* SAGE Isabel   <-- Sola
    * aspects (incomplete list): computers, documents, social / friendly, heraldry, local knowledge, respected
* SCOUT Michał  <-- Fireseed
    * aspects (incomplete list): sniper, lying, devious, stealth, master of wilderness, twined, Eidolon power suit (stealth suit)

### Aurum :

## ENGLISH NOTES

### Energy types

* Esuriit Energy (encoded: YELLOW)
    * Primary: HUNGER; unquenchable ambition, corrosion of soul. Exploitation without end. Obsession; hunger which, satiated, will create more hunger. Addiction.
    * Secondary: HATRED; "what others have I cannot, the relief I cannot get. Pain and suffering which always flares and pushes me and I need to DEVOUR and THEY ARE THE CAUSE OF IT!"
    * Song representation: 
        * Umbra et Imago "Endorphin": "In a state of total isolation The suffering decides my behaviour The bloodlust begins" 
        * Luca Turilli "Black Rose": "Red rose so attractive bleeding hurting feelings in this state of hate your colours turning black"
    * "horrific mass of organic beings melded together, and every one of those beings is pulling and clawing away eternally, trying to escape the pain and pursue even the smallest pleasure or consumption to distract themselves from the agony of their horrible existence" <-- Path of Exile; good representation of its aspects
    * Esuriit is not just hate or hunger. Esuriit energy HATES YOU. It is un-life.
* Ixion Energy (encoded: PINK)
    * Primary: ADAPTATION; ever-changing magical world will defeat you unless you change to make it work. Shed everything inefficient and irrelevant - friends, love, your own body.
    * Secondary: SACRIFICE; nothing is free. You need to give up what you used to be and love. "You" tomorrow wouldn't understand "You" yesterday. Drop humanity for efficiency.
    * You did not have to deal with Ixion energy yet; it is related to The Marsh.

Esuriit >> Ixion. Technically, Esuriit > most energies, with the exception of Interis (PRIMARY: entropy SECONDARY: helplesness). TECHNICALLY every other energy represents a human facet, a way for humans to cope with the eventual victory of entropy. If you want me to, I will explain.
