## Metadane

* title: "Pierwsza randka Eustachego"
* threads: kidiron-zbawca-nativis
* motives: fish-out-of-water, randka, symbol-lepszego-jutra, spotlight-na-postac
* gm: żółw
* players: fox, kapsel

## Kontynuacja
### Kampanijna

* [230201 - Wyłączone generatory Memoriam Inferni](230201-wylaczone-generatory-memoriam-inferni)

### Chronologiczna

* [230201 - Wyłączone generatory Memoriam Inferni](230201-wylaczone-generatory-memoriam-inferni)

## Plan sesji
### Theme & Vision

* The past returns to Nativis
    * "To nie ma znaczenia. Oni wszyscy o to nie dbają. Chcą bezpiecznej arkologii a ja ją zapewniam, Bartku." - Rafał Kidiron
    * Dziennikarz chce zrobić program o przeklętym Kidironie, Bartłomiej nalega (w tajemnicy przed Kidironem). Kidiron się, o dziwo, docelowo zgadza.
    * Misteria Anakris kiedyś była częścią arkologii Lirvint. Straciła wszystko po tym jak Kidiron zdobył Światło Ukojenia.
        * terrorism. Misteria chce odzyskać Światło, wiły oraz ujawnić okrucieństwo i zło Kidirona.
* Sadness, eternal corporatism
    * "Twoje ideały są niepraktyczne. Straciłeś Infernię. Nie masz po co walczyć. Bez Ciebie Nativis nie byłoby tym czym jest. Czerp korzyści z Twych poświęceń." - Rafał Kidiron
* RIFT się pogłębia - Bartłomiej Korkoran / Rafał Kidiron
    * Korkoran jest wściekły na Kidirona za to co zrobił 10 lat temu z Lirvint. Zdaniem Kidirona to jedyna sensowna opcja.
    * Kidiron stabilizuje i ratuje arkologię przed zagrożeniami. "głód? Mamy jedzenie. Inne dobra? Mamy wiły. Bunty? Stłumione. Ludzie są bezpieczni. Nativis jest w szczycie!"
    * Kidiron jest drakolitą ("nigdy nie złamałem żadnej zasady"), Korkoran jest faerilem ("to jest niezgodne z wolą Jej Ekscelencji! Zadajesz cierpienie!").
    * Kidiron chce Misterię żywcem. Jest możliwa do wykorzystania na chwałę Nativis.
* Pokazanie bogatej dzielnicy 

### Co się stanie (what will happen)

* S0: Eustachy, wujek i Kidiron wrt zbrodni wojennych. Prośba o randkę ze strony Kalii
    * BK: "Jak mogłeś strzelać do cywili? To nie było potrzebne. Infernia by sobie poradziła, jesteś dobry, jesteś NAJLEPSZY. Dałbyś radę!"
    * RK: "To było konieczne. Infernia mogła być uszkodzona. A oni strzelali do Inferni pierwsi."
    * BK: "To nie oni, to amalgamoid Trianai."
    * RK: "I zakładasz, że Eustachy to wiedział? Strzelali - musieli zginąć."
    * RK: "Eustachy wygrał w konkursie. Spędzi dzień w Dzielnicy Luksusu z Kalią."
* S0: Ardilla i Ralf. 
    * SOS ze strony młodego człowieka, który próbuje czyścić rurę w arkologii. Marcin Pietras.
    * Marcin zaprasza do domu. Tam Franciszek Pietras (ENCAO:  +0-0- |Spontaniczny;;Lubi zaczynać rozmowę| VALS: Face, Family| DRIVE: Preserver) opowiada jak wujek uratował kiedyś skorpiona w którym był. Tylko on przeżył; wujek ściągnął go tu, do Nativis. Daje Ardilli symbol Bezimiennej Pani. Ogromna wdzięczność jej, wujkowi i arkologii.
    * Ralf "jesteście ścisłą rodziną"
* S1: Randka Eustachego z Kalią
    * piękne ogrody z ludźmi uważającymi Eustachego za bohatera (pomysł Kalii - nie mają prawa go znać!)
    * "co lubisz robić?" -> holosymulator -> Kalia próbuje zrobić tak, by Eustachy musiał ją dotknąć.
    * restauracja z dobrym jedzeniem i sama Kalia, która opowiada że się stara dać ludziom nadzieję i powód do życia jutro - czy nie uważa że warto?
    * emanacja magii z Ambasadorki Ukojenia. Coś tam się dzieje. 97 osób w akcji terrorystycznej
    * JEŚLI Eustachy tam wpadł, wujek odda się na zakładnika

### Sukces graczy (when you win)

* .

## Sesja właściwa

### Fiszki

#### 1. Infernia

* Bartłomiej Korkoran: wuj i twarda łapka rządząca Infernią (WG: "System służy ludziom") | faeril: "Infernia służy Korkoranom jako awatar Bezimiennej Pani."
    * (ENCAO: +-000 |Bezkompromisowy, nieustępliwy;;Ciekawski|Family, Benevolence, Self-direction > Achievement, Tradition, Humility| DRIVE: Inkwizytor: ujawnianie bolesnych prawd)
    * "Nasza Infernia ma za zadanie dać nam wolność oraz pomagać innym!"
* Emilia d'Infernia
    * "żona" Bartłomieja. Żywy arsenał. Egzekutor Bartłomieja. (Emilia -> "Egzekucja MILItarnA")
* Ardilla Korkoran: faerilka działająca jak drakolitka i przyszywana kuzynka Eustachego  <-- często Fox
    * badacz/odkrywca/scrapper. Zmodyfikowana tak, by łatwiej wchodzić w niedostępne miejsca, bardziej zwinność + orientacja w terenie + pewne umiejętności badawcze.
* Celina Lertys: drakolitka z Aspirii podkochująca się w Eustachym  <-- często Kić
    * (ENCAO: --+-0 |Stanowcza, skryta;;O wielu maskach| Universalism, Tradition, Security > Power, Humility| DRIVE: Harmonia naturalna między wszystkim)
    * złotoskóra o błękitnych elektrycznych włosach podkochująca się w Eustachym (science / bio officer + medical); augmentowana na widzenie rzeczy ukrytych; 19 lat
    * "Jesteśmy częścią Neikatis i podlegamy procesom Neikatis. Ale nadal mam zamiar zrobić wszystko by uratować swoich przyjaciół."
* Jan Lertys: drakolita z Aspirii
    * (ENCAO: -0-0+ |Zapominalski, obserwator, łatwo nań wywrzeć wrażenie | Humility, Universalism > Self-direction, Achievement | DRIVE: Pokój: Kapuleti i Monteki MUSZĄ żyć w pokoju, tylko nie Celina x Eustachy)
    * "Ciężką pracą dojdziemy do tego, że wszystko będzie działać jak powinno. Ja nie wtrącam się do działania Bartłomieja Korkorana; on wie lepiej."
* Ralf Tapszecz: mag (domena: DOMINUJĄCA ROZPACZ) i nowy dodatek do Inferni, kultysta Interis. Noktianin, savaranin.
    * (ENCAO:  -00-+ |Bezbarwny, przezroczysty;;Złośliwy;;Buja w obłokach| VALS: Conformity, Humility >> Benevolence| DRIVE: Piękno zniszczenia)
    * "Nasze porażki nie mają znaczenia. Tak czy inaczej zwyciężymy."
* Tymon Korkoran: egzaltowany lekkoduch i już nie następca Bartłomieja
    * (ENCAO:  +--00 |Co chce, weźmie;;Niecierpliwy;;Egzaltowany | Stimulation, Tradition > Face, Humility | TAK: Być lepszym od Bartłomieja w oczach Kidirona)
    * "Infernia jest moja i należy do mnie. Bartłomiej marnuje jej potencjał, a dzięki niej Nativis może rządzić Neikatis!"
* Mariusz Dobrowąs: oficer wojskowy Inferni
    * (ENCAO:  0-0+- |Nudny i przewidywalny;;Rodzinny| VALS: Humility, Hedonism >> Stimulation | DRIVE: Supremacja Nativis i Inferni)
    * "Pomagamy SWOIM, inni nie mają znaczenia. Pisałem się na pomoc swoim."

#### 2. Arkologia Nativis

* Rafał Kidiron
    * (ENCAO: 00+-0 |precyzyjny i zaplanowany;; DIMIR (UB);; Żywy wąż nie człowiek | Achievement, Power, Face | DRIVE: Duma i monument)
    * security lord i nieformalny dyktator arkologii
    * "Nasza arkologia zaszła tak daleko i nie możemy pozwolić, by cokolwiek stanęło na drodzę jej absolutnej wielkości."
    * "Tylko ci, którzy są przydatni mają miejsce w Nativis"
* Lycoris Kidiron: 57 lat (ze wspomaganiami drakolickimi dającymi jej efektywność 3x-latki)
    * (ENCAO: +0+-0 | Proaktywna;; Pierwsza w działaniu | Security, Achievement > Conformism | DRIVE: Kapitan Ahab (perfekcyjna arkologia))
    * ekspert od bioinżynierii i terraformacji, pionier (zdolna do przeżycia w trudnym terenie)
    * "Nikt, kto próbował się dopasować do innych nie doprowadził do postępu. Dzięki mnie Nativis będzie bezpieczna."
* Laurencjusz Kidiron
    * (ENCAO: +0--- |Żyje chwilą;;Nudny| Face, Power, Hedonism > Achievement | DRIVE: Przejąć władzę nad arkologią)
    * "Ta arkologia musi należeć do mnie. Ile może jeszcze być ograniczana przez Rafała? On nie jest tylko 'szefem ochrony'..."
* Kalia Awiter: influencerka z Nativis. Aktywnie próbuje pójść do łóżka z Eustachym i go podbić. Wierzy w Infernię. 21 lat. Drakolitka.
    * (ENCAO:  +--00 |Stoicka;;Radosna, cieszy się i daje radość;;Dzikie i spontaniczne pomysły| VALS: Universalism, Stimulation, Conformity | DRIVE: Hopebringer: Dać każdemu coś o co warto walczyć)
    * "Nativis musi być centrum kultury Neikatis. Musimy dać COŚ dlaczego warto żyć. Coś więcej niż tylko przetrwanie!"

### Scena Zero - impl

.

### Sesja Właściwa - impl

Eustachy, świeżo po "zbrodni wojennej". Czyli po ustrzeleniu noktian. Zbrodnia Inferni a nie Eustachego. Wujek poprosił Eustachego do gabinetu.

* Wujek: "Myślałem że wtedy to był przypadek. Ale teraz - Jak mogłeś strzelać do cywili? To nie było potrzebne. Infernia by sobie poradziła, jesteś dobry, jesteś NAJLEPSZY. Dałbyś radę!"
* Eustachy: "Otworzyli do nas ogień z najcięższych dział i mimo, że komunikacja była wydana i misja ratunkowa to w odpowiedzi powiedzieli że... (prawdziwy przebieg bez roli Inferni)"
* Wujek (z rozczarowaniem, nie złością): "Gdyby dziecko Cię uderzyło ze złości, kopniesz?"
* Eustachy: "Z glockiem w ręku, tak"
* Wujek: "Zrobiłeś WSZYSTKO podczas misji by uniknąć ofiar ludzkich?"
* Eustachy: "Tak (dowody)"
* Wujek: "To czemu użyłeś 8 rakiet a nie dwóch!"
* Eustachy: "Point defense?"
* Kidiron (wchodząc do pomieszczenia): "To było konieczne. Infernia mogła być uszkodzona. A oni strzelali do Inferni pierwsi."
* Kidiron (do Eustachego): "Eustachy wygrał w konkursie."
* Wujek: "Rafał, co odpierdoliłeś tym razem?"
* Kidiron: "Bartek, nic. Jakkolwiek to wygląda, loteria była uczciwa, nie wiem by nie była. Eustachy wygrał dzień w Dzielnicy Luksusu z Kalią."
* Wujek: "Jeśli próbujesz go przekupić lub odczarować jego reputację tą swoją słodką idolką..."
* Kidiron: "Przemknęło mi to przez myśl, ale nie. Naprawdę, nie mam z tym nic wspólnego. Tym razem to musiał być ślepy los."
* Eustachy: "A może to Kalia?"
* Kidiron: "To możliwe, ona losowała."
* Wujek: "(patrzy na Eustachego) Co jej... jak Ty i ona..."
* Kidiron: "Wiesz, są w podobnym wieku."
* Wujek: "Rafał. Zdeprawuje mi Eustachego."
* Kidiron: "Kalia nie jest taką dziewczyną jak myślisz. Jest w niej dużo więcej niż Ci się wydaje."
* Eustachy: "Mogę odpuścić? Nie zapisywałem się do loterii, byłoby nieuczciwe..."
* Kidiron: "Nie zapisywałeś się?" /zdziwiony "97 osób wrzuciło Twój los. Bo to było 'kto idzie na randkę z Kalią' i 97 osób wybrało Ciebie"
* Eustachy: "Ja nie chcę..."
* Kidiron: "Byłoby to... słuchaj (ojcowskim głosem) <mówi o tym jak zostać bohaterem ludu>"
* Kidiron: "Arkologia jest surową panią. Nie robimy tego co chcemy. Robimy to dla arkologii. (JEŻELI CHCESZ BYĆ DOWÓDCĄ INFERNI)"

Kidiron poszedł pomóc Eustachemu wybrać jakiś mundurowaty obiekt.

2 DNI PÓŹNIEJ

Ardilla i Ralf. Ralf pokazuje Ardilli jak ciszej, ona mu jak szybciej. Ralf "też wrzuciłaś los na Eustachego?". Ardilla: "może". Ralf się uśmiechnął. Ardilla: "Eustachy jest tak zgorzkniały że potrzebna mu dziewczyna która go ustawi". Ralf: "ja dlatego, bo nie powinien być samotny. Mag jego mocy nie może być samotny. Musi mieć o co walczyć."

Ardilla "chcesz sobie znaleźć dziewczynę?" Ralf "nie, skończy się w pustce"

Tr +3:

* V: Jestem ostatni... mój statek nie żyje. Widziałem... JEGO oblicze. Dlatego mag nie może być samotny.
    * A: Niesiesz nadzieję załogi, powinieneś korzystać z życia, tylko ty możesz
    * R: (spojrzał dziwnie) ale po co korzystać z życia jak marnujesz energię?
    * A: po co energia skoro nie korzystasz z życia?
    * R: jesteś jedyną osobą spoza mojej rodziny z którą rozmawiam
    * R: prośba - nie znajduj mi dziewczyny
    * A: (...) ci dziwni znajomi byli użyteczni. /to zrozumiał

Ardilla i Ralf trafili na mniej czysty obszar, taki... nietypowy. I Ardilla słyszy wołanie o pomoc z jednej z rur. To młody głos. Ciągnie tam Ralfa. Młody zapłakany głos. I jak usłyszał Ardillę zaczął płakać.

Musiał iść górą, coś się zawaliło i jest nad Wami. "Jesteś dziewczyną, dziewczyny nie umieją". To jakiś 14-latek zaklinowany w rurze i to solidnie.

Ardilla opuszcza rurę kontrolowanie i technicznie.

TrZ+3:

* V: udało się rurę bezproblemowo zsunąć żeby dzieciak wypadał.
* V: Ralf go złapał i nic się nikomu nie stał
* V: Ardilla zrzuciła rurę kontrolowanie by nic nikomu się nie stało.

Chłopak wpełzł do rury, znalazł broszkę i dał ją Ardilli. Ardilla przypięła jemu. Chłopak chce sprzedać broszkę i kupić wujkowi. Ardilla poprosiła o chustkę ("do skompletowania image" by zrobić chłopcu dobrze).

Chłopak przedstawił jako Marcin i opowiada. Ardilla "nie mów wujkowi że wpadłeś w kłopoty - czego nie wie to nie zrobi mu krzywdy".

Wujek mieszka w bardziej uszkodzonej części arkologii. Franciszek. Tańsze protezy. Koleś poznał Ardillę z Inferni - "gdyby nie Infernia nie byłoby nas tutaj". Opowiedział o ucieczce z umierającej arkologii i wujek Bartłomiej ich uratował.

Marcin: "Jak będę duży to będę latał na Inferni!!"

.

* Ralf: "Masz dobre podejście do ludzi. Może się myliłem. Może Infernia jest symbolem. Może symbol jest potrzebny."

Tr Z (bo widzi Ardillę w akcji) +3:

* X: Ralf spochmurniał lekko.
    * A: Czasem ludzie potrzebują czegoś co daje oparcie
    * R: Dlaczego Bartłomiej Korkoran dba o ludzi spoza swego statku i zmniejsza swoją użyteczność?
    * R: Zmniejszyłaś swoją użyteczność dla Inferni ale podniosłaś dla arkologii. Jaką funkcję mają ci ludzie? Poza tym, że są... ciepli?
    * A: Są... ludźmi, jak my. Muszą być czymś więcej?
    * R: (smutno) Jednostki bez użyteczności są przekształcone. Muszą odejść.
    * A: Patrzyłeś na dzieci?
    * R: Mają potencjał.
    * A: WSZYSCY mają potencjał i mogą pomóc. Nawet ten gość tutaj. Pomógłby nam.
    * R: (ciężko myśli) Myślisz w świecie większej energii niż ja. Ale ja nie jestem... mój dom nie żyje...

PÓŹNIEJ

Piękny dzień. Eustachy - jesteś w Dzielnicy Luksusu. Eustachy ma ordery - za zasługi, za odwagę...

* A: "Jednak zaprosiłeś Kalię na kolację? Myślałem że nie spotkasz się z żadną dziewczyną. Myślałam, że wolisz Kidirona niż dziewczyny w Twoim wieku"
* E: "Udam że tego nie słyszałem"

Kalia to OBRAZ I ZJAWISKO. Naprawdę ładna. I wyraźnie się cieszy na widok Eustachego.

* Kalia: "Eustachy Korkoran! Chciałam poznać od dawna." /podaje rękę

Miło przywitała Ardillę i Eustachego i pociągnęła Eustachego za sobą ze zdjęciem do mediów społecznościowych. Spacerują razem i ona pokazuje rośliny i o nich opowiada. Eustachy "nie jestem znużony spacerem. Wyciągam co mogę". Kalia "wiesz, chciałam pokazać Ci co robisz i o co walczysz. Ale może to był zły plan. Jakie masz hobby i co lubisz?" Eustachy: "wybuchy". Kalia odpaliła holoprojektor (by wyglądać inaczej) i zaprowadziła go...

* Kalia: "Podobno nie chciałeś iść ze mną na randkę?"
* Eustachy: "Nie brałem udziału w loterii, nie wiedziałem że tu jest"
* Kalia: "To jest ustawka. Zrobiłam, żebyś wygrał" /z kamienną miną "Nie domyśliłeś się?"
* Eustachy: "Domyśliłem się"
* Eustachy: "Ale dlaczego to zrobiłaś? Podobno zagłosowało na mnie 97 osób"
* Kalia: "Bo nigdy nie zaprosiłeś mnie na randkę. A chciałam" 

Eustachy:

* K: "Naprawdę nie chciałeś iść ze mną na randkę w fajnym miejscu, takim spokojniejszym, słuchaj - mogę nie być w Twoim typie ale nic nie płacisz i robimy fajne rzeczy."
* E: "Nie mój target jak chodzi o wolny czas. Plus za bardzo zajęty by mieć głowę w obłokach"
* K: "Dlatego MUSISZ mieć głowę w obłokach"
* K: "Patrzyłeś kiedyś na tą arkologię jak na coś innego niż cele?"
* E: "To mój dom"
* K: "Nie. Twoim domem jest Infernia. Jesteś tu się napić, olać dziewczyny, nie idziesz z chłopakami."
* E: "Potrzebuję zaplecza, garażu. Osiedla. Infernia potrzebuje arkologii więc ja też potrzebuję arkologii."
* K: (przytuliła E) "Musisz mieć coś dla CZEGO walczysz. Nie mnie, ok, to może "
* E: (zbliża, nachyla się do ucha K) A co jeśli już jej nie przejąłem?
* E: A co jeśli on już wygrał?
* K: (nie udaje) To jesteśmy zgubieni. Nie dlatego że Infernia. Dlatego, że szkoda Ciebie. Tyle robisz.
* E: (do Inferni) uruchom się /by pokazać wujkowi intencyjnie
* K: (śmiech) Podrywasz mnie na Infernię! XD

ZDJĘCIE PRZED AKTYWNĄ INFERNIĄ!

Eustachy chce poznać korzenie arkologii, by przegrzebać historię Inferni. Kalia jest niesamowicie dobrze poinformowana.

* Infernia jest starą jednostką. Infernia przyleciała z Bartłomiejem Korkoranem. Podobno on ją "zdobył" ale nie "kupił", nie "pozyskał". Mówił, że jest "stara". Traktuje ją jak... dziwną ciotkę.
* "Infernia jest symbolem nadziei" (wujek). Powiedział "Bo czasami diabły wypalają zło."
* Arkologia umierała, Kidironowie ją naprawili i odbudowali. Oni ją rozwijają i sprawiają że inni mogą tu przybyć
    * Ale... tylko użyteczne osoby mają miejsce w naszej arkologii. Model savarański.

Eustachy i Kalia są pogrążeni w rozmowie o muzach

TrZ +2:

* X: Ardilla nie da rady się wycofać i schować. Poświeca przyszły materiał.
* V: Ardilla popycha Eustachego. Eustachy wpada na Kalię.

Kalia leży pod Eustachy.

* K: Szzzybko działasz
* K: Możesz wstać. Pocałować w policzek też.

Eustachy wstaje. Nie całuje. Odprowadza Kalię.

* Kalia: /zrezygnowana "Czyli to nasza ostatnia randka, prawda?"
* Eustachy: "Nie zakładałbym najgorszego, nie lubię się spieszyć"
* Kalia: "Czy jest COŚ w tej arkologii na czym Tobie zależy? Tobie, nie Inferni?"
* Eustachy: "Jest." (nie ludzie a arkologia, przeciwności i widzą w wujku i w rodzinie tą dobrą stronę, ostoję bezpieczeństwa)

.

* Kalia zadaje pytanie: "A jakie dziewczyny lubisz? Wiem, że nie mam szans, chciałabym wiedzieć... czy masz szansę. Tu. Zależy mi na Tobie, wiesz?"
* Eustachy: "Miło z Twojej strony, ale jeszcze o tym nie myślałem" /Eustachy jest samotnikiem. Introwertyk samotnik z wybuchami.

Eustachy odprowadza Kalię do domu. I w okolicy Ambasadorki - Eustachy poczuł potężną emanację magiczną. Klasy jego mocy. W Ambasadorce stało się coś ZŁEGO i magicznego.

Eustachy: "Przepraszam, jestem potrzebny na mostku Inferni". I czmychnął. Kalia patrzy za Eustachym ze zdziwieniem. Co się właśnie stało..?

Ardilla śledzi młodą parę, ale nie wie o magii, więc widzi po prostu, że Eustachy nagle ucieka od Kalii XD. Ardilla podchodzi do Kalii Do zażartować, że Eustachy stracił dobrą okazję i że burak jest.

## Streszczenie

Wujek opieprza Eustachego za krzywdę niewinnych noktian, ale Kidiron się za Eustachym wstawia. Ardilla socjalizuje Ralfa i ratuje młodego Marcinka który się zaklinował w rurze Szczurowiska; okazuje się, że wszyscy kojarzą Infernię jako symbol nadziei. Potem Eustachy idzie (z przymusu) na randkę z Kalią; okazuje się, że Kalia zmanipulowała by Eustachy wygrał bo chce dać mu jeden piękny dzień. Wszystkie dowody podsłuchała i złapała Ardilla. Gdy Eustachy poczuł chore emanacje magiczne z Ambasadorki, porzucił Kalię i pobiegł do Inferni.

## Progresja

* Eustachy Korkoran: w arkologii Nativis najbardziej zależy mu w sumie na tym że jego ród i wujek jest podziwiany i szanowany. Że jest ostoją bezpieczeństwa dla arkologii. Też sama arkologia i jej działanie.
* Kalia Awiter: ma suknię pozwalającą jej na holoprojektor oraz na zmianę wyglądu. Oraz zapewnia wszystkie kamery itp. na media społecznościowe.

## Zasługi

* Eustachy Korkoran: Rafał Kidiron broni jego decyzji przed wujkiem. Poszedł na randkę z Kalią (niechętnie, bo Kidiron mu kazał). Powiedział Kalii, że lubi wybuchy i że w sumie ma coś w arkologii na czym mu zależy. Trochę z nią flirtuje, ale tak po swojemu. Zdobył od Kalii co ona wie o Inferni. Gdy poczuł emanację magiczną z Ambasadorki, zostawił Kalię samą przy Ambasadorce i pobiegł w kierunku Inferni.
* Ardilla Korkoran: oswaja Ralfa i z nim lekko flirtuje (chcesz sobie znaleźć dziewczynę?). Pokazuje Ralfowi, że Nihilus nie jest wszechmocny, że jest po co żyć i działać. Ratuje Marcinka i wyjaśnia Ralfowi, że wszyscy ludzie mają potencjał. Ma do niego cierpliwość. Potem - śledzi Eustachego na randce z Kalią (i kibicuje Kalii). Zdobywa dowody, że Eustachy ma autokontrolę nad Infernią. Końsko zalotuje Eustachego i Kalię ;-). Gdy Eustachy zostawił Kalię w dziwnych okolicznościach, idzie do niej.
* Ralf Tapszecz: powoli zbliża się do Ardilli. Wyraźnie ma problemy z konfliktem kultury savarańskiej (musisz być użyteczny) a tym co _chce_. Przyznał się Ardilli że widział oczy Nihilusa i to go zmieniło. Wyraźnie chce pomóc innym, ale nie rozumie dlaczego warto pomóc komuś kto nie ma już potencjału - ale CHCE pomóc. Bardzo skonfliktowany. WIE, że mag (Eustachy) nie może być sam. Ma problem z pozycjonowaniem Bartłomieja Korkorana jako bohatera lub głupca marnującego zasoby.
* Bartłomiej Korkoran: próbuje pokazać Eustachemu, że E. jest lepszy i skuteczniejszy. Że nie musiał zabijać tych noktian. Próbuje przekonać Eustachego pod kątem honoru i prawości. Ale Kidiron się zgadza z Eustachym - Bartłomiej wygląda na starszego niż jest. Widzi, że przegrywa wojnę o Eustachego i nie wie czemu. DOWIADUJE SIĘ, że Infernia jest pod mentalną kontrolą Eustachego.
* Rafał Kidiron: broni Eustachego i jego decyzji przed wujkiem - jego zdaniem Eustachy zrobił co należy. Gdy Eustachy powiedział, że nie chce iść na randkę z Kalią, Kidiron mu powiedział - robimy co robimy dla arkologii. Nie to, co chcemy robić.
* Kalia Awiter: 21-letnia śliczna i inteligentna influencerka / idolka / inspiratorka Nativis. (ENCAO:  +--00 |Stoicka;;Radosna, cieszy się i daje radość;;Dzikie i spontaniczne pomysły| VALS: Universalism, Stimulation, Conformity | DRIVE: Hopebringer: Dać każdemu coś o co warto walczyć). Zmanipulowała loterią by wziąć Eustachego na randkę. Zmartwiona tym, że Eustachy nie ma niczego w arkologii na czym mu naprawdę zależy, chciała dać mu jeden dobry, udany dzień i zlinkować go mocniej z arkologią. Robi solidny research na temat tego co się dzieje.
* Marcin Pietraszczyk: młody chłopak (13), uratowany z jakiejś arkologii przez Infernię. Chciał zdobyć bogactwa i kupić prezent Franciszkowi, ale się zaklinował i Ardilla z Ralfem go uratowali.
* Franciszek Pietraszczyk: starszy człowiek z protezami, uratowany z jakiejś ruiny arkologii przez Infernię. Jest wdzięczny Bartłomiejowi Korkoranowi, adoptował Marcina. Nie jest bogaty, ale z przyjemnością się dzieli czym ma z tymi co mają mniej. Ciężko pracuje wdzięczny za nową szansę. Poznał Ardillę z Inferni i dał jej ciepło w slumsach arkologii.
* OO Infernia: okazuje się, że za rządów Bartłomieja Korkorana stała się symbolem nadziei dla Nativis. Ludzie rozpoznają ją jako pozytywną jednostkę która przyniesie dobro.

### Frakcje

* Imperium Kidirona w Nativis: ma też Kalię Awiter - propagandzistkę i idolkę najwyższej klasy, która pragnie dobra i powodzenia wszystkich ludzi, łącznie z Nativis.
* Szczury Arkologii Nativis: jest tam sporo dobrych, sympatycznych ludzi mających pecha, którzy żywią się resztkami ze stołu Kidirona.

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Nativis
                        1. Poziom 1 - Dolny
                            1. Północ - Stara Arkologia
                                1. Blokhaus F: najbardziej pasujące do definicji 'poruinnych slumsów' tereny, acz mające energię i generatory. Ulubione miejsce spacerów Ardilli z Ralfem.
                                1. Szczurowisko: jest ciepło, są grzyby i da się tam mieszkać i schronić. Mało kto się tam zapuszcza. Mieszka tam m.in. Franciszek z Marcinkiem.
                        1. Poziom 3 - Górny Środkowy
                            1. Wschód
                                1. Dzielnica Luksusu
                                    1. Ogrody Wiecznej Zieleni: piękna vista. Najlepsze ogrody widokowe, wspomagane holoprojektorami itp. Miejsce pierwotnej randki Eustachego i Kalii.
                                    1. Ambasadorka Ukojenia: klub towarzyski. Są tam wiły, ale też Towarzysze i Towarzyszki. Wyższej klasy ekipa. Coś chorego magicznie tam się stało; atak maga?
                                    1. Stacja holosymulacji: możliwość postrzelania sobie z Eustachym, miejsce faktycznej randki Kalii i Eustachego.
                            

## Czas

* Opóźnienie: 2
* Dni: 7

## Inne

### Układ arkologii Nativis

* Aspekty:
    * Bardzo dużo roślin, drzew itp. Piękne roślinne miejsce.
    * Bardzo spokojna arkologia, silnie stechnicyzowana i sprawna.
    * Dominanta: faeril > drakolici, ale próba balansowania. Niskie stężenie AI.
    * BIA jako centralna jednostka dowodząca, imieniem "Prometeus"
* Struktura:
    * Wielkość: 4.74 km kwadratowych, trzy poziomy
    * Populacja: 11900 (Stroszek)
        * Operations & Administration: 1100
        * Extraction & Manufacturing: 3213
        * Medical: 355
        * Science: 360
        * Maintenance: 700
        * Leisure: 1000
        * Social Services: 240
        * Transportation: 800
        * Training: 229
        * Life Support: 500
        * Computer & TAI & Communications: 500
        * Engineering: 500
        * Supply / Food: 1200
        * Security: 300
    * Bogactwo: wysokie. Technologia + piękna roślinność.
    * Eksport: żywność (przede wszystkim), komponenty konstrukcyjne, hightech, terraformacja
    * Import: luxuries, prime materials, water, terraforming devices, weapons
    * Manufacturing: jedzenie, konstrukcja, terraformacja
    * Kultura: 
        * dominanta: piękno, duma z tego co osiągnęli, duma z tego że nie ma wojny domowej faeril/drakolici
        * wyjątek: brak.
    * Przestrzenie
        * rozrywki: parki, natura, zdrowe ciało - zdrowy duch
        * inne: hotele, laboratoria terraformacyjne, restauracje...
    * Zasilanie
        * solar + microfusion
    * Żywność
        * różnorodna; to miejsce jest źródłem żywności
        * przede wszystkim roślinna, ale też zawiera elementy zwierzęce (te najczęściej z importu)
    * Life support => terraformacja + filtracja
    * Comms => biomechaniczne okablowanie + normalne sieci
    * TAI => BIA wysokiego stopnia imieniem Prometeus
    * Starport => large
    * Infrastructure => high-tech, dużo półautonomicznych TAI, wszystko nadrzędne przez Prometeusa. Świetny transport publiczny. 
    * Access & Security => arkologia z silnymi blast walls, silnymi liniami dostępowymi.

Historia: 

* w trakcie wojny domowej sprzedawała żywność na 2 fronty co spowodowało znaczne bogactwo ale też częściową niechęć innych arkologii
* obecnie prowadzi ekspansje na struktury zrujnowane/opuszczone po wojnie żeby całkowicie się uniezależnić od dostaw spoza planety
