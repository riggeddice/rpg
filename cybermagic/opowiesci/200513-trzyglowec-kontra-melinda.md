## Metadane

* title: "Trzygłowiec kontra Melinda"
* threads: dzien-z-zycia-terminusa
* gm: żółw
* players: fox, kapsel, kić

## Kontynuacja
### Kampanijna

* [200520 - Figurka a kopie zapasowe](200520-figurka-a-kopie-zapasowe)

### Chronologiczna

* [200520 - Figurka a kopie zapasowe](200520-figurka-a-kopie-zapasowe)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* .

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

* jak ostatnio.

### Opis sytuacji

.

## Punkt zero

.

## Misja właściwa

Tydzień później, normalny biznes w Fantasmagorii. Nagle wpada Melinda z zakrwawionym chłopakiem i krzyczy, że muszą ich uratować. Janek cały załamany, co tu się dzieje - natychmiast wysyła się tą dwójkę na zaplecze. I wpada grupka z Trzygłowa, kilka osób (wszyscy to uciekinierzy z Aurum). Szukają zakrwawionego chłopaka i widzą ślady krwi na ziemi.

Natasza zaczyna próbować ich przekonać, że nie ma w tym sklepie nikogo takiego (Tr). Niestety, pewien staruszek nakapował na "przestępców" (Melinda + Adaś) i osiłki idą na zaplecze, gdzie teraz znajdują się Janek wraz z Melindą i Adasiem.

Janek wychodzi do przodu, pomiędzy nich, osiłki zaczynają się rzucać a Melinda odpala bombę dymną. BOMBĘ DYMNĄ. Mega cuchnącą, straszliwą bombę dymną. W sklepie robi się pandemonium - ludzie wymiotują, osiłki uciekają (by nie wymiotować), smród wchodzi w kobierce itp. Ogólnie, pandemonium i tragedia. Nawet Natasza, Janek i Kinga muszą uciec z tego miejsca.

Ale Natasza coś "poczuła". Coś... coś tu jest, coś jeszcze. Coś magicznego. Janek się skupił i to znalazł - to są jakieś pajęczaki. Infiltratory - pajęczaki o wielkości przedramienia. Jeden z gości miał na plecach pajęczaka, toteż osiłki. Plus ze sklepu ucieka jeden z pajęczaków, próbując poradzić sobie ze straszliwym smrodem. Są tu też dwie niewidzialne drony...

Janek dał radę rzucić nożem i odciąć jednemu z pajęczaków nóżkę. Ma dzięki temu coś, co stanowi dowód że coś tu jest nie tak. Coś, co można zbadać.

Coś tu się dzieje, coś strasznego. Drony, infiltratory i Melinda z zakrawionym delikwentem?

Paręnaście minut później Janek i Natasza wrócili do sklepu. Melinda jest w środku, 100% spokoju i komfortu. Wzięła jakiś 'antyeliksir'. Otwiera wszystkie okna dla Nataszy; a Janek zauważa, że ranny chłopak na ziemi (20 lat?) ma na plecach jednego z pajęczaków.

Podczas swobodnej rozmowy z Melindą Natasza doszła do tego o co wysokopoziomowo chodzi, w czasie gdy Janek zaczajął się na infiltratora.

* Adam jest jednym z gości z Trzygłowca. Podobno ich zdradził; na pewno chciał uciec z gangu.
* Melinda poznała go podczas prowadzenia show w którym zarabia - "Księżniczka doradzi". Ludzie pytają, ona doradza. Za to jej płacą (Melinda wierzy że pomaga, ludzie przychodzą się śmiać)
* WOW. Zdaniem Janka to jest coś przed czym Tukan powinien ją chronić. A to Tukan ją w to wprowadził; jak miałaby sama znaleźć pracę?

Ech. Czyli Melinda wciągnęła się w bandę uciekinierów z Aurum. I wpadła w kłopoty. A w tym czasie Janek poluje na pajęczaka; z zaskoczenia wziął "packę" którą zdobył od Karaluchów i zaatakował pajęczaka z zaskoczenia.

* Skutecznie go odczepił i zneutralizował
* Wsadził go do "słoika" - kontenera - póki sam pajęczak jest niegroźny
* Adam na którym znajdował się infiltrator był z nim neurosprzężony. Adama boli i Adam cierpi.
* Melinda opieprza Janka, dość solidnie.

Dobrze, Janek powoli ma dość tego wszystkiego. Serio. Melinda się do niego przyczepiła, nie chciał jej mieć w swojej okolicy, nie zależało mu na jej obecności a zdemolowała sklep i ogólnie wprowadziła niepotrzebny chaos do całej tej sytuacji. Janek zdecydował się spacyfikować Melindę - dał jej z liścia i zdecydował się ją poważnie opieprzyć za to co zrobiła. (XXXXOV):

* Melinda ucieka. Płacz, załamana, jej idol UPADŁ, jej serduszko jest złamane. Melinda nie patrzy na Janka jak patrzyła wcześniej.
* Melindę jeszcze się DA przekonać; jest skonfliktowana i widzi, że wprowadziła chaos, że stanowi pewien problem.

Janek nie jest z tego powodu szczęśliwy. Chciał ją trochę usadzić, chciał pokazać jej że jest zły. Ale nie spodziewał się tak silnej reakcji młodej arystokratki. Nie do końca o to mu chodziło. No nic... jakoś to się rozwiąże w przyszłości. A tymczasem Kinga skupia się na tym, by dojść do tego co to za cholerny pajęczak. Na szczęście ma informacje z hipernetu.

* Doszła do tego, że pajęczak to infiltrator klasy IV; model stosowany przede wszystkim w Aurum, reklamowany jako "przeczytaj wspomnienia i dowiedz się czy Cię zdradza"
* Ale niestety dojście do tego co i jak, lub śledzenie infiltratora - wymaga magii

Ech, magia. Nie są magami i magii nie mają. Ale mają KONTAKTY. Może są w stanie znaleźć jakiegoś maga który pomoże za dobre słowo. Tak więc Natasza zadzwoniła do swojej znajomej, Katji. Katja obraca się wokół Rekinów, a tam jest sporo magów. No i Katja poleciła właściwego maga - Filip Keksik, chcący być zwanym "Pożeraczem". Mentalista i iluzjonista; powinien być perfekcyjny wobec celu jaki ma Zespół.

Kinga przygotowała się na to spotkanie. Sfabrykowała Obsydianowy Posążek Żabboga. Zrobiła go MEGA EPICKO, tak, by wyglądał na najdoskonalszy przedmiot na świecie, z głęboką magią i dużą powagą. Udało jej się (Ex!). Co prawda Pożeracz dowie się o Melindzie, ale będzie przekonany że ten przedmiot to jest THE THING.

Przybył Pożeracz. Ubrany w potężny, kompozytowy pancerz. Z płonącymi oczami. Na ścigaczu porośniętym skórą rekina. Natasza próbowała zachować powagę - udało jej się to. Z maksymalną uniżonością, pokorą i wazeliną podała Pożeraczowi figurkę. Dzięki wybitnej pracy zrobionej przez Kingę Pożeracz UWIERZYŁ, że ten artefakt faktycznie jest prawdziwy i posiada specjalne możliwości i umiejętności.

Pożeracz stwierdził, że się odwdzięczy. Bardzo się odwdzięczy. Znajdzie kto stoi za infiltratorem i im pomoże w zamian za tą figurkę, która prawie jest godna jego pomocy i jego splendoru. Natasza powiedziała, że jest to wielki czyn. Podnóżkowała zdrowo jak Toadie księciu Iktornowi.

Pożeracz odnalazł maga - Mariusz Grabarz. Nagrywa filmy, live action, propagandowiec. Wysłał mu sygnał, że chce go tu zobaczyć. Grabarz, oczywiście, się pojawi jeśli arystokrata go wzywa - to ten typ maga. Co jednak naszego Pożeracza STRASZNIE zmartwiło - Grabarz puszcza to wszystko na żywo. I jest to transmitowane m.in. w Aurum. Jest wysokie ryzyko, że Pożeracz ma przechlapane u rodziny!!! BO JEST OGLĄDANY NA ŻYWO! A tam nie wiedzą o jego personie "mroczny rekini rycerz z płomieniami w oczach".

Gdy tylko Grabarz się pojawił, z przyjemnością wyjaśnił o co mu chodzi:

* Nagrywa program propagandowy.
* Trzygłowce są "durniami, którzy opuścili Aurum", pełnią rolę antagonistów.
* Jego klient, Rafał Torszecki, pełni rolę bohatera. Kogoś, kto nie pozwoli Trzygłowcom skrzywdzić Adama.
* Robi to dla pieniędzy i wpływów. Robi to co lubi. Jego scenariusz jest w zasadzie nieedytowalny.

Pożeracz zaczął skomleć, żeby Grabarz nie uwzględniał go w nagraniach, w końcu kiedyś mu pomógł - Grabarz jest jednak nieubłagany. Nie jest w stanie tego łatwo zrobić a to jedynie pomoże Torszeckiemu lepiej wyglądać. A Pożeracz przecież miał szansę pomóc Grabarzowi, ale tego nie zrobił.

W świetle powyższego, Natasza ruszyła do negocjacji. Będzie współpracować z Grabarzem, by wzmocnić sklep z perspektywy Aurum (tam jest kasa) i chronić Melindę (na czym bardzo zależy Jankowi - a Grabarz nie zorientował się jeszcze z obecności Melindy i potencjalnej z nią dramą).

* Scenariusz bez zmian - Torszecki jest bohaterem.
* Pokazanie Podwiertu jako takiej śmieciowej, żałosnej lokalizacji - ale sklepu Fantasmagoria jako miejsca mającego SENS i wartość.
* Czyli Fantasmagoria jest jedynym miejscem godnym w całym Aurum. Jest to też miejsce serwilistyczne.
* Wbudowano plausible deniability - "że Fantasmagoria nei wiedziała że to idzie do Aurum i że jest film"
* A sam Pożeracz, cóż - wielka hańba, do poziomu wycofania do Aurum by nie hańbić rodziców i siostry.

Więc wszyscy zostali rzuceni pod ciężarówkę. Nadszedł czas porozmawiania z Melindą, by nie zrobiła nic głupiego i nie skupiła na siebie ognia Grabarza. Janek poszedł do klubu Arkadia, gdzie jest Melinda z przyjaciółką. I odbił się od Diany "Lauris". Diana powiedziała, że Melinda nie chce z nim rozmawiać. Janek zaczął Dianę przekonywać, że robił to dla Melindy, że się zagalopował, że chce przeprosić... Melinda to słyszała, ale nie jest przekonywalna, nie teraz (XXXXXVV) - zwłaszcza, że Janek idzie współpracować z Grabarzem i uważa się za słabego.

Ale jakby Janek zrobił jakiś Heroiczny Czyn, kto wie, może Melinda wróci ;-). Acz akceptuje ryzyko z Dianą - Melinda będzie grzeczna i bezpieczna, zostanie w Arkadii.

(na marginesie, TAK, to Diana załatwiła Melindzie tą upokarzającą pracę pod wpływem Tukana)

## Streszczenie

Mariusz Grabarz robi program mający podnieść swojego klienta (Rafała Torszeckiego) w oczach Aurum; przez to wkręca Trzygłowca, gang uciekinierów z Aurum w małą wojnę domową. Ma wszystkie pozwolenia. Gdy Melinda próbowała jednemu z Trzygłowców pomóc, wpadło to na Fantasmagorię. W wyniku Zespół poszedł na współpracę z Grabarzem, skompromitowali Podwiert ale zyskali pewne korzyści finansowe. Niestety (?), przy okazji Jan stracił Melindę jako stalkerkę.

## Progresja

* Melinda Teilert: Janek złamał jej serce, uderzył ją w twarz. Uciekła do Diany, swojej przyjaciółki w Arkadii. Nie jest już stalkerką.
* Jan Łowicz: Nie ma już stalkerki; złamał Melindzie serce. Ale może jeszcze mieć przyjaciółkę jeśli zrobi Bohaterski Czyn i przeprosi.

### Frakcji

* .

## Zasługi

* Jan Łowicz: znalazł infiltratory w śmierdzącej mgle zrobionej przez Melindę; potem jednak by Melindę uspokoić dał jej z liścia. Nie dał rady jej przeprosić. Melinda uciekła do Diany.
* Natasza Aniel: koordynuje całość komunikacji z Grabarzem i osiłkami. Dba o swoje - mimo kryzysowej sytuacji z reality show, wyszła na wierzch choć raniąc reputację Podwiertu w Aurum.
* Kinga Kruk: mistrzyni robienia fałszywek - zrobiła taką fałszywkę, że nawet mag się nie zorientował że jest bezwartościowa. Plus, znalazła w hipernecie co to jest infiltrator.
* Melinda Teilert: chce ratować chłopaka z Trzygłowca (Adama), chowa się na zapleczu i rzuca bombę śmierdzącą. Doprowadziła Janka do utraty kontroli i ów ją spoliczkował. Uciekła.
* Katja Nowik: poproszona przez Nataszę, poleciła Feliksa ("Pożeracza") jako proste rozwiązanie problemów ze znalezieniem mentalisty.
* Feliks Keksik: niewielki i mało znaczący mentalista, każe zwać się "Pożeraczem" i chodzi na iluzjach. Pomógł Fantasmagorii i jego wizerunek "Pożeracza" trafił do Aurum. HAŃBA.
* Adam Cześń: chłopak z Trzygłowca, który chciał odejść i przez machinacje Grabarza został wplątany w to, że koledzy myśleli że ich zdradził - i chcieli go zbić.
* Diana Lemurczak: jako "Diana Lauris"; przyjaciółka Melindy, która ją przyjęła i dała pracę. Bardzo inteligentna alchemiczka, dała Melindzie bombę gazową.
* Mariusz Grabarz: propagandzista Aurum, który skłóca Trzygłowca ze sobą, by tylko kosztem krwi ludzi zdobyć punkty w Aurum i zadowolić klienta - Torszeckiego.
* Rafał Torszecki: arystokrata Aurum, zapłacił Grabarzowi by podnieść swoją pozycję i reputację w Aurum.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Osiedle Leszczynowe
                                    1. Sklep z reliktami Fantasmagoria: na zapleczu ratowany był Adam z Trzygłowca, a potem odpaliła bomba smrodowa...
                                    1. Klub Arkadia: należy do Diany Lauris (Lemurczak). Miejsce, w którym czasem Melinda występuje w programie "Księżniczka doradzi".

## Czas

* Opóźnienie: 19
* Dni: 9
