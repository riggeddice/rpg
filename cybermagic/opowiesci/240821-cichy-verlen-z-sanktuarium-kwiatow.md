## Metadane

* title: "Cichy Verlen z Sanktuarium Kwiatów"
* threads: strefa-sanktuarium-verlenlandu
* motives: tien-na-wlosciach, misja-ratunkowa, bardzo-grozne-potwory, magowie-pomagaja-ludziom, symbol-lepszego-jutra, strefa-sanktuarium
* gm: żółw
* players: fox, kić

## Kontynuacja
### Kampanijna

* [240801 - Bohaterska śmierć Wężomysza](240801-bohaterska-smierc-wezomysza)

### Chronologiczna

* [240801 - Bohaterska śmierć Wężomysza](240801-bohaterska-smierc-wezomysza)

## Plan sesji
### Projekt Wizji Sesji

Sesja powstała z sesji 0, więc jest ograniczona w motywach. I nie tylko.

* PIOSENKA WIODĄCA: 
    * Brak XD
    * Inspiracje
        * Malenia, blade of Miquella
        * Isuzu, Magic:Reloaded
     * Uczucie dominujące
        * brak XD
* Opowieść o (Theme and vision):
    * "Sanktuarium Kwiatów - zupełnie inny Verlenland"
        * .
    * ?
        * ?
    * Lokalizacja: Sanktuarium Kwiatów
* Po co ta sesja?
    * Jak wzbogaca świat
        * Pokazuje bardzo fajnego maga, Cichośwista
    * Czemu jest ciekawa?
        * Pokazuje jak działa Exemplis, w różny sposób
    * Jakie emocje?
        * "Wow, to TEŻ jest Verlenland?"
* Motive-split
    * tien-na-wlosciach: samotny Cichoświst w Sanktuarium Kwiatów, walczący o piękno w Verlenlandzie i wspierany przez swoich ludzi
    * misja-ratunkowa: randomy z Przelotyka wysłały SOS, Cichoświst odpowiedział. Bo jest Verlenem, jest to konieczne i to zrobi. A potem jego ratują Arianna i Viorika :D.
    * bardzo-grozne-potwory: splitharpia z Przelotyka ("Brisela"-class), na którą poluje Cichoświst by odepchnąć ją od narażonych ludzi
    * magowie-pomagaja-ludziom: tien Cichoświst Verlen, który rzucił się ratować randomy z Przelotyka. A potem Arianna i Viorika, które poleciały pomóc Cichoświstowi i tym ludziom.
    * symbol-lepszego-jutra: może Cichoświst mało mówi, może jest mało widoczny, ale działa tak jak powinien - cicho, szybko, ratując wszystkich. Jest Exemplarem Verlenlandu, nawet, jak trochę innym.
    * strefa-sanktuarium: Sanktuarium Kwiatów jest sanktuarium odmiennym od reszty Verlenlandu, ale też jest miejscem odpoczynku i radości.
* O co grają Gracze?
    * Sukces:
        * .
    * Porażka: 
        * .
* O co gra MG?
    * Highlevel
        * Pokazanie Cichośwista i Sanktuarium Kwiatów
    * Co chcę uzyskać fabularnie
        * Zainteresowanie Zespołu Sanktuarium Kwiatów
* Default Future
    * .
* Dilemma:
    * ?
* Crisis source: 
    * HEART: ""
    * VISIBLE: ""
        
### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)

* Cichoświst poluje na splitharpię w okolicach Przelotyku i prosi o wsparcie (bo to krzywdzi ludzi)
* Cichoświst pokonany, ale uratowali ludzi

### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)

* Faza 1: Walka z splitharpią alucis
    * "Brisela" (Lerissa-class, alucis + praecis, groźny przeciwnik)
        * z Cichoświstem używającym shotguna, miecza i tarczy, niezwykłej prędkości i techniki
        * Cichoświst chroni 3 ludzi; jego pięcioosobowy oddział ledwo daje radę. On chroni wszystkich.
        * ona używa: MINDCONTROL, SPLIT, CLONE STRIKE! itp
* Faza 2: Odpoczynek w Sanktuarium Kwiatów
    * zasadzenie własnych kwiatów (herbalist Kamil)
    * zmierzenie się ze szczytem strachu (historian Maja)
    * pokazanie żołnierzom jak wyciągnąć młodych z lasu mgieł (kapral Wojmił)
    * porozmawiać z rannymi i chorymi w szpitalu (lekarz Brunon)
    * teatrzyk (dzieci dla tienów, pokazują walkę ze smokiem ze Smoczej Góry, z zabawkami)
* Faza 3: Brunhilda
    * 
* Faza 4: Sanktuarium, Upadłe
* Faza 5: Ucieczka od Cichośwista
* Overall
    * chains:
        * ?
    * stakes:
        * śmierć cywili
    * opponent
        * toksyczna sentisieć; atmosfera
    * problem
        * ludzie cierpią

## Sesja - analiza
### Mapa Frakcji

.

### Potwory i blokady

* teren
* fanatyzm

### Fiszki

.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Arianna i Viorika w północnej części Verlenlandu. I dostają sygnał SOS od Verlena - z okolic Przelotyka, czyli safari. Wiadomość czytelna "Cichoświst Verlen, niebezpieczny przeciwnik, osłaniam ludzi, nie utrzymam. Wsparcie Verlena - potrzebne."

A+V szybko się zbierają i lecą. Maksymalna prędkością sensowną (nic się nie stanie); ścigacz z założeniem że Verlen ma broń. Szybkie pytanie o dobry skrót do miejscowych Verlenlandczyków i jest informacja - +P.

Tr P +3:

* V: tak szybko że nic się nie stało
    * po drodze Wasz ścigacz mija 8-osobą grupkę Verlenlandrynków, uzbrojonych, zmierzających ostrożnie w tą samą stronę. Idą wesprzeć Cichośwista.
    * dostarczyli Ariannie i Viorice elementy różnorodnego sprzętu który może im się przydać. Działko automatyczne - wielkokalibrowe działko rozstawne.
    * zgodnie z rozkazami Arianny mają zabezpieczyć bezpieczny teren i odwrót.

Ścigacz wyskakuje zza skał i wyżyn tylko po to by wpaść na Verlena ostrzeliwuje przeciwko... anielicom? A jeden z chronionych ludzi przez Verlena próbuje strzelić mu w plecy.

Arianna robi drift ścigaczem, Viorika zeskakuje na kolesia który strzela do Cichośwista by go unieszkodliwić; Cichoświst chroni 10-15 ludzi. "Karawanę". A Arianna rozstawia działko.

* V: 
    * Viorika rzuciła się na delikwenta, delikatnie go zmiażdżyła serwopancerzem (no lethal intent, ale jak coś się złamie to nie płaczemy) i zdjęła go z Cichośwista
    * Arianna się rzuciła i rozstawiła działko automatyczne.
   
Anielice przygotowują się do ataku. Są w kręgu otaczającym Cichośwista i Viorikę. Arianna jest troszkę dalej. Lokalsi są częściowo w walce, częściowo oszołomieni.

Arianna się SKUPIA ze snajperką. Chce widzieć patterny. Działko ma strzelić i ma za zadanie pokazać PRAWDZIWOŚĆ przeciwników.

Tp +4:

* Xz: Gdy działko wystrzeliło, kilka...naście? anielic skupiło się na nowym zagrożeniu. Lecą w stronę działka. Nie zmniejsza to presji z Cichośwista i Vioriki, ale podnosi dla Arianny i działka.
* V: Arianna widzi coś... niepokojącego. Tych anielic jest mniej - to się zgadza - to są iluzyjne klony. Większość z tego to ułudy. Ale Arianna nie jest pewna czy wie gdzie są oryginalne anielice.
* X: Anielice słusznie uznały działko za główne zagrożenie. Skupiły się na JEGO ZNISZCZENIU. Działko je widzi - a przynajmniej lepiej.

Viorika próbuje skrzyknąć i zebrać do kupy lokalsów. Wytrącić ich ze stuporu. Zmobilizować do ognia zaporowego. Atakuje Alucis - siłę Anielic. Idzie "jako sierżant".

Tr Z +3:

* Xz: Widząc sytuację, Anielice skupiają się na wzmocnieniu ataku. Przestały udawać, że są nie-tam gdzie są i zintensyfikowały atak. Wyraźnie widać, że traktowały Cichośwista jako potencjalną przynętę. "Przyprowadzi ludzi". Ale Cichoświt okazał się być silniejszy - został ranny, dość ciężko ranny, z "niewidzialnego miejsca" - ale sam ściął niewidzialną Anielicę.
    * Arianna zorientowała się, że przeciwnik najpewniej nie może utrzymywać w tym momencie aż tyle iluzji i mocy i skupiła się na przekierowaniu feedów co podniosła Viorika. Viorika: +P
* Vz: Viorika otrząsnęła ludzi i ich zmobilizowała. Widok padającego ale wstającego Cichośwista pomógł. Ogień zaporowy, co sprawia, że Anielice mają duży problem - nie mogą się dobrze zbliżyć. WIĘC ich uwaga kieruje się ku Ariannie.
* ARIANNA CZMYCHNĘŁA NA DRZEWO! W konary drzewa. Przeciwnik tam do niej nie dotrze. (+P do puli; nie ma łatwego celu i Anielice wolą się wycofać)
* V: Anielice się wycofują. Nie ma tu dla nich niczego.

Cichoświst się obraca do Arianny i Vioriki. Lekki uśmiech. Mowa Cichośwista była krótka i treściwa - o jedności, współpracy i tym że razem ludzie chronią się przed potworami. Ludzie byli zdziwieni że mowa taka... krótka i mało kwiecista, ale W SUMIE CICHOŚWIST MOŻE BYĆ ZMĘCZONY. Nadal - jest larger niż life.

Jak tylko się udało problem rozwiązać, jego dźwiedź - Ponury Bicz - zaczął go łajać. Że znowu za dużo wrzucił na siebie. Że znowu próbował zrobić samemu.

Ponury Bicz spytał Ariannę o jej stan matrymonialny i dokucza Cichoświstowi że potrzebuje narzeczonej. Arianna się świetnie bawi a Cichoświst - czerwieni. Z jego perspektywy, Verlenowie powinni dążyć do piękna i kwiatów a nie tylko jak najlepszej walki.

Arianna się wprosiła do Sanktuarium Kwiatów. Viorika zabezpieczyła Anielicę. Teraz jest przekonana, że musi coś z tym zrobić XD. Dźwiedź zbierze truchło Anielicy - co z tym zrobić i jak do tego podejść. Bo potwór nie może przetrwać. W Przelotyku byłby niebezpieczny.

Sanktuarium Kwiatów, unikalne miejsce w Verlenlandzie. Dotarcie tam to trzeba się przebić przez typowy Verlenland - wzgórza, niebezpieczeństwa itp. Ale macie oddział, macie 3 Verlenóœ i jesteście "na swoich ziemiach". Nie jest to przyjemna trasa, ale to Wasz dom. Typowa trasa.

Cichoświst jak tylko jest w stanie próbuje się ruszać sam itp. Dźwiedź "popisujesz się przed Arianną!" Cichoświst odpuścił z pójściem "pokonam, dam radę" bo nie chce wyjść na osobę popisującą się przed Arianną. O to dźwiedziowi chodziło.

Viorika jest zaskoczona jego podejściem. Skąd pomysł na piękno, kwiaty - i lokacja Sanktuarium Kwiatów? XD. Nieco nie-Verlenowe.

* Viorika: Sanktuarium Kwiatów? Nietypowa nazwa i niestandardowa lokalizacja. Twój pomysł czy było?
* Cichoświst: Vioriko, (chwila ciszy bo nie wie jak to powiedzieć), jesteśmy istotami Exemplis. Szukamy najlepszego w naszej naturze. Jeśli ludzie... ludzie potrzebują piękna, potrzebują miejsca wartego ochrony. To jest w naszej naturze. Perfekcja to nie tylko walka. Jest piękno w walce, ale jest też piękno poza nią.
* Arianna: Twój pomysł z ogrodem czy u was rodzinne?
* Cichoświst: To miejsce było przede mną. Ono było zaniedbane. Miało potencjał. I...
* Arianna: wziąłeś pod skrzydła i rozwinąłeś by kwiaty mogły rozkwitnąć?
* Cichoświst: Verlenowie kojarzą się z walką i głośnymi przechwałkami. Ja chcę pokazać, że możemy też działać ciszą i pięknem. To też Exemplis i Praecis. To jest zgodne z sentisiecią i sentisieć... ona Odpowiada. Ona z tym rezonuje. To miejsce takie powinno być.

Arianna ma złą (EVIL!) strategię. Dopytać go o to SKĄD POMYSŁ a jak będzie kręcił to oprowadzi po ogrodzie, miło czas, odwraca uwagę - i wydobyć informacje. A on ma słabość do dziewczyn i piękna.

Tr Z +3:

* Vr: Cichoświst nie jest osobą, która dużo mówi i opowiada o przeszłości. Ale Arianna ma swoje niewieści triki. A on nie ma odporności.
    * Cichoświst się przyznał, że jak był młody, to jego rodzice mieli jako przyjaciół Diakonów
    * Cichoświst miał... romans (tu się zaczerwienił) z trzema Diakonkami. Jednocześnie. To było... trudne.
    * Cichoświst pojechał za Diakonkami. Tam... nauczył się wielu rzeczy których się nie spodziewał (tu nie wchodził w detale)
    * One powiedziały, żę nie przyjadą do niego, bo tu jest smutno potwornie itp. Przekonały go, że piękno jest ważne.
    * On to wyniósł.
    * TO MIEJSCE jego wybrało. A on uznał, że jego obowiązkiem jest to dać sentisieci.
   
DŹWIEDŹ oprowadza Ariannę i Viorikę. "Spektakl Teatralny dla VIPów".

Teatr. Jest ładnym, funkcjonalnym budynkiem. Stoi tam historyczka Maja - dzieci zrobiły spektakl dla swojego tiena a dźwiedź wpisał Ariannę i Viorikę na listę. Dzieci zrobiły spektakl o Smoczej Jamie i Verlenach ratujących ludzi przed Ostatnim Smokiem, bardzo kwieciście. High cringe level, lots of fun.

Viorika poszła się siłować z dźwiedziem na arenie. Dźwiedź jest dobrej klasy wojownikiem - tam są wojownicy którzy ćwiczą. Dźwiedź poszedł w siłę, Viorika "mmm, ładne futerko, używasz olejków?"

Tp +3:

* V:
    * Dźwiedź DOSTAŁ, zdestabilizował się lekko: 'moje futerko lepiej wygląda niż Twoje włosy, bardziej zadbane'
    * Viorika: "bo moje włosy są świeżo po walce z aniołkiem, misiaczku"
    * Dźwiedź: "dobra, pewna Verlenka chce zobaczyć czemu mówią o mnie Ponury Bicz."
* V: 
    * Viorika krąży dookoła dźwiedzia by wyczuć styl i skąd jest
    * Ten dźwiedź jest z domeny Atraksjusza Verlena - styl Atraksjusza to jest "ból jest tylko ceną za zwycięstwo"
* V: 
    * Viorika fintami i manewrami wyczuła jego ruchy
    * Dźwiedź manipuluje odległością. Jego celem jest użyć łańcucha jako dywersji ALBO kontrolować ruchy Vioriki
    * Viorika poszła w "złagodniałeś i zrobiłeś się mięciutki jak futerko" by go sprowokować i wyciągnąć jego intencje (+1V)
* V: 
    * Dźwiedź o dziwo się trochę rozluźnił. Ostrzejszy w walce, ale nie da się tauntować.
    
Tr +4: 

* Vr: Viorika przeskakuje jak o tyczka jak on robi sweep po ziemi; ona jest na jego plecach, wybija się i robi dystans.

Viorika samą bitwą udowodniła, że jest wartościową Verlenką. Dysydenci, ludzie którym to nie pasuje - mogą do Vioriki podbić.

Chwilę powalczyli, po czym dźwiedź w końcu Viorikę złapał, ale miała bardzo duże sukcesy w samej walce.
    
A Arianna chce zobaczyć czym to miejsce się różni od Verlenlandu. Czym jest to miejsce, jak tu objawia się Praecis Exemplis:

* Każda PARA energii magicznych ma 'facet'
    * Verlenland "Marcinozaura" to jest 'facet': BOAST, EPICKOŚĆ, SYMBOLICZNOŚĆ (jestem exemplarem / symbolem)
    * Verlenland "Adelicji" idzie mocniej w kontrolę. Kontrola Która Chroni.
    * Verlenland "Cichośwista" idzie w stronę Exemplis jako WYOSTRZENIE NATURALNEGO PIĘKNA, SYMBOL a Praecis ZAPEWNIENIE TEGO

Podleczony Cichoświst i Arianna SADZĄ KWIAT.

Cichoświst "cieszę się, że zdecydowałaś się zasadzić piękny kwiat w tym miejscu". 
Arianna "najlepiej Was zrozumiem jeśli zrobię to co Wy zwykle robicie, sadzenie kwiatów będzie krokiem w dobrą stronę. Coś co nie zrobiłam nigdzie w Verlenlandzie. Tak wyobrażałam sobie Verlenland ale takiego miejsca nie było"
Cichoświst "to MY decydujemy jaki będzie Verlenland i gdzie będzie. Sentisieć odpowie na nasze żądanie. Są miejsca z takim potencjałem."
Arianna "to miejsce jest piękne ale trzeba wybierać wojny. Można się skupić na takim miejscu, ale też pomagać ludziom zagrożonym przez potwory. Nie na wszystko jest czas. Cieszę się, że są Verlenowie, którzy to robią. Mogło źle zabrzmieć, lepiej brzmiało w mojej głowie... chciałam powiedzieć że chcę to mieć u siebie, ale presja ze strony rodziny sprawia, że nie mogłam i nie było czasu. I cieszę się, że ktoś inny miał czas."
Cichoświst "Pamiętaj, że jesteś Verlenką. NIKT nie powie Ci co masz robić. TY decydujesz że ich słuchasz. Naprawdę. Rozumiem presję. Naprawdę oni Cię kontrolują, bo Ty im na to pozwalasz. I tylko w tym zakresie."
Arianna "Mogę odrzucić wpływy ale pomogę mniejszej ilości osób"
Cichoświst "Prawda, masz rację."
Arianna "Cena, którą płacę za możliwości"
Cichoświst "Zawsze jesteś tu mile widziana /z uśmiechem. Bo Verlenland jest miejscem, o który warto walczyć"
Arianna "przyjadę"

Viorika ma okazję pogawędzić z ludźmi. Oni widząc ją, jako "normalną" tienkę, nie boją się jej - i ci, którym mniej pasuje tutejszy tien będą uważali, że ona jest "bezpieczna". Viorika uważa, że to jest naprawdę fajne miejsce (choć nie dla niej), ale szuka zagrożenia dla terenu czy stwory. Polityka, ktoś-coś. Tak samo interesuje ją ogólne poczucie czy ludzie MUSZĄ tu być. Chcą czy nie?

Tp +3:

* V: 
    * zagrożeniem jest to, że to jest JEDEN tien i to introspektywny. Ludzie to widzą. Oni sobie swojego tiena chwalą, ale jest JEDEN i Sanktuarium umrze.
    * to miejsce JEST dość ascetyczne. Więc - "turyści" nie mają czego tu szukać. Ale jest za ładne i za "nie-verleńskie"
        * sentisieć to akceptuje, ale to miejsce jest na odludziu więc nic z nią nie walczy
    * przez to, że Cichoświst nie do końca jest szanowany wysoko, on nie liczy na pomoc. On próbuje wszystko wygrać samemu, bo nie ma nadziei że mu pomogą.
    * boją się, że jest sam. To jest zawsze zagrożenie.
    * dźwiedź przypałętał się, został i pilnuje tiena. "lojalny jak dźwiedź". I prostolinijnie próbuje znaleźć mu partnerkę.

## Streszczenie

Arianna i Viorika otrzymują SOS od Cichośwista, który wysunął się do Przelotyka ratować randomowych ludzi. Zespół odpiera Anielice, po czym z Cichoświstem odwiedzają jego Sanktuarium Kwiatów - unikalnego miejsca w Verlenlandzie, które pielęgnuje piękno i harmonię, kontrastując z typowym dla Verlenów skupieniem na walce i chwaleniem się. Arianna wścibsko wyciąga z Cichośwista jego przeszłość i inspirację do Sanktuarium; razem sadzą kwiat. Viorika angażuje się w sparing z Ponurym Biczem, dźwiedziem. Zauważa zagrożenie sentisieciowe i memetyczne samotnego Sanktuarium Kwiatów. W sumie - takie miejsce powinno istnieć...

## Progresja

.

## Zasługi

* Arianna Verlen: podczas ataku Anielic zorganizowała obronę z działkiem automatycznym i zauważyła iluzje i klony. Zaprzyjaźniła się z Cichoświstem, doceniając jego nietypowe na Verlena podejdzie. Razem zasadzili kwiat w Sanktuarium Kwiatów. Akceptuje Praecis+Exemplis jako doskonałość też przez piękno i nie śmieje się z tego (głośno), że Cichoświst miał edukację u Diakonek.
* Viorika Verlen: zauważając, że Cichoświst jest atakowany przez swojego człowieka go natychmiast osłoniła i zebrała rozbitków do walki z Anielicą. W Sanktuarium Kwiatów skupiła się na dźwiedziu Ponurym Biczu i pokazała swoje umiejętności. Zauważyła, że Sanktuarium ma problem polityczny i sentisieciowy - Cichoświst nie może po prostu być izolowany, bo sentisieć nie wytrzyma.
* Cichoświst Verlen: gdy randomowi ludzie z Przelotyka wysłali SOS, poleciał ich ratować i sam wysłał SOS, by dostać wsparcie. Honor i osłona nad to jak nań patrzą. Mało mówi, świetnie walczy - zwłaszcza wręcz. Zasadził z Arianną kwiat w Sanktuarium Kwiatów i pokazał Ariannie i Viorice swoje przemyślenia odnośnie piękna, gdzie jego miejsce w kontekście Praecis i Exemplis. Silnie angażuje się w życie społeczności Sanktuarium Kwiatów i jest ich tienem.
* Ponury Bicz: dźwiedź, złośliwy, wierny i oddany. Koniecznie chce wyswatać Cichośwista i mówi to wprost, jak to dźwiedź. Przy swojej grubiańskości jest niesamowicie lojalny. Kiedyś pod Atraksjuszem Verlenem. Starł się z Vioriką; dobrze walczy. Dba o Sanktuarium Kwiatów.

## Frakcje

* .

## Fakty Lokalizacji

* Sanktuarium Kwiatów: w Verlenlandzie, niedaleko Przelotyka; jest to miejsce, gdzie Exemplis materializuje się też jako piękno.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. Sanktuarium Kwiatów
                                1. Klasztor Miecza
                                1. Fortiwioska
                                    1. Szpital Opieki
                            1. Sanktuarium Kwiatów, okolice
                                1. Las Mgieł
                                1. Szczyt Strachu

## Czas

* Opóźnienie: 10
* Dni: 6

## Inne

