## Metadane

* title: "Osy w CES Purdont"
* threads: historia-eustachego, arkologia-nativis, plagi-neikatis, zbrodnie-kidirona
* gm: żółw
* players: fox, kapsel, kić

## Kontynuacja
### Kampanijna

* [220720 - Infernia taksówką dla Lycoris](220720-infernia-taksowka-dla-lycoris)

### Chronologiczna

* [220720 - Infernia taksówką dla Lycoris](220720-infernia-taksowka-dla-lycoris)

## Plan sesji
### Co się wydarzyło

* CES (Closed Ecological System https://en.wikipedia.org/wiki/Closed_ecological_system ) Purdont ma infekcję Osodrzewa Trianai
    * Mamy do czynienia z Osodrzewami, Ainshkerami, Kralvathami i potencjalnie czymś jeszcze
* Lycoris została wysłana by sprawdzić czemu mamy błędne odczyty z CES Purdont, z ich Wiertła Ecopoiesis
    * Zbliża się burza piaskowa, trzy Remory - Lycoris i dwie inne z agentami mającymi ją chronić
    * Niestety, Trianai uszkodziły komunikację CES Purdont
* Zbliża się POTĘŻNA burza piaskowa. Przestraszony Rafał Kidiron ŻĄDA uratowania Lycoris; nie wie co się stało, ale stracili kontakt z Purdont.
* Joachim zainfekował CES Purdont Trianai; miało być prosto i ludzie z łazika Karglondel mieli wejść do Purdont 

### Co się stanie

* Cel Trianai: expand, corrupt Ecopoiesis, create Araispawn
    * drolmot chce złapać i zainfekować Eustachego; użyć Darii jako pułapki, zainfekować Karglondel
* Cel Joachima: ujawnić prawdę, przetrwać. Koleś jest kompetentnym i uzbrojonym advancerem.
* Cel Karglondel: podłożyć "dowody" na Trianai, usunąć / przechwycić Joachima (Strzała)
* Cel Lycoris: contain Trianai; na przyszłość. Nie możemy zniszczyć, to jest niesamowicie wartościowe.
* Sceny:
    * s0: Eustachy i Kordian Olgator; gdzie najlepiej ufortyfikować pozycję. Gra CtF młodych ludzi (Eustachy + Ardilla + Celina vs Daria + Jan + Tymon)
    * s1: 
    * sA1: Daria potrzebuje pomocy bo umrze; ale w Laboratorium Ekopoezy są Trianai
    * sA2: Pod Generatorami Magicznymi znajdują się komputery; tam COŚ jest. 

### Sukces graczy

* Przetrwać, wycofać się BEZPIECZNIE z CES Purdont
* Uratować Darię
* Odkryć sekret CES Purdont (razemir)
* Zatrzymać / zniszczyć drolmota (kiedyś-Kamil)

## Sesja właściwa
### SPECJALNE ZASADY SESJI

Każde 4 krzyżyki i ptaszki (żetony) -> +1 do torów.
Każde 2 Akcje -> +1 do torów.

Tory:

* TOR: ROSNĄCA SIŁA TRIANAI: (1-5 (basic: osy & ainshker), 6-7 (adv: swarm of ainshker & drolmot), 8-9 (razemir), 9, 10): 0
    * 1. Osy Trianai dookoła laboratorium, Ainshkery też. Polowanie na ludzi
    * 2. 
* TOR: POSTĘP INWAZJI: (1-3 (mobility), 3-5, 6-8): 0
    * 1. Kołczan -> do AI Core, Strzała -> do magazynów (zew), Łuk -> do generatorów
    * 2. Kołczan -> downloading data, Strzała -> szabrujemy i selekcja, Łuk -> odkrycie razemira
    * 3. Kołczan -> evac, Strzała -> evac, Łuk -> EXTERMINATED
* TOR: DROLMOT DARII: (1-3, 4-6, 7): zarażona; gaśnie => z nożem w łóżku, nieaktywna => drolmot
* TOR: EWAKUACJA JOACHIMA: (1-6, 7-8): ukrywanie + manewry => evac
* TOR: KORDIAN ZBIERA LUDZI: (1-3, 4-6, 7-9): zbiera => fortyfikuje => ucieka
* TOR: DEGENERACJA CES PURDONT (1-5, 6-8, 9-12, 13): nic, breach, damage, INFERNIA!


### Scena Zero - impl

3 lata temu. CES Purdont.

Eustachy zdecydował, że najlepiej zrobić bazę tam gdzie zapasy - w jakichś magazynach. Tam gdzie jest sprzęt i części. Tymon, niestety, wybrał starport, bo to "romantyczne". Magazyny są raczej na uboczu; na wypadek rozszczelnienia mają otwarty teren. Nie trzeba ich ogrzewać. Ardilla: "mogę zostać przy fladze, tu jestem w stanie rozwiązać problem". Ardilla: "jeśli długo nie przyjdziemy, oni przyjdą do nas a oni są w otwartym terenie".

Ardilla ma podły pomysł - położyć flagę na stercie źle położonych rzeczy, żeby nie dało się tam łatwo wejść. Ona jest w stanie. Zamiast tego fikcyjna flaga.

TrZ (magazyn)+3:

* Vz: Ardilla znajduje i składa mnóstwo "fałszywych flag" i różne flagi w różnych miejscach a prawdziwą chowa. To powoduje, że zajmie 10 min+ znalezienie prawdziwej flagi.
* V: Nie trzeba zostawiać obrońcy; zbyt duży chaos. Janek może rozpoznać, ale nie w tym czasie + Tymon mu nie ufa.

Tymon nie lubi Janka, rywalizują o Darię.

Eustachy składa broń w bazie, by wyglądało na dywersję. Coś, co ma wystrzelić by oni nie weszli tylko uznali, że muszą się przebić. I z wysokiego miejsca.

TrZ (magazyn)+3:

* X: Tymon będzie bardzo urażony że oszukiwaliście. Bo nie wolno takich rzeczy robić, miała być uczciwa walka.
* V: Broń jest złożona i odwróci ich uwagę.
* X: Broń po pierwszym strzale spadnie, przestanie działać itp. Koszty.
* Vz: Broni jest kilka. Oddział napadający będzie niechętnie walczył w tym pomieszczeniu. Nie wiadomo co się dzieje i skąd.

Operacja szybki pełz - Zespół próbuje dostać się do starportu inną drogą, wolniejszą, ale bez przeciwników. Ardilla przodem. 

TrZ (bioforma)+2:

* Xz: Przeciwnik spodziewał się że Ardilla zadziała nietypowo. Janek + Daria. Janek jest na drodze i jest w skrzyni. Ardilla porusza się po wentylacjach i dziwnych terenach. Wychodzi na zewnątrz. (+2Vg)
* X: Wydostanie się na zewnątrz zajmie za dużo czasu. Przeciwnicy już są przy bazie. Ale - Janek jest na obronie z pancerzem. Dwóch pozostałych jest przy fladze ale... nie mogą tam wejść. A Wy w trójkę jesteście tutaj.

Ardilla robi dywersję Jankowi; żeby się zdawało że broni przed 3 a Eustachy i Celina lecą atakować od pleców.

TrZ (zaskoczenie, dywersja) + 3 (wrażenie otoczenia):

* XzX: usłyszeli i się spodziewali. Niestety, Janek ich ostrzegł. Daria i Tymon są na otwartej przestrzeni i strzelają do Was, ale nic to nie daje.
* V: przez otwartą przestrzeń Tymon i Daria są ustrzeleni.

Tymon zaczął "TO NIE FAIR! OSZUKUJECIE!" Daria, nic nie mówi, jest zrezygnowana. Jej niespecjalnie zależało.

Jako, że mamy 3v1 i Janek nie ma jak się wycofać, flaga została zdobyta. 

Janek z rezygnacją do Celiny "próbowałem, siorka". Celina "ale Eustachy wymyślił genialny plan". Janek "czy taki genialny... wszystko za niego zrobiły dziewczyny". Eustachy "właśnie, chodzi by wygrywać a się nie narobić." Daria "czyli pomysł ze zbroją był dobry?" Eustachy "tak". Daria -> Tymon "widzisz?" Tymon burknął.

### Scena Właściwa - impl

Kordian, widząc sytuację, spróbował ufortyfikować się i zrobić tymczasową bazę (ratując ludzi) w okolicach Medical.

Zespół jest w centrum komunikacyjnym. Wiecie, że macie od Kidirona plany jak zbudować rzeczy pomagające na ten jad os i inne patogeny tego cholerstwa. Eustachy zanomalizował system łączności ostatnio. Przez przypadek. Eustachy, widzisz na komputerze (kody wujka), że ktoś z tej bazy kontaktował się na nieznanym sygnale na zewnątrz...  

Szybko Zespół dostał się do Command Center; Eustachy wydał polecenie z CC - niech się ufortyfikują, łączność itp. Jaki jest stan sytuacji.

TpZ (autoryzacja) +2:

* V: Eustachy ma ogląd sytuacji
    * na początku miałeś koło 50 osób w Purdont
    * większość osób jest zarażonych; 35. Włączając Darię
    * kontrolujesz wizję, system itp.
    * WIDZISZ, że Kordian (trochę szeryf) w Medycznym i wysyła po 2-3 agentów w skafandrach by zbierali ludzi wyglądających na zdrowych.
    * Plaga znajduje się w okolicach systemu podtrzymywania życia
    * Dużo czujników i kamer jest zniszczona
* V: Zniszczenie nie pasuje do Plagi. Dowód istnienia sabotażysty - gdzieś w okolicach południowej części bazy.
* X: Nagranie jest nieprecyzyjne i inni nie do końca uwierzą bo to nowy TYP agenta
* V: OBLICZE DROLMOTA. Kamil jest agentem Plagi.

Eustachy decyduje się na przejęcie dowodzenia. Łączy się z Resztkami. Kordian odpowiada na pytanie Eustachego - nie zgadzam się. Eustachy ma autoryzację od samego Kidirona. Kordian akceptuje że to prawda; Eustachy "Nativis to miało, my się na tym znamy". TAK MA BYĆ I TAK BĘDZIE.

Tr Z(autoryzacja) +3 (zna się na tym):

* Vz: Kordianowi nie podoba się podejście Eustachego, ale akceptuje autoryzację. Akceptuje, że możesz to znać. Oddaje Ci dowodzenie warunkowo.
* V: Kordian oddał dowodzenie bezwarunkowo, bo Kidiron dał autoryzację.

Kordian będzie współpracował. Eustachy blokuje dostępy - część zainfekowana i czysta to korytarzyk. Plus blokowana jest czerwona śluza na zewnątrz. On ma 6 ludzi. Po jednym człowieku na przejściu, zostaje trzech. NIE! Ainshker ich rozsypie.

Eustachy wziął komunikację z Hestią; nie poradzi sobie z blokadą (bo Kamil - zainfekowany przez drolmota - jest na pełnej autoryzacji). Celina zaproponowała że w takim razie zaspawamy te drzwi które zrobimy. Kamil kazał zewnętrzne i wewnętrzne zaspawać: medical - warehouse, lab - processing, itp. Tylko koło command center. I wysłać 3 ludzi Kordiana na to.

Janek "MUSIMY ściągnąć Darię! Jak najszybciej". Celina "jak tylko zrobię antidotum!".

Hestia -> Eustachy "dowódco, zewnętrzne sensory średniego zasięgu przestały działać". Eustachy zaczął spawać zewnętrze.

Eustachy i Janek jak najszybciej składają by zdążyć zanim niebezpieczne formy. By nic nowego się tu nie pojawiło. By tylko to co jest. By nie było kłopotów - odciąć północną część bazy.

Tr (ex -> tr za zasób) +2:

* X: Sabotażysta wie i przekazał istotne informacje do Łazika. 
* V: Udało się odciąć i nic nowego / groźnego tu nie weszło. Jesteście chronieni przed nie-ainshkerami i nie-osami.
    * Chwilowo Ainshkery ładnie powiększają biomasę koło systemu podtrzymywania życia

TOR LEN: 2.

Zespół udał się do Medycznego. Można tu zbudować zarówno pułapkę z biomasą jak i zrobić antitodum jak i obie rzeczy. Hestia zaraportowała, że zewnętrzne drzwi przy magazynach zostały siłowo otwarte i kaskadowo są niszczone wszystkie sensory wizualne itp.

Eustachy chce z pomocą Hestii zdobyć informacje o napastnikach. Kto się włamał. Superpozycje itp. korzystając z anomalnego systemu łączności.

Tr (przeciwnik musi się spieszyć) Z (anomalność o której nikt nie wie) +2:

* V: Co to jest
    * Widzisz grupę ludzi w skafandrach. Opancerzonych. To kommando team.
* V: Komandosi nie są BARDZO komandosowaci; mają sprzęt, są nieźli, ale np. Janek, Ardilla czy Eustachy są lepsi. Więcej niż pięciu. Oni też otworzyli ogień do ainshkera. Ledwo sobie poradzili. Wbijają w głąb bazy.
* Vz: Dzięki anomalności sukces. Na pewno jeden oddział idzie w kierunku na zasilanie. Znają teren. Ale inny oddział idzie w kierunku na centrum dowodzenia. Nie macie DOKŁADNEJ pewności do którego idą dokładnie.
    * Każdy zespół ma 5 osób (typowy PUNKT).
    * Jeden zespół chce dostać się do Was.
* Xz: Bez kłopotu przebiją się przez zaspawane drzwi. Mają dość sprzętu.
* V: Macie komunikację, możecie ich podsłuchiwać
    * "Strzała - czy macie żywność z magazynów - pamiętajcie, konserwy, NIC żywego. Tylko zamknięte."
    * "Q: Kołczan - czy macie AI Core? A: Nie, ufortyfikowany. Q: Czy jesteście w stanie sprawić, by wyglądało to na potwory? A: Pracujemy nad tym"
    * "Q: Łuk, czy macie wejście do podziemnej bazy? A: Nie wiemy dokładnie gdzie jest. Q: Oczyścćie drogę przy magitrowni, gdzieś tam."

TOR LEN: 3 (i 1 ekstra)

Eustachy decyduje się użyć mocy magicznej. Co się dzieje. Połączyć się z anomalnym systemem i za jego pomocą wejść w komunikację tych dwóch zespołów które są w Purdont.

-> Tr Z (anomalność) M (użycie magii) +2:

* Vz: Eustachy użył magii i wszedł w przeszłą komunikację tych skafandrów.
    * Ich polecenia:
        * Kołczan ma zebrać dane z AI Core i wyłączyć AI Core. Żeby nie było śladów ich obecności. Szukają informacji o Skażonej żywności z arkologii Nativis.
        * Łuk ma znaleźć gdzieś pod magitrownią ukryte laboratorium. Dlaczego Nativis ma taki sukces z żywnością? Czy wykorzystują bezpiecznie Trianai?
        * Są z łazika Karglondel. Ten łazik jest sprzymierzony z arkologią która miała problem Trianai i on jest po żywności z Nativis.
        * Tu jest ich advancer. To on jest sabotażystą.
        * Nie mogą znaleźć Lycoris. Zniknęła.
            * Grupa "Cięciwa" wycofana; nie chcą spotkać się z Infernią. Lycoris jest poza ich zasięgiem.
        * Nie do końca zdają sobie sprawę z tego jak groźne są Trianai.
* V: Eustachy przejął kontrolę nad komunikacją i powiedział "zmiana planów, jest pod life support, musimy użyć wszystkich sił". I wszystkie 10 osób poszło polować na Trianai.
    * "Kamil" też przekierował swój cel - zamiast poszerzać teren skupia się na walce z agentami Karglondela.
* X: Zorientują się dość szybko, że coś tu jest nie tak; wycofają się dość bezpiecznie. Acz mogą mieć straty.

TOR LEN: 5.

Tymczasem Celina z Jankiem siedzą i Celina próbuje złożyć z kiepskiego systemu medycznego co się da. By zrobić antidotum. Ale Celina ma próbki ainshkera, os itp. Ma wszystko co jest potrzebne by dopasować kod przesłany przez Kidirona do tej Plagi Trianai.

Tr (ex -> tr) Z (macie lokalne próbki, macie medical i nie ma presji czasu, kody i autoryzacje i AI) +2:

* XX: ta Plaga Trianai jest szczególnie "dzika". Inny strain, to powoduje, że antidotum nie naprawi a tylko zatrzyma i utrzymać przy życiu.
* V: macie antidotum które FAKTYCZNIE zatrzyma i utrzyma przy życiu. Ale jest trudne w podaniu, powoli się syntetyzuje itp.
* Vz: da się uratować ludzi nawet Ciężko Skażonych. Ale potrzebne są pewne rzeczy z magazynów.

Ale mając dywersję da się relatywnie bezpiecznie zdobyć te rzeczy. Hestia informuje Eustachego, że pojawia się coraz więcej ainshkerów i innych niekompletnych form. "Komandosi" nie są w stanie długo się utrzymać. Szczęśliwie, dzięki temu że jest perymetr itp. da się osłonić / ochronić. Ale dobrze zrobić to teraz jak jest dywersja.

Ardilla poluje na rzeczy w magazynie. Musi znaleźć potrzebne środki i rzeczy. I je zdobyć ZANIM dywersja minie.

Tr Z (rozkładała próbki biologiczne w innych miejscach by szukały nie tam gdzie jest Ardilla) +3:

* V: Pozyskane elementy są pozyskane. Jest ich dość.
* V: Skutecznie udało się odciąć CES zanim ainshkery się uruchomią.

TOR LEN: 6 i 1.

Ardilla ma NOWY PLAN.

Hestia zamyka w kontrolowany sposób drzwi w warsztatach. Dzięki temu komandosi nie mają jak uciec. Ardilla natomiast biega korzystając z prędkości i rozmieszcza smaczki by sterować ruchem Trianai by komandosi mieli szansę przeżyć a Infernia ich uratuje. Ale nie ma czasu i to jest zbyt niebezpieczne. Mamy 3 opcje:

* Ardilla może ryzykować + zamknięcie drzwi, ale część komandosów przeżyje
* Ardilla nie ryzykuje + zamknięcie drzwi, komandosi nie przeżyją, jest czas na Infernię
* Nie zamykamy drzwi, komandosi wieją. Trianai są problemem.

Nie możemy ryzykować. Zamykamy drzwi i wyłączamy audio z warsztatów. Komandosi giną, kupując czas Trianai.

CZAS RATOWAĆ DARIĘ. Znowu Ardilla, bo jest najszybsza. Ma antidotum i wszystko. I tazery. Mieszkalne mają długie korytarze, więc użyjmy wentylacji na pełnej mocy. Lecimy SZYBKO.

Tr (bo serio presja czasu) Z (bo Ardilla zna bazę, ma uprawnienia itp) +4 +3Or:

* X: części osób nie da się już uratować i stały się nośnikami zarazy.
* Vz: Ardilla dotarła do Darii w OSTATNIM momencie. Daria jest w pokoju, leży na łóżku, jest półprzytomna, ma wykwity i pasożytnicze formy na ciele. Ma nóż w ręku i chce się bronić przed Ardillą. Ardilla po prostu wsadziła jej TURBO DAWKĘ W CIAŁO ASAP.
* VX: Daria potrzebuje ogromnej pomocy medycznej, kilka miesięcy, ale wyjdzie z tego cało, choć oszpecona itp. No ale magitech biotech itp. Da się pomóc.
    * Jankowi będzie smutno.

Ilość Trianai rośnie. Ilość komandosów maleje. Ufortyfikowaliście się. Potem drugą pozycję. Potem trzecią - by się wycofywać z wszystkimi. Wszyscy w skafandrach. I - Infernia. Infernia wkroczyła i od razu w Lancerach. Poza Emilią, ona nie ma Lancera, jest androidem. I wtedy usłyszeliście wiekopomne słowa. Emilia - Eksterminacja Militarna. 4 Lancery + Emilia rozwiązali problem Trianai - a przynajmniej pobieżnie. I Infernia szybko ewakuuje kogo się da. CES Purdont zostaje tymczasowo bez załogi ludzkiej.

Dobrze się spisaliście.

## Streszczenie

Eustachy przejął dowodzenie nad obroną CES i sealował część przejść. Postawili serię pułapek i defensyw - i wtedy poza Trianai pojawili się komandosi z zewnątrz, z łazika (crawlera) Karglondel. Czegoś szukają pod bazą. Magia Eustachego naprowadziła ich prosto na rdzeń Trianai i Ardilla wykorzystała komandosów jako dywersję dla Trianai gdy ratowała Darię przed ostateczną transformacją w istotę Trianai (antidotum złożyła Celina). Udało im się przetrwać do przybycia Inferni i uratowali wszystkich kto był ważny. Komandosi nie przeżyli ;-).

## Progresja

* .

### Frakcji

* .

## Zasługi

* Eustachy Korkoran: przejął kontrolę nad obroną CES Purdont, magią zakłócił komunikację napastników-komandosów, dowiedział się czemu atakują i kto. Utrzymał CES Purdont aż Infernia przybyła na ratunek.
* Ardilla Korkoran: wpierw z pomocą Janka zyskała kluczowe do antidotum rzeczy dla Celiny, potem porozkładała szybko próbki po bazie by Trianai vs komandosi i zrobić dywersję. I zdążyła uratować Darię i dostarczyć jej antidotum w ostatnim momencie.
* Celina Lertys: z kiepskiego systemu medycznego zsyntetyzowała antidotum mające uratować Darię przed nędznym losem. 
* Jan Lertys: skupia się na ochronie Celiny gdy ta robi antidotum, na szybkim wspieraniu Ardilli gdy ta szuka składników i na strzelaniu do Trianai by chronić Zespół. Kompetentny ale nic super.
* Kamil Wraczok: Drolmot Trianai; Przebudzony przez magię Eustachego, koordynuje Trianai do walki z komandosami i rozbudowy rdzenia. KIA.
* Joachim Puriur: próbuje ukryć swoje ślady; sabotażysta od środka który wpuścił komandosów z Karglondel i wprowadził plagę Trianai do CES. Zginął z komandosami zjedzony przez Trianai. KIA.
* Kordian Olgator: od ochrony bazy; ufortyfikował remnants koło Medical i niechętnie, ale oddał Eustachemu dowodzenie nad obroną CES (Kidiron autoryzował). Dobrze pomagał Eustachemu chronić przed Trianai.
* VN Karglondel: łazik (crawler) neikatiański; komandosi próbowali zdobyć COŚ pod CES Purdont i doprowadzili do Plagi Trianai. Zostali odparci ze strasznymi stratami.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. CES Purdont: gdzieś w środku znajduje się ukryte COŚ czego szukali komandosi z VN Karglondel i dlaczego byli skłonni sprowadzić na CES Plagę Trianai.

## Czas

* Opóźnienie: 1
* Dni: 1

## Konflikty

.

## Kto
### Arkologia Nativis

* Lycoris Kidiron, 57 lat (ze wspomaganiami drakolickimi dającymi jej efektywność 3x-latki) (objęta przez Wiki)
    * Wartości
        * TAK: Bezpieczeństwo, Osiągnięcia ("dzięki mnie ta arkologia będzie bezpieczna i osiągniemy najwyższy poziom eksportu żywności")
        * NIE: Konformizm ("nikt, kto próbował się dopasować do innych nie doprowadził do postępu")
    * Ocean: ENCAO: +-0+0 (optymistyczna, nieustraszona, lubi się dogadywać z innymi, nie patrzy na ryzyko)
    * Silniki:
        * TAK: Kapitan Ahab: polowanie na białego wieloryba do końca świata i jeszcze dalej, nieważne co przy tym stracimy. (tu: "zrobię najlepszą arkologię, lepszą dla ludzi niż planeta macierzysta")
    * Rola:
        * ekspert od bioinżynierii i terraformacji, pionier (zdolna do przeżycia w trudnym terenie)
* Rafał Kidiron
    * Wartości
        * TAK: Achievement, Power("wraz z sukcesami przychodzi władza, wraz z władzą musisz osiągać więcej sukcesów")
    * Ocean
        * ENCAO: 00+-0 (precyzyjny i dobrze wszystko ma zaplanowane, lubi się kłócić i nie zgadzać z innymi)
    * Silniki:
        * TAK: Duma i monument: to NASZE sukcesy są najlepsze i to co MY osiągnęliśmy jest najlepsze. Musimy wspierać NASZE dzieła i propagować ich chwałę.
    * Rola:
        * kuzyn Lycoris, security lord, w wiecznym konflikcie z Gurdaczem (security & money - beauty)
* Jarlow Gurdacz
    * Wartości
        * TAK: Hedonism, Face NIE: Conformism ("najpiękniejsza arkologia, klejnot Neikatis - i to nasza!")
    * Ocean
        * ENCAO: 000++ (kocha piękno i ma mnóstwo świetnych pomysłów)
    * Silniki:
        * TAK: Artystyczna dusza: Stworzyć piękno, pokazać je całemu światu lub wręcz przeciwnie, zachować dla godnej publiczności. Być docenionym.
    * Rola:
        * ekspert od roślinności, architektury i zdrowia arkologii, w wiecznym konflikcie z Rafałem Kidironem (security & money - beauty)

### CES Purdont

* Wiktor Turkalis
    * Ocean: ENCAO:  0+0-0
        * Prostacki, prymitywny, nie dba co myślą inni
        * Nieśmiały, skrępowany i często zażenowany
    * Wartości:
        * TAK: Humility, TAK: Family, TAK: Face, NIE: Achievement, NIE: Conformity
    * Silnik:
        * TAK: Do the right thing: niezależnie od tego co jest wygodne czy tanie, własne zasady i własny kompas moralny są najważniejsze. By spojrzeć w lustro.
        * TAK: Papa Smerf: zawsze doradza, daje dobre rady. Chce być pomocny i dać rady które naprawdę pomogą. Chce, by za jego radami KTOŚ poszedł i by było mu lepiej.
        * NIE: Tępić debili: nienawidzę głupoty do poziomu NUKLEARNEGO. Niech cierpią. Sama przyjemność. I zemsta.
    * Rola: przełożony 
* Kamil Wraczok
    * Ocean: ENCAO:  +0---
        * Nigdy się nie nudzi, lubi rutynę i powtarzalne rzeczy
        * Nie dba o potrzeby innych, tylko o swoje
        * Ryzykant, zawsze rzuca się w ogień
    * Wartości:
        * TAK: Self-direction, TAK: Face, TAK: Stimulation, NIE: Security, NIE: Conformity
    * Silnik:
        * TAK: Wędrowny mistrz Ryu: chcę być coraz lepszy w Czymś, chcę trudnych wyzwań, chcę się wykazać. There is always someone stronger.
        * TAK: Prowokator: podkręcanie atmosfery, by inni działali ostro pod stresem. Obserwowanie ich prawdziwej natury.
    * Rola: P.O. bazy, próbuje wszystko ukryć i naprawić sprawę samemu.
* Daria Triazis
    * Ocean: ENCAO:  -0+00
        * Polega tylko na sobie; nie wierzy w innych
        * Szczególnie rozsądny i poczytalny
    * Wartości:
        * TAK: Conformity, TAK: Hedonism, TAK: Self-direction, NIE: Universalism, NIE: Humility
    * Silnik:
        * TAK: Odnaleźć dziecko: znaleźć i uratować zaginioną osobę, która jest poza zasięgiem.
        * TAK: Wyplątać się z demonicznych długów: muszę jeść by przeżyć i wpakowałem się w ostre kłopoty z siłami, które się nie patyczkują. Spłacić lub uciec.
    * Rola: pracuje jako inżynier terraformacji; atm zarażona. Lertys jest w niej zakochany.
* Joachim Puriur
    * Ocean: ENCAO:  0--+-
        * Poważny, unika żartów
        * Jeżeli czegoś chce, weźmie to dla siebie; zazdrosny i zawistny
        * Szlachetny, skupia się na czynieniu dobra
    * Wartości:
        * TAK: Security, TAK: Tradition, TAK: Face, NIE: Humility, NIE: Hedonism
    * Silnik:
        * TAK: Utopia Star Trek: znajdowanie porozumienia i dobra nawet w osobach bardzo różnych od nas. Współpraca i koegzystencja.
* Kordian Olgator
    * Ocean: ENCAO:  --+00
        * Polega tylko na sobie; nie wierzy w innych
        * Stabilny, opoka niemożliwa do ruszenia, niemożliwy do wyprowadzenia z równowagi
        * Przewidujący, trudny do zaskoczenia - na wszystko ma plan awaryjny
    * Wartości:
        * TAK: Power, TAK: Universalism, TAK: Stimulation, NIE: Conformity, NIE: Family
    * Silnik:
        * TAK: Preserver: "this planet is dying..." - TO jest ważne i piękne i niewidoczne! Muszę mieć pomoc byśmy wszyscy TEGO nie stracili! GM w umierającym MMO.
        * TAK: Red Queen Race: jeśli stoisz w miejscu,

### Infernia

* Bartłomiej Korkoran, wuj i twarda łapka rządząca Infernią.
    * Ocean: ENCAO:  +-000
        * Bezkompromisowy, nieustępliwy, niemożliwy do zatrzymania
        * Ciekawski, wszystko chce wiedzieć i wszędzie wsadzić nos
    * Wartości:
        * TAK: Family, TAK: Self-direction, TAK: Achievement, NIE: Tradition, NIE: Humility
    * Silnik:
        * TAK: Inkwizytor: niszczenie fake news, publiczne pokazywanie hipokryzji, ujawnianie bolesnych prawd
        * TAK: Kolekcjoner: kolekcjonuje coś rzadkiego ()
    * Rola: wuj, kapitan Inferni
* Celina Lertys, podkochująca się w Eustachym 
    * Ocean: ENCAO:  --+-0
        * Stanowczy i silny, zdecydowany
        * Skryty; zachowujący swoją prywatność dla siebie
        * Nigdy nie wiadomo, co jest prawdą a co maską
    * Wartości:
        * TAK: Universalism, TAK: Tradition, TAK: Security, NIE: Power, NIE: Humility
    * Silnik:
        * TAK: Harmonia naturalna: balans między technologią i naturą, utrzymanie cudów natury. Lasy Amazonii.
        * NIE: Awans za wszelką cenę: albo lepsza firma albo wojna o stanowisko z innymi. To stanowisko będzie moje.
    * Rola: science / bio officer + medical. Młoda podkochująca się w Eustachym :D. Złota, płonąca skóra z błękitnymi włosami, może robić wyładowania
* Jan Lertys
    * Ocean: ENCAO: -0-0+
        * Zapominalski; non stop trzeba coś przypomnieć
        * Obserwator; raczej stoi z boku i patrzy niż działa bezpośrednio
        * Łatwo wywrzeć na tej osobie wrażenie
    * Wartości:
        * TAK: Humility, TAK: Universalism, TAK: Self-direction, NIE: Achievement, NIE: Security
    * Silnik:
        * TAK: Pokój: Kapuleti i Monteki MUSZĄ żyć w pokoju. Unikajmy starć, idźmy w pacyfizm. Wygaszanie konfliktów, harmonia.
        * TAK: Ratunek przed chorą miłością: TYLKO NIE Celina -> Eustachy. Pls.
    * Rola: First Mate (siłowy / ładownia); chce zejść do bazy bo tu pracuje jego ukochana Daria
* Emilia d'Infernia
    * "żona" Bartłomieja. Żywy arsenał. Egzekutor Bartłomieja. (Emilia -> "Egzekucja MILItarnA")

## Układ arkologii Nativis

* Aspekty:
    * Bardzo dużo roślin, drzew itp. Piękne roślinne miejsce.
    * Bardzo spokojna arkologia, silnie stechnicyzowana i sprawna.
    * Dominanta: faeril > drakolici, ale próba balansowania. Niskie stężenie AI.
    * BIA jako centralna jednostka dowodząca, imieniem "Prometeus"
* Struktura:
    * Wielkość: 4.74 km kwadratowych, trzy poziomy
    * Populacja: 11900 (Stroszek)
        * Operations & Administration: 1100
        * Extraction & Manufacturing: 3213
        * Medical: 355
        * Science: 360
        * Maintenance: 700
        * Leisure: 1000
        * Social Services: 240
        * Transportation: 800
        * Training: 229
        * Life Support: 500
        * Computer & TAI & Communications: 500
        * Engineering: 500
        * Supply / Food: 1200
        * Security: 300
    * Bogactwo: wysokie. Technologia + piękna roślinność.
    * Eksport: żywność (przede wszystkim), komponenty konstrukcyjne, hightech, terraformacja
    * Import: luxuries, prime materials, water, terraforming devices, weapons
    * Manufacturing: jedzenie, konstrukcja, terraformacja
    * Kultura: 
        * dominanta: piękno, duma z tego co osiągnęli, duma z tego że nie ma wojny domowej faeril/drakolici
        * wyjątek: brak.
    * Przestrzenie
        * rozrywki: parki, natura, zdrowe ciało - zdrowy duch
        * inne: hotele, laboratoria terraformacyjne, restauracje...
    * Zasilanie
        * solar + microfusion
    * Żywność
        * różnorodna; to miejsce jest źródłem żywności
        * przede wszystkim roślinna, ale też zawiera elementy zwierzęce (te najczęściej z importu)
    * Life support => terraformacja + filtracja
    * Comms => biomechaniczne okablowanie + normalne sieci
    * TAI => BIA wysokiego stopnia imieniem Prometeus
    * Starport => large
    * Infrastructure => high-tech, dużo półautonomicznych TAI, wszystko nadrzędne przez Prometeusa. Świetny transport publiczny. 
    * Access & Security => arkologia z silnymi blast walls, silnymi liniami dostępowymi.

Historia: 

* w trakcie wojny domowej sprzedawała żywność na 2 fronty co spowodowało znaczne bogactwo ale też częściową niechęć innych arkologii
* obecnie prowadzi ekspansje na struktury zrujnowane/opuszczone po wojnie żeby całkowicie się uniezależnić od dostaw z poza planety
