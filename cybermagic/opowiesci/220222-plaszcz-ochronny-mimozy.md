## Metadane

* title: "Płaszcz ochronny Mimozy"
* threads: rekiny-a-akademia
* gm: żółw
* players: kić, anadia

## Kontynuacja
### Kampanijna

* [220111 - Marysiowa Hestia Rekinów](220111-marysiowa-hestia-rekinow)

### Chronologiczna

* [220111 - Marysiowa Hestia Rekinów](220111-marysiowa-hestia-rekinow)

## Plan sesji

### Theme & Vision

.
    
### Ważne postacie + agendy

.

### Co się wydarzyło

* Keira, agentka Ernesta, przechwyciła z pustego Apartamentu grupę kultystek Ośmiornicy. A przynajmniej tak to wygląda.
    * Keira + Ernest mają przerażającą opinię wśród Rekinów (Keira is a menace); nikomu to nie przeszkadza.
    * Wszystkie osoby przechwycił Ernest z Apartamentu, m.in. Melissę - "należącą" do Chevaleresse.
* W sprawę zamieszana jest najbardziej "samotna" elegancka dama Diakonów, Mimoza Elegancja Diakon, która nie ma NIC wspólnego z Karoliną.
    * Mimoza miała jakieś działania związane z kultystkami, a przynajmniej na to wszystko wskazuje. Typowe na Diakonkę, nietypowe na Mimozę.
* TAI Hestia jest Skażona ixionem - i jest sojuszniczką Marysi
    * Przeniesienie ixiońskie Marysia -> Hestia (przez kontakt z Marysią Hestia staje się bardziej jak Marysia).
    * Hestia jest aktywną sojuszniczką Marysi i chce, by Ernest x Marysia a nie Ernest x Amelia :3
* Sowińscy są niezadowoleni, uważają to za błąd - wyślą kuzyna Jeremiego by pomógł Marysi. ALE jeszcze się nie stałe
* Rekiny się dowiadują, że Marysia stoi za "państwem policyjnym" w Dzielnicy Rekinów
* Amelia Sowińska odzyskała częściowo kontakt z Aurum i Rekinami. Przez to, że chcieli się skonsultować z nią wrt Hestii itp. Przez Hestię.
    * To też jest forma ixiońska, o czym nikt nie ma pojęcia. Amelia nie jest ekspertem, nikt tu nie jest więc "ok, udało się połączyć". Nie wiedzą jak.
        * Czyli jeśli Amelia wejdzie w esuriit w jakimkolwiek stopniu, straci kontakt z Hestią / przez Hestię.0
* Pomniejsze waśnie Marysiowe
    * Liliana Bankierz (po stronie AMZ) nie może darować Marysi "zdrady". Ani Karolinie. Ani nikomu.
    * DJ Babu (po stronie Rekinów) nie może darować Marysi "zdrady" Stasia Arienika i oddania go "tyranizującej mamie".
        * I tego że robi z Rekinów państwo policyjne.
    * Santino Mysiokornik słusznie obawia się oskarżenia o nadmierne zainteresowanie młodymi damami (zwłaszcza Chevaleresse) i chwilowo unika Marysi.

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

2 dni później. Ernest jeszcze "procesuje" przechwycone osoby w swoim apartamencie. Wysłał Keirę jako jednostkę szturmową by znalazła innych członków Kultu. I Keira się odbiła. Nie była w stanie spenetrować jednego z Apartamentów. Apartamentu Mimozy. Mimoza wyjęła jakąś grupę szturmowych stworów, potworów Diakońskich, roślin, bioterrorów. Keira nie chciała ryzykować. I dlatego poprosił do siebie Marysię Ernest.

Ernest poprosił Marysię, by ta WPŁYNĘŁA JAKOŚ na Mimozę. Mimoza na pewno przechwytuje i ma tam jakieś kultystki. Wie to od przesłuchiwanych kultystek które już ma. 2 stąd były wcześniej u Mimozy. Ale wybrały opiekę Santino a nie Mimozę. Ernestowi bardzo zależy "zrób to proszę bo będę musiał zrobić szturm..." - wyraźnie jest zdestabilizowany.

Ernest przekazał Marysi dane od przesłuchanych Kultystek Ośmiornicy. Marysia przetwarza dane. Z Hestią. Nie sama. I nie Torszeckim.

Tr (Dane od kultystek) Z (Hestia świetnie przetwarza) +3:

* X: Lorena przeszła spod opieki Ernesta pod opiekę Mimozy. Ale kultystki ją tam widziały.
* V: Mimoza najpewniej nie jest powiązana z kultem kralotycznym. Nic na to nie wskazuje. To te laski tutaj pod Santino są. U Mimozy chyba nie. CHYBA. Ernest nadal nie wierzy.
* V: Mimoza ma w Apartamencie sporo osób. Przechowuje tam jakieś kilkanaście osób w przestrzeni dla służby. A te dwie kultystki "od Mimozy" są odratowane z ulic Podwiertu.
* Vz: Mimoza ma BARDZO DUŻE opory przeciwko wszystkiemu powiązanemu z Esuriit. Mimoza jest słabą czarodziejką, ale ma świetne zaplecze biotechniczne i jest prawdziwie dobrą arystokratką. 

Marysia przedstawia wszystkie informacje Ernestowi. Chce, by ten się uspokoił. Niech nie robi wjazdu na chatę. Ernest niechętny... ale ok, Marysia sobie poradzi. Ernest uznał za PERSONALNĄ obrazę i PERSONALNY afront to, że Mimoza zwinęła mu Lorenę. I Ernest nie może dostać swojego aktu. To jest "wypowiedzenie wojny". Dlatego Ernest jest tak nastawiony ostro na Mimozę.

Marysia => Mimoza, że chciałaby się spotkać i porozmawiać. Mimoza zaprosiła ją do swojego Apartamentu. Oczywiście, wszystko poszło kanałami formalnymi. Keira zaznaczyła z niezadowoleniem, że Apartament Mimozy ma własne źródło zasilania. I w ogóle. Keira chce towarzyszyć Marysi dla ochrony Marysi. Mimoza oczywiście się nie zgodzi. Ernest nalega. Więc, jako kompromis, z Marysią idzie Karolina. Ernest jest spokojniejszy. Keira ma dalej obiekcje, ale trudno.

Spotkanie na żywo z Mimozą. Marysia ubrała się zgodnie ze stylem. Karolina założyła lepszy pancerz. Taki z mniej wytartym wzorkiem. Bo Marysia powiedziała, by ta się dobrze ubrała. 

Apartament Mimozy jest... żywą twierdzą. Kojarzy się Marysi ze spotkaniem z Satarailem. Wszędzie piękne kwiaty, ale toksyczne. Co rzuciło się w oczy Marysi - Mimoza zatrudnia miejscowych. Miejscowi w liberii (acz widać, że nie noszą jej na co dzień) zaprowadzili Marysię i Karo do Mimozy mimo kwiatów. Mimoza jest przepiękna; 33 lata (?). Ukłoniła się wdzięcznie. 100% zazdrości. Mimoza porusza się perfekcyjnie.

A na ścianie - wielkie, efektowne lustro. 

Czerwony alarm dla każdego maga. Mimoza 100% elegancka, podała herbatkę i w ogóle. Jak może służyć.

Marysia poruszyła temat Loreny Gwozdnik. Podobno się boi Marysi. Marysia chciałaby naprawić tą sprawę - nie ma nic do Loreny. Marysia chciałaby zamówić u Loreny nowe dzieło, nowe zlecenie. Ale jak Lorena się jej boi jest to niewykonalne. A jest artystką wybitną, można to rozwinąć w dalszą karierę. Czy Mimoza może jej pomóc?

Karolina zorientowała się w dwóch rzeczach. Pierwsza - to lustro nie jest lustrem. To holoprojekcja lustra. Czyli Mimoza nie jest aż tak głupia. Nie jest całkowicie bezpieczne, ale zawsze coś. Po drugie, jak to nie jest lustro, co coś ukrywa. Więc Karolina ma tam swoją uwagę. I wyczuwa obecność innej czarodziejki. Za lustrem. Karo jest na 90% pewna, że jest tam Lorena.

Mimoza się elegancko uśmiechnęła do Marysi. Lorena pochodzi z militarnego rodu - nie jest artystką, nie ma talentu i tworzy dzieł sztuki. Marysia - każdy ma ukryte hobby. Może Lorena maluje pod pseudonimem? A nawet jak oficjalnie pochodzi z militarnego rodu i oficjalnie nie tworzy dzieł to są artyści pod pseudonimem. A sztuka jest potencjalnie świetnym źródłem utrzymania. Oczywiście, NIKT INNY NIE BĘDZIE WIEDZIAŁ. Mimoza na to, że ona jest koneserem piękna i sztuki. Może ktoś się maskuje jako Lorena? Może to nie ona? Może... (tu lekki uśmiech Mimozy) to ja? (tu "nieeee" Karoliny).

Karo ma to gdzieś. Podchodzi do lustra by wyjąć Lorenę z niego. Mimoza reaguje.

Ex (systemy defensywne Mimozy) Z (bo Karo ma gdzieś że oberwie) +3:

* V: Karo uniknęła systemu defensywnego. Roślin, pnączy, śluzu, wsadziła rękę w lustro i wyjęła piszczącą Lorenę. "NIEEEE!!! ze szlochem"

Mimoza zasłoniła Lorenę swoim ciałem. "Tien Terienak, proszę WYJŚĆ". "Strzeliła we mnie z wyrzutni rakiet. I bardzo nie lubię, jak ktoś gra w głupie gierki."

* Marysia: "Tien Diakon, nie spodziewałam się po Tobie tak niegodnego zachowania. Podsłuchiwana przez Lorenę. Zawsze miałam dobre zdanie o Tobie w świecie tej polityki. Muszę je zrewidować. Specjalnie nie przyszłam z nikim kto by Ci zagrażał."
* Mimoza: "Tien Sowińska, ubolewam nad zastałą sytuacją (z lekką irytacją (nie wiedziała o Lorenie)), ale nie pozwolę, by ktoś pod moją ochroną ucierpiał."
* Karolina: "No nie biję jej..." (dyskretnie, z lekkim fochem)
* Mimoza: "Godność arystokratki jest ważniejsza niż jej forma fizyczna." - i ją zasłania.
* Marysia, kuca by zrównać poziom z Loreną i by uspokoić ją jak dziecko i mówi powoli jak do dziecka, kojąco: "Loreno, kwestia między Tobą i Karoliną, rozwiązałyście ją. Ona Cię nie skrzywdzi jeśli nie będziesz jej prowokować." //komentarz Gracza i MG: Marysi idzie to lepiej.
* Mimoza: "Tien Sowińska! Moment. Odrobina godności. Wrócimy jak moja niefortunna towarzyszka będzie w stanie godnie się zaprezentować."
* Marysia: "Oczywiście, liczę, że wrócicie obie". (Mimoza przełknęła zniewagę.)

Dwie godziny później, wróciły. 

W tym czasie Marysię i Karolinę zabawiał trefniś i jacyś klauni. Wyraźnie lokalsi. I lokalne dzieci - jakieś "domowe przedszkole" z nauczycielką z wielkimi oczami "WOW ARYSTOKRACJA AURUM". Karo się świetnie bawi, Marysia chce się powiesić.

Lorena jest elegancko ubrana, w jakiś mundur elegancki. Oficer działań dywersyjnych zjednoczonych sił Aurum. Opinia Karo o Zjednoczonych Siłach Aurum właśnie spadła do zera. Mimoza "zresetowała" spotkanie. Karo ma to gdzieś.

* Marysia: "Tien Gwozdnik, zauważyłam, że mnie unikasz."
* Lorena: "Proszę o wybaczenie, tien Sowińska. Obowiązki dywersyjne zmuszają mnie do znajdowania się gdzieś indziej. Służba wojskowa." - westchnięcie. Lekkie spojrzenie na Mimozę. Karo nie zauważyła. Marysia - oczywiście.
* Karo: "Jasne. Wojsko.", pogardliwie
* Mimoza patrzy z dezaprobatą na Karo. Karo ma to gdzieś.
* Marysia: "Oprócz wojska masz też hobby, niekoniecznie się nim chwalisz. Bardzo wartościowe estetycznie. Pokarm dla duszy." (Karo z niesmakiem patrzy na Marysię) "Można pod pseudonimem propagować sztukę w szerszym zakresie. I NIE ZABIJĘ CIĘ ZA TEN AKT!!! I sugeruję Lorenie że ma zrobić kolejny akt dla Ernesta i ma się nie bać tego aktu zrobić, jest spoko." //znowu, ma być dobrze a nie to jest to jest napisane.
* Lorena: "Tien Sowińska, obawiam się, że musiałaś mnie z kimś pomylić. Nie jestem artystką. Nie potrafię malować ani rysować ani tworzyć dzieł sztuki. Jestem tylko prostym oficerem Aurum, specjalizuję się w miragentach. Sztuka jest czymś odległym od moich horyzontów."
* Marysia: "Miragent też może być dziełem sztuki..." (straszna myśl: czy Lorena zrobiła miragenta w kształcie Marysi? XD)

Marysia chce przekonać Lorenę - niech ta narysuje ten akt. Może udawać, że to nie ona. Ale bez aktu nie będzie romansu. No cholera.

Tr+3+3Vg (ochrona Mimozy):

* V: Lorena jest skłonna "doprowadzić do powstania dzieła, oczywiście, nie maluje sama, bo nie umie"
* X: Mimoza i Lorena nalegają, by utwór powstał w tym apartamencie.
* X: Mimoza jest skłonna udostępnić apartament do tego zacnego celu, ALE żąda wydania dwóch ofiar Ernesta jej.

.

* Marysia: "Ale wiesz, że są w Kulcie Ośmiornicy?"
* Mimoza: "Nie wykluczam. Poproszę kuzyna Sensacjusza. Wszystko lepsze niż by odratowane przeze mnie młode damy były w szponach siepacza z Eterni."
* Karo chce coś powiedzieć. Marysia chce przerwać ale jest ZBYT WOLNA. Karo: "Taki z niego siepacz jak z Ciebie dziewica".
* Mimoza uniosła jedną brew. Lorena zbladła. Marysia udaje, że Karo nic nie powiedziała. Mimoza odezwała się do Marysi: "Widzę, że nie tylko Ty, tien Sowińska, masz powody do rozczarowania."
* Marysia: "czasem życie jest brutalne i nie wszystko jest pod naszą kontrolą" // elokwentnie
* Marysia: "Ernest nie odbił tych ludzi by się nad nimi znęcać a dlatego że to tajna operacja mająca usunąć Kult Ośmiornicy i uratować ludzi z Kultu. Potem przekażemy je Sensacjuszowi do wyleczenia. Ale wpierw dowiedzieć się czy komuś nie trzeba pomóc."
* Mimoza: "Roland Sowiński, Amelia Sowińska i nawet Justynian Diakon rozwiązaliby ten problem bez wykorzystywania wpływów Eterni. Interesuje mnie, dlaczego eternianka próbowała włamać się do MOJEGO apartamentu."
* Marysia: "Obawiałam się, że jesteś zakażona kralothem i chciałam chronić Twoją godność. Dlatego Ernest to zrobił." (V) -> Mimoza potraktowała to jako wystarczająco dobrą prawdę by nie drążyć.

.

* Vg: Mimoza chce te dwie Skażone dziewczyny z powrotem, bo nie chce, by ktoś (domyślnie: Ernest) uszkodził im pamięć, chce je przekazać do Sensacjusza w SWOIM apartamencie i chce się upewnić, że NAPRAWDĘ tu był Kult Ośmiornicy. Marysia TUPNĘŁA NÓŻKĄ (metaforycznie). Odda je Sensacjuszowi. A Mimoza może być przy przekazaniu albo przyjść. Ale Skażone dziewczyny nie trafią do apartamentu Mimozy. Kropka. Bo a nuż coś tu się stanie. O. A Sensacjusz nie będzie glizdy wprowadzał tutaj. O. Mimoza się WYCOFAŁA ze swojego żądania.

Marysi udało się zapewnić swój akt oraz zapewnić, że Mimoza zaakceptuje przekazanie Sensacjuszowi tych dziewczyn. Mimoza jednak naciska na Marysię w innej kwestii.

* Mimoza: Czy otrzymam oficjalne przeprosiny od tien Namertela w kwestii próby włamania?
* Marysia: Tien Diakon, odpowiedź jest przecież oczywista. (tak, to koniec odpowiedzi)
* Mimoza: Rozumiem. (nie eskaluje)

Jakimś cudem jakkolwiek spotkanie zajęło Marysi cały dzień, ale udało się deeskalować problem. Oczywiście, potem Marysia dostała wiadomość od Triany, ale to był zupełnie inny temat...

Sprawdźmy tylko Ernesta:

* Marysia: Erneeest, przeprosisz Mimozę?
* Ernest: Że niby za co?
* Marysia: Wiesz od czego pochodzi to imię?
* Ernest: ...nie wycofa się bez tego? Słyszałem, że Diakonki się nadają tylko do... wycofywania (skończył nieco kulawo)
* Marysia: Będzie bardziej _uległa_ jak się ją przeprosi (lekko flirtując, że niby przez przypadek)
* Ernest: Dobrze. Przeproszę. Jeśli Ci to pomoże...
* Marysia kładzie mu rękę na ramieniu "Dziękuję, wiedziałam, że mogę na Ciebie liczyć."
* Ernest: zdecydowanie się mu poprawił humor.

I wtedy Marysi zepsuł się humor. Od Hestii dostała sygnał, że nawiązała kontakt z Amelią. Ale - to też za tydzień ;-).

## Streszczenie

Mimoza weszła do akcji - wzięła Lorenę do azylu i ochroniła część kultystek przed Ernestem. Konflikt Ernest - Mimoza się rozpalił, Marysia + Karo poszły się z Mimozą spotkać by zdeeskalować problem. Lorena podsłuchiwała, więc Mimoza straciła twarz. Cóż. Mimoza wynegocjowała przekazanie kultystek Sensacjuszowi, Marysia wynegocjowała by Lorena zrobiła ten akt. ALE W DOMU MIMOZY. Zaczyna się ciekawie...

## Progresja

* Mimoza Elegancja Diakon: jej apartament ma zakochanych ludzi, rośliny i bioterror defensywny. Trudna do infiltracji.
* Mimoza Elegancja Diakon: ma w Apartamencie sporo osób. Przechowuje tam jakieś kilkanaście osób w przestrzeni dla służby. Odratowane z ulic Podwiertu.
* Mimoza Elegancja Diakon: opinia "słaba czarodziejka, ale ma świetne zaplecze biotechniczne i jest prawdziwie dobrą arystokratką". Silnie związana z Podwiertem, najbardziej lubiana arystokratka Aurum.
* Ernest Namertel: uznał za PERSONALNĄ obrazę i PERSONALNY afront to, że Mimoza zwinęła mu Lorenę. I Ernest nie może dostać swojego aktu. STARCIE z Mimozą Diakon.
* Lorena Gwozdnik: STARCIE z Ernestem Namertelem. Ona uważa go za siepacza z Eterni. Żąda wydania dwóch kultystek Sensacjuszowi.
* Lorena Gwozdnik: okazuje się, że jest oficerem działań dywersyjnych zjednoczonych sił Aurum (co bardzo obniża opinię o tych siłach wszystkich co znają Lorenę)

### Frakcji

* .

## Zasługi

* Marysia Sowińska: by doprowadzić do stabilizacji Ernesta, chce zapewnić by Lorena narysowała ten obiecany akt XD. Deeskaluje konflikt Mimoza - Ernest (tymczasowo), ale stabilizuje relację swoją z Mimozą.
* Karolina Terienak: brutalnie wyciąga kiepsko ukrytą Lorenę i pokazuje ją grupie, wzmacniając ruchy Marysi. Przeraża Lorenę samą swoją obecnością.
* Ernest Namertel: chce ochronić i wyczyścić wszystkich z kralotyzacji; przejmuje kontrolę nad terenem Rekinów. Mimoza staje mu na drodze. Ścierają się w nim impulsy: Esuriit - dobro.
* Mimoza Elegancja Diakon: 33 lata, perfekcyjna i piękna arystokratka z gracją i delikatnością. Dała azyl Lorenie, która go nadużyła. Chroni Lorenę własnym ciałem, nieustraszona. Przez Lorenę straciła trochę honoru wobec Marysi. Robi dobrą minę do złej gry i próbuje zresetować spotkanie i relacje. Bardzo silnie zaangażowana w sprawy lokalne Podwiertu. Chroni Lorenę i sprawia, by informacja że to akt zrobiony przez Lorenę nie wyszedł na jaw. Żąda wydania kultystek jej od "siepacza z Eterni" (Ernesta).
* Lorena Gwozdnik: podsłuchiwała rozmowę Mimozy - Marysi (Mimoza o tym nie wie, więc sprawiła, że Mimoza straciła twarz). Robi co może by wyprzeć się że to ona narysowała akt.
* Keira Amarco d'Namertel: jednostka szturmowa Ernesta, odbiła się od apartamentu Mimozy. Nie umie jej zinfiltrować. 

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Akademia Magii, kampus
                                    1. Arena Treningowa
                                    1. Akademik
                                    1. Las Trzęsawny
                            1. Podwiert
                                1. Dzielnica Luksusu Rekinów
                                    1. Fortyfikacje Rolanda
                                    1. Serce Luksusu
                                        1. Lecznica Rannej Rybki
                                        1. Apartamentowce Elity
                                        1. Fontanna Królewska
                                        1. Kawiarenka Relaks
                                        1. Arena Amelii
                                    1. Obrzeża Biedy
                                        1. Hotel Milord
                                        1. Stadion Lotników
                                        1. Domy Ubóstwa
                                        1. Stajnia Rumaków
                                    1. Sektor Brudu i Nudy
                                        1. Komputerownia
                                        1. Skrytki Czereśniaka
                                        1. Magitrownia Pogardy
                                        1. Konwerter Magielektryczny

## Czas

* Opóźnienie: 2
* Dni: 4

## Konflikty

* 1 - Ernest przekazał Marysi dane od przesłuchanych Kultystek Ośmiornicy. Marysia przetwarza dane. Z Hestią. Nie sama. I nie Torszeckim.
    * Tr (Dane od kultystek) Z (Hestia świetnie przetwarza) +3:
    * XVVVz: Lorena pod opieką Mimozy która najpewniej niepowiązana z kultem kralotycznym i Mimoza tam chroni sporo osób. Mimoza ma BARDZO duże opory przeciw Esuriit i Esuriit-adjacent.
* 2 - Karo ma to gdzieś. Podchodzi do lustra by wyjąć Lorenę z niego. Mimoza reaguje.
    * Ex (systemy defensywne Mimozy) Z (bo Karo ma gdzieś że oberwie) +3:
    * V: Karo uniknęła systemu defensywnego. Roślin, pnączy, śluzu, wsadziła rękę w lustro i wyjęła piszczącą Lorenę. "NIEEEE!!! ze szlochem"
* 3 - Marysia chce przekonać Lorenę - niech ta narysuje ten akt. Może udawać, że to nie ona. Ale bez aktu nie będzie romansu. No cholera.
    * Tr+3+3Vg (ochrona Mimozy)
    * VXXVg: Lorena "doprowadzi do powstania dzieła", Mimoza i Lorena nalegają - w tym apartamencie ALE żąda wydania dwóch ofiar Ernesta dla niej. Skażone dziewczyny nie trafią jednak do apartamentu Mimozy a do Sensacjusza.
