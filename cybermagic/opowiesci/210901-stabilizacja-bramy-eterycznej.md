## Metadane

* title: "Stabilizacja Bramy Eterycznej"
* threads: legenda-arianny, niestabilna-brama
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [210825 - Uszkodzona Brama Eteryczna](210825-uszkodzona-brama-eteryczna)

### Chronologiczna

* [210825 - Uszkodzona Brama Eteryczna](210825-uszkodzona-brama-eteryczna)

### Plan sesji
#### Co się wydarzyło

.

#### Sukces graczy

* 

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Mostek Inferni, Medea.

Fachowcy podreperowywali Infernię, by jakoś działała i latała. Nie ma pełnej mocy... 70% mocy? NIE PRZESADZAJCIE! Na taśmie klejącej. Nocnej Krypty nie ma w okolicy. Cel - podreperować Bramę. Medea przekazała Wam brakującą pintkę.

Mamy trzy jednostki mające naprawiać Bramę. Jedną z nich jest Infernia. Ale Infernia jest najciężej uzbrojona i była przygotowana by działać w warunkach niezależnych. Tamte jednostki zwykle mają eskortę. Infernia pełni tą rolę.

Medea ściąga jako czwartą jednostkę Netrahinę. Ma pomóc. Ale Arianna i Klaudia _wiedzą_, jaka jest konsekwencja Netrahiny z odblokowanym AI...

Arianna - Medea z pewnością NIE CHCE narażać Netrahiny na tą misję. Niedawno oberwała i może być słabym punktem. Lepiej niech jest w odwodach.

Tr+3:

* X: Arianna martwiła się, by ta jednostka PRZY NIEJ znowu nie została uszkodzona - dlatego.
* V: Netrahina zostanie w odwodach

Klaudia wykorzystuje siły specjalne. Tu się trochę przesunie, tu zadziała - i mamy możliwość poprzesuwania biurokratycznego + wsparcie siły Medei. Chodzi o to by jak najwięcej zasobów jak najszybciej tu się dostało.

ExZ+3:

* X: Klaudia współpracuje z siłami specjalnymi. Ok, może tylko teraz - ale niektóre jej kontakty to zapamiętają jako niebezpieczne.
* V: Eustachy będzie miał więcej zasobów niż myśli. Toteż: pozostałe jednostki.
* V: Klaudia dała do zrozumienia że współpracuje z siłami specjalnymi. Na K1 takie "zrozumienie" daje jej więcej możliwości. "Nie ma żadnej mafii".

Eustachy ma plan - pintki jako "jednostki szybkiego reagowania" gdzie potrzebny kopniak a statki jako "dowodzące obszarami naprawczymi". Na szybko trzeba ufortyfikować pintki Inferni - nie ma super dużo zasobów, ale są zasoby Inferni + te co dotarły. Plus masz dostęp do ekspertów od ekranowania. No i Janusa.

ExZ+4:

* X: zasoby zapasowe od Medei i Klaudii szybko się wyczerpują. Macie porcję na naprawę, ale wiele więcej się nie da zrobić.
* V: pintki są ufortyfikowane. Mogą działać niezależnie. Plan Eustachego może działać.
* V: pintki są dodatkowo maskowane. Nie trzeba ich bronić - efemerydy i dziwne byty celują w duże jednostki.
* V: dopasowanie pintek tak, by w ostateczności mogły pełnić rolę tymczasowego Węzła Bramy.

Grupa inżynieryjna dzielnie pracuje. 

OO Kanagar, OO Trasman - duże jednostki. Infernia bierze 2 pintki, pozostałe jednostki - 3 pintki. Czyli mamy w sumie pintkę na węzeł.

Medea, jak to Medea, coś wie i nie powiedziała. Ale Gilbert na pewno wie to samo. I można użyć Flawii. Która ma włosy do pasa i mniej "mundur", bardziej "mundurek". Eustachy zauważył to od razu.

Lecimy w strefę niebezpieczną. W miarę bezpieczny obszar - więc efemerydy nie zwracają uwagi. Część wraków została reanimowana. Czyli nie są efemeryczne tylko i wyłącznie. Mamy do czynienia z echami wraków. Dwa niszczyciele i krążownik?

Janus i Klaudia próbują dojść do tego co jest innego i nie tak. Lecą na pintce z Janusem - tak jest bezpieczniej.

TrZ+4+3O:

* V: Doszli do tego - coś przeszło w tą stronę. Mamy jakąś jednostkę która przeszła. Ale to nie może być jednostka z ludźmi, bo są martwi. Klaudia nie "znajduje" tej jednostki na skanerach.

Arianna wie jak się poruszały efemerydy - i zobaczyć jaka jednostka wielkością itp na podstawie ruchu efemeryd. Eustachy dodał, że użycie kilku pintek - mamy "sieć radarową".

ExZ+4+3O:

* X: martwe okręty Orbitera się obudziły. Nawiązują kontakt. "Tu OO Perforator. Zatrzymaj się i zawróć. Bronię bramy." Niestety, Perforator widzi wszystkie jednostki.
* X: żeby móc więcej się dowiedzieć, musicie być blisko. Ale sygnatura jest noktiańska.

Eustachy jest nieco nieszczęśliwy. Wraz z Klaudią - co wiedzą o Perforatorze? To jest dobry moment, by wykorzystać Flawię - niech Flawia "przekona" Blocha o przekazanie poszerzonej bazy danych do Persefony Inferni.

ExZ+3:

* XX: Będzie skandal...
* V: ...ale Infernia ma bazę danych Sił Specjalnych odnośnie jednostek Orbitera.

Perforator jest uzbrojony w pociski z opcją samozniszczenia (rakiety). Używa jako broń krótkiego zasięgu. Plus działa energetyczne które są tak złożone by pasmo defensywne i aura Bramy je neutralizowały. Sam Perforator nie ma dużych detekcji - do tego służyły niszczyciele.

Plan Eustachego - siedem WIDOCZNYCH pintek, jedna niewidoczna jako złom kosmiczny i wysyłamy Ariannę w ostatniej pintce. Eustachy ARANŻUJE.

TrZ+3:

* XX: Arianna musi tam lecieć POWOLI. Wolny, pełzający złom.
* V: ...ale doleci.

Dobra. Czyli NOWY plan:

* Elena w Gwieździstym Ptaku --> Perforator.
* Pintki lecą na Bramę i się pozycjonują.
* Pintki TYMCZASOWO są Bramą. Odcina się "prąd" do Perforatora i jednostek i umierają.

Działamy. Elena ściąga ogień Perforatora w swoim Ptaku.

ExZ+3:

* V: Elena odciągnęła Perforatora. Kilka celnych strzałów, manewry, uniki.

Pintki się zbliżają do Bramy. Wiadomość do Arianny - "Twoja rola w tym zadaniu jest marginalna". Pintki tymczasowa Brama, dezaktywacja wszystkich jednostek. Arianna wbija się na Perforatora.

(+3 do testu Eleny):

* X: Ściągnięta Elena przeleciała za blisko Bramy. Ptak jest Skażony. Wymaga odkażenia.
* V: Elena przeprowadziła Ariannę na Perforator.
* V: Elena zdążyła na Perforator ZANIM Perforator zgasł i pintka pojawiła się na miejscu.

Arianna i Elena są na pokładzie Perforatora. Z mostka sygnał "Oliwio Karelan, zgłoś się". Na poziomie na poziomie kanału magicznego. Jednostronne. Ostrożnie idą na mostek. Idą spacerem kosmicznym w kierunku na mostek.

TrZ+3+3:

* V: Bezproblemowo dotarły na mostek.
* X: Elena ma lekkie Skażenie - była dłużej.
* V: Zaskoczenie --> mostek

Na mostku są upiory załogi. I konsoleta komunikacyjna - to ta konsoleta komunikuje się z Arianną. Arianna używa technomancji - impuls elektryczny by skierować uwagę wszystkich w stronę innego elementu by spadli XD. I Elena do eksterminacji.

TrZM+3+5O:

* O: Konsoleta hiperlinkuje z Arianną.
* Vm: Konsoleta komunikacyjna WESSAŁA duchy załogi.

Elena próbuje zgrać co jest w stanie z danych Perforatora.

ExZM+3+5O:

* V: Elena zgrała informacje jakich szukała Medea i Bloch.
* XX: Elena troszkę odpływa. Jej moc sama się wylewa.

Tymczasem do Arianny: "Oliwio Karelan, zgłoś się". Admirał Atanair, ze Spatium Gelidy. On nie ma pojęcia, że minęło więcej niż kilka godzin. Faktycznie, siły specjalne skutecznie ich uwięziły. Gelida przesłała "Catarinę". W tym momecie pintki zadziałały i wszystko zgasło.

Elena nie chce wracać. Muszą coś wymyślić by nikt się nie dobierał do Eustachego. Arianna widząc Esuriit zaproponowała pas cnoty. Elena nie jest wytrącona - jest szczęśliwa pomysłem. Nieletalny pomysł, zadziała. Arianna przekonuje Elenę do natychmiastowego powrotu.

TrZ+3:

* X: Elena broadcastuje "Eustachy! Pas cnoty czeka! Do gabinetu Martyna, natychmiast!!!"
* V: Elena natychmiast chce lecieć na Eustachego.

Elena + Eustachy, do gabinetu Martyna. Martyn powiedział Elenie, że Eustachemu trzeba pas cnoty założyć. Niech ona to zrobi. Elena wyszła z Lancera i zaatakowała Elenę Leona.

TrZ+3

* X: Leona i Elena są pokiereszowane walką
* X: Medical jest zdewastowany. Wymaga "renowacji".
* V: Elena jest uśpiona i wyłączona z akcji.

Leona, ranna, szczęśliwa. Martyn wsadza Elenę i Leonę do tego co mu zostało. Nie ma pełnych detoksów, ale ma dość środków. "A na marginesie, nie mam pasa cnoty".

* E: "Moja cnota nigdy nie była zagrożona."
* A: "Aż tak podoba Ci się moja kuzynka?"
* E: "Nie odpowiadam na takie pytania. To nieetyczne. Źle rzutowałoby na moją opinię."

Duże jednostki nie są w stanie zbliżyć się do Bramy. Efemerydy i inne takie. Ale pintki zajęły miejsce dookoła Bramy i przeprowadzają naprawę. 

Arianna pyta Blocha o Catarinę. Bloch odpowiedział - to jest jednostka dalekiego działania. Jednostka autonomiczna, wykorzystująca BIA. Koordynująca inne jednostki. "Genestealer-type" - leci i buduje komórkę terroru.

Wycofano jedną pintkę. Na pokład - Eustachy, Klaudia, Arianna, Janus. Cel - dowiedzieć się z Bramy jak najwięcej.

(powrót do ExZ+4+3O:)

* V: Mamy kierunek Catariny. Wiemy gdzie poleciała. WIemy, że jest zagubiona jak cholera.

Klaudia nadaje do Catariny. Informacje - Zona Tres, Spatium Gelida, kody kontrolne Noctis (przechwycone przez siły specjalne z torturowanych noktian). Czyli niby wszystko jest OK. I Klaudia próbuje wrobić Catarinę, by ta zrobiła jakiś błąd. Odsłoniła pozycję, nawiązała kontakt... cokolwiek. Plus wpina w sygnał to co czuła gdy była spięta z BIA.

ExZ+4: 

* Vz: Catarina zrobiła błąd. Jest namiar. Da się na nią polować.
* XX: Catarina nawiązała kontakt z siłami Noctis. Zostawiła informacje, wytyczne itp.
* Xz: Nasi noktianie nadali jej sygnał, by ją ostrzec.
* V: MIMO WSZYSTKO Niobe + Netrahina dały radę Catarinę zniszczyć. Netrahina i Niobe nie są nawet uszkodzone.

Połączenie hipernetowe. Admirał Atanair. "Doprowadziłaś do zniszczenia Catariny. Nie wiem co jest po drugiej stronie i co tam się stało, ale jesteś osobiście za to odpowiedzialna, Oliwio."

## Streszczenie

Infernia wróciła z innymi jednostkami naprawić Bramę Kariańską. Eustachy ufortyfikował i uniewidocznił pintki; Arianna pozyskała dane z reanimowanego magią krążownika Orbitera. Przez Bramę przeszła ON Catarina; Klaudia ją zlokalizowała i wystawiła Netrahinie i Niobe. Niestety, Arianna hiperlinkowała się z ON Spatium Gelida. Dla nich minęło tylko kilka godzin, dla nas - kilkadziesiąt lat...

## Progresja

* Arianna Verlen: połączona ze Spatium Gelida przez hipernet. W jakiś sposób. Magicznie. Jest kotwicą i komunikatorem.
* Klaudia Stryk: jawnie współpracuje biurokratycznie z siłami specjalnymi Medei; dla niektórych to SUPER, dla innych to poważny problem.
* Gilbert Bloch: skandal, przekazał Flawii coś czego nie powinien (baza danych sił specjalnych Orbitera). Poważny cios w reputację u Medei.
* OO Infernia: dorobiła się bazy danych Sił Specjalnych odnośnie jednostek Orbitera.

### Frakcji

* .

## Zasługi

* Arianna Verlen: gdy Elena była pod wpływem Esuriit, zneutralizowała ją mówiąc o pasie cnoty dla Eustachego. Bezbłędnie nawigowała Skażony OO Perforator, gdzie połowa systemów była efemeryczna. Aha, zlinkowała się hipernetem z ON Spatium Gelida.
* Eustachy Korkoran: mistrzowski plan i inżynieria by ochronić pinasy przed efemerydami i naprawić bezproblemowo Bramę Kariańską; Elena pod Esuriit chce założyć mu pas cnoty.
* Klaudia Stryk: przy współpracy z Medeą zdobyła SPORO zasobów biurokracją; używając Bramy, Janusa i kodów zlokalizowała ON Catarina i skłoniła Catarinę do zrobienia błędu. Acz było Klaudii żal śmierci BII.
* Medea Sowińska: na szybko naprawiła systemy Inferni i dostarczyła sporo sprzętu i wiedzy. Po raz pierwszy BARDZO współpracuje by ratować Bramę Kariańską.
* Gilbert Bloch: zależało mu na naprawie Bramy; przekazał Inferni info o Perforatorze i innych jednostkach Orbitera pod czarem Flawii.
* Janus Krzak: duże wsparcie Klaudii w radzeniu sobie ze zrozumieniem tego co się stało z Bramą Kariańską i zdobywanie informacji o ON Catarinie.
* Elena Verlen: wykazała się mistrzowskim pilotażem ściągając echo krążownika Perforator, po czym pobierając Ariannę spod Bramy i wprowadzając ją na ów Perforator. Ale potem Skażona Esuriit krzyczała, że Eustachemu trzeba założyć pas cnoty i padła do Leony, acz ją poturbowała. CO ZA WSTYD (ten pas...)
* Leona Astrienko: gdy Elena była Skażona Esuriit, zaatakowała ją by unieszkodliwić. Esuriitowa Elena jest bardzo groźna; Leona jednak ją pokonała.
* Flawia Blakenbauer: uwiodła i przekonała Gilberta Blocha do przekazania Inferni tajnych danych sił specjalnych. Jest jej z tym źle.
* Martyn Hiwasser: zdaniem Eleny, na pewno ma pas cnoty w sprzęcie (fałsz). Pomógł naprawić Elenę z Esuriit i Leonę po walce z Eleną.
* Seweryn Atanair: admirał Noctis dowodzący ON Spatium Gelida. Myślał że połączył się z Oliwią a to była Arianna. Dla niego minęło kilka godzin...
* OO Netrahina: miała służyć do naprawy Bramy, ale została w odwodzie. W związku z tym posłużyła do znalezienia i zniszczenia ON klasy Catarina.
* OO Kanagar: silnie ekranowana przed Eterem jednostka wezwana przez Medeę do naprawy Bramy.
* OO Trasman: silnie ekranowana przed Eterem jednostka wezwana przez Medeę do naprawy Bramy.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Brama Kariańska: wszystkie węzły Bramy zostały naprawione przez Infernię, Kanagar, Trasman. Brama była użyta przez ON/BIA Catarina.

## Czas

* Opóźnienie: 2
* Dni: 3

## Inne

.

## Konflikty

* 1 - Arianna -> Medea, z pewnością NIE CHCE narażać Netrahiny na tą misję. Niedawno oberwała i może być słabym punktem. Lepiej niech jest w odwodach.
    * Tr+3
    * XV: Arianna ma dobre serce i martwi się o Netrahinę; sukces, odwody
* 2 - Klaudia ma możliwość poprzesuwania biurokratycznego + wsparcie siły Medei. Chodzi o to by jak najwięcej zasobów jak najszybciej tu się dostało.
    * ExZ+3
    * XVV: jawna współpraca Klaudia - specjalne siły. Na plus dla jednych, na minus dla innych. A Eustachy ma dość środków na wszystkie pintki.
* 3 - Eustachy ma plan - pintki jako "jednostki szybkiego reagowania" gdzie potrzebny kopniak a statki jako "dowodzące obszarami naprawczymi". Na szybko trzeba ufortyfikować pintki Inferni i wszystkie inne.
    * ExZ+4
    * XVVV: wyczerpane zasoby; ufortyfikowane i maskowane pintki; efemerydy widzą tylko duże jednostki. I pintki mogą być temp-węzłami Bramy jak trzeba.
* 4 - Janus i Klaudia próbują dojść do tego co jest innego i nie tak z Bramą. Lecą na pintce z Janusem - tak jest bezpieczniej.
    * TrZ+4+3O
    * V: coś przeszło przez Bramę.
    * PROMOCJA KONFLIKTU DO ExZ+4+3O
    * XX: martwe okręty Orbitera się obudziły; a trzeba podlecieć blisko
    * (jak już się skończyło praktycznie wszystko) V: Mamy kierunek Catariny. Wiemy gdzie poleciała. WIemy, że jest zagubiona jak cholera.
* 5 - Niech Flawia "przekona" Blocha o przekazanie poszerzonej bazy danych do Persefony Inferni, bo Perforator.
    * ExZ+3
    * XXV: Będzie straszny skandal, ale Flawia przekazała bazę do Inferni a Klaudia zrobiła kopię.
* 6 - Plan Eustachego - siedem WIDOCZNYCH pintek, jedna niewidoczna jako złom kosmiczny i wysyłamy Ariannę w ostatniej pintce. Eustachy ARANŻUJE
    * TrZ+3
    * XXV: Arianna leci POWOLI, pod okiem Perforatora... ale doleci bezpiecznie
* 7 - NOWY plan: Elena odciąga Perforatora swoim Gwieździstym Ptakiem by pintki mogły się usytuować
    * ExZ+3
    * V: Elena odciągnęła Perforatora
    * (+3 do Eleny) XVV: Za blisko Bramy; Ptak Skażony; Elena przeprowadziła Ariannę na Perforator i perfekcyjnie wylądowała
* 8 - Elena i Arianna ostrożnie idą na mostek. Idą spacerem kosmicznym w kierunku na mostek.
    * TrZ+3+3
    * VXV: Elena lekko Skażona; dotarły i efekt zaskoczenia
* 9 - Arianna używa technomancji - impuls elektryczny by skierować uwagę wszystkich w stronę innego elementu by spadli XD.
    * TrZM+3+5O
    * OVm: Konsoleta hiperlinkuje trwale Ariannę i Spatium Gelida; konsoleta WESSAŁA duchy załogi
* 10 - Elena próbuje zgrać co jest w stanie z danych Perforatora
    * ExZM+3+5O
    * VXX: Sukces, dane pozyskane; ale Elena odpłynęła i jej moc objęło Esuriit.
* 11 - Arianna przekonuje Elenę do natychmiastowego powrotu. Elena nie chce wracać (Skażenie Esuriit). Arianna rzuca pomysł pasa cnoty.
    * TrZ+3
    * XV: Elena na przylocie broadcastuje o pasie cnoty; leci do Eustachego jak chciała Arianna.
* 12 - Elena wyszła z Lancera i zaatakowała ją Leona by unieszkodliwić.
    * TrZ+3
    * XXV: Leona, Elena pokiereszowane walką; medical wymaga naprawy. Elena nieaktywna.
* 13 - Klaudia nadaje do Catariny. Chce, by ta zrobiła błąd i się ujawniła.
    * ExZ+4
    * VzXXXzV: Catarina zrobiła błąd ale dała instrukcje lojalistom Noctis. A nasi noktianie ją ostrzegli. Ale Netrahina + Niobe dorwały Catarinę.
