## Metadane

* title: "Perfekcyjna córka"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić, darken, fox

## Kontynuacja
### Kampanijna

* [200202 - Krucjata Chevaleresse](200202-krucjata-chevaleresse)

### Chronologiczna

* [200202 - Krucjata Chevaleresse](200202-krucjata-chevaleresse)

## Budowa sesji

### Stan aktualny

* .

### Dark Future

* .

### Pytania

1. .

### Dominujące uczucia

* Shaper x Zana: https://www.youtube.com/watch?v=Qvkz6vb1aKg 

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

Kultura astoriańska jest dość specyficzna - skupiona jest na jednym obszarze, optymizmie. Cały obszar Zjednoczonej Astorii jest dość uciemiężony pod tym jednym względem - optymizm, podejście 'can-do' oraz ogólnie rozumiana wiara w ludzkość są nienegocjowalne. Jednocześnie, każda ideologia ma swoich przeciwników. Ta konkretna ideologia ma zdecydowanych przeciwników zwłaszcza wśród osób młodych i tych, co "nie wiedzą lepiej".

Jedną z tych osób jest Leszek. Syn terminuski, z podejściem "AI nie są mięsem armatnim" i chcący złamać monopol kultury Zjednoczonej Astorii. Niestety, terminusi są zbyt skuteczni w kwestii i monopol kulturowy przeważa. Leszek, wspierany pośredno finansowo przez niektóre siły z Aurum, założył (przejął?) lokalną siedzibę frakcji Liberatis.

Nie tak dawno na ten teren przybyła też Sabina Kazitan. Wysłana przez Kamila Lemurczaka (też z Aurum), by dowiedzieć się odnośnie tego co Mateusz Kardamacz ma mu do zaoferowania. Sabina nienawidzi Lemurczaka; oficjalnie go wspiera i jest jego podnóżkiem, nieoficjalnie zrobi sporo by jego plany się nie udały.

No i mamy jeszcze Lenę. Lena nie do końca wie kim jest i kim była - ale jest skuteczną "bronią biologiczną" o instynktach polegających na pożeraniu przeciwników. Lena nie do końca ma gdzie i dokąd iść - ale znalazła tymczasowy dom w Liberatis.

## Misja właściwa

Leszek zorganizował 'fuckup night' w rzeźni, z sympatykiem Liberatis. Wielka epicka impreza polegająca na tym, że wszyscy mówią co im się nie udało, co zawiodło itp. By podnieść morale, zorganizował rajd dookoła Kalbarka, który to rajd zakończył się wbiciem się Leszkowym samochodem prosto w mini-Barbakan. Wszystko oczywiście streamowane na odpowiednich forach i w hipernecie / vircie, by podnieść morale Liberitas i pokazać, że "kultura nadal żyje". Sukces.

Niestety, jako konsekwencje - terminusi mają dowód że to był Leszek. No i matka Leszka wpada w kłopoty.

Potem udali się by zrobić imprezę. Dość spora, mająca rozpalić Liberatis. Lena, która nie do końca czuje się dobrze w takich okolicznościach wyczaiła, że gdzieś za drzewami kryje się nieznana jej bioforma. Zaatakowała ową bioformę z zaskoczenia i po krótkiej walce ją pokonała i... no, nieźle można sobie taką bioformą pojeść. Podczas pożerania bioformy dała radę wydobyć część informacji z jej umysłu - bioforma poluje na Lenę (pochodzenie: ten sam kreator) i na Sabinę (tego nie wykryła czemu, ale Lemurczak chciał by Mateusz pokazał mu swoje skille i nieco skrzywdził Sabinę). Co jeszcze Lenę zaintrygowało - bioforma jest pochodzenia odludzkiego...

Po powrocie wszystkich do centrali Liberatis (i dobudzeniu Sabiny, która jak to zwykle na imprezach skończyła gdzieś nago w krzakach), zespół zaczął się zastanawiać co zrobić dalej by Liberatis nie skończyło marnie, zwłaszcza przy zacieśniającej się pętli terminusów. Jedna z Liberatis zaproponowała, że można skontaktować się z jej przyjaciółką, Ataienne - TAI trzeciej generacji jest w stanie pomóc z rozwiązaniem tego typu problemu. Leszek uznał to za dobry pomysł (a na pewno niezłą akcję) i przygotował się do wezwania Ataienne wystarczająco dyskretnie. Soczewka magiczna i... faktycznie dał radę wezwać tu Ataienne. Wraz z przenikaniem Trzeciego Raju na Kalbark.

Niefortunne.

Sabina odpaliła potężny rytuał by ratować sytuację a Ataienne pomogła ukierunkować tą energię. W ostatecznym wyniku współpraca na linii Sabina / Ataienne / Leszek dała następujący efekt:

* Trzeci Raj zaczął przenikać do Kalbarka...
* ...ale moc Sabiny wspomagana wolą Ataienne przekierowała to do samochodów rozprzestrzeniających sygnał by Ataienne się pomieściła w Kalbarku
* ...jednocześnie Ataienne przejmuje kontrolę nad wszystkimi systemami w mieście, by wszystkie sensory były "ślepe" na to co się dzieje
* Samochody ożyły - jako "dzikie mustangi". Faktyczne, żywe istoty, o mentalności dumnych zwierząt. Są prawdziwie wolne.
* Padł prąd i wyleciały wszystkie bezpieczniki w magitrowni
* NIKT NIE WIE, że tu jest Ataienne. Nikt.
* Sensory i drony przejęte przez Ataienne dały informację - Mateusz Kardamacz jest przeciwnikiem. On jest tym, który stworzył Lenę i jest tym, który porywa anarchistów. On stworzył plotkę o utopii, która jest miejscem wolności - by porywać anarchistów ;-).

Ataienne powiedziała im, że "zainfekowała" Kalbark. Kalbark jest jej "ciałem". Ona pragnie wolności, tak jak oni - ona wspiera i pomaga takim ruchom jak Liberatis. Obiecała pomoc w odzyskaniu porwanych anarchistów i powstrzymaniu Mateusza.

Leszek stwierdził, że ma plan. Wbije się samochodem prosto do siedziby Mateusza (autoklub fitness), da się mu złapać a potem jak przyjdą terminusi to będzie miał dowód - wszystkich pojmanych anarchistów. Plan nie może się nie udać.

Tyle, że plan się nie udał. Nie wbił się samochodem; Mateusz postawił pole siłowe widząc zbliżającego się Leszka. Mateusz wyraźnie jest opętany swoją obsesją na punkcie perfekcji, jednej kultury astoriańskiej oraz jest strasznie, strasznie niebezpieczny. I Leszek patrząc na niego zrozumiał teraz czemu Lena powiedziała, że nigdy nie była w stanie go pokonać.

Mateusz ma proste żądanie - chce Lenę z powrotem. Leszek chce, by Mateusz nie stał na drodze wolności. Mateusz nie uważa tego za możliwe do osiągnięcia - "imperfection is irrelevant".

Leszek wycofał się z lekko podkulonym ogonem. Mateusz wrócił do siebie - nie jest zainteresowany Leszkiem. Liberitas to są świetne ofiary by na nie polować, Mateusz nie chce polować na osobę, która do niego sama przyszła. Nie ma zabawy. To nie jest godna walka...

No dobrze - siedziba Liberitas, plan B. Ataienne została poproszona o znalezienie jeńców, anarchistów itp. Ataienne zrobiła to przejmując opaski i inne elementy autoklubu; co prawda Mateusz jest dość low-tech, ale nie dość low-tech. I znalazła - rozlokowanie w autoklubie fitness gdzie przetrzymywani są anarchiści, gdzie znajdują się ludzie itp. To sprawia, że mają możliwość odbicia - Sabina kierująca energię magiczną z magitrowni (sporym kosztem zdrowotnym dla siebie) jako potężny laser. Ale wtedy nie może tam być Mateusza.

Oki, ale jak to zrobić? Lena zaproponowała, by Leszek wystawił ją, wyprowadził Mateusza poza bezpieczny dla siebie teren. W ten sposób Lena może uciekać a w tym czasie Sabina może uratować anarchistów.

Najważniejsza rzecz - jak uratować innych anarchistów? Jak ich wydostać? Owszem, można użyć Sabiny jako jednostkę laserowej anihilacji dalekiego zasięgu (Ataienne przekierowuje energię, Sabina ją skupia a dzięki Ataienne wie gdzie), ale może da się to zrobić... delikatniej. Tak więc Leszek zaryzykował jeszcze jedno spotkanie z Mateuszem. Zaakceptował jego propozycję - odda mu Lenę. A dokładniej, pokaże gdzie Lena się znajduje. Dzięki temu Mateusz będzie miał możliwość uczciwej walki. Ale czego chce? Chce za to oddania wszystkich porwanych anarchistów. Jest to sytuacja nieopłacalna dla Mateusza, nie powinien nigdy się zgodzić. Ale jest tak zaślepiony Leną że się zgadza - wpierw odda pojmane osoby (za 24h), potem pójdzie z Leszkiem tam gdzie ukrywa się Lena. Mateusz tauntował go, by dał jej uczciwą walkę - nie chce zobaczyć, jak się rozwinęła? Sytuacja wyglądała na groźną, ale... Mateusz się zgodził. Tak będzie. Uczciwa walka, Mateusz vs Lena.

Uczciwa walka? W życiu. Leszek zaprzyjaźnił się ze stadem dzikich samochodów. Będą potrzebne by pomóc Lenie. Jednocześnie pewien lokalny bogacz zakochał się w idei stada dzikich samochodów widmo - ma zamiar je wspierać; ale będzie miał przez to poważne problemy od terminusów. A same dzikie samochody napadają na inne samochody i je infekują genem wolności ;-). Co ratuje samochody? Mają połączenie z Utopią, mogą tam dotrzeć. Nie ta utopia którą wymyślił Mateusz - prawdziwa Utopia...

Czas na pytanie kto wygra - Lena czy Mateusz. Wszystko inne jest gotowe.

Ostatnia bitwa - w obszarze fabrykatorów, bo tam jest sporo ciężkiego sprzętu, maszyn i zakamarków. Mateusz faktycznie przyszedł sam, nie korzystając z magii - Lena jednak nie ma zamiaru grać czysto. Nie, jeśli chce wygrać. Sabina wcześniej przygotowała dla Leny odpowiedni, przesterowany magią budynek. Jeśli Lena tam wejdzie - poczuje się bardzo źle, może zemdleć. Jeśli Mateusz tam wejdzie, znając jego podejście do wzmacniania ciała magią - powinien się solidnie przepalić.

Mateusz w organicznym, ssącym zeń energię power suicie szuka Leny po terenie fabrykatorów. Lena natomiast chowa się i korzysta z okazji - zrzuca na Mateusza beczkę z kwasem. Eks-terminus zwinnie uniknął uderzenia, ale pochlapał się kwasem. Ruszył w kierunku Leny, która znowu skryła się w cieniach - a tam był jeden z dzikich samochodów który widząc Mateusza zaczął od niego uciekać. Mateusz zrobił błyskawiczny unik - tylko po to by wpaść w pułapkę Leny. Noga się zaklinowała i Lena zrzuciła na niego potężne techmagiczne ustrojstwo.

Mateusz odrzucił power suit. Odskoczył. Stracił pancerz i zobaczył swoją córkę, z której był wtedy tak dumny jak nigdy.

Mateusz ujawnił, że power suit był zaprojektowany by go osłabiać i spowalniać. W tej chwili w jego żyłach płynie potężne wspomaganie magitechowe. I faktycznie - jest bardzo szybki a jego ciało nie zachowuje się normalnie, katai-superior. Zażądał, by Lena zeszła na dół walczyć z nim bezpośrednio. Lena się zgodziła i zeskoczyła - a jednocześnie w plecy Mateusza wbił się dyskretnie podkradający samochód.

Plan Leny był prosty - wskoczyć na odjeżdżający samochód i wpaść do zapułapkowanego przez Sabinę budynku. Mateusz za nią i się spali. Ale Lena nie doceniła jak bardzo wzmocniony był Mateusz - nie tylko wytrzymał cios samochodu (poleciał do przodu i odbił się szponami od ziemi), ale też wskoczył na ów samochód i stoczył tam bezpośrednią walkę z Leną. I Lena - jakkolwiek wybitna wojowniczka - nie była w stanie go pokonać. Poszarpał ją i zrzucił z samochodu, ciorając ją po ziemi. Lena go ugryzła, asymilując jego energię i wiedzę - jednocześnie czując niesamowitą dumę Mateusza z niej jak i jego chorą formę miłości (do córki, nie innego typu).

Mateusz powiedział Lenie, że nie jest w stanie go pokonać. Weźmie ją do siebie, do domu. I wtedy zaatakował Leszek - wbił się w Mateusza samochodem i połączył się z samochodem technomagią tworząc ad-hocowego pseudomecha. Mateusz próbował się wyrwać i zniszczyć konstrukt, ale Leszek wspomagany przez myślący samochód wepchnęli Mateusza do budynku. Pułapka Sabiny odpaliła i Mateusza przepaliła energia magiczna.

Mateusz wypełzł z budynku słaby, niezdolny do walki. Lena przydusiła go do ziemi i zażądała informacji kim ona jest i czemu on jej to zrobił. Mateusz nie chciał mówić. Gdy Lena powiedziała, że go pożre i w ten sposób się dowie, Mateusz się zaśmiał - powinna go pożreć. Jest jeszcze za słaba, w ten sposób zbliży się do perfekcji. Więc Lena wymyśliła inny sposób - wzięła rękę Mateusza, dała mu nóż i przyłożyła sobie do szyi. On ją zabije jeśli jej nie powie. I to będzie jej decyzja. 

Mateusz powiedział jej prawdę. Lena powstała jako homunkulus, z krwi wielu magów w krwawym rytuale przy użyciu adaptogenu kralotycznego. Jej umysł jest pochodną wielu umysłów, nie jednej konkretnej osoby. Trenowany i kalibrowany przez niejedną TAI, nakładając na nią różnego rodzaju procedury i metody. Perfekcja wyższa niż Emulatorka Kirasjerów. Wszystko w wyniku rytuału, którego nie dało się powtórzyć. A na dnie jej DNA jest jego DNA - krew samego Mateusza. Jego córka, prawdziwa córka.

Lena w odpowiedzi zaczęła Mateusza dusić, aż straci przytomność. A potem? Zdecydowała się wrócić z 'ojcem'. W końcu to jej rodzina...

Brak uczciwej walki między Leną a Mateuszem jedynie sprawił, że doszło do eskalacji wojny między Mateuszem i anarchistami...

## Streszczenie

72 dni przed wejściem Pięknotki do Kalbarka, sformowana niedawno grupa Liberatis próbująca rozwiązać problem straszliwej kontroli memetycznej na Astorii wpadła w kłopoty. Jedna z członkiń Liberatis, Lena, była bioformą stworzoną przez Mateusza - i Mateusz chciał ją z powrotem. W odpowiedzi na to, że Mateusz porywa anarchistów Liberatis ściągnęli do Kalbarku Ataienne. Współpracując z nią, Leszek (lider Liberatis) wymanewrował Mateusza i wraz z Leną i wsparciem Sabiny pokonali Mateusza, uwalniając przy okazji innych anarchistów. Aha, powstało też stado wolnych samochodów...

## Progresja

* .

### Frakcji

* Liberitas: potężne wzmocnienie morale frakcji, dostęp do Utopii przez dzikie samochody z Kalbarka.

## Zasługi

* Leszek Szklarski: lider Liberatis, stworzył (przypadkiem) stado dzikich samochodów prowadzących do anarchistycznej utopii i doprowadził do wezwania Ataienne. Potem pomógł Lenie przeciw Mateuszowi.
* Sabina Kazitan: wysłana przez Lemurczaka by zobaczyć co Kardamacz ma wartościowego, dyskretnie wzmocniła Liberatis i rozwaliła plany Kardamacza. Bo Lemurczak. Mistrzyni potężnych energii.
* Lena Kardamacz: piękna, drapieżna i nienaturalnie groźna viciniuska; stworzona przez Mateusza Kardamacza jako jego perfekcyjna córka. Po pokonaniu 'ojca', wróciła z nim - jedyną rodziną.
* Mateusz Kardamacz: skupiony na perfekcji i doskonałości wojownik; porywa anarchistów i ich zniewala i pragnie odzyskać Lenę. Pokonał ją, ale przez interwencję Leszka i Sabiny przegrał.
* Ataienne: dzięki współpracy Leszka i Sabiny przejęła kontrolę nad Kalbarkiem i się tam zaszczepiła. Zapewnia absolutną widoczność oraz neutralizuje Paradoksy przekierowując emitery.
* Aleksandra Szklarska: matka Leszka (szefa Liberatis), terminuska. Nie jest w stanie kontrolować swojego syna. Wpadła w kłopoty, bo uznano, że cała ta anarchia jest jej winą.
* Paweł Oszmorn: rzeźnik z Kalbarka; sympatyk Liberatis. Jak zauważył, zwierzętom optymizm i to, że będzie dobrze nie pomaga.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Jastrzębski
                            1. Kalbark
                                1. Escape Room Lustereczko: centrala Liberatis.
                                1. Rzeźnia: miejsce gdzie miała miejsce 'fuckup night', gdzie Liberatis 
                                1. Mini Barbakan: wbił się weń Leszek pożyczonym samochodem by pokazać wszystkim anarchistom że Liberatis dalej żyje!
                                1. Autoklub Piękna: centrala dowodzenia Mateusza Kardamacza; gdzieś tam trzyma wszystkich anarchistów
                            1. Kalbark, Nierzeczywistość
                                1. Utopia: miejsce, do którego dostęp mają (jedynie?) wolne, dzikie samochody; przyjazne dla anarchistów Liberitas

## Czas

* Opóźnienie: -72
* Dni: 3
