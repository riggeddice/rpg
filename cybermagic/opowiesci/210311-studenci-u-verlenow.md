## Metadane

* title: "Studenci u Verlenów"
* threads: legenda-arianny
* gm: żółw
* players: kić, fox

## Kontynuacja
### Kampanijna

* [210306 - Wiktoriata](210306-wiktoriata)
* [210821 - Cichy Verlen z Sanktuarium Kwiatów](240821-cichy-verlen-z-sanktuarium-kwiatow)

### Chronologiczna

* [210306 - Wiktoriata](210306-wiktoriata)

## Punkt zerowy

### Dark Past

* .

### Opis sytuacji

* .

### Postacie

* .

## Punkt zero

N/A

## Misja właściwa

Arianna i Viorika leciały ścigaczem w kierunku Poniewierzy, pomóc Apollo. Jednak w oddali przywitał je kłąb dymu na horyzoncie. W Poniewierzy stało się coś strasznego. Elena: (OXV): eksplozja. Formowanie efemerydy. Elena próbowała pomóc, ale zakończyło się... katastrofą. Ale nie ABSOLUTNĄ.

Arianna ma większy, czteroosobowy ścigacz. Wraz z Vioriką - lecą. Ale to pierwszy bardzo poważny lot Arianny i Arianna leci na pełnej mocy. Gaz do dechy. 

* XX: Zdewastowała swojego awiana, prawie zabijając się na drodze.
* XX: Po tym, jak zarekwirowała inny pojazd, tamten TEŻ rozwaliła. Nie zdążą... zdążą po wszystkim...

Jednak jak Elena prowadzi, to tak łatwo to wygląda. Jak prowadzi Arianna, ech. Cóż, będą się z niej wszyscy śmiali >.>.

Gdy A&V dotarły na miejsce, wszędzie ruina. Dom Uciech został zniszczony (pół roku odbudowy). Są ranni i ofiary. Apollo z obłędem w oczach stabilizuje wszystko, by nic więcej się nie popsuło i by wszystko działało. Przedawkował magię by chronić wszystko i wszystkich. Apollo kazał szybko A&V zająć się Eleną; nie wie gdzie jest, ale to ona jest przyczyną tego wszystkiego.

Arianna i Viorika poszły za śladem zniszczenia. Przez lasek. Znalazły małą jaskinię, miejsce, gdzie Elena często się chowała jak było jej bardzo źle. Najpewniej schowała się tam znowu. Arianna i Viorika spróbowały wyciągnąć stamtąd Elenę, przytulić, porozmawiać - Elena jest zrozpaczona. Całkowicie zrozpaczona. Ma krew na rękach. Straciła kontrolę nad mocą magiczną. Nie przewidziała rezonansu?

"Eleno, wyjdź, przecież wiesz, że nigdy nie umiałaś się schować" - Arianna

Elena to kłębek radioaktywnej żałości. Wypełzła z jaskini, cała nieszczęśliwa. Nie chce ich widzieć, nie chce istnieć, chce by to wszystko się nie stało.

Elena powiedziała pochlipując, że nie jest Arianną. Ma moc, miała mieć moc, miała być potężna, ale NIE JEST ARIANNĄ. Nie umie utrzymać energii i teraz zginęli przez to niewinni ludzie. Arianna powiedziała, że fakt - Elena nie jest nią, ale Arianna zniszczyła dwa ścigacze. Elena zauważyła, że Arianna jest arcymagiem a Elena umie pilotować ścigacz. Zupełnie nie to samo.

* X: od teraz Elena dąży do kontroli swojej mocy za WSZELKĄ cenę
* V: Elena się trochę uspokoiła
* V: Elena nie zrobi nic skrajnie głupiego (nie zabije się, nie pozbawi się mocy...)
* V: Viorika ją zaciągnęła na Osiedle Nadziei. Do cywilizacji. By Elena była w normalnym miejscu.

Verlenki przejęły na Osiedlu Nadziei kontrolę nad randomową chatą i skupiły się na 2-3 dniowej opiece nad Eleną. W czasie, w którym Viorika się silnie skupiała na Elenie i trzymaniem jej przy poczytalności, Arianna poszła dowiedzieć się o co chodzi. Wiedzą już, że trzy osoby nie żyją i 14 wił zostało zabitych. Ale jak? Dlaczego? Czas na rekonstrukcję wydarzeń - bo wszyscy twierdzą (poza Eleną), że to jest możliwe. A Elena powiedziała, że jej moc się wyrwała spod kontroli i uderzyła zupełnie inne miejsce, coś, co nie ma sensu i nie powinno zaistnieć; to nie był efekt Paradoksalny. Wyczułaby.

Arianna przemyciła trochę alkoholu dla żołnierzy w szpitalu i poszła ich przepytać.

SYTUACJA:

Ogólnie, od początku pobytu Eleny i magów innych rodów w Poniewierzy sytuacja jest taka, że Elena próbuje zachowywać się godnie i poważnie, a inni traktują ten teren po swojemu. Elena próbuje opanować młodych magów, ale zupełnie jej to nie wychodzi - nikt jej nie słucha a Apollo nie pomaga.

Odnośnie tej sprawy, żołnierze powiedzieli, że był transport nowych przedmiotów anomalnych do magazynu rzeczy niebezpiecznych (w południowej części Poniewierzy). Po drodze od bramy do magazynu doszło do erupcji; pojawiła się dziwna forma efemeryczna z tych wszystkich rzeczy. Wojsko zaczęło to zwalczać, Elena dołączyła im pomóc i wtedy jej moc zniszczyła budynek w innej części miasteczka (północnej).

PRZESŁUCHANIE:

* V: Żołnierze mówią, że Elena chciała pomóc. Słuchała rozkazów. Jej moc wyraźnie z czymś zarezonowała.
* X: Elena i Arianna są jak demon i anioł w Poniewierzy. Elena jest tu skreślona; nie będą jej ufać. A Ariannę kochają jeszcze bardziej.
* V: Anomalia pojawiła się na drodze DO MAGAZYNU. Anihilacja mocy Eleny uderzyła w DOM UCIECH. To nie po drodze. Coś jest cholernie nie tak - to spójna relacja wszystkich obecnych.

Dla Arianny to bardzo dziwne i niepokojące. Magia tak nie działa.

Wieczorem Arianna wymknęła się - poszła zobaczyć ruiny Domu Uciech i zobaczyć co tam się stało. O co chodzi. Może jej magia coś pomoże jej odkryć (ExMZ+2):

* X: praca nad tym zajmie jej całą problematyczną, długą, męczącą noc.
* V: jest obecność anomalii na tym terenie. W ruinach znajduje się anomaliczne LUSTRO. To jest niepokojące, bo wszystkie anomalne lustra są nielegalne.
* X: pojawiła się efemeryda, coś w stylu Eleny (?!) - Arianna natychmiast wezwała Viorikę na pomoc. Gdy Viorika weszła w interakcję z anomalią, Arianna wróciła do badań.
* V: lustro nie miało właściwości erotycznych. Ale ODBIJA Elenę. Innymi słowy, jest jakaś korelacja między tym lustrem a naszą Eleną.
* X: wtórny rezonans - Elena wie, że tu jest to lustro i że ono ją odbija. Przyjdzie "pomóc" lub "obejrzeć"
* V: efemeryda która powstała była co najmniej częściowo sztucznie stworzona. Nie jest w 100% naturalna. Ktoś - jakiś mag - maczał w to palce.
* X: potrzebna jest Viorice grupa żołnierzy do walki z anomalią która wygląda jak Elena

A Viorika wraz z żołnierzami skupiła się na walce z anomalią. Czego nie wiedziały - Elena patrzy z oddali i nie rozumie co się tu dzieje. Czemu ona? Ale nie wejdzie na pole bitwy, nie pomoże - boi się swojej własnej magii. Nie odważy się. Viorika vs "Elena" (wygląda tak samo, tylko mniej ubrana i nieco większy biust):

* V: "Elena" jest na tyle odbita, że nie atakuje cywili. Nie chce niczyjej krzywdy.
* XV: "Elena" ma swoje odbicie w drugim, bliźniaczym lustrze - gdzieś w Poniewierzy. Tamto lustro jest sprzężone z Eleną i Elena wie o jego istnieniu i położeniu.
* V: Viorice udało się zwalczyć efemeryczną Elenę.
* O: Elena ma **stały** namiar na lustro. Zawsze.

W końcu Viorika potrzebuje Eleny i ta się szybko pojawiła (nic dziwnego, patrzyła od początku). Ma emo-gothic makijaż. Zrobiony bardzo, bardzo źle. Viorika zdecydowała się poczekać aż Arianna dojdzie do siebie; ona nie czuje się na siłach wyjaśnić Elenie czemu ten makijaż do niej tak bardzo nie pasuje.

* "Lubię Cię, więc Ci to powiem. Zmyj to." - Arianna

Elena, zapytana gdzie jest drugie lustro (po tym jak dziewczyny wyjaśniły jej czego ona ma szukać i jak) zaprowadziła Verlenki do Zamku Gościnnego. Do **swojego** pokoju. I przy drzwiach do pokoju są ślady - ktoś próbował się włamać. Ale Elena - zawsze paranoiczna - ufortyfikowała defensywy.

Mniej więcej wtedy do Verlenek odezwał się Apollo. On po swojemu zrobił własny research na temat tych wydarzeń (VXV). Lustra pochodzą od **Blakenbauerów**. Otóż, Dariusz Blakenbauer słynie z wykorzystywania anomalii i jest w tym bardzo dobry. Martwił się, że rody Verlen, Samszar i Perikas się do siebie zbliżą - stąd wprowadził takie działania.

A&V mu pięknie podziękowały. Elena też - wie już, że za tym jakoś stali Blakenbauerowie. Ale jak? Czemu? Elena nie rozumie - co **ona** zrobiła? Co zrobili ci biedni ludzie, którzy zginęli? Czemu tak okrutnie?

A&V podziękowały Elenie - niech wraca na Osiedle Nadziei. Elena nie chce, ale będzie słuchać kuzynek. Gdy poszła sobie, Zespół wezwał Sylwię Perikas i Rufusa Samszara. Pokazały im ślady na drzwiach Eleny - KTOŚ próbował się włamać i KTOŚ z grupy odpowiada za to wszystko co się tu stało.

Gdy Sylwia zaczęła się stawiać, Viorika zdecydowała przejąć kontrolę i wydobyć o co tu chodzi i kto co wie.

* X: Viorika odpaliła zabezpieczenie Eleny, pokazała im co mogło grozić jakby próbowali się włamać do Eleny na serio
* V: Sylwia i Rufus na pewno tego nie zrobili. Nie ten wzór. I potwierdzili notarialnie - do papierów, że ktoś próbował się włamać. 
* XX: "To wszystko wina Eleny, bo prowokowała"

Grr. Wtedy Arianna powiedziała, że mają badaczy anomalii. Przetransportują lustro, zbadają i poznają prawdę. Jako, że to wina Blakenbauerów, faktycznie nikt tu nie ma czym się przejmować.

Wieczorem, Elena skonfrontowała się z A&V. Chce wiedzieć, kto za tym stoi. Kto jej to zrobił. Czy jej powiedzą jak się dowiedzą.

* XX: "wszystko muszę robić sama" - Elena po raz kolejny jest upewniona w tym, że musi liczyć na siebie, że boją się że Elena zrobi coś głupiego.
* V: Elena obiecała, że odda sprawę w ich ręce. Nie będzie ingerować w plan A&V.

W nocy - zastawiona pułapka. Transport lustra. Viorika zaplanowała wszystko perfekcyjnie, by amator wpadł w pułapkę.

* XX: Viorika nie przewidziała DEBILA. Napastniczka szarżowała na karabiny. Jest solidnie ranna. 
* VV: Napastniczka jest złapana i lustro przetrwało.

Napastniczką okazała się... Sylwia. Cała płacze, ranna i obolała. Zero skilli black ops. Natychmiast A&V wezwały Apollo (który z nią regularnie spał). Sylwia się przyznała - chroniła swoich podopiecznych. Oni chcieli móc się zabawić z wiłą w stylu Eleny, nie spodziewali się co się stanie. A ona chce tego sojuszu, a teraz jest straszne ryzyko i w ogóle...

Cóż. WSZYSCY chcą tego sojuszu. Nikt nie chce, by Blakenbauerowie wygrali. Więc:

* Elena wypada z wymiany. Ona musi iść gdzieś indziej, poza tymi magami.
* Wszystko oficjalnie jest winą Blakenbauerów.

A co na to Milena Blakenbauer? Potwierdziła z radością, że to ich wina. W jej oczach, Dariusz był zazdrosny, że Elena flirtuje z magami rodu Samszarów, więc zrobił taką głupotę... (to co zrobiła Milena tutaj dało grupie dyplomatów pracę na dobre 2 miesiące...)

## Streszczenie

Verlenowie mieli wymianę studencką, współpracę z magami rodów Perikas i Samszar. Dariusz Blakenbauer chciał rozbić potencjalny sojusz, dając młodym Perikasom lustro mające zaspokoić ich ochotę na Elenę. Niestety, wszystko poszło katastrofalnie nie tak bo Elena ma anomalną magię - w wyniku eksplozji zginęło kilkanaście wił i 2 żołnierzy. Elena w ruinie psychicznej, wszyscy przerażeni. Wezwane Arianna i Viorika odkryły że za wszystkim stoi Blakenbauer i znalazły winnych wśród młodych Perikasów. Jednak - dla polityki - wszystko zwaliły na Blakenbauerów :D.

## Progresja

* Elena Verlen: powszechnie uważana w Verlenlandzie za niebezpieczną. Verlenka, która straciła kontrolę. Elena - przecież przyjazne stworzenie - się całkowicie oddaliła od "przeciętnego człowieka". Znienawidzona w Poniewierzy.
* Arianna Verlen: powszechna opinia u żołnierzy i w rodzie, że ma dużo zalet, ale pilotem nigdy nie będzie... rozwaliła DWA ścigacze JEDNEGO DNIA. Acz kochana w Poniewierzy.

### Frakcji

* Ród Blakenbauer: podpadli w opinii publicznej. Atak przez Dariusza na dziecko (Elenę)? Serio?
* Ród Verlen: ma casus belli przeciwko Blakenbauerom; są w stanie wypowiedzieć wojnę jakby chcieli - za to, co zrobił Elenie Dariusz.

## Zasługi

* Viorika Verlen: 21 lat, podczas walki z echem Eleny sprzęgła je z lustrem, po czym odnalazła to lustro używając Eleny. Uspokajała i zajmowała się Eleną.
* Arianna Verlen: 20 lat, uspokoiła zdruzgotaną zabiciem ludzi Elenę; do tego rozwaliła dwa ścigacze. Magicznie odkryła, że za wszystkim stał Dariusz Blakenbauer i dziwne lustro - że to nie sama Elena utraciła kontrolę nad mocą.
* Elena Verlen: 16 lat, próbowała opanować młodych, lecz nie miała autorytetu. Potem pod wpływem rezonansu z lustrem (podłożonym przez Blakenbauera i Perikasa) doszło do erupcji jej mocy i zginęły 2 osoby i 14 wił. Schowała się w jaskini; Arianna i Viorika ją wyciągały. Tak bardzo, bardzo pragnęła się wykazać i być jak Arianna.
* Apollo Verlen: wykazał się kompetencją opanowując zniszczony Poniewierz po erupcji energii Eleny. Większość czasu jednak spędzał z Sylwią Perikas ;-). Na końcu wybrał rodzinę nad Sylwię - ona zdradziła jego zaufanie. Plus, mały research odnośnie lustra i Blakenbauera.
* Sylwia Perikas: 20 lat, bardzo chciała się wyrwać z domu, zafascynowana Apollem. Nie wychodziła mu prawie z łóżka, całkowicie zaniedbując młodych.
* Michał Perikas: 17 lat, zdolniejszy i sprytniejszy od Rafała; podłożył lustro i wymanewrował Elenę, podkładając jej drugie lustro do pokoju. Bardziej sprytny. Słabość do twardych dziewczyn ;-).
* Rafał Perikas: 16 lat, Blakenbauer dał mu lustro które "skopiuje" wiłę w Elenę; podkochuje się w Elenie. Przekonał brata do planu. Bardziej społeczny.
* Rufus Samszar: 21 lat, wierzy swojej młodszej siostrzyczce bezgranicznie. Jedyny, który próbuje pilnować nastolatków, co mu całkowicie nie wychodzi.
* Maja Samszar: 15 lat, uroczy, mały diabełek, w co nikt nie wierzy. Poza Eleną, która widziała Maję w akcji. Podżegaczka.
* Dariusz Blakenbauer: 26 lat, BUR; przestraszony potencjalnym sojuszem trzech rodów przeciw Blakenbauerom chciał zrobić kompromitującą intrygę rozrywającą rody używającą lustra i wił. Nie wiedział o magii Eleny i doprowadził do śmierci 3 osób i 14 wił rękami (magią) Eleny.

### Frakcji

* .

## Plany

* Elena Verlen: ma krew na rękach - zabiła 2 ludzi. Jest wstrząśnięta. Zrobi WSZYSTKO by kontrolować swoją energię magiczną i moc; nie chce być potworem. Nie chce zabijać. Nie chce być bronią w cudzych rękach.
* Elena Verlen: MUSI pomścić to, co jej zrobił Dariusz Blakenbauer i młodzi magowie rodu Perikas. Dla swojej chorej chuci młodzi są odpowiedzialni za śmierć ludzi.

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. Poniewierz
                                1. Dom Uciech Wszelakich: zniszczony w płomieniach przez anomaliczną moc Eleny; było tam lustro które miało "dostosować" wiłę do wyglądania jak Elena. Ruina przez pół roku.
                                1. Zamek Gościnny: niewielki, świetnie ufortyfikowany zameczek gdzie mieszkają arystokraci na wymianie studenckiej oraz Elena.
                            1. Poniewierz, północ
                                1. Osiedle Nadziei: najsilniej ufortyfikowana i najbezpieczniejsza część Poniewierzy. Sypialnia. Teraz - miejsce przechowania Eleny
                            1. Poniewierz, obrzeża
                                1. Jaskinie Poniewierskie: seria niewielkich jaskiń, w których Elena zwykła się chować jak jej źle. Ogólnie bardzo fajne i bezpieczne miejsce (sentisieć <3)

## Czas

* Opóźnienie: 1120
* Dni: 6

## Inne

### Projekt sesji

* ?

### Sceny sesji

* ?

## Idea sesji

* ?
