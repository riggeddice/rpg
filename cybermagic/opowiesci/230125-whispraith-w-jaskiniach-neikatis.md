## Metadane

* title: "Whispraith w jaskiniach Neikatis"
* threads: brak
* motives: dreszczowiec, milosc-ponad-zycie, teren-morderczy, potwor-z-horroru, one-by-one-they-die, izolacja-zespolu, burza-magiczna, broken-mind, magowie-pomagaja-ludziom
* gm: żółw
* players: fox, kapsel, kić

## Kontynuacja
### Kampanijna

* [230104 - To, co zostało po burzy](230104-to-co-zostalo-po-burzy)

### Chronologiczna

* [230104 - To, co zostało po burzy](230104-to-co-zostalo-po-burzy)

## Plan sesji
### Theme & Vision

* Nativis ma straszliwe potwory i nie ze wszystkim da się walczyć
* Nativis ma straszny teren
* Nativis ma też zespoły złomiarzy i ekstraktorów
* Planeta nie należy do nas...

### Co się stanie (what will happen)

przed sesją:

* Zniknęła Kamila (żona Antoniego); Antoni ją zostawił przy anomalnym miejscu i gdy wrócił, nie było jej

.

* S00: PRZESZŁOŚĆ, SETUP: 
    * Rufus, Anna, (Aniela, Zofia, Eustachy), Antoni robią operację harvestu po Burzy Piaskowej
        * harvest 'ruchomych wyładowań'
        * pomniejsze kwarcyty z wyładowaniami + 'resztki' (cenne do magitrowni)
        * duży kwarcyt manipulujący piaskiem i wirem
        * POKAŻ: Antoniego i przyjaźń Antoni - Anna - Rufus; Anna i jej mechaniczny ptak

.

S0 -> S1:

* Anna pojechała z Antonim - był sygnał Kamili. Rufus nie chciał jechać, ale Anna pojechała. Miało być za chwilę. Ale nie zdążyli i przyszła burza.
* Rufus rozpaczliwie szuka Eustachego by ten mu pomógł odzyskać Annę. Jest to niebezpieczne, potrzebny mu mag i upoważnienie.

.

* S01: TERAŹNIEJSZOŚĆ, SETUP: 
    * Anna z ekipą nie wróciła z sektora 74s; tam było coś wskazującego na Birutan i los Kamili. -> Rufus, ratujmy Annę
    * Wiadomość od Kalii -> Eustachego, good natured ribbing przez Cypriana i retorta Michała.
* S02: Zbliżamy się do sygnałów
    * Przechwycony sygnał: Anna1
    * Cyprian ma lekkie nerwy; Rufus i Michał go opieprzają
* S03: Łazik
    * Samobójstwo Mirki.
    * Advancer w cieniu dojrzy Scutiera wskazującego na Annę
* S04: Jaskinia 1
    * Złamany Antoni z martwym Benkiem
    * Wiadomość od Anny (Anna2)
    * whispraith DOTKNĄŁ MENTALNIE Michała ("Sekret Rufusa - co jest specjalnego w Seiren")
    * Rufus: "Nie! Patrz na niego! Nie zostawię jej samej, Anna nie skończy jak Antoni!!!"
    * Jaskinia 2 ma sygnały tektoniczne; coś tam się dzieje. I jest transponder. Trzeba się spieszyć.
* S05: Jaskinia 2
    * Wiadomość od A->M
    * Anna, stoi w jaskini i patrzy na ścianę.
    * whispraith: "Nie! Patrz na niego! Nie zostawię jej samej, Rufus nie skończy jak Antoni!!!"; pożera go.
    * whispraith: "Ale... dasz mi się zaprosić na kolację?"
* S06: Ucieczka
    * Cyprian złamany. "Nie jesteście ludźmi!!!". Widzi Annę jako jedyną osobę, ich jako potwory.
    * Antoni opętany przez whispraitha. Anna 1: "Nie wrócę do domu".

Z CZYM WALCZYMY:

* whispraith (mirror wraith | Anna) <-- IMPOSSIBLE
    * "mirror of soul": pokazuje to co było w sercu; widzisz to czego pragniesz
    * "mirror form / souleat": wygląda jak ofiara + pożera ją
    * "displacement": nie jest tam gdzie się wydaje; opętuje Scutiery. Gdzie Scutier tam whispraith
    * "send back corrupted signal": wysyła sygnał jaki się pojawia
* krystalnik
    * wyładowanie elektryczne, manipulacja piaskiem, chce zjeść energię

TEKSTY które się pojawią:

* Kalia1: "Ta wiadomość jest dla Eustachego: tu Kalia. Nie wiem czy mnie poznajesz. Ale... dasz się mi zaprosić na kolację? ALE TY STAWIASZ! I NIE POMYŚL NIC! To jest..."
* Anna1: "Kocham Cię, najdroższy. Nie powinnam nigdy tu przyjeżdżać. Nie wrócę do domu... Kocham Cię, Rufusie..."
* Anna2: "Mam sygnał Kamili, Antoni. Wreszcie ją mam!"
* Antoni1: "Kamilo? Kamilo!"
* Antoni2: "Halo? Jest tam ktoś? Ktokolwiek? Pomocy..."
* Mirka1: "Aaah! Dobra, Benek, to Ty... Benek? BENEK?!"
* Benek1: "Tu Benek; nie wykrywam żadnych anomalii; schodzi w głąb."
* A->M: "Jej sygnał dochodzi stamtąd? Jesteś całkowicie pewna?" "Tak, to dziwne, ale tak jest"
* Kornelia: "tu Kornelia... jestem w środku burzy... nie wracam... zginę tutaj... pomocy..." (ten nie jest retransmitowany)

Lokacje / aspekty:

* burza piaskowa. 
    * Aspekty: nic nie widać, static, echo sygnałów
* jaskinie. 
    * Aspekty: ciasno, niebezpieczna trasa, krystalniki, łatwo uszkodzić Scutiery
    * Jaskinia 1
        * Znajduje się tu złamany Antoni, głaszczący w Scutierze martwy Scutier z Benkiem "Kamila... moja Kamila..."
    * Jaskinia 2
        * Znajduje się tu whispraith
* derelict. 
    * Aspekty: echo komputerowe, migające światła, zwłoki Mirki (samobójstwo)

### Sukces graczy (when you win)

* run away, cut losses

## Sesja właściwa

### Fiszki

ŁAZIK SEIREN:

* Zofia d'Seiren 
    * **objęta na sesji przez Kić**
    * (ENCAO: -0--+ |Unika kłopotów, roztargniona| VALS: Conformity, Security >> Hedonism| DRIVE: Wieczny dług (Anna))
    * salvager Seiren (Seiren Local / combat / medical / tectonics) faeril: "Anna mnie uratowała gdy nikt nie dał mi szansy"   
    * "Życie może jest ciężkie, ale możemy sobie pomóc; działajmy razem." "Uważaj... to może być niebezpieczne."
* Aniela Myszawcowa 
    * **objęta na sesji przez Fox**
    * (ENCAO: -0+00 |Wszystko rozwiążę teorią;;Przedsiębiorcza i pomysłowa| VALS: Achievement, Face| DRIVE: Neikatis ma tyle sekretów i je zrozumiem)
    * salvager Seiren (Seiren Local / advancer / signal operations) faeril: "Im doskonalsza technologia, tym lepiej kontrolujemy sytuację"   
    * "Dzięki technologii Neikatis stanie przed nami otworem. Tyle jeszcze nie rozumiemy, ale nauką i postępem złamiemy tą planetę"
* Anna Seiren
    * (ENCAO: -00+-, Nie lubi ryzyka + Altruistyczny, TAK: face + achi, NIE: Self-direction, Strach przed zapomnieniem)
    * salvager Serien (owner, analityczka / radar / hacker) faeril: mechaniczny ptakozwierz na którym jej zależy, life is pain work is life
    * "Walczmy o lepszy świat! Jak długo unikniemy Birutan, jesteśmy w stanie zbudować własne małe sukcesy!"
    * kadencja: optymistyczny głos typu Sisko
* Rufus Seiren
    * (ENCAO: +-00- |Szuka wyjaśnień w istniejącej rzeczywistości + Niefrasobliwy, beztroski, TAK: Benevolence, Hedonism >> Face | DRIVE: Hardholder)
    * salvager Seiren (owner, pilot / heavy stuff / appraisal ) faeril: skupienie na przychodach i miłość do sprzętu | Lancer z pochodnią plazmową
    * "Bez nas to wszystko by się rozpadło", "Chcecie wrócić do domu ubodzy? :D"
    * kadencja: wesoły, HERE COMES ARTILLERY!!!
* Michał Uszwon
    * (ENCAO: 00-0+ |Zakłóca spokój; nie toleruje spokoju i nudy| VALS: Face >> Achievement| DRIVE: Poznać sekret Rufusa - skąd ma Seiren?)
    * salvager Seiren (hired muscle / engineer) faeril: robota czeka.
    * "Jak masz czas gadać to masz czas robić" "Zamknij mordę."
    * kadencja: twardy, nie okazuje słabości, gardzi mamlasami
* Cyprian Kugrak
    * ENCAO:  0-0++ |Unika nieprzyjemnej roboty;;Lubi pochwały| VALS: Hedonism, Family| DRIVE: Bezpieczeństwo swego dziecka
    * salvager Serien (hired muscle / combat) faeril: tylko ludzkie formy
    * "Nienawidzę tych wszystkich anomalicznych gówien..."
    * kadencja: pomocny, dobrą rękę poda

ŁAZIK Uśmiech Kamili:

* Antoni Grzypf 
    * (ENCAO: -+0-0 |Niepokojący;;Apatyczny| VALS: Family, Power >> Face, Hedonism| DRIVE: Odnaleźć Kamilę, Zadośćuczynienie za zdradę)
    * salvager Uśmiechu (owner)
    * "Straciłem to co kochałem najbardziej. Tylko pracą mogę zagłuszyć pustkę."
    * kadencja: puste serce, obsesja na punkcie Kamili, nie ma uczuć
* (Kamila, nieobecna salvager Uśmiechu, echo)
    * zniknęła - próbowała komuś pomóc, niestety, najpewniej natknęła na Birutan
* Kornelia Lichitis
    * (ENCAO:  00+0- |Autentyczna; mówi jak jest;;Świetnie zorganizowana| VALS: Stimulation, Face | DRIVE: Rana ego (nie dość dobra))
    * salvager Uśmiechu (inżynier), atarienka: "Poradzimy sobie ludzkim intelektem"
    * "Krok po kroku dojdziemy do rozwiązania, rzeczowo i solidnie"
    * kadencja: panika, tchórz

### Scena Zero - impl

.

### Sesja Właściwa - impl

STAN AKTUALNY:

* Rufus + Michał: Skorpion ("Uśmiech Kamili"). Dowiedzieć się co sprawiło że opuścili łazik. Po tym wrócić.
* Zofia + Aniela: Seiren. Aniela dekoduje dane i szuka, Zofia robi autopsję. Potencjalnie polowanie na Kornelię.
* Eustachy + Cyprian: idą w jaskinie. I Eustachy robi nić Ariadny.

Ruszamy silnik i jedziemy w burzę szukać Kornelii. Jak się zbliżacie "gdzieś tam" to złapałyście pętlę:

* Benek1: "Tu Benek; nie wykrywam żadnych anomalii; schodzi w głąb."
* Kornelia2: "Nie! Benek, nie żyjesz, NIE ŻYJESZ!!!"
* Zofia: Tu Seiren, podaj swoją pozycję, dotrzemy
* Kornelia: Seiren? Was też dorwało? To Wy? To... co się dzieje?
* Zofia: To nasze pytanie do Ciebie, co się dzieje
* Anna_COR: "Nie wrócę do domu..."

Aniela wysyła wiadomości do Kornelii.

Tr (brak zasobu bo anomalie) +3:

* X: Kornelia jest przerażona i w lekkim amoku
* X: Musicie zbliżyć się niebezpiecznie daleko z perspektywy reszty zespołu
* V: Masz ją. Widzisz ją. Zbliża się niebezpiecznie do Stworów. Kornelia się odwraca i strzela do Seirena krzyczy "Zostawcie mnie! Pomocy!"

Seiren pojechał szybko, by być pomiędzy stworami i Kornelią. Kornelia krzyczy i strzela - bez amunicji i strzela. 

Seiren ZAGARNIA Kornelię używając łychy. A dokładniej, Aniela to robi ;-).

Tr Z +3:

* V: przechwycona, w łyżce Seirena. I uciekamy od stworów.
    * Kornelia: "Nie tam, nie tam, nie tam! Tylko nie tam!"
    * Zofia: "Wolisz żeby Cię zeżarły?"
    * Kornelia: "Oni wszyscy nie żyją!"
    * Anna_COR: "Nie wrócę do domu... Nie powinnam nigdy tu przyjeżdżać."

TYMCZASEM EUSTACHY:

Cyprian i Eustachy idą w głąb jaskini. Cyprianowi podoba się podejście Eustachego. Eustachy na IFF coś przed Wami. Coś metalicznego. Jakiś scutier. Głębiej. Ale nie ma światła. A na ścianach są takie obślizgłe porosty. Lekko anomaliczna "agresja biologiczna". Idziemy na ten scutier. To Antoni, "znalazłem moją kochaną Kamilę".

* Antoni: Znalazłem ją, jest z nami, wreszcie jest w domu, wreszcie jest ze mną. (tam jest nieżywy Benek)

Benek jest zmumifikowany, ale scutier jest szczelny. Antoni wyraźnie nie jest z Wami. Odpłynął i wydrapał sobie oczy. Plus coś się dzieje na powierzchni między Michałem i Rufusem. Antoni, Cyprian, Eustachy i zwłoki Benka wracają ostrożnie w czwórkę; tamci muszą sobie poradzić.

Tr Z (jest Was dwoje i dajecie radę) +4 +3Or:

* Vz: Eustachy i Cyprian przejdą bez problemu
* X: scutier Benka de facto ześlizgnął się. Antoni "Kamilo!"
* Vr: Cyprian "pierdolę to! i zszedł wyciągać zwłoki"

Zespół skutecznie wychodzicie z jaskini. I Waszym oczom - zbliża się Seiren. A "Uśmiech Kamili" się uruchomił. Ze światłami i wszystkim. I komunikat.

* Rufus: "Halo? Jesteście tu? Dzięki niech będą Seilii... już się bałem."
* Cyprian, cicho: "Wolę Eustachego i chyba Kalia też. Ale nie w ten sam sposób, dobra?!"
* Rufus: "Widzicie gdzieś Michała? Uciekł w piaski"
* Aniela: "Czemu uciekł w piaski? Przed chwilą Kornelię przechwyciliśmy. CO POWIEDZIAŁ? Jesteśmy tu dla Anny!"

Tp (niepełny) Z (pełne zaufanie) +4:

* V: 
    * Rufus: "Cholera, celowałem do niego i uciekł, chciał mnie zabić. Coś mu odbiło. Mówił o ANnie, że ja ją mu odbiłem. Okej, zalecał się do niej, ale dawno i to była nie wiem szczenięca miłość. Nic do niej nie miał, naprawdę!"
    * Zanim zniknął, postawiliśmy ten skorpion. Mamy plan działania. Wiem gdzie jest Anna!!! W innej jaskini! Michał też to wie, tam będzie!

Kornelia się zsuwa z łychy i przytula się do Seirena: "To nie jest nasz skorpion! Ten jest bezpieczny!!! Jestem uratowana!!!" Kornelia że Antoni WIEDZIAŁ gdzie jechać, że nie dotarli tam. To nie jest to miejsce. Tu chcieli się schować przed ostrzejszą falą burzy. Ale musieli znaleźć miejsce i nagle...

Kornelia tak chodzi by nie stracić Was z pola widzenia. Rufus chce iść z Eustachym.

* Kalia_COR: "Ta wiadomość jest dla Eustachego. Ale... dasz się mi zaprosić na kolację?"
* Kornelia2: "Nie! Benek, nie żyjesz, NIE ŻYJESZ!!!"

Cyprian: nie wiem o co chodzi ale nie jest to zbyt inteligentne... Rufus: "znajdźmy Annę!!!" Kornelia: "Nie! Uciekajmy stąd! Nie wiecie co tu jest!"

Zofia robi (częściowo wstecznie) sekcję zwłok:

Tr Z (Benek a raczej jego zwłoki które można oglądać) +2:

* V: Zofia zauważyła coś ciekawego. Zwłoki Mirki mają POCZĄTKI mumifikacji która się zatrzymała.
* V: (badając Benka) jakaś forma broni energetycznej. Coś przeszło przez scutiera nie dotykając pancerza.

Nowa wiadomość:

* Anna_COR: "Nie wrócę do domu... Kocham Cię, (głosem Anieli: EUSTACHY)..."

Rufus: "ja idę!" Eustachy: "chcesz zminimalizować jej szansę? Nawet nie dojdziesz. Ustalimy co się dzieje... rozumiem, to żona a żona to rodzina, ale musisz się uzbroić w cierpliwość."

Tr (niepełny) Z (jesteś magiem i masz pieprzoną rację) +3:

* V: Rufus podkulił ogon. "Ale Anna, moja kochana Anna!!!"

Zespół wrócił na Seiren. Zofia zaaplikowała Kornelii środki. Na Kornelii nie ma ŚLADU mumifikacji. I Zofia zrelaksowała Kornelię środkami - nie zakłada że Kornelia będzie operacyjna na akcji. Zofia chce ją dać środków by usunąć proces paniki. Łącznie ze stymulantami i "ogłupiaczami".

Tr +3:

* X: Kornelia NADAL boi się być sama i musi widzieć twarz kogoś.
* V: Kornelia jest w stanie odpowiadać na pytania.
* V: Kornelia nadaje się do działania operacyjnego.

Eustachy i ekipa przesłuchuje jeszcze rozmarzoną Kornelię. A Antoni dostał środki na nieprzytomność. Jest nieprzytomny.

Tr Z (środki chemiczne) +3:

* Vz: 
    * Kornelia pamięta jak skorpionem dotarli tu, tu się zatrzymali by przeczekać i mieli kłopoty z pętlami sygnałów
    * Kornelia towarzyszyła Benkowi i weszli gdzieś do jaskini - bo są salvagerami, szukają anomalii. Mieli sprzęt i wszystko.
    * Antoni był coraz bardziej przekonany że tu jest Kamila. Podobno ją słyszał. Ale tylko on.
    * Antoni kazał im zejść i szukać. Podzielił ich.
    * ALE jak tylko Kornelia spotkała Antoniego to on powiedział że nic nie kazał.
* X: Teraz to Rufus jest przekonany że trzeba ratować Annę i na pewno żyje. Bo nie może nie żyć.
* X: whispraith wie o Eustachym.
* V: Kornelia uważa, że Antoni nie dał im tego rozkazu. To "zlepek z różnych komunikatów". 

Aniela szuka danych o śmierci Mirki. Co się tam stało.

Tr Z (robota Michała) +3:

* X: były pewne zakłócenia. Skorpion nie dał rady ogólnie z burzą itp więc nie wszystko jest czyste.
* Vz: Aniela nie ma widoku na twarz Benka stricte. Ale faktycznie to był Benek - Benek chciał wejść a Mirka chciała go odeprzeć.
* V: Zgodnie z chronologią. Opowieść Kornelii itp. Benek już nie żył. To nie był on. To było coś w jego scutierze. A Wy macie scutier z ciałem Benka na pokładzie Seiren.

Cyprian się przestraszył - wypieprzył scutier na zewnątrz.

* Kalia_COR: "Ta wiadomość jest dla Eustachego. (Kornelia)"zginę tutaj... pomocy..." (Kalia) dasz się zaprosić na kolację?"

Rufus: ZAMKNIJ SIĘ (do Kornelii)!!! Nie opuścimy tego miejsca bez Anny! Celuje w Kornelię z plazmowego działa. A Kornelia patrzy na Antoniego i na Rufusa. I na Antoniego i na Rufusa.

* Michał: "Tu Michał! Anna żyje! Mam ją! Jest ranna, potrzebuję jak najszybciej ekstrakcji! (podał koordynaty). Uważajcie na krystalniki!"

Schodzimy do jaskini. Cała ekipa bez Cypriana. Ta jest fatalna - ślisko i Michał się musiał PRZECZOŁGAĆ przez tunel. Kornelia zrobiła mały protest. Eustachy pokazał, że nie może zostać sama. Dotarliście do pieczary. Anna leży, częściowo oparta przy jednym z kamieni.

Zofia dopada do Anny ale Rufus jest szybszy. Dotyka go Michał i transfer energii. Krzyk.

* Gestalt_COR: "Tu Kornelia (Kornelia) | Benek (Mirki) | Antoni (Anny) | Kalia (Kalia)... DASZ SIĘ ZAPROSIĆ NA KOLACJĘ? (głosem Kalii)"

Zofia rzuca granat antymagiczny (lapisowy):

Ex Z (granat szok) +4 + 5Or: 

* XX: Rufus jest nie do uratowania.
* Vr: Zofia rzuciła się, złapała Annę i pchnęła ją w stronę Zespołu.
* Or: Anna została wepchnięta w tunel ale oczy Michała dotknęły oczu Zofii. (compel - uratować Annę)
* Vz: Zofia i Anna są w stanie się wycofać.

Eustachy ryzykuje. Zaklęcie i użyć pochodni plazmowej lancera by spalić whispraitha.

Tr ZM +3 +5Ob:

* Vm: Lancer się przebudził. Pochodnia plazmowa zaśpiewała. Michał i jego servar przestały istnieć. Po chwili wiadomość "Anna..." i servar obrócił lancę przeciw hełmowi i otworzył ogień.

W miarę ostrożnie przeszliście przez tunel i jest droga która prowadzi przez te wszystkie cholerne krystalniki i stromość. Anna, słyszycie głos "co.. co się dzieje... gdzie jestem... gdzie to lustro... burza." "Chodź!" "Zofia?"

Wiadomość od Cypriana: "KURDE BENEK DO WAS IDZIE! PRZEJADĘ GO!" i go przejechał XD.

Przechodzimy przez jaskinię:

Tr +3:

* X: Zofia i Anna się poślizgnęły i zjeżdżają w dół.
* Vr: Aniela i Eustachy złapali Zofię i Annę zanim one się stoczyły.
* X: Zajmuje to upierdliwie dużo czasu by się stąd wydostać. Zwłaszcza z Kornelią która PODEJRZLIWIE patrzy na Annę.
* V: Udaje Wam się zbliżyć do wyjścia jaskini.

Eustachy jest najbardziej zaprawionym w boju weteranem. On jako jedyny ma czas reakcji.

Tr +3 +5Or:

* Vr: Eustachy i Aniela są TUTAJ i padli na ziemię.
* XX: Zofia i Anna i Kornelia wpadły do jaskini i się ześlizgują w dół.

Cyprian: "Nie zbliżajcie się!" /na krawędzi paniki

* V: "to my"

Cyprian tak patrzy... "Eustachy? Aniela?"

OrOrOr: Kornelia jest nieprzytomna i jest zdehermetyzowana, Anna jest połamana a Zofia wylądowała na nich.

Zofia udziela pomocy medycznej Kornelii i Annie. Żadna nie "zjedzie". I Zofia próbuje je powciągać. Sama.

OPERACJA: Cyprian się spuszcza po linie i pomaga wnieść dziewczyny unikając różnego rodzaju krystalników itp, Aniela pomaga z wejściem a Eustachy stoi na straży.

Tr Z (lina, Zofia itp) +3 + 5Og (krystalniki) +5Or (uszkodzenia wynikające z terenu) + 3Ob -3X:

* Or: Cyprianowy scutier nie będzie dobrze działał. Dehermetyzacja, kiepski sprzęt i po prostu się psuje
* Xz: (odraczam +1X)
* Or: Rany są większe niż się wydaje.
* X: Mamy GWARANCJĘ śmierci Cypriana w przepaści.
* Ob: whispraith się budzi.
* Vz: Cyprian wyciąga wszystkich - Zofię, Annę, Kornelię itp. Ale w którymś momencie coś poszło nie tak z liną lub krystalnikami i Cyprian po prostu spadł. Krystalniki na dole.

...

Eustachy, masz wiadomość na priv w komunikatorze.

* "Zaprosić na kolację? Kornelia. Wrócimy do domu!"

Zespół wraca do Seiren. Czas wracać do domu. Seiren wraca do domu. Ale robaki NIE przyleciały. Są: Kornelia, Antoni, Anna. Czyli jedno z nich ma infekcję upiora. Ale kto? I jak to powstrzymać?

Eustachy proponuje przeczekać burzę. Jak się zmniejszy pole magiczne to coś się z tym uda zrobić - whispraith może przestać istnieć.

Zofia próbuje utrzymać Kornelię przy życiu:

Tr Z (masz jednak sprzęt; na tym Rufus i Anna nie oszczędzali) +3:

* Vr: Kornelia przeżyje
* Vr: Kornelia nie będize potrzebowała transplantacji itp.

PODSUMOWANIE: po burzy magicznej faktycznie whispraith przestał istnieć (trzy dni przeczekali). Zabrakło mocy by go utrzymać.

## Streszczenie

...Zespół był zmuszony się rozdzielić, by wykonać wszystkie operacje. Uratowali dowódcę "Uśmiechu" i głównego inżyniera, choć przez rozdzielenie się Upiór Piasków dopadł inżyniera Seiren. Próbując odzyskać inżyniera i znaleźć Annę, Zespół stracił dowódcę Seirena, inżyniera i jednego z salvagerów - ale udało im się zniszczyć fizyczną formę whispraitha (dzięki magii Eustachego), Annę (której szukali) i te dwie wcześniej uratowane osoby z "Uśmiechu". Potem przeczekali burzę w piaskach - nie chcieli, by whispraith zainfekował arkologię. Ogólnie - sukces? Neikatis to jednak mordercza planeta.

## Progresja

* .

## Zasługi

* Eustachy Korkoran: użył zaklęcia by reanimować zniszczony Lancer Rufusa. Dzięki temu zniszczył fizyczną formę whispraitha i samego Lancera (resztką woli Rufusa). Technicznie przejął dowodzenie nad Seiren. Opracował plan - przeczekują burzę, by whispraith się rozproszył.
* Zofia d'Seiren: stabilizuje Kornelię zarówno stymulantami jak i medycznie. Wykryła częściową mumifikację jako efekt whispraitha i wykluczyła infekcję Kornelii na podstawie tego.
* Aniela Myszawcowa: wysyła wiadomości do Kornelii; zagarnęła ją łychą w burzy piaskowej. Doszła do tego, że Benek nie żył, gdy Mirka i Kornelia go widziały. Zauważyła, że dziwne stwory się nie zbliżają, więc na pokładzie Seiren jest whispraith.
* Kornelia Lichitis: widziała whispraitha więc uciekła w burzę; uratowana przez Seiren, ostrzegła ich by nie schodzili. Nie wie CO widziała ale ma histerię. Po ustabilizowaniu przez Zofię, przydatna.
* Rufus Seiren: stracił dużo morale; w starciu z Michałem udało mu się odepchnąć Michała i wziąć go na muszkę. Obsesja na punkcie Anny. Whispraith go wyssał, ale napełniony (po śmierci) energią Eustachego zniszczył swój servar by nie krzywdzić nikogo innego.
* Anna Seiren: częściowo katatoniczna przez działania i ekstrakcję whispraitha; przynęta. Zofia ją wyciągnęła ryzykując życie swoje i zespołu.
* Cyprian Kugrak: wpadł w lekką panikę, ale dalej trzyma fason; pomaga wszystkim najlepiej jak umie. Wypieprzył scutier Benka poza Seiren a potem go przejechał. Halucynacje przez whispraitha. Uratował dziewczyny, które wpadły do jaskini - ale swoim kosztem.
* Michał Uszwon: whispraith go Dotknął; zaczął starcie z Rufusem i uciekł w piaski. Whispraith pozwolił mu dotrzeć do Anny i go opętał.
* Antoni Grzypf: wydrapane oczy, pieści zwłoki Benka i myśli że to Kamila. Mimo szczelności Scutiera. Wyciągnięty i uratowany przez Seiren, mimo Dotyku whispraitha.
* JAN Seiren: przejechał reanimowanego whispraitha, przeprowadził (kosztem dużych uszkodzeń) operację ratunku Anny, Kornelii i Antoniego. Acz tracąc Rufusa, Cypriana i Michała podczas operacji.
* JAN Uśmiech Kamili: nadal zniszczony, został na Szepczących Wydmach jako wrak i ruina.

## Frakcje

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Nativis, okolice
                        1. Szepczące Wydmy: znajduje się tam sporo podziemnych niebezpiecznych jaskiń. Znajduje się tam też whispraith polujący na nieopatrznych wędrowców.

## Czas

* Opóźnienie: 1
* Dni: 4

