## Metadane

* title: "Broszka dla eternianki"
* threads: młodosc-martyna
* gm: kić
* players: żółw

## Kontynuacja
### Kampanijna

* [210904 - Broszka dla eternianki](210904-broszka-dla-eternianki)

### Chronologiczna

* [210904 - Broszka dla eternianki](210904-broszka-dla-eternianki)

### Plan sesji
#### Co się wydarzyło

.

#### Sukces graczy

* 

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Martyn ma około 19-21 lat.

Znajduje się w Klamży. Klamża nie jest miejscem światowym, łatwo się w niej znudzić. Ale jest tam tor wyścigowy i knajpa "Wygrana" - jedyna większa knajpa w Klamży i tam odbywają się po wyścigach imprezy itp. Właściciele toru bardzo próbują z toru zrobić coś większego, ale na razie średnio wychodzi.

A Martyn regularnie odwiedza Wygraną. Bo tam są dziewczyny i możliwość zabawy. Laski przede wszystkim "hej" do ludzi co się dobrze ścigają, ale niektóre zauważają Martyna (zwłaszcza jego byłe).

Od tygodnia wpadła Martynowi w oko taka jedna, dość egzotyczna. Ciemnawa skóra, lekko skośne oczy, włosy z odcieniem fioletu..? Ciemne. Z jednym zielonym pasmem. Przychodzi raczej sama, jest otoczona grupą ludzi ale nie jako centrum a po prostu jest znana / lubiana. Przez ostatni tydzień Martyn miał inne cele i na czym oko zawiesić, ale ta siedzi przy barze, sama w kącie.

Martyn postawił jej brandy. Przedstawiła się jako Kalina Rota d'Kopiec. Własność pani w Eterni. Szuka broszki w kształcie krokodyla, konkretnej. Podróbka nie wchodzi w grę. Należy do żony właściciela toru. Kalina nie wie, czemu Jolancie Kopiec tak na tej broszce zależy, ale jej zażądała. A czego żąda, to dostaje. Martyn spytał, czy mają jakiś limit czasowy. Tak - TERAZ mają tydzień.

Dla Martyna ta broszka krokodyla i jej właściciele są "za wysoko". Raczej ma kontakty wśród pilotów, nie wśród właścicieli torów. Szybki sweep - on jest kimś kto tor ma, dba o niego i próbuje go rozwijać. Ona robi za pierwszą damę która firmuje swoją osobą i dodaje szyku. Tacy rozsądni bogacze co umieją inwestować. I nie, Martyn nie zwrócił uwagę na broszkę u żony właściciela. Raczej na biust.

Martyn ma już kilka planów:

1. zdobyć broszkę, oddać ją Kalinie --> tien Kopiec
    1. wymiana za inną broszkę? Coś egzotycznego?
    2. znaleźć fałszerza i wymienić na duplikat używając magicznego szczura?
2. uwolnić Kalinę zmieniając jej Wzór (nie wie jak, ale KTOŚ wie)
3. pomóc Kalinie tak, by tien Kopiec widziała jej wartość. Np. skonfliktować tien Kopiec z jakimś Sowińskim.

.

* Martyn: "Tien Kopiec - oby żyła wiecznie i się szybko starzała", polując na uśmiech ze strony Kaliny.

Pierwsze pytanie do Kaliny - czy ma egzotyczną biżuterię. Czyli coś co na tym terenie nie jest częste. Coś, co może zainteresować właścielkę toru (Dagmarę, żonę Karola o nazwisku Kamyk). 

Martyn nie jest ekspertem od biżuterii - musi załatwić przyjaciółkę. Aktorka, taka której trochę nie idzie, która nie widzi przyszłości i ma oko do strojów, mody itp. Owa dama próbuje grać w Cieniaszczycie po czym wraca do Klamży i łka w poduszkę. Chciałaby być celebrytką, ale tymczasowo tańczy na zlecenie i zabawia dzieci na imprezach. A imię jej Ola Klamka.

Ola jest smutna, bo znowu nie dostała roli. Pomoże Martynowi w wybraniu najciekawszej biżuterii która by pasowała do jego mrocznego planu. A Martyn w duchu obiecał sobie, że zdobędzie dla Oli jakąś rolę - choć jedną szansę.

Martyn wie, że Dagmara ubiera się nie tyle wyzywająco co "nie ukrywa kobiecości" - odważny i gustowny ubiór. I te dane przekazuje Oli. Plus zdjęcia z wyścigów, z gazet, inne rzeczy tego typu. Też Ola, jako aktorka wie w jakich kręgach się Dagmara musi poruszać i wie co tam jest znane i lubiane. Na podstawie tego maksymalizujemy (Ola maksymalizuje) egzotyczną biżuterię dostępną Kalinie pod kątem Dagmary. (ExZ+3).

ZA MAŁO! Martyn zdecydował się zrobić HISTORIĘ dookoła biżuterii Kaliny. Jako, że część z tych kamieni, biżuterii (np. łańcuszek o bardzo specyficznym wzorze pojedynczych ogniw) itp. jest bardzo elegancka i służy do tego by Kalina była reprezentatywna (stanowi biokońcówkę hazardową dla tien Kopiec), to Martyn do tego dobudowuje opowieści i historie używając znajomych artystów. Np. "ten łańcuszek symbolizuje X, Y i był używany na dworze Eterni", po czym linia --> artyści a docelowo --> nie najpotężniejsze gazety modowe ale _jakieś_. A jak inni wiedzą co to za kreacje, wartość kreacji po prostu wzrośnie.

Czyli - Kalina maksymalnie eterniańsko ubrana, w pięknym stroju, z sekwencyjnie różnym uzbrojeniem biżuterii + fotografie + opowieści.

Martyn --> znajomi artyści. Oni mają konflikt. Cel: zbudować historie, potem do gazet by --> Dagmara POŻĄDAŁA tych kreacji. Widziała ich korzyść.

ExZ+3+3O:

* X: lokalne siły modowe się "zbroją" przeciw wejściu Kaliny, eternijskiej baronetki modowej
* O: jest zainteresowanie nową modą, ale terminusi zwracają uwagę na symbole zależności tien Kopiec
* X: przynajmniej część rzuca się na symbole zależności od tien Kopiec. Kalina protestuje, ale lokalsi robią tego podróbki. Martyn chichocze.
    * to zwłaszcza jest "blood ruby" - kamień wpinający się w system krwionośny człowieka.
    * taki poddany z Rubinem Eterni ma wysokie uprawnienia, ale jego pan może go zabić i sterować rozkazem.
    * takiego Rubinu nie da się zdjąć nieumiejętnie, bo zaczynasz krwawić. I działa nawet po zdjęciu.
    * oni tutaj nie wiedzą o tym jak działa taki Rubin, więc mają podobnie wyglądające podróbki... z symboliką tien Kopiec XD.
* Vz: udało się pozycjonować Kalinę jako "certyfikowaną eterniankę od mody i biżuterii", która zna się na tym. Ok, są podróbki - ale KALINA jest tą, z którą warto gadać i która opowie, która jest tą "najlepszą". To podnosi jej wartość (i zwiększa jej ryzyko)

Martyn zaciera ręce. Dagmara będzie bardzo zainteresowana. A Kalina jest tymczasową celebrytką - więc Martyn ma tymczasową przewagę. Ciąg dalszy planu - niech Kalina spotka się z Dagmarą i wymienią się na kreacje - broszka za coś ładnego. Martyn jako wsparcie rzeczoznawcze - w rzeczywistości jest magiem, więc spróbuje ocenić czy to jest magiczne czy nie. Jak magiczne, to może nie zadziałać. Jak niemagiczne, to czar Martyna + uśmiech Kaliny + przekupstwo i okazja do pokazania się na dworze _powinny_ zadziałać. I robimy to w dniu wyścigów i Kalinę ubieramy w full epic mode.

Martyn + Kalina --> Dagmara, cel: przekonać Dagmarę do wymiany broszki na kreację. Okoliczności: kreacje są popularne, Dagmarze się podobają, Kalina jest tymczasową celebrytką.

ExZ+3 --> TrZ+3

* X: Ta broszka jest czymś, czego NIGDY nie założyłby arystokrata Aurum. To dla mieszczaństwa. Więc Jolanta Kopiec nie będzie zadowolona (+zły humor). A my o tym nie wiemy ^^
* V: Kalina ma swoją broszkę. Broszka jest całkowicie niemagiczna. Zwykła broszka, nie mająca żadnych rodzinnych konotacji dla Dagmary.
* X: Kalina była tu wysłana jako akt niełaski; to znaczy, że jej możliwości zdobywania zasobów są bardzo ograniczone. Mamy popyt bez podaży.
* V: Dagmara jest zadowolona z wymiany

Kalina ma broszkę. Oddała ją Jolancie Kopiec (od ręki). Tien Kopiec jest wystarczająco zadowolona... do momentu aż jej kuzyn ją wyśmiał. (następny dzień) Wtedy Kopiec powiedziała Kalinie, że jest bardzo niezadowolona i odcina ją od biżuterii z Eterni. Nie dała sobie wyperswadować, że Kalina zrobiła WSZYSTKO co jej kazano - Kalina nawet nie próbowała się bronić. Ale Kopiec zaakceptowała pozytywny koniec misji Kaliny. She is not unreasonable.

Martyn ma jeszcze trzy ważne plany:

1. Przespać się z Kaliną. Za broszkę "zrobi wszystko co on chce". Dostała broszkę i nawet została na tym terenie :3.
2. Zdobyć Oli jakąś rolę, najlepiej przy użyciu biżuterii Kaliny i jej rekomendacji. Potem jak się uda - przespać się z Olą.
3. Odbudować kanał biżuterii póki jest popyt. Część pieniedzy odłożyć, rozpocząć pracę nad uwolnieniem Kaliny. Najlepiej za pieniądze tien Kopiec.

Oki - nie mamy biżuterii? Martyn zdecydował się na kontakt z tien Kopiec. Prezentuje się jako osoba, która chce zająć się handlem biżuterią dla tien Kopiec, korzystając z okazji, że jest w tej chwili na to popyt. Kalina przedstawia ten kontakt adekwatnie a Martyn próbuje być odpowiednio miły. Plus uczył się etykiety u Kaliny. Wideorozmowa.

* Jolanta: "Znalazłeś dla Kaliny zastosowanie?"
* Martyn: "Jest egzotyczna, jako modelka Kalina radzi sobie doskonale przy sprzedaży biżuterii"
* Jolanta: "Chcesz więcej?" (w domyśle: egzotycznych dam w służbie tien Kopiec takich jak Kalina)
* Martyn: "..." - totalny szok i zgubienie myśli.

Konflikt jest o coś innego. Jak duże zasoby przeznaczy Jolanta Kopiec bo jest _amused_ oraz o poziom niełaski Kaliny. Czyli - jeśli wszystko się popieprzy, to co dalej? I trzeci aspekt - ile zdążymy ugrać zanim wszystko pójdzie w cholerę.

ExZ+2:

* X: tien Kopiec nie jest fanką Martyna. Do kontaktów z nią - Kalina. Nie chce już nigdy z Martynem rozmawiać. Plus - Martyn odrzucił jej ofertę większej ilości egzotycznych lasek ;-)
* Vz: tien Kopiec zapewni wystarczające zasoby na sfinansowanie tego wszystkiego i Kalina może odbudować biżuterię
* V: tien Kopiec _wie_ że to jest sezonowe. Nie zamierza wykorzystywać tego przeciwko Kalinie. Wie, że to się spieprzy. Niech Kalina jeszcze tu zostanie, jest ciekawie i "jest egzotyczna".
* Vz: zdążą ugrać tyle, że mają na ważne łapówki i życie na pewien czas. 

Trudny dylemat - Kalina jest piękna i chętna, ale biżuteria TERAZ jest gorąca. Plus, będzie dostawa. Martyn chciałby z Kaliną nie wychodzić z łóżka, ale Ola... ech. No cóż, dla Oli się poświęci. Wyjdzie z łóżka. Ale dwie godziny od poranku... ;-).

Plan Martyna na Olę jest prosty:

1. Kalina pożyczy Oli coś fajnego i dostarczy sztukę eternijską.
2. Martyn znajdzie miejsce które chciałoby egzotyczną sztukę i warunkiem udostępnienia jej jest użycie Oli w jakiejś ważniejszej roli. Skoro mamy modę na Eternię...
3. Po tym, jak Ola będzie mieć powodzenie, Martyn się z nią prześpi. Najważniejszy punkt planu.

Martyn --> odnaleźć i pozyskać takie miejsce dla Oli w Cieniaszczycie. Łazić od teatru do teatru. I proponować sztukę z Olą. Pełne przygotowanie na gadanie. Pokazanie nagrań od Kaliny jak to jest interpretowane. Plus - patronat Kaliny. "To jej ulubiona sztuka, chodzi tu, blablabla". Do zachęcenia teatrów i potencjalnych widzów. A Ola ma się z nagrań nauczyć danej roli, która jest znacząca i fajna. Nie pomaga fakt, że Ola ma reputację zdesperowanej aktoreczki od tańczenia i dawania dzieciom lizaków...

ExZ+3+3O (zagrożenie dla Oli albo Kaliny):

* XX: Nie zatrudnili Oli na stałe - tylko po to, by móc wystawić sztukę. Martynowi się FUNDAMENTALNIE nie udało.
* X: Walnie hasło o symbolach poddaństwa tien Kopiec i w ogóle w środku spektaklu. Kalina się musi ewakuować, Martyn z nią musi zwiać.
* X: Fad na eternijską biżuterię niestety się skończył przez to wszystko. Kalina została z Martynem w Klamży. Nie mają nic. Nie zyskali, nie stracili.

...przynajmniej Ola i Kalina nie są zagrożone...

## Streszczenie

Kaprys eternijskiej arystokratki, która chciała pozyskać broszkę i zażądała tego od swojej wygnanej służki splótł losy owej służki z Martynem Hiwasserem. Ten od razu zwąchał okazję do założenia HANDLU BIŻUTERIĄ EGZOTYCZNĄ! Plan był niezły - ale ktoś podrobił część biżuterii (krwawe rubiny), dzięki czemu sygnał poddaństwa wobec tien Kopiec stał się ozdobą. Gdy Martyn i Kalina się z tego wyplątywali, nie osiągnęli ogromnego majątku - ale przynajmniej nikomu nic się nie stało.

## Progresja

* Kalina Rota d'Kopiec: wyszła z niełaski Jolanty Kopiec; zostaje dalej na tym terenie. Rozpoznawana jako egzotyczna eternijka.

### Frakcji

* .

## Zasługi

* Martyn Hiwasser: 20 lat. Chcąc pomóc dwóm dziewczynom zrobił potężny popyt na biżuterię eternijską co sprawiło, że część ludzi zaczęła replikować symbole poddaństwa Jolanty Kopiec. Pomógł trochę, ale przekroczył swoje umiejętności - nie został krezusem eternijskiej mody. Ale Kalina dostała broszkę i wszystko dobrze się skończyło.
* Kalina Rota d'Kopiec: tajemnicza femme fatale dla Martyna; dzięki działaniom Martyna uzyskała broszkę dla swojej pani. Nie chce wolności od Jolanty Kopiec. Przez moment bardzo popularna jako egzotyczna eternijka dzięki Martynowi.
* Dagmara Kamyk: właścicielka Toru Wyścigowego w Klamży; wymieniła starą broszkę na eternijskie ozdoby dzięki czemu wzrosła w popularności. Kobieca, troszkę próżna, ale dobrotliwa. Trochę oczarowana przez Martyna. Trochę.
* Ola Klamka: przyjaciółka Martyna; nie dostała wymarzonej roli. Pomogła Martynowi wybrać maksymalnie skuteczną biżuterię dla Kaliny by zrobić z niej baronetkę mody. Niestety, ma złą reputację jako aktorka i mało talentu.
* Jolanta Kopiec: eternijska arystokratka, która chciała mieć broszkę bo miała kaprys; nie jest zła, tylko rozpuszczona. Nie jest fanką Martyna, ale poszła na jego plan by Kalina mogła trochę się pobawić.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Wojnowiec
                            1. Klamża
                                1. Tor Wyścigowy: własność Karola i Dagmary Kamyk; centrum kulturalne Klamży.
                                1. Knajpa Wygrana: centrum dowodzenia Martyna w Klamży; tu poluje na dziewczyny i opracowuje plany jak pomóc nieznajomym i znajomym ;-).
                        1. Przelotyk Wschodni
                            1. Cieniaszczyt
                                1. Teatr Posępny: niezbyt respektowany lokal; przyjął Olę po namowach Martyna by móc wystawić modną eternijską sztukę.

## Czas

* Chronologia: Inwazja Noctis
* Opóźnienie: -1834
* Dni: 11

## Inne

.

## Konflikty

* 1 - Martyn --> znajomi artyści. Cel: zbudować historie, potem do gazet by --> Dagmara POŻĄDAŁA tych kreacji. Widziała ich korzyść.
    * ExZ+3+3O
    * XOXVz: lokalni modziarze niezadowoleni i się zbroją; podrabiacze robią podróbki zależności od tien Kopiec; terminusi niezadowoleni z symboli zależności; Kalina pozycjonowana na tymczasową celebrytkę i go-to odnośnie eternijskiej biżuterii
* 2 - Martyn + Kalina --> Dagmara, cel: przekonać Dagmarę do wymiany broszki na kreację. Okoliczności: kreacje są popularne, Dagmarze się podobają, Kalina jest tymczasową celebrytką.
    * ExZ+3 --> TrZ+3
    * XVXV: broszka jest czymś czego NIGDY nie założy arystokrata Aurum; Kalina zesłana tu jako akt niełaski; Kalina ma broszkę; Dagmara zadowolona z wymiany
* 3 - Martyn -> Jolanta: jak duże zasoby przeznaczy Jolanta Kopiec bo jest _amused_ oraz o poziom niełaski Kaliny. Czyli - jeśli wszystko się popieprzy, to co dalej? I trzeci aspekt - ile zdążymy ugrać zanim wszystko pójdzie w cholerę.
    * ExZ+2
    * XVzVVz: Kopiec nie jest fanką Martyna, nie chce z nim rozmawiać a z Kaliną; da zasoby na odbudowanie biżuterii Kaliny; Kopiec WIE że to sezonowe i nie ma oczekiwań; zdążą ugrać tyle że mają na łapówki i życie i zabawę ;-)
* 4 - Martyn --> odnaleźć i pozyskać takie miejsce dla Oli w Cieniaszczycie. Łazić od teatru do teatru. I proponować sztukę z Olą.
    * ExZ+3+3O (zagrożenie dla Oli albo Kaliny)
    * XXXX: Ola zatrudniona tylko na czas sztuki; wyszło o symbolach poddaństwa tien Kopiec; Martyn i Kalina nie mają nic godnego w Klamży
