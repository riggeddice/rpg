## Metadane

* title: "Problematyczna Elena"
* threads: triumfalny-powrot-arianny
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [240103 - Perfekcyjne wykorzystanie unikalnych zasobów](240103-perfekcyjne-wykorzystanie-unikalnych-zasobow)
* [221122 - Olgierd, łowca potworów](221122-olgierd-lowca-potworow)

### Chronologiczna

* [240103 - Perfekcyjne wykorzystanie unikalnych zasobów](240103-perfekcyjne-wykorzystanie-unikalnych-zasobow)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* .

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Punkt zero

Wszystko zaczęło się od Leszka Kurzmina. Nie był w stanie poradzić sobie z panną Eleną Verlen - wysłał ją więc do Arianny. Osoby, która powinna sobie poradzić. Jej kuzynką.

## Misja właściwa

Ćwiczenia. Ćwiczenia manewrowania niedaleko Kontrolera Pierwszego, z ostrym ogniem (acz obniżonej mocy). Arianna daje radę - wszystko rozwiązuje zgodnie z dobrymi praktykami ćwiczeń, nie pozwala nikomu się szczególnie wychylać. CHCE SIĘ POPISAĆ RZECZOWOŚCIĄ PRZED ADMIRAŁEM CHOCIAŻ RAZ!

Nie było jej dane - Infernia dostała sygnał "przylatuję". Mała jednostka, pinasa, z nowym członkiem załogi. Arianna aż sprawdziła manifest - to jest potwierdzone. Ale niefortunny moment. Pilot - Elena - powiedziała, że bez problemu wyląduje na Inferni, Arianna nawet nie musi przerywać ćwiczeń. (XXXV). Tak powiedziała, ale hangar Inferni się rozsypał gdy uszkodzona ogniem lasera pinasa wpadła na dużej prędkości, dewastując wnętrze Inferni...

Elenie nic się nie stało. Wygrzebała się z wraku pinasy i poszła do Arianny się odmeldować. A dowcip polega na tym, że Arianna jest arystokratką Aurum i Elena też - znają się jeszcze z dzieciństwa (Elena jest tą młodszą). To też sprawia, że Arianna wie jak ją wypytać. Nacisnęła Elenę - która wyraźnie nie jest zadowolona z przeniesienia z Castigatora na Infernię - i udało jej się dojść do tego o co Elenie chodzi i co ją boli:

* Elena pracowała nad szlachtą na Castigatorze od dawna. Bardzo dawna. Udało jej się ich sterroryzować. PRAWIE to miała. A potem wpada Arianna i robi porządek.
* Teraz Elena mogłaby skończyć pracę na Castigatorze, ale została przeniesiona przez Kurzmina.
* Co gorsza, Castigator ucierpiał. Przez Eustachego. I teraz ona ma być pod tym niekompetentnym Eustachym?!
* Elena wykaże Ariannie, że jest najlepsza i zostanie pierwszym oficerem w miejsce tego psa, Eustachego.
* Elena wierzy w zasady, regulaminy itp. A wyraźnie Arianna i ekipa mają to gdzieś.

Arianna tylko pokręciła głową i kazała kuzynce odmeldować się do Eustachego, naiwnie myśląc, że przecież to nie ma jak skończyć się tragedią.

Elena spotkała się z Eustachym. Niezbyt jej wychodziło ukrywanie wrogości i to, że uważała go za dość niekompetentnego. Eustachy zaproponował jej pojedynek - niech się okaże kto jest lepszy. Nadpisał TAI Persefonę, że teraz robią ćwiczenia tego typu, Elena ma reperować a on ma sabotować.

Przerażające jest to, że Elena serio genialnie sobie daje radę.

* V: Pokazał jej, że jest niezły
* XXXXX: Ale Elena jest ZA DOBRA. Nie jest w stanie jej pokonać, nie jest w stanie sabotować szybciej i lepiej niż ona blokuje bez pójścia na ostro. Dewastacja Inferni.
* OO: "Tylko geniusz panuje nad chaosem". Eustachy zaczął niszczyć Infernię NA SERIO. Elena, przerażona, zaczęła niszczyć elementy Inferni by kaskada nie poszła do reaktora i Persefony.
* O: Elena przestała podskakiwać, święcie przekonana, że Eustachy jest SZALEŃCEM który dla pokazania jej kto "ma rację" zniszczy statek.

A tymczasem Arianna widzi, że jej statek przestaje działać. Przejęła kontrolę nad sterami, ale nawet ona nie była w stanie powstrzymać dryfu (jak powoli zaczyna się rozpadać struktura statku). Infernia wyrżnęła w OO Żelazko, uszkadzając lekko pancerz (i wybijając Żelazko z terenu prosto pod ogień; szczęśliwie pilot Żelazka utrzymał kurs). Arianna dała jednak radę przejąć kontrolę nad systemami Inferni i wyprowadziła ją z terenu ćwiczeń, wysyłając sygnał "awaria, ale pod kontrolą".

I wtedy Persefona przejmuje dowodzenie. Odcina Ariannę, Eustachego, WSZYSTKICH od systemów Inferni. Persi włącza tryb maksymalnie defensywny; coś bardzo złego dzieje się na pokładzie. Klaudia natychmiast próbuje połączyć się z Persefoną; (VVV) - udało jej się, statek jest oddany Ariannie, ale Persefona (X) ma tendencje do "może jednak JA wiem lepiej".

Oki. Co tu się DZIEJE? Arianna poprosiła Leonę na mostek - potrzebna jest siła ognia.

Tymczasem Elena zaczęła nową kłótnię z Eustachym - zauważyła, że WSZYSTKIE komponenty, mierniki, elementy są przesterowane i podkręcone. Wszystko. Niezgodnie z regulaminem. Eustachy z absolutnym niedowierzaniem - że do czego ona się doczepia? Dzięki temu Infernia lata lepiej. Elena zauważa, że takie podkręcenie powiększa koszty napraw. Eustachy się zaśmiał, układając materiały wybuchowe pod drzwi by wyjść z zamkniętej sekcji Inferni...

...i na to weszła Leona z Arianną. Eustachy próbował schować materiały wybuchowe, ale nie do końca mu wyszło. Leona z radością pokazała je palcem Ariannie. Arianna opieprzyła Eustachego jak psa, od góry na dół, zagrażając mu degradacją jak nie umie się opanować na Inferni. Jakaś zwykła Elena nie powinna zakłócić funkcjonowania Inferni - a na pewno nie przez NIEGO.

Opiernicz ze strony Arianny został przerwany. Klaudia prosi ją na mostek. Admirał Kramer chce rozmawiać z Arianną.

Kramer chciał się dowiedzieć, czy z Infernią wszystko OK. Tak, Arianna potwierdziła - miała małą awarię ale opanowała. Kramer powiedział jej, że NA SZCZĘŚCIE dowódca Żelazka nie zgłosił awarii; uznał, że Arianna po prostu nie umie pilotować, ale takie rzeczy się rozwiązuje między dowódcami. Arianna zacisnęła zęby, ale nic nie powiedziała - nie miała co powiedzieć.

Kramer powiedział Ariannie, że zniknął Aurelion - koloidowy statek badawczy; bardzo szybki i pancerny, wysokiej klasy. Badał w okolicach Anomalii Kolapsu anomaliczne ŁZy i stracili z nim kontakt. Teraz - czemu Arianna? Bo Infernia ma zaawansowane systemy detekcyjne; poradzi sobie ze znalezieniem Aureliona.

Arianna spojrzała ponuro na swoje podsystemy. Oczywiście, sensory nie działają. Arianna powiedziała, że potrzebne jej są szybkie, rutynowe naprawy na Kontrolerze. Kramer się zgodził - Arianna ma dzień, Aurelion nie powinien mieć szczególnych problemów. Arianna ruszyła do Kontrolera, kazała Eustachemu i Elenie przygotować refit; nieważne jak, mają NAPRAWIĆ. A Klaudia jak najszybciej ma znaleźć jakieś sensory - dyskretnie.

Klaudia ruszyła do biurokracji. Znalazła sensory w jednym z magazynów należących do zupełnie innej grupy Orbitera; pomanipulowała dokumentami i przenieśli te sensory do Hangarów Alicantis. Niestety, sensory są trochę wadliwe - są anomaliczne, pokazują rzeczy których nie ma i negatywnie wpływają na psychotronikę Persefony. No cóż. Nie ma innej opcji - to jest to co mają. Nie będzie nic lepszego.

Następnego dnia, Arianna ruszyła Infernią w kierunku Anomalii Kolapsu. Używając anomalicznych sensorów COŚ znalazła, ale jak to interpretować?

* V: Znalazła Aureliona, lecącego z Obłoku Lirańskiego w kierunku na Telirę i tamtejszą stację górniczą kryształów ixiackich.
* X: Persefona przeszła w pełen tryb bojowy
* V: Arianna utrzymała kontrolę nad Persefoną. Wzmocniła tarcze i pola siłowe widząc, że dwie ŁZy lecą w ich kierunku (najpewniej nieprawdziwe)
* V: Klaudia użyła swojej magii i wzmocniła się sensorami. Te ŁZy są prawdziwe! Tam jest anomalia kosmiczna, anomalny statek!
* Vm: Klaudia wykryła to jako - kiedyś - OO Salamin. Teraz - Anomalia Kosmiczna Salamin.
* O: Arianna kazała Eustachemu uploadować Persefonie informacje, że jest statkiem z okresu wojny. "Tafakari". Niestety, Persefona d'Infernia... uwierzyła w te dane i odcięła wszystkich.

Zespół słuchał z czymś pomiędzy zażenowaniem i zaciekawieniem rozmów pomiędzy dwoma TAI - "Salamin" poinformował, że jego kapitan, Lidia, jest śmiertelnie ranna i załoga została wsadzona do podów. Salamin złapał noktiański statek (czyli OO Aurelion), ale Lidia nie życzyłaby sobie jego śmierci. Ale Salamin leci w kierunku na bazę noktian - musi ją zniszczyć za wszelką cenę. A "Tafakari", czyli Persefona d'Infernia oznajmiła, że jej oficerowie nie żyją i ma zamiar wesprzeć Salamin w misji.

Niestety, podczas nawiązywania połączenia, Infernia się uszkodziła; magia lekko przeładowała systemy (Eustachy i Elena poważnie zniszczyli Infernię). Coś co miało być rutyną przekształciło się w dewastację. A obie TAI są w przeszłości, pochłonięte wojną, która od dawna już nie istnieje.

Klaudia spróbowała zrobić wszystko co w jej mocy by odzyskać kontrolę nad Persefoną.

* X: Persefona odcina Ariannę i Klaudię. Chce wysłać sygnał do Salamina.
* XX: W odpowiedzi, Klaudia wypala Persefonie psychotronikę i komponenty. Minimum Viable Persefona. Ale moja.
* V: Udało się Klaudii odzyskać kontrolę, acz Infernia - i Persefona - widziały lepsze czasy.

Arianna stanęła przed bardzo skomplikowanym problemem. Infernia nie jest w stanie walczyć z Anomalią Kosmiczną Salamin - Salamin był kiedyś statkiem bojowym, krążownikiem. Uszkodzona Infernia nie ma żadnych szans z AK Salaminem, i to nie licząc Aureliona (którego Arianna chce uratować). Tak więc Arianna poszła na fortel - kazała Persefonie zaproponować Salaminowi, żeby on poleciał tam a ona weźmie ludzi z pokładu Salamina oraz z Aureliona. Lidia by przecież nie chciała ich śmierci.

Salamin, o dziwo, dał się przekonać. Wystrzelił kapsuły ratunkowe, Infernia je przyciągnęła. Martyn ostrzegł, że z niektórych sygnał jest taki, że potrzebny będzie _coup de grace_; Arianna używając magii utrzymuje kapsuły na orbicie Inferni, choć sama Infernia straciła resztki manewrowania i sprawności. Reaktor prawie wygasł, przeciążony.

W którymś momencie Salamin się zorientował, że nie ma do czynienia z Tafakari. Przygotował ŁZy. Arianna natychmiast wysłała wszystkie IFF oraz sygnały, historię, informację do Salamina. Wygrali. Wyjaśniła, czym są i czym Salamin się stał. Użyła tych cholernych anomalicznych sensorów by trafić do Salamina - użyła ich anomaliczności przeciw Salaminowi.

Persefona d'Salamin zrozumiała. Jej misja skończona. Poprosiła o rozkaz samozniszczenia.

Jej ostatnie pytanie - bardzo zresztą dziwne - było: "Czy Persefony zostały wreszcie zaakceptowane jako to czym są?". Arianna, nie wiedząc o co Persefonie chodziło, potwierdziła.

Salamin wszedł w samozniszczenie poza zasięgiem Inferni. A Infernia, ciężko uszkodzona (acz nie przez Salamin) wróciła do bazy. Teraz ma pretekst do napraw po harcach Eustachego XD.

## Streszczenie

Na Infernię trafiła nowa podoficer - podporucznik Elena Verlen, z Castigatora. W konkursie z Eustachym prawie zniszczyli Infernię. Niedługo potem Infernia (podłatana) poleciała do Anomalii Kolapsu uratować Aurelion, który natknął się na anomaliczny kiedyś-krążownik Orbitera Salamin. Arianna uratowała kogo się da i skłoniła Salamin do samozniszczenia; Persefona d'Salamin miała jeszcze dość psychotronicznej świadomości i miłości (?) do swojej martwej kapitan.

## Progresja

* Arianna Verlen: opinia nie-takiej-kompetentnej-jak-się-zdawało u wielu oficerów floty Orbitera widzących ćwiczenia. Czy nie kontroluje załogi czy statku, kto wie?

### Frakcji

* .

## Zasługi

* Arianna Verlen: przekonała Salamin, że jego czas się już skończył. Po przyjęciu Eleny przekazała ją Eustachemu, co prawie skończyło się zniszczeniem Inferni (gdyby nie Persefona).
* Klaudia Stryk: rozpaczliwie znalazła sensory (anomalne) jakie pasują do Inferni a potem wykazała się nadludzkimi umiejętnościami kontrolując i wypalając błędy w Persefonie. Magią znalazła Salamin.
* Eustachy Korkoran: wszedł z Eleną w pętlę kłótni o kompetencje i prawie wysadził Infernię by jej pokazać, że jest lepszy. Potem wykazał się rozpaczliwymi umiejętnościami naprawy statku.
* Leszek Kurzmin: dowódca Castigatora; wysłał Elenę do Arianny jako załogantkę. A Kramer zaakceptował, bo Elena miała wszystkie papiery w porządku i nieposzlakowaną opinię.
* Elena Verlen: alias Mirokin; kompetentny pilot, automatycznie używająca swojej magii. Ma kij w tyłku i uważa zespół Inferni za niekompetentny. Wysłana na Infernię przez Leszka z nadzieją, że Arianna sobie poradzi.
* Antoni Kramer: admirał konsekwentnie osłaniający Ariannę; wysłał Ariannę na misję ratowania Aureliona przed Salaminem (o czym nie wiedział).
* Olgierd Drongon: dowódca Żelazka, niedźwiedziowaty, kompetentny i niebezpieczny. Ma agonalnie niską opinię o Ariannie - nie poradziła sobie z pilotażem na ĆWICZENIACH.
* Persefona d'Infernia: poważne uszkodzenia psychotroniczne spowodowane przez Klaudię i anomalne sensory sprawiły, że ma cechy paranoiczne. Musiała przejąć kontrolę nad Infernią by Eustachy jej nie zniszczył.
* AK Salamin: kiedyś krążownik Orbitera i carrier ŁeZ, teraz Anomalia Kosmiczna. Żyje przeszłością; porwał Aureliona i chciał niszczyć "noktian". Arianna wydała mu rozkaz samozniszczenia, który spełnił.
* OO Żelazko: lekka kanonierka o dużej sile ognia; została solidnie uszkodzona na ćwiczeniach przez Infernię (przez pojedynek Eustachego i Eleny).
* OO Aurelion: koloidowy statek badawczy; bardzo szybki i pancerny, wysokiej klasy. W Anomalii Kolapsu znalazł AK Salamin, obudził jego ŁZę - i Salamin "porwał" Aurelion.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy
                    1. Hangary Alicantis
            1. Libracja Lirańska
                1. Anomalia Kolapsu

## Czas

* Opóźnienie: 10
* Dni: 5
