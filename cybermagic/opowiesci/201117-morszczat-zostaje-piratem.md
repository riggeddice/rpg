## Metadane

* title: "Morszczat zostaje piratem"
* threads: legenda-arianny
* gm: żółw
* players: kić, darken

## Kontynuacja
### Kampanijna

* [201104 - Sabotaż Świni](201104-sabotaz-swini)

### Chronologiczna

* [201104 - Sabotaż Świni](201104-sabotaz-swini)

## Punkt zerowy

### Dark Past

_odsyłam do 200916_

### Opis sytuacji

Pas Omszawera. Na obrzeżach Sektora Astoriańskiego. Dwa tygodnie lotem statku kosmicznego z Kontrolera Pierwszego. I komodor Morszczat potrzebuje pomocy swoich najbardziej zaufanych agentów i przyjaciół, którzy byli z nim tam od zawsze...

### Postacie

Frakcje: i reprezentanci

* Zbigniew Morszczat, komodor w rozpaczy
  * chce: zniszczyć wszystkich noktian, wyplątać się z sytuacji
  * ma: pieniądze, statki i przyjaciół
* Persefona d'Amientor, autonomiczny TAI-dowódca kolonii
  * chce: pozyskać komodora Morszczata jako element Aleksandrii, utrzymać Amientor przy życiu
  * ma: zaadaptowaną Aleksandrię, mechosystem Amientor
  * oferuje: miejsce w Aleksandrii i wieczne ukojenie
* Stacja Omszawer Wielki, neutralne miejsce na mapie pasu Omszawera
  * chce: powodzenia regionu, niezależności od Astorii
  * ma: plotki, informacje, wie z czym do kogo
  * oferuje: możliwość przetransportowania Morszczata poza sektor.
* Kolonia Noctifera, ramię Noctis w Omszawerze
  * chce: więcej ludzi, ochronić noktian, pozostać w ukryciu, rozpełznąć się
  * ma: amat, wysoką technologię, potężne uzbrojenie, komfort w Omszawer, sojusz z Amientor
  * oferuje: nic. Ale można się na Noctiferze zemścić. W końcu - noktianie.
* Kolonia Samojed, zdewastowana kolonia z astropociagami
  * chce: zemsty na Ariannie Verlen po ogromnych zniszczeniach, uratować swoich znikających ludzi (podejrzewają Amientor lub Noctiferę)
  * ma: futro samojedów, harvest-materiały, dość sporą populację
  * oferuje: Morszczatowi pozycję oficera militarnego / strażniczego
* Ludwik Kiron, dekadencki lord Eterni
  * chce: więcej ludzi, zniszczenia Noctifery ORAZ Amientor, żyć potencjalnie wiecznie
  * ma: eternijskie mechanizmy kontrolne, thralli, świetny taktyk; kiedyś dowódca Orbitera
  * oferuje: okazję do zemsty na noktianach i ew. Ariannie. Nienawidzi Orbitera.

## Punkt zero

INTENCJA-1: pokazać Darkenowi, czemu Morszczat tak bardzo nienawidzi noktian.
REALIZACJA: pokazać nieludzkie traktowanie przez noktian cywili astoriańskich. 
DESIGN: by uciec i móc się wycofać, noktianie zostawili cywili w agonii.
IMPLEMENTACJA: 

5 lat po wojnie Astoria-Noctis. Jeden z oddziałów noktiańskich eks-komandosów a dziś terrorystów został namierzony w centrum handlowym gdzieś w Drzewcu. Komodor Morszczat wysłał oddział mający ich pokonać i usunąć; niestety, terroryści wiedzą dokładnie jak działają siły Orbitera. Zorientowali się, że zostali namierzeni i jakkolwiek nie to było ich celem, zaczęli fortyfikować Centrum Handlowe.

Noktiańscy terroryści nie mają pojęcia, że w Centrum Handlowym znajduje się dwójka magów. Siergiej i Bożena. Agenci Orbitera, oficerowie floty. Są tam w cywilu, po prostu poszli kupić parówki i się zaczęło. Wraz z innymi cywilami pochowali się po kątach.

Terroryści noktiańscy - 15 osób - podzielili się na podgrupki. Trzy dwuosobowe grupy zbudowały fortyfikacje w kluczowych punktach, piątka opanowała halę i wzięła zakładników a dwie dwuosobowe grupy sformowały patrole, mające wypłoszyć wszystkich cywili i przenieść ich do hali głównej. **Noktianie nie chcą nikogo zabijać - ich celem jest się jakoś ewakuować**.

Bożena, Siergiej i jeszcze jeden cywil schowali się w jednym z pomniejszych sklepów. Bożena poprosiła cywila, by ten zwrócił na siebie uwagę noktian - a ona i Siergiej schowali się przy suficie, na rurach. Jako, że noktianie używają inhibitorów magicznych, magowie nie są w stanie niczego im zrobić - ale jeśli ich obezwładnią, będą w stanie przejąć kontrolę nad patrolem magicznie. Potem - unieszkodliwią szefa noktiańskich terrorystów.

(XXXVXX):

Tak więc cywil wydał dziwny dźwięk, chcąc skusić noktian. Patrol zareagował - jeden wszedł do środka znaleźć cywila, drugi został na zewnątrz asekurując pierwszego.. Niestety, cywil za bardzo się przejął swoją rolą (X) - chciał POMÓC. Noktianin uderzył cywila w twarz, z refleksem, po czym go obezwładnił. Na to zeskoczył Siergiej - ryzykowny manewr, korzystając z tego, że zewnętrzny noktianin zaraportował "mamy tu bohatera i coś jest nie tak" (X). Siergiej unieszkodliwił noktianina w środku (V); z wprawą zewnętrzny noktianin zastrzelił cywila (nie wiedząc czy jest zagrożeniem czy nie) (X).

Siergiej nie był w stanie się dobrze zasłonić czy asekurować przed kompetentnym przeciwnikiem; szybko zasłonił się unieszkodliwionym noktianinem i jeden terrorysta zastrzelił drugiego (X). Niestety, sytuacja jest stracona - pozostały noktianin ostrzegł, że mają strzelaninę i jeden z nich nie żyje. Szybko się wycofał, prosząc swoich o wsparcie (X).

Morszczat kazał natychmiast Zespołowi się wycofać. Siergiej stwierdził, że "w życiu" i stworzył iluzję potężnego oddziału szturmowego (VXOXVOV - OOV). W ten sposób noktianie strzelają do iluzji  a Zespół do prawdziwych noktian - i to z powodzeniem (V). Jednak komandosi Noctis szybko się zorientowali że coś jest nie w porządku (X), zwłaszcza, gdy w ogniu zaczęli ginąć pochowani przypadkowi cywile (O). Noktianie zaczęli używać sprzętu antymagicznego (X) - iluzje zaczęły się rozsypywać i Siergiej zaczął walczyć ze zbliżającymi się siłami Paradoksu. W dalszej walce niestety zginęło jeszcze kilku cywili (O), ale kombinacja iluzji i magii bojowej sprawiła, że patrol noktiański i dwie fortyfikacje zostały zniszczone.

Centrala komandosów noktiańskich kazała wycofać się ostatniej placówce do rdzenia. Siergiej próbował przechwycić ich zanim dobiegną (OOV). Niestety, destabilizacja energii spowodowała Paradoks, potem EKSPLOZJĘ Paradoksu - iluzje Siergieja stały się prawdą. Wirtualni żołnierze Siergieja wpadli do miejsca z zakładnikami i ZMASAKROWALI noktiańskich komandosów - łącznie z niemałą ilością cywili.

To wydarzenie zostało potem nazwane "masakrą noktiańską w centrum handlowym". Orbiter ukrył działanie ich agentów, stwierdził, że to wszystko zrobili noktianie i to był zamach terrorystyczny oparty na nienawiści i bezdusznej chęci mordowania cywili...

## Misja właściwa

W dzisiejszym czasie - Morszczat wie, że sytuacja jest stracona. Jego miragent jest na własnym programowaniu. Arianna Verlen będzie jego zgubą. Morszczat chciał zniszczyć Trzeci Raj i najpewniej mu się to udało - ale skrzywdził zbyt wielu astorian. Nikt mu tego nie daruje. Jest skończony.

Tak więc komodor Zbigniew Morszczat, pod siłami admirał Aleksandry Termii, poprosił o wsparcie swoich dwóch przyjaciół - Bożenę (złamaną i cyniczną agentkę bezpieczeństwa) i Zbigniewa (siły specjalne Orbitera pod Medeą Sowińską). Niech oni udadzą się do Pasa Omszawera - miejsca tak daleko, że normalnie Orbiter tam nie zagląda - i niech pomogą mu zmienić swoją przyszłość. Co bowiem zostaje Morszczatowi?

1. Poczekać i oddać się w ręce admirał Termii. Wiemy, jak się to skończy ;-).
2. Zemścić się na Ariannie Verlen za to co zrobiła i co sobą reprezentuje - zgubę Astorii (bo wspiera noktian).
3. Ucieczka poza Sektor Astoriański.

I po to polecieli Bożena i Siergiej. Dać Morszczatowi ostatnią szansę, przez wzgląd na dawną przyjaźń.

Wpierw jednak trudne pytanie - JAK chcemy ewakuować Morszczata? Ewentualnie, czy w ogóle? Po dłuższej dyskusji, Bożena i Siergiej uznali, że pomogą mu uciec - przez wzgląd na dawną przyjaźń. A zdaniem Bożeny, Arianna musi ucierpieć - przecież przez jej działania sama Bożena czy sporo niewinnych magów pod komendą Morszczata będą mieć straszne problemy. Siergiej się z tym zgadza - choć jest w siłach specjalnych, jednak jemu pasuje takie rozwiązanie. Arianna troszkę zaczepnie podskakuje, trzeba ją tyci ukrócić. Więc przyjemne z pożytecznym.

Są dwie możliwości wycofania Morszczata: ALBO wycofać jego samego, znaleźć statek który przeleci na inny sektor, ALBO wyłączyć kody obronne jego Persefony i zdobyć mu mapę eteryczną. W ten sposób może przejść do innego Sektora i oddać w ich ręce swój statek. Po zastanowieniu, Bożena i Siergiej nie zgodzą się na wariant "zdrady" - wariant "uciekł" jest mniej bolesny dla wszystkich. Czyli potrzebne będą: miragent (Morszczat poradzi sobie tu sam), fałszywa tożsamość i statek. Dwie ostatnie Zespół jest w stanie zdobyć.

I tak Siergiej i Bożena znaleźli się na Stacji Omszawer Wielki. Miejsca, gdzie wymieniane są plotki, gdzie nasłuchują piraci i które jest idealnym hubem dla Pasa Omszawera.

Tam Zespół poznał imię jednego z pirackich lordów - Ludwik Kiron. Arystokrata Eterni, kiedyś agent Orbitera, wygnany za okrucieństwo i bezduszność. Ale Kiron nienawidzi noktian - jest to potencjalna osoba mogąca pomóc Zespołowi w ewakuowaniu Morszczata.

Po tym, jak dostali audiencję w Hikirionie - kolonii kontrolowanej przez Ludwika Kirona - sam Kiron zrobił na nich ciekawe wrażenie. Szczupły i blady arystokrata, wyraźnie "wampir z Eterni", który próbuje zachować przepych i luksus w warunkach chronicznego braku energii i dość niskiego budżetu (acz jak na standardy Omszawera, przepych i luksus). Gdy tylko Zespół zaproponował, by Kiron pomógł Zbigniewowi Morszczatowi, Ludwik Kiron zaproponował kontrofertę.

Otóż, Kiron chciałby Zbigniewa Morszczata dla siebie. Komodor Orbitera, zwłaszcza taki jak Zbigniew Morszczat, byłby idealnym sojusznikiem i prawą ręką dla Kirona.

Zespół uznał to za świetny pomysł. Po pierwsze, nie trzeba opuszczać sektora - jest bezpieczniej. Po drugie, z perspektywy Siergieja, to daje możliwość wsadzenia potencjalnego przyszłego agenta sił specjalnych do Kirona. Po trzecie, zdaniem Bożeny, to daje opcję rehabilitacji Morszczata w przyszłości.

Kiron się ucieszył, że taka ich decyzja. Zaproponował test dla Morszczata - jest w okolicy kapitan piratów zwący się Donald Parziarz. Parziarz nie pochodzi z sektora astoriańskiego; przyleciał przez którąś z eterycznych bram. Używa koloidowego statku o dużej sile ognia, o nazwie "Harem Gwiazd". Kiron chciałby się Parziarza pozbyć, ale nie jest w stanie - koloidowy statek po prostu jest poza zasięgiem floty Kirona.

A gdyby tak Arianna Verlen i Donald Parziarz zetknęli się ze sobą? Nieważne kto przegra, Kiron wygrywa.

Ale jak do tego doprowadzić?

Bożena ma prosty pomysł. Trzeba spreparować odpowiednio odcinek "Sekretów Orbitera" i podłożyć to jako przynętę Parziarzowi - z profilu Parziarza wynika, że temu zależy przede wszystkim na sławie. Poluje na sławę dla siebie, zbiera ludzi i jest ostrożny - usuwa zwłaszcza niewielkie jednostki, które może alphastrikować z koloidu. Jeśli tak, Arianna z "Sekretami Orbitera" jest idealnym celem.

Siergiej to podchwycił. Połączył się (z ogromnym opóźnieniem, ku swemu nieszczęściu) z jawnymi bazami Kontrolera Pierwszego i użył swoich umiejętności blackops by dostać informacje o nowym odcinku. (VVXV). Ku swojemu WIELKIEMU zadowoleniu, dowiedział się, że następny odcinek będzie realizowany jako odcinek specjalny - Izabela Zarantel przygotowuje coś super i efektownego. Siergiej pozując jako jeden z fanów zaproponował Izabeli działanie w okolicach Pasu Omszawera i znalezienie dziwnej anomalii. Dodatkowo podłożył jej, że można zaaranżować walkę z piratami. Owszem, na K1 zostaną ślady jego działań - ale ślady wskazują na Morszczata, nie na Siergieja.

Jak sprawić, by piraci Parziarza to łyknęli? Siergiej ma plan genialny w swojej prostocie - przegra scenariusz w karty do piratów Parziarza.

* X: piraci zorientowali się, że to wew. rozgrywki w Orbiterze, co im zupełnie nie przeszkadza
* X: Parziarz wie, że Siergiej za tym stoi. Osobiście.
* X: po grze piraci uznali, że Siergiej oszukiwał i spuścili mu manto. DOSŁOWNIE puścili go w galotach.
* X: Parziarz uważa, że to personalna wina Siergieja, jeśli skrypt będzie inny niż działania Arianny.

Po czym plan został zrealizowany. Pułapka na Ariannę Verlen zastawiona...

## Streszczenie

Zbigniew Morszczat stojący za niszczeniem Trzeciego Raju nie chce zgodzić się na swój los i wysłał dwóch przyjaciół (i agentów) by pomogli mu uciec. Oni wpakowali Morszczata w nowe miejsce - do pirackiego lorda Kirona z pasa Omszawera. Ceną za to było pozbycie się rywala - kapitana Parziarza. A dokładniej: wpakowanie go na Ariannę Verlen...

## Progresja

* Zbigniew Morszczat: opuszcza Kontroler Pierwszy i nie jest już komodorem pod adm. Termią. Zostaje prawą ręką Ludwika Kirona, lorda pirackiego z Pasu Omszawera.

### Frakcji

* .

## Zasługi

* Siergiej Rożen: blackops pod Medeą Sowińską i przyjaciel Morszczata. Pozyskał plany Sekretów Orbitera a potem "przegrał je" do piratów by zastawić pułapkę na Ariannę.
* Bożena Kaldesz: oficer security Orbitera i architekt planu "Użyjmy Sekretów Orbitera przeciw Ariannie, jako pułapkę". Przyjaciółka Morszczata.
* Zbigniew Morszczat: komodor Orbitera polujący na noktian, który po wydarzeniach w Trzecim Raju chciał uciec z sektora. Ale przeszedł do piratów Kirona jako jego prawa ręka.
* Ludwik Kiron: dekadencki lord piracki, kiedyś arystokrata Eterni. Przyjął do siebie Morszczata. By się pozbyć rywala - Parziarza - wpakował go na Ariannę Verlen.
* Donald Parziarz: kapitan piratów spoza Astorii używający koloidowego statku; chce sławy i ludzi. Złapany na przynętę przez Zespół - chce zapolować na Ariannę Verlen.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Pas Omszawera
                1. Anomalia Somnium, orbita
                    1. Mechosystem Amientor
                        1. Aleksandria 
                    1. Anomalia Somnium
                1. Stacja Omszawer Wielki: główne miejsce plotek i wymiany wiedzy; teren neutralny dla Pasa Omszawera.
                1. Kolonia Hikirion: w rękach lorda pirackiego Ludwika Kirona.

## Czas

* Opóźnienie: -5
* Dni: 4

## Inne

.
