## Metadane

* title: "Upiorny servar"
* threads: brak
* motives: wyscig-szybkich-pojazdow, bardzo-irytujacy-reporter, toksyczny-zwiazek, manifestacja-ducha
* gm: żółw
* players: xsazar, pawel_f, ania_1, marcin_ch, radek_1

## Kontynuacja
### Kampanijna

* [181112 - Odklątwianie Ateny](181112-odklatwianie-ateny)

### Chronologiczna

* [181112 - Odklątwianie Ateny](181112-odklatwianie-ateny)

## Projektowanie sesji

### Struktura sesji: MitH

Scooby Doo, ale duch jest prawdziwy ;-).

### Dark Future

1. 25: Nigdy więcej nie będzie już wyścigów; postacie trafiają do więzień za przeszłe czyny
1. 25: Celina, Michał, Gabriel umierają, wszystkie servary autonomicznie odchodzą

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie (NPC)

Irek  (Wyścigowiec)

* Podkochiwał się w Celinie; nie żyje
* Lojalny przyjaciel, niezbyt kompetentny
* Zawsze marzył o byciu docenionym

Gabriel (Wyścigowiec)

* Aktualny chłopak Celiny
* Zwycięstwo ponad wszystko
* Zawsze lubił wygrywać

Celina (eks-Wyścigowiec)

* Aktualnie nie jeździ z Wami
* Ostatnio dosyć przygaszona
* Zarabia dzięki zdjęciom na Havogram

Beata (eks-Wyścigowiec)

* Aktualnie nie jeździ z Wami
* Zajmuje się oświetleniem i iluzjami
* Kiedyś przyjaciółka Celiny i Irka

Aleksandra (Wyścigowiec)

* Wygrała ostatnie wyścigi
* Pamięta czasy rozbicia się Irka
* Bardzo zwalczała Michała

Michał (Wyścigowiec)

* Medyk, od wypadku – okultysta
* Żyje w poczuciu winy
* Kiepski kierowca; miał długą przerwę

Patrycja (Dziennikarka)

* Dokumentuje wszelkie problemy i wypaczenia na wyścigach
* Chce zamknięcia wyścigów

Wojciech (Kustosz Cmentarzyska Servarów)

* Główny dostawca części do servarów w całym Przelotyku
* „Wie wszystko o każdej maszynie”

### Opis sytuacji

.

## Misja właściwa

A new racing track was present and the team wanted to test it. The team consisted of five humans, rich kids – Bogdan, Śrubek, Ryszard, Alicja and Jurek. On top of that, two more people were present – Alexandra, the previous winner of the race and Michael, the medic.

Of course, if there is a race, someone might want to win it. Śrubek went for it – he decided to raise Alexandra, to win at least now during the completely unimportant contest. Jurek decided to help him - he kind of slowed Alexandra down while Śrubek raced forward and used the blinding light directly at Alexandra. What happened then – they managed to push Alexandra off track and she stumbled upon an old unexploded mine made by Śrubek. Her servar was heavily damaged; there was lots of blood and she was in pain.

The race has decided it doesn’t really matter – they need to win. Three however returned - and they were the ones who stumbled upon a reawakening ghost. An unholy amalgam of human flesh and black technology emerged from the fog and, in the form of a very entwined servar was closing to the medic. It seemed it was Irek - the person who died on the track about two years ago.

Alice was sure something is wrong; she was sure a ghost cannot appear because there are no ghosts at all. She used her advanced sensors to pierce through the ghost to see what she is really dealing with. She has managed to notice that the „ghost” does exist here but something is wrong: it should have very low power supply because of an old model, it physically exists here and unlike their power suits it has a weapon. While she was scanning the power suit, it retaliated – it shot the missile at Alice. Fortunately, there was no load so the missile just got bounced off the power suit. Alice fell down on the ground, trying to catch her breath. The impact was enough to damage her power suit considerably. Civilian model.

Jurek decided to slow down the „ghost power suit” - he started fighting it using his invisibility and great agility. Unfortunately, enemy power suit proved to be much more powerful than expected – Jurek was sure he is fighting a military grade power suit. And his torso got ripped by the close of the ghost power suit. In the meantime, Richard was scanning the power suit – he noticed the holo- projector and that ghost power suit needs to be made from other suits; it has some components which come from war times. It has military grade equipment. Being scanned, it retaliated by shockwave - Richards sensors got fried.

Ghost power suit tried to execute the medic – he lifted him and through him at another unused mine. This one had no load either, so the medics person was slightly damaged but it did not blow up. (-2 Wpływ Graczy). Alexandra started shouting at the ghost - he should not act this way, he used to be a good person, what the hell is happening here.

Seeing that, Śrubek and Bogdan decided to be heroes. Śrubek loaded self-destruct sequence on his power suit and road as fast as he could directly into the ghost exploding his power suit in the process. (Heroic Conflict). Two power suits got annihilated, everyone was more or less wounded but the ghost power suit had to disappear. It was unable to function after that type of explosion, even with military grade armor.

The players noticed that the thing was, there are hidden cameras on the track. Someone has seen all that. Probably it was Patricia. Well, having the journalist who wants to dismantle the races look at all of that would be slightly unfortunate, so they had to split. Bogdan and Jurek went to Patricia while Ala, Śrubek and Ryszard worked on finding some clues.

Alicja has managed to find several interesting clues – she found a damaged holographic image generator, and some proofs that the ghost power suit was made out of five or six different machines. Ryszard knew were those machines could be made – a usual place where a lot of parts are located is the „power suit graveyard”, under the custody of Wojciech. So there is that.

In the meantime, Śrubek has found were Patricia is probably located – in the broadcasting station even if it is almost midnight. This bought some time for the ones who were trying to catch her before she broadcasts anything; they have managed to catch her on time.

How to get to the office then? Jurek decided to climb up the building using his style suits having the other power suit on his back. Unfortunately, his suit was majorly damaged after fighting the ghost - what happened was that Bogdan’s suit fell down because in other parts of the armor simply gave in. Patricia got closer to the window and to buy Bogdan some time, Jurek tackled her forgetting he is in a stealth suit – but still in a power suit – while she is a fragile woman. In other words, she got unconscious.

Bogdan went up and confronted awakened Patricia. He told her that he is really interested in solving the case of the ghost power suit and he really wants to propel her career as well – he has money and he can make everyone’s life miserable. He has managed to convince Patricia, a woman of many ambitions. She has started cooperating with the team in solving the mystery of the ghost.

Having access to Patricia’s knowledge and investigation and to overall clues and information they are able to pinpoint two people able to do it - to build that type of a military power suit and operate it. Beata and Wojtek. They decided to split again and Alicja with Bogdan went to see Beata.

Beth’s house has a garage. Next to it there are many damaged, molten parts. It seems the ghost power suit has returned here and it was quite damaged in the fight. Beth doesn’t answer the door so they have simply broken in. Beth was lying on the sofa, she lost a lot of blood and she was only semiconscious. Bogdan started interrogating her and she told the whole truth – she has seen a ghost and the ghosts told her what to do and she did toward the ghosts told her and the ghosts will return and that the relationship needs to be broken. And they have noticed that the parts are moving slowly towards the garage. And Beth’s blood is also moving slowly towards the garage. Before, it was Scooby Doo – but right now the ghost becomes real.

They did their best to alert the rest of the party mates and to stop Beth’s blood flowing into the servar. They have started negotiating with the ghost – what does he really want? The ghost wanted to break the relationship between Gabriel and Celina. Irek’s ghost said that this relationship is toxic for her and she is going to be very hurt if he doesn’t do anything.

Fortunately, the rest of the party mates arrived and Śrubek took with himself several antitank mines. Using the fact that the ghost was distracted talking to Alice and Bogdan, Śrubek put their minds inside the power suit and the ghosts body good destroyed. That was the first ghost known to humanity which got exorcised using antitank mines.

After all that, the party went towards Gabriel’s and Celina’s home and they did a little investigation. They have managed to find that Gabriel was really not a good fit for Celina and by planting some evidence and doing some stuff from the past they have managed to break the relationship with time.

So, the ghost won, after all, I suppose?

### Wpływ na świat

* Po tym wszystkim: Liga wyścigowa ma lepsze wsparcie. Nazywa się "Iskra Irka", w hołdzie dla przyjaciela
* Alicja i Patrycja weszły w sojusz; razem rozwiązują dziwne zagadki okultystyczne na tym terenie
* Wojciech trafił do więzienia za zbudowanie terrorystycznego servara. Jego miejsce zajął Ryszard.
* Gabriel ma ogólnie przechlapane - nie ma życia (na servagram czy innych mediach społecznościowych) - persona non grata. Kiedyś przez niego Irek zginął.
* Śrubek zainteresował się większą ilością eksplozji; chce wejść w kompleks wojskowy
* Beata trafi do zakładu zamkniętego - nie poradziła sobie ze stężeniem nienaturalności wynikającej z dotyku Irka

## Streszczenie

Grupa bogatych dzieciaków ścigała się na nowym torze i pojawił się "duch" dawnego zmarłego przyjaciela. Okazało się, że za duchem stoi jedna z nich - przyjaciółka byłej mistrzyni która przestała się ścigać. Jednak po pewnym czasie duch ożył i faktycznie inkarnował się w servarze. Wysadzenie servara i rozbicie niefortunnego związku rozwiązało problem ducha.

## Progresja

* Patrycja Karzec: kariera skoczyła do góry po wsparciu rodziny Daneb. Ma silniejszą pozycję i reputację niż kiedykolwiek.
* Patrycja Karzec: wraz z Alicją Wielżak skupia się na rozwiązywaniu zagadek okultystycznych na terenie Przelotyka. Policjantka i reporter.
* Alicja Wielżak: wraz z Patrycją Karzec skupia się na rozwiązywaniu zagadek okultystycznych na terenie Przelotyka. Policjantka i reporter.
* Wojciech Tuczmowil: za zbudowanie terrorystycznego servara trafił do więzienia
* Ryszard Januszewicz: przejął po Wojciechu cmentarzysko servarów, nie do końca zdając sobie sprawę z implikacji. Spełnił swoją vendettę.
* Beata Wielinek: trafiła do zakładu zamkniętego; dotknął ją duch i nieco za dużo przeszła
* Gabriel Szaczyr: nie ma życia w Przelotyku za przeszłe czyny oraz kampanię weń wymierzoną

### Frakcji

* 

## Zasługi

* Bogdan Daneb: dupek, zależy mu na prestiżu, złośliwy manipulator. Przekonał Patrycję do współpracy i wyciągnął z ducha czego tamten od nich chce.
* Alicja Wielżak: detektyw, córka komendanta policji. Świetnie łączy fakty i jest mistrzowską obserwatorką. Odkryła znaczenie ducha i jak z nim walczyć.
* Artur Śrubek: mechanik-innowator ze skłonnościami do wybuchów. Wysadził ducha poświęcając swojego servara, potem wysadził go przy rekonstrukcji.
* Ryszard Januszewicz: od dzieciństwa siedzi w servarach; podejrzewa Wojciecha o morderstwo ojca. Przejął kontrolę nad cmentarzyskiem i odkrył miejsce budowy servarów.
* Jerzy Cieniż: szpieg i bardzo zespołowy agent. Stoczył pojedynek z duchem nie przegrywając totalnie i dotarł na czas do Patrycji, kupując czas Bogdanowi.
* Aleksandra Garwen: zwyciężczyni poprzedniego Wyścigu Nadziei. Wpakowana na minę Śrubka, skończyła solidnie ranna - nadal próbowała zatrzymać ducha dobrym słowem.
* Michał Wypras: medyk, który kiedyś nie udzielił Irkowi pomocy (bo nic mu nie groziło). Duch na niego polował i chciał go zabić.
* Beata Wielinek: przyjaciółka Irka i Celiny, która została Dotknięta przez ducha. Złożyła z pomocą Wojciecha bojowy servar i ruszyła ratować przyjaciółkę. Skończyła w szpitalu.
* Wojciech Tuczmowil: kustosz cmentarzyska servarów dostrojony do owego cmentarzyska. Chwilowo trafił do więzienia za zbudowanie bojowego servara dla Beaty.
* Patrycja Karzec: dziennikarka z ogromnymi ambicjami nienawidząca Turnieju Nadziei. Uderzyła w turniej i posunęła karierę do przodu wspierając drużynę w odkryciu ducha.
* Gabriel Krajczok: bardzo drapieżny mistrz wyścigów. Stoi za śmiercią swego rywala, Irka. Zdominował Celinę, przez co Beata wezwała (niechcący) ducha Irka. Nie ma życia.
* Celina Szaczyr: kiedyś też się ścigała; Irek i Gabriel o nią rywalizowali. Skończyła jako lekko ubezwłasnowolniona ofiara Gabriela.

## Plany

* 

### Frakcji

* 

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Przelotyk Wschodni
                            1. Roszbór: Miasteczko blisko Zasłony Pustogorskiej. Raczej bez przyszłości jako nie-sypialnia; mało pracy, na dobrej trasie. Bogaci ludzie.
                                1. Stary Tor Wyścigowy: częściowo na terenach Skażonych; tam kiedyś zginął Irek.
                        1. Wojnowiec
                            1. Klamża: Niewielkie miasteczko przemysłowo-logistyczne. Dużo automatyzacji. Dużo pracy dla specjalistów.
                                1. Cmentarzysko Servarów: miejsce, w którym wyprodukowany był servar wojskowy przez Wojciecha dla Beaty
                                1. Tor Wyścigowy: miejsce starcia z duchem; skończyło się "małym" wybuchem eksplodującego servara
                                1. Stacja Nadawcza Eter: miejsce pracy Patrycji - wysyłanie i nasłuch. Zaatakowane przez 2 servary (stealth i gadający)

## Czas

* Opóźnienie: 2
* Dni: 2

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* nic

### Wykorzystana mechanika

1811
