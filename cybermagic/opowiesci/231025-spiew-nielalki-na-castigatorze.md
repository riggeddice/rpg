## Metadane

* title: "Śpiew NieLalki na Castigatorze"
* threads: triumfalny-powrot-arianny
* motives: energia-alteris, obrona-przyczolka, pozyskanie-groznego-artefaktu, przemyt, rodzina-trzyma-sie-razem, sabotaz-niespodziewany, starcia-kultur, teren-skazony, zaraza-powoli-infekujaca, koniecznosc-pelnej-dyskrecji
* gm: żółw
* players: kapsel, fox, kić

## Kontynuacja
### Kampanijna

* [231011 - Ekstaflos na Tezifeng](231011-ekstaflos-na-tezifeng)

### Chronologiczna

* [231011 - Ekstaflos na Tezifeng](231011-ekstaflos-na-tezifeng)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA: 
    * Emilie Autumn "Time for tea"
        * "There was a little girl, who had a little curl | Right in the middle of her forehead | And when she was good, she was very, very good | But when she was bad, she was homicidal!"
        * "Hatchet… Check! | Scalpel… Check!" ... "It’s time for war! | It’s time for blood! | It’s time for TEA!"
* Opowieść o (Theme and vision): 
    * "Nie możesz do sprawy w kosmosie podchodzić zbyt łagodnie czy nieostrożnie. Każdy - najmniejszy błąd opsec - skończy się katastrofą."
    * Przemyt poszedł nie do końca tam gdzie powinien. Na pokład Castigatora został wprowadzony bardzo potężny artefakt Altarient (bóg: Tessemont).
    * Demonstracja Altarient jako osobnego bytu - nie energii memetycznej, a energii znaczeń i NieRzeczywistości
    * SUKCES
        * uratowanie jak największej części Castigatora
        * ukrycie Skażenia przed Orbiterem
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * energia-alteris: (drp ALL): rzeczywistość się zniekształca. Zmysły i znaczenia się rozpadają. Persefona ostrzega, że jest niebezpiecznie "bo nie ma imprezy", inni wysadzają "pomidora". Ale przestrzeń też się zmienia. 
    * obrona-przyczolka: (drp Eustachy): Dotknięci przez Artefakt członkowie załogi próbują ochronić Artefakt i jego okolice. Niekoniecznie w sensowny sposób.
    * pozyskanie-groznego-artefaktu (drp ): 'Pomidorowa NieLalka', artefakt Altarient o dużej mocy, przekształcający znaczenia oraz przestrzeń (containment: próżnia)
    * przemyt (drp NONE): tien Keksik tym dowodził i jakoś pozyskał NieLalkę. NieLalka trafiła docelowo na Castigator, powodując zniekształcenie znaczeń i przestrzeni
    * rodzina-trzyma-sie-razem (drp NONE): tienowie ukrywają przemyt, korzystają z okazji. Sami próbują to rozwiązać.
    * sabotaz-niespodziewany (drp Eustachy): załoga Castigatora sabotuje Castigator, w całkowicie bezsensowny sposób.
    * starcia-kultur (drp Arianna): tien / Orbiter. Tieni nie widzą wojny, ale chcą się uczyć i dobrze bawić. Czemu się NIE bawić? Orbiter natomiast próbuje ich opanować i podejść poważnie. Stąd: tieni nie ufają Kurzminowi.
    * teren-skazony (drp Klaudia): okolice NieLalki zaczynają zawodzić. Anomalie przestrzenne i znaczeniowe.
    * zaraza-powoli-infekujaca (drp Klaudia): rozprzestrzenianie się NieLalki i po osobach i po przestrzeni.
    * koniecznosc-pelnej-dyskrecji (drp Arianna, Eustachy): Kurzminowi zależy, by tieni za bardzo nie ucierpieli. By to jednak działało prawidłowo.
* Detale
    * Crisis source: 
        * HEART: "obecność anomalii Altarient"
        * VISIBLE: "ludzie zachowują się dziwnie i znaczenia się rozpadają -> rzeczywistość się zmienia"
    * Delta future
        * DARK FUTURE:
            * chain 1: Ofiary śmiertelne
            * chain 2: program integracji tienów uszkodzony (wiedzą - Orbiter traci nadzieję - Aurum nie ma jak utrzymać) 
            * chain 3: Castigator uszkodzony
            * chain 4: Kurzmin ma problemy bo nie zapanował
        * LIGHT FUTURE: "Nikt nic nie wie, NieLalka w kosmosie (contained), tieni wstrząśnięci i Kurzmin ma możliwość coś z tym zrobić"
    * Dilemma: brak, sesja funkcjonalna

### Co się stało i co wiemy

* Przeszłość
    * Przemyt dóbr na pokład Castigatora doprowadził do przemytu NieLalki Pomidorowej (Konstanty Keksik), która trafiła do Środkowo-Dolnych Radiatorów
        * NieLalka nie musiała być containowana. Nie była aktywna, bo 'kura nie zniosła jajka'
        * była w przemycanych rzeczach, postrzegana jako coś wartościowego czy coś do imprezy
    * Konstanty Keksik dotknięty przez NieLalkę, zaniósł ją by 'kura' mogła 'wysiedzieć jajka'
* Teraźniejszość
    * Igor Arłacz orientuje się, że to co przenieśli jest szkodliwe / problematyczne i próbuje ją wysadzić, ale już jego Znaczenia się pozmieniały

### Co się stanie (what will happen)

* F0: 
    * Mowa do tienów, że sytuacja jest pod kontrolą i ostrzeżenie
    * Leona i "questions three" od tien Marty Keksik
    * Arianna dowiaduje się, że Elena oddaliła się z komodorem Walrondem polować na kralotha
* F1: _Ślad Anomalii_
    * INSTANCES
        * Eszara: 'alarm, bo nie było imprezy od dawna', 'jest to w regulacjach', 'błąd związany z brakiem wyrzutni czołgów', 'nie czuję reaktora'
        * Sabotażyści sabotują Castigator; próbują wysadzić tematy związane z ŁZami; Eszara ostrzega. "moja załoga mnie sabotuje (filozoficzne uzasadnienie)"
        * Ignacy Arłacz, że problemem jest "pomidor w surówce". Jest nienaturalnie szybki.
        * Kłótnia K. Keksika i I. Arłacza o to, czy to 'kura' czy 'pomidor'
    * DARK: 
        * uszkodzenia na pokładzie Castigatora
        * sygnał wycieka poza C. i idzie dalej
* F2: _Pełnoskalowa Anomalia_
    * Środkowo-Dolne Radiatory: lokalizacja NieLalki
    * INSTANCES
        * Drzwi prowadzą nie tam gdzie powinny, niektóre przejścia nie prowadzą nigdzie
        * Znane Przedmioty zachowują się w Nieznany sposób (np. maszynka do kawy robi kwas)
        * Skrzydła-Radiatory są jak pajęcza sieć, oplatając metalem
        * Ludzie i załoga broni NieLalki wierząc że to inwazja potworów
        * Niektórzy zaczynają wierzyć, że są kimś innym ("to ja jestem Arianną", Kurzmin "JA JESTEM CASTIGATOREM"), "to nie moja ręka".
        * Rozpełzanie się do źródeł energii. Mamy sporo tienów na szczęście (około 40, czyli 10% załogi)
        * Ściana jest gorąca i zimna jednocześnie.
        * "NIE ZBLIŻAJ SIĘ! NIE RÓB MI KRZYWDY!": tien i załoga, aż do używania magii
    * DARK: 
        * chain 1: Ofiary śmiertelne
        * chain 2: program integracji tienów uszkodzony (wiedzą - Orbiter traci nadzieję - Aurum nie ma jak utrzymać) 
        * chain 3: Castigator uszkodzony
        * chain 4: Kurzmin ma problemy bo nie zapanował

## Sesja - analiza

### Fiszki

Zespół:

* Leona Astrienko: psychotyczna, lojalna, super-happy and super-dangerous, likes provocative clothes
* Martyn Hiwasser: silent, stable and loyal, smiling and a force of optimism
* Raoul Lavanis: ENCAO: -++0+ | Cichy i pomocny;; niewidoczny| Conformity Humility Benevolence > Pwr Face Achi | DRIVE: zrozumieć
* Alicja Szadawir: młoda neuromarine z Inferni, podkochuje się w Eustachym, ma neurosprzężenie i komputer w głowie, (nie cirrus). Strasznie nieśmiała, ale groźna. Ekspert od materiałów wybuchowych. Niepozorna, bardzo umięśniona.
    * OCEAN: N+C+ | ma problemy ze startowaniem rozmowy;; nieśmiała jak cholera;; straszny kujon | VALS: Humility, Conformity | DRIVE: Uwolnić niewolników
* Kamil Lyraczek
    * OCEAN: (E+O-): Charyzmatyczny lider, który zawsze ma wytyczony kierunek i przekonuje do niego innych. "To jest nasza droga, musimy iść naprzód!"
    * VALS: (Face, Universalism, Tradition): Przekonuje wszystkich do swojego widzenia świata - optymizm, dobro i porządek. "Dla dobra wszystkich, dla lepszej przyszłości!"
    * Styl: Inspirujący lider z ogromnym sercem, który działa z cienia, zawsze gotów poświęcić się dla innych. "Nie dla chwały, ale dla przyszłości. Dla dobra wszystkich."
* Horst Kluskow: chorąży, szef ochrony / komandosów na Inferni, kompetentny i stary wiarus Orbitera, wręcz paranoiczny
    * OCEAN: (E-O+C+): Cichy, nieufny, kładzie nacisk na dyscyplinę i odpowiedzialność. "Zasady są po to, by je przestrzegać. Bezpieczeństwo przede wszystkim!"
    * VALS: (Security, Conformity): Prawdziwa siła tkwi w jedności i wspólnym działaniu ku innowacji. "Porządek, jedność i pomysłowość to klucz do przetrwania."
    * Core Wound - Lie: "Opętany szałem zniszczyłem oddział" - "Kontrola, żadnych używek i jedność. A jak trzeba - nóż."
    * Styl: Opanowany, zawsze gotowy do działania, bezlitośnie uczciwy. "Jeśli nie możesz się dostosować, nie nadajesz się do tej załogi."
* Lars Kidironus: asystent oficera naukowego, wierzy w Kidirona i jego wielkość i jest lojalny Eustachemu. Wszędzie widzi spiski. "Czemu amnestyki, hm?"
    * OCEAN: (N+C+): bardzo metodyczny i precyzyjny, wszystko kataloguje, mistrzowski archiwista. "Anomalia w danych jest znakiem potencjalnego zagrożenia"
    * VALS: (Stimulation, Conformity): dopasuje się do grupy i będzie udawał, że wszystko jest idealnie i w porządku. Ale _patrzy_. "Czemu mnie o to pytasz? Co chcesz usłyszeć?"
    * Core Wound - Lie: "wiedziałem i mnie wymazali, nie wiem CO wiedziałem" - "wszystko zapiszę, zobaczę, będę daleko i dojdę do PRAWDY"
    * Styl: coś między Starscreamem i Cyclonusem. Ostrożny, zawsze na baczności, analizujący każdy ruch i słowo innych. "Dlaczego pytasz? Co chcesz ukryć?"
* Dorota Radraszew: oficer zaopatrzenia Inferni, bardzo przedsiębiorcza i zawsze ma pomysł jak ominąć problem nie konfrontując się z nim bezpośrednio. Cichy żarliwy optymizm. ŚWIETNA W PRZEMYCIE.
    * OCEAN: (E+O+): Mimo precyzji się gubi w zmieniającym się świecie, ale ma wewnętrzny optymizm. "Dorota może nie wiedzieć, co się dzieje, ale zawsze wierzy, że jutro będzie lepsze."
    * VALS: (Universalism, Humility): Często medytuje, szuka spokoju wewnętrznego i go znajduje. Wyznaje Lichsewrona (UCIECZKA + POSZUKIWANIE). "Wszyscy znajdziemy sposób, by uniknąć przeznaczenia. Dopasujemy się."
    * Core Wound - Lie: "Jej arkologia stanęła naprzeciw Anomaliom i została Zniekształcona. Ona uciekła przed własną Skażoną rodziną" - "Nie da się wygrać z Anomaliami, ale można im uciec, jak On mi pomógł."
    * Styl: Spokojna, w eleganckim mundurze, promieniuje optymizmem 'że z każdej sytuacji da się wyjść'. Unika konfrontacji. Uśmiech nie znika z jej twarzy mimo, że rzeczy nie działają jak powinny. Magów uważa za 'gorszych'.
    * metakultura: faerilka; niezwykle pracowita, bez kwestionowania wykonuje polecenia, "zawsze jest możliwość zwycięstwa nawet jak się pojawi w ostatniej chwili"

Castigator:

* Leszek Kurzmin: OCEAN: C+A+O+ | Żyje według własnych przekonań;; Lojalny i proaktywny| VALS: Benevolence, Humility >> Face| DRIVE: Właściwe miejsce w rzeczywistości.

Randomy z Castigatora:

* Marta Keksik: OCEAN: E+C-A- | Pełna pasji, rozwiązuje problemy siłą. | VALS: Power, Stimulation | DRIVE: Udowodnić swą wartość.
    * świetna duelistka, elegancko ubrana, zimne i stalowe spojrzenie i bardzo duża asertywność. Chroni brata i 'swoich'.
* Konstanty Keksik: OCEAN: O+A+ | Zawsze z rozmachem, robi epickie rzeczy, zwłaszcza dla grupy. | VALS: Hedonism, Achievement | DRIVE: Zorganizować jak najlepsze życie.
* Igor Arłacz: OCEAN: E-C+ | Zawsze w cieniu, ale obserwuje i słucha. | VALS: Tradition, Security | DRIVE: Niech wszystko dobrze działa
* Anna Tessalon: OCEAN: O+A+ | Nieprzewidywalna, kieruje się intuicją, niesamowite wyczucie. Wrażliwa i empatyczna. | VALS: Stimulation, Universalism | DRIVE: Znaleźć swoje miejsce w świecie, niespokojny duch.
* Patryk Samszar: OCEAN: A+N- | Nie boi się żadnego ryzyka. Nie martwi się opinią innych. Lubi towarzystwo i nie znosi samotności | VALS: Stimulation, Self-Direction | DRIVE: Znaleźć prawdę za legendami.

* Kamil Burgacz: OCEAN: E+C+N+ | Impulsywny i namiętny, zawsze w centrum uwagi. | VALS: Hedonism, Power | DRIVE: Zostać bohaterem.
* Małgorzata Terienak: OCEAN: C+N+E- | Spokojna, obserwująca, ale z twardym rdzeniem. | VALS: Universalism, Benevolence | DRIVE: Leczyć, niezależnie od okoliczności.
* Mikołaj Blakenbauer: OCEAN: N-A-O- | Zawsze z tyłu, ale nieoceniony w planowaniu. | VALS: Security, Tradition | DRIVE: Chronić drużynę myślą.
* Zuzanna Mysiokornik: OCEAN: E+O-C+ | Pełna życia, ale z zacięciem do sprawdzania granic. | VALS: Hedonism, Self-Direction | DRIVE: Zdefiniować własne granice.
* Julia Myrczek: OCEAN: C+O-A+ | Elegancka, ale z silnym poczuciem obowiązku. | VALS: Tradition, Benevolence | DRIVE: Znaleźć wspólne rozwiązanie dla konfliktów.

nie-tien:

* Benedykt Czarny Harpun: OCEAN: E-O-C+ | Pewny siebie, ale dystansuje się od innych; | Analityczny i metodyczny| VALS: Security, Power >> Leadership | DRIVE: Kontrolować swoje otoczenie i podyktować reguły.
* Eleonora Perłamila: OCEAN: C+A+O- | Skoncentrowana i spokojna; | Unika ryzyka| VALS: Universalism, Self-Direction >> Visionary | DRIVE: Zrozumieć naturę świata i podążać za własną wizją.
* Stanisław Świktorny: OCEAN: E+O+N- | Pełen życia i energii; | Skłonny do zmienności nastrojów| VALS: Hedonism, Stimulation >> Passion | DRIVE: Doświadczać życia w pełni, niezależnie od konsekwencji.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Nie tak dawno, parę godzin temu wróciliście pod Castigatora. I Kurzmin poprosił, byście adresowali tienów i wyjaśnili im, że wszystko jest pod kontrolą.

Eustachy, siedzisz na Castigatorze i coś pije, fraternizując się z Orbiterowcami a nie arystokratami. Wchodzi Leona. Tym razem z jakąś tienką. Ta tienka wygląda młodo, dumnie, jest dobrze ubrana, wyprasowany mundur. Ale chodzi... sprężyście. Podchodzą do Ciebie i czekają. Leona ma kurwiki w oczach.

* E: Co się dzieje?
* L: O, najszlachetniejszy, tien Marta Keksik przybyła, by zażądać prawa Trzech Pytań. Czy udzielasz jej prawa zadania...
* E: Jedno zostało zadane, więc zostały dwa.
* L: (zachichotała) i do tienki: "lord Eustachy jest żartownisiem. On udaje, żeby było ciekawiej. Więc ja, pokorna herold lorda Eustachego Korkorana, z ukrytym nazwiskiem, wyjaśnię Ci zasady."
* L: Lord Eustachy Korkoran zada Ci trzy pytania. Jeśli na nie odpowiesz prawidłowo, będziesz miała prawo albo wyzwać go na pojedynek, albo dostać odpowiedź na pytanie, albo dostać jego pomoc. Czy przyjmujesz wyzwanie.
* M: (z powagą) Tak, heroldzie.
* L: Nie nie nie, to nie tak. Musisz powiedzieć, cytuję, "wzywam prawa do trzech pytań."
* M: Ja, Marta Keksik, wzywam prawa do trzech pytań!
* L: (uroczyście) Zadaj pierwsze pytanie, Lordzie!
* E: E... o co mam pytać, przyszłaś do mnie, gadasz, ja chilluję...
* L: (czeka)
* M: (patrzy na L, na E, na L, na E)... 
* E: Powiedz co masz powiedzieć bo musisz dokończyć drinka.
* E: Imię, nazwisko...
* M: (zaczyna coś mówić)
* L: PRZEGRAŁAŚ! Nie odpowiedziałaś na pierwsze pytanie lorda Eustachego
* M: (zdziwiona)
* E: (zdziwiony)
* L: (spokojnie, z namaszczeniem) Lord Eustachy w swojej łaskawości SPYTAŁ CIĘ o co ma pytać, bo Ty przyszłaś do niego. Najprostsze pytania. Nie odpowiedziałaś.
* M: To... nie jest ani honorowe ani nie brzmi sensownie.
* E: kobieto... robisz dziwną scenę, odpierdalasz protokół... siedzę tu bo mam w dupie protokół... POWIEDZ co masz!
* M: (zwężone oczy) (chce coś powiedzieć)
* L: Nie. Musisz wyjść, i wrócić. Tak zresetujesz kontekst. Tego wymaga protokół. Jesteś tienką. Rozumiesz. Eustachy jest lordem z Neikatis, tam robią wszystko INACZEJ.
* E: Leona... uspokój się...
* L: (wzruszyła ramionami) jak sobie życzysz, o nieskończone słońce piasku.
* E: ZEJDŹ NA ZIEMIĘ WIEM ŻE SIĘ NUDZISZ!

Marta na wszelki wypadek wyszła i wróciła. Leona stoi koło Eustachego udając powagę.

* E: Uspokój się... będę słuchał od Arianny jakiegoś pierdzielenia głodnego. Idź poznęcaj się nad czarnymi hełmami czy innymi trepami
* L: Jak wasza ekscelencja każe. Za nieskończony piasek! (wyszła świetnie się bawiąc)
* M: Wielki Piaskowy Duchu.
* E: Kończ.
* M: Czy dasz mi pozwolenie na...
* E: Tak, mów...
* M: Na pojedynek ze sobą?
* E: Jaki pojedynek?
* M: Twoja... _ona_ powiedziała, że jesteś największym wojownikiem floty Neikatis i że dasz mi lekcję lub dwie, jeśli odpowiem na pytania. Przepraszam, Wielki Piaskowy Duchu.
* E: Skończ...
* M: Skończyć... co?
* E: Leona zrobiła Ci wodę z mózgu, Ty to łykasz...
* M: To nie jest oficjalny tytuł?
* E: Nie.
* M: To jaki jest oficjalny?
* E: Nie jestem z tych sztywnych szlachciców.
* M: Co się stało z Natalią? I czy mogę się z Tobą zmierzyć w pojedynku?
* E: Czemu?
* M: Podobno jesteś magiem neikatiańskim i jesteś mistrzem pojedynków. Ona tak powiedziała. Podobno nauczyłeś ją eksterminacji. W godzinę. Twoja magia uczy innych.
* E: To tak nie działa.
* M: (patrzy na Ciebie z frustracją) To do czego możesz mi się przydać?
* E: Kto powiedział że muszę?
* M: No... _ona_. Powiedziała, że jesteś jak dżin spełniający życzenia.
* E: Nie jestem.
* E: Chcesz powiedzieć że potrzesz drzwi z mojej kajuty i spełniasz życzenia?
* (głos z sali): Jak się pociera coś innego, to czasem wyskakują życzenia. (Tienka zrobiła się czerwona)
* E: Który taki kurwa zabawny?
* (cisza)
* M: To chociaż powiedz co się stało z Natalią. Nikt nam nic nie powie. Ja miałam tam lecieć, ale Natalia wygrała los na loterii.
* E: A co mam CI powiedzieć... idź do dowództwa, gdziekolwiek... strać się. Woda z mózgu, zaczepiasz mnie... nie chcę o tym dyskutować. Zaraz Cię starsi panowie będą docinać dowcipami z brzuszkami...
* M: Dobrze. Dziękuję, _lordzie_ (z przekąsem).
* E: Nie jestem lordem.
* M: I masz nieelegancką służkę!
* E: Nie mam służki.

10 minut później.

* L: Eustachy, wyzwała mnie na pojedynek. Przyjęłam. Dzięki.
* E: Cieszę się.
* L: Liczyłam na to, że Twoja unikalna perspektywa i wybitna ogłada ją zirytuje. Jak zawsze, spełniłeś moje oczekiwania.
* E: CIeszę się.
* L: Gdzie jest moja służka? Miałam mieć służkę na Castigatorze. Zrób coś.
* E: (tryb ignorowania)
* E: Twoja służka, Twój problem. Znajdź sobie.

TYMCZASEM ARIANNA. Przygotowuje się do przemowy. I dostaje request od Leony "Zgubiłam służkę. Mogę poszukać sama?" Arianna "NIE!". Leona: "Eustachy mi ją znajdzie?" Arianna "Kto jest? Od kiedy masz?" Leona: "Ostatnio na Castigatorze wygrałam, na tydzień." Arianna "nie mamy handlu niewolnikami na Orbiterze" Leona "przegrała zakład. Nie chcę żeby robiła nic dziwnego. Nie wiem, masaż stóp. Koktajl. Mam sama poszukać czy odnajdziecie moją służkę?"

* A -> E: O co chodzi ze służką Leony?
* E: nie wiem
* A: wiesz, powiedziała że wiesz
* E: kiedy ostatnio powiedziała coś z sensem co nie dotyczyło przemocy?
* A: w słowach Leony jest głębsza prawda. Np. odnośnie wspólnych konszachtów
* E: Nie wiem, zaczepiała, pojedynek, tienka, nie słuchałem, nie interesowałem się... przepustka i... 
* A: zaraz nie będziesz na przepustce
* E: HALO ZAKŁÓCENIA WYŁADOWANIA ELMAG!
* A: jeśli Leona zrobi krzywdę KOMUKOLWIEK osobiście wyślę Cię do latryn na tydzień. I Leona będzie Cię nadzorowała.
* E: ...czemu ja... czemu każesz mnie za jej...
* A: byłeś przy Leonie i nie powstrzymałeś? jesteś oficerem, drzyj ryja BARDZIEJ!
* E: (idę jej szukać)

Leona stoi i debatuje z Martą. Marta proponuje broń, Leona się zgadza. Marta - inną broń, Leona też się zgadza. Marta zaczyna być sfrustrowana.

* E: Leona, chodź, robota do zrobienia
* L: przepraszam, tien Keksik. Mój lord mnie wzywa. (do Eustachego) Wielki Piaskowy Duchu, ach przepraszam, jesteś incognito...
* E (nagrywa i wysyła do Arianny...) -> A:  POZWÓL MI JĄ ZASTRZELIĆ!
* A: (nagraj jak się Leona kłania, będziemy to puszczać...)
* E: zachowanie etykiety wymaga stosownego ukłonu?
* L: Wielki Piaskowy Duchu, nie przeginaj. Wiesz, że bolą mnie plecy
* E: możesz uklęknąć. (puszcza oczko, pokazując Martę)

Tr Z +3:

* Vr: Leona pięknie uklękła i ślicznie się nagrało
* Vz: Leona uzna to za godny żart. Łyka to.
* Vr: Leona zapomniała o temacie służek. Jest skupiona nad naprawieniem "uklęknięć" i wprowadzaniem "Wielkiego Piaskowego Ducha".

I DLATEGO EUSTACHY JEST OFICEREM!

Arianna osobiście przemawia do tienów. Z 40 stawiło się 32. Arianna opowiada - Natalia zaginęła, trwają prace poszukiwawcze. Była zdolnym rekrutem i skoro uciekła z tamtego statku to sobie poradzi i uda się odzyskać. (to podnosi morale). Może została przekształcona, ale MUSIAŁA uciec. Inne opcje ciężko wymyśleć.

Arianna chce postawić Natalię jako przykład - zła sytuacja, wyszła z niej, jestem z niej dumna. Tacy przedstawiciele Aurum. Powinni być z niej dumni.

Tr Z +3:

* Vr: (p) Ogólnie morale jest wyższe.
* V: (m) Ryzyka w kosmosie są duże. Trzeba grać ostrzej. Nie można tylko się bawić.

Tieni zadają pytania - takie "operacyjne". "czemu nie pomogliście", "czemu nie mogą i się bawić i być skuteczni, mają magię". Arianna 'Castigator to jednostka specjalna, tu możecie, ale normalnie można tracić cennych członków załogi'.

* X: Podniesiony jest temat Leony i Eustachego ("Wielkiego Ducha Piasku") i czemu oni mogą.
    * Arianna: "nie są w pracy, mają wolne XD, gdy Infernia jest w próżni, działają jak zgrany organizm"
    * (łyknęli to) Plus, jak będą tak dobrzy jak Leona, mogą się bawić jak Leona.
* Vr: Dociera do nich, że Castigator jest jedynym 'bezpiecznym' miejscem i że muszą być dużo lepsi. Bo Arianna jest tienką i pracowała z tienami.

Kurzmin -> Arianna

* K: Dobra robota. Dotarłaś do nich w sposób jaki mi się nie udało.
* A: Może będzie lepiej
* K: Szkoda, że Eleny tu nie było. Nie udało mi się jej zatrzymać.
* A: Opuściła Castigator?
* K: Jak dowiedziała się, że Natalii coś się stało, tymczasowo dołączyła do najbliższego komodora, Walronda, żeby pomóc mu szukać kralotha. Podniosła swoje umiejętności detekcyjne i że jest Verlenką. Moim zdaniem? Ona mu się podoba.
* A: Nie pierwszy raz robi coś takiego...
* K: Mogłem albo rozwalić jej reputację albo zaakceptować ten ruch. Mam nadzieję, że wróci bezpiecznie.
* A: Gdzie polecieli? Są poszlaki?
* K: Komodor Walrond rozpuścił jednostki i skanują statki cywilne. Mają sterowanie od góry. Wiem też, że siły specjalne się tym też zajmują.

Wiadomość od Eszary.

* TAI: Kapitanie? Czy... czemu członkowie mojej załogi próbują sabotować Castigator?
* K: O_O
* TAI: Nie chcę robić im krzywdy, próbują wysadzić hangar oraz syntezator ŁZ.
* K: Zatrzymaj ich.
* TAI: Nie mam możliwości zatrzymania im nie robiąc im krzywdy, tylko spowolnić
* K: (zdziwienie)
* A: Nie możesz ich odciąć?
* TAI: Nie. Nie mam możliwości. Coś... coś jest nie tak. Uruchamiam diagnostykę. Rdzeń AI wygląda sprawnie... (zdziwiona, że nie przyszło jej do głowy póki nie zapytałaś)
* K: Pełna diagnostyka. (do swoich ludzi z ochrony - zatrzymajcie ich.). Do Arianny: wyślesz tam swoich? Żeby się nie nudzili?

Eustachy się bierze do roboty. Ochrona próbuje zajść tiena. I nagle w komunikatorze Eustachy słyszy: 

* (niezidentyfikowany tien): "Alarm! Ketchup się zbliża!"
* (ochrona Castigatora): "on oddał strzał ostrzegawczy"
* (tien): "Jeśli się zbliżysz, będę musiał Cię zastrzelić. Panie kapitanie! Proszę o uprawnienia do zastrzelenia ketchupu!"

Kurzmin -> Arianna

* K: Widziałaś coś takiego? 
* A: Nie..?

Ochrona monitoruje, tien metodycznie rozkłada ładunki (jeszcze zajmie, to nie jest ekspert). Dane od Kurzmina: Igor Arłacz. Cichy, sensowny koleś, raczej na uboczu. Robi dobrą robotę. Nic szczególnego.

* Eustachy -> Leona: "Udawaj tragarza dynamitem"
* E -> I: "Ziomuś pomogę mam dynamit"
* I: "Szybko zanim kapitan się dowie, tien Korkoran!" (Leona z trudem powstrzymuje śmiech)
* I: "Zapewnię, żeby ketchup nas nie dotknął"

Eustachy wchodzi. Koleś w Ciebie nie celuje. Nie postrzega E. jako zagrożenie. Eustachy (muszę wiedzieć co wysadzam wyjaśnij i pokazał bugi w wysadzaniu)

Tr (niepełny) Z +4:

* XX: Jego słowa nie mają najmniejszego sensu. Ale on jest przekonany że mają.
* X: Igor rozstawia ładunki by wysadzić maszynkę do kawy. (to ON przyniósł tu ten ekspres do kawy)

.

* E: "To chcesz wysadzić?"
* I: "Bardzo niebezpieczne. Błękitny bażant... (przypatruje się) nie, pomidor."
* E: "Co DOKŁADNIE musimy rozwalić?" 

Niech on to rozstawi, a potm wyjmiemy ostatni drucik i zrobimy iluzję flashbanga

Tr Z +4:

* V: Igor myśli że wysadził i do niczego nie doszło
* V: Leona go przechwyciła, nie stała mu się nawet krzywda.

Eustachy zbiera ładunki wybuchowe.

Tymczasem Klaudia pomaga w diagnostyce Eszary. Eszara ma swoją pierwszą hipotezę co się stało, czemu nie miała dostępu:

* TAI: Kapitanie Kurzmin, mam informację dlaczego nie miałam dostępu do systemów
* K: Słucham?
* TAI: Z uwagi na to, że zgodnie z regulaminem raz na tydzień powinna być zrobiona impreza.
* K: ...Eszaro powtórz?
* TAI: Hulaszcza rozrywka. Impreza. Zgodnie z regulacjami. Nie została zrobiona. Nie mam dostępu do systemów.

Klaudia sprawdza dane Eszary. Szuka rzeczy które nie pasują. Ale też... (do Kurzmina) kiedy TAI działała normalnie? Kurzmin "przed wydarzeniami z Saitaerem. Wtedy mieliśmy problemy z Memoriam" (tydzień?)

Klaudia szuka niskopoziomowo przelecenia po plikach systemowych, koło zapisów o imprezie, szuka kiedy zmienione, przez kogo itp.

Tr Z +3:

* V: Klaudia zobaczyła coś niepokojącego.
    * Eszara nie do końca czuje swoje "ciało". Nie widzi że coś jest nie tak. Zgodnie z diagnostyką jest jak powinno być
    * Czujniki pokazują... zniekształcenie Castigatora. Castigator nie ma swojej normalnej formy
        * Eszara nie umie ruszać ŁZami
    * Eszara ma pogubione pojęcia i znaczenia. Zgubiła "znaczenie" i zyskała "inne" znaczenie.
        * Eszara ma DUŻO takich uszkodzeń
* Vz: Z perspektywy Klaudii sprawa jest jasna. Macie anomalię
    * Energia Altarient. Pytanie SKĄD.
* X: Nie ma dokładnego monitorowania nikogo, bo już się pokrzywiły
* X: Różne miejsca na Castigatorze już są pod wpływem Altarient. (--> droższa naprawa i trudniejszy teren)
    * koleś zamknięty w kiblu
    * radiatory Castigatora się rozwijają
        * Eszara: "mamy nadmiar ciepła. MUSZĘ go odprowadzić"
            * Infernia twierdzi że nie ma nadmiaru ciepła

Eustachy wraca spokojnie do baru. Bo Eustachy. I nagle słyszy krzyk. Z baru.

Ekspres do kawy roztopił szklankę. Na ziemię leje się kwas. Ekspres do kawy syntetyzuje kwas. Powinien się roztopić. (Znaczenie w świecie rzeczywistym ORAZ dodatek...)

Klaudia chce mieć pełny skan Castigatora. Jakie obszary są dotknięte? Co się dzieje? Jakie są bezpieczne trasy. I czy coś dotknęło AI Core? (NIE). Plus - interpolacja. Gdzie może być źródło?

Tr (+skanery Inferni -Castigator jest ekranowany +info od Eszary) Z +3:

* X: nie da się jednoznacznie określić miejsca. Są tylko predyktory.
* V: (p) gdzieś blisko reaktorów? W okolicach "centralnego korpusu", tam gdzie nie chodzisz chyba że strzelasz.

Tieni niekoherentni i ogólnie niekoherentna załoga jest bliżej inżynierii ale nie tylko. 

(scena: dwóch tienów: Igor Arłacz oraz inny, Konstanty Keksik siedzą i się przekrzykują "pomidor i bażant"! "jesteś głupi, to jest kura!"). Igor jest z technicznego, Konstanty z komunikacji.

Część załogi zaczyna się zbroić, by się chronić.

Infernia dostarcza na szybko generatory memoriam, Eustachy kombinuje, inżynierowie Inferni oraz inżynierowie Castigatora robią co mogą by jak najszybciej zabezpieczyć mostek. Wsparcie Patryka Samszara (tien), grupę inżynierów Castigatora - nawet część dotkniętych może pomóc. (jeden przenosił słoik dżemu zamist skrzynki narzędziowej). Plus, można podprowadzić Infernię blisko mostka i overchargować generatory póki jest budowanie.

Operacja zabezpieczania mostka potrwa. WPIERW - redukcja wpływu Altarient używając Inferni.

Tr Z +3:

* X: Część załogi informuje Castigator, że na statku pojawiły się potwory. Infernia nie wykrywa potworów. Więc część załogantów postrzega innych załogantów jako potwory.
    * "jeden inżynier spojrzał na Kurzmina i Ariannę z przerażonymi oczami i pobiegł w głąb Castigatora"
* V: Infernia, przeładowując Memoriam, jest w stanie chronić przynajmniej części Castigatora
* Vz: Solidna kochana Infernia. Osłania Castigator.
* V: Operacja wnoszenia aktywnych generatorów Memoriam na Castigator. Mostek jest osłonięty. "Przód" Castigatora.

Kurzmin: "ewakuujcie się z Castigatora, ALBO się nie ruszajcie. Nie strzelajcie, nie zbliżajcie się do niczego, zamknijcie oczy i myślcie o czymś miłym". Klaudia kieruje do kapsuł którym można jeszcze ufać.

Tr Z +2:

* Vr: (p) Część załogi, tej zdrowej, bezpiecznie się ewakuowała.
* Vz: (m) Wykluczyliśmy ofiary śmiertelne którym dało się zapobiec. Część osób jest "pozamykana" w kapsułach. Część kapsuł działa "inaczej"

Tieni patrzą na sytuację z wielkimi oczami.

Arianna: "DLATEGO nie robi się imprez i trzeba czujność."

Tp +3:

* X: Anna próbuje coś powiedzieć. Ale szybko inny tien ją ĆŚŚ.
    * Ania jest Dotknięta.
    * Arianna: "REKOMENDUJĘ KONSERWY RYBNE ALBO INICJATYWA!" (+1Vg)
* V: Anna tłumaczy: 'zielony kamień w rydwanie jagodowym, to jest problem'
    * Arianna daje jej kartkę
* Vg: Anna dostała kartkę. Jest wyraźnie zniecierpliwiona, ale narysowała coś. Coś co wskazuje na przemyt.
    * "Dlatego Ignacy jadł ekspres do kawy!"
    * Patryk (inżynier) "przed sprawą z atakiem był transport dóbr z planety i nie wiemy dokładnie... Konstanty to załatwiaj i przewoził"
    * Marta: "tak, zrzuć na mojego brata."
    * Patryk: "mówię poważnie, Ignacy mówił Konstantemu że coś jest nie tak, Konstanty mówił - już wtedy - że... e... kura musi znosić jajka."
    * Marta: "FAKTYCZNIE Konstanty ostatnio coś... ale my dobrze przemycamy."
    * Arianna: "Nie wątpię... Anomalia Castigatora..."
* X: Anomalia DOTKNĘŁA rzeczy koło. 
    * (radiatory są jak sieć pająka)
* Vr: Logi Eszary są "uszkodzone", są prawidłowe, ale rzeczywistość... jakby z 10-tnego na 8-kowy.
    * ale udało się Klaudii do tego dojść - Konstanty umieścił anomaliczne coś (Eszara widzi to jako "lalkę szmacianą") w Środkowo-Dolnych Radiatorach. Lalka jest TAM.
        * sęk w tym, że radiatory są... _inne_.

Od środka trzeba przejść przez NieKorytarze koło załogi która... nie do końca rozumie znaczenia i jest uzbrojona. Nawet się ostrzeliwują... ale... wiesz... tak... parę osób nie wie jak działa pistolet, stracili KONCEPT broni.

Od zewnątrz trzeba przejść przez Pajęczynę, która jest Radiatorem.

Klaudia przygotowała odpowiednie narzędzie utrzymywania w kupie: 6 luster skierowanych ku sobie. Na to lapis. Na to jakaś obudowa i silniczek. Powinno wytrzymać.

Raoul Lavanis. Advancer. Dostaje "paczuszkę" z instrukcją obsługi. Ma nie dotknąć pajęczynki. Ma przeprowadzić i przemanewrować, wyjąć "lalkę" nie dotykając jej i wsadzić do pudełka i wystrzelić w kosmos. W idealnej sytuacji niech lalka poleci gdzieś gdzie nie przeszkadza. Ale do tego jest Infernia.

Raoul bez zbędnych słów "tak jest, pani kapitan" wziął rzeczy i wyruszył w kosmos. Radiator w innej perspektywie jest jak spirala prowadząca w głąb. Więc Raoul używa silników, by nie wlecieć w "spiralę" ale zbliżyć się nie dotykając radiatora.

Tr Z (Klaudia) +3 +3Ob:

* Ob: Raoul zaczął zbliżać się do Castigatora, i nagle stanął na radiatorze, którego tam nie było. Ale jest. Jakaś forma zniekształcenia przestrzennego. Raoul błyskawicznie odskoczył, wraca do bazy. ASAP. Zanim mu servar skoroduje.
* Vr: Raoul ZDĄŻYŁ zanim doszło do Skażenia - rozpuścił servar zanim ten servar go Dotknął. To oznacza, że trzeba wyprodukować nowy. Raoul... wie już czegoś się spodziewać... (+Ob+Vr+Vg+3Oy (czas))

Tr Z (Klaudia) +4 +3Ob +3Oy:

* X: (p) Raoul zbliża się do Castigatora, ale nie da się do niego zbliżyć, przestrzeń... nie pod tym kątem. Niestety, Raoul musi podejść od strony spirali.
    * Raoul: "Cokolwiek to nie jest, da się to zrozumieć. A jeśli to zrozumiemy, możemy to pokonać."
* X: (m) Raoul zbliża się do Castigatora, ale nie zauważył, że pudełka nie trzyma. Pudełko sobie a Raoul sobie.
    * Klaudia: "Raoul, pojemnik."
    * Raoul: "Pojemnik? O co chodzi?"
    * Klaudia: "(na servar pinga o lokalizacji pudełka)"
    * Raoul: (patrzy) "Nie rozumiem. Wszystko się zgadza (zagubiony)"
    * Arianna: "Raoul WYKONAJ POLECENIA!" (i dokładnie jakie ruchy)"
* V: Raoul nie wie co się dzieje, nie rozumie poleceń Arianny i Klaudii, ale wykonuje polecenia. Trzyma pudełko, choć o tym nie wie.
* Vz: (p) Spirala zmieniła się w multihelisę. Czasem Raoul musi CZEKAĆ. Czasem musi PRZYSPIESZYĆ. Przesuwa się przez nią.
* Vr: (m) Raoul dotarł do serca anomalii. Do Lalki. Ale to NieLalka.
    * Istnieje KĄT w którym mieści się do pudełka oraz ten KĄT sprawia, że Raoul może otoczyć to pudełkiem. Acz Arianna i Klaudia tłumaczą co ma robić. Servar operuje na dziwnych kanałach.
* V: Raoul otoczył pudełkiem NieLalkę. Zamknął pudełko. Castigator - z biegiem czasu - wyjdzie z NieRzeczywistości Altarient.
* Vr: Udało mu się to włączyć i powoli pudełko odlatuje. Cała superstruktura też się porusza z nim, ale nie, bo pudełko nie jest częścią Castigatora.

Raoul wrócił na pokład Inferni. Jest zlany potem, Skażony (3 dni częstych pryszniców), nie do końca wie co robił, ale ma to... 'zrozumienie' w oczach.

Jak pudełko jest odpowiednio daleko, niszczymy je. Anomalia Altarient została unicestwiona.

..

ARIANNA. PRZEMAWIA. DO LUDNOŚCI! Jej celem jest to, by ci tieni wzięli się w garść i mieli OpSec. Żadnego przemytu. "Jak wasze drobne niedopatrzenia doprowadziły do katastrofy. Jedna mała niechciana rzecz to cały pokład popadnie w ruinę. Jedna mała NieLalka prawie zrobiła katastrofa na statku. Nie zorientowalibyśmy się nawet. A Wy na pewno. Działo się od tygodni i NIKT NIC NIE ZAUWAŻYŁ. Nie ma zmiany. Jesteśmy w kosmosie, nie na planecie. Nie w wygodnej willi z imprezami. Jest niebezpiecznie. Przylecieliście do Orbitera w jakimś celu. Takie anomalie jak ta, by krzywdziły ludzi których cenicie?"

Tp +3:

* V: Większość tienów będzie się starać, naprawdę starać.
* V: Większość tienów współpracuje z kapitanem, docelowo nie będzie "tienowie" i "Orbiter"
* V: Będzie wstyd bycia naleśniorem. Wreszcie bądźmy sensowni.

Epilog:

* Klaudia robiła biurokratyczną magię, by ukryć ten fakt że Coś Się Działo przed Admiralicją

PYTANIE: Jak działałaby magia w polu Altarient?

* mogłaby spełniać CEL, ale czy atmosfera nie wykrzywiłaby działania?

ODPOWIEDŹ:

* wykrzywiłaby, ale spełniałaby CEL tak jak postrzegasz go swoją siatką znaczeniową i pojęciową
* więc "tieni - nie czarować, Arianna/Klaudia - możecie"

PYTANIE: To co z tienami potem?

* ...czyli po wszystkim trzeba było straumatyzowanym tienom tłumaczyć, że ich wzór wcale nie uległ uszkodzeniu?
* Nie wierzę, że jak do siebie strzelali nie zdarzyło się któremuś czarować

ODPOWIEDŹ:

* tak. Niekoniecznie spotkali się z Altarient, to nie jest częsta Energia na planecie.

## Streszczenie

Na pokład Castigatora tieni przemycili NieLalkę Altarient i zaczęły mieszać się Znaczenia oraz Rzeczywistość. Zespół doprowadził do kontrolowanego opanowania i wygaszania Castigatora, po czym po zlokalizowaniu Anomalii Altarient Raoul został wysłany by ją zaizolować. Infernia ją zestrzeliła. A Arianna wykorzystała tą okazję by młodzi tieni wzięli się w garść i nie robili głupot. Szczęśliwie, Admiralicja nic nie wie, acz było nieciekawie przez moment.

## Progresja

* Anna Tessalon: wstrząśnięta przez wydarzenia na Castigatorze z NieLalką. Potrzebuje więcej oparcia, boi się działać sama. Ale znalazła komfort w głębszej przyjaźni z Patrykiem.
* Igor Arłacz: wstrząśnięty przez NieLalkę. Przeświadczony, że MUSI znaleźć sposób radzenia sobie z Altarient i dowiedzieć się o tej energii jak najwięcej. Straumatyzowany po sprawie.
* Natalia Gwozdnik: dzięki Ariannie została bohaterką, symbolem tego, że tien musi móc sobie poradzić w różnych sytuacjach i być maksymalnie kompetentną.

## Zasługi

* Arianna Verlen: korzystając z kryzysu Altarient na Castigatorze ustabilizowała tienów; niech zaczną się zachowywać jak Natalia. Prawidłowo deleguje - Leonę do Eustachego, potem Raoula do rozwiązania problemu Anomalii i wydaje mu precyzyjne rozkazy dzięki czemu osiągnął sukces. Podczas samego kryzysu z NieLalką ściśle wspiera Kurzmina i dowodzi izolacją kluczowych kompententów Castigatora.
* Eustachy Korkoran: Leona mu dokucza że jest 'Wielkim Duchem Piaskowym' i ściąga nań tienkę (Martę Keksik), on chce po prostu być zostawiony w spokoju na przepustce. Deeskalował 'służkę' Leony robiąc jej dowcip który doceniła. A potem unieszkodliwił tiena dotkniętego Altarient, który chciał by Eustachy pomógł mu wysadzić ekspres do kawy.
* Klaudia Stryk: diagnozując Eszarę dochodzi do Altarient; wspierając się danymi z Inferni izoluje lokalizację Altarient na Castigatorze, buduje odpowiednie pudełko do izolacji NieLalki Altarient i z Arianną sterują Raoulem, by jemu udało się prawidłowo rozwiązać problem. A potem -  biurokracja i dokumenty, by nikt nie wiedział że coś się tam naprawdę stało.
* Leona Astrienko: znudzona, wkręca tienkę że Eustachy to Wielki Piaskowy Duch (tytuł lorda Neikatis) i bawi się w herolda Eustachego; gdy trzeba było przechwycić tiena próbującego wysadzić ekspres do kawy, poczekała cierpliwie (jak nie ona) i go przechwyciła. Umie nie tylko robić dowcipy ale też je przyjmować.
* Leszek Kurzmin: próbuje chronić swoich tienów przed konsekwencjami biurokratycznymi oraz anomalią Altarient; widząc problem na Castigatorze szybko kazał containować jednostkę i ewakuować kogo się da. Zarządza administracyjną stroną Castigatora, by anomalia Altarient nie zniszczyła niczego ważnego.
* Elena Verlen: wiedząc o losie Natalii Gwozdnik, opuściła Castigator i dołączyła do sił komodora Walronda. Myśli, że to z uwagi na jej wysoką kompetencję i rekomendacje, bo jest Verlenką polującą na potwory.
* Feliks Walrond: komodor, który objął tymczasowo Elenę do operacji antykralotycznej i do odzyskania Natalii Gwozdnik. Zaakceptował prośbę Eleny do dołączenia do jego floty tymczasowo, bo Elena mu się podoba.
* TAI Eszara d'Castigator: nieświadoma infekcji Altarient, próbuje najlepiej jak potrafi zarządzać Castigatorem i utrzymała go w kupie mimo, że jej własne znaczenia się rozsypały i np. 'nie czuje hangaru ŁeZ'. Prawidłowo zrobiła 'graceful degradation' i jak tylko zorientowała się że nic nie działa, słuchała rozkazów Kurzmina a potem przestała robić cokolwiek (bo Znaczenia mogły się za mocno pozmieniać)
* Konstanty Keksik: tien na Castigatorze, zarządzał przemytem. To on ściągnął NieLalkę i umieścił ją by Kura Złożyła Jajko. Pierwszy najmnocniej zarażony, nie zrobił nic złego.
* Marta Keksik: tien na Castigatorze, poważna duelistka która chciała przez Trzy Pytania dotrzeć do Eustachego i poznać prawdę o Natalii (ona miała lecieć); chroni brata i przemyt do końca, ale ma sensowne podejście - w chwili kryzysu stanęła za Arianną i przestała przeszkadzać tienom. Ringleader.
* Igor Arłacz: tien na Castigatorze, chciał powstrzymać NieLalkę i przez to zaczął sabotować Hangar ŁeZ (zmiana Znaczeń). Współpracował z Eustachym (XD) przeciw 'ketchupowi' (ochronie Castigatora). Skończył w medycznym. Niegroźny, dość kompetentny we wszystkim i w niczym nie jest świetny.
* Anna Tessalon: tien na Castigatorze, bardzo wyczulona na wszystko co się dzieje i otwarta na wszystkie możliwości. Pierwsza chciała współpracować z Arianną, ale uszkodzenie Znaczeń jej to utrudniło. Za to jak dostała od Arianny kartkę potrafiła świetnie narysować przemyt, co uruchomiło Patryka.
* Patryk Samszar: tien na Castigatorze, szybko kojarzy. Inżynier i najbliższy przyjaciel Anny. Wpierw pomagał na mostku nie patrząc na ryzyko dla siebie, potem pomógł zinterpretować rysunek Anny i powiedział Ariannie o przemycie, mimo niezadowoleniu Marty (której wszyscy się trochę boją).
* Raoul Lavanis: nieskończenie opanowany advancer, wierzy, że da się wszystko zrozumieć ale przede wszystkim WYKONUJE ROZKAZY DO LITERY nawet jak ich nie rozumie. Wszedł w głąb anomalii Altarient i mimo że stracił pojęcie pudełka, idąc precyzyjnie zgodnie z poleceniami rozwiązał problem. Niezwykle precyzyjny, cierpliwy i stoicki. MVP sesji - ani Martyn ani Elena nie daliby rady.
* OO Infernia: overcharguje generatory Memoriam, podlatuje do Castigatora i chroni mostek używając swoich Memoriam. Jej przeładowanie generatorami Memoriam jedynie bardzo pomogło. Plus, zapewnia prawdę swoimi sensorami.
* OO Castigator: przez problemy z Saitaerem i awarię Memoriam doszło do manifestacji Altarient przez przemycony potężny artefakt źródłowy, 'NieLalkę', zmieniającą znaczenia, percepcję i rzeczywistość. Szczęśliwie udało się wszystko zachować 'w środku', by we flocie nikt niczego nie zauważył (przynajmniej, nie oficjalnie).

## Frakcje

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita

## Czas

* Opóźnienie: 2
* Dni: 2

## OTHER
### Status Skażony

* bardziej wrażliwy na energię magiczną i silniejsza magia, ale mniej kontrolowana
* ZAWSZE: +1X, +1Ob
* DODATKOWO JEŚLI MAGIA: 1V -> 1Vb, 1X -> 1Xb, +1Ob

### Status Rozproszony

* pod wpływem silnych emocji / myśli o czymś innym
* -1V, 3X -> 3Xg, +3Og
* Og reprezentuje "skupiony na swoich myślach i swoim działaniu" lub "działanie automatyczne"
* można wymienić Og na Or - sukces ALE zadajesz sobie ranę by się skupić

### Status Solidnie Ranny

* osłabiony i mniej zdolny do działania
* -1V, 2V -> 2Or (sukces ale dalsza rana); możesz usunąć Or

### Kształt Castigatora

1. **Część przednia (Głowica)**
    1. **Mostek, Dowodzenie**
        * Systemy nawigacyjne, sensory i komunikacyjne.
        * Command&Control – centralne miejsce kontroli zaawansowanych technologii statku.
    2. **AI Core**
        * Miejsce, w którym mieści się główna sztuczna inteligencja statku.
    3. **Pokład Życiowy 1**
        * System Podtrzymywania Życia.
        * Kwatery Załogi.
        * Rozrywka, mesa...
    4. **Czujniki i Systemy Obserwacyjne**
    5. **System komunikacji**
    6. **Systemy defensywne przednie**
    7. **Ekrany Ochronne**
2. **Pokład przedni dolny** 
    1. **Głowica działa strumieniowego**
    2. **Skrzydła - radiatory**
3. **Część środkowa (Korpus)**
    1. **Pokład Inżynieryjny**
    2. **Medical Treatment**
    3. **Pokład Życiowy 2**
        * System Podtrzymywania Życia.
        * Kwatery Załogi.
        * Rozrywka, mesa...
    4. **Ekrany Ochronne**
    5. **Hangar ŁZ**
        * Fabrykatory ŁZ
    6. **Hangar statków i pinas**
4. **Część środkowa (dolna)**
    1. **Pokład Generatora**
    2. **Pokład Chłodzenia**
    3. **Skrzydła - radiatory**
    4. **Pokład Zaopatrzenia Działa Strumieniowego**
        1. Akcelerator działa strumieniowego
    5. **Amunicja i wkłady Działa Strumieniowego**
    6. **Pokład Inżynieryjny**
    7. **Ekrany Ochronne**
    8. **Hangary wymiany elementów Castigatora**
4. **Część tylna (Sekcja napędowa)**
    1. **Silniki i reaktory**
    2. **Doki Serwisowe**
    3. **Systemy defensywne**
    4. **Pokład Zaopatrzenia**
