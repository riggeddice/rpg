## Metadane

* title: "Infekcja Serenit"
* threads: legenda-arianny
* gm: żółw
* players: kić, fox, kapsel

## Kontynuacja
### Kampanijna

* [210421 - Znudzona załoga Inferni](210421-znudzona-zaloga-inferni)

### Chronologiczna

* [210421 - Znudzona załoga Inferni](210421-znudzona-zaloga-inferni)

### Postacie, byty istotne i sytuacja

* Tadeusz Ursus, eternijski szlachcic
    * koleś od simulacrum
    * właściciel "Pięknej Eleny"
    * ostatnio czyścił reputację Arianny
* Infernia
    * ma spalony i uszkodzony system rozrywki
    * ma opętaną i specyficznie działającą Persefonę
    * OGÓLNIE DZIAŁA OK?
* Tomasz Sowiński
    * nienawidzi. Zamknął się z Jolką na serio.
    * robi strajk głodowy, czyli tylko 4 dobre posiłki dziennie, bez deserów.
* Jolanta Sowińska
    * jest naprawdę nieprzytomna. Martyn forever.
* komandosi Verlenów
    * zbierają dowody na Leonę
    * chcą pacyfikować Leonę
    * ...ale są zajęci trzymaniem statku do kupy
* Leona
    * jest zajęta trzymaniem statku do kupy
* "Morrigan d'Tirakal"
    * fabrykuje zło, by komandosi i Leona mieli co robić
    * w zasadzie niegroźna
* 2 dni po poprzedniej sesji

### Sesja właściwa

"Tu 'Piękna Elena'. SOS. Czy ktoś mnie słyszy. Tu 'Piękna Elena'." z Eteru. To nie jest typowy sygnał. Daleki, trudny, skomplikowany itp. Klaudia wszystkich informuje + podrzuca ID statku Elenie, która dalej jest zamknięta w pokoju.

TrZ+2: 

* V: Komunikat od TADEUSZA URSUSA. Klaudia nawiązała połączenie. 

Rozmowa:

* Klaudia: "mamy sygnał, tu Infernia, kapitan przyjdzie niedługo". 
* Tadeusz: "DZIĘKI WSZYSTKIM BOGOM!". Uratujcie nasz zamaskowany statek badawczy z awarią Persefony.
* Eustachy: "A co on robił"
* Tadeusz: Misja badawcza z anomaliami, celem podniesienia współpracy Aurum - Eternia. Dokładnie nie wie o co chodzi. Pozyskanie jakichś próbek.
* Eustachy, logi: statek nazywa się "Falołamacz". Niski poziom detekcji. Arogancki kapitan. Ogólnie kompetentna załoga. Nie są to siły specjalne, porządny "Orbiter", czasem statek "kaperski" a w ramach kaprowania można przewieźć... niewoln- tzn. "jeńców" na planetę. Bardzo szybka jednostka, raczej nie-militarna, lekko uzbrojona. Zwrotność + badania + infiltracja > siła ognia. Jakkolwiek statki Eterni rzadko używają TAI, ten używa TAI Persefony.

Eustachy + Arianna - i tak się dowiemy. GADAJ CO WIESZ. Jakie zagrożenia. (ExZ+3)

* X: Tadeusz potrzebuje od Eustachego RADY.
    * "Naucz mnie podrywać tak jak Ty umiesz, o kwiecie lotosu dla kobiet!!!" - "Nie." -> transmisja min do Eleny.
* V: 
    * Falołamacz miał pozyskać próbki pozwalające na stworzenie lepszego sentisprzężonego statku. Z różnych anomalii.
    * M.in. dużo akcji różnych advancerów.
    * Ale dodatkowo jest na pokładzie PRZEMYT. Jakoś trzeba dorobić, nie?
    * Czyli jest tam sporo sprzecznych interesów. Kapitan jest kompetentny i trzyma rzeczy twardo, ale... nie wszystko wie.
    * Persefona jest jednostką o maksymalnej mocy. Dziwne, że doszło do awarii.
    * <a tak w ogóle są tam fajne komponenty i Eustachy wie jakie można poszabrować>
        * tak, można poszabrować i będzie spoko :3.
        * tak, Eternijski statek nie jest... bardzo uznawany przez Orbiter.

Trzeba znaleźć Falołamacz w kosmosie.

ExZ+3, problem Klaudii. Klaudia mając sygnatury może poszukać czegoś podobnego. Można poszukać sygnatur zbliżonych / poprzesuwanych:

* V: Udało się relatywnie szybko zlokalizować gdzie w kosmosie powinien się znajdować ten statek. I gdzie może lecieć jeśli tam jest.

Chcemy się tam szybko dostać. Infernia jest szybkim statkiem, ale to jest sprawa dużej prędkości. Arianna musi się przyłożyć.

TrZ+2, Arianna jest w końcu dobrym pilotem ;-):

* XX: zajęło to chwilę. Infernia jest w gorszym stanie niż się wydawało. Arianna nie mogła ryzykować. Trzeba ponaprawiać silniki itp.
* V: udało się dotrzeć do miejsca, gdzie jest ślad i sygnał jednostki (Falołamacza).
    * udało się zlokalizować kierunek (+ zwrot Falołamacza) i znaleźli jednostkę.

Falołamacz leci na 0.15 prędkości maksymalnej w kierunku na Anomalię Kolapsu. Nie jest responsywny. Infernia jest szybsza.

Klaudii nie podoba się to, że reakcja radioelektroniczna Falołamacza jest taka dziwna. Chce użyć mocy swojej Persefony, by przeskanować transmisje, emisje - zlokalizować wszystko co może być nie tak.

ExZ+2:

* XX: HELLO, MORRIGAN!
* V: 
    * Q: "szukam czegokolwiek, co może być zagrożeniem dla nas jak się wbijemy"
    * A: wszystko wskazuje na to, że Persi jest na twardo wyłączona. Jednak reaktor + silniki działają. Life support działa. Statek działa, ale jest na oko bez Persi. Plus... ma _dziwną_ sygnaturę. Trochę anomaliczną, zwłaszcza w okolicach silników.
* V: Klaudia + Persefona wykryły dziwne krystaliczne narośla w okolicach silników. Silniki statku zdecydowanie mają własności anomaliczne. Nie do końca wiadomo o co chodzi, ale Klaudia może w dobrej wierze ostrzec o anomaliach. Statek może mieć własności anomaliczne.

Persefona interfejsuje z Klaudią, Klaudia odrzuca połączenie. Morrigan próbuje przejąć kontrolę nad czymś nad czym kontrolę ma. Samuraj może ją powstrzymać, ale trzeba go ZASILIĆ! Morrigan próbuje włączyć główne działa i otworzyć ogień do Falołamacza. Co samo w sobie jest dziwne.

Klaudia chce się dowiedzieć "ale czemu?"

* XX: Morrigan otworzy ogień. MAŁO CZASU by ją zatrzymać. (Najlepiej sabotować broń?).
    * : to nie jest przypadek. Morrigan _coś_ wie. Morrigan albo coś rozpoznała albo wiedziała. Czyli Jolanta powinna to wiedzieć.

Arianna: "UŻYJMY CZĘŚCI GOLDARIONA JAKO ŻYWEJ TARCZY!!!"

Eustachy: TrZ+2:    | <TODO>: PLUS DODAĆ EUSTACHEMU DO PROGRESJI: bonus do operacji niszczenia Inferni

* V: udało się przekierować ogień działa Morrigan.
* V: Infernia nie ucierpiała. Tylko Goldarionowa powłoka.
* XV: jest dość czasu, by Morrigan dało się unieczynnić innymi sposobami. Ale - Eustachy przegrzał bronie. COOLANT KRITIKAL! Małe, efektowne wybuchy nic nie zmieniające
    * Elena, zdziwiona: "ale... czemu? Czemu teraz niszczy statek? Bo się z nim nie umówiłam...? O_o. W SUMIE MA SENS."

Elena do Eustachego, komunikator.

* "Czy mogę Cię dowódco prosić byś nauczył Tadeusza Ursusa by nauczył się podrywać kobiety by zmienił nazwę swojego cholernego statku?"
* "Ale to bardzo ładna nazwa statku o co Ci chodzi"
* Spłoniła się i wyłączyła połączenie.

Co z Morrigan? Dezaktywacja Tirakala i niech Morrigan go opęta i marnuje czas próbując zrobić z niego zagrożenie.

Eustachy, mistrz sabotażu! Eustachy robi system defensywny, żeby jak Morrigan próbuje wejść do systemów to się odcinają i przestają działać. Klaudia + Eustachy + magia. ExZM+2:

* XX: część systemów Inferni, łącznie z tymi przydatnymi jest "ciemna". Za duża kaskada odcięć.
* V: udało się odciąć i dezaktywować Morrigan na pewien czas. W końcu Morrigan jest zamknięta w "pudełku" (systemie rozrywkowym). Zajmie parę dni zanim wyjdzie. Persi wróciła.
* V: Gdy Morrigan myśli że przejęła władzę nad Infernią:
    * troszczy się o Jolantę Sowińską - koniecznie ją budzi
    * unieczynnia załogę Inferni używając jednostek bojowych. Nie ma nic przeciw zabijaniu, jeśli ludzie się buntują.
    * oddaje dowodzenie nad statkiem Jolancie, ALE jeśli Jolanta zachowuje się "kralotycznie", Morrigan nie oddaje dowodzenia.
    * ZAWSZE zestrzeliwuje Falołamacza.

Infernia jest w pełnej kontroli Arianny (choć nie wszystko działa >.>). Teraz - trzeba rozwiązać problem Falołamacza.

Klaudia nawiązuje kontakt z Falołamaczem i przepuszcza ten kontakt przez zestaw filtrów anomalnych. Chodzi o to, by nie wpaść na killware czy dodatkowe uszkodzenie biednej Persi. 

TrZ+2:

* V:
    * automatyczny sygnał od Persefony: "Kapitan zachowuje się niebezpiecznie. Podejrzenie skażenia anomalnego. Podejrzenie skażenia Persefony. Wyłączenie jest niebezpieczne. Rekomendacja - samozniszczenie. Statek plag."
* V: "tu Falołamacz. Macie coś do naprawy Persefony? Mamy kawał metalu a nie Persefonę. Tu Rafał Grambucz. Łącznościowiec."

Klaudia wysyła info do Martyna co z nim może być nie tak: (ExZ+2)

* X: <odroczony>
* V: 
    * Martyn twierdzi, że koleś na pewno jest pod wpływem czegoś. Może pił, może coś innego.
    * Kapitan jest niebezpieczny. Forma Simulacrum krąży po statku i szuka potwora, ale nie wiadomo czy Simulacrum czy Potwór są groźniejsze. Są ofiary śmiertelne. I grzyby na suficie.

I co Arianna na to? ^___^

Holujemy tractor beam statek - wiemy, że jest niesterowny i leci na "autopilocie" (na bezwładności). Więc tractor beam pozwoli nam na to, by zmienić jego wektor i zwrot i nie musimy go dotykać i się brudzić / Skazić :3.

Ku wielkiemu zdziwieniu Arianny, tractor beam nie zadziałał. A dokładniej - zadziałał. Ale Falołamacz skorygował kurs. Czyli to nie jest niesterowana rzecz w formie inercji. Coś, jakoś, gdzieś steruje kursem tego statku. I Falołamacz zaczął przyspieszać.

To znaczy, że jest tam jakaś _Wola_. Coś czegoś chce. A Persi jest stopiona do metalu - to na pewno nie ona.

OPERACJA: KLAUDIA I ELENA. Elena pobiera próbkę. Klaudia bada próbkę.

Elena i Klaudia biorą klasyczne Lancery (zmodyfikowane do działania próżniowego). Klaudia i Elena robią kosmiczny spacer by dostać się do Skażonych silników (i nie wpaść prosto w wylot).

Elena prowadzi kosmiczny spacer (TrZ+1):

* V: udało się dotrzeć do celu
* X: z problemami; nie udało im się w pełni przez zakłócenia i anomalie; coś TAI na pokładzie Lancerów szaleją. Musiałyście lądować na pokładzie niedaleko silników.
* V: Elena pobrała bez większego problemu próbkę z silników. To nie są grzyby - to jakaś krystaliczna narośl. Coś wyglądajęcgo jak... skała? Kryształy? Bardzo nieorganiczne.
* V: Klaudia rozpoznała objawy. SERENIT.

Elena + Klaudia próbują się ewakuować ze statku dotkniętego przez komponent Serenit. ExZ+2:

* X: kamienne struktury Serenitu próbują złapać Elenę i Klaudię.
* V: Elena skutecznie ciąga Klaudię i manewruje.
* EUSTACHY STRZELA:
    * V: Udało się KANONIERKĄ i SIŁĄ OGNIA odeprzeć struktury i Elena z Klaudią ruszyły w kosmiczny spacer powrotny, gdy działka Inferni osłaniają piękne kosmiczne damy.
* Eustachy kupił dość czasu - Klaudia dała radę określić, że Elena ani Klaudia nie mają Skażenia Serenitem. Są czyste. Lancery... może. Zostawią je gdzieś w próżni.

Po zostawieniu ich w kosmosie Eustachy dla sportu je zestrzelił.

Na pokładzie może nie być potwora? Jest skażenie Serenitem, to jest pewne... Ale jak to uratować? Ping do Martyna - mało kto wie o Serenicie tyle co on.

Martyn poprosił Klaudię, by ta sprawdziła na dalekosiężnych sensorach czy Serenit nie leci w tym kierunku aby. Klaudia chce sprawdzić ile mają czasu na operację ratunkową.

* XX: Klaudia włączyła zaawansowane sensory i puściła wiązkę. Serenit Odpowiedział. Klaudia natychmiast spaliła sensory. (TRZEBA NAPRAWIĆ, poszły ścieżki a nie sensory właściwe!)
* V: 3-4 godziny aż NIE BĘDZIE SIĘ DAŁO OD SERENITA UCIEC. Serenit leci tu z dużą prędkością i wabi do siebie ten statek. A ten statek leci na Serenit.
* : na pokładzie Falołamacza są jeszcze ludzie.

Martyn mówi o Serenicie:

* najpewniej ten statek (Falołamacz) został zainfekowany przez Serenit nie przez to co przynieśli ale przez advancera który wrócił. Nigdy advancer nie wrócił, wróciła "podróbka". -> Serenit spróbuje wsadzić na statek agenta.
* agent próbuje przejąć kontrolę nad jednostką przez atak na indywidualne osoby. Np. kapitana. Destabilizuje połączenia mentalne u ofiar. Ofiary Serenita nie operują z "nadrzędnym celem" - nie wiedzą, że Serenit "im to robi".
* forma krystaliczna na silnikach musiała być przyniesiona z Serenita. Ogólnie, Serenit próbuje użyć środków statku (np. Persefony).

Innymi słowy, ewakuacja ludzi jest niebezpieczna i trudna.

Arianna chce ich ratować. Więc jak to zrobić? Na szczęście, od tego jest Eustachy.

PLAN EUSTACHEGO:

* odstrzelić silnik i statek będzie wolniejszy.
* ludzi wyssie w kosmos i się ich uratuje
    * lub nie
* a potem coś zrobimy z pokładem. O.

## Streszczenie

Ciężko uszkodzona Infernia wraca do domu i dostaje SOS od Ursusa - OE Falołamacz "zniknął", coś z anomaliami. Gdy Zespół dotarł do Falołamacza, próbowali dowiedzieć się co się dzieje - faktycznie mamy anomaliczny okręt. Podczas badań Falołamacza Morrigan przejęła Persefonę Inferni; Eustachy musiał poważnie uszkodzić Infernię by nie zniszczyli Falołamacza. Po kosmicznym spacerze Eleny i Klaudii doszli do tego co się stało - Falołamacz staje się Odłamkiem Serenit. I jeszcze da się ludzi ratować. Ale Infernia jest w bardzo złym stanie...

## Progresja

* Eustachy Korkoran: bonus do niszczenia Inferni. Nie "pozorowanego" a "faktycznego". Eustachy potrafi niszczyć / sabotować Infernię jak nikt inny.
* OO Infernia: przeciążone silniki. Ledwo sprawne, przegrzane bronie. Martwy lifesupport. Sensory poodcinane. Systemy niekrytyczne nie działają. Statek ledwo trzyma się kupy.

### Frakcji

* .

## Zasługi

* Arianna Verlen: dowiedziała się od Tadeusza wszystkiego co trzeba o Falołamaczu, potem na podstawie wyliczeń Klaudii poleciała w kierunku na przewidywaną lokalizację Falołamacza. Silniki ledwo wytrzymały (uszkodzone).
* Eustachy Korkoran: rozpaczliwie przekierował ogień Inferni (Morrigan) na resztki Goldariona, sabotował Infernię by odciąć Morrigan i osłaniał artyleryjsko Klaudię i Elenę jak uciekały z Falołamacza przed Odłamkiem Serenit.
* Klaudia Stryk: obliczyła gdzie powinien być Falołamacz (był tam), próbowała zatrzymać Morrigan (nie wyszło), po czym zrobiła z Eleną kosmiczny spacer i wykryła że walczą z Odłamkiem Serenit...
* Tadeusz Ursus: posiadacz statku "Piękna Elena". Prosi Eustachego, by ten nauczył go podrywać. Prosi Ariannę o pomoc dla OE Falołamacz i przekazuje jej wszystko co wie na temat Falołamacza (nawet czego nie powinien)
* Elena Verlen: zażenowana "Piękną Eleną" Tadeusza Ursusa; robi kosmiczny spacer na Falołamacz i ratuje Klaudię przed złapaniem przez Serenit (mistrzyni manewrów i uników).
* Persefona d'Infernia: Aspekt 'Morrigan' próbuje zestrzelić Falołamacza, bo wykryła Serenit. Morrigan przejęła Falołamacza aż Eustachy wyłączył pół Inferni by Morrigan unieczynnić.
* Rafał Grambucz: łącznościowiec z Falołamacza. Wpływa na niego Serenit, więc nie jest składny i logiczny. Odpowiada na pytania Inferni i dzięki temu Zespół wie, że Falołamacz jest anomaliczny.
* Martyn Hiwasser: rozpoznał zdalnie problemy Grambucza (anomalizacja). Gdy dowiedział się, że walczą przeciwko Serenitowi, przekazał Klaudii informację że mają mało czasu - faktycznie, Serenit tu leci.
* OO Infernia: bardzo ciężko uszkodzona; poszły silniki, life support i sporo innych komponentów. Morrigan przejęła Persefonę. Ale dalej skutecznie służy Orbiterowi i wykonuje misję.
* OE Piękna Elena: okręt dowodzony przez Tadeusza Ursusa, który rozpaczliwie szuka pomocy dla OE Falołamacza.
* OE Falołamacz: wpakował się na Serenit. Serenit go zaczął infekować, zmieniając go w "odłamek Serenit". Falołamacz jest stracony, ale da się jeszcze uratować załogę - i to próbuje zrobić Infernia.
* AK Serenit: leci w kierunku na OE Falołamacz (który staje się "odłamkiem Serenit"). 

### Frakcji

* .

## Plany

* Tadeusz Ursus: święcie przekonany o mistrzostwie Eustachego wobec podrywania dziewczyn, chce by ten nauczył go wszystkiego co wie.

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 3
* Dni: 1

## Konflikty

* 1 - Klaudia wzmacnia sygnał z "Pięknej Eleny" by dało się skomunikować z Tadeuszem Ursusem.
    * TrZ+2
    * V: jest nawiązanie połączenia
* 2 - Arianna - i tak się dowiemy co się dzieje na Falołamaczu. GADAJ CO WIESZ. Jakie zagrożenia. Bo nie pomożemy.
    * ExZ+3
    * XV: Tadeusz chce od Eustachego rad o podrywanie. SENPAI! Dowiedzieli się o naturze Falołamacza, że advanceruje po anomaliach.
* 3 - Klaudia mając wiedzę o silnikach i systemach Falołamacza (dość ukrytego) szuka go w kosmosie. Gdzie może być. Bardziej matematyka/wiedza niż sensory.
    * ExZ+3
    * V: ma szybko odpowiedź gdzie POWINIEN być.
* 4 - Arianna leci w tamtym kierunku - jest świetnym pilotem, chodzi o naciśnięcie systemów Inferni.
    * TrZ+2
    * XXV: Infernia jest w dużo gorszym stanie niż się wydawało, silniki uległy mocnym uszkodzeniom. Infernia powoli się rozpada. Ale doleciała, poszukała i znalazła.
* 5 - Klaudii nie podoba się to, że reakcja radioelektroniczna Falołamacza jest taka dziwna. Chce użyć mocy swojej Persefony, by przeskanować transmisje, emisje - zlokalizować wszystko co może być nie tak.
    * ExZ+2
    * XXVV: Morrigan AWAKEN. Falołamacz działa... bez Persefony? Jak? Ma krystaliczne narośla niedaleko silników - własności anomaliczne.
* 6 - Klaudia próbuje zatrzymać Morrigan, odciąć ją od systemów Inferni.
    * Tr+2
    * XX: Nie udało się. Potrzebny sabotaż. Morrigan zdominowała Persefonę i chce zniszczyć Falołamacz.
* 7 - Eustachy: plan Arianny - użyć Goldarionowej powłoki do przekierowania ognia Morrigan.
    * TrZ+2
    * VVXV: udało się, powłoka Goldariona zniszczona. Infernia nie ucierpiała. Ale bronie Inferni są przegrzane, coolant critical. Kolejne eksplozje na Inferni. Ten statek się rozpada...
* 8 - Eustachy robi system defensywny, żeby jak Morrigan próbuje wejść do systemów to się odcinają i przestają działać. Klaudia + Eustachy + magia. ExZM+2:
    * ExZM+2
    * XXVV: Morrigan w pudełku, ale WIĘKSZOŚĆ systemów Inferni są "ciemne". Infernia ledwo działa. Nie działa np. life support, medical... - ale Morrigan myśli, że kontroluje Infernię.
* 9 - Klaudia nawiązuje kontakt z Falołamaczem i przepuszcza ten kontakt przez zestaw filtrów anomalnych. Chodzi o to, by nie wpaść na killware czy dodatkowe uszkodzenie biednej Persi. 
    * TrZ+2
    * VV: autosygnał od Persefony, łącznie ze Skażeniem anomalią. Samozniszczenie. Statek plag. Plus odpowiedź od łącznościowca Grambucza - czy mają coś do naprawy Persi?
* 10 - Na podstawie rozmowy z Grambuczem Martyn określa co może być nie tak z załogą
    * ExZ+2
    * XV: Grambucz jest pod wpływem czegoś. Załoga nie ma nad sobą kontroli i są niebezpieczni.
* 11 - Klaudia i Elena robią kosmiczny spacer by dostać się do Skażonych silników (i nie wpaść prosto w wylot); prowadzi Elena.
    * TrZ+1
    * VXVV: dotarły do celu, TAI na pokładzie Lancerów szaleją. Elena pobrała próbkę a Klaudia rozpoznała objawy. SERENIT!!!
* 12 - Elena + Klaudia próbują się ewakuować ze statku dotkniętego przez komponent Serenit. Unikanie ataków + kosmiczny spacer powrotny.
    * ExZ+2
    * XVV: struktury łapią Elenę i Klaudią, rozpełzają się. Elena skutecznie ciąga Klaudię i manewruje. Eustachy osłania ogniem. Lancery - pełna moc, odlatujemy!
* 13 - Klaudia: ile jeszcze czasu mamy na operację ratunkową? Czy Serenit leci w tą stronę?
    * XXV: Serenit odpowiedział. Leci tu. Klaudia spaliła sensory by pozbyć się ryzyka. Mają 3-4 godziny aż się nie będzie dało uciec od Serenita.

## Inne

.