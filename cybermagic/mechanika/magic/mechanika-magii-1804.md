---
layout: mechanika
title: "Mechanika magii, 1804"
---

# {{ page.title }}

## 1. Aktualność dokumentu

* Dokument jest aktualny wobec mechaniki 1804; działa z kartami postaci 1804
* Dokument jest wyprodukowany: 180430
* Ostatnia aktualizacja: 180430
* Krystalizowana koncepcja magii: [O magii na szybko](o-magii-na-szybko-1804.html)

## 2. Wpływ magii na Konflikt

![Pokazanie poniższych punktów w formie rysunku](Materials/1804/1804-magic-influence-on-conflict.png)

* Użycie magii kosztuje dodatkową Kartę Akcji
* Użycie magii gwarantuje obniżenie stopnia trudności o 1 kategorę (Trudny -> Typowy, Typowy -> Łatwy...)
* Użycie magii dodaje [1, 4] Żetony Sukcesu o kolorze Magia i zmienia 1 Żeton Porażki na kolor Magia
* Wyciągnięcie Żetonu o kolorze Magii powoduje Efekt Skażenia

## 3. Zasady rzucania zaklęć

![Jak rzucać zaklęcie: źródło energii: drzewa, mag ma wolę i uczucie, adresem jest tarcza](Materials/1804/1804-anatomy-of-a-spell.png)

Zaklęcie składa się z czterech komponentów:

* **źródło** - skąd czerpiemy energię
* **adres** - co ma być punktem manifestacji zaklęcia ("celem")
* **wola** - co chce osiągnąć mag, co ma się stać
* **uczucie** - jakie uczucia kierowały magiem

By rzucić zaklęcie, Gracz musi odpowiedzieć na cztery pytania:

* Skąd czerpiesz energię?
* Co chcesz osiągnąć? Co ma się stać?
* Co czujesz? Jakie uczucia Tobą kierują?
* W co kierujesz swoje zaklęcie?

Powyższe pozwalają na sformułowanie sprawnego zaklęcia.

## 4. Zasady przydzielania żetonów

### 4.1. Ogólnie

Za każdym razem dodawane żetony Sukcesu i Porażki są w kolorze Magii. Nie są bezbarwne.

### 4.1. Źródło

![Poniższa tabelka w formie rysunku](Materials/1804/1804-mechanics-of-a-source.png)

* Jeżeli źródłem jest **energia magiczna tła**, niezdefiniowana, **dodaj 1 Żeton Sukcesu**
* Jeżeli źródłem jest **czarujący mag (self)**:
    * **dodaj 2 Żetony Sukcesu**
    * **przesuń jego licznik Wyczerpania o 1**
    * jeśli mag jednocześnie jest Adresem jak i Źródłem, czar jest trudno wykrywalny
* Jeżeli źródło jest **określone, sensowne i dostępne, dodaj 2 Żetony Sukcesu**
    * sensowne, np. "coś z prądem do magii związanej z technologią" czy "coś żywego do biomancji"
    * dostępne, np. w zasięgu dotyku czy adresowanie. Też: nie jest to vicinius / mag którzy są przeciwni.
* Jeżeli źródłem jest **Węzeł, Pływ czy odpowiednik, dodaj 4 Żetony Sukcesu**

Przykłady:

* _Ania rzuca czar leczący. Jako źródło energii specyfikuje otaczające ją rośliny. Dostaje 2 Żetony._
* _Basia próbuje zagregować informacje w internecie. Jako źródło energii używa podłączonego do prądu komputera. Dostaje 2 Żetony_
* _Celina próbuje zapalić światło magicznie. Jako źródło energii używa energii tła. Dostaje 1 Żeton._

### 4.2. Wola i Uczucie

* Jeżeli mag jest pod wpływem silnych uczuć:
    * i te uczucia są spójne z zaklęciem (np. nadzieja / leczenie), **dodaj 1 Żeton Sukcesu**
    * i te uczucia są sprzeczne z zaklęciem (np. nadzieja / czar niszczący), **dodaj 1 Żeton Porażki**
* Jeżeli wola i uczucia maga są wspierane przez Pryzmat, **dodaj 1-3 Żetony Sukcesu**
* Jeżeli wola i uczucia maga są zwalczane przez Pryzmat, **dodaj 1-3 Żetony Porażki**

Innymi słowy, nie warto czarować, jeśli odpowiednio duża grupa wierzy w coś sprzecznego, bo energia magiczna może obrócić się przeciw magowi.
