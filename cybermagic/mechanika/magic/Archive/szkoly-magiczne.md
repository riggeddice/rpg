---
layout: mechanika
title: "Szkoły magiczne"
---

# {{ page.title }}

## Enumeracja

### Kiedyś

#### Dawno, dawno temu: 
Sztywne domeny magiczne (elementy):

* Terram
* Igni
* Auram
* Aquam
* Spiritus
* Vim
* Alucinor
* Causa

Oraz usztywnione metody 
* Creo
* Perdo
* Muto
* Intelligo
* Rego

Zalety: dość proste składanie zaklęć
Wady: słaby opis postaci, spowolnienie systemu.

Najpierw wystarczyło wybrać domenę i dostępne były wszystkie metody, później należało wybrać również metody i dostępne były jedynie te "wybrane" połączenia.

Zalety: nikt nie mógł zrobić wszystkiego
Wady: spowolnienie, ogólna toporność systemu.

#### Później:
Całkowita dowolność w wyborze "rodzaju" magii.

Zalety: dobry opis postaci.
Wady: niejasna pożądana szerokość domen magicznych

#### W końcu:
Magię można wykorzystać do każdej czynności, na której postać się zna, daje matematyczny bonus.
Zalety: niski koszt kognitywny
Wady: mylące dla początkujących i doprowadziło do utraty poczucia, że magia jest magią

### Działy w firmach (czyli spojrzenie przez analogię):

* Księgowość        infomancja
* HR                zmysły, mentalna
* Marketing         mentalna, astralika
* Sales             mentalna
* Audyt i jakość    mentalna, materia, infomancja
* Logistyka         transport
* Zaopatrzenia      materia, transport
* Produkcja         technomancja, materia, xxx
* Infrastruktura    materia, technomancja, elementy, xxx

## Propozycja

### Niezależne szkoły magiczne
Istnieje z góry określona ilość szkół magicznych, które obejmują pełen zakres magii.
Każda szkoła może zostać wyspecjalizowana w konkretny "zawód" magiczny.
Niektóre specjalizacje są specyficzne dla szkoły magicznej, inne mogą wykorzystywać kilka szkół.

**Rozważane przez nas szkoły**:
* biomancja
    * szeroko pojęte "życie"
* technomancja
    * szeroko pojęta technologia - kłódki, urządzenia, komputery (raczej od strony "mechanicznej")
* infomancja
    * informatyka, dokumenty, praca z danymi
* magia mentalna
    * dotyczy każdej istoty świadomej siebie (posiadającej umysł i osobowość)
* magia materii
    * materiały, przedmioty, tworzenie, przemiana...
* astralika
    * emocje, nastroje, pryzmat
* elementalna
    * cztery żywioły, elektryczność, źródła energii
* kataliza
    * manipulacja KAŻDEJ energii magicznej
* magia zmysłów
    * iluzje
* magia transportu
    * przestrzeń, kineza, przemieszczanie się...

**Możliwe (ale nie jedyne) specjalizacje w ramach szkół**:

* biomancja
    * rośliny
    * zwierzęta
    * ludzie
    * leczenie
    * rolnicza	
* technomancja
    * full technology edit
    * kłódki, narzędzia, maszyny
* infomancja
    * informatyka
    * dokumenty
* magia mentalna
    * kontrola i dominacja
    * każdej istoty świadomej siebie
    * uroki i dominacje
    * umysły i osobowości
* magia materii
    * materiały
    * budowlanka
* astralika
    * pryzmat
    * echa emocji, spojrzenie wstecz
    * nastrój grupowy
* elementalna
    * cztery żywioły
    * elektryczność, źródła energii
* kataliza
    * manipulacja KAŻDEJ energii magicznej
    * puryfikacja
    * plątanie, rozplątywanie magii, energii...
* magia zmysłów
    * iluzje
* magia transportu
    * przestrzeń
    * portale
    * "upychanie"
    * kineza


### Specjalizacje wielu szkół:

* nekromancja       (spec astraliki i biomancji)
* golemancja        (spec...?)                 
* alchemia
* artefakcja
* uwodzenia         (spec zmysłów, bio...)
* demonologia       (spec katalizy i elementalna?)
* imprezowa         (spec zmysłów i materii LUB biomancji)
