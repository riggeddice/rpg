---
layout: mechanika
title: Intention path-cut-off system
---

# {{ page.title }}

# The story:

The battle between the Crystal Empire and the Dominion still waged. This was but one of the skirmishes… Two groups of starfighters and several capital ships from both sides…

Eileen has shot down another Dominion starfighter. In the corner of her eye she noticed her friend, Terry, being endangered. She noticed the blip on the radar - which was the missile flying towards Terry’s starfighter. She didn’t think twice – shot the flak antimissile cannon and destroyed enemy missile; although it was risky for Terry, she was a marksman. She could make it.

“Eileen, look out!” she heard and made a drastic turn. Sadly, too late. She was so focused on saving Terry, that she got hit by a plasma beam. She looked at the cockpit - systems damaged, but her trusty ship was still operational. 
She has managed to confuse two enemy starfighters - she was an ace pilot after all – and approached a strange looking supply ship… and then she heard an alarm. A stealth destroyer has appeared near her position and – as she was obscured by enemy supply ship - it has hit Terry’s fighter with a stream cannon – glanced it. Fortunately, she could see his life support was still working, but he was slowly starting to descend into the toxic atmosphere of the gas giant...

And Terry was going to be killed the next hit.

“Command, I’m going to ram into that destroyer. Pick me up, please” - she calmly informed her command officer, even if her heart was racing. She prepared the overload routine to maximize the damage from turning her ship into projectile, focused on avoiding the point defenses from the destroyer and flew in its direction the fastest she could… she just had to save Terry.

She did take full attention of the destroyer. But she was an ace pilot after all. She was so close…

Not close enough. She didn’t count on debris. She hit… something, but it was enough for her ship to miss the target. She ejected and saw her trusty ship be destroyed by point defense of the destroyer in vain... and she was next, a helpless pod in the space.

Then, a flash of light. Enemy destroyer got ripped apart by Alice, her rival. Both Eileen and Terry were safe… but for both of them the battle was over.

“Hey, lousy!” - Eileen heard in intercom – “next time try better because I’m not going to save you every time..." 
Of course it was Alice… it just HAD to be Alice.

Alone in the dark, watching the explosions from the battle they have almost won, Eileen had to answer the same question again - whom does she hate more - the Dominion or Alice…

# Explanation

## Prelude

### Typical mechanical RPGs:

So, what you have just read was not my attempt to write horrible prose. This was a small set of conflicts in a role-playing game with the outcomes. As in, this is what happened in fiction. This was enabled by the intention system of the mechanics.

Did your role-playing session look like this?

I am probably not better game master than your game master, the player of Eileen is probably not better than your players. Yet the tools most people are using do not allow them to do something like this.

For example, let’s take something from the book. A girl. Trying to swim underwater. Trying to get the box to the surface. How would it look like in the system like… ”Typical RPG”?

* _**Game Master**: “make a roll against strength + lifting”_
* _**Player**: “success”_
* _**GM**: “okay, so you are halfway there. Again.”_
* _**Player**: “failure”_
* _**GM**: “shame... Okay, the box slips from your hands and falls down.”_
* _**Player**: “okay, so I get out of the water, sit down for a bit and recover. Afterwards I try to get the box.”_

Nothing changed. The rules of statistics say the player will eventually get the box from the water (two consecutive tests against 30%, for example, mean that in 100 tries the player would have to be extremely unlucky). What is more, because the player can repeat the action, effectively this whole part is irrelevant. It just takes time. It doesn’t really mean anything, doesn’t push the story forward. Have you ever seen something like this in a book on the movie? It would be boring and simply dumb.

So, let’s try it again:

* _**GM**: “this place is deep. Very deep. If you slip it, you lose that box.”_
* _**Player**: “okay… But I really want that box. I will try using… Snake skin gloves for better handling.”_
* _**GM**: “good idea, actually. Take +3 modifier.”_
* _**Player**: “thanks!”_
* _**GM**: “make a roll against strength + lifting”_
* _**Player**: “success”_
* _**GM**: “okay, so you are halfway there. Again.”_
* _**Player**: “failure”_
* _**GM**: “shame… Okay, the box slips from your hands and falls down.”_
* _**Player**: “and I can’t really recover it?”_
* _**GM**: “no, sorry. It’s just… impossible. To deep. You have to have magical items.”_
* _**Player**: “sucks :-(”_

In this case the result is binary. The player really wanted the box, it was not really important for the game master. However, the game master has to provide some kind of conflict – and there was nothing game master could actually take from the player other than the box. The player could have been really invested in that box; he could have even communicated that to the game master, but the rules of the game they were playing did not let the game master do anything with it. That is, assuming the game master even noticed - game master has a lot to think about and being a mind reader is not the easiest (sadly, not the hardest either) of the stuff they have to do.

### Storytelling-based RPG:

Very often game masters and players eschew mechanics completely and go for storytelling. In the language introduced in this and previous documents, this means that the game master gets the role of the processor - game master decides what happens in fiction. But this means that game master effectively creates the reality and decides which of the players’ actions are actually implemented into the fiction. In other words, player lives in an illusion that her choices matter. If the game master wants something to happen it will simply happen.

So how would this case look like in the storytelling mode?

* _**GM**: “you can see the black box covered in plans on the edge of the abyss…”_
* _**Player**: “I swim up to it, pick it up with my gloves and furiously swim upwards”_
* _**GM**: “it is too heavy! It is so heavy… Your lungs scream for air… You will not make it!”_
* _**Player**: “No! I must! I recall the spirits of my ancestors, I recall the spirit of the fish, of my tribe. I will persevere!” (Side note: this happened. Really.)_
* _**GM**: “you can’t… But... you have noticed a place you could put the box and recover.”_
* _**Player**: “Praised be the fish totem! I put the box there, get to the surface and rest. I will return with new strength to get the box to see the sun once again.”_

Notice something interesting? There are no conflicts here. There is only an illusion of a conflict. Game master puts fake challenges up to the player and the player tries to make the game master resolve the reality in a way player wants it to happen. Don’t get me wrong – this is fun for both parties. But the only side having real power here is the game master. Player is just a spectator who is trying to convince game master to let some things go her way. A good game master playing this way would let the player win the most important things but depending on the temperament of the player the player might not ever be at risk of losing anything. Consider the situation below:

_**GM** (Kate, 17 years old): “a Dragon landed on the field, spewing fire and destruction!”_
_**Player** (Jake, 14 years old, Kate’s brother): “I ran towards him with my two-handed sword screaming the battle cry of the berserkers!”_
_**GM** (Kate): “the Dragon just killed 10 people with one swing of his claws. You feel it might be too strong for you.”_
_**Player** (Jake): “I am a barbarian and I fear nothing! I cannot retreat, I never run away!”_

I have seen this scenario many times. Jake is really invested in his berserker. He created this character to be an arrogant and bloodthirsty warrior. He loves this character. He will not run away. And Kate made the Dragon in such a way that there does not exist a statistical chance for Jake to survive. And they are playing storytelling.
What do you think would happen? Kate won’t kill Jake’s character. So the fiction gets broken – either Kate neutralizes a Dragon with angelic forces or something (to preserve a barbarian) or Jake’s barbarian will kill a dragon. Yay.

In both cases there is a copout. Some would say that a game master made a mistake not taking into account character’s personality. But really, does a game master have to remember and do everything here?

From my point of view this model is broken. Because Jake wanted to fight a Dragon. And Kate either kills his barbarian (binary) or takes this fight away from him. Jake does not control his barbarian’s destiny. The only meaningful person sitting at this table is Kate - Jake can only color the picture she is painting - and only with colors she lets him use.

So basically, at the heart of storytelling there can be extremely awesome atmosphere - and I know it can, because I played this way. But there is no tension. This is a theme park, similar to the cinema with some level of interaction and adaptation from the game master to the players decisions, but in the end if the game master doesn’t like something he can always override it. He is the processor and the creator. Effectively, he is a God. 

… so why do we need Jake in this game? Why doesn’t Kate write short stories and publish them somewhere?

### Path cut-off intention based system:

So let’s go back to the example with the box underwater. How would like it to happen? First of all, I would like there to be some tension - the girl can get the box if she wants to; I mean, the player does, game master doesn’t care - why not? But the player might have to pay for the box losing the precious resources her character could have used on something else. 

Let’s do it again, then:

* _**GM**: “this place is deep. Very deep. If you slip it, you lose that box.”_
* _**Player**: “okay… But I really want that box. I will try using… Snake skin gloves for better handling.”_
* _**GM**: “good idea, actually. Take +3 modifier.”_
* _**Player**: “thanks!”_
* _**GM**: “make a roll against strength + lifting”_
* _**Player**: “success”_
* _**GM**: “okay, so you are halfway there. Again.”_
* _**Player**: “failure”_
* _**GM**: “shame… Okay, the box slips from your hands and falls down.”_
* _**Player**: “unacceptable. I want that box.”_
* _**GM**: “okay, so the box slips down but you managed to catch it and you get wounded in the process - it is rusty and has a lot of edges. Fair enough?”_
* _**Player**: “okay, I can live with it”_
* _**GM**: “make a reroll then, but with a bonus +1”_
* _**Player**: “failed again… Those dice hate me today”_
* _**GM**: “okay, I will let you get the box to the surface – but your enemies will be waiting there for you.”_
* _**Player**: “I think it’s too harsh - can you just poison me with the foul water here? I will have to recover…”_
* _**GM**: “three days of fever for the box?”_
* _**Player**: “let me remind you I am low on food”_
* _**GM**: “deal”_

Let’s analyze this case. It is closer to the first example (the player can reroll as many times as she wants), but every failure takes something away from the character or her possibilities. What is taken away is negotiated between the player and a game master. The player can suggest her own negative status or situation in game. 

What is more, the player said “I want that box”. The player is invested in this particular storyline. The game master cannot take the box from the player - this conflict did not have it “in stakes” (it was not the conflict “will I get the box or not?” But a conflict of “how far will I go to get that box - consequences?”).

Note, that both the player and the game master can generate the reality even outside the scope of the conflict – “your enemies will be waiting there” and “poison me with the foul water”. 

This means that tension exists here - the player character can, in effect, lose a lot of things (during 3 feverish days a lot of stuff could have happened). But the player decided that the topic which interested that player was this box. So the player prioritized this story thread over other ones.

In effect, game master doesn’t really have to be a mind reader here. And game master doesn’t really have to generate everything; every person sitting at the table has a role of creating stuff.

## How did the story go, mechanically

So now, let’s look at the story from the very first paragraph and explain it in form of a dialogue which happened at a real role-playing session. More or less. I don’t remember all details.

(this time without _italics_)

* **GM**: “okay, Terry is endangered.”
* **Player**: “who was Terry?”
* **GM**: “one of the pilots in the base; you know, the one you managed to drink to sleep.”
* **Player**: “oh God… Sure, totally going to save him. He sucks anyway.”
* **GM**: “so there is a missile flying at him. And he won’t make it.”
* **Player**: “can I warn him?”
* **GM**: “very little time, you can, but it is a hard test - he would have to react in time”
* **Player**: “okay, can I shoot that missile with my flak cannon?”
* **GM**: “sure, but with flak… It’s close to him. He can get grazed.”
* **Player**: “But not killed. And I am a marksman. And I - seriously - I can target the flak - don't laugh - I kind of try to predict.”
* **GM**: “willing to risk being hit by something?”
* **Player**: “sure, lots of things tried to hit me already. It is saves Terry <rolls eyes>.”
* **GM**: “roll then”
* **Player**: “oh, failure”
* **GM**: “you saved Terry. But you got hit; your battle computer is severely damaged.”
* **Player**: “-2 to targeting and detection?”
* **GM**: “Yep”
* **Player**: “I just hope Terry knows who actually saved his life…”
* **GM**: “oh trust me, he does”
* **Player**: “okay, so I try to get fighter superiority over our opponents. Even with damaged fighter.”
* **GM**: “any specific reason?”
* **Player**: “I want the encounter to end; and I want our forces to win. So the secret weapon of the Dominion in this region, I want to see it.”
* **GM**: “fair enough, so you see here an enemy supply ship. It’s awkward; you don’t bring supply ships to capital ship fights.”
* **Player**: “indeed… Disengaging from my squadron, I want to scan it ”
* **GM**: “two enemy fighters approach you”
* **Player**: “taking them down. Ace pilot, marksman, high computers, high reaction.”
* **GM**: “okay, 40% test ”
* **Player**: “why 40?”
* **GM**: “damaged computers.”
* **Player**: “okay… And if I only try to confuse them to scan the supply ship, without destroying them?”
* **GM**: “90%”
* **Player**: “Suits me just fine. It’s not as if you count how many fighters I kill?”
* **GM**: “I don’t count how many they killed either.”
* **Player**: “Point. So, success!”
* **GM**: “this supply ship is actually interesting; it is extremely well endowed in an electronic warfare. Your scanners start picking up a very faint signature.”
* **Player**: “I position myself in such a way that the supply ship is between me and that signature. And I want to know what is it.”
* **GM**: “test the detection”
* **Player**: “failed”
* **GM**: “anything you want to pay to know?”
* **Player**: “I am safe from that, right?”
* **GM**: “yes, whatever is it can only fire at our fleet - you are in a safe spot”
* **Player**: “so I want it to appear”
* **GM**: “not conflicting that. The stealth destroyer materialized right in front of your eyes and fired the stream cannon at our fleet.”
* **Player**: “stealth destroyer? How? I should be able to detect it, I’m close enough.”
* **GM**: “supply ship with electronic warfare.”
* **Player**: “they combined stealth and jamming. Charming. What’s the damage? I kind of intercepted it, didn’t I ?”
* **GM**: “not conflicting. It tried to destroy the cruiser ‘Fury’. However, it was out of range. So the stream cannon was wasted; tragically, the beam vaporized * Terry’s Starfighter.”
* **Player**: “Can you kill someone else please? I mean, I saved him once already… And you gave him a name.”
* **GM**: “fair enough. So, this stream cannon heavily damaged Terry’s Starfighter, but didn’t kill it yet. And Terry is plummeting onto the gas giant. He needs help * fast.”
* **Player**: “okay, my eject pod is still working, right?”
* **GM**: “only the battle computer got damaged”
* **Player**: “going to ram into that stealth destroyer and eject.”
* **GM**: “why?”
* **Player**: “first of all, I’m going to be hero. Second, buying time to save Terry. Third, it totally sounds like fun - and stealth ships are very delicate and almost armor-less.”
* **GM**: “indeed, you do have a lot of chances. However, destroyer has many point defenses. It can rip you apart from the close range.”
* **Player**: “okay, so the stakes are higher than usual. What are my chances?”
* **GM**: “you are an ace pilot. You have a good ship. Destroyer is delicate and you attack from ambush. 60%.”
* **Player**: “if I fail, I am alive, Terry is alive and destroyer is destroyed.”
* **GM**: “okay, but your greatest rival will save you if you fail.”
* **Player**: “I don’t have one.”
* **GM**: “then we will generate one. And she will have an upper hand over you. Because she saved your life.”
* **Player**: “fine. Oh, scrap. Failure…”
* **GM**: “at least you get a cool rival - Alice”
* **Player**: “shoot me…”
* **GM**: “with pleasure.”

So what you can see here between all the banter is an actual part of a session. As you can see, important NPCs were generated on the fly (Terry, Alice). A player was able to actually act like a hero – her character, Eileen, could not have died. From the moment the player got interested in Terry – he also could not have died. The player was invested in that thread.

However, a lot of conflicts here were failed. And those failed conflicts have actually created the emergent story. For example, if Eileen has managed to ram into the destroyer she would have been hailed as a hero when she returned. As it is, Alice will be hailed as a hero – Alice saved both Eileen and Terry. This story is different.

Note how Terry became an important NPC even if earlier he was just “a guy she managed to drink to sleep”. After this battle a game master should consider introducing Terry more often. If Eileen takes interest in him and his storyline, Terry might become an important person. He might even get a surname. Or a back story. Or Eileen’s player will generate something about him.

What is important, this type of role-playing session is what the system you are reading about promotes. This is a shared story where the environment and surroundings are actually the context in which we can explore different people, their motivations and successes – or failures. If there was no starfighter battle, we would not generate Terry and his relations with Eileen. We would not know, that Eileen is a kind of a person to ram her starship into a stealth destroyer. We would not know, that Alice is a kind of person to take all the glory for herself.

In effect, those characters are created emergently. We see what they are doing and after they make their appearances we start fleshing those character sheets - their descriptions - with details. And that way we can consistently animate them.

Also, "not conflicting that". This is a key phrase. If noone minds something being implemented in fiction, this is in effect treated as "automatically won conflict". So "I want that to appear" - "not conflicting that" means, in effect, that this will happen and there is no purpose to roll the dice.

Admit it, how many times did you roll to see if your character... tripped? Never, right? You simply "didn't conflict that".

Or Superman, would he be hurt if shot by AK-47? "Not conflicting that", he won't get harmed.

This is why conflicts and their proper use are a core components to create a shared story. And this is why both successes and failures are extremely important.

## The case of the box (and what happened to the descriptions)

One could have thought, that the example with starfighters above was quite dry. There was some banter, true, but where was the immersion? Where were the amazing descriptions and beautiful vistas usually present in storytelling? Some people play because they want to be immersed in the world, to live inside. To see the beauty and the danger of their own.

For those people, storytelling is likely to remain the best possible choice. If you have to make a decision on a higher level of abstraction – “what do you want to achieve” and “why do you do this” instead of “what do you do” - then a part of immersion, the illusion your part of that world, is broken. And it is okay, as this system is not designed for the immersion. It optimizes shared story creation and balance of power between all parties. This is not a silver bullet system, something which will work for everyone under any circumstances.

However, this does not mean you can either use descriptions or play creating story. Those are not mutually exclusive. I have shown you a bare-bones example to prove that this mechanics actually creates that type of story. But if you want to, you can easily inject the descriptions and make the world seem more alive.

Let’s consider the case with the box, again. I’m going to use the descriptions combined with the intention system.

* _**GM**: “you can see the black box covered in plants on the edge of the abyss… It’s dark and cold, even if you are a person used to this type of conditions, you shiver.”_
* _**Player**: “I got it. Still, I swim up to it, pick it up with my gloves - Dragon skin gloves - and furiously swim upwards.”_
* _**GM**: “in the beginning you had a good start, however you can feel your lungs burning and the heavy box is slipping from your hands. You are not sure you are able to make it.”_
* _**Player**: “Conflict. I will make it. I have trained for this – my tribe has prepared me for the harsh situations and I used to hunt seals underwater.”_
* _**GM**: “use resilience and fortitude then.”_
* _**Player**: “I failed. But the box is more important than most other things for me. It’s my destiny.”_
* _**GM**: “on the last breath you have managed to see – in the corner of your eye – a rusty pipe structure. You can maneuver the box there and swim to get some air; however, catching the box means you will have a nasty cut from the rough edges.”_
* _**Player**: “I grin in pain, getting the box to those pipes. Then I get to the surface taking a desperate breath of air. I’m cold and tired, but I know I’m going to get down there to retrieve what is rightfully mine.”_

Okay, I have never said I am a good storyteller in terms of making descriptions. I’m a more of a story guy. But you can inject into this dialogue your own meaningful descriptions which do not sound somewhat unnatural. Note, that the use of the mechanics here is quite seamless; you don’t have to exactly break the immersion to be able to do it. This does push on the player more descriptive and creative role, but in storytelling the player is supposed to take those roles anyway.
