---
layout: mechanika
title: "Pytania Eksploracyjne"
---

# {{ page.title }}

## Problem

Mistrz Gry jest odpowiedzialny za stworzenie sesji, scen, NPCów, świata... ogólnie, za stworzenie całości dalekiego otoczenia Postaci Gracza. Jeżeli Mistrz Gry nie jest bardzo doświadczony w improwizowaniu, bardzo łatwo może zostać przeciążony i zwyczajnie nie będzie wiedzieć, co ma w danym momencie robić.

Jeżeli Mistrz Gry jest bardzo dobry w improwizowaniu, po około 100-200 sesjach z tymi samymi Graczami najpewniej zacznie wykorzystywać te same koncepty i klocki, które już się pojawiły wcześniej. Przez to stanie się przewidywalny.

Technika _Pytań Eksploracyjnych_ polega na tym, by oddać Graczom część tworzenia fabuły. Niech Mistrz Gry skupi się na budowaniu wysokopoziomowego szkieletu fabuły i frakcji - kto konfliktuje się z kim, jakie mamy strony, komu na czym zależy - a w tym samym czasie Gracze wypełnią ten szkielet faktyczną implementacją.

## Przykład

**Osoby uczestniczące**:

* **Ania** jest Graczem. Jej postać to Anette. Anette za dnia sprzedaje Inteligentne Maskotki a w nocy realizuje się jako wokalistka zespołu "Krwawe Jaszczury".
* **Basia** jest Mistrzem Gry (dalej: MG). 
* **Celina** jest Graczem. Jej postać to Charlotte. Charlotte za dnia programuje Inteligentne Maskotki a w nocy realizuje się jako gitarzystka "Krwawych Jaszczurów".

**Sytuacja**: 

Świat: rok 2020, w bliżej nie zidentyfikowanym kraju Europy Zachodniej.

Anette i Charlotte pracują w firmie ProBrainToys; firma ta zatrudnia 120 pracowników. Sesje RPG zawierające te dwie damy obracają się zazwyczaj dookoła prób wyciągnięcia zespołu "Krwawe Jaszczury" na listę przebojów zespołów bardzo niszowych - oraz dookoła prób uzyskania większych zasobów z dziennej pracy w korporacji.

**Stan początkowy**:

Basia wymyśliła sesję RPG, ale w sumie nie do końca ma ją opracowaną. Ma ogólną ideę składającą się z następujących konceptów:

* Fajnie by było, gdyby "Krwawe Jaszczury" mogły wystąpić (lub nie) w reklamie Inteligentnych Maskotek
* Fajnie by było, gdyby Inteligentne Maskotki mogły wzmocnić powodzenie "Krwawych Jaszczurów"
* Jeszcze fajniej by było, gdyby Anette i Charlotte musiały zmierzyć się z kolegami i koleżankami z firmy odnośnie swojego hobby

Przy takim postawieniu problemu, Basia ma - mniej więcej - następujące warianty zakończenia sesji RPG w głowie:

* "Krwawe Jaszczury" reklamują Inteligentne Maskotki i ta piosenka staje się hitem
    * Przez co postacie muszą zmierzyć się z opinią, że się sprzedały
* "Krwawe Jaszczury" reklamują Inteligentne Maskotki i ta piosenka jest katastrofą
    * Przez co postacie muszą zmierzyć się w pracy z opinią niekompetentnych i liczyć się z tarciami
* "Krwawe Jaszczury" nie reklamują Inteligentnych Maskotek
    * Przez co postacie będą pominięte przy awansach, zredukuje się im zasoby i wpływy...
* Anette i Charlotte muszą obronić możliwość gry w zespole przed działem prawnym
    * Przez co postacie mogą musieć zgodzić się na niekorzystne warunki, by móc dalej być "Jaszczurami".
    * Lub postacie muszą znaleźć sojuszników politycznych w firmie. Jakim kosztem?

**Pytania eksploracyjne:**

Czyli jest to potencjalnie materiał zapalny; nadaje się to na sesję. Ale jest to sam szkielet. Jest to za mało, by Gracze i MG mogli z tym pracować. W związku z tym na początku sesji Basia zaczęła zadawać _pytania eksploracyjne_ Ani i Celinie.

* **Basia**: Aniu, gdyby CO "Jaszczury" miały, lub gdyby CO się wydarzyło - to to mogłoby bardzo pomóc w ich karierze?
* **Ania**: Hmm... może, gdyby jakiś znany muzyk z nami zagrał raz? I może... oddał nam coś swojego?
* **Basia**: Świetnie. Celino, czemu ProBrainToys słucha się dziwnych pomysłów geniusza marketingu? I jak on się nazywa?
* **Celina**: Darren Werner. Jest on niby tylko dozorcą, ale jego rady nie wiedzieć czemu zawsze działają. Jego pomysły podłapuje szef Charlotte i bierze jako swoje.
* **Basia**: Aniu, opowiedz o wydarzeniu z przeszłości ProBrainToys, gdy pracownik robiąc rzeczy po godzinach poważnie zaszkodził firmie.
* **Ania**: To było trzy lata temu; gość pracował też jako konsultant dla konkurencji i przekazał im przypadkowo nową linię naszych Maskotek.
* **Basia**: Celino, opowiedz o gadżecie, który mógłby pasować do "Krwawych Jaszczurów" oraz Waszego segmentu słuchaczy. Aha, family-friendly.
* **Celina**: Taki... waran z komodo, który powtarza nihilistyczne, dołujące teksty.
* **Basia**: Oki, dajcie mi 10 minut i zaczynamy sesję.

**Interpretacja pytań eksploracyjnych przez MG**:

Basia mogła zadać więcej pytań, ale nie miała potrzeby. Miała już sytuację startową:

* Darren, geniusz marketingu, zaproponował wprowadzenie nowej Inteligentnej Maskotki - małego warana z nihilistycznymi tekstami. I dodanie jej jako członka jakiegoś niszowego zespołu muzycznego.
* Szef Charlotte, Erwin, od razu pomyślał o zespole, w którym gra Charlotte. Oczywiście, od razu jej zaproponował taką opcję.
* Dodatkowo Erwin stwierdził, że do reklamy (na żywo, ale i w telewizji) warto wykorzystać Feliksa - emerytowanego i poważanego w środowisku muzyka. Charlotte wie, że to może pomóc karierze "Krwawych Jaszczurów"
* Jest bardzo mało czasu na zrobienie tej kampanii reklamowej i załatwienie wszystkich tych rzeczy, więc... Charlotte i Anette będą musiały robić w pracy nadgodziny niezależnie od tego czy się zgodzą, czy nie.
* Jeśli "Jaszczury" się nie zgodzą na ten pomysł, Darren będzie osobiście rozczarowany a Erwin będzie wściekły na Charlotte. Jakoś trzeba to rozładować.
* Jeśli "Jaszczury" się zgodzą na ten pomysł, koledzy muzycy - zwłaszcza ci antykorporacyjni - będą rozczarowani postawą "Jaszczurów"
* Dział prawny niezależnie od okoliczności zainteresuje się "drugim życiem" Anette i Charlotte. Jeśli się zgodzą, mają jakieś wsparcie Erwina, ale jak nie... tak czy inaczej, jakoś trzeba to rozwiązać.
* Trzeba stoczyć walkę o "nihilistyczne, dołujące teksty". Anette vs Graham, który chce się wybić na pozytywnych tekstach motywujących opowiadanych przez warana ;-).

Innymi słowy, to powyżej jest już sytuacją konfliktową i niestabilną. Z tego powyżej da się wygenerować sporo problemów i zrobić bardzo ciekawą sesję...

## Wyjaśnienie pytań eksploracyjnych:

### Mechanicznie:

Ta technika polega na tym, że MG pyta osoby przy stole (na zmianę) odnośnie jakiegoś faktu i _odpowiedź tej osoby staje się rzeczywistością_.

Struktura pytań wygląda w taki sposób:

* Zadający_pytanie (Basia, MG) pyta Osobę_1 (Ania, Gracz)
* Osoba_1 odpowiada.
* Zadający_pytanie (Basia, MG) pyta Osobę_2 (Celina, Gracz)
* Osoba_2 odpowiada.

MG nie powinien pominąć nikogo przy zadawaniu pytań.

Oczywiście, MG nie musi zaakceptować odpowiedzi Gracza. Jak wszystko, to też jest negocjowane. MG może poprosić Gracza o inną odpowiedź, lub zaznaczyć, jaki fragment odpowiedzi Gracza psuje koncept pierwotny sesji. MG może też doprecyzować pytanie. Tak samo, Gracz może zawetować pytanie, jeśli niszczy to Graczowi koncepcję postaci. Przykład takiego pytania:

* **Basia**: Kiedy Charlotte przestała zdradzać swojego męża?
* **Celina**: Nie zgadzam się. Nie chcę, by Charlotte miała męża.

W tej sytuacji oczywiście MG i Gracz mogą negocjować, ale w sprawach dotyczących świata ostatnie słowo ma MG a w sprawach dotyczących postaci ostatnie słowo ma Gracz.

Pytania eksploracyjne nie muszą być zadawane tylko na początku sesji. Bardzo często można zadać je w połowie sesji; np. ja regularnie proszę moich graczy o nadawanie imion i nazwisk NPCom, lub proszę ich o dogenerowanie fragmentów rzeczywistości, w wypadku których dla mnie nie ma coś (jeszcze) znaczenia.

MG może też poprosić Graczy o zadanie sobie pytań eksploracyjnych, lub poprosić Graczy by zadali MG pytania eksploracyjne. W ten sposób MG może dowiedzieć się co w tej sesji szczególnie interesuje Graczy i odpowiednio poprowadzić opowieść.

Ważna rada - nie warto zadawać pytań, na które odpowiedź brzmi "tak" lub "nie", czyli tych zaczynających się na "Czy XXX?". Dużo lepsze pytania to "Jak XXX" lub "Dlaczego XXX" - te pytania wymagają odpowiedzi pełnym zdaniem i budują nowy fragment rzeczywistości.

Druga ważna rada - warto budować na odpowiedziach Graczy. Zadawać pytania pogłębiające. Znajdować nowe relacje między bytami stworzonymi przez Graczy. Wspólnie budować kontekst wspólnej rzeczywistości, by potem móc wspólnie wkroczyć do interesującej, niestabilnej sytuacji i rozwiązać ją wspólnie.

### Cele i oczekiwane wyniki:

Technika pytań eksploracyjnych ma następujące efekty:

* Gracz, nie MG, buduje fragment świata - to zwiększa nieprzewidywalność opowieści.
* Odpowiedź Gracza faktycznie jest często powiązana z tym, co Gracza interesuje. Rośnie motywacja Gracza.
* Przez to, że Gracze udzielają odpowiedzi, dochodzi do wymiany wiedzy między Graczem a MG. Obie strony wiedzą to samo.
* MG nie musi wszystkiego wymyślać; może skupić się na szkielecie fabuły i pozwolić Graczom budować elementy opowieści.

Cel pytań eksploracyjnych:

* Zbudowanie wspólnego kontekstu rzeczywistości między Graczami a MG
* Oddanie części kreacji Graczom, by MG nie musiał o wszystkim myśleć
* Zwiększenie nieoznaczoności rzeczywistości; jeszcze większa chaotyczność
* Zbudowanie zawiązek scen - czy to startowych, czy nawet dalszych podczas sesji. Sojuszników i przeciwników, warunków zwycięstwa...
* Dowiedzenie się, co chcą postacie Graczy od tej konkretnej sesji ;-)
* Określenie relacji między NPCami lub NPCami i postaciami.
* Określenie, czemu postaciom zależy na współpracowaniu ze sobą na tej sesji ;-)

### Przykładowe pytania zadawane przeze mnie

#### Współpraca postaci graczy:

Jeżeli nie wiem, czemu postacie mają współpracować na tej sesji, zadaję pytania typu:

1. Aniu, dlaczego Anette zależy na tym, by Charlotte osiągnęła sukces w tej sprawie?
1. Celino, jak pozytywne rozwiązanie tej sprawy wpłynie pozytywnie na coś, na czym Anette zależy?
1. Aniu, jeśli Wam się to uda, w jaki sposób wpłynie to korzystnie na Wasze działania w pracy?
1. Celino, jeśli Wam się to uda, w jaki sposób wpłynie to korzystnie na Wasze działania na scenie muzycznej?

No i macie. Nagle, niezależnie od odpowiedzi, Anette i Charlotte są w tym bagnie razem. Nie wiem, czym jest "pozytywne rozwiązanie", ale podejrzewam, że jeden wariant dotyczy "inni muzycy nas lubią" a drugi wariant dotyczy "w pracy nas szanują i doceniają". Nie przez przypadek NIE zadałem pytań o to, "czym pozytywne rozwiązanie" jest - pozwólmy sytuacji się samej rozwinąć.

#### Budowa otoczenia postaci graczy:

Jeżeli nie do końca wiem, w jakim otoczeniu postacie się poruszają, zadaję pytania tego typu:

1. Aniu, kto jest Waszym największym fanem? I w jaki sposób próbuje pomóc?
1. Celino, kto w firmie może Wam sprzyjać w rozwiązaniu tego problemu? Na czym tej osobie zależy?
1. Aniu, jakie problemy ma miejsce (klub, bar), w którym normalnie występują "Krwawe Jaszczury"?
1. Celino, jak Wasza firma mogłaby temu miejscu pomóc? I czemu jest to niezgodne z polityką firmy?
1. Aniu, kto jest szczególnie zazdrosny o Charlotte? I dlaczego?
1. Celino, kto w firmie - wybitnie nieudolnie - próbuje pomagać Anette właśnie z uwagi na "Jaszczury"? Czemu jest to kłopotliwe?

Te pytania zbudują mi podstawowy kontekst, w którym poruszają się postacie graczy. Ktoś chce postaciom pomóc. Ktoś przeszkodzić. Miejsca dla postaci istotne mają własne kłopoty i jakoś firma może pomóc - ale nie pomoże. Może postacie się tym zainteresują? Może otworzy się nowy wątek? Też, w pracy - ktoś nasze postacie lubi i próbuje pomóc w strasznie niekompetentny sposób, ktoś naszych postaci nie lubi i kopie pod nimi dołki...

To powoduje, że świat zaczyna żyć sam. Myślę postaciami - to ludzie wykonują działania i są centralnym punktem większości opowieści. Ale miejsca też są ważne, mają klimat i znaczenie. Co prowadzi do nowego potencjalnego pytania...

1. Aniu, dlaczego - jeśli Wasza miejscówka się zamknie - będzie to lokalną tragedią dla osób zainteresowanych alternatywną muzyką?
1. Basiu, co jest szczególnie cenne dla osób zainteresowanych 'światem alternatywnym' w tamtej miejscówce?

...i tak dalej, i tak dalej.

#### Budowa ukrytych działań różnych stron:

Zwłaszcza, gdy pracuję nad sesją detektywistyczną lub jakąś tajemnicą, warto doprecyzować motywację i okoliczności działania różnych stron. Zaznaczam, że rzeczy _w kursywie_ nigdy nie zostały zdefiniowane; Gracze nie wiedzą, o czym mówię.

1. Aniu, dlaczego _ktoś_ podłożył świnię szefowi Charlotte? Co ta osoba chciała przez to uzyskać?
1. Celino, czemu _ten ktoś_ uważał, że Charlotte przez to ucierpi?
1. Aniu, dlaczego dla rozładowania _ostatniego skandalu w firmie_ ta sprawa z Inteligentnymi Maskotkami i "Krwawymi Jaszczurami" jest tak ważna?
1. Celino, dlaczego _komuś innemu_ zależy na tym, by Wasza firma nie współpracowała z żadnym zespołem niszowym? Motywacja osobista.
1. Aniu, jakie wydarzenie łączy miejsce, gdzie normalnie występują "Jaszczury" oraz Waszą firmę?
1. Celino, dlaczego _komuś postawionemu wysoko_ wyjątkowo nie podoba się to, że Wasze postacie są WŁAŚNIE w "Krwawych Jaszczurach"?
1. Aniu, dlaczego _osoba, która Was dyskretnie wspiera w zarządzie_ jest tak dyskretna i nic z tym nie robi?
1. Celino, jak sukces tych maskotek pomogą _tej osobie, która Was wspiera_ wspierać Was dalej?

Nagle, jakkolwiek gracze nadal nie wiedzą, na czym polega sytuacja na sesji - nagle mają kontekst poprzednich wydarzeń (jak biurowe plotki). A Mistrz Gry widzi jakieś podstawowe motywacje i podstawowe siły działające wewnątrz firmy. To powoduje, że ma dużo więcej frakcji do animowania i świat nagle zaczyna żyć własnym życiem.

Mniej pracy dla MG -> większa przyjemność z prowadzenia. Trzeba tylko jednorazowo zapisać odpowiedzi, złożyć informacje o stronach aktywnych... i tyle.
