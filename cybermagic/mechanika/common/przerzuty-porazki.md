---
layout: mechanika
title: "Przerzuty, porażki"
---

# {{ page.title }}

## Ilustracja przykładu

![Anette uciekająca z płonącego budynku](Materials\1705_pics\anette_fire_run.JPG)

## Przykład

* Ania jest Graczem. Jej postacią jest Anette - taka 'Indiana Jones w spódnicy', zdobywca artefaktów...
* Basia jest Mistrzem Gry (dalej: MG). 

Sytuacja jest dość typowa. Anette jest uwięziona na drugim piętrze płonącego budynku. 

Ania deklaruje, że chce, by Anette wyskoczyła z budynku zanim butle z gazem wybuchną. Anette jest przyzwyczajona do takich akcji - jest to akcja, w której Anette ma poziom elitarny (5). Basia zauważa, że to jest drugie piętro, przeciwnicy Anette nadal obserwują budynek a butle zaraz wybuchną. Połączone stopnie trudności zostały ustawione na (6).

Ania rzuca kostką i wypada... 3, co przekłada się na -2. Oznacza to, że Anette się nie udało (5 - 6 - 2 jest mniej niż zero).

Co to znaczy?

## Konsekwencje porażki

Najbardziej oczywistym rozwiązaniem byłoby powiedzieć "Anette nie żyje, nie zdążyła wyskoczyć i zginęła w wybuchu". Jest to jednak najbardziej nieciekawe rozwiązanie, bo Ania nie ma już co robić podczas rozgrywki. Dodatkowo, wszystkie wątki fabularne powiązane z Anette właśnie się zakończyły.

Nikomu nie zależy na śmierci Anette - ani Ani, ani Basi. Zwłaszcza nie z powodu głupiego rzutu kostką.

W ten system wbudowany jest mechanizm przerzutów (rerolli), który ma sprawić, że Anette przeżyje nawet w wyniku złego rzutu kością. Ale za cenę _komplikacji fabularnych_.

## Przykład, ciąg dalszy

Ania rzuciła 3. Anette nie osiągnęła sukcesu.

* **Ania:** przerzucam.
* **Basia:** co proponujesz jako komplikację fabularną?
* **Ania:** eee... może... przeciwnicy zauważyli, że wyskoczyłam z budynku?
* **Basia:** pasuje. Rzuć jeszcze raz
* **Ania:** <rzuca> znowu 3...
* **Basia:** masz pomysł na kolejną komplikację?
* **Ania:** po prawdzie, nie.
* **Basia:** ten posążek jadeitowej małpki, który masz w plecaku? Nie wiesz, gdzie jest.
* **Ania:** nie zgadzam się. 
* **Basia:** zaproponuj mi więc coś innego.
* **Ania:** ech... dotknie mnie wybuch. Stracę broń. Nie dość, że wiedzą, że wyskoczyłam to JESZCZE jestem nieuzbrojona.
* **Basia:** zgadzam się, zwłaszcza, że oni mają psy. Rzucaj jeszcze raz.
* **Ania:** mają psy... ech... dobrze, podtrzymuję komplikację. <rzuca> 17! Sukces!
* **Basia:** Świetnie! Więc, jesteś na ulicy, ale oni widzą, że tam jesteś. Biegną za Tobą; jeden wyciąga pistolet. Będzie strzelał w biegu.
* **Ania:** Kluczę! Próbuję ich przegonić i nie zostać trafiona!
* **Basia:** Dobrze, sprawdzimy stopień trudności i rzucimy na to... psy pójdą potem, jako osobny konflikt.

## Wyjaśnienie przykładu, ciąg dalszy:

Za _pierwszym_ razem, gdy Ania przerzuciła, pojawiła się pierwsza _komplikacja fabularna_. Przeciwnicy zauważyli, że Anette nie znajduje się już w budynku. Inne przykładowe komplikacje fabularne, które mogły się pojawić w tym momencie:

* W budynku byli cywile. Owszem, Anette nic się nie stało, ale oni zginęli. (mało interesujące, bo nie generuje nic nowego)
* Lokalne siły policyjne będą uznawały ten wybuch za winę Anette i będą jej szukać (interesujące, bo pojawia się nowy wątek fabularny)
* Przeciwnicy Anette ściągnęli na ten teren helikopter patrolowy (interesujące, bo to jest wyzwanie do pokonania)
* Anette usłyszała płacz dziecka. Powinna uratować cywila (interesujące, bo to jest trudna komplikacja; tu MG powinien dać jej czas a nie deklarować wybuch.)
* Przeciwnicy mają psy gotowe do spuszczenia (interesujące, bo to jest wyzwanie. Acz może być zbyt trudne dla Anette; nie poszedłbym tą drogą)

Widzicie ogólną ideę? _Komplikacja fabularna_ jest czymś, co postacie graczy muszą przezwyciężyć prędzej czy później. Ewentualnie jest to kolejny wątek który gracze wezmą do rozwiązania... lub zignorują.

Gdy Ania rzuciła po raz _drugi_, rzuciła znowu '3'. To znaczy, że nie tylko zapłaciła cenę (przeciwnicy wiedzą, że Anette opuściła budynek), ale musi przerzucić jeszcze raz. Cena zostanie powiększona. Tym razem Anette traci broń w wybuchu. Szczęśliwie, kolejny przerzut już dał '17', czyli sukces. Teraz Anette - bez broni i na odkrytej pozycji - musi poradzić sobie z przeciwnikami, którzy chcą ją zlikwidować.

Zaczyna się nowy konflikt - "czy Anette da radę zgubić ludzi i nie zostać trafioną"...

## Zasady przerzutów

* Maksymalnie można zrobić do dwóch przerzutów pojedynczej akcji.
    * W wypadku trzech porażek (oryginał + 2 przerzuty), MG opowiada, co się stało. Może zmienić stawkę konfliktu na niższą (np. Anette może być ogłuszona i pojmana przez przeciwników).
    * W wypadku pierwszego przerzutu, postać gracza dostaje +1. W drugim przerzucie postać gracza dostaje +2. Chcemy, by postać gracza wygrywała.
* Z każdym przerzutem _komplikacje fabularne_ rosną.
* Gracz może NIE przerzucać, tylko zaakceptować konsekwencje porażki
* By zapoznać się z komplikacjami fabularnymi, patrz [odpowiedni rozdział - Komplikacje Fabularne](komplikacje-fabularne.html)

## Rola przerzutów

* Przerzuty promują to, by gracze mogli podejmować ryzykowne akcje bez ryzyka utraty najcenniejszych rzeczy na których im zależy.
* Przerzuty ratują postacie i te wątki fabularne, które dla graczy są szczególnie istotne przed 'kiepskimi rzutami'.
* Przerzuty powodują, że pojawiają się nowe wątki fabularne lub nowe komplikacje i wyzwania.
    * Przez to, że to GRACZ a nie MG proponuje komplikacje fabularne, historia jest budowana emergentnie.
* Przy odpowiedniej ilości konfliktów i przerzutów, nikt nie jest w stanie kontrolować historii, bo gracz nie może wszystkiego wygrać.
