---
layout: mechanika
title: "Ścieżki Fabularne"
---

# {{ page.title }}

## Zakres narzędzia

Ścieżki Fabularne są narzędziem wykorzystywanym przez Mistrza Gry (dalej: MG) do zarządzania postępami pojedynczego wątku fabularnego w zakresie pojedynczej rozgrywki.

Ścieżki wymagają przygotowania przed rozgrywką i są wykorzystywane podczas przeprowadzania rozgrywki. Czasami nowa Ścieżka pojawia się w trakcie rozgrywki.

## Problem


## Rozwiązanie



## Przykład



## Omówienie

### Koncepcyjnie:


### Mechanicznie:

