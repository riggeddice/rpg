---
layout: mechanika
title: "Mechanika Projektów, 2009"
---

# {{ page.title }}

## 1. Aktualność dokumentu

* Dokument jest aktualny wobec mechaniki 2009; działa z kartami postaci 2004, 2007
* Dokument jest wyprodukowany: 200910
* Ostatnia aktualizacja: 200910

## 2. Co to jest

Podczas sesji seria postaci próbuje osiągnąć wspólny cel. Przykładowo:

* Inżynierowie próbują zbudować pole siłowe ZANIM promieniowanie z wybuchającego statku zniszczy arkologię (wyścig)
* Grupa studentów próbuje zrobić skomplikowany Totem Szczura (wymagający badań i komponentów), by dostać zaliczenie z artefakcji (projekt)
* Potwór przebija się przez miasto. Gracze go flankują, zastawiają pułapki, uciekają z rzeczą do której potwór chce dojść (bossfight jednofazowy)
* Grupa bohaterów z mitów greckich poluje na Dzika Kaledońskiego na chwałę Artemidy (cała sesja: bossfight jednofazowy)

Mechanika projektów jest konfliktem dwufazowym.

* W fazie zero podaje się bazową pulę Krzyżyków
* W fazie pierwszej gracze wykonują serię konfliktów dodającą do puli Ptaszków, ewentualnie Krzyżyków
* W fazie drugiej ciągniemy ze zbudowanej puli po dwa żetony - do osiągnięcia 5 * V lub 5 * X.

## 3. Przykład użycia

Przykład z sesji "200826 - nienawiść do świń":

KONTEKST: 

* przez działania Klaudii (gracz) załoga Inferni (npce) się zbuntowała i pociągnęła za sobą wiele innych ludzi (npc)
* buntownikom grozi karcer, a może nawet egzekucja
* Arianna (gracz) próbuje to deeskalować, by zmniejszyć poziom wściekłości i konsekwencji (cel)

FAZA ZERO:

* Pula startowych krzyżyków: 10 X

FAZA JEDEN:

* Arianna jest świetna w przekonywaniu, przemowach itp. +3 V
* "Żeby być komodorem trzeba mieć KLASĘ" - Eustachy (gracz) zrobił żenujące show: +1 V, przekierowanie pierwszego X (część załogi do kolonii karnej)
* Plan - uruchamiamy Leonę (przerażająca, jak komisarz w W40k)
    * (Tr) przekonanie admirał Termię, by ta się zgodziła uwolnić lekarza z więzienia
    * (ExM) postawienie Leony: +3 V, przekierowanie drugiego X. ALE: (XX): Leona ma 2 tygodnie w plecy
* Arianna prosi o wsparcie arystokratę Aurum by wziął to na siebie; ona uratowała mu życie. +2V. ALE: (XXXX): arystokrata ma wpiernicz od swoich
* Przemowa Arianny (retoryka gracza): +1V

W sumie faza 1 wygenerowała: 10 V i 2 przekierowania X. Każdy żeton ma "swój kolor", pokazujący skąd pochodzi.

FAZA DWA:

* VV: Żeton arystokraty + show. Kupili kicz, arystokrata wziął na siebie "jestę rebeliantę". Czyli niby nie robią tego dla Arianny a dla niego. Są zmanipulowani.
* VV: Żetony Leony. Czyli jej okrutne, twarde podejście po Ariannie pokazało co zrobili i że Leona, którą poważają nimi gardzi. Plus, strzeliła do kogoś XD.
* (Xp): pierwszy przekierowany X -> do kolonii karnej
* V: SUKCES. Bunt pod kontrolą. Ale gracz chce iść dalej, pomóc swojemu przełożonemu.
* (Xp): drugi przekierowany X -> krzyżyk żenady
* X: trafiło jako dodatkowy, specjalny odcinek reality show
* X: odebranie Ariannie jej statku kosmicznego na miesiąc
* X: śmierć mało ważnego NPCa
* X: napięcia we frakcji...

(dalej nie ma eskalacji, bo poza przekierowanymi X by był piąty X i by było baaaaardzo źle)

WYNIK:

* Bunt opanowany
* Spore konsekwencje dla postaci i otoczenia

## 4. Kiedy tego użyć

Warto wykorzystać mechanizm Projektów wtedy, gdy mamy następujące cechy:

* Kilku graczy próbuje osiągnąć jeden cel
* Cel jest czymś większym
* Chcemy osiągnąć efekt "przeciągania liny": raz w tą, raz w tamtą
* Porażka jest możliwa i prawdopodobna; **to jedyny obszar, który NIE ma mechanizmów kompensujących i gwarantujących graczom sukces**

Czyli, przykładowo:

* Wyścig
  * Grupa inżynierów buduje pole siłowe ZANIM promieniowanie wszystkich zabije
  * Gracze uciekają z miasta ogarniętego zarazą ZANIM będzie lockdown
  * Gracze ścigają się na ścigaczach z NPCami w Tour de Barbaros
  * Struktura gry "Ducktales: The Quest for Gold": https://www.mobygames.com/game/disneys-duck-tales-the-quest-for-gold

* Budowanie projektu (punkt kulminacyjny)
  * Grupa inżynierów buduje pole siłowe. W wypadku porażki, teren stanie się niemożliwy do zamieszkania przez ludzi.
  * Arianna próbuje powstrzymać bunt (jak w przykładzie)
  * Gracze planują i przeprowadzają napad na bank
  * Grupa studentów Akademii Magicznej próbuje utrzymać przy życiu delikatną Rosa Divina Aerinus

* Złożony plan dynamiczny
  * Grupa inżynierów buduje pole siłowe. Pracując nad tym odkrywają coraz to nowe problemy i adaptują się do obserwacji.
  * Akcja ratunkowa w Skażonej magicznie ładowni - plan był dobry, ale się wszystko rozsypuje i trzeba pozmieniać kroki (sesja 200909).
  * Znalezienie kim jest Fantomas wymaga zostawienia przynęty i zaplanowania Wielkiej Pułapki- a jest ryzyko, że Fantomas weźmie przynętę i zostaniemy z niczym.

* Bossfight jednofazowy
  * Inżynierowie + pole siłowe: kierowanie Smoka przeciwko małemu obszarowi chronionemu przez ruchome pole siłowe, używanie pola do odbijania strzałów w Smoka, wzmocnienie reaktorów by pole objęło całą arkologię by Smok nie mógł atakować.
  * Grupa bohaterów z mitów greckich poluje na Dzika Kaledońskiego na chwałę Artemidy: od wytropienia, przez ujawnienie słabego punktu, przez odsłonięcie do zabicia.
  
Cechy powyższych konfliktów, jeszcze raz:

* Żadna z tych powyższych rzeczy nie ma sensu jako jeden test. To raczej bliżej "jednej sesji" niż "jednemu testowi".
* Porażka jest możliwa. Stawką jest FINALNY wynik.
* Podczas wykonywania mniejszych kroków Gracze mogą zbierać negatywne statusy i problemy.
* Raz coś dostaje jedna strona, raz druga. Nie jest tak, że "wszystko się udaje" lub "tylko jedna strona dominuje"

## 5. Mechanicznie - jak to złożyć i składniki

Zaczynamy od **standardowej puli Krzyżyków**:

* 10 * (X) : wariant standardowy
* 5-7 * (X) : wariant prostszy LUB dwie strony walczą

Teraz, **limit V**. Z doświadczenia, najlepiej, by maksymalna ilość (V) nie mogła przekroczyć 2*X. Czyli - dla sytuacji z 10X nie powinno być możliwości zdobycia więcej niż 20V.

**Jak budujemy pulę?** Przez składanie faktów i konfliktów:

* Jeśli np. któraś postać ma tam aktywną rolę, dodaj (V), aż do maksymalnie (VVV). Np. "Arianna przemawia i przekonuje buntowników by przestali"
* Jeśli gracze wykonają akcję wspomagającą (np. "dowiem się jakie słabości ma przeciwnik"), niech to jest konflikt Trudny (lub Ekstremalny) i daj za sukces: (VV).
    * Jeśli gracze zaeskalują do (VVV), niech mogą podnieść (VV) do (VVV).
    * Każdy (X) podczas konfliktu przekłada się na malus, problem lub coś niefortunnego / niepożądanego.
    * Gracze mogą w dowolnym momencie poddać konflikt. Wtedy konflikt doda (XX) do puli, wzmacniając pulę przeciwnika.
* Jeśli gracze mają fajny pomysł lub okoliczności, dodaj im (V).

Jako, że potem dociągamy "do pięciu", jest niemała szansa że nawet idealna dla graczy pula 20 (V) 10 (X) zakończy się porażką. Tu wchodzi dodatkowy mechanizm - **przekierowane krzyżyki**.

Jeśli gracze mogą sobie zapewnić jakiś wariant porażki który NIE jest powiązany z konfliktem głównym a jednocześnie stanowi dla nich poważniejszy problem to mamy do czynienia z przekierowaniem Krzyżyka. Przykładowo:

* "pierwszy X idzie na to, że komodor Arianna Verlen będzie miała opinię żenującego psa na szkło"

**Przekierowane krzyżyki mają ogromne zalety**:

* Po pierwsze, nie liczą się do puli porażki. Czyli wynik V(Xp)VXVXVX to nie jest [4(V), 4(X)] a [4(V), 3(X), 1(Xp)]. Można jeszcze raz "bezpiecznie" eskalować, nie będzie 5 Krzyżyków kończących naturalnie konflikt.
* Po drugie, zmuszają graczy do określenia co mogą poświęcić jeszcze przed konfliktem. Wymyślanie tych konsekwencji i budowanie konfliktów dążących do "ratowania się przed przegraną głównego konfliktu" to świetna okazja do tego by gracze się przygotowali na stratę czegoś.
* Po trzecie, więcej testów -> więcej porażek -> więcej okoliczności -> żywszy świat, więcej przeciągania liny. Bardziej filmowo niż "Hrunk poszedł, Hrunk wygrał".

**Jak więc buduję przekierowanie krzyżyków**?

* By gracze mogli pozyskać (Xp), muszą wymyślić i wykonać nowy konflikt. Ten konflikt ma następujące własności:
  * Wymaga (VVV) a nie (V) dla Trudnego, ew. (V) dla Ekstremalnego. Acz nie wahałbym się użyć (VVV) dla Ekstremalnego.
  * (Xp) może nie jest powiązane ściśle z kontekstem głównego Projektu, ale musi być bolesne dla postaci graczy lub dla celu graczy (np. w systemie szkolnym gdy robią projekt, (Xp) może być: "ukochana Ania została odepchnięta przez to, że Bartek skupiał się na projekcie").
  * Gracze proponują mi przekierowanie. Mogę im coś zaproponować jak to oczywiste, ale zwykle proponuję graczom wymyślanie ;-).

**Czyli mamy pulę**. Składa się z komponentów:

* (V), (X), (Xp)

Jak wygląda idealna pula?

* Dobrze, jeśli pula wygląda jakoś tak: 12 (V), 10 (X).
* Przy liczbach takich jak powyżej, warto, by gracze pozyskali 1-2 (Xp), czyli krzyżyki przekierowane.

## 5. Mechanicznie - który wariant

### 5.1. Projekt - "punkt kulminacyjny"

Ten wariant oznacza "punkt kulminacyjny sesji". 

Cała sesja buduje do tego jednego momentu. Plany graczy prowadzą do tego jednego momentu, wszystkie ruchy i działania. W tym momencie zwykle biorę następujące parametry:

* pula startowa: 10 *(X)
* sukces mniejszych konfliktów daje (V) lub (VV)
* nie daję bonusu za postać uczestniczącą, bo cały czas wszystkie uczestniczą

Co powoduje, że mamy co najmniej 5-6 konfliktów budujących pulę typu [12 (V), 10 (X)]. Gracze poczują się pewniej przy [15 (V), 10 (X)], ale najpewniej zabraknie im pomysłów i sytuacji co z tym zrobić. Do tego pojawiają się 1-2 dodatkowe, bardzo trudne konflikty budujące (Xr). A oczywiście po drodze każdy (X) w mniejszych konfliktach nadaje okoliczności i uciemiężenie postaci.

Potem ciągniemy co dwa żetony aż do osiągnięcia 5*(V) lub 5*(X), z narracją w punkcie kulminacyjnym oznaczającą "i dzieje się to".

Przykład ze wstępu: "Arianna opanowuje bunt" to było właśnie to, z tą różnicą, że wszystko było wycentrowane dookoła Arianny - więc dałem (VVV).

### 5.2. Złożony plan

Ten wariant nie jest punktem kulminacyjnym. To jest plan o znanej długości - gracze szkicują kroki do wykonania jako słowa, potem dopiero składamy z tego wynik. 

Przykładowo, na sesji 200909 mieliśmy tą strukturę:

WPIERW gracze naszkicowali jak uratować dziewczynę zamkniętą w skrajnie Skażonej ładowni w ostrzeliwanym statku kosmicznym w formie kroków do wykonania:

1. Arianna: modulacja i wzmocnienie pola siłowego statku by ekranować osoby wychodzące na zewnątrz statku podczas strzelaniny
2. Martyn: wyjście w kosmos i dopełznięcie do miejsca gdzie będziemy spuszczać Skażone powietrze
3. WCZEŚNIEJ: Eustachy: zmiana 'anomalicznej gąbki wchłaniającej magię' (dalej: gąbki) w 'pocisk/petardę' by odleciała od statku gdy się naje
4. Arianna: dehermetyzacja ładowni; niech anomaliczna energia ucieka z powietrzem
5. Klaudia: kontrola anomalicznej gąbki, niech się naje i umożliwi Martynowi wejście do ładowni
6. Martyn: wpada do ładowni, ratuje nieletnią, biegnie z nią do medbay i stabilizuje

Każdy z tych kroków może dać (VV): do 12*(VV). Z uwagi na to, że część kroków może fatalnie zawieść, dałem pulę startową 5*(X); powinienem dać 7*(X).

W wyniku działania samych konfliktów poszło tyci inaczej:

5. Przy pracy z gąbką okazało się, że to ORAZ poprzednie konflikty sprawiły, że trzeba dodać nowy krok - odwrócenie statku kosmicznego (manewr pilotażu).

Czyli pojawił się nowy krok, nie dający (V), ale wymuszony przez konsekwencje narracyjne subkonfliktów. W taki właśnie sposób tworzymy plany adaptujące się do sytuacji i budujemy większy konflikt "składający się" z mniejszych kroków innych postaci.

Znowu, jak to składam:

* pula startowa: 10 *(X)
* sukces mniejszych konfliktów daje (V) lub (VV)
* nie daję bonusu za postać uczestniczącą, bo cały czas wszystkie uczestniczą

### 5.4. Wyścig / Bossfight

Ta sytuacja jest troszkę inna. Tu mamy drugą stronę aktywną (dalej: Rywal). Albo to inne siły ścigające się z nami, albo boss (który też się z nami ściga).

TODO. 

HIPOTEZA: konflikty naprzemienne; poddanie "wrogich" konfliktów wzmacnia pulę wroga, wygranie "wrogich" konfliktów unikają problemów.

DO SPRAWDZENIA.

## 7. Jaki powinien być wynik przy stole

TODO - więcej próbek potrzebne.

## 8. Typowe błędy / wypaczenia

Za długo -> nudno
Za prosto -> nudno

TODO

### 9 Changelog

* 200910: init
