---
layout: mechanika
title: "Ustalenie poziomu konfliktu"
---

# {{ page.title }}

## Wszystkie tabelki pod ręką

### Poziom postaci w tym zadaniu:

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Inklinacja. | .POSTAĆ. |
|  [-1, 1]  |  [-1, 1] |  [0, 1]  |   [0, 1]  |   [-1, 1]    |  [-3, 5] |

### Strona przeciwna w tym zadaniu:

| .Podstawa.  | .Stopień trudności akcji. | .Wpływ. | .PRZECIWNIK.    |
| 2 LUB karta | karta LUB [-1, 5]         | [-2, 2] | realnie: [-3,7] |

### Otoczenie:

| .Sytuacja. | .Sprzęt. | .Skala.  | .Magia.  | .Surowiec. | .OTOCZENIE.     |
|  dev(0,3)  | dev(0,3) | dev(0,8) | dev(0,3) |     1      | realnie: [-6,7] |

### Przeprowadzenie samej akcji:

| .POSTAĆ. | .PRZECIWNIK. | .OTOCZENIE. | .RZUT.                 |  .WYNIK.        |
|  [-3, 5] |    [-3,7]    |    [-6,7]   | (1k20): [-3,3] W[-2,2] | S lub F W[-2,2] |

## Wykorzystanie

### Poziom postaci w tym zadaniu:

Przydzielenie punktów postaci przy zadaniu:

* Sumujemy punkty ze wszystkich kategorii poziomych.
* Jeżeli w zbiorze istnieje choć jeden element który pasuje, on wchodzi.
* Impuls może być _pozytywny_ lub _sprzeczny_. Lub może _nie być_ impulsu. Zarówno strategiczny jak i taktyczny.
* Specjalizacja i umiejętności mogą być _pozytywne_ lub ich _nie być_.
* Zawsze wchodzi jakaś inklinacja. Może być _pozytywna_, _negatywna_ lub _neutralna_.

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Inklinacja. | .POSTAĆ. |
|  [-1, 1]  |  [-1, 1] |  [0, 1]  |   [0, 1]  |   [-1, 1]    |  [-3, 5] |

* Jeśli jest choć jeden _sprzeczny_ impuls, nie możemy dostać "+1" w tej grupie impulsów
* Jeśli jest choć jeden _pozytywny_ impuls, nie może dostać "-1" w tej grupie impulsów
* Jeśli wszystkie impulsy nie pasują lub mamy zarówno _sprzeczny_ jak i _pozytywny_, dajemy 0.
* Dobieramy najbardziej pasującą inklinację do deklaracji akcji.

Uzyskana liczba się na "realny świat" przekształca w taki sposób:

| **Ilość powiązanych tagów**   | **Umiejętności postaci**       |
| [-3,-1]                       | antytalent                     |
| 0                             | brak umiejętności              |
| 1                             | amatorski                      |
| 2                             | zawodowy                       |
| 3                             | zaawansowany                   |
| 4                             | ekspert                        |
| 5                             | elita                          |
| 6                             | arcymistrz (skala województwa) |
| 7                             | arcymistrz (skala kraju)       |
| 8                             | arcymistrz (skala kontynentu)  |
| 9                             | arcymistrz (skala światowa)    |

### Strona przeciwna w tym zadaniu:

Przypomnijmy raz jeszcze tabelkę:

| .Podstawa.  | .Stopień trudności akcji. | .Wpływ. | .PRZECIWNIK.    |
| 2 LUB karta | karta LUB [-1, 5]         | [-2, 2] | realnie: [-3,7] |

#### Ustalenie stopnia trudności przeciwnika:

##### Jeśli mamy do czynienia z ogólnie rozumianą opozycją: 

Jako Podstawę ustawiamy '2', odpowiadający poziomowi zawodowemu.

| .Podstawa.  | .Stopień trudności akcji. | .Wpływ. | .PRZECIWNIK.    |
| 2           |           [-1, 5]         | [-2, 2] | realnie: [-3,7] |

Wtedy najważniejsze jest pytanie "jak często oczekujemy, że typowy zawodowiec sobie z tym poradzi w normalnych okolicznościach?"

| **Poziom**   | Łatwizna | Normalne | Trudne | Bardzo trudne | Będziemy zaskoczeni | Praktycznie niemożliwe | Zero szans |
| **Trudność** |   -1     |    0     |   1    |      2        |         3           |           4            |     5      |

##### Jeśli mamy do czynienia z przeciwnikiem  mającym kartę:

Łączymy (Podstawę z Trudnością) w jedno pole i podstawiamy w tamto miejsce wartość z karty. Tabelka wygląda wtedy tak:

| .Podstawa Przeciwnika. | .Wpływ. | .PRZECIWNIK.    |
|      karta             | [-2, 2] | realnie: [-3,7] |

Jeśli macie problemy z analizą stopnia trudności pod kątem zawodowca a np. oczekujecie, że to jest coś co ktoś w skali kraju sobie z tym poradzi, to możecie użyć tego wariantu: pod "karta" w Podstawie Oponenta podstawcie wartość z tabelki "realnego świata" (w tym wypadku "skali kraju" podstawiłoby się 7).

#### Ustalenie Wpływu:

Wpływ oznacza "jak wysoka jest intensywność akcji, którą próbuję wykonać".

* Wpływ _niewielki_ to np. popchnięcie kogoś. To wykonanie czynności, która nie wymaga dużej energii, lub którą druga strona i tak chciała zrobić. To zadanie lekkiej rany. Wywarcie takiego wpływu to -2 do poziomu przeciwnika.
* Wpływ _normalny_ to np. uderzenie kogoś. Większość normalnych działań. To typowe akcje. Rana. Wywarcie takiego wpływu to +0 do poziomu przeciwnika.
* Wpływ _duży_ to np. unieszkodliwienie kogoś. To wykonanie czynności wymagającej dużej ilości energii, lub coś, przed czym druga strona się bardzo broniła. Ciężka, uniemożliwiająca działanie rana. Wywarcie takiego wpływu to +2 do poziomu przeciwnika.

### Otoczenie akcji

Przypomnijmy tabelkę:

| .Sytuacja. | .Sprzęt. | .Skala.  | .Magia.  | .Surowiec. | .OTOCZENIE.     |
|  dev(0,3)  | dev(0,3) | dev(0,8) | dev(0,3) |     1      | realnie: [-6,7] |

Tu także mamy do czynienia z odchyleniami od poziomu standardowego, czyli: jeśli postać gracza ma przewagę, dostaje bonus. Jeśli strona przeciwnika ma przewagę, w tym miejscu pojawia się malus.

Rozpatrzmy poszczególne komponenty:

#### Modyfikatory sytuacyjne

Modyfikatory sytuacyjne oznaczają przewagi okolicznościowe. 

* Kto miał więcej czasu na wykonanie zadania?
* Kto mógł lepiej wykorzystać teren / rzeczy dostępne dla obu stron?
* Kto lepiej "wytaktycznił" sytuację, dopasowując się do słabych stron przeciwnika?

| **Sytuacja** | Podobna | Przewaga | Duża przewaga | Pełna dominacja |
| **Trudność** |    0    |    1     |     2         |     3           |
| **Częstość** |   40%   |   40%    |    18%        |     2%          |

* Ktoś mający przewagę zaskoczenia ma _przewagę_
* Hacker atakujący z zaskoczenia podsystem, którego nikt nie uważa za istotny ma _dużą przewagę_
* Wojownik na arenie walczący pod światło musi **oddać** przeciwnikowi _przewagę_.

#### Modyfikatory sprzętowe

Modyfikatory sprzętowe oznaczają przewagi ekwipunkowe.

* Kto miał większą wiedzę na tematy nad którymi pracowano?
* Kto miał sprzęt lepiej dostosowany do zadeklarowanej akcji?
* Kto był w stanie wykorzystać przedmioty lepiej?

| **Sytuacja** | Podobna | Przewaga | Duża przewaga | Pełna dominacja |
| **Trudność** |    0    |    1     |     2         |     3           |
| **Częstość** |   40%   |   40%    |    18%        |     2%          |

* Ktoś mający dobrej klasy sprzęt wspinaczkowy uciekający po górach przed kimś nie mającym nic ma _dużą przewagę_
* Dwie osoby ścigające się po górach z dobrej klasy sprzętem wspinaczkowym mają _podobny_ poziom

#### Modyfikatory skali:

* Kto miał większą ilość sojuszników?
* Kto miał 'bardziej wydajnych', większych sojuszników?
* Na jak duże otoczenie próbujemy wpływać jednocześnie (jak bardzo się rozpraszamy)?

Jeśli np. mamy żołnierza i mecha, mech ma większą skalę. Jeśli mamy grupę żołnierzy i mecha, znowu mamy podobną skalę.

| **Skala**    | Podobna | Przewaga | Grupa / Mech | Rój / Gigant | Niezliczone / Tytan |
| **Trudność** |    0    |    1     |   2          |  4           |     8               |
| **Częstość** |   65%   |   20%    |  10%         |  4%          |     1%              |

* Przewaga to np. po jednej stronie są 2 osoby, po drugiej są 3.
* Grupa to np. na 1 osobę przypadają dwie inne (lub: 5 osób przewagi). Odpowiednikiem tego jest człowiek vs spory mech.
* Rój to już zdecydowanie więcej istot. Np. rój szczurów. Odpowiednikiem jest człowiek vs gestalt / combiner.
* Niezliczone to już coś, czego realnie nie da się zniszczyć. Odpowiednikiem jest człowiek vs tytan.

#### Modyfikatory magiczne

Te modyfikatory oznaczają przewagi magiczne. 

* Zazwyczaj magia po prostu dodaje "+1" jeśli jest użyta podczas akcji; wszystko inne idzie w modyfikatory sprzętowe lub okolicznościowe.
* Czasem jednak magia działa inaczej - przewaga przez magię oferowana staje się tak kosmiczna, że to aż nieuczciwe. Wtedy używamy wyższych poziomów.

| **Sytuacja** | Podobna | Przewaga | Duża przewaga | Pełna dominacja |
| **Trudność** |    0    |    1     |     2         |     3           |
| **Częstość** |   40%   |   50%    |     9%        |     1%          |

* Jedna strona wspina się po górach ze sprzętem wspinaczkowym. Druga używa magii by się wznieść. _Zrównuje_ to ich przewagę sprzętową oraz daje _dużą przewagę magiczną_.
* Dwóch zapaśników się siłuje. Jeden wspomaga się magiczną inkantacją, drugi nie. To daje pierwszemu _przewagę_.

#### Modyfikatory wynikające z surowca

Jeżeli podczas testu użyty został surowiec, najpewniej wpłynął on na przewagi sprzętowe lub sytuacyjne. **Poza tym** użycie surowca dodaje zawsze +1 do testu.

### Przeprowadzenie samej akcji:

Po przejściu przez trzy powyższe tabelki, sumujemy ze sobą wynik wszystkich tych tabelek, wykonujemy rzut zgodnie z tabelką i na bazie tego otrzymujemy jakiś wynik.

| .POSTAĆ. | .PRZECIWNIK. | .OTOCZENIE. | .RZUT.                 |  .WYNIK.        |
|  [-3, 5] |    [-3,7]    |    [-6,7]   | (1k20): [-3,3] W[-1,1] | S lub F W[-1,1] |

Jeśli wynik nie pasuje stronie aktywnej (graczowi), ma prawo do przerzutu w zamian za koszt fabularny czy inny zaakceptowany.

O wpływie porozmawiamy w innym fragmencie podręcznika; tu zajmujemy się tylko poziomami trudności.

## Tablica zbiorcza:

![TODO](Materials/170511_cr_refined_board/cr_board_170511.png)

## Przykłady

### Reporterka uciekająca z kopalni

#### Sytuacja:

[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/maria-newa-1704.html) jest reporterką. Podczas jednej ze swoich eskapad przekonała jednego z emerytowanych górników, by ten poszedł z nią do nieczynnej już kopalni. Tam jednak jej przewodnik został dotknięty magią i, niestety, lekko mu odbiło. Maria próbowała przed nim uciec po drabinie (ewakuacja awaryjna z szybu). On próbował ją złapać...

#### Maria

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Inklinacja. | .Um. Mag. | .POSTAĆ. |
|    -1     |   -1     |   1      |     1     |     1        |     0     |    1     |

* Impulsy strategiczne jej nie wspierają. "Paladyn" nawet jej utrudnia, pozostałe nie pomagają. JEDEN negatywny i brak pozytywnych; Maria nie zostawia ludzi bez pomocy.
* Impulsy taktyczne to samo. "Odważna" i "Uparta" sprawiają, że raczej próbowałaby zostać a nie uciekać. To nie jest jej TYP akcji.
* Specjalizacja: działanie w trudnym terenie tu zdecydowanie wejdzie.
* Umiejętność: parkour tu wejdzie (umiejętności są szersze).
* Inklinacja: Tym razem 'Nimbleness': chodzi o prędkość a nie wytrzymałość.
* Umiejętności magiczne: Maria nie jest magiem i nie czaruje. Nie użyte.

Daje to Marii niezbyt elitarny poziom "amatorski". 1.

#### Przeciwnik

| .VERSUS. | .Diff. | .Wpływ. | .PRZECIWNIK.    |
|    2     |   0    |   0     |       2         |

* Poziom zawodowy to podstawa.
* Niby jest to starszy człowiek, ale wysportowany i wzmocniony manią napędzaną przez magię.
* Wpływ bez zmian: Maria chce mu zwiać; typowy poziom akcji w tej sytuacji.

Przeciwnik zatem ma standardowy poziom stopnia trudności przy takiej deklaracji akcji Marii.

#### Otoczenie:

| .Sytuacja. | .Sprzęt. | .Skala. | .Magia. | .Surowiec. | .OTOCZENIE. |
|   +1       |    +1    |   0     |    0    |     0      |    +2       |

* Maria ma przewagę sytuacyjną. Zorientowała się w zagrożeniu i zaczęła uciekać mając przewagę czasową.
* Maria ma też przewagę sprzętu. A dokładniej, jej eks-przewodnik biegnie z dużym i niewygodnym kamieniem. Maria nie ma obciążenia.
* Skala bez zmian: Ani jeśli chodzi o wielkość ani jeśli chodzi o ilość uczestników.
* Nie ma przewagi magicznej po żadnej ze stron; mania wywołana magią wpłynęła na pierwotny stopień przeciwnika.
* Maria nie wykorzystuje żadnego surowca.

Finalnie, Maria ma całkiem miłą przewagę otoczenia: +2.

#### Przeprowadzenie samej akcji:

Maria ma poziom amatorski (1), jej przeciwnik zawodowy (2). Maria ma bonus z otoczenia (+2). Gracz Marii rzucił 10 na 1k20, co przekłada się na -1 do wartości testu:

| .POSTAĆ. | .PRZECIWNIK. | .OTOCZENIE. | .RZUT.        |  .WYNIK. |
|    1     |        2     |     +2      | (10): -1 W[0] | R  W0    |

Poziom Marii (1) - Poziom Przeciwnika (2) + Otoczenie (2) + Rzut (-1) = 0. To oznacza remis. Gracz Marii ma następujące możliwości:

* Sukces, ALE z mniejszym Wpływem (czyli np. nie uciekła, ale zwiększyła swoją przewagę, co da jej bonus do następnego testu)
* Przerzut, kosztem fabularnym (czyli np. uciekła, ale musiała zostawić sprzęt / dowody)

I w obu wypadkach sytuacja nie jest zamknięta...
