---
layout: mechanika
title: "Budowanie Profilu Postaci"
---

# {{ page.title }}

## 0. Aktualność dokumentu

* Dokument jest aktualny wobec mechaniki 1801
* Dokument jest wyprodukowany: 180107
* Ostatnia aktualizacja: 180107

## 1. Omówienie elementów Profilu

### 1.1. Ogólnie

Profil postaci składa się z pięciu kategorii.

* Trzy oznaczają "wnętrze" postaci (Motywacja, Umiejętności, Silne i Słabe Strony).
* Jedna oznacza rzeczy "zewnętrzne" do postaci do których postać ma dostęp (Otoczenie).
* Jedna oznacza magię, jeśli mamy do czynienia z postacią maga (Magia).

Wszystkie kategorie mają taką samą moc matematyczną (patrz: rozwiązywanie konfliktów) i synergizują pionowo (jednocześnie możesz raz użyć każdej kategorii osobno).

Każda kategoria składa się z Obszarów. Każdy Obszar podzielony jest na Aspekty.

* Obszar oznacza coś szerokiego, czym postać może się definiować. Na przykład: zawód (Umiejętność: "Terminus zwalczający potwory", "Listonosz wiejski").
* Obszar podzielony jest na Aspekty; zwykle około pięciu. ("Listonosz wiejski": "znajomość okolicy", "wyciąganie plotek"...)
* Rolą Obszaru jest zbudować ogólne zrozumienie kim jest postać.
* Rolą Aspektu jest rozróżnić postacie mające podobne Obszary. Dwaj Terminusi będący Listonoszami niekoniecznie są do siebie podobni.

Dla dokładnego wyjaśnienia przyczyn, polecam sekcję "Czemu dzielimy postać na Obszary i Aspekty".

### 1.2. Motywacja
#### 1.2.1. Co oznacza ta sekcja

Co steruje postacią? Co sprawia, że postać chce coś osiągnąć? Czemu postać maga - istoty, która nic "nie musi" - w ogóle wychodzi z łóżka rano? Co postać uważa za ważne dla siebie? Na jaką formę niesprawiedliwości się nie godzi? Kiedy postać stwierdzi "wygrałam - mogę wreszcie odpocząć".

Innymi słowy, Motywacja to zbiór motywatorów i demotywatorów, dzięki którym zarówno Gracz ma możliwość dążyć do jakiejś przyszłości świata, w którym gracie jak i Mistrz Gry może tworzyć alternatywną przyszłość, która ową postać wciągnie. Ewentualnie - wyzwania i opozycję.

#### 1.2.2. Jak złożyć z niej postać?

* Motywacja jest o tyle odmienna od pozostałych kategorii, że posiada pięć usztywnionych Obszarów: Ból, Marzenia, Filozofia, Kultura i Metody. 
* Gdy składasz postać, wybierz trzy, cztery lub pięć z tych Obszarów. Np. możesz mieć postać mającą Ból, Marzenia, Filozofię czy Marzenia, Kulturę i Metody. Ważne, by mieć 3,4 lub 5.
* Nazwij Obszar. Np. "Ból: Samotny i zapomniany".

#### 1.2.3. Ból

"Ból" oznacza to wszystko, czego postać się boi, co postać denerwuje, co postać uważa za swoją porażkę. Ból to to, co postać próbuje swoimi działaniami _uśmierzyć_.

Z perspektywy [propozycji wartości](http://www.expressiveproductdesign.com/value-proposition-canvas/) Ból oznacza "wszystko, co boli naszą postać". To, na co się nie zgadza. To, co chce zniszczyć. To, co chce usunąć. To, co ją boli i drażni.

Przykład:

* Daniel Akwitański: "BÓL: Znienawidzony, biedny i zapomniany"
* Kajetan Weiner: "BÓL: Waśnie, brutalność i złe prawa - a on może tylko patrzeć"

#### 1.2.4. Marzenia

"Marzenia" oznacza to wszystko, do czego postać dąży. Odwrotność Bólu, Marzenia oznaczają co chce uzyskać, co chce zobaczyć i osiągnąć.

Przykład:

* Kaja Maślaczek: "MRZ: Być badaczem, nie szpiegiem"
* Ewa Zajcew: "MRZ: Odnaleźć Jakuba Urbanka"

#### 1.2.5. Filozofia

"Filozofia" to ogólny sposób myślenia postaci. Jej poziom wartości. To, jak patrzy na rzeczywistość i to, jak ją filtruje pod swoim kątem.

Przykład:

* Kaja Maślaczek: "FIL: Lepsza iluzja niż rzeczywistość"
* Robert Sądeczny: "FIL: Opiekuńcza Tyrania"

#### 1.2.6. Metody

"Metody" to ogólnie rozumiane działania postaci. Jak się zachowuje, jakimi metodami osiąga Marzenie, niweluje Ból czy spełnia Filozofię.

Przykład:

* Dracena Diakon: "MET: Artystyczna Spustoszona Wiła"
* Hektor Reszniaczek: "MET: Niezależna, uczciwa praca"
* Infernia Diakon: "MET: Wandal, wamp i agresor"

#### 1.2.6. Kultura

"Kultura" oznacza jak otoczenie ukształtowało postać. Co sobie ceni, jak patrzy na świat - przez osoby będące jej bliskie i przez kulturę, z którą się utożsamia.

Przykład:

* Melodia Diakon: "KLT: Efektowna, zaskakująca współpracowniczka"
* Jakub Dobrocień: "KLT: Współpraca, uczciwość i oszczędność"

### 1.3. Umiejętności
#### 1.3.1. Co oznacza ta sekcja

Zbiór rzeczy, które postać potrafi zrobić _szczególnie_ dobrze do poziomu, w którym inni rozpoznają TĄ POSTAĆ jako specjalistkę od tego czegoś. Nie trzeba wpisywać tu np. "Kierowca ciężarówki", jeśli nasza postać potrafi prowadzić ciężarówkę - trzeba jednak to wpisać, jeśli nasza postać ma być bardzo dobrym kierowcą, do poziomu zawodowego / eksperckiego.

Jeśli postać ma tu coś wpisane, to znaczy, że jest w stanie pełnić tą rolę na poziomie zawodowym.

Przykład Umiejętności:

* Sukkub KADEMu
* Agentka wywiadu
* Dyskretny sabotażysta

#### 1.3.2. Jak złożyć z niej postać?

* Każda postać ma docelowo 3 Umiejętności. 
* Proponuję wymyśleć sobie 3 Umiejętności i podzielić je na aspekty.
* Niektóre postacie mają 4 Umiejętności, nieliczne 5. Rekomenduję 3.

### 1.4. Silne i Słabe Strony
#### 1.4.1. Co oznacza ta sekcja

Postacie mają rzeczy, które są dla nich unikalne; coś, co wyróżnia postacie od losowych nienazwanych NPCów. Zarówno na plus jak i na minus.

Przykład:

* Andżelika Leszczyńska: Nieposkromiona Ciekawość
    * Opis Siły: Andżelika jest mistrzynią trivii i dziwnych faktów. Wie mnóstwo niesamowitych rzeczy.
    * Opis Słabości: Andżelika przez swoją ciekawość ma tendencje do wpakowywania się we wszystkie możliwe problemy. Jest też lękliwa, więc wpakowuje INNYCH w tarapaty.
* Ewa Zajcew: Ognisty Koszmar KADEMu.
    * Opis Siły: podnosi Skalę Ewy podczas walki magicznej. Ewa jest jak skrzydło terminusów.
    * Opis Siły: podnosi Skalę Ewy podczas używania magii ognia. Też: jest niewrażliwa na ogień.
    * Opis Słabości: Ewa jest ogólnie nielubiana i się jej większość boi. Jest jak demon ognia, nie czarodziejka.
    * Opis Słabości: Ewa jest szczególnie wrażliwa na zimno i lód. Woda także zadaje jej obrażenia.

#### 1.4.2. Jak złożyć z niej postać?

* Każda postać posiada jeden Obszar Silnych i Słabych Stron.
    * Postać może mieć do 5 aspektów Silnych Stron
    * Postać musi mieć co najmniej 1 aspekt Słabych Stron
* Proponuję wymyśleć jeden Obszar i rozpatrzyć:
    * jakie aspekty pozytywne zapewnia? Co jest szczególną siłą postaci?
    * jakie aspekty negatywne są konsekwencją tego Obszaru?
* Nieliczne postacie mają więcej niż jeden Obszar. Każdy Obszar zapewnia własne silne ORAZ słabe strony.

### 1.5. Otoczenie
#### 1.5.1. Co oznacza ta sekcja

Postać nie działa w izolacji. Ma znajomości, przyjaciół, narzędzia których używa i które ją charakteryzują. Zaznaczam, że nie zapisujemy tu WSZYSTKIEGO. Nie musimy pisać "samochód" by postać miała dostęp do samochodu - musimy wpisać "samochód", jeśli jest to pojazd który wyróżnia tą postać od innych postaci.

Dzielimy Otoczenie na "Zna" i "Ma". Jak o postaci mówią? Jaki jest jej krąg znajomych których może wykorzystać? Kto jest jej winny przysługę? Jakie narzędzia ma do dyspozycji? Sprzęt?

Postacie takie jak np. Batman są w większości definiowane przez swoje Otoczenie.

Przykład Otoczenia:

* Zna:
    * Znana w śląskiej Srebrnej Świecy jako mediator i badacz
    * Powiązana ze środowiskiem Cybergoth; działa jako DJ
    * Sąsiedzkie kółko samopomocy w Półdarze
* Ma:
    * Dostęp do Wektora Sigma i Hal Symulacji
    * Adaptowalny pancerz magitechowy
    * Kolekcję trucizn i narkotyków

#### 1.5.2. Jak złożyć z niej postać?

* Wybierz 3-5 rzeczy jakie pasują do tego, kogo postać "Zna". Ta kategoria oznacza jej reputację, znajomości, kontakty...
* Wybierz 3-5 rzeczy jakie postać "Ma". Ta kategoria oznacza rzeczy i osoby, które są do jej absolutnej dyspozycji.
* Zwykle postać ma 3 rzeczy w kategorii "Zna" i 3 w kategorii "Ma", acz 5 w każdej jest całkowicie akceptowalne.
* Niech te rzeczy nie będą całkowicie generyczne ("samochód", "domek z garażem"). Niech są potencjalnym generatorem aspektów i kolorytu postaci.

### 1.6. Magia
#### 1.6.1. Co oznacza ta sekcja

Zbiór mocy magicznych i magii pod kontrolą postaci maga. Postacie ludzkie nie posiadają magii dynamicznej. Większość viciniusów też nie posiada magii dynamicznej.

#### 1.6.2. Jak złożyć z niej postać?

* Wybierz 3 szkoły z listy istniejących i nadaj im aspekty.
* Zdarzają się postacie mające 2 szkoły lub 4 szkoły, ale to nie jest typowa sytuacja.

#### 1.6.3. Szkoły magiczne

Odsyłam do dokumentu "Szkoły magiczne".

## 2. Minimalna Postać potrzebna do gry

### 2.1. Jak złożyć minimalną postać?

Z uwagi na Stożek Nieoznaczoności (patrz "Filozofia stojąca za tworzeniem Profilu" w tym dokumencie), postać będzie zmieniała się bardzo często. Podczas budowy pierwszej Opowieści, postać będzie zmieniała się i przekształa się praktycznie cały czas. Nowe aspekty będą się pojawiać, stare będą zanikać. Gracz będzie szukał swojej "idealnej" postaci.

To sprawia, że minimalny Profil by móc rozpocząć grę musi zawierać tylko:

* jedną Motywację z trzema aspektami
* jedną Umiejętność z dwoma aspektami
* jedno Otoczenie z trzema aspektami
* jeśli jest magiem, jedną Szkołę z dwoma aspektami
* Dwu- lub trzyzdaniowy opis tego kim ta postać jest, czemu Gracz chce ją mieć podczas Opowieści... jakiś koncept.

Z powyższego najpewniej około 33% przetrwa przez następnych 5 Opowieści. 

* Graczu, po prostu weź coś co wydaje Ci się interesujące i zmieniaj w czasie rzeczywistym podczas budowania Opowieści ;-).
* Mistrzu Gry, pozwól Graczowi przekształcać postać, by Gracz znalazł swój Voice&Tone i idealny koncept.

### 2.2. Jak zachować spójność świata?

#### 2.2.1. Zasada filmu

Kojarzycie dowolny film lub serial? Czasami okazuje się, że znany nam bohater w pewnym momencie zrobił coś niespodziewanego i pojawia się efekt "o, nie wiedziałem, że on to potrafi". Jednak najczęściej poczucie spójności świata jest zachowane - to, że _widz_ nie wie, że postać coś potrafi nie oznacza, że z perspektywy świata gry ta postać tego nie potrafiła od samego początku.

Przykład filmowy:

[Youtube: Princess Bride, scena z "I am not left-handed"](https://www.youtube.com/watch?v=rUczpTPATyU)

To sprawia, że jeśli postać dostanie nowy aspekt, czy nową Umiejętność w czasie rozgrywki - świat nadal jest spójny. Po prostu postać wcześniej nie pokazała, że coś potrafi.

#### 2.2.2. Mechaniczna ewolucja postaci

Jednym słowem: Progresja.

Zamiast punktów doświadczenia znanych z innych systemów, w tym systemie zarówno Gracz jak i MG otrzymują punkty Wpływu po skończeniu Opowieści. Wydają te punkty na zmianę rzeczywistości. To może być zarówno przesuwanie NPCów jak i dodanie nowych rzeczy do postaci.

Przykładowo, mieliśmy postać magazyniera na portalisku. W ramach wydawania Wpływu Gracz stwierdził, że chciałby zmienić funkcję postaci na detektywa. Więc po skończeniu Opowieści lokalny mafiozo dał owemu magazynierowi małą agencję detektywistyczną, co dopisaliśmy do Progresji postaci a Gracz sam dopisał Postaci nową Umiejętność.

Z perspektywy świata gry, ów magazynier być może kiedyś był detektywem, ale nie spotkaliśmy go w tej roli wcześniej.

## 3. Jak zbudować Postać?

### 3.1. Co to za sekcja

Ta sekcja służy to ułatwienia Graczowi stworzenia Profilu. Przedstawia grupę przydatnych strategii i pytań diagnostycznych, dzięki którym da się złożyć całkiem grywalną postać.

By móc określić kiedy przestać budować postać, spójrz na sekcję tego dokumentu "Minimalna Postać potrzebna do gry".

### 3.2. Wariant klasyczny "Z opisu w Profil"

Najczęściej spotykany, acz najbardziej czasochłonny wariant. Gracz tworzy opis postaci mający około 4-5 linijek tekstu. Następnie z tego tekstu próbujemy wyekstraktować poszczególne Obszary czy Aspekty i je odpowiednio przyporządkować. Czyli: wpierw przychodzi opis, potem z opisu wypełnia się to, co się da. Technika klasyczna.

### 3.3. Wariant "Chcę grać detektywem"
#### 3.3.1. Co to za wariant

* Wymagania: Gracz posiada mikrokernel idei - jakiś zawód, czy jakąś ogólną koncepcję postaci.
* Działanie: Ta technika pozwala nam wyjść od tego zawodu (tej idei) i stopniowo wypełniać koncepcję.

#### 3.3.2. Pytania generacyjne

Graczu, odpowiedz na poniższe pytania. Niech skrót "TP" oznacza "Twoja Postać".

1. Jakie jest największe dokonanie TP (uznane przez kogoś innego)? Czemu TP była w stanie to zrobić?
2. Z czego w ramach tego największego dokonania TP jest szczególnie dumna? Dlaczego właśnie z tego?
3. W ramach tego dokonania komu i jak TP pomogła najmocniej? Dlaczego nadal ten ktoś utrzymuje kontakt?
4. Z czego TP jest najlepiej znana wśród swoich znajomych? Z czego słynie? Co robi inaczej?
5. Pod jakim kątem TP wybiera zlecenia / pracę? Jakiej nigdy by nie przyjęła? Dlaczego?
6. Jakiego typu umiejętności TP wykorzystuje podczas wykonywania zadań - coś, czego nie robi nikt inny? Czemu to działa?
7. Co przez rywali i zleceniodawców jest uważane za "nieuczciwą" przewagę TP? Czemu to jej pomaga?
8. Kiedy TP powie "O nie, tak się nie stanie". Dlaczego? Jak spróbuje temu zaradzić?
9. Gdzie TP znajduje zlecenia gdzie jej rywale nie szukają? Jaki jest jej unikalny kanał? Czemu tam / jak się poznali?
10. Z kim przestaje TP? Jak się zachowuje w swoim otoczeniu? Jak by działania TP opisali jej towarzysze?
11. W jaki sposób otoczenie TP wsparło jej unikalne umiejętności? Jak wpłynęło na jej zachowanie?
12. W jakich okolicznościach TP jest NAJLEPSZĄ osobą do rozwiązania problemu? Jaki to problem? Czemu TP jest wtedy najlepsza?
13. O czym TP marzy? Czego się boi/co ją boli? Co robi, by jej marzenia się spełniły?
14. Co jest kłopotliwym wydarzeniem z przeszłości TP? Co próbowała zrobić i jej nie wyszło? Czemu? Jak na to zareagowała?
15. Co TP robi w czasie wolnym? Co lubi robić? Jak odpoczywa?

#### 3.3.3. Jak te pytania wykorzystać

1. Główna akcja, z jakiej postać jest dumna najpewniej doprecyzowuje jedną Umiejętność.
2. "Szczególnie dumna" implikuje albo jakiś dominujący Aspekt Umiejętności, albo dotyka Motywacji.
3. Osoba, grupa lub frakcja której pomogła postać powinna trafić do Otoczenia. "Czemu trzyma kontakt" mówi też coś o postaci lub tym elemencie Otoczenia. Aspekt?
4. To dodefiniuje nam albo Otoczenie (znajomi), albo nową Umiejętność, albo nową Motywację albo jakiś aspekt do powyższych.
5. Zdecydowanie Motywacja - jak pojawi się "pieniądze" to już wiemy, że ma Motywację pod tym kątem.
6. Próba zbudowania nowej Umiejętności lub przynajmniej jakichś aspektów. Próba znalezienia jakichś synergii między aspektami / Umiejętnościami.
7. Próba dotknięcia Silnych i Słabych Stron postaci; zahaczenie albo o Aspekty albo o dodatkowe synergie, których jeszcze nie widziemy.
8. Próba zbudowania BÓLU. Połączenia synergicznego Motywacji i Umiejętności. Może Otoczenia.
9. Dodefiniowanie Otoczenia przez znajomości i kontakty.
10. Próba znalezienia METOD (Motywacja) oraz podniesienia Otoczenia o jeszcze jeden krok.
11. Próba zbudowania Umiejętności przez Otoczenie. Może dotknie to też Motywacji?
12. Znowu dotknięcie Silnych/Słabych Stron oraz dotknięcie synergii między Umiejętnościami i Motywacją.
13. Silna próba wywołania MARZEŃ oraz BÓLU (Motywacje) oraz jakiejś Umiejętności / Otoczenia. 
14. Próba zobaczenia kontekstu porażki postaci celem wygenerowania Słabej Strony. Może uda się dotknąć też METOD?
15. Łączymy Motywację z Umiejętnością.

## 4. Filozofia stojąca za budową Profilu

### 4.1. Parametry jakościowe Profilu

1. Nowy Gracz musi być w stanie zacząć grę w ciągu 15 minut od rozpoczęcia budowy Profilu
2. Każdy Gracz/ MG musi być w stanie objąć i prowadzić każdą postać - postać musi promować prawidłowe nią granie
3. Postać, niezależnie od tego kto nią gra, powinna zachowywać się spójnie w świecie gry
4. Mechanicznie, postać powinna być szybka do używania (~30 sekund) i powinna być czytelna (nie-RPGowiec potrafi przewidzieć jej zachowanie)
5. Dwie postacie 'detektyw' czy 'kucharz' powinny się znacząco różnić od siebie podczas tworzenia Opowieści. Postać winna być charakterystyczna.
6. Profil postaci musi być łatwy w zarządzaniu, zwłaszcza podczas tworzenia Opowieści achronologicznie.

### 4.2. Stożek Nieoznaczoności

Najmniej o postaci wiemy na etapie tworzenia. Dopiero uczestniczenie w Opowieściach będą naszą postać kalibrować i przekształcać. Coś takiego nazywa się [Stożkiem Nieoznaczoności](https://en.wikipedia.org/wiki/Cone_of_Uncertainty) i wygląda mniej więcej tak:

![RPGowy Stożek Nieoznaczoności](Materials/180103_profile_build/rpg_cone_of_uncertainty.png)

To znaczy, że najbardziej postać będzie mieć modyfikowaną kartę gdy jest "młoda". Im jest starsza, im więcej przeszła, tym rzadziej się będzie zmieniała (wtedy zmiany będą wynikać z ewolucji postaci - czyli z Progresji).

### 4.3. Zasada zmniejszających się korzyści

Zasada zmniejszających się korzyści - czyli implementacja Zasady Pareto na budowanie Profili postaci - mówi mniej więcej "80% korzyści z budowania Profilu pochodzi z 20% czasu na to poświęconego". Jako, że zgodnie ze Stożkiem Nieoznaczoności postać _i tak_ będzie się drastycznie zmieniać podczas tworzenia Opowieści to i tak silnie skupiamy się na Profilach postaci minimalnych potrzebnych do rozpoczęcia gry.

Marnowaniem czasu byłoby tworzenie postaci przez półtorej godziny tylko po to, by zrobić przy jej użyciu jedną Opowieść.

Dlatego lepiej jest doprowadzić wszystkie obszary postaci (Motywacja, Umiejętności, Silne i Słabe Strony, Otoczenie...) do 1-2 sekcji posiadającej 2-3 aspekty każda w ciągu 15-20 minut niż skupiać się na zbudowaniu postaci perfekcyjnej i w 100% skończonej... która zmieni się drastycznie po pierwszej Opowieści.

### 4.4. Znaczenie Otoczenia

Zgodnie z [analizą SWOT](https://en.wikipedia.org/wiki/SWOT_analysis) podczas pracy nad organizacją / bytem trzeba zwrócić uwagę nie tylko na sam byt (tu: postać) a też na otoczenie postaci. Innymi słowy, duża część sił i słabości postaci może znajdować się nie w samej postaci a w jej otoczeniu.

Batman czy IronMan są właśnie definiowani w większości przez swoje Otoczenie (ma sprzęt i bogactwo). Nawet Superman nie miałby informacji (leadów) o problemach gdyby nie Otoczenie jego alter-ego, Clarka Kenta, który pracuje jako reporter.

To sprawia, że Otoczenie postaci bardzo silnie ową postać definiuje. Dlatego musi być tak silne mechanicznie - bez otoczenia postać jest po prostu nie dość zdefiniowana.

### 4.5. Znaczenie Motywacji

#### 4.5.1. Problem dryfu postaci

Poważnym problemem podczas grania przez dłuższy okres czasu jest dryf postaci. Podczas pierwszej Opowieści wszyscy świetnie pamiętają, jak ta postać się zachowuje. Jednak jak minie miesiąc czy rok - zwłaszcza podczas małej ilości Opowieści (np. 1/miesiąc) - sam Gracz zapomina jak jego postać winna się zachowywać.

Postać zaczyna zachowywać się jak Gracz. Bo Gracz zwyczajnie nie pamięta czym gra.

#### 4.5.2. Problem sprzeczności mechaniki z fikcją

Czasem postać ma mechaniczne bonusy do czegoś, co powinno być sprzeczne z konceptem postaci / trudne dla postaci. Wyobraźmy sobie taką sztampową scenę:

Paladyn może podczas przesłuchania torturować swojego przeciwnika... lub przekonać go, by ów współpracował. Rozpatrzmy to typowo mechanicznie:

* Paladyn: 
    * torturowanie: 1
    * przekonywanie: 5
* Przeciwnik 
    * odporny na ból: 3 
    * odporny na przekonywanie: 10
* Potencjalny konflikt:
    * Paladyn torturuje: 1 -  3 = (-2)
    * Paladyn negocjuje: 5 - 10 = (-5)

Jak widać, z perspektywy czysto logicznej i taktycznej, paladyn powinien torturować swojego przeciwnika. Gracz **chce** wygrać konflikt. Ale jego postać - paladyn - **nie chce** torturować. Innymi słowy, matematyka systemu RPG zachęca Gracza do zrobienia czegoś niezgodnego z postacią. **Innymi słowy**: mechanika nie wspiera fikcji.

A podstawowym założeniem tej mechaniki jest to, że chcemy wynagradzać _promowane_ zachowania. Czyli powyższa sytuacja jest błędem mechaniki.

#### 4.5.3. Rozwiązanie obu problemów

Wprowadzenie _Motywacji_ jako bonusu do wykonywanej akcji. Niech Motywacja także zmienia matematykę sesji, niech sprawia, że to co jest właściwe _z perspektywy postaci_ jest tym, co będzie wykonywane. Dla naszego paladyna łatwiej jest przekonywać niż torturować. Więc:

* Paladyn: 
    * torturowanie: 1
    * przekonywanie: 5
    * motywacja: "czynić to co należy": 2
* Przeciwnik 
    * odporny na ból: 3 
    * odporny na przekonywanie: 10
* Potencjalny konflikt:
    * Paladyn torturuje: 1 -  3 - 2 = (-4)
    * Paladyn negocjuje: 5 - 10 + 2 = (-3)

Teraz sytuacja jest prawidłowa. Naszemu paladynowi bardziej opłaca się zrobić to, co _dla paladyna_ jest łatwiejsze i bardziej naturalne. Nie trzeba pamiętać, jak postać ma się zachować - wynika to samo z matematyki mechaniki.

Zauważcie, że gdyby nasz gracz grał jakimś czarnym rycerzem czy antypaladynem, to taka postać zachowa się inaczej:

* Czarny Paladyn: 
    * torturowanie: 1
    * przekonywanie: 5
    * motywacja: "maksymalizować cierpienie": 2
* Przeciwnik 
    * odporny na ból: 3 
    * odporny na przekonywanie: 10
* Potencjalny konflikt:
    * Paladyn torturuje: 1 -  3 + 2 = ( 0)
    * Paladyn negocjuje: 5 - 10 - 2 = (-7)

I nagle ów nieszczęsny "Munchkin" czy "Powergamer" gra prawidłowo swoją postacią. Paladyn zachowuje się jak paladyn, czarny paladyn jak czarny paladyn. Gracz nie musi pamiętać jak powinna zachowywać się postać. Mechanika wspiera a nie przeszkadza.

I dlatego Motywacja jest ważna ;-).

### 4.6. Czemu dzielimy postać na Obszary i Aspekty?

#### 4.6.1. Ogólny opis zagadnienia

Ogólna struktura Profilu wygląda tak:

* Kategoria (Umiejętność)
    * Obszar (Listonosz)
        * Aspekt_1, Aspekt_2... ("znajomość okolicy", "wyciąganie plotek")

#### 4.6.2. Powód: Od ogółu do szczegółu

Gdy próbujemy postać zrobić, zapoznać się z nią, przypomnieć ją sobie lub po prostu ją zrozumieć, musimy mieć "coś" czego możemy się złapać. Obszary dają taką możliwość.

Spójrzmy na przykładową postać; same motywacje i umiejętności:

* Motywacje
    * BÓL: Być zimnym mięśniakiem mafii
    * FILOZOFIA: Pacyfistyczny optymizm
    * METODY: Trucker z koneksjami
    * MARZENIE: Własny warsztat i 'garaż'
    * KULTURA: Współpraca, oszczędność, uczciwość
* Umiejętności
    * Kierowca ciężarówki
    * Mechanik pojazdów
    * Strongman

Mniej więcej widać postać powyżej jest i w jaki sposób się zachowa w różnych sytuacjach. Nie są nawet potrzebne dodefiniowujące aspekty. Jest _wystarczająco_ zdefiniowany z perspektywy prowadzenia poszczególnej Opowieści. Pięć minut i można zaczynać tworzyć z nim Opowieść.

#### 4.6.3. Powód: Zróżnicowanie postaci

Wyobraźmy sobie dwie postacie, mające następujące Motywacje i Umiejętności (uproszczone):

Ania:

* Motywacja
    * MARZENIE: Żeby terminusi nie byli już potrzebni
    * METODY: Odstraszająca przewaga
* Umiejętności:
    * Terminus antypotworowy
    * Medyk polowy

Basia:

* Motywacja
    * MARZENIE: Żeby terminusi nie byli już potrzebni
    * METODY: Odstraszająca przewaga
* Umiejętności:
    * Terminus antypotworowy
    * Medyk polowy

Te postacie są identyczne, zarówno w Konfliktach jak i w odgrywaniu. Nie rozumiemy między nimi różnic. A teraz dodajmy im Aspekty:

Ania:

* Motywacja
    * MARZENIE: Żeby terminusi nie byli już potrzebni
        * Aspekty: niewinni nie mogą ucierpieć, leczenie nad niszczenie
    * METODY: Odstraszająca przewaga
        * Aspekty: efektowność przez przygotowanie, inspirowanie własnym przykładem
* Umiejętności:
    * Terminus antypotworowy
        * Aspekty: specjalistka od efemeryd, unieszkodliwianie a nie zabijanie
    * Medyk polowy
        * Aspekty: stabilizacja i staza, wzmacnianie artefaktów leczniczych

Basia:

* Motywacja
    * MARZENIE: Żeby terminusi nie byli już potrzebni
        * Aspekty: cel uświęca środki, promuje działania długoterminowe
    * METODY: Odstraszająca przewaga
        * Aspekty: efektowność przez siłę ognia, dowodzenie przez zastraszanie
* Umiejętności:
    * Terminus antypotworowy
        * Aspekty: walka w mieście, sprzątanie śladów
    * Medyk polowy
        * Aspekty: kojenie cierpienia, rozrywanie opętań

Widzicie, co się stało?

**Ania** jest osobą, która raczej unieszkodliwia przeciwników i skupia się na tym, by dostarczyć ciężko rannych do innego lekarza. Ważniejsze dla niej jest to, by niewinnym nic się nie stało niż na niszczeniu potworów. Raczej inspiruje niż rozkazuje. Jest to osoba wyglądająca na bardziej metodyczną - jest to jednostka wsparcia.

**Basia** natomiast jest osobą działającą na pierwszej linii ognia. Nie będzie mieć problemów z poświęceniem niewinnych czy cywilów, jak długo świat będzie lepszy. Ania raczej nie powie "ich się nie da uratować", Basia nie będzie miała takich oporów. Basia jest zdecydowanie terminusem miejskim i w miastach czuje się najlepiej.

Zauważcie, że obie te postacie mogą mieć identyczne Obszary, ale są zupełnie różne.

