### Punkt startowy

Pomyśl o dwóch postaciach filmowych lub książkowych. Lub pomyśl o konceptach takich postaci. Nie musisz mieć świetnej wizji, wystarczy Ci "kapitan statku" czy "bohaterski barbarzyńca w mieście"

### Koncept

1. Spróbuj sformułować postać w formie ogólnego obrazu.
    1. (przymiotnik)(zawód) który (często/czasem/zwykle) (działanie). 
    1. _"Twardy kapitan statku który często śpiewa piosenki pop_
2. Z czego jest znany w swoich kręgach / w okolicy? Co sprawia, że nie jest anonimową postacią?
    1. Znany z (działanie)(przy okoliczności).
    2. _"Znany z niesamowitych opowieści o przygodach na morzu by się popisać przed młodymi"_
3. Co inni powiedzieliby o tej postaci - co szczególnie rzuca się w oczy? Co sprawia, że postać jest nietypowa?
    1. Dziwne jest to, że (cecha/własność), ponieważ (powód nietypowości).
    2. _Dziwne jest to, że nie pije alkoholu, ponieważ kolekcjonuje rzadkie trunki._
4. Co robił wcześniej? Skąd pochodzi? Coś o przeszłości postaci.
    1. Kiedyś był (znany/znana) z (czynność/zawód), ale (niefortunna/radosna okoliczność).
    2. _Kiedyś był znanym kabareciarzem, ale wpadł w długi i musiał zwiać na morze._

### Otoczenie

1. Spróbuj określić, gdzie ta postać najchętniej działa, ogólnie. Gdzie czuje się najlepiej, najbezpieczniej. Gdzie jest najskuteczniejsza.
    1. Czuje się w (miejsce z okolicznościami) jak w domu. Szczególnie lubi, gdy (okoliczności).
    2. _Czuje się w tawernach portowych jak w domu. Szczególnie lubi, gdy jest ciasno i niewiele widać._
2. Czy lubi tłumy? Samotność? Co lubi robić w tamym miejscu i jak to się ma do ludzi?
    1. Najbardziej kocha (okoliczność działania w miejscu) w towarzystwie (dużo/mało/brak) ludzi, ponieważ (powód). 
    2. _Najbardziej kocha robić show w towarzystwie niewielkiej ilości ludzi, ponieważ trochę brak mu kabaretu._
3. Czy woli konkretną porę roku? Porę dnia? Pogodę? Czy ma jakieś poszerzone czy zawężone miejsce na którym postaci zależy?
    1. Najlepiej czuje się gdy (okoliczność dnia/pogody). Ogólnie, lubi (poszerzone/zawężone miejsce)
    2. _Najlepiej czuje się wieczorem, przy ograniczonej widoczności i paskudnej pogodzie. Ogólnie, lubi miasta portowe i statki._

### Misja

1. Jaki fragment świata uważa za niewłaściwy i chce go zmienić? W jakim zakresie chce zmienić świat na "właściwsze"? Co faktycznie robi?
    1. Nie zgadza się na (aktualna sytuacja). Pragnie świata, w którym (obserwowalna zmiana świata). Chce pomóc (komu?) w (robieniu czego) przez (czynność).
    2. _Nie zgadza się na to, by być anonimowym, szarym magiem. Pragnie świata, w którym wszyscy będą z utęsknieniem wyczekiwać jego przybycia do tawerny. Chce pomóc sobie w osiągnięciu sławy przez epickie opowieści_
2. W jaki sposób przekładają się działania postaci na to, co chce osiągnąć? Co robi i dlaczego?
    1. Pragnie (czego:zdarzenie/okoliczność/zmiana świata) i by to się stało (wykonuje działanie/pozyskuje zasoby).
    2. _Pragnie być znanym i podziwianym i by to się stało silnie reinwestuje zyski w budowanie swojej pozycji - kupuje klakierów_
3. Co **obserwowalnego** ta postać chce zobaczyć na łożu śmierci? Po czym pozna, że osiągnęła sukces?
    1. Jeżeli wszystko mu się uda, to (zmiana rzeczywistości).
    2. _Jeżeli wszystko mu się uda, to będzie najbardziej podziwianym kapitanem morskim w Pierwokraju_

### Wada

1. Z czym postać ma największe problemy? Z jaką czynnością wiążą się główne słabości postaci?
    1. Najsłabiej radzi sobie z (czynność/okoliczności) podczas (okoliczności) co jest spowodowane przez (przyczyna).
    2. _Najsłabiej radzi sobie z orientowaniem się na lądzie, co jest spowodowane przez klątwę podłapaną podczas ucieczki przed długami_
1. Jak postać reaguje na tą wadę? Wie o tym? Ignoruje? Jak się czuje z tą wadą?
    1. Przez problemy z (czynność/okoliczności), (reakcja)(czynność). Czuje się z tym (uczucie).
    2. _Przez problemy z orientacją, unika samotnego poruszania się na lądzie. Jest bardzo zażenowany zarówno długami jak i tą klątwą_
1. W czym obserwowalnie postaci przeszkadza ta wada? W jakich okolicznościach sprawia to problem postaci?
    1. Z uwagi na to, (postać wykonuje czynność inaczej). Nie jest zbyt skuteczny przy (wykonanie czynności) i często musi (podejście alternatywne).
    2. _Z uwagi na to, rzadko porusza się po portach i w głębi lądu. Nie jest zbyt skuteczny przy pozyskiwaniu zleceń i sławy i często musi płacić mniej pewnym agentom lub prosić by ktoś do niego przyszedł. Nie może wiarygodnie sprawdzić swojej sławki osobiście._

### Motywacje

1. Jakie czynności wykonuje postać by osiągnąć swoje cele? Co sprawia, że wstaje z łóżka gdy nadchodzi nowy piękny dzień?
    1. (czynność w formie rozkazującej) (w jakich okolicznościach powiązanych z Misją)
    2. _Buduj swój wizerunek, by być sławnym i zapraszanym do tawern._
2. Jak wykorzystuje swoje talenty by czerpać korzyści dla siebie? Jakie to korzyści?
    1. (czynność w formie rozkazującej powiązana z Koncepcją) by (konkretna obserwowalna korzyść dla siebie)
    2. _Pływaj po niebezpiecznych trasach, by odkryć sposób na zdjęcie z siebie klątwy._
3. Jak wykorzystuje swoje talenty by zmieniać rzeczywistość dla innych? Jak wyglądać ma ta zmiana?
    1. (czynność w formie rozkazującej powiązana z Koncepcją) by (konkretna zmiana rzeczywistości dotykająca innych osób)
    2. _Śpiewaj i rób show by dać innym radość i pokazać, że życie nie jest złe._

Dla wszystkich tych trzech rzeczy powyżej określ MAKSYMALNĄ antytezę postaci - coś, czego by nie chciała zrobić - i podaj je do odpowiedniej antytezy postaci w prawej stronie tabelki.

### Zachowania

1. Jak ta postać zachowuje się na co dzień? Jak zachowuje się w sytuacjach typowych? Jakie jest pierwsze wrażenie gdy się spotka tą postać? 
    1. (przymiotnik), (jak się zachowuje), (opis pierwszego wrażenia)
    2. _Wesoły, pełen energii wilk morski chętny do dzielenia się opowieściami._
2. Jak ta postać się zachowuje gdy kogoś zna bliżej? Jak postrzegają ją osoby którym ufa? Jak zachowuje się w dobrym humorze?
    1. (przymiotnik), (jak się zachowuje), (opis dla przyjaciół)
    2. _Lekko cyniczny, zawsze chce Coś Robić, próbuje wszystkim pomóc by nie cierpieli_
3. Jak postać się zachowuje gdy ma zły humor? Gdy coś wytrąci ją z równowagi? Ma jakieś tiki nerwowe? Jak reaguje jak wszystko się psuje?
    1. (przymiotnik), (jak się zachowuje), (opis pod wpływem presji lub stresu)
    2. _Histerycznie wesoły, staje się niesamowicie głośny i energiczny; działanie dla działania._

### Umiejętności

1. W jaki sposób ta postać zarabia? Co najczęściej robi podczas wykonywania tej pracy?
    1. (zawód/kategoria): (czynność_1), (czynność_2), (czynność_3)...
    2. _Kapitan statku eterycznego: nawigacja, zarządzanie niewielką załogą, ogólne poruszanie się na statku._
2. W czym jeszcze ta postać jest kompetentna? Może jako hobby a może co robiła kiedyś? Co robi podczas wykonywania tej pracy?
    1. (zawód/kategoria): (czynność_1), (czynność_2), (czynność_3)...
    2. _Kabareciarz: dobrze śpiewa pop, robi dobre show, wzbudzanie śmiechu, proste sztuczki magiczne_
3. Jakie są główne umiejętności wspomagające zawód postaci? Czego jeszcze potrzebuje do bycia skuteczną? Jakie czynności wykonuje?
    1. (zawód/kategoria): (czynność_1), (czynność_2), (czynność_3)...
    2. _Bajarz: przyciąganie uwagi morskimi opowieściami, budowanie wizerunku, bajerowanie innych, oczarowywanie opowieściami_

### Magia - gdy kontroluje

1. 
    1. 
    2. 
2. 
    1. 
    2. 
3. 
    1. 
    2. 

### Magia - gdy traci kontrolę

1. 
    1. 
    2. 
2. 
    1. 
    2. 
3. 
    1. 
    2. 
