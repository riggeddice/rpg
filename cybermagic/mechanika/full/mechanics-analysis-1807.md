# Mechanika tokenowa - analiza

## 1. Aktualność dokumentu

* Dokument jest aktualny wobec mechaniki 1807
* Dokument jest wyprodukowany: 180723
* Ostatnia aktualizacja: 180723

## 2. Dla parametrów

| Parametr ustawiony    | Wartość |
| weak-profile          |  2, 0, 0  |
| competent-profile     |  6, 0, 0  |
| maxed-profile         |  9, 0, 0  |
| easy-conflict         |  2, 3, 4  |
| typical-conflict      |  0, 3, 6  |
| hard-conflict         |  -2, 3, 8  |
| heroic-conflict       |  -4, 5, 20  |
| resource              |  3, 0, 0  |
| magic                 |  3, 0, 0  |
| scene (up to)         |  4, 0, 0  |
| red reduction step    |  3 |

## 3. Założenia

### 3.1. Usability

* Naraz w pudełku może być maksymalnie 15 tokenów

### 3.2. Założenia co do konfliktów

#### 3.2.1 Rozkład na czuja

* Test typowy przy MAKSYMALNYM WYMAKSOWANIU ma rozkład podobny do [95, 5, 0]
* Test typowy dla kompetentnej postaci ma rozkład podobny do [40, 30, 30]
* Test heroiczny dla kompetentnej postaci ma rozkład podobny do [0, 25, 75]
* Test heroiczny przy MAKSYMALNYM WYMAKSOWANIU ma rozkład podobny do [30, 40, 30]

#### 3.2.2 Optymalizacja pod kątem Kompetentnego

**Założenia**

* Testujemy dla osoby Kompetentnej
* [Sukces, Skonfliktowanie, Porażka]

**Oczekiwane prawdopodobieństwa**

* Łatwy konflikt: [60, 30, 10]
* Typowy konflikt: [50, 30, 20]
* Trudny konflikt: [20, 30, 50]
* Heroiczny konflikt: [ 0, 20, 80]

## 4. Analiza liczb mechaniki

### 4.1. Kluczowe prawdopodobieństwa

#### Dla braku modyfikatorów i "równorzędnych" konfliktów

| Profil, konflikt, modyfikatory                                     | P(Sukces) | P(Skonfliktowany) | P(Porażka) |
|--------------------------------------------------------------------|-----------|-------------------|------------|
| weak-profile easy-conflict no-mods                                 | 0.40 | 0.33 | 0.27 |
| competent-profile typical-conflict no-mods                         | 0.42 | 0.35 | 0.23 |
| maxed-profile hard-conflict no-mods                                | 0.35 | 0.38 | 0.28 |

Z powyższego wynika jednoznacznie - profil danego typu wspiera "swój" konflikt. Jest około 30% szansy całkowitej porażki, 30% skonfliktowania i 30-40% sukcesu w "swojej" kategorii.

#### Dla użycia dodatkowej karty w "równorzędnym" konflikcie (Zasób lub Magia)

| Profil, konflikt, modyfikatory                                     | P(Sukces) | P(Skonfliktowany) | P(Porażka) |
|--------------------------------------------------------------------|-----------|-------------------|------------|
| weak-profile easy-conflict resources                               | 0.64 | 0.26 | 0.11 |
| weak-profile easy-conflict resources magic                         | 0.82 | 0.14 | 0.03 |
| competent-profile typical-conflict resources                       | 0.60 | 0.29 | 0.11 |
| competent-profile typical-conflict resources magic                 | 0.75 | 0.20 | 0.05 |
| maxed-profile hard-conflict resources                              | 0.49 | 0.35 | 0.16 |
| maxed-profile hard-conflict resources magic                        | 0.62 | 0.29 | 0.09 |

Progresywnie, wpływ dodatkowej karty maleje. Jednak przesuwa to "odpowiedni" konflikt na poziom 50% sukcesu i ~10% całkowitej porażki. Co więcej, z uwagi na skok między poszczególnymi (weak-competent-maxed), mamy obraz sytuacji.

Dodanie drugiej karty jest mniej korzystne - pomaga gdy stawka jest za duża. Im wyższy stopień trudności konfliktu, tym mniejszy wpływ.

#### Dla braku modyfikatorów i "słabszych" konfliktów

| Profil, konflikt, modyfikatory                                     | P(Sukces) | P(Skonfliktowany) | P(Porażka) |
|--------------------------------------------------------------------|-----------|-------------------|------------|
| competent-profile easy-conflict no-mods                            | 0.67 | 0.24 | 0.09 |
| maxed-profile easy-conflict no-mods                                | 0.84 | 0.13 | 0.03 |
| maxed-profile typical-conflict no-mods                             | 0.60 | 0.29 | 0.11 |

Najważniejsze, że nie ma tu dużych szans na pełną Porażkę. Raczej mamy Skonfliktowany Sukces. Czyli interesy gracza są zapewnione gdy celuje w dół.

#### Dla braku modyfikatorów i "strzelania w górę" konfliktów

| Profil, konflikt, modyfikatory                                     | P(Sukces) | P(Skonfliktowany) | P(Porażka) |
|--------------------------------------------------------------------|-----------|-------------------|------------|
| weak-profile typical-conflict no-mods                              | 0.13 | 0.27 | 0.60 |
| competent-profile hard-conflict no-mods                            | 0.20 | 0.34 | 0.46 |
| maxed-profile heroic-conflict no-mods                              | 0.09 | 0.26 | 0.66 |

Sytuacja jest odwrotna wobec "słabszych" konfliktów. Tu prawdopodobieństwo Porażki jest bardzo duże - Gracz wchodzący do takiego konfliktu będzie chciał raczej wspomóc się Zasobem czy ufortyfikować jakoś okolicznościami.

#### Dla braku modyfikatorów i "strzelania w górę" konfliktów o 2 stopnie

| Profil, konflikt, modyfikatory                                     | P(Sukces) | P(Skonfliktowany) | P(Porażka) |
|--------------------------------------------------------------------|-----------|-------------------|------------|
| weak-profile hard-conflict no-mods                                 | 0.00 | 0.05 | 0.95 |
| competent-profile heroic-conflict no-mods                          | 0.03 | 0.14 | 0.83 |

Zgodnie z planem, nie ma po co w to celować bez wzmocnienia. Ale z modyfikatorami daje to możliwość całkiem niezłego ufortyfikowania się jak pokaże następna kategoria.

#### Dla Heroicznych konfliktów

| Profil, konflikt, modyfikatory                                     | P(Sukces) | P(Skonfliktowany) | P(Porażka) |
|--------------------------------------------------------------------|-----------|-------------------|------------|
| weak-profile heroic-conflict scene resources magic                 | 0.15 | 0.33 | 0.52 |
| competent-profile heroic-conflict scene resources magic            | 0.24 | 0.38 | 0.38 |
| maxed-profile heroic-conflict scene resources magic                | 0.30 | 0.39 | 0.30 |

Heroiczne konflikty są konfliktami w których już nie da się sensownie wygrywać. Tutaj dosłownie "jak nas kości poniosą", jak pójdzie RNG. Najlepiej jest rozbijać konflikty tej klasy na niższe poziomy i nie ryzykować porażek przy odpowiednio dużych stawkach.

## 5. Wyniki mechaniki tokenowej

| Profil, konflikt, modyfikatory                                     | P(Sukces) | P(Skonfliktowany) | P(Porażka) |
|--------------------------------------------------------------------|-----------|-------------------|------------|
| weak-profile easy-conflict no-mods                                 | 0.40 | 0.33 | 0.27 |
| weak-profile easy-conflict magic                                   | 0.64 | 0.26 | 0.11 |
| weak-profile easy-conflict resources                               | 0.64 | 0.26 | 0.11 |
| weak-profile easy-conflict scene                                   | 0.67 | 0.24 | 0.09 |
| weak-profile easy-conflict weakness                                | 0.27 | 0.35 | 0.38 |
| weak-profile easy-conflict scene resources                         | 0.84 | 0.13 | 0.03 |
| weak-profile easy-conflict resources magic                         | 0.82 | 0.14 | 0.03 |
| weak-profile easy-conflict scene resources magic                   | 0.98 | 0.02 | 0.00 |
| weak-profile easy-conflict scene weakness                          | 0.50 | 0.33 | 0.17 |
| weak-profile easy-conflict resources weakness                      | 0.46 | 0.34 | 0.20 |
| weak-profile easy-conflict scene resources weakness                | 0.65 | 0.26 | 0.09 |
| weak-profile easy-conflict scene resources magic weakness          | 0.78 | 0.18 | 0.04 |
| weak-profile typical-conflict no-mods                              | 0.13 | 0.27 | 0.60 |
| weak-profile typical-conflict magic                                | 0.32 | 0.36 | 0.32 |
| weak-profile typical-conflict resources                            | 0.32 | 0.36 | 0.32 |
| weak-profile typical-conflict scene                                | 0.42 | 0.35 | 0.23 |
| weak-profile typical-conflict weakness                             | 0.09 | 0.24 | 0.67 |
| weak-profile typical-conflict scene resources                      | 0.60 | 0.29 | 0.11 |
| weak-profile typical-conflict resources magic                      | 0.50 | 0.33 | 0.17 |
| weak-profile typical-conflict scene resources magic                | 0.75 | 0.20 | 0.05 |
| weak-profile typical-conflict scene weakness                       | 0.31 | 0.37 | 0.31 |
| weak-profile typical-conflict resources weakness                   | 0.24 | 0.36 | 0.40 |
| weak-profile typical-conflict scene resources weakness             | 0.46 | 0.35 | 0.18 |
| weak-profile typical-conflict scene resources magic weakness       | 0.60 | 0.30 | 0.11 |
| weak-profile hard-conflict no-mods                                 | 0.00 | 0.05 | 0.95 |
| weak-profile hard-conflict magic                                   | 0.15 | 0.31 | 0.54 |
| weak-profile hard-conflict resources                               | 0.15 | 0.31 | 0.54 |
| weak-profile hard-conflict scene                                   | 0.20 | 0.34 | 0.46 |
| weak-profile hard-conflict weakness                                | 0.00 | 0.04 | 0.96 |
| weak-profile hard-conflict scene resources                         | 0.35 | 0.38 | 0.28 |
| weak-profile hard-conflict resources magic                         | 0.31 | 0.37 | 0.31 |
| weak-profile hard-conflict scene resources magic                   | 0.49 | 0.35 | 0.16 |
| weak-profile hard-conflict scene weakness                          | 0.15 | 0.32 | 0.52 |
| weak-profile hard-conflict resources weakness                      | 0.11 | 0.29 | 0.60 |
| weak-profile hard-conflict scene resources weakness                | 0.27 | 0.39 | 0.34 |
| weak-profile hard-conflict scene resources magic weakness          | 0.39 | 0.38 | 0.22 |
| weak-profile heroic-conflict no-mods                               | 0.00 | 0.03 | 0.97 |
| weak-profile heroic-conflict magic                                 | 0.02 | 0.09 | 0.89 |
| weak-profile heroic-conflict resources                             | 0.02 | 0.09 | 0.89 |
| weak-profile heroic-conflict scene                                 | 0.03 | 0.14 | 0.83 |
| weak-profile heroic-conflict weakness                              | 0.00 | 0.03 | 0.97 |
| weak-profile heroic-conflict scene resources                       | 0.09 | 0.26 | 0.66 |
| weak-profile heroic-conflict resources magic                       | 0.07 | 0.23 | 0.70 |
| weak-profile heroic-conflict scene resources magic                 | 0.15 | 0.33 | 0.52 |
| weak-profile heroic-conflict scene weakness                        | 0.03 | 0.13 | 0.84 |
| weak-profile heroic-conflict resources weakness                    | 0.01 | 0.08 | 0.90 |
| weak-profile heroic-conflict scene resources weakness              | 0.08 | 0.25 | 0.68 |
| weak-profile heroic-conflict scene resources magic weakness        | 0.13 | 0.32 | 0.55 |
| competent-profile easy-conflict no-mods                            | 0.67 | 0.24 | 0.09 |
| competent-profile easy-conflict magic                              | 0.84 | 0.13 | 0.03 |
| competent-profile easy-conflict resources                          | 0.84 | 0.13 | 0.03 |
| competent-profile easy-conflict scene                              | 0.97 | 0.03 | 0.00 |
| competent-profile easy-conflict weakness                           | 0.50 | 0.33 | 0.17 |
| competent-profile easy-conflict scene resources                    | 0.98 | 0.02 | 0.00 |
| competent-profile easy-conflict resources magic                    | 0.98 | 0.02 | 0.00 |
| competent-profile easy-conflict scene resources magic              | 0.99 | 0.01 | 0.00 |
| competent-profile easy-conflict scene weakness                     | 0.75 | 0.20 | 0.05 |
| competent-profile easy-conflict resources weakness                 | 0.65 | 0.26 | 0.09 |
| competent-profile easy-conflict scene resources weakness           | 0.88 | 0.11 | 0.02 |
| competent-profile easy-conflict scene resources magic weakness     | 0.99 | 0.01 | 0.00 |
| competent-profile typical-conflict no-mods                         | 0.42 | 0.35 | 0.23 |
| competent-profile typical-conflict magic                           | 0.60 | 0.29 | 0.11 |
| competent-profile typical-conflict resources                       | 0.60 | 0.29 | 0.11 |
| competent-profile typical-conflict scene                           | 0.62 | 0.28 | 0.10 |
| competent-profile typical-conflict weakness                        | 0.31 | 0.37 | 0.31 |
| competent-profile typical-conflict scene resources                 | 0.76 | 0.19 | 0.05 |
| competent-profile typical-conflict resources magic                 | 0.75 | 0.20 | 0.05 |
| competent-profile typical-conflict scene resources magic           | 0.88 | 0.10 | 0.02 |
| competent-profile typical-conflict scene weakness                  | 0.49 | 0.35 | 0.16 |
| competent-profile typical-conflict resources weakness              | 0.46 | 0.35 | 0.18 |
| competent-profile typical-conflict scene resources weakness        | 0.62 | 0.29 | 0.09 |
| competent-profile typical-conflict scene resources magic weakness  | 0.73 | 0.22 | 0.05 |
| competent-profile hard-conflict no-mods                            | 0.20 | 0.34 | 0.46 |
| competent-profile hard-conflict magic                              | 0.35 | 0.38 | 0.28 |
| competent-profile hard-conflict resources                          | 0.35 | 0.38 | 0.28 |
| competent-profile hard-conflict scene                              | 0.38 | 0.38 | 0.24 |
| competent-profile hard-conflict weakness                           | 0.15 | 0.32 | 0.52 |
| competent-profile hard-conflict scene resources                    | 0.51 | 0.34 | 0.15 |
| competent-profile hard-conflict resources magic                    | 0.49 | 0.35 | 0.16 |
| competent-profile hard-conflict scene resources magic              | 0.63 | 0.28 | 0.09 |
| competent-profile hard-conflict scene weakness                     | 0.30 | 0.39 | 0.30 |
| competent-profile hard-conflict resources weakness                 | 0.27 | 0.39 | 0.34 |
| competent-profile hard-conflict scene resources weakness           | 0.42 | 0.38 | 0.20 |
| competent-profile hard-conflict scene resources magic weakness     | 0.53 | 0.34 | 0.13 |
| competent-profile heroic-conflict no-mods                          | 0.03 | 0.14 | 0.83 |
| competent-profile heroic-conflict magic                            | 0.09 | 0.26 | 0.66 |
| competent-profile heroic-conflict resources                        | 0.09 | 0.26 | 0.66 |
| competent-profile heroic-conflict scene                            | 0.11 | 0.29 | 0.60 |
| competent-profile heroic-conflict weakness                         | 0.03 | 0.13 | 0.84 |
| competent-profile heroic-conflict scene resources                  | 0.17 | 0.35 | 0.48 |
| competent-profile heroic-conflict resources magic                  | 0.15 | 0.33 | 0.52 |
| competent-profile heroic-conflict scene resources magic            | 0.24 | 0.38 | 0.38 |
| competent-profile heroic-conflict scene weakness                   | 0.10 | 0.28 | 0.62 |
| competent-profile heroic-conflict resources weakness               | 0.08 | 0.25 | 0.68 |
| competent-profile heroic-conflict scene resources weakness         | 0.15 | 0.34 | 0.50 |
| competent-profile heroic-conflict scene resources magic weakness   | 0.21 | 0.38 | 0.41 |
| maxed-profile easy-conflict no-mods                                | 0.84 | 0.13 | 0.03 |
| maxed-profile easy-conflict magic                                  | 0.98 | 0.02 | 0.00 |
| maxed-profile easy-conflict resources                              | 0.98 | 0.02 | 0.00 |
| maxed-profile easy-conflict scene                                  | 0.98 | 0.02 | 0.00 |
| maxed-profile easy-conflict weakness                               | 0.65 | 0.26 | 0.09 |
| maxed-profile easy-conflict scene resources                        | 0.99 | 0.01 | 0.00 |
| maxed-profile easy-conflict resources magic                        | 0.98 | 0.02 | 0.00 |
| maxed-profile easy-conflict scene resources magic                  | 0.99 | 0.01 | 0.00 |
| maxed-profile easy-conflict scene weakness                         | 0.88 | 0.11 | 0.02 |
| maxed-profile easy-conflict resources weakness                     | 0.78 | 0.18 | 0.04 |
| maxed-profile easy-conflict scene resources weakness               | 0.99 | 0.01 | 0.00 |
| maxed-profile easy-conflict scene resources magic weakness         | 0.99 | 0.01 | 0.00 |
| maxed-profile typical-conflict no-mods                             | 0.60 | 0.29 | 0.11 |
| maxed-profile typical-conflict magic                               | 0.75 | 0.20 | 0.05 |
| maxed-profile typical-conflict resources                           | 0.75 | 0.20 | 0.05 |
| maxed-profile typical-conflict scene                               | 0.76 | 0.19 | 0.05 |
| maxed-profile typical-conflict weakness                            | 0.46 | 0.35 | 0.18 |
| maxed-profile typical-conflict scene resources                     | 0.88 | 0.10 | 0.02 |
| maxed-profile typical-conflict resources magic                     | 0.88 | 0.11 | 0.02 |
| maxed-profile typical-conflict scene resources magic               | 0.99 | 0.01 | 0.00 |
| maxed-profile typical-conflict scene weakness                      | 0.62 | 0.29 | 0.09 |
| maxed-profile typical-conflict resources weakness                  | 0.60 | 0.30 | 0.11 |
| maxed-profile typical-conflict scene resources weakness            | 0.73 | 0.22 | 0.05 |
| maxed-profile typical-conflict scene resources magic weakness      | 0.83 | 0.15 | 0.03 |
| maxed-profile hard-conflict no-mods                                | 0.35 | 0.38 | 0.28 |
| maxed-profile hard-conflict magic                                  | 0.49 | 0.35 | 0.16 |
| maxed-profile hard-conflict resources                              | 0.49 | 0.35 | 0.16 |
| maxed-profile hard-conflict scene                                  | 0.51 | 0.34 | 0.15 |
| maxed-profile hard-conflict weakness                               | 0.27 | 0.39 | 0.34 |
| maxed-profile hard-conflict scene resources                        | 0.63 | 0.28 | 0.09 |
| maxed-profile hard-conflict resources magic                        | 0.62 | 0.29 | 0.09 |
| maxed-profile hard-conflict scene resources magic                  | 0.74 | 0.21 | 0.05 |
| maxed-profile hard-conflict scene weakness                         | 0.42 | 0.38 | 0.20 |
| maxed-profile hard-conflict resources weakness                     | 0.39 | 0.38 | 0.22 |
| maxed-profile hard-conflict scene resources weakness               | 0.53 | 0.34 | 0.13 |
| maxed-profile hard-conflict scene resources magic weakness         | 0.62 | 0.29 | 0.08 |
| maxed-profile heroic-conflict no-mods                              | 0.09 | 0.26 | 0.66 |
| maxed-profile heroic-conflict magic                                | 0.15 | 0.33 | 0.52 |
| maxed-profile heroic-conflict resources                            | 0.15 | 0.33 | 0.52 |
| maxed-profile heroic-conflict scene                                | 0.17 | 0.35 | 0.48 |
| maxed-profile heroic-conflict weakness                             | 0.08 | 0.25 | 0.68 |
| maxed-profile heroic-conflict scene resources                      | 0.24 | 0.38 | 0.38 |
| maxed-profile heroic-conflict resources magic                      | 0.21 | 0.37 | 0.42 |
| maxed-profile heroic-conflict scene resources magic                | 0.30 | 0.39 | 0.30 |
| maxed-profile heroic-conflict scene weakness                       | 0.15 | 0.34 | 0.50 |
| maxed-profile heroic-conflict resources weakness                   | 0.13 | 0.32 | 0.55 |
| maxed-profile heroic-conflict scene resources weakness             | 0.21 | 0.38 | 0.41 |
| maxed-profile heroic-conflict scene resources magic weakness       | 0.27 | 0.40 | 0.33 |
