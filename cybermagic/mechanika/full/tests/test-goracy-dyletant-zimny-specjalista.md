---
layout: mechanika
title: "Test: Gorący dyletant rywalizujący z zimnym specjalistą"
---

# {{ page.title }}

# Opis testu

## Słowniczek

* **Gorący XXX**: osoba mająca wszystkie bonusy na linii psychiczno-motywacyjnej, czyli Dążenia/Motywacje (proaktywne) i Charakter/Zachowania (reaktywne)
* **Zimny XXX**: osoba nie mająca żadnych bonusów na linii psychiczno-motywacyjnej, czyli Motywacje i Zachowania
* **Dyletant**: osoba nie mająca żadnych umiejętności w danej dziedzinie, czyli ani Specjalizacji ani Umiejętności
* **Specjalista**: osoba mająca wszystkie bonusy na linii umiejętności, czyli zarówno Specjalizacje jak i Umiejętności

## Cel testu

Sprawdzenie, czy Gorący Dyletant w walce z Zimnym Specjalistą działa poprawnie z perspektywy systemu. 

Ten system ma promować bardziej 'realistyczne' podejście niż 'od zera do bohatera' ('jeśli w coś bardzo wierzysz, stanie się prawdą') z przyczyn zarówno taktycznych jak i anty-minmakserskich.

W wyniku tego testu chcemy udowodnić, że osoba bardzo pragnąca czegoś, ale niekompetentna _nie_ jest w stanie mieć przewagi nad profesjonalistą, któremu zupełnie jednak nie zależy. To powoduje następujące efekty podczas samej rozgrywki:

* postacie wchodzą w konflikt dookoła swoich umiejętności...
* ...ale zwycięzcę determinują cechy psychiczno-motywacyjne i taktyka

W takim spojrzeniu umiejętności pełnią rolę 'Gate': która postać może wejść w konflikt z nadzieją sukcesu. A linia psychiczno-motywacyjna pełni rolę określenia, w jaką stronę owa postać powinna przesunąć wynik tego konfliktu.

## Oczekiwany wynik

By osiągnąć powyższe, oczekujemy następujących efektów:

* Gorący amator ma bardzo małe szanse wygrać z zimnym specjalistą w warunkach neutralnych
    * Jest to możliwe
    * Specjalista MOŻE przegrać, acz to nietypowe
* Zimny amator nie jest w stanie wygrać w warunkach neutralnych z zimnym specjalistą
* Gorący amator nie jest w stanie wygrać w warunkach neutralnych z gorącym specjalistą

# Analiza mechaniki

## Rozpatrywana mechanika

Aktualna wersja mechaniki: 1705

## Omówienie istotnych elementów mechaniki

Aktualna wersja mechaniki zawiera podział na linię psychiczno-motywacyjną (dalej: PM) i linię umiejętności (dalej: UM). Między tymi liniami występuje wewnętrzna synergia. Obie linie zapewniają bonus do +2.

Aktualna wersja mechaniki zawiera również Okoliczności akcji lub konfliktu.

W wypadku próby wykonania czynności, co do której postać nie ma ani specjalizacji ani umiejętności, postać ukarana zostaje malusem Okolicznościowym -2.

## Wynik hipotetyczny

* Gorący Dyletant: Motywacja + Zachowanie = 2
* Zimny Specjalista: Umiejętność + Specjalizacja = 2

W wypadku konfliktu wymagającego starcia się tych dwóch, Gorący Dyletant otrzymuje dodatkowy malus -2. To sprawia, że faktyczne wyniki wynoszą:

* Gorący Dyletant: Motywacja + Zachowanie - Okoliczności (2) = 0
* Zimny Specjalista: Umiejętność + Specjalizacja = 2

Z uwagi na zakres elementu Losowego [-3, 3], istnieje stosunkowo niewielka szansa na wygraną Dyletanta (5%) lub na remis Dyletanta ze Specjalistą (20%). Jeżeli jednak Specjalista nie jest całkowicie Zimny, Dyletant ma już tylko 5% szansy na remis.

# Symulacja

Niepotrzebna. Oczywista sytuacja.

# Symulacja, mechanicznie

Niepotrzebne. Oczywista sytuacja.
