---
layout: mechanika
title: "Budowanie Profilu Postaci"
---

# {{ page.title }}

## 0. Aktualność dokumentu

* Dokument jest aktualny wobec mechaniki 1804
* Dokument jest wyprodukowany: 180422
* Ostatnia aktualizacja: 180422

## 1. Ogólna struktura Profilu

### 1.1. Wyjaśnienia

![Rysunek logicznego profilu](Materials/180499_all/180422-profile-explained.png)

Profil postaci _(karta postaci)_ składa się z pięciu Obszarów.

* Trzy oznaczają "wnętrze" postaci (Motywacja, Umiejętności, Silne i Słabe Strony).
* Jedna oznacza rzeczy "zewnętrzne" do postaci do których postać ma dostęp (Zasoby).
* Jedna oznacza magię, jeśli mamy do czynienia z postacią maga (Magia).

Każdy Obszar podzielony jest dodatkowo na część ogólną (na rysunku: niebieska) i szczegółową (na rysunku: różowa)

* Część ogólna Obszaru oznacza coś szerokiego, czym postać możemy definiować.
    * Dalej Obszar jest podzielony na Kategorie (coś szerszego) i Aspekty (elementy opisowe)
    * _na rysunku: Kategorią w Motywacjach jest "Dla mnie", Aspektami: "bezpieczeństwo, szacunek"_
    * _na rysunku: Kategorią w Magii jest "Technomancja", Aspektami: "pojazdy ciężkie, zdalne sterowanie"_
* Część szczegółowa Obszaru oznacza doprecyzowanie i doszczegółowienie części ogólnej.
    * Dalej część szczegółowa podzielona jest na konkretne działania. Są one różne w zależności od Obszaru.
    * _na rysunku: w Motywacjach mamy "szczególnie Tak: **promuj praktyczne**" i "szczególnie nie: **wybierz przeżycia**"_
    * _na rysunku: w Magii mamy "co: **overdrive (...)**" i "czym: **(...) wspomaganie**"_

Wyjaśnienie struktury:

* Część ogólna jest szeroka. Ma za zadanie dać Graczowi duże możliwości działania postacią. Cel prosty - Gracz ma łatwe spojrzenie "czy to pasuje czy nie". Wie, kim postać ogólnie jest.
* Część szczegółowa jest bardzo precyzyjna. Ma za zadanie rozróżnić dwie postacie od siebie. Cel prosty - po pół roku Gracz wraca do postaci i wie, jak działać. Wie, gdzie postać jest świetna.
* Użycie tych pięciu Obszarów zapewnia wnętrze (kim jestem), zewnętrze (co mam do dyspozycji) i magię.

Wszystkie elementy Obszarów mają taką samą moc matematyczną (patrz: rozwiązywanie konfliktów) i synergizują pionowo (jednocześnie możesz raz użyć każdego elementu osobno).

### 1.2. Przydatne linki

* [Aktualny szablon (templatka)](/rpg/inwazja/opowiesci/karty-postaci/__template-1803.md)

Przykładowe postacie referencyjne:

* [Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)
* [Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)
* [Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)
* [Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html)

## 2. Motywacja

### 2.1. Co oznacza ta sekcja

Co steruje postacią? Co sprawia, że postać chce coś osiągnąć? Czemu postać maga - istoty, która nic "nie musi" - w ogóle wychodzi z łóżka rano? Co postać uważa za ważne dla siebie? Na jaką formę niesprawiedliwości się nie godzi? Kiedy postać stwierdzi "wygrałam - mogę wreszcie odpocząć"?

Innymi słowy, Motywacja to zbiór motywatorów i demotywatorów, dzięki którym zarówno Gracz ma możliwość dążyć do jakiejś przyszłości świata, w którym gracie jak i Mistrz Gry może tworzyć alternatywną przyszłość, która ową postać wciągnie. Ewentualnie - wyzwania i opozycję.

### 2.2. Jak złożyć z niej postać

### 2.2.1. Ogólne

Motywacja jest o tyle odmienna od pozostałych kategorii, że posiada trzy usztywnione Kategorie:

* Indywidualne: "co chcę dla siebie / co chcę osiągnąć"
* Społeczne: "co chcę dla innych / jakie chcę by było społeczeństwo"
* Wartości: "co jest dla mnie ważne / co cenię"

Każdej z tych kategorii przydziel docelowo ~3 aspektów.

Przykładowe kombinacje:

* Krystalia Diakon, indywidualne: _intensywność; doskonałość_
* Maurycy Maus, społeczne: _transhumanizm; altruizm_
* Siluria Diakon, wartości: _dobra zabawa; rodzina; wolność; sprawiedliwość_

### 2.2.1. Szczegółowe

Motywacja podzielona jest na dwie kolumny: **szczególnie tak** i **szczególnie nie**. Oznaczają one "do czego postać szczególnie dąży" i "przeciwko czemu postać szczególnie protestuje". Oba są podane w formie imperatywnej (rozkazującej) - _rób, promuj, dąż do_.

Wpisz sobie ~3 **tak** i **nie**, by dało się określić co postać przyciąga a co odpycha. Przykłady (po jednym):

* Krystalia Diakon: _TAK - wymuś silne emocje od 'twardziela'_ NIE - _pomóż innym zachować twarz i kontrolę nad sobą_
* Maurycy Maus: _TAK - zbuduj najdoskonalsze, wieczne ciało_ _NIE  - dąż do potęgi nawet kosztem duszy_
* Siluria Diakon: _TAK - używaj ciała jako narzędzia_ _NIE - bądź skrępowany przez konwenanse i moralność_

## 3. Umiejętności

### 3.1. Co oznacza ta sekcja

Zbiór rzeczy, które postać potrafi zrobić _szczególnie_ dobrze do poziomu, w którym inni rozpoznają TĄ POSTAĆ jako specjalistkę od tego czegoś. Nie trzeba wpisywać tu np. "Kierowca ciężarówki", jeśli nasza postać potrafi prowadzić ciężarówkę - trzeba jednak to wpisać, jeśli nasza postać ma być bardzo dobrym kierowcą, do poziomu zawodowego / eksperckiego.

Jeśli postać ma tu coś wpisane, to znaczy, że jest w stanie pełnić tą rolę na poziomie zawodowym.

### 3.2. Jak złożyć z niej postać

### 3.2.1. Ogólne

Umiejętności są podzielone na Kategorie i Aspekty. Każda Kategoria ma przypisane ~3~5 Aspektów.

Przykładowe kombinacje:

* Alojzy Przylaz: KATEGORIA: _wykidajło_ ASPEKTY: _zastraszanie; walka barowa; czytanie ludzi_
* Kornel Maus: KATEGORIA: _resocjalizator_ ASPEKTY: _praca z młodzieżą; labirynt biurokracji; inspirowanie ku dobru_
* Krystalia Diakon: KATEGORIA _farmaceuta_ ASPEKTY: _psychotropy; mindbreak_

### 3.2.2. Szczegółowe

Szczególny podział Umiejętności nazywamy Manewrami.

Manewry są podzielone na dwie kolumny: **co** i **czym**. W ramach pojedynczego rzędu są one produktem kartezjańskim - każdy element **co** oddzielony średnikiem łączy się z każdym elementem **czym**. Zwykle postać ma około 5 rzędów logicznie powiązanych ze sobą Manewrów.

Manewry oznaczają "jak ta postać najczęściej działa". To jak na filmie - pojawia się postać XXX i od razu wiemy, co się będzie mniej więcej działo, bo postać XXX zawsze robi YYY.

Przykładowe kombinacje pojedynczego rzędu Manewrów:

* Alojzy Przylaz: CO: _rozładowanie konfliktu_ CZYM: _zastraszanie; czytanie ludzi_
* Kornel Maus: CO: _powstrzymanie od złego; osłona nieletniego przed kłopotem;_ CZYM: _autorytet własny; zniechęcenie biurokracją_
* Krystalia Diakon: CO: _zniewolenie ofiary; wzbudzanie grozy; przesłuchiwanie_ CZYM: _psychotropy; intensywne uczucia;_

## 4. Silne i Słabe Strony

### 4.1. Co oznacza ta sekcja

Niektóre postacie mają rzeczy, które nie pasują do normalnego zestawienia Motywacji i Umiejętności. Coś, co je odróżnia od losowych nienazwanych postaci. Zarówno na plus jak i na minus. Może postać jest silna, ale wpada w szał berserkerski? Może jest... inteligentnym kotem? Wszelkie tego typu odchylenia od szarości zawierają się w tej sekcji. 

Jeśli postać ma tu coś wpisane, to znaczy, że dla Gracza ta cecha postaci jest kluczowa do zrozumienia koncepcji tej postaci. Nie każda silna postać będzie mieć tu "Silny". Każda postać, która ma tu wpisane "Silny" oznacza, że Gracz tej postaci oczekuje, że postać rozwiązuje swoje problemy siłą.

### 4.2. Jak złożyć z niej postać

### 4.2.1. Ogólne

Siły/Słabości są podzielone na Kategorie i Aspekty. Każda Kategoria ma przypisane ~3~5 Aspektów. Nie każda postać ma Silne i Słabe Strony; zwykle postać ma najwyżej jedną Kategorię (w odróżnieniu od Umiejętności, gdzie postać ma zwykle trzy).

Przykładowe kombinacje:

* Lucjan Kopidół: KATEGORIA: _niezrozumiały umysł_ ASPEKTY: _dziwak; obcy umysł; mag anomalny_
* Siluria Diakon: KATEGORIA: _nieuchwytna tyranka tyranów_ ASPEKTY: _dominatrix; bezwzględna pani_
* Krystalia Diakon: KATEGORIA _kralothbound_ ASPEKTY: _samosyntezator biologiczny; nieludzka_

### 4.2.2. Szczegółowe

Szczegółowe rozbicie Silnych Słabych Stron to Manewry opisane jako **co jest wzmocnione** i **kosztem czego**. Dwie tabelki, zasilane logicznie z Aspektów części ogólnej. Tak jak w wypadku Motywacji, nie analizujemy tu produktu kartezjańskiego - każdą komórkę (kombinację wiersza i kolumny) rozpatrujemy w izolacji.

Przykładowe kombinacje:

* Lucjan Kopidół: CO: _bardzo odporny na wszelkie działania mentalne i "dziwność"_ KOSZTEM: _nie może korzystać z pozytywnych cech Pryzmatu_
* Siluria Diakon: CO: _uroda; ciało z marzeń rozmówcy_ KOSZTEM: _MUSI wygrywać i być na szczycie_
* Krystalia Diakon: CO: _niezatrzymywalna i nieperswadowalna; fizycznie i chemicznie_ KOSZTEM: _jej percepcja rzeczywistości jest inna; fatalnie obserwuje_

## 5. Otoczenie

### 5.1. Co oznacza ta sekcja

Żadna postać nie działa w izolacji. Ma znajomości, przyjaciół, narzędzia których używa i które ją charakteryzują. Czym byłby Batman bez pasa, Alfreda czy jaskini? Zaznaczam, że nie zapisujemy tu WSZYSTKIEGO. Nie musimy pisać "samochód" by postać miała dostęp do samochodu - musimy wpisać "samochód", jeśli jest to pojazd który wyróżnia tą postać od innych postaci.

Znowu: to, co tu wpisujemy to nie jest "wszystko, co postać posiada". To są rzeczy znaczące, którymi Gracz chce wyróżnić postać od innych jej podobnych. Narzędzie fabularnego wpływu na rzeczywistość.

Co może należeć do Otoczenia? Wszystko, do czego postać może mieć dostęp:

* Reputacja (straszna, pozytywna, bohater łysej szczeliny...)
* Ludzie (stronnicy, podwładni, możny protektor, mecenas...)
* Przedmioty (artefakty, świetne auto, poukrywane noże...)
* Miejsca (forteca, liczne kryjówki, biblioteka...)

Postacie takie jak np. Batman są w większości definiowane przez swoje Otoczenie.

### 5.2. Jak złożyć z niej postać

### 5.2.1. Ogólne

Także i tu dzielimy Otoczenie na Kategorie i Aspekty. Każda Kategoria ma ~3~5 Aspektów. Każda postać powinna mieć do ~3~5 Kategorii. Znowu - nie dopisujemy tu wszystkich przedmiotów / znajomości / reputacji / miejsc. Tylko te elementy Otoczenia tu wprowadzamy, które są wyróżnikami postaci w jakimś stopniu.

Przykładowe kombinacje:

* Artur Żupan: KATEGORIA: _Wataha słabych agentów_ ASPEKT: _posłuszni; są dla zysków; w kupie siła_
* Lucjan Kopidół: KATEGORIA: _Znajomość wielu lokalnych firm_ ASPEKT: _luźne kontakty; puls rynku; przepływy biznesowe_
* Karolina Kupiec: KATEGORIA: _Czar Pocałunek Arazille_ ASPEKT: _unieszkodliwienie; arazille; rozkosz; symulowana utopia_

### 5.2.1. Szczegółowe

Tu też mamy do czynienia z Manewrami. **jaka akcja jest wspierana** krzyżujemy z **czym to wsparcie osiąga**.

Ważne jest to, że jeśli mamy do czynienia z bytami niezależnymi (np. agenci), Gracz może deklarować akcje przy użyciu swoich zasobów, bez używania karty postaci głównej. Jest to szczególnie przydatne, jeśli Zasób ma własną kartę (co jest możliwe - karta gangu, czy nazwanego artefaktu, czy nazwanego NPC jest jak najbardziej możliwa).

Tak jak w wypadku Umiejętności, mamy do czynienia z produktem kartezjańskim. Postać ma ~3~5 rzędów, w którym każda z akcji oddzielona średnikiem jest powiązana z "czym".

Przykładowe kombinacje:

* Artur Żupan: CO WSPIERA: _kupienie czasu; odwracanie uwagi; ukrywanie czegoś_ CZYM: _kryjówki miejskie; przez swoją watahę_
* Lucjan Kopidół: CO WSPIERA: _monitorowanie ruchów; zdobycie danych_ CZYM: _wiedza z okolicznych firm; benchmarking; płatne raporty_
* Karolina Kupiec: CO WSPIERA: _obezwładnienie; poszerzenie świadomości; podnoszenie morale_ CZYM: _środki psychoaktywne; Pocałunek Arazille_

## 6. Magia

### 6.1. Co oznacza ta sekcja

Zbiór mocy magicznych i magii pod kontrolą postaci maga. Postacie ludzkie nie posiadają magii dynamicznej. Większość viciniusów też nie posiada magii dynamicznej. Wszyscy magowie posiadają magię dynamiczną ;-).

Zaklęcia statyczne, czy "nazwane" powinny znajdować się w Zasobach.

### 6.2. Jak złożyć z niej postać

### 6.2.1. Ogólne

Przeciętny mag ma 3 Kategorie (nazwane Szkoły Magiczne) a każda Kategoria ma 3-5 Aspektów. Szkół magicznych jest 8, są opisane na osobnej stronie: [Szkoły Magiczne](/rpg/mechanika/magic/szkoly-magiczne-1801.html).

Mag może rzucać czary jedynie ze szkół magicznych, które posiada. Mag może dowolnie mieszać szkoły magiczne i aspekty między nimi (z tych, które posiada).

Przykładowe kombinacje:

* Marek Kromlan: KATEGORIA: _Magia Zmysłów_ ASPEKTY: _odwracanie uwagi; ukrywanie terenu; maskowanie snajpera; wzmocnienie sztuki_
* Karolina Kupiec: KATEGORIA: _Biomancja_ ASPEKTY: _alchemia; hedonistyczne środki psychoaktywne; trucizny i narkotyki; magia lecznicza_
* Lucjan Kopidół: KATEGORIA: _Magia Mentalna_ ASPEKTY: _adaptacja do obcych warunków; wzmocnienie intuicji; wspomaganie umysłu; szersze zbieranie informacji;_

### 6.2.2. Szczegółowe

Manewry. Produkt kartezjański. Jak w wypadku Umiejętności czy Zasobów: **jaką akcję wykonujemy** krzyżujemy z **czym realizujemy**. Różnica jest taka, że w ramach swoich aspektów mag jest w stanie rzucić dowolny sensowny czar. Z uwagi na absurdalną szerokość magii dynamicznej, Manewry ratują Gracza przed paraliżem związanym z nadmiarem możliwości.

Wpiszcie sobie około 3-5 rzędów takich Manewrów. Ułatwi Wam to życie na przyszłość ;-).

Przykładowe kombinacje:

* Marek Kromlan: CO: _zagubienie przeciwnika; przerażanie przeciwnika; niewykrywalność Kromlana_ CZYM: _częste teleportacje; iluzje i maskowania; pułapki i kontrola terenu_
* Karolina Kupiec: CO: _kojenie; deuzależnianie; uspokajanie; hiperimprezowanie_ CZYM: _hedonistyczne środki psychoaktywne; menta-pryzmat_
* Lucjan Kopidół: CO: _przetrwanie w piekle; ustabilizowanie poczytalności; zrozumienie szaleństwa_ CZYM: _adaptacja; wspomaganie zmysłów; wspomaganie umysłu_

## 7. Minimalna Postać potrzebna do gry

### 7.1. Jak złożyć minimalną postać

Z uwagi na Stożek Nieoznaczoności (patrz "Filozofia stojąca za tworzeniem Profilu" w tym dokumencie), postać będzie zmieniała się bardzo często. Podczas budowy pierwszej Opowieści, postać będzie zmieniała się i przekształa się praktycznie cały czas. Nowe aspekty będą się pojawiać, stare będą zanikać. Gracz będzie szukał swojej "idealnej" postaci.

To sprawia, że minimalny Profil by móc rozpocząć grę musi zawierać tylko:

* jedną Motywację z dwoma aspektami
* jedną Umiejętność z dwoma aspektami
* jeden Zasób z dwoma aspektami
* jeśli jest magiem, jedną Szkołę (Kategorię) z dwoma aspektami
* nigdzie ani jednego doprecyzowania / szczegółowości
* Dwu- lub trzyzdaniowy opis tego kim ta postać jest, czemu Gracz chce ją mieć podczas Opowieści... jakiś koncept.

Z powyższego najpewniej około 33% przetrwa przez następnych 5 Opowieści.

* Graczu, po prostu weź coś co wydaje Ci się interesujące i zmieniaj w czasie rzeczywistym podczas budowania Opowieści ;-).
* Mistrzu Gry, pozwól Graczowi przekształcać postać, by Gracz znalazł swój Voice&Tone i idealny koncept.

### 7.2. Jak zachować spójność świata

#### 7.2.1. Zasada filmu

Kojarzycie dowolny film lub serial? Czasami okazuje się, że znany nam bohater w pewnym momencie zrobił coś niespodziewanego i pojawia się efekt "o, nie wiedziałem, że on to potrafi". Jednak najczęściej poczucie spójności świata jest zachowane - to, że _widz_ nie wie, że postać coś potrafi nie oznacza, że z perspektywy świata gry ta postać tego nie potrafiła od samego początku.

Przykład filmowy:

[Youtube: Princess Bride, scena z "I am not left-handed"](https://www.youtube.com/watch?v=rUczpTPATyU)

To sprawia, że jeśli postać dostanie nowy aspekt, czy nową Umiejętność w czasie rozgrywki - świat nadal jest spójny. Po prostu postać wcześniej nie pokazała, że coś potrafi.

#### 7.2.2. Mechaniczna ewolucja postaci

Jednym słowem: Progresja.

Zamiast punktów doświadczenia znanych z innych systemów, w tym systemie zarówno Gracz jak i MG otrzymują punkty Wpływu po skończeniu Opowieści. Wydają te punkty na zmianę rzeczywistości. To może być zarówno przesuwanie NPCów jak i dodanie nowych rzeczy do postaci.

Przykładowo, mieliśmy postać magazyniera na portalisku. W ramach wydawania Wpływu Gracz stwierdził, że chciałby zmienić funkcję postaci na detektywa. Więc po skończeniu Opowieści lokalny mafiozo dał owemu magazynierowi małą agencję detektywistyczną, co dopisaliśmy do Progresji postaci a Gracz sam dopisał Postaci nową Umiejętność.

Z perspektywy świata gry, ów magazynier być może kiedyś był detektywem, ale nie spotkaliśmy go w tej roli wcześniej.

## 8. Jak zbudować Postać?

### 8.1. Co to za sekcja

Ta sekcja służy to ułatwienia Graczowi stworzenia Profilu. Przedstawia grupę przydatnych strategii i pytań diagnostycznych, dzięki którym da się złożyć całkiem grywalną postać.

By móc określić kiedy przestać budować postać, spójrz na sekcję tego dokumentu "Minimalna Postać potrzebna do gry".

### 8.2. Wariant klasyczny "Z opisu w Profil"

Najczęściej spotykany, acz najbardziej czasochłonny wariant. Gracz tworzy opis postaci mający około 4-5 linijek tekstu. Następnie z tego tekstu próbujemy wyekstraktować poszczególne Obszary czy Aspekty i je odpowiednio przyporządkować. Czyli: wpierw przychodzi opis, potem z opisu wypełnia się to, co się da. Technika klasyczna.

### 8.3. Wariant "Chcę grać detektywem"

#### 8.3.1. Co to za wariant

* Wymagania: Gracz posiada mikrokernel idei - jakiś zawód, czy jakąś ogólną koncepcję postaci.
* Działanie: Ta technika pozwala nam wyjść od tego zawodu (tej idei) i stopniowo wypełniać koncepcję.

#### 8.3.2. Pytania generacyjne

Graczu, odpowiedz na poniższe pytania. Niech skrót "TP" oznacza "Twoja Postać".

1. Jakie jest największe dokonanie TP (uznane przez kogoś innego)? Czemu TP była w stanie to zrobić?
2. Z czego w ramach tego największego dokonania TP jest szczególnie dumna? Dlaczego właśnie z tego?
3. W ramach tego dokonania komu i jak TP pomogła najmocniej? Dlaczego nadal ten ktoś utrzymuje kontakt?
4. Z czego TP jest najlepiej znana wśród swoich znajomych? Z czego słynie? Co robi inaczej?
5. Pod jakim kątem TP wybiera zlecenia / pracę? Jakiej nigdy by nie przyjęła? Dlaczego?
6. Jakiego typu umiejętności TP wykorzystuje podczas wykonywania zadań - coś, czego nie robi nikt inny? Czemu to działa?
7. Co przez rywali i zleceniodawców jest uważane za "nieuczciwą" przewagę TP? Czemu to jej pomaga?
8. Kiedy TP powie "O nie, tak się nie stanie". Dlaczego? Jak spróbuje temu zaradzić?
9. Gdzie TP znajduje zlecenia gdzie jej rywale nie szukają? Jaki jest jej unikalny kanał? Czemu tam / jak się poznali?
10. Z kim przestaje TP? Jak się zachowuje w swoim otoczeniu? Jak by działania TP opisali jej towarzysze?
11. W jaki sposób otoczenie TP wsparło jej unikalne umiejętności? Jak wpłynęło na jej zachowanie?
12. W jakich okolicznościach TP jest NAJLEPSZĄ osobą do rozwiązania problemu? Jaki to problem? Czemu TP jest wtedy najlepsza?
13. O czym TP marzy? Czego się boi/co ją boli? Co robi, by jej marzenia się spełniły?
14. Co jest kłopotliwym wydarzeniem z przeszłości TP? Co próbowała zrobić i jej nie wyszło? Czemu? Jak na to zareagowała?
15. Co TP robi w czasie wolnym? Co lubi robić? Jak odpoczywa?

#### 8.3.3. Jak te pytania wykorzystać

1. Główna akcja, z jakiej postać jest dumna najpewniej doprecyzowuje jedną Umiejętność.
2. "Szczególnie dumna" implikuje albo jakiś dominujący Aspekt Umiejętności, albo dotyka Motywacji.
3. Osoba, grupa lub frakcja której pomogła postać powinna trafić do Otoczenia. "Czemu trzyma kontakt" mówi też coś o postaci lub tym elemencie Otoczenia. Aspekt?
4. To dodefiniuje nam albo Otoczenie (znajomi), albo nową Umiejętność, albo nową Motywację albo jakiś aspekt do powyższych.
5. Zdecydowanie Motywacja - jak pojawi się "pieniądze" to już wiemy, że ma Motywację pod tym kątem.
6. Próba zbudowania nowej Umiejętności lub przynajmniej jakichś aspektów. Próba znalezienia jakichś synergii między aspektami / Umiejętnościami.
7. Próba dotknięcia Silnych i Słabych Stron postaci; zahaczenie albo o Aspekty albo o dodatkowe synergie, których jeszcze nie widziemy.
8. Próba zbudowania BÓLU. Połączenia synergicznego Motywacji i Umiejętności. Może Otoczenia.
9. Dodefiniowanie Otoczenia przez znajomości i kontakty.
10. Próba znalezienia METOD (Motywacja) oraz podniesienia Otoczenia o jeszcze jeden krok.
11. Próba zbudowania Umiejętności przez Otoczenie. Może dotknie to też Motywacji?
12. Znowu dotknięcie Silnych/Słabych Stron oraz dotknięcie synergii między Umiejętnościami i Motywacją.
13. Silna próba wywołania MARZEŃ oraz BÓLU (Motywacje) oraz jakiejś Umiejętności / Otoczenia. 
14. Próba zobaczenia kontekstu porażki postaci celem wygenerowania Słabej Strony. Może uda się dotknąć też METOD?
15. Łączymy Motywację z Umiejętnością.

## 9. Filozofia stojąca za budową Profilu

### 9.1. Parametry jakościowe Profilu

1. Nowy Gracz musi być w stanie zacząć grę w ciągu 15 minut od rozpoczęcia budowy Profilu
2. Każdy Gracz/ MG musi być w stanie objąć i prowadzić każdą postać - postać musi promować prawidłowe nią granie
3. Postać, niezależnie od tego kto nią gra, powinna zachowywać się spójnie w świecie gry
4. Mechanicznie, postać powinna być szybka do używania (~30 sekund) i powinna być czytelna (nie-RPGowiec potrafi przewidzieć jej zachowanie)
5. Dwie postacie 'detektyw' czy 'kucharz' powinny się znacząco różnić od siebie podczas tworzenia Opowieści. Postać winna być charakterystyczna.
6. Profil postaci musi być łatwy w zarządzaniu, zwłaszcza podczas tworzenia Opowieści achronologicznie.

### 9.2. Stożek Nieoznaczoności

Najmniej o postaci wiemy na etapie tworzenia. Dopiero uczestniczenie w Opowieściach będą naszą postać kalibrować i przekształcać. Coś takiego nazywa się [Stożkiem Nieoznaczoności](https://en.wikipedia.org/wiki/Cone_of_Uncertainty) i wygląda mniej więcej tak:

![RPGowy Stożek Nieoznaczoności](Materials/180103_profile_build/rpg_cone_of_uncertainty.png)

To znaczy, że najbardziej postać będzie mieć modyfikowaną kartę gdy jest "młoda". Im jest starsza, im więcej przeszła, tym rzadziej się będzie zmieniała (wtedy zmiany będą wynikać z ewolucji postaci - czyli z Progresji).

### 9.3. Zasada zmniejszających się korzyści

Zasada zmniejszających się korzyści - czyli implementacja Zasady Pareto na budowanie Profili postaci - mówi mniej więcej "80% korzyści z budowania Profilu pochodzi z 20% czasu na to poświęconego". Jako, że zgodnie ze Stożkiem Nieoznaczoności postać _i tak_ będzie się drastycznie zmieniać podczas tworzenia Opowieści to i tak silnie skupiamy się na Profilach postaci minimalnych potrzebnych do rozpoczęcia gry.

Marnowaniem czasu byłoby tworzenie postaci przez półtorej godziny tylko po to, by zrobić przy jej użyciu jedną Opowieść.

Dlatego lepiej jest doprowadzić wszystkie obszary postaci (Motywacja, Umiejętności, Silne i Słabe Strony, Otoczenie...) do 1-2 sekcji posiadającej 2-3 aspekty każda w ciągu 15-20 minut niż skupiać się na zbudowaniu postaci perfekcyjnej i w 100% skończonej... która zmieni się drastycznie po pierwszej Opowieści.

### 9.4. Znaczenie Otoczenia / Zasobów

Zgodnie z [analizą SWOT](https://en.wikipedia.org/wiki/SWOT_analysis) podczas pracy nad organizacją / bytem trzeba zwrócić uwagę nie tylko na sam byt (tu: postać) a też na otoczenie postaci. Innymi słowy, duża część sił i słabości postaci może znajdować się nie w samej postaci a w jej otoczeniu.

Batman czy IronMan są właśnie definiowani w większości przez swoje Otoczenie (ma sprzęt i bogactwo). Nawet Superman nie miałby informacji (leadów) o problemach gdyby nie Otoczenie jego alter-ego, Clarka Kenta, który pracuje jako reporter.

To sprawia, że Otoczenie postaci bardzo silnie ową postać definiuje. Dlatego musi być tak silne mechanicznie - bez otoczenia postać jest po prostu nie dość zdefiniowana.

### 9.5. Znaczenie Motywacji

#### 9.5.1. Problem dryfu postaci

Poważnym problemem podczas grania przez dłuższy okres czasu jest dryf postaci. Podczas pierwszej Opowieści wszyscy świetnie pamiętają, jak ta postać się zachowuje. Jednak jak minie miesiąc czy rok - zwłaszcza podczas małej ilości Opowieści (np. 1/miesiąc) - sam Gracz zapomina jak jego postać winna się zachowywać.

Postać zaczyna zachowywać się jak Gracz. Bo Gracz zwyczajnie nie pamięta czym gra.

#### 9.5.2. Problem sprzeczności mechaniki z fikcją

Czasem postać ma mechaniczne bonusy do czegoś, co powinno być sprzeczne z konceptem postaci / trudne dla postaci. Wyobraźmy sobie taką sztampową scenę:

Paladyn może podczas przesłuchania torturować swojego przeciwnika... lub przekonać go, by ów współpracował. Rozpatrzmy to typowo mechanicznie:

* Paladyn:
    * torturowanie: 1
    * przekonywanie: 5
* Przeciwnik
    * odporny na ból: 3
    * odporny na przekonywanie: 10
* Potencjalny konflikt:
    * Paladyn torturuje: 1 -  3 = (-2)
    * Paladyn negocjuje: 5 - 10 = (-5)

Jak widać, z perspektywy czysto logicznej i taktycznej, paladyn powinien torturować swojego przeciwnika. Gracz **chce** wygrać konflikt. Ale jego postać - paladyn - **nie chce** torturować. Innymi słowy, matematyka systemu RPG zachęca Gracza do zrobienia czegoś niezgodnego z postacią. **Innymi słowy**: mechanika nie wspiera fikcji.

A podstawowym założeniem tej mechaniki jest to, że chcemy wynagradzać _promowane_ zachowania. Czyli powyższa sytuacja jest błędem mechaniki.

#### 9.5.3. Rozwiązanie obu problemów

Wprowadzenie _Motywacji_ jako bonusu do wykonywanej akcji. Niech Motywacja także zmienia matematykę sesji, niech sprawia, że to co jest właściwe _z perspektywy postaci_ jest tym, co będzie wykonywane. Dla naszego paladyna łatwiej jest przekonywać niż torturować. Więc:

* Paladyn:
    * torturowanie: 1
    * przekonywanie: 5
    * motywacja: "czynić to co należy": 2
* Przeciwnik
    * odporny na ból: 3
    * odporny na przekonywanie: 10
* Potencjalny konflikt:
    * Paladyn torturuje: 1 -  3 - 2 = (-4)
    * Paladyn negocjuje: 5 - 10 + 2 = (-3)

Teraz sytuacja jest prawidłowa. Naszemu paladynowi bardziej opłaca się zrobić to, co _dla paladyna_ jest łatwiejsze i bardziej naturalne. Nie trzeba pamiętać, jak postać ma się zachować - wynika to samo z matematyki mechaniki.

Zauważcie, że gdyby nasz gracz grał jakimś czarnym rycerzem czy antypaladynem, to taka postać zachowa się inaczej:

* Czarny Paladyn:
    * torturowanie: 1
    * przekonywanie: 5
    * motywacja: "maksymalizować cierpienie": 2
* Przeciwnik
    * odporny na ból: 3
    * odporny na przekonywanie: 10
* Potencjalny konflikt:
    * Paladyn torturuje: 1 -  3 + 2 = ( 0)
    * Paladyn negocjuje: 5 - 10 - 2 = (-7)

I nagle ów nieszczęsny "Munchkin" czy "Powergamer" gra prawidłowo swoją postacią. Paladyn zachowuje się jak paladyn, czarny paladyn jak czarny paladyn. Gracz nie musi pamiętać jak powinna zachowywać się postać. Mechanika wspiera a nie przeszkadza.

I dlatego Motywacja jest ważna ;-).

### 9.6. Czemu dzielimy postać na Obszary, Kategorie i Aspekty

#### 9.6.1. Ogólny opis zagadnienia

Ogólna struktura Profilu wygląda tak:

* Obszar (Umiejętność)
    * Kategoria (Listonosz)
        * Aspekt_1, Aspekt_2... ("znajomość okolicy", "wyciąganie plotek")

#### 9.6.2. Powód: Od ogółu do szczegółu (ogólny podział)

Gdy próbujemy postać zrobić, zapoznać się z nią, przypomnieć ją sobie lub po prostu ją zrozumieć, musimy mieć "coś" czego możemy się złapać. Obszary dają taką możliwość.

Spójrzmy na przykładową postać; same motywacje i umiejętności (ogólne):

**Karolina Kupiec:**

* Motywacje
    * Indywidualne : postęp; pokój
    * Społeczne : radość; autonomia; altruizm
    * Wartości : przyjemność; radość; pacyfista
* Umiejętności
    * Terminus: rewolwerowiec; pułapki
    * Biochemik: narkotyki; herbalista; naukowiec
    * Dyplomata: bard; rozładowanie konfliktów

Mniej więcej widać kim Karolina jest i w jaki sposób się zachowa w różnych sytuacjach. Jest _wystarczająco_ zdefiniowana z perspektywy prowadzenia poszczególnej Opowieści. Pięć minut i można zaczynać tworzyć z nią Opowieść.

#### 9.6.3. Po co nam w ogóle aspekty?

Wyobraźmy sobie dwie postacie, mające następujące Motywacje i Umiejętności (uproszczone):

Ania:

* Motywacja
    * MARZENIE: Żeby terminusi nie byli już potrzebni
    * METODY: Odstraszająca przewaga
* Umiejętności:
    * Terminus antypotworowy
    * Medyk polowy

Basia:

* Motywacja
    * MARZENIE: Żeby terminusi nie byli już potrzebni
    * METODY: Odstraszająca przewaga
* Umiejętności:
    * Terminus antypotworowy
    * Medyk polowy

Te postacie są identyczne, zarówno w Konfliktach jak i w odgrywaniu. Nie rozumiemy między nimi różnic. A teraz dodajmy im Aspekty:

Ania:

* Motywacja
    * MARZENIE: Żeby terminusi nie byli już potrzebni
        * Aspekty: niewinni nie mogą ucierpieć, leczenie nad niszczenie
    * METODY: Odstraszająca przewaga
        * Aspekty: efektowność przez przygotowanie, inspirowanie własnym przykładem
* Umiejętności:
    * Terminus antypotworowy
        * Aspekty: specjalistka od efemeryd, unieszkodliwianie a nie zabijanie
    * Medyk polowy
        * Aspekty: stabilizacja i staza, wzmacnianie artefaktów leczniczych

Basia:

* Motywacja
    * MARZENIE: Żeby terminusi nie byli już potrzebni
        * Aspekty: cel uświęca środki, promuje działania długoterminowe
    * METODY: Odstraszająca przewaga
        * Aspekty: efektowność przez siłę ognia, dowodzenie przez zastraszanie
* Umiejętności:
    * Terminus antypotworowy
        * Aspekty: walka w mieście, sprzątanie śladów
    * Medyk polowy
        * Aspekty: kojenie cierpienia, rozrywanie opętań

Widzicie, co się stało?

**Ania** jest osobą, która raczej unieszkodliwia przeciwników i skupia się na tym, by dostarczyć ciężko rannych do innego lekarza. Ważniejsze dla niej jest to, by niewinnym nic się nie stało niż na niszczeniu potworów. Raczej inspiruje niż rozkazuje. Jest to osoba wyglądająca na bardziej metodyczną - jest to jednostka wsparcia.

**Basia** natomiast jest osobą działającą na pierwszej linii ognia. Nie będzie mieć problemów z poświęceniem niewinnych czy cywilów, jak długo świat będzie lepszy. Ania raczej nie powie "ich się nie da uratować", Basia nie będzie miała takich oporów. Basia jest zdecydowanie terminusem miejskim i w miastach czuje się najlepiej.

Zauważcie, że obie te postacie mogą mieć identyczne Obszary, ale są zupełnie różne.

#### 9.6.4. Po co nam doprecyzowania szczegółowe

TODO. W skrócie: do premiowania stałych fragmentów gry, klasycznych ruchów, manewrów będących "wow" factor postaci. By po pół roku Gracz wrócił i wiedział jak grać. Dodatkowo, by każdy Gracz obejmujący postać wiedział, jakie są główne manewry. Jak grać, czego używać.

No i dla powergamingu, oczywiście.
