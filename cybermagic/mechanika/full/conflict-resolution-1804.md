---
layout: mechanika
title: "Rozwiązywanie Konfliktów, 1804"
---

# {{ page.title }}

## 1. Aktualność dokumentu

* Dokument jest aktualny wobec mechaniki 1804; działa z kartami postaci 1803
* Dokument jest wyprodukowany: 180407
* Ostatnia aktualizacja: 180422

## 2. Flowchart, wysokopoziomowo

![Flowchart mechaniki CR, wysokopoziomowo](Materials/180499_all/180407_cr_flowchart.png)

## 3. Moja pierwsza sesja RPG

### 3.1. Modyfikacja zasad

Przede wszystkim, gratulacje. To powinien być dobry wybór ;-).

Proponujemy wykorzystać nieco uproszczone zasady:

* Uproszczona Karta Postaci - nie wypełniajcie Szczególności ani Manewrów. Same wysokopoziomowe aspekty i kategorie.
* Mechanicznie, traktujcie manewry jako "zawsze udane" (dawajcie Żetony Sukcesu jakby Manewry i Szczególności wchodziły).
* Większość testów na poziomie Łatwym i Typowym
* Nie próbujcie określać Przewag Sceny. Wykorzystajcie Scenę bardzo pobieżnie; 0-2 Żetony w Sukces czy w Porażkę.
* Mistrzu Gry, ZAWSZE w sytuacjach konfliktowych (czy Żeton Sukcesu ze sceny wchodzi, czy nie; czy Motywacja wchodzi, czy nie...) pozwól Graczowi dodać sobie żeton. Gra przyspieszy.

### 3.2. Wyjaśnienie przyczyn uproszczenia

1. Mechanika przyspieszy. To jest korzystne, bo na początku mechanika może wyglądać na trudną.
2. Postacie graczy będą trochę silniejsze a Przeciwnicy będą słabsi, więc Graczom będzie trochę łatwiej.
3. Mniej problemów z wyczuciem uznaniowości

Ogólny 'feel' gry będzie działał tak samo. Różnica w tym, że postaciom jest łatwiej i nieco mniej dokładnie muszą być odgrywane.

## 4. Wyjaśnienie poszczególnych kroków mechaniki

### 4.1. Określenie sceny i stron

_Renata Reporterka jest schowana w jednym z magazynów i rozpaczliwie próbuje włamać się do samochodu ZANIM strażnicy ją znajdą. Jest ich pięciu i rozeszli się po terenie. Renata jest pod presją czasu, ale jest dość niewidoczna. Czy Renacie uda się włamać do samochodu (by uciec) ZANIM zostanie znaleziona i złapana?_

Powyższy krótki opis to Scena. Daje nam pewne informacje o sytuacji - Renata jest ukryta, ale strażnicy wiedzą o jej obecności. Prędzej czy później ją znajdą. Renata fundamentalnie chce się wydostać z magazynu i odjechać niezauważona samochodem.

* Wola Gracza: "Renata odjechała samochodem w siną dal; strażnicy nie wiedzą, kto włamał się do magazynu."
* Wola MG: "Renata została złapana na gorącym uczynku. Nie tylko strażnicy mają na nią infiltrację - mają też włamanie."

Wyobrażacie sobie tą scenę? Jest ciemno. Pada lekki deszcz. Słychać okrzyki strażników a dzielna reporterka kuli się za samochodem i próbuje agrafką otworzyć klamką szepcząc cicho "no dalej... dawaj...". Aha, a oprócz tego samochód jest w magazynie. Nie pytajcie czemu. Nie wiem.

### 4.2. Ustalenie stopnia trudności

W skali: _Bezproblemowa - Łatwa - Typowa - Trudna - Heroiczna_, gdzie umiejscowilibyście trudność zadania Renaty? Ja powiedziałbym, że jest to działanie Trudne. Renata ma niewielkie szanse na to, że jej się uda bez żadnych komplikacji; najpewniej Coś Pójdzie Nie Tak.

Stopnie Trudności są mniej więcej skalibrowane w następujący sposób:

Dla 4-Żetonowej postaci:

| Stopień Trudności | Sukces | Skonfliktowany Sukces | Porażka |
|-------------------|--------|-----------------------|---------|
| Bezproblemowy     |  80%   |      20%              |   0%    |
| Łatwy             |  50%   |      30%              |  20%    |
| Typowy            |  30%   |      40%              |  30%    |
| Trudny            |  15%   |      30%              |  55%    |
| Heroiczny         |   0%   |      20%              |  80%    |

Dla 8-Żetonowej postaci:

| Stopień Trudności | Sukces | Skonfliktowany Sukces | Porażka |
|-------------------|--------|-----------------------|---------|
| Bezproblemowy     |  95%   |       5%              |   0%    |
| Łatwy             |  70%   |      20%              |  10%    |
| Typowy            |  55%   |      30%              |  15%    |
| Trudny            |  40%   |      35%              |  25%    |
| Heroiczny         |  25%   |      35%              |  40%    |

Tak więc, Renata w powyższej sytuacji ma niewielkie szanse na powodzenie bez problemów.

### 4.3. Zbudowanie puli żetonów

#### 4.3.1. Ogólnie

Do rozwiązania Konfliktu potrzebujemy puli żetonów składająca się z Sukcesów, Remisów i Porażek. Sukcesy to żetony korzystne dla Gracza, Remisy są neutralne a Porażki są niekorzystne dla Gracza.

![Budowa puli żetonów - diagram rysunku odzwierciedlającego punkty](Materials/180499_all/180408_cr_pool_influences.png)

#### 4.3.2. Żetony Sukcesu z Karty Postaci

Idziemy od góry do dołu. Motywacje? Szczególności? Umiejętności? Manewry? Każda rzecz idąc z góry na dół która pasuje dodaje jeden żeton Sukcesu. To nasza pierwsza kupka.

_Renata ma Motywację "pozostać w cieniu" i Szczególność "dąż do nie ujawniania się za wszelką cenę". Daje to dwa żetony Sukcesu. Umiejętności - "włamywacz" oraz Manewr "ukrywanie się w mieście" także wchodzą - kolejne dwa żetony Sukcesu. Niestety, nic więcej nie pasuje. Z bólem, Gracz Renaty wydaje 1 kartę by uruchomić Zasób i zdobyć agrafkę. Renata ma więc 5 Żetonów Sukcesu._

#### 4.3.3. Żetony pochodzące ze Sceny

Spójrzmy na samą Scenę jak opisana na początku. Jest ciemno, ale strażników jest sporo. Renata ma trochę czasu, pada lekki deszcz... Co z tego możemy wykorzystać do samego Konfliktu?

Każde z poniższych może dodać +1 lub +2 do jednej ze stron. Na bazie tego dodajemy Żetony Sukcesu lub Porażki (to oznacza, że _statystycznie opłaca się niwelować negatywne sytuacje Sceny bo da to "lepsze" prawdopodobieństwa - to premiuje podejście taktyczne_ ):

* **Sytuacja:** w ramach konfliktu, która strona ma lepszą Sytuację? Która działa z zaskoczenia? Pogoda?
* **Sprzęt:** kto ma lepszy sprzęt? Np. włócznik kontra nożownik ma przewagę na otwartym terenie, niedowagę w podziemiach.
* **Skala:** kogo jest więcej? Jak bardzo to znaczące w tym konflikcie?
* **Otoczenie:** czy jest coś w otoczeniu co może pomóc? Aspekty miejsca?
* **Magia / Pryzmat:** szczególnie gdy używana jest magia; czy Pryzmat jest korzystny? Niekorzystny? Czy energia sprzyja postaci? Zwalcza ją?

_Renata ma lepszą Sytuację, choć nieznacznie. Jest schowana, i jakkolwiek czas nie gra na jej korzyść, to nie zgodnie z opinią Gracza i MG Sytuacja jest dla niej korzystna. +1. Za to jest w niedowadze Skali. Ich jest po prostu więcej. -1. Sprzęt nie ma znaczenia w tym konflikcie. Ale Otoczenie: magazyn jest Zagracony. To aspekt dający Renacie +1. Tak więc, Renata dostaje +1 Żeton Sukcesu. Gdyby skończyła na niedowadze, dostałaby +1 Żeton Porażki._

#### 4.3.4. Podstawa ze Stopnia Trudności

Każdy z tych Stopni Trudności ma przyporządkowaną odpowiednią liczbę żetonów, które należy dodać (lub na wyższym poziomie odjąć):

| Stopień Trudności | Ż. Sukcesu | Ż. Remisu | Ż. Porażki |
|-------------------|------------|-----------|------------|
| Bezproblemowy     |       3    |       2   |   2        |
| Łatwy             |       2    |       3   |   4        |
| Typowy            |       0    |       2   |   5        |
| Trudny            |    **-2**  |       3   |   6        |
| Heroiczny         |    **-3**  |       4   |   9        |

_W naszej sytuacji Renata ma 6 Żetonów Sukcesu. Trudny test wymaga odjęcia 2. To daje nam początkową pulę konfliktu: 4 Żetony Sukcesu, 3 Żetony Remisu, 6 Żetonów Porażki_

#### 4.3.5. Redukcja Porażek wynikająca z siły postaci

Za każde 4 Żetony Sukcesu **przed uwzględnieniem Podstawy** usuń 1 Żeton Porażki. Czyli jeśli Karta Postaci + Scena dały '8', skasuj 2 Żetony Porażki z puli konfliktu. Jeśli to jest test Trudny i startowa pula wynosi 6 Żetonów Sukcesu, nadal skasuj 2 Żetony Porażki - Karta Postaci + Scena dają 8, niezależnie od redukcji z Podstawy.

_Renata ma 6 Żetonów Sukcesu. To sprawia, że ma prawo odliczyć sobie 1 Żeton Porażki, z czego skwapliwie korzysta. Daje jej to startową pulę 4 Żetonów Sukcesu, 3 Żetonów Remisu i 5 Żetonów Porażki. Nadal niefajnie, ale już lepiej niż na początku._

### 4.4. Wykonanie Testu

Po umieszczeniu wszystkich żetonów w woreczku, Gracz losuje DWA żetony. Interpretacja wygląda następująco (kolejność wyciągnięcia żetonów nie ma znaczenia):

![Tabelka poniżej wyjaśni](Materials/180499_all/180408_cr_token_transition.png)

| Żeton   | Żeton   | Wynik                  |
|---------|---------|------------------------|
| Sukces  | Sukces  | Sukces                 |
| Sukces  | Remis   | Sukces                 |
| Sukces  | Porażka | Skonfliktowany Sukces  |
| Remis   | Remis   | Skonfliktowany Sukces  |
| Porażka | Remis   | Porażka                |
| Porażka | Porażka | Porażka                |

_Gracz Renaty wrzucił 4 Żetony Sukcesu, 3 Żetony Remisu i 5 Żetonów Porażki do woreczka, wymieszał i wyjął dwa Żetony. Wyjął Remis oraz Porażkę. To oznacza, że konflikt zakończył się Porażką - zwyciężyła interpretacja Mistrza Gry. Renata zostanie złapana zanim włamie się do samochodu._

Ważna rzecz:

* Za wykonanie akcji (niezależnie od wyniku), Mistrz Gry dostaje 1 Punkt Wpływu.
* Za Skonfliktowany Sukces, Gracz dostaje 1 Punkt Wpływu
* Za Porażkę, Gracz dostaje 2 Punkty Wpływu

### 4.5. Przelosowanie

Jeżeli Gracz nie akceptuje wyniku Konfliktu, ma możliwość przelosowania (losowania jeszcze raz). Polega to na tym, że:

* Gracz wrzuca jeden Żeton z wylosowanych do woreczka (wybiera który, więc najpewniej skasuje Żeton Porażki)
* Mistrz Gry dostaje Punkt Wpływu oraz 1 Punkt Problemu (komplikacja fabularna)
* Gracz losuje jeszcze raz

Gracz może przelosowywać nieskończenie wiele razy. To znaczy, że jeśli Graczowi na czymś zależy to **zawsze** wygra.

_Gracz Renaty nie akceptuje tego, że Renata jest złapana. Nie pasuje mu do opowieści. Zamiast tego decyduje się przelosować. Wyrzuca wyciągnięty żeton Porażki i zwraca do woreczka żeton Remisu (przez co w woreczku znajdują się teraz: 4 Żetony Sukcesu, 3 Żetony Remisu, 4 Żetony Porażki). Mistrz Gry dostaje +1 punkt Wpływu i +1 punkt Problemu; gracz nie dostaje nic, bo przelosował swoją Porażkę. Gracz Renaty ciągnie ponownie i wyciągnął Sukces oraz Remis. Pełen Sukces._

### 4.6. Koniec Konfliktu

Osoba, która wygrała opisuje, jak zakończył się konflikt i jaki był wynik. Koniec Sceny.

Wtedy pododajemy Punkty Wpływu i inne takie.

_Gracz Renaty wygrał. Opisuje, jak Renacie udało się dostać do samochodu i w ostatniej chwili odpalić maszynę, by uciec zanim strażnicy zorientowali się, kto to był. Mistrz Gry wydał swój Punkt Problemu na to, że Renata zgubiła coś, co może ich doprowadzić do kolejnego celu Renaty. Innymi słowy, w następnej akcji Renaty przeciwników może być więcej i będą przygotowani. Gracz Renaty westchnął - i tak nie ma łatwo..._

### 5. Co może zrobić Gracz?

#### 5.1. Ogólnie

![Transformacja Akcji w działania](Materials/180499_all/180408_cr_actions.png)

* Gracz musi zapłacić 1 Akcję, by móc w ogóle rozpocząć Konflikt.
* By wprowadzić do gry Zasób, musi zapłacić 1 Akcję.
* By wprowadzić do gry Magię, musi zapłacić 1 Akcję.
* By pomóc innej postaci, musi zapłacić 1 Akcję.

To oznacza, że wykonanie pojedynczego Konfliktu kosztuje 1-3 Akcje.

Zasób i Magia mogą nie tylko dodać Żeton Sukcesu. Mogą w pewnych okolicznościach usunąć całkowicie Żeton Porażki lub zmienić zakres konfliktu.

#### 5.2. Deklaracja Konfliktu

* By móc uczestniczyć w konflikcie, Gracz musi wykorzystać jedną Akcję.
    * Gracz zaczyna konflikt z innym Graczem lub MG.
    * Gracz mówi "nie" na opis wydarzenia deklarowanego przez MG czy innego Gracza.
    * Gracz chce pomóc innemu Graczowi w akcji.
* Konflikt jest zakończony wtedy, gdy któraś ze stron przegrywa coś znaczącego.
* Przeciwnicy posiadający Trwałość tracą 1 punkt Trwałości za każdym razem, gdy przegrywają konflikt.

#### 5.3. Wykorzystanie Zasobów

* Gracz może wykorzystać tylko jedną Akcję na wykorzystanie Zasobów podczas konfliktu.
* Rzeczy zdobyte wcześniej przez Gracza **w wyniku poprzednich konfliktów** (np. wiedza o słabości przeciwnika) **nie** są Zasobami. Należą do Modyfikatorów Sytuacyjnych Sceny i są dla postaci zawsze dostępne.
* Rzeczy uzyskane przez Progresję muszą być wykorzystane przez Akcję.
* Przez wykorzystanie Otoczenia podczas konfliktu Gracz może powołać dowolny Aspekt ze swojego Otoczenia, jeśli potrafi wyjaśnić jak ten Aspekt rozwiązuje jego problem.

Przykład:

_Renata Reporterka ucieka strażnikom w tym "nowym miejscu". Oskrzydlili ją i zapędzili do ciemnej uliczki. Gracz wykorzystuje 'szybki motor' znajdujący się w Zasobach postaci. Opisuje jak Renata ucieka do pozornie ślepej uliczki i wyciąga spod szmat swój motor, który przygotowała na taką okazję (co właśnie teraz pojawiło się w grze). Dostaje +1 Żeton Sukcesu za motor oraz obniżyła Stopień Trudności ucieczki z Trudnego na Typowy. Całkiem nieźle jak na jedną akcję._

#### 5.4. Wykorzystanie Magii

* Gracz może wykorzystać tylko jedną Akcję na wykorzystanie Magii podczas konfliktu.
* Przez wykorzystanie Magii podczas konfliktu Gracz może stworzyć **dowolne** zaklęcie, na które pozwalają mu Aspekty jego szkół magicznych. To zaklęcie może stworzyć **dowolny Aspekt** dodany do tej sytuacji.

Magia jest prostsza do użycia dla Gracza niż Otoczenie i jest nie mniej potężna. Jako, że Otoczenie i Magia synergizują, można spodziewać się naprawdę dużej mocy gdy mamy do czynienia z magiem.

#### 5.5. Pomoc innej postaci

Gracz jest w stanie pomóc innej postaci wykonać jej konflikt. By to zrobić, musi wydać swoją Akcję. Za to może użyć dowolnego aspektu ze swoich Umiejętności czy Zasobów (jeśli ma to sens z perspektywy świata fikcji) i dołożyć ten aspekt do konfliktu.

Cenniejsze jest jednak to, że Gracz jest w stanie zniwelować jakąś trudność Sceny czy Przeciwnika. Matematycznie bardziej opłaca się redukować Porażki niż dodawać Sukcesy; jeśli jest taka możliwość, warto to zrobić.

Czasem nie warto jest pomagać innej postaci. Czasem warto jest podzielić się z nią obowiązkami, by móc obniżyć Stopień Trudności testu. To jest wariant preferowany.

Przykład 1: nie wykorzystanie wzmocnienia:

_Kasia próbuje uratować przerażone koty z płonącego domu. Karol chce pomóc Kasi. Koty są przerażone i mogą uciec do piwnicy (co będzie końcem gry) a Kasia i Karol koniecznie muszą koty ewakuować przez drzwi. Zarówno Karol jak i Kasia mają tarcze magiczne, ale koty nie. Test jest ustalony jako Trudny (tarcze magiczne sprawiają, że Kasia i Karol nie muszą martwić się o swoje bezpieczeństwo)._

_Kasia i Karol się podzielili. Kasia odetnie kotom drogę do piwnicy i zapędzi je w kierunku na Karola, a Karol rzuci zaklęcie usypiania na koty. W ten sposób będą w stanie wynieść nieprzytomne koty pod tarczami magicznymi i zwierzakom nie stanie się nic złego. Trudny test przekształcił się w dwa testy: Typowy (zapędzenie przerażonych kotów w kierunku dla nich bezpiecznym gdzie jest Karol) i drugi Typowy (rzucić czar stożkowo na obszar wtedy, gdy będą biegły dwa koty)._

_Co ważniejsze, udało się zniwelować wszystkie negatywne aspekty Sceny._

Przykład 2: pomoc przez redukcję Porażek.

_Ignat ma problem. Stanął naprzeciw gangowi klasycznych obijmord. Będą się bić; sęk w tym, że przeciwnicy mają zdecydowaną przewagę Skali (jest około 10 wrogów, więc to będą co najmniej 2 Żetony Porażki). Widząc to, Karolina zdecydowała się wejść do akcji. Czarodziejka zdecydowała się przekierować ogień na siebie, usuwając wszystkie Żetony Porażki wynikające ze Skali. Ignatowi zostaje pokonać tylko szefa obijmord. Musi się jednak spieszyć, bo czarodziejka czy nie - Karolina nie utrzyma 9 przeciwników na sobie długo._

### 5. Rady odnośnie poszczególnych elementów

#### 5.1. Ogólna zasada przydzielania żetonów

Ogólna zasada: idziemy w **prędkość mechaniki** a nie **precyzję mechaniki**, za wyjątkiem najważniejszych konfliktów. Przeglądamy pobieżnie Scenę, Kartę itp.

Nie ma co się kłócić o pojedynczy parametr; matematyka całego systemu jest tak skonstruowana, że nie ma to aż tak dużego znaczenia. Lepiej, by wszyscy więcej kombinowali nad następnymi ruchami niż nad dyskutowaniem czy śliska podłoga to 0 czy -1.

#### 5.2. Określenie Sceny

Przed rozpoczęciem konfliktu spróbujcie dojść do tego co się dzieje. Kto jest obecny, jakie jest ułożenie postaci, gdzie znajdują się poszczególne obiekty. Jakie w ogóle są obiekty. Nie traktujcie wyniku tej rozmowy jako rzecz ostateczną - jeśli Gracz musi coś dodać do Sceny co ma sens (np. żyrandol w zabytkowym domu), pozwólcie mu.

Określenie sceny ma dać wysokopoziomowy kontekst, nie zniewolić Was na wieczność.

#### 5.3. Określenie Stopnia Trudności

##### 5.3.1. Sytuacja normalna

To jest najtrudniejszy fragment ustalania konfliktu, bo jest całkowicie uznaniowy. Spróbujmy ustalić kilka heurystyk:

* Łatwe testy to około 10% testów.
* Typowe testy to około 60% testów.
* Trudne testy to około 30% testów.
* Heroiczne testy... jeden na Opowieść?

Oczywiście, powyżej mamy tylko przewidywany rozkład - wszystko zależy od deklaracji Graczy. Spróbujmy inaczej:

* Test łatwy to coś, o czym nawet byśmy nie wspomnieli w dyskusji.
* Test typowy to coś, co powiesz w "jak Ci minął dzień"
* Test trudny to coś, czym się pochwalisz. Coś, z czego można być zadowolonym.
* Test heroiczny to coś, z czego jesteś dumny przez rok.

Spróbujmy więc przejść przez kilka przykładów by sprawdzić, jak powyższe działa:

_Renata ucieka na motorze przed goniącymi ją na piechotę strażnikami. Zna miasto, oni też; jest ich więcej. Ma lekkie wyprzedzenie. Chce nie tylko uciec, ale też sprawić, by nie wiedzieli kim ona jest_ - Normalnie bym nie testował, ale "by nie wiedzieli kim ona jest". Czyli test Łatwy, by nie rozpoznali motoru czy tablicy rejestracyjnej. Jak trzeba, zażądam Akcji za użycie Zasobu (zamaskowanie blach motoru).

_Elf z łukiem próbuje ustrzelić szybko regenerującego się trolla_ - test Heroiczny. Łuk jest fatalną bronią do pokonania istotę szybko się regenerującą. Jednak jeśli elf podpali strzały, osłabię trudność do testu Typowego.

_Anna chce wyróżnić się suknią i zachowaniem na balu charytatywnym, by zwrócić na siebie uwagę pewnego młodego biznesmena_ - test Trudny; jest tam wiele dam o podobnych celach. Oczywiście, jeśli po drodze uzyskała informacje na temat jego zachowań, upodobań itp., zredukuję stopień trudności do Typowego.

_Anna chce wyróżnić się suknią i zachowaniem na balu charytatywnym, by zwrócić na siebie uwagę pewnego młodego biznesmena **w taki sposób, by nikt nie zauważył, że na niego poluje i nikt nie miał jej tego za złe**_ - test Trudny i to możliwy tylko wtedy, jeśli ma informacje na temat jego zachowań, upodobań itp. Najpewniej zażądam więcej researchu na temat innych osób obecnych na tym balu. W innym wypadku, test Heroiczny.

##### 5.3.2. Gracz kontra Gracz (PvP)

Sytuacja rzadka, acz możliwa do przeprowadzenia. Jako podstawę stopnia trudności weź poziom symetryczny, ale **bez jednego żetonu sukcesu**. Ten system nie promuje gry PvP, dlatego broniący powinien mieć lekką przewagę.

| Stopień Trudności  | Ż. Sukcesu | Ż. Remisu | Ż. Porażki |
|--------------------|------------|-----------|------------|
| Gracz kontra Gracz |       1    |       3   |   2        |

PvP wiąże się z następującymi zasadami:

* Atakujący wyciąga dwa żetony; broniący nie wyciąga żetonów.
* Atakujący dorzuca ze swojej karty i Sceny żetony sukcesu.
* Broniący dorzuca ze swojej karty i Sceny żetony porażki.
* Zarówno Atakujący jak i Broniący oddają po karcie energii MG.
* Normalnie można używać Magii i Zasobów, przy normalnych zasadach kart energii.
* Każdy konflikt tego typu może odbywać się na zasadach Eskalacji, z Licznikami.
* Nie ma możliwości przerzutów, ale jest Eskalacja.

#### 5.4. Wykonanie Testu i Przerzuty

Pozwól Graczom przerzucać. Ciesz się z tego. Zbieraj Żetony Problemów lub strzelaj im komplikacje na lewo i prawo. To, że Gracz ma wygrać to na czym mu zależy nie oznacza, że MG nie ma niczego wygrać.

#### 5.5. A co jeśli walczymy z inną Kartą Postaci?

* Określ Stopień Trudności z perspektywy Gracza. Najpewniej (80%) będzie Typowy.
* Policz Żetony Karty Postaci Gracza (ŻKPG). Policz Żetony Karty Postaci Przeciwnika (ŻKPP).
* ŻKPG - ŻKPP: nadwyżka to żetony Sukcesu. Niedowaga to żetony Porażki.

#### 5.6. Skala, Sytuacja, Sprzęt

Rozpatrz trzy podstawowe pytania _w kontekście tego konfliktu_:

* **Skala**:
    * Kogo jest "więcej"? (2 zabijaków kontra postać to przewaga Skali)
    * Kto ma "większą skalę"? (czołg kontra człowiek to przewaga Skali)
* **Sprzęt**:
    * Kto ma lepszy sprzęt? (ktoś z mieczem na otwartej przestrzeni kontra ktoś ze sztyletem to przewaga Sprzętu)
    * Ogólnie, Przedmioty / Wiedza / Sprzęt
* **Sytuacja**:
    * Kto jest zaskoczony?
    * Kto jest pod presją czasu?
    * Ogólnie: Czas / Taktyka / Teren

To tak w uproszczeniu.

#### 5.7. Jak wyczuć co kontruje co?

Kontekst. Doświadczenie. Innymi słowy, trzeba zrobić sporo pomyłek zanim się tego nauczysz ;-).

Czasami możesz mieć sytuację, w której aspekt kontruje kilka aspektów, ale sam jest skontrowany. I co wtedy? Przykład:

* Ania ma 'tarczę ognistą'.
* Gniazdo Szerszeni ma 'sporo tam szerszeni', 'są naprawdę wściekłe', 'stałe pole antymagiczne'

Jeśli 'pole antymagiczne' i 'tarcza ognista' się zniwelują, to latające tam szerszenie powinny móc Anię pociąć (nie ma osłony tarczy ognistej).

Ale:

* Ania ma 'szerokokątny ognisty stożek'
* Gniazdo Szerszeni ma 'sporo tam szerszeni', 'są naprawdę wściekłe', 'stałe pole antymagiczne'

Nawet, jeśli 'stałe pole antymagiczne' zniweluje 'ognisty stożek', to szerszenie najpewniej już zostały spalone.

Jak zatem ja rozwiązuję te problemy?

**Na korzyść Gracza.**

1. Jeśli mam wątpliwości, czy aspekt coś kontruje czy nie, zakładam, że nie ;-).
2. Jeśli argumentacja Gracza ma sens, pozwalam mu skreślić aspekty po swojemu.
3. Pamiętam, że będą inne konflikty ;-).

Zalety takiego rozwiązania:

* Mniej kłócenia się przy stole "ALE JAK TO!". Gracze szczęśliwsi -> lepsza Opowieść.
* Akcja jest szybsza i bardziej dynamiczna. Mniej kłócenia, więcej akcji.
* Promuję kombinowanie ze strony Graczy. Gra taktyczna, działają szare komórki...

Kluczem jest to, by z perspektywy fikcji wynik mechaniki miał sens.

### 5.8. Sukces, Porażka, Skonfliktowany Sukces?

#### 4.9.1. Sukces

* W skrócie, Gracz dostał to co chciał. Przeciwnik traci 1 punkt Trwałości, jeśli jego dobrobyt był elementem konfliktu.
* Gracz może opowiedzieć co się stało i w jaki sposób. Wygrał.

#### 4.9.2. Porażka

* W skrócie, MG dostał to co chciał.
* MG może opowiedzieć co się stało i w jaki sposób. Wygrał.
* Jeśli nieznana jest stawka konfliktu, Postać Gracza dostaje Ranę lub MG dostaje Problem do użycia później.
* Gracz dostaje 2 punkty Wpływu.

#### 4.9.3. Skonfliktowany Sukces

* W skrócie, zarówno Gracz jak i MG dostali to, czego chcieli. Wybierz jedno z poniższych:
    * Gracz nie dostał tego, co chciał ani MG nie dostał tego, co chciał. ALE sytuacja posunęła się do przodu.
    * Zarówno Gracz dostał to, co chciał jak i MG dostał to, co chciał.
    * Gracz dostał to, co chciał, ale postać gracza dostaje Ranę lub MG dostaje Problem.
    * Gracz dostał to, co chciał, ale Coś Poszło Nie Tak.
* Gracz może opowiedzieć co się stało i w jaki sposób, acz MG też może coś dopowiedzieć.
* Gracz dostaje 1 punkt Wpływu.

### 5.9. Przelosowania

W tej mechanice Gracz - jeśli mu zależy - **zawsze** wygrywa. Kwestia taka, że MG jest w stanie wygrać wszystko inne niż tą jedną rzecz, na której Graczowi zależy. Dlatego pojawia się mechanizm przerzutów - jeśli Gracz nie akceptuje wyniku rzutu kostką, może rzucić jeszcze raz. I tak do oporu.

Więc, jeśli Gracz nie akceptuje wyniku rzutu kostką:

* Postać zwraca jeden z dwóch wyjętych Żetonów do woreczka (czyli kasuje z Konfliktu ten słabszy)
* Mistrz Gry dostaje +1 Problem i +1 punkt Wpływu
* Gracz rzuca jeszcze raz.

Po co:

* Celem Gracza jest osiągnięcie tego, co zamierza.
* Celem Mistrza Gry jest wygenerowanie jak największej ilości Wpływu.

## 6. Tabelka prawdopodobieństw konfliktów

TODO

## 7. Poziomy wyzwań od łatwych do trudnych

TODO

## 8. Extras

### 8.1. Dodatki

Jupyterowy dokument analizy mechaniki tokenów: rpg/mechanika/full/Materials/180499_all/mechanics_token_analysis.ipynb

### 8.2. Changelog

* 180422: amendowane o PvP, zmieniona zasada pierwszych sesji / wariant uproszczony. Korekta markdowna.
* 180408: release candidate, bez pkt 6 (tabelka) i 7 (poziomów wyzwań)
