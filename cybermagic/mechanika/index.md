---
layout: default
title: Mechanika
---

# {{ page.title }}

## Mechanika skrócona:

[Mechanika skrócona](mechanika-skrocona.html)

## Mechanika pełna:

Ta mechanika jest zaprojektowana jako wersja 'dokładniejsza'. Zaprojektowana jest dla osób, którym _mniej_ zależy na prędkości rozgrywki a _bardziej_ na wpływie i różnorodności możliwości. Wymaga trochę więcej zapamiętywania, liczenia i zapisywania, ale dzięki temu daje większą kontrolę i granularność konfliktów. Polecam osobom bardziej doświadczonym w mechanice uproszczonej. Polecam zwłaszcza wtedy, jeśli chcecie podnieść sobie stopień złożoności rzeczywistości.

[Mechanika pełna, dokładniejsza](mechanika-pelna.html)

## Notatki mechaniki:

Ta kategoria zawiera opis zmian na przestrzeni czasu. Pokazuje ewolucję mechaniki lub wektory ewolucji; jest to z jednej strony TODO (co ma być do zrobienia) jak i próbę racjonalizacji do czego dążymy.

* [Notatki, zmiany w czasie](notes/index.html)
