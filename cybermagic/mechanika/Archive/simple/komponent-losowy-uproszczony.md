---
layout: mechanika
title: "Komponent losowy - uproszczony"
---

# {{ page.title }}

## Opis

* Kostka: 1k20, przekształcona w pseudo-Gaussa.
* Rzuca jedna strona (zawsze gracz) 
    * Więc musi być od ujemnej do dodatniej (by rzucający nie miał przewagi)
    * Więc musi być symetryczny

## Przełożenie na grę

|  **Wynik na kości**   | **Modyfikator**   |
| 20                    | +3                |
| 19, 18, 17, 16        | +2                |
| 15, 14, 13, 12, 11    | +1                |
| 10, 09, 08, 07, 06    | -1                |
| 05, 04, 03, 02        | -2                |
| 01                    | -3                |
