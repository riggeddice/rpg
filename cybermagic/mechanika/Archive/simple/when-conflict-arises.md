---
layout: mechanika
title: "When conflict arises"
---

# {{ page.title }}

## 1. Note what the conflict is about
## 2. Determine difficulty level, write it down:
Determine how difficlt is should be (3-7)
If the opponent had a chance to prepare or circumstances support the opponent, add up to 2.

| .Difficulty level. | .Very easy. | .Easy. | .Normal. | .Hard. | .Very hard. |
|:-:                 |:-:          |:-:     |:-:       |:-:     |:-:          |
|    Value           |       3     |  4     |      5   |   6    |        7    |

## 3. Determine player level, write it down:
Decide, who participates in the conflict.

Add:
Value of an attribute appopriate to the conflict.
If character has an appropriate skill, add 2.
If at least one motivation supports the action and there are no opposing motivations, add 2.
If at least one motivation opposes the action and there are no supporting motivations, subtract 2.
If there are both supporting and opposing motivations, add 0.

If player had a chance to prepare or circumstances support them, add up to 2.
If you find the idea surprising, good or likely to success, add up to 2.

If players cooperate, they choose which character is leading the conflict.
Cooperation adds 1 to players' value.


## 4. Roll a die, write it down:
Die roll translates to -3 to 3 according to table below:

| .Roll. | .1. | .2-5. | .6-10. | .11-15. | .16-19. |.20. |
|:-:     |:-:  |:-:    |:-:     |:-:      |:-:      |:-:  |
|  Value |  -3 |  -2   |  -1    |  +1     |  +2     |  +3 |

## 5. Add player strength and roll and compare to opponent strength.
1. If player strength is higher than difficulty level, players won.
Describe what happened. 
1. If player strength is higher than difficulty level, players lost the conflict. 
In this case you have two options:
    1. Players accept the defeat. Describe what happened. 
    1. Players do not accept the defeat. Player can re-roll at a cost of complications. Determine what happens before player rolls again. Complication will be implemented even if player in the end fails the conflict. First complication should not be too harsh. Up to two re-rolls are allowed. If player decides to re-roll a second time, they get +1 to test, and complication is more serious.
1. In case of a tie, negotiate the result. Answer to question "did player win"? is then "yes, but..." or "no, but..."
In the first case a complication for the player appears, in second, some positive thing will happen.
