---
layout: mechanika
title: "Kiedy na sesji pojawia się konflikt"
---

# {{ page.title }}

## 1. Zanotuj, czego dotyczy konflikt (np. naprawa kserokopiarki)
## 2. Określ poziom trudności, zanotuj wartość:
Oceń, jak trudny powinien być konflikt (od 3 do 7)
Jeżeli w konflikcie występuje przeciwnik i miał okazję się przygotować, dodaj 1 lub 2.

| .Poziom trudności. | .Bardzo łatwy. | .Łatwy. | .Normalny. | .Trudny. | .Bardzo trudny. |
|:-:                 |:-:             |:-:      |:-:         |:-:       |:-:              |
|    Wartość         |       3        |  4      |      5     |   6      |        7        |

## 3. Określ siłę graczy, zanotuj wartość
Ustalcie, który z graczy uczestniczy w konflikcie.

Weź wartość odpowiadającej konfliktowi cechy (-1, 0 lub 1)
Jeżeli postać posiada umiejętność pasującą do konfliktu, dodaj 2
Jeżeli akcja, którą postać wykonuje jest zgodna z przynajmniej jednym z impulsów postaci, dodaj 2. Jeżeli jest sprzeczna, odejmij 2. Jeżeli postać posiada impulsy zarówno zgodne jak i sprzeczne, nic nie dodawaj.

Jeżeli postać miała możliwość przygotować się do konfliktu albo posiada odpowiednie zasoby, dodaj od 1 do 2.
Jeżeli pomysł graczy uznasz za interesujący, zaskakujący, mający dużą szansę na sukces, możesz dodać 1 lub 2.

Jeżeli gracze współpracują, wybierają, która postać wykonuje test.
Za współpracę otrzymują bonus +1.



## 4. Rzuć kością, zanotuj wartość
Rzut kością przełoży się na wartość od -3 do 3 według poniższej tabelki:

| .Wynik rzutu. | .1. | .2-5. | .6-10. | .11-15. | .16-19. |.20. |
|:-:            |:-:  |:-:    |:-:     |:-:      |:-:      |:-:  |
|  Wartość      |  -3 |  -2   |  -1    |  +1     |  +2     |  +3 |

## 5. Zsumuj siłę graczy z zanotowanym wynikiem rzutu kością i porównaj z poziomem trudności.
1. Jeżeli gracze uzyskali wynik wyższy niż poziom trudności, wygrali konflikt. 
Opisz, co się wydarzyło.
1. Jeżeli gracze uzyskali wynik niższy niż poziom trudności, przegrali konflikt. 
W tej sytuacji macie dwie możliwości:
    1. Gracze akceptują porażkę. Opisz, co się wydarzyło.
    1. Gracze nie akceptują porażki. Gracz może rzucić ponownie kosztem komplikacji fabularnej. Ustalcie, co niekorzystnego się stanie, zanim gracz rzuci drugi raz. Komplikacja fabularna pojawi się nawet, jeśli gracz ostatecznie przegra konflikt. Pierwsza komplikacja fabularna nie powinna być zbyt poważna. Dozwolone są dwie próby przerzutu. Przy drugiej próbie gracz ma +1 do testu, ale komplikacja fabularna jest poważniejsza.
1. W przypadku remisu negocjujcie wynik. Odpowiedź na pytanie "czy gracz wygrał?" brzmi wtedy "tak, ale..." lub "nie, ale..."
W pierwszym wypadku pojawia się komplikacja fabularna dla graczy, w drugim jakieś korzystne wydarzenie.
