---
layout: mechanika
title: "Ustalenie poziomu konfliktu uproszczone"
---

# {{ page.title }}

## Wszystkie tabelki pod ręką

### Poziom postaci w tym zadaniu:

| .Poziom trudności. | .Bardzo łatwy. | .Łatwy. | .Normalny. | .Trudny. | .Bardzo trudny. |
|:-:                 |:-:             |:-:      |:-:         |:-:       |:-:              |
|    Wartość         |       3        |  4      |      5     |   6      |        7        |

## Przykłady
