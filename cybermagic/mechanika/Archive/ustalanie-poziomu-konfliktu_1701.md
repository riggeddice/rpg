---
layout: mechanika
title: "Ustalenie poziomu konfliktu"
---

# {{ page.title }}

## Wszystkie tabelki pod ręką

### Poziom postaci:

![TODO](Materials/170416_inklinacje.png)

### Deklaracja akcji:

![TODO](Materials/170416_trudnosc_akcji.png)

### Określenie stanu akcji:

![TODO](Materials/170416_przeciwstawne.png)

### Przeprowadzenie samej akcji:

**Poziom postaci + Deklaracja akcji + Stan akcji - Poziom Zawodowy (2) = Wartość Testowana** (dodatnia lub ujemna)

## Wyjaśnienie

### Określenie poziomu postaci w tym zadaniu:

![TODO](Materials/170416_inklinacje.png)

| **Ilość powiązanych tagów**   | **Umiejętności postaci**  |
| -2                            | antytalent                |
| -1                            | dwie lewe ręce            |
| 0                             | brak umiejętności         |
| 1                             | amatorski                 |
| 2                             | zawodowy                  |
| 3                             | ekspert                   |
| 4                             | elita                     |
| 5                             | arcymistrz                |

PRZYKŁAD:

* Wariant 1:

Hektor, prokurator, próbuje wyciągnąć zeznania z przesłuchiwanego opryszka. Ma impuls "dociekliwy", umiejętność "prokurator", specjalizację "wyciąganie zeznań", inklinację "SC_A" na plusie. Są to cztery tagi, więc w tym konflikcje występuje na poziomie ELITARNYM.

* Wariant 2:

Karolina jest alchemikiem. Próbuje stworzyć miksturę, która dodana do ponczu wywoła dość sporą dywersję. Ma impuls "hedonistka", zawodowo jest chemikiem oraz specjalizuje się w hedonistycznych środkach psychoaktywnych. Inklinację "Craft" ma na plusie. Przy czterech tagach występuje w konflikcie na poziomie ELITARNYM.

* Wariant 3:

Paulina jest lekarzem. Próbuje wkraść się w łaski grupy prowadzącej nielegalną arenę, by pozbierać dowody. Ma impuls "make things right", zawodowo potrafi plotkować a z inklinacji "SC_F" na plusie. Daje jej to poziom EKSPERCKI.

### Określenie trudności testu:

#### Deklaracja akcji:

Deklarację akcji rozpatruje się zawsze przeciwko poziomowi ZAWODOWEMU. Czyli zakładamy na tym etapie, że po drugiej stronie jest kompetentny profesjonalista.

![TODO](Materials/170416_trudnosc_akcji.png)

PRZYKŁAD:

* Wariant 1:

Hektor przesłuchuje opryszka sam, w prokuraturze. Jest to akcja, jaką profesjonalista powinien często zrobić z powodzeniem; +1. Wpływ jest typowy. Skala jest podobna. To implikuje, że jest to akcja łatwa: +1.

* Wariant 2:

Karolina potrzebuje eliksiru zdolnego do wywołania sporej dywersji. Jakkolwiek zrobienie eliksiru rozleniwiającej przyjemności (z odpowiednim opóźnieniem) jest typowym zadaniem (0), na jej niekorzyść działa bardzo przytłaczająca skala tej akcji (-4). Szczęśliwie, sam efekt jest niewielki (+2). W wyniku, Karolina będzie miała do czynienia z trudnym zadaniem: -2.

* Wariant 3:

Paulina próbuje przekonać magów, by włączyli ją w swoje szeregi. Dla profesjonalnego manipulatora jest to akcja trudna, rzadko się to udaje od strzału (-1). Klasa efektu też jest duża, bo staje się jedną z nich (-2). I musi przekonać osoby w dużej grupie (-1). Ogólnie, akcja ekstremalnie trudna: -4.

#### Określenie stanu samej akcji:

Mając ustalony poziom postaci pod kątem danej akcji oraz wynikający z deklaracji akcji poziom trudności można nareszcie określić jakie są parametry przeciwstawne samego konfliktu.

![TODO](Materials/170416_przeciwstawne.png)

PRZYKŁAD:

* Wariant 1:

Hektor ma pozycję dominującą (+1) wobec opryszka; jest na swoim terenie, opryszek ogólnie ma przechlapane. Nie ma przewagi sprzętowej ani magicznej. Nie wykorzystuje żadnych opcjonalnych surowców. W związku z tym przewaga Hektora jest lekka: +1.

* Wariant 2:

Karolina chce zrobić dywersję; z uwagi na to, że nikt nie spodziewa się ponczu jako wektora ataku ma pozycję dominującą (+1). Druga strona nie ma czym się bronić, więc ma też przewagę sprzętową (+1). Ma też przewagę magiczną (+1). By zwiększyć swoje szanse, proponuje komplikację fabularną: część znaczących osób to skompromitowało, jest skandal, będą szukali autora eliksiru; zostało to zaakceptowane dla kolejnego +1. W sumie - jej działania mają zdecydowaną przewagę nad drugą stroną (+4).

* Wariant 3:

Paulina ma pozycję zbliżoną (0); ona potrzebuje ich a oni ją. Nie ma też przewagi sprzętowej (0) ani magicznej (0). W związku z tym Paulina decyduje się spalić surowiec (+1), by pozyskać wiedzę na ich temat i wkraść się w ich łaski oraz przyjąć komplikację fabularną (+1) - któryś z jej pacjentów będzie ofiarą na tej arenie, gdy ona będzie w niej uczestniczyć. W wyniku, ma pewną przewagę nad drugą stroną (+2).

#### Przeprowadzenie samej akcji:

Mając wszystkie powyższe elementy można złożyć parametry konfliktu przez dodanie do siebie wszystkich liczb i odjęcie poziomu zawodowego.

**Poziom postaci + Deklaracja akcji + Stan akcji - Poziom Zawodowy (2) = Wartość Testowana** (dodatnia lub ujemna)

* Wariant 1:

* Hektor ma poziom elitarny (+4)
* Zgodnie z deklaracją akcji, Hektor ma do czynienia z akcją łatwą (+1)
* Stan akcji daje Hektorowi lekką przewagę (+1)
* Poziom zawodowy to koszt (-2)
* Wartość Testowana tej akcji (suma) wynosi +4.

* Wariant 2:

* Karolina ma poziom elitarny (+4)
* Zgodnie z deklaracją akcji, Karolina ma do czynienia z akcją trudną (-2)
* Stan akcji daje Karolinie zdecydowaną przewagę (+4)
* Poziom zawodowy to koszt (-2)
* Wartość Testowana tej akcji (suma) wynosi +4.

* Wariant 3:

* Paulina ma poziom ekspercki (+3)
* Zgodnie z deklaracją akcji, Paulina ma do czynienia z akcją ekstremalnie trudną (-4)
* Stan akcji daje Paulinie przewagę (+2)
* Poziom zawodowy to koszt (-2)
* Wartość Testowana tej akcji (suma) wynosi -1.
