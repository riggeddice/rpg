---
layout: mechanika
title: Mechanika, wersja uproszczona
---

# {{ page.title }}

## Poziom ideowy

* [Co to jest system intencyjny? (EN)](common/intention_rpg_en.html)

## Budowanie postaci

* [Karta postaci (uproszczona)](simple/karta-postaci-uproszczona.html)

## Bezpośrednie wykorzystanie na sesji:

* [Kiedy na sesji pojawia się konflikt](simple/kiedy-pojawia-sie-konflikt-uproszczone.html)
* [Komponent losowy (uproszczony)](simple/komponent-losowy-uproszczony.html)
* [Ustalanie poziomu konfliktu (uproszczone)](simple/ustalanie-poziomu-konfliktu-uproszczone.html)
* [Przerzuty, zasady rerollowania](common/przerzuty-porazki.html)

## Narzędzia MG:

* [Pytania eksploracyjne](common/pytania-eksploracyjne.html)
* [Frakcje](common/frakcje.html)

## Symulacje i warunki stopu:

* [Test: terminus zaskoczony przez gorathaula (TODO: UPROSZCZONA WERSJA)](tests/test-terminus-zaskoczony-gorathaul-1704.html)
