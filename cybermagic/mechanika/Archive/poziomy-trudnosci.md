---
layout: mechanika
title: "Tabela poziomów trudności"
---
# {{ page.title }}

## Tabelka

By się udało, należy mieć wartość ponad zero wobec wartości z poniższej tabelki.
Jeśli odpowiednik maga, dodać: [1,5]: 1 jeśli to zaklęcie, 3 dla jednej Przewagi, 5 dla dwóch.
 
Bez bazy, zwykła delta dookoła zero:

| bez bazy     | trywialny | łatwy  | równy  | trudny | bardzo trudny | heroiczny | legendarny | legendarny mag |
| antybohater  |     -6    |   -4   |   -2   |    0   |      3        |     5     |      7     |       12       |
| commoner     |     -4    |   -2   |    0   |    2   |      5        |     7     |      9     |       14       |
| beginner     |     -3    |   -1   |    1   |    3   |      6        |     8     |     10     |       15       |
| hobbysta     |     -2    |    0   |    2   |    4   |      7        |     9     |     11     |       16       |
| zawodowiec   |     -1    |    1   |    3   |    5   |      8        |    10     |     12     |       17       |
| ekspert      |      0    |    2   |    4   |    6   |      9        |    11     |     13     |       18       |
| elita        |      1    |    3   |    5   |    7   |     10        |    12     |     14     |       19       |
| arcymistrz   |      3    |    5   |    7   |    9   |     12        |    14     |     16     |       21       |
                                                                                                                
## Wyjaśnienie

#### Założenia mechaniki:

Przyrost eskalacji: (0, 0, 1, 2) dla (oryginał, reroll, 2 reroll, 3 reroll)

#### Klasy trudności:

* **trywialny**: istnieją szanse, że to się nie uda w niesprzyjających okolicznościach, normalnie się uda. R_min = -3, więc: -4.
* **łatwy**: powinno się udać bez eskalacji. -2.
* **równy**: poziom równy (zero): 0.
* **trudny**: mamy niewielką przewagę w dwóch obszarach lub znaczącą w jednym, lub szczęście. A = 2 lub R = 2, więc: 2
* **bardzo trudny**: oczekujemy szczęścia oraz znaczącej przewagi w dwóch obszarach. A = 4 i R = 1, więc: 5
* **heroiczny**: oczekujemy szczęścia i super przygotowań. P(sukces_bez_eskalacji) = 25%, P(sukces_max_eskalacja) = 93%. A=6 i R=1, więc: 7. 
* **legendarny**: nie może się udać bez eskalacji. Wymaga super przygotowań a i tak w praktyce koszt i ryzyko są strasznie wysokie. P(sukces_max_eskalacja): 29%.  R_max = 3, A_max = 6 = +9.
* **legendarny dla maga**: to samo, ale dla maga. P(sukces_max_eskalacja): 29%. R_max = 3, magia = 1, magiczne narzędzia i okoliczności = 10 = +14.

#### Poziomy użytkowników:

**parametry**: impulsy:1, inklinacje: 1, umiejętności: 3, 

* **antybohater**: poziom minimum, więc: -2.
* **commoner**: nie zna się, typowy poziom zero, więc: 0.
* **beginner**: ma jedną rzecz na swoją korzyść, więc: 1.
* **zawodowiec**: ma inklinację LUB impuls i poziom zawodowy, więc: 3
* **ekspert**: ma inklinację LUB impuls i poziom ekspercki, więc: 4
* **elita**: ma inklinację, impuls i poziom ekspercki, więc: 5
* **arcymistrz**: maksymalnie skierowane, rozpoznawany krajowo. Ma poziom mistrzowski i podniesioną kategorię, więc: 7

modyfikator:

* **mag**: dodatkowo +1 przez używanie magii.
