---
layout: mechanika-warsztat-jak-grac
title: "Purchase"
---
<br/>
<br/>
....................................

**Position**: {{ page.title }}
Biggest rewards: 
Being talked about in social media.
Recognition from CEO.

"We are just purchasing"
**Attributes:**
<table>
<tr>
	<th>Influence</th>
	<td>&nbsp;+1&nbsp;</td>
	<td>Historiraclly stronger and more respected.</td>
</tr>
<tr>
	<th>Charisma</th>
	<td>&nbsp;-1&nbsp;</td>
	<td>Usually quiet or trying to make feel bad about themselves. "we could do it but it's not our job"</td>
</tr>
<tr>
	<th>Hierarchy</th>
	<td>&nbsp;-1&nbsp;</td>
	<td>?</td>
</tr>
</table>

**Motivations:**

<table>
<tr>
	<th>Look good and be pretigious</th>
	<td>?</td>
	<td align="center" valign="center" width="20%" rowspan="3">
		<strong>-2 / 0 / +2</strong><br/>
		If beneficial, add 2 <br/>
		If oposite, subtract 2<br/>
		If both – add 0</td>
</tr>
<tr>
	<th>Make others feel bad</th>
	<td>Because we're so cool</td>
</tr>
<tr>
	<th>Create sellable assortment</th>
	<td>?</td>
</tr>
</table>

**Skills:**

<table>
<tr>
	<th>Negotiate with manufacturers</th>
	<td>Get a new brand of feed. Get promo materials and sample.</td>
	<td align="center" valign="center" width="20%" rowspan="3">
		<strong>+2</strong><br/>
		If used, add 2<br/>
	</td>
</tr>
<tr>
	<th>Get new products in the system</th>
	<td>This way others can use them as well.</td>
</tr>
<tr>
	<th>Create educational content input</th>
	<td>Not final material, but just input</td>
</tr>
</table>

**Restraints:**

<table>
<tr>
	<td>?</td>
	<td align="center" valign="center" width="20%" rowspan="4">
		Can block execution if possible.
	</td>
</tr>
<tr>
	<td>?</td>
</tr>
<tr>
	<td>?</td>
</tr>
<tr>
	<td>?</td>
</tr>
</table>

**Resources:**

<table>
<tr>
	<td>
		Logistics: <br/>
		Goals: <br/>
		<ul>
			<li>?</li>
		</ul>
	</td>
	<td align="center" valign="center" width="20%" rowspan="4">
		<strong>+2</strong><br/>
		If used, add 2<br/>
	</td>
</tr>
<tr>
	<td>
		?
	</td>
</tr>
<tr>
	<td>?</td>
</tr>
<tr>
	<td>?</td>
</tr>
</table>
