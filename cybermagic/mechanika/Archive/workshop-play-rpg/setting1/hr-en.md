---
layout: mechanika-warsztat-jak-grac
title: "Junior Human Resources Specialist"
---
<br/>
<br/>
....................................

**Role**: {{ page.title }}
Signed a contract as junior and it kind of remained this way, even if he works third year now...
A little bitter about the title, fulfills his purpose organizing different events. Can get anything and organize anything. Skilled in dealing with conflicts - either by soothing them or escalating.

**Attributes:**
<table>
<tr>
	<th>Physical</th>
	<td>&nbsp;0&nbsp;</td>
	<td>Good looks for the win!</td>
</tr>
<tr>
	<th>Social</th>
	<td>&nbsp;+1&nbsp;</td>
	<td>I'm managing. After all there is a reason why we call them "human resources"</td>
</tr>
<tr>
	<th>Science / technological</th>
	<td>&nbsp;-1&nbsp;</td>
	<td>I can read mails and use facebok. Who needs more?</td>
</tr>
</table>

**Motivators:**

<table>
<tr>
	<th>Company image is most important!</th>
	<td>How else can we get new people??</td>
	<td align="center" valign="center" width="20%" rowspan="3">
		<strong>-2 / 0 / +2</strong><br/>
		If beneficial, add 2 <br/>
		If oposite, subtract 2<br/>
		If both – add 0</td>
</tr>
<tr>
	<th>People hoarder</th>
	<td>I know everyone! If not, I will! I am only missing a plumber in my contacts collection...</td>
</tr>
<tr>
	<th>**I** am right!</th>
	<td>Duh!</td>
</tr>
</table>

**Skills:**

<table>
<tr>
	<th>I know a guy...</th>
	<td>I had so many interviews I can always find someone...</td>
	<td align="center" valign="center" width="20%" rowspan="6">
		<strong>+2</strong><br/>
		If used, add 2<br/>
	</td>
</tr>
<tr>
	<th>Parkour</th>
	<td>No candidate can run from me!</td>
</tr>
<tr>
	<th>Organizer</th>
	<td>I even organized boss' son birthday!</td>
</tr>
<tr>
	<th>Procedures</th>
	<td>To do that you need to fill in the form 46B/9...</td>
</tr>
<tr>
	<th>Smalltalk</th>
	<td>Thrives in informal situations</td>
</tr>
<tr>
	<th>Temper tantrum</th>
	<td>NO WAY! This terrible defiance would make even Spock uneasy...</td>
</tr>
</table>

**Resources:**

<table>
<tr>
	<td>Pile of unread CVs</td>
	<td align="center" valign="center" width="20%" rowspan="4">
		<strong>+2</strong><br/>
		If used, add 2<br/>
	</td>
</tr>
<tr>
	<td>Connections on LinkedIn and other portals</td>
</tr>
<tr>
	<td>Friends in all departments</td>
</tr>
<tr>
	<td>Proper running shoes</td>
</tr>
</table>
