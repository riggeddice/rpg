---
layout: mechanika-warsztat-jak-grac
title: "Informatyk"
---
<br/>
<br/>
....................................

**Stanowisko**: {{ page.title }}
...A przy okazji tech support wszystkich i wszystkiego.
Lubi konkrety , pizzę i kawę. W trakcie pracy przed komputerem zapada w “sen programisty” po 3 minutach. Obudzić go może jedynie okrzyk zespołu “To co? KAWA?”. Wtedy wraz z całym stadem przemieszcza się do kuchni, gdzie napełnia czarną cieczą swój ukochany półlitrowy kubek z Homerem Simpsonem. Prowadzi intensywne dyskusje używając takich sformułowań jak “bazy danych”, “refaktoring”, “repozytorium”. Nie cierpi spotkań zespołowych, ponieważ uważa je za nieefektywne. Woli pogadać twarzą w twarz i przy okazji merytorycznej dyskusji trochę poplotkować.

**Cechy:**
<table>
<tr>
	<th>Fizyczne</th>
	<td>&nbsp;-1&nbsp;</td>
	<td>Komputer w pracy, komputer w zabawie... biegam najwyżej na autobus!</td>
</tr>
<tr>
	<th>Społeczne</th>
	<td>&nbsp;0&nbsp;</td>
	<td>Wbrew pozorom nie jesteśmy bandą aspołecznych typów... Po prostu "normalni" ludzie nas nie rozumieją.</td>
</tr>
<tr>
	<th>Naukowo-techniczne</th>
	<td>&nbsp;+1&nbsp;</td>
	<td>O, tak, słonko! Daj mi się tym pobawić!</td>
</tr>
</table>

**Impulsy:**

<table>
<tr>
	<th>A słyszeliście, że...</th>
	<td>Lubi być na topie, wiedzieć wszystko o wszystkim i w sumie… największy plotkarz działu.</td>
	<td align="center" valign="center" width="20%" rowspan="3">
		<strong>-2 / 0 / +2</strong><br/>
		Jeśli są zgodne, dodaj 2. <br/>
		Jeśli są przeciwne, odejmij 2<br/>
		Jeśli takie i takie – dodaj 0</td>
</tr>
<tr>
	<th>Ooo! Coś nowego!</th>
	<td>Nie lubi robić rzeczy, które już robił. Zawsze da się zrobić to trochę inaczej, coś nowego, może… czymś mniej typowym… ;-)</td>
</tr>
<tr>
	<th>Ten zespół to w sumie rodzina...</th>
	<td> Mimo, że nie przepada za ludźmi – swojemu zespołowi pomoże w osiągnięciu celu</td>
</tr>
</table>



**Umiejętności:**

<table>
<tr>
	<th>Techno-gadka</th>
	<td> To straszliwe narzędzie zniszczenia wykorzystuje umiejętności techniczne zamiast społecznych. Można albo próbować to zrozumieć (test techniczny) albo przestać słuchać (test społeczny)</td>
	<td align="center" valign="center" width="20%" rowspan="4">
		<strong>+2</strong><br/>
		Jeśli używasz, dodaj 2<br/>
	</td>
</tr>
<tr>
	<th>Napraw to!</th>
	<td>Sprzęt komputerowy w obecności serwisanta działa. A przynajmniej przez moment.</td>
</tr>
<tr>
	<th>Wszystkie koty moje są!</th>
	<td>Jeśli to jest na sieci, ja to znajdę! Jak tego nie ma na sieci… włamię się i to znajdę ;-).</td>
</tr>
<tr>
	<th>Król Excela</th>
	<td>Ja wiem, że pie chartów powinno się używać tylko na konferencji cukierników, ale szef lubi...</td>
</tr>
</table>

**Zasoby:**

<table>
<tr>
	<td>Prywatny laptop, którym się NIE DZIELI! NIE DOTYKAĆ!</td>
	<td align="center" valign="center" width="20%" rowspan="4">
		<strong>+2</strong><br/>
		Jeśli używasz, dodaj 2<br/>
	</td>
</tr>
<tr>
	<td>Kubek z Homerem</td>
</tr>
<tr>
	<td>Konto na Wykopie</td>
</tr>
<tr>
	<td>Kanciapa, w kanciapie zestaw narzędzi i części zamiennych.</td>
</tr>
</table>