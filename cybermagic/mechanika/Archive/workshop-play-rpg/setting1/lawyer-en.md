---
layout: mechanika-warsztat-jak-grac
title: "Lawyer"
---
<br/>
<br/>
....................................

**Role**: {{ page.title }}
Each corporation is vulneable to attack.
Lawyer is often first and last line of defence from patent trolls.
Tons of contracts pile on his desk and all must be done for yesterday. Delegates questions into oblivion, because he's the only lawyer in the company.
When not at his desk, he paces the corridors, talking via phone with an invisible consultant.
**Attributes:**
<table>
<tr>
	<th>Physical</th>
	<td>&nbsp;-1&nbsp;</td>
	<td>
		Does not get too many opportunities to work out, but gosh, those books are heavy!
	
	</td>
</tr>
<tr>
	<th>Social</th>
	<td>&nbsp;0&nbsp;</td>
	<td>Lawyers mostly get along with other lawyers.</td>
</tr>
<tr>
	<th>Science / technological</th>
	<td>&nbsp;+1&nbsp;</td>
	<td>Rather academic than technical expertise, but really, really good at it.</td>
</tr>
</table>

**Motivators:**

<table>
<tr>
	<th>Leave no secrets uncovered!</th>
	<td>If it's a secret, it's worth knowing.</td>
	<td align="center" valign="center" width="20%" rowspan="3">
		<strong>-2 / 0 / +2</strong><br/>
		If beneficial, add 2 <br/>
		If oposite, subtract 2<br/>
		If both – add 0</td>
</tr>
<tr>
	<th>Citizen man</th>
	<td>Quiet admirer of all lawyer rights defenders and community worker</td>
</tr>
<tr>
	<th>Careful</th>
	<td>Take it easy, law - any law - is not piece of cake. Do you have ANY idea how many acts we must take into account?</td>
</tr>
</table>

**Skills:**

<table>
<tr>
	<th>I know all laws by heart</th>
	<td>I am the one who catches others with some obscure regulations, not the one getting caught!</td>
	<td align="center" valign="center" width="20%" rowspan="4">
		<strong>+2</strong><br/>
		If used, add 2<br/>
	</td>
</tr>
<tr>
	<th>Find a catch</th>
	<td>Anything can go, the more obscure the rule, the better.</td>
</tr>
<tr>
	<th>Stall for time</th>
	<td>With so many rules and catches, I can always find one more thing to talk about...</td>
</tr>
<tr>
	<th>Inqusitor</th>
	<td/>CONFESS RIGHT NOW! IT HAS TO BE SOMEONE'S FAULT SO IT WILL BE YOURS!</td>
</tr>
</table>

**Resources:**

<table>
<tr>
	<td>Office with one additional, very uncomfy chair for exactly one person.</td>
	<td align="center" valign="center" width="20%" rowspan="5">
		<strong>+2</strong><br/>
		If used, add 2<br/>
	</td>
</tr>
<tr>
	<td>Books, booklets and pamphlets</td>
</tr>
<tr>
	<td>Forms, applications and petitions for all purposes.</td>
</tr>
<tr>
	<td>Luxury Mont Blanc pen with diamonds.</td>
</tr>
<tr>
	<td>Temida figurine on the desk.</td>
</tr>
</table>
