---
layout: mechanika-warsztat-jak-grac
title: "Ochroniarz"
---
<br/>
<br/>
....................................

**Stanowisko**: {{ page.title }}
Przyjeżdża do pracy na rowerze. Siedzi przy biurku lub chodzi na obchodzie (co godzinę). 
Patrzy w monitory, a na boku zerka na czarno-biały telewizorek z serialem. 
Odbiera paczki i wpuszcza pracowników, którzy regularnie zapominają karty do budynku.  Czasem słucha radia z przebojami z lat 80tych. Zwykle nudzi się jak mops na posterunku.
Raz na jakiś czas idzie do kuchni zrobić sobie kanapki z masłem i pomidorem.

**Cechy:**
<table>
<tr>
	<th>Fizyczne</th>
	<td>&nbsp;1&nbsp;</td>
	<td>Ktoś w tej firmie musi poruszać się sprawniej od kulawego dziadka... Serio, firma pełna młodych ludzi, którzy dostają zadyszki po wejściu na półpiętro...</td>
</tr>
<tr>
	<th>Społeczne</th>
	<td>&nbsp;1&nbsp;</td>
	<td>Lubi sobie pogadać, łatwo się zaprzyjaźnia.</td>
</tr>
<tr>
	<th>Naukowo-techniczne</th>
	<td>&nbsp;-1&nbsp;</td>
	<td>To nie jego działka</td>
</tr>
</table>

**Impulsy:**

<table>
<tr>
	<th>Nie jestem Twoim służącym!</th>
	<td>Bardzo czuły na punkcie swojej roli</td>
	<td align="center" valign="center" width="20%" rowspan="2">
		<strong>-2 / 0 / +2</strong><br/>
		Jeśli są zgodne, dodaj 2. <br/>
		Jeśli są przeciwne, odejmij 2<br/>
		Jeśli takie i takie – dodaj 0</td>
</tr>
<tr>
	<th>Gaduła</th>
	<td>“Dobry wieczór Pani Marylko, znowu praca po godzinach co?”</td>
</tr>
</table>

**Umiejętności:**

<table>
<tr>
	<th>Zidentyfikuj rozrabiakę</th>
	<td> On po prostu wie, kto może narozrabiać. Wtedy może w porę się wycofać </td>
	<td align="center" valign="center" width="20%" rowspan="3">
		<strong>+2</strong><br/>
		Jeśli używasz, dodaj 2<br/>
	</td>
</tr>
<tr>
	<th>Twarz bez wyrazu</th>
	<td>&nbsp;</td>
</tr>
<tr>
	<th>Sprawianie dobrego wrażenia</th>
	<td>&nbsp;</td>
</tr>
</table>

**Zasoby:**

<table>
<tr>
	<td>Rower</td>
	<td align="center" valign="center" width="20%" rowspan="4">
		<strong>+2</strong><br/>
		Jeśli używasz, dodaj 2<br/>
	</td>
</tr>
<tr>
	<td>Przestrzeń robocza</td>
</tr>
<tr>
	<td>Monitoring na około i w budynkach</td>
</tr>
<tr>
	<td>Klucze do wszystkich pomieszczeń poza biurem szefa</td>
</tr>
</table>