---
layout: mechanika-warsztat-jak-grac
title: "Karta konfliktów"
---
<table>
<tr>
	<th align="center" valign="center">Konflikt</th>
	<th width="30%" align="center" valign="center">Opis</th>
	<th align="center" valign="center">Siła graczy</th>
	<th align="center" valign="center">Siła przeciwnika</th>
	<th align="center" valign="center">Rzut kością</th>
</tr>
<tr>
	<td align="center" valign="center">1.</td>
	<td align="center" valign="center"></td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Cecha</td>
				<td align="center" valign="center">Impuls</td>
				<td align="center" valign="center">Umiejętność</td>
			</tr>
			<tr>
				<td align="center" valign="center">-1 / 0 / +1</td>
				<td align="center" valign="center">-2 / 0 / +2</td>
				<td align="center" valign="center">0 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Zasób</td>
				<td align="center" valign="center">Współpraca</td>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +2</td>
				<td align="center" valign="center">0 / +1</td>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Pomysł</td>
				<td  align="center" valign="center" rowspan="2">Suma</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>
	</td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Trudność</td>
			</tr>
			<tr>
				<td align="center" valign="center">3 / 4 / 5 / 6 / 7</td>
			</tr>
			<tr>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>	
	</td>
	<td align="center" valign="center"></td>
</tr>
<tr>
	<td align="center" valign="center">2.</td>
	<td align="center" valign="center"></td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Cecha</td>
				<td align="center" valign="center">Impuls</td>
				<td align="center" valign="center">Umiejętność</td>
			</tr>
			<tr>
				<td align="center" valign="center">-1 / 0 / +1</td>
				<td align="center" valign="center">-2 / 0 / +2</td>
				<td align="center" valign="center">0 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Zasób</td>
				<td align="center" valign="center">Współpraca</td>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +2</td>
				<td align="center" valign="center">0 / +1</td>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Pomysł</td>
				<td  align="center" valign="center" rowspan="2">Suma</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>
	</td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Trudność</td>
			</tr>
			<tr>
				<td align="center" valign="center">3 / 4 / 5 / 6 / 7</td>
			</tr>
			<tr>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>	
	</td>
	<td align="center" valign="center"></td>
</tr>
<tr>
	<td align="center" valign="center">3.</td>
	<td align="center" valign="center"></td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Cecha</td>
				<td align="center" valign="center">Impuls</td>
				<td align="center" valign="center">Umiejętność</td>
			</tr>
			<tr>
				<td align="center" valign="center">-1 / 0 / +1</td>
				<td align="center" valign="center">-2 / 0 / +2</td>
				<td align="center" valign="center">0 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Zasób</td>
				<td align="center" valign="center">Współpraca</td>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +2</td>
				<td align="center" valign="center">0 / +1</td>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Pomysł</td>
				<td  align="center" valign="center" rowspan="2">Suma</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>
	</td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Trudność</td>
			</tr>
			<tr>
				<td align="center" valign="center">3 / 4 / 5 / 6 / 7</td>
			</tr>
			<tr>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>	
	</td>
	<td align="center" valign="center"></td>
</tr>
<tr>
	<td align="center" valign="center">4.</td>
	<td align="center" valign="center"></td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Cecha</td>
				<td align="center" valign="center">Impuls</td>
				<td align="center" valign="center">Umiejętność</td>
			</tr>
			<tr>
				<td align="center" valign="center">-1 / 0 / +1</td>
				<td align="center" valign="center">-2 / 0 / +2</td>
				<td align="center" valign="center">0 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Zasób</td>
				<td align="center" valign="center">Współpraca</td>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +2</td>
				<td align="center" valign="center">0 / +1</td>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Pomysł</td>
				<td  align="center" valign="center" rowspan="2">Suma</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>
	</td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Trudność</td>
			</tr>
			<tr>
				<td align="center" valign="center">3 / 4 / 5 / 6 / 7</td>
			</tr>
			<tr>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>	
	</td>
	<td align="center" valign="center"></td>
</tr>
<tr>
	<td align="center" valign="center">5.</td>
	<td align="center" valign="center"></td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Cecha</td>
				<td align="center" valign="center">Impuls</td>
				<td align="center" valign="center">Umiejętność</td>
			</tr>
			<tr>
				<td align="center" valign="center">-1 / 0 / +1</td>
				<td align="center" valign="center">-2 / 0 / +2</td>
				<td align="center" valign="center">0 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Zasób</td>
				<td align="center" valign="center">Współpraca</td>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +2</td>
				<td align="center" valign="center">0 / +1</td>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Pomysł</td>
				<td  align="center" valign="center" rowspan="2">Suma</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>
	</td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Trudność</td>
			</tr>
			<tr>
				<td align="center" valign="center">3 / 4 / 5 / 6 / 7</td>
			</tr>
			<tr>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>	
	</td>
	<td align="center" valign="center"></td>
</tr>
<tr>
	<td align="center" valign="center">6.</td>
	<td align="center" valign="center"></td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Cecha</td>
				<td align="center" valign="center">Impuls</td>
				<td align="center" valign="center">Umiejętność</td>
			</tr>
			<tr>
				<td align="center" valign="center">-1 / 0 / +1</td>
				<td align="center" valign="center">-2 / 0 / +2</td>
				<td align="center" valign="center">0 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Zasób</td>
				<td align="center" valign="center">Współpraca</td>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +2</td>
				<td align="center" valign="center">0 / +1</td>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Pomysł</td>
				<td  align="center" valign="center" rowspan="2">Suma</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>
	</td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Trudność</td>
			</tr>
			<tr>
				<td align="center" valign="center">3 / 4 / 5 / 6 / 7</td>
			</tr>
			<tr>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>	
	</td>
	<td align="center" valign="center"></td>
</tr>
<tr>
	<td align="center" valign="center">7.</td>
	<td align="center" valign="center"></td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Cecha</td>
				<td align="center" valign="center">Impuls</td>
				<td align="center" valign="center">Umiejętność</td>
			</tr>
			<tr>
				<td align="center" valign="center">-1 / 0 / +1</td>
				<td align="center" valign="center">-2 / 0 / +2</td>
				<td align="center" valign="center">0 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Zasób</td>
				<td align="center" valign="center">Współpraca</td>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +2</td>
				<td align="center" valign="center">0 / +1</td>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Pomysł</td>
				<td  align="center" valign="center" rowspan="2">Suma</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>
	</td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Trudność</td>
			</tr>
			<tr>
				<td align="center" valign="center">3 / 4 / 5 / 6 / 7</td>
			</tr>
			<tr>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>	
	</td>
	<td align="center" valign="center"></td>
</tr>
<tr>
	<td align="center" valign="center">8.</td>
	<td align="center" valign="center"></td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Cecha</td>
				<td align="center" valign="center">Impuls</td>
				<td align="center" valign="center">Umiejętność</td>
			</tr>
			<tr>
				<td align="center" valign="center">-1 / 0 / +1</td>
				<td align="center" valign="center">-2 / 0 / +2</td>
				<td align="center" valign="center">0 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Zasób</td>
				<td align="center" valign="center">Współpraca</td>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +2</td>
				<td align="center" valign="center">0 / +1</td>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Pomysł</td>
				<td  align="center" valign="center" rowspan="2">Suma</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>
	</td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Trudność</td>
			</tr>
			<tr>
				<td align="center" valign="center">3 / 4 / 5 / 6 / 7</td>
			</tr>
			<tr>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>	
	</td>
	<td align="center" valign="center"></td>
</tr>
<tr>
	<td align="center" valign="center">9.</td>
	<td align="center" valign="center"></td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Cecha</td>
				<td align="center" valign="center">Impuls</td>
				<td align="center" valign="center">Umiejętność</td>
			</tr>
			<tr>
				<td align="center" valign="center">-1 / 0 / +1</td>
				<td align="center" valign="center">-2 / 0 / +2</td>
				<td align="center" valign="center">0 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Zasób</td>
				<td align="center" valign="center">Współpraca</td>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +2</td>
				<td align="center" valign="center">0 / +1</td>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Pomysł</td>
				<td  align="center" valign="center" rowspan="2">Suma</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>
	</td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Trudność</td>
			</tr>
			<tr>
				<td align="center" valign="center">3 / 4 / 5 / 6 / 7</td>
			</tr>
			<tr>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>	
	</td>
	<td align="center" valign="center"></td>
</tr>
<tr>
	<td align="center" valign="center">10.</td>
	<td align="center" valign="center"></td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Cecha</td>
				<td align="center" valign="center">Impuls</td>
				<td align="center" valign="center">Umiejętność</td>
			</tr>
			<tr>
				<td align="center" valign="center">-1 / 0 / +1</td>
				<td align="center" valign="center">-2 / 0 / +2</td>
				<td align="center" valign="center">0 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Zasób</td>
				<td align="center" valign="center">Współpraca</td>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +2</td>
				<td align="center" valign="center">0 / +1</td>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Pomysł</td>
				<td  align="center" valign="center" rowspan="2">Suma</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>
	</td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Trudność</td>
			</tr>
			<tr>
				<td align="center" valign="center">3 / 4 / 5 / 6 / 7</td>
			</tr>
			<tr>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>	
	</td>
	<td align="center" valign="center"></td>
</tr>
<tr>
	<td align="center" valign="center">11.</td>
	<td align="center" valign="center"></td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Cecha</td>
				<td align="center" valign="center">Impuls</td>
				<td align="center" valign="center">Umiejętność</td>
			</tr>
			<tr>
				<td align="center" valign="center">-1 / 0 / +1</td>
				<td align="center" valign="center">-2 / 0 / +2</td>
				<td align="center" valign="center">0 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Zasób</td>
				<td align="center" valign="center">Współpraca</td>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +2</td>
				<td align="center" valign="center">0 / +1</td>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Pomysł</td>
				<td  align="center" valign="center" rowspan="2">Suma</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>
	</td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Trudność</td>
			</tr>
			<tr>
				<td align="center" valign="center">3 / 4 / 5 / 6 / 7</td>
			</tr>
			<tr>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>	
	</td>
	<td align="center" valign="center"></td>
</tr>
<tr>
	<td align="center" valign="center">12.</td>
	<td align="center" valign="center"></td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Cecha</td>
				<td align="center" valign="center">Impuls</td>
				<td align="center" valign="center">Umiejętność</td>
			</tr>
			<tr>
				<td align="center" valign="center">-1 / 0 / +1</td>
				<td align="center" valign="center">-2 / 0 / +2</td>
				<td align="center" valign="center">0 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Zasób</td>
				<td align="center" valign="center">Współpraca</td>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +2</td>
				<td align="center" valign="center">0 / +1</td>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Pomysł</td>
				<td  align="center" valign="center" rowspan="2">Suma</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>
	</td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Trudność</td>
			</tr>
			<tr>
				<td align="center" valign="center">3 / 4 / 5 / 6 / 7</td>
			</tr>
			<tr>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>	
	</td>
	<td align="center" valign="center"></td>
</tr>
<tr>
	<td align="center" valign="center">13.</td>
	<td align="center" valign="center"></td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Cecha</td>
				<td align="center" valign="center">Impuls</td>
				<td align="center" valign="center">Umiejętność</td>
			</tr>
			<tr>
				<td align="center" valign="center">-1 / 0 / +1</td>
				<td align="center" valign="center">-2 / 0 / +2</td>
				<td align="center" valign="center">0 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Zasób</td>
				<td align="center" valign="center">Współpraca</td>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +2</td>
				<td align="center" valign="center">0 / +1</td>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Pomysł</td>
				<td  align="center" valign="center" rowspan="2">Suma</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>
	</td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Trudność</td>
			</tr>
			<tr>
				<td align="center" valign="center">3 / 4 / 5 / 6 / 7</td>
			</tr>
			<tr>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>	
	</td>
	<td align="center" valign="center"></td>
</tr>
<tr>
	<td align="center" valign="center">14.</td>
	<td align="center" valign="center"></td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Cecha</td>
				<td align="center" valign="center">Impuls</td>
				<td align="center" valign="center">Umiejętność</td>
			</tr>
			<tr>
				<td align="center" valign="center">-1 / 0 / +1</td>
				<td align="center" valign="center">-2 / 0 / +2</td>
				<td align="center" valign="center">0 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Zasób</td>
				<td align="center" valign="center">Współpraca</td>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +2</td>
				<td align="center" valign="center">0 / +1</td>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Pomysł</td>
				<td  align="center" valign="center" rowspan="2">Suma</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>
	</td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Trudność</td>
			</tr>
			<tr>
				<td align="center" valign="center">3 / 4 / 5 / 6 / 7</td>
			</tr>
			<tr>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>	
	</td>
	<td align="center" valign="center"></td>
</tr>
<tr>
	<td align="center" valign="center">15.</td>
	<td align="center" valign="center"></td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Cecha</td>
				<td align="center" valign="center">Impuls</td>
				<td align="center" valign="center">Umiejętność</td>
			</tr>
			<tr>
				<td align="center" valign="center">-1 / 0 / +1</td>
				<td align="center" valign="center">-2 / 0 / +2</td>
				<td align="center" valign="center">0 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Zasób</td>
				<td align="center" valign="center">Współpraca</td>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +2</td>
				<td align="center" valign="center">0 / +1</td>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Pomysł</td>
				<td  align="center" valign="center" rowspan="2">Suma</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>
	</td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Trudność</td>
			</tr>
			<tr>
				<td align="center" valign="center">3 / 4 / 5 / 6 / 7</td>
			</tr>
			<tr>
				<td align="center" valign="center">Przygotowanie</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>	
	</td>
	<td align="center" valign="center"></td>
</tr>
</table>
