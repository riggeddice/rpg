---
layout: mechanika
title: Mechanika, wersja pełna
---

# {{ page.title }}

## Poziom ideowy

* [Co to jest system intencyjny? (EN)](common/intention_rpg_en.html)
* TODO: [Zasada współpracy Graczy i MG](zasada-wspolpracy-graczy-mg.html)
* TODO: [Co to jest konflikt?](co-to-jest-konflikt.html)

## Role

* TODO: [Gracz i Mistrz Gry](gracz-i-mistrz-gry.html)
* TODO: [Twórca fabuły, Moderator grupy](tworca-fabuly-moderator-grupy.html)

## Budowanie postaci

* [Budowanie Profilu Postaci](full/profile-building-1804.html)

## Bezpośrednie wykorzystanie na sesji

### Rdzeń mechaniki

* [Rozwiązywanie konfliktu](full/conflict-resolution-1804.html)
* [Analiza matematyczna mechaniki](full/mechanics-analysis-1807.html)

### Magia

* [Omówienie magii - czym jest w tym systemie](magic/o-magii-na-szybko-1804.html)
* [Mechanika magii - jak rzucać czary](magic/mechanika-magii-1804.html)
* [Efekty Skażenia](magic/efekty-skazenia.html)

## Narzędzia MG

* [Pytania eksploracyjne](common/pytania-eksploracyjne.html)
* [Frakcje](common/frakcje.html)
* TODO: [Zasada Lenistwa MG](zasada-lenistwa-mg.html)
* TODO: [Jak (i czemu) złożyć konflikt?](jak-i-czemu-zlozyc-konflikt.html)
* TODO: [Tory, ścieżki celów, wątki](tory-sciezki-celow-watki.html)

## Symulacje i warunki stopu:

* [Test: Gorący dyletant rywalizujący z zimnym specjalistą](full/tests/test-goracy-dyletant-zimny-specjalista.html)
