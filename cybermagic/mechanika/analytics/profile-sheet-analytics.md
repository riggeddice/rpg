# Profile Sheet Analytics

## 1. Aktualność dokumentu

* Dokument jest aktualny wobec mechaniki 1807
* Dokument jest wyprodukowany: 180725
* Ostatnia aktualizacja: 180725

## 2. Czas budowy Profili

| Twórca        | Postać                    | Czas budowania (minuty) |
|---------------|---------------------------|-------------------------|
| Rejnułar      | kuba wirus                | 29 |
| Arleta        | bronisława strzelczyk     | 34 |

## 3. Obserwacje przy używaniu Profili

* 180724
    * Arleta: 'czy Zachowanie wchodzi czy nie?'
    * Rejnułar: 'ale na Otoczeniu z tego konfliktu czy wstecznego?'
* tbd
