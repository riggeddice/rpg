# Influence mechanics analytics

## 1. Aktualność dokumentu

* Dokument jest aktualny wobec mechaniki 1807
* Dokument jest wyprodukowany: 180725
* Ostatnia aktualizacja: 180731

## 2. Liczby

| Numer sesji   | Gracz / MG    | Wydany Wpływ podczas Scen | Wydany Wpływ po sesji | Zmarnowany Wpływ |
|---------------|---------------|---------------------------|-----------------------|------------------|
| 180724        | Arleta        | 1                         | 1                     | 1                |
| 180724        | Rejnułar      | 1                         | 1                     | 1                |
| 180724        | Żółw          | 0                         | 6                     | 2                |
| 180730        | Kić           | 2                         | 0                     | 0                |
| 180730        | Żółw          | 6                         | 2                     | 0                |
| 180808        | Kić           | 8                         | 9                     | 2                |
| 180808        | Żółw          | 31                        | 5                     | 1                |

## 3. Omówienie liczb

### 180724

1. Wysokie zmęczenie MG
2. Gracze na sesji dokupują mechanizmy im potrzebne
    1. Wiedza postaci o Skorpipedach (R.) - działanie wsteczne
    1. Wiedza postaci o okolicy (A.) - działanie wsteczne
3. Gracze wykorzystują Wpływ po sesji jako kompensator szkody jaką wyrządzono ICH postaciom

### 180730

1. Sesja bardzo ambitna, MitH z ząbkami
2. Podczas sesji
    1. Kić:
        1. dodanie Okoliczności uwalniającej NPC od statusu (1)
        2. kupienie jednej sceny (1)
    2. Żółw (MG):
        1. powołanie NPCa do pomocy postaci co będzie przeszkadzał (1)
        2. dodanie Okoliczności nadającej negatywny status NPCowi - ścigany przez prawo (1)
        3. dodanie ataku przeciw Graczowi (proszek kralotyczny w powietrzu) (1)
        4. nadanie statusu Zmęczony Graczowi i NPCowi (1)
        5. ratowanie Wroga przed eliminacją (2)
3. Po sesji
    1. Kić
        1. BLOKADA - będzie kontynuacja
    2. Żółw
        1. wzmocnienie wrogiej frakcji (1)

### 180808

1. Sesja była dziwna, bardzo dziwna. Bardzo chaotyczna.
2. Podczas sesji:
    1. Kić:
        1. 2* dodanie sceny (2*2)
        2. 2* powołanie NPCa: Feliks i Antonio (2*1)
        3. 1* wyłączenie Syreny (1)
    2. Żółw (MG):
        1. ~10* negatywne okoliczności czy 'malfunction' (10*1)
        2. wyłączenie ważnego NPCa (Stach) (3)
        3. konflikt Trudny (przez okoliczności -> Heroiczny): ładunki (2)
        4. Zatrucie sojusznika (2)
        5. wykupienie Kasandry jako kultystki - dark future (10)

### XXX

