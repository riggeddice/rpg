---
layout: cybermagic-karta-postaci
categories: archetype
title: "Nawigator Roju"
---

# {{ page.title }}

## Archetyp

### Co robi? (3)

* przechwytuje każdą komunikację
* zauważa subtelne szczegóły
* wysyła i konstruuje drony

### Czego chce? (3)

* TAK: wszystko musi zrozumieć
* TAK: obudować się zasobami
* NIE: uczucia ponad analizę danych
* NIE: odrzucenie wiedzy

### Jak działa? (3)

* TAK: metodyczny i cierpliwy
* TAK: zbiera i analizuje dane
* TAK: wysyła drony, nie samemu
* NIE: impulsywny i emocjonalny
* NIE: przemoc i wymuszanie

### Zasoby i otoczenie (3)

* sprzęt podsłuchowy i fałszujący
* podręczne analizatory danych
* potężne systemy komunikacyjne
* zbudowane własnoręcznie drony

### Co to za archetyp

Pająk, kontrolujący komunikację i wysyłający drony do wykonywania jego poleceń. Zauważa większość odchyleń od normy.

### Tagi i przykłady

* SILNY: Drony, Komunikacja, Chaos
* SŁABY: Impulsywność, Prędkość
* Obserwator. Metodyczny badacz. Hacker komunikacji.

### Przykładowe działania

* Wprowadzi chaos komunikacją
* Podsłucha wszystko dronami
* Odwraca uwagę dronami
* Znajdzie drogę w Eterze
* Zrozumie dziwny komunikat
