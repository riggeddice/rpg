---
layout: cybermagic-karta-postaci
categories: archetype
title: "Majster nanitek"
---

# {{ page.title }}

## Archetyp

### Co robi? (3)

* przekracza możliwości pojazdu
* naprawi praktycznie wszystko
* nanowspomagany, bardzo szybki

### Czego chce? (3)

* TAK: duma ze swych dzieł
* TAK: praca i wyniki nad gadaninę
* NIE: dyplomacja nad inżynierię
* NIE: ludzkość odrzuca technikę

### Jak działa? (3)

* TAK: używa maszyn i sprzętu
* TAK: każdy grat można naprawić
* TAK: szybko i pancernie
* NIE: dyplomatycznie gada
* NIE: pozycja i fatałaszki

### Zasoby i otoczenie (3)

* specjalistyczny sprzęt naprawczy
* liczne plany i mapy pojazdów
* serwopancerz próżniowy
* nanitkowe wspomaganie
* przyjazna TAI klasy I

### Co to za archetyp

Konstruktor używający nanitek. Nie tylko naprawia, buduje czy leczy; ma przyspieszone ruchy i pancerz.

### Tagi i przykłady

* SILNY: Konstruktor, Omnimedyk, Walka, Szybkość
* SŁABY: Dyplomacja, Magia
* 

### Przykładowe działania

* wskrzesi grat by zaskoczyć wroga
* skonstruuje golema na szybko
* zbuduje schronienie na pustyni
* świetnie steruje pojazdami
* naprawi wyciek reaktora i przetrwa
