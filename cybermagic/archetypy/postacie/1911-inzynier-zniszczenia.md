---
layout: cybermagic-karta-postaci
categories: archetype
title: "Inżynier zniszczenia"
---

# {{ page.title }}

## Archetyp

### Co robi? (3)

* wie jak bezpiecznie niszczyć
* niezwykle celny na zasięg
* badacz i konstruktor broni

### Czego chce? (3)

* TAK: duży zysk najmniejszym kosztem
* TAK: nowe materiały i anomalie
* NIE: sadyzm i zadawanie bólu
* NIE: marnowanie zasobów

### Jak działa? (3)

* TAK: zastraszanie siłą ognia
* TAK: konstrukcja i użycie broni
* TAK: adaptacja broni do problemu
* NIE: wymuszanie bólem
* NIE: niszczenie bez celu i korzyści

### Zasoby i otoczenie (3)

* ciężki serwopancerz Fulmen
* baterie dział energetycznych
* liczne prototypy broni, pancerzy, materiałów

### Co to za archetyp

Artylerzysta i mistrz kontrolowanej destrukcji. Zniszczenie dlań jest dziełem sztuki. Konstruuje i usprawnia broń.

### Tagi i przykłady

* SILNY: Konstruktor, Niszczenie, Nauka
* SŁABY: Dyplomacja, Agenci
* Optymalizator. Artylerzysta. Badacz.

### Przykładowe działania

* Usprawni broń by przebić tarczę
* Unieruchomi wroga pułapką
* Sprawdzi pochodzenie sprzętu
* Przerazi i złamie demonstracją
* Zbada nieznany byt i go użyje
