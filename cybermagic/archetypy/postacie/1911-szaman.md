---
layout: cybermagic-karta-postaci
categories: archetype
title: "Szaman"
---

# {{ page.title }}

## Archetyp

### Co robi? (3)

* leczy ciało, koi umysł
* widzi to, co trudno zobaczyć
* mediuje i studzi gorące głowy

### Czego chce? (3)

* TAK: harmonia magii, natury, ludzkości
* TAK: pokojowe współistnienie
* NIE: podbijanie świata technologią
* NIE: dominowanie i wymuszanie

### Jak działa? (3)

* TAK: dyplomacja nad siłę
* TAK: delikatność nad zniszczenie
* TAK: kojenie nad wymuszanie
* NIE: krzywdzenie czegokolwiek
* NIE: eksploatacja czy niewola

### Zasoby i otoczenie (3)

* środki psychodeliczne na różne okazje
* znajomości wśród anomalnych istot
* podstawowe środki medyczne

### Co to za archetyp

Widzi więcej niż inni. Dba o harmonię. Lekarz ciała, duszy i umysłu. Doskonały w mediacjach i deeskalacji konfliktu.

### Tagi i przykłady

* SILNY: Społecznie, Percepcja, Magia
* SŁABY: Walka, Technologia

### Przykładowe działania

* Przekonanie kogoś, że trzeba pomóc
* Uspokojenie groźnego potwora
* Znalezienie śladu w echu magii
* Wyleczenie chorego sojusznika
