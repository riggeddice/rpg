---
layout: cybermagic-karta-postaci
categories: archetype
title: "Biostymulator"
---

# {{ page.title }}

## Archetyp

### Co robi? (3)

* udziela pomocy medycznej
* steruje ludźmi przez stymulanty
* aptekarz i truciciel

### Czego chce? (3)

* TAK: utrzymać wszystkich przy życiu
* TAK: podnosić ludzkie możliwości
* TAK: badać nieznane bioformy
* NIE: pozwolić komuś na cierpienie
* NIE: człowieczeństwo nad pragmatyzm

### Jak działa? (3)

* TAK: stymulanty i lekarstwa
* TAK: mutowanie bioform i ludzi
* TAK: przejmuje dowodzenie w kryzysie
* NIE: walka i zadawanie cierpienia
* NIE: tłumaczenie się czy przepraszanie

### Zasoby i otoczenie (3)

* stymulanty, lekarstwa itp
* biosyntezator i narzędzia analityczne
* serwopancerz medyczny
* adaptogen kralotyczny

### Co to za archetyp

Lekarz. Mutator żywych istot. Władca środków chemicznych. Ten, dla którego ciało jest jedynie kanwą.

### Tagi i przykłady

* SILNY: Wsparcie, Badanie, Agenci
* SŁABY: Technika, Pojazdy
* Lekarz. Badacz na bagnach.

### Przykładowe działania

* skutecznie przesłuchuje ofiarę
* udziela pomocy medycznej
* wzmacnia sojuszników
* chmury gazów czy toksyn
* zmienia ludzi czy zwierzęta w agentów
