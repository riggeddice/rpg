---
layout: inwazja-karta-postaci
categories: archetype
title: "Infotancerz"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* master hacker włamujący się i przejmujący kontrolę nad urządzeniami

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: szacunek i chwała, wolność, ciekawość co jest ukryte
* dla innych: merytokracja, walka z Systemem

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* rozbrajanie zabezpieczeń, kontrola kamer, włam
* komputery i informatyka, gubienie śladów
* sterowanie dronami i zdalne

### Zasoby i otoczenie (3)

* dobrej klasy sprzęt komputerowy
* dostęp do darknetów i niejawnych informacji

## Opis

Przykłady:

* Klaudia (Warehouse 13)
