---
layout: inwazja-karta-postaci
categories: archetype
title: "Demonita"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* okultysta skłonny przyzwać demony dla wiedzy i potęgi

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: wiedza, władza i potęga, ciekawość, potwierdzenie racji
* dla innych: społeczeństwo kastowe, kosmiczny porządek

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* charyzma, kuszenie, nawracanie
* przyzywanie demonów, okultystyczne rytuały
* archiwa, poszukiwanie informacji

### Zasoby i otoczenie (3)

* czarna technologia, artefakty i anomalie
* nadnaturalni sojusznicy, zakazane księgi

## Opis

Przykłady:

* Sheaim (Fall From Heaven)
* Bible Black Origin
