---
layout: inwazja-karta-postaci
categories: archetype
title: "Arystokrata"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* nieskazitelny elegant i oficer chroniący aktualnego porządku

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: zachować twarz, władza, ród, pozycja, honor
* dla innych: społeczeństwo kastowe, silni chronią słabych

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* przyciąga uwagę, retoryka i przemowy, etykieta
* pojedynki, onieśmielanie, budzi wrażenie

### Zasoby i otoczenie (3)

* specjalne uprawnienia, luksus, agenci i siedziba
* dobry i drogi sprzęt, kontakty wśród arystokracji

## Opis

Przykłady:

* magowie Świecy
* oficer-arystokrata
