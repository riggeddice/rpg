---
layout: inwazja-karta-postaci
categories: archetype
title: "Reporter niezależny"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* reporter w pogoni za sławą i jak najlepszym ujęciem

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: sława i nagrody, adrenalina i ryzyko, prestiż i fani
* dla innych: mają prawo wiedzieć, zniszczyć hipokryzję

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* pozyskiwanie, obróbka, prezentacja danych
* rozmowy z ludźmi, praca z komputerami
* skradanie się, włamanie, aktorstwo

### Zasoby i otoczenie (3)

* sprzęt śledczy, kamery i transmitery
* uprawnienia prasy, kontakty w organizacjach

## Opis

Przykłady:

* Peter Parker (Spiderman), Lois Lane (Superman)
