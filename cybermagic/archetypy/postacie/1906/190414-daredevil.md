---
layout: inwazja-karta-postaci
categories: archetype
title: "Daredevil"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* Ryzykant z ogniem w oczach zawsze przekraczający granice

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: ryzyko i wyzwanie, zostać legendą
* dla innych: zainspirować ich, pobić rekordy, zniszczyć skostnienie

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* walka, siła i szybkość, nieustraszony i niepowstrzymany
* wzbudza wrażenie, zaskakuje, błyskawiczne reakcje
* nieprzewidywalny, podnosi stawkę

### Zasoby i otoczenie (3)

* narzędzia, liny, stroje maskujące
* grupa "normalsów" którym imponuje
* rywale

## Opis

Przykłady:

* "Cry cry cry" Oceana
