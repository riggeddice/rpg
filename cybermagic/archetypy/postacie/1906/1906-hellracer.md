---
layout: inwazja-karta-postaci
categories: archetype
title: "Hellracer"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Miłośnik wyścigów i dopakowywania swojego ścigacza; na krawędzi prawa.

### Czego pragnie a nie ma (3)

* najszybszy i najlepszy; podziw i chwała
* godni rywale; pomaga innym w rozwoju
* doceniony sport; chce szacunku

### Jak działa najskuteczniej (3)

* szalony pilot: imponuje, szybki, manewry
* inspiracja: wzbudza wrażenie, prowokuje
* naprawia: przebudowa, włam do pojazdów

### Zasoby i otoczenie (3)

* wybitny ścigacz, odporniejszy i szybszy
* grupa fanów, świetni rywale, uznany mistrz
* sprzęt do włamań

## Opis

Przykłady:

* Initial D
