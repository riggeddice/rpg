---
layout: inwazja-karta-postaci
categories: archetype
title: "Eris Władczyni"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* wojownik kultury siejący niezgodę by rządzić

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: władza i posłuch, grupa dookoła
* dla innych: chaos i nieufność, skupienie wokół Eris

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* niszczenie reputacji, plotki, szantażowanie
* organizowanie grup, agitacja, niezgoda
* aktorstwo, manipulowanie

### Zasoby i otoczenie (3)

* profesjonalni podżegacze, zwolennicy Sprawy
* plotki, materiały do szantażu

## Opis

Przykłady:

* GamersGate
