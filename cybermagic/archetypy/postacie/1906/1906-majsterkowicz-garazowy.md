---
layout: inwazja-karta-postaci
categories: archetype
title: "Majsterkowicz garażowy"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Tworzy konstrukcje w swoim warsztacie; złom przerabia w narzędzia.

### Czego pragnie a nie ma (3)

* promowanie; niech inni chcą robić to samo
* budować przydatne rzeczy; niech używają
* źródło maszyn i surowców; dla możnowości

### Jak działa najskuteczniej (3)

* dostosowanie: przerabianie prowizorki
* naprawa, analiza, używanie urządzeń
* konstrukcja: chemik, mechanik, elektronik

### Zasoby i otoczenie (3)

* dobry, solidny pojazd; prawie czołg
* bezpieczne schronienie i warsztat
* gadżety, chemikalia i narzędzia

## Opis

Przykłady:

* MacGyver
