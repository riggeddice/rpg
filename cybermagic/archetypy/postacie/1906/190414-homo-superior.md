---
layout: inwazja-karta-postaci
categories: archetype
title: "Homo Superior"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* człowiek wspomagany, dużo lepszy - ale kosztem dawnego życia

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: reintegracja z ludźmi, znaleźć swoje miejsce
* dla innych: akceptacja inności, wolność i swoboda

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* silniejszy, szybszy, odporniejszy, lepsza percepcja
* wszystko zauważy, zastraszanie, niemożliwe manewry

### Zasoby i otoczenie (3)

* ciało czarnej technologii, wspomagany umysł
* dostęp do nadnaturalnych zdolności

## Opis

Przykłady:

* Superman
* Metallo
