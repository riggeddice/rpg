---
layout: inwazja-karta-postaci
categories: archetype
title: "Szczur Miejski"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* ignorujący prawo i wbijający nóż w plecy szczur miejski

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: zbierać władzę dobra i sekrety, jak największa siła
* dla innych: każdy dba o siebie, pomagać swoim

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* szantaż, wymuszenia, oszustwa
* walka uliczna, ucieczka, przetrwanie
* skradanie się, przemyt

### Zasoby i otoczenie (3)

* wszędzie kryjówki, ukryta broń
* materiały do szantażu, sekrety
* fałszywki, znajomości w półświatku

## Opis

Przykłady:

* Niles Ferrier
* Groundsel
* Artur Żupan
