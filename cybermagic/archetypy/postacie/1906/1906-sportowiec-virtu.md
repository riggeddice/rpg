---
layout: inwazja-karta-postaci
categories: archetype
title: "Sportowiec Virtu"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Sportowiec w vircie, świetny oficer gildii

### Czego pragnie a nie ma (3)

* mistrzostwo i sława; być na szczycie
* łączyć oba światy; często kolidują
* sponsorzy i przyszłość; co z emeryturą?

### Jak działa najskuteczniej (3)

* virt: komunikacja, szukanie, różne gry
* oficer: taktyka, łagodzenie konfliktów
* rywalizacja: prowokuje, reakcje, spokój

### Zasoby i otoczenie (3)

* szerokie znajomości w virt; fani
* reputacja sportowca, z dobrą drużyną

## Opis

Przykłady:

* Sword Art Online
