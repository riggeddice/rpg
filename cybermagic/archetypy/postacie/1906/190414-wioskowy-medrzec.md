---
layout: inwazja-karta-postaci
categories: archetype
title: "Wioskowy Mędrzec"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* szanowany mędrzec; kiedyś awanturnik, ma życiową wiedzę

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: część wspólnoty, pozytywna harmonia, nie szkodzić
* dla innych: dobro ponad prawo, ład, pokój, wspólnota

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* niepozorny, uważny, potrafi dowodzić
* uważny, widzi ślady, dobrze ocenia charaktery

### Zasoby i otoczenie (3)

* ogromny szacunek w okolicy, mnóstwo przysług
* lokalne zioła i legendy
* zebrane w podróżach artefakty

## Opis

Przykłady:

* Brat Cadfael
* Arivald z Wybrzeża
