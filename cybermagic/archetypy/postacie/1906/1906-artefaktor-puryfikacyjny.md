---
layout: inwazja-karta-postaci
categories: archetype
title: "Artefaktor Puryfikacyjny"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Konstruktor polujący na anomalie i ujarzmiający je w służbie ludzkości

### Czego pragnie a nie ma (3)

* technomagia; łączenie technologii z magią
* ujarzmić świat; ludzie nad Eter, porządek
* kontrola; magia służy lub zniszczona

### Jak działa najskuteczniej (3)

* łowi anomalie: znajduje, unieszkodliwia
* kontroluje: technomagia, słabe punkty
* dominacja: zastrasza, rozkazuje, kłamie
* buduje: artefakcja, pułapki, antymagia

### Zasoby i otoczenie (3)

* lapis i narzędzia neutralizacji magii
* srebro i narzędzia krzywdzenia magii
* sprzęt do artefakcji i budowania rzeczy

## Przykłady

* Grace Hall (Dents)
