---
layout: inwazja-karta-postaci
categories: archetype
title: "Apokalipta"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* mędrzec i kapłan dążący do końca świata i zniszczenia Czegoś

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: zniszczyć znany Świat, gromadzić wyznawców
* dla innych: pokazać im horror świata, kult apokalipsy

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* charyzmatyczny, nawracanie, odbieranie nadziei, terror
* maszyny zagłady, inżynier czarnej technologii

### Zasoby i otoczenie (3)

* czarna technologia, zakazana technika
* wyznawcy, kultyści i naśladowcy
* as w rękawie

## Opis

Przykłady:

* Sheaim (Fall From Heaven)
* Ezekiel Rage (Jonny Quest)
* Raw Le Cruiset (Gundam:Seed)
