---
layout: inwazja-karta-postaci
categories: archetype
title: "Szaman"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Mistyk wykorzystujący zioła i wizje do znalezienia harmonii z nowym światem.

### Czego pragnie a nie ma (3)

* przyszłość; adaptacja ludzi do świata
* harmonia; natura, magia, ludzie, duch
* znaleźć drogę; pomóc znaleźć ją innym

### Jak działa najskuteczniej (3)

* widzi rzeczy: intuicja, energie, uważny
* psychodeliki: koi duszę, pokazuje wizje
* medycyna: leczy, zielarstwo, robi leki
* opanowuje: uspokaja, dowodzi

### Zasoby i otoczenie (3)

* środki psychodeliczne na różne okazje
* znajomości wśród inteligentnych anomalii
* podstawowe środki medyczne

## Opis

Przykłady:

* Indianie (prawdziwi)
