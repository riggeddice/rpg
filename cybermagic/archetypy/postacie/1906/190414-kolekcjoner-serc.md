---
layout: inwazja-karta-postaci
categories: archetype
title: "Kolekcjoner Serc"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* wpływowy uwodziciel zawsze w centrum ważnego ekosystemu

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: podziw i adoracja, zawsze w centrum wydarzeń, na świeczniku
* dla innych: radosne społeczeństwo, skupienie na przyjemnościach

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* włada ludzkimi sercami, oczarowanie, każdy jest wektorem
* budowanie wrażenia, przyciąganie uwagi, zagrzewanie grupy

### Zasoby i otoczenie (3)

* grupa fanów i adoratorów, też wpływowych
* doskonałej klasy stroje i kosmetyki, bogactwo i luksus

## Opis

Przykłady:

* Nicara (Scooby Doo)
* Poison Ivy (Batman)
