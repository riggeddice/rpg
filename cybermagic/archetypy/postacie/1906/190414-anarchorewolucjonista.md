---
layout: inwazja-karta-postaci
categories: archetype
title: "Anarchorewolucjonista"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* anarchista próbujący zniszczyć System przemocą i propagandą

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: zniszczyć narzędzia kontroli, absolutna wolność, więcej popleczników
* dla innych: uwolnić ich od Systemu, otworzyć im oczy

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* walka, zastraszanie, zmuszanie innych
* bomby, zniszczenia, terror
* szczur miejski z tysiącem dróg ucieczki

### Zasoby i otoczenie (3)

* skrytki z narzędziami terroru i bronią
* mały gang, porządny motor i sprzęt
* chroniona baza i fabrykator

## Opis

Przykłady:

* Mad Stan (Batman Beyond)
* Disturbed "Land of Confusion"
