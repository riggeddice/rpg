---
layout: inwazja-karta-postaci
categories: archetype
title: "Bibliotekarz Veritas"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Bibliotekarz usprawniający świat wiedzą, komputerami i procesami.

### Czego pragnie a nie ma (3)

* głód usprawniania; nieefektywność to zło
* szukanie anomalii; trzeba zrozumieć świat
* gromadzenie wiedzy; jedyna droga postępu

### Jak działa najskuteczniej (3)

* znajduje: ślady, wiedzę z książek i virtu
* rozwiązuje: zagadki, dopasowuje narzędzia
* zarządza: przyspiesza pracę, koordynuje

### Zasoby i otoczenie (3)

* stałe połączenie z virtem
* liczne księgi i dostęp do bibliotek

## Opis

Przykłady:

* ?
