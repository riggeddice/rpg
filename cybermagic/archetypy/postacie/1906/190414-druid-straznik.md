---
layout: inwazja-karta-postaci
categories: archetype
title: "Druid Strażnik"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* strażnik natury dbający o naturalny cykl życia

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: harmonia, usuwać anomalie, ratować zwierzęta
* dla innych: czystość biologiczna, ludzie w zgodzie z naturą

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* survival, pułapki, tropienie
* rośliny, zioła, trucizny
* natura domem druida, puryfikacja

### Zasoby i otoczenie (3)

* rośliny, zwierzęta, kryjówki
* biolab, naturalne puryfikanty

## Opis

Przykłady:

* Miraculix / Getafix / Panoramix (Asterix)
* Poison Ivy (Batman)
