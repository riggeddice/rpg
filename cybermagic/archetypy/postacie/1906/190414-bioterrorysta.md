---
layout: inwazja-karta-postaci
categories: archetype
title: "Bioterrorysta"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* bioinżynier wykorzystujący plagi i Skażeńców do zdobycia posłuchu

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: nowe próbki i eksperymenty, bojownik Sprawy
* dla innych: oddać się Sprawie, porządek przez strach

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* plagi, trucizny, bioskażeńcy
* biomodyfikacje, biowzmacniacze
* badania, synteza, przeciwdziałanie

### Zasoby i otoczenie (3)

* narkotyki, biowspomagania i mutageny
* potężny biolab, plagi, agenci

## Opis

Przykłady:

* Doctor Viper (Swat Kats)
