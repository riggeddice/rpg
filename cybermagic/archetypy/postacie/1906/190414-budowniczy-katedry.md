---
layout: inwazja-karta-postaci
categories: archetype
title: "Budowniczy Katedry"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* konstruktor chcący zbudować coś wielkiego, zostawić ślad

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: zostawić coś wielkiego po sobie, robić solidną robotę
* dla innych: spójne społeczeństwo, ład i porządek

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* świetny konstruktor i mechanik, problemy konstrukcji
* metodyczny i systematyczny, działa w trudnych warunkach
* omija biurokrację

### Zasoby i otoczenie (3)

* solidni fachowcy, materiały i sprzęt konstrukcyjny
* ma historię dobrej roboty

## Opis

Przykłady:

* .
