---
layout: inwazja-karta-postaci
categories: archetype
title: "Fortyfikator"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* obrońca specjalizujący się w niezdobywalnych schronieniach

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: bezpieczny dom, punkt stabilizacji wszystkiego
* dla innych: ochrona absolutna, odzyskać miejsce dla swoich

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* podnoszenie morale, taktyka, prowokowanie przeciwnika
* pułapki, działka i defensywy, fortyfikowanie bazy

### Zasoby i otoczenie (3)

* działka, miny, pułapki, wiedza o przeciwnikach
* podwładni: saperzy, inżynierowie, żołnierze

## Opis

Przykłady:

* .
