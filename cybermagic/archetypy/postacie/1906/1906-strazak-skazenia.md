---
layout: inwazja-karta-postaci
categories: archetype
title: "Strażak Skażenia"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Strażak przeciw anomaliom, forpoczta, często niezależny.

### Czego pragnie a nie ma (3)

* bezpieczeństwo ludzi; niech nikt nie cierpi
* czystość świata; za dużo wypaczeń i Skażenia
* robić rzeczy ważne i dobre; życie jest krótkie

### Jak działa najskuteczniej (3)

* dowodzenie: posłuch, inspiracja
* wiedza i badanie: Skażenie, katastrofy
* ratowanie: niezłomny, szybki, w ogień


### Zasoby i otoczenie (3)

* szybki ścigacz, sprzęt medyczny, broń
* wsparcie oddziału i służb medycznych
* szerokie uprawnienia

## Opis

Przykłady:

* Iria
