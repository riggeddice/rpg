---
layout: inwazja-karta-postaci
categories: archetype
title: "Zbyt genialny mag"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Mag o zbyt dużej mocy i małej kontroli.

### Czego pragnie a nie ma (3)

* kontroli; moc czasem manifestuje się sama
* szacunku i przyjaźni; nie tylko strachu
* przynależeć; nie być tylko narzędziem

### Jak działa najskuteczniej (3)

* magia: adaptuje się, samowzbudna
* percepcja: intuicja, łączenie faktów
* przez uczucia: empatia, straszenie  

### Zasoby i otoczenie (3)

* reputacja straszliwego i niestabilnego
* spontanicznie tworzy anomalie
* chętni do bycia uczniami

## Opis

Przykłady:

* Suspiria (Flipside)
