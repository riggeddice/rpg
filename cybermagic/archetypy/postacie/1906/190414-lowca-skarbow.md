---
layout: inwazja-karta-postaci
categories: archetype
title: "Łowca Skarbów"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* poszukiwacz skarbów radzący sobie w niebezpiecznym terenie

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: wiedza i eksploracja, gromadzenie unikalnych rzeczy, ciekawość
* dla innych: propagowanie wiedzy, podziw i szacunek

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* wszystko zauważa, tajne przejścia, zagadki i łamigłówki, pułapki
* wspinaczka, atletyka, zwinność, survival
* wiedza o przeszłości, praca z danymi i komputerami

### Zasoby i otoczenie (3)

* sprzęt wspinaczkowy, mapy i mnóstwo książek
* asystenci i sojusznicy na wypadek problemów ekspedycji

## Opis

Przykłady:

* Indiana Jones, Lara Croft
