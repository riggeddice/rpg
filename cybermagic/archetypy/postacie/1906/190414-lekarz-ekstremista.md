---
layout: inwazja-karta-postaci
categories: archetype
title: "Lekarz Ekstremista"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* lekarz który nie pozwoli na śmierć nawet za cenę człowieczeństwa

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: uratuję Kogoś, pokonam śmierć
* dla innych: lepsze życie, wyższy stopień ewolucji

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* pierwsza pomoc, leczenie, maszyny medyczne
* autorytet, przejmowanie kontroli, niezłomny
* okultyzm, fuzja bio-X, biokonstrukcja

### Zasoby i otoczenie (3)

* czarna technologia, zakazana technika
* reputacja wybitnie skutecznego

## Opis

Przykłady:

* lekarz-nekromanta lub cybertech
* ojciec ratujący córkę za cenę duszy
