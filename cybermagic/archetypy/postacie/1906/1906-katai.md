---
layout: inwazja-karta-postaci
categories: archetype
title: "Katai"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Mistrz sztuk walki, perfekcyjnie kontrolujący swoje ciało i umysł.

### Czego pragnie a nie ma (3)

* wyzwania; mistrzostwo ciała i umysłu
* ciało ponad narzędzia czy cybernetykę
* stabilność; pomaga innym opanować emocje

### Jak działa najskuteczniej (3)

* sztuki walki: zastraszanie, imponowanie
* ciało: szybki, silny, odporny, precyzja
* stabilizacja: uspokoi, kojenie, niezłomny

### Zasoby i otoczenie (3)

* reputacja wybitnego mistrza
* wewnętrzne zasoby energii: wzmocnienie

## Opis

Przykłady:

* Ryu (Street Fighter Animated)
