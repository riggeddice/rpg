---
layout: inwazja-karta-postaci
categories: archetype
title: "Przyjazny kameleon"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* mimik ma dla każdego taką twarz jaka jest najbardziej lubiana

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: lubiany, podziw, adoracja
* dla innych: wspólnota, radość, poczucie przynależności do grupy

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* czytanie innych, obserwacja, plotki i wiadomości o innych
* aktorstwo, kłamstwa, manipulacja; dla każdego właściwa twarz

### Zasoby i otoczenie (3)

* traktowany jako członek każdej grupy, powszechnie lubiany
* ogromna ilość informacji o każdym

## Opis

Przykłady:

* Maytag (Flipside)
