---
layout: inwazja-karta-postaci
categories: archetype
title: "Magnat na Włościach"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* wpływowy magnat władający dworkiem i włościami

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: władza absolutna, więcej siły i bogactwa, prestiż
* dla innych: zbiór niezależnych księstw, utopia dla swoich poddanych

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* majestat, wzbudzanie wrażenia, posłuch
* dyplomata, negocjator; świetnie zarządza majątkiem

### Zasoby i otoczenie (3)

* poddani, stronnicy, służący
* swoja posiadłość, ziemie i dominujący biznes
* silne wpływy polityczne w okolicy oraz wpływowi znajomi

## Opis

Przykłady:

* Ainz (Overlord)
* Dungeon Keeper (Dungeon Keeper)
