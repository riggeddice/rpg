---
layout: inwazja-karta-postaci
categories: archetype
title: "Anioł Śmierci"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* niepowstrzymany mściciel wzbudzający strach

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: zabić ostatniego wroga, zemsta za Czyn
* dla innych: ostracyzm wrogów, poparcie dla Sprawy

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* niezłomny, niepowstrzymany, efektowna eksterminacja
* wzbudzanie strachu, tropienie, walka, pułapki

### Zasoby i otoczenie (3)

* prasa i reportaże, reputacja potwora
* kombinezon kameleona, broń biała i snajperska
* nieskończona nienawiść

## Opis

Przykłady:

* Cyril Dent (Warzone)
* Phantasm (Batman: Mask of Phantasm)
