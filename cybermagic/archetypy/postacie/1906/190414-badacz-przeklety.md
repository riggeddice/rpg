---
layout: inwazja-karta-postaci
categories: archetype
title: "Badacz Przeklęty"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* naukowiec opętany przez Istotę/Moc, unikający zatracenia

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: pozbyć się klątwy, zachować moc, zrozumieć Istotę
* dla innych: użyć swej mocy by ratować, ograniczać moc Istoty

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* analizuje i odkrywa, widzi czego nikt nie widzi
* archiwa, zrozumienie Istoty, praca z komputerami
* nadnaturalna samokontrola, cierpliwość i odporność

### Zasoby i otoczenie (3)

* wspomaganie Istotą, aura strachu
* na jego tropie: łowcy mocy, wyznawcy Istoty

## Opis

Przykłady:

* Faust
