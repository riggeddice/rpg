---
layout: inwazja-karta-postaci
categories: archetype
title: "Biznesmen z nizin"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* biznesman z nizin, z własnymi produktami, stający na nogi

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: więcej klientów, bezpieczeństwo finansowe
* dla innych: pomóc innym w jego sytuacji, szanse dla każdego

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* żebranie i sępienie, bezpieczny w mieście, nawiązuje znajomości
* walka uliczna, fabrykacja produktów, sprzedaż produktów

### Zasoby i otoczenie (3)

* własny niewielki biznes, odpowiednie fabrykatory, swoje produkty
* znajomości w półświatku, klienci, osoby którym pomógł

## Opis

Przykłady:

* .
