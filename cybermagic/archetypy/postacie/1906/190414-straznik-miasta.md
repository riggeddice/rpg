---
layout: inwazja-karta-postaci
categories: archetype
title: "Strażnik Miasta"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* strażnik miasta lub obszaru, oddany nie prawu ale ludziom i zasadom

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: ustabilizować ten teren, to MOJE miasto
* dla innych: niech wszyscy są bezpieczni, dobro ponad prawo

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* posłuch, zastraszanie, wyciąganie zeznań
* walka uliczna, szybki bieg, zna miasto na wylot
* detektyw oraz wojownik

### Zasoby i otoczenie (3)

* wie kto w mieście jest kim i kto jak działa
* zna każdą kryjówkę i każdy skrót w mieście
* ma uprawnienia do egzekwowania prawa

## Opis

Przykłady:

* Batman (Batman)
* Vimes (Pratchett)
