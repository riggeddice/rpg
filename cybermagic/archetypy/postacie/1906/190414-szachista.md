---
layout: inwazja-karta-postaci
categories: archetype
title: "Szachista"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* elegancki strateg osiągający cel agentami

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: być najlepszym, chronić swoich ludzi
* dla innych: przewidywalność, więcej możliwości

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* znajduje słabe punkty, taktyka, kontrola terenu
* prowokuje przeciwnika, przewiduje ruchy
* wzbudza strach, skryty

### Zasoby i otoczenie (3)

* kompendium wiedzy o przeciwnikach
* oddział agentów, pułapki

## Opis

Przykłady:

* Wielki Admirał Thrawn
* L/Kira
