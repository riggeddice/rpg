---
layout: inwazja-karta-postaci
categories: archetype
title: "Przegrany Paladyn"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* straszliwy wojownik w pełni oddany Sprawie, która już przegrała

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: być symbolem resztek Sprawy, póki walczy - sprawa żyje
* dla innych: zmusić innych do uznania Sprawy, zniszczyć zło, symbol

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* morderczy w walce, upiorna odporność, niepowstrzymany
* przekierowuje uwagę na siebie, inspiruje, przejmuje dowodzenie

### Zasoby i otoczenie (3)

* zasłużona reputacja Symbolu oraz stronnicy Sprawy
* najlepszej klasy sprzęt bojowy,  medyczny, ochronny

## Opis

Przykłady:

* Mal (Firefly)
* Pirotess (Lodoss)
* Grave (Gungrave)
