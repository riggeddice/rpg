---
layout: inwazja-karta-postaci
categories: archetype
title: "Krwawy Baron"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* straszliwy wojownik i mądry tyran rządzący terrorem i siłą

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: więcej władzy, mocy i potęgi, zbudować stalowe imperium
* dla innych: pokój przez tyranię, słudzy stalowego imperium

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* zastraszanie, terror, taktyka, charyzmatyczny przywódca
* walka i otwarta konfrontacja, zarządzanie, łamanie woli

### Zasoby i otoczenie (3)

* wyszkoleni żołnierze, militarna struktura i baza
* najlepszej klasy sprzęt i czarna technologia
* dla swoich: król-bóg

## Opis

Przykłady:

* Megatron (inkarnacje z Frankiem Welkerem)
* Darkseid (Superman)
