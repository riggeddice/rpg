---
layout: inwazja-karta-postaci
categories: archetype
title: "Arlekin Pozytywny"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* Arlekin, mistrz manipulacji niosący radość żartem oraz nożem

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: podziw, radość, adoracja, wrażenia
* dla innych: radość i życzliwość, żadnego okrucieństwa

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* skupianie uwagi, manipulacja, zastraszanie i rozśmieszanie
* akrobacje, zwinność, noże, sztuczki magiczne, iluzje
* bardzo łatwo nawiązuje kontakty

### Zasoby i otoczenie (3)

* ekstrawaganckie stroje, żarty, anegdoty
* odwracacze uwagi, karty, sprzęt do sztuczek

## Opis

Przykłady:

* Maytag (Flipside)
