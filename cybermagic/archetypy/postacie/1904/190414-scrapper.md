---
layout: inwazja-karta-postaci
categories: archetype
title: "Scrapper"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* zrobi helikopter z zapałek i przetrwa wszędzie

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: pełna niezależność, luksus
* dla innych: pomagać sobie nawzajem, twardzi i niezależni

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* konstrukcje z byle czego, adaptacja sprzętu
* chemikalia, survival, pułapki
* ocena sytuacji

### Zasoby i otoczenie (3)

* gadżety-prowizorki, bomby-prowizorki
* solidny pojazd, bezpieczne schronienie

## Opis

Przykłady:

* MacGyver
