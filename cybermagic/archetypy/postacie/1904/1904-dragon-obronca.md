---
layout: inwazja-karta-postaci
categories: archetype
title: "Dragon Obrońca"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* Bardzo potężny wojownik, przyboczny Dowódcy. Obrońca, w pełni oddany swemu Dowódcy, niezłomny i poświęcający się za Dowódcę.

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* chce być doceniany i podziwiany przez Dowódcę, dąży do potęgi by zawsze móc pokonać każdego przeciwnika
* przede wszystkim - chronić Dowódcę, doprowadzić Sprawę do sukcesu

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* przekierowuje ciosy i uwagę na siebie, poświęca siebie za innych, pierwsza linia
* morderczo niebezpieczny w walce, upiorna wręcz odporność na wszystkich liniach

### Zasoby i otoczenie (3)

* zasłużona reputacja Dragona oraz reputacja oddanego oficera
* sprzęt bojowy, ochronny i medyczny bardzo dobrej klasy

## Opis

Przykłady:

* Polly z Flipside
* Pirotess z Lodoss
* Omega Supreme z TF
* Dark Nicole z When Dreams Collide
