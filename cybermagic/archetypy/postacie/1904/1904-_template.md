---
layout: inwazja-karta-postaci
categories: archetype
title: "template_name"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* .

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)
 
* .
* .

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* .
* .

### Zasoby i otoczenie (3)

* .
* .

## Opis

Przykłady:

* .
* .
