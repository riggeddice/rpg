---
layout: inwazja-karta-postaci
categories: archetype
title: "Przyjazny Kameleon"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* Mitrz adaptacji do sytuacji społecznych, który każdemu pokazuje taką twarz jaka jest najbardziej lubiana i najbardziej pasuje

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)
 
* chce być lubiany i kochany, pragnie podziwu i adoracji
* dla innych: wspólnota, radość, poczucie przynależności do grupy

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* czytanie innych, obserwacja społeczna, plotki i wiadomości na temat innych
* aktorstwo i mistrzowska manipulacja; dla każdego ma odpowiednią twarz

### Zasoby i otoczenie (3)

* w wielu grupach traktowany jako "jeden z nich", sympatyczny i lubiany
* ogromna ilość informacji o każdym

## Opis

Przykłady:

* Maytag z Flipside
