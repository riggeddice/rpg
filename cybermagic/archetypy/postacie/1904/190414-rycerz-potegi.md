---
layout: inwazja-karta-postaci
categories: archetype
title: "Rycerz Potęgi"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* wojownik dążący do jak największej siły

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: nigdy bezradny, siła sprzęt umiejętności
* dla innych: merytokracja, kultura wiecznej ewolucji

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* walczy, szybki, silny, wytrzymały, niezłomny
* inspiruje, onieśmiela, zastrasza, przyciąga uwagę

### Zasoby i otoczenie (3)

* rywale - byli i aktualni
* reputacja mistrza walki i ewolucji
* naśladowcy chcący być jak on

## Opis

Przykłady:

* Goku (Dragonball)
* Sinon (Sword Art Online)
