---
layout: inwazja-karta-postaci
categories: archetype
title: "Pająk"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* Pająk-manipulator, z drugiej linii przesuwa pionki i wszystko obserwuje, kierując wydarzeniami

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)
 
* Koniecznie chce kontrolować sytuację. Buduje sobie bezpieczne gniazdo i akumuluje agentów i zabezpieczenia.
* Chce zbudować taki świat, by mógł kontrolować ruchy innych - by wszystko monitorować i o wszystkim wiedzieć. Nie można mieć przed nim sekretów.

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* Doskonały taktyk i manipulator. Czy to kłamstwem czy planem potrafi zmusić innych do zrobienia tego, czego od nich chce.
* Niepozorny, nie zwraca na siebie uwagi. Świetnie ukrywa swe myśli. Działa agentami / dronami, rzadko osobiście. 

### Zasoby i otoczenie (3)

* Drony i agenci. Każdy Pająk działa przez mnóstwo agentów.
* Paranoicznie bezpieczna baza z centrum dowodzenia.

## Opis

Przykłady:

* Jerzy Marduk
