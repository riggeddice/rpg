---
layout: inwazja-karta-postaci
categories: archetype
title: "Femme Fatale"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* Uwodzicielka niemożliwa do odparcia, używająca swego uroku by dostać to, czego chce.

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)
 
* Chcę mieć jak najwięcej przyjaciół, znajomości i osób wykonujących moje żądania.
* Chcę być kochana i podziwiana - im większy wpływ na innych, tym lepiej. Nikt nie może być mi obojętny.

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* Bezpośrednie działanie, przy użyciu uroku i uwodzenia. Dominacja. Włada ludzkimi sercami.
* Budowanie wrażenia, przyciąganie na siebie uwagi, kontrola tłumów.

### Zasoby i otoczenie (3)

* Grupa fanów i adoratorów. Część z nich bogatych i skłonnych spełniać zachcianki.
* Doskonałej klasy stroje i kosmetyki. Dostęp do luksusu i bogactwa.

## Opis

Przykłady:

* Nicara (Scooby Doo)
* Poison Ivy (Batman)
* Succubus
