---
layout: inwazja-karta-postaci
categories: archetype
title: "Trefniś"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* Zwinny i wygadany trefniś potrafi skutecznie przyciągnąć uwagę, zirytować lub rozśmieszyć żartem i zrobić niezłe show.

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* Chcę podziwu dla siebie oraz radości dla moich towarzyszy. Chcę adoracji i uwagi.
* Więcej radości i życzliwości na świecie. Żadnego brutalnego okrucieństwa - spryt może rozwiązać każdy problem.

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* Zwinność, zręczność, akrobacje, sztuczki magiczne.
* Rozładowywanie napięcia, żarty i przyciąganie uwagi na siebie. Trochę manipulacji.

### Zasoby i otoczenie (3)

* Przedmioty służące do sztuczek magicznych. Karty, różdżki, ekstrawaganckie stroje. Mikstury hukowe.
* Stroje - bardzo ekstrawaganckie i przyciągające uwagę. I cekiny.

## Opis

Przykłady:

* Maytag z Flipside (+ Supernowa)
* Julian Krukowicz.
