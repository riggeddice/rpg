---
layout: inwazja-karta-postaci
categories: archetype
title: "Daredevil"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* Agent, działający osobiście, najlepiej operujący w warunkach skrajnie ryzykownych i wymagających ogromnej adrenaliny i stawki.

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)
 
* Takie życie jak mają jest nudne i bezsensowne. Niech się obudzą, niech mnie podziwiają. Niech widzą co można!
* Potrzebne mi zagrożenie, niebezpieczeństwo, ryzyko. Muszę działać na krawędzi. Nie odrzucę zagrożenia.

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* Akrobacje, walka, skradanie się, szybki bieg. Nieustraszony. Niemożliwy do zatrzymania.
* Wzbudza wrażenie, zaskakuje - takiej akcji nikt się nie spodziewa. Szybkie reakcje.

### Zasoby i otoczenie (3)

* Linki, stroje maskujące, narzędzia do wspinania się...
* Ludzie, którym imponuje takim zachowaniem i którzy chcieliby być nim. Ale nie są.

## Opis

Przykłady:

* Indiana Jones (+ Badacz)
* .
