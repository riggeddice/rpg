---
layout: cybermagic-karta-postaci
categories: archetype
title: "Biostymulator"
---

# {{ page.title }}

## Archetyp

### Paradoksalny Koncept

Lekarz, ekspert od dopingu i mutacji żywych istot. Jeśli coś jest organiczne, biostymulator to „usprawni”.
Potrafi udzielić pomocy medycznej lub zatruć, kontrolować żywe istoty lub je wzmacniać.
(Medycyna/Stymulacja + Wiedza/Poznanie).

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Badania, przekształcenia i wzmacnianie istot organicznych. Różnego rodzaju środki.
* ATUT: Identyfikacja, badanie i poznawanie natury nietypowych rzeczy. Jest naukowcem.
* SŁABA: Wie, co należy zrobić, przez co często ignoruje ludzie uczucia. Nie jest najlepszy w przekonywaniu.
* SŁABA: Preferuje wykorzystanie bioform i rzeczy organicznych; zaniedbuje technologiczny aspekt świata.

### O co walczy (3)

* ZA: Podnosić ludzkie możliwości, zrozumieć nieznane.
* ZA: Zdobycie jeszcze jednej nietypowej próbki, pozyskanie jeszcze jednego okazu.
* VS: Nie zgadza się, by ktokolwiek umarł czy ucierpiał na jego warcie.
* VS: Skazać kogoś na cierpienie. Po prostu - nie.

### Znaczące Czyny (3)

* Utrzymał umierającego kolegę przy życiu wprowadzając go w stazę do momentu dojścia do szpitala.
* Na bazie jednego szczura doszedł do tego jak przeciwdziałać zarazie i zsyntetyzował antidotum.
* Dzięki niemu koleżanka wygrała w walce gladiatorów. Nielegalny, trudny do wykrycia doping.

### Kluczowe Zasoby (3)

* COŚ: Przenośny sprzęt badawczy i syntezujący biostruktury. Też medyczny.
* KTOŚ: Osoby, którym pomógł "nielegalnie". Te osoby często informują swoich przyjaciół o takim lekarzu...
* WIEM: Specjalizuje się we wzmacnianiu i leczeniu ludzi; wie o większości bioanomalii dotyczących ludzi.
* OPINIA: Czasami zapomina, że człowieczeństwo też jest warte zachowania, idzie tak daleko w biomodyfikacjach.
