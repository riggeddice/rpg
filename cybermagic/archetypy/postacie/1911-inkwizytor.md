---
layout: cybermagic-karta-postaci
categories: archetype
title: "Inkwizytor"
---

# {{ page.title }}

## Archetyp

### Co robi? (3)

* odkrywa prawdę ze śladów
* zastrasza i przesłuchuje
* walczy na krótki zasięg

### Czego chce? (3)

* TAK: poznać prawdę i winnego
* TAK: zrozumieć zło, spojrzeć w Otchłań
* NIE: korupcji i zdrajcom
* NIE: pozwoli odejść winnemu

### Jak działa? (3)

* TAK: wymusza terrorem
* TAK: przesłuchanie i śledztwa
* TAK: manipulacja i subtelność
* NIE: skrzywdzenie osoby niewinnej
* NIE: zatrzyma się zanim zrozumie

### Zasoby i otoczenie (3)

* serwopancerz ochronny i broń boczna
* wsparcie wydziału wewnętrznego
* informatorzy w półświatku
* reputacja bezwzględnego

### Co to za archetyp

Nieubłagany agent Praworządności, który nie spocznie póki nie znajdzie swojej ofiary i nie pozna Prawdy.

### Tagi i przykłady

* SILNY: Uprawnienia, Detektyw, Strach, Walka
* SŁABY: Dyplomacja, Technologia, Wybaczanie
* Detektyw. Nocny łowca. Wydział Wewnętrzny.

### Przykładowe działania

* Łączy fakty - co się stało
* Przesłuchuje swoją ofiarę
* Powstrzymuje walkę rozkazem
* Pokonuje kilka osób w walce
* Chroni innych przed Otchłanią
