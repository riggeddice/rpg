---
layout: inwazja-karta-postaci
categories: archetype
title: "Katai"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Mistrz sztuk walki, perfekcyjnie kontrolujący swoje ciało i umysł.

### Jak działa najskuteczniej / Dokonanie (3)

* sztuki walki: zastrasza, imponuje, walczy
* ciało: szybki, silny, odporny, precyzyjny
* stabilizuje: uspokoi, kojenie, niezłomny

### Zasoby i otoczenie (3)

* reputacja wybitnego mistrza
* wewnętrzne zasoby energii: wzmocnienie

## Przykłady (0)

* Ryu (Street Fighter)
