---
layout: inwazja-karta-postaci
categories: archetype
title: "Sportowiec Virtu"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Sportowiec w vircie, w grach grupowych. Świetny oficer gildii.

### Jak działa najskuteczniej / Dokonanie (3)

* dowodzi: taktyka, łagodzi, koi, prowokuje
* żyje w virt: komunikacja, różne gry
* walczy w virt: reakcje, spokój, zasady

### Zasoby i otoczenie (3)

* szerokie znajomości w virt; fani
* reputacja sportowca, z dobrą drużyną

## Przykłady (0)

* Sword Art Online
