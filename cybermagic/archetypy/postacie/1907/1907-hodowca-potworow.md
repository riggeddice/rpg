---
layout: inwazja-karta-postaci
categories: archetype
title: "Hodowca Potworów"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Hodowca potworów użytkowych; specjalizuje się w łapaniu, weterynarii i tresowaniu takich istot.

### Jak działa najskuteczniej / Dokonanie (3)

* potwory: weterynarz, hodowca, środowisko
* łapanie: pułapki, przynęty, unikanie
* biurokracja: ofensywnie i defensywnie
* badanie: Skażenia, terenu, istot

### Zasoby i otoczenie (3)

* istoty użytkowe: glukszwajny, oczkowce...
* posiadłość na uboczu z terenami
* pancerny pojazd transportowo-więzienny

## Przykłady (0)

* Olga Myszeczka
