---
layout: inwazja-karta-postaci
categories: archetype
title: "Podżegacz"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Podżegacz jest symbolem, rozpalającym rewolucję i swoją Sprawę z pierwszej linii. Trochę mędrzec, trochę kapłan.

### Jak działa najskuteczniej / Dokonanie (3)

* dowodzi: porywa tłumy, emocje, symbol
* przegaduje: retoryka, fakty, presja
* infiltruje: maskowanie, skradanie

### Zasoby i otoczenie (3)

* wyznawcy, kultyści i naśladowcy
* liczne dowody i fakty na swą tezę
* efektowne stroje ORAZ narzędzia ukrycia
* sympatycy w wielu znaczących miejscach

## Przykłady (0)

* Ezekiel Rage (Jonny Quest)
* V (V for Vendetta)
* Kudelia (Gundam Iron Blooded Orphans)
