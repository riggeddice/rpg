---
layout: inwazja-karta-postaci
categories: archetype
title: "Koci Włamywacz"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

42 znaki na kategorię dla jednej linijki. Jeszcze dotąd można się dostać.

### Jak działa najskuteczniej / Dokonanie (3)

* relokacja: cisza, akrobacje, wspinaczka
* ukrywanie się: kryjówki, skradanie
* infiltracja: włam, zabezpieczenia
* zauroczenie: gracja, urok, kłamanie

### Zasoby i otoczenie (3)

* 40 znaków na kategorię tutaj; aż dotąd...

## Przykłady (0)

* Catwoman (Batman)
* Arsene Lupin (Castle Cagliostro)
* Carmen Sandiego (Carmen Sandiego)
