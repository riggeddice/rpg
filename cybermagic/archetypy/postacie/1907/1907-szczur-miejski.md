---
layout: inwazja-karta-postaci
categories: archetype
title: "Szczur Miejski"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Rzezimieszek perfekcyjnie znający miasto i wbijający nóż w plecy gdy nikt się nie spodziewa.

### Jak działa najskuteczniej / Dokonanie (3)

* wymusza: szantaż, zastraszanie
* ocenia: przedmiotów, wyczucie słabości
* walczy: broń ukryta, walka uliczna
* ulica: skrytki, bieg, skradanie, włam

### Zasoby i otoczenie (3)

* wszędzie kryjówki, ukryta broń
* materiały do szantażu, sekrety
* fałszywki, znajomości w półświatku

## Przykłady (0)

* Niles Ferrier
* Groundsel
* Artur Żupan
