---
layout: inwazja-karta-postaci
categories: archetype
title: "Majsterkowicz"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Majsterkowicz-wynalazca. Tworzy konstrukcje w swoim warsztacie.

### Jak działa najskuteczniej / Dokonanie (3)

* dostosowuje: pojazdy, sprzęt, pojazdy
* buduje: chemik, mechanik, elektronik
* używa urządzeń: praktycznie wszystko
* naprawia: praktycznie wszystko

### Zasoby i otoczenie (3)

* dobry, solidny pojazd; prawie czołg
* bezpieczne schronienie i warsztat
* gadżety, chemikalia i narzędzia
* złom, rzeczy normalnie niebezpieczne

## Przykłady (0)

* MacGyver
