---
layout: inwazja-karta-postaci
categories: archetype
title: "DJ, podziemny"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

DJ z podziemnego klubu; trafia do młodzieży i sfrustrowanych. Samemu zajmuje się tymi tematami.

### Jak działa najskuteczniej / Dokonanie (3)

* inspiruje: dowodzi, retoryka, muzyką
* oczarowuje: wyczucie nastroju, młodzież
* kontroluje sprzęt: zdalnie, dźwięk
* na ulicy: walka uliczna, biega, chowa się

### Zasoby i otoczenie (3)

* zacna ekipa: włamy, kradzieże, pobicia
* fani: młodzi ludzie z różnych sfer
* sprzęt: nagrania, ale też drony

## Przykłady (0)

* Dracena Diakon
