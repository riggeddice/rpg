---
layout: inwazja-karta-postaci
categories: archetype
title: "Zbyt genialny mag"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Mag o zbyt dużej mocy i małej kontroli. Niesamowita moc, nieświadomie używana.

### Jak działa najskuteczniej / Dokonanie (3)

* wola staje się prawdą: magia samowzbudna
* widzi rzeczy: empatia, intuicja, zagadki
* zastrasza: zwierzęta, ludzi, magów

### Zasoby i otoczenie (3)

* reputacja straszliwego i niestabilnego
* spontanicznie tworzy anomalie
* chętni do bycia uczniami

## Przykłady (0)

* Suspiria (Flipside)
