---
layout: inwazja-karta-postaci
categories: archetype
title: "Advancer"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Eksplorator wrogiego terenu. Łączy technologię, gadanie oraz trochę walki by wbić się i wycofać.

### Jak działa najskuteczniej / Dokonanie (3)

* relokacja: prędkość, bezpieczne miejsce
* przetrwanie: kłamanie, karaluch
* zwiad: ocena, wykrycie, postrzeganie
* infiltracja: włam, elektronika

### Zasoby i otoczenie (3)

* Lekki sprzęt wojskowy i ukrywający
* Łamacze kodów, sprzęt włamywacza
* Granaty dymne / EMP, odwracanie uwagi
* Przenośna TAI zapisująca wszystko

## Przykłady (0)

* Iria
* Tina
