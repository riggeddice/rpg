---
layout: inwazja-karta-postaci
categories: archetype
title: "Strażak Skażenia"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Strażak przeciw anomaliom, forpoczta, często działa niezależnie.

### Jak działa najskuteczniej / Dokonanie (3)

* dowodzi: ma posłuch, inspiruje
* ocenia sytuację: Skażenie, katastrofy
* ratuje: niezłomny, szybki, gna w ogień
* leczy: pierwsza pomoc, odkażanie

### Zasoby i otoczenie (3)

* szybki ścigacz, sprzęt medyczny, broń
* wsparcie oddziału i służb medycznych
* szerokie uprawnienia

## Przykłady (0)

* Iria
