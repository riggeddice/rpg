---
layout: inwazja-karta-postaci
categories: archetype
title: "Szpieg manipulator"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Plotkarz i szpieg podsłuchujący sekrety, kontrolujący plotki i narrację.

### Jak działa najskuteczniej / Dokonanie (3)

* dominuje: szantaż, niszczenie opinii
* manipuluje: odwraca uwagę, puszcza plotki
* szpieguje: podsłuchy, drony, hasła
* włamuje się: komputery, virt, ukrywa się

### Zasoby i otoczenie (3)

* cywilny sprzęt szpiegowski i drony
* ma coś na każdego: sekrecik czy szantaż
* ludzie, którymi buduje/zbiera plotki

## Przykłady (0)

* Jerzy Marduk
