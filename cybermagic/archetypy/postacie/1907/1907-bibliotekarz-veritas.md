---
layout: inwazja-karta-postaci
categories: archetype
title: "Bibliotekarz Veritas"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Bibliotekarz usprawniający świat wiedzą, komputerami i procesami.

### Jak działa najskuteczniej / Dokonanie (3)

* znajduje: ślady, wiedzę z książek i virtu
* rozwiązuje: zagadki, dopasowuje narzędzia
* zarządza: przyspiesza pracę, koordynuje
* przekonuje: demonstrując dowody, fakty

### Zasoby i otoczenie (3)

* stałe połączenie z virtem
* liczne księgi i dostęp do bibliotek
* sprzęt do analizy i porównywania

## Przykłady (0)

* brak
