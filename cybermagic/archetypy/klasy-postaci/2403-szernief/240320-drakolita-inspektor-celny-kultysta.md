digraph Character {
    
    // Core
    node [shape=rect]
    rankdir=LR;
    
    Centrum [label="Savaranin,\nInspektor Celny\nKultysta", shape=ellipse, fontname="bold"];
    {
        rank=same;
        Cechy [label="Cechy,\nwłasności"];
        Wiedza [label="Co wie szczególnego\nJaką wiedzę dostarcza?"];
        Relacje [label="Unikalne Relacje\nZ kim się lubi?"];
        Zasoby [label="Unikalne Zasoby\nDo czego ma dostęp?"];
        Akcje [label="Akcje\nCo robi szczególnie dobrze?"];
        Wpływ [label="Czemu Ci zależy?"];
    }

    // Łączenie węzłów
    Centrum -> Cechy;
    Centrum -> Wiedza;
    Centrum -> Relacje;
    Centrum -> Zasoby;
    Centrum -> Akcje;
    Centrum -> Wpływ;

    // Subcomponents for 'Cechy'
    CechyNode1 [label="Drobny, wątły i oszczędny w ruchach"]
    CechyNode2 [label="Z perspektywy nie-savaran,\ndwóch savaran jest identycznych"]
    CechyNode3 [label="Przekonany, że to Wielki Test\nSaitaera, Władcy Adaptacji"]

    // Links for 'Cechy'
    Cechy -> CechyNode1
    Cechy -> CechyNode2
    Cechy -> CechyNode3
    
    // Subcomponents for 'Wiedza'
    WiedzaNode1 [label="Wie, że magia istnieje;\nwidział nienaturalne rzeczy\nwie trochę jak magia działa"]
    WiedzaNode2 [label="Co na stację wchodzi i wychodzi;\njak dobra krążą po stacji"]
    WiedzaNode3 [label="Gdzie coś najlepiej ukryć"]

    // Links for 'Wiedza'
    Wiedza -> WiedzaNode1
    Wiedza -> WiedzaNode2
    Wiedza -> WiedzaNode3

    // Subcomponents for 'Relacje'
    RelacjeNode1 [label="Karl-724, savarański górnik\n(z frakcji Radykałów Sii)\n(jest miłośnikiem dziwnych kultów)"]
    RelacjeNode2 [label="Anara Grabnik, drakolicka inżynier\n(szefowa komórki kultu Saitaera\nbezpośrednio pod Mawirem)"]
    RelacjeNode3 [label="Mirt-17, savarański oficer ochrony\n(kuzyn; traktuje kamery jak reality show)"]
    RelacjeNode4 [label="Ogólnie, savaranie go nie lubią\nbo nieefektywnie wydatkuje energię"]

    // Links for 'Relacje'
    Relacje -> RelacjeNode1
    Relacje -> RelacjeNode2
    Relacje -> RelacjeNode3
    Relacje -> RelacjeNode4

    // Subcomponents for 'Zasoby'
    ZasobyNode1 [label="kultyści (zwykle mawirowcy, nie tylko)\n z nim gadają i traktują jak maskotkę"]
    ZasobyNode2 [label="dane o ruchach dóbr i ludzi;\nzużycie, import / eksport..."]
    ZasobyNode3 [label="narzędzia (i dokumenty) oceny i analizy\ndóbr, przedmiotów, rzadkich rzeczy..."]

    // Links for 'Zasoby'
    Zasoby -> ZasobyNode1
    Zasoby -> ZasobyNode2
    Zasoby -> ZasobyNode3

    // Subcomponents for 'Akcje'
    AkcjeNode1 [label="przesłuchiwanie osoby\nktóra nie chce mówić prawdy"]
    AkcjeNode2 [label="badanie podejrzanej rzeczy\npod kątem anomalii, dziwności, wartości"]
    AkcjeNode3 [label="dokładne szukanie kryjówek,\nprzejść, rzeczy niepasujących"]
    AkcjeNode4 [label="sianie terroru wśród turystów"]

    // Links for 'Akcje'
    Akcje -> AkcjeNode1
    Akcje -> AkcjeNode2
    Akcje -> AkcjeNode3
    Akcje -> AkcjeNode4

    // Subcomponents for 'Wpływ'
    WpływNode1 [label="to Wielki Test Saitaera;\nczy poradzimy sobie SAMI"]
    WpływNode2 [label="to okazja udowodnienia, że\nnasze działania są lepsze niż 'obcy'"]
    WpływNode3 [label="na tej stacji mam rodzinę\noraz przyjaciół"]

    // Links for 'Wpływ'
    Wpływ -> WpływNode1
    Wpływ -> WpływNode2
    Wpływ -> WpływNode3
}
