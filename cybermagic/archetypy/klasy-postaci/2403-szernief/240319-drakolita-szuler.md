digraph Character {
    
    // Core
    node [shape=rect]
    rankdir=LR;
    
    Centrum [label="Drakolita\nSzuler,\nhazardzista,\nprzemytnik", shape=ellipse, fontname="bold"];
    {
        rank=same;
        Cechy [label="Cechy,\nwłasności"];
        Wiedza [label="Co wie szczególnego\nJaką wiedzę dostarcza?"];
        Relacje [label="Unikalne Relacje\nZ kim się lubi?"];
        Zasoby [label="Unikalne Zasoby\nDo czego ma dostęp?"];
        Akcje [label="Akcje\nCo robi szczególnie dobrze?"];
        Wpływ [label="Czemu Ci zależy?"];
    }

    // Łączenie węzłów
    Centrum -> Cechy;
    Centrum -> Wiedza;
    Centrum -> Relacje;
    Centrum -> Zasoby;
    Centrum -> Akcje;
    Centrum -> Wpływ;

    // Subcomponents for 'Cechy'
    CechyNode1 [label="Drakolita: zmodyfikowany genetycznie\nolśniewający, uroczy, SZYBKI - jak wąż"]
    CechyNode2 [label="Przyciąga uwagę i nie wygląda groźnie;\nale jest bardzo niebezpieczny"]

    // Links for 'Cechy'
    Cechy -> CechyNode1
    Cechy -> CechyNode2
    
    // Subcomponents for 'Wiedza'
    WiedzaNode1 [label="Gdzie są najlepsze kryjówki? Gdzie nikogo nie ma?\nGdzie nie poruszają się patrole?"]
    WiedzaNode2 [label="Kto miał problemy z prawem / innymi słabościami?\nJak zaszantażować / wpłynąć na różne osoby?"]
    WiedzaNode3 [label="O czym mówi się w półświatku / w miejscach\nuważanych za 'nieważne' przez możnych?"]
    WiedzaNode4 [label="Gdzie robi się bimber / robi eksperymenty?\nOgólnie, co dzieje się na stacji o czym 'nikt' nie wie."]

    // Links for 'Wiedza'
    Wiedza -> WiedzaNode1
    Wiedza -> WiedzaNode2
    Wiedza -> WiedzaNode3
    Wiedza -> WiedzaNode4

    // Subcomponents for 'Relacje'
    RelacjeNode1 [label="Dorion Fughar, drakolita i prawa ręka Mawira\n(gdy oszukujesz, dajesz mu sporo zarobić)"]
    RelacjeNode2 [label="Firtea Sermarin, bogata turystka\n(uważa Cię za najlepszego przyjaciela na Stacji)"]
    RelacjeNode3 [label="Nie jest lubiany przez Ochronę Stacji\nani przez bardziej 'praworządne' grupy"]

    // Links for 'Relacje'
    Relacje -> RelacjeNode1
    Relacje -> RelacjeNode2
    Relacje -> RelacjeNode3

    // Subcomponents for 'Zasoby'
    ZasobyNode1 [label="Turyści, imprezy i miejsca,\ngdzie normalnie są pieniądze"]
    ZasobyNode2 [label="Przemycona broń, narkotyki i rzeczy niebezpieczne\nktórych nie powinno być na stacji"]
    ZasobyNode3 [label="Jest jak w domu i wśród Teraquid (najemników)\njak i wśród mawirowców"]

    // Links for 'Zasoby'
    Zasoby -> ZasobyNode1
    Zasoby -> ZasobyNode2
    Zasoby -> ZasobyNode3

    // Subcomponents for 'Akcje'
    AkcjeNode1 [label="Doskonale wyczuwa uczucia drugiej osoby;\nświetnie manipuluje ludźmi"]
    AkcjeNode2 [label="Znajduje słabe punkty innych;\nw sam raz do szantażu lub handlu"]
    AkcjeNode3 [label="Drobiazgowo znajduje gdzie warto coś ukryć;\npotrafi coś ukryć lub ujawnić rzeczy ukryte"]

    // Links for 'Akcje'
    Akcje -> AkcjeNode1
    Akcje -> AkcjeNode2
    Akcje -> AkcjeNode3

    // Subcomponents for 'Wpływ'
    WpływNode1 [label="Poza tą Stacją jesteś poszukiwany. Tu nikt nie pyta.\nNAWET OCHRONA nie pyta."]
    WpływNode2 [label="Anomalia sprawia, że nie możesz zarabiać\nani normalnie żyć. Trzeba to rozwiązać."]

    // Links for 'Wpływ'
    Wpływ -> WpływNode1
    Wpływ -> WpływNode2
}
