digraph Character {
    
    // Core
    node [shape=rect]
    rankdir=LR;
    
    Centrum [label="Biosynt\n Medyczny\n(android)", shape=ellipse, fontname="bold"];
    {
        rank=same;
        Cechy [label="Cechy,\nwłasności"];
        Wiedza [label="Co wie szczególnego\nJaką wiedzę dostarcza?"];
        Relacje [label="Unikalne Relacje\nZ kim się lubi?"];
        Zasoby [label="Unikalne Zasoby\nDo czego ma dostęp?"];
        Akcje [label="Akcje\nCo robi szczególnie dobrze?"];
        Wpływ [label="Czemu Ci zależy?"];
    }

    // Łączenie węzłów
    Centrum -> Cechy;
    Centrum -> Wiedza;
    Centrum -> Relacje;
    Centrum -> Zasoby;
    Centrum -> Akcje;
    Centrum -> Wpływ;

    // Subcomponents for 'Cechy'
    CechyNode1 [label="Krystaliczna biotech-struktura:\nSilny, szybki, odporny, wytrwały"]
    CechyNode2 [label="Doskonałe zmysły i sensory\nwidzi rzeczy niewidoczne"]
    CechyNode3 [label="Niemożliwy do zarażenia,\nnieludzki umysł"]

    // Links for 'Cechy'
    Cechy -> CechyNode1
    Cechy -> CechyNode2
    Cechy -> CechyNode3
    
    // Subcomponents for 'Wiedza'
    WiedzaNode1 [label="Zna tereny gdzie nikt nie chodzi\nWie jak przetrwać w skrajnych warunkach"]
    WiedzaNode2 [label="Pozyskuje wiedzę o stanie załogi\nSystemy medyczne i raporty"]
    
    // Links for 'Wiedza'
    Wiedza -> WiedzaNode1
    Wiedza -> WiedzaNode2
    
    // Subcomponents for 'Relacje'
    RelacjeNode1 [label="Artur Tavit, arystokrata\n(który pomaga integrować biosynty)"]
    RelacjeNode2 [label="Delgado Vitriol, biosynt bojowy\n(który unika i nienawidzi ludzi)"]
    RelacjeNode3 [label="Dorion Fughar, gladiator-mawirowiec\nniejeden raz składał jego i innych na arenie"]
    RelacjeNode4 [label="Niewiele zaufania na stacji;\nbiosynty są PODEJRZANE"]

    // Links for 'Relacje'
    Relacje -> RelacjeNode1
    Relacje -> RelacjeNode2
    Relacje -> RelacjeNode3
    Relacje -> RelacjeNode4

    // Subcomponents for 'Zasoby'
    ZasobyNode1 [label="Akceptowany przez półświatek i mawirowców;\nbędą z nim rozmawiać i dopuszczą do swoich"]
    ZasobyNode2 [label="Dostęp do systemów medycznych;\nnikt nie przejmuje się że coś się dowie"]
    ZasobyNode3 [label="Ignorowany z perspektywy politycznej;\ngdzie nie wejdzie, ignorują, jak 'odkurzacz'"]

    // Links for 'Zasoby'
    Zasoby -> ZasobyNode1
    Zasoby -> ZasobyNode2
    Zasoby -> ZasobyNode3

    // Subcomponents for 'Akcje'
    AkcjeNode1 [label="Bada i ocenia stan fizyczny i psychiczny"]
    AkcjeNode2 [label="Porusza się w sposób niezauważany przez innych"]
    AkcjeNode3 [label="Obserwuje ślady i zauważa rzeczy niewidoczne"]

    // Links for 'Akcje'
    Akcje -> AkcjeNode1
    Akcje -> AkcjeNode2
    Akcje -> AkcjeNode3

    // Subcomponents for 'Wpływ'
    WpływNode1 [label="Więcej pacjentów i podejrzeń\nto mniej ludzi i więcej problemów"]
    WpływNode2 [label="Biosynty są podejrzane,\n że to ich wina (nie wiadomo jak)"]
    WpływNode3 [label="Integracja ludzi i biosyntów\njest bardzo świeża i delikatna..."]
    
    // Links for 'Wpływ'
    Wpływ -> WpływNode1
    Wpływ -> WpływNode2
    Wpływ -> WpływNode3
}
