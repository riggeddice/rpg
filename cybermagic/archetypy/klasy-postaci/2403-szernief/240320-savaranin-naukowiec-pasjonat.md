digraph Character {
    
    // Core
    node [shape=rect]
    rankdir=LR;
    
    Centrum [label="Savaranin,\nNaukowiec\nPasjonat", shape=ellipse, fontname="bold"];
    {
        rank=same;
        Cechy [label="Cechy,\nwłasności"];
        Wiedza [label="Co wie szczególnego\nJaką wiedzę dostarcza?"];
        Relacje [label="Unikalne Relacje\nZ kim się lubi?"];
        Zasoby [label="Unikalne Zasoby\nDo czego ma dostęp?"];
        Akcje [label="Akcje\nCo robi szczególnie dobrze?"];
        Wpływ [label="Czemu Ci zależy?"];
    }

    // Łączenie węzłów
    Centrum -> Cechy;
    Centrum -> Wiedza;
    Centrum -> Relacje;
    Centrum -> Zasoby;
    Centrum -> Akcje;
    Centrum -> Wpływ;

    // Subcomponents for 'Cechy'
    CechyNode1 [label="savaranin: niepozorny, łatwy do przeoczenia"]
    CechyNode2 [label="umie używać WSZYSTKICH\nsprzętów na Stacji; survivalista"]
    CechyNode3 [label="uczynny i lubiany przez kolegów\npracowity i pełen pasji"]

    // Links for 'Cechy'
    Cechy -> CechyNode1
    Cechy -> CechyNode2
    Cechy -> CechyNode3
    
    // Subcomponents for 'Wiedza'
    WiedzaNode1 [label="dostęp do danych naukowych\nz Szernief i z innych baz danych"]
    WiedzaNode2 [label="jak interpretować 'coś takiego'\nnawet jak to nie ma sensu"]
    WiedzaNode3 [label="jak pracować z magią i anomaliami;\nspotkał się z anomaliami w pracy"]

    // Links for 'Wiedza'
    Wiedza -> WiedzaNode1
    Wiedza -> WiedzaNode2
    Wiedza -> WiedzaNode3

    // Subcomponents for 'Relacje'
    RelacjeNode1 [label="Elwira Barknis, jego ciekawska szefowa\n(zawsze szuka przewag politycznych i naukowych)"]
    RelacjeNode2 [label="Markus Yarlow, popularyzator i PR\n(wie o rozrywkach i zabawia turystów)"]
    RelacjeNode3 [label="Karim Szepot, weteran z Teraquid\n(lubi wymieniać opowieści)"]

    // Links for 'Relacje'
    Relacje -> RelacjeNode1
    Relacje -> RelacjeNode2
    Relacje -> RelacjeNode3

    // Subcomponents for 'Zasoby'
    ZasobyNode1 [label="Zewnętrzne laboratorium biologiczne\n(ale są tu też inne laboratoria)"]
    ZasobyNode2 [label="Zaawansowany przenośny sprzęt badawczy\ni sensory, też rzeczy do 'zamknięcia' czegoś."]
    ZasobyNode3 [label="Odczynniki i rzeczy do niszczenia\nniebezpiecznych eksperymentów"]

    // Links for 'Zasoby'
    Zasoby -> ZasobyNode1
    Zasoby -> ZasobyNode2
    Zasoby -> ZasobyNode3

    // Subcomponents for 'Akcje'
    AkcjeNode1 [label="Głęboka analiza obiektu / sytuacji;\nteż: budowanie antidotum"]
    AkcjeNode2 [label="Ciekawie opowiada i zaciekawia\nrozmówcę do tego co chce osiągnąć"]
    AkcjeNode3 [label="Odpowiednią dokumentacją załatwi\nproblemy biurokratyczne i co trzeba"]

    // Links for 'Akcje'
    Akcje -> AkcjeNode1
    Akcje -> AkcjeNode2
    Akcje -> AkcjeNode3

    // Subcomponents for 'Wpływ'
    WpływNode1 [label="Można polegać tylko na siłach swojej załogi,\nnie na ludziach 'z zewnątrz'"]
    WpływNode2 [label="Optymizm: nauka służy do\nzrozumienia niezrozumiałego"]
    WpływNode3 [label="Ci ludzie przygarnęli go\nw najgorszych chwilach jego życia."]

    // Links for 'Wpływ'
    Wpływ -> WpływNode1
    Wpływ -> WpływNode2
    Wpływ -> WpływNode3
}
