digraph Character {
    
    // Core
    node [shape=rect]
    rankdir=LR;
    
    Centrum [label="Arystokrata\nPopularyzator,\nDyplomata", shape=ellipse, fontname="bold"];
    {
        rank=same;
        Cechy [label="Cechy,\nwłasności"];
        Wiedza [label="Co wie szczególnego\nJaką wiedzę dostarcza?"];
        Relacje [label="Unikalne Relacje\nZ kim się lubi?"];
        Zasoby [label="Unikalne Zasoby\nDo czego ma dostęp?"];
        Akcje [label="Akcje\nCo robi szczególnie dobrze?"];
        Wpływ [label="Czemu Ci zależy?"];
    }

    // Łączenie węzłów
    Centrum -> Cechy;
    Centrum -> Wiedza;
    Centrum -> Relacje;
    Centrum -> Zasoby;
    Centrum -> Akcje;
    Centrum -> Wpływ;

    // Subcomponents for 'Cechy'
    CechyNode1 [label="Osoba poważana na stacji, młoda i bogata\nOsoby 'z wyższej klasy' z Tobą rozmawiają"]
    CechyNode2 [label="Osoba spokrewniona z Inwestorami Stacji"]


    // Links for 'Cechy'
    Cechy -> CechyNode1
    Cechy -> CechyNode2
    
    // Subcomponents for 'Wiedza'
    WiedzaNode1 [label="O czym mówi się 'na górze'? Co jest ważnego?\nJak wygląda ekonomia Stacji i co Stację martwi?"]
    WiedzaNode2 [label="Jak działać mniej zgodnie z prawem, ale NADAL\nbez zbyt wielkich konsekwencji"]
    WiedzaNode3 [label="Co się fajnego dzieje w okolicy i jakie rozrywki?\n Co warto zobaczyć? Co się dzieje? Ogólnie - plotki."]

    // Links for 'Wiedza'
    Wiedza -> WiedzaNode1
    Wiedza -> WiedzaNode2
    Wiedza -> WiedzaNode3

    // Subcomponents for 'Relacje'
    RelacjeNode1 [label="Aerina Cavalis, arystokratka, popularyzuje Stację\n(ona COŚ WIE o magii, miała z nią jakieś problemy?)"]
    RelacjeNode2 [label="Artur Tavit, arystokrata, integrujący biosynty ze Stacją\n(on MA KONTAKTY z biosyntami)"]
    RelacjeNode3 [label="Elwira Barknis, naukowiec, ekspertka od roślin w kosmosie\n(ona miała jakieś problemy i ma świetne laboratorium)"]
    RelacjeNode4 [label="Mawir Hong, szef gangu, drakolita DEWASTATOR\n(spotkał się z jakimś potworem i go ZJADŁ. Serio.)"]

    // Links for 'Relacje'
    Relacje -> RelacjeNode1
    Relacje -> RelacjeNode2
    Relacje -> RelacjeNode3
    Relacje -> RelacjeNode4

    // Subcomponents for 'Zasoby'
    ZasobyNode1 [label="Agenci i służący, którzy przyniosą, podadzą\nlub wykonają prace typu 'obserwuj to'."]
    ZasobyNode2 [label="Jeśli chce z kimś się spotkać i porozmawiać,\nzawsze da się to załatwić"]
    ZasobyNode3 [label="Arystokracie wolno więcej. Niby 'nie wolno'\nale tego prawo normalnie nie egzekwuje."]

    // Links for 'Zasoby'
    Zasoby -> ZasobyNode1
    Zasoby -> ZasobyNode2
    Zasoby -> ZasobyNode3

    // Subcomponents for 'Akcje'
    AkcjeNode1 [label="Możesz wpływać na ruchy grup ludzi i politykę Stacji;\nNa co warto spojrzeć, co warto pominąć"]
    AkcjeNode2 [label="Zawsze masz pieniądze na przekupienie kogoś\ni możesz trochę nagiąć zasady dla sprawy"]
    AkcjeNode3 [label="Skutecznie przekonujesz innych;\nrozładowujesz napięcie\ni uspokajasz tłumy"]
    AkcjeNode4 [label="Przyciągasz uwagę na siebie i swoje słowa\numożliwiając dyskretne działanie innym"]
    AkcjeNode5 [label="Zastraszasz - jeśli ktoś nie zrobi co chce\nto może spotkać go coś złego"]

    // Links for 'Akcje'
    Akcje -> AkcjeNode1
    Akcje -> AkcjeNode2
    Akcje -> AkcjeNode3
    Akcje -> AkcjeNode4
    Akcje -> AkcjeNode5

    // Subcomponents for 'Wpływ'
    WpływNode1 [label="Jest tu śmiertelnie nudno i utopiono w to pieniądze\nTwojej rodziny. Albo to się naprawi, albo bankructwo."]
    WpływNode2 [label="Jeśli NAPRAWDĘ dzieje się tu coś dziwnego\nto warto o tym wiedzieć. Da się to wykorzystać."]

    // Links for 'Wpływ'
    Wpływ -> WpływNode1
    Wpływ -> WpływNode2
}
