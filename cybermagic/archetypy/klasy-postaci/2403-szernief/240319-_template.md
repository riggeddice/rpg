digraph Character {
    
    // Core
    node [shape=rect]
    rankdir=LR;
    
    Centrum [label="WYPEŁNIJ MNIE!!!", shape=ellipse, fontname="bold"];
    {
        rank=same;
        Cechy [label="Cechy,\nwłasności"];
        Wiedza [label="Co wie szczególnego\nJaką wiedzę dostarcza?"];
        Relacje [label="Unikalne Relacje\nZ kim się lubi?"];
        Zasoby [label="Unikalne Zasoby\nDo czego ma dostęp?"];
        Akcje [label="Akcje\nCo robi szczególnie dobrze?"];
        Wpływ [label="Czemu Ci zależy?"];
    }

    // Łączenie węzłów
    Centrum -> Cechy;
    Centrum -> Wiedza;
    Centrum -> Relacje;
    Centrum -> Zasoby;
    Centrum -> Akcje;
    Centrum -> Wpływ;

    // Subcomponents for 'Cechy'
    CechyNode1 [label=""]
    CechyNode2 [label=""]
    CechyNode3 [label=""]

    // Links for 'Cechy'
    Cechy -> CechyNode1
    Cechy -> CechyNode2
    Cechy -> CechyNode3
    
    // Subcomponents for 'Wiedza'
    WiedzaNode1 [label=""]
    WiedzaNode2 [label=""]
    WiedzaNode3 [label=""]

    // Links for 'Wiedza'
    Wiedza -> WiedzaNode1
    Wiedza -> WiedzaNode2
    Wiedza -> WiedzaNode3

    // Subcomponents for 'Relacje'
    RelacjeNode1 [label=""]
    RelacjeNode2 [label=""]
    RelacjeNode3 [label=""]

    // Links for 'Relacje'
    Relacje -> RelacjeNode1
    Relacje -> RelacjeNode2
    Relacje -> RelacjeNode3

    // Subcomponents for 'Zasoby'
    ZasobyNode1 [label=""]
    ZasobyNode2 [label=""]
    ZasobyNode3 [label=""]

    // Links for 'Zasoby'
    Zasoby -> ZasobyNode1
    Zasoby -> ZasobyNode2
    Zasoby -> ZasobyNode3

    // Subcomponents for 'Akcje'
    AkcjeNode1 [label=""]
    AkcjeNode2 [label=""]
    AkcjeNode3 [label=""]

    // Links for 'Akcje'
    Akcje -> AkcjeNode1
    Akcje -> AkcjeNode2
    Akcje -> AkcjeNode3

    // Subcomponents for 'Wpływ'
    WpływNode1 [label=""]
    WpływNode2 [label=""]
    WpływNode3 [label=""]

    // Links for 'Wpływ'
    Wpływ -> WpływNode1
    Wpływ -> WpływNode2
    Wpływ -> WpływNode3
}
