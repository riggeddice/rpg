digraph Character {
    
    // Core
    node [shape=rect]
    rankdir=LR;
    
    Centrum [label="Drakolita\nUnifikata\nInżynier Stacji", shape=ellipse, fontname="bold"];
    {
        rank=same;
        Cechy [label="Cechy,\nwłasności"];
        Wiedza [label="Co wie szczególnego\nJaką wiedzę dostarcza?"];
        Relacje [label="Unikalne Relacje\nZ kim się lubi?"];
        Zasoby [label="Unikalne Zasoby\nDo czego ma dostęp?"];
        Akcje [label="Akcje\nCo robi szczególnie dobrze?"];
        Wpływ [label="Czemu Ci zależy?"];
    }

    // Łączenie węzłów
    Centrum -> Cechy;
    Centrum -> Wiedza;
    Centrum -> Relacje;
    Centrum -> Zasoby;
    Centrum -> Akcje;
    Centrum -> Wpływ;

    // Subcomponents for 'Cechy'
    CechyNode1 [label="Drakolita: zmodyfikowany genetycznie\nsilny, wytrzymały, z refleksem, szybko myśli"]
    CechyNode2 [label="Jego oblicze budzi przerażenie\ni nigdy nie jest rozbrojony"]
    CechyNode3 [label="Nigdy nie łamie kontraktów\ni jego słowo jest święte."]

    // Links for 'Cechy'
    Cechy -> CechyNode1
    Cechy -> CechyNode2
    Cechy -> CechyNode3
    
    // Subcomponents for 'Wiedza'
    WiedzaNode1 [label="Perfekcyjna znajomość stacji i mapy stacji;\nwie, skąd się gdzie dostać szybko i sprawnie,\nwie gdzie idą rury, kanały i inne takie"]
    WiedzaNode2 [label="Dane z czujników i sensorów, dane diagnostyczne Stacji\nzużycie zasobów i materiałów, miejsca bezpieczne itp."]
    WiedzaNode3 [label="Co się działo w sektorach inżynierii\ni jakie są plotki w inżynierii"]

    // Links for 'Wiedza'
    Wiedza -> WiedzaNode1
    Wiedza -> WiedzaNode2
    Wiedza -> WiedzaNode3

    // Subcomponents for 'Relacje'
    RelacjeNode1 [label="Mara Czuk, córka (emerytowanego) dowódcy najemników\n(eks-najemnicza kompania Teraquid ma sprzęt do utrzymania)"]
    RelacjeNode2 [label="Teriman Skarun + Wit-421, przywódcy Unifikacji\n(solidnie wykonujesz swoją pracę i pracujesz z każdym)"]
    RelacjeNode4 [label="Izolacjoniści: mawirowcy czy skrajni savaranie\ngo bardzo nie lubią i będą zwalczać"]

    // Links for 'Relacje'
    Relacje -> RelacjeNode1
    Relacje -> RelacjeNode2
    Relacje -> RelacjeNode4

    // Subcomponents for 'Zasoby'
    ZasobyNode1 [label="Dostęp do sprzętu inżynierskiego (naprawa, konstrukcja)\ni do różnych sensorów czy dron (badanie, skanowanie)"]
    ZasobyNode2 [label="Dostęp do dron inżynieryjnych, które mogą być\nw miejscach gdzie człowiek nie wejdzie"]
    ZasobyNode3 [label="Członek Unifikatów; sporo savaran i drakolitów\nmu pomoże i będzie z nim gadać"]

    // Links for 'Zasoby'
    Zasoby -> ZasobyNode1
    Zasoby -> ZasobyNode2
    Zasoby -> ZasobyNode3

    // Subcomponents for 'Akcje'
    AkcjeNode1 [label="Gladiator i wojownik na arenie - bardzo dobrze walczy"]
    AkcjeNode2 [label="Jeśli trzeba odwrócić uwagę, zrobi mały sabotaż."]
    AkcjeNode3 [label="Steruje zdalnie śluzami, gaśnicami, sprzętem...\njak trzeba, też zmieni parametry innych urządzeń"]
    AkcjeNode4 [label="Naprawi co trzeba lub przekształci istniejące\n urządzenie w coś, co się przyda."]

    // Links for 'Akcje'
    Akcje -> AkcjeNode1
    Akcje -> AkcjeNode2
    Akcje -> AkcjeNode3
    Akcje -> AkcjeNode4

    // Subcomponents for 'Wpływ'
    WpływNode1 [label="To jest Twój dom. Coś zabija Twoich przyjaciół.\nA Ty jesteś wojownikiem i możesz pomóc."]
    WpływNode2 [label="Stacja składa się z kryjówek i problemów;\nbez finansowania (turystycznego) Stacja umrze."]
    WpływNode3 [label="To jest miejsce, gdzie można być sobą\ni nawet 'wrogowie' dbają o Stację. Niezłe miejsce."]

    // Links for 'Wpływ'
    Wpływ -> WpływNode1
    Wpływ -> WpływNode2
    Wpływ -> WpływNode3
}
