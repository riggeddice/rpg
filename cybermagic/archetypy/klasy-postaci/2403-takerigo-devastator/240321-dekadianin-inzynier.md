digraph Character {
    
    // Core
    node [shape=rect]
    rankdir=LR;
    
    Centrum [label="Dekadianin\ninżynier\nAdvancer\nWojownik", shape=ellipse, fontname="bold"];
    {
        rank=same;
        Cechy [label="Cechy,\nwłasności"];
        Wiedza [label="Co wie szczególnego\nJaką wiedzę dostarcza?"];
        Relacje [label="Unikalne Relacje\nZ kim się lubi?"];
        Zasoby [label="Unikalne Zasoby\nDo czego ma dostęp?"];
        Akcje [label="Akcje\nCo robi szczególnie dobrze?"];
    }

    // Łączenie węzłów
    Centrum -> Cechy;
    Centrum -> Wiedza;
    Centrum -> Relacje;
    Centrum -> Zasoby;
    Centrum -> Akcje;

    // Subcomponents for 'Cechy'
    CechyNode1 [label="Dekadianin: żyje w ogniu walki\noraz nie traci głowy"]
    CechyNode2 [label="Niesamowicie paranoiczny;\npraktycznie zawsze przygotowany"]

    // Links for 'Cechy'
    Cechy -> CechyNode1
    Cechy -> CechyNode2
    
    // Subcomponents for 'Wiedza'
    WiedzaNode1 [label="Przewiduje ruchy przeciwnika\njeśli rozumie jego cel"]
    WiedzaNode2 [label="Jak przejąć kontrolę nad systemem statku\nmimo że teoretycznie nie powinien"]
    WiedzaNode3 [label="Jak zmodyfikować system by go\nsabotować, lub zrobić go lepszym\n(odwracalnie lub nie)"]
    WiedzaNode4 [label="Jak wydobyć z TAI danego miejsca\nrzeczy których ona nie chce dać"]
    
    // Links for 'Wiedza'
    Wiedza -> WiedzaNode1
    Wiedza -> WiedzaNode2
    Wiedza -> WiedzaNode3
    Wiedza -> WiedzaNode4

    // Subcomponents for 'Relacje'
    RelacjeNode1 [label="Samun Tiris: pierwszy oficer\n(Samun i Inżynier są dekadianami i rozumieją\nhonor oraz 'większe dobro', kiedyś jeden oddział)"]
    RelacjeNode2 [label="Jonatan Worot: lekko sadystyczny arystokrata\n(Jonatan zwyczajnie się go boi)"]
    RelacjeNode3 [label="Miron Worot: od systemów podtrzymywania życia\n(zrezygnowany starszy człowiek\nstudiujący broń biologiczną)"]

    // Links for 'Relacje'
    Relacje -> RelacjeNode1
    Relacje -> RelacjeNode2
    Relacje -> RelacjeNode3

    // Subcomponents for 'Zasoby'
    ZasobyNode1 [label="Kolekcjonuje broń różnego typu;\nzwykle jest zamknięta w Magazynach"]
    ZasobyNode2 [label="Drony inżynieryjne, zarówno do działań\nw środku jak i na zewnątrz statku"]
    ZasobyNode3 [label="Poszerzone uprawnienia inżyniera;\nkontroluje statek jak własną kieszeń"]
    ZasobyNode4 [label="Serwopancerz klasy SCUTIER;\nuzbrojony w strzelbę i laser"]

    // Links for 'Zasoby'
    Zasoby -> ZasobyNode1
    Zasoby -> ZasobyNode2
    Zasoby -> ZasobyNode3
    Zasoby -> ZasobyNode4

    // Subcomponents for 'Akcje'
    AkcjeNode1 [label="Naprawia i przebudowuje części statku;\ndziała jak McGyver"]
    AkcjeNode2 [label="Doskonały w walce wręcz i zasięgowej;\njeden z najlepszych na Statku"]
    AkcjeNode3 [label="Działa w próżni jak w domu;\nnie przywykł do życia na planecie"]
    AkcjeNode4 [label="Przejmuje kontrolę nad urządzeniami\noraz systemami, zdalnie lub na miejscu"]

    // Links for 'Akcje'
    Akcje -> AkcjeNode1
    Akcje -> AkcjeNode2
    Akcje -> AkcjeNode3
    Akcje -> AkcjeNode4
}
