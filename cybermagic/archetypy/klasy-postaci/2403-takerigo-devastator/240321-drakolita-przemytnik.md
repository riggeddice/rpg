digraph Character {
    
    // Core
    node [shape=rect]
    rankdir=LR;
    
    Centrum [label="Drakolita\nPrzemytnik,\nDyplomata\nWojownik", shape=ellipse, fontname="bold"];
    {
        rank=same;
        Cechy [label="Cechy,\nwłasności"];
        Wiedza [label="Co wie szczególnego\nJaką wiedzę dostarcza?"];
        Relacje [label="Unikalne Relacje\nZ kim się lubi?"];
        Zasoby [label="Unikalne Zasoby\nDo czego ma dostęp?"];
        Akcje [label="Akcje\nCo robi szczególnie dobrze?"];
    }

    // Łączenie węzłów
    Centrum -> Cechy;
    Centrum -> Wiedza;
    Centrum -> Relacje;
    Centrum -> Zasoby;
    Centrum -> Akcje;

    // Subcomponents for 'Cechy'
    CechyNode1 [label="Drakolita: zmodyfikowany genetycznie\nsilny i bardzo szybki"]
    CechyNode2 [label="Dużo lepsze zmysły i pamięć niż normalni ludzie"]

    // Links for 'Cechy'
    Cechy -> CechyNode1
    Cechy -> CechyNode2
    
    // Subcomponents for 'Wiedza'
    WiedzaNode1 [label="Gdzie są najlepsze kryjówki? Gdzie można co i jak ukryć?"]
    WiedzaNode2 [label="Kto miał problemy z prawem / innymi słabościami?\nJak wpłynąć na różne osoby?"]
    WiedzaNode3 [label="Jak wyceniać różne rzeczy?\nCo powinno być na statku a co nie?"]
    WiedzaNode4 [label="Jakie są INNE metody wzmocnienia;\nczęsto magiczne / nielegalne / groźne"]
    
    // Links for 'Wiedza'
    Wiedza -> WiedzaNode1
    Wiedza -> WiedzaNode2
    Wiedza -> WiedzaNode3
    Wiedza -> WiedzaNode4

    // Subcomponents for 'Relacje'
    RelacjeNode1 [label="Edgar Valtin: oficer łączności;\n(razem grają w gry i ŚWIETNIE się bawią)"]
    RelacjeNode2 [label="Jonatan Worot: lekko sadystyczny arystokrata\n(Jonatan za dużo wisi Przemytnikowi by odmówić)"]
    RelacjeNode3 [label="Kasjopea Tiris: sympatyczna medyczka\n(Kasjopea szuka sposobu jak Przemytnika PRZYSPIESZYĆ)"]

    // Links for 'Relacje'
    Relacje -> RelacjeNode1
    Relacje -> RelacjeNode2
    Relacje -> RelacjeNode3

    // Subcomponents for 'Zasoby'
    ZasobyNode1 [label="Zawsze ma przy sobie ukrytą broń;\nnigdy nie jest zaskoczony"]
    ZasobyNode2 [label="Prywatna nakładka badawcza na systemy;\nzawsze chce móc dojść co się dzieje"]
    ZasobyNode3 [label="Coś, na czym innym zależy;\nzwykle umie innych przekupić"]
    ZasobyNode4 [label="Serwopancerz klasy SCUTIER;\nuzbrojony w strzelbę i laser"]

    // Links for 'Zasoby'
    Zasoby -> ZasobyNode1
    Zasoby -> ZasobyNode2
    Zasoby -> ZasobyNode3
    Zasoby -> ZasobyNode4

    // Subcomponents for 'Akcje'
    AkcjeNode1 [label="Doskonale wyczuwa uczucia drugiej osoby;\nświetnie manipuluje ludźmi"]
    AkcjeNode2 [label="Odwraca uwagę, ściągając ją na siebie\nlub zrzucając gdzieś indziej"]
    AkcjeNode3 [label="Robi się całkowicie nieruchomy;\npraktycznie nie do zauważenia"]
    AkcjeNode4 [label="Znajduje kryjówki i przejścia;\nzawsze jest sposób COŚ zrobić"]

    // Links for 'Akcje'
    Akcje -> AkcjeNode1
    Akcje -> AkcjeNode2
    Akcje -> AkcjeNode3
    Akcje -> AkcjeNode4
}
