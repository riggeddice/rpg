---
layout: inwazja-karta-postaci
categories: archetype
title: "Wolność dla wszystkich"
---

# {{ page.title }}

## Motywacja

### Czego pragnie a nie ma (3)

Wszyscy jesteśmy ludźmi lub pochodnymi. Każdy ma prawo do wolności i swobody. Pomagajmy sobie - surowców starczy dla każdego.

### Doprecyzowanie

* Pomaga wszystkim w potrzebie
* Niszczy systemy kontroli, też radością
* Wybiera wolność nad radość i ochronę
* Nie wolno ograniczać decyzji i swobody
* Zawsze jest nadzieja na poprawę

### Przykłady

* Dracena Diakon
