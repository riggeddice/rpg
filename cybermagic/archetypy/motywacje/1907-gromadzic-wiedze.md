---
layout: inwazja-karta-postaci
categories: archetype
title: "Gromadzić wiedzę"
---

# {{ page.title }}

## Motywacja

### Czego pragnie a nie ma (3)

Zrozumieć ten świat, czymkolwiek nie jest. Gromadzić wiedzę dla siebie i potomnych.

### Doprecyzowanie

* Budować repozytorium wiedzy
* Zrozumieć Anomalie, badać Eter
* Nie ma "zakazanej" wiedzy - zbierać ZAWSZE
* Chronić wiedzę przed zniszczeniem
* Poszerzać biblioteki, uwalniać sekrety

### Przykłady

* Astinus (Dragonlance)
