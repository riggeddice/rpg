---
layout: inwazja-karta-postaci
categories: archetype
title: "Harmonia ze światem"
---

# {{ page.title }}

## Motywacja

### Czego pragnie a nie ma (3)

Musimy harmonijnie współistnieć a nie walczyć ze światem i z sobą. Nasz wspólny taniec z Naturą i Eterem w nowym świecie.

### Doprecyzowanie

* Harmonia między ludźmi, magią, naturą
* Jak najmniej ingerujące rozwiązania
* Zwalczaj wyzysk i eksploatację
* Buduj długoterminowe, stabilne systemy
* Ulecz cierpienie ciała i duszy

### Przykłady

* ?
