---
layout: inwazja-karta-postaci
categories: archetype
title: "Ewolucja przez walkę"
---

# {{ page.title }}

## Motywacja

### Czego pragnie a nie ma (3)

Walka jest wartością samą w sobie. Wszyscy musimy się wzmacniać walką. Kto chce inaczej - zostanie zniszczony.

### Doprecyzowanie

* Posiadasz tylko to, co umiesz obronić
* Silniejszy ma prawo zabrać słabszym
* Walcz i rywalizuj - ten świat wymaga siły
* Pomóż innym walczyć skuteczniej
* Skuteczność ponad honor. Bądź najlepszy

### Przykłady

* Blood Knight
