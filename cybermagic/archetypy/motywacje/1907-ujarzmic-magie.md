---
layout: inwazja-karta-postaci
categories: archetype
title: "Ujarzmić Magię"
---

# {{ page.title }}

## Motywacja

### Czego pragnie a nie ma (3)

Magię należy ujarzmić. Świat musi być pod kontrolą ludzi. Ludzkość ponad nienaturalność.

### Doprecyzowanie

* Stworzyć porządek w Skażonym świecie.
* Świat powinien należeć do ludzi.
* Spętać magię techniką. Kontrolować Eter.
* Czego nie da się kontrolować - trzeba usunąć.
* Anomalie i Skażeńcy - w ryzach jako narzędzia.

### Przykłady

* Grace Hall (Dents)
