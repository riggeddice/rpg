---
layout: inwazja-karta-postaci
categories: archetype
title: "Chronić swoich"
---

# {{ page.title }}

## Motywacja

### Czego pragnie a nie ma (3)

To są moi ludzie. Zrobię co w mojej mocy, by dobrze im się wiodło i by nie spotkała ich żadna szkoda.

### Doprecyzowanie

* Chroni swoich przed obcymi / anomaliami
* Poświęci obcych by chronić swoich
* Pomoże swoim znaleźć pracę czy wyjść na prostą
* Zawsze da swoim drugą szansę
* Chroni kulturę i zwyczaje swoich

### Przykłady

* Ernest Kajrat
* Tekkadan; Iron Blooded Orphans
