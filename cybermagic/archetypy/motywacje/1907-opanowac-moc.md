---
layout: inwazja-karta-postaci
categories: archetype
title: "Opanować Moc"
---

# {{ page.title }}

## Motywacja

### Czego pragnie a nie ma (3)

Masz ogromną Moc, ale nie do końca ją kontrolujesz. Czasem sama się uwalnia i terroryzuje. Uzyskaj kontrolę nie tracąc mocy.

### Doprecyzowanie

* Szukaj przyjaźni, nie strachu
* Nie bądź tylko groźną bronią dla innych
* Eksperymentuj z formami kontroli mocy
* Zachowaj swoją moc za wszelką cenę
* Nie ograniczaj się, pokaż swą siłę

### Przykłady

* Suspiria (Flipside)
