---
layout: inwazja-karta-postaci
categories: archetype
title: "Pokonać rywala"
---

# {{ page.title }}

## Motywacja

### Czego pragnie a nie ma (3)

Mój rywal jest lepszy ode mnie. Pokonam go - doszczętnie. Będę lepszy.

### Doprecyzowanie

* pokonaj Rywala w bezpośrednim starciu
* przekonaj innych, żeś lepszy od Rywala
* bądź lepszy od Rywala na innym polu
* przechwyć towarzyszy Rywala jako swoich
* uszkadzaj zasoby i możliwości Rywala

### Przykłady

* większość Shonen
