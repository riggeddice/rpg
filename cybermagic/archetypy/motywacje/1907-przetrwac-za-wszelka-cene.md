---
layout: inwazja-karta-postaci
categories: archetype
title: "Przetrwać za wszelką cenę"
---

# {{ page.title }}

## Motywacja

### Czego pragnie a nie ma (3)

Bomba atomowa. Plaga. Wirus komputerowy. Wszystko to może zniszczyć i skrzywdzić - a ja na wszystko mam sposób i sobie poradzę.

### Doprecyzowanie

* Znajdź sojuszników lepszych od siebie
* Zdobywaj przysługi i kontakty
* Buduj kryjówki, drogi ucieczki
* Chomikuj surowce i przydatny sprzęt
* Wystawiaj się na ryzyko, kontrolowane
