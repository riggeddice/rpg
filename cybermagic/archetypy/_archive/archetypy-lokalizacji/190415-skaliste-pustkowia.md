---
layout: inwazja-karta-postaci
categories: archetype
title: "Skaliste Pustkowie"
---

# {{ page.title }}

## Lokalizacja

### Ogólny teren

* skaliste pustkowia z niewielką ilością roślinności
* kruchy piaskowiec i silne wiatry; silna erozja
* dziwne formacje skalne

### Aspekty

* liczne szczeliny i jaskinie
* teren się rozpada
* bardzo mało roślinności
* bardzo suchy obszar

## Konflikty

* spadające kamienie; wpadnięcie w szczelinę
* węże i pająki; suche rośliny; pożar
