---
layout: inwazja-karta-postaci
categories: archetype
title: "Dworzec kolejowy"
---

# {{ page.title }}

## Lokalizacja

### Ogólny teren

* dworzec i dla towarów i dla podróżnych
* efektywna komunikacja, centrum logistyczne

### Aspekty

* aktywne i nieaktywne pociągi
* magazyny i poczekalnie
* strażnicy i monitoring
* sporo ciekawych ludzi

## Konflikty

* spóźnienie się
* znalezienie kogoś w tłumie
* powstrzymanie odjazdu
