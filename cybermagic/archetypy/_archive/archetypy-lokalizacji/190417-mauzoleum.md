---
layout: inwazja-karta-postaci
categories: archetype
title: "Mauzoleum"
---

# {{ page.title }}

## Lokalizacja

### Ogólny teren

* grobowiec, muzeum ku czci
* monumentalne, efektowne

### Aspekty

* potężna, przytłaczająca aura
* monumentalne, bogate miejsce
* ogrom wiedzy na temat
* pamiątki i artefakty

### Spotkania

* kustosz i strażnicy
* turyści
* fanatycy celu Mauzoleum

## Konflikty

* podpaść fanatykom
* konieczność kradzieży z Mauzoleum
* dostać się na zaplecze
