---
layout: inwazja-karta-postaci
categories: archetype
title: "Użytkowy kosmoport"
---

# {{ page.title }}

## Lokalizacja

### Ogólny teren

* niewielki kosmoport z przeładownią
* magazyny, kilka budynków, wieża kontrolna

### Aspekty

* otwarty teren z obszarem budynków
* mało ludzi, raczej obsługa
* automatyczne przeładownie
* celnicy i skanery

## Konflikty

* problemy z biurokracją
* pijany i sfrustrowany załogant
* opóźnienia nietrwałego towaru
