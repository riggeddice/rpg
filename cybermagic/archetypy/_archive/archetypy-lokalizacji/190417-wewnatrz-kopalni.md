---
layout: inwazja-karta-postaci
categories: archetype
title: "Wewnątrz Kopalni"
---

# {{ page.title }}

## Lokalizacja

### Ogólny teren

* administracja na powierzchni
* maszyny, chodniki, tunele pod ziemią
* winda łączy światy

### Aspekty

* ciasne tunele z metanem
* ciężkie, niebezpieczne maszyny
* głośna winda, bez ukrycia
* gra świateł i cieni

### Spotkania

* sztygar, górnicy
* roboty wydobywcze
* ratownicy

## Konflikty

* wypadek w kopalni
* znaleźć nowy szyb
* ufortyfikować teren
