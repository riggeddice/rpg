---
layout: inwazja-karta-postaci
categories: archetype
title: "Sektor Przemysłowy"
---

# {{ page.title }}

## Lokalizacja

### Ogólny teren

* obszar z dużą ilością przemysłu
* silnie zrobotyzowany, niezbyt bezpieczny

### Aspekty

* dużo zakamarków i zaułków, niezbyt czysty
* obszary nie przystosowane do ludzi
* dużo poruszających się maszyn
* liczne fabrykatory
* wysoka temperatura

## Konflikty

* uszkodzenie ważnego robota
* problem z łańcuchem dostaw
* istoty żyjące w cieniach
