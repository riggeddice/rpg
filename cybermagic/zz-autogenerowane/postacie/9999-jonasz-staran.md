---
categories: profile
factions: 
owner: public
title: Jonasz Staran
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210918-pierwsza-bitwa-martyna       | dowódca OO Invictus. Mając kiepską jednostkę świetnie wymanewrował tajemniczy okręt noktiański i pozwolił siłom Sojuszu wesprzeć tien Kopiec. W walce poniósł straszne uszkodzenia i musiał się ewakuować; ale docelowo przejął tę jednostkę noktiańską. | 0080-11-23 - 0080-11-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Jolanta Kopiec       | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Kalina Rota d'Kopiec | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Martyn Hiwasser      | 1 | ((210918-pierwsza-bitwa-martyna)) |
| OO Invictus          | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Pandemiusz Diakon    | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Szczepan Kutarcjusz  | 1 | ((210918-pierwsza-bitwa-martyna)) |