---
categories: profile
factions: 
owner: public
title: Amadeusz Sowiński
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181112-odklatwianie-ateny           | lord terminus Cieniaszczytu silnie współpracujący z kralothami. Chce znaleźć i usunąć tych co atakują Atenę. Zrekonstruował Atenę mocą kralothów Cieniaszczytu | 0109-10-23 - 0109-10-26 |
| 181226-finis-vitae                  | powiedział Pięknotce o autowarze i zapewnił odpowiednią siłę ognia by Moktar (Saitaer) nie dowiedział się o co chodzi. | 0109-11-18 - 0109-11-27 |
| 181227-adieu-cieniaszczycie         | wziął na siebie zbudowanie świątyni Arazille nad Studnią Bez Dna. Zabezpieczył Cieniaszczyt przed autowarem Finis Vitae ukrytym pod ziemią. | 0109-11-28 - 0109-12-01 |
| 190106-a-moze-pustogorska-mafia     | źródło leadów Pięknotki jak chodzi o sponsorów w Cieniaszczycie. Zaskakująco dobry w tej kwestii. Działał zdalnie, z Cieniaszczytu. | 0109-12-24 - 0109-12-26 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181227-adieu-cieniaszczycie         | za miesiąc straci stanowisko przywódcy terminusów Cieniaszczytu - za świątynię Arazille blokującą Finis Vitae. | 0109-12-01

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 4 | ((181112-odklatwianie-ateny; 181226-finis-vitae; 181227-adieu-cieniaszczycie; 190106-a-moze-pustogorska-mafia)) |
| Atena Sowińska       | 3 | ((181112-odklatwianie-ateny; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Moktar Gradon        | 2 | ((181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Waleria Cyklon       | 2 | ((181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Adela Kirys          | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Arazille             | 1 | ((181226-finis-vitae)) |
| Brygida Maczkowik    | 1 | ((181227-adieu-cieniaszczycie)) |
| Cezary Zwierz        | 1 | ((181112-odklatwianie-ateny)) |
| Erwin Galilien       | 1 | ((181227-adieu-cieniaszczycie)) |
| Kasjan Czerwoczłek   | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Lilia Ursus          | 1 | ((181227-adieu-cieniaszczycie)) |
| Lucjusz Blakenbauer  | 1 | ((181112-odklatwianie-ateny)) |
| Minerwa Diakon       | 1 | ((181112-odklatwianie-ateny)) |
| Mirela Niecień       | 1 | ((181227-adieu-cieniaszczycie)) |
| Pietro Dwarczan      | 1 | ((181227-adieu-cieniaszczycie)) |
| Roland Grzymość      | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Saitaer              | 1 | ((181112-odklatwianie-ateny)) |
| Tadeusz Rupczak      | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Waldemar Mózg        | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Zbigniew Burzycki    | 1 | ((181227-adieu-cieniaszczycie)) |