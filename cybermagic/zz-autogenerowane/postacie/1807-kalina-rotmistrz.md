---
categories: profile
factions: 
owner: kić
title: Kalina Rotmistrz
---

# {{ page.title }}


# Read: 

## Metadane

* factions: 
* type: "PC"
* owner: "kić"
* title: "Kalina Rotmistrz"


## Postać

### Koncept (1)

Odarta ze złudzeń terminuska, która zawsze dotrzymuje danego słowa.

Pochodzi z rodziny żeglarzy eteru, uczyła się zawodu od terminusów pływających z jej rodziną.
Chciała zostać katalistką, ale moc nie dopisała. Potem chciała zostać terminusem i to się udało, ale straciła swoją pierwszą kompanię po tym, jak wskutek nieudolnego dowództwa niemal cała drużyna zginęła. Przeżyła tylko ona. Teraz nie potrafi zaufać jakiemukolwiek nowemu dowódcy.

Pragmatyczna, ale nie okrutna. 

Na akcjach obwieszona sprzętem, aby kompensować swoje braki. Nieco paranoiczna, zawsze gotowa na wszystko.
Znana z nieortodoksyjnych rozwiązań problemów.

Dorabia sobie skupując od artefaktorów nieaktywne odpady produkcyjne i sprzedając je jubilerom jako nietypowe kamienie.

### Otoczenie (1)

Statek. Morze. Rzeka. Strumyk chociaż... Kalina dobrze czuje się w pobliżu wody. Na wodzie się wychowała i stąd może w miarę bezpiecznie czerpać energię. Jeśli nie siedzi na statku, zwykle kręci się w pobliżu wody. 
Kiedy już oddala się od wody, zwykle po to, aby pohandlować - głównie z artefaktorami. Targi, dzielnice handlowe i sklepy / warsztaty artefaktorów to jej drugi dom.

### Misja (1)

Zapewnić bezpieczeństwo w eterze. Eter to niebezpieczne miejsce, o czym Kalina przekonała się wielokrotnie w swoim życiu. Ona postanowiła, że zrobi cokolwiek, aby magowie i ludzie mogli czuć się tam bezpieczniej.

Kalina nie zgadza się, aby istotom pod jej opieką stała się krzywda. 
Jeśli nie otoczyła Cię opieką (kontrakt lub sama z siebie), może nie kiwnąć palcem, ale pod jej ochroną jesteś bezpieczny. 

### Wada (-2)

She can't into fun.
Z natury raczej ponura, nie radzi sobie w sytuacjach towarzyskich. Pogawędki na błahe tematy to dla niej marnowanie czasu. Umie tańczyć (bo wypada), ale na imprezach zwykle podpiera ściany. Kiedy każą się jej uśmiechnąć, wygląda, jakby bolał ją brzuch. 

### Motywacje (1)

| Co chce by się działo? Co jest pożądane?  | Co jest antytezą postaci? Co jest jej negacją?        |
|-------------------------------------------|-------------------------------------------------------|
| dąż do zrozumienia sytuacji               | bezkrytycznie przyjmuj rozkazy                        |
| przygotuj się na każdą ewentualność       | bierz na wiarę to, co mówią                           |
| w handlu dąż do uczciwej transakcji       | daj się naciągnąć                                     |  
| chroń podopiecznych za wszelką cenę       | gódź się na okrucieństwo;pozwól, by krzywda działa się z twojej winy |

### Zachowania (1)

| Jak zazwyczaj się zachowuje?             | Pod wpływem jakich uczuć / w jakich okolicznościach to robi?|
|------------------------------------------|-------------------------------------------------------------|
| uprzejma, no-nonsense                    | zazwyczaj, w sytuacjach typowych |
| metodyczna, przewidująca, opanowana      | przed akcją |
| improwizuje, przejmuje dowodzenie        | w stresie |
| ukrywa emocje, gra pod publiczność       | w trakcie negocjacji lub przesłuchań |

### Umiejętności (4)

Terminus z zacięciem do taktyki. Zawsze przygotowana, jednak zdaje sobie sprawę, że każdy plan jest nieważny w momencie powstania - dlatego gotowa jest do improwizacji. Skupia się na ochronie, atakuje bezpośrednio tylko jeśli jest to konieczne. Bardziej terminus wsparcia, choć doświadczenia zmusiły ją do działania na pierwszej linii...

Zaopatrzeniowiec. "Niemożliwe załatwiamy od ręki, na cuda trzeba poczekać." Załatwi niezbędne środki i materiały. Twardy negocjator. Nie daje się łatwo oszwabić na handlu. Dąży do tego, aby ten, z kim handluje nie czuł się oszukany. Jak potrzeba, bajeruje na potęgę.

Żeglarz eteru. Choć nie jest katalistką, Kalina sporo wie o eterze i jego istotach i potrafi przetrwać w niesprzyjających warunkach. Zna statki - zwłaszcza eteryczne - i choć raczej statku nie naprawi, wie co oznaczają jego ruchy i wydawane dźwięki, potrafi się też poruszać po większości statków - to dla niej swojski teren.

### Magia (3)

#### Gdy kontroluje energię

* Błyskawiczne i bezpieczne przemieszczanie się na średni lub krótki dystans.
* Magia obronna - Kalina skupia się na ochronie i ratowaniu. 
* Elementalna magia bojowa - fireball, ścięcie wody by unieruchomić, stara, dobra błyskawica... stare, dobre, proste metody walki magicznej.

#### Gdy traci kontrolę

* Zagrożenia są bezwzględnie usuwane z drogi - fireball czy teleport... kto wie...?
* Woda. Przy wodzie czuje się pewnie, jeśli brak jej tego poczucia, woda może pojawić się gdziekolwiek, skądkolwiek, w jakiejkolwiek postaci.

### Zasoby i otoczenie

#### Ogólnie (4)

* Znajomości wśród artefaktorów
* Kasa ze sprzedaży "klejnotów" - odpadów artefaktycznych
* Fundusze na sprzęt i rekonesans

#### Powiązane frakcje

{{ page.factions }}

## Opis



# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180623-krzyk-w-wyplywowie           | współautorka zniszczenia hotelu, ochroniarz Kajetana chroniąca go przed Skorpionem i głównodowodząca operacji 'ratujmy cholerną magitrownię'. | 0109-07-02 - 0109-07-05 |
| 180701-dwa-rejsy-w-potrzebie        | odwróciła uwagę Lewiatana od statku używając szalupy, potem uniesmaczniła statek w Eterze przekonując Gerarda do użycia Esuriit a na samym Wojowniku koordynowała pozbycie się Martaurona ze statku. | 0109-08-07 - 0109-08-11 |
| 180708-niewidzialne-potwory-z-rzeki | zidentyfikowała że za Dziwnym Potworem z rzeki stoi stary oszust, po czym go wygnała (grzecznie). Nie dała się oszukać i zachowała dobre relacje. | 0109-08-18 - 0109-08-20 |
| 180718-msciwa-ryba-z-eteru          | uratowała posiadłość rezydentki, odkryła sekret Majki (że lubi malować) oraz doprowadziła do zniszczenia feralnego obrazu Majki. | 0109-08-25 - 0109-08-27 |
| 180730-prasyrena-z-zemsty           | przeraża Syrenę, przesuwa Pryzmat w mieście ze Stachem i daje się załatwić kralotycznym proszkiem. Ogólnie, terminuska. | 0109-08-29 - 0109-08-30 |
| 180808-kultystka-z-milosci          | pozyskała sojusznika, spaliła Kacprowi dom i uratowała thugów z tego domu. W końcu się zirytowała i pojmała Kacpra, kończąc sprawę Kręgu Ośmiornicy. | 0109-08-30 - 0109-09-02 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180708-niewidzialne-potwory-z-rzeki | uznawana za lojalistkę Magdy przez osoby w okolicach Niedźwiedźnika, ale pomogła odbudować magitrownię i system obronny za co też ma szacun w okolicy. | 0109-08-20
| 180718-msciwa-ryba-z-eteru          | pozbywa się etykietki lojalistki. Jest po prostu "wędrującym terminusem". | 0109-08-27
| 180718-msciwa-ryba-z-eteru          | Magda Patiril, właścicielka posiadłości w Niedźwiedźniku, uważa ją za przyjaciółkę. | 0109-08-27
| 180730-prasyrena-z-zemsty           | całość chwały za chęć pomocy kobietom w formie amuletów ochronnych przypada Kalinie. Pozytywnie kojarzona w Toporzysku. | 0109-08-30
| 180808-kultystka-z-milosci          | Stachu polubił podejście Kaliny. Nie to, żeby coś zaszło; jest w końcu gliną. Ale może jej czasem pomóc czy złapać z nią kontakt. | 0109-09-02

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Zajcew        | 3 | ((180708-niewidzialne-potwory-z-rzeki; 180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Stach Sosnowiecki    | 3 | ((180708-niewidzialne-potwory-z-rzeki; 180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Joachim Kozioro      | 2 | ((180708-niewidzialne-potwory-z-rzeki; 180718-msciwa-ryba-z-eteru)) |
| Kasandra Kirnał      | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Lawenda Weiner       | 2 | ((180718-msciwa-ryba-z-eteru; 180730-prasyrena-z-zemsty)) |
| Magda Patiril        | 2 | ((180708-niewidzialne-potwory-z-rzeki; 180718-msciwa-ryba-z-eteru)) |
| Maksymilian Supolont | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Małgorzata Kirnał    | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Aiden Wielkołacz     | 1 | ((180623-krzyk-w-wyplywowie)) |
| Anita Perczoluk      | 1 | ((180730-prasyrena-z-zemsty)) |
| Borys Perikas        | 1 | ((180718-msciwa-ryba-z-eteru)) |
| Emilia Kariamon      | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Fabian Komczatkow    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Feliks Weiner        | 1 | ((180808-kultystka-z-milosci)) |
| Gerard Weiner        | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Kacper Pyszałnik     | 1 | ((180808-kultystka-z-milosci)) |
| Kacper Wontarczyk    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Kajetan Bosman       | 1 | ((180623-krzyk-w-wyplywowie)) |
| Krystian Moborok     | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Liwia Barbaturran    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Majka Perikas        | 1 | ((180718-msciwa-ryba-z-eteru)) |
| Małgorzata Nowacka   | 1 | ((180623-krzyk-w-wyplywowie)) |
| Marcjanna Maszotka   | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Martauron Attylla    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Patryk Paterecki     | 1 | ((180808-kultystka-z-milosci)) |
| Sabrina Powsimrożek  | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Stanisław Mirczak    | 1 | ((180623-krzyk-w-wyplywowie)) |
| Tymon Barbaturran    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Wojciech Zajcew      | 1 | ((180701-dwa-rejsy-w-potrzebie)) |