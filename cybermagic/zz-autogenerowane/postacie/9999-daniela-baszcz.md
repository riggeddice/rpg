---
categories: profile
factions: 
owner: public
title: Daniela Baszcz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191108-ukojenie-aleksandrii         | żona Leszka, która chciała ratować rodzinę. Zniewolona przez Kuratorów, stała się szczęśliwą marionetką. | 0110-07-18 - 0110-07-20 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Mirzant       | 1 | ((191108-ukojenie-aleksandrii)) |
| Kinga Stryk          | 1 | ((191108-ukojenie-aleksandrii)) |
| Klara Baszcz         | 1 | ((191108-ukojenie-aleksandrii)) |
| Leszek Baszcz        | 1 | ((191108-ukojenie-aleksandrii)) |
| Paweł Kukułnik       | 1 | ((191108-ukojenie-aleksandrii)) |