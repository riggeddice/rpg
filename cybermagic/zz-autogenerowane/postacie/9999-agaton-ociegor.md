---
categories: profile
factions: 
owner: public
title: Agaton Ociegor
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201011-narodziny-paladynki-saitaera | biochemik, korozja i leczenie, lekarz Grzymościa. Mimo ataku na Grzymościa przybył naprawić ten teren; złapał Pięknotkę i prawie zginął do Cienia. Skończył nieprzytomny. | 0110-10-12 - 0110-10-13 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 201025-kraloth-w-parku-janor        | oddany przez Lucjusza Blakenbauera jako "sukces Pięknotki" jako dywersja do Pustogoru. Siedzi zamknięty w więzieniu. | 0110-10-31

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksander Muniakiewicz | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Gabriel Ursus        | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Minerwa Metalia      | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Pięknotka Diakon     | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Sabina Kazitan       | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Saitaer              | 1 | ((201011-narodziny-paladynki-saitaera)) |