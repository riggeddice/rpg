---
categories: profile
factions: 
owner: public
title: TAI Mirris d'Paravilius
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230117-ros-wiertloplyw-i-tai-mirris | nie wiedziała, że wojna się skończyła. Używając Paravilius przejęła kontrolę nad Wiertłopływem, ale potem po rozmowie z noktianami pomogła im najbardziej jak mogła - samozniszczeniem. | 0084-02-22 - 0084-02-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eleonora Alintirias  | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Kasia Karmnik        | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Lucas Septrien       | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Łucja Nirimis        | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Napoleon Myszogłów   | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Niferus Sentriak     | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| OR Błyskawica        | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| SC Wiertłopływ       | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Talia Irris          | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Tristan Andrait      | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |