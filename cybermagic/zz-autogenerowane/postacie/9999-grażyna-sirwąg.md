---
categories: profile
factions: 
owner: public
title: Grażyna Sirwąg
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180906-nikt-nie-spi-w-swoim-lozku   | nauczycielka polskiego i bibliotekarka w Cyberszkole oraz członek Grupy Okultystycznej Zaczęstwa. Ma materiały do szantażu na dyrektora. | 0109-09-09 - 0109-09-11 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180906-nikt-nie-spi-w-swoim-lozku   | ma materiały do szantażu dyrektora cyberszkoły - spał z Brygidą. | 0109-09-11

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Atena Sowińska       | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Brygida Maczkowik    | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Mieszko Weiner       | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Pięknotka Diakon     | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Szczepan Mensic      | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Tadeusz Kruszawiecki | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |