---
categories: profile
factions: 
owner: public
title: Szczepan Kutarcjusz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210918-pierwsza-bitwa-martyna       | kapral sił specjalnych dowodzący oddziałkiem ochotników pomagających Jolancie Kopiec. Wszedł w tą akcję, bo chciał poznać Eternię. Widząc jak straszna jest eternijska tienka, już nie chce mieć z nimi nic wspólnego. Świetny taktyk, doprowadził do przejęcia dziwnej noktiańskiej jednostki przez grupę kompetentnej ale jednak milicji + jeden emiter anihilacji (blaster launcher). | 0080-11-23 - 0080-11-28 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210918-pierwsza-bitwa-martyna       | MEDAL! Za zdobycie noktiańskiego okrętu, odwagę i działania taktyczne w obliczu przeważających sił wroga w skrajnej sytuacji. I awans - jest sierżantem po tej akcji. | 0080-11-28

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Jolanta Kopiec       | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Jonasz Staran        | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Kalina Rota d'Kopiec | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Martyn Hiwasser      | 1 | ((210918-pierwsza-bitwa-martyna)) |
| OO Invictus          | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Pandemiusz Diakon    | 1 | ((210918-pierwsza-bitwa-martyna)) |