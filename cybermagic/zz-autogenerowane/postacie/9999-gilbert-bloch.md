---
categories: profile
factions: 
owner: public
title: Gilbert Bloch
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210825-uszkodzona-brama-eteryczna   | niby "komisarz" sił specjalnych by Infernia nie spieprzyła tematu z Bramą, ale dość przyjemny i nieproblematyczny. Pod niesamowitym wrażeniem tego, jak Infernia rozwiązała problem efemeryd i uratowała wszystkich. | 0112-02-04 - 0112-02-07 |
| 210901-stabilizacja-bramy-eterycznej | zależało mu na naprawie Bramy; przekazał Inferni info o Perforatorze i innych jednostkach Orbitera pod czarem Flawii. | 0112-02-09 - 0112-02-12 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210901-stabilizacja-bramy-eterycznej | skandal, przekazał Flawii coś czego nie powinien (baza danych sił specjalnych Orbitera). Poważny cios w reputację u Medei. | 0112-02-12

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Elena Verlen         | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Eustachy Korkoran    | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Flawia Blakenbauer   | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Klaudia Stryk        | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Medea Sowińska       | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| AK Nocna Krypta      | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Diana d'Infernia     | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Janus Krzak          | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Leona Astrienko      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Martyn Hiwasser      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Infernia          | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Netrahina         | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |