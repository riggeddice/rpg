---
categories: profile
factions: 
owner: public
title: OO Athamarein
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221116-astralna-flara-dociera-do-nonariona-nadziei | ciężka korweta rakietowa Orbitera p.d. Leszka Kurzmina i statek flagowy Gabriela Lodowca. Walczy dużo ponad swą wagę, acz wymaga jednostki wsparcia (zasięg, amunicja). Skutecznie unika ataku asteroid anomalnych, ale wpada w pułapkę i Flara go uratowała. | 0100-09-08 - 0100-09-11 |
| 221123-egzotyczna-pieknosc-na-astralnej-flarze | ma na pokładzie nadspodziewanie dobrego psychotronika. Na pokład trafił do aresztu Frank Mgrot ("marine z Orbitera / dezerter") zanim Hadiah Emas odleciała dalej. | 0100-09-12 - 0100-09-15 |
| 221130-astralna-flara-w-strefie-duchow | udało się użyć Persi i Zarralei do zrozumienia co się dzieje w Strefie Duchów. | 0100-10-05 - 0100-10-08 |
| 221214-astralna-flara-kontra-domina-lucis | ostrożnie skanuje okolicę Strefy Duchów i wykrywa miny i inne noktiańskie niespodzianki; przygotuje odpowiednie kąty rażenia, by nikt nie dał rady się wycofać z Dominy Lucis. | 0100-10-09 - 0100-10-11 |
| 221221-astralna-flara-i-nowy-komodor | retransmiter dla Astralnej Flary by Maja mogła odzyskać kontrolę nad Kazmirian i unieszkodliwić squatterów. | 0100-11-07 - 0100-11-10 |
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | przekroczyła moc silników (150%), ale złapała Adragaina (brata pilotki Nox Ignis) i wróciła manewrować przeciw Nox Ignis. Uległa lekkim uszkodzeniom silników (max. 70% pełnej mocy) | 0100-11-13 - 0100-11-16 |
| 231018-anomalne-awarie-athamarein   | bardzo stara już korweta, która Odbiła się w Eterze. Traci nadspodziewanie mało załogi. SAMA zrobiła manewr unikający działa Syndykatu. W chwili, w której załoga Athamarein nie jest zdolna do pełnienia misji, Athamarein zaczęła sama się sabotować, uniemożliwiając im iść na akcję gdzie mogliby zginąć. Pozytywna jednostka o podstawowych cechach Anomalii Kosmicznej, ale nastawionej pro-ludzko. | 0111-12-22 - 0111-12-27 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | uszkodzona; pełna moc silników to 70%. Przesterowana. | 0100-11-16
| 231018-anomalne-awarie-athamarein   | przeznaczona od teraz do misji szkoleniowych. Jej ochrona załogi, jej odporność i niskokosztowość są idealne w tej jakże przecież ważnej roli. Czas na emeryturę. | 0111-12-27

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 7 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 231018-anomalne-awarie-athamarein)) |
| Daria Czarnewik      | 6 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kajetan Kircznik     | 6 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 231018-anomalne-awarie-athamarein)) |
| OO Astralna Flara    | 6 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leszek Kurzmin       | 5 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Elena Verlen         | 4 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 231018-anomalne-awarie-athamarein)) |
| Ellarina Samarintael | 4 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Gabriel Lodowiec     | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Maja Samszar         | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Władawiec Diakon     | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Arnulf Perikas       | 3 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow)) |
| NekroTAI Zarralea    | 3 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Grażyna Burgacz      | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kirea Rialirat       | 2 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Leo Kasztop          | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Marcel Kulgard       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| OO Loricatus         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Salazar Bolza        | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| SCA Hadiah Emas      | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Szczepan Myrczek     | 2 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Szymon Wanad         | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alezja Dumorin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Alicja Szadawir      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Andrzej Gwozdnik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Axel Nargan          | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Błażej Sowiński      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Erwin Pies           | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Eustachy Korkoran    | 1 | ((231018-anomalne-awarie-athamarein)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gerwazy Kircznik     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hiacynt Samszar      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hubert Kerwelenios   | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Jakub Oroginiec      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Kaspian Certisarius  | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Klarysa Jirnik       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Klaudia Stryk        | 1 | ((231018-anomalne-awarie-athamarein)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leona Astrienko      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Remigiusz Alkarenit  | 1 | ((231018-anomalne-awarie-athamarein)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Sarian Xadaar        | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Tomasz Dojnicz       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Tomasz Ruppok        | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Tristan Rialirat     | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Uśmiechniczka Konstrukcjonistka Aurora Diakon | 1 | ((231018-anomalne-awarie-athamarein)) |