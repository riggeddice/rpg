---
categories: profile
factions: 
owner: public
title: OO Kariatyda
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210609-sekrety-kariatydy            | całkowicie autonomiczny Okręt Orbitera, który 6 lat temu został porwany przez TAI i uciekł do Anomalii Kolapsu. By nie było kłopotów, K1 ukrywa informację że Kariatyda kiedykolwiek istniała. | 0112-01-10 - 0112-01-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((210609-sekrety-kariatydy)) |
| Eustachy Korkoran    | 1 | ((210609-sekrety-kariatydy)) |
| Marian Tosen         | 1 | ((210609-sekrety-kariatydy)) |
| Rafael Galwarn       | 1 | ((210609-sekrety-kariatydy)) |
| Roland Sowiński      | 1 | ((210609-sekrety-kariatydy)) |