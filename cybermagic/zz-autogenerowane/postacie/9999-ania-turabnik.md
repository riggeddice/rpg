---
categories: profile
factions: 
owner: public
title: Ania Turabnik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230314-brudna-konkurencja-w-arachnoziem | sekretarka w ArachnoBuild i dziewczyna Fircjusza (brata Fiony), podejrzana przez Fionę o zmianę planów, okazuje się, że przekazywała dane nieświadomie pod wpływem magii mentalnej. Spuryfikowana przez Karolinusa. | 0095-06-20 - 0095-06-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Fiona Szarstasz      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Fircjusz Szarstasz   | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Julita Mopsarin      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Kacper Aczramin      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Karolinus Samszar    | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Laura Turabnik       | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |