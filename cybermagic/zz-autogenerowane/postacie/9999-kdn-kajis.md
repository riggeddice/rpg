---
categories: profile
factions: 
owner: public
title: KDN Kajis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230124-kjs-wygrac-za-wszelka-cene   | kończy się energia, więc Kajis ląduje w Verlenlandzie i maskuje się jako skała by ludzie na pokładzie zdobyli dlań jakąś energię. | 0095-06-23 - 0095-06-25 |
| 230307-kjs-stymulanty-szeptomandry  | obecny, zamaskowany, w niskim stanie energii. Nic szczególnego nie zrobił. | 0095-06-26 - 0095-06-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Apollo Verlen        | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Lidia Nemert         | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Lucas Oktromin       | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Marta Krissit        | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Żaneta Krawędź       | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Elena Verlen         | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Emilia Lawendowiec   | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Franciszek Korel     | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| Karol Atenuatia      | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Michał Waczarek      | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| Serena Krissit       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Wojciech Namczak     | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Zuzanna Kraczamin    | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |