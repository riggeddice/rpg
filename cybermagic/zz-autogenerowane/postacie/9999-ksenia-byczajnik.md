---
categories: profile
factions: 
owner: public
title: Ksenia Byczajnik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240306-potwornosc-powojenna-trzykwiatu | jej kuzynka nie dogadywała się z Latiszą i zniknęła. Ksenia zainicjowała poszukiwania i poszła do Jakuba, co zastartowało wszystko. Lojalna kuzynce, chciała jej szukać, więc musiała zostać zamknięta w piwnicy. Teraz - dołączyła do Pustogoru. | 0084-04-16 - 0084-04-21 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Jakub Kurbeczko      | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Latisza Warkolnicz   | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Natalia Pszaruk      | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Natan Wierzbitowiec  | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Petra Łomniczajew    | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Radosław Zientarmik  | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Ulrich Warkolnicz    | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Zofia Skorupniczek   | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Zuzanna Szagbin      | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |