---
categories: profile
factions: 
owner: public
title: OO Tezifeng
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231011-ekstaflos-na-tezifeng        | korweta szybkiego i dalekiego patrolowania przypisana do Castigatora; pełniła rolę jednostki celnej (celnik). Spotkała kralotha i Klaudiusz Terienak stał się ekstaflosem. Ostrzelana przez Infernię, wymaga solidnych napraw. | 0110-10-20 - 0110-10-22 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 231011-ekstaflos-na-tezifeng        | ostrzelana przez Infernię, potrzebuje dobrych 3 tygodni napraw, co najmniej. | 0110-10-22

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Elena Verlen         | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Eustachy Korkoran    | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Klaudiusz Terienak   | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Lars Kidironus       | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Leona Astrienko      | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Leszek Kurzmin       | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Natalia Gwozdnik     | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OO Castigator        | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OO Infernia          | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Raoul Lavanis        | 1 | ((231011-ekstaflos-na-tezifeng)) |