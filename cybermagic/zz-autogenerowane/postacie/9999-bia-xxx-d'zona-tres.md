---
categories: profile
factions: 
owner: public
title: BIA XXX d'Zona Tres
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210714-baza-zona-tres               | sprzężona anomalicznie z Klaudią, potencjalnie używa nanitek albo magii. Współpracuje z Infernią, biorąc ich częściowo za Alivię Nocturnę. Ma uszkodzoną percepcję i nie wie że jest chora. | 0111-05-14 - 0111-05-16 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210714-baza-zona-tres               | uszkodzona percepcja - nie widzi Lewiatana lub nie rejestruje jego obecności. Nie wie też o tym, że sama jest uszkodzona/chora. | 0111-05-16

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((210714-baza-zona-tres)) |
| Elena Verlen         | 1 | ((210714-baza-zona-tres)) |
| Eustachy Korkoran    | 1 | ((210714-baza-zona-tres)) |
| Janus Krzak          | 1 | ((210714-baza-zona-tres)) |
| Martyn Hiwasser      | 1 | ((210714-baza-zona-tres)) |
| Ulisses Kalidon      | 1 | ((210714-baza-zona-tres)) |