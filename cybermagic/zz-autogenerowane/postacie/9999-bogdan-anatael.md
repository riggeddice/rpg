---
categories: profile
factions: 
owner: public
title: Bogdan Anatael
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210512-ewakuacja-z-serenit          | kapitan Falołamacza, mag, dowódca kombinowanej akcji Eternia - Aurum do zrobienia floty i badania anomalii kosmicznych. Uratowany przez Infernię. Jego simulacrum krążyło po Falołamaczu... | 0111-11-23 - 0111-12-02 |
| 210630-listy-od-fanow               | wdzięczny Ariannie za ratunek jego i tylu ludzi z Falołamacza ilu się dało; pomoże jej w przyszłych odcinkach Sekretów Orbitera i w pozyskaniu Eidolona do ratowania kolejnych ludzi. | 0112-01-15 - 0112-01-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((210512-ewakuacja-z-serenit; 210630-listy-od-fanow)) |
| Elena Verlen         | 2 | ((210512-ewakuacja-z-serenit; 210630-listy-od-fanow)) |
| Klaudia Stryk        | 2 | ((210512-ewakuacja-z-serenit; 210630-listy-od-fanow)) |
| Aida Serenit         | 1 | ((210512-ewakuacja-z-serenit)) |
| AK Serenit           | 1 | ((210512-ewakuacja-z-serenit)) |
| Eustachy Korkoran    | 1 | ((210512-ewakuacja-z-serenit)) |
| Izabela Zarantel     | 1 | ((210630-listy-od-fanow)) |
| Martyn Hiwasser      | 1 | ((210512-ewakuacja-z-serenit)) |
| Michał Teriakin      | 1 | ((210630-listy-od-fanow)) |
| OE Falołamacz        | 1 | ((210512-ewakuacja-z-serenit)) |
| OE Lord Savaron      | 1 | ((210630-listy-od-fanow)) |
| Olgierd Drongon      | 1 | ((210630-listy-od-fanow)) |
| OO Infernia          | 1 | ((210512-ewakuacja-z-serenit)) |
| Rafael Galwarn       | 1 | ((210630-listy-od-fanow)) |
| Remigiusz Falorin    | 1 | ((210630-listy-od-fanow)) |
| Roland Sowiński      | 1 | ((210512-ewakuacja-z-serenit)) |
| TAI Rzieza d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI XT-723 d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI Zefiris          | 1 | ((210630-listy-od-fanow)) |