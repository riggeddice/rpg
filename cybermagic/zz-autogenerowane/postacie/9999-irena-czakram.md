---
categories: profile
factions: 
owner: public
title: Irena Czakram
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210912-medyk-przeciwko-cieniaszczytowi | terminuska Cieniaszczycka; od lat rozpracowywała eksperymenty BlackTech w Cieniaszczycie. Zablokowana przez przełożonych. Martyn Hiwasser wszedł z nią w sojusz i ją odblokował. Z wdzięczności dała cynk Orbiterowi, by nie zabili Martyna w celi. Ostrożni przyjaciele, acz trzyma Martyna na odległość kija bo maverick. | 0079-09-13 - 0079-09-16 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210912-medyk-przeciwko-cieniaszczytowi | odblokowana przez przełożonych; po działaniach Martyna już nie było po co chronić BlackTech. | 0079-09-16

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adela Kołczan        | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Adrian Kozioł        | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Franciszek Tocz      | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Kasia Pieprz         | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Martyn Hiwasser      | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Paweł Pieprz         | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |