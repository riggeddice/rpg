---
categories: profile
factions: 
owner: public
title: Olgierd Drongon
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "orbiter, zelazko"
* owner: "public"
* title: "Olgierd Drongon"


## Kim jest

### W kilku zdaniach

Niedźwiedziowaty, wyglądający na niezgrabnego i nieszczególnie bystrego kapitan fregaty Orbitera. W rzeczywistości - agent do operacji beznadziejnych i wybitny strzelec, którego doskonałe wyczucie niejednokrotnie uratowało sytuację. Generator ogromnych kosztów dla Orbitera.

### Co się rzuca w oczy

* TODO

### Jak sterować postacią

* Olgierd jest nieprawdopodobnie twardy i jego magia to wspiera. Robi rzeczy, które dla większości postaci są niemożliwe (bo ciało / umysł by nie wytrzymały). Jego manewry mogą być mordercze dla kogoś z innym ciałem niż on.
* Nie dba o prestiż czy co myślą o nim inni. Nie dba o koszty i politykę. Jest lojalny i jeśli przyjaciel potrzebuje pomocy, pomoże mu.
* Nie oczekuje od nikogo czegoś, czego sam by nie zrobił. Jest żywym przykładem epickości Orbitera. Larger-than-life figure. Zachęci, pomoże, podniesie. Niepowstrzymywalny. Paragon of Orbiter.
* Bardzo optymistyczny, z nieskończonym zapasem morale i pozytywnego podejścia do wszystkiego. Nigdy nie narzeka. Nie jest cyniczny. Sprawia wrażenie naiwnego.
* Nie podejdzie do dziewczyny, która mu się podoba. Tu jest nieśmiały.
* Nie jest mściwy ani złośliwy. Jest dobroduszny i sympatyczny. Ale zrób coś niegodnego a jego gniew będzie straszny.
* Lubi się wygłupiać i żyć pełnią życia. 
* Zachowuje zimną krew. Jest odpowiedzialny wobec załogi czy Orbitera.
* Zawsze się przygotowuje.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Wszedł do Aurum uratować pirata, wobec którego miał dług honorowy. Wkradł się do więzienia, po czym unieszkodliwił kilka osób, zastraszył resztę i wyszli stamtąd nieniepokojeni. Potem oddał się do dyspozycji Kramera.
* Gdy Anomalna Infernia wróciła z Anomalii Kolapsu, Olgierd przekonał Kramera by dal Żelazku misję zweryfikowania i albo uratowania Inferni albo jej egzekucji.
* Zamaskowany jako gladiator na Valentinie, zebrał informacje o przyszłej aktywności piratów.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* CECHA: Nieprawdopodobnie twardy i wytrzymały na wszystkich kanałach - magicznym, fizycznym, emocjonalnym, mentalnym. Niepowstrzymany juggernaut.
* AKCJA: Zastraszanie i dominacja. Niezależnie od tego z kim ma do czynienia, Olgierd potrafi zmusić innych do zrobienia tego czego sobie życzy.
* AKCJA: Inspiracja własnym przykładem. Z optymizmem i odwagą zrobi to, co jest potrzebne i pociągnie za sobą innych. Koło niego jest najbezpieczniej XD.
* AKCJA: Szturmowiec Orbitera. Walczy w zwarciu i z bliska. Specjalizuje się w nieczystej walce. Mistrz sztuk walki + walki nożem.
* AKCJA: Każda broń w rękach Olgierda to coś morderczego. Jest mistrzowskim artylerzystą. Zarówno na zasięg bliski jak i daleki, statkiem kosmicznym lub osobiście.
* AKCJA: Jest perfekcyjnie przygotowany i ma świetne wyczucie. Niekoniecznie wie jak dokładnie działa przeciwnik, ale wie DOKŁADNIE jak daleko on może się posunąć ze swoją załoga, pojazdem i sprzętem.
* COŚ: Słynie z nieoczekiwanych i nietypowych akcji i zachowań. Jego reputacja sprawia, że przeciwnicy niekoniecznie wiedzą czego się spodziewać. Słynie też z tego, że nie blefuje.

### Serce i Wartości (3)

* Tradycja
    * My jesteśmy Orbiterem. My jesteśmy po to, by inni żyli bezpiecznie. By mogli budować nowy, lepszy świat. My umieramy, by oni mogli żyć. To jest słuszne - silni bronią słabych.
    * Rozkaz to rozkaz. Nie musi się podobać. Ktoś wie, o co chodzi i cel nadrzędny przełożonych należy wykonać a samych przełożonych wspierać. Gadanie o tym jak głupi jest przełożony jest nieakceptowalne. Bunt jest nieakceptowalny.
    * Ważniejsze niż życie czy smierć jest LEGACY - to, co po Tobie zostanie. To, jak zmieniłeś świat. Wszyscy umrzemy. Ważne, by umrzeć ZA COŚ.
* Stymulacja 
    * Zgłasza się do najtrudniejszych misji, bo jego oddział sobie poradzi. Plus, bo tylko wtedy warto naprawdę żyć.
    * Wszystko próbuje przekształcić w grę z punktacją. Nie chodzi o to by wygrać. Chodzi o to JAK BARDZO wygrać. Nie ma sytuacji, których wygrać się nie da.
    * Wszystkie operacje wedle możliwości realizuje samemu, osobiście. Jeśli jest coś ryzykownego lub trudnego, chce zająć się tym osobiście.
    * Wybiera bardzo nieoczekiwane i pozornie bardzo ryzykowne rozwiązania, których często nie wybrałby ktoś „zdrowy psychicznie”.  Słynie z bardzo nieoczekiwanych działań.
* Samosterowność
    * Dług honorowy, lojalność, przyjaźń - znaczą dużo więcej niż rozkaz czy jakakolwiek frakcja. Na frontierze musisz podejmować decyzje nie mając dostępu do łańcucha dowodzenia.
    * Robi to, co uważa za słuszne, nawet, jeśli to jest niezgodne z rozkazami. W czym potem wróci do Kramera, powie dokładnie co zrobil i da się aresztować. Mimo wszystko szanuje Orbiter i zasady.
    * Niezależnie od tego jaka jest załoga, jakie masz środki, czego Ci brakuje - póki masz COKOLWIEK należy walczyć i zrobić wszystko by wykonać misję.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Nieważne jak wiele nie robimy i nie działamy, nigdy nie jesteśmy w stanie wygrać. Zawsze nasi umierają, zawsze jest jeszcze jedna bitwa."
* CORE LIE: "A warrior walks alone. Mogę mieć podwładnych, ale nie poproszę o pomoc. Nie mam komu się zwierzyć. Przeznaczeniem każdego wojownika jest samotna wyprawa na górę, która w końcu go pokona."
* Nie manipuluje, nie oszukuje, nie kłamie. Jest bardzo prostolinijny. Nie oszukuje nawet przez ominięcie mówienia o czymś. Jest najgorszym negocjatorem jakiego spotkasz. Po prostu jak nie chce to czegoś nie powie.
* Bardzo łatwy do wprowadzenia w pułapkę; nie chce nikogo zostawić w potrzebie samego. Nie zostawi umierającego statku kosmicznego jeśli może pomóc, mimo ryzyka.
* Zawsze spłaca swoje długi. Ale nigdy nie zaciąga ich w imieniu innych osób. Bardzo honorowy, we wszystkich znaczeniach tego słowa.
* Zupełnie nie radzi sobie z polityką i „dworem”. Nigdy nie awansuje i nie dostanie większej jednostki. Reputacja „maverick”.

### Magia (3M)

#### W czym jest świetna

* Korozja materii: potrafi rozpaść czy zdestabilizować różne mechanizmy lub materiały. W wypadku zaawansowanych narzędzi, może je eksplodować.
* Ekrany ochronne: pola siłowe, wzmocnienia materii, sfery kinetycznej ochrony. Efekty chroniące przed kanałami atakujacymi.
* Skanowanie słabych punktów: Olgierd często potrafi znaleźć jakie są słabe punkty materii, pojazdu czy struktury.

#### Jak się objawia utrata kontroli

* Devastating Collateral Damage. Eksplozje, rozpad, zniszczenie - a on w epicentrum.
* Rzeczywistość się lokalnie rozpada - teren staje się skrajnie niebezpieczny dla wszystkich; wymaga ewakuacji

### Specjalne

* .

## Inne

### Wygląd

* ?

### Coś Więcej

* .

### Endgame

* 


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221122-olgierd-lowca-potworow       | młodszy (23 lata), jeszcze nie tak rozsądny, poluje na niebezpiecznych ludzi i magów ('potwory'), zwłaszcza noktiańskich i Orbiterian, w ramach black ops z ramienia Orbitera. Szukał 'potwora' na Kartalianie, znalazł noktianina Lutusa Saraana (po drodze obijając wszystko na jego drodze) i wszedł z nim w sojusz - pomoże odzyskać 'brata' Lutusa Saraana z niewoli na Neikatis. Bardzo niebezpieczny; pozornie łagodny niedźwiedź, ale przechodzi 0-100 w moment. Niesamowicie wręcz szczery i bezpośredni. | 0096-07-11 - 0096-07-16 |
| 200708-problematyczna-elena         | dowódca Żelazka, niedźwiedziowaty, kompetentny i niebezpieczny. Ma agonalnie niską opinię o Ariannie - nie poradziła sobie z pilotażem na ĆWICZENIACH. | 0110-12-27 - 0111-01-01 |
| 200722-wielki-kosmiczny-romans      | kapitan Żelazka. Podśmiewuje się z Eustachego - mistrza podrywu. Ale nie chce krzywdy załogi Inferni. Humor mu zepsuło, bo podobno podrywa Elenę z Inferni... coś do rozwiązania. | 0111-01-10 - 0111-01-13 |
| 201125-wyscig-jako-randka           | od dawna pracuje nad Aleksandrią Ekstraktywną; wpadł w kolizję z Arianną. W pojedynku ścigaczy przegrał, ale to była doskonała walka. | 0111-04-08 - 0111-04-13 |
| 210120-sympozjum-zniszczenia        | Maria Naavas wpakowała go z Arianną na "randkę" w kapsule ratunkowej z zimnym life supportem. Arianna grzała magią a jak nie miała sił, Olgierd przejął i trzymał energię. | 0111-08-01 - 0111-08-05 |
| 210630-listy-od-fanow               | zrobił ostrą dywersję dla Arianny, przeciążenia Żelazkiem w okolicy tajnej bazy Eterni na K1. Potem na szybką prośbę Arianny ewakuował stamtąd ranną Elenę. | 0112-01-15 - 0112-01-18 |
| 210818-siostrzenica-morlana         | pomógł Ariannie dostać się na Netrahinę i skutecznie zestrzelił koloidowy statek. Trochę zazdrosny o Ariannę x Tomasza ;-). Pomógł Ariannie za wspólną kolację. | 0112-01-20 - 0112-01-24 |
| 210922-ostatnia-akcja-bohaterki     | Arianna przekonała go do pomocy prosto - "Roland ją podrywa. Halp". By ją ratować i się popisać prawie zniszczył Żelazko. Ale bez tego akcja byłaby niemożliwa. | 0112-02-23 - 0112-03-09 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 221122-olgierd-lowca-potworow       | zrobił PIEKIELNE wrażenie na lokalsach bazy XXX. Złe, bo pokrzywdził w samoobronie. Dobre, bo pomógł. | 0096-07-16
| 200722-wielki-kosmiczny-romans      | dowiaduje się, że ubiega się o rękę Eleny Verlen. Zupełnie mu się to nie podoba. | 0111-01-13
| 201125-wyscig-jako-randka           | jest zainteresowany Arianną Verlen. Uważa, że ta się w nim podkochuje, ale nie wie jak zagadać. Ale on też nie wie. Ma o niej (wreszcie!)opinię kompetentnej. | 0111-04-13
| 210108-ratunkowa-misja-goldariona   | Klaudia go wrobiła, że niby interesuje się "Uśmiechniętą" - statkiem Luxuritias, który zaginął. Nie wie o tym nic. | 0111-08-15
| 210630-listy-od-fanow               | na niego spadło "wykrycie tajnej bazy Eterni na K1" (plan Arianny). Nie ma nic przeciw temu, choć wpakował się w wojnę silnych interesów. | 0112-01-18

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 7 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210630-listy-od-fanow; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Klaudia Stryk        | 6 | ((200708-problematyczna-elena; 201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210630-listy-od-fanow; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Elena Verlen         | 5 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210120-sympozjum-zniszczenia; 210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki)) |
| Antoni Kramer        | 4 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Eustachy Korkoran    | 4 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana)) |
| OO Żelazko           | 4 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Leona Astrienko      | 3 | ((200722-wielki-kosmiczny-romans; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Maria Naavas         | 3 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana)) |
| Martyn Hiwasser      | 3 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210922-ostatnia-akcja-bohaterki)) |
| Diana Arłacz         | 2 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia)) |
| Izabela Zarantel     | 2 | ((210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki)) |
| AK Salamin           | 1 | ((200708-problematyczna-elena)) |
| Alicja Kirnan        | 1 | ((221122-olgierd-lowca-potworow)) |
| Bogdan Anatael       | 1 | ((210630-listy-od-fanow)) |
| Borys Uprakocz       | 1 | ((221122-olgierd-lowca-potworow)) |
| Damian Orion         | 1 | ((200722-wielki-kosmiczny-romans)) |
| Gunnar Brunt         | 1 | ((201125-wyscig-jako-randka)) |
| Jakub Altair         | 1 | ((221122-olgierd-lowca-potworow)) |
| Jolanta Kopiec       | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Julian Muszel        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Konrad Wolczątek     | 1 | ((200722-wielki-kosmiczny-romans)) |
| Leszek Kurzmin       | 1 | ((200708-problematyczna-elena)) |
| Lutus Saraan         | 1 | ((221122-olgierd-lowca-potworow)) |
| Maciek Kwaśnica      | 1 | ((221122-olgierd-lowca-potworow)) |
| Michał Teriakin      | 1 | ((210630-listy-od-fanow)) |
| Nataniel Morlan      | 1 | ((210818-siostrzenica-morlana)) |
| OA Bakałarz          | 1 | ((210120-sympozjum-zniszczenia)) |
| OE Lord Savaron      | 1 | ((210630-listy-od-fanow)) |
| Ofelia Morlan        | 1 | ((210818-siostrzenica-morlana)) |
| OO Aurelion          | 1 | ((200708-problematyczna-elena)) |
| OO Netrahina         | 1 | ((210818-siostrzenica-morlana)) |
| OO Welgat            | 1 | ((200722-wielki-kosmiczny-romans)) |
| Persefona d'Infernia | 1 | ((200708-problematyczna-elena)) |
| Rafael Galwarn       | 1 | ((210630-listy-od-fanow)) |
| Remigiusz Falorin    | 1 | ((210630-listy-od-fanow)) |
| Roland Sowiński      | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| SC Fecundatis        | 1 | ((210818-siostrzenica-morlana)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |
| Tadeusz Ursus        | 1 | ((200722-wielki-kosmiczny-romans)) |
| TAI Rzieza d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI XT-723 d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI Zefiris          | 1 | ((210630-listy-od-fanow)) |
| Tomasz Sowiński      | 1 | ((210818-siostrzenica-morlana)) |