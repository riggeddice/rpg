---
categories: profile
factions: 
owner: public
title: Anna Tessalon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231025-spiew-nielalki-na-castigatorze | tien na Castigatorze, bardzo wyczulona na wszystko co się dzieje i otwarta na wszystkie możliwości. Pierwsza chciała współpracować z Arianną, ale uszkodzenie Znaczeń jej to utrudniło. Za to jak dostała od Arianny kartkę potrafiła świetnie narysować przemyt, co uruchomiło Patryka. | 0110-10-24 - 0110-10-26 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 231025-spiew-nielalki-na-castigatorze | wstrząśnięta przez wydarzenia na Castigatorze z NieLalką. Potrzebuje więcej oparcia, boi się działać sama. Ale znalazła komfort w głębszej przyjaźni z Patrykiem. | 0110-10-26

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Elena Verlen         | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Eustachy Korkoran    | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Feliks Walrond       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Igor Arłacz          | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Klaudia Stryk        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Konstanty Keksik     | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Leona Astrienko      | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Leszek Kurzmin       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Marta Keksik         | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| OO Castigator        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| OO Infernia          | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Patryk Samszar       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Raoul Lavanis        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| TAI Eszara d'Castigator | 1 | ((231025-spiew-nielalki-na-castigatorze)) |