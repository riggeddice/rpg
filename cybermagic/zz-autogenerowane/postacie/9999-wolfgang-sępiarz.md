---
categories: profile
factions: 
owner: public
title: Wolfgang Sępiarz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240102-zaloga-vishaera-przezyje     | KIA. Wyłączył generatory Memoriam by ochronić swoją załogę, ale zapłacił za to życiem i człowieczeństwem swoim i oficerów-ochotników. Zaiste, "zszedł z okrętu ostatni". | 0109-10-26 - 0109-10-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazy Termann     | 1 | ((240102-zaloga-vishaera-przezyje)) |
| Fabian Korneliusz    | 1 | ((240102-zaloga-vishaera-przezyje)) |
| Helmut Szczypacz     | 1 | ((240102-zaloga-vishaera-przezyje)) |
| Klaudia Stryk        | 1 | ((240102-zaloga-vishaera-przezyje)) |
| Martyn Hiwasser      | 1 | ((240102-zaloga-vishaera-przezyje)) |