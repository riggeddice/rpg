---
categories: profile
factions: 
owner: public
title: Aleksander Muniakiewicz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201011-narodziny-paladynki-saitaera | Latający Rekin, chwilowo przejął kontrolę nad parkiem rozrywki Janor; próbował zatrzymać Pięknotkę i zauważył obecność Minerwy (acz ta się wyślizgnęła). | 0110-10-12 - 0110-10-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Agaton Ociegor       | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Gabriel Ursus        | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Minerwa Metalia      | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Pięknotka Diakon     | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Sabina Kazitan       | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Saitaer              | 1 | ((201011-narodziny-paladynki-saitaera)) |