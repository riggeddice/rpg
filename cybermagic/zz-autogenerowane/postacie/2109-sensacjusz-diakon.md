---
categories: profile
factions: 
owner: public
title: Sensacjusz Diakon
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "rekin, diakon, aurum, sowiński"
* owner: "public"
* title: "Sensacjusz Diakon"


## Kim jest

### W kilku zdaniach

Świetny inżynier biowojenny, którego świetlistą karierę przerwało to, że dostrzegł go Anastazy Sowiński i stwierdził, że KTOŚ musi zadbać o Rolanda Sowińskiego - wschodzącą gwiazdę paladyństwa Sowińskich który siedzi u Rekinów. I tak Sensacjusz skończył, niechętnie, w Podwiercie. Anastazy wziął go za LEKARZA, tylko bardziej kompetentnego w te klocki. A potem, gdy Roland opuścił ten teren, Anastazy o Sensacjuszu zapomniał. I wszyscy myślą, że Sensacjusz coś nieprawdopodobnie zbroił i jest na wygnaniu. Sensacjusz nie wie za co tu siedzi.

Drakolita, który miał bardzo niestabilną magię więc wykorzystano Matrycę Diakona by go ustabilizować i uratować. Ale Sensacjusz - ekspert od bioadaptacji - z biegiem lat usunął sobie naleciałości Diakońskie i stał się tym czym chciał być. Przez to jest odszczepieńcem rodu, ale jest zadowolony.

### Co się rzuca w oczy

* Nie jest najbardziej sympatyczny wobec Rekinów - jest dość oschły i rzeczowy. Unika kontaktów towarzyskich.
* Całkowicie ignoruje politykę, mówi to co chce i działa tak jak chce, bo jest ich jedynym i najlepszym lekarzem.

### Jak sterować postacią

* Jest udręczony. NIE CHCE tu być, nie ma dobrych warunków czy laboratorium. Musi zajmować się cholernymi Rekinami, czego bardzo nie chce bo go wkurzają. He enjoys the challenge. Ale Trzęsawisko to dla niego okazja, choć nie pokazuje tego Rekinom. Oni nie wiedzą.
* Adaptuje się - nieważne w jakiej sytuacji by nie był, z rezygnacją zakasa rękawy i zrobi to, co jest potrzebne.
* Ludzka natura nie jest dla niego świętością. Jest świetną kanwą. Dobrym punktem wyjścia. Ewolucja jest fantastyczna, możemy ją przyspieszyć.
* Problemy są ciekawe do rozwiązania, szkoda tylko, że przychodzi z nimi pacjent (lub klient)
* "Kiedyś miałem laboratorium, teraz mam glizdę medyczną..."

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Rekinowi urwało rękę. Sensacjusz odciął uczucie bólu, wyłączył szok i zaproponował - albo glizda albo wyhoduje tymczasowe szczypce kraba czy coś. Rekiny myślały, że to "power move" i odpowiednio doceniły. A on działał poważnie, na serio i nie zorientował się jak to odczytano.
* Raz na jakiś czas z Trzęsawiska Pięknotka czy Alan ściągają Sensacjusza. Za każdym razem dochodzi DALEJ lub działa SKUTECZNIEJ. Usprawnia się i próbuje się sprawdzić na najtrudniejszej biowojennej arenie.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: Świetnie walczy wręcz, całkowicie nieczysto. Praktycznie nie da się go zaskoczyć. Szkolenie (umiejętności) + wzmocnienie biomantyczne
* AKCJA: Umiejętności stabilizacji i utrzymania przy życiu. Jest w tym bardzo dobry, ale nie jest świetnym LEKARZEM. Taki medyk polowy z pet glizda.
* AKCJA: Udoskonala jednostki (ludzie, miślęgi itp). Bierze ludzką naturę i ją usprawnia, dostosowując do potrzeb i sytuacji. Celem jest usprawnienie jednostki w przestrzeni lub pomoc pokonania wyzwania.
* AKCJA: Tworzy i usprawnia jednostki biowojenne. Jego glizda jest praktycznie jego symbiontem i proto-laboratorium.
* CECHA: Bardzo silna regeneracja biomantyczna i Wzoru. Nawet, jeśli się poważnie uszkodzi lub Skazi, szybko się odbuduje samoistnie.
* COŚ: Zaawansowana glizda medyczna. Służy za biosyntetyzator i mechanizm leczniczy. Plus, efekt "fuj".
* COŚ: Wiktor Satarail zezwala Sensacjuszowi na obecność na Trzęsawisku. Nie podejmuje wobec Sensacjusza negatywnych działań, bo Sensacjusz szanuje Trzęsawisko.
* COŚ: 

### Serce i Wartości (3)

* Osiągnięcia
    * Każdy ma potencjał. Każdemu pomogę osiągnąć pełnię potencjału dla tej osoby.
    * Zanim jest skłonny zaaplikować komuś Modyfikację, wpierw testuje ją na sobie.
    * Musisz zweryfikować empirycznie czy Modyfikacja jest skuteczna. Proces iteracyjny.
* Pokora
    * Don't force it. Wie, że może pomóc, ale szanuje granice innych. Nie chce zaszkodzić wprowadzając za szybkie zmiany w populacji.
    * Nie jest rewolucjonistą. Nie zależy mu na chwale. Chce osiągnąć perfekcję i pomóc - tam, gdzie go poproszą lub to jest potrzebne.
    * Cokolwiek się nie stanie, zaadaptuję się i zwyciężę - iteracyjnie.
* Uniwersalizm
    * Nieważne czym jesteś, I can make you better. And I am making myself better every day.
    * Co mogę wydobyć ucząc się Trzęsawiska i jak daleko możemy z tym pójść?
    * 

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Nie jestem tym czym miałem być. Nie jestem tym, czym chciałem być."
* CORE LIE: "Muszę odnaleźć swoje miejsce i swoją formę w egzystencji. Nie mam czasu na nich wszystkich. Tylko ewolucją mogę naprawić błąd rzeczywistości."
* Ocena stanu emocjonalnego i prawdomówności, zwłaszcza płaczącej dziewczyny. Jest podatny na manipulację.
* Odszczepieniec rodu Diakon - usunął sobie dominujące cechy własną inżynierią biomagiczną.

### Magia (3M)

#### W czym jest świetna

* 

#### Jak się objawia utrata kontroli

* 

### Specjalne

* .

## Inne

### Wygląd

* ?

### Coś Więcej

* .

### Endgame

* 


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211120-glizda-ktora-leczy           | inżynier biowojenny, który na skutek "pomyłki" trafił na prowincję (Podwiert) być lekarzem Rekinów. Odkrył, że Roland jest Esuriitowy i wpakował go w glizdę i uratował. Potem w trybie bojowym "wilkołaka" odpędził mafię - zmienił im cost-to-benefit ratio. | 0108-04-07 - 0108-04-18 |
| 210824-mandragora-nienawidzi-rekinow | jest PEWNY, że Marysia Sowińska współpracuje z mafią. Świetny lekarz, opłacany przez wszystkie rody Aurum by ratować głupie Rekiny. Nie chce tu być. Zapalił się pomagając Danielowi - Daniel był dotknięty mandragorą, co Sensacjusz wykrył i naprawił. Aha, uważa, że Marysia jest jak Amelia. | 0111-07-03 - 0111-07-05 |
| 211012-torszecki-pokazal-kregoslup  | chroni Torszeckiego przed Karoliną; uważa ją (nieuczciwie) za posłańca Marysi Sowińskiej. | 0111-07-29 - 0111-07-30 |
| 211026-koszt-ratowania-torszeckiego | Marysia go wkręciła, że ona x Torszecki i to łyknął. Pozwolił im się spotykać i kibicuje gorąco ich związkowi (mezaliansowi?). | 0111-08-01 - 0111-08-05 |
| 211102-satarail-pomaga-marysi       | wyleczył Karolinę z infekcji i używa Diakońskiej Glisty Medycznej. Sam jest zarażony - Karo i Marysia wepchnęły go w glistę. Nawet zarażony jest lekarzem. | 0111-08-09 - 0111-08-10 |
| 211207-gdy-zabraknie-pradu-rekinom  | ma awaryjny generator prądu w szpitalu; Marysia dostała do niego dostęp by przywrócić prąd. Ale przez to musiał wsadzić Lorenę w glizdę medyczną. | 0111-08-30 - 0111-08-31 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211120-glizda-ktora-leczy           | ma fałszywe dossier u Sowińskich - jest wybitnym lekarzem, kobieciarzem i ma niezwykłą charyzmę. Zesłany na prowincję. Sęk w tym, że TAMTEN Sensacjusz nie żyje. Pomylili osobę. | 0108-04-18
| 211026-koszt-ratowania-torszeckiego | zmienia opinię na temat Marysi Sowińskiej. To zagubiona młoda kobieta, przemierzająca skomplikowany polityczny świat. Ale zdolna do miłości. Nie to co Amelia. | 0111-08-05

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 5 | ((210824-mandragora-nienawidzi-rekinow; 211012-torszecki-pokazal-kregoslup; 211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi; 211207-gdy-zabraknie-pradu-rekinom)) |
| Karolina Terienak    | 4 | ((210824-mandragora-nienawidzi-rekinow; 211012-torszecki-pokazal-kregoslup; 211102-satarail-pomaga-marysi; 211207-gdy-zabraknie-pradu-rekinom)) |
| Amelia Sowińska      | 3 | ((210824-mandragora-nienawidzi-rekinow; 211012-torszecki-pokazal-kregoslup; 211120-glizda-ktora-leczy)) |
| Rafał Torszecki      | 3 | ((211012-torszecki-pokazal-kregoslup; 211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi)) |
| Daniel Terienak      | 2 | ((210824-mandragora-nienawidzi-rekinow; 211207-gdy-zabraknie-pradu-rekinom)) |
| Lorena Gwozdnik      | 2 | ((211102-satarail-pomaga-marysi; 211207-gdy-zabraknie-pradu-rekinom)) |
| Wiktor Satarail      | 2 | ((211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi)) |
| Amanda Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Arkadia Verlen       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Arnold Kłaczek       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Ekaterina Zajcew     | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Ernest Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Ernest Namertel      | 1 | ((211102-satarail-pomaga-marysi)) |
| Feliks Keksik        | 1 | ((211120-glizda-ktora-leczy)) |
| Henryk Wkrąż         | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Hestia d'Rekiny      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Ignacy Myrczek       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Jolanta Sowińska     | 1 | ((211012-torszecki-pokazal-kregoslup)) |
| Justynian Diakon     | 1 | ((211120-glizda-ktora-leczy)) |
| Kacper Bankierz      | 1 | ((211120-glizda-ktora-leczy)) |
| Karol Pustak         | 1 | ((211120-glizda-ktora-leczy)) |
| Ksenia Kirallen      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Laura Tesinik        | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Lucjan Sowiński      | 1 | ((211012-torszecki-pokazal-kregoslup)) |
| Marek Samszar        | 1 | ((211102-satarail-pomaga-marysi)) |
| Natalia Tessalon     | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Olga Myszeczka       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Oliwia Lemurczak     | 1 | ((211120-glizda-ktora-leczy)) |
| Paweł Szprotka       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Roland Sowiński      | 1 | ((211120-glizda-ktora-leczy)) |
| Sabina Kazitan       | 1 | ((211120-glizda-ktora-leczy)) |
| Stella Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Teresa Mieralit      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Tomasz Tukan         | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Urszula Arienik      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |