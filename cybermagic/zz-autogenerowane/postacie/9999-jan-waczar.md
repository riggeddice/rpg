---
categories: profile
factions: 
owner: public
title: Jan Waczar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190702-smieciornica-w-magitrowni    | rolnik zmartwiony stanem swoich pól i śmieciornicą w magitrowni. No i synem, który włóczy się po tych niebezpiecznych terenach. | 0109-07-14 - 0109-07-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alia Naszemba        | 1 | ((190702-smieciornica-w-magitrowni)) |
| Irek Waczar          | 1 | ((190702-smieciornica-w-magitrowni)) |
| Klara Orkaczyn       | 1 | ((190702-smieciornica-w-magitrowni)) |
| Patrycja Radniak     | 1 | ((190702-smieciornica-w-magitrowni)) |
| Przemysław Grumcz    | 1 | ((190702-smieciornica-w-magitrowni)) |