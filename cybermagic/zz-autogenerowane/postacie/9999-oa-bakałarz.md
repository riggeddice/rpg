---
categories: profile
factions: 
owner: public
title: OA Bakałarz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210120-sympozjum-zniszczenia        | statek naukowy między rodami Aurum; organizuje Sympozjum Zniszczenia gdzie rozpracowywali dziwny materiał z Anomalii Kolapsu. | 0111-08-01 - 0111-08-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((210120-sympozjum-zniszczenia)) |
| Diana Arłacz         | 1 | ((210120-sympozjum-zniszczenia)) |
| Elena Verlen         | 1 | ((210120-sympozjum-zniszczenia)) |
| Eustachy Korkoran    | 1 | ((210120-sympozjum-zniszczenia)) |
| Klaudia Stryk        | 1 | ((210120-sympozjum-zniszczenia)) |
| Maria Naavas         | 1 | ((210120-sympozjum-zniszczenia)) |
| Martyn Hiwasser      | 1 | ((210120-sympozjum-zniszczenia)) |
| Olgierd Drongon      | 1 | ((210120-sympozjum-zniszczenia)) |