---
categories: profile
factions: 
owner: public
title: Marzena Marius
---

# {{ page.title }}


# Generated: 



## Fiszki


* była komodor Orbitera, która zdecydowała się na zbudowanie lepszego życia na Neikatis (ma ok. 15 doświadczonych osób); wie o farighanach | @ 230816-orbiter-nihilus-i-ruch-oporu-w-nativis
* OCEAN: (C+A+): Emanuje spokojem i pewnością siebie, potrafi zjednać sobie tłumy swoją retoryką i charyzmą. "gdy z nią rozmawiasz, czujesz jej głęboką wiarę w jej przekonania" | @ 230816-orbiter-nihilus-i-ruch-oporu-w-nativis
* VALS: (Power, Security): Jej zdaniem kontrakt społeczny ma stabilizować systemy "Każdy system jest tyle wart, ile stabilności może przynieść." | @ 230816-orbiter-nihilus-i-ruch-oporu-w-nativis
* Core Wound - Lie: "brak zdecydowanego działania doprowadził już do tej samej tragedii" - "Moja wizja to jedyna droga do pokoju" | @ 230816-orbiter-nihilus-i-ruch-oporu-w-nativis
* Styl: nie unika trudnych tematów, podaje je w przemyślany, logiczny sposób. Jej retoryka koncentruje się na potrzebie jedności i silnego kierownictwa | @ 230816-orbiter-nihilus-i-ruch-oporu-w-nativis
* metakultura: Atarien: "prawdziwa siła leży w jedności pod właściwym kierownictwem" | @ 230816-orbiter-nihilus-i-ruch-oporu-w-nativis

### Wątki


kidiron-zbawca-nativis
neikatianska-gloria-saviripatel

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230816-orbiter-nihilus-i-ruch-oporu-w-nativis | niesamowicie dobry polityk i taktyk, eks-Orbiter i idealistka; zażądała wydania przez Infernię ludzi z Inferni co skrzywdzili niewinnych (eks=piratów) oraz Laurencjusza i Feliksa Kidironów. Spokojnie umacnia rząd dusz, fortyfikuje pozycje i pozycjonuje się jako sędzia. Ardilla nie jest w stanie jej łatwo politycznie wymanewrować. | 0093-03-30 - 0093-03-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((230816-orbiter-nihilus-i-ruch-oporu-w-nativis)) |
| Eustachy Korkoran    | 1 | ((230816-orbiter-nihilus-i-ruch-oporu-w-nativis)) |
| Kalia Awiter         | 1 | ((230816-orbiter-nihilus-i-ruch-oporu-w-nativis)) |
| Marcel Draglin       | 1 | ((230816-orbiter-nihilus-i-ruch-oporu-w-nativis)) |