---
categories: profile
factions: 
owner: public
title: Oliwia Lemurczak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211120-glizda-ktora-leczy           | wszyscy podejrzewają, że to ona kazała Sabinie Kazitan Skazić Rolanda Sowińskiego. Sytuacja wymknęła jej się spod kontroli, ale nic nie może zrobić. | 0108-04-07 - 0108-04-18 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211120-glizda-ktora-leczy           | Stella Sowińska (pośrednio: sporo Sowińskich) jest święcie przekonana, że to ona stoi za Skażeniem Rolanda Esuriit. Będzie vendetta. | 0108-04-18

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Amelia Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Ernest Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Feliks Keksik        | 1 | ((211120-glizda-ktora-leczy)) |
| Justynian Diakon     | 1 | ((211120-glizda-ktora-leczy)) |
| Kacper Bankierz      | 1 | ((211120-glizda-ktora-leczy)) |
| Karol Pustak         | 1 | ((211120-glizda-ktora-leczy)) |
| Roland Sowiński      | 1 | ((211120-glizda-ktora-leczy)) |
| Sabina Kazitan       | 1 | ((211120-glizda-ktora-leczy)) |
| Sensacjusz Diakon    | 1 | ((211120-glizda-ktora-leczy)) |
| Stella Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |