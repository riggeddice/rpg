---
categories: profile
factions: 
owner: public
title: Zenobia Samszar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230905-druga-tienka-przybywa-na-pomoc | matka Eleny; wyjaśniła jej sytuację w jakiej się znajduje, kto stoi za Eleną i kto przeciw. Dała Elenie list polecający i wyjaśniła, że ona kiedyś walczyła z siłami Saitaera współpracując z Orbiterem. Zrobiła wszystko co mogła. | 0095-10-05 - 0095-10-07 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adelajda Kalmiris    | 1 | ((230905-druga-tienka-przybywa-na-pomoc)) |
| Cyprian Mirisztalnik | 1 | ((230905-druga-tienka-przybywa-na-pomoc)) |
| Elena Samszar        | 1 | ((230905-druga-tienka-przybywa-na-pomoc)) |
| Mikołaj Larnecjat    | 1 | ((230905-druga-tienka-przybywa-na-pomoc)) |