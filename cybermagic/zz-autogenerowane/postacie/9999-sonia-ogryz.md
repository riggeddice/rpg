---
categories: profile
factions: 
owner: public
title: Sonia Ogryz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200115-pech-komodora-ogryza         | żona Dominika Ogryza; z ambicjami. Absolutnie zachwycona Janet Erwon - oddała jej swój statek Rozbójnik. Marzy o przygodach! | 0110-07-20 - 0110-07-23 |
| 200129-nieudana-niechetna-inwazja   |  | 0110-10-22 - 0110-10-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Janet Erwon          | 2 | ((200115-pech-komodora-ogryza; 200129-nieudana-niechetna-inwazja)) |
| Dominik Ogryz        | 1 | ((200115-pech-komodora-ogryza)) |
| Kaja Tamaris         | 1 | ((200129-nieudana-niechetna-inwazja)) |
| Klaudia Stryk        | 1 | ((200115-pech-komodora-ogryza)) |
| Laura Orion          | 1 | ((200129-nieudana-niechetna-inwazja)) |
| Marian Kurczak       | 1 | ((200129-nieudana-niechetna-inwazja)) |
| Martyn Hiwasser      | 1 | ((200115-pech-komodora-ogryza)) |
| Rufus Karmazyn       | 1 | ((200129-nieudana-niechetna-inwazja)) |
| Wioletta Keiril      | 1 | ((200115-pech-komodora-ogryza)) |