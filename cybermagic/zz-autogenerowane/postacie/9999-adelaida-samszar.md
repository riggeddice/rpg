---
categories: profile
factions: 
owner: public
title: Adelaida Samszar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230418-zywy-artefakt-w-gwiazdoczach | (LONG DEAD), duch Samszarów, kiedyś potężna czarodziejka dowodząca studenckimi akademikami w Gwiazdoczach. Ciepła, ale twarda i pomocna. Tym razem działania Neidrii sprawiły, że jej imię zostało użyte przez Sarę Mazirin; podobno to ona (Adelaida) karze ludzi. | 0094-10-04 - 0094-10-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antonina Blakenbauer | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Elena Samszar        | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Joachim Pulkmocz     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Neidria Lazvarin     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Robinson Porzecznik  | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Sara Mazirin         | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |