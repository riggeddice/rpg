---
categories: profile
factions: 
owner: public
title: Arazille
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220119-sekret-samanty-arienik       | z jej woli ród Arieników został chroniony przed tym, że wszyscy praktycznie zginęli (poza Sławomirem i Ulą). Wyegzorcyzmowana przez Pustogor i Talię Aegis. Zabrała ze sobą Sławomira Arienika. | 0085-07-21 - 0085-07-23 |
| 181128-upiory-w-biurowcu            | pojawiła się w Czerwinorze i została wyegzorcyzmowana Artylerią Koszmarów zrobioną przez Wiktora i Pięknotkę. Poświęciła się ku temu grupa ludzi. | 0109-10-24 - 0109-10-25 |
| 181226-finis-vitae                  | pomogła Pięknotce wprowadzić chaos do sieci autowara Finis Vitae oraz zagnieździła się w tamtej okolicy. | 0109-11-18 - 0109-11-27 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181226-finis-vitae                  | zagnieździła się w okolicy autowara Finis Vitae. Uzyskała obietnicę Pięknotki, że ta pomoże. | 0109-11-27

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amadeusz Sowiński    | 1 | ((181226-finis-vitae)) |
| Atena Sowińska       | 1 | ((181226-finis-vitae)) |
| Błażej Arienik       | 1 | ((220119-sekret-samanty-arienik)) |
| Felicjan Szarak      | 1 | ((220119-sekret-samanty-arienik)) |
| Klaudia Stryk        | 1 | ((220119-sekret-samanty-arienik)) |
| Ksenia Kirallen      | 1 | ((220119-sekret-samanty-arienik)) |
| Maria Arienik        | 1 | ((220119-sekret-samanty-arienik)) |
| Moktar Gradon        | 1 | ((181226-finis-vitae)) |
| Pięknotka Diakon     | 1 | ((181226-finis-vitae)) |
| Samanta Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| Sławomir Arienik     | 1 | ((220119-sekret-samanty-arienik)) |
| Talia Aegis          | 1 | ((220119-sekret-samanty-arienik)) |
| Urszula Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| Waleria Cyklon       | 1 | ((181226-finis-vitae)) |