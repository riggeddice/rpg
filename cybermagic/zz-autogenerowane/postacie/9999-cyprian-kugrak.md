---
categories: profile
factions: 
owner: public
title: Cyprian Kugrak
---

# {{ page.title }}


# Generated: 



## Fiszki


* salvager Serien (hired muscle / combat) faeril: tylko ludzkie formy | @ 230104-to-co-zostalo-po-burzy
* ENCAO:  0-0++ |Unika nieprzyjemnej roboty;;Lubi pochwały| VALS: Hedonism, Family| DRIVE: Bezpieczeństwo swego dziecka | @ 230104-to-co-zostalo-po-burzy
* "Nienawidzę tych wszystkich anomalicznych gówien..." | @ 230104-to-co-zostalo-po-burzy
* kadencja: pomocny, dobrą rękę poda | @ 230104-to-co-zostalo-po-burzy

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230104-to-co-zostalo-po-burzy       | nie pozwolił Zofii iść ryzykować; ona jest jedynym medykiem. Wie, że jest najmniej potrzebny i bierze groźne role na siebie. Gentle ribbing Eustachego bo Kalia ;-). | 0092-10-26 - 0092-10-28 |
| 230125-whispraith-w-jaskiniach-neikatis | wpadł w lekką panikę, ale dalej trzyma fason; pomaga wszystkim najlepiej jak umie. Wypieprzył scutier Benka poza Seiren a potem go przejechał. Halucynacje przez whispraitha. Uratował dziewczyny, które wpadły do jaskini - ale swoim kosztem. | 0092-10-29 - 0092-11-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aniela Myszawcowa    | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Anna Seiren          | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Antoni Grzypf        | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Eustachy Korkoran    | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| JAN Seiren           | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| JAN Uśmiech Kamili   | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Michał Uszwon        | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Rufus Seiren         | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Zofia d'Seiren       | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Kalia Awiter         | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Kornelia Lichitis    | 1 | ((230125-whispraith-w-jaskiniach-neikatis)) |
| Rafał Kidiron        | 1 | ((230104-to-co-zostalo-po-burzy)) |