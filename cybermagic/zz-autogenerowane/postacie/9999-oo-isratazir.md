---
categories: profile
factions: 
owner: public
title: OO Isratazir
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240124-mikiptur-zemsta-woltaren     | Mimo ciężkich uszkodzeń i strat spowodowanych przez Dewastatora z Mikiptur, załoga wykazała wyjątkową odporność i determinację w obronie statku, co umożliwiło późniejsze operacje ratunkowe i minimalizację strat. | 0110-12-29 - 0111-01-01 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Bladawir      | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Arianna Verlen       | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Eustachy Korkoran    | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Klaudia Stryk        | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Lars Kidironus       | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| OA Mikiptur          | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Raoul Lavanis        | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Władawiec Diakon     | 1 | ((240124-mikiptur-zemsta-woltaren)) |