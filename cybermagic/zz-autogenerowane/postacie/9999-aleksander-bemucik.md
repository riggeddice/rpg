---
categories: profile
factions: 
owner: public
title: Aleksander Bemucik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201013-pojedynek-akademia-rekiny    | woźny-militarysta w Akademii Magicznej Zaczęstwa; staje twardo po stronie studentów AMZ. Nosi flashbangi itp. Kiedyś wojownik Zjednoczenia Letyckiego. | 0110-10-14 - 0110-10-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ignacy Myrczek       | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Julia Kardolin       | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Justynian Diakon     | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Kacper Bankierz      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Liliana Bankierz     | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Napoleon Bankierz    | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Remor 340D           | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Robert Pakiszon      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Stella Armadion      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Teresa Mieralit      | 1 | ((201013-pojedynek-akademia-rekiny)) |