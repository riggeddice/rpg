---
categories: profile
factions: 
owner: public
title: Remigiusz Błyszczyk
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211013-szara-nawalnica              | mag z Savery; kartograf + pływy magii + analiza Bram. Przesłuchany przez mentalistów Termii, wyjaśnił co wie o sektorze Mevilig. | 0112-03-18 - 0112-03-23 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksandra Termia    | 1 | ((211013-szara-nawalnica)) |
| Arianna Verlen       | 1 | ((211013-szara-nawalnica)) |
| Elena Verlen         | 1 | ((211013-szara-nawalnica)) |
| Eustachy Korkoran    | 1 | ((211013-szara-nawalnica)) |
| Klaudia Stryk        | 1 | ((211013-szara-nawalnica)) |
| Martyn Hiwasser      | 1 | ((211013-szara-nawalnica)) |
| Otto Azgorn          | 1 | ((211013-szara-nawalnica)) |
| TAI Rzieza d'K1      | 1 | ((211013-szara-nawalnica)) |