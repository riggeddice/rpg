---
categories: profile
factions: 
owner: public
title: Borys Uprakocz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221122-olgierd-lowca-potworow       | nożownik pracujący pod Lutusem Saraanem; dał się przekonać Olgierdowi że lepiej nie wyciągać nań noża. Najpewniej pracuje dla potężnego syndykatu na planecie ale ich nie chce wspierać i nie podoba mu się co tam się dzieje. | 0096-07-11 - 0096-07-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alicja Kirnan        | 1 | ((221122-olgierd-lowca-potworow)) |
| Jakub Altair         | 1 | ((221122-olgierd-lowca-potworow)) |
| Lutus Saraan         | 1 | ((221122-olgierd-lowca-potworow)) |
| Maciek Kwaśnica      | 1 | ((221122-olgierd-lowca-potworow)) |
| Olgierd Drongon      | 1 | ((221122-olgierd-lowca-potworow)) |