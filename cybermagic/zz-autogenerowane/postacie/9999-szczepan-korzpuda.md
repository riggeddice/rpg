---
categories: profile
factions: 
owner: public
title: Szczepan Korzpuda
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181116-michal-w-otchlani-wezosmokow | xxTerrorxx, nie ma życia. Bully, świnia i nieprzyjemny typek. Przez niego doszło do Skażenia pola magicznego w Żarni. | 0103-09-13 - 0103-09-15 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181116-michal-w-otchlani-wezosmokow | kocha Smocze Wojny, ale nie ma życia jako xxTerrorxx - Zaczęstwiacy na niego polują. | 0103-09-15

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Wroński         | 1 | ((181116-michal-w-otchlani-wezosmokow)) |
| Maciej Korzpuda      | 1 | ((181116-michal-w-otchlani-wezosmokow)) |
| Olga Myszeczka       | 1 | ((181116-michal-w-otchlani-wezosmokow)) |
| Piotr Ryszardowiec   | 1 | ((181116-michal-w-otchlani-wezosmokow)) |
| Robert Einz          | 1 | ((181116-michal-w-otchlani-wezosmokow)) |
| Zdzich Aprantyk      | 1 | ((181116-michal-w-otchlani-wezosmokow)) |
| Zuzanna Klawornia    | 1 | ((181116-michal-w-otchlani-wezosmokow)) |