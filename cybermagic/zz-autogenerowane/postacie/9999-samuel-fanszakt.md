---
categories: profile
factions: 
owner: public
title: Samuel Fanszakt
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240110-wieczna-wojna-bladawira      | mag służący za puryfikatora i bariery. W projekcie ekstrakcji danych o kralocie, uległ Skażeniu kralotycznemu. Bladawir go shutdownował gazem z customowego Lancera zanim coś się stało ;-). | 0110-12-24 - 0110-12-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Bladawir      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Arianna Verlen       | 1 | ((240110-wieczna-wojna-bladawira)) |
| Dormand Miraris      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Ernest Bankierz      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Kazimierz Darbik     | 1 | ((240110-wieczna-wojna-bladawira)) |
| Klaudia Stryk        | 1 | ((240110-wieczna-wojna-bladawira)) |
| Marian Witaczek      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Talia Miraris        | 1 | ((240110-wieczna-wojna-bladawira)) |
| Władawiec Diakon     | 1 | ((240110-wieczna-wojna-bladawira)) |
| Zaara Mieralit       | 1 | ((240110-wieczna-wojna-bladawira)) |