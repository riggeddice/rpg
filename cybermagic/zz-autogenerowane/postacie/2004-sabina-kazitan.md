---
categories: profile
factions: 
owner: public
title: Sabina Kazitan
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "Arystokracja Aurum, Banda Lemurczaka, Nocny Krąg"
* owner: "public"
* title: "Sabina Kazitan"


## Kim jest

### Motto

"Odzyskam kontrolę, bo posunę się dalej niż Ty. Wszystko, co nie jest niezbędne, można odrzucić."

### Paradoksalny Koncept

Arystokratka, podległa Kamilowi Lemurczakowi. Badaczka zakazanych i niebezpiecznych rytuałów o bardzo silnej woli i niezłomnej, własnej moralności. Wyrzeźbiona jako potwór przez Lemurczaka, nauczyła się bezwzględności by chronić swoich bliskich. Degeneratka i miłośniczka imprez do utraty przytomności, która poświęci się dla kogoś kogo uważa za wartego przetrwania. Tak bardzo kocha wolność i samostanowienie, że jest skłonna spętać w kajdanach wszystkich innych.

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: badania i niebezpieczne rytuały; wiedza jest bronią
* ATUT: bardzo silna wola i rdzeń mentalno-emocjonalny; trudno ją ruszyć
* SŁABA: ciągnie do używek, pragnie czuć się wolna i swobodna
* SŁABA: negocjacje typu 'równy z równym'. ALBO jest silna ALBO słaba.

### O co walczy (3)

* ZA: gromadzenie władzy, wiedzy, potęgi; wiecznie jej mało
* ZA: zbudować własne, lepsze społeczeństwo z nią na szczycie
* VS: chce zniszczyć system arystokratyczny Aurum
* VS: zniszczyć wszystko powiązane z Lemurczakiem, winnych i niewinnych

### Znaczące Czyny (3)

* Torturami zmusiła służkę, która ją szpiegowała do stania się podwójnym agentem.
* Traktowana jak niewolnica, zniszczyła laboratorium Lemurczaka podmieniając reagenty.
* Po tym, jak wszyscy ją zostawili, skutecznie zarządzała (niewielkim) majątkiem samotnie.

### Kluczowe Zasoby (3)

* COŚ: Robot klasy 'Serratus' z TAI klasy Elainka; do walki i tortur. Imię: "Seilia", jak bogini.
* KTOŚ: Krąg Nocy - organizacja radykalnej ewolucji i wolności
* WIEM: sporo drobnych sekretów słabych aktorów Aurum; materiały do szantażu
* OPINIA: bezwzględna i niezłomna; zawsze w służbie Wielkich Aurum

## Inne

### Wygląd

Świdrujące, niezwykle intensywne zielone oczy. Śnieżnobiałe włosy, do ramion. Lekko opalona, zdrowa cera. Stosunkowo niska. Ubrana w kobiecy frak z zaklęciami autoczyszczenia.

### Coś Więcej

Bardzo niebezpieczna postać, zarówno jako sojusznik jak i przeciwnik. Inteligentna, oportunistyczna i zawsze dba o swoje przetrwanie.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211120-glizda-ktora-leczy           | przekazana przez Oliwię Rolandowi, skusiła go możliwością pokonania Kajrata i wkręciła go w Esuriit. Zdominowana przez Rolanda, jednak tak naprawdę to ona wygrywa. | 0108-04-07 - 0108-04-18 |
| 200219-perfekcyjna-corka            | wysłana przez Lemurczaka by zobaczyć co Kardamacz ma wartościowego, dyskretnie wzmocniła Liberatis i rozwaliła plany Kardamacza. Bo Lemurczak. Mistrzyni potężnych energii. | 0110-05-16 - 0110-05-19 |
| 191112-korupcja-z-arystokratki      | arystokratka. Dla przyjemności znęcała się nad Myrczkiem, sadystycznie. Pięknotka wywaliła ją z terenu i zniszczyła jej robota bojowego klasy Serratus. | 0110-07-04 - 0110-07-05 |
| 200414-arystokraci-na-trzesawisku   | zmieniona w seksbombę dla przyjemności Franciszka, zemściła się - wprowadziła Franciszka na Trzęsawisko i z lubością patrzyła na jego dewastację. Zeznała wszystko. | 0110-08-04 - 0110-08-05 |
| 200417-nawolywanie-trzesawiska      | ma koszmary przez Trzęsawisko, które ją nawołują. Próbuje sama rozwiązać problem do momentu zdradzenia jej sekretu przed Pięknotką. Oddała Pięknotce nad sobą kontrolę. | 0110-08-12 - 0110-08-14 |
| 200418-wojna-trzesawiska            | leży w szpitalu z Ignacym Myrczkiem; próbuje nie dać się zwariować od choroby i od opowieści o grzybach. | 0110-08-16 - 0110-08-21 |
| 200509-rekin-z-aurum-i-fortifarma   | z przyjemnością przekazała niebezpieczny rytuał Tessalonom i ostrzegła o tym Pięknotkę. Z przyjemnością patrzyła na zniszczenie Tadeusza Tessalona i ból Natalii. | 0110-08-30 - 0110-09-04 |
| 200510-tajna-baza-orbitera          | skonfliktowana arystokratka Aurum. Skonfrontowana z Natalią przyjmowała na chłodno aż Myrczek się wtrącił. By go chronić przed Natalią, zraniła go. Aresztowana. | 0110-09-07 - 0110-09-11 |
| 191113-jeden-problem-dwie-rodziny   | degeneratka, która wpierw wypaplała o krwawym rytuale człowiekowi a potem wróciła by pomóc. Panicznie się boi Kamila Lemurczaka i go nienawidzi do poziomu, że wybiera śmierć. Ma ogromną wiedzę o rytuałach, magii krwi i rzeczach bardzo ezoterycznych i niebezpiecznych. | 0110-09-30 - 0110-10-05 |
| 201006-dezinhibitor-dla-sabiny      | miała 3 dni do powrotu do Aurum. Ale wpakowana w intrygę przez Rekiny złamała parol by uratować Myrczka przed nim samym. Wparowała, opieprzyła Myrczka i z nim "zerwała". Wierzy, że za wszystkim stoi Gabriel i to była intryga co miała ją zatrzymać w Pustogorze. | 0110-10-07 - 0110-10-09 |
| 201011-narodziny-paladynki-saitaera | przekonała Pięknotkę, by ta pozwoliła jej odejść. Zarzuciła wszelkie vendetty i zostawiła Pustogor za sobą. Czas wrócić do domu, do Aurum. | 0110-10-12 - 0110-10-13 |
| 210406-potencjalnie-eksterytorialny-seksbot | wielka nieobecna sesji, która jednak zaplanowała rytuał zrobienie seksbota waśni typu 'Eris' wykorzystując eksterytorialność dzielnicy Rekinów w Podwiercie i swoją mroczną magię. Jako prezent ;-). Cel: spowodowanie wojen między rodami w Aurum przez to, jak młodzi magowie się zachowują w Podwiercie. Rok temu - miała starcie z Marysią Sowińską. | 0111-04-27 - 0111-04-28 |
| 211114-admiral-gruby-wieprz         | współpracuje z Adalbertem Brześniakiem; daje teren i energię by móc chronić TAI. M.in. tam znajduje się Neita? Adalbert wysłał jej trochę wojska / najemników. Jej motywy są niezrozumiałe i nieznane - ruch polityczny? Ale frakcja pro-wolne TAI to akceptuje. | 0112-02-15 - 0112-02-20 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 200417-nawolywanie-trzesawiska      | kiedyś skrzywdziła Lilię Ursus, siostrę Gabriela. Ma nienawiść Gabriela i Lilii do siebie. | 0110-08-14
| 200417-nawolywanie-trzesawiska      | płynnie kontroluje czarną magię i magię krwi. Potrafi nawet ukryć ich emanacje, wchłaniając to w siebie. | 0110-08-14
| 200417-nawolywanie-trzesawiska      | jej sekret (czarna magia) został wykryty przez Gabriela Ursusa i Pięknotkę Diakon. | 0110-08-14
| 200417-nawolywanie-trzesawiska      | jest kompatybilna z Trzęsawiskiem Zjawosztup; Trzęsawisko pragnie jej obecności i ją przyzywa. | 0110-08-14
| 200418-wojna-trzesawiska            | stała się dużo bardziej rozpoznawalna w Szczelińcu przez anomalie Trzęsawiska. BARDZO nielubiana przez to. | 0110-08-21
| 191113-jeden-problem-dwie-rodziny   | przeżyła. Jej zrozumienie Krwawych Rytuałów i Esuriit okazało się naprawdę duże i dość subtelne, co jest niepokojące. | 0110-10-05
| 201006-dezinhibitor-dla-sabiny      | nienawiść wobec Gabriela Ursusa. On chce ją zniszczyć, uderzył też w Myrczka jako przynętę (jej zdaniem). Typowy arystokrata w skórze owcy; groźny i inteligentny. | 0110-10-09
| 201011-narodziny-paladynki-saitaera | wraca do Aurum. Jest wolna od Pustogoru. | 0110-10-13
| 201011-narodziny-paladynki-saitaera | zrzeka się vendetty i jakichkolwiek ofensywnych ruchów wobec Gabriela, Lilii itp. Plus, musi informować Pięknotkę o istotnych rzeczach z Aurum dotyczących Pustogoru. | 0110-10-13
| 211114-admiral-gruby-wieprz         | zbudowała niepewność w Aurum; czy współpracuje z Orbiterem? Jest agentką? Można z nią walczyć / ją zdominować czy to niebezpieczne? | 0112-02-20

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 7 | ((191112-korupcja-z-arystokratki; 200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera; 201011-narodziny-paladynki-saitaera)) |
| Gabriel Ursus        | 6 | ((200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera; 201006-dezinhibitor-dla-sabiny; 201011-narodziny-paladynki-saitaera)) |
| Ignacy Myrczek       | 6 | ((191112-korupcja-z-arystokratki; 200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200510-tajna-baza-orbitera; 201006-dezinhibitor-dla-sabiny)) |
| Mariusz Trzewń       | 3 | ((191112-korupcja-z-arystokratki; 200414-arystokraci-na-trzesawisku; 200510-tajna-baza-orbitera)) |
| Artur Kołczond       | 2 | ((200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera)) |
| Ataienne             | 2 | ((191112-korupcja-z-arystokratki; 200219-perfekcyjna-corka)) |
| Feliks Keksik        | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 211120-glizda-ktora-leczy)) |
| Justynian Diakon     | 2 | ((201006-dezinhibitor-dla-sabiny; 211120-glizda-ktora-leczy)) |
| Laura Tesinik        | 2 | ((200510-tajna-baza-orbitera; 201006-dezinhibitor-dla-sabiny)) |
| Lorena Gwozdnik      | 2 | ((201006-dezinhibitor-dla-sabiny; 210406-potencjalnie-eksterytorialny-seksbot)) |
| Minerwa Metalia      | 2 | ((191112-korupcja-z-arystokratki; 201011-narodziny-paladynki-saitaera)) |
| Napoleon Bankierz    | 2 | ((200417-nawolywanie-trzesawiska; 201006-dezinhibitor-dla-sabiny)) |
| Natalia Tessalon     | 2 | ((200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera)) |
| Strażniczka Alair    | 2 | ((200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska)) |
| Adalbert Brześniak   | 1 | ((211114-admiral-gruby-wieprz)) |
| Agaton Ociegor       | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Aleksander Muniakiewicz | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Aleksandra Szklarska | 1 | ((200219-perfekcyjna-corka)) |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Amanda Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Amelia Mirzant       | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Amelia Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Aranea Diakon        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Artur Michasiewicz   | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Barnaba Burgacz      | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Ernest Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Erwin Galilien       | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Franciszek Leszczowik | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Franek Bulterier     | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Henryk Wkrąż         | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Izabela Zarantel     | 1 | ((211114-admiral-gruby-wieprz)) |
| Julia Kardolin       | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Kacper Bankierz      | 1 | ((211120-glizda-ktora-leczy)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Kamil Lemurczak      | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Karla Mrozik         | 1 | ((200418-wojna-trzesawiska)) |
| Karol Kszatniak      | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Karol Pustak         | 1 | ((211120-glizda-ktora-leczy)) |
| Karolina Erenit      | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Klara Baszcz         | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Klaudia Stryk        | 1 | ((211114-admiral-gruby-wieprz)) |
| Lena Kardamacz       | 1 | ((200219-perfekcyjna-corka)) |
| Leszek Baszcz        | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Leszek Szklarski     | 1 | ((200219-perfekcyjna-corka)) |
| Marysia Sowińska     | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Mateusz Kardamacz    | 1 | ((200219-perfekcyjna-corka)) |
| Olaf Zuchwały        | 1 | ((200417-nawolywanie-trzesawiska)) |
| Oliwia Lemurczak     | 1 | ((211120-glizda-ktora-leczy)) |
| Paweł Kukułnik       | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Paweł Oszmorn        | 1 | ((200219-perfekcyjna-corka)) |
| Persefona d'Jastrząbiec | 1 | ((191112-korupcja-z-arystokratki)) |
| Rafael Galwarn       | 1 | ((211114-admiral-gruby-wieprz)) |
| Rafał Kumczek        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Roland Sowiński      | 1 | ((211120-glizda-ktora-leczy)) |
| Roman Panracz        | 1 | ((211114-admiral-gruby-wieprz)) |
| Rupert Mysiokornik   | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Rzieza d'K1          | 1 | ((211114-admiral-gruby-wieprz)) |
| Saitaer              | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Sensacjusz Diakon    | 1 | ((211120-glizda-ktora-leczy)) |
| Stella Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Tadeusz Sklerzec     | 1 | ((191112-korupcja-z-arystokratki)) |
| Tadeusz Tessalon     | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Talia Aegis          | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Teresa Marszalnik    | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Teresa Mieralit      | 1 | ((200510-tajna-baza-orbitera)) |
| Triana Porzecznik    | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Tymon Grubosz        | 1 | ((200510-tajna-baza-orbitera)) |
| Wiktor Satarail      | 1 | ((200418-wojna-trzesawiska)) |