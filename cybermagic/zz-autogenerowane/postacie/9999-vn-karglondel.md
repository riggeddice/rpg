---
categories: profile
factions: 
owner: public
title: VN Karglondel
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220817-osy-w-ces-purdont            | łazik (crawler) neikatiański; komandosi próbowali zdobyć COŚ pod CES Purdont i doprowadzili do Plagi Trianai. Zostali odparci ze strasznymi stratami. | 0093-01-23 - 0093-01-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((220817-osy-w-ces-purdont)) |
| Celina Lertys        | 1 | ((220817-osy-w-ces-purdont)) |
| Eustachy Korkoran    | 1 | ((220817-osy-w-ces-purdont)) |
| Jan Lertys           | 1 | ((220817-osy-w-ces-purdont)) |
| Joachim Puriur       | 1 | ((220817-osy-w-ces-purdont)) |
| Kamil Wraczok        | 1 | ((220817-osy-w-ces-purdont)) |
| Kordian Olgator      | 1 | ((220817-osy-w-ces-purdont)) |