---
categories: profile
factions: 
owner: public
title: BIA Prometeus
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230329-zdrada-rozrywajaca-arkologie | wspiera Feliksa Kidirona by pomóc Ewelinie sfabrykować fikcyjne rekordy. Pilnuje elektronicznych pamiętników. Ogólnie - overlord arkologii Nativis. | 0093-03-14 - 0093-03-16 |
| 230621-infiltrator-ucieka-a-arkologia-plonie | walczy z Malictrix wypalającą mu subsystemy, zostaje ranny w ataku na Centrum Prometeusa. Ale robi co może. | 0093-03-25 - 0093-03-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Eustachy Korkoran    | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Kalia Awiter         | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Małgorzata Maratelus | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| OO Infernia          | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Rafał Kidiron        | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Ralf Tapszecz        | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Bartłomiej Korkoran  | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Celina Lertys        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Dalmjer Servart      | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Franciszek Pietraszczyk | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Laurencjusz Kidiron  | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Wojciech Grzebawron  | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |