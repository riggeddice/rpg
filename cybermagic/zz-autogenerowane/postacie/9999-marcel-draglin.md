---
categories: profile
factions: 
owner: public
title: Marcel Draglin
---

# {{ page.title }}


# Generated: 



## Fiszki


* oficer Kidirona dowodzący Czarnymi Hełmami | @ 230215-terrorystka-w-ambasadorce
* (ENCAO: --+00 |Bezbarwny, bez osobowości;;Bezkompromisowy;;Skupiony na wyglądzie| VALS: Family, Achievement >> Hedonism, Humility| DRIVE: Lokalny społecznik) | @ 230215-terrorystka-w-ambasadorce
* "Nativis będzie bezpieczna. A jedynym bezpieczeństwem jest Kidiron." | @ 230215-terrorystka-w-ambasadorce
* oficer Kidirona dowodzący Czarnymi Hełmami, dumny "pies Kidirona" | @ 230628-wojna-o-arkologie-nativis-konsolidacja-sil
* (ENCAO: --+00 |Bezbarwny, bez osobowości;;Bezkompromisowy i bezwzględny;;Skupiony na wyglądzie| VALS: Family (Kidiron), Achievement >> Hedonism, Humility| DRIVE: Lokalny społecznik) | @ 230628-wojna-o-arkologie-nativis-konsolidacja-sil
* Najwierniejszy z wiernych, wspiera Rafała Kidirona do śmierci a nawet potem. Jego jedyna lojalność jest wobec Rafała Kidirona. TAK, to jego natura a nie jakaś forma kontroli. | @ 230628-wojna-o-arkologie-nativis-konsolidacja-sil

### Wątki


kidiron-zbawca-nativis
neikatianska-gloria-saviripatel

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230628-wojna-o-arkologie-nativis-konsolidacja-sil | najwierniejszy człowiek Rafała Kidirona. Szef Czarnych Hełmów. Jak tylko dowiedział się że Eustachy osłonił Kidirona i chce mu oddać kontrolę, Marcel w pełni poszedł za Eustachym. | 0093-03-27 - 0093-03-28 |
| 230719-wojna-o-arkologie-nativis-nowa-regentka | z Wujkiem wbił się i zdobył Radiowęzeł. Chce zniszczenia wszystkich zdrajców. | 0093-03-28 - 0093-03-29 |
| 230726-korkoran-placi-cene-za-nativis | brutalny ale skuteczny dowódca, słuchający poleceń Eustachego. Chce maksymalnej kary dla wszystkich zdrajców i chce by Infernia wyglądała na Źródło Światła I Dobroci. Niesamowicie kompetentnie przesuwa oddziały i próbuje osłonić Eustachego od okrucieństwa świata XD. | 0093-03-29 - 0093-03-30 |
| 230816-orbiter-nihilus-i-ruch-oporu-w-nativis | blokuje ruchy Eustachego by zrzucić politykę publicznie na Ardillę i Kalię; jego zdaniem on oraz Eustachy są od 'krwawej roboty' i dziewczyny mają być symbolem czegoś lepszego. Lepszy politycznie niż się wydawało, bo nauczył się czegoś od Kidirona. Największy sojusznik Ardilli i Kalii w arkologii, mimo, że obie go nie lubią i nim gardzą. Infernia ma być niewinna. Z woli Kidirona. | 0093-03-30 - 0093-03-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 4 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis; 230816-orbiter-nihilus-i-ruch-oporu-w-nativis)) |
| Eustachy Korkoran    | 4 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis; 230816-orbiter-nihilus-i-ruch-oporu-w-nativis)) |
| Bartłomiej Korkoran  | 3 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Izabella Saviripatel | 3 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Tobiasz Lobrak       | 3 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Kalia Awiter         | 2 | ((230719-wojna-o-arkologie-nativis-nowa-regentka; 230816-orbiter-nihilus-i-ruch-oporu-w-nativis)) |
| OO Infernia          | 2 | ((230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Ralf Tapszecz        | 2 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Tymon Korkoran       | 2 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230726-korkoran-placi-cene-za-nativis)) |
| Celina Lertys        | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Karol Lertys         | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Laurencjusz Kidiron  | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Marzena Marius       | 1 | ((230816-orbiter-nihilus-i-ruch-oporu-w-nativis)) |