---
categories: profile
factions: 
owner: public
title: Władysław Owczarek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220101-karolina-w-ciezkim-mlocie    | właściciel Zakładu Recyklingu Owczarek. Samemu próbuje naprawiać co się da. Omija regulacje, zbiera złom. "Janusz biznesu", ale dba o swoich ludzi. Przyjaciel Henryka Murkota. | 0111-04-25 - 0111-05-05 |
| 220730-supersupertajny-plan-loreny  | podpisał kontrakt z Ernestem na recykling, ale dostaje złe części i boi się eskalować, więc poprosił o mediację Karolinę. Pomógł Karo zastawić pułapkę na prawdziwego sabotażystę - Marsena. | 0111-09-30 - 0111-10-04 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Daniel Terienak      | 2 | ((220101-karolina-w-ciezkim-mlocie; 220730-supersupertajny-plan-loreny)) |
| Karolina Terienak    | 2 | ((220101-karolina-w-ciezkim-mlocie; 220730-supersupertajny-plan-loreny)) |
| Arkadia Verlen       | 1 | ((220730-supersupertajny-plan-loreny)) |
| Henryk Murkot        | 1 | ((220101-karolina-w-ciezkim-mlocie)) |
| Liliana Bankierz     | 1 | ((220730-supersupertajny-plan-loreny)) |
| Marsen Gwozdnik      | 1 | ((220730-supersupertajny-plan-loreny)) |
| Mimoza Elegancja Diakon | 1 | ((220101-karolina-w-ciezkim-mlocie)) |
| Żorż d'Namertel      | 1 | ((220730-supersupertajny-plan-loreny)) |