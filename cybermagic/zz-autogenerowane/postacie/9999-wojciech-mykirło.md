---
categories: profile
factions: 
owner: public
title: Wojciech Mykirło
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231109-komodor-bladawir-i-korona-woltaren | załogant Korony Woltaren zajmujący się sadem i biologicznymi komponentami. Chce współpracować z Orbiterem, uważa że to da więcej sukcesów. Gdy Lidia była ranna, zasłonił ją przed nanofarem i terrorformem. | 0110-10-27 - 0110-11-03 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 231109-komodor-bladawir-i-korona-woltaren | skończył ciężko ranny dotknięty przez nanofar ixioński gdy Klaudia urżnęła mu rękę by go ratować. | 0110-11-03

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aneta Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Antoni Bladawir      | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Arianna Verlen       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Elena Verlen         | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Feliks Walrond       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Klaudia Stryk        | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Krzysztof Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Leszek Kurzmin       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Lidia Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Marta Keksik         | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| OO Paprykowiec       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Patryk Samszar       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| SC Korona Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Tomasz Rewernik      | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |