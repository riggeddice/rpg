---
categories: profile
factions: 
owner: public
title: Napoleon Bankierz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180929-dwa-tygodnie-szkoly          | kandydat na terminusa w szkole magów, dostał od Felicji (protomag) i jej nie trawi, ale przemowa Ateny obudziła w nim uczucia wielkości i zachwytu do Ateny. | 0109-09-17 - 0109-09-19 |
| 190113-chronmy-karoline-przed-uczniami | chciał chronić Karolinę Erenit przed innymi magami, zdobył eliksir od Adeli ale źle wyspecyfikował. Zaraził się, zdemolował pokój i - na szczęście - dyrektor wszystko wyciszył. | 0110-01-05 - 0110-01-06 |
| 190127-ixionski-transorganik        | obudzony przez Pięknotkę w środku nocy, natychmiast jej odpowiedział na pytanie gdzie mieszka Karolina Erenit. | 0110-01-29 - 0110-01-30 |
| 200326-test-z-etyki                 | AMZ; podrywa dziewczyny na arenie ćwicząc, ale jak przychodzi temat Liliany to przekierował się na nią by chronić Myrczka. Chronił Zespół przed insektami; solidnie pokąsany. | 0110-07-29 - 0110-07-31 |
| 200417-nawolywanie-trzesawiska      | z rozkazu Teresy Mieralit pilnował Myrczka by nic mu się nie stało gdy ten szukał kobiety której coś grozi; bezpiecznie doprowadził go do Pięknotki. | 0110-08-12 - 0110-08-14 |
| 201006-dezinhibitor-dla-sabiny      | traktuje Laurę i Gabriela z szacunkiem; są tam gdzie on chce być. Po ćwiczeniach na Arenie wskazał kto pomagał Myrczkowi - arystokrata i Karolina Erenit. | 0110-10-07 - 0110-10-09 |
| 201013-pojedynek-akademia-rekiny    | podczas ustawki ostrzegł nauczycieli o efemerydzie; miłośnik historii i wojskowości, tu: ŚWIETNY pilot ścigacza który trafił na podium walcząc z Rekinami. | 0110-10-14 - 0110-10-22 |
| 210323-grzybopreza                  | próbował uratować porwanego przez Marysię Myrczka; niestety, Marysia dużo lepiej nawiguje ścigaczem niż Napoleon. Po dostaniu wiązką śluzu skończył w jeziorze Zaczęstwa. Wezwał Ulę (terminuskę) na swoje miejsce. | 0111-04-22 - 0111-04-23 |
| 211228-akt-o-ktorym-marysia-nie-wie | zmierzył się z Karo na arenie i przegrał ostro, ale Karo podniosła go reputacyjnie. Powiedział jej o akcie Marysi i o tym, że ów należy do Liliany. Należy do miłośników piękna. | 0111-09-09 - 0111-09-12 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180929-dwa-tygodnie-szkoly          | stał się wrogi Felicji (bo go uderzyła) i jest zakochany w słowach Ateny - chce pod nią służyć na Epirjonie. | 0109-09-19
| 201013-pojedynek-akademia-rekiny    | nie ma sprzężenia z sentisiecią Bankierz, ale był uczony na arystokratę Aurum. | 0110-10-22

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ignacy Myrczek       | 6 | ((180929-dwa-tygodnie-szkoly; 200326-test-z-etyki; 200417-nawolywanie-trzesawiska; 201006-dezinhibitor-dla-sabiny; 201013-pojedynek-akademia-rekiny; 210323-grzybopreza)) |
| Liliana Bankierz     | 4 | ((190113-chronmy-karoline-przed-uczniami; 200326-test-z-etyki; 201013-pojedynek-akademia-rekiny; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Pięknotka Diakon     | 4 | ((180929-dwa-tygodnie-szkoly; 190113-chronmy-karoline-przed-uczniami; 190127-ixionski-transorganik; 200417-nawolywanie-trzesawiska)) |
| Karolina Erenit      | 3 | ((190113-chronmy-karoline-przed-uczniami; 190127-ixionski-transorganik; 201006-dezinhibitor-dla-sabiny)) |
| Teresa Mieralit      | 3 | ((190113-chronmy-karoline-przed-uczniami; 200326-test-z-etyki; 201013-pojedynek-akademia-rekiny)) |
| Adela Kirys          | 2 | ((180929-dwa-tygodnie-szkoly; 190113-chronmy-karoline-przed-uczniami)) |
| Arnulf Poważny       | 2 | ((180929-dwa-tygodnie-szkoly; 190113-chronmy-karoline-przed-uczniami)) |
| Erwin Galilien       | 2 | ((180929-dwa-tygodnie-szkoly; 190127-ixionski-transorganik)) |
| Gabriel Ursus        | 2 | ((200417-nawolywanie-trzesawiska; 201006-dezinhibitor-dla-sabiny)) |
| Julia Kardolin       | 2 | ((201013-pojedynek-akademia-rekiny; 210323-grzybopreza)) |
| Justynian Diakon     | 2 | ((201006-dezinhibitor-dla-sabiny; 201013-pojedynek-akademia-rekiny)) |
| Kacper Bankierz      | 2 | ((201013-pojedynek-akademia-rekiny; 210323-grzybopreza)) |
| Lorena Gwozdnik      | 2 | ((201006-dezinhibitor-dla-sabiny; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Marysia Sowińska     | 2 | ((210323-grzybopreza; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Sabina Kazitan       | 2 | ((200417-nawolywanie-trzesawiska; 201006-dezinhibitor-dla-sabiny)) |
| Aleksander Bemucik   | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Aniela Kark          | 1 | ((200326-test-z-etyki)) |
| Aranea Diakon        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Atena Sowińska       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Berenika Wrążowiec   | 1 | ((200326-test-z-etyki)) |
| Daniel Terienak      | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Ernest Namertel      | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Felicja Melitniek    | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Karolina Terienak    | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Keira Amarco d'Namertel | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Kirył Najłalmin      | 1 | ((190127-ixionski-transorganik)) |
| Laura Tesinik        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Miedwied Zajcew      | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Minerwa Metalia      | 1 | ((190127-ixionski-transorganik)) |
| Olaf Zuchwały        | 1 | ((200417-nawolywanie-trzesawiska)) |
| Rafał Kumczek        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Rafał Torszecki      | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Remor 340D           | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Robert Pakiszon      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Roland Grzymość      | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Saitaer              | 1 | ((190127-ixionski-transorganik)) |
| Stella Armadion      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Strażniczka Alair    | 1 | ((200417-nawolywanie-trzesawiska)) |
| Triana Porzecznik    | 1 | ((210323-grzybopreza)) |
| Tymon Grubosz        | 1 | ((190127-ixionski-transorganik)) |
| Urszula Miłkowicz    | 1 | ((210323-grzybopreza)) |
| Wiktor Satarail      | 1 | ((190127-ixionski-transorganik)) |
| Wojtek Kurczynos     | 1 | ((190127-ixionski-transorganik)) |