---
categories: profile
factions: 
owner: public
title: Klara Baszcz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191108-ukojenie-aleksandrii         | nieżywa córka Leszka i Danieli. Kurator. Nie ma wielkiej siły ognia, ale ma wiedzę i umiejętności negocjacji Aleksandrii. | 0110-07-18 - 0110-07-20 |
| 191113-jeden-problem-dwie-rodziny   | nieumarła; przejęta przez moc Esuriit prawie pożarła ojca - zatrzymały ją Amelia i Teresa i finalnie wypaliło się z niej wszystko Aleksandrią. | 0110-09-30 - 0110-10-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Mirzant       | 2 | ((191108-ukojenie-aleksandrii; 191113-jeden-problem-dwie-rodziny)) |
| Leszek Baszcz        | 2 | ((191108-ukojenie-aleksandrii; 191113-jeden-problem-dwie-rodziny)) |
| Paweł Kukułnik       | 2 | ((191108-ukojenie-aleksandrii; 191113-jeden-problem-dwie-rodziny)) |
| Daniela Baszcz       | 1 | ((191108-ukojenie-aleksandrii)) |
| Kamil Lemurczak      | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Karol Kszatniak      | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Kinga Stryk          | 1 | ((191108-ukojenie-aleksandrii)) |
| Sabina Kazitan       | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Teresa Marszalnik    | 1 | ((191113-jeden-problem-dwie-rodziny)) |