---
categories: profile
factions: 
owner: public
title: Rafał Grambucz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210428-infekcja-serenit             | łącznościowiec z Falołamacza. Wpływa na niego Serenit, więc nie jest składny i logiczny. Odpowiada na pytania Inferni i dzięki temu Zespół wie, że Falołamacz jest anomaliczny. | 0111-11-22 - 0111-11-23 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Serenit           | 1 | ((210428-infekcja-serenit)) |
| Arianna Verlen       | 1 | ((210428-infekcja-serenit)) |
| Elena Verlen         | 1 | ((210428-infekcja-serenit)) |
| Eustachy Korkoran    | 1 | ((210428-infekcja-serenit)) |
| Klaudia Stryk        | 1 | ((210428-infekcja-serenit)) |
| Martyn Hiwasser      | 1 | ((210428-infekcja-serenit)) |
| OE Falołamacz        | 1 | ((210428-infekcja-serenit)) |
| OE Piękna Elena      | 1 | ((210428-infekcja-serenit)) |
| OO Infernia          | 1 | ((210428-infekcja-serenit)) |
| Persefona d'Infernia | 1 | ((210428-infekcja-serenit)) |
| Tadeusz Ursus        | 1 | ((210428-infekcja-serenit)) |