---
categories: profile
factions: 
owner: public
title: Albert Samszar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230523-romeo-dyskretny-instalator-supreme-missionforce | ojciec Mai, dba o poprawność etykiety i niespecjalnie wie co robi Maja (UW: structure). Gdy Maja dała ślady obecności o 2 rano w okolicach Centrum Danych Symulacji Zarządzania, ruszył jej na pomoc komunikując się z wykrytą tam Eleną Samszar. Jego uwaga odwrócona, dzięki działaniom Eleny i Karolinusa Maja nie wpadła w kłopoty. Dba o Maję. | 0095-08-09 - 0095-08-11 |
| 230704-maja-chciala-byc-dorosla     | na wakacjach, odstresowuje się; o dziwo, niespecjalnie interesuje się hedonizmem. Trzymał Maję pod kloszem, przez co wpadła w pierwszego manipulatora. Zawiódł córkę. Ale by ją ratować był skłonny nawet samemu zaatakować dwóch Lemurczaków. | 0095-08-29 - 0095-09-01 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230704-maja-chciala-byc-dorosla     | przekierował uwagę na Maję, schodząc z pozycji, reputacji itp. Sojusz z Karolinusem, którego uważa za pozytywny wpływ na Maję. | 0095-09-01
| 230704-maja-chciala-byc-dorosla     | jego zdaniem, hiperpsychotronicy są powiązani z cierpieniem Mai. A on nie wybaczy cierpienia córki. ŚMIERTELNY WRÓG Wirgota Samszara, Vanessy Lemurczak, Jonatana Lemurczaka | 0095-09-01

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolinus Samszar    | 2 | ((230523-romeo-dyskretny-instalator-supreme-missionforce; 230704-maja-chciala-byc-dorosla)) |
| Maja Samszar         | 2 | ((230523-romeo-dyskretny-instalator-supreme-missionforce; 230704-maja-chciala-byc-dorosla)) |
| AJA Szybka Strzała   | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Amanda Kajrat        | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Elena Samszar        | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Jonatan Lemurczak    | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Nadia Obiris         | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Nataniel Samszar     | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Romeo Verlen         | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Vanessa Lemurczak    | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Wirgot Samszar       | 1 | ((230704-maja-chciala-byc-dorosla)) |