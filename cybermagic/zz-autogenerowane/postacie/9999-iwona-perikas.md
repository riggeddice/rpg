---
categories: profile
factions: 
owner: public
title: Iwona Perikas
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220819-tank-as-a-love-letter        | Rekin; Shark; she found Flara Astorii and tried to reanimate it and decontaminate it for Daniel, to impress him. She wooed Henryk Wkrąż and inserted some Esuriit-parts to different transports. She was the driver, but she was under some form of mindwarp by a third party. And she worked with mafia. | 0111-10-07 - 0111-10-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ariel Kubunczak      | 1 | ((220819-tank-as-a-love-letter)) |
| Arkadiusz Terienak   | 1 | ((220819-tank-as-a-love-letter)) |
| AU Flara Astorii     | 1 | ((220819-tank-as-a-love-letter)) |
| Daniel Terienak      | 1 | ((220819-tank-as-a-love-letter)) |
| Henryk Wkrąż         | 1 | ((220819-tank-as-a-love-letter)) |
| Izabela Selentik     | 1 | ((220819-tank-as-a-love-letter)) |
| Michał Kabarniec     | 1 | ((220819-tank-as-a-love-letter)) |
| Mimoza Diakon        | 1 | ((220819-tank-as-a-love-letter)) |
| Talia Mikrit         | 1 | ((220819-tank-as-a-love-letter)) |