---
categories: profile
factions: 
owner: public
title: Ralf Tapszecz
---

# {{ page.title }}


# Generated: 



## Fiszki


* mag (domena: DOMINUJĄCA ROZPACZ) i nowy dodatek do Inferni, kultysta Interis. Noktianin, savaranin. | @ 230201-wylaczone-generatory-memoriam-inferni
* (ENCAO:  -00-+ |Bezbarwny, przezroczysty;;Złośliwy;;Buja w obłokach| VALS: Conformity, Humility >> Benevolence| DRIVE: Piękno zniszczenia) | @ 230201-wylaczone-generatory-memoriam-inferni
* "Nasze porażki nie mają znaczenia. Tak czy inaczej zwyciężymy." | @ 230201-wylaczone-generatory-memoriam-inferni
* mag (domena: ROZPACZ + KINEZA) i nowy dodatek do Inferni, kultysta Interis. Noktianin, savaranin. | @ 230315-bardzo-nieudane-porwanie-inferni
* (ENCAO:  -00-+ |Bezbarwny, przezroczysty;;Buja w obłokach| VALS: Conformity, Humility >> Benevolence| DRIVE: Piękno zniszczenia) | @ 230315-bardzo-nieudane-porwanie-inferni
* mag (domena: rozpacz + kineza). Noktianin, savaranin. Pomaga Ardilli, wyraźnie się nią zainteresował (personalnie) i ją lubi. | @ 230329-zdrada-rozrywajaca-arkologie

### Wątki


infernia-jej-imieniem
kidiron-zbawca-nativis
brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230720-savaranie-przed-obliczem-nihilusa | na imię ma ARNOLD. Jedyny ocalały ze swojego statku gdzie była Anomalia Nihilusa, uratowany przez 'Kantalę Ravis'; stworzył magią manifestację swego ojca mocą Nihilusa (ojciec RALF jest jego bohaterem). 'Ojciec' - manifestacja Nihilusa - zatruwała Kantalę Ravis, ale ten jego aspekt w któego wierzył 'Ralf' próbowała go zabić (i przez to chronić jego i Kantalę Ravis). Polubił się z Kiranem z Kantali; Igor wpakował Ralfa i Arnolda do wahadłowca, który potem został wysadzony. KIA, ale Nihilus go przywrócił na Neikatis, z mniejszą dozą pamięci. Był ogniście lojalny rodzinie. | 0092-07-01 - 0092-07-02 |
| 230201-wylaczone-generatory-memoriam-inferni | nowy dodatek do Inferni, mag (domena: DOMINUJĄCA ROZPACZ) i kultysta Interis. Noktianin, savaranin. (ENCAO:  -00-+ |Bezbarwny, przezroczysty;;Buja w obłokach| VALS: Conformity, Humility >> Benevolence| DRIVE: Piękno zniszczenia). Świetny w bezszelestności i infiltracji, nigdy się nie skarży, polubił się z Ardillą. Wujek dodał go do Inferni by mu pomóc. | 0093-02-10 - 0093-02-12 |
| 230208-pierwsza-randka-eustachego   | powoli zbliża się do Ardilli. Wyraźnie ma problemy z konfliktem kultury savarańskiej (musisz być użyteczny) a tym co _chce_. Przyznał się Ardilli że widział oczy Nihilusa i to go zmieniło. Wyraźnie chce pomóc innym, ale nie rozumie dlaczego warto pomóc komuś kto nie ma już potencjału - ale CHCE pomóc. Bardzo skonfliktowany. WIE, że mag (Eustachy) nie może być sam. Ma problem z pozycjonowaniem Bartłomieja Korkorana jako bohatera lub głupca marnującego zasoby. | 0093-02-14 - 0093-02-21 |
| 230215-terrorystka-w-ambasadorce    | w jakiś sposób dał radę nawiązać połączenie z Ardillą gdy ona była w Ambasadorce pełznąc korytarzami. Nawiązał link Eustachy - Ardilla i zadbał o bezpieczeństwo wszystkich. | 0093-02-22 - 0093-02-23 |
| 230315-bardzo-nieudane-porwanie-inferni | uczestniczył w akcji ratunkowej (ale przede wszystkim pilnował Ardillę), gdy mostek był zaatakowany to szybkim atakiem zranił dwóch napastników po oaczach. Potem współpraca z Ardillą w sabotowaniu generatorów Memoriam i dywersji. | 0093-03-06 - 0093-03-09 |
| 230329-zdrada-rozrywajaca-arkologie | wiedząc, że pamiętnik Eweliny został upubliczniony i wiedząc, że Ardilli na Ewelinie zależy był w pobliżu Eweliny gdy ta próbowała się zabić. Nie miał siły, by przenieść Ewelinę z Ardillą do bezpiecznego miejsca (savaranin jest słaby fizycznie). Uważa, że Ewelina zachowała się "nieefektywnie". | 0093-03-14 - 0093-03-16 |
| 230614-atak-na-kidirona             | Ardilla podejrzewa go o bycie magiem Nihilusa. On jeszcze nie wie. Gdy doszło do ataku na arkologię, Ralf od razu pojawił się przy Ardilli i chroni ją i tylko ją. Skutecznie zgasił pożar drzew w arkologii, zaczynając od drzew które Ardilla lubi najbardziej. | 0093-03-22 - 0093-03-24 |
| 230621-infiltrator-ucieka-a-arkologia-plonie | wykazał się savarańską doktryną, strzelając do buntowników w Szczurowisku i poświęcając nawet swojego Lancera by chronić Prometeusa. Jest cieniem Ardilli, nie da jej zrobić krzywdy. | 0093-03-25 - 0093-03-26 |
| 230628-wojna-o-arkologie-nativis-konsolidacja-sil | ranny i niewidoczny; jest cieniem Ardilli. | 0093-03-27 - 0093-03-28 |
| 230719-wojna-o-arkologie-nativis-nowa-regentka | cień Ardilli, ochronił ją przed atakiem mentalnym przepisującym ją wzywając energię Nihilusa niszcząc umysł Izabelli. Jego ostatnią wartościową kotwicą jest Ardilla. | 0093-03-28 - 0093-03-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 9 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Eustachy Korkoran    | 9 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Bartłomiej Korkoran  | 7 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Kalia Awiter         | 7 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| OO Infernia          | 7 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Rafał Kidiron        | 7 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Celina Lertys        | 3 | ((230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Tobiasz Lobrak       | 3 | ((230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Tymon Korkoran       | 3 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| BIA Prometeus        | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Dalmjer Servart      | 2 | ((230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Franciszek Pietraszczyk | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| Izabella Saviripatel | 2 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Laurencjusz Kidiron  | 2 | ((230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Małgorzata Maratelus | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Marcel Draglin       | 2 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| SAN Szare Ostrze     | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230614-atak-na-kidirona)) |
| Wojciech Grzebawron  | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Agnieszka Serkis     | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Arnold Tapszecz      | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Hubert Grzebawron    | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Igor Seklamant       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Jan Lertys           | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Jola Seklamant       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| JRN Kantala Ravis    | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Karol Lertys         | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Katrina Kirten       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Kiran Ravis          | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Lena Morazik         | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Lycoris Kidiron      | 1 | ((230614-atak-na-kidirona)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Marcelin Viirdus     | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Marcin Pietraszczyk  | 1 | ((230208-pierwsza-randka-eustachego)) |
| Mariusz Dobrowąs     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Nadia Sekernik       | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Rafał Kurrodis       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Ralena Annitas       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Stanisław Uczantor   | 1 | ((230614-atak-na-kidirona)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |