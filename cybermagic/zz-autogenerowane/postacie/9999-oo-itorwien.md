---
categories: profile
factions: 
owner: public
title: OO Itorwien
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230521-rozszczepiona-persefona-na-itorwienie | pługośmieciarka kosmiczna, wpadła w kłopoty - dwóch przemytników w załodze doprowadziło do Skażenia substratem całego statku przez life support. Persefona się rozszczepiła przez blokady założone przez Arkaladisa by ratować załogę. Persi odpaliła jeden _charge_ pancerza aktywnego, prawie raniąc Martyna. | 0109-09-15 - 0109-09-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Emilia Ibris         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Fabian Korneliusz    | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Helmut Szczypacz     | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Iskander Matorin     | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Karol Brinik         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Klaudia Stryk        | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Martyn Hiwasser      | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Mojra Karstall       | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| OO Serbinius         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Tadeusz Arkaladis    | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |