---
categories: profile
factions: 
owner: public
title: Kirea Rialirat
---

# {{ page.title }}


# Generated: 



## Fiszki


* advancer Dominy Lucis (savaranka) | @ 221130-astralna-flara-w-strefie-duchow
* ENCAO:  --+00 |Odporna na stres;;Introspektywna| VALS: Humility, Conformity | DRIVE: Potrzeba samotności i ciszy; problemy z ludźmi i towarzystwem | @ 221130-astralna-flara-w-strefie-duchow

### Wątki


historia-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221130-astralna-flara-w-strefie-duchow | advancer Dominy Lucis (savaranka) i dziewczyna w wieku Eleny; przechwycona przez Huberta z planetoidy TKO-4271. Bardzo cicha savaranka przyzwyczajona do 0.5g. Nieświadomie, powiedziała bardzo dużo Ariannie o Dominie Lucis i ekosystemie Strefy Duchów. Przerażona, mimo, że traktowana dobrze (do poziomu próby samobójstwa). Uważa Orbiter za tych, co sprowadzą koniec jej świata. Albo Orbiter zginie albo baza noktiańska zginie. | 0100-10-05 - 0100-10-08 |
| 221214-astralna-flara-kontra-domina-lucis | KIA. Element propagandowy dla Ellariny by zmusić noktian do poddania się. Z radością zobaczyła Tristana - krewnego. Zginęła z jego ręki, by ją ratować przed służeniem w imię Orbitera. | 0100-10-09 - 0100-10-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Daria Czarnewik      | 2 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Ellarina Samarintael | 2 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Gabriel Lodowiec     | 2 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| NekroTAI Zarralea    | 2 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| OO Astralna Flara    | 2 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| OO Athamarein        | 2 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Arnulf Perikas       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Axel Nargan          | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Elena Verlen         | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Hubert Kerwelenios   | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Kajetan Kircznik     | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Klarysa Jirnik       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Leo Kasztop          | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Leszek Kurzmin       | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Maja Samszar         | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Sarian Xadaar        | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Szymon Wanad         | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Tomasz Dojnicz       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Tristan Rialirat     | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |