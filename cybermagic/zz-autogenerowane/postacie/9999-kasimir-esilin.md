---
categories: profile
factions: 
owner: public
title: Kasimir Esilin
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (E-C+O-): zamknięty w sobie i poważny, świetnie planuje, myśli tylko o misji, dość formulaiczny. "Nie wiem o co im chodzi, ale wiem, że z nimi wygram." | @ 230715-amanda-konsoliduje-zloty-cien
* VALS: (Security, Conformity): przewidywalność i bezpieczeństwo. Nie wychylanie się. Minimalizacja niespodziewanego. "Dyscyplina, porządek, planowanie, ochrona." | @ 230715-amanda-konsoliduje-zloty-cien
* Core Wound - Lie: "byłem żołnierzem elitarnej jednostki, wywalili mnie jak śmiecia bo poznałem prawdę" - "ochronię wszystkich innych i zreintegruję ich w imię Nocnego Nieba" | @ 230715-amanda-konsoliduje-zloty-cien
* styl: twardy wojownik z dobrym sercem, który wykona zadanie ale spróbuje zmniejszyć ilość strat. "Bezpieczeństwo wymaga ciągłej walki." | @ 230715-amanda-konsoliduje-zloty-cien

### Wątki


pronoktianska-mafia-kajrata

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230715-amanda-konsoliduje-zloty-cien | 37, starszy żołnierz Nocnego Nieba; przybył i wspiera Amandę niezależnie od tego co się stanie. Opracował plan złapania żywcem Przemytników atakujących magazyn i go przeprowadził sprawnie i bez słowa. | 0095-09-09 - 0095-09-12 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Bella Samszar        | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Emil Samszar         | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Rufus Bilgemener     | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Tadeusz Samszar      | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Wacław Samszar       | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |