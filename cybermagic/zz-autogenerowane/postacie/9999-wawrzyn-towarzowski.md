---
categories: profile
factions: 
owner: public
title: Wawrzyn Towarzowski
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181104-kotlina-duchow               | terminus w okolicach Czarnopalca. Skażony przez efemerydę, został zagryziony przez gorathaula gdy Pięknotka wprowadziła go w trans zapomnienia. KIA. | 0109-10-22 - 0109-10-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kirył Najłalmin      | 1 | ((181104-kotlina-duchow)) |
| Marlena Maja Leszczyńska | 1 | ((181104-kotlina-duchow)) |
| Olga Myszeczka       | 1 | ((181104-kotlina-duchow)) |
| Pięknotka Diakon     | 1 | ((181104-kotlina-duchow)) |