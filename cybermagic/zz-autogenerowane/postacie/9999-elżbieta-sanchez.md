---
categories: profile
factions: 
owner: public
title: Elżbieta Sanchez
---

# {{ page.title }}


# Generated: 



## Fiszki


* Kierownik Badawczy, Profesor | @ 240117-echo-z-odmetow-przeszlosci
* Ekspertka od farmaceutyki, kosmetyki i chemii | @ 240117-echo-z-odmetow-przeszlosci
* Udaje oddaną korporacji oraz badaniom dla niej, jednak w rzeczywistości zależy jej tylko na osiągnięciu długowieczności dla siebie. Jednak chce też zachować młody wygląd, dba o cerę, robi projekty na boku, którymi się nie dzieli robiąc specyfiki dla siebie. | @ 240117-echo-z-odmetow-przeszlosci
* Elżbieta nie będzie chciała, by zespół opuścił stację. Niezależnie od wszystkiego, ona chce kontynuować. | @ 240117-echo-z-odmetow-przeszlosci

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240117-echo-z-odmetow-przeszlosci   | ostatecznie zrealizowała swoje pragnienie nieśmiertelności, zapewnia sobie niemal wieczne stanowisko zarządcy stacji w luksusie i wygodzie oraz swobodę działania w obrębie stacji. | 0096-01-24 - 0096-01-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anna Ivanova         | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Gerald Barowiecki    | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Jędrzej Sanchez      | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Klasa Dyplomata      | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Klasa Inżynier       | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Klasa Oficer Ochrony | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Robert Tisso         | 1 | ((240117-echo-z-odmetow-przeszlosci)) |