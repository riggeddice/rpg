---
categories: profile
factions: 
owner: public
title: Jola Seklamant
---

# {{ page.title }}


# Generated: 



## Fiszki


* inżynier strukturalny, specjalista od silników i fabrykacji | @ 230720-savaranie-przed-obliczem-nihilusa
* OCEAN: (C+O+E-): Precyzyjny i skrupulatny, nieustannie szuka usprawnień. "Silniki mówią. Ty nie słuchasz." | @ 230720-savaranie-przed-obliczem-nihilusa
* VALS: (Achievement, Self-Direction): jego osiągnięcia są dowodem jego wartości. "Wszystko potrafię zrobić ze statkiem jeśli trzeba" | @ 230720-savaranie-przed-obliczem-nihilusa
* Core Wound - Lie: "Wszyscy się śmieją z mojej obsesji na punkcie silników" - "Jeżeli stworzę najdoskonalszy silnik, w końcu mnie docenią." | @ 230720-savaranie-przed-obliczem-nihilusa
* styl: Zafascynowany swoją pracą, ciągle na bieżąco. "Szum silników to poezja" | @ 230720-savaranie-przed-obliczem-nihilusa

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230720-savaranie-przed-obliczem-nihilusa | Wydobywała apatyczne Pacyfikatory z rur, badała silnik (odkrywając sabotaż i Skażenie), pozyskała zdrowego Pacyfikatora i doszła do tego, że mają potężną anomalię - Generatory Memoriam spłonęły. | 0092-07-01 - 0092-07-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Agnieszka Serkis     | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Arnold Tapszecz      | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Igor Seklamant       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| JRN Kantala Ravis    | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Katrina Kirten       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Kiran Ravis          | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Lena Morazik         | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Marcelin Viirdus     | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Rafał Kurrodis       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Ralena Annitas       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Ralf Tapszecz        | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |