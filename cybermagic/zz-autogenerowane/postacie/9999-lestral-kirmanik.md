---
categories: profile
factions: 
owner: public
title: Lestral Kirmanik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230806-zwiad-w-iliminar-caos        | śmieszek, próbujący żartować z Amandy i jej przyszłego pójścia do łóżka z Leonem, ale ogólnie dobry pilot Quispisa. Ogólnie jest opinia, że między dwójką jego uszu jest prześwit ;-). | 0081-06-22 - 0081-06-24 |
| 230906-operacja-mag-dla-symlotosu   | jawnie flirtuje z Xaverą, która jest w jego typie. Dokucza Leonowi. Pod wpływem Kwiatów prawie naraził Quispis, ale wrócił do bazy zachęcony Orgią z Furiami. | 0081-06-26 - 0081-06-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Caelia Calaris       | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Dragan Halatis       | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Ernest Kajrat        | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Leon Varkas          | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Alaric Rakkeir       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Edmund Garzin        | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Isaura Velaska       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Xavera Sirtas        | 1 | ((230906-operacja-mag-dla-symlotosu)) |