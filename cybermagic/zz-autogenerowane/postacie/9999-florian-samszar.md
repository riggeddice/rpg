---
categories: profile
factions: 
owner: public
title: Florian Samszar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230808-nauczmy-mlodego-tiena-jak-zyc | 27-letni tien wierzący szczerze w to, że Karolinus i Elena mają ogromny potencjał; zrobił operację "nauczmy młodego tiena jak żyć" by oni się przekonali że mają wsparcie rodu. Padło katastrofalnie, nie tylko Armin został ranny i Karolinus i Elena skrzywdzili zwykłych ludzi ale i Florian wyszedł na idiotę i naiwniaka. | 0095-09-14 - 0095-09-23 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230808-nauczmy-mlodego-tiena-jak-zyc | ma potężny WPIERDOL od rodu za niekompetencję, narażenie Armina, narażenie okolicy itp. Wierzył w Karolinusa i Elenę, ale naprawdę przez niego wszyscy (ludzie, Armin) ucierpieli. | 0095-09-23

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Armin Samszar        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Celina Maskiewnik    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Elena Samszar        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Karolinus Samszar    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Kleopatra Trusiek    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Rufus Bilgemener     | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Tadeusz Maskiewnik   | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Wiktor Blakenbauer   | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |