---
categories: profile
factions: 
owner: public
title: Izabella Saviripatel
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230628-wojna-o-arkologie-nativis-konsolidacja-sil | piękna egzotyczna piękność (17?) pod kontrolą Lobraka, oddana przez Lobraka Tymonowi i Laurencjuszowi; nie wygląda na szczególnie szczęśliwą. | 0093-03-27 - 0093-03-28 |
| 230719-wojna-o-arkologie-nativis-nowa-regentka | agentka Syndykatu, 'złapana' przez Wujka i Draglina, spróbowała gambitu mającego przeprogramować Ardillę do zabicia Eustachego. Jej umysł zniszczył Nihilus/Ralf w obronie Ardilli. Praktycznie KIA. | 0093-03-28 - 0093-03-29 |
| 230726-korkoran-placi-cene-za-nativis | żywa demonstracja dla Lobraka co Eustachy ma do dyspozycji; jest mentalnie w piekle | 0093-03-29 - 0093-03-30 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 3 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Bartłomiej Korkoran  | 3 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Eustachy Korkoran    | 3 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Marcel Draglin       | 3 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Tobiasz Lobrak       | 3 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| OO Infernia          | 2 | ((230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Ralf Tapszecz        | 2 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Tymon Korkoran       | 2 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230726-korkoran-placi-cene-za-nativis)) |
| Celina Lertys        | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Kalia Awiter         | 1 | ((230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Karol Lertys         | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Laurencjusz Kidiron  | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |