---
categories: profile
factions: 
owner: public
title: Stefan Ukrand
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191120-mafia-na-stacji-gorniczej    | wisiał pieniądze mafii; poleciał przez Wormhole i znalazł Kuratorów w Sektorze Paradizo. Skończył w Aleksandrii, szczęśliwszy niż kiedykolwiek "za życia". Zostawił rodzinę na Stacji. | 0110-06-14 - 0110-06-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eszara d'Dorszant    | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Gerwazy Kruczkut     | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Hestia d'Dorszant    | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Janet Erwon          | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Jaromir Uczkram      | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Kamelia Termit       | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Oliwier Pszteng      | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Rafał Kirlat         | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Szymon Szelmer       | 1 | ((191120-mafia-na-stacji-gorniczej)) |