---
categories: profile
factions: 
owner: public
title: Kamil Lyraczek
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "orbiter, oddział verlen"
* owner: "public"
* title: "Kamil Lyraczek"


## Kim jest

### Paradoksalny Koncept

Ludzki kaznodzieja Arianny, traktujący ją jak boginię. Człowiek wierzący w czarodziejkę. Człowiek, który przemowami ma większy wpływ na Pryzmat niż większość magów. Przejął dowodzenie nad grupką Zbieraczy gdy ich statek został ciężko uszkodzony i patrzył jak jeden po drugim umierają - ale utrzymał ich w wysokim morale. Wtedy uratowała go Arianna. Ta trauma zdefiniowała całą jego przyszłość.

### Motto

"Kosmiczny porządek wymaga, by pojawiła się Arianna Verlen i wszystkich uratowała."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Żarliwa wiara i absolutne przekonanie w swoje racje. Wolą przekracza granice ciała. Jest niekorumpowalny i posunie się absurdalnie daleko by zapewnić sukces Sprawie.
* ATUT: Mistrz przemów, gadania i porywania za sobą tłumów. Manipuluje Pryzmatem, ciągnie za sobą tłumy, ale też potrafi przekonać większość osób indywidualnie do swojej racji.
* SŁABA: Jest absolutnym pacyfistą. Nie tylko odmawia ranienia kogokolwiek, ale w ogóle nie chce zadawać cierpienia.
* SŁABA: Uzależniony od pewnych środków uspokajających. Gdy ich nie ma, miesza mu się czasem rzeczywistość z demonami przeszłości.

### O co walczy (3)

* ZA: Optymizm. Dobro. Ciepło. Pozytywne uczucia. Rzeczy, które sprawiają, że życie jest "dobre". Propagowaniem tego zajmował się odkąd stracił przyjaciół Zbieraczy.
* ZA: Odpowiedni PORZĄDEK na świecie - magowie u góry, ludzie poniżej, wszyscy inni poniżej. Świat winien być uporządkowany i bezpieczny.
* VS: Nie dopuścić do tego, by ktokolwiek umierał samotnie w ciemności próżni, tracąc resztki tlenu. Chce uratować wszystkich. Uratować jak największą ilość osób. 
* VS: Nie chce być bohaterem. Nie chce być zapamiętany. Chce działać z drugiej linii, móc być dumnym z piękna świata który pomógł zbudować.

### Znaczące Czyny (3)

* Przejął dowodzenie nad grupką Zbieraczy i dawał im nadzieję aż poumierali zginęli w Anomalii Kolapsu. Do samego końca zachowywał wszystkich w nadziei oraz się nie złamał.
* Podczas procesu Arianny Verlen doprowadził do tego, że została uznana za "działającą w słusznej sprawie". Znalazł adwokata i obrócił opinię publiczną na jej korzyść.
* Zebrał załogę Inferni. Przekonał, by Infernia trafiła do Arianny. To on "stworzył" nowe życie Arianny Verlen. By to osiągnąć oddał wszystko, odrzucił wszystko.

### Kluczowe Zasoby (3)

* COŚ: Środki uspokajające dobrej klasy. Potrafią wyłączyć z akcji lub skupić umysł i usunąć strach.
* KTOŚ: Spora grupa zelotów Arianny Verlen, w zdecydowanej większości ludzi. Regularnie utrzymuje z nimi kontakty. Większość jest na Inferni.
* WIEM: Doskonale manipuluje Pryzmatem i wie jak wzmocnić lub osłabić maga używając grupy oddanych mu ludzi.
* OPINIA: Nawiedzony jak cholera, ale ma niesamowicie gadane. Warto z nim porozmawiać, zna świetne opowieści i historie.

## Inne

### Wygląd

.

### Coś Więcej

.


# Generated: 



## Fiszki


* OCEAN: (E+O-): Charyzmatyczny lider, który zawsze ma wytyczony kierunek i przekonuje do niego innych. "To jest nasza droga, musimy iść naprzód!" | @ 231011-ekstaflos-na-tezifeng
* VALS: (Face, Universalism, Tradition): Przekonuje wszystkich do swojego widzenia świata - optymizm, dobro i porządek. "Dla dobra wszystkich, dla lepszej przyszłości!" | @ 231011-ekstaflos-na-tezifeng
* Styl: Inspirujący lider z ogromnym sercem, który działa z cienia, zawsze gotów poświęcić się dla innych. "Nie dla chwały, ale dla przyszłości. Dla dobra wszystkich." | @ 231011-ekstaflos-na-tezifeng

### Wątki


triumfalny-powrot-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191025-nocna-krypta-i-bohaterka     | kaznodzieja ludzki, wierzący w Ariannę jak w boginię. Przez to przesuwa ją do poziomu Arcymaga, ale też ryzykuje życiem załogi oraz samej Arianny. Oficialnie, medyk. | 0108-05-06 - 0108-05-08 |
| 200106-infernia-martyn-hiwasser     | ludzki kaznodzieja Arianny, nadal z nią. Dobry do przesłuchiwania i ludzkich aspektów załogi - gadał, przekonywał i dopytywał by pomóc znaleźć Wiolettę a potem Martyna. | 0110-06-28 - 0110-07-03 |
| 200408-o-psach-i-krysztalach        | nie pamiętał o członkach załogi dotkniętych anomalią krystaliczną (co było cenną podpowiedzią dla Arianny i Klaudii). Dzięki temu Klaudia i Martyn poznali prawdę. | 0110-09-29 - 0110-10-04 |
| 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira | charyzmatyczny inspirator, po kiepskiej mowie Bladawira wygłosił własną do załogi. Robi show dla Arianny. Wysłany przez Ariannę na wydobycie informacji od Zaary i jej przekonanie o racjach Arianny wykonał zadanie - dowiedział się czego był w stanie i pokazał jej potencjalnie lepszy świat. | 0110-11-26 - 0110-11-30 |
| 200826-nienawisc-do-swin            | podburzony przez Klaudię, by chronić Ariannę doprowadził do buntu na Orbiterze. Klaudia skutecznie ukryła ten fakt przed wszystkimi, by nikt nie wiedział że to on zaczął. | 0111-02-05 - 0111-02-11 |
| 200909-arystokratka-w-ladowni-na-swinie | miał pomagać Klaudii; niestety, skończył nieprzytomny przez anomalię terroru Anastazji (w czym pomogły środki Martyna). | 0111-02-12 - 0111-02-17 |
| 201118-anastazja-bohaterka          | potężnym głosem kaznodziei przesunął miłość wobec Eustachego wszystkich na Inferni w stronę Arianny - gdzie owej miłości miejsce. | 0111-03-22 - 0111-03-25 |
| 201216-krypta-i-wyjec               | z kultystami Arianny udało mu się przyjąć potężne uderzenie krzyku Wyjca; utrzymał Infernię pryzmatycznie pod wpływem chorej anomalii. | 0111-03-29 - 0111-03-31 |
| 210414-dekralotyzacja-asimear       | skutecznie nastraszył Tomasza Sowińskiego, żeby ten współpracował z Arianną. Potem - ustawił kult Arianny, by wzmocnić jej moc przy łączeniu brainwave Jolanty i Eleny (by oszukać Entropik). | 0111-11-03 - 0111-11-12 |
| 210526-morderstwo-na-inferni        | mimo swoich wybitnych umiejętności ludzkich, inspiracji i perswazji nawet jemu nie udało się dotrzeć do noktian na Inferni. Wie o nich trochę, ale nie jest blisko i nie zna sekretów. | 0111-12-31 - 0112-01-06 |
| 210616-nieudana-infiltracja-inferni | skutecznie przekonał Rolanda Sowińskiego (i jego guwernantki), że Anomalia Kolapsu to najstraszniejsze miejsce ever. Nie chcą tam lecieć :D. | 0112-01-27 - 0112-02-01 |
| 211020-kurczakownia                 | pisze teksty, które biozsyntetyzowana "głowa" kultysty Zbawiciela-Niszczyciela ma mówić. Chodzi o przekonanie innych kultystów, że Arianna JEST Zbawicielem. | 0112-03-24 - 0112-03-26 |
| 211215-sklejanie-inferni-do-kupy    | wyjaśnił Ariannie, że z Izą tworzą kult dookoła Arianny (Vigilusa) i Eustachego (Nihilusa). Figurki itp. Jest za głęboko jej oddany. Arianna się zmartwiła... | 0112-04-27 - 0112-04-29 |
| 220105-to-nie-pulapka-na-nereide    | oddał życie, by tylko Infernia i kult mogły przetrwać atak Saitaera. Utrzymał memetycznie wiarę w Ariannę, w integralność Inferni. Wierzył nawet gdy miażdżyły go wrota. Nie miał lekkiej śmierci. KIA. | 0112-05-04 - 0112-05-09 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211020-kurczakownia                 | TRAUMA. Kurczakowanie - rekurczakowanie. Plus Zbawiciel-Niszczyciel. To oznacza, że ARIANNA jest NAPRAWDĘ aspektem Zbawiciela. Włączył w kult. | 0112-03-26

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 14 | ((191025-nocna-krypta-i-bohaterka; 200106-infernia-martyn-hiwasser; 200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211020-kurczakownia; 211215-sklejanie-inferni-do-kupy; 220105-to-nie-pulapka-na-nereide; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Klaudia Stryk        | 12 | ((200106-infernia-martyn-hiwasser; 200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211020-kurczakownia; 220105-to-nie-pulapka-na-nereide; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Eustachy Korkoran    | 11 | ((200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211020-kurczakownia; 211215-sklejanie-inferni-do-kupy; 220105-to-nie-pulapka-na-nereide)) |
| Martyn Hiwasser      | 9 | ((200106-infernia-martyn-hiwasser; 200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 211020-kurczakownia)) |
| Elena Verlen         | 8 | ((200909-arystokratka-w-ladowni-na-swinie; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211020-kurczakownia; 211215-sklejanie-inferni-do-kupy; 220105-to-nie-pulapka-na-nereide; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Leona Astrienko      | 5 | ((200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211215-sklejanie-inferni-do-kupy)) |
| Anastazja Sowińska   | 3 | ((200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec)) |
| Izabela Zarantel     | 3 | ((200909-arystokratka-w-ladowni-na-swinie; 210616-nieudana-infiltracja-inferni; 220105-to-nie-pulapka-na-nereide)) |
| OO Infernia          | 3 | ((211020-kurczakownia; 211215-sklejanie-inferni-do-kupy; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| AK Nocna Krypta      | 2 | ((191025-nocna-krypta-i-bohaterka; 201216-krypta-i-wyjec)) |
| Antoni Kramer        | 2 | ((200826-nienawisc-do-swin; 210526-morderstwo-na-inferni)) |
| Diana Arłacz         | 2 | ((201118-anastazja-bohaterka; 201216-krypta-i-wyjec)) |
| Flawia Blakenbauer   | 2 | ((210616-nieudana-infiltracja-inferni; 211020-kurczakownia)) |
| Maria Naavas         | 2 | ((211215-sklejanie-inferni-do-kupy; 220105-to-nie-pulapka-na-nereide)) |
| Marian Tosen         | 2 | ((210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni)) |
| Otto Azgorn          | 2 | ((210526-morderstwo-na-inferni; 211020-kurczakownia)) |
| Roland Sowiński      | 2 | ((210616-nieudana-infiltracja-inferni; 211215-sklejanie-inferni-do-kupy)) |
| Adam Szarjan         | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| AK Wyjec             | 1 | ((201216-krypta-i-wyjec)) |
| Aleksandra Termia    | 1 | ((200826-nienawisc-do-swin)) |
| Antoni Bladawir      | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Diana d'Infernia     | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Dominik Ogryz        | 1 | ((200106-infernia-martyn-hiwasser)) |
| Feliks Walrond       | 1 | ((210526-morderstwo-na-inferni)) |
| Karol Reichard       | 1 | ((210526-morderstwo-na-inferni)) |
| Kasandra Destrukcja Diakon | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Lars Kidironus       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Llarnagraht          | 1 | ((210414-dekralotyzacja-asimear)) |
| Lutus Amerin         | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Malictrix d'Pandora  | 1 | ((210414-dekralotyzacja-asimear)) |
| Mariusz Tubalon      | 1 | ((210414-dekralotyzacja-asimear)) |
| Marta Keksik         | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Martyna Bianistek    | 1 | ((210414-dekralotyzacja-asimear)) |
| Natalia Aradin       | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| OO Galaktyczny Tucznik | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| OO Opresor           | 1 | ((210616-nieudana-infiltracja-inferni)) |
| OO Paprykowiec       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| OO Tivr              | 1 | ((210526-morderstwo-na-inferni)) |
| Patryk Samszar       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Saitaer              | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| SCA Płetwal Błękitny | 1 | ((210414-dekralotyzacja-asimear)) |
| Sebastian Alarius    | 1 | ((200826-nienawisc-do-swin)) |
| Sebastian Namczek    | 1 | ((200408-o-psach-i-krysztalach)) |
| Stefan Jamniczek     | 1 | ((201118-anastazja-bohaterka)) |
| Szczepan Olgrod      | 1 | ((200106-infernia-martyn-hiwasser)) |
| Tadeusz Ursus        | 1 | ((200826-nienawisc-do-swin)) |
| Tal Marczak          | 1 | ((210526-morderstwo-na-inferni)) |
| Tomasz Sowiński      | 1 | ((210414-dekralotyzacja-asimear)) |
| Vigilus Mevilig      | 1 | ((211020-kurczakownia)) |
| Wioletta Keiril      | 1 | ((200106-infernia-martyn-hiwasser)) |
| Wojciech Kuszar      | 1 | ((191025-nocna-krypta-i-bohaterka)) |
| Zaara Mieralit       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |