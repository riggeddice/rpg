---
categories: profile
factions: 
owner: public
title: Olena Orion
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210929-grupa-ekspedycyjna-kellert   | Emulatorka dostarczona przez Termię na Infernię. Walczyła przeciwko Vigilusowi z Sektora Mevilig wraz z paladynami Verlenów. | 0112-03-13 - 0112-03-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Aleksandra Termia    | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Antoni Kramer        | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Arianna Verlen       | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Elena Verlen         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Eustachy Korkoran    | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Klaudia Stryk        | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Leona Astrienko      | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Martyn Hiwasser      | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Infernia          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Otto Azgorn          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Vigilus Mevilig      | 1 | ((210929-grupa-ekspedycyjna-kellert)) |