---
categories: profile
factions: 
owner: public
title: Marcjanna Maszotka
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180701-dwa-rejsy-w-potrzebie        | lekarz pokładowy Krwawego Wojownika, która ma zasady i nie zgadza się na niektóre rzeczy co na Wojowniku się dzieją. Odmówiła sprzężenia Liwii z Gladiatorem i zajmowała się Gerardem, gdy ten był ciężko Skażony. Starsza czarodziejka, odpowiedzialna i oczytana. I... tania, jej słowami. | 0109-08-07 - 0109-08-11 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180701-dwa-rejsy-w-potrzebie        | ma adoratora w Gerardzie Weinerze. Ona jeszcze nie zauważa tego faktu. | 0109-08-11
| 180701-dwa-rejsy-w-potrzebie        | dostała stanowisko Chief Medical Officer za działania na Wojowniku; jest przypisana do statku chwilowo. | 0109-08-11

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Emilia Kariamon      | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Fabian Komczatkow    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Gerard Weiner        | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Kacper Wontarczyk    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Kalina Rotmistrz     | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Krystian Moborok     | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Liwia Barbaturran    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Martauron Attylla    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Sabrina Powsimrożek  | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Tymon Barbaturran    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Wojciech Zajcew      | 1 | ((180701-dwa-rejsy-w-potrzebie)) |