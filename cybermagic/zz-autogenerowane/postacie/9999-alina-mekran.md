---
categories: profile
factions: 
owner: public
title: Alina Mekran
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240214-relikwia-z-androida          | asystentka Elwiry; odkryła plagiat Elwiry i się z nią skonfrontowała. Zginęła z ręki Elwiry, ale wróciła dla męża. Anomalia Anteclis. Zginęła na dobre w ogniu silników Szernief. | 0107-05-16 - 0107-05-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aerina Cavalis       | 1 | ((240214-relikwia-z-androida)) |
| Elwira Barknis       | 1 | ((240214-relikwia-z-androida)) |
| Kalista Surilik      | 1 | ((240214-relikwia-z-androida)) |
| Klasa Dyplomata      | 1 | ((240214-relikwia-z-androida)) |
| Klasa Hacker         | 1 | ((240214-relikwia-z-androida)) |
| Klasa Inżynier       | 1 | ((240214-relikwia-z-androida)) |
| Klasa Oficer Naukowy | 1 | ((240214-relikwia-z-androida)) |
| Klasa Sabotażysta    | 1 | ((240214-relikwia-z-androida)) |
| Mawir Hong           | 1 | ((240214-relikwia-z-androida)) |
| Tadeusz Mekran       | 1 | ((240214-relikwia-z-androida)) |
| Vanessa d'Cavalis    | 1 | ((240214-relikwia-z-androida)) |