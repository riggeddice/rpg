---
categories: profile
factions: 
owner: public
title: Fircjusz Szarstasz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230314-brudna-konkurencja-w-arachnoziem | właściciel ArachnoBuild, próbuje uratować swoją rodzinę i firmę. Ma się ku Ani (sekretarki) i ogólnie wykonuje pracę dobrze. Ufa swoim pracownikom i dobiera takich że są godni zaufania. | 0095-06-20 - 0095-06-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Ania Turabnik        | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Fiona Szarstasz      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Julita Mopsarin      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Kacper Aczramin      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Karolinus Samszar    | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Laura Turabnik       | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |