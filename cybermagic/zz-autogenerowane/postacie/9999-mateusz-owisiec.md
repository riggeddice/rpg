---
categories: profile
factions: 
owner: public
title: Mateusz Owisiec
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230831-potwory-ktore-przetrwaly-eter | 'corrupted Perceptor', wężoform z laserem, kiedyś: snajper, syn Alberta (kapitana); skutecznie robi dywersję dla reszty Zespołu by Mimik mógł się wślizgnąć do arkologii. Ogólnie optymistyczny, acz z czarnym humorem. | 0093-03-11 - 0093-03-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Mardiblon     | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Bartek Mardiblon     | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Dawid Nałęcznik      | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Diana Nałęcznik      | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Esaria Mirtalis      | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Karol La Viris       | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Rosenkrat Amiribasit | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |