---
categories: profile
factions: 
owner: public
title: Miranda Ceres
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220327-wskrzeszenie-krolowej-przygod | bardzo zmodyfikowana TAI Ceres; ma jakieś powiązanie z Percivalem Diakonem. Reanimowana w Pasie Kazimierza, wrobiła wszystkich w to, że jej statek (Królowa Przygód) będzie odbudowana przez blakvelowców i trzech górników zostanie członkami jej załogi. | 0108-05-01 - 0108-05-16 |
| 231027-planetoida-bogatsza-niz-byc-powinna | na typowej operacji górniczej ratowała załogę przed kiepskim sprzętem i awariami udając Ceres. Gdy pojawiła się anomalia Alteris to najpierw wylogikowała z czym ma do czynienia, potem użyła paranoi Łucjana by on też do tego doszedł a potem udawała że jest psyche ludzi i gadała do nich bezpośrednio. Na końcu - zestrzeliła NieGórnika i pokasowała odpowiednio logi. | 0108-06-29 - 0108-07-02 |
| 220329-mlodociani-i-pirat-na-krolowej | nigdy się nie zdradziła, choć to ona steruje Królową Przygód. Uratowała statek przed Berdyszem, potem udając Annę połączyła z nim dzieci. Niewidzialne wsparcie. | 0108-08-29 - 0108-09-09 |
| 220405-lepsza-kariera-dla-romki     | nadal działa z cienia - pokazała Annie że Helena się szprycuje i pokazała Romce ULTRA HARD PORN by ta nie szła w prostytucję. Królowa cieni :-). | 0108-09-15 - 0108-09-17 |
| 220503-antos-szafa-i-statek-piscernikow | koordynowała i dyskretnie przesyłała wszystkie ważne informacje Annie i Zespołowi - np. to, że Luxuritias / Piscernik to może być statek niewolniczy. | 0108-09-23 - 0108-09-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| SC Królowa Przygód   | 4 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Anna Szrakt          | 3 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki; 220503-antos-szafa-i-statek-piscernikow)) |
| Gotard Kicjusz       | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Łucjan Torwold       | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Prokop Umarkon       | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Seweryn Grzęźlik     | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Antos Kuramin        | 2 | ((220405-lepsza-kariera-dla-romki; 220503-antos-szafa-i-statek-piscernikow)) |
| Bartek Wudrak        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220503-antos-szafa-i-statek-piscernikow)) |
| Damian Szczugor      | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220405-lepsza-kariera-dla-romki)) |
| Helena Banbadan      | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Kara Prazdnik        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220503-antos-szafa-i-statek-piscernikow)) |
| Romana Kundel        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Wojciech Kaznodzieja | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Bartek Burbundow     | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Berdysz Rozdzieracz  | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Grzegorz Fabutownik  | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Juliusz Cieślawok    | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Kazimierz Zamglis    | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Mikołaj Resztkowiec  | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Ursyn Uszat          | 1 | ((220405-lepsza-kariera-dla-romki)) |