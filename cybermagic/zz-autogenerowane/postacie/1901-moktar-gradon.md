---
categories: profile
factions: 
owner: public
title: Moktar Gradon
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "cieniaszczyt, lyse_psy"
* owner: "public"
* title: "Moktar Gradon"


## Postać

### Ogólny pomysł (3)

Potężna góra mięśni i złości, częściowo techorganiczny. Ciało zbudowane przez Saitaera, umysł uwolniony przez Arazille. Kapłan dwóch bogów, niewolnik żadnego. Mistyk wykorzystujący energię bogów. Terminator. Nieskończona wola walki i zniszczenia. Szturmowiec i gladiator, zna się perfekcyjnie na różnych typach broni. Charyzmatyczny i przerażający przywódca Łysych Psów.

Z uwagi na specyfikę (techorganiczny awatar dwóch bogów) **+1 kategoria w sprawach powiązanych z konceptem**

### Motywacja (gniew/wartość, zmiana, sposób) (3)
 
* ENJOYMENT/POWER; wszystko podda się jego woli i jego siła przewyższy wszystko; robienie tego, na co ma ochotę i dewastacja wrogów
* ADAPTABILITY/FREEDOM; perfekcja ciała i wolność umysłu dla każdego; zaczynając od Łysych Psów a kończąc na świecie - explore, expand, evolve
* odebrali mu Karradraela; będzie mieć WSZYSTKICH bogów; zintegrować się ze wszystkimi tak jak z Arazille i Saitaerem
* bezwzględny, okrutny, warczący, wszystko sprowadza do konfrontacji, pogardliwy

### Wyróżniki (3)

* Military freak: jeśli jest cokolwiek do wiedzenia na temat broni i jej efektów w różnych okolicznościach, on to wie.
* Zmuszanie i zastraszanie: zna język siły i przymusu i doskonale potrafi go używać by dostać czego chce.
* Technomagiczne implanty: silniejszy, szybszy i potężniejszy; jest żywym triumfem magitechnologii i determinacji nad dobrym smakiem i człowieczeństwem
* Używa mocy bogów - Saitaera i Arazille, nie będąc przez nie Skażony.
* Używa mocy bogów w sposób niemożliwy dla maga, by zrobić rzeczy niemożliwe. Najczęściej to wolność Arazille czy furia Saitaera.
* Używając swojej charyzmy, intelektu i okrutnych mocy sprzęga i łamie nawet najsilniejsze istoty.
* Niepowstrzymany. Niezależnie od siły ognia, obrażeń, trucizny itp. Nie da się go powstrzymać. Moktar nie padnie.

### Zasoby i otoczenie (3)

* Szantaże, długi i plotki: zawsze może zmusić drugą stronę do stawienia mu czoła w bezpośredniej walce.
* Nielegalne wspomagania i środki: narkotyki, stymulanty i inne środki wspomagające w walce lub łamiące innych.
* Dowódca Łysych Psów: dowodzi oddziałem straceńców których trzyma karnie i brutalnie - słuchają go ze strachu. 
* Pancerny Power Suit: ciężko opancerzony power suit o dużej sile ognia, silny i wytrzymały acz niezbyt szybki.
* Niezliczone typy broni: military freak, zawsze ma mniej lub bardziej legalną broń radzącą sobie z danym problemem.

### Magia (3)

#### Gdy kontroluje energię

* Manipulacja bronią i sprzętem: military freak zdolny do adaptacji i przekształcania broni w zależności od sytuacji.
* Mroczna strona natury: potrafi wszystkich wepchnąć w stan berserk czy strachu. Ciemna strona emocji to jego strona.
* Last Mage Standing: potrafi leczyć się kosztem innych i regenerować kosztem innych. Niekoniecznie sojuszników.
* Potęga Saitaera i Arazille: potrafi przywołać energię bóstw i się nimi wzmocnić.

#### Gdy traci kontrolę

* Eruptor: jego gniew i nienawiść wpływają na niego lub innych, zwykle realizując się w berserku lub terrorze.
* Krwawiec: jego magia kieruje się w formę Krwi, działając potężniej, acz ze straszliwymi efektami ubocznymi.
* Strzelba na ścianie: rzeczy zdolne do zadawania bólu i ran zaczynają zadawać ból i rany (np. broń sama wystrzeli).
* Uwolnieni Bogowie: Saitaer lub Arazille manifestują swoją moc w tym obszarze.

### Powiązane frakcje

{{ page.factions }}

## Opis

Chyba najgroźniejsza postać jaką potrafię stworzyć w tym systemie.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181125-swaty-w-cieniu-potwora       | potwór. Złapał sobie Lilię, by ją złamać. Stoczył walkę z Pięknotką, w wyniku której Pietro wyniósł Lilię. Nie osiągnął sukcesu - ale obiecał Pięknotce, że się spotkają. | 0109-10-29 - 0109-11-01 |
| 181218-tajemniczy-oltarz-moktara    | upewnił się, że Pięknotki nie dotyczą wpływy Arazille. Wzmacnia Arazille, ale nie chce by ona miała gdzieś tu swój hold. | 0109-11-11 - 0109-11-13 |
| 181225-czyszczenie-toksycznych-zwiazkow | wszedł do Colubrinus Meditech sprawdzić o co chodzi z Julią i uratował Pięknotkę od Bogdana. Przez MOMENT stracił kontrolę. Channelował Arazille i Saitaera. | 0109-11-13 - 0109-11-17 |
| 181226-finis-vitae                  | po zregenerowaniu Pięknotki dał jej zadanie uratowania Dariusza; co więcej, oddał jej życie Lilii i wyjaśnił problem podziemi. | 0109-11-18 - 0109-11-27 |
| 181227-adieu-cieniaszczycie         | przekonał (?!) Pięknotkę, że jeśli walka ma się toczyć z Saitaerem, Pięknotka CHCE by Psy miały Julię na pełnej mocy. | 0109-11-28 - 0109-12-01 |
| 190724-odzyskana-agentka-orbitera   | champion Pięknotki do walki z kralotycznym championem przekupiony walką z Kirasjerką. Pokonał wszystko z czym walczył i się doskonale bawił. | 0110-06-12 - 0110-06-15 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181125-swaty-w-cieniu-potwora       | epicka ballada Romualda sprawiła, że jest postrzegany jako jeszcze straszniejszy potwór. Z czym Moktar dobrze się czuje. | 0109-11-01
| 181125-swaty-w-cieniu-potwora       | planuje zapolować na Pięknotkę Diakon (tak przy okazji) - chce jej pokazać KTO naprawdę tu rządzi. | 0109-11-01
| 181209-kajdan-na-moktara            | ma ogromną wiedzę o dziwnych nojrepach i całej tej sprawie z Orbiterem Pierwszym | 0109-11-03
| 181209-kajdan-na-moktara            | wie, że Pięknotka ma dowód mogący poważnie uszkodzić jak nie zniszczyć Łyse Psy. Musi działać przeciw niej ostrożniej. | 0109-11-03
| 181209-kajdan-na-moktara            | Podzielił się z Pięknotką strefami wpływów; jak długo nie wchodzą sobie w drogę, Pięknotka jest bezpieczna. | 0109-11-03
| 181225-czyszczenie-toksycznych-zwiazkow | czuje do Pięknotki niechętny szacunek - wystarczająco godny przeciwnik. | 0109-11-17

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 6 | ((181125-swaty-w-cieniu-potwora; 181218-tajemniczy-oltarz-moktara; 181225-czyszczenie-toksycznych-zwiazkow; 181226-finis-vitae; 181227-adieu-cieniaszczycie; 190724-odzyskana-agentka-orbitera)) |
| Atena Sowińska       | 4 | ((181218-tajemniczy-oltarz-moktara; 181225-czyszczenie-toksycznych-zwiazkow; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Waleria Cyklon       | 4 | ((181125-swaty-w-cieniu-potwora; 181218-tajemniczy-oltarz-moktara; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Amadeusz Sowiński    | 2 | ((181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Bogdan Szerl         | 2 | ((181218-tajemniczy-oltarz-moktara; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Julia Morwisz        | 2 | ((181225-czyszczenie-toksycznych-zwiazkow; 190724-odzyskana-agentka-orbitera)) |
| Lilia Ursus          | 2 | ((181125-swaty-w-cieniu-potwora; 181227-adieu-cieniaszczycie)) |
| Mirela Niecień       | 2 | ((181227-adieu-cieniaszczycie; 190724-odzyskana-agentka-orbitera)) |
| Pietro Dwarczan      | 2 | ((181125-swaty-w-cieniu-potwora; 181227-adieu-cieniaszczycie)) |
| Romuald Czurukin     | 2 | ((181125-swaty-w-cieniu-potwora; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Adam Szarjan         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Aida Serenit         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Arazille             | 1 | ((181226-finis-vitae)) |
| Brygida Maczkowik    | 1 | ((181227-adieu-cieniaszczycie)) |
| Erwin Galilien       | 1 | ((181227-adieu-cieniaszczycie)) |
| Mirela Orion         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Wioletta Kalazar     | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Zbigniew Burzycki    | 1 | ((181227-adieu-cieniaszczycie)) |