---
categories: profile
factions: 
owner: public
title: Tomasz Kaltaben
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220223-stabilizacja-keldan-voss     | kapitan Światła Nadziei; pallidańskiego statku mającego uratować co się da z kolonii Keldan Voss. | 0112-06-04 - 0112-06-07 |
| 220309-upadek-eleny                 | pomoże Keldan Voss ze strony pallidan. Nie podoba mu się eksperyment z Keldan Voss ale będzie kontynuował. Nie będzie tępić Anniki, wesprze ją politycznie. Dogadał się z Arianną. | 0112-06-08 - 0112-06-14 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220309-upadek-eleny                 | reputacja: "on wykazał się skutecznością i niezłomnością. Jego PRZYWÓDZTWO doprowadziło do tego, że sprawa jest rozwiązana po stronie pallidan." Plus - 3 magów Orbitera co jego wspierają. | 0112-06-14

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Annika Pradis        | 2 | ((220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Arianna Verlen       | 2 | ((220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Elena Verlen         | 2 | ((220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Eustachy Korkoran    | 2 | ((220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Klaudia Stryk        | 2 | ((220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| SP Światło Nadziei   | 1 | ((220223-stabilizacja-keldan-voss)) |
| Szczepan Kaltaben    | 1 | ((220223-stabilizacja-keldan-voss)) |