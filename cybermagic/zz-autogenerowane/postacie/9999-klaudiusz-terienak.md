---
categories: profile
factions: 
owner: public
title: Klaudiusz Terienak
---

# {{ page.title }}


# Generated: 



## Fiszki


* medical officer, tien, (2 med pod nim) | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* ENCAO:  +-000 |Bezkompromisowy, nieustępliwy| VALS: Power, Stimulation| DRIVE: Przejąć władzę | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej

### Wątki


kosmiczna-chwala-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly | oficer medyczny Królowej i tien; współpracuje z Władawcem i jedyną osobą której naprawdę się boi jest Leona Astrienko. | 0100-05-06 - 0100-05-12 |
| 221026-kapitan-verlen-i-koniec-przygody-na-krolowej | jest grzeczniutki bo się boi po tym jak spieprzył z Marceliną; zatruł syntezator alkoholu dla Arianny by było przeczyszczenie i wejście do Ruppoka. | 0100-06-08 - 0100-06-15 |
| 231011-ekstaflos-na-tezifeng        | podczas działań celnych napotkał na kralotha, co zmieniło go w ekstaflos. Stał się żywą pułapką, infekującą statki i przekształcającą ludzi w piękne dziewczyny w swoim Kwiecie. Najbardziej na świecie pragnął zdominować Ariannę i Leonę, ale jego pułapka nie zadziałała. Przechwycony przez siły Orbitera. | 0110-10-20 - 0110-10-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 231011-ekstaflos-na-tezifeng)) |
| Arnulf Perikas       | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Daria Czarnewik      | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Erwin Pies           | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Leona Astrienko      | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 231011-ekstaflos-na-tezifeng)) |
| Leszek Kurzmin       | 2 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 231011-ekstaflos-na-tezifeng)) |
| OO Królowa Kosmicznej Chwały | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Alezja Dumorin       | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Antoni Kramer        | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Elena Verlen         | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Eustachy Korkoran    | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Grażyna Burgacz      | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Klarysa Jirnik       | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Lars Kidironus       | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Maja Samszar         | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Natalia Gwozdnik     | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OO Castigator        | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OO Infernia          | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OO Tezifeng          | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OO Tucznik Trzeci    | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Raoul Lavanis        | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Stefan Torkil        | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Szymon Wanad         | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Tomasz Ruppok        | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Władawiec Diakon     | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |