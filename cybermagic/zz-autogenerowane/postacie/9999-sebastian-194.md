---
categories: profile
factions: 
owner: public
title: Sebastian-194
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231213-polowanie-na-biosynty-na-szernief | z ukrycia chroni Kalistę z sobie tylko znanych powodów. Gdy została zaatakowana przez Parasektowców, Sebastian ich brutalnie pobił. Gdy pojawiła się Agencja, po przekonaniu, dał im ciepły kontakt do Delgado. | 0105-07-26 - 0105-07-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Artur Tavit          | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Delgado Vitriol      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Felina Amatanir      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Kalista Surilik      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Klasa Biurokrata     | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Klasa Dyplomata      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Klasa Hacker         | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Klasa Oficer Naukowy | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Klasa Sabotażysta    | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Malik Darien         | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Vanessa d'Cavalis    | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |