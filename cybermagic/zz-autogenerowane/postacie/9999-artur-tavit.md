---
categories: profile
factions: 
owner: public
title: Artur Tavit
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231213-polowanie-na-biosynty-na-szernief | arystokrata na Szernief; walczy o prawa biosyntów i współpracuje z Vanessą d'Cavalis. Agenca dała mu 'własność' wszystkich biosyntów przez co nie wolno ich niszczyć, ale on płaci za wszystkie problemy z nimi reputacyjnie. | 0105-07-26 - 0105-07-31 |
| 240117-dla-swych-marzen-warto       | NIE dotknięty przez Esuriit, robi gorące apele i mowy by zintegrować biosynty. Maia chciała go zabić, lecz Agencja go uratowała. Jego marzenie spełnione - biosynty zintegrowane. Ale jego lojalna Vanessa została zniszczona. | 0106-11-04 - 0106-11-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Felina Amatanir      | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Kalista Surilik      | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Klasa Hacker         | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Klasa Oficer Naukowy | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Klasa Sabotażysta    | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Vanessa d'Cavalis    | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Aerina Cavalis       | 1 | ((240117-dla-swych-marzen-warto)) |
| Delgado Vitriol      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Estril Cavalis       | 1 | ((240117-dla-swych-marzen-warto)) |
| Klasa Biurokrata     | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Klasa Dyplomata      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Larkus Talvinir      | 1 | ((240117-dla-swych-marzen-warto)) |
| Maia Sakiran         | 1 | ((240117-dla-swych-marzen-warto)) |
| Malik Darien         | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Mawir Hong           | 1 | ((240117-dla-swych-marzen-warto)) |
| Sebastian-194        | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |