---
categories: profile
factions: 
owner: public
title: Róża Kalatrix
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240227-cykl-krwawej-przadki         | Michał wymordował jej rodzinę pod wpływem Prządki. Sama przemierza drogę od zemsty do poszukiwania uwolnienia od mrocznej mocy, która nią kieruje. Ma krew na rękach tak jak Michał; podjęła decyzję o współpracy z Michałem nawet jak ją to zabije by tylko pozbyć się cyklu Prządki. | 0099-05-22 - 0099-05-28 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 240227-cykl-krwawej-przadki         | docelowo będzie uwolniona od Prządki; Michał i Instytut jej pomogli. Zaprzyjaźni się z Lucyną Castelli. | 0099-05-28

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Wokniaczek      | 1 | ((240227-cykl-krwawej-przadki)) |
| Edward Kopoktris     | 1 | ((240227-cykl-krwawej-przadki)) |
| Janusz Umizarit      | 1 | ((240227-cykl-krwawej-przadki)) |
| Jola Kalatrix        | 1 | ((240227-cykl-krwawej-przadki)) |
| Lucyna Castelli      | 1 | ((240227-cykl-krwawej-przadki)) |
| Michał Castelli      | 1 | ((240227-cykl-krwawej-przadki)) |
| Wincenty Frak        | 1 | ((240227-cykl-krwawej-przadki)) |