---
categories: profile
factions: 
owner: public
title: Keraina d'Orion
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191201-ukradziony-entropik          | miragent z customowym TAI. Pięknotka jej nie ufa. Szuka zaginionego Entropika z ramienia Kirasjerów Orbitera. | 0110-07-13 - 0110-07-15 |
| 200202-krucjata-chevaleresse        | miragent płynnie poruszający się po patrolowanym obszarze szpitala w Jastrząbcu. Przesłuchuje wszystko. Korozja defensyw spowodowana przez Malictrix którą umieściła. | 0110-07-24 - 0110-07-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Mariusz Trzewń       | 2 | ((191201-ukradziony-entropik; 200202-krucjata-chevaleresse)) |
| Minerwa Metalia      | 2 | ((191201-ukradziony-entropik; 200202-krucjata-chevaleresse)) |
| Pięknotka Diakon     | 2 | ((191201-ukradziony-entropik; 200202-krucjata-chevaleresse)) |
| Ataienne             | 1 | ((200202-krucjata-chevaleresse)) |
| Damian Orion         | 1 | ((200202-krucjata-chevaleresse)) |
| Diana Tevalier       | 1 | ((200202-krucjata-chevaleresse)) |
| Hestia d'Itaran      | 1 | ((191201-ukradziony-entropik)) |
| Malictrix d'Itaran   | 1 | ((200202-krucjata-chevaleresse)) |
| Mateusz Kardamacz    | 1 | ((200202-krucjata-chevaleresse)) |
| Persefona d'Jastrząbiec | 1 | ((191201-ukradziony-entropik)) |
| Roland Grzymość      | 1 | ((191201-ukradziony-entropik)) |
| Talia Aegis          | 1 | ((200202-krucjata-chevaleresse)) |
| Tomasz Tukan         | 1 | ((200202-krucjata-chevaleresse)) |