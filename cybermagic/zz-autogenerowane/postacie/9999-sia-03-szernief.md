---
categories: profile
factions: 
owner: public
title: Sia-03 Szernief
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa | po tym jak jej mentor - Tom-74 - zrobił rytuał Krwi, ona została Dotknięta Fidetis. Założyła frakcję Oszczędnościowców, sabotując na lewo i prawo niepotrzebne do przetrwania rzeczy. Niech wszyscy żyją jak savaranie. Po tym jak Agencja zniszczyła Łowcę, Sia ascendowała połączona z Tkaczką. | 0105-12-11 - 0105-12-13 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa | jest ascendowana przez integrację z Tkaczką Fidetis, może dalej prowadzić frakcję Oszczędnościowców. Jest silniejsza niż była, częściowo homo superior. | 0105-12-13

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aerina Cavalis       | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Dorion Fughar        | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Felina Amatanir      | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Gabriel Septenas     | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Kaldor Czuk          | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Kalista Surilik      | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klasa Dyplomata      | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klasa Inżynier       | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klasa Sabotażysta    | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Mawir Hong           | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |