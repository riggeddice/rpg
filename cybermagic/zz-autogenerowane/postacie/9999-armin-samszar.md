---
categories: profile
factions: 
owner: public
title: Armin Samszar
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (E+N+C-): głośny i zapalczywy, przyciąga uwagę wszystkich rozkazującymi gestami. Wszystko czego pragnie ma być jego. "Kto nie żąda, nie ma dość sił by móc żądać." | @ 230808-nauczmy-mlodego-tiena-jak-zyc
* VALS: (Face, Hedonism): skupia się na tym by traktowano go jak księcia i udawaniu dobrotliwego. "Jeśli czegoś pragnę, dostanę to, dobry człowieku. Spraw, bym pragnął Ci pomóc." | @ 230808-nauczmy-mlodego-tiena-jak-zyc
* Core Wound - Lie: "Myśleli, że nie jestem tienem to mnie odrzucili" - "Zasługuję na to, aby być w centrum uwagi! Jak mi nie dadzą, wezmę sam!" | @ 230808-nauczmy-mlodego-tiena-jak-zyc
* styl: extremely confident and prideful, acting in a dignified manner and constantly giving the impression that he looks down upon others. | @ 230808-nauczmy-mlodego-tiena-jak-zyc

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230808-nauczmy-mlodego-tiena-jak-zyc | 16-letni 'książę', którego trzeba nauczyć życia; ma tendencje do robienia wszystkiego samemu, otaczania się młodymi dziewczynami i nadużywania władzy. Ma cztery strażniczki, śliczniuteńkie i nadużywa władzy wobec nich itp. Poszedł z jedną z nich podpuszczony przez Kleopatrę, spotkawszy się z POTWOREM ten złamał Arminowi rękę. Żałośnie uciekał, zostawił dziewczyny na śmierć i Blakenbauerskie stwory go pocięły. W końcu uratował go traper (ranger) po stronie Blakenbauerów. Nauczył się, że ludzie to COŚ WIĘCEJ. | 0095-09-14 - 0095-09-23 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230808-nauczmy-mlodego-tiena-jak-zyc | ciężkimi ranami, złamaniem ręki itp. nauczył się, że ludzie mają wartość i nie można ich lekceważyć ani nadużywać oraz że nie wszystko rozwiążesz samemu. | 0095-09-23

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Celina Maskiewnik    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Elena Samszar        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Florian Samszar      | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Karolinus Samszar    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Kleopatra Trusiek    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Rufus Bilgemener     | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Tadeusz Maskiewnik   | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Wiktor Blakenbauer   | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |