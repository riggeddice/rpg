---
categories: profile
factions: 
owner: public
title: Esaria Mirtalis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230831-potwory-ktore-przetrwaly-eter | komandoska Coruscatis; dowiedziawszy się że Potwory porwały dzieci z arkologii udała się z Rosenkratem w infiltrację resztek Śmiechu Sępa. Miała pecha - nie zginęła, dołączyła do czegokolwiek tam jest, przekształcona... | 0093-03-11 - 0093-03-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Mardiblon     | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Bartek Mardiblon     | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Dawid Nałęcznik      | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Diana Nałęcznik      | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Karol La Viris       | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Mateusz Owisiec      | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Rosenkrat Amiribasit | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |