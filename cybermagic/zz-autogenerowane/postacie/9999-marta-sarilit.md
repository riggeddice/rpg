---
categories: profile
factions: 
owner: public
title: Marta Sarilit
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (N+ O+) | "Jeśli nie będziemy dość szybcy, zostaniemy zniszczeni." | VALS: (Benevolence, Power) | DRIVE: "Naprawa i lecznie" | @ 231119-tajemnicze-tunele-sebirialis
* dowodzi więzieniem NavirMed; to jej operacja | @ 231119-tajemnicze-tunele-sebirialis

### Wątki


agencja-lux-umbrarum
problemy-con-szernief

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231119-tajemnicze-tunele-sebirialis | dyrektor NavirMed; bezwzględnie mistrzowska w polityce, angażuje się w eksperymenty z neurokontrolą i biomodyfikacją. Bardzo pragmatyczna, chce być gotowa na następną wojnę z Astorią. Decyduje się współpracować z Agencją. | 0104-11-20 - 0104-11-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aniela Kafantelas    | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Eryk Kawanicz        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Felina Amatanir      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Ignatius Sozyliw     | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Janvir Krassus       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Kalista Surilik      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Klasa Dyplomata      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Klasa Sabotażysta    | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Larkus Talvinir      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Leon Hurmniow        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| OLU Luminarius       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |