---
categories: profile
factions: 
owner: public
title: Tymon Korkoran
---

# {{ page.title }}


# Generated: 



## Fiszki


* egzaltowany lekkoduch i następca Bartłomieja | @ 230201-wylaczone-generatory-memoriam-inferni
* (ENCAO:  +--00 |Co chce, weźmie;;Niecierpliwy;;Egzaltowany | Stimulation, Tradition > Face, Humility | TAK: Być lepszym od Bartłomieja w oczach Kidirona) | @ 230201-wylaczone-generatory-memoriam-inferni
* "Infernia jest moja i należy do mnie. Bartłomiej marnuje jej potencjał, a dzięki niej Nativis może rządzić Neikatis!" | @ 230201-wylaczone-generatory-memoriam-inferni
* egzaltowany lekkoduch i już nie następca Bartłomieja | @ 230208-pierwsza-randka-eustachego

### Wątki


infernia-jej-imieniem
kidiron-zbawca-nativis

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220723-polowanie-na-szczury-w-nativis | lekkoduch rozdarty między wujem Bartłomiejem i Laurencjuszem Kidironem (15); został słupem dla Celiny odnośnie polowania na szczury, bo chce zaimponować swojej eks-dziewczynie że robi coś przydatnego (i zarobić za nic). | 0087-05-03 - 0087-05-12 |
| 220831-czarne-helmy-i-robaki        | kuzyn Eustachego; bardzo chce się przypodobać Kidironom; Eustachy wpakował go w pułapkę by Robaki go sklepały. Tak dąży do chwały i potęgi że wpadł w to jak śliwka w kompot. De facto został "tym przez kogo rozwiązano konflikt" w oczach Kidironów i Nativis. | 0092-08-15 - 0092-08-27 |
| 230201-wylaczone-generatory-memoriam-inferni | jedyny świadek tego, że Eustachy COŚ ZROBIŁ z Infernią. Wszystko się mu rozpada - Kidiron skupia się na Eustachym, Infernia wypada z rąk. Eksplodował na Celinę nazywając ją znajdką i wujek nim wzgardził. Potem sklupany przez Janka. Stracił WSZYSTKO. | 0093-02-10 - 0093-02-12 |
| 230215-terrorystka-w-ambasadorce    | regularnie próbował się wkraść do Ambasadorki podglądać dziewczyny i znalazł ścieżkę którą powiedział Ralfowi. Nie wiadomo czy być z niego dumnym czy nim gardzić. | 0093-02-22 - 0093-02-23 |
| 230628-wojna-o-arkologie-nativis-konsolidacja-sil | zdradził Infernię i dołączył do Laurencjusza. Zależy mu na przekonaniu Eustachego do sojuszu. Wierzy, że Lobrak to tylko doradca i dał się zaślepić Izabelli. | 0093-03-27 - 0093-03-28 |
| 230726-korkoran-placi-cene-za-nativis | naciśnięty przez Eustachego poza zakres maksimum, został złamany. Powiedział Eustachemu o wszystkich planach KiKo, po czym wysadził się z Wujkiem (swoim ojcem). "Zabrałeś mi wszystko, ojca Ci nie oddam". KIA. | 0093-03-29 - 0093-03-30 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220831-czarne-helmy-i-robaki        | przesunięty na zupełnie inną pozycję, wielka chwała u Kidironów. PLUS nieufność ojca (Bartłomieja Korkorana) i Karola Lertysa (dziadka Celiny i Janka) | 0092-08-27

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 5 | ((220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230726-korkoran-placi-cene-za-nativis)) |
| Eustachy Korkoran    | 5 | ((220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230726-korkoran-placi-cene-za-nativis)) |
| Bartłomiej Korkoran  | 4 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230726-korkoran-placi-cene-za-nativis)) |
| Celina Lertys        | 4 | ((220723-polowanie-na-szczury-w-nativis; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Jan Lertys           | 3 | ((220723-polowanie-na-szczury-w-nativis; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni)) |
| Ralf Tapszecz        | 3 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Tobiasz Lobrak       | 3 | ((230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230726-korkoran-placi-cene-za-nativis)) |
| Izabella Saviripatel | 2 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230726-korkoran-placi-cene-za-nativis)) |
| Kalia Awiter         | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce)) |
| Karol Lertys         | 2 | ((220723-polowanie-na-szczury-w-nativis; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Laurencjusz Kidiron  | 2 | ((220723-polowanie-na-szczury-w-nativis; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Marcel Draglin       | 2 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230726-korkoran-placi-cene-za-nativis)) |
| OO Infernia          | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230726-korkoran-placi-cene-za-nativis)) |
| Rafał Kidiron        | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce)) |
| Emilia d'Erozja      | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| OA Erozja Ego        | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Stanisław Uczantor   | 1 | ((220831-czarne-helmy-i-robaki)) |
| Wojciech Czerpń      | 1 | ((220831-czarne-helmy-i-robaki)) |