---
categories: profile
factions: 
owner: public
title: Aerina Cavalis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231202-protest-przed-kultem-na-szernief | arystokratka z Szernief; płomienna miłośniczka 'dobra' zmieniająca się w wendigo przez Esuriit. Kiedyś protestowała przeciwko ściąganiu drakolitów na Szernief. Odkąd manifestowała się Anomalia, Aerina ma niekontrolowany głód kanibalistyczny. Jej pierwszą ofiarą była znajoma z protestów. Zniknięcia ludzi wykonuje jej biosyntka, Vanessa, by Aerina nie była głodna. Współpracuje z Agencją by się wyleczyć. | 0103-10-15 - 0103-10-19 |
| 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa | próbuje jakoś przywrócić pokój na stacji mimo, że Sia-03 wysadziła jej pokój (by zredukować marnotrawstwo energii) | 0105-12-11 - 0105-12-13 |
| 240117-dla-swych-marzen-warto       | wpierw chroni ją Vanessa, potem ojciec. Straciła Vanessę. Celuje pistoletem w Agenta, by oddali jej pierścień; z jego pomocą może uratować stację, nawet kosztem swego życia, by jej ojciec nie ucierpiał. Przekonana by oddała broń; wszyscy udają że nic się nie stało. | 0106-11-04 - 0106-11-06 |
| 240214-relikwia-z-androida          | dążąc do ochrony reputacji stacji oraz Elwiry Barknis, próbowała zmanipulować sytuację, by utrzymać status quo, co niestety skutkowało izolacją Kalisty. Tormentowana przez przeszłość, będzie chronić stację za wszelką cenę. | 0107-05-16 - 0107-05-18 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 231202-protest-przed-kultem-na-szernief | Skażona przez Anomalię Esuriit; potrzebuje ludzkiego mięsa. Wyciągnięta przez Agencję, by jej pomóc. | 0103-10-19
| 240214-relikwia-z-androida          | jej przyjaźń z Kalistą się skończyła. Będzie chronić stację, Kalista może ją zniszczyć. | 0107-05-18

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kalista Surilik      | 4 | ((231202-protest-przed-kultem-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Hacker         | 3 | ((231202-protest-przed-kultem-na-szernief; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Sabotażysta    | 3 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Mawir Hong           | 3 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Vanessa d'Cavalis    | 3 | ((231202-protest-przed-kultem-na-szernief; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Felina Amatanir      | 2 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240117-dla-swych-marzen-warto)) |
| Klasa Dyplomata      | 2 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240214-relikwia-z-androida)) |
| Klasa Inżynier       | 2 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240214-relikwia-z-androida)) |
| Klasa Oficer Naukowy | 2 | ((240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Alina Mekran         | 1 | ((240214-relikwia-z-androida)) |
| Artur Tavit          | 1 | ((240117-dla-swych-marzen-warto)) |
| Dorion Fughar        | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Elwira Barknis       | 1 | ((240214-relikwia-z-androida)) |
| Estril Cavalis       | 1 | ((240117-dla-swych-marzen-warto)) |
| Frederico Zyklas     | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Gabriel Septenas     | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Kaldor Czuk          | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klasa Biurokrata     | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Larkus Talvinir      | 1 | ((240117-dla-swych-marzen-warto)) |
| Maia Sakiran         | 1 | ((240117-dla-swych-marzen-warto)) |
| OLU Luminarius       | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Sia-03 Szernief      | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Tadeusz Mekran       | 1 | ((240214-relikwia-z-androida)) |