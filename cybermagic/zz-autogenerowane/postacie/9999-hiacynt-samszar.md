---
categories: profile
factions: 
owner: public
title: Hiacynt Samszar
---

# {{ page.title }}


# Generated: 



## Fiszki


* młodszy oficer ds zaopatrzenia, bardzo wyczulony na świat duchów | @ 231018-anomalne-awarie-athamarein
* OCEAN: N+A+ | Ufny i wiecznie niepewny;; pragnie być lubiany;; nieśmiały | VALS: Universalism, Conformity | DRIVE: Zapewnić ludzkości jutro | @ 231018-anomalne-awarie-athamarein

### Wątki


legenda-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231018-anomalne-awarie-athamarein   | inżynier, zapatrzony w Uśmiechniczkę; jako jedyny podejrzewa, że to Athamarein jest problemem sama w sobie (Samszar), nikomu oczywiście słowa nie powie, bo się zbyt boi i wstydzi i nie jest pewny że ma rację. | 0111-12-22 - 0111-12-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alicja Szadawir      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Andrzej Gwozdnik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Arianna Verlen       | 1 | ((231018-anomalne-awarie-athamarein)) |
| Błażej Sowiński      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Elena Verlen         | 1 | ((231018-anomalne-awarie-athamarein)) |
| Eustachy Korkoran    | 1 | ((231018-anomalne-awarie-athamarein)) |
| Jakub Oroginiec      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Kajetan Kircznik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Klaudia Stryk        | 1 | ((231018-anomalne-awarie-athamarein)) |
| Leona Astrienko      | 1 | ((231018-anomalne-awarie-athamarein)) |
| OO Athamarein        | 1 | ((231018-anomalne-awarie-athamarein)) |
| Remigiusz Alkarenit  | 1 | ((231018-anomalne-awarie-athamarein)) |
| Uśmiechniczka Konstrukcjonistka Aurora Diakon | 1 | ((231018-anomalne-awarie-athamarein)) |