---
categories: profile
factions: 
owner: public
title: AK Wyjec
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201210-pocalunek-aspirii            | kiedyś noktiańska stacja medyczno naprawcza, anomalia sprzężona z Pocałunkiem Aspirii. Dostarcza Pocałunkowi koloidu, "żywi" się jeńcami. | 0111-03-26 - 0111-03-29 |
| 201216-krypta-i-wyjec               | walczył z Kryptą, by zostać przez nią zintegrowany w siebie. Przestaje być osobnym bytem. KIA. | 0111-03-29 - 0111-03-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Nocna Krypta      | 2 | ((201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Anastazja Sowińska   | 2 | ((201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Arianna Verlen       | 2 | ((201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Eustachy Korkoran    | 2 | ((201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Klaudia Stryk        | 2 | ((201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Diana Arłacz         | 1 | ((201216-krypta-i-wyjec)) |
| Donald Parziarz      | 1 | ((201210-pocalunek-aspirii)) |
| Juliusz Sowiński     | 1 | ((201210-pocalunek-aspirii)) |
| Kamil Lyraczek       | 1 | ((201216-krypta-i-wyjec)) |
| Katra Igneus         | 1 | ((201210-pocalunek-aspirii)) |
| Martyn Hiwasser      | 1 | ((201216-krypta-i-wyjec)) |
| OA Zguba Tytanów     | 1 | ((201210-pocalunek-aspirii)) |
| OO Infernia          | 1 | ((201210-pocalunek-aspirii)) |