---
categories: profile
factions: 
owner: public
title: SC Karnaxian
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230530-ziarno-kuratorow-na-karnaxianie | KIA. Wpierw przejęty przez Kuratorów (Semla + lifesupport -> Alexandria + Kurator Sarkamair), potem wektor do przejęcia Serbiniusa, potem zestrzelony przez Serbiniusa jak nie dało się uratować załogi. | 0109-10-06 - 0109-10-07 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazy Termann     | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Fabian Korneliusz    | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Helmut Szczypacz     | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Klaudia Stryk        | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Kurator Sarkamair    | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Leo Mikirnik         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Martyn Hiwasser      | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| OO Serbinius         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |