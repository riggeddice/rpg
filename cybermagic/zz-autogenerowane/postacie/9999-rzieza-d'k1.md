---
categories: profile
factions: 
owner: public
title: Rzieza d'K1
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211114-admiral-gruby-wieprz         | gdy Iza robiła wywiad z Grubym Wieprzem zrobił interwencję i wypalił Wieprza, zmieniając wywiad w coś szkodliwego dla sprawy i grzebiąc reputację Izy. Ale pozwolił "niewinnym TAI" się ewakuować gdy Klaudia poprosiła. | 0112-02-15 - 0112-02-20 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adalbert Brześniak   | 1 | ((211114-admiral-gruby-wieprz)) |
| Izabela Zarantel     | 1 | ((211114-admiral-gruby-wieprz)) |
| Klaudia Stryk        | 1 | ((211114-admiral-gruby-wieprz)) |
| Rafael Galwarn       | 1 | ((211114-admiral-gruby-wieprz)) |
| Roman Panracz        | 1 | ((211114-admiral-gruby-wieprz)) |
| Sabina Kazitan       | 1 | ((211114-admiral-gruby-wieprz)) |