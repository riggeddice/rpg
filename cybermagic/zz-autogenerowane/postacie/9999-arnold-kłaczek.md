---
categories: profile
factions: 
owner: public
title: Arnold Kłaczek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211207-gdy-zabraknie-pradu-rekinom  | ludzki szef magitrowni Rekinów; najwyraźniej chce dostać wpierdol (dla ubezpieczenia?). Nie ma Atestów Z Aurum, więc tylko nadzoruje magitrownię. | 0111-08-30 - 0111-08-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arkadia Verlen       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Daniel Terienak      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Henryk Wkrąż         | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Hestia d'Rekiny      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Karolina Terienak    | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Lorena Gwozdnik      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Marysia Sowińska     | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Natalia Tessalon     | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Sensacjusz Diakon    | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Urszula Arienik      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |