---
categories: profile
factions: 
owner: public
title: Marianna Lemurczak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200923-magiczna-burza-w-raju        | tien, przyjaciółka Nikodema. Tak chce mu pomóc i wynieść go ponad Anastazję, że 2 lata temu użyła magii Krwi by pomóc mu złapać noktian i przejąć sentisieć. | 0111-02-21 - 0111-02-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazja Sowińska   | 1 | ((200923-magiczna-burza-w-raju)) |
| Arianna Verlen       | 1 | ((200923-magiczna-burza-w-raju)) |
| Dariusz Krantak      | 1 | ((200923-magiczna-burza-w-raju)) |
| Elena Verlen         | 1 | ((200923-magiczna-burza-w-raju)) |
| Eliza Ira            | 1 | ((200923-magiczna-burza-w-raju)) |
| Eustachy Korkoran    | 1 | ((200923-magiczna-burza-w-raju)) |
| Izabela Zarantel     | 1 | ((200923-magiczna-burza-w-raju)) |
| Klaudia Stryk        | 1 | ((200923-magiczna-burza-w-raju)) |
| Marian Fartel        | 1 | ((200923-magiczna-burza-w-raju)) |
| Nikodem Sowiński     | 1 | ((200923-magiczna-burza-w-raju)) |