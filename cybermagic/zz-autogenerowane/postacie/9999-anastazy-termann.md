---
categories: profile
factions: 
owner: public
title: Anastazy Termann
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230528-helmut-i-nieoczekiwana-awaria-lancera | advancer i żołnierz na Serbiniusie; narzeka i szuka ambitnych akcji. Trafił na Serbiniusa "za karę" miesiąc temu, chce się wykazać. Doprowadził do przeszukania przemytu (bez sensu) na jednostce cywilnej. | 0109-09-23 - 0109-09-26 |
| 230530-ziarno-kuratorow-na-karnaxianie | narzeka - jak zawsze - że nie ma nic wartościowego do walki. Jak pojawił się Kurator, na przyczółku Karnaxian okazał się być świetnym strzelcem. Zupełnie nie dba o ratowanie ludzi, chce walczyć. | 0109-10-06 - 0109-10-07 |
| 240102-zaloga-vishaera-przezyje     | ogólnie w bojowym nastroju i chce 'strzelać do potworów', nie umie myśleć o Skażeńcach jak o ludziach. Mimo kiepskich żartów osłania kapitana gdy ten poszedł na Talikazer. | 0109-10-26 - 0109-10-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Fabian Korneliusz    | 3 | ((230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 240102-zaloga-vishaera-przezyje)) |
| Helmut Szczypacz     | 3 | ((230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 240102-zaloga-vishaera-przezyje)) |
| Klaudia Stryk        | 3 | ((230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 240102-zaloga-vishaera-przezyje)) |
| Martyn Hiwasser      | 3 | ((230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 240102-zaloga-vishaera-przezyje)) |
| OO Serbinius         | 2 | ((230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Kurator Sarkamair    | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Leo Mikirnik         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Miranda Termann      | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Nikodem Dewiremicz   | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| SC Karnaxian         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Wolfgang Sępiarz     | 1 | ((240102-zaloga-vishaera-przezyje)) |