---
categories: profile
factions: 
owner: public
title: Lucia Veidril
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230723-crashlanding-furii-mataris   | złapana w głodne rośliny w Lesie Pustym, była zmuszona do ostrzeliwania się by się ochronić. To sprawiło, że została zaatakowana i pojmana przez przeciwników. Służy jako dywersja dla innych Furii. | 0081-06-15 - 0081-06-17 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230723-crashlanding-furii-mataris   | złapana przez salvagerów i sprzedana Wolnemu Uśmiechowi | 0081-06-17

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 1 | ((230723-crashlanding-furii-mataris)) |
| Ayna Marialin        | 1 | ((230723-crashlanding-furii-mataris)) |
| Ernest Kajrat        | 1 | ((230723-crashlanding-furii-mataris)) |
| Xavera Sirtas        | 1 | ((230723-crashlanding-furii-mataris)) |