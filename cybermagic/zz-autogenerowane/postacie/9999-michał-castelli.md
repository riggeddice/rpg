---
categories: profile
factions: 
owner: public
title: Michał Castelli
---

# {{ page.title }}


# Generated: 



## Fiszki


* Derek Powers PROFIL POWAŻANIA|BatmanBeyond | @ 240227-cykl-krwawej-przadki
* Wincenty Frak: Cyclonus|IDW | @ 240227-cykl-krwawej-przadki
* Lucyna Castelli: HotRod|G1 | @ 240227-cykl-krwawej-przadki

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240227-cykl-krwawej-przadki         | pod neuroobrożą Kalatrix wymordował swoją rodzinę, pod Prządką wymordował Kalatrix. Gdy był już wolny, przeznaczył swój majątek i majątek Calatrix by stać się filantropem. Pomaga dzieciom dotkniętym przez magię i Arkologii Terkelis. Negocjując z Różą, zawarł z nią pokój i jej też pomoże. | 0099-05-22 - 0099-05-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Wokniaczek      | 1 | ((240227-cykl-krwawej-przadki)) |
| Edward Kopoktris     | 1 | ((240227-cykl-krwawej-przadki)) |
| Janusz Umizarit      | 1 | ((240227-cykl-krwawej-przadki)) |
| Jola Kalatrix        | 1 | ((240227-cykl-krwawej-przadki)) |
| Lucyna Castelli      | 1 | ((240227-cykl-krwawej-przadki)) |
| Róża Kalatrix        | 1 | ((240227-cykl-krwawej-przadki)) |
| Wincenty Frak        | 1 | ((240227-cykl-krwawej-przadki)) |