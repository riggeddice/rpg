---
categories: profile
factions: 
owner: Dust
title: Mieszko Weiner
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "faction_1, faction_2"
* type: "PC"
* owner: "Dust"
* title: "Mieszko Weiner"


## Postać

### Koncept (1)
Cyber-nekromanta, kolekcjoner. Ekscentryczny naukowiec specjalizujący się w konwersji istot niedawno zmarłych do cybermechanicznych bytów, zachowując część ich jaźni i wspomnień. Ma bogatą kolekcję "lalek" i "zabawek", które są mu posłuszne. Posiada jednak moralny kręgosłup i nie tyka ludzi bez ich woli. Dorabia budując mechaniczne protezy. Lekko szalony.


### Otoczenie (1)
Twierdz i warsztat na kółkach - ogromna ciężarówka wypełniona sprzętem i kolekcją


### Misja (1)
Walczy o prawo do uprawiania sztuki. Pragnie rozszerzać kolację o coraz ciekawsze "eksponaty", co czasami bywa nibezpieczne. 


### Wada (-2)
Jego ekscentryczna natura powoduje, że ma problemy w kontaktach społecznych.


### Motywacje (1)

| Co chce by się działo? Co jest pożądane?                 | Co jest antytezą postaci? Co jest jej negacją?               |
|----------------------------------------------------------|--------------------------------------------------------------|
| Pozyskuj ciekawe okazy | Naraź cenne okazy ze swojej kolekcji |
| Eksponuj swoją pasję | Konwertuj człowieka bez jego woli |
| Pomóż człowiekowi w potrzebie | Ulegnij bałamuceniu moralistów |

### Zachowania (1)

| Jak zazwyczaj się zachowuje?                             | Pod wpływem jakich uczuć / w jakich okolicznościach to robi? |
|----------------------------------------------------------|--------------------------------------------------------------|
| pogodny i pełen pasji, próbuje zarazić nią innych | zazwyczaj, w sytuacjach typowych |
| troskliwy i czuły | wobec swoich tworów |
| Nerwowy i niespokojny | gdy zbyt długo rpzebywa z daleka od swojej kolekcji |
| Opryskliwy i pogardliwy | wobec krytyów |

### Umiejętności (4)

* zna się na anatomii i wie jak zastępować poszczególne jej elementy. Dozskonale zna się na mechanicznych i cybernetycznych urządzeniach - budowie, działaniu, sterowaniu. Steruje swoją kolekcją nawet bez użycia magii.

### Magia (2 LUB 4)

#### Gdy kontroluje energię

* Przekształca, bądź przenosi istoty żywe do cyber-mechanicznego ciała
* Twoży użyteczne narzędzia

#### Gdy traci kontrolę

* Echa emocjonalne

### Zasoby i otoczenie

#### Ogólnie (4)

* Ciężarówka - warsztat
* Nekromechaniczna część ciała (inna za każdym razem)

#### Powiązane frakcje

{{ page.factions }}

## Opis



# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180906-nikt-nie-spi-w-swoim-lozku   | pomógł naprawić Epirjon i został wykorzystany do pozbycia się Ateny ze stacji. Creepy. Odprawiał fałszywe egzorcyzmy i zajumał ducha. | 0109-09-09 - 0109-09-11 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180906-nikt-nie-spi-w-swoim-lozku   | inkarnował ducha Wąsacza (ojca Brygidy) w pluszowego misia i go zajumał do swojej kolekcji | 0109-09-11

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Atena Sowińska       | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Brygida Maczkowik    | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Grażyna Sirwąg       | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Pięknotka Diakon     | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Szczepan Mensic      | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Tadeusz Kruszawiecki | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |