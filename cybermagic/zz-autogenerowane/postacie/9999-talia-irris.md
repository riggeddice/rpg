---
categories: profile
factions: 
owner: public
title: Talia Irris
---

# {{ page.title }}


# Generated: 



## Fiszki


* (ENCAO:  0--0+ |lubi jak się jej schlebia;;Spontaniczna;;Przesądna| VALS: Security >> Self-direction| DRIVE: Życie wśród przyjaciół) | @ 230113-ros-adrienne-a-new-recruit
* savaranka: "słabość jednego z nas to słabość nas wszystkich" | @ 230113-ros-adrienne-a-new-recruit
* kadencja: nostalgiczna savaranka; | @ 230113-ros-adrienne-a-new-recruit
* "Nie bądź słabym ogniwem"; TRAKTOWANA JAK MASKOTKA | @ 230113-ros-adrienne-a-new-recruit
* SPEC: science / medical / space | @ 230113-ros-adrienne-a-new-recruit
* (ENCAO:  0++++ |lubi dzieci;;Przesądna;;Wierzy w duchy;;Nie jest zazdrosna| VALS: Security >> Self-direction| DRIVE: Życie wśród przyjaciół) | @ 230117-ros-wiertloplyw-i-tai-mirris
* core wound: nigdy już nie zobaczę swoich dzieci i swojej rodziny; nienawidzę być sama i samotna | @ 230117-ros-wiertloplyw-i-tai-mirris
* core lie: ten statek to moja ostatnia szansa na namiastkę rodziny | @ 230117-ros-wiertloplyw-i-tai-mirris
* styl: nostalgiczna, melancholijna, cicha savaranka lubiąca towarzystwo ludzi | @ 230117-ros-wiertloplyw-i-tai-mirris
* SPEC: space / void operations | @ 230117-ros-wiertloplyw-i-tai-mirris

### Wątki


ratownicy-ostatniej-szansy

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230113-ros-adrienne-a-new-recruit   | without speaking much, communicates well with the team and shows what should be done and how. Leads Adrianna through dangerous things by her actions. Secures Team from the shuttle as primary pilot. | 0083-12-07 - 0083-12-10 |
| 230117-ros-wiertloplyw-i-tai-mirris | mistrzyni spacerów kosmicznych; wykryła minę w okolicach TAI. Mało mówi, ale przeprowadza ludzi po nanowłóknie z Wiertłopływu do Błyskawicy. | 0084-02-22 - 0084-02-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Napoleon Myszogłów   | 2 | ((230113-ros-adrienne-a-new-recruit; 230117-ros-wiertloplyw-i-tai-mirris)) |
| Niferus Sentriak     | 2 | ((230113-ros-adrienne-a-new-recruit; 230117-ros-wiertloplyw-i-tai-mirris)) |
| Tristan Andrait      | 2 | ((230113-ros-adrienne-a-new-recruit; 230117-ros-wiertloplyw-i-tai-mirris)) |
| Adrianna Kastir      | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Aster Sarvinn        | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Eleonora Alintirias  | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Hel Otereien         | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Kasia Karmnik        | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Lucas Septrien       | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Łucja Neiser         | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Łucja Nirimis        | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| OR Błyskawica        | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| SC Wiertłopływ       | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| TAI Mirris d'Paravilius | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |