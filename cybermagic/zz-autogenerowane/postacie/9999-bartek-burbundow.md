---
categories: profile
factions: 
owner: public
title: Bartek Burbundow
---

# {{ page.title }}


# Generated: 



## Fiszki


* górnik w przestrzeni Blakvela | @ 231027-planetoida-bogatsza-niz-byc-powinna
* OCEAN: A+O+ |Mało energii;; Kreatywny, tworzy wiecznie coś nowego;; Pomocny, starający się wesprzeć innych| VALS: Face, Tradition| DRIVE: Odbudowa i odnowa | @ 231027-planetoida-bogatsza-niz-byc-powinna

### Wątki


planetoidy-kazimierza
sekret-mirandy-ceres

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231027-planetoida-bogatsza-niz-byc-powinna | nie jest fanem Blakvelowców, nie uważa, że powinno się tyle się im płacić za ochronę. Zwolennik czystości - bardzo obrzydzony tym, że Królową trzeba karmić organiczną papką. Jednocześnie - uczciwy górnik. | 0108-06-29 - 0108-07-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Gotard Kicjusz       | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Grzegorz Fabutownik  | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Juliusz Cieślawok    | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Kazimierz Zamglis    | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Łucjan Torwold       | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Mikołaj Resztkowiec  | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Miranda Ceres        | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Prokop Umarkon       | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| SC Królowa Przygód   | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Seweryn Grzęźlik     | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |