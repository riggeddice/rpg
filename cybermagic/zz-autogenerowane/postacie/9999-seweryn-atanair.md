---
categories: profile
factions: 
owner: public
title: Seweryn Atanair
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210901-stabilizacja-bramy-eterycznej | admirał Noctis dowodzący ON Spatium Gelida. Myślał że połączył się z Oliwią a to była Arianna. Dla niego minęło kilka godzin... | 0112-02-09 - 0112-02-12 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Elena Verlen         | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Eustachy Korkoran    | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Flawia Blakenbauer   | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Gilbert Bloch        | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Janus Krzak          | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Klaudia Stryk        | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Leona Astrienko      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Martyn Hiwasser      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Medea Sowińska       | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Netrahina         | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |