---
categories: profile
factions: 
owner: public
title: Janvir Krassus
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (O+ C+) | "Przez współpracę i innowacje budujemy lepszą przyszłość." (Locke) | VALS: (Tradition, Security) | DRIVE: "Przekształcenie Szernief w zaawansowaną kolonię." | @ 231119-tajemnicze-tunele-sebirialis
* on jest odpowiedzialny za współpracę z NavirMed | @ 231119-tajemnicze-tunele-sebirialis

### Wątki


agencja-lux-umbrarum
problemy-con-szernief

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231119-tajemnicze-tunele-sebirialis | elegancki radny, to on stoi za otwarciem Sebiralis dla różnych firm i agencji, w formie ukrytej (przez co zrezygnowała Felina). Chce dobra dla stacji, acz niekoniecznie dla aktualnej populacji stacji. Dba o stację a nie o osoby na niej. | 0104-11-20 - 0104-11-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aniela Kafantelas    | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Eryk Kawanicz        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Felina Amatanir      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Ignatius Sozyliw     | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Kalista Surilik      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Klasa Dyplomata      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Klasa Sabotażysta    | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Larkus Talvinir      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Leon Hurmniow        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Marta Sarilit        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| OLU Luminarius       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |