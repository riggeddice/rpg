---
categories: profile
factions: 
owner: public
title: Estril Cavalis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240117-dla-swych-marzen-warto       | zarażony przez Pierścień, próbuje rozpaczliwie osłonić Aerinę (i zamknął ją w pokoju). Współpracuje z Agencją, bo to jedyny sposób by ratować córkę. Wyleczony przez Luminariusa. | 0106-11-04 - 0106-11-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aerina Cavalis       | 1 | ((240117-dla-swych-marzen-warto)) |
| Artur Tavit          | 1 | ((240117-dla-swych-marzen-warto)) |
| Felina Amatanir      | 1 | ((240117-dla-swych-marzen-warto)) |
| Kalista Surilik      | 1 | ((240117-dla-swych-marzen-warto)) |
| Klasa Hacker         | 1 | ((240117-dla-swych-marzen-warto)) |
| Klasa Oficer Naukowy | 1 | ((240117-dla-swych-marzen-warto)) |
| Klasa Sabotażysta    | 1 | ((240117-dla-swych-marzen-warto)) |
| Larkus Talvinir      | 1 | ((240117-dla-swych-marzen-warto)) |
| Maia Sakiran         | 1 | ((240117-dla-swych-marzen-warto)) |
| Mawir Hong           | 1 | ((240117-dla-swych-marzen-warto)) |
| Vanessa d'Cavalis    | 1 | ((240117-dla-swych-marzen-warto)) |