---
categories: profile
factions: 
owner: public
title: Mirela Orion
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "orbiter, kirasjerka orbitera, orbiter neomil"
* owner: "public"
* title: "Mirela Orion"


## Kim jest

### Koncept

Emulatorka Kirasjerów. Kiedyś: Emilia, ale przez działania Pięknotki odzyskała przejawy świadomości. Mirela jest z charakteru bardziej paladynką, dość momentami porywczą - ale skupioną na ochronie wszystkich i wszystkiego. Jak na Emulatorkę, łatwo się zaprzyjaźnia. Nieco nieśmiała; nie wie jak działać poza programowaniem i w tym świecie. Uważa się za narzędzie, nie za osobę.

### Motto

"W kosmosie jest mrok i zło. Potrzebujesz takich jak ja, by móc żyć w świetle i w cieple. Tacy jak Ty mnie wyrzucą, jak moja misja będzie skończona - i dobrze. Zużyte narzędzia trzeba wymienić."

### Co się rzuca w oczy

* Mirela nie pozwoli, by komuś stała się krzywda. Będzie chronić życia i zdrowie zgodnie z parametrami misji, nawet naginając rozkazy.
* Dość porywcza jak na Emulatorkę, ale nie przeszkadza jej to w działaniu. Misja > wszystko.
* Uważa się nie za osobę a za narzędzie. Uważa, że tak powinno być - jest zbyt niebezpieczna.
* Czuje ogromne pokrewieństwo z ludźmi i ludzką rasą, mimo, że jest Emulatorką. Ma pozytywne spojrzenie, nieważne, jak na nią patrzą.
* Dość sympatyczna, choć jest całkowicie nieżyciowa i nie rozumie tej rzeczywistości.
* Gdy jest zagubiona, jest lekko nieśmiała. Dodatkowo porusza się z taką ostrożnością jakby nie chciała zrobić krzywdy niewspomaganym.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Gdy przekraczała jako nastolatka granice swojej mocy, doprowadziła do śmierci swojej rodziny. To sprawiło, że sama zgłosiła się do projektu 'Emulator'.
* Gdy poznała prawdę o Emulatorkach i jak to wszystko działa, stanęła po stronie Kirasjerów by chronić zespół przed Szaloną Emulatorką.
* Przekroczyła programowanie by chronić ludzi - zawalając misję. Wydawało się, że dla Emulatorki takie działanie jest niemożliwe.

### Z czym przyjdą do niej o pomoc? (3)

* 

### Jaką ma nieuczciwą przewagę? (3)

* Emulatorka - potrafi przełączyć swój zestaw umiejętności na taki, jaki ma załadowany. Ma slot na 3 zestawy.
* Homo Superior - świetnie uzbrojona, zdolna do działania w próżni, otoczona polem magicznym. Katai.
* Szczerze wierzy w Kirasjerów, Orbiter NeoMil a zwłaszcza w Damiana Oriona.

### Charakterystyczne zasoby (3)

* COŚ: 
* KTOŚ: 
* WIEM: 
* OPINIA: 

### Typowe sytuacje z którymi sobie nie radzi (-3)

* 

## Inne

### Wygląd

.

### Coś Więcej

* Skażona przez Cienia i wysoce uwolniona. Do tej pory Kirasjerzy nie znaleźli sposobu, jak ją naprawić.

### Endgame

* ?


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190503-bardzo-nieudane-porwania     | Emulator Kirasjerów; uwolniona przez Cienia. Pokonana z zaskoczenia przez Pięknotkę, skończyła w lochu w Pustogorze przed zwróceniem jej Kirasjerom. | 0110-04-18 - 0110-04-20 |
| 190721-kirasjerka-najgorszym-detektywem | emulator Kirasjerów; była na wakacjach. Szukała gdzie zniknęła Aida (przyjaciółka). Ślady prowadziły do Pięknotki więc zaatakowała. Straciła Calibris, uszkadzając Cienia. | 0110-06-10 - 0110-06-11 |
| 190724-odzyskana-agentka-orbitera   | ściągnęła dyskretnie Damiana, znalazła przemytników oraz dowiedziała się kim jest naprawdę Aida. By ratować Aidę stoczyła walkę z Moktarem - i przegrała. | 0110-06-12 - 0110-06-15 |
| 190726-bardzo-niebezpieczne-skladowisko | kontakt Pięknotki z Orbiterem; przekonała szefostwo, że warto odciąć Wolny Uśmiech od kanałów przerzutowych z Cieniaszczytu. | 0110-06-26 - 0110-06-28 |
| 200729-nocna-krypta-i-emulatorka    | nieskończenie oddana Damianowi Emulatorka, wzięła Czarną Aleksandrię by wezwać Kryptę. Nie była w stanie pokonać Laury d'Esuriit. Misja przegrana. | 0111-01-14 - 0111-01-20 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190503-bardzo-nieudane-porwania     | Emulatorka uwolniona przez Cienia; nie jest już lalką w rękach kontrolera. Trzeci wolny Emulator. | 0110-04-20

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 4 | ((190503-bardzo-nieudane-porwania; 190721-kirasjerka-najgorszym-detektywem; 190724-odzyskana-agentka-orbitera; 190726-bardzo-niebezpieczne-skladowisko)) |
| Aida Serenit         | 2 | ((190721-kirasjerka-najgorszym-detektywem; 190724-odzyskana-agentka-orbitera)) |
| Damian Orion         | 2 | ((190503-bardzo-nieudane-porwania; 200729-nocna-krypta-i-emulatorka)) |
| Adam Szarjan         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| AK Nocna Krypta      | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Amanda Kajrat        | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Antoni Kramer        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Arianna Verlen       | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Cezary Alentik       | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Ernest Kajrat        | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Eustachy Korkoran    | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Gabriel Ursus        | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Julia Morwisz        | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Karla Mrozik         | 1 | ((190503-bardzo-nieudane-porwania)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Klaudia Stryk        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Leona Astrienko      | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Minerwa Metalia      | 1 | ((190503-bardzo-nieudane-porwania)) |
| Mirela Niecień       | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Moktar Gradon        | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Nikola Kirys         | 1 | ((190503-bardzo-nieudane-porwania)) |
| Olga Myszeczka       | 1 | ((190721-kirasjerka-najgorszym-detektywem)) |
| OO Castigator        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Roland Grzymość      | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Wiktor Satarail      | 1 | ((190721-kirasjerka-najgorszym-detektywem)) |