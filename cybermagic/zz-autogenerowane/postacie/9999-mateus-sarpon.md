---
categories: profile
factions: 
owner: public
title: Mateus Sarpon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220126-keldan-voss-kolonia-saitaera | drakolita; drugi po Kormonowie; wyjaśnił Ariannie czemu kolonia się zbuntowała - żyją z woli Saitaera a przez generatory Memoriam ich ludzie umierają. Plus pallidanie na nich polują. | 0112-05-24 - 0112-05-27 |
| 220202-sekrety-keldan-voss          | zdecydował się na współpracę z pallidanami - przejmie korporację w imię Saitaera. Opowiedział Ariannie sporo odnośnie tego co się tu działo - martwa BIA zintegrowana z ochotnikiem, historia... | 0112-05-29 - 0112-05-31 |
| 220216-polityka-rujnuje-pallide-voss | pasywne wsparcie dla Arianny. Chce pomóc by wszyscy zostali uratowani i przeniesieni tam gdzie mają być. | 0112-06-01 - 0112-06-03 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220309-upadek-eleny                 | absolutnie przerażony istnieniem Eleny Verlen. Czymkolwiek jest, w jego oczach jest arcybluźnierstwem. | 0112-06-14

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Annika Pradis        | 3 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss)) |
| Arianna Verlen       | 3 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss)) |
| Elena Verlen         | 3 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss)) |
| Eustachy Korkoran    | 3 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss)) |
| Klaudia Stryk        | 3 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss)) |
| SP Pallida Voss      | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Szczepan Kaltaben    | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Kormonow Voss        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| OO Kastor            | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Raoul Lavanis        | 1 | ((220216-polityka-rujnuje-pallide-voss)) |
| Zygfryd Maus         | 1 | ((220126-keldan-voss-kolonia-saitaera)) |