---
categories: profile
factions: 
owner: public
title: SC Fecundatis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210813-szmuglowanie-antonelli       | statek Brightona transportujący skorpioidy do celów medycznych; jest długim, sekwencyjnym pojazdem z uchwytami przypominającymi żebra czy wypustki na dysku gdzie można podczepić kontenery - te kontenery są przymocowane magnetycznie. Bardzo słabo oświetlony i ogrzany. Zanim stał się przemytniczym transportowcem był foodtruckiem. Zaopatrzeniowym wewnątrzsystemowym. Duży statek, ale niewiele osób. 12 osób załogi oficjalnie. Silnie zrobotyzowany. | 0109-02-11 - 0109-02-23 |
| 210818-siostrzenica-morlana         | współpracuje z Ofelią Morlan i Jolantą Sowińską przy ratowaniu arystokratów Eterni przed Natanielem Morlanem. | 0112-01-20 - 0112-01-24 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210820-fecundatis-w-domenie-barana  | zapas skorpioidów spadł do zera. Mamy tylko królową i jednego malutkiego skorpioida, uratowanego przez Antonellę (oczywiście). | 0109-03-09

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Tomasz Sowiński      | 2 | ((210813-szmuglowanie-antonelli; 210818-siostrzenica-morlana)) |
| Antonella Temaris    | 1 | ((210813-szmuglowanie-antonelli)) |
| Antoni Kramer        | 1 | ((210818-siostrzenica-morlana)) |
| Arianna Verlen       | 1 | ((210818-siostrzenica-morlana)) |
| Bruno Baran          | 1 | ((210813-szmuglowanie-antonelli)) |
| Cień Brighton        | 1 | ((210813-szmuglowanie-antonelli)) |
| Eustachy Korkoran    | 1 | ((210818-siostrzenica-morlana)) |
| Flawia Blakenbauer   | 1 | ((210813-szmuglowanie-antonelli)) |
| Jolanta Sowińska     | 1 | ((210813-szmuglowanie-antonelli)) |
| Klaudia Stryk        | 1 | ((210818-siostrzenica-morlana)) |
| Leona Astrienko      | 1 | ((210818-siostrzenica-morlana)) |
| Lucjusz Blakenbauer  | 1 | ((210813-szmuglowanie-antonelli)) |
| Maria Naavas         | 1 | ((210818-siostrzenica-morlana)) |
| Nataniel Morlan      | 1 | ((210818-siostrzenica-morlana)) |
| Ofelia Morlan        | 1 | ((210818-siostrzenica-morlana)) |
| Olgierd Drongon      | 1 | ((210818-siostrzenica-morlana)) |
| OO Netrahina         | 1 | ((210818-siostrzenica-morlana)) |
| OO Żelazko           | 1 | ((210818-siostrzenica-morlana)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |