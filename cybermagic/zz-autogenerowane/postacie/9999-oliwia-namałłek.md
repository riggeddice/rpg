---
categories: profile
factions: 
owner: public
title: Oliwia Namałłek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190505-szczur-ktory-chroni          | ofiara mafii i siostra Krystiana, uzależniona od kralotycznego proszku. Trafiła do Wiktora Sataraila na leczenie. W sumie - przyczyna wszystkich problemów. | 0110-04-22 - 0110-04-24 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190505-szczur-ktory-chroni          | otrzymała pomoc u Wiktora Sataraila; wyleczy ją z uzależnienia po jakmiś czasie | 0110-04-24

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adela Kirys          | 1 | ((190505-szczur-ktory-chroni)) |
| Alan Bartozol        | 1 | ((190505-szczur-ktory-chroni)) |
| Ernest Kajrat        | 1 | ((190505-szczur-ktory-chroni)) |
| Kasjan Czerwoczłek   | 1 | ((190505-szczur-ktory-chroni)) |
| Krystian Namałłek    | 1 | ((190505-szczur-ktory-chroni)) |
| Lucjusz Blakenbauer  | 1 | ((190505-szczur-ktory-chroni)) |
| Pięknotka Diakon     | 1 | ((190505-szczur-ktory-chroni)) |
| Wiktor Satarail      | 1 | ((190505-szczur-ktory-chroni)) |