---
categories: profile
factions: 
owner: public
title: Wirgot Samszar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230704-maja-chciala-byc-dorosla     | silny mag który zaprosił Jonatana i Vanessę do Karmazynowego Świtu. Chce ich zachwycić hedonizmem. Potrzebował pomocy Jonatana by coś osiągnąć, nawet, jeśli nie był fanem działań Lemurczaków. Chronił ich lojalnie przed Albertem aż się dowiedział co się stało. | 0095-08-29 - 0095-09-01 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Albert Samszar       | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Amanda Kajrat        | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Jonatan Lemurczak    | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Karolinus Samszar    | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Maja Samszar         | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Nadia Obiris         | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Vanessa Lemurczak    | 1 | ((230704-maja-chciala-byc-dorosla)) |