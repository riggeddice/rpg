---
categories: profile
factions: 
owner: public
title: Kurator Sarkamair
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230530-ziarno-kuratorow-na-karnaxianie | rozłożył Ziarna Kuratorów (zbiór fabrykatorów) i przejął Karnaxian, gdzie się zrekonstruował. Zaatakował też Serbiniusa, ale został odparty. Magia Klaudii PRAWIE dała mu skille i możliwości Malictrix, ale jego plan został odparty i zestrzelony. Okazał się być niesamowicie niebezpiecznym przeciwnikiem - dobrze planuje, atakuje z zaskoczenia i lubi porywać ludzi, adaptować ich jednostki i odlecieć do bazy. | 0109-10-06 - 0109-10-07 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazy Termann     | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Fabian Korneliusz    | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Helmut Szczypacz     | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Klaudia Stryk        | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Leo Mikirnik         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Martyn Hiwasser      | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| OO Serbinius         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| SC Karnaxian         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |