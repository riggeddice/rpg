---
categories: profile
factions: 
owner: public
title: Robinson Porzecznik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230418-zywy-artefakt-w-gwiazdoczach | technomanta i artefaktor, 31 lat; współpracuje z Aurum w sprawie przemytu artefaktów na teren Szczelińca; zajmował się swoimi sprawami i dorwała go tien Elena Samszar z pytaniami. Jako, że wcześniej widział frywolnie ubraną Samszarkę, wolał nie odpowiadać i nie wpadać między dwie młode tienki. Pomógł jak mógł - odkrył żywy artefakt magią. Potem ciągnął dwa wielkie niuchacze na smyczy, bo przecież 'dama nie będzie ciągana przez płaszczki'. | 0094-10-04 - 0094-10-06 |
| 201020-przygoda-randka-i-porwanie   | ojciec Triany; córka odwracała jego uwagę od alarmu prośbą o dopilnowanie ciasta francuskiego. Ojciec z dumą pokazał córce co umie (a młodzi się wkradli). | 0110-10-25 - 0110-10-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adelaida Samszar     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Antonina Blakenbauer | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Daniel Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Elena Samszar        | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Gabriel Ursus        | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Henryk Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Joachim Pulkmocz     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Kacper Bankierz      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Laurencjusz Sorbian  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Liliana Bankierz     | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Neidria Lazvarin     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Robert Pakiszon      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Sara Mazirin         | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Tomasz Tukan         | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Triana Porzecznik    | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Tymon Grubosz        | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Urszula Miłkowicz    | 1 | ((201020-przygoda-randka-i-porwanie)) |