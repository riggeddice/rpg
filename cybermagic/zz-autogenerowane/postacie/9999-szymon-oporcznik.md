---
categories: profile
factions: 
owner: public
title: Szymon Oporcznik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190331-kurz-po-ataienne             | mag Orbitera, który nie jest wrogi Pięknotce. Opiekun Ataienne. Chciał zabrać ją na koncert; uważa siebie i Pięknotkę za patriotów. | 0110-03-07 - 0110-03-09 |
| 190402-eksperymentalny-power-suit   | z ramienia Orbiter Aurora przekazał Pięknotce Cień - superniebezpieczny ixioński żywy servar symbiotyczny i zrobił co mógł, by Pięknotka mogła go przejąć | 0110-03-15 - 0110-03-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ataienne             | 2 | ((190331-kurz-po-ataienne; 190402-eksperymentalny-power-suit)) |
| Pięknotka Diakon     | 2 | ((190331-kurz-po-ataienne; 190402-eksperymentalny-power-suit)) |
| Alan Bartozol        | 1 | ((190331-kurz-po-ataienne)) |
| Diana Tevalier       | 1 | ((190331-kurz-po-ataienne)) |
| Erwin Galilien       | 1 | ((190402-eksperymentalny-power-suit)) |
| Kaja Selerek         | 1 | ((190402-eksperymentalny-power-suit)) |
| Minerwa Metalia      | 1 | ((190402-eksperymentalny-power-suit)) |
| Romeo Węglas         | 1 | ((190331-kurz-po-ataienne)) |