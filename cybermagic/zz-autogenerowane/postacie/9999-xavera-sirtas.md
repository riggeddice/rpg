---
categories: profile
factions: 
owner: public
title: Xavera Sirtas
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (C+A-): żywy ogień, jej dusza jest pełna pasji, a jej słowa ostrzem. "Kto siedzi cicho, tego spotyka wszystko co najgorsze" | @ 230723-crashlanding-furii-mataris
* VALS: (Security, Tradition): wszystko odnosi do tego co już widziała i co było, działa dość tradycyjnie. "Wszystko już było i się stało" | @ 230723-crashlanding-furii-mataris
* Core Wound - Lie: "Przeze to, że dużo mówiłam i szperałam, moi rodzice się rozwiedli" - "Będę walczyć i ognistą walką udowodnię, że warto być razem!" | @ 230723-crashlanding-furii-mataris
* Styl: Zawsze gotowa do walki. Agresywna i asertywna. Dobrze wybiera walki. Jej upór często wpędza ją w kłopoty. | @ 230723-crashlanding-furii-mataris

### Wątki


pronoktianska-mafia-kajrata
furia-mataris-agentka-mafii

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230723-crashlanding-furii-mataris   | fierce and loyal; najlepsza wojowniczka Furii Mataris w okolicy. Ma dość agresywne plany - chce się zakraść, ustrzelić itp. Ale daje się przekonać, że to nie jest najlepszy pomysł. Opiekuje się Ayną. | 0081-06-15 - 0081-06-17 |
| 230729-furia-poluje-na-furie        | najlepsza w walce z trzech Furii; wyciągnęła Amandę z gniazda jaszczuroskrzydeł podpalając je, oddała Amandzie część energii i granatem uszkodziła servar wroga. Gdy Aynie groziło porwanie, zderzyła awianem w porywającą Aynę Ralenę by przechwycić Aynę i wiać | 0081-06-17 - 0081-06-18 |
| 230730-skazone-schronienie-w-fortecy | ranna, ale przez zaciśnięte zęby prze do przodu. Zauważyła, że ludzie na obszarze Symlotosu nie zachowują się naturalnie, coś ich zmieniło. Skończyła ciężko ranna i w sztucznej śpiączce. | 0081-06-18 - 0081-06-21 |
| 230906-operacja-mag-dla-symlotosu   | wysłana przez Kajrata do Zaczęstwa; żartownisia i flirciara, dokucza Amandzie. Szybko wymanewrowała gąsienicostwora i wymyśliła plotkę o orgiach Furii. | 0081-06-26 - 0081-06-28 |
| 230913-operacja-spotkac-sie-z-dmitrim | ściągnęła żukowca i dała znać 'Dmitriemu' gdzie mniej więcej są; gdy już polowały na nie Lancery to ściągnęła potwora odpowiednimi dźwiękami. Gdy napotkały Dmitriego, ona była centrum dyplomacji ;-). | 0081-06-28 - 0081-06-30 |
| 230920-legenda-o-noktianskiej-mafii | gdy nie udało się cicho ukraść ścigacza, przechwyciła i złamała Patryka. Korzystając, że łowcy dziewczyn skupiają się na Amandzie, skrzywdziła faceta i się oderwały. Dotarła do Zaczęstwa. | 0081-07-10 - 0081-07-13 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230729-furia-poluje-na-furie        | ranna acz funkcjonalna i zatruta przez straszne korzenie w okolicach Enklaw; ma personal armour i sprzęt brawlera | 0081-06-18
| 230730-skazone-schronienie-w-fortecy | bardzo ciężko ranna, wprowadzona w śpiączkę przez medyków Kajrata; za dwa miesiące będzie w stanie operacyjnym przy aktualnym poziomie technologicznym w obszarze Enklaw | 0081-06-21

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 6 | ((230723-crashlanding-furii-mataris; 230729-furia-poluje-na-furie; 230730-skazone-schronienie-w-fortecy; 230906-operacja-mag-dla-symlotosu; 230913-operacja-spotkac-sie-z-dmitrim; 230920-legenda-o-noktianskiej-mafii)) |
| Ernest Kajrat        | 4 | ((230723-crashlanding-furii-mataris; 230729-furia-poluje-na-furie; 230730-skazone-schronienie-w-fortecy; 230906-operacja-mag-dla-symlotosu)) |
| Ayna Marialin        | 3 | ((230723-crashlanding-furii-mataris; 230729-furia-poluje-na-furie; 230730-skazone-schronienie-w-fortecy)) |
| Dmitri Karpov        | 2 | ((230913-operacja-spotkac-sie-z-dmitrim; 230920-legenda-o-noktianskiej-mafii)) |
| Petra Karpov         | 2 | ((230913-operacja-spotkac-sie-z-dmitrim; 230920-legenda-o-noktianskiej-mafii)) |
| Arnulf Poważny       | 1 | ((230913-operacja-spotkac-sie-z-dmitrim)) |
| Bogdan Gwiazdocisz   | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Caelia Calaris       | 1 | ((230906-operacja-mag-dla-symlotosu)) |
| Dragan Halatis       | 1 | ((230906-operacja-mag-dla-symlotosu)) |
| Irelia Kairanolis    | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Isaura Velaska       | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Leira Euridis        | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Leon Varkas          | 1 | ((230906-operacja-mag-dla-symlotosu)) |
| Lestral Kirmanik     | 1 | ((230906-operacja-mag-dla-symlotosu)) |
| Lucia Veidril        | 1 | ((230723-crashlanding-furii-mataris)) |
| Patryk Majwuron      | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Raab Navan           | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Ralena Karimin       | 1 | ((230729-furia-poluje-na-furie)) |