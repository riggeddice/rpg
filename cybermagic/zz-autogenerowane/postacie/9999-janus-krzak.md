---
categories: profile
factions: 
owner: public
title: Janus Krzak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210707-po-drugiej-stronie-bramy     | ekspert od Eteru sponsorowany przez Sowińskich, chciał lecieć na konferencję (poza Sektorem Astoriańskim). Pomógł Zespołowi rozpracować co się mogło stać z Klaudią. | 0111-05-11 - 0111-05-13 |
| 210714-baza-zona-tres               | de facto ostrzegł, że powrót Bramą jest piekielnie niebezpieczny. Arianna czuje smutek. | 0111-05-14 - 0111-05-16 |
| 210721-pierwsza-bia-mag             | jedyny astoriański naukowiec ever jaki tłumaczył Bii zasady eteru i jak należy używać magii. Srs. | 0111-05-17 - 0111-05-19 |
| 210728-w-cieniu-nocnej-krypty       | z Eustachym zmontował sondę która wysłała sygnał udający, że w rzeczywistości Finis Vitae opanował bazę Sarairen. Jego wiedza o Eterze uprawdopodobniła ów sygnał. | 0111-05-20 - 0111-06-06 |
| 210901-stabilizacja-bramy-eterycznej | duże wsparcie Klaudii w radzeniu sobie ze zrozumieniem tego co się stało z Bramą Kariańską i zdobywanie informacji o ON Catarinie. | 0112-02-09 - 0112-02-12 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 5 | ((210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210901-stabilizacja-bramy-eterycznej)) |
| Eustachy Korkoran    | 5 | ((210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210901-stabilizacja-bramy-eterycznej)) |
| Martyn Hiwasser      | 4 | ((210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej)) |
| Elena Verlen         | 3 | ((210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210901-stabilizacja-bramy-eterycznej)) |
| Klaudia Stryk        | 3 | ((210707-po-drugiej-stronie-bramy; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej)) |
| Romana Arnatin       | 3 | ((210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| Ulisses Kalidon      | 2 | ((210714-baza-zona-tres; 210728-w-cieniu-nocnej-krypty)) |
| AK Nocna Krypta      | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Atrius Kurunen       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| BIA Solitaria d'Zona Tres | 1 | ((210721-pierwsza-bia-mag)) |
| BIA XXX d'Zona Tres  | 1 | ((210714-baza-zona-tres)) |
| Finis Vitae          | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Flawia Blakenbauer   | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Gerard Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Gilbert Bloch        | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Helena Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Leona Astrienko      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Medea Sowińska       | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Oliwia Karelan       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Netrahina         | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Raoul Lavanis        | 1 | ((210707-po-drugiej-stronie-bramy)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Tal Marczak          | 1 | ((210707-po-drugiej-stronie-bramy)) |