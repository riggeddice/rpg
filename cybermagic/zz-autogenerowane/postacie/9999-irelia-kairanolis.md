---
categories: profile
factions: 
owner: public
title: Irelia Kairanolis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230920-legenda-o-noktianskiej-mafii | kiedyś Furia; miała niesamowicie silną wolę i była świetna w pułapkach, przyczajaniu się, oszustwach. Teraz 'Irilea', maskotka zespołu. Cicha, sympatyczna... ale podpuszcza i psychotyczna. 'Irilea' świetnie walczy i sama jest w stanie mając dobry teren walczyć z Amandą i Xaverą. Nie dba o swoich sojuszników - Grzymościowców. | 0081-07-10 - 0081-07-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Bogdan Gwiazdocisz   | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Dmitri Karpov        | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Patryk Majwuron      | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Petra Karpov         | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Xavera Sirtas        | 1 | ((230920-legenda-o-noktianskiej-mafii)) |