---
categories: profile
factions: 
owner: public
title: Andrzej Gwozdnik
---

# {{ page.title }}


# Generated: 



## Fiszki


* pierwszy oficer Athamarein, świetny artylerzysta i doskonały w pojedynkach, doskonale ubrany | @ 231018-anomalne-awarie-athamarein
* OCEAN: E-A- | Surowy, wymagający;; Nie socjalizuje się;; Niezwykle skuteczny acz brutalny| VALS: Achievement, Power| DRIVE: Tajemnica Wszechświata | @ 231018-anomalne-awarie-athamarein

### Wątki


legenda-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231018-anomalne-awarie-athamarein   | pierwszy oficer, osoba 'bezduszna'. Arogancki; uważa, że byłby lepszym kapitanem. Całkowicie omija aspekt ludzki - po ostatniej katastrofie, tym bardziej pewny że to jedyna droga. | 0111-12-22 - 0111-12-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alicja Szadawir      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Arianna Verlen       | 1 | ((231018-anomalne-awarie-athamarein)) |
| Błażej Sowiński      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Elena Verlen         | 1 | ((231018-anomalne-awarie-athamarein)) |
| Eustachy Korkoran    | 1 | ((231018-anomalne-awarie-athamarein)) |
| Hiacynt Samszar      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Jakub Oroginiec      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Kajetan Kircznik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Klaudia Stryk        | 1 | ((231018-anomalne-awarie-athamarein)) |
| Leona Astrienko      | 1 | ((231018-anomalne-awarie-athamarein)) |
| OO Athamarein        | 1 | ((231018-anomalne-awarie-athamarein)) |
| Remigiusz Alkarenit  | 1 | ((231018-anomalne-awarie-athamarein)) |
| Uśmiechniczka Konstrukcjonistka Aurora Diakon | 1 | ((231018-anomalne-awarie-athamarein)) |