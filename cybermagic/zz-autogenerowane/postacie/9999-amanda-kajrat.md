---
categories: profile
factions: 
owner: public
title: Amanda Kajrat
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211120-glizda-ktora-leczy           | przekradła się koło czterech Lancerów sterowanych przez Elainkę i zestrzeliła Rolanda Sowińskiego jak przemawiał na podwyższeniu, zmieniając go w klauna. Zwykłe ćwiczenia. | 0108-04-03 - 0108-04-14 |
| 190714-kult-choroba-esuriit         | traktowana przez Kajrata jako córka, chce uratować "ojca". Świetnie wyczuwa Esuriit; wskazała Pięknotce Czółenko jako źródło problemów. | 0110-06-03 - 0110-06-05 |
| 190726-bardzo-niebezpieczne-skladowisko | skłonna do skrzywdzenia grupy terminusów-rekrutów by rozpalić wojnę Pustogor - Wolny Uśmiech. Nie chce wojny. Nie walczyła z Pięknotką, trochę podpowiedziała. Świetnie się chowa w stealth suit. | 0110-06-22 - 0110-06-24 |
| 190804-niespodziewany-wplyw-aidy    | dostarczyła Pięknotce dowodów odnośnie narkotyku Grzymościa by zredukować gorącą wojnę. Niestety, wpadła w ręce Ossidii i Saitaera - ma być przynętą na Kajrata. | 0110-06-26 - 0110-06-28 |
| 200311-wygrany-kontrakt             | Skażona energiami ixiońskimi przez Saitaera, wpadła w ręce Grzymościa, acz poniszczyła mu mafiozów solidnie | 0110-07-16 - 0110-07-19 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ernest Kajrat        | 4 | ((190714-kult-choroba-esuriit; 190726-bardzo-niebezpieczne-skladowisko; 200311-wygrany-kontrakt; 211120-glizda-ktora-leczy)) |
| Pięknotka Diakon     | 3 | ((190714-kult-choroba-esuriit; 190726-bardzo-niebezpieczne-skladowisko; 190804-niespodziewany-wplyw-aidy)) |
| Gabriel Ursus        | 2 | ((190726-bardzo-niebezpieczne-skladowisko; 190804-niespodziewany-wplyw-aidy)) |
| Amelia Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Cezary Alentik       | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Dagmara Doberman     | 1 | ((200311-wygrany-kontrakt)) |
| Feliks Keksik        | 1 | ((211120-glizda-ktora-leczy)) |
| Julia Morwisz        | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Justynian Diakon     | 1 | ((211120-glizda-ktora-leczy)) |
| Kacper Bankierz      | 1 | ((211120-glizda-ktora-leczy)) |
| Karol Pustak         | 1 | ((211120-glizda-ktora-leczy)) |
| Liliana Bankierz     | 1 | ((200311-wygrany-kontrakt)) |
| Lucjusz Blakenbauer  | 1 | ((200311-wygrany-kontrakt)) |
| Minerwa Metalia      | 1 | ((200311-wygrany-kontrakt)) |
| Mirela Orion         | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Oliwia Lemurczak     | 1 | ((211120-glizda-ktora-leczy)) |
| Ossidia Saitis       | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Roland Grzymość      | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Roland Sowiński      | 1 | ((211120-glizda-ktora-leczy)) |
| Sabina Kazitan       | 1 | ((211120-glizda-ktora-leczy)) |
| Saitaer              | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Sensacjusz Diakon    | 1 | ((211120-glizda-ktora-leczy)) |
| Stella Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Tomasz Tukan         | 1 | ((190714-kult-choroba-esuriit)) |
| Ziemowit Zięba       | 1 | ((200311-wygrany-kontrakt)) |