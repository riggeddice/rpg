---
categories: profile
factions: 
owner: public
title: Maja Szewieczak
---

# {{ page.title }}


# Generated: 



## Fiszki


* engineering, comms (atarienka); wyjątkowo kompetentna | @ 221220-dezerter-z-mrocznego-wolu
* ENCAO:  -+000 |Nie lubi ryzyka;;Podejrzliwa| VALS: Power, Face >> Humility, Tradition| DRIVE: Wyrwać się z tej budy | @ 221220-dezerter-z-mrocznego-wolu
* "zrobię wszystko, jeśli mnie zabierzecie z tej jednostki; nie mogę skończyć na ŚMIECIARCE!" | @ 221220-dezerter-z-mrocznego-wolu

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230103-protomag-z-mrocznego-wolu    | jej rodzice mają kłopoty z Syndykatem Aureliona. Jest kompetentna i jest tu by ją chronić (XD). Nawet, jeśli ona o tym nie wie i nie chce. | 0111-11-19 - 0111-11-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adela Myrias         | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Juliusz Akramantanis | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Krzysztof Workisz    | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Lena Ifirentis       | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Marianna Atrain      | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Nadja Kilmodrian     | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Nela Kaltaner        | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| OO Mroczny Wół       | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Robert Warłomkacz    | 1 | ((230103-protomag-z-mrocznego-wolu)) |