---
categories: profile
factions: 
owner: public
title: Erwin Pies
---

# {{ page.title }}


# Generated: 



## Fiszki


* kapral marine, Orbiter (w sumie z nim - 4 marines) | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* ENCAO:  -0-+- |Skryty;;Wiecznie zagubiony;;Praktyczny| VALS: Tradition, Stimulation >> Power| DRIVE: Ochraniać słabszych | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* kapral marine, Orbiter | @ 221102-astralna-flara-i-porwanie-na-karsztarinie

### Wątki


kosmiczna-chwala-arianny
historia-arianny
wplywy-aureliona-na-orbiterze

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly | kapral i dowódca marines na Królowej; stanął po stronie Arianny. Zbiera bardzo dokładny raport i wspiera Ariannę jak tylko ten Orbiterowiec potrafi. Dość kompetentny, choć nie miał czym i z kim pracować póki nie miał Arianny. | 0100-05-06 - 0100-05-12 |
| 220928-kapitan-verlen-i-pojedynek-z-marine | dyskretnie przekazał Ariannie, że Arnulf Perikas biczuje ludzi na pokładzie jednostki - wprowadza zasady Aurum. Ogólnie, Arianna ufa mu najbardziej a on jej najbardziej pomaga. | 0100-05-14 - 0100-05-16 |
| 221019-kapitan-verlen-i-pierwszy-ruch-statku | kapral przygotował mordercze ćwiczenia i przećwiczył załogę na działanie w warunkach stresowych i awaryjnych. Porządne 10 dni. | 0100-05-23 - 0100-06-04 |
| 221026-kapitan-verlen-i-koniec-przygody-na-krolowej | dyskretnie porozstawiał marines w miejscach które mogą być sabotowane; cicho zapewnił, że wszystko bezbłędnie działa z tą załogą. | 0100-06-08 - 0100-06-15 |
| 221123-egzotyczna-pieknosc-na-astralnej-flarze | z G.Kircznikiem próbował ostrzec Ariannę by nie wprowadzać uratowanych na Flarę (ryzyko). Nie poradził sobie z przesłuchaniem Ellariny i został elementem mema "Pies szczeka nie głaszcze" jak clipowali fragment przesłuchania. | 0100-09-12 - 0100-09-15 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 221123-egzotyczna-pieknosc-na-astralnej-flarze | pojawił się mem, wyciek z przesłuchania: "Pies szczeka nie głaszcze". Jak Pies podniósł głos, Ellarina zaczęła płakać a Pies nie wie co robić i mówi "uspokój się, to ROZKAZ!" | 0100-09-15

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Daria Czarnewik      | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Arnulf Perikas       | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| OO Królowa Kosmicznej Chwały | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Leszek Kurzmin       | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Maja Samszar         | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Stefan Torkil        | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Władawiec Diakon     | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Alezja Dumorin       | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Grażyna Burgacz      | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Klaudiusz Terienak   | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Leona Astrienko      | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Szymon Wanad         | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Tomasz Ruppok        | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Antoni Kramer        | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Ellarina Samarintael | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gabriel Lodowiec     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gerwazy Kircznik     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Kajetan Kircznik     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Klarysa Jirnik       | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Marcel Kulgard       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Marcelina Trzęsiel   | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| OO Astralna Flara    | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| OO Athamarein        | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| OO Tucznik Trzeci    | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Romeo Verlen         | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Rufus Warkoczyk      | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| SCA Hadiah Emas      | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Szczepan Myrczek     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |