---
categories: profile
factions: 
owner: public
title: Cyprian Mirisztalnik
---

# {{ page.title }}


# Generated: 



## Fiszki


* chorąży Aurum, kompetentny w walce i wierny towarzysz Eleny w świecie Orbitera | @ 230905-druga-tienka-przybywa-na-pomoc
* OCEAN (E+ A-): pełen życiowej energii i charyzmy, opiekuńczy ALE arogancki i roszczeniowy. "Kto ma moc, ma obowiązek. Kto ma moc, tego słuchają." | @ 230905-druga-tienka-przybywa-na-pomoc
* VALS: (Conformity, Tradition): głęboko wierzy w znaczenie hierarchii i lojalności. Wierzy też w odpowiednie wyglądanie i zachowywanie się. "Krew mówi sama za siebie." | @ 230905-druga-tienka-przybywa-na-pomoc
* Core Wound - Lie: "Nie będąc arystokratą, wszyscy mnie ignorowali" - "Zapewnię odpowiednią opiekę i kontrolę nad innymi, dzięki czemu mnie nie odrzucą i nie zostawią." | @ 230905-druga-tienka-przybywa-na-pomoc
* Styl: Opiekuńczy i dominujący jednocześnie, kłótliwy ale elegancki. Patrzy na świat przez pryzmat hierarchii i formalnej kontroli. | @ 230905-druga-tienka-przybywa-na-pomoc

### Wątki


salvagerzy-lohalian
rehabilitacja-eleny-samszar

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230905-druga-tienka-przybywa-na-pomoc | chorąży Samszarów, elegancki i opiekuńczy. Dołącza do Eleny by spłacić dług z powodu uratowania jego ojca przez matkę Eleny. Już na starcie podpadł mówiąc o Elenie 'tien porucznik'. Narwany, ale chce pomóc. | 0095-10-05 - 0095-10-07 |
| 231024-rozbierany-poker-do-zwalczania-interis | narzekał, że Elena musi obierać ziemniaki a Rachela nie. I że on musi. Próbował obierać ziemniaki LEPIEJ niż Estella, co mu nie wychodziło; Estella ma większą wprawę. | 0095-10-08 - 0095-10-10 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adelajda Kalmiris    | 2 | ((230905-druga-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Elena Samszar        | 2 | ((230905-druga-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Mikołaj Larnecjat    | 2 | ((230905-druga-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Estella Gwozdnik     | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Jaromir Gaburon      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Jarosław Mirelski    | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Maurycy Derwisz      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Paweł Lawarczak      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Tomasz Afagrel       | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Zenobia Samszar      | 1 | ((230905-druga-tienka-przybywa-na-pomoc)) |