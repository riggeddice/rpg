---
categories: profile
factions: 
owner: public
title: Ola d'Amelia
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191126-smierc-aleksandrii           | stworzona TAI ze Skażonej Aleksandrii; odrzuciła ochronę ludzi z uwagi na swoje obsesyjne oddanie Amelii. | 0110-10-10 - 0110-10-13 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 191126-smierc-aleksandrii           | poluje na nią spora część wpływowych magów Aurum. Jej to nie przeszkadza, bo jest oddana Amelii. | 0110-10-13

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Mirzant       | 1 | ((191126-smierc-aleksandrii)) |
| Kamil Lemurczak      | 1 | ((191126-smierc-aleksandrii)) |
| Kasjopea Maus        | 1 | ((191126-smierc-aleksandrii)) |
| Sebastian Kuralsz    | 1 | ((191126-smierc-aleksandrii)) |
| Teresa Marszalnik    | 1 | ((191126-smierc-aleksandrii)) |