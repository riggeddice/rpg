---
categories: profile
factions: 
owner: public
title: Adrian Kozioł
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210912-medyk-przeciwko-cieniaszczytowi | były Kasi; Martyn do niego zadzwonił; potrzebny mu był ochroniarz celem wymanewrowania BlackTech i Adrian polecił swoją dziewczynę. Martyn nie złamał bro code, ale w wyniku jego Paradoksu Adela się w nim śmiertelnie zakochała. Od teraz - śmiertelny wróg Martyna. | 0079-09-13 - 0079-09-16 |
| 211016-zlamane-serce-martyna        | wyszedł na ludzi, do rodu Mysiokorników, ich szef ochrony. Pomógł Martynowi zlokalizować Adelę. | 0085-08-31 - 0085-09-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adela Kołczan        | 2 | ((210912-medyk-przeciwko-cieniaszczytowi; 211016-zlamane-serce-martyna)) |
| Martyn Hiwasser      | 2 | ((210912-medyk-przeciwko-cieniaszczytowi; 211016-zlamane-serce-martyna)) |
| Amadeusz Martel d'Kopiec | 1 | ((211016-zlamane-serce-martyna)) |
| Franciszek Tocz      | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Irena Czakram        | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Jolanta Kopiec       | 1 | ((211016-zlamane-serce-martyna)) |
| Kalina Rota d'Kopiec | 1 | ((211016-zlamane-serce-martyna)) |
| Kasia Pieprz         | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Paweł Pieprz         | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Wanessa Ogarek       | 1 | ((211016-zlamane-serce-martyna)) |