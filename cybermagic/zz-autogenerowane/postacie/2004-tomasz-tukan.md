---
categories: profile
factions: 
owner: public
title: Tomasz Tukan
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "szczeliniec, terminus, wolny uśmiech"
* owner: "public"
* title: "Tomasz Tukan"


## Kim jest

### Paradoksalny Koncept

Pustogorski terminus współpracujący z mafią Grzymościa. Neuronauta, katalista i terminus posiadający słabostki do pięknych kobiet. Świetny w walce magicznej i kontroli mentalnej, ale pomiatany przez Kajrata. Ma bardzo duże umiejętności, jest bardzo uzdolniony - ale jednocześnie próbuje politycznie skonsolidować swoją władzę; nie chce samej mocy. Mściwy, ale na zimno. Arogancki, uważa, że należy mu się więcej niż ma - a jednocześnie drży ze strachu przed Esuriit i Ixionem.

### Motto

"Technologia jest błędem - biomagia powinna zastąpić tą głupią ścieżkę ewolucji."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Neuronauta bardzo wysokiej klasy - wchodzi w głowę i potrafi wykryć cokolwiek oraz stworzyć dowolne wspomnienia; też przepisać komuś osobowość.
* ATUT: Bardzo skuteczny katalista bojowy; jako terminus specjalizuje się w obracaniu energii przeciw przeciwnikowi i atakach mentalnych i wypalających.
* SŁABA: Kocha piękne kobiety. Chce być przez nie uwielbiany - a Grzymość mu to zapewnia (i tak go złapał).
* SŁABA: Mimo wszelkich jego talentów, PRZERAŻAJĄ go Ixion i Esuriit. Jego moc w ogóle nie wchodzi z tymi energiami w interakcję.

### O co walczy (3)

* ZA: Pragnie małej baronii, a najlepiej tytuł arystokratyczny od Aurum. Chce miejsca, w którym będzie mieć pełną władzę i pełną kontrolę. Nieważne - mafia, Pustogor, Aurum...
* ZA: Przekonać wszystkich, że bioformy są lepszym rozwiązaniem niż magitech. Zbudowanie Absolutnej Ochrony, perfekcyjnej linii biomagicznej.
* VS: Zwalcza TAI, roboty, a już szczególnie seksboty (do tych ma patologicznie wrogi stosunek). Preferuje biomagię nad magitech.
* VS: Niech mafia nie pokona terminusów. Niech terminusi nie pokonają mafii. Potrzebna jest harmonia konfliktowych stron. Nie "pokój" a "stabilny konflikt".

### Znaczące Czyny (3)

* Gdy doszło do emisji koszmarów w Kasynie Marzeń, wszystkich cierpliwie wyleczył z traumy mentalnej i usunął im pamięć o incydencie. Po drodze dyskretnie zniszczył seksbota.
* Wynegocjował włączenie niejednego pomniejszego gangu do mafii Grzymościa, dając szefom tych gangów dokładnie to o czym marzyli, niezależnie jak podłe to nie było.
* Znalazł i rozszarpał w walce orbiterskiego miragenta, który próbował zbudować siatkę na terenie Pustogoru; potem zgłosił to Karli.

### Kluczowe Zasoby (3)

* COŚ: Eksperymentalny Biopancerz będący odpowiednikiem servarów terminuskich; cały czas rozwijany, regenerujący się i żywy, z wieloma mackami, z bronią opartą na kwasie.
* KTOŚ: Organizacja "Wolny Uśmiech" Grzymościa dostarcza mu ludzi, terminusi pustogorscy zapewniają mu intel. Jego harem - zapewnia mu wszystko inne.
* WIEM: Zna skryte marzenia i nadzieje bardzo wielu znaczących magów w Szczelińcu. Jak trzeba, potrafi je spełnić by pozyskać możliwość współpracy.
* OPINIA: Łajza. Bardzo uzdolniona i w sumie groźna w walce, ale łajza. Nie jest szczególnie szanowany.

## Inne

### Wygląd

.

### Coś Więcej

.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200507-anomalna-figurka-zabboga     | stworzył nietrywialny plan zdobycia w ukryciu przed Pustogorem figurki niszczącej dane i dokumenty, by ostatecznie ją otrzymać w handlu z Fantasmagorią. | 0109-09-19 - 0109-09-26 |
| 190519-uciekajacy-seksbot           | neuronauta Pustogoru, terminus, który nie chce podpadać Ernestowi. Nienawidzi seksbotów, całkowicie. Najchętniej zadowoliłby mafię nawet kosztem żywej istoty. | 0110-04-25 - 0110-04-26 |
| 190616-anomalna-serafina            | niezły neuronauta, katalista i terminus ale nie ma kręgosłupa i nie radzi sobie z konfliktem. Poniewierany i upokarzany przez mafię, mimo, że powinien słuchać Pięknotkę.. | 0110-05-12 - 0110-05-15 |
| 190709-somnibel-uciekl-arienikom    | nieprzekonany do pomocy Pięknotce, ale mógł pomóc dzieciakom Dotkniętym przez energię somnibela; znalazł coś u jednej z dziewczynek. | 0110-06-05 - 0110-06-07 |
| 190714-kult-choroba-esuriit         | chciał rozmontować kult i się o nim wszystkiego dowiedzieć; zainfekował się Esuriit i podpadł Kajratowi. Uratowany przez Pięknotkę. | 0110-06-07 - 0110-06-09 |
| 200202-krucjata-chevaleresse        | chroni interesy mafii przeciw Pustogorowi. Ale nie chce szkody terminusów. Zwrócił się do Pięknotki; Malictrix spadnie na niego jako jego "zasługa". Nie ma za co. | 0110-07-24 - 0110-07-27 |
| 200425-inflitrator-poluje-na-tai-minerwy | neuronauta-terminus; nie cierpi Minerwy i jej "chorej" magii, nie cierpi Pięknotki. Chce koniecznie udowodnić winę Minerwy; upokorzony przed studentką przez Pięknotkę. | 0110-08-24 - 0110-08-26 |
| 201020-przygoda-randka-i-porwanie   | Gabriel się doń zwrócił by pomógł przekonać opiekuna Uli. Tak wymanewrował w rozmowie, że wygrał WSZYSTKO - dostał kontakt do Sowińskich oraz chwałę za sukces. Ula nie dostała nic a Gabriel dostał straszny opiernicz. | 0110-10-25 - 0110-10-27 |
| 210713-rekin-wspiera-mafie          | pomógł Marysi Sowińskiej uwolnić Arkadię by rozwiązać problemy Rekinów w świecie Rekinów, za co dostał dobry weekend z piękną damą w luksusowych hotelach Sowińskich w Aurum. | 0111-06-06 - 0111-06-09 |
| 210720-porwanie-daniela-terienaka   | powiedział Marysi, że Ula nie ma autoryzowanej żadnej akcji terminusów. Był skłonny sprzedać Marysi sekrety przeszłości Uli za przysługę. Pies na luksusy w Aurum z ładnymi kobietami. | 0111-06-14 - 0111-06-16 |
| 210817-zgubiony-holokrysztal-w-lesie | sprawa z ko-matrycą Kuratorów była tak poważna, że zajął się nią sam a nie delegował Laurze. Przekonał Marysię, by oddała ją Pustogorowi i dał ją Alanowi, by zastawić pułapkę na mafię. Plus, wślizgnął się na emeryturę do Sowińskich. Wielki wygrany. I postąpił właściwie. | 0111-06-22 - 0111-06-24 |
| 210824-mandragora-nienawidzi-rekinow | gdy Marysia powiedziała o mandragorze, natychmiast wysłał Marysi do pomocy Laurę i Ekaterinę. | 0111-07-03 - 0111-07-05 |
| 210921-przybycie-rekina-z-eterni    | zamiast wsadzić Arkadię za niewinność do więzienia (by usunąć ją jako zagrożenie dla Ernesta Namartela) wkręcił ją w program dla uczniów terminusa. I tak zakręcił, że mu podziękowali XD. | 0111-07-20 - 0111-07-25 |
| 220816-jak-wsadzic-ule-alanowi      | jakkolwiek spełnia prośby Marysi, zmartwiła go prośba by wsadzić Ulę (z visterminem!) pod Alana bo się o Ulę boi. Może gardzić subterminuskami, ale nie chce ich krzywdy - więc się postawił Marysi. | 0111-10-12 - 0111-10-13 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 200507-anomalna-figurka-zabboga     | zobowiązany do ochrony Fantasmagorii, jej członków oraz do pomocy w odbudowaniu pozycji i bogactwa Melindy Teilert (plus ochrony). | 0109-09-26
| 200507-anomalna-figurka-zabboga     | uzyskał Anomalną Figurkę Żabboga, dzięki której może zniszczyć dowolne dokumenty. Ale użyć może jej tylko ktoś kto nie wie jak działa i kto ją ukradł. | 0109-09-26
| 200507-anomalna-figurka-zabboga     | jest absolutnie przekonany, że na terenie Podwiertu jest potężna ukryta organizacja (najpewniej z Aurum), która stoi za Fantasmagorią. | 0109-09-26
| 190616-anomalna-serafina            | Upokorzony i pogardzany przez mafię. Ma jeszcze mniejszy kręgosłup niż kiedykolwiek. | 0110-05-15
| 200616-bardzo-straszna-mysz         | Dostał LEGENDARNY opiernicz od Ksenii za to, że puszcza Laurę na akcje z potworami w terenie Esuriit I NAWET O TYM NIE WIE. Poszło do papierów. | 0110-09-21
| 201020-przygoda-randka-i-porwanie   | uznanie Laurencjusza Sorbiana i kontakty z Sowińskimi; przechwycił lwią część sławy i zasług za akcję Gabriela i Roberta. | 0110-10-27
| 210720-porwanie-daniela-terienaka   | w Pustogorze już nikt nie ma wątpliwości, że się sprzedał Aurum/Sowińskim. Przynajmniej nie mafii, nie? | 0111-06-16
| 210817-zgubiony-holokrysztal-w-lesie | znalazł sobie złoty bilet - gdy Pustogor go usunie lub gdy Marysia Sowińska opuści ten teren ma gwarantowane miejsce w Aurum, przy Marysi Sowińskiej. | 0111-06-24
| 210921-przybycie-rekina-z-eterni    | zadowolenie ze strony zarówno Arkadii Verlen jak i Pustogoru - znalazł dla niej coś fajnego: fuszkę "uczennicy terminusa". | 0111-07-25

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 6 | ((210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka; 210817-zgubiony-holokrysztal-w-lesie; 210824-mandragora-nienawidzi-rekinow; 210921-przybycie-rekina-z-eterni; 220816-jak-wsadzic-ule-alanowi)) |
| Pięknotka Diakon     | 6 | ((190519-uciekajacy-seksbot; 190616-anomalna-serafina; 190709-somnibel-uciekl-arienikom; 190714-kult-choroba-esuriit; 200202-krucjata-chevaleresse; 200425-inflitrator-poluje-na-tai-minerwy)) |
| Ernest Kajrat        | 4 | ((190519-uciekajacy-seksbot; 190616-anomalna-serafina; 190709-somnibel-uciekl-arienikom; 190714-kult-choroba-esuriit)) |
| Karolina Terienak    | 4 | ((210720-porwanie-daniela-terienaka; 210824-mandragora-nienawidzi-rekinow; 210921-przybycie-rekina-z-eterni; 220816-jak-wsadzic-ule-alanowi)) |
| Urszula Miłkowicz    | 4 | ((201020-przygoda-randka-i-porwanie; 210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka; 220816-jak-wsadzic-ule-alanowi)) |
| Kacper Bankierz      | 3 | ((201020-przygoda-randka-i-porwanie; 210713-rekin-wspiera-mafie; 210817-zgubiony-holokrysztal-w-lesie)) |
| Laura Tesinik        | 3 | ((200425-inflitrator-poluje-na-tai-minerwy; 210817-zgubiony-holokrysztal-w-lesie; 210824-mandragora-nienawidzi-rekinow)) |
| Liliana Bankierz     | 3 | ((190519-uciekajacy-seksbot; 201020-przygoda-randka-i-porwanie; 210817-zgubiony-holokrysztal-w-lesie)) |
| Mariusz Trzewń       | 3 | ((200202-krucjata-chevaleresse; 210720-porwanie-daniela-terienaka; 210817-zgubiony-holokrysztal-w-lesie)) |
| Rafał Torszecki      | 3 | ((210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka; 210921-przybycie-rekina-z-eterni)) |
| Amelia Sowińska      | 2 | ((210824-mandragora-nienawidzi-rekinow; 210921-przybycie-rekina-z-eterni)) |
| Daniel Terienak      | 2 | ((210720-porwanie-daniela-terienaka; 210824-mandragora-nienawidzi-rekinow)) |
| Gabriel Ursus        | 2 | ((200425-inflitrator-poluje-na-tai-minerwy; 201020-przygoda-randka-i-porwanie)) |
| Minerwa Metalia      | 2 | ((200202-krucjata-chevaleresse; 200425-inflitrator-poluje-na-tai-minerwy)) |
| Alan Bartozol        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Amanda Kajrat        | 1 | ((190714-kult-choroba-esuriit)) |
| Antoni Żuwaczka      | 1 | ((190616-anomalna-serafina)) |
| Arkadia Verlen       | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Arnulf Poważny       | 1 | ((190519-uciekajacy-seksbot)) |
| Ataienne             | 1 | ((200202-krucjata-chevaleresse)) |
| Cyryl Perikas        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Damian Orion         | 1 | ((200202-krucjata-chevaleresse)) |
| Damian Polwonien     | 1 | ((200507-anomalna-figurka-zabboga)) |
| Daniel Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Diana Tevalier       | 1 | ((200202-krucjata-chevaleresse)) |
| Ekaterina Zajcew     | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Eliza Ira            | 1 | ((190519-uciekajacy-seksbot)) |
| Ernest Namertel      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Erwin Galilien       | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Gustaf Profnos       | 1 | ((200507-anomalna-figurka-zabboga)) |
| Henryk Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Jan Łowicz           | 1 | ((200507-anomalna-figurka-zabboga)) |
| Jan Revlen           | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Jolanta Sowińska     | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Julia Kardolin       | 1 | ((210713-rekin-wspiera-mafie)) |
| Karol Pustak         | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Keraina d'Orion      | 1 | ((200202-krucjata-chevaleresse)) |
| Kinga Kruk           | 1 | ((200507-anomalna-figurka-zabboga)) |
| Krystian Namałłek    | 1 | ((190616-anomalna-serafina)) |
| Ksawery Wojnicki     | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Laurencjusz Sorbian  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Lucjan Sowiński      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Malictrix d'Itaran   | 1 | ((200202-krucjata-chevaleresse)) |
| Mateusz Kardamacz    | 1 | ((200202-krucjata-chevaleresse)) |
| Melinda Teilert      | 1 | ((200507-anomalna-figurka-zabboga)) |
| Mimoza Diakon        | 1 | ((220816-jak-wsadzic-ule-alanowi)) |
| Nataniel Morlan      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Natasza Aniel        | 1 | ((200507-anomalna-figurka-zabboga)) |
| Ossidia Saitis       | 1 | ((190519-uciekajacy-seksbot)) |
| Robert Pakiszon      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Robinson Porzecznik  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Ronald Grzymość      | 1 | ((190616-anomalna-serafina)) |
| Rupert Mysiokornik   | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Saitaer              | 1 | ((190519-uciekajacy-seksbot)) |
| Sensacjusz Diakon    | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Serafina Ira         | 1 | ((190616-anomalna-serafina)) |
| Staś Arienik         | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Stella Armadion      | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Strażniczka Alair    | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Talia Aegis          | 1 | ((200202-krucjata-chevaleresse)) |
| Teresa Mieralit      | 1 | ((190519-uciekajacy-seksbot)) |
| Triana Porzecznik    | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Tymon Grubosz        | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Urszula Arienik      | 1 | ((190709-somnibel-uciekl-arienikom)) |