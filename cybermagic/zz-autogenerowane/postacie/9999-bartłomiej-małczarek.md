---
categories: profile
factions: 
owner: public
title: Bartłomiej Małczarek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200222-rozbrojenie-bomby-w-kalbarku | terminus; Skażony kralotycznym wspomaganiem i pokonany przez Pięknotkę. Bardzo ciężki stan. | 0110-07-27 - 0110-08-01 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksandra Szklarska | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Ataienne             | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Diana Tevalier       | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Lucjusz Blakenbauer  | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Mateusz Kardamacz    | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Pięknotka Diakon     | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Tymon Grubosz        | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |