---
categories: profile
factions: 
owner: public
title: Feliks Ketran
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220323-zatruta-furia-gaulronow      | tien eternijski lekarz, mag; chce zajmować się pomocą dzieciom a nie gaulronom. Ale nie pozwoli na zatruwanie gaulronów. Z rozkazu Suwana zajął się gaulronami i złożył z Nephthys inhibitory dla gaulronów. | 0111-11-27 - 0111-12-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dominika Perikas     | 1 | ((220323-zatruta-furia-gaulronow)) |
| Erwin Mumurnik       | 1 | ((220323-zatruta-furia-gaulronow)) |
| Graniec Borgon       | 1 | ((220323-zatruta-furia-gaulronow)) |
| Lamia Akacja         | 1 | ((220323-zatruta-furia-gaulronow)) |
| Maciej Parczak       | 1 | ((220323-zatruta-furia-gaulronow)) |
| Suwan Chankar        | 1 | ((220323-zatruta-furia-gaulronow)) |
| TAI Nephthys         | 1 | ((220323-zatruta-furia-gaulronow)) |