---
categories: profile
factions: 
owner: public
title: Diana Nałęcznik
---

# {{ page.title }}


# Generated: 



## Fiszki


*  | @ 230831-potwory-ktore-przetrwaly-eter
* OCEAN: (A+E+): Niezwykle empatyczna, o ciepłym sercu, gotowa poświęcić się dla innych. | @ 230831-potwory-ktore-przetrwaly-eter
* VALS: (Benevolence, Universalism): "Dla moich przyjaciół zrobię wszystko. Oni są moją rodziną." | @ 230831-potwory-ktore-przetrwaly-eter
* Core Wound - Lie: "Uratowali mnie. Pomogli mi w potrzebie. A potem umierali - jeden za drugim" - "Nigdy więcej nie zostawię nikogo w potrzebie, poświęcę WSZYSTKO" | @ 230831-potwory-ktore-przetrwaly-eter
* Styl: Jej oczy zawsze patrzą uważnie na otoczenie, szukając sposobu, by pomóc. | @ 230831-potwory-ktore-przetrwaly-eter
* Metakultura: Sybrianin: "Współczucie i troska o innych to nasza największa siła. Przyjaźń jest wieczna." | @ 230831-potwory-ktore-przetrwaly-eter

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230831-potwory-ktore-przetrwaly-eter | potężny terrorform, czarodziejka. Uratowała wszystkich, zmieniając ich w terrorformy. Nie chciała dołączyć do Centrali, ale zew był silniejszy od niej. Corrupted, głos w głowie Dawida - chce swojego męża z powrotem i wierzy, że uratuje 'swoich' i 'skończy misję'... | 0093-03-11 - 0093-03-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Mardiblon     | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Bartek Mardiblon     | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Dawid Nałęcznik      | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Esaria Mirtalis      | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Karol La Viris       | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Mateusz Owisiec      | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Rosenkrat Amiribasit | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |