---
categories: profile
factions: 
owner: public
title: Karol Lertys
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220723-polowanie-na-szczury-w-nativis | 73-letni "dziadek" Celiny i Jana; kiedyś kapitan aspiriańskiego statku OA Erozja Ego. Gdy został zdradzony i otruty, lojaliści go wydobyli ze statku i został "dziadkiem" gdy prawdziwi rodzice zginęli. Potem został inżynierem Nativis i się ukrywa. Nadal jest jedynym zdolnym do aktywacji Erozji Ego. | 0087-05-03 - 0087-05-12 |
| 230628-wojna-o-arkologie-nativis-konsolidacja-sil | mimo ogromnej nieufności i niechęci do Kidirona, przekonał swoich ludzi że muszą współpracować z Infernią. To ważniejsze - muszą uratować Arkologię przed kimś GORSZYM (Lobrakiem). | 0093-03-27 - 0093-03-28 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220831-czarne-helmy-i-robaki        | coś jest nie tak, Kidironowie i Robaki nie mogli zrobić tego sabotażu; zajmie się tym i wejdzie w głąb. Podejrzewa Tymona Korkorana. | 0092-08-27
| 230628-wojna-o-arkologie-nativis-konsolidacja-sil | wspiera Bartłomieja Korkorana i tylko jego z rodziny Korkoranów. Wszyscy inni są jego zdaniem lojalistami Kidirona. | 0093-03-28

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Celina Lertys        | 2 | ((220723-polowanie-na-szczury-w-nativis; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Laurencjusz Kidiron  | 2 | ((220723-polowanie-na-szczury-w-nativis; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Tymon Korkoran       | 2 | ((220723-polowanie-na-szczury-w-nativis; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Ardilla Korkoran     | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Bartłomiej Korkoran  | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Emilia d'Erozja      | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Eustachy Korkoran    | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Izabella Saviripatel | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Jan Lertys           | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Marcel Draglin       | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| OA Erozja Ego        | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Ralf Tapszecz        | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Tobiasz Lobrak       | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |