---
categories: profile
factions: 
owner: public
title: Raoul Lavanis
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "orbiter, oddział verlen, noktianin"
* owner: "public"
* title: "Raoul Lavanis"

## Kim jest

### W kilku zdaniach

Noktiański advancer Inferni, starszy koleś (jak na advancera). Doskonale zna archaiczny noktiański, interesuje się archeologią i archeotechem oraz im mniej ma z magią do czynienia tym czuje się lepiej. Spostrzegawczy snajper, doskonały w operacjach próżniowych. Dopiero tu - na Astorii - został neurosprzężony i połączony z eidolonem. Najlepiej czuje się w kosmosie, lubi przebywać poza statkiem. W sercu wie, że rzeczywistość nie ma sensu i się ciągle ZMIENIA. Miał pecha widzieć ze snajperki oblicze Saitaera i walczyć z lustrem Arazille.

### Co się rzuca w oczy

* Motto: "Nigdy nie da się niczego zaplanować, nigdy nie wiesz czy np. nie zmienią Ci się prawa fizyki czy nie pojawi nowy bóg."
* Nawet noktianie uważają go za dziwnego. Najchętniej jest gdzieś na boku i dąży do ciszy i samotności.
* Ma bardzo dobre pomysły, szybko je wymyśla i szybko się adaptuje.
* Jest chyba najbardziej ciekawską osobą jaką można spotkać. Wszystko próbuje ZROZUMIEĆ - zupełnie jak Klaudia.
* Czasem coś powie. Dziwnego. Coś co świadczy, że NIE DO KOŃCA TU JEST. Np. żart typu "jak pi będzie wynosiło 3.11" i się zaśmieje z żartu.
* Nie unika magii ani anomalii. Wyraźnie nie traktuje ich jako coś dziwnego. Dla niego WSZYSTKO jest dziwne.

### Jak sterować postacią

* Trzyma się na uboczu zarówno magów jak i ludzi. Unika absolutnie wszystkich. Wykonuje misję, współpracuje z innymi, ale woli iść na bok niż się socjalizować.
* Najchętniej przebywa w kosmosie i patrzy w gwiazdy w ciszy. To nie melancholia, to jego forma regeneracji - spacer kosmiczny.
* Uważa TAI i BIA za pełnoprawne istoty żywe i tak je traktuje - acz nie próbuje naprawić ich losu by samemu nie zniszczyć tego co zapewniła im Arianna. 
* Nerd, regularnie ucieka w książki i bibliotekę. Próbuje ZROZUMIEĆ co się stało, jak wszystko działa. Zrozumienie jest jego obsesją.
* Bardzo irytują go nielogiczności i nie pasujące do siebie elementy równania.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* To on doszedł do tego, że to Tal Marczak próbuje zniszczyć Tivr. Przekazał to innym noktianom. Skąd wiedział? Zmienił się wzór zachowań Tala.
* Zestrzelił lustro Arazille sterujące pewnym oficerem na fregacie z zewnątrz fregaty (kosmos), używając snajperki elmag i wykorzystując zakrzywienie grawitacyjne magią.
* Ściśle współpracował z Orbiterem po napotkaniu Saitaera i po tym jak zrozumiał, że nie rozumie rzeczywistości. M.in. dzięki niemu dano szansę noktianom. Nigdy nie próbował zdrady.

### Co się rzuca w oczy: Atuty, Przewagi, Zasoby (3, 6)

* PRACA: neurosprzężony advancer, snajper i specjalista od operacji próżniowych
* PRACA: bardzo interesuje się archeotechem i kulturą noktiańską; zna różne stare języki noktiańskie
* PRACA: całkiem niezły aktor; potrafi nieźle kłamać i zachowywać się jak nie-on
* PRACA: jeden z najlepszych detektywów jakich ma Infernia. Zarówno z zamiłowania ("zrozumieć") jak i z cech (drobiazgowość). Wszystko zauważa.
* CECHA: bardzo drobiazgowy, mało rzeczy mu unika. Od razu zauważa jak coś do czegoś nie pasuje. To nie jest paranoja, to cholerna irytacja.
* COŚ: dobrej klasy sprzęt - eidolon oraz snajperka elmag.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Wpierw straciłem rodzinę na Noctis. Potem okazało się, że wszystko w co wierzyłem jest kłamstwem. Rzeczywistość NIE MA SENSU i NIE JEST MOŻLIWA."
* CORE LIE: "Muszę zrozumieć, co się dzieje w tym świecie. Przez zrozumienie - odzyskam kontrolę. Bo inaczej ZNOWU wszystko się zmieni."
* Jest bardzo podatny na pętle logiczne, teorie spiskowe itp. Widzi wzory gdzie ich nie ma i potrafi oddać im za dużo czasu i uwagi.
* Dla wiedzy wejdzie w pułapkę. Paradoksalnie, nie zauważy że wpada w pułapkę.
* Ma wątpliwości na wszelkie możliwe tematy. Wie, że nie rozumie. Wie, że nie wie. Często nie ufa zmysłom i pamięci. Sprawdza wszystko kilka razy.

### Serce i Wartości (3)

* Wartości
    * TAK: Conformity (rozpłynąć się), Humility (nie jesteśmy w stanie ogarnąć), Benevolence (we're in it together)
    * NIE: Power, Prestige, Achievement (all: pointless in the face of reality!)
    * Raoul chciałby być niewidoczny, rozpłynąć się w większej grupie i im wszystkim pomóc. Jednocześnie zawsze stoi z boku, nie wie co się ze światem stanie TERAZ.
    * Nie widzi sensu w akumulacji środków, wiedzy czy czegokolwiek - świat jest zbyt polimorficzny.
    * Zawsze pomoże jak jest w stanie. Jest bardzo spokojny i poczciwy.
* Ocean
    * E:-, N:+, C:+, A:0, O:+
    * Bardzo pomysłowy, zawsze znajdzie inne wyjście z sytuacji i szybko kombinuje.
    * Wszędzie widzi wzory, nawet tam, gdzie ich nie ma - i często widzi je jako niebezpieczeństwo.
    * Nie znosi czegoś NIE ROZUMIEĆ. Ma tendencję do obsesji na punkcie rzeczy których nie rozumie.
* Silnik
    * Zrozumieć rzeczywistość, zrozumieć ją za wszelką cenę. Dojść do tego jak rzeczywistość działa.
    * Zintegrować noktian z Astorią. Nie ma innej opcji - wszyscy jesteśmy ludźmi i wszyscy musimy działać przeciwko Rzeczywistości.
    
## Inne

### Wygląd

.

### Coś Więcej

* 2-3 lata starszy niż Martyn.

### Endgame

* ?


# Generated: 



## Fiszki


* (dekadianin); noktiański snajper, advancer, detektyw. | @ 221230-dowody-na-istnienie-nox-ignis
* ENCAO: -++0+ | Cichy i pomocny;; niewidoczny| Conformity Humility Benevolence > Pwr Face Achi | DRIVE: zrozumieć | @ 221230-dowody-na-istnienie-nox-ignis

### Wątki


historia-talii
cena-nox-ignis
wojna-deorianska

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231011-ekstaflos-na-tezifeng        | desygnowany przez Ariannę do bycia dywersją na pintce. Zadanie spełnił bez słowa i bez błędu. Niewidoczny i skuteczny. | 0110-10-20 - 0110-10-22 |
| 231025-spiew-nielalki-na-castigatorze | nieskończenie opanowany advancer, wierzy, że da się wszystko zrozumieć ale przede wszystkim WYKONUJE ROZKAZY DO LITERY nawet jak ich nie rozumie. Wszedł w głąb anomalii Altarient i mimo że stracił pojęcie pudełka, idąc precyzyjnie zgodnie z poleceniami rozwiązał problem. Niezwykle precyzyjny, cierpliwy i stoicki. MVP sesji - ani Martyn ani Elena nie daliby rady. | 0110-10-24 - 0110-10-26 |
| 231220-bladawir-kontra-przemyt-tienow | jako advancer robił operację poszukiwania przemytu na kadłubie innej jednostki. Mimo że szukał 'specjalnego', to znalazł 'zwykły' przez co Arianna ma kłopot. Plus, nawet on nie miał jak tego robić. | 0110-12-06 - 0110-12-11 |
| 240124-mikiptur-zemsta-woltaren     | odzyskał kapsułę z danymi z ataku na Isratazir, co było kluczowe dla morale załogi i gromadzenia informacji na temat wroga. | 0110-12-29 - 0111-01-01 |
| 240131-anomalna-mavidiz             | połączył Tivr i Mavidiz siatką jako advancer; gdy Eustachy wypadł w kosmos walcząc z nihilosektem, przechwycił Eustachego na pokład Tivra, ratując mu życie. | 0111-01-03 - 0111-01-08 |
| 210707-po-drugiej-stronie-bramy     | ludzki noktiański advancer Inferni; wraz z Eleną rozpracował starą bazę noktiańską. W grupie roboczej tłumaczącej sigile tej dziwnej archaicznej bazy. | 0111-05-11 - 0111-05-13 |
| 220216-polityka-rujnuje-pallide-voss | infiltruje z Eustachym Pallidę Voss; wpadł w kłopoty bo nie ufali Annice (karta była prawidłowa), ale sam się uwolnił. | 0112-06-01 - 0112-06-03 |
| 220330-etaur-i-przyneta-na-krypte   | chroni Marię na EtAur; odparł z Eustachym "potwora", po czym ucierpiał jak Eustachy wypalał fagi (phage) Marii. | 0112-07-02 - 0112-07-05 |
| 220610-ratujemy-porywaczy-eleny     | bardzo zauważliwy; zauważył, że "Elena" (miragent) jest źle podpięty do Containment Chamber. A potem w kosmosie zrobił insercję i wszedł na statek Aureliona z Leoną i Eustachym. | 0112-09-15 - 0112-09-17 |
| 220622-lewiatan-za-pandore          | wszedł na pokład jednostek tworzących Lewiatana i uratował tyle osób ile się dało; świetny advancer. Spacer kosmiczny, uruchamianie kapsuł ratunkowych itp. | 0112-09-30 - 0112-10-03 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 10 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220622-lewiatan-za-pandore; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231220-bladawir-kontra-przemyt-tienow; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Eustachy Korkoran    | 10 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220622-lewiatan-za-pandore; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231220-bladawir-kontra-przemyt-tienow; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Klaudia Stryk        | 9 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220622-lewiatan-za-pandore; 231025-spiew-nielalki-na-castigatorze; 231220-bladawir-kontra-przemyt-tienow; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Elena Verlen         | 6 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220610-ratujemy-porywaczy-eleny; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231220-bladawir-kontra-przemyt-tienow)) |
| OO Infernia          | 4 | ((220622-lewiatan-za-pandore; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231220-bladawir-kontra-przemyt-tienow)) |
| Antoni Bladawir      | 3 | ((231220-bladawir-kontra-przemyt-tienow; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Leona Astrienko      | 3 | ((220610-ratujemy-porywaczy-eleny; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Lars Kidironus       | 2 | ((231011-ekstaflos-na-tezifeng; 240124-mikiptur-zemsta-woltaren)) |
| Leszek Kurzmin       | 2 | ((231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Maria Naavas         | 2 | ((220330-etaur-i-przyneta-na-krypte; 220622-lewiatan-za-pandore)) |
| Martyn Hiwasser      | 2 | ((210707-po-drugiej-stronie-bramy; 220610-ratujemy-porywaczy-eleny)) |
| OO Castigator        | 2 | ((231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| OO Tivr              | 2 | ((220622-lewiatan-za-pandore; 240131-anomalna-mavidiz)) |
| Anna Tessalon        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Annika Pradis        | 1 | ((220216-polityka-rujnuje-pallide-voss)) |
| Borys Kragin         | 1 | ((240131-anomalna-mavidiz)) |
| Ewa Razalis          | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Feliks Walrond       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Grigor Tarnow        | 1 | ((240131-anomalna-mavidiz)) |
| Hestia d'Atropos     | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Igor Arłacz          | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Igor Stratos         | 1 | ((240131-anomalna-mavidiz)) |
| Jakub Bulgocz        | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Janus Krzak          | 1 | ((210707-po-drugiej-stronie-bramy)) |
| Karl Murnoff         | 1 | ((240131-anomalna-mavidiz)) |
| Kazimierz Darbik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Klaudiusz Terienak   | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Konstanty Keksik     | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Lamia Akacja         | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Leszek Czarban       | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Marcinozaur Verlen   | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Markus Wąż           | 1 | ((240131-anomalna-mavidiz)) |
| Marta Keksik         | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Mateus Sarpon        | 1 | ((220216-polityka-rujnuje-pallide-voss)) |
| Natalia Gwozdnik     | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OA Mikiptur          | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Ola Szerszeń         | 1 | ((220622-lewiatan-za-pandore)) |
| ONS Mavidiz          | 1 | ((240131-anomalna-mavidiz)) |
| OO Isratazir         | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| OO Karsztarin        | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| OO Pandora           | 1 | ((220622-lewiatan-za-pandore)) |
| OO Straszliwy Pająk  | 1 | ((220622-lewiatan-za-pandore)) |
| OO Tezifeng          | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Patryk Samszar       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Rita Stratos         | 1 | ((240131-anomalna-mavidiz)) |
| Rzeźnik Parszywiec Diakon | 1 | ((220622-lewiatan-za-pandore)) |
| SP Pallida Voss      | 1 | ((220216-polityka-rujnuje-pallide-voss)) |
| Szczepan Kaltaben    | 1 | ((220216-polityka-rujnuje-pallide-voss)) |
| Szymon Orzesznik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| TAI Eszara d'Castigator | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Tal Marczak          | 1 | ((210707-po-drugiej-stronie-bramy)) |
| Tara Ogniczek        | 1 | ((240131-anomalna-mavidiz)) |
| Władawiec Diakon     | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Zaara Mieralit       | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |