---
categories: profile
factions: 
owner: public
title: Rupert Mysiokornik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210406-potencjalnie-eksterytorialny-seksbot | 3 mc temu napalony agresywnie i zneutralizowany przez Marysię i Lorenę. Teraz: szabrował głowę seksbota by zrobić epicki rytuał dzięki któremu będzie miał dziewczynę która go doceni! | 0111-04-27 - 0111-04-28 |
| 210817-zgubiony-holokrysztal-w-lesie | na zlecenie Kacpra Bankierza szukał zgubionej ko-matrycy Kuratorów, nie wiedząc co to jest. Wygadał wszystko Marysi Sowińskiej. | 0111-06-22 - 0111-06-24 |
| 211221-chevaleresse-infiltruje-rekiny | kumpel Stasia Arienika, który by chronić go przed Justynianem Diakonem pokazał Justynianowi dupsko i rzucił się nań z Babu. | 0111-09-06 - 0111-09-07 |
| 230325-ten-nawiedzany-i-ta-ukryta   | Rekin; Koniecznie chciał wygrać wyścig (mający zdrażnić cywili) i zaimponować Oli Burgacz, więc kupił droższe paliwo i zaprosił Bulteriera (bo wolny ścigacz) i Daniela (bo uważa że wygra). Niestety, Daniel był lepszy a Mysiokornik się rozbił bo ktoś mu paliwo zatruł. Dostał mandat od konstruminusa. | 0111-10-16 - 0111-10-17 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210406-potencjalnie-eksterytorialny-seksbot | zna rytuał Sabiny Kazitan o tym, jak zrobić ultra-atrakcyjnego seksbota. Nie wie, co ten rytuał naprawdę robi. Wie, że od Sabiny Kazitan. Marysia Sowińska absolutnie zakazała go używać. | 0111-04-28

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 3 | ((210406-potencjalnie-eksterytorialny-seksbot; 210817-zgubiony-holokrysztal-w-lesie; 211221-chevaleresse-infiltruje-rekiny)) |
| Alan Bartozol        | 2 | ((210817-zgubiony-holokrysztal-w-lesie; 211221-chevaleresse-infiltruje-rekiny)) |
| Barnaba Burgacz      | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 211221-chevaleresse-infiltruje-rekiny)) |
| Franek Bulterier     | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 230325-ten-nawiedzany-i-ta-ukryta)) |
| Karolina Terienak    | 2 | ((211221-chevaleresse-infiltruje-rekiny; 230325-ten-nawiedzany-i-ta-ukryta)) |
| Aleksandra Burgacz   | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Cyryl Perikas        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Daniel Terienak      | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Diana Tevalier       | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Feliks Keksik        | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Henryk Wkrąż         | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Hestia d'Rekiny      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Julia Kardolin       | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Justynian Diakon     | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Kacper Bankierz      | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Laura Tesinik        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Lea Samszar          | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Liliana Bankierz     | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Lorena Gwozdnik      | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Mariusz Trzewń       | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Melissa Durszenko    | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Michał Klabacz       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Nadia Uprewien       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Oliwier Czepek       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Sabina Kazitan       | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Santino Mysiokornik  | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Staś Arienik         | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Stella Armadion      | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Tomasz Tukan         | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Triana Porzecznik    | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Żorż d'Namertel      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |