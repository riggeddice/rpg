---
categories: profile
factions: 
owner: public
title: Alicja Sowińska
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190123-skazenie-grazoniusza         | idealistyczna kapitan statku ASD Grazoniusz. Nieświadoma obecności Saitaera na swoim statku, została zainfekowana i dołączyła do sił Saitaera. | 0079-03-30 - 0079-04-01 |
| 190123-parszywa-ekspedycja          | kiedyś kapitan, teraz TAI Grazioniusza. "Porwana" przez ekspedycję ASD. | 0109-12-20 - 0109-12-22 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190123-parszywa-ekspedycja          | powróciła do Orbitera Pierwszego, ale jako TAI a nie czarodziejka. Nie wiadomo co z nią zrobią. Nie wiadomo co ona może zrobić. | 0109-12-22

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| ASD Grazoniusz       | 2 | ((190123-parszywa-ekspedycja; 190123-skazenie-grazoniusza)) |
| ASD Bubuta           | 1 | ((190123-parszywa-ekspedycja)) |