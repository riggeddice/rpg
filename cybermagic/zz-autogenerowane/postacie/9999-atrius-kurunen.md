---
categories: profile
factions: 
owner: public
title: Atrius Kurunen
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210728-w-cieniu-nocnej-krypty       | echo; dowódca Sarairen. Za jego dowodzenia Sarairen została przejęta przez Finis Vitae. | 0111-05-20 - 0111-06-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Romana Arnatin       | 2 | ((210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| AK Nocna Krypta      | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Arianna Verlen       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Eustachy Korkoran    | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Finis Vitae          | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Gerard Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Helena Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Janus Krzak          | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Oliwia Karelan       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Ulisses Kalidon      | 1 | ((210728-w-cieniu-nocnej-krypty)) |