---
categories: profile
factions: 
owner: public
title: Franciszek Korel
---

# {{ page.title }}


# Generated: 



## Fiszki


* <-- WHISPERED | @ 230124-kjs-wygrac-za-wszelka-cene
* (ENCAO:  -0+00 |Nie znosi być w centrum uwagi;;Cierpliwy| VALS: Universalism, Self-direction >> Face| DRIVE: Niestabilny geniusz Suspirii) | @ 230124-kjs-wygrac-za-wszelka-cene
* styl: Jurek | @ 230124-kjs-wygrac-za-wszelka-cene
* "Zwyciężymy, nieważne z czym mamy do czynienia. To tylko kwestia tego ile zapłacisz." | @ 230124-kjs-wygrac-za-wszelka-cene

### Wątki


przeznaczenie-eleny-verlen

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230307-kjs-stymulanty-szeptomandry  | miał romans z Martą i planował wykorzystać Żanetę jako ofiarę, ale ostatecznie został pokonany przez Apollo i jego żołnierzy. Źródło stymulantów od Szeptomandry w jaskiniach. | 0095-06-26 - 0095-06-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Apollo Verlen        | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| KDN Kajis            | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| Lidia Nemert         | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| Lucas Oktromin       | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| Marta Krissit        | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| Michał Waczarek      | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| Żaneta Krawędź       | 1 | ((230307-kjs-stymulanty-szeptomandry)) |