---
categories: profile
factions: 
owner: public
title: Daven Hassik
---

# {{ page.title }}


# Generated: 



## Fiszki


* 45 lat, specjalista od łączności, zbierania danych i informacji. | @ 230720-savaranie-przed-obliczem-nihilusa
* OCEAN: (O+C+A+): Analityczny, spokojny, skoncentrowany na szczegółach. "Informacje to nasza siła. Im więcej wiemy, tym lepiej możemy działać." | @ 230720-savaranie-przed-obliczem-nihilusa
* VALS: (Achievement, Security): "Chcę zebrać jak najwięcej informacji, żeby zapewnić naszej załodze bezpieczeństwo." | @ 230720-savaranie-przed-obliczem-nihilusa
* Core Wound - Lie: "Kiedyś nie zrozumiałem sygnału i przez to wpadliśmy w kłopoty" - "Jeśli będę uważnie słuchać i analizować, zapobiegnę wszelkim problemom." | @ 230720-savaranie-przed-obliczem-nihilusa
* Styl: Wycofany, poważny, zawsze skoncentrowany na swojej pracy. | @ 230720-savaranie-przed-obliczem-nihilusa

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230923-ciemnosc-pozerajaca-arcadalian | XO Ferrivata; rzeczowy ale przestraszony sytuacją i decyzjami kapitana, że nie można po niego teraz lecieć | 0091-07-30 - 0091-08-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aulus Terrentus      | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Iwo Bretonis         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Lester Martz         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Livia Sertiano       | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| OLU Luminarius       | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Salma Bluszcz        | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| SN Arcadalian        | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| SN Ferrivat          | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| SN Murisatia         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |