---
categories: profile
factions: 
owner: public
title: Sabrina Powsimrożek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180701-dwa-rejsy-w-potrzebie        | oficer polityczna Luxuritiasu na niezbyt dobrej klasy statku Krwawy Wojownik. Dba o interesy Luxuritiasu kosztem wszystkiego innego. | 0109-08-07 - 0109-08-11 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180701-dwa-rejsy-w-potrzebie        | na nią spadła całość winy za wydarzenia na Krwawym Wojowniku; w niełasce u szefostwa. Na dół hierarchii Luxuritiasu. | 0109-08-11

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Emilia Kariamon      | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Fabian Komczatkow    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Gerard Weiner        | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Kacper Wontarczyk    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Kalina Rotmistrz     | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Krystian Moborok     | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Liwia Barbaturran    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Marcjanna Maszotka   | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Martauron Attylla    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Tymon Barbaturran    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Wojciech Zajcew      | 1 | ((180701-dwa-rejsy-w-potrzebie)) |