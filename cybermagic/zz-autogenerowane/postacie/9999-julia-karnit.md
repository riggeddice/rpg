---
categories: profile
factions: 
owner: public
title: Julia Karnit
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221113-ailira-niezalezna-handlarka-woda | (ENCAO: +0--0 |Nie dba o to co inni czują;;Śmiała i przedsiębiorcza| VALS: Security, Face >> Achievement| DRIVE: Zatrzymać czas) (poluje na Kaspiana, kiedyś Orbiter, 36 lat) szuka morderczego advancera noktiańskiego który wygrał 1v6. Pokonał jej oddział i zabił jej brata. Podejrzewa, że szuka Kaspiana Certisariusa. Iga ją przekonała tymczasowo, że teraz go nie znajdzie i że to nie on. | 0090-06-22 - 0090-06-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ailira Niiris        | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Daria Czarnewik      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Filip Szukurkor      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Iga Mikikot          | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Jakub Uprzężnik      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Kaspian Certisarius  | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Leo Kasztop          | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Ludwik Trójkadur     | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Nastia Barbatov      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Patryk Lapszyn       | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Safira d'Hiyori      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |