---
categories: profile
factions: 
owner: public
title: Artemis Lawellan
---

# {{ page.title }}


# Generated: 



## Fiszki


* dowódczyni Opiekunów, huntress | @ 230425-klotnie-sasiadow-w-wanczarku
* styl: Chevill z MtG (arogancka, cicha w akcji, lots of bling); cel: zapewnić Mandragorze żywność | @ 230425-klotnie-sasiadow-w-wanczarku

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230425-klotnie-sasiadow-w-wanczarku | commander Opiekunów, nie podoba jej się jej przeznaczenie i życie, ale chce zdrowia i bezpieczeństwa rodziny więc się słucha. Żyje na uboczu. Bawiła się z myślą zabicia Karolinusa, ale zmieniła zdanie. Powiedziała Karolinusowi, co się dzieje w obszarze mandragor i szpitala (eliksir) i o sojuszu mrocznych Blakenbauerów i mrocznych Samszarów. | 0095-05-16 - 0095-05-19 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Artur Lawellan       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Damian Fenekis       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Elea Brzozecka       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Ilfons Lawellan      | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Karolinus Samszar    | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Olga Fenekis         | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |