---
categories: profile
factions: 
owner: public
title: OOn Tabester
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240204-skazona-symulacja-tabester   | long range simulation cruiser Oneiritalis, prowadzący zaawansowane symulacje i gry; ktoś na pokładzie użył Skażonego magitecha i wszystko poszło do piekła. Serbinius pomógł. | 0109-11-01 - 0109-11-04 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ewaryst Kajman       | 1 | ((240204-skazona-symulacja-tabester)) |
| Klaudia Stryk        | 1 | ((240204-skazona-symulacja-tabester)) |
| Lara Linven          | 1 | ((240204-skazona-symulacja-tabester)) |
| Martyn Hiwasser      | 1 | ((240204-skazona-symulacja-tabester)) |
| OO Serbinius         | 1 | ((240204-skazona-symulacja-tabester)) |
| TAI Ashtaria d'Tabester | 1 | ((240204-skazona-symulacja-tabester)) |