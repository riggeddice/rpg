---
categories: profile
factions: 
owner: public
title: Finis Vitae
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210728-w-cieniu-nocnej-krypty       | echo; straszliwy Lewiatan - autosent niszczący wszystko i mający jako jedyny cel koniec rzeczywistości. Opanował Sarairen i zastawił pułapkę na statek medyczny... | 0111-05-20 - 0111-06-06 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181227-adieu-cieniaszczycie         | po stworzeniu świątyni Arazille zapadł w sen, nieskończony sen nieskończonego zwycięstwa. Nojrepy dalej działają, ale bez jego intencji. | 0109-12-01

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Romana Arnatin       | 2 | ((210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| AK Nocna Krypta      | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Arianna Verlen       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Atrius Kurunen       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Eustachy Korkoran    | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Gerard Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Helena Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Janus Krzak          | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Oliwia Karelan       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Ulisses Kalidon      | 1 | ((210728-w-cieniu-nocnej-krypty)) |