---
categories: profile
factions: 
owner: public
title: Dalmjer Servart
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230614-atak-na-kidirona             | infiltrator; uszkodził BIA i wstrzyknął jej Malictrix, zniszczył sztab Kidirona, ma koloidowy sprzęt i chciał zabić Kidirona. Ucieka z porwaną Kalią. | 0093-03-22 - 0093-03-24 |
| 230621-infiltrator-ucieka-a-arkologia-plonie | prawdziwy sadystyczny potwór; zniszczył servar Eustachego pułapką i zranił Kalię, robiąc z niej żywy przykład co może się stać innym którzy stoją przeciw niemu. Zabrał Skorpiona z 4 ludźmi. Nikt nie wróci mimo jego obietnic - ale Kalii pozwolił odejść wolno. Eustachy tyle wynegocjował. | 0093-03-25 - 0093-03-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 2 | ((230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Bartłomiej Korkoran  | 2 | ((230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Eustachy Korkoran    | 2 | ((230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Kalia Awiter         | 2 | ((230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| OO Infernia          | 2 | ((230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Rafał Kidiron        | 2 | ((230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Ralf Tapszecz        | 2 | ((230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| BIA Prometeus        | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Laurencjusz Kidiron  | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Lycoris Kidiron      | 1 | ((230614-atak-na-kidirona)) |
| Małgorzata Maratelus | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| SAN Szare Ostrze     | 1 | ((230614-atak-na-kidirona)) |
| Stanisław Uczantor   | 1 | ((230614-atak-na-kidirona)) |