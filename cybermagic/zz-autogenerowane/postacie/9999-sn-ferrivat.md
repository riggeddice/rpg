---
categories: profile
factions: 
owner: public
title: SN Ferrivat
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230923-ciemnosc-pozerajaca-arcadalian | ciężki statek inżynieryjny; chciał ratować Arcadalian i prawie wpadł w kłopoty. Oddalił się od Arcadaliana z rozkazu kpt Aulusa. Nie został Skażony. | 0091-07-30 - 0091-08-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aulus Terrentus      | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Daven Hassik         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Iwo Bretonis         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Lester Martz         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Livia Sertiano       | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| OLU Luminarius       | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Salma Bluszcz        | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| SN Arcadalian        | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| SN Murisatia         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |