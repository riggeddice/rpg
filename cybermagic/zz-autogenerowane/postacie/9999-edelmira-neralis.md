---
categories: profile
factions: 
owner: public
title: Edelmira Neralis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211009-szukaj-serpentisa-w-lesie    | noktiański serpentis (bioformowany komandos); w stanie superciężkim i umierająca od magii; Ksenia ją stabilizowała, ale niewiele mogła zrobić. Tylko Szpital Terminuski w Pustogorze. Oddana w ręce astorian przez Udoma. | 0084-11-13 - 0084-11-14 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Agostino Karwen      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Dariusz Skórnik      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Felicjan Szarak      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Grzegorz Czerw       | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Klaudia Stryk        | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Ksenia Kirallen      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Mariusz Trzewń       | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Strażniczka Alair    | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Udom Rapnak          | 1 | ((211009-szukaj-serpentisa-w-lesie)) |