---
categories: profile
factions: 
owner: public
title: TAI XT-723 d'K1
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210630-listy-od-fanow               | proste TAI komunikacyjne. Powiedziało Klaudii że sygnał od Michała pochodził z ciemnych sektorów K1, nie ma tam repeatera i wszystko wskazuje że jest tam jakaś baza. Wierzy w powrót Zefiris. | 0112-01-15 - 0112-01-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((210630-listy-od-fanow)) |
| Bogdan Anatael       | 1 | ((210630-listy-od-fanow)) |
| Elena Verlen         | 1 | ((210630-listy-od-fanow)) |
| Izabela Zarantel     | 1 | ((210630-listy-od-fanow)) |
| Klaudia Stryk        | 1 | ((210630-listy-od-fanow)) |
| Michał Teriakin      | 1 | ((210630-listy-od-fanow)) |
| OE Lord Savaron      | 1 | ((210630-listy-od-fanow)) |
| Olgierd Drongon      | 1 | ((210630-listy-od-fanow)) |
| Rafael Galwarn       | 1 | ((210630-listy-od-fanow)) |
| Remigiusz Falorin    | 1 | ((210630-listy-od-fanow)) |
| TAI Rzieza d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI Zefiris          | 1 | ((210630-listy-od-fanow)) |