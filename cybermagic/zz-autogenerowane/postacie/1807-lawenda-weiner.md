---
categories: profile
factions: 
owner: public
title: Lawenda Weiner
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "weiner, pionierzy eteru"
* type: "NPC"
* owner: "public"
* title: "Lawenda Weiner"


## Postać

### Koncept (1)

Identyfikatorka, analityczka oraz "szlifierz" Skażenia - osoba przekształcająca magię 'naturalną' w cudowne, acz potencjalnie mało stabilne narzędzia. Badaczka artefaktów i Eteru Nieskończonego. Realizuje się robiąc domowe lody. Deckard Cain + Chemik ze "Ślepej Plamy".

### Otoczenie (1)

Frontier, miejsce mniej cywilizowane i mniej kontrolowane przez świat ludzi i magów. Obszar Skażony i potencjalnie niestabilny i czarowny. Obszar Eteru Nieskończonego. Ogólnie, miejsce typu "postapokaliptycznego" - w tamtym miejscu najlepiej czuje się w małym laboratorium czy warsztacie.

### Misja (1)

Skupia się na zrozumieniu, tworzeniu i wolności. Jako czarodziejka, chce by nie czarujący (lub magowie innego typu) mogli lepiej rozumieć i korzystać z owoców wiedzy i potęgi magii i czystego Eteru. Wierzy w postęp technologiczny i w sukces rasy ludzkiej i magów.

### Wada (-2)

Nie umie utrzymać sekretu. Zbyt skupia się na aplikacjach wiedzy i magii i zbyt mało skupia się na potencjalnych negatywnych zastosowaniach. Bardzo łatwo jest z niej wszystko wyciągnąć, sama to powie. Dodatkowo, niekoniecznie dostosowuje poziom wiedzy przekazywanej do poziomu odbiorcy.

### Motywacje (1)

| Co chce by się działo? Co jest pożądane?                 | Co jest antytezą postaci? Co jest jej negacją?               |
|----------------------------------------------------------|--------------------------------------------------------------|
| przekonać Borysa do tego, że magia i Skażenie to potencjalny postęp ludzkości | dać mu wygrać (że tylko jako zasilanie) |
| napisać Wielką Encyklopedię Eteru i Kuriozów, podnieść zrozumienie | odrzucić zdobycie wiedzy czy poznanie czegoś nowego |
| uczyć korzystania z eteru; uczyć radzenia sobie z magią  | pozwolić komuś ucierpieć przez magię z uwagi na niewiedzę |

### Zachowania (1)

| Jak zazwyczaj się zachowuje?                             | Pod wpływem jakich uczuć / w jakich okolicznościach to robi? |
|----------------------------------------------------------|--------------------------------------------------------------|
| oszczędna w słowach; lekko zarozumiała i belferska       | zazwyczaj, przy normalnych warunkach |
| krzyczy na ludzi; klnie jak szewc; apodyktyczna i władcza | gdy jest zdenerwowana lub się boi |
| ma maniery robota; płaski monotonny głos; nie zwraca na innych uwagi | gdy jest szczęśliwa lub nad czymś ciężko pracuje |

### Umiejętności (4)

* Nomad Skażenia, potrafi znaleźć schronienie, poruszać się, pożywić się i przetrwać na silnie Skażonych obszarach. Przetrwa sama.
* Badacz artefaktów i Eteru, Identyfikatorka; potrafi rozpoznawać i badać własności bytów przeróżnych dotykających Eteru i świata magii.
* Scrapper i artefaktor Eteru, łączy 'normalny' świat z rzeczami pochodzącymi z Eteru. Łączy rzeczy pochodzące z Eteru tworząc z nich nowe unikalne rzeczy. Da radę z tym co jest.
* Twórca lodów craftowych, od zbierania składników aż po finalne produkty. Jako efekt uboczny, robi też niezłe słodycze.

### Magia (2 LUB 4)

#### Gdy kontroluje energię

* Magia identyfikująca, badająca i klasyfikująca - potrafi odkryć prawdziwą naturę większości rzeczy jakich dotknęło Skażenie i jak je wykorzystać.
* Kataliza, artefakcja i tworzenie narzędzi - umiejętność przekształcenia 'wolnej' energii magicznej w dziwne i czarowne często narzędzia Eteryczne.
* Potrafi przekształcać własności energii magicznych, artefaktów, Skażenia, Skażeńców itp. w subtelny sposób - tak, by niby dalej robiły to samo, ALE.

#### Gdy traci kontrolę

* Gdy się cieszy lub jest pod wpływem pozytywnych uczuć, istniejące byty Eteryczne mogą ulec modyfikacji oraz mogą pojawić się nowe (niekoniecznie korzystne) Skażenia.
* Gdy się martwi lub boi, magia w jej pobliżu zaczyna zachowywać się bardzo chaotycznie - rzeczy stabilne destabilizują, zaklęcia się przekształcają...

### Zasoby i otoczenie

#### Ogólnie (4)

* Małe przenośne laboratorium do identyfikacji artefaktów i bytów Eteru
* Specjalnie uodporniony pojazd do poruszania się w Eterze / na terenach Skażenia, działający też jako pojazd do lodów
* Narzędzia i artefakty odstraszające i odwracające uwagę (zwłaszcza groźnych Skażeńców)
* Mąż i partner: Borys Perikas
* Silne umocowanie w społeczeństwie pionierów działających blisko rzeczy Skażonych i Eterów; znana i ceniona jako ekspert

#### Powiązane frakcje

{{ page.factions }}

## Opis

### Ogólnie

(27 min)

Lawenda i Borys tak długo rywalizowali ze swoimi podejściami, tak długo walczyli które z nich ma rację, że aż się w sobie w końcu zakochali i zostali parą. Teraz walczą dużo więcej, acz są szczęśliwsi.

### Motywacje


### Działanie


### Specjalne


### Magia


### Otoczenie


### Motto


## Historia


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180718-msciwa-ryba-z-eteru          | wybitna analityczka niezdolna do utrzymania sekretu; zidentyfikowała Majce wszystko oraz powiedziała Kalinie to samo wszystko potem. | 0109-08-25 - 0109-08-27 |
| 180730-prasyrena-z-zemsty           | w przyspieszonym tempie produkuje antidotum na popularny narkotyk kralotyczny. Identyfikator (zdalny) i producent (też zdalny). | 0109-08-29 - 0109-08-30 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kalina Rotmistrz     | 2 | ((180718-msciwa-ryba-z-eteru; 180730-prasyrena-z-zemsty)) |
| Anita Perczoluk      | 1 | ((180730-prasyrena-z-zemsty)) |
| Antoni Zajcew        | 1 | ((180730-prasyrena-z-zemsty)) |
| Borys Perikas        | 1 | ((180718-msciwa-ryba-z-eteru)) |
| Joachim Kozioro      | 1 | ((180718-msciwa-ryba-z-eteru)) |
| Kasandra Kirnał      | 1 | ((180730-prasyrena-z-zemsty)) |
| Magda Patiril        | 1 | ((180718-msciwa-ryba-z-eteru)) |
| Majka Perikas        | 1 | ((180718-msciwa-ryba-z-eteru)) |
| Maksymilian Supolont | 1 | ((180730-prasyrena-z-zemsty)) |
| Małgorzata Kirnał    | 1 | ((180730-prasyrena-z-zemsty)) |
| Stach Sosnowiecki    | 1 | ((180730-prasyrena-z-zemsty)) |