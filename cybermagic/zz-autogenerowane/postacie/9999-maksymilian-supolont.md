---
categories: profile
factions: 
owner: public
title: Maksymilian Supolont
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180730-prasyrena-z-zemsty           | dobrze się bawił w dominowanie kobiet na lewo i prawo aż dorwała go Prasyrena i odbiła z trudem Kalina. Obecnie, w niewoli Kaliny. | 0109-08-29 - 0109-08-30 |
| 180808-kultystka-z-milosci          | wszystko powiedział Kalinie co wiedział. Pionek Ośmiornicy. Poza tym - bez sensownej roli. | 0109-08-30 - 0109-09-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Zajcew        | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Kalina Rotmistrz     | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Kasandra Kirnał      | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Małgorzata Kirnał    | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Stach Sosnowiecki    | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Anita Perczoluk      | 1 | ((180730-prasyrena-z-zemsty)) |
| Feliks Weiner        | 1 | ((180808-kultystka-z-milosci)) |
| Kacper Pyszałnik     | 1 | ((180808-kultystka-z-milosci)) |
| Lawenda Weiner       | 1 | ((180730-prasyrena-z-zemsty)) |
| Patryk Paterecki     | 1 | ((180808-kultystka-z-milosci)) |