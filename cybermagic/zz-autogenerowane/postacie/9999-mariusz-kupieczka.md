---
categories: profile
factions: 
owner: public
title: Mariusz Kupieczka
---

# {{ page.title }}


# Generated: 



## Fiszki


* artysta AMZ podkochujący się w Lilianie (oryginalnie z małej mieściny w Przelotyku), kulturowo: drakolita / sybrianin | @ 230212-pierwszy-tajemniczy-wielbiciel-liliany
* (ENCAO:  ++-0+ |Niefrasobliwy;;Nieodpowiedzialny;;Ekstremalne pomysły| VALS: Achievement, Face >> Humility| DRIVE: Żyć jak celebryta) | @ 230212-pierwszy-tajemniczy-wielbiciel-liliany
* "Muszę jej zaimponować, bo na mnie nie zwróci uwagi", "nie poświęcisz -> nie stworzysz piękna" | @ 230212-pierwszy-tajemniczy-wielbiciel-liliany

### Wątki


tysiace-wojen-liliany

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230212-pierwszy-tajemniczy-wielbiciel-liliany | AMZ. 22 lata. Chciał zaimponować Lilianie i zdobyć jej serce, więc zrobił jej portret (technicznie, 3 z czego 2 musiał sprzedać). Niestety spartaczył i portrety wysysają ludzi poza silnym polem magicznym. Gdy Liliana go zaatakowała plugawymi słowami się szybko odkochał. | 0111-10-22 - 0111-10-24 |
| 240305-lea-strazniczka-lasu         | mag AMZ, który próbował pomóc Trianie i Gwiazdociszowi w archeologii z czasów wojny. Ucieszył się mogąc pracować z Rekinami. Ucierpiał walcząc z Horrorem. | 0111-10-21 - 0111-10-25 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230212-pierwszy-tajemniczy-wielbiciel-liliany | silna niechęć i złość wobec Liliany Bankierz. Był zakochany, ale mu przeszło. Nie jest dziewczyną z jaką chce mieć cokolwiek do czynienia. | 0111-10-24
| 230212-pierwszy-tajemniczy-wielbiciel-liliany | niewielkie wykroczenie za umożliwienie ukradzenia artefaktu przez ludzi. Dalej jest nieszczęśliwy, bo | 0111-10-24

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Triana Porzecznik    | 2 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany; 240305-lea-strazniczka-lasu)) |
| Arkadia Verlen       | 1 | ((240305-lea-strazniczka-lasu)) |
| Bogdan Gwiazdocisz   | 1 | ((240305-lea-strazniczka-lasu)) |
| Ernest Namertel      | 1 | ((240305-lea-strazniczka-lasu)) |
| Julia Kardolin       | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Karolina Terienak    | 1 | ((240305-lea-strazniczka-lasu)) |
| Keira Amarco d'Namertel | 1 | ((240305-lea-strazniczka-lasu)) |
| Laura Tesinik        | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Lea Samszar          | 1 | ((240305-lea-strazniczka-lasu)) |
| Liliana Bankierz     | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Malena Barandis      | 1 | ((240305-lea-strazniczka-lasu)) |
| Marek Samszar        | 1 | ((240305-lea-strazniczka-lasu)) |
| Marysia Sowińska     | 1 | ((240305-lea-strazniczka-lasu)) |
| Michał Klabacz       | 1 | ((240305-lea-strazniczka-lasu)) |