---
categories: profile
factions: 
owner: public
title: Jan Uszczar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190917-zagubiony-efemerydyta        | 17-latek, uzdolniony eternianin, nie chciał dołączyć do mafii i nie chciał żyć w Eterni. Przyzwał efemerydę w Pustogorze pod presją; w końcu wpadł w ręce Pięknotki i został oddany Orbiterowi. | 0110-06-21 - 0110-06-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Gabriel Ursus        | 1 | ((190917-zagubiony-efemerydyta)) |
| Marek Puszczok       | 1 | ((190917-zagubiony-efemerydyta)) |
| Olaf Zuchwały        | 1 | ((190917-zagubiony-efemerydyta)) |
| Pięknotka Diakon     | 1 | ((190917-zagubiony-efemerydyta)) |