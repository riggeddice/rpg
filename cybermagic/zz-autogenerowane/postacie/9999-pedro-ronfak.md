---
categories: profile
factions: 
owner: public
title: Pedro Ronfak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190928-ostatnia-misja-tarna         | eks-noktianin, który dołączył do mafii. Ma słabość do Talii; ogólnie, dba o swoją skórę ale dobrze chroni Talię. | 0110-01-27 - 0110-01-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| BIA Tarn             | 1 | ((190928-ostatnia-misja-tarna)) |
| Eustachy Mrownik     | 1 | ((190928-ostatnia-misja-tarna)) |
| Hestia d'Tiamenat    | 1 | ((190928-ostatnia-misja-tarna)) |
| Talia Aegis          | 1 | ((190928-ostatnia-misja-tarna)) |
| Wiktor Satarail      | 1 | ((190928-ostatnia-misja-tarna)) |