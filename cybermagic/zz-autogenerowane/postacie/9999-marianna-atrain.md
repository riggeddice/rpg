---
categories: profile
factions: 
owner: public
title: Marianna Atrain
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221220-dezerter-z-mrocznego-wolu    | inspektorka K1 szukająca informacji o dezercji szefa ochrony z Wołu; skupia się na aspekcie samego szefa ochrony i tego kim był i jak się zachowywał. Dużo pracy z TAI Semla i z szerszą populacją załogi. Skupia się na ofierze a potem na danych statystycznych i grupowych, z czego zbudowała kontekst i mapę. | 0111-11-15 - 0111-11-18 |
| 230103-protomag-z-mrocznego-wolu    | bada desanguanizator i widzi, że to nie Juliusz stoi za zabójstwem Wincentego. Po rozmowie z pilotem doszła, że science lab jest za dobry na Wole - tak, Nela coś robi dookoła magii i inhibicji. Doszła do tego, że Nela jest protomagiem. | 0111-11-19 - 0111-11-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adela Myrias         | 2 | ((221220-dezerter-z-mrocznego-wolu; 230103-protomag-z-mrocznego-wolu)) |
| Juliusz Akramantanis | 2 | ((221220-dezerter-z-mrocznego-wolu; 230103-protomag-z-mrocznego-wolu)) |
| Nadja Kilmodrian     | 2 | ((221220-dezerter-z-mrocznego-wolu; 230103-protomag-z-mrocznego-wolu)) |
| OO Mroczny Wół       | 2 | ((221220-dezerter-z-mrocznego-wolu; 230103-protomag-z-mrocznego-wolu)) |
| Baltazar Kwarcyk     | 1 | ((221220-dezerter-z-mrocznego-wolu)) |
| Krzysztof Workisz    | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Lena Ifirentis       | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Maja Szewieczak      | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Nela Kaltaner        | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Robert Warłomkacz    | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Tytus Muszczak       | 1 | ((221220-dezerter-z-mrocznego-wolu)) |