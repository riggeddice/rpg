---
categories: profile
factions: 
owner: public
title: Berdysz Rozpruwacz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220420-samobojstwo-kapitana-wielkiego-weza | pełni rolę advancera na Wielkim Wężu; dyskretnie przekonał Maję, by ta przejęła kontrolę nad Wielkim Wężem zamiast Kornelii i że Mai potrzebne jest jego wsparcie. | 0108-07-20 - 0108-07-26 |
| 220427-dziwne-strachy-w-morzu-ulud  | jako advancer doleciał do boi i rozszarpał swoim kombinezonem kalcynita. Jakkolwiek załoga Węża odkryła kim jest i są osoby aktywnie go zwalczające, zapewnił sobie na Wężu stronnictwo silnie stojące za nim. | 0108-07-29 - 0108-07-31 |
| 220518-okrutna-wrona-kalcynici-i-koszmary | wpadł na pintkę, rozwalił kalcynita i wrócił z pintką po wprowadzeniu dron; potem opracował jak wbić na Wronę i wyczyścić kalcynitów. Nie zdążył uratować Alana przed opętanym Antonim ale uratował Filipa i wsadził Antoniemu neuroobrożę. Użył eks-piratów z Wrony do upewnienia się, że sytuacja dalej jest pod jego kontrolą. Taktyka ORAZ masterful strategy. Uratował tyle ile się dało. | 0108-08-02 - 0108-08-11 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220518-okrutna-wrona-kalcynici-i-koszmary | wszyscy (Maja, Lila, Pola, Kornelia, Filip, NAWET Antoni, Ksawery i Mikołaj) uważają go za tego kto przeprowadził ich przez piekło. Ma czystą współpracę z agentami Węża. | 0108-08-11

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Falkam          | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Antoni Krutacz       | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Filip Gościc         | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Kornelia Sanoros     | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Lila Cziras          | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Maja Kormoran        | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Pola Mornak          | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Amanda Korel         | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Elwira Piscernik     | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Jerzy Odmiczak       | 1 | ((220420-samobojstwo-kapitana-wielkiego-weza)) |
| Ksawery Janowar      | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Mikołaj Faczon       | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |