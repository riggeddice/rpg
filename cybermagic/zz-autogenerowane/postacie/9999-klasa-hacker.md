---
categories: profile
factions: 
owner: public
title: Klasa Hacker
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240111-zlomowanie-legendarnej-anitikaplan | skutecznie połączył się z kapsułą ratunkową (i pośrednio z umysłem Pereza i Anitikaplan); potem zagregował dane ze wszystkich serwopancerzy i danych statku tworząc superstrumień. | 0084-04-02 - 0084-04-06 |
| 231202-protest-przed-kultem-na-szernief | włamała się do danych Kalisty i znalazła z jej pomocą dane o arystokratach, zidentyfikowała anomalne zachowania Frederico i przygotowała możliwość wartościowej ewakuacji arystokratów (by rozproszyć anomalię przez redukcję masy krytycznej energii). | 0103-10-15 - 0103-10-19 |
| 231213-polowanie-na-biosynty-na-szernief | zestawienie połączenia zdalnego z Malikiem, odkrycie danych z kamer: to savaranie atakowali biosynty pierwsi, śledzenie WSZYSTKICH ludzi by z szokiem dojść do tego kto jest zarażony, budowa fałszywych dowodów (że Agencja jest niewinna). | 0105-07-26 - 0105-07-31 |
| 231122-sen-chroniacy-kochankow      | Mery; zebrała informacje o przyczynie problemów (inwestorzy), wbiła się w dane Kalisty odkrywając jej hipotezy i potrafiła prześledzić ruchy Rovisa jak i nagrania z walki gdy savaranie odbijali Jolę-09. | 0105-09-02 - 0105-09-05 |
| 240117-dla-swych-marzen-warto       | poza standardową integracją strumieni danych, przekonała Kalistę do współpracy i containowała niebezpieczny Pierścień | 0106-11-04 - 0106-11-06 |
| 240214-relikwia-z-androida          | obrona przed psychotronicznym atakiem, zabezpieczając systemy stacji przed przejęciem przez Anomalię BIA. Też: znalezienie pacjenta zero Szernief i danych z dokumentacji, co tu się naprawdę działo i jak daleki zasięg ma Elwira. | 0107-05-16 - 0107-05-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kalista Surilik      | 5 | ((231122-sen-chroniacy-kochankow; 231202-protest-przed-kultem-na-szernief; 231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Oficer Naukowy | 5 | ((231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 240111-zlomowanie-legendarnej-anitikaplan; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Sabotażysta    | 4 | ((231213-polowanie-na-biosynty-na-szernief; 240111-zlomowanie-legendarnej-anitikaplan; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Vanessa d'Cavalis    | 4 | ((231202-protest-przed-kultem-na-szernief; 231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Aerina Cavalis       | 3 | ((231202-protest-przed-kultem-na-szernief; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Felina Amatanir      | 3 | ((231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Klasa Biurokrata     | 3 | ((231202-protest-przed-kultem-na-szernief; 231213-polowanie-na-biosynty-na-szernief; 240111-zlomowanie-legendarnej-anitikaplan)) |
| Klasa Dyplomata      | 3 | ((231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 240214-relikwia-z-androida)) |
| Mawir Hong           | 3 | ((231122-sen-chroniacy-kochankow; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Artur Tavit          | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Alina Mekran         | 1 | ((240214-relikwia-z-androida)) |
| Damian Orczakin      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Delgado Vitriol      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Dorion Fughar        | 1 | ((231122-sen-chroniacy-kochankow)) |
| Elwira Barknis       | 1 | ((240214-relikwia-z-androida)) |
| Estril Cavalis       | 1 | ((240117-dla-swych-marzen-warto)) |
| Frederico Zyklas     | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Jola-09 Szernief     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Klasa Inżynier       | 1 | ((240214-relikwia-z-androida)) |
| Klaudiusz Widar      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Larkus Talvinir      | 1 | ((240117-dla-swych-marzen-warto)) |
| Maia Sakiran         | 1 | ((240117-dla-swych-marzen-warto)) |
| Malik Darien         | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| OLU Luminarius       | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Rovis Skarun         | 1 | ((231122-sen-chroniacy-kochankow)) |
| Sebastian-194        | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| SN Anitikaplan       | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| SN Varilen           | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Szymon Alifajrin     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Tadeusz Mekran       | 1 | ((240214-relikwia-z-androida)) |
| Twaróg Worl          | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Wiktor Worl          | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |