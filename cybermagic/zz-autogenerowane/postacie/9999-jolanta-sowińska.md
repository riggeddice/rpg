---
categories: profile
factions: 
owner: public
title: Jolanta Sowińska
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210810-porwanie-na-gwiezdnym-motylu | twinnowana czarodziejka. Zupełnie nie wie jak rozmawiać z innymi. Powiedziała Flawii wszystko co mogła o Tomaszu i dała namiar na jego porywaczy i aktualną lokalizację. | 0108-12-25 - 0108-12-30 |
| 210813-szmuglowanie-antonelli       | świeżo hipersprzężona z TAI Elainka; wie o paśmie Kazimierza (bo słała kiedyś listy miłosne do Bruno Barana). Poleciała z Brightonem i Flawią do Pasa Kazimierza by ratować Antonellę; nabawiła się strachu przed skorpioidami XD. | 0109-02-11 - 0109-02-23 |
| 210820-fecundatis-w-domenie-barana  | autorka pomysłu użycia Antonelli i jej wiedzy o Esuriit jako baterii do przekształcenia skorpioidów; drugim torem zasilała Fecundatis swoją Elainką. Doszło do rozszczepienia ona - TAI. | 0109-03-06 - 0109-03-09 |
| 210921-przybycie-rekina-z-eterni    | jej ruchy mające uwolnić osoby spod kontroli Nataniela Morlana są zauważone przez Morlana i stanowią coraz więcej problemów dla rodu Sowińskich. Skonfliktowana politycznie z Amelią Sowińską. | 0111-07-20 - 0111-07-25 |
| 211012-torszecki-pokazal-kregoslup  | o dziwo, zaakceptowała plan Amelii Sowińskiej by pokazać Morlanowi krew "córki Morlana". Ten plan po prostu ma sens. | 0111-07-29 - 0111-07-30 |
| 210218-infernia-jako-goldarion      | nie pojawiła się jeszcze; kociła w przeszłości Tomasza Sowińskiego. Do niej przesłano zaawansowany antynanitkowy Entropik. Sama używa Tirakal/Morrigan. | 0111-09-16 - 0111-10-01 |
| 210317-arianna-podbija-asimear      | spotkała się kiedyś z kralothem na Asimear, została przezeń zdominowana. Porwana przez Ariannę, naprawiona przez Martyna - ale kralotycznie uzależniona. | 0111-10-18 - 0111-11-02 |
| 210421-znudzona-zaloga-inferni      | pod wpływem środków i niestabilna unieszkodliwiła Martyna magią, po czym zapolowała na nią Leona i ją szybko zdjęła. | 0111-11-16 - 0111-11-19 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210813-szmuglowanie-antonelli       | boi się skorpioidów... po podróży Fecundatis z Antonellą nic dziwnego że ma pomniejszą fobię. | 0109-02-23
| 210813-szmuglowanie-antonelli       | Brighton wyjaśnił jej, że ucieczka przed strachem to ucieczka. Nie warto oddać TAI kontroli nad sobą. Uwrażliwiona. Nie odda kontroli. PLUS - zainteresowana programem kosmicznym by inne tak nie cierpiały jak Flawia. | 0109-02-23
| 210820-fecundatis-w-domenie-barana  | rozszczepienie Jolanta - Elainka; Jolanta jest niezborna i ma problemy z dostępem do swojej magii / niektórych funkcji. | 0109-03-09
| 210317-arianna-podbija-asimear      | usunięto jej pasożyta kralotycznego i odbudowano połączenie z TAI, ale jest skrajnie uzależniona kralotycznie. | 0111-11-02
| 210818-siostrzenica-morlana         | jej plany / wiedza odnośnie programu kosmicznego Aurum zostały przekazane Kramerowi a jej reputacja w Aurum poważnie uszkodzona, że "dała cynk". | 0112-01-24
| 210818-siostrzenica-morlana         | wisi Ariannie Verlen spory dług wdzięczności za ratunek Ofelii. Tomasz zawarł dług, ale Jolanta będzie honorować. | 0112-01-24

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Tomasz Sowiński      | 5 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli)) |
| Antonella Temaris    | 3 | ((210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Arianna Verlen       | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Elena Verlen         | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Eustachy Korkoran    | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Flawia Blakenbauer   | 3 | ((210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Klaudia Stryk        | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Leona Astrienko      | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Martyn Hiwasser      | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Amelia Sowińska      | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Bruno Baran          | 2 | ((210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Cień Brighton        | 2 | ((210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Karolina Terienak    | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Lucjan Sowiński      | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Marysia Sowińska     | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Nataniel Morlan      | 2 | ((210810-porwanie-na-gwiezdnym-motylu; 210921-przybycie-rekina-z-eterni)) |
| Rafał Torszecki      | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Antoni Kramer        | 1 | ((210218-infernia-jako-goldarion)) |
| Antos Kuramin        | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Arkadia Verlen       | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Damian Szczugor      | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Deneb Ira            | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Elsa Kułak           | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Ernest Namertel      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Franek Kuparał       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Kara Szamun          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Karol Pustak         | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Lena Fenatil         | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Leon Kantor          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Lucjusz Blakenbauer  | 1 | ((210813-szmuglowanie-antonelli)) |
| Martyna Bianistek    | 1 | ((210317-arianna-podbija-asimear)) |
| Morrigan d'Tirakal   | 1 | ((210421-znudzona-zaloga-inferni)) |
| OO Infernia          | 1 | ((210218-infernia-jako-goldarion)) |
| Otto Azgorn          | 1 | ((210421-znudzona-zaloga-inferni)) |
| Persefona d'Infernia | 1 | ((210421-znudzona-zaloga-inferni)) |
| Renata Szarżun       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Rick Varias          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| SC Fecundatis        | 1 | ((210813-szmuglowanie-antonelli)) |
| SCA Płetwal Błękitny | 1 | ((210317-arianna-podbija-asimear)) |
| Sensacjusz Diakon    | 1 | ((211012-torszecki-pokazal-kregoslup)) |
| SLX Gwiezdny Motyl   | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Tamara Mardius       | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Tomasz Tukan         | 1 | ((210921-przybycie-rekina-z-eterni)) |