---
categories: profile
factions: 
owner: public
title: Nela Kaltaner
---

# {{ page.title }}


# Generated: 



## Fiszki


* science; śliczna i wycofana drakolitka | @ 221220-dezerter-z-mrocznego-wolu
* ENCAO: 0+-00 |Zwiewna i niestała;;Nikomu nie ufa| VALS: Self-direction, Conformity, | DRIVE: Ukryć sekret (GUARDIAN) | @ 221220-dezerter-z-mrocznego-wolu
* "odkryję to, o co chodzi" | @ 221220-dezerter-z-mrocznego-wolu

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230103-protomag-z-mrocznego-wolu    | śliczna drakolitka szukająca rozwiązania swojej natury protomaga i Tego Co Ją Chroni. Została zabrana na Orbiter - bo jak magia eksploduje, to będzie katastrofa. I zamiast do więzienia to do instytutu - by kontrolowała swoją energię magiczną. | 0111-11-19 - 0111-11-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adela Myrias         | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Juliusz Akramantanis | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Krzysztof Workisz    | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Lena Ifirentis       | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Maja Szewieczak      | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Marianna Atrain      | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Nadja Kilmodrian     | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| OO Mroczny Wół       | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Robert Warłomkacz    | 1 | ((230103-protomag-z-mrocznego-wolu)) |