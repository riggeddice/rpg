---
categories: profile
factions: 
owner: public
title: Franek Kuparał
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210810-porwanie-na-gwiezdnym-motylu | przełożony Strażniczki na Gwiezdnym Motylu. Uzależniony od dobrego towaru Szmuglerki, pomógł dziewczynom znaleźć Ducha. | 0108-12-25 - 0108-12-30 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antonella Temaris    | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Flawia Blakenbauer   | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Jolanta Sowińska     | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Lena Fenatil         | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Nataniel Morlan      | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Renata Szarżun       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| SLX Gwiezdny Motyl   | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Tomasz Sowiński      | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |