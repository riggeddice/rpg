---
categories: profile
factions: 
owner: public
title: Diana Tevalier
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "elisquid, multivirt"
* owner: "public"
* title: "Diana Tevalier"


## Postać

### Ogólny pomysł (2)

* W multivirt gra supportem; nazwa kodowa Chevaleresse. Członkini EliSquid. Bezwzględnie oddana Alanowi i gildii.
* Scavenger i dziecko ulicy. Potrafi przetrwać w praktycznie każdym mieście. Świetnie się chowa.
* Aktorka. Potrafi grać dużo młodszą niż jest. Wykorzystuje swoje umiejętności do przebierania się i maskowania.
* Kucharka. Jest świetną kucharką, potrafi zrobić niesamowicie smaczne jedzenie przy posiadaniu prawie niczego.

### Wyróżnik (2)

* Poszerzony hipernet; z każdego miejsca potrafi wejść w multivirt. W multivirt jest skuteczniejsza i lepsza niż w realu.
* Koszmary i strach. Diana jest świetna pod względem przerażania wszystkich. Ma wyjątkowo wiarygodne obrazy. Dużo widziała.
* Potrafi się schować. Ma duży talent do chowania się i pozostawania niezauważoną.

### Czego chce od innych (2)

* Niech inni też zobaczą, że multivirt i świat rzeczywisty są realnym światem - niech dla nich te światy się połączą.
* Niech słabsi od niej są chronieni. Jest skłonna sama ich chronić - niech nie mają takich koszmarnych obrazów do emisji jak ona.
* Wolność. Całkowita autonomia, swoboda poruszania się i możliwość robienia co tylko chce.
* Oddaje lojalność całą sobą i bezgranicznie. Oczekuje podobnej lojalności.

### W jaki sposób działa (2)

* Żywioł Ziemi.
* Strasznie uparta; nie boi się stanąć naprzeciw większym siłom od siebie. Immovable object. Bardzo wytrzymała.
* Pomocna; chroni oraz opiekuje się innymi. Przede wszystkim wsparcie lub ochotniczka do misji gdzie nikt nie chce iść.

### Zasoby i otoczenie (3)

* Spokrewniona ze znaną oficer, Teresą Tevalier. Przez to niektórzy jej idą lekko na rękę.

### Magia (3)

#### Gdy kontroluje energię

* Ukrywanie się i maskowanie: ukrywanie się, maskowanie, makijaż, iluzje... wszystko, by nie dało się dostrzec drobnej Chevaleresse.
* Koszmary i odstraszanie: odwracanie uwagi, terror, zabezpieczenie terenu. Bonus: przed owadami i dziką zwierzyną.
* Stabilizacja i przetrwanie: modyfikacja metabolizmu, przetrwanie w trudnych warunkach, podtrzymanie życia.
* Przyprawy i gotowanie: potrafi robić świetne i bardzo smaczne jedzenie.

#### Gdy traci kontrolę

* Multivirt i świat rzeczywisty stają się jednością

### Powiązane frakcje

{{ page.factions }}

## Opis

(xx min)


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190217-chevaleresse                 | w grze "Chevaleresse", support, 16-latka a wygląda na 13. Przybyła na Astorię z Anathi by odnaleźć Alana, zaatakowała Marlenę, wpadła na Tymona i w końcu udało jej się odnaleźć Alana. | 0110-02-25 - 0110-02-27 |
| 190330-polowanie-na-ataienne        | dzielnie walczyła z magami z Zaczęstwa broniąc Ataienne (sprowadzonym sprzętem Alana), za co ów ją uziemił (szlaban). Nie dała się, uciekła do multivirtu. | 0110-03-03 - 0110-03-04 |
| 190331-kurz-po-ataienne             | straciła zaufanie do Alana (bo ją uziemił). Rozważała opuszczenie tego terenu, ale Pięknotka ją przekonała do pozostania. | 0110-03-07 - 0110-03-09 |
| 190429-sabotaz-szeptow-elizy        | maleństwo broniące wojskowych, straciła nerwy i uderzyła kolegę za co dostała lekki wpierdol; powiedziała Pięknotce o Elizie i ogólnie, jak na nią, wyjątkowo dorosła. | 0110-04-10 - 0110-04-13 |
| 190830-kto-wrobil-alana             | chciała się pochwalić artefaktem który posłużył do wrobienia Alana. Bardzo przeprosiła Talię i posłużyła jako przynętę przeciwko magom Aurum dla Pięknotki. | 0110-06-03 - 0110-06-05 |
| 190901-polowanie-na-pieknotke       | porwana przez Józefa, wykorzystana by Alan się poddał. Nie straciła przytomności umysłu - weszła w virt i zaalarmowała Pięknotkę o problemie. | 0110-06-21 - 0110-06-24 |
| 200202-krucjata-chevaleresse        | zrozpaczona sytuacją z Alanem, zrobiła coś niewłaściwego - uzbroiła jego bronią Liberitias oraz weszła w sojusz z Ataienne. Najpewniej to nie koniec problemów. | 0110-07-24 - 0110-07-27 |
| 200222-rozbrojenie-bomby-w-kalbarku | przyczyna wojny Liberatis - Kardamacz, zmanipulowana kralotycznie przez Kardamacza i doń dołączyła. Skończyła przez Pięknotkę w szpitalu. | 0110-07-27 - 0110-08-01 |
| 200524-dom-dla-melindy              | Pięknotka ją przekonała, że jest taka Melinda która potrzebuje opieki i ma przechlapane. Sama Chevaleresse przekonała Alana że tak ma być. Alan nie podejrzewa Pięknotki. | 0110-09-13 - 0110-09-16 |
| 200913-haracz-w-parku-rozrywki      | świetnie się bawiła w parku rozrywki Janus; tam Dotknęła Kultu Ośmiornicy. Pomogła Alanowi w dyskretnym połączeniu z TAI Hestia d'Janus. | 0110-10-09 - 0110-10-10 |
| 211221-chevaleresse-infiltruje-rekiny | 17 lat (ale mówi 18, wygląda 15). Zinfiltrowała Rekiny (po raz kolejny), by znaleźć Stasia Arienika. Jeśli tu go nie ma, Alan będzie szukał bliżej Trzęsawiska, a na to ona się nie godzi. Powiedziała prawdę Karo i Marysi. Używa zrobionych przez siebie ciasteczek by być słodsza... lub podać truciznę XD. | 0111-09-06 - 0111-09-07 |
| 220111-marysiowa-hestia-rekinow     | nie ustąpiła w sprawie Melissy; doprowadziła do tego że Karolina przechwyciła Melissę i rozbiła Kult Ośmiornicy. Rękoma ETERNI. | 0111-09-16 - 0111-09-19 |
| 220814-stary-kot-a-trzesawisko      | skrajnie chroni Alana, ale jak usłyszała, że problem to ludzie na Trzęsawisku, dała Karo się z Alanem skomunikować. | 0111-10-12 - 0111-10-13 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190217-chevaleresse                 | Alan stał się jej prawnym opiekunem. | 0110-02-27
| 200913-haracz-w-parku-rozrywki      | zainteresowana Kultem Ośmiornicy; poczuła jego potęgę. Ona chce rząd dusz i wspierać innych; ludzie Kultu to mają, czemu ona nie może? | 0110-10-10

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 10 | ((190217-chevaleresse; 190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 190429-sabotaz-szeptow-elizy; 190830-kto-wrobil-alana; 190901-polowanie-na-pieknotke; 200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku; 200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Alan Bartozol        | 9 | ((190217-chevaleresse; 190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 190429-sabotaz-szeptow-elizy; 190830-kto-wrobil-alana; 190901-polowanie-na-pieknotke; 200913-haracz-w-parku-rozrywki; 211221-chevaleresse-infiltruje-rekiny; 220814-stary-kot-a-trzesawisko)) |
| Ataienne             | 4 | ((190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Lucjusz Blakenbauer  | 4 | ((190330-polowanie-na-ataienne; 190429-sabotaz-szeptow-elizy; 190901-polowanie-na-pieknotke; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Mariusz Trzewń       | 4 | ((190901-polowanie-na-pieknotke; 200202-krucjata-chevaleresse; 200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Karolina Terienak    | 3 | ((211221-chevaleresse-infiltruje-rekiny; 220111-marysiowa-hestia-rekinow; 220814-stary-kot-a-trzesawisko)) |
| Minerwa Metalia      | 3 | ((190429-sabotaz-szeptow-elizy; 190901-polowanie-na-pieknotke; 200202-krucjata-chevaleresse)) |
| Hestia d'Rekiny      | 2 | ((211221-chevaleresse-infiltruje-rekiny; 220111-marysiowa-hestia-rekinow)) |
| Karolina Erenit      | 2 | ((190217-chevaleresse; 190830-kto-wrobil-alana)) |
| Marysia Sowińska     | 2 | ((211221-chevaleresse-infiltruje-rekiny; 220111-marysiowa-hestia-rekinow)) |
| Mateusz Kardamacz    | 2 | ((200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Melinda Teilert      | 2 | ((200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Talia Aegis          | 2 | ((190830-kto-wrobil-alana; 200202-krucjata-chevaleresse)) |
| Tymon Grubosz        | 2 | ((190217-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Adam Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Aleksander Rugczuk   | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Aleksandra Szklarska | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Ariela Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Atena Sowińska       | 1 | ((190330-polowanie-na-ataienne)) |
| Barnaba Burgacz      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Bartłomiej Małczarek | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Damian Orion         | 1 | ((200202-krucjata-chevaleresse)) |
| Diana Lemurczak      | 1 | ((200524-dom-dla-melindy)) |
| Eliza Ira            | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Ernest Namertel      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Erwin Galilien       | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Franciszek Owadowiec | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Franek Bulterier     | 1 | ((200524-dom-dla-melindy)) |
| Gerwazy Lemurczak    | 1 | ((200524-dom-dla-melindy)) |
| Iwan Zawtrak         | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Jeremi Sowiński      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Józef Małmałaz       | 1 | ((190901-polowanie-na-pieknotke)) |
| Justynian Diakon     | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Karla Mrozik         | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Karol Senonik        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Keira Amarco d'Namertel | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Keraina d'Orion      | 1 | ((200202-krucjata-chevaleresse)) |
| Kot Rozbójnik        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Krystyna Senonik     | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Laura Tesinik        | 1 | ((200524-dom-dla-melindy)) |
| Liliana Bankierz     | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Maja Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Malictrix d'Itaran   | 1 | ((200202-krucjata-chevaleresse)) |
| Marian Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Marlena Maja Leszczyńska | 1 | ((190217-chevaleresse)) |
| Melissa Durszenko    | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Olaf Zuchwały        | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Rafał Kamaron        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Romeo Węglas         | 1 | ((190331-kurz-po-ataienne)) |
| Rupert Mysiokornik   | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Santino Mysiokornik  | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Staś Arienik         | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Szymon Oporcznik     | 1 | ((190331-kurz-po-ataienne)) |
| Tomasz Tukan         | 1 | ((200202-krucjata-chevaleresse)) |
| Urszula Miłkowicz    | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Viirai d'Lechotka    | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Wojciech Zermann     | 1 | ((190830-kto-wrobil-alana)) |
| Żorż d'Namertel      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |