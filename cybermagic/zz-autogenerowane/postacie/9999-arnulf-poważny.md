---
categories: profile
factions: 
owner: public
title: Arnulf Poważny
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230913-operacja-spotkac-sie-z-dmitrim | vigilante z Aurum; ze swoją grupką poluje na noktian i próbuje ich odstawić do bezpiecznego miejsca LUB ukarać za zbrodnie wojenne. Polował na Amandę i Xaverę, ale mimo świetnie zastawionej pułapki i sprzętu nie poradził sobie z ich determinacją i tym że są homo superior. They suffered more than he expected ;-). | 0081-06-28 - 0081-06-30 |
| 210926-nowa-strazniczka-amz         | 36 lat; nowy dyrektor AMZ (poprzedni zginął na wojnie). Ma jakąś przeszłość z siłami specjalnymi Pustogoru. Współpracuje z Tymonem i Talią nad zregenerowaniem hybrydowej Eszary jako Strażniczki Alair. Nie ufa noktiance, ale skłonny zaryzykować. | 0084-06-14 - 0084-06-26 |
| 211010-ukryta-wychowanka-arnulfa    | przyznał się Talii i Tymonowi do tego, że ukrywa małą noktiankę (Teresę Mieralit). Wygnał terminusa Saszę z terenu szkoły, robiąc sobie w nim wroga. Samemu zestrzelił serię dron Strażniczki. | 0084-12-11 - 0084-12-12 |
| 211017-nastolatka-w-bieliznie-na-dachu-w-burzy | przespał infiltrację AMZ przez Saszę. Gdy się obudził, Teresa była podejrzana o prostytucję i Klaudia + Ksenia chciały ją w pokoju. Arnulf bardzo starał się nie łączyć tych zdarzeń i dał im trzyosobowy pokój... | 0084-12-14 - 0084-12-15 |
| 211019-czarodziejka-ktora-jednak-moze-sie-zabic | opiekuje się Teresą; dał jej ubrania po córce. Zgodził się na plan Klaudii by zapewnić AMZ kadrę ze strony Weteranów. Wyraźnie przeładowany. | 0084-12-20 - 0084-12-24 |
| 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie | 38 lat w tej chronologii. Gdy Klaudia i Mariusz przyszli doń z planem ratowania dzieci, zaproponował plan alternatywny; wykorzystał AMZ do produkcji potworów i wraz z Tymonem wkradł się do obozu. Wspierał Zespół, ale nie pozwoli by jego podwładni ucierpieli. | 0085-01-26 - 0085-01-28 |
| 180929-dwa-tygodnie-szkoly          | dyrektor szkoły magów. Dobry mag, któremu zależy na uczniach (min Felicji). Próbuje współpracować z opiekunami i chronić uczniów. | 0109-09-17 - 0109-09-19 |
| 181027-terminuska-czy-kosmetyczka   | skutecznie osłonił reputację Ateny; nie miał pojęcia, że jego uczeń uczestniczył w akcji wymierzonej w Atenę. | 0109-10-07 - 0109-10-11 |
| 190113-chronmy-karoline-przed-uczniami | dyrektor skłonny do osłony swoich uczniów nawet, jeśli coś zbroili. Będzie współpracował z Pięknotką, bo udowodniła, że zależy jej na dobru a nie prawie. | 0110-01-05 - 0110-01-06 |
| 190206-nie-da-sie-odrzucic-mocy     | mimo zagrożeń i niebezpieczeństw, da Karolinie Erenit szansę i pomoże jej w AMZ. Będzie się nią opiekował. I przygarnie też Marlenę Maję by pomóc Karolinie. | 0110-02-17 - 0110-02-20 |
| 190519-uciekajacy-seksbot           | dyrektor, który przedkłada potencjalnie żywego seksbota nad pozycję i nad zadowolenie Ernesta Kajrata z mafii. | 0110-04-25 - 0110-04-26 |
| 190820-liliana-w-swiecie-dokumentow | dyrektor siwiejący przez beztroskie działania Liliany. Próbuje zapewnić by interesy wszystkich były spełnione - co rzadko wychodzi tak jak by chciał. | 0110-07-04 - 0110-07-08 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211010-ukryta-wychowanka-arnulfa    | ma wroga w Saszy Morwowcu (terminus). Uważa go za ko-konspiratora Tymona. | 0084-12-12
| 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie | zbudował silniejszy link z Tymonem Gruboszem; wspólnie uczestniczyli w operacji niszczenia porywaczy dzieci. | 0085-01-28
| 190113-chronmy-karoline-przed-uczniami | będzie współpracował z Pięknotką jako terminuską zanim z jakimkolwiek innym terminusem. | 0110-01-06
| 190113-chronmy-karoline-przed-uczniami | stracił trochę szacunku w oczach innych uczniów; myślą, że nie rozpoznał Pięknotki jako terminuski. Cóż. | 0110-01-06

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Teresa Mieralit      | 6 | ((190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Klaudia Stryk        | 5 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Pięknotka Diakon     | 5 | ((180929-dwa-tygodnie-szkoly; 181027-terminuska-czy-kosmetyczka; 190113-chronmy-karoline-przed-uczniami; 190206-nie-da-sie-odrzucic-mocy; 190519-uciekajacy-seksbot)) |
| Tymon Grubosz        | 5 | ((190206-nie-da-sie-odrzucic-mocy; 190820-liliana-w-swiecie-dokumentow; 210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Ksenia Kirallen      | 4 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Adela Kirys          | 3 | ((180929-dwa-tygodnie-szkoly; 181027-terminuska-czy-kosmetyczka; 190113-chronmy-karoline-przed-uczniami)) |
| Liliana Bankierz     | 3 | ((190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 190820-liliana-w-swiecie-dokumentow)) |
| Mariusz Trzewń       | 3 | ((210926-nowa-strazniczka-amz; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Talia Aegis          | 3 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Ignacy Myrczek       | 2 | ((180929-dwa-tygodnie-szkoly; 181027-terminuska-czy-kosmetyczka)) |
| Karolina Erenit      | 2 | ((190113-chronmy-karoline-przed-uczniami; 190206-nie-da-sie-odrzucic-mocy)) |
| Minerwa Metalia      | 2 | ((181027-terminuska-czy-kosmetyczka; 190206-nie-da-sie-odrzucic-mocy)) |
| Napoleon Bankierz    | 2 | ((180929-dwa-tygodnie-szkoly; 190113-chronmy-karoline-przed-uczniami)) |
| Sasza Morwowiec      | 2 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Strażniczka Alair    | 2 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Adela Pieczar        | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Albert Kalandryk     | 1 | ((210926-nowa-strazniczka-amz)) |
| Amanda Kajrat        | 1 | ((230913-operacja-spotkac-sie-z-dmitrim)) |
| Atena Sowińska       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Brygida Maczkowik    | 1 | ((181027-terminuska-czy-kosmetyczka)) |
| Dmitri Karpov        | 1 | ((230913-operacja-spotkac-sie-z-dmitrim)) |
| Eliza Ira            | 1 | ((190519-uciekajacy-seksbot)) |
| Ernest Kajrat        | 1 | ((190519-uciekajacy-seksbot)) |
| Erwin Galilien       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Felicja Melitniek    | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Felicjan Szarak      | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Kaella Sarimanis     | 1 | ((231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Kasjopea Maus        | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Kornel Garn          | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Marlena Maja Leszczyńska | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Maryla Koternik      | 1 | ((211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Miedwied Zajcew      | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Ossidia Saitis       | 1 | ((190519-uciekajacy-seksbot)) |
| Petra Karpov         | 1 | ((230913-operacja-spotkac-sie-z-dmitrim)) |
| Roland Grzymość      | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Saitaer              | 1 | ((190519-uciekajacy-seksbot)) |
| Szymon Jaszczurzec   | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Tadeusz Kruszawiecki | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Tomasz Tukan         | 1 | ((190519-uciekajacy-seksbot)) |
| Waldemar Grzymość    | 1 | ((211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Xavera Sirtas        | 1 | ((230913-operacja-spotkac-sie-z-dmitrim)) |