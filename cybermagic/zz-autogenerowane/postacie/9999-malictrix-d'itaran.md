---
categories: profile
factions: 
owner: public
title: Malictrix d'Itaran
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200202-krucjata-chevaleresse        | ixioński pasożyt TAI, sformowana przez Minerwę do zniszczenia kompleksu Itaran. Wygra i wsadzi Grzymościa za kratki. Okrutna, poza kontrolą, pożera wszystko. | 0110-07-24 - 0110-07-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ataienne             | 1 | ((200202-krucjata-chevaleresse)) |
| Damian Orion         | 1 | ((200202-krucjata-chevaleresse)) |
| Diana Tevalier       | 1 | ((200202-krucjata-chevaleresse)) |
| Keraina d'Orion      | 1 | ((200202-krucjata-chevaleresse)) |
| Mariusz Trzewń       | 1 | ((200202-krucjata-chevaleresse)) |
| Mateusz Kardamacz    | 1 | ((200202-krucjata-chevaleresse)) |
| Minerwa Metalia      | 1 | ((200202-krucjata-chevaleresse)) |
| Pięknotka Diakon     | 1 | ((200202-krucjata-chevaleresse)) |
| Talia Aegis          | 1 | ((200202-krucjata-chevaleresse)) |
| Tomasz Tukan         | 1 | ((200202-krucjata-chevaleresse)) |