---
categories: profile
factions: 
owner: public
title: Kinga Kruk
---

# {{ page.title }}


# Read: 

## Metadane

* factions: ""
* owner: "public"
* title: "Kinga Kruk"


## Kim jest

### Paradoksalny Koncept

Artystka - fałszerz, pałająca ogromnym szacunkiem do oryginałów.

### Motto

"Skoro chcesz mieć coś ładnego to ci to dam w sensownej cenie. Nie każdy potrzebuje w życiu oryginału."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Tworzenie wiarygodnych imitacji i wszelkiego rodzaju oszustwa. 
* ATUT: Praca z dokumentami, wyszukiwanie ukrytych faktów.
* SŁABA: Walka. Nie i już.
* SŁABA: 

### O co walczy (3)

* ZA: Każdy załuguje na coś pięknego w życiu, a ja przy tym mogę zarobić.
* ZA: 
* VS: Kradzieże i handel skradzionymi oryginałami.
* VS: 

### Znaczące Czyny (3)

* Jedno z jej "okultystycznych" dzieł wisi w miejscowym ratuszu.
* 

### Kluczowe Zasoby (3)

* COŚ: Terenówka - czasem żeby zdobyć składniki trzeba pojechać w dziwne miejsca.
* KTOŚ: Sieć zaprzyjaźnionych dostawców elementów, składników itp do produkcji fałszywek.
* WIEM: 
* OPINIA: Najbardziej praworządna obywatelka w historii.

## Inne

### Wygląd



### Coś Więcej



# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200423-lojalna-zdrajczyni           | zza kulis przygotowuje różnego rodzaju przedmioty którymi odwraca uwagę lub przekupuje niewłaściwe osoby. | 0109-07-21 - 0109-07-24 |
| 200507-anomalna-figurka-zabboga     | stworzyła perfekcyjne fikcyjne figurki, niszcząc jakiekolwiek możliwości użycia oryginalnych przez kogokolwiek - i dodała tracker by odnaleźć Tukana w tle. | 0109-09-19 - 0109-09-26 |
| 200513-trzyglowec-kontra-melinda    | mistrzyni robienia fałszywek - zrobiła taką fałszywkę, że nawet mag się nie zorientował że jest bezwartościowa. Plus, znalazła w hipernecie co to jest infiltrator. | 0109-11-08 - 0109-11-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Jan Łowicz           | 3 | ((200423-lojalna-zdrajczyni; 200507-anomalna-figurka-zabboga; 200513-trzyglowec-kontra-melinda)) |
| Melinda Teilert      | 3 | ((200423-lojalna-zdrajczyni; 200507-anomalna-figurka-zabboga; 200513-trzyglowec-kontra-melinda)) |
| Natasza Aniel        | 3 | ((200423-lojalna-zdrajczyni; 200507-anomalna-figurka-zabboga; 200513-trzyglowec-kontra-melinda)) |
| Katja Nowik          | 2 | ((200423-lojalna-zdrajczyni; 200513-trzyglowec-kontra-melinda)) |
| Adam Cześń           | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Damian Polwonien     | 1 | ((200507-anomalna-figurka-zabboga)) |
| Diana Lemurczak      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Duży Tom             | 1 | ((200423-lojalna-zdrajczyni)) |
| Felicja Melitniek    | 1 | ((200423-lojalna-zdrajczyni)) |
| Feliks Keksik        | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Gustaf Profnos       | 1 | ((200507-anomalna-figurka-zabboga)) |
| Mariusz Grabarz      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Rafał Torszecki      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Tomasz Tukan         | 1 | ((200507-anomalna-figurka-zabboga)) |