---
categories: profile
factions: 
owner: public
title: Melinda Teilert
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200423-lojalna-zdrajczyni           | młoda bogaczka z Aurum (nie czarodziejka), która chce dołączyć do Janka i być groźnym łowcą potworów. Lub ludzi. Nieważne - chce być groźna | 0109-07-21 - 0109-07-24 |
| 200507-anomalna-figurka-zabboga     | nie jest tylko głupią stalkerką, wyrolowała Jana przez kooperację z Nataszą; okazuje się, że uciekła z domu przed aranżowanym małżeństwem (dzięki Rekinom) i próbuje stanąć na swoim. Chce pomóc słabszym. | 0109-09-19 - 0109-09-26 |
| 200520-figurka-a-kopie-zapasowe     | bawiła się w klubie Pierwiosnek. Wpadł tam Gerwazy Lemurczak i wykorzystał ją jako przynętę na Dianę. Przerażona Gerwazym. | 0109-10-13 - 0109-10-20 |
| 200513-trzyglowec-kontra-melinda    | chce ratować chłopaka z Trzygłowca (Adama), chowa się na zapleczu i rzuca bombę śmierdzącą. Doprowadziła Janka do utraty kontroli i ów ją spoliczkował. Uciekła. | 0109-11-08 - 0109-11-17 |
| 200524-dom-dla-melindy              | nie jest szczególnie mądra, ale podobno jest świetna w opowieściach; Pięknotka zaczęła na nią polować by... dać jej dom u Alana, z Chevaleresse. | 0110-09-13 - 0110-09-16 |
| 200913-haracz-w-parku-rozrywki      | świetnie się bawiła w parku rozrywki Janus; tam Dotknęła Kultu Ośmiornicy. Niewiele więcej tam zrobiła. | 0110-10-09 - 0110-10-10 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 200507-anomalna-figurka-zabboga     | jednak jest uboższa niż się zdaje; uciekła z domu przed aranżowanym małżeństwem i boi się, że ją znajdą i ściągną z powrotem. "Odpad rodu", bo nie jest magiem. | 0109-09-26
| 200507-anomalna-figurka-zabboga     | powiązana z gangiem Latających Rekinów; dzięki nim uciekła i dostała się do Podwiertu (gdzie nie radzi sobie z życiem). | 0109-09-26
| 200507-anomalna-figurka-zabboga     | zapewniła sobie ochronę Jana Łowicza + to, że on będzie uczył jej walczyć. | 0109-09-26
| 200507-anomalna-figurka-zabboga     | zauroczona i podkochuje się w Janie Łowiczu. Niestety dla niego. | 0109-09-26
| 200513-trzyglowec-kontra-melinda    | Janek złamał jej serce, uderzył ją w twarz. Uciekła do Diany, swojej przyjaciółki w Arkadii. Nie jest już stalkerką. | 0109-11-17
| 200913-haracz-w-parku-rozrywki      | zainteresowana Kultem Ośmiornicy; poczuła jego potęgę. Ona chce prawdziwej potęgi; ludzie Kultu ją mają, czemu ona nie może? | 0110-10-10

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Diana Lemurczak      | 3 | ((200513-trzyglowec-kontra-melinda; 200520-figurka-a-kopie-zapasowe; 200524-dom-dla-melindy)) |
| Jan Łowicz           | 3 | ((200423-lojalna-zdrajczyni; 200507-anomalna-figurka-zabboga; 200513-trzyglowec-kontra-melinda)) |
| Kinga Kruk           | 3 | ((200423-lojalna-zdrajczyni; 200507-anomalna-figurka-zabboga; 200513-trzyglowec-kontra-melinda)) |
| Natasza Aniel        | 3 | ((200423-lojalna-zdrajczyni; 200507-anomalna-figurka-zabboga; 200513-trzyglowec-kontra-melinda)) |
| Diana Tevalier       | 2 | ((200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Feliks Keksik        | 2 | ((200513-trzyglowec-kontra-melinda; 200520-figurka-a-kopie-zapasowe)) |
| Gerwazy Lemurczak    | 2 | ((200520-figurka-a-kopie-zapasowe; 200524-dom-dla-melindy)) |
| Katja Nowik          | 2 | ((200423-lojalna-zdrajczyni; 200513-trzyglowec-kontra-melinda)) |
| Mariusz Grabarz      | 2 | ((200513-trzyglowec-kontra-melinda; 200520-figurka-a-kopie-zapasowe)) |
| Mariusz Trzewń       | 2 | ((200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Pięknotka Diakon     | 2 | ((200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Adam Cześń           | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Adam Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Alan Bartozol        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Aranea Diakon        | 1 | ((200520-figurka-a-kopie-zapasowe)) |
| Damian Polwonien     | 1 | ((200507-anomalna-figurka-zabboga)) |
| Duży Tom             | 1 | ((200423-lojalna-zdrajczyni)) |
| Felicja Melitniek    | 1 | ((200423-lojalna-zdrajczyni)) |
| Franciszek Owadowiec | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Franek Bulterier     | 1 | ((200524-dom-dla-melindy)) |
| Gustaf Profnos       | 1 | ((200507-anomalna-figurka-zabboga)) |
| Karol Senonik        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Krystyna Senonik     | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Laura Tesinik        | 1 | ((200524-dom-dla-melindy)) |
| Maja Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Rafał Kamaron        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Rafał Torszecki      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Tomasz Tukan         | 1 | ((200507-anomalna-figurka-zabboga)) |