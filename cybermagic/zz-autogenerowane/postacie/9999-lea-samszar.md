---
categories: profile
factions: 
owner: public
title: Lea Samszar
---

# {{ page.title }}


# Generated: 



## Fiszki


* czarodziejka, źródło problemów, 24 | @ 230325-ten-nawiedzany-i-ta-ukryta
* ENCAO:  +-+-0 |Arogancja i poczucie wyższości;;Kłótliwa;;Królowa lodu;;Overextending| VALS: Face, Universalism >> Hedonism| DRIVE: Deal with pests | @ 230325-ten-nawiedzany-i-ta-ukryta
* styl: Adelicia (Rental Magica) | @ 230325-ten-nawiedzany-i-ta-ukryta
* przewaga: ogromna siła magii - Horrory (efemerydy strachu i koszmarów na podstawie sacrifice) | @ 230325-ten-nawiedzany-i-ta-ukryta

### Wątki


magiczne-szczury-podwierckie

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230325-ten-nawiedzany-i-ta-ukryta   | Rekin; kiedyś pomogła Michałowi Klabaczowi i się z nim zaprzyjaźniła ale jako że tienka nie może przyjaźnić się z człowiekiem, poświęciła go. Widząc, że magowie krzywdzą Michała, zostawiła mu plany zrobienia antymagicznej bransolety, ale gdy tchnęła weń efemerydę, Paradoks nałożył EfemeHorrora. Próbowała sprawić, by ktoś jej przyprowadził Michała do pomocy, ale Ola nie rozumiała jej hintów. W końcu Terienakowie przyprowadzili Daniela, acz Lea i Karo się pogryzły o styl komunikacji i Daniel mediował. | 0111-10-16 - 0111-10-17 |
| 240305-lea-strazniczka-lasu         | wyleczyła Michała rytuałem z jednego EfemeHorrora i zaraz stworzyła nowego EfemeHorrora mającego odpędzać magów badających las (by czegoś nie obudzić). Nie chce współpracować ani się komunikować z innymi. Nie pokaże po sobie, że jej zależy. Wykonuje polecenia Marysi ("Rodów"), choć uważa Marysię za pionka Rodów. Łamie tabu Eterni, bo nie zgadza sie na Eternię i ich wpływy tutaj. Ale poddała się woli Marysi. | 0111-10-21 - 0111-10-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolina Terienak    | 2 | ((230325-ten-nawiedzany-i-ta-ukryta; 240305-lea-strazniczka-lasu)) |
| Michał Klabacz       | 2 | ((230325-ten-nawiedzany-i-ta-ukryta; 240305-lea-strazniczka-lasu)) |
| Aleksandra Burgacz   | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Arkadia Verlen       | 1 | ((240305-lea-strazniczka-lasu)) |
| Bogdan Gwiazdocisz   | 1 | ((240305-lea-strazniczka-lasu)) |
| Daniel Terienak      | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Ernest Namertel      | 1 | ((240305-lea-strazniczka-lasu)) |
| Franek Bulterier     | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Keira Amarco d'Namertel | 1 | ((240305-lea-strazniczka-lasu)) |
| Malena Barandis      | 1 | ((240305-lea-strazniczka-lasu)) |
| Marek Samszar        | 1 | ((240305-lea-strazniczka-lasu)) |
| Mariusz Kupieczka    | 1 | ((240305-lea-strazniczka-lasu)) |
| Marysia Sowińska     | 1 | ((240305-lea-strazniczka-lasu)) |
| Nadia Uprewien       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Oliwier Czepek       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Rupert Mysiokornik   | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Triana Porzecznik    | 1 | ((240305-lea-strazniczka-lasu)) |