---
categories: profile
factions: 
owner: public
title: Szymon Jaszczurzec
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190820-liliana-w-swiecie-dokumentow | mag firmy spedycyjnej Błyskowąż powiązany z Kajratem; krzyżowo zaskoczył się z Lilianą i stracił spacyfikowanego mimika. Skończył aresztowany przez Tymona. | 0110-07-04 - 0110-07-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adela Pieczar        | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Arnulf Poważny       | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Liliana Bankierz     | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Tadeusz Kruszawiecki | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Tymon Grubosz        | 1 | ((190820-liliana-w-swiecie-dokumentow)) |