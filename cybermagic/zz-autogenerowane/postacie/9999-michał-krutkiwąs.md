---
categories: profile
factions: 
owner: public
title: Michał Krutkiwąs
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181030-zaczestwiacy-czy-karolina    | streamer żądny chwały i właściciel sklepu z komputerami Komputrix, rzucił się na ducha by inni mogli uciec. | 0109-10-15 - 0109-10-16 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181030-zaczestwiacy-czy-karolina    | zdobył niezły szacun za to patostreamowanie - nagle wszyscy krzyczą i Mariusz łapie Damiana i skacze przez okno. Epicki performance. | 0109-10-16
| 181101-wojna-o-uczciwe-polfinaly    | ma w historii skuteczną reklamę żelków połączoną z patostreamowaniem. Epicko dziwne. | 0109-10-19

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Damian Podpalnik     | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Karolina Erenit      | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Kirisu Gero          | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Lia Sagabello        | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Mariusz Kozaczek     | 1 | ((181030-zaczestwiacy-czy-karolina)) |