---
categories: profile
factions: 
owner: public
title: Bartek Mardiblon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230831-potwory-ktore-przetrwaly-eter | 'Braciszek' ('bronić DIANĘ!', entropik-class, kiedyś: brat Amelii; uszkodzony umysł, lojalny Amelii i chce ją chronić. Gdy opętał go zew, wrócił do Amelii - lojalność silniejsza niż zew Diany i Centrali. | 0093-03-11 - 0093-03-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Mardiblon     | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Dawid Nałęcznik      | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Diana Nałęcznik      | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Esaria Mirtalis      | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Karol La Viris       | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Mateusz Owisiec      | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Rosenkrat Amiribasit | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |