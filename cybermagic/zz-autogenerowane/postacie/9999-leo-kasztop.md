---
categories: profile
factions: 
owner: public
title: Leo Kasztop
---

# {{ page.title }}


# Generated: 



## Fiszki


* sprzedawca sekretów na Poezji Obłoków | @ 221022-derelict-okarantis-wejscie
* ENCAO:  0-+00 |Intrygancki, polityka;;Nie odracza| VALS: Self-direction >> Stimulation, Tradition| DRIVE: Wygrać w rywalizacji) | @ 221022-derelict-okarantis-wejscie
* sprzedawca sekretów na Nonarionie (atarienin) | @ 221111-niebezpieczna-woda-na-hiyori
* ENCAO:  0-+00 | Intrygancki, polityka;;Nie odracza| VALS: Self-direction >> Stimulation, Tradition| DRIVE: Wygrać w rywalizacji) | @ 221111-niebezpieczna-woda-na-hiyori

### Wątki


historia_darii
salvagerzy-anomalii-kolapsu
syndykat-aureliona-w-anomalii-kolapsu

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221022-derelict-okarantis-wejscie   | sprzedawca sekretów na Nonarionie Nadziei; |ENCAO:  0-+00 |Intrygancki, polityka;;Nie odracza| VALS: Self-direction >> Stimulation, Tradition| DRIVE: Wygrać w rywalizacji)|; sprzedał Barbatovowi informacje o derelikcie Okarantis znajdującym się w Anomalii Kolapsu. Zna się z Ogdenem dobrze. | 0090-03-03 - 0090-03-14 |
| 221111-niebezpieczna-woda-na-hiyori | zmartwił się tym, że Hiyori może mieć problemy finansowe. Ostrzegł Nastię, by nie brała tanich pożyczek bo dzieje się tu coś dziwnego. | 0090-06-17 - 0090-06-21 |
| 221113-ailira-niezalezna-handlarka-woda | kryminalista mający coś pozytywnego w przeszłości z Ogdenem; wyraźnie próbuje pomóc Hiyori odpychając ich od Ailiry i znajdując subzlecenia. Całkowicie amoralny, tylko chrupki się liczą. Ale ostrzegł Darię przed zleceniem. | 0090-06-22 - 0090-06-28 |
| 221116-astralna-flara-dociera-do-nonariona-nadziei | postarzał się; powiedział Darii, że 'nie jest już stąd'. Ale sprzedał jej dobrą planetoidę, choć z anomalią. Będzie z nią współpracował. | 0100-09-08 - 0100-09-11 |
| 221130-astralna-flara-w-strefie-duchow | przekazał Darii informacje o Strefie Duchów i planetoidzie TKO-4271; po tym jak Daria ostrzegła go o działaniach Orbitera wobec neikatiańskich TAI przekazał info gdzie trzeba. | 0100-10-05 - 0100-10-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Daria Czarnewik      | 5 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Iga Mikikot          | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Jakub Uprzężnik      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Kaspian Certisarius  | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Nastia Barbatov      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Patryk Lapszyn       | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Safira d'Hiyori      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Ailira Niiris        | 2 | ((221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Arianna Verlen       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Arnulf Perikas       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Gabriel Lodowiec     | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Kajetan Kircznik     | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Maja Samszar         | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Ogden Barbatov       | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| OO Astralna Flara    | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| OO Athamarein        | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| SCA Hiyori           | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Szymon Wanad         | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Alezja Dumorin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Elena Verlen         | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Ellarina Samarintael | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Filip Szukurkor      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Grażyna Burgacz      | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Hubert Kerwelenios   | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Julia Karnit         | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Kirea Rialirat       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Klarysa Jirnik       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Leszek Kurzmin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Ludwik Trójkadur     | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Marcel Kulgard       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| NekroTAI Zarralea    | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| SCA Hadiah Emas      | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Tomasz Dojnicz       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Tomasz Ruppok        | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Władawiec Diakon     | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |