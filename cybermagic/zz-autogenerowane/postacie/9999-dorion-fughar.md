---
categories: profile
factions: 
owner: public
title: Dorion Fughar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231122-sen-chroniacy-kochankow      | przyjaciel i rywal Rovisa, który próbuje stać się prawą ręką Honga. Współuczestniczył w ataku na Jolę-09. Pierwszy stał za próbą rozwalenia Rovisa za 'zdradę rasy'. Skończył Uśpiony przez Alucis. Odratowany przez Agencję. | 0105-09-02 - 0105-09-05 |
| 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa | zastępca Mawira, który dobrze reprezentuje myślenie szefa - to kolejny test Saitaera. A Agencja wrobiła ich w to że oni robią sabotaże. Nie oni. Chce, by stacja była szczęśliwa. | 0105-12-11 - 0105-12-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Felina Amatanir      | 2 | ((231122-sen-chroniacy-kochankow; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Kalista Surilik      | 2 | ((231122-sen-chroniacy-kochankow; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klasa Dyplomata      | 2 | ((231122-sen-chroniacy-kochankow; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Mawir Hong           | 2 | ((231122-sen-chroniacy-kochankow; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Aerina Cavalis       | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Damian Orczakin      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Gabriel Septenas     | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Jola-09 Szernief     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Kaldor Czuk          | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klasa Hacker         | 1 | ((231122-sen-chroniacy-kochankow)) |
| Klasa Inżynier       | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klasa Oficer Naukowy | 1 | ((231122-sen-chroniacy-kochankow)) |
| Klasa Sabotażysta    | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klaudiusz Widar      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Rovis Skarun         | 1 | ((231122-sen-chroniacy-kochankow)) |
| Sia-03 Szernief      | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Szymon Alifajrin     | 1 | ((231122-sen-chroniacy-kochankow)) |