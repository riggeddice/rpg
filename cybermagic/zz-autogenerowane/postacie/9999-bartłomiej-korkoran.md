---
categories: profile
factions: 
owner: public
title: Bartłomiej Korkoran
---

# {{ page.title }}


# Generated: 



## Fiszki


* wuj i twarda łapka rządząca Infernią | faeril: "Infernia służy Korkoranom jako awatar Bezimiennej Pani." | @ 230201-wylaczone-generatory-memoriam-inferni
* (ENCAO: +-000 |Bezkompromisowy, nieustępliwy;;Ciekawski|Family, Benevolence, Self-direction > Achievement, Tradition, Humility| DRIVE: Inkwizytor: ujawnianie bolesnych prawd) | @ 230201-wylaczone-generatory-memoriam-inferni
* "Nasza Infernia ma za zadanie dać nam wolność oraz pomagać innym!" | @ 230201-wylaczone-generatory-memoriam-inferni
* wuj i twarda łapka rządząca Infernią (WG: "System służy ludziom") | faeril: "Infernia służy Korkoranom jako awatar Bezimiennej Pani." | @ 230208-pierwsza-randka-eustachego

### Wątki


infernia-jej-imieniem
kidiron-zbawca-nativis

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220914-dziewczynka-trianai          | wujek poprosił Eustachego i Ardillę, by oni rozwiązali problem znikających nastolatków w Starej Arkologii. Nie wierzy w Ducha Arkologii. | 0092-09-10 - 0092-09-11 |
| 220720-infernia-taksowka-dla-lycoris | PAST: łagodny wujek opiekujący się Eustachym i Adrillą, który jednak rozdziela opierdol gdy zasłużyli; bardzo ważna jest dlań rodzina. ACTUAL: dba o to, by Janek mógł spotkać się z Darią i próbuje chronić swoich młodych podopiecznych. Jednak zaufał im i oddał im do działania zainfekowany przez Trianai CES Purdont (na usprawiedliwienie, nie wie z czym ma do czynienia). | 0093-01-20 - 0093-01-22 |
| 230201-wylaczone-generatory-memoriam-inferni | moralny, chce pomóc noktianom i przygarnął młodego noktianina, Ralfa Tapszecza. Chce by Infernia czyniła dobro. Bardzo rozczarowany Tymonem, który wzgardził Celiną i Ardillą - spoliczkował go i oddał dowodzenie misją Ardilli. | 0093-02-10 - 0093-02-12 |
| 230208-pierwsza-randka-eustachego   | próbuje pokazać Eustachemu, że E. jest lepszy i skuteczniejszy. Że nie musiał zabijać tych noktian. Próbuje przekonać Eustachego pod kątem honoru i prawości. Ale Kidiron się zgadza z Eustachym - Bartłomiej wygląda na starszego niż jest. Widzi, że przegrywa wojnę o Eustachego i nie wie czemu. DOWIADUJE SIĘ, że Infernia jest pod mentalną kontrolą Eustachego. | 0093-02-14 - 0093-02-21 |
| 230215-terrorystka-w-ambasadorce    | chciał zastąpić Ardillę jako zakładnik, ale nie wyszło. Więc wymienił się na grupę rannych ludzi. W momencie w którym Misteria już nie była racjonalna zaatakował ją i obezwładnił, ciężko ranny przez wiły. Dzięki temu pomógł doprowadzić do ewakuacji wszystkich z Ambasadorki. | 0093-02-22 - 0093-02-23 |
| 230614-atak-na-kidirona             | wreszcie zregenerowany. Nie wiedział nic o teorii 'Ralf jest magiem Nihilusa'. Próbuje chronić Eustachego przed Infernią, Infernię przed memoriam, Ardillę przed Ralfem. Po raz pierwszy potraktował Eustachego i Ardillę jak dorosłych. Gdy był atak na Arkologię, przejął kontrolę tymczasowo. Z łóżka w szpitalu. | 0093-03-22 - 0093-03-24 |
| 230621-infiltrator-ucieka-a-arkologia-plonie | przejął część Hełmów i próbuje ochronić Arkologię oraz Prometeusa. Nie ma Lancerów, więc działa czym może. Walczy z młodym Laurencjuszem Kidironem o to kto będzie regentem Arkologii. | 0093-03-25 - 0093-03-26 |
| 230628-wojna-o-arkologie-nativis-konsolidacja-sil | zajmuje główne komponenty Arkologii by zapewnić jej przetrwanie (i oba statki - Infernię i Szare Ostrze). Gdy Draglin obiecał współpracę pod Eustachym, oficjalnie oddał się do dyspozycji Eustachego. Ale robi swoje. | 0093-03-27 - 0093-03-28 |
| 230719-wojna-o-arkologie-nativis-nowa-regentka | z Draglinem wbił się i zdobył Radiowęzeł. Chce uratować syna, Tymona. | 0093-03-28 - 0093-03-29 |
| 230726-korkoran-placi-cene-za-nativis | częściowo Dotknięty przez Izabellę, częściowo żałujący swoich decyzji odnośnie Tymona, poszedł go przekonać. Tymon się wysadził, zabijając siebie i Bartłomieja. Bartłomiej MÓGŁBY może go powstrzymać... ale nie miał do tego serca. Jeden raz gdy potrzebował rodziny, został sam. KIA. | 0093-03-29 - 0093-03-30 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220831-czarne-helmy-i-robaki        | coś jest nie tak, Kidironowie i Robaki nie mogli zrobić tego sabotażu; zajmie się tym i wejdzie w głąb. Podejrzewa Tymona Korkorana. | 0092-08-27
| 220720-infernia-taksowka-dla-lycoris | w przeszłości znał się z Wiktorem Turkalisem. Lubią się. | 0093-01-22

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 10 | ((220720-infernia-taksowka-dla-lycoris; 220914-dziewczynka-trianai; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Eustachy Korkoran    | 10 | ((220720-infernia-taksowka-dla-lycoris; 220914-dziewczynka-trianai; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| OO Infernia          | 7 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Ralf Tapszecz        | 7 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Kalia Awiter         | 6 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Rafał Kidiron        | 6 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Tobiasz Lobrak       | 4 | ((230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Tymon Korkoran       | 4 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230726-korkoran-placi-cene-za-nativis)) |
| Celina Lertys        | 3 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Izabella Saviripatel | 3 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Marcel Draglin       | 3 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Dalmjer Servart      | 2 | ((230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Jan Lertys           | 2 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni)) |
| Laurencjusz Kidiron  | 2 | ((230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Lycoris Kidiron      | 2 | ((220720-infernia-taksowka-dla-lycoris; 230614-atak-na-kidirona)) |
| Stanisław Uczantor   | 2 | ((220914-dziewczynka-trianai; 230614-atak-na-kidirona)) |
| BIA Prometeus        | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Czesław Żuczek       | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Daria Raizis         | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Franciszek Pietraszczyk | 1 | ((230208-pierwsza-randka-eustachego)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kamil Wraczok        | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Karina Nezerin       | 1 | ((220914-dziewczynka-trianai)) |
| Karol Lertys         | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Małgorzata Maratelus | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Marcin Pietraszczyk  | 1 | ((230208-pierwsza-randka-eustachego)) |
| SAN Szare Ostrze     | 1 | ((230614-atak-na-kidirona)) |
| Wiktor Turkalis      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |