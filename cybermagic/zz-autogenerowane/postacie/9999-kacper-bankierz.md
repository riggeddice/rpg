---
categories: profile
factions: 
owner: public
title: Kacper Bankierz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211120-glizda-ktora-leczy           | w nagraniu polecał innym arystokratom przyjechanie na prowincję, BO TU JEST MEGA FAJNIE! Amelia robi zawody z bicia się żuli i w ogóle! | 0108-04-07 - 0108-04-18 |
| 201013-pojedynek-akademia-rekiny    | wieczny przeciwnik Liliany - Rekin, który gardzi podejściem Liliany i uważa ją za impostora w rodzie. Zwykle robił przeciw niej ustawki. Ma małą frakcję Rekinów. | 0110-10-14 - 0110-10-22 |
| 201020-przygoda-randka-i-porwanie   | młodzi Sowińscy doprowadzili do pojedynku, po czym ich opiekunowie oszukali. Kacper ma z nimi kosę. Ale przy efemerydzie - pomógł ją rozmontować. I wyciągnął arystokratów - nikt nie zasłużył na areszt przez Tymona, to nie-Aurumowate. | 0110-10-25 - 0110-10-27 |
| 210323-grzybopreza                  | gorąco pogratulował Marysi Sowińskiej wpakowanie Napoleona do jeziora. Potem namówiony przez Marysię zrobił grzybową CZYSTĄ imprezę ze swoimi Rekinami - by tylko wpakować w kłopoty terminusów i by się to głośno odbiło. | 0111-04-22 - 0111-04-23 |
| 210713-rekin-wspiera-mafie          | współpracuje z mafią Kajrata by być KIMŚ na tym terenie i nie tylko. Nie chciał się dzielić z Marysią i używa kurierów (agentów) by sam nie ucierpieć. Bezpośrednia kolizja z praworządną Arkadią. | 0111-06-06 - 0111-06-09 |
| 210817-zgubiony-holokrysztal-w-lesie | po tym jak jego kurier (Cyryl) zgubił ko-matrycę Kuratorów, jest zmuszony do wycofania się z tego terenu. Nie odda ko-matrycy mafii (zwłaszcza że Marysia ją zdobyła) | 0111-06-22 - 0111-06-24 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 201013-pojedynek-akademia-rekiny    | do jego ścigacza połączyło się echo 'ustawki' i efemerydy. Ścigacz robi czasem "swoje rzeczy". | 0110-10-22
| 201020-przygoda-randka-i-porwanie   | Henryk i Daniel Sowińscy mają u niego dług honorowy, który ma zamiar wykorzystać w podły dla nich sposób. | 0110-10-27
| 210323-grzybopreza                  | szacunek wobec Marysi Sowińskiej - nie tylko jest SOWIŃSKA, ale też wyrolowała terminuskę (Ulę) oraz wpakowała Napoleona do jeziora na jego ścigaczu. | 0111-04-23
| 210713-rekin-wspiera-mafie          | wrobiony przez Marysię w to, że niby to on otruł Arkadię. A jest niewinny. | 0111-06-09
| 210817-zgubiony-holokrysztal-w-lesie | ma poważnie przechlapane u mafii Grzymościa (Wolny Uśmiech), bo "zgubił" ko-matrycę i na jej miejsce podłożył fałszywkę (czego NIE zrobił on a Pustogor). | 0111-06-24

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Julia Kardolin       | 3 | ((201013-pojedynek-akademia-rekiny; 210323-grzybopreza; 210713-rekin-wspiera-mafie)) |
| Liliana Bankierz     | 3 | ((201013-pojedynek-akademia-rekiny; 201020-przygoda-randka-i-porwanie; 210817-zgubiony-holokrysztal-w-lesie)) |
| Marysia Sowińska     | 3 | ((210323-grzybopreza; 210713-rekin-wspiera-mafie; 210817-zgubiony-holokrysztal-w-lesie)) |
| Tomasz Tukan         | 3 | ((201020-przygoda-randka-i-porwanie; 210713-rekin-wspiera-mafie; 210817-zgubiony-holokrysztal-w-lesie)) |
| Urszula Miłkowicz    | 3 | ((201020-przygoda-randka-i-porwanie; 210323-grzybopreza; 210713-rekin-wspiera-mafie)) |
| Ignacy Myrczek       | 2 | ((201013-pojedynek-akademia-rekiny; 210323-grzybopreza)) |
| Justynian Diakon     | 2 | ((201013-pojedynek-akademia-rekiny; 211120-glizda-ktora-leczy)) |
| Napoleon Bankierz    | 2 | ((201013-pojedynek-akademia-rekiny; 210323-grzybopreza)) |
| Robert Pakiszon      | 2 | ((201013-pojedynek-akademia-rekiny; 201020-przygoda-randka-i-porwanie)) |
| Stella Armadion      | 2 | ((201013-pojedynek-akademia-rekiny; 210817-zgubiony-holokrysztal-w-lesie)) |
| Triana Porzecznik    | 2 | ((201020-przygoda-randka-i-porwanie; 210323-grzybopreza)) |
| Alan Bartozol        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Aleksander Bemucik   | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Amanda Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Amelia Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Cyryl Perikas        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Daniel Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Ernest Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Feliks Keksik        | 1 | ((211120-glizda-ktora-leczy)) |
| Gabriel Ursus        | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Henryk Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Karol Pustak         | 1 | ((211120-glizda-ktora-leczy)) |
| Laura Tesinik        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Laurencjusz Sorbian  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Mariusz Trzewń       | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Oliwia Lemurczak     | 1 | ((211120-glizda-ktora-leczy)) |
| Rafał Torszecki      | 1 | ((210713-rekin-wspiera-mafie)) |
| Remor 340D           | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Robinson Porzecznik  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Roland Sowiński      | 1 | ((211120-glizda-ktora-leczy)) |
| Rupert Mysiokornik   | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Sabina Kazitan       | 1 | ((211120-glizda-ktora-leczy)) |
| Sensacjusz Diakon    | 1 | ((211120-glizda-ktora-leczy)) |
| Stella Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Teresa Mieralit      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Tymon Grubosz        | 1 | ((201020-przygoda-randka-i-porwanie)) |