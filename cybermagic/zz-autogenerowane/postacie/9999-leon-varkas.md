---
categories: profile
factions: 
owner: public
title: Leon Varkas
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230806-zwiad-w-iliminar-caos        | cały czas próbuje flirtować z Amandą, ale jest kompetentny; wraz z Amandą porwali kilku wartowników. Infiltrator. Ma opinię absolutnego kobieciarza. | 0081-06-22 - 0081-06-24 |
| 230906-operacja-mag-dla-symlotosu   | bardzo zainteresowany tematem Furii i łóżek; flirtuje z Amandą i łyka od Xavery o Furiach jak młody pelikan. Nie w pełni wierzy w plan Kajrata. Pod wpływem kwiatów niedaleko Muru wpadł w rycerskość i chciał pomóc Furiom, ale się oddalił za ich poleceniem, bo uwierzył w wielką orgię Furii Mataris. | 0081-06-26 - 0081-06-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Caelia Calaris       | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Dragan Halatis       | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Ernest Kajrat        | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Lestral Kirmanik     | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Alaric Rakkeir       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Edmund Garzin        | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Isaura Velaska       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Xavera Sirtas        | 1 | ((230906-operacja-mag-dla-symlotosu)) |