---
categories: profile
factions: 
owner: public
title: Brunon Szwagacz
---

# {{ page.title }}


# Generated: 



## Fiszki


* (klarkartianin), oddział Sessair | @ 230102-elwira-koszmar-nox-ignis
* ENCAO:  +--00 |Działa zgodnie z błędnym modelem;;Drapieżny| VALS: Achievement, Family| DRIVE: Noctis together, others go away | @ 230102-elwira-koszmar-nox-ignis
* "We shall make it work. Noctis forever." | @ 230102-elwira-koszmar-nox-ignis

### Wątki


cena-nox-ignis
wojna-deorianska

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230102-elwira-koszmar-nox-ignis     | oficer noktiański, teraz porządkowy na CON Ratio Spei. Zainteresował się Talią i będzie z nią współpracować jeśli odzyska noktianki, które porwał Orbiter. (ENCAO:  +--00 |Działa zgodnie z błędnym modelem;;Drapieżny| VALS: Achievement, Family| DRIVE: Noctis stands) (klarkartianin). Tymczasowy ostrożny sojusznik Talii. | 0082-08-02 - 0082-08-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aida Liminis         | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| AK Nox Ignis         | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Aletia Nix           | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Dominik Łarnisz      | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Franz Szczypiornik   | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Medea Sowińska       | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| OO Loricatus         | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Riaon Diralik        | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Talia Derwisz        | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Tatiana Ozariat      | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Tristan Ozariat      | 1 | ((230102-elwira-koszmar-nox-ignis)) |