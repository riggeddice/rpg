---
categories: profile
factions: 
owner: public
title: Tal Marczak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210707-po-drugiej-stronie-bramy     | ludzki noktiański mechanik Inferni; rozpoznał starą bazę na wzorach noktiańskich. Z innymi noktianami w grupie roboczej tłumaczącej sigile tej dziwnej archaicznej bazy. | 0111-05-11 - 0111-05-13 |
| 210526-morderstwo-na-inferni        | noktiański ludzki inżynier na Inferni. Kiedyś: Alrund Tal, silnie powiązany z eks-noktiańską korwetą Tivr (kiedyś: Aries Tal). Chciał wysadzić Tivr, więc mechanicy noktiańscy go zabili, by nie było kłopotów. | 0111-12-31 - 0112-01-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy)) |
| Elena Verlen         | 2 | ((210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy)) |
| Eustachy Korkoran    | 2 | ((210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy)) |
| Klaudia Stryk        | 2 | ((210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy)) |
| Martyn Hiwasser      | 2 | ((210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy)) |
| Antoni Kramer        | 1 | ((210526-morderstwo-na-inferni)) |
| Feliks Walrond       | 1 | ((210526-morderstwo-na-inferni)) |
| Janus Krzak          | 1 | ((210707-po-drugiej-stronie-bramy)) |
| Kamil Lyraczek       | 1 | ((210526-morderstwo-na-inferni)) |
| Karol Reichard       | 1 | ((210526-morderstwo-na-inferni)) |
| Leona Astrienko      | 1 | ((210526-morderstwo-na-inferni)) |
| Marian Tosen         | 1 | ((210526-morderstwo-na-inferni)) |
| OO Tivr              | 1 | ((210526-morderstwo-na-inferni)) |
| Otto Azgorn          | 1 | ((210526-morderstwo-na-inferni)) |
| Raoul Lavanis        | 1 | ((210707-po-drugiej-stronie-bramy)) |