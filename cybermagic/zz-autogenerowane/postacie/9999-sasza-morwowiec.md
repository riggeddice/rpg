---
categories: profile
factions: 
owner: public
title: Sasza Morwowiec
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211010-ukryta-wychowanka-arnulfa    | terminus; od dawna poluje na Tymona i uważa go za szkodnika. Gdy skonfrontował się z Tymonem w AMZ, Arnulf powiedział że to jego sprawa i Saszę wygnał - robiąc sobie z Saszy wroga. | 0084-12-11 - 0084-12-12 |
| 211017-nastolatka-w-bieliznie-na-dachu-w-burzy | terminus chcący wykazać zdradę Arnulfa i Tymona; przeciwny samodzielności AMZ, ma grudge do Tymona i wierzy, że Tymon współpracuje z Grzymościem. Anulował misję, by ratować lunatyczkę w AMZ (to była Teresa i jej nie dorwał). Powiedział Klaudii, że szuka linka Tymon - Grzymość. Wbrew pozorom, całkiem spoko koleś. Nie nienawidzi noktian. Ale żąda porządku. | 0084-12-14 - 0084-12-15 |
| 240114-o-seksbotach-i-syntetycznych-intelektach | poluje na Talię, bo próbuje dorwać Arnulfa i Tymona. Podejrzewa jakieś mroczne działania Talii. Przekonany przez Klaudię, osłania Talię przed dziwnym atakiem sabotażysty. Firefight z Raleną. Jest zniesmaczony tym, że Talia robi seksboty zachowujące się jak noktianki. Chce chronić Klaudię i osłonić ją przed złym wpływem Talii. | 0085-01-13 - 0085-01-18 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211010-ukryta-wychowanka-arnulfa    | święcie przekonany, że Tymon Grubosz i Arnulf Poważny współpracują nad czymś przeciwko Pustogorowi. Nie podejrzewa Talii (bo to noktianka). | 0084-12-12

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 3 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 240114-o-seksbotach-i-syntetycznych-intelektach)) |
| Ksenia Kirallen      | 3 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 240114-o-seksbotach-i-syntetycznych-intelektach)) |
| Teresa Mieralit      | 3 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 240114-o-seksbotach-i-syntetycznych-intelektach)) |
| Arnulf Poważny       | 2 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Talia Aegis          | 2 | ((211010-ukryta-wychowanka-arnulfa; 240114-o-seksbotach-i-syntetycznych-intelektach)) |
| Felicjan Szarak      | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Ralena Drewniak      | 1 | ((240114-o-seksbotach-i-syntetycznych-intelektach)) |
| Strażniczka Alair    | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Tymon Grubosz        | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Waldemar Grzymość    | 1 | ((211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |