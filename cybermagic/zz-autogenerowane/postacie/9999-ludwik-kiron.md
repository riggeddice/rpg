---
categories: profile
factions: 
owner: public
title: Ludwik Kiron
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201117-morszczat-zostaje-piratem    | dekadencki lord piracki, kiedyś arystokrata Eterni. Przyjął do siebie Morszczata. By się pozbyć rywala - Parziarza - wpakował go na Ariannę Verlen. | 0111-03-11 - 0111-03-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Bożena Kaldesz       | 1 | ((201117-morszczat-zostaje-piratem)) |
| Donald Parziarz      | 1 | ((201117-morszczat-zostaje-piratem)) |
| Siergiej Rożen       | 1 | ((201117-morszczat-zostaje-piratem)) |
| Zbigniew Morszczat   | 1 | ((201117-morszczat-zostaje-piratem)) |