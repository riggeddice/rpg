---
categories: profile
factions: 
owner: public
title: Antoni Bladawir
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: A-O+ | Brutalnie szczery i pogardliwy; Chce mieć rację | VALS: Power, Family | DRIVE: Wyczyścić kosmos ze słabości i leszczy. | @ 231109-komodor-bladawir-i-korona-woltaren
* "nawet jego zwycięstwa mają gorzki posmak dla tych, którzy z nim służą". Wykorzystuje każdą okazję, by wykazać swoją wyższość. | @ 231109-komodor-bladawir-i-korona-woltaren
* Doskonały taktyk, niedościgniony na polu bitwy. Jednocześnie podły tyran dla swoich ludzi. | @ 231109-komodor-bladawir-i-korona-woltaren
* Uważa tylko Orbiterowców i próżniowców za prawidłowe byty w kosmosie. Nie jest fanem 'ziemniaków w kosmosie' (planetarnych). | @ 231109-komodor-bladawir-i-korona-woltaren

### Wątki


triumfalny-powrot-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231109-komodor-bladawir-i-korona-woltaren | komodor. Tępił 'Koronę Woltaren' dla prywaty i traktuje nie-Orbiter jak śmiecie. W końcu 'Korona' miała problem z terrorformem i wysławszy tam Ariannę - jego przypuszczenia się spełniły. Mściwy i niemiły. | 0110-10-27 - 0110-11-03 |
| 231115-cwiczenia-komodora-bladawira | były kochanek i przełożony kmdr Ewy Razalis, przez kilka lat podkładał jej ludzi do elitarnej gwardii by ostatecznie ją ZNISZCZYĆ i pokazać jej że każdego da się kupić. Ściągnął do siebie Ariannę jako dywersję, ale jak wykazała się skillsetem to dołączył ją bo kompetentna. Absolutnie okrutny. Jego słabością jest przekonanie o jego intelekcie i absolutny brak sympatii do ludzi. | 0110-11-08 - 0110-11-15 |
| 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira | zrobił ćwiczenia by postawić Ariannę przed wyborem 'skuteczność' lub 'dobre serce i przyjaciele', wystawiając jako swoją namiestniczkę Zaarę. Jednocześnie testuje umiejętności i lojalność swoich pozostałych oficerów. Po tej operacji w której Arianna skutecznie ominęła wszystkie jego pułapki jest przekonany do jej kompetencji, acz zmartwiony jej pro-ludzkimi słabościami. | 0110-11-26 - 0110-11-30 |
| 231220-bladawir-kontra-przemyt-tienow | tyran planujący zniszczenie 'zdrajcy Orbitera' przemycającego tienów z Aurum przez Karsztarin gdzieś poza Orbiter. Zaara na jego polecenie opracowała potwora wprowadzonego na Karsztarin (za czasów starcia z Ewą Razalis). Wszystko opracował - wygnanie zdrajcy z Karsztarina i przechwycenia go jego jednostkami które nawet nie wiedzą w jakiej operacji uczestniczą. Ale Arianna i Klaudia pokrzyżowały mu plany, jego działania się rozpadły. PRAWIE powiedział Ariannie o co chodzi, ale nie powie tego przecież 'wrogiej agentce'... | 0110-12-06 - 0110-12-11 |
| 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow | czegoś szuka w okolicy Neotik i przekazał część Mrocznych Danych Klaudii i Zaarze do analizy. Po tym jak Klaudia znalazła anomalię w danych, pozyskał zwłoki dla Arianny by zrobić Dark Awakening. Gdy się okazało, że to nie to czego szuka a potwór, skupił się na jego usunięciu bo Orbiter zawiódł ludzi. Zagroził Ariannie zdrowiem jej bliskich jeśli stanie przeciw niemu. Co ważne - nie ukarał ani Arianny ani Klaudii bo uważa je za kompetentne i przydatne dla Orbitera. | 0110-12-03 - 0110-12-17 |
| 240110-wieczna-wojna-bladawira      | grasping at straws. Próbuje łagodniej podejść do Inferni; tak zdewastowany, że aż PRAWIE powiedział Ariannie coś czego nie chciał. I tak pokazał kilka śladów które Arianna może znaleźć. Zaczyna robić błędy - chciał użyć magów bez obstawy ludzi, chciał Reanimować kralotha by go przesłuchać. Ale coś znalazł i dookoła kralotha i działań Ernesta Bankierza. Pytanie - co. | 0110-12-24 - 0110-12-27 |
| 240124-mikiptur-zemsta-woltaren     | był w złym miejscu, wierząc w swoje plany i działania i został wymanewrowany przez Aureliona. Gdyby nie Arianna i Infernia, przestałby być komodorem. | 0110-12-29 - 0111-01-01 |
| 240125-wszystkie-sekrety-caralei    | gdy dostał problem Caralei - nie do końca prawidłowej jednostki z żywą BIA - włączył ją do swoich tajnych wspieranych jednostek i wprowadził tam Toga jako swojego człowieka. | 0110-12-29 - 0111-01-02 |
| 240131-anomalna-mavidiz             | wysłał Ariannę Tivrem (którego miała zdobyć) na Mavidiz, by przejąć Tarę (od Ernesta Bankierza) jako dowód, że Ernest jest zdrajcą. | 0111-01-03 - 0111-01-08 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 231109-komodor-bladawir-i-korona-woltaren | pełnia zasług za to, że skutecznie ograniczył problem ixiońskiego terrorforma na "Koronie Woltaren". Zgodnie z papierami, 'miał rację' doczepiając się do tej jednostki. | 0110-11-03
| 231115-cwiczenia-komodora-bladawira | przez Izę, Bladawir spotyka się z publicznym potępieniem, a Razalis postrzegana jest jako ofiara. | 0110-11-15
| 231220-bladawir-kontra-przemyt-tienow | Przegrał operację 'usunięcia zdrajcy z Orbitera', nad którą pracował od dawna. Największa porażka od bardzo dawna. Do tego ogromny OPR z Admiralicji za działania, sabotaż i niedopilnowanie Inferni. Ogólnie, poważny cios. | 0110-12-11
| 231220-bladawir-kontra-przemyt-tienow | Dowiedział się, że Verlenowie wiedzą o 'przesuwaniu tienów z Aurum na Orbitę' ale w tym nie uczestniczą. I o 'Ariannie, księżniczce Verlenlandu' i części jej powiązań. | 0110-12-11
| 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow | ma offlinowe dane w których ma Podłe Czyny wielu ludzi XD. Wielką Teczkę Czynów Złych i Podłych. | 0110-12-17
| 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow | przez działania Arianny, dostał poważne uszkodzenie od Orbitera. Patrzą mu na ręce i unieszkodliwili jego działania. | 0110-12-17
| 240110-wieczna-wojna-bladawira      | całkowicie zagubiony pomiędzy dziesiątkami konspiracji i planów Orbitera. Bierze narkotyki, by się wzmocnić i by być niestrawnym dla magii. He Will Not Serve Arianna After Death. | 0110-12-27
| 240124-mikiptur-zemsta-woltaren     | potężna strata autorytetu, opinii świetnego oficera itp. Gdyby nie Arianna, straciłby poziom komodora. | 0111-01-01
| 240131-anomalna-mavidiz             | dzięki operacji Arianny na Mavidiz uzyskał eks-Aurelionowca (Karl Murnoff) i eks-agentkę Ernesta Bankierza (Tarę Ogniczek). | 0111-01-08

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 8 | ((231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240110-wieczna-wojna-bladawira; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Klaudia Stryk        | 8 | ((231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240110-wieczna-wojna-bladawira; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Zaara Mieralit       | 4 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240110-wieczna-wojna-bladawira)) |
| Elena Verlen         | 3 | ((231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Eustachy Korkoran    | 3 | ((231220-bladawir-kontra-przemyt-tienow; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Kazimierz Darbik     | 3 | ((231220-bladawir-kontra-przemyt-tienow; 240110-wieczna-wojna-bladawira; 240125-wszystkie-sekrety-caralei)) |
| Lars Kidironus       | 3 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240124-mikiptur-zemsta-woltaren)) |
| OO Paprykowiec       | 3 | ((231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Raoul Lavanis        | 3 | ((231220-bladawir-kontra-przemyt-tienow; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Ewa Razalis          | 2 | ((231115-cwiczenia-komodora-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Leszek Kurzmin       | 2 | ((231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira)) |
| Marta Keksik         | 2 | ((231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| OO Infernia          | 2 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| OO Karsztarin        | 2 | ((231115-cwiczenia-komodora-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Patryk Samszar       | 2 | ((231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Władawiec Diakon     | 2 | ((240110-wieczna-wojna-bladawira; 240124-mikiptur-zemsta-woltaren)) |
| Aneta Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Borys Kragin         | 1 | ((240131-anomalna-mavidiz)) |
| Dobromir Misiak      | 1 | ((240125-wszystkie-sekrety-caralei)) |
| Dormand Miraris      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Dorota Radraszew     | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Ernest Bankierz      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Feliks Walrond       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Grigor Tarnow        | 1 | ((240131-anomalna-mavidiz)) |
| Igor Stratos         | 1 | ((240131-anomalna-mavidiz)) |
| Izabela Zarantel     | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Kamil Lyraczek       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Karl Murnoff         | 1 | ((240131-anomalna-mavidiz)) |
| Krzysztof Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Lidia Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Marcinozaur Verlen   | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Marian Witaczek      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Markus Wąż           | 1 | ((240131-anomalna-mavidiz)) |
| OA Mikiptur          | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Olaf Rawen           | 1 | ((240125-wszystkie-sekrety-caralei)) |
| ONS Mavidiz          | 1 | ((240131-anomalna-mavidiz)) |
| OO Isratazir         | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| OO Tivr              | 1 | ((240131-anomalna-mavidiz)) |
| Rita Stratos         | 1 | ((240131-anomalna-mavidiz)) |
| Samuel Fanszakt      | 1 | ((240110-wieczna-wojna-bladawira)) |
| SC Korona Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Szymon Orzesznik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Talia Miraris        | 1 | ((240110-wieczna-wojna-bladawira)) |
| Tara Ogniczek        | 1 | ((240131-anomalna-mavidiz)) |
| Tog Worniacz         | 1 | ((240125-wszystkie-sekrety-caralei)) |
| Tomasz Rewernik      | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Wojciech Mykirło     | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |