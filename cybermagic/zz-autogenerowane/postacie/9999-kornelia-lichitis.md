---
categories: profile
factions: 
owner: public
title: Kornelia Lichitis
---

# {{ page.title }}


# Generated: 



## Fiszki


* (ENCAO:  00+0- |Autentyczna; mówi jak jest;;Świetnie zorganizowana| VALS: Stimulation, Face | DRIVE: Rana ego (nie dość dobra)) | @ 230125-whispraith-w-jaskiniach-neikatis
* salvager Uśmiechu (inżynier), atarienka: "Poradzimy sobie ludzkim intelektem" | @ 230125-whispraith-w-jaskiniach-neikatis
* "Krok po kroku dojdziemy do rozwiązania, rzeczowo i solidnie" | @ 230125-whispraith-w-jaskiniach-neikatis
* kadencja: panika, tchórz | @ 230125-whispraith-w-jaskiniach-neikatis

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230125-whispraith-w-jaskiniach-neikatis | widziała whispraitha więc uciekła w burzę; uratowana przez Seiren, ostrzegła ich by nie schodzili. Nie wie CO widziała ale ma histerię. Po ustabilizowaniu przez Zofię, przydatna. | 0092-10-29 - 0092-11-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aniela Myszawcowa    | 1 | ((230125-whispraith-w-jaskiniach-neikatis)) |
| Anna Seiren          | 1 | ((230125-whispraith-w-jaskiniach-neikatis)) |
| Antoni Grzypf        | 1 | ((230125-whispraith-w-jaskiniach-neikatis)) |
| Cyprian Kugrak       | 1 | ((230125-whispraith-w-jaskiniach-neikatis)) |
| Eustachy Korkoran    | 1 | ((230125-whispraith-w-jaskiniach-neikatis)) |
| JAN Seiren           | 1 | ((230125-whispraith-w-jaskiniach-neikatis)) |
| JAN Uśmiech Kamili   | 1 | ((230125-whispraith-w-jaskiniach-neikatis)) |
| Michał Uszwon        | 1 | ((230125-whispraith-w-jaskiniach-neikatis)) |
| Rufus Seiren         | 1 | ((230125-whispraith-w-jaskiniach-neikatis)) |
| Zofia d'Seiren       | 1 | ((230125-whispraith-w-jaskiniach-neikatis)) |