---
categories: profile
factions: 
owner: public
title: AJA Szybka Strzała
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230620-karolinus-sedzia-mirkali     | odkryła, że wszystkie dokumenty Mirkali są całkowicie fikcyjne. Potem przygotowała hiper-paczkę wiadomości odnośnie rodziny przyjaciela Juanity i złamała morale Juanity. Juanita przestała walczyć. | 0095-04-15 - 0095-04-18 |
| 230425-klotnie-sasiadow-w-wanczarku | sprawdza przychody, rozchody, sprawdza wioskę pobieżnie. Spaliła pół elektroniki wioski (za słabe), ale ma kluczowe dane. Ratuje życie topielca Ilfonsa. Potem monitoruje Artemis Lawellan i ją konfrontuje w imieniu Karolinusa. | 0095-05-16 - 0095-05-19 |
| 230314-brudna-konkurencja-w-arachnoziem | (Autonomiczna Jednostka Aurum; zamaskowana eks-ŁZa militarna która podczas wojny uratowała życie Samszara i teraz jest sprzężona z duchem. ADVANTAGES: walka elektroniczna, walka, skan, transport, hidden.) Zlokalizowała telefon używany przez ludzi zatrudnionych przez EnMilStrukt a potem szybko dostarczyła Karolinusa do lokalizacji wrogiej czarodziejki. Skutecznie zwiała Viorice lecąc w stratosferę. | 0095-06-20 - 0095-06-22 |
| 230328-niepotrzebny-ratunek-mai     | skuteczne unikanie wykrycia, dzięki czemu Karolinus mógł dostać się do VirtuFortis. Wymanewrowała potwora i Viorikę (która na nią nie polowała) | 0095-06-30 - 0095-07-02 |
| 230404-wszystkie-duchy-siewczyna    | odkryła konflikt między duchami a ludźmi, samodzielnie wykonywała zwiady i weszła w wojnę psychotroniczną z Hybrydą AI-ducha. | 0095-07-18 - 0095-07-20 |
| 230411-egzorcysta-z-sanktuarium     | Sanktuarium Kazitan podczas Emisji jest miejscem na granicy możliwości Strzały; została uszkodzona, ale skutecznie tam Zespół doprowadziła. Jednak potem, gdy Zespół uciekał z porwanym Irkiem z Sanktuarium, Strzała tak starała się żadnego cywila nie zastrzelić że została ciężko uszkodzona (pokazując, że jest najbardziej "ludzka i humanitarna" z całego Zespołu). Ale doprowadziła wszystkich bezpiecznie do Przelotyka i wylądowała w ruinie. | 0095-07-21 - 0095-07-23 |
| 230509-samszarowie-lemurczak-i-fortel-strzaly | mimo połowy systemów zniszczonych, dotarła do bezpiecznej jaskini niedaleko grzmotoptaków. Używając okoliczności i inteligencji w tak marnym stanie przestraszyła Lemurczaka (jego hovertank), że tu są Verlenowie i mają złe intencje. A potem zdała pełen raport Rolandowi Samszarowi i zgasła. Shutdown + autorepair. | 0095-07-24 - 0095-07-26 |
| 230613-zaginiecie-psychotronika-cede | analizuje dane komputera w pokoju Cede, zmasakrowała biednego demona i znalazła że w tle jest jednostka do walki psychotronicznej. Została niewykryta. | 0095-08-02 - 0095-08-05 |
| 230523-romeo-dyskretny-instalator-supreme-missionforce | identyfikuje urządzenia Romeo jako detektory i ciężarówkę jako pojazd opancerzony, rozstawia drony monitorujące teren. Potem - uspokaja Maję i leci z pełną prędkością by ją umieścić w bezpiecznym miejscu zanim jej ojciec się wścieknie. | 0095-08-09 - 0095-08-11 |
| 230606-piekna-diakonka-i-rytual-nirwany-koz | zbiera informacje na co ostatnio 'poleciała' Itria, stanowi bezpieczne miejsce rozmowy z Herbertem i monitoruje + patternuje zachowania Itrii by zrobić jej projekcję dla Maksa. | 0095-08-15 - 0095-08-18 |
| 230627-ratuj-mlodziez-dla-kajrata   | anty-amnestykowała Karolinusa, po czym skontaktowała zespół z Kajratem by móc naprawić swoją psychotronikę używając Talii Aegis. Zdobyła plany inkubatora gdzie jest baza hiperpsychotroników i podpowiadała Karolinusowi jak gadać z Itrią ;-). | 0095-08-20 - 0095-08-25 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230411-egzorcysta-z-sanktuarium     | bardzo ciężko uszkodzona, wymaga sporej naprawy. Doprowadziła jednak wszystkich poza Sanktuarium Kazitan i skutecznie porwała Irka. | 0095-07-23
| 230613-zaginiecie-psychotronika-cede | okazuje się, że jej pamięć jest uszkodzona; robili z nią jakieś eksperymenty. Ma geas i nie jest w stanie sobie przypomnieć. | 0095-08-05

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolinus Samszar    | 11 | ((230314-brudna-konkurencja-w-arachnoziem; 230328-niepotrzebny-ratunek-mai; 230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230425-klotnie-sasiadow-w-wanczarku; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230523-romeo-dyskretny-instalator-supreme-missionforce; 230606-piekna-diakonka-i-rytual-nirwany-koz; 230613-zaginiecie-psychotronika-cede; 230620-karolinus-sedzia-mirkali; 230627-ratuj-mlodziez-dla-kajrata)) |
| Elena Samszar        | 6 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230523-romeo-dyskretny-instalator-supreme-missionforce; 230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata)) |
| Irek Kraczownik      | 3 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Fiona Szarstasz      | 2 | ((230314-brudna-konkurencja-w-arachnoziem; 230328-niepotrzebny-ratunek-mai)) |
| Herbert Samszar      | 2 | ((230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata)) |
| Impresja Ignicja Incydencja Diakon | 2 | ((230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata)) |
| Maja Samszar         | 2 | ((230328-niepotrzebny-ratunek-mai; 230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Romeo Verlen         | 2 | ((230328-niepotrzebny-ratunek-mai; 230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Agnieszka Klirpin    | 1 | ((230620-karolinus-sedzia-mirkali)) |
| Albert Samszar       | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Aleksander Samszar   | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Amanda Kajrat        | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Ania Turabnik        | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Apollo Verlen        | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Arnold Kazitan       | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Artemis Lawellan     | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Artur Lawellan       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Bonifacy Samszar     | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Cede Burian          | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Celina Burian        | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Damian Fenekis       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Dźwiedź Łagodne Słowo | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Elea Brzozecka       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Ernest Kajrat        | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Fabian Samszar       | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Filip Klirpin        | 1 | ((230620-karolinus-sedzia-mirkali)) |
| Fircjusz Szarstasz   | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Ilfons Lawellan      | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Jonatan Lemurczak    | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Juanita Derwisz      | 1 | ((230620-karolinus-sedzia-mirkali)) |
| Julita Mopsarin      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Kacper Aczramin      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Lara Ukraptin        | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Laura Turabnik       | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Maks Samszar         | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Maksymilian Sforzeczok | 1 | ((230404-wszystkie-duchy-siewczyna)) |
| Mitria Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Nataniel Samszar     | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Olga Fenekis         | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Roland Samszar       | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Tadeusz Dzwańczak    | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Talia Aegis          | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Viorika Verlen       | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Wacław Samszar       | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Wargun Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |