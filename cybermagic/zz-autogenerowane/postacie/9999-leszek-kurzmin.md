---
categories: profile
factions: 
owner: public
title: Leszek Kurzmin
---

# {{ page.title }}


# Generated: 



## Fiszki


* dowódca jednostki | @ 221123-egzotyczna-pieknosc-na-astralnej-flarze
* ENCAO: 0-+++ | | @ 221123-egzotyczna-pieknosc-na-astralnej-flarze
* ENCAO: 0-+++ | Żyje według własnych przekonań;; Lojalny i proaktywny| VALS: Benevolence, Humility >> Face| DRIVE: Właściwe miejsce w rzeczywistości. | @ 221214-astralna-flara-kontra-domina-lucis
* dowódca jednostki (atarienin defensor) TACTICAL NERD | @ 221221-astralna-flara-i-nowy-komodor

### Wątki


historia-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220928-kapitan-verlen-i-pojedynek-z-marine | chce pomóc Ariannie (nawet dyskretnie) by ratować Królową Kosmicznej Chwały i Alezję. Rywalizował z Alezją w Akademii, nie wierzy jak bardzo upadła. | 0100-05-14 - 0100-05-16 |
| 221026-kapitan-verlen-i-koniec-przygody-na-krolowej | współpracuje z Arianną. Ona zapewnia linię 'Aurum - Tucznik Trzeci', on linię 'Tucznik Trzeci - Nonarion'. I tak powstanie udany handel na którym wszyscy wygrają. | 0100-06-08 - 0100-06-15 |
| 221116-astralna-flara-dociera-do-nonariona-nadziei | szczęśliwy, że mają sensownego komodora; | 0100-09-08 - 0100-09-11 |
| 221123-egzotyczna-pieknosc-na-astralnej-flarze | dobrze pracuje z dokumentami i przetwarza dane; znalazł dla Gabriela Lodowca info, że jeden z niewolników to koleś z Orbitera. Przeprowadził dyskretną operację Ograniczenia Mirtaeli, TAI Hadiah Emas z rozkazu Gerwazego Kircznika. | 0100-09-12 - 0100-09-15 |
| 221214-astralna-flara-kontra-domina-lucis | logistycznie przeprowadził operację poddawania się noktian, by na pewno nie ucierpiał nikt niewinny i nie dało się zniszczyć Flary ani Athamarein. Wykonywał wielokrotne skany aż doszedł do tego, że teren jest silnie zaminowany. Wiedział, że Domina Lucis nie będzie bez defensyw. | 0100-10-09 - 0100-10-11 |
| 221221-astralna-flara-i-nowy-komodor | to on zaproponował sojusz ze squatterami i oddanie im bazy. Twórca modyfikowanego planu jak odzyskać Kazmirian by wywołać największy szok u squatterów. Oddał Ariannie możliwość spięcia z Kazmirian, by jej załoga mogła ćwiczyć (Bolza jest bardzo wymagający). Doszedł do tego, że najpewniej to Bolza stoi za zniszczeniem Zarralei i przekazał informację o tym Ariannie. | 0100-11-07 - 0100-11-10 |
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | pewny umiejętności Athamarein; chciał walczyć z Nox Ignis, ale pod wpływem pomysłów Arianny zmienił Athamarein w super-szybką jednostkę kurierską i wymanewrował Nox Ignis nie walcząc z nią bezpośrednio. CREEPED OUT przez Destructor Animarum. Zabił Adragaina, nie chciał widzieć tego upiornego pustego uśmiechu. | 0100-11-13 - 0100-11-16 |
| 200624-ratujmy-castigator           | kapitan Castigatora i przyjaciel Arianny; poprosił ją o pomoc w "oczyszczeniu" Castigatora z naleśniczej arystokracji. Mocno jej ufa... i ją friendzonował. | 0110-10-15 - 0110-10-19 |
| 231011-ekstaflos-na-tezifeng        | z przyjemnością odbudował kontakt z Arianną; poprosił ją o dyskretne rozwiązanie sprawy z Klaudiuszem i Natalią. Wyraźnie próbuje robić dobrą minę do złej gry, pomagać młodym tienom i wydobyć najlepsze diamenty z tego co tam ma. | 0110-10-20 - 0110-10-22 |
| 231025-spiew-nielalki-na-castigatorze | próbuje chronić swoich tienów przed konsekwencjami biurokratycznymi oraz anomalią Altarient; widząc problem na Castigatorze szybko kazał containować jednostkę i ewakuować kogo się da. Zarządza administracyjną stroną Castigatora, by anomalia Altarient nie zniszczyła niczego ważnego. | 0110-10-24 - 0110-10-26 |
| 231109-komodor-bladawir-i-korona-woltaren | w dobrej wierze dał Ariannie na akcję Patryka i Martę, bo są kompetentni i mogą coś fajnego zrobić. Prosta akcja. Nie wiedział, że wysyła ich na terroforma ixiońskiego XD. Zaproponował Ariannie korwetę 'Paprykowiec'. | 0110-10-27 - 0110-11-03 |
| 231115-cwiczenia-komodora-bladawira | dąży do udowodnienia błędów w podejściu Bladawira; chce pomóc Ewie Razalis bo NIE ZGADZA SIĘ z wartościami Bladawira. Wspiera Ariannę. | 0110-11-08 - 0110-11-15 |
| 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze | (nieobecny) za bardzo ufa swojej załodze i za bardzo ufa swoim tienom. Jego oficerowie bardziej dbają o jego reputację niż on sam. | 0110-12-19 - 0110-12-21 |
| 200708-problematyczna-elena         | dowódca Castigatora; wysłał Elenę do Arianny jako załogantkę. A Kramer zaakceptował, bo Elena miała wszystkie papiery w porządku i nieposzlakowaną opinię. | 0110-12-27 - 0111-01-01 |
| 200819-sekrety-orbitera-historia-prawdziwa | by pomóc Ariannie, wziął na siebie Leonę, mówiąc jej że wykryli koloidowy statek Eterni. | 0111-01-24 - 0111-02-01 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | solidny opierdol od kmdr Salazara Bolzy za śmierć Adragaina Ferriasa. Emocje zwyciężyły nad możliwością zbadania jak działa Destructor Animarum. | 0100-11-16
| 231115-cwiczenia-komodora-bladawira | zdeklarował się jednoznacznie przeciwko Bladawirowi i Bladawir o tym wie. | 0110-11-15
| 240124-mikiptur-zemsta-woltaren     | potężny cios - "jest nierozsądny" i przez niego potencjalnie Syndykat miał dostęp do magów Aurum. I to z winy Arianny XD. | 0111-01-01

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 14 | ((200624-ratujmy-castigator; 200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa; 220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira)) |
| Elena Verlen         | 8 | ((200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Daria Czarnewik      | 7 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Klaudia Stryk        | 7 | ((200624-ratujmy-castigator; 200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa; 231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Eustachy Korkoran    | 5 | ((200624-ratujmy-castigator; 200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Leona Astrienko      | 5 | ((200624-ratujmy-castigator; 200819-sekrety-orbitera-historia-prawdziwa; 220928-kapitan-verlen-i-pojedynek-z-marine; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| OO Astralna Flara    | 5 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 5 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Władawiec Diakon     | 5 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Arnulf Perikas       | 4 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Kajetan Kircznik     | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Maja Samszar         | 4 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Ellarina Samarintael | 3 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Erwin Pies           | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gabriel Lodowiec     | 3 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis)) |
| Grażyna Burgacz      | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Marta Keksik         | 3 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| OO Castigator        | 3 | ((200624-ratujmy-castigator; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Patryk Samszar       | 3 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Tomasz Ruppok        | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Alezja Dumorin       | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Antoni Bladawir      | 2 | ((231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira)) |
| Antoni Kramer        | 2 | ((200708-problematyczna-elena; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Feliks Walrond       | 2 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren)) |
| Igor Arłacz          | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Izabela Zarantel     | 2 | ((200819-sekrety-orbitera-historia-prawdziwa; 231115-cwiczenia-komodora-bladawira)) |
| Klaudiusz Terienak   | 2 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 231011-ekstaflos-na-tezifeng)) |
| Konstanty Keksik     | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Marcel Kulgard       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Martyn Hiwasser      | 2 | ((200819-sekrety-orbitera-historia-prawdziwa; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| NekroTAI Zarralea    | 2 | ((221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| OO Infernia          | 2 | ((231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| OO Królowa Kosmicznej Chwały | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| OO Loricatus         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Paprykowiec       | 2 | ((231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira)) |
| Raoul Lavanis        | 2 | ((231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Salazar Bolza        | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| SCA Hadiah Emas      | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Stefan Torkil        | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Szczepan Myrczek     | 2 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Szymon Wanad         | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Salamin           | 1 | ((200708-problematyczna-elena)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Aneta Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Anna Tessalon        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Axel Nargan          | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Damian Orion         | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Dorota Radraszew     | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Eleonora Perłamila   | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Ewa Razalis          | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gerwazy Kircznik     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Julia Myrczek        | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Kamil Burgacz        | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Kaspian Certisarius  | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Kirea Rialirat       | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Kosmicjusz Tanecznik Diakon | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Krzysztof Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Lars Kidironus       | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Leo Kasztop          | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Lidia Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Łucja Larnecjat      | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Natalia Gwozdnik     | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Olgierd Drongon      | 1 | ((200708-problematyczna-elena)) |
| OO Alkaris           | 1 | ((200624-ratujmy-castigator)) |
| OO Aurelion          | 1 | ((200708-problematyczna-elena)) |
| OO Karsztarin        | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| OO Tezifeng          | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OO Tucznik Trzeci    | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| OO Żelazko           | 1 | ((200708-problematyczna-elena)) |
| Persefona d'Infernia | 1 | ((200708-problematyczna-elena)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Rozalia Wączak       | 1 | ((200624-ratujmy-castigator)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Sarian Xadaar        | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| SC Korona Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Tadeusz Ursus        | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| TAI Eszara d'Castigator | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Tomasz Rewernik      | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Tristan Rialirat     | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Wojciech Mykirło     | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |