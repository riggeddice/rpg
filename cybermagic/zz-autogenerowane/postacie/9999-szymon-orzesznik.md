---
categories: profile
factions: 
owner: public
title: Szymon Orzesznik
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (A+ N-) "Każdy człowiek może osiągnąć wielkość" | VALS: (Universalism, Benevolence) "Jesteśmy światłem dla tych, którzy nas potrzebują." | DRIVE: "Tworzenie lepszego świata przez bezpośrednią pomoc." | @ 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira
* kapitan sił Orbitera, niezależny i inteligentny agent | @ 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira

### Wątki


triumfalny-powrot-arianny
naprawa-swiata-przez-bladawira

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231220-bladawir-kontra-przemyt-tienow | inteligentny i niezależny, przemyca tienów z Aurum w kosmos. Kapitan Orbitera. Bladawir zastawił nań pułapkę i go przyskrzynił - ale Arianna go uwolniła i dała mu uciec, o czym nawet nie wiedział. Przyjaciel Leszka Kurzmina. | 0110-12-06 - 0110-12-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Bladawir      | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Arianna Verlen       | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Elena Verlen         | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Eustachy Korkoran    | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Ewa Razalis          | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Kazimierz Darbik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Klaudia Stryk        | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Marcinozaur Verlen   | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| OO Infernia          | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| OO Karsztarin        | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Raoul Lavanis        | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Zaara Mieralit       | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |