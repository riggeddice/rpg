---
categories: profile
factions: 
owner: public
title: Tymek Samszar
---

# {{ page.title }}


# Generated: 



## Fiszki


* mag, niefrasobliwy i niezwykle pomocny | @ 230419-karolinka-nieokielznana-swinka
* (ENCAO:  0-+++ |Niefrasobliwy, beztroski;;Kieruje się zasadami;;Elokwenty| VALS: Stimulation, Security| DRIVE: "To ma znaczenie dla TEJ ostrygi") | @ 230419-karolinka-nieokielznana-swinka
* styl: Joker, gdy chce być miły dla Andrei Beaumont / Rankle z MtG. O dziwo, dobry w planowaniu. | @ 230419-karolinka-nieokielznana-swinka
* Poeta oraz bardzo pomocny mag, który uwielbia niewielkie żarty. Iluzjonista. Jego magia jest niezwykle smakowita dla glukszwajnów. | @ 230419-karolinka-nieokielznana-swinka

### Wątki


karolinus-heart-viorika
przeznaczenie-eleny-verlen
hiperpsychotronika-samszarow

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230419-karolinka-nieokielznana-swinka | łagodny i sympatyczny, lecz miał pecha - spotkał się z Eleną i został wintegrowany przez nią w Menhir (panika Eleny). | 0095-07-19 - 0095-07-23 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksander Samszar   | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Apollo Verlen        | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Arianna Verlen       | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Elena Verlen         | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Fantazjusz Verlen    | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Marcinozaur Verlen   | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Ula Blakenbauer      | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Viorika Verlen       | 1 | ((230419-karolinka-nieokielznana-swinka)) |