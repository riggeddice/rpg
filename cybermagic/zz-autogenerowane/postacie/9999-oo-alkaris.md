---
categories: profile
factions: 
owner: public
title: OO Alkaris
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200624-ratujmy-castigator           | nanorojowy statek naprawczo-pozyskujący należący do NeoMil w Orbiterze; pod kontrolą Rozalii Wączak jako PO TAI. Tu: opętany przez Wiktora Sataraila; atakuje Castigator by go zniszczyć. | 0110-10-15 - 0110-10-19 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((200624-ratujmy-castigator)) |
| Eustachy Korkoran    | 1 | ((200624-ratujmy-castigator)) |
| Klaudia Stryk        | 1 | ((200624-ratujmy-castigator)) |
| Leona Astrienko      | 1 | ((200624-ratujmy-castigator)) |
| Leszek Kurzmin       | 1 | ((200624-ratujmy-castigator)) |
| OO Castigator        | 1 | ((200624-ratujmy-castigator)) |
| Rozalia Wączak       | 1 | ((200624-ratujmy-castigator)) |