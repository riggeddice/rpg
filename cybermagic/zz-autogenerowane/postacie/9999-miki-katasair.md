---
categories: profile
factions: 
owner: public
title: Miki Katasair
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210209-wolna-tai-na-k1              | RG; virtka specjalizująca się w przyjaźni i byciu "normalną dziewczyną". Ludzkie i nieludzkie zachowania; TAI? Człowiek? Dla TAI Neita staje naprzeciw Adalbertowi a nawet Rziezie. Niekompetentna w walce jak cholera, ale 'fiercely loyal'. Uczennica TAI Neutropia (KIA). | 0111-08-30 - 0111-09-04 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adalbert Brześniak   | 1 | ((210209-wolna-tai-na-k1)) |
| Andrea Burgacz       | 1 | ((210209-wolna-tai-na-k1)) |
| Janusz Parzydeł      | 1 | ((210209-wolna-tai-na-k1)) |
| Klaudia Stryk        | 1 | ((210209-wolna-tai-na-k1)) |
| Leona Astrienko      | 1 | ((210209-wolna-tai-na-k1)) |
| Martyn Hiwasser      | 1 | ((210209-wolna-tai-na-k1)) |
| TAI Neita Lairossa   | 1 | ((210209-wolna-tai-na-k1)) |
| TAI Rzieza d'K1      | 1 | ((210209-wolna-tai-na-k1)) |