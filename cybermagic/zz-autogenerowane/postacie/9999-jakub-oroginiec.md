---
categories: profile
factions: 
owner: public
title: Jakub Oroginiec
---

# {{ page.title }}


# Generated: 



## Fiszki


* komodor, pod którym jest Athamarein; chce pomóc młodym tienom bo uważa ich za godnych uwagi | @ 231018-anomalne-awarie-athamarein
* OCEAN: E-C- | Tajemniczy, sekrety za uśmiechem;; Silnie intuicyjny;; Życzliwy i taktowny | VALS: Benevolence, Self-Direction | DRIVE: Carmen Sandiego (naprawić szkody) | @ 231018-anomalne-awarie-athamarein

### Wątki


legenda-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231018-anomalne-awarie-athamarein   | komodor pod którym jest tymczasowo Athamarein; prawie został zmiażdżony przez Alicję rzuconym automatem do sprzedaży drinków. Patrzy na Ariannę z TAKIMI oczkami i z radością dał jej Athamarein do przebadania. Nie jest to najlepszy komodor w sensie samodzielności i wyzwań, ale zna swoje ograniczenia i dba o swoich ludzi - tu robi dobrą robotę. Kiepsko trzyma język za zębami (wygadał Ariannie o Syndykacie) | 0111-12-22 - 0111-12-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alicja Szadawir      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Andrzej Gwozdnik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Arianna Verlen       | 1 | ((231018-anomalne-awarie-athamarein)) |
| Błażej Sowiński      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Elena Verlen         | 1 | ((231018-anomalne-awarie-athamarein)) |
| Eustachy Korkoran    | 1 | ((231018-anomalne-awarie-athamarein)) |
| Hiacynt Samszar      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Kajetan Kircznik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Klaudia Stryk        | 1 | ((231018-anomalne-awarie-athamarein)) |
| Leona Astrienko      | 1 | ((231018-anomalne-awarie-athamarein)) |
| OO Athamarein        | 1 | ((231018-anomalne-awarie-athamarein)) |
| Remigiusz Alkarenit  | 1 | ((231018-anomalne-awarie-athamarein)) |
| Uśmiechniczka Konstrukcjonistka Aurora Diakon | 1 | ((231018-anomalne-awarie-athamarein)) |