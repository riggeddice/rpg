---
categories: profile
factions: 
owner: public
title: Celina Szaczyr
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181220-upiorny-servar               | kiedyś też się ścigała; Irek i Gabriel o nią rywalizowali. Skończyła jako lekko ubezwłasnowolniona ofiara Gabriela. | 0109-10-28 - 0109-10-30 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksandra Garwen    | 1 | ((181220-upiorny-servar)) |
| Alicja Wielżak       | 1 | ((181220-upiorny-servar)) |
| Artur Śrubek         | 1 | ((181220-upiorny-servar)) |
| Beata Wielinek       | 1 | ((181220-upiorny-servar)) |
| Bogdan Daneb         | 1 | ((181220-upiorny-servar)) |
| Gabriel Krajczok     | 1 | ((181220-upiorny-servar)) |
| Jerzy Cieniż         | 1 | ((181220-upiorny-servar)) |
| Michał Wypras        | 1 | ((181220-upiorny-servar)) |
| Patrycja Karzec      | 1 | ((181220-upiorny-servar)) |
| Ryszard Januszewicz  | 1 | ((181220-upiorny-servar)) |
| Wojciech Tuczmowil   | 1 | ((181220-upiorny-servar)) |