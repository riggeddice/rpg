---
categories: profile
factions: 
owner: public
title: Lily Sanarton
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230618-reality-show-z-zaskoczenia   | Cieniaszczycki szczur uliczny; prawie wpadła w pułapkę a potem wyciągnęła z Olafem Maksa z przepaści. Szybko się uczy i umie robić pułapki z włosów. Bardzo cicha, ale podoba jej się współpraca. | 0083-10-02 - 0083-10-07 |
| 230625-przegrywy-ratuja-przegrywy   | no-nonsense, jej obecność i słowa pomogły przekonać dwóch nieszczęśników z innego zespołu. Dobry zwiadowca. | 0083-10-08 - 0083-10-11 |
| 230626-pustynny-final-igrzysk       | wymyśliła, by pułapką była ona i Maja, co potem zmieniło się w plan 3+5. Mimo młodego wieku, godna zaufania. Przeniesiona z Olafem do Szczelińca gdy wygrali. | 0083-10-12 - 0083-10-15 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230626-pustynny-final-igrzysk       | trafiła do Szczelińca z Olafem Zuchwałym metodą teleportacji. W przyszłości ona i Olaf zostaną rodziną. | 0083-10-15

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Klart           | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Maja Wurmramin       | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Maks Ardyceń         | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Olaf Zuchwały        | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Roman Wyrkmycz       | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Antoni Kmandir       | 2 | ((230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Bogdan Ubuddan       | 2 | ((230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Jan Firatiel         | 1 | ((230626-pustynny-final-igrzysk)) |
| Rafał Armadion       | 1 | ((230626-pustynny-final-igrzysk)) |