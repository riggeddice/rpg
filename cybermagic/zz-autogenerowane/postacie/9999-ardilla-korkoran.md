---
categories: profile
factions: 
owner: public
title: Ardilla Korkoran
---

# {{ page.title }}


# Generated: 



## Fiszki


* faerilka działająca jak drakolitka i przyszywana kuzynka Eustachego  <-- często Fox | @ 230201-wylaczone-generatory-memoriam-inferni
* badacz/odkrywca/scrapper. Zmodyfikowana tak, by łatwiej wchodzić w niedostępne miejsca, bardziej zwinność + orientacja w terenie + pewne umiejętności badawcze. | @ 230201-wylaczone-generatory-memoriam-inferni

### Wątki


infernia-jej-imieniem
kidiron-zbawca-nativis

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220831-czarne-helmy-i-robaki        | uratowała przemytnika i przekazała Eustachemu paczkę z analizatorem żywności; gdy chciała podłożyć pluskwę Stanisławowi to on ją opieprzył i będzie resocjalizował. Ale wyciągnęła go z Robaków i sprowadziła na dobrą drogę. | 0092-08-15 - 0092-08-27 |
| 220914-dziewczynka-trianai          | w imieniu Stanisława zaniosła jedzenie dla "Ducha" Arkologii, potem znalazła gdzie ów "Duch" się znajduje (piętnastolatka trianai) i oddała ją Kidironom. | 0092-09-10 - 0092-09-11 |
| 221006-ona-chce-dziecko-eustachego  | zapewniła sobie współpracę Robaków którzy uciekli do Exerinna z Nativis, po czym nie dała sczeznąć Lertenowi zmienionemu w Farighana - wpadła do quada i go wyciągnęła unieruchamiając zanim on coś złego / głupiego zrobi pod wpływem implantu. | 0092-09-20 - 0092-09-24 |
| 220720-infernia-taksowka-dla-lycoris | CONCEPT: badacz/odkrywca/scrapper. Zmodyfikowana tak, by łatwiej wchodzić w niedostępne miejsca, bardziej zwinność + orientacja w terenie + pewne umiejętności badawcze. Eustachy składa maszyny, ja kradnę znaleziska ze zrujnowanych struktur. Rywalizujemy na polu Mój jeszcze-nie-wiem-co-to-inator jest lepszy, niż twój! PAST: wiewiórcza rozrabiaka, próbowała ukraść keykartę i chciała wkraść się z Darią do laboratorium ekopoezy; Eustachy podkablował więc złapana. ACTUAL: przekonała wujka by mimo presji czasowej pomógł Jankowi spotkać się z Darią, skoczyła na rurę i w ten sposób dała okazję Eustachemu i Jankowi na zestrzelenie ainshkera i pełniła rolę głównego zwiadowcy. | 0093-01-20 - 0093-01-22 |
| 220817-osy-w-ces-purdont            | wpierw z pomocą Janka zyskała kluczowe do antidotum rzeczy dla Celiny, potem porozkładała szybko próbki po bazie by Trianai vs komandosi i zrobić dywersję. I zdążyła uratować Darię i dostarczyć jej antidotum w ostatnim momencie. | 0093-01-23 - 0093-01-24 |
| 230201-wylaczone-generatory-memoriam-inferni | wzięła pod opiekę Ralfa i pokazała mu teren arkologii, dając mu namiastkę czegoś dla czego warto żyć; potem zastraszyła kilka lowlifeów z arkologii którzy chcieli sklupać noktianina. Okazuje się, że ma ostry język. Dostała dowodzenie operacją pomocy noktianom i twardo wybrała Wujka nad Kidirona. Ma dobre serce i zbiera informacje odnośnie używaniu Trianai jako broni biologicznej Kidironów. | 0093-02-10 - 0093-02-12 |
| 230208-pierwsza-randka-eustachego   | oswaja Ralfa i z nim lekko flirtuje (chcesz sobie znaleźć dziewczynę?). Pokazuje Ralfowi, że Nihilus nie jest wszechmocny, że jest po co żyć i działać. Ratuje Marcinka i wyjaśnia Ralfowi, że wszyscy ludzie mają potencjał. Ma do niego cierpliwość. Potem - śledzi Eustachego na randce z Kalią (i kibicuje Kalii). Zdobywa dowody, że Eustachy ma autokontrolę nad Infernią. Końsko zalotuje Eustachego i Kalię ;-). Gdy Eustachy zostawił Kalię w dziwnych okolicznościach, idzie do niej. | 0093-02-14 - 0093-02-21 |
| 230215-terrorystka-w-ambasadorce    | weszła do Ambasadorki razem z Kalią; została pojmana przez Misterię, ale przekonała ją do wymiany rannych na wujka. Próbowała negocjować ewakuację Misterii i innych osób. Przekonała Misterię by wymienić rannych na wujka i stanowiła serce zespołu; bez niej Misteria zostałaby zestrzelona lub zabita. Współpracując z Kalią doprowadziła do tego, że nikomu nic się nie stało. Współpracując z Misterią i Kalią doprowadziła do ruchu oporu przeciw Kidironowi. | 0093-02-22 - 0093-02-23 |
| 230315-bardzo-nieudane-porwanie-inferni | koordynowała akcję ratunkową, wyłączyła Infernię by napastnicy nie mogli jej zdobyć, złośliwie uszkodziła klimatyzację Czarnym Hełmom, schowała się w kanałach Inferni gdy mostek napadli i uszkodziła dla Eustachego generatory Memoriam Inferni. Gwarant bezpieczeństwa piratów podczas ich przesłuchania przez Kidirona. | 0093-03-06 - 0093-03-09 |
| 230329-zdrada-rozrywajaca-arkologie | zajmowała się komunikacją Infernia - arkologia by znaleźć Skorpiona Szczepana, poprosiła Amelię o shackowanie pamiętnika Eweliny, potem wciągnęła Kalię do sprawy Eweliny, znalazła dowód na obecność maga Interis który uratował Ewelinę i zdecydowała się pomóc Ewelinie. Musimy uratować jej reputację i odwrócić sprawę. | 0093-03-14 - 0093-03-16 |
| 230614-atak-na-kidirona             | od Stanisława usłyszała o dziwnej arkologii Nox Aegis; potem broniła Ralfa przed wujkiem (tak chce się dalej spotykać). Gdy był zamach na Kidirona, skupiła się na samym Kidironie i go z Ralfem uratowała, ku swym mieszanym uczuciom. Ale dostała dostęp do jego mrocznych planów. | 0093-03-22 - 0093-03-24 |
| 230621-infiltrator-ucieka-a-arkologia-plonie | utrzymuje Kidirona przy życiu, negocjując dla niego bezpieczne miejsce w Szczurowisku i stawiając swoją i Ralfową reputację na szali. Potem - rozpaczliwie ratuje Prometeusa, acz z ciężkimi stratami. | 0093-03-25 - 0093-03-26 |
| 230628-wojna-o-arkologie-nativis-konsolidacja-sil | używając Szarego Ostrza ma listę farighanów; przekonała Lertysów do pomocy przeciw nim. Próbuje chronić Szczury Arkologii przed brutalnością Eustachego i jego piratów, ale nadaremno. Eustachy zrobił co chciał. Poświęca personalną reputację dla jutrzejszej żywej i sprawnej arkologii. Uważa, że Rafał Kidiron jest jedynym co faktycznie utrzyma arkologię Nativis... | 0093-03-27 - 0093-03-28 |
| 230719-wojna-o-arkologie-nativis-nowa-regentka | doszła, że Lobrak jest aktywnym agentem Syndykatu i za wszystkim stoi mag; chce przejąć władzę nad Arkologią i współpracuje z Kalią. Ewakuowała Kalię i wpadła pod zaklęcie czarodziejki Syndykatu; zanim ta przeformatowała Ardillę, Ralf uratował Ardillę niszcząc umysł czarodziejki Syndykatu w imię Nihilusa. | 0093-03-28 - 0093-03-29 |
| 230726-korkoran-placi-cene-za-nativis | przede wszystkim skupia się na Ralfie, monitoruje go i patrzy na Izabellę. Przez to przeoczyła sytuację z Wujkiem. Ale chroni Arkologię przed Eustachym, który próbuje wejść w sojusz z Syndykatem Aureliona. Pokazuje Eustachemu, że to nie jest dobra opcja. Jest ostatnim elementem sumienia Eustachego. Gdyby nie Ardilla to Lobrak by wygrał, bo Eustachy by oddał mu Arkologię. | 0093-03-29 - 0093-03-30 |
| 230816-orbiter-nihilus-i-ruch-oporu-w-nativis | jej pierwszy plan - udawać kult Nihilusa - sama złamała (zbyt niebezpieczne). Próbowała wybadać ruchy Aurory Nativis i rozmawiała z Marzeną Marius, ale okazało się, że eks-komodor Orbitera jest po prostu politycznie LEPSZA od Ardilli. Ardilla odroczyła działania ruchu oporu. | 0093-03-30 - 0093-03-31 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230201-wylaczone-generatory-memoriam-inferni | w Nativis uznana za przerażającą. Zamiast przestraszyć nastolatków, woli ich skrzywdzić. "Element podły" się jej boi, Mroczne Hełmy niechętnie z nią walczą. | 0093-02-12
| 230201-wylaczone-generatory-memoriam-inferni | uznana za przyjaciółkę noktian z Coruscatis na Neikatis. Coruscatis jej tego nie zapomną. | 0093-02-12
| 230614-atak-na-kidirona             | ma dostęp do wszystkich planów operacyjnych Kidirona. Sekrety farighanów, trianai, bazy Nox Aegis... wszystko. | 0093-03-24
| 230614-atak-na-kidirona             | w sprawie Ralfa musi Wujkowi wszystko donosić - została konfidentem XD. | 0093-03-24
| 230621-infiltrator-ucieka-a-arkologia-plonie | dostęp do kodów i haseł Kidirona dających jej praktycznie władzę dyktatorki i autokraty | 0093-03-26
| 230628-wojna-o-arkologie-nativis-konsolidacja-sil | oficjalnie i powszechnie rozpoznawana jako sympatyczka i sojuszniczka Rafała Kidirona | 0093-03-28
| 230719-wojna-o-arkologie-nativis-nowa-regentka | ma potencjał przejęcia kontroli nad Arkologią Nativis w swoim imieniu, a w tle Kidiron. | 0093-03-29

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eustachy Korkoran    | 16 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 220914-dziewczynka-trianai; 221006-ona-chce-dziecko-eustachego; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis; 230816-orbiter-nihilus-i-ruch-oporu-w-nativis)) |
| Bartłomiej Korkoran  | 10 | ((220720-infernia-taksowka-dla-lycoris; 220914-dziewczynka-trianai; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| OO Infernia          | 10 | ((220720-infernia-taksowka-dla-lycoris; 221006-ona-chce-dziecko-eustachego; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Ralf Tapszecz        | 9 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Kalia Awiter         | 8 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230816-orbiter-nihilus-i-ruch-oporu-w-nativis)) |
| Rafał Kidiron        | 8 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Celina Lertys        | 6 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Tymon Korkoran       | 5 | ((220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230726-korkoran-placi-cene-za-nativis)) |
| Jan Lertys           | 4 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni)) |
| Marcel Draglin       | 4 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis; 230816-orbiter-nihilus-i-ruch-oporu-w-nativis)) |
| Tobiasz Lobrak       | 4 | ((230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Izabella Saviripatel | 3 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Stanisław Uczantor   | 3 | ((220831-czarne-helmy-i-robaki; 220914-dziewczynka-trianai; 230614-atak-na-kidirona)) |
| BIA Prometeus        | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Dalmjer Servart      | 2 | ((230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Franciszek Pietraszczyk | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| Kamil Wraczok        | 2 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont)) |
| Laurencjusz Kidiron  | 2 | ((230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Lycoris Kidiron      | 2 | ((220720-infernia-taksowka-dla-lycoris; 230614-atak-na-kidirona)) |
| Małgorzata Maratelus | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| SAN Szare Ostrze     | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230614-atak-na-kidirona)) |
| Wojciech Grzebawron  | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Ava Kieras           | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Czesław Żuczek       | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Daria Raizis         | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Emban Dolamor        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Hubert Grzebawron    | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Joachim Puriur       | 1 | ((220817-osy-w-ces-purdont)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Karina Nezerin       | 1 | ((220914-dziewczynka-trianai)) |
| Karol Lertys         | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kordian Olgator      | 1 | ((220817-osy-w-ces-purdont)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Lerten Kieras        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Maks Selert          | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Marcin Pietraszczyk  | 1 | ((230208-pierwsza-randka-eustachego)) |
| Mariusz Dobrowąs     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Marzena Marius       | 1 | ((230816-orbiter-nihilus-i-ruch-oporu-w-nativis)) |
| Michał Kervendal     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Nadia Sekernik       | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Staszek Zakraton     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| VN Exerinn           | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| VN Karglondel        | 1 | ((220817-osy-w-ces-purdont)) |
| Wiktor Turkalis      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Wojciech Czerpń      | 1 | ((220831-czarne-helmy-i-robaki)) |