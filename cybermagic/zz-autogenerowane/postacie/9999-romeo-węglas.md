---
categories: profile
factions: 
owner: public
title: Romeo Węglas
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190331-kurz-po-ataienne             | oficer Złotych Szakali; złamał się po rozmowie z Pięknotką. Powiedział jej kto stał za zleceniem na zniszczenie Ataienne. | 0110-03-07 - 0110-03-09 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Bartozol        | 1 | ((190331-kurz-po-ataienne)) |
| Ataienne             | 1 | ((190331-kurz-po-ataienne)) |
| Diana Tevalier       | 1 | ((190331-kurz-po-ataienne)) |
| Pięknotka Diakon     | 1 | ((190331-kurz-po-ataienne)) |
| Szymon Oporcznik     | 1 | ((190331-kurz-po-ataienne)) |