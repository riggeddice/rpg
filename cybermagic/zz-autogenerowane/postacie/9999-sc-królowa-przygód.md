---
categories: profile
factions: 
owner: public
title: SC Królowa Przygód
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220327-wskrzeszenie-krolowej-przygod | średni transportowiec na którym znajduje się TAI Miranda Ceres. Ma sekret. Aktualnie: w ruinie, będzie odbudowany przez blakvelowców. | 0108-05-01 - 0108-05-16 |
| 231027-planetoida-bogatsza-niz-byc-powinna | dość naprawiona, ma pojedyncze stare działko, nie ma możliwości podmontowania lasera górniczego, ma kiepskie sensory itp. Zestrzeliła NieGrzegorza na misji górniczej. | 0108-06-29 - 0108-07-02 |
| 220329-mlodociani-i-pirat-na-krolowej | naprawiona przez blakvelowców, przeprowadziła maiden voyage przez Asimear i po zestrzeleniu kilku Strachów uczestniczyła w akcji destrukcji piratów Berdysza. | 0108-08-29 - 0108-09-09 |
| 220405-lepsza-kariera-dla-romki     | ma zakaz odlotu; znajduje się chwilowo w głównej bazie Blakvelowców niedaleko Szamunczak. Nadal baza Zespołu. | 0108-09-15 - 0108-09-17 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220329-mlodociani-i-pirat-na-krolowej | dostaje do załogi nowego psychotronika (Wojciech Kaznodzieja) i medyka (Helena Banbadan). Ale najpewniej opuści ją Seweryn Grzęźlik. | 0108-09-09

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Miranda Ceres        | 4 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Gotard Kicjusz       | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Łucjan Torwold       | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Prokop Umarkon       | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Seweryn Grzęźlik     | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Anna Szrakt          | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Damian Szczugor      | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220405-lepsza-kariera-dla-romki)) |
| Helena Banbadan      | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Romana Kundel        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Wojciech Kaznodzieja | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Antos Kuramin        | 1 | ((220405-lepsza-kariera-dla-romki)) |
| Bartek Burbundow     | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Bartek Wudrak        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Berdysz Rozdzieracz  | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Grzegorz Fabutownik  | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Juliusz Cieślawok    | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Kara Prazdnik        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Kazimierz Zamglis    | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Mikołaj Resztkowiec  | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Ursyn Uszat          | 1 | ((220405-lepsza-kariera-dla-romki)) |