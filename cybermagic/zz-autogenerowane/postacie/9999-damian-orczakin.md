---
categories: profile
factions: 
owner: public
title: Damian Orczakin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231122-sen-chroniacy-kochankow      | Inwestor pro-turyzm; chce się pozbyć savaran (i kultystów) ze stacji. On wprowadził paranoizator do stymulantów z nadzieją, że zamieszki. Poszło za ostro, więc potem próbował to neutralizować. | 0105-09-02 - 0105-09-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dorion Fughar        | 1 | ((231122-sen-chroniacy-kochankow)) |
| Felina Amatanir      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Jola-09 Szernief     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Kalista Surilik      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Klasa Dyplomata      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Klasa Hacker         | 1 | ((231122-sen-chroniacy-kochankow)) |
| Klasa Oficer Naukowy | 1 | ((231122-sen-chroniacy-kochankow)) |
| Klaudiusz Widar      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Mawir Hong           | 1 | ((231122-sen-chroniacy-kochankow)) |
| Rovis Skarun         | 1 | ((231122-sen-chroniacy-kochankow)) |
| Szymon Alifajrin     | 1 | ((231122-sen-chroniacy-kochankow)) |