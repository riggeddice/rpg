---
categories: profile
factions: 
owner: public
title: Julia Morwisz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181118-romuald-i-julia              | Emulator wysłana by zaprzyjaźnić się z Pięknotką. Slightly creepy. Zakochała się w Romualdzie i ostro pobiła Pięknotkę. Uzyskała wolność. | 0109-10-27 - 0109-10-29 |
| 181216-wolna-od-terrorforma         | wpierw wykorzystana przez Pięknotkę jako ostateczna linia obrony (zadziałało), potem jako wsparcie do wyciągnięcia (nie zadziałało, Pięknotka wyciągała ją). Oddała kontrolę nad sobą Pięknotce. | 0109-11-05 - 0109-11-11 |
| 181225-czyszczenie-toksycznych-zwiazkow | w trójkąciku z Romualdem i Pięknotką. Źródło informacji dla Ateny. Ogólnie, nędzny los jak na Emulatora. | 0109-11-13 - 0109-11-17 |
| 190724-odzyskana-agentka-orbitera   | opracowała plan uratowania Emulatorki a porwała Aidę. Cóż. Pomogła Pięknotce odkręcić sprawę, ale nie do końca jej wyszło. | 0110-06-12 - 0110-06-15 |
| 190804-niespodziewany-wplyw-aidy    | przekonała kilka osób w Cieniaszczycie do ratowania Emulatorów Orbitera; niestety, Orbiter odcina dostęp środków a Hralwagh zwariował. Przestała robić cokolwiek. | 0110-06-30 - 0110-07-02 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181118-romuald-i-julia              | jest wolna. Jako Emulator wyrwała się spod kontroli koordynatorów, jak długo jest w Cieniaszczycie | 0109-10-29
| 181118-romuald-i-julia              | jest niezdolna do zrobienia jakiejkolwiek krzywdy ani Romualdowi ani Pięknotce - nawet by ratować ich życie, nawet pod kontrolą Krwi. | 0109-10-29
| 181118-romuald-i-julia              | nigdy nie opuści Cieniaszczytu, by koordynatorzy nie odzyskali nad nią kontroli | 0109-10-29
| 181118-romuald-i-julia              | zostaje na stałe z Romualdem Czurukinem; kochają się | 0109-10-29
| 181216-wolna-od-terrorforma         | straszna słabość i oddanie wobec Pięknotki Diakon. Nie potrafi odmówić jej rozkazów. Co więcej, pragnie tych rozkazów. | 0109-11-11
| 181227-adieu-cieniaszczycie         | zostaje Łysym Psem, ale z wyboru. Może mu powiedzieć "nie". Jest jak Waleria a nie jak zabawka. | 0109-12-01

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 5 | ((181118-romuald-i-julia; 181216-wolna-od-terrorforma; 181225-czyszczenie-toksycznych-zwiazkow; 190724-odzyskana-agentka-orbitera; 190804-niespodziewany-wplyw-aidy)) |
| Adam Szarjan         | 2 | ((181118-romuald-i-julia; 190724-odzyskana-agentka-orbitera)) |
| Atena Sowińska       | 2 | ((181216-wolna-od-terrorforma; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Bogdan Szerl         | 2 | ((181216-wolna-od-terrorforma; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Mirela Niecień       | 2 | ((181216-wolna-od-terrorforma; 190724-odzyskana-agentka-orbitera)) |
| Moktar Gradon        | 2 | ((181225-czyszczenie-toksycznych-zwiazkow; 190724-odzyskana-agentka-orbitera)) |
| Romuald Czurukin     | 2 | ((181118-romuald-i-julia; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Aida Serenit         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Amanda Kajrat        | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Gabriel Ursus        | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Mirela Orion         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Ossidia Saitis       | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Pietro Dwarczan      | 1 | ((181118-romuald-i-julia)) |
| Saitaer              | 1 | ((190804-niespodziewany-wplyw-aidy)) |