---
categories: profile
factions: 
owner: public
title: Lucyna Castelli
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240227-cykl-krwawej-przadki         | przybrana córka Michała, dziedziczka jego działań i wpływów. Kiedyś skrzywdzona przez magię, naprawiona w Instytucie. Lojalnie pomaga "ojcu". | 0099-05-22 - 0099-05-28 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 240227-cykl-krwawej-przadki         | zaprzyjaźni się z Różą Kalatrix. One się rozumieją - obie były ofiarami magii. Przejmie bogactwo i potęgę Michała. | 0099-05-28

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Wokniaczek      | 1 | ((240227-cykl-krwawej-przadki)) |
| Edward Kopoktris     | 1 | ((240227-cykl-krwawej-przadki)) |
| Janusz Umizarit      | 1 | ((240227-cykl-krwawej-przadki)) |
| Jola Kalatrix        | 1 | ((240227-cykl-krwawej-przadki)) |
| Michał Castelli      | 1 | ((240227-cykl-krwawej-przadki)) |
| Róża Kalatrix        | 1 | ((240227-cykl-krwawej-przadki)) |
| Wincenty Frak        | 1 | ((240227-cykl-krwawej-przadki)) |