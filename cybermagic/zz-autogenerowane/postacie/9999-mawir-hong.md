---
categories: profile
factions: 
owner: public
title: Mawir Hong
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231122-sen-chroniacy-kochankow      | jeden z bardziej agresywnych drakolitów, pracujący w Energii, niepisany 'szef gangu'. Najmocniej dotknięty Esuriit. | 0105-09-02 - 0105-09-05 |
| 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa | uważa zmianę stanu stacji w Wielki Test Saitaera. Wielokrotnie walczył z Łowcą i nawet go pokonał. Zjadł jego kawałek. | 0105-12-11 - 0105-12-13 |
| 231221-pan-skarpetek-i-odratowany-ogrod | inżynier którego uwagę Inżynier odwrócił kłócąc się o 'te plany są z dupy'. Dzięki temu Oficer Naukowy mógł solo pogadać z Kalistą. | 0106-04-25 - 0106-04-27 |
| 240117-dla-swych-marzen-warto       | chroni Kalistę, ale jak Agencja przekonała go że jej coś jest, to pomógł Agencji. Walczył jak równy z równym z Jonatanem i wyżarł mu fragment barku. Niebezpieczny. | 0106-11-04 - 0106-11-06 |
| 240214-relikwia-z-androida          | Tadeusz błagał go o pomoc by móc się zabić tam gdzie zginęła Alina. Pomógł mu. Pomógł Tadeuszowi, bo Tadeusz ma jaja. Trafił do więzienia, ale go szybko wypuścili za wstawiennictwem Elwiry. | 0107-05-16 - 0107-05-18 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa | miejsce, w którym Łowca się zamanifestował - bardzo osłabiony przez lapis - to były okolice Mawira. Mawir Łowcę pokonał i zjadł jego kawałek. Bardzo podniesiona pozycja. | 0105-12-13
| 240117-dla-swych-marzen-warto       | dotknięty przez ixion, stał się bardzo niebezpiecznym wojownikiem; pożera żywcem anomalie i to go wzmacnia w imię Saitaera | 0106-11-06

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kalista Surilik      | 5 | ((231122-sen-chroniacy-kochankow; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Felina Amatanir      | 4 | ((231122-sen-chroniacy-kochankow; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto)) |
| Klasa Oficer Naukowy | 4 | ((231122-sen-chroniacy-kochankow; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Sabotażysta    | 4 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Aerina Cavalis       | 3 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Dyplomata      | 3 | ((231122-sen-chroniacy-kochankow; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240214-relikwia-z-androida)) |
| Klasa Hacker         | 3 | ((231122-sen-chroniacy-kochankow; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Inżynier       | 3 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240214-relikwia-z-androida)) |
| Dorion Fughar        | 2 | ((231122-sen-chroniacy-kochankow; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Vanessa d'Cavalis    | 2 | ((240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Alina Mekran         | 1 | ((240214-relikwia-z-androida)) |
| Artur Tavit          | 1 | ((240117-dla-swych-marzen-warto)) |
| Damian Orczakin      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Elwira Barknis       | 1 | ((240214-relikwia-z-androida)) |
| Estril Cavalis       | 1 | ((240117-dla-swych-marzen-warto)) |
| Gabriel Septenas     | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Ignatius Sozyliw     | 1 | ((231221-pan-skarpetek-i-odratowany-ogrod)) |
| Jola-09 Szernief     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Kaldor Czuk          | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klaudiusz Widar      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Larkus Talvinir      | 1 | ((240117-dla-swych-marzen-warto)) |
| Maia Sakiran         | 1 | ((240117-dla-swych-marzen-warto)) |
| Rovis Skarun         | 1 | ((231122-sen-chroniacy-kochankow)) |
| Sia-03 Szernief      | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Szymon Alifajrin     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Tadeusz Mekran       | 1 | ((240214-relikwia-z-androida)) |