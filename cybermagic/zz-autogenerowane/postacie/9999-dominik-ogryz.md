---
categories: profile
factions: 
owner: public
title: Dominik Ogryz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200106-infernia-martyn-hiwasser     | komodor Orbitera na Kontrolerze Pierwszym. Porwał Martyna Hiwassera, bo się pokłócili o kobietę. A że przy okazji założył się, że Wioletta przegra a Martyn ją wzmacniał... | 0110-06-28 - 0110-07-03 |
| 200115-pech-komodora-ogryza         | komodor Orbitera na Kontrolerze Pierwszym. Przez kosę z Martynem Hiwasserem i wejście w grę Janet i Klaudii stracił lukratywne miejsce jako polityk i wyruszył w kosmos jako podróżnik ku radości swojej żony. Aha, z Janet. I jednym statkiem mniej. | 0110-07-20 - 0110-07-23 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 2 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza)) |
| Martyn Hiwasser      | 2 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza)) |
| Wioletta Keiril      | 2 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza)) |
| Arianna Verlen       | 1 | ((200106-infernia-martyn-hiwasser)) |
| Janet Erwon          | 1 | ((200115-pech-komodora-ogryza)) |
| Kamil Lyraczek       | 1 | ((200106-infernia-martyn-hiwasser)) |
| Sonia Ogryz          | 1 | ((200115-pech-komodora-ogryza)) |
| Szczepan Olgrod      | 1 | ((200106-infernia-martyn-hiwasser)) |