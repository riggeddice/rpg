---
categories: profile
factions: 
owner: public
title: Kalia Awiter
---

# {{ page.title }}


# Generated: 



## Fiszki


* influencerka z Nativis. Aktywnie próbuje pójść do łóżka z Eustachym i go podbić. Wierzy w Infernię. 21 lat. Drakolitka. | @ 230208-pierwsza-randka-eustachego
* (ENCAO:  +--00 |Stoicka;;Radosna, cieszy się i daje radość;;Dzikie i spontaniczne pomysły| VALS: Universalism, Stimulation, Conformity | DRIVE: Hopebringer: Dać każdemu coś o co warto walczyć) | @ 230208-pierwsza-randka-eustachego
* "Nativis musi być centrum kultury Neikatis. Musimy dać COŚ dlaczego warto żyć. Coś więcej niż tylko przetrwanie!" | @ 230208-pierwsza-randka-eustachego
* (ENCAO:  +--00 |Stoicka;;Radosna, cieszy się i daje radość;;Dzikie i spontaniczne pomysły| VALS: Universalism, Stimulation, Conformity | DRIVE: Zatrzymać czas, zrobić coś dobrego) | @ 230215-terrorystka-w-ambasadorce

### Wątki


kidiron-zbawca-nativis

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230104-to-co-zostalo-po-burzy       | młoda dama, bardzo zainteresowana Eustachym. Chce się z nim umówić na kolację, ale on stawia ;-). | 0092-10-26 - 0092-10-28 |
| 230201-wylaczone-generatory-memoriam-inferni | influencerka z Nativis. Aktywnie próbuje pójść do łóżka z Eustachym i go podbić, ale to on musi poprosić bo ona jest influencerką. | 0093-02-10 - 0093-02-12 |
| 230208-pierwsza-randka-eustachego   | 21-letnia śliczna i inteligentna influencerka / idolka / inspiratorka Nativis. (ENCAO:  +--00 |Stoicka;;Radosna, cieszy się i daje radość;;Dzikie i spontaniczne pomysły| VALS: Universalism, Stimulation, Conformity | DRIVE: Hopebringer: Dać każdemu coś o co warto walczyć). Zmanipulowała loterią by wziąć Eustachego na randkę. Zmartwiona tym, że Eustachy nie ma niczego w arkologii na czym mu naprawdę zależy, chciała dać mu jeden dobry, udany dzień i zlinkować go mocniej z arkologią. Robi solidny research na temat tego co się dzieje. | 0093-02-14 - 0093-02-21 |
| 230215-terrorystka-w-ambasadorce    | dzielnie weszła ratować Ambasadorkę, później stworzyła reportaż mający na celu ujawnienie prawdy o Kidironie i arkologii Lirvint mimo niechęci do Misterii. Współpracowała z Ardillą i pomogła ewakuować osoby z Ambasadorki, będąc gwarantką że nic złego się Misterii nie stanie z ręki Kidirona. Mimo, że jest po stronie Kidirona, współpracując z Misterią stworzyła ruch oporu przeciw Kidironowi. | 0093-02-22 - 0093-02-23 |
| 230329-zdrada-rozrywajaca-arkologie | szybko ściągnięta przez Ardillę by pomóc Ewelinie. Kalia współczuje młodej nastolatce i pomoże jej poprawić reputację i uniknąć najgorszych konsekwencji. Pierwszy raz w życiu zeszła do Szczurowiska, z Ardillą. | 0093-03-14 - 0093-03-16 |
| 230614-atak-na-kidirona             | zostawiła Eustachemu podpowiedź, że Kidiron robi coś groźnego. Jak był zamach na Kidirona, spanikowała gdy została ranna. Po rozmowie z Eustachym, odpaliła fałszywą wiadomość od Kidirona że jest bezpieczny itp. Powiedziała Eustachemu gdzie jest Kidiron. | 0093-03-22 - 0093-03-24 |
| 230621-infiltrator-ucieka-a-arkologia-plonie | porwana przez Dalmjera, służyła jako przedmiot nie podmiot. Silnie straumatyzowana widząc co Dalmjer robi z tymi co wchodzą mu w szkodę. | 0093-03-25 - 0093-03-26 |
| 230719-wojna-o-arkologie-nativis-nowa-regentka | mimo ran, została ewakuowana na Infernię i tam zrobiła akcję propagandową stając za Ardillą. Poszło jej niespodziewanie dobrze i to ONA została tymczasową Regentką Arkologii. | 0093-03-28 - 0093-03-29 |
| 230816-orbiter-nihilus-i-ruch-oporu-w-nativis | konsultuje się z Ardillą w sprawie tego jak pokonać Marzenę Marius (z ruchu oporu). Jako regentka tymczasowo jest symbolem wszystkiego co dobre i piękne. | 0093-03-30 - 0093-03-31 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230208-pierwsza-randka-eustachego   | ma suknię pozwalającą jej na holoprojektor oraz na zmianę wyglądu. Oraz zapewnia wszystkie kamery itp. na media społecznościowe. | 0093-02-21
| 230614-atak-na-kidirona             | została ranna, na szczęście nie bardzo ciężko w zamachu na Kidirona. Za to jest porwana XD. | 0093-03-24
| 230614-atak-na-kidirona             | zna więcej sekretów i skrytek Kidirona niż ktokolwiek inny. | 0093-03-24
| 230719-wojna-o-arkologie-nativis-nowa-regentka | przypadkowo została tymczasową regentką Arkologii Nativis, stoi za nią Ardilla i Infernia. A docelowo - Rafał Kidiron. | 0093-03-29

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eustachy Korkoran    | 9 | ((230104-to-co-zostalo-po-burzy; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230816-orbiter-nihilus-i-ruch-oporu-w-nativis)) |
| Ardilla Korkoran     | 8 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230816-orbiter-nihilus-i-ruch-oporu-w-nativis)) |
| Rafał Kidiron        | 7 | ((230104-to-co-zostalo-po-burzy; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Ralf Tapszecz        | 7 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Bartłomiej Korkoran  | 6 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| OO Infernia          | 6 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| BIA Prometeus        | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Celina Lertys        | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Dalmjer Servart      | 2 | ((230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Franciszek Pietraszczyk | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| Małgorzata Maratelus | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Marcel Draglin       | 2 | ((230719-wojna-o-arkologie-nativis-nowa-regentka; 230816-orbiter-nihilus-i-ruch-oporu-w-nativis)) |
| Tobiasz Lobrak       | 2 | ((230215-terrorystka-w-ambasadorce; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Tymon Korkoran       | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce)) |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Aniela Myszawcowa    | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Anna Seiren          | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Antoni Grzypf        | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Cyprian Kugrak       | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Izabella Saviripatel | 1 | ((230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Jan Lertys           | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| JAN Seiren           | 1 | ((230104-to-co-zostalo-po-burzy)) |
| JAN Uśmiech Kamili   | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Laurencjusz Kidiron  | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Lycoris Kidiron      | 1 | ((230614-atak-na-kidirona)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Marcin Pietraszczyk  | 1 | ((230208-pierwsza-randka-eustachego)) |
| Marzena Marius       | 1 | ((230816-orbiter-nihilus-i-ruch-oporu-w-nativis)) |
| Michał Uszwon        | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Rufus Seiren         | 1 | ((230104-to-co-zostalo-po-burzy)) |
| SAN Szare Ostrze     | 1 | ((230614-atak-na-kidirona)) |
| Stanisław Uczantor   | 1 | ((230614-atak-na-kidirona)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Wojciech Grzebawron  | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Zofia d'Seiren       | 1 | ((230104-to-co-zostalo-po-burzy)) |