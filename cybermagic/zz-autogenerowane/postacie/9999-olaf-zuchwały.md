---
categories: profile
factions: 
owner: public
title: Olaf Zuchwały
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190101-morderczyni-jednej-plotki    | właściciel Góskiej Szalupy. Ma topór. Stylizuje się na kuriozum godne Pustogoru. Fajny facet. Pomógł Pięknotce znaleźć Szurnaka i zrobić pojedynek. | 0109-12-12 - 0109-12-16 |
| 190422-pustogorski-konflikt         | wyrósł na nieformalnego przywódcę Miasteczkowców; negocjował z Pięknotką i powiedział jej o tragedii dwóch Miasteczkowiczanek. | 0110-03-29 - 0110-03-30 |
| 190424-budowa-ixionskiego-mimika    | kiedyś, członek Inwazji Noctis. Teraz sympatyczny barman; postawił się terrorformowi/ mimikowi i powiedział, że nie o to walczyła Eliza. Skończył ciężko ranny. | 0110-03-30 - 0110-04-01 |
| 190429-sabotaz-szeptow-elizy        | kiedyś wysoki oficer Inwazji, teraz spokojny barman chcący współpracować z Pustogorem. Nie chce powrotu wojny. Pomógł Pięknotce w pozyskaniu Kryształu Elizy. | 0110-04-06 - 0110-04-09 |
| 190502-pierwszy-emulator-orbitera   | ma ogromny uraz do Orbitera za to, co stało się Nikoli dawno temu; powiedział Pięknotce, że Nikola nie żyje a to co tam jest to duch. | 0110-04-10 - 0110-04-12 |
| 190622-wojna-kajrata                | który ciężko pracował nad integracją ludzi i magów z Enklaw z Pustogorem, ale jego plany właśnie legły w gruzach przez działania Pięknotki. | 0110-05-14 - 0110-05-17 |
| 190917-zagubiony-efemerydyta        | właściciel baru, który nie chce krzywdy Janka ale nie da się okradać. Współpracuje z Pięknotką by złapać 17-latka. | 0110-06-17 - 0110-06-18 |
| 191103-kontrpolowanie-pieknotki-pulapka | postawił się Pięknotce - nie chce, by Enklawy ucierpiały przez Małmałaza. Powiedział Pięknotce i Kseni to, co one potrzebowały wiedzieć - ale nie zdradził niczego z Enklaw. | 0110-06-20 - 0110-06-27 |
| 200417-nawolywanie-trzesawiska      | znalazł nić porozumienia z Sabiną Kazitan, mówiąc jej, że nawet on - noktianin - został zaakceptowany. Sabina uciekła od niego, bo bała się zaprzyjaźnić. | 0110-08-08 - 0110-08-10 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190424-budowa-ixionskiego-mimika    | tydzień w szpitalu | 0110-04-01

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 9 | ((190101-morderczyni-jednej-plotki; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 190622-wojna-kajrata; 190917-zagubiony-efemerydyta; 191103-kontrpolowanie-pieknotki-pulapka; 200417-nawolywanie-trzesawiska)) |
| Erwin Galilien       | 4 | ((190101-morderczyni-jednej-plotki; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Minerwa Metalia      | 4 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Karla Mrozik         | 3 | ((190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Alan Bartozol        | 2 | ((190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera)) |
| Aleksander Rugczuk   | 2 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Gabriel Ursus        | 2 | ((190917-zagubiony-efemerydyta; 200417-nawolywanie-trzesawiska)) |
| Marek Puszczok       | 2 | ((190917-zagubiony-efemerydyta; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Nikola Kirys         | 2 | ((190502-pierwszy-emulator-orbitera; 190622-wojna-kajrata)) |
| Aleksander Iczak     | 1 | ((190101-morderczyni-jednej-plotki)) |
| Bożymir Szczupak     | 1 | ((190502-pierwszy-emulator-orbitera)) |
| Damian Orion         | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Diana Tevalier       | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Eliza Ira            | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Ernest Kajrat        | 1 | ((190622-wojna-kajrata)) |
| Ignacy Myrczek       | 1 | ((200417-nawolywanie-trzesawiska)) |
| Jan Uszczar          | 1 | ((190917-zagubiony-efemerydyta)) |
| Józef Małmałaz       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Karol Szurnak        | 1 | ((190101-morderczyni-jednej-plotki)) |
| Ksenia Kirallen      | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Lilia Ursus          | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Liliana Bankierz     | 1 | ((190622-wojna-kajrata)) |
| Lucjusz Blakenbauer  | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Marcel Nieciesz      | 1 | ((190422-pustogorski-konflikt)) |
| Mariusz Trzewń       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Napoleon Bankierz    | 1 | ((200417-nawolywanie-trzesawiska)) |
| Rafał Roszczeniok    | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Sabina Kazitan       | 1 | ((200417-nawolywanie-trzesawiska)) |
| Serafina Ira         | 1 | ((190622-wojna-kajrata)) |
| Strażniczka Alair    | 1 | ((200417-nawolywanie-trzesawiska)) |
| Teresa Mieralit      | 1 | ((190101-morderczyni-jednej-plotki)) |
| Wojmił Siwywilk      | 1 | ((190422-pustogorski-konflikt)) |