---
categories: profile
factions: 
owner: public
title: ON Spatium Gelida
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210825-uszkodzona-brama-eteryczna   | Superpancernik noktiański, który nie dotarł na pole bitwy, uwięziony pomiędzy Bramami przez pasożyty sił specjalnych Orbitera. Nadal tam jest. | 0112-02-04 - 0112-02-07 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210825-uszkodzona-brama-eteryczna   | ma tether na Infernię po stronie Sektora Astoriańskiego. Ma skonfundowany sygnał - Infernia? Alivia Nocturna? Ale ma i się obudził. | 0112-02-07

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Nocna Krypta      | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Arianna Verlen       | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Diana d'Infernia     | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Elena Verlen         | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Eustachy Korkoran    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Flawia Blakenbauer   | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Gilbert Bloch        | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Klaudia Stryk        | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Medea Sowińska       | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Infernia          | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |