---
categories: profile
factions: 
owner: public
title: Kasia Pieprz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210912-medyk-przeciwko-cieniaszczytowi | siostra Pawła; KIA. Sympatyczna dziewczyna bez szczególnych cech, która była kompatybilna z krwią Lewiatana nad którą pracowali BlackTech. Porwana, przeszła proces adaptacji nieco za wcześnie (przez działania Martyna) i zgineła. | 0079-09-13 - 0079-09-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adela Kołczan        | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Adrian Kozioł        | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Franciszek Tocz      | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Irena Czakram        | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Martyn Hiwasser      | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Paweł Pieprz         | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |