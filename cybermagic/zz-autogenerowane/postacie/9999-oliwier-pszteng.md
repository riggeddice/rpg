---
categories: profile
factions: 
owner: public
title: Oliwier Pszteng
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191120-mafia-na-stacji-gorniczej    | As Pilotażu. Pobrał od Stefana próbkę. Nieustraszony, czasem szaleńczy. Wprowadził biorobota na Arkę Kuratorów - co prawie przypłacił życiem. Musiał być ewakuowany czarem strategicznym. | 0110-06-14 - 0110-06-25 |
| 191218-kijara-corka-szotaron        | as pilotażu Dorszant; poświęcił kilku skrzydłowych by tylko ratować rodzinę Filipa. Starł się z Kijarą - i przegrał, acz uleciał z życiem. | 0110-07-04 - 0110-07-07 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eszara d'Dorszant    | 2 | ((191120-mafia-na-stacji-gorniczej; 191218-kijara-corka-szotaron)) |
| Jaromir Uczkram      | 2 | ((191120-mafia-na-stacji-gorniczej; 191218-kijara-corka-szotaron)) |
| Rafał Kirlat         | 2 | ((191120-mafia-na-stacji-gorniczej; 191218-kijara-corka-szotaron)) |
| Szymon Szelmer       | 2 | ((191120-mafia-na-stacji-gorniczej; 191218-kijara-corka-szotaron)) |
| Azonia Arris         | 1 | ((191218-kijara-corka-szotaron)) |
| Felicja Taranit      | 1 | ((191218-kijara-corka-szotaron)) |
| Filip Szczatken      | 1 | ((191218-kijara-corka-szotaron)) |
| Gerwazy Kruczkut     | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Hestia d'Dorszant    | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Janet Erwon          | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Kamelia Termit       | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Kijara d'Esuriit     | 1 | ((191218-kijara-corka-szotaron)) |
| Stefan Ukrand        | 1 | ((191120-mafia-na-stacji-gorniczej)) |