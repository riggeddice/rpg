---
categories: profile
factions: 
owner: public
title: Uśmiechniczka Konstrukcjonistka Aurora Diakon
---

# {{ page.title }}


# Generated: 



## Fiszki


* świetny inżynier, 'engineering can be very sexy!', nadspodziewanie dobra w walce wręcz | @ 231018-anomalne-awarie-athamarein
* OCEAN: E+C+O+ | Zero barier i nieobliczalna;; Niezwykle konsekwentna i długoterminowa w celach;; Potrafi wiele wytrzymać | VALS: Face, Stimulation | Drive: Zostanę influencerką | @ 231018-anomalne-awarie-athamarein

### Wątki


legenda-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231018-anomalne-awarie-athamarein   | 'Ula', robi sobie selfie z Arianną i ogólnie nie przeszkadza jej stan Athamarein. Jest dobra, ale skupia się na sławie a nie Athamarein, generując sporo problemów (załoga vs oficerowie). | 0111-12-22 - 0111-12-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alicja Szadawir      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Andrzej Gwozdnik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Arianna Verlen       | 1 | ((231018-anomalne-awarie-athamarein)) |
| Błażej Sowiński      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Elena Verlen         | 1 | ((231018-anomalne-awarie-athamarein)) |
| Eustachy Korkoran    | 1 | ((231018-anomalne-awarie-athamarein)) |
| Hiacynt Samszar      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Jakub Oroginiec      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Kajetan Kircznik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Klaudia Stryk        | 1 | ((231018-anomalne-awarie-athamarein)) |
| Leona Astrienko      | 1 | ((231018-anomalne-awarie-athamarein)) |
| OO Athamarein        | 1 | ((231018-anomalne-awarie-athamarein)) |
| Remigiusz Alkarenit  | 1 | ((231018-anomalne-awarie-athamarein)) |