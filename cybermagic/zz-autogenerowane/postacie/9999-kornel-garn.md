---
categories: profile
factions: 
owner: public
title: Kornel Garn
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190112-eksperymenty-na-wilach       | były wojskowy ekstremista badający wiły i świetnie walczący z Alanem. Zobaczył majestat Saitaera i także chce wykorzystać potęgę boga. Prawie pokonał Alana. | 0109-12-28 - 0109-12-31 |
| 190206-nie-da-sie-odrzucic-mocy     | by chronić Minerwę, pomógł Tymonowi w odparciu ixiońskiej infekcji Cyberszkoły. Po czym zniknął. Jego ruchy i działania są całkowicie niemożliwe do zrozumienia czy przewidzenia. | 0110-02-17 - 0110-02-20 |
| 190210-minerwa-i-kwiaty-nadziei     | używając wiedzy Saitaera, dostał się na Grazoniusza i wyhodował Ixiońskie Kwiaty Nadziei. Pokazał je Minerwie. Próbował pozyskać Minerwę, ale Pięknotka weszła mu w szkodę. Musiał uciekać. | 0110-02-21 - 0110-02-23 |
| 190817-kwiaty-w-sluzbie-puryfikacji |  | 0110-07-04 - 0110-07-09 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190112-eksperymenty-na-wilach       | ma dość wił i materiału by Kontynuować pracę nad Kwiatami Hipnotycznymi w swoim laboratorium. | 0109-12-31
| 190210-minerwa-i-kwiaty-nadziei     | persona non grata w Pustogorze i okolicach; nie polują na niego, ale nie powinien się tu pojawiać (działania Pięknotki i Kasjopei) | 0110-02-23
| 190210-minerwa-i-kwiaty-nadziei     | zabezpieczył sobie pomoc Minerwy Metalii; Minerwa mu pomoże we wszystkich kwestiach związanych z Black Technology (jeśli zbudują kontakt) | 0110-02-23

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 4 | ((190112-eksperymenty-na-wilach; 190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei; 190817-kwiaty-w-sluzbie-puryfikacji)) |
| Erwin Galilien       | 2 | ((190210-minerwa-i-kwiaty-nadziei; 190817-kwiaty-w-sluzbie-puryfikacji)) |
| Kasjopea Maus        | 2 | ((190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei)) |
| Minerwa Metalia      | 2 | ((190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei)) |
| Alan Bartozol        | 1 | ((190112-eksperymenty-na-wilach)) |
| Arnulf Poważny       | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Ataienne             | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Atena Sowińska       | 1 | ((190210-minerwa-i-kwiaty-nadziei)) |
| Karolina Erenit      | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Marlena Maja Leszczyńska | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Nataniel Marszalnik  | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Saitaer              | 1 | ((190112-eksperymenty-na-wilach)) |
| Sławomir Muczarek    | 1 | ((190112-eksperymenty-na-wilach)) |
| Teresa Marszalnik    | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Tymon Grubosz        | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |