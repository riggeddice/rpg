---
categories: profile
factions: 
owner: public
title: Jan Firatiel
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230626-pustynny-final-igrzysk       | Infiltrator, zostawiony w bardzo trudnej sytuacji - jedyny z zespołu był w stanie sobie poradzić. Czuł się odpowiedzialny za swój zespół i zakradł się do zespołu Olafa by zobaczyć kto to. Wszedł z nimi w sojusz gdy zaatakował morderczy zespół. | 0083-10-12 - 0083-10-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Klart           | 1 | ((230626-pustynny-final-igrzysk)) |
| Antoni Kmandir       | 1 | ((230626-pustynny-final-igrzysk)) |
| Bogdan Ubuddan       | 1 | ((230626-pustynny-final-igrzysk)) |
| Lily Sanarton        | 1 | ((230626-pustynny-final-igrzysk)) |
| Maja Wurmramin       | 1 | ((230626-pustynny-final-igrzysk)) |
| Maks Ardyceń         | 1 | ((230626-pustynny-final-igrzysk)) |
| Olaf Zuchwały        | 1 | ((230626-pustynny-final-igrzysk)) |
| Rafał Armadion       | 1 | ((230626-pustynny-final-igrzysk)) |
| Roman Wyrkmycz       | 1 | ((230626-pustynny-final-igrzysk)) |