---
categories: profile
factions: 
owner: public
title: Axel Nargan
---

# {{ page.title }}


# Generated: 



## Fiszki


* zainfekowany kapitan Dominy Lucis (dekadianin) | @ 221130-astralna-flara-w-strefie-duchow
* ENCAO:  00+-- |Agresywny;; Bezpośredni| VALS: Security, Power | DRIVE: Zwyciężyć w sporze | @ 221130-astralna-flara-w-strefie-duchow
* "We are immortal" | @ 221214-astralna-flara-kontra-domina-lucis

### Wątki


historia-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221214-astralna-flara-kontra-domina-lucis | KIA; dekadianin i kapitan Dominy Lucis; dobry strateg, ale w obliczu nekroTAI i "służenia żywy lub martwy" nie był w stanie wiele zrobić. Pozwolił odejść noktianom którzy chcieli, potem w obliczu Zarralei dał się zabić serpentisowi. | 0100-10-09 - 0100-10-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Daria Czarnewik      | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Ellarina Samarintael | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Gabriel Lodowiec     | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Kirea Rialirat       | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Leszek Kurzmin       | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| NekroTAI Zarralea    | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| OO Astralna Flara    | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| OO Athamarein        | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Sarian Xadaar        | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Tristan Rialirat     | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |