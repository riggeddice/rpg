---
categories: profile
factions: 
owner: public
title: Sławomir Muczarek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190112-eksperymenty-na-wilach       | Członek Czerwonych Myszy, któremu nie podobało się przemycanie wił i uznał to za niebezpieczne. Przez to zostawił Pięknotce ślad. | 0109-12-28 - 0109-12-31 |
| 190119-skorpipedy-krolewskiego-xirathira | troszkę atakował Olgę zwykłymi ludźmi; za to zmodyfikowany skorpiped zrobił mu poważną ranę i dopiero Wiktor Satarail zatrzymał skorpipeda przed egzekucją Sławka. | 0110-01-14 - 0110-01-15 |
| 190202-czarodziejka-z-woli-saitaera | ninja który na prośbę Adeli pomógł Karolinie i zdestabilizował energię magiczną Wojtka - dzięki czemu mógł zadziałać Saitaer. | 0110-02-01 - 0110-02-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 3 | ((190112-eksperymenty-na-wilach; 190119-skorpipedy-krolewskiego-xirathira; 190202-czarodziejka-z-woli-saitaera)) |
| Adela Kirys          | 2 | ((190119-skorpipedy-krolewskiego-xirathira; 190202-czarodziejka-z-woli-saitaera)) |
| Saitaer              | 2 | ((190112-eksperymenty-na-wilach; 190202-czarodziejka-z-woli-saitaera)) |
| Alan Bartozol        | 1 | ((190112-eksperymenty-na-wilach)) |
| Karolina Erenit      | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
| Kornel Garn          | 1 | ((190112-eksperymenty-na-wilach)) |
| Minerwa Metalia      | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
| Olga Myszeczka       | 1 | ((190119-skorpipedy-krolewskiego-xirathira)) |
| Tymon Grubosz        | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
| Waldemar Mózg        | 1 | ((190119-skorpipedy-krolewskiego-xirathira)) |
| Wiktor Satarail      | 1 | ((190119-skorpipedy-krolewskiego-xirathira)) |
| Wojtek Kurczynos     | 1 | ((190202-czarodziejka-z-woli-saitaera)) |