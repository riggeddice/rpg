---
categories: profile
factions: 
owner: public
title: Rafał Kirlat
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191120-mafia-na-stacji-gorniczej    | Inżynier Zniszczenia. Z pomocą Kamelii zmienił biorobota w młot na kuratorów. Ogólnie, twardy i nie dający żadnych szans przetrwać czemukolwiek zagrażającemu Stacji. | 0110-06-14 - 0110-06-25 |
| 191218-kijara-corka-szotaron        | inżynier zniszczenia Dorszant; opracował genialny sposób odepchnięcia Kijary bombą, dzięki czemu udało się Oliwierowi uratować Azonię. | 0110-07-04 - 0110-07-07 |
| 191123-echo-eszary-na-dorszancie    | Inżynier zniszczenia i mafiozo na Dorszancie. Rozwalił potwora ze ścieków, a potem rozwalił armię drugiej Hestii i zmusił ją do negocjacji. Twardy i nie mający litości. | 0110-07-11 - 0110-07-14 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eszara d'Dorszant    | 3 | ((191120-mafia-na-stacji-gorniczej; 191123-echo-eszary-na-dorszancie; 191218-kijara-corka-szotaron)) |
| Filip Szczatken      | 2 | ((191123-echo-eszary-na-dorszancie; 191218-kijara-corka-szotaron)) |
| Gerwazy Kruczkut     | 2 | ((191120-mafia-na-stacji-gorniczej; 191123-echo-eszary-na-dorszancie)) |
| Jaromir Uczkram      | 2 | ((191120-mafia-na-stacji-gorniczej; 191218-kijara-corka-szotaron)) |
| Kamelia Termit       | 2 | ((191120-mafia-na-stacji-gorniczej; 191123-echo-eszary-na-dorszancie)) |
| Oliwier Pszteng      | 2 | ((191120-mafia-na-stacji-gorniczej; 191218-kijara-corka-szotaron)) |
| Szymon Szelmer       | 2 | ((191120-mafia-na-stacji-gorniczej; 191218-kijara-corka-szotaron)) |
| Azonia Arris         | 1 | ((191218-kijara-corka-szotaron)) |
| Felicja Taranit      | 1 | ((191218-kijara-corka-szotaron)) |
| Hestia Ain d'Dorszant | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Hestia d'Dorszant    | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Hestia Dis d'Dorszant | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Janet Erwon          | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Kijara d'Esuriit     | 1 | ((191218-kijara-corka-szotaron)) |
| Stefan Ukrand        | 1 | ((191120-mafia-na-stacji-gorniczej)) |