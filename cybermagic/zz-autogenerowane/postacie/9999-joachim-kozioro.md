---
categories: profile
factions: 
owner: public
title: Joachim Kozioro
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180708-niewidzialne-potwory-z-rzeki | terminus rezydentki; purytański i poważny, próbuje własnymi siłami ochronić cały teren i pilnuje by Magda była godną rezydentką. Nie waha się prosić o pomoc. | 0109-08-18 - 0109-08-20 |
| 180718-msciwa-ryba-z-eteru          | dzielnie walczył Materią przeciwko inkarnacjom Kamiennej Ryby w formie golemów i ratował gości oraz dworek Magdy. Wściekły na Majkę jak mało kto. | 0109-08-25 - 0109-08-27 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180718-msciwa-ryba-z-eteru          | uważa, że nie może taka mała Majka po prostu pomiatać Rezydentką i nim. Trzeba jej odpłacić w jakiś sposób i należycie ukarać... | 0109-08-27

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kalina Rotmistrz     | 2 | ((180708-niewidzialne-potwory-z-rzeki; 180718-msciwa-ryba-z-eteru)) |
| Magda Patiril        | 2 | ((180708-niewidzialne-potwory-z-rzeki; 180718-msciwa-ryba-z-eteru)) |
| Antoni Zajcew        | 1 | ((180708-niewidzialne-potwory-z-rzeki)) |
| Borys Perikas        | 1 | ((180718-msciwa-ryba-z-eteru)) |
| Lawenda Weiner       | 1 | ((180718-msciwa-ryba-z-eteru)) |
| Majka Perikas        | 1 | ((180718-msciwa-ryba-z-eteru)) |
| Stach Sosnowiecki    | 1 | ((180708-niewidzialne-potwory-z-rzeki)) |