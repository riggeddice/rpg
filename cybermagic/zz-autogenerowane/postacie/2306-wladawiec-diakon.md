---
categories: profile
factions: 
owner: public
title: Władawiec Diakon
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "diakon"
* owner: "public"
* title: "Władawiec Diakon"

## Mechanika

* **Obietnica**
    * "Przyciągniesz wszystkie oczy i będziesz kompetentny w każdej roli na statku kosmicznym. Twoje zrozumienie innych i manipulowanie ich ruchami będzie niezwykłe. I genialnie walczysz w zwarciu."
    * "Nieważne gdzie się nie znajdziesz, będziesz mieć władzę nad ludźmi. Staniesz się szarą eminencją. Będą słuchać Twych słów a kobiety - zwłaszcza dumne i potężne - będą na Twoje skinienie."
* **Rdzeń mechaniczny**
    * **Mistrzostwo (1-3)**
        * GDY pojawi się dama która ma być Twoja, TO ją usidlisz - feromonami, słowami, zachowaniem. Dopasujesz się do jej marzeń. _Nie rozumiesz... one służąc z Władawcem robiły RZECZY... i tęsknią._
        * GDY wchodzisz w umysł drugiej osoby, TO rozumiesz ją dogłębnie; czy to magią czy empatią. _Nikt tak Cię nie zrozumie i nie wysłucha jak on. Zawsze czujesz się przy nim bezpieczna._
        * GDY walczysz w zwarciu poza servarem, TO wygrywasz starcia; walka wręcz, broń biała czy broń krótka. _Świetnie walczy i to w niesamowitym stylu._
    * **Kompetencje (3-5)**
        * GDY wchodzi na salę, TO wszystkie oczy ma na sobie. Elegancja, gravitas i styl. _Wygląda i zachowuje się jak prawdziwy dowódca, w stresie czy w akcji._ 
        * GDY jest na statku kosmicznym, TO potrafi pełnić praktycznie każdą rolę na mostku. _Świetny generalista, zawsze przydatny i pomocny._
        * GDY pojawia się panika, TO przejmuje kontrolę i kieruje wszystkich w odpowiednią stronę. _Jego ludzie są mu oddani i ich morale się nie łamie - zadowolą go za wszelką cenę._
    * **Agenda i styl działania (3-5)**
        * PRAGNIE mieć wszystko co istotne pod kontrolą. Ciało, serce i duszę. Wszystko ma działać perfekcyjnie. Wszystko ma być piękne. _Zapewni Ci utopię - jeśli pragniesz szczęścia, on nim jest._
        * GDY pojawia się nowa osoba w jego domenie, TO znajdzie czego ta osoba pragnie i jej to zapewni - za nieskończoną lojalność. _Wszystko będzie kontrolował - od snów po najmniejszy szczegół._
        * GDY pojawia się dama która z nim konkuruje lub jest wyzwaniem, TO powoli ją usidli aż stanie się jego akolitką. _Naturalnym dla kobiet jest ulec silnemu mężczyźnie. Z ich własnej woli._
        * GDY widzi strach, TO pomoże Ci go przekroczyć i przesunąć się dalej. _Prowadzi do perfekcji we wszystkim co robi, jeszcze bardziej Cię usidlając._
    * **Słabości (1-5)**
        * GDY widzi damę w jego profilu, TO musi spróbować ją usidlić. _Jego słabością są silne kobiety. Nie zostają silne długo, ale dlatego nie będzie nigdy kapitanem._
        * GDY czegoś naprawdę pragnie lub się nudzi, TO wykorzystuje swoje umiejętności w mroczny sposób. _Nigdy nie zapisuj się na misję patrolową z nim na pokładzie. Zaufaj mi, to zły pomysł._
    * **Zasoby (3-5)**
        * Reputacja doskonałego XO, acz takiego któremu ulegają wszelkie damy. Przez to jego reputacja jest tyci toksyczna dla kobiet.
        * Reputacja - jak się pojawia na statku kosmicznym, statek w pewnym momencie zaczyna działać dużo lepiej.

## Reszta

### Fiszka

* Władawiec Diakon: tien, corruptor of ladies, duelist, second-in-command na statku kosmicznym (XO)
    * ENCAO: (E+C+) | intrygancki;; kontrolowany wulkan;; pająk w sieci | VALS: Hedonism, Power | DRIVE: Perfekcja Zaufania
    * styl: nieskazitelny oficer, delikatny uśmiech, niezmiennie pewny. Gabriel Durindal (Gundam Seed Destiny)
    * piosenka wiodąca: Epica "Obsessive Devotion"

### W kilku zdaniach

* .

### Jak sterować postacią

* **O czym myśli**
    * "Skuteczna organizacja i prawdziwa władza wymaga personalnego oddania personelu wobec przełożonego. I dostarczę im tego czego potrzebują."
    * "Pokażę Orbiterowi, w jaki sposób należy zmienić organizację. Jak to zrobić, by wszystko działało prawidłowo."
* **Serce**
    * **Wound**
        * **Core Wound**: ""
        * **Core Lie**: ""
    * **ENCAO**: E+C+
        * "Każda sytuacja jest możliwa do rozwiązania - wymaga tylko odpowiedniego podejścia i adaptacji."
        * "Każda dziewczyna pragnie mi służyć. To normalne prawo natury."
    * **Wartości**: Hedonism, Power
        * H: "Nie ma nic piękniejszego niż dumna dama, która w rzeczywistości nie może się doczekać aż zostaniemy sam na sam."
        * P: "Moim spadkiem będzie sprawny Orbiter, doskonałe załogi i zwycięstwo. Osiągnę to przez UWOLNIENIE załóg. Będą wdzięczni MNIE."
        * HP: "Prawdziwa władza i ekstaza to nie łóżko - to absolutna kontrola drugiej osoby, wszystkich elementów, każdego snu i ruchu."
    * **DELTA_DRIVE**: 
        * **Stan aktualny**: "Jestem doskonałym pierwszym oficerem na jednostce, na której jestem skuteczny."
        * **Stan oczekiwany**: "Statek kosmiczny - a potem Orbiter - jest jednym organizmem, który musi sobie idealnie ufać i w pełni współpracować ze sobą. Ze mną."
    * **Metakultura**: drakolita. "_Moje słowo jest święte i moja perfekcja musi być absolutna. Więc obiecam Ci że się Tobą zaopiekuję - jeśli ulegniesz._"
    * **Kolory**: UB "Każda istota ma prawo do ewolucji. Każda dziewczyna ma prawo do ewolucji. Ja mam obowiązek dążenia do władzy absolutnej. A to prowadzi do brania czego pragnę."
    * **Wzory**: 
        * (Gabriel Durindal) MIX (Altena) MIX (Kiryuu)
        * Próbuje być pozytywną siłą, ale żąda za to odpowiedniego zachowania w wymianie.
    * **Inne**:
        * .

## Możliwości

* **Dominujące Strategie**: 
    * Corruption of Angels. Mediacja. Kestra'chern
    * Magia mentalna, dominacja, The Spider
    * Inspiracja własnym przykładem
    * Mimikra do potrzeb i adaptacja, by druga strona za nim poszła. Użyje uczuć drugiej strony przeciw niej.
* **Umocowanie w świecie**: 
    * ?

## Magia
### Dominująca moc

* JEDNYM ZDANIEM: 
    * _manipulation of souls_; perfekcyjna mimikra i wejście w umysł drugiej osoby by stopniowo ją zmieniać. Czytanie myśli, obecność w snach i feromony.
* AFFINITY
    * Esuriit, Praecis, elementy Ixion (z Rodu)
        * w jego wypadku Alucis -> Praecis, Esuriit. 
            * Nie chce koić, chce rządzić i mieć świat pod kontrolą
            * Skupia się na pożądaniu i głodzie, na uczuciach ROZPACZLIWEJ POTRZEBY
            * Praecis, Ixion ORAZ Esuriit działają tylko w domenie mentalnej i 'ultrafizycznej' (np. _ars amandi_)
        * Ixion dotyczy jedynie jego własnej bioformy
            * Zmiana i dostosowanie siebie do _ars amandi_, konstrukcja feromonów
            * Zmiana swojego zachowania pod kątem tego co jest potrzebne, perfekcyjny mimik-corruptor

#### Jak się objawia utrata kontroli

* 


# Generated: 



## Fiszki


* pierwszy oficer, tien, (p.o. Stefana) | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* ENCAO:  +-0-0 |Intrygancki;;Żywy wulkan| VALS: Hedonism, Self-direction| DRIVE: Follow My Dreams, Korupcja anioła | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* tien, corruptor of ladies, duelist, second-in-command na statku kosmicznym (XO) | @ 240110-wieczna-wojna-bladawira
* ENCAO: (E+C+) | intrygancki;; kontrolowany wulkan;; pająk w sieci | VALS: Hedonism, Power | DRIVE: Perfekcja Zaufania | @ 240110-wieczna-wojna-bladawira
* styl: nieskazitelny oficer, delikatny uśmiech, niezmiennie pewny. Gabriel Durindal (Gundam Seed Destiny) | @ 240110-wieczna-wojna-bladawira
* piosenka wiodąca: Epica "Obsessive Devotion" | @ 240110-wieczna-wojna-bladawira

### Wątki


kosmiczna-chwala-arianny
triumfalny-powrot-arianny
program-kosmiczny-aurum
naprawa-swiata-przez-bladawira

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly | pierwszy oficer Królowej; zdominował i scorruptował Alezję i 'wszystkie spódniczki jego'. Zdecydował się nie sabotować Arianny jeśli ona mu nie przeszkadza. Nie chce oddać holdu na Alezji - jego zabawka. | 0100-05-06 - 0100-05-12 |
| 220928-kapitan-verlen-i-pojedynek-z-marine | okazuje się, że na pokładzie są cztery osoby które go nie kochają - Daria, Arianna (nowe), Leona (scaaary) i Grażyna (nie spełnia klasycznych kanonów). He did stuff to water/food... | 0100-05-14 - 0100-05-16 |
| 221012-kapitan-verlen-i-niezapowiedziana-inspekcja | wpuścił na Infernię inspektora Adama, ale chronił Ariannę (nie wiedział o inspekcji). Powiedział jej, że nie złamał porozumienia. Postara się, by wszystko co stało się na jednostce zostało na jednostce. | 0100-05-17 - 0100-05-21 |
| 221102-astralna-flara-i-porwanie-na-karsztarinie | chce się wykazać przed Arianną i być najlepszym oficerem ever. Acz podrywanie lasek na propsie, choć bez ekstra środków. CEL: Elena. | 0100-07-28 - 0100-08-03 |
| 221116-astralna-flara-dociera-do-nonariona-nadziei | zachowuje absolutny optymizm i rozpoczął ćwiczenia zapasów z Eleną i z marines. Skutecznie skanuje asteroidy; pełni rolę eksperta od radarów i detektorów. | 0100-09-08 - 0100-09-11 |
| 221123-egzotyczna-pieknosc-na-astralnej-flarze | oglądał przesłuchanie Ellariny. Docenił jej umiejętności manipulacyjne i jej wpływ na morale. Uczy się, jak do niej podejść by ją kontrolować dla Arianny - może być jedyną osobą zdolną do przewidzenia jej ruchów i kontroli Ellariny na Flarze. | 0100-09-12 - 0100-09-15 |
| 221221-astralna-flara-i-nowy-komodor | przekonał Elenę do tańca z szablami i zrobienia epickiego widowiska. Został może ranny, ale pokazał Elenie jak można to zrobić, wyciągnął ją troszkę ze skorupki i pokazał jej PIĘKNO tańca. Zbliża ją do siebie i siebie do niej. | 0100-11-07 - 0100-11-10 |
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | osoba uspokajająca Lanę, by dało się jej przeczytać pamięć i poznać prawdę odnośnie dziwnej anomalicznej jednostki (Nox Ignis) | 0100-11-13 - 0100-11-16 |
| 240110-wieczna-wojna-bladawira      | dzięki machinacjom Klaudii, jest dostępny na Infernię. W swoim stylu podrywa Ariannę i z przyjemnością dołączy do niej na Inferni pomóc jej z Zaarą. | 0110-12-24 - 0110-12-27 |
| 240124-mikiptur-zemsta-woltaren     | bezwzględnie flirtuje z Arianną, ale postawił ją wtedy kiedy powinien tak jak powinien. | 0110-12-29 - 0111-01-01 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 221102-astralna-flara-i-porwanie-na-karsztarinie | będzie flirtował z Eleną, will make her his. Ale bez użycia specjalnych technik, samym urokiem i umiejętnościami. | 0100-08-03

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 10 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 240110-wieczna-wojna-bladawira; 240124-mikiptur-zemsta-woltaren)) |
| Daria Czarnewik      | 8 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Maja Samszar         | 7 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Arnulf Perikas       | 6 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Grażyna Burgacz      | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kajetan Kircznik     | 5 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leszek Kurzmin       | 5 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alezja Dumorin       | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| OO Astralna Flara    | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Szczepan Myrczek     | 4 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Elena Verlen         | 3 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Erwin Pies           | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Leona Astrienko      | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| OO Królowa Kosmicznej Chwały | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Szymon Wanad         | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Antoni Bladawir      | 2 | ((240110-wieczna-wojna-bladawira; 240124-mikiptur-zemsta-woltaren)) |
| Ellarina Samarintael | 2 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor)) |
| Gabriel Lodowiec     | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gerwazy Kircznik     | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hubert Kerwelenios   | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Klarysa Jirnik       | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Klaudia Stryk        | 2 | ((240110-wieczna-wojna-bladawira; 240124-mikiptur-zemsta-woltaren)) |
| Marcel Kulgard       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Mariusz Bulterier    | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Loricatus         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Salazar Bolza        | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| SCA Hadiah Emas      | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Tomasz Ruppok        | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Ada Wyrocznik        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Adam Chrząszczewicz  | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Dormand Miraris      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Ernest Bankierz      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Eustachy Korkoran    | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Kaspian Certisarius  | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Kazimierz Darbik     | 1 | ((240110-wieczna-wojna-bladawira)) |
| Klaudiusz Terienak   | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Lars Kidironus       | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Leo Kasztop          | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Marian Witaczek      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| NekroTAI Zarralea    | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| OA Mikiptur          | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| OO Isratazir         | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| OO Karsztarin        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Raoul Lavanis        | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Samuel Fanszakt      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Stefan Torkil        | 1 | ((220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Talia Miraris        | 1 | ((240110-wieczna-wojna-bladawira)) |
| Tomasz Dojnicz       | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Zaara Mieralit       | 1 | ((240110-wieczna-wojna-bladawira)) |