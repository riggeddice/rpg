---
categories: profile
factions: 
owner: public
title: Adam Cześń
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200513-trzyglowec-kontra-melinda    | chłopak z Trzygłowca, który chciał odejść i przez machinacje Grabarza został wplątany w to, że koledzy myśleli że ich zdradził - i chcieli go zbić. | 0109-11-08 - 0109-11-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Diana Lemurczak      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Feliks Keksik        | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Jan Łowicz           | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Katja Nowik          | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Kinga Kruk           | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Mariusz Grabarz      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Melinda Teilert      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Natasza Aniel        | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Rafał Torszecki      | 1 | ((200513-trzyglowec-kontra-melinda)) |