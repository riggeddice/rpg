---
categories: profile
factions: 
owner: public
title: Marcinozaur Verlen
---

# {{ page.title }}


# Generated: 



## Fiszki


* mag, skrajny sybrianin, mag iluzji i katai | @ 230412-dzwiedzie-poluja-na-ser
* (ENCAO: +-+00 | Don't worry, be happy;;Żarliwy, skłonny do wydania ogromnej ilości energii;;Punktualny| VALS: Face, Stimulation >> Humility| DRIVE: Nawracanie na VERLENIZM) | @ 230412-dzwiedzie-poluja-na-ser
* styl: Alex Louis Armstrong x Volo Guide To Monsters. | @ 230412-dzwiedzie-poluja-na-ser
* mistrz szpiegów Verlenlandu. Apex szpiegator. | @ 230412-dzwiedzie-poluja-na-ser
* Sam sobie zmienił imię. Miał "Marcin". Był JEDYNYM Verlenem z normalnym imieniem, ale chciał pójść jeszcze efektowniej. | @ 230412-dzwiedzie-poluja-na-ser
* typowa reakcja: "nazywasz się Maciuś, jak brzuszek, bo tylko jesz. CHODŹ BIEGAĆ PO LESIE!" (do kogoś kto nazywa się inaczej niż Maciek) | @ 230412-dzwiedzie-poluja-na-ser
* chłopak Uli Blakenbauer | @ 230412-dzwiedzie-poluja-na-ser

### Wątki


przeznaczenie-eleny-verlen

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230412-dzwiedzie-poluja-na-ser      | ma teraz 27 lat. SZPIEGATOR. Troszczy się o Elenę i o Verlenów; chce by Apollo przeprosił Elenę, chce pokazać Verlenom siłę Blakenbauerów (stąd Ula i jej płaszczki), chce pokazać Uli siłę Verlenów (stąd dźwiedzie, Arianna i Viorika) oraz chce rozwiązać Szeptomandrę. A wszystko to w swoim uroczym stylu GŁOŚNIEJ ZNACZY LEPIEJ. | 0095-07-12 - 0095-07-14 |
| 230502-strasznolabedz-atakuje-granice | Paradoks Uli sprawił, że dopakowany strasznołabędź uciekł do Fortu Tawalizer. Poprosił o pomoc Viorikę i kryje Ulę. Nie naraził V. na ryzyko, acz powiedział tylko to co musiał. | 0095-07-17 - 0095-07-19 |
| 230419-karolinka-nieokielznana-swinka | szpiegator zaplanował jak pomścić Viorikę - podkładając świnię. Zorganizował 'Karolinkę', dowiedział się o planach w terenie itp. Zrobił show w VirtuFortis dla Arianny i Vioriki. | 0095-07-19 - 0095-07-23 |
| 231220-bladawir-kontra-przemyt-tienow | kontakt i przyjaciel Arianny, 'szpiegator', który dyskretnie przekazał Ariannie to co ona potrzebowała wiedzieć o transferze tienów poza Aurum. | 0110-12-06 - 0110-12-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Elena Verlen         | 4 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230502-strasznolabedz-atakuje-granice; 231220-bladawir-kontra-przemyt-tienow)) |
| Arianna Verlen       | 3 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 231220-bladawir-kontra-przemyt-tienow)) |
| Ula Blakenbauer      | 3 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230502-strasznolabedz-atakuje-granice)) |
| Viorika Verlen       | 3 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230502-strasznolabedz-atakuje-granice)) |
| Apollo Verlen        | 2 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka)) |
| Aleksander Samszar   | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Antoni Bladawir      | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Eustachy Korkoran    | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Ewa Razalis          | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Fantazjusz Verlen    | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Kazimierz Darbik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Klaudia Stryk        | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Maks Samszar         | 1 | ((230502-strasznolabedz-atakuje-granice)) |
| OO Infernia          | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| OO Karsztarin        | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Raoul Lavanis        | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Szymon Kapeć         | 1 | ((230502-strasznolabedz-atakuje-granice)) |
| Szymon Orzesznik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Tymek Samszar        | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Zaara Mieralit       | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |