---
categories: profile
factions: 
owner: public
title: Edmund Garzin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230806-zwiad-w-iliminar-caos        | porucznik i oficer taktyczny, ale z innej jednostki niż Kajrat; chce kontynuować wojnę na Astorii i przegrupować się. Słucha poleceń Kajrata, choć nie rozumie, czemu dla Kajrata Furie są kluczem. Dowodzi pionem personalnym i taktycznym niewielkich sił Kajrata. | 0081-06-22 - 0081-06-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alaric Rakkeir       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Amanda Kajrat        | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Caelia Calaris       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Dragan Halatis       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Ernest Kajrat        | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Isaura Velaska       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Leon Varkas          | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Lestral Kirmanik     | 1 | ((230806-zwiad-w-iliminar-caos)) |