---
categories: profile
factions: 
owner: public
title: Billy Czuwak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190720-sos-w-eterze                 | uratował Karola przed zgnieceniem, odnalazł ukrytą Annę. Zabił krakena razem z Gregiem i Johnem. | 0110-11-01 - 0110-11-03 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190720-sos-w-eterze                 | ciężko przeżył śmierć kapitana "Oka" | 0110-11-03
| 190720-sos-w-eterze                 | Pryzowe za przyprowadzenie "Oka" oraz rekompensata od Senetis za krakena. | 0110-11-03

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Greg Światełko       | 1 | ((190720-sos-w-eterze)) |
| John Wytwórca        | 1 | ((190720-sos-w-eterze)) |
| Okręt Oko prawdy     | 1 | ((190720-sos-w-eterze)) |