---
categories: profile
factions: 
owner: public
title: Ewa Razalis
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (A+ E+) "Lepsza jest siła serca niż siła broni." | VALS: (Benevolence, Hedonism) "Radość i prosperity, zwłaszcza w kosmosie." | DRIVE: "Empatia ma miejsce w kosmosie." | @ 231115-cwiczenia-komodora-bladawira
* kiedyś pod Bladawirem, teraz niezależny komodor chcąca udowodnić Bladawirowi, że jego podejście jest błędne | @ 231115-cwiczenia-komodora-bladawira

### Wątki


triumfalny-powrot-arianny
naprawa-swiata-przez-bladawira

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231115-cwiczenia-komodora-bladawira | komodor, kochanka Bladawira i jego była podkomendna. 'Lepsza jest siła serca niż siła broni' chyba że chodzi o Bladawira. Weszła z nim w konflikt by tylko udowodnić mu że nie jest na tyle inteligentny i skuteczny, ale jej własna gwardia ją skrzywdziła. Posłuchała Kurzmina, i nie wyszła całkowicie na minus. Jej słabością jest jej wiara w ludzi. | 0110-11-08 - 0110-11-15 |
| 231220-bladawir-kontra-przemyt-tienow | dzięki danym dyskretnie przekazanym przez Klaudię udało jej się uderzyć w Bladawira i trochę się zemścić (a jemu zniszczyć operację). | 0110-12-06 - 0110-12-11 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 231115-cwiczenia-komodora-bladawira | przez Izę jest postrzegana jako uczciwa ofiara Bladawira; wpada w lekką paranoję, ale Bladawirowi nie udało się jej zniszczyć. | 0110-11-15

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Bladawir      | 2 | ((231115-cwiczenia-komodora-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Arianna Verlen       | 2 | ((231115-cwiczenia-komodora-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Klaudia Stryk        | 2 | ((231115-cwiczenia-komodora-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| OO Karsztarin        | 2 | ((231115-cwiczenia-komodora-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Dorota Radraszew     | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Elena Verlen         | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Eustachy Korkoran    | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Izabela Zarantel     | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Kazimierz Darbik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Leszek Kurzmin       | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Marcinozaur Verlen   | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| OO Infernia          | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| OO Paprykowiec       | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Raoul Lavanis        | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Szymon Orzesznik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Zaara Mieralit       | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |