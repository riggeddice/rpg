---
categories: profile
factions: 
owner: public
title: Lidia Woltaren
---

# {{ page.title }}


# Generated: 



## Fiszki


*  | @ 231109-komodor-bladawir-i-korona-woltaren
* OCEAN: A+E+ | Empatyczna, łatwo nawiązuje kontakt; Energiczna, nie opuszcza jej entuzjazm; Pełna pasji w każdym działaniu | VALS: Benevolence, Hedonism | DRIVE: ? | @ 231109-komodor-bladawir-i-korona-woltaren
* dziecko, zajmuje się kotkami | @ 231109-komodor-bladawir-i-korona-woltaren

### Wątki


triumfalny-powrot-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231109-komodor-bladawir-i-korona-woltaren | młoda (14?) załogantka 'Korony' opiekująca się Pacyfikatorami. Narwana i ekspresyjna, łazi za kotami w kocie zakamarki i podziękowała Ariannie uściśnięciem dłoni za 'ratunek statku'. Zaatakowana przez sterrorformowanego Pacyfikatora Kwazara, skończyła ciężko ranna na ziemi. Przeżyła tylko dzięki ixiońskiej infekcji. | 0110-10-27 - 0110-11-03 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 231109-komodor-bladawir-i-korona-woltaren | BARDZO ciężko ranna w wyniku ataku terrorforma; co najmniej miesiąc w laboratorium naprawczym z uwagi na infekcję ixiońską wywołaną ranami terrorforma. Ale kto za to zapłaci? | 0110-11-03

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aneta Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Antoni Bladawir      | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Arianna Verlen       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Elena Verlen         | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Feliks Walrond       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Klaudia Stryk        | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Krzysztof Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Leszek Kurzmin       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Marta Keksik         | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| OO Paprykowiec       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Patryk Samszar       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| SC Korona Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Tomasz Rewernik      | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Wojciech Mykirło     | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |