---
categories: profile
factions: 
owner: public
title: Dobromir Misiak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240125-wszystkie-sekrety-caralei    | skutecznie wykorzystał swoje umiejętności biurokratyczne do odkrycia anomalii w dokumentach statku Caralea i ujawnienia sabotażu TAI (żywej TAI). Agresywny, pobił kapitana podejrzanego o przemyt. | 0110-12-29 - 0111-01-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Bladawir      | 1 | ((240125-wszystkie-sekrety-caralei)) |
| Kazimierz Darbik     | 1 | ((240125-wszystkie-sekrety-caralei)) |
| Olaf Rawen           | 1 | ((240125-wszystkie-sekrety-caralei)) |
| Tog Worniacz         | 1 | ((240125-wszystkie-sekrety-caralei)) |