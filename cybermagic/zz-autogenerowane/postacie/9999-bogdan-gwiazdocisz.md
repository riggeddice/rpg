---
categories: profile
factions: 
owner: public
title: Bogdan Gwiazdocisz
---

# {{ page.title }}


# Generated: 



## Fiszki


* założył przytułek powojenny pod Zaczęstwem, gdzie nie zadaje nikomu pytań | @ 230920-legenda-o-noktianskiej-mafii
* OCEAN (N+ E-): pełen refleksji i samokrytyki. "Przeszłość kształtuje, ale nie definiuje." | @ 230920-legenda-o-noktianskiej-mafii
* VALS (Benevolence, Universalism): "Każdy zasługuje na drugą szansę, bez względu na przeszłość." | @ 230920-legenda-o-noktianskiej-mafii
* Core Wound - Lie: "zniszczyłem tak wiele istnień, wierzyłem w cel uświęca środki" - "jeśli uratuję kogo się da i będę nieść dobro, być może pozyskam wybaczenie" | @ 230920-legenda-o-noktianskiej-mafii
* Styl: Bogdan jest powściągliwy, zawsze unika rozmów o sobie, w jego oczach widać ciężar winy i pragnienie odkupienia. | @ 230920-legenda-o-noktianskiej-mafii
* Deed: served as an interrogator and his methods were ruthless, employing psychological and physical torture to extract information from prisoners | @ 230920-legenda-o-noktianskiej-mafii

### Wątki


pronoktianska-mafia-kajrata
furia-mataris-agentka-mafii

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230920-legenda-o-noktianskiej-mafii | opiekun przytułka; nie zadaje pytań, zaprosił Amandę i Xaverę do Fortifarmy CustoLucis przy Zaczęstwie i powiedział jej podstawowe informacje what-is-what itp. | 0081-07-10 - 0081-07-13 |
| 240305-lea-strazniczka-lasu         | stary już człowiek, który próbuje znaleźć _closure_ jak chodzi o wszystkie osoby które stracił podczas wojny i świeżo po wojnie. Operacja archeologiczna w Lesie Trzęsawnym, okolice Podwiertu i próba integracji Rekinów i AMZ. Świetnie opowiada. Skończył ranny przez EfemeHorror. | 0111-10-21 - 0111-10-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Arkadia Verlen       | 1 | ((240305-lea-strazniczka-lasu)) |
| Dmitri Karpov        | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Ernest Namertel      | 1 | ((240305-lea-strazniczka-lasu)) |
| Irelia Kairanolis    | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Karolina Terienak    | 1 | ((240305-lea-strazniczka-lasu)) |
| Keira Amarco d'Namertel | 1 | ((240305-lea-strazniczka-lasu)) |
| Lea Samszar          | 1 | ((240305-lea-strazniczka-lasu)) |
| Malena Barandis      | 1 | ((240305-lea-strazniczka-lasu)) |
| Marek Samszar        | 1 | ((240305-lea-strazniczka-lasu)) |
| Mariusz Kupieczka    | 1 | ((240305-lea-strazniczka-lasu)) |
| Marysia Sowińska     | 1 | ((240305-lea-strazniczka-lasu)) |
| Michał Klabacz       | 1 | ((240305-lea-strazniczka-lasu)) |
| Patryk Majwuron      | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Petra Karpov         | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Triana Porzecznik    | 1 | ((240305-lea-strazniczka-lasu)) |
| Xavera Sirtas        | 1 | ((230920-legenda-o-noktianskiej-mafii)) |