---
categories: profile
factions: 
owner: public
title: Salazar Bolza
---

# {{ page.title }}


# Generated: 



## Fiszki


* komodor (drakolita ewolucjonista; misja: założyć placówkę badawczą Anomalii), INHUMAN (bioforma typ serpentis, ale mniej) model after Lorca | @ 221221-astralna-flara-i-nowy-komodor
* ENCAO:  --+0+ |Racjonalny;;Powściągliwy, skryty i wyważony;;Surowy, wymagający| VALS: Power, Achievement >> Security, Stimulation| DRIVE: Survival of the fittest | @ 221221-astralna-flara-i-nowy-komodor

### Wątki


historia-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221221-astralna-flara-i-nowy-komodor | komodor Orbitera nad Arianną w okolicach Anomalii Kolapsu w miejscu Lodowca. (((Drakolita ewolucjonista; misja: założyć placówkę badawczą Anomalii. INHUMAN (bioforma typ serpentis, ale mniej) model after Lorca. ENCAO:  --+0+ |Racjonalny;;Powściągliwy, skryty i wyważony;;Surowy, wymagający| VALS: Power, Achievement >> Security, Stimulation| DRIVE: Survival of the fittest.))). Dowiedział się, że Arianna szperała na jego temat i zrobił jej quiz kogo ratować (Kurzmina, Elenę czy jego). Wiecznie testuje wszystkich wobec swoich wysokich standardów. Skłonił Ariannę do myślenia większymi planami - nie 'wykurzyć squatterów' a 'sojusz ze squatterami'. Po zapoznaniu się z tym kim są ludzie z Kazmirian, zmienił na 'triage i nauka i indoktrynacja'. Podjął decyzję o dyskretnej eksterminacji Zarralei. | 0100-11-07 - 0100-11-10 |
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | zabronił szperania na Dominie Lucis; kazał Ariannie uspokoić młodych infiltratorów których Kircznik przestraszył. Świetnie rozpracował Lanę (agentkę Biur HR) i same Biura, nie dał się ich podstępom i słodkiej agentce. | 0100-11-13 - 0100-11-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Daria Czarnewik      | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Elena Verlen         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kajetan Kircznik     | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leszek Kurzmin       | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Maja Samszar         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Astralna Flara    | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Loricatus         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Władawiec Diakon     | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Ellarina Samarintael | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Grażyna Burgacz      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kaspian Certisarius  | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| NekroTAI Zarralea    | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Szczepan Myrczek     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |