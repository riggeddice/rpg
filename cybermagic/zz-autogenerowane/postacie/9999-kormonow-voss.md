---
categories: profile
factions: 
owner: public
title: Kormonow Voss
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220126-keldan-voss-kolonia-saitaera | drakolita; faith healer; wielki nieobecny i overlord kultystów tej bazy. | 0112-05-24 - 0112-05-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Annika Pradis        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Arianna Verlen       | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Elena Verlen         | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Eustachy Korkoran    | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Klaudia Stryk        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Mateus Sarpon        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| OO Kastor            | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| SP Pallida Voss      | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Szczepan Kaltaben    | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Zygfryd Maus         | 1 | ((220126-keldan-voss-kolonia-saitaera)) |