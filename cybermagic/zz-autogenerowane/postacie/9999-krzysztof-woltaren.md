---
categories: profile
factions: 
owner: public
title: Krzysztof Woltaren
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231109-komodor-bladawir-i-korona-woltaren | pierwszy oficer Korony Woltaren o nastawieniu technicznym, chroni Anetę przed problemami z audytami. Po śmierci Anety i zniszczeniu jednostki, on został dowódcą zgrai która była 'Koroną Woltaren'. | 0110-10-27 - 0110-11-03 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aneta Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Antoni Bladawir      | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Arianna Verlen       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Elena Verlen         | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Feliks Walrond       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Klaudia Stryk        | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Leszek Kurzmin       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Lidia Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Marta Keksik         | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| OO Paprykowiec       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Patryk Samszar       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| SC Korona Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Tomasz Rewernik      | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Wojciech Mykirło     | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |