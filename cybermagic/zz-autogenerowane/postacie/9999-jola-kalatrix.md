---
categories: profile
factions: 
owner: public
title: Jola Kalatrix
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240227-cykl-krwawej-przadki         | manifestacja energii magicznej Róży (i nieprawdziwa przyjaciółka Róży), która próbuje ją chronić, pomagać jej i dostarczać jej ludzi. Pozyskała naukowca dla Róży i ogólnie dbała o to, by Róży niczego ważnego nie zabrakło. | 0099-05-22 - 0099-05-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Wokniaczek      | 1 | ((240227-cykl-krwawej-przadki)) |
| Edward Kopoktris     | 1 | ((240227-cykl-krwawej-przadki)) |
| Janusz Umizarit      | 1 | ((240227-cykl-krwawej-przadki)) |
| Lucyna Castelli      | 1 | ((240227-cykl-krwawej-przadki)) |
| Michał Castelli      | 1 | ((240227-cykl-krwawej-przadki)) |
| Róża Kalatrix        | 1 | ((240227-cykl-krwawej-przadki)) |
| Wincenty Frak        | 1 | ((240227-cykl-krwawej-przadki)) |