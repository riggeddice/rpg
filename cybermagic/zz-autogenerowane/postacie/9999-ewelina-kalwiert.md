---
categories: profile
factions: 
owner: public
title: Ewelina Kalwiert
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240313-ostatni-lot-valuvanok        | waleczny agent Valuvanok (sabotaż + infiltracja + walka); znalazła sposób walki z Potworami Plagi, wykorzystując zręczność, taktykę, radioaktywność. Odkryła kluczową wiedzę o własnej odporności na Plagę oraz fakt że jest wektorem Plagi. Podjęła decyzję o zniszczeniu Valuvanok, by powstrzymać rozprzestrzenianie się Plagi. Nie będzie niewolnikiem. | 0082-11-28 - 0082-11-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Jasper Arimetus      | 1 | ((240313-ostatni-lot-valuvanok)) |
| Mara Arimetus        | 1 | ((240313-ostatni-lot-valuvanok)) |
| OnS Valuvanok        | 1 | ((240313-ostatni-lot-valuvanok)) |
| Xavier Kalwiert      | 1 | ((240313-ostatni-lot-valuvanok)) |