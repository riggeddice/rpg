---
categories: profile
factions: 
owner: public
title: Stella Sowińska
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211120-glizda-ktora-leczy           | agentka specjalna Sowińskich przebrana za Blakenbauerkę przebraną za Sowińską. Pociągnęła Rolanda za język; odkryła, że jest Esuriitowy. Tymczasowo przejęła kontrolę nad Podwierckimi Rekinami, po czym jak rozwiązała najważniejsze rzeczy, oddała dowodzenie niechętnej Amelii wbrew niej. | 0108-04-07 - 0108-04-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Amelia Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Ernest Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Feliks Keksik        | 1 | ((211120-glizda-ktora-leczy)) |
| Justynian Diakon     | 1 | ((211120-glizda-ktora-leczy)) |
| Kacper Bankierz      | 1 | ((211120-glizda-ktora-leczy)) |
| Karol Pustak         | 1 | ((211120-glizda-ktora-leczy)) |
| Oliwia Lemurczak     | 1 | ((211120-glizda-ktora-leczy)) |
| Roland Sowiński      | 1 | ((211120-glizda-ktora-leczy)) |
| Sabina Kazitan       | 1 | ((211120-glizda-ktora-leczy)) |
| Sensacjusz Diakon    | 1 | ((211120-glizda-ktora-leczy)) |