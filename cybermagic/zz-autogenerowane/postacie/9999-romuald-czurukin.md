---
categories: profile
factions: 
owner: public
title: Romuald Czurukin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181118-romuald-i-julia              | bard, zakochany w Julii od pierwszego wejrzenia. Zniszczył plany Emulatora. Miłością wyciągnął Julię do świata wolności i radości. | 0109-10-27 - 0109-10-29 |
| 181125-swaty-w-cieniu-potwora       | bard, który zrobił megaepicką balladę, by Wioletta zabujała się w Pietrze. Prawie wyszło - zabujała się w Pięknotce... | 0109-10-29 - 0109-11-01 |
| 181225-czyszczenie-toksycznych-zwiazkow | unikał trójkątu z Diakonką (!). Ma poczucie winy. Poszedł błagać Moktara o łaskę dla Julii - przypadkowo wyciągnął Pięknotkę z opresji. | 0109-11-13 - 0109-11-17 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181118-romuald-i-julia              | dostaje stałą i sensowną posadę barda w Szkarłatnym Szepcie. | 0109-10-29
| 181118-romuald-i-julia              | zostaje na stałe z Julią Morwisz; kochają się | 0109-10-29
| 181225-czyszczenie-toksycznych-zwiazkow | przez trójkącik z Pięknotką i Julią ma katastrofalne, niszczycielskie poczucie winy - zrobił coś niezgodnego z wiernością swej ukochanej Julii. | 0109-11-17
| 181227-adieu-cieniaszczycie         | mimo sytuacji z Julią zostaje z nią. Moktar pozwala mu dołączyć jako bard czy coś. Nie jest pełnym Psem, ale jest tolerowany. | 0109-12-01

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 3 | ((181118-romuald-i-julia; 181125-swaty-w-cieniu-potwora; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Julia Morwisz        | 2 | ((181118-romuald-i-julia; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Moktar Gradon        | 2 | ((181125-swaty-w-cieniu-potwora; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Pietro Dwarczan      | 2 | ((181118-romuald-i-julia; 181125-swaty-w-cieniu-potwora)) |
| Adam Szarjan         | 1 | ((181118-romuald-i-julia)) |
| Atena Sowińska       | 1 | ((181225-czyszczenie-toksycznych-zwiazkow)) |
| Bogdan Szerl         | 1 | ((181225-czyszczenie-toksycznych-zwiazkow)) |
| Lilia Ursus          | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Waleria Cyklon       | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Wioletta Kalazar     | 1 | ((181125-swaty-w-cieniu-potwora)) |