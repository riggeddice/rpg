---
categories: profile
factions: 
owner: public
title: Malik Darien
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231213-polowanie-na-biosynty-na-szernief | rezydent Agencji na Szernief, gdy doszło do intruzji więźniów (i Parasekta) na Szernief ruszył za nimi i został pierwszym Skażonym. Nieświadomy prowadził ludzi do Parasekta. Chciał się zabić gdy odzyskał kontrolę przesłuchiwany przez Hackera, ale skończył boleśnie na stole operacyjnym. Po paru tygodniach regeneracji będzie znowu sprawny. | 0105-07-26 - 0105-07-31 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 231213-polowanie-na-biosynty-na-szernief | wymaga paru tygodni ciężkiej regeneracji i znowu może służyć Agencji. | 0105-07-31

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Artur Tavit          | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Delgado Vitriol      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Felina Amatanir      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Kalista Surilik      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Klasa Biurokrata     | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Klasa Dyplomata      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Klasa Hacker         | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Klasa Oficer Naukowy | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Klasa Sabotażysta    | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Sebastian-194        | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Vanessa d'Cavalis    | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |