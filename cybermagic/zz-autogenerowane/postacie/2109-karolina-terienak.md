---
categories: profile
factions: 
owner: kić
title: Karolina Terienak
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "pustogor, rekin, terienak, aurum"
* owner: "kić"
* title: "Karolina Terienak"


## Kim jest

### W kilku zdaniach

Neurosprzężona czarodziejka (słaba) z rodu Terienak o umierającej sentisieci. 

### Co się rzuca w oczy

* Skóra, żółw (pancerz) na plecach, wypasiony ścigacz. Ubiera się tak, by było jej wygodnie, a wygodnie jest jej w stroju motorowym.
* Na imprezach głośna, zwykle lekko wstawiona i świetnie się bawi.
* Ze wszystkich osób najlepiej rozumie Arkadię Verlen i czuje do niej nić sympatii (odwzajemnioną).
* Wesoła. Intensywna gdy nad czymś pracuje, ale normalnie wesoła, w dobrym humorze. Nieco wulgarny humor / odzywki, ale co z tego.

### Jak sterować postacią

* Wchodzi twardo i bezpośrednio, nie przebiera w słowach, prosto do celu i bardzo kinetycznie.
* Nie kłamie. Nie zawsze mówi prawdę, ale nie ukrywa nawet bolesnej prawdy.
* Mówi co czuje. Jak jej się coś nie podoba, wszyscy wiedzą.
* Pomoże innym z pojazdami, ale JEJ ścigacz jest najlepszy i trzeba to uznać.
* Lubi rywalizację. Ale nie chce wygrywać nieuczciwie. Jest twarda i najlepsza, nie musi tego udowadniać. Nothing to prove.
* Jeśli komuś dzieje się krzywda, wejdzie. Nie stoi z boku.
* Nie do końca czuje się częścią Aurum. Gdyby mogła naprawić ród, zmieniłoby się to, ale wie, że jest w "borrowed time".
* Szuka dla siebie miejsca poza Aurum, gdzieś tutaj najlepiej. Ale z jej umiejętnościami i niezależnością.
* Nie toleruje brudnej polityki, rozgrywania ludźmi i traktowania ich jak marionetki i zasoby.
* Gdyby znalazła sposób pomocy rodowi to go zrealizuje. Żal jej tej sentisieci.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Wygrała brudny (nie grali czysto) wyścig ścigaczy w Aurum. Pokazała ród Terienak jako "nadal żywy" w ten sposób. Zdobyła uznanie Juliusza Sowińskiego.
* Gdy stary czołg rakietowy Yamarl się uszkodził i miał wybuchnąć, Karo używając neurosprzężenia i zdalnego połączenia magią spowolniła eksplozję by wszyscy zdążyli się ewakuować.
* Gdy była manipulowana przez polityków w Aurum to postawiła się i skopała jednego glanami. Juliusz Sowiński ją osłonił przed zarzutami i zesłał jako Rekinkę.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: Perfekcyjny Rekin. Waleczna arystokratka na ścigaczu. Umie walczyć nieczysto. Nie pojedynkuje się.
* AKCJA: Świetnie i nieortodoksyjnie pilotuje (zwłaszcza swój) ścigacz. A potem go naprawia (ale naprawia tylko swój).
* AKCJA: Inspiruje, dominuje, zastrasza. Jest niezależna, silna, zdecydowana - co ciągnie za sobą innych. Jednocześnie nie rzuca gróźb na wiatr i nie unika siły - dają jej co chce.
* AKCJA: Kontroluje roje pojazdów i maszyn. Jej neurosprzężenie pozwala jej na jednoczesną pracę z wieloma maszynami naraz.
* CECHA: Nerd ścigaczowy kwadrat. Czyta o nich. Szuka ich. Bada je. Wie o nich WSZYSTKO i jeszcze troszeczkę. To jest jej hobby, pasja i życie.
* COŚ: Aktywnie stara się mieć NAJLEPSZY ścigacz w okolicy, niezależnie od frakcji. Najszybszy, przeciążenia, mody, zwrotność. ZAWSZE uzbrojony.
* COŚ: Reputacja badass. Świetny modder ścigaczy. Nie podpadać jej, bo skopie lub zawoła brata.

### Serce i Wartości (3)

* Samosterowność
    * Robi to co uważa za słuszne wtedy kiedy uważa że powinna. Niezatrzymywalna.
    * Lojalna i oczekuje lojalności. Przedłoży swoich nad prawo i zasady.
    * Reaguje, gdy ktoś krzywdzi innych. Nie patrzy na status czy różnicę siły.
* Stymulacja
    * Tylko sensowne akcje, ale trudność to zaleta. Ona nie spieprzy, wiec bierze trudne na siebie.
    * Pełnia życia - wulgarna, ostra, żywioł natury. Nie do zatrzymania.
    * Żaden potencjał nie bedzie zmarnowany. Inspiruje / wykopie z dołka, ale nie mentoruje.
* Osiągnięcia
    * Nie chce być WIDZIANA jako najlepsza - ważne, by być perfekcyjnym Rekinem.
    * Ja jestem najlepsza i NIKT się nie ośmieli mnie zaatakować. Straszna retaliacja. 
    * Agresywna rywalizacja - będzie najlepsza i wszyscy o tym będą wiedzieć

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Znam się z wieloma magami Aurum od zawsze. Ale moja sentisieć umiera. Oni zaczną traktować mnie jak plebs, odrzucą naszą przyjaźń."
* CORE LIE: "Pozycja i Tradycja są tylko iluzją. Będę zachowywać się wulgarnie, jak plebs itp - bo co za różnica. Odrzucę ich zanim oni odrzucą mnie."
* Niecierpliwa. Nie lubi czekać, nie lubi kombinować.
* Gardzi etykietą. Nie lubi formalności. Załatwmy to i będzie dobrze. Nie lubi i nie robi tego dobrze. Jak próbuje, wychodzi dziwnie.
* Jej magia jest naprawdę osłabiona. Była "typową" czarodziejką a neurosprzężenie bardzo tą moc osłabiło.
* Ma osoby w Aurum (niektórych polityków) które jej nie lubią. Skopała jednego który próbował nią rozgrywać swoje sprawy.

### Magia (3M)

#### W czym jest świetna

* Interfejsowanie z maszynami. Potrafi zdalnie połączyć się z różnymi urządzeniami i podpiąć je do swojego neurosprzęgu.
* Skanowanie maszyn. Potrafi znajdować ich słabe punkty, luki konstrukcyjne i obszary w których są uszkodzone lub "można więcej".
* Regeneracja maszyn. Mechanizmy wracają do swojej poprzedniej natury. To samo pancerze itp.

#### Jak się objawia utrata kontroli

* Jej magia po prostu przestaje działać. Znika.
* Maszyny zaczynają się disruptować i świrować; jak EMP (nie takiej mocy).

### Specjalne

* .

## Inne

### Wygląd

* 

### Coś Więcej

* .

### Endgame

* .


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201201-impreza-w-malopsie           | Rekin. Miłośniczka swojego ścigacza i świetny pilot. Normalnie łagodna - ale brutalnie zdewastowała DJ Babu za uszkodzenie ścigacza. | 0110-11-04 - 0110-11-06 |
| 201215-dziewczyna-i-pies            | przemanewrowała dookoła gorathaula we mgle i porwała sobie Zygmunta (policjanta) by móc go przesłuchać w dogodnym momencie. Robi co chcą bracia, skupia się na ścigaczu. | 0110-11-15 - 0110-11-17 |
| 220101-karolina-w-ciezkim-mlocie    | wbiła się do knajpy Ciężki Młot i mimo niechęci do "AMZ / Rekina" została zaakceptowana po tym, jak pomogła w ogarnięciu anomalicznej awarii w zakładzie recyklingowym. Nie boi się ciężkiej i żmudnej roboty. | 0111-04-25 - 0111-05-05 |
| 210720-porwanie-daniela-terienaka   | by ratować porwanego brata skopała Torszeckiego, poszła na współpracę z Marysią Sowińską i nacisnęła na Ulę Miłkowicz (że użyje krwi) zmuszając ją do decyzji - chroni skórę czy ratuje innych. | 0111-06-14 - 0111-06-16 |
| 210824-mandragora-nienawidzi-rekinow | wow factor - zaimponowała Laurze i Ekaterinie przez manewrowanie mimo dopalonego Esuriit pnączoszpona. Potem, mimo uszkodzeń ścigacza, wpakowała go w jezioro by ratować Zespół przed duchami. I ścigacz umarł. | 0111-07-03 - 0111-07-05 |
| 210831-serafina-staje-za-wydrami    | przesłuchała Lorenę i doszła do tego jakie informacje są w ścigaczu jej brata; odkryła, że istnieje grupa Wydry polująca na Rekiny. | 0111-07-07 - 0111-07-10 |
| 210921-przybycie-rekina-z-eterni    | połączyła Pustaka (osobę wiedzącą wszystko o Royalsach) z Marysią. De facto Pustak Karolinie teraz trochę wisi. | 0111-07-20 - 0111-07-25 |
| 210928-wysadzony-zywy-scigacz       | zaprzyjaźniła się z Ernestem, ignorując bariery tienowatych. Zapoznała go z Marysią, podroczyła się odnośnie ścigaczy a potem by ratować Bulteriera ostrzelała go ścigaczem, wygrała z nim w powietrzu i niestety nie udało jej się uratować ścigacza Ernesta. Mimo wsparcia jego Krwi i Sieci. | 0111-07-26 - 0111-07-27 |
| 211012-torszecki-pokazal-kregoslup  | poszła do Torszeckiego by ten przestał być dupą wołową i się ośmielił; wydobyła od niego, że to ON stoi za zniszczeniem ścigacza. Poszła z tym potem do Marysi... to skomplikowana sprawa. Wygarnęła Marysi, że traktowanie Torszeckiego częściowo do tego doprowadziło. | 0111-07-29 - 0111-07-30 |
| 211102-satarail-pomaga-marysi       | pokazała, że jest świetna w walce - wrzuciła Sensacjusza (i Marysię) do glisty, staranowała Lorenę, zagrała Owadem w baseball. Skutecznie sklupała wszystko co ją chciało skrzywdzić. Don't mess with her. | 0111-08-09 - 0111-08-10 |
| 211123-odbudowa-wedlug-justyniana   | tymczasowo przeprowadziła się do Ernesta; zorientowała się, że przez brak Marysi zrobił się _power vacuum_ który przejął Justynian Diakon (XD). Zdobyła info i ostrzegła Marysię. | 0111-08-11 - 0111-08-20 |
| 211127-waśń-o-ryby-w-majklapcu      | uznała, że chce pomóc Iwanowi i rozwiązać problem lokalnego oddziału mafii; zebrała drużynę (Torszecki i Arkadia), po wyłapaniu kotów z Danielem manewrowała ścigaczem by uratować od mafii Arkadię i ewakuować Daniela. | 0111-08-19 - 0111-08-24 |
| 211207-gdy-zabraknie-pradu-rekinom  | wyciągnęła z Arnolda czemu nie ma prądu, uratowała ludzi pracujących w magitrowni Rekinów przed pobiciem i wciągnęła Arkadię w opiernicz Natalii za mafię. | 0111-08-30 - 0111-08-31 |
| 211221-chevaleresse-infiltruje-rekiny | złapała Chevaleresse (i tyci się z nią dogadała), dogoniła Stasia Arienika i przechwyciła go, po czym zastraszyła i oddała Alanowi Bartozolowi zrzucając ze ścigacza. | 0111-09-06 - 0111-09-07 |
| 211228-akt-o-ktorym-marysia-nie-wie | zmierzyła się z Napoleonem Bankierzem na arenie i go pokonała z łatwością; podniosła mu reputację. Ściągnęła brata, by ten odzyskał akt od Torszeckiego. | 0111-09-09 - 0111-09-12 |
| 220111-marysiowa-hestia-rekinow     | pod namową Chevaleresse zwalcza Kult Ośmiornicy wśród Rekinów; zlokalizowała gdzie jest Melissa (u Santino) i współpracując z Ernestem przechwyciła Melissę. | 0111-09-16 - 0111-09-19 |
| 220222-plaszcz-ochronny-mimozy      | brutalnie wyciąga kiepsko ukrytą Lorenę i pokazuje ją grupie, wzmacniając ruchy Marysi. Przeraża Lorenę samą swoją obecnością. | 0111-09-21 - 0111-09-25 |
| 220730-supersupertajny-plan-loreny  | by ochronić lokalny biznes przed problemami z kontraktem z Ernestem poszła do Żorża i doszła do tego, że ktoś się podle podszywa. Karo zastawiła pułapkę z pomocą Arkadii i Daniela, złapała Marsena Gwozdnika i miała zwis - koleś zupełnie źle widzi Lorenę (swoją kuzynkę). Wmówiła mu, że Karo jest agentką Loreny i Marsen ma nie szkodzić terenowi bo plan Loreny. | 0111-09-30 - 0111-10-04 |
| 220814-stary-kot-a-trzesawisko      | uratowała rannego kota z okolic Trzęsawiska i dała go do kociarni Zawtrak; potem szukawszy gdzie ów kot jest pomogła Uli wydostać się z self-inflicted roboty papierkowej. Po przekonaniu TAI Viirai, że naprawdę CHCĄ pomóc wezwała Alana i po rozwiązaniu misji chroniła życie kota przed terminusami. Wsadziła go w końcu Uli :D. | 0111-10-12 - 0111-10-13 |
| 220816-jak-wsadzic-ule-alanowi      | po raz pierwszy w życiu poprosiła o coś Marysię - niech wsadzi Ulę Alanowi. Chroni informację o TAI i chroni fortifarmę przed Tukanem. Ostro kłóci się z Marysią XD. | 0111-10-12 - 0111-10-13 |
| 230325-ten-nawiedzany-i-ta-ukryta   | Rekin; przygotowała lepsze paliwo dla Daniela by ten wygrał wyścig, obiecała Oliwierowi że pomoże Michałowi, potem pozbyła się Nadii podkładając jej szczura. Rozmawiała z Olą by pozyskać info o historii jej i Michała a potem, niestety, porozmawiała z Leą by ta pomogła Michałowi i powiedziała prawdę. Lea, oczywiście, żąda protokołu komunikacyjnego więc tienki się pożarły - ale Lea pomoże Michałowi. | 0111-10-16 - 0111-10-17 |
| 240305-lea-strazniczka-lasu         | pomaga rannym w lesie i szybko transportuje wszystkich, po czym montuje oddział do walki z EfemeHorrorem który pociął AMZ i Rekiny. | 0111-10-21 - 0111-10-25 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220101-karolina-w-ciezkim-mlocie    | zaakceptowana jako "swoja" w barze Ciężki Młot. Jest jedynym "nie-człowiekiem pracy" który jest tam traktowany jak swój. | 0111-05-05
| 210824-mandragora-nienawidzi-rekinow | jej ścigacz jest nieaktywny następne 3 tygodnie. Jest nie tylko popsuty (co jest spoko), ale też zalany (co nie jest spoko). | 0111-07-05
| 210824-mandragora-nienawidzi-rekinow | wywołała ogromne WOW u Laury i Ekateriny - za pnączoszpona, manewry i genialny ruch ze ścigaczem w jeziorze. | 0111-07-05
| 210831-serafina-staje-za-wydrami    | ma wroga w Lorenie Gwozdnik. Zbyt twardo weszła i rozerwała związek jej i jej brata. | 0111-07-10
| 210928-wysadzony-zywy-scigacz       | ma przyjaźń i zaufanie Ernesta Namertela. Ernest czuje z nią duży kontakt. Jest flow. | 0111-07-27
| 211102-satarail-pomaga-marysi       | za atak Loreny pod wpływem na nią, Karolina połamała jej nogi. Opinia absolutnie bezwzględnej i niebezpieczniej. Don't EVER mess with her. Terror works. | 0111-08-10
| 211127-waśń-o-ryby-w-majklapcu      | w Majkłapcu jest lubiana przez Iwana Zawtraka z Kociarni i ma jego zaufanie. | 0111-08-24

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 15 | ((210720-porwanie-daniela-terienaka; 210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami; 210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211012-torszecki-pokazal-kregoslup; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211207-gdy-zabraknie-pradu-rekinom; 211221-chevaleresse-infiltruje-rekiny; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 220816-jak-wsadzic-ule-alanowi; 240305-lea-strazniczka-lasu)) |
| Daniel Terienak      | 12 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies; 210720-porwanie-daniela-terienaka; 210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami; 211123-odbudowa-wedlug-justyniana; 211127-waśń-o-ryby-w-majklapcu; 211207-gdy-zabraknie-pradu-rekinom; 211228-akt-o-ktorym-marysia-nie-wie; 220101-karolina-w-ciezkim-mlocie; 220730-supersupertajny-plan-loreny; 230325-ten-nawiedzany-i-ta-ukryta)) |
| Rafał Torszecki      | 9 | ((210720-porwanie-daniela-terienaka; 210831-serafina-staje-za-wydrami; 210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211012-torszecki-pokazal-kregoslup; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211127-waśń-o-ryby-w-majklapcu; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Ernest Namertel      | 8 | ((210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 240305-lea-strazniczka-lasu)) |
| Arkadia Verlen       | 5 | ((210921-przybycie-rekina-z-eterni; 211127-waśń-o-ryby-w-majklapcu; 211207-gdy-zabraknie-pradu-rekinom; 220730-supersupertajny-plan-loreny; 240305-lea-strazniczka-lasu)) |
| Keira Amarco d'Namertel | 5 | ((210928-wysadzony-zywy-scigacz; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 240305-lea-strazniczka-lasu)) |
| Lorena Gwozdnik      | 5 | ((210831-serafina-staje-za-wydrami; 211102-satarail-pomaga-marysi; 211207-gdy-zabraknie-pradu-rekinom; 211228-akt-o-ktorym-marysia-nie-wie; 220222-plaszcz-ochronny-mimozy)) |
| Sensacjusz Diakon    | 4 | ((210824-mandragora-nienawidzi-rekinow; 211012-torszecki-pokazal-kregoslup; 211102-satarail-pomaga-marysi; 211207-gdy-zabraknie-pradu-rekinom)) |
| Tomasz Tukan         | 4 | ((210720-porwanie-daniela-terienaka; 210824-mandragora-nienawidzi-rekinow; 210921-przybycie-rekina-z-eterni; 220816-jak-wsadzic-ule-alanowi)) |
| Amelia Sowińska      | 3 | ((210824-mandragora-nienawidzi-rekinow; 210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Diana Tevalier       | 3 | ((211221-chevaleresse-infiltruje-rekiny; 220111-marysiowa-hestia-rekinow; 220814-stary-kot-a-trzesawisko)) |
| Hestia d'Rekiny      | 3 | ((211207-gdy-zabraknie-pradu-rekinom; 211221-chevaleresse-infiltruje-rekiny; 220111-marysiowa-hestia-rekinow)) |
| Liliana Bankierz     | 3 | ((211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220730-supersupertajny-plan-loreny)) |
| Urszula Miłkowicz    | 3 | ((210720-porwanie-daniela-terienaka; 220814-stary-kot-a-trzesawisko; 220816-jak-wsadzic-ule-alanowi)) |
| Żorż d'Namertel      | 3 | ((210928-wysadzony-zywy-scigacz; 211221-chevaleresse-infiltruje-rekiny; 220730-supersupertajny-plan-loreny)) |
| Alan Bartozol        | 2 | ((211221-chevaleresse-infiltruje-rekiny; 220814-stary-kot-a-trzesawisko)) |
| Andrzej Kuncerzyk    | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Azalia Sernat d'Namertel | 2 | ((210928-wysadzony-zywy-scigacz; 211123-odbudowa-wedlug-justyniana)) |
| Barnaba Burgacz      | 2 | ((201201-impreza-w-malopsie; 211221-chevaleresse-infiltruje-rekiny)) |
| Franciszek Zygmunt   | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Franek Bulterier     | 2 | ((210928-wysadzony-zywy-scigacz; 230325-ten-nawiedzany-i-ta-ukryta)) |
| Iwan Zawtrak         | 2 | ((211127-waśń-o-ryby-w-majklapcu; 220814-stary-kot-a-trzesawisko)) |
| Izydor Grumczewicz   | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Jolanta Sowińska     | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Justynian Diakon     | 2 | ((211123-odbudowa-wedlug-justyniana; 211221-chevaleresse-infiltruje-rekiny)) |
| Karol Pustak         | 2 | ((210921-przybycie-rekina-z-eterni; 211123-odbudowa-wedlug-justyniana)) |
| Laura Tesinik        | 2 | ((210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami)) |
| Lea Samszar          | 2 | ((230325-ten-nawiedzany-i-ta-ukryta; 240305-lea-strazniczka-lasu)) |
| Lucjan Sowiński      | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Marek Samszar        | 2 | ((211102-satarail-pomaga-marysi; 240305-lea-strazniczka-lasu)) |
| Michał Klabacz       | 2 | ((230325-ten-nawiedzany-i-ta-ukryta; 240305-lea-strazniczka-lasu)) |
| Mimoza Elegancja Diakon | 2 | ((220101-karolina-w-ciezkim-mlocie; 220222-plaszcz-ochronny-mimozy)) |
| Paulina Mordoch      | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Rupert Mysiokornik   | 2 | ((211221-chevaleresse-infiltruje-rekiny; 230325-ten-nawiedzany-i-ta-ukryta)) |
| Tadeusz Łaśnic       | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Władysław Owczarek   | 2 | ((220101-karolina-w-ciezkim-mlocie; 220730-supersupertajny-plan-loreny)) |
| Aleksandra Burgacz   | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Ariela Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Arnold Kłaczek       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Bogdan Gwiazdocisz   | 1 | ((240305-lea-strazniczka-lasu)) |
| Cezary Urmaszcz      | 1 | ((201201-impreza-w-malopsie)) |
| Ekaterina Zajcew     | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Genowefa Krecik      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Grzegorz Terienak    | 1 | ((201215-dziewczyna-i-pies)) |
| Halina Sermniek      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Henryk Murkot        | 1 | ((220101-karolina-w-ciezkim-mlocie)) |
| Henryk Wkrąż         | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Ignacy Myrczek       | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Jeremi Sowiński      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Kot Rozbójnik        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Ksenia Kirallen      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Malena Barandis      | 1 | ((240305-lea-strazniczka-lasu)) |
| Marian Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Mariusz Kupieczka    | 1 | ((240305-lea-strazniczka-lasu)) |
| Mariusz Trzewń       | 1 | ((210720-porwanie-daniela-terienaka)) |
| Marsen Gwozdnik      | 1 | ((220730-supersupertajny-plan-loreny)) |
| Melissa Durszenko    | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Mimoza Diakon        | 1 | ((220816-jak-wsadzic-ule-alanowi)) |
| Nadia Uprewien       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Napoleon Bankierz    | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Natalia Tessalon     | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Nataniel Morlan      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Oliwier Czepek       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Patrycja Radniak     | 1 | ((201215-dziewczyna-i-pies)) |
| Paweł Szprotka       | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Santino Mysiokornik  | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Serafina Ira         | 1 | ((210831-serafina-staje-za-wydrami)) |
| Staś Arienik         | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Stella Amakirin      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Triana Porzecznik    | 1 | ((240305-lea-strazniczka-lasu)) |
| Urszula Arienik      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Viirai d'Lechotka    | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Wiktor Satarail      | 1 | ((211102-satarail-pomaga-marysi)) |