---
categories: profile
factions: 
owner: public
title: Nastia Barbatov
---

# {{ page.title }}


# Generated: 



## Fiszki


* 43 lata, pilot + marine + salvager (atarienka) | @ 221022-derelict-okarantis-wejscie
* ENCAO:  +0--- |Anarchistyczna, nie ufa władzy;;Lubi żartować| VALS: Universalism, Stimulation >> Face| DRIVE: Utopia Star Trek | @ 221022-derelict-okarantis-wejscie

### Wątki


historia_darii
salvagerzy-anomalii-kolapsu

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221022-derelict-okarantis-wejscie   | 43 lata, pilot + marine + salvager (atarienka); |ENCAO: +0--- |Anarchistyczna, nie ufa władzy;;Lubi żartować| VALS: Universalism, Stimulation >> Face| DRIVE: Utopia Star Trek|; świetnie pilotuje Hiyori oraz drony. Zajmuje się też skanami dalekosiężnymi czy Hiyori / derelictowi nic nie grozi. | 0090-03-03 - 0090-03-14 |
| 221111-niebezpieczna-woda-na-hiyori | kupiła wodę od Ailiry nie sprawdzając jej specjalnie (zaufany dostawca), nie chce tego tak zostawić, ale ważniejsze jest chronienie załogi niż coś innego. | 0090-06-17 - 0090-06-21 |
| 221113-ailira-niezalezna-handlarka-woda | zdecydowała się na drastyczny ruch by utrzymać Hiyori po ostatniej operacji - poszła z Filipem (swoim eks chłopakiem) na operację najemniczą jako marine i dała Safirę jako P.O. Safira odmówiła, więc Kaspiana. | 0090-06-22 - 0090-06-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Daria Czarnewik      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Iga Mikikot          | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Jakub Uprzężnik      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Kaspian Certisarius  | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Leo Kasztop          | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Patryk Lapszyn       | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Safira d'Hiyori      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Ailira Niiris        | 2 | ((221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Ogden Barbatov       | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| SCA Hiyori           | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Filip Szukurkor      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Julia Karnit         | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Ludwik Trójkadur     | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |