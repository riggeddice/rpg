---
categories: profile
factions: 
owner: public
title: Waldemar Mózg
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190102-stalker-i-czerwone-myszy     | zaproponował Pięknotce dołączenie do Myszy i ostrzegł, że przecież Pięknotka ma wrogów w Pustogorze. Dlatego Pięknotka pozbyła się odpowiedzialności. | 0109-12-18 - 0109-12-21 |
| 190106-a-moze-pustogorska-mafia     | przyszedł podziękować Pięknotce i ją przeprosił za kłopoty z młodym Kasjanem. Wynegocjował, że Myszy nie zapłacą dużo - to byłoby niebezpieczne. | 0109-12-24 - 0109-12-26 |
| 190119-skorpipedy-krolewskiego-xirathira | przejął kontrolę nad rozpadającymi się Czerwonymi Myszami i próbuje je utrzymać w dobrym stanie. Poprosił Pięknotkę o rozwiązanie problemu ze skorpipedami. | 0110-01-14 - 0110-01-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 3 | ((190102-stalker-i-czerwone-myszy; 190106-a-moze-pustogorska-mafia; 190119-skorpipedy-krolewskiego-xirathira)) |
| Adela Kirys          | 2 | ((190106-a-moze-pustogorska-mafia; 190119-skorpipedy-krolewskiego-xirathira)) |
| Amadeusz Sowiński    | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Jan Kramczuk         | 1 | ((190102-stalker-i-czerwone-myszy)) |
| Kasjan Czerwoczłek   | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Olga Myszeczka       | 1 | ((190119-skorpipedy-krolewskiego-xirathira)) |
| Roland Grzymość      | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Sławomir Muczarek    | 1 | ((190119-skorpipedy-krolewskiego-xirathira)) |
| Tadeusz Rupczak      | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Teresa Mieralit      | 1 | ((190102-stalker-i-czerwone-myszy)) |
| Wiktor Satarail      | 1 | ((190119-skorpipedy-krolewskiego-xirathira)) |