---
categories: profile
factions: 
owner: public
title: Almeda Literna
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190208-herbata-grzyby-i-mimik       | policjantka zainfekowana Mimikiem Symbiotycznym; bardzo ambitna i chętna do ochrony. Niestety, skłóciła Dariusza i Jadwigę przez mimika. | 0110-03-04 - 0110-03-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Kotomin       | 1 | ((190208-herbata-grzyby-i-mimik)) |
| Baltazar Rączniak    | 1 | ((190208-herbata-grzyby-i-mimik)) |
| Dariusz Bankierz     | 1 | ((190208-herbata-grzyby-i-mimik)) |
| Jadwiga Pszarnik     | 1 | ((190208-herbata-grzyby-i-mimik)) |
| Pięknotka Diakon     | 1 | ((190208-herbata-grzyby-i-mimik)) |
| Stach Sosnowiecki    | 1 | ((190208-herbata-grzyby-i-mimik)) |