---
categories: profile
factions: 
owner: public
title: Adelajda Kalmiris
---

# {{ page.title }}


# Generated: 



## Fiszki


* Core Wound - Lie: "tylko mnie udało się uciec i tylko mnie udało się rodowi Eleny uratować." - "ród Samszar jest inny - to jedyny szlachetny, dobry ród; oni nie pozwalają na zło" | @ 230905-druga-tienka-przybywa-na-pomoc

### Wątki


salvagerzy-lohalian
rehabilitacja-eleny-samszar

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230905-druga-tienka-przybywa-na-pomoc | bardzo atrakcyjna, ale jednocześnie doświadczona osoba. Pokojówka i doświadczona agentka i komandoska. Podróżuje chronić Elenę. | 0095-10-05 - 0095-10-07 |
| 231024-rozbierany-poker-do-zwalczania-interis | wykorzystana przez Elenę jako dywersja dla tienek grających w karty. Skończyła w bieliźnie w rozbieranego pokera, co sprawiło, że żołnierze nie poczuli się całkowicie oszukani. Ona nie ma wstydu, więc bawiła się nieźle. | 0095-10-08 - 0095-10-10 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Cyprian Mirisztalnik | 2 | ((230905-druga-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Elena Samszar        | 2 | ((230905-druga-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Mikołaj Larnecjat    | 2 | ((230905-druga-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Estella Gwozdnik     | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Jaromir Gaburon      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Jarosław Mirelski    | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Maurycy Derwisz      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Paweł Lawarczak      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Tomasz Afagrel       | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Zenobia Samszar      | 1 | ((230905-druga-tienka-przybywa-na-pomoc)) |