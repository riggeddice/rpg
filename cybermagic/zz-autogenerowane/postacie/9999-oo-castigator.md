---
categories: profile
factions: 
owner: public
title: OO Castigator
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200624-ratujmy-castigator           | potężny statek artyleryjski na orbicie Astorii. Opanowany przez naleśniczych arystokratów z Aurum. Po wizycie Arianny (i Leony) - wrócił do formy. Uszkodzony przez Rozalię (tak naprawdę Eustachego), po tym jak Wiktor Satarail Skaził ixionem Azalię d'Alkaris. | 0110-10-15 - 0110-10-19 |
| 231011-ekstaflos-na-tezifeng        | uszkodzony zewnętrznie i strukturalnie, lekko Skażony, ale ogólnie działa bez zarzutu. Centrum dowodzenia Kurzmina i tymczasowa platforma szkoleniowa... | 0110-10-20 - 0110-10-22 |
| 231025-spiew-nielalki-na-castigatorze | przez problemy z Saitaerem i awarię Memoriam doszło do manifestacji Altarient przez przemycony potężny artefakt źródłowy, 'NieLalkę', zmieniającą znaczenia, percepcję i rzeczywistość. Szczęśliwie udało się wszystko zachować 'w środku', by we flocie nikt niczego nie zauważył (przynajmniej, nie oficjalnie). | 0110-10-24 - 0110-10-26 |
| 200729-nocna-krypta-i-emulatorka    | Kramer załatwił uprawnienia, Arianna przyzwała Kryptę, anihilator Castigatora wypalił Nocną Kryptę do zera, niszcząc zagrożenie Esuriit. | 0111-01-14 - 0111-01-20 |
| 190326-arcymag-w-raju               | superciężka artyleria Orbitera gotowa do zestrzelenia na planecie Elizy Iry jeśli do niczego nie dojdzie w osi Trzeci Raj - Eliza Ira. | 0111-07-18 - 0111-07-19 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 4 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Eustachy Korkoran    | 4 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Leona Astrienko      | 4 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Klaudia Stryk        | 3 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka; 231025-spiew-nielalki-na-castigatorze)) |
| Leszek Kurzmin       | 3 | ((200624-ratujmy-castigator; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Elena Verlen         | 2 | ((231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| OO Infernia          | 2 | ((231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Raoul Lavanis        | 2 | ((231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| AK Nocna Krypta      | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Anna Tessalon        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Antoni Kramer        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Ataienne             | 1 | ((190326-arcymag-w-raju)) |
| Damian Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Dawid Szardak        | 1 | ((190326-arcymag-w-raju)) |
| Eliza Ira            | 1 | ((190326-arcymag-w-raju)) |
| Feliks Walrond       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Fergus Salien        | 1 | ((190326-arcymag-w-raju)) |
| Grzegorz Kamczarnik  | 1 | ((190326-arcymag-w-raju)) |
| Igor Arłacz          | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Klaudiusz Terienak   | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Konstanty Keksik     | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Lars Kidironus       | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Marta Keksik         | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Mirela Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Natalia Gwozdnik     | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Olga Leszcz          | 1 | ((190326-arcymag-w-raju)) |
| OO Alkaris           | 1 | ((200624-ratujmy-castigator)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Tezifeng          | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Patryk Samszar       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Rozalia Wączak       | 1 | ((200624-ratujmy-castigator)) |
| TAI Eszara d'Castigator | 1 | ((231025-spiew-nielalki-na-castigatorze)) |