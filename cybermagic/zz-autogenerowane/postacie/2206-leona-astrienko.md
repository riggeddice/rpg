---
categories: profile
factions: 
owner: public
title: Leona Astrienko
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "orbiter, oddział verlen"
* owner: "public"
* title: "Leona Astrienko"

## Kim jest

### W kilku zdaniach

Ludzka zabójczyni magów z Eterni (cirrus), agentka Orbitera, z ostrą paranoją. Na oko słodkie, drobne stworzenie, ale w praktyce socjopatka niezrównana w walce wręcz. Modularna, używa wszczepów, specjalizuje się w niszczeniu magów. W okrutny sposób zdewastuje każdego, kto deprecjonuje ludzi lub ich gnębi. Lubi się bić i wygrywać, lubi straszyć innych... ogólnie, NIE DOTYKAĆ.

### Co się rzuca w oczy

* Motto: "Wszystko da się zniszczyć. Każdego zabić. Brutalnością i bezwzględnością osiągniesz sukces. A Eternia - niech spłonie."
* Mała psychotycznie uśmiechnięta blondyneczka z dużą ilością cyborgizacji, pixie cut i OGROMNĄ giwerą.
* Lubi dokuczać, wyzywać przeciwników i ostrą walkę. Nieustraszona. Skrajnie konfrontacyjna.
* Nie dba o swoje zdrowie czy życie. Chce po prostu rozwalić jak najwięcej wrogów Orbitera, najlepiej z Eterni.

### Jak sterować postacią

* SPEC: neurosprzężony komandos modularny, specjalistka do eksterminacji magów ćwiczona w Eterni. Stały bonus Dużej Przewagi do WSZYSTKICH operacji eksterminacji magów.
* Jeśli może rzucić chorą plotkę - rzuci ją. Jeśli może się bawić cudzym kosztem - zrobi to. Jeśli może dokuczyć, dokuczy.
* Skrajnie nieufna i paranoiczna wobec wszystkiego powiązanego z Eternią. Skrzywdzili ją i nie umie wybaczyć. W ogóle nie wybacza.
* Lubi być wykorzystywana jako psychotyczna broń czy agent terroru. Lubi walczyć z przeważającym, niemożliwym wrogiem. LUBI zadawać cierpienie i straszyć.
* Tryb psychotyczny (radość, walka, energia, wybuchy, zniszczenie, energia) i tryb poważny (smutne oczy, mało mówi, dużo pije).
* Lojalna wobec przyjaciół (których nie ma wielu); rzuci się by osłonić Ariannę przed eternijskim simulacrum nawet kosztem życia BEZ WAHANIA.
* Ceni siłę i odwagę. Gardzi jakąkolwiek formą słabości.
* Bawi się tak jak walczy - na całego. Żyje pełnią życia teraz.
* Jeśli jest możliwość wzmocnienia, pójdzie na to. Chce być najgroźniejszym cirrusem w galaktyce.
* Musi mieć silnego handlera, bo szanuje tylko siłę. Trudno jej się powstrzymać przed powodowaniem kryzysów jak się nudzi.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Trzecie miejsce w turnieju walki w zwarciu marines; pokonała magów katai w walce 1v1. Do dzisiaj paraduje z dumą pokazując odznaczenie.
* Zatrzymała walkę w barze pomiędzy magami - częściowo swoją siłą woli i rozkazami a częściowo wybijając strategiczne zęby. Nie musiała, ale mogła.
* Podczas wypadku w laboratorium na Kontrolerze Pierwszym zatrzymała Anomalię kupując wszystkim czas na ucieczkę. Wynik - miesiąc w szpitalu.
* Miała tylko sprowokować arystokratów do głupiego hazardu a zrobiła Krwawą Noc pięciu magom którzy ją napadli. Najlepsza zabawa ever - stała się postrachem a filmiki poszły.
* Gdy Elena była Skażona Esuriit, zaatakowała ją by unieszkodliwić. Esuriitowa Elena jest bardzo groźna; Leona jednak ją pokonała.

### Co się rzuca w oczy: Atuty, Przewagi, Zasoby (3, 6)

* SPEC: Neurosprzężony komandos modularny, specjalistka do eksterminacji magów ćwiczona w Eterni. Stały bonus Dużej Przewagi do WSZYSTKICH operacji eksterminacji magów.
* ATUT: Antymagiczne wszczepy i sprzęt. Jako, że jest człowiekiem, to nie ma problemów ze stosowaniem dwustronnie antymagicznych bytów (np. lapisowanych). Nie przeszkadzają jej.
* ATUT: Neurosprzężenie, zdolna do kontrolowania jednostek takich jak Entropik.
* ATUT: Bardzo silna cyborgizacja. Faktyczny homo superior, z dopalaczem, możliwością działania w próżni, własnym reaktorem itp.
* PRACA: Niezrównana w walce w zwarciu. Nie dość że wszystko jest bronią to jeszcze jej wszczepy sprawiają, że niełatwo ją zneutralizować.
* PRACA: Terror. Potrafi zastraszyć, zmusić do współpracy itp. Dodajmy fakt, że praktycznie wszędzie się dostanie...
* OPINIA: Niebezpieczna, nie podchodzić! Nieobliczalna, groźna oraz ogólnie chodzące ryzyko. Najlepiej zejść jej z drogi. Z dowodami.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Zniszczyli moją rodzinę. Zniszczyli mnie. Zostawili mnie - a Orbiter mnie uratował."
* CORE LIE: "Płomienie i choroba duszy odejdzie jeśli zniszczę ich wszystkich. Nie ma czegoś takiego jak dobry Eternianin."
* Nie nadaje się do normalnych kontaktów z ludźmi. Nie ma jak odejść do cywila. Jest mechanicznym koszmarem.
* Zawsze COŚ musi się dziać. Jak nic się nie dzieje, sama to sprowokuje. Musi mieć silnego handlera, bo szanuje tylko siłę.

### Serce i Wartości (3)

* Wartości
    * TAK: Stymulacja (skrajnie), Prestiż, Osiągnięcia (killcount, bigger monster)
    * NIE: Pokora
    * Coś się ZAWSZE musi dziać. Nie można się zatrzymać ani na moment, bo może być chwila na refleksję i co straciliśmy...
    * Jeśli Cię nie szanują to Cię zniszczą. Może być tylko jeden top dog - muszą POSTRZEGAĆ Cię jako apex predatora.
    * Życie i tak nie ma większego znaczenia, ale zawsze jest jeszcze jeden eternianin do zastrzelenia i jeden potwór do zabicia
    * Zostawili mnie na śmierć. Nie ma przeznaczenia. Sama zbuduję własne. Wygram każdą bitwę, zawsze.
* Ocean
    * E:+, N:-, C:-, A:-, O:0
    * Skrajnie agresywna, wybierająca strategie o wysokim zwrocie i rzucająca się na ryzyko impulsywna agentka która nie przegrywa czystą determinacją.
    * Nie dogaduje się z ludźmi. Ale szanuje tych silnych. Słabi są ofiarami które trzeba wzmocnić lub wykorzystać.
* Silnik
    * "There is always one more eternian bastard to hurt"
    * Tylko siła ma znaczenie. Będę silniejsza.
    * Osłonię swoje stado. 

## Inne

### Wygląd

.

### Coś Więcej

* ?

### Endgame

* ?


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly | marine na Królowej; wszyscy się jej boją. Chroni ludzi i dobrze się bawi tępiąc tienowatych. Niesterowalna. Jej pomysłem było upokorzenie Alezji przez Szymona Wanada. Pilnuje, by inżynierowie mogli odciąć ciepłą wodę (z woli Arianny). | 0100-05-06 - 0100-05-12 |
| 220928-kapitan-verlen-i-pojedynek-z-marine | sędzia pojedynku Arianna - Szymon. Uznała, że coś z Arianny będzie i ostrzegła ją by ta nic nie jadła i nie piła - tylko konserwy. Alezji też kiedyś powiedziała. | 0100-05-14 - 0100-05-16 |
| 221012-kapitan-verlen-i-niezapowiedziana-inspekcja | nikt nie wiedział co z sarderytami zrobić, więc przejęła nad nimi dowodzenie i np. robili kosmiczne spacery i grali w piłkę w kosmosie. | 0100-05-17 - 0100-05-21 |
| 200408-o-psach-i-krysztalach        | dowódca marine na Inferni; człowiek w świecie magów. Dowodziła operacją dzięki której pojmano Sebastiana. Drobny kłębek agresji; udaje sadystkę, ale jest oficerem. | 0110-09-29 - 0110-10-04 |
| 200624-ratujmy-castigator           | miała tylko sprowokować arystokratów do głupiego hazardu a zrobiła Krwawą Noc pięciu magom którzy ją napadli. Najlepsza zabawa ever - stała się postrachem a filmiki poszły... | 0110-10-15 - 0110-10-19 |
| 231011-ekstaflos-na-tezifeng        | weszła w zakład z Eustachym kto więcej zniszczy 'ech Saitaera' po Rozalii. Tauntowała Elenę w zakład i go wygrała (acz było blisko); Elena będzie jej służką przez pewien czas. Potem zrobiła insercję na Tezifeng; ogłuszyła kralotyczną Marionetkę (z trudem) i ją przechwyciła. Ogólnie, świetny dzień. | 0110-10-20 - 0110-10-22 |
| 231025-spiew-nielalki-na-castigatorze | znudzona, wkręca tienkę że Eustachy to Wielki Piaskowy Duch (tytuł lorda Neikatis) i bawi się w herolda Eustachego; gdy trzeba było przechwycić tiena próbującego wysadzić ekspres do kawy, poczekała cierpliwie (jak nie ona) i go przechwyciła. Umie nie tylko robić dowcipy ale też je przyjmować. | 0110-10-24 - 0110-10-26 |
| 200722-wielki-kosmiczny-romans      | źródło plotek pierwotnych odnośnie trójkąta Arianna - Eustachy - Elena. Potem pełni rolę straszaka dla WSZYSTKICH adoratorów Eleny - Tadeusza i Konrada. Nawet nikogo (poza Eustachym) nie walnęła solidnie. | 0111-01-10 - 0111-01-13 |
| 200729-nocna-krypta-i-emulatorka    | nie jest w stanie walczyć przeciw istocie klasy Emulatorki, ale chroniła skutecznie Zespół przed ghulami Esuriit, zabijając je precyzyjnie. | 0111-01-14 - 0111-01-20 |
| 200819-sekrety-orbitera-historia-prawdziwa | poznaliśmy jej historię - jest cirrusem, eternijskim łowcą magów. Tym razem walczyła z Tadeuszem z Eterni i jego simulacrum. Ikona Grozy. | 0111-01-24 - 0111-02-01 |
| 200826-nienawisc-do-swin            | wybudzona z regeneracyjnej śpiączki przez Martyna by być terrorem buntowników, zapłaciła strasznie zdrowiem, ale pomogła Ariannie opanować bunt. Strzeliła do jednego z buntowników; nic tak nie działa dobrze. | 0111-02-05 - 0111-02-11 |
| 200610-ixiacka-wersja-malictrix     | przeprowadziła akcję szturmu na stację Telira-Melusit VII, by uratować ludzi przed ixiacką Malictrix. Zero problemów, bo Mal nie stała jej na drodze XD. | 0111-05-01 - 0111-05-05 |
| 210804-infernia-jest-nasza          | wykorzystana jako psychotyczna broń odwracająca uwagę od Inferni by Eustachy mógł się wkraść na Infernię mimo żandarmów. Ciężko raniła żandarmów, w końcu zestrzelona trafiła do więzienia. | 0111-06-21 - 0111-06-24 |
| 210106-sos-z-haremu                 | przebrała się za seks-laskę, by móc pokroić czterech twardych ochroniarzy Horacego. Nie przeszkadza jej ten strój, ale zdecydowanie bardziej lubi kroić ochroniarzy ;-). | 0111-07-26 - 0111-07-27 |
| 210209-wolna-tai-na-k1              | zabawa życia ewakuując Andreę przed Rziezą: "eee-ooo-eee-ooo KARETKA JEDZIE!", autoryzowana przez Martyna by wszystkich odrzucić z drogi by Andrea opuściła K1. | 0111-08-30 - 0111-09-04 |
| 210218-infernia-jako-goldarion      | świetnie się bawiła upijając Tomasza Sowińskiego i kombinując, jak najlepiej spieprzyć mu życie. | 0111-09-16 - 0111-10-01 |
| 200429-porwanie-cywila-z-kokitii    | komandos; wpadła na Kokitię i ekstraktowała kilku cywilów plus cel (Alarę z córką). Ciężko poparzona, ale udowodniła że potrafi. | 0111-10-01 - 0111-10-06 |
| 210317-arianna-podbija-asimear      | jedyna siła ognia jaką Arianna zabrała ze sobą na spotkanie z Eleną. Wystarczająca. Pokonała kralotycznie kontrolowanych cywili PLUS strażników Tomasza Sowińskiego (z zaskoczenia). | 0111-10-18 - 0111-11-02 |
| 210421-znudzona-zaloga-inferni      | jak jest znudzona to jest tragedia na Inferni. Poluje na kuchcika pod prysznicem i patrzy na ludzi w nocy w odległości 10 cm od twarzy. Ale jak jest kryzys to unieszkodliwiła Jolantę Sowińską ot tak. | 0111-11-16 - 0111-11-19 |
| 210519-osiemnascie-oczu             | jej bezwzględna determinacja uratowała misję - zaakceptowała stratę Eleny, unieszkodliwiła Skażone Klaudię i Ariannę, wsadziła WSZYSTKICH do biovata z amnestykami i została na straży. Jak zaczęła wpadać pod infekcję, oddaliła się, by na pewno nie zrobić krzywdy reszcie załogi. | 0111-12-07 - 0111-12-18 |
| 231018-anomalne-awarie-athamarein   | dokucza Alicji Szadawir, chcąc ją skłonić do walki. Mówi, że Alicja się w nim podkochuje i rzuca weń stanikiem (czystym). Robi siarę Eustachemu, chcąc go wciągnąć w grę - a on po prostu chce mieć spokój. Siedzi w areszcie i Arianna jej nie ratuje :-(. | 0111-12-22 - 0111-12-27 |
| 210526-morderstwo-na-inferni        | włączona w sprawę poszukiwania Tala Marczaka przez Eustachego zaczęła go szukać bo "sprawa osobista". I nagle wszyscy zaczęli Tala szukać, by uratować go przed Leoną XD. | 0111-12-31 - 0112-01-06 |
| 210818-siostrzenica-morlana         | z lubością nazywa Eustachego "callsign kefir". Ratuje Ofelię z Eustachym używając swojej prędkości poruszania się, a potem wyłuskuje Ofelię ze skafandra i ją zasłania ciałem. | 0112-01-20 - 0112-01-24 |
| 210616-nieudana-infiltracja-inferni | Rola: "pies gończy". Wystrzeliła i dogoniła uciekającą przez hangary Flawię, złapała i unieszkodliwiła ją, po czym przyniosła z powrotem na Infernię :-). | 0112-01-27 - 0112-02-01 |
| 210901-stabilizacja-bramy-eterycznej | gdy Elena była Skażona Esuriit, zaatakowała ją by unieszkodliwić. Esuriitowa Elena jest bardzo groźna; Leona jednak ją pokonała. | 0112-02-09 - 0112-02-12 |
| 210922-ostatnia-akcja-bohaterki     | cieszyła się że zabije DWÓCH tienów eternijskich - Arianna przekonała ją by ta poczekała. Wpierw rozproszona NIE utrzymała terenu (armia minionów ją poraniła), POTEM ryzykując życiem utrzymała simulacrum odstępcy (miesiąc+ szpitala), ale serce jej się złamało, gdy dowiedziała się, że Martyn to honorowy eternijski tien. Jest w szpitalu i nie chce z nikim rozmawiać. | 0112-02-23 - 0112-03-09 |
| 210929-grupa-ekspedycyjna-kellert   | leży w szpitalu; ostrzegła Eustachego, że Martyn jest eternijskim lordem, miał niewolników i ogólnie zniszczy Infernię. Eustachy jest lekko sceptyczny ale ok - będzie uważał. | 0112-03-13 - 0112-03-16 |
| 211110-romans-dzieki-esuriit        | w stanie niesprawnym; wyszła ze szpitala, bo Infernia jej potrzebuje. Sama się wypisała z pomocą Eustachego. 20% pełnej mocy. | 0112-04-13 - 0112-04-14 |
| 211117-porwany-trismegistos         | słaba i ledwo aktywna, ale weszła na pokład Trismegistosa jako osłona. Dała się "pokonać" magom, ale czekała aż może zaatakować i naprawić sytuację. | 0112-04-15 - 0112-04-17 |
| 211124-prototypowa-nereida-natalii  | pokłóciła się ze swoim medykiem (Wawrzynem), rzuciła weń czymś wybuchowym a on odrzucił. Polubiła go trochę. Ma z kim się kłócić. Ma "swojego medyka". | 0112-04-20 - 0112-04-24 |
| 211208-o-krok-za-daleko             | podczas ujeżdżania Inferni przez Eustachego Wawrzyn ją uratował i zginął. Leona zaatakowała komputery Inferni z pełnej mocy. Ciężko przeszła śmierć Wawrzyna. | 0112-04-25 - 0112-04-26 |
| 211215-sklejanie-inferni-do-kupy    | zapolowała na Eustachego by go uratować od ixionu; zmanipulowała Kasandrę Diakon, by ta jej pomogła. Zastawiła pułapkę, używając czerwia Esuriit ZRANIŁA Eustachego. Dała się jednak mu przekonać, że on kontroluje Dianę. Została jako morderca magów; ktoś musi skończyć Eustachego i Ariannę jak nie będzie wyjścia... | 0112-04-27 - 0112-04-29 |
| 220610-ratujemy-porywaczy-eleny     | po dramatycznej augmentacji (cobra-class) miała okazję zaatakować statek niewolniczy Aureliona. Zrobiła apokalipsę. Próbowała się powstrzymać, ale część osób wyginęła - testuje nowe augmentacje. | 0112-09-15 - 0112-09-17 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 200624-ratujmy-castigator           | pakuje się w kłopoty hobbystycznie jak jest na Kontrolerze Pierwszym. Wniosek - NIGDY nie może być po prostu na Kontrolerze Pierwszym bez nadzoru... | 0110-10-19
| 200624-ratujmy-castigator           | po tym jak urządziła arystokratów na Castigatorze ma opinię Anioła Krwi I Śmierci. Królowa Terroru. Plus, ma co najmniej jedną wendettę przeciwko sobie. | 0110-10-19
| 231011-ekstaflos-na-tezifeng        | Elena będzie jej służką przez jakiś tydzień gdy będzie okazja; wygrała zakład eksterminacji małych 'ech Saitaera' na Castigatorze. | 0110-10-22
| 200819-sekrety-orbitera-historia-prawdziwa | święcie przekonana o ogromnej i pełnej pasji miłości Eustachego do Eleny. | 0111-02-01
| 200819-sekrety-orbitera-historia-prawdziwa | Ikona Grozy w Orbiterze. Popularnie wiedzą, że pokonała Simulacrum i wiedzą, jak masakruje wszystkich. Jej reputacja rośnie. She is TERROR. | 0111-02-01
| 200819-sekrety-orbitera-historia-prawdziwa | wdzięczna załodze Inferni. Wreszcie jest jedną z nich. I po zmianach dokonanych przez Martyna nie boli jej wszystko cały czas. | 0111-02-01
| 200819-sekrety-orbitera-historia-prawdziwa | będzie na pełnej mocy cirrusa - stanie się modularna (możliwość wymiany części wewnętrznych implantów na stole operacyjnym bez większych problemów). | 0111-02-01
| 200819-sekrety-orbitera-historia-prawdziwa | miesiąc z głowy na regenerację i odbudowę po tym, co się stało. | 0111-02-01
| 200826-nienawisc-do-swin            | postawiona awaryjnie przez Martyna ucierpiała koszmarnie. Ekstra 2 tygodnie zdjęcia z akcji, bo poniszczona. | 0111-02-11
| 200826-nienawisc-do-swin            | Ikona Terroru, Inkarnacja i Awatar Terroru. Stoi lojalnie za Arianną i będzie jej egzekutorem wobec KAŻDEGO. +999 do zastraszania i dyscyplinowania. | 0111-02-11
| 210804-infernia-jest-nasza          | następny tydzień spędza na zmianę w więzieniu i w medvacie po akcji z masakrą żandarmów pilnujących Infernię. | 0111-06-24
| 210519-osiemnascie-oczu             | wysokie uznanie ze strony Mariana Tosena z grupy antymemetyczej Orbitera. | 0111-12-18
| 210922-ostatnia-akcja-bohaterki     | 23 dni od końca tej sesji w szpitalu na Kontrolerze Pierwszym. | 0112-03-09
| 210922-ostatnia-akcja-bohaterki     | widzi Martyna jako tiena Eterni - pokazał czynami, zdolnościami i reputacją. EXTREMELY CONFLICTED. | 0112-03-09
| 220610-ratujemy-porywaczy-eleny     | za zgodą Arianny i z opłaty admirał Termii została wzmocniona i przebudowana na najgroźniejszego techno-cirrusa. -20 lat życia, -komfort, +siła ognia. | 0112-09-17

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 32 | ((200408-o-psach-i-krysztalach; 200429-porwanie-cywila-z-kokitii; 200610-ixiacka-wersja-malictrix; 200624-ratujmy-castigator; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210106-sos-z-haremu; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 220610-ratujemy-porywaczy-eleny; 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 231011-ekstaflos-na-tezifeng; 231018-anomalne-awarie-athamarein; 231025-spiew-nielalki-na-castigatorze)) |
| Eustachy Korkoran    | 27 | ((200408-o-psach-i-krysztalach; 200429-porwanie-cywila-z-kokitii; 200624-ratujmy-castigator; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210106-sos-z-haremu; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 220610-ratujemy-porywaczy-eleny; 231011-ekstaflos-na-tezifeng; 231018-anomalne-awarie-athamarein; 231025-spiew-nielalki-na-castigatorze)) |
| Klaudia Stryk        | 27 | ((200408-o-psach-i-krysztalach; 200429-porwanie-cywila-z-kokitii; 200610-ixiacka-wersja-malictrix; 200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210106-sos-z-haremu; 210209-wolna-tai-na-k1; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 220610-ratujemy-porywaczy-eleny; 231018-anomalne-awarie-athamarein; 231025-spiew-nielalki-na-castigatorze)) |
| Elena Verlen         | 21 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 210106-sos-z-haremu; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 220610-ratujemy-porywaczy-eleny; 231011-ekstaflos-na-tezifeng; 231018-anomalne-awarie-athamarein; 231025-spiew-nielalki-na-castigatorze)) |
| Martyn Hiwasser      | 14 | ((200408-o-psach-i-krysztalach; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210106-sos-z-haremu; 210209-wolna-tai-na-k1; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 220610-ratujemy-porywaczy-eleny)) |
| Antoni Kramer        | 10 | ((200429-porwanie-cywila-z-kokitii; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert)) |
| OO Infernia          | 7 | ((210218-infernia-jako-goldarion; 210929-grupa-ekspedycyjna-kellert; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Izabela Zarantel     | 5 | ((200819-sekrety-orbitera-historia-prawdziwa; 210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii)) |
| Kamil Lyraczek       | 5 | ((200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211215-sklejanie-inferni-do-kupy)) |
| Leszek Kurzmin       | 5 | ((200624-ratujmy-castigator; 200819-sekrety-orbitera-historia-prawdziwa; 220928-kapitan-verlen-i-pojedynek-z-marine; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Maria Naavas         | 5 | ((210818-siostrzenica-morlana; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Roland Sowiński      | 5 | ((210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| OO Castigator        | 4 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Tomasz Sowiński      | 4 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210818-siostrzenica-morlana)) |
| Adam Szarjan         | 3 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy)) |
| Aleksandra Termia    | 3 | ((200826-nienawisc-do-swin; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert)) |
| Arnulf Perikas       | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Damian Orion         | 3 | ((200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa)) |
| Daria Czarnewik      | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Diana d'Infernia     | 3 | ((210804-infernia-jest-nasza; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy)) |
| Jolanta Sowińska     | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Maja Samszar         | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Marian Tosen         | 3 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni)) |
| Olgierd Drongon      | 3 | ((200722-wielki-kosmiczny-romans; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| OO Królowa Kosmicznej Chwały | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| OO Tivr              | 3 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos)) |
| OO Żelazko           | 3 | ((200722-wielki-kosmiczny-romans; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Otto Azgorn          | 3 | ((210421-znudzona-zaloga-inferni; 210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert)) |
| Raoul Lavanis        | 3 | ((220610-ratujemy-porywaczy-eleny; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Tadeusz Ursus        | 3 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin)) |
| Wawrzyn Rewemis      | 3 | ((211110-romans-dzieki-esuriit; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| Władawiec Diakon     | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Alezja Dumorin       | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Erwin Pies           | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Feliks Walrond       | 2 | ((210526-morderstwo-na-inferni; 231025-spiew-nielalki-na-castigatorze)) |
| Flawia Blakenbauer   | 2 | ((210616-nieudana-infiltracja-inferni; 210901-stabilizacja-bramy-eterycznej)) |
| Grażyna Burgacz      | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Karol Reichard       | 2 | ((210526-morderstwo-na-inferni; 211110-romans-dzieki-esuriit)) |
| Klaudiusz Terienak   | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 231011-ekstaflos-na-tezifeng)) |
| Medea Sowińska       | 2 | ((200429-porwanie-cywila-z-kokitii; 210901-stabilizacja-bramy-eterycznej)) |
| OO Netrahina         | 2 | ((210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej)) |
| Szymon Wanad         | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Adalbert Brześniak   | 1 | ((210209-wolna-tai-na-k1)) |
| Adam Chrząszczewicz  | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| AK Nocna Krypta      | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Alara Ehmes          | 1 | ((200429-porwanie-cywila-z-kokitii)) |
| Alicja Szadawir      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Andrea Burgacz       | 1 | ((210209-wolna-tai-na-k1)) |
| Andrzej Gwozdnik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Anna Tessalon        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Artur Traffal        | 1 | ((210804-infernia-jest-nasza)) |
| Błażej Sowiński      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Diana Arłacz         | 1 | ((210106-sos-z-haremu)) |
| Franciszek Maszkiet  | 1 | ((210804-infernia-jest-nasza)) |
| Gilbert Bloch        | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Hestia d'Atropos     | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Hestia d'Neotik      | 1 | ((211208-o-krok-za-daleko)) |
| Hiacynt Samszar      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Horacy Aktenir       | 1 | ((210106-sos-z-haremu)) |
| Hubert Kerwelenios   | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Igor Arłacz          | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Jakub Bulgocz        | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Jakub Oroginiec      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Janus Krzak          | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Janusz Parzydeł      | 1 | ((210209-wolna-tai-na-k1)) |
| Jarosław Szarjan     | 1 | ((211208-o-krok-za-daleko)) |
| Jolanta Kopiec       | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Julia Aktenir        | 1 | ((210106-sos-z-haremu)) |
| Julian Muszel        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Kajetan Kircznik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Kasandra Destrukcja Diakon | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Klarysa Jirnik       | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Konrad Wolczątek     | 1 | ((200722-wielki-kosmiczny-romans)) |
| Konstanty Keksik     | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Lars Kidironus       | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Maciej Żarand        | 1 | ((210804-infernia-jest-nasza)) |
| Malictrix d'Pandora  | 1 | ((200610-ixiacka-wersja-malictrix)) |
| Mariusz Bulterier    | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Marta Keksik         | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Martyna Bianistek    | 1 | ((210317-arianna-podbija-asimear)) |
| Melwin Sito          | 1 | ((200610-ixiacka-wersja-malictrix)) |
| Miki Katasair        | 1 | ((210209-wolna-tai-na-k1)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Mirela Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Morrigan d'Tirakal   | 1 | ((210421-znudzona-zaloga-inferni)) |
| Natalia Aradin       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Natalia Gwozdnik     | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Nataniel Morlan      | 1 | ((210818-siostrzenica-morlana)) |
| Ofelia Morlan        | 1 | ((210818-siostrzenica-morlana)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Alaya             | 1 | ((210519-osiemnascie-oczu)) |
| OO Alkaris           | 1 | ((200624-ratujmy-castigator)) |
| OO Athamarein        | 1 | ((231018-anomalne-awarie-athamarein)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Opresor           | 1 | ((210616-nieudana-infiltracja-inferni)) |
| OO Samotność Gwiazd  | 1 | ((210804-infernia-jest-nasza)) |
| OO Tezifeng          | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Welgat            | 1 | ((200722-wielki-kosmiczny-romans)) |
| Patryk Samszar       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Persefona d'Infernia | 1 | ((210421-znudzona-zaloga-inferni)) |
| Remigiusz Alkarenit  | 1 | ((231018-anomalne-awarie-athamarein)) |
| Rozalia Teirik       | 1 | ((210106-sos-z-haremu)) |
| Rozalia Wączak       | 1 | ((200624-ratujmy-castigator)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| SC Fecundatis        | 1 | ((210818-siostrzenica-morlana)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| SCA Płetwal Błękitny | 1 | ((210317-arianna-podbija-asimear)) |
| Sebastian Alarius    | 1 | ((200826-nienawisc-do-swin)) |
| Sebastian Namczek    | 1 | ((200408-o-psach-i-krysztalach)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Stefan Torkil        | 1 | ((220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Szczepan Myrczek     | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| TAI Eszara d'Castigator | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| TAI Neita Lairossa   | 1 | ((210209-wolna-tai-na-k1)) |
| TAI Rzieza d'K1      | 1 | ((210209-wolna-tai-na-k1)) |
| Tal Marczak          | 1 | ((210526-morderstwo-na-inferni)) |
| Tomasz Dojnicz       | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Tomasz Ruppok        | 1 | ((220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Uśmiechniczka Konstrukcjonistka Aurora Diakon | 1 | ((231018-anomalne-awarie-athamarein)) |
| Vigilus Mevilig      | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Zygfryd Maus         | 1 | ((211117-porwany-trismegistos)) |