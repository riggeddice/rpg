---
categories: profile
factions: 
owner: public
title: Rafał Armadion
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230626-pustynny-final-igrzysk       | noktianin wykorzystany w grach Aurum, szybko zdominowany przez swój zespół. Służył jako mięso armatnie i minesweeper. Gdy skonfrontował się z Olafem, ostrzegł go cicho mimo trudnej sytuacji i pomógł mu pokonać swoich 'panów'. | 0083-10-12 - 0083-10-15 |
| 200916-smierc-raju                  | noktianin zajmujący się glukszwajnami w Trzecim Raju; na polecenie przekazania świń na pokład Tucznika do redukcji Skażenia zrobił to z przyjemnością. Poczciwy. | 0111-02-18 - 0111-02-20 |
| 201104-sabotaz-swini                | jako Rafał maskował się sabotujący miragent, co oznacza, że Rafał nie żyje od pewnego czasu. KIA. | 0111-03-13 - 0111-03-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazja Sowińska   | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Arianna Verlen       | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Ataienne             | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Eustachy Korkoran    | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Klaudia Stryk        | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Robert Garwen        | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Alan Klart           | 1 | ((230626-pustynny-final-igrzysk)) |
| Antoni Kmandir       | 1 | ((230626-pustynny-final-igrzysk)) |
| Bogdan Ubuddan       | 1 | ((230626-pustynny-final-igrzysk)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Diana Arłacz         | 1 | ((201104-sabotaz-swini)) |
| Elena Verlen         | 1 | ((200916-smierc-raju)) |
| Izabela Zarantel     | 1 | ((200916-smierc-raju)) |
| Jan Firatiel         | 1 | ((230626-pustynny-final-igrzysk)) |
| Lily Sanarton        | 1 | ((230626-pustynny-final-igrzysk)) |
| Maja Wurmramin       | 1 | ((230626-pustynny-final-igrzysk)) |
| Maks Ardyceń         | 1 | ((230626-pustynny-final-igrzysk)) |
| Marian Fartel        | 1 | ((200916-smierc-raju)) |
| Martyn Hiwasser      | 1 | ((201104-sabotaz-swini)) |
| Olaf Zuchwały        | 1 | ((230626-pustynny-final-igrzysk)) |
| Roman Wyrkmycz       | 1 | ((230626-pustynny-final-igrzysk)) |
| Wanessa Pyszcz       | 1 | ((200916-smierc-raju)) |