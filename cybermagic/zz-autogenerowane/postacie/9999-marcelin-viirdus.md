---
categories: profile
factions: 
owner: public
title: Marcelin Viirdus
---

# {{ page.title }}


# Generated: 



## Fiszki


* advancer i ekspert od zbierania śladów; żołnierz walki kosmicznej | @ 230720-savaranie-przed-obliczem-nihilusa
* OCEAN: (E-N+C+): pozornie nie mający osobowości cichy ekspert od znajdowania problemów tam gdzie nikt się ich nie spodziewa | @ 230720-savaranie-przed-obliczem-nihilusa
* VALS: (Benevolence, Security): "wszystko co robimy, robimy dla nas samych jutro; każdy kiedyś potrzebuje pomocy" | @ 230720-savaranie-przed-obliczem-nihilusa
* Core Wound - Lie: "zginęło zbyt wielu naszych i nic nie możemy na to poradzić" - "jeśli wszystkiego się dowiemy i przed wszystkim zabezpieczymy, przetrwamy" | @ 230720-savaranie-przed-obliczem-nihilusa
* styl: raczej cichy, woli demonstrować umiejętności niż gadać. | @ 230720-savaranie-przed-obliczem-nihilusa

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230720-savaranie-przed-obliczem-nihilusa | daleki advancer z wyjątkową antypatią do kotów; doszedł do stanu kapsuły ratunkowej uratowanej z Kantali Ravis. Uniknął wszystkich encounterów. | 0092-07-01 - 0092-07-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Agnieszka Serkis     | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Arnold Tapszecz      | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Igor Seklamant       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Jola Seklamant       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| JRN Kantala Ravis    | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Katrina Kirten       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Kiran Ravis          | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Lena Morazik         | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Rafał Kurrodis       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Ralena Annitas       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Ralf Tapszecz        | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |