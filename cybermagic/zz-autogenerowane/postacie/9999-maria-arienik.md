---
categories: profile
factions: 
owner: public
title: Maria Arienik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220119-sekret-samanty-arienik       | miragent emulujący 44 lata; "matka" Samanty, Błażeja, Uli. Stała się TAI. | 0085-07-21 - 0085-07-23 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arazille             | 1 | ((220119-sekret-samanty-arienik)) |
| Błażej Arienik       | 1 | ((220119-sekret-samanty-arienik)) |
| Felicjan Szarak      | 1 | ((220119-sekret-samanty-arienik)) |
| Klaudia Stryk        | 1 | ((220119-sekret-samanty-arienik)) |
| Ksenia Kirallen      | 1 | ((220119-sekret-samanty-arienik)) |
| Samanta Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| Sławomir Arienik     | 1 | ((220119-sekret-samanty-arienik)) |
| Talia Aegis          | 1 | ((220119-sekret-samanty-arienik)) |
| Urszula Arienik      | 1 | ((220119-sekret-samanty-arienik)) |