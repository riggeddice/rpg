---
categories: profile
factions: 
owner: public
title: Natasza Aniel
---

# {{ page.title }}


# Read: 

## Metadane

* factions: ""
* owner: "public"
* title: "Natasza Aniel"


## Kim jest

### Paradoksalny Koncept

Okultystka nie wierząca w magię.

### Motto


## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Zna i potrafi wykorzystać rytuały "magiczne" pochodzące z folkloru. Demony, istoty lasu, nic co nadnaturalne nie jest jej obce.
* ATUT: 
* SŁABA: Nie jest mistrzem sportów - wysiłek fizyczny jest dla innych.
* SŁABA: 

### O co walczy (3)

* ZA: Skusi się na każdą możliwość zarobku, nieważne jak bardzo szemraną. Chciwa. 
* ZA: 
* VS: 
* VS: 

### Znaczące Czyny (3)

* Przeprowadziła z bratem Edwardem rytuał, który poszedł nie po ich myśli. W wyniku tego Edward zagninął.

### Kluczowe Zasoby (3)

* COŚ: Sklep. Bunkier z kolekcją okultystycznych ksiąg, przekazywanych w rodzinie z pokolenia na pokolenie. Mieszkanie na poddaszu.
* KTOŚ: Felicja, złodziejka i włamywaczka. 
* WIEM: 
* OPINIA: Ktoś, kto może załatwić nawet najdziwniejsze przedmioty.

## Inne

### Wygląd

Niebieskie oczy, rude włosy. 160 cm wzrostu, drobna, prawie dziecięca budowa ciała. Ostre kości policzkowe i wyraźnie zarysowane obojczyki. Jasna cera z nielicznymi piegami. Chłopięcy ubiór, najczęściej nosi ciasne, skórzane spodnie i obszerną koszulę z dopasowaną do talii kamizelką.

### Coś Więcej



# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200423-lojalna-zdrajczyni           | głos łączący Felicję z Tomem, osoba negocjująca i dbająca o swój sklepik okultystyczny. Doszła do tego, że mają do czynienia z mimikiem symbiotycznym. | 0109-07-21 - 0109-07-24 |
| 200507-anomalna-figurka-zabboga     | odpowiednio pragmatyczna jak chodzi o kasę; wpierw wystawiła Jana w sprawie Melindy za kontakty z Aurum a potem załatwiła z Tukanem oddanie mu figurki. | 0109-09-19 - 0109-09-26 |
| 200513-trzyglowec-kontra-melinda    | koordynuje całość komunikacji z Grabarzem i osiłkami. Dba o swoje - mimo kryzysowej sytuacji z reality show, wyszła na wierzch choć raniąc reputację Podwiertu w Aurum. | 0109-11-08 - 0109-11-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Jan Łowicz           | 3 | ((200423-lojalna-zdrajczyni; 200507-anomalna-figurka-zabboga; 200513-trzyglowec-kontra-melinda)) |
| Kinga Kruk           | 3 | ((200423-lojalna-zdrajczyni; 200507-anomalna-figurka-zabboga; 200513-trzyglowec-kontra-melinda)) |
| Melinda Teilert      | 3 | ((200423-lojalna-zdrajczyni; 200507-anomalna-figurka-zabboga; 200513-trzyglowec-kontra-melinda)) |
| Katja Nowik          | 2 | ((200423-lojalna-zdrajczyni; 200513-trzyglowec-kontra-melinda)) |
| Adam Cześń           | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Damian Polwonien     | 1 | ((200507-anomalna-figurka-zabboga)) |
| Diana Lemurczak      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Duży Tom             | 1 | ((200423-lojalna-zdrajczyni)) |
| Felicja Melitniek    | 1 | ((200423-lojalna-zdrajczyni)) |
| Feliks Keksik        | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Gustaf Profnos       | 1 | ((200507-anomalna-figurka-zabboga)) |
| Mariusz Grabarz      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Rafał Torszecki      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Tomasz Tukan         | 1 | ((200507-anomalna-figurka-zabboga)) |