---
categories: profile
factions: 
owner: public
title: Ayna Marialin
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (N+O+): wiruje kalejdoskopem pomysłów, które stale ją inspirują i napędzają. Wiecznie martwi się jutrem i planuje. "WIEM, że nie ma niedźwiedzi. A jeśli są?" | @ 230723-crashlanding-furii-mataris
* VALS: (Family, Universalism): pozytywny wpływ przy pomocy Zespołu uratuje wszystko. "Jesteśmy w stanie kształtować naszą przyszłość. Ostrożnie." | @ 230723-crashlanding-furii-mataris
* Core Wound - Lie: "Nie jestem tak dobra w niczym jak moje przyjaciółki; przeze mnie coś im się stanie" - "Dojdę do tego, rozwiążę to! Nie zaskoczą nas niczym!" | @ 230723-crashlanding-furii-mataris
* Styl: Staranna i metodyczna, Ayna zawsze analizuje sytuację zanim podejmie działanie. | @ 230723-crashlanding-furii-mataris

### Wątki


pronoktianska-mafia-kajrata
furia-mataris-agentka-mafii

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230723-crashlanding-furii-mataris   | bardzo ostrożna Furia Mataris; moduje servary najlepiej jak potrafi by jak najdłużej przetrwały. Zapamiętała rozkład terenu i dekoduje dane od Kajrata. Ostrożna, staje po stronie Amandy. | 0081-06-15 - 0081-06-17 |
| 230729-furia-poluje-na-furie        | poświęciła swój servar by wzmocnić detekcję i znaleźć _stash_ Kajrata. Przekierowała energię z servara Xavery do Amandy. Zawaliła jaskinię, gdy Ralena próbowała je pokonać, ale skończyła bardzo ciężko ranna i prawie porwana przez Ralenę. | 0081-06-17 - 0081-06-18 |
| 230730-skazone-schronienie-w-fortecy | zbyt ranna; prosi by ją zostawiły, ale X+A się nie zgadzają. Odkryła, że na tym terenie nie działają drony. Pomogła z zaprojektowaniem planu zmiażdżenia Czarnych Czaszek. | 0081-06-18 - 0081-06-21 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230729-furia-poluje-na-furie        | ciężko ranna (nie może chodzić) i zatruta przez straszne korzenie w okolicach Enklaw; ma personal armour i nie ma sprzętu | 0081-06-18
| 230730-skazone-schronienie-w-fortecy | za 2 tygodnie będzie działać; znajduje się w Fortecy Symlotosu. Zgodnie z umową z Kajratem Lotos jej nie dotknie, ale wymienią ją za maga. | 0081-06-21

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 3 | ((230723-crashlanding-furii-mataris; 230729-furia-poluje-na-furie; 230730-skazone-schronienie-w-fortecy)) |
| Ernest Kajrat        | 3 | ((230723-crashlanding-furii-mataris; 230729-furia-poluje-na-furie; 230730-skazone-schronienie-w-fortecy)) |
| Xavera Sirtas        | 3 | ((230723-crashlanding-furii-mataris; 230729-furia-poluje-na-furie; 230730-skazone-schronienie-w-fortecy)) |
| Isaura Velaska       | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Leira Euridis        | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Lucia Veidril        | 1 | ((230723-crashlanding-furii-mataris)) |
| Raab Navan           | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Ralena Karimin       | 1 | ((230729-furia-poluje-na-furie)) |