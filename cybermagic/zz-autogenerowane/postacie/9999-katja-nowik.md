---
categories: profile
factions: 
owner: public
title: Katja Nowik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200423-lojalna-zdrajczyni           | powiązana z Aurum, kiedyś kochanka Dużego Toma. Teraz ma powiązania z Latającymi Rekinami. | 0109-07-21 - 0109-07-24 |
| 200513-trzyglowec-kontra-melinda    | poproszona przez Nataszę, poleciła Feliksa ("Pożeracza") jako proste rozwiązanie problemów ze znalezieniem mentalisty. | 0109-11-08 - 0109-11-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Jan Łowicz           | 2 | ((200423-lojalna-zdrajczyni; 200513-trzyglowec-kontra-melinda)) |
| Kinga Kruk           | 2 | ((200423-lojalna-zdrajczyni; 200513-trzyglowec-kontra-melinda)) |
| Melinda Teilert      | 2 | ((200423-lojalna-zdrajczyni; 200513-trzyglowec-kontra-melinda)) |
| Natasza Aniel        | 2 | ((200423-lojalna-zdrajczyni; 200513-trzyglowec-kontra-melinda)) |
| Adam Cześń           | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Diana Lemurczak      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Duży Tom             | 1 | ((200423-lojalna-zdrajczyni)) |
| Felicja Melitniek    | 1 | ((200423-lojalna-zdrajczyni)) |
| Feliks Keksik        | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Mariusz Grabarz      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Rafał Torszecki      | 1 | ((200513-trzyglowec-kontra-melinda)) |