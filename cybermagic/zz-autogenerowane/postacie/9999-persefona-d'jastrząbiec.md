---
categories: profile
factions: 
owner: public
title: Persefona d'Jastrząbiec
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191112-korupcja-z-arystokratki      | TAI, która strzeliła focha. Nie była w stanie inaczej skomunikować problemów bo miała blokady. Zdominowana przez Ataienne, niechętnie dzieliła się systemami. | 0110-07-04 - 0110-07-05 |
| 191201-ukradziony-entropik          | TAI skupiona na ochronie i wysokim poziomie, która czasem gubi szczegóły. Acz nie powinna aż tak ich gubić jak tym razem. | 0110-07-13 - 0110-07-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Mariusz Trzewń       | 2 | ((191112-korupcja-z-arystokratki; 191201-ukradziony-entropik)) |
| Minerwa Metalia      | 2 | ((191112-korupcja-z-arystokratki; 191201-ukradziony-entropik)) |
| Pięknotka Diakon     | 2 | ((191112-korupcja-z-arystokratki; 191201-ukradziony-entropik)) |
| Ataienne             | 1 | ((191112-korupcja-z-arystokratki)) |
| Hestia d'Itaran      | 1 | ((191201-ukradziony-entropik)) |
| Ignacy Myrczek       | 1 | ((191112-korupcja-z-arystokratki)) |
| Keraina d'Orion      | 1 | ((191201-ukradziony-entropik)) |
| Roland Grzymość      | 1 | ((191201-ukradziony-entropik)) |
| Sabina Kazitan       | 1 | ((191112-korupcja-z-arystokratki)) |
| Tadeusz Sklerzec     | 1 | ((191112-korupcja-z-arystokratki)) |