---
categories: profile
factions: 
owner: public
title: Udom Rapnak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211009-szukaj-serpentisa-w-lesie    | noktiański żołnierz; ranny i słaby. Wpierw pilnował jeńców (Trzewnia i żołnierzy), potem pomagał Edelmirze i przenieść ją do medbunkra. Zdecydował się z Edelmirą zostać i oddać w ręce astorian. | 0084-11-13 - 0084-11-14 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Agostino Karwen      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Dariusz Skórnik      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Edelmira Neralis     | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Felicjan Szarak      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Grzegorz Czerw       | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Klaudia Stryk        | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Ksenia Kirallen      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Mariusz Trzewń       | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Strażniczka Alair    | 1 | ((211009-szukaj-serpentisa-w-lesie)) |