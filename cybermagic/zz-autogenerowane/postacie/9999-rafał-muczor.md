---
categories: profile
factions: 
owner: public
title: Rafał Muczor
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190619-esuriit-w-sercu-alicji       | uczeń Szkoły Magów; chciał na plecach Alicji wejść do sławy i poważania. Nie udało mu się - zaczął jej pomagać by zostać ważnym i istotnym magiem. | 0110-04-16 - 0110-04-18 |
| 200226-strazniczka-przez-lzy        | Ściągnięty by pomóc Alicji w transformacji w Strażniczkę Esuriit; bardzo ciężko straumatyzowany tymi wydarzeniami. | 0110-07-29 - 0110-08-01 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alicja Kiermacz      | 2 | ((190619-esuriit-w-sercu-alicji; 200226-strazniczka-przez-lzy)) |
| Eliza Farnorz        | 2 | ((190619-esuriit-w-sercu-alicji; 200226-strazniczka-przez-lzy)) |
| Alina Anakonda       | 1 | ((200226-strazniczka-przez-lzy)) |
| Crystal d'Corieris   | 1 | ((200226-strazniczka-przez-lzy)) |
| Darek Ampieczak      | 1 | ((200226-strazniczka-przez-lzy)) |
| Kastor Miczokan      | 1 | ((200226-strazniczka-przez-lzy)) |
| Lucjusz Blakenbauer  | 1 | ((200226-strazniczka-przez-lzy)) |
| Tymon Grubosz        | 1 | ((190619-esuriit-w-sercu-alicji)) |