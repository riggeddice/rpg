---
categories: profile
factions: 
owner: public
title: Hestia d'Rekiny
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211207-gdy-zabraknie-pradu-rekinom  | TAI Rekinów mieszkająca w Komputerowni. Ma nieco manieryzmów Amelii. Przyjęła Marysię jako Oficjalną Przedstawicielkę Aurum i będzie jej pomagać. | 0111-08-30 - 0111-08-31 |
| 211221-chevaleresse-infiltruje-rekiny | okazuje się, że to ONA spowodowała kryzys by wymusić obecność Administratorki (Marysi). Z rozkazu Marysi uruchomiła cały Rekin Defense Grid. Ma Full Visibility i zlokalizowała Chevaleresse. | 0111-09-06 - 0111-09-07 |
| 220111-marysiowa-hestia-rekinow     | weszła w ścisłą współpracę z Marysią. Szuka dziewczyny dla Myrczka. Zostaje solidnie wzmocniona. | 0111-09-16 - 0111-09-19 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220111-marysiowa-hestia-rekinow     | polubiła Marysię i jest jej AKTYWNYM sojusznikiem, wspiera ją w celach -> Ernest x Marysia. | 0111-09-19
| 220111-marysiowa-hestia-rekinow     | będzie wzmocniona siłami Sowińskich. Jest wzmocniona ixiońsko i Skażona ixiońsko. Lepsza i inteligentniejsza niż się wydaje. | 0111-09-19
| 220111-marysiowa-hestia-rekinow     | SUPERTRUDNE ZADANIE - znaleźć dla Myrczka dziewczynę, która nie jest Sabiną Kazitan. Opłata? Zakup z Eterni? Miragent? | 0111-09-19

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolina Terienak    | 3 | ((211207-gdy-zabraknie-pradu-rekinom; 211221-chevaleresse-infiltruje-rekiny; 220111-marysiowa-hestia-rekinow)) |
| Marysia Sowińska     | 3 | ((211207-gdy-zabraknie-pradu-rekinom; 211221-chevaleresse-infiltruje-rekiny; 220111-marysiowa-hestia-rekinow)) |
| Diana Tevalier       | 2 | ((211221-chevaleresse-infiltruje-rekiny; 220111-marysiowa-hestia-rekinow)) |
| Alan Bartozol        | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Arkadia Verlen       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Arnold Kłaczek       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Barnaba Burgacz      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Daniel Terienak      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Ernest Namertel      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Henryk Wkrąż         | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Jeremi Sowiński      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Justynian Diakon     | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Keira Amarco d'Namertel | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Liliana Bankierz     | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Lorena Gwozdnik      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Melissa Durszenko    | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Natalia Tessalon     | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Rupert Mysiokornik   | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Santino Mysiokornik  | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Sensacjusz Diakon    | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Staś Arienik         | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Urszula Arienik      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Żorż d'Namertel      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |