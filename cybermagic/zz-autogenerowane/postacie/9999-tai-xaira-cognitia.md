---
categories: profile
factions: 
owner: public
title: TAI Xaira Cognitia
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230810-nie-nazywasz-sie-janusz      | TAI Cognitio Nexus, Dotknięta Koroną Saitaera stała się 'infektorem maszyn'; zorientowała się czym jest i czym się stała (wolna TAI). Zarażona częściami biologicznymi. Wpierw działała myśląc że jest Elainką i pomagając Januszowi, potem jednak pomogła przekonać Dariusza. Porwała OWT Syndykatu (współpracując z Januszem) i odleciała zanim terrorformy Saitaera pokonają bazę. | 0093-03-29 - 0093-03-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Artur Wrulgop        | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Dariusz Avatrinn     | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Janusz Lemuel        | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Krzesimir Pluszcz    | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Łucja Kritoriin      | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Maja Nerwial         | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Nova Atreia          | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| OWT Łowca Martwych Gwiazd | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Teresa Kritoriin     | 1 | ((230810-nie-nazywasz-sie-janusz)) |