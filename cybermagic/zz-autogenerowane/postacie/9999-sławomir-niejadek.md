---
categories: profile
factions: 
owner: public
title: Sławomir Niejadek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190827-rozpaczliwe-ratowanie-bii    | słup z Wolnych Ptaków; płacisz mu za to, że nie zadaje pytań jak się mu coś zleca. Tym razem użyty przez Talię by ukryć ślady BIA w pnączoszponach. | 0110-01-18 - 0110-01-21 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| BIA Tarn             | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Ernest Kajrat        | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Marek Puszczok       | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Mariusz Trzewń       | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Pięknotka Diakon     | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Talia Aegis          | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Tymon Grubosz        | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |