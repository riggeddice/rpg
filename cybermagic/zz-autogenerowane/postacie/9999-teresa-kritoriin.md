---
categories: profile
factions: 
owner: public
title: Teresa Kritoriin
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (C+A+): Perfekcjonistka z uśmiechem, zawsze dba o szczegóły. "Diabeł tkwi w szczegółach, ale i anioł też." | @ 230810-nie-nazywasz-sie-janusz
* VALS: (Conformity, Tradition): "Trzymajmy się razem, jak zawsze to robiliśmy." | @ 230810-nie-nazywasz-sie-janusz
* Core Wound - Lie: "?" - "?" | @ 230810-nie-nazywasz-sie-janusz
* Styl: Dokładna w każdym zadaniu, zawsze zwraca uwagę na małe rzeczy, które mogą zrobić różnicę. | @ 230810-nie-nazywasz-sie-janusz

### Wątki


neikatianska-gloria-saviripatel
tarcza-nox-aegis

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230810-nie-nazywasz-sie-janusz      | córka Łucji (awatara-terrorforma). Młoda praktykantka, świetnie schowana w bazie. Łucja zrobi WSZYSTKO by uratować córkę i Korona Cierni odpowiada. | 0093-03-29 - 0093-03-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Artur Wrulgop        | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Dariusz Avatrinn     | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Janusz Lemuel        | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Krzesimir Pluszcz    | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Łucja Kritoriin      | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Maja Nerwial         | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Nova Atreia          | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| OWT Łowca Martwych Gwiazd | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| TAI Xaira Cognitia   | 1 | ((230810-nie-nazywasz-sie-janusz)) |