---
categories: profile
factions: 
owner: public
title: Jan Łowny
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "szczeliniec, podwiert, karaczan"
* owner: "public"
* title: "Jan Łowny"


## Kim jest

### Paradoksalny Koncept

Prywatny detektyw i awanturnik, altruista nie wierzący w altruizm innych.

### Motto


## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Jak tego nie pobiję to to zastrzelę.
* ATUT: 
* SŁABA: Mściwy. Jeśli skrzywdzisz jego lub jego przyjaciół, strzeż się.
* SŁABA: Przyjmowanie pomocy. Podejrzliwy, nieufny i nie wierzy w altruizm innych. 

### O co walczy (3)

* ZA: Pomóc słabszym w potrzebie. Sam wyszedł z kiepskiej sytuacji i wie, co to znaczy mieć problemy.
* ZA: 
* VS: 
* VS: 

### Znaczące Czyny (3)

* Wydostał się z głębokiej studni, w której jego dawny przyjaciel pozostawił go na śmierć.

### Kluczowe Zasoby (3)

* COŚ: Pracownia chemiczno-rusznikarska do tworzenia specyficznych potrzebnych środków chemicznych/specjalnych naboi.
* KTOŚ: Mikołaj Rudzik, handlarz, złodziej, podróżnik, członek gangu dużego Toma.
* WIEM: 
* OPINIA: 

## Inne

### Wygląd

Niebieskie oczy, krótkie, brązowe włosy. 182 cm wzrostu, umięśniony, pokryty bliznami.
Na co dzień nosi lekką kamizelkę orchronną ukrytą pod wygodnym ubraniem, do tego spodnie bojówki z licznymi kieszeniami i pas ze schowkami (utility belt). U góry luźna koszula na którą narzucony jest płaszcz. Kapelusz.

### Coś Więcej

.

# Generated: 

