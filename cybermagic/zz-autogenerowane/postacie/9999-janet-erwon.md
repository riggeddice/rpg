---
categories: profile
factions: 
owner: public
title: Janet Erwon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191120-mafia-na-stacji-gorniczej    | oficer Orbitera odpowiedzialna za kontakty handlowe z Dorszant. Pod namową Szymona, zlitowała się i przekazała pomoc humanitarną by pomóc odeprzeć Kuratorów. | 0110-06-14 - 0110-06-25 |
| 200115-pech-komodora-ogryza         | komodor Orbitera; bardzo lubi różnego rodzaju manewry taktyczne, oskrzydlanie i practical jokes. Poszła o krok dalej niż chciała i "wygrała" akcję z Ogryzem... | 0110-07-20 - 0110-07-23 |
| 200129-nieudana-niechetna-inwazja   |  | 0110-10-22 - 0110-10-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Sonia Ogryz          | 2 | ((200115-pech-komodora-ogryza; 200129-nieudana-niechetna-inwazja)) |
| Dominik Ogryz        | 1 | ((200115-pech-komodora-ogryza)) |
| Eszara d'Dorszant    | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Gerwazy Kruczkut     | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Hestia d'Dorszant    | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Jaromir Uczkram      | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Kaja Tamaris         | 1 | ((200129-nieudana-niechetna-inwazja)) |
| Kamelia Termit       | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Klaudia Stryk        | 1 | ((200115-pech-komodora-ogryza)) |
| Laura Orion          | 1 | ((200129-nieudana-niechetna-inwazja)) |
| Marian Kurczak       | 1 | ((200129-nieudana-niechetna-inwazja)) |
| Martyn Hiwasser      | 1 | ((200115-pech-komodora-ogryza)) |
| Oliwier Pszteng      | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Rafał Kirlat         | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Rufus Karmazyn       | 1 | ((200129-nieudana-niechetna-inwazja)) |
| Stefan Ukrand        | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Szymon Szelmer       | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Wioletta Keiril      | 1 | ((200115-pech-komodora-ogryza)) |