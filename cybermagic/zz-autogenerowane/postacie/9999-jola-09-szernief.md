---
categories: profile
factions: 
owner: public
title: Jola-09 Szernief
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231122-sen-chroniacy-kochankow      | savarańska inżynier solarna i artystka, porwana przez Rovisa się w nim zakochała. Gdy Rovis ją osłaniał przed swoimi eks-przyjaciółmi, jej krew posłużyła do ko-manifestacji Alucis. Opuściła stację za sprzęt do recyklingu. | 0105-09-02 - 0105-09-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Damian Orczakin      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Dorion Fughar        | 1 | ((231122-sen-chroniacy-kochankow)) |
| Felina Amatanir      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Kalista Surilik      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Klasa Dyplomata      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Klasa Hacker         | 1 | ((231122-sen-chroniacy-kochankow)) |
| Klasa Oficer Naukowy | 1 | ((231122-sen-chroniacy-kochankow)) |
| Klaudiusz Widar      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Mawir Hong           | 1 | ((231122-sen-chroniacy-kochankow)) |
| Rovis Skarun         | 1 | ((231122-sen-chroniacy-kochankow)) |
| Szymon Alifajrin     | 1 | ((231122-sen-chroniacy-kochankow)) |