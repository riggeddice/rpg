---
categories: profile
factions: 
owner: public
title: Roman Wyrkmycz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230618-reality-show-z-zaskoczenia   | Astorianin, eks-Orbiter; pilnuje bezpieczeństwa zespołu i patrzy nieufnie na Olafa. Wziął dowodzenie taktyczne i działał w najbardziej niebezpiecznych sytuacjach (warta, padły zwierz). | 0083-10-02 - 0083-10-07 |
| 230625-przegrywy-ratuja-przegrywy   | chroni całość bazy i zespołu przed bardziej szalonymi pomysłami Olafa, planuje jak przetrwać przeciwko rajderom pustynnym. Zastawił dobre pułapki. | 0083-10-08 - 0083-10-11 |
| 230626-pustynny-final-igrzysk       | z Alanem i Maksem schowali się by Olaf i reszta zostali pułapką. Skutecznie z zaskoczenia, od pleców zabił jednego z morderczych napastników włócznią, co zniszczyło ich morale. | 0083-10-12 - 0083-10-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Klart           | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Lily Sanarton        | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Maja Wurmramin       | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Maks Ardyceń         | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Olaf Zuchwały        | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Antoni Kmandir       | 2 | ((230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Bogdan Ubuddan       | 2 | ((230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Jan Firatiel         | 1 | ((230626-pustynny-final-igrzysk)) |
| Rafał Armadion       | 1 | ((230626-pustynny-final-igrzysk)) |