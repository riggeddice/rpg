---
categories: profile
factions: 
owner: public
title: Karol Atenuatia
---

# {{ page.title }}


# Generated: 



## Fiszki


* ???: "???" (szary prochowiec, siwiejący brunet, oddalone oczy, uszkodzona proteza nogi) | @ 230124-kjs-wygrac-za-wszelka-cene
* (ENCAO:  +-+00 |Płomienny;;Żarliwy| VALS: Family, Security >> Self-direction| DRIVE: Noctian Legacy) | @ 230124-kjs-wygrac-za-wszelka-cene
* core wound: Nie możemy wszystkich uratować. Przegraliśmy. Nie mam szans, mam tylko walkę. | @ 230124-kjs-wygrac-za-wszelka-cene
* core lie: Walka by być wolnym i uratować kogo się da jest ostatnim co mogę zrobić. | @ 230124-kjs-wygrac-za-wszelka-cene
* styl: cichy, z boku AŻ eksploduje cytatem z Księgi Wiecznej Wojny. | @ 230124-kjs-wygrac-za-wszelka-cene
* "Wolność wymaga walki i poświęcenia." | @ 230124-kjs-wygrac-za-wszelka-cene
* SPEC: miragent, typ: infiltrator | @ 230124-kjs-wygrac-za-wszelka-cene

### Wątki


przeznaczenie-eleny-verlen

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230124-kjs-wygrac-za-wszelka-cene   | właściciel Kajis; próbował dowiedzieć się coś o lokalizacji w której się znajdują, ale wie jedynie o Elenie i tym że w Koszarowie Chłopięcym jest "coś" z dopingiem. | 0095-06-23 - 0095-06-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Apollo Verlen        | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Elena Verlen         | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Emilia Lawendowiec   | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| KDN Kajis            | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Lidia Nemert         | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Lucas Oktromin       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Marta Krissit        | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Serena Krissit       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Wojciech Namczak     | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Zuzanna Kraczamin    | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Żaneta Krawędź       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |