---
categories: profile
factions: 
owner: public
title: Dmitri Karpov
---

# {{ page.title }}


# Generated: 



## Fiszki


* astoriański sojusznik Kajrata chcący zniszczenia Grzymościa | @ 230913-operacja-spotkac-sie-z-dmitrim
* OCEAN (O- C+): very hands-on and methodical, gruff and silent. "Keep your friends close, but your enemies closer." | @ 230913-operacja-spotkac-sie-z-dmitrim
* VALS: (Tradition, Power): strength and control became paramount after his loss. While vengeance burns in his heart, he's not careless. "Strength allows you to shape the world." | @ 230913-operacja-spotkac-sie-z-dmitrim
* Core Wound - Lie: "straciłem to co kochałem najbardziej przez mafię" - "jeśli ich zniszczę, znajdę lepsze życie" | @ 230913-operacja-spotkac-sie-z-dmitrim
* Styl: relentless and gritty, willing to work with anyone; sometimes too trusting of those who promise aid against Grzymość | @ 230913-operacja-spotkac-sie-z-dmitrim
* Loss: Wolny Uśmiech framed Dmitri's family in a major scandal, causing them to lose their standing in society | @ 230913-operacja-spotkac-sie-z-dmitrim
* Work: Zwiadowca i Identyfikator zagrożeń; ma dobry pojazd (Narveen) i porusza się dobrze w trudnych warunkach | @ 230913-operacja-spotkac-sie-z-dmitrim

### Wątki


pronoktianska-mafia-kajrata
furia-mataris-agentka-mafii

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230913-operacja-spotkac-sie-z-dmitrim | astoriański sojusznik Kajrata; Wolny Uśmiech zabrał mu wszystko łącznie z córką. Mieszka na uboczu, w fortifarmie. Bardzo kocha żonę. Pomógł Xaverze i Amandzie, bo zwalczają Grzymościa. | 0081-06-28 - 0081-06-30 |
| 230920-legenda-o-noktianskiej-mafii | torturowany przez Grzymościowców; jest link 'skąd masz te leki'. Szczęśliwie, przetrwał. | 0081-07-10 - 0081-07-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 2 | ((230913-operacja-spotkac-sie-z-dmitrim; 230920-legenda-o-noktianskiej-mafii)) |
| Petra Karpov         | 2 | ((230913-operacja-spotkac-sie-z-dmitrim; 230920-legenda-o-noktianskiej-mafii)) |
| Xavera Sirtas        | 2 | ((230913-operacja-spotkac-sie-z-dmitrim; 230920-legenda-o-noktianskiej-mafii)) |
| Arnulf Poważny       | 1 | ((230913-operacja-spotkac-sie-z-dmitrim)) |
| Bogdan Gwiazdocisz   | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Irelia Kairanolis    | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Patryk Majwuron      | 1 | ((230920-legenda-o-noktianskiej-mafii)) |