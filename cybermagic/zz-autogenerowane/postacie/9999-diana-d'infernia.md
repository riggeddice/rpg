---
categories: profile
factions: 
owner: public
title: Diana d'Infernia
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210804-infernia-jest-nasza          | psychotyczna aparycja Diany Arłacz; wróciła wezwana przez Eustachego magią Paradoksu. Zraniła Żaranda infiltrującego Infernię i z radością wylizywała jego ranę. Potem tańczyła dla Eustachego. Arianna ją "egzorcyzmowała" magią. Ale wróci :-). | 0111-06-21 - 0111-06-24 |
| 210825-uszkodzona-brama-eteryczna   | powołana przez efekt Paradoksu Eustachego, nacisnęła za niego spust (którego nie chciał naciskać) i spowodowała kłopoty z efemerydami. Rozwiana przez Elenę. Obiecała Eustachemu, że Elena i Eustachy będą razem. | 0112-02-04 - 0112-02-07 |
| 211208-o-krok-za-daleko             | anomalizacja ixiońska zmieniła ją w TAI Inferni; integruje Persefonę, Morrigan i wiele innych bytów. Wszechobecny na Inferni byt, kontroluje Infernię totalnie. | 0112-04-25 - 0112-04-26 |
| 211215-sklejanie-inferni-do-kupy    | przekonana, że może POŻREĆ Serenit; nieco nie ma balansu między wiedzą i mocą. Dość słodka, jak na ixiońską stabilną efemerydę. Modeluje charakter po Leonie; psychopatyczna słodka lolitka. | 0112-04-27 - 0112-04-29 |
| 211222-kult-saitaera-w-neotik       | nadal jest przekonana, że jest silniejsza niż ixioński potwór Saitaera na Neotik. "Skoro nawet TA LASKA (Elena) to przetrwała to ja to zjem.". Chce współpracować z Eustachym. "Kochana lolitka Inferni". | 0112-04-30 - 0112-05-01 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211215-sklejanie-inferni-do-kupy    | jest słodsza i poczciwa w brutalny sposób, co dla nowych jest STRASZNE. "Młodsza siostrzyczka", creepy like fuck. W stylu Eustachego. Ale JEST słodsza. | 0112-04-29
| 211222-kult-saitaera-w-neotik       | absolutnie nie znosi Eleny. Nie chce z nią współpracować, pomagać itp. Tylko na wyraźny rozkaz Eustachego. | 0112-05-01
| 211222-kult-saitaera-w-neotik       | aktywnie CHCE pomóc Eustachemu. Nie wykonuje rozkazów ze strachu, ale też bo chce. | 0112-05-01

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 5 | ((210804-infernia-jest-nasza; 210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Eustachy Korkoran    | 5 | ((210804-infernia-jest-nasza; 210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Elena Verlen         | 4 | ((210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Klaudia Stryk        | 4 | ((210804-infernia-jest-nasza; 210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik)) |
| Adam Szarjan         | 3 | ((211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Leona Astrienko      | 3 | ((210804-infernia-jest-nasza; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy)) |
| OO Infernia          | 3 | ((210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy)) |
| Kasandra Destrukcja Diakon | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Maria Naavas         | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| AK Nocna Krypta      | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Aleksandra Termia    | 1 | ((210804-infernia-jest-nasza)) |
| Antoni Kramer        | 1 | ((210804-infernia-jest-nasza)) |
| Artur Traffal        | 1 | ((210804-infernia-jest-nasza)) |
| Flawia Blakenbauer   | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Franciszek Maszkiet  | 1 | ((210804-infernia-jest-nasza)) |
| Gilbert Bloch        | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Hestia d'Neotik      | 1 | ((211208-o-krok-za-daleko)) |
| Izabela Zarantel     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Jarosław Szarjan     | 1 | ((211208-o-krok-za-daleko)) |
| Kamil Lyraczek       | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Lutus Amerin         | 1 | ((211222-kult-saitaera-w-neotik)) |
| Maciej Żarand        | 1 | ((210804-infernia-jest-nasza)) |
| Maciek Kalmarzec     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Medea Sowińska       | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Samotność Gwiazd  | 1 | ((210804-infernia-jest-nasza)) |
| Roland Sowiński      | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Wawrzyn Rewemis      | 1 | ((211208-o-krok-za-daleko)) |