---
categories: profile
factions: 
owner: public
title: Magda Misteria Sarbanik
---

# {{ page.title }}


# Generated: 



## Fiszki


* czarodziejka mimikry i roślin, (33) | @ 230215-terrorystka-w-ambasadorce
* (ENCAO:  +00-0 |Brutalna;;Łatwo zdobywa przyjaciół| VALS: Family, Tradition, Achievement >> Conformity| DRIVE: CORRUPTED SOUL) | @ 230215-terrorystka-w-ambasadorce
* "Kidiron zabrał mi WSZYSTKO. Zabiorę mu reputację, arkologię i odzyskam moje wiły!" | @ 230215-terrorystka-w-ambasadorce
* "Lirvint była w trakcie odbudowy. Udałoby nam się gdyby nie Wy!!!" | @ 230215-terrorystka-w-ambasadorce
* AGENDA: 1) zniszczyć Kidirona ujawniając sabotaż arkologii Lirvint, 2) ewakuować wiły, 3) zniszczyć Tobiasza | @ 230215-terrorystka-w-ambasadorce
* ŚRODEK: 1) magia krwi 2) rośliny + spory (Black Heart) 3) wiły  | blood heart | @ 230215-terrorystka-w-ambasadorce

### Wątki


kidiron-zbawca-nativis

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230215-terrorystka-w-ambasadorce    | pragnęła odzyskać swoje wiły i ujawnić zbrodnię Kidirona. Tworzyła Czarne Serce, co zmieniało jej sposób myślenia. Ostatecznie została ewakuowana z Ambasadorki, ale Kidiron umieścił w niej tracker. | 0093-02-22 - 0093-02-23 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230215-terrorystka-w-ambasadorce    | ma na sobie tracker Kidirona, jest przerażona Tobiaszem i jego możliwościami. Wycofała się na pustynię Neikatis, tam będzie dość bezpieczna ze swoją magią. Chyba. | 0093-02-23

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Bartłomiej Korkoran  | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Eustachy Korkoran    | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Kalia Awiter         | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Rafał Kidiron        | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Ralf Tapszecz        | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Tobiasz Lobrak       | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Tymon Korkoran       | 1 | ((230215-terrorystka-w-ambasadorce)) |