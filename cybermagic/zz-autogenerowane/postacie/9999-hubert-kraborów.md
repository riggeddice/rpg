---
categories: profile
factions: 
owner: public
title: Hubert Kraborów
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190626-upadek-enklawy-floris        | kiedyś bandyta, dziś ma rodzinę. Oddał się w ręce Pustogoru; chce żyć normalnie. Mag. Spowolnił Pustogor by pozwolić innym z Floris na ucieczkę. Wierzy, że oni zginęli. | 0110-05-28 - 0110-05-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ariela Sirmin        | 1 | ((190626-upadek-enklawy-floris)) |
| Ernest Kajrat        | 1 | ((190626-upadek-enklawy-floris)) |
| Jolanta Teresis      | 1 | ((190626-upadek-enklawy-floris)) |
| Konrad Czukajczek    | 1 | ((190626-upadek-enklawy-floris)) |
| Marcel Sowiński      | 1 | ((190626-upadek-enklawy-floris)) |
| Nikola Kirys         | 1 | ((190626-upadek-enklawy-floris)) |
| Roman Rymtusz        | 1 | ((190626-upadek-enklawy-floris)) |
| Szymon Maszczor      | 1 | ((190626-upadek-enklawy-floris)) |
| Wargun Ira           | 1 | ((190626-upadek-enklawy-floris)) |