---
categories: profile
factions: 
owner: public
title: Aiden Wielkołacz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180623-krzyk-w-wyplywowie           | detektyw w Wypływowie, podejrzewa Kajetana o działania związane ze zniknięciem Zofii. Z natury nieufny; zależy mu na społeczności Wypływowa. Nie wie o magii. | 0109-07-02 - 0109-07-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kajetan Bosman       | 1 | ((180623-krzyk-w-wyplywowie)) |
| Kalina Rotmistrz     | 1 | ((180623-krzyk-w-wyplywowie)) |
| Małgorzata Nowacka   | 1 | ((180623-krzyk-w-wyplywowie)) |
| Stanisław Mirczak    | 1 | ((180623-krzyk-w-wyplywowie)) |