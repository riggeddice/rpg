---
categories: profile
factions: 
owner: public
title: Wiktor Satarail
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "arainit"
* type: "NPC"
* owner: "public"
* title: "Wiktor Satarail"


## Postać

### Koncept (1)

Vicinius - wojownik i badacz. Wojownik, badacz i kustosz; uwielbia muzykę i sztukę (często coś nuci). Na co dzień zajmuje się muzeami i starymi artefaktami, wieczorem uruchamia prawdziwą formę i dąży do zapewnienia sensownej przyszłości swojej rasie i swojemu ludowi. Blink + Strickler.

### Otoczenie (1)

Ciemność. Przede wszystkim dobrze czuje się w ciemności i w otwartej przestrzeni, pomiędzy atakami dalekiego zasięgu oraz działaniem bezpośrednim. Nie czuje się dobrze w miejscu gdzie jest sporo innych ludzi. Bardziej "potwór" niż "człowiek", acz potrafi się zaadaptować... z trudem.

### Misja (1)

Dba o swój lud i pragnie jego powodzenia. Pragnie znaleźć miejsce dla sobie pokrewnych. To często prowadzi do potrzeby destabilizacji sytuacji i struktur magów - jest to poświęcenie, na które jest zdolny.

### Wada (-2)

Jest magiem bliskim viciniusowi. W sytuacjach "ekstrawertycznych" sobie trochę nie radzi. Najlepiej na polu bitwy albo w bibliotece - zdecydowanie nie przy wystąpieniach publicznych czy przy działaniu związanemu z tłumem.

### Motywacje (1)

| Co chce by się działo? Co jest pożądane?                 | Co jest antytezą postaci? Co jest jej negacją?               |
|----------------------------------------------------------|--------------------------------------------------------------|
| znaleźć miejsce, w którym jego ród może żyć dobrze       | skrzywdzić istotę swojego rodu czy niewinnego viciniusa    |
| destabilizować istniejący porządek i budować chaos       | wzmacniać świat magów, jego struktury i działania     |
| powiększać swój arsenał bioform, eliksirów i eksperymentów | rezygnować z potencjalnie niebezpiecznej broni bo "to groźne" |
| pomagać słabszym jeśli nie zagraża to jego misji (magom też)   |  poświęcić niewinne osoby tylko dlatego, bo może wygrać |

### Zachowania (1)

| Jak zazwyczaj się zachowuje?                             | Pod wpływem jakich uczuć / w jakich okolicznościach to robi? |
|----------------------------------------------------------|--------------------------------------------------------------|
| elegancki, wytworny, oczytany erudyta; wyraźnie lubi ludzi |  domyślnie, gdy nic paranormalnego się nie dzieje i jest w uśpieniu |
| ostrożny, cichy, dyskretny i atakujący z zaskoczenia     | podczas walki, podczas starcia |
| charakterystyczny tik nerwowy; tendencja do ataku jako pierwszy | w gniewie; gdy jest pod silną presją; gdy się boi |
| celowe wykorzystywanie innych jako pionków        |   robienie komuś / czemuś krzywdy dla zadawania cierpienia i przyjemności |

### Umiejętności (4)

* Wojownik, zarówno w walce bezpośredniej swymi szponami jak i dalekosiężnej kwaśnym splunięciem. Do tego potrafi latać - oczywiście...
* Taktyk, odkrywanie czego pragnie jego przeciwnik i zmienienie tego w pułapkę. Zastawianie pułapek też tu się liczy.
* Kustosz muzeum, kocha sztukę każdego rodzaju oraz ma szeroką wiedzę na temat artefaktów tak naturalnych jak i magicznych.
* Biokonstruktor, mistrz ewolucji istot żywych. Naukowiec laboratoryjny, ale także ekspert od neogenetyki praktycznej.

### Magia (2 LUB 4)

#### Gdy kontroluje energię

* Ewolucja i przekształcanie istot żywych to jego podstawowa moc - czy to zmiana struktury siebie samego czy też istot powiązanych.
* Ukrywanie się, odwracanie uwagi i maskowanie obecności. Też: udawanie kogoś, kim nie jest.
* Stymulanty, wspomagania, ale też eliksiry kontroli umysłu i inne typu _confusion_ i _chaos_.

#### Gdy traci kontrolę

* Jego pragnienie ewolucji wymyka się spod kontroli gdy jest w dobrym humorze, wzmacniając wszystko i wszystkich dookoła.
* Gdy się boi lub jest zmartwiony, moc objawia się drastycznym ukrywaniem czy chaosem.

### Zasoby i otoczenie

#### Ogólnie (4)

* Bardzo rzadko działa sam; zwykle towarzyszy mu grupa wspierających go bioform, przygotowanych na tą akcję bezpośrednio.
* Podręczny biolab niezłej klasy; najczęściej jako bioforma.
* Nietypowej klasy stare artefakty o niewielkiej mocy, które może wykorzystać jako przynętę lub element chaosu.

#### Powiązane frakcje

{{ page.factions }}

## Opis

### Ogólnie

(29 minut budowania pełnej postaci, 180709).

### Motywacje


### Działanie


### Specjalne


### Magia


### Otoczenie


### Motto


## Historia


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181114-neutralizacja-artylerii-koszmarow | zirytowany tym, że magowie ingerują w Trzęsawisko Zjawosztup zdecydował się zrobić artylerię koszmarów. Artyleria została zneutralizowana przez słodką Pięknotkę w bardzo sprytny sposób. | 0109-10-20 - 0109-10-21 |
| 190105-turysci-na-trzesawisku       | nie jest Skażonym potworem i chyba nie jest sterowany przez Saitaera. Pomógł Pięknotce - a mógł zrobić cokolwiek. Chroni Trzęsawisko i powstrzymywał jego działanie. | 0109-12-22 - 0109-12-24 |
| 190116-wypalenie-saitaera-z-trzesawiska | próbował rozpaczliwie zatrzymać atak magów na Trzęsawisko - bez skutku. Potem Pięknotka rzuciła weń Kasjopeą, rozpraszając wpływ Saitaera. Jest wściekły jak osa. | 0110-01-07 - 0110-01-09 |
| 190119-skorpipedy-krolewskiego-xirathira | zdecydował się pomóc Oldze (chyba z nią flirtuje). Stworzył dziwne skorpipedy i odwiedza Olgę regularnie w jej Pustej Wsi. Chce jej pomóc. | 0110-01-14 - 0110-01-15 |
| 190928-ostatnia-misja-tarna         | na prośbę Pięknotki uderzył w Tiamenat by skażeniem biologicznym zniszczyć Tarna. Gdy Tarn przejął kompleks, Wiktor go Zatruł - ale darował życie. | 0110-01-27 - 0110-01-28 |
| 190127-ixionski-transorganik        | pomógł Pięknotce na Trzęsawisku by nie stała się nadmierna krzywda Wojtkowi; odwrócił jego transorganizację. | 0110-01-29 - 0110-01-30 |
| 190406-bardzo-kosztowne-lzy         | uratował kilkanaście nieszczęsnych wił z fabryk Sensus. Ma krwawą wendettę z Sensus. Starł się z Pięknotką, potem z Cieniem. Liże rany na bagnie. Nauczył się groźnych technik od Saitaera. | 0110-03-21 - 0110-03-26 |
| 190419-osopokalipsa-wiktora         | pokazał pełnię mocy tworząc osy by zniszczyć Sensoplex. Pięknotka przekonała go, że można ich wrobić i usunąć politycznie. Osiągnął cel. | 0110-03-29 - 0110-03-31 |
| 190524-corka-mimika                 | pomógł Córce Mimika i dostał ją pod swoją opiekę; dodatkowo wzbudził grozę w Podwiercie (bo co tym razem knuje) | 0110-04-20 - 0110-04-23 |
| 190505-szczur-ktory-chroni          | pomógł Pięknotce przeprowadzając Krystiana przez Trzęsawisko i biorąc do siebie Oliwię i Adelę na jakiś czas. Adelę nauczy, Oliwię wyleczy. | 0110-04-22 - 0110-04-24 |
| 190527-mimik-sni-o-esuriit          | zmartwiony ucieczką Mireli; poprosił Pięknotkę (alternatywą jest jego osobiste działanie). Dał Pięknotkce trochę krwi Mireli by ta zrobiła detektor. | 0110-05-07 - 0110-05-09 |
| 190721-kirasjerka-najgorszym-detektywem | znalazł bioformę kralotyczną więc podłożył ślady na Pięknotkę by do niego przyszła. Niestety, ślady znalazła Mirela. Wiktor zapoznał się z Emulatorami Orbitera. | 0110-06-10 - 0110-06-11 |
| 200418-wojna-trzesawiska            | zapowiedział wojnę jeśli będzie bombardowanie Trzęsawiska z orbity, pomoże uspokoić Trzęsawisko jeśli Pustogor pomoże mu ze zbudowaniem bioformy na bazie Sabiny. | 0110-08-16 - 0110-08-21 |
| 200623-adaptacja-azalii             | Orbiter próbował wejść na jego teren, na jego Trzęsawisko by wyszkolić Azalię d'Alkaris. On użył ixiońskiej energii do Skażenia oryginalnej Azalii na Alkarisie. | 0110-09-26 - 0110-10-04 |
| 210615-skradziony-kot-olgi          | znajdował się na zapleczu domku Olgi Myszeczki. Gdyby Olga nie odzyskała swojego somnibela, Wiktor by go dla niej odzyskał. | 0111-05-13 - 0111-05-15 |
| 211026-koszt-ratowania-torszeckiego | po otrzymaniu od Marysi opery dał Marysi robaka do ratowania Torszeckiego. "Winny zapłaci". Uznał Marysię za zdesperowaną lub arogancką. | 0111-08-01 - 0111-08-05 |
| 211102-satarail-pomaga-marysi       | pomścił somnibela Olgi. Stworzył Owada który przez Torszeckiego wszedł w Marka Samszara i zadał mu straszne cierpienie, uszkadzając Dzielnicę Rekinów. | 0111-08-09 - 0111-08-10 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181027-terminuska-czy-kosmetyczka   | zainteresuje się Brygidą Maczkowik - może uda się ją dostosować do rodu Satarail? | 0109-10-11
| 181114-neutralizacja-artylerii-koszmarow | ma lekką słabość do Pięknotki. Soft spot. Pięknotka nie jest kill on sight. | 0109-10-21
| 181230-uwiezienie-saitaera          | potencjalnie Skażony przez Saitaera. A przynajmniej, tak uważa Karla Mrozik. | 0109-12-09
| 190116-wypalenie-saitaera-z-trzesawiska | oczyszczony z wpływów Saitaera, ale jednocześnie wściekły na Pustogor za rany zadane Trzęsawisku, jak i na Pięknotkę za zabranie mu potencjalnych jeńców. | 0110-01-09
| 190119-skorpipedy-krolewskiego-xirathira | chce pomóc Oldze w jej świecie, z jej viciniusami i jej problemami. Niech Olga ma spokojne, fajne życie. | 0110-01-15
| 190928-ostatnia-misja-tarna         | zdobywa sporo wiedzy noktiańskiej dzięki współpracy z BIA Tarn. | 0110-01-28
| 190127-ixionski-transorganik        | opracowuje sposób odwrócenia ixiońskiej transorganizacji; częściowo już mu się to udało. | 0110-01-30
| 190406-bardzo-kosztowne-lzy         | wieczna wojna przeciwko Sensus; zarówno on wobec nich jak i oni wobec niego. | 0110-03-26

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 12 | ((181114-neutralizacja-artylerii-koszmarow; 190105-turysci-na-trzesawisku; 190116-wypalenie-saitaera-z-trzesawiska; 190119-skorpipedy-krolewskiego-xirathira; 190127-ixionski-transorganik; 190406-bardzo-kosztowne-lzy; 190419-osopokalipsa-wiktora; 190505-szczur-ktory-chroni; 190527-mimik-sni-o-esuriit; 190721-kirasjerka-najgorszym-detektywem; 200418-wojna-trzesawiska; 200623-adaptacja-azalii)) |
| Alan Bartozol        | 4 | ((190105-turysci-na-trzesawisku; 190116-wypalenie-saitaera-z-trzesawiska; 190419-osopokalipsa-wiktora; 190505-szczur-ktory-chroni)) |
| Olga Myszeczka       | 4 | ((190119-skorpipedy-krolewskiego-xirathira; 190721-kirasjerka-najgorszym-detektywem; 210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego)) |
| Adela Kirys          | 3 | ((190119-skorpipedy-krolewskiego-xirathira; 190505-szczur-ktory-chroni; 190527-mimik-sni-o-esuriit)) |
| Marysia Sowińska     | 3 | ((210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi)) |
| Erwin Galilien       | 2 | ((190127-ixionski-transorganik; 190527-mimik-sni-o-esuriit)) |
| Ignacy Myrczek       | 2 | ((200418-wojna-trzesawiska; 211026-koszt-ratowania-torszeckiego)) |
| Karla Mrozik         | 2 | ((200418-wojna-trzesawiska; 200623-adaptacja-azalii)) |
| Kasjopea Maus        | 2 | ((190116-wypalenie-saitaera-z-trzesawiska; 190419-osopokalipsa-wiktora)) |
| Marek Samszar        | 2 | ((210615-skradziony-kot-olgi; 211102-satarail-pomaga-marysi)) |
| Minerwa Metalia      | 2 | ((190127-ixionski-transorganik; 200623-adaptacja-azalii)) |
| Mirela Satarail      | 2 | ((190524-corka-mimika; 190527-mimik-sni-o-esuriit)) |
| Paweł Szprotka       | 2 | ((210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego)) |
| Rafał Torszecki      | 2 | ((211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi)) |
| Saitaer              | 2 | ((190116-wypalenie-saitaera-z-trzesawiska; 190127-ixionski-transorganik)) |
| Sensacjusz Diakon    | 2 | ((211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi)) |
| Tymon Grubosz        | 2 | ((181114-neutralizacja-artylerii-koszmarow; 190127-ixionski-transorganik)) |
| Adrian Wężoskór      | 1 | ((190406-bardzo-kosztowne-lzy)) |
| Aida Serenit         | 1 | ((190721-kirasjerka-najgorszym-detektywem)) |
| Antoni Żuwaczka      | 1 | ((190524-corka-mimika)) |
| Arkadia Verlen       | 1 | ((210615-skradziony-kot-olgi)) |
| ASD Centurion        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Azalia d'Alkaris     | 1 | ((200623-adaptacja-azalii)) |
| BIA Tarn             | 1 | ((190928-ostatnia-misja-tarna)) |
| Dariusz Puszczak     | 1 | ((190524-corka-mimika)) |
| Ernest Kajrat        | 1 | ((190505-szczur-ktory-chroni)) |
| Ernest Namertel      | 1 | ((211102-satarail-pomaga-marysi)) |
| Eustachy Mrownik     | 1 | ((190928-ostatnia-misja-tarna)) |
| Felicja Melitniek    | 1 | ((181114-neutralizacja-artylerii-koszmarow)) |
| Gabriel Ursus        | 1 | ((200418-wojna-trzesawiska)) |
| Hestia d'Tiamenat    | 1 | ((190928-ostatnia-misja-tarna)) |
| Hieronim Maus        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Ida Tiara            | 1 | ((190406-bardzo-kosztowne-lzy)) |
| Julia Kardolin       | 1 | ((210615-skradziony-kot-olgi)) |
| Karolina Erenit      | 1 | ((190127-ixionski-transorganik)) |
| Karolina Terienak    | 1 | ((211102-satarail-pomaga-marysi)) |
| Karradrael           | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Kasjan Czerwoczłek   | 1 | ((190505-szczur-ktory-chroni)) |
| Kirył Najłalmin      | 1 | ((190127-ixionski-transorganik)) |
| Konrad Wączak        | 1 | ((200623-adaptacja-azalii)) |
| Krystian Namałłek    | 1 | ((190505-szczur-ktory-chroni)) |
| Ksenia Kirallen      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Liliana Bankierz     | 1 | ((210615-skradziony-kot-olgi)) |
| Lorena Gwozdnik      | 1 | ((211102-satarail-pomaga-marysi)) |
| Lucjusz Blakenbauer  | 1 | ((190505-szczur-ktory-chroni)) |
| Mirela Orion         | 1 | ((190721-kirasjerka-najgorszym-detektywem)) |
| Napoleon Bankierz    | 1 | ((190127-ixionski-transorganik)) |
| Oliwia Namałłek      | 1 | ((190505-szczur-ktory-chroni)) |
| Pedro Ronfak         | 1 | ((190928-ostatnia-misja-tarna)) |
| Sabina Kazitan       | 1 | ((200418-wojna-trzesawiska)) |
| Sławomir Muczarek    | 1 | ((190119-skorpipedy-krolewskiego-xirathira)) |
| Talia Aegis          | 1 | ((190928-ostatnia-misja-tarna)) |
| Teresa Mieralit      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Waldemar Mózg        | 1 | ((190119-skorpipedy-krolewskiego-xirathira)) |
| Wojtek Kurczynos     | 1 | ((190127-ixionski-transorganik)) |