---
categories: profile
factions: 
owner: public
title: Stanisław Uczantor
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220831-czarne-helmy-i-robaki        | social worker którego chciała okraść Ardilla i który ją będzie resocjalizował (XD). Były Robak; pod wpływem Ardilli opuścił Robaki i oddał się pod opiekę Kidironów | 0092-08-15 - 0092-08-27 |
| 220914-dziewczynka-trianai          | ex-social worker; karmił "Ducha" arkologii i próbuje resocjalizować Ardillę, by ta została przedszkolanką. | 0092-09-10 - 0092-09-11 |
| 230614-atak-na-kidirona             | poprosił Ardillę o spotkanie by poznać sekrety arkologii Nox Aegis. Ardilla nic nie wie. Wie o tym, że w okolicy jest więcej noktiańskich dzieci niż powinno być. | 0093-03-22 - 0093-03-24 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220831-czarne-helmy-i-robaki        | wzgardzony i stracił pracę jako social worker, bo "eks-Robak i to zdrajca" | 0092-08-27

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 3 | ((220831-czarne-helmy-i-robaki; 220914-dziewczynka-trianai; 230614-atak-na-kidirona)) |
| Eustachy Korkoran    | 3 | ((220831-czarne-helmy-i-robaki; 220914-dziewczynka-trianai; 230614-atak-na-kidirona)) |
| Bartłomiej Korkoran  | 2 | ((220914-dziewczynka-trianai; 230614-atak-na-kidirona)) |
| Celina Lertys        | 1 | ((220831-czarne-helmy-i-robaki)) |
| Dalmjer Servart      | 1 | ((230614-atak-na-kidirona)) |
| Jan Lertys           | 1 | ((220831-czarne-helmy-i-robaki)) |
| Kalia Awiter         | 1 | ((230614-atak-na-kidirona)) |
| Karina Nezerin       | 1 | ((220914-dziewczynka-trianai)) |
| Lycoris Kidiron      | 1 | ((230614-atak-na-kidirona)) |
| OO Infernia          | 1 | ((230614-atak-na-kidirona)) |
| Rafał Kidiron        | 1 | ((230614-atak-na-kidirona)) |
| Ralf Tapszecz        | 1 | ((230614-atak-na-kidirona)) |
| SAN Szare Ostrze     | 1 | ((230614-atak-na-kidirona)) |
| Tymon Korkoran       | 1 | ((220831-czarne-helmy-i-robaki)) |
| Wojciech Czerpń      | 1 | ((220831-czarne-helmy-i-robaki)) |