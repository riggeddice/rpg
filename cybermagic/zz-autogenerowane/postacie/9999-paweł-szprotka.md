---
categories: profile
factions: 
owner: public
title: Paweł Szprotka
---

# {{ page.title }}


# Generated: 



## Fiszki


* weterynarz | @ 230303-the-goose-from-hell
* (ENCAO:  -++0+ |Skłonny do refleksji;; Skryty;; Trwożliwy | VALS: Benevolence, Universalism, Self-direction >> Achievement| DRIVE: Utopia (zwierzaki)) | @ 230303-the-goose-from-hell

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210615-skradziony-kot-olgi          | AMZ. Ma relację uczeń-mistrz z Olgą; jest uciekinierem eternijskim (fakt nieznany) którego Olga wyprowadziła z dziczy, nauczyła jak funkcjonować i dała mu rekomendację do AMZ. Próbuje odzyskać somnibela ukradzionego Oldze i za to Rekiny go tępią. | 0111-05-13 - 0111-05-15 |
| 211026-koszt-ratowania-torszeckiego | przyznał się Marysi, że jest zbiegłym eternianinem i zdobył zapewnienie, że ta będzie go chronić. Zorganizował Marysi spotkanie z Olgą. Jest weterynarzem. Chroni go też "Dama w Błękicie". Boi się tego, że tu jest Ernest z Eterni. Uważa, że Ernest jest tu go porwać (srs?). | 0111-08-01 - 0111-08-05 |
| 211127-waśń-o-ryby-w-majklapcu      | zajmuje się leczeniem ryb i gęsi w Majkłapcu z ran kotów-pacyfikatorów (z ramienia farmy Krecik). Z samego nagrania zauważył, że koty były czymś naćpane - ktoś im coś podał. Karo poprowadziła go by z Danielem stworzył detektor kotów po sierści. | 0111-08-19 - 0111-08-24 |
| 230303-the-goose-from-hell          | Created the anomalous goose to protect animals from stupid hunters, made the tastiest goose food, and provided guidance on setting the trap to capture a goose. | 0111-10-28 - 0111-10-30 |
| 230331-an-unfortunate-ratnapping    | assists Julia in designing the perfect rat trap and lure. Not really present; more of advisory role. | 0111-11-08 - 0111-11-10 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210615-skradziony-kot-olgi          | Rekiny na niego polują by zemścić się za to, że Rekin nie jest całkowicie bezkarny na tym terenie. | 0111-05-15
| 211026-koszt-ratowania-torszeckiego | jest przekonany, że Marysia Sowińska (tienka) traktuje go tylko jak zasób. Ale obiecała, że będzie go chronić. Więc jest dobrze. | 0111-08-05

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Julia Kardolin       | 3 | ((210615-skradziony-kot-olgi; 230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Alex Deverien        | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Arkadia Verlen       | 2 | ((210615-skradziony-kot-olgi; 211127-waśń-o-ryby-w-majklapcu)) |
| Carmen Deverien      | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| kot-pacyfikator Tobias | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Ksenia Kirallen      | 2 | ((211026-koszt-ratowania-torszeckiego; 211127-waśń-o-ryby-w-majklapcu)) |
| Marysia Sowińska     | 2 | ((210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego)) |
| Olga Myszeczka       | 2 | ((210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego)) |
| Rafał Torszecki      | 2 | ((211026-koszt-ratowania-torszeckiego; 211127-waśń-o-ryby-w-majklapcu)) |
| Teresa Mieralit      | 2 | ((211026-koszt-ratowania-torszeckiego; 230303-the-goose-from-hell)) |
| Wiktor Satarail      | 2 | ((210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego)) |
| Alicja Trawlis       | 1 | ((230303-the-goose-from-hell)) |
| Daniel Terienak      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Genowefa Krecik      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Ignacy Myrczek       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Iwan Zawtrak         | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Karolina Terienak    | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Liliana Bankierz     | 1 | ((210615-skradziony-kot-olgi)) |
| Marek Samszar        | 1 | ((210615-skradziony-kot-olgi)) |
| Radosław Turkamenin  | 1 | ((230331-an-unfortunate-ratnapping)) |
| Sensacjusz Diakon    | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Stella Amakirin      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |