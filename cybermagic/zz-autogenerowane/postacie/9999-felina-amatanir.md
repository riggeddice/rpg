---
categories: profile
factions: 
owner: public
title: Felina Amatanir
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (N- C+) | "Nieustraszona i niezależna, wyróżnia się determinacją i profesjonalizmem" | VALS: (Power, Security) | DRIVE: "Porządek i sprawiedliwość, ponad kontrakt" | @ 231119-tajemnicze-tunele-sebirialis
* drakolitka o zwiększonych parametrach; wywalili ją z zarządu i z dowodzenia ochroną po tej operacji | @ 231119-tajemnicze-tunele-sebirialis

### Wątki


agencja-lux-umbrarum
problemy-con-szernief

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231119-tajemnicze-tunele-sebirialis | nieustraszona; jak tylko doszła do tego że nie może zrobić Dobra będąc w radzie i zablokowana przez kontrakt, szybko opuściła kontrakt. Poszła do tuneli Sebiralis szukać Kalisty i inwestorów. Spotkawszy Alteris, udało jej się ewakuować z Kalistą. Doszła do współpracy z NavirMed i jej się to nie podoba. Nie chce współpracować z Agencją, ale nie ma wyjścia. | 0104-11-20 - 0104-11-24 |
| 231213-polowanie-na-biosynty-na-szernief | próbuje pomóc Agencji, ale jest zablokowana przez Radę; część jej podwładnych (o czym nie wie) jest zainfekowana Parasektem. Nadal dała polecenie pomocy w roznoszeniu 'krewetkowych sensorów' po stacji i podpowiada Agencji tam gdzie jest w stanie. | 0105-07-26 - 0105-07-31 |
| 231122-sen-chroniacy-kochankow      | wie gdzie schował się Rovis i próbuje opanować i utrzymać CON Szernief do kupy, ale ma problemy przez energie magiczne. Współpracuje z Agencją. Mimo problemów, dalej ma zaufanie populacji stacji. | 0105-09-02 - 0105-09-05 |
| 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa | nadal zablokowana przez Radę, ale zebrała dane zdjęciowe od Kalisty i pokazała je Agencji - tak wygląda ofiara potwora (The Hunter). I że Kalista dostała je od Mawira. | 0105-12-11 - 0105-12-13 |
| 231221-pan-skarpetek-i-odratowany-ogrod | odcięta od własnych systemów przez swoich ludzi dotkniętych Alteris; briefowała Zespół i skierowała ich na Ignatiusa i innych. | 0106-04-25 - 0106-04-27 |
| 240117-dla-swych-marzen-warto       | wezwała Agencję rozpoznając Alteris w nagraniach o Szmaciarzu. Silnie przekonywała Estrila do współpracy z Agencją. Sprawdziła i dostarczała dossier ludzi na stacji. Kompetentna i skuteczna, acz w roli wsparcia. | 0106-11-04 - 0106-11-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kalista Surilik      | 6 | ((231119-tajemnicze-tunele-sebirialis; 231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto)) |
| Klasa Sabotażysta    | 5 | ((231119-tajemnicze-tunele-sebirialis; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto)) |
| Klasa Dyplomata      | 4 | ((231119-tajemnicze-tunele-sebirialis; 231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klasa Oficer Naukowy | 4 | ((231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto)) |
| Mawir Hong           | 4 | ((231122-sen-chroniacy-kochankow; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto)) |
| Klasa Hacker         | 3 | ((231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Aerina Cavalis       | 2 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240117-dla-swych-marzen-warto)) |
| Artur Tavit          | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Dorion Fughar        | 2 | ((231122-sen-chroniacy-kochankow; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Ignatius Sozyliw     | 2 | ((231119-tajemnicze-tunele-sebirialis; 231221-pan-skarpetek-i-odratowany-ogrod)) |
| Klasa Inżynier       | 2 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod)) |
| Larkus Talvinir      | 2 | ((231119-tajemnicze-tunele-sebirialis; 240117-dla-swych-marzen-warto)) |
| Vanessa d'Cavalis    | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Aniela Kafantelas    | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Damian Orczakin      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Delgado Vitriol      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Eryk Kawanicz        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Estril Cavalis       | 1 | ((240117-dla-swych-marzen-warto)) |
| Gabriel Septenas     | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Janvir Krassus       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Jola-09 Szernief     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Kaldor Czuk          | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klasa Biurokrata     | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Klaudiusz Widar      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Leon Hurmniow        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Maia Sakiran         | 1 | ((240117-dla-swych-marzen-warto)) |
| Malik Darien         | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Marta Sarilit        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| OLU Luminarius       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Rovis Skarun         | 1 | ((231122-sen-chroniacy-kochankow)) |
| Sebastian-194        | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Sia-03 Szernief      | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Szymon Alifajrin     | 1 | ((231122-sen-chroniacy-kochankow)) |