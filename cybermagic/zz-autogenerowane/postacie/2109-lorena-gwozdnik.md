---
categories: profile
factions: 
owner: public
title: Lorena Gwozdnik
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "rekin, gwozdnik, aurum"
* owner: "public"
* title: "Lorena Gwozdnik"


## Kim jest

### W kilku zdaniach

Pochodząca z bardzo militarnego i silnie współpracującego z Orbiterem rodu czarodziejka, wygnana na Prowincję by "zrobiła się dzielna" i "dorosła". Rozpaczliwie próbuje gdzieś się wpasować i gdzieś należeć. Nie potrafi się odnaleźć wśród Rekinów i się trochę boi wszystkiego. I nienawidzi faktu, że się tego wszystkiego boi. Poczciwe maleństwo, acz zachowuje się jak GROŹNA OSTRA LASKA.

### Co się rzuca w oczy

* Trochę szara myszka, ale bardzo próbuje pasować. Trochę za bardzo.

### Jak sterować postacią

* Jeśli ktoś zrobił jej krzywdę, nie mści się. Unika. Chowa się.
* Udaje dzielną i bojową, ale tak naprawdę to jest łagodna i unika konfliktów. Chętniej pomaga / koi niż krzywdzi.
* Łatwo daje się porwać grupie. Bardzo socjalna. Życie każdej imprezy.
* Zwykle zgłasza się na ochotniczkę, jeśli myśli, że może pomóc.
* Potrafi być waleczna, choć jak się ją naciśnie to się natychmiast wycofuje.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Gdy miała 14 lat, perfekcyjnie przyjęła niezapowiedzianą wizytę Sowińskich jako wiodąca arystokratka. Nie zrobiła ani jednego błędu w etykiecie i zachowała się jak młoda dama.
* Anonimowo wygrała konkurs na "strasznego potwora" dla młodych talentów w Domenie Gwozdników. Rodzice jej pogratulowali, ale spalili jej rysunki. Nie to jest jej przyszłością.
* Mimo ciągłego monitoringu, udało jej się złożyć niewielkią pracownię w Domenie Gwozdników. Do końca nikt nie doszedł do tego, że nie przestała rysować.
* Podczas jednej z operacji Justyniana do polowania na potwory z Trzęsawiska stanowiła ochronę ogniową jako snajper. Bardzo skutecznie.
* W Podwiercie nawiązała współpracę z Galerią Sztuki Makabra, oczywiście anonimowo. Pod pseudonimem "Kirasjer" dostarcza rysunku różnych niepokojących istot.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: Życie każdej imprezy. Potrafi łagodzić konflikty, świetnie się bawić i rozchmurzyć każdego. Genialnie tańczy.
* AKCJA: Zna etykietę, ma doskonałe poczucie stylu i jest perfekcyjną "damą". Tyle, że tego nie używa bo to oznaki słabości XD.
* AKCJA: Cat burglar. Świetnie się chowa. Bardzo szybko biega. Umie się włamywać.
* AKCJA: Bardzo spostrzegawcza i ma świetną pamięć. Praktycznie sokole oko. Dobrze wykrywa zagrożenia - gdzie się NIE pojawiać.
* AKCJA: Nieźle pilotuje ścigacz i świetnie strzela. Zwłaszcza snajperką. Jest bardzo celna, ale jej walka wręcz SSIE.
* AKCJA: Przepięknie rysuje. To jest to, co najbardziej lubi robić. Jej rysunki są bardzo realistyczne i bardzo plastyczne. Wybitna w _eldritch horror_ i aktach.
* COŚ: Dostęp do arsenału broni, do którego zdecydowanie nie powinna mieć dostępu. Jej ulubiona broń? Karabin snajperski.

### Serce i Wartości (3)

* Konformizm
    * Wierzy, że ważną i cenną rzeczą jest to, by tkanka społeczna działała i ludzie się lubili. Bardzo jej zależy, by to się działo.
    * Chce być częścią czegoś większego. Chce być zaakceptowana przez ród i przez jakąś grupę Rekinów. Chce być sprawnym elementem większej maszyny.
    * Pójdzie za tym, co chcą inni; trochę pasywna. Chyba, że to zagrozi jej reputacji, jej Rodowi lub ogólnie Tradycji.
* Twarz
    * Zależy jej na opinii innych na jej temat. Zależy jej, by przed sobą samą wyglądać na godną i sensowną. Nie chce być "trashy girl". A boi się, że jest.
    * Bardzo zależy jej, by dorównać obrazowi jej rodu. Silnie militarystyczny, bojowy ród ze zdolnymi wojownikami. Będzie dbała o swoją reputację.
    * Bardzo próbuje się popisać przed innymi, pokazać, jaka jest skuteczna i dobra.
    * Ma w sobie bardzo dużo godności i dumy - mimo wszystkiego. Nie pozwoli sobą pomiatać. Może jest myszką, ale myszka TEŻ ugryzie.
* Tradycja
    * WIE jakie powinny być Rekiny, WIE jak działają Rody Aurum - ale ona po prostu nie pasuje do tego obrazu. Próbuje jednak utrzymać istniejące systemy. Bo są dobre.
    * Jest święcie przekonana, że jej ród jest najlepszym rodem Aurum jaki jest. Specjaliści od miragentów i uzbrojenia.
    * Odrzuca publicznie swoje umiejętności związane ze sztuką. Póki jest wśród Rekinów, obniża status umiejętności związanych z byciem damą. GWOZDNIK MUSI WALCZYĆ.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "W moim rodzie wszyscy są świetni. Poza mną. Nigdy nie zaakceptują tego, że ja chcę być artystką. Że nie chcę walczyć."
* CORE LIE: "Tylko bycie wojowniczką i ostrą laską ma jakiekolwiek znaczenie. To jest dla mnie jedyna droga by coś osiągnąć. Wszystko inne jest godne pogardy i muszę ukrywać."
* Stosunkowo niski próg strachu - potrafi być ODWAŻNA, ale łatwo ją przestraszyć. Tzn. łatwo się poddaje woli innych.
* Bardzo skonfliktowana wewnętrznie. Z jednej strony musi być agresywna, z drugiej jest łagodniutka i spolegliwa.
* Fatalna w walce wręcz. FATALNA. To jej tak całkowicie nie wychodzi. I się boi walki w zwarciu.

### Magia (3M)

#### W czym jest świetna

* Mentalistka: potrafi wprowadzić drugą osobę w stan prawdziwego Piękna. Ekstatyczne delirium. Ukojenie, piękno, wrażenia, emocje.
* Mentalistka: mindbreak. Terror. Złamanie serca. Uczucia, które czasem czuje i nie chce ich pokazywać. Inkarnuje swoje rysunki ;-).
* Biomancja: potrafi przenieść "oczy", zmienić kanały wizualne (ciepło itp), wzmocnić wzrok itp. Dotyczy tylko zmysłu wzroku.

#### Jak się objawia utrata kontroli

* Atak w zmysły i ciało - tracimy wzrok, tracimy precyzję ruchów, jesteśmy zmęczeni itp.
* Niewłaściwe stany emocjonalne.
* Aportacja jakichś jej rysunków (zwykle wielu) lub referencyjnych zdjęć (zwłaszcza aktów, bo się ich najbardziej wstydzi).

### Specjalne

* .

## Inne

### Wygląd

* ?

### Coś Więcej

* .

### Endgame

* 


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201006-dezinhibitor-dla-sabiny      | Rekin; dramatycznie pragnie się wykazać przed Justynianem lub kimkolwiek. Wzięła na siebie rolę syntezatora dezinhibitora i udawała seksbota (ubranego) przed Myrczkiem. | 0110-10-07 - 0110-10-09 |
| 210406-potencjalnie-eksterytorialny-seksbot | 3 mc temu Marysia kazała jej zająć się Rupertem Mysiokornikiem, by ten nie szalał na imprezie. Zajęła się nim, sama się upiła, tańczyła na stole a potem skończyła w łóżku z Mysiokornikiem... wstydzi się. | 0111-04-27 - 0111-04-28 |
| 210518-porywaczka-miragentow        | chciała się popisać przed Gerwazym i wysłała miragenta przeciw Dianie (i go straciła). Próbując go odzyskać, wpakowała się w strzelaninę z Marysią Sowińską. Skończyła ranna i z mandatem. | 0111-05-07 - 0111-05-08 |
| 210831-serafina-staje-za-wydrami    | kręci się koło Daniela (dziewczyna?). Duża antypatia do Karoliny; dość zadziorna. Nie pierwszy raz włamuje się do ludzi - wie jak to robić i ma przygotowane odpowiednie czarne, ekranowane stroje. | 0111-07-07 - 0111-07-10 |
| 211102-satarail-pomaga-marysi       | zainfekowana przez Owada Sataraila zaatakowała ścigacz Karoliny wyrzutnią rakiet nie mając pełnej władzy mentalnej nad sobą. Karolina połamała jej nogi ścigaczem. | 0111-08-09 - 0111-08-10 |
| 211207-gdy-zabraknie-pradu-rekinom  | w gliździe medycznej, bo Marysia potrzebowała prądu z generatora Sensacjusza. | 0111-08-30 - 0111-08-31 |
| 211228-akt-o-ktorym-marysia-nie-wie | stworzyła piękny akt Marysi Sowińskiej mającej wszystko na głowie, który trafił do Napoleona Bankierza. Dowiedziała się, że Marysia o niej wie, więc oddała się pod ochronę Ernesta Namertela. | 0111-09-09 - 0111-09-12 |
| 220222-plaszcz-ochronny-mimozy      | podsłuchiwała rozmowę Mimozy - Marysi (Mimoza o tym nie wie, więc sprawiła, że Mimoza straciła twarz). Robi co może by wyprzeć się że to ona narysowała akt. | 0111-09-21 - 0111-09-25 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 201006-dezinhibitor-dla-sabiny      | po wstrzyknięciu sobie dezinhibitora chciała PRZEJĄĆ SENTISIEĆ. Ale po spotkaniu z Gabrielem dezinhibitor przekierował na niego jej oddanie. Pirotess do Ashrama. | 0110-10-09
| 210831-serafina-staje-za-wydrami    | zerwała z Danielem Terienakiem. Ma psychiczną siostrę. | 0111-07-10
| 210831-serafina-staje-za-wydrami    | personalnie wściekła na Karolinę Terienak. Ma z nią kosę za wszystko (obrażanie, traktowanie, zerwanie jej związku itp). | 0111-07-10
| 211102-satarail-pomaga-marysi       | ciężko połamana; Karolina wbiła się jej w nogi swoim ścigaczem gdy Lorena pod wpływem Owada ją zaatakowała wyrzutnią rakiet | 0111-08-10
| 211102-satarail-pomaga-marysi       | jej relacja z Karoliną Terienak przekroczyła point of no return. Lorena nienawidzi Karoliny (loathing, nie hate). Chce być jak najdalej od niej. | 0111-08-10
| 211207-gdy-zabraknie-pradu-rekinom  | święcie przekonana, że Marysia stoi po stronie Karoliny i nią gardzi / jej nienawidzi. Musi unikać Marysi. | 0111-08-31
| 211228-akt-o-ktorym-marysia-nie-wie | pod ochroną Ernesta Namertela by Marysia Sowińska nie zrobiła jej krzywdy (nie, żeby chciała). | 0111-09-12
| 211228-akt-o-ktorym-marysia-nie-wie | przyjaciółka Liliany Bankierz. Połączyło je upodobanie do piękna. | 0111-09-12
| 220222-plaszcz-ochronny-mimozy      | STARCIE z Ernestem Namertelem. Ona uważa go za siepacza z Eterni. Żąda wydania dwóch kultystek Sensacjuszowi. | 0111-09-25
| 220222-plaszcz-ochronny-mimozy      | okazuje się, że jest oficerem działań dywersyjnych zjednoczonych sił Aurum (co bardzo obniża opinię o tych siłach wszystkich co znają Lorenę) | 0111-09-25
| 220730-supersupertajny-plan-loreny  | Marsen wierzy, że ona jest  najlepszym strategiem i że Ernest x Marysia walczą przeciw niej. Nie wie jak się z tego wyplątać. | 0111-10-04

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 7 | ((210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow; 210831-serafina-staje-za-wydrami; 211102-satarail-pomaga-marysi; 211207-gdy-zabraknie-pradu-rekinom; 211228-akt-o-ktorym-marysia-nie-wie; 220222-plaszcz-ochronny-mimozy)) |
| Karolina Terienak    | 5 | ((210831-serafina-staje-za-wydrami; 211102-satarail-pomaga-marysi; 211207-gdy-zabraknie-pradu-rekinom; 211228-akt-o-ktorym-marysia-nie-wie; 220222-plaszcz-ochronny-mimozy)) |
| Daniel Terienak      | 3 | ((210831-serafina-staje-za-wydrami; 211207-gdy-zabraknie-pradu-rekinom; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Ernest Namertel      | 3 | ((211102-satarail-pomaga-marysi; 211228-akt-o-ktorym-marysia-nie-wie; 220222-plaszcz-ochronny-mimozy)) |
| Rafał Torszecki      | 3 | ((210831-serafina-staje-za-wydrami; 211102-satarail-pomaga-marysi; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Henryk Wkrąż         | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 211207-gdy-zabraknie-pradu-rekinom)) |
| Julia Kardolin       | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow)) |
| Keira Amarco d'Namertel | 2 | ((211228-akt-o-ktorym-marysia-nie-wie; 220222-plaszcz-ochronny-mimozy)) |
| Laura Tesinik        | 2 | ((201006-dezinhibitor-dla-sabiny; 210831-serafina-staje-za-wydrami)) |
| Napoleon Bankierz    | 2 | ((201006-dezinhibitor-dla-sabiny; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Sabina Kazitan       | 2 | ((201006-dezinhibitor-dla-sabiny; 210406-potencjalnie-eksterytorialny-seksbot)) |
| Sensacjusz Diakon    | 2 | ((211102-satarail-pomaga-marysi; 211207-gdy-zabraknie-pradu-rekinom)) |
| Triana Porzecznik    | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow)) |
| Aranea Diakon        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Arkadia Verlen       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Arnold Kłaczek       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Barnaba Burgacz      | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Diana Lemurczak      | 1 | ((210518-porywaczka-miragentow)) |
| Feliks Keksik        | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Franek Bulterier     | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Gabriel Ursus        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Halina Sermniek      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Hestia d'Rekiny      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Ignacy Myrczek       | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Justynian Diakon     | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Karolina Erenit      | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Liliana Bankierz     | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Marek Samszar        | 1 | ((211102-satarail-pomaga-marysi)) |
| Mimoza Elegancja Diakon | 1 | ((220222-plaszcz-ochronny-mimozy)) |
| Natalia Tessalon     | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Rafał Kumczek        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Rupert Mysiokornik   | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Serafina Ira         | 1 | ((210831-serafina-staje-za-wydrami)) |
| Urszula Arienik      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Wiktor Satarail      | 1 | ((211102-satarail-pomaga-marysi)) |