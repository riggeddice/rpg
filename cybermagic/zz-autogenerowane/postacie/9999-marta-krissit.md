---
categories: profile
factions: 
owner: public
title: Marta Krissit
---

# {{ page.title }}


# Generated: 



## Fiszki


* <-- WHISPERED | @ 230124-kjs-wygrac-za-wszelka-cene
* (ENCAO:  +---0 |Narcyz;;Nie ma blokad| VALS: Family, Stimulation, Achievement| DRIVE: Lepsza od siostry; przejąć kapitanat) | @ 230124-kjs-wygrac-za-wszelka-cene
* styl: aggresive, shouty | @ 230124-kjs-wygrac-za-wszelka-cene
* "Jestem lepsza od Ciebie i od każdego!" | @ 230124-kjs-wygrac-za-wszelka-cene

### Wątki


przeznaczenie-eleny-verlen

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230124-kjs-wygrac-za-wszelka-cene   | młodsza siostra, wzmocniła się amplifikatorem i pokonała starszą od siebie Serenę. W zamian za to oddała się Franciszkowi. | 0095-06-23 - 0095-06-25 |
| 230307-kjs-stymulanty-szeptomandry  | korzystała ze stymulantów, a następnie wyjawiła ich tajemnice Żanecie; podkreśliła, że by móc wygrać czasami trzeba dużo zapłacić. Też w TEN sposób. | 0095-06-26 - 0095-06-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Apollo Verlen        | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| KDN Kajis            | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Lidia Nemert         | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Lucas Oktromin       | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Żaneta Krawędź       | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Elena Verlen         | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Emilia Lawendowiec   | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Franciszek Korel     | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| Karol Atenuatia      | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Michał Waczarek      | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| Serena Krissit       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Wojciech Namczak     | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Zuzanna Kraczamin    | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |