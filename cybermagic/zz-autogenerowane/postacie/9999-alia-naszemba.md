---
categories: profile
factions: 
owner: public
title: Alia Naszemba
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190702-smieciornica-w-magitrowni    | wojowniczka i firebrand w Maczkowcu; siłą i dominacją przebija się przez nastolatków i dorosłuch by rozwiązać problem Patrycji używającej magii krwi. | 0109-07-14 - 0109-07-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Irek Waczar          | 1 | ((190702-smieciornica-w-magitrowni)) |
| Jan Waczar           | 1 | ((190702-smieciornica-w-magitrowni)) |
| Klara Orkaczyn       | 1 | ((190702-smieciornica-w-magitrowni)) |
| Patrycja Radniak     | 1 | ((190702-smieciornica-w-magitrowni)) |
| Przemysław Grumcz    | 1 | ((190702-smieciornica-w-magitrowni)) |