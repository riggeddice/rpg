---
categories: profile
factions: 
owner: public
title: Sandra Kantarelo
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221004-samotna-w-programie-advancer | advancer-medyk, pokonała (oszukując trucizną) Michała (szturmowca) w zapasach. Zbliżyła się do Eleny (po rozkazie Lary) i skutecznie ją zneutralizowała medykamentami gdy ta miała erupcję emocjonalną + magiczną. | 0099-05-20 - 0099-05-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Elena Verlen         | 1 | ((221004-samotna-w-programie-advancer)) |
| Hubert Mirsz         | 1 | ((221004-samotna-w-programie-advancer)) |
| Kacper Wentel        | 1 | ((221004-samotna-w-programie-advancer)) |
| Lara Kiriczko        | 1 | ((221004-samotna-w-programie-advancer)) |
| Michał Warkoczak     | 1 | ((221004-samotna-w-programie-advancer)) |
| OO Karsztarin        | 1 | ((221004-samotna-w-programie-advancer)) |
| Tymon Krakdacz       | 1 | ((221004-samotna-w-programie-advancer)) |