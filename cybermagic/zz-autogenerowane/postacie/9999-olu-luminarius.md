---
categories: profile
factions: 
owner: public
title: OLU Luminarius
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230923-ciemnosc-pozerajaca-arcadalian | jednostka Agencji Lux Umbrarum; szybka korweta fabrykacyjna i badawcza, 17 osób załogi. Tym razem - rozwiązuje problem statków dotkniętych Umbrą przez Zakon i magię. | 0091-07-30 - 0091-08-02 |
| 231202-protest-przed-kultem-na-szernief | dostęp do jego danych był kluczowym mechanizmem przekupienia Kalisty; ponadto pomógł w analizie danych związanych z ruchami Frederico i wzmacniał moc lokalnej TAI. | 0103-10-15 - 0103-10-19 |
| 231119-tajemnicze-tunele-sebirialis | główne repozytorium danych Agencji i taktyczna TAI która pomogła w hackowaniu danych Kalisty a przedtem Feliny. | 0104-11-20 - 0104-11-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kalista Surilik      | 2 | ((231119-tajemnicze-tunele-sebirialis; 231202-protest-przed-kultem-na-szernief)) |
| Aerina Cavalis       | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Aniela Kafantelas    | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Aulus Terrentus      | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Daven Hassik         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Eryk Kawanicz        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Felina Amatanir      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Frederico Zyklas     | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Ignatius Sozyliw     | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Iwo Bretonis         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Janvir Krassus       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Klasa Biurokrata     | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Klasa Dyplomata      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Klasa Hacker         | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Klasa Sabotażysta    | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Larkus Talvinir      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Leon Hurmniow        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Lester Martz         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Livia Sertiano       | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Marta Sarilit        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Salma Bluszcz        | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| SN Arcadalian        | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| SN Ferrivat          | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| SN Murisatia         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Vanessa d'Cavalis    | 1 | ((231202-protest-przed-kultem-na-szernief)) |