---
categories: profile
factions: 
owner: public
title: BIA Solitaria d'Zona Tres
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210721-pierwsza-bia-mag             | nieufna, skontaktowała się za plecami Arianny z noktianami i poznała prawdę o Inferni. Jednak pozwoliła Martynowi i Eustachemu się naprawić i wydzielić z siebie Klaudię. | 0111-05-17 - 0111-05-19 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210721-pierwsza-bia-mag             | Uzyskała umiejętności magiczne po integracji z Klaudią. Innymi słowy, Martyn zrobił z Bii maga. | 0111-05-19

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((210721-pierwsza-bia-mag)) |
| Eustachy Korkoran    | 1 | ((210721-pierwsza-bia-mag)) |
| Janus Krzak          | 1 | ((210721-pierwsza-bia-mag)) |
| Klaudia Stryk        | 1 | ((210721-pierwsza-bia-mag)) |
| Martyn Hiwasser      | 1 | ((210721-pierwsza-bia-mag)) |
| Romana Arnatin       | 1 | ((210721-pierwsza-bia-mag)) |