---
categories: profile
factions: 
owner: public
title: Patrycja Karzec
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181220-upiorny-servar               | dziennikarka z ogromnymi ambicjami nienawidząca Turnieju Nadziei. Uderzyła w turniej i posunęła karierę do przodu wspierając drużynę w odkryciu ducha. | 0109-10-28 - 0109-10-30 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181220-upiorny-servar               | kariera skoczyła do góry po wsparciu rodziny Daneb. Ma silniejszą pozycję i reputację niż kiedykolwiek. | 0109-10-30
| 181220-upiorny-servar               | wraz z Alicją Wielżak skupia się na rozwiązywaniu zagadek okultystycznych na terenie Przelotyka. Policjantka i reporter. | 0109-10-30

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksandra Garwen    | 1 | ((181220-upiorny-servar)) |
| Alicja Wielżak       | 1 | ((181220-upiorny-servar)) |
| Artur Śrubek         | 1 | ((181220-upiorny-servar)) |
| Beata Wielinek       | 1 | ((181220-upiorny-servar)) |
| Bogdan Daneb         | 1 | ((181220-upiorny-servar)) |
| Celina Szaczyr       | 1 | ((181220-upiorny-servar)) |
| Gabriel Krajczok     | 1 | ((181220-upiorny-servar)) |
| Jerzy Cieniż         | 1 | ((181220-upiorny-servar)) |
| Michał Wypras        | 1 | ((181220-upiorny-servar)) |
| Ryszard Januszewicz  | 1 | ((181220-upiorny-servar)) |
| Wojciech Tuczmowil   | 1 | ((181220-upiorny-servar)) |