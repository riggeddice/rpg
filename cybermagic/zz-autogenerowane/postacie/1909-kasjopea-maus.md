---
categories: profile
factions: 
owner: public
title: Kasjopea Maus
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "maus, luxuritias, cieniaszczyt"
* owner: "public"
* title: "Kasjopea Maus"


## Paradoks

Narkomanka intensywnych emocji, która kręci filmy zamiast działać osobiście.

## Motywacja

### O co walczy

* pokazanie takiego świata jakim jest, rozerwanie welonu - szokująca prawda dla każdego
* maksymalizacja doznań i uczuć dla siebie oraz innych
* być sławną - z filmów, doznań, skandali

### Przeciw czemu walczy

* nie zgadza się na wyzysk słabszych przez silniejszych - to z radością zniszczy
* obnaży wszelką hipokryzję, zwłaszcza taką uniemożliwiającą innym tego co Ty robisz
* nie stanie się potworem. Nie będzie krzywdzić innych. Jest reżyserem, nie psychopatą.

## Działania

### Specjalność

* doskonale rozpływa się w miastach, potrafi się schować w zakamarkach
* bezbłędnie udaje osobę z niższej klasy społecznej; w tej kwestii świetna aktorka
* świetnie włada nożami i nie zawaha się zranić ani posunąć dość daleko
* nieustraszona, niemożliwa do zgorszenia. Strasznie odporna na wszelkie elementy mentalne
* akrobatka i włamywaczka jakich mało. Też świetnie oszukuje w karty
* reżyser holokostek Mausów, zwłaszcza tych z niższych instynktów

### Słabość

* impulsywna i skłonna do ryzyka - rzuca się w najgorszy ogień i wpada w pułapki
* fascynacja cierpieniem i bólem - przyciąga ją to jak ćma ogień; może się "zawiesić"
* attention whore - nie jest zbyt lubiana i przyciąga uwagę w zły sposób
* fascynacja narkotykami i chemikaliami. Lubi sobie zażyć i być w odmiennym stanie świadomości
* skupia się na maksymalizacju intensywności swojej i innych; czasem idzie za daleko
* wyzywające tatuaże utrudniają jej ukrywanie się
* ZBYT odważna i brawurowa. Nie odmawia używek, nie unika ryzyka...

### Akcje

* wywiady, przepytuje innych
* wkrada się w różne miejsca i podgląda
* im bardziej gorszące czy intensywne, tym większa szansa że ona tam jest
* kocha ból, płacz, silne emocje... WSZYSTKO.
* testuje różne używki i czaruje pod ich wpływem

### Znaczące czyny



* uratowana przez Orbiter z niewoli w Eternii
* odmówiono jej transferu do Trzeciego Raju, ale zaakceptowano do Pustogoru; tu jest dużo noktian i ktoś musi mieć na nich oko
* oślepiona przez EMP, zdjęła przeciwników wbudowaną bronią
* regularnie pomaga jako wolontariuszka w Zamku Weteranów
* podczas Szturmu, użyła działa servara dzięki augmentacjom; połamało ją.
* rozbiła szajkę terminuskich przemytników w Pustogorze, po śladach do celu
* ze szczególną pasją niszczy wszystko czego dotknie Noktis lub Kajrat
* strachem, stymulantami i siłą ognia wymusiła sprawną ewakuację przy pożarze
* przekonała starego terminusa- weterana do eutanazji, gdyż był za drogi dla rodziny i dla Zamku. Wbrew życzeniom wszystkich.

## Mechanika

### Archetypy

cat burglar, intrepid reporter, daredevil, tancerz chaosu

### Motywacje

.

## Inne

### Wygląd

* Tatuaże. Wszędzie, łącznie z twarzą. Przedstawiają dziwne symbole i znaki. Przyciągają uwagę.
* Krótkie fioletowo-żółto-zielone włosy a la jeż.
* Ubrana w wyzywający strój.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190116-wypalenie-saitaera-z-trzesawiska | poszła jako dziennikarka uczuć na Trzęsawisko. Motywowała Hieronima. Niespecjalnie przeszkadzała Pięknotce, acz skończyła jako pocisk balistyczny niosący Karradraela. | 0110-01-07 - 0110-01-09 |
| 190120-nowa-minerwa-w-nowym-swiecie | psycholka która uważa, że Minerwa jest cudowna - żyje na krawędzi i ma chory umysł. Chce zrobić holokostki z wizji Minerwy. Pięknotka staje jej na drodze. | 0110-01-22 - 0110-01-26 |
| 190206-nie-da-sie-odrzucic-mocy     | wyczuła coś piekielnie interesującego w obszarze Cyberszkoły Zaczęstwa, ale Minerwa przekierowała jej uwagę na siebie. | 0110-02-17 - 0110-02-20 |
| 190210-minerwa-i-kwiaty-nadziei     | dziennikarka, która zrobiła na prośbę Pięknotki niewiarygodne świństwo Kornelowi - sprawiła, że jest persona non grata. Będzie mogła negocjować z Minerwą odnośnie Kryształu Pamięci. | 0110-02-21 - 0110-02-23 |
| 190419-osopokalipsa-wiktora         | dziennikarka, chciała sfilmować apokalipsę Sensoplex; osłoniła Pięknotkę biorąc na siebie źródło pierwotne i wyszła na mega bohaterkę w Sensopleksie. | 0110-03-29 - 0110-03-31 |
| 190906-wypadek-w-kramamczu          | wezwana jako rozpaczliwe wsparcie przez Pięknotkę; zaraz elegancko zabrała się do znalezienia taniego ale pewnego zlecenia dla Włóknina - dla weteranów terminuskich | 0110-06-18 - 0110-06-19 |
| 190912-rexpapier-i-wloknin          | ściągnęła detektywów by uratować Włóknin; poprosiła ją Pięknotka a jej ulubiona sukienka jest stąd. Będzie reżyserować reality show by pogodzić Annę i Dariusza. | 0110-06-21 - 0110-06-25 |
| 191126-smierc-aleksandrii           | szukała brudów na Lemurczaka w Kruczańcu. Znalazła Aleksandrię którą z pomocą Karradraela rozmontowała (tzn. on rozmontował). | 0110-10-10 - 0110-10-13 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 191126-smierc-aleksandrii           | uszkodzona przez wpływ Aleksandrii i Karradraela. Potężne osłony w jej umyśle są mniej potężne. | 0110-10-13

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 6 | ((190116-wypalenie-saitaera-z-trzesawiska; 190120-nowa-minerwa-w-nowym-swiecie; 190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei; 190419-osopokalipsa-wiktora; 190906-wypadek-w-kramamczu)) |
| Minerwa Metalia      | 3 | ((190120-nowa-minerwa-w-nowym-swiecie; 190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei)) |
| Alan Bartozol        | 2 | ((190116-wypalenie-saitaera-z-trzesawiska; 190419-osopokalipsa-wiktora)) |
| Dariusz Kuromin      | 2 | ((190906-wypadek-w-kramamczu; 190912-rexpapier-i-wloknin)) |
| Erwin Galilien       | 2 | ((190120-nowa-minerwa-w-nowym-swiecie; 190210-minerwa-i-kwiaty-nadziei)) |
| Kornel Garn          | 2 | ((190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei)) |
| Ksenia Kirallen      | 2 | ((190906-wypadek-w-kramamczu; 190912-rexpapier-i-wloknin)) |
| Tymon Grubosz        | 2 | ((190120-nowa-minerwa-w-nowym-swiecie; 190206-nie-da-sie-odrzucic-mocy)) |
| Wiktor Satarail      | 2 | ((190116-wypalenie-saitaera-z-trzesawiska; 190419-osopokalipsa-wiktora)) |
| Amelia Mirzant       | 1 | ((191126-smierc-aleksandrii)) |
| Anna Warlank         | 1 | ((190912-rexpapier-i-wloknin)) |
| Arnulf Poważny       | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| ASD Centurion        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Atena Sowińska       | 1 | ((190210-minerwa-i-kwiaty-nadziei)) |
| Eleonora Rdeść       | 1 | ((190912-rexpapier-i-wloknin)) |
| Hieronim Maus        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Jarek Gułanczak      | 1 | ((190912-rexpapier-i-wloknin)) |
| Kamil Lemurczak      | 1 | ((191126-smierc-aleksandrii)) |
| Karla Mrozik         | 1 | ((190120-nowa-minerwa-w-nowym-swiecie)) |
| Karolina Erenit      | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Karradrael           | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Mariusz Trzewń       | 1 | ((190906-wypadek-w-kramamczu)) |
| Marlena Maja Leszczyńska | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Mateusz Urszank      | 1 | ((190912-rexpapier-i-wloknin)) |
| Ola d'Amelia         | 1 | ((191126-smierc-aleksandrii)) |
| Saitaer              | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Sebastian Kuralsz    | 1 | ((191126-smierc-aleksandrii)) |
| Teresa Marszalnik    | 1 | ((191126-smierc-aleksandrii)) |