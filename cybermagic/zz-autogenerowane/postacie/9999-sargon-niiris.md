---
categories: profile
factions: 
owner: public
title: Sargon Niiris
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221221-astralna-flara-i-nowy-komodor | młody i niedoświadczony dowódca grupy młodzików - uciekinierów z Nonariona i okolic. Zajęli Kazmirian na nieobecność Orbitera mając ogromne szczęście (bo Semla nie była w pełni sprawna). Unieszkodliwiony przez Semlę, trafił pod opiekę Orbitera / Bolzy ze swymi ludźmi. Do Strefy Duchów. | 0100-11-07 - 0100-11-10 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Daria Czarnewik      | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Elena Verlen         | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Ellarina Samarintael | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Kajetan Kircznik     | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Kaspian Certisarius  | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Leszek Kurzmin       | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Maja Samszar         | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| NekroTAI Zarralea    | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| OO Astralna Flara    | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| OO Athamarein        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| OO Loricatus         | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Salazar Bolza        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Władawiec Diakon     | 1 | ((221221-astralna-flara-i-nowy-komodor)) |