---
categories: profile
factions: 
owner: public
title: Mikołaj Larnecjat
---

# {{ page.title }}


# Generated: 



## Fiszki


* dowódca grupy wydzielonej sprzątającej Przelotyk Północny; stracił bliskich podczas wojny | @ 230813-jedna-tienka-przybywa-na-pomoc
* OCEAN: (E-O-): zamknięty w sobie, rzetelny, spokojny i dobry w powtarzalnych operacjach. "Nie powinienem był przetrwać wojny. Jeszcze Orbiterowi jednak się przydam." | @ 230813-jedna-tienka-przybywa-na-pomoc
* VALS: (Conformism(Duty), Humility): jak coś ma zrobić, zrobi i dostarczy. "Nieważne czy mi się podoba, czy nie. Jestem elementem maszyny Orbitera." | @ 230813-jedna-tienka-przybywa-na-pomoc
* Core Wound - Lie: "Wszystko, co kochałem zginęło i zostało zniszczone. Jestem sam." - "Nie przywiązuj się. Jest tylko misja." | @ 230813-jedna-tienka-przybywa-na-pomoc
* Styl: Spokojny, absolutnie nie inspirujący i cichy miłośnik muzyki klasycznej pochłonięty swoimi myślami, ale wykonujący każde polecenie rzetelnie i sensownie. | @ 230813-jedna-tienka-przybywa-na-pomoc
* metakultura: Atarien: "dowódca każe, ja wykonuję. Na tym polega hierarchia. Tak się buduje coś większego niż my sami." | @ 230813-jedna-tienka-przybywa-na-pomoc

### Wątki


salvagerzy-lohalian

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230813-jedna-tienka-przybywa-na-pomoc | komendant Salvagerów Lohalian, nieco nieufny wobec tienów i bardzo pro-Orbiter; nieco zrezygnowany i melancholijny, ale robi robotę wystarczająco dobrze. | 0095-09-08 - 0095-09-13 |
| 230905-druga-tienka-przybywa-na-pomoc | major, którego ucieszyło proste podejście Eleny do tematu - ma zapał, nie zna się, ale można dać Elenie szansę. | 0095-10-05 - 0095-10-07 |
| 231024-rozbierany-poker-do-zwalczania-interis | zrezygnowany kapitan, który jednak potraktował Interis poważnie, wysępił od Eleny wsparcie żywnościowe od Samszarów i odpowiednio ją pochwalił w nagrodę. Oboje zyskali. Nadal dostarcza zero morale, ale jest udanym administratorem. | 0095-10-08 - 0095-10-10 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adelajda Kalmiris    | 2 | ((230905-druga-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Cyprian Mirisztalnik | 2 | ((230905-druga-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Elena Samszar        | 2 | ((230905-druga-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Estella Gwozdnik     | 2 | ((230813-jedna-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Tomasz Afagrel       | 2 | ((230813-jedna-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Antoni Paklinos      | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |
| Jaromir Gaburon      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Jarosław Mirelski    | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Maurycy Derwisz      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Paweł Lawarczak      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Rachela Brześniak    | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |
| Serafin Gwozdnik     | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |
| Zenobia Samszar      | 1 | ((230905-druga-tienka-przybywa-na-pomoc)) |