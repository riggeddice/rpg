---
categories: profile
factions: 
owner: public
title: Elwira Barknis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240214-relikwia-z-androida          | dorobiła się pozycji na plagiacie i agresywnych działaniach marketingowych i zarządczych; gdy Alina to odkryła, Elwira ją zabiła a potem usunęła ślady. Przyniosła dużo wartościowych ludzi na stację i podnosi jej reputację. | 0107-05-16 - 0107-05-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aerina Cavalis       | 1 | ((240214-relikwia-z-androida)) |
| Alina Mekran         | 1 | ((240214-relikwia-z-androida)) |
| Kalista Surilik      | 1 | ((240214-relikwia-z-androida)) |
| Klasa Dyplomata      | 1 | ((240214-relikwia-z-androida)) |
| Klasa Hacker         | 1 | ((240214-relikwia-z-androida)) |
| Klasa Inżynier       | 1 | ((240214-relikwia-z-androida)) |
| Klasa Oficer Naukowy | 1 | ((240214-relikwia-z-androida)) |
| Klasa Sabotażysta    | 1 | ((240214-relikwia-z-androida)) |
| Mawir Hong           | 1 | ((240214-relikwia-z-androida)) |
| Tadeusz Mekran       | 1 | ((240214-relikwia-z-androida)) |
| Vanessa d'Cavalis    | 1 | ((240214-relikwia-z-androida)) |