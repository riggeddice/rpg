---
categories: profile
factions: 
owner: public
title: Tadeusz Rupczak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190106-a-moze-pustogorska-mafia     | wysoki rangą terminus Pustogoru prowadzący śledztwo w sprawie Kasjana i Pięknotki. Z powodzeniem. Bezwzględny, trzyma się litery prawa. | 0109-12-24 - 0109-12-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adela Kirys          | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Amadeusz Sowiński    | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Kasjan Czerwoczłek   | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Pięknotka Diakon     | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Roland Grzymość      | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Waldemar Mózg        | 1 | ((190106-a-moze-pustogorska-mafia)) |