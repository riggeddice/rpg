---
categories: profile
factions: 
owner: public
title: Wojciech Kaznodzieja
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220329-mlodociani-i-pirat-na-krolowej | pseudo "profesor"; próbował ukrywać manifest piratów ale pokonała go Miranda i Seweryn. Ostatecznie pomógł rozmontować miny i przyłączył się do załogi Królowej Przygód. Zażądał wolności dla Heleny. | 0108-08-29 - 0108-09-09 |
| 220405-lepsza-kariera-dla-romki     | chroni Helenę za wszelką cenę; wszedł w starcie z Anną, ale skończył na kolanach skopany (acz ją też skrzywdził). Zajmie się Romką, by ta dostała sprawny zawód technika. | 0108-09-15 - 0108-09-17 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220405-lepsza-kariera-dla-romki     | ma pewne podejrzenia co do TAI "Ceres" (czyli Mirandy). Coś wygodnie to działa. | 0108-09-17

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anna Szrakt          | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Helena Banbadan      | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Miranda Ceres        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Romana Kundel        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| SC Królowa Przygód   | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Antos Kuramin        | 1 | ((220405-lepsza-kariera-dla-romki)) |
| Bartek Wudrak        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Berdysz Rozdzieracz  | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Damian Szczugor      | 1 | ((220405-lepsza-kariera-dla-romki)) |
| Gotard Kicjusz       | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Kara Prazdnik        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Łucjan Torwold       | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Prokop Umarkon       | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Seweryn Grzęźlik     | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Ursyn Uszat          | 1 | ((220405-lepsza-kariera-dla-romki)) |