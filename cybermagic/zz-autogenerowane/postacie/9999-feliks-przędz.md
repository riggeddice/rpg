---
categories: profile
factions: 
owner: public
title: Feliks Przędz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210108-ratunkowa-misja-goldariona   | właściciel firmy na K1, ArcheoPrzędz; ledwo trzyma firmę na nogach. Współpracuje z Zespołem, by spełnić kontrakt ratunkowy - uratować piratów. | 0111-08-09 - 0111-08-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Permin          | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Aleksander Leszert   | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Elena Verlen         | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Kamil Frederico      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Klaudia Stryk        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Martyn Hiwasser      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Oliwia Pietrova      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| SCA Goldarion        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Semla d'Goldarion    | 1 | ((210108-ratunkowa-misja-goldariona)) |
| SL Uśmiechnięta      | 1 | ((210108-ratunkowa-misja-goldariona)) |