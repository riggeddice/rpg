---
categories: profile
factions: 
owner: public
title: Lena Fenatil
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210810-porwanie-na-gwiezdnym-motylu | Strażniczka. Odnalazła lokalizację Antonelli, świetnie trianguluje pola prywatności i odkryła gdzie znajduje się miragent. Oczy Zespołu. | 0108-12-25 - 0108-12-30 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antonella Temaris    | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Flawia Blakenbauer   | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Franek Kuparał       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Jolanta Sowińska     | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Nataniel Morlan      | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Renata Szarżun       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| SLX Gwiezdny Motyl   | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Tomasz Sowiński      | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |