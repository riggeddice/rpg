---
categories: profile
factions: 
owner: public
title: Zuzanna Szagbin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240306-potwornosc-powojenna-trzykwiatu | dewastator i infiltrator; zaprzyjaźniona z Petrą, dowiedziała się że Latisza jest mniej popularna niż się wydawało i poznała sekret Zofii - narkotyki dla Latiszy. Opracowała ewakuację i wybrała 10 osób z którymi należy udać się do Pustogoru. | 0084-04-16 - 0084-04-21 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 240306-potwornosc-powojenna-trzykwiatu | uznana za zdrajcę Trzykwiata, wyniosła się do innego miejsca niż Trzykwiat wspierana przez Pustogor. | 0084-04-21

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Jakub Kurbeczko      | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Ksenia Byczajnik     | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Latisza Warkolnicz   | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Natalia Pszaruk      | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Natan Wierzbitowiec  | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Petra Łomniczajew    | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Radosław Zientarmik  | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Ulrich Warkolnicz    | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Zofia Skorupniczek   | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |