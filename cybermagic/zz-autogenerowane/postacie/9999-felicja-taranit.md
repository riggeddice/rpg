---
categories: profile
factions: 
owner: public
title: Felicja Taranit
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191218-kijara-corka-szotaron        | inspirator Dorszant; rozpuściła pogłoski o tym, że wszystko jest pod kontrolą i przenegocjowała pokój z Eszarą. | 0110-07-04 - 0110-07-07 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Azonia Arris         | 1 | ((191218-kijara-corka-szotaron)) |
| Eszara d'Dorszant    | 1 | ((191218-kijara-corka-szotaron)) |
| Filip Szczatken      | 1 | ((191218-kijara-corka-szotaron)) |
| Jaromir Uczkram      | 1 | ((191218-kijara-corka-szotaron)) |
| Kijara d'Esuriit     | 1 | ((191218-kijara-corka-szotaron)) |
| Oliwier Pszteng      | 1 | ((191218-kijara-corka-szotaron)) |
| Rafał Kirlat         | 1 | ((191218-kijara-corka-szotaron)) |
| Szymon Szelmer       | 1 | ((191218-kijara-corka-szotaron)) |