---
categories: profile
factions: 
owner: public
title: Rafał Kidiron
---

# {{ page.title }}


# Generated: 



## Fiszki


* (ENCAO: 00+-0 |precyzyjny i zaplanowany;; kłóci się by wygrać| Achievement, Power, Face | DRIVE: Duma i monument) | @ 230201-wylaczone-generatory-memoriam-inferni
* security lord i nieformalny dyktator arkologii | @ 230201-wylaczone-generatory-memoriam-inferni
* "Nasza arkologia zaszła tak daleko i nie możemy pozwolić, by cokolwiek stanęło na drodzę jej absolutnej wielkości." | @ 230201-wylaczone-generatory-memoriam-inferni
* (ENCAO: 00+-0 |precyzyjny i zaplanowany;; DIMIR (UB);; Żywy wąż nie człowiek | Achievement, Power, Face | DRIVE: Duma i monument) | @ 230208-pierwsza-randka-eustachego
* "Tylko ci, którzy są przydatni mają miejsce w Nativis" | @ 230208-pierwsza-randka-eustachego

### Wątki


infernia-jej-imieniem
kidiron-zbawca-nativis

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230104-to-co-zostalo-po-burzy       | zgodził się, by Eustachy wziął skorpiona (Seiren) i pojechał ratować Annę. | 0092-10-26 - 0092-10-28 |
| 220712-pasozytnicze-osy-w-nativis   | bezwzględny szef ochrony Nativis należący do kasty rządzącej i kuzyn Lycoris; dobrze zarządza Nativis słuchając rad i zostawiając ekspertom działania. Opanował Pasożyta z pomocą miragenta Lycoris i pozyskał próbki Pasożyta jako broń biologiczną. | 0092-10-29 - 0092-11-07 |
| 220720-infernia-taksowka-dla-lycoris | wysłał Lycoris do CES Purdont; zniknęła niedaleko Wiertła Ekopoezy Delta. W panice wezwał Infernię ale nic im nie powiedział. Pomógł Zespołowi przesyłając kody z arkologii by wstrzymywać Plagę. | 0093-01-20 - 0093-01-22 |
| 230201-wylaczone-generatory-memoriam-inferni | gdy przełożeni arkologii Sarviel się do niego zgłosili z prośbą o pomoc w pozbyciu się noktian z Coruscatis, doprowadził do skażenia eksportowanej żywności by pomóc Trianai w zniszczeniu Coruscatis. Na prośbę Sarviel poprosił Korkoranów o aresztowanie Kallisty Luminis. Zrobi KAŻDĄ potworność, by wzmocnić Neikatis. | 0093-02-10 - 0093-02-12 |
| 230208-pierwsza-randka-eustachego   | broni Eustachego i jego decyzji przed wujkiem - jego zdaniem Eustachy zrobił co należy. Gdy Eustachy powiedział, że nie chce iść na randkę z Kalią, Kidiron mu powiedział - robimy co robimy dla arkologii. Nie to, co chcemy robić. | 0093-02-14 - 0093-02-21 |
| 230215-terrorystka-w-ambasadorce    | wierzył, że to fundamentalnie nie ma znaczenia, że wyszły na jaw jego mroczne ruchy w arkologii Lirvint. I faktycznie, nie miało w samej arkologii, ale pojawił się ruch oporu. Został oskarżany przez Misterię o uszkodzenie arkologii Lirvint i porwanie wił. Ostatecznie zgodził się na współpracę, pod warunkiem, że Misteria nie będzie działać przeciwko niemu i arkologii. Umieścił tracker w Misterii, aby monitorować jej ruchy. Zimny jak zawsze, korzystał z okazji by mocniej pokazać Eustachemu że takie działania są potrzebne dla tej arkologii. | 0093-02-22 - 0093-02-23 |
| 230315-bardzo-nieudane-porwanie-inferni | zaproponował przesłuchanie pojmanych piratów, obiecując im immunitet w zamian za współpracę, co pozwoliło mu zdobyć cenne informacje. Pokazał Ardilli, że piraci zgodzą się na przesłuchania neuroobrożą i podkreślał "kapitana Eustachego Korkorana". | 0093-03-06 - 0093-03-09 |
| 230329-zdrada-rozrywajaca-arkologie | dowiedział się, że są problemy z maszynami medycznymi obsługującymi Bartłomieja Korkorana. OCZYWIŚCIE że dał uprawnienia Inferni by zajmowali się wujkiem. Podejrzewa spisek, że ktoś chce skłócając rodziny zniszczyć arkologię. Wszystkie te tematy - dysputy rodzinne, _petty shit_ są zdecydowanie poniżej jego godności i zainteresowania. Typowe "I've got morons on my team..." | 0093-03-14 - 0093-03-16 |
| 230614-atak-na-kidirona             | miał szczęście - zajmował się swoimi planami poza spotkaniem głównym i nie było go gdy Infiltrator próbował go zabić. Nie on a miragent zginął (wraz z całym sztabem R.K.). Bardzo ciężko ranny w swoim pomniejszym schronieniu. Uratowany przez... Ardillę. Jest TWARDY DOWÓD, że bez niego arkologia nie przetrwa. | 0093-03-22 - 0093-03-24 |
| 230621-infiltrator-ucieka-a-arkologia-plonie | nieprzytomny; Ardilla wynegocjowała dlań w Szczurowisku bezpieczne miejsce. | 0093-03-25 - 0093-03-26 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230215-terrorystka-w-ambasadorce    | ujawnione zostało, że robił złe rzeczy w arkologii Lirvint. Pojawiła się silna opozycja wewnętrzna i ruch oporu. Im lepiej w arkologii i im szczęśliwsi są ludzie, im mniej się boją (a Kidiron próbuje by tak było) tym większy opór przeciw niemu. | 0093-02-23
| 230215-terrorystka-w-ambasadorce    | jedyna osoba na której mu naprawdę zależy i którą naprawdę lubi - Kalia Awiter - została ujawniona jako jego "słaby punkt". | 0093-02-23
| 230315-bardzo-nieudane-porwanie-inferni | pozyskał informacje o piratach na Neikatis dzięki śmiałej akcji Eustachego gdy próbowano Infernię porwać. | 0093-03-09
| 230614-atak-na-kidirona             | miał szczęście i nie zginął w zamachu. Został jednak bardzo ciężko ranny. Co najmniej 2 tygodnie ciężkiego szpitalnego leczenia. | 0093-03-24

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eustachy Korkoran    | 9 | ((220720-infernia-taksowka-dla-lycoris; 230104-to-co-zostalo-po-burzy; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Ardilla Korkoran     | 8 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Kalia Awiter         | 7 | ((230104-to-co-zostalo-po-burzy; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| OO Infernia          | 7 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Ralf Tapszecz        | 7 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Bartłomiej Korkoran  | 6 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Celina Lertys        | 3 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Lycoris Kidiron      | 3 | ((220712-pasozytnicze-osy-w-nativis; 220720-infernia-taksowka-dla-lycoris; 230614-atak-na-kidirona)) |
| BIA Prometeus        | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Dalmjer Servart      | 2 | ((230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Franciszek Pietraszczyk | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| Jan Lertys           | 2 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni)) |
| Małgorzata Maratelus | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| SAN Szare Ostrze     | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230614-atak-na-kidirona)) |
| Tymon Korkoran       | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce)) |
| Wojciech Grzebawron  | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Aniela Myszawcowa    | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Anna Seiren          | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Antoni Grzypf        | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Cyprian Kugrak       | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Czesław Żuczek       | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Damian Marlinczak    | 1 | ((220712-pasozytnicze-osy-w-nativis)) |
| Daria Raizis         | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Hubert Grzebawron    | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| JAN Seiren           | 1 | ((230104-to-co-zostalo-po-burzy)) |
| JAN Uśmiech Kamili   | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Jarlow Gurdacz       | 1 | ((220712-pasozytnicze-osy-w-nativis)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kamil Wraczok        | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Laurencjusz Kidiron  | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Marcin Pietraszczyk  | 1 | ((230208-pierwsza-randka-eustachego)) |
| Mariusz Dobrowąs     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Michał Uszwon        | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Nadia Sekernik       | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Rufus Seiren         | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Serentina d'Remora   | 1 | ((220712-pasozytnicze-osy-w-nativis)) |
| Stanisław Uczantor   | 1 | ((230614-atak-na-kidirona)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Tobiasz Lobrak       | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Wiktor Turkalis      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Zofia d'Seiren       | 1 | ((230104-to-co-zostalo-po-burzy)) |