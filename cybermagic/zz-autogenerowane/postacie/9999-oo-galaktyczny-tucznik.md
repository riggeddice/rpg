---
categories: profile
factions: 
owner: public
title: OO Galaktyczny Tucznik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200909-arystokratka-w-ladowni-na-swinie | superpancerny, mało zwrotny transportowiec glukszwajnów, bardzo trudny do uszkodzenia. Został uszkodzony przy pilotażu Arianny pod ostrzałem nojrepów i przez superParadoks Anastazji. | 0111-02-12 - 0111-02-17 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 200909-arystokratka-w-ladowni-na-swinie | potrzaskany i wymaga naprawy. Tarcze, właz do ładowni, jedno lekkie działko (z czterech) zniszczone, uszkodzone ekrany antymagiczne w ładowni, kadłub powgniatany. Ale solidny i dalej lata. | 0111-02-17

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazja Sowińska   | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| Arianna Verlen       | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| Elena Verlen         | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| Eustachy Korkoran    | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| Izabela Zarantel     | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| Kamil Lyraczek       | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| Klaudia Stryk        | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| Martyn Hiwasser      | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |