---
categories: profile
factions: 
owner: public
title: Wojciech Zajcew
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180701-dwa-rejsy-w-potrzebie        | katalista. Pomocny i przyjacielski, acz agresywny z natury. Dla uratowania załogi nie przekazał informacji o Lewiatanie ratującym ich magom. Pomaga w trudnych i ryzykownych pracach - przenoszenie cargo w Eterze, napromieniowanie szalupy... | 0109-08-07 - 0109-08-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Emilia Kariamon      | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Fabian Komczatkow    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Gerard Weiner        | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Kacper Wontarczyk    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Kalina Rotmistrz     | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Krystian Moborok     | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Liwia Barbaturran    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Marcjanna Maszotka   | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Martauron Attylla    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Sabrina Powsimrożek  | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Tymon Barbaturran    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |