---
categories: profile
factions: 
owner: public
title: Emil Samszar
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (O+N+): Inteligentny i ma ogromne zainteresowania, martwi się o potencjalne problemy. "Muszę zrozumieć i przygotować wszystko, zanim podejmę decyzję." | @ 230715-amanda-konsoliduje-zloty-cien
* VALS: (Self-Direction, Universalism): Ceni prawdę i odkrycia ponad wszystko inne. "Nauka, rozwój oraz Aurum to klucz do poprawy świata." | @ 230715-amanda-konsoliduje-zloty-cien
* Core Wound - Lie: "Nazwali mnie ekstremistą i odebrali mi możliwości badań" - "Udowodnię, że moje badania mogą zmienić świat, nawet jeśli muszę to zrobić sam." | @ 230715-amanda-konsoliduje-zloty-cien
* Styl: Lekko zagubiony i niespokojny, ale świetny w tym co robi; zawsze głodny wiedzy. "Na przekór, nauka musi iść naprzód. Pojmiemy niewyobrażalne, choćby miał to być nasz ostatni oddech." | @ 230715-amanda-konsoliduje-zloty-cien

### Wątki


pronoktianska-mafia-kajrata

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230715-amanda-konsoliduje-zloty-cien | 41 lat, już zaplątany w Złoty Cień, który dostarczał mu badań itp; rywalizuje z Bellą o elementy Secesjonistów i Oteriiel. | 0095-09-09 - 0095-09-12 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Bella Samszar        | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Kasimir Esilin       | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Rufus Bilgemener     | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Tadeusz Samszar      | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Wacław Samszar       | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |