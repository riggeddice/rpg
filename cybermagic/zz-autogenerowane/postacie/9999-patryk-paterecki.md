---
categories: profile
factions: 
owner: public
title: Patryk Paterecki
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180808-kultystka-z-milosci          | kucharz pracujący w burgerowni, Pionek Ośmiornicy. Nie uważał, że robi coś złego i chętnie współpracował z Kaliną. Zakłócone poczucie dobra i zła. | 0109-08-30 - 0109-09-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Zajcew        | 1 | ((180808-kultystka-z-milosci)) |
| Feliks Weiner        | 1 | ((180808-kultystka-z-milosci)) |
| Kacper Pyszałnik     | 1 | ((180808-kultystka-z-milosci)) |
| Kalina Rotmistrz     | 1 | ((180808-kultystka-z-milosci)) |
| Kasandra Kirnał      | 1 | ((180808-kultystka-z-milosci)) |
| Maksymilian Supolont | 1 | ((180808-kultystka-z-milosci)) |
| Małgorzata Kirnał    | 1 | ((180808-kultystka-z-milosci)) |
| Stach Sosnowiecki    | 1 | ((180808-kultystka-z-milosci)) |