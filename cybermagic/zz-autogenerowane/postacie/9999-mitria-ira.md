---
categories: profile
factions: 
owner: public
title: Mitria Ira
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230627-ratuj-mlodziez-dla-kajrata   | noktianka, 18 lat, _conduit_ do wiedzy Talii Aegis i psychotroniczka; gdy Mitria jest torturowana, wiedza Talii jest przekazywana Mitrii i przez nią - hiperpsychotronikom. Uratowana przez Zespół. | 0095-08-20 - 0095-08-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Amanda Kajrat        | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Elena Samszar        | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Ernest Kajrat        | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Herbert Samszar      | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Impresja Ignicja Incydencja Diakon | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Karolinus Samszar    | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Talia Aegis          | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Wacław Samszar       | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Wargun Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |