---
categories: profile
factions: 
owner: public
title: Wiktor Turkalis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220720-infernia-taksowka-dla-lycoris | szef CES Purdont; prostacki i krzykliwy, ale robi co powinien. Pod namową Bartłomieja Korkorana nie tępi młodych rozrabiaków. Wraz z Lycoris udał się do wiertła ekopoezy i oddał bazę Kamilowi. Nie wiadomo co się z nim dzieje. | 0093-01-20 - 0093-01-22 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220720-infernia-taksowka-dla-lycoris | w przeszłości znał się z Bartłomiejem Korkoranem. Lubią się. | 0093-01-22

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Bartłomiej Korkoran  | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Celina Lertys        | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Czesław Żuczek       | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Daria Raizis         | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Eustachy Korkoran    | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Jan Lertys           | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Kamil Wraczok        | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Lycoris Kidiron      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| OO Infernia          | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Rafał Kidiron        | 1 | ((220720-infernia-taksowka-dla-lycoris)) |