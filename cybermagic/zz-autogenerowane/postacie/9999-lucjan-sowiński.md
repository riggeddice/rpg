---
categories: profile
factions: 
owner: public
title: Lucjan Sowiński
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210921-przybycie-rekina-z-eterni    | kuzyn Marysi który ją lubi. Dla niej poszperał w Aurum na temat misji Amelii i jakkolwiek naraził się na gniew samej Amelii, ale odkrył problem na linii Amelia - Jolanta. Uważany za niezdarnego politycznie. | 0111-07-20 - 0111-07-25 |
| 211012-torszecki-pokazal-kregoslup  | spokesman Dworu Sowińskich, przekazał Marysi nowe zadanie - zdobyć krew "córki Morlana". Nie wie jak to ruszyć. | 0111-07-29 - 0111-07-30 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Sowińska      | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Jolanta Sowińska     | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Karolina Terienak    | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Marysia Sowińska     | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Rafał Torszecki      | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Arkadia Verlen       | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Ernest Namertel      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Karol Pustak         | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Nataniel Morlan      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Sensacjusz Diakon    | 1 | ((211012-torszecki-pokazal-kregoslup)) |
| Tomasz Tukan         | 1 | ((210921-przybycie-rekina-z-eterni)) |