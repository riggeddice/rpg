---
categories: profile
factions: 
owner: public
title: Iskander Matorin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230521-rozszczepiona-persefona-na-itorwienie | noktiański advancer Itorwien; nic szczególnego. Jak tylko pojawiły się problemy, zasealował się w scutierze i słuchał instrukcji rozszczepionej Persefony. Potem współpracował z Serbiniusem. Ogólnie - robił co powinien. | 0109-09-15 - 0109-09-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Emilia Ibris         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Fabian Korneliusz    | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Helmut Szczypacz     | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Karol Brinik         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Klaudia Stryk        | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Martyn Hiwasser      | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Mojra Karstall       | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| OO Itorwien          | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| OO Serbinius         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Tadeusz Arkaladis    | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |