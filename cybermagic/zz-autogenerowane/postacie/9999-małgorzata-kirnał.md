---
categories: profile
factions: 
owner: public
title: Małgorzata Kirnał
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180730-prasyrena-z-zemsty           | przyprowadzona do narkotyzowania przez siostrę obudziła w sobie Prasyrenę. Polowała na Maksymiliana; odgoniona i Przeklęta przez Kalinę. | 0109-08-29 - 0109-08-30 |
| 180808-kultystka-z-milosci          | w większości ofiara tej sesji. Słaba, zniewolona przez Kacpra - ale po poratowaniu przez Feliksa wróci do normy. | 0109-08-30 - 0109-09-02 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180730-prasyrena-z-zemsty           | dostała Klątwę Błyskawic. Okresowo wali w nią potężne wyładowanie, Raniąc. Musi unikać otwartych powierzchni i gniazdek elektrycznych. | 0109-08-30
| 180730-prasyrena-z-zemsty           | ma absolutny TERROR w kierunku na Kalinę. Boi się jej bardziej niż czegokolwiek. Kalina śni jej w nocy. | 0109-08-30
| 180808-kultystka-z-milosci          | zdjęto z niej Klątwę Błyskawic; jest w stanie w miarę normalnie żyć w społeczeństwie z pomocą siostry Kasandry. | 0109-09-02

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Zajcew        | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Kalina Rotmistrz     | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Kasandra Kirnał      | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Maksymilian Supolont | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Stach Sosnowiecki    | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Anita Perczoluk      | 1 | ((180730-prasyrena-z-zemsty)) |
| Feliks Weiner        | 1 | ((180808-kultystka-z-milosci)) |
| Kacper Pyszałnik     | 1 | ((180808-kultystka-z-milosci)) |
| Lawenda Weiner       | 1 | ((180730-prasyrena-z-zemsty)) |
| Patryk Paterecki     | 1 | ((180808-kultystka-z-milosci)) |