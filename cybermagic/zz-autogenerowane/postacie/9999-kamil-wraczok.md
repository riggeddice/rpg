---
categories: profile
factions: 
owner: public
title: Kamil Wraczok
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220720-infernia-taksowka-dla-lycoris | biurokrata i p.o. dowódcy CES Purdont z kompleksem KAPITANA MARINES. Złapał Eustachego kilka lat temu jak ten się wkradał do laboratorium, ale okazał litość. Aktualnie - zarażony przez Plagę Trianai; doprowadził do tego że CES Purdont straciła jakiekolwiek szanse odparcia Plagi. Pobity przez Janka, nieprzytomny. | 0093-01-20 - 0093-01-22 |
| 220817-osy-w-ces-purdont            | Drolmot Trianai; Przebudzony przez magię Eustachego, koordynuje Trianai do walki z komandosami i rozbudowy rdzenia. KIA. | 0093-01-23 - 0093-01-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 2 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont)) |
| Celina Lertys        | 2 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont)) |
| Eustachy Korkoran    | 2 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont)) |
| Jan Lertys           | 2 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont)) |
| Bartłomiej Korkoran  | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Czesław Żuczek       | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Daria Raizis         | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Joachim Puriur       | 1 | ((220817-osy-w-ces-purdont)) |
| Kordian Olgator      | 1 | ((220817-osy-w-ces-purdont)) |
| Lycoris Kidiron      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| OO Infernia          | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Rafał Kidiron        | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| VN Karglondel        | 1 | ((220817-osy-w-ces-purdont)) |
| Wiktor Turkalis      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |