---
categories: profile
factions: 
owner: public
title: Konrad Wączak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200623-adaptacja-azalii             | brat Rozalii (której Cieniem jest Azalia d'Alkaris), agent NeoMil, frakcji Orbitera. Dowodził nanoswarmową bazą na Trzęsawisku. Uratowany przez Pięknotkę. Przekonał siostrę, że jednak da się coś z tym zrobić. | 0110-09-26 - 0110-10-04 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Azalia d'Alkaris     | 1 | ((200623-adaptacja-azalii)) |
| Karla Mrozik         | 1 | ((200623-adaptacja-azalii)) |
| Minerwa Metalia      | 1 | ((200623-adaptacja-azalii)) |
| Pięknotka Diakon     | 1 | ((200623-adaptacja-azalii)) |
| Wiktor Satarail      | 1 | ((200623-adaptacja-azalii)) |