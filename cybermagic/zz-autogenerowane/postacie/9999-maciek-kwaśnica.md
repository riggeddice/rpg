---
categories: profile
factions: 
owner: public
title: Maciek Kwaśnica
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221122-olgierd-lowca-potworow       | przestępca z Kartaliany; chciał z kolegami skroić Olgierda Drongona, skończyło się na ciężkim pobiciu. Ale w sumie Olgierd spoko chłop, popytał i nawet pomógł medycznie. | 0096-07-11 - 0096-07-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alicja Kirnan        | 1 | ((221122-olgierd-lowca-potworow)) |
| Borys Uprakocz       | 1 | ((221122-olgierd-lowca-potworow)) |
| Jakub Altair         | 1 | ((221122-olgierd-lowca-potworow)) |
| Lutus Saraan         | 1 | ((221122-olgierd-lowca-potworow)) |
| Olgierd Drongon      | 1 | ((221122-olgierd-lowca-potworow)) |