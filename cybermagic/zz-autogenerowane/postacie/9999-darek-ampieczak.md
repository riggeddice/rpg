---
categories: profile
factions: 
owner: public
title: Darek Ampieczak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200226-strazniczka-przez-lzy        | agent Pustogoru, biostymulator; wraz z Kastorem i Darkiem weszli w umysł Alicji by zmienić ją w Strażniczkę Esuriit; podawał jej środki i przeprowadzał zabieg. | 0110-07-29 - 0110-08-01 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alicja Kiermacz      | 1 | ((200226-strazniczka-przez-lzy)) |
| Alina Anakonda       | 1 | ((200226-strazniczka-przez-lzy)) |
| Crystal d'Corieris   | 1 | ((200226-strazniczka-przez-lzy)) |
| Eliza Farnorz        | 1 | ((200226-strazniczka-przez-lzy)) |
| Kastor Miczokan      | 1 | ((200226-strazniczka-przez-lzy)) |
| Lucjusz Blakenbauer  | 1 | ((200226-strazniczka-przez-lzy)) |
| Rafał Muczor         | 1 | ((200226-strazniczka-przez-lzy)) |