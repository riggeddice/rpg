---
categories: profile
factions: 
owner: public
title: Patryk Majwuron
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230920-legenda-o-noktianskiej-mafii | Grzymościowiec; by zaimponować 'Irilei' torturował Karpovów. Pobity i shańbiony przez Xaverę, doszedł jednak do linku 'Furie - Irelia'. Skończył oddany Grzymościowi w pohańbiony sposób. | 0081-07-10 - 0081-07-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Bogdan Gwiazdocisz   | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Dmitri Karpov        | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Irelia Kairanolis    | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Petra Karpov         | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Xavera Sirtas        | 1 | ((230920-legenda-o-noktianskiej-mafii)) |