---
categories: profile
factions: 
owner: public
title: Ernest Bankierz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240110-wieczna-wojna-bladawira      | mało znaczący i ważny kapitan Orbitera, pies na kobiety (z danych Klaudii). Jego mostek jest jak mostek z anime - same ładne dziewczyny. ROZWALIŁ KRALOTHA czego nie może przeboleć Bladawir. | 0110-12-24 - 0110-12-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Bladawir      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Arianna Verlen       | 1 | ((240110-wieczna-wojna-bladawira)) |
| Dormand Miraris      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Kazimierz Darbik     | 1 | ((240110-wieczna-wojna-bladawira)) |
| Klaudia Stryk        | 1 | ((240110-wieczna-wojna-bladawira)) |
| Marian Witaczek      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Samuel Fanszakt      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Talia Miraris        | 1 | ((240110-wieczna-wojna-bladawira)) |
| Władawiec Diakon     | 1 | ((240110-wieczna-wojna-bladawira)) |
| Zaara Mieralit       | 1 | ((240110-wieczna-wojna-bladawira)) |