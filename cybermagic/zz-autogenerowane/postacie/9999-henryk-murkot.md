---
categories: profile
factions: 
owner: public
title: Henryk Murkot
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220101-karolina-w-ciezkim-mlocie    | starszy wiarus podwiercki, zna się na ścigaczach i pojazdach, złota rączka. Przyjaciel Władka Owczarka. Pomógł opanować awarię w Zakładzie Recyklingu Owczarek z Karoliną. Wyjaśnił Karo, czemu ludzie z Ciężkiego Młota nie lubią AMZ / Rekinów. | 0111-04-25 - 0111-05-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Daniel Terienak      | 1 | ((220101-karolina-w-ciezkim-mlocie)) |
| Karolina Terienak    | 1 | ((220101-karolina-w-ciezkim-mlocie)) |
| Mimoza Elegancja Diakon | 1 | ((220101-karolina-w-ciezkim-mlocie)) |
| Władysław Owczarek   | 1 | ((220101-karolina-w-ciezkim-mlocie)) |