---
categories: profile
factions: 
owner: public
title: Lerten Kieras
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221006-ona-chce-dziecko-eustachego  | Exerinn; brat Avy, który miał w Ogrodzie Ciał zająć się harvestem drogich anomalii - tam napadli go Farighanie i pasożytniczym implantem zmusili do służenia ich celowi. Uratowany przez Ardillę (która go zatrzymała) i Embana (zdjął implant) | 0092-09-20 - 0092-09-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Ava Kieras           | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Emban Dolamor        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Eustachy Korkoran    | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Maks Selert          | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Michał Kervendal     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| OO Infernia          | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Staszek Zakraton     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| VN Exerinn           | 1 | ((221006-ona-chce-dziecko-eustachego)) |