---
categories: profile
factions: 
owner: public
title: Celina Lertys
---

# {{ page.title }}


# Generated: 



## Fiszki


* drakolitka z Aspirii podkochująca się w Eustachym  <-- często Kić | @ 230201-wylaczone-generatory-memoriam-inferni
* (ENCAO: --+-0 |Stanowcza, skryta;;O wielu maskach| Universalism, Tradition, Security > Power, Humility| DRIVE: Harmonia naturalna między wszystkim) | @ 230201-wylaczone-generatory-memoriam-inferni
* złotoskóra o błękitnych elektrycznych włosach podkochująca się w Eustachym (science / bio officer + medical); augmentowana na widzenie rzeczy ukrytych; 19 lat | @ 230201-wylaczone-generatory-memoriam-inferni
* "Jesteśmy częścią Neikatis i podlegamy procesom Neikatis. Ale nadal mam zamiar zrobić wszystko by uratować swoich przyjaciół." | @ 230201-wylaczone-generatory-memoriam-inferni

### Wątki


infernia-jej-imieniem
kidiron-zbawca-nativis

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220723-polowanie-na-szczury-w-nativis | przekonała Tymona do wzięcia szczurzego zlecenia jako słup, zastawiła pułapkę na Laurencjusza, przeszukała rzeczy niebezpiecznego advancera i wyciągnęła z dziadka info o przeszłości. Aha, uratowała wandali przed dziadkiem strzelającym przez ścianę. A, i jest aspirianką. | 0087-05-03 - 0087-05-12 |
| 220831-czarne-helmy-i-robaki        | doszła do tego, że Robaki robią analizy żywności i dlatego Kidironowie ich tępią..? Jako "ta społeczna" poznała kilka nazwisk i doszła, że to nie kult śmierci. | 0092-08-15 - 0092-08-27 |
| 220720-infernia-taksowka-dla-lycoris | złotoskóra o błękitnych elektrycznych włosach podkochująca się w Eustachym (science / bio officer + medical) Inferni. Dość poważna, nosi przy sobie kosmetyki do podrywania Eustachego, augmentowana na widzenie rzeczy ukrytych (np. zmian skóry Kamila). Mimo młodego wieku (19) dość kompetentna i umie się opanować. | 0093-01-20 - 0093-01-22 |
| 220817-osy-w-ces-purdont            | z kiepskiego systemu medycznego zsyntetyzowała antidotum mające uratować Darię przed nędznym losem. | 0093-01-23 - 0093-01-24 |
| 230201-wylaczone-generatory-memoriam-inferni | aktorstwem spowolniła amalgamoid trianai przed atakiem na Infernię; potem ostro postawiła się planom Kidirona (Tymon && Eustachy) i skłoniła Eustachego by jednak pomógł noktianom. Pomogła noktianom i ostrzegła Kallistę, że chcą ją złapać. Zbiera dane o infekcji żywności przez trianai. | 0093-02-10 - 0093-02-12 |
| 230329-zdrada-rozrywajaca-arkologie | ostrzegła Ardillę, że mechanizmy medyczne pilnujące wujka mają błędne odczyty. Pilnuje, by NIKT nie dostał się do wujka i nie zrobił mu krzywdy. | 0093-03-14 - 0093-03-16 |
| 230628-wojna-o-arkologie-nativis-konsolidacja-sil | czuje się zdradzona przez Ardillę i nie przyjmuje jej argumentów - Rafał Kidiron MUSI zostać odsunięty od władzy. Dziadek przekonał ją, że muszą pomóc Ardilli, ale Celina rozerwała przyjaźń z Ardillą. | 0093-03-27 - 0093-03-28 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220723-polowanie-na-szczury-w-nativis | ogólna reputacja 'dziwnej' w Nativis; nikt nie chce z nią zadzierać. Wie, że pochodzi z Aspirii. | 0087-05-12
| 230201-wylaczone-generatory-memoriam-inferni | uznana za przyjaciółkę noktian z Coruscatis na Neikatis. Coruscatis jej tego nie zapomną. | 0093-02-12
| 230201-wylaczone-generatory-memoriam-inferni | straciła jakiekolwiek uczucia wobec Eustachego. Eustachy okazał się być innym człowiekiem niż wierzyła. | 0093-02-12
| 230628-wojna-o-arkologie-nativis-konsolidacja-sil | wspiera Bartłomieja Korkorana i tylko jego z rodziny Korkoranów. Wszyscy inni są jej zdaniem lojalistami Kidirona. | 0093-03-28

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 6 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Eustachy Korkoran    | 6 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Jan Lertys           | 5 | ((220720-infernia-taksowka-dla-lycoris; 220723-polowanie-na-szczury-w-nativis; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni)) |
| Tymon Korkoran       | 4 | ((220723-polowanie-na-szczury-w-nativis; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Bartłomiej Korkoran  | 3 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| OO Infernia          | 3 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Rafał Kidiron        | 3 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Ralf Tapszecz        | 3 | ((230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Kalia Awiter         | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Kamil Wraczok        | 2 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont)) |
| Karol Lertys         | 2 | ((220723-polowanie-na-szczury-w-nativis; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Laurencjusz Kidiron  | 2 | ((220723-polowanie-na-szczury-w-nativis; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| BIA Prometeus        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Czesław Żuczek       | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Daria Raizis         | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Emilia d'Erozja      | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Franciszek Pietraszczyk | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Izabella Saviripatel | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Joachim Puriur       | 1 | ((220817-osy-w-ces-purdont)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kordian Olgator      | 1 | ((220817-osy-w-ces-purdont)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Lycoris Kidiron      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Małgorzata Maratelus | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Marcel Draglin       | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| OA Erozja Ego        | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Stanisław Uczantor   | 1 | ((220831-czarne-helmy-i-robaki)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Tobiasz Lobrak       | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| VN Karglondel        | 1 | ((220817-osy-w-ces-purdont)) |
| Wiktor Turkalis      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Wojciech Czerpń      | 1 | ((220831-czarne-helmy-i-robaki)) |
| Wojciech Grzebawron  | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |