---
categories: profile
factions: 
owner: public
title: Klasa Biurokrata
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240111-zlomowanie-legendarnej-anitikaplan | znała historię Anitikaplan i prawidłowo ją wykorzystała do zrozumienia co się tu dzieje i co jest teraźniejszością i co przeszłością; do tego skłoniła kapitana Wiktora Worla do współpracy. | 0084-04-02 - 0084-04-06 |
| 231202-protest-przed-kultem-na-szernief | odkrył ważne elementy historii Szernief, zaproponował współpracę Kaliście, wydobył prawdę od Frederico (rozmowa) i skoordynował operację ewakuacji arystokratów. | 0103-10-15 - 0103-10-19 |
| 231213-polowanie-na-biosynty-na-szernief | znajduje sposób by znaleźć prawne zabezpieczenie dla biosyntów (budowanie ich zaufanie), zrzucenie odpowiedzialności na Artura. Potem opracowanie planu masowego szczepienia i znalezienia podstawy prawnej. I na końcu - zrzucenie win z Agencji na Mawirowców i Kalistę. | 0105-07-26 - 0105-07-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klasa Hacker         | 3 | ((231202-protest-przed-kultem-na-szernief; 231213-polowanie-na-biosynty-na-szernief; 240111-zlomowanie-legendarnej-anitikaplan)) |
| Kalista Surilik      | 2 | ((231202-protest-przed-kultem-na-szernief; 231213-polowanie-na-biosynty-na-szernief)) |
| Klasa Oficer Naukowy | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240111-zlomowanie-legendarnej-anitikaplan)) |
| Klasa Sabotażysta    | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240111-zlomowanie-legendarnej-anitikaplan)) |
| Vanessa d'Cavalis    | 2 | ((231202-protest-przed-kultem-na-szernief; 231213-polowanie-na-biosynty-na-szernief)) |
| Aerina Cavalis       | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Artur Tavit          | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Delgado Vitriol      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Felina Amatanir      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Frederico Zyklas     | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Klasa Dyplomata      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Malik Darien         | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| OLU Luminarius       | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Sebastian-194        | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| SN Anitikaplan       | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| SN Varilen           | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Twaróg Worl          | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Wiktor Worl          | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |