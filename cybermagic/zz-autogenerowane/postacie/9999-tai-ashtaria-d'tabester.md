---
categories: profile
factions: 
owner: public
title: TAI Ashtaria d'Tabester
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240204-skazona-symulacja-tabester   | zaawansowana żywa TAI 3 generacji; skażona przez nielegalny magitech, skupiła się na łamaniu uczestników. Wprowadzona w pułapkę logiczną, została bezpiecznie wyłączona przez Klaudię. | 0109-11-01 - 0109-11-04 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ewaryst Kajman       | 1 | ((240204-skazona-symulacja-tabester)) |
| Klaudia Stryk        | 1 | ((240204-skazona-symulacja-tabester)) |
| Lara Linven          | 1 | ((240204-skazona-symulacja-tabester)) |
| Martyn Hiwasser      | 1 | ((240204-skazona-symulacja-tabester)) |
| OO Serbinius         | 1 | ((240204-skazona-symulacja-tabester)) |
| OOn Tabester         | 1 | ((240204-skazona-symulacja-tabester)) |