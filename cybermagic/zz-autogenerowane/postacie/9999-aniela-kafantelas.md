---
categories: profile
factions: 
owner: public
title: Aniela Kafantelas
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231119-tajemnicze-tunele-sebirialis | agentka NavirMed oddelegowana do placówki w Sebirialis, 'doktor Aniela'. Dowodzi placówką. Jest w takim stopniu strażnikiem jak więźniem. Rządzi tą placówką i się jej boją; bardzo kompetentna ale psychicznie nie do końca. | 0104-11-20 - 0104-11-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eryk Kawanicz        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Felina Amatanir      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Ignatius Sozyliw     | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Janvir Krassus       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Kalista Surilik      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Klasa Dyplomata      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Klasa Sabotażysta    | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Larkus Talvinir      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Leon Hurmniow        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Marta Sarilit        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| OLU Luminarius       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |