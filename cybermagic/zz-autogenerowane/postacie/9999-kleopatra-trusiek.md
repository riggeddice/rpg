---
categories: profile
factions: 
owner: public
title: Kleopatra Trusiek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230808-nauczmy-mlodego-tiena-jak-zyc | egzotyczna drakolicka piękność Bilgemenera; podała narkotyki Arminowi zgodnie z planem, podpuściła Armina do polowania na makaronotwora i prawie ucierpiała - gdyby nie Amanda. Przerażona i posłuszna. | 0095-09-14 - 0095-09-23 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Armin Samszar        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Celina Maskiewnik    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Elena Samszar        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Florian Samszar      | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Karolinus Samszar    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Rufus Bilgemener     | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Tadeusz Maskiewnik   | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Wiktor Blakenbauer   | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |