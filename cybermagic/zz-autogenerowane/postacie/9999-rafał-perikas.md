---
categories: profile
factions: 
owner: public
title: Rafał Perikas
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210311-studenci-u-verlenow          | 16 lat, Blakenbauer dał mu lustro które "skopiuje" wiłę w Elenę; podkochuje się w Elenie. Przekonał brata do planu. Bardziej społeczny. | 0097-01-16 - 0097-01-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Apollo Verlen        | 1 | ((210311-studenci-u-verlenow)) |
| Arianna Verlen       | 1 | ((210311-studenci-u-verlenow)) |
| Dariusz Blakenbauer  | 1 | ((210311-studenci-u-verlenow)) |
| Elena Verlen         | 1 | ((210311-studenci-u-verlenow)) |
| Maja Samszar         | 1 | ((210311-studenci-u-verlenow)) |
| Michał Perikas       | 1 | ((210311-studenci-u-verlenow)) |
| Rufus Samszar        | 1 | ((210311-studenci-u-verlenow)) |
| Sylwia Perikas       | 1 | ((210311-studenci-u-verlenow)) |
| Viorika Verlen       | 1 | ((210311-studenci-u-verlenow)) |