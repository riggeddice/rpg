---
categories: profile
factions: 
owner: public
title: Wojciech Czerpń
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220831-czarne-helmy-i-robaki        | technik pracujący pod Lycoris mający dostęp do analizatora żywności na co dzień. On jest "ekspertem" Robaków od badań żywności. Ważny w Robakach. | 0092-08-15 - 0092-08-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((220831-czarne-helmy-i-robaki)) |
| Celina Lertys        | 1 | ((220831-czarne-helmy-i-robaki)) |
| Eustachy Korkoran    | 1 | ((220831-czarne-helmy-i-robaki)) |
| Jan Lertys           | 1 | ((220831-czarne-helmy-i-robaki)) |
| Stanisław Uczantor   | 1 | ((220831-czarne-helmy-i-robaki)) |
| Tymon Korkoran       | 1 | ((220831-czarne-helmy-i-robaki)) |