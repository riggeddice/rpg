---
categories: profile
factions: 
owner: public
title: Rafał Torszecki
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "pustogor, rekin, torszecki, aurum"
* owner: "public"
* title: "Rafał Torszecki"


## Kim jest

### W kilku zdaniach

Mało ważny arystokrata z mało ważnego rodu Aurum. Cały jego ród ma domieszkę krwi noktiańskiej i interesy z noktianami. Rafał jest trochę gogusiem, płaszczy się wobec Władzy i wygląda na niegroźnego. Jest jednak świetny w pozyskiwaniu informacji, agregacji wiedzy i jest dobrym aktorem. Boi się bólu, ale odważnie bierze na siebie problemy przełożonych - wierzy w system i chce nim pomagać.

### Co się rzuca w oczy

* Świetnie ubrany, w odpowiedni sposób do każdej okazji. Wyraźnie lubi piękno.
* Płaszczy się strasznie przed Władzą (Marysią). Jest niegroźny. Jest niegodny ataku.
* Praktycznie nie czaruje. 

### Jak sterować postacią

* Preferuje używanie kontaktów > narzędzi > artefaktów nad działania osobiste czy magię.
* Zawsze tak dobrze ubrany i wymuskany jak tylko jest to możliwe. Bardzo lubi się dobrze ubierać i dobrze wyglądać.
* Płaszczy się jawnie przed najsilniejszą osobą (tu: Marysia Sowińska). Żyje by spełniać jej życzenia.
* Ogólnie, wierzy w to, że system i tradycja działają. Działa zgodnie z nimi a nie przeciw nim.
* Zwalcza wszelkie alternatywne systemy - mafie itp. Tylko te, które wspierają prawny porządek (Pustogor) i Aurum mają działać.
* Nie ufa nadmiarowi magii i stosowaniu magitechów. Duża niechęć do Eterni. Sympatia i wspieranie zasymilowanych noktian.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Zbierając informacje i raporty udowodnił swojemu rodowi, że w Parwólce użycie magitrowni jest podobnie skuteczne jak zwykła elektrownia - i dużo groźniejsze. Nie zdecydowano się na budowę magitrowni na rzece.
* Parę miesięcy po pojawieniu się wśród Rekinów złożył swoją sieć kontaktów z lokalsów nie wydając specjalnie środków. Handlował informacjami, plotkami itp. Broker informacji.
* Gdy był w klubie sportowym i ów klub został napadnięty przez chuliganów, zdołał wypłaszczyć i wyżebrać tylko kilka pogardliwych kopniaków. Jego koledzy dostali dużo mocniej - nie byli w stanie grać potem.
* Mimo swoich działań na terenie Szczelińca, NIKT nie podejrzewał go o proaktywność i bycie sensownym. Świetnie się ukrył za fasadą płaszczaka i gogusia.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: Broker i agregator informacji. Potrafi połączyć różne dziwne fakty i wyprowadzić jak powinna sytuacja wyglądać lub co się dzieje.
* AKCJA: Potrafi załatwić wszystko - perfekcyjny asystent. Mistrz biurokracji i etykiety.
* AKCJA: Potrafi opóźnić wszystko - plany, agregacje i okoliczności potrafi tak ułożyć, by dramatycznie spowolnić drugą stronę.
* AKCJA: Doskonały aktor. Świetnie kłamie. Nie ma cienia skrupułów przed kłamaniem i przekazaniem kłamstwa o kilku warstwach.
* COŚ: Seria agentów, którzy informują go o plotkach i sytuacjach wśród Rekinów i na terenie Szczelińca. Szeroka sieć kontaktów.
* COŚ: Lista wielu ważnych adresów, maili itp. Ma możliwość skontaktowania się z osobami, o których mało kto wie.
* COŚ: Bardzo dużo wie o noktianach, Noctis itp. Ma kontakty, adresy, wiedzę o technologiach itp.

### Serce i Wartości (3)

* Bezpieczeństwo. 
    * Płaszczy się przed Władzą, bo to maksymalizuje wpływ i bezpieczeństwo. Plus, pozory niegroźności. 
    * Chroni swych przełożonych. Czerpie wpływy i bezpieczeństwo z możnych i bycia faktycznie przydatnym.
    * Plany w planach, pułapki w pułapkach. Chce wiedzieć wszystko o wszystkim.
* Tradycja
    * Wzmacnia system bo ten działa. Indywidualizm prowadzi do ruiny i cierpienia.
    * On jest mniej ważny, wiec zgłasza się na ochotnika. Przełożony wie lepiej.
    * Wszystko zostaje w rodzinie. Need to know basis. 
* Dobrodziejstwo
    * Przekierowuje uderzenia w siebie, bo dużo wytrzyma i ma sieć ochronną. Bez problemu się „poświęci” dla innych.
    * Usprawnia istniejący system - by lepiej pomagał wszystkim grupom.
    * Wspiera akceptację noktian - tajemnicą poliszynela jest to, że jego ród ma dużo krwi i powiązań z Noctis.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Nie mam znaczenia i jestem z nieważnego rodu. Nikt się ze mną nie liczy. Pragnę mocy i wpływu, ale NIGDY jej nie dostaję. Zawsze ktoś inny."
* CORE LIE: "Nie jestem w stanie nic zmienić i tak jak jest tak będzie. Mogę jedynie wspierać możnych i dopasować się do ich woli. Ja nie mam znaczenia."
* Agenci Torszeckiego są często niegodni zaufania, korzysta z ich liczby; może być bardzo zmylony przez odpowiednio zdeterminowanego przeciwnika.
* Torszecki wierzy w informacje od Sowińskich, bo to jest strategicznie dlań korzystne. Ale przez to może być kierowany przez Sowińskich w Aurum.
* Fizycznie Torszecki nie nadaje się do walki, nie jest ani silny, szybki ani dobry technicznie. Bardzo słaby jako Rekin.
* Bardzo słaba pozycja. Torszecki to idealny cel po którym można jechać. Nie ma jak się bronić i nie chroni go silny ród. Plus, nielubiany.
* Boi się bólu. I Lemurczaków. Zwłaszcza Lemurczaków.

### Magia (3M)

#### W czym jest świetna

* "Magiczny krawiec": umiejętność umagiczniania ubrań i materiałów. Wodoodporność, lepiej wyglądają, usztywnienie, nośnik dalszych czarów...
* Pomniejsza magia soniczna: emisja, przenoszenie i odbieranie dźwięków.
* Zwykle robi coś w stylu "zostawiam rękawiczkę, która przeniesie o czym mówią do mnie".

#### Jak się objawia utrata kontroli

* Głosy, szepty, kakofonia, rozsypane wszelkie formy komunikacji, przenoszenie losowych dźwięków w niewłaściwe miejsca...
* Coś strasznego dzieje się z ubraniami. Ożywienie ubrań, stworzenie mimika, zniknięcie, zmiana tekstury...
* Czasem ulegają ujawnieniu Straszne Sekrety osób niedaleko. Lub samego Torszeckiego.

### Specjalne

* .

## Inne

### Wygląd

* ?

### Coś Więcej

* .

### Endgame

* Optymistycznie 1:
    * Ożeni się z Marysią Sowińską.
* Pesymistycznie 1: 
    * Zostanie oddany w ręce Lemurczaków - podpadnie zbyt wielu osobom.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200513-trzyglowec-kontra-melinda    | arystokrata Aurum, zapłacił Grabarzowi by podnieść swoją pozycję i reputację w Aurum. | 0109-11-08 - 0109-11-17 |
| 210622-verlenka-na-grzybkach        | Rekin. Cel życiowy - być największym przydupasem Marysi Sowińskiej na świecie. Dość przydatny w pozyskiwaniu informacji, acz nie ma zupełnie inicjatywy. Wykonuje rozkazy, nie myśli proaktywnie. Skończył jako przynęta na Arkadię. | 0111-05-30 - 0111-05-31 |
| 210713-rekin-wspiera-mafie          | wpierw działał jako detektyw (wykrył skrytkę Kacpra Bankierza x mafii) a potem jako szpieg (podłożył dowody świadczące że to Kacper otruł Arkadię). Lojalny Marysi. | 0111-06-06 - 0111-06-09 |
| 210720-porwanie-daniela-terienaka   | po tym jak ślady destrukcji magazynu Kacpra wskazywały na niego został skopany przez Karolinę Terienak. Wyjął na nią różdżkę kontra jej ścigacz i wywołał KOLEJNĄ anomalię... | 0111-06-14 - 0111-06-16 |
| 210831-serafina-staje-za-wydrami    | znalazł dla Marysi kontakt i namiar na Serafinę Irę. Oddał się Serafinie na zakładnika, by Marysia mogła z Serafiną porozmawiać. | 0111-07-07 - 0111-07-10 |
| 210921-przybycie-rekina-z-eterni    | nieprawdopodobnie zazdrosny o świtę Ernesta Namertela; to Marysia Sowińska powinna mieć największą świtę wśród Rekinów. Przeprowadził PLANY by pozycja Marysi wróciła do najwyższego poziomu / punktu. | 0111-07-20 - 0111-07-25 |
| 210928-wysadzony-zywy-scigacz       | wpierw robił jako herold Marysi wzbudzając radość Ernesta, potem pomagał Marysi wydobyć od Ernesta wiedzę (jako błazen nie ze swojej winy). Doszedł do relacji Ernest - Amelia. Gdy Ernest stracił panowanie, zasłonił Marysię i dostał poważne rany. | 0111-07-26 - 0111-07-27 |
| 211012-torszecki-pokazal-kregoslup  | leżąc w szpitalu przyznał się Karolinie że to on zniszczył ścigacz. Nie chce, by Karolina ucierpiała z ręki Marysi tak jak on. Zrobił to by chronić Marysię przed miłością wywołaną przez Esuriit z ręki Ernesta. Powiązał (błędnie) fakty Ernest x Amelia za "Marysię czeka ten sam los". | 0111-07-29 - 0111-07-30 |
| 211026-koszt-ratowania-torszeckiego | nie wiedział jak z tego z Ernestem wyjść, ale Marysia przyszła do niego z planem. Zaakceptował ten plan - wziął od niej robaka podskórnego. Musi go w końcu kochać. | 0111-08-01 - 0111-08-05 |
| 211102-satarail-pomaga-marysi       | Owad Sataraila sprawił, że faktycznie nikt nie wini go za to co się stało. Ale nie jest ulubionym magiem nikogo ;-). | 0111-08-09 - 0111-08-10 |
| 211123-odbudowa-wedlug-justyniana   | stawał w obronie czci Marysi, za co pobili go raz czy dwa. Justynian powiedział Marysi o jego nędznym losie. | 0111-08-11 - 0111-08-20 |
| 211127-waśń-o-ryby-w-majklapcu      | nie znalazł dla Karo info o mafii i tylko podpadł mafii, ale zaproponował transponder co Karo przekształciła w wabik na koty. Dostarczył kompromitujące twarde narkotyki Danielowi, by ten mógł podłożyć je mafii. Pomógł Karo w manipulowaniu mafii. | 0111-08-19 - 0111-08-24 |
| 211228-akt-o-ktorym-marysia-nie-wie | ostrzegł Marysię przed tym że jej akt istnieje. Jak ta zignorowała, sam go zdobył od Liliany Bankierz. Zanim zdążył go zniszczyć, Daniel Terienak go pobił i mu zabrał ów akt. | 0111-09-09 - 0111-09-12 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210713-rekin-wspiera-mafie          | Kacper Bankierz uznał, że Torszecki to jego osobisty wróg za poszczucie go Arkadią. | 0111-06-09
| 210720-porwanie-daniela-terienaka   | sterroryzowany przez Karolinę Terienak; jest tchórzem. Zrobi to co Karolina mu każe. Nie z miłości a ze strachu. | 0111-06-16
| 210928-wysadzony-zywy-scigacz       | szczera nienawiść do Ernesta - wyśmiewał go, nie szanuje Marysi i ma większą świtę niż ona. Plus, ciężko go skrzywdził poniżej 5 sekund. | 0111-07-27
| 210928-wysadzony-zywy-scigacz       | 5 dni regeneracji u Różewicza Diakona. Ernest zadaje straszne obrażenia. | 0111-07-27
| 210928-wysadzony-zywy-scigacz       | zorientował się, że jest coś między Amelią Sowińską i Ernestem Namertelem. Wie też, że Ernest patrzy na Amelię jak na paladynkę. I wie, że Amelia to podła sucz. | 0111-07-27
| 211012-torszecki-pokazal-kregoslup  | naprawdę szczerze LUBI Karolinę Terienak; by ją osłonić przed krzywdą przyznał jej się, że to on zniszczył ścigacz. Ona nie odwzajemnia przyjaźni. | 0111-07-30
| 211012-torszecki-pokazal-kregoslup  | nie ufa Marysi Sowińskiej. Jest jej ultralojalny, ale nie ufa jej. Planuje za jej plecami; dostał info z Rodu Sowińskich o Erneście i skrzyżował fakty Amelia x Ernest i dlatego chciał Marysię wesprzeć i ją chronić... i poszła katastrofa. | 0111-07-30
| 211026-koszt-ratowania-torszeckiego | pojawiają się plotki na temat jego romansu z Marysią Sowińską. Na razie nie podsyca. | 0111-08-05
| 211026-koszt-ratowania-torszeckiego | jest przekonany, że Marysia Sowińska się w nim podkochuje. Mówi mu groźne dla siebie sekrety i zaryzykowała nawet Sataraila by go ratować. | 0111-08-05
| 211102-satarail-pomaga-marysi       | eternianie WIEDZĄ, że był zainfekowany, ale NIE DBAJĄ o to. Sezon bicia Torszeckiego otwarty. Rekinom też to pasuje, bo Torszecki (plus ciekawe co Marysia). | 0111-08-10
| 211127-waśń-o-ryby-w-majklapcu      | próbował znaleźć info o mafii i ich działaniach w kierunku lokalnych biznesów i mafia się zirytowała. Niech się nie interesuje. Ma problem z mafią. | 0111-08-24
| 211228-akt-o-ktorym-marysia-nie-wie | tydzień w szpitalu, pobity przez Daniela. Bo próbował ochronić Marysię przed jej aktem "na wolności". | 0111-09-12

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 11 | ((210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka; 210831-serafina-staje-za-wydrami; 210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211012-torszecki-pokazal-kregoslup; 211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Karolina Terienak    | 9 | ((210720-porwanie-daniela-terienaka; 210831-serafina-staje-za-wydrami; 210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211012-torszecki-pokazal-kregoslup; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211127-waśń-o-ryby-w-majklapcu; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Daniel Terienak      | 5 | ((210720-porwanie-daniela-terienaka; 210831-serafina-staje-za-wydrami; 211123-odbudowa-wedlug-justyniana; 211127-waśń-o-ryby-w-majklapcu; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Ernest Namertel      | 5 | ((210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Arkadia Verlen       | 3 | ((210622-verlenka-na-grzybkach; 210921-przybycie-rekina-z-eterni; 211127-waśń-o-ryby-w-majklapcu)) |
| Ignacy Myrczek       | 3 | ((210622-verlenka-na-grzybkach; 211026-koszt-ratowania-torszeckiego; 211123-odbudowa-wedlug-justyniana)) |
| Lorena Gwozdnik      | 3 | ((210831-serafina-staje-za-wydrami; 211102-satarail-pomaga-marysi; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Sensacjusz Diakon    | 3 | ((211012-torszecki-pokazal-kregoslup; 211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi)) |
| Tomasz Tukan         | 3 | ((210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka; 210921-przybycie-rekina-z-eterni)) |
| Urszula Miłkowicz    | 3 | ((210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka)) |
| Amelia Sowińska      | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Azalia Sernat d'Namertel | 2 | ((210928-wysadzony-zywy-scigacz; 211123-odbudowa-wedlug-justyniana)) |
| Jolanta Sowińska     | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Julia Kardolin       | 2 | ((210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie)) |
| Karol Pustak         | 2 | ((210921-przybycie-rekina-z-eterni; 211123-odbudowa-wedlug-justyniana)) |
| Keira Amarco d'Namertel | 2 | ((210928-wysadzony-zywy-scigacz; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Ksenia Kirallen      | 2 | ((211026-koszt-ratowania-torszeckiego; 211127-waśń-o-ryby-w-majklapcu)) |
| Liliana Bankierz     | 2 | ((210622-verlenka-na-grzybkach; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Lucjan Sowiński      | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Marek Samszar        | 2 | ((210622-verlenka-na-grzybkach; 211102-satarail-pomaga-marysi)) |
| Paweł Szprotka       | 2 | ((211026-koszt-ratowania-torszeckiego; 211127-waśń-o-ryby-w-majklapcu)) |
| Wiktor Satarail      | 2 | ((211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi)) |
| Adam Cześń           | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Diana Lemurczak      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Feliks Keksik        | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Franek Bulterier     | 1 | ((210928-wysadzony-zywy-scigacz)) |
| Genowefa Krecik      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Halina Sermniek      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Iwan Zawtrak         | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Jan Łowicz           | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Justynian Diakon     | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Kacper Bankierz      | 1 | ((210713-rekin-wspiera-mafie)) |
| Katja Nowik          | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Kinga Kruk           | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Laura Tesinik        | 1 | ((210831-serafina-staje-za-wydrami)) |
| Mariusz Grabarz      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Mariusz Trzewń       | 1 | ((210720-porwanie-daniela-terienaka)) |
| Melinda Teilert      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Napoleon Bankierz    | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Nataniel Morlan      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Natasza Aniel        | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Olga Myszeczka       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Różewicz Diakon      | 1 | ((210622-verlenka-na-grzybkach)) |
| Serafina Ira         | 1 | ((210831-serafina-staje-za-wydrami)) |
| Stella Amakirin      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Teresa Mieralit      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Triana Porzecznik    | 1 | ((210622-verlenka-na-grzybkach)) |
| Żorż d'Namertel      | 1 | ((210928-wysadzony-zywy-scigacz)) |