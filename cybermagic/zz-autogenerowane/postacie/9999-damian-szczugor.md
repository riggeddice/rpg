---
categories: profile
factions: 
owner: public
title: Damian Szczugor
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220327-wskrzeszenie-krolowej-przygod | mięsień blakvelowców; chciał upewnić się, że wszystko zadziała i że da się kupić Królową Przygód od Prokopa. Okazało się, że się nie da XD. | 0108-05-01 - 0108-05-16 |
| 220405-lepsza-kariera-dla-romki     | chce się pozbyć eks-piratów z "Królowej". Miranda i Prokop walczyli o to, by Damian dał sobie z tym tymczasowo spokój. TYMCZASOWO Damian machnął na to ręką... | 0108-09-15 - 0108-09-17 |
| 210820-fecundatis-w-domenie-barana  | przeprowadzając operację polowania na Mardiusowców prawie wpakował się na Strachy i Rick go ostrzegł. Potem gdy świętował w Koronie, przyczepiła się doń Flawia i go poderwała. Walnęła mu psychotropami że było mu dobrze. Przespał największą walkę ze Strachami i jak się obudził - nie był już "prawą ręką Ernesta Blakvela" na tym terenie XD. | 0109-03-06 - 0109-03-09 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antos Kuramin        | 2 | ((210820-fecundatis-w-domenie-barana; 220405-lepsza-kariera-dla-romki)) |
| Miranda Ceres        | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220405-lepsza-kariera-dla-romki)) |
| SC Królowa Przygód   | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220405-lepsza-kariera-dla-romki)) |
| Anna Szrakt          | 1 | ((220405-lepsza-kariera-dla-romki)) |
| Antonella Temaris    | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Bruno Baran          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Cień Brighton        | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Deneb Ira            | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Elsa Kułak           | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Flawia Blakenbauer   | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Gotard Kicjusz       | 1 | ((220327-wskrzeszenie-krolowej-przygod)) |
| Helena Banbadan      | 1 | ((220405-lepsza-kariera-dla-romki)) |
| Jolanta Sowińska     | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Kara Szamun          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Leon Kantor          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Łucjan Torwold       | 1 | ((220327-wskrzeszenie-krolowej-przygod)) |
| Prokop Umarkon       | 1 | ((220327-wskrzeszenie-krolowej-przygod)) |
| Rick Varias          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Romana Kundel        | 1 | ((220405-lepsza-kariera-dla-romki)) |
| Seweryn Grzęźlik     | 1 | ((220327-wskrzeszenie-krolowej-przygod)) |
| Tamara Mardius       | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Ursyn Uszat          | 1 | ((220405-lepsza-kariera-dla-romki)) |
| Wojciech Kaznodzieja | 1 | ((220405-lepsza-kariera-dla-romki)) |