---
categories: profile
factions: 
owner: public
title: Frank Mgrot
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221123-egzotyczna-pieknosc-na-astralnej-flarze | kiedyś marine Orbitera, teraz dezerter. Zakochał się w noktiance i uciekli koło Anomalii Kolapsu dla lepszego życia. Złapani jako niewolnicy; jego kochana noktianka zniknęła mu sprzed oczu a on się tułał. Aż teraz na Hadiah Emas został uratowany przez Orbiter i jego koszmar się dopiero zacznie XD. | 0100-09-12 - 0100-09-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Arnulf Perikas       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Daria Czarnewik      | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Ellarina Samarintael | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Erwin Pies           | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gabriel Lodowiec     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gerwazy Kircznik     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Kajetan Kircznik     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Leszek Kurzmin       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Marcel Kulgard       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| OO Astralna Flara    | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| OO Athamarein        | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| SCA Hadiah Emas      | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Szczepan Myrczek     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Władawiec Diakon     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |