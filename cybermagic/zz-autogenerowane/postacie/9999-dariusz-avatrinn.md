---
categories: profile
factions: 
owner: public
title: Dariusz Avatrinn
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230810-nie-nazywasz-sie-janusz      | dowódca siły inwazyjnej Syndykatu na Cognitio Nexus, pod Saviripatel; ufa swoim ludziom i robi co może mimo że pod Syndykatem. Nie ma nadziei że mogą coś zmienić aż Janusz zapoznał go z Xairą. Chce naprawić Syndykat, ale przy przewadze Janusza i Xairy akceptuje 'weźmy kogo warto i lojalny' a resztę bazy zostawmy terrorformom Saitaera. | 0093-03-29 - 0093-03-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Artur Wrulgop        | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Janusz Lemuel        | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Krzesimir Pluszcz    | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Łucja Kritoriin      | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Maja Nerwial         | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Nova Atreia          | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| OWT Łowca Martwych Gwiazd | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| TAI Xaira Cognitia   | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Teresa Kritoriin     | 1 | ((230810-nie-nazywasz-sie-janusz)) |