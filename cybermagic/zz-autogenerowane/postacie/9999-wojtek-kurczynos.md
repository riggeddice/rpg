---
categories: profile
factions: 
owner: public
title: Wojtek Kurczynos
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190127-ixionski-transorganik        | stał się terrorformem przez błędne zaklęcie Minerwy. Chciał zniszczyć Karolinę Erenit, ale został powstrzymany przez Pięknotkę. | 0110-01-29 - 0110-01-30 |
| 190202-czarodziejka-z-woli-saitaera | poszedł do Karoliny o jeden raz za dużo. Pobity przez ninję, zdestabilizowano mu energię, zmieniono w terrorforma i stracił moc magiczną na rzecz Karoliny. | 0110-02-01 - 0110-02-05 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190127-ixionski-transorganik        | naładowany energią ixiońską; przez to jest do niego dostęp przez moc Saitaera. | 0110-01-30
| 190202-czarodziejka-z-woli-saitaera | utracił moc magiczną. Jego moc trafiła do Karoliny Erenit z woli Saitaera. | 0110-02-05

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolina Erenit      | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Minerwa Metalia      | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Pięknotka Diakon     | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Saitaer              | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Tymon Grubosz        | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Adela Kirys          | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
| Erwin Galilien       | 1 | ((190127-ixionski-transorganik)) |
| Kirył Najłalmin      | 1 | ((190127-ixionski-transorganik)) |
| Napoleon Bankierz    | 1 | ((190127-ixionski-transorganik)) |
| Sławomir Muczarek    | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
| Wiktor Satarail      | 1 | ((190127-ixionski-transorganik)) |