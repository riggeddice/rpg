---
categories: profile
factions: 
owner: public
title: Wiktor Blakenbauer
---

# {{ page.title }}


# Generated: 



## Fiszki


* 22, ekspert od środków psychoaktywnych | @ 230711-zablokowana-sentisiec-w-krainie-makaronu
* OCEAN: (C+O-): cierpliwy z charakterem lodowca, monotematyczny i monomaniczny, "Trochę cierpliwości i po mojemu będzie." | @ 230711-zablokowana-sentisiec-w-krainie-makaronu
* VALS: (Humility, Universalism): nikt nie jest specjalny, wszyscy podlegają tym samym regułom, "Jak jest, tak było i będzie. Kolejna iteracja tego samego cyklu." | @ 230711-zablokowana-sentisiec-w-krainie-makaronu
* Core Wound - Lie: "monomaniczny Blakenbauer alchemik" - "wzmocnię alchemię i udowodnię wszystkim, że alchemią osiągniesz wszystko" | @ 230711-zablokowana-sentisiec-w-krainie-makaronu
* styl: beznamiętny alchemik, "_I make elixirs. That's what I do. When I do this elixir I will make another one._" | @ 230711-zablokowana-sentisiec-w-krainie-makaronu

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210306-wiktoriata                   | BG; 20. Twórca super-skutecznego psychoaktywnego środku o nazwie wiktoriata; rozprzestrzenia przez wiły. By dorobić i eksperymentować, sprzedaje go do Verlenów. Zatrzymany przez Lucjusza, bo wiktoriata ma psychotyczne efekty uboczne. | 0093-12-19 - 0093-12-28 |
| 230711-zablokowana-sentisiec-w-krainie-makaronu | gdy Petra wjechała mu na ambicję, powiedział jej że może zniszczyć cały Triticatus gdyby nie sentisieć. I faktycznie, jego eliksiry skutecznie powodowały problem - ale gdy Petra przyszła i przeprosiła, anulował operację. Świetny alchemik o rybich oczach. | 0095-09-05 - 0095-09-08 |
| 230808-nauczmy-mlodego-tiena-jak-zyc | 22, ekspert od środków psychoaktywnych; ku wielkiemu zdziwieniu skontaktowała się z nim Elena i skontaktowała go z mafią Kajrata. Ale za to pomógł postraszyć młodego Armina Samszara używając płaszczek i uratował go starym traperem (człowiekiem). | 0095-09-14 - 0095-09-23 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Elena Samszar        | 2 | ((230711-zablokowana-sentisiec-w-krainie-makaronu; 230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Karolinus Samszar    | 2 | ((230711-zablokowana-sentisiec-w-krainie-makaronu; 230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Amanda Kajrat        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Apollo Verlen        | 1 | ((210306-wiktoriata)) |
| Armin Samszar        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Celina Maskiewnik    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Florian Samszar      | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Ignatus Blakenbauer  | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Irek Kraczownik      | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Kleopatra Trusiek    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Lucjusz Blakenbauer  | 1 | ((210306-wiktoriata)) |
| Petra Samszar        | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Przemysław Czapurt   | 1 | ((210306-wiktoriata)) |
| Rufus Bilgemener     | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Tadeusz Maskiewnik   | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Viorika Verlen       | 1 | ((210306-wiktoriata)) |
| Wacław Samszar       | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |