---
categories: profile
factions: 
owner: public
title: Pandemiusz Diakon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210918-pierwsza-bitwa-martyna       | przełożony Martyna na OO Invictus. Dobry lekarz ufający swojej załodze, aprobował pomysły Martyna by wesprzeć Jolantę Kopiec i wysłał go na akcję jako ochotnika mimo, że powinien wysłać kogoś bardziej doświadczonego. Jego przeczucia zwróciły się pozytywnie. | 0080-11-23 - 0080-11-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Jolanta Kopiec       | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Jonasz Staran        | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Kalina Rota d'Kopiec | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Martyn Hiwasser      | 1 | ((210918-pierwsza-bitwa-martyna)) |
| OO Invictus          | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Szczepan Kutarcjusz  | 1 | ((210918-pierwsza-bitwa-martyna)) |