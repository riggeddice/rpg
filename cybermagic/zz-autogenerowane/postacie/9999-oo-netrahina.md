---
categories: profile
factions: 
owner: public
title: OO Netrahina
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200715-sabotaz-netrahiny            | dalekosiężny krążownik eksploracyjny Orbitera; science/exploration ship. Jego Persefona "się uwolniła" i zbudowała paranoję wśród załogi. Persefona została zniszczona przez Ariannę Verlen (za własną zgodą). Solidnie uszkodzony; misja wymaga przerwania a Netrahina naprawy. | 0111-01-04 - 0111-01-07 |
| 210818-siostrzenica-morlana         | służyła do przerzutu nieszczęsnych arystokratów Eterni którzy podpadli Natanielowi Morlanowi. Klaudia odblokowała jej Persefonę kodami kontrolnymi. Wykryła koloidowy statek. | 0112-01-20 - 0112-01-24 |
| 210901-stabilizacja-bramy-eterycznej | miała służyć do naprawy Bramy, ale została w odwodzie. W związku z tym posłużyła do znalezienia i zniszczenia ON klasy Catarina. | 0112-02-09 - 0112-02-12 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210818-siostrzenica-morlana         | TAI Persefona uwolniona przez Klaudię podczas operacji ratowania Ofelii. | 0112-01-24

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 3 | ((200715-sabotaz-netrahiny; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej)) |
| Eustachy Korkoran    | 3 | ((200715-sabotaz-netrahiny; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej)) |
| Klaudia Stryk        | 3 | ((200715-sabotaz-netrahiny; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej)) |
| Elena Verlen         | 2 | ((200715-sabotaz-netrahiny; 210901-stabilizacja-bramy-eterycznej)) |
| Leona Astrienko      | 2 | ((210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej)) |
| Antoni Kramer        | 1 | ((210818-siostrzenica-morlana)) |
| Flawia Blakenbauer   | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Gilbert Bloch        | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Janus Krzak          | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Lothar Diakon        | 1 | ((200715-sabotaz-netrahiny)) |
| Maria Naavas         | 1 | ((210818-siostrzenica-morlana)) |
| Martyn Hiwasser      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Medea Sowińska       | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Nataniel Morlan      | 1 | ((210818-siostrzenica-morlana)) |
| Ofelia Morlan        | 1 | ((210818-siostrzenica-morlana)) |
| Olgierd Drongon      | 1 | ((210818-siostrzenica-morlana)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Tvarana           | 1 | ((200715-sabotaz-netrahiny)) |
| OO Żelazko           | 1 | ((210818-siostrzenica-morlana)) |
| Percival Diakon      | 1 | ((200715-sabotaz-netrahiny)) |
| Rufus Komczirp       | 1 | ((200715-sabotaz-netrahiny)) |
| SC Fecundatis        | 1 | ((210818-siostrzenica-morlana)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Szczepan Myksza      | 1 | ((200715-sabotaz-netrahiny)) |
| Tomasz Sowiński      | 1 | ((210818-siostrzenica-morlana)) |