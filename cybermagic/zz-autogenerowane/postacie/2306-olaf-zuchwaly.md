---
categories: profile
factions: 
owner: public
title: Olaf Zuchwały
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "Pustogor"
* owner: "public"
* title: "Olaf Zuchwały"

## Funkcjonalna mechanika

* **Obietnica**
    * "Będziesz śledczym, który został barmanem. Zdeeskalujesz niejedną sytuację, przeprowadzisz śledztwo a jak trzeba - unieszkodliwisz nogą od krzesła."
    * Były agent biura spraw wewnętrznych klanu dekadiańskiego Noctis, który nie wierzył w magię. 
        * Postrzelony przez przyjaciół i zostawiony na śmierć zakochał się w Astorii i życiu planetarnym i jako śledczy został świetnym właścicielem baru.
        * Odrzucił swoje poprzednie nazwisko wraz ze "śmiercią".
* **Rdzeń mechaniczny**
    * **Mistrzostwo (1-3)**
        * GDY sytuacja nie jest zbyt napięta, TO bez problemu wyciągnie z ludzi wszystkie sekrety. _Sympatyczny i współczujący, aż chce się z nim gadać._
        * GDY musi zrozumieć co się stało w scenie, TO składając fakty i robiąc robotę policyjną dojdzie do tego co się tu stało. _Dokładny i cierpliwy, ma czas._
    * **Kompetencje (3-5)**
        * GDY trzeba walczyć w zwarciu, TO jest mistrzem brudnej walki, zwłaszcza bronią improwizowaną. _Walczy agresywnie i do końca, jak dekadianin._
        * GDY wszyscy są zdenerwowani i przestraszeni, TO potrafi zdeeskalować śmiechem, żartem i łagodnym słowem. _Łagodny z natury, idzie za flow_.
        * GDY trzeba zmajstrować coś przy użyciu zwykłych narzędzi, TO bez problemu zrobi karmnik czy drabinę. _Lubi proste, spokojne życie na planecie._
        * GDY trzeba zająć czyjąś uwagę, TO potrafi opowiadać niezwykłe historie z czasów Noctis czy pracy śledczego. _Jest gadatliwy i ujmujący jak na dekadianina._
    * **Agenda i styl działania (3-5)**
        * PRAGNIE życia na planecie. Bar, domek, natura, majstrowanie. Wszystko, czego nie miał na statku kosmicznym. _To jest prawdziwe, dobre życie, nie te wszystkie wojny._
        * GDY sytuacja jest niejednoznaczna moralnie, TO nawet nie próbuje jej zrozumieć - tylko minimalizuje cierpienie. _Nie mieszam się, to nie na moją głowę._
        * GDY są różne nacje, style lub działania, TO próbuje integrować i pomagać wszystkim się dogadać. _Wszyscy jedziemy na tym samym wózku..._
    * **Słabości (1-5)**
        * GDY może osiągnąć sukces zabijając kogoś, TO próbuje tego uniknąć - on sam przeżył tylko dzięki szczęściu. _Nie jest pacyfistą, ale unika zabijania._
        * NAWET GDY ktoś jest niezaprzeczalnym potworem (Sabina Kazitan), TO próbuje pomóc i dać osobie drugą szansę. _Każdy ma jeszcze jedną szansę._
        
## Reszta

### W kilku zdaniach

* Były agent biura spraw wewnętrznych klanu dekadiańskiego Noctis, który nie wierzył w magię.
    * Skuteczny śledczy, usuwał problemy z morale i ścigał frakcję pro-magiczną
    * Gdy okazało się, że jego "brat" jest magiem i ma dokumenty o magii, miał go na muszce ale nie strzelił.
    * Postrzelony przez przyjaciół i zostawiony na śmierć. Wtedy "umarł".
* Dostał drugą szansę w Noctis. Uczestniczył w Inwazji jako ten co wie o magii.
* Podczas Inwazji zestrzelony.
    * Stoczył się, alkohol itp.
    * Odrzucił swoje poprzednie nazwisko, bo "zginął" zdradzony przez Noctis.
* Trzecie życie - na Astorii
    * Zakochał się w Astorii i życiu planetarnym i został świetnym właścicielem baru.

### Jak sterować postacią

* **O czym myśli**
    * "To wszystko nie na moją głowę. Proste, uczciwe życie wśród natury na planecie - to jest to."
    * "Przy wszystkich wadach Noctis niektóre rzeczy są świetne i warto je zachować i utrzymać."
    * "Po co się wszyscy kłócą, to wszystko i tak nie ma znaczenia. Nie poznamy prawdy, bo jak?"
* **Serce**
    * **Wound**
        * **Core Wound**: "W imię nieprawdy krzywdziłem innych. Wszystko w co wierzyłem obróciło się w proch. Nawet najbliżsi mnie opuścili. Próbowali mnie zastrzelić. Odrzucono mnie jak pionka."
        * **Core Lie**: "Da się żyć prostym, spokojnym życiem i się nie angażować."
    * **ENCAO**: +00++ | "Świetnie opowiada", "King of bad jokes"
    * **Wartości**: Humility, Tradition > Security
        * H: "Nie ogarnę tego co się dzieje. Miało nie być magii - jest magia. Miałem mieć rodzinę i sojuszników, zastrzelili mnie. To nie na moją głowę. Go with the flow."
        * T: "Dekadiańskie doktryny i strategie ogólnie działają, warto je rozprzestrzenić."
        * S: "Możesz zrobić wszystko dobrze a i tak wszystko stracisz. Just go with the flow."
    * **DELTA_DRIVE**: 
        * **Stan aktualny**: "Jestem nomadem, nie mam niczego, jestem w obcym świecie i muszę się angażować w rzeczy niemożliwe"
        * **Stan oczekiwany**: "Spokojne życie na Astorii wśród natury, domek, majsterkowanie, trochę bimbru i nie mieszanie się w za trudne dla mnie sprawy"
    * **Metakultura**: dekadianin. "_Nacja i Twoja grupa są wszystkim. Ja nie mam grupy ani nacji. Tylko spokojne życie na pięknej planecie._", "_Wolność jest nieistotna, tylko robienie dobrej roboty._"
    * **Kolory**: G>WR: pasja i pragnienie harmonii, pogodzenie z tym co jest
    * **Wzory**: 
        * przeciętny wesoły barman-wiking opiekujący się zbłąkanymi kotkami. Ale samotny.
    * **Inne**:
        * brak

## Możliwości

* **Dominujące Strategie**: 
    * nieokreślone
* **Zasoby**: 
    * nieznane
* **Umocowanie w świecie**: 
    * drifter, potem barman w Pustogorze.

## Magia
### Dominująca moc

* JEDNYM ZDANIEM: nie czaruje
    * .

#### Jak się objawia utrata kontroli

* .


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230618-reality-show-z-zaskoczenia   | zapijaczony stoczony dekadianin, porwany do reality show. O dziwo, integruje zespół i podnosi mu morale zamiast zginąć w piątej minucie. Uratował sporo osób przed błędami i śmiercią, po czym zrobił z nich zgrany oddział. Wpadł na reality show i kombinował jak przekonać magów by wszyscy przetrwali. Zaadaptował się i próbuje wszystkich uratować. | 0083-10-02 - 0083-10-07 |
| 230625-przegrywy-ratuja-przegrywy   | znając _genre_ tego reality show, atakuje samą strukturę i organizatorów wpływając na publiczność; konsekwentnie przekonuje wszystkich że warto ratować wszystkich. Poza napastnikami. | 0083-10-08 - 0083-10-11 |
| 230626-pustynny-final-igrzysk       | zaproponował plan pułapki 3+5, rozmawiał z noktianinem i obrócił go na stronę Zespołu, pomógł przekonać Infiltratora do współpracy i wprowadził przeciwników w pułapkę. Po zwycięstwie apelował, by dać szansę nawet tym 'morderczym psychopatom'. Mimo braku szczególnie dobrych umiejętności w jakiejkolwiek dziedzinie jego optymizm i podejście sprawiło, że udało im się wygrać. | 0083-10-12 - 0083-10-15 |
| 190101-morderczyni-jednej-plotki    | właściciel Góskiej Szalupy. Ma topór. Stylizuje się na kuriozum godne Pustogoru. Fajny facet. Pomógł Pięknotce znaleźć Szurnaka i zrobić pojedynek. | 0109-12-13 - 0109-12-17 |
| 190925-wrobieni-detektywi           | właściciel Górskiej Szalupy, zapewnił detektywom miejsce na rozmowę z przedstawicielami Luxuritias na terenie. | 0110-02-25 - 0110-03-05 |
| 190422-pustogorski-konflikt         | wyrósł na nieformalnego przywódcę Miasteczkowców; negocjował z Pięknotką i powiedział jej o tragedii dwóch Miasteczkowiczanek. | 0110-04-02 - 0110-04-03 |
| 190424-budowa-ixionskiego-mimika    | kiedyś, członek Inwazji Noctis. Teraz sympatyczny barman; postawił się terrorformowi/ mimikowi i powiedział, że nie o to walczyła Eliza. Skończył ciężko ranny. | 0110-04-03 - 0110-04-05 |
| 190429-sabotaz-szeptow-elizy        | kiedyś wysoki oficer Inwazji, teraz spokojny barman chcący współpracować z Pustogorem. Nie chce powrotu wojny. Pomógł Pięknotce w pozyskaniu Kryształu Elizy. | 0110-04-10 - 0110-04-13 |
| 190502-pierwszy-emulator-orbitera   | ma ogromny uraz do Orbitera za to, co stało się Nikoli dawno temu; powiedział Pięknotce, że Nikola nie żyje a to co tam jest to duch. | 0110-04-14 - 0110-04-16 |
| 190622-wojna-kajrata                | który ciężko pracował nad integracją ludzi i magów z Enklaw z Pustogorem, ale jego plany właśnie legły w gruzach przez działania Pięknotki. | 0110-05-18 - 0110-05-21 |
| 190917-zagubiony-efemerydyta        | właściciel baru, który nie chce krzywdy Janka ale nie da się okradać. Współpracuje z Pięknotką by złapać 17-latka. | 0110-06-21 - 0110-06-22 |
| 191103-kontrpolowanie-pieknotki-pulapka | postawił się Pięknotce - nie chce, by Enklawy ucierpiały przez Małmałaza. Powiedział Pięknotce i Kseni to, co one potrzebowały wiedzieć - ale nie zdradził niczego z Enklaw. | 0110-06-24 - 0110-07-01 |
| 200417-nawolywanie-trzesawiska      | znalazł nić porozumienia z Sabiną Kazitan, mówiąc jej, że nawet on - noktianin - został zaakceptowany. Sabina uciekła od niego, bo bała się zaprzyjaźnić. | 0110-08-12 - 0110-08-14 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230626-pustynny-final-igrzysk       | trafił do Szczelińca z Lily Sanarton metodą teleportacji. W przyszłości on i Lily zostaną rodziną. | 0083-10-15
| 190424-budowa-ixionskiego-mimika    | tydzień w szpitalu | 0110-04-05

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 9 | ((190101-morderczyni-jednej-plotki; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 190622-wojna-kajrata; 190917-zagubiony-efemerydyta; 191103-kontrpolowanie-pieknotki-pulapka; 200417-nawolywanie-trzesawiska)) |
| Erwin Galilien       | 4 | ((190101-morderczyni-jednej-plotki; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Minerwa Metalia      | 4 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Alan Bartozol        | 3 | ((190101-morderczyni-jednej-plotki; 190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera)) |
| Alan Klart           | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Karla Mrozik         | 3 | ((190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Lily Sanarton        | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Maja Wurmramin       | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Maks Ardyceń         | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Roman Wyrkmycz       | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Aleksander Rugczuk   | 2 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Antoni Kmandir       | 2 | ((230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Bogdan Ubuddan       | 2 | ((230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Gabriel Ursus        | 2 | ((190917-zagubiony-efemerydyta; 200417-nawolywanie-trzesawiska)) |
| Ksenia Kirallen      | 2 | ((190925-wrobieni-detektywi; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Marek Puszczok       | 2 | ((190917-zagubiony-efemerydyta; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Nikola Kirys         | 2 | ((190502-pierwszy-emulator-orbitera; 190622-wojna-kajrata)) |
| Aleksander Iczak     | 1 | ((190101-morderczyni-jednej-plotki)) |
| Bożymir Szczupak     | 1 | ((190502-pierwszy-emulator-orbitera)) |
| Damian Orion         | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Diana Tevalier       | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Dobby Kmic           | 1 | ((190925-wrobieni-detektywi)) |
| Eliza Ira            | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Ernest Kajrat        | 1 | ((190622-wojna-kajrata)) |
| Ignacy Myrczek       | 1 | ((200417-nawolywanie-trzesawiska)) |
| Jan Firatiel         | 1 | ((230626-pustynny-final-igrzysk)) |
| Jan Uszczar          | 1 | ((190917-zagubiony-efemerydyta)) |
| Józef Małmałaz       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Karol Szurnak        | 1 | ((190101-morderczyni-jednej-plotki)) |
| Lilia Ursus          | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Liliana Bankierz     | 1 | ((190622-wojna-kajrata)) |
| Lucjusz Blakenbauer  | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Marcel Nieciesz      | 1 | ((190422-pustogorski-konflikt)) |
| Mariusz Trzewń       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Napoleon Bankierz    | 1 | ((200417-nawolywanie-trzesawiska)) |
| Rafał Armadion       | 1 | ((230626-pustynny-final-igrzysk)) |
| Rafał Roszczeniok    | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Sabina Kazitan       | 1 | ((200417-nawolywanie-trzesawiska)) |
| Serafina Ira         | 1 | ((190622-wojna-kajrata)) |
| Strażniczka Alair    | 1 | ((200417-nawolywanie-trzesawiska)) |
| Teresa Mieralit      | 1 | ((190101-morderczyni-jednej-plotki)) |
| Wojmił Siwywilk      | 1 | ((190422-pustogorski-konflikt)) |