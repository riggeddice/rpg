---
categories: profile
factions: 
owner: public
title: Bruno Baran
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210813-szmuglowanie-antonelli       | tien eternijski w Pasie Kazimierza i obiekt westchnień Jolanty Sowińskiej kiedyś. Dostawał od niej listy miłosne i nawet odpisywał z faktami i informacjami młodej tience z Aurum. | 0109-02-11 - 0109-02-23 |
| 210820-fecundatis-w-domenie-barana  | założyciel Domeny Ukojenia, wycofał się na ubocze (nie lubi demokracji). Sporo wie o tym terenie, Strachach itp - ale się dąsa i nie chce wyjść. | 0109-03-06 - 0109-03-09 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antonella Temaris    | 2 | ((210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Cień Brighton        | 2 | ((210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Flawia Blakenbauer   | 2 | ((210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Jolanta Sowińska     | 2 | ((210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Antos Kuramin        | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Damian Szczugor      | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Deneb Ira            | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Elsa Kułak           | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Kara Szamun          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Leon Kantor          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Lucjusz Blakenbauer  | 1 | ((210813-szmuglowanie-antonelli)) |
| Rick Varias          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| SC Fecundatis        | 1 | ((210813-szmuglowanie-antonelli)) |
| Tamara Mardius       | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Tomasz Sowiński      | 1 | ((210813-szmuglowanie-antonelli)) |