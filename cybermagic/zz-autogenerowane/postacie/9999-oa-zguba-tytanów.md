---
categories: profile
factions: 
owner: public
title: OA Zguba Tytanów
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201210-pocalunek-aspirii            | potężny krążownik Aurum pod dowództwem Sowińskich, wysłany na pomoc Inferni by uwolnić Anastazję od klątwy miłości. | 0111-03-26 - 0111-03-29 |
| 201230-pulapka-z-anastazji          | uszkodzony po kontakcie z Kryptą, więc wolniejszy pojazd; uderzył w Rodivas i przejął wszystkie anomalie i działania. Poza Anastazją ;-). | 0111-07-19 - 0111-07-20 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Nocna Krypta      | 2 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji)) |
| Arianna Verlen       | 2 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji)) |
| Eustachy Korkoran    | 2 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji)) |
| Klaudia Stryk        | 2 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji)) |
| OO Infernia          | 2 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji)) |
| AK Rodivas           | 1 | ((201230-pulapka-z-anastazji)) |
| AK Wyjec             | 1 | ((201210-pocalunek-aspirii)) |
| Anastazja Sowińska   | 1 | ((201210-pocalunek-aspirii)) |
| Anastazja Sowińska Dwa | 1 | ((201230-pulapka-z-anastazji)) |
| Damian Orion         | 1 | ((201230-pulapka-z-anastazji)) |
| Donald Parziarz      | 1 | ((201210-pocalunek-aspirii)) |
| Elena Verlen         | 1 | ((201230-pulapka-z-anastazji)) |
| Henryk Sowiński      | 1 | ((201230-pulapka-z-anastazji)) |
| Juliusz Sowiński     | 1 | ((201210-pocalunek-aspirii)) |
| Katra Igneus         | 1 | ((201210-pocalunek-aspirii)) |
| Martyn Hiwasser      | 1 | ((201230-pulapka-z-anastazji)) |