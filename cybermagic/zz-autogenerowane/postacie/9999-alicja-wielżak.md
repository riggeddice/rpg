---
categories: profile
factions: 
owner: public
title: Alicja Wielżak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181220-upiorny-servar               | detektyw, córka komendanta policji. Świetnie łączy fakty i jest mistrzowską obserwatorką. Odkryła znaczenie ducha i jak z nim walczyć. | 0109-10-28 - 0109-10-30 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181220-upiorny-servar               | wraz z Patrycją Karzec skupia się na rozwiązywaniu zagadek okultystycznych na terenie Przelotyka. Policjantka i reporter. | 0109-10-30

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksandra Garwen    | 1 | ((181220-upiorny-servar)) |
| Artur Śrubek         | 1 | ((181220-upiorny-servar)) |
| Beata Wielinek       | 1 | ((181220-upiorny-servar)) |
| Bogdan Daneb         | 1 | ((181220-upiorny-servar)) |
| Celina Szaczyr       | 1 | ((181220-upiorny-servar)) |
| Gabriel Krajczok     | 1 | ((181220-upiorny-servar)) |
| Jerzy Cieniż         | 1 | ((181220-upiorny-servar)) |
| Michał Wypras        | 1 | ((181220-upiorny-servar)) |
| Patrycja Karzec      | 1 | ((181220-upiorny-servar)) |
| Ryszard Januszewicz  | 1 | ((181220-upiorny-servar)) |
| Wojciech Tuczmowil   | 1 | ((181220-upiorny-servar)) |