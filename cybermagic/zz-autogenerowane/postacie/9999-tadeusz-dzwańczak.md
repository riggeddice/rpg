---
categories: profile
factions: 
owner: public
title: Tadeusz Dzwańczak
---

# {{ page.title }}


# Generated: 



## Fiszki


* ENCAO: -++0- | Analityczny, zorganizowany, introwertyczny, uparty;; Osoba skupiona na celach i wynikach | Bezpieczeństwo, Samorealizacja > Przyjemność | @ 230411-egzorcysta-z-sanktuarium
* MovieActor: Sherlock Holmes | @ 230411-egzorcysta-z-sanktuarium
* Job: Detektyw (kiedyś) i strażak (teraz) Sanktuarium Kazitan | @ 230411-egzorcysta-z-sanktuarium
* Deed: Gdy stawił czoła niebezpiecznemu przestępcy, ujął go dzięki swojej wiedzy i umiejętnościom analitycznym, zamiast ryzykować konfrontację fizyczną | @ 230411-egzorcysta-z-sanktuarium
* Deed: Kiedy znajomy poprosił go o radę w sprawie osobistej, zamiast udzielić wsparcia emocjonalnego, podszedł do sprawy racjonalnie i zaproponował logiczne rozwiązanie | @ 230411-egzorcysta-z-sanktuarium

### Wątki


odbudowa-rodu-kazitan

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230411-egzorcysta-z-sanktuarium     | uciekinier z Aurum i mieszkaniec Sanktuarium Kazitan; jako strażak próbował uratować dziecko przed Emisją która pochłonęła jego ojca (i przekształciło go w hybrydowego żywiołaka). Karolinus magią mu to uniemożliwił, potem sam uratował owo dziecko. Tadeusz zaprowadził Karolinusa do Irka, jedynego maga jaki został w Sanktuarium - i tego żałuje. | 0095-07-21 - 0095-07-23 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Arnold Kazitan       | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Elena Samszar        | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Irek Kraczownik      | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Karolinus Samszar    | 1 | ((230411-egzorcysta-z-sanktuarium)) |