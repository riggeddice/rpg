---
categories: profile
factions: 
owner: public
title: Nataniel Samszar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230523-romeo-dyskretny-instalator-supreme-missionforce | władający amnestykami czarny Samszar (BRG: refusal to slow), dogadał się z Albertem odnośnie utrzymania lokalnej bazy w Karmazynowym Świcie w ukryciu za to, że da Mai coś co jej się spodoba i przyda. Odzyskał kontrolę nad Techbunkrem Arvitas. | 0095-08-09 - 0095-08-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Albert Samszar       | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Elena Samszar        | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Karolinus Samszar    | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Maja Samszar         | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Romeo Verlen         | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |