---
categories: profile
factions: 
owner: public
title: Ewelina Paroknis
---

# {{ page.title }}


# Generated: 



## Fiszki


* nastolatka pragnąca iść w stronę astrofizyki i badań anomalii. Jej matka nie żyje, ojcem jest Jonasz. 16 | @ 230329-zdrada-rozrywajaca-arkologie
* ENCAO:  ++0-0 |Zrzędliwa;;Ulega sile czy charyzmie;;Idealistyczna| VALS: Self-direction, Universalism >> Power| DRIVE: To JA wygram | @ 230329-zdrada-rozrywajaca-arkologie
* nastoletnia drama queen, diva, niezrozumiana i lashing out, KOCHA KALIĘ AWITER <3<3<3 | @ 230329-zdrada-rozrywajaca-arkologie

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230329-zdrada-rozrywajaca-arkologie | macocha Iwona czytała jej pamiętnik a ojciec nie reagował, więc zrobiła entry jak to widziała jak Jonasz zdradzał Iwonę z Kasią. To spowodowało straszną kaskadę. By to ukryć, z pomocą Feliksa zrobiła "że była szantażowana". Gdy Amelia z Inferni przypadkowo upubliczniła jej pamiętnik całej arkologii (a Ewelina ma cięty język o ludziach), próbowała popełnić samobójstwo. Ale uratował ją jakiś mag Interis. Oddana pod pieczę Kalii Awiter przez Ardillę; trzeba jej pomóc XD. | 0093-03-14 - 0093-03-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Ardilla Korkoran     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| BIA Prometeus        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Celina Lertys        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Eustachy Korkoran    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Franciszek Pietraszczyk | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kalia Awiter         | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Małgorzata Maratelus | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| OO Infernia          | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Rafał Kidiron        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Ralf Tapszecz        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Wojciech Grzebawron  | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |