---
categories: profile
factions: 
owner: public
title: Dawid Nałęcznik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230831-potwory-ktore-przetrwaly-eter | Skażony Miragent, 'mimik' (lekarz); po wyczyszczeniu przez Dianę dowiedział się od Braciszka czym był i jakie miał relacje z Dianą (która jest na stałym połączeniu z jego głową). Przekradł się z dzieckiem do Damnos, udał poprzedniego komendanta (Stefana) i doprowadził do sojuszu między wszystkimi stronami. | 0093-03-11 - 0093-03-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Mardiblon     | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Bartek Mardiblon     | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Diana Nałęcznik      | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Esaria Mirtalis      | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Karol La Viris       | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Mateusz Owisiec      | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Rosenkrat Amiribasit | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |