---
categories: profile
factions: 
owner: public
title: Aurus Czarmadon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220512-influencerskie-mikotki-z-talio | manager mikotek; po śmierci ojca Mireli ją zaadoptował i chce zrobić z niej gwiazdę. Dał jej i Kamilowi wszystko. Mirela napuściła nań potwora (kiedyś: ojca) i tak skończyła się kariera managera... KIA. | 0104-05-02 - 0104-05-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antonina Terkin      | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Elwira Piscernik     | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Jacek Ożgor          | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Kamil Czarmadon      | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Kara Prazdnik        | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Mirella Czarmadon    | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Tytus Ramkon         | 1 | ((220512-influencerskie-mikotki-z-talio)) |