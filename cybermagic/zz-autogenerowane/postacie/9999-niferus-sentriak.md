---
categories: profile
factions: 
owner: public
title: Niferus Sentriak
---

# {{ page.title }}


# Generated: 



## Fiszki


* (ENCAO:  -0++0 |Wszystko rozwiążę teorią;;Przedsiębiorczy i pomysłowa| VALS: Achievement, Face| DRIVE: Legacy of helping) | @ 230113-ros-adrienne-a-new-recruit
* klarkartianin: "przeszłość nie ma znaczenia, ważne co możemy zrobić" | @ 230113-ros-adrienne-a-new-recruit
* kadencja: Chris Voss 'late night DJ voice' | @ 230113-ros-adrienne-a-new-recruit
* "Wszystko rozwiążemy, to kwestia czasu" | @ 230113-ros-adrienne-a-new-recruit

### Wątki


ratownicy-ostatniej-szansy

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230113-ros-adrienne-a-new-recruit   | wants to give Adrienne a chance the most of all; afterwards expertly fights real fire on a derelict. | 0083-12-07 - 0083-12-10 |
| 230117-ros-wiertloplyw-i-tai-mirris | dostarczył potrzebnej wiedzy odnośnie tego jak działają noktiańskie jednostki pasożytnicze. | 0084-02-22 - 0084-02-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Napoleon Myszogłów   | 2 | ((230113-ros-adrienne-a-new-recruit; 230117-ros-wiertloplyw-i-tai-mirris)) |
| Talia Irris          | 2 | ((230113-ros-adrienne-a-new-recruit; 230117-ros-wiertloplyw-i-tai-mirris)) |
| Tristan Andrait      | 2 | ((230113-ros-adrienne-a-new-recruit; 230117-ros-wiertloplyw-i-tai-mirris)) |
| Adrianna Kastir      | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Aster Sarvinn        | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Eleonora Alintirias  | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Hel Otereien         | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Kasia Karmnik        | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Lucas Septrien       | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Łucja Neiser         | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Łucja Nirimis        | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| OR Błyskawica        | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| SC Wiertłopływ       | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| TAI Mirris d'Paravilius | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |