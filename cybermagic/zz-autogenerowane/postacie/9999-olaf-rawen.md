---
categories: profile
factions: 
owner: public
title: Olaf Rawen
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240125-wszystkie-sekrety-caralei    | kapitan Ferrelith; przemyca Verminusy na Stację Saltomak. Po pobiciu przez ludzi Darbika przyznał się do przemytu verminusów i podał pełną listę kanałów kontaktowych, ku utrapieniu Darbika (acz zadowoleniu Bladawira) | 0110-12-29 - 0111-01-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Bladawir      | 1 | ((240125-wszystkie-sekrety-caralei)) |
| Dobromir Misiak      | 1 | ((240125-wszystkie-sekrety-caralei)) |
| Kazimierz Darbik     | 1 | ((240125-wszystkie-sekrety-caralei)) |
| Tog Worniacz         | 1 | ((240125-wszystkie-sekrety-caralei)) |