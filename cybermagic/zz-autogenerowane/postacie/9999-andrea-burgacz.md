---
categories: profile
factions: 
owner: public
title: Andrea Burgacz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210112-elainka-kontra-caly-swiat    | pomniejsza 19-letnia arystokratka Aurum, która miała kontrolować sentisieć na Ogienku; skończyło się jej ciężką infekcją i zamknięciu w biovat. Uratowana przez załogę Granoli. | 0097-09-29 - 0097-10-02 |
| 210209-wolna-tai-na-k1              | BWG; poświęciła wszystko by zniszczyć AI Lorda który zabrał jej nową przyjaciółkę, Neutropię. Ekspert od killware. Rzuciła wszystko na Rziezę, po czym władowała Neitę w swoje implanty. | 0111-08-30 - 0111-09-04 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210112-elainka-kontra-caly-swiat    | straciła Elainkę (przyjaciółkę) i wymaga implantacji po strasznych wydarzeniach z sentisiecią na Ogienku. Wklejono w nią sporo części z ludzkich zwłok co uszkodziło jej wzór. | 0097-10-02

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adalbert Brześniak   | 1 | ((210209-wolna-tai-na-k1)) |
| Janusz Parzydeł      | 1 | ((210209-wolna-tai-na-k1)) |
| Klaudia Stryk        | 1 | ((210209-wolna-tai-na-k1)) |
| Leona Astrienko      | 1 | ((210209-wolna-tai-na-k1)) |
| Martyn Hiwasser      | 1 | ((210209-wolna-tai-na-k1)) |
| Miki Katasair        | 1 | ((210209-wolna-tai-na-k1)) |
| OA Ogienko           | 1 | ((210112-elainka-kontra-caly-swiat)) |
| SCA Granola          | 1 | ((210112-elainka-kontra-caly-swiat)) |
| TAI Neita Lairossa   | 1 | ((210209-wolna-tai-na-k1)) |
| TAI Rzieza d'K1      | 1 | ((210209-wolna-tai-na-k1)) |