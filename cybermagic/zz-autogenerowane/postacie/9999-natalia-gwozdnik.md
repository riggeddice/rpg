---
categories: profile
factions: 
owner: public
title: Natalia Gwozdnik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231011-ekstaflos-na-tezifeng        | podczas działań celnych napotkał na kralotha, zniknęła. Jej los jest nieznany. Podobno bardzo obiecująca tienka na Orbiterze. | 0110-10-20 - 0110-10-22 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 231025-spiew-nielalki-na-castigatorze | dzięki Ariannie została bohaterką, symbolem tego, że tien musi móc sobie poradzić w różnych sytuacjach i być maksymalnie kompetentną. | 0110-10-26

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Elena Verlen         | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Eustachy Korkoran    | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Klaudiusz Terienak   | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Lars Kidironus       | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Leona Astrienko      | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Leszek Kurzmin       | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OO Castigator        | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OO Infernia          | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OO Tezifeng          | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Raoul Lavanis        | 1 | ((231011-ekstaflos-na-tezifeng)) |