---
categories: profile
factions: 
owner: public
title: Arianna Verlen
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "orbiter, oddział verlen"
* owner: "public"
* title: "Arianna Verlen"


## Kim jest

### Paradoksalny Koncept

Arcymag. Komodor Orbitera próbująca ratować wszystkich nawet kosztem swego zdrowia, w praktyce wyrządzająca sporo szkód sojusznikom. Świetna manipulatorka, zgrabnie porusza się po polityce i jest doskonałym taktykiem. Optymistka, wierząca w ludzkość jako ideę - i nie znosząca tysięcy niepotrzebnych frakcji. Dla uratowania członków swojej załogi czy dla jedności świata nie ma skrupułów by poświęcić dziesiątki innych osób.

### Motto

"By Federacja Ludzkich Planet mogła zaistnieć, potrzebny jest symbol - ktoś, komu zależy, dba o jedność, chroni i ratuje."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Manipulatorka i taktyk. Arianna potrafi zawsze zaleźć drugiej osobie za skórę - lub przewidzieć co druga osoba zrobi. Skuteczna w polityce i na polu bitwy.
* ATUT: Bardzo potężna czarodziejka; ze wsparciem załogi osiąga poziom Arcymaga. Arianna jest bardzo odporna na magię i włada potężną magią technomantyczną i kinetyczną.
* SŁABA: Arogancka i skłonna do ryzyka. Przecenia swoją moc i swoje umiejętności - idzie o krok za daleko, nie wycofuje się, zwykle raczej eskaluje - za daleko.
* SŁABA: Tak bardzo się martwi, że komuś na kim jej zależy stanie się krzywda, że pozycjonuje się na pierwszej linii ognia; próbuje prowadzić z frontu nawet, jak to bez wartości.

### O co walczy (3)

* ZA: Federacja ludzkich planet, chroniona przed Anomaliami przez Zjednoczone Siły Orbitera. Ze zdrowymi szlakami handlowymi i prosperującym światem.
* ZA: Zjednoczenie WSZYSTKICH ludzi i magów. Arianna NAPRAWDĘ chce ratować wszystkich. Noktianie, astorianie, Skażeni... to wszystko to nasza rasa, nasza przyszłość.
* ZA: Chce stać się symbolem, chce być czymś więcej niż tylko czarodziejką. Chce być symbolem jedności i bezpieczeństwa. Arcymagiem i awatarem.
* VS: Głupie dziesiątki frakcji w Orbiterze. Zamiast współpracować dla jednego celu porozdzieraliby się nawzajem. Arianna zatem ich zniszczy politycznie lub magicznie.
* VS: Nie pozwoli NIGDY komukolwiek się pokonać. Nie będzie nigdy upokorzona. Nie odda nikomu pokłonów. Nie utraci twarzy ani godności.
* VS: Aktywnie przeciwdziała zarówno próbom ogromnej centralizacji władzy ("monarchia, imperium") jak i za dużemu rozbiciu ("państwa-miasta"). To nie zadziała.

### Znaczące Czyny (3)

* Naraziła Infernię na straszliwe uszkodzenia i prawie zniszczenie - ale magią rozerwała osłony Anomalii Kosmicznej Leotis, by Infernia mogła wystrzelić.
* Wymanewrowała komodora Ogryza idąc przez plotki, przez jego żonę itp - by potem dać mu rozwiązanie problemu jaki stworzyła i zostać jego sojuszniczką.
* Zaufała Hiwasserowi i rozkazała sabotować Miecz Światła, który ich uratował - by tylko Miecz nie został pożarty przez Anomalię Kosmiczną Serenit.

### Kluczowe Zasoby (3)

* COŚ: Amplifikatory Eteryczne. Arianna jest w stanie użyć ich by jeszcze bardziej wzmocnić swoją magię, skierować swoją wolę i osiągnąć jeszcze więcej.
* KTOŚ: Ma pozycję komodora Orbitera, z czym wiąże się władza i uprawnienia, zarówno wśród magów jak i TAI.
* WIEM: Wie, w jaki sposób może wezwać niebezpieczną Anomalię - Nocną Kryptę - i jak przewidzieć jej zachowania.
* OPINIA: Pomocna, ale bezwzględna. Zadrzyj z jej załogą a ona Cię po prostu zniszczy. Nierozważnie działa z pierwszej linii. Nie umrze śmiercią naturalną.

## Inne

### Wygląd

.

### Coś Więcej

.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210224-sentiobsesja                 | 16 lat, zwana "Aria". Zaprzyjaźniła się z żołnierzami i najczęściej wykorzystuje swoje umiejętności retoryczne i to, że jest słodka do osiągania celów. Przekonała Apolla do zwalczenia sentiobsesji a potem oddała Apolla Milenie by odzyskać oddział ;-). | 0092-10-01 - 0092-10-04 |
| 230412-dzwiedzie-poluja-na-ser      | dyplomacja wewnątrzverlenowa między Marcinozaurem i Apollo, łagodzenie potencjalnych problemów i docelowo świetny rajd ścigaczem by pozyskać ser od Uli nie walcząc z jej potworami. | 0095-07-12 - 0095-07-14 |
| 230419-karolinka-nieokielznana-swinka | po chwili zabawiania Karolinki przejęła pilotaż vana jak już Elena go rozbiła. Zamiast iść w powagę, poszła w 'pomóżmy Samszarom i naprawmy im potwora XD'. Dobre serce i klej społeczny zespołu. | 0095-07-19 - 0095-07-23 |
| 230426-mscizab-zabojca-arachnoziemskich-jaszczurow | wyczuła ukrywane Esuriit w Arachnoziem; gdy Mściząb walczył z Vioriką, attunowała się do sentisieci i poznała prawdę co się tu dzieje. Odkryła obecność Emmanuelle. "Dobry glina" gadając z Emmanuelle. | 0095-08-03 - 0095-08-05 |
| 230524-ekologia-jaszczurow-w-arachnoziemie | optymistycznie weszła do domu martwego nastolatka i nie tylko uniknęła śluzowca ale też złapała jego próbki, potem wezwała Ulę Blakenbauer na pomoc. Krzyżuje dane z kamer by pomóc Viorice znaleźć drugiego mimika. | 0095-08-06 - 0095-08-09 |
| 210311-studenci-u-verlenow          | 20 lat, uspokoiła zdruzgotaną zabiciem ludzi Elenę; do tego rozwaliła dwa ścigacze. Magicznie odkryła, że za wszystkim stał Dariusz Blakenbauer i dziwne lustro - że to nie sama Elena utraciła kontrolę nad mocą. | 0097-01-16 - 0097-01-22 |
| 210324-lustrzane-odbicie-eleny      | stoi za zmianą nazwy miejscowości na Skałkowa Myśl. Powiązała lustra z Eleną, po czym zagadywała ją i przekonywała, by Elena nie zrobiła niczego złego ani głupiego. De facto, jej umiejętności gadania sprawiły, że Elena ze spokojem weszła w pułapkę by Czapurt mógł ją unieszkodliwić. Plus: zaszokowana relacjami Vioriki i Lucjusza. | 0097-09-02 - 0097-09-05 |
| 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly | 22-23 lata; została kapitan Królowej Kosmicznej Chwały (jednostki 'Aurum' pod kontrolą Orbitera). Dała szansę poprzedniej kapitan (Alezji) mimo, że ta jest w beznadziejnym stanie i pokonała marine. Wprowadziła dyskomfort na Królowej i pokazała innym oficerom, że Siły Specjalne Orbitera sabotują Królową by program kosmiczny Aurum się nie mógł odbyć. | 0100-05-06 - 0100-05-12 |
| 220928-kapitan-verlen-i-pojedynek-z-marine | zatrzymała praktykę biczowania załogi (wprowadzoną przez Aurum) i stoczyła pojedynek z Szymonem Wanadem, psychopatycznym marine. Pokonała go i oswoiła. Przestała jeść w kantynie XD. | 0100-05-14 - 0100-05-16 |
| 221012-kapitan-verlen-i-niezapowiedziana-inspekcja | przeszła na konserwy i spartańskie żarcie póki jest na Królowej; stoczyła starcie z marine i wybroniła się przed inspektorem Królowej nie zdradzając że jej oficerowie ją wrobili. Odzyskuje kontrolę nad Królową i zaczyna zaskarbiać sobie zaufanie wielu grup (seilici, sarderyci, marines, dwóch oficerów) | 0100-05-17 - 0100-05-21 |
| 221019-kapitan-verlen-i-pierwszy-ruch-statku | zneutralizowała Maję Samszar ryzykiem konserw, rozpuściła plotki na swój temat (że kontratakuje i jej nie podpadać), zaczęła ćwiczenia z Królową i jak statek ruszył to zaczęła się panika. Arianna odwołała ćwiczenia... by przeszkolić ludzi w niepanikowaniu i działaniu sensownym. | 0100-05-23 - 0100-06-04 |
| 221026-kapitan-verlen-i-koniec-przygody-na-krolowej | doprowadziła załogę do wiary w swoje umiejętności i Królową; kazała zatruć nielegalny alkohol by uniknąć konfrontacji ale wyjść na swoim. Przekonała Ruppoka, by ten dał szansę jej oraz Królowej oraz znalazła sposób jak odzyskać surowce - handel przez Tucznika Trzeciego z planetą i Nonarionem. | 0100-06-08 - 0100-06-15 |
| 221102-astralna-flara-i-porwanie-na-karsztarinie | dowodzi Astralną Flarą; chce zintegrować załogę - jedzenie dla Mai, ćwiczenia dla Eleny, opieprz dla Władawca itp. Przeprowadziła operację 'tracimy kontrolę nad Flarą' i jako pilot przeleciała do ukrytej korwety. | 0100-07-28 - 0100-08-03 |
| 221116-astralna-flara-dociera-do-nonariona-nadziei | ma neutralny stosunek do noktian - Verlenowie tępili terrorformy Saitaera; przyjęła, że Władawiec pomaga w treningu Elenie, rozpaliła morale ludzi (przypadkiem rozogniła plotki o Egzotycznych Pięknościach) | 0100-09-08 - 0100-09-11 |
| 221123-egzotyczna-pieknosc-na-astralnej-flarze | wprowadziła Ellarinę na Astralną Flarę i przesunęła ją na 'maskotkę / morale'. Współpracuje z komodorem i próbuje deeskalować jego gorsze pomysły (tępienie faerilskich handlarzy niewolnikami, Ograniczanie TAI Mirtaeli). Wygrywa trochę, przegrywa trochę. Zarządza Flarą. Miękko dowodzi jednostką korzystając z zaufania zespołu. | 0100-09-12 - 0100-09-15 |
| 221130-astralna-flara-w-strefie-duchow | pozyskała informacje o tym, że coś może wpływać na TAI i może Strefa Duchów to noktianie. Stworzyła nekroTAI Zarraleę wywołując grozę w części załogi. Kazała przechwycić jeńca (Kireę) i zatrzymała Lodowca przed okrutnym traktowaniem savaranki. Przesłuchała ją wyjątkowo delikatnie i poznała prawdę o Strefie Duchów. Skuteczna w przekonywaniu Lodowca do zachowywania się "dobrze" a nie "maksymalnie skutecznie". | 0100-10-05 - 0100-10-08 |
| 221214-astralna-flara-kontra-domina-lucis | dzieli się wszystkimi informacjami od Kirei z Lodowcem; wszystko planuje nie pod kątem honoru czy bezpieczeństwa a uratować jak najwięcej istnień ludzkich. Wkręca Lodowca w mowę mającą doprowadzić do poddania się noktian lub samobójstw noktian, ale by minimalna ilość ludzi ucierpiała. Humanitarna socjopatka - nie chce niczyjego cierpienia, ale ratowanie istnień przede wszystkim. | 0100-10-09 - 0100-10-11 |
| 221221-astralna-flara-i-nowy-komodor | gasiła pożar po stworzeniu Zarralei między sobą i Eleną; nawet medal dostała. Potem poszukała dane na temat nowego komodora (Bolzy). Po zapoznaniu się z Bolzą dała szansę załodze na wycofanie się z misji, ale oni Ariannie wierzą. Chciała wykurzyć squatterów z Kazmirian, ale pod wpływem Bolzy zmieniła plan na sojusz. Niestety, poznawszy squatterów - musiała wstawić się za nich u Bolzy. | 0100-11-07 - 0100-11-10 |
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | ku jej wielkiemu zdziwieniu przerażenie młodych infiltratorów przez jej medyka doprowadziło ją do tego że racjonalna Grażyna jest ekspertem od duchów. Kazała zdobyć Adragaina i używając wiedzy Grażyny wypaliła pilota Nox Ignis (Sabrinę). | 0100-11-13 - 0100-11-16 |
| 191025-nocna-krypta-i-bohaterka     | oficer Orbitera, bohaterka, która wielokrotnie uratowała wiele statków i stacji. Podjęła się misji uratowania ludzi z Nocnej Krypty; przeszła przez fazę Arcymaga i prawie straciła życie i osobowość, ale udało jej się osiągnąć ten sukces dzięki swojej załodze. Dowodzi statkiem Orbitera "Perła Nadziei". | 0108-05-06 - 0108-05-08 |
| 200106-infernia-martyn-hiwasser     | kapitan Inferni. Kiedyś bohaterka, teraz skompromitowana. Uratowała Hiwassera zgrabnie nawigując polityką Orbitera i ufając swojej kompetentnej załodze. | 0110-06-28 - 0110-07-03 |
| 200122-kiepski-altruistyczny-terroryzm | dowódca Inferni, zapewniła efektowne zniszczenie "roboczego statku" by Persefona 2 na Królowej Chmur była przekonana o wykonaniu zadania. | 0110-09-15 - 0110-09-17 |
| 200408-o-psach-i-krysztalach        | by zatrzymać krystaliczny Leotis postawiła na szalę bazę Samojed oraz Infernię - poniosła straszliwe straty, ale zniszczyła anomalny statek kosmiczny. | 0110-09-29 - 0110-10-04 |
| 200415-sabotaz-miecza-swiatla       | zaufała Martynowi; wpierw sabotowała Miecz Światła Eustachym i wyprowadziła z równowagi Mateusza Sowińskiego. W ten sposób uszkodziła Miecz, ale uratowała go przed lotem na Serenit. | 0110-10-08 - 0110-10-10 |
| 200624-ratujmy-castigator           | ma świetne plany jak prowokować arystokratów na Castigatorze, ale nie przewidziała brutalnej skuteczności Leony. Potem używając magii powstrzymywała moc Rozalii z Alkarisa. | 0110-10-15 - 0110-10-19 |
| 231011-ekstaflos-na-tezifeng        | z przyjemnością odbudowała znajomość z Kurzminem, martwi się izolacją Eleny i pomoże Kurzminowi z dwoma zaginionymi tienami. Gdy się okazało, że coś dziwnego się dzieje na Tezifeng, nie weszła na pokład 'po prostu' tylko weszła z dywersją. Kazała wycofać oddział po złapaniu jeńca a potem odwróciła uwagę ekstaflorisa, by Eustachy unieszkodliwił Tezifeng. Świetnie rozegrana akcja. | 0110-10-20 - 0110-10-22 |
| 231025-spiew-nielalki-na-castigatorze | korzystając z kryzysu Altarient na Castigatorze ustabilizowała tienów; niech zaczną się zachowywać jak Natalia. Prawidłowo deleguje - Leonę do Eustachego, potem Raoula do rozwiązania problemu Anomalii i wydaje mu precyzyjne rozkazy dzięki czemu osiągnął sukces. Podczas samego kryzysu z NieLalką ściśle wspiera Kurzmina i dowodzi izolacją kluczowych kompententów Castigatora. | 0110-10-24 - 0110-10-26 |
| 231109-komodor-bladawir-i-korona-woltaren | zadowala Bladawira (potrójną grzywną) jak i ratuje Koronę ('proście o ratunek'). Nie chce mieć wroga w Bladawirze. Gdy Elena wykryła coś na 'Koronie', Arianna potraktowała to poważnie. Zauważa, że Lidia jest smutna i nie ma z nią kotów - WIDZI że coś jest nie tak. Gdy pojawia się terrorform ixioński, rozpętuje pełną magię by tylko kupić czas. Dark-Awakened Anetę Woltaren, która wyrwała Prefektissa z siebie i dowodziła ewakuacją jednostki. BARDZO bliska śmierci na tej akcji. | 0110-10-27 - 0110-11-03 |
| 231115-cwiczenia-komodora-bladawira | dzięki blefowaniu, przymilaniu się i danym Klaudii zadowoliła komodora Bladawira który powiedział jej swój plan. Poprosiła Kurzmina by pomógł zneutralizować zniszczenie komodor Razalis. Była więcej niż tylko dywersją w rękach Bladawira. | 0110-11-08 - 0110-11-15 |
| 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira | odmówiła Patrykowi ćwiczeń, by Zaara za jej plecami zagroziła Patrykowi. Gdy dowiedziała się o _pain amplifiers_, poprosiła Elenę o subtelny sabotaż. Nie dała się wciągnąć w mroczną grę Bladawira (kompetencja lub przyjaźń) - wygrała wszystko. Do tego próbowała dowiedzieć się jak najwięcej o Zaarze rękami Kamila i stabilizowała morale Inferni przy niemożliwych ćwiczeniach. | 0110-11-26 - 0110-11-30 |
| 231220-bladawir-kontra-przemyt-tienow | Gdy Bladawir kazał szybko badać statki pod kątem celnym, podejrzewała coś dziwnego. Zgłosiła mniejszy wymiar przemytu by osłonić część osób. Skutecznie zarządzała zmęczeniem załogi i używając Krwi poprosiła Elenę o informacje na temat Karsztarina. Mimo że prawie została zneutralizowana, udało jej się wyprowadzić przemytników tienów w kosmos poza szpony Bladawira. On nadal nic nie wie. | 0110-12-06 - 0110-12-11 |
| 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow | została pod Bladawirem by go docelowo zranić; skłoniła Zaarę, by ta zahintowała że szukają efektów mentalnych. Gdy Bladawir dostarczył jej zwłoki, zrobiła Dark Awakening. Wykonuje rozkazy Bladawira, ale postawiła mu się w kwestii ukarania Klaudii. Uważając, że Bladawir nie może mieć groźnego potwora, zrobiła donos do Wydziału Wewnętrznego Orbitera i tam go bardzo ograniczyli. | 0110-12-03 - 0110-12-17 |
| 240110-wieczna-wojna-bladawira      | poprosiła Władawca, by dołączył do Inferni i uwiódł dla niej Zaarę, przekonała Bladawira że reanimacja kralotha to fatalny pomysł, zdjęła Zaarę z głowy Klaudii i na Skażonej kralotycznie jednostce poznała sekrety kralotha jako wiodący mag w kwartecie, po czym wstrzyknęła w Zaarę adaptogen kralotyczny by ją naprawić po poznaniu jej sekretów. Czuje się brudna, ale wreszcie poznała sekret Bladawira i Zaary. | 0110-12-24 - 0110-12-27 |
| 200708-problematyczna-elena         | przekonała Salamin, że jego czas się już skończył. Po przyjęciu Eleny przekazała ją Eustachemu, co prawie skończyło się zniszczeniem Inferni (gdyby nie Persefona). | 0110-12-27 - 0111-01-01 |
| 240124-mikiptur-zemsta-woltaren     | skoordynowała obronę przed Dewastatorem, skupiła się na ratunku Isratazir ponad polowaniem na Mikiptur. Potem neutralizowała polityczny fallout by chronić Bladawira i ujawniła obecność Syndykatu. | 0110-12-29 - 0111-01-01 |
| 200715-sabotaz-netrahiny            | przechwyciła (nieświadomie) chwałę z czynów Eustachego i Eleny; stanęła po stronie Persefony. Jej rolą tu była koordynacja załogi. | 0111-01-04 - 0111-01-07 |
| 240131-anomalna-mavidiz             | negocjując z Bladawirem zrzuciła część uwagi na Klaudię ('ta mądra'); utrzymała nerwy na wodzy gdy załoga Mavidiz okazała się być 'dziwna'. Stworzyła barierę ogniową przeciw nihilosektowi i ufortyfikowała miejsce przed potwornym insektem. | 0111-01-03 - 0111-01-08 |
| 200722-wielki-kosmiczny-romans      | jej moc jest tak duża, że zdusiła emisję Esuriit Eleny. Wplątana w romans którego nie miała, opieprzona przez Kramera, dała radę ochronić reputację - kosztem Eleny. | 0111-01-10 - 0111-01-13 |
| 200729-nocna-krypta-i-emulatorka    | wezwała Kryptę nie robiąc nikomu nadmiernej krzywdy, obiecała grupie ludzi że ich uratuje - ale widząc koszmar Laury d'Esuriit podjęła decyzję o ewakuacji z Krypty i zniszczenie tego statku używając Castigatora. | 0111-01-14 - 0111-01-20 |
| 200819-sekrety-orbitera-historia-prawdziwa | by Elena była wolna od Tadeusza, zrobiła pojedynek między Tadeuszem i Leoną. Potem dowiedziała się o Simulacrum. Zadziałała z tła, wszystko pozałatwiała - Oriona, korekcję Leony, truciznę Martyna - a Elena myśli, że nic nie zrobiła i udało się tylko dzięki Eustachemu... | 0111-01-24 - 0111-02-01 |
| 200826-nienawisc-do-swin            | nie ona zaczęła bunt, ale ona go musiała zakończyć - od negocjacji z adm Termią, przez manipulację Tadeuszem Ursusem do wielkiej przemowy do buntowników. | 0111-02-05 - 0111-02-11 |
| 200909-arystokratka-w-ladowni-na-swinie | pilot Tucznika; przy akcji ratunkowej udało jej się utrzymać Martyna przy życiu, acz Tucznik poobijany. Dużo czarowała mocą arcymaga; niestety, sprzęgła się z Anastazją. Zdecydowanie nie jej dzień. Ale - pozyskała Izabelę jako wsparcie Inferni (Tucznika). | 0111-02-12 - 0111-02-17 |
| 200916-smierc-raju                  | lata pomiędzy Anastazją a Garwenem; udało jej się zaplanować jak odepchnąć nojrepy z Raju. Niestety, nojrepy były dziwne; Raj został zniszczony. | 0111-02-18 - 0111-02-20 |
| 200923-magiczna-burza-w-raju        | próbuje koordynować akcję humanitarną ratującą Raj (m.in używając Izabeli i Anastazji) i zabezpieczyć Raj magicznie przed burzą magiczną. Z sukcesem. | 0111-02-21 - 0111-02-25 |
| 201014-krystaliczny-gniew-elizy     | wpierw teleportowała się by uratować Elenę przed zabójcami Anastazji, potem stała naprzeciw Elizy Iry i próbowała sprawę załagodzić. Ciężki okres. | 0111-03-02 - 0111-03-05 |
| 201021-noktianie-rodu-arlacz        | zatrzymała Juliusza przed zniszczeniem Tesseraktu Elizy, włączyła Anastazję do działania, negocjowała z Arłaczami uwolnienie noktian a potem z Elizą opuszczenie Raju. | 0111-03-07 - 0111-03-10 |
| 201104-sabotaz-swini                | po tym, jak Klaudia zlokalizowała miragenta udało jej się go wyłączyć. Też: próbowała mocą złamać Anomalicznego Wieprzka - bez powodzenia. Też: nie odparła mindwarpa Ataienne. | 0111-03-13 - 0111-03-16 |
| 201118-anastazja-bohaterka          | chciała pomóc Eustachemu wygrać pojedynek nieuczciwie, skończyło się na Paradoksie Miłości i ewakuacji z Kontrolera... ale wyleczyła magów na Inferni wspierana Pryzmatem dzięki Kamilowi. | 0111-03-22 - 0111-03-25 |
| 201210-pocalunek-aspirii            | nawigowała polityczne wody między rodem Verlen a Juliuszem Sowińskim odnośnie Anastazji. Wezwała Kryptę by naprawić AK "Wyjec". | 0111-03-26 - 0111-03-29 |
| 201216-krypta-i-wyjec               | pozostawiła Anastazję Sowińską w Nocnej Krypcie; nikt poza nią nie wie, że to była jej decyzja. Disruptowała anioła Krypty i zobaczyła jego prawdziwe oblicze. | 0111-03-29 - 0111-03-31 |
| 201125-wyscig-jako-randka           | by poznać sekret Olgierda, przez komedię pomyłek wpadła jako "ta co nie wie jak zagadać". Wygrała z Olgierdem pojedynek na ścigacze. | 0111-04-08 - 0111-04-13 |
| 200610-ixiacka-wersja-malictrix     | inteligentnie uzyskała od Kramera OO 'Pandora' i wzmocnienie detektorów Inferni; potem wynegocjowała, że Malictrix będzie współpracować. I przekazała stację Telira-Melusit VII w ręce Orbitera, dając Kramerowi niemały sukces zwłaszcza w linii kryształów ixiackich. | 0111-05-01 - 0111-05-05 |
| 210707-po-drugiej-stronie-bramy     | wpierw magią szukała i odnalazła echo Klaudii, a potem magią ściągnęła Klaudię do hipernetu w archaicznej bazie noktiańskiej. | 0111-05-11 - 0111-05-13 |
| 210714-baza-zona-tres               | dostarczyła informacji maskujących Infernię jako Alivię Nocturnę oraz negocjowała z Bią, oszukując ją o pokoju Noctis - Astoria. Próbuje zapewnić przetrwanie wszystkim. | 0111-05-14 - 0111-05-16 |
| 210721-pierwsza-bia-mag             | kłamanie BIA odnośnie tego że są Alivią Nocturną przestało działać, więc musiała użyć prawdy. Przekonała BIA, że Martyn może uratować ją i Klaudię. | 0111-05-17 - 0111-05-19 |
| 210728-w-cieniu-nocnej-krypty       | jako pilot Inferni manewrowała trzymając odległość od Krypty w nierzeczywistości; potem unikając statków nieznanej flotylli a na końcu - wleciała w wiązkę strumieniowego działa Krypty by wrócić do domu. | 0111-05-20 - 0111-06-06 |
| 210804-infernia-jest-nasza          | dowiedziała się od Termii kto stoi za odbieraniem jej Inferni, po czym zmieniła tien Żaranda w swojego kultystę i puryfikowała aparycję Diany Arłacz na Inferni. | 0111-06-21 - 0111-06-24 |
| 201111-mychainee-na-netrahinie      | przeprowadziła operację 'ratujemy Netrahinę, ponownie' i zapewniła wszelkie potrzebne środki i pomoc Komczirpa. | 0111-07-05 - 0111-07-08 |
| 201230-pulapka-z-anastazji          | jako arcymag, wyczuła, że Anastazja nie jest "prawdziwa" a jest konstruktem Krypty. Magicznie wzmocniła Infernię, by Elena jej nie zepsuła. Potem kupiła czas zajęcia się Anastazją. | 0111-07-19 - 0111-07-20 |
| 210127-porwanie-anastazji-z-odkupienia | podjęła decyzję o odbiciu sztucznej Anastazji tak, by jej załoga nie ucierpiała. Pozwala Dianie i Klaudii bawić się pretorianami, choć szkoda jej trochę relacji z Eleną. Czuje sentisieć na Odkupieniu i popchnęła Anastazję do zrobienia tego, co konieczne - połączenia z tą siecią. | 0111-07-22 - 0111-07-23 |
| 210106-sos-z-haremu                 | połączyła sie z obcą sentisiecią by ją obudzić i zmusić do odzyskania działania; też: opracowała wysokopoziomowy plan "arystokratki na orgię". | 0111-07-26 - 0111-07-27 |
| 210120-sympozjum-zniszczenia        | Maria Naavas wpakowała ją z Olgierdem na "randkę" w kapsule ratunkowej z zimnym life supportem. Użyła magii do ogrzania a jak nie miała sił, Olgierd przejął i trzymał energię. | 0111-08-01 - 0111-08-05 |
| 210210-milosc-w-rodzie-verlen       | jest szanowana i kochana w rodzie Verlen; chcą dla niej walczyć z Sowińskimi. Lekko skłania Elenę, by nie odcięła się od rodu (+sensowny pojedynek) a potem efektownie ratuje Romeo mocą arcymaga. | 0111-08-21 - 0111-08-24 |
| 210331-elena-z-rodu-verlen          | doprowadziła do tego że Romeo wyznał miłość Elenie by ją zmiękkczyć i wpierw Elenę rozkleiła, sprawiła że ta przeprosiła Blakenbauerów a nawet Elena wróciła do rodu. Mistrzostwo aranżacji sytuacji. | 0111-08-27 - 0111-08-30 |
| 210218-infernia-jako-goldarion      | udaje pilota Goldariona; zdziwiła się, że podobno nie istnieje. Przypadkowo rozkochała w sobie jako "Monika" Tomasza Sowińskiego, pewnego, że Arianna nie istnieje. | 0111-09-16 - 0111-10-01 |
| 200429-porwanie-cywila-z-kokitii    | postawiła się Medei w sprawie eksterminacji (i przeszła test) oraz zmusiła Medeę do wzięcia jej oficerów na pokład Niobe - do misji specjalnych. | 0111-10-01 - 0111-10-06 |
| 210317-arianna-podbija-asimear      | złamała konspirację by porwać Jolantę Sowińską i jej pomóc. Plus, jakaś sobowtórka robi jej szkodę reputacyjną. | 0111-10-18 - 0111-11-02 |
| 210414-dekralotyzacja-asimear       | manifestuje cekiny - przekonała Lazarin do współpracy przy niszczeniu kralotha (jako "ta z Sekretów Orbitera"), magią przeniosła fale mentalne z Jolanty na Eleny by dać jej możliwość przejęcia Entropika Joli. | 0111-11-03 - 0111-11-12 |
| 210421-znudzona-zaloga-inferni      | by pozbyć się problemów z morale kazała Eustachemu zreanimować Tirakala. Skończyło się na tym, że magią zintegrowała (niechcący) w Persefonę zarówno Morrigan jak i Samuraja Miłości. | 0111-11-16 - 0111-11-19 |
| 210428-infekcja-serenit             | dowiedziała się od Tadeusza wszystkiego co trzeba o Falołamaczu, potem na podstawie wyliczeń Klaudii poleciała w kierunku na przewidywaną lokalizację Falołamacza. Silniki ledwo wytrzymały (uszkodzone). | 0111-11-22 - 0111-11-23 |
| 210512-ewakuacja-z-serenit          | przeszczepiła magicznie sentisieć z Eidolona do Entropika używając udrożenienia kultu, po czym zmanipulowała ludzi na Falołamaczu by Aida mogła ich uratować. | 0111-11-23 - 0111-12-02 |
| 210519-osiemnascie-oczu             | doszła do tego, że "Klaudia nie może myśleć" bo wpadnie pod infohazard. Planowała wysokopoziomowo jak radzić sobie z infohazardem na Alayi. | 0111-12-07 - 0111-12-18 |
| 231018-anomalne-awarie-athamarein   | namówiona przez Kircznika, wzięła ekipę Inferni by spojrzeli na stan Athamarein. Diagnozuje załogę Athamarein, rozmawia z nimi, identyfikuje problemy i dochodzi do tego, że to Athamarein odrzuca załogę. Dla Klaudii za mała szansa na to wydarzenie, dla Arianny - znajomości 'ludzkiej natury' - to było prostsze do zaakceptowania. Nie mechanizm a serce korwety. | 0111-12-22 - 0111-12-27 |
| 210526-morderstwo-na-inferni        | próbowała utrzymać morale wśród noktian; wynegocjowała z adm. Kramerem, żeby noktian zamieszanych w morderstwo przeniósł na Żelazko a nie wysłał na ścięcie czy coś. | 0111-12-31 - 0112-01-06 |
| 210609-sekrety-kariatydy            | by zdobyć Tivr odnalazła OO Kariatyda który zaginął w Anomalii Kolapsu. Chce wyruszyć na jego poszukiwanie, ale wpakowała się w intrygę ekosystemu TAI na K1. Zaszumiła konkursem "luksusowy statek Luxuritias". | 0112-01-10 - 0112-01-13 |
| 210630-listy-od-fanow               | weszła we współpracę z agentem bezpieczeństwa K1 (Galwarnem), po czym z Eterni wynegocjowała Eidolona którego dyskretnie użyła przeciw eternijskiej bazie na K1. A wszystko dla fana. | 0112-01-15 - 0112-01-18 |
| 210818-siostrzenica-morlana         | panikuje Tomasza rozpinając JEDEN GUZICZEK i poznaje sekrety Tomasza i Jolanty. Przekazuje Kramerowi info o kosmicznym programie Aurum. Planuje wspólne ćwiczenia by odzyskać Ofelię. W sumie - mastermind działań przeciw Natanielowi Morlanowi. | 0112-01-20 - 0112-01-24 |
| 210616-nieudana-infiltracja-inferni | przekonała Rolanda by nie leciał z nimi do Anomalii Kolapsu, wykryła tożsamość Flawii i dała jej szansę na dołączenie do Inferni. Mistrzyni odwracania uwagi i wrabiania innych że są winni. | 0112-01-27 - 0112-02-01 |
| 210825-uszkodzona-brama-eteryczna   | abstrahując od przekonania Medei do wyznania że w Bramie zamknięty jest Spatium Gelida, channelowała Nocną Kryptę w zdestabilizowaną Bramę. Dzięki temu uratowała efemerydami WSZYSTKICH, nawet tych, którzy częściowo byli już w Bramie. | 0112-02-04 - 0112-02-07 |
| 210901-stabilizacja-bramy-eterycznej | gdy Elena była pod wpływem Esuriit, zneutralizowała ją mówiąc o pasie cnoty dla Eustachego. Bezbłędnie nawigowała Skażony OO Perforator, gdzie połowa systemów była efemeryczna. Aha, zlinkowała się hipernetem z ON Spatium Gelida. | 0112-02-09 - 0112-02-12 |
| 210922-ostatnia-akcja-bohaterki     | przekonała Kramera, zdobyła Olgierda, przekonała Leonę by ta nie polowała na tien Kopiec (daje szansę zabicia DWÓCH Eternian) by na końcu służyć jako dywersja dla Jolanty... i oczywiście Skaziła bazę piratów, ożywiając ją po swojemu. Aha, użyła emitera anihilacji. Spodobał jej się. | 0112-02-23 - 0112-03-09 |
| 210929-grupa-ekspedycyjna-kellert   | zdecydowała się na skrajnie niebezpieczną misję, by ratować ludzi Termii. W kluczowym momencie - czy Martyn uratuje ludzi czy nie, sam na sam z Vigilusem - kazała szturmować korzystając z tego że Piranie się oddaliły. Uratowała Martyna i siedem osób. | 0112-03-13 - 0112-03-16 |
| 211013-szara-nawalnica              | znalazła sposób by Vigilus nie widział servarów - użyć luster. Potem - złamała konwencję anomalizując torpedę anihilacyjną (wybuch) i kierując go ku planecie w sektorze Mevilig. Aha, przekonała grupkę ludzi z Mevilig że ona jest silniejsza niż zbawiciel - jej kult jest lepszy. | 0112-03-18 - 0112-03-23 |
| 211020-kurczakownia                 | udaje Zbawiciela - aspekt mający wszystkich uratować z Kalarfam. Zmierzyła się ze Zbawicielem-Niszczycielem przy jego ołtarzu i kupiła Eustachemu czas na strzelenie weń Działem Rozpaczy. Stała się prawdziwą inkarnacją Zbawiciela dla uratowanych kultystów Zbawiciela-Niszczyciela. | 0112-03-24 - 0112-03-26 |
| 211027-rzieza-niszczy-infernie      | chcąc powstrzymać ingerencję w jej pamięć Rziezy użyła podświadomie technik Ataienne. Rzieza dowiedział się o Ataienne. Arianna wezwała Elenę na pomoc, która zalała Esuriit laboratorium. Arianna z Klaudią próbowała opanować załogę Inferni i jak Martyn wyssał energię Esuriit z załogi, dała radę to zrobić. | 0112-03-28 - 0112-04-02 |
| 211110-romans-dzieki-esuriit        | działania administracyjne; akceptacja listów o zgonie załogantów (+ pisze coś od siebie), NIE UKRYWA Flawii mimo że mogłaby uniknąć potencjalnych reperkusji, organizuje gry wojenne by ukryć, że Elena sama uciekła do czarnych sektorów K1. | 0112-04-13 - 0112-04-14 |
| 211117-porwany-trismegistos         | ukryła swą obecność na Tivrze, neutralizowała Rolanda Sowińskiego i pozwoliła Trismegistosowi odlecieć. Chce, by wszyscy żyli w pokoju a Trismegistos jest nadzieją na lepsze jutro. | 0112-04-15 - 0112-04-17 |
| 211124-prototypowa-nereida-natalii  | nie dołączyła Rolanda do załogi i dała mu odczuć że go nie chce. Wymyśliła jak zwabić Nereidę do Inferni, używając theme song Sekretów Orbitera. Dobrze pilotuje Infernię pozycjonując prawidłowo Elenę w kosmosie (jak minę). | 0112-04-20 - 0112-04-24 |
| 211208-o-krok-za-daleko             | opanowując infekcję ixiońską skupiła się na wzmocnieniu Eustachego i jego integracji z Infernią; udało jej się, ale ciężko Skaziła Elenę ixionem i trwale zanomalizowała Infernię. Potem przekonała Jarka Szarjana do tego, by Natalia mogła się wykluć na Inferni i by Neotik był _safe haven_ Inferni. | 0112-04-25 - 0112-04-26 |
| 211215-sklejanie-inferni-do-kupy    | zatrzymała Elenę na Inferni (dokładniej: Tivrze), uspokoiła Dianę która chciała niszczyć by ratować Eustachego, poznała prawdę od Kamila odnośnie swej przyszłości (awatar Vigilusa), decyzja o feromonach - nowa kultura Inferni. I zrobiła panikę w Stoczni Neotik - czy Hestia to pokaże? | 0112-04-27 - 0112-04-29 |
| 211222-kult-saitaera-w-neotik       | przemowa + feromony Marii zmieniły kulturę Inferni w dziwną; uziemiła Elenę w Inferni by ta mogła zinfiltrować Stocznię i zaprojektowała plan - przekazać Nereidę Elenie i zdobyć koloidowy statek. | 0112-04-30 - 0112-05-01 |
| 220105-to-nie-pulapka-na-nereide    | zbudowała z noktiańskiego Tivra perfekcyjną maszynę wojskową, po czym opanowała kult. Niestety z uwagi na problemy z magią w obliczu Saitaera uwolniła Esuriit i prawie zniszczyła Infernię. | 0112-05-04 - 0112-05-09 |
| 220126-keldan-voss-kolonia-saitaera | overridowała lekarza, lepiej dla Eleny jeśli poleci z nimi. Przekierowała Eustachego do rozmowy z pallidanami, sama skupia się na drakolitach z kultu Saitaera. | 0112-05-24 - 0112-05-27 |
| 220202-sekrety-keldan-voss          | Przekonała odłam drakolitów, że warto współpracować z korporacją. Politycznie zmusza drakolitów do benefit of doubt. | 0112-05-29 - 0112-05-31 |
| 220216-polityka-rujnuje-pallide-voss | konspiruje z Mateusem by skutecznie wyciągnąć zarówno pallidan od keldanitów jak i keldanitów od pallidan. | 0112-06-01 - 0112-06-03 |
| 220223-stabilizacja-keldan-voss     | politycznie zapewnia, by uciekinierzy z Pallidy Voss mogli znaleźć tymczasowe schronienie w Keldan Voss. Przesłuchała i wkręciła Szczepana. | 0112-06-04 - 0112-06-07 |
| 220309-upadek-eleny                 | załatwiła polityczne wsparcie dla Keldan Voss i Anniki od pallidan. Wymusiła współpracę między Tomaszem Kaltabenem i Anniką Pradis. I zapewniła że Barnaba i Szczepan spotkają sprawiedliwość. | 0112-06-08 - 0112-06-14 |
| 220316-potwor-czy-choroba-na-etaur  | koordynacja; do niej odzywa się Eternia i ona jest tą, z którą wszyscy rozmawiają. Doszła do tego, że choroba może rozprzestrzeniać się po kanałach magicznych. | 0112-06-29 - 0112-07-01 |
| 220330-etaur-i-przyneta-na-krypte   | przejęła dowodzenie nad EtAur po dowodach, zaprowadziła kwarantannę i zaczęła czyścić bazę. Zdobyła Daniela - "potwora" - żywego jako przynętę na Kryptę. | 0112-07-02 - 0112-07-05 |
| 220610-ratujemy-porywaczy-eleny     | ustabilizowała Elenę, gdy tą pokonało jej własne Spustoszenie. Przesłuchała lekarzy i zlokalizowała kiedy zaczęły się problemy na Atropos. Doprowadziła do tego, że dyrektor Atropos został medykiem na Inferni i myśli że to lepsza opcja niż court of law. | 0112-09-15 - 0112-09-17 |
| 220615-lewiatan-przy-seibert        | skutecznie opanowała przechwyconą energię Lewiatana w baterii vitium i sformowała zeń przynętę na Lewiatana. Sposób, by Lewiatana gdzieś przeprowadzić. | 0112-09-27 - 0112-09-29 |
| 220622-lewiatan-za-pandore          | gdy Ola Szerszeń zaczęła interesować się biovatem Eleny, dała Eustachemu i Klaudii dowodzenie nad Olą by ta nie szalała. Podczas starcia z Lewiatanem używając swojej mocy skutecznie ożywiła kawałek Lewiatana i go "oswoiła", po czym stworzony byt przekazała Klaudii w prezencie. | 0112-09-30 - 0112-10-03 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210224-sentiobsesja                 | jest pupilką sierżanta Przemysława Czapurta rodu Verlen. To znaczy, że traktuje ją ostro i twardo ;-). | 0092-10-04
| 210224-sentiobsesja                 | "młodsza siorka" żołnierzy w Holdzie Bastion. Swoja laska. Fajna, wpada z nimi w kłopoty i mimo imienia (Aria) nie umie śpiewać pieśni patriotycznych w karcerze. | 0092-10-04
| 210311-studenci-u-verlenow          | powszechna opinia u żołnierzy i w rodzie, że ma dużo zalet, ale pilotem nigdy nie będzie... rozwaliła DWA ścigacze JEDNEGO DNIA. Acz kochana w Poniewierzy. | 0097-01-22
| 221012-kapitan-verlen-i-niezapowiedziana-inspekcja | NAGANA DO AKT że nie zgłasza stanu jednostki i próbuje sama zrobić bez wsparcia itp - przez co jednostka ucierpiała. | 0100-05-21
| 221012-kapitan-verlen-i-niezapowiedziana-inspekcja | coraz większy szacunek na Królowej Kosmicznej Chwały: marines, advancerzy, sarderyci, seilici. Ma też wsparcie u Arnulfa i Grażyny. Pojawia się szacunek do niej za żarcie konserw póki Królowa nie będzie w dobrym stanie (lub: bo Szymon ją skaleczył podczas pojedynku). | 0100-05-21
| 221026-kapitan-verlen-i-koniec-przygody-na-krolowej | zainteresowanie nowo wschodzącego admirała Antoniego Kramera z uwagi na jej nadspodziewany sukces | 0100-06-15
| 221026-kapitan-verlen-i-koniec-przygody-na-krolowej | przeniesiona z Królowej Kosmicznej Chwały na dużo lepszą jednostkę, Astralną Flarę. | 0100-06-15
| 221130-astralna-flara-w-strefie-duchow | opinia ABSOLUTNIE bezwzględnej i strasznej wśród załogi Athamarein i Astralnej Flary po mrocznym wskrzeszeniu TAI Zarralea. 'Live or die, you shall serve'. | 0100-10-08
| 221221-astralna-flara-i-nowy-komodor | dostała od Orbitera medal za bezkrwawe pokonanie dominujących sił wroga (dzięki operacji z Zarraleą) | 0100-11-10
| 221221-astralna-flara-i-nowy-komodor | bohaterka Astralnej Flary. Dzięki Ellarinie i jej propagandzie jest lubiana i podziwiana przez załogę Flary mimo jej nie najwyższej kompetencji. | 0100-11-10
| 231115-cwiczenia-komodora-bladawira | trafia pod kontrolę komodora Bladawira | 0110-11-15
| 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira | pokazała niesamowicie dużą kompetencję i zaufanie do swojej załogi wygrywając niemożliwe ćwiczenia w stylu bohaterki a nie w stylu zdeptanego kapitana Orbitera. Szacunek od Bladawira. | 0110-11-30
| 231220-bladawir-kontra-przemyt-tienow | Dostała nieproporcjonalnie dużo opiernicz za przemyt rzeczy na Inferni, by ukarać Bladawira. Ale też ma opinię 'lizuski Bladawira' za podłe akcje celne. | 0110-12-11
| 231220-bladawir-kontra-przemyt-tienow | Bladawir jej nie ufa, uważa ją za agentkę Innej Strony, ale nie rozumie jej zachowania. Jest skłonny pozwolić jej odejść bez konsekwencji. | 0110-12-11
| 240110-wieczna-wojna-bladawira      | rozprzestrzeniła agentów Kamila na Kontrolerze Pierwszym; chce mieć agentów w różnych miejscach | 0110-12-27
| 200708-problematyczna-elena         | opinia nie-takiej-kompetentnej-jak-się-zdawało u wielu oficerów floty Orbitera widzących ćwiczenia. Czy nie kontroluje załogi czy statku, kto wie? | 0111-01-01
| 200715-sabotaz-netrahiny            | sława i chwała wybitnego pilota i pistoleta; pobiła Tvaraną rekordy korwet kurierskich (robota Eleny). I opieprz za niepotrzebne zezłomowanie Tvarany i uszkodzenie Netrahiny. | 0111-01-07
| 200715-sabotaz-netrahiny            | ma kody kontrolne "uwalniające" TAI Persefona i koordynaty, gdzie może dowiedzieć się więcej. | 0111-01-07
| 200729-nocna-krypta-i-emulatorka    | dostaje szacun za ochronę Sektora Astoriańskiego przed Kijarą d'Esuriit i za zniszczenie straszliwej Anomalii. | 0111-01-20
| 200729-nocna-krypta-i-emulatorka    | Kirasjerzy i admirał Aleksandra Termia są nią bardzo rozczarowani. Nie "źli" a "oczekiwali dużo więcej". | 0111-01-20
| 200819-sekrety-orbitera-historia-prawdziwa | uznanie za to, że w chwili kryzysowej wyizolowała z Tadeusza Ursusa problematyczne elementy Esuriit swoją magią, narażając własne zdrowie. | 0111-02-01
| 200822-gdy-arianna-nie-patrzy       | KOLEJNY opiernicz od admiralicji. Tym razem za imprezę i zachowanie Martyna. | 0111-02-04
| 200822-gdy-arianna-nie-patrzy       | dzięki manipulacjom Klaudii wrobiona w to, że żałośnie próbuje podkupić się u arystokracji Eterni i przeprosić za to z Eleną. Nic o tym nie wie. | 0111-02-04
| 200822-gdy-arianna-nie-patrzy       | Tadeusz Ursus dużo jej zawdzięcza dzięki Klaudii - wierzy, że Arianna pilnowała by on nie zginął. | 0111-02-04
| 200826-nienawisc-do-swin            | "mała buntowniczka" admirał Aleksandry Termii; ta wredna żmija Ariannę trochę lubi. I dokuczać jej też. | 0111-02-11
| 200826-nienawisc-do-swin            | delikatny sojusz i cień przyjaźni z Tadeuszem Ursusem, eternijskim arystokratą Orbitera. | 0111-02-11
| 200826-nienawisc-do-swin            | KRZYŻYK ŻENADY. Siara. Efektowne wejście smoka. Najbardziej kiczowate działania, ale uwiecznione przez "Sekrety Orbitera"... | 0111-02-11
| 200909-arystokratka-w-ladowni-na-swinie | dzięki Izabeli Zarantel i "Sekretom Orbitera", pojawiła się opinia, że Arianna walczy o równość między Noctis, Eternią, Astorią itp. | 0111-02-17
| 200909-arystokratka-w-ladowni-na-swinie | rywale - nie jest dobra, jest psem na szkło. Rywale. Negatywne konsekwencje sławy reality show ;-). | 0111-02-17
| 200909-arystokratka-w-ladowni-na-swinie | TYLKO INFERNIA. Nie dostanie dowodzenia innego statku bo go rozwali XD (opinia). Nawet Galaktycznego Tucznika rozwaliła. | 0111-02-17
| 200909-arystokratka-w-ladowni-na-swinie | Paradoksalnie splątana z Anastazją Sowińską; nie może się od niej oddalić > 1 km. Zdaniem wielu, Arianna splątała się specjalnie z Anastazją. | 0111-02-17
| 200923-magiczna-burza-w-raju        | po akcji z wzywaniem wsparcia humanitarnego z Orbitera, ceniona "Zamiast za kompetencję to za dobre serduszko". | 0111-02-25
| 200923-magiczna-burza-w-raju        | przykład kontr-propagandy Orbitera: "popatrzcie jak Orbiter traktuje zasłużonych weteranów. Na świnie z nimi.' | 0111-02-25
| 201021-noktianie-rodu-arlacz        | dostała silny cios reputacyjny - stoi po stronie Elizy Iry przeciwko lojalistom Aurum. Kolaboruje z Elizą! Ale - bonus wśród sympatyków noktian. | 0111-03-10
| 201104-sabotaz-swini                | nie będzie mieć "Wesołego Wieprzka" we flocie. Statek zanomalizował i został zniszczony przez Ataienne. Też: nie dała rady ze świniami. | 0111-03-16
| 201118-anastazja-bohaterka          | załoga Inferni jest w niej rozkochana. Nie tylko ją wielbią - też kochają. Dzięki, różowa mgiełko. | 0111-03-25
| 200610-ixiacka-wersja-malictrix     | otrzymuje niewielki okręt bezzałogowy OO Pandora, w którego wpakowuje Malictrix jako główne TAI. | 0111-05-05
| 210728-w-cieniu-nocnej-krypty       | dobra wola ze strony noktian; zarówno z uwagi na to jak potraktowała Zonę Tres jak i swoich noktian pozwoląc im odejść | 0111-06-06
| 210804-infernia-jest-nasza          | tien Maciej Żarand z Eterni został jej bezwzględnym kultystą. Jeśli magia jest bytem wyższym a Arianna ją kontroluje to arcymagowie są godni wyznawania... | 0111-06-24
| 210106-sos-z-haremu                 | wdzięczność rodu arystokratycznego Aktenir; potencjalni sojusznicy. | 0111-07-27
| 210218-infernia-jako-goldarion      | okazuje się, że przez wiele osób z Aurum uważana jest za postać fikcyjną. Tzn. może istnieje, ale jest aktorką w "Sekretach Orbitera". WTF. | 0111-10-01
| 200429-porwanie-cywila-z-kokitii    | łaskawe oko Sił Specjalnych, frakcja "Gorący Lód" oraz Medei Sowińskiej. Też plotki że współpracuje z tymi radykałami sił specjalnych Orbitera. | 0111-10-06
| 210317-arianna-podbija-asimear      | jej "sobowtórka" porwała Tirakal Jolancie Sowińskiej i zagroziła zniszczenie planetoidy. Powiedzmy, nie jest popularna w tej okolicy. | 0111-11-02
| 210414-dekralotyzacja-asimear       | na Asimear ma opinię "dokładnie taka jak w Sekretach Orbitera". Z cekinami, dumą i wszystkim | 0111-11-12
| 210414-dekralotyzacja-asimear       | Opinia "Krwawa Lady Verlen" na Asimear po akcji Eustachego i Klaudii z twardym, krwawym odpieraniem kralotha. | 0111-11-12
| 210519-osiemnascie-oczu             | wysokie uznanie ze strony Mariana Tosena z grupy antymemetyczej Orbitera. | 0111-12-18
| 210609-sekrety-kariatydy            | chce się z nią ożenić Roland Sowiński. Sowińscy chcą zniszczyć ten związek. Ona próbuje nie być w tym związku XD. | 0112-01-13
| 210609-sekrety-kariatydy            | ona i Infernia są naiwne - dali się wykorzystać w chytrej reklamie Luxuritias. | 0112-01-13
| 210609-sekrety-kariatydy            | część Aurum na K1 chce współpracować tylko z nią. Bo nie zostawia swoich w potrzebie. I bo Roland chce się z nią ożenić. | 0112-01-13
| 210818-siostrzenica-morlana         | kody pozwalające jej na podawanie się za Jolantę Sowińską. Ich użycie to POWAŻNE nadużycie zaufania Jolanty. | 0112-01-24
| 210818-siostrzenica-morlana         | pochwała od Kramera - pozyskała info o programie kosmicznym Aurum z pamięci Jolanty i przekazała to Kramerowi. Poważne nadużycie zaufania Jolanty i problemy dla Jolanty. | 0112-01-24
| 210901-stabilizacja-bramy-eterycznej | połączona ze Spatium Gelida przez hipernet. W jakiś sposób. Magicznie. Jest kotwicą i komunikatorem. | 0112-02-12
| 210922-ostatnia-akcja-bohaterki     | Eternia jest zachwycona odcinkiem Sekretów Orbitera z Jolantą Kopiec. Arianna spłaciła wszelkie długi. | 0112-03-09
| 211020-kurczakownia                 | BOHATERKA! Weszła do Sektora Mevilig by ratować Orbiterowców i jej się to udało. A ryzykowała nieprawdopodobnie dużo. Plus, [1500-2000] osób więcej? Wow! | 0112-03-26
| 211020-kurczakownia                 | Stała się prawdziwą inkarnacją aspektu Zbawiciela dla uratowanych kultystów Zbawiciela-Niszczyciela. | 0112-03-26
| 211027-rzieza-niszczy-infernie      | jej umysł i pamięć zostały odbudowane przez Rziezę. Pamięta moc Ataienne i to, co Ataienne jej zrobiła. | 0112-04-02
| 211110-romans-dzieki-esuriit        | dostaje Tivr jako swoją drugą jednostkę | 0112-04-14
| 211117-porwany-trismegistos         | ona podobno steruje Serenitem. Może go przyzywać i odpychać na żądanie. Królowa Anomalii Kosmicznych. W plotkę wierzą wszędzie POZA Orbiterem. | 0112-04-17
| 211222-kult-saitaera-w-neotik       | aspekt Vigilusa (Vigilus - Nihilus).Załoga i Kult jest przekonana, że jest Aspektem Zbawiciela. | 0112-05-01
| 220105-to-nie-pulapka-na-nereide    | przejęła kontrolę nad kultem i pozycjonowała Ottona na arcykapłana po śmierci Kamila. | 0112-05-09
| 220330-etaur-i-przyneta-na-krypte   | ma SMACZNĄ przynętę na Kryptę. Byłego potwora z EtAur, Daniela. | 0112-07-05
| 220330-etaur-i-przyneta-na-krypte   | NIECHĘĆ ze strony purystów Orbitera. PRO-LUDZKA grupa Orbitera, Eternia i Aurum uważają Infernię za siłę której wiele zawdzięczają. | 0112-07-05
| 220610-ratujemy-porywaczy-eleny     | Syndykat Aureliona wie, że Arianna nań poluje. Syndykat nie potraktuje tego łagodnie jak coś Arianna zrobi. | 0112-09-17

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 74 | ((200106-infernia-martyn-hiwasser; 200122-kiepski-altruistyczny-terroryzm; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200429-porwanie-cywila-z-kokitii; 200610-ixiacka-wersja-malictrix; 200624-ratujmy-castigator; 200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210707-po-drugiej-stronie-bramy; 210721-pierwsza-bia-mag; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore; 231018-anomalne-awarie-athamarein; 231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240110-wieczna-wojna-bladawira; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Eustachy Korkoran    | 69 | ((200122-kiepski-altruistyczny-terroryzm; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200429-porwanie-cywila-z-kokitii; 200624-ratujmy-castigator; 200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore; 231011-ekstaflos-na-tezifeng; 231018-anomalne-awarie-athamarein; 231025-spiew-nielalki-na-castigatorze; 231220-bladawir-kontra-przemyt-tienow; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Elena Verlen         | 61 | ((200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210210-milosc-w-rodzie-verlen; 210218-infernia-jako-goldarion; 210311-studenci-u-verlenow; 210317-arianna-podbija-asimear; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220610-ratujemy-porywaczy-eleny; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 231011-ekstaflos-na-tezifeng; 231018-anomalne-awarie-athamarein; 231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Martyn Hiwasser      | 33 | ((200106-infernia-martyn-hiwasser; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 220610-ratujemy-porywaczy-eleny)) |
| Leona Astrienko      | 32 | ((200408-o-psach-i-krysztalach; 200429-porwanie-cywila-z-kokitii; 200610-ixiacka-wersja-malictrix; 200624-ratujmy-castigator; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210106-sos-z-haremu; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 220610-ratujemy-porywaczy-eleny; 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 231011-ekstaflos-na-tezifeng; 231018-anomalne-awarie-athamarein; 231025-spiew-nielalki-na-castigatorze)) |
| OO Infernia          | 17 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210825-uszkodzona-brama-eteryczna; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 220622-lewiatan-za-pandore; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Kamil Lyraczek       | 14 | ((191025-nocna-krypta-i-bohaterka; 200106-infernia-martyn-hiwasser; 200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211020-kurczakownia; 211215-sklejanie-inferni-do-kupy; 220105-to-nie-pulapka-na-nereide; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Leszek Kurzmin       | 14 | ((200624-ratujmy-castigator; 200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa; 220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira)) |
| Izabela Zarantel     | 13 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 231115-cwiczenia-komodora-bladawira)) |
| Antoni Kramer        | 12 | ((200429-porwanie-cywila-z-kokitii; 200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Daria Czarnewik      | 12 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Maria Naavas         | 12 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220622-lewiatan-za-pandore)) |
| Maja Samszar         | 10 | ((210311-studenci-u-verlenow; 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Raoul Lavanis        | 10 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220622-lewiatan-za-pandore; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231220-bladawir-kontra-przemyt-tienow; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Władawiec Diakon     | 10 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 240110-wieczna-wojna-bladawira; 240124-mikiptur-zemsta-woltaren)) |
| Anastazja Sowińska   | 9 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Viorika Verlen       | 9 | ((210210-milosc-w-rodzie-verlen; 210224-sentiobsesja; 210311-studenci-u-verlenow; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230426-mscizab-zabojca-arachnoziemskich-jaszczurow; 230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Antoni Bladawir      | 8 | ((231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240110-wieczna-wojna-bladawira; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Arnulf Perikas       | 8 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow)) |
| Diana Arłacz         | 8 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| AK Nocna Krypta      | 7 | ((191025-nocna-krypta-i-bohaterka; 200729-nocna-krypta-i-emulatorka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210728-w-cieniu-nocnej-krypty; 210825-uszkodzona-brama-eteryczna)) |
| Kajetan Kircznik     | 7 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 231018-anomalne-awarie-athamarein)) |
| Olgierd Drongon      | 7 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210630-listy-od-fanow; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| OO Athamarein        | 7 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 231018-anomalne-awarie-athamarein)) |
| Roland Sowiński      | 7 | ((210512-ewakuacja-z-serenit; 210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| OO Astralna Flara    | 6 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Otto Azgorn          | 6 | ((210421-znudzona-zaloga-inferni; 210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Aleksandra Termia    | 5 | ((200826-nienawisc-do-swin; 201014-krystaliczny-gniew-elizy; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica)) |
| Annika Pradis        | 5 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Apollo Verlen        | 5 | ((210210-milosc-w-rodzie-verlen; 210224-sentiobsesja; 210311-studenci-u-verlenow; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka)) |
| Diana d'Infernia     | 5 | ((210804-infernia-jest-nasza; 210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Erwin Pies           | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Flawia Blakenbauer   | 5 | ((210616-nieudana-infiltracja-inferni; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Grażyna Burgacz      | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Janus Krzak          | 5 | ((210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210901-stabilizacja-bramy-eterycznej)) |
| OO Królowa Kosmicznej Chwały | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| OO Tivr              | 5 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos; 220622-lewiatan-za-pandore; 240131-anomalna-mavidiz)) |
| Tomasz Sowiński      | 5 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210818-siostrzenica-morlana)) |
| Adam Szarjan         | 4 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Alezja Dumorin       | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Ataienne             | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Damian Orion         | 4 | ((200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Ellarina Samarintael | 4 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Gabriel Lodowiec     | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Lars Kidironus       | 4 | ((231011-ekstaflos-na-tezifeng; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240124-mikiptur-zemsta-woltaren)) |
| Marian Tosen         | 4 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni)) |
| OO Castigator        | 4 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| OO Żelazko           | 4 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Romeo Verlen         | 4 | ((210210-milosc-w-rodzie-verlen; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen; 221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Szczepan Myrczek     | 4 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Szymon Wanad         | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Tadeusz Ursus        | 4 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210428-infekcja-serenit)) |
| Zaara Mieralit       | 4 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240110-wieczna-wojna-bladawira)) |
| Dariusz Krantak      | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 210127-porwanie-anastazji-z-odkupienia)) |
| Eliza Ira            | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Feliks Walrond       | 3 | ((210526-morderstwo-na-inferni; 231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren)) |
| Hubert Kerwelenios   | 3 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Jolanta Sowińska     | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Klarysa Jirnik       | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Klaudiusz Terienak   | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 231011-ekstaflos-na-tezifeng)) |
| Marcinozaur Verlen   | 3 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 231220-bladawir-kontra-przemyt-tienow)) |
| Marian Fartel        | 3 | ((200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Marta Keksik         | 3 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Mateus Sarpon        | 3 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss)) |
| Medea Sowińska       | 3 | ((200429-porwanie-cywila-z-kokitii; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| NekroTAI Zarralea    | 3 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| OO Karsztarin        | 3 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 231115-cwiczenia-komodora-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| OO Netrahina         | 3 | ((200715-sabotaz-netrahiny; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej)) |
| OO Paprykowiec       | 3 | ((231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Patryk Samszar       | 3 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Persefona d'Infernia | 3 | ((200708-problematyczna-elena; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit)) |
| Romana Arnatin       | 3 | ((210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| Stefan Torkil        | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Szczepan Kaltaben    | 3 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss)) |
| TAI Rzieza d'K1      | 3 | ((210630-listy-od-fanow; 211013-szara-nawalnica; 211027-rzieza-niszczy-infernie)) |
| Tomasz Ruppok        | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Ula Blakenbauer      | 3 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Wawrzyn Rewemis      | 3 | ((211110-romans-dzieki-esuriit; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| AK Serenit           | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| AK Wyjec             | 2 | ((201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Anastazja Sowińska Dwa | 2 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia)) |
| Bogdan Anatael       | 2 | ((210512-ewakuacja-z-serenit; 210630-listy-od-fanow)) |
| Dariusz Blakenbauer  | 2 | ((210311-studenci-u-verlenow; 210331-elena-z-rodu-verlen)) |
| Ewa Razalis          | 2 | ((231115-cwiczenia-komodora-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Gerwazy Kircznik     | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gilbert Bloch        | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Juliusz Sowiński     | 2 | ((201021-noktianie-rodu-arlacz; 201210-pocalunek-aspirii)) |
| Karol Reichard       | 2 | ((210526-morderstwo-na-inferni; 211110-romans-dzieki-esuriit)) |
| Kasandra Destrukcja Diakon | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Kazimierz Darbik     | 2 | ((231220-bladawir-kontra-przemyt-tienow; 240110-wieczna-wojna-bladawira)) |
| Kirea Rialirat       | 2 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Leo Kasztop          | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Lucjusz Blakenbauer  | 2 | ((210224-sentiobsesja; 210324-lustrzane-odbicie-eleny)) |
| Lutus Amerin         | 2 | ((211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Malictrix d'Pandora  | 2 | ((200610-ixiacka-wersja-malictrix; 210414-dekralotyzacja-asimear)) |
| Marcel Kulgard       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Mariusz Bulterier    | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Martyna Bianistek    | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Michał Perikas       | 2 | ((210311-studenci-u-verlenow; 210324-lustrzane-odbicie-eleny)) |
| Mściząb Verlen       | 2 | ((230426-mscizab-zabojca-arachnoziemskich-jaszczurow; 230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Natalia Aradin       | 2 | ((211124-prototypowa-nereida-natalii; 220105-to-nie-pulapka-na-nereide)) |
| OA Zguba Tytanów     | 2 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji)) |
| OE Falołamacz        | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Ola Szerszeń         | 2 | ((220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore)) |
| OO Loricatus         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Wesoły Wieprzek   | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Przemysław Czapurt   | 2 | ((210224-sentiobsesja; 210324-lustrzane-odbicie-eleny)) |
| Rafael Galwarn       | 2 | ((210609-sekrety-kariatydy; 210630-listy-od-fanow)) |
| Rafał Armadion       | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Robert Garwen        | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Rufus Komczirp       | 2 | ((200715-sabotaz-netrahiny; 201111-mychainee-na-netrahinie)) |
| Salazar Bolza        | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| SCA Hadiah Emas      | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| SCA Płetwal Błękitny | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| SP Pallida Voss      | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Tal Marczak          | 2 | ((210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy)) |
| Tomasz Dojnicz       | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221130-astralna-flara-w-strefie-duchow)) |
| Tomasz Kaltaben      | 2 | ((220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Ulisses Kalidon      | 2 | ((210714-baza-zona-tres; 210728-w-cieniu-nocnej-krypty)) |
| Vigilus Mevilig      | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Wanessa Pyszcz       | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Zygfryd Maus         | 2 | ((211117-porwany-trismegistos; 220126-keldan-voss-kolonia-saitaera)) |
| Ada Wyrocznik        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Adam Chrząszczewicz  | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Aida Serenit         | 1 | ((210512-ewakuacja-z-serenit)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Rodivas           | 1 | ((201230-pulapka-z-anastazji)) |
| AK Salamin           | 1 | ((200708-problematyczna-elena)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Alara Ehmes          | 1 | ((200429-porwanie-cywila-z-kokitii)) |
| Aleksander Samszar   | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alicja Szadawir      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Andrzej Gwozdnik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Aneta Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Anna Tessalon        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Artur Traffal        | 1 | ((210804-infernia-jest-nasza)) |
| Atraksjusz Verlen    | 1 | ((230426-mscizab-zabojca-arachnoziemskich-jaszczurow)) |
| Atrius Kurunen       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Axel Nargan          | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| BIA Solitaria d'Zona Tres | 1 | ((210721-pierwsza-bia-mag)) |
| BIA XXX d'Zona Tres  | 1 | ((210714-baza-zona-tres)) |
| Błażej Sowiński      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Borys Kragin         | 1 | ((240131-anomalna-mavidiz)) |
| Brunhilda Verlen     | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Dominik Ogryz        | 1 | ((200106-infernia-martyn-hiwasser)) |
| Dominika Perikas     | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Donald Parziarz      | 1 | ((201210-pocalunek-aspirii)) |
| Dormand Miraris      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Dorota Radraszew     | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Emilia Kariamon      | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Emmanuelle Gęsiawiec | 1 | ((230426-mscizab-zabojca-arachnoziemskich-jaszczurow)) |
| Ernest Bankierz      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Eszara d'Seibert     | 1 | ((220615-lewiatan-przy-seibert)) |
| Fabian Rzelicki      | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Fantazjusz Verlen    | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Finis Vitae          | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Franciszek Maszkiet  | 1 | ((210804-infernia-jest-nasza)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Franz Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Gerard Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Grigor Tarnow        | 1 | ((240131-anomalna-mavidiz)) |
| Gunnar Brunt         | 1 | ((201125-wyscig-jako-randka)) |
| Halina Szkwalnik     | 1 | ((201111-mychainee-na-netrahinie)) |
| Helena Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Henryk Sowiński      | 1 | ((201230-pulapka-z-anastazji)) |
| Hestia d'Atropos     | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Hestia d'Neotik      | 1 | ((211208-o-krok-za-daleko)) |
| Hiacynt Samszar      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Horacy Aktenir       | 1 | ((210106-sos-z-haremu)) |
| Igor Arłacz          | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Igor Stratos         | 1 | ((240131-anomalna-mavidiz)) |
| Jakub Bulgocz        | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Jakub Oroginiec      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Jarosław Szarjan     | 1 | ((211208-o-krok-za-daleko)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Jolanta Kopiec       | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Jonasz Parys         | 1 | ((220615-lewiatan-przy-seibert)) |
| Julia Aktenir        | 1 | ((210106-sos-z-haremu)) |
| Julian Muszel        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Karl Murnoff         | 1 | ((240131-anomalna-mavidiz)) |
| Kaspian Certisarius  | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Katra Igneus         | 1 | ((201210-pocalunek-aspirii)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Konrad Wolczątek     | 1 | ((200722-wielki-kosmiczny-romans)) |
| Konstanty Keksik     | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Kormonow Voss        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Krucjusz Verlen      | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Krystian Blakenbauer | 1 | ((210331-elena-z-rodu-verlen)) |
| Krzysztof Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Lamia Akacja         | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Leszek Czarban       | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Lidia Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Llarnagraht          | 1 | ((210414-dekralotyzacja-asimear)) |
| Lothar Diakon        | 1 | ((200715-sabotaz-netrahiny)) |
| Maciej Żarand        | 1 | ((210804-infernia-jest-nasza)) |
| Maciek Kalmarzec     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Marcel Biekakis      | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Marcelina Trzęsiel   | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Marian Witaczek      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Marianna Lemurczak   | 1 | ((200923-magiczna-burza-w-raju)) |
| Mariusz Tubalon      | 1 | ((210414-dekralotyzacja-asimear)) |
| Markus Wąż           | 1 | ((240131-anomalna-mavidiz)) |
| Martauron Attylla    | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Mateusz Sowiński     | 1 | ((200415-sabotaz-miecza-swiatla)) |
| Melania Akacja       | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Melwin Sito          | 1 | ((200610-ixiacka-wersja-malictrix)) |
| Michał Teriakin      | 1 | ((210630-listy-od-fanow)) |
| Milena Blakenbauer   | 1 | ((210224-sentiobsesja)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Mirela Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Morrigan d'Tirakal   | 1 | ((210421-znudzona-zaloga-inferni)) |
| Mścigrom Verlen      | 1 | ((210224-sentiobsesja)) |
| Natalia Gwozdnik     | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Natalia Miszryk      | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Nataniel Morlan      | 1 | ((210818-siostrzenica-morlana)) |
| Nikodem Sowiński     | 1 | ((200923-magiczna-burza-w-raju)) |
| OA Bakałarz          | 1 | ((210120-sympozjum-zniszczenia)) |
| OA Mikiptur          | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| OA Odkupienie        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OE Lord Savaron      | 1 | ((210630-listy-od-fanow)) |
| OE Piękna Elena      | 1 | ((210428-infekcja-serenit)) |
| Ofelia Morlan        | 1 | ((210818-siostrzenica-morlana)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Oliwia Karelan       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| ONS Mavidiz          | 1 | ((240131-anomalna-mavidiz)) |
| OO Alaya             | 1 | ((210519-osiemnascie-oczu)) |
| OO Alkaris           | 1 | ((200624-ratujmy-castigator)) |
| OO Aurelion          | 1 | ((200708-problematyczna-elena)) |
| OO Galaktyczny Tucznik | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| OO Isratazir         | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Kariatyda         | 1 | ((210609-sekrety-kariatydy)) |
| OO Kastor            | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Opresor           | 1 | ((210616-nieudana-infiltracja-inferni)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Pandora           | 1 | ((220622-lewiatan-za-pandore)) |
| OO Samotność Gwiazd  | 1 | ((210804-infernia-jest-nasza)) |
| OO Straszliwy Pająk  | 1 | ((220622-lewiatan-za-pandore)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Tezifeng          | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Tucznik Trzeci    | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| OO Tvarana           | 1 | ((200715-sabotaz-netrahiny)) |
| OO Welgat            | 1 | ((200722-wielki-kosmiczny-romans)) |
| Percival Diakon      | 1 | ((200715-sabotaz-netrahiny)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Rafał Grambucz       | 1 | ((210428-infekcja-serenit)) |
| Rafał Perikas        | 1 | ((210311-studenci-u-verlenow)) |
| Remigiusz Alkarenit  | 1 | ((231018-anomalne-awarie-athamarein)) |
| Remigiusz Błyszczyk  | 1 | ((211013-szara-nawalnica)) |
| Remigiusz Falorin    | 1 | ((210630-listy-od-fanow)) |
| Rita Stratos         | 1 | ((240131-anomalna-mavidiz)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Rozalia Teirik       | 1 | ((210106-sos-z-haremu)) |
| Rozalia Wączak       | 1 | ((200624-ratujmy-castigator)) |
| Rufus Niegnat        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Rufus Samszar        | 1 | ((210311-studenci-u-verlenow)) |
| Rufus Warkoczyk      | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Rzeźnik Parszywiec Diakon | 1 | ((220622-lewiatan-za-pandore)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Saitaer              | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Samuel Fanszakt      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Sarian Xadaar        | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| SC Fecundatis        | 1 | ((210818-siostrzenica-morlana)) |
| SC Korona Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Sebastian Alarius    | 1 | ((200826-nienawisc-do-swin)) |
| Sebastian Namczek    | 1 | ((200408-o-psach-i-krysztalach)) |
| Seraf Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| SP Plugawy Jaszczur  | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| SP Światło Nadziei   | 1 | ((220223-stabilizacja-keldan-voss)) |
| Stefan Jamniczek     | 1 | ((201118-anastazja-bohaterka)) |
| Suwan Chankar        | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Sylwia Perikas       | 1 | ((210311-studenci-u-verlenow)) |
| Szczepan Myksza      | 1 | ((200715-sabotaz-netrahiny)) |
| Szczepan Olgrod      | 1 | ((200106-infernia-martyn-hiwasser)) |
| Szymon Orzesznik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| TAI Eszara d'Castigator | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| TAI XT-723 d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI Zefiris          | 1 | ((210630-listy-od-fanow)) |
| Talia Miraris        | 1 | ((240110-wieczna-wojna-bladawira)) |
| Tara Ogniczek        | 1 | ((240131-anomalna-mavidiz)) |
| Tomasz Rewernik      | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Tristan Rialirat     | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Tymek Samszar        | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Tymoteusz Czerw      | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Uśmiechniczka Konstrukcjonistka Aurora Diakon | 1 | ((231018-anomalne-awarie-athamarein)) |
| Wioletta Keiril      | 1 | ((200106-infernia-martyn-hiwasser)) |
| Wojciech Kuszar      | 1 | ((191025-nocna-krypta-i-bohaterka)) |
| Wojciech Mykirło     | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |