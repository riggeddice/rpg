---
categories: profile
factions: 
owner: public
title: Ellarina Samarintael
---

# {{ page.title }}


# Generated: 



## Fiszki


* Egzotyczna Piękność | @ 221123-egzotyczna-pieknosc-na-astralnej-flarze
* ENCAO:  +-00+ | Spontaniczna;; Chipper;; | VALS: Power, Hedonism >> Family | DRIVE: Wyciągnąć kogoś z bagna, dobrze wyjść za mąż | @ 221123-egzotyczna-pieknosc-na-astralnej-flarze

### Wątki


historia-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221123-egzotyczna-pieknosc-na-astralnej-flarze | Egzotyczna Piękność z Isigtand; jej przyjaciele są na Nonarionie jako collateral a ona straciła grazer. Przyjaciółka Darii. W desperackiej sytuacji. Arianna dała jej szansę i przeniosła ją na "maskotkę / jednostkę od morale". | 0100-09-12 - 0100-09-15 |
| 221130-astralna-flara-w-strefie-duchow | na polecenie Arianny śpiewała 'martwej' Zarralei co posłużyło do jej 'mrocznego wskrzeszenia' przez Ariannę. W szoku, obwinia się, bo przyczyniła się do tej reanimacji. Potem wraz z nekroZarraleą zrobiła koncert, co ją mocniej straumatyzowało, ale podniosło morale załogi. | 0100-10-05 - 0100-10-08 |
| 221214-astralna-flara-kontra-domina-lucis | przeprowadziła akcję propagandową (używając Kirei jako propsa) pokazującą noktianom, że warto się poddać bo nie mają jak obronić tego co mają teraz jak Orbiter o tym wie. Zresztą bardzo skutecznie. | 0100-10-09 - 0100-10-11 |
| 221221-astralna-flara-i-nowy-komodor | kamień spadł jej z serca ze zniszczeniem Zarralei; zorganizowała wybitne widowisko z Eleną i Władawcem, pokazującym ich mistrzostwo i zaufanie do siebie. Ostrzegła Ariannę o tym widowisku (bardzo nie chce Ariannie podpaść) | 0100-11-07 - 0100-11-10 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 221130-astralna-flara-w-strefie-duchow | bardzo nieszczęśliwa; lubiła Zarraleę, która została zmieniona w nekroTAI. W szoku - Zarralea została 'dark awakened' i przesunęła lojalność w Ariannę, acz rozpoznaje swoją dawną koleżankę (Ellarinę). Ellarina bardzo dobrze ukrywa swoje emocje; it is what it is, ale reewaluowała wszystko co wie o Orbiterze. Obwinia siebie za stworzenie nekroTAI i mroczne wskrzeszenie Zarralei. Nie chce po śmierci służyć Ariannie. Będzie grzeczna i zrobi co Arianna każe. | 0100-10-08

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 4 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Daria Czarnewik      | 4 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| OO Astralna Flara    | 4 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| OO Athamarein        | 4 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Gabriel Lodowiec     | 3 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Kajetan Kircznik     | 3 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor)) |
| Leszek Kurzmin       | 3 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| NekroTAI Zarralea    | 3 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Arnulf Perikas       | 2 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow)) |
| Elena Verlen         | 2 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor)) |
| Kirea Rialirat       | 2 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Maja Samszar         | 2 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor)) |
| Władawiec Diakon     | 2 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor)) |
| Axel Nargan          | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Erwin Pies           | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gerwazy Kircznik     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hubert Kerwelenios   | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Kaspian Certisarius  | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Klarysa Jirnik       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Leo Kasztop          | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Marcel Kulgard       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| OO Loricatus         | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Salazar Bolza        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Sarian Xadaar        | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| SCA Hadiah Emas      | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Szczepan Myrczek     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Szymon Wanad         | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Tomasz Dojnicz       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Tristan Rialirat     | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |