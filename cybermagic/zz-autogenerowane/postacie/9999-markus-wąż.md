---
categories: profile
factions: 
owner: public
title: Markus Wąż
---

# {{ page.title }}


# Generated: 



## Fiszki


* gardzi Arianną za Bladawira, słucha poleceń, sztywny, przygotowany na wszystko, szybki refleks i duma | @ 240131-anomalna-mavidiz

### Wątki


triumfalny-powrot-arianny
naprawa-swiata-przez-bladawira

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240131-anomalna-mavidiz             | nie lubi załogi Inferni za współpracę z Bladawirem, ale absolutny profesjonalista; kompetentny pilot Tivra który dogonił Mavidiz. Zderzył się z Mavidiz przez elementy Alteris, co sprawiło mu ogromny wstyd. | 0111-01-03 - 0111-01-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Bladawir      | 1 | ((240131-anomalna-mavidiz)) |
| Arianna Verlen       | 1 | ((240131-anomalna-mavidiz)) |
| Borys Kragin         | 1 | ((240131-anomalna-mavidiz)) |
| Eustachy Korkoran    | 1 | ((240131-anomalna-mavidiz)) |
| Grigor Tarnow        | 1 | ((240131-anomalna-mavidiz)) |
| Igor Stratos         | 1 | ((240131-anomalna-mavidiz)) |
| Karl Murnoff         | 1 | ((240131-anomalna-mavidiz)) |
| Klaudia Stryk        | 1 | ((240131-anomalna-mavidiz)) |
| ONS Mavidiz          | 1 | ((240131-anomalna-mavidiz)) |
| OO Tivr              | 1 | ((240131-anomalna-mavidiz)) |
| Raoul Lavanis        | 1 | ((240131-anomalna-mavidiz)) |
| Rita Stratos         | 1 | ((240131-anomalna-mavidiz)) |
| Tara Ogniczek        | 1 | ((240131-anomalna-mavidiz)) |