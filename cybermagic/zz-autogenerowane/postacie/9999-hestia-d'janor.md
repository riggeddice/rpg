---
categories: profile
factions: 
owner: public
title: Hestia d'Janor
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201025-kraloth-w-parku-janor        | nadal pod kontrolą Minerwy; pomogła Minerwie zlokalizować kralotha. | 0110-10-29 - 0110-10-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Lucjusz Blakenbauer  | 1 | ((201025-kraloth-w-parku-janor)) |
| Maciej Oczorniak     | 1 | ((201025-kraloth-w-parku-janor)) |
| Minerwa Metalia      | 1 | ((201025-kraloth-w-parku-janor)) |
| Pięknotka Diakon     | 1 | ((201025-kraloth-w-parku-janor)) |