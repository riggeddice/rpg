---
categories: profile
factions: 
owner: public
title: TAI Nephthys
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220323-zatruta-furia-gaulronow      | "żywa" TAI; wkręciła Mumurnika że musi do tego dojść i koordynowała dane by wszyscy dostali to co potrzebują. | 0111-11-27 - 0111-12-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dominika Perikas     | 1 | ((220323-zatruta-furia-gaulronow)) |
| Erwin Mumurnik       | 1 | ((220323-zatruta-furia-gaulronow)) |
| Feliks Ketran        | 1 | ((220323-zatruta-furia-gaulronow)) |
| Graniec Borgon       | 1 | ((220323-zatruta-furia-gaulronow)) |
| Lamia Akacja         | 1 | ((220323-zatruta-furia-gaulronow)) |
| Maciej Parczak       | 1 | ((220323-zatruta-furia-gaulronow)) |
| Suwan Chankar        | 1 | ((220323-zatruta-furia-gaulronow)) |