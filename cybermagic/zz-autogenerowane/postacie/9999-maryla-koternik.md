---
categories: profile
factions: 
owner: public
title: Maryla Koternik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211019-czarodziejka-ktora-jednak-moze-sie-zabic | z jakichś przyczyn skupiała się na noktianach i na Teresie. Płaci za informacje. Studentka AMZ ostatniego roku, dwa lata starsza od Klaudii. | 0084-12-20 - 0084-12-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arnulf Poważny       | 1 | ((211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Klaudia Stryk        | 1 | ((211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Ksenia Kirallen      | 1 | ((211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Mariusz Trzewń       | 1 | ((211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Talia Aegis          | 1 | ((211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Teresa Mieralit      | 1 | ((211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |