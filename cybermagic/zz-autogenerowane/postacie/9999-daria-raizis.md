---
categories: profile
factions: 
owner: public
title: Daria Raizis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220720-infernia-taksowka-dla-lycoris | PAST: grzeczna dziewczyna, która próbowała wkręcić Eustachego i Ardillę w coś z terraformacji; ACTUAL: odkryła osy Trianai w life support, została dziabnięta, dodała do logów informacje i półprzytomna dała uprawnienia do tematów medycznych i life support ukochanemu Jankowi i reszcie Zespołu. | 0093-01-20 - 0093-01-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Bartłomiej Korkoran  | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Celina Lertys        | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Czesław Żuczek       | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Eustachy Korkoran    | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Jan Lertys           | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Kamil Wraczok        | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Lycoris Kidiron      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| OO Infernia          | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Rafał Kidiron        | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Wiktor Turkalis      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |