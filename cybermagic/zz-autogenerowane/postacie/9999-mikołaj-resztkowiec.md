---
categories: profile
factions: 
owner: public
title: Mikołaj Resztkowiec
---

# {{ page.title }}


# Generated: 



## Fiszki


* górnik w przestrzeni Blakvela | @ 231027-planetoida-bogatsza-niz-byc-powinna
* OCEAN: E+N- |Nie kłania się nikomu;; Ciekawski;; Oczytany, ceni wiedzę dla wiedzy| VALS: Hedonism, Achievement| DRIVE: Drama | @ 231027-planetoida-bogatsza-niz-byc-powinna

### Wątki


planetoidy-kazimierza
sekret-mirandy-ceres

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231027-planetoida-bogatsza-niz-byc-powinna | lubi zaczynać dramę i antagonizować np. Kazimierza. Poza tym - wystarczająco kompetentny i nic wielkiego nie zrobił. | 0108-06-29 - 0108-07-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Bartek Burbundow     | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Gotard Kicjusz       | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Grzegorz Fabutownik  | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Juliusz Cieślawok    | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Kazimierz Zamglis    | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Łucjan Torwold       | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Miranda Ceres        | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Prokop Umarkon       | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| SC Królowa Przygód   | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Seweryn Grzęźlik     | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |