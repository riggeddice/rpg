---
categories: profile
factions: 
owner: public
title: Keira Amarco d'Namertel
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210928-wysadzony-zywy-scigacz       | bardzo lekko ubrana (eufemizm) zabójczyni Ernesta. Mało mówi. Brunetka. W jakiś sposób ma dostęp do kinezy (przemieszczanie się, akceleracja) i do ostrza eterycznego. Ale zabójca powinien być człowiekiem a nie magiem. | 0111-07-26 - 0111-07-27 |
| 211228-akt-o-ktorym-marysia-nie-wie | zabójczyni wysłana w roli infiltratorki, by pozyskać akt Marysi. Jest dobra, ale nie tak dobra jak Torszecki, który był szybszy. No i nie zna terenu który infiltruje. | 0111-09-09 - 0111-09-12 |
| 220111-marysiowa-hestia-rekinow     | podczas "ćwiczeń" zaplanowanych przez Ernesta by wyciągnąć "przypadkiem" Mimozę wpadła, załatwiła kilku magów, pokonała feromony kralotyczne i wyciągnęła Melissę. | 0111-09-16 - 0111-09-19 |
| 220222-plaszcz-ochronny-mimozy      | jednostka szturmowa Ernesta, odbiła się od apartamentu Mimozy. Nie umie jej zinfiltrować. | 0111-09-21 - 0111-09-25 |
| 240305-lea-strazniczka-lasu         | bardzo niezadowolona z uwagi na to, że Lea łamie zasady strojów eternijskich (tabu). Marysia ją uspokaja, zajmując się tą sprawą. | 0111-10-21 - 0111-10-25 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220111-marysiowa-hestia-rekinow     | Arkadia Verlen wraca zrobić z nią porządek. Ernest zbyt się szarogęsi. | 0111-09-19

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ernest Namertel      | 5 | ((210928-wysadzony-zywy-scigacz; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 240305-lea-strazniczka-lasu)) |
| Karolina Terienak    | 5 | ((210928-wysadzony-zywy-scigacz; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 240305-lea-strazniczka-lasu)) |
| Marysia Sowińska     | 5 | ((210928-wysadzony-zywy-scigacz; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 240305-lea-strazniczka-lasu)) |
| Liliana Bankierz     | 2 | ((211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow)) |
| Lorena Gwozdnik      | 2 | ((211228-akt-o-ktorym-marysia-nie-wie; 220222-plaszcz-ochronny-mimozy)) |
| Rafał Torszecki      | 2 | ((210928-wysadzony-zywy-scigacz; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Arkadia Verlen       | 1 | ((240305-lea-strazniczka-lasu)) |
| Azalia Sernat d'Namertel | 1 | ((210928-wysadzony-zywy-scigacz)) |
| Bogdan Gwiazdocisz   | 1 | ((240305-lea-strazniczka-lasu)) |
| Daniel Terienak      | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Diana Tevalier       | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Franek Bulterier     | 1 | ((210928-wysadzony-zywy-scigacz)) |
| Hestia d'Rekiny      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Jeremi Sowiński      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Lea Samszar          | 1 | ((240305-lea-strazniczka-lasu)) |
| Malena Barandis      | 1 | ((240305-lea-strazniczka-lasu)) |
| Marek Samszar        | 1 | ((240305-lea-strazniczka-lasu)) |
| Mariusz Kupieczka    | 1 | ((240305-lea-strazniczka-lasu)) |
| Michał Klabacz       | 1 | ((240305-lea-strazniczka-lasu)) |
| Mimoza Elegancja Diakon | 1 | ((220222-plaszcz-ochronny-mimozy)) |
| Napoleon Bankierz    | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Triana Porzecznik    | 1 | ((240305-lea-strazniczka-lasu)) |
| Żorż d'Namertel      | 1 | ((210928-wysadzony-zywy-scigacz)) |