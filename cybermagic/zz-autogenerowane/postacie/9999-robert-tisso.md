---
categories: profile
factions: 
owner: public
title: Robert Tisso
---

# {{ page.title }}


# Generated: 



## Fiszki


* Główny elektryk, Inżynier | @ 240117-echo-z-odmetow-przeszlosci
* Wyszedł sprawdzić co z generatorami energii, od razu po opuszczeniu umarł. | @ 240117-echo-z-odmetow-przeszlosci

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240117-echo-z-odmetow-przeszlosci   | znaleziony przez Kelpie martwy, pokrojony i złożony do kupy, ożywiony przez bazę. Na jego ciele widać ślady po składaniu. | 0096-01-24 - 0096-01-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anna Ivanova         | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Elżbieta Sanchez     | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Gerald Barowiecki    | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Jędrzej Sanchez      | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Klasa Dyplomata      | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Klasa Inżynier       | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Klasa Oficer Ochrony | 1 | ((240117-echo-z-odmetow-przeszlosci)) |