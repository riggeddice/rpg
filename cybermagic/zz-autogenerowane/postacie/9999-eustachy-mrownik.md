---
categories: profile
factions: 
owner: public
title: Eustachy Mrownik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190928-ostatnia-misja-tarna         | naukowiec astoriański; powiązany z mafią. Uratowany przez Tarna przed skażeniem, współpracował z nim mimo oporów ze strachu przed alternatywą. | 0110-01-27 - 0110-01-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| BIA Tarn             | 1 | ((190928-ostatnia-misja-tarna)) |
| Hestia d'Tiamenat    | 1 | ((190928-ostatnia-misja-tarna)) |
| Pedro Ronfak         | 1 | ((190928-ostatnia-misja-tarna)) |
| Talia Aegis          | 1 | ((190928-ostatnia-misja-tarna)) |
| Wiktor Satarail      | 1 | ((190928-ostatnia-misja-tarna)) |