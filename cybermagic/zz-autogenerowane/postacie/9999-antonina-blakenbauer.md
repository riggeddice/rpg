---
categories: profile
factions: 
owner: public
title: Antonina Blakenbauer
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230418-zywy-artefakt-w-gwiazdoczach | tienka, bukiety, znaczenie kwiatów, trucizny; chce mieć częsty dostęp do Kalejdoskopu do konstrukcji płaszczki; poproszona przez Elenę o wsparcie (ktoś używa Esuriit i chyba zrzuca winę na Blakenbauerów), poszła, odkryła że to żywy artefakt i wysłała niuchacze by znalazły sprawcę. Oddała smycze Robinsonowi, bo przecież 'dama nie będzie ciągana przez niuchacze od tego ma ludzi'. Paradoksem sformowała niedźwiedziogory (mandragory ale niegroźne) śpiewające serenady, zwalczane przez sentisieć Eleny. | 0094-10-04 - 0094-10-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adelaida Samszar     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Elena Samszar        | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Joachim Pulkmocz     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Neidria Lazvarin     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Robinson Porzecznik  | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Sara Mazirin         | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |