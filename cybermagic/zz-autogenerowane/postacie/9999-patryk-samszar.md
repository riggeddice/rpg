---
categories: profile
factions: 
owner: public
title: Patryk Samszar
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: A+N- | Nie boi się żadnego ryzyka. Nie martwi się opinią innych. Lubi towarzystwo i nie znosi samotności | VALS: Stimulation, Self-Direction | DRIVE: Znaleźć prawdę za legendami. | @ 231109-komodor-bladawir-i-korona-woltaren
* inżynier i ekspert od mechanizmów, działa świetnie w warunkach stresowych | @ 231109-komodor-bladawir-i-korona-woltaren

### Wątki


triumfalny-powrot-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231025-spiew-nielalki-na-castigatorze | tien na Castigatorze, szybko kojarzy. Inżynier i najbliższy przyjaciel Anny. Wpierw pomagał na mostku nie patrząc na ryzyko dla siebie, potem pomógł zinterpretować rysunek Anny i powiedział Ariannie o przemycie, mimo niezadowoleniu Marty (której wszyscy się trochę boją). | 0110-10-24 - 0110-10-26 |
| 231109-komodor-bladawir-i-korona-woltaren | niekompetentny w walce i TYLKO DLATEGO PRZETRWAŁ starcie z terrorformem, ale chętny i nieco _zbyt_ stabilny emocjonalnie. Zafascynowany Lidią ("jaka śliczna dziewczynka!"). Wykonuje robotę tak dobrze jak się można spodziewać po młodym tienie w kosmosie; o dziwo, faktyczny asset XD. | 0110-10-27 - 0110-11-03 |
| 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira | chciał być na ćwiczeniach z Arianną, ale ona obawiając się Bladawira odmówiła. Nacisnął na Martę by napisała mu rekomendację ale nie zadziałało. Gdy Zaara zaproponowała mu dowodzenie Paprykowcem i zrobienie _pain amplifiers_, zgodził się - Arianna da radę! Gdy doszło do pełnej operacji, robił co mógł mimo braku doświadczenia. Kupił czas Ariannie. | 0110-11-26 - 0110-11-30 |
| 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze | (nieobecny) wraz z Martą planują, jak pokonać kralotha; też korzysta z faktu milszej Eleny. | 0110-12-19 - 0110-12-21 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Elena Verlen         | 4 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Klaudia Stryk        | 4 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Marta Keksik         | 4 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Arianna Verlen       | 3 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Leszek Kurzmin       | 3 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Antoni Bladawir      | 2 | ((231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Feliks Walrond       | 2 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren)) |
| Igor Arłacz          | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Konstanty Keksik     | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| OO Infernia          | 2 | ((231025-spiew-nielalki-na-castigatorze; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| OO Paprykowiec       | 2 | ((231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Aneta Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Anna Tessalon        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Eleonora Perłamila   | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Eustachy Korkoran    | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Julia Myrczek        | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Kamil Burgacz        | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Kamil Lyraczek       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Kosmicjusz Tanecznik Diakon | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Krzysztof Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Lars Kidironus       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Leona Astrienko      | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Lidia Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Łucja Larnecjat      | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Martyn Hiwasser      | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| OO Castigator        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Raoul Lavanis        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| SC Korona Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| TAI Eszara d'Castigator | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Tomasz Rewernik      | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Wojciech Mykirło     | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Zaara Mieralit       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |