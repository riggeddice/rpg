---
categories: profile
factions: 
owner: public
title: Jonatan Piegacz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210330-pletwalowcy-na-goldarionie   | pierwszy oficer Płetwala Błękitnego; dobry duch i "boy scout" Płetwala Błękitnego. Przodownik pracy, wspiera Martynę w każdej sytuacji w jakiej jest w stanie, nieważne co o tym myśli. | 0109-03-11 - 0109-03-26 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210330-pletwalowcy-na-goldarionie   | porażony prądem w sabotażu elementu przestępczego na Goldarionie. 2 tygodnie regeneracji (bo medical na Goldarionie jest jaki jest...). | 0109-03-26

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Permin          | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Aleksander Leszert   | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Janusz Krumlod       | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Karolina Kartusz     | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Martyna Bianistek    | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| SC Goldarion         | 1 | ((210330-pletwalowcy-na-goldarionie)) |