---
categories: profile
factions: 
owner: public
title: Tomasz Afagrel
---

# {{ page.title }}


# Generated: 



## Fiszki


* kapral grupy wydzielonej pod dowództwem Larnecjata; były przemytnik | @ 230813-jedna-tienka-przybywa-na-pomoc
* OCEAN: (E+O+): nie boi się ryzyka i zawsze pierwszy do szydery czy okazji. "Jeśli jest zysk, warto spróbować." | @ 230813-jedna-tienka-przybywa-na-pomoc
* VALS: (Hedonism, Power): pierwszy szuka okazji do dorobienia czy dobrej zabawy. Straszny hustler. "Nikt nie dostanie za darmo niczego. Trzeba chcieć i działać." | @ 230813-jedna-tienka-przybywa-na-pomoc
* Core Wound - Lie: "Może jestem z bogatego domu, ale wojna zniszczyła wszystko co mieliśmy" - "KAŻDĄ okazję trzeba wykorzystać, WSZYSTKIEGO spróbować - by nie żałować." | @ 230813-jedna-tienka-przybywa-na-pomoc
* Styl: Głośny, pełen energii, uśmiech drwiący i szyderczy. Zawsze szuka sposobu na zabawę czy okazję. Lojalny wobec "swoich". Kolekcjonuje fanty. | @ 230813-jedna-tienka-przybywa-na-pomoc
* metakultura: Atarien: "moja rodzina miała wszystko i wszystko straciła. Moja kolej odbudowania potęgi rodziny." | @ 230813-jedna-tienka-przybywa-na-pomoc

### Wątki


salvagerzy-lohalian

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230813-jedna-tienka-przybywa-na-pomoc | kapral, wesoły człowiek który jako pierwszy porozmawiał z tienką (a nie musiał). Pogadał z nią o magii i odśnieżaniu. Ogólnie, wesoły pozytywny koleś. | 0095-09-08 - 0095-09-13 |
| 231024-rozbierany-poker-do-zwalczania-interis | zaprojektował plan jak obejrzeć nieubrane tienki - gra w strip poker, Maurycy wprowadzi sprawę, Paweł będzie oszukiwał. Tylko, że Elena oszukiwała lepiej (magią). Potraktował sprawę z humorem. | 0095-10-08 - 0095-10-10 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Estella Gwozdnik     | 2 | ((230813-jedna-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Mikołaj Larnecjat    | 2 | ((230813-jedna-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Adelajda Kalmiris    | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Antoni Paklinos      | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |
| Cyprian Mirisztalnik | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Elena Samszar        | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Jaromir Gaburon      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Jarosław Mirelski    | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Maurycy Derwisz      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Paweł Lawarczak      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Rachela Brześniak    | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |
| Serafin Gwozdnik     | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |