---
categories: profile
factions: 
owner: public
title: Xavier Kalwiert
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240313-ostatni-lot-valuvanok        | techniczny agent Valuvanok (inżynier + hacker); zbudował ekran ochronny przed promieniowaniem, co było kluczowym krokiem do bezpiecznego dotarcia do Jaspera. W walce z potworami wykorzystał granaty odłamkowe z radioaktywnymi odłamkami, tworząc skuteczną broń przeciwko wrogom. W pułapce zastawionej przez Plagę uratował Jaspera, ale to go zdehermetyzowało i doprowadziło do jego śmierci. | 0082-11-28 - 0082-11-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ewelina Kalwiert     | 1 | ((240313-ostatni-lot-valuvanok)) |
| Jasper Arimetus      | 1 | ((240313-ostatni-lot-valuvanok)) |
| Mara Arimetus        | 1 | ((240313-ostatni-lot-valuvanok)) |
| OnS Valuvanok        | 1 | ((240313-ostatni-lot-valuvanok)) |