---
categories: profile
factions: 
owner: public
title: Marian Witaczek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240110-wieczna-wojna-bladawira      | XO Bladawira, na Adilidam. Bladawir wysłał go do komunikacji z Arianną i Klaudią by niepotrzebnie nie antagonizować. | 0110-12-24 - 0110-12-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Bladawir      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Arianna Verlen       | 1 | ((240110-wieczna-wojna-bladawira)) |
| Dormand Miraris      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Ernest Bankierz      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Kazimierz Darbik     | 1 | ((240110-wieczna-wojna-bladawira)) |
| Klaudia Stryk        | 1 | ((240110-wieczna-wojna-bladawira)) |
| Samuel Fanszakt      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Talia Miraris        | 1 | ((240110-wieczna-wojna-bladawira)) |
| Władawiec Diakon     | 1 | ((240110-wieczna-wojna-bladawira)) |
| Zaara Mieralit       | 1 | ((240110-wieczna-wojna-bladawira)) |