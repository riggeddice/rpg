---
categories: profile
factions: 
owner: public
title: Michał Klabacz
---

# {{ page.title }}


# Generated: 



## Fiszki


* maintenance technician in a factory that produces advanced robotic limbs, CURSED ONE, 33 | @ 230325-ten-nawiedzany-i-ta-ukryta
* ENCAO:  0+++- |Pełny godności;;Sceptyczny;;Odpowiedzialny;;Ascetyczny| VALS: Benevolence, Achievement >> Tradition| DRIVE: Harmonia i akceptacja | @ 230325-ten-nawiedzany-i-ta-ukryta
* styl: rzeczowy, staroświecki, pomocny | @ 230325-ten-nawiedzany-i-ta-ukryta
* przewaga: jak się do czegoś przyłoży... | @ 230325-ten-nawiedzany-i-ta-ukryta

### Wątki


magiczne-szczury-podwierckie

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230325-ten-nawiedzany-i-ta-ukryta   | kiedyś przyjaciel Lei, potem poświęcony przez nią w rytuale. Potem Ola z czystej głupoty mu dokuczała (jak wyglądałbyś w tej sukience? słuchaj mam problem z kolegą co byś poradził o 2 rano?). Scraftował bransoletę i przez Paradoks Lei został opętany przez efemerycznego horrora. Wpada w pętlę zniszczenia, ale Daniel transportuje go do Lei która mu pomoże. | 0111-10-16 - 0111-10-17 |
| 240305-lea-strazniczka-lasu         | nieszczęśnik, którego uratowała Lea od jej własnego efemerycznego horrora. Nie ucierpiał trwale. Może już opuścić Dzielnicę Rekinów. | 0111-10-21 - 0111-10-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolina Terienak    | 2 | ((230325-ten-nawiedzany-i-ta-ukryta; 240305-lea-strazniczka-lasu)) |
| Lea Samszar          | 2 | ((230325-ten-nawiedzany-i-ta-ukryta; 240305-lea-strazniczka-lasu)) |
| Aleksandra Burgacz   | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Arkadia Verlen       | 1 | ((240305-lea-strazniczka-lasu)) |
| Bogdan Gwiazdocisz   | 1 | ((240305-lea-strazniczka-lasu)) |
| Daniel Terienak      | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Ernest Namertel      | 1 | ((240305-lea-strazniczka-lasu)) |
| Franek Bulterier     | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Keira Amarco d'Namertel | 1 | ((240305-lea-strazniczka-lasu)) |
| Malena Barandis      | 1 | ((240305-lea-strazniczka-lasu)) |
| Marek Samszar        | 1 | ((240305-lea-strazniczka-lasu)) |
| Mariusz Kupieczka    | 1 | ((240305-lea-strazniczka-lasu)) |
| Marysia Sowińska     | 1 | ((240305-lea-strazniczka-lasu)) |
| Nadia Uprewien       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Oliwier Czepek       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Rupert Mysiokornik   | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Triana Porzecznik    | 1 | ((240305-lea-strazniczka-lasu)) |