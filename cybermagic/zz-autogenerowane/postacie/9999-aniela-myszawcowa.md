---
categories: profile
factions: 
owner: public
title: Aniela Myszawcowa
---

# {{ page.title }}


# Generated: 



## Fiszki


* salvager Serien (Seiren Local / advancer / signal operations) faeril: "Im doskonalsza technologia, tym lepiej kontrolujemy sytuację"   <-- FOX | @ 230104-to-co-zostalo-po-burzy
* (ENCAO: -0+00 |Wszystko rozwiążę teorią;;Przedsiębiorcza i pomysłowa| VALS: Achievement, Face| DRIVE: Neikatis ma tyle sekretów i je zrozumiem) | @ 230104-to-co-zostalo-po-burzy
* "Dzięki technologii Neikatis stanie przed nami otworem. Tyle jeszcze nie rozumiemy, ale nauką i postępem złamiemy tą planetę" | @ 230104-to-co-zostalo-po-burzy
* **objęta na sesji przez Fox** | @ 230125-whispraith-w-jaskiniach-neikatis
* salvager Seiren (Seiren Local / advancer / signal operations) faeril: "Im doskonalsza technologia, tym lepiej kontrolujemy sytuację" | @ 230125-whispraith-w-jaskiniach-neikatis

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230104-to-co-zostalo-po-burzy       | advancer / signal operations Seiren; wykrywa krystalniki i trasuje. Granatem sonicznym zniszczyła dużego krystalnika, ale tydzień była poza akcją. Dekodując dane znalazła sygnał Kornelii. | 0092-10-26 - 0092-10-28 |
| 230125-whispraith-w-jaskiniach-neikatis | wysyła wiadomości do Kornelii; zagarnęła ją łychą w burzy piaskowej. Doszła do tego, że Benek nie żył, gdy Mirka i Kornelia go widziały. Zauważyła, że dziwne stwory się nie zbliżają, więc na pokładzie Seiren jest whispraith. | 0092-10-29 - 0092-11-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anna Seiren          | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Antoni Grzypf        | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Cyprian Kugrak       | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Eustachy Korkoran    | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| JAN Seiren           | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| JAN Uśmiech Kamili   | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Michał Uszwon        | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Rufus Seiren         | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Zofia d'Seiren       | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Kalia Awiter         | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Kornelia Lichitis    | 1 | ((230125-whispraith-w-jaskiniach-neikatis)) |
| Rafał Kidiron        | 1 | ((230104-to-co-zostalo-po-burzy)) |