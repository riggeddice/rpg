---
categories: profile
factions: 
owner: public
title: Marcel Nieciesz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190422-pustogorski-konflikt         | sympatyczny terminus z Fortu Mikado w Pustogorze, który padł ofiarą mimika symbiotycznego i porwał dwie Miasteczkowiczanki. Jego mimik został pokonany przez Cienia. | 0110-04-02 - 0110-04-03 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Erwin Galilien       | 1 | ((190422-pustogorski-konflikt)) |
| Karla Mrozik         | 1 | ((190422-pustogorski-konflikt)) |
| Olaf Zuchwały        | 1 | ((190422-pustogorski-konflikt)) |
| Pięknotka Diakon     | 1 | ((190422-pustogorski-konflikt)) |
| Wojmił Siwywilk      | 1 | ((190422-pustogorski-konflikt)) |