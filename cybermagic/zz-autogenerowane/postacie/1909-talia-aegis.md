---
categories: profile
factions: 
owner: public
title: Talia Aegis
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "noktianka"
* owner: "public"
* title: "Talia Aegis"


## Paradoks

Humanistyczna i pro-ludzka biomantka - chroniąca AI jako formę życia.

## Motywacja

### O co walczy

- Saitaer musi zostać zniszczony lub opanowany
- prawa dla AI i BIA
- świat znowu będzie dobry dla magów i ludzi- pokój i kontrola

### Przeciw czemu walczy

- wszelkie przejawy niekontrolowanej energii: ixion, esuriit
- traktowanie AI jako zabawek czy narzędzia a nie istoty świadome
- wojny, konflikty militarne, przemoc, krzywdzenie innych
- nieodpowiedzialne igranie z siłami których nie rozumiemy

## Działania

### Specjalność

- pamięta jeszcze Erę Kosmiczną; ma ogromną wiedzę i doświadczenie
- konstruktorka TAI czy BIA. Psychotroniczka i neuronautka najwyższej klasy
- potrafi ukryć to co wie, może nawet na torturach (multi-twine)
- wyjątkowo nie wychodzi jej umieranie

### Słabość

- nie ma wiele siły ani energii i bardzo szybko się męczy
- jej moc magiczna jest słaba; wypadła poza Paradygmat
- Z uwagi na zdecydowane poglądy jest lekceważona i niezbyt ceniona

### Akcje

- ktokolwiek jej o coś nie poprosi, zwykle się zgadza - mniej roboty i konfliktów
- jest bezgranicznie uczciwa - nie intryguje przeciw innym, da się przekonać faktom i dowodom- jak długo może je zweryfikować
- oschła; trzyma ludzi na dystans. Preferuje kontakt z Al.
- ostrożność w ruchach i wiecznie odległe spojrzenie zdradzają jej wiek.
- podejście lekarza - nie krzywdź, nie rań, nie odbieraj życia. Też wobec Al.
- lubi pozostawać w ciszy i cieniu. To już nie jest jej świat.
- identyfikuje się z kulturą noktiańską

### Znaczące czyny

- sprzęgła Nikolę z Finis Vitae przez BIA 5 kategorii. Do dziś ma koszmary.
- wprowadziła BIA jako warunek konieczny przetrwania Noctis po Pęknięciu.
- pierwsza rzuciła temat 'Nie wolno nam zniszczyć Astorii' podczas Inwazji Noctis.

## Mechanika

### Archetypy

psychotronik, biomanta, naukowiec, medyk polowy

### Motywacje

?

## Inne

### Wygląd

?


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210926-nowa-strazniczka-amz         | 47 lat; ekspert od TAI i BIA. Pomaga Arnulfowi i Tymonowi zrobić Strażniczkę Alair łącząc technologie BIA z Eszarą. W zamian za to - oni pomagają jej uratować jak najwięcej BIA i TAI; składowane są w Złomiarium w AMZ. | 0084-06-14 - 0084-06-26 |
| 211010-ukryta-wychowanka-arnulfa    | przed jej domem pojawiają się jakieś manifestacje i próby napaści nocą..? Tak czy inaczej, dowiedziawszy się o problemach Strażniczki natychmiast w nocy się zebrała by jej pomóc. Jej wezwanie wzbudziło zaskoczenie Ksenii i Felicjana. | 0084-12-11 - 0084-12-12 |
| 211019-czarodziejka-ktora-jednak-moze-sie-zabic | ochraniana przez jakichś typów (mafię), konfiguruje seksboty na "mroczniejsze przyjemności". | 0084-12-20 - 0084-12-24 |
| 240114-o-seksbotach-i-syntetycznych-intelektach | musi Klaudię uczyć o TAI, więc zaczęła. Nie chce tego robić, ale Klaudia ją przekonała. Wyjaśniła Klaudii jak działają Syntetyczne Intelekty, jak to działa, że to żywe istoty. Aha, i ma problemy bo Ralena (o czym nie wie) próbuje sabotować jej laboratorium i ją skrzywdzić. Dzięki Klaudii Sasza osłonił Talię. TAK, cały czas pracuje nad seksbotami - m.in. wprowadza w nie TAI militarne i daje im szansę na lepsze życie. | 0085-01-13 - 0085-01-18 |
| 220119-sekret-samanty-arienik       | podjęła się naprawy Samanty Arienik i przeniesienie ją w TAI. Ustabilizowała jej pamięć, usunęła inhibitory i przeniosła do "bycia żywą TAI". Uratowała WSZYSTKICH Arieników którzy byli zamknięci w miragentach. Wielki sukces. Acz coś zepsuła z seksbotami nad którymi pracowała dla mafii... | 0085-07-21 - 0085-07-23 |
| 230627-ratuj-mlodziez-dla-kajrata   | psychotroniczka połączona sympatią z Mitrią; gdy Mitria jest torturowana, wiedza Talii jest przekazywana Mitrii i przez nią - hiperpsychotronikom. Talia czuje echa, nie może spać i degeneruje. | 0095-08-20 - 0095-08-25 |
| 190827-rozpaczliwe-ratowanie-bii    | rozpaczliwie chciała uratować BIA 3 generacji; udawała, że to Wiktor Satarail zaatakował Tiamenat - a to była ona. Uratowana przed więzieniem przez Ernesta Kajrata. | 0110-01-18 - 0110-01-21 |
| 190928-ostatnia-misja-tarna         | robiła wszystko by uratować Tarna i... w sumie się udało. Odkażała skażeńców Wiktora, budowała antidotum itp. | 0110-01-27 - 0110-01-28 |
| 190828-migswiatlo-psychotroniczek   | przyszła do Pięknotki bo nie chce by subturingowa BIA którą musiała zbudować dla Puszczoka stała się zagrożeniem. | 0110-02-08 - 0110-02-10 |
| 190830-kto-wrobil-alana             | chciała poważnie uszkodzić Alana za scrambler AI, ale jako, że to było podłożone... odpuściła. Nie będzie atakować niewinnego. | 0110-06-03 - 0110-06-05 |
| 200202-krucjata-chevaleresse        | nie stoi za działaniami przeciw Pustogorowi ani Grzymościowi tym razem; wyjaśniła Pięknotce wymagania na TAI 3. Przyjrzy się "dziwnej TAI" w okolicy. | 0110-07-24 - 0110-07-27 |
| 200414-arystokraci-na-trzesawisku   | okazuje się, że pomogła Tymonowi sformować Strażniczkę Alair i doprowadzić ją do pełnego funkcjonowania. | 0110-08-04 - 0110-08-05 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210926-nowa-strazniczka-amz         | uczy Klaudię Stryk o TAI/BIA, wykorzystywaniu magitechu w stylu noktiańskim i odnośnie wolności AI. | 0084-06-26
| 211019-czarodziejka-ktora-jednak-moze-sie-zabic | ma opiekę i ochronę jakichś typów (mafii) - chronią ją przed napaścią i demonstrantami. Zajmuje się seksbotami. | 0084-12-24
| 240114-o-seksbotach-i-syntetycznych-intelektach | opinia tej co robi seksboty dla mafii, nie dość że noktianka to jeszcze zła noktianka. Zła reputacja w okolicy. Zadawanie się z nią jest lekko toksyczne reputacyjnie. | 0085-01-18

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 5 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 220119-sekret-samanty-arienik; 240114-o-seksbotach-i-syntetycznych-intelektach)) |
| Ksenia Kirallen      | 5 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 220119-sekret-samanty-arienik; 240114-o-seksbotach-i-syntetycznych-intelektach)) |
| Mariusz Trzewń       | 5 | ((190827-rozpaczliwe-ratowanie-bii; 200202-krucjata-chevaleresse; 200414-arystokraci-na-trzesawisku; 210926-nowa-strazniczka-amz; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Pięknotka Diakon     | 5 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek; 190830-kto-wrobil-alana; 200202-krucjata-chevaleresse; 200414-arystokraci-na-trzesawisku)) |
| Tymon Grubosz        | 4 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek; 210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Arnulf Poważny       | 3 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Ernest Kajrat        | 3 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek; 230627-ratuj-mlodziez-dla-kajrata)) |
| Strażniczka Alair    | 3 | ((200414-arystokraci-na-trzesawisku; 210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Teresa Mieralit      | 3 | ((211010-ukryta-wychowanka-arnulfa; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 240114-o-seksbotach-i-syntetycznych-intelektach)) |
| BIA Tarn             | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190928-ostatnia-misja-tarna)) |
| Diana Tevalier       | 2 | ((190830-kto-wrobil-alana; 200202-krucjata-chevaleresse)) |
| Felicjan Szarak      | 2 | ((211010-ukryta-wychowanka-arnulfa; 220119-sekret-samanty-arienik)) |
| Marek Puszczok       | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Minerwa Metalia      | 2 | ((190828-migswiatlo-psychotroniczek; 200202-krucjata-chevaleresse)) |
| Sasza Morwowiec      | 2 | ((211010-ukryta-wychowanka-arnulfa; 240114-o-seksbotach-i-syntetycznych-intelektach)) |
| AJA Szybka Strzała   | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Alan Bartozol        | 1 | ((190830-kto-wrobil-alana)) |
| Albert Kalandryk     | 1 | ((210926-nowa-strazniczka-amz)) |
| Amanda Kajrat        | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Arazille             | 1 | ((220119-sekret-samanty-arienik)) |
| Artur Michasiewicz   | 1 | ((190828-migswiatlo-psychotroniczek)) |
| Ataienne             | 1 | ((200202-krucjata-chevaleresse)) |
| Błażej Arienik       | 1 | ((220119-sekret-samanty-arienik)) |
| Damian Orion         | 1 | ((200202-krucjata-chevaleresse)) |
| Elena Samszar        | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Eustachy Mrownik     | 1 | ((190928-ostatnia-misja-tarna)) |
| Franciszek Leszczowik | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Herbert Samszar      | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Hestia d'Tiamenat    | 1 | ((190928-ostatnia-misja-tarna)) |
| Ignacy Myrczek       | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Impresja Ignicja Incydencja Diakon | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Karolina Erenit      | 1 | ((190830-kto-wrobil-alana)) |
| Karolinus Samszar    | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Keraina d'Orion      | 1 | ((200202-krucjata-chevaleresse)) |
| Malictrix d'Itaran   | 1 | ((200202-krucjata-chevaleresse)) |
| Maria Arienik        | 1 | ((220119-sekret-samanty-arienik)) |
| Maryla Koternik      | 1 | ((211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Mateusz Kardamacz    | 1 | ((200202-krucjata-chevaleresse)) |
| Mitria Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Pedro Ronfak         | 1 | ((190928-ostatnia-misja-tarna)) |
| Ralena Drewniak      | 1 | ((240114-o-seksbotach-i-syntetycznych-intelektach)) |
| Sabina Kazitan       | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Samanta Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| Sławomir Arienik     | 1 | ((220119-sekret-samanty-arienik)) |
| Sławomir Niejadek    | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Tomasz Tukan         | 1 | ((200202-krucjata-chevaleresse)) |
| Urszula Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| Wacław Samszar       | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Wargun Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Wiktor Satarail      | 1 | ((190928-ostatnia-misja-tarna)) |
| Wojciech Zermann     | 1 | ((190830-kto-wrobil-alana)) |