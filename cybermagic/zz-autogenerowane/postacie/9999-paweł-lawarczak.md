---
categories: profile
factions: 
owner: public
title: Paweł Lawarczak
---

# {{ page.title }}


# Generated: 



## Fiszki


* szeregowy, znany z niezwykłej zręczności i brawurowej gry w karty; advancer | @ 231024-rozbierany-poker-do-zwalczania-interis
* OCEAN: (E+N-): Charyzmatyczny, gadatliwy i uwielbia być w centrum uwagi. Działa zanim pomyśli, ryzykant. "Paweł zawsze ma jakąś sztuczkę, żart czy pomysł. Nieważne jak źle jest, Paweł rozchmurzy." | @ 231024-rozbierany-poker-do-zwalczania-interis
* VALS: (Hedonism, Security): Żyje dla chwili, uwielbia emocje i ryzyko, ale dba by nie spotkała go krzywda. Małe oszustwa to kolejny sposób na świetną zabawę. "Najlepszy do zabawy, ale go pilnuj..." | @ 231024-rozbierany-poker-do-zwalczania-interis
* Core Wound - Lie: "Jako dziecko byłem złodziejem i akrobatą; jak pili to bili" - "Jeśli wszyscy się śmieją i postrzegają mnie za niegroźnego, jestem bezpieczny" | @ 231024-rozbierany-poker-do-zwalczania-interis
* Styl: Wiecznie uśmiechnięty, zwinny jak kot, zawsze ma gotowy dowcip na ustach. Ma sztuczki karciane, nic nie bierze na poważnie i ogólnie co się nie stanie - jest radość. | @ 231024-rozbierany-poker-do-zwalczania-interis
* metakultura: Atarien: "To są moi ludzie. Tylko mnie wolno ich oszukiwać." | @ 231024-rozbierany-poker-do-zwalczania-interis

### Wątki


salvagerzy-lohalian
rehabilitacja-eleny-samszar

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231024-rozbierany-poker-do-zwalczania-interis | świetnie oszukuje w karty (acz Elena lepiej) i robi 'sztuczki magiczne'. Najweselszy z zespołu. Przyjął przegraną z pokorą i szerokim uśmiechem. | 0095-10-08 - 0095-10-10 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adelajda Kalmiris    | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Cyprian Mirisztalnik | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Elena Samszar        | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Estella Gwozdnik     | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Jaromir Gaburon      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Jarosław Mirelski    | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Maurycy Derwisz      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Mikołaj Larnecjat    | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Tomasz Afagrel       | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |