---
categories: profile
factions: 
owner: public
title: Mimoza Diakon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220809-20-razy-za-duzo-esuriit      | operates in Szczeliniec trying to show people how an Aurum aristocrat should act, pouring money into the region and improving the lives of people here. This time she has hired the group of Aurum-based mercenary agents (or freelancers, it's the same) so they can solve the strange problems troubling Sunflower Decontamination-Purification plant. | 0111-09-30 - 0111-10-06 |
| 220819-tank-as-a-love-letter        | Rekin; Shark; when she heart the Team is dealing with Esuriit she ordered them to stop because she did not want them to get hurt. She cares about her people. Also, wanted to give a chance to Henryk Wkrąż, who blew it. Mimosa won't trust him again. | 0111-10-07 - 0111-10-08 |
| 220816-jak-wsadzic-ule-alanowi      | blokuje rozbudowę TAI Hestia bo region potrzebuje tych części bardziej; porozmawiała z Marysią i pokazała, że ona nie udaje - ona taka jest | 0111-10-12 - 0111-10-13 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220809-20-razy-za-duzo-esuriit      | receives backlash because "it's her stuff which is corrupted by Esuriit" and "her political shit is damaging the local region". She won't be happy. | 0111-10-06
| 220819-tank-as-a-love-letter        | strong reduction of reputation because of Henryk and Iwona. Ariel Kubunczak leaves her. She lost the operation, even if she helped the region. | 0111-10-08
| 220816-jak-wsadzic-ule-alanowi      | TAK, ona naprawdę nie gra w politykę. Próbuje być najlepszą arystokratką Aurum jaką powinna być każda arystokratka. Exemplar. Żywy przykład. I Marysia to wie. | 0111-10-13

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Izabela Selentik     | 2 | ((220809-20-razy-za-duzo-esuriit; 220819-tank-as-a-love-letter)) |
| Michał Kabarniec     | 2 | ((220809-20-razy-za-duzo-esuriit; 220819-tank-as-a-love-letter)) |
| Talia Mikrit         | 2 | ((220809-20-razy-za-duzo-esuriit; 220819-tank-as-a-love-letter)) |
| Ariel Kubunczak      | 1 | ((220819-tank-as-a-love-letter)) |
| Arkadiusz Terienak   | 1 | ((220819-tank-as-a-love-letter)) |
| AU Flara Astorii     | 1 | ((220819-tank-as-a-love-letter)) |
| Daniel Terienak      | 1 | ((220819-tank-as-a-love-letter)) |
| Henryk Wkrąż         | 1 | ((220819-tank-as-a-love-letter)) |
| Iwona Perikas        | 1 | ((220819-tank-as-a-love-letter)) |
| Karol Walgoryn       | 1 | ((220809-20-razy-za-duzo-esuriit)) |
| Karolina Terienak    | 1 | ((220816-jak-wsadzic-ule-alanowi)) |
| Marysia Sowińska     | 1 | ((220816-jak-wsadzic-ule-alanowi)) |
| Tomasz Tukan         | 1 | ((220816-jak-wsadzic-ule-alanowi)) |
| Urszula Miłkowicz    | 1 | ((220816-jak-wsadzic-ule-alanowi)) |