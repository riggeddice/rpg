---
categories: profile
factions: 
owner: public
title: Grzegorz Czerw
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211009-szukaj-serpentisa-w-lesie    | 25; żołnierz towarzyszący magom AMZ by ich chronić. Dowodzi. Dał Felicjanowi dowodzić (dobre dla młodego maga). Dobrze rozstawił perymetr, lecz padł do serpentisa Agostino. | 0084-11-13 - 0084-11-14 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Agostino Karwen      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Dariusz Skórnik      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Edelmira Neralis     | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Felicjan Szarak      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Klaudia Stryk        | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Ksenia Kirallen      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Mariusz Trzewń       | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Strażniczka Alair    | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Udom Rapnak          | 1 | ((211009-szukaj-serpentisa-w-lesie)) |