---
categories: profile
factions: 
owner: public
title: Feliks Mirtan
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200502-po-co-atakuja-minerwe        | kiedyś agent Orbitera i nie powiązany z Astorią - teraz żyje w Aurum jako sprzedawca zmilitaryzowanych TAI. Zrobił anty-TAI użyte przez Infiltratora. | 0110-08-26 - 0110-08-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Halina Sermniek      | 1 | ((200502-po-co-atakuja-minerwe)) |
| Kreacjusz Diakon     | 1 | ((200502-po-co-atakuja-minerwe)) |
| Minerwa Metalia      | 1 | ((200502-po-co-atakuja-minerwe)) |
| Nikodem Larwent      | 1 | ((200502-po-co-atakuja-minerwe)) |
| Pięknotka Diakon     | 1 | ((200502-po-co-atakuja-minerwe)) |