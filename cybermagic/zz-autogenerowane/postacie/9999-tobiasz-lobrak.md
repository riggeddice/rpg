---
categories: profile
factions: 
owner: public
title: Tobiasz Lobrak
---

# {{ page.title }}


# Generated: 



## Fiszki


* (nie Nativis), 44 lata | @ 230215-terrorystka-w-ambasadorce
* (ENCAO:  ---0+ |Lekceważy obowiązki;;Skryty;;Marzycielski | VALS: Face, Power >> Face| DRIVE: TORMENT) | @ 230215-terrorystka-w-ambasadorce
* dostawca dużej ilości luksusowych środków do Nativis po niższej cenie | @ 230215-terrorystka-w-ambasadorce
* uwielbia znęcać się nad kobietami. Upodobał sobie Misterię. | @ 230215-terrorystka-w-ambasadorce
* "Możesz mnie zabić, ale będę ZAWSZE żył w twojej głowie, mała" | @ 230215-terrorystka-w-ambasadorce

### Wątki


kidiron-zbawca-nativis

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230215-terrorystka-w-ambasadorce    | straszy Misterię, że ona wpadnie w jego ręce. Obiecuje jej to. Zamknięty w panic roomie, Kidiron go wyciągnął. Ma powiązanie z Syndykatem Aureliona, ale chyba gra na dwa fronty bo Kidiron daje mu okazję zaspokojenia mroku. | 0093-02-22 - 0093-02-23 |
| 230628-wojna-o-arkologie-nativis-konsolidacja-sil | wie, że Bartłomiej Korkoran nim gardzi, więc połączył siły z Laurencjuszem i Tymonem, oddając im Izabellę do zabawy. CHYBA ma coś wspólnego z Infiltratorem. On lub L+T. | 0093-03-27 - 0093-03-28 |
| 230719-wojna-o-arkologie-nativis-nowa-regentka | okazało się, że jest aktywnym agentem Syndykatu i najpewniej on stoi za Infiltratorem i atakiem na Arkologię Nativis. Traci kontrolę nad Arkologią i atakiem. Stracił maga Syndykatu. | 0093-03-28 - 0093-03-29 |
| 230726-korkoran-placi-cene-za-nativis | ujawnił się Eustachemu jako aktywny agent Aureliona. Negocjuje z nim otwarcie, chce pomóc Arkologii Nativis - ale pokazuje, że potęga Aureliona i tak zmiażdży Nativis i wszystko inne. | 0093-03-29 - 0093-03-30 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230215-terrorystka-w-ambasadorce    | wie, że Misteria chciała go zabić. Od tej pory Tobiasz skupia się na próbie pozyskania Misterii i założenia jej neuroobroży. She WILL comply. | 0093-02-23

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 4 | ((230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Bartłomiej Korkoran  | 4 | ((230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Eustachy Korkoran    | 4 | ((230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Izabella Saviripatel | 3 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Marcel Draglin       | 3 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Ralf Tapszecz        | 3 | ((230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Tymon Korkoran       | 3 | ((230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230726-korkoran-placi-cene-za-nativis)) |
| Kalia Awiter         | 2 | ((230215-terrorystka-w-ambasadorce; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| OO Infernia          | 2 | ((230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Celina Lertys        | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Karol Lertys         | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Laurencjusz Kidiron  | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Rafał Kidiron        | 1 | ((230215-terrorystka-w-ambasadorce)) |