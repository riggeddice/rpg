---
categories: profile
factions: 
owner: public
title: Jędrzej Sanchez
---

# {{ page.title }}


# Generated: 



## Fiszki


* Specjalista ds. Bezpieczeństwa, Inżynier | @ 240117-echo-z-odmetow-przeszlosci
* Ekspert ds. bezpieczeństwa placówki badawczej, nadzorujący zabezpieczenia i środki ostrożności, również “szef ochrony”. Ekstramalnie znudzony i unika realnej pracy. Najchętniej do końca życia siedziałby w placówce, bo tu się nic nie robi. Silne więzi rodzinne, ochroni siostrę, mimo że udaje, że mu nie zależy [nie wychodzi] | @ 240117-echo-z-odmetow-przeszlosci
* Będzie chciał pozostać, jeśli to możliwe. Nie chce umierać, poprze siostrę. Ma dostęp do systemów awaryjnych stacji, może zarządzić kwarantannę i lockdown. | @ 240117-echo-z-odmetow-przeszlosci

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240117-echo-z-odmetow-przeszlosci   | interweniuje, gdy nieznane siły włamują się na stację oraz gdy Karolina jest potencjalnie zagrożona. | 0096-01-24 - 0096-01-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anna Ivanova         | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Elżbieta Sanchez     | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Gerald Barowiecki    | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Klasa Dyplomata      | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Klasa Inżynier       | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Klasa Oficer Ochrony | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Robert Tisso         | 1 | ((240117-echo-z-odmetow-przeszlosci)) |