---
categories: profile
factions: 
owner: public
title: Lars Kidironus
---

# {{ page.title }}


# Generated: 



## Fiszki


* asystent oficera naukowego, wierzy w Kidirona i jego wielkość i jest lojalny Eustachemu. Wszędzie widzi spiski. "Czemu amnestyki, hm?" | @ 231011-ekstaflos-na-tezifeng
* OCEAN: (N+C+): bardzo metodyczny i precyzyjny, wszystko kataloguje, mistrzowski archiwista. "Anomalia w danych jest znakiem potencjalnego zagrożenia" | @ 231011-ekstaflos-na-tezifeng
* VALS: (Stimulation, Conformity): dopasuje się do grupy i będzie udawał, że wszystko jest idealnie i w porządku. Ale _patrzy_. "Czemu mnie o to pytasz? Co chcesz usłyszeć?" | @ 231011-ekstaflos-na-tezifeng
* Core Wound - Lie: "wiedziałem i mnie wymazali, nie wiem CO wiedziałem" - "wszystko zapiszę, zobaczę, będę daleko i dojdę do PRAWDY" | @ 231011-ekstaflos-na-tezifeng
* Styl: coś między Starscreamem i Cyclonusem. Ostrożny, zawsze na baczności, analizujący każdy ruch i słowo innych. "Dlaczego pytasz? Co chcesz ukryć?" | @ 231011-ekstaflos-na-tezifeng

### Wątki


triumfalny-powrot-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231011-ekstaflos-na-tezifeng        | paranoiczny; z braku Klaudii on skanował Tezifeng, próbując określić, czy sygnatury życia się zgadzają. Przed insercją zrobił "MEGA NUDNĄ PREZENTACJĘ" - każdy członek załogi i jak groźny. O dziwo, pomogło to Ariannie, która poznała brak Egzotycznej Piękności. | 0110-10-20 - 0110-10-22 |
| 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira | na mostku zaatakował słownie Zaarę, że to spisek jej i Bladawira. Ale Infernia - jego zdaniem - sobie poradzi. Arianna załagodziła sprawę, ale ucieszył ją wybuch Larsa. | 0110-11-26 - 0110-11-30 |
| 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow | podniósł Ariannie, że jej servar (od Bladawira) ma zapasowy zbiornik z gazem. Co prawda jest fanem Eustachego, ale Arianny też nie da skrzywdzić. | 0110-12-03 - 0110-12-17 |
| 240124-mikiptur-zemsta-woltaren     | ostrzegł Eustachego o potencjalnych problemach z Władawcem i Bladawirem. Oczywiście, błędnie. | 0110-12-29 - 0111-01-01 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 4 | ((231011-ekstaflos-na-tezifeng; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240124-mikiptur-zemsta-woltaren)) |
| Antoni Bladawir      | 3 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240124-mikiptur-zemsta-woltaren)) |
| Klaudia Stryk        | 3 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240124-mikiptur-zemsta-woltaren)) |
| Elena Verlen         | 2 | ((231011-ekstaflos-na-tezifeng; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Eustachy Korkoran    | 2 | ((231011-ekstaflos-na-tezifeng; 240124-mikiptur-zemsta-woltaren)) |
| OO Infernia          | 2 | ((231011-ekstaflos-na-tezifeng; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Raoul Lavanis        | 2 | ((231011-ekstaflos-na-tezifeng; 240124-mikiptur-zemsta-woltaren)) |
| Zaara Mieralit       | 2 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow)) |
| Kamil Lyraczek       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Klaudiusz Terienak   | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Leona Astrienko      | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Leszek Kurzmin       | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Marta Keksik         | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Natalia Gwozdnik     | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OA Mikiptur          | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| OO Castigator        | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OO Isratazir         | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| OO Paprykowiec       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| OO Tezifeng          | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Patryk Samszar       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Władawiec Diakon     | 1 | ((240124-mikiptur-zemsta-woltaren)) |