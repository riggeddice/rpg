---
categories: profile
factions: 
owner: public
title: Klasa Dyplomata
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240117-echo-z-odmetow-przeszlosci   | przekonuje Karolinę, że na Kelpie znajduje się laboratorium, którego mogłaby użyć, przekonuje kapitana Barowieckiego do swojego pomysłu na pomoc stacji i wcielenie jej do organizacji, przekonuje Elżbietę do współpracy oraz Annę, by pracowała dla nich, dając jej nowy sens życia. | 0096-01-24 - 0096-01-25 |
| 231119-tajemnicze-tunele-sebirialis | Sigwald Asidar z Lux Umbrarum; rozmawiając z dokerami dowiaduje się o blackships, z Radą o problemach, łapie powiązania kto-z-kim-kiedy i ogólnie robi świetną robotę śledczą. Praktycznie fraternizując się ze wszystkimi doszedł do tego że nikt nic nie wie, ale zapewnił ich współpracę. | 0104-11-20 - 0104-11-24 |
| 231213-polowanie-na-biosynty-na-szernief | przekonanie Kalisty do współpracy, przekonanie szerszej populacji że szczepienie to jedyne wyjście (presja na Parasektowców), zrzucenie win z Agencji na Mawirowców i Kalistę. | 0105-07-26 - 0105-07-31 |
| 231122-sen-chroniacy-kochankow      | Ola; wydobyła od Feliny że ta wie gdzie jest Rovis, ułagodziła napięcia na stacji i doprowadziła do miękkiej ewakuacji Rovisa i Joli ze stacji za recykler. Skutecznie zarządziła naprawą sytuacji. | 0105-09-02 - 0105-09-05 |
| 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa | wyłapała różnice ideowe między drakolitami i doszła do radykalizacji. Podniosła pozycję Teraquid i dodała Teraquid zaufanie do Agencji. Nowa populacja i to przyjazna celowi. | 0105-12-11 - 0105-12-13 |
| 240214-relikwia-z-androida          | kampania informacyjna, która umożliwiła pomyślną ewakuację Vanessy, minimalizując szkody i zapewniając jej nową rolę jako ikony turystycznej. Też: negocjacje z 'shardem Aliny' wewnątrz TAI Szernief. | 0107-05-16 - 0107-05-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kalista Surilik      | 5 | ((231119-tajemnicze-tunele-sebirialis; 231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240214-relikwia-z-androida)) |
| Felina Amatanir      | 4 | ((231119-tajemnicze-tunele-sebirialis; 231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klasa Sabotażysta    | 4 | ((231119-tajemnicze-tunele-sebirialis; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240214-relikwia-z-androida)) |
| Klasa Hacker         | 3 | ((231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 240214-relikwia-z-androida)) |
| Klasa Inżynier       | 3 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240117-echo-z-odmetow-przeszlosci; 240214-relikwia-z-androida)) |
| Klasa Oficer Naukowy | 3 | ((231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 240214-relikwia-z-androida)) |
| Mawir Hong           | 3 | ((231122-sen-chroniacy-kochankow; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240214-relikwia-z-androida)) |
| Aerina Cavalis       | 2 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240214-relikwia-z-androida)) |
| Dorion Fughar        | 2 | ((231122-sen-chroniacy-kochankow; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Vanessa d'Cavalis    | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240214-relikwia-z-androida)) |
| Alina Mekran         | 1 | ((240214-relikwia-z-androida)) |
| Aniela Kafantelas    | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Anna Ivanova         | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Artur Tavit          | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Damian Orczakin      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Delgado Vitriol      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Elwira Barknis       | 1 | ((240214-relikwia-z-androida)) |
| Elżbieta Sanchez     | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Eryk Kawanicz        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Gabriel Septenas     | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Gerald Barowiecki    | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Ignatius Sozyliw     | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Janvir Krassus       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Jędrzej Sanchez      | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Jola-09 Szernief     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Kaldor Czuk          | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klasa Biurokrata     | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Klasa Oficer Ochrony | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Klaudiusz Widar      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Larkus Talvinir      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Leon Hurmniow        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Malik Darien         | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Marta Sarilit        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| OLU Luminarius       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Robert Tisso         | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Rovis Skarun         | 1 | ((231122-sen-chroniacy-kochankow)) |
| Sebastian-194        | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Sia-03 Szernief      | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Szymon Alifajrin     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Tadeusz Mekran       | 1 | ((240214-relikwia-z-androida)) |