---
categories: profile
factions: 
owner: public
title: Impresja Ignicja Incydencja Diakon
---

# {{ page.title }}


# Generated: 



## Fiszki


* ENCAO:  +--0+ | dramatic, spontaneous, liberty | VALS: Self-direction, Hedonism, Stimulation | DRIVE: Przygody!!! | @ 230606-piekna-diakonka-i-rytual-nirwany-koz
* styl: BR; dramatic, flamboyant, spontaneous, sensitive, "I lead you follow" | @ 230606-piekna-diakonka-i-rytual-nirwany-koz
* "wszystko jest jednym wielkim doświadczeniem, rytuałem przejścia, przeprowadzę Cię przez to!" | @ 230606-piekna-diakonka-i-rytual-nirwany-koz
* "ROZUMIEM co się tu dzieje" (fiksacja na kolejnej dziwnej teorii) | @ 230606-piekna-diakonka-i-rytual-nirwany-koz
* magia: "astralne wizje i mieszanie rzeczywistości"; "amplifikacja tego co jest" | @ 230606-piekna-diakonka-i-rytual-nirwany-koz
* ruch: "wchodzi, wszystko zmienia i pozostawia za sobą pożogę" | @ 230606-piekna-diakonka-i-rytual-nirwany-koz
* piękna brunetka | @ 230606-piekna-diakonka-i-rytual-nirwany-koz

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230606-piekna-diakonka-i-rytual-nirwany-koz | jako 'Itria' (23) Diakonka. Tym razem wpadła na pomysł rytuału nirwany kóz, bo Samszarowie mają kozy na pożarcie dla potworów i ona chce im umilić życie. Ona skupia się na kozach, ale Herbert i Maks przez nią szaleją i robią głupoty. Skończyło się na tym, że się przespała z Karolinusem, Herbertem i Maksem (w sekwencji) i jakkolwiek doszła do tego że to plan Karolinusa i Eleny, nie przeszkadzało jej to zupełnie. Rytuał nirwany kóz został osiągnięty. | 0095-08-15 - 0095-08-18 |
| 230627-ratuj-mlodziez-dla-kajrata   | Karolinus zrobił z niej żywą przynętkę dla Wacława. ŻYWO zainteresowana działaniami Zespołu - 'dołącza do zespołu' jako awatar chaosu. Zajmuje Wacława który olał eksfiltrację Warguna i Mitrii. Wyciągnie Wacława z Mrocznej Sekty. | 0095-08-20 - 0095-08-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 2 | ((230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata)) |
| Elena Samszar        | 2 | ((230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata)) |
| Herbert Samszar      | 2 | ((230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata)) |
| Karolinus Samszar    | 2 | ((230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata)) |
| Amanda Kajrat        | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Dźwiedź Łagodne Słowo | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Ernest Kajrat        | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Maks Samszar         | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Mitria Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Talia Aegis          | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Wacław Samszar       | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Wargun Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |