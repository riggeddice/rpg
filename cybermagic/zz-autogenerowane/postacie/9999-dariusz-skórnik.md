---
categories: profile
factions: 
owner: public
title: Dariusz Skórnik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211009-szukaj-serpentisa-w-lesie    | 26; żołnierz towarzyszący magom AMZ by ich chronić. Zna noktiański. Pokonany przez Agostino, tłumaczył potem Ksenię na noktiański i pomógł Ksenii przekonać serpentisów do leczenia Edelmiry i oddania się w ręce astorian. | 0084-11-13 - 0084-11-14 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Agostino Karwen      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Edelmira Neralis     | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Felicjan Szarak      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Grzegorz Czerw       | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Klaudia Stryk        | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Ksenia Kirallen      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Mariusz Trzewń       | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Strażniczka Alair    | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Udom Rapnak          | 1 | ((211009-szukaj-serpentisa-w-lesie)) |