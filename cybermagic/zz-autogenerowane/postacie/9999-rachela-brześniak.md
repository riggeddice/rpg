---
categories: profile
factions: 
owner: public
title: Rachela Brześniak
---

# {{ page.title }}


# Generated: 



## Fiszki


* czarodziejka Orbitera (katalistka i technomantka), dołączona do pomocy w dekontaminacji terenu; na uboczu, bo jest "dziwna" | @ 230813-jedna-tienka-przybywa-na-pomoc
* OCEAN: (N+C+): niezwykle ostrożna z czarowaniem i anomaliami, niewiele mówi i raczej trzyma się sama ze sobą. | @ 230813-jedna-tienka-przybywa-na-pomoc
* VALS: (Achievement, Universalism): Skupia się dokładnie na zadaniu, by je wykonać może pracować z każdym. "Dekontaminacja to moje zadanie. Zajmijcie się resztą." | @ 230813-jedna-tienka-przybywa-na-pomoc
* Core Wound - Lie: "Współpracowała z żywymi TAI i na jej oczach Rzieza zniszczył jej przyjaciół; myśli że to jej wina" - "Magia MUSI być kontrolowana. Wszystko co anomalne musi być reglamentowane." | @ 230813-jedna-tienka-przybywa-na-pomoc
* Styl: Cicha, pełna wewnętrznego ognia. Skupia się na dekontaminacji z żarliwością godną faerilki. | @ 230813-jedna-tienka-przybywa-na-pomoc
* metakultura: Faeril: "ciężką pracą da się rozwiązać każdy problem - to tylko kwestia cierpliwości i determinacji" | @ 230813-jedna-tienka-przybywa-na-pomoc

### Wątki


salvagerzy-lohalian

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230813-jedna-tienka-przybywa-na-pomoc | chorąża i czarodziejka; jest bardzo dziwna i nie ma kontekstu społecznego; niezwykle pracowita i próbuje nie czarować jak może uniknąć. Bardzo dobra w pisaniu raportów i dokumentów, niesamowicie precyzyjna, unika czarowania. Przez to że nie czaruje Skaża się lokalnymi energiami - to sprawia, że ma pewne dysfunkcje. Ale jak dostała rozkaz 'rzuć czar', rzuciła. Wściekła na Estellę. | 0095-09-08 - 0095-09-13 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230813-jedna-tienka-przybywa-na-pomoc | lekko Skażona energiami Finis Vitae, po rzuceniu zaklęcia się wyfiltruje sama. WŚCIEKŁA na Estellę. | 0095-09-13

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Paklinos      | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |
| Estella Gwozdnik     | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |
| Mikołaj Larnecjat    | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |
| Serafin Gwozdnik     | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |
| Tomasz Afagrel       | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |