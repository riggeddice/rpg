---
categories: profile
factions: 
owner: public
title: Isaura Velaska
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (E+A+): uspokajający głos w zespole, zawsze znajduje chwile na uśmiech i dowcip. "Nieważne co się dzieje, trzymaj fason i się uśmiechaj" | @ 230729-furia-poluje-na-furie
* VALS: (Harmony, Benevolence): "Zespół to jak rodzina. Musimy być dla siebie wsparciem." | @ 230729-furia-poluje-na-furie
* Core Wound - Lie: "Rodzina mnie porzuciła, ale Furie adoptowały" - "Jeśli zrobię dla Was rodzinę, wszyscy zawsze będziemy razem bez kłótni i sporów" | @ 230729-furia-poluje-na-furie
* Styl: Ostoja spokoju wśród chaosu, rozładowuje napięcia i tworzy poczucie wspólnoty i jedności | @ 230729-furia-poluje-na-furie
* Styl: Ostoja spokoju wśród chaosu, rozładowuje napięcia i tworzy poczucie wspólnoty i jedności; dystyngowana, zachowuje się jak arystokratka | @ 230730-skazone-schronienie-w-fortecy

### Wątki


pronoktianska-mafia-kajrata
furia-mataris-agentka-mafii

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230730-skazone-schronienie-w-fortecy | Furia współpracująca z Kajratem; wyciągnęła Amandę i Xaverę z ognia między Symlotosem a Czarnymi Czaszkami. | 0081-06-18 - 0081-06-21 |
| 230806-zwiad-w-iliminar-caos        | wraz z Garzinem wykonuje operację pomocy Symlotosowi, by Ayna i Leira były bezpieczne. | 0081-06-22 - 0081-06-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 2 | ((230730-skazone-schronienie-w-fortecy; 230806-zwiad-w-iliminar-caos)) |
| Ernest Kajrat        | 2 | ((230730-skazone-schronienie-w-fortecy; 230806-zwiad-w-iliminar-caos)) |
| Alaric Rakkeir       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Ayna Marialin        | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Caelia Calaris       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Dragan Halatis       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Edmund Garzin        | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Leira Euridis        | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Leon Varkas          | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Lestral Kirmanik     | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Raab Navan           | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Xavera Sirtas        | 1 | ((230730-skazone-schronienie-w-fortecy)) |