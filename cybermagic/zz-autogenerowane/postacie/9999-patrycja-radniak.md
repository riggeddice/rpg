---
categories: profile
factions: 
owner: public
title: Patrycja Radniak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190702-smieciornica-w-magitrowni    | czternastolatka, która walczyła o swojego ojca po jego śmierci - wpierw magitrownią, potem magią krwi. Za wszelką cenę. Uciekła, gdy okazało się że przegrała. | 0109-07-14 - 0109-07-17 |
| 201215-dziewczyna-i-pies            | chciała mieć prawdziwą rodzinę, więc jej pies jej to zapewnił. Spotkanie z trzema Rekinami sprawiło, że uciekła bezpiecznie. Była przez moment szczęśliwa, acz ich zabijała (czego nie czuje) | 0110-11-15 - 0110-11-17 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190704-magitrownia-finis-vitae      | dziewięciolatka; straciła matkę w katastrofie magitrowni. Magowie pomogli jej i jej ojcu, jak obiecali matce Patrycji w formie ducha. | 0104-08-10
| 190702-smieciornica-w-magitrowni    | 14latka; po raz pierwszy (i jak obiecała sobie, ostatni) użyła magii krwi do dostarczenia ojcu energii by powrócił | 0109-07-17
| 190702-smieciornica-w-magitrowni    | ma psa Lucka; jest to Skażeniec jej niesamowicie oddany i lojalny | 0109-07-17

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alia Naszemba        | 1 | ((190702-smieciornica-w-magitrowni)) |
| Andrzej Kuncerzyk    | 1 | ((201215-dziewczyna-i-pies)) |
| Daniel Terienak      | 1 | ((201215-dziewczyna-i-pies)) |
| Franciszek Zygmunt   | 1 | ((201215-dziewczyna-i-pies)) |
| Grzegorz Terienak    | 1 | ((201215-dziewczyna-i-pies)) |
| Irek Waczar          | 1 | ((190702-smieciornica-w-magitrowni)) |
| Izydor Grumczewicz   | 1 | ((201215-dziewczyna-i-pies)) |
| Jan Waczar           | 1 | ((190702-smieciornica-w-magitrowni)) |
| Karolina Terienak    | 1 | ((201215-dziewczyna-i-pies)) |
| Klara Orkaczyn       | 1 | ((190702-smieciornica-w-magitrowni)) |
| Paulina Mordoch      | 1 | ((201215-dziewczyna-i-pies)) |
| Przemysław Grumcz    | 1 | ((190702-smieciornica-w-magitrowni)) |
| Tadeusz Łaśnic       | 1 | ((201215-dziewczyna-i-pies)) |