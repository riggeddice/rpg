---
categories: profile
factions: 
owner: public
title: Serafin Gwozdnik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230813-jedna-tienka-przybywa-na-pomoc | tien i kuzyn Estelli, komunikuje się z nią i wspiera jej działania wzmacniające Orbiter. Pozytywny młody mag, LUBI Estellę i ją wspiera. Wziął na siebie badania tego terenu. | 0095-09-08 - 0095-09-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Paklinos      | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |
| Estella Gwozdnik     | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |
| Mikołaj Larnecjat    | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |
| Rachela Brześniak    | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |
| Tomasz Afagrel       | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |