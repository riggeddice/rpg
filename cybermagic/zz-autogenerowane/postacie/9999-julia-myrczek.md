---
categories: profile
factions: 
owner: public
title: Julia Myrczek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze | prowadzi własne eksperymenty z amplifikacją systemów podtrzymywania życia i stymulacją, by wzmocnić załogę. Fraternizuje się z technikami by to osiągnąć. | 0110-12-19 - 0110-12-21 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Elena Verlen         | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Eleonora Perłamila   | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Igor Arłacz          | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Kamil Burgacz        | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Klaudia Stryk        | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Konstanty Keksik     | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Kosmicjusz Tanecznik Diakon | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Leszek Kurzmin       | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Łucja Larnecjat      | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Marta Keksik         | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Martyn Hiwasser      | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Patryk Samszar       | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |