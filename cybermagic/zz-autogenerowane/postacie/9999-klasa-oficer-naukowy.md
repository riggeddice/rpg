---
categories: profile
factions: 
owner: public
title: Klasa Oficer Naukowy
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240111-zlomowanie-legendarnej-anitikaplan | zbadał Pereza i jakkolwiek doszło do Manifestacji, zrozumiał z jakimi energiami ma do czynienia. Stworzył detektory krewetkowe i pomógł w filtracji superstrumienia danych hackera. | 0084-04-02 - 0084-04-06 |
| 231213-polowanie-na-biosynty-na-szernief | bada Malika i ekstraktuje z ciała Parasekta, składa 'detektory krewetkowe', robi _feedback shock_ by ujawnić wszystkie osoby zarażone i gdy próbuje zrobić szczepionkę na Parasekta, zostaje zarażony przez Skażeńca. Ale przetrwa to. | 0105-07-26 - 0105-07-31 |
| 231122-sen-chroniacy-kochankow      | Walter; odnalazł starcie Alucis-Esuriit, zbadał śpiących i stworzył antidotum by móc ludzi wyciągnąć ze Snu. Znajdował rzeczy które do siebie nie pasowały i wyciągał z nich dalsze wnioski. | 0105-09-02 - 0105-09-05 |
| 231221-pan-skarpetek-i-odratowany-ogrod | poznał savarańskie legendy, gdy w Nie-Ogrodzie-Botanicznym doszło do 'powodzi', użył servara by ratować dzieci i nikt nie ucierpiał (choć servar zniszczony). Wyjaśnił Kaliście na czym polega problem z Pryzmatem i zobowiązał ją do milczenia. | 0106-04-25 - 0106-04-27 |
| 240117-dla-swych-marzen-warto       | uratował Estrila, wyciągnął kolejność wydarzeń i kto-kiedy, opracował odpowiedni detektor i upewnił się, że problem zniknął | 0106-11-04 - 0106-11-06 |
| 240214-relikwia-z-androida          | odkrycie przyczyny hipnotycznego wpływu na mieszkańców stacji i rozwój metody odtrucia, co pozwoliło na uratowanie większości osób pod wpływem narkotyków. Odkrycie energii Anteclis. | 0107-05-16 - 0107-05-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kalista Surilik      | 5 | ((231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Hacker         | 5 | ((231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 240111-zlomowanie-legendarnej-anitikaplan; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Sabotażysta    | 5 | ((231213-polowanie-na-biosynty-na-szernief; 231221-pan-skarpetek-i-odratowany-ogrod; 240111-zlomowanie-legendarnej-anitikaplan; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Felina Amatanir      | 4 | ((231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto)) |
| Mawir Hong           | 4 | ((231122-sen-chroniacy-kochankow; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Dyplomata      | 3 | ((231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 240214-relikwia-z-androida)) |
| Vanessa d'Cavalis    | 3 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Aerina Cavalis       | 2 | ((240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Artur Tavit          | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Klasa Biurokrata     | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240111-zlomowanie-legendarnej-anitikaplan)) |
| Klasa Inżynier       | 2 | ((231221-pan-skarpetek-i-odratowany-ogrod; 240214-relikwia-z-androida)) |
| Alina Mekran         | 1 | ((240214-relikwia-z-androida)) |
| Damian Orczakin      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Delgado Vitriol      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Dorion Fughar        | 1 | ((231122-sen-chroniacy-kochankow)) |
| Elwira Barknis       | 1 | ((240214-relikwia-z-androida)) |
| Estril Cavalis       | 1 | ((240117-dla-swych-marzen-warto)) |
| Ignatius Sozyliw     | 1 | ((231221-pan-skarpetek-i-odratowany-ogrod)) |
| Jola-09 Szernief     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Klaudiusz Widar      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Larkus Talvinir      | 1 | ((240117-dla-swych-marzen-warto)) |
| Maia Sakiran         | 1 | ((240117-dla-swych-marzen-warto)) |
| Malik Darien         | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Rovis Skarun         | 1 | ((231122-sen-chroniacy-kochankow)) |
| Sebastian-194        | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| SN Anitikaplan       | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| SN Varilen           | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Szymon Alifajrin     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Tadeusz Mekran       | 1 | ((240214-relikwia-z-androida)) |
| Twaróg Worl          | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Wiktor Worl          | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |