---
categories: profile
factions: 
owner: public
title: Twaróg Worl
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240111-zlomowanie-legendarnej-anitikaplan | kompetentny oficer Wiktora, choć go nie lubi (i zmienił imię po zakładzie). Podkrada, jest niskim człowiekiem, ale nie zdradzi szefa. Współpracuje z Agencją by dokopać szefowi. | 0084-04-02 - 0084-04-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klasa Biurokrata     | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Klasa Hacker         | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Klasa Oficer Naukowy | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Klasa Sabotażysta    | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| SN Anitikaplan       | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| SN Varilen           | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Wiktor Worl          | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |