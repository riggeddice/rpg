---
categories: profile
factions: 
owner: public
title: Alan Nierkamin
---

# {{ page.title }}


# Generated: 



## Fiszki


* eks-lokalny przewodnik, advancer | @ 221123-egzotyczna-pieknosc-na-astralnej-flarze
* ENCAO: ++-0- |Nietolerancyjny, musi być tak jak uważa;;Dokładnie przemyśli wszystko zanim coś powie| VALS: Power, Achievement >> Tradition| DRIVE: wprowadzi Orbiter na Nonarion | @ 221123-egzotyczna-pieknosc-na-astralnej-flarze

### Wątki


historia-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221116-astralna-flara-dociera-do-nonariona-nadziei | (ENCAO: ++-0- |Nietolerancyjny, musi być tak jak uważa;;Dokładnie przemyśli wszystko zanim coś powie| VALS: Power, Achievement >> Tradition| DRIVE: wprowadzi Orbiter na Nonarion); lokals z Nonariona, dołączył do Orbitera 2 lata temu i jest zakochany w Orbiterze. Chce zastąpić kulturę Nonariona kulturą Orbitera. Pomaga Lodowcowi i Kurzminowi w zrozumieniu tego miejsca. Advancer. Nie lubi sarderytów. Silny, solidny, masywny koleś. Kosmiczny górnik. Grazerowiec. Altinianin ("Nonarion needs to be cleansed by Orbiter's fire"). | 0100-09-08 - 0100-09-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alezja Dumorin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Arianna Verlen       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Arnulf Perikas       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Daria Czarnewik      | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Gabriel Lodowiec     | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Grażyna Burgacz      | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Kajetan Kircznik     | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Leo Kasztop          | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Leszek Kurzmin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Maja Samszar         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Marcel Kulgard       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| OO Astralna Flara    | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| OO Athamarein        | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| SCA Hadiah Emas      | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Szymon Wanad         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Tomasz Ruppok        | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Władawiec Diakon     | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |