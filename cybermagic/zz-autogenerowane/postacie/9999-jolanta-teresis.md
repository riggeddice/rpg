---
categories: profile
factions: 
owner: public
title: Jolanta Teresis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190626-upadek-enklawy-floris        | noktianka; władczyni roju + lekarz. Ważna w Floris. Dzięki niej udało się przetrwać w Maszynie, doprowadziła Konrada do działania i pomaga z technomancją. | 0110-05-28 - 0110-05-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ariela Sirmin        | 1 | ((190626-upadek-enklawy-floris)) |
| Ernest Kajrat        | 1 | ((190626-upadek-enklawy-floris)) |
| Hubert Kraborów      | 1 | ((190626-upadek-enklawy-floris)) |
| Konrad Czukajczek    | 1 | ((190626-upadek-enklawy-floris)) |
| Marcel Sowiński      | 1 | ((190626-upadek-enklawy-floris)) |
| Nikola Kirys         | 1 | ((190626-upadek-enklawy-floris)) |
| Roman Rymtusz        | 1 | ((190626-upadek-enklawy-floris)) |
| Szymon Maszczor      | 1 | ((190626-upadek-enklawy-floris)) |
| Wargun Ira           | 1 | ((190626-upadek-enklawy-floris)) |