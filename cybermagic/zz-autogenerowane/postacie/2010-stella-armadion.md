---
categories: profile
factions: 
owner: public
title: Stella Armadion
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "noctis, akademia magiczna zaczęstwa"
* owner: "public"
* title: "Stella Armadion"


## Kim jest

### W kilku zdaniach

Młoda (19) noktianka, która nie kojarzy życia na Noctis (miała <8 lat), uczennica Akademii Magicznej Zaczęstwa. Buntowniczka przeciwko wszystkiemu co widzi. Szuler z nosem w książkach; próbuje zrozumieć historię jaką była a nie propagandę astoriańską. Można powiedzieć, że nie ma w niej cienia uśmiechu - jest tylko knurd. Katalistka negacji.

### Co się rzuca w oczy

* Idealizuje Noctis i wszystko co Noctis sobą reprezentuje. Noctis jest tym miejscem bez wad, tym wyjątkowym.
* Obrabia i prezentuje dane tak, by jej pasowały jak najbardziej - ale potrafi dojść do prawdy.
* Uwielbia prowokować innych i pokazywać ich hipokryzję. Nienawidzi iluzji, niech wszyscy widzą świat w jego nagiej, upiornej glorii.
* Nadal twierdzi, że magia nie do końca jest prawdą. To tylko kontrolowana forma nauki, zniewolenie przez INNYCH. ONI oszukują społeczeństwo.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Jako projekt zaliczeniowy zrobiła z premedytacją holoprojekcję pokazującą sukcesy Noctis przeciwko Saitaerowi, robiąc wroga z Napoleona i burzę na sali.
* Mimo wieku utrzymuje się z hazardu w Kasynie Marzeń Grzymościa; ten ją wspiera, bo jest fajną nihilistyczną maskotką. Jest świetną szulerką.
* Odrestaurowała noktiańskie magitechowe rytuały negacji magii; zbierała trzy lata ślady. Świetnie wyjaśniają, czemu to nie magia a coś.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* PRACA: Szuler w Kasynie Marzeń w Zaczęstwie. Świetne wyczucie ryzyk, charakterów i prawdopodobieństwa.
* PRACA: Przetwarza, obrabia i prezentuje dane, też dane kryminalistyczne. Potrafi wyciągnąć każdy element ze śladów.
* SERCE: Chwała Noctis. Poznać historię Noctis. Poznać Elizę Irę. Pokazać, że Noctis to są "ci dobrzy". Bunt przeciw astoriańskiej glorii przeciw Noctis.
* SERCE: Kontrarianka - wierzy, że magii nie ma; to ICH narzędzie kontroli. Odrzuca kulturę astoriańską i Seilię. Kult danych i faktów.
* ATUT: Bardzo przenikliwa - niezwykle odporna na iluzje, manipulacje i zafałszowania rzeczywistości.
* ATUT: Katalistka negacji - w jej obecności magia działa słabiej. Całość umiejętności magicznych skupia na negacji... bo nie wierzy w magię.

### Charakterystyczne zasoby (3)

* COŚ: Noktiańskie magitechowe kryształy i rytuały negacji magii. Świetnie wyjaśniają, czemu to NIE magia a coś innego.
* KTOŚ: Talia Aegis - noktiańska ekspertka od TAI i BIA mieszkająca w Zaczęstwie.
* WIEM: Ogromna wiedza na wszelkie tematy okołonoktiańskie, łącznie z tymi których mieć nie powinna. Chcesz wiedzieć o Noctis? Jest Twoim kontaktem.
* OPINIA: Szulerka o nihilistycznym nastawieniu, która ma "fazę na kontrarianizm". W zasadzie niegroźna, acz świetna w antymagii.

### Typowe problemy z którymi sobie nie radzi (-3)

* SERCE: Nie ma wiele osób, miejsc czy struktur w których znajduje oparcie; jest twarda, ale łatwo pęka czy się rozsypuje.
* SERCE: Tak bardzo chroni Noctis, że nawet jak noktianie zrobili OCZYWISTE zło to będzie próbować ich wybielać. To samo dotyczy technologii noktiańskich.
* STRACH: Boi się Skażenia Magicznego i "dziwnych" rzeczy jakie magia robi z ludźmi i magami. STRASZNIE boi się Saitaera i ixionu.
* MAGIA: Tak bardzo jest na 'nie' jeśli chodzi o magię, że będzie sabotować moc swoich sojuszników by tylko mieć rację.

### Specjalne

* .

## Inne

### Wygląd

.

### Coś Więcej

* .

### Endgame

* v1: Skończy jako współpracownik Elizy Iry w Enklawie Noktiańskiej (dziś: Trzecim Raju).
* v2: Zostanie puryfikatorem w służbie Pustogoru; minie jej faza na "magia nie istnieje".


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201013-pojedynek-akademia-rekiny    | czarodziejka AMZ noktiańskiego pochodzenia; szulerka; zarabia na grach hazardowych. Cicha. Tu: wygrała z Rekinami i zamiast jej zapłacić, potraktowali ją proszkiem alergicznym. Liliana używa jej jako przykładu że trzeba tępić Rekiny. | 0110-10-14 - 0110-10-22 |
| 210817-zgubiony-holokrysztal-w-lesie | była na wycieczce AMZ dowodzonej przez Alana Bartozola; znalazła i schowała ko-matrycę Kuratorów który przekonał ją, że to technologia noktiańska. Ów kryształ wykradł potem Tukan. | 0111-06-22 - 0111-06-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kacper Bankierz      | 2 | ((201013-pojedynek-akademia-rekiny; 210817-zgubiony-holokrysztal-w-lesie)) |
| Liliana Bankierz     | 2 | ((201013-pojedynek-akademia-rekiny; 210817-zgubiony-holokrysztal-w-lesie)) |
| Alan Bartozol        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Aleksander Bemucik   | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Cyryl Perikas        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Ignacy Myrczek       | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Julia Kardolin       | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Justynian Diakon     | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Laura Tesinik        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Mariusz Trzewń       | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Marysia Sowińska     | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Napoleon Bankierz    | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Remor 340D           | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Robert Pakiszon      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Rupert Mysiokornik   | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Teresa Mieralit      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Tomasz Tukan         | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |