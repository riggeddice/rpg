---
categories: profile
factions: 
owner: public
title: Antoni Grzypf
---

# {{ page.title }}


# Generated: 



## Fiszki


* salvager Uśmiechu (owner) | @ 230104-to-co-zostalo-po-burzy
* (ENCAO: -+0-0 |Niepokojący;;Apatyczny| VALS: Family, Power >> Face, Hedonism| DRIVE: Odnaleźć Kamilę, Zadośćuczynienie za zdradę) | @ 230104-to-co-zostalo-po-burzy
* "Straciłem to co kochałem najbardziej. Tylko pracą mogę zagłuszyć pustkę." | @ 230104-to-co-zostalo-po-burzy
* kadencja: puste serce, obsesja na punkcie Kamili, nie ma uczuć | @ 230104-to-co-zostalo-po-burzy

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230104-to-co-zostalo-po-burzy       | przyjaciel Rufusa i Anny; porwał Annę by odzyskać swoją ukochaną Kamilę którą kiedyś zostawił gdzieś na jednym ze złóż w tej okolicy. | 0092-10-26 - 0092-10-28 |
| 230125-whispraith-w-jaskiniach-neikatis | wydrapane oczy, pieści zwłoki Benka i myśli że to Kamila. Mimo szczelności Scutiera. Wyciągnięty i uratowany przez Seiren, mimo Dotyku whispraitha. | 0092-10-29 - 0092-11-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aniela Myszawcowa    | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Anna Seiren          | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Cyprian Kugrak       | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Eustachy Korkoran    | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| JAN Seiren           | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| JAN Uśmiech Kamili   | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Michał Uszwon        | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Rufus Seiren         | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Zofia d'Seiren       | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Kalia Awiter         | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Kornelia Lichitis    | 1 | ((230125-whispraith-w-jaskiniach-neikatis)) |
| Rafał Kidiron        | 1 | ((230104-to-co-zostalo-po-burzy)) |