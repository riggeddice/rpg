---
categories: profile
factions: 
owner: public
title: Tog Worniacz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240125-wszystkie-sekrety-caralei    | idzie koncyliacyjnie, dogaduje się z nieperfekcyjnymi załogami Ferrelith oraz Caralei; kończy na Caralei z ramienia Bladawira (i jako osoba kontrolująca TAI). | 0110-12-29 - 0111-01-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Bladawir      | 1 | ((240125-wszystkie-sekrety-caralei)) |
| Dobromir Misiak      | 1 | ((240125-wszystkie-sekrety-caralei)) |
| Kazimierz Darbik     | 1 | ((240125-wszystkie-sekrety-caralei)) |
| Olaf Rawen           | 1 | ((240125-wszystkie-sekrety-caralei)) |