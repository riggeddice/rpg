---
categories: profile
factions: 
owner: public
title: Prokop Umarkon
---

# {{ page.title }}


# Generated: 



## Fiszki


*  | @ 231027-planetoida-bogatsza-niz-byc-powinna
* rola: kapitan, górnik, mały przedsiębiorca (trzech kumpli: PŁG) | @ 231027-planetoida-bogatsza-niz-byc-powinna
* personality: extravert, agreeable (outgoing, enthusiastic, trusting, empathetic) | @ 231027-planetoida-bogatsza-niz-byc-powinna
* values: power + conformity | @ 231027-planetoida-bogatsza-niz-byc-powinna
* wants: pragnie zostać KIMŚ, wydobyć się z bagna | @ 231027-planetoida-bogatsza-niz-byc-powinna

### Wątki


planetoidy-kazimierza
sekret-mirandy-ceres

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220327-wskrzeszenie-krolowej-przygod | górnik z Trzech Przyjaciół; został wrobiony w zostanie kapitanem Królowej Przygód. Chciał ją sprzedać blakvelowcom i mieć dobre życie, ale nie wyszło. | 0108-05-01 - 0108-05-16 |
| 231027-planetoida-bogatsza-niz-byc-powinna | nieskończenie optymistyczny, co prawie zgubiło zespół. Blakvelowcy kazali mu robić operacje górnicze, przyjął na klatę, ale do końca nie widział potwora Alteris. Na szczęście ufa swojej załodze. | 0108-06-29 - 0108-07-02 |
| 220329-mlodociani-i-pirat-na-krolowej | postawił się Sewerynowi w sprawie dzieci; mają mieć szansę. Chce pomóc zagrożonej jednostce (nawet jak piraci). Nie odrzuci życia Łucjana nawet jak to zabije groźnych piratów. | 0108-08-29 - 0108-09-09 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220329-mlodociani-i-pirat-na-krolowej | POWAŻNY OPIERDOL od blakvelowców, bo przedkłada jakieś dzieciaki nad finanse Blakvela i swoją załogę. | 0108-09-09

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Gotard Kicjusz       | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Łucjan Torwold       | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Miranda Ceres        | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| SC Królowa Przygód   | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Seweryn Grzęźlik     | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Anna Szrakt          | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Bartek Burbundow     | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Bartek Wudrak        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Berdysz Rozdzieracz  | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Damian Szczugor      | 1 | ((220327-wskrzeszenie-krolowej-przygod)) |
| Grzegorz Fabutownik  | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Helena Banbadan      | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Juliusz Cieślawok    | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Kara Prazdnik        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Kazimierz Zamglis    | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Mikołaj Resztkowiec  | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Romana Kundel        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Wojciech Kaznodzieja | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |