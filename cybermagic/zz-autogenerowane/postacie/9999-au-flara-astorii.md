---
categories: profile
factions: 
owner: public
title: AU Flara Astorii
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220819-tank-as-a-love-letter        | MEMORY. Anomalous Unit, drone-equipped hovertank of Carcharein class, Maia-equipped; lies somewhere near the Swamp and is completely corrupted. Iwona Perikas tried to reawaken it. | 0111-10-07 - 0111-10-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ariel Kubunczak      | 1 | ((220819-tank-as-a-love-letter)) |
| Arkadiusz Terienak   | 1 | ((220819-tank-as-a-love-letter)) |
| Daniel Terienak      | 1 | ((220819-tank-as-a-love-letter)) |
| Henryk Wkrąż         | 1 | ((220819-tank-as-a-love-letter)) |
| Iwona Perikas        | 1 | ((220819-tank-as-a-love-letter)) |
| Izabela Selentik     | 1 | ((220819-tank-as-a-love-letter)) |
| Michał Kabarniec     | 1 | ((220819-tank-as-a-love-letter)) |
| Mimoza Diakon        | 1 | ((220819-tank-as-a-love-letter)) |
| Talia Mikrit         | 1 | ((220819-tank-as-a-love-letter)) |