---
categories: profile
factions: 
owner: public
title: Mel Mirsiak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190720-klatwa-czarnej-piramidy      | szczur, zdobyła skaner i odkryła prawdę o Tatianie. Wraz z Samuelem porwali i uratowali biznesmena który stał się "pasikonikiem". | 0110-06-08 - 0110-06-10 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dawid Klamczran      | 1 | ((190720-klatwa-czarnej-piramidy)) |
| Jacek Cisrak         | 1 | ((190720-klatwa-czarnej-piramidy)) |
| Samuel Czałczak      | 1 | ((190720-klatwa-czarnej-piramidy)) |
| Tatiana Cisrak       | 1 | ((190720-klatwa-czarnej-piramidy)) |