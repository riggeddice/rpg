---
categories: profile
factions: 
owner: public
title: Maks Ardyceń
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230618-reality-show-z-zaskoczenia   | były strażnik parku w Drzewcu; ekspertyza w tropieniu, zna się na górach i survivalu; prawie spadł w przepaść wyciągnięty przez Olafa i Lily. Usunął pijawki z Olafa i Mai więcej niż raz. | 0083-10-02 - 0083-10-07 |
| 230625-przegrywy-ratuja-przegrywy   | z Alanem przeprowadził operację ratunkową; planuje jak przetrwać na pustyni. Pełni rolę medyka zespołowego. | 0083-10-08 - 0083-10-11 |
| 230626-pustynny-final-igrzysk       | z Alanem i Romanem schowali się by Olaf i reszta zostali pułapką - a oni młotem rażącym. | 0083-10-12 - 0083-10-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Klart           | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Lily Sanarton        | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Maja Wurmramin       | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Olaf Zuchwały        | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Roman Wyrkmycz       | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Antoni Kmandir       | 2 | ((230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Bogdan Ubuddan       | 2 | ((230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Jan Firatiel         | 1 | ((230626-pustynny-final-igrzysk)) |
| Rafał Armadion       | 1 | ((230626-pustynny-final-igrzysk)) |