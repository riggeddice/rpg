---
categories: profile
factions: 
owner: public
title: OO Wesoły Wieprzek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201014-krystaliczny-gniew-elizy     | okręt Orbitera, jak Galaktyczny Tucznik (tylko prostszy w pilotażu i mniejszy). Przekazany Ariannie Verlen do dyspozycji na czas świńskich problemów. | 0111-03-02 - 0111-03-05 |
| 201021-noktianie-rodu-arlacz        | pierwszy rajd bojowy w wykonaniu Zespołu - odbić 100 noktian z rodu Aurum: Arłacz. Gościł na pokładzie Anastazję Sowińską. | 0111-03-07 - 0111-03-10 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 201104-sabotaz-swini                | statek zanomalizował i został zniszczony przez Ataienne w Trzecim Raju. KIA. | 0111-03-16

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazja Sowińska   | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Arianna Verlen       | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Ataienne             | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Eliza Ira            | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Eustachy Korkoran    | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Klaudia Stryk        | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Aleksandra Termia    | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Dariusz Krantak      | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Diana Arłacz         | 1 | ((201021-noktianie-rodu-arlacz)) |
| Elena Verlen         | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Izabela Zarantel     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Juliusz Sowiński     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Marian Fartel        | 1 | ((201014-krystaliczny-gniew-elizy)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Wanessa Pyszcz       | 1 | ((201021-noktianie-rodu-arlacz)) |