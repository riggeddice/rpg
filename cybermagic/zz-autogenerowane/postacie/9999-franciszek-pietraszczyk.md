---
categories: profile
factions: 
owner: public
title: Franciszek Pietraszczyk
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230208-pierwsza-randka-eustachego   | starszy człowiek z protezami, uratowany z jakiejś ruiny arkologii przez Infernię. Jest wdzięczny Bartłomiejowi Korkoranowi, adoptował Marcina. Nie jest bogaty, ale z przyjemnością się dzieli czym ma z tymi co mają mniej. Ciężko pracuje wdzięczny za nową szansę. Poznał Ardillę z Inferni i dał jej ciepło w slumsach arkologii. | 0093-02-14 - 0093-02-21 |
| 230329-zdrada-rozrywajaca-arkologie | widział upadek Eweliny. Powinna była zginąć. To kwestia magii. Powiedział o tym Ardilli, która znalazła ślad maga Interis. | 0093-03-14 - 0093-03-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| Eustachy Korkoran    | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| Kalia Awiter         | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| OO Infernia          | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| Rafał Kidiron        | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| Ralf Tapszecz        | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Bartłomiej Korkoran  | 1 | ((230208-pierwsza-randka-eustachego)) |
| BIA Prometeus        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Celina Lertys        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Małgorzata Maratelus | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Marcin Pietraszczyk  | 1 | ((230208-pierwsza-randka-eustachego)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Wojciech Grzebawron  | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |