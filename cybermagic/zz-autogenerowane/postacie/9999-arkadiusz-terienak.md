---
categories: profile
factions: 
owner: public
title: Arkadiusz Terienak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220819-tank-as-a-love-letter        | MEMORY. In the past, he linked with contaminated Flara Astorii to stop monsters. In process they became a monster and Flara drowned in the Swamp in order to not be dangerous. | 0111-10-07 - 0111-10-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ariel Kubunczak      | 1 | ((220819-tank-as-a-love-letter)) |
| AU Flara Astorii     | 1 | ((220819-tank-as-a-love-letter)) |
| Daniel Terienak      | 1 | ((220819-tank-as-a-love-letter)) |
| Henryk Wkrąż         | 1 | ((220819-tank-as-a-love-letter)) |
| Iwona Perikas        | 1 | ((220819-tank-as-a-love-letter)) |
| Izabela Selentik     | 1 | ((220819-tank-as-a-love-letter)) |
| Michał Kabarniec     | 1 | ((220819-tank-as-a-love-letter)) |
| Mimoza Diakon        | 1 | ((220819-tank-as-a-love-letter)) |
| Talia Mikrit         | 1 | ((220819-tank-as-a-love-letter)) |