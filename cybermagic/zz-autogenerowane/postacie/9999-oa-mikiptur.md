---
categories: profile
factions: 
owner: public
title: OA Mikiptur
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240124-mikiptur-zemsta-woltaren     | zmieniona w jednostkę Aureliona zawierająca The Devastator, zniszczyła Isratazir i polowała też na Infernię. Próbuje zniszczyć reputację Bladawira i udaje powiązanie z Woltarenami. | 0110-12-29 - 0111-01-01 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Bladawir      | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Arianna Verlen       | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Eustachy Korkoran    | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Klaudia Stryk        | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Lars Kidironus       | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| OO Isratazir         | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Raoul Lavanis        | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Władawiec Diakon     | 1 | ((240124-mikiptur-zemsta-woltaren)) |