---
categories: profile
factions: 
owner: public
title: Aneta Woltaren
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231109-komodor-bladawir-i-korona-woltaren | świetny dowódca 'Korony', broni się przed grzywnami Orbitera i organizuje załogę dookoła siebie. Wyciągnęła ekipę z Anomalii Kolapsu chcąc dać im lepsze życie. Niestety, przez Bladawira była zmuszona do pójścia w stronę bardzo ryzykownego kursu na Iorus i tam dorwał ją Prefektiss. KIA. Reanimowana przez Ariannę, echem zgromadziła 'Koronę' dookoła siebie by walczyć z ixionem i kupić czas i nadzieję załodze. | 0110-10-27 - 0110-11-03 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Bladawir      | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Arianna Verlen       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Elena Verlen         | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Feliks Walrond       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Klaudia Stryk        | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Krzysztof Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Leszek Kurzmin       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Lidia Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Marta Keksik         | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| OO Paprykowiec       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Patryk Samszar       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| SC Korona Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Tomasz Rewernik      | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Wojciech Mykirło     | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |