---
categories: profile
factions: 
owner: public
title: Grażyna Burgacz
---

# {{ page.title }}


# Generated: 



## Fiszki


* logistyka i sprzęt (officer), tien (3 osoby pod nią) | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* ENCAO:  -0+-- |Powściągliwa i 'nudna';;Ascetyczna| VALS: Humility, Tradition| DRIVE: Starszy Brat | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* ENCAO:  -0+-- |Powściągliwa i 'nudna';;Ascetyczna| VALS: Humility, Tradition| DRIVE: Starszy Brat | Zainteresowana duchami XD | @ 230111-gdy-hr-reedukuje-niewlasciwa-osobe

### Wątki


kosmiczna-chwala-arianny
cena-nox-ignis
samotna-maja-w-strasznym-aurum

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly | kompetentna logistyk Królowej i tien; pomogła Ariannie rozlokować ludzi. Uważana za najlepszą oficer Królowej przez załogę. | 0100-05-06 - 0100-05-12 |
| 220928-kapitan-verlen-i-pojedynek-z-marine | wskazała Ariannie, że na Królowej załoga jest miękka i niewiele robi. Są konflikty w załodze. | 0100-05-14 - 0100-05-16 |
| 221102-astralna-flara-i-porwanie-na-karsztarinie | z Darią opracowały które blueprinty warto mieć na Astralnej Flarze i zapewniła, że Flara ma odpowiedni sprzęt i elementy na działania niedaleko Anomalii Kolapsu. | 0100-07-28 - 0100-08-03 |
| 221116-astralna-flara-dociera-do-nonariona-nadziei | jako jedyna doceniła klasę Nonariona jako sprawną konstrukcję. Potrzeba jej 11h na odzyskanie wody z czystej asteroidy. | 0100-09-08 - 0100-09-11 |
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | okazała się być absolutnym ekspertem od duchów. Przeprowadzi 'egzorcyzm' by uspokoić młodych infiltratorów. Pomogła też wprowadzić Nox Ignis w pętle mentalne, by szybciej wypalić pilotkę. | 0100-11-13 - 0100-11-16 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly | opinia na "Królowej" że podlizuje się Ariannie, ale jest bardzo kompetentna w logistyce. | 0100-05-12
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | jest wściekła na KAJETANA KIRCZNIKA. I nie będzie mówić o przeszłości i duchach. Na pewno nie. I filmik z jej egzorcyzmu trafi do bazy memów Astralnej Flary "Grażyna 'Paw' Burgacz". | 0100-11-16

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Daria Czarnewik      | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Maja Samszar         | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Władawiec Diakon     | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alezja Dumorin       | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Arnulf Perikas       | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Kajetan Kircznik     | 3 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leszek Kurzmin       | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Szymon Wanad         | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Elena Verlen         | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Erwin Pies           | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Klarysa Jirnik       | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Leona Astrienko      | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| OO Astralna Flara    | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Królowa Kosmicznej Chwały | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Szczepan Myrczek     | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Tomasz Ruppok        | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Ada Wyrocznik        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Gabriel Lodowiec     | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Gerwazy Kircznik     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Hubert Kerwelenios   | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Klaudiusz Terienak   | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leo Kasztop          | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Marcel Kulgard       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Mariusz Bulterier    | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Karsztarin        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Loricatus         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Salazar Bolza        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| SCA Hadiah Emas      | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Stefan Torkil        | 1 | ((220928-kapitan-verlen-i-pojedynek-z-marine)) |