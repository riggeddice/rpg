---
categories: profile
factions: 
owner: public
title: Miranda Termann
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230528-helmut-i-nieoczekiwana-awaria-lancera | (NIEOBECNA) komodor Orbitera; przerażająca ciotka Anastazego, która jest słodka dla sojuszników ale koszmarna jak się jest przeciwko niej czy Anastazemu. | 0109-09-23 - 0109-09-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazy Termann     | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Fabian Korneliusz    | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Helmut Szczypacz     | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Klaudia Stryk        | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Martyn Hiwasser      | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Nikodem Dewiremicz   | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| OO Serbinius         | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |