---
categories: profile
factions: 
owner: public
title: Marian Tosen
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210519-osiemnascie-oczu             | ekspert anomalii memetycznych na K1, pod adm. Termią. Jak tylko dowiedział się że Alaya padła, zarekwirował Tivr i wziął ze sobą załogę Inferni. Trochę test, trochę potrzebuje wsparcia. Nie powiedział im o co chodzi (bo nie mógł - jakby wiedzieli, anomalia by ich pochłonęła). | 0111-12-07 - 0111-12-18 |
| 210526-morderstwo-na-inferni        | opowiedział o przeszłości OO Tivr; akurat do tego miał dostęp dzięki uprawnieniom adm. Termii. | 0111-12-31 - 0112-01-06 |
| 210609-sekrety-kariatydy            | nie jest może ekspertem od Anomalii Kolapsu, ale słyszał o fiasku Kariatydy. Opowiedział o tym Ariannie. | 0112-01-10 - 0112-01-13 |
| 210616-nieudana-infiltracja-inferni | martwi się, że Infernia może zniknąć w Anomalii Kolapsu, więc przekazał jej trochę sprzętu i dobrych rad. Jest ekspertem od anomalii memetycznych (więc nie jego działka), ale poczciwy z niego mag. | 0112-01-27 - 0112-02-01 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 4 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni)) |
| Eustachy Korkoran    | 4 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni)) |
| Elena Verlen         | 3 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni)) |
| Klaudia Stryk        | 3 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni)) |
| Leona Astrienko      | 3 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni)) |
| Kamil Lyraczek       | 2 | ((210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni)) |
| Martyn Hiwasser      | 2 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni)) |
| OO Tivr              | 2 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni)) |
| Roland Sowiński      | 2 | ((210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni)) |
| Antoni Kramer        | 1 | ((210526-morderstwo-na-inferni)) |
| Feliks Walrond       | 1 | ((210526-morderstwo-na-inferni)) |
| Flawia Blakenbauer   | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Izabela Zarantel     | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Karol Reichard       | 1 | ((210526-morderstwo-na-inferni)) |
| OO Alaya             | 1 | ((210519-osiemnascie-oczu)) |
| OO Kariatyda         | 1 | ((210609-sekrety-kariatydy)) |
| OO Opresor           | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Otto Azgorn          | 1 | ((210526-morderstwo-na-inferni)) |
| Rafael Galwarn       | 1 | ((210609-sekrety-kariatydy)) |
| Tal Marczak          | 1 | ((210526-morderstwo-na-inferni)) |