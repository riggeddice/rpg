---
categories: profile
factions: 
owner: public
title: Tadeusz Maskiewnik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230808-nauczmy-mlodego-tiena-jak-zyc | 57-letni kapral i weteran; trafił za karę by chronić młodego Armina i swoją Celinę. Lekko paranoiczny, próbuje Armina wychowywać co nie wychodzi a Armin 'eggs him on'. Karolinus zaklęciem go z zaskoczenia rozwalił i Tadeusz wyszedł na niezgrabnego głupca. | 0095-09-14 - 0095-09-23 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230808-nauczmy-mlodego-tiena-jak-zyc | ranny przez działania Karolinusa (magia); uszkodzone nogi i walnął się w głowę. | 0095-09-23

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Armin Samszar        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Celina Maskiewnik    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Elena Samszar        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Florian Samszar      | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Karolinus Samszar    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Kleopatra Trusiek    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Rufus Bilgemener     | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Wiktor Blakenbauer   | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |