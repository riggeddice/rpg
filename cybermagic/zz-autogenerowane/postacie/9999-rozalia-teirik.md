---
categories: profile
factions: 
owner: public
title: Rozalia Teirik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210106-sos-z-haremu                 | buńczuczna ludzka breacherka z "Kardamona". Przyjaciółka Julii Aktenir, ściągającego na jej pomoc Eustachego. | 0111-07-26 - 0111-07-27 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210106-sos-z-haremu                 | beznadziejnie zakochana w Eustachym Korkoranie. Nie chciała tego, ale się stało... | 0111-07-27

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((210106-sos-z-haremu)) |
| Diana Arłacz         | 1 | ((210106-sos-z-haremu)) |
| Elena Verlen         | 1 | ((210106-sos-z-haremu)) |
| Eustachy Korkoran    | 1 | ((210106-sos-z-haremu)) |
| Horacy Aktenir       | 1 | ((210106-sos-z-haremu)) |
| Julia Aktenir        | 1 | ((210106-sos-z-haremu)) |
| Klaudia Stryk        | 1 | ((210106-sos-z-haremu)) |
| Leona Astrienko      | 1 | ((210106-sos-z-haremu)) |
| Martyn Hiwasser      | 1 | ((210106-sos-z-haremu)) |