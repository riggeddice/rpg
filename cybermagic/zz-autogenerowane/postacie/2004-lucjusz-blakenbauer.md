---
categories: profile
factions: 
owner: public
title: Lucjusz Blakenbauer
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "szczeliniec"
* owner: "public"
* title: "Lucjusz Blakenbauer"


## Kim jest

### Paradoksalny Koncept

Lekarz walczący o każde życie, stworzony w laboratoriach Blakenbauerów jako maszyna do zabijania. Lekarz nie dyskryminujący żadnego życia, zdolny do zabijania biologicznie lub swoim ciałem. Wspomagany biomagicznie, walczy przeciwko jakimkolwiek zbędnym wspomaganiom. Nie będąc człowiekiem, walczy o prawo ludzi do "czystości" struktury. Nie dyskryminuje władzy, mafii, "wrogów". Liczy się tylko misja - ratowanie życia.

### Motto

"Każde życie jest warte ratowania. Jeśli się nie zgadzasz - zejdź mi z drogi."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Wybitny lekarz i badacz biomantyczny. Specjalizuje się w Skażeniu i operowaniu energiami magicznymi.
* ATUT: Klątwa Blakenbauerów daje mu siłę i drugą formę - bojową. Gdy jest Bestią, jest niepowstrzymany.
* SŁABA: Nie kontroluje transformacji i drugiej formy. Jeśli będzie zmuszony, zmieni formę i zacznie eksterminację.
* SŁABA: Lucjusz jest absolutnie przewidywalny. Ma swoją misję i idzie do celu. Chroni życia i zdrowie - i tyle.

### O co walczy (3)

* ZA: Świat, w którym ludzie mogą być ludźmi, bez Skażania czy cyborgizacji.
* ZA: Miejsce bez "noktian" i "astorian", bez frakcji i wojen. To "my kontra świat".
* VS: Nie wolno poświęcać najsłabszych - będzie o nich walczył za cenę kariery i majątku.
* VS: Wzmocnienia organizmu - czy to bio/magi/techno - to błąd. Do niebezpiecznych rzeczy są TAI.

### Znaczące Czyny (3)

* Gdy w jego Rezydencji schroniła się ofiara pewnego gangu i ów gang zaatakował, Lucjusz ich pokonał - siłą fizyczną i bioformami. A potem mafia ich rozgromiła.
* Walczył o życie Ernesta Kajrata nawet, gdy nikt nie miał nadziei że to się uda. I wygrał - Kajrat przeżył.
* Opuścił stanowisko lekarza w Szpitalu Terminuskim by chronić Alicją przed przekształceniem jej w Strażniczkę Esuriit. Przegrał.

### Kluczowe Zasoby (3)

* COŚ: Supernowoczesna Rezydencja Blakenbauerów pod Pustogorem z zaawansowanym biolabem.
* KTOŚ: Dostęp do większości baz większości frakcji w Szczelińcu. Pomagał wszystkim.
* WIEM: Jak przekształcić większość owadów czy grzybów w straszliwą broń biologiczną.
* OPINIA: Apolityczny lekarz, który się nie poddaje i walczy o pacjenta / ofiarę Skażenia do samego końca.

## Inne

### Wygląd

Silny fizycznie i solidnie zbudowany wysoki mężczyzna; strasznie żylasty. Brązowe oczy, brak zarostu na twarzy, włosy bardzo krótko obcięte. Twarz pociągła, lekko zapadnięta. Ubrany zwykle w praktyczny, biały strój; często ma coś ochronnego.

### Coś Więcej

Karta postaci nie oddaje jak niesamowicie niebezpieczny jest w formie Bestii. Nie pokazuje też jednego - Lucjusz NIE jest pacyfistą. Unika krzywdzenia i szkodzenia, ale jeśli nie ma wyjścia... w końcu był zaprojektowany jako broń, prawda?


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210224-sentiobsesja                 | 17 lat, UB. Próbuje stabilizować Milenkę; taki trochę nerd. Przekonał Milenkę, że jednak najlepszą opcją jest oddanie oddziału rodowi Verlen. Z góry patrzy na standardowe bioformy, mogą być ulepszone. | 0092-10-01 - 0092-10-04 |
| 210302-umierajaca-farma-biovatow    | chciał pomóc ludziom z biovatów by nie zostali zniszczeni, więc zinfiltrował Verlen syfonując z odpoczywających żołnierzy _vitae_. Znaleziony i pokonany przez Viorikę. | 0093-08-17 - 0093-08-19 |
| 210306-wiktoriata                   | dotrzymuje słowa; pomaga Viorice uratować zaginionych. Rozbija (przypadkowo) siatkę szpiegowską Blakenbauerów na terenie Verlenów. Identyfikuje wiktoriatę, detoksyfikuje psychotycznych żołnierzy Verlenów i ratuje kogo się da. | 0093-12-19 - 0093-12-28 |
| 210324-lustrzane-odbicie-eleny      | chłopak Vioriki; był pod ręką gdy Lustrzany Golem zranił Michała Perikasa. Poza standardowym sarkazmem, udało mu się utrzymać Michała przy życiu i zmienić go w Ropucha. | 0097-09-02 - 0097-09-05 |
| 210813-szmuglowanie-antonelli       | już objął szpital terminuski w Pustogorze; przekonał Pustogor do tego by pomóc Antonelli (zasoby Sowińskich + kontakty z orbitą poza Orbiterem). | 0109-02-11 - 0109-02-23 |
| 180817-protomag-z-trzesawisk        | współpracujący z mafią Miedwieda lekarz wybitnej klasy. Przede wszystkim skupiony na potrzebach pacjentów, nie da nikomu zrobić krzywdy. | 0109-09-07 - 0109-09-09 |
| 181112-odklatwianie-ateny           | lekarz dbający twardo o swoich pacjentów. Starł się z Pięknotką; dopiero wpływy i złoto Adama Szarjana zmusiły go do oddania Ateny jako pacjentki. | 0109-10-23 - 0109-10-26 |
| 181230-uwiezienie-saitaera          | wpierw upewnił się, że Pięknotka nie jest pod wpływem żadnego bóstwa a potem zajął się Minerwą. Choć ona przekracza jego możliwości. | 0109-12-07 - 0109-12-09 |
| 190330-polowanie-na-ataienne        | zadbał o uciemiężonego agenta Orbiter Negatech - wybudził go i mu pomógł. | 0110-03-03 - 0110-03-04 |
| 190429-sabotaz-szeptow-elizy        | overridowany z decyzją o utrzymanie Minerwy w szpitalu; opieprzył Karlę jak nikt za to, że wróciła bardziej ranna | 0110-04-10 - 0110-04-13 |
| 190505-szczur-ktory-chroni          | skażenie kralotyczne było tak silne, że aż musiał przyjść na Nieużytki Staszka robić ratunkowe sytuacje. | 0110-04-22 - 0110-04-24 |
| 190901-polowanie-na-pieknotke       | doprowadził Pięknotkę do porządku i pozwolił Minerwie czarować w szpitalu. Bardzo potem tego żałował (odkażanie sali). | 0110-06-21 - 0110-06-24 |
| 200311-wygrany-kontrakt             | próbuje utrzymać przy życiu Ernesta Kajrata i opracowuje z magami z "Zająca" jak wyciągnąć zixionizowaną Amandę Kajrat z rąk Grzymościa jedzeniem | 0110-07-20 - 0110-07-23 |
| 200222-rozbrojenie-bomby-w-kalbarku | znalazł chorobę Chevaleresse; nie lubi kralotycznych środków jak to co jej podali. Pomoże Pięknotce w ratowaniu terminusów z Kalbarka. | 0110-07-27 - 0110-08-01 |
| 200226-strazniczka-przez-lzy        | stanął przeciwko TAI Crystal, Alinie i Pustogorowi w ochronie Alicji przed losem Strażniczki Esuriit. I przegrał. | 0110-07-29 - 0110-08-01 |
| 201025-kraloth-w-parku-janor        | naprawił Pięknotkę, używając matrycy Blakenbauer do kontroli sił Saitaera. Zaprosił Pięknotkę do Rezydencji; tu będzie bezpieczna. | 0110-10-29 - 0110-10-31 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210306-wiktoriata                   | przypadkowo wykrył tajną operację sił specjalnych Blakenbauerów w Trójkącie Chaosu i ją SPALIŁ. Współpracując z Vioriką VERLEN. Powiedział jej o wiłach. Szefostwo jest z niego MEGA niezadowolone. Podpadł. Bardzo. | 0093-12-28
| 210306-wiktoriata                   | Viorika i ogólnie Verlenowie zauważyli, że jeśli może to pomoże - zwłaszcza ludziom i słabszym od siebie. To jego zdecydowana słabość. Tajnym agentem nie będzie, ale dzięki temu można mu zaufać bardziej niż przeciętnemu Blakenbauerowi. | 0093-12-28
| 210306-wiktoriata                   | jakieś pół roku po tym wydarzeniu został parą z Vioriką Verlen. | 0093-12-28
| 210813-szmuglowanie-antonelli       | ma dostęp do krwi Antonelli Temaris, z czego wynika - Wzór pasujący do Nataniela Morlana. | 0109-02-23
| 200226-strazniczka-przez-lzy        | traci wiarę w to, że Pustogor chce dobrze. Wpada w kłopoty przez wejście w kolizję z agentami specjalnymi Pustogoru. Traci też stanowisko elitarnego lekarza. | 0110-08-01

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 9 | ((180817-protomag-z-trzesawisk; 181112-odklatwianie-ateny; 181230-uwiezienie-saitaera; 190330-polowanie-na-ataienne; 190429-sabotaz-szeptow-elizy; 190505-szczur-ktory-chroni; 190901-polowanie-na-pieknotke; 200222-rozbrojenie-bomby-w-kalbarku; 201025-kraloth-w-parku-janor)) |
| Alan Bartozol        | 5 | ((180817-protomag-z-trzesawisk; 190330-polowanie-na-ataienne; 190429-sabotaz-szeptow-elizy; 190505-szczur-ktory-chroni; 190901-polowanie-na-pieknotke)) |
| Minerwa Metalia      | 5 | ((181230-uwiezienie-saitaera; 190429-sabotaz-szeptow-elizy; 190901-polowanie-na-pieknotke; 200311-wygrany-kontrakt; 201025-kraloth-w-parku-janor)) |
| Diana Tevalier       | 4 | ((190330-polowanie-na-ataienne; 190429-sabotaz-szeptow-elizy; 190901-polowanie-na-pieknotke; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Viorika Verlen       | 4 | ((210224-sentiobsesja; 210302-umierajaca-farma-biovatow; 210306-wiktoriata; 210324-lustrzane-odbicie-eleny)) |
| Atena Sowińska       | 3 | ((180817-protomag-z-trzesawisk; 181112-odklatwianie-ateny; 190330-polowanie-na-ataienne)) |
| Przemysław Czapurt   | 3 | ((210224-sentiobsesja; 210306-wiktoriata; 210324-lustrzane-odbicie-eleny)) |
| Apollo Verlen        | 2 | ((210224-sentiobsesja; 210306-wiktoriata)) |
| Arianna Verlen       | 2 | ((210224-sentiobsesja; 210324-lustrzane-odbicie-eleny)) |
| Ataienne             | 2 | ((190330-polowanie-na-ataienne; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Ernest Kajrat        | 2 | ((190505-szczur-ktory-chroni; 200311-wygrany-kontrakt)) |
| Erwin Galilien       | 2 | ((180817-protomag-z-trzesawisk; 190429-sabotaz-szeptow-elizy)) |
| Karla Mrozik         | 2 | ((181230-uwiezienie-saitaera; 190429-sabotaz-szeptow-elizy)) |
| Adela Kirys          | 1 | ((190505-szczur-ktory-chroni)) |
| Aleksander Rugczuk   | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Aleksandra Szklarska | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Alicja Kiermacz      | 1 | ((200226-strazniczka-przez-lzy)) |
| Alina Anakonda       | 1 | ((200226-strazniczka-przez-lzy)) |
| Amadeusz Sowiński    | 1 | ((181112-odklatwianie-ateny)) |
| Amanda Kajrat        | 1 | ((200311-wygrany-kontrakt)) |
| Antonella Temaris    | 1 | ((210813-szmuglowanie-antonelli)) |
| Bartłomiej Małczarek | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Bruno Baran          | 1 | ((210813-szmuglowanie-antonelli)) |
| Cezary Zwierz        | 1 | ((181112-odklatwianie-ateny)) |
| Cień Brighton        | 1 | ((210813-szmuglowanie-antonelli)) |
| Crystal d'Corieris   | 1 | ((200226-strazniczka-przez-lzy)) |
| Dagmara Doberman     | 1 | ((200311-wygrany-kontrakt)) |
| Darek Ampieczak      | 1 | ((200226-strazniczka-przez-lzy)) |
| Elena Verlen         | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Eliza Farnorz        | 1 | ((200226-strazniczka-przez-lzy)) |
| Eliza Ira            | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Felicja Melitniek    | 1 | ((180817-protomag-z-trzesawisk)) |
| Flawia Blakenbauer   | 1 | ((210813-szmuglowanie-antonelli)) |
| Frezja Amanit        | 1 | ((210302-umierajaca-farma-biovatow)) |
| Hestia d'Janor       | 1 | ((201025-kraloth-w-parku-janor)) |
| Jolanta Sowińska     | 1 | ((210813-szmuglowanie-antonelli)) |
| Józef Małmałaz       | 1 | ((190901-polowanie-na-pieknotke)) |
| Kasjan Czerwoczłek   | 1 | ((190505-szczur-ktory-chroni)) |
| Kastor Miczokan      | 1 | ((200226-strazniczka-przez-lzy)) |
| Kreacjusz Diakon     | 1 | ((181230-uwiezienie-saitaera)) |
| Krystian Namałłek    | 1 | ((190505-szczur-ktory-chroni)) |
| Liliana Bankierz     | 1 | ((200311-wygrany-kontrakt)) |
| Maciej Oczorniak     | 1 | ((201025-kraloth-w-parku-janor)) |
| Mariusz Trzewń       | 1 | ((190901-polowanie-na-pieknotke)) |
| Mateusz Kardamacz    | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Michał Perikas       | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Miedwied Zajcew      | 1 | ((180817-protomag-z-trzesawisk)) |
| Milena Blakenbauer   | 1 | ((210224-sentiobsesja)) |
| Minerwa Diakon       | 1 | ((181112-odklatwianie-ateny)) |
| Mścigrom Verlen      | 1 | ((210224-sentiobsesja)) |
| Olaf Zuchwały        | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Oliwia Namałłek      | 1 | ((190505-szczur-ktory-chroni)) |
| Rafał Muczor         | 1 | ((200226-strazniczka-przez-lzy)) |
| Romeo Verlen         | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Saitaer              | 1 | ((181112-odklatwianie-ateny)) |
| SC Fecundatis        | 1 | ((210813-szmuglowanie-antonelli)) |
| Selena Walecznik     | 1 | ((210302-umierajaca-farma-biovatow)) |
| Tomasz Sowiński      | 1 | ((210813-szmuglowanie-antonelli)) |
| Tymon Grubosz        | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Wiktor Blakenbauer   | 1 | ((210306-wiktoriata)) |
| Wiktor Satarail      | 1 | ((190505-szczur-ktory-chroni)) |
| Ziemowit Zięba       | 1 | ((200311-wygrany-kontrakt)) |