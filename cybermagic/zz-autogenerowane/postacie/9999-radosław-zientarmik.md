---
categories: profile
factions: 
owner: public
title: Radosław Zientarmik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240306-potwornosc-powojenna-trzykwiatu | zarządzał zbrojownią i dostarczał sprzęt na tajne misje; pokazał Natalii, że mnóstwo wartościowych rzeczy została eksportowana i są tajne transporty na fortifarmy. Nadal lojalny Latiszy, ale serce go boli i nie wie co myśleć. | 0084-04-16 - 0084-04-21 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Jakub Kurbeczko      | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Ksenia Byczajnik     | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Latisza Warkolnicz   | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Natalia Pszaruk      | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Natan Wierzbitowiec  | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Petra Łomniczajew    | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Ulrich Warkolnicz    | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Zofia Skorupniczek   | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Zuzanna Szagbin      | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |