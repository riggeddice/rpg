---
categories: profile
factions: 
owner: public
title: Maja Kormoran
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220420-samobojstwo-kapitana-wielkiego-weza | aspirująca kapitan Wielkiego Węża; ambitna acz nielubiana. Przejmuje dowodzenie w sprawach kryzysowych, zwłaszcza, gdy Berdysz za nią staje. Zaplanowała jak się pozbyć kill agenta i zrealizowała plan poprawnie. | 0108-09-26 - 0108-10-02 |
| 220427-dziwne-strachy-w-morzu-ulud  | udało jej się przejąć nieformalnie władzę dzięki pomocy Berdysza i dobrego wykorzystania kryzysu. Skutecznie wykorzystała kryzys do przejęcia władzy. Opanowała załogę - nie zdradzamy że wiemy kim jest Berdysz i bezpieczniej jest współpracować. I tak, ratujemy piratów z Okrutnej Wrony. | 0108-10-05 - 0108-10-07 |
| 220518-okrutna-wrona-kalcynici-i-koszmary | z Kornelią robi forensic martwych niewolnic i odkryła, że jeden z kalcynitów jest halucynacją. Potem obezwładniła Kornelię i wsadziła jej neuroobrożę. Doszła do tego co jest konieczne do opętania. | 0108-10-09 - 0108-10-18 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220427-dziwne-strachy-w-morzu-ulud  | nie ma opcji, by mieć serca załogi Wielkiego Węża. Jest najlepszym pierwszym oficerem której można ufać w sprawach kryzysowych, ale nie będzie nigdy lubiana. | 0108-10-07
| 220518-okrutna-wrona-kalcynici-i-koszmary | zostaje kapitanem Węża i tej załogi. | 0108-10-18

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Falkam          | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Antoni Krutacz       | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Berdysz Rozdzieracz  | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Filip Gościc         | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Kornelia Sanoros     | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Lila Cziras          | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Pola Mornak          | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Amanda Korel         | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Elwira Piscernik     | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Jerzy Odmiczak       | 1 | ((220420-samobojstwo-kapitana-wielkiego-weza)) |
| Ksawery Janowar      | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Mikołaj Faczon       | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |