---
categories: profile
factions: 
owner: public
title: Bartek Wudrak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220525-dzieci-z-arinkarii-odpychaja-piratow | 15, zmodyfikowany drakolicko; właściciel "świątyni wrestlera Tytana", ma kolekcję noży. Wyciągnął dziewczyny z dziury, zaprowadził do swojej kryjówki i dał broń. Potem - wepchnął pirata do maszyny przetwarzającej i nerwowo nie wytrzymał (pierwszy raz kogoś zabił); oddelegowany do ochrony dzieci. | 0105-05-14 - 0105-05-15 |
| 220329-mlodociani-i-pirat-na-krolowej | chroni "swoje" dziewczyny nożem, postawił się nawet Berdyszowi. Mało mówi, opiekuńczy. Zainteresował się maszynami pod wpływem Mirandy. | 0108-08-29 - 0108-09-09 |
| 220503-antos-szafa-i-statek-piscernikow | starł się z Anną i został brutalnie pokonany. Musi wziąć się w garść. Nie zrobi nic Wojtkowi, acz chce sprać Antosa za Karę. | 0108-09-23 - 0108-09-25 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220329-mlodociani-i-pirat-na-krolowej | przekonany, że Anna ma rację - dobro i pomaganie mają znaczenie. Przekonany, że Berdysz Rozdzieracz też jest dobrą osobą. Uczestniczył w negocjacjach z nim. | 0108-09-09
| 220503-antos-szafa-i-statek-piscernikow | chce zrobić wpierdol Antosowi Kuraminowi za zamknięcie Kary w szafie. | 0108-09-25
| 220503-antos-szafa-i-statek-piscernikow | musi wziąć się w garść. On jako jedyny - poza Karą i Romaną - nie ma przyszłości czy czegoś co go przesunie do bycia godnym w przyszłości. Zrozumiał, że nie może już brać odpowiedzialności za los Romki i Kary - są dorosłe. | 0108-09-25

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anna Szrakt          | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220503-antos-szafa-i-statek-piscernikow)) |
| Kara Prazdnik        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220503-antos-szafa-i-statek-piscernikow)) |
| Miranda Ceres        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220503-antos-szafa-i-statek-piscernikow)) |
| Romana Kundel        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Adam Wudrak          | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Albert Rybowąż       | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Antos Kuramin        | 1 | ((220503-antos-szafa-i-statek-piscernikow)) |
| Berdysz Rozdzieracz  | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Bruno Wesper         | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Gotard Kicjusz       | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Helena Banbadan      | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Kaja Czmuch          | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Koralina Szprot      | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Łucjan Torwold       | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Prokop Umarkon       | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| SC Królowa Przygód   | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Seweryn Grzęźlik     | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Wojciech Kaznodzieja | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |