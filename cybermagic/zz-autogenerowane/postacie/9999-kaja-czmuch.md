---
categories: profile
factions: 
owner: public
title: Kaja Czmuch
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220525-dzieci-z-arinkarii-odpychaja-piratow | 15, zmodyfikowana drakolicko; "odpowiedzialna" dziewczyna Arinkarii opiekująca się innymi. Zmodowała komunikatory i dowodziła dzieciakami atakując piratów. Ściągnęła piratów w pułapkę elektroniką, dając sygnały gdzie są. | 0105-05-14 - 0105-05-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Wudrak          | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Albert Rybowąż       | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Bartek Wudrak        | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Bruno Wesper         | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Koralina Szprot      | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Romana Kundel        | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |