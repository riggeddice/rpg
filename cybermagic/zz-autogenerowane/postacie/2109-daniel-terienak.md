---
categories: profile
factions: 
owner: public
title: Daniel Terienak
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "pustogor, rekin, terienak, aurum"
* owner: "public"
* title: "Daniel Terienak"


## Kim jest

### W kilku zdaniach

Brat Karoliny. 

### Co się rzuca w oczy

* ?

### Jak sterować postacią

* TODO

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* TODO

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: ?
* AKCJA: ?
* SERCE: 
* COŚ: 

### Serce i Wartości (3)

* Tradycja
    * Rozpaczliwie próbuje znaleźć środki na odbudowę umierającego rodu
    * Kodeks rycerski - mogę oćwiczyć chłopa, ale dziewczyny respektowane będą.
    * W istotnych sprawach zawsze po właściwej stronie - jest tien Aurum.
* Twarz 
    * Tylko siostrze pozwala na brak szacunku. Walczy o reputacje pełną siłą.
    * Lepiej, by się mnie bali niż kochali. WYJĄTEK: dziewczyny. Im wolno więcej. 
    * Bardzo dba o reputację doskonałego zwadźcy i świetnego kuriera. Nie odmawia pojedynków, prowokuje do nich.
* Potęga
    * Zdobyć wpływy, środki itp - musi uratować rod i chronić jego członków.
    * Zero sympatii i empatii wobec ludzi i viciniusów. Gdzie Karo widzi potencjał on widzi zasoby.
    * Buduje sojusze na lewo i prawo. Nieważne z kim, ważne jak mocne.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Moja sentisieć umiera. Jestem lordem - powinienem coś z tym zrobić. Wszyscy na mnie patrzą a ja jestem bezradny."
* CORE LIE: "Niezależnie od czegokolwiek to na mnie i tylko na mnie leży uratowanie sentisieci. Bez sentisieci jestem nikim i mój Ród umrze."
* ?

### Magia (3M)

#### W czym jest świetna

* ?

#### Jak się objawia utrata kontroli

* ?

### Specjalne

* .

## Inne

### Wygląd

* 

### Coś Więcej

* On ma affinity do sentisieci rodu Terienak. On by był następnym patriarchą. Dlatego tak ciężko przeżywa jej umieranie - on to czuje.

### Endgame

* .


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201201-impreza-w-malopsie           | Rekin. Dziąsłowiec i biedaartefaktor, który nie lubi jak ktoś lekceważy kobiety. Idzie prosto do celu pięścią i agresją. Stworzył "z demona" artefakt, który oddał Grzymościowcom za to, że oni posprzątają po Rekinach. | 0110-11-04 - 0110-11-06 |
| 201215-dziewczyna-i-pies            | nie waha się użyć tortur by osiągnąć cel; wyrwał Paulinę i Andrzeja z mgły mentalnej wygenerowanej przez Paulinę. Zero sympatii i współczucia wobec ludzi. | 0110-11-15 - 0110-11-17 |
| 220101-karolina-w-ciezkim-mlocie    | chciał wygrać zakład z Mysiokornikiem, więc Karo próbowała wbić się do knajpy. Gdy nie miała akceptacji, sprowokował anomaliczny problem, dzięki czemu Karo pomogła i została zaakceptowana. Po wściekłej Karolinie zrezygnował z zakładu i ma ogólnie ponury nastrój. | 0111-04-25 - 0111-05-05 |
| 210720-porwanie-daniela-terienaka   | chciał sobie dorobić u Kacpra Bankierza i poszedł na współpracę z mafią. Ula Miłkowicz go wymanewrowała, porwała i zaczęła skan. Uratowany przez siostrę (Karolinę). | 0111-06-14 - 0111-06-16 |
| 210824-mandragora-nienawidzi-rekinow | wpadł na mandragorę która nienawidzi Rekinów. Udało mu się przesunąć swoje Skażenie z "rekiny" na "mafia", by chronić siostrę. | 0111-07-03 - 0111-07-05 |
| 210831-serafina-staje-za-wydrami    | ma podejrzanie dużo "przyjaciółek" w okolicy. Kiepska nawijka. Prowadzi prywatnego wideobloga. Dzięki temu udało się namierzyć Serafinę Irę - po jej muzyce. Acz siorka płakała jak bloga oglądała... | 0111-07-07 - 0111-07-10 |
| 211123-odbudowa-wedlug-justyniana   | cieszy się, bo Justynian zmontował Arenę Amelii i będą walki ludzi (gladiatorów). Z przyjemnością mówi siostrze co się dzieje i pomaga w odbudowie Justynianowi. Nie chroni przed Karoliną Loreny - jeśli postawiła się jego siostrze to słusznie dostała. | 0111-08-11 - 0111-08-20 |
| 211127-waśń-o-ryby-w-majklapcu      | chce pomóc domowej sentisieci, więc zainteresował się pomocą morderców ryb w Majkłapcu (by się zbliżyć do czarodziejki z AMZ). Złożył czar z Pawłem Szprotką - artefakt szukający kotów. Doszedł do tego, że Iwan Zawtrak nie mówi całej prawdy; wszedł na twardo i jakkolwiek dostał cios, to prawda wyszła na jaw. Włamał się skutecznie do mafii i podłożył im narkotyki. | 0111-08-19 - 0111-08-24 |
| 211207-gdy-zabraknie-pradu-rekinom  | miał się bić z Bulterierem na arenie, ale padł prąd. Potem pomagał Karo ciągnąć kable i jak Karo była narażona na walkę z Rekinami, powiedział, że siostra to świętość. Walka się nie odbyła. | 0111-08-30 - 0111-08-31 |
| 211228-akt-o-ktorym-marysia-nie-wie | wezwany awaryjnie przez siostrę do wyśledzenia Torszeckiego i jego kryjówek by zdobyć akt Marysi Sowińskiej zanim on go zniszczy. Znalazł Torszeckiego, ale by go zatrzymać musiał go ciężko zbić. | 0111-09-09 - 0111-09-12 |
| 220730-supersupertajny-plan-loreny  | pomógł siostrze zastawić pułapkę na osoby atakujące mały biznes podwiercki (bo siostra); pokonał kilku ludzi w walce wręcz, demoralizując ich. | 0111-09-30 - 0111-10-04 |
| 220819-tank-as-a-love-letter        | Rekin; Shark; Henryk summoned him to confront him about Iwona Perikas. He was unaware of AU Flara Astorii being found and stopped them from doing stupid stuff and unearthing the Esuriit anomaly. He beat Henryk up and broke all contact with Iwona. | 0111-10-07 - 0111-10-08 |
| 230325-ten-nawiedzany-i-ta-ukryta   | Rekin; master tracker, dobry w pułapkach i mistrz walki wręcz prowadzący videobloga; tu przydała się jego kataliza detekcji. Wygrał wyścig  O dziwo, został dyplomatą mediując między Leą i Karo jak te się gryzły o protokół XD. Źródło Plagi Szczurów w Podwiercie, za które obwinione będzie AMZ. | 0111-10-16 - 0111-10-17 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210824-mandragora-nienawidzi-rekinow | nieaktywny przez następne 3 dni (regeneracja) | 0111-07-05
| 211127-waśń-o-ryby-w-majklapcu      | w Majkłapcu jest lubiany przez Iwana Zawtraka z Kociarni i ma jego zaufanie a Genowefa z Farmy Krecik nim gardzi. | 0111-08-24
| 220819-tank-as-a-love-letter        | broke all contact with Iwona Perikas; she might love him, but she is despicable - hurting Henryk Wkrąż and reanimating Flara Astorii... | 0111-10-08

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolina Terienak    | 12 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies; 210720-porwanie-daniela-terienaka; 210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami; 211123-odbudowa-wedlug-justyniana; 211127-waśń-o-ryby-w-majklapcu; 211207-gdy-zabraknie-pradu-rekinom; 211228-akt-o-ktorym-marysia-nie-wie; 220101-karolina-w-ciezkim-mlocie; 220730-supersupertajny-plan-loreny; 230325-ten-nawiedzany-i-ta-ukryta)) |
| Marysia Sowińska     | 6 | ((210720-porwanie-daniela-terienaka; 210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami; 211123-odbudowa-wedlug-justyniana; 211207-gdy-zabraknie-pradu-rekinom; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Rafał Torszecki      | 5 | ((210720-porwanie-daniela-terienaka; 210831-serafina-staje-za-wydrami; 211123-odbudowa-wedlug-justyniana; 211127-waśń-o-ryby-w-majklapcu; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Arkadia Verlen       | 3 | ((211127-waśń-o-ryby-w-majklapcu; 211207-gdy-zabraknie-pradu-rekinom; 220730-supersupertajny-plan-loreny)) |
| Lorena Gwozdnik      | 3 | ((210831-serafina-staje-za-wydrami; 211207-gdy-zabraknie-pradu-rekinom; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Andrzej Kuncerzyk    | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Ernest Namertel      | 2 | ((211123-odbudowa-wedlug-justyniana; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Franciszek Zygmunt   | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Henryk Wkrąż         | 2 | ((211207-gdy-zabraknie-pradu-rekinom; 220819-tank-as-a-love-letter)) |
| Izydor Grumczewicz   | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Laura Tesinik        | 2 | ((210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami)) |
| Liliana Bankierz     | 2 | ((211228-akt-o-ktorym-marysia-nie-wie; 220730-supersupertajny-plan-loreny)) |
| Paulina Mordoch      | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Sensacjusz Diakon    | 2 | ((210824-mandragora-nienawidzi-rekinow; 211207-gdy-zabraknie-pradu-rekinom)) |
| Tadeusz Łaśnic       | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Tomasz Tukan         | 2 | ((210720-porwanie-daniela-terienaka; 210824-mandragora-nienawidzi-rekinow)) |
| Władysław Owczarek   | 2 | ((220101-karolina-w-ciezkim-mlocie; 220730-supersupertajny-plan-loreny)) |
| Aleksandra Burgacz   | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Amelia Sowińska      | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Ariel Kubunczak      | 1 | ((220819-tank-as-a-love-letter)) |
| Arkadiusz Terienak   | 1 | ((220819-tank-as-a-love-letter)) |
| Arnold Kłaczek       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| AU Flara Astorii     | 1 | ((220819-tank-as-a-love-letter)) |
| Azalia Sernat d'Namertel | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Barnaba Burgacz      | 1 | ((201201-impreza-w-malopsie)) |
| Cezary Urmaszcz      | 1 | ((201201-impreza-w-malopsie)) |
| Ekaterina Zajcew     | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Franek Bulterier     | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Genowefa Krecik      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Grzegorz Terienak    | 1 | ((201215-dziewczyna-i-pies)) |
| Halina Sermniek      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Henryk Murkot        | 1 | ((220101-karolina-w-ciezkim-mlocie)) |
| Hestia d'Rekiny      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Ignacy Myrczek       | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Iwan Zawtrak         | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Iwona Perikas        | 1 | ((220819-tank-as-a-love-letter)) |
| Izabela Selentik     | 1 | ((220819-tank-as-a-love-letter)) |
| Justynian Diakon     | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Karol Pustak         | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Keira Amarco d'Namertel | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Ksenia Kirallen      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Lea Samszar          | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Mariusz Trzewń       | 1 | ((210720-porwanie-daniela-terienaka)) |
| Marsen Gwozdnik      | 1 | ((220730-supersupertajny-plan-loreny)) |
| Michał Kabarniec     | 1 | ((220819-tank-as-a-love-letter)) |
| Michał Klabacz       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Mimoza Diakon        | 1 | ((220819-tank-as-a-love-letter)) |
| Mimoza Elegancja Diakon | 1 | ((220101-karolina-w-ciezkim-mlocie)) |
| Nadia Uprewien       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Napoleon Bankierz    | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Natalia Tessalon     | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Oliwier Czepek       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Patrycja Radniak     | 1 | ((201215-dziewczyna-i-pies)) |
| Paweł Szprotka       | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Rupert Mysiokornik   | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Serafina Ira         | 1 | ((210831-serafina-staje-za-wydrami)) |
| Stella Amakirin      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Talia Mikrit         | 1 | ((220819-tank-as-a-love-letter)) |
| Urszula Arienik      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Urszula Miłkowicz    | 1 | ((210720-porwanie-daniela-terienaka)) |
| Żorż d'Namertel      | 1 | ((220730-supersupertajny-plan-loreny)) |