---
categories: profile
factions: 
owner: public
title: OO Minerwa
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200729-nocna-krypta-i-emulatorka    | Statek dowódczy Kirasjerów, nazwany tak po porażce Damiana odnośnie Minerwy Metalii. Supernowoczesny pojazd NeoMil, sterowany przez Zodiac (czarodziejkę, nie TAI). Ma 'czarną Aleksandrię' - pain module. | 0111-01-14 - 0111-01-20 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Nocna Krypta      | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Antoni Kramer        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Arianna Verlen       | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Damian Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Eustachy Korkoran    | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Klaudia Stryk        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Leona Astrienko      | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Mirela Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Castigator        | 1 | ((200729-nocna-krypta-i-emulatorka)) |