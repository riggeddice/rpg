---
categories: profile
factions: 
owner: public
title: Wojmił Siwywilk
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190422-pustogorski-konflikt         | Rycerz Potęgi i Firebrand; chciał podpalić Miasteczko przeciw Barbakanowi, ale Pięknotka rozmontowała jego argumenty. Najpewniej odejdzie do Wolnych Ptaków. | 0110-04-02 - 0110-04-03 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Erwin Galilien       | 1 | ((190422-pustogorski-konflikt)) |
| Karla Mrozik         | 1 | ((190422-pustogorski-konflikt)) |
| Marcel Nieciesz      | 1 | ((190422-pustogorski-konflikt)) |
| Olaf Zuchwały        | 1 | ((190422-pustogorski-konflikt)) |
| Pięknotka Diakon     | 1 | ((190422-pustogorski-konflikt)) |