---
categories: profile
factions: 
owner: public
title: Anna Ivanova
---

# {{ page.title }}


# Generated: 



## Fiszki


* Główny Inżynier, Inżynier | @ 240117-echo-z-odmetow-przeszlosci
* Stara się, by wszystko działało. Chciałaby odzyskać zalane pomieszczenia, oczyścić life support [dziwnie pachnie], generalnie uważa, że dzięki niej to wszystko jakoś działa. Ma podejście - jak działa nie dotykaj, bo zepsujesz. Stosunkowo wyluzowana, lubi jak jest zrobione po kosztach i lubi komfort. Scrapperka i mistrzyni improwizacji. Odkąd elektryk wyszedł i nie wrócił, nie odważyła się wyjść. | @ 240117-echo-z-odmetow-przeszlosci
* “Nie dotykaj, bo zepsujesz”, ale tęskni za jakimś wyzwaniem. Chciałaby zrobić coś więcej niż w kółko naprawiać działające systemy. | @ 240117-echo-z-odmetow-przeszlosci

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240117-echo-z-odmetow-przeszlosci   | pomaga w naprawie przekaźników komunikacji, po etapie załamania z powodu swojej śmierci oraz śmierci bliskich decyduje się współpracować z Kelpie i poczekać na “lekarstwo” na jej stan; chce przed śmiercią ujrzeć słońce. | 0096-01-24 - 0096-01-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Elżbieta Sanchez     | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Gerald Barowiecki    | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Jędrzej Sanchez      | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Klasa Dyplomata      | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Klasa Inżynier       | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Klasa Oficer Ochrony | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Robert Tisso         | 1 | ((240117-echo-z-odmetow-przeszlosci)) |