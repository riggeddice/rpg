---
categories: profile
factions: 
owner: public
title: Tomasz Sowiński
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210810-porwanie-na-gwiezdnym-motylu | żądny przygód młody mag o dobrym sercu; poszedł z niewłaściwą dziewczyną na stronę i został porwany przez miragenta. Pomógł Flawii i Duchowi (Antonelli) wyjść z trudnych sytuacji życiowych. | 0108-12-25 - 0108-12-30 |
| 210813-szmuglowanie-antonelli       | twórca najgorszych planów ratowania Antonelli w historii, ale jego gorące serce zapaliło Jolantę do pomocy i przekonał do tego też Cienia. Dostarcza surowców Lucjuszowi Blakenbauerowi by zmienić Wzór Antonelli. | 0109-02-11 - 0109-02-23 |
| 210218-infernia-jako-goldarion      | udaje twardego i męskiego, ale to przerażony duży dzieciak skonfrontowany z wszechświatem. Zakochał się w Ariannie ("Monice"), bo jest pierwszą kobietą w jego życiu XD. | 0111-09-16 - 0111-10-01 |
| 210317-arianna-podbija-asimear      | zorientował się, że z Jolantą jest coś bardzo poważnie nie tak - i przekazał tą wiadomość dyskretnie Ariannie (prowadząc do porwania Joli). | 0111-10-18 - 0111-11-02 |
| 210414-dekralotyzacja-asimear       | przerażony Arianną jak cholera, współpracuje z nią by nikt nie ucierpiał. Przekonał Mariusza Tubalona, by ten dał mu uprawnienia takie jak Jolancie. | 0111-11-03 - 0111-11-12 |
| 210421-znudzona-zaloga-inferni      | chciał chronić kuzynkę przed Martynem (by ten jej nie uwiódł?). Uniemożliwił Martynowi uratowanie Jolanty, skończył przez Martyna unieszkodliwiony. | 0111-11-16 - 0111-11-19 |
| 210818-siostrzenica-morlana         | chce uratować Ofelię i zwraca się z prośbą do Arianny Verlen. Podkochuje się w niej. Ma najgorsze poczucie spec ops w historii - zakłada dziwny strój i zwraca na siebie uwagę. | 0112-01-20 - 0112-01-24 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210810-porwanie-na-gwiezdnym-motylu | zaprzyjaźnił się z Flawią Blakenbauer i Antonellą Tamaris. | 0108-12-30
| 210218-infernia-jako-goldarion      | odblokował przesyłkę dla Jolanty Sowińskiej. Będzie wiadomo, że to on. Solidny wpiernicz od Jolanty się będzie należał? | 0111-10-01
| 210218-infernia-jako-goldarion      | zakochał się w Ariannie jako "Monice", która uratowała go przed Leoną, dbała o niego i w ogóle była miła, inteligentna i kompetentna. I nie śmiała się z niego. Chce ją wyciągnąć. | 0111-10-01
| 210414-dekralotyzacja-asimear       | Opinia "Krwawy Lord Sowiński" na Asimear po akcji Eustachego i Klaudii z twardym, krwawym odpieraniem kralotha. | 0111-11-12
| 210818-siostrzenica-morlana         | podkochuje się w Ariannie Verlen. Uważa ją za mądrą i piękną. Nigdy jej tego nie powie. | 0112-01-24
| 210818-siostrzenica-morlana         | wisi Ariannie Verlen spory dług wdzięczności za ratunek Ofelii. | 0112-01-24

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 5 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210818-siostrzenica-morlana)) |
| Eustachy Korkoran    | 5 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210818-siostrzenica-morlana)) |
| Jolanta Sowińska     | 5 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli)) |
| Klaudia Stryk        | 5 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210818-siostrzenica-morlana)) |
| Elena Verlen         | 4 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni)) |
| Leona Astrienko      | 4 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210818-siostrzenica-morlana)) |
| Martyn Hiwasser      | 4 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni)) |
| Antonella Temaris    | 2 | ((210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli)) |
| Antoni Kramer        | 2 | ((210218-infernia-jako-goldarion; 210818-siostrzenica-morlana)) |
| Flawia Blakenbauer   | 2 | ((210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli)) |
| Martyna Bianistek    | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Nataniel Morlan      | 2 | ((210810-porwanie-na-gwiezdnym-motylu; 210818-siostrzenica-morlana)) |
| SC Fecundatis        | 2 | ((210813-szmuglowanie-antonelli; 210818-siostrzenica-morlana)) |
| SCA Płetwal Błękitny | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Bruno Baran          | 1 | ((210813-szmuglowanie-antonelli)) |
| Cień Brighton        | 1 | ((210813-szmuglowanie-antonelli)) |
| Franek Kuparał       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Kamil Lyraczek       | 1 | ((210414-dekralotyzacja-asimear)) |
| Lena Fenatil         | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Llarnagraht          | 1 | ((210414-dekralotyzacja-asimear)) |
| Lucjusz Blakenbauer  | 1 | ((210813-szmuglowanie-antonelli)) |
| Malictrix d'Pandora  | 1 | ((210414-dekralotyzacja-asimear)) |
| Maria Naavas         | 1 | ((210818-siostrzenica-morlana)) |
| Mariusz Tubalon      | 1 | ((210414-dekralotyzacja-asimear)) |
| Morrigan d'Tirakal   | 1 | ((210421-znudzona-zaloga-inferni)) |
| Ofelia Morlan        | 1 | ((210818-siostrzenica-morlana)) |
| Olgierd Drongon      | 1 | ((210818-siostrzenica-morlana)) |
| OO Infernia          | 1 | ((210218-infernia-jako-goldarion)) |
| OO Netrahina         | 1 | ((210818-siostrzenica-morlana)) |
| OO Żelazko           | 1 | ((210818-siostrzenica-morlana)) |
| Otto Azgorn          | 1 | ((210421-znudzona-zaloga-inferni)) |
| Persefona d'Infernia | 1 | ((210421-znudzona-zaloga-inferni)) |
| Renata Szarżun       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |
| SLX Gwiezdny Motyl   | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |