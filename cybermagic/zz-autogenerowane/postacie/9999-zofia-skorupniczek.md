---
categories: profile
factions: 
owner: public
title: Zofia Skorupniczek
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: N+C+ | niezwykle intensywna i amoralna, charyzmatyczna i przekonywująca - Bombshell, IDW | @ 240306-potwornosc-powojenna-trzykwiatu
* VALS: Achievement, Hedonism | sadystka i niezwykle precyzyjna badaczka magii; zrobi wszystko by pomóc Latiszy i Trzykwiatowi | @ 240306-potwornosc-powojenna-trzykwiatu

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240306-potwornosc-powojenna-trzykwiatu | Amoralna i sadystyczna (acz lojalna) naukowiec Esuriit w służbie Latiszy; ona zaprojektowała 'farmy ludzi' by dostarczać esurientowi posiłek, ona przekonała Latiszę o konieczności pójścia głębiej w Esuriit. A najsmutniejsze, że działała na niepełnych danych, bo dało się to zmienić. | 0084-04-16 - 0084-04-21 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Jakub Kurbeczko      | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Ksenia Byczajnik     | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Latisza Warkolnicz   | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Natalia Pszaruk      | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Natan Wierzbitowiec  | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Petra Łomniczajew    | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Radosław Zientarmik  | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Ulrich Warkolnicz    | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Zuzanna Szagbin      | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |