---
categories: profile
factions: 
owner: public
title: Dragan Halatis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230806-zwiad-w-iliminar-caos        | główny inżynier Kajrata; przebudował servar Culicyd by mieć inżynierski. Przekształcił Quispis, by był hoverAPC a nie tylko jednostką zwiadowczą. Wygląda jak niedźwiedź, ma chłopięcy entuzjazm. FATALNY w zastraszaniu. Zdecydowanie woli handlować z Enklawami niż po prostu zabijać ludzi. | 0081-06-22 - 0081-06-24 |
| 230906-operacja-mag-dla-symlotosu   | nie do końca ogarnia koncept 'dyskrecji', ale prawidłowo zmienił Culicydy pod kątem potrzeb Xavery i Amandy. Świetny inżynier. | 0081-06-26 - 0081-06-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Caelia Calaris       | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Ernest Kajrat        | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Leon Varkas          | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Lestral Kirmanik     | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Alaric Rakkeir       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Edmund Garzin        | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Isaura Velaska       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Xavera Sirtas        | 1 | ((230906-operacja-mag-dla-symlotosu)) |