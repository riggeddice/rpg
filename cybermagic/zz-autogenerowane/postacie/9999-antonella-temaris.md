---
categories: profile
factions: 
owner: public
title: Antonella Temaris
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210810-porwanie-na-gwiezdnym-motylu | eternijska szlachcianka, uciekinierka. Jej ród został wymordowany Krwawą Klątwą przez Nataniela Morlana, którego córka była jej przyjaciółką. Została "Duchem" ukrywając się na Gwiezdnym Motylu. | 0108-12-25 - 0108-12-30 |
| 210813-szmuglowanie-antonelli       | uszkodzona. Ma dysasocjację. Nie zapamiętuje. Poprzednie osobowości się jej mieszają. Nie podejmie decyzji. Ale nauczyła się nawyków i chce się przydać. | 0109-02-11 - 0109-02-23 |
| 210820-fecundatis-w-domenie-barana  | poszła za planem Jolanty i użyła mocy eternijskiej Esuriit by przekierować energię z Jolanty (a potem: załogi) by dać Flawii dość energii do nadprodukcji skorpioidowej pajęczyny. Lekko Skażona Esuriit. | 0109-03-06 - 0109-03-09 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210813-szmuglowanie-antonelli       | instynkt macierzyński do skorpioidów i insektów jadowitych; niestety, gubi je na lewo i prawo. | 0109-02-23
| 210813-szmuglowanie-antonelli       | ma rytuał i nawyk pozwalający jej na częściowe odzyskanie kontroli i reintegrację + uspokojenie się. | 0109-02-23
| 210820-fecundatis-w-domenie-barana  | pomniejsze Skażenie Esuriit; przepuściła przez siebie za dużo Krwi i energii Krwi. | 0109-03-09

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Flawia Blakenbauer   | 3 | ((210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Jolanta Sowińska     | 3 | ((210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Bruno Baran          | 2 | ((210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Cień Brighton        | 2 | ((210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Tomasz Sowiński      | 2 | ((210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli)) |
| Antos Kuramin        | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Damian Szczugor      | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Deneb Ira            | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Elsa Kułak           | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Franek Kuparał       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Kara Szamun          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Lena Fenatil         | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Leon Kantor          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Lucjusz Blakenbauer  | 1 | ((210813-szmuglowanie-antonelli)) |
| Nataniel Morlan      | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Renata Szarżun       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Rick Varias          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| SC Fecundatis        | 1 | ((210813-szmuglowanie-antonelli)) |
| SLX Gwiezdny Motyl   | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Tamara Mardius       | 1 | ((210820-fecundatis-w-domenie-barana)) |