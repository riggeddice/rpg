---
categories: profile
factions: 
owner: public
title: Kiran Ravis
---

# {{ page.title }}


# Generated: 



## Fiszki


* 21 lat, junior inżynier i mechanik, pracuje pod opieką starszych mechaników. | @ 230720-savaranie-przed-obliczem-nihilusa
* OCEAN: (O+E+N-): Ciekawy świata, energiczny, zawsze chętny do pomocy. "Chcę zrozumieć jak wszystko działa, żeby móc naprawiać i utrzymywać nasz dom w najlepszym stanie." | @ 230720-savaranie-przed-obliczem-nihilusa
* VALS: (Achievement, Stimulation): "Nie mogę doczekać się, kiedy będę mógł naprawiać silniki samodzielnie! Chcę być tak dobry jak starsi mechanicy!" | @ 230720-savaranie-przed-obliczem-nihilusa
* Core Wound - Lie: "Jestem za młody i niewystarczająco doświadczony, żeby być pożyteczny" - "Jeśli nauczę się jak najwięcej, będę niezbędny dla załogi." | @ 230720-savaranie-przed-obliczem-nihilusa
* styl: Pełen entuzjazmu, otwarty, nieco naiwny, ale zawsze chętny do nauki. | @ 230720-savaranie-przed-obliczem-nihilusa
* rola: zaprzyjaźnił się z Ralfem, nie rozumie co się tu dzieje | @ 230720-savaranie-przed-obliczem-nihilusa

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230720-savaranie-przed-obliczem-nihilusa | młodszy inżynier na 'Kantali Ravis' i syn kapitana; powitał Zespół i dał im kody kontrolne i potrzebne dostępy. Polubił się z Ralfem. | 0092-07-01 - 0092-07-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Agnieszka Serkis     | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Arnold Tapszecz      | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Igor Seklamant       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Jola Seklamant       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| JRN Kantala Ravis    | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Katrina Kirten       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Lena Morazik         | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Marcelin Viirdus     | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Rafał Kurrodis       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Ralena Annitas       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Ralf Tapszecz        | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |