---
categories: profile
factions: 
owner: public
title: Jakub Kurbeczko
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240306-potwornosc-powojenna-trzykwiatu | sierżant; chciał szukać zaginionych ale doszedł do tego że Latisza coś wie. Lubiany, skonfrontował się z Latiszą - nie jest tą samą osobą co kiedyś. Zamknął Ksenię w piwnicy by czegoś głupiego nie zrobiła i postanowił z Zuzą i Natalią zniszczyć esurienta oddając kontrolę Pustogorowi. | 0084-04-16 - 0084-04-21 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 240306-potwornosc-powojenna-trzykwiatu | uznany za zdrajcę Trzykwiata, został regentem Trzykwiata z ramienia Pustogoru. Do końca wspiera Trzykwiat i przyjaciół, nawet pod terminusami. | 0084-04-21

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ksenia Byczajnik     | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Latisza Warkolnicz   | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Natalia Pszaruk      | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Natan Wierzbitowiec  | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Petra Łomniczajew    | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Radosław Zientarmik  | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Ulrich Warkolnicz    | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Zofia Skorupniczek   | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Zuzanna Szagbin      | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |