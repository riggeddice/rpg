---
categories: profile
factions: 
owner: public
title: Marian Fartel
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200916-smierc-raju                  | lekarz, pozyskujący zasoby i rzeczy używając kotów. Przekonał Garwena do użycia wszelkiej materii organicznej jako substraty. Bardzo racjonalny i ekonomiczny. | 0111-02-18 - 0111-02-20 |
| 200923-magiczna-burza-w-raju        | lekarz, który miał MNÓSTWO roboty podczas burzy magicznej i przed nią, naprawiając chorych ludzi. | 0111-02-21 - 0111-02-25 |
| 201014-krystaliczny-gniew-elizy     | noktiański lekarz używający symbiotycznych kotów, współpracujący z Elizą Irą by ratować Trzeci Raj. Pamięta przeszłość - a jednak chce współpracy. | 0111-03-02 - 0111-03-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazja Sowińska   | 3 | ((200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Arianna Verlen       | 3 | ((200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Elena Verlen         | 3 | ((200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Eustachy Korkoran    | 3 | ((200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Klaudia Stryk        | 3 | ((200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Ataienne             | 2 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy)) |
| Dariusz Krantak      | 2 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Eliza Ira            | 2 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Izabela Zarantel     | 2 | ((200916-smierc-raju; 200923-magiczna-burza-w-raju)) |
| Aleksandra Termia    | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Marianna Lemurczak   | 1 | ((200923-magiczna-burza-w-raju)) |
| Nikodem Sowiński     | 1 | ((200923-magiczna-burza-w-raju)) |
| OO Wesoły Wieprzek   | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Rafał Armadion       | 1 | ((200916-smierc-raju)) |
| Robert Garwen        | 1 | ((200916-smierc-raju)) |
| Wanessa Pyszcz       | 1 | ((200916-smierc-raju)) |