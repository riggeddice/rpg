---
categories: profile
factions: 
owner: public
title: TAI Marszałek Grzmotoszpon Trzeci
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210929-grupa-ekspedycyjna-kellert   | Jedno z TAI na K1, które lubi podróżować. Ale nie może, więc ogląda filmy i się tam "wkleja". Wykrył, że dane ze zwiadowców to było 100% sfabrykowany footage z danych już dostępnych na K1. | 0112-03-13 - 0112-03-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Aleksandra Termia    | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Antoni Kramer        | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Arianna Verlen       | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Elena Verlen         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Eustachy Korkoran    | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Klaudia Stryk        | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Leona Astrienko      | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Martyn Hiwasser      | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Infernia          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Otto Azgorn          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Vigilus Mevilig      | 1 | ((210929-grupa-ekspedycyjna-kellert)) |