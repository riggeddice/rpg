---
categories: profile
factions: 
owner: public
title: SN Murisatia
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230923-ciemnosc-pozerajaca-arcadalian | pierwsza jednostka gdzie uciekli zbiegowie-kultyści; wpadła w kłopoty, pomógł im Arcadalian. To było źródło infekcji problemów na Arcadalian. Murisatia jest w dobrym stanie, nic się jej nie stało. | 0091-07-30 - 0091-08-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aulus Terrentus      | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Daven Hassik         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Iwo Bretonis         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Lester Martz         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Livia Sertiano       | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| OLU Luminarius       | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Salma Bluszcz        | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| SN Arcadalian        | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| SN Ferrivat          | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |