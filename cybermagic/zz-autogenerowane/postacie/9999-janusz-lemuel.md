---
categories: profile
factions: 
owner: public
title: Janusz Lemuel
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230810-nie-nazywasz-sie-janusz      | agent Syndykatu; Dotknięty Koroną Saitaera zapomniał kim jest i pomógł Łucji walczyć z Syndykatem z pomocą Xairy; jednak zorientował się, że jest członkiem Syndykatu. Przekonał przyjaciela (Dariusza), że mogą być wolni i ewakuowali wszystkich przyjaciół. Po czym odleciał by być wśród "Wolnych TAI". | 0093-03-29 - 0093-03-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Artur Wrulgop        | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Dariusz Avatrinn     | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Krzesimir Pluszcz    | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Łucja Kritoriin      | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Maja Nerwial         | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Nova Atreia          | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| OWT Łowca Martwych Gwiazd | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| TAI Xaira Cognitia   | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Teresa Kritoriin     | 1 | ((230810-nie-nazywasz-sie-janusz)) |