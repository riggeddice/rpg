---
categories: profile
factions: 
owner: public
title: Cyryl Perikas
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210817-zgubiony-holokrysztal-w-lesie | "najszybszy" kurier i pilot ścigacza wśród Rekinów. Gdy terminus nań zapolował, odrzucił obiekt kurierowany (ko-matrycę Kuratorów). I nie mógł potem znaleźć (bo zwinięto ją). | 0111-06-22 - 0111-06-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Bartozol        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Kacper Bankierz      | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Laura Tesinik        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Liliana Bankierz     | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Mariusz Trzewń       | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Marysia Sowińska     | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Rupert Mysiokornik   | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Stella Armadion      | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Tomasz Tukan         | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |