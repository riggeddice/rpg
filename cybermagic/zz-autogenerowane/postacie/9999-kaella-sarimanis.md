---
categories: profile
factions: 
owner: public
title: Kaella Sarimanis
---

# {{ page.title }}


# Generated: 



## Fiszki


* 13 lat: zniknęła, protomag Noctis która miała więcej szczęścia i od 2 lat była adoptowana. Zakochana w bajce Księżniczki Arianny | @ 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie | Porwana przez gang polujący na dzieci i młodzież trzynastolatka. Protomag, eks-Noctis. W panice stworzyła manifestację ‘Księżniczki Arianny’ jako istotę Alucis/Esuriit. Dzięki temu porwane dzieci zostały porwane ponownie do relatywnie bezpiecznego miejsca. | 0085-01-26 - 0085-01-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arnulf Poważny       | 1 | ((231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Klaudia Stryk        | 1 | ((231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Mariusz Trzewń       | 1 | ((231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Teresa Mieralit      | 1 | ((231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Tymon Grubosz        | 1 | ((231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |