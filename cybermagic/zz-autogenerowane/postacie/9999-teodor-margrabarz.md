---
categories: profile
factions: 
owner: public
title: Teodor Margrabarz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220716-chory-piesek-na-statku-luxuritias | lord strażnik Santorinusów i mag (44), współpracując z Achellorem przeszmuglował piękne kwiaty z Neikatis i zaraził się Śmiechowicą. Niebezpieczny w walce, ale obezwładniony przez Martyna jednym zaklęciem. | 0109-05-05 - 0109-05-07 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Achellor Santorinus  | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Bożena Kokorobant    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Fabian Korneliusz    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Henryk Urkon         | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Klaudia Stryk        | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Martyn Hiwasser      | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| OO Serbinius         | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Roberto Santorinus   | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| SL Rajasee Bagh      | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |