---
categories: profile
factions: 
owner: public
title: OnS Valuvanok
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240313-ostatni-lot-valuvanok        | fregata transportująca zahibernowanych ludzi na linii Iorus - Anomalia, gdzie doszło do wprowadzenia Plagi. Samozniszczenie po działaniach Zespołu. | 0082-11-28 - 0082-11-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ewelina Kalwiert     | 1 | ((240313-ostatni-lot-valuvanok)) |
| Jasper Arimetus      | 1 | ((240313-ostatni-lot-valuvanok)) |
| Mara Arimetus        | 1 | ((240313-ostatni-lot-valuvanok)) |
| Xavier Kalwiert      | 1 | ((240313-ostatni-lot-valuvanok)) |