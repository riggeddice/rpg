---
categories: profile
factions: 
owner: public
title: Frederico Zyklas
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231202-protest-przed-kultem-na-szernief | kiedyś kultysta, wyciągnęła go żona. Przy protestach zginęła jego żona i dziecko. Próbował ich przywrócić w rozpaczy i sprawił, że pojawiła się Anomalia. Idąc za jej podszeptem zabił arystokratkę za pomocą zdalnie sterowanego drona z ładunkiem wybuchowym. Próbował popełnić samobójstwo, ale złapała go policja. Wyjaśnił wszystko Agencji, gdy był przesłuchany. | 0103-10-15 - 0103-10-19 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aerina Cavalis       | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Kalista Surilik      | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Klasa Biurokrata     | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Klasa Hacker         | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| OLU Luminarius       | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Vanessa d'Cavalis    | 1 | ((231202-protest-przed-kultem-na-szernief)) |