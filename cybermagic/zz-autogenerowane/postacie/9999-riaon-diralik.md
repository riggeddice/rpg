---
categories: profile
factions: 
owner: public
title: Riaon Diralik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230102-elwira-koszmar-nox-ignis     | młody noktiański chłopak, nastolatek, próbuje okraść Dominika. Nie ma dla siebie przyszłości jako poeta na tej stacji. Talia chce dla niej lepszego życia. | 0082-08-02 - 0082-08-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aida Liminis         | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| AK Nox Ignis         | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Aletia Nix           | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Brunon Szwagacz      | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Dominik Łarnisz      | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Franz Szczypiornik   | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Medea Sowińska       | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| OO Loricatus         | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Talia Derwisz        | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Tatiana Ozariat      | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Tristan Ozariat      | 1 | ((230102-elwira-koszmar-nox-ignis)) |