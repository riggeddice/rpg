---
categories: profile
factions: 
owner: public
title: John Wytwórca
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190720-sos-w-eterze                 | naprawił generator umożliwiając "Oku" powrót do portu. Zabił krakena razem z Billym i Gregiem. | 0110-11-01 - 0110-11-03 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190720-sos-w-eterze                 | Pryzowe za przyprowadzenie "Oka" oraz rekompensata od Senetis za krakena. | 0110-11-03

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Billy Czuwak         | 1 | ((190720-sos-w-eterze)) |
| Greg Światełko       | 1 | ((190720-sos-w-eterze)) |
| Okręt Oko prawdy     | 1 | ((190720-sos-w-eterze)) |