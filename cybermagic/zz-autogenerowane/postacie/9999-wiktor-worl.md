---
categories: profile
factions: 
owner: public
title: Wiktor Worl
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240111-zlomowanie-legendarnej-anitikaplan | kapitan SN Varilen, bardzo próbuje zezłomować Anitikaplan; jedna z ostatnich szans i okazji by przetrwać biznesowo. Współpracuje z Agencją zwłaszcza po tym jak dostał rekompensatę finansową. | 0084-04-02 - 0084-04-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klasa Biurokrata     | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Klasa Hacker         | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Klasa Oficer Naukowy | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Klasa Sabotażysta    | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| SN Anitikaplan       | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| SN Varilen           | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Twaróg Worl          | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |