---
categories: profile
factions: 
owner: public
title: Stefan Torkil
---

# {{ page.title }}


# Generated: 



## Fiszki


* pilot, K1 | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* ENCAO:  -+--0 |Nie lubi ryzyka;;Spontaniczny| VALS: Benevolence >> Stimulation| DRIVE: Odbudować reputację | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej

### Wątki


kosmiczna-chwala-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220928-kapitan-verlen-i-pojedynek-z-marine | pilot Królowej Kosmicznej Chwały; robi co może. Nie pochodzi z Aurum. | 0100-05-14 - 0100-05-16 |
| 221019-kapitan-verlen-i-pierwszy-ruch-statku | kiedyś przyjaciel Marcela; rozbił jednostkę po pijanemu i jego dziewczyna zginęła. Nie używa neurosprzężenia przez to. Ma dobrą reakcję na to, że Marcel wygląda jak jego była dziewczyna. | 0100-05-23 - 0100-06-04 |
| 221026-kapitan-verlen-i-koniec-przygody-na-krolowej | zrobił kilka kółek Królową, nic wyczynowego, ale dał radę. Potwierdził że statek działa. | 0100-06-08 - 0100-06-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Daria Czarnewik      | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Erwin Pies           | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| OO Królowa Kosmicznej Chwały | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Arnulf Perikas       | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Leszek Kurzmin       | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Maja Samszar         | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Tomasz Ruppok        | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Alezja Dumorin       | 1 | ((220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Antoni Kramer        | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Grażyna Burgacz      | 1 | ((220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Klaudiusz Terienak   | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Leona Astrienko      | 1 | ((220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Marcelina Trzęsiel   | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| OO Tucznik Trzeci    | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Romeo Verlen         | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Rufus Warkoczyk      | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Szymon Wanad         | 1 | ((220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Władawiec Diakon     | 1 | ((220928-kapitan-verlen-i-pojedynek-z-marine)) |