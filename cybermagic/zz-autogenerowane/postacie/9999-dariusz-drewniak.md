---
categories: profile
factions: 
owner: public
title: Dariusz Drewniak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211122-czolenkowe-esuriit-w-amz     | nauczyciel wojny. Były terminus i komandos. Wziął Beacon mający zwabić Agenta Esuriit i wprowadził go do Labiryntu Nieskończonego. Energie Esuriit + Beacon poważnie wypaliły mu moc. | 0083-10-13 - 0083-10-22 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211122-czolenkowe-esuriit-w-amz     | 3 miesiące ciężkiej regeneracji w Szpitalu Terminuskim, potem 6 miesięcy w Domu Weteranów zanim na tyle wydobrzeje by móc wrócić do AMZ na stanowisko nauczyciela emeritus. | 0083-10-22

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ernest Termann       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Felicjan Szarak      | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Klaudia Stryk        | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Ksenia Kirallen      | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Mariusz Trzewń       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Wiktor Szurmak       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |