---
categories: profile
factions: 
owner: public
title: Tristan Rialirat
---

# {{ page.title }}


# Generated: 



## Fiszki


* wujek Kirei, lekarz (savaranin); ciężko poraniony kiedyś, powymieniane na prymitywne implanty. Ledwo ludzka forma. | @ 221214-astralna-flara-kontra-domina-lucis
* ENCAO:  0-+-0 |Brutalny, bezwzględny;;Solidny, twardy;;Zawsze wie co, gdzie i kiedy| VALS: Face, Conformity >> Humility, Hedonism| DRIVE: Dominacja | @ 221214-astralna-flara-kontra-domina-lucis
* "Uratuję Cię, Kireo, moja kochana iskierko - jak nie dał rady Twój ojciec" | @ 221214-astralna-flara-kontra-domina-lucis

### Wątki


historia-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221214-astralna-flara-kontra-domina-lucis | KIA. Savaranin. Przybył na Orbiter się poddać by być z Kireą ("krewną"). Zabił ją i siebie by nie wpadli w ręce Orbitera. | 0100-10-09 - 0100-10-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Axel Nargan          | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Daria Czarnewik      | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Ellarina Samarintael | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Gabriel Lodowiec     | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Kirea Rialirat       | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Leszek Kurzmin       | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| NekroTAI Zarralea    | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| OO Astralna Flara    | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| OO Athamarein        | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Sarian Xadaar        | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |