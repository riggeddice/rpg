---
categories: profile
factions: 
owner: public
title: Martauron Attylla
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180701-dwa-rejsy-w-potrzebie        | gladiator Luxuritiasu; stworzony kiedyś z człowieka, teraz vicinius potrzebujący koordynatorki do działania. Rozpoczął rebelię z Emilią - nawet posiekał (acz nie zabił) terminusa. Skończył z Emilią w Eterze Nieskończonym... | 0109-08-07 - 0109-08-11 |
| 200122-kiepski-altruistyczny-terroryzm | gladiator i aparycja Emilii; zintegrowany z Persefoną uzyskał jej umiejętności taktyczne i strategiczne. | 0110-09-15 - 0110-09-17 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180701-dwa-rejsy-w-potrzebie        | przetrwał w Eterze Nieskończonym i ma stałe, stabilne sprzężenie z Emilią. Jest w bezpiecznym miejscu. Luxuritias wydał na niego wyrok śmierci. | 0109-08-11

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Emilia Kariamon      | 2 | ((180701-dwa-rejsy-w-potrzebie; 200122-kiepski-altruistyczny-terroryzm)) |
| Arianna Verlen       | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Eustachy Korkoran    | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Fabian Komczatkow    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Gerard Weiner        | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Kacper Wontarczyk    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Kalina Rotmistrz     | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Klaudia Stryk        | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Krystian Moborok     | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Liwia Barbaturran    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Marcjanna Maszotka   | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Natalia Miszryk      | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Sabrina Powsimrożek  | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Tymon Barbaturran    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Wojciech Zajcew      | 1 | ((180701-dwa-rejsy-w-potrzebie)) |