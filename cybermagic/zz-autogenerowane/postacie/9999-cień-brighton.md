---
categories: profile
factions: 
owner: public
title: Cień Brighton
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210813-szmuglowanie-antonelli       | kapitan SC Fecundatis. Rozplątał "intrygę" Tomasza, wydobył od Jolanty mroczne sekrety o listach miłosnych i skłonił ją do podróży z nim do Pasa Kazimierza. Ojcowski styl - chce pomóc Antonelli tylko jeśli jej to pomoże. Przewozi skorpioidy, kiepsko poluje na szczury i skutecznie gubi łowców nagród ;-). | 0109-02-11 - 0109-02-23 |
| 210820-fecundatis-w-domenie-barana  | zdecydował się pomóc Elsie i Karze (eliminacja miragenta); zaryzykował Fecundatis, by tylko ochronić Szamunczak przed armią Strachów. Stracił cargo (skorpioidy) i trochę zdrowia, ale dał radę. | 0109-03-06 - 0109-03-09 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210813-szmuglowanie-antonelli       | ciało starsze niż powinno być. Dużo depigmentacji od energii magicznych. | 0109-02-23
| 210813-szmuglowanie-antonelli       | nałapał kontaktów w Pustogorze ("znam tam kogoś") i jest "oficjalnym ukrytym szmuglerem". | 0109-02-23
| 210820-fecundatis-w-domenie-barana  | zawsze ma miejsce w Szamunczaku za obronę przed Strachami. Wielkie osiągnięcie. | 0109-03-09

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antonella Temaris    | 2 | ((210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Bruno Baran          | 2 | ((210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Flawia Blakenbauer   | 2 | ((210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Jolanta Sowińska     | 2 | ((210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Antos Kuramin        | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Damian Szczugor      | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Deneb Ira            | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Elsa Kułak           | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Kara Szamun          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Leon Kantor          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Lucjusz Blakenbauer  | 1 | ((210813-szmuglowanie-antonelli)) |
| Rick Varias          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| SC Fecundatis        | 1 | ((210813-szmuglowanie-antonelli)) |
| Tamara Mardius       | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Tomasz Sowiński      | 1 | ((210813-szmuglowanie-antonelli)) |