---
categories: profile
factions: 
owner: public
title: Perdius Aximar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220925-mlodziaki-na-savaranskim-statku-handlowym | kapitan 'Hektora 17'; savaranin. Z uwagi na chorobę pilota zatrudnił młodych Orbiterowców i wpadł w tarapaty przez inspekcję Orbitera i różnice kulturowe. Szczęśliwie dogadał się z Klaudią i po oddaniu jej tych Orbiterowców może kontynuować odbudowę jednostki domowej. | 0109-05-24 - 0109-05-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dawid Aximar         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Fabian Korneliusz    | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Klaudia Stryk        | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Klaudiusz Zanęcik    | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Martyn Hiwasser      | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| OO Serbinius         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| SC Hektor 17         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |