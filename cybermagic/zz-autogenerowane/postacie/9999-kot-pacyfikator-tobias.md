---
categories: profile
factions: 
owner: public
title: kot-pacyfikator Tobias
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230303-the-goose-from-hell          | A cat who hunts and teleports, found the plant-rat, got shot at and helped locate the menacing goose. Also made a mess in the bar. | 0111-10-28 - 0111-10-30 |
| 230331-an-unfortunate-ratnapping    | mischevious one, helps capture the rats and plays a crucial role in the story when it is found in the Guardians' truck (where he sneaked in to hunt some rats). | 0111-11-08 - 0111-11-10 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alex Deverien        | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Carmen Deverien      | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Julia Kardolin       | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Paweł Szprotka       | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Alicja Trawlis       | 1 | ((230303-the-goose-from-hell)) |
| Radosław Turkamenin  | 1 | ((230331-an-unfortunate-ratnapping)) |
| Teresa Mieralit      | 1 | ((230303-the-goose-from-hell)) |