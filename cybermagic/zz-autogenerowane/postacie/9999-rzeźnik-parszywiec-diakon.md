---
categories: profile
factions: 
owner: public
title: Rzeźnik Parszywiec Diakon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220622-lewiatan-za-pandore          | PRZESZŁOŚĆ: kiedyś mag, uznawany za potwora. Zdradził swoich ludzi z Orbitera, potem zdradził noktian i ich zniszczył syntetyzując w swoim ciele plagę. Ekspert od broni biologicznej, 69-letni kapitan Straszliwego Pająka. Noktianie do dziś go nienawidzą. Jowialny i sympatyczny z natury, lojalny Ariannie. | 0112-09-30 - 0112-10-03 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((220622-lewiatan-za-pandore)) |
| Eustachy Korkoran    | 1 | ((220622-lewiatan-za-pandore)) |
| Klaudia Stryk        | 1 | ((220622-lewiatan-za-pandore)) |
| Maria Naavas         | 1 | ((220622-lewiatan-za-pandore)) |
| Ola Szerszeń         | 1 | ((220622-lewiatan-za-pandore)) |
| OO Infernia          | 1 | ((220622-lewiatan-za-pandore)) |
| OO Pandora           | 1 | ((220622-lewiatan-za-pandore)) |
| OO Straszliwy Pająk  | 1 | ((220622-lewiatan-za-pandore)) |
| OO Tivr              | 1 | ((220622-lewiatan-za-pandore)) |
| Raoul Lavanis        | 1 | ((220622-lewiatan-za-pandore)) |