---
categories: profile
factions: 
owner: public
title: Kasia Karmnik
---

# {{ page.title }}


# Generated: 



## Fiszki


* (ENCAO:  +00-- |Zapominalska;;Z natury odrzuca wszystkie pomysły| VALS: Face, Family >> Power| DRIVE: Być jak idol (admirał Termia)) | @ 230117-ros-wiertloplyw-i-tai-mirris
* atarienka: "Możesz polegać tylko na sobie" | @ 230117-ros-wiertloplyw-i-tai-mirris
* kadencja: irytująca młodsza siostra | @ 230117-ros-wiertloplyw-i-tai-mirris
* "To niesprawiedliwe, nie pasuję tu!" | @ 230117-ros-wiertloplyw-i-tai-mirris
* SPEC: advancer, technik | @ 230117-ros-wiertloplyw-i-tai-mirris

### Wątki


ratownicy-ostatniej-szansy

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230117-ros-wiertloplyw-i-tai-mirris | atarienka, młoda i kompetentna ratowniczka która NIE CHCE być przypisana do grupy noktian. Mimo początkowej niechęci i sporów z Tristanem, przekonuje się do Zespołu bo jest kompetentny. | 0084-02-22 - 0084-02-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eleonora Alintirias  | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Lucas Septrien       | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Łucja Nirimis        | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Napoleon Myszogłów   | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Niferus Sentriak     | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| OR Błyskawica        | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| SC Wiertłopływ       | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| TAI Mirris d'Paravilius | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Talia Irris          | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Tristan Andrait      | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |