---
categories: profile
factions: 
owner: public
title: Seweryn Grzęźlik
---

# {{ page.title }}


# Generated: 



## Fiszki


*  | @ 231027-planetoida-bogatsza-niz-byc-powinna
* rola: psychotronik, blakvelowiec | @ 231027-planetoida-bogatsza-niz-byc-powinna
* personality: neuro, low-extra, consc (workaholic, obsesyjny, cichy, skłonności do wybuchania, umiłowanie porządku) | @ 231027-planetoida-bogatsza-niz-byc-powinna
* values: self-direction, self-enhancement | @ 231027-planetoida-bogatsza-niz-byc-powinna
* wants: zrozumieć sekret Mirandy, Blakvel forever, ciszy i rozwoju | @ 231027-planetoida-bogatsza-niz-byc-powinna

### Wątki


planetoidy-kazimierza
sekret-mirandy-ceres

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220327-wskrzeszenie-krolowej-przygod | psychotronik blakvelowców; chciał się włamać do Ceres i zmienić dowodzenie na Damiana. Okazało się, że się nie da; oddelegowany w imieniu blakvelowców do Królowej Przygód. | 0108-05-01 - 0108-05-16 |
| 231027-planetoida-bogatsza-niz-byc-powinna | narzeka na Ceres. Bo się zawiesza, bo coś nie działa, ogólnie - wyraźnie nie chce być na tej jednostce. Ale dzięki swojej kompetencji wyprowadził Mirandę z pętli po zobaczeniu potwora Alteris. | 0108-06-29 - 0108-07-02 |
| 220329-mlodociani-i-pirat-na-krolowej | zgryźliwy, kochający ciszę i nie znoszący dzieci. Zdekodował manifest jednostki pirackiej i miał sporo dobrych rad. Coś podejrzewa o Mirandzie. | 0108-08-29 - 0108-09-09 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Gotard Kicjusz       | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Łucjan Torwold       | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Miranda Ceres        | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Prokop Umarkon       | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| SC Królowa Przygód   | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Anna Szrakt          | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Bartek Burbundow     | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Bartek Wudrak        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Berdysz Rozdzieracz  | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Damian Szczugor      | 1 | ((220327-wskrzeszenie-krolowej-przygod)) |
| Grzegorz Fabutownik  | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Helena Banbadan      | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Juliusz Cieślawok    | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Kara Prazdnik        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Kazimierz Zamglis    | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Mikołaj Resztkowiec  | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Romana Kundel        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Wojciech Kaznodzieja | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |