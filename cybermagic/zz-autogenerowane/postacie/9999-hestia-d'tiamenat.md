---
categories: profile
factions: 
owner: public
title: Hestia d'Tiamenat
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190928-ostatnia-misja-tarna         | strażnicza i naukowa TAI Tiamenat, superturing; dla ochrony Tiamenat potrafiła współpracować z Tarnem. Przerażona możliwościami BIA lv3. | 0110-01-27 - 0110-01-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| BIA Tarn             | 1 | ((190928-ostatnia-misja-tarna)) |
| Eustachy Mrownik     | 1 | ((190928-ostatnia-misja-tarna)) |
| Pedro Ronfak         | 1 | ((190928-ostatnia-misja-tarna)) |
| Talia Aegis          | 1 | ((190928-ostatnia-misja-tarna)) |
| Wiktor Satarail      | 1 | ((190928-ostatnia-misja-tarna)) |