---
categories: profile
factions: 
owner: public
title: Minerwa Metalia
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "pustogor"
* owner: "public"
* title: "Minerwa Metalia"


## Postać

### Ogólny pomysł (2)

* Neuronautka i naukowiec, specjalizująca się w psychotronice. Niezła konstruktorka power suitów. Naukowiec z serca.
* Kiedyś, urocza i otwarta Diakonka owijająca wszystkich jednym paluszkiem.
* Zrekonstruowana przez Saitaera i zregenerowana przez Pustogor w zwykłą czarodziejkę.

### Misja (co dla innych) (2)

* Dąży do ewolucji ludzkości; niech ludzkość i człowieczeństwo są najlepszą formą siebie
* Szybki postęp w psychotronice i magitechnologii - by tworzyć byty inne niż tylko magowie i ludzie, lepsze i pomocne
* Naturalna optymistka - narzędzia zawsze są dobre, po prostu nie można przekazać ich w złe ręce

### Motywacja (co dla mnie) (2)

* Chce być akceptowana i chce być częścią tego świata. Chce mieć przyjaciół i być ceniona. Szuka akceptacji.
* Jest świadoma swojego ciała i wszystkich aspektów cielesnych. Trochę hedonistka. Lubi luksus i rozpieszczanie. Tęskni za Dotykiem Saitaera.

### Wyróżnik (2)

* Wybitnej klasy neuronautka: specjalizuje się w konstrukcji sztucznych umysłów i osobowości, ale też poradzi sobie z analizą
* Badaczka anomalii, zwłaszcza technomantycznych: potrafi robić cuda związane z dziwnymi anomaliami technomantycznymi
* Urocza dusza towarzystwa: potrafi się zachowywać w odpowiedni, bardzo pociągający innych sposób
* Ciało Saitaera: przede wszystkim defensywnie. Jest w stanie funkcjonować w warunkach niemożliwych dla maga czy człowieka

### Zasoby i otoczenie (3)

* Black Technology: rozumie dziwne technomantyczne anomalie. Saitaer jej to "dał". Nie wie czemu to rozumie i jak je budować, ale potrafi...
* Black Stories: dziwne opowieści, z odmiennych światów i czasów. Znowu, dar Saitaera. Im bardziej się oddalamy od Primusa, tym lepiej ona poznaje świat.
* Koszmary: Minerwa ma nieprawdopodobnie chore koszmary i myśli oraz nieludzki umysł. Jej ciało i umysł nie radzą sobie ze wspomnieniami Saitaera i nieludzkiego ciała.
* Wskrzeszona: większość magów którzy o niej wiedzą wie też o jej historii. Sprawia to, że Minerwa jest fundamentalnie minusem społecznie.

### Magia (3)

#### Gdy kontroluje energię

* Mentalistka: zwłaszcza w dziedzinie konstrukcji i tworzenia/modyfikacji sygnału. Raczej pracuje na wzorach (modelach mentalnych) niż pojedynczych myślach i pamięci
* Technomantka: przede wszystkim analiza i konstrukcja. Dotyk Saitaera dał jej unikalną zdolność rekonstrukcji mechanicznej
* Golemy technomantyczne: wiedza o psychotronice i Dotyk Saitaera dały jej coś nowego - umiejętność łatwego tworzenia inteligentnych agentów. Nigdy nie jest sama...

#### Gdy traci kontrolę

* Pojawiają się anomalie, zwłaszcza technomantyczne. Bonus, jeśli pochodzą z martwego świata Saitaera.
* Saitaer widzi obszar w którym zaklęcie jest rzucone przez Minerwę.

### Powiązane frakcje

{{ page.factions }}

## Opis

(15 min)


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181024-decyzja-minerwy              | zastąpiła Nutkę jako servar Erwina. Próbowała sprawdzić granicę autonomii i człowieczeństwa. Nieszczęśliwa w formie servara, ale zostanie - by Erwin nie był sam. | 0109-10-01 - 0109-10-04 |
| 181027-terminuska-czy-kosmetyczka   | testuje swoje nowe możliwości; powiedziała Pięknotce o zbliżającym się problemie z cybergrzybami inkarnaty (?); w pewnym momencie straciła kontrolę do terrorforma i tego nie zauważyła. | 0109-10-07 - 0109-10-11 |
| 181230-uwiezienie-saitaera          | tak dalece pochłonięta przez Saitaera, że chciała przetrwać kosztem ludzi. Zrekonstruowana przez Saitaera po asymilacji nieszczęsnego człowieka. | 0109-12-07 - 0109-12-09 |
| 190120-nowa-minerwa-w-nowym-swiecie | w nowym ciele, 7 smutnych wspomnień i 4 dobre, niepewna i zestresowana. Chce odejść, ale zostaje (na razie) w Zaczęstwie. Największe osiągnięcie psychotroniki w Nutce z Black Technology. | 0110-01-22 - 0110-01-26 |
| 190127-ixionski-transorganik        | bardzo zależy jej na pomocy Karolinie Erenit, więc użyła transorganizacji ixiońskiej na Wojtka. Niestety. Potem odwracała z pomocą Pięknotki. | 0110-01-29 - 0110-01-30 |
| 190202-czarodziejka-z-woli-saitaera | główny detektor Saitaera i energii ixiońskich. Dodatkowo, przez to, że dotknęła energią Karoliny, dała pretekst Karli by ona sprawdziła Karolinę Karradraelem. | 0110-02-01 - 0110-02-05 |
| 190828-migswiatlo-psychotroniczek   | stworzyła własne TAI do walki na arenie migświatła w Zaczęstwie; w ten sposób się z przyjemnością realizuje. Zabrali jej TAI bo boją się energii ixiońskiej. | 0110-02-08 - 0110-02-10 |
| 190206-nie-da-sie-odrzucic-mocy     | wspiera Pięknotkę by ratować Karolinę Erenit w samym sercu ixiońsko Skażonej Cyberszkoły, mimo, że zostaje ranna przy okazji. Przekierowała na siebie Minerwę by Pięknotka mogła zająć się Karoliną. | 0110-02-17 - 0110-02-20 |
| 190210-minerwa-i-kwiaty-nadziei     | coraz bardziej zaprzyjaźniała się z Kornelem, co przerwała Pięknotka. Obudziła i rozpoznała Kwiaty Nadziei z Ixionu. | 0110-02-21 - 0110-02-23 |
| 190402-eksperymentalny-power-suit   | stworzyła kiedyś psychotronikę Cienia i próbowała nad nim zapanować. Niestety, nie udało jej się - skończyła w szpitalu. | 0110-03-15 - 0110-03-17 |
| 190424-budowa-ixionskiego-mimika    | najlepsza psychotroniczka i katalistka ixiońska w okolicy. Pomogła Pięknotce zbudować bezpiecznego mimika ixiońskiego, choć zapłaciła za to zdrowiem. | 0110-04-03 - 0110-04-05 |
| 190427-zrzut-w-pacyfice             | pomogła Pięknotce zrozumieć dlaczego Cień zachowuje się tak pasywnie, mimo, że nadal leży w szpitalu | 0110-04-07 - 0110-04-09 |
| 190429-sabotaz-szeptow-elizy        | wyszła ranna, wróciła bardziej ranna; Cień wyraźnie chce ją zabić. Kontroluje energię ixiońską i dzięki temu z Alanem i Erwinem złożyli doskonały rezonator. | 0110-04-10 - 0110-04-13 |
| 190502-pierwszy-emulator-orbitera   | leżąc w szpitalu pomaga Pięknotce zrozumieć o co chodzi z Emulatorami Orbitera i jak bardzo to niebezpieczna sprawa. | 0110-04-14 - 0110-04-16 |
| 190503-bardzo-nieudane-porwania     | polowali na nią Kirasjerzy; Pięknotka przekonała ją do zamieszkania w pustogorskim Miasteczku. Tam jest bezpieczna. | 0110-04-18 - 0110-04-20 |
| 190901-polowanie-na-pieknotke       | przyszła pomóc Pięknotce zregenerować Cienia i dowiedzieć się kim jest napastnik. Udało się, acz rozwaliła pokój w szpitalu. | 0110-06-21 - 0110-06-24 |
| 191103-kontrpolowanie-pieknotki-pulapka | rozpoczyna handel TAI i artefaktami / anomaliami z Aurum przez Grzymościa by zdobyć zasoby do badań. Idzie, jak to ona, o jeden krok za daleko. Jej cień-terminus ją wspiera. | 0110-06-24 - 0110-07-01 |
| 191112-korupcja-z-arystokratki      | doradziła Pięknotce, że Persefona jest nietypową TAI do miasta; dodatkowo, jak zbadać czy działa i co z nią nie tak. | 0110-07-04 - 0110-07-05 |
| 191201-ukradziony-entropik          | zdalna konsultantka Pięknotki odnośnie różnic między Persefoną i Hestią. Potrafi, ku osłupieniu Pięknotki, zniszczyć AI zdalnie. | 0110-07-13 - 0110-07-15 |
| 200311-wygrany-kontrakt             | współkonspiratorka z Lucjuszem Blakenbauerem, skupiła się na użyciu energii ixiońskich do pomocy Kajratowi - zarówno Ernestowi jak i Amandzie | 0110-07-20 - 0110-07-23 |
| 200202-krucjata-chevaleresse        | nieco zaniepokojona, ale sformowała dla Pięknotki anty-TAI klasy Malictrix do zniszczenia kompleksu Itaran. Poszło jej za dobrze. | 0110-07-24 - 0110-07-27 |
| 200425-inflitrator-poluje-na-tai-minerwy | dorabia sobie usprawniając TAI. Jako, że część się psuje w groźny sposób, aresztował ją Tukan. Ale naprawdę to był sabotaż... kogoś z Aurum? | 0110-08-24 - 0110-08-26 |
| 200502-po-co-atakuja-minerwe        | analizując anty-TAI z Infiltratora doszła do tego, że to kupione anty-TAI Mirtana; to nie chodziło o atak na nią. | 0110-08-26 - 0110-08-28 |
| 200623-adaptacja-azalii             | z Pięknotką na tajnej pustogorskiej operacji ratowania Orbitera/NeoMil przed Trzęsawiskiem; miała też wykryć co jest dziwnego z TAI Azalia. Używając energii ixiońskiej przejęła kontrolę nad nanitkami Azalii by wszystkich wyprowadzić. | 0110-09-26 - 0110-10-04 |
| 201011-narodziny-paladynki-saitaera | zinfiltrowała TAI Hestia w parku rozrywki Janor po dywersji zrobionej przez Pięknotkę. Przekonała Pięknotkę, że życie po Saitaerze to nie koniec; da się naprawić. | 0110-10-12 - 0110-10-13 |
| 201025-kraloth-w-parku-janor        | pomaga Lucjuszowi w rekonstrukcji Pięknotki, potem zastawia z Pięknotką pułapkę na kralotycznie zniewolonego terminusa i robi ixiońską efemerydę co krzyczy. | 0110-10-29 - 0110-10-31 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181024-decyzja-minerwy              | trójpersona: Minerwa, Nutka (nieco starsza) i terrorform. Erwin będzie miał niezłe dyskusje. | 0109-10-04
| 181024-decyzja-minerwy              | musi doprowadzić do tego, by Lilia i Erwin zostali parą. Potem może zniszczyć swoją psychotronikę. | 0109-10-04
| 181024-decyzja-minerwy              | Minerwa NIE zniszczy psychotroniki bez możliwości wpływu ze strony Kić (thread claim) | 0109-10-04
| 181027-terminuska-czy-kosmetyczka   | by powstrzymać terrorforma, złożyła psychotronikę by móc się odciąć i wyłączyć servar w sytuacji awaryjnej. | 0109-10-11
| 181230-uwiezienie-saitaera          | ma nowe ciało, stworzone przez Saitaera. Nie powinno działać, ale działa - świetnie. Jej ciało i umysł są naprawiane w Pustogorze. | 0109-12-09
| 190120-nowa-minerwa-w-nowym-swiecie | ma nowe ciało, maga. Nie jest już piękną Diakonką, ale ma ciało wspomagane przez Saitaera. Niezbyt lubiana w okolicy Pustogoru. | 0110-01-26
| 190120-nowa-minerwa-w-nowym-swiecie | utraciła Erwina. Zwana defilerką. Odepchnięta przez Pustogor. Nie kontroluje magii dobrze. Ogólnie, bardzo zdeptana psychicznie i w złej formie. | 0110-01-26
| 190127-ixionski-transorganik        | jej morale sięgnęło dna. Nie ufa swojej magii i swoim umiejętnościom już w ogóle. Nie czuje się częścią tego świata. | 0110-01-30
| 190210-minerwa-i-kwiaty-nadziei     | jeszcze większa złość na terminusów pustogorskich. Odepchnęli Kornela, któremu na niej zależało. Chcą ograniczyć energię ixiońską. | 0110-02-23
| 190210-minerwa-i-kwiaty-nadziei     | nie zamierza nigdzie wyjeżdżać; zostaje w Szczelińcu, w okolicach Zaczęstwa. | 0110-02-23
| 190210-minerwa-i-kwiaty-nadziei     | ma zamiar pomagać Kornelowi, jeśli jest tylko na to okazja i sposobność. | 0110-02-23
| 190424-budowa-ixionskiego-mimika    | dyskretny medal za zasługi dla Pustogoru od Karli, za ixiońską infuzję mimika i utrzymanie porządku | 0110-04-05
| 190424-budowa-ixionskiego-mimika    | dwa tygodnie w szpitalu | 0110-04-05
| 190502-pierwszy-emulator-orbitera   | wyszło na jaw Pięknotce, że to ona stała za psychotroniką pierwszych Emulatorów; naprawdę jest genialnym naukowcem | 0110-04-16
| 190503-bardzo-nieudane-porwania     | chroniona przed Orbiterem; przeniesiona do Miasteczka w Pustogorze i dano jej autonomię do czarowania i eksperymentów | 0110-04-20
| 201025-kraloth-w-parku-janor        | przejmuje Erwina Galiliena; ona nadal go kocha a Pięknotka jej go "oddała". | 0110-10-31

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 25 | ((181024-decyzja-minerwy; 181027-terminuska-czy-kosmetyczka; 181230-uwiezienie-saitaera; 190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei; 190402-eksperymentalny-power-suit; 190424-budowa-ixionskiego-mimika; 190427-zrzut-w-pacyfice; 190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 190503-bardzo-nieudane-porwania; 190828-migswiatlo-psychotroniczek; 190901-polowanie-na-pieknotke; 191103-kontrpolowanie-pieknotki-pulapka; 191112-korupcja-z-arystokratki; 191201-ukradziony-entropik; 200202-krucjata-chevaleresse; 200425-inflitrator-poluje-na-tai-minerwy; 200502-po-co-atakuja-minerwe; 200623-adaptacja-azalii; 201011-narodziny-paladynki-saitaera; 201025-kraloth-w-parku-janor)) |
| Erwin Galilien       | 8 | ((190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 190210-minerwa-i-kwiaty-nadziei; 190402-eksperymentalny-power-suit; 190424-budowa-ixionskiego-mimika; 190427-zrzut-w-pacyfice; 190429-sabotaz-szeptow-elizy; 200425-inflitrator-poluje-na-tai-minerwy)) |
| Karla Mrozik         | 6 | ((181230-uwiezienie-saitaera; 190120-nowa-minerwa-w-nowym-swiecie; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy; 190503-bardzo-nieudane-porwania; 200623-adaptacja-azalii)) |
| Lucjusz Blakenbauer  | 5 | ((181230-uwiezienie-saitaera; 190429-sabotaz-szeptow-elizy; 190901-polowanie-na-pieknotke; 200311-wygrany-kontrakt; 201025-kraloth-w-parku-janor)) |
| Mariusz Trzewń       | 5 | ((190901-polowanie-na-pieknotke; 191103-kontrpolowanie-pieknotki-pulapka; 191112-korupcja-z-arystokratki; 191201-ukradziony-entropik; 200202-krucjata-chevaleresse)) |
| Tymon Grubosz        | 5 | ((190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy; 190828-migswiatlo-psychotroniczek)) |
| Olaf Zuchwały        | 4 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Adela Kirys          | 3 | ((181024-decyzja-minerwy; 181027-terminuska-czy-kosmetyczka; 190202-czarodziejka-z-woli-saitaera)) |
| Alan Bartozol        | 3 | ((190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 190901-polowanie-na-pieknotke)) |
| Ataienne             | 3 | ((190402-eksperymentalny-power-suit; 191112-korupcja-z-arystokratki; 200202-krucjata-chevaleresse)) |
| Damian Orion         | 3 | ((190503-bardzo-nieudane-porwania; 191103-kontrpolowanie-pieknotki-pulapka; 200202-krucjata-chevaleresse)) |
| Diana Tevalier       | 3 | ((190429-sabotaz-szeptow-elizy; 190901-polowanie-na-pieknotke; 200202-krucjata-chevaleresse)) |
| Karolina Erenit      | 3 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy)) |
| Kasjopea Maus        | 3 | ((190120-nowa-minerwa-w-nowym-swiecie; 190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei)) |
| Nikola Kirys         | 3 | ((190427-zrzut-w-pacyfice; 190502-pierwszy-emulator-orbitera; 190503-bardzo-nieudane-porwania)) |
| Saitaer              | 3 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 201011-narodziny-paladynki-saitaera)) |
| Aleksander Rugczuk   | 2 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Arnulf Poważny       | 2 | ((181027-terminuska-czy-kosmetyczka; 190206-nie-da-sie-odrzucic-mocy)) |
| Ernest Kajrat        | 2 | ((190828-migswiatlo-psychotroniczek; 200311-wygrany-kontrakt)) |
| Gabriel Ursus        | 2 | ((200425-inflitrator-poluje-na-tai-minerwy; 201011-narodziny-paladynki-saitaera)) |
| Ignacy Myrczek       | 2 | ((181027-terminuska-czy-kosmetyczka; 191112-korupcja-z-arystokratki)) |
| Józef Małmałaz       | 2 | ((190901-polowanie-na-pieknotke; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Keraina d'Orion      | 2 | ((191201-ukradziony-entropik; 200202-krucjata-chevaleresse)) |
| Kornel Garn          | 2 | ((190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei)) |
| Kreacjusz Diakon     | 2 | ((181230-uwiezienie-saitaera; 200502-po-co-atakuja-minerwe)) |
| Lilia Ursus          | 2 | ((181024-decyzja-minerwy; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Marek Puszczok       | 2 | ((190828-migswiatlo-psychotroniczek; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Persefona d'Jastrząbiec | 2 | ((191112-korupcja-z-arystokratki; 191201-ukradziony-entropik)) |
| Sabina Kazitan       | 2 | ((191112-korupcja-z-arystokratki; 201011-narodziny-paladynki-saitaera)) |
| Talia Aegis          | 2 | ((190828-migswiatlo-psychotroniczek; 200202-krucjata-chevaleresse)) |
| Tomasz Tukan         | 2 | ((200202-krucjata-chevaleresse; 200425-inflitrator-poluje-na-tai-minerwy)) |
| Wiktor Satarail      | 2 | ((190127-ixionski-transorganik; 200623-adaptacja-azalii)) |
| Wojtek Kurczynos     | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Adam Szarjan         | 1 | ((181024-decyzja-minerwy)) |
| Agaton Ociegor       | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Aleksander Muniakiewicz | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Amanda Kajrat        | 1 | ((200311-wygrany-kontrakt)) |
| Artur Michasiewicz   | 1 | ((190828-migswiatlo-psychotroniczek)) |
| Atena Sowińska       | 1 | ((190210-minerwa-i-kwiaty-nadziei)) |
| Azalia d'Alkaris     | 1 | ((200623-adaptacja-azalii)) |
| Bożymir Szczupak     | 1 | ((190502-pierwszy-emulator-orbitera)) |
| Brygida Maczkowik    | 1 | ((181027-terminuska-czy-kosmetyczka)) |
| Dagmara Doberman     | 1 | ((200311-wygrany-kontrakt)) |
| Eliza Ira            | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Feliks Mirtan        | 1 | ((200502-po-co-atakuja-minerwe)) |
| Halina Sermniek      | 1 | ((200502-po-co-atakuja-minerwe)) |
| Hestia d'Itaran      | 1 | ((191201-ukradziony-entropik)) |
| Hestia d'Janor       | 1 | ((201025-kraloth-w-parku-janor)) |
| Kaja Selerek         | 1 | ((190402-eksperymentalny-power-suit)) |
| Kirył Najłalmin      | 1 | ((190127-ixionski-transorganik)) |
| Konrad Wączak        | 1 | ((200623-adaptacja-azalii)) |
| Ksenia Kirallen      | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Laura Tesinik        | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Liliana Bankierz     | 1 | ((200311-wygrany-kontrakt)) |
| Maciej Oczorniak     | 1 | ((201025-kraloth-w-parku-janor)) |
| Malictrix d'Itaran   | 1 | ((200202-krucjata-chevaleresse)) |
| Marlena Maja Leszczyńska | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Mateusz Kardamacz    | 1 | ((200202-krucjata-chevaleresse)) |
| Mirela Orion         | 1 | ((190503-bardzo-nieudane-porwania)) |
| Napoleon Bankierz    | 1 | ((190127-ixionski-transorganik)) |
| Nikodem Larwent      | 1 | ((200502-po-co-atakuja-minerwe)) |
| Rafał Roszczeniok    | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Roland Grzymość      | 1 | ((191201-ukradziony-entropik)) |
| Sławomir Muczarek    | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
| Strażniczka Alair    | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Szymon Oporcznik     | 1 | ((190402-eksperymentalny-power-suit)) |
| Tadeusz Sklerzec     | 1 | ((191112-korupcja-z-arystokratki)) |
| Ziemowit Zięba       | 1 | ((200311-wygrany-kontrakt)) |