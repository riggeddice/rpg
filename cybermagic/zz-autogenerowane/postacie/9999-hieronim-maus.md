---
categories: profile
factions: 
owner: public
title: Hieronim Maus
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190116-wypalenie-saitaera-z-trzesawiska | tłuściutki kapłan Karradraela, najlepszy nośnik na Astorii. Bardzo nieszczęśliwy, bo musiał być odeskortowany do ołtarza Saitaera na Trzęsawisku. | 0110-01-07 - 0110-01-09 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Bartozol        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| ASD Centurion        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Karradrael           | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Kasjopea Maus        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Pięknotka Diakon     | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Saitaer              | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Wiktor Satarail      | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |