---
categories: profile
factions: 
owner: public
title: Wacław Samszar
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (E+C-): ekscentryczny i arogancki, lubiący być w centrum uwagi i impulsywny. "Bawmy się i korzystajmy z życia, konsekwencje naprawi sentisieć. Nie po to jestem tienem by się pieprzyć." | @ 230715-amanda-konsoliduje-zloty-cien
* VALS: (Power, Hedonism): ceni sobie status i przyjemności wszelakie. "Życie to zabawa! Mam prawo bawić się i cieszyć się jak chcę!" | @ 230715-amanda-konsoliduje-zloty-cien
* Core Wound - Lie: "Zawsze byłem traktowany jak nieudacznik z powodu braku bystrości" - "Skoro tak, wezmę co mogę używając pozycji i kasy ORAZ pomogę jak mogę tym, co są warci i mnie lubią." | @ 230715-amanda-konsoliduje-zloty-cien
* styl: lekko niepewny siebie, ale arogancki mag żądający pokory i docenienia. Ale jeśli się zdobędzie jego przyjaźń, zostanie lojalny. | @ 230715-amanda-konsoliduje-zloty-cien

### Wątki


pronoktianska-mafia-kajrata

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230627-ratuj-mlodziez-dla-kajrata   | młody (24) mag hiperpsychotroników, pilnował Warguna i Mitrii by ekstraktować wiedzę z Talii. Karolinus go skusił Itrią i Wacław skupił się na Itrii a nie na zadaniu. Wacław DA SIĘ WYCIĄGNĄĆ Itrii z hiperpsychotroników. | 0095-08-20 - 0095-08-25 |
| 230711-zablokowana-sentisiec-w-krainie-makaronu | Karolinus wkręcił go w frontowanie Złotego Cienia dla Amandy. Wacław, po amnestykach ("niekompetentny dla Hiperpsychotroników") uznał, że TYLKO Karolinusowi na nim zależy i tylko Karolinus jest jego prawdziwym przyjacielem. | 0095-09-05 - 0095-09-08 |
| 230715-amanda-konsoliduje-zloty-cien | pies na dziewczyny; upokorzył Amandę (bo nie lubi noktianek), ale będzie z nią współpracował. Jest udanym frontem na Złoty Cień - jak długo ma ładne dziewczyny. Else, Amanda będzie pełnić tą rolę. "Przejął" Amandę od Karolinusa jako swoją maskotkę / agentkę. | 0095-09-09 - 0095-09-12 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230627-ratuj-mlodziez-dla-kajrata   | absolutnie zafascynowany Itrią, do poziomu 'dam jej się wyciągnąć z tzw. Mrocznej Sekty'. | 0095-08-25
| 230711-zablokowana-sentisiec-w-krainie-makaronu | jest LOJALNY Karolinusowi, bo Karolinus wielokrotnie udowodnił, że dba o niego. I zapewnia Wacławowi dziewczyny przez Złoty Cień. | 0095-09-08
| 230711-zablokowana-sentisiec-w-krainie-makaronu | ma gwarantowane (przez Karolinusa) dziewczyny ze Złotego Cienia i dlatego frontuje Złoty Cień i swoje interesy w tym obszarze. | 0095-09-08
| 230715-amanda-konsoliduje-zloty-cien | nagle stał się odpowiedzialny za powodzenie Złotego Cienia wraz z Karolinusem Samszarem. Mają szansę do pierwszej poważnej wtopy Złotego Cienia. | 0095-09-12

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 2 | ((230627-ratuj-mlodziez-dla-kajrata; 230715-amanda-konsoliduje-zloty-cien)) |
| Elena Samszar        | 2 | ((230627-ratuj-mlodziez-dla-kajrata; 230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Karolinus Samszar    | 2 | ((230627-ratuj-mlodziez-dla-kajrata; 230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| AJA Szybka Strzała   | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Bella Samszar        | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Emil Samszar         | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Ernest Kajrat        | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Herbert Samszar      | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Ignatus Blakenbauer  | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Impresja Ignicja Incydencja Diakon | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Irek Kraczownik      | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Kasimir Esilin       | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Mitria Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Petra Samszar        | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Rufus Bilgemener     | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Tadeusz Samszar      | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Talia Aegis          | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Wargun Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Wiktor Blakenbauer   | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |