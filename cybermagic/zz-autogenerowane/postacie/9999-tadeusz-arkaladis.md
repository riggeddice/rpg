---
categories: profile
factions: 
owner: public
title: Tadeusz Arkaladis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230521-rozszczepiona-persefona-na-itorwienie | KIA; skompromitowany (źle traktował kobiety na uczelni) acz kompetentny doktor zaawansowanych technologii i psychotroniki. Dorabiał sobie przemytem i zablokował Persefonę d'Itorwien. Zginął, gdy rozszczepiona Persi zidentyfikowała go jako zagrożenie. | 0109-09-15 - 0109-09-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Emilia Ibris         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Fabian Korneliusz    | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Helmut Szczypacz     | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Iskander Matorin     | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Karol Brinik         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Klaudia Stryk        | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Martyn Hiwasser      | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Mojra Karstall       | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| OO Itorwien          | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| OO Serbinius         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |