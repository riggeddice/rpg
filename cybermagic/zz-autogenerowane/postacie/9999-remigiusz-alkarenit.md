---
categories: profile
factions: 
owner: public
title: Remigiusz Alkarenit
---

# {{ page.title }}


# Generated: 



## Fiszki


* oficer taktyczny i komunikacji i stary weteran Athamarein jeszcze z czasów Lodowca; noktianin; wtedy był marine | @ 231018-anomalne-awarie-athamarein
* OCEAN: C+A- | Cichy, wypowiada się gdy musi;; Brawurowy i szybko podejmuje decyzje;; z boku załogi | VALS: Hedonism, Security | Drive: Przysięga (doprowadzić Orbiter do wielkości) | @ 231018-anomalne-awarie-athamarein

### Wątki


legenda-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231018-anomalne-awarie-athamarein   | chciałby być na akcjach, chciałby być PRZYDATNY. Nie chce szkolić i mentorować, a do tego kom.Oroginiec go przeznaczył. Chciałby na inną jednostkę, ma dość "niekompetentnych tienowatych". Mało mówi. Za poleceniem Arianny, spróbuje pomóc tym tienom. | 0111-12-22 - 0111-12-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alicja Szadawir      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Andrzej Gwozdnik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Arianna Verlen       | 1 | ((231018-anomalne-awarie-athamarein)) |
| Błażej Sowiński      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Elena Verlen         | 1 | ((231018-anomalne-awarie-athamarein)) |
| Eustachy Korkoran    | 1 | ((231018-anomalne-awarie-athamarein)) |
| Hiacynt Samszar      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Jakub Oroginiec      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Kajetan Kircznik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Klaudia Stryk        | 1 | ((231018-anomalne-awarie-athamarein)) |
| Leona Astrienko      | 1 | ((231018-anomalne-awarie-athamarein)) |
| OO Athamarein        | 1 | ((231018-anomalne-awarie-athamarein)) |
| Uśmiechniczka Konstrukcjonistka Aurora Diakon | 1 | ((231018-anomalne-awarie-athamarein)) |