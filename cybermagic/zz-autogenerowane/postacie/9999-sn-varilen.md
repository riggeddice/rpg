---
categories: profile
factions: 
owner: public
title: SN Varilen
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240111-zlomowanie-legendarnej-anitikaplan | statek salvagerski, z laserem dekombinacyjnym i sporą załogą. Podjął się zezłomowania Anitikaplan; na jego pokładzie jest kilka firm mających za zadanie maksymalizować zasoby i pieniądz. | 0084-04-02 - 0084-04-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klasa Biurokrata     | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Klasa Hacker         | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Klasa Oficer Naukowy | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Klasa Sabotażysta    | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| SN Anitikaplan       | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Twaróg Worl          | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Wiktor Worl          | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |