---
categories: profile
factions: 
owner: public
title: Dorota Radraszew
---

# {{ page.title }}


# Generated: 



## Fiszki


* oficer zaopatrzenia Inferni, bardzo przedsiębiorcza i zawsze ma pomysł jak ominąć problem nie konfrontując się z nim bezpośrednio. Cichy żarliwy optymizm. ŚWIETNA W PRZEMYCIE. | @ 231025-spiew-nielalki-na-castigatorze
* OCEAN: (E+O+): Mimo precyzji się gubi w zmieniającym się świecie, ale ma wewnętrzny optymizm. "Dorota może nie wiedzieć, co się dzieje, ale zawsze wierzy, że jutro będzie lepsze." | @ 231025-spiew-nielalki-na-castigatorze
* VALS: (Universalism, Humility): Często medytuje, szuka spokoju wewnętrznego i go znajduje. Wyznaje Lichsewrona (UCIECZKA + POSZUKIWANIE). "Wszyscy znajdziemy sposób, by uniknąć przeznaczenia. Dopasujemy się." | @ 231025-spiew-nielalki-na-castigatorze
* Core Wound - Lie: "Jej arkologia stanęła naprzeciw Anomaliom i została Zniekształcona. Ona uciekła przed własną Skażoną rodziną" - "Nie da się wygrać z Anomaliami, ale można im uciec, jak On mi pomógł." | @ 231025-spiew-nielalki-na-castigatorze
* Styl: Spokojna, w eleganckim mundurze, promieniuje optymizmem 'że z każdej sytuacji da się wyjść'. Unika konfrontacji. Uśmiech nie znika z jej twarzy mimo, że rzeczy nie działają jak powinny. Magów uważa za 'gorszych'. | @ 231025-spiew-nielalki-na-castigatorze
* metakultura: faerilka; niezwykle pracowita, bez kwestionowania wykonuje polecenia, "zawsze jest możliwość zwycięstwa nawet jak się pojawi w ostatniej chwili" | @ 231025-spiew-nielalki-na-castigatorze

### Wątki


triumfalny-powrot-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231115-cwiczenia-komodora-bladawira | lekko zrezygnowana (wie że pracuje dla magów i nie da się nic zrobić z Infernią) oficer zapoatrzeniowa Inferni; gdy miała poznać plan Razalis, odwiedziła Karsztarin i co prawda została ciężko pobita ale doszła do tego, że ktoś 'leakował' plan Bladawira co dało Klaudii i Ariannie kluczowe fakty. | 0110-11-08 - 0110-11-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Bladawir      | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Arianna Verlen       | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Ewa Razalis          | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Izabela Zarantel     | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Klaudia Stryk        | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Leszek Kurzmin       | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| OO Karsztarin        | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| OO Paprykowiec       | 1 | ((231115-cwiczenia-komodora-bladawira)) |