---
categories: profile
factions: 
owner: public
title: Antoni Zajcew
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180708-niewidzialne-potwory-z-rzeki | łowca potwórów; chciał odzyskać obraz dla Majki współpracując ze Stachem i stary oszust na nim też zarobił. Poczciwy i myśli że jest rekinem biznesu. | 0109-08-18 - 0109-08-20 |
| 180730-prasyrena-z-zemsty           | łowca potworów sprowadzony do roli kuriera. Szczęśliwie, pomoże niedługo Kalinie i tak pokaże jaki jest świetny. | 0109-08-29 - 0109-08-30 |
| 180808-kultystka-z-milosci          | przybył, rozwalił sojusznika (Stacha), opuścił teren a potem przybył na ostatnią bitwę pobić kilku ludzi. I jemu przypisano zasługi. Epic. | 0109-08-30 - 0109-09-02 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180708-niewidzialne-potwory-z-rzeki | zdobył pozycję wśród osób słuchających opowieści za opowieść o kupie i śledziach. Ale opowieść o Stachu zbił jego pozycję w dół tam gdzie się liczy. | 0109-08-20
| 180808-kultystka-z-milosci          | Zajcewscy bardowie się postarali - większość epickich sukcesów z walki z Kręgiem Ośmiornic przypisano właśnie jemu. | 0109-09-02

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kalina Rotmistrz     | 3 | ((180708-niewidzialne-potwory-z-rzeki; 180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Stach Sosnowiecki    | 3 | ((180708-niewidzialne-potwory-z-rzeki; 180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Kasandra Kirnał      | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Maksymilian Supolont | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Małgorzata Kirnał    | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Anita Perczoluk      | 1 | ((180730-prasyrena-z-zemsty)) |
| Feliks Weiner        | 1 | ((180808-kultystka-z-milosci)) |
| Joachim Kozioro      | 1 | ((180708-niewidzialne-potwory-z-rzeki)) |
| Kacper Pyszałnik     | 1 | ((180808-kultystka-z-milosci)) |
| Lawenda Weiner       | 1 | ((180730-prasyrena-z-zemsty)) |
| Magda Patiril        | 1 | ((180708-niewidzialne-potwory-z-rzeki)) |
| Patryk Paterecki     | 1 | ((180808-kultystka-z-milosci)) |