---
categories: profile
factions: 
owner: public
title: Joachim Puriur
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220817-osy-w-ces-purdont            | próbuje ukryć swoje ślady; sabotażysta od środka który wpuścił komandosów z Karglondel i wprowadził plagę Trianai do CES. Zginął z komandosami zjedzony przez Trianai. KIA. | 0093-01-23 - 0093-01-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((220817-osy-w-ces-purdont)) |
| Celina Lertys        | 1 | ((220817-osy-w-ces-purdont)) |
| Eustachy Korkoran    | 1 | ((220817-osy-w-ces-purdont)) |
| Jan Lertys           | 1 | ((220817-osy-w-ces-purdont)) |
| Kamil Wraczok        | 1 | ((220817-osy-w-ces-purdont)) |
| Kordian Olgator      | 1 | ((220817-osy-w-ces-purdont)) |
| VN Karglondel        | 1 | ((220817-osy-w-ces-purdont)) |