---
categories: profile
factions: 
owner: public
title: Ilfons Lawellan
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230425-klotnie-sasiadow-w-wanczarku | narzeczony Elei Brzozeckiej; zjadł jej szarlotkę (z sokiem ze Skażonej Jabłoni) i Elea prawie go utopiła - gdyby nie Strzała. Skończył jako roślinoczłowiek z ratunku Karolinusa. | 0095-05-16 - 0095-05-19 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230425-klotnie-sasiadow-w-wanczarku | prawie utopiony przez Eleę Brzozecką, Karolinus uratował mu życie robiąc z niego hybrydową bioformę między człowiekiem i drzewcem. | 0095-05-19

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Artemis Lawellan     | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Artur Lawellan       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Damian Fenekis       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Elea Brzozecka       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Karolinus Samszar    | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Olga Fenekis         | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |