---
categories: profile
factions: 
owner: public
title: Izydor Grumczewicz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201201-impreza-w-malopsie           | Rekin, żonaty, nie potrafi utrzymać "końcówki" przy sobie. Zlecił Zespołowi by rozwiązali problem kochanki (Pauliny). | 0110-11-04 - 0110-11-06 |
| 201215-dziewczyna-i-pies            | chciał usunąć swojej dawnej miłości (Paulinie) pamięć o sobie i został złapany przez Patrycję i Lucka (paradoksalnie, chcących chronić Paulinę i Andrzeja). | 0110-11-15 - 0110-11-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Andrzej Kuncerzyk    | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Daniel Terienak      | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Franciszek Zygmunt   | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Karolina Terienak    | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Paulina Mordoch      | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Tadeusz Łaśnic       | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Barnaba Burgacz      | 1 | ((201201-impreza-w-malopsie)) |
| Cezary Urmaszcz      | 1 | ((201201-impreza-w-malopsie)) |
| Grzegorz Terienak    | 1 | ((201215-dziewczyna-i-pies)) |
| Patrycja Radniak     | 1 | ((201215-dziewczyna-i-pies)) |