---
categories: profile
factions: 
owner: public
title: Alfred Cerulean
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190813-niesmiertelny-komandos-saitaera | komandos noktiański; próbował dawno temu zniszczyć Saitaera i stał się nanitkowym regenerującym potworem. Jego krew wstrzyknięto Robertowi Arłaczowi i Alfred próbował zatrzymać transformację. Ciągle traci nad sobą kontrolę. Bardziej potwór niż mag noktiański którym był kiedyś. | 0110-05-20 - 0110-05-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Maria Gołąb          | 1 | ((190813-niesmiertelny-komandos-saitaera)) |
| Robert Arłacz        | 1 | ((190813-niesmiertelny-komandos-saitaera)) |
| Robert Garwen        | 1 | ((190813-niesmiertelny-komandos-saitaera)) |