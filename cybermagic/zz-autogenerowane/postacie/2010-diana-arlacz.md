---
categories: profile
factions: 
owner: public
title: Diana Arłacz
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "orbiter, arystokracja aurum, oddział verlen, ród arłacz"
* owner: "public"
* title: "Diana Arłacz"


## Kim jest

### W kilku zdaniach

Piromanka i czarodziejka entropii. Diana kocha płomienie, jest nimi zafascynowana. Ma unikalny talent do niszczenia i pociągają ją osoby mające podobne umiejętności i pasję. Przygotowywana jako Arystokratka Aurum, reaguje na sentisieć Arłacz, ale nie została wybrana przez Ród. Oddana Eustachemu na zabój.

### Co się rzuca w oczy

* Niebiesko-zielone włosy, niebiesko-czarny frak arystokratki, figlarny uśmiech i pasja w oczach.
* Bombastyczna i nieprawdopodobnie dramatyczna, zachowuje się jak herosi z filmów.
* Oddana Eustachemu do końca. Spełni każdą jego zachciankę.
* Sieje chaos, destrukcję i niezgodę. W jej otoczeniu wszystko się docelowo rozpada.
* Jest istotą entropii i zniszczenia, ale w pełni akceptuje tą naturę. Nie chce być niczym innym. To jest to czym jest i czemu jest.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Wyleciała z Kontrolera Pierwszego po całkowitej destrukcji przyjęcia kadetów (łącznie z antymagiczną zastawą), gdy wypiła ZDECYDOWANIE za dużo.
* Nie do końca świadomie doprowadziła do niejednego pojedynku na Castigatorze między arystokratami. Kokietka XD.
* Gdy zarządzała sentisiecią rodu Arłacz, utrzymywała majątek w stanie aktywnym i sprawnym. Sprawnie zarządza i administruje.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* PRACA: Regentka Sentisieci Aurum - potrafi zarządzać i administrować majątkiem czy małą kolonią. Jeśli tylko starczy jej uwagi.
* PRACA: Puryfikatorka - specjalistka od Odkażania i usuwania anomalii. Dobra też w walce jako artyleria.
* SERCE: Płomienie i destrukcja to jej narkotyk. Nie potrafi żyć bez nich. Pragnie rozkładać, dekombinować i niszczyć.
* SERCE: Imprezowiczka i istota głęboko się przywiązująca. Opanowana niszczycielską miłością do Eustachego - i pasją do każdego destruktora.
* ATUT: Wulkan niekierowanej energii magicznej, żywa bateria energii.
* ATUT: Unikalny talent do dekombinacji materii, energii i życia. Istota entropii. Mało czego nie potrafi zniszczyć.

### Charakterystyczne zasoby (3)

* COŚ: Noktiańskie kryształy amplifikujące moc magiczną, ale też destabilizujące efekt (+V +OOO). Świetne jako broń.
* KTOŚ: Maria Gołąb, groźna wojowniczka lojalna rodowi Arłacz, ale bardzo lubiąca niesforną i niszczycielską Dianę.
* WIEM: Perfekcyjnie porusza się po wszystkich mechanizmach arystokratycznych w Aurum i Eterni.
* OPINIA: Kokietka i flirciara, która jak się upije to jest śmiertelnie niebezpieczna. A pije, oj, pije...

### Typowe problemy z którymi sobie nie radzi (-3)

* STRACH: FOMO. Boi się, że dzieją się dziesiątki fajnych i ważnych rzeczy - a wszystkie poza nią.
* SKILL: Nie potrafi pilotować żadnym pojazdem. Serio. Po prostu technologia nie jest z nią kompatybilna.
* SERCE: Łatwo ulega impulsom - ciągnie ją do groźnych mężczyzn i do destrukcji jak ćmę do ognia.
* SERCE: W głębi duszy wierzy, że zniszczenie i entropia to są siły które zawsze wygrywają. "Why run, what's the point".
* MAGIA: Jej moc nie pozwala jej na tworzenie - jedynie na destrukcję.

### Specjalne

* Na śmierć oddana Eustachemu, nie potrafi mu odmówić (Eustachy ma +6 do wszelkich testów wobec niej)

## Inne

### Wygląd

* Krótkie niebiesko-zielone włosy
* Upraktyczniony strój arystokratki, w niebiesko-czarnych kolorach, z emblematem Inferni

### Coś Więcej

* Gdyby filozofia Siriusa@PoE połączyła się z pasją Azonii@Robotech i mocą dekombinacji Scar@FMA, mamy szkielet Diany.

### Endgame

* Jej zdaniem? Wyjdzie za Eustachego.
* Zdaniem Eleny? Wróci do siebie, do Aurum.
* Zdaniem Leony? Wymaga porządnego przeszkolenia bojowego.
* Może trafi do jakiegoś programu treningowego magów entropicznych, nie wiem.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201021-noktianie-rodu-arlacz        | Była senti-następczynią, ale po 'korekcji' Roberta została przesunięta na drugi plan. Uciekła z Eustachym na pokład Inferni, odrzucając ród i ratując noktian. Piromanka. | 0111-03-07 - 0111-03-10 |
| 201104-sabotaz-swini                | żywa bateria energii magicznej, by Eustachy mógł uratować Ariannę i załogę anomalicznego Wesołego Wieprzka. | 0111-03-13 - 0111-03-16 |
| 201118-anastazja-bohaterka          | doprowadziła do pojedynku między Eustachym a Jamniczkiem, bo nie umiała trzymać języka za zębami i "Eustachy jest najlepszy i męski". | 0111-03-22 - 0111-03-25 |
| 201216-krypta-i-wyjec               | na pokładzie Krypty poddała się halucynacjom i obrazom eksplozji. Podatna na moc mentalną Krypty. | 0111-03-29 - 0111-03-31 |
| 201125-wyscig-jako-randka           | przyzwoitka Arianny na spotkaniu z Olgierdem. Monotematyczna: "Eustachy zrobiłby to lepiej niż Olgierd". | 0111-04-08 - 0111-04-13 |
| 201224-nieprawdopodobny-zbieg-okolicznosci | pokazała co potrafi potęgą entropicznej anihilacji, strącając Grzegorza z sufitu. Jednak nie nienawidzą się z Eleną. | 0111-04-15 - 0111-04-16 |
| 210127-porwanie-anastazji-z-odkupienia | wpadła na pomysł chorej anomalii by odsunąć pretorian. A potem posłużyła jako dywersja dla Eleny. Ogólnie - perfekcyjna Eris. | 0111-07-22 - 0111-07-23 |
| 210106-sos-z-haremu                 | pokazała swoją "ostrą", perwersyjną stronę w służbie zastawienia pułapki na kralotycznie zdominowanego arystokratę. "She's so wild noone can tame her". Zneutralizowała pocałunkiem Horacego. | 0111-07-26 - 0111-07-27 |
| 210120-sympozjum-zniszczenia        | by zaimponować Eustachemu za wszelką cenę weszła w głąb energii Esuriit; została przez Esuriit opętana i wyciągnął ją Eustachy. Ale wymusiła pocałunek. Poważnie skrzywdziła kogoś. | 0111-08-01 - 0111-08-05 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 201021-noktianie-rodu-arlacz        | wyrzucona z rodu Arłacz mimo umiejętności sprzęgania się z Sentisiecią za zdradę na rzecz Arianny Verlen. | 0111-03-10
| 201021-noktianie-rodu-arlacz        | zafascynowana i podkochująca się w Eustachym Korkoranie; przekonana, że on ma fetysz że INNI MUSZĄ PATRZEĆ. | 0111-03-10
| 210120-sympozjum-zniszczenia        | dwa miesiące kuracji po Esuriit. W trakcie - psycholog. Potem - kurator. Plus zniszczona reputacja; persona non grata na imprezach. Ogólnie, tragedia. Ale wymusiła całus Eustachego. Warto było? XD | 0111-08-05
| 210120-sympozjum-zniszczenia        | reputacja "świetni w niszczeniu i destrukcji, ale idźcie na bok i nie psujcie więcej". Takie trochę dzieci od zniszczenia. | 0111-08-05

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 9 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 201224-nieprawdopodobny-zbieg-okolicznosci; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| Arianna Verlen       | 8 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| Eustachy Korkoran    | 7 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| Martyn Hiwasser      | 7 | ((201104-sabotaz-swini; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| Anastazja Sowińska   | 4 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec)) |
| Elena Verlen         | 4 | ((201224-nieprawdopodobny-zbieg-okolicznosci; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| Ataienne             | 2 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Kamil Lyraczek       | 2 | ((201118-anastazja-bohaterka; 201216-krypta-i-wyjec)) |
| Maria Naavas         | 2 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia)) |
| Olgierd Drongon      | 2 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia)) |
| AK Nocna Krypta      | 1 | ((201216-krypta-i-wyjec)) |
| AK Wyjec             | 1 | ((201216-krypta-i-wyjec)) |
| Anastazja Sowińska Dwa | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Dariusz Krantak      | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Eliza Ira            | 1 | ((201021-noktianie-rodu-arlacz)) |
| Grzegorz Chropst     | 1 | ((201224-nieprawdopodobny-zbieg-okolicznosci)) |
| Gunnar Brunt         | 1 | ((201125-wyscig-jako-randka)) |
| Horacy Aktenir       | 1 | ((210106-sos-z-haremu)) |
| Izabela Zarantel     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Jasmina Perikas      | 1 | ((201224-nieprawdopodobny-zbieg-okolicznosci)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Julia Aktenir        | 1 | ((210106-sos-z-haremu)) |
| Juliusz Sowiński     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Leona Astrienko      | 1 | ((210106-sos-z-haremu)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| OA Bakałarz          | 1 | ((210120-sympozjum-zniszczenia)) |
| OA Odkupienie        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OO Infernia          | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Wesoły Wieprzek   | 1 | ((201021-noktianie-rodu-arlacz)) |
| Rafał Armadion       | 1 | ((201104-sabotaz-swini)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Robert Garwen        | 1 | ((201104-sabotaz-swini)) |
| Rozalia Teirik       | 1 | ((210106-sos-z-haremu)) |
| Rufus Niegnat        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| SP Plugawy Jaszczur  | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Stefan Jamniczek     | 1 | ((201118-anastazja-bohaterka)) |
| Wanessa Pyszcz       | 1 | ((201021-noktianie-rodu-arlacz)) |