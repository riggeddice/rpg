---
categories: profile
factions: 
owner: public
title: Maciej Brzeszczak
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "orbiter, oddział litaar"
* owner: "public"
* title: "Maciej Brzeszczak"


## Kim jest

### Paradoksalny Koncept

Doskonały mechanik, zakochany w swoim statku kosmicznym i w precyzyji dokumentów. Uwielbia pracować z dokumentami, nie z ludźmi czy sztucznymi inteligencjami. Tak bardzo mu zależy na perfekcji statku i załogi, że odrzuca od siebie członków tej załogi. Nie lubi sztucznych inteligencji, ale pracuje nad własną - która ma pełnić rolę inteligentnego narzędzia. Wierzy w pangalaktyczną cywilizację ludzką, ale próbuje uniknąć nadmiaru magii, anomalii i sztucznych inteligencji które są takiej cywilizacji potrzebne. Niechętny ludziom i gadaniu, został nieformalnym przywódcą frakcji "czystej ludzkości" na statku kosmicznym.

### Motto

"Ludzie, sztuczne inteligencje... dajcie mi spokój, niech robią co chcą - ja sprawię, by nasz statek był najlepszy w całej flocie. Dla ludzkości."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Zna statek jak własną kieszeń, wie o nim praktycznie wszystko, zna każde najdrobniejsze przejście i każdy podsystem.
* ATUT: W tematach biurokracji, prawa i dokumentów czuje się jak ryba w wodzie.
* SŁABA: Woli rozwiązywać problemy pracą z dokumentami i sprawdzeniem osobistym niż gadaniem z ludźmi czy sztucznymi inteligencjami.
* SŁABA: Lekceważy sztuczne inteligencje, nie ufa im zupełnie - musi wszystko sprawdzić samemu.

### O co walczy (3)

* ZA: Mój statek ma być najlepszym pojazdem we flocie. Moja załoga też. Każde niewłaściwe zachowanie statku to osobista obraza.
* ZA: Ludzkość kiedyś podbiła kosmos. Potem rzeczywistość pękła. Niech znowu ludzkość odzyska swoje miejsce wśród gwiazd - cywilizacja galaktyczna.
* VS: Sztuczne inteligencje mają być tylko narzędziem wspomagającym. Mają siedzieć cicho i najwyżej coś nieśmiało sugerować. Żadnej autonomii.
* VS: "Normalni" ludzie i magowie to jest to co ważne. Te wszystkie AI, nadmiar magii, anomalie - to grozi cywilizacji ludzkiej.

### Znaczące Czyny (3)

* Na jednym z poprzednich statków kosmicznych wykrył, że TAI została uszkodzona i dał radę ją wyłączyć i przejść na kontrolę awaryjną. Nikt inny w to nie wierzył.
* Przesterował pola siłowe aktualnego statku do tego stopnia, że statek był w stanie przetrwać burzę magiczną w kosmosie przy niewielkich stratach (acz potem reaktor umarł).
* Podczas odbudowy uszkodzonego statku w porcie był w stanie wykryć w dokumentach, że stocznia oszczędza na częściach. Był tak uciążliwy z dokumentami, że dostał lepsze części od stoczni.

### Kluczowe Zasoby (3)

* COŚ: Niezłej klasy wspomagająca sztuczna inteligencja o imieniu "TAI-447", która zbiera, agreguje i pomaga w pracy.
* KTOŚ: Nieformalny lider grupy ludzi i magów którzy uważają, że świat idzie troszkę w złą stronę - za dużo magii i AI, za mało ludzkiego podejścia.
* WIEM: O każdej nielegalnej kontrabandzie i wszystkich grzeszkach członków załogi. Jak naprawdę trzeba, mogę zdobyć sprzęt i wymusić zachowanie.
* OPINIA: Perfekcjonistyczny typ; nie wszystkie procedury są bardzo ważne a on tego wymaga. Kocha maszyny, unika ludzi.

## Inne

### Wygląd

.

### Coś Więcej

.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200422-pionier-w-anomalii-kolapsu   | oficer inżynier Miecza Światła; skutecznie wprowadził odpromienniki thaumiczne na Luminusie, acz zainfekował się echem Pioniera; skończył w izolatce. | 0110-02-02 - 0110-02-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aneta Szermen        | 1 | ((200422-pionier-w-anomalii-kolapsu)) |
| Ildefons Szombar     | 1 | ((200422-pionier-w-anomalii-kolapsu)) |
| Mateusz Sowiński     | 1 | ((200422-pionier-w-anomalii-kolapsu)) |
| Tobiasz Agronom      | 1 | ((200422-pionier-w-anomalii-kolapsu)) |