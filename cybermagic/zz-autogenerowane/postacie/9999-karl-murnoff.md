---
categories: profile
factions: 
owner: public
title: Karl Murnoff
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240131-anomalna-mavidiz             | dumny eks-Aurelionowiec, pragnął wielkich czynów. Po tym jak Mavidiz został opanowany przez nihilosekta, wpadł w jego ręce. Teraz - przechwycony przez Bladawira. | 0111-01-03 - 0111-01-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Bladawir      | 1 | ((240131-anomalna-mavidiz)) |
| Arianna Verlen       | 1 | ((240131-anomalna-mavidiz)) |
| Borys Kragin         | 1 | ((240131-anomalna-mavidiz)) |
| Eustachy Korkoran    | 1 | ((240131-anomalna-mavidiz)) |
| Grigor Tarnow        | 1 | ((240131-anomalna-mavidiz)) |
| Igor Stratos         | 1 | ((240131-anomalna-mavidiz)) |
| Klaudia Stryk        | 1 | ((240131-anomalna-mavidiz)) |
| Markus Wąż           | 1 | ((240131-anomalna-mavidiz)) |
| ONS Mavidiz          | 1 | ((240131-anomalna-mavidiz)) |
| OO Tivr              | 1 | ((240131-anomalna-mavidiz)) |
| Raoul Lavanis        | 1 | ((240131-anomalna-mavidiz)) |
| Rita Stratos         | 1 | ((240131-anomalna-mavidiz)) |
| Tara Ogniczek        | 1 | ((240131-anomalna-mavidiz)) |