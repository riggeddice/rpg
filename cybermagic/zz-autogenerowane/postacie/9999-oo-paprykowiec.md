---
categories: profile
factions: 
owner: public
title: OO Paprykowiec
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231109-komodor-bladawir-i-korona-woltaren | patrolowiec ratunkowy Castigatora. Jednostka szybka i dalekosiężna. Współinwestowana przez ród Samszar (stąd nazwa). | 0110-10-27 - 0110-11-03 |
| 231115-cwiczenia-komodora-bladawira | znów okręt flagowy Arianny, spełnił się w tej roli doskonale, nie robiąc nic wielkiego XD | 0110-11-08 - 0110-11-15 |
| 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira | na ćwiczeniach jednostka dowodzona przez tienów i eskortowana przez Infernię. Jej TAI pokazała Klaudii potencjalny problem z _pain amplifiers_, Elena je sabotowała i Paprykowiec zrobił co powinien - dał się "zniszczyć" na ćwiczeniach. | 0110-11-26 - 0110-11-30 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Bladawir      | 3 | ((231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Arianna Verlen       | 3 | ((231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Klaudia Stryk        | 3 | ((231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Elena Verlen         | 2 | ((231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Leszek Kurzmin       | 2 | ((231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira)) |
| Marta Keksik         | 2 | ((231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Patryk Samszar       | 2 | ((231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Aneta Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Dorota Radraszew     | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Ewa Razalis          | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Feliks Walrond       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Izabela Zarantel     | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Kamil Lyraczek       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Krzysztof Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Lars Kidironus       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Lidia Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| OO Infernia          | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| OO Karsztarin        | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| SC Korona Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Tomasz Rewernik      | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Wojciech Mykirło     | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Zaara Mieralit       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |