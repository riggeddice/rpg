---
categories: profile
factions: 
owner: public
title: Staś Kruszawiecki
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180821-programista-mimo-woli        | syn Tadeusza, który zawsze marzył o rysowaniu ale ojciec kazał mu być programistą. Sprzęgł się z Efemerydą, która została zniszczona. | 0109-09-06 - 0109-09-07 |
| 180912-inkarnata-nienawisci         | syn dyrektora Cyberszkoły, któremu Inkarnata zastąpił miłość do rodziców czystą nienawiścią. Wyleczony przez Pięknotkę i Atenę. | 0109-09-14 - 0109-09-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Atena Sowińska       | 2 | ((180821-programista-mimo-woli; 180912-inkarnata-nienawisci)) |
| Pięknotka Diakon     | 2 | ((180821-programista-mimo-woli; 180912-inkarnata-nienawisci)) |
| Bronisława Strzelczyk | 1 | ((180821-programista-mimo-woli)) |
| Brygida Maczkowik    | 1 | ((180912-inkarnata-nienawisci)) |
| Tadeusz Kruszawiecki | 1 | ((180821-programista-mimo-woli)) |