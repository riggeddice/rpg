---
categories: profile
factions: 
owner: public
title: Alaric Rakkeir
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230806-zwiad-w-iliminar-caos        | nie czuje się komfortowo dowodząc zespołem gdzie jest kobieta (Amanda), ale dobrze prowadzi operację. Trzyma morale i nie chce zabijać salvagerów. Słucha pomysłów Amandy. Wierzy w Kajrata i Garzina. | 0081-06-22 - 0081-06-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Caelia Calaris       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Dragan Halatis       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Edmund Garzin        | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Ernest Kajrat        | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Isaura Velaska       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Leon Varkas          | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Lestral Kirmanik     | 1 | ((230806-zwiad-w-iliminar-caos)) |