---
categories: profile
factions: 
owner: public
title: Roman Panracz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211114-admiral-gruby-wieprz         | kiedyś kolega Tymona Grubosza; 66 lat. W 73% maszyna. Weteran na rencie, ale nie bezużyteczny. Mówi na młódki "słoneczka". Współpracuje z Klaudią; pomaga lokalnym niezdarnym TAI zrobić efektowne i mało użyteczne akcje, by się nie pozabijały. | 0112-02-15 - 0112-02-20 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adalbert Brześniak   | 1 | ((211114-admiral-gruby-wieprz)) |
| Izabela Zarantel     | 1 | ((211114-admiral-gruby-wieprz)) |
| Klaudia Stryk        | 1 | ((211114-admiral-gruby-wieprz)) |
| Rafael Galwarn       | 1 | ((211114-admiral-gruby-wieprz)) |
| Rzieza d'K1          | 1 | ((211114-admiral-gruby-wieprz)) |
| Sabina Kazitan       | 1 | ((211114-admiral-gruby-wieprz)) |