---
categories: profile
factions: 
owner: public
title: Maurycy Derwisz
---

# {{ page.title }}


# Generated: 



## Fiszki


* szeregowy, technik i miłośnik historii Harmonii Diakon | @ 231024-rozbierany-poker-do-zwalczania-interis
* OCEAN: (N+C+): Nieśmiały, skoncentrowany na badaniach i technologii, cierpliwy cichy i dokładny. "Zna się na rzeczach, o których inni nawet nie słyszeli. Ale musisz się dobrze przyjrzeć, żeby go zauważyć." | @ 231024-rozbierany-poker-do-zwalczania-interis
* VALS: (Achievement, Self-Direction): Zbiera wiedzę o Harmonii Diakon; nieskończenie układający 'puzzle' jej życia. "Tajemnice Harmonii to wyzwanie które przezwyciężę." | @ 231024-rozbierany-poker-do-zwalczania-interis
* Core Wound - Lie: "Jego rodzina musiała uciekać z Aurum z uwagi na powiązanie z Harmonią Diakon" - "Jeśli zrozumiem o co chodziło z Harmonią, zrozumiem sekret mojej rodziny" | @ 231024-rozbierany-poker-do-zwalczania-interis
* Styl: Praktyczne ubrania, do swobodnej pracy z różnymi urządzeniami i dyskrecji. Mimo nieśmiałości wobec kobiet, jeden z lepszych strzelców i ekspertów od sprzętu. Przystojny chłopaczek. | @ 231024-rozbierany-poker-do-zwalczania-interis

### Wątki


salvagerzy-lohalian
rehabilitacja-eleny-samszar

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231024-rozbierany-poker-do-zwalczania-interis | prześliczny chłopiec, wstydzi się rozmowy z Eleną. Wciągnął ją do gry w karty na polecenie Tomasza. Też na polecenie zaproponował grę w strip poker. | 0095-10-08 - 0095-10-10 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adelajda Kalmiris    | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Cyprian Mirisztalnik | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Elena Samszar        | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Estella Gwozdnik     | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Jaromir Gaburon      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Jarosław Mirelski    | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Mikołaj Larnecjat    | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Paweł Lawarczak      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Tomasz Afagrel       | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |