---
categories: profile
factions: 
owner: public
title: Berenika Wrążowiec
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200326-test-z-etyki                 | AMZ; Zmaterializowała homunkulusa Napoleona, a potem impa który gnębi szkołę magów. Dość bezwzględna w swoich celach; byle mieć tą durną etykę za sobą. | 0110-07-29 - 0110-07-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aniela Kark          | 1 | ((200326-test-z-etyki)) |
| Ignacy Myrczek       | 1 | ((200326-test-z-etyki)) |
| Liliana Bankierz     | 1 | ((200326-test-z-etyki)) |
| Napoleon Bankierz    | 1 | ((200326-test-z-etyki)) |
| Teresa Mieralit      | 1 | ((200326-test-z-etyki)) |