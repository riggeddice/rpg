---
categories: profile
factions: 
owner: public
title: SC Hektor 17
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220925-mlodziaki-na-savaranskim-statku-handlowym | savarański statek handlowy; przez chorobę pilota trafił do K1 i zatrudnili tymczasowo rozpieszczonych ludzi niezdolnych do działania na savarańskiej jednostce. Ma biosynta, pacyfikatory, operuje na zerowej energii i przewozi części by odbudować jednostkę 'domową'. | 0109-05-24 - 0109-05-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dawid Aximar         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Fabian Korneliusz    | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Klaudia Stryk        | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Klaudiusz Zanęcik    | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Martyn Hiwasser      | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| OO Serbinius         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Perdius Aximar       | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |