---
categories: profile
factions: 
owner: public
title: Aida Serenit
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190802-statek-zjada-statki          | żywa, chodząca szczepionka przeciwko asymilacji Serenita. Uratowana z Serenita przez Zespół dzięki advancerowi Michałowi oraz reszcie Zespołu. | 0087-08-09 - 0087-08-12 |
| 190721-kirasjerka-najgorszym-detektywem | agentka Orbitera, blackops; uwielbia koncerty Serafiny. Zniknęła, najpewniej porwana przez Cieniaszczyt. Zaprzyjaźniła się z Mirelą (emulatorką) | 0110-06-10 - 0110-06-11 |
| 190724-odzyskana-agentka-orbitera   | nie agentka a zwykły cywil Orbitera; całkowicie niegroźna. Uratowana z kapsuły w kosmosie. Skażona kralotycznie, wygrana przez championa Pięknotki od kralotha. | 0110-06-12 - 0110-06-15 |
| 210512-ewakuacja-z-serenit          | nieszczęsny pasażer Falołamacza. Posłuchała rozkazów Arianny i uratowała kogo się dało z Falołamacza (m.in. Sowińskiego i Anataela). | 0111-11-23 - 0111-12-02 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190802-statek-zjada-statki          | emanuje energią którą wyczuwa Saitaer. | 0087-08-12

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Serenit           | 2 | ((190802-statek-zjada-statki; 210512-ewakuacja-z-serenit)) |
| Mirela Orion         | 2 | ((190721-kirasjerka-najgorszym-detektywem; 190724-odzyskana-agentka-orbitera)) |
| Pięknotka Diakon     | 2 | ((190721-kirasjerka-najgorszym-detektywem; 190724-odzyskana-agentka-orbitera)) |
| Adam Szarjan         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Arianna Verlen       | 1 | ((210512-ewakuacja-z-serenit)) |
| Bogdan Anatael       | 1 | ((210512-ewakuacja-z-serenit)) |
| Elena Verlen         | 1 | ((210512-ewakuacja-z-serenit)) |
| Eustachy Korkoran    | 1 | ((210512-ewakuacja-z-serenit)) |
| Eva d'Mikado         | 1 | ((190802-statek-zjada-statki)) |
| Ignaś Orbita         | 1 | ((190802-statek-zjada-statki)) |
| Julia Morwisz        | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Klaudia Stryk        | 1 | ((210512-ewakuacja-z-serenit)) |
| Laura Prunal         | 1 | ((190802-statek-zjada-statki)) |
| Martyn Hiwasser      | 1 | ((210512-ewakuacja-z-serenit)) |
| Michał Dusiciel      | 1 | ((190802-statek-zjada-statki)) |
| Mirela Niecień       | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Moktar Gradon        | 1 | ((190724-odzyskana-agentka-orbitera)) |
| OE Falołamacz        | 1 | ((210512-ewakuacja-z-serenit)) |
| Olga Myszeczka       | 1 | ((190721-kirasjerka-najgorszym-detektywem)) |
| OO Infernia          | 1 | ((210512-ewakuacja-z-serenit)) |
| Roland Sowiński      | 1 | ((210512-ewakuacja-z-serenit)) |
| SC Mikado            | 1 | ((190802-statek-zjada-statki)) |
| Sia d'Cranis         | 1 | ((190802-statek-zjada-statki)) |
| Stella Koral         | 1 | ((190802-statek-zjada-statki)) |
| Travis Longhorn      | 1 | ((190802-statek-zjada-statki)) |
| Wiktor Satarail      | 1 | ((190721-kirasjerka-najgorszym-detektywem)) |