---
categories: profile
factions: 
owner: public
title: Natan Wierzbitowiec
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: N+O+ | niezwykle lojalny, cichy, wykonuje rozkazy bez kwestionowania; bezwzględny - Soundwave, TF:Prime. | @ 240306-potwornosc-powojenna-trzykwiatu
* VALS: Tradition > Self-direction | Powodzenie Trzykwiatu, zachowanie status quo. Latisza nade wszystko. | @ 240306-potwornosc-powojenna-trzykwiatu

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240306-potwornosc-powojenna-trzykwiatu | kompetentny administrator i świetny żołnierz, outer patrol i łapanie ludzi by karmić esurienta. To on naprawdę dowodzi w miejscu Latiszy. Nadal zostaje w Trzykwiacie, ale nie musi już nikogo zabijać i nie musi łapać ludzi by wzmacniać esurienta. Ewakuował Zofię i najbardziej umoczonych, tworząc Ciernie Latiszy. | 0084-04-16 - 0084-04-21 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 240306-potwornosc-powojenna-trzykwiatu | mimo, że jest mastermindem stojącym za Trzykwiatem, Cierniami Latiszy oraz esurientem, ma pełen pardon ze strony Pustogoru. Zastępca Jakuba. | 0084-04-21

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Jakub Kurbeczko      | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Ksenia Byczajnik     | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Latisza Warkolnicz   | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Natalia Pszaruk      | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Petra Łomniczajew    | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Radosław Zientarmik  | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Ulrich Warkolnicz    | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Zofia Skorupniczek   | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |
| Zuzanna Szagbin      | 1 | ((240306-potwornosc-powojenna-trzykwiatu)) |