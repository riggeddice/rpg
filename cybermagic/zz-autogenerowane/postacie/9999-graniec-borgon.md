---
categories: profile
factions: 
owner: public
title: Graniec Borgon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220323-zatruta-furia-gaulronow      | gaulron dotknięty Furią; niszczy bazę. Pierwszy gaulron któremu zaczęło odbijać; powiedział Lamii, że zaczęło się 2-3 tyg temu. Poprosił że chce do więzienia bo nie chce skrzywdzić nikogo. | 0111-11-27 - 0111-12-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dominika Perikas     | 1 | ((220323-zatruta-furia-gaulronow)) |
| Erwin Mumurnik       | 1 | ((220323-zatruta-furia-gaulronow)) |
| Feliks Ketran        | 1 | ((220323-zatruta-furia-gaulronow)) |
| Lamia Akacja         | 1 | ((220323-zatruta-furia-gaulronow)) |
| Maciej Parczak       | 1 | ((220323-zatruta-furia-gaulronow)) |
| Suwan Chankar        | 1 | ((220323-zatruta-furia-gaulronow)) |
| TAI Nephthys         | 1 | ((220323-zatruta-furia-gaulronow)) |