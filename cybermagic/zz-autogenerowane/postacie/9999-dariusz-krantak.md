---
categories: profile
factions: 
owner: public
title: Dariusz Krantak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200923-magiczna-burza-w-raju        | strażnik Anastazji, który chciałby się jej pozbyć; twardy dowódca terrorem. 2 lata temu zmiażdżył siły noktiańskie próbujące porwać Anastazję. | 0111-02-21 - 0111-02-25 |
| 201014-krystaliczny-gniew-elizy     | ochroniarz Anastazji któremu ufała i który chciał ją zabić; mastermind, używający zniewolonych noktian. Oddany Ataienne by wydobyć zeń prawdę. | 0111-03-02 - 0111-03-05 |
| 210127-porwanie-anastazji-z-odkupienia | przekształcony przez Sowińskich w cybernetycznego kapitana Odkupienia / technohorror / TAI. Żałuje i cierpi. Powstrzymany przez Eustachego przed złapaniem Anastazji. | 0111-07-22 - 0111-07-23 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 210127-porwanie-anastazji-z-odkupienia)) |
| Elena Verlen         | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 210127-porwanie-anastazji-z-odkupienia)) |
| Eustachy Korkoran    | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 210127-porwanie-anastazji-z-odkupienia)) |
| Klaudia Stryk        | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 210127-porwanie-anastazji-z-odkupienia)) |
| Anastazja Sowińska   | 2 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Eliza Ira            | 2 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Marian Fartel        | 2 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Aleksandra Termia    | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Anastazja Sowińska Dwa | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Ataienne             | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Diana Arłacz         | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Izabela Zarantel     | 1 | ((200923-magiczna-burza-w-raju)) |
| Marianna Lemurczak   | 1 | ((200923-magiczna-burza-w-raju)) |
| Martyn Hiwasser      | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Nikodem Sowiński     | 1 | ((200923-magiczna-burza-w-raju)) |
| OA Odkupienie        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OO Infernia          | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OO Wesoły Wieprzek   | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Rufus Niegnat        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| SP Plugawy Jaszczur  | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |