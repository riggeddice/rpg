---
categories: profile
factions: 
owner: public
title: Rufus Bilgemener
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230708-wojna-w-zlotym-cieniu        | biurokrata, który całkowitym przypadkiem został 'donem mafijnym' przy Amandzie Kajrat, bo tylko na jego bazę (przez Nadię) Amanda miała namiar. Wyjątkowo rozsądny jak na średniej klasy managera Złotego Cienia; nieszczęśliwy, bo Samszarowie w jego oczach zdradzili lojalnych agentów Cienia. Bo to nie oni skrzywdzili Maję Samszar. | 0095-09-04 - 0095-09-08 |
| 230715-amanda-konsoliduje-zloty-cien | aktualny don Złotego Cienia namaszczony przez Tadeusza Samszara, pracujący 'pod Wacławem i Karolinusem', acz wie, że Amanda ma najlepsze pomysły i bez niej sobie nie poradzi. Administruje Złotym Cieniem. | 0095-09-09 - 0095-09-12 |
| 230808-nauczmy-mlodego-tiena-jak-zyc | na żądanie Amandy dostarczył jej Egzotyczną Piękność do wpłynięcia na Armina. Dowiedział się jak okrutnymi Samszarami są ci, z którymi pracuje Amanda. Nie stanie przeciw Amandzie. | 0095-09-14 - 0095-09-23 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 3 | ((230708-wojna-w-zlotym-cieniu; 230715-amanda-konsoliduje-zloty-cien; 230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Armin Samszar        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Bella Samszar        | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Celina Maskiewnik    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Elena Samszar        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Emil Samszar         | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Florian Samszar      | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Karolinus Samszar    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Kasimir Esilin       | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Kleopatra Trusiek    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Nadia Obiris         | 1 | ((230708-wojna-w-zlotym-cieniu)) |
| Tadeusz Maskiewnik   | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Tadeusz Samszar      | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Wacław Samszar       | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Wiktor Blakenbauer   | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |