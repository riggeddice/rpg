---
categories: profile
factions: 
owner: public
title: Gabriel Krajczok
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181220-upiorny-servar               | bardzo drapieżny mistrz wyścigów. Stoi za śmiercią swego rywala, Irka. Zdominował Celinę, przez co Beata wezwała (niechcący) ducha Irka. Nie ma życia. | 0109-10-28 - 0109-10-30 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksandra Garwen    | 1 | ((181220-upiorny-servar)) |
| Alicja Wielżak       | 1 | ((181220-upiorny-servar)) |
| Artur Śrubek         | 1 | ((181220-upiorny-servar)) |
| Beata Wielinek       | 1 | ((181220-upiorny-servar)) |
| Bogdan Daneb         | 1 | ((181220-upiorny-servar)) |
| Celina Szaczyr       | 1 | ((181220-upiorny-servar)) |
| Jerzy Cieniż         | 1 | ((181220-upiorny-servar)) |
| Michał Wypras        | 1 | ((181220-upiorny-servar)) |
| Patrycja Karzec      | 1 | ((181220-upiorny-servar)) |
| Ryszard Januszewicz  | 1 | ((181220-upiorny-servar)) |
| Wojciech Tuczmowil   | 1 | ((181220-upiorny-servar)) |