---
categories: profile
factions: 
owner: public
title: Kara Prazdnik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220512-influencerskie-mikotki-z-talio | 13 lat, ratując przyjaciela (Jacka) przed naćpanym Ognikiem przypadkiem reanimowała ojca Mireli; ma inteligentnego szczura. Doszła do tego, że to Mirela jest przyczyną zła na Talio. Lojalna Ognikom, zadziorna, nic nie powie. Chce śpiewać! | 0104-05-02 - 0104-05-08 |
| 220329-mlodociani-i-pirat-na-krolowej | miłośniczka karaoke wchodząca w szkodę Sewerynowi. Próbowała rozwiązać z Berdyszem sytuację kryzysową - prosiła by nie zniszczył jej przyszłości. | 0108-08-29 - 0108-09-09 |
| 220503-antos-szafa-i-statek-piscernikow | nie wygrała konkursu na najlepszy show w Szamunczaku, ale została "lokalną maskotką". Gdy przybył statek Luxuritias, chciała się do nich wymknąć - więc Antos zamknął ją w szafie. | 0108-09-23 - 0108-09-25 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220329-mlodociani-i-pirat-na-krolowej | przekonana, że Anna ma rację - dobro i pomaganie mają znaczenie. Przekonana, że Berdysz Rozdzieracz też jest dobrą osobą. Uczestniczyła w negocjacjach z nim. | 0108-09-09

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anna Szrakt          | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220503-antos-szafa-i-statek-piscernikow)) |
| Bartek Wudrak        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220503-antos-szafa-i-statek-piscernikow)) |
| Miranda Ceres        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220503-antos-szafa-i-statek-piscernikow)) |
| Antonina Terkin      | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Antos Kuramin        | 1 | ((220503-antos-szafa-i-statek-piscernikow)) |
| Aurus Czarmadon      | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Berdysz Rozdzieracz  | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Elwira Piscernik     | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Gotard Kicjusz       | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Helena Banbadan      | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Jacek Ożgor          | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Kamil Czarmadon      | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Łucjan Torwold       | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Mirella Czarmadon    | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Prokop Umarkon       | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Romana Kundel        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| SC Królowa Przygód   | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Seweryn Grzęźlik     | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Tytus Ramkon         | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Wojciech Kaznodzieja | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |