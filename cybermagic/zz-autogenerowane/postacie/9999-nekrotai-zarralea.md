---
categories: profile
factions: 
owner: public
title: NekroTAI Zarralea
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221130-astralna-flara-w-strefie-duchow | kiedyś TAI d'Isigtand, niezbyt bystra, ale ładnie Ellarinie śpiewała. Teraz - nekroTAI, ożywiona przez Ariannę i zmieniona w detektor fal działających na neikatiańskie TAI. Mądrzejsza, komunikuje się muzyką i w ten sposób przesyła emocje, anomalna istota próbująca ukoić Ellarinę i wykonać misję dla Arianny - ku przerażeniu Ellariny. | 0100-10-05 - 0100-10-08 |
| 221214-astralna-flara-kontra-domina-lucis | jednoosobowo weszła na TKO-4271, syrenim śpiewem unieszkodliwiła ludzi i wyłączyła selfdestruct. Serpentis nie był w stanie się do niej dostać; musiał terminować wszystkich noktian na Dominie Lucis. | 0100-10-09 - 0100-10-11 |
| 221221-astralna-flara-i-nowy-komodor | zestrzelona z rozkazu komodora Bolzy offscreenowo; ma to wyglądać na noktiańskie działania. | 0100-11-07 - 0100-11-10 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 3 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Daria Czarnewik      | 3 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Ellarina Samarintael | 3 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| OO Astralna Flara    | 3 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| OO Athamarein        | 3 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Elena Verlen         | 2 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor)) |
| Gabriel Lodowiec     | 2 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Kajetan Kircznik     | 2 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor)) |
| Kirea Rialirat       | 2 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Leszek Kurzmin       | 2 | ((221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Maja Samszar         | 2 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor)) |
| Arnulf Perikas       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Axel Nargan          | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Hubert Kerwelenios   | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Kaspian Certisarius  | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Klarysa Jirnik       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Leo Kasztop          | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| OO Loricatus         | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Salazar Bolza        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Sarian Xadaar        | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Szymon Wanad         | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Tomasz Dojnicz       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Tristan Rialirat     | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Władawiec Diakon     | 1 | ((221221-astralna-flara-i-nowy-komodor)) |