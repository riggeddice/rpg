---
categories: profile
factions: 
owner: public
title: Bella Samszar
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (E+O+): towarzyska, bardzo spontaniczna, uwielbia nowe pomysły i wymyślać rzeczy. "TEGO jeszcze nikt nie wymyślił!" | @ 230715-amanda-konsoliduje-zloty-cien
* VALS: (Hedonism, Stimulation): dobre jedzenie, towarzystwo, świetne imprezy i niedomiar surowców. "Niech się dzieje, człowieku!" | @ 230715-amanda-konsoliduje-zloty-cien
* Core Wound - Lie: "zostałam sama w rodzie po śmierci rodziny" - "jeśli będę epicentrum dobrej zabawy, nikt mnie nigdy nie opuści" | @ 230715-amanda-konsoliduje-zloty-cien
* styl: żywiołowa, zawsze uśmiechnięta i pomocna ze świetnymi pomysłami, grace and charm. "_Bawmy się tak, jakby jutro wszyscy mieli umrzeć!_" | @ 230715-amanda-konsoliduje-zloty-cien

### Wątki


pronoktianska-mafia-kajrata

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230715-amanda-konsoliduje-zloty-cien | 24, event manager. Ma dobre serce i zna się na rzeczy, chce zreformować Złoty Cień ale czerpać zeń korzyści; skupia się na Secesjonistach i części Lojalistów oraz na Oteriiel. | 0095-09-09 - 0095-09-12 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Emil Samszar         | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Kasimir Esilin       | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Rufus Bilgemener     | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Tadeusz Samszar      | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Wacław Samszar       | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |