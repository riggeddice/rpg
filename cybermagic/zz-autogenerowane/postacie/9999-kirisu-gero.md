---
categories: profile
factions: 
owner: public
title: Kirisu Gero
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181030-zaczestwiacy-czy-karolina    | masażysta i mistrz wyciągania informacji. Wyciągnął sporo informacji z Damiana i przekonał Eteryczne Echa do rozmowy. | 0109-10-15 - 0109-10-16 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181030-zaczestwiacy-czy-karolina    | zainteresował się tymi sprawami poważniej - został psychologiem esportu. Świetnie wspiera całą drużynę Zaczęstwiaków. | 0109-10-16

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Damian Podpalnik     | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Karolina Erenit      | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Lia Sagabello        | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Mariusz Kozaczek     | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Michał Krutkiwąs     | 1 | ((181030-zaczestwiacy-czy-karolina)) |