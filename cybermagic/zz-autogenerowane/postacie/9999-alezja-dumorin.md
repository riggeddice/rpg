---
categories: profile
factions: 
owner: public
title: Alezja Dumorin
---

# {{ page.title }}


# Generated: 



## Fiszki


* eks-kapitan, Orbiter | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* ENCAO:  +0--0 |Amoralna, skuteczna| VALS: Hedonism, Face, Power| DRIVE: Wędrowny mistrz Ryu | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* eks-kapitan, Orbiter, | @ 221102-astralna-flara-i-porwanie-na-karsztarinie

### Wątki


kosmiczna-chwala-arianny
historia-arianny
wplywy-aureliona-na-orbiterze

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly | poprzednia kapitan Królowej; pod wpływem Władawca praktycznie założyła OnlyFans. Arianna dała jej szansę i zdegenerowała do drugiego oficera. Mimo złamanego morale i braku wiary w siebie pomaga Ariannie jak może. | 0100-05-06 - 0100-05-12 |
| 220928-kapitan-verlen-i-pojedynek-z-marine | zna się z Leszkiem Kurzminem, rywalizowali w Akademii. Była świetna. Była wredna dla Leszka. | 0100-05-14 - 0100-05-16 |
| 221102-astralna-flara-i-porwanie-na-karsztarinie | ostrzega Ariannę jak aspołeczna jest Elena; wyjęła ćwiczenia orbiterowe "gramy w piłkę na kadłubie". Arianna poprosiła ją, by pomogła Elenie wyjść do ludzi. | 0100-07-28 - 0100-08-03 |
| 221116-astralna-flara-dociera-do-nonariona-nadziei | ostrzegła Ariannę, że Władawiec zaczyna się interesować Eleną. I o 'Egzotycznych Pięknościach' tanio. Ktoś rozpuścił plotki! | 0100-09-08 - 0100-09-11 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly | załoga "Królowej" uważa ją za silny minus i obniża status Arianny. Ale jej reputacja nie wyjdzie poza Królową. | 0100-05-12

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Arnulf Perikas       | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Daria Czarnewik      | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Grażyna Burgacz      | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Maja Samszar         | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Władawiec Diakon     | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Szymon Wanad         | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Erwin Pies           | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Kajetan Kircznik     | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Klarysa Jirnik       | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Leona Astrienko      | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Leszek Kurzmin       | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| OO Królowa Kosmicznej Chwały | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Tomasz Ruppok        | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Ada Wyrocznik        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Elena Verlen         | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Gabriel Lodowiec     | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Gerwazy Kircznik     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Hubert Kerwelenios   | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Klaudiusz Terienak   | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Leo Kasztop          | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Marcel Kulgard       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Mariusz Bulterier    | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Astralna Flara    | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| OO Athamarein        | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| OO Karsztarin        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| SCA Hadiah Emas      | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Stefan Torkil        | 1 | ((220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Szczepan Myrczek     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |