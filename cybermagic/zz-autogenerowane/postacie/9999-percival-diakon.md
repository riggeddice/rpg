---
categories: profile
factions: 
owner: public
title: Percival Diakon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200715-sabotaz-netrahiny            | wspomnienie o nim; wybitny psychotronik, mag, który dowodził projektem "Persefona". Imię tej TAI dał po córce. Jakieś ~40 lat temu. Chyba nie żyje? | 0111-01-04 - 0111-01-07 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((200715-sabotaz-netrahiny)) |
| Elena Verlen         | 1 | ((200715-sabotaz-netrahiny)) |
| Eustachy Korkoran    | 1 | ((200715-sabotaz-netrahiny)) |
| Klaudia Stryk        | 1 | ((200715-sabotaz-netrahiny)) |
| Lothar Diakon        | 1 | ((200715-sabotaz-netrahiny)) |
| OO Netrahina         | 1 | ((200715-sabotaz-netrahiny)) |
| OO Tvarana           | 1 | ((200715-sabotaz-netrahiny)) |
| Rufus Komczirp       | 1 | ((200715-sabotaz-netrahiny)) |
| Szczepan Myksza      | 1 | ((200715-sabotaz-netrahiny)) |