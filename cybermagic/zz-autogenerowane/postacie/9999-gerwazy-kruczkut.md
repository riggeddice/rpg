---
categories: profile
factions: 
owner: public
title: Gerwazy Kruczkut
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191120-mafia-na-stacji-gorniczej    | Badacz Otchłani. Doszedł do tego z czym mają do czynienia - z biorobotem Kuratorów. Skonfliktowany czy pozwolić Kuratorom wygrać czy ich zniszczyć. Skupił się na ratowaniu Oliwiera. | 0110-06-14 - 0110-06-25 |
| 191123-echo-eszary-na-dorszancie    | badacz otchłani na Dorszancie. Wrobiony przez Hestię Dis w stworzenie potwora ze ścieków; odkrył obecność problemu, ale nie był w stanie go rozwiązać. To zrobili inni. | 0110-07-11 - 0110-07-14 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eszara d'Dorszant    | 2 | ((191120-mafia-na-stacji-gorniczej; 191123-echo-eszary-na-dorszancie)) |
| Kamelia Termit       | 2 | ((191120-mafia-na-stacji-gorniczej; 191123-echo-eszary-na-dorszancie)) |
| Rafał Kirlat         | 2 | ((191120-mafia-na-stacji-gorniczej; 191123-echo-eszary-na-dorszancie)) |
| Filip Szczatken      | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Hestia Ain d'Dorszant | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Hestia d'Dorszant    | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Hestia Dis d'Dorszant | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Janet Erwon          | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Jaromir Uczkram      | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Oliwier Pszteng      | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Stefan Ukrand        | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Szymon Szelmer       | 1 | ((191120-mafia-na-stacji-gorniczej)) |