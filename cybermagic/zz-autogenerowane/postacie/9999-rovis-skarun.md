---
categories: profile
factions: 
owner: public
title: Rovis Skarun
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231122-sen-chroniacy-kochankow      | przez swoją decyzję o porwaniu Joli-09 wplątał się z nią w relację romantyczną. Jego konflikt z Dorionem ją ujawnił, i gdy uciekał z ukochaną doszło do manifestacji Alucis. Opuścił stację przez Agencję. | 0105-09-02 - 0105-09-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Damian Orczakin      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Dorion Fughar        | 1 | ((231122-sen-chroniacy-kochankow)) |
| Felina Amatanir      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Jola-09 Szernief     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Kalista Surilik      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Klasa Dyplomata      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Klasa Hacker         | 1 | ((231122-sen-chroniacy-kochankow)) |
| Klasa Oficer Naukowy | 1 | ((231122-sen-chroniacy-kochankow)) |
| Klaudiusz Widar      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Mawir Hong           | 1 | ((231122-sen-chroniacy-kochankow)) |
| Szymon Alifajrin     | 1 | ((231122-sen-chroniacy-kochankow)) |