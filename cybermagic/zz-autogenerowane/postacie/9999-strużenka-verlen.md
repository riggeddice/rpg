---
categories: profile
factions: 
owner: public
title: Strużenka Verlen
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230517-tchorzliwy-leonidas-na-niedzwiedziowisku | instruktorka skradania na niedźwiedziowisku, Verlen drugiej klasy. Doszła do tego o co chodzi od sierżant Ciężkiej Łapy, znalazła Leonidasa i przekonała go, że da radę pokonać "Blakenbestię". Doprowadziła do konfrontacji Leonidasa z potworem pod wodospadem ("Verlen kontra potwór, półnago pod wodospadem"). | 0095-03-24 - 0095-03-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dźwiedź Ciężka Łapa  | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
| Dźwiedź Lekka Stopa  | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
| Dźwiedź Palisadowspinacz | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
| Leonidas Verlen      | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
| Wędziwój Verlen      | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |