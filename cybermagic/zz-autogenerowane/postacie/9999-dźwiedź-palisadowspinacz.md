---
categories: profile
factions: 
owner: public
title: Dźwiedź Palisadowspinacz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230517-tchorzliwy-leonidas-na-niedzwiedziowisku | komandos na niedźwiedziowisku; asekuruje Leonidasa, jest "desygnowanym niedźwiedziem do pokonania", ale po wpadnięciu w pułapkę na tyle przestraszył Leonidasa, że Paradoks zmergował go z Lekką Stopą by zmienić ich w Blakenbestię. | 0095-03-24 - 0095-03-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dźwiedź Ciężka Łapa  | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
| Dźwiedź Lekka Stopa  | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
| Leonidas Verlen      | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
| Strużenka Verlen     | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
| Wędziwój Verlen      | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |