---
categories: profile
factions: 
owner: public
title: Karradrael
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190116-wypalenie-saitaera-z-trzesawiska | doprowadzony przez swojego kapłana do ołtarza Saitaera rozpoczął wypalanie - Saitaer nie podjął rękawicy, więc go odepchnął od zarówno Trzęsawiska jak i Wiktora. | 0110-01-07 - 0110-01-09 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Bartozol        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| ASD Centurion        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Hieronim Maus        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Kasjopea Maus        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Pięknotka Diakon     | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Saitaer              | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Wiktor Satarail      | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |