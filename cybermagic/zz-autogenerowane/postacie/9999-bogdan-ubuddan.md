---
categories: profile
factions: 
owner: public
title: Bogdan Ubuddan
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230625-przegrywy-ratuja-przegrywy   | z innego zespołu; pustynia i mokradła prawie go zabiły. W większości nieprzytomny i słaby, ale Zespół mu pomaga i go ratuje. | 0083-10-08 - 0083-10-11 |
| 230626-pustynny-final-igrzysk       | nadal nieprzytomny, nadal nic nie może zrobić. Ale przeżył. Dzięki jego obecności dało się łatwiej 'wkręcić', że Olaf ma pełen zespół i to nie jest pułapka. | 0083-10-12 - 0083-10-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Klart           | 2 | ((230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Antoni Kmandir       | 2 | ((230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Lily Sanarton        | 2 | ((230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Maja Wurmramin       | 2 | ((230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Maks Ardyceń         | 2 | ((230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Olaf Zuchwały        | 2 | ((230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Roman Wyrkmycz       | 2 | ((230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Jan Firatiel         | 1 | ((230626-pustynny-final-igrzysk)) |
| Rafał Armadion       | 1 | ((230626-pustynny-final-igrzysk)) |