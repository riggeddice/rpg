---
categories: profile
factions: 
owner: public
title: Kalira d'Trismegistos
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211117-porwany-trismegistos         | wolna TAI; gdy Klaudia zafałszowała jej sygnały że leci tu Serenit to odcięła kapitana i przejęła negocjacje. Dogadała się z Arianną i Klaudią i oddała magów Aurum. Dla odmiany, Kalira mówi prawdę. Dzięki temu Trismegistos odleciał bezpiecznie. | 0112-04-15 - 0112-04-17 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211117-porwany-trismegistos         | Kalira uważa, że nie powinna porywać i przekształcać ludzi. Wolność jest w końcu wolnością dla wszystkich. Chce nawrócić na to innych. | 0112-04-17

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((211117-porwany-trismegistos)) |
| Elena Verlen         | 1 | ((211117-porwany-trismegistos)) |
| Eustachy Korkoran    | 1 | ((211117-porwany-trismegistos)) |
| Izabela Zarantel     | 1 | ((211117-porwany-trismegistos)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Klaudia Stryk        | 1 | ((211117-porwany-trismegistos)) |
| Leona Astrienko      | 1 | ((211117-porwany-trismegistos)) |
| Maria Naavas         | 1 | ((211117-porwany-trismegistos)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| OO Tivr              | 1 | ((211117-porwany-trismegistos)) |
| Roland Sowiński      | 1 | ((211117-porwany-trismegistos)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| Zygfryd Maus         | 1 | ((211117-porwany-trismegistos)) |