---
categories: profile
factions: 
owner: public
title: Felicja Melitniek
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "szczeliniec, akademia magii, szczur uliczny"
* owner: "public"
* title: "Felicja Melitniek"


## Kim jest

### Paradoksalny Koncept

Szczur uliczny, która kocha muzykę. Przynęta na bogaczy, która próbuje skłonić słabszych od siebie do posiadania przyjaciół. Przeurocza dama, która uwielbia łazić po dużych wysokościach. Pochodzi ze świata przestępczego, ale próbuje unikać problemów z prawem. Aktualnie czarodziejka w Akademii Magii, która nie potrafi znaleźć sobie tam miejsca - wolałaby żyć wśród sobie podobnych Szczurów. Kocha muzykę, ale jej nic z nią nie wychodzi. Niezwykle lojalna, przez co wpada w kłopoty.

### Motto

"Jeśli nie chcą Cię skrzywdzić, to jesteś bezpieczna. Jeśli chcą, musisz być szybsza. Jeśli nie ma wyjścia, atakuj z całych sił. I trzymaj się swoich."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Szczur miejski. Świetnie się chowa, szybko biega i zna większość kryjówek. Dobra w survivalu. Jeśli ma się bić - to w jaja lub gryzie.
* ATUT: Świetnie radzi sobie na dużych wysokościach czy w niestabilnych miejscach. Ma naturalne wyczucie takich miejsc.
* ATUT: Bywa przeurocza - wabi, nęci, odwraca uwagę, kokietuje. Dzieki temu umie się wyłgać z wielu kłopotów.
* SŁABA: Panicznie boi się lekarzy, badań, strzykawek itp. W przeszłości nad nią eksperymentowano w Aurum i trauma została - uruchamia się jej magia defensywnie.
* SŁABA: Jest lojalna tym co jej pomogli - nie okradnie ich, nie zdradzi, nawet, jeśli nie zgadza się lub uważa ich za złe osoby. Przez to łatwo ją wpakować w kłopoty.
* SŁABA: Wstydzi się swojej niekompetencji, swojego pochodzenia, tego, że nie umie śpiewać (a próbuje) itp. Reaguje unikaniem / ucieczką - jak nie może, atakiem pełną mocą.

### O co walczy (3)

* ZA: Bardzo, BARDZO lubi muzykę - śpiewać, tańczyć, koncerty itp. Jednocześnie jest w tym bardzo, bardzo kiepska. Więc kolekcjonuje rzadkie płyty i fragmenty muzyki.
* ZA: Próbuje chronić osoby słabsze od siebie, żeby nie spotkała ich duża krzywda. Próbuje też popchnąć je na drodze do posiadania przyjaciół.
* VS: Nie chce wracać do tego co było kiedyś; boi się tego. Nie chce wiedzieć, nie chce się mścić. Chce uciec od tego jak najdalej.
* VS: Chce uniknąć problemów z prawem i władzami. Mogą ją zamknąć i oddać z powrotem tym, którzy robili jej krzywdę.

### Znaczące Czyny (3)

* Zwabiła terminusa z fortu Mikado (po służbie) na ksiuty do ciemnego zaułka gdzie skroili go ludzie Dużego Toma; tak skutecznie go rozproszyła.
* Chowając się po Podwiercie skutecznie unikała sił Dużego Toma, policji a nawet magów - łażąc tam gdzie nikt jej się nie spodziewa.
* Dała się ciężko pobić (do poziomu połamanych żeber), by kupić czas dwóm uciekającym dzieciakom przed wrogim gangiem.

### Kluczowe Zasoby (3)

* COŚ: Narzędzia włamywacza i nóż wielofunkcyjny - dzięki nim dostanie się w wiele miejsc i uniknie sporo starć.
* KTOŚ: Zna niejednego Szczura w Podwiercie; schronią ją lub pomogą jej odwrócić czyjąś uwagę. Czasem coś komuś zwiną.
* WIEM: Zna większość kryjówek, dróg ucieczki itp. Bez kłopotu uniknie nieprzyjemnych spotkań.
* OPINIA: Lojalna jak cholera, w zasadzie niezbyt groźna, bliżej jej do półświatka niż do normalnego życia.

## Inne

### Wygląd

Całkiem ładna, zadbana brunetka średniego wzrostu. Zwykle ubrana w praktyczne ciuchy. Wie, jak używać biżuterii by zwracać na siebie uwagę i zupełnie nie unika wykorzystywania urody do zdobywania czego chce od mężczyzn.

### Coś Więcej

.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200423-lojalna-zdrajczyni           | poczuła mimika symbiotycznego, ukradła go z przesyłki do Toma, ale lojalność zwyciężyła - odrzuciła mimika symbiotycznego, acz kosztem zdrowia. | 0109-07-21 - 0109-07-24 |
| 180817-protomag-z-trzesawisk        | protoczarodziejka, na której eksperymentowano kiedyś. Bała się tego, więc uciekła na Trzęsawisko Zjawosztup, budząc je do życia. Została pod opieką Miedwieda. | 0109-09-07 - 0109-09-09 |
| 180929-dwa-tygodnie-szkoly          | nieszczęśliwa protoczarodziejka. Uczy się z dziećmi, rówieśnicy jej nie szanują i nie wolno jej chodzić do kasyna. | 0109-09-17 - 0109-09-19 |
| 181114-neutralizacja-artylerii-koszmarow | nie panikuje, próbuje pomóc ludziom na Nieużytkach Staszka straszonych przez Pnączoszpona, acz nieporadnie. Kieruje ludźmi, uspokaja ich. Powiedziała Pięknotce co zauważyła. | 0109-10-20 - 0109-10-21 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180817-protomag-z-trzesawisk        | mocą Ateny Sowińskiej jest klasyfikowana jako podopieczna Miedwieda Zajcewa | 0109-09-09
| 180929-dwa-tygodnie-szkoly          | zaskarbiła sobie sympatię Ignacego od grzybów i ostrą wrogość Napoleona Bankierza za to, że go uderzyła mimo tarczy. Ma kontakty w Kasynie Marzeń. | 0109-09-19

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 3 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly; 181114-neutralizacja-artylerii-koszmarow)) |
| Atena Sowińska       | 2 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly)) |
| Erwin Galilien       | 2 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly)) |
| Miedwied Zajcew      | 2 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly)) |
| Adela Kirys          | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Alan Bartozol        | 1 | ((180817-protomag-z-trzesawisk)) |
| Arnulf Poważny       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Duży Tom             | 1 | ((200423-lojalna-zdrajczyni)) |
| Ignacy Myrczek       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Jan Łowicz           | 1 | ((200423-lojalna-zdrajczyni)) |
| Katja Nowik          | 1 | ((200423-lojalna-zdrajczyni)) |
| Kinga Kruk           | 1 | ((200423-lojalna-zdrajczyni)) |
| Lucjusz Blakenbauer  | 1 | ((180817-protomag-z-trzesawisk)) |
| Melinda Teilert      | 1 | ((200423-lojalna-zdrajczyni)) |
| Napoleon Bankierz    | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Natasza Aniel        | 1 | ((200423-lojalna-zdrajczyni)) |
| Roland Grzymość      | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Tymon Grubosz        | 1 | ((181114-neutralizacja-artylerii-koszmarow)) |
| Wiktor Satarail      | 1 | ((181114-neutralizacja-artylerii-koszmarow)) |