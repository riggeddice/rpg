---
categories: profile
factions: 
owner: public
title: Konstanty Keksik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231025-spiew-nielalki-na-castigatorze | tien na Castigatorze, zarządzał przemytem. To on ściągnął NieLalkę i umieścił ją by Kura Złożyła Jajko. Pierwszy najmnocniej zarażony, nie zrobił nic złego. | 0110-10-24 - 0110-10-26 |
| 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze | (nieobecny) to on sprowadził anipacis do zainfekowania Eleny. Nawet jego własna siostra uważa, że nie bywa trzeźwy. | 0110-12-19 - 0110-12-21 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Elena Verlen         | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Igor Arłacz          | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Klaudia Stryk        | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Leszek Kurzmin       | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Marta Keksik         | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Patryk Samszar       | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Anna Tessalon        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Arianna Verlen       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Eleonora Perłamila   | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Eustachy Korkoran    | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Feliks Walrond       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Julia Myrczek        | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Kamil Burgacz        | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Kosmicjusz Tanecznik Diakon | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Leona Astrienko      | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Łucja Larnecjat      | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Martyn Hiwasser      | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| OO Castigator        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| OO Infernia          | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Raoul Lavanis        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| TAI Eszara d'Castigator | 1 | ((231025-spiew-nielalki-na-castigatorze)) |