---
categories: profile
factions: 
owner: public
title: Aleksandra Burgacz
---

# {{ page.title }}


# Generated: 



## Fiszki


* czarodziejka, winggirl Lei, 23 | @ 230325-ten-nawiedzany-i-ta-ukryta
* ENCAO:  0-0++ |Niefrasobliwa, beztroska i nieco naiwna;Zawsze zgodna z najnowszą modą;;Barwna| VALS: Universalism, Hedonism| DRIVE: Supernowa | @ 230325-ten-nawiedzany-i-ta-ukryta
* styl: elegant butterfly, na świeczniku, nie myśli o innych i jest niezbyt bystra; jak parodia Misy z Death Note | @ 230325-ten-nawiedzany-i-ta-ukryta
* przewaga: artefaktorka, specjalistka od materii | @ 230325-ten-nawiedzany-i-ta-ukryta

### Wątki


magiczne-szczury-podwierckie

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230325-ten-nawiedzany-i-ta-ukryta   | Rekin; nieco poślednia córa Burgaczów. Bardzo mało bystra. Nie ma w niej zła, ale jest całkowite ignorowanie potrzeb innych. Dokucza Michałowi (jak wyglądałbyś w tej sukience? słuchaj mam problem z kolegą co byś poradził o 2 rano?), ale jak zobaczyła że Michał ma problem to próbowała pomóc mu go rozwiązać. Chciała zdrażnić lokalsów w Podwiercie głośnym wyścigiem, ale skończyło się tym że musiała pocałować Bulteriera który wygrał XD | 0111-10-16 - 0111-10-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Daniel Terienak      | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Franek Bulterier     | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Karolina Terienak    | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Lea Samszar          | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Michał Klabacz       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Nadia Uprewien       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Oliwier Czepek       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Rupert Mysiokornik   | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |