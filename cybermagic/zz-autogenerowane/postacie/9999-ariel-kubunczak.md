---
categories: profile
factions: 
owner: public
title: Ariel Kubunczak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220819-tank-as-a-love-letter        | administrator of Mimosa's site near Czółenko. Henryk Wkrąż forged Mimosa's documents so he didn't know he should be in charge. Worked with Team to solve it, but he wants to have NOTHING to do with Aurum and Aurum intrigues anymore. Mimosa lost him as an agent. | 0111-10-07 - 0111-10-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arkadiusz Terienak   | 1 | ((220819-tank-as-a-love-letter)) |
| AU Flara Astorii     | 1 | ((220819-tank-as-a-love-letter)) |
| Daniel Terienak      | 1 | ((220819-tank-as-a-love-letter)) |
| Henryk Wkrąż         | 1 | ((220819-tank-as-a-love-letter)) |
| Iwona Perikas        | 1 | ((220819-tank-as-a-love-letter)) |
| Izabela Selentik     | 1 | ((220819-tank-as-a-love-letter)) |
| Michał Kabarniec     | 1 | ((220819-tank-as-a-love-letter)) |
| Mimoza Diakon        | 1 | ((220819-tank-as-a-love-letter)) |
| Talia Mikrit         | 1 | ((220819-tank-as-a-love-letter)) |