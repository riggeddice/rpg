---
categories: profile
factions: 
owner: public
title: Błażej Sowiński
---

# {{ page.title }}


# Generated: 



## Fiszki


* kapitan Athamarein, zależy mu na ratowaniu każdego ludzkiego życia | @ 231018-anomalne-awarie-athamarein
* OCEAN: E-A+ | Introspektywny i bardzo analityczny;; Niechętnie się kłóci;; | VALS: Benevolence, Humility | DRIVE: Papa Smerf | @ 231018-anomalne-awarie-athamarein

### Wątki


legenda-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231018-anomalne-awarie-athamarein   | kapitan, zna historię Athamarein i nauczył się wszystkiego na jej temat czego był w stanie. Niezbyt pewny siebie i nieco za lekko trzyma ludzi za mordę, ale ma potencjał. Stanął naprzeciw Arianny chroniąc załogę i Athamarein. | 0111-12-22 - 0111-12-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alicja Szadawir      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Andrzej Gwozdnik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Arianna Verlen       | 1 | ((231018-anomalne-awarie-athamarein)) |
| Elena Verlen         | 1 | ((231018-anomalne-awarie-athamarein)) |
| Eustachy Korkoran    | 1 | ((231018-anomalne-awarie-athamarein)) |
| Hiacynt Samszar      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Jakub Oroginiec      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Kajetan Kircznik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Klaudia Stryk        | 1 | ((231018-anomalne-awarie-athamarein)) |
| Leona Astrienko      | 1 | ((231018-anomalne-awarie-athamarein)) |
| OO Athamarein        | 1 | ((231018-anomalne-awarie-athamarein)) |
| Remigiusz Alkarenit  | 1 | ((231018-anomalne-awarie-athamarein)) |
| Uśmiechniczka Konstrukcjonistka Aurora Diakon | 1 | ((231018-anomalne-awarie-athamarein)) |