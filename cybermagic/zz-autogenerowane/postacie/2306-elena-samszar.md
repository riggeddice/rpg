---
categories: profile
factions: 
owner: anadia
title: Elena Samszar
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "Pustogor"
* owner: "anadia"
* title: "Elena Samszar"

## Funkcjonalna mechanika

* **Obietnica**
    * ?
* **Rdzeń mechaniczny**
    * **Mistrzostwo (1-3)**
        * GDY , TO 
    * **Kompetencje (3-5)**
        * .
    * **Agenda i styl działania (3-5)**
        * PRAGNIE .
        * GDY  TO 
    * **Słabości (1-5)**
        * NAWET GDY  TO  
        
## Reszta

### W kilku zdaniach

* Elena Samszar: czarodziejka origami i kinezy <-- objęta przez Anadię
    * Archiwistka, biurokracja, prawo i dokumenty
    * Jej papier potrafi zrobić krzywdę
    * W wolnym czasie robię rzeczy z papieru, wieże, łabędzie itp.
    * A teraz, ku wielkiemu smutkowi, pomagam stanąć na nogi kuzynowi Karolinusowi >.>
* Nauka i duchy
* Arystokratka, nie w stylu Torszeckiego. W Bibliotece raczej wygląda jak taka uczennica z anime, bo to wygodniejsze, żeby siedzieć w bibliotece i chodzić po regałach niż w długiej sukni, ale jak chce to potrafi "zmienić się" w 100% arystokratkę.

.

* Jej cel pośredni: najlepsza biblioteka na świecie (Gwiazdoczy?) 
* Cel główny: WIEDZIEĆ i umieć. 
    * Dlatego dużo czyta, kontaktuje się z duchami, bo chce wiedzy. 
    * Ma bdb pamięć. Ma dużą wiedzę, albo wie, gdzie coś znaleźć. 
    * Jest młoda, więc czasem wydaje jej się, że wszystko potrafi zrobić, bo o tym czytała, ale nie jest to prawdą (np. czytała o sztukach walki, więc umie kung-fu... no ale fizycznie nie ćwiczyła tej sztuki walki, więc lepiej, żeby walczyła papierem ;)) 
        * Chociaż zakładam, że fizycznie jest sprawna, bo np. nosi książki czy włazi na regały po stary skrypt "hujwieczego". 
* Myślę, że gdy słyszy o jakimś odkryciu np.zaginionego starodruku, to chce go chociaż przeczytać. Więc ma "subskrypcje" na nowinki ze świata archeologicznego, archiwistycznego itp. (Może jest tajną członkinią fanów papieru czerpanego - jeszcze przemyślę czego xD) 
* Chroni swoje ziemie i swoich ludzi i swoje duchy. Nie zawaha się, by poświęcić kogoś do ich obrony (kogoś kto nabroił np.tego kiepskiego maga z ostatniej sesji, swoich nie). Myślę, że bardziej kieruje się w stronę starszych, bo są mniej chaotyczni i "mądrzejsi" niż młodzi i tym mogę wyjaśnić jej irytację na Karolinusa ;)
* Stosunek do starszych: jeśli mają wiedzę to spoko np. Ojciec Eleny miał wiedzę, więc to był spoko. Niekoniecznie będzie szanować tych bez wiedzy lub jeśli już nic się nie może od nich nauczyć. Aczkolwiek nie będzie nimi jawnie pogardzać. * Jak wiemy z ostatniej sesji, manipulatorka.

### Jak sterować postacią

* **O czym myśli**
    * ?
* **Serce**
    * **Wound**
        * **Core Wound**: ?
        * **Core Lie**: ?
    * **ENCAO**: ?
    * **Wartości**: ?
    * **DELTA_DRIVE**: 
        * **Stan aktualny**: ?
        * **Stan oczekiwany**: ?
    * **Metakultura**: ?
    * **Kolory**: ?
    * **Wzory**: 
        * ?
    * **Inne**:
        * brak

## Możliwości

* **Dominujące Strategie**: 
    * nieokreślone
* **Zasoby**: 
    * nieznane
* **Umocowanie w świecie**: 
    * tien Samszar, arystokratka

## Magia
### Dominująca moc

* JEDNYM ZDANIEM: czarodziejka origami i kinezy
    * .

#### Jak się objawia utrata kontroli

* .


# Generated: 



## Fiszki


* czarodziejka origami i kinezy, <-- objęta przez Anadię | @ 230404-wszystkie-duchy-siewczyna
* Archiwistka, biurokracja, prawo i dokumenty | @ 230404-wszystkie-duchy-siewczyna
* Jej papier potrafi zrobić krzywdę | @ 230404-wszystkie-duchy-siewczyna
* W wolnym czasie robię rzeczy z papieru, wieże, łabędzie itp. | @ 230404-wszystkie-duchy-siewczyna
* A teraz, ku wielkiemu smutkowi, pomagam stanąć na nogi kuzynowi Karolinusowi >.> | @ 230404-wszystkie-duchy-siewczyna
* czarodziejka origami i kinezy <-- objęta przez Anadię | @ 230418-zywy-artefakt-w-gwiazdoczach

### Wątki


hiperpsychotronika-samszarow
karolinus-heart-viorika
brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230418-zywy-artefakt-w-gwiazdoczach | gdy Joachim się do niej zwrócił, że Samszarka przeklęła jego kuzyna, zajęła się sprawą. Zebrała ekipę - turysta Porzecznik, koleżanka Blakenbauer - i doszła do tego, że to nie był czar a 'żywy artefakt', tatuaż Esuriit. Po zlokalizowaniu ofiary, zamknęła ją sentisiecią w komnacie i złapała też ducha doradzającego w sprawie Esuriit zanim sekrety jak duch został unsealowany zanikną. | 0094-10-04 - 0094-10-06 |
| 230404-wszystkie-duchy-siewczyna    | przesłuchiwała jako tienka ludzi pracujących dla lokalnego dyrektora nie lubiącego magii; magicznie połączyła się ze Strażnikiem Spichlerza i Paradoksem dała Hybrydzie owego Strażnika zniszczyć. Ale przekonała Hybrydę, że egzorcysta jest winny i kupiła czas Karolinusowi i Strzale. | 0095-07-18 - 0095-07-20 |
| 230411-egzorcysta-z-sanktuarium     | nie jest zainteresowana pomaganiem dziecku, ale nie chce niszczyć Sanktuarium. Jednak sprawiedliwość i "swoi ludzie" muszą być uratowani. Skonfliktowana, pozwala Karolinusowi porwać Irka. Bardziej pasywna rola, nie wie co robić w zastałej sytuacji. | 0095-07-21 - 0095-07-23 |
| 230509-samszarowie-lemurczak-i-fortel-strzaly | duża wrogość do egzorcysty Irka; nie chce ocieplać stosunków. Unieruchomiła magią przekształconą przez Lemurczaka matkę nastolatków. Zmanipulowała Irka, by ten powiedział że Elena i Karolinus są po właściwej stronie i on nie był porwany tylko ich potrzebował. Dzięki temu Samszarowie wyszli na bohaterów (acz jeszcze nieudolnych bo młodych) a nie na potwory z Aurum XD. | 0095-07-24 - 0095-07-26 |
| 230516-karolinka-raciczki-zemsty-verlenow | chciała uniknąć straty twarzy, ale bardziej chciała chronić duchy przed glukszwajnem. Unikała prasy, ale rzuciła w świnkę jabłkiem. Zniszczyła drony dziennikarza "przypadkiem". | 0095-07-29 - 0095-07-31 |
| 230523-romeo-dyskretny-instalator-supreme-missionforce | przeszła przez sentisieć i wykryła obecność monterów w ciężarówce i zmiany w sentisieci (acz zaalarmowała wszystkie strony łącznie z Albertem). Bablała Albertowi kupując czas Strzale na ewakuację Mai, nawet kosztem swojej reputacji. Na końcu połączyła się z dziwnym duchem, co ją wyłączyło z akcji na moment. | 0095-08-09 - 0095-08-11 |
| 230606-piekna-diakonka-i-rytual-nirwany-koz | znalazła rytuał nirwany kóz szperając po bibliotekach, ale nie poszukała bardzo głęboko by nie musieć się przyznawać kolegom z biblioteki. Potem nauczyła Maksa tego rytuału, ale nie zadbała o dokładność - bo to i tak będzie tylko raz czy dwa razy a nie będzie się upokarzać. Nie chce patrzeć na kolesia przebranego za kozę. | 0095-08-15 - 0095-08-18 |
| 230627-ratuj-mlodziez-dla-kajrata   | ostro negocjuje z Kajratem, zapewniając m.in. wsparcie Amandy. Potem ukrywa Amandę sentisiecią, wchodzi do bazy hiperpsychotroników i wyciąga uwięzionych noktiańskich magów kombinacją magii i sentisieci. | 0095-08-20 - 0095-08-25 |
| 230711-zablokowana-sentisiec-w-krainie-makaronu | potraktowana przez Hiperpsychotroników amnestykami i nie wie co jej zrobili. Mistrzyni ciętej riposty konfliktująca się z Petrą; używając znajomego Ignatusa Blakenbauera doszła do linku eternijskiego Hiperpsychotroników. W Triticatusie skonfliktowała się z Petrą, rozwaliła jej drony, Paradoksem zablokowała sentisieć, uratowała nieszczęśnika opętanego przez makaron i ogólnia była niebezpieczna i konfliktowa. Ale skuteczna. | 0095-09-05 - 0095-09-08 |
| 230808-nauczmy-mlodego-tiena-jak-zyc | mercenary (co mam z tego mieć) a nie pomoc rodowi; opracowała (z Amandą) plan jak przerazić młodego Armina oraz sama uznała, że nie będzie współpracować do tego celu z innymi Samszarami. Zrobiła Makaronowego Potwora, zbudowała sojusz z Wiktorem Blakenbauerem (nadając mu Amandę Kajrat jako mafię za nagrodę) i nie dała Karolinusowi zwinąć do łóżka Kleopatry. Gdy pojawił się Karolinus, zaprzestała aktywnych działań. | 0095-09-14 - 0095-09-23 |
| 230905-druga-tienka-przybywa-na-pomoc | przekonała wszystkich, by jednak nie dawali jej nadmiernych niepotrzebnych rzeczy i pozytywnie podeszła do tematu 'wygnania w góry'. Ma wsparcie matki i list polecający. | 0095-10-05 - 0095-10-07 |
| 231024-rozbierany-poker-do-zwalczania-interis | bardzo kiepsko obiera ziemniaki (aż się zacięła); wykryła magicznie źródło Interis. Potem oszukiwała żołnierzy w karty, ale wprowadziła do gry Adelajdę, żeby nie byli zbyt wściekli. Pozyskała jedzenie dla Orbitera od Samszarów. | 0095-10-08 - 0095-10-10 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230404-wszystkie-duchy-siewczyna    | zniszczyła lokalnego Ducha Strażniczego Spichlerza, który istniał 80 lat. W Siewczynie jej tego nie zapomną... | 0095-07-20
| 230516-karolinka-raciczki-zemsty-verlenow | na wideo Paktu gdy zwalczali glukszwajna jako "opiekunka duchów i obrończyni ich przed świnią". Popularność wśród Paktu rośnie. | 0095-07-31
| 230523-romeo-dyskretny-instalator-supreme-missionforce | zdaniem Alberta Samszara, gdy pije to nie da się z nią dogadać i jest niezwykle irytująca. Ogólnie - zwykle niegodna uwagi. | 0095-08-11
| 230606-piekna-diakonka-i-rytual-nirwany-koz | wysłała do Vioriki informację o tym, że Romeo spieprzył operację. Romeo powiedział Viorice, że Elena S. sobie z nim nie radziła. Wniosek Verlenów: Elena jest 'słaba' i 'irytująca'. | 0095-08-18
| 230711-zablokowana-sentisiec-w-krainie-makaronu | Petra Samszar jest przekonana, że ona jest psychopatką i nie dba o nikogo. Chce to ujawnić i udowodnić. | 0095-09-08
| 230711-zablokowana-sentisiec-w-krainie-makaronu | po operacji chronienia duchów udowodniła w okolicy, że dba o duchy, ale o ludzi zupełnie nie. | 0095-09-08
| 230808-nauczmy-mlodego-tiena-jak-zyc | zbudowała silny sojusz z Wiktorem Blakenbauerem; Wiktor jest zainteresowany byciem wsparciem dla Eleny, ona jest znacząca i nie tylko w kontekście mafii | 0095-09-23
| 230808-nauczmy-mlodego-tiena-jak-zyc | Samszarowie stwierdzili, że nie dba o ród. Wykonuje polecenia, ale nie ma tam 'serca'. Zdaniem Samszarów nie zależy jej na nikim poza niej samej. | 0095-09-23
| 230905-druga-tienka-przybywa-na-pomoc | wsparcie Adelajdy, Cypriana oraz własny Chevalier. I sprzęt do przetrwania w paskudnych warunkach górskich. | 0095-10-07
| 231024-rozbierany-poker-do-zwalczania-interis | pochwała od kapitana Larnecjata, skierowana do rodu Samszar odnośnie samej Eleny | 0095-10-10
| 231024-rozbierany-poker-do-zwalczania-interis | zaakceptowana przez żołnierzy; traktowana jako 'swoja tien porucznik'. Ale - ma zakaz obierania ziemniaków i się z niej tu podśmiewują | 0095-10-10

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolinus Samszar    | 9 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230516-karolinka-raciczki-zemsty-verlenow; 230523-romeo-dyskretny-instalator-supreme-missionforce; 230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata; 230711-zablokowana-sentisiec-w-krainie-makaronu; 230808-nauczmy-mlodego-tiena-jak-zyc)) |
| AJA Szybka Strzała   | 6 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230523-romeo-dyskretny-instalator-supreme-missionforce; 230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata)) |
| Irek Kraczownik      | 4 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Adelajda Kalmiris    | 2 | ((230905-druga-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Amanda Kajrat        | 2 | ((230627-ratuj-mlodziez-dla-kajrata; 230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Cyprian Mirisztalnik | 2 | ((230905-druga-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Herbert Samszar      | 2 | ((230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata)) |
| Impresja Ignicja Incydencja Diakon | 2 | ((230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata)) |
| Mikołaj Larnecjat    | 2 | ((230905-druga-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Wacław Samszar       | 2 | ((230627-ratuj-mlodziez-dla-kajrata; 230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Wiktor Blakenbauer   | 2 | ((230711-zablokowana-sentisiec-w-krainie-makaronu; 230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Adelaida Samszar     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Albert Samszar       | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Aleksander Samszar   | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Amara Zegarzec       | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Antonina Blakenbauer | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Armin Samszar        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Arnold Kazitan       | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Celina Maskiewnik    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Dźwiedź Łagodne Słowo | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Ernest Kajrat        | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Estella Gwozdnik     | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Florian Samszar      | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Franciszek Chartowiec | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Ignatus Blakenbauer  | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Jaromir Gaburon      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Jarosław Mirelski    | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Joachim Pulkmocz     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Jonatan Lemurczak    | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Kleopatra Trusiek    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Ludmiła Zegarzec     | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Maja Samszar         | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Maks Samszar         | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Maksymilian Sforzeczok | 1 | ((230404-wszystkie-duchy-siewczyna)) |
| Maurycy Derwisz      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Mitria Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Nataniel Samszar     | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Neidria Lazvarin     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Paweł Lawarczak      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Petra Samszar        | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Robinson Porzecznik  | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Roland Samszar       | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Romeo Verlen         | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Rufus Bilgemener     | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Sara Mazirin         | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Tadeusz Dzwańczak    | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Tadeusz Maskiewnik   | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Talia Aegis          | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Tomasz Afagrel       | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Wargun Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Zenobia Samszar      | 1 | ((230905-druga-tienka-przybywa-na-pomoc)) |