---
categories: profile
factions: 
owner: public
title: Klasa Sabotażysta
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240111-zlomowanie-legendarnej-anitikaplan | zniszczyła wolne TAI na pokładzie Anitikaplan a potem skutecznie rozmontowała Anomalię Alteris-Sempitus w Krypcie, czerpiąc z energii magicznej dookoła. | 0084-04-02 - 0084-04-06 |
| 231119-tajemnicze-tunele-sebirialis | Riana Sanesset z Lux Umbrarum; doszła do tego, że stacja jest finansowo zaniedbana i z pomocą Luminariusa wbiła się do systemów stacji. Dobra w zastraszaniu, skłoniła Martę z NavirMed do współpracy. Decyduje o ewakuacji stacji. | 0104-11-20 - 0104-11-24 |
| 231213-polowanie-na-biosynty-na-szernief | wbija do pomieszczenia z Malikiem eksplozją i zatrzymuje go przed samobójstwem; gdy idzie porozmawiać z Delgado to zabija na jego oczach savaranina by zyskać zaufanie. Porywa człowieka by testować antidotum na Parasekta i odwraca uwagę od ruchów biosyntów by te mogły wejść i zniszczyć Parasekta. | 0105-07-26 - 0105-07-31 |
| 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa | określił że Aerinie nic nie groziło przy sabotażu Sii; doszedł do jej motywu. Udowodnił obecność magii przez nienaturalne sabotaże. Doszedł do roli Toma-74 i że on był przyczyną. | 0105-12-11 - 0105-12-13 |
| 231221-pan-skarpetek-i-odratowany-ogrod | porozmawiał z Ignatiusem i dowiedział się o wpływie Pana Skarpetka; potem załatwił sprawę i pozyskał dzieci ze stacji do lapisowania na Luminariusie, usuwając efemerydę Pana Skarpetka. Wyszedł na dziwnego łosia w oczach stacji, ale udało mu się usunąć Anomalię. | 0106-04-25 - 0106-04-27 |
| 240117-dla-swych-marzen-warto       | uratował Artura i ostrzelał Maię; zaproponował pomnik dla Vanessy by zintegrować biosynty z ludźmi | 0106-11-04 - 0106-11-06 |
| 240214-relikwia-z-androida          | skuteczne negocjacje z Kalistą, które odsłoniły kluczowe informacje dotyczące Elwiry i Aliny, umożliwiając Agencji głębsze zrozumienie sytuacji i dalsze kroki działania. Potem - współpraca z Inżynierem by zniszczyć H4 Hydrolab używając płomieni silników Szernief. | 0107-05-16 - 0107-05-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kalista Surilik      | 6 | ((231119-tajemnicze-tunele-sebirialis; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Felina Amatanir      | 5 | ((231119-tajemnicze-tunele-sebirialis; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto)) |
| Klasa Oficer Naukowy | 5 | ((231213-polowanie-na-biosynty-na-szernief; 231221-pan-skarpetek-i-odratowany-ogrod; 240111-zlomowanie-legendarnej-anitikaplan; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Dyplomata      | 4 | ((231119-tajemnicze-tunele-sebirialis; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240214-relikwia-z-androida)) |
| Klasa Hacker         | 4 | ((231213-polowanie-na-biosynty-na-szernief; 240111-zlomowanie-legendarnej-anitikaplan; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Mawir Hong           | 4 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Aerina Cavalis       | 3 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Inżynier       | 3 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240214-relikwia-z-androida)) |
| Vanessa d'Cavalis    | 3 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Artur Tavit          | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Ignatius Sozyliw     | 2 | ((231119-tajemnicze-tunele-sebirialis; 231221-pan-skarpetek-i-odratowany-ogrod)) |
| Klasa Biurokrata     | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240111-zlomowanie-legendarnej-anitikaplan)) |
| Larkus Talvinir      | 2 | ((231119-tajemnicze-tunele-sebirialis; 240117-dla-swych-marzen-warto)) |
| Alina Mekran         | 1 | ((240214-relikwia-z-androida)) |
| Aniela Kafantelas    | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Delgado Vitriol      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Dorion Fughar        | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Elwira Barknis       | 1 | ((240214-relikwia-z-androida)) |
| Eryk Kawanicz        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Estril Cavalis       | 1 | ((240117-dla-swych-marzen-warto)) |
| Gabriel Septenas     | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Janvir Krassus       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Kaldor Czuk          | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Leon Hurmniow        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Maia Sakiran         | 1 | ((240117-dla-swych-marzen-warto)) |
| Malik Darien         | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Marta Sarilit        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| OLU Luminarius       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Sebastian-194        | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Sia-03 Szernief      | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| SN Anitikaplan       | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| SN Varilen           | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Tadeusz Mekran       | 1 | ((240214-relikwia-z-androida)) |
| Twaróg Worl          | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Wiktor Worl          | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |