---
categories: profile
factions: 
owner: public
title: OO Karsztarin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221004-samotna-w-programie-advancer | wrak kosmiczny; stacja; jednostka treningowa dla Orbitera. | 0099-05-20 - 0099-05-29 |
| 221102-astralna-flara-i-porwanie-na-karsztarinie | jednostka treningowa, na której doszło do próby porwania wartościowego kadeta przez Syndykat Aureliona. Wprowadzili relay sygnałów + załatwili strażników. | 0100-07-28 - 0100-08-03 |
| 231115-cwiczenia-komodora-bladawira | przekształcony w 'fortecę Aureliona', zmieniony w obiekt niemożliwy do sforsowania. Bladawir nie próbował - porwał komodor Razalis inaczej. | 0110-11-08 - 0110-11-15 |
| 231220-bladawir-kontra-przemyt-tienow | na pokładzie tej niezbyt monitorowanej jednostki przemycani są tieni z Aurum, jak i Zaara umieściła potwora mającego wypłoszyć przemytników i przemycanych. Obie sprawy zniknęły (Arianna i Klaudia). | 0110-12-06 - 0110-12-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 3 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 231115-cwiczenia-komodora-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Elena Verlen         | 3 | ((221004-samotna-w-programie-advancer; 221102-astralna-flara-i-porwanie-na-karsztarinie; 231220-bladawir-kontra-przemyt-tienow)) |
| Antoni Bladawir      | 2 | ((231115-cwiczenia-komodora-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Ewa Razalis          | 2 | ((231115-cwiczenia-komodora-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Klaudia Stryk        | 2 | ((231115-cwiczenia-komodora-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Ada Wyrocznik        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Alezja Dumorin       | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Arnulf Perikas       | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Daria Czarnewik      | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Dorota Radraszew     | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Eustachy Korkoran    | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Gerwazy Kircznik     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Grażyna Burgacz      | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Hubert Kerwelenios   | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Hubert Mirsz         | 1 | ((221004-samotna-w-programie-advancer)) |
| Izabela Zarantel     | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Kacper Wentel        | 1 | ((221004-samotna-w-programie-advancer)) |
| Kajetan Kircznik     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Kazimierz Darbik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Klarysa Jirnik       | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Lara Kiriczko        | 1 | ((221004-samotna-w-programie-advancer)) |
| Leszek Kurzmin       | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Maja Samszar         | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Marcinozaur Verlen   | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Mariusz Bulterier    | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Michał Warkoczak     | 1 | ((221004-samotna-w-programie-advancer)) |
| OO Infernia          | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Paprykowiec       | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Raoul Lavanis        | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Sandra Kantarelo     | 1 | ((221004-samotna-w-programie-advancer)) |
| Szczepan Myrczek     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Szymon Orzesznik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Tymon Krakdacz       | 1 | ((221004-samotna-w-programie-advancer)) |
| Władawiec Diakon     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Zaara Mieralit       | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |