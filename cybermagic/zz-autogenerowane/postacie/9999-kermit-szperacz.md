---
categories: profile
factions: 
owner: public
title: Kermit Szperacz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190226-korporacyjna-wojna-w-mmo     | aka "Sekator Pierwszy", tymczasowy lider EliSquid. Mieszka w Cieniaszczycie. Okazuje się że na usługach Elizy Iry; chciał doprowadzić EliSquid do wielkości. Niestety, też zabanowany. | 0110-03-14 - 0110-03-17 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190226-korporacyjna-wojna-w-mmo     | został zabanowany przez działania Mi Rudej. Nie dowodzi już EliSquid. | 0110-03-17

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adrian Wężoskór      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Alan Bartozol        | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Antoni Kotomin       | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Dariusz Bankierz     | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Eliza Kotlet         | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Mi Ruda              | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Rafał Królewski      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Wojciech Słabizna    | 1 | ((190226-korporacyjna-wojna-w-mmo)) |