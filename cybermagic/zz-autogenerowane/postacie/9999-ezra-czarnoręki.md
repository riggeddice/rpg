---
categories: profile
factions: 
owner: public
title: Ezra Czarnoręki
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191115-tajemnica-wyspy-rafanna      | wachtmistrz na Syrenie; doprowadził do tego, że zamiast opuścić Wyspę Rafanna Zespół próbował przejąć nad nią kontrolę. Bezwzględny ale lojalny. | 0109-09-06 - 0109-09-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Andrij Zarębski      | 1 | ((191115-tajemnica-wyspy-rafanna)) |
| Karol Rotmistrz      | 1 | ((191115-tajemnica-wyspy-rafanna)) |
| Szczepan Kazitan     | 1 | ((191115-tajemnica-wyspy-rafanna)) |