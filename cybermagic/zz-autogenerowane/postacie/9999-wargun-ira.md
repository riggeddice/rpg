---
categories: profile
factions: 
owner: public
title: Wargun Ira
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230627-ratuj-mlodziez-dla-kajrata   | noktianin, 19 lat, technomanta; uratowany przez Zespół. | 0095-08-20 - 0095-08-25 |
| 190626-upadek-enklawy-floris        | nie pamięta już do końca kim jest, ale ten mag rodu Ira jest Skażeńcem zdolnym do częściowego kontrolowania fabrykatora Maszyny. Powiązany z Kajratem i Floris. | 0110-05-28 - 0110-05-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ernest Kajrat        | 2 | ((190626-upadek-enklawy-floris; 230627-ratuj-mlodziez-dla-kajrata)) |
| AJA Szybka Strzała   | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Amanda Kajrat        | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Ariela Sirmin        | 1 | ((190626-upadek-enklawy-floris)) |
| Elena Samszar        | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Herbert Samszar      | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Hubert Kraborów      | 1 | ((190626-upadek-enklawy-floris)) |
| Impresja Ignicja Incydencja Diakon | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Jolanta Teresis      | 1 | ((190626-upadek-enklawy-floris)) |
| Karolinus Samszar    | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Konrad Czukajczek    | 1 | ((190626-upadek-enklawy-floris)) |
| Marcel Sowiński      | 1 | ((190626-upadek-enklawy-floris)) |
| Mitria Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Nikola Kirys         | 1 | ((190626-upadek-enklawy-floris)) |
| Roman Rymtusz        | 1 | ((190626-upadek-enklawy-floris)) |
| Szymon Maszczor      | 1 | ((190626-upadek-enklawy-floris)) |
| Talia Aegis          | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Wacław Samszar       | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |