---
categories: profile
factions: 
owner: public
title: SCA Isigtand
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221116-astralna-flara-dociera-do-nonariona-nadziei | bardzo ciężko uszkodzony trzyosobowy grazer; nie do naprawienia. Zestrzelony przez anomalną asteroidę grawitacyjną. 3 osoby. | 0100-09-08 - 0100-09-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Alezja Dumorin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Arianna Verlen       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Arnulf Perikas       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Daria Czarnewik      | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Gabriel Lodowiec     | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Grażyna Burgacz      | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Kajetan Kircznik     | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Leo Kasztop          | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Leszek Kurzmin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Maja Samszar         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Marcel Kulgard       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| OO Astralna Flara    | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| OO Athamarein        | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| SCA Hadiah Emas      | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Szymon Wanad         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Tomasz Ruppok        | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Władawiec Diakon     | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |