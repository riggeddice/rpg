---
categories: profile
factions: 
owner: public
title: Paweł Kukułnik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191108-ukojenie-aleksandrii         | lokalny zniewolony przez Aleksandrię terminus. Pokonany przez Leszka wpakował do Aleksandrii własną żonę. Nie może cierpieć. | 0110-07-18 - 0110-07-20 |
| 191113-jeden-problem-dwie-rodziny   | stoczył kilka bitew z Potworem. Szczególnie wrażliwy na energię Esuriit; czyżby była sprzeczna z technologią Kuratorów? | 0110-09-30 - 0110-10-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Mirzant       | 2 | ((191108-ukojenie-aleksandrii; 191113-jeden-problem-dwie-rodziny)) |
| Klara Baszcz         | 2 | ((191108-ukojenie-aleksandrii; 191113-jeden-problem-dwie-rodziny)) |
| Leszek Baszcz        | 2 | ((191108-ukojenie-aleksandrii; 191113-jeden-problem-dwie-rodziny)) |
| Daniela Baszcz       | 1 | ((191108-ukojenie-aleksandrii)) |
| Kamil Lemurczak      | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Karol Kszatniak      | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Kinga Stryk          | 1 | ((191108-ukojenie-aleksandrii)) |
| Sabina Kazitan       | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Teresa Marszalnik    | 1 | ((191113-jeden-problem-dwie-rodziny)) |