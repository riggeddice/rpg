---
categories: profile
factions: 
owner: public
title: Jasper Arimetus
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240313-ostatni-lot-valuvanok        | naukowiec Valuvanok i mag który stracił oczy; widząc katastrofalne rozprzestrzenianie Plagi zdecydował się zniszczyć Valuvanok by nikt inny nie ucierpiał. Zainicjował sabotaż i zniszczenie statku, ale nie był w stanie tego dokończyć. Pomógł TAI zrozumieć jak zła jest sytuacja. | 0082-11-28 - 0082-11-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ewelina Kalwiert     | 1 | ((240313-ostatni-lot-valuvanok)) |
| Mara Arimetus        | 1 | ((240313-ostatni-lot-valuvanok)) |
| OnS Valuvanok        | 1 | ((240313-ostatni-lot-valuvanok)) |
| Xavier Kalwiert      | 1 | ((240313-ostatni-lot-valuvanok)) |