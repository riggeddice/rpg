---
categories: profile
factions: 
owner: public
title: Leo Mikirnik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230530-ziarno-kuratorow-na-karnaxianie | tymczasowo zamiast Helmuta na Serbiniusie; świetny EVA (Extra-Vehicular Activity), mechanik i żołnierz, (41); bardzo doświadczony żołnierz, skutecznie manewrował samobójczymi dronami i ręcznie przejął _point defence_ Serbiniusa by zniszczyć Ziarna Kuratora; potem wbił się na przyczółek w Karnaxianie a gdy się okazało że Kurator jest za mocny, ogłosił ewakuację. Doskonale przeprowadził manewry taktyczne. | 0109-10-06 - 0109-10-07 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230530-ziarno-kuratorow-na-karnaxianie | niezadowolenie ze strony Fabiana Korneliusza; zdaniem Fabiana Leo posuwa się zdecydowanie za daleko w ataku na SC Karnaxian. Zdaniem Fabiana Leo nie dba o ludzi. | 0109-10-07

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazy Termann     | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Fabian Korneliusz    | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Helmut Szczypacz     | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Klaudia Stryk        | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Kurator Sarkamair    | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Martyn Hiwasser      | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| OO Serbinius         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| SC Karnaxian         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |