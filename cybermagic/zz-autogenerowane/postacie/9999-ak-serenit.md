---
categories: profile
factions: 
owner: public
title: AK Serenit
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190802-statek-zjada-statki          | statek-asteroida (RAMA), kontrolowany przez saitaero-kralotycznego Overminda, asymilujący statki i integrujący ze sobą magów z AI. Skrajnie niebezpieczny. | 0087-08-09 - 0087-08-12 |
| 210428-infekcja-serenit             | leci w kierunku na OE Falołamacz (który staje się "odłamkiem Serenit"). | 0111-11-22 - 0111-11-23 |
| 210512-ewakuacja-z-serenit          | Klaudia odwróciła jego uwagę co kupiło czas. Pozyskał Falołamacz jako jeden ze swoich statków. Sposób infekcji: zastępuje advancera swoim konstruktem i wprowadza Serenit na statek macierzysty. | 0111-11-23 - 0111-12-02 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210512-ewakuacja-z-serenit          | WIE o istnieniu Eleny Verlen i o Inferni. Jeśli jest gdzieś w pobliżu, może na nich zapolować. | 0111-12-02

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aida Serenit         | 2 | ((190802-statek-zjada-statki; 210512-ewakuacja-z-serenit)) |
| Arianna Verlen       | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Elena Verlen         | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Eustachy Korkoran    | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Klaudia Stryk        | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Martyn Hiwasser      | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| OE Falołamacz        | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| OO Infernia          | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Bogdan Anatael       | 1 | ((210512-ewakuacja-z-serenit)) |
| Eva d'Mikado         | 1 | ((190802-statek-zjada-statki)) |
| Ignaś Orbita         | 1 | ((190802-statek-zjada-statki)) |
| Laura Prunal         | 1 | ((190802-statek-zjada-statki)) |
| Michał Dusiciel      | 1 | ((190802-statek-zjada-statki)) |
| OE Piękna Elena      | 1 | ((210428-infekcja-serenit)) |
| Persefona d'Infernia | 1 | ((210428-infekcja-serenit)) |
| Rafał Grambucz       | 1 | ((210428-infekcja-serenit)) |
| Roland Sowiński      | 1 | ((210512-ewakuacja-z-serenit)) |
| SC Mikado            | 1 | ((190802-statek-zjada-statki)) |
| Sia d'Cranis         | 1 | ((190802-statek-zjada-statki)) |
| Stella Koral         | 1 | ((190802-statek-zjada-statki)) |
| Tadeusz Ursus        | 1 | ((210428-infekcja-serenit)) |
| Travis Longhorn      | 1 | ((190802-statek-zjada-statki)) |