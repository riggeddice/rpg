---
categories: profile
factions: 
owner: public
title: Aulus Terrentus
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230923-ciemnosc-pozerajaca-arcadalian | kapitan Ferrivata; BARDZO za ratowaniem swoich ludzi, zwłaszcza, że w Umbrze jest jego krewny. Porywczy, ale kompetentny i współpracuje z Agencją. Salma powstrzymała go przed wejściem w Umbrę i potencjalną śmiercią. | 0091-07-30 - 0091-08-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Daven Hassik         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Iwo Bretonis         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Lester Martz         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Livia Sertiano       | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| OLU Luminarius       | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Salma Bluszcz        | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| SN Arcadalian        | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| SN Ferrivat          | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| SN Murisatia         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |