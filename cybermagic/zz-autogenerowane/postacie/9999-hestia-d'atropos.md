---
categories: profile
factions: 
owner: public
title: Hestia d'Atropos
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220610-ratujemy-porywaczy-eleny     | opiekuńcza TAI transcendent+1 która paliła własne banki danych by móc komuś powiedzieć że przełożony Atropos współpracuje z Syndykatem. Ryzykowała życiem, ale Klaudia ją zregenerowała. VERY vindictive towards those who hurt her charges. | 0112-09-15 - 0112-09-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Elena Verlen         | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Eustachy Korkoran    | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Jakub Bulgocz        | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Klaudia Stryk        | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Leona Astrienko      | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Martyn Hiwasser      | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Raoul Lavanis        | 1 | ((220610-ratujemy-porywaczy-eleny)) |