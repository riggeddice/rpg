---
categories: profile
factions: 
owner: public
title: Pola Mornak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220420-samobojstwo-kapitana-wielkiego-weza | badaczka szukająca wśród Strachów iskry bogów; uważa kill agenta za coś więcej i wierzy w plotki o dziwnych rzeczach tu, w Morzu Ułudy. Twardo stoi za Kornelią i chce ją pozycjonować na kapitana. | 0108-09-26 - 0108-10-02 |
| 220427-dziwne-strachy-w-morzu-ulud  | stoi silnie we frakcji pro-Kornelii. Doszła do tego jak działają Strachy i jak użyć Berdysza by dało się ominąć Strachy. Niezły naukowiec. | 0108-10-05 - 0108-10-07 |
| 220518-okrutna-wrona-kalcynici-i-koszmary | naukowiec; świetnie doszła do tego mniej więcej czym są kalcynici / głębniaki i korelację z Elwirą Piscernik. Została solidnie ranna, ale aktywna. | 0108-10-09 - 0108-10-18 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220518-okrutna-wrona-kalcynici-i-koszmary | obsesja na punkcie PRAWDY. Co tu się dzieje? Czym są głębniaki / kalcynici? Musi się dowiedzieć czym jest ryzyko i na czym polega. | 0108-10-18

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Falkam          | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Antoni Krutacz       | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Berdysz Rozdzieracz  | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Filip Gościc         | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Kornelia Sanoros     | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Lila Cziras          | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Maja Kormoran        | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Amanda Korel         | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Elwira Piscernik     | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Jerzy Odmiczak       | 1 | ((220420-samobojstwo-kapitana-wielkiego-weza)) |
| Ksawery Janowar      | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Mikołaj Faczon       | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |