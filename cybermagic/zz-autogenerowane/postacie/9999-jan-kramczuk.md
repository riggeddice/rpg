---
categories: profile
factions: 
owner: public
title: Jan Kramczuk
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190102-stalker-i-czerwone-myszy     | infiltrator; wszedł do mieszkania Pięknotki niezauważony i ją obudził by przepytać. Reporter, stowarzyszony z Dare Shiver. Potem łaził z Pięknotką po Trzęsawisku i spojrzał w Toń. | 0109-12-18 - 0109-12-21 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190102-stalker-i-czerwone-myszy     | zobaczył w Toni coś, co dało mu zahaczkę do nowej historii, nieprawdopodobnej. I takiej, co wpędzi go w kłopoty. | 0109-12-21

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 1 | ((190102-stalker-i-czerwone-myszy)) |
| Teresa Mieralit      | 1 | ((190102-stalker-i-czerwone-myszy)) |
| Waldemar Mózg        | 1 | ((190102-stalker-i-czerwone-myszy)) |