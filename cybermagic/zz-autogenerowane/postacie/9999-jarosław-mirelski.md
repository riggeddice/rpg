---
categories: profile
factions: 
owner: public
title: Jarosław Mirelski
---

# {{ page.title }}


# Generated: 



## Fiszki


* szeregowy, ekspert od walki w zwarciu; wygląda jak kupa mięśni o świńskich oczkach | @ 231024-rozbierany-poker-do-zwalczania-interis
* OCEAN: (E+C-): skłonny do ryzyka, łatwo się denerwuje, impulsywny, ale też bardzo lojalny. "Z Jarkiem lepiej nie wchodzić w konflikt, chyba że masz ochotę na siniaka." | @ 231024-rozbierany-poker-do-zwalczania-interis
* VALS: (Achievement, Tradition): duma z własnych umiejętności w walce. Tradycyjnie patrzy na honor i wyzwania. "Prawdziwi faceci rozwiązują problemy walką, a ich przełożeni ich uruchamiają" | @ 231024-rozbierany-poker-do-zwalczania-interis
* Core Wound - Lie: "W wyniku scamu moja rodzina została bez niczego i tylko wojsko było dla mnie nadzieją" - "Pokaż, że warto na ciebie stawiać, bądź świetnym żołnierzem." | @ 231024-rozbierany-poker-do-zwalczania-interis
* Styl: Krótki temperament, zawsze gotów do wyzwania. Przeciwników mierzy wzrokiem, ale też ma w sobie pewien urok, który przyciąga innych. Przyjazny dla "plebsu". | @ 231024-rozbierany-poker-do-zwalczania-interis
* metakultura: Atarien: "Ludzie są ludźmi. Trzymamy się razem. Każdemu normalsowi warto pomóc." | @ 231024-rozbierany-poker-do-zwalczania-interis

### Wątki


salvagerzy-lohalian
rehabilitacja-eleny-samszar

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231024-rozbierany-poker-do-zwalczania-interis | nie lubi tienek, bardziej lubi oglądać Adelajdę. Niechętnie mówi, nie wchodzi w interakcję szczególnie. Grał w karty i swoją obecnością psuł nastrój przy stole ;-). | 0095-10-08 - 0095-10-10 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adelajda Kalmiris    | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Cyprian Mirisztalnik | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Elena Samszar        | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Estella Gwozdnik     | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Jaromir Gaburon      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Maurycy Derwisz      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Mikołaj Larnecjat    | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Paweł Lawarczak      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Tomasz Afagrel       | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |