---
categories: profile
factions: 
owner: public
title: Tadeusz Sklerzec
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191112-korupcja-z-arystokratki      | burmistrz Jastrząbca. Gość chciał lepiej dla miasta, więc poszedł na nieetyczny eksperyment arystokratki; dostał wpiernicz od Pięknotki. | 0110-07-04 - 0110-07-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ataienne             | 1 | ((191112-korupcja-z-arystokratki)) |
| Ignacy Myrczek       | 1 | ((191112-korupcja-z-arystokratki)) |
| Mariusz Trzewń       | 1 | ((191112-korupcja-z-arystokratki)) |
| Minerwa Metalia      | 1 | ((191112-korupcja-z-arystokratki)) |
| Persefona d'Jastrząbiec | 1 | ((191112-korupcja-z-arystokratki)) |
| Pięknotka Diakon     | 1 | ((191112-korupcja-z-arystokratki)) |
| Sabina Kazitan       | 1 | ((191112-korupcja-z-arystokratki)) |