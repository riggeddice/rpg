---
categories: profile
factions: 
owner: public
title: Mara Arimetus
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240313-ostatni-lot-valuvanok        | biomantka Valuvanok, która pragnie przetrwać; przez cały czas dążyła do znalezienia lekarstwa na Plagę; z uwagi na arogancję młodości sama się zaraziła i stała się instrumentem destrukcji Valuvanok. Powstrzymana przez Zespół. | 0082-11-28 - 0082-11-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ewelina Kalwiert     | 1 | ((240313-ostatni-lot-valuvanok)) |
| Jasper Arimetus      | 1 | ((240313-ostatni-lot-valuvanok)) |
| OnS Valuvanok        | 1 | ((240313-ostatni-lot-valuvanok)) |
| Xavier Kalwiert      | 1 | ((240313-ostatni-lot-valuvanok)) |