---
categories: profile
factions: 
owner: public
title: Marlena Maja Leszczyńska
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "ambitne orzeszki"
* type: "NPC"
* owner: "public"
* title: "Marlena Maja Leszczyńska"


## Postać

### Ogólny pomysł (2)

Młoda czarodziejka (19) o niesamowitym poczuciu humoru, zwana Wiewiórką. Miłośniczka Supreme Missionforce i przełożona gildii "Ambitne Orzeszki". Bardzo zwinna i świetnie radzi sobie z komputerami. Wybitny taktyk i strateg.

### Co motywuje postać (2)

* Supreme Missionforce nie jest poważaną grą -> by turnieje w Supreme Missionforce były esportem podziwianym i obserwowanym na całym świecie
* świat nie jest sprawiedliwym miejscem -> każdemu wedle potrzeb i zasług, zawsze i tylko gra fair
* bardzo wesoła i energiczna, wszędzie pierwsza, pomocna i uczynna

### Idealne okoliczności (2)

* gra w Supreme Missionforce, najlepiej tam, gdzie potrzebny jest największy refleks i jest najtrudniej
* musi szybko się gdzieś wkraść czy przedostać - najlepiej, jeśli wymaga to utrzymania równowagi ;-)
* trzeba kogoś przechytrzyć i sytuacja ma w miarę znane zasoby - jeśli to się da sprowadzić do "taktyki" to ona sobie poradzi

### Szczególne umiejętności (3)

* rozładowywanie atmosfery: urocza młoda dama z doskonałymi żartami i naturalnym darem do mediacji i przewodzenia innym
* zwinna jak wiewiórka, świetnie się wspina i lubi to robić. Bardzo szybka.
* taktyk w SupMis, świetna w kompozycji oddziałów, micro- i macro.
* przełożona gildii Ambitne Orzeszki. Rozwiązuje problemy, organizuje działania dookoła idei itp.
* świetnie przeszukuje informacje po internecie i rozwiązuje problemy tego typu.

### Zasoby i otoczenie (3)

* członkowie Ambitnych Orzeszków, na wielu światach; też osoby lubiące SupMis znają "CuteSquirrel".
* niezły komputer; zarówno do grania jak i do hackowania

### Magia (3)

#### Gdy kontroluje energię

* przeszukiwanie informacji w internecie: infomancja rozumiana jako znajdowanie rzeczy które nie pasują
* szczególnie wyczulona na znalezienie oszustw w grach komputerowych; sama jest w stanie oszukiwać i wpływać na internet
* inkarnacja; jest w stanie "opętać" byt spirytystycznie i kontrolować bezpośrednio jak awatar. Forma golemancji.

#### Gdy traci kontrolę

* komputery zaczynają zachowywać się bardzo dziwnie; w skrajnych wypadkach zaczynają walczyć ze sobą
* może dojść do spontanicznych inkarnacji różnych osób w różne byty
* summon squirrels. Serio. Mogą się pojawić, może być bardzo... wiewiórkowato.

### Powiązane frakcje

{{ page.factions }}

## Opis

(20 min)


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181101-wojna-o-uczciwe-polfinaly    | próbowała sama rozwiązać problem Skażenia i yyizdisa, ale szczęśliwie dla siebie miała Skażenie i sprawą zajęła się Pięknotka której wyszło. | 0109-10-17 - 0109-10-19 |
| 181104-kotlina-duchow               | wdzięczna Pięknotce za pomoc Zaczęstwiakom pomogła jej znaleźć lepszy teren niż Zaczęstwo. Znalazła jej Podwiert i skontaktowała z Kiryłem. | 0109-10-22 - 0109-10-24 |
| 190206-nie-da-sie-odrzucic-mocy     | obiecała, że przybędzie do Zaczęstwa by pomóc Karolinie Erenit i by ta nie była sama. | 0110-02-17 - 0110-02-20 |
| 190217-chevaleresse                 | cel ataku Diany (Chevaleresse). Doszła do tego, że jest powiązana z zabanowaniem Alana. Przyznała się i przeprosiła. Teraz Alan jest na nią zły. | 0110-02-25 - 0110-02-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 4 | ((181101-wojna-o-uczciwe-polfinaly; 181104-kotlina-duchow; 190206-nie-da-sie-odrzucic-mocy; 190217-chevaleresse)) |
| Karolina Erenit      | 3 | ((181101-wojna-o-uczciwe-polfinaly; 190206-nie-da-sie-odrzucic-mocy; 190217-chevaleresse)) |
| Tymon Grubosz        | 3 | ((181101-wojna-o-uczciwe-polfinaly; 190206-nie-da-sie-odrzucic-mocy; 190217-chevaleresse)) |
| Alan Bartozol        | 2 | ((181101-wojna-o-uczciwe-polfinaly; 190217-chevaleresse)) |
| Arnulf Poważny       | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Damian Podpalnik     | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
| Diana Tevalier       | 1 | ((190217-chevaleresse)) |
| Kasjopea Maus        | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Kirył Najłalmin      | 1 | ((181104-kotlina-duchow)) |
| Kornel Garn          | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Minerwa Metalia      | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Olga Myszeczka       | 1 | ((181104-kotlina-duchow)) |
| Tadeusz Kruszawiecki | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
| Wawrzyn Towarzowski  | 1 | ((181104-kotlina-duchow)) |