---
categories: profile
factions: 
owner: public
title: Pięknotka Diakon
---

# {{ page.title }}


# Read: 

## Metadane

* factions: ""
* owner: "public"
* title: "Pięknotka Diakon"


## Kim jest

### Motto

"Każdy problem wymaga innego podejścia. Z każdym sobie poradzę."

### Paradoksalny Koncept

Świetna terminuska, która chciałaby być kosmetyczką, z upodobaniem łażąca po trzęsawisku Zjawosztup.

## Mechanika

Terminus chemiczny: 
Paladyn: ochroni niewinnych za wszelką cenę, ukarze nadużywających siły i władzy
Trzęsawisko: czuje się w nim jak w domu, zna ścieżki i trucizny
Dotyk Saitaera: 



### Czym osiąga sukcesy (3)

* ATUT: Biochemia - kosmetyki i narkotyki.
* ATUT: Wykorzystanie i manipulacja terenem i sytuacją, by działały na jej korzyść.
* SŁABA: Na wyżynach społecznych - źle się czuje, nie wie, co robić.
* SŁABA: Jest taktykiem, ale nie strategiem, fatalna w administracji.

### O co walczy (3)

* ZA: Harmonia i porządek. Każdy ma prawo do swojej wersji, ale jakaś musi być.
* ZA: Piękno jest ważne. Jeśli odrzucimy je na rzecz przetrwania to nie przetrwamy.
* VS: Zwalcza wszelkie formy nadużywania siły i władzy.
* VS: Mściwa. Jeśli skrzywdzisz ją lub jej przyjaciół, oglądaj się za siebie.

### Znaczące Czyny (3)

* Uśpiła Finis Vitae, łącząc moc Arazille i Saitaera, osobiście wchodząc w paszczę lwa.
* Opanowała Cienia siłą woli i ogromnymi stratami we krwi własnej.
* Trochę oswoiła Chevaleresse i wpakowała ją Alanowi.
* Przetrwała w pojedynku snajperskim z Walerią oraz przetrwała atak z zaskoczenia Kirasjerki.

### Kluczowe Zasoby (3)

* COŚ: Cień, niezwykły pancerz łączący Ixion z Esuriit.
* KTOŚ: Sieć wsparcia terminusów Szczelińca.
* WIEM: Znam teren, okolicę, trzęsawisko.
* OPINIA: W obronie Pustogoru posunie się bardzo daleko. Bezwzględna.

## Inne

### Wygląd

Niezbyt ładna jak na Diakonkę. A bit on the sporty side, widać, że potrafi walczyć.
Porusza się miękko i płynnie.
Lekko zmęczone oczy, które widziały wiele walk.

### Coś Więcej


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180814-trufle-z-kosmosu             | wyciągnęła Kiryła na pożądanie z jego ufortyfikowanego pokoju prosto na jego własną minę. | 0109-08-30 - 0109-09-01 |
| 180815-komary-i-kosmetyki           | próbowała dojść do tego skąd pojawiły się magiczne komary i znalazła Adelę. Adela nie chciała po dobroci - więc poprosiła, by Erwin rozwiązał dla niej problem. | 0109-09-02 - 0109-09-05 |
| 180821-programista-mimo-woli        | wraz z Bronką rozwiązały problem efemerydy; mentalnie zmusiła dyrektora szkoły by ten pozwolił dziecku rozwijać rysowanie (jak Staś chciał) | 0109-09-06 - 0109-09-07 |
| 180817-protomag-z-trzesawisk        | neutralizowała Czerwone Myszy dając czas Miedwiedowi, potem użyła kruczków prawnych i Ateny by ich całkowicie wyrolować. | 0109-09-07 - 0109-09-09 |
| 180906-nikt-nie-spi-w-swoim-lozku   | wyciągnęła prawdę z Grażyny i Szczepana, uspokoiła Brygidę a potem ducha Wąsacza i przekonała ducha, by dał się zapuszkować Mieszkowi. | 0109-09-09 - 0109-09-11 |
| 180912-inkarnata-nienawisci         | przede wszystkim, kontrolowała Atenę i dowiedziała się mnóstwo zaskakujących rzeczy na jej temat. Trochę się zaprzyjaźniła z Ateną. Trochę. | 0109-09-14 - 0109-09-15 |
| 180929-dwa-tygodnie-szkoly          | współpracowała z Ateną i Erwinem nad odzyskaniem Nutki i nad unieczynnieniem kasyna. | 0109-09-17 - 0109-09-19 |
| 181003-lilia-na-trzesawisku         | rozpaczliwie łagodzi starcie między Nutką i Lilią; też, główny przewodnik po Trzęsawisku. | 0109-09-20 - 0109-09-22 |
| 181021-powrot-minerwy-z-terrorforma | przekonywała, knuła, robiła administrację, wciągnęła Atenę... i na końcu jako jedyna jest tylko lekko ranna. | 0109-09-24 - 0109-09-29 |
| 181024-decyzja-minerwy              | oswoiła Minerwę oraz przekonała Adama, by dał jej szansę. Doprowadziła do tego, że Minerwa zdecydowała się zostać. | 0109-10-01 - 0109-10-04 |
| 181027-terminuska-czy-kosmetyczka   | trudny wybór - terminuska czy kosmetyczka? Pomóc Atenie czy wygrać z Adelą? Uniosła się kompetencjami i wygrała oba, pokazując poziom mistrzowski. Wielki sukces. | 0109-10-07 - 0109-10-11 |
| 181205-jeden-dokument-nie-w-pore    | terminuska która coraz mniej kocha Zaczęstwo; tym razem golem z papierów z księgowości i czarodziej któremu w życiu nic nie wyszło | 0109-10-12 - 0109-10-13 |
| 181101-wojna-o-uczciwe-polfinaly    | knuje, obrabia tyłek Alanowi i trochę wbrew sobie pomaga Marlenie w tych dziwnych grach online, choć zupełnie jej nie rozumie | 0109-10-17 - 0109-10-19 |
| 181114-neutralizacja-artylerii-koszmarow | Wiktor Satarail miał wszystkie możliwe przewagi, więc "dała mu wygrać" - a potem go uwiodła i zneutralizowała koszmarną moc jego artylerii słodyczą. | 0109-10-20 - 0109-10-21 |
| 181104-kotlina-duchow               | chcąc się wyplątać z Zaczęstwa wpadła w świeżo formującą się Efemerydę Kotliny niezauważalną dla Epirjonu. Kupiła z Olgą czas by terminusi uratowali sytuację. | 0109-10-22 - 0109-10-24 |
| 181112-odklatwianie-ateny           | skłonna uratować Atenę za wszelką cenę - podpadając lekarzom, współpracując z kralothami, nawet z terrorformem. Przechytrzyła wszystkich, wygrała zdrowie Ateny. | 0109-10-23 - 0109-10-26 |
| 181118-romuald-i-julia              | chciała zakosztować rozkoszy w Cieniaszczycie, skończyła w szpitalu dwa razy i pokonała morderczo niebezpieczną Emulator. Taki los. | 0109-10-27 - 0109-10-29 |
| 181125-swaty-w-cieniu-potwora       | dowiedziała się o dziwnych nojrepach, dała radę stawić czoło potwornemu Moktarowi oraz pomogła Wioletcie wygrać taniec z szablami. Potem - poderwała Wiolettę. | 0109-10-29 - 0109-11-01 |
| 181209-kajdan-na-moktara            | pokonała w pojedynku snajperskim Walerię, uzyskała dowód mogący pogrążyć Moktara oraz wyszła na zero - nie ma groźnych wrogów. Pełen sukces. | 0109-11-02 - 0109-11-03 |
| 181216-wolna-od-terrorforma         | całkowicie przebudowana przez kralothy oraz Bogdana. Obudziły się w niej diakońskie instynkty i stała się jeszcze groźniejsza i piękniejsza. Praktycznie - nowy początek. | 0109-11-05 - 0109-11-11 |
| 181218-tajemniczy-oltarz-moktara    | dowiedziała się o relacjach między Moktarem, Saitaerem i Arazille (przynajmniej częściowo). Wpadła we współzależność z Bogdanem. Jest szczęśliwa. | 0109-11-11 - 0109-11-13 |
| 181225-czyszczenie-toksycznych-zwiazkow | spiknęła ze sobą dwie pary, zapewniła Atenie źródło informacji, ściągnęła Erwina do detoksu od Saitaera i została pokonana przez Moktara gdy broniła Bogdana. | 0109-11-13 - 0109-11-17 |
| 181226-finis-vitae                  | uratowała Dariusza z macek autowara. Przeszła przez piekło robiąc straszne rzeczy. Celuje w uratowanie Lilii. Sama się nie poznaje. | 0109-11-18 - 0109-11-27 |
| 181227-adieu-cieniaszczycie         | wyprostowała wszystkie tematy w Cieniaszczycie, uwolniła się od długu Arazille, zdobyła ciało dla Minerwy i poderwała skutecznie Erwina. | 0109-11-28 - 0109-12-01 |
| 181230-uwiezienie-saitaera          | podjęła bardzo trudną decyzję - sprawa Saitaera przekracza jej możliwości i kompetencje, patrząc na biedną Minerwę. Oddała temat Karli i Pustogorowi. | 0109-12-07 - 0109-12-09 |
| 190101-morderczyni-jednej-plotki    | gdy uderzono w reputację jej salonu i w jej pracowników, zrobiła kontratak, pokazówkę, i zniszczyła wszelkie plotki na swój temat. Strachem. | 0109-12-13 - 0109-12-17 |
| 190102-stalker-i-czerwone-myszy     | wyplątała się ze wszelkich wymagań terenowych. Potem zaprosiła stalkera (Jana) na Trzęsawisko i spojrzała w Toń, odzyskując kontakt z Saitaerem. | 0109-12-18 - 0109-12-21 |
| 190105-turysci-na-trzesawisku       | uratowała grupę turystów z Alanem, pokazała swoje zaprzyjaźnione wiły i skończyła przelatując Alana. O tym nie rozmawiają i nikomu nie mówią. | 0109-12-22 - 0109-12-24 |
| 190106-a-moze-pustogorska-mafia     | lawirowała pomiędzy stronnictwami politycznymi - Adela, Kasjan, terminusi, sponsorzy. Bardzo dużo załatwiania i negocjacji. Ale udało jej się odepchnąć Adelę i Kasjana od mafii pustogorskiej. | 0109-12-24 - 0109-12-26 |
| 190112-eksperymenty-na-wilach       | poszła tropem przerzucanych w dziwne miejsce wił a skończyła na modlitwie do Saitaera, by tylko rekonstruowana wiła nie zabiła wszystkich... | 0109-12-28 - 0109-12-31 |
| 190113-chronmy-karoline-przed-uczniami | poszła do Szkoły Magów zająć się papierkową robotą, skończyła rozwiązując małą Plagę sprowadzoną przypadkowo przez Adelę - przy okazji poznała kilku uczniów. | 0110-01-05 - 0110-01-06 |
| 190116-wypalenie-saitaera-z-trzesawiska | eskortowała (niechętnie) cywili na Trzęsawisko by zniszczyć wpływ Saitaera. Udało jej się - dodatkowo rozproszyła wpływ Saitaera na Wiktorze. Uległa transformacji Saitaera (znowu). | 0110-01-07 - 0110-01-09 |
| 190119-skorpipedy-krolewskiego-xirathira | zdecydowała się uratować Myszy przed nimi samymi - odkryła, że Olga przyjaźni się z Wiktorem i że Wiktor zdecydował się skorpipedami zrobić problemy harrassującym Olgę Myszom. | 0110-01-14 - 0110-01-15 |
| 190827-rozpaczliwe-ratowanie-bii    | nie uwierzyła w winę Wiktora Sataraila; zlokalizowała, że to wina BIA i przesłuchała Talię. Potem złapała Kajrata (serio) i załatwiła z Wiktorem Satarailem, że BIA będzie przezeń zniszczona. | 0110-01-18 - 0110-01-21 |
| 190120-nowa-minerwa-w-nowym-swiecie | rozpaczliwie próbuje poprawić samopoczucie Minerwy, żonglować polityką i Karlą oraz nie stracić do Minerwy Erwina. | 0110-01-22 - 0110-01-26 |
| 190127-ixionski-transorganik        | wezwana przez Saitaera do odwrócenia ixiońskiej transorganizacji, stoczyła ciężką walkę z terrorformem (Wojtkiem). Z pomocą Wiktora, odwróciła to. | 0110-01-29 - 0110-01-30 |
| 190202-czarodziejka-z-woli-saitaera | ukrywała wpływ Saitaera na Wojtka chroniąc: Adelę, Sławka, Karolinę i Minerwę. Wyintrygowała zmianę Wojtka w terrorforma by "stracił magię". | 0110-02-01 - 0110-02-05 |
| 190828-migswiatlo-psychotroniczek   | wezwana przez Talię do rozwiązania problemu z BIA. Znalazła winną (Minerwę) i drugiego winnego (Puszczoka). Z Minerwą wysadziła Migświatło; nie jest z tego szczególnie dumna. | 0110-02-08 - 0110-02-10 |
| 190206-nie-da-sie-odrzucic-mocy     | przebija się przez Cyberszkołę uszkadzając Ixion (w czym Minerwę) i unieszkodliwiając Karolinę Erenit. Potem ją reintegruje ze społeczeństwem - już jako maga. | 0110-02-17 - 0110-02-20 |
| 190210-minerwa-i-kwiaty-nadziei     | walczyła o duszę Minerwy z Kornelem. Udało jej się zmontować front polityczny przeciw niemu i go wywalić z okolic Pustogoru - oraz zatrzymać Minerwę dla siebie. | 0110-02-21 - 0110-02-23 |
| 190217-chevaleresse                 | rozpaczliwie deeskaluje wszystkie problemy między Alanem i Tymonem, między Marleną/Karoliną i Dianą. Potem nieźle się bawi kosztem Alana gdy do niego wprowadza się Chevaleresse. | 0110-02-25 - 0110-02-27 |
| 190330-polowanie-na-ataienne        | wpadła zatrzymać wojnę Chevaleresse - uczniowie szkoły magów, uratowała Ataienne przed technovorem a potem jeszcze przed grupą najemników. Straciła power suit. | 0110-03-03 - 0110-03-04 |
| 190208-herbata-grzyby-i-mimik       | synchronizowała się z grzybami i gasiła pożar bombą chemiczną; pokonała z Antonim mimika symbiotycznego. | 0110-03-04 - 0110-03-06 |
| 190213-wygasniecie-starego-autosenta | przełożona operacji neutralizacji autosenta. Ku swojemu wielkiemu zdziwieniu, lokalne siły były kompetentne. Przekazała autosenta Cieniaszczytowi. | 0110-03-08 - 0110-03-09 |
| 190331-kurz-po-ataienne             | uspokoiła sytuację pomiędzy Chevaleresse i Alanem, dodatkowo doszła do porozumienia z Szymonem z Orbitera i dotarła do zleceniodawców eksterminacji Ataienne. | 0110-03-07 - 0110-03-09 |
| 190402-eksperymentalny-power-suit   | przejęła Cień od Orbitera i spróbowała go opanować. Niestety, psychotronika Cienia ją wciągnęła; Ataienne uratowała jej życie ale i tak skończyła w szpitalu. | 0110-03-15 - 0110-03-17 |
| 190406-bardzo-kosztowne-lzy         | uratowała Sensus przed Wiktorem Satarailem, jednorazowo. Odpaliła Cień by odepchnąć Wiktora. Jeden dzień w szpitalu. | 0110-03-21 - 0110-03-26 |
| 190419-osopokalipsa-wiktora         | chroniła ludzi, Wiktora oraz Sensus - negocjacjami i akcją bezpośrednią. Minimalizowała szkody; nie dało się niczego tu wygrać. | 0110-03-29 - 0110-03-31 |
| 190422-pustogorski-konflikt         | zaczęła jako negocjatorka z Miasteczkowcami a skończyła walcząc w Cieniu przeciwko wzmocnionemu mimikowi symbiotycznemu Marcela. | 0110-04-02 - 0110-04-03 |
| 190424-budowa-ixionskiego-mimika    | stworzyła zimny plan jak zaprowadzić pokój w Pustogorze i go przeprowadziła za aprobatą Karli. Skazić mimika energią ixiońską i wzbudzić gniew... | 0110-04-03 - 0110-04-05 |
| 190427-zrzut-w-pacyfice             | przygotowała akcję dojścia do zrzuconego ścigacza; zobaczyła dlaczego Pacyfika jest tak strasznym miejscem i doceniła Erwina ponownie | 0110-04-07 - 0110-04-09 |
| 190429-sabotaz-szeptow-elizy        | po śladach Chevaleresse dotarła do Elizy, sformowała zespół mający rozwalić jej plan i go wykonała. Czyżby manager? | 0110-04-10 - 0110-04-13 |
| 190502-pierwszy-emulator-orbitera   | jak nigdy, spotkała się ze stonewallem i nikt nie chciał jej nic powiedzieć. Na szczęście, wiedza Barbakanu oraz Cień pomogły jej w uratowaniu Nikoli oraz wszystkich przed Nikolą. | 0110-04-14 - 0110-04-16 |
| 190503-bardzo-nieudane-porwania     | wpierw porwała Kirasjerom Emulatorkę, potem ją uwolniła Cieniem, potem wycofała do Pustogoru i jeszcze politycznie zabezpieczyła Minerwę i Nikolę przed nimi. Lol. | 0110-04-18 - 0110-04-20 |
| 190505-szczur-ktory-chroni          | uratowała Adelę mimo, że kosztowało ją to: trafienie z działa Koenig, tydzień w szpitalu, negocjacje z Satarailem i ogólnie mnóstwo bólu głowy i wysiłku. | 0110-04-22 - 0110-04-24 |
| 190519-uciekajacy-seksbot           | od negocjacji z Ernestem, przez walkę z Elizą aż do prośby wobec Saitaera o uratowanie seksbota. Bardzo trudny okres. | 0110-04-25 - 0110-04-26 |
| 190527-mimik-sni-o-esuriit          | pozyskała krew Mireli i kosmetyki Adeli by znaleźć i wkupić się w łaski Mireli, po czym nie dopuściła do spotkania Mireli z Esuriit i wezwała grupę terminusów do rozwiązania problemu. | 0110-05-07 - 0110-05-09 |
| 190616-anomalna-serafina            | zatrzymuje wojnę Grzymościem, | 0110-05-12 - 0110-05-15 |
| 190622-wojna-kajrata                | utraciła kontrolę nad Cieniem i zmasakrowała grupę napastników; dodatkowo poznała przeszłość Serafiny i chroniła jak mogła Lilianę przed Kajratem. | 0110-05-18 - 0110-05-21 |
| 190623-noc-kajrata                  | uratowała Ossidię przed zniszczeniem z Kajratowej ręki Esuriit, po czym odkryła sekret Kajrata. Wyciągnęła Lilianę ze szponów Kajrata, ale nie dała rady powstrzymać Serafiny. | 0110-05-22 - 0110-05-25 |
| 190830-kto-wrobil-alana             | pogodziła Chevaleresse i Talię, osłoniła Alana przed skargą oraz zastawiła skuteczną pułapkę na Wojciecha Zermanna. | 0110-06-03 - 0110-06-05 |
| 190709-somnibel-uciekl-arienikom    | znalazła somnibela, który uciekł do Kajrata i zdecydowała się na nie deeskalowanie konfliktu między nieprzyjemną arystokratką a "plebsem" dookoła. | 0110-06-05 - 0110-06-07 |
| 190714-kult-choroba-esuriit         | uratowała Tukana przed Kajratem i współpracując z Amandą Kajrat doszła do tego, że kult Esuriit jest w okolicach Czółenka. | 0110-06-07 - 0110-06-09 |
| 190721-kirasjerka-najgorszym-detektywem | pokonała w walce Mirelę (Kirasjerkę). Od Wiktora dowiedziała się o co chodzi z kralotami cieniaszczyckimi w okolicy. Oraz od Orbitera wysępiła stracony power suit. | 0110-06-10 - 0110-06-11 |
| 190724-odzyskana-agentka-orbitera   | rozwiązała problem polityczny; zinfiltrowała Cieniaszczyt, wkręciła Mirelę do walki z Moktarem i vice versa oraz ostatecznie odbiła Aidę... cudzymi siłami. | 0110-06-12 - 0110-06-15 |
| 190906-wypadek-w-kramamczu          | zamiast pójść w zniszczenie Włóknina, skupiła się na uratowaniu tej niewielkiej firmy - poprosiła Kasjopeę i zneutralizowała Ksenię. | 0110-06-18 - 0110-06-19 |
| 190917-zagubiony-efemerydyta        | nie ma głowy do zabawy z nieletnimi efemerydytami z Eterni. Po znalezieniu efemerydyty, oddała go Orbiterowi. | 0110-06-21 - 0110-06-22 |
| 190901-polowanie-na-pieknotke       | przegrała walkę z Józefem; uratował ją Alan. Ale - udało jej się dowiedzieć kim był napastnik, zatrzymać głupi atak na enklawy a potem uratowała Alana i Chevaleresse. | 0110-06-21 - 0110-06-24 |
| 190726-bardzo-niebezpieczne-skladowisko | politycznie rozmontowała potencjalną wojnę Wolnego Uśmiechu i Nocnego Nieba o dziwne narkotyki. Ściągnęła do tego Orbiter i deeskalowała sprawę. | 0110-06-26 - 0110-06-28 |
| 191103-kontrpolowanie-pieknotki-pulapka | . | 0110-06-24 - 0110-07-01 |
| 190804-niespodziewany-wplyw-aidy    | pokonała splugawionych przez Hralwagha magów, potem we współpracy z Saitaerem i Ossidią samego Hralwagha. Oddała Saitaerowi Amandę Kajrat. | 0110-06-30 - 0110-07-02 |
| 191105-zaginiona-soniczka           | posłuchała prośby Ataienne, nie wierząc o co chodzi - i rozwiązała ring transportujący dziewczyny do Cieniaszczytu. Dzięki temu mają ślad na oficera Grzymościa. | 0110-07-02 - 0110-07-03 |
| 191112-korupcja-z-arystokratki      | rozdysponowała wpierdol - arystokratce, uczniakowi, burmistrzowi, nawet TAI. Ściągnęła Ataienne by wiedzieć komu dać wpierdol. | 0110-07-04 - 0110-07-05 |
| 190817-kwiaty-w-sluzbie-puryfikacji | udaje Adelę Kirys; | 0110-07-04 - 0110-07-09 |
| 191201-ukradziony-entropik          | stanęła przed niemożliwą sytuacją - Orbiter czy Karla. Tępić Grzymościa czy zdrowie ludzi. A wszystko dlatego, bo zniknął jeden Entropik. | 0110-07-13 - 0110-07-15 |
| 200202-krucjata-chevaleresse        | rozplątuje upiorną sieć zarzuconą przez Ataienne i Chevaleresse przeciwko Grzymościowi, Kardamaczowi oraz krzywdzicielowi Alana. Zmartwiona niestabilnością Ataienne. | 0110-07-24 - 0110-07-27 |
| 200222-rozbrojenie-bomby-w-kalbarku | pokonała dwóch terminusów i bioformę bez Cienia. Potem zdobyła wsparcie Ataienne, wydobyła Chevaleresse i zapewniła siły w Kalbarku na potem. | 0110-07-27 - 0110-08-01 |
| 200414-arystokraci-na-trzesawisku   | sprowadziła kontrolowanego potwora Trzęsawiska na głupią ekspedycję arystokratów a potem by ich ratować użyła chemikaliów by straumatyzować Franciszka. | 0110-08-04 - 0110-08-05 |
| 200417-nawolywanie-trzesawiska      | śledząc dziwne zachowanie Sabiny odkryła jej sekret i ją złamała. Ale potem spróbowała jej pomóc i odpięła ją od Trzęsawiska używając Szpitala Terminuskiego. Przekonała Gabriela by zachował sekret Sabiny dla siebie. | 0110-08-12 - 0110-08-14 |
| 200418-wojna-trzesawiska            | wpierw broniła Czemerty przed bioformami Trzęsawiska (przez kłąb feromonów ściągających) a potem przekradła się po Trzęsawisku by negocjować z Wiktorem Satarailem. | 0110-08-16 - 0110-08-21 |
| 200425-inflitrator-poluje-na-tai-minerwy | chcąc pomóc Minerwie przed fałszywym oskarżeniem o uszkadzanie TAI odkryła i zniszczyła Infiltratora Iniekcyjnego. Spadła z awiana by spłaszczyć elektro-DJa. | 0110-08-24 - 0110-08-26 |
| 200502-po-co-atakuja-minerwe        | pełne zastraszenie Haliny; weszła w ciemności PLUS po aktywacji Cienia dokonała demolki. Zastraszanie było super-efektywne. | 0110-08-26 - 0110-08-28 |
| 200509-rekin-z-aurum-i-fortifarma   |  | 0110-08-30 - 0110-09-04 |
| 200510-tajna-baza-orbitera          | katalizator: wyłączyła relację Myrczek - Kazitan, odnalazła tajną bazę Irrydius przy użyciu Tymona oraz porozmawiała z tamtejszą Emulatorką. | 0110-09-07 - 0110-09-11 |
| 200524-dom-dla-melindy              | zaczęła od szukania zaginionej Melindy, skończyła na współpracowaniu z Melindą by ta nie wracała do domu; wsadziła Melindę Alanowi dla poprawienia humoru. | 0110-09-13 - 0110-09-16 |
| 200623-adaptacja-azalii             | przeprowadziła ekspedycję Orbitera przez Trzęsawisko z pomocą Minerwy; wcześniej przenegocjowała z Wiktorem żeby im bardzo nie przeszkadzał. | 0110-09-26 - 0110-10-04 |
| 200913-haracz-w-parku-rozrywki      | zestrzeliwuje servary szybciej niż Alan Fulmenem, gada z ludźmi i zbiera informacje dla Alana, też znalazła dronę która ich śledziła. | 0110-10-09 - 0110-10-10 |
| 201011-narodziny-paladynki-saitaera | skończyła waśń między rodami Kazitan i Ursus; potem zrobiła nagą dywersję w parku rozrywki i gdy złapał ją Agaton by "wyleczyć", spanikowała. Gdy Cień prawie zabił Agatona, Pięknotka stanęła przeciw Cieniowi twarzą w twarz i zginęła. Saitaer ją zixionizował i przywrócił jako swoją paladynkę, ku wielkiej żałości Pięknotki. | 0110-10-12 - 0110-10-13 |
| 201025-kraloth-w-parku-janor        | próbuje się odnaleźć w świecie, w którym jest nie-sobą. Znajduje z Minerwą kralotha w parku rozrywki i porywa zniewolonego terminusa używając nowej siły. | 0110-10-29 - 0110-10-31 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180815-komary-i-kosmetyki           | jej reputacja nie ucierpiała i jej przeszłość - tworzenie problematycznych komarów - poszła w niepamięć w Pustogorze | 0109-09-05
| 180817-protomag-z-trzesawisk        | zrobiła sobie wroga w Czerwonych Myszach (dokładnie: Alanie Bartozolu), bo wymanewrowała Alana ze zdobycia Felicji. | 0109-09-09
| 181003-lilia-na-trzesawisku         | ma dostęp do nasion Lis'ha'ora. | 0109-09-22
| 181021-powrot-minerwy-z-terrorforma | reputacja wyjątkowo niszczycielskiej. Wypożyczyła salę i 2 osoby skończyły krytycznie w szpitalu a as awianów stracił servara. | 0109-09-29
| 181024-decyzja-minerwy              | stosunki Pięknotki i Minerwy się ociepliły | 0109-10-04
| 181027-terminuska-czy-kosmetyczka   | pierwsze miejsce w konkursie kosmetyczek w Pustogorze. Najlepsza i najbardziej ceniona. Ciężko na to zapracowała. | 0109-10-11
| 181027-terminuska-czy-kosmetyczka   | chce dowiedzieć się, kto do cholery próbuje się pozbyć Ateny Sowińskiej | 0109-10-11
| 181101-wojna-o-uczciwe-polfinaly    | dostała odpowiedzialność za teren Zaczęstwa, co jej wyjątkowo nie pasuje. Tymon jej będzie pomagał. | 0109-10-19
| 181101-wojna-o-uczciwe-polfinaly    | chce zrzucić z siebie Zaczęstwo i dostać jakiś sensowny teren. JAKIKOLWIEK. Ale nie Zaczęstwo - nie jest technomantką ani katalistką. | 0109-10-19
| 181114-neutralizacja-artylerii-koszmarow | Wiktor Satarail ma do niej lekką słabość. | 0109-10-21
| 181114-neutralizacja-artylerii-koszmarow | uważana przez wielu terminusów za zdecydowanie niepoważną - dała odejść Satarailowi a potem strasznie ryzykowała chcąc rozmontować Artylerię Koszmarów. | 0109-10-21
| 181104-kotlina-duchow               | nie odpowiada już za Zaczęstwo. Za to odpowiada za Czarnopalec, Podwiert oraz Żarnię (i okolice) | 0109-10-24
| 181104-kotlina-duchow               | trzy dni w szpitalu - detoks i odkażanie po sprawach z Efemerydą Kotliny Mikarajły | 0109-10-24
| 181118-romuald-i-julia              | w Cieniaszczycie Pięknotka ma reputację osoby, która lubi być ciężko poturbowana (kraloth/Zwierz, 2 razy w szpitalu...) | 0109-10-29
| 181118-romuald-i-julia              | chce wzmocnić swój power suit lokalną technologią. Emulator jest w końcu technomantką i inżynierem... | 0109-10-29
| 181125-swaty-w-cieniu-potwora       | zaprzyjaźniła się blisko (łącznie z łóżkiem) z Wiolettą. | 0109-11-01
| 181125-swaty-w-cieniu-potwora       | chce zrozumieć czym jest Moktar Gradon i znaleźć środki, którymi może go unieszkodliwić by nigdy nie miał jej bezradnej w swoich szponach. | 0109-11-01
| 181209-kajdan-na-moktara            | Moktar i ona nie są już w stanie otwartej wojny. Pięknotka ma hak na Moktara - może zniszczyć Łyse Psy i osłabić jego plany. | 0109-11-03
| 181209-kajdan-na-moktara            | ma dostęp do ech pamięci Walerii z różnych momentów, czasów i punktów przeszłości. | 0109-11-03
| 181216-wolna-od-terrorforma         | pełna przebudowa karty postaci. Główne aspekty: chemiczna, seksowna broń uwodząca. Nadal pozostaje kosmetyczką i terminuską - jest po prostu groźniejsza. | 0109-11-11
| 181216-wolna-od-terrorforma         | straszna słabość do Bogdana Szerla. Trudno jej się mu oprzeć w dowolnej formie. | 0109-11-11
| 181216-wolna-od-terrorforma         | bardzo jej zależy, by jakoś się uodpornić na działania Bogdana Szerla. | 0109-11-11
| 181216-wolna-od-terrorforma         | chce spiknąć ze sobą Pietra i Wiolettę. Skoro ma odejść z terenu, niech przynajmniej zostanie po niej coś fajnego. | 0109-11-11
| 181218-tajemniczy-oltarz-moktara    | potrzebuje Bogdana. Po prostu. To nie jest miłość, to jest uzależnienie. Spirala. | 0109-11-13
| 181218-tajemniczy-oltarz-moktara    | przez następne dwa tygodnie jest całkowicie niemożliwe by cokolwiek zdominowało Pięknotkę przez energię Arazille. | 0109-11-13
| 181225-czyszczenie-toksycznych-zwiazkow | jest "wolna" od wpływu Bogdana. Ona kontroluje tą relację ze swojej strony, choć ma słabość - ale nie jest kontrolowana. | 0109-11-17
| 181225-czyszczenie-toksycznych-zwiazkow | Erwin Galilien się w niej zakochał. Nie jest tylko zwykłą przyjaciółką - zostali kochankami. | 0109-11-17
| 181225-czyszczenie-toksycznych-zwiazkow | Moktar czuje do niej szacunek. Niechętny szacunek, jako wystarczająco godnego przeciwnika. | 0109-11-17
| 181225-czyszczenie-toksycznych-zwiazkow | chce jak najszybciej spreparować ciało dla Minerwy stąd, z Cieniaszczytu. By jak najbardziej ułatwić Minerwie przejście i bycie sobą. | 0109-11-17
| 181226-finis-vitae                  | wzięła dług u Arazille - pomoże jej założyć świątynię blokującą działania autowara Finis Vitae | 0109-11-27
| 181226-finis-vitae                  | utraciła ochronę Arazille - czas minął. Jest znowu "normalną Pięknotką". | 0109-11-27
| 181226-finis-vitae                  | jest traktowana jako pomniejszy sojusznik Moktara oraz Łysych Psów przez Moktara i Łyse Psy. One of them, though not completely. | 0109-11-27
| 181227-adieu-cieniaszczycie         | uwolniła się od długu u Arazille. Wszystkie tematy w Cieniaszczycie załatwione - wraca do Pustogoru. | 0109-12-01
| 190101-morderczyni-jednej-plotki    | w Pustogorze ma reputację osoby której nie wolno stanąć na drodze, bo go zniszczy. Zrobi krzywdę na kilku poziomach. | 0109-12-17
| 190102-stalker-i-czerwone-myszy     | pozbyła się jakiegokolwiek terenu - za żaden nie odpowiada. Od tej pory jest terminusem lotnym, wykonującym działania dla Pustogoru. | 0109-12-21
| 190102-stalker-i-czerwone-myszy     | znowu poczuła głos Saitaera w jej głowie; ale nie jest to złe. Tym razem nic jej z jego strony nie grozi. | 0109-12-21
| 190105-turysci-na-trzesawisku       | ma grupę zaprzyjaźnionych wił na Trzęsawisku Zjawosztup. | 0109-12-24
| 190106-a-moze-pustogorska-mafia     | nie ma większości swoich bagiennych zasobów po ostatniej akcji. | 0109-12-26
| 190113-chronmy-karoline-przed-uczniami | Arnulf Poważny jej ufa i będzie z nią współpracował zanim wybierze innego terminusa. | 0110-01-06
| 190116-wypalenie-saitaera-z-trzesawiska | wśród załogi ASD Centurion ma reputację szalonej, zarówno w negatywnym spojrzeniu jak i z pewnym podziwem. Co więcej, przetrwała. | 0110-01-09
| 190116-wypalenie-saitaera-z-trzesawiska | jej Wzór jest łatwy do zmodyfikowania przez Saitaera. Za dużo razy to już robił i zbyt dobrze ją już zna. | 0110-01-09
| 190119-skorpipedy-krolewskiego-xirathira | poprawiła swoje relacje z Wiktorem Satarailem - nie jakoś bardzo, ale wrócili do neutralnego chłodnego. | 0110-01-15
| 190202-czarodziejka-z-woli-saitaera | potrafi Skażać energią Saitaera. Niestety dla niej, umie przesyłać ixiońską energię. | 0110-02-05
| 190402-eksperymentalny-power-suit   | ma "Cień". Bardzo potężny symbiotyczny ixioński nienawistny techorganiczny power suit którego nie może kontrolować... | 0110-03-17
| 190424-budowa-ixionskiego-mimika    | dyskretny medal BlackOps od Karli, w służbie Pustogoru | 0110-04-05
| 190502-pierwszy-emulator-orbitera   | skutecznie narzuciła swoją wolę Cieniowi w chwili, w której instynkt jej i Cienia były maksymalnie sprzeczne | 0110-04-16
| 190503-bardzo-nieudane-porwania     | zdobyła 'rapport' u Cienia za uratowanie Emulatorki Kirasjerów (Mireli) | 0110-04-20
| 190503-bardzo-nieudane-porwania     | zdobyła szacunek Kirasjerów Orbitera za to, że samodzielnie dała radę wyślizgnąć się z ich operacji | 0110-04-20
| 190505-szczur-ktory-chroni          | Cień oraz ona dostają reputację morderczych potworów w mafii. I wśród terminusów. Jak jest w Cieniu - uciekaj. | 0110-04-24
| 190519-uciekajacy-seksbot           | o krok bliżej do Saitaera. Wezwała go, a on odpowiedział. | 0110-04-26
| 190622-wojna-kajrata                | opinia bezwzględnej i okrutnej morderczyni wśród Wolnych Ptaków; niechętnie widziana poza murami Centrum Astorii. | 0110-05-21
| 200417-nawolywanie-trzesawiska      | ma pełną kontrolę nad losem Sabiny Kazitan; Sabina jej NIE nienawidzi, ale się jej boi. | 0110-08-14
| 200502-po-co-atakuja-minerwe        | opinia wyjątkowo okrutnej terminuski wśród cywili; wchodzi twardo w pacyfistkę Halinę. Ale jest niesamowicie skuteczna - to się liczy. | 0110-08-28
| 200509-rekin-z-aurum-i-fortifarma   | podpadła Arturowi Michasiewiczowi, kwatermistrzowi Aurum. Nie szanuje sprzętu i go uszkadza a nie musi. | 0110-09-04
| 200616-bardzo-straszna-mysz         | Dostała solidny opiernicz od Ksenii za to, że puszcza Gabriela na akcje z potworami w terenie Esuriit. Poszło do papierów. | 0110-09-21
| 201011-narodziny-paladynki-saitaera | PIVOTAL POINT; zginęła w walce z Cieniem i została odrodzona przez Saitaera jako jego paladynka. Cień został zniszczony, zintegrowany z jej ciałem (poza Esuriit). Pięknotka jest klasyfikowana jako mag ixioński, zupełnie jak Minerwa. Wyjątkowo wrażliwa na Esuriit, ale z nanorozkładem materii i świetną biosyntezą. | 0110-10-13
| 201025-kraloth-w-parku-janor        | koniecznie potrzebuje transfuzji puryfikacyjnej krwi raz na tydzień, pełna transfuzja. Pod opieką Lucjusza Blakenbauera. | 0110-10-31

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Minerwa Metalia      | 25 | ((181024-decyzja-minerwy; 181027-terminuska-czy-kosmetyczka; 181230-uwiezienie-saitaera; 190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei; 190402-eksperymentalny-power-suit; 190424-budowa-ixionskiego-mimika; 190427-zrzut-w-pacyfice; 190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 190503-bardzo-nieudane-porwania; 190828-migswiatlo-psychotroniczek; 190901-polowanie-na-pieknotke; 191103-kontrpolowanie-pieknotki-pulapka; 191112-korupcja-z-arystokratki; 191201-ukradziony-entropik; 200202-krucjata-chevaleresse; 200425-inflitrator-poluje-na-tai-minerwy; 200502-po-co-atakuja-minerwe; 200623-adaptacja-azalii; 201011-narodziny-paladynki-saitaera; 201025-kraloth-w-parku-janor)) |
| Erwin Galilien       | 20 | ((180815-komary-i-kosmetyki; 180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly; 181003-lilia-na-trzesawisku; 181021-powrot-minerwy-z-terrorforma; 181227-adieu-cieniaszczycie; 190101-morderczyni-jednej-plotki; 190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 190210-minerwa-i-kwiaty-nadziei; 190402-eksperymentalny-power-suit; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190427-zrzut-w-pacyfice; 190429-sabotaz-szeptow-elizy; 190527-mimik-sni-o-esuriit; 190817-kwiaty-w-sluzbie-puryfikacji; 191105-zaginiona-soniczka; 200425-inflitrator-poluje-na-tai-minerwy; 200509-rekin-z-aurum-i-fortifarma)) |
| Alan Bartozol        | 16 | ((180817-protomag-z-trzesawisk; 181101-wojna-o-uczciwe-polfinaly; 190101-morderczyni-jednej-plotki; 190105-turysci-na-trzesawisku; 190112-eksperymenty-na-wilach; 190116-wypalenie-saitaera-z-trzesawiska; 190217-chevaleresse; 190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 190419-osopokalipsa-wiktora; 190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 190505-szczur-ktory-chroni; 190830-kto-wrobil-alana; 190901-polowanie-na-pieknotke; 200913-haracz-w-parku-rozrywki)) |
| Atena Sowińska       | 15 | ((180817-protomag-z-trzesawisk; 180821-programista-mimo-woli; 180906-nikt-nie-spi-w-swoim-lozku; 180912-inkarnata-nienawisci; 180929-dwa-tygodnie-szkoly; 181021-powrot-minerwy-z-terrorforma; 181112-odklatwianie-ateny; 181216-wolna-od-terrorforma; 181218-tajemniczy-oltarz-moktara; 181225-czyszczenie-toksycznych-zwiazkow; 181226-finis-vitae; 181227-adieu-cieniaszczycie; 190210-minerwa-i-kwiaty-nadziei; 190213-wygasniecie-starego-autosenta; 190330-polowanie-na-ataienne)) |
| Mariusz Trzewń       | 12 | ((190827-rozpaczliwe-ratowanie-bii; 190901-polowanie-na-pieknotke; 190906-wypadek-w-kramamczu; 191103-kontrpolowanie-pieknotki-pulapka; 191105-zaginiona-soniczka; 191112-korupcja-z-arystokratki; 191201-ukradziony-entropik; 200202-krucjata-chevaleresse; 200414-arystokraci-na-trzesawisku; 200510-tajna-baza-orbitera; 200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Tymon Grubosz        | 12 | ((181101-wojna-o-uczciwe-polfinaly; 181114-neutralizacja-artylerii-koszmarow; 190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy; 190217-chevaleresse; 190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek; 191105-zaginiona-soniczka; 200222-rozbrojenie-bomby-w-kalbarku; 200510-tajna-baza-orbitera)) |
| Wiktor Satarail      | 12 | ((181114-neutralizacja-artylerii-koszmarow; 190105-turysci-na-trzesawisku; 190116-wypalenie-saitaera-z-trzesawiska; 190119-skorpipedy-krolewskiego-xirathira; 190127-ixionski-transorganik; 190406-bardzo-kosztowne-lzy; 190419-osopokalipsa-wiktora; 190505-szczur-ktory-chroni; 190527-mimik-sni-o-esuriit; 190721-kirasjerka-najgorszym-detektywem; 200418-wojna-trzesawiska; 200623-adaptacja-azalii)) |
| Adela Kirys          | 10 | ((180815-komary-i-kosmetyki; 180929-dwa-tygodnie-szkoly; 181024-decyzja-minerwy; 181027-terminuska-czy-kosmetyczka; 190106-a-moze-pustogorska-mafia; 190113-chronmy-karoline-przed-uczniami; 190119-skorpipedy-krolewskiego-xirathira; 190202-czarodziejka-z-woli-saitaera; 190505-szczur-ktory-chroni; 190527-mimik-sni-o-esuriit)) |
| Diana Tevalier       | 10 | ((190217-chevaleresse; 190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 190429-sabotaz-szeptow-elizy; 190830-kto-wrobil-alana; 190901-polowanie-na-pieknotke; 200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku; 200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Ernest Kajrat        | 10 | ((190505-szczur-ktory-chroni; 190519-uciekajacy-seksbot; 190616-anomalna-serafina; 190622-wojna-kajrata; 190623-noc-kajrata; 190709-somnibel-uciekl-arienikom; 190714-kult-choroba-esuriit; 190726-bardzo-niebezpieczne-skladowisko; 190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Gabriel Ursus        | 9 | ((190726-bardzo-niebezpieczne-skladowisko; 190804-niespodziewany-wplyw-aidy; 190917-zagubiony-efemerydyta; 200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200425-inflitrator-poluje-na-tai-minerwy; 200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera; 201011-narodziny-paladynki-saitaera)) |
| Lucjusz Blakenbauer  | 9 | ((180817-protomag-z-trzesawisk; 181112-odklatwianie-ateny; 181230-uwiezienie-saitaera; 190330-polowanie-na-ataienne; 190429-sabotaz-szeptow-elizy; 190505-szczur-ktory-chroni; 190901-polowanie-na-pieknotke; 200222-rozbrojenie-bomby-w-kalbarku; 201025-kraloth-w-parku-janor)) |
| Olaf Zuchwały        | 9 | ((190101-morderczyni-jednej-plotki; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 190622-wojna-kajrata; 190917-zagubiony-efemerydyta; 191103-kontrpolowanie-pieknotki-pulapka; 200417-nawolywanie-trzesawiska)) |
| Ataienne             | 8 | ((190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 190402-eksperymentalny-power-suit; 190817-kwiaty-w-sluzbie-puryfikacji; 191105-zaginiona-soniczka; 191112-korupcja-z-arystokratki; 200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Karla Mrozik         | 8 | ((181230-uwiezienie-saitaera; 190120-nowa-minerwa-w-nowym-swiecie; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy; 190503-bardzo-nieudane-porwania; 200418-wojna-trzesawiska; 200623-adaptacja-azalii)) |
| Saitaer              | 8 | ((181112-odklatwianie-ateny; 190112-eksperymenty-na-wilach; 190116-wypalenie-saitaera-z-trzesawiska; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190519-uciekajacy-seksbot; 190804-niespodziewany-wplyw-aidy; 201011-narodziny-paladynki-saitaera)) |
| Ignacy Myrczek       | 7 | ((180929-dwa-tygodnie-szkoly; 181027-terminuska-czy-kosmetyczka; 191112-korupcja-z-arystokratki; 200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200510-tajna-baza-orbitera)) |
| Karolina Erenit      | 7 | ((181101-wojna-o-uczciwe-polfinaly; 190113-chronmy-karoline-przed-uczniami; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy; 190217-chevaleresse; 190830-kto-wrobil-alana)) |
| Sabina Kazitan       | 7 | ((191112-korupcja-z-arystokratki; 200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera; 201011-narodziny-paladynki-saitaera)) |
| Kasjopea Maus        | 6 | ((190116-wypalenie-saitaera-z-trzesawiska; 190120-nowa-minerwa-w-nowym-swiecie; 190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei; 190419-osopokalipsa-wiktora; 190906-wypadek-w-kramamczu)) |
| Lilia Ursus          | 6 | ((181003-lilia-na-trzesawisku; 181021-powrot-minerwy-z-terrorforma; 181024-decyzja-minerwy; 181125-swaty-w-cieniu-potwora; 181227-adieu-cieniaszczycie; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Moktar Gradon        | 6 | ((181125-swaty-w-cieniu-potwora; 181218-tajemniczy-oltarz-moktara; 181225-czyszczenie-toksycznych-zwiazkow; 181226-finis-vitae; 181227-adieu-cieniaszczycie; 190724-odzyskana-agentka-orbitera)) |
| Tomasz Tukan         | 6 | ((190519-uciekajacy-seksbot; 190616-anomalna-serafina; 190709-somnibel-uciekl-arienikom; 190714-kult-choroba-esuriit; 200202-krucjata-chevaleresse; 200425-inflitrator-poluje-na-tai-minerwy)) |
| Arnulf Poważny       | 5 | ((180929-dwa-tygodnie-szkoly; 181027-terminuska-czy-kosmetyczka; 190113-chronmy-karoline-przed-uczniami; 190206-nie-da-sie-odrzucic-mocy; 190519-uciekajacy-seksbot)) |
| Julia Morwisz        | 5 | ((181118-romuald-i-julia; 181216-wolna-od-terrorforma; 181225-czyszczenie-toksycznych-zwiazkow; 190724-odzyskana-agentka-orbitera; 190804-niespodziewany-wplyw-aidy)) |
| Talia Aegis          | 5 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek; 190830-kto-wrobil-alana; 200202-krucjata-chevaleresse; 200414-arystokraci-na-trzesawisku)) |
| Teresa Mieralit      | 5 | ((190101-morderczyni-jednej-plotki; 190102-stalker-i-czerwone-myszy; 190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 200510-tajna-baza-orbitera)) |
| Waleria Cyklon       | 5 | ((181125-swaty-w-cieniu-potwora; 181209-kajdan-na-moktara; 181218-tajemniczy-oltarz-moktara; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Adam Szarjan         | 4 | ((181021-powrot-minerwy-z-terrorforma; 181024-decyzja-minerwy; 181118-romuald-i-julia; 190724-odzyskana-agentka-orbitera)) |
| Amadeusz Sowiński    | 4 | ((181112-odklatwianie-ateny; 181226-finis-vitae; 181227-adieu-cieniaszczycie; 190106-a-moze-pustogorska-mafia)) |
| Brygida Maczkowik    | 4 | ((180906-nikt-nie-spi-w-swoim-lozku; 180912-inkarnata-nienawisci; 181027-terminuska-czy-kosmetyczka; 181227-adieu-cieniaszczycie)) |
| Kornel Garn          | 4 | ((190112-eksperymenty-na-wilach; 190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei; 190817-kwiaty-w-sluzbie-puryfikacji)) |
| Liliana Bankierz     | 4 | ((190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 190622-wojna-kajrata; 190623-noc-kajrata)) |
| Marek Puszczok       | 4 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek; 190917-zagubiony-efemerydyta; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Marlena Maja Leszczyńska | 4 | ((181101-wojna-o-uczciwe-polfinaly; 181104-kotlina-duchow; 190206-nie-da-sie-odrzucic-mocy; 190217-chevaleresse)) |
| Mirela Orion         | 4 | ((190503-bardzo-nieudane-porwania; 190721-kirasjerka-najgorszym-detektywem; 190724-odzyskana-agentka-orbitera; 190726-bardzo-niebezpieczne-skladowisko)) |
| Napoleon Bankierz    | 4 | ((180929-dwa-tygodnie-szkoly; 190113-chronmy-karoline-przed-uczniami; 190127-ixionski-transorganik; 200417-nawolywanie-trzesawiska)) |
| Nikola Kirys         | 4 | ((190427-zrzut-w-pacyfice; 190502-pierwszy-emulator-orbitera; 190503-bardzo-nieudane-porwania; 190622-wojna-kajrata)) |
| Pietro Dwarczan      | 4 | ((181118-romuald-i-julia; 181125-swaty-w-cieniu-potwora; 181209-kajdan-na-moktara; 181227-adieu-cieniaszczycie)) |
| Roland Grzymość      | 4 | ((180929-dwa-tygodnie-szkoly; 190106-a-moze-pustogorska-mafia; 190726-bardzo-niebezpieczne-skladowisko; 191201-ukradziony-entropik)) |
| Amanda Kajrat        | 3 | ((190714-kult-choroba-esuriit; 190726-bardzo-niebezpieczne-skladowisko; 190804-niespodziewany-wplyw-aidy)) |
| Bogdan Szerl         | 3 | ((181216-wolna-od-terrorforma; 181218-tajemniczy-oltarz-moktara; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Damian Orion         | 3 | ((190503-bardzo-nieudane-porwania; 191103-kontrpolowanie-pieknotki-pulapka; 200202-krucjata-chevaleresse)) |
| Felicja Melitniek    | 3 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly; 181114-neutralizacja-artylerii-koszmarow)) |
| Kirył Najłalmin      | 3 | ((180814-trufle-z-kosmosu; 181104-kotlina-duchow; 190127-ixionski-transorganik)) |
| Laura Tesinik        | 3 | ((200425-inflitrator-poluje-na-tai-minerwy; 200510-tajna-baza-orbitera; 200524-dom-dla-melindy)) |
| Mateusz Kardamacz    | 3 | ((191105-zaginiona-soniczka; 200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Mirela Niecień       | 3 | ((181216-wolna-od-terrorforma; 181227-adieu-cieniaszczycie; 190724-odzyskana-agentka-orbitera)) |
| Olga Myszeczka       | 3 | ((181104-kotlina-duchow; 190119-skorpipedy-krolewskiego-xirathira; 190721-kirasjerka-najgorszym-detektywem)) |
| Ossidia Saitis       | 3 | ((190519-uciekajacy-seksbot; 190623-noc-kajrata; 190804-niespodziewany-wplyw-aidy)) |
| Romuald Czurukin     | 3 | ((181118-romuald-i-julia; 181125-swaty-w-cieniu-potwora; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Serafina Ira         | 3 | ((190616-anomalna-serafina; 190622-wojna-kajrata; 190623-noc-kajrata)) |
| Sławomir Muczarek    | 3 | ((190112-eksperymenty-na-wilach; 190119-skorpipedy-krolewskiego-xirathira; 190202-czarodziejka-z-woli-saitaera)) |
| Strażniczka Alair    | 3 | ((200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska; 200425-inflitrator-poluje-na-tai-minerwy)) |
| Tadeusz Kruszawiecki | 3 | ((180821-programista-mimo-woli; 180906-nikt-nie-spi-w-swoim-lozku; 181101-wojna-o-uczciwe-polfinaly)) |
| Waldemar Mózg        | 3 | ((190102-stalker-i-czerwone-myszy; 190106-a-moze-pustogorska-mafia; 190119-skorpipedy-krolewskiego-xirathira)) |
| Aida Serenit         | 2 | ((190721-kirasjerka-najgorszym-detektywem; 190724-odzyskana-agentka-orbitera)) |
| Aleksander Rugczuk   | 2 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Antoni Kotomin       | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Artur Kołczond       | 2 | ((200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera)) |
| Artur Michasiewicz   | 2 | ((190828-migswiatlo-psychotroniczek; 200509-rekin-z-aurum-i-fortifarma)) |
| Baltazar Rączniak    | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Bronisława Strzelczyk | 2 | ((180814-trufle-z-kosmosu; 180821-programista-mimo-woli)) |
| Dariusz Bankierz     | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Eliza Ira            | 2 | ((190429-sabotaz-szeptow-elizy; 190519-uciekajacy-seksbot)) |
| Jadwiga Pszarnik     | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Józef Małmałaz       | 2 | ((190901-polowanie-na-pieknotke; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Kasjan Czerwoczłek   | 2 | ((190106-a-moze-pustogorska-mafia; 190505-szczur-ktory-chroni)) |
| Keraina d'Orion      | 2 | ((191201-ukradziony-entropik; 200202-krucjata-chevaleresse)) |
| Kreacjusz Diakon     | 2 | ((181230-uwiezienie-saitaera; 200502-po-co-atakuja-minerwe)) |
| Krystian Namałłek    | 2 | ((190505-szczur-ktory-chroni; 190616-anomalna-serafina)) |
| Ksenia Kirallen      | 2 | ((190906-wypadek-w-kramamczu; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Melinda Teilert      | 2 | ((200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Miedwied Zajcew      | 2 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly)) |
| Natalia Tessalon     | 2 | ((200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera)) |
| Persefona d'Jastrząbiec | 2 | ((191112-korupcja-z-arystokratki; 191201-ukradziony-entropik)) |
| Staś Kruszawiecki    | 2 | ((180821-programista-mimo-woli; 180912-inkarnata-nienawisci)) |
| Szymon Oporcznik     | 2 | ((190331-kurz-po-ataienne; 190402-eksperymentalny-power-suit)) |
| Wojtek Kurczynos     | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Adam Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Adrian Wężoskór      | 1 | ((190406-bardzo-kosztowne-lzy)) |
| Agaton Ociegor       | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Aleksander Iczak     | 1 | ((190101-morderczyni-jednej-plotki)) |
| Aleksander Muniakiewicz | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Aleksandra Szklarska | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Almeda Literna       | 1 | ((190208-herbata-grzyby-i-mimik)) |
| Anita Wirkot         | 1 | ((181205-jeden-dokument-nie-w-pore)) |
| Antoni Żuwaczka      | 1 | ((190616-anomalna-serafina)) |
| Arazille             | 1 | ((181226-finis-vitae)) |
| Arkadiusz Mocarny    | 1 | ((180814-trufle-z-kosmosu)) |
| ASD Centurion        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Azalia d'Alkaris     | 1 | ((200623-adaptacja-azalii)) |
| Bartłomiej Małczarek | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| BIA Tarn             | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Bożymir Szczupak     | 1 | ((190502-pierwszy-emulator-orbitera)) |
| Cezary Alentik       | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Cezary Zwierz        | 1 | ((181112-odklatwianie-ateny)) |
| Damian Podpalnik     | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
| Dariusz Kuromin      | 1 | ((190906-wypadek-w-kramamczu)) |
| Diana Lemurczak      | 1 | ((200524-dom-dla-melindy)) |
| Feliks Mirtan        | 1 | ((200502-po-co-atakuja-minerwe)) |
| Franciszek Leszczowik | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Franciszek Owadowiec | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Franek Bulterier     | 1 | ((200524-dom-dla-melindy)) |
| Gerwazy Lemurczak    | 1 | ((200524-dom-dla-melindy)) |
| Grażyna Sirwąg       | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Halina Sermniek      | 1 | ((200502-po-co-atakuja-minerwe)) |
| Hestia d'Itaran      | 1 | ((191201-ukradziony-entropik)) |
| Hestia d'Janor       | 1 | ((201025-kraloth-w-parku-janor)) |
| Hieronim Maus        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Ida Tiara            | 1 | ((190406-bardzo-kosztowne-lzy)) |
| Jakub Wirus          | 1 | ((180814-trufle-z-kosmosu)) |
| Jan Kramczuk         | 1 | ((190102-stalker-i-czerwone-myszy)) |
| Jan Revlen           | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Jan Uszczar          | 1 | ((190917-zagubiony-efemerydyta)) |
| Kaja Selerek         | 1 | ((190402-eksperymentalny-power-suit)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Karol Senonik        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Karol Szurnak        | 1 | ((190101-morderczyni-jednej-plotki)) |
| Karradrael           | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Konrad Wączak        | 1 | ((200623-adaptacja-azalii)) |
| Krystyna Senonik     | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Ksawery Wojnicki     | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Maciej Oczorniak     | 1 | ((201025-kraloth-w-parku-janor)) |
| Maja Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Malictrix d'Itaran   | 1 | ((200202-krucjata-chevaleresse)) |
| Marcel Nieciesz      | 1 | ((190422-pustogorski-konflikt)) |
| Mariola Tralment     | 1 | ((191105-zaginiona-soniczka)) |
| Mieszko Weiner       | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Minerwa Diakon       | 1 | ((181112-odklatwianie-ateny)) |
| Mirela Satarail      | 1 | ((190527-mimik-sni-o-esuriit)) |
| Nataniel Marszalnik  | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Nikodem Larwent      | 1 | ((200502-po-co-atakuja-minerwe)) |
| Oliwia Namałłek      | 1 | ((190505-szczur-ktory-chroni)) |
| Rafał Bobowiec       | 1 | ((190213-wygasniecie-starego-autosenta)) |
| Rafał Kamaron        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Rafał Roszczeniok    | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Roman Kłębek         | 1 | ((180814-trufle-z-kosmosu)) |
| Romeo Węglas         | 1 | ((190331-kurz-po-ataienne)) |
| Ronald Grzymość      | 1 | ((190616-anomalna-serafina)) |
| Sławomir Niejadek    | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Stach Sosnowiecki    | 1 | ((190208-herbata-grzyby-i-mimik)) |
| Staś Arienik         | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Szczepan Mensic      | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Szymon Grej          | 1 | ((181205-jeden-dokument-nie-w-pore)) |
| Tadeusz Rupczak      | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Tadeusz Sklerzec     | 1 | ((191112-korupcja-z-arystokratki)) |
| Tadeusz Tessalon     | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Teresa Marszalnik    | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Urszula Arienik      | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Wawrzyn Towarzowski  | 1 | ((181104-kotlina-duchow)) |
| Wioletta Kalazar     | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Wojciech Zermann     | 1 | ((190830-kto-wrobil-alana)) |
| Wojmił Siwywilk      | 1 | ((190422-pustogorski-konflikt)) |
| Zbigniew Burzycki    | 1 | ((181227-adieu-cieniaszczycie)) |