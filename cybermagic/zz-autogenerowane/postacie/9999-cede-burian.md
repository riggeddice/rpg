---
categories: profile
factions: 
owner: public
title: Cede Burian
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230613-zaginiecie-psychotronika-cede | przyjaciel Karolinusa; 31 lat, kompetentny psychotronik z noktiańskim szkoleniem. Zaplątał się w coś z Samszarami i zniknął. Bardzo rodzinny i sympatyczny. Przeprowadzał niedawno analizę Strzały. | 0095-08-02 - 0095-08-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Aleksander Samszar   | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Celina Burian        | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Fabian Samszar       | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Karolinus Samszar    | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Lara Ukraptin        | 1 | ((230613-zaginiecie-psychotronika-cede)) |