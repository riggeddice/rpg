---
categories: profile
factions: 
owner: public
title: Talarand d'Irrydius
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200510-tajna-baza-orbitera          | BIA wspomagająca Grzymościa, jego specjalnej bazy. Koordynator Kallisty; tym razem załadował Kallistę w wiedzę, że jest agentką Orbitera a nie Grzymościa na negocjacje z Pięknotką. | 0110-09-07 - 0110-09-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Artur Kołczond       | 1 | ((200510-tajna-baza-orbitera)) |
| Gabriel Ursus        | 1 | ((200510-tajna-baza-orbitera)) |
| Ignacy Myrczek       | 1 | ((200510-tajna-baza-orbitera)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Laura Tesinik        | 1 | ((200510-tajna-baza-orbitera)) |
| Mariusz Trzewń       | 1 | ((200510-tajna-baza-orbitera)) |
| Natalia Tessalon     | 1 | ((200510-tajna-baza-orbitera)) |
| Pięknotka Diakon     | 1 | ((200510-tajna-baza-orbitera)) |
| Sabina Kazitan       | 1 | ((200510-tajna-baza-orbitera)) |
| Teresa Mieralit      | 1 | ((200510-tajna-baza-orbitera)) |
| Tymon Grubosz        | 1 | ((200510-tajna-baza-orbitera)) |