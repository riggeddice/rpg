---
categories: profile
factions: 
owner: public
title: Kinga Stryk
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191108-ukojenie-aleksandrii         | egzorcystka i wojskowa. Skutecznie odwraca uwagę terminusa oraz przyzywa efemerydy. Katalistka i organiczny detektor magii. Pracuje z Zenkami i Amelią. | 0110-07-18 - 0110-07-20 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Mirzant       | 1 | ((191108-ukojenie-aleksandrii)) |
| Daniela Baszcz       | 1 | ((191108-ukojenie-aleksandrii)) |
| Klara Baszcz         | 1 | ((191108-ukojenie-aleksandrii)) |
| Leszek Baszcz        | 1 | ((191108-ukojenie-aleksandrii)) |
| Paweł Kukułnik       | 1 | ((191108-ukojenie-aleksandrii)) |