---
categories: profile
factions: 
owner: public
title: Lara Linven
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240204-skazona-symulacja-tabester   | czempionka na Tabester; osiągała wybitne wyniki i popisywała się stylem. Niestety, TAI Ashtaria ją złamała i udowodniła Larze, że nic nie może w świecie Ashtarii. | 0109-11-01 - 0109-11-04 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 240204-skazona-symulacja-tabester   | BROKEN przez TAI Ashtarię, ale wyciągnięta przez Martyna zanim złamanie zrobiło jej trwalszą krzywdę. Boi się wrócić do świata symulacji. | 0109-11-04

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ewaryst Kajman       | 1 | ((240204-skazona-symulacja-tabester)) |
| Klaudia Stryk        | 1 | ((240204-skazona-symulacja-tabester)) |
| Martyn Hiwasser      | 1 | ((240204-skazona-symulacja-tabester)) |
| OO Serbinius         | 1 | ((240204-skazona-symulacja-tabester)) |
| OOn Tabester         | 1 | ((240204-skazona-symulacja-tabester)) |
| TAI Ashtaria d'Tabester | 1 | ((240204-skazona-symulacja-tabester)) |