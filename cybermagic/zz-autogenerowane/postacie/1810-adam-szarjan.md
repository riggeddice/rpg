---
categories: profile
factions: 
owner: public
title: Adam Szarjan
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "niezrzeszeni"
* type: "NPC"
* owner: "public"
* title: "Adam Szarjan"


## Postać

### Ogólny pomysł (2)

As awianów oraz dziedzic imperium technomantycznego Orbitera Pierwszego. Nie ma problemów finansowych, jest doskonałym pilotem awianów i serwarów. Wojownik-konstruktor. Wielokrotny czempion różnego rodzaju starć maszyn. Jeśli ma wsparcie logistyczne, uznawany za niepokonanego.

### Co motywuje postać (2)

* Ludzie robią to, co uważają za możliwe -> Kierowany miłością do przekraczania granic przez ludzi. Kocha wysiłek i ogólnie rozumiane "pokonywanie siebie".
* Rzeczywistość jest niebezpieczna odkąd przyszły Zaćmienia -> Ludzka rasa i natura powinna po prostu przetrwać i rozkwitać w nowym świecie.
* Ludzie często robią rzeczy narażające innych ludzi a nie siebie samych -> Każdy ma prawo do ryzyka, jak długo to ryzyko osobiste - a nie dotyczące innych.
* wesoły, uprzejmy, lekko egzaltowany, arogancki, zawsze wspiera innych, zawsze pomocny, skupiony na swojej pracy

### Idealne okoliczności (2)

* W swoim awianie lub serwarze, wspomagany przez sojuszników, atakujący jakiegoś przeciwnika
* Wspomagany przez swoje laboratoria i konstrukcje, budujący kolejną customizację awiana lub serwara

### Szczególne umiejętności (3)

* Wybitnej klasy pilot awiana i serwara. Urodzony żołnierz i wojownik.
* Konstruktor awianów i serwarów. Zna na temat większości z nich słabości, siły oraz szczególne ciekawostki.
* Podnoszenie morale, motywowanie, doprowadzanie akcji do końca.

### Zasoby i otoczenie (3)

* Światowej klasy laboratoria i narzędzia konstrukcyjne w Orbiterze Pierwszym.
* Customizowany awian i serwar. Najwyższej klasy sprzęt militarny.
* Grupa szturmowa 'Koszmar', wybrana osobiście przez niego - ogromna siła ognia i siła technologiczna.
* Chwała i posłuch wynikające z jego pozycji, siły i podejścia. Jest bohaterem i wie o tym.
* Łatwe połączenia wszystkimi możliwymi linkami i liniami - jest w stanie używać stacji orbitalnej itp.

### Magia (3)

#### Gdy kontroluje energię

* Wszelkie formy wspomagania konstruowania awianów i serwarów. Manipulacja materią, metalami, energią i czystą technomancją.
* Magia entropiczna - destrukcja wszystkiego co NIE jest żywą, organiczną tkanką. Korozja, rozpad. Z wyłączeniem tkanki żywej.

#### Gdy traci kontrolę

* Entropiczne wspomaganie - jego magia wzmacnia ludzi i urządzenia jak hiperstymulanty, ale kosztem ich degeneracji i osłabienia. Jackup spike.
* Jego magia skupia się na ochronie i bunkrowaniu, kosztem zmniejszenia mobilności. Innymi słowy, fortyfikacja a nie działanie.
* Gdy jest w szczególnie złym humorze, jego magia entropiczna obraca się przeciwko istotom żywym i tkankom żywym. Świat dookoła niego zaczyna umierać.

### Powiązane frakcje

{{ page.factions }}

## Opis



# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181021-powrot-minerwy-z-terrorforma | o dziwo, przybył by pomóc Erwinowi bez ukrytej agendy. Dużo poświęcił, by pomóc "Nutce" stać się Minerwą. Bardzo pozytywna postać. | 0109-09-24 - 0109-09-29 |
| 181024-decyzja-minerwy              | dążył do zniszczenia psychotroniki Minerwy; bał się terrorforma i chciał, by Erwin oraz wszyscy dookoła byli bezpieczni. | 0109-10-01 - 0109-10-04 |
| 181118-romuald-i-julia              | hipernetowe źródło informacji dla Pięknotki odnośnie symboli z Laboratorium Technomantycznego Orbitera Pierwszego. | 0109-10-27 - 0109-10-29 |
| 190724-odzyskana-agentka-orbitera   | powiedział Pięknotce, że Aida nie jest przecież jakąś elitką czy nikim ważnym. Uratowana z kapsuły w kosmosie. | 0110-06-12 - 0110-06-15 |
| 211124-prototypowa-nereida-natalii  | by uratować Natalię wyciągnął swoimi kanałami Klaudię z procesu i przekazał Inferni info o projekcie "Nereida". Niestety, jego marzenia się nie spełniły - Natalia ucierpiała. | 0112-04-20 - 0112-04-24 |
| 211208-o-krok-za-daleko             | przerażony, wstrząśnięty i zszokowany wszystkim co wydarzyło się na Inferni - próba ekstrakcji ixiońskiej, integracji Eustachego... te śmierci itp. Oddelegowany na Infernię przez ojca (Jarka Szarjana). | 0112-04-25 - 0112-04-26 |
| 211215-sklejanie-inferni-do-kupy    | przekonany przez Ariannę, że ten ixioński potwór na Neotik jest prawdziwy i nikt nie wie o jego istnieniu bo potencjalnie współpracuje z Hestią. | 0112-04-27 - 0112-04-29 |
| 211222-kult-saitaera-w-neotik       | stoi za przekazaniem Nereidy na testy Elenie; w pełni współpracuje z Arianną i oddaje jej się pod komendę nieformalnie. Przekonał Stocznię, że Nereidę przekazaną Elenie należy prawidłowo uzbroić. | 0112-04-30 - 0112-05-01 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181024-decyzja-minerwy              | bardzo nieufny wobec Minerwy; ogólnie, pilnuje, by nic złego się nie stało | 0109-10-04
| 211208-o-krok-za-daleko             | oddelegowany na Infernię przez Jarosława Szarjana jako wsparcie dla Inferni i obserwator, by Natalia się dobrze wykluła. | 0112-04-26

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 4 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Elena Verlen         | 4 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Eustachy Korkoran    | 4 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Pięknotka Diakon     | 4 | ((181021-powrot-minerwy-z-terrorforma; 181024-decyzja-minerwy; 181118-romuald-i-julia; 190724-odzyskana-agentka-orbitera)) |
| Diana d'Infernia     | 3 | ((211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Klaudia Stryk        | 3 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik)) |
| Leona Astrienko      | 3 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy)) |
| Maria Naavas         | 3 | ((211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| OO Infernia          | 3 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy)) |
| Izabela Zarantel     | 2 | ((211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik)) |
| Julia Morwisz        | 2 | ((181118-romuald-i-julia; 190724-odzyskana-agentka-orbitera)) |
| Kasandra Destrukcja Diakon | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Lilia Ursus          | 2 | ((181021-powrot-minerwy-z-terrorforma; 181024-decyzja-minerwy)) |
| Roland Sowiński      | 2 | ((211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Wawrzyn Rewemis      | 2 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| Adela Kirys          | 1 | ((181024-decyzja-minerwy)) |
| Aida Serenit         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Atena Sowińska       | 1 | ((181021-powrot-minerwy-z-terrorforma)) |
| Erwin Galilien       | 1 | ((181021-powrot-minerwy-z-terrorforma)) |
| Hestia d'Neotik      | 1 | ((211208-o-krok-za-daleko)) |
| Jarosław Szarjan     | 1 | ((211208-o-krok-za-daleko)) |
| Kamil Lyraczek       | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Lutus Amerin         | 1 | ((211222-kult-saitaera-w-neotik)) |
| Maciek Kalmarzec     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Minerwa Metalia      | 1 | ((181024-decyzja-minerwy)) |
| Mirela Niecień       | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Mirela Orion         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Moktar Gradon        | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Natalia Aradin       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Pietro Dwarczan      | 1 | ((181118-romuald-i-julia)) |
| Romuald Czurukin     | 1 | ((181118-romuald-i-julia)) |