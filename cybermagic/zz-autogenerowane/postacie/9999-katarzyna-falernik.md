---
categories: profile
factions: 
owner: public
title: Katarzyna Falernik
---

# {{ page.title }}


# Generated: 



## Fiszki


* talented and respected hydrologist, responsible for managing and optimizing the arcology's water supply and recycling systems. | @ 230329-zdrada-rozrywajaca-arkologie
* ENCAO:  0-+++ |Intrygancka, lubi sieci intryg i politykę;;Niefrasobliwa, beztroska;;Oczytana, ceni wiedzę dla wiedzy| VALS: Achievement, Security >> Self-direction| DRIVE: Papa Smerf (godzinami o wodzie) | @ 230329-zdrada-rozrywajaca-arkologie
* trochę jak smerf ważniak | @ 230329-zdrada-rozrywajaca-arkologie

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230329-zdrada-rozrywajaca-arkologie | ekspertka hydrologii, specjalizująca się w optymalizacji arkologii. Sama zaproponowała Szczepanowi, żeby Kidiron ją przesłuchał pod neuroobrożą. Nie zdradza Szczepana. | 0093-03-14 - 0093-03-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Ardilla Korkoran     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| BIA Prometeus        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Celina Lertys        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Eustachy Korkoran    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Franciszek Pietraszczyk | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kalia Awiter         | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Małgorzata Maratelus | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| OO Infernia          | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Rafał Kidiron        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Ralf Tapszecz        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Wojciech Grzebawron  | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |