---
categories: profile
factions: 
owner: public
title: TAI Eszara d'Castigator
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231025-spiew-nielalki-na-castigatorze | nieświadoma infekcji Altarient, próbuje najlepiej jak potrafi zarządzać Castigatorem i utrzymała go w kupie mimo, że jej własne znaczenia się rozsypały i np. 'nie czuje hangaru ŁeZ'. Prawidłowo zrobiła 'graceful degradation' i jak tylko zorientowała się że nic nie działa, słuchała rozkazów Kurzmina a potem przestała robić cokolwiek (bo Znaczenia mogły się za mocno pozmieniać) | 0110-10-24 - 0110-10-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anna Tessalon        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Arianna Verlen       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Elena Verlen         | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Eustachy Korkoran    | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Feliks Walrond       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Igor Arłacz          | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Klaudia Stryk        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Konstanty Keksik     | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Leona Astrienko      | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Leszek Kurzmin       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Marta Keksik         | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| OO Castigator        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| OO Infernia          | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Patryk Samszar       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Raoul Lavanis        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |