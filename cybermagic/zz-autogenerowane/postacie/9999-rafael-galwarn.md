---
categories: profile
factions: 
owner: public
title: Rafael Galwarn
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210609-sekrety-kariatydy            | inspektor Orbitera, siły specjalne K1. Ukrywał informacje o Kariatydzie aż Arianna i Eustachy weszli mu w szkodę. Zaakceptował pomysł "konkursu i reklamy" i użył środków K1 by prawda nie wyszła na jaw. | 0112-01-10 - 0112-01-13 |
| 210630-listy-od-fanow               | bezpiecznik z K1 któremu nie podoba się to co się dzieje na "Lordzie Savaronie". Dowiedziawszy się prawdy, tak powiedział Ariannie o wszystkim by ta wiedziała, że ma procedować. W odróżnieniu od Kariatydy ;-). | 0112-01-15 - 0112-01-18 |
| 211114-admiral-gruby-wieprz         | oddał Klaudii (na jej prośbę) sprawę "fałszywego SOS" (przestraszonych TAI z virtsystemu). Klaudia ukryła przed nim prawdę i to że chroni te TAI. Ma jej to za złe. | 0112-02-15 - 0112-02-20 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((210609-sekrety-kariatydy; 210630-listy-od-fanow)) |
| Izabela Zarantel     | 2 | ((210630-listy-od-fanow; 211114-admiral-gruby-wieprz)) |
| Klaudia Stryk        | 2 | ((210630-listy-od-fanow; 211114-admiral-gruby-wieprz)) |
| Adalbert Brześniak   | 1 | ((211114-admiral-gruby-wieprz)) |
| Bogdan Anatael       | 1 | ((210630-listy-od-fanow)) |
| Elena Verlen         | 1 | ((210630-listy-od-fanow)) |
| Eustachy Korkoran    | 1 | ((210609-sekrety-kariatydy)) |
| Marian Tosen         | 1 | ((210609-sekrety-kariatydy)) |
| Michał Teriakin      | 1 | ((210630-listy-od-fanow)) |
| OE Lord Savaron      | 1 | ((210630-listy-od-fanow)) |
| Olgierd Drongon      | 1 | ((210630-listy-od-fanow)) |
| OO Kariatyda         | 1 | ((210609-sekrety-kariatydy)) |
| Remigiusz Falorin    | 1 | ((210630-listy-od-fanow)) |
| Roland Sowiński      | 1 | ((210609-sekrety-kariatydy)) |
| Roman Panracz        | 1 | ((211114-admiral-gruby-wieprz)) |
| Rzieza d'K1          | 1 | ((211114-admiral-gruby-wieprz)) |
| Sabina Kazitan       | 1 | ((211114-admiral-gruby-wieprz)) |
| TAI Rzieza d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI XT-723 d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI Zefiris          | 1 | ((210630-listy-od-fanow)) |