---
categories: profile
factions: 
owner: public
title: Caelia Calaris
---

# {{ page.title }}


# Generated: 



## Fiszki


* savarańska lekarka Kajrata | @ 230906-operacja-mag-dla-symlotosu
* OCEAN: (E-N+A+): wyjątkowo nieruchoma i ostrożna społecznie NAWET jak na savarankę, ale empatyczna i przyjacielska. Przytulna. "Oszczędzajmy wszystko." | @ 230906-operacja-mag-dla-symlotosu
* VALS: (Harmony, Achievement): robi co może tym co ma i dba o morale jak umie. "Razem sobie poradzimy." | @ 230906-operacja-mag-dla-symlotosu
* Core Wound - Lie: "Calaris, macierzysta jednostka umarła przez brak zasobów" - "Jeśli będziemy oszczędzać na wszystkim, przetrwamy! Nie kosztem załogi, nigdy!" | @ 230906-operacja-mag-dla-symlotosu
* Styl: Całkowicie nieruchoma, unika bycia pod słońcem, mało mówi i bardzo ciężko pracuje. Wrażliwa, widać ślady łez. Słaba fizycznie z nieprzystosowania planetarnego. | @ 230906-operacja-mag-dla-symlotosu

### Wątki


pronoktianska-mafia-kajrata
furia-mataris-agentka-mafii

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230806-zwiad-w-iliminar-caos        | lekarka utrzymująca Amandę i Xaverę w najlepszym stanie jak potrafi. Praktycznie się nie rusza, creepy nawet jak na savarankę. | 0081-06-22 - 0081-06-24 |
| 230906-operacja-mag-dla-symlotosu   | savarańska lekarka nie jest fanką dzielenia się medykamentami, bo siły Kajrata mają ich po prostu za mało. Ale wykonuje polecenie. | 0081-06-26 - 0081-06-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Dragan Halatis       | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Ernest Kajrat        | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Leon Varkas          | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Lestral Kirmanik     | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Alaric Rakkeir       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Edmund Garzin        | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Isaura Velaska       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Xavera Sirtas        | 1 | ((230906-operacja-mag-dla-symlotosu)) |