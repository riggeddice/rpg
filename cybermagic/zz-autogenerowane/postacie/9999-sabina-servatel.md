---
categories: profile
factions: 
owner: public
title: Sabina Servatel
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200819-sekrety-orbitera-historia-prawdziwa | eterniańska szlachcianka nad Tadeuszem; drażni ją nędzne podejście Tadeusza do Eleny. Albo jest silny i ją bierze, albo słaby i się odczepi. | 0111-01-24 - 0111-02-01 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 200819-sekrety-orbitera-historia-prawdziwa | bezpośrednia a-tien Tadeusza Ursusa, z Eterni. Jest gdzieś w kosmosie, niedaleko Astorii. W koloidowym statku niewolniczym? | 0111-02-01

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Damian Orion         | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Elena Verlen         | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Eustachy Korkoran    | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Izabela Zarantel     | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Klaudia Stryk        | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Leona Astrienko      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Leszek Kurzmin       | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Martyn Hiwasser      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Tadeusz Ursus        | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |