---
categories: profile
factions: 
owner: public
title: Kratos Coruscatis
---

# {{ page.title }}


# Generated: 



## Fiszki


* wspomagany komandos noktiański; posiada wyrzutnię rakiet i servar klasy Fulmen, dekadianin, CES Coruscatis | @ 230201-wylaczone-generatory-memoriam-inferni
* (ENCAO:  --+00 |Kocha silny stres;;Introspektywny;;Pracowity| VALS: Benevolence, Conformity >> Family| DRIVE: Zwyciężyć w debacie o współpracy z Neikatis) | @ 230201-wylaczone-generatory-memoriam-inferni
* "Od początku mówiłem, że ta współpraca z Nativis się nie uda. Noctis powinna współpracować z noktianami." | @ 230201-wylaczone-generatory-memoriam-inferni

### Wątki


infernia-jej-imieniem
kidiron-zbawca-nativis

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230201-wylaczone-generatory-memoriam-inferni | dekadiański wspomagany komandos noktiański; posiada wyrzutnię rakiet i servar klasy Fulmen; (ENCAO:  --+00 |Kocha silny stres;;Introspektywny;;Pracowity| VALS: Benevolence, Conformity >> Family| DRIVE: Zwyciężyć w debacie o współpracy z Neikatis). Przejął kontrolę nad Coruscatis i bezpiecznie wyprowadził wszystkich z Coruscatis broniąc ich przed Amalgamoidem Trianai. To on zorientował się, że nie są w stanie uratować Coruscatis i on wysłał sygnał SOS. | 0093-02-10 - 0093-02-12 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Bartłomiej Korkoran  | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Celina Lertys        | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Eustachy Korkoran    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Jan Lertys           | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kalia Awiter         | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| OO Infernia          | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Rafał Kidiron        | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Ralf Tapszecz        | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Tymon Korkoran       | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |