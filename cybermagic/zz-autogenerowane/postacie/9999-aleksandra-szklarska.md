---
categories: profile
factions: 
owner: public
title: Aleksandra Szklarska
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200219-perfekcyjna-corka            | matka Leszka (szefa Liberatis), terminuska. Nie jest w stanie kontrolować swojego syna. Wpadła w kłopoty, bo uznano, że cała ta anarchia jest jej winą. | 0110-05-16 - 0110-05-19 |
| 200222-rozbrojenie-bomby-w-kalbarku | terminuska; Skażona kralotycznym wspomaganiem i pokonana przez Pięknotkę. Bardzo ciężki stan. | 0110-07-27 - 0110-08-01 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ataienne             | 2 | ((200219-perfekcyjna-corka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Mateusz Kardamacz    | 2 | ((200219-perfekcyjna-corka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Bartłomiej Małczarek | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Diana Tevalier       | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Lena Kardamacz       | 1 | ((200219-perfekcyjna-corka)) |
| Leszek Szklarski     | 1 | ((200219-perfekcyjna-corka)) |
| Lucjusz Blakenbauer  | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Paweł Oszmorn        | 1 | ((200219-perfekcyjna-corka)) |
| Pięknotka Diakon     | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Sabina Kazitan       | 1 | ((200219-perfekcyjna-corka)) |
| Tymon Grubosz        | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |