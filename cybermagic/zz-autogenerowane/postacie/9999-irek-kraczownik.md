---
categories: profile
factions: 
owner: public
title: Irek Kraczownik
---

# {{ page.title }}


# Generated: 



## Fiszki


* bojący się Blakenbauerów egzorcysta chroniący ludzi | @ 230711-zablokowana-sentisiec-w-krainie-makaronu
* OCEAN: (N+C-): zrezygnowany, wiecznie zmęczony, szybko zmienia tematy, szuka następnego zagrożenia, "Muszę to zrobić. Dla nich!" | @ 230711-zablokowana-sentisiec-w-krainie-makaronu
* VALS: (Benevolence, Security): chronić ludzi nawet kosztem siebie, "Nie chodzi o mnie, chodzi o nich!" | @ 230711-zablokowana-sentisiec-w-krainie-makaronu
* Core Wound - Lie: "Magowie Aurum zniszczyli mnie i wszystko o co walczyłem" - "Ja już się nie liczę, mnie nic nie uratuje. Chodzi o nich." | @ 230711-zablokowana-sentisiec-w-krainie-makaronu
* styl: znerwicowany panikarz który robi co może by chronić ludzi i przekonać innych do tego samego. Unika magów, raczej chce siedzieć z innymi ludźmi. | @ 230711-zablokowana-sentisiec-w-krainie-makaronu

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230404-wszystkie-duchy-siewczyna    | niekompetentny egzorcysta, który przypałętał na ten teren Hybrydę noktiańskiej TAI i ducha zemsty 3 lata temu. Nieobecny na TEJ sesji, acz jej przyczyna. | 0095-07-18 - 0095-07-20 |
| 230411-egzorcysta-z-sanktuarium     | kiedyś egzorcysta, teraz zabija się i oddaje swoją energię by tylko utrzymać Sanktuarium (swój nowy dom) przy życiu. Porwany przez Karolinusa Samszara przy biernym współudziale Eleny Samszar. | 0095-07-21 - 0095-07-23 |
| 230509-samszarowie-lemurczak-i-fortel-strzaly | próbuje przekonać Samszarów, że Aurum jest z dupy i on próbuje naprawdę chronić ludzi. Opowiedział, jak kiedyś Blakenbauerowie go krzywdzili. Zależy mu na Sanktuarium. Przekonał (compel) Elenę, by ona wstawiła się za Sanktuarium Kazitan i za to wsparł jej cele (by Elena i Karolinus wyszli na bohaterów a nie porywaczy). | 0095-07-24 - 0095-07-26 |
| 230711-zablokowana-sentisiec-w-krainie-makaronu | wypuszczony przez Samszarów, dołączył do Grupy Proludzkiej Aurum i pomaga ludziom w Triticatus jak jest w stanie. Wpierw odbił się od Petry - tience nie chce się 'robić', potem temat przejął Karolinus i Elena. Ale Irek robi co może by ratować ludzi. Postawił reputację na szali, by Karolinus dostał szansę w GPA. | 0095-09-05 - 0095-09-08 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230509-samszarowie-lemurczak-i-fortel-strzaly | straszna trauma i strach przed magami Aurum. Boi się ich i nie chce współpracować. Kiedyś był torturowany przez Blakenbauerów. | 0095-07-26

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Elena Samszar        | 4 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Karolinus Samszar    | 4 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| AJA Szybka Strzała   | 3 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Arnold Kazitan       | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Ignatus Blakenbauer  | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Jonatan Lemurczak    | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Maksymilian Sforzeczok | 1 | ((230404-wszystkie-duchy-siewczyna)) |
| Petra Samszar        | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Roland Samszar       | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Tadeusz Dzwańczak    | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Wacław Samszar       | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Wiktor Blakenbauer   | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |