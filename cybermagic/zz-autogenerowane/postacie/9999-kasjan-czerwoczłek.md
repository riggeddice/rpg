---
categories: profile
factions: 
owner: public
title: Kasjan Czerwoczłek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190106-a-moze-pustogorska-mafia     | młody mag podkochujący się w Adeli, który zaatakował gabinet Pięknotki - przez co spadły na niego straszne problemy | 0109-12-24 - 0109-12-26 |
| 190505-szczur-ktory-chroni          | pomógł Adeli w autoryzacji starej generacji konstruminusów; podpuszczony przez Pięknotkę wyleciał od Pietra. Stoi po stronie Adeli, nie władz. | 0110-04-22 - 0110-04-24 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190106-a-moze-pustogorska-mafia     | wydalony z Pustogoru za atak na gabinet Pięknotki, trafił do Cieniaszczytu pod opiekę Pietra | 0109-12-26

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adela Kirys          | 2 | ((190106-a-moze-pustogorska-mafia; 190505-szczur-ktory-chroni)) |
| Pięknotka Diakon     | 2 | ((190106-a-moze-pustogorska-mafia; 190505-szczur-ktory-chroni)) |
| Alan Bartozol        | 1 | ((190505-szczur-ktory-chroni)) |
| Amadeusz Sowiński    | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Ernest Kajrat        | 1 | ((190505-szczur-ktory-chroni)) |
| Krystian Namałłek    | 1 | ((190505-szczur-ktory-chroni)) |
| Lucjusz Blakenbauer  | 1 | ((190505-szczur-ktory-chroni)) |
| Oliwia Namałłek      | 1 | ((190505-szczur-ktory-chroni)) |
| Roland Grzymość      | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Tadeusz Rupczak      | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Waldemar Mózg        | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Wiktor Satarail      | 1 | ((190505-szczur-ktory-chroni)) |