---
categories: profile
factions: 
owner: public
title: Kamil Frederico
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210108-ratunkowa-misja-goldariona   | degenerat z Luxuritias. Właściciel "Uśmiechniętej", porzucił ją i uciekł z załogą. Miłośnik wił. Robił straszną krzywdę wszelkim damom, jakie pojawiały się na pokładzie tej jednostki. | 0111-08-09 - 0111-08-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Permin          | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Aleksander Leszert   | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Elena Verlen         | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Feliks Przędz        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Klaudia Stryk        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Martyn Hiwasser      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Oliwia Pietrova      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| SCA Goldarion        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Semla d'Goldarion    | 1 | ((210108-ratunkowa-misja-goldariona)) |
| SL Uśmiechnięta      | 1 | ((210108-ratunkowa-misja-goldariona)) |