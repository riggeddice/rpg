---
categories: profile
factions: 
owner: public
title: Teresa Mieralit
---

# {{ page.title }}


# Generated: 



## Fiszki


* nauczycielka magii leczniczej i katalistka (disruptorka magii) w Szkole Magów | @ 230303-the-goose-from-hell
* (ENCAO:  +0+-+ |Manipulatorka i meddler;; Kreatywna, tworząca wiecznie coś nowego| VALS: Benevolence, Achievement >> Face, Security| DRIVE: AMZ będzie najskuteczniejsze) | @ 230303-the-goose-from-hell
* 16 lat: affinity Interis (negamagia) + Exemplis (lecznicza i wzmacniająca); świetna akrobatka. Nie umie walczyć: flashbacks śmierci rodziców (FREEZE) | @ 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211010-ukryta-wychowanka-arnulfa    | 15 lat; disruptorka magii i paramedyk; noktianka pod opieką dyrektora Arnulfa (jego wychowanka). Ma niewyparzoną gębę. Jej obecność spowodowała kolizję w Strażniczce - elementy BIA wykryły jako "friend", elementy TAI jako "foe". | 0084-12-11 - 0084-12-12 |
| 211017-nastolatka-w-bieliznie-na-dachu-w-burzy | noktiańska czarodziejka z deathwish?; trochę się boi astorian i trochę ich nienawidzi, więc jest na uboczu. Gdy terminus infiltrował AMZ rozebrała się do bielizny (by nie uszkodzić ubrania) i schowała się na dachu w burzy na Złomiarium dla dreszczyka. Uratowana przez Klaudię, podejrzana o prostytucję i współpracę z Grzymościem (o którym nawet nie wie), skończyła śpiąc na łóżku Klaudii a potem - w pokoju z nią i Ksenią. Całkowicie dzika, niezsocjalizowana. | 0084-12-14 - 0084-12-15 |
| 211019-czarodziejka-ktora-jednak-moze-sie-zabic | powoli się oswaja z Klaudią i Ksenią, przez co je odpycha. Przestraszona tym, że pojawiły się seksboty "skrzywdź noktiankę". Świetna w negamagii, ma naturalny talent; rozbiła nałożony na nią geas. | 0084-12-20 - 0084-12-24 |
| 240114-o-seksbotach-i-syntetycznych-intelektach | cała czerwona (podsłuchuje rozmowę Klaudii i Talii) odnośnie seksbotów i projektu; gdy jedna z uczennic śmieje się z Klaudii że ona uczy się od seksbota, Teresa ją zaatakowała pięścią (i dostała wpierdol). Sama rozwiązuje swoje problemy. Uważa Talię Aegis za 'zdradziecką noktiankę' i nie chce mieć z nią nic do czynienia. | 0085-01-13 - 0085-01-18 |
| 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie | Została dotknięta przez manifestację Alucis Księżniczki Arianny; szukała w lesie swoich rodziców i prawie została porwana. Gdy Trzewń i Klaudia ją spytali co się dzieje, powiedziała im co wie. Poproszona o pomoc w ratowaniu dzieci, poszła mimo że bała się magii. Retraumatyzowana przez sceny wojenne, potem przez wizję swoich rodziców, POTEM gdy Alucis zmieniło się w Esuriit. Ale pomogła. | 0085-01-26 - 0085-01-28 |
| 190101-morderczyni-jednej-plotki    | nauczycielka w AMZ. Bardzo (zbyt) zainteresowana Mrocznymi Drżeniami Cieniaszczytu, cokolwiek to jest. Agentka Czerwonych Myszy i agentka Dare Shiver. Zafascynowana Cieniaszczytem i Cieniaszczycką kulturą. | 0109-12-13 - 0109-12-17 |
| 190102-stalker-i-czerwone-myszy     | przyszła skonsumować darmowy kupon od Pięknotki i wyszła jako megaepicka reklama gabinetu Pięknotki. Pięknotka przeszła samą siebie. | 0109-12-18 - 0109-12-21 |
| 190113-chronmy-karoline-przed-uczniami | nauczycielka magii leczniczej i katalistka w Szkole Magów. Pomogła Lilianie. | 0110-01-05 - 0110-01-06 |
| 190519-uciekajacy-seksbot           | pancerz dyrektora w szkole magów, zwalczająca Ernesta i biorąca potencjalny ogień na siebie. Co udowadnia, że jest baaardzo nierozsądna. | 0110-04-25 - 0110-04-26 |
| 200326-test-z-etyki                 | Nauczycielka etyki ORAZ agentka Dare Shiver. Wpierw dostarczyła Lilianie narzędzia do robienia problemów a potem poszczuła ją dwoma uczennicami. I nic nie musiała robić. | 0110-07-29 - 0110-07-31 |
| 200510-tajna-baza-orbitera          | poproszona przez Pięknotkę, by skupić się na Myrczku. On ma trochę za dużo czasu i podkochuje się w Sabinie Kazitan, co do niczego nie prowadzi. Obiecała, że go od niej odsunie. | 0110-09-07 - 0110-09-11 |
| 201013-pojedynek-akademia-rekiny    | miesiąc temu katalitycznie rozproszyła efemerydę złożoną przez bitwę studentów AMZ vs Rekiny. Zgodziła się by Napoleon pożyczył ścigacz z artefaktorium. | 0110-10-14 - 0110-10-22 |
| 211026-koszt-ratowania-torszeckiego | dla Pawła Szprotki jest "Damą w Błękicie". Chroni go i daje mu pracę, bo chce jego powodzenia. Tak jak kiedyś Klaudia i Ksenia chroniły ją. I ofc Arnulf. | 0111-08-01 - 0111-08-05 |
| 230303-the-goose-from-hell          | An ethics teacher who recruited Carmen, Alex, and Julia to solve the goose problem. She was the only one who had a good night sleep while the students were sleeping on the top of a building guarding the captured goose. | 0111-10-28 - 0111-10-30 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211017-nastolatka-w-bieliznie-na-dachu-w-burzy | spisana za podejrzenie prostytucji (SRS!). Podejrzewa ją o to Sasza i pół AMZ po nocnym spacerze w "lekkim stroju". | 0084-12-15
| 211017-nastolatka-w-bieliznie-na-dachu-w-burzy | mieszka w akademiku AMZ z Ksenią i Klaudią. | 0084-12-15
| 211019-czarodziejka-ktora-jednak-moze-sie-zabic | miała na sobie geas uniemożliwiający jej zabicie się, założony przez jej ojca przed śmiercią. Geas już nie działa, rozproszony przez jej negamagię. | 0084-12-24
| 211019-czarodziejka-ktora-jednak-moze-sie-zabic | nie ma pieniędzy, nie pożyczy i dlatego uważa swoje ciuchy za praktyczne. Arnulf dał jej ubrania po córce i nie kupiła nic nowego. | 0084-12-24
| 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie | retraumatyzowana przez transformację manifestacji jej rodziców z Alucis w Esuriit, ze złamanym przez to wszystko sercem ponownie. | 0085-01-28

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arnulf Poważny       | 6 | ((190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Klaudia Stryk        | 5 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie; 240114-o-seksbotach-i-syntetycznych-intelektach)) |
| Ksenia Kirallen      | 5 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 211026-koszt-ratowania-torszeckiego; 240114-o-seksbotach-i-syntetycznych-intelektach)) |
| Pięknotka Diakon     | 5 | ((190101-morderczyni-jednej-plotki; 190102-stalker-i-czerwone-myszy; 190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 200510-tajna-baza-orbitera)) |
| Ignacy Myrczek       | 4 | ((200326-test-z-etyki; 200510-tajna-baza-orbitera; 201013-pojedynek-akademia-rekiny; 211026-koszt-ratowania-torszeckiego)) |
| Liliana Bankierz     | 4 | ((190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 200326-test-z-etyki; 201013-pojedynek-akademia-rekiny)) |
| Mariusz Trzewń       | 3 | ((200510-tajna-baza-orbitera; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Napoleon Bankierz    | 3 | ((190113-chronmy-karoline-przed-uczniami; 200326-test-z-etyki; 201013-pojedynek-akademia-rekiny)) |
| Sasza Morwowiec      | 3 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 240114-o-seksbotach-i-syntetycznych-intelektach)) |
| Talia Aegis          | 3 | ((211010-ukryta-wychowanka-arnulfa; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 240114-o-seksbotach-i-syntetycznych-intelektach)) |
| Tymon Grubosz        | 3 | ((200510-tajna-baza-orbitera; 211010-ukryta-wychowanka-arnulfa; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Julia Kardolin       | 2 | ((201013-pojedynek-akademia-rekiny; 230303-the-goose-from-hell)) |
| Paweł Szprotka       | 2 | ((211026-koszt-ratowania-torszeckiego; 230303-the-goose-from-hell)) |
| Adela Kirys          | 1 | ((190113-chronmy-karoline-przed-uczniami)) |
| Alan Bartozol        | 1 | ((190101-morderczyni-jednej-plotki)) |
| Aleksander Bemucik   | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Aleksander Iczak     | 1 | ((190101-morderczyni-jednej-plotki)) |
| Alex Deverien        | 1 | ((230303-the-goose-from-hell)) |
| Alicja Trawlis       | 1 | ((230303-the-goose-from-hell)) |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Aniela Kark          | 1 | ((200326-test-z-etyki)) |
| Artur Kołczond       | 1 | ((200510-tajna-baza-orbitera)) |
| Berenika Wrążowiec   | 1 | ((200326-test-z-etyki)) |
| Carmen Deverien      | 1 | ((230303-the-goose-from-hell)) |
| Eliza Ira            | 1 | ((190519-uciekajacy-seksbot)) |
| Ernest Kajrat        | 1 | ((190519-uciekajacy-seksbot)) |
| Erwin Galilien       | 1 | ((190101-morderczyni-jednej-plotki)) |
| Felicjan Szarak      | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Gabriel Ursus        | 1 | ((200510-tajna-baza-orbitera)) |
| Jan Kramczuk         | 1 | ((190102-stalker-i-czerwone-myszy)) |
| Justynian Diakon     | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Kacper Bankierz      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Kaella Sarimanis     | 1 | ((231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Karol Szurnak        | 1 | ((190101-morderczyni-jednej-plotki)) |
| Karolina Erenit      | 1 | ((190113-chronmy-karoline-przed-uczniami)) |
| kot-pacyfikator Tobias | 1 | ((230303-the-goose-from-hell)) |
| Laura Tesinik        | 1 | ((200510-tajna-baza-orbitera)) |
| Maryla Koternik      | 1 | ((211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Marysia Sowińska     | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Natalia Tessalon     | 1 | ((200510-tajna-baza-orbitera)) |
| Olaf Zuchwały        | 1 | ((190101-morderczyni-jednej-plotki)) |
| Olga Myszeczka       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Ossidia Saitis       | 1 | ((190519-uciekajacy-seksbot)) |
| Rafał Torszecki      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Ralena Drewniak      | 1 | ((240114-o-seksbotach-i-syntetycznych-intelektach)) |
| Remor 340D           | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Robert Pakiszon      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Sabina Kazitan       | 1 | ((200510-tajna-baza-orbitera)) |
| Saitaer              | 1 | ((190519-uciekajacy-seksbot)) |
| Sensacjusz Diakon    | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Stella Armadion      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Strażniczka Alair    | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Tomasz Tukan         | 1 | ((190519-uciekajacy-seksbot)) |
| Waldemar Grzymość    | 1 | ((211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Waldemar Mózg        | 1 | ((190102-stalker-i-czerwone-myszy)) |
| Wiktor Satarail      | 1 | ((211026-koszt-ratowania-torszeckiego)) |