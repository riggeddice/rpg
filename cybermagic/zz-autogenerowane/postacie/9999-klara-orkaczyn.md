---
categories: profile
factions: 
owner: public
title: Klara Orkaczyn
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190702-smieciornica-w-magitrowni    | lokalny szczur i lekarz w Maczkowcu; leczy, wykrywa i używa magii by dojść do prawdy odnośnie śmieciornicy, Patrycji i efemerydy. | 0109-07-14 - 0109-07-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alia Naszemba        | 1 | ((190702-smieciornica-w-magitrowni)) |
| Irek Waczar          | 1 | ((190702-smieciornica-w-magitrowni)) |
| Jan Waczar           | 1 | ((190702-smieciornica-w-magitrowni)) |
| Patrycja Radniak     | 1 | ((190702-smieciornica-w-magitrowni)) |
| Przemysław Grumcz    | 1 | ((190702-smieciornica-w-magitrowni)) |