---
categories: profile
factions: 
owner: public
title: Napoleon Myszogłów
---

# {{ page.title }}


# Generated: 



## Fiszki


* (ENCAO:  +0--- |Prawi wszystkim morały;;;;Bardzo lubi być w środku uwagi| VALS: Universalism, Conformity| DRIVE: Urażony szacunek) | @ 230113-ros-adrienne-a-new-recruit
* atarienin: "Co zrobisz to dostaniesz" | @ 230113-ros-adrienne-a-new-recruit
* kadencja: smerf ważniak | @ 230113-ros-adrienne-a-new-recruit
* "Jesteście w stanie osiągnąć coś lepszego niż macie, tylko musicie się wykazać i udowodnić, że można Wam ufać" | @ 230113-ros-adrienne-a-new-recruit
* SPEC: ? | @ 230113-ros-adrienne-a-new-recruit
* SPEC: dyplomacja między ROS a stacją Orbitera | @ 230117-ros-wiertloplyw-i-tai-mirris

### Wątki


ratownicy-ostatniej-szansy

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230113-ros-adrienne-a-new-recruit   | promotes Adrianna and asks for her to have a chance in the ROS noktian team. Made an impression on young Adrianna. | 0083-12-07 - 0083-12-10 |
| 230117-ros-wiertloplyw-i-tai-mirris | dowódca Błyskawicy; bardzo nie ufa Mirris ale dał się przekonać Zespołowi by spróbować po ichniemu. Jego pogląd na temat TAI się nie zmienił, ale pogląd na temat noktian - tak. Na lepsze. Obrzydzenie do "żywych TAI". | 0084-02-22 - 0084-02-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Niferus Sentriak     | 2 | ((230113-ros-adrienne-a-new-recruit; 230117-ros-wiertloplyw-i-tai-mirris)) |
| Talia Irris          | 2 | ((230113-ros-adrienne-a-new-recruit; 230117-ros-wiertloplyw-i-tai-mirris)) |
| Tristan Andrait      | 2 | ((230113-ros-adrienne-a-new-recruit; 230117-ros-wiertloplyw-i-tai-mirris)) |
| Adrianna Kastir      | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Aster Sarvinn        | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Eleonora Alintirias  | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Hel Otereien         | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Kasia Karmnik        | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Lucas Septrien       | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Łucja Neiser         | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Łucja Nirimis        | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| OR Błyskawica        | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| SC Wiertłopływ       | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| TAI Mirris d'Paravilius | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |