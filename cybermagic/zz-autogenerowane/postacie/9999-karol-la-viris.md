---
categories: profile
factions: 
owner: public
title: Karol La Viris
---

# {{ page.title }}


# Generated: 



## Fiszki


* Cyberwspomagany oficer wojskowy | @ 230831-potwory-ktore-przetrwaly-eter
* OCEAN: (A-E+): Zdecydowany i agresywny. Maskuje rozpacz agresją. Deathwish. | @ 230831-potwory-ktore-przetrwaly-eter
* VALS: (Power, Security): "Ochronię wszystkich. Jakoś. Nie wiem jak. Dowolna broń." | @ 230831-potwory-ktore-przetrwaly-eter
* Core Wound - Lie: "Potwory niszczą mój świat a ja jestem bezradny!" - "SZTUCZNY OPTYMIZM I ZNISZCZENIE POTWÓR!" | @ 230831-potwory-ktore-przetrwaly-eter
* Styl: "Wszystko będzie dobrze /otwarty śmiech" | @ 230831-potwory-ktore-przetrwaly-eter
* Metakultura: Atarien ("wszyscy polegają na mnie a ja dowodzę... tym co mam.") | @ 230831-potwory-ktore-przetrwaly-eter

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230831-potwory-ktore-przetrwaly-eter | cyberwspomagany oficer wojskowy z Travistas, nadrabia miną; próbuje twardo robić sojusze by pozbyć się potworów, ale dał się przekonać Mimikowi (wierzy, że ten jest poprzednim komendantem stacji Damnos). Po prawdzie, nie ma siły ognia na pokonanie potworów; ważniejsze jest przetrwanie... | 0093-03-11 - 0093-03-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Mardiblon     | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Bartek Mardiblon     | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Dawid Nałęcznik      | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Diana Nałęcznik      | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Esaria Mirtalis      | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Mateusz Owisiec      | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |
| Rosenkrat Amiribasit | 1 | ((230831-potwory-ktore-przetrwaly-eter)) |