---
categories: profile
factions: 
owner: public
title: Tadeusz Samszar
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (C+A-): pozornie niezniszczalny, bezwzględny, kontrolujący. "Moje zasady, moja kontrola. Działasz jak sobie życzę lub lecisz. Każdego da się zastąpić." | @ 230715-amanda-konsoliduje-zloty-cien
* VALS: (Power, Achievement): uwielbia wyzwania, pokonywanie przeciwności. "Każda organizacja może zostać przebudowana w coś wartościowego. Pytanie ile tłuszczu usuniesz." | @ 230715-amanda-konsoliduje-zloty-cien
* styl: bezkompromisowy dyktator, dyrektywny, traktuje innych jak śmieci. | @ 230715-amanda-konsoliduje-zloty-cien

### Wątki


pronoktianska-mafia-kajrata

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230715-amanda-konsoliduje-zloty-cien | 43, chce jak najszybciej naprawić Złoty Cień i zająć się czymś innym. Widząc Wacława Samszara uznał, że to Bilgemener jest mózgiem operacji - nie zauważył znaczenia Amandy. Dał Bilgemenerowi szansę do pierwszego dużego fuckupu. | 0095-09-09 - 0095-09-12 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Bella Samszar        | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Emil Samszar         | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Kasimir Esilin       | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Rufus Bilgemener     | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Wacław Samszar       | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |