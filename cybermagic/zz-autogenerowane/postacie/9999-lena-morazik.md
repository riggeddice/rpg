---
categories: profile
factions: 
owner: public
title: Lena Morazik
---

# {{ page.title }}


# Generated: 



## Fiszki


* specjalistka od komunikacji, negocjatorka i psychotroniczka. Kiedyś: biuro propagandy. | @ 230720-savaranie-przed-obliczem-nihilusa
* OCEAN: (E+N-A+): Optymistyczna i pełna energii. Talent do łagodzenia napięć. Idealna kumpela. "Możemy rozmawiać o wszystkim." | @ 230720-savaranie-przed-obliczem-nihilusa
* VALS: (Humility, Self-Direction): Wierzy w służbę dla innych, ale każdy ma prawo wyboru jak. "Każdy ma swoje miejsce w Wielkiej Maszynie i każdy znajdzie własne." | @ 230720-savaranie-przed-obliczem-nihilusa
* Core Wound - Lie: "Kazali mi wspierać nieprawdziwy system" - "Jeśli wszyscy mówią prawdę i ujawnimy swoje różnice, znajdziemy coś, co działa" | @ 230720-savaranie-przed-obliczem-nihilusa
* styl: Empatyczna, cierpliwa, niemożliwa do wyprowadzenia z równowagi. Awatar spokoju. "Jeśli Ci źle, porozmawiaj ze mną. Pomogę Ci." | @ 230720-savaranie-przed-obliczem-nihilusa

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230720-savaranie-przed-obliczem-nihilusa | psychotroniczka i negocjatorka, która zajrzała w głąb TAI Ikarii Dotkniętej przez Nihilusa i dostała anomalię memetyczną. Walczyła z tym; była nawet przydatna - wyciągnęła prawdę od Raleny, było lepiej - ale Nihilus ją pokonał. Igor musiał ją wyrzucić za śluzę, nic już nie czuła. KIA. | 0092-07-01 - 0092-07-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Agnieszka Serkis     | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Arnold Tapszecz      | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Igor Seklamant       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Jola Seklamant       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| JRN Kantala Ravis    | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Katrina Kirten       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Kiran Ravis          | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Marcelin Viirdus     | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Rafał Kurrodis       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Ralena Annitas       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Ralf Tapszecz        | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |