---
categories: profile
factions: 
owner: public
title: Talia Mikrit
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220809-20-razy-za-duzo-esuriit      | Thalia; Brawler; wanted to stop a truck manifesting Esuriit and did that with explosives saving driver's life. She REALLY likes blowing stuff up. | 0111-09-30 - 0111-10-06 |
| 220819-tank-as-a-love-letter        | Mimosa's agent; intimidated Ariel and made him talk for his own good. | 0111-10-07 - 0111-10-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Izabela Selentik     | 2 | ((220809-20-razy-za-duzo-esuriit; 220819-tank-as-a-love-letter)) |
| Michał Kabarniec     | 2 | ((220809-20-razy-za-duzo-esuriit; 220819-tank-as-a-love-letter)) |
| Mimoza Diakon        | 2 | ((220809-20-razy-za-duzo-esuriit; 220819-tank-as-a-love-letter)) |
| Ariel Kubunczak      | 1 | ((220819-tank-as-a-love-letter)) |
| Arkadiusz Terienak   | 1 | ((220819-tank-as-a-love-letter)) |
| AU Flara Astorii     | 1 | ((220819-tank-as-a-love-letter)) |
| Daniel Terienak      | 1 | ((220819-tank-as-a-love-letter)) |
| Henryk Wkrąż         | 1 | ((220819-tank-as-a-love-letter)) |
| Iwona Perikas        | 1 | ((220819-tank-as-a-love-letter)) |
| Karol Walgoryn       | 1 | ((220809-20-razy-za-duzo-esuriit)) |