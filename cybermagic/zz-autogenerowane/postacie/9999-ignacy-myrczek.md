---
categories: profile
factions: 
owner: public
title: Ignacy Myrczek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180929-dwa-tygodnie-szkoly          | czarodziej który marzy o grzybach. Felicja podłożyła mu pod poduszkę kaktusa bo myślała że on jej zrobił numer pierwszy. Na końcu - przeprosiła. | 0109-09-17 - 0109-09-19 |
| 181027-terminuska-czy-kosmetyczka   | grzybomag, który wplątał się w polityczny atak na Atenę z chęci pomocy Brygidzie oraz z miłości do grzybów. Już więcej nie będzie tak eksperymentował. | 0109-10-07 - 0109-10-11 |
| 191112-korupcja-z-arystokratki      | nerd od grzybów. Robił rozpaczliwe eksperymenty z narkotykami, by udowodnić arystokratce, że nie zrobił jej krzywdy - to były narkotyki. Pięknotka dała mu wpiernicz. | 0110-07-04 - 0110-07-05 |
| 200326-test-z-etyki                 | AMZ; zgnębiony przez Lilianę, próbuje pomóc jej testując konserwy "Zygmunta Zająca". Bardzo pasywna rola - ona każe, on się boi i jej słucha. | 0110-07-29 - 0110-07-31 |
| 200414-arystokraci-na-trzesawisku   | dał się skusić na zarobek by pomóc znaleźć grzyby na Trzęsawisku; niestety, skusił się na Sabinę Kazitan a skończył ratowany przez Pięknotkę. | 0110-08-04 - 0110-08-05 |
| 200417-nawolywanie-trzesawiska      | szukał kobiety, której grozi coś złego (zew Trzęsawiska). Skończył w Szpitalu Terminuskim w Pustogorze, na sali z Sabiną Kazitan. Przerażony. | 0110-08-12 - 0110-08-14 |
| 200418-wojna-trzesawiska            | przestał się bać Sabiny Kazitan; gdy ona leży z nim w szpitalu to czyta jej książki i opowiada o grzybach. | 0110-08-16 - 0110-08-21 |
| 200510-tajna-baza-orbitera          | stanął w obronie Sabiny przed Natalią, za co Sabina walnęła mu szpicrutą w twarz. Wyraźnie się w Sabinie podkochuje i chce ją "uratować od jej złej natury". | 0110-09-07 - 0110-09-11 |
| 201006-dezinhibitor-dla-sabiny      | nie chce zrezygnować z miłości do Sabiny. Zachęcony przez Kumczka do strasznego planu - eliksir "tortur" na seksbocie (na serio: dezinhibitor na Lorenie XD). Sabina z nim "zerwała", wparowała tam, opieprzyła go i powiedziała by nie robił głupot. | 0110-10-07 - 0110-10-09 |
| 201013-pojedynek-akademia-rekiny    | Liliana używa tego, że Sabina z nim "zerwała" jako przykładu tego że trzeba tępić Rekiny. | 0110-10-14 - 0110-10-22 |
| 210323-grzybopreza                  | porwany przez Marysię Sowińską ścigaczem z akademika w nocy, pomógł Trianie zrobić super-śmierdzące wybuchające trufle i grzyby do grzybo-imprezy. Potem poszedł na grzyboprezę i nawet ZATAŃCZYŁ Z MARYSIĄ SOWIŃSKĄ! Ogólnie, super dzień. | 0111-04-22 - 0111-04-23 |
| 210622-verlenka-na-grzybkach        | jego grzybki były użyte do wpłynięcia na Arkadię; pracuje nad robotem kultywacyjnym i jest bezwartościowy pod ostrzałem. | 0111-05-30 - 0111-05-31 |
| 211026-koszt-ratowania-torszeckiego | okazuje się, że dorabia sobie jako kucharz i jest współlokatorem Pawła Szprotki. | 0111-08-01 - 0111-08-05 |
| 211123-odbudowa-wedlug-justyniana   | zebrał się na odwagę i poprosił Marysię (przez Karola Pustaka) o skontaktowanie go z jego miłością - Sabiną Kazitan. | 0111-08-11 - 0111-08-20 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 201006-dezinhibitor-dla-sabiny      | dostawca środków psychoaktywnych do "Latających Rekinów" z Aurum. Lubiany przez Rekiny i studentów Akademii Magii. | 0110-10-09
| 210323-grzybopreza                  | Urszula Miłkowicz, uczennica terminusa ma z nim kosę. | 0111-04-23

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 7 | ((180929-dwa-tygodnie-szkoly; 181027-terminuska-czy-kosmetyczka; 191112-korupcja-z-arystokratki; 200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200510-tajna-baza-orbitera)) |
| Napoleon Bankierz    | 6 | ((180929-dwa-tygodnie-szkoly; 200326-test-z-etyki; 200417-nawolywanie-trzesawiska; 201006-dezinhibitor-dla-sabiny; 201013-pojedynek-akademia-rekiny; 210323-grzybopreza)) |
| Sabina Kazitan       | 6 | ((191112-korupcja-z-arystokratki; 200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200510-tajna-baza-orbitera; 201006-dezinhibitor-dla-sabiny)) |
| Gabriel Ursus        | 4 | ((200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200510-tajna-baza-orbitera; 201006-dezinhibitor-dla-sabiny)) |
| Marysia Sowińska     | 4 | ((210323-grzybopreza; 210622-verlenka-na-grzybkach; 211026-koszt-ratowania-torszeckiego; 211123-odbudowa-wedlug-justyniana)) |
| Teresa Mieralit      | 4 | ((200326-test-z-etyki; 200510-tajna-baza-orbitera; 201013-pojedynek-akademia-rekiny; 211026-koszt-ratowania-torszeckiego)) |
| Julia Kardolin       | 3 | ((201013-pojedynek-akademia-rekiny; 210323-grzybopreza; 210622-verlenka-na-grzybkach)) |
| Justynian Diakon     | 3 | ((201006-dezinhibitor-dla-sabiny; 201013-pojedynek-akademia-rekiny; 211123-odbudowa-wedlug-justyniana)) |
| Liliana Bankierz     | 3 | ((200326-test-z-etyki; 201013-pojedynek-akademia-rekiny; 210622-verlenka-na-grzybkach)) |
| Mariusz Trzewń       | 3 | ((191112-korupcja-z-arystokratki; 200414-arystokraci-na-trzesawisku; 200510-tajna-baza-orbitera)) |
| Rafał Torszecki      | 3 | ((210622-verlenka-na-grzybkach; 211026-koszt-ratowania-torszeckiego; 211123-odbudowa-wedlug-justyniana)) |
| Adela Kirys          | 2 | ((180929-dwa-tygodnie-szkoly; 181027-terminuska-czy-kosmetyczka)) |
| Arnulf Poważny       | 2 | ((180929-dwa-tygodnie-szkoly; 181027-terminuska-czy-kosmetyczka)) |
| Kacper Bankierz      | 2 | ((201013-pojedynek-akademia-rekiny; 210323-grzybopreza)) |
| Laura Tesinik        | 2 | ((200510-tajna-baza-orbitera; 201006-dezinhibitor-dla-sabiny)) |
| Minerwa Metalia      | 2 | ((181027-terminuska-czy-kosmetyczka; 191112-korupcja-z-arystokratki)) |
| Strażniczka Alair    | 2 | ((200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska)) |
| Triana Porzecznik    | 2 | ((210323-grzybopreza; 210622-verlenka-na-grzybkach)) |
| Urszula Miłkowicz    | 2 | ((210323-grzybopreza; 210622-verlenka-na-grzybkach)) |
| Wiktor Satarail      | 2 | ((200418-wojna-trzesawiska; 211026-koszt-ratowania-torszeckiego)) |
| Aleksander Bemucik   | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Aniela Kark          | 1 | ((200326-test-z-etyki)) |
| Aranea Diakon        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Arkadia Verlen       | 1 | ((210622-verlenka-na-grzybkach)) |
| Artur Kołczond       | 1 | ((200510-tajna-baza-orbitera)) |
| Ataienne             | 1 | ((191112-korupcja-z-arystokratki)) |
| Atena Sowińska       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Azalia Sernat d'Namertel | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Berenika Wrążowiec   | 1 | ((200326-test-z-etyki)) |
| Brygida Maczkowik    | 1 | ((181027-terminuska-czy-kosmetyczka)) |
| Daniel Terienak      | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Ernest Namertel      | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Erwin Galilien       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Felicja Melitniek    | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Franciszek Leszczowik | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Karla Mrozik         | 1 | ((200418-wojna-trzesawiska)) |
| Karol Pustak         | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Karolina Erenit      | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Karolina Terienak    | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Ksenia Kirallen      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Lorena Gwozdnik      | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Marek Samszar        | 1 | ((210622-verlenka-na-grzybkach)) |
| Miedwied Zajcew      | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Natalia Tessalon     | 1 | ((200510-tajna-baza-orbitera)) |
| Olaf Zuchwały        | 1 | ((200417-nawolywanie-trzesawiska)) |
| Olga Myszeczka       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Paweł Szprotka       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Persefona d'Jastrząbiec | 1 | ((191112-korupcja-z-arystokratki)) |
| Rafał Kumczek        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Remor 340D           | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Robert Pakiszon      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Roland Grzymość      | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Różewicz Diakon      | 1 | ((210622-verlenka-na-grzybkach)) |
| Sensacjusz Diakon    | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Stella Armadion      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Tadeusz Sklerzec     | 1 | ((191112-korupcja-z-arystokratki)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Talia Aegis          | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Tymon Grubosz        | 1 | ((200510-tajna-baza-orbitera)) |