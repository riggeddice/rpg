---
categories: profile
factions: 
owner: public
title: Rafał Kurrodis
---

# {{ page.title }}


# Generated: 



## Fiszki


* advancer, ekspert od zabezpieczeń i inżynier | @ 230720-savaranie-przed-obliczem-nihilusa
* OCEAN: (C+O+E-): Perfekcjonista z umiejętnością dostrzeżenia szczegółów, które innym umykają. Poważny i skoncentrowany na swojej pracy. "Bezpieczeństwo jest moją pierwszą i ostatnią myślą." | @ 230720-savaranie-przed-obliczem-nihilusa
* VALS: (Conformity, Security): Wierzy w istotność zasad i procedur, które chronią życie i majątek. "To nie sprzęt, to nasze życia." | @ 230720-savaranie-przed-obliczem-nihilusa
* Core Wound - Lie: "Ktoś spieprzył i straciłem rękę" - "Jeśli będę bardzo uważał i trzymał się procedur, możemy wszystkich uratować" | @ 230720-savaranie-przed-obliczem-nihilusa
* styl: Praktyczny, metodyczny, dokładny. Zawsze sprawdza podwójnie. "Nie ma miejsca na błędy w kosmosie." | @ 230720-savaranie-przed-obliczem-nihilusa

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230720-savaranie-przed-obliczem-nihilusa | Badał logi TAI Ikarii i odkrył, że Ikaria jest bardzo nie w porządku, sama się lobotomizowała. Potem przy pozyskiwaniu Pacyfikatora spieprzał przed anomalnym Pacyfikatorem Nihilusa i wpakował go w pułapkę Igora. | 0092-07-01 - 0092-07-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Agnieszka Serkis     | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Arnold Tapszecz      | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Igor Seklamant       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Jola Seklamant       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| JRN Kantala Ravis    | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Katrina Kirten       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Kiran Ravis          | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Lena Morazik         | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Marcelin Viirdus     | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Ralena Annitas       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Ralf Tapszecz        | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |