---
categories: profile
factions: 
owner: public
title: Jonasz Parys
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220615-lewiatan-przy-seibert        | kapitan 'Zająca 3', szybkiej korwety koloidowej, który nie umie przegrywać i zbluzgał Ariannę gdy Infernia go wyrolowała i został złapany. Pod Kramerem. Kolesia nikt nie złapał, ale Klaudia wciągnęła go w pułapkę jego własną arogancją... i przegrał. | 0112-09-27 - 0112-09-29 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220615-lewiatan-przy-seibert        | wściekły na Infernię - zwłaszcza Klaudię (gra nie fair) i Ariannę (psychiczna), bo przegrał. Stracił reputację nieuchwytnego i uszkodził swoją korwetę koloidową. | 0112-09-29

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((220615-lewiatan-przy-seibert)) |
| Eszara d'Seibert     | 1 | ((220615-lewiatan-przy-seibert)) |
| Eustachy Korkoran    | 1 | ((220615-lewiatan-przy-seibert)) |
| Klaudia Stryk        | 1 | ((220615-lewiatan-przy-seibert)) |
| Ola Szerszeń         | 1 | ((220615-lewiatan-przy-seibert)) |