---
categories: profile
factions: 
owner: public
title: Mi Ruda
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190226-korporacyjna-wojna-w-mmo     | zabanowała nieszczęsnego Sekatora Pierwszego mocą Yyizdatha. Też powstrzymała działania Sekatora odnośnie EliSquid. | 0110-03-14 - 0110-03-17 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190226-korporacyjna-wojna-w-mmo     | ma wpływy w obu korporacjach: Rexpapier i Sensus. | 0110-03-17

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adrian Wężoskór      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Alan Bartozol        | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Antoni Kotomin       | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Dariusz Bankierz     | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Eliza Kotlet         | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Kermit Szperacz      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Rafał Królewski      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Wojciech Słabizna    | 1 | ((190226-korporacyjna-wojna-w-mmo)) |