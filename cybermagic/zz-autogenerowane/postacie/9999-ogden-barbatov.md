---
categories: profile
factions: 
owner: public
title: Ogden Barbatov
---

# {{ page.title }}


# Generated: 



## Fiszki


* 44 lata, kapitan + marine + salvager (atarienin) | @ 221022-derelict-okarantis-wejscie
* ENCAO:  +-00- |Osoba starej daty;;Jeżeli czegoś chce, weźmie| VALS: Achievement, Power >> Conformity| DRIVE: Uwolnić niewolników | @ 221022-derelict-okarantis-wejscie

### Wątki


historia_darii
salvagerzy-anomalii-kolapsu

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221022-derelict-okarantis-wejscie   | 44 lata, kapitan + marine + salvager (atarienin); |ENCAO: +-00- |Osoba starej daty;;Jeżeli czegoś chce, weźmie| VALS: Achievement, Power >> Conformity| DRIVE: Uwolnić niewolników|; chce chronić swoich ludzi nawet kosztem rezygnacji z derelictu. Bierze brudną i żmudną robotę, jak trzeba to asekuruje bronią. | 0090-03-03 - 0090-03-14 |
| 221111-niebezpieczna-woda-na-hiyori | podjął decyzję o SOS i abortowaniu misji. Uznał niebezpieczną wodę za broń i chce ją sprzedać Orbiterowi. Widać po nim doświadczenie wojskowe. | 0090-06-17 - 0090-06-21 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Daria Czarnewik      | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Iga Mikikot          | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Jakub Uprzężnik      | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Kaspian Certisarius  | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Leo Kasztop          | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Nastia Barbatov      | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Patryk Lapszyn       | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Safira d'Hiyori      | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| SCA Hiyori           | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Ailira Niiris        | 1 | ((221111-niebezpieczna-woda-na-hiyori)) |