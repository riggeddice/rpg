---
categories: profile
factions: 
owner: public
title: Larkus Talvinir
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: A-O- | "Żartuję nawet w złych sytuacjach; prowokuję i nie boję się konfliktów" | VALS: Stimulation, Universalism | DRIVE: "Zabawa kosztem innych to moja rozrywka" | @ 231119-tajemnicze-tunele-sebirialis
* główny inżynier w sztabie | @ 231119-tajemnicze-tunele-sebirialis

### Wątki


agencja-lux-umbrarum
problemy-con-szernief

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231119-tajemnicze-tunele-sebirialis | radny i inżynier, obwiniony o kupowanie kiepskiego sprzętu (bo sensory przy alteris nie dają sensownej odpowiedzi). Wie o umowie z NavirMed, ale to nie jej linia. Żąda immunitetu, szuka przyczyn i jest bardzo zestresowany. Zaradny, potrafi z minimalnych pieniędzy wyciągnąć coś co zadziała. Wieczne zwierzę w radzie XD. | 0104-11-20 - 0104-11-24 |
| 240117-dla-swych-marzen-warto       | nalega, by wszyscy przebadali się przez Agencję i współpracowali i NADAL dogryza Estrilowi, że jego córka została kanibalem. Mdleje, gdy liże go krewetkowy detektor. | 0106-11-04 - 0106-11-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Felina Amatanir      | 2 | ((231119-tajemnicze-tunele-sebirialis; 240117-dla-swych-marzen-warto)) |
| Kalista Surilik      | 2 | ((231119-tajemnicze-tunele-sebirialis; 240117-dla-swych-marzen-warto)) |
| Klasa Sabotażysta    | 2 | ((231119-tajemnicze-tunele-sebirialis; 240117-dla-swych-marzen-warto)) |
| Aerina Cavalis       | 1 | ((240117-dla-swych-marzen-warto)) |
| Aniela Kafantelas    | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Artur Tavit          | 1 | ((240117-dla-swych-marzen-warto)) |
| Eryk Kawanicz        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Estril Cavalis       | 1 | ((240117-dla-swych-marzen-warto)) |
| Ignatius Sozyliw     | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Janvir Krassus       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Klasa Dyplomata      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Klasa Hacker         | 1 | ((240117-dla-swych-marzen-warto)) |
| Klasa Oficer Naukowy | 1 | ((240117-dla-swych-marzen-warto)) |
| Leon Hurmniow        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Maia Sakiran         | 1 | ((240117-dla-swych-marzen-warto)) |
| Marta Sarilit        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Mawir Hong           | 1 | ((240117-dla-swych-marzen-warto)) |
| OLU Luminarius       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Vanessa d'Cavalis    | 1 | ((240117-dla-swych-marzen-warto)) |