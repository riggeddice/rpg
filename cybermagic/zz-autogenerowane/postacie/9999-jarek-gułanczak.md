---
categories: profile
factions: 
owner: public
title: Jarek Gułanczak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190912-rexpapier-i-wloknin          | szef lokalnego oddziału Rexpapier; próbuje zniszczyć Włóknin, bo Anna tak nań naciska. Nie pozwolił skrzywdzić Eleonory. Znalazł rozwiązanie. | 0110-06-21 - 0110-06-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anna Warlank         | 1 | ((190912-rexpapier-i-wloknin)) |
| Dariusz Kuromin      | 1 | ((190912-rexpapier-i-wloknin)) |
| Eleonora Rdeść       | 1 | ((190912-rexpapier-i-wloknin)) |
| Kasjopea Maus        | 1 | ((190912-rexpapier-i-wloknin)) |
| Ksenia Kirallen      | 1 | ((190912-rexpapier-i-wloknin)) |
| Mateusz Urszank      | 1 | ((190912-rexpapier-i-wloknin)) |