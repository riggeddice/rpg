---
categories: profile
factions: 
owner: public
title: Tristan Andrait
---

# {{ page.title }}


# Generated: 



## Fiszki


* (ENCAO:  0+--+ |Nie toleruje spokoju i nudy; wściekły, na siłowni| VALS: Face >> Achievement| DRIVE: Wolność + Chwała Noctis) | @ 230113-ros-adrienne-a-new-recruit
* dekadianin: "jest tylko starcie i walka" | @ 230113-ros-adrienne-a-new-recruit
* kadencja: twardy, nie okazuje słabości, gardzi mamlasami, cyniczny | @ 230113-ros-adrienne-a-new-recruit
* "Poświęć więcej energii na pracę a mniej na gadanie, ok?" | @ 230113-ros-adrienne-a-new-recruit
* SPEC: walka, mięsień | @ 230113-ros-adrienne-a-new-recruit
* (ENCAO:  0+--0 |Agresywny i waleczny;; Bezpośredni;; Nigdy nie kończy walki| VALS: Face >> Achievement| DRIVE: Wolność + Chwała Noctis) | @ 230117-ros-wiertloplyw-i-tai-mirris
* dekadianin: "Noctis shall not fall - czas się uzbroić i walczyć na nowym polu" | @ 230117-ros-wiertloplyw-i-tai-mirris
* styl: twardy, nie okazuje słabości, gardzi mamlasami, cyniczny | @ 230117-ros-wiertloplyw-i-tai-mirris
* SPEC: breacher, inżynieria materiałów, przebicie / penetracja | @ 230117-ros-wiertloplyw-i-tai-mirris

### Wątki


ratownicy-ostatniej-szansy

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230113-ros-adrienne-a-new-recruit   | expert in looking very disappointed at Adrianna. Has some nice ribbing with Aster. To slow to get to Adrienne when the crisis was real. | 0083-12-07 - 0083-12-10 |
| 230117-ros-wiertloplyw-i-tai-mirris | najbardziej nie zgadzał się na żadne próby zniewolenia Mirris. Wszedł na Wiertłopływ i wyciągnął wszystkich co nie chcieli iść siłą, nie bojąc się zadać bólu gdy to potrzebne. | 0084-02-22 - 0084-02-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Napoleon Myszogłów   | 2 | ((230113-ros-adrienne-a-new-recruit; 230117-ros-wiertloplyw-i-tai-mirris)) |
| Niferus Sentriak     | 2 | ((230113-ros-adrienne-a-new-recruit; 230117-ros-wiertloplyw-i-tai-mirris)) |
| Talia Irris          | 2 | ((230113-ros-adrienne-a-new-recruit; 230117-ros-wiertloplyw-i-tai-mirris)) |
| Adrianna Kastir      | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Aster Sarvinn        | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Eleonora Alintirias  | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Hel Otereien         | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Kasia Karmnik        | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Lucas Septrien       | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| Łucja Neiser         | 1 | ((230113-ros-adrienne-a-new-recruit)) |
| Łucja Nirimis        | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| OR Błyskawica        | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| SC Wiertłopływ       | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |
| TAI Mirris d'Paravilius | 1 | ((230117-ros-wiertloplyw-i-tai-mirris)) |