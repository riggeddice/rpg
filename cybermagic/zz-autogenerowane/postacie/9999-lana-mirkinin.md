---
categories: profile
factions: 
owner: public
title: Lana Mirkinin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | agentka Biur HR (ENCAO:  --0+0 |Skromna;;Zgodna z najnowszą modą| VALS: Self-direction, Face, Hedonism |DRIVE: Biura Forever). Szybki pilot, ładna i elegancka dziewczyna i potencjalny komponent handlowy by Orbiter pomógł Biurom HR przetrwać Nox Ignis. | 0100-11-13 - 0100-11-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Arianna Verlen       | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Daria Czarnewik      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Elena Verlen         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Grażyna Burgacz      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kajetan Kircznik     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leszek Kurzmin       | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Maja Samszar         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Astralna Flara    | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Loricatus         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Salazar Bolza        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Szczepan Myrczek     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Władawiec Diakon     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |