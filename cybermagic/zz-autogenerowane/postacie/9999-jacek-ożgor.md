---
categories: profile
factions: 
owner: public
title: Jacek Ożgor
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220512-influencerskie-mikotki-z-talio | 14 lat, przyjaciel Kary na Talio. Wpierw Kara go uratowała gdy brutalny Ognik chciał go skrzywdzić (co uruchomiło Potwora). Potem widział potwora i zamknął się w skrzydle medycznym. Powiedział Elwirze wszystko za luksusy. | 0104-05-02 - 0104-05-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antonina Terkin      | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Aurus Czarmadon      | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Elwira Piscernik     | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Kamil Czarmadon      | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Kara Prazdnik        | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Mirella Czarmadon    | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Tytus Ramkon         | 1 | ((220512-influencerskie-mikotki-z-talio)) |