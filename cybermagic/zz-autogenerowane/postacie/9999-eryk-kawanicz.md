---
categories: profile
factions: 
owner: public
title: Eryk Kawanicz
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (A- C-) | "W moim świecie, nie ma miejsca na litość." | VALS: (Power, Self-Direction) | DRIVE: "PODDAŁEM SIĘ. Zniszczę NavirMed." | @ 231119-tajemnicze-tunele-sebirialis
* Protomag Alteris | @ 231119-tajemnicze-tunele-sebirialis

### Wątki


agencja-lux-umbrarum
problemy-con-szernief

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231119-tajemnicze-tunele-sebirialis | astoriański więzień, na którym eksperymentował NavirMed na Sebirialis. Protomag. Dotknięty Alteris, dotarł do kopalni na Sebirialis. Super-niebezpieczny więzień powodujący wszystkie problemy i planujący zniszczyć bazę NavirMed używając krwi porwanych ludzi; agencja doprowadziła do ewakuacji Sebirialis i umarł z głodu i pragnienia. KIA. | 0104-11-20 - 0104-11-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aniela Kafantelas    | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Felina Amatanir      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Ignatius Sozyliw     | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Janvir Krassus       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Kalista Surilik      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Klasa Dyplomata      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Klasa Sabotażysta    | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Larkus Talvinir      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Leon Hurmniow        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Marta Sarilit        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| OLU Luminarius       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |