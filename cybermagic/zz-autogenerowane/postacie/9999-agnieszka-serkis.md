---
categories: profile
factions: 
owner: public
title: Agnieszka Serkis
---

# {{ page.title }}


# Generated: 



## Fiszki


* advancer i medyk o świetnej intuicji i empatii | @ 230720-savaranie-przed-obliczem-nihilusa
* OCEAN: (E+N+A+): bardzo empatyczna i silna intuicyjnie, szybko się decyduje pod presją. Wrażliwa jak na medyczkę. "Jeśli czegoś nie zrozumiesz, to znaczy, że nie patrzysz wystarczająco głęboko." | @ 230720-savaranie-przed-obliczem-nihilusa
* VALS: (Achievement, Stimulation): dąży do bycia najlepsza w swojej dziedzinie, no bullshit "coś nowego! Coś, czego jeszcze nie robiliśmy!" | @ 230720-savaranie-przed-obliczem-nihilusa
* Core Wound - Lie: "coś DZIWNEGO zniszczyło mój statek" - "jeśli wszystko zrozumiem i znajdę, się to już nigdy nie powtórzy!" | @ 230720-savaranie-przed-obliczem-nihilusa
* styl: niezależna, zdecydowana, gotowa na wyzwania. "Nigdy nie wiesz co spotkasz, ale jak jesteś wyczulona i przygotowana to sobie poradzisz" | @ 230720-savaranie-przed-obliczem-nihilusa

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230720-savaranie-przed-obliczem-nihilusa | medyczka, którą zaniepokoił stan inżyniera na Kantali Ravis i która zauważyła niezgodności w dokumentacji medycznej. A potem skupiła się na anomalnie osowiałych pacyfikatorach. Więcej badała niż leczyła tym razem. | 0092-07-01 - 0092-07-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arnold Tapszecz      | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Igor Seklamant       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Jola Seklamant       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| JRN Kantala Ravis    | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Katrina Kirten       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Kiran Ravis          | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Lena Morazik         | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Marcelin Viirdus     | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Rafał Kurrodis       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Ralena Annitas       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Ralf Tapszecz        | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |