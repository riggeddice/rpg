---
categories: profile
factions: 
owner: public
title: Ignatus Blakenbauer
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230711-zablokowana-sentisiec-w-krainie-makaronu | nerd, kolega Eleny S.; z dziwnymi zainteresowaniami (które pomagała mu badać). Znalazł dla Eleny połączenie pomiędzy Hiperpsychotronikami i Eternią, acz wpakował się na ich celownik. | 0095-09-05 - 0095-09-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Elena Samszar        | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Irek Kraczownik      | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Karolinus Samszar    | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Petra Samszar        | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Wacław Samszar       | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Wiktor Blakenbauer   | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |