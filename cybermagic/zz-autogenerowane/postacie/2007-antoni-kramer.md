---
categories: profile
factions: 
owner: public
title: Antoni Kramer
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "orbiter, admiralicja orbitera"
* owner: "public"
* title: "Antoni Kramer"


## Kim jest

### Koncept

Admirał Orbitera o twarzy zmęczonego mopsa, specjalizujący się w ocenie ludzi i logistyce. Idealista - pragnie bezpieczeństwa i jedności Zjednoczonej Ludzkości. Widzi Orbiter jako miejsce pokojowej, bezpiecznej eksploracji i nieskończonych możliwości. Gdy trzeba, twardo podejmie właściwą decyzję, acz z żalem, nie delegując tego na innych.

### Motto

"Jeżeli przetrwamy do jutra, ale stracimy wszystko co czyni nas ludźmi - czy naprawdę przetrwaliśmy?"

### Co się rzuca w oczy

* Starszy czarodziej o twarzy bardzo zmęczonego mopsa, z przerzedzonymi włosami.
* Mówi spokojnie i powoli, choć czasami się unosi. Wygląda, jakby miał mało energii.
* Zawsze ma mnóstwo serca do swoich podopiecznych i podwładnych. Staje za nimi w potrzebie.
* Wzmacnia tematy związane ze stabilizacją Orbitera i jego bezpieczeństwem militarnym, politycznym, ekonomicznym.
* Bardzo moralny. Bardzo dba o to, by Orbiter nie był siłą zła i nie czynił zła.

### Przekonania

* Pomaganie innym i dawanie drugiej szansy to nie jest nasza słabość - to nasza siła z której powinniśmy być dumni.
* Naszym obowiązkiem jest znaleźć najsilniejsze strony naszych podopiecznych i umieścić ich tam, gdzie są najskuteczniejsi.
* Nieważne, czy jesteśmy z Noctis w stanie wojny, czy nie - są ludźmi i powinniśmy im pomóc jeśli trzeba.
* Orbiter musi się ustabilizować - wzmocnić flotę, łańcuchy dostaw i łańcuch dowodzenia.
* Indywidualizm, rywalizacja - tak, ale nie kosztem moralności i podstawowego człowieczeństwa.
* Zakazane energie, zakazane technologie są zakazane nie przez przypadek.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Znalazł Leonę Astrienko, tak potrzaskaną jak była i znalazł dla niej miejsce w Orbiterze. Potem dał ją Kamilowi jako warunek wyciągnięcia Arianny do chwały.
* Zobaczył w Ataienne coś więcej niż tylko Skażoną przez Saitaera TAI i wynegocjował z Aleksandrą Termią użycie Ataienne a nie rozebranie jej na kawałeczki.

### Z czym przyjdą do niej o pomoc? (3)

* Doskonały w ocenie ludzi - gdzie umieścić kogoś, by pokazał swoje najlepsze cechy.
* Nienaturalnie więc dobry w pozyskiwaniu sprzętu i zasobów oraz logistyce - jego podwładni zwykle mają dobry sprzęt w odpowiednim miejscu.
* Potrafi pogodzić ogień z wodą; dobrze mediuje i godzi interesy wielu stron. Jeden z najlepszych dyplomatów w Admiralicji.

### Jaką ma nieuczciwą przewagę? (3)

* Zna sporo bardzo kompetentnych magów Orbitera. Jest doceniany i lubiany za swoje humanistyczne podejście i starania.

### Charakterystyczne zasoby (3)

* COŚ: Sprzęt ponadprzeciętnej jakości w miejscach, w których jest potrzebny. Jego podwładni są dobrze wyposażeni i mają wysokie morale.
* KTOŚ: Ataienne uznaje go za jedyną nadzieję Orbitera. Pomaga mu z ukrycia jak jest w stanie - a nie jest w stanie bardzo, niestety.
* WIEM: Sieć agentów, którzy mówią mu o aktualnościach i dziwnych rzeczach które się pojawiają w Orbiterze. Ma rękę na pulsie.
* OPINIA: solidny koń roboczy, taki biurokrata, który chroni swoich i konsoliduje swoją pozycję w Orbiterze.

### Typowe sytuacje z którymi sobie nie radzi (-3)

* Zbyt próbuje pomóc innym w potrzebie. Zbyt często próbuje dać drugą szansę. Jest przez to przewidywalny i da się go wrobić w coś.
* Jest to dużo gorszy taktyk niż przeciętny komodor. Bądźmy szczerzy - w taktyce i dowodzeniu statkiem jest beznadziejny.
* Wobec swoich wychowanków i podwładnych ma bardzo łagodne podejście. Daje sobie wejść na głowę. Osłania ich, bierze problemy na siebie, nie ma ogromnych konsekwencji.

### Specjalne

* brak

## Inne

### Wygląd

* Starszy czarodziej o twarzy bardzo zmęczonego mopsa, z przerzedzonymi włosami.
* Mówi spokojnie i powoli, choć czasami się unosi. Wygląda, jakby miał mało energii.

### Coś Więcej

* 

### Endgame

* On: Antoni Kramer widzi dla siebie przyszłość jako edukator; zbuduje swoich następców, którzy będą podzielać jego ideały. 
* Ataienne: jej zdaniem, Antoni powinien przejąć władzę nad Orbiterem. On lub osoby z nim powiązane. Ten punkt widzenia musi być dominujący.
* Aleksandra Termia: zdaniem Aleksandry, Antoni powinien zostać w K1 i robić to co robi dobrze - administrować i zarządzać ludźmi. I żadnej władzy ustawodawczej.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221026-kapitan-verlen-i-koniec-przygody-na-krolowej | nowo wschodzący admirał Orbitera; zainteresował się Arianną Verlen widząc jej skuteczność i powodzenie. To osłoniło ją przed działaniami Sił Specjalnych. | 0100-06-08 - 0100-06-15 |
| 200708-problematyczna-elena         | admirał konsekwentnie osłaniający Ariannę; wysłał Ariannę na misję ratowania Aureliona przed Salaminem (o czym nie wiedział). | 0110-12-27 - 0111-01-01 |
| 200722-wielki-kosmiczny-romans      | admirał, który opieprzył Ariannę za dewastowanie korwet (wina Eleny) i za romanse (wina Leony). Dał Ariannie zadanie naprawy tego. Nadal silnie stoi za Arianną mimo nacisków. | 0111-01-10 - 0111-01-13 |
| 200729-nocna-krypta-i-emulatorka    | jak tylko admirał dowiedział się o powadze sytuacji, stanął po stronie Arianny i sprawił, by Castigator mógł zniszczyć Kryptę. Dostał podziękowania z Admiralicji i upewnił się, że Arianna też je dostanie. | 0111-01-14 - 0111-01-20 |
| 200826-nienawisc-do-swin            | dał Ariannie tajną misję w Trzecim Raju i ukrył to za karą do wożenia świń; strasznie zapłacił gdy część Orbitera się zbuntowała. | 0111-02-05 - 0111-02-11 |
| 210804-infernia-jest-nasza          | ucieszył się, że dla odmiany Arianna przyszła do niego rozwiązać problem "złomowania Inferni" zamiast robić własne plany (nie przyszła). Odwrócił rozkaz złomowania i oddał Ariannie Infernię. | 0111-06-21 - 0111-06-24 |
| 210218-infernia-jako-goldarion      | stracił advancera na Asimear i wysłał Infernię, by go znaleźli. Plus cośtam z Sowińskimi i potencjałem na flotę Aurum. | 0111-09-16 - 0111-10-01 |
| 200429-porwanie-cywila-z-kokitii    | admirał Orbitera i zwolennik Arianny; powiedział jej parę rzeczy o Medei, m.in. jak stała się tylko kapitanem w wyniku wypadku. | 0111-10-01 - 0111-10-06 |
| 210526-morderstwo-na-inferni        | zatrzymał ćwiczenia komodora Walronda, by nie wyszło że noktianie zabili swojego na K1. Potem przeniósł winnych noktian na Żelazko dla Arianny - by nie ucierpieli za mocno a kara by była. | 0111-12-31 - 0112-01-06 |
| 210818-siostrzenica-morlana         | dostał info o planie na program kosmiczny Jolanty Sowińskiej. Zaaranżował ćwiczenia Netrahina - Żelazko z dobrego serca i chęci pomocy Ofelii. | 0112-01-20 - 0112-01-24 |
| 210922-ostatnia-akcja-bohaterki     | pod wpływem raportu Klaudii i prośby Arianny dał się przekonać do autoryzowania Inferni do zniszczenia Krwawej Bazy Piratów. | 0112-02-23 - 0112-03-09 |
| 210929-grupa-ekspedycyjna-kellert   | nie zgadza się na to, że Termia nie ma jak uratować 6 statków i 200 osób. Zakoloidował Infernię, autoryzował sprzęt i wysłał Infernię przez Bramę. | 0112-03-13 - 0112-03-16 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 200729-nocna-krypta-i-emulatorka    | dostaje szacun za ochronę Sektora Astoriańskiego przed Kijarą d'Esuriit i za zniszczenie straszliwej Anomalii. | 0111-01-20
| 200819-sekrety-orbitera-historia-prawdziwa | z uwagi na działania Arianny i Klaudii odnośnie pojedynku Leona - Tadeusz, duże zniszczenia a zwłaszcza holodramę 'Sekrety Orbitera' ma solidny opiernicz. | 0111-02-01
| 200826-nienawisc-do-swin            | straszny cios w reputację i zasoby; bunt spowodowany przez załogę Inferni uderzył w jego silną stronę i pokazał, że Kramer nic nie kontroluje. | 0111-02-11

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 12 | ((200429-porwanie-cywila-z-kokitii; 200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Eustachy Korkoran    | 10 | ((200429-porwanie-cywila-z-kokitii; 200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210929-grupa-ekspedycyjna-kellert)) |
| Klaudia Stryk        | 10 | ((200429-porwanie-cywila-z-kokitii; 200708-problematyczna-elena; 200729-nocna-krypta-i-emulatorka; 200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert)) |
| Leona Astrienko      | 10 | ((200429-porwanie-cywila-z-kokitii; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert)) |
| Elena Verlen         | 6 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert)) |
| Martyn Hiwasser      | 5 | ((200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert)) |
| Olgierd Drongon      | 4 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| OO Żelazko           | 4 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Aleksandra Termia    | 3 | ((200826-nienawisc-do-swin; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert)) |
| Damian Orion         | 2 | ((200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka)) |
| Kamil Lyraczek       | 2 | ((200826-nienawisc-do-swin; 210526-morderstwo-na-inferni)) |
| Leszek Kurzmin       | 2 | ((200708-problematyczna-elena; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| OO Infernia          | 2 | ((210218-infernia-jako-goldarion; 210929-grupa-ekspedycyjna-kellert)) |
| Otto Azgorn          | 2 | ((210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert)) |
| Tadeusz Ursus        | 2 | ((200722-wielki-kosmiczny-romans; 200826-nienawisc-do-swin)) |
| Tomasz Sowiński      | 2 | ((210218-infernia-jako-goldarion; 210818-siostrzenica-morlana)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| AK Nocna Krypta      | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| AK Salamin           | 1 | ((200708-problematyczna-elena)) |
| Alara Ehmes          | 1 | ((200429-porwanie-cywila-z-kokitii)) |
| Arnulf Perikas       | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Artur Traffal        | 1 | ((210804-infernia-jest-nasza)) |
| Daria Czarnewik      | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Diana d'Infernia     | 1 | ((210804-infernia-jest-nasza)) |
| Erwin Pies           | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Feliks Walrond       | 1 | ((210526-morderstwo-na-inferni)) |
| Franciszek Maszkiet  | 1 | ((210804-infernia-jest-nasza)) |
| Izabela Zarantel     | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Jolanta Kopiec       | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Jolanta Sowińska     | 1 | ((210218-infernia-jako-goldarion)) |
| Julian Muszel        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Karol Reichard       | 1 | ((210526-morderstwo-na-inferni)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Klaudiusz Terienak   | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Konrad Wolczątek     | 1 | ((200722-wielki-kosmiczny-romans)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Maciej Żarand        | 1 | ((210804-infernia-jest-nasza)) |
| Maria Naavas         | 1 | ((210818-siostrzenica-morlana)) |
| Marian Tosen         | 1 | ((210526-morderstwo-na-inferni)) |
| Medea Sowińska       | 1 | ((200429-porwanie-cywila-z-kokitii)) |
| Mirela Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Nataniel Morlan      | 1 | ((210818-siostrzenica-morlana)) |
| Ofelia Morlan        | 1 | ((210818-siostrzenica-morlana)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Aurelion          | 1 | ((200708-problematyczna-elena)) |
| OO Castigator        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Królowa Kosmicznej Chwały | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Netrahina         | 1 | ((210818-siostrzenica-morlana)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Samotność Gwiazd  | 1 | ((210804-infernia-jest-nasza)) |
| OO Tivr              | 1 | ((210526-morderstwo-na-inferni)) |
| OO Tucznik Trzeci    | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| OO Welgat            | 1 | ((200722-wielki-kosmiczny-romans)) |
| Persefona d'Infernia | 1 | ((200708-problematyczna-elena)) |
| Roland Sowiński      | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| SC Fecundatis        | 1 | ((210818-siostrzenica-morlana)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |
| Sebastian Alarius    | 1 | ((200826-nienawisc-do-swin)) |
| Stefan Torkil        | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Tal Marczak          | 1 | ((210526-morderstwo-na-inferni)) |
| Tomasz Ruppok        | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Vigilus Mevilig      | 1 | ((210929-grupa-ekspedycyjna-kellert)) |