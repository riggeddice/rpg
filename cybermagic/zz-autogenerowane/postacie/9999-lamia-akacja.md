---
categories: profile
factions: 
owner: public
title: Lamia Akacja
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220323-zatruta-furia-gaulronow      | empatyczne serduszko stacji. Wyciągnęła od gaulronów sześciu ochotników idąc w paszczę lwa bez broni i ze 100% naiwnością i dobrodusznością. Wykorzystana przez Suwana który stłumił atak gaulronów. | 0111-11-27 - 0111-12-02 |
| 220330-etaur-i-przyneta-na-krypte   | jej bioforma skutecznie zwalczyła Plagę; mimo że stwór chciał ją zarazić to odparła chorobę. Lol. | 0112-07-02 - 0112-07-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Dominika Perikas     | 1 | ((220323-zatruta-furia-gaulronow)) |
| Erwin Mumurnik       | 1 | ((220323-zatruta-furia-gaulronow)) |
| Eustachy Korkoran    | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Feliks Ketran        | 1 | ((220323-zatruta-furia-gaulronow)) |
| Graniec Borgon       | 1 | ((220323-zatruta-furia-gaulronow)) |
| Klaudia Stryk        | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Leszek Czarban       | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Maciej Parczak       | 1 | ((220323-zatruta-furia-gaulronow)) |
| Maria Naavas         | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Raoul Lavanis        | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Suwan Chankar        | 1 | ((220323-zatruta-furia-gaulronow)) |
| TAI Nephthys         | 1 | ((220323-zatruta-furia-gaulronow)) |