---
categories: profile
factions: 
owner: public
title: Antoni Paklinos
---

# {{ page.title }}


# Generated: 



## Fiszki


* chorąży odpowiedzialny za integrację z ramienia grupy wydzielonej Salvagerów Lohalian | @ 230813-jedna-tienka-przybywa-na-pomoc
* OCEAN: (A-C+): niezwykle cierpliwy i nieźle współpracuje z ludźmi, jednocześnie dość agresywny; lubi dawać ludziom szansę i ich testować. "Paklinos to swój chłop; nieco zmierzły i testujący, ale daje radę" | @ 230813-jedna-tienka-przybywa-na-pomoc
* VALS: (Security, Benevolence): niezależnie czego nie dostaje i czym się nie zajmuje, kluczowa dla niego jest pomoc innym i zadbanie o ochronę. "On nawet do kibla wchodzi z nożem" | @ 230813-jedna-tienka-przybywa-na-pomoc
* Core Wound - Lie: "Zrobiłem to, co należało zrobić i zostałem wygnany na planetę" - "Nic co robimy nie ma znaczenia, ale możemy pomagać w najbliższej okolicy. Od myślenia jest dowództwo." | @ 230813-jedna-tienka-przybywa-na-pomoc
* Styl: Dość zaczepny ale pozytywny facet; dużo testuje i zawsze jest 'in your face'. Zaufany i daje radę. | @ 230813-jedna-tienka-przybywa-na-pomoc
* metakultura: Atarien: "Oddział jest tyle warty ile jego wytrenowanie i spójność z rozkazami. Co nie jest testowane i szkolone, to się nie dzieje." | @ 230813-jedna-tienka-przybywa-na-pomoc

### Wątki


salvagerzy-lohalian

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230813-jedna-tienka-przybywa-na-pomoc | chorąży,  odpowiedzialny za integrację Estelli, lubi testować; dał Estelli wpierw zadanie przetrzeć stołówkę, potem odśnieżać - by ją przetestować. Zdała test. Gdy Estella wykryła niewłaściwość mentalną w czarodziejce Orbitera, Paklinos stanął za Estellą by razem udało się im pomóc Racheli. | 0095-09-08 - 0095-09-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Estella Gwozdnik     | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |
| Mikołaj Larnecjat    | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |
| Rachela Brześniak    | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |
| Serafin Gwozdnik     | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |
| Tomasz Afagrel       | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |