---
categories: profile
factions: 
owner: public
title: Emilia Kariamon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180701-dwa-rejsy-w-potrzebie        | młoda Koordynatorka Luxuritiasu, sprzężona z Martauronem. Po przykrych przeżyciach z Tymonem Barbaturranem, rozpoczęła rebelię z Martauronem - z miłości do niego i z uwagi na to, że ich życie było kłamstwem. Była skłonna zginąć z Martauronem, ale skończyła z nim w Eterze Nieskończonym... | 0109-08-07 - 0109-08-11 |
| 200122-kiepski-altruistyczny-terroryzm | zintegrowana z Martauronem była Emulatorka; przeprowadziła atak terrorystyczny na Królową Chmur by ratować ludzi. Udało jej się, dzięki załodze Inferni. | 0110-09-15 - 0110-09-17 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180701-dwa-rejsy-w-potrzebie        | przetrwała w Eterze Nieskończonym i ma stałe, stabilne sprzężenie z Martauronem. Jest w bezpiecznym miejscu. Luxuritias wydał na nią wyrok śmierci. | 0109-08-11

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Martauron Attylla    | 2 | ((180701-dwa-rejsy-w-potrzebie; 200122-kiepski-altruistyczny-terroryzm)) |
| Arianna Verlen       | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Eustachy Korkoran    | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Fabian Komczatkow    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Gerard Weiner        | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Kacper Wontarczyk    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Kalina Rotmistrz     | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Klaudia Stryk        | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Krystian Moborok     | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Liwia Barbaturran    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Marcjanna Maszotka   | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Natalia Miszryk      | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Sabrina Powsimrożek  | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Tymon Barbaturran    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Wojciech Zajcew      | 1 | ((180701-dwa-rejsy-w-potrzebie)) |