---
categories: profile
factions: 
owner: public
title: Celina Maskiewnik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230808-nauczmy-mlodego-tiena-jak-zyc | śliczna bratanica kaprala Maskiewnika w ochronie Armina Samszara; jako jedyna poszła z Arminem i Kleopatrą na makaronowego potwora, postawiła się że ONA wchodzi pierwsza, wpadła w pułapkę i prawie zginęła. Bardzo bliska wujkowi. | 0095-09-14 - 0095-09-23 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230808-nauczmy-mlodego-tiena-jak-zyc | ranna; wprowadzona na pułapkę która odcięła jej nogę i PRAWIE zabita przez makaronotwora gdyby nie Amanda a POTEM amnestyki i szpital. Miesiąc w szpitalu. | 0095-09-23

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Armin Samszar        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Elena Samszar        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Florian Samszar      | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Karolinus Samszar    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Kleopatra Trusiek    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Rufus Bilgemener     | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Tadeusz Maskiewnik   | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Wiktor Blakenbauer   | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |