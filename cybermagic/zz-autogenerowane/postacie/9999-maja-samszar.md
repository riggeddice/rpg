---
categories: profile
factions: 
owner: public
title: Maja Samszar
---

# {{ page.title }}


# Generated: 



## Fiszki


* comms officer, tien (p.o. Klarysy jak K. nie może) | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* ENCAO:  0+-0- |Moralizatorska| VALS: Stimulation, Conformity >> Power| DRIVE: Długi | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* młodziutka czarodziejka (rok młodsza od Eleny) uwielbiająca różnorakie potrawy i gotowanie. | @ 230328-niepotrzebny-ratunek-mai
* ENCAO:  ++-0- |Moralizatorska| VALS: Stimulation, Conformity >> Power| DRIVE: Rite of passage | @ 230328-niepotrzebny-ratunek-mai
* styl: UR z elementami WB; lekko emocjonalnie niestabilna arystokratka która UWIELBIA wszystkich pouczać i dużo mówi. DUŻO. | @ 230328-niepotrzebny-ratunek-mai

### Wątki


kosmiczna-chwala-arianny
samotna-maja-w-strasznym-aurum
karolinus-heart-viorika
turniej-supmis-41

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230328-niepotrzebny-ratunek-mai     | ma ZATARG z Romeem (ma zespół w Supreme Missionforce) i pojechała walczyć z nim w VirtuFortis uciekając przed rodzicami. Karolinus jej pomógł, co się mu u Mai bardzo zapisała na plus. | 0095-06-30 - 0095-07-02 |
| 230523-romeo-dyskretny-instalator-supreme-missionforce | uroczy diabełek (UR: creativity), nieszczęśliwa bo musi być "elegancką damą" a ma talent w Supreme Missionforce. Opieprzyła Romeo we wściekłości gdy on ją wyśmiał (bo nikogo nie miała) i Romeo przyjechał jej to zamontować. Znalazła miejsce, ale jak się okazało że jest tam ukryta baza i Romeo wpadł w dziurę, spanikowała. Wezwała Karolinusa (bo nikogo nie miała) i nie chciała zostawić Romeo (KTÓREGO NAWET NIE LUBI!). Lojalna. W końcu - nie wpadła w kłopoty, bo z rozkazu Karolinusa i Strzały schowała się w Ogrodach Medytacji XD | 0095-08-09 - 0095-08-11 |
| 230704-maja-chciala-byc-dorosla     | naiwna uwierzyła Karolinusowi i chciała zobaczyć jak to jest gdy jest się dorosłą kobietą i "nie Mają". Dostała wygląd Vanessy Lemurczak. Poszła do baru, gdzie dostała od Amandy pigułkę, trafiła do Lemurczaków na 3h zabawy. Skończyła w bardzo ciężkim stanie. | 0095-08-29 - 0095-09-01 |
| 210311-studenci-u-verlenow          | 15 lat, uroczy, mały diabełek, w co nikt nie wierzy. Poza Eleną, która widziała Maję w akcji. Podżegaczka. | 0097-01-16 - 0097-01-22 |
| 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly | oficer łącznościowy Królowej i tien; nie przekazała informacji o przybyciu Arianny Alezji. Stoi lojalnie za Władawcem. | 0100-05-06 - 0100-05-12 |
| 220928-kapitan-verlen-i-pojedynek-z-marine | koordynowała sygnały między Arianną a Leszkiem Kurzminem. Aktywnie Arianny nie sabotowała, ale nie pomagała. | 0100-05-14 - 0100-05-16 |
| 221012-kapitan-verlen-i-niezapowiedziana-inspekcja | znalazła perfekcyjny sposób na usunięcie Arianny - nie powiedziała jej o inspekcji i wpakowała ją w trening z marine. Potem pousuwała dowody. Ma sprawę osobistą, nie tylko z uwagi na Władawca. Ale - Arianna nie została usunięta a reaktywacja Semli sprawia, że możliwości zaszkodzenia Ariannie się Mai skończyły. | 0100-05-17 - 0100-05-21 |
| 221019-kapitan-verlen-i-pierwszy-ruch-statku | przekonana przez Ariannę że ma do wyboru - być posłuszną lub jeść konserwy. Maja to foodie. Robi ciasta. Kocha jedzenie. Arianna jej obiecała, że pomoże jej się zemścić na Romeo - ale Maja ma Ariannie nie przeszkadzać. Nieufna, ale zneutralizowana. | 0100-05-23 - 0100-06-04 |
| 221102-astralna-flara-i-porwanie-na-karsztarinie | próbuje przekonać Ariannę, by zamówiła specjalne substraty do jedzenia. Done, uda się - ale ona czasem dokarmi oficerów swoimi potrawami. Nie jest fanką Eleny więc chciała by Daria sprawdziła funkcje komunikacyjne - okazało się, że to sygnał przez relay na Karsztarinie. Stąd wiedzą o porwaniu. | 0100-07-28 - 0100-08-03 |
| 221116-astralna-flara-dociera-do-nonariona-nadziei | absolutnie załamana stanem Nonariona - jej pierwsza myśl to coup de grace. Pełni rolę comms experta bez problemu. | 0100-09-08 - 0100-09-11 |
| 221130-astralna-flara-w-strefie-duchow | dekodowała dane dostarczone przez Darię; doszła do tego, że wszystko wskazuje na sojusz noktian i TAI? Niestety, za słaba - nie ma na to dowodów. Ma gwiazdki w oczach - potencjalnie walczą z NOKTIANAMI WROGAMI LUDZKOŚCI! Zachowała dla Arianny dyskrecję o danych. Zrobiła potrawę dla noktianki Kirei; nieco za mocną, ale jej doświadczenia z niewolnikami noktiańskimi się sprawdza. | 0100-10-05 - 0100-10-08 |
| 221221-astralna-flara-i-nowy-komodor | spięła się z Kazmirian, ma informacje kto jest w środku i przestawiła lifesupport by wszystkich uśpić i unieszkodliwić. Dobre ćwiczenie retransmitując z Athamarein. Wyjątkowo dobrze jej poszło. | 0100-11-07 - 0100-11-10 |
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | o dziwo nie zna się na duchach; tym się nie interesowała. Ale wie, że Grażyna tak. Za to pomaga Darii złożyć ekrany ochronne chroniące je przed banshee Nox Ignis. | 0100-11-13 - 0100-11-16 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230328-niepotrzebny-ratunek-mai     | rodzice Mai są z tych... "moja córka jest elegancka, grzeczna i jest prawdziwą arystokratką Aurum." więc "Verlenowie musieli porwać". | 0095-07-02
| 230606-piekna-diakonka-i-rytual-nirwany-koz | Romeo poinformował ją o działaniach Eleny S. i że on nie ma zamiaru się więcej zadawać z jej 'crazy bloodline'. | 0095-08-18
| 230704-maja-chciala-byc-dorosla     | BROKEN! Maltretowana 3h przez Lemurczaków (Jonatan, potem Vanessa), złamana i rozbita. 3 następne tygodnie w szpitalu na rekonstrukcję. | 0095-09-01
| 230704-maja-chciala-byc-dorosla     | COŚ wstrzykiwał jej Jonatan Lemurczak. COŚ jej zrobił. Nie wiedział, że jest tienką, więc to może działać dziwnie. | 0095-09-01
| 230704-maja-chciala-byc-dorosla     | Jej rekonstrukcja wymaga: amnestyków, integracji z duchami ("przeszczep"), detoksu, regeneracji tkanki... naprawa ciała, umysłu, duszy. | 0095-09-01
| 230704-maja-chciala-byc-dorosla     | Od tej pory przygotowanie jedzenia jest tym co ją 'stabilizuje' i jej obsesją, ma luki w pamięci, całkowicie traci link z duchami Samszarów i randomowo się boi i płacze. | 0095-09-01
| 230704-maja-chciala-byc-dorosla     | Ojciec - Albert - skupia uwagę na niej jako osobie a nie "perfekcyjnej córeczce by ją dobrze wydać". Chce jej dobra i regeneracji. | 0095-09-01
| 230704-maja-chciala-byc-dorosla     | Jej pamięć rejestruje rzeczy źle. Rozerwała link z Karolinusem (nie ostrzegł jej) i z Eleną (odbiła jej Romeo). Jest skrzywdzona i sama. | 0095-09-01

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 10 | ((210311-studenci-u-verlenow; 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Daria Czarnewik      | 9 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Władawiec Diakon     | 7 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Arnulf Perikas       | 6 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Elena Verlen         | 5 | ((210311-studenci-u-verlenow; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Grażyna Burgacz      | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kajetan Kircznik     | 5 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alezja Dumorin       | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Leszek Kurzmin       | 4 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Astralna Flara    | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Królowa Kosmicznej Chwały | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Szymon Wanad         | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Erwin Pies           | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Hubert Kerwelenios   | 3 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Karolinus Samszar    | 3 | ((230328-niepotrzebny-ratunek-mai; 230523-romeo-dyskretny-instalator-supreme-missionforce; 230704-maja-chciala-byc-dorosla)) |
| Klarysa Jirnik       | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Leona Astrienko      | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Romeo Verlen         | 3 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku; 230328-niepotrzebny-ratunek-mai; 230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Szczepan Myrczek     | 3 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AJA Szybka Strzała   | 2 | ((230328-niepotrzebny-ratunek-mai; 230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Albert Samszar       | 2 | ((230523-romeo-dyskretny-instalator-supreme-missionforce; 230704-maja-chciala-byc-dorosla)) |
| Apollo Verlen        | 2 | ((210311-studenci-u-verlenow; 230328-niepotrzebny-ratunek-mai)) |
| Ellarina Samarintael | 2 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor)) |
| Gabriel Lodowiec     | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Leo Kasztop          | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Mariusz Bulterier    | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| NekroTAI Zarralea    | 2 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor)) |
| OO Loricatus         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Salazar Bolza        | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Stefan Torkil        | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Tomasz Dojnicz       | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221130-astralna-flara-w-strefie-duchow)) |
| Tomasz Ruppok        | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Viorika Verlen       | 2 | ((210311-studenci-u-verlenow; 230328-niepotrzebny-ratunek-mai)) |
| Ada Wyrocznik        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Adam Chrząszczewicz  | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Amanda Kajrat        | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Bonifacy Samszar     | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Dariusz Blakenbauer  | 1 | ((210311-studenci-u-verlenow)) |
| Elena Samszar        | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Fiona Szarstasz      | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Gerwazy Kircznik     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Jonatan Lemurczak    | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Kaspian Certisarius  | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Kirea Rialirat       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Klaudiusz Terienak   | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Marcel Kulgard       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Marcelina Trzęsiel   | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Michał Perikas       | 1 | ((210311-studenci-u-verlenow)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Nadia Obiris         | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Nataniel Samszar     | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| OO Karsztarin        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Rafał Perikas        | 1 | ((210311-studenci-u-verlenow)) |
| Rufus Samszar        | 1 | ((210311-studenci-u-verlenow)) |
| Rufus Warkoczyk      | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| SCA Hadiah Emas      | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Sylwia Perikas       | 1 | ((210311-studenci-u-verlenow)) |
| Vanessa Lemurczak    | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Wirgot Samszar       | 1 | ((230704-maja-chciala-byc-dorosla)) |