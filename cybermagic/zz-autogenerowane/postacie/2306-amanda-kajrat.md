---
categories: profile
factions: 
owner: kić
title: Amanda Kajrat
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "mafia kajrata"
* owner: "kić"
* title: "Amanda Kajrat"

## Funkcjonalna mechanika

* **Obietnica**
    * "Będziesz infiltratorką i snajperem, oficerem noktiańskiej mafii. Będziesz zabójczynią wysokiej klasy, budującą organizację przestępczą dla Kajrata pięścią i sprytem."
    * "Byłaś infiltratorką noktiańską, usuwającą przeciwników ciężką snajperką. Stałaś się prawą ręką i ulubienicą szefa mafii, budując nową przyszłość dla noktian na Astorii."
* **Rdzeń mechaniczny**
    * **Mistrzostwo (1-3)**
        * GDY musi się gdzieś przedostać, TO się tam dostanie niezauważona i cierpliwie przeczeka aż będzie mieć okazję do działania. _Gibka, zwinna i cierpliwa - infiltratorka najwyższej klasy_
        * GDY walczy na serio, TO się skutecznie przemieści, schowa i snajperką ustrzeli wroga. _Niezależnie czy to servar czy anomalia, zastrzeli bez cienia zawahania._
    * **Kompetencje (3-5)**
        * GDY trzeba walczyć w zwarciu, TO walczy brudno i świetnie 'robi' nożem. _Dekadiańskie szkolenie i naturalna bezwzględność zmieniły ją w rosomaka w walce_
        * GDY trzeba przejąć kontrolę, TO żelazną pięścią i strachem wymusi co jest potrzebne. _"Możecie protestować, ale zrobicie co Wam każę. Macie w końcu rodziny."_
        * GDY jest w neutralnej sytuacji społecznej w której jej nie znają, TO urokiem i dekoltem osiągnie co chce. _Ciało i uśmiech są jej bronią tak jak nóż, strach i snajperka._
        * GDY ma do czynienia ze sprzętem wojskowym / ochroniarskim, TO znajdzie słaby punkt i zrozumie co to. "_Każdy ma hobby. Tak się składa, że moim jest niszczenie TYCH rzeczy."_
    * **Agenda i styl działania (3-5)**
        * PRAGNIE zbudować nowe, stabilne życie dla noktian na Astorii, które nie zależy od nikogo poza noktianami na Astorii. _Noctis nas zdradziło. Astoria nas niszczy. Sami stworzymy nasz los._
        * GDY ma do czynienia z ludźmi niewłaściwie traktującymi noktian, TO nie wybaczy i ich okrutnie pomści, robiąc przykład z ofiary. _Nikt nie skrzywdzi moich. Nie odważą się._
        * GDY ma okazję na pozyskanie zasobów niewłaściwych moralnie, TO się nie zawaha i nie ma z tym problemów. _Przede wszystkim przetrwać i się wzmocnić. Potem wszystko naprawimy._
        * GDY ma do wyboru - reputację lub wykonać zadanie, TO skupia na tym, co jest najlepsze dla noktian i Kajrata; ona nie ma znaczenia. _Nie jestem tutaj, by zdobywać popularność. Kajrat mnie osłoni._
        * GDY może uratować lub pomóc noktianom nie naruszając zadania, TO niewiele ją przed tym powstrzyma. _Misja przede wszystkim, ale nie możemy zostawić NASZYCH._
        * GDY ma do czynienia ze zdrajcami, TO nimi gardzi, nie ufa, nie wybacza i najchętniej usunie. _Misja to misja. Ale zdrada jest niewybaczalna. Wszystko zaczyna się od tego, że nas zdradzono._
    * **Słabości (1-5)**
        * GDY ma do czynienia z normalną sytuacją społeczną, TO zupełnie nie ma poczucia humoru. _Nawet jak na dekadiankę, humor jest czymś jej obcym._
        * GDY sytuacja wymaga empatii i wrażliwości, TO się gubi w swym pragmatyzmie. _"Rozumiem ból i strach. Ale o co TERAZ chodzi? Nic się przecież nie stało"_
        * GDY ma zadanie zlecone przez Kajrata, TO ma tendencję do nadmiernej wiary w jego dbanie o nią. _"Kajrat mnie nie zdradzi. I tylko on mnie nie zdradzi."_
    * **Zasoby (3-5)**
        * Dane z mafii odnośnie świata przestępczego i okolicy działań.
        * Dostęp do narkotyków i pomniejszych grup przestępczych, z ramienia Kajrata i własnej żelaznej pięści
        * Snajperka. Na późniejszym etapie - Eidolon.

## Reszta

### W kilku zdaniach

* .

### Jak sterować postacią

* **O czym myśli**
    * "Noctis nie wróci. Ale naszą odpowiedzialnością jest zapewnić 'naszym' jak najwięcej szans".
    * "Ernest Kajrat jest naszą najlepszą szansą i nadzieją na lepsze jutro. On nas nie zdradził. On jeden stoi za ideałami Noctis."
    * "Nie da się działać w normalnych strukturach, bo jesteśmy traktowani jak śmieci. Zrobimy własne struktury. Buduj, nie narzekaj."
    * "Magia, anomalie, magowie, całe to zawracanie głowy. Mam rozkazy i snajperkę."
* **Serce**
    * **Wound**
        * **Core Wound**: "Jestem sama, w nowym świecie i wszystko co miałam i kochałam obróciło się w proch. Straciłam wszystko i mogę stracić więcej przez astorian."
        * **Core Lie**: "Ernest Kajrat jest jedyną siłą, która daje mi - i innym - nadzieję na jutro. Nie da się zasymilować na Astorii. Odrębność noktian i mafia to jedyna droga."
    * **ENCAO**: N-C+
        * "nie da się jej wyprowadzić z równowagi", "niezależnie od poziomu stresu w sytuacji, wykona plan na zimno"
        * "nie zawaha się zrobić tego co trzeba", "jest nieskończenie cierpliwa i pragmatyczna"
        * "raczej zachowuje swoje myśli dla siebie"
        * "zawsze jest przygotowana", "jest jak skała - stabilna, niezłomna i zawsze można na niej polegać."
    * **Wartości**: Power, Security, Family
        * F: "Zostało nam tak niewiele - jesteśmy rozproszeni i rozbici. Lojalność i odbudowa, dbanie o to, co nam zostało - to się liczy. Noctis nas zdradziło, ale Kajrat nie."
        * S: "Musimy przetrwać i zapewnić, że nie stracimy tego czym jesteśmy. Uratujemy i zintegrujemy noktian."
        * P: "Siła jest istotna. Wpływ na świat ma znaczenie. Bez dobrych sojuszy, zasobów i dalszych możliwości nie tylko nikomu nie pomożemy ale się sami rozpadniemy."
    * **DELTA_DRIVE**: 
        * **Stan aktualny**: "Nie wiem co nas czeka i tylko Kajrat ma wizję jutra. Nie wiem, co mnie czeka w przyszłości. A noktianie cierpią za zdradę ze strony innych noktian."
        * **Stan oczekiwany**: "Imperium Noctis na Astorii, pod dowództwem Kajrata. Niezależność od Astorian. A ja - egzekutor Jego woli."
    * **Metakultura**: dekadianin. "_Wilk nie poluje samotnie. Jestem członkiem stada. Jednostka jest mniej istotna niż cele grupowe._"
    * **Kolory**: BW. "Porządek i wspólnota Noctis przez poświęcenie i bezwzględność. Nie wszystko musi mi się podobać, ale grupa jest ważniejsza niż jednostka."
    * **Wzory**: 
        * Kerrigan w relacji do Mengska przed jej infestacją
    * **Inne**:
        * brak

## Możliwości

* **Dominujące Strategie**: 
    * infiltracja - przeczekanie - egzekucja - ewakuacja
    * używanie narkotyków i strachu by zmusić innych do działania
    * używanie pomniejszych grup przestępczych by mieć wsparcie w dowolnej sytuacji
* **Umocowanie w świecie**: 
    * prawa ręka Ernesta Kajrata, dowódcy pronoktiańskiej mafii w Szczelińcu

## Magia
### Dominująca moc

* JEDNYM ZDANIEM: nie czaruje
    * .

#### Jak się objawia utrata kontroli

* .


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230723-crashlanding-furii-mataris   | liniowa Furia Mataris która unika wykrycia przez astorian po rozbiciu się. Schowała się przed awianami, autodestructowała kapsułę, zostawiła Lucię jako dywersję nie mogąc jej pomóc i ostrożnie prze w kierunku na Pacyfikę - tylko tam jest jakakolwiek nadzieja w aktualnej beznadziejnej sytuacji. | 0081-06-15 - 0081-06-17 |
| 230729-furia-poluje-na-furie        | weszła servarem w gniazdo jaszczuroskrzydeł by zdobyć _stash_ od Kajrata; opracowała sposób jak uciec od sił Wolnego Uśmiechu i go przeprowadziła. Snajperką zestrzeliła wrogiego awiana i ewakuowała się z Xaverą i Ayną do nowego miejsca. W najlepszej formie z Trzech Furii. | 0081-06-17 - 0081-06-18 |
| 230730-skazone-schronienie-w-fortecy | przeprowadziła Furie przez piekło do Symlotosu, tam ustrzegła je przed Skażeniem a potem opracowała plan pozyskania zasobów i dała się uratować Kajratowi. Nigdy nie straciła nadziei i wymanewrowała wszystkich przeciwników - myśleli, że Amanda się złamie, podda czy straci nadzieję, ale Amanda szła do przodu i kombinowała. Niezłomna NAWET jak na Furię. | 0081-06-18 - 0081-06-21 |
| 230806-zwiad-w-iliminar-caos        | zero poczucia humoru, co sprawiło pewną konfuzję w grupie wydzielonej Quispis; jakoś się zintegrowała z grupą noktiańską. Akceptuje erotyczne żarty na swój temat, akceptuje brak profesjonalizmu przez stosunkowo niskie morale, zaproponowała plan przebrania się za Czaszki by zastraszyć salvagerów i porwała kilku wartowników salvagerów z pomocą Leona. | 0081-06-22 - 0081-06-24 |
| 230906-operacja-mag-dla-symlotosu   | wysłana przez Kajrata do Zaczęstwa; zero poczucia humoru na Quispisie. Ale robi z grzmotodrzew dywersję dla patrolu i poświęca inteligentnie Culicyd by przejść przez Mur Pustogorski z Xaverą. | 0081-06-26 - 0081-06-28 |
| 230913-operacja-spotkac-sie-z-dmitrim | połączyła się z 'Dmitrim', ale nie dała mu swojej pozycji; gdy wyskoczył na nią polujący technolampart, zestrzeliła go snajperką. Poraniona i w bąblach, ale uniknęła Arnulfa i dotarła z Xaverą do Dmitriego. | 0081-06-28 - 0081-06-30 |
| 230920-legenda-o-noktianskiej-mafii | gdy nie udało się cicho ukraść ścigacza, zostawiła granat i ustrzeliła przeciwników (acz nie Irelię). Złamała morale Grzymościowców. Dotarła do Zaczęstwa. | 0081-07-10 - 0081-07-13 |
| 230627-ratuj-mlodziez-dla-kajrata   | 26-letnia noktiańska komandoska; ma pomóc i chronić młodych Samszarów z woli Ernesta Kajrata. Zabiła dwóch strażników bazy hiperpsychotroników, przeprowadziła Elenę bezpiecznie do uwięzionych magów. | 0095-08-20 - 0095-08-25 |
| 230704-maja-chciala-byc-dorosla     | przyhaczyła Maję na imprezie, dała jej tabletkę i przekazała agentom Wirgota by dali ją Lemurczakowi. Potem dostała się na dach i ustrzeliła Vanessę Lemurczak gdy ta torturowała Maję. Nie dbała o stan Mai - to tienka. Wszystko dla Kajrata - przechwycić dlań grupę przestępczą Złoty Cień. | 0095-08-29 - 0095-09-01 |
| 230708-wojna-w-zlotym-cieniu        | inteligentnie zinfiltrowała bazę Złotego Cienia Bilgemenera, po czym obiecała mu, że osłoni go przed gniewem Samszarów. Ściągnęła wsparcie od Kajrata by na bazie Bilgemenera odbudować Złoty Cień w nowej formie. By kupić czas, rozpaliła konflikt Secesjoniści - Lojaliści przez zabicie Secesjonisty udając Lojalistkę. | 0095-09-04 - 0095-09-08 |
| 230715-amanda-konsoliduje-zloty-cien | oddała smyczkę Wacławowi Samszarowi, przez co nieformalnie dowodzi Złotym Cieniem. Poszerzyła wpływy Cienia i dodała do Cienia grupę Przemytników, frontem są Wacław i Karolinus plus przejęła kontrolę nad większością Cienia z tła. | 0095-09-09 - 0095-09-12 |
| 230808-nauczmy-mlodego-tiena-jak-zyc | pozyskała od Bilgemenera ładną dziewczynę do pułapkowania Armina oraz dzięki Elenie nawiązała silny link z Wiktorem Blakenbauerem. Zastraszyła barmana pokazując zmumifikowane kobiece ucho (które nosi dla takich chwil). Ciężko zraniła Celinę pułapką (przypadkowo, nie doceniła słabości servara) ale potem poszła ją uratować przed makaronowym potworem jak żaden z magów nie uznał tego za istotne. Chroni "swoich". | 0095-09-14 - 0095-09-23 |
| 211120-glizda-ktora-leczy           | przekradła się koło czterech Lancerów sterowanych przez Elainkę i zestrzeliła Rolanda Sowińskiego jak przemawiał na podwyższeniu, zmieniając go w klauna. Zwykłe ćwiczenia. | 0108-04-07 - 0108-04-18 |
| 190714-kult-choroba-esuriit         | traktowana przez Kajrata jako córka, chce uratować "ojca". Świetnie wyczuwa Esuriit; wskazała Pięknotce Czółenko jako źródło problemów. | 0110-06-07 - 0110-06-09 |
| 190726-bardzo-niebezpieczne-skladowisko | skłonna do skrzywdzenia grupy terminusów-rekrutów by rozpalić wojnę Pustogor - Wolny Uśmiech. Nie chce wojny. Nie walczyła z Pięknotką, trochę podpowiedziała. Świetnie się chowa w stealth suit. | 0110-06-26 - 0110-06-28 |
| 190804-niespodziewany-wplyw-aidy    | dostarczyła Pięknotce dowodów odnośnie narkotyku Grzymościa by zredukować gorącą wojnę. Niestety, wpadła w ręce Ossidii i Saitaera - ma być przynętą na Kajrata. | 0110-06-30 - 0110-07-02 |
| 200311-wygrany-kontrakt             | Skażona energiami ixiońskimi przez Saitaera, wpadła w ręce Grzymościa, acz poniszczyła mu mafiozów solidnie | 0110-07-20 - 0110-07-23 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230729-furia-poluje-na-furie        | lekko ranna i zatruta przez straszne korzenie w okolicach Enklaw; ma personal armour i snajperkę ppanc | 0081-06-18
| 230730-skazone-schronienie-w-fortecy | lekko ranna (2 dni regeneracji), przechwycona przez siły Kajrata i inkorporowana do oddziału Isaury. | 0081-06-21
| 230708-wojna-w-zlotym-cieniu        | dalej wykrywana jako powiązana z Karolinusem i Eleną, dalej ukryta w sentisieci i pasywnie niewidoczna | 0095-09-08
| 230715-amanda-konsoliduje-zloty-cien | dostała wsparcie od Kajrata. Kasimir Esilin oraz 7 noktiańskich żołnierzy. Lojalni, cisi, robią operacje militarne. Mają też akceptowalny sprzęt. | 0095-09-12
| 230715-amanda-konsoliduje-zloty-cien | opinia pokornej służki Wacława Samszara u samego Wacława i innych osób widzących sytuację. Bilgemener widzi więcej, ale wie gdzie jest power level. | 0095-09-12
| 230808-nauczmy-mlodego-tiena-jak-zyc | ma opinię, że współpracuje z najmroczniejszymi z mrocznych Samszarów. Bilgemener się jej boi. Złoty Cień nie stanie jej na drodze. Jednocześnie w oczach Złotego Cienia, broni swoich ludzi nawet jeśli Samszarowie ich poświęcają. | 0095-09-23
| 230808-nauczmy-mlodego-tiena-jak-zyc | dzięki Elenie Samszar ma kontakt z Wiktorem Blakenbauerem, który uważa ją za bardzo potężną i wpływową agentką mafii. | 0095-09-23

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ernest Kajrat        | 10 | ((190714-kult-choroba-esuriit; 190726-bardzo-niebezpieczne-skladowisko; 200311-wygrany-kontrakt; 211120-glizda-ktora-leczy; 230627-ratuj-mlodziez-dla-kajrata; 230723-crashlanding-furii-mataris; 230729-furia-poluje-na-furie; 230730-skazone-schronienie-w-fortecy; 230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Xavera Sirtas        | 6 | ((230723-crashlanding-furii-mataris; 230729-furia-poluje-na-furie; 230730-skazone-schronienie-w-fortecy; 230906-operacja-mag-dla-symlotosu; 230913-operacja-spotkac-sie-z-dmitrim; 230920-legenda-o-noktianskiej-mafii)) |
| Ayna Marialin        | 3 | ((230723-crashlanding-furii-mataris; 230729-furia-poluje-na-furie; 230730-skazone-schronienie-w-fortecy)) |
| Karolinus Samszar    | 3 | ((230627-ratuj-mlodziez-dla-kajrata; 230704-maja-chciala-byc-dorosla; 230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Pięknotka Diakon     | 3 | ((190714-kult-choroba-esuriit; 190726-bardzo-niebezpieczne-skladowisko; 190804-niespodziewany-wplyw-aidy)) |
| Rufus Bilgemener     | 3 | ((230708-wojna-w-zlotym-cieniu; 230715-amanda-konsoliduje-zloty-cien; 230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Caelia Calaris       | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Dmitri Karpov        | 2 | ((230913-operacja-spotkac-sie-z-dmitrim; 230920-legenda-o-noktianskiej-mafii)) |
| Dragan Halatis       | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Elena Samszar        | 2 | ((230627-ratuj-mlodziez-dla-kajrata; 230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Gabriel Ursus        | 2 | ((190726-bardzo-niebezpieczne-skladowisko; 190804-niespodziewany-wplyw-aidy)) |
| Isaura Velaska       | 2 | ((230730-skazone-schronienie-w-fortecy; 230806-zwiad-w-iliminar-caos)) |
| Leon Varkas          | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Lestral Kirmanik     | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Nadia Obiris         | 2 | ((230704-maja-chciala-byc-dorosla; 230708-wojna-w-zlotym-cieniu)) |
| Petra Karpov         | 2 | ((230913-operacja-spotkac-sie-z-dmitrim; 230920-legenda-o-noktianskiej-mafii)) |
| Wacław Samszar       | 2 | ((230627-ratuj-mlodziez-dla-kajrata; 230715-amanda-konsoliduje-zloty-cien)) |
| AJA Szybka Strzała   | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Alaric Rakkeir       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Albert Samszar       | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Amelia Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Armin Samszar        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Arnulf Poważny       | 1 | ((230913-operacja-spotkac-sie-z-dmitrim)) |
| Bella Samszar        | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Bogdan Gwiazdocisz   | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Celina Maskiewnik    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Cezary Alentik       | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Dagmara Doberman     | 1 | ((200311-wygrany-kontrakt)) |
| Edmund Garzin        | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Emil Samszar         | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Feliks Keksik        | 1 | ((211120-glizda-ktora-leczy)) |
| Florian Samszar      | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Herbert Samszar      | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Impresja Ignicja Incydencja Diakon | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Irelia Kairanolis    | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Jonatan Lemurczak    | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Julia Morwisz        | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Justynian Diakon     | 1 | ((211120-glizda-ktora-leczy)) |
| Kacper Bankierz      | 1 | ((211120-glizda-ktora-leczy)) |
| Karol Pustak         | 1 | ((211120-glizda-ktora-leczy)) |
| Kasimir Esilin       | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Kleopatra Trusiek    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Leira Euridis        | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Liliana Bankierz     | 1 | ((200311-wygrany-kontrakt)) |
| Lucia Veidril        | 1 | ((230723-crashlanding-furii-mataris)) |
| Lucjusz Blakenbauer  | 1 | ((200311-wygrany-kontrakt)) |
| Maja Samszar         | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Minerwa Metalia      | 1 | ((200311-wygrany-kontrakt)) |
| Mirela Orion         | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Mitria Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Oliwia Lemurczak     | 1 | ((211120-glizda-ktora-leczy)) |
| Ossidia Saitis       | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Patryk Majwuron      | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Raab Navan           | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Ralena Karimin       | 1 | ((230729-furia-poluje-na-furie)) |
| Roland Grzymość      | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Roland Sowiński      | 1 | ((211120-glizda-ktora-leczy)) |
| Sabina Kazitan       | 1 | ((211120-glizda-ktora-leczy)) |
| Saitaer              | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Sensacjusz Diakon    | 1 | ((211120-glizda-ktora-leczy)) |
| Stella Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Tadeusz Maskiewnik   | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Tadeusz Samszar      | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Talia Aegis          | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Tomasz Tukan         | 1 | ((190714-kult-choroba-esuriit)) |
| Vanessa Lemurczak    | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Wargun Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Wiktor Blakenbauer   | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Wirgot Samszar       | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Ziemowit Zięba       | 1 | ((200311-wygrany-kontrakt)) |