---
categories: profile
factions: 
owner: public
title: SN Arcadalian
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230923-ciemnosc-pozerajaca-arcadalian | supply ship, 140 osób; chciał pomóc Murisatii i dostał na pokład kultystów którzy wpakowali mu Umbrę. Potem zagroził Ferrivatowi. Gdy stawał się Anomalią Kosmiczną, został uszkodzony i przechwycony przez ciężkie siły Agencji. | 0091-07-30 - 0091-08-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aulus Terrentus      | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Daven Hassik         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Iwo Bretonis         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Lester Martz         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Livia Sertiano       | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| OLU Luminarius       | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Salma Bluszcz        | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| SN Ferrivat          | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| SN Murisatia         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |