---
categories: profile
factions: 
owner: public
title: Dawid Aximar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220925-mlodziaki-na-savaranskim-statku-handlowym | 16-latek syn kapitana na 'Hektorze 17'; savaranin. Nieufny Orbiterowi, ale już wystarczająco kompetentny by oprowadzać Klaudię i nie doprowadzić do katastrofy. Opiekuje się sześcioma pacyfikatorami. | 0109-05-24 - 0109-05-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Fabian Korneliusz    | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Klaudia Stryk        | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Klaudiusz Zanęcik    | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Martyn Hiwasser      | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| OO Serbinius         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Perdius Aximar       | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| SC Hektor 17         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |