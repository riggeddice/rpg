---
categories: profile
factions: 
owner: public
title: Ksawery Wojnicki
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190709-somnibel-uciekl-arienikom    | niebezpieczny agent Kajrata (?), biochemik; tu: przekierowywał winę na Małgosię (astorianka, arystokratka) by nie był winny Janek (pół-noktianin). | 0110-06-05 - 0110-06-07 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ernest Kajrat        | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Jan Revlen           | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Pięknotka Diakon     | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Staś Arienik         | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Tomasz Tukan         | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Urszula Arienik      | 1 | ((190709-somnibel-uciekl-arienikom)) |