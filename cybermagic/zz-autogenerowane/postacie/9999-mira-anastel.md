---
categories: profile
factions: 
owner: public
title: Mira Anastel
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211117-porwany-trismegistos         | silnie zmieniona drakolitka; pierwszy oficer Trismegistosa. Błękitna skóra, żółte oczy, wzmocnione piękno. Dobry humor, niewiele traktuje poważnie. Kaper. | 0112-04-15 - 0112-04-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((211117-porwany-trismegistos)) |
| Elena Verlen         | 1 | ((211117-porwany-trismegistos)) |
| Eustachy Korkoran    | 1 | ((211117-porwany-trismegistos)) |
| Izabela Zarantel     | 1 | ((211117-porwany-trismegistos)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Klaudia Stryk        | 1 | ((211117-porwany-trismegistos)) |
| Leona Astrienko      | 1 | ((211117-porwany-trismegistos)) |
| Maria Naavas         | 1 | ((211117-porwany-trismegistos)) |
| OO Tivr              | 1 | ((211117-porwany-trismegistos)) |
| Roland Sowiński      | 1 | ((211117-porwany-trismegistos)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| Zygfryd Maus         | 1 | ((211117-porwany-trismegistos)) |