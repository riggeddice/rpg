---
categories: profile
factions: 
owner: public
title: Kalista Surilik
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (A+E+) | "Odważna i zdecydowana, w gorącej siarce kąpana" | VALS: (Benevolence, Achievement) | DRIVE: "Prawda MUSI wyjść na jaw" | @ 231119-tajemnicze-tunele-sebirialis
* już z nią Agencja miała do czynienia... | @ 231119-tajemnicze-tunele-sebirialis

### Wątki


agencja-lux-umbrarum
problemy-con-szernief

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231202-protest-przed-kultem-na-szernief | świetnie złapała Agencję za tematy anomalne; przekazała informacje Zespołowi o tym co wie (historia, Frederico, działania) i za to dostała wsparcie Luminariusa do prowadzenia swoich badań i śledztwa. Tak czy inaczej, jej chęć pomocy jest większa niż jej nieufność. Acz nie jest traktowana poważnie przez Agencję. | 0103-10-15 - 0103-10-19 |
| 231119-tajemnicze-tunele-sebirialis | dziennikarka badająca znikanie górników na Sebiralisie; zeszła z potencjalnymi inwestorami do kopalni robić badania i została porwana przez Eryka i Alteris. W jakiś sposób przetrwała, uratowana przez Felinę. Jej zniknięcie było dużym utrapieniem dla Rady. | 0104-11-20 - 0104-11-24 |
| 231213-polowanie-na-biosynty-na-szernief | wezwała Agencję bo coś jest bardzo nie tak; gdy już przyskrzyniła fakty (ale bez magii) Agencja poprosiła ją o współpracę przeciw chorobie pasożytniczej. Kalista na to poszła, ale były efekty uboczne i Agencja zrzuciła na nią problemy. Ucierpiała, przesunęła się bliżej Mawira Honga i dalej od Feliny. | 0105-07-26 - 0105-07-31 |
| 231122-sen-chroniacy-kochankow      | dziennikarka skupiona na prawdzie i uczciwości, podejrzewa Inwestorów o dodanie czegoś do wody i skutecznie ujawniła niektóre ruchy Agencji. Świetnie zbiera informacje, ma je też spoza stacji. | 0105-09-02 - 0105-09-05 |
| 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa | z ogromną niechęcią przyjęła pojawienie się Agencji na terenie Szernief znowu. Unika ich. Zebrała dane o tym że coś się tu dzieje i przekazała je Felinie. Ogólnie, offscreenowa. | 0105-12-11 - 0105-12-13 |
| 231221-pan-skarpetek-i-odratowany-ogrod | dowiaduje się o rzeczach magicznych i Oficer Naukowy wyjaśnia jej Pryzmat. Rozumie czemu jest tak a nie inaczej. Dalej nie ufa Agencji, ale ufa JEDNEJ OSOBIE. | 0106-04-25 - 0106-04-27 |
| 240117-dla-swych-marzen-warto       | doszła do tego, że za wszystkim stoi tajemniczy Pierścień; współpracuje z Agencją, bo to mniejsze zło. Straciła palec do Jonatana i nosiła Pierścień, co ją Aktywowało. | 0106-11-04 - 0106-11-06 |
| 240214-relikwia-z-androida          | walcząca o prawdę za wszelką cenę i zamknięta przez Aerinę w izolatce, odkryła prawdę o Elwirze i podzieliła się nią z Agencją. Zdradzona przez Aerinę i Agencję, pójdzie własną drogą. | 0107-05-16 - 0107-05-18 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 231202-protest-przed-kultem-na-szernief | miała dostęp do Luminariusa, jego publicznych banków danych i mocy obliczeniowej co mogła doskonale wykorzystać. I to zrobiła. | 0103-10-19
| 231202-protest-przed-kultem-na-szernief | nie ufa Agencji. Wie o linkach Agencji z dziwnymi rzeczami jakie się tu dzieją. Wie o ciężkiej operacji Agencji i że są dziwni. | 0103-10-19
| 231213-polowanie-na-biosynty-na-szernief | przez działania Agencji dostała silny cios reputacyjny i przesunęła się bliżej Mawira Honga (który też dostał za niewinność). Ogromna nieufność wobec Agencji. | 0105-07-31
| 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa | dzięki działaniom Mawira, jej pozycja jest bardzo podniesiona. | 0105-12-13
| 240117-dla-swych-marzen-warto       | trwale straciła palec, ale pierścień Esuriit Aktywował jej moce protomaga. Od teraz musi nosić rękawiczkę - jej palec NIGDY nie zadziała. | 0106-11-06
| 240214-relikwia-z-androida          | zdradzona przez Agencję (ponownie) oraz przez Aerinę, zbliża się do Mawira. Prawda jest najważniejsza. | 0107-05-18

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Felina Amatanir      | 6 | ((231119-tajemnicze-tunele-sebirialis; 231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto)) |
| Klasa Sabotażysta    | 6 | ((231119-tajemnicze-tunele-sebirialis; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Dyplomata      | 5 | ((231119-tajemnicze-tunele-sebirialis; 231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240214-relikwia-z-androida)) |
| Klasa Hacker         | 5 | ((231122-sen-chroniacy-kochankow; 231202-protest-przed-kultem-na-szernief; 231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Oficer Naukowy | 5 | ((231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Mawir Hong           | 5 | ((231122-sen-chroniacy-kochankow; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Aerina Cavalis       | 4 | ((231202-protest-przed-kultem-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Vanessa d'Cavalis    | 4 | ((231202-protest-przed-kultem-na-szernief; 231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Inżynier       | 3 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240214-relikwia-z-androida)) |
| Artur Tavit          | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Dorion Fughar        | 2 | ((231122-sen-chroniacy-kochankow; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Ignatius Sozyliw     | 2 | ((231119-tajemnicze-tunele-sebirialis; 231221-pan-skarpetek-i-odratowany-ogrod)) |
| Klasa Biurokrata     | 2 | ((231202-protest-przed-kultem-na-szernief; 231213-polowanie-na-biosynty-na-szernief)) |
| Larkus Talvinir      | 2 | ((231119-tajemnicze-tunele-sebirialis; 240117-dla-swych-marzen-warto)) |
| OLU Luminarius       | 2 | ((231119-tajemnicze-tunele-sebirialis; 231202-protest-przed-kultem-na-szernief)) |
| Alina Mekran         | 1 | ((240214-relikwia-z-androida)) |
| Aniela Kafantelas    | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Damian Orczakin      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Delgado Vitriol      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Elwira Barknis       | 1 | ((240214-relikwia-z-androida)) |
| Eryk Kawanicz        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Estril Cavalis       | 1 | ((240117-dla-swych-marzen-warto)) |
| Frederico Zyklas     | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Gabriel Septenas     | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Janvir Krassus       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Jola-09 Szernief     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Kaldor Czuk          | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klaudiusz Widar      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Leon Hurmniow        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Maia Sakiran         | 1 | ((240117-dla-swych-marzen-warto)) |
| Malik Darien         | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Marta Sarilit        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Rovis Skarun         | 1 | ((231122-sen-chroniacy-kochankow)) |
| Sebastian-194        | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Sia-03 Szernief      | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Szymon Alifajrin     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Tadeusz Mekran       | 1 | ((240214-relikwia-z-androida)) |