---
categories: profile
factions: 
owner: public
title: SCA Hadiah Emas
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221116-astralna-flara-dociera-do-nonariona-nadziei | ciężko uszodzona jednostka niewolniczo-transportowa; próbowali pomóc Isigtand i TEŻ zestrzeliła ich anomalna asteroida grawitacyjna. 23 osoby, z czego 15 to niewolnicy. | 0100-09-08 - 0100-09-11 |
| 221123-egzotyczna-pieknosc-na-astralnej-flarze | obiekt naprawy przez inżynierów z Astralnej Flary; doprowadzona do funkcjonowania. | 0100-09-12 - 0100-09-15 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 221123-egzotyczna-pieknosc-na-astralnej-flarze | części złożone przez Flarę; nawet jeśli jednostka jest delikatniejsza, to jednak jest sprawna. Niewolnicy uwolnieni, ale nadal na Hadiah Emas. | 0100-09-15
| 221123-egzotyczna-pieknosc-na-astralnej-flarze | jej TAI, Mirtaela, została Ograniczona przez Orbiter. Straciła "duszę", nawet jeśli zyskała efektywność. Załoga jeszcze nie wie. | 0100-09-15

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Arnulf Perikas       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Daria Czarnewik      | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gabriel Lodowiec     | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Kajetan Kircznik     | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Leszek Kurzmin       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Marcel Kulgard       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| OO Astralna Flara    | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| OO Athamarein        | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Władawiec Diakon     | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Alezja Dumorin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Ellarina Samarintael | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Erwin Pies           | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gerwazy Kircznik     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Grażyna Burgacz      | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Leo Kasztop          | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Maja Samszar         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Szczepan Myrczek     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Szymon Wanad         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Tomasz Ruppok        | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |