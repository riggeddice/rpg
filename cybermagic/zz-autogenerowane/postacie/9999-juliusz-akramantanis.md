---
categories: profile
factions: 
owner: public
title: Juliusz Akramantanis
---

# {{ page.title }}


# Generated: 



## Fiszki


* advancer, (dekadianin) | @ 221220-dezerter-z-mrocznego-wolu
* ENCAO:  --0-+ |Niepokojący;;Innowacyjny, szuka nowych rozwiązań| VALS: Benevolence, Family >> Tradition| DRIVE: Złota klatka Lauriego (możliwość hobby) | @ 221220-dezerter-z-mrocznego-wolu
* "leave me alone"; ma kolekcję krwawych kryształów z każdego zabitego przeciwnika (34) | @ 221220-dezerter-z-mrocznego-wolu

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221220-dezerter-z-mrocznego-wolu    | advancer noktiański na Mrocznym Wole, dekadianin; bił się z szefem ochrony i nazwano go 'sick puppy', niepokojący jak cholera. Przemyca coś w jednej z wnęk statku. | 0111-11-15 - 0111-11-18 |
| 230103-protomag-z-mrocznego-wolu    | ignoruje Adelę, nie chce współpracować z agentką Orbitera. Wyraźnie lubi Nelę po swojemu. Gardzi załogą Mrocznego Wołu. Mało sympatyczny, ale po swojemu lojalny choć robi kłopoty. Lena chce się go pozbyć lub zrobić mu krzywdę. Przyznał się do zabicia Wincentego by ratować Nelę, ale Zespół nie zgodził się na to - znają prawdę. Stawia opór w każdy możliwy sposób i niszczy statystyki. | 0111-11-19 - 0111-11-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adela Myrias         | 2 | ((221220-dezerter-z-mrocznego-wolu; 230103-protomag-z-mrocznego-wolu)) |
| Marianna Atrain      | 2 | ((221220-dezerter-z-mrocznego-wolu; 230103-protomag-z-mrocznego-wolu)) |
| Nadja Kilmodrian     | 2 | ((221220-dezerter-z-mrocznego-wolu; 230103-protomag-z-mrocznego-wolu)) |
| OO Mroczny Wół       | 2 | ((221220-dezerter-z-mrocznego-wolu; 230103-protomag-z-mrocznego-wolu)) |
| Baltazar Kwarcyk     | 1 | ((221220-dezerter-z-mrocznego-wolu)) |
| Krzysztof Workisz    | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Lena Ifirentis       | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Maja Szewieczak      | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Nela Kaltaner        | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Robert Warłomkacz    | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Tytus Muszczak       | 1 | ((221220-dezerter-z-mrocznego-wolu)) |