---
categories: profile
factions: 
owner: public
title: Leira Euridis
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (C+N-O-): Chłodna i skoncentrowana, jej stalowe nerwy sprawiają, że nigdy nie traci zimnej krwi. "Ruch po ruchu, krok po kroku - to jest sposób, aby przetrwać." | @ 230730-skazone-schronienie-w-fortecy
* VALS: (Achievement, Self-Direction): Uważa, że przez ciężką pracę i samodzielność można osiągnąć cel. "Nie mogę polegać na nikim innym niż ja sama." | @ 230730-skazone-schronienie-w-fortecy
* Core Wound - Lie: "Byłam zabawką dla możnych, gdy byłam słabą dziewczynką" - "Muszę być cicha i ostrożna, ponieważ ludzie są nieprzewidywalni i niebezpieczni." | @ 230730-skazone-schronienie-w-fortecy
* Styl: Cicha i ostrożna samotniczka, Leira jest zawsze czujna i gotowa do walki. Jej chłodny spokój skrywa gorącą pasję do ochrony tych, których kocha. | @ 230730-skazone-schronienie-w-fortecy

### Wątki


pronoktianska-mafia-kajrata
furia-mataris-agentka-mafii

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230730-skazone-schronienie-w-fortecy | Furia, którą uratowała Forteca Symlotosu. Zaadaptowana przez nich, została przewodniczką dla Trzech Furii. Jest szczęśliwa, pierwszy raz ma spokój. Jeszcze nie jest zainfekowana Symlotosem. | 0081-06-18 - 0081-06-21 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230730-skazone-schronienie-w-fortecy | dotknięta przez Symlotos, agentka Fortecy choć jeszcze nie do końca tego świadoma. Kajrat może ją wykupić za maga. | 0081-06-21

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Ayna Marialin        | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Ernest Kajrat        | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Isaura Velaska       | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Raab Navan           | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Xavera Sirtas        | 1 | ((230730-skazone-schronienie-w-fortecy)) |