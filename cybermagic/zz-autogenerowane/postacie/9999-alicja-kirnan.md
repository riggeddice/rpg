---
categories: profile
factions: 
owner: public
title: Alicja Kirnan
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221122-olgierd-lowca-potworow       | agentka Lutusa Saraana, jego kurierka. Uratowana przez Olgierda, powiedziała mu o Lutusie. | 0096-07-11 - 0096-07-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Borys Uprakocz       | 1 | ((221122-olgierd-lowca-potworow)) |
| Jakub Altair         | 1 | ((221122-olgierd-lowca-potworow)) |
| Lutus Saraan         | 1 | ((221122-olgierd-lowca-potworow)) |
| Maciek Kwaśnica      | 1 | ((221122-olgierd-lowca-potworow)) |
| Olgierd Drongon      | 1 | ((221122-olgierd-lowca-potworow)) |