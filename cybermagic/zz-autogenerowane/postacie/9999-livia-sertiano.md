---
categories: profile
factions: 
owner: public
title: Livia Sertiano
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230923-ciemnosc-pozerajaca-arcadalian | kapitan Arcadaliana; kiedyś naukowiec, szybko zorientowała się, że coś się tu dzieje dziwnego i staje po stronie Agencji. Nie zrobiła żadnych poważnych błędów, po prostu nie wiedziała o magii i dała zainfekować Arcadalian przez Zakon. | 0091-07-30 - 0091-08-02 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230923-ciemnosc-pozerajaca-arcadalian | po wszystkim została Junior Szpieg Agencji Lux Umbrarum; wie o magii i jej wiedza naukowca okaże się być bardzo przydatna w służbie Agencji | 0091-08-02

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aulus Terrentus      | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Daven Hassik         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Iwo Bretonis         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Lester Martz         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| OLU Luminarius       | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| Salma Bluszcz        | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| SN Arcadalian        | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| SN Ferrivat          | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |
| SN Murisatia         | 1 | ((230923-ciemnosc-pozerajaca-arcadalian)) |