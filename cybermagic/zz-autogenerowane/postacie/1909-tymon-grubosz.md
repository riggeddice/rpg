---
categories: profile
factions: 
owner: public
title: Tymon Grubosz
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "terminus, pustogor"
* owner: "public"
* title: "Tymon Grubosz"


## Paradoks

strasznie groźny i potężny terminus szczerze wierzący w Astorię - jednocześnie trochę naiwny

## Motywacja

### O co walczy

* o zachowanie ludzkości i cywilizacji astoriańskiej; optymizm, bezpieczeństwo, pomaganie sobie
* szczerze wierzy w Pustogor, Karlę oraz w misję terminusów na tym terenie
* integracja noktian, astorian - wszystkich w obszarze Szczelińca. Integracja ludzkości.

### Przeciw czemu walczy

* cokolwiek zagraża jego przyjaciołom i osobom pod jego opieką
* anomalie magiczne i Skażenie - włączając w to Saitaera czy Wiktora Sataraila
* chaos, szaleństwo, utrata kontroli - utrata cywilizacji i kultury

## Działania

### Specjalność

* terminus o ogromnej wręcz odporności na wszelkie ciosy, z hiperstabilnym Wzorem
* katalista zdolny do transferu, kontroli lub przerzucania ogromnych energii magicznych
* katai, wspomagany przez ciężki sprzęt terminuski.
* pokorny, o nieskończenie wielkiej wierze w Karlę, Pustogor i Zjednoczenie Astoriańskie - na co magia odpowiedziała

### Słabość

* gołębie serce; czasem zbyt łagodny lub daje o jedną szansę za dużo
* nie chce robić innym problemów, ma tendencje do brania za dużo na siebie
* zawsze próbuje pomóc przyjaciołom; nie odrzuca ich próśb - przez co często cierpi
* naiwny jak na terminusa

### Akcje

* Katai w superciężkim servarze Entropik, niszczący problemy wspomaganą bronią białą
* Zawsze próbuje pomóc i zawsze zakłada, że druga strona chce dobrze
* Przekierowuje i steruje energią magiczną jakby był istotą Vim
* Traktuje Pustogor czy Zjednoczenie Astoriańskie w sposób religijny i tak o tym mówi

### Znaczące czyny

* Potrafił samemu zogniskować i odepchnąć falę energii z Trzęsawiska. Ucierpiał, ale przetrwał.
* Swoją obecnością dał radę zdeeskalować niebezpieczną sytuację; czterech magów nie odważyło się z nim walczyć.
* Infekcja Sataraila. Fala Esuriit. Wszystkie takie ciosy energetyczne jego Wzór wytrzymał. Prawdziwy niezłomny paladyn. Wiara jest jego tarczą.
* Pod wpływem narkotyków czy innych środków, nie obrócił się przeciwko Karli czy Pustogorowi. Nieskończenie wierzy w swoją misję.
* Samotnie potrafił przebić się przez niewielką bandę Grzymościa, po czym ich rozbił i zmusił do ucieczki.

## Mechanika

### Archetypy

.

### Motywacje

.

## Inne

### Wygląd

Dwumetrowy i 120-kilogramowy terminus o strasznym obliczu; zwykle w swoim superciężkim servarze Entropik. Na ciele ma 84 tatuaże - imiona i nazwiska magów, którzy zginęli za Astorię / Szczeliniec i których znał za życia.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210926-nowa-strazniczka-amz         | 28 lat; "emerytowany" neuroszturmowiec Orbitera. "Gość" terminusów. Współpracuje z Arnulfem i Talią nad zrobieniem Strażniczki Alair. Przekonuje młodziutkiego Trzewnia, żeby nie zgłaszał tego Pustogorowi (chce mu oszczędzić amnestyków). | 0084-06-14 - 0084-06-26 |
| 211010-ukryta-wychowanka-arnulfa    | terminus; przybył pomóc Strażniczce w nocy do AMZ i pomógł Arnulfowi w obronie przed atakami dron aż Talia i Klaudia nie wyłączyły TAI. Potem skonfrontował się z nim Sasza, ale Tymon to olał. | 0084-12-11 - 0084-12-12 |
| 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie | Morderczy terminus, ze wsparciem dyrektora i pod osłoną atakujących potworów i braku detekcji zdewastował obóz porywaczy dzieci. Dyskretna operacja dla dyskretnego problemu. | 0085-01-26 - 0085-01-28 |
| 181101-wojna-o-uczciwe-polfinaly    | przerażający terminus-katalista o gołębim sercu. Dobry przyjaciel Pięknotki. Miał niefortunnie piękny efekt Skażenia rozrywając Karolinę przy studni. | 0109-10-17 - 0109-10-19 |
| 181114-neutralizacja-artylerii-koszmarow | wsparł Pięknotkę w przejęciu kontrolę nad Nieużytkami Staszka atakowanymi przez Pnączoszpona (który tylko ich straszył) | 0109-10-20 - 0109-10-21 |
| 190827-rozpaczliwe-ratowanie-bii    | pomaga Pięknotce rozwiązać problem "Wiktora" (czyli BIA) w Zaczęstwie. Osłania Pięknotkę gdy idą do Talii Aegis. | 0110-01-18 - 0110-01-21 |
| 190120-nowa-minerwa-w-nowym-swiecie | miał nadzieję na wsparcie i dzięki Pięknotce do Zaczęstwa dostał wsparcie oraz Minerwę. Akceptuje pomoc Minerwie; da jej szansę. | 0110-01-22 - 0110-01-26 |
| 190127-ixionski-transorganik        | lubi Pięknotkę i nie chce jej robić problemów; puścił płazem ewentualne problemy z Minerwą i Erwinem. | 0110-01-29 - 0110-01-30 |
| 190202-czarodziejka-z-woli-saitaera | terminus który prawie wpadł w katastrofalne kłopoty, bo ukrył działanie Sławka i Adeli wobec Wojtka. Potem osłaniał Minerwę i Pięknotkę. O dziwo, przeszło. | 0110-02-01 - 0110-02-05 |
| 190828-migswiatlo-psychotroniczek   | terminus - technomanta i katalista kontrolujący Zaczęstwo. Tym razem po prostu uznał, że Minerwa nie może budować TAI na rynek - za duże ryzyko ixionu. | 0110-02-08 - 0110-02-10 |
| 190206-nie-da-sie-odrzucic-mocy     | wsparcie Pięknotki w przebijaniu się przez Cyberszkołę skażoną ixiońsko, chroni uderzenia padające w Minerwę i Pięknotkę. | 0110-02-17 - 0110-02-20 |
| 190217-chevaleresse                 | wezwał Pięknotkę na wszelki wypadek - i była potrzebna. Sklupał Dianę i Karolinę. Nie umiał deeskalować z dziewczynami i wolał się wycofać i oddać jej problem. | 0110-02-25 - 0110-02-27 |
| 190619-esuriit-w-sercu-alicji       | terminus, który zatroszczył się o los Alicji Kiermacz. Wywiózł ją do Pustogoru po upewnieniu się, że w Alicji jakoś zagnieździło się Esuriit. | 0110-04-16 - 0110-04-18 |
| 191105-zaginiona-soniczka           | dewastująca maszyna zniszczenia, który przez Efekt Skażenia wygląda jak słodka Mariolka. Nieszczęśliwy, ale poszedł szturmować. | 0110-07-02 - 0110-07-03 |
| 190820-liliana-w-swiecie-dokumentow | terminus, który BARDZO próbował nie zauważyć noktiańskiej firmy Kajrata pomagającej Trzeciemu Rajowi. Niestety, uczniowie szkoły magów nie dali mu tej szansy. | 0110-07-04 - 0110-07-08 |
| 200222-rozbrojenie-bomby-w-kalbarku | znalazł zastępstwo na Zaczęstwo i pojedzie pomóc Pięknotce i Ataienne rozwiązać problemy w Kalbarku. Oczywiście, dyskretnie i na urlopie. | 0110-07-27 - 0110-08-01 |
| 200510-tajna-baza-orbitera          | tym razem jego groźne oblicze, kataliza i technomancja się przydały - wykrył, że czujniki na autofarmie Kołczonda mają NIE znaleźć bazy w Studni Irrydiańskiej. | 0110-09-07 - 0110-09-11 |
| 201020-przygoda-randka-i-porwanie   | aresztował Sorbiana - dygnitarza i ochroniarza Aurum. Zniszczył prototypowego, niebezpiecznego konstruminusa Triany który wymknął się spod kontroli. Złapał Gabriela i Ulę przy efemerydzie. Ogólnie, żmudny, głupi i pracowity czas. | 0110-10-25 - 0110-10-27 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211010-ukryta-wychowanka-arnulfa    | ma wroga w Saszy Morwowcu (terminus). Uważa go za konspiratora próbującego działać na szkodę Pustogoru. Z jakiegoś powodu. | 0084-12-12
| 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie | zbudował silniejszy link z Arnulfem Poważnym; wspólnie uczestniczyli w operacji niszczenia porywaczy dzieci. | 0085-01-28

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 12 | ((181101-wojna-o-uczciwe-polfinaly; 181114-neutralizacja-artylerii-koszmarow; 190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy; 190217-chevaleresse; 190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek; 191105-zaginiona-soniczka; 200222-rozbrojenie-bomby-w-kalbarku; 200510-tajna-baza-orbitera)) |
| Arnulf Poważny       | 5 | ((190206-nie-da-sie-odrzucic-mocy; 190820-liliana-w-swiecie-dokumentow; 210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Karolina Erenit      | 5 | ((181101-wojna-o-uczciwe-polfinaly; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy; 190217-chevaleresse)) |
| Mariusz Trzewń       | 5 | ((190827-rozpaczliwe-ratowanie-bii; 191105-zaginiona-soniczka; 200510-tajna-baza-orbitera; 210926-nowa-strazniczka-amz; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Minerwa Metalia      | 5 | ((190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy; 190828-migswiatlo-psychotroniczek)) |
| Talia Aegis          | 4 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek; 210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Erwin Galilien       | 3 | ((190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 191105-zaginiona-soniczka)) |
| Klaudia Stryk        | 3 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Marlena Maja Leszczyńska | 3 | ((181101-wojna-o-uczciwe-polfinaly; 190206-nie-da-sie-odrzucic-mocy; 190217-chevaleresse)) |
| Teresa Mieralit      | 3 | ((200510-tajna-baza-orbitera; 211010-ukryta-wychowanka-arnulfa; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Alan Bartozol        | 2 | ((181101-wojna-o-uczciwe-polfinaly; 190217-chevaleresse)) |
| Ataienne             | 2 | ((191105-zaginiona-soniczka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Diana Tevalier       | 2 | ((190217-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Ernest Kajrat        | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Gabriel Ursus        | 2 | ((200510-tajna-baza-orbitera; 201020-przygoda-randka-i-porwanie)) |
| Kasjopea Maus        | 2 | ((190120-nowa-minerwa-w-nowym-swiecie; 190206-nie-da-sie-odrzucic-mocy)) |
| Ksenia Kirallen      | 2 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Liliana Bankierz     | 2 | ((190820-liliana-w-swiecie-dokumentow; 201020-przygoda-randka-i-porwanie)) |
| Marek Puszczok       | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Mateusz Kardamacz    | 2 | ((191105-zaginiona-soniczka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Saitaer              | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Strażniczka Alair    | 2 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Tadeusz Kruszawiecki | 2 | ((181101-wojna-o-uczciwe-polfinaly; 190820-liliana-w-swiecie-dokumentow)) |
| Wiktor Satarail      | 2 | ((181114-neutralizacja-artylerii-koszmarow; 190127-ixionski-transorganik)) |
| Wojtek Kurczynos     | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Adela Kirys          | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
| Adela Pieczar        | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Albert Kalandryk     | 1 | ((210926-nowa-strazniczka-amz)) |
| Aleksandra Szklarska | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Alicja Kiermacz      | 1 | ((190619-esuriit-w-sercu-alicji)) |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Artur Kołczond       | 1 | ((200510-tajna-baza-orbitera)) |
| Artur Michasiewicz   | 1 | ((190828-migswiatlo-psychotroniczek)) |
| Bartłomiej Małczarek | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| BIA Tarn             | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Damian Podpalnik     | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
| Daniel Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Eliza Farnorz        | 1 | ((190619-esuriit-w-sercu-alicji)) |
| Felicja Melitniek    | 1 | ((181114-neutralizacja-artylerii-koszmarow)) |
| Felicjan Szarak      | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Henryk Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Ignacy Myrczek       | 1 | ((200510-tajna-baza-orbitera)) |
| Kacper Bankierz      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Kaella Sarimanis     | 1 | ((231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Karla Mrozik         | 1 | ((190120-nowa-minerwa-w-nowym-swiecie)) |
| Kirył Najłalmin      | 1 | ((190127-ixionski-transorganik)) |
| Kornel Garn          | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Laura Tesinik        | 1 | ((200510-tajna-baza-orbitera)) |
| Laurencjusz Sorbian  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Lucjusz Blakenbauer  | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Mariola Tralment     | 1 | ((191105-zaginiona-soniczka)) |
| Napoleon Bankierz    | 1 | ((190127-ixionski-transorganik)) |
| Natalia Tessalon     | 1 | ((200510-tajna-baza-orbitera)) |
| Rafał Muczor         | 1 | ((190619-esuriit-w-sercu-alicji)) |
| Robert Pakiszon      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Robinson Porzecznik  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Sabina Kazitan       | 1 | ((200510-tajna-baza-orbitera)) |
| Sasza Morwowiec      | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Sławomir Muczarek    | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
| Sławomir Niejadek    | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Szymon Jaszczurzec   | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Tomasz Tukan         | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Triana Porzecznik    | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Urszula Miłkowicz    | 1 | ((201020-przygoda-randka-i-porwanie)) |