---
categories: profile
factions: 
owner: public
title: Mścigrom Verlen
---

# {{ page.title }}


# Generated: 



## Fiszki


* ENCAO: 0+++- | Przeszłość jest kluczem do przyszłości | VALS: Tradition, Achievement >> Self-Direction | DRIVE: Past ideals, traditions and duty | @ 230627-ratuj-mlodziez-dla-kajrata
* styl: WG; spolegliwy, sympatyczny, bardzo sztywne podejście | "sprawdzi w przeszłości jak zadziałać w teraźniejszości" | @ 230627-ratuj-mlodziez-dla-kajrata
* ruch: | @ 230627-ratuj-mlodziez-dla-kajrata
* "wszystko co jest potrzebne jest w naszej przeszłości, wszystko da się rozwiązać przez analizę tego co było" | @ 230627-ratuj-mlodziez-dla-kajrata

### Wątki


hiperpsychotronika-samszarow
pronoktianska-mafia-kajrata

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210224-sentiobsesja                 | 28 lat, dał się przekonać kuzynowi i wyjechał z Holdu Bastion, przez co naraził Apolla na sentiobsesję. Powinien wiedzieć lepiej. Bardzo spolegliwy i chętny do pomocy swoim. | 0092-10-01 - 0092-10-04 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Apollo Verlen        | 1 | ((210224-sentiobsesja)) |
| Arianna Verlen       | 1 | ((210224-sentiobsesja)) |
| Lucjusz Blakenbauer  | 1 | ((210224-sentiobsesja)) |
| Milena Blakenbauer   | 1 | ((210224-sentiobsesja)) |
| Przemysław Czapurt   | 1 | ((210224-sentiobsesja)) |
| Viorika Verlen       | 1 | ((210224-sentiobsesja)) |