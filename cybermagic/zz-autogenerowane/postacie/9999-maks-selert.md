---
categories: profile
factions: 
owner: public
title: Maks Selert
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221006-ona-chce-dziecko-eustachego  | Exerinn; podoba mu się Ardilla. Ma 15 lat i myśli że ją podrywa. Ogólnie dość przydatny, ale nie zrobił nic szczególnego - pokazał gdzie ona ma iść. | 0092-09-20 - 0092-09-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Ava Kieras           | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Emban Dolamor        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Eustachy Korkoran    | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Lerten Kieras        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Michał Kervendal     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| OO Infernia          | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Staszek Zakraton     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| VN Exerinn           | 1 | ((221006-ona-chce-dziecko-eustachego)) |