---
categories: profile
factions: 
owner: public
title: Igor Arłacz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231025-spiew-nielalki-na-castigatorze | tien na Castigatorze, chciał powstrzymać NieLalkę i przez to zaczął sabotować Hangar ŁeZ (zmiana Znaczeń). Współpracował z Eustachym (XD) przeciw 'ketchupowi' (ochronie Castigatora). Skończył w medycznym. Niegroźny, dość kompetentny we wszystkim i w niczym nie jest świetny. | 0110-10-24 - 0110-10-26 |
| 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze | (nieobecny) został chłopakiem Eleny (ona zrobiła pierwszy ruch); kolejny ślad obecności anipacis. | 0110-12-19 - 0110-12-21 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 231025-spiew-nielalki-na-castigatorze | wstrząśnięty przez NieLalkę. Przeświadczony, że MUSI znaleźć sposób radzenia sobie z Altarient i dowiedzieć się o tej energii jak najwięcej. Straumatyzowany po sprawie. | 0110-10-26

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Elena Verlen         | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Klaudia Stryk        | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Konstanty Keksik     | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Leszek Kurzmin       | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Marta Keksik         | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Patryk Samszar       | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Anna Tessalon        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Arianna Verlen       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Eleonora Perłamila   | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Eustachy Korkoran    | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Feliks Walrond       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Julia Myrczek        | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Kamil Burgacz        | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Kosmicjusz Tanecznik Diakon | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Leona Astrienko      | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Łucja Larnecjat      | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Martyn Hiwasser      | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| OO Castigator        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| OO Infernia          | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Raoul Lavanis        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| TAI Eszara d'Castigator | 1 | ((231025-spiew-nielalki-na-castigatorze)) |