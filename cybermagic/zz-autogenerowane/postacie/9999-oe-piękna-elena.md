---
categories: profile
factions: 
owner: public
title: OE Piękna Elena
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210428-infekcja-serenit             | okręt dowodzony przez Tadeusza Ursusa, który rozpaczliwie szuka pomocy dla OE Falołamacza. | 0111-11-22 - 0111-11-23 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Serenit           | 1 | ((210428-infekcja-serenit)) |
| Arianna Verlen       | 1 | ((210428-infekcja-serenit)) |
| Elena Verlen         | 1 | ((210428-infekcja-serenit)) |
| Eustachy Korkoran    | 1 | ((210428-infekcja-serenit)) |
| Klaudia Stryk        | 1 | ((210428-infekcja-serenit)) |
| Martyn Hiwasser      | 1 | ((210428-infekcja-serenit)) |
| OE Falołamacz        | 1 | ((210428-infekcja-serenit)) |
| OO Infernia          | 1 | ((210428-infekcja-serenit)) |
| Persefona d'Infernia | 1 | ((210428-infekcja-serenit)) |
| Rafał Grambucz       | 1 | ((210428-infekcja-serenit)) |
| Tadeusz Ursus        | 1 | ((210428-infekcja-serenit)) |