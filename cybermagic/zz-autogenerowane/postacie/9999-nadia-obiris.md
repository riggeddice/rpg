---
categories: profile
factions: 
owner: public
title: Nadia Obiris
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230704-maja-chciala-byc-dorosla     | agentka Złotego Cienia; na callu z Karolinusem powiedziała jakie blondynki ma dla niego do oferty. Karolinus ją namierzył sentisiecią i podał lokalizację Amandzie. | 0095-08-29 - 0095-09-01 |
| 230708-wojna-w-zlotym-cieniu        | bardzo niechętnie, ale wprowadzona ponownie do działań operacyjnych. Przekonała dziewczyny 'Czachy' z Secesjonistów, że ktoś ze Złotego Cienia chce się spotkać z 'Czachą'. | 0095-09-04 - 0095-09-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 2 | ((230704-maja-chciala-byc-dorosla; 230708-wojna-w-zlotym-cieniu)) |
| Albert Samszar       | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Jonatan Lemurczak    | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Karolinus Samszar    | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Maja Samszar         | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Rufus Bilgemener     | 1 | ((230708-wojna-w-zlotym-cieniu)) |
| Vanessa Lemurczak    | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Wirgot Samszar       | 1 | ((230704-maja-chciala-byc-dorosla)) |