---
categories: profile
factions: 
owner: public
title: Amadeusz Martel d'Kopiec
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211016-zlamane-serce-martyna        | najbardziej zaufany przez Martyna agent Jolanty. Pomógł Martynowi się zregenerować po zamachu i chronić arkin. | 0085-08-31 - 0085-09-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adela Kołczan        | 1 | ((211016-zlamane-serce-martyna)) |
| Adrian Kozioł        | 1 | ((211016-zlamane-serce-martyna)) |
| Jolanta Kopiec       | 1 | ((211016-zlamane-serce-martyna)) |
| Kalina Rota d'Kopiec | 1 | ((211016-zlamane-serce-martyna)) |
| Martyn Hiwasser      | 1 | ((211016-zlamane-serce-martyna)) |
| Wanessa Ogarek       | 1 | ((211016-zlamane-serce-martyna)) |