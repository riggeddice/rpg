---
categories: profile
factions: 
owner: public
title: Nikodem Sowiński
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200923-magiczna-burza-w-raju        | tien, z pomocą Marianny 2 lata temu pokonał oddział noktian, którzy chcieli porwać Anastazję. Przejął tymczasowo dziką sentisieć; poszedł w górę w pozycji. | 0111-02-21 - 0111-02-25 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 201021-noktianie-rodu-arlacz        | publiczne (na wizji) potwierdził, że Anastazja i on współpracowali by znaleźć zdrajcę. Jest bezpieczny przed konsekwencjami dzięki Ariannie. | 0111-03-10

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazja Sowińska   | 1 | ((200923-magiczna-burza-w-raju)) |
| Arianna Verlen       | 1 | ((200923-magiczna-burza-w-raju)) |
| Dariusz Krantak      | 1 | ((200923-magiczna-burza-w-raju)) |
| Elena Verlen         | 1 | ((200923-magiczna-burza-w-raju)) |
| Eliza Ira            | 1 | ((200923-magiczna-burza-w-raju)) |
| Eustachy Korkoran    | 1 | ((200923-magiczna-burza-w-raju)) |
| Izabela Zarantel     | 1 | ((200923-magiczna-burza-w-raju)) |
| Klaudia Stryk        | 1 | ((200923-magiczna-burza-w-raju)) |
| Marian Fartel        | 1 | ((200923-magiczna-burza-w-raju)) |
| Marianna Lemurczak   | 1 | ((200923-magiczna-burza-w-raju)) |