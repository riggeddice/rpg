---
categories: profile
factions: 
owner: public
title: Selena Walecznik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210302-umierajaca-farma-biovatow    | lekarka we Wremłowie; bardzo sensowna, ale tym razem zdominowana przez Lucjuszową płaszczkę; nie zauważała przez nią działań Lucjusza. Wykryta przez Viorikę, płaszczka grała na czas. | 0093-08-17 - 0093-08-19 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Frezja Amanit        | 1 | ((210302-umierajaca-farma-biovatow)) |
| Lucjusz Blakenbauer  | 1 | ((210302-umierajaca-farma-biovatow)) |
| Viorika Verlen       | 1 | ((210302-umierajaca-farma-biovatow)) |