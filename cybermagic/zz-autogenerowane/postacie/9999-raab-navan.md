---
categories: profile
factions: 
owner: public
title: Raab Navan
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230730-skazone-schronienie-w-fortecy | jeden ze speakerów Symlotosu, dowodził operacją mającą uratować Trzy Furie przed Czarnymi Czaszkami. Z powodzeniem. | 0081-06-18 - 0081-06-21 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Ayna Marialin        | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Ernest Kajrat        | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Isaura Velaska       | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Leira Euridis        | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Xavera Sirtas        | 1 | ((230730-skazone-schronienie-w-fortecy)) |