---
categories: profile
factions: 
owner: public
title: Ossidia Saitis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190519-uciekajacy-seksbot           | seksbot bardzo źle traktowany przez Kajrata; pomaga mu Eliza Ira oraz Pięknotka przekazała go Saitaerowi, by seksbot nie cierpiał. | 0110-04-25 - 0110-04-26 |
| 190623-noc-kajrata                  | prawie zabiła Kajrata gdyby nie energia Esuriit; prawie zginęła do upiora Esuriit gdyby nie Pięknotka. Kajrat jest z niej bardzo dumny - uważa ją za swoje arcydzieło. Wróciła do Saitaera. | 0110-05-22 - 0110-05-25 |
| 190804-niespodziewany-wplyw-aidy    | nadal poluje na Kajrata; Pięknotka ją przekonała by pomogła z wyczyszczeniem tematu z Hralwaghiem. W zamian za Amandę Kajrat. | 0110-06-30 - 0110-07-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 3 | ((190519-uciekajacy-seksbot; 190623-noc-kajrata; 190804-niespodziewany-wplyw-aidy)) |
| Ernest Kajrat        | 2 | ((190519-uciekajacy-seksbot; 190623-noc-kajrata)) |
| Liliana Bankierz     | 2 | ((190519-uciekajacy-seksbot; 190623-noc-kajrata)) |
| Saitaer              | 2 | ((190519-uciekajacy-seksbot; 190804-niespodziewany-wplyw-aidy)) |
| Amanda Kajrat        | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Arnulf Poważny       | 1 | ((190519-uciekajacy-seksbot)) |
| Eliza Ira            | 1 | ((190519-uciekajacy-seksbot)) |
| Gabriel Ursus        | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Julia Morwisz        | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Serafina Ira         | 1 | ((190623-noc-kajrata)) |
| Teresa Mieralit      | 1 | ((190519-uciekajacy-seksbot)) |
| Tomasz Tukan         | 1 | ((190519-uciekajacy-seksbot)) |