---
categories: profile
factions: 
owner: public
title: Laurencjusz Kidiron
---

# {{ page.title }}


# Generated: 



## Fiszki


* (ENCAO: +0--- |Żyje chwilą;;Nudny| Face, Power, Hedonism > Achievement | DRIVE: Przejąć władzę nad arkologią) | @ 230201-wylaczone-generatory-memoriam-inferni
* "Ta arkologia musi należeć do mnie. Ile może jeszcze być ograniczana przez Rafała? On nie jest tylko 'szefem ochrony'..." | @ 230201-wylaczone-generatory-memoriam-inferni

### Wątki


infernia-jej-imieniem
kidiron-zbawca-nativis

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220723-polowanie-na-szczury-w-nativis | mściwy młody (16) Kidiron próbujący wygrać "eksterminację szczurów" z Lertysami a potem zwandalizować slumsowy dom Lertysów. Zmienił zdanie po tym jak został OSTRZELANY. | 0087-05-03 - 0087-05-12 |
| 230621-infiltrator-ucieka-a-arkologia-plonie | BYŁ WE WŁAŚCIWYM MIEJSCU WE WŁAŚCIWYM CZASIE by ratować Arkologię i ludzie wierzą, że to ON a nie Wujek robi robotę. Czas przejęcia Arkologii nadszedł! | 0093-03-25 - 0093-03-26 |
| 230628-wojna-o-arkologie-nativis-konsolidacja-sil | Polityczna propaganda i 'nowy lepszy Kidiron'. Przejął radiowęzeł by kontrolować sygnał. Szczuje na Infernię i na Rafała Kidirona. O dziwo, bardzo wiele ludzi mu ufa. | 0093-03-27 - 0093-03-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 2 | ((230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Bartłomiej Korkoran  | 2 | ((230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Celina Lertys        | 2 | ((220723-polowanie-na-szczury-w-nativis; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Eustachy Korkoran    | 2 | ((230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Karol Lertys         | 2 | ((220723-polowanie-na-szczury-w-nativis; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Ralf Tapszecz        | 2 | ((230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Tymon Korkoran       | 2 | ((220723-polowanie-na-szczury-w-nativis; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| BIA Prometeus        | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Dalmjer Servart      | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Emilia d'Erozja      | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Izabella Saviripatel | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Jan Lertys           | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Kalia Awiter         | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Małgorzata Maratelus | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Marcel Draglin       | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| OA Erozja Ego        | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| OO Infernia          | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Rafał Kidiron        | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Tobiasz Lobrak       | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |