---
categories: profile
factions: 
owner: public
title: Franek Bulterier
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200524-dom-dla-melindy              | jeden z gangu Rekinów z Podwiertu; szczególnie zaprzyjaźiony z Dianą. Gość chodzi w łańcuchach (ozdoba) i lubi się bić, hobbystycznie. | 0110-09-13 - 0110-09-16 |
| 200616-bardzo-straszna-mysz         | Rekin który chciał pomóc Dianie i nie pozwolić, by inne osoby z Aurum wpadły w kłopoty. Więc poszedł po linii arystokratycznej do Gabriela Ursusa. | 0110-09-18 - 0110-09-21 |
| 210406-potencjalnie-eksterytorialny-seksbot | wezwany przez Marysię by zdewastować szabrowników. Zeskoczył ze ścigacza i zdewastował szabrowników. Dokładnie to lubi i w ten sposób :-). | 0111-04-27 - 0111-04-28 |
| 210928-wysadzony-zywy-scigacz       | chciał się bić z Ernestem. Wie, kto wysadził ścigacz Ernesta (nie wiedział, że ów żyje). Przed śmiercią do simulacrum uratowała go Karolina, wchodząc z nim w strzelankę ścigaczami - rozbił się w lesie. | 0111-07-26 - 0111-07-27 |
| 230325-ten-nawiedzany-i-ta-ukryta   | Rekin latający na wielkim, głośnym, ciężkim ścigaczu (bardziej czołg). Zgodził się na wyścig z Mysiokornikiem, bo czemu nie? Wulgarny, powiedział Oli Burgacz wprost, że wszyscy 'chcą umoczyć'. Przez to, że Mysiokornik się rozbił a Daniel się zmył to Bulterier wygrał wyścig i Ola go pocałowała XD. Bulterier się nieźle bawił. | 0111-10-16 - 0111-10-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Henryk Wkrąż         | 2 | ((200616-bardzo-straszna-mysz; 210406-potencjalnie-eksterytorialny-seksbot)) |
| Karolina Terienak    | 2 | ((210928-wysadzony-zywy-scigacz; 230325-ten-nawiedzany-i-ta-ukryta)) |
| Laura Tesinik        | 2 | ((200524-dom-dla-melindy; 200616-bardzo-straszna-mysz)) |
| Marysia Sowińska     | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 210928-wysadzony-zywy-scigacz)) |
| Rupert Mysiokornik   | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 230325-ten-nawiedzany-i-ta-ukryta)) |
| Aleksandra Burgacz   | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Azalia Sernat d'Namertel | 1 | ((210928-wysadzony-zywy-scigacz)) |
| Barnaba Burgacz      | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Daniel Terienak      | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Diana Lauris         | 1 | ((200616-bardzo-straszna-mysz)) |
| Diana Lemurczak      | 1 | ((200524-dom-dla-melindy)) |
| Diana Tevalier       | 1 | ((200524-dom-dla-melindy)) |
| Ernest Namertel      | 1 | ((210928-wysadzony-zywy-scigacz)) |
| Feliks Keksik        | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Gabriel Ursus        | 1 | ((200616-bardzo-straszna-mysz)) |
| Gerwazy Lemurczak    | 1 | ((200524-dom-dla-melindy)) |
| Julia Kardolin       | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Keira Amarco d'Namertel | 1 | ((210928-wysadzony-zywy-scigacz)) |
| Ksenia Kirallen      | 1 | ((200616-bardzo-straszna-mysz)) |
| Lea Samszar          | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Lorena Gwozdnik      | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Mariusz Trzewń       | 1 | ((200524-dom-dla-melindy)) |
| Matylda Sęk          | 1 | ((200616-bardzo-straszna-mysz)) |
| Melinda Teilert      | 1 | ((200524-dom-dla-melindy)) |
| Michał Klabacz       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Nadia Uprewien       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Oliwier Czepek       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Pięknotka Diakon     | 1 | ((200524-dom-dla-melindy)) |
| Rafał Torszecki      | 1 | ((210928-wysadzony-zywy-scigacz)) |
| Sabina Kazitan       | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Triana Porzecznik    | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Żorż d'Namertel      | 1 | ((210928-wysadzony-zywy-scigacz)) |