---
categories: profile
factions: 
owner: public
title: Halina Sermniek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200502-po-co-atakuja-minerwe        | szefowa Technożerców (do teraz). Czarodziejka błyskawic. Pomaga ludziom którzy są wypierani przez TAI z rynku pracy, ale też sabotuje Infiltratorem dla sponsora. Walnęła artefaktyczną błyskawicą i uszkodziła power suit Pięknotki. | 0110-08-26 - 0110-08-28 |
| 210831-serafina-staje-za-wydrami    | laska która NIE przespała się z Danielem, ale miała karteczkę, że "Wydry polują na Rekiny" w notatkach. "Rewolucjonistka". Źródło nieaktualnych informacji o Wydrach po tym, jak Zespół się do niej włamał (i nikt jej nie pomógł / nie wezwał terminusów). | 0111-07-07 - 0111-07-10 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 200502-po-co-atakuja-minerwe        | oddaje przywództwo Technożerców, nie jest w stanie dalej nimi dowodzić - nie po zastraszeniu przez Pięknotkę | 0110-08-28
| 200502-po-co-atakuja-minerwe        | rezygnuje z kontaktów z Aurum; uważa, że to nie jest bezpieczne dla niej osobiście. | 0110-08-28
| 200502-po-co-atakuja-minerwe        | święcie przekonana, że terminusi z Pustogoru to okrutne potwory | 0110-08-28

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Daniel Terienak      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Feliks Mirtan        | 1 | ((200502-po-co-atakuja-minerwe)) |
| Karolina Terienak    | 1 | ((210831-serafina-staje-za-wydrami)) |
| Kreacjusz Diakon     | 1 | ((200502-po-co-atakuja-minerwe)) |
| Laura Tesinik        | 1 | ((210831-serafina-staje-za-wydrami)) |
| Lorena Gwozdnik      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Marysia Sowińska     | 1 | ((210831-serafina-staje-za-wydrami)) |
| Minerwa Metalia      | 1 | ((200502-po-co-atakuja-minerwe)) |
| Nikodem Larwent      | 1 | ((200502-po-co-atakuja-minerwe)) |
| Pięknotka Diakon     | 1 | ((200502-po-co-atakuja-minerwe)) |
| Rafał Torszecki      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Serafina Ira         | 1 | ((210831-serafina-staje-za-wydrami)) |