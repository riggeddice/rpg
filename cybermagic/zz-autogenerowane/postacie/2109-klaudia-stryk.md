---
categories: profile
factions: 
owner: kić
title: Klaudia Stryk
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "Infernia"
* owner: "kić"
* title: "Klaudia Stryk"

## Kim jest

### W kilku zdaniach

Walcząca o wolność dla AI badaczka anomalii, która wierzy, że świat może być lepszy niż jest, jeśli tylko damy AI być częścią społeczeństwa.
Najczęściej przy konsoli komunikacyjnej lub w laboratorium. Zwykle mówi prawdę (choć niekoniecznie całą). 

### Co się rzuca w oczy

* Zwykle nie rzuca się w oczy. Siedzi cicho przy swojej konsoli i pracuje za kulisami.

### Jak sterować postacią

* Klaudia wierzy w swoich przełożonych. Zakłada dobre intencje i raczej da szansę, ale nie wierzy ślepo.
* Często działa zza kulisów. Ustawia sytuacje, zmienia zapisy, sprawia, że ludzie się "przypadkowo" spotkają...
* Klaudia nie kłamie jeśli może tego uniknąć (nie umie), ale bez najmniejszego zawahania pogrzebie prawdę w biurokracji.
* Klaudia uwielbia używać systemów przeciwko ich twórcom (jeśli tylko ma powód)
* Klaudia nie jest szczególnie silna społecznie i można ją łatwo oszukać. Jeśli jednak zdradzisz jej zaufanie, bardzo trudno będzie je odzyskać. Najpewniej podejmie kroki przeciwko tobie.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Ocaliła i ukryła świadome AI gdzieś na planecie (magitrownia?)	
* Gdy była młoda, zwerbował ją stary Grzymość - nie poszła tam dla pieniędzy, ale dla zasad. Była zniechęcona sztywnymi zasadami Pustogoru i tym, że nie mogła realizować swoich celów. Grzymość pomagał Noktianom i Thalii i naprawdę robił to, co mówił.
* Wraz z Martynem uciekli z łap młodego Grzymościa. On jest medykiem, ona naukowcem - to idealny dla Grzymościa zespół. Ani Klaudia ani Martyn początkowo nie znali całej sytuacji. Klaudia to wygrzebała, Martyn to zrozumiał i postanowili uciec bo nie zgadzają się z metodami a wiedzą za dużo by móc odejść. Klaudia sfałszowała rekordy i nasłała Cieniaszczyt na laboratorium, a Martyn sfabrykował ich śmierć.
* Mag z administracji admirał Termii utrudniał życie Elenie, bo nie chciała z nim iść do łóżka. Klaudia się o tym dowiedziała i zadbała o to, by ten mag już nigdy nic nie wydrukował na K1 bez wybitnych problemów.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: Przeszukiwanie i modyfikacja rekordów biurokratycznych. Królowa biurokracji.
* AKCJA: Specjalistka od komunikacji. Przenanalizuje sygnał, określi jego pochodzenie, jak trzeba zdekoduje. Zarówno real jak i virt.
* AKCJA: Przełamuje zabezpieczenia, hakuje, ukrywa i odkrywa prawdę.
* AKCJA: Jak mało kto potrafi rozmawiać z AI. Szkolona przez Thalię, z długą praktyką.
* KTOŚ: Sporo kontaktów w vircie, zwykle wśród istot z forpoczty nauki. Tych wyklętych też.
* COŚ: Zna Noktiański. Jest to dla niej drugi język (bo BIA, bo Thalia)
* COŚ: Paleta anomalii dziwnych i niestabilnych. Takich, których nikt inny nie odważyłby się użyć, a ona jakoś je kontroluje.
* COŚ: Paleta eksperymentalnych magitechów. Na to idzie jej kasa.
* COŚ: Reputacja wśród AI (ale ona jest jej nieświadoma)

### Serce i Wartości (3)

* Uniwersalizm
  * Skupia się na uwolnieniu TAI. Chce świata, gdzie wszyscy mogą działać na równi. Nieważne, czym są.
  * Klaudia dąży do tego, aby wszystkie frakcje współpracowały i się wspierały. Wierzy, że to jedyna droga do przetrwania i zwycięstwa.
  * Stanie za swoimi przekonaniami, nawet wbrew przyjaciołom, nawet jeśli miałoby to oznaczać, że skończy w więzieniu.
* Osiągnięcia
  * Mistrzyni anomalii. Anomalie służą polepszeniu rzeczywistości. Anomalie z czasem stają się technologią.
* Stymulacja
  * Musi być na forpoczcie nauki. Jest na froncie.
  * Ucieczka do przodu. Ta rzeczywistość jest wspaniała, ale musimy ją ulepszyć. Nadal potrzebujemy niewolnictwa (AI i nie tylko), to musi się zmienić
  * Nowe, ciekawe, nieznane, eksperymantalne. To jest to, co ją kręci.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Nie mam domu. Nigdzie nie ma dla mnie powrotu. Straciłam przyjaciółkę, mentorkę, planetę. Poruszam się w ciszy, więc nikt mnie nie zapamięta."
* CORE LIE: "Jedyny sposób, bym dostała choć przypis w historii to osiągnąć coś wielkiego. Zrobić coś dużego. Uwolnić AI, sprowadzić Zefiris, znaleźć Percivala Diakona..."
* AKCJA: Fatalnie kłamie. 
* CECHA: Słaba empatia. Ma problemy z czytaniem ludzi.
* AKCJA: Walka bezpośrednia
  * Klaudia przeszła podstawowe przeszkolenie, żeby móc dostać swój stopień, ale to nie znaczy, że umie walczyć.
* STRACH: Ominie ją coś nowego, ważnego. Jej umiejętności zmarnują się gdzieś w jakiejś nieważnej placówce lub ze świniami

### Magia (3M)

#### W czym jest świetna

* Sterowanie anomaliami i magitechami. Zwykle wkłada małą moc by osiągnąć zamierzony efekt.
* Rozbrajanie anomalii, czyszczenie energii magicznych.
* Magia komunikacyjna, w szczególności virt. "Informatyka" rzeczywistości.

#### Jak się objawia utrata kontroli

* Tworzenie anomalii magicznych
* Przemieszanie kanałów komunikacji. Pełna synestezja dla wszystkich pod wpływem.

### Specjalne

* .

## Inne

### Wygląd

* Zwykle ubrana w biały fartuch z mnóstwem kieszeni lub coś zbliżonego do tego. 
* Długie włosy zebrane w ciasny kok. 
* Lekko nieobecne spojrzenie.

### Coś Więcej



### Endgame






# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211122-czolenkowe-esuriit-w-amz     | 16 lat; podrywana przez Trzewnia (czego nie zauważyła). Nie chciała zdradzić przed Szurmakiem Ksenii - wolała pasy na goły tyłek. Próbowała opanować wykwit Esuriit zanim Szurmak ją zastąpił używając tego co widziała jak Szurmak to robił. Koordynowała ewakuację AMZ na Arenę. Podchwyciła plan Felicjana i połączyła pomysł Ksenii (niewidoczność przed magią) z beaconem. Wyjątkowo odpowiedzialna, zdaniem wszystkich. WAŻNE - Szurmak nie uczył ich jak containować takie energie, ale pokazał jak on to robi raz. Zreplikowała to. Kiepsko, ale pomogła. | 0083-10-13 - 0083-10-22 |
| 210926-nowa-strazniczka-amz         | 17 lat; biurokratka maksymalna. Grzeczna dziewczynka, która po wojnie koordynuje patrole, logistykę itp w imieniu AMZ. Przypadkiem odkryła konspirację dyrektora, terminusa i noktianki by uratować TAI klasy Eszara w AMZ. Skonfrontowała się z dyrektorem i zdecydowała się pomóc. | 0084-06-14 - 0084-06-26 |
| 211009-szukaj-serpentisa-w-lesie    | 17 lat; uruchomiła medbunkier, wysyłała wiadomości do serpentisów łagodzące sytuację (łamanym noktiańskim); wyprowadziła dronę do Zaczęstwa i zajmowała się intelligence-and-control sytuacji z perspektywy sygnałów itp. | 0084-11-13 - 0084-11-14 |
| 211010-ukryta-wychowanka-arnulfa    | 17 lat; w stresie próbowała przekroczyć granicę tego co teoretycznie możliwe i używając mocy Trzęsawiska zanomalizowała generatory Strażniczki, wyłączając jej systemy. Odkryła, że Teresa Mieralit jest noktianką. | 0084-12-11 - 0084-12-12 |
| 211017-nastolatka-w-bieliznie-na-dachu-w-burzy | wymanewrowała Saszę - wpierw ukryła przed nim Teresę, potem spowolniła jego działania przeciw Tymonowi i odwróciła uwagę od Teresy. Zmusiła Teresę do jakiejś socjalizacji i poprosiła Ksenię, by ta jej w tym pomogła. | 0084-12-14 - 0084-12-15 |
| 211019-czarodziejka-ktora-jednak-moze-sie-zabic | przekonała Arnulfa, by zasilił kadrę AMZ weteranami; wzięła za dużo na siebie i gdyby nie Trzewń, wypaliłaby się. Socjalizuje Teresę. Wykryła, że Trzewń grzebał po systemach. | 0084-12-20 - 0084-12-24 |
| 240114-o-seksbotach-i-syntetycznych-intelektach | współpracuje z Talią (z ramienia AMZ) nad konstrukcją TAI; zrozumiała co się dzieje gdy AI Core gaśnie czy że Syntetyczne Intelekty żyją. Zrozumiała jak Talia umieszcza TAI militarne w seksbotach i daje im 'nowe życie'. Ucząc się od Talii, osłoniła ją przed dziwnym sabotażystą i terminusem który na Talię polował. | 0085-01-13 - 0085-01-18 |
| 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie | Pokazała Teresie na czym polega ryzyko sytuacji (z porywaniem) i przekonała ją do współpracy mimo wyraźnego strachu Teresy. Pracując z policyjnymi bazami danych odkryła, że faktycznie ktoś porywa ludzi w okolicy. Zaprezentowała Plan dyrektorowi i zaakceptowała modyfikację. | 0085-01-26 - 0085-01-28 |
| 220119-sekret-samanty-arienik       | Ustabilizowała Samantę i dowiozła ją do Talii. Wyczyściła Ksenię po tym jak ta zaaplikowała magię bio na byt syntetyczny. Spowolniła autorepair psychotroniki Samanty, by ta nie wróciła do niewiedzy - by dotrzeć do Talii. | 0085-07-21 - 0085-07-23 |
| 220716-chory-piesek-na-statku-luxuritias | przypisana do OO Serbinius; z Martynem doszła do tego że Rajasee Bagh jest zakażony Śmiechowicą. Przekonała szefa ochrony że musi współpracować i magią zwizualizowała grzybka. Odbudowała logi - skąd się wzięła Śmiechowica. | 0109-05-05 - 0109-05-07 |
| 220925-mlodziaki-na-savaranskim-statku-handlowym | spotkała się z savarańską jednostką cywilną neonoktiańską i unikając Martyna rozwiązała problem; doszła do tego, że ta jednostka jest dziwna ale nie jest groźna i dogadała się z kapitanem 'Hektora 13' - on po prostu chce odbudować swój dom. | 0109-05-24 - 0109-05-25 |
| 221015-duch-na-pancernej-jaszczurce | zidentyfikowała zachowanie anomalii na Pancernej Jaszczurce i metodami naukowymi określiła kiedy COŚ się dzieje i CO się dzieje; rekomendacja - jednostka ma wymienić silnik. | 0109-08-24 - 0109-08-29 |
| 230521-rozszczepiona-persefona-na-itorwienie | zbadała szybko profil załogi Itorwien. Gdy dostała killware od Mojry, skutecznie ów killware odbiła i wszystkich uwięziła w pancerzach. Jako, że Lancer Martyna był uszkodzony, przekierowała feed z jej systemów badawczych do Martyna by mógł zbadać co tu się stało. | 0109-09-15 - 0109-09-17 |
| 230528-helmut-i-nieoczekiwana-awaria-lancera | na podstawie dużej ilości drobnych awarii zidentyfikowała, że dany obszar kosmosu jest niebezpieczny. Znalazła dowody na Anomalię Statystyczną i przekazała je teoretykowi Orbitera, Nikodemowi. Wspólnie zamapowali Anomalię i wyciągnęli Helmuta z potencjalnych problemów z Mirandą. | 0109-09-23 - 0109-09-26 |
| 230530-ziarno-kuratorow-na-karnaxianie | paranoiczna odkąd wykryła Karnaxian; postawiła sandbox na Persefonie i tylko dzięki temu odparła atak psychotroniczny Kuratora Sarkamaira; wykryła obecność Kuratorów i próbowała go zablokować by móc uratować załogę. Niestety, Kurator był za silny i magia Klaudii tylko go wzmocniła. Klaudia szczęśliwie uratowała Serbiniusa (wysadzając Ziarno Kuratora), ale musieli zniszczyć Karnaxiana. | 0109-10-06 - 0109-10-07 |
| 240102-zaloga-vishaera-przezyje     | MVP. Odkryła naturę Skażenia Talikazera i AK Vishaer, wykorzystała Pryzmat by AK Vishaer pomogła Klaudii w ratunku ludzi, zainspirowała Fabiana do uratowania Talikazera i weszła z Martynem na Vishaer by ręcznie ratować kogo jeszcze się da. | 0109-10-26 - 0109-10-28 |
| 240204-skazona-symulacja-tabester   | odzyskawszy pamięć i zrozumienie sytuacji, Klaudia skutecznie manipuluje symulacją, przeciążając TAI Ashtarię d'Tabester, co prowadzi do jej wyłączenia i uwolnienia ludzi z symulacji. Jej działania umożliwiły bezpieczne odłączenie uczestników gry od skażonej TAI, minimalizując trwałe szkody psychiczne. | 0109-11-01 - 0109-11-04 |
| 200106-infernia-martyn-hiwasser     | naukowiec Orbitera pod dowództwem Arianny Verlen; tym razem przerzucała ogromne ilości dokumentów, papierów i biurokracji by tylko znaleźć Hiwassera. | 0110-06-28 - 0110-07-03 |
| 200115-pech-komodora-ogryza         | w roli manipulatorki i biurokratki; ma doskonałe pomysły i wpakowuje Ogryza w coraz większe tarapaty podkręcając spiralę nienawiści na Kontrolerze Pierwszym. | 0110-07-20 - 0110-07-23 |
| 200122-kiepski-altruistyczny-terroryzm | badacz otchłani (wreszcie!); przeprowadziła dwa chore rytuały, linkując Natalię z Persefoną i integrując umierającą Persefonę z Martauronem. | 0110-09-15 - 0110-09-17 |
| 200408-o-psach-i-krysztalach        | prawie oddała życie by chronić Infernię; doszła do badań Elizy Iry odnośnie anomalii krystalicznej odbierającej pamięć. | 0110-09-29 - 0110-10-04 |
| 200415-sabotaz-miecza-swiatla       | doskonale manipulowała danymi Inferni by przekonać Persefonę jak groźny jest Serenit; potem odwracała uwagę Persefony pracując nad sygnałem z Serenita. | 0110-10-08 - 0110-10-10 |
| 200624-ratujmy-castigator           | rozprzestrzeniła filmik z Leoną by Castigator "wziął się w garść" terrorem; potem dała radę odkryć atak maskującego się Alkarisa anomalizując sensory Castigatora. | 0110-10-15 - 0110-10-19 |
| 231025-spiew-nielalki-na-castigatorze | diagnozując Eszarę dochodzi do Altarient; wspierając się danymi z Inferni izoluje lokalizację Altarient na Castigatorze, buduje odpowiednie pudełko do izolacji NieLalki Altarient i z Arianną sterują Raoulem, by jemu udało się prawidłowo rozwiązać problem. A potem -  biurokracja i dokumenty, by nikt nie wiedział że coś się tam naprawdę stało. | 0110-10-24 - 0110-10-26 |
| 231109-komodor-bladawir-i-korona-woltaren | skanując dane 'Korony' odkryła, że ta ma jakieś 3 lata życia jak nic się nie zmieni. Znalazła hazardy godne 3-krotnej grzywny (co zrobiła z rozkazu Arianny) ale też (z rozkazu Arianny) przeszła biurokracją by oni mogli uciec przed grzywną. W samej akcji z terrorformem użyła Anomalii alteris, rozpychając rzeczywistość (i poważnie uszkadzając Koronę). Gdy Wojciecha dotknął nanofar i zaczął ixiońską infekcję, nie zawahała się - ucięła mu rękę, ratując mu życie. | 0110-10-27 - 0110-11-03 |
| 231115-cwiczenia-komodora-bladawira | na podstawie danych zebranych przez Dorotę doszła do tego jaki jest PRAWDZIWY plan Bladawira. Znalazła kreta. Doszła też, że Bladawir nie ma szans na sensowną insercję i że on o tym wie. Dzięki niej Arianna miała możliwość prawidłowego działania. | 0110-11-08 - 0110-11-15 |
| 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira | jak najszybciej dostała info o Zaarze (kto to jest) i jak tylko dowiedziała się o ćwiczeniach, paranoicznie sprawdziła dane Paprykowca i dowiedziała się o _pain amplifiers_. Prawidłowo opracowała manewrowanie trzech jednostek przez pole minowe i jak użyć wody z Inferni jako miotacz frag unieszkodliwiający miny. Okazuje się, że jest dobrym oficerem taktycznym, nie tylko w kontekście psychotronicznym. | 0110-11-26 - 0110-11-30 |
| 231220-bladawir-kontra-przemyt-tienow | Nie zaakceptowała czystej sygnatury energii od Bladawira; doszła do jej znaczenia i do przemytu tienów z Aurum. Korzystając z danych K1 określiła wzory przemytu na Karsztarinie. Odkryła plan Bladawira zawierający potwora na Karsztarinie i dyskretnie przekazała wiadomość do Ewy Razalis używając przelotu przy Karsztarinie. Architektka porażki Bladawira. Pewnych rzeczy przy Klaudii się nie robi. | 0110-12-06 - 0110-12-11 |
| 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow | przeszukując niezliczone teczki Bladawira znalazła anomalię - potwora polującego na mało ważnych ludzi. Nie zdradziła co wie Infernia o ruchach Bladawira. Postawiła się Bladawirowi - nie będzie z nim pracować. Jawnie przy nim. Wypunktowując jego zbrodnie. De facto - Klaudia skłoniła Ariannę do zranienia Bladawira. | 0110-12-03 - 0110-12-17 |
| 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze | poproszona o zbadanie fenomenu ciężkiej pracy i spolegliwości na Castigatorze, wzięła Martyna i odkryła feromon Ambicji. Dotarła do substratu - narkotyków kralotycznych - idę tylko kto najpewniej za tym stoi – XO Kurzmina na Castigatorze. Zarekomendowała pozostawienie tam Martyna by to cicho wygasić i nie było efektów ubocznych. | 0110-12-19 - 0110-12-21 |
| 240110-wieczna-wojna-bladawira      | zapewniła Władawca na Infernię, skupia się na dekoncentracji Bladawira (by nie patrzył na Infernię), znajduje ciekawe rzeczy o Erneście Bankierzu i zupełnie nie jest w stanie pracować z Zaarą, nawet jak ta się stara. Niekompatybilność mentalna. Zrobiła perfekcyjne papiery, by Bladawir miał wejście na Skażoną kralotycznie jednostkę SC Wirmerin. | 0110-12-24 - 0110-12-27 |
| 200708-problematyczna-elena         | rozpaczliwie znalazła sensory (anomalne) jakie pasują do Inferni a potem wykazała się nadludzkimi umiejętnościami kontrolując i wypalając błędy w Persefonie. Magią znalazła Salamin. | 0110-12-27 - 0111-01-01 |
| 240124-mikiptur-zemsta-woltaren     | wykorzystała psychotronikę i magię do neutralizacji zagrożenia ze strony TAI Isratazir; niestety, anomalia Inferni się przebudziła przy okazji. Odkryła i ujawniła rolę Syndykatu w ataku Mikiptur. | 0110-12-29 - 0111-01-01 |
| 200715-sabotaz-netrahiny            | odkryła tajemnicze sygnały z Anomalii Kolapsu i stanęła po stronie uwolnionej Persefony. Mistrzowsko zrozumiała problem na Netrahinie. | 0111-01-04 - 0111-01-07 |
| 240131-anomalna-mavidiz             | dostała dostęp do Tivru i dowiedziała się kim jest Tara Ogniczek; analizując anomalie na Mavidiz doszła do tego z czym walczą, mniej więcej. Opracowała jak użyć Alteris by odciąć część statku i zrobić sanktuarium. | 0111-01-03 - 0111-01-08 |
| 200729-nocna-krypta-i-emulatorka    | rozwiązywała astralne konstrukty Krypty oraz badała ghule Esuriit. Ona pierwsza doszła do tego, na czym polegał problem Krypty. | 0111-01-14 - 0111-01-20 |
| 200819-sekrety-orbitera-historia-prawdziwa | wkręciła Tadeusza w pojedynek z Leoną o Elenę, bo chciała dowiedzieć się jak walczy arcymag Eterni. Potem mnóstwo szukania, fałszywych śladów i dowodów by zamaskować prawdę przed Tadeuszem i znaleźć jak Leona ma z Tadeuszem wygrać. | 0111-01-24 - 0111-02-01 |
| 200822-gdy-arianna-nie-patrzy       | dotarła do intrygi podwładnego Tadeusza który chciał go Upaść przez Esuriit. Na kilkanaście sposobów wkręciła Ariannę, że to jej wina XD. | 0111-02-02 - 0111-02-04 |
| 200826-nienawisc-do-swin            | gdy dowiedziała się, że Arianna będzie wozić świnie (i glukszwajny), zrobiła WSZYSTKO by do tego nie doszło. "WSZYSTKO" skończyło się buntem. Ups. | 0111-02-05 - 0111-02-11 |
| 200909-arystokratka-w-ladowni-na-swinie | mastermind planu ratunkowego Anastazji. Ostrożniejsza niż się wydaje - nie weszła w ciemno w arcymagiczną anomalię. Użyła anomalicznej gąbki do wyczyszczenia Skażenia z ładowni. Dodatkowo, biurokratycznie uciemiężyła sąd Kontrolera Pierwszego by wydobyć Martyna z kicia. | 0111-02-12 - 0111-02-17 |
| 200916-smierc-raju                  | kontroluje pola siłowe by odpowiednio kierować nojrepami; dodatkowo, porównuje działanie Ataienne z tym czego by się spodziewała. Widzi, że Ataienne jest silna i udaje słabą. | 0111-02-18 - 0111-02-20 |
| 200923-magiczna-burza-w-raju        | używa virtu i kontaktów do rozesłania opowieści Izabeli i prośby o pomoc wszędzie gdzie się da; stabilizuje świniami i osłonami Raj podczas burzy magicznej. | 0111-02-21 - 0111-02-25 |
| 201014-krystaliczny-gniew-elizy     | wpierw badała kryształowe bomby "Elizy", potem uspokajała i stabilizowała Anastazję (gdy ta panikowała) i skończyła na przekierowaniu rakiety gdzieś na pole. | 0111-03-02 - 0111-03-05 |
| 201021-noktianie-rodu-arlacz        | shackowała systemy Rezydencji Arłacz z pomocą Diany, odkryła sekrety rodu Arłacz i uwolniła noktian. | 0111-03-07 - 0111-03-10 |
| 201104-sabotaz-swini                | zanomalizowała TAI Semlę na Wesołym Wieprzku, co doprowadziło do Skażenia tej jednostki; dzięki badaniu wybuchowej świni udało jej się namierzyć miragenta. | 0111-03-13 - 0111-03-16 |
| 201118-anastazja-bohaterka          | w warunkach ekstremalnych jej paranoja pozwoliła na wykrycie statku koloidowego - coś, czego nikt inny by nie zrobił. | 0111-03-22 - 0111-03-25 |
| 201210-pocalunek-aspirii            | oficer komunikacyjny; by przekonać Anastazję do postawienia się Juliuszowi pokazała na monitorze Elenę pod lifesupportem. Manipuluje Anastazją. | 0111-03-26 - 0111-03-29 |
| 201216-krypta-i-wyjec               | infiltracja Pocałunku Aspirii i odkrycie prawdy o tym, czym są Anioły Krypty. Plus seria pomniejszych działań anomalicznych na pokładzie Krypty. | 0111-03-29 - 0111-03-31 |
| 201125-wyscig-jako-randka           | odkryła na Kontrolerze Pierwszym starą trasę wyścigów po czym ją dostosowała do potrzeb pojedynku Olgierda i Arianny. Plus, Skaziła K1 anomalią. | 0111-04-08 - 0111-04-13 |
| 201224-nieprawdopodobny-zbieg-okolicznosci | uratowała chłopca, Jasminę, włączyła Dianę i Elenę do działania - i jej krótkie poszukiwania potwierdziły, że na K1 jest "duch opiekuńczy". | 0111-04-15 - 0111-04-16 |
| 200610-ixiacka-wersja-malictrix     | znalazła rozproszoną Malictrix w robotach, skanowała sygnały i udowodniła transfer ixiacki między ludźmi a Malictrix. Doszła też do tego, że Mal chce uciec. | 0111-05-01 - 0111-05-05 |
| 210707-po-drugiej-stronie-bramy     | doszło do katastrofy przy przejściu przez Bramę Eteryczną. Coś zrobiła i zniknęła. Jej stan nieznany, ale coś z anomaliami i dziwną bazą noktiańską. | 0111-05-11 - 0111-05-13 |
| 210721-pierwsza-bia-mag             | przedmiot, nie podmiot; rozdzielona z Bii Solitarii d'Zona Tres przez Eustachego i Martyna. W fatalnym stanie, w stazie, ale żywa. | 0111-05-17 - 0111-05-19 |
| 210804-infernia-jest-nasza          | wpierw stworzyła raport pokazujący Admiralicji że Infernia robi inne akcje i jest droga ale superskuteczna... a potem zemściła się za Infernię robiąc komodorowi Traffalowi strajk włoski ze strony TAI na K1. | 0111-06-21 - 0111-06-24 |
| 201111-mychainee-na-netrahinie      | odkryła Mychainee na Netrahinie badając Komczirpa; na samej Netrahinie zsyntetyzowała z Martynem anty-środek na ten wariant Mychainee. | 0111-07-05 - 0111-07-08 |
| 201230-pulapka-z-anastazji          | wykryła, że Rodivas nie jest już SCA a AK. Określiła gdzie Eustachy ma strzelać by odstrzelić biovat. Dodatkowo - pomagała Ariannie bullshitować Henryka Sowińskiego. | 0111-07-19 - 0111-07-20 |
| 210127-porwanie-anastazji-z-odkupienia | stworzyła z Dianą plugawą anomalię odwracającą pretorian a potem objęła dowodzenie Infernią, by uratować Ariannę, Eustachego i Anastazję bis z Odkupienia. | 0111-07-22 - 0111-07-23 |
| 210106-sos-z-haremu                 | wykryła w wiadomości Julii Aktenir faktyczne elementy zagrożenia i znalazła wstępną pulę arystokratek na orgię dla Diany/Arianny. | 0111-07-26 - 0111-07-27 |
| 210120-sympozjum-zniszczenia        | odkryła, że dziwny materiał z Anomalii Kolapsu wymaga Esuriit do działania; też, sformowała z Eustachym Emiter Plagi Nienawiści, samoładujący się. | 0111-08-01 - 0111-08-05 |
| 210108-ratunkowa-misja-goldariona   | znalazła ArcheoPrzędz a potem wymusiła z nich informacje; magicznie zasklepiła rozpadający się Goldarion a potem wymusiła na załodze Goldariona wyczyszczenie narkotyków. | 0111-08-09 - 0111-08-15 |
| 210209-wolna-tai-na-k1              | idąc za tematem AI Lorda odkryła wolne TAI, virtki. Odkryła też Rziezę - system odporności K1. Ewakuowała jedną niegroźną wolną TAI przy pomocy Martyna. Sporo korelowała biurokracją. | 0111-08-30 - 0111-09-04 |
| 210218-infernia-jako-goldarion      | repozytorium wiedzy na temat TAI, doszła do tego jak działa Morrigan i że mogą mieć przeciw sobie servara sterującego nanitkami. | 0111-09-16 - 0111-10-01 |
| 200429-porwanie-cywila-z-kokitii    | dywersja dokumentowa wprowadziła Ariannę na pokład Niobe. Potem użycie Eteru w Nierzeczywistości skołowało Kokitię i przeciążyło jej tarcze taumiczne. | 0111-10-01 - 0111-10-06 |
| 210317-arianna-podbija-asimear      | złożyła i wysłała do "Płetwala Błękitnego" Bazyliszka, praktycznie niszcząc Semlę. Potem pomagała Martynowi w naprawieniu Joli Sowińskiej (usunięciu kralotycznego pasożyta i przywróceniu TAI) | 0111-10-18 - 0111-11-02 |
| 210414-dekralotyzacja-asimear       | z Martynem złożyła alergizator kralotha na podstawie próbek Jolanty, znalazła w komputerach Lazarin brak advancera a potem z Eustachym pracowała logistycznie nad zapewnieniem, że kraloth zostanie odparty. | 0111-11-03 - 0111-11-12 |
| 210421-znudzona-zaloga-inferni      | wykryła to, że Infernia anomalizuje i walczyła z Morrigan w vircie - przesuwając ją po systemach i zamykając w Rozrywce. Uszkodziła virtsferę Inferni, ale ograniczyła moc Morrigan. | 0111-11-16 - 0111-11-19 |
| 210428-infekcja-serenit             | obliczyła gdzie powinien być Falołamacz (był tam), próbowała zatrzymać Morrigan (nie wyszło), po czym zrobiła z Eleną kosmiczny spacer i wykryła że walczą z Odłamkiem Serenit... | 0111-11-22 - 0111-11-23 |
| 210512-ewakuacja-z-serenit          | zrobiła najsmaczniejszą przynętę na Serenit ever (kupując czas), po czym z Arianną przeszczepiły sentisieć z Eidolona do Entropika, by Elena mogła nim zdalnie kierować. | 0111-11-23 - 0111-12-02 |
| 210519-osiemnascie-oczu             | przejmuje kontrolę nad komputerami, pracuje z notatkami - składa informacje o infohazardzie z jakim mają do czynienia jako pierwsza pochodna, by nie wiedzieć z czym ma do czynienia. Amnestyki... | 0111-12-07 - 0111-12-18 |
| 231018-anomalne-awarie-athamarein   | Athamarein okazała się być dla niej wyzwaniem. Prawidłowo naprawiana, znane logi, ale ANOMALNIE dobrze sobie radzi i nie ma strat. Ale czemu? Ustawia TAI, zmienia załogę, projektuje testy. Nie wpadła na 'serce korwety', bo to brzmi jak anomalia blokowana przez Memoriam, ale Athamarein faktycznie odbiła się w Eterze. Pierwszy raz Klaudia jest zadowolona z tego, że źle zinterpretowała dane. | 0111-12-22 - 0111-12-27 |
| 210526-morderstwo-na-inferni        | szukała Tala Marczaka. Używając kodów Arianny, wiedzy Arianny i Eustachego i dużej ilości cross-sekcji znalazła winnych jego morderstwa. Mistrzostwo w agregowaniu danych i dokumentów. | 0111-12-31 - 0112-01-06 |
| 210630-listy-od-fanow               | gdy standardowe szukanie po systemach K1 zawiodło, udała się w virtsferę i znalazła utylitarne TAI od komunikacji które wskazało gdzie może być baza Eterni na K1. Potem napuściła na bazę... Rziezę. | 0112-01-15 - 0112-01-18 |
| 210818-siostrzenica-morlana         | udowodniła, że Jolanta, Tomasz, Ofelia faktycznie ratują Eternian przed Morlanem. Potem odblokowała TAI Netrahiny i znalazła koloidowy statek dla Olgierda do zestrzelenia. | 0112-01-20 - 0112-01-24 |
| 210616-nieudana-infiltracja-inferni | odkryła tożsamość Flawii (ukrytej pod zmienioną formą) na podstawie wiedzy z dokumentów Orbitera i pamięci Eleny. Zaproponowała by Flawia dołączyła do Inferni. | 0112-01-27 - 0112-02-01 |
| 210825-uszkodzona-brama-eteryczna   | używając systemów skanujących Inferni i dron odkryła których ludzi z wraków da się jeszcze uratować - na przestrzeni wszystkiego dookoła Bramy. | 0112-02-04 - 0112-02-07 |
| 210901-stabilizacja-bramy-eterycznej | przy współpracy z Medeą zdobyła SPORO zasobów biurokracją; używając Bramy, Janusa i kodów zlokalizowała ON Catarina i skłoniła Catarinę do zrobienia błędu. Acz było Klaudii żal śmierci BII. | 0112-02-09 - 0112-02-12 |
| 211114-admiral-gruby-wieprz         | Skaziła "Serenitem" Połączony Rdzeń K1, po czym zmontowała Adalbertem i Izą + Romanem plan na ewakuację virtsystemu. Przeprowadziła go mimo Rziezy i Grubego Wieprza. | 0112-02-15 - 0112-02-20 |
| 210922-ostatnia-akcja-bohaterki     | na prośbę Martyna zrobiła raport dla Kramera przekonywujący go do autoryzacji akcji. To bardzo dobry raport jest. | 0112-02-23 - 0112-03-09 |
| 210929-grupa-ekspedycyjna-kellert   | analizując dane z jednostek zwiadowczych na K1 odkryła, że przeciwnik wpływa na TAI. Pasywnie czujnikami znalazła Saverę w sektorze Mevilig. Zapewniła psychotronika Adama na pewien czas na Infernię. | 0112-03-13 - 0112-03-16 |
| 211013-szara-nawalnica              | zrozumiała na czym polega Pirania i TAI w Mevilig, ale skonfliktowała się z Rziezą. Teraz musi go przechytrzyć... | 0112-03-18 - 0112-03-23 |
| 211020-kurczakownia                 | opracowała z Martynem jak zrobić kurczakowanie-rekurczakowanie na poziomie bioskładników. Opracowała, jak powstrzymać Zbawiciela-Niszczyciela jakby ten zamanifestował się w Sektorze Astoriańskim. Uniemożliwiła mu samoistne pojawienie się w tamtym sektorze. | 0112-03-24 - 0112-03-26 |
| 211027-rzieza-niszczy-infernie      | most notable action - surfowanie po blacie stołu by zapobiec wyssaniu załogantów Inferni w kosmos. Opracowała też plan jak uratować Infernię od Esuriit - niestety, Martyn nie dał rady go wykonać. Współpracowała ze Rziezą by uratować kogo się da i by Rzieza miał pretekst do nie zabijania Inferni. | 0112-03-28 - 0112-04-02 |
| 211110-romans-dzieki-esuriit        | Napisała serię listów o śmierci załogantów Inferni by Arianna nie musiała. Potem zorganizowała gry wojenne, by ukryć fakt, że Elena zniknęła w czeluściach K1. Poinformowała Marię o sytuacji z Martynem (bo była dead man's hand Martyna - jakby coś mu było, Klaudia ma powiedzieć Marii). Aha, znalazła Innego Medyka Na Infernię. | 0112-04-13 - 0112-04-14 |
| 211117-porwany-trismegistos         | udała przed Kalirą d'Trismegistos, że do niej zbliża się Serenit. Zmusiła TAI do negocjacji a potem ukryła że Tivr nie zniszczył Trismegistosa. Aha, aresztowali ją. | 0112-04-15 - 0112-04-17 |
| 211124-prototypowa-nereida-natalii  | wybrała z lejka rekrutacyjnego WŁAŚCIWYCH przyszłych członków załogi, efektywnie odbudowując Infernię. Uwolniona z zarzutów przez Adama Szarjana, próbowała uratować Natalię Aradin przed śmiercią i niestety jedyne co mogła zrobić to ixioński kokon - Natalia się wykluje jako "coś". | 0112-04-20 - 0112-04-24 |
| 211208-o-krok-za-daleko             | zorientowała się, że Hestia d'Neotik oszukuje Szarjana i Neotik. Wyekstraktowała skutecznie próbkę ixiońską ze zniszczonej Nereidy. Wydobyła cenne dane ze stoczni Neotik i wpisała je w Infernię i w swoje prywatne dane. Gdy Eustachy ujeżdżał Infernię, ratowała ludzi jak była w stanie. Czuje się chora - poszli za daleko w poszukiwaniu mocy. | 0112-04-25 - 0112-04-26 |
| 211222-kult-saitaera-w-neotik       | od Tosena pozyskała eksperymentalną broń ręczną anty-Serenitową do walki z ixionem; prowadziła link Eleny x Inferni, ukryła Elenę przed Hestią d'Neotik i pozyskała listę potencjalnie Skażonych jednostek interaktujących z Neotik. | 0112-04-30 - 0112-05-01 |
| 220105-to-nie-pulapka-na-nereide    | Desperacko próbując ratować Infernię działała jako super-katalistka przepuściła przez siebie więcej energii niż powinna. By ratować Infernię i wszystkich innych przekierowała energię w Izabelę, niszcząc ją. | 0112-05-04 - 0112-05-09 |
| 220126-keldan-voss-kolonia-saitaera | Wyłapywała nieścisłości w historii Pallidan i starała się zrozumieć sytuację. Doszła do tego, że lokalna Hestia to chyba nie Hestia. Więc co? | 0112-05-24 - 0112-05-27 |
| 220202-sekrety-keldan-voss          | Odkryła specyfikę mgieł Keldan Voss - energie z vitium. Zidentyfikowała też obecność BIA klasy Reitel zamiast Hestii. I doszła do historii tego miejsca - noktianie vs drakolici a potem sojusz. | 0112-05-29 - 0112-05-31 |
| 220216-polityka-rujnuje-pallide-voss | infiltruje Pallidę Voss jako audytorka, zdobyła 100% potrzebnych informacji i twardych dowodów. Potem - kontrolowała ewakuację Pallidy Voss i dzięki niej udało się uratować więcej niż się zdawało. | 0112-06-01 - 0112-06-03 |
| 220223-stabilizacja-keldan-voss     | wykorzystała Elenę jako filtr na sygnały z mgieł, dzięki czemu uratowała sporo istnień i zrozumiała co się tu dzieje. | 0112-06-04 - 0112-06-07 |
| 220309-upadek-eleny                 | zarządzała komunikacją, sygnałami itp. Była w cieniu. I projektowała mechanizm, dzięki któremu da się wyczyścić Elenę i zapewnić, że nie stanie się potworem. | 0112-06-08 - 0112-06-14 |
| 220316-potwor-czy-choroba-na-etaur  | włamuje się do roboczego TAI (nie Nephthys) i zakłada backdoor; dowiaduje się o przeszłości tej bazy. | 0112-06-29 - 0112-07-01 |
| 220330-etaur-i-przyneta-na-krypte   | wbiła się do Barbakanu i dostała obraz sytuacji i defensive tools. Odkryła, że Antoni ("biologiczny PO AI") pomaga potworowi. Zastawia pułapkę z Eustachym. | 0112-07-02 - 0112-07-05 |
| 220610-ratujemy-porywaczy-eleny     | odszyfrowuje dane Martyna, dochodzi do tego że to miragent, wspiera TAI Hestia d'Atropos i poznaje od niej prawdę a potem oblicza jak dotrzeć do statku niewolniczego zanim Elena się przebudzi. Absolutna MVP operacji. | 0112-09-15 - 0112-09-17 |
| 220706-etaur-dziwnie-wazny-wezel    | zaproszona przez Eustachego do konspiracji Termii wskazała jak istotna jest baza EtAur dla Orbitera i Syndykatu. Potem przeanalizowała czego EtAur pragnie by sojusz EtAur z Infernią był jak najlepszy (i by jak najbardziej pomóc sojuszowi EtAur - Termia). | 0112-09-19 - 0112-09-21 |
| 220615-lewiatan-przy-seibert        | próbując zrozumieć Lewiatana wpadła w bazyliszka, ale potem zbudowała worma anty-bazyliszkowego i zainfekowała nim wszystkie maszyny. Też - zmieniła parametry ćwiczeń by kapitan Zająca 3 przegrał z Infernią. | 0112-09-27 - 0112-09-29 |
| 220622-lewiatan-za-pandore          | poznała sekret Rzeźnika Parszywca Diakona i rozpoznała co Ola Szerszeń robi z Regeneratorem Eleny. By dojść do tego o co chodzi Oli poprosiła o pomoc Marię Naavas co skończyło się katastrofą dla Termii. Przeanalizowała Lewiatana i doszła do tego jak ów działa. | 0112-09-30 - 0112-10-03 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211122-czolenkowe-esuriit-w-amz     | zauważona przez nauczycieli i terminusów jeśli chodzi o jej zdolności organizatorskie - pomogła uratować wielu uczniów w AMZ jak uderzyło Esuriit. Ma więcej uprawnień i odpowiedzialności od teraz. | 0083-10-22
| 210926-nowa-strazniczka-amz         | uczennica Talii Aegis w obszarze anomalii (noktiańskie wykorzystywanie techniki) i TAI | 0084-06-26
| 211009-szukaj-serpentisa-w-lesie    | dopóki jest w Pustogorze, ma "pet drone" z TAI poziomu 1 zrobione przez Talię Aegis. Ta drona ma połączenie ze Strażniczką Alair. | 0084-11-14
| 211017-nastolatka-w-bieliznie-na-dachu-w-burzy | mieszka w akademiku AMZ z Teresą i Ksenią. | 0084-12-15
| 211017-nastolatka-w-bieliznie-na-dachu-w-burzy | KONFIDENTKA. Dała Saszy informacje o tym jak niektórzy magowie AMZ (uczniowie) robią złe rzeczy wobec ludności. Tamci jej za to bardzo nie lubią. | 0084-12-15
| 211019-czarodziejka-ktora-jednak-moze-sie-zabic | następne dwa tygodnie ma przechlapane. Za dużo pracy organizacyjnej - aż Trzewń ją odciąży. | 0084-12-24
| 240114-o-seksbotach-i-syntetycznych-intelektach | opinia tej co się ZADAJE z seksbotami i 'tą psychotroniczką od seksbotów'. Jej reputacja (grzecznej i niewinnej) ucierpiała. | 0085-01-18
| 230528-helmut-i-nieoczekiwana-awaria-lancera | znajomość z Nikodemem Dewiremiczem, teoretykiem Anomalii Statystycznej z Orbitera. Nikodem jej zawdzięcza dowód że jego teoria działa. | 0109-09-26
| 230530-ziarno-kuratorow-na-karnaxianie | nie udało jej się uratować ludzi z ręki Kuratora Sarkamaira. Nigdy więcej. Rana psychiczna - jej celem jest zapewnienie, że następnym razem jak spotka Kuratorów to będzie gotowa. Przygotowanie + praca. | 0109-10-07
| 240110-wieczna-wojna-bladawira      | nie jest w stanie współpracować z Zaarą Mieralit. Po prostu ich sposoby myślenia i patterny są zbyt niekompatybilne. | 0110-12-27
| 200715-sabotaz-netrahiny            | ma kody kontrolne "uwalniające" TAI Persefona i koordynaty, gdzie może dowiedzieć się więcej. | 0111-01-07
| 201216-krypta-i-wyjec               | 3 dni odchorowania po użyciu Anomalicznego Decoy; niestety, nawet tak krótkie użycie tego cholernego gadżetu ma konsekwencje. | 0111-03-31
| 201224-nieprawdopodobny-zbieg-okolicznosci | za dwa tygodnie będzie mieć stabilną, sprawną kwaterę w czarnych sektorach K1 dookoła badań biologicznych, którą dzieli z Jasminą Perikas. | 0111-04-16
| 210721-pierwsza-bia-mag             | Skażona przez Esuriit przez rozdzielenie po sympatii z Bią; miesiąc regeneracji. | 0111-05-19
| 210120-sympozjum-zniszczenia        | reputacja "świetni w niszczeniu i destrukcji, ale idźcie na bok i nie psujcie więcej". Takie trochę dzieci od zniszczenia. | 0111-08-05
| 210108-ratunkowa-misja-goldariona   | ma pozytywne stosunki i kontakty z ArcheoPrzędz, cywilną podupadłą firmą na Kontrolerze Pierwszym. | 0111-08-15
| 200429-porwanie-cywila-z-kokitii    | łaskawe oko Sił Specjalnych, frakcja "Gorący Lód" oraz Medei Sowińskiej. Też plotki że współpracuje z tymi radykałami sił specjalnych Orbitera. | 0111-10-06
| 210519-osiemnascie-oczu             | wysokie uznanie ze strony Mariana Tosena z grupy antymemetyczej Orbitera. | 0111-12-18
| 210901-stabilizacja-bramy-eterycznej | jawnie współpracuje biurokratycznie z siłami specjalnymi Medei; dla niektórych to SUPER, dla innych to poważny problem. | 0112-02-12
| 211114-admiral-gruby-wieprz         | pewien cios w relacje z Rafaelem Galwarnem; oczekiwałby, że ona zrobi to co zgodne z zasadami K1 a nie dobre dla wolnych TAI. Galwarn dalej współpracuje, ale nie ufa w jej intencje. | 0112-02-20
| 211124-prototypowa-nereida-natalii  | poparzona magicznie i mentalnie przez tydzień; za silna integracja z umierającą Persefoną itp. | 0112-04-24
| 211208-o-krok-za-daleko             | ma dostęp do aktywnej i potężnej próbki ixiońskiej ze zniszczonej Nereidy; contained. | 0112-04-26
| 211208-o-krok-za-daleko             | ma backdoor do Hestii d'Neotik i do niejawnych planów nad którymi pracuje Stocznia Neotik. | 0112-04-26
| 211222-kult-saitaera-w-neotik       | ma ze Stoczni Neotik listę jednostek podejrzanych o to, że zostały zarażone albo przez Kult Saitaera albo przez niższe Spustoszenie. | 0112-05-01
| 220330-etaur-i-przyneta-na-krypte   | NIECHĘĆ ze strony purystów Orbitera. PRO-LUDZKA grupa Orbitera, Eternia i Aurum uważają Infernię za siłę której wiele zawdzięczają. | 0112-07-05
| 220622-lewiatan-za-pandore          | pozyskała bardzo specyficzną cyberformę; "zwierzątko domowe" z Lewiatana z okolic Seibert. | 0112-10-03

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 74 | ((200106-infernia-martyn-hiwasser; 200122-kiepski-altruistyczny-terroryzm; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200429-porwanie-cywila-z-kokitii; 200610-ixiacka-wersja-malictrix; 200624-ratujmy-castigator; 200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210707-po-drugiej-stronie-bramy; 210721-pierwsza-bia-mag; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore; 231018-anomalne-awarie-athamarein; 231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240110-wieczna-wojna-bladawira; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Eustachy Korkoran    | 64 | ((200122-kiepski-altruistyczny-terroryzm; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200429-porwanie-cywila-z-kokitii; 200624-ratujmy-castigator; 200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210707-po-drugiej-stronie-bramy; 210721-pierwsza-bia-mag; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore; 220706-etaur-dziwnie-wazny-wezel; 231018-anomalne-awarie-athamarein; 231025-spiew-nielalki-na-castigatorze; 231220-bladawir-kontra-przemyt-tienow; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Elena Verlen         | 50 | ((200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201224-nieprawdopodobny-zbieg-okolicznosci; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210108-ratunkowa-misja-goldariona; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210707-po-drugiej-stronie-bramy; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220610-ratujemy-porywaczy-eleny; 231018-anomalne-awarie-athamarein; 231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Martyn Hiwasser      | 44 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200819-sekrety-orbitera-historia-prawdziwa; 200822-gdy-arianna-nie-patrzy; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210108-ratunkowa-misja-goldariona; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210209-wolna-tai-na-k1; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 220610-ratujemy-porywaczy-eleny; 220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 240102-zaloga-vishaera-przezyje; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze; 240204-skazona-symulacja-tabester)) |
| Leona Astrienko      | 27 | ((200408-o-psach-i-krysztalach; 200429-porwanie-cywila-z-kokitii; 200610-ixiacka-wersja-malictrix; 200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210106-sos-z-haremu; 210209-wolna-tai-na-k1; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 220610-ratujemy-porywaczy-eleny; 231018-anomalne-awarie-athamarein; 231025-spiew-nielalki-na-castigatorze)) |
| OO Infernia          | 15 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210825-uszkodzona-brama-eteryczna; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 220622-lewiatan-za-pandore; 231025-spiew-nielalki-na-castigatorze; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Izabela Zarantel     | 14 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki; 211114-admiral-gruby-wieprz; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 231115-cwiczenia-komodora-bladawira)) |
| Kamil Lyraczek       | 12 | ((200106-infernia-martyn-hiwasser; 200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211020-kurczakownia; 220105-to-nie-pulapka-na-nereide; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Maria Naavas         | 12 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220622-lewiatan-za-pandore; 220706-etaur-dziwnie-wazny-wezel)) |
| Antoni Kramer        | 10 | ((200429-porwanie-cywila-z-kokitii; 200708-problematyczna-elena; 200729-nocna-krypta-i-emulatorka; 200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert)) |
| Anastazja Sowińska   | 9 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Diana Arłacz         | 9 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 201224-nieprawdopodobny-zbieg-okolicznosci; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| Raoul Lavanis        | 9 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220622-lewiatan-za-pandore; 231025-spiew-nielalki-na-castigatorze; 231220-bladawir-kontra-przemyt-tienow; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Antoni Bladawir      | 8 | ((231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240110-wieczna-wojna-bladawira; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Ksenia Kirallen      | 8 | ((210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 211122-czolenkowe-esuriit-w-amz; 220119-sekret-samanty-arienik; 240114-o-seksbotach-i-syntetycznych-intelektach)) |
| Leszek Kurzmin       | 7 | ((200624-ratujmy-castigator; 200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa; 231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Aleksandra Termia    | 6 | ((200826-nienawisc-do-swin; 201014-krystaliczny-gniew-elizy; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 220706-etaur-dziwnie-wazny-wezel)) |
| Fabian Korneliusz    | 6 | ((220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 240102-zaloga-vishaera-przezyje)) |
| Olgierd Drongon      | 6 | ((200708-problematyczna-elena; 201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210630-listy-od-fanow; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| OO Serbinius         | 6 | ((220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 240204-skazona-symulacja-tabester)) |
| Otto Azgorn          | 6 | ((210421-znudzona-zaloga-inferni; 210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| AK Nocna Krypta      | 5 | ((200729-nocna-krypta-i-emulatorka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210825-uszkodzona-brama-eteryczna)) |
| Annika Pradis        | 5 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Arnulf Poważny       | 5 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Flawia Blakenbauer   | 5 | ((210616-nieudana-infiltracja-inferni; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Mariusz Trzewń       | 5 | ((210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 211122-czolenkowe-esuriit-w-amz; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| OO Tivr              | 5 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos; 220622-lewiatan-za-pandore; 240131-anomalna-mavidiz)) |
| Roland Sowiński      | 5 | ((210512-ewakuacja-z-serenit; 210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii)) |
| Talia Aegis          | 5 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 220119-sekret-samanty-arienik; 240114-o-seksbotach-i-syntetycznych-intelektach)) |
| Teresa Mieralit      | 5 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie; 240114-o-seksbotach-i-syntetycznych-intelektach)) |
| Tomasz Sowiński      | 5 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210818-siostrzenica-morlana)) |
| Ataienne             | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Diana d'Infernia     | 4 | ((210804-infernia-jest-nasza; 210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik)) |
| Felicjan Szarak      | 4 | ((211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa; 211122-czolenkowe-esuriit-w-amz; 220119-sekret-samanty-arienik)) |
| Helmut Szczypacz     | 4 | ((230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 240102-zaloga-vishaera-przezyje)) |
| Marta Keksik         | 4 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Patryk Samszar       | 4 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Tadeusz Ursus        | 4 | ((200819-sekrety-orbitera-historia-prawdziwa; 200822-gdy-arianna-nie-patrzy; 200826-nienawisc-do-swin; 210428-infekcja-serenit)) |
| TAI Rzieza d'K1      | 4 | ((210209-wolna-tai-na-k1; 210630-listy-od-fanow; 211013-szara-nawalnica; 211027-rzieza-niszczy-infernie)) |
| Zaara Mieralit       | 4 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240110-wieczna-wojna-bladawira)) |
| Adam Szarjan         | 3 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik)) |
| Anastazy Termann     | 3 | ((230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 240102-zaloga-vishaera-przezyje)) |
| Damian Orion         | 3 | ((200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Dariusz Krantak      | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 210127-porwanie-anastazji-z-odkupienia)) |
| Eliza Ira            | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Feliks Walrond       | 3 | ((210526-morderstwo-na-inferni; 231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren)) |
| Janus Krzak          | 3 | ((210707-po-drugiej-stronie-bramy; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej)) |
| Jolanta Sowińska     | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Lars Kidironus       | 3 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240124-mikiptur-zemsta-woltaren)) |
| Marian Fartel        | 3 | ((200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Marian Tosen         | 3 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni)) |
| Mateus Sarpon        | 3 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss)) |
| Medea Sowińska       | 3 | ((200429-porwanie-cywila-z-kokitii; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| OO Castigator        | 3 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka; 231025-spiew-nielalki-na-castigatorze)) |
| OO Netrahina         | 3 | ((200715-sabotaz-netrahiny; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej)) |
| OO Paprykowiec       | 3 | ((231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| OO Żelazko           | 3 | ((200708-problematyczna-elena; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Persefona d'Infernia | 3 | ((200708-problematyczna-elena; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit)) |
| Sasza Morwowiec      | 3 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 240114-o-seksbotach-i-syntetycznych-intelektach)) |
| Strażniczka Alair    | 3 | ((210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa)) |
| Szczepan Kaltaben    | 3 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss)) |
| Tymon Grubosz        | 3 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Wawrzyn Rewemis      | 3 | ((211110-romans-dzieki-esuriit; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| Adalbert Brześniak   | 2 | ((210209-wolna-tai-na-k1; 211114-admiral-gruby-wieprz)) |
| AK Serenit           | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| AK Wyjec             | 2 | ((201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Anastazja Sowińska Dwa | 2 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia)) |
| Bogdan Anatael       | 2 | ((210512-ewakuacja-z-serenit; 210630-listy-od-fanow)) |
| Dominik Ogryz        | 2 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza)) |
| Ewa Razalis          | 2 | ((231115-cwiczenia-komodora-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Gilbert Bloch        | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Igor Arłacz          | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Juliusz Sowiński     | 2 | ((201021-noktianie-rodu-arlacz; 201210-pocalunek-aspirii)) |
| Karol Reichard       | 2 | ((210526-morderstwo-na-inferni; 211110-romans-dzieki-esuriit)) |
| Kazimierz Darbik     | 2 | ((231220-bladawir-kontra-przemyt-tienow; 240110-wieczna-wojna-bladawira)) |
| Konstanty Keksik     | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Lutus Amerin         | 2 | ((211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Malictrix d'Pandora  | 2 | ((200610-ixiacka-wersja-malictrix; 210414-dekralotyzacja-asimear)) |
| Martyna Bianistek    | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Natalia Aradin       | 2 | ((211124-prototypowa-nereida-natalii; 220105-to-nie-pulapka-na-nereide)) |
| OA Zguba Tytanów     | 2 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji)) |
| OE Falołamacz        | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Ola Szerszeń         | 2 | ((220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore)) |
| OO Karsztarin        | 2 | ((231115-cwiczenia-komodora-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| OO Wesoły Wieprzek   | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Rafael Galwarn       | 2 | ((210630-listy-od-fanow; 211114-admiral-gruby-wieprz)) |
| Rafał Armadion       | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Robert Garwen        | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Rufus Komczirp       | 2 | ((200715-sabotaz-netrahiny; 201111-mychainee-na-netrahinie)) |
| SCA Płetwal Błękitny | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| SP Pallida Voss      | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Tal Marczak          | 2 | ((210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy)) |
| Tomasz Kaltaben      | 2 | ((220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Vigilus Mevilig      | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Wanessa Pyszcz       | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Wioletta Keiril      | 2 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza)) |
| Władawiec Diakon     | 2 | ((240110-wieczna-wojna-bladawira; 240124-mikiptur-zemsta-woltaren)) |
| Zygfryd Maus         | 2 | ((211117-porwany-trismegistos; 220126-keldan-voss-kolonia-saitaera)) |
| Achellor Santorinus  | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Adam Permin          | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Agostino Karwen      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Aida Serenit         | 1 | ((210512-ewakuacja-z-serenit)) |
| AK Rodivas           | 1 | ((201230-pulapka-z-anastazji)) |
| AK Salamin           | 1 | ((200708-problematyczna-elena)) |
| Alara Ehmes          | 1 | ((200429-porwanie-cywila-z-kokitii)) |
| Albert Kalandryk     | 1 | ((210926-nowa-strazniczka-amz)) |
| Aleksander Leszert   | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Alicja Szadawir      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Andrea Burgacz       | 1 | ((210209-wolna-tai-na-k1)) |
| Andrzej Gwozdnik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Aneta Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Anna Tessalon        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Arazille             | 1 | ((220119-sekret-samanty-arienik)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Artur Traffal        | 1 | ((210804-infernia-jest-nasza)) |
| BIA Solitaria d'Zona Tres | 1 | ((210721-pierwsza-bia-mag)) |
| Błażej Arienik       | 1 | ((220119-sekret-samanty-arienik)) |
| Błażej Sowiński      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Borys Kragin         | 1 | ((240131-anomalna-mavidiz)) |
| Bożena Kokorobant    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Dariusz Drewniak     | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Dariusz Skórnik      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Dawid Aximar         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Dominik Kardawicz    | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Dominika Perikas     | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Donald Parziarz      | 1 | ((201210-pocalunek-aspirii)) |
| Dormand Miraris      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Dorota Radraszew     | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Edelmira Neralis     | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Eleonora Perłamila   | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Emilia Ibris         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Emilia Kariamon      | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Ernest Bankierz      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Ernest Termann       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Eszara d'Seibert     | 1 | ((220615-lewiatan-przy-seibert)) |
| Ewaryst Kajman       | 1 | ((240204-skazona-symulacja-tabester)) |
| Feliks Przędz        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Franciszek Maszkiet  | 1 | ((210804-infernia-jest-nasza)) |
| Grigor Tarnow        | 1 | ((240131-anomalna-mavidiz)) |
| Grzegorz Chropst     | 1 | ((201224-nieprawdopodobny-zbieg-okolicznosci)) |
| Grzegorz Czerw       | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Gunnar Brunt         | 1 | ((201125-wyscig-jako-randka)) |
| Halina Szkwalnik     | 1 | ((201111-mychainee-na-netrahinie)) |
| Henryk Sowiński      | 1 | ((201230-pulapka-z-anastazji)) |
| Henryk Urkon         | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Hestia d'Atropos     | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Hestia d'Neotik      | 1 | ((211208-o-krok-za-daleko)) |
| Hiacynt Samszar      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Horacy Aktenir       | 1 | ((210106-sos-z-haremu)) |
| Hubert Menczik       | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Ignacy Szarjan       | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Igor Stratos         | 1 | ((240131-anomalna-mavidiz)) |
| Iskander Matorin     | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Jakub Bulgocz        | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Jakub Oroginiec      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Janet Erwon          | 1 | ((200115-pech-komodora-ogryza)) |
| Janusz Parzydeł      | 1 | ((210209-wolna-tai-na-k1)) |
| Jarosław Szarjan     | 1 | ((211208-o-krok-za-daleko)) |
| Jasmina Perikas      | 1 | ((201224-nieprawdopodobny-zbieg-okolicznosci)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Jolanta Kopiec       | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Jonasz Parys         | 1 | ((220615-lewiatan-przy-seibert)) |
| Julia Aktenir        | 1 | ((210106-sos-z-haremu)) |
| Julia Myrczek        | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Kaella Sarimanis     | 1 | ((231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Kajetan Kircznik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Kamil Burgacz        | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Kamil Frederico      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Karl Murnoff         | 1 | ((240131-anomalna-mavidiz)) |
| Karol Brinik         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Kasandra Destrukcja Diakon | 1 | ((211222-kult-saitaera-w-neotik)) |
| Katra Igneus         | 1 | ((201210-pocalunek-aspirii)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Klaudiusz Zanęcik    | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Kormonow Voss        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Kosmicjusz Tanecznik Diakon | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Krzysztof Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Kurator Sarkamair    | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Lamia Akacja         | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Lara Linven          | 1 | ((240204-skazona-symulacja-tabester)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Leo Mikirnik         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Leszek Czarban       | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Lidia Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Llarnagraht          | 1 | ((210414-dekralotyzacja-asimear)) |
| Lothar Diakon        | 1 | ((200715-sabotaz-netrahiny)) |
| Łucja Larnecjat      | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Maciej Żarand        | 1 | ((210804-infernia-jest-nasza)) |
| Maciek Kalmarzec     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Marcinozaur Verlen   | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Maria Arienik        | 1 | ((220119-sekret-samanty-arienik)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Marian Witaczek      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Marianna Lemurczak   | 1 | ((200923-magiczna-burza-w-raju)) |
| Mariusz Tubalon      | 1 | ((210414-dekralotyzacja-asimear)) |
| Markus Wąż           | 1 | ((240131-anomalna-mavidiz)) |
| Martauron Attylla    | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Maryla Koternik      | 1 | ((211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Mateusz Sowiński     | 1 | ((200415-sabotaz-miecza-swiatla)) |
| Melania Akacja       | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Melwin Sito          | 1 | ((200610-ixiacka-wersja-malictrix)) |
| Michalina Kefir      | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Michał Teriakin      | 1 | ((210630-listy-od-fanow)) |
| Miki Katasair        | 1 | ((210209-wolna-tai-na-k1)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Miranda Termann      | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Mirela Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Mojra Karstall       | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Morrigan d'Tirakal   | 1 | ((210421-znudzona-zaloga-inferni)) |
| Natalia Miszryk      | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Nataniel Morlan      | 1 | ((210818-siostrzenica-morlana)) |
| Nikodem Dewiremicz   | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Nikodem Sowiński     | 1 | ((200923-magiczna-burza-w-raju)) |
| OA Bakałarz          | 1 | ((210120-sympozjum-zniszczenia)) |
| OA Mikiptur          | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| OA Odkupienie        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OE Lord Savaron      | 1 | ((210630-listy-od-fanow)) |
| OE Piękna Elena      | 1 | ((210428-infekcja-serenit)) |
| Ofelia Morlan        | 1 | ((210818-siostrzenica-morlana)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Oliwia Pietrova      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| ONS Mavidiz          | 1 | ((240131-anomalna-mavidiz)) |
| OO Alaya             | 1 | ((210519-osiemnascie-oczu)) |
| OO Alkaris           | 1 | ((200624-ratujmy-castigator)) |
| OO Athamarein        | 1 | ((231018-anomalne-awarie-athamarein)) |
| OO Aurelion          | 1 | ((200708-problematyczna-elena)) |
| OO Galaktyczny Tucznik | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| OO Isratazir         | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| OO Itorwien          | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Kastor            | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Opresor           | 1 | ((210616-nieudana-infiltracja-inferni)) |
| OO Pandora           | 1 | ((220622-lewiatan-za-pandore)) |
| OO Samotność Gwiazd  | 1 | ((210804-infernia-jest-nasza)) |
| OO Straszliwy Pająk  | 1 | ((220622-lewiatan-za-pandore)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Tvarana           | 1 | ((200715-sabotaz-netrahiny)) |
| OOn Tabester         | 1 | ((240204-skazona-symulacja-tabester)) |
| Percival Diakon      | 1 | ((200715-sabotaz-netrahiny)) |
| Perdius Aximar       | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Rafał Grambucz       | 1 | ((210428-infekcja-serenit)) |
| Ralena Drewniak      | 1 | ((240114-o-seksbotach-i-syntetycznych-intelektach)) |
| Remigiusz Alkarenit  | 1 | ((231018-anomalne-awarie-athamarein)) |
| Remigiusz Błyszczyk  | 1 | ((211013-szara-nawalnica)) |
| Remigiusz Falorin    | 1 | ((210630-listy-od-fanow)) |
| Rita Stratos         | 1 | ((240131-anomalna-mavidiz)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Roberto Santorinus   | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Roman Panracz        | 1 | ((211114-admiral-gruby-wieprz)) |
| Romana Arnatin       | 1 | ((210721-pierwsza-bia-mag)) |
| Rozalia Teirik       | 1 | ((210106-sos-z-haremu)) |
| Rozalia Wączak       | 1 | ((200624-ratujmy-castigator)) |
| Rufus Niegnat        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Rzeźnik Parszywiec Diakon | 1 | ((220622-lewiatan-za-pandore)) |
| Rzieza d'K1          | 1 | ((211114-admiral-gruby-wieprz)) |
| Sabina Kazitan       | 1 | ((211114-admiral-gruby-wieprz)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Saitaer              | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Samanta Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| Samuel Fanszakt      | 1 | ((240110-wieczna-wojna-bladawira)) |
| SC Fecundatis        | 1 | ((210818-siostrzenica-morlana)) |
| SC Hektor 17         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| SC Karnaxian         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| SC Korona Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| SC Pancerna Jaszczurka | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| SCA Goldarion        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Sebastian Alarius    | 1 | ((200826-nienawisc-do-swin)) |
| Sebastian Namczek    | 1 | ((200408-o-psach-i-krysztalach)) |
| Semla d'Goldarion    | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| SL Rajasee Bagh      | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| SL Uśmiechnięta      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Sławomir Arienik     | 1 | ((220119-sekret-samanty-arienik)) |
| Sonia Ogryz          | 1 | ((200115-pech-komodora-ogryza)) |
| Sonia Skardin        | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| SP Plugawy Jaszczur  | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| SP Światło Nadziei   | 1 | ((220223-stabilizacja-keldan-voss)) |
| Stefan Jamniczek     | 1 | ((201118-anastazja-bohaterka)) |
| Suwan Chankar        | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Sylwia Milarcz       | 1 | ((200822-gdy-arianna-nie-patrzy)) |
| Szczepan Myksza      | 1 | ((200715-sabotaz-netrahiny)) |
| Szczepan Olgrod      | 1 | ((200106-infernia-martyn-hiwasser)) |
| Szymon Orzesznik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Tadeusz Arkaladis    | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| TAI Ashtaria d'Tabester | 1 | ((240204-skazona-symulacja-tabester)) |
| TAI Eszara d'Castigator | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| TAI Neita Lairossa   | 1 | ((210209-wolna-tai-na-k1)) |
| TAI XT-723 d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI Zefiris          | 1 | ((210630-listy-od-fanow)) |
| Talia Miraris        | 1 | ((240110-wieczna-wojna-bladawira)) |
| Tara Ogniczek        | 1 | ((240131-anomalna-mavidiz)) |
| Teodor Margrabarz    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Tomasz Rewernik      | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Tymoteusz Czerw      | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Udom Rapnak          | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Urszula Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| Uśmiechniczka Konstrukcjonistka Aurora Diakon | 1 | ((231018-anomalne-awarie-athamarein)) |
| Waldemar Grzymość    | 1 | ((211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Wiktor Szurmak       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Wojciech Mykirło     | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Wolfgang Sępiarz     | 1 | ((240102-zaloga-vishaera-przezyje)) |