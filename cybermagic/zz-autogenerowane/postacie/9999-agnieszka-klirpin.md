---
categories: profile
factions: 
owner: public
title: Agnieszka Klirpin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230620-karolinus-sedzia-mirkali     | sekretarka burmistrza Mirkali; wielka fanka Samszarów. Z przyjemnością przekazała Karolinusowi wszystkie dokumenty drugiego obiegu i pokazała jak je dekodować. | 0095-04-15 - 0095-04-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230620-karolinus-sedzia-mirkali)) |
| Filip Klirpin        | 1 | ((230620-karolinus-sedzia-mirkali)) |
| Juanita Derwisz      | 1 | ((230620-karolinus-sedzia-mirkali)) |
| Karolinus Samszar    | 1 | ((230620-karolinus-sedzia-mirkali)) |