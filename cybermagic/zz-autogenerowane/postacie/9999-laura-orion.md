---
categories: profile
factions: 
owner: public
title: Laura Orion
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200129-nieudana-niechetna-inwazja   |  | 0110-10-22 - 0110-10-27 |
| 200729-nocna-krypta-i-emulatorka    | KIA. Kijara nie zniszczyła jej rdzenia; Laura chciała zniszczyć tylko Eternię. Nie miała jednak szans - skończyła w ogniu Castigatora. | 0111-01-14 - 0111-01-20 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Nocna Krypta      | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Antoni Kramer        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Arianna Verlen       | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Damian Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Eustachy Korkoran    | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Janet Erwon          | 1 | ((200129-nieudana-niechetna-inwazja)) |
| Kaja Tamaris         | 1 | ((200129-nieudana-niechetna-inwazja)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Klaudia Stryk        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Leona Astrienko      | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Marian Kurczak       | 1 | ((200129-nieudana-niechetna-inwazja)) |
| Mirela Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Castigator        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Rufus Karmazyn       | 1 | ((200129-nieudana-niechetna-inwazja)) |
| Sonia Ogryz          | 1 | ((200129-nieudana-niechetna-inwazja)) |