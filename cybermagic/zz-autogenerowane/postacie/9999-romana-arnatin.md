---
categories: profile
factions: 
owner: public
title: Romana Arnatin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210721-pierwsza-bia-mag             | noktiański lekarz na Inferni, wsparcie Martyna. Wraz z Solitarią pomogła Martynowi używać noktiańskiej technologii w obawie, że ten skrzywdzi Klaudię. | 0111-05-17 - 0111-05-19 |
| 210728-w-cieniu-nocnej-krypty       | zdecydowała się zostać w Zonie Tres i opuściła załogę Inferni wraz z kilkunastoma noktianami. | 0111-05-20 - 0111-06-06 |
| 210728-w-cieniu-nocnej-krypty       | zdecydowała się zostać w Zonie Tres, opuściła Infernię z kilkunastoma noktianami. | 0111-05-20 - 0111-06-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 3 | ((210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| Eustachy Korkoran    | 3 | ((210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| Janus Krzak          | 3 | ((210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| AK Nocna Krypta      | 2 | ((210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| Atrius Kurunen       | 2 | ((210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| Finis Vitae          | 2 | ((210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| Gerard Adanor        | 2 | ((210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| Helena Adanor        | 2 | ((210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| Oliwia Karelan       | 2 | ((210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| Ulisses Kalidon      | 2 | ((210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| BIA Solitaria d'Zona Tres | 1 | ((210721-pierwsza-bia-mag)) |
| Klaudia Stryk        | 1 | ((210721-pierwsza-bia-mag)) |
| Martyn Hiwasser      | 1 | ((210721-pierwsza-bia-mag)) |