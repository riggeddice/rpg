---
categories: profile
factions: 
owner: public
title: OO Serbinius
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220716-chory-piesek-na-statku-luxuritias | szybka korweta patrolowa przypisana do Atropos i misji medycznych; przechwyciła Rajasee Bagh by zatrzymać tajemniczą plagę zgłoszoną przez dzieciaka. Dowodzi Fabian Korneliusz. | 0109-05-05 - 0109-05-07 |
| 220925-mlodziaki-na-savaranskim-statku-handlowym | przekierowany przez kapitana Fabiana by uratować zaginiętego syna jego mentora. | 0109-05-24 - 0109-05-25 |
| 230521-rozszczepiona-persefona-na-itorwienie | korweta awaryjnego ratunku, ma własne konfiguratory Lancerów i do 6 operacyjnych agentów (Fabian, Klaudia, Martyn, Helmut i jeszcze dwóch). Tym razem - na ratunek Itorwien. | 0109-09-15 - 0109-09-17 |
| 230528-helmut-i-nieoczekiwana-awaria-lancera | skutecznie uratował kilka jednostek cywilnych od niewielkich awarii, po czym dostarczył Martyna do ratowania Anastazego i jego jetpacka. | 0109-09-23 - 0109-09-26 |
| 230530-ziarno-kuratorow-na-karnaxianie | odparł atak Kuratora Sarkamaira, ma lakko uszkodzone systemy komunikacyjne. Musiał zestrzelić przechwyconego Karnaxiana, nie był w stanie niestety więcej zrobić. | 0109-10-06 - 0109-10-07 |
| 240204-skazona-symulacja-tabester   | wysłany zdecydowanie poza zasięg działań Orbitera, by pomóc OOn Tabester, bo z jednostek klasy 'far logistics' tylko Serbinius ma psychotroniczkę klasy Klaudii w pobliżu. | 0109-11-01 - 0109-11-04 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 6 | ((220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 240204-skazona-symulacja-tabester)) |
| Martyn Hiwasser      | 6 | ((220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 240204-skazona-symulacja-tabester)) |
| Fabian Korneliusz    | 5 | ((220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Helmut Szczypacz     | 3 | ((230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Anastazy Termann     | 2 | ((230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Achellor Santorinus  | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Bożena Kokorobant    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Dawid Aximar         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Emilia Ibris         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Ewaryst Kajman       | 1 | ((240204-skazona-symulacja-tabester)) |
| Henryk Urkon         | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Iskander Matorin     | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Karol Brinik         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Klaudiusz Zanęcik    | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Kurator Sarkamair    | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Lara Linven          | 1 | ((240204-skazona-symulacja-tabester)) |
| Leo Mikirnik         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Miranda Termann      | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Mojra Karstall       | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Nikodem Dewiremicz   | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| OO Itorwien          | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| OOn Tabester         | 1 | ((240204-skazona-symulacja-tabester)) |
| Perdius Aximar       | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Roberto Santorinus   | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| SC Hektor 17         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| SC Karnaxian         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| SL Rajasee Bagh      | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Tadeusz Arkaladis    | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| TAI Ashtaria d'Tabester | 1 | ((240204-skazona-symulacja-tabester)) |
| Teodor Margrabarz    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |