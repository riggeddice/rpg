---
categories: profile
factions: 
owner: public
title: OO Tivr
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240131-anomalna-mavidiz             | szybka korweta którą pozyskała Klaudia; wsparcie logistyczne i bojowe. Strzał uszkodził Mavidiz, ale uratował Eustachego przed nihilosektem. Na końcu - poleciał po pomoc. | 0111-01-03 - 0111-01-08 |
| 210519-osiemnascie-oczu             | ultraszybka korweta zarekwirowana przez Mariana Tosena do szybkiego transportu magów Inferni (i Leony) na Alayę. Normalnie stacjonarna na K1. | 0111-12-07 - 0111-12-18 |
| 210526-morderstwo-na-inferni        | kiedyś: noktiański statek usprawniany przez pokolenia ("Aries Tal"). Wszyscy na tej jednostce byli jakoś połączeni z rodziną Tal. Orbiter splugawił komponenty XD. Atm w rękach komodora Walronda z Orbitera. | 0111-12-31 - 0112-01-06 |
| 211117-porwany-trismegistos         | pierwszy lot Arianny i ograniczonej załogi Inferni, z dwoma obserwatorami na pokładzie - Roland Sowiński i Zygfryd Maus. Pilotaż Eleny pobił rekordy Tivra. | 0112-04-15 - 0112-04-17 |
| 220622-lewiatan-za-pandore          | odwraca uwagę Lewiatana od Inferni przykładającej się do ataku torpedą anihilacyjną do Lewiatana. | 0112-09-30 - 0112-10-03 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211110-romans-dzieki-esuriit        | przechodzi pod dowództwo Arianny Verlen, z daru admirał Termii | 0112-04-14
| 220615-lewiatan-przy-seibert        | lekko uszkodzony, trafiony odłamkami złomu kosmicznego tworzącego Lewiatana. | 0112-09-29

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 5 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos; 220622-lewiatan-za-pandore; 240131-anomalna-mavidiz)) |
| Eustachy Korkoran    | 5 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos; 220622-lewiatan-za-pandore; 240131-anomalna-mavidiz)) |
| Klaudia Stryk        | 5 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos; 220622-lewiatan-za-pandore; 240131-anomalna-mavidiz)) |
| Elena Verlen         | 3 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos)) |
| Leona Astrienko      | 3 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos)) |
| Maria Naavas         | 2 | ((211117-porwany-trismegistos; 220622-lewiatan-za-pandore)) |
| Marian Tosen         | 2 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni)) |
| Martyn Hiwasser      | 2 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni)) |
| Raoul Lavanis        | 2 | ((220622-lewiatan-za-pandore; 240131-anomalna-mavidiz)) |
| Antoni Bladawir      | 1 | ((240131-anomalna-mavidiz)) |
| Antoni Kramer        | 1 | ((210526-morderstwo-na-inferni)) |
| Borys Kragin         | 1 | ((240131-anomalna-mavidiz)) |
| Feliks Walrond       | 1 | ((210526-morderstwo-na-inferni)) |
| Grigor Tarnow        | 1 | ((240131-anomalna-mavidiz)) |
| Igor Stratos         | 1 | ((240131-anomalna-mavidiz)) |
| Izabela Zarantel     | 1 | ((211117-porwany-trismegistos)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Kamil Lyraczek       | 1 | ((210526-morderstwo-na-inferni)) |
| Karl Murnoff         | 1 | ((240131-anomalna-mavidiz)) |
| Karol Reichard       | 1 | ((210526-morderstwo-na-inferni)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Markus Wąż           | 1 | ((240131-anomalna-mavidiz)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Ola Szerszeń         | 1 | ((220622-lewiatan-za-pandore)) |
| ONS Mavidiz          | 1 | ((240131-anomalna-mavidiz)) |
| OO Alaya             | 1 | ((210519-osiemnascie-oczu)) |
| OO Infernia          | 1 | ((220622-lewiatan-za-pandore)) |
| OO Pandora           | 1 | ((220622-lewiatan-za-pandore)) |
| OO Straszliwy Pająk  | 1 | ((220622-lewiatan-za-pandore)) |
| Otto Azgorn          | 1 | ((210526-morderstwo-na-inferni)) |
| Rita Stratos         | 1 | ((240131-anomalna-mavidiz)) |
| Roland Sowiński      | 1 | ((211117-porwany-trismegistos)) |
| Rzeźnik Parszywiec Diakon | 1 | ((220622-lewiatan-za-pandore)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| Tal Marczak          | 1 | ((210526-morderstwo-na-inferni)) |
| Tara Ogniczek        | 1 | ((240131-anomalna-mavidiz)) |
| Zygfryd Maus         | 1 | ((211117-porwany-trismegistos)) |