---
categories: profile
factions: 
owner: public
title: SC Pancerna Jaszczurka
---

# {{ page.title }}


# Generated: 



## Fiszki


* kiedyś: OO, pochodzący z Anomalii Kolapsu i dostosowany z innych jednostek z Kolapsu; jedna z jednostek odzyskanych | @ 221015-duch-na-pancernej-jaszczurce
* "Jaszczurka" miała swoją maiden mission. The ship seems haunted. Cywile z K1 zażądali oddania Jaszczurki do naprawy. | @ 221015-duch-na-pancernej-jaszczurce

### Wątki


historia-klaudii
grupa-wydzielona-serbinius

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221015-duch-na-pancernej-jaszczurce | jednostka złożona z części z Anomalii Kolapsu przez Neotik; opętana i z "bombą" w silniku. Oddana Orbiterowi do naprawy, Klaudia znalazła przyczyny problemów i jednostka znów bezpieczna na Orbiterze. | 0109-08-24 - 0109-08-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dominik Kardawicz    | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Hubert Menczik       | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Ignacy Szarjan       | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Klaudia Stryk        | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Michalina Kefir      | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Sonia Skardin        | 1 | ((221015-duch-na-pancernej-jaszczurce)) |