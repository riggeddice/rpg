---
categories: profile
factions: 
owner: public
title: Petra Karpov
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230913-operacja-spotkac-sie-z-dmitrim | kiedyś arystokratka, oszpecona przez chorobę i ślepa; przyjazna Furiom i pogodzona z losem. Uczy Furie jak zachowywać się jak lokals i opowiedziała im historię Karpovów. | 0081-06-28 - 0081-06-30 |
| 230920-legenda-o-noktianskiej-mafii | torturowana przez Grzymościowców, ale przetrwała. | 0081-07-10 - 0081-07-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 2 | ((230913-operacja-spotkac-sie-z-dmitrim; 230920-legenda-o-noktianskiej-mafii)) |
| Dmitri Karpov        | 2 | ((230913-operacja-spotkac-sie-z-dmitrim; 230920-legenda-o-noktianskiej-mafii)) |
| Xavera Sirtas        | 2 | ((230913-operacja-spotkac-sie-z-dmitrim; 230920-legenda-o-noktianskiej-mafii)) |
| Arnulf Poważny       | 1 | ((230913-operacja-spotkac-sie-z-dmitrim)) |
| Bogdan Gwiazdocisz   | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Irelia Kairanolis    | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Patryk Majwuron      | 1 | ((230920-legenda-o-noktianskiej-mafii)) |