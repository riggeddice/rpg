---
categories: profile
factions: 
owner: public
title: Lila Cziras
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220420-samobojstwo-kapitana-wielkiego-weza | nawigator z uszkodzonym sprzężonym TAI; kiedyś w przeszłości kapitan Jerzy Odmiczak wyciągnął ją z jakiejś zapijaczonej dziury i okazała się przydatna na Wielkim Wężu. Doszła do tego jak zginął kapitan i odkryła kill agenta przetwarzając dane. Wzięła kill agenta na siebie korzystając z neurosprzężenia. | 0108-09-26 - 0108-10-02 |
| 220427-dziwne-strachy-w-morzu-ulud  | z uwagi na problemy z integracją z AI wpakowała wszystkich w paskudne miejsce między Strachami, ale wyprowadziła stamtąd statek. Doszła do tego, że Berdysz - gaulron - ma specjalne potrzeby i próbuje (skutecznie) z nim wejść w jakąś formę nieformalnego sojuszu przeciwko światu. Odszyfrowała z Alanem sygnały z boi "Okrutnej Wrony". | 0108-10-05 - 0108-10-07 |
| 220518-okrutna-wrona-kalcynici-i-koszmary | wraz z Filipem zrobiła appraisal pintki i odkryła link do Elwiry Piscernik; czujnikami i skanem logów doszła do tego co się stało na Wronie - "upiór"?. Włączyła dopalacz, ratując życie Poli przed opętaną Kornelią i uszkodziła strukturalnie Węża. Przekonała Berdysza, że Amandę TRZEBA ratować. Wyprowadziła Węża z piekła... | 0108-10-09 - 0108-10-18 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220420-samobojstwo-kapitana-wielkiego-weza | jej uszkodzone TAI zostało zniszczone przez memetic kill agenta i ów kill agent jest teraz zamknięty w jej implancie. | 0108-10-02

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Falkam          | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Antoni Krutacz       | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Berdysz Rozdzieracz  | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Filip Gościc         | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Kornelia Sanoros     | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Maja Kormoran        | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Pola Mornak          | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Amanda Korel         | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Elwira Piscernik     | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Jerzy Odmiczak       | 1 | ((220420-samobojstwo-kapitana-wielkiego-weza)) |
| Ksawery Janowar      | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Mikołaj Faczon       | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |