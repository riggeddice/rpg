---
categories: profile
factions: 
owner: public
title: Klasa Inżynier
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240117-echo-z-odmetow-przeszlosci   | Ruszył na pomoc oficerowi ochrony, gdy ten miał problem z wydostaniem się z alterisowej rafy Prisma, naprawił sondę i wydobył z niej dane. Zbadał dronami stację badawczą Medimmortal, naprawił przekaźniki do komunikacji, ostatecznie traci zaufanie kapitana Barowieckiego próbując go przekonać do oszukania załogi stacji, potwierdza, że powiedzenie prawdy o anomalii nie zabije załogantów informując Annę, wpędzając ją tym w depresję. | 0096-01-24 - 0096-01-25 |
| 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa | wygasił system recyklingowy gdzie skończył Tom-73 i zalapisował go; znalazł gdzie rurami idzie biomasa i przez to zniszczył aspekt Łowcy energii Fidetis. | 0105-12-11 - 0105-12-13 |
| 231221-pan-skarpetek-i-odratowany-ogrod | gdy napotkał Anomalię Alteris, zdecydował się ją przetestować i 'założył Pana Skarpetka' na głowę. Drony uratowały go przed przejęciem (woda). Potem odwrócił uwagę Mawira i ponaprawiał rury na koszt Agencji, dzięki czemu +5 szacunku ze strony Stacji. | 0106-04-25 - 0106-04-27 |
| 240214-relikwia-z-androida          | wypalenie H4 Hydrolab silnikami Szernief i stabilizacja stacji po trudnym manewrze. Potem, z hackerem, naprawa Vanessy. | 0107-05-16 - 0107-05-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kalista Surilik      | 3 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240214-relikwia-z-androida)) |
| Klasa Dyplomata      | 3 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240117-echo-z-odmetow-przeszlosci; 240214-relikwia-z-androida)) |
| Klasa Sabotażysta    | 3 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240214-relikwia-z-androida)) |
| Mawir Hong           | 3 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240214-relikwia-z-androida)) |
| Aerina Cavalis       | 2 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240214-relikwia-z-androida)) |
| Felina Amatanir      | 2 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod)) |
| Klasa Oficer Naukowy | 2 | ((231221-pan-skarpetek-i-odratowany-ogrod; 240214-relikwia-z-androida)) |
| Alina Mekran         | 1 | ((240214-relikwia-z-androida)) |
| Anna Ivanova         | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Dorion Fughar        | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Elwira Barknis       | 1 | ((240214-relikwia-z-androida)) |
| Elżbieta Sanchez     | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Gabriel Septenas     | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Gerald Barowiecki    | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Ignatius Sozyliw     | 1 | ((231221-pan-skarpetek-i-odratowany-ogrod)) |
| Jędrzej Sanchez      | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Kaldor Czuk          | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klasa Hacker         | 1 | ((240214-relikwia-z-androida)) |
| Klasa Oficer Ochrony | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Robert Tisso         | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Sia-03 Szernief      | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Tadeusz Mekran       | 1 | ((240214-relikwia-z-androida)) |
| Vanessa d'Cavalis    | 1 | ((240214-relikwia-z-androida)) |