---
categories: profile
factions: 
owner: public
title: Zaara Mieralit
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (E- O+) "Zniszczenie to sztuka." | VALS: (Power, Family) "Najlepsza, wir zniszczenia w rękach mojego komodora." | DRIVE: "Spalić świat stojący na drodze Bladawira" | @ 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira
* corrupted ex-noctian, ex-Aurelion 'princess', oddana Bladawirowi fanatyczka. Jego prywatna infomantka. Zakochana w nim na ślepo. | @ 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira
* afiliacja magiczna: Alucis, Astinian | @ 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira

### Wątki


triumfalny-powrot-arianny
naprawa-swiata-przez-bladawira

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira | hiperlojalna zakochana w Bladawirze mentalistka i infomantka. Noktianka porwana przez Aurelion i odbita przez Bladawira. Bardzo chłodna, nieskończenie lojalna i bezwzględna bardziej niż sam Bladawir. Walczy z Arianną psychologicznie próbując ją zmusić do działania jak inni kapitanowie Bladawira. Jej dusza jest uszkodzona, ale umysł jest sprawny. | 0110-11-26 - 0110-11-30 |
| 231220-bladawir-kontra-przemyt-tienow | neutralizowała Klaudię i jej infiltrację (nieskutecznie; Klaudia wdarła się do systemów Karsztarina). Ostrzegła Bladawira o sytuacji zanim go wezwano na dywanik. Z woli Bladawira zrobiła potwora na Karsztarinie (z TAI), który miał wypłoszyć Orzesznika. Udało jej się, ale inne elementy planu się rozsypały. | 0110-12-06 - 0110-12-11 |
| 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow | prowadzi dla Bladawira analizę danych z Klaudią. Nie mówi czego szukają, czuje niesmak i pogardę wobec Klaudii (która pracuje na 80%). PRAWIE się wygadała czego Bladawir szuka, desperacko jej zależy by to znaleźć. | 0110-12-03 - 0110-12-17 |
| 240110-wieczna-wojna-bladawira      | smutna, bo Bladawirowi się rozsypuje plan. Pracuje z całych sił chcąc współpracować z Klaudią, ale nie umie - przez resentymenty i skillset. Nie jest dość dobra. Gdy pomaga Ariannie użyć mocy na tkankę kralotyczną, Arianna się wbija w jej rdzeń i ją edytuje kralotycznie i wydobywa z niej wiedzę. Zaara CZUJE że coś jest nie tak ale nie wie co. | 0110-12-24 - 0110-12-27 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira | pod wpływem Kamila Lyraczka widzi ciepło którego nie ma ale za którym tęskni. Nadal jest nieskończenie lojalna, ale wie też że mogłoby być nieco inaczej niż jest. | 0110-11-30
| 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira | krążą plotki, że Zaara pomaga Bladawirowi w korupcji kapitanów. Niekoniecznie magią (są plotki ale Arianna wie że by wyszło), raczej szuka słabych punktów i je wzmacnia a potem Bladawir stawia tak sytuacje, by oni musieli pójść jego drogą albo być nieefektywni i słabi w oczach swojej załogi | 0110-11-30
| 240110-wieczna-wojna-bladawira      | nie jest w stanie współpracować z Klaudią Stryk. Po prostu ich sposoby myślenia i patterny są zbyt niekompatybilne. | 0110-12-27
| 240110-wieczna-wojna-bladawira      | jej Wzór jest zdestabilizowany przez kralotyczną infuzję (działania Arianny) i nie jest w stanie nikomu o tym powiedzieć że coś się z nią stało lub dzieje. | 0110-12-27
| 240110-wieczna-wojna-bladawira      | prawdziwa nienawiść do Klaudii i do Arianny. Częściowo nie wie czemu (kraloth violation), częściowo: dlaczego one są o tyle od niej lepsze. Dlaczego one nie chcą służyć Bladawirowi jak powinny? Czemu ONA nie jest dość dobra? | 0110-12-27

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Bladawir      | 4 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240110-wieczna-wojna-bladawira)) |
| Arianna Verlen       | 4 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240110-wieczna-wojna-bladawira)) |
| Klaudia Stryk        | 4 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240110-wieczna-wojna-bladawira)) |
| Elena Verlen         | 2 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Kazimierz Darbik     | 2 | ((231220-bladawir-kontra-przemyt-tienow; 240110-wieczna-wojna-bladawira)) |
| Lars Kidironus       | 2 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow)) |
| OO Infernia          | 2 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Dormand Miraris      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Ernest Bankierz      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Eustachy Korkoran    | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Ewa Razalis          | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Kamil Lyraczek       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Marcinozaur Verlen   | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Marian Witaczek      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Marta Keksik         | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| OO Karsztarin        | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| OO Paprykowiec       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Patryk Samszar       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Raoul Lavanis        | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Samuel Fanszakt      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Szymon Orzesznik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Talia Miraris        | 1 | ((240110-wieczna-wojna-bladawira)) |
| Władawiec Diakon     | 1 | ((240110-wieczna-wojna-bladawira)) |