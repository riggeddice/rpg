---
categories: profile
factions: 
owner: public
title: Szczepan Falernik
---

# {{ page.title }}


# Generated: 



## Fiszki


* bardzo dobry salvager arkologii Nativis | @ 230329-zdrada-rozrywajaca-arkologie
* ENCAO:  ++-00 |Nierozważny i nieroztropny;;Ryzykant, zawsze rzuca się w ogień;;Nieśmiały, skrępowany| VALS: Hedonism, Tradition| DRIVE: Zdobyć najlepsze narzędzia (best tools for arcology) | @ 230329-zdrada-rozrywajaca-arkologie

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230329-zdrada-rozrywajaca-arkologie | bardzo poważany salvager, uratował wielu ludzi. Zazdrosny o żonę, porwał ją by dowiedzieć się czy go zdradza. Wpierw pobił Jonasza bo "zdradza z Kasią", potem bo "rozpuszcza plotki o Kasi". Ogólnie pozytywna postać, ale narwany i agresywny. | 0093-03-14 - 0093-03-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Ardilla Korkoran     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| BIA Prometeus        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Celina Lertys        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Eustachy Korkoran    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Franciszek Pietraszczyk | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kalia Awiter         | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Małgorzata Maratelus | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| OO Infernia          | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Rafał Kidiron        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Ralf Tapszecz        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Wojciech Grzebawron  | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |