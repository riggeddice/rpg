---
categories: profile
factions: 
owner: public
title: Leon Kantor
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210820-fecundatis-w-domenie-barana  | świetny górnik, przyjaciel Tamary Mardius. Zniknął, bo przypadkowo uruchomił Aleksandrię przy jednej ze swoich wypraw. | 0109-03-06 - 0109-03-09 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antonella Temaris    | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Antos Kuramin        | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Bruno Baran          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Cień Brighton        | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Damian Szczugor      | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Deneb Ira            | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Elsa Kułak           | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Flawia Blakenbauer   | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Jolanta Sowińska     | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Kara Szamun          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Rick Varias          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Tamara Mardius       | 1 | ((210820-fecundatis-w-domenie-barana)) |