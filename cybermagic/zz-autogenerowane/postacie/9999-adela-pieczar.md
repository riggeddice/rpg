---
categories: profile
factions: 
owner: public
title: Adela Pieczar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190820-liliana-w-swiecie-dokumentow | dokładna i surowa mistrzyni Papierówki; jak Liliana ją denerwuje tak do Maksa ma słabość (bo pomógł jej naprawić roboty). | 0110-07-04 - 0110-07-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arnulf Poważny       | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Liliana Bankierz     | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Szymon Jaszczurzec   | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Tadeusz Kruszawiecki | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Tymon Grubosz        | 1 | ((190820-liliana-w-swiecie-dokumentow)) |