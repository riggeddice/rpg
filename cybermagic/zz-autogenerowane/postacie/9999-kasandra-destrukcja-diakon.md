---
categories: profile
factions: 
owner: public
title: Kasandra Destrukcja Diakon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211215-sklejanie-inferni-do-kupy    | wydział wewnętrzny Stoczni Neotik. Zaniepokojona tym, że ludzie zachowują się dziwnie i Hestia coś ukrywa, współpracuje z Leoną by porwać Eustachego. Znokautowana przez Leonę gdy Eustachy udowodnił, że nie mógł stać za zniknięciami ludzi. | 0112-04-27 - 0112-04-29 |
| 211222-kult-saitaera-w-neotik       | chce aresztować Leonę; daje się jednak przekonać Ariannie, że dużo ważniejszy jest ixioński potwór na Neotik. | 0112-04-30 - 0112-05-01 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Szarjan         | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Arianna Verlen       | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Diana d'Infernia     | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Elena Verlen         | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Eustachy Korkoran    | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Maria Naavas         | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Izabela Zarantel     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Kamil Lyraczek       | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Klaudia Stryk        | 1 | ((211222-kult-saitaera-w-neotik)) |
| Leona Astrienko      | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Lutus Amerin         | 1 | ((211222-kult-saitaera-w-neotik)) |
| Maciek Kalmarzec     | 1 | ((211222-kult-saitaera-w-neotik)) |
| OO Infernia          | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Roland Sowiński      | 1 | ((211215-sklejanie-inferni-do-kupy)) |