---
categories: profile
factions: 
owner: public
title: Azalia Sernat d'Namertel
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210928-wysadzony-zywy-scigacz       | bardzo lekko ubrana (eufemizm) mistrzyni dyplomacji, zarządzania, logistyki i szpiegostwa Ernesta. Sassy. Lubi dogadywać i bardzo blisko przyjaźni się z Żorżem. Blondynka. | 0111-07-26 - 0111-07-27 |
| 211123-odbudowa-wedlug-justyniana   | opracowała solidny plan odbudowy Dzielnicy Rekinów, który potem przywłaszczyła sobie Marysia. Azalia o to nie dba. | 0111-08-11 - 0111-08-20 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ernest Namertel      | 2 | ((210928-wysadzony-zywy-scigacz; 211123-odbudowa-wedlug-justyniana)) |
| Karolina Terienak    | 2 | ((210928-wysadzony-zywy-scigacz; 211123-odbudowa-wedlug-justyniana)) |
| Marysia Sowińska     | 2 | ((210928-wysadzony-zywy-scigacz; 211123-odbudowa-wedlug-justyniana)) |
| Rafał Torszecki      | 2 | ((210928-wysadzony-zywy-scigacz; 211123-odbudowa-wedlug-justyniana)) |
| Daniel Terienak      | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Franek Bulterier     | 1 | ((210928-wysadzony-zywy-scigacz)) |
| Ignacy Myrczek       | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Justynian Diakon     | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Karol Pustak         | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Keira Amarco d'Namertel | 1 | ((210928-wysadzony-zywy-scigacz)) |
| Żorż d'Namertel      | 1 | ((210928-wysadzony-zywy-scigacz)) |