---
categories: profile
factions: 
owner: public
title: Lara Kiriczko
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221004-samotna-w-programie-advancer | kapral Orbitera dowodząca oddziałem treningowym advancerów. Chciała zintegrować Elenę z resztą drużyny i nawet wydała odpowiednie rozkazy, ale się spóźniła - Elena miała erupcję. Przeniosła Elenę pod kogoś innego. | 0099-05-20 - 0099-05-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Elena Verlen         | 1 | ((221004-samotna-w-programie-advancer)) |
| Hubert Mirsz         | 1 | ((221004-samotna-w-programie-advancer)) |
| Kacper Wentel        | 1 | ((221004-samotna-w-programie-advancer)) |
| Michał Warkoczak     | 1 | ((221004-samotna-w-programie-advancer)) |
| OO Karsztarin        | 1 | ((221004-samotna-w-programie-advancer)) |
| Sandra Kantarelo     | 1 | ((221004-samotna-w-programie-advancer)) |
| Tymon Krakdacz       | 1 | ((221004-samotna-w-programie-advancer)) |