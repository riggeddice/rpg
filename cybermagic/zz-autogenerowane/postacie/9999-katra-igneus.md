---
categories: profile
factions: 
owner: public
title: Katra Igneus
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201210-pocalunek-aspirii            | noktiańska przełożona stacji medyczno-naprawczej w Aspirii; na przestrzeni wojen etnicznych stała się rdzeniem AK Wyjec. | 0111-03-26 - 0111-03-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Nocna Krypta      | 1 | ((201210-pocalunek-aspirii)) |
| AK Wyjec             | 1 | ((201210-pocalunek-aspirii)) |
| Anastazja Sowińska   | 1 | ((201210-pocalunek-aspirii)) |
| Arianna Verlen       | 1 | ((201210-pocalunek-aspirii)) |
| Donald Parziarz      | 1 | ((201210-pocalunek-aspirii)) |
| Eustachy Korkoran    | 1 | ((201210-pocalunek-aspirii)) |
| Juliusz Sowiński     | 1 | ((201210-pocalunek-aspirii)) |
| Klaudia Stryk        | 1 | ((201210-pocalunek-aspirii)) |
| OA Zguba Tytanów     | 1 | ((201210-pocalunek-aspirii)) |
| OO Infernia          | 1 | ((201210-pocalunek-aspirii)) |