---
categories: profile
factions: 
owner: public
title: Wojciech Kuszar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191025-nocna-krypta-i-bohaterka     | oficer Orbitera podlegający Ariannie Verlen, którego rodzina dokonała zbrodni wojennych (zarejestrowane w "Nocnej Krypcie"). To jego działania obudziły Helenę na pokładzie Krypty i, fundamentalnie, doprowadziły prawie do zniszczenia całej załogi Orbitera próbującej uratować ocalałych na Krypcie. | 0108-05-06 - 0108-05-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Nocna Krypta      | 1 | ((191025-nocna-krypta-i-bohaterka)) |
| Arianna Verlen       | 1 | ((191025-nocna-krypta-i-bohaterka)) |
| Kamil Lyraczek       | 1 | ((191025-nocna-krypta-i-bohaterka)) |