---
categories: profile
factions: 
owner: public
title: Ailira Niiris
---

# {{ page.title }}


# Generated: 



## Fiszki


* tanio sprzedaje surowce na Nonarionie (adastratka) (34 lata) | @ 221111-niebezpieczna-woda-na-hiyori
* ENCAO:  0-0+- | Stoicka;;Mówi jak jest;;Dobrze wychowana| VALS: Face >> Humility| DRIVE: Odbudowa i odnowa | @ 221111-niebezpieczna-woda-na-hiyori

### Wątki


historia_darii
salvagerzy-anomalii-kolapsu
syndykat-aureliona-w-anomalii-kolapsu

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221111-niebezpieczna-woda-na-hiyori | (adastratka | ENCAO: 0-0+- | Stoicka;;Mówi jak jest;;Dobrze wychowana| VALS: Face >> Humility| DRIVE: Odbudowa i odnowa) jedna ze sprzedawczyń wody na Nonarionie; sprzedała niebezpieczną wodę Hiyori. Zmartwiło ją to; jak się dowiedziała, pobiegła do kapitanatu poinformować jakie jednostki mogą mieć problemy. | 0090-06-17 - 0090-06-21 |
| 221113-ailira-niezalezna-handlarka-woda | KIA. Pobita; przyznała Idze, że nie miała wyboru dając im i innym jednostkom ten lód. Nie poradziła sobie psychicznie z tym co zrobiła i z potencjalnym zniszczeniem serii statków kosmicznych z jej winy. Popełniła samobójstwo sprzedając swoje ciało sarderytom z Modułu Remedianin i przekazując pieniądze ofiarom i rodzinie. | 0090-06-22 - 0090-06-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Daria Czarnewik      | 2 | ((221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Iga Mikikot          | 2 | ((221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Jakub Uprzężnik      | 2 | ((221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Kaspian Certisarius  | 2 | ((221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Leo Kasztop          | 2 | ((221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Nastia Barbatov      | 2 | ((221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Patryk Lapszyn       | 2 | ((221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Safira d'Hiyori      | 2 | ((221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Filip Szukurkor      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Julia Karnit         | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Ludwik Trójkadur     | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Ogden Barbatov       | 1 | ((221111-niebezpieczna-woda-na-hiyori)) |
| SCA Hiyori           | 1 | ((221111-niebezpieczna-woda-na-hiyori)) |