---
categories: profile
factions: 
owner: public
title: Nadja Kilmodrian
---

# {{ page.title }}


# Generated: 



## Fiszki


* security_2, (sybrianka) | @ 221220-dezerter-z-mrocznego-wolu
* ENCAO:  -000+ |Mistrz improwizacji| VALS: Power, Hedonism >> Conformity| DRIVE: Wygrać w rywalizacji; jest najlepsza z security, odzyskać nadzieję | @ 221220-dezerter-z-mrocznego-wolu

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221220-dezerter-z-mrocznego-wolu    | p.o. szefa ochrony, sybrianka; chroni dobre imię szefa ochrony (bo by nie uciekł) mając chrapkę na jego miejsce. Wyraźnie nie lubi noktiańskiego advancera. Próbowała awansować przez łóżko, źle trafiła i wylądowała na Wole. Dość agresywna, ale nie jest najbystrzejsza ani szczególnie lojalna. Acz pracowita i solidna. | 0111-11-15 - 0111-11-18 |
| 230103-protomag-z-mrocznego-wolu    | wyraźnie znęca się nad Krzysztofem. Bo ten koktajl wstydu, strachu i pożądania jej smakuje jeszcze bardziej. | 0111-11-19 - 0111-11-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adela Myrias         | 2 | ((221220-dezerter-z-mrocznego-wolu; 230103-protomag-z-mrocznego-wolu)) |
| Juliusz Akramantanis | 2 | ((221220-dezerter-z-mrocznego-wolu; 230103-protomag-z-mrocznego-wolu)) |
| Marianna Atrain      | 2 | ((221220-dezerter-z-mrocznego-wolu; 230103-protomag-z-mrocznego-wolu)) |
| OO Mroczny Wół       | 2 | ((221220-dezerter-z-mrocznego-wolu; 230103-protomag-z-mrocznego-wolu)) |
| Baltazar Kwarcyk     | 1 | ((221220-dezerter-z-mrocznego-wolu)) |
| Krzysztof Workisz    | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Lena Ifirentis       | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Maja Szewieczak      | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Nela Kaltaner        | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Robert Warłomkacz    | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Tytus Muszczak       | 1 | ((221220-dezerter-z-mrocznego-wolu)) |