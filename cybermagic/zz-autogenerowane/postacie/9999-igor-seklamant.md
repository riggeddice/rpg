---
categories: profile
factions: 
owner: public
title: Igor Seklamant
---

# {{ page.title }}


# Generated: 



## Fiszki


* ekspert od walki wręcz w ograniczonym terenie, dewastator przełamania, koleś od zastraszania i porządku. | @ 230720-savaranie-przed-obliczem-nihilusa
* OCEAN: (E+N-A-): Dominujący, agresywny i kłótliwy, ale lojalny dla swoich. "Moim nic nie będzie. Ty - słuchaj poleceń." | @ 230720-savaranie-przed-obliczem-nihilusa
* VALS: (Face, Family): Ważny dla niego jest respekt, siła i ekipa - zwłaszcza starszy brat. "Najpierw pokazują brzuszek, potem odpowiadają na pytania. W tej kolejności." | @ 230720-savaranie-przed-obliczem-nihilusa
* Core Wound - Lie: "Jak tylko się odwracał, kocili jego siostrę" - "Jeśli sterroryzuje wszystkich, NIKT nie odważy się dotknąć jego bliskich" | @ 230720-savaranie-przed-obliczem-nihilusa
* styl: Wybuchowy i agresywny, ale to kontroluje; to strategia a nie natura. Ryzykuje dla przyjaciół. "Koniec gadania, czas dać w mordę." | @ 230720-savaranie-przed-obliczem-nihilusa

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230720-savaranie-przed-obliczem-nihilusa | chronił swoich jak umiał. Gdy Rafała gonił anomalny Pacyfikator, wsadził go do skrzyni i wywalił ją poza śluzę. Gdy uznał że problemem jest Ralf i Arnold, wsadził ich na wahadłowiec i kazał wysadzić. Działał szybko i zdecydowanie - i bezwzględnie. | 0092-07-01 - 0092-07-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Agnieszka Serkis     | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Arnold Tapszecz      | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Jola Seklamant       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| JRN Kantala Ravis    | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Katrina Kirten       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Kiran Ravis          | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Lena Morazik         | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Marcelin Viirdus     | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Rafał Kurrodis       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Ralena Annitas       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Ralf Tapszecz        | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |