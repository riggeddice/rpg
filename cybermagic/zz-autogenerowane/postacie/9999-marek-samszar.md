---
categories: profile
factions: 
owner: public
title: Marek Samszar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210615-skradziony-kot-olgi          | Rekin. Chciał zrobić dziewczynie (Arkadii) piękny prezent i zwinął "nieważnym lokalsom" somnibela dla Arkadii. Potem chciał lecieć na karaoke i skończył w starciu z Lilianą. Chciał eskalować Justynianem Diakonem, ale skończył pobity przez Arkadię za kradzież. | 0111-05-13 - 0111-05-15 |
| 210622-verlenka-na-grzybkach        | niewinny, ale wziął na siebie winę (przejął od Arkadii Lancera), by Arkadia mogła szukać bezpiecznie tego co ją otruł grzybkami. | 0111-05-30 - 0111-05-31 |
| 211102-satarail-pomaga-marysi       | zainfekowany Owadem Sataraila (za kradzież somnibela Olgi) został przekształcony w potwora. Przejął grupę Rekinów i zaczął dewastację Dzielnicy. Zatrzymany przez użądlenie ze strony Marysi i wielką glistę Sensacjusza. Skończył w szpitalu terminuskim w Pustogorze. | 0111-08-09 - 0111-08-10 |
| 240305-lea-strazniczka-lasu         | próbował pomóc Gwiazdociszowi w sprawie badań z czasów wojny. Ucierpiała przez EfemeHorror. Wyśmiała go Arkadia, że nie dał rady. | 0111-10-21 - 0111-10-25 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210615-skradziony-kot-olgi          | Arkadia Verlen z nim zerwała i go solidnie pobiła za kradzież somnibela Oldze Myszeczce. | 0111-05-15
| 211102-satarail-pomaga-marysi       | 3 miesiące rekonstrukcji w Szpitalu Terminuskim w Pustogorze. To jest cena za stanięcie na drodze Wiktorowi Satarailowi. | 0111-08-10

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 4 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 211102-satarail-pomaga-marysi; 240305-lea-strazniczka-lasu)) |
| Arkadia Verlen       | 3 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 240305-lea-strazniczka-lasu)) |
| Ernest Namertel      | 2 | ((211102-satarail-pomaga-marysi; 240305-lea-strazniczka-lasu)) |
| Julia Kardolin       | 2 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach)) |
| Karolina Terienak    | 2 | ((211102-satarail-pomaga-marysi; 240305-lea-strazniczka-lasu)) |
| Liliana Bankierz     | 2 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach)) |
| Rafał Torszecki      | 2 | ((210622-verlenka-na-grzybkach; 211102-satarail-pomaga-marysi)) |
| Triana Porzecznik    | 2 | ((210622-verlenka-na-grzybkach; 240305-lea-strazniczka-lasu)) |
| Wiktor Satarail      | 2 | ((210615-skradziony-kot-olgi; 211102-satarail-pomaga-marysi)) |
| Bogdan Gwiazdocisz   | 1 | ((240305-lea-strazniczka-lasu)) |
| Ignacy Myrczek       | 1 | ((210622-verlenka-na-grzybkach)) |
| Keira Amarco d'Namertel | 1 | ((240305-lea-strazniczka-lasu)) |
| Lea Samszar          | 1 | ((240305-lea-strazniczka-lasu)) |
| Lorena Gwozdnik      | 1 | ((211102-satarail-pomaga-marysi)) |
| Malena Barandis      | 1 | ((240305-lea-strazniczka-lasu)) |
| Mariusz Kupieczka    | 1 | ((240305-lea-strazniczka-lasu)) |
| Michał Klabacz       | 1 | ((240305-lea-strazniczka-lasu)) |
| Olga Myszeczka       | 1 | ((210615-skradziony-kot-olgi)) |
| Paweł Szprotka       | 1 | ((210615-skradziony-kot-olgi)) |
| Różewicz Diakon      | 1 | ((210622-verlenka-na-grzybkach)) |
| Sensacjusz Diakon    | 1 | ((211102-satarail-pomaga-marysi)) |
| Urszula Miłkowicz    | 1 | ((210622-verlenka-na-grzybkach)) |