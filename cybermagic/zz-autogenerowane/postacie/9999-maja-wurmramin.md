---
categories: profile
factions: 
owner: public
title: Maja Wurmramin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230618-reality-show-z-zaskoczenia   | Aurum, botanistka uninterested in everything BUT. Ekspert od jadalności i roślin, kula u nogi przy wszystkim innym. W każdym z dwóch biomów znała się na roślinach i widziała, że coś nie pasuje. | 0083-10-02 - 0083-10-07 |
| 230625-przegrywy-ratuja-przegrywy   | żywy dowód, że ten zespół sobie dobrze radzi (bo ona żyje). Dodatkowo, poznajdowała cenne zioła i rzeczy. | 0083-10-08 - 0083-10-11 |
| 230626-pustynny-final-igrzysk       | jako element pułapki głośno woła SOS; jakkolwiek w walce nieprzydatna, ale świetnie udowadnia że to nie jest pułapka. | 0083-10-12 - 0083-10-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Klart           | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Lily Sanarton        | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Maks Ardyceń         | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Olaf Zuchwały        | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Roman Wyrkmycz       | 3 | ((230618-reality-show-z-zaskoczenia; 230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Antoni Kmandir       | 2 | ((230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Bogdan Ubuddan       | 2 | ((230625-przegrywy-ratuja-przegrywy; 230626-pustynny-final-igrzysk)) |
| Jan Firatiel         | 1 | ((230626-pustynny-final-igrzysk)) |
| Rafał Armadion       | 1 | ((230626-pustynny-final-igrzysk)) |