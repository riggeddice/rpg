---
categories: profile
factions: 
owner: public
title: OO Tucznik Trzeci
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221026-kapitan-verlen-i-koniec-przygody-na-krolowej | transport pomiędzy Aurum a Orbiterem, służy do przemytu na korzyść obu stron. Arnulf Perikas ma tam bliskie kontakty. | 0100-06-08 - 0100-06-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Kramer        | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Arianna Verlen       | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Arnulf Perikas       | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Daria Czarnewik      | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Erwin Pies           | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Klaudiusz Terienak   | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Leszek Kurzmin       | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| OO Królowa Kosmicznej Chwały | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Stefan Torkil        | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Tomasz Ruppok        | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |