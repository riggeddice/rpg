---
categories: profile
factions: 
owner: public
title: Lena Ifirentis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230103-protomag-z-mrocznego-wolu    | agentka Orbitera która zajmuje się wprowadzaniem noktian do pożytecznych prac a Juliusz obniża jej success rate. Prosi Adelę, by ta znalazła coś za co można go przyskrzynić. To ona stoi za tym, by Wincenty tępił Nelę by Juliusz wybuchł; nie spodziewała się protomaga na pokładzie. Po wszystkim dostała reprymendę i ją przenieśli i Juliusz nie jest już pod nią. | 0111-11-19 - 0111-11-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adela Myrias         | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Juliusz Akramantanis | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Krzysztof Workisz    | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Maja Szewieczak      | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Marianna Atrain      | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Nadja Kilmodrian     | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Nela Kaltaner        | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| OO Mroczny Wół       | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Robert Warłomkacz    | 1 | ((230103-protomag-z-mrocznego-wolu)) |