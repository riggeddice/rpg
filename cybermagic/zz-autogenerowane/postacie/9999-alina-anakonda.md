---
categories: profile
factions: 
owner: public
title: Alina Anakonda
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200226-strazniczka-przez-lzy        | badaczka otchłani i lekarz operująca w Szpitalu Terminuskim; kontrolowała wysokie energie i udało jej się uratować życie Alicji (zanim ta zginęła). Konfliktowała się z Lucjuszem. Wybitna partnerka (w pracy, nie prywatnie) Lucjusza. | 0110-07-29 - 0110-08-01 |
| 200510-tajna-baza-orbitera          | główny lekarz w Szpitalu Terminuskim (odkąd zabrakło Blakenbauera); nie zgadza się, by Tadeusz Tessalon opuścił szpital, bo to mu zagrozi. Włączyła w akcję Pięknotkę. | 0110-09-07 - 0110-09-11 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 200226-strazniczka-przez-lzy        | zastępuje Lucjusza jako specjalistka od trudnych przypadków. Pustogor wie, że jej może ufać i że jest wystarczająco bezwzględna. | 0110-08-01

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alicja Kiermacz      | 1 | ((200226-strazniczka-przez-lzy)) |
| Artur Kołczond       | 1 | ((200510-tajna-baza-orbitera)) |
| Crystal d'Corieris   | 1 | ((200226-strazniczka-przez-lzy)) |
| Darek Ampieczak      | 1 | ((200226-strazniczka-przez-lzy)) |
| Eliza Farnorz        | 1 | ((200226-strazniczka-przez-lzy)) |
| Gabriel Ursus        | 1 | ((200510-tajna-baza-orbitera)) |
| Ignacy Myrczek       | 1 | ((200510-tajna-baza-orbitera)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Kastor Miczokan      | 1 | ((200226-strazniczka-przez-lzy)) |
| Laura Tesinik        | 1 | ((200510-tajna-baza-orbitera)) |
| Lucjusz Blakenbauer  | 1 | ((200226-strazniczka-przez-lzy)) |
| Mariusz Trzewń       | 1 | ((200510-tajna-baza-orbitera)) |
| Natalia Tessalon     | 1 | ((200510-tajna-baza-orbitera)) |
| Pięknotka Diakon     | 1 | ((200510-tajna-baza-orbitera)) |
| Rafał Muczor         | 1 | ((200226-strazniczka-przez-lzy)) |
| Sabina Kazitan       | 1 | ((200510-tajna-baza-orbitera)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Teresa Mieralit      | 1 | ((200510-tajna-baza-orbitera)) |
| Tymon Grubosz        | 1 | ((200510-tajna-baza-orbitera)) |