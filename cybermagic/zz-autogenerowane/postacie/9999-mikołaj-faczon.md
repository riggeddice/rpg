---
categories: profile
factions: 
owner: public
title: Mikołaj Faczon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220518-okrutna-wrona-kalcynici-i-koszmary | eks-pirat z Okrutnej Wrony; szowinista który jednak słucha Mai bo Berdysz kazał. Doskonały strzelec; osłaniał Berdysza, gdy ten chronił silniki Węża przed kalcynitami. | 0108-10-09 - 0108-10-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Falkam          | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Amanda Korel         | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Antoni Krutacz       | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Berdysz Rozdzieracz  | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Elwira Piscernik     | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Filip Gościc         | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Kornelia Sanoros     | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Ksawery Janowar      | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Lila Cziras          | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Maja Kormoran        | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Pola Mornak          | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |