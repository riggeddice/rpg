---
categories: profile
factions: 
owner: public
title: Serenit
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190802-statek-zjada-statki          | statek-asteroida (RAMA), kontrolowany przez saitaero-kralotycznego Overminda, asymilujący statki i integrujący ze sobą magów z AI. Skrajnie niebezpieczny. | 0087-08-09 - 0087-08-12 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190802-statek-zjada-statki          | po asymilacji straina Evy stał się groźniejszy, bo zintegrował inny ułamek energii Saitaera. | 0087-08-12

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aida Serenit         | 1 | ((190802-statek-zjada-statki)) |
| Eva d'Mikado         | 1 | ((190802-statek-zjada-statki)) |
| Ignaś Orbita         | 1 | ((190802-statek-zjada-statki)) |
| Laura Prunal         | 1 | ((190802-statek-zjada-statki)) |
| Michał Dusiciel      | 1 | ((190802-statek-zjada-statki)) |
| Mikado               | 1 | ((190802-statek-zjada-statki)) |
| Sia d'Cranis         | 1 | ((190802-statek-zjada-statki)) |
| Stella Koral         | 1 | ((190802-statek-zjada-statki)) |
| Travis Longhorn      | 1 | ((190802-statek-zjada-statki)) |