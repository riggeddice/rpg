---
categories: profile
factions: 
owner: public
title: Marcel Kulgard
---

# {{ page.title }}


# Generated: 



## Fiszki


* ENCAO:  000-+ |Demotywuje innych;;Innowacyjny| VALS: Achievement, Face| DRIVE: Zbudować legacy | @ 221116-astralna-flara-dociera-do-nonariona-nadziei
* marine, Orbiter, stary znajomy Arianny | @ 221123-egzotyczna-pieknosc-na-astralnej-flarze

### Wątki


historia-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221116-astralna-flara-dociera-do-nonariona-nadziei | (ENCAO:  000-+ |Demotywuje innych;;Innowacyjny| VALS: Achievement, Face| DRIVE: Zbudować legacy) marine, Arianna go zna z czasów akademii. | 0100-09-08 - 0100-09-11 |
| 221123-egzotyczna-pieknosc-na-astralnej-flarze | miał stint w policji militarnej; przesłuchał niewolnika z Orbitera dla Arianny (odzyskanego z Hadiah Emas) i doszedł do tego że ten jest dezerterem. Brutalnie ale beznamiętnie. Dobry, kompetentny marine Orbitera i przyjaciel Arianny. | 0100-09-12 - 0100-09-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Arnulf Perikas       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Daria Czarnewik      | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gabriel Lodowiec     | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Kajetan Kircznik     | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Leszek Kurzmin       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| OO Astralna Flara    | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| OO Athamarein        | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| SCA Hadiah Emas      | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Władawiec Diakon     | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Alezja Dumorin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Ellarina Samarintael | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Erwin Pies           | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gerwazy Kircznik     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Grażyna Burgacz      | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Leo Kasztop          | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Maja Samszar         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Szczepan Myrczek     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Szymon Wanad         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Tomasz Ruppok        | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |