---
categories: profile
factions: 
owner: public
title: Kacper Wentel
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221004-samotna-w-programie-advancer | advancer-technik, żartowniś i zapalony gracz w 'Trzy Diamenty' (gra kościana); wyjątkowo nie lubi Eleny i gdy dowodził operacją to ją sprowokował do utraty kontroli i magicznego wiru. | 0099-05-20 - 0099-05-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Elena Verlen         | 1 | ((221004-samotna-w-programie-advancer)) |
| Hubert Mirsz         | 1 | ((221004-samotna-w-programie-advancer)) |
| Lara Kiriczko        | 1 | ((221004-samotna-w-programie-advancer)) |
| Michał Warkoczak     | 1 | ((221004-samotna-w-programie-advancer)) |
| OO Karsztarin        | 1 | ((221004-samotna-w-programie-advancer)) |
| Sandra Kantarelo     | 1 | ((221004-samotna-w-programie-advancer)) |
| Tymon Krakdacz       | 1 | ((221004-samotna-w-programie-advancer)) |