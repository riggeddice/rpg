---
categories: profile
factions: 
owner: public
title: Paweł Pieprz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210912-medyk-przeciwko-cieniaszczytowi | wciągnął Martyna Hiwassera do Rozpruwaczy; przyszedł do Martyna by ten pomógł mu uratować siostrę. Pomógł Martynowi z hasłami siostry i ogólnymi działaniami - ale jego siostra zginęła. Wini Martyna. Od teraz - śmiertelny wróg Martyna. | 0079-09-13 - 0079-09-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adela Kołczan        | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Adrian Kozioł        | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Franciszek Tocz      | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Irena Czakram        | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Kasia Pieprz         | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Martyn Hiwasser      | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |