---
categories: profile
factions: 
owner: public
title: Fantazjusz Verlen
---

# {{ page.title }}


# Generated: 



## Fiszki


* mag, sybrianin, hodowca świń w Verlenlandzie | @ 230419-karolinka-nieokielznana-swinka
* (ENCAO: -0+0+ | Wszystko przemyśli kilka razy;;Kierowany obowiązkiem i powinnością| VALS: Self-direction, Family| DRIVE: Follow My Dreams) | @ 230419-karolinka-nieokielznana-swinka
* styl: Haviland Tuf | @ 230419-karolinka-nieokielznana-swinka
* Zero włosów na ciele, dwumetrowa góra. Brzuchacz. Jeździ na świni, która nazywa się "Grubiszonek". Klasyczny zapach świń. | @ 230419-karolinka-nieokielznana-swinka
* Nie każdy ma jako 'dream' założenie farmy nienaturalnych świń w Verlenlandzie ze wszystkich miejsc, ale Fantazjusz nie jest 'kimkolwiek'. | @ 230419-karolinka-nieokielznana-swinka

### Wątki


karolinus-heart-viorika
przeznaczenie-eleny-verlen
hiperpsychotronika-samszarow

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230419-karolinka-nieokielznana-swinka | 41 lat, twórca kawalerii - świnnicy. Wprowadził dla Marcinozaura świnkę Karolinkę do irytowania Karolinusa. | 0095-07-19 - 0095-07-23 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksander Samszar   | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Apollo Verlen        | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Arianna Verlen       | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Elena Verlen         | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Marcinozaur Verlen   | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Tymek Samszar        | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Ula Blakenbauer      | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Viorika Verlen       | 1 | ((230419-karolinka-nieokielznana-swinka)) |