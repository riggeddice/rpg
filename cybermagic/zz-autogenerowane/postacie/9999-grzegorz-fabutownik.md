---
categories: profile
factions: 
owner: public
title: Grzegorz Fabutownik
---

# {{ page.title }}


# Generated: 



## Fiszki


* górnik w przestrzeni Blakvela | @ 231027-planetoida-bogatsza-niz-byc-powinna
* OCEAN: E+A- |Zawsze szuka adrenalinki i kasy;; Nie dba o innych, może kraść | VALS: Power, Hedonism | DRIVE: Kasa i wszystkie laski jego | @ 231027-planetoida-bogatsza-niz-byc-powinna

### Wątki


planetoidy-kazimierza
sekret-mirandy-ceres

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231027-planetoida-bogatsza-niz-byc-powinna | górnik Blakvelowców. Narzeka na kasę, łasy na kasę, pragnie kasy dla siebie, antagonizuje wszystkich innych i jak widzi okazję na łatwy zarobek, bierze dla siebie. Napotkał jajo Alterienta i na nim się wykluło. KIA. | 0108-06-29 - 0108-07-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Bartek Burbundow     | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Gotard Kicjusz       | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Juliusz Cieślawok    | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Kazimierz Zamglis    | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Łucjan Torwold       | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Mikołaj Resztkowiec  | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Miranda Ceres        | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Prokop Umarkon       | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| SC Królowa Przygód   | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Seweryn Grzęźlik     | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |