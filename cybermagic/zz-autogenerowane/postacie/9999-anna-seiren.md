---
categories: profile
factions: 
owner: public
title: Anna Seiren
---

# {{ page.title }}


# Generated: 



## Fiszki


* salvager Serien (owner, analityczka / radar / hacker) faeril: mechaniczny ptakozwierz na którym jej zależy, life is pain work is life | @ 230104-to-co-zostalo-po-burzy
* (ENCAO: -00+-, Nie lubi ryzyka + Altruistyczny, TAK: face + achi, NIE: Self-direction, Strach przed zapomnieniem) | @ 230104-to-co-zostalo-po-burzy
* "Walczmy o lepszy świat! Jak długo unikniemy farighanów, jesteśmy w stanie zbudować własne małe sukcesy!" | @ 230104-to-co-zostalo-po-burzy
* kadencja: optymistyczny głos typu Sisko | @ 230104-to-co-zostalo-po-burzy

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230104-to-co-zostalo-po-burzy       | posiadaczka mechanicznego ptaka; porwana przez Antoniego by pomogła mu odzyskać i uratować Kamilę. | 0092-10-26 - 0092-10-28 |
| 230125-whispraith-w-jaskiniach-neikatis | częściowo katatoniczna przez działania i ekstrakcję whispraitha; przynęta. Zofia ją wyciągnęła ryzykując życie swoje i zespołu. | 0092-10-29 - 0092-11-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aniela Myszawcowa    | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Antoni Grzypf        | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Cyprian Kugrak       | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Eustachy Korkoran    | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| JAN Seiren           | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| JAN Uśmiech Kamili   | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Michał Uszwon        | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Rufus Seiren         | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Zofia d'Seiren       | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Kalia Awiter         | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Kornelia Lichitis    | 1 | ((230125-whispraith-w-jaskiniach-neikatis)) |
| Rafał Kidiron        | 1 | ((230104-to-co-zostalo-po-burzy)) |