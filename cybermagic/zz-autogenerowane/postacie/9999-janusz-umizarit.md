---
categories: profile
factions: 
owner: public
title: Janusz Umizarit
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240227-cykl-krwawej-przadki         | pracownik Instytutu, który staje się ofiarą Róży i jej mocy. Ma długi. KIA (pożarty przez Prządkę). | 0099-05-22 - 0099-05-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Wokniaczek      | 1 | ((240227-cykl-krwawej-przadki)) |
| Edward Kopoktris     | 1 | ((240227-cykl-krwawej-przadki)) |
| Jola Kalatrix        | 1 | ((240227-cykl-krwawej-przadki)) |
| Lucyna Castelli      | 1 | ((240227-cykl-krwawej-przadki)) |
| Michał Castelli      | 1 | ((240227-cykl-krwawej-przadki)) |
| Róża Kalatrix        | 1 | ((240227-cykl-krwawej-przadki)) |
| Wincenty Frak        | 1 | ((240227-cykl-krwawej-przadki)) |