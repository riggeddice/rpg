---
categories: profile
factions: 
owner: public
title: Gerald Barowiecki
---

# {{ page.title }}


# Generated: 



## Fiszki


* Kapitan i sponsor Kelpie | @ 240117-echo-z-odmetow-przeszlosci
* Kapitan spełnia swoje marzenie o podmorskiej przygodzie. Bardzo poczciwy człowiek, chce pomóc, ale wie, że nie jest specjalistom, wycofa się jeśli zobaczy taką potrzebę. | @ 240117-echo-z-odmetow-przeszlosci

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240117-echo-z-odmetow-przeszlosci   | identyfikuje niedomówienia oraz nie do końca czyste intencje w słowach oficerów i wybiera najbardziej moralną opcję z proponowanych, traci zaufanie do inżyniera i oficera ochrony, przez co nie zauważa, że jego dyplomata może zadziałać wbrew niemu. Nie wiem o szczegółach układu z bazą i uważa, że postąpił słusznie. | 0096-01-24 - 0096-01-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anna Ivanova         | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Elżbieta Sanchez     | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Jędrzej Sanchez      | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Klasa Dyplomata      | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Klasa Inżynier       | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Klasa Oficer Ochrony | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Robert Tisso         | 1 | ((240117-echo-z-odmetow-przeszlosci)) |