---
categories: profile
factions: 
owner: public
title: Adam Wudrak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220525-dzieci-z-arinkarii-odpychaja-piratow | 36, zmodyfikowany drakolicko operator maszyn ciężkich i górnik; rozpieszcza swojego syna (Bartka) i zmontował mu "Świątynie Wrestlera Tytana" dyskretnie w nieaktywnym już fragmencie Arinkarii. | 0105-05-14 - 0105-05-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Albert Rybowąż       | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Bartek Wudrak        | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Bruno Wesper         | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Kaja Czmuch          | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Koralina Szprot      | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Romana Kundel        | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |