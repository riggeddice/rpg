---
categories: profile
factions: 
owner: public
title: Natalia Tessalon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200509-rekin-z-aurum-i-fortifarma   | dla brata zażądała od Sabiny Kazitan niebezpieczny rytuał. Dostała go, przekazała bratu. Teraz ma wyrzuty sumienia - jej brat jest "zniszczony". | 0110-08-30 - 0110-09-04 |
| 200510-tajna-baza-orbitera          | zrozpaczona przez stan brata; nawiązała kontakt z bazą Irrydius i postanowiła przenieść brata do Aurum. Wini Sabinę oraz jest wroga Szczelińcowi za wszystko. | 0110-09-07 - 0110-09-11 |
| 211207-gdy-zabraknie-pradu-rekinom  | NIE JEST REKINEM. By pomóc bratu, współpracuje z mafią (Wolny Uśmiech). Chciała wciągnąć Rekiny do brania prądu od mafii, ale Arkadia skutecznie to zablokowała. Przerażona Arkadią. | 0111-08-30 - 0111-08-31 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 200509-rekin-z-aurum-i-fortifarma   | eks Gabriela Ursusa. Pokłóciła się z nim solidnie i go rzuciła. | 0110-09-04

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Artur Kołczond       | 2 | ((200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera)) |
| Gabriel Ursus        | 2 | ((200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera)) |
| Pięknotka Diakon     | 2 | ((200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera)) |
| Sabina Kazitan       | 2 | ((200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera)) |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Arkadia Verlen       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Arnold Kłaczek       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Artur Michasiewicz   | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Daniel Terienak      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Erwin Galilien       | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Henryk Wkrąż         | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Hestia d'Rekiny      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Ignacy Myrczek       | 1 | ((200510-tajna-baza-orbitera)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Karolina Terienak    | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Laura Tesinik        | 1 | ((200510-tajna-baza-orbitera)) |
| Lorena Gwozdnik      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Mariusz Trzewń       | 1 | ((200510-tajna-baza-orbitera)) |
| Marysia Sowińska     | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Sensacjusz Diakon    | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Tadeusz Tessalon     | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Teresa Mieralit      | 1 | ((200510-tajna-baza-orbitera)) |
| Tymon Grubosz        | 1 | ((200510-tajna-baza-orbitera)) |
| Urszula Arienik      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |