---
categories: profile
factions: 
owner: public
title: Klarysa Jirnik
---

# {{ page.title }}


# Generated: 



## Fiszki


* artillery, (p.o. Mai jak Maja nie może) | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* ENCAO:  00+-- |Wymagający;;Małostkowy| VALS: Tradition, Conformity|DRIVE: Sprawiedliwość | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej

### Wątki


kosmiczna-chwala-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly | artylerzystka i odpowiedzialna za broń na Królowej; chwilowo nie ma czym strzelać. Z ciekawością patrzy co się będzie działo. | 0100-05-06 - 0100-05-12 |
| 221102-astralna-flara-i-porwanie-na-karsztarinie | jest niemiła. Ale strzela dobrze. Laser górniczy poważnie uszkodził korwetę przeciwnika. Przy wysokiej rotacji itp. | 0100-07-28 - 0100-08-03 |
| 221130-astralna-flara-w-strefie-duchow | precyzyjny strzał nanowłóknem, skuteczna ewakuacja Huberta i Kirei. | 0100-10-05 - 0100-10-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Arnulf Perikas       | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Daria Czarnewik      | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Maja Samszar         | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Alezja Dumorin       | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Elena Verlen         | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Grażyna Burgacz      | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Hubert Kerwelenios   | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Kajetan Kircznik     | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Szymon Wanad         | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221130-astralna-flara-w-strefie-duchow)) |
| Władawiec Diakon     | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Ada Wyrocznik        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Ellarina Samarintael | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Erwin Pies           | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Gabriel Lodowiec     | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Gerwazy Kircznik     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Kirea Rialirat       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Klaudiusz Terienak   | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Leo Kasztop          | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Leona Astrienko      | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Mariusz Bulterier    | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| NekroTAI Zarralea    | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| OO Astralna Flara    | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| OO Athamarein        | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| OO Karsztarin        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Królowa Kosmicznej Chwały | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Szczepan Myrczek     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Tomasz Dojnicz       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |