---
categories: profile
factions: 
owner: public
title: Antos Kuramin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220405-lepsza-kariera-dla-romki     | zainteresowany Romką erotycznie z nudów. Anna przekonała go, by się dzieciakami trochę poopiekował a nie dzieciaki podrywał. Zgodził się, bo dzieciaki są z odzysku. | 0108-09-15 - 0108-09-17 |
| 220503-antos-szafa-i-statek-piscernikow | zaopiekował się Karą na prośbę Mirandy; miała gdzie spać. Chciał wyjaśnić jej mechanizmy życia, zwłaszcza gdy przybył statek Luxuritias, ale Kara chciała się wymknąć. Więc zamknął ją w szafie. | 0108-09-23 - 0108-09-25 |
| 210820-fecundatis-w-domenie-barana  | "zastępca" Damiana Szczugora; Rick go przekonał, by poprowadził operację obrony Szamunczak przed Strachami. Antos to zrobił - kompetentny zeń żołnierz. Został bohaterem ludowym, uznanym przez Blakvelowców i lokalsów. | 0109-03-06 - 0109-03-09 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220503-antos-szafa-i-statek-piscernikow | sporo paskudnych plotek na jego temat jak to źle traktował Karę gdy miał się nią opiekować. Ma to gdzieś. | 0108-09-25

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anna Szrakt          | 2 | ((220405-lepsza-kariera-dla-romki; 220503-antos-szafa-i-statek-piscernikow)) |
| Damian Szczugor      | 2 | ((210820-fecundatis-w-domenie-barana; 220405-lepsza-kariera-dla-romki)) |
| Miranda Ceres        | 2 | ((220405-lepsza-kariera-dla-romki; 220503-antos-szafa-i-statek-piscernikow)) |
| Antonella Temaris    | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Bartek Wudrak        | 1 | ((220503-antos-szafa-i-statek-piscernikow)) |
| Bruno Baran          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Cień Brighton        | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Deneb Ira            | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Elsa Kułak           | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Flawia Blakenbauer   | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Helena Banbadan      | 1 | ((220405-lepsza-kariera-dla-romki)) |
| Jolanta Sowińska     | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Kara Prazdnik        | 1 | ((220503-antos-szafa-i-statek-piscernikow)) |
| Kara Szamun          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Leon Kantor          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Rick Varias          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Romana Kundel        | 1 | ((220405-lepsza-kariera-dla-romki)) |
| SC Królowa Przygód   | 1 | ((220405-lepsza-kariera-dla-romki)) |
| Tamara Mardius       | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Ursyn Uszat          | 1 | ((220405-lepsza-kariera-dla-romki)) |
| Wojciech Kaznodzieja | 1 | ((220405-lepsza-kariera-dla-romki)) |