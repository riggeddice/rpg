---
categories: profile
factions: 
owner: public
title: Antoni Krutacz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220420-samobojstwo-kapitana-wielkiego-weza | eks-niewolnik piratów jak ryba na rowerze; spec od dron i rozkładania rzeczy na kawałki. STRASZNIE nie ufa Berdyszowi i zarzucił mu, że tak naprawdę to Berdysz zabił kapitana i stoi za potencjalnymi piratami. | 0108-09-26 - 0108-10-02 |
| 220427-dziwne-strachy-w-morzu-ulud  | z Filipem doszedł do tego kim jest Berdysz - morderczy kapitan piratów. Wymyślał serię planów jak się Berdysza pozbyć ze statku - bez szans. Cała ta sytuacja zmieniła go w trwożliwe stworzonko. | 0108-10-05 - 0108-10-07 |
| 220518-okrutna-wrona-kalcynici-i-koszmary | niesamowicie uważny, zauważył że Alan dyskretnie współpracuje z Berdyszem; dzięki jego dronom mają wgląd w sytuację na Wronie. Opętany, zabił Alana i Berdysz założył mu neuroobrożę. BROKEN. | 0108-10-09 - 0108-10-18 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220518-okrutna-wrona-kalcynici-i-koszmary | straszliwa trauma - zabił Alana pod wpływem opętania a potem neuroobroża. | 0108-10-18

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Falkam          | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Berdysz Rozdzieracz  | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Filip Gościc         | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Kornelia Sanoros     | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Lila Cziras          | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Maja Kormoran        | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Pola Mornak          | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Amanda Korel         | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Elwira Piscernik     | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Jerzy Odmiczak       | 1 | ((220420-samobojstwo-kapitana-wielkiego-weza)) |
| Ksawery Janowar      | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Mikołaj Faczon       | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |