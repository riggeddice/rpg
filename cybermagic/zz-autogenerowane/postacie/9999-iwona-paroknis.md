---
categories: profile
factions: 
owner: public
title: Iwona Paroknis
---

# {{ page.title }}


# Generated: 



## Fiszki


* director of the arcology's medical facilities, goes for drama | @ 230329-zdrada-rozrywajaca-arkologie
* ENCAO:  +++-0 |Kłótliwa, lubi spory dla sporów;;Ekstrawagancka, ekspresywna;;Skupiona na logice jako narzędziu decyzyjnym| VALS: Stimulation, Power >> Security| DRIVE: Zrozumieć zagadkę (czemu Renata tak się zachowuje) | @ 230329-zdrada-rozrywajaca-arkologie
* głośna i kłótliwa, to ONA dowodzi | @ 230329-zdrada-rozrywajaca-arkologie

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230329-zdrada-rozrywajaca-arkologie | macocha Eweliny, która czyta pamiętnik Eweliny. Zrobiła opieprz Kasi za "zdrady" Jonasza (co skończyło się pobiciem Jonasza prez Szczepana). Uszkodzona reputacja, rozproszona, zrobiła kilka błędów co sprawiło, że Infernia zmartwiła się że wujka Bartłomieja próbuje Kidiron zabić. | 0093-03-14 - 0093-03-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Ardilla Korkoran     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| BIA Prometeus        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Celina Lertys        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Eustachy Korkoran    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Franciszek Pietraszczyk | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kalia Awiter         | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Małgorzata Maratelus | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| OO Infernia          | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Rafał Kidiron        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Ralf Tapszecz        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Wojciech Grzebawron  | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |