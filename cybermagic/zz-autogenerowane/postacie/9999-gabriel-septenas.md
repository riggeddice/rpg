---
categories: profile
factions: 
owner: public
title: Gabriel Septenas
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa | kiedyś Mawirowiec, ale po tym jak się rzucił uratować savaran i został przez nich krzyżowo uratowany, pod wpływem Fidetis, został Unifikatą. | 0105-12-11 - 0105-12-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aerina Cavalis       | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Dorion Fughar        | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Felina Amatanir      | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Kaldor Czuk          | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Kalista Surilik      | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klasa Dyplomata      | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klasa Inżynier       | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klasa Sabotażysta    | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Mawir Hong           | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Sia-03 Szernief      | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |