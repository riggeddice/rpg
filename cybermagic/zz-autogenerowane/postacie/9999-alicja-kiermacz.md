---
categories: profile
factions: 
owner: public
title: Alicja Kiermacz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190530-porwana-foodfluencerka       | czarodziejka i foodfluencerka; porwana przez swoich fanów była bezradna i zasiało się w niej ziarno Esuriit ze strachu i z rozpaczy. Uratowana przez ludzkich policjantów. | 0110-04-05 - 0110-04-06 |
| 190619-esuriit-w-sercu-alicji       | wydawało się, że się wyleczyła, ale Esuriit ją pochłonęło. W końcu skończyła w Pustogorze po tym, jak zainteresował się nią Kajrat z uwagi na jej unikalne umiejętności. | 0110-04-16 - 0110-04-18 |
| 200226-strazniczka-przez-lzy        | kiedyś foodfluencerka i nastolatka. Teraz - sprzężona z Esuriit, pełni rolę Kajrata jako Strażniczka. Prawie umarła podczas zabiegu; ostra cyborgizacja. Prawie nikt nie wie że żyje. | 0110-07-29 - 0110-08-01 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190530-porwana-foodfluencerka       | została pośmiewiskiem - czarodziejka porwana przez FANÓW i uratowana przez POLICJĘ. Plus, sama sobie to zrobiła (bo chciała uciec z bycia gwiazdą). | 0110-04-06
| 190530-porwana-foodfluencerka       | zagnieździło się w niej Ziarno Esuriit; ma koszmary senne i jest w niej Mrok Esuriit. | 0110-04-06
| 190619-esuriit-w-sercu-alicji       | niechętnie, ale wywieziona z Zaczęstwa do Pustogoru. Tam można jej pomoc z Esuriit. | 0110-04-18
| 200226-strazniczka-przez-lzy        | ostra cyborgizacja (ok. 40%), za czym idzie silne osłabienie mocy magicznej. Traci też kontakt z rodziną i przyjaciółmi. | 0110-08-01

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eliza Farnorz        | 3 | ((190530-porwana-foodfluencerka; 190619-esuriit-w-sercu-alicji; 200226-strazniczka-przez-lzy)) |
| Rafał Muczor         | 2 | ((190619-esuriit-w-sercu-alicji; 200226-strazniczka-przez-lzy)) |
| Alina Anakonda       | 1 | ((200226-strazniczka-przez-lzy)) |
| Crystal d'Corieris   | 1 | ((200226-strazniczka-przez-lzy)) |
| Darek Ampieczak      | 1 | ((200226-strazniczka-przez-lzy)) |
| Jacek Brzytwa        | 1 | ((190530-porwana-foodfluencerka)) |
| Kastor Miczokan      | 1 | ((200226-strazniczka-przez-lzy)) |
| Lucjusz Blakenbauer  | 1 | ((200226-strazniczka-przez-lzy)) |
| Małgorzata Wisus     | 1 | ((190530-porwana-foodfluencerka)) |
| Tymon Grubosz        | 1 | ((190619-esuriit-w-sercu-alicji)) |