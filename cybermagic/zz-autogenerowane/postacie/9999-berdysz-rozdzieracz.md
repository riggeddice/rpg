---
categories: profile
factions: 
owner: public
title: Berdysz Rozdzieracz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220329-mlodociani-i-pirat-na-krolowej | gaulron i kapitan piratów z Pasa Kazimierza. Zinfiltrował Królową Przygód jako ostatni z grupy. Wymanewrował dzieciaki i doprowadził do tego że mając jedynie jednego zakładnika i będąc na nie swoim terenie NADAL odszedł żywy i zdrowy z dwoma członkami załogi. Strasznie niebezpieczny i ciałem i umysłem. Gra ostro. | 0108-08-29 - 0108-09-09 |
| 220420-samobojstwo-kapitana-wielkiego-weza | pełni rolę advancera na Wielkim Wężu; dyskretnie przekonał Maję, by ta przejęła kontrolę nad Wielkim Wężem zamiast Kornelii i że Mai potrzebne jest jego wsparcie. | 0108-09-26 - 0108-10-02 |
| 220427-dziwne-strachy-w-morzu-ulud  | jako advancer doleciał do boi i rozszarpał swoim kombinezonem kalcynita. Jakkolwiek załoga Węża odkryła kim jest i są osoby aktywnie go zwalczające, zapewnił sobie na Wężu stronnictwo silnie stojące za nim. | 0108-10-05 - 0108-10-07 |
| 220518-okrutna-wrona-kalcynici-i-koszmary | wpadł na pintkę, rozwalił kalcynita i wrócił z pintką po wprowadzeniu dron; potem opracował jak wbić na Wronę i wyczyścić kalcynitów. Nie zdążył uratować Alana przed opętanym Antonim ale uratował Filipa i wsadził Antoniemu neuroobrożę. Użył eks-piratów z Wrony do upewnienia się, że sytuacja dalej jest pod jego kontrolą. Taktyka ORAZ masterful strategy. Uratował tyle ile się dało. | 0108-10-09 - 0108-10-18 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220518-okrutna-wrona-kalcynici-i-koszmary | wszyscy (Maja, Lila, Pola, Kornelia, Filip, NAWET Antoni, Ksawery i Mikołaj) uważają go za tego kto przeprowadził ich przez piekło. Ma czystą współpracę z agentami Węża. | 0108-10-18

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Falkam          | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Antoni Krutacz       | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Filip Gościc         | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Kornelia Sanoros     | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Lila Cziras          | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Maja Kormoran        | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Pola Mornak          | 3 | ((220420-samobojstwo-kapitana-wielkiego-weza; 220427-dziwne-strachy-w-morzu-ulud; 220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Amanda Korel         | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Anna Szrakt          | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Bartek Wudrak        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Elwira Piscernik     | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Gotard Kicjusz       | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Helena Banbadan      | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Jerzy Odmiczak       | 1 | ((220420-samobojstwo-kapitana-wielkiego-weza)) |
| Kara Prazdnik        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Ksawery Janowar      | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Łucjan Torwold       | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Mikołaj Faczon       | 1 | ((220518-okrutna-wrona-kalcynici-i-koszmary)) |
| Miranda Ceres        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Prokop Umarkon       | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Romana Kundel        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| SC Królowa Przygód   | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Seweryn Grzęźlik     | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Wojciech Kaznodzieja | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |