---
categories: profile
factions: 
owner: public
title: Ignatius Sozyliw
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: O+A- | "Świat to szereg wyzwań do pokonania; rzadko zważam na emocje innych" | VALS: Achievement, Power | DRIVE: "Moc to moje przeznaczenie, a walka - droga do niej" | @ 231119-tajemnicze-tunele-sebirialis

### Wątki


agencja-lux-umbrarum
problemy-con-szernief

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231119-tajemnicze-tunele-sebirialis | drakolita, zastępca Feliny. Ochrona, bardzo sensowny ale nie ma w nim podstępu ani politycznego intelektu. Cybernetycznie wzmocniony. Lojalny wobec Feliny oraz stacji, pomaga Agencji bo wierzy, że w ten sposób odkryją szybciej jak to rozwiązać. | 0104-11-20 - 0104-11-24 |
| 231221-pan-skarpetek-i-odratowany-ogrod | Skażony przez Alteris (Pan Skarpetek go poprosił), ukrywa dane przed Feliną. Nie walczy z Agencją, nie rozumie czemu oni nie chcą pomóc Skarpetkowi. | 0106-04-25 - 0106-04-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Felina Amatanir      | 2 | ((231119-tajemnicze-tunele-sebirialis; 231221-pan-skarpetek-i-odratowany-ogrod)) |
| Kalista Surilik      | 2 | ((231119-tajemnicze-tunele-sebirialis; 231221-pan-skarpetek-i-odratowany-ogrod)) |
| Klasa Sabotażysta    | 2 | ((231119-tajemnicze-tunele-sebirialis; 231221-pan-skarpetek-i-odratowany-ogrod)) |
| Aniela Kafantelas    | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Eryk Kawanicz        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Janvir Krassus       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Klasa Dyplomata      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Klasa Inżynier       | 1 | ((231221-pan-skarpetek-i-odratowany-ogrod)) |
| Klasa Oficer Naukowy | 1 | ((231221-pan-skarpetek-i-odratowany-ogrod)) |
| Larkus Talvinir      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Leon Hurmniow        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Marta Sarilit        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Mawir Hong           | 1 | ((231221-pan-skarpetek-i-odratowany-ogrod)) |
| OLU Luminarius       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |