---
categories: profile
factions: 
owner: kić
title: Estella Gwozdnik
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "ród gwozdnik"
* owner: "kić"
* title: "Estella Gwozdnik"

## Funkcjonalna mechanika

* **Obietnica**
    * "Będziesz elegancką tienką o niesamowitej spostrzegawczości, dbającą o piękno i uhonorowanie przeszłości taktyką oraz bronią."
* **Rdzeń mechaniczny**
    * **Mistrzostwo (1-3)**
        * GDY jest coś choć trochę dziwnego, anomalnego lub nie pasującego do wzoru, TO od razu zauważa te odchylenia. _Nic nie umyka jej uwagi, zauważa najmniejszy szczegół._
        * NAWET GDY przed chwilą była katastrofa, TO zawsze wygląda i zachowuje się tak nieskazitelnie jak to możliwe. _W jej wypadku perfekcja i elegancja są częścią jej życia a nie mundurem._
    * **Kompetencje (3-5)**
        * GDY jest w sytuacji wojskowej / grupa vs grupa, TO potrafi przewidzieć przeciwnika i go wymanewrować. _Jej skupienie na detalach daje jej nadnaturalne zrozumienie bitwy._
        * GDY działa w zwarciu, TO jest niesamowicie precyzyjna magią, maszyną i ciałem. _Mało kto potrafi trafić w dokładnie ten sam punkt dwa razy._
        * GDY jest w sytuacji bojowej, TO preferuje broń do walki w mieście. _Urban fighter wysokiej klasy; karabin, nóż i pistolet. I servar._
    * **Agenda i styl działania (3-5)**
        * PRAGNIE odbudować Astorię po wojnie, uhonorować poległych wszystkich stron i sprawić, by Astoria była rajem dla ludzi i magów. _Lepsze jutro jest kwiatem nawożonym krwią bohaterów._
        * GDY widzi coś naprawdę pięknego, TO się zatrzyma i spróbuje dowiedzieć się więcej na tego temat. _Zawsze jest czas na kontemplację doskonałości ludzkiej natury._
        * NAWET GDY sytuacja jest bardzo napięta lub pod presją, TO znajdzie chwilę na grawerowanie w stali lub malowanie obrazów, jak długo nie narazi to misji.
        * GDY ma do czynienia z 'martwymi bohaterami' niezależnie od grakcji, TO spróbuje się dowiedzieć jak najwięcej by godnie ich uhonorować. _To odróżnia nas od bestii - poszanowanie historii i poświęceń._
        * GDY ma do czynienia z różnymi frakcjami, TO nie mają one dla niej żadnego znaczenia. Odpowiadają przed historią, nie ludźmi. _Każdy inaczej dochodzi do lepszego jutra, nie mnie oceniać._
        * GDY spotyka się z kimś kto nie dąży do lepszego jutra lub niszczy historię, TO zatrzyma go nawet ryzykując cele poboczne misji. _Jeśli nie zapamiętamy przeszłości, powtórzymy ją w gorszy sposób._
    * **Słabości (1-5)**
        * GDY zagrożone jest coś pięknego lub o dużej wartości historycznej, TO będzie to chronić ponad wartość faktyczną. _Tak można ją wpakować w pułapkę. To jej niezaprzeczalna słabość._
        * GDY spotyka się z 'normalnymi' ludźmi i tienami, TO często jej uduchowienie i perspektywa utrudnia ich dogadanie się. _Jest... nawiedzona, co tu dużo mówić. Jest dobra, ale nawiedzona._
    * **Zasoby (3-5)**
        * Dobrej klasy sprzęt: sprzęt do walki w mieście, servar klasy Lancer itp.
        * Dobre kontakty ze swoim rodem. Mogą się nie zawsze rozumieć, ale się szanują.

## Reszta

### W kilku zdaniach

Elegancko ubrana paragonka dekadiańska władająca precyzyjną magią ognia, która pragnie zachować piękno oraz zapewnić właściwe uhonorowanie przeszłości.

### Jak sterować postacią

* **O czym myśli**
    * "_Każda nasza akcja przynosi korzyści i koszty. Każda z nich wpływa na morale, ciało, umysł i cele. Trzeba zrobić plan. A potem - wykonajmy go i dostosujmy._"
    * "_Martwi bohaterowie wymagają uhonorowania. Nawet po śmierci przydadzą się pokazując drogę następnym generacjom. Przydatni do końca._"
    * "_Piękno jest najważniejsze - pokazuje dlaczego warto w ogóle walczyć. Jeśli nie ma piękna, nie ma motywacji i ludzkiej duszy._"
    * "_Detale mają znaczenie. Jaki sens w posiadaniu czołgu jak nie masz do niego paliwa?_"
* **Serce**
    * **Wound**
        * **Core Wound**: "_Bred for perfection. Wychowano mnie jako Paragona rodu. Nie mam przyjaciółek. Nie mam życia. Nie wiem jak żyć._"
        * **Core Lie**: "_Nie ma drugiej szansy. Nie ma wybaczania. Musisz zrobić dobrze ALBO wziąć ryzyko i odpłacić potem. Wszystko musi się zrównoważyć._"
    * **ENCAO**: 
        * N+C+
        * "zauważa praktycznie wszystko", "jej poważne oczy rejestrują każdy ruch każdego", "przejmuje się każdym detalem, ale jej to nie paraliżuje"
        * "potrafi kilkanaście razy zmienić plan podczas akcji gdy pojawiają się nowe dane", "zawsze nieskazitelny wygląd; nic nie przeoczy"
        * "dba o wszelkie szczegóły", "PRAWDZIWA tienka w dobrym rozumieniu", "przeuroczo zachwyca się pięknem"
    * **Wartości**: Tradition, Stimulation (Beauty)
        * T: "_To co było przed nami działało w tamtym kontekście. Teraz czas to przyłożyć do nowego kontekstu by poszerzyć możliwości. To co było przed nami jest WAŻNE._"
        * T: "_Odpowiadasz przed sobą, przodkami i historią. Nieważne, kto widział co robisz lub nie. Ważne, czy robisz to co należy._"
        * S: "_Jeśli nie próbujemy walczyć o piękno, nie zwracamy na to uwagi - czemu w ogóle próbować cokolwiek robić? Życie to nie tylko walka._"
        * S: "_Myślenie bez działania nic nie wnosi. Nikt niczego nie osiągnął siedząc na zapleczu i robiąc modele matematyczne._"
    * **DELTA_DRIVE**: 
        * **Stan aktualny**: "Po wojnie jest wiele zniszczonych jednostek, zagubionych dusz i ludzie próbują przetrwać, nie patrząc na piękno."
        * **Stan oczekiwany**: "Uczynić Skażone miejsce bezpiecznym, uhonorować poległych (też noktian) i zachować ich historie w przystępny sposób. Pokazać piękno i pomóc wyciągnąć dobre lekcje ze wszystkich."
    * **Metakultura**: dekadianin. "_Zadanie niekoniecznie jest najważniejsze - czasem trzeba się wycofać, by zwyciężyć w innym momencie. Ale jeśli jest najważniejsze, trzeba poświęcić wszystko_"
    * **Kolory**: UW. "_Jeśli poznamy wszystkie potrzebne dane, zrobimy najlepszy możliwy plan. Nie dostaniemy wszystkich danych, więc musimy robić założenia i rzucić się w ogień._"
    * **Wzory**: brak
    * **Inne**: brak

## Możliwości

* **Dominujące Strategie**: 
    * wykorzystywanie istniejących systemów oraz hierarchii zgodnie z ich przeznaczeniem
    * działania bezpośrednie: hierarchia > wpływy > militarne > magia
    * świetna w walce i lubiana przez innych żołnierzy jako "jedna z nich"
    * zachowanie piękna i minimalne zniszczenie czegoś przydatnego lub pięknego
    * prawdziwa tienka, 'immaculate' i imponująca - używa swego majestatu
* **Umocowanie w świecie**: 
    * tien Gwozdnik, córka Mariana i Krystalii

## Magia
### Dominująca moc

* JEDNYM ZDANIEM: może nie najpotężniejsza, ale precyzyjnie wykorzystywana magia ognia
    * magia użytkowa: "ognisty prysznic", "oczyszczenie munduru", "rozświetlenie jaskini", "ciepłe łóżko w zimie"
    * magia piękna: "grawerka przy użyciu super-precyzyjnego ognia"
    * magia bojowa: "oślepienie rozbłyskiem", "plasma lance", "spawanie / rozspawanie", "superheated metal"

#### Jak się objawia utrata kontroli

* Żywe Płomienie manifestują się w sposób upewniający że Oryginalny Plan zadziała niezależnie od zmian w sytuacji
* Magia uwypukla piękno CZEGOŚ, do poziomu praktycznie hipnotycznego
* Echo przeszłości - 'the past returns to haunt the present'

## INNE
### Konstrukcja postaci

* Wychodzę od metakultury
    * technologiczna + dekadianka: (próżniowcy i gaianie (acz postnoktiańscy), państwowcy, terragenofile, heterogeniczni memetycznie, bardzo militarystyczni)
    * Tradition + sprawne zasady działania
    * **element zaskakujący**: Beauty
* Przechodzę do niej jako jej samej, serce: Kolory, Magia (ogień; to wiedzieliśmy), Strategie.
* Widzę jej zachowania
    * działa bezpośrednio, analizując przeciwnika i dopasowując plany
    * jest raczej praworządna, wykorzystuje istniejące systemy zamiast je nadpisywać
    * odpowiada przed historią i przodkami a nie przed osobami dookoła niej; za to ocenia się bardzo surowo <-- tu jest potencjał na Core Wound / Lie
        * --> Core Lie: 'nie ma wybaczania, nie ma drugiej szansy - wszystko musisz odpłacić'
            * --> Core Wound: 'bred for perfection'
* Więc o czym myśli?
* DELTA_DRIVE
* Mechanika...


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230813-jedna-tienka-przybywa-na-pomoc | chorąża i tienka, próbuje się zintegrować z Orbiterem i jej się to udaje - nie jest ponad odśnieżanie czy uczciwą pracę; nie chce czarować i zakłócać eksperymentu Racheli, ale jak wykryła w patternie dokumentacji że tu coś jest nie tak i Rachela najpewniej ulega Skażeniu, zaeskalowała do dowództwa Orbitera. | 0095-09-08 - 0095-09-13 |
| 231024-rozbierany-poker-do-zwalczania-interis | kompetentna w obieraniu ziemniaków (i umie się odgryźć kucharzowi), pomaga Elenie w oszukiwaniu żołnierzy w grę w karty jako element dywersji | 0095-10-08 - 0095-10-10 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230813-jedna-tienka-przybywa-na-pomoc | Salvagerzy Lohalian, oddział Orbitera ją chłodno zaakceptował; udowodniła swoją przydatność i wartość. | 0095-09-13
| 231024-rozbierany-poker-do-zwalczania-interis | traktowana jako 'swoja tien chorąża', z zaakceptowanym tytułem | 0095-10-10

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Mikołaj Larnecjat    | 2 | ((230813-jedna-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Tomasz Afagrel       | 2 | ((230813-jedna-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Adelajda Kalmiris    | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Antoni Paklinos      | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |
| Cyprian Mirisztalnik | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Elena Samszar        | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Jaromir Gaburon      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Jarosław Mirelski    | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Maurycy Derwisz      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Paweł Lawarczak      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Rachela Brześniak    | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |
| Serafin Gwozdnik     | 1 | ((230813-jedna-tienka-przybywa-na-pomoc)) |