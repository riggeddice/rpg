---
categories: profile
factions: 
owner: public
title: Alicja Szadawir
---

# {{ page.title }}


# Generated: 



## Fiszki


* młoda neuromarine z Inferni, podkochuje się w Eustachym, ma neurosprzężenie i komputer w głowie, (nie cirrus). Strasznie nieśmiała, ale groźna. Ekspert od materiałów wybuchowych. Niepozorna, bardzo umięśniona. | @ 231018-anomalne-awarie-athamarein
* OCEAN: N+C+ | ma problemy ze startowaniem rozmowy;; nieśmiała jak cholera;; straszny kujon | VALS: Humility, Conformity | DRIVE: Uwolnić niewolników | @ 231018-anomalne-awarie-athamarein

### Wątki


legenda-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231018-anomalne-awarie-athamarein   | poluje na Leonę, bo nie chce się przed Eustachym zdradzić z podkochiwania. Wściekła, rzuca w Leonę automatem do wydawania chrupków, prawie trafiając w komodora Orogińca. Kończy w areszcie, klatka obok Leony. | 0111-12-22 - 0111-12-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Andrzej Gwozdnik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Arianna Verlen       | 1 | ((231018-anomalne-awarie-athamarein)) |
| Błażej Sowiński      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Elena Verlen         | 1 | ((231018-anomalne-awarie-athamarein)) |
| Eustachy Korkoran    | 1 | ((231018-anomalne-awarie-athamarein)) |
| Hiacynt Samszar      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Jakub Oroginiec      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Kajetan Kircznik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Klaudia Stryk        | 1 | ((231018-anomalne-awarie-athamarein)) |
| Leona Astrienko      | 1 | ((231018-anomalne-awarie-athamarein)) |
| OO Athamarein        | 1 | ((231018-anomalne-awarie-athamarein)) |
| Remigiusz Alkarenit  | 1 | ((231018-anomalne-awarie-athamarein)) |
| Uśmiechniczka Konstrukcjonistka Aurora Diakon | 1 | ((231018-anomalne-awarie-athamarein)) |