---
categories: profile
factions: 
owner: public
title: Gotard Kicjusz
---

# {{ page.title }}


# Generated: 



## Fiszki


*  | @ 231027-planetoida-bogatsza-niz-byc-powinna
* rola: technical guy, górnik, mały przedsiębiorca (trzech kumpli: PŁG) | @ 231027-planetoida-bogatsza-niz-byc-powinna
* personality: open, low-neuro, low-consc (extra stable, spontaneous, eccentric) | @ 231027-planetoida-bogatsza-niz-byc-powinna
* values: self-direction | @ 231027-planetoida-bogatsza-niz-byc-powinna
* wants: przygód, odzyskać rodzinę | @ 231027-planetoida-bogatsza-niz-byc-powinna

### Wątki


planetoidy-kazimierza
sekret-mirandy-ceres

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220327-wskrzeszenie-krolowej-przygod | górnik z Trzech Przyjaciół; lekko ekscentryczny i optymistyczny, chce przejąć Królową dla nich. Chce przygód i zobaczenia świata. | 0108-05-01 - 0108-05-16 |
| 231027-planetoida-bogatsza-niz-byc-powinna | niezależnie od okoliczności bronił Królowej że nie jest złym statkiem. Odpowiednio przesądny, uważa, że Królowa 'ich lubi'. Jego otwartość sprawiła, że zaufał Łucjanowi, że jest potwór Alteris, choć go nie umiał najpierw zobaczyć. | 0108-06-29 - 0108-07-02 |
| 220329-mlodociani-i-pirat-na-krolowej | próbuje ukryć problemy z dzieciakami tak, by nie było oficjalnie. Potem strzelał (kiepsko, ale Miranda strzelała dobrze) i rozbrajał miny (na szczęście nie musiał poważnie). | 0108-08-29 - 0108-09-09 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Łucjan Torwold       | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Miranda Ceres        | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Prokop Umarkon       | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| SC Królowa Przygód   | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Seweryn Grzęźlik     | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Anna Szrakt          | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Bartek Burbundow     | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Bartek Wudrak        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Berdysz Rozdzieracz  | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Damian Szczugor      | 1 | ((220327-wskrzeszenie-krolowej-przygod)) |
| Grzegorz Fabutownik  | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Helena Banbadan      | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Juliusz Cieślawok    | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Kara Prazdnik        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Kazimierz Zamglis    | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Mikołaj Resztkowiec  | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Romana Kundel        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Wojciech Kaznodzieja | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |