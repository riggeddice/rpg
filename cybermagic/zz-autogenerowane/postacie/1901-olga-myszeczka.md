---
categories: profile
factions: 
owner: public
title: Olga Myszeczka
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "myszeczka, pustogor"
* owner: "public"
* title: "Olga Myszeczka"


## Postać

### Ogólny pomysł (3)

Hodowca i eliminator nadmiernego Skażenia przy użyciu viciniusów użytkowych. Osoba ratująca i pomagająca zwierzakom. Technomantka specjalizująca się w transporcie viciniusów do odpowiedniego miejsca i zarządzaniu hodowlą. Mało kto tak jak ona zna się na różnych viciniusach i formach Skażenia. Ekspert od Skażeńców. Trochę "Solarianin" Asimova.

### Motywacja (gniew/wartość, zmiana, sposób) (3)

* VISION/POTENTIAL; świat gdzie zwierzęta i viciniusy są równi magom; separacja od świata i budowa własnej utopii
* STEWARDSHIP/SUSTAINABILITY; bezpieczna dla wszystkich rzeczywistość w harmonii; redukcja Skażenia i promocja swojego podejścia
* SELF-RELIANCE/RESTRAINT; nikt nie nadużywa mocy i władzy, wszystkim wedle potrzeb; jest żywym przykładem oraz udowadnia, że się da
* poważna, nieagresywna, lękliwa, unika ludzi i magów, cicha i nieśmiała

### Wyróżniki (3)

* hodowca i weterynarz zwierząt i viciniusów: zna się na większości viciniusów, jak im pomóc oraz jak zapewnić im idealne warunki.
* ekspert od Skażenia: wie doskonale jak działa Skażenie, jakie są jego formy i jak z danym Skażeniem wejść w interakcję.
* transport istot niebezpiecznych: mistrzyni logistyki i unieszkodliwiania niebezpiecznych istot.

### Zasoby i otoczenie (3)

* Linie dystrybucyjne Zajcewów, pozyskuje bardzo dziwne istoty z Syberii i próbuje je zagospodarować. Też: przemytnicy.
* Wsparcie hodowli Myszeczków, ma znajomości w różnych egzotycznych hodowlach swojego rodu i może pozyskać co ciekawsze istoty - lub dowiedzieć się, z czym tym razem ma do czynienia.
* Chmara ocznych impów służebnych, pomniejsze nietoperzokształtne impy zajmujące się pracami domowymi, obserwowaniem, szpiegowaniem i obserwowaniem terenu.
* Pojazd transportowy służący też jako więzienny, bardzo wzmocniony pojazd do transportu viciniusów... lub bezpiecznego zamknięcia przeciwnika.
* Niemała posiadłość niedaleko Wielkiej Szczeliny - silnie zautomatyzowana - gdzie żyją viciniusy. Droga i trudna do utrzymania; ma tam odratowane viciniusy.

### Magia (3)

#### Gdy kontroluje energię

* Technomantka, budująca roboty i maszyny wspierające i logistyczne. Sztuczne łapki dla glukszwajna, czy mechaniczne skrzydła dla śledzia syberyjskiego...
* Magia lecznicza, zarówno wobec ludzi i magów (gorzej) jak i wobec viciniusów i zwierząt (zdecydowanie lepiej). Nie adaptuje ich - tylko leczy i naprawia.
* Ogólnie rozumiane pułapki i sposoby powstrzymywania przeciwnika (czy to biochemicznie czy mechanicznie) by obezwładnić nie robiąc krzywdy.

#### Gdy traci kontrolę

* Pod wpływem pozytywnych emocji, jej magia lecznicza ma tendencje do nadmiernego wzmocnienia istot żywych - zwłaszcza rośliny próbują się bronić przed zwierzakami ;-).
* Pod wpływem negatywnych emocji, maszyny i zniewolone zwierzęta (znaczy, 'pet') buntują się przeciwko ludziom i magom.
* Jej magia zwykle objawia się technomantycznie lub biomantycznie. Czasem sama wpada w pułapkę którą zastawiła, czasem zmienia się (lub kogoś) w zwierzątko... 

### Powiązane frakcje

{{ page.factions }}

## Opis

.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181116-michal-w-otchlani-wezosmokow | najbardziej niewłaściwa czarodziejka do rozwiązywania infomantycznego Skażenia z magią krwi robioną przez ludzi. Poradziła sobie terminusami i świnią tropiącą. | 0103-09-13 - 0103-09-15 |
| 180724-skorpiony-spod-ziemi         | wkręcona w akcję ze Skażonymi Skorpipedami, udało jej się jednego oswoić (acz zaraz zginął). Większość kłopotów po akcji spadła na nią. | 0109-08-23 - 0109-08-25 |
| 181104-kotlina-duchow               | straumatyzowana ludźmi hodowczyni viciniusów; z pomocą Pięknotki wyplątała się z posterunku i przyszła jej z pomocą walczyć z Efemerydą Kotliny. Skończyła w szpitalu. | 0109-10-22 - 0109-10-24 |
| 190119-skorpipedy-krolewskiego-xirathira | nie radzi sobie z magami i ludźmi którzy non stop rozbijają jej spokojną Pustą Wieś. Powiedziała całą prawdę Pięknotce - o wsparciu Wiktora Sataraila. | 0110-01-14 - 0110-01-15 |
| 190313-plaga-jamnikow               | zesłała Plagę Jamników na Podwiert by ludzie dali jej spokój. No i dali - stworzyła Wyjący Zew. | 0110-02-28 - 0110-03-01 |
| 190721-kirasjerka-najgorszym-detektywem | najgorszy medyk świata; nie dała rady pomóc Mireli. Szczęśliwie przyszedł Wiktor i naprawił sytuację. | 0110-06-10 - 0110-06-11 |
| 210615-skradziony-kot-olgi          | przygotowuje somnibele by je sprzedawać / oddawać i pomagać ludziom po traumie. Jednego somnibela ukradł od niej Marek Samszar. Dobrodziejka Pawła Szprotki i jego promotorka do AMZ. | 0111-05-13 - 0111-05-15 |
| 211026-koszt-ratowania-torszeckiego | skontaktowała Marysię z Wiktorem, nie chce nic za to. Praktyczna i sympatyczna, acz żyje na uboczu. Ostrzegła Marysię, że Wiktor nie myśli jak człowiek. | 0111-08-01 - 0111-08-05 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181116-michal-w-otchlani-wezosmokow | dostaje odznaczenie za najbardziej nieefektowne użycie magii - ludzie nie chcieli wejść w świat magów, bo nie podobał im się styl Olgi. To taki "ignobel". | 0103-09-15
| 180724-skorpiony-spod-ziemi         | domyślnie podejrzana o sporo rzeczy przez Straż Wielkiej Szczeliny oraz Komitet Obrony Szczelińca z uwagi na sprawy związane ze skorpipedami. | 0109-08-25
| 180724-skorpiony-spod-ziemi         | mieszka w Czarnopalcu, samotnie i stamtąd odstrasza ludzi. Czarnopalec jest jej włościami utrzymującymi 3 typy viciniusów. | 0109-08-25
| 180724-skorpiony-spod-ziemi         | ma dostęp do glukszwajnów, śledzi syberyjskich oraz gorathaula | 0109-08-25
| 181104-kotlina-duchow               | tydzień w szpitalu - detoks i odkażanie po sprawach z Efemerydą Kotliny Mikarajły | 0109-10-24
| 190119-skorpipedy-krolewskiego-xirathira | Wiktor Satarail jej pomógł w odparciu harrasserów z Czerwonych Myszy. Wiktor jest jej cichym sojusznikiem, kimś, kto jej pomaga. | 0110-01-15

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Wiktor Satarail      | 4 | ((190119-skorpipedy-krolewskiego-xirathira; 190721-kirasjerka-najgorszym-detektywem; 210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego)) |
| Pięknotka Diakon     | 3 | ((181104-kotlina-duchow; 190119-skorpipedy-krolewskiego-xirathira; 190721-kirasjerka-najgorszym-detektywem)) |
| Marysia Sowińska     | 2 | ((210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego)) |
| Paweł Szprotka       | 2 | ((210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego)) |
| Adam Wroński         | 1 | ((181116-michal-w-otchlani-wezosmokow)) |
| Adela Kirys          | 1 | ((190119-skorpipedy-krolewskiego-xirathira)) |
| Aida Serenit         | 1 | ((190721-kirasjerka-najgorszym-detektywem)) |
| Alicja Kłębek        | 1 | ((180724-skorpiony-spod-ziemi)) |
| Alsa Fryta           | 1 | ((190313-plaga-jamnikow)) |
| Arkadia Verlen       | 1 | ((210615-skradziony-kot-olgi)) |
| Bronisława Strzelczyk | 1 | ((180724-skorpiony-spod-ziemi)) |
| Eliza Ira            | 1 | ((190313-plaga-jamnikow)) |
| Ignacy Myrczek       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Jakub Wirus          | 1 | ((180724-skorpiony-spod-ziemi)) |
| Julia Kardolin       | 1 | ((210615-skradziony-kot-olgi)) |
| Kirył Najłalmin      | 1 | ((181104-kotlina-duchow)) |
| Ksenia Kirallen      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Liliana Bankierz     | 1 | ((210615-skradziony-kot-olgi)) |
| Maciej Korzpuda      | 1 | ((181116-michal-w-otchlani-wezosmokow)) |
| Marek Samszar        | 1 | ((210615-skradziony-kot-olgi)) |
| Marlena Maja Leszczyńska | 1 | ((181104-kotlina-duchow)) |
| Mirela Orion         | 1 | ((190721-kirasjerka-najgorszym-detektywem)) |
| Piotr Ryszardowiec   | 1 | ((181116-michal-w-otchlani-wezosmokow)) |
| Rafał Torszecki      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Robert Einz          | 1 | ((181116-michal-w-otchlani-wezosmokow)) |
| Roman Kłębek         | 1 | ((180724-skorpiony-spod-ziemi)) |
| Sensacjusz Diakon    | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Sławomir Muczarek    | 1 | ((190119-skorpipedy-krolewskiego-xirathira)) |
| Szczepan Korzpuda    | 1 | ((181116-michal-w-otchlani-wezosmokow)) |
| Teresa Mieralit      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Tomek Żuchwiacz      | 1 | ((190313-plaga-jamnikow)) |
| Waldemar Mózg        | 1 | ((190119-skorpipedy-krolewskiego-xirathira)) |
| Wawrzyn Towarzowski  | 1 | ((181104-kotlina-duchow)) |
| Zdzich Aprantyk      | 1 | ((181116-michal-w-otchlani-wezosmokow)) |
| Zuzanna Klawornia    | 1 | ((181116-michal-w-otchlani-wezosmokow)) |