---
categories: profile
factions: 
owner: public
title: Ursyn Uszat
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220405-lepsza-kariera-dla-romki     | barman w Koronie; człowiek który znalazł robotę dla Kary. | 0108-09-15 - 0108-09-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anna Szrakt          | 1 | ((220405-lepsza-kariera-dla-romki)) |
| Antos Kuramin        | 1 | ((220405-lepsza-kariera-dla-romki)) |
| Damian Szczugor      | 1 | ((220405-lepsza-kariera-dla-romki)) |
| Helena Banbadan      | 1 | ((220405-lepsza-kariera-dla-romki)) |
| Miranda Ceres        | 1 | ((220405-lepsza-kariera-dla-romki)) |
| Romana Kundel        | 1 | ((220405-lepsza-kariera-dla-romki)) |
| SC Królowa Przygód   | 1 | ((220405-lepsza-kariera-dla-romki)) |
| Wojciech Kaznodzieja | 1 | ((220405-lepsza-kariera-dla-romki)) |