---
categories: profile
factions: 
owner: public
title: Eliza Kotlet
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190226-korporacyjna-wojna-w-mmo     | negocjatorka i psycholog małżeński, która próbuje zapobiec wojnie; najpierw pomogła w negocjacjach a potem przekierowała ogień na Dariusza (z rosy volant). | 0110-03-14 - 0110-03-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adrian Wężoskór      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Alan Bartozol        | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Antoni Kotomin       | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Dariusz Bankierz     | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Kermit Szperacz      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Mi Ruda              | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Rafał Królewski      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Wojciech Słabizna    | 1 | ((190226-korporacyjna-wojna-w-mmo)) |