---
categories: profile
factions: 
owner: public
title: Gerwazy Kircznik
---

# {{ page.title }}


# Generated: 



## Fiszki


* sierżant marine, Orbiter | @ 221102-astralna-flara-i-porwanie-na-karsztarinie
* ENCAO:  +0+-0 |Głośny;;Honorowy| VALS: Humility, Face| DRIVE: Lokalny społecznik | @ 221102-astralna-flara-i-porwanie-na-karsztarinie

### Wątki


historia-arianny
wplywy-aureliona-na-orbiterze

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221102-astralna-flara-i-porwanie-na-karsztarinie | sierżant marine Astralnej Flary, Orbiter (ENCAO:  +0+-0 |Głośny;;Honorowy| VALS: Humility, Face| DRIVE: Lokalny społecznik); | 0100-07-28 - 0100-08-03 |
| 221123-egzotyczna-pieknosc-na-astralnej-flarze | ma doświadczenie z Egzotycznymi Pięknościami; kazał Psu przesłuchać Ellarinę a sam przygotował defensywy by nic głupiego nie zrobiła. Ostrzegł Ariannę, że wprowadzanie uratowanych na pokład będzie niebezpieczne. Dobrze współpracuje z Erwinem Psem. | 0100-09-12 - 0100-09-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Arnulf Perikas       | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Daria Czarnewik      | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Kajetan Kircznik     | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Szczepan Myrczek     | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Władawiec Diakon     | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Ada Wyrocznik        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Alezja Dumorin       | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Elena Verlen         | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Ellarina Samarintael | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Erwin Pies           | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gabriel Lodowiec     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Grażyna Burgacz      | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hubert Kerwelenios   | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Klarysa Jirnik       | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Leszek Kurzmin       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Maja Samszar         | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Marcel Kulgard       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Mariusz Bulterier    | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Astralna Flara    | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| OO Athamarein        | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| OO Karsztarin        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| SCA Hadiah Emas      | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |