---
categories: profile
factions: 
owner: public
title: Rick Varias
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210820-fecundatis-w-domenie-barana  | kiedyś: twardy najemnik. Uratował Blakvelowców przed Strachami, potem przekonał Antosa by ów dowodził obroną Szamunczak a na końcu wmanipulował Kuratorów w opuszczenie tego miejsca na tydzień bo nie mogą pomóc XD. | 0109-03-06 - 0109-03-09 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210820-fecundatis-w-domenie-barana  | zawsze ma miejsce w Szamunczaku za obronę przed Strachami. Wielkie osiągnięcie. | 0109-03-09
| 210820-fecundatis-w-domenie-barana  | Antos Kuramin, "bohater ludu" jest mu bardzo wdzięczny za działania Ricka i wybicie Antosa na szczyt. | 0109-03-09

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antonella Temaris    | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Antos Kuramin        | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Bruno Baran          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Cień Brighton        | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Damian Szczugor      | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Deneb Ira            | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Elsa Kułak           | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Flawia Blakenbauer   | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Jolanta Sowińska     | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Kara Szamun          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Leon Kantor          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Tamara Mardius       | 1 | ((210820-fecundatis-w-domenie-barana)) |