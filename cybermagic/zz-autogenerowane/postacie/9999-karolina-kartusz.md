---
categories: profile
factions: 
owner: public
title: Karolina Kartusz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210330-pletwalowcy-na-goldarionie   | świetna, młoda mistrzyni cargo i logistyki Płetwala. Arogancka, "wie lepiej". Zdeptana przez Janusza z Goldariona, zgnębiona, odzyskała z trudem swoje morale i nawet się zaprzyjaźniła z Januszem. | 0109-03-11 - 0109-03-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Permin          | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Aleksander Leszert   | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Janusz Krumlod       | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Jonatan Piegacz      | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Martyna Bianistek    | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| SC Goldarion         | 1 | ((210330-pletwalowcy-na-goldarionie)) |