---
categories: profile
factions: 
owner: public
title: Jakub Bulgocz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220610-ratujemy-porywaczy-eleny     | dyrektor stacji medycznej Atropos współpracujący z syndykatem Aureliona. Zablokował Hestię. Ale Klaudia doń dotarła i Arianna go przesłuchała. Zero kręgosłupa - współpracuje z Arianną i zamiast egzekucji / oddania Kramerowi został przeniesiony jako medyk na Infernię. On jeszcze nie wie co go czeka. | 0112-09-15 - 0112-09-17 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220610-ratujemy-porywaczy-eleny     | po umowie z Arianną, przeniesiony z roli dyrektora Atropos na rolę medyka na Inferni. Kramer z uśmiechem zaakceptował transfer. | 0112-09-17

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Elena Verlen         | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Eustachy Korkoran    | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Hestia d'Atropos     | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Klaudia Stryk        | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Leona Astrienko      | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Martyn Hiwasser      | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Raoul Lavanis        | 1 | ((220610-ratujemy-porywaczy-eleny)) |