---
categories: profile
factions: 
owner: public
title: Jan Lertys
---

# {{ page.title }}


# Generated: 



## Fiszki


* drakolita z Aspirii | @ 230201-wylaczone-generatory-memoriam-inferni
* (ENCAO: -0-0+ |Zapominalski, obserwator, łatwo nań wywrzeć wrażenie | Humility, Universalism > Self-direction, Achievement | DRIVE: Pokój: Kapuleti i Monteki MUSZĄ żyć w pokoju, tylko nie Celina x Eustachy) | @ 230201-wylaczone-generatory-memoriam-inferni
* "Ciężką pracą dojdziemy do tego, że wszystko będzie działać jak powinno. Ja nie wtrącam się do działania Bartłomieja Korkorana; on wie lepiej." | @ 230201-wylaczone-generatory-memoriam-inferni

### Wątki


infernia-jej-imieniem
kidiron-zbawca-nativis

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220723-polowanie-na-szczury-w-nativis | brat Celiny; świetnie porusza się po tunelach arkologii i umie pozyskać różne rzeczy (np. spray skunksowy). Raczej słucha się siostry, bardzo związany z dziadkiem. | 0087-05-03 - 0087-05-12 |
| 220831-czarne-helmy-i-robaki        | pomagał Ardilii w scoutowaniu przemytników i przesunął się na pozycję "Infernia" a nie "Nativis" w postrzeganiu; | 0092-08-15 - 0092-08-27 |
| 220720-infernia-taksowka-dla-lycoris | silny i zdolny do bitki, choć nie najinteligentniejszy mat Inferni zakochany w Darii Raizis. Pisze dla Darii kompromitujące wiersze i nie aprobuje relacji Celiny w stronę Eustachego. Bardzo opiekuńczy wobec siostry i Zespołu. Rozstrzelał ainshkera z Eustachym. | 0093-01-20 - 0093-01-22 |
| 220817-osy-w-ces-purdont            | skupia się na ochronie Celiny gdy ta robi antidotum, na szybkim wspieraniu Ardilli gdy ta szuka składników i na strzelaniu do Trianai by chronić Zespół. Kompetentny ale nic super. | 0093-01-23 - 0093-01-24 |
| 230201-wylaczone-generatory-memoriam-inferni | cichy i spokojny Janek po tym jak Tymon Korkoran wzgardził jego siostrą dał mu TAK SOLIDNIE W MORDĘ że Tymon stracił przytomność i do końca sesji nie odzyskał. | 0093-02-10 - 0093-02-12 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220723-polowanie-na-szczury-w-nativis | ogólna reputacja 'dziwnego' w Nativis; nikt nie chce z nim zadzierać. Wie, że pochodzi z Aspirii. | 0087-05-12
| 220831-czarne-helmy-i-robaki        | opierdol i nieufność ludzi z Nativis, że pozwala Ardilli z Inferni się "wałęsać na lewo i prawo". On jest z Inferni, nie z Nativis. | 0092-08-27
| 230628-wojna-o-arkologie-nativis-konsolidacja-sil | wspiera Bartłomieja Korkorana i tylko jego z rodziny Korkoranów. Wszyscy inni są jego zdaniem lojalistami Kidirona. | 0093-03-28

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Celina Lertys        | 5 | ((220720-infernia-taksowka-dla-lycoris; 220723-polowanie-na-szczury-w-nativis; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni)) |
| Ardilla Korkoran     | 4 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni)) |
| Eustachy Korkoran    | 4 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni)) |
| Tymon Korkoran       | 3 | ((220723-polowanie-na-szczury-w-nativis; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni)) |
| Bartłomiej Korkoran  | 2 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni)) |
| Kamil Wraczok        | 2 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont)) |
| OO Infernia          | 2 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni)) |
| Rafał Kidiron        | 2 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni)) |
| Czesław Żuczek       | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Daria Raizis         | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Emilia d'Erozja      | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Joachim Puriur       | 1 | ((220817-osy-w-ces-purdont)) |
| Kalia Awiter         | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Karol Lertys         | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Kordian Olgator      | 1 | ((220817-osy-w-ces-purdont)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Laurencjusz Kidiron  | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Lycoris Kidiron      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| OA Erozja Ego        | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Ralf Tapszecz        | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Stanisław Uczantor   | 1 | ((220831-czarne-helmy-i-robaki)) |
| VN Karglondel        | 1 | ((220817-osy-w-ces-purdont)) |
| Wiktor Turkalis      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Wojciech Czerpń      | 1 | ((220831-czarne-helmy-i-robaki)) |