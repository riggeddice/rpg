---
categories: profile
factions: 
owner: public
title: ASD Centurion
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190116-wypalenie-saitaera-z-trzesawiska | statek artyleryjski na orbicie Astorii który wpierw doprowadził do wymuszenia regeneracji ołtarza w odpowiednim miejscu a potem wspierał ogniem Pięknotkę. | 0110-01-07 - 0110-01-09 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Bartozol        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Hieronim Maus        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Karradrael           | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Kasjopea Maus        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Pięknotka Diakon     | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Saitaer              | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Wiktor Satarail      | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |