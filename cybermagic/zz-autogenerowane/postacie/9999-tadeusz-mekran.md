---
categories: profile
factions: 
owner: public
title: Tadeusz Mekran
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240214-relikwia-z-androida          | kochający mąż Aliny, który pod wpływem hipnonarkotyków chciał się zabić po zniszczeniu serii danych (dla Elwiry). Aktywowało to ducha Aliny. Ewakuowany przez Agencję z Szernief. | 0107-05-16 - 0107-05-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aerina Cavalis       | 1 | ((240214-relikwia-z-androida)) |
| Alina Mekran         | 1 | ((240214-relikwia-z-androida)) |
| Elwira Barknis       | 1 | ((240214-relikwia-z-androida)) |
| Kalista Surilik      | 1 | ((240214-relikwia-z-androida)) |
| Klasa Dyplomata      | 1 | ((240214-relikwia-z-androida)) |
| Klasa Hacker         | 1 | ((240214-relikwia-z-androida)) |
| Klasa Inżynier       | 1 | ((240214-relikwia-z-androida)) |
| Klasa Oficer Naukowy | 1 | ((240214-relikwia-z-androida)) |
| Klasa Sabotażysta    | 1 | ((240214-relikwia-z-androida)) |
| Mawir Hong           | 1 | ((240214-relikwia-z-androida)) |
| Vanessa d'Cavalis    | 1 | ((240214-relikwia-z-androida)) |