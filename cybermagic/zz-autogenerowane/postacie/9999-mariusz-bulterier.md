---
categories: profile
factions: 
owner: public
title: Mariusz Bulterier
---

# {{ page.title }}


# Generated: 



## Fiszki


* advancer, tien, seilita, sybrianin | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* ENCAO:  0-+-- |Prostolinijny i otwarty;;Nie kłania się nikomu;;Uroczysty i poważny| VALS: Humility, Tradition| DRIVE: Nawracanie | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej

### Wątki


kosmiczna-chwala-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221012-kapitan-verlen-i-niezapowiedziana-inspekcja | advancer; seiliowiec; tien; wysoki szacunek do Arianny, bo je konserwy i żyje spartańsko. Puszcza większość plotek :D. | 0100-05-17 - 0100-05-21 |
| 221102-astralna-flara-i-porwanie-na-karsztarinie | współpracując z Eleną, skutecznie zrobił insercję i unieszkodliwił komandosów Aureliona próbujących porwać kadeta z Karsztarina. | 0100-07-28 - 0100-08-03 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Arnulf Perikas       | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Daria Czarnewik      | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Hubert Kerwelenios   | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Maja Samszar         | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Szczepan Myrczek     | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Władawiec Diakon     | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Ada Wyrocznik        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Adam Chrząszczewicz  | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Alezja Dumorin       | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Elena Verlen         | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Gerwazy Kircznik     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Grażyna Burgacz      | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Kajetan Kircznik     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Klarysa Jirnik       | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Leona Astrienko      | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| OO Karsztarin        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Królowa Kosmicznej Chwały | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Tomasz Dojnicz       | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |