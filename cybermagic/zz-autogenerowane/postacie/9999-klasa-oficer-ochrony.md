---
categories: profile
factions: 
owner: public
title: Klasa Oficer Ochrony
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240117-echo-z-odmetow-przeszlosci   | Odzyskał sondę morską sprzed pęknięcia z dna alterisowej rafy Prisma, ostatecznie traci zaufanie kapitana Barowieckiego próbując go przekonać do oszukania załogi stacji, potwierdza, że powiedzenie prawdy o anomalii nie zabije załogantów informując Annę, wpędzając ją tym w depresję. | 0096-01-24 - 0096-01-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anna Ivanova         | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Elżbieta Sanchez     | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Gerald Barowiecki    | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Jędrzej Sanchez      | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Klasa Dyplomata      | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Klasa Inżynier       | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Robert Tisso         | 1 | ((240117-echo-z-odmetow-przeszlosci)) |