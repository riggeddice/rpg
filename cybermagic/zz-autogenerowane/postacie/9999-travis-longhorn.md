---
categories: profile
factions: 
owner: public
title: Travis Longhorn
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190802-statek-zjada-statki          | inżynier zniszczenia i fortyfikator Mikado; rozwalił modliszkoidalnego "boga" oraz zaprzyjaźnił się z Evą - co wydawało się niemożliwe. | 0087-08-09 - 0087-08-12 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aida Serenit         | 1 | ((190802-statek-zjada-statki)) |
| AK Serenit           | 1 | ((190802-statek-zjada-statki)) |
| Eva d'Mikado         | 1 | ((190802-statek-zjada-statki)) |
| Ignaś Orbita         | 1 | ((190802-statek-zjada-statki)) |
| Laura Prunal         | 1 | ((190802-statek-zjada-statki)) |
| Michał Dusiciel      | 1 | ((190802-statek-zjada-statki)) |
| SC Mikado            | 1 | ((190802-statek-zjada-statki)) |
| Sia d'Cranis         | 1 | ((190802-statek-zjada-statki)) |
| Stella Koral         | 1 | ((190802-statek-zjada-statki)) |