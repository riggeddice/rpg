---
categories: profile
factions: 
owner: public
title: SN Anitikaplan
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240111-zlomowanie-legendarnej-anitikaplan | legendarny statek rysowników Tygryska, wagabunda kosmiczny. Podczas Wojny Deoriańskiej napadnięty przez fanatyków którzy go zniszczyli. Teraz - jednostka anomalizująca (Sempitus-Alteris). Zneutralizowany przez Agencję. | 0084-04-02 - 0084-04-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klasa Biurokrata     | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Klasa Hacker         | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Klasa Oficer Naukowy | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Klasa Sabotażysta    | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| SN Varilen           | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Twaróg Worl          | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Wiktor Worl          | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |