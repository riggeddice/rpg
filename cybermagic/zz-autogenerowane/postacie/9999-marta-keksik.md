---
categories: profile
factions: 
owner: public
title: Marta Keksik
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: E+C-A- | Pełna pasji, rozwiązuje problemy siłą. | VALS: Power, Stimulation | DRIVE: Udowodnić swą wartość. | @ 231025-spiew-nielalki-na-castigatorze
* świetna duelistka, elegancko ubrana, zimne i stalowe spojrzenie i bardzo duża asertywność. Chroni brata i 'swoich'. | @ 231025-spiew-nielalki-na-castigatorze

### Wątki


triumfalny-powrot-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231025-spiew-nielalki-na-castigatorze | tien na Castigatorze, poważna duelistka która chciała przez Trzy Pytania dotrzeć do Eustachego i poznać prawdę o Natalii (ona miała lecieć); chroni brata i przemyt do końca, ale ma sensowne podejście - w chwili kryzysu stanęła za Arianną i przestała przeszkadzać tienom. Ringleader. | 0110-10-24 - 0110-10-26 |
| 231109-komodor-bladawir-i-korona-woltaren | uważa Elenę za bezużyteczną i nie boi się o tym mówić - chce być LEPSZA. Ma lekceważący ton ale słucha się hierarchii. Jednak gdy atakuje terroform a Arianna i Klaudia są w stuporze, Marta odważnie stawia mu czoła i kończy bardzo ciężko ranna. | 0110-10-27 - 0110-11-03 |
| 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira | poproszona przez Patryka, napisała prośbę do Arianny by ta wzięła Patryka na ćwiczenia. 'Bo on mi smęci nad łóżkiem. Ma moją rekomendację, bo się zamknie'. Rekomendacja nie przekonała Arianny XD. | 0110-11-26 - 0110-11-30 |
| 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze | bardzo szanuje Klaudię i nie tylko powiedziała jej co wie, ale też dostarczyła próbkę kralotycznego narkotyku wejściowego (anipacis). Zbliżyła się do Eleny; dużo razem ćwiczą. | 0110-12-19 - 0110-12-21 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 231109-komodor-bladawir-i-korona-woltaren | bardzo ciężko ranna w wyniku ataku terrorforma. 2 tygodnie w infirmarium, bo ixiońska infekcja. Do tego kolejny szok - wpierw alteris, teraz ixion. I 'dorośli' nie dali rady. | 0110-11-03

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Elena Verlen         | 4 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Klaudia Stryk        | 4 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Patryk Samszar       | 4 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Arianna Verlen       | 3 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Leszek Kurzmin       | 3 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Antoni Bladawir      | 2 | ((231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Feliks Walrond       | 2 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren)) |
| Igor Arłacz          | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Konstanty Keksik     | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| OO Infernia          | 2 | ((231025-spiew-nielalki-na-castigatorze; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| OO Paprykowiec       | 2 | ((231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Aneta Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Anna Tessalon        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Eleonora Perłamila   | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Eustachy Korkoran    | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Julia Myrczek        | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Kamil Burgacz        | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Kamil Lyraczek       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Kosmicjusz Tanecznik Diakon | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Krzysztof Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Lars Kidironus       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Leona Astrienko      | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Lidia Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Łucja Larnecjat      | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Martyn Hiwasser      | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| OO Castigator        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Raoul Lavanis        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| SC Korona Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| TAI Eszara d'Castigator | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Tomasz Rewernik      | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Wojciech Mykirło     | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Zaara Mieralit       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |