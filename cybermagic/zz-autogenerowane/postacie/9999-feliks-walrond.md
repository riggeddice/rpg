---
categories: profile
factions: 
owner: public
title: Feliks Walrond
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231025-spiew-nielalki-na-castigatorze | komodor, który objął tymczasowo Elenę do operacji antykralotycznej i do odzyskania Natalii Gwozdnik. Zaakceptował prośbę Eleny do dołączenia do jego floty tymczasowo, bo Elena mu się podoba. | 0110-10-24 - 0110-10-26 |
| 231109-komodor-bladawir-i-korona-woltaren | komodor szukający kralotha; zignorował żądanie Eleny że tam coś jest. Potem poważnie Elenę opieprzył bo przez nią musiał płacić odszkodowanie. Dzięki Bladawirowi, wyszło na jego. | 0110-10-27 - 0110-11-03 |
| 210526-morderstwo-na-inferni        | komodor Orbitera (w linii Szradmanna); ma duże wonty do noktian. Jego zdaniem integracja z noktianami to błąd itp. Wykorzystywał czarne sektory do ćwiczeń Lancerami. | 0111-12-31 - 0112-01-06 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 231109-komodor-bladawir-i-korona-woltaren | dzięki Elenie i jej zniszczeniu 'drzewa' miał problem. Dzięki jej wykryciu terrorforma ixiońskiego miał bonus. Wdzięczny Bladawirowi i w sumie wyszedł do przodu. | 0110-11-03

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 3 | ((210526-morderstwo-na-inferni; 231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren)) |
| Elena Verlen         | 3 | ((210526-morderstwo-na-inferni; 231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren)) |
| Klaudia Stryk        | 3 | ((210526-morderstwo-na-inferni; 231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren)) |
| Eustachy Korkoran    | 2 | ((210526-morderstwo-na-inferni; 231025-spiew-nielalki-na-castigatorze)) |
| Leona Astrienko      | 2 | ((210526-morderstwo-na-inferni; 231025-spiew-nielalki-na-castigatorze)) |
| Leszek Kurzmin       | 2 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren)) |
| Marta Keksik         | 2 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren)) |
| Patryk Samszar       | 2 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren)) |
| Aneta Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Anna Tessalon        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Antoni Bladawir      | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Antoni Kramer        | 1 | ((210526-morderstwo-na-inferni)) |
| Igor Arłacz          | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Kamil Lyraczek       | 1 | ((210526-morderstwo-na-inferni)) |
| Karol Reichard       | 1 | ((210526-morderstwo-na-inferni)) |
| Konstanty Keksik     | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Krzysztof Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Lidia Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Marian Tosen         | 1 | ((210526-morderstwo-na-inferni)) |
| Martyn Hiwasser      | 1 | ((210526-morderstwo-na-inferni)) |
| OO Castigator        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| OO Infernia          | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| OO Paprykowiec       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| OO Tivr              | 1 | ((210526-morderstwo-na-inferni)) |
| Otto Azgorn          | 1 | ((210526-morderstwo-na-inferni)) |
| Raoul Lavanis        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| SC Korona Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| TAI Eszara d'Castigator | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Tal Marczak          | 1 | ((210526-morderstwo-na-inferni)) |
| Tomasz Rewernik      | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Wojciech Mykirło     | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |