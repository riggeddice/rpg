---
categories: profile
factions: 
owner: public
title: Zofia d'Seiren
---

# {{ page.title }}


# Generated: 



## Fiszki


* salvager Serien (Seiren Local / combat / medical / tectonics) faeril: "Anna mnie uratowała gdy nikt nie dał mi szansy"   <-- KIĆ | @ 230104-to-co-zostalo-po-burzy
* (ENCAO: -0--+ |Unika kłopotów, roztargniona| VALS: Conformity, Security >> Hedonism| DRIVE: Wieczny dług (Anna)) | @ 230104-to-co-zostalo-po-burzy
* "Życie może jest ciężkie, ale możemy sobie pomóc; działajmy razem." "Uważaj... to może być niebezpieczne." | @ 230104-to-co-zostalo-po-burzy
* **objęta na sesji przez Kić** | @ 230125-whispraith-w-jaskiniach-neikatis
* salvager Seiren (Seiren Local / combat / medical / tectonics) faeril: "Anna mnie uratowała gdy nikt nie dał mi szansy" | @ 230125-whispraith-w-jaskiniach-neikatis

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230104-to-co-zostalo-po-burzy       | combat / medical / tectonics Seiren; skutecznie ekstraktuje energię z krystalników. Radzi sobie z pilotażem Seiren mimo dziwnych sygnałów i stworów. Autopsja nieżywej Mirki z "Uśmiechu" | 0092-10-26 - 0092-10-28 |
| 230125-whispraith-w-jaskiniach-neikatis | stabilizuje Kornelię zarówno stymulantami jak i medycznie. Wykryła częściową mumifikację jako efekt whispraitha i wykluczyła infekcję Kornelii na podstawie tego. | 0092-10-29 - 0092-11-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aniela Myszawcowa    | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Anna Seiren          | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Antoni Grzypf        | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Cyprian Kugrak       | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Eustachy Korkoran    | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| JAN Seiren           | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| JAN Uśmiech Kamili   | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Michał Uszwon        | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Rufus Seiren         | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Kalia Awiter         | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Kornelia Lichitis    | 1 | ((230125-whispraith-w-jaskiniach-neikatis)) |
| Rafał Kidiron        | 1 | ((230104-to-co-zostalo-po-burzy)) |