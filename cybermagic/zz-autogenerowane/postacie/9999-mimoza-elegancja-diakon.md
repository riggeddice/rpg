---
categories: profile
factions: 
owner: public
title: Mimoza Elegancja Diakon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220101-karolina-w-ciezkim-mlocie    | NIEOBECNA. Rekin; prawdziwa dama. O nią był zakład - czy wypije piwo w Ciężkim Młocie. Daniel i Karo próbowali do tego doprowadzić (mimo jej absolutnej niewiedzy). | 0111-04-25 - 0111-05-05 |
| 220222-plaszcz-ochronny-mimozy      | 33 lata, perfekcyjna i piękna arystokratka z gracją i delikatnością. Dała azyl Lorenie, która go nadużyła. Chroni Lorenę własnym ciałem, nieustraszona. Przez Lorenę straciła trochę honoru wobec Marysi. Robi dobrą minę do złej gry i próbuje zresetować spotkanie i relacje. Bardzo silnie zaangażowana w sprawy lokalne Podwiertu. Chroni Lorenę i sprawia, by informacja że to akt zrobiony przez Lorenę nie wyszedł na jaw. Żąda wydania kultystek jej od "siepacza z Eterni" (Ernesta). | 0111-09-21 - 0111-09-25 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220111-marysiowa-hestia-rekinow     | silnie zaniepokojona działaniami Ernesta wrt wjazdu na apartament czy jego samowolą. Mimoza przeszła na tryb bojowy / defensywny. | 0111-09-19
| 220222-plaszcz-ochronny-mimozy      | jej apartament ma zakochanych ludzi, rośliny i bioterror defensywny. Trudna do infiltracji. | 0111-09-25
| 220222-plaszcz-ochronny-mimozy      | ma w Apartamencie sporo osób. Przechowuje tam jakieś kilkanaście osób w przestrzeni dla służby. Odratowane z ulic Podwiertu. | 0111-09-25
| 220222-plaszcz-ochronny-mimozy      | opinia "słaba czarodziejka, ale ma świetne zaplecze biotechniczne i jest prawdziwie dobrą arystokratką". Silnie związana z Podwiertem, najbardziej lubiana arystokratka Aurum. | 0111-09-25

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolina Terienak    | 2 | ((220101-karolina-w-ciezkim-mlocie; 220222-plaszcz-ochronny-mimozy)) |
| Daniel Terienak      | 1 | ((220101-karolina-w-ciezkim-mlocie)) |
| Ernest Namertel      | 1 | ((220222-plaszcz-ochronny-mimozy)) |
| Henryk Murkot        | 1 | ((220101-karolina-w-ciezkim-mlocie)) |
| Keira Amarco d'Namertel | 1 | ((220222-plaszcz-ochronny-mimozy)) |
| Lorena Gwozdnik      | 1 | ((220222-plaszcz-ochronny-mimozy)) |
| Marysia Sowińska     | 1 | ((220222-plaszcz-ochronny-mimozy)) |
| Władysław Owczarek   | 1 | ((220101-karolina-w-ciezkim-mlocie)) |