---
categories: profile
factions: 
owner: public
title: Maja Nerwial
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (E+N-): Nieukrywana ciekawość świata, uwielbia zaskakiwać ludzi. "Wszystko ma swoją zabawną stronę, nawet najgorsze sytuacje." | @ 230810-nie-nazywasz-sie-janusz
* VALS: (Self-Direction, Stimulation): "Każdy dzień to nowa przygoda. Zobaczmy, co dzisiaj przyniesie." | @ 230810-nie-nazywasz-sie-janusz
* Core Wound - Lie: "Zdobyta jako janczar w bitwie" - "Jeśli zawsze będę nieprzewidywalna, nie zostanę znowu porzucona." | @ 230810-nie-nazywasz-sie-janusz
* Styl: Lubi ryzyko, zawsze szuka nowych wyzwań i doświadczeń, ale nigdy nie zapomina o tych, którzy są obok niej. | @ 230810-nie-nazywasz-sie-janusz

### Wątki


neikatianska-gloria-saviripatel
tarcza-nox-aegis

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230810-nie-nazywasz-sie-janusz      | narzeczona Janusza; dzięki niej przypomniał sobie i wyrwał się spod kontroli | 0093-03-29 - 0093-03-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Artur Wrulgop        | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Dariusz Avatrinn     | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Janusz Lemuel        | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Krzesimir Pluszcz    | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Łucja Kritoriin      | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Nova Atreia          | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| OWT Łowca Martwych Gwiazd | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| TAI Xaira Cognitia   | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Teresa Kritoriin     | 1 | ((230810-nie-nazywasz-sie-janusz)) |