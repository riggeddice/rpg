---
categories: profile
factions: 
owner: public
title: Mirela Niecień
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181216-wolna-od-terrorforma         | lekarz z Kompleksu Nukleon; opiekuje się Pięknotką i Ateną. Zna Bogdana oraz wie, jak jest niebezpieczny. Ogólnie, dobra i pozytywna siła wspierająca Pięknotkę i przyjaciół. Dostała informacje od Bogdana jak naprawiać magów dotkniętych przez terrorforma z pomocą kralothów. | 0109-11-05 - 0109-11-11 |
| 181227-adieu-cieniaszczycie         | niechętnie budowała dla Pięknotki wspomagane ciało dla Minerwy na planach Saitaera. Próbowała wyjaśnić, czemu to zły pomysł, ale Pięknotka była zdeterminowana. | 0109-11-28 - 0109-12-01 |
| 190724-odzyskana-agentka-orbitera   | zajmuje się Aidą; powiedziała Pięknotce co zrobić - wygrać na arenie z championem kralotha lub porwać Aidę. | 0110-06-12 - 0110-06-15 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181216-wolna-od-terrorforma         | dostała formułę pomagającą w zwalczaniu wpływów terrorforma Saitaera na magów; wymaga kralotha. | 0109-11-11

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 3 | ((181216-wolna-od-terrorforma; 181227-adieu-cieniaszczycie; 190724-odzyskana-agentka-orbitera)) |
| Atena Sowińska       | 2 | ((181216-wolna-od-terrorforma; 181227-adieu-cieniaszczycie)) |
| Julia Morwisz        | 2 | ((181216-wolna-od-terrorforma; 190724-odzyskana-agentka-orbitera)) |
| Moktar Gradon        | 2 | ((181227-adieu-cieniaszczycie; 190724-odzyskana-agentka-orbitera)) |
| Adam Szarjan         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Aida Serenit         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Amadeusz Sowiński    | 1 | ((181227-adieu-cieniaszczycie)) |
| Bogdan Szerl         | 1 | ((181216-wolna-od-terrorforma)) |
| Brygida Maczkowik    | 1 | ((181227-adieu-cieniaszczycie)) |
| Erwin Galilien       | 1 | ((181227-adieu-cieniaszczycie)) |
| Lilia Ursus          | 1 | ((181227-adieu-cieniaszczycie)) |
| Mirela Orion         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Pietro Dwarczan      | 1 | ((181227-adieu-cieniaszczycie)) |
| Waleria Cyklon       | 1 | ((181227-adieu-cieniaszczycie)) |
| Zbigniew Burzycki    | 1 | ((181227-adieu-cieniaszczycie)) |