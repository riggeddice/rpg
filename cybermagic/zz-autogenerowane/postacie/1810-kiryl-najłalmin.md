---
categories: profile
factions: 
owner: public
title: Kirył Najłalmin
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "ambitne orzeszki"
* type: "NPC"
* owner: "public"
* title: "Kirył Najłalmin"


## Postać

### Ogólny pomysł (2)

Ogromny miłośnik Supreme Missionforce i ekspert od procesów i logistyki na stacji Epirjon. Trochę roztargniony specjalista od minimalizacji kosztów i optymalizacji zużycia kosztów. Przy okazji, spec od dalekosiężnych portali i teleportacji. W czasie wolnym ćwiczy sztuki walki.

### Co motywuje postać (2)

* regularnie przegrywa walcząc z Wiewiórką -> pokona swoją szefową gildii w 1v1 w SupMis
* strasznie się stresuje na Epirjonie; Atena się nad nim znęca -> nie napracować się za bardzo; usunąć stresory z otoczenia
* jego życie jest bardzo uporządkowane i odpowiedzialne -> chce robić rzeczy które są Przygodą
* lekko zagubiony, źle reaguje na stres, bardzo precyzyjny, zdecydowanie unika pracy

### Idealne okoliczności (2)

* przy desce kreślarskiej próbuje z posiadanych materiałów zrobić coś czego normalnie byłoby trudno; engineering miracle worker
* gra w Supreme Missionforce, zwłaszcza, jeśli zajmuje się poziomem ekonomii, jednostek i bazy
* pracuje nad portalami lub teleporterami; albo portaluje przedmiot dalekosiężnie albo próbuje dojść do połączenia wiązek

### Szczególne umiejętności (3)

* optymalizacja kosztów: rzadko kiedy da się znaleźć proces którego on nie usprawni czy połączenia jakiego nie znajdzie. Logistyka i procesy.
* praca z portalami i teleportacjami: jest to specjalista na skalę astoriańską portali, teleporterów i wszelkich rzeczy związanych z lokalizacją.
* świetny matematyk i estymator: ma wyczucie liczb i potrafi doskonale oszacowywać różne rzeczy.
* taktyk w Supreme Missionforce: jeden z najlepszych graczy pod kątem makro i ekonomii jaki jest w okolicy

### Zasoby i otoczenie (3)

* przemytnicy w terenie Skażonym: niejednokrotnie pomagał im przez Epirjon przenieść niektóre przedmioty
* perfekcyjna znajomość sieci teleportacyjnych na Astorii
* stealth suit: nie jest to bojowe, ale daje mu opcję wspomagania fizycznego, ochrony środowiskowej i utrudnia wykrycie

### Magia (3)

#### Gdy kontroluje energię

* Teleportacje i portale. Szczególnie specjalizuje się w dalekosiężnych precyzyjnych zaklęciach teleportacyjnych; specjalista klasy astoriańskiej.
* Ogólnie rozumiana magia naprawcza, zwłaszcza na linii Materii i Technomancji.

#### Gdy traci kontrolę

* przede wszystkim, rzeczy zaczynają się losowo teleportować lub portale się same uruchamiają
* inne formy: ognie świętego elma czy emisje elektryczności z bytów które mogą tak działać
* czasem dochodzi do uszkodzenia informacji w dokumentach czy komputerach

### Powiązane frakcje

{{ page.factions }}

## Opis



# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180814-trufle-z-kosmosu             | mag naleśnik ze stacji Epirjon. Zgnębiony przez szefową (Atenę Sowińską) pomylił się i zesłał Trufle na Primusa, na Żarnię. Potem łaził naprawiać sytuację. | 0109-08-30 - 0109-09-01 |
| 181104-kotlina-duchow               | naleśnik okazał się wybitnym ekspertem od teleportacji na stacji Epirjon. Został bohaterem pomagając Pięknotce osłonić się przed Efemerydą Kotliny. | 0109-10-22 - 0109-10-24 |
| 190127-ixionski-transorganik        | mistrz teleportacji; dał radę przenieść jedynie Wojtka (terrorforma) jak i Pięknotkę na Głodną Ziemię. | 0110-01-29 - 0110-01-30 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 3 | ((180814-trufle-z-kosmosu; 181104-kotlina-duchow; 190127-ixionski-transorganik)) |
| Arkadiusz Mocarny    | 1 | ((180814-trufle-z-kosmosu)) |
| Bronisława Strzelczyk | 1 | ((180814-trufle-z-kosmosu)) |
| Erwin Galilien       | 1 | ((190127-ixionski-transorganik)) |
| Jakub Wirus          | 1 | ((180814-trufle-z-kosmosu)) |
| Karolina Erenit      | 1 | ((190127-ixionski-transorganik)) |
| Marlena Maja Leszczyńska | 1 | ((181104-kotlina-duchow)) |
| Minerwa Metalia      | 1 | ((190127-ixionski-transorganik)) |
| Napoleon Bankierz    | 1 | ((190127-ixionski-transorganik)) |
| Olga Myszeczka       | 1 | ((181104-kotlina-duchow)) |
| Roman Kłębek         | 1 | ((180814-trufle-z-kosmosu)) |
| Saitaer              | 1 | ((190127-ixionski-transorganik)) |
| Tymon Grubosz        | 1 | ((190127-ixionski-transorganik)) |
| Wawrzyn Towarzowski  | 1 | ((181104-kotlina-duchow)) |
| Wiktor Satarail      | 1 | ((190127-ixionski-transorganik)) |
| Wojtek Kurczynos     | 1 | ((190127-ixionski-transorganik)) |