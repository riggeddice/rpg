---
categories: profile
factions: 
owner: public
title: Seraf Verlen
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210210-milosc-w-rodzie-verlen       | kiedyś obiekt westnień Eleny; UW; słitaśny cherubinek, który jest ostry jak żyleta dla żołnierzy i podwładnych. | 0111-08-21 - 0111-08-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Apollo Verlen        | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Arianna Verlen       | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Brunhilda Verlen     | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Elena Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Franz Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Krucjusz Verlen      | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Romeo Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Viorika Verlen       | 1 | ((210210-milosc-w-rodzie-verlen)) |