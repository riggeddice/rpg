---
categories: profile
factions: 
owner: public
title: OO Alaya
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210519-osiemnascie-oczu             | statek zainfekowany anomalicznym infohazardem. Chciał przejść przez Bramę w krwawym rytuale. Marian Tosen wysłał elitarną załogę Inferni, by unieszkodliwili hazard i uratowali Alayę. | 0111-12-07 - 0111-12-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((210519-osiemnascie-oczu)) |
| Elena Verlen         | 1 | ((210519-osiemnascie-oczu)) |
| Eustachy Korkoran    | 1 | ((210519-osiemnascie-oczu)) |
| Klaudia Stryk        | 1 | ((210519-osiemnascie-oczu)) |
| Leona Astrienko      | 1 | ((210519-osiemnascie-oczu)) |
| Marian Tosen         | 1 | ((210519-osiemnascie-oczu)) |
| Martyn Hiwasser      | 1 | ((210519-osiemnascie-oczu)) |
| OO Tivr              | 1 | ((210519-osiemnascie-oczu)) |