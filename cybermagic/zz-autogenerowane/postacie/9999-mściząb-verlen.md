---
categories: profile
factions: 
owner: public
title: Mściząb Verlen
---

# {{ page.title }}


# Generated: 



## Fiszki


*  | @ 230426-mscizab-zabojca-arachnoziemskich-jaszczurow
* Dostał swoje imię po tym, jak pobił pierwszą osobę którą uznał za to, że ukradła mu mleczaka. | @ 230426-mscizab-zabojca-arachnoziemskich-jaszczurow
* Chce być Verlenem bojowym, chce wygrywać, nie do końca dba o ludzi. Ważne jest zwycięstwo. | @ 230426-mscizab-zabojca-arachnoziemskich-jaszczurow

### Wątki


spawner-godny-verlenlandu

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230426-mscizab-zabojca-arachnoziemskich-jaszczurow | 17 lat, sfrustrowany że nie może pokonać Łowcy. Zablokował możliwość wezwania pomocy do Arachnoziem mimo ofiar; po zniszczeniu Jaszczurów Tunelowych nie radzi sobie z Łowcą. | 0095-08-03 - 0095-08-05 |
| 230524-ekologia-jaszczurow-w-arachnoziemie | załamany zdradą ze strony Emmanuelle, poszedł za rozkazami Vioriki i uczestniczył w operacji zestrzelenia krystalicznego mimika z powietrza. | 0095-08-06 - 0095-08-09 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230524-ekologia-jaszczurow-w-arachnoziemie | Arianna i Viorika przypisały mu złośliwie wszystkie zasługi z uratowania sytuacji w Arachnoziemie (mimik i śluzowiec). | 0095-08-09

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((230426-mscizab-zabojca-arachnoziemskich-jaszczurow; 230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Viorika Verlen       | 2 | ((230426-mscizab-zabojca-arachnoziemskich-jaszczurow; 230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Atraksjusz Verlen    | 1 | ((230426-mscizab-zabojca-arachnoziemskich-jaszczurow)) |
| Emmanuelle Gęsiawiec | 1 | ((230426-mscizab-zabojca-arachnoziemskich-jaszczurow)) |
| Fabian Rzelicki      | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Marcel Biekakis      | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Ula Blakenbauer      | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |