---
categories: profile
factions: 
owner: public
title: Vanessa d'Cavalis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231202-protest-przed-kultem-na-szernief | biosyntka bojowa Aeriny; pomagała Aerinie w ukryciu jej nowej natury (kanibalistycznej) oraz porywała dla niej ludzi. Nieskończenie lojalna. | 0103-10-15 - 0103-10-19 |
| 231213-polowanie-na-biosynty-na-szernief | uczestniczyła w operacji eliminacji Parasekta za prośbą Delgado. Nadal chroni Artura Tavita jako jego lojalna strażniczka (i pojawiają się plotki że nie tylko jako strażniczka). | 0105-07-26 - 0105-07-31 |
| 240117-dla-swych-marzen-warto       | po pokonaniu mawirowca Esuriit który ją uszkodził poszła do Aeriny by ją ostrzec - tam uszkodził ją Jonatan. Chroni Artura i osłoniła go przed Maią (zabójczynią). Przyjęła jej pocisk na siebie i została zniszczona. KIA. W jej imieniu zbudowano pomnik i zrobiono integrację biosyntów i Szernief. | 0106-11-04 - 0106-11-06 |
| 240214-relikwia-z-androida          | została ikoną, symbolem, celem religijnym i miała posłużyć do wskrzeszenia Aliny. Przechwycona przez Agencję i naprawiona, z nową psychotroniką, została idolką i podcasterką turystyczną. | 0107-05-16 - 0107-05-18 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 240117-dla-swych-marzen-warto       | uznana za bohaterską biosyntkę stacji; w jej imieniu zbudowano pomnik integracji biosyntów i ludzi na Szernief | 0106-11-06

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kalista Surilik      | 4 | ((231202-protest-przed-kultem-na-szernief; 231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Hacker         | 4 | ((231202-protest-przed-kultem-na-szernief; 231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Aerina Cavalis       | 3 | ((231202-protest-przed-kultem-na-szernief; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Oficer Naukowy | 3 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Sabotażysta    | 3 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Artur Tavit          | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Felina Amatanir      | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Klasa Biurokrata     | 2 | ((231202-protest-przed-kultem-na-szernief; 231213-polowanie-na-biosynty-na-szernief)) |
| Klasa Dyplomata      | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240214-relikwia-z-androida)) |
| Mawir Hong           | 2 | ((240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Alina Mekran         | 1 | ((240214-relikwia-z-androida)) |
| Delgado Vitriol      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Elwira Barknis       | 1 | ((240214-relikwia-z-androida)) |
| Estril Cavalis       | 1 | ((240117-dla-swych-marzen-warto)) |
| Frederico Zyklas     | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Klasa Inżynier       | 1 | ((240214-relikwia-z-androida)) |
| Larkus Talvinir      | 1 | ((240117-dla-swych-marzen-warto)) |
| Maia Sakiran         | 1 | ((240117-dla-swych-marzen-warto)) |
| Malik Darien         | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| OLU Luminarius       | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Sebastian-194        | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Tadeusz Mekran       | 1 | ((240214-relikwia-z-androida)) |