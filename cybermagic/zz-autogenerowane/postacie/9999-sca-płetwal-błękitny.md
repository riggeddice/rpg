---
categories: profile
factions: 
owner: public
title: SCA Płetwal Błękitny
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210317-arianna-podbija-asimear      | dostał Bazyliszka od Inferni, przez co jest unieruchomiony w okolicach stacji Lazarin. Działają tylko systemy awaryjne. | 0111-10-18 - 0111-11-02 |
| 210414-dekralotyzacja-asimear       | miejsce bitwy pomiędzy Entropikiem Eleny i Morrigan d'Tirakal. Szczęśliwie, nie został bardzo poważnie uszkodzony a potem Eustachy i ekipa Inferni pomogli go trochę zreperować. | 0111-11-03 - 0111-11-12 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210317-arianna-podbija-asimear      | TAI Semla jest zniszczona Bazyliszkiem Inferni. | 0111-11-02

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Elena Verlen         | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Eustachy Korkoran    | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Klaudia Stryk        | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Martyn Hiwasser      | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Martyna Bianistek    | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Tomasz Sowiński      | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Jolanta Sowińska     | 1 | ((210317-arianna-podbija-asimear)) |
| Kamil Lyraczek       | 1 | ((210414-dekralotyzacja-asimear)) |
| Leona Astrienko      | 1 | ((210317-arianna-podbija-asimear)) |
| Llarnagraht          | 1 | ((210414-dekralotyzacja-asimear)) |
| Malictrix d'Pandora  | 1 | ((210414-dekralotyzacja-asimear)) |
| Mariusz Tubalon      | 1 | ((210414-dekralotyzacja-asimear)) |