---
categories: profile
factions: 
owner: public
title: Maria Gołąb
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190813-niesmiertelny-komandos-saitaera | komendant Rodu Arłaczów; w misji sformowania oddziału szturmowego do zniszczenia Alfreda Ceruleana. Na szczęście do tego nie doszło. Zimna, dumna i lojalna. | 0110-05-20 - 0110-05-25 |
| 201021-noktianie-rodu-arlacz        | bardzo kompetentna i paranoiczna; zrobiła research nt. załogi Inferni i skutecznie wyłączyła Ariannowe i Anastazjowe "Sowińska chce noktian". Świetny szef ochrony. | 0111-03-07 - 0111-03-10 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Robert Arłacz        | 2 | ((190813-niesmiertelny-komandos-saitaera; 201021-noktianie-rodu-arlacz)) |
| Alfred Cerulean      | 1 | ((190813-niesmiertelny-komandos-saitaera)) |
| Anastazja Sowińska   | 1 | ((201021-noktianie-rodu-arlacz)) |
| Arianna Verlen       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Ataienne             | 1 | ((201021-noktianie-rodu-arlacz)) |
| Diana Arłacz         | 1 | ((201021-noktianie-rodu-arlacz)) |
| Eliza Ira            | 1 | ((201021-noktianie-rodu-arlacz)) |
| Eustachy Korkoran    | 1 | ((201021-noktianie-rodu-arlacz)) |
| Izabela Zarantel     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Juliusz Sowiński     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Klaudia Stryk        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Wesoły Wieprzek   | 1 | ((201021-noktianie-rodu-arlacz)) |
| Robert Garwen        | 1 | ((190813-niesmiertelny-komandos-saitaera)) |
| Wanessa Pyszcz       | 1 | ((201021-noktianie-rodu-arlacz)) |