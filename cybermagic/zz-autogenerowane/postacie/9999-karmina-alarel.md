---
categories: profile
factions: 
owner: public
title: Karmina Alarel
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200930-lekarz-dla-elizy             | elitarna gwardzistka Noctis, pilot i przyjaciółka Elizy; doskonale pilotuje ścigacz, pokonała elementalnego potwora Skażonego ixionem i opanowała Elizę. | 0082-02-10 - 0082-02-14 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Autofort Imperatrix  | 1 | ((200930-lekarz-dla-elizy)) |
| Eliza Ira            | 1 | ((200930-lekarz-dla-elizy)) |
| Szymon Szynek        | 1 | ((200930-lekarz-dla-elizy)) |
| Wanessa Pyszcz       | 1 | ((200930-lekarz-dla-elizy)) |