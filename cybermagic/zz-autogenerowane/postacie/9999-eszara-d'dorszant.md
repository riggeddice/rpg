---
categories: profile
factions: 
owner: public
title: Eszara d'Dorszant
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191120-mafia-na-stacji-gorniczej    | TAI zarządcza stacji o trzecim poziomie topozoficznym. Wyłączyła się gdy Zespół przejął władzę. Od tej pory śpi, nieaktywna. | 0110-06-14 - 0110-06-25 |
| 191218-kijara-corka-szotaron        | zaakceptowała mafię jako głównodowodzących na Dorszant, ale zbudowała własną enklawę i ściągnęła orbiterowców. | 0110-07-04 - 0110-07-07 |
| 191123-echo-eszary-na-dorszancie    | mimo, że nieaktywna, nadal pozostawia za sobą długoterminowe echa i plany jak odzyskać kontrolę nad stacją. | 0110-07-11 - 0110-07-14 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Rafał Kirlat         | 3 | ((191120-mafia-na-stacji-gorniczej; 191123-echo-eszary-na-dorszancie; 191218-kijara-corka-szotaron)) |
| Filip Szczatken      | 2 | ((191123-echo-eszary-na-dorszancie; 191218-kijara-corka-szotaron)) |
| Gerwazy Kruczkut     | 2 | ((191120-mafia-na-stacji-gorniczej; 191123-echo-eszary-na-dorszancie)) |
| Jaromir Uczkram      | 2 | ((191120-mafia-na-stacji-gorniczej; 191218-kijara-corka-szotaron)) |
| Kamelia Termit       | 2 | ((191120-mafia-na-stacji-gorniczej; 191123-echo-eszary-na-dorszancie)) |
| Oliwier Pszteng      | 2 | ((191120-mafia-na-stacji-gorniczej; 191218-kijara-corka-szotaron)) |
| Szymon Szelmer       | 2 | ((191120-mafia-na-stacji-gorniczej; 191218-kijara-corka-szotaron)) |
| Azonia Arris         | 1 | ((191218-kijara-corka-szotaron)) |
| Felicja Taranit      | 1 | ((191218-kijara-corka-szotaron)) |
| Hestia Ain d'Dorszant | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Hestia d'Dorszant    | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Hestia Dis d'Dorszant | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Janet Erwon          | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Kijara d'Esuriit     | 1 | ((191218-kijara-corka-szotaron)) |
| Stefan Ukrand        | 1 | ((191120-mafia-na-stacji-gorniczej)) |