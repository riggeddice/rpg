---
categories: profile
factions: 
owner: public
title: Ralena Karimin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230729-furia-poluje-na-furie        | Furia, która została złapana przez siły Grzymościa i przejęta magią mentalną. Przeprowadziła operację złapania Xavery i mimo, że walczyła z TRZEMA Furiami (Amanda, Ayna, Xavera), prawie złapała Aynę. Gdyby chciała zabić a nie złapać, byłaby w stanie ze swoim oddziałem Wolnego Uśmiechu zniszczyć wszystkie trzy Furie | 0081-06-17 - 0081-06-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 1 | ((230729-furia-poluje-na-furie)) |
| Ayna Marialin        | 1 | ((230729-furia-poluje-na-furie)) |
| Ernest Kajrat        | 1 | ((230729-furia-poluje-na-furie)) |
| Xavera Sirtas        | 1 | ((230729-furia-poluje-na-furie)) |