---
categories: profile
factions: 
owner: public
title: Kot Rozbójnik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220814-stary-kot-a-trzesawisko      | Stary (16 lat) kot klasy Vistermin (vis - terminus), eksterminujący anomalie i przypisany do fortifarmy Lechotka. Z Przeniesieniem Ixiońskim. Próbował uchronić swoich ludzkich opiekunów przed Trzęsawiskiem; został przez Trzęsawisko poraniony. Gdy uciekł z kociarni Zawtrak (gdzie zaniosła go do żywych Karo) dał się uspokoić przez Ulę Miłkowicz. Z trackerem przeszedł przez Trzęsawisko by znaleźć bioformy swych opiekunów i widział bombardowanie artyleryjskie które ich zabiło. Został kotem Uli :-). | 0111-10-12 - 0111-10-13 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220814-stary-kot-a-trzesawisko      | został kotem Uli Miłkowicz. Jeszcze ze trzy lata pociągnie. Albo zginie. | 0111-10-13

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Bartozol        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Ariela Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Diana Tevalier       | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Iwan Zawtrak         | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Karolina Terienak    | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Marian Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Urszula Miłkowicz    | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Viirai d'Lechotka    | 1 | ((220814-stary-kot-a-trzesawisko)) |