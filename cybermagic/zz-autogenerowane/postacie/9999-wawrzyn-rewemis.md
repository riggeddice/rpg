---
categories: profile
factions: 
owner: public
title: Wawrzyn Rewemis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221230-dowody-na-istnienie-nox-ignis | medyk Loricatus; blisko zaprzyjaźniony z Persefoną d'Loricatus. Podobno ta TAI jest jego dziewczyną. | 0082-07-28 - 0082-08-01 |
| 211110-romans-dzieki-esuriit        | 72-letni weteran, medyk, 36% zmechanizowania, maska na twarzy. Klaudia ściągnęła go na Infernię do zajmowania się Leoną. Nie jest wybitny, ale może pokierować młodych. | 0112-04-13 - 0112-04-14 |
| 211124-prototypowa-nereida-natalii  | stary medyk ściągnięty przez Klaudię; gdy Leona weń rzuciła czymś wybuchającym to odrzucił. He doesn't give a meow. Trochę polubili się z Leoną. Eustachy zaszokowany jego działaniem. | 0112-04-20 - 0112-04-24 |
| 211208-o-krok-za-daleko             | KIA. Został na Inferni gdy Eustachy próbował ją opanować i ratował Leonę wszystkimi siłami. Uratował Leonę, ale samemu zginął. | 0112-04-25 - 0112-04-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 3 | ((211110-romans-dzieki-esuriit; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| Elena Verlen         | 3 | ((211110-romans-dzieki-esuriit; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| Eustachy Korkoran    | 3 | ((211110-romans-dzieki-esuriit; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| Klaudia Stryk        | 3 | ((211110-romans-dzieki-esuriit; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| Leona Astrienko      | 3 | ((211110-romans-dzieki-esuriit; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| Adam Szarjan         | 2 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| Maria Naavas         | 2 | ((211110-romans-dzieki-esuriit; 211124-prototypowa-nereida-natalii)) |
| OO Infernia          | 2 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| AK Nox Ignis         | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Aletia Nix           | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Diana d'Infernia     | 1 | ((211208-o-krok-za-daleko)) |
| Dominik Łarnisz      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Franz Szczypiornik   | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Hestia d'Neotik      | 1 | ((211208-o-krok-za-daleko)) |
| Izabela Zarantel     | 1 | ((211124-prototypowa-nereida-natalii)) |
| Jarosław Szarjan     | 1 | ((211208-o-krok-za-daleko)) |
| Karol Reichard       | 1 | ((211110-romans-dzieki-esuriit)) |
| Katrina Komczirp     | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Medea Sowińska       | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Natalia Aradin       | 1 | ((211124-prototypowa-nereida-natalii)) |
| OO Loricatus         | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Persefona d'Loricatus | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Roland Sowiński      | 1 | ((211124-prototypowa-nereida-natalii)) |
| Sarian Xadaar        | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Talia Derwisz        | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Tatiana Ozariat      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Tristan Ozariat      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |