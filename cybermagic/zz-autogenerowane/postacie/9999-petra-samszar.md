---
categories: profile
factions: 
owner: public
title: Petra Samszar
---

# {{ page.title }}


# Generated: 



## Fiszki


* 21, najszybsza z dziennikarek, przyszła superstar dziennikarstwa | @ 230711-zablokowana-sentisiec-w-krainie-makaronu
* OCEAN: (E+A-): rywalizująca i zawsze chce mieć rację, uwielbia prędkość i widoki. Łatwo się złości. "Wszystkie oczy NA MNIE!" | @ 230711-zablokowana-sentisiec-w-krainie-makaronu
* VALS: (Face, Achievement): wiecznie znudzona, trenuje prędkość, drony i dziennikarstwo. Barbed quips. Dotrzymuje słowa. "To wszystko na co was stać?! Szybciej! MOCNIEJ!" | @ 230711-zablokowana-sentisiec-w-krainie-makaronu
* Core Wound - Lie: "niezauważana wśród rodzeństwa, nieważna, najmniej zdolna" - "I must rise like a superstar! I must be famous and awesome! Then they will see me!" | @ 230711-zablokowana-sentisiec-w-krainie-makaronu
* styl: superstar reporter śledczy, drama queen, "_Nie możesz ignorować prasy. Albo będziesz współpracował, albo sama się wszystkiego o Tobie dowiem._" | @ 230711-zablokowana-sentisiec-w-krainie-makaronu

### Wątki


brak

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230711-zablokowana-sentisiec-w-krainie-makaronu | jako tien na włościach miała chronić Triticatus, ale była znudzona; chciała pokonać Wiktora Blakenbauera i pokazać mu że jako alchemik jest mało groźny. Myliła się. Po tym jak zablokowała sentisieć ścięła się z Eleną S. i gdy TAMTA zablokowała sentisieć na dobre, Petra przeprosiła Wiktora i chciała wycofać działania. Uznała, że Elena to psychopatka. Chciała współpracować JEŚLI będzie przeproszona. Nie została przeproszona, tylko wzgardzona przez zarówno Karolinusa i Elenę. | 0095-09-05 - 0095-09-08 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230711-zablokowana-sentisiec-w-krainie-makaronu | wie, że Karolinus i Elena S. nie są pozytywni. To okrutne osoby a Elena to czysta psychopatka. Petra - jako dziennikarka - wykaże mrok tej parki. | 0095-09-08

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Elena Samszar        | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Ignatus Blakenbauer  | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Irek Kraczownik      | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Karolinus Samszar    | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Wacław Samszar       | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Wiktor Blakenbauer   | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |