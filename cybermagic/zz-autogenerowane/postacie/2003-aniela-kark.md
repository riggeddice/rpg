---
categories: profile
factions: 
owner: kić
title: Aniela Kark
---

# {{ page.title }}


# Read: 

## Metadane

* factions: ""
* type: "NPC"
* owner: "kić"
* title: "Aniela Kark"
 

## Postać

ARCHETYP: 	
Koncept, kim jest ta osoba? (3 żetony)
Lekarz, co wszystko wyszczurzy.


Jakie ma silne strony? (3 żetony)
Ciało / Fizyczne	
* Serce / Społeczne
Wiedza / Naukowo-Biurokratyczne	
* Technika / Narzędzia I Konstrukcja	
Eter / Magia i Intuicja

Czym osiąga sukcesy? (3 żetony)
Konfrontacja / bezpośrednio, agresywnie	
* Wpływanie / manipulacja, manewry	
Wspieranie / dowodzenie, wzmacnianie, budowanie
1. Stymulanty, lekarstwa, środki biochemiczne
2. 
3. 

Jakie zasoby ma do dyspozycji? (3 żetony + konsekwencja)
Uczniowie, którym pomogła.




# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200326-test-z-etyki                 | AMZ; pozyskała sporo informacji na temat Liliany, jej sił i słabości. Dodatkowo jako lekarka ustabilizowała Napoleona pokąsanego przez dziwne rzeczy. | 0110-07-29 - 0110-07-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Berenika Wrążowiec   | 1 | ((200326-test-z-etyki)) |
| Ignacy Myrczek       | 1 | ((200326-test-z-etyki)) |
| Liliana Bankierz     | 1 | ((200326-test-z-etyki)) |
| Napoleon Bankierz    | 1 | ((200326-test-z-etyki)) |
| Teresa Mieralit      | 1 | ((200326-test-z-etyki)) |