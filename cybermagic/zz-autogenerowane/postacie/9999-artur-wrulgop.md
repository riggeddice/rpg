---
categories: profile
factions: 
owner: public
title: Artur Wrulgop
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230810-nie-nazywasz-sie-janusz      | medyk Syndykatu, świetny w łamanie umysłów przy użyciu neuroobroży. Poleciał z Januszem do Wolnych TAI i usunął z niego infektora Saitaera. | 0093-03-29 - 0093-03-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dariusz Avatrinn     | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Janusz Lemuel        | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Krzesimir Pluszcz    | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Łucja Kritoriin      | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Maja Nerwial         | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Nova Atreia          | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| OWT Łowca Martwych Gwiazd | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| TAI Xaira Cognitia   | 1 | ((230810-nie-nazywasz-sie-janusz)) |
| Teresa Kritoriin     | 1 | ((230810-nie-nazywasz-sie-janusz)) |