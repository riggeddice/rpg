---
categories: profile
factions: 
owner: public
title: Delgado Vitriol
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231213-polowanie-na-biosynty-na-szernief | morderczy biosynt przeciwny ludziom i ludzkości; eksterminuje ludzi atakujących inne biosynty i dowodzi lokalnymi biosyntami. Próbował skrzywdzić Kalistę, ale Sebastian go zatrzymał. Gdy tylko Agencja pokazała że jest skłonna też zabić ludzi, zgodził się pomóc w zniszczeniu Parasekta - za system ładowania który mogą wykorzystać w głębi stacji Szernief. | 0105-07-26 - 0105-07-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Artur Tavit          | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Felina Amatanir      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Kalista Surilik      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Klasa Biurokrata     | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Klasa Dyplomata      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Klasa Hacker         | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Klasa Oficer Naukowy | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Klasa Sabotażysta    | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Malik Darien         | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Sebastian-194        | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Vanessa d'Cavalis    | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |