---
categories: profile
factions: 
owner: public
title: Dormand Miraris
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240110-wieczna-wojna-bladawira      | (nieobecny) kapitan Orbitera i przyjaciel Ernesta Bankierza; ogólnie (z danych Klaudii) ma jakieś związki z narkotykami imprezowymi. | 0110-12-24 - 0110-12-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Bladawir      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Arianna Verlen       | 1 | ((240110-wieczna-wojna-bladawira)) |
| Ernest Bankierz      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Kazimierz Darbik     | 1 | ((240110-wieczna-wojna-bladawira)) |
| Klaudia Stryk        | 1 | ((240110-wieczna-wojna-bladawira)) |
| Marian Witaczek      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Samuel Fanszakt      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Talia Miraris        | 1 | ((240110-wieczna-wojna-bladawira)) |
| Władawiec Diakon     | 1 | ((240110-wieczna-wojna-bladawira)) |
| Zaara Mieralit       | 1 | ((240110-wieczna-wojna-bladawira)) |