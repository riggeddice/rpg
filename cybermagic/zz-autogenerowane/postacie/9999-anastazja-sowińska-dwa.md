---
categories: profile
factions: 
owner: public
title: Anastazja Sowińska Dwa
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201230-pulapka-z-anastazji          | stworzona przez Kryptę jako "trochę lepsza" wersja Anastazji, pułapka na Ariannę i na Sowińskich. Przechwycona przez Ariannę; atm na Inferni. | 0111-07-19 - 0111-07-20 |
| 210127-porwanie-anastazji-z-odkupienia | lepsza wersja Anastazji, połączyła Kryptę z sentisiecią Odkupienia i wygenerowała falę leczniczą, dezorganizując Kryptę. Skończyła w biovacie. Szanuje Ariannę i Elenę. | 0111-07-22 - 0111-07-23 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210127-porwanie-anastazji-z-odkupienia | śpi wiecznym snem w biovacie na Kontrolerze Pierwszym, o czym nikt nie wie | 0111-07-23

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia)) |
| Elena Verlen         | 2 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia)) |
| Eustachy Korkoran    | 2 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia)) |
| Klaudia Stryk        | 2 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia)) |
| Martyn Hiwasser      | 2 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia)) |
| OO Infernia          | 2 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia)) |
| AK Nocna Krypta      | 1 | ((201230-pulapka-z-anastazji)) |
| AK Rodivas           | 1 | ((201230-pulapka-z-anastazji)) |
| Damian Orion         | 1 | ((201230-pulapka-z-anastazji)) |
| Dariusz Krantak      | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Diana Arłacz         | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Henryk Sowiński      | 1 | ((201230-pulapka-z-anastazji)) |
| OA Odkupienie        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OA Zguba Tytanów     | 1 | ((201230-pulapka-z-anastazji)) |
| Rufus Niegnat        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| SP Plugawy Jaszczur  | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |