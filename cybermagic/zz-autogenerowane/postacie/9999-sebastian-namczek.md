---
categories: profile
factions: 
owner: public
title: Sebastian Namczek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200408-o-psach-i-krysztalach        | kiedyś kapitan piratów; teraz wojownik w służbie kryształu Leotisa. Pokonany przez Leonę i odkrysztalniony przez Eustachego, pomógł w zniszczeniu Leotis. KIA. | 0110-09-29 - 0110-10-04 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((200408-o-psach-i-krysztalach)) |
| Eustachy Korkoran    | 1 | ((200408-o-psach-i-krysztalach)) |
| Kamil Lyraczek       | 1 | ((200408-o-psach-i-krysztalach)) |
| Klaudia Stryk        | 1 | ((200408-o-psach-i-krysztalach)) |
| Leona Astrienko      | 1 | ((200408-o-psach-i-krysztalach)) |
| Martyn Hiwasser      | 1 | ((200408-o-psach-i-krysztalach)) |