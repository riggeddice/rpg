---
categories: profile
factions: 
owner: public
title: Kazimierz Darbik
---

# {{ page.title }}


# Generated: 



## Fiszki


* OCEAN: (E- N+) "Cisza przed burzą jest najgorsza." | VALS: (Power, Achievement) "Tylko zwycięstwo liczy się." | DRIVE: "Odzyskać to, co straciłem." | @ 231115-cwiczenia-komodora-bladawira
* kapitan sił Orbitera pod dowództwem Antoniego Bladawira | @ 231115-cwiczenia-komodora-bladawira

### Wątki


triumfalny-powrot-arianny
naprawa-swiata-przez-bladawira

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231220-bladawir-kontra-przemyt-tienow | p.d. Bladawira i kapitan OO Actazir, zimny i uczciwy, acz bardzo 'gadzi'; gdy Eustachy się rzucał, upomniał go za ton ale udostępnił sprzęt jakiego Eustachy zażądał. Acz Eustachy ma u niego krechę. | 0110-12-06 - 0110-12-11 |
| 240110-wieczna-wojna-bladawira      | nie boi się dyskutować z Bladawirem i nawet się wstawił za Arianną przed Bladawirem (nie znając kontekstu historii jej i Bladawira) | 0110-12-24 - 0110-12-27 |
| 240125-wszystkie-sekrety-caralei    | nie jest zadowolony z nadmiernej brutalności swojej załogi w sprawie przemytu; przemyt verminusów jest PRAWIDŁOWY choć nielegalny. Ale wyjaśnił co zrobili źle i pozwolił im działać po swojemu. | 0110-12-29 - 0111-01-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Bladawir      | 3 | ((231220-bladawir-kontra-przemyt-tienow; 240110-wieczna-wojna-bladawira; 240125-wszystkie-sekrety-caralei)) |
| Arianna Verlen       | 2 | ((231220-bladawir-kontra-przemyt-tienow; 240110-wieczna-wojna-bladawira)) |
| Klaudia Stryk        | 2 | ((231220-bladawir-kontra-przemyt-tienow; 240110-wieczna-wojna-bladawira)) |
| Zaara Mieralit       | 2 | ((231220-bladawir-kontra-przemyt-tienow; 240110-wieczna-wojna-bladawira)) |
| Dobromir Misiak      | 1 | ((240125-wszystkie-sekrety-caralei)) |
| Dormand Miraris      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Elena Verlen         | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Ernest Bankierz      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Eustachy Korkoran    | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Ewa Razalis          | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Marcinozaur Verlen   | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Marian Witaczek      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Olaf Rawen           | 1 | ((240125-wszystkie-sekrety-caralei)) |
| OO Infernia          | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| OO Karsztarin        | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Raoul Lavanis        | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Samuel Fanszakt      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Szymon Orzesznik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Talia Miraris        | 1 | ((240110-wieczna-wojna-bladawira)) |
| Tog Worniacz         | 1 | ((240125-wszystkie-sekrety-caralei)) |
| Władawiec Diakon     | 1 | ((240110-wieczna-wojna-bladawira)) |