---
categories: profile
factions: 
owner: public
title: SP Plugawy Jaszczur
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210127-porwanie-anastazji-z-odkupienia | bardzo groźny statek piracki; Infernia zamaskowała się jako oni by porwać Anastazję. Słyną z brawurowych akcji. | 0111-07-22 - 0111-07-23 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210127-porwanie-anastazji-z-odkupienia | Sowińscy go nienawidzą; szukają go by odzyskać swoją ukochaną Anastazję. A Jaszczur jest niewinny. | 0111-07-23

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazja Sowińska Dwa | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Arianna Verlen       | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Dariusz Krantak      | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Diana Arłacz         | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Elena Verlen         | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Eustachy Korkoran    | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Klaudia Stryk        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Martyn Hiwasser      | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OA Odkupienie        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OO Infernia          | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Rufus Niegnat        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |