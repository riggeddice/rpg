---
categories: profile
factions: 
owner: public
title: SP Pallida Voss
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220126-keldan-voss-kolonia-saitaera | statek Pallidan krążący nad Keldan Voss, którego celem jest opanować sytuację ze zbuntowaną kolonią. | 0112-05-24 - 0112-05-27 |
| 220216-polityka-rujnuje-pallide-voss | staje się Anomalią Kosmiczną po tym jak Eustachy wysadził generatory Memoriam i miał Paradoks... ogromne straty wśród pallidan. I na pokładzie pojawiła się Mgła. | 0112-06-01 - 0112-06-03 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Annika Pradis        | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Arianna Verlen       | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Elena Verlen         | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Eustachy Korkoran    | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Klaudia Stryk        | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Mateus Sarpon        | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Szczepan Kaltaben    | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Kormonow Voss        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| OO Kastor            | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Raoul Lavanis        | 1 | ((220216-polityka-rujnuje-pallide-voss)) |
| Zygfryd Maus         | 1 | ((220126-keldan-voss-kolonia-saitaera)) |