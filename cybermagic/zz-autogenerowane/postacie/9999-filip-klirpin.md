---
categories: profile
factions: 
owner: public
title: Filip Klirpin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230620-karolinus-sedzia-mirkali     | burmistrz Mirkali; należy do Starszyzny rządzącej Mirkalą i robiącą drugi, nielegalny obieg. Współpracuje z Karolinusem. Chce się pozbyć Juanity Derwisz, bo nie da się jej kontrolować. Słucha głosu rozsądku Karolinusa i pracuje nad naprawą miasta. | 0095-04-15 - 0095-04-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Agnieszka Klirpin    | 1 | ((230620-karolinus-sedzia-mirkali)) |
| AJA Szybka Strzała   | 1 | ((230620-karolinus-sedzia-mirkali)) |
| Juanita Derwisz      | 1 | ((230620-karolinus-sedzia-mirkali)) |
| Karolinus Samszar    | 1 | ((230620-karolinus-sedzia-mirkali)) |