---
categories: profile
factions: 
owner: public
title: Aleksander Rugczuk
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190424-budowa-ixionskiego-mimika    | Miasteczkowiec; pijak niewiele chcący od życia; kiedyś, członek Inwazji Noctis. Dotknięty przez mimika ixiońskiego, stał się terrorformem - zestrzelony przez Pustogor. | 0110-04-03 - 0110-04-05 |
| 190429-sabotaz-szeptow-elizy        | bardzo żałuje, że mimik zmusił go do niektórych rzeczy. Wie, że jest w nim ta chęć dalszej walki, ale nie zamierza walczyć. Też współpracuje - co mu zostało? | 0110-04-10 - 0110-04-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Erwin Galilien       | 2 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Karla Mrozik         | 2 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Minerwa Metalia      | 2 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Olaf Zuchwały        | 2 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Pięknotka Diakon     | 2 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Alan Bartozol        | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Diana Tevalier       | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Eliza Ira            | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Lucjusz Blakenbauer  | 1 | ((190429-sabotaz-szeptow-elizy)) |