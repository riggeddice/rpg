---
categories: profile
factions: 
owner: public
title: Juliusz Cieślawok
---

# {{ page.title }}


# Generated: 



## Fiszki


*  | @ 231027-planetoida-bogatsza-niz-byc-powinna
* OCEAN: A-O- |Fatalne żarty;; Antagonizuje;; Ciemięży ludzi| VALS: Stimulation, Universalism | DRIVE: Niska zabawa i ciemiężenie bliźniego | @ 231027-planetoida-bogatsza-niz-byc-powinna

### Wątki


planetoidy-kazimierza
sekret-mirandy-ceres

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 231027-planetoida-bogatsza-niz-byc-powinna | żołnierz Blakvelowców, który chciałby jak największej ilości ludzi zabrać kasę i ich pociemiężyć. Dowodzi operacją bo nikt inny (Kazimierz) nie chce. Gdyby nie Miranda, zostałby Zmieniony przez Alteris. | 0108-06-29 - 0108-07-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Bartek Burbundow     | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Gotard Kicjusz       | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Grzegorz Fabutownik  | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Kazimierz Zamglis    | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Łucjan Torwold       | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Mikołaj Resztkowiec  | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Miranda Ceres        | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Prokop Umarkon       | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| SC Królowa Przygód   | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Seweryn Grzęźlik     | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |