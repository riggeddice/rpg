---
categories: profile
factions: 
owner: public
title: Strażniczka Alair
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210926-nowa-strazniczka-amz         | TAI kontrolujące Akademię Magiczną Zaczęstwa; wchodzi online. Niestabilna. Ma osobowość, chce chronić i ma dość zabijania. Tak nieufna wobec "nowych ludzi" jak oni wobec niej. Integrowana z komponentami BIA. Dużo potężniejsza (i więcej może) niż ktokolwiek myśli - łącznie z Tymonem. Baza: Eszara. | 0084-06-14 - 0084-06-26 |
| 211009-szukaj-serpentisa-w-lesie    | przechwyciła sygnały Klaudii jak tylko drona Klaudii/Strażniczki przedostała się przez zagłuszanie. Odpowiednio przekazała info Tymonowi, który przeprowadził operację ratunkową (na szczęście, nie musiał bojowej - o czym nie wiedział). | 0084-11-13 - 0084-11-14 |
| 211010-ukryta-wychowanka-arnulfa    | elementy TAI w kolizji z BIA po wykryciu Teresy (noktianki) wprowadziły ją w nieskończoną pętlę i zaczęła chronić AI Core i przeżywać jeden z przeszłych koszmarów. Klaudia zanomalizowała jej generatory, przez co Strażniczka zasnęła. | 0084-12-11 - 0084-12-12 |
| 200414-arystokraci-na-trzesawisku   | TAI Akademii Magicznej w Zaczęstwie. Jest tu jakiś sekret Tymona; Strażniczka przedstawia się jako Hestia ale jej psychotronika jest dużo wyżej. | 0110-08-04 - 0110-08-05 |
| 200417-nawolywanie-trzesawiska      | Pięknotka jest przekonana, że jest najlepszą rzeczą jaka zdarzyła się Zaczęstwu. Ostrzegła Pięknotkę że Sabina Kazitan czegoś szuka. Lepsza niż aktualny terminus. | 0110-08-12 - 0110-08-14 |
| 200425-inflitrator-poluje-na-tai-minerwy | naprawdę Eszara d'AlephAiren; z uwagi na wpływ Trzęsawiska na Cyberszkołę w coraz gorszej formie. Operuje na coraz mniejszej mocy. Tymon się martwi. | 0110-08-24 - 0110-08-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 3 | ((210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa)) |
| Ksenia Kirallen      | 3 | ((210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa)) |
| Mariusz Trzewń       | 3 | ((200414-arystokraci-na-trzesawisku; 210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie)) |
| Pięknotka Diakon     | 3 | ((200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska; 200425-inflitrator-poluje-na-tai-minerwy)) |
| Talia Aegis          | 3 | ((200414-arystokraci-na-trzesawisku; 210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Arnulf Poważny       | 2 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Felicjan Szarak      | 2 | ((211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa)) |
| Gabriel Ursus        | 2 | ((200417-nawolywanie-trzesawiska; 200425-inflitrator-poluje-na-tai-minerwy)) |
| Ignacy Myrczek       | 2 | ((200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska)) |
| Sabina Kazitan       | 2 | ((200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska)) |
| Tymon Grubosz        | 2 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Agostino Karwen      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Albert Kalandryk     | 1 | ((210926-nowa-strazniczka-amz)) |
| Dariusz Skórnik      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Edelmira Neralis     | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Erwin Galilien       | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Franciszek Leszczowik | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Grzegorz Czerw       | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Laura Tesinik        | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Minerwa Metalia      | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Napoleon Bankierz    | 1 | ((200417-nawolywanie-trzesawiska)) |
| Olaf Zuchwały        | 1 | ((200417-nawolywanie-trzesawiska)) |
| Sasza Morwowiec      | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Teresa Mieralit      | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Tomasz Tukan         | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Udom Rapnak          | 1 | ((211009-szukaj-serpentisa-w-lesie)) |