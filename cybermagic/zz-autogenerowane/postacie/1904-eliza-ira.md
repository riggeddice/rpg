---
categories: profile
factions: 
owner: public
title: Eliza Ira
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "pozostalosci eris"
* owner: "public"
* title: "Eliza Ira"


## Postać

### Ogólny pomysł (2)

* Dowódca TerrorShipa Eris
* Ekspert od krystaloform i hodowania kryształów; naukowiec

### Wyróżnik (2)

* Arcymag
* Silnie sprzężona z Multivirtem
* Kryształowe plagi i krystaloformy

### Misja (co dla innych) (2)

* Zwycięstwo ludzkości nad anomaliami i bogami
* Czystość i wolność wyboru dla wszystkich
* Znalezienie dobrego miejsca dla swoich ludzi

### Motywacja (co dla mnie) (2)

* Odkupienie tego co zrobiła (lub nie zrobiła)
* Przebaczenie od "swoich", z Eris lub kultury Eris
* Zapamiętanie przeszłości; zachowanie kultury i stylu życia Eris; legacy

### Zasoby i otoczenie (3)

* Powszechnie znienawidzona i uważana za przerażającą - zarówno przez Eris jak i przez Astorię
* Grupka lojalnych jej ludzi i magów chcących podążać jej ścieżką
* Kompleks Kryształowej Fortecy na wschodzie Cieniaszczytu
* Liczne technologie z czasów floty inwazyjnej Eris

### Magia (3)

#### Gdy kontroluje energię

* Arcymagini lodu i kryształów
* Kryształowe plagi oraz krystaloformy

#### Gdy traci kontrolę

* Anomalie krystaliczne, kryształowe plagi i inne takie

### Powiązane frakcje

{{ page.factions }}

## Opis

(10 min)


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200930-lekarz-dla-elizy             | jeszcze nie arcymagini. Na wpół sparaliżowana, niweluje Skażenie i wątpi w swoje decyzje o nie niszczeniu Astorii. Pierwszy raz użyła magii by zamaskować ścigacz kryształami. | 0082-02-10 - 0082-02-14 |
| 190313-plaga-jamnikow               | chciała zdobyć stare systemy bojowe. Zamiast tego dostała je zsabotowane plus dwóch górników (czego nie chciała). I jedno ciało martwego kompana. | 0110-02-28 - 0110-03-01 |
| 190418-spozniona-wojna-elizy        | arcymagini kryształów, która odparła Saitaera i Ataienne krystalizując swą nienawiść. Po 10 latach uderzyła w Trzeci Raj by odzyskać swój oddział po uprzednim wyłączeniu z akcji Ataienne. Zespół ją odtajał i kupił szansę Trzeciemu Rajowi. | 0110-03-20 - 0110-03-22 |
| 190429-sabotaz-szeptow-elizy        | zakłada kulty i sieci komunikacyjne przez kryształy. Po akcji Zespołu, koszt używania tych kryształów i jej Matrycy znacząco wzrósł. Ale ma już jakieś kontakty. | 0110-04-10 - 0110-04-13 |
| 190519-uciekajacy-seksbot           | mimo osłabionego połączenia robiła co była w stanie by móc pomóc seksbotowi, by ów mógł być wolny i nie musiał wracać do Ernesta. Utraciła połączenie przez Pięknotkę. | 0110-04-25 - 0110-04-26 |
| 200923-magiczna-burza-w-raju        | osłabiła burzę magiczną, która spowodowałaby dużo większe zniszczenia w Ruinie Trzeciego Raju, acz tak, by nikt nie wiedział. Poluje na kompetentnych arcymagów. | 0111-02-21 - 0111-02-25 |
| 201014-krystaliczny-gniew-elizy     | weszła do Trzeciego Raju po śmierci mindwarpowanych komandosów noktiańskich; zażądała oddania jej wszystkich noktian. Jak nie - wszyscy poza noktianami zginą. | 0111-03-02 - 0111-03-05 |
| 201021-noktianie-rodu-arlacz        | przekonana przez Ariannę, że jeśli przejmie Trzeci Raj to nie ma szans na integrację noktian z astorianami. Zrezygnowała z aktualnej walki - ale będzie chronić Raj. | 0111-03-07 - 0111-03-10 |
| 190326-arcymag-w-raju               | zdecydowała się na uratowanie swoich ludzi albo śmierć. Skończyła w sojuszu z Orbiterem Pierwszym, by razem odzyskali jak najwięcej Astorii dla ludzkości. | 0111-07-18 - 0111-07-19 |
| 190521-dwa-stare-miragenty          | nic nie wie o tym, by zniszczyła grupę archeologów. Pomogła Zespołowi tłumacząc zasady działania kryształów i krystalicznych struktur Noctis. | 0111-07-26 - 0111-07-28 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 200930-lekarz-dla-elizy             | dookoła niej jest bezpiecznie; anomalie się nie materializują. Jest neutralizatorem chorych energii. | 0082-02-14
| 200930-lekarz-dla-elizy             | silna zależność emocjonalna od Karminy i Wanessy. Nienawiść do siebie. Oraz - pragnienie ochrony noktian i zbudowanie dla nich bezpiecznego miejsca. | 0082-02-14
| 190226-korporacyjna-wojna-w-mmo     | jako "Iglica" przejęła dowodzenie nad EliSquid. Kryształowy Pałac jest zagrożony jak nigdy ;-). | 0110-03-17
| 190418-spozniona-wojna-elizy        | arcymagini kryształów odtajała. Nie jest napędzana jedynie skrystalizowaną nienawiścią do Astorii i dawnej Elizy. | 0110-03-22
| 190326-arcymag-w-raju               | odzyskała kilkanaście osób chcących z nią współpracować. Dodatkowo, weszła w ostrożny sojusz z Orbiterem Pierwszym. | 0111-07-19
| 190326-arcymag-w-raju               | nienawiść wielu z jej dawnej frakcji do niej jest niesamowita. Tak samo jak nienawiść wielu z Orbitera. Nie jest niczyją ulubioną osobą. | 0111-07-19

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazja Sowińska   | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Arianna Verlen       | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Ataienne             | 3 | ((190326-arcymag-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Eustachy Korkoran    | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Klaudia Stryk        | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Dariusz Krantak      | 2 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Elena Verlen         | 2 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Fergus Salien        | 2 | ((190326-arcymag-w-raju; 190521-dwa-stare-miragenty)) |
| Izabela Zarantel     | 2 | ((200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz)) |
| Marian Fartel        | 2 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Olga Leszcz          | 2 | ((190326-arcymag-w-raju; 190521-dwa-stare-miragenty)) |
| OO Wesoły Wieprzek   | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Pięknotka Diakon     | 2 | ((190429-sabotaz-szeptow-elizy; 190519-uciekajacy-seksbot)) |
| Wanessa Pyszcz       | 2 | ((200930-lekarz-dla-elizy; 201021-noktianie-rodu-arlacz)) |
| Alan Bartozol        | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Aleksander Rugczuk   | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Aleksandra Termia    | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Alojzy Wypyszcz      | 1 | ((190521-dwa-stare-miragenty)) |
| Alsa Fryta           | 1 | ((190313-plaga-jamnikow)) |
| Arnulf Poważny       | 1 | ((190519-uciekajacy-seksbot)) |
| Autofort Imperatrix  | 1 | ((200930-lekarz-dla-elizy)) |
| Damian Drób          | 1 | ((190418-spozniona-wojna-elizy)) |
| Dawid Szardak        | 1 | ((190326-arcymag-w-raju)) |
| Diana Arłacz         | 1 | ((201021-noktianie-rodu-arlacz)) |
| Diana Tevalier       | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Ernest Kajrat        | 1 | ((190519-uciekajacy-seksbot)) |
| Erwin Galilien       | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Fred Wypyszcz        | 1 | ((190521-dwa-stare-miragenty)) |
| Grzegorz Kamczarnik  | 1 | ((190326-arcymag-w-raju)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Juliusz Sowiński     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Karla Mrozik         | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Karmina Alarel       | 1 | ((200930-lekarz-dla-elizy)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Liliana Bankierz     | 1 | ((190519-uciekajacy-seksbot)) |
| Lucjusz Blakenbauer  | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Marianna Lemurczak   | 1 | ((200923-magiczna-burza-w-raju)) |
| Minerwa Metalia      | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Nikodem Sowiński     | 1 | ((200923-magiczna-burza-w-raju)) |
| Olaf Zuchwały        | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Olga Myszeczka       | 1 | ((190313-plaga-jamnikow)) |
| OO Castigator        | 1 | ((190326-arcymag-w-raju)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Ossidia Saitis       | 1 | ((190519-uciekajacy-seksbot)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Robert Garwen        | 1 | ((190418-spozniona-wojna-elizy)) |
| Saitaer              | 1 | ((190519-uciekajacy-seksbot)) |
| Szymon Szynek        | 1 | ((200930-lekarz-dla-elizy)) |
| Teresa Mieralit      | 1 | ((190519-uciekajacy-seksbot)) |
| Tomasz Tukan         | 1 | ((190519-uciekajacy-seksbot)) |
| Tomek Żuchwiacz      | 1 | ((190313-plaga-jamnikow)) |