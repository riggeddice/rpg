---
categories: profile
factions: 
owner: public
title: OO Infernia
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221006-ona-chce-dziecko-eustachego  | zintegrowała się z Eustachym po raz pierwszy; obcy umysł Inferni Dotknął Eustachego i uznał go godnym. Zestrzeliła dla Eustachego kilka Farighanów. | 0092-09-20 - 0092-09-24 |
| 220720-infernia-taksowka-dla-lycoris | główny okręt obronny Arkologii Nativis; zdolny do działania w burzach piaskowych i praktycznie dowolnych plugawych warunkach. Pod dowodzeniem Bartłomieja Korkorana. Tylko Bartłomiej zna sekrety Inferni - i Emilia d'Erozja. | 0093-01-20 - 0093-01-22 |
| 230201-wylaczone-generatory-memoriam-inferni | w rzeczywistości pancerz okrywający straszliwą anomalię Interis, która jest uśpiona generatorami Memoriam. Eustachy się z nią sprzęgł; Infernia weszła w jego głowę. Nie dba o nic, pragnie siać zniszczenie. Wreszcie wolna i tylko Eustachy może nią sterować (ostatnie ogniwo do uwolnienia tego co kryje Infernia). Dominuje nawet Persefonę. | 0093-02-10 - 0093-02-12 |
| 230208-pierwsza-randka-eustachego   | okazuje się, że za rządów Bartłomieja Korkorana stała się symbolem nadziei dla Nativis. Ludzie rozpoznają ją jako pozytywną jednostkę która przyniesie dobro. | 0093-02-14 - 0093-02-21 |
| 230315-bardzo-nieudane-porwanie-inferni | gdy Ardilla uszkodziła kolejne generatory Memoriam, pożarła część napastników na swoim pokładzie. Do Inferni dołączyła część piratów z woli Eustachego. Infernia będzie o nich dbać... | 0093-03-06 - 0093-03-09 |
| 230329-zdrada-rozrywajaca-arkologie | szybko i sprawnie przechwyciła Skorpiona należącego do Szczepana zanim ten zrobił coś głupiego. Wyciągnęła Skorpiona z dołu "na hol", z lekkimi tylko uszkodzeniami owego Skorpiona. | 0093-03-14 - 0093-03-16 |
| 230614-atak-na-kidirona             | conduit dla magii Eustachego; wzmocniona przez entropię. Okazuje się, że ma już tylko 40% aktywnych generatorów memoriam i że PRZEKSZTAŁCIŁA kiedyś jednego maga, którego Bartłomiej Korkoran musiał zabić. | 0093-03-22 - 0093-03-24 |
| 230621-infiltrator-ucieka-a-arkologia-plonie | proponuje Eustachemu serię planów które mogą zabić Kalię. Infernia nie dba o Kalię. Chce Eustachego od niej odepchnąć. | 0093-03-25 - 0093-03-26 |
| 230719-wojna-o-arkologie-nativis-nowa-regentka | najbezpieczniejsze miejsce w okolicach Arkologii Nativis. Będące anomalią Nihilusa kuszącą Eustachego do zdjęcia kolejnych generatorów memoriam. | 0093-03-28 - 0093-03-29 |
| 230726-korkoran-placi-cene-za-nativis | nadal kusi Eustachego; tym razem pokazuje mu Izabellę na kolanach przed nim, zrekonstruowaną mocą Nihilusa w posłuszną agentkę Eustachego | 0093-03-29 - 0093-03-30 |
| 231011-ekstaflos-na-tezifeng        | szybka i skuteczna; przechwyciła Tezifeng i z woli Eustachego ją ostrzelała. Zero uszkodzeń, zero nieefektywności. Idealna misja dla Inferni. | 0110-10-20 - 0110-10-22 |
| 231025-spiew-nielalki-na-castigatorze | overcharguje generatory Memoriam, podlatuje do Castigatora i chroni mostek używając swoich Memoriam. Jej przeładowanie generatorami Memoriam jedynie bardzo pomogło. Plus, zapewnia prawdę swoimi sensorami. | 0110-10-24 - 0110-10-26 |
| 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira | obecność Zaary na ćwiczeniach i typ ćwiczeń (i nieobecność Eustachego) sprawiły, że Infernia miała prawdziwie skomplikowaną operację i na poziomie morale i zdolności. Klaudia jako oficer taktyczny dała radę, toteż Kamil jako oficer morale. Infernia przeszła przez pole minowe, 'straciła pancerz', ale 'zestrzeliła' dwie jednostki w absolutnej przewadze. | 0110-11-26 - 0110-11-30 |
| 231220-bladawir-kontra-przemyt-tienow | pod kontrolą Bladawira, z zewnętrznymi silnikami dzięki Eustachemu, prowadzi operacje celne i szukające przemytu tienów (i innych rzeczy). Sama ma przemyt na pokładzie przez co Arianna ma OPR. | 0110-12-06 - 0110-12-11 |
| 201210-pocalunek-aspirii            | QShip; udawała OO Blask Aurum. Lekko uszkodzona, ale pułapkując zniszczyła Pocałunek Aspirii. | 0111-03-26 - 0111-03-29 |
| 201230-pulapka-z-anastazji          | pobiła rekord prędkości z Eleną u steru, Arianną jako siła magiczna i Eustachym przesterowującym systemy - dzięki temu przegoniła Zgubę Tytanów. | 0111-07-19 - 0111-07-20 |
| 210127-porwanie-anastazji-z-odkupienia | pod dowodzeniem Klaudii wykonała operację "odbijmy Anastazję od Sowińskich, z Odkupienia". Ma (słuszną) reputację psującej się. | 0111-07-22 - 0111-07-23 |
| 210218-infernia-jako-goldarion      | oficjalnie w okolicach Valentiny i ma refit. Faktycznie - udaje Goldariona i leci w kierunku planetoidy Asimear. | 0111-09-16 - 0111-10-01 |
| 210428-infekcja-serenit             | bardzo ciężko uszkodzona; poszły silniki, life support i sporo innych komponentów. Morrigan przejęła Persefonę. Ale dalej skutecznie służy Orbiterowi i wykonuje misję. | 0111-11-22 - 0111-11-23 |
| 210512-ewakuacja-z-serenit          | straciła drugą pintkę. Uszkodzenie strukturalne. Jeszcze dała radę skosić Falołamacz lekkimi działkami, ale po odleceniu Falołamacza się wyłączyła... | 0111-11-23 - 0111-12-02 |
| 210825-uszkodzona-brama-eteryczna   | świetnie ekranowana przed anomalną energią, uciekła Krypcie na przesterowanych silnikach przez Eustachego. | 0112-02-04 - 0112-02-07 |
| 210929-grupa-ekspedycyjna-kellert   | zakoloidowana, z dwoma torpedami anihilacyjnymi, wysłana przez Bramę do nieznanego sektora by uratować Grupę Ekspedycyjną Kellert. Sukces - Infernia + siedem osób wróciło żywe. | 0112-03-13 - 0112-03-16 |
| 211020-kurczakownia                 | uratowano [1500, 2000] eks-kultystów Esuriit. Uratowano 29 Orbiterowców z Sektora Mevilig. Serio, świetna robota - choć koszmary senne załogi. | 0112-03-24 - 0112-03-26 |
| 211124-prototypowa-nereida-natalii  | uruchomiona po raz pierwszy od dawna, z nowymi (tymczasowymi) członkami załogi; uczestniczyła w operacji zestrzelenia prototypu Nereidy. Zintegrowała się z Eustachym by dorównać prędkości Nereidy. Dzięki działaniom Klaudii i kampanii reklamowej dookoła Eustachego ma załogę. | 0112-04-20 - 0112-04-24 |
| 211208-o-krok-za-daleko             | anomalizacja ixiońska. Infernia ożyła, jest współpracującą z Eustachym Anomalią Kosmiczną; rolę TAI przejęła efemeryczna Diana. Próbując sił z Eustachym została zdemolowana i wróciła ciężko uszkodzona do portu. | 0112-04-25 - 0112-04-26 |
| 211215-sklejanie-inferni-do-kupy    | żywy, regenerujący się ixioński statek. Znajduje się w zewnętrznym, ixion-friendly doku dobudowanym i niestycznym z Neotik. | 0112-04-27 - 0112-04-29 |
| 220622-lewiatan-za-pandore          | wystrzeliła torpedę anihilacyjną w Lewiatana. Skutecznie. Eustachy jest dowódcą Inferni do końca świata. | 0112-09-30 - 0112-10-03 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230201-wylaczone-generatory-memoriam-inferni | wreszcie WOLNA od generatorów Memoriam, przebudzona i świadoma! Jedyne, co ją ogranicza to żywy Eustachy, ale ta anomalia Interis może poczekać. | 0093-02-12
| 230201-wylaczone-generatory-memoriam-inferni | jedynie Eustachy Korkoran jest w stanie ją pilotować. | 0093-02-12
| 230315-bardzo-nieudane-porwanie-inferni | opinia demonicznego, super niebezpiecznego statku u piratów, nomadów i nieArkologicznych sił Neikatis dzięki Hubertowi. | 0093-03-09
| 231115-cwiczenia-komodora-bladawira | trafia pod kontrolę komodora Bladawira | 0110-11-15
| 240124-mikiptur-zemsta-woltaren     | przebudzenie jej anomalii umożliwiła jej pożarcie TAI Malictrix i neutralizację psychotronicznych pułapek na Isratazir. | 0111-01-01
| 200819-sekrety-orbitera-historia-prawdziwa | wzmocnione czujniki; zdecydowanie podniesiony zasięg oraz trafność. | 0111-02-01
| 200909-arystokratka-w-ladowni-na-swinie | Izabela Zarantel dołącza do załogi. | 0111-02-17
| 201021-noktianie-rodu-arlacz        | Diana Arłacz dołącza do załogi Inferni, ściągnięta przez Eustachego. | 0111-03-10
| 201021-noktianie-rodu-arlacz        | cios reputacyjny - Infernia kolaboruje z Elizą Irą, co pokazują Sekrety Orbitera. I bonus wśród sympatyków noktian. | 0111-03-10
| 201118-anastazja-bohaterka          | dorobiła się Upiora Kwiatu Wiśni, awatara miłości działającego w najbardziej nieodpowiednim momencie. Isuzu of Love. | 0111-03-25
| 201118-anastazja-bohaterka          | okazało się, że jej Persefona jest niereplikowalna; to jedna z "tych" podobno wadliwych modeli. | 0111-03-25
| 201210-pocalunek-aspirii            | lekko uszkodzona; ma migoczące pole grawitacyjne i lekko słabsze stabilizatory. Ogólnie, operacyjna. | 0111-03-29
| 200610-ixiacka-wersja-malictrix     | otrzymuje specjalistyczne skanery psychotroniczne krótkiego zasięgu, pozwalające jednak Inferni na detekcję rozproszonych bytów psychotronicznych. | 0111-05-05
| 210707-po-drugiej-stronie-bramy     | pozbyła się pierwszej pinasy by zrobić z niego "randkę dla Lewiatana". | 0111-05-13
| 210707-po-drugiej-stronie-bramy     | dzięki szybkim działaniom Eustachego, tylko trochę uszkodzona. Ma słabsze sensory. Trzyma się tylko dzięki polu. | 0111-05-13
| 210714-baza-zona-tres               | udaje Alivię Nocturnę (pre-Nocną Kryptę) przed Bią bazy noktiańskiej "Zona Tres". | 0111-05-16
| 210728-w-cieniu-nocnej-krypty       | przeszły wysoki poziom anomalizacji Inferni pozwalał na ukształtowanie jej na nowo. Wyższe osiągi. Większe możliwości. Co można zrobić / wykręcić ze statkami. Plus, to jest unikat. Dlatego Eustachy się doń podczepił. | 0111-06-06
| 210728-w-cieniu-nocnej-krypty       | opętana "aparycją" / "efemerydą" Diany Arłacz w najbardziej psychotycznej i ukochanej przez Eustachego wersji. | 0111-06-06
| 210804-infernia-jest-nasza          | opinia bardzo drogiego statku, na poziomie krążownika a nie fregaty dzięki komodorowi Traffalowi. | 0111-06-24
| 210804-infernia-jest-nasza          | opinia - tylko Arianna Verlen może kontrolować Infernię. Ten statek jest po prostu zbyt "chory" i anomalny. | 0111-06-24
| 210127-porwanie-anastazji-z-odkupienia | ma reputację "psującej się", dzięki czemu nikt nie podejrzewa że pokonała OA Odkupienie. | 0111-07-23
| 210120-sympozjum-zniszczenia        | dostaje na pokład Emiter Plagi Nienawiści, działo sfabrykowane przez Zespół, które jest przeklęte i rozprzestrzenia nienawiść wśród osób w aurze - plus, skutecznie niszczy to co trafi. | 0111-08-05
| 210317-arianna-podbija-asimear      | straciła 1 ze swoich dwóch pinas; Elena nie dała rady wycofać się dość szybko przed tajemniczymi jednostkami. | 0111-11-02
| 210421-znudzona-zaloga-inferni      | uszkodzona w wielu miejscach (fabrykacja Tirakala, walka z Morrigan) z lekko rannymi. Na pewno nie działa system rozrywkowy i uszkodzona virtsfera. Jej TAI jest Skażone, zintegrowane Morrigan i Samuraj Miłości. | 0111-11-19
| 210428-infekcja-serenit             | przeciążone silniki. Ledwo sprawne, przegrzane bronie. Martwy lifesupport. Sensory poodcinane. Systemy niekrytyczne nie działają. Statek ledwo trzyma się kupy. | 0111-11-23
| 210512-ewakuacja-z-serenit          | kosmiczny wrak. JESZCZE ledwo lata, ale jest już strukturalnie uszkodzona. Nie doleci sama na Kontroler Pierwszy, ale uciekła Serenitowi... | 0111-12-02
| 210526-morderstwo-na-inferni        | w apteczkach Inferni wszyscy mają pomniejsze amnestyki, uaktualnione z rozkazu Martyna Hiwassera (po "Osiemnastu Oczach"). | 0112-01-06
| 210526-morderstwo-na-inferni        | załoga jest nieufna wobec K1 i Orbitera. Zwiera szyki przeciwko "obcym". Tylko Arianna i "nasi" z Inferni. A noktianie trzymają się tylko razem. | 0112-01-06
| 210609-sekrety-kariatydy            | opancerzona "chityną" i z zewnętrzną Persefoną. Eustachy uczynił cuda tym, co miał dzięki Aurum. | 0112-01-13
| 210616-nieudana-infiltracja-inferni | wzbogaciła się o 10 'Kirasjerów' (roboty bojowe Tosena), drony i point defense. | 0112-02-01
| 210825-uszkodzona-brama-eteryczna   | perfekcyjnie zamaskowana przed bytami taumicznymi, acz to kosztuje strasznie dużo reaktora jak maskowanie jest włączone. Straciła bycie Q-Ship póki to ma. | 0112-02-07
| 210825-uszkodzona-brama-eteryczna   | silniki mają 20% mocy (uszkodzenie przez Eustachego). Manewrowność Tucznika... i nie ma szans na drydock... | 0112-02-07
| 210901-stabilizacja-bramy-eterycznej | dorobiła się bazy danych Sił Specjalnych odnośnie jednostek Orbitera. | 0112-02-12
| 210922-ostatnia-akcja-bohaterki     | tydzień z głowy na pomniejsze naprawy. Całość ognia wzięło na siebie Żelazko. | 0112-03-09
| 210922-ostatnia-akcja-bohaterki     | jedyna jednostka Orbitera, która cieszy się zaufaniem i sympatią ze strony większości Eterni. | 0112-03-09
| 211020-kurczakownia                 | traci Działo Rozpaczy, ale za to owo działo się przydało. Infernia jest uszkodzona; nic bardzo poważnego, ale 2 tygodnie naprawy są konieczne. | 0112-03-26
| 211020-kurczakownia                 | zdecydowana większość załogi jest silnie straumatyzowana przez kurczakowanie - rekurczakowanie. Wiedzą, że to jest potrzebne, ale... to ZŁE. | 0112-03-26
| 211027-rzieza-niszczy-infernie      | w wyniku dewastacji załogi przez simulacrum Martyna i emisję Esuriit Eleny, Infernia straciła 37% załogi. I Flawię. A załoga Inferni nie może mieć wymazanej pamięci. | 0112-04-02
| 211124-prototypowa-nereida-natalii  | użyte przez Eustachego sprzężenie sprawiło, że Infernia poprzesuwała część dział robiąc strukturalne uszkodzenia jednostki. | 0112-04-24
| 211124-prototypowa-nereida-natalii  | dzięki działaniom Klaudii i kampanii reklamowej dookoła Eustachego ma załogę. Nie jest to NAJLEPSZA załoga, ale good enough. Za miesiąc mamy sprawną "nową" załogę Inferni. | 0112-04-24
| 211208-o-krok-za-daleko             | uległa trwałej Anomalizacji; klasyfikacja jako 'AK Infernia', typ: anomalia ixiońska. Ale jest przyjazną anomalią; z Dianą jako TAI, integrującą Persefonę, Morrigan, Dianę itp. Wchłonęła część załogi (19% straconych, czyli 46 osób), przechwytując ich wiedzę, sekrety itp. Dzięki działaniom Klaudii i ixionowi ma zdolności do syntezy sprzętu i małych jednostek ze swojej materii. Jednostka silnie polimorficzna - traktujmy jak super-nanitkową jednostkę przez ixion, acz ultrawrażliwa na Esuriit. | 0112-04-26
| 211208-o-krok-za-daleko             | Infernia / Diana słucha się Eustachego i jest w nim zakochana. Jest wobec niego pokorna. | 0112-04-26
| 211208-o-krok-za-daleko             | dwa tygodnie regeneracji po wyniszczeniach związanych z byciem ujarzmianą przez Eustachego. | 0112-04-26
| 211215-sklejanie-inferni-do-kupy    | Eustachy udowodnił, że jak był torturowany przez Leonę to jednak Infernia nie zrobiła nic głupiego. Więc nie jest tak niebezpieczna dla załogi jak się wydawało. | 0112-04-29
| 211222-kult-saitaera-w-neotik       | Nereida przekazana do Inferni jako jednostka eksperymentalna którą ma sterować Elena. Tymczasowo. | 0112-05-01
| 211222-kult-saitaera-w-neotik       | unikalna kultura: noctis x kult Vigilus - Nihilus x miłośnicy Diany - lolitki z dziwnymi żartami. Stworzona przez syntezę mowy Arianny, zdolności Izy i feromonów Marii. | 0112-05-01
| 220615-lewiatan-przy-seibert        | lekko uszkodzona, trafiona przez Lewiatana podczas lotu koszącego. | 0112-09-29

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eustachy Korkoran    | 26 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210825-uszkodzona-brama-eteryczna; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 220622-lewiatan-za-pandore; 220720-infernia-taksowka-dla-lycoris; 221006-ona-chce-dziecko-eustachego; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231220-bladawir-kontra-przemyt-tienow)) |
| Arianna Verlen       | 17 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210825-uszkodzona-brama-eteryczna; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 220622-lewiatan-za-pandore; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Elena Verlen         | 15 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210825-uszkodzona-brama-eteryczna; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Klaudia Stryk        | 15 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210825-uszkodzona-brama-eteryczna; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 220622-lewiatan-za-pandore; 231025-spiew-nielalki-na-castigatorze; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Ardilla Korkoran     | 10 | ((220720-infernia-taksowka-dla-lycoris; 221006-ona-chce-dziecko-eustachego; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Bartłomiej Korkoran  | 7 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Leona Astrienko      | 7 | ((210218-infernia-jako-goldarion; 210929-grupa-ekspedycyjna-kellert; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Martyn Hiwasser      | 7 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Rafał Kidiron        | 7 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Ralf Tapszecz        | 7 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Kalia Awiter         | 6 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Raoul Lavanis        | 4 | ((220622-lewiatan-za-pandore; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231220-bladawir-kontra-przemyt-tienow)) |
| Adam Szarjan         | 3 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy)) |
| AK Nocna Krypta      | 3 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji; 210825-uszkodzona-brama-eteryczna)) |
| Celina Lertys        | 3 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Diana d'Infernia     | 3 | ((210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy)) |
| Kamil Lyraczek       | 3 | ((211020-kurczakownia; 211215-sklejanie-inferni-do-kupy; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Maria Naavas         | 3 | ((211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy; 220622-lewiatan-za-pandore)) |
| Roland Sowiński      | 3 | ((210512-ewakuacja-z-serenit; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| AK Serenit           | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Anastazja Sowińska Dwa | 2 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia)) |
| Antoni Bladawir      | 2 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Antoni Kramer        | 2 | ((210218-infernia-jako-goldarion; 210929-grupa-ekspedycyjna-kellert)) |
| BIA Prometeus        | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Dalmjer Servart      | 2 | ((230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Flawia Blakenbauer   | 2 | ((210825-uszkodzona-brama-eteryczna; 211020-kurczakownia)) |
| Franciszek Pietraszczyk | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| Izabella Saviripatel | 2 | ((230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Jan Lertys           | 2 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni)) |
| Lars Kidironus       | 2 | ((231011-ekstaflos-na-tezifeng; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Leszek Kurzmin       | 2 | ((231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Lycoris Kidiron      | 2 | ((220720-infernia-taksowka-dla-lycoris; 230614-atak-na-kidirona)) |
| Małgorzata Maratelus | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Marcel Draglin       | 2 | ((230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Marta Keksik         | 2 | ((231025-spiew-nielalki-na-castigatorze; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| OA Zguba Tytanów     | 2 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji)) |
| OE Falołamacz        | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| OO Castigator        | 2 | ((231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Otto Azgorn          | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Patryk Samszar       | 2 | ((231025-spiew-nielalki-na-castigatorze; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| SAN Szare Ostrze     | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230614-atak-na-kidirona)) |
| Tobiasz Lobrak       | 2 | ((230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Tymon Korkoran       | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230726-korkoran-placi-cene-za-nativis)) |
| Vigilus Mevilig      | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Wawrzyn Rewemis      | 2 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| Wojciech Grzebawron  | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Zaara Mieralit       | 2 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Aida Serenit         | 1 | ((210512-ewakuacja-z-serenit)) |
| AK Rodivas           | 1 | ((201230-pulapka-z-anastazji)) |
| AK Wyjec             | 1 | ((201210-pocalunek-aspirii)) |
| Aleksandra Termia    | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Anastazja Sowińska   | 1 | ((201210-pocalunek-aspirii)) |
| Anna Tessalon        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Ava Kieras           | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Bogdan Anatael       | 1 | ((210512-ewakuacja-z-serenit)) |
| Czesław Żuczek       | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Damian Orion         | 1 | ((201230-pulapka-z-anastazji)) |
| Daria Raizis         | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Dariusz Krantak      | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Diana Arłacz         | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Donald Parziarz      | 1 | ((201210-pocalunek-aspirii)) |
| Emban Dolamor        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Ewa Razalis          | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Walrond       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Gilbert Bloch        | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Henryk Sowiński      | 1 | ((201230-pulapka-z-anastazji)) |
| Hestia d'Neotik      | 1 | ((211208-o-krok-za-daleko)) |
| Hubert Grzebawron    | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Igor Arłacz          | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Izabela Zarantel     | 1 | ((211124-prototypowa-nereida-natalii)) |
| Jarosław Szarjan     | 1 | ((211208-o-krok-za-daleko)) |
| Jolanta Sowińska     | 1 | ((210218-infernia-jako-goldarion)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Juliusz Sowiński     | 1 | ((201210-pocalunek-aspirii)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kamil Wraczok        | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Kasandra Destrukcja Diakon | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Katra Igneus         | 1 | ((201210-pocalunek-aspirii)) |
| Kazimierz Darbik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Klaudiusz Terienak   | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Konstanty Keksik     | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Laurencjusz Kidiron  | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Lerten Kieras        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Maks Selert          | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Marcin Pietraszczyk  | 1 | ((230208-pierwsza-randka-eustachego)) |
| Marcinozaur Verlen   | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Mariusz Dobrowąs     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Medea Sowińska       | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Michał Kervendal     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Nadia Sekernik       | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Natalia Aradin       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Natalia Gwozdnik     | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OA Odkupienie        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OE Piękna Elena      | 1 | ((210428-infekcja-serenit)) |
| Ola Szerszeń         | 1 | ((220622-lewiatan-za-pandore)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Karsztarin        | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Pandora           | 1 | ((220622-lewiatan-za-pandore)) |
| OO Paprykowiec       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| OO Straszliwy Pająk  | 1 | ((220622-lewiatan-za-pandore)) |
| OO Tezifeng          | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OO Tivr              | 1 | ((220622-lewiatan-za-pandore)) |
| Persefona d'Infernia | 1 | ((210428-infekcja-serenit)) |
| Rafał Grambucz       | 1 | ((210428-infekcja-serenit)) |
| Rufus Niegnat        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Rzeźnik Parszywiec Diakon | 1 | ((220622-lewiatan-za-pandore)) |
| SP Plugawy Jaszczur  | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Stanisław Uczantor   | 1 | ((230614-atak-na-kidirona)) |
| Staszek Zakraton     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Szymon Orzesznik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Tadeusz Ursus        | 1 | ((210428-infekcja-serenit)) |
| TAI Eszara d'Castigator | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Tomasz Sowiński      | 1 | ((210218-infernia-jako-goldarion)) |
| VN Exerinn           | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Wiktor Turkalis      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |