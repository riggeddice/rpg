---
categories: profile
factions: 
owner: public
title: Aleksander Leszert
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210330-pletwalowcy-na-goldarionie   | kapitan Goldariona; skupiony na swojej załodze i swoim statku a nie na ogólnych korzyściach dla wszystkich. Dość niski z charakteru. Ale pod wpływem Adama zintegrował te załogi. | 0109-03-11 - 0109-03-26 |
| 210108-ratunkowa-misja-goldariona   | kapitan Goldariona. mtg_WB. Lojalny swoim, chyba ostatni na Goldarionie który próbuje trzymać dyscyplinę i naprawdę zależy mu na tym statku. | 0111-08-09 - 0111-08-15 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210330-pletwalowcy-na-goldarionie   | w relacji z Adamem Perminem (swoim oficerem bezpieczeństwa) | 0109-03-26

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Permin          | 2 | ((210108-ratunkowa-misja-goldariona; 210330-pletwalowcy-na-goldarionie)) |
| Elena Verlen         | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Feliks Przędz        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Janusz Krumlod       | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Jonatan Piegacz      | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Kamil Frederico      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Karolina Kartusz     | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Klaudia Stryk        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Martyn Hiwasser      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Martyna Bianistek    | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Oliwia Pietrova      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| SC Goldarion         | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| SCA Goldarion        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Semla d'Goldarion    | 1 | ((210108-ratunkowa-misja-goldariona)) |
| SL Uśmiechnięta      | 1 | ((210108-ratunkowa-misja-goldariona)) |