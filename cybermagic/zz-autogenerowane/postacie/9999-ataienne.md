---
categories: profile
factions: 
owner: public
title: Ataienne
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190123-skazenie-grazoniusza         | jeszcze Alicja Sowińska. Idealistyczna kapitan statku ASD Grazoniusz. Nieświadoma obecności Saitaera na swoim statku, została zainfekowana i dołączyła do sił Saitaera. | 0079-03-30 - 0079-04-01 |
| 190123-parszywa-ekspedycja          | kiedyś kapitan Alicja Sowińska, teraz z woli Saitaera TAI Grazioniusza. "Porwana" przez ekspedycję OO Bubuta. | 0095-04-14 - 0095-04-16 |
| 190330-polowanie-na-ataienne        | chciała tylko zrobić koncert - a cudem uniknęła śmierci dzięki ratunkowi Chevaleresse a potem Pięknotki. Miała hosta w formie człowieka ku niezadowoleniu Pięknotki. | 0110-03-03 - 0110-03-04 |
| 190331-kurz-po-ataienne             | uważa Chevaleresse za przyjaciółkę i dlatego nie udało się Pięknotce jej przekonać do użycia swoich mocy by nieco ustabilizować Chevaleresse. Wdzięczna Pięknotce. | 0110-03-07 - 0110-03-09 |
| 190402-eksperymentalny-power-suit   | wniknęła do Cienia i uratowała Pięknotkę przed nienawistnym instynktem psychotroniki. Skończyła bardzo ciężko ranna, z rozpadającą się matrycą. | 0110-03-15 - 0110-03-17 |
| 200219-perfekcyjna-corka            | dzięki współpracy Leszka i Sabiny przejęła kontrolę nad Kalbarkiem i się tam zaszczepiła. Zapewnia absolutną widoczność oraz neutralizuje Paradoksy przekierowując emitery. | 0110-05-16 - 0110-05-19 |
| 191105-zaginiona-soniczka           | przestraszyła się o Mariolę (swoją soniczkę która zaginęła) i udała się z prośbą o pomoc do Pięknotki. Przypadkowo, Ataienne wykryła gang przerzutowy dziewczyn do Cieniaszczytu. | 0110-07-02 - 0110-07-03 |
| 191112-korupcja-z-arystokratki      | nieprawdopodobnie potężna z perspektywy opętywania maszyn i przejmowania kontroli. Sojuszniczka Pięknotki, która przełamała wszystkie zabezpieczenia nawet się nie starając... | 0110-07-04 - 0110-07-05 |
| 190817-kwiaty-w-sluzbie-puryfikacji |  | 0110-07-04 - 0110-07-09 |
| 200202-krucjata-chevaleresse        | TAI powyżej 3 stopnia. Działa w tle - taktyk oraz commander, ale niekoniecznie mistrzyni zdobywania informacji. Jedyna TAI 3.5 w okolicy Szczelińca. Sformowała 'Liberitas' | 0110-07-24 - 0110-07-27 |
| 200222-rozbrojenie-bomby-w-kalbarku | autoryzowana przez Pięknotkę do pomocy w Barbakanie, pokazuje bezwzględność wobec Małmałaza i Kardamacza. Rozmontowała Liberatis i odzyskała broń. | 0110-07-27 - 0110-08-01 |
| 200916-smierc-raju                  | w Trzecim Raju działa w ukryciu, nie pokazując co naprawdę umie i stanowi źródło rozczarowań. Ale jak Raj został zniszczony, użyła pełnej mocy hipnotycznej. Nikt jej nie jest w stanie kontrolować, acz Arianna i Klaudia widzą, co potrafi. | 0111-02-18 - 0111-02-20 |
| 201014-krystaliczny-gniew-elizy     | widząc jak Dariusz potraktował Anastazję Sowińską (jej krewną), użyła swej mocy na życzenie Arianny by go zmusić do przyznania się. | 0111-03-02 - 0111-03-05 |
| 201021-noktianie-rodu-arlacz        | bardzo chce się połączyć psychotronicznie z Anastazją. Nie ma okazji - Arianna jej nie pozwala. Za to ma nowych 100 noktian do zmanipulowania i Raj do trzymania. | 0111-03-07 - 0111-03-10 |
| 201104-sabotaz-swini                | zestrzeliła działkami Trzeciego Raju anomalnego Wieprzka, po czym użyła pełni mindwarpa by wyczyścić ofiary anomalnego Wieprzka. I nikt nic nie wie. | 0111-03-13 - 0111-03-16 |
| 190326-arcymag-w-raju               | mindwarpująca TAI o funkcji nadpisywania wspomnień; hipnopiosenkarka holograficzna. Kontroluje Trzeci Raj składający się z jeńców wojennych. | 0111-07-18 - 0111-07-19 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190123-parszywa-ekspedycja          | Alicja Sowińska powróciła do Orbitera Pierwszego, ale jako TAI a nie czarodziejka. Nie wiadomo co z nią zrobią. Nie wiadomo co ona może zrobić. | 0095-04-16
| 190402-eksperymentalny-power-suit   | ciężko ranna, z uszkodzoną matrycą. Uratowała życie Pięknotce, ale zapłaciła rozpad matrycy; ma trochę czasu regeneracji. | 0110-03-17
| 200923-magiczna-burza-w-raju        | niechęć wobec Arianny Verlen. Ataienne jest przeciwna bogom i istotom bogom podobnym a Arianna jest arcymagiem używającym eterniańskich metod. | 0111-02-25

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 8 | ((190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 190402-eksperymentalny-power-suit; 190817-kwiaty-w-sluzbie-puryfikacji; 191105-zaginiona-soniczka; 191112-korupcja-z-arystokratki; 200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Anastazja Sowińska   | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Arianna Verlen       | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Diana Tevalier       | 4 | ((190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Eustachy Korkoran    | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Klaudia Stryk        | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Mateusz Kardamacz    | 4 | ((191105-zaginiona-soniczka; 200202-krucjata-chevaleresse; 200219-perfekcyjna-corka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Eliza Ira            | 3 | ((190326-arcymag-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Erwin Galilien       | 3 | ((190402-eksperymentalny-power-suit; 190817-kwiaty-w-sluzbie-puryfikacji; 191105-zaginiona-soniczka)) |
| Mariusz Trzewń       | 3 | ((191105-zaginiona-soniczka; 191112-korupcja-z-arystokratki; 200202-krucjata-chevaleresse)) |
| Minerwa Metalia      | 3 | ((190402-eksperymentalny-power-suit; 191112-korupcja-z-arystokratki; 200202-krucjata-chevaleresse)) |
| Alan Bartozol        | 2 | ((190330-polowanie-na-ataienne; 190331-kurz-po-ataienne)) |
| Aleksandra Szklarska | 2 | ((200219-perfekcyjna-corka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| ASD Grazoniusz       | 2 | ((190123-parszywa-ekspedycja; 190123-skazenie-grazoniusza)) |
| Diana Arłacz         | 2 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Elena Verlen         | 2 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy)) |
| Izabela Zarantel     | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Lucjusz Blakenbauer  | 2 | ((190330-polowanie-na-ataienne; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Marian Fartel        | 2 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy)) |
| OO Wesoły Wieprzek   | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Rafał Armadion       | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Robert Garwen        | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Sabina Kazitan       | 2 | ((191112-korupcja-z-arystokratki; 200219-perfekcyjna-corka)) |
| Szymon Oporcznik     | 2 | ((190331-kurz-po-ataienne; 190402-eksperymentalny-power-suit)) |
| Tymon Grubosz        | 2 | ((191105-zaginiona-soniczka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Wanessa Pyszcz       | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Aleksandra Termia    | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Atena Sowińska       | 1 | ((190330-polowanie-na-ataienne)) |
| Bartłomiej Małczarek | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Damian Orion         | 1 | ((200202-krucjata-chevaleresse)) |
| Dariusz Krantak      | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Dawid Szardak        | 1 | ((190326-arcymag-w-raju)) |
| Fergus Salien        | 1 | ((190326-arcymag-w-raju)) |
| Grzegorz Kamczarnik  | 1 | ((190326-arcymag-w-raju)) |
| Ignacy Myrczek       | 1 | ((191112-korupcja-z-arystokratki)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Juliusz Sowiński     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Kaja Selerek         | 1 | ((190402-eksperymentalny-power-suit)) |
| Keraina d'Orion      | 1 | ((200202-krucjata-chevaleresse)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Kornel Garn          | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Lena Kardamacz       | 1 | ((200219-perfekcyjna-corka)) |
| Leszek Szklarski     | 1 | ((200219-perfekcyjna-corka)) |
| Malictrix d'Itaran   | 1 | ((200202-krucjata-chevaleresse)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Mariola Tralment     | 1 | ((191105-zaginiona-soniczka)) |
| Martyn Hiwasser      | 1 | ((201104-sabotaz-swini)) |
| Nataniel Marszalnik  | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Olga Leszcz          | 1 | ((190326-arcymag-w-raju)) |
| OO Bubuta            | 1 | ((190123-parszywa-ekspedycja)) |
| OO Castigator        | 1 | ((190326-arcymag-w-raju)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Paweł Oszmorn        | 1 | ((200219-perfekcyjna-corka)) |
| Persefona d'Jastrząbiec | 1 | ((191112-korupcja-z-arystokratki)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Romeo Węglas         | 1 | ((190331-kurz-po-ataienne)) |
| Tadeusz Sklerzec     | 1 | ((191112-korupcja-z-arystokratki)) |
| Talia Aegis          | 1 | ((200202-krucjata-chevaleresse)) |
| Teresa Marszalnik    | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Tomasz Tukan         | 1 | ((200202-krucjata-chevaleresse)) |