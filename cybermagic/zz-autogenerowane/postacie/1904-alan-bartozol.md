---
categories: profile
factions: 
owner: public
title: Alan Bartozol
---

# {{ page.title }}


# Read: 

## Metadane

* factions: "pustogor, miasteczkowcy, multivirt, elisquid"
* owner: "public"
* title: "Alan Bartozol"


## Postać

### Ogólny pomysł (2)

* Pustogorski terminus szturmowy. Wykorzystuje bardzo ciężkie servary lub mechy, zawsze z ogromną siłą ognia. Siła ognia to jego wyróżnik ;-).
* Świetnie zna się na materiałach wybuchowych, wysadzaniu rzeczy i konstrukcji tego typu spraw. Specjalizuje się w destabilizacji _vitium_.
* Mistrz MultiVirt. Dowódca i imperator własnej gildii wielogrowej - Elisquid. Nie jest technomantą, ale jest świetnym graczem i przywódcą.

### Misja (co dla innych) (2)

* Silna segregacja i podejście kastowe na zasadzie kompetencji. Każdemu wedle potrzeb, od każdego według umiejętności. Dążenie do bycia homo superior dla każdego.
* Wprowadzić porządek, ład oraz przewidywalność do tego świata pełnego chaosu. Zwłaszcza wobec tych, którzy są mniej kompetentni i tego potrzebują.

### Motywacja (co dla mnie) (2)

* Cisza. Adrenalina. Pierwotny łowca przeciwko światu. On sam, bez polityki, bez innych ludzi i magów. Apex Predator. Spokój i brak uniesień - tylko czysta destrukcja.
* Homo superior, można od niego oczekiwać wszystkiego, też niemożliwego. Kodeks wojownika i inżyniera.
* Wolność - jest na tyle dobry, skuteczny i kompetentny że nikt nie ma prawa niczego od niego żądać.

### Wyróżnik (2)

* Niesamowicie szybki (reakcje) i celny terminus; mnóstwo pracy, energii i siły wkłada w to by być najszybszy i najcelniejszy.
* Trzęsawisko Zjawosztup zna jak własny dom, wykorzystuje je jako miejsce do ciągłego doskonalenia się i pozyskiwania pieniędzy.
* Niekwestionowany specjalista od przełamywania obrony i broni oblężniczej. Świetnie znajduje słabe punkty ciężkich przeciwników i penetruje pancerz.
* Nieprawdopodobny wręcz ponurak. Potrafi zdemotywować, zmartwić i wyłączyć wszelką radość z każdego.
* Neutralizator. Potrafi bardzo szybko znaleźć kontrzaklęcie czy kontrtaktykę. Nie potrafi budować, ale potrafi niszczyć.
* Ceniony i szacowany Krwawy Rycerz w multivirt. Przełożony Elisquid.

### Zasoby i otoczenie (3)

* Nie objął żadnego terenu w Pustogorze, więc jest terminusem lotnym; można go wysłać na każdą operację jako siłę główną czy wsparcie.
* Szturmowo-artyleryjski servar klasy 'Fulmen', bez opcji self-activate, z opcją 'trybu artylerii'. Niezbyt szybki, ale pancerny i silny.
* Ma dostęp do Laboratorium Zniszczenia w Pustogorze oraz sporą ilość niestabilnych kryształów _vitium_.
* Własna niewielka gildia gdzie pomaga ludziom - Elisquid.

### Magia (3)

#### Gdy kontroluje energię

* katalityczne portale energetyczne: potrafi wykorzystywać energię otoczenia by wzmacniać zaklęcia swoje oraz swoich sojuszników. Świetny w drenażu energii wrogów.
* kontrmagia: neutralizacja wrogich zaklęć i ogólnie negacja wrogich ruchów. Nie jest to puryfikacja; to wszelka forma KONTRDZIAŁAŃ.
* magia oblężnicza: powolne zaklęcia o upiornej sile ognia i umiejętnościach przebicia się przez defensywy. W ostateczności, destabilizacja _vitium_ czy eksplozja samej magii.

#### Gdy traci kontrolę

* Siła ognia jeszcze bardziej rośnie, ale kosztem wszelkich możliwych działań (overload).
* Jako, że pracuje na niestabilnych energiach, może coś mu wybuchnąć czy przestać dobrze działać.

### Powiązane frakcje

{{ page.factions }}

## Opis

.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180817-protomag-z-trzesawisk        | terminus współpracujący z Czerwonymi Myszami, któremu zależało na zdobyciu Felicji. Przez działania Pięknotki i Ateny musiał obejść się smakiem. | 0109-09-07 - 0109-09-09 |
| 181101-wojna-o-uczciwe-polfinaly    | terminus Czerwonych Myszy zajmujący się sprawą Skażenia z ramienia Pustogoru. Dostał za niewinność; no dobra, chciał wrobić Pięknotkę w ten teren. | 0109-10-17 - 0109-10-19 |
| 190101-morderczyni-jednej-plotki    | zawetował akces Pięknotki do Czerwonych Myszy i został przegłosowany; jest wściekły na Czerwone Myszy, bo wybierają Pięknotkę nad niego | 0109-12-13 - 0109-12-17 |
| 190105-turysci-na-trzesawisku       | pokazał, jak artyleryjski servar poradzi sobie na Trzęsawisku. Świetny terminus, acz skończył nieprzytomny mimo stymulantów. Uratował wielu. | 0109-12-22 - 0109-12-24 |
| 190112-eksperymenty-na-wilach       | niechętnie, ale dał się wpakować w akcję przeciwko własnej gildii z uwagi na ryzyko dla Pustogoru jeśli Saitaer skazi Trzęsawisko. Prawie pokonał Kornela. | 0109-12-28 - 0109-12-31 |
| 190115-skazony-pnaczoszpon          | źródło podstawowej wiedzy o pnączoszponach i terminus, który rozwiązał problem skażonej istoty. Krótkie żołnierskie słowa i skuteczne rozwiązywanie problemu. | 0110-01-07 - 0110-01-09 |
| 190116-wypalenie-saitaera-z-trzesawiska | wraz ze swoją małą ekipą szturmowców odwracał uwagę Trzęsawiska od Pięknotki. Z powodzeniem. Zdobył ogromną nienawiść Wiktora. | 0110-01-07 - 0110-01-09 |
| 190217-chevaleresse                 | w grze "Voidcarver", lider EliSquid. Powadził się z Tymonem o Chevaleresse i ją ku swemu utrapieniu przygarnął. Załatwiła go i został jej opiekunem prawnym. | 0110-02-25 - 0110-02-27 |
| 190330-polowanie-na-ataienne        | opierdolił Chevaleresse jak psa za zwinięcie jego sprzętu i walkę; Chevaleresse od niego uciekła w hipernet. Potem wspierał defensywnie Pięknotkę przeciw Orbiterowi by chronić też Chevaleresse. | 0110-03-03 - 0110-03-04 |
| 190331-kurz-po-ataienne             | uziemił Chevaleresse; nie radzi sobie jednak z małolatą i poprosił Pięknotkę by była mediatorką. Oj. | 0110-03-07 - 0110-03-09 |
| 190226-korporacyjna-wojna-w-mmo     | były lider EliSquid który zmusił Sekatora Pierwszego do współpracy. Niestety, wiele więcej nie dał rady zrobić. | 0110-03-14 - 0110-03-17 |
| 190419-osopokalipsa-wiktora         | wsparcie Pięknotki. Jego wiedza o bagnie przydała się opanowując apokalipsę Sensoplexu. | 0110-03-29 - 0110-03-31 |
| 190429-sabotaz-szeptow-elizy        | dowiedział się o Cieniu, Elizie i kilku rzeczach. Ma wrażenie, że Chevaleresse go "zostawia". Dodatkowo, z Erwinem i Minerwą udowodnił czemu jest najlepszym inżynierem zniszczenia w okolicy. | 0110-04-10 - 0110-04-13 |
| 190502-pierwszy-emulator-orbitera   | pełnił rolę taksówki dla Pięknotki w hovertanku klasy Timor. | 0110-04-14 - 0110-04-16 |
| 190505-szczur-ktory-chroni          | wezwany jako wsparcie, odpalił dwukrotnie działo Koenig - masakrując oficera mafiozów i masakrując Pięknotkę w Cieniu. Ogólnie, masakrował. | 0110-04-22 - 0110-04-24 |
| 190830-kto-wrobil-alana             | nic nie wie, że był wrobiony. Ale potem się dowiaduje, że Chevaleresse zwinęła artefakt oraz że zdenerwowała magów Aurum. Skończyło się na gorącej kłótni. | 0110-06-03 - 0110-06-05 |
| 190901-polowanie-na-pieknotke       | uratował Pięknotkę przed Józefem strzałem z Fulmena (700 metrów przez kilka ścian); potem by chronić Chevaleresse dał się Alanowi ciężko poranić. | 0110-06-21 - 0110-06-24 |
| 200913-haracz-w-parku-rozrywki      | anihilator Fulmenem; bezwzględnie zdjął maga. Badał dronę która śledziła. Dogadał się z TAI za plecami wszystkich. Bezwzględny i bardzo skuteczny. | 0110-10-09 - 0110-10-10 |
| 210817-zgubiony-holokrysztal-w-lesie | przekonany przez Chevaleresse do poprowadzenia wycieczki studentów w Lesie Trzęsawnym (ku swojemu smutkowi). Potem gdy Tukan powiedział mu o ko-matrycy z przyjemnością zrobił fałszywkę z trackerem i wziął na siebie "winę" ze strony mafii. | 0111-06-22 - 0111-06-24 |
| 211221-chevaleresse-infiltruje-rekiny | Chevaleresse zdobyła dla niego Stasia Arienika rękami Karoliny Terienak (o czym on nie wie). NADAL nie cierpi Aurum, ale tyci mniej. | 0111-09-06 - 0111-09-07 |
| 220814-stary-kot-a-trzesawisko      | wezwany do ratowania ludzi z Lechotki zorientował się, że ich już się nie uratuje. Zrobił im MIRV coup de grace. Potem wsadził Ulę do podejrzanej fortifarmy Lechotka i ją czujnie monitoruje. Dał się przekonać, by pozwolił kotu klasy vistermin żyć. | 0111-10-12 - 0111-10-13 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180817-protomag-z-trzesawisk        | ma kosę z Pięknotką. Zrobiła z niego idiotę przy użyciu Ateny Sowińskiej. | 0109-09-09
| 181101-wojna-o-uczciwe-polfinaly    | skrycie miłośnik gry 'Unreal Tremor'. Zabanowany przez Yyizdatha i nie wie czemu... | 0109-10-19
| 190116-wypalenie-saitaera-z-trzesawiska | Wiktor Satarail go nienawidzi i chce go zniszczyć | 0110-01-09
| 190119-skorpipedy-krolewskiego-xirathira | pomógł trochę Adeli Kirys z sympatii. Opuścił Czerwone Myszy. Nie mają nic dla niego, lepiej mu żyć samotnie. | 0110-01-15
| 190217-chevaleresse                 | zostaje prawnym opiekunem Diany Tevalier, aka Chevaleresse. | 0110-02-27
| 190217-chevaleresse                 | zimny gniew wobec Marleny Mai Leszczyńskiej, co do której myśli, że go zabanowała. | 0110-02-27
| 190217-chevaleresse                 | eskalacja z Tymonem Gruboszem; serio się nie lubią. A fakt że on skrzywdził Dianę nie pomógł. | 0110-02-27
| 190505-szczur-ktory-chroni          | działo Koenig zamontowane na Fulmenie działa perfekcyjnie. Jego nowa standardowa konfiguracja. Wymaga trochę więcej chłodzenia. Warte opierdolu. | 0110-04-24
| 190505-szczur-ktory-chroni          | reputacja wybitnego strzelca i straszliwego przeciwnika jedynie się wzmocniła. Nikt nie chce się z nim pojedynkować. | 0110-04-24
| 190901-polowanie-na-pieknotke       | na miesiąc trafił do szpitala przez dwa sztylety Józefa Małmałaza | 0110-06-24
| 200913-haracz-w-parku-rozrywki      | nowy aspekt - obsesyjnie chroni swoje podopieczne. | 0110-10-10
| 200913-haracz-w-parku-rozrywki      | bardzo ostry opieprz od Karli; nie do tego służy Fulmen, nie powinien używać ciężkiego działa przeciw piechocie. | 0110-10-10

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 16 | ((180817-protomag-z-trzesawisk; 181101-wojna-o-uczciwe-polfinaly; 190101-morderczyni-jednej-plotki; 190105-turysci-na-trzesawisku; 190112-eksperymenty-na-wilach; 190116-wypalenie-saitaera-z-trzesawiska; 190217-chevaleresse; 190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 190419-osopokalipsa-wiktora; 190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 190505-szczur-ktory-chroni; 190830-kto-wrobil-alana; 190901-polowanie-na-pieknotke; 200913-haracz-w-parku-rozrywki)) |
| Diana Tevalier       | 9 | ((190217-chevaleresse; 190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 190429-sabotaz-szeptow-elizy; 190830-kto-wrobil-alana; 190901-polowanie-na-pieknotke; 200913-haracz-w-parku-rozrywki; 211221-chevaleresse-infiltruje-rekiny; 220814-stary-kot-a-trzesawisko)) |
| Lucjusz Blakenbauer  | 5 | ((180817-protomag-z-trzesawisk; 190330-polowanie-na-ataienne; 190429-sabotaz-szeptow-elizy; 190505-szczur-ktory-chroni; 190901-polowanie-na-pieknotke)) |
| Wiktor Satarail      | 4 | ((190105-turysci-na-trzesawisku; 190116-wypalenie-saitaera-z-trzesawiska; 190419-osopokalipsa-wiktora; 190505-szczur-ktory-chroni)) |
| Erwin Galilien       | 3 | ((180817-protomag-z-trzesawisk; 190101-morderczyni-jednej-plotki; 190429-sabotaz-szeptow-elizy)) |
| Karolina Erenit      | 3 | ((181101-wojna-o-uczciwe-polfinaly; 190217-chevaleresse; 190830-kto-wrobil-alana)) |
| Mariusz Trzewń       | 3 | ((190901-polowanie-na-pieknotke; 200913-haracz-w-parku-rozrywki; 210817-zgubiony-holokrysztal-w-lesie)) |
| Minerwa Metalia      | 3 | ((190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 190901-polowanie-na-pieknotke)) |
| Olaf Zuchwały        | 3 | ((190101-morderczyni-jednej-plotki; 190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera)) |
| Ataienne             | 2 | ((190330-polowanie-na-ataienne; 190331-kurz-po-ataienne)) |
| Atena Sowińska       | 2 | ((180817-protomag-z-trzesawisk; 190330-polowanie-na-ataienne)) |
| Karolina Terienak    | 2 | ((211221-chevaleresse-infiltruje-rekiny; 220814-stary-kot-a-trzesawisko)) |
| Kasjopea Maus        | 2 | ((190116-wypalenie-saitaera-z-trzesawiska; 190419-osopokalipsa-wiktora)) |
| Marlena Maja Leszczyńska | 2 | ((181101-wojna-o-uczciwe-polfinaly; 190217-chevaleresse)) |
| Marysia Sowińska     | 2 | ((210817-zgubiony-holokrysztal-w-lesie; 211221-chevaleresse-infiltruje-rekiny)) |
| Rupert Mysiokornik   | 2 | ((210817-zgubiony-holokrysztal-w-lesie; 211221-chevaleresse-infiltruje-rekiny)) |
| Saitaer              | 2 | ((190112-eksperymenty-na-wilach; 190116-wypalenie-saitaera-z-trzesawiska)) |
| Tymon Grubosz        | 2 | ((181101-wojna-o-uczciwe-polfinaly; 190217-chevaleresse)) |
| Adam Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Adela Kirys          | 1 | ((190505-szczur-ktory-chroni)) |
| Adrian Wężoskór      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Aleksander Iczak     | 1 | ((190101-morderczyni-jednej-plotki)) |
| Aleksander Rugczuk   | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Alfred Bułka         | 1 | ((190115-skazony-pnaczoszpon)) |
| Antoni Kotomin       | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Ariela Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| ASD Centurion        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Barnaba Burgacz      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Bożymir Szczupak     | 1 | ((190502-pierwszy-emulator-orbitera)) |
| Cyryl Perikas        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Damian Podpalnik     | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
| Dariusz Bankierz     | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Eliza Ira            | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Eliza Kotlet         | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Ernest Kajrat        | 1 | ((190505-szczur-ktory-chroni)) |
| Felicja Melitniek    | 1 | ((180817-protomag-z-trzesawisk)) |
| Franciszek Owadowiec | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Hestia d'Rekiny      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Hieronim Maus        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Iwan Zawtrak         | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Józef Małmałaz       | 1 | ((190901-polowanie-na-pieknotke)) |
| Justynian Diakon     | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Kacper Bankierz      | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Karla Mrozik         | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Karol Senonik        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Karol Szurnak        | 1 | ((190101-morderczyni-jednej-plotki)) |
| Karradrael           | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Kasjan Czerwoczłek   | 1 | ((190505-szczur-ktory-chroni)) |
| Kermit Szperacz      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Kornel Garn          | 1 | ((190112-eksperymenty-na-wilach)) |
| Kornel Szczepanik    | 1 | ((190115-skazony-pnaczoszpon)) |
| Kornelia Weiner      | 1 | ((190115-skazony-pnaczoszpon)) |
| Kot Rozbójnik        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Krystian Namałłek    | 1 | ((190505-szczur-ktory-chroni)) |
| Krystyna Senonik     | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Laura Tesinik        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Liliana Bankierz     | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Maja Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Marian Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Melinda Teilert      | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Melissa Durszenko    | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Mi Ruda              | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Miedwied Zajcew      | 1 | ((180817-protomag-z-trzesawisk)) |
| Nikola Kirys         | 1 | ((190502-pierwszy-emulator-orbitera)) |
| Oliwia Namałłek      | 1 | ((190505-szczur-ktory-chroni)) |
| Rafał Kamaron        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Rafał Królewski      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Romeo Węglas         | 1 | ((190331-kurz-po-ataienne)) |
| Santino Mysiokornik  | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Sławomir Muczarek    | 1 | ((190112-eksperymenty-na-wilach)) |
| Staś Arienik         | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Stella Armadion      | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Szymon Oporcznik     | 1 | ((190331-kurz-po-ataienne)) |
| Tadeusz Kruszawiecki | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
| Talia Aegis          | 1 | ((190830-kto-wrobil-alana)) |
| Teresa Mieralit      | 1 | ((190101-morderczyni-jednej-plotki)) |
| Tomasz Tukan         | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Urszula Miłkowicz    | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Viirai d'Lechotka    | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Wojciech Słabizna    | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Wojciech Zermann     | 1 | ((190830-kto-wrobil-alana)) |
| Żorż d'Namertel      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |