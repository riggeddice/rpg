---
categories: profile
factions: 
owner: public
title: Romana Kundel
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220525-dzieci-z-arinkarii-odpychaja-piratow | 14, nieco nie pasuje do innych arinkarian (nie ma modyfikacji drakolickich); bardzo cicha i wszędzie się wślizgnie w szczeliny Arinkarii. Eksploruje Arinkarię, zna niebezpieczne miejsca i wciągnęła piratów w pułapkę. Potem przeniosła pistolet do inżynierów. Ogólnie, "duch Arinkarii". | 0105-05-14 - 0105-05-15 |
| 220329-mlodociani-i-pirat-na-krolowej | ukradła narzędzie i playboye dla Bartka. Wślizguje się w różne miejsca. Wyciszona, ale zadaje trafne pytania Berdyszowi - czemu ścieżka pirata. | 0108-08-29 - 0108-09-09 |
| 220405-lepsza-kariera-dla-romki     | miała plany iść w prostytucję i z tego zainwestować w coś. Anna wybiła jej to z głowy, Miranda też. Zamiast tego Romka pójdzie w technika pod Wojtkiem. Zaczyna się w Wojtku zakochiwać... | 0108-09-15 - 0108-09-17 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220329-mlodociani-i-pirat-na-krolowej | przekonana, że Anna ma rację - dobro i pomaganie mają znaczenie. Przekonana, że Berdysz Rozdzieracz też jest dobrą osobą. Uczestniczyła w negocjacjach z nim. | 0108-09-09
| 220405-lepsza-kariera-dla-romki     | powoli zakochuje się w Wojciechu Kaznodziei i uczy się od niego jak być technikiem statku takiego jak Królowa Przygód. | 0108-09-17
| 220405-lepsza-kariera-dla-romki     | ma pewne podejrzenia co do TAI "Ceres" (czyli Mirandy). Coś wygodnie to działa. | 0108-09-17

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anna Szrakt          | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Bartek Wudrak        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Helena Banbadan      | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Miranda Ceres        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| SC Królowa Przygód   | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Wojciech Kaznodzieja | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Adam Wudrak          | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Albert Rybowąż       | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Antos Kuramin        | 1 | ((220405-lepsza-kariera-dla-romki)) |
| Berdysz Rozdzieracz  | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Bruno Wesper         | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Damian Szczugor      | 1 | ((220405-lepsza-kariera-dla-romki)) |
| Gotard Kicjusz       | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Kaja Czmuch          | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Kara Prazdnik        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Koralina Szprot      | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Łucjan Torwold       | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Prokop Umarkon       | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Seweryn Grzęźlik     | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Ursyn Uszat          | 1 | ((220405-lepsza-kariera-dla-romki)) |