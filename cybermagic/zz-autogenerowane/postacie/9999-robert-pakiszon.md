---
categories: profile
factions: 
owner: public
title: Robert Pakiszon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201013-pojedynek-akademia-rekiny    | cel nie uświęca środków; nie zrobił z Liliany kozła ofiarnego. Dealer z AMZ dla Rekinów, współpracuje z Grzymościem. Przekonał Napoleona do pilotażu ścigacza w Turnieju Rekinów oraz zebrał dlań wszelkie potrzebne informacje o trasie i przeciwnikach by mógł skończyć na podium. | 0110-10-14 - 0110-10-22 |
| 201020-przygoda-randka-i-porwanie   | zaprojektował intrygę - gra w 'porwać dziewczynę', po czym przygotował feromony, załatwił porwanie i nauczył 'ludzi-węży' jak grać. Potem podkładał ślady by opóźnić niekompetentnych Sowińskich i kupić czas Gabrielowi na naprawienie sytuacji z Lilianą. | 0110-10-25 - 0110-10-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kacper Bankierz      | 2 | ((201013-pojedynek-akademia-rekiny; 201020-przygoda-randka-i-porwanie)) |
| Liliana Bankierz     | 2 | ((201013-pojedynek-akademia-rekiny; 201020-przygoda-randka-i-porwanie)) |
| Aleksander Bemucik   | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Daniel Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Gabriel Ursus        | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Henryk Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Ignacy Myrczek       | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Julia Kardolin       | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Justynian Diakon     | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Laurencjusz Sorbian  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Napoleon Bankierz    | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Remor 340D           | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Robinson Porzecznik  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Stella Armadion      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Teresa Mieralit      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Tomasz Tukan         | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Triana Porzecznik    | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Tymon Grubosz        | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Urszula Miłkowicz    | 1 | ((201020-przygoda-randka-i-porwanie)) |