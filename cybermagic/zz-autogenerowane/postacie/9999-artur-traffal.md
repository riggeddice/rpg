---
categories: profile
factions: 
owner: public
title: Artur Traffal
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210804-infernia-jest-nasza          | komodor Termii; zastawił pułapkę na Żaranda przy użyciu Inferni (i sobie dorobił). Wymanewrował Ariannę z Inferni, po czym chciał zapolować na kultystę magii z Eterni na K1. Niestety dla niego, wszystko poszło źle a kultystę oswoiła Arianna. | 0111-06-21 - 0111-06-24 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210804-infernia-jest-nasza          | ma skrajnego pecha na K1 dzięki Klaudii Stryk. W kasynie ma gorsze prawdopodobieństwa, jego wybierają do kontroli itp. Innymi słowy, chce unikać K1 jak to tylko możliwe. | 0111-06-24

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksandra Termia    | 1 | ((210804-infernia-jest-nasza)) |
| Antoni Kramer        | 1 | ((210804-infernia-jest-nasza)) |
| Arianna Verlen       | 1 | ((210804-infernia-jest-nasza)) |
| Diana d'Infernia     | 1 | ((210804-infernia-jest-nasza)) |
| Eustachy Korkoran    | 1 | ((210804-infernia-jest-nasza)) |
| Franciszek Maszkiet  | 1 | ((210804-infernia-jest-nasza)) |
| Klaudia Stryk        | 1 | ((210804-infernia-jest-nasza)) |
| Leona Astrienko      | 1 | ((210804-infernia-jest-nasza)) |
| Maciej Żarand        | 1 | ((210804-infernia-jest-nasza)) |
| OO Samotność Gwiazd  | 1 | ((210804-infernia-jest-nasza)) |