---
categories: profile
factions: 
owner: public
title: OO Mroczny Wół
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221220-dezerter-z-mrocznego-wolu    | nieodświeżona, cuchnąca ciężka barka czyszcząca trasy w Zewnętrznym Pierścieniu Astorii; transportuje anomalny derelict noktiański i na pokładzie doszło do dezercji w niemożliwy sposób - zniknął szef ochrony. Wysłano dwie inspektorki by dojść do tego o co chodzi. | 0111-11-15 - 0111-11-18 |
| 230103-protomag-z-mrocznego-wolu    | musi wymienić część załogi po śledztwie: Nela i Wincenty zostaną wymienieni. | 0111-11-19 - 0111-11-22 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 221220-dezerter-z-mrocznego-wolu    | ogólnie bardzo duża nieufność wobec inspektorek bo każdy ma coś na sumieniu; co gorsza, przez zgłaszanie formalne potem będą wielkie czystki na statku. | 0111-11-18

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adela Myrias         | 2 | ((221220-dezerter-z-mrocznego-wolu; 230103-protomag-z-mrocznego-wolu)) |
| Juliusz Akramantanis | 2 | ((221220-dezerter-z-mrocznego-wolu; 230103-protomag-z-mrocznego-wolu)) |
| Marianna Atrain      | 2 | ((221220-dezerter-z-mrocznego-wolu; 230103-protomag-z-mrocznego-wolu)) |
| Nadja Kilmodrian     | 2 | ((221220-dezerter-z-mrocznego-wolu; 230103-protomag-z-mrocznego-wolu)) |
| Baltazar Kwarcyk     | 1 | ((221220-dezerter-z-mrocznego-wolu)) |
| Krzysztof Workisz    | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Lena Ifirentis       | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Maja Szewieczak      | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Nela Kaltaner        | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Robert Warłomkacz    | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Tytus Muszczak       | 1 | ((221220-dezerter-z-mrocznego-wolu)) |