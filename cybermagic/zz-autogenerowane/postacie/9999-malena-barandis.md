---
categories: profile
factions: 
owner: public
title: Malena Barandis
---

# {{ page.title }}


# Generated: 



## Fiszki


* czarodziejka, winggirl Lei, 25 | @ 230325-ten-nawiedzany-i-ta-ukryta
* ENCAO:  000-+ |Wyrachowana;;Proponuje niekonwencjonalne, dziwne i często śmieszne rozwiązania| VALS: Security, Tradition >> Family| DRIVE: Impress the superior / senpai | @ 230325-ten-nawiedzany-i-ta-ukryta
* styl: młoda Asuka ale z nastawieniem na swoje korzyści / Blaster TF G1 | @ 230325-ten-nawiedzany-i-ta-ukryta
* przewaga: czarodziejka soniczna, elegancka arystokratka, BARDZO dobre zmysły (ród Barandis) | @ 230325-ten-nawiedzany-i-ta-ukryta
* bardzo zainteresowana historią i rzeczami historycznymi | @ 240305-lea-strazniczka-lasu

### Wątki


magiczne-szczury-podwierckie
rekiny-a-akademia

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 240305-lea-strazniczka-lasu         | niesamowicie świetna percepcja, winggirl Lei; poszła z Markiem i AMZ pomóc Gwiazdociszowi w sprawie badań z czasów wojny. Ucierpiała przez EfemeHorror. | 0111-10-21 - 0111-10-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arkadia Verlen       | 1 | ((240305-lea-strazniczka-lasu)) |
| Bogdan Gwiazdocisz   | 1 | ((240305-lea-strazniczka-lasu)) |
| Ernest Namertel      | 1 | ((240305-lea-strazniczka-lasu)) |
| Karolina Terienak    | 1 | ((240305-lea-strazniczka-lasu)) |
| Keira Amarco d'Namertel | 1 | ((240305-lea-strazniczka-lasu)) |
| Lea Samszar          | 1 | ((240305-lea-strazniczka-lasu)) |
| Marek Samszar        | 1 | ((240305-lea-strazniczka-lasu)) |
| Mariusz Kupieczka    | 1 | ((240305-lea-strazniczka-lasu)) |
| Marysia Sowińska     | 1 | ((240305-lea-strazniczka-lasu)) |
| Michał Klabacz       | 1 | ((240305-lea-strazniczka-lasu)) |
| Triana Porzecznik    | 1 | ((240305-lea-strazniczka-lasu)) |