---
categories: profile
factions: 
owner: public
title: Remigiusz Falorin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210630-listy-od-fanow               | eternianin, odpowiedzialny za PR. Oddał Ariannie Eidolona dla Eleny za to, że w kolejnych odcinkach Sekretów Orbitera pojawi się odcinek pokazujący dobrą stronę Eterni. Ma HISTORIĘ z Izabelą Zarantel. | 0112-01-15 - 0112-01-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((210630-listy-od-fanow)) |
| Bogdan Anatael       | 1 | ((210630-listy-od-fanow)) |
| Elena Verlen         | 1 | ((210630-listy-od-fanow)) |
| Izabela Zarantel     | 1 | ((210630-listy-od-fanow)) |
| Klaudia Stryk        | 1 | ((210630-listy-od-fanow)) |
| Michał Teriakin      | 1 | ((210630-listy-od-fanow)) |
| OE Lord Savaron      | 1 | ((210630-listy-od-fanow)) |
| Olgierd Drongon      | 1 | ((210630-listy-od-fanow)) |
| Rafael Galwarn       | 1 | ((210630-listy-od-fanow)) |
| TAI Rzieza d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI XT-723 d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI Zefiris          | 1 | ((210630-listy-od-fanow)) |