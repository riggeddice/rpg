---
categories: profile
factions: 
owner: public
title: Morrigan d'Tirakal
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210421-znudzona-zaloga-inferni      | reanimowane jej echo przez Eustachego, prawie przejęła kontrolę nad Persefoną d'Infernia. Ale Persi walczyła. Morrigan objęła Tirakala i skończyła zintegrowana z Samurajem i Persefoną jako jeden byt. | 0111-11-16 - 0111-11-19 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((210421-znudzona-zaloga-inferni)) |
| Elena Verlen         | 1 | ((210421-znudzona-zaloga-inferni)) |
| Eustachy Korkoran    | 1 | ((210421-znudzona-zaloga-inferni)) |
| Jolanta Sowińska     | 1 | ((210421-znudzona-zaloga-inferni)) |
| Klaudia Stryk        | 1 | ((210421-znudzona-zaloga-inferni)) |
| Leona Astrienko      | 1 | ((210421-znudzona-zaloga-inferni)) |
| Martyn Hiwasser      | 1 | ((210421-znudzona-zaloga-inferni)) |
| Otto Azgorn          | 1 | ((210421-znudzona-zaloga-inferni)) |
| Persefona d'Infernia | 1 | ((210421-znudzona-zaloga-inferni)) |
| Tomasz Sowiński      | 1 | ((210421-znudzona-zaloga-inferni)) |