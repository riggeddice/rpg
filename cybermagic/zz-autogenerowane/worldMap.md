1. Świat
    1. Eter Nieskończony
    1. Fikcja
    1. Multivirt
        1. MMO
            1. Kryształowy Pałac
        1. Smocze Wojny
            1. Otchłań Wężosmoków
            1. Siedziby Gildii
    1. Nierzeczywistość
    1. Primus
        1. Eter Nieskończony
            1. Mare Quixos
                1. Wyspa Rafanna
                    1. Centrum Wyspy
                    1. Dżungla Zmienna
                    1. Pierwsza Osada
                1. Zasadzka Scylli i Harybdy
        1. Sektor Astoriański
            1. Astoria, Kosmos
                1. Brama Trzypływów
            1. Astoria, Orbita
                1. Kontroler Pierwszy
                    1. Akademia Orbitera
                    1. Arena Kalaternijska
                    1. Hangary Alicantis
                    1. Laboratoria Dekontaminacyjne
                    1. Połączony Rdzeń
                    1. Sektor 22
                        1. Dom Uciech Wszelakich
                        1. Kasyno Stanisława
                    1. Sektor 25
                        1. Bar Solatium
                    1. Sektor 43
                        1. Laboratorium biomantyczne
                        1. Tor wyścigowy ścigaczy
                    1. Sektor 49
                    1. Sektor 57
                        1. Mordownia
                    1. Sektor Cywilny
                1. Stacja Orbitalna Epirjon
            1. Astoria, Pierścień Zewnętrzny
                1. Laboratorium Kranix
                1. Poligon Stoczni Neotik
                1. Stacja Medyczna Atropos
                1. Stacja przeładunkowa Saltomak
                1. Stocznia Neotik
                    1. Zewnętrzny dok ixioński
            1. Astoria
                1. Sojusz Letejski, NW
                    1. Ruiniec
                        1. Colubrinus Meditech
                        1. Colubrinus Psiarnia
                        1. Diamentowa Forteca
                        1. Kryształowa Forteca
                            1. Mauzoleum
                            1. Rajskie Peryferia
                        1. Ortus-Conticium, okolice
                            1. Rzeka Tirellia
                        1. Ortus-Conticium
                        1. Pustynia Kryształowa
                            1. Kryształowa Forteca
                        1. Skalny Labirynt
                        1. Studnia Bez Dna
                        1. Świątynia Bez Dna
                        1. Trzeci Raj, okolice
                            1. Kopiec Nojrepów
                            1. Zdrowa Ziemia
                        1. Trzeci Raj
                            1. Barbakan
                            1. Centrala Ataienne
                            1. Mały Kosmoport
                            1. Ratusz
                            1. Stacja Nadawcza
                            1. Zdrowa Ziemia
                1. Sojusz Letejski, SW
                    1. Granica Anomalii
                        1. Czarna Piramida
                        1. Forteca Symlotosu
                        1. Las Pusty, okolice
                        1. Pacyfika, obrzeża
                        1. Pacyfika
                        1. Ruina Iliminar Caos
                        1. Ruina miasteczka Kalterweiser
                        1. Ruiny Kaliritosa
                        1. Skałopływ
                            1. Herbast
                            1. Hotel Pustogorski
                            1. Jezioro Macek
                            1. Martwy Step
                            1. Osiedle Bezpieczne
                            1. Podfarma
                            1. Strażnica
                        1. Wieczna Maszyna, okolice
                        1. Wolne Ptaki
                            1. Królewska Baza
                    1. Wielki Mur Pustogorski
                1. Sojusz Letejski, W
                    1. Góry Hallarmeng
                        1. Przyprzelotyk
                            1. Korony Cmentarne
                                1. Dolina Krosadasza
                                1. Ruina Lohalian
                1. Sojusz Letejski
                    1. Aurum
                        1. Imperium Sowińskich
                            1. Krystalitium
                                1. Klub Eksplozja
                                1. Pałac Świateł
                        1. Pięciokąt Ichtis
                            1. Powiat Dzikiej Sieci
                            1. Powiat Pentalis
                                1. Pałac dla gości
                                1. Swawolnik
                        1. Powiat Blakenbauer
                            1. Gęstwina Duchów
                        1. Powiat Lemurski
                            1. Kruczaniec
                                1. Fabryka Mebli Larmat
                                1. Jezioro Topielców
                                1. Supermarket Złotko
                                1. Upiorny Las
                                    1. Mordownia
                                1. Zamek Pisarza
                            1. Wielka Arena Igrzysk
                        1. Powiat Niskowzgórza
                            1. Domena Arłacz
                                1. Posiadłość Arłacz
                                    1. Baterie Puryfikacji Krwi
                                    1. Małe lądowisko
                        1. Powiat Samszar
                            1. Fort Tawalizer
                                1. Centrum Miasta (C)
                                    1. Luksusowa dzielnica mieszkalna
                                        1. Jezioro Fortowe
                                1. Dzielnica mieszkaniowa (S)
                                    1. Domy mieszkalne
                                    1. Sklepy i centra handlowe
                                    1. Szkoły i placówki oświatowe
                                1. Obserwatorium Potworów i Fort (E)
                                    1. Areszt
                                    1. Koszary garnizonu
                                1. Wzgórza Potworów (N)
                                    1. Baterie defensywne
                                    1. Farmy kóz
                                    1. Obserwatoria potworów
                            1. Gwiazdoczy, okolice
                                1. Technopark Jutra
                                    1. Inkubatory małego biznesu
                            1. Gwiazdoczy
                                1. Centrum (Centrum)
                                    1. Puby i restauracje
                                    1. Sklepy i kawiarnie
                                1. Dzielnica Akademicka (NW, W)
                                    1. Czytelnie naukowe
                                    1. Kampus uczelniany
                                    1. Muzeum Historii
                                    1. Uniwersytet (NW)
                                    1. Wielka Biblioteka (W)
                                1. Dzielnica studencka (N)
                                    1. Strefa Sportowa
                                1. Dzielnica Technologii (S)
                                    1. Centralna stacja pociągów
                                    1. Centrum Eszary
                                    1. Park Technologiczny
                                    1. Warsztaty i inkubatory
                                1. Szczelina Światów (E)
                                    1. Archiwa duchów
                                    1. Instytut Sztuki Wspomaganej
                                    1. Kalejdoskop Astralny
                                    1. Magitrownie puryfikacyjne
                                    1. Plac rytuałów
                                    1. Wielki Cenotaf Skupiający
                            1. Karmazynowy Świt, okolice
                                1. Centrum Danych Symulacji Zarządzania
                                    1. Techbunkier Arvitas
                                        1. Kontrola bezpieczeństwa (1)
                                        1. Kwatery mieszkalne (1)
                                1. Stadion Mistrzów
                                1. Strefa ekonomiczna
                                    1. Biurowce
                            1. Karmazynowy Świt
                                1. Centralny Park Harmonii
                                    1. Gwiezdna Rozkosz
                                    1. Pałac Harmonii
                            1. Mirkala
                                1. Wielka Zielarnia (E)
                                1. Wzgórza Bezduszne (W)
                                    1. Fabryka Domów
                                    1. Technopark
                            1. Siewczyn
                                1. Astralne Ogrody (ES)
                                    1. Centralne Biura Rolnicze
                                    1. Centrum R&D dla zrównoważonego rolnictwa
                                    1. Dom Szamana
                                    1. Fabryka neutralizatorów astralnych
                                    1. Gaj Duchów
                                    1. Rezydencje Harmonii
                                1. Centrum Jedności Mieszka (Center)
                                    1. Bazar rękodzieła
                                    1. Drzewo Jedności
                                    1. Most jedności
                                    1. Park miejski
                                    1. Ratusz miejski
                                1. Północne obrzeża
                                    1. Spichlerz Jedności
                                1. Wzgórza Industrialnej Harmonii (NW)
                                    1. Fabryka siewników i sadzarek
                                    1. Fabryka traktorów
                                    1. Przestrzeń mieszkalna
                                    1. Sklepy i centra handlowe
                            1. Triticatus, północ
                                1. Dopływ Strumienia Pszenicznego
                                1. Stare magazyny
                            1. Triticatus
                                1. Makaroniarnia (NW)
                                    1. Formatornia
                                    1. Pola Pszenicy
                                    1. Semolinatorium
                                    1. Strumień Pszeniczny
                                    1. Systemy Irygacyjne
                                    1. Wielka Suszarnia
                                    1. Wielki Młyn
                                1. Pszenicznik (SE)
                                    1. Dworzec Maglev
                                    1. Magazyny
                                    1. Obszar administracyjny
                                    1. Rezydencje mieszkalne
                            1. Wańczarek
                                1. Wańczarek
                                    1. Dzielnica Sadowa (NW, W)
                                        1. Dom sołtysa
                                        1. Sady owocowe (NW)
                                    1. Dzielnica Smutku (E)
                                        1. Szpital Jabłoni
                            1. Wielkie Kwiatowisko
                                1. Hotel Odpoczynek Pszczół
                                1. Menhir Centralny
                        1. Sentipustkowie Pierwotne
                        1. Świat Dżungli
                            1. Ostropnącz
                        1. Verlenland
                            1. Arachnoziem
                                1. Bar Łeb Jaszczura
                                1. Garnizon
                                1. Kopalnia
                                1. Stacja Pociągu Maglev
                            1. Duchowiec Śmieszny, okolice
                            1. Duchowiec Śmieszny
                            1. Hold Bastion
                                1. Barbakan
                                1. Karcer
                                1. Karczma Szczur
                            1. Hold Karaan, obrzeża
                                1. Lądowisko
                            1. Hold Karaan
                                1. Barbakan Centralny
                                1. Krypta Plugastwa
                                1. Szpital Centralny
                            1. Koszarów Chłopięcy, okolice
                                1. Las Wszystkich Niedźwiedzi
                                1. Skały Koszarowe
                            1. Koszarów Chłopięcy
                                1. Miasteczko
                                    1. Bar Krwawy Topór
                                1. Wielki Stadion
                            1. Mikrast
                            1. Poniewierz, obrzeża
                                1. Jaskinie Poniewierskie
                            1. Poniewierz, północ
                                1. Osiedle Nadziei
                            1. Poniewierz
                                1. Archiwum Poniewierskie
                                1. Arena
                                1. Dom Uciech Wszelakich
                                1. Garnizon
                                1. Magazyny Anomalii
                                1. Zamek Gościnny
                            1. Potworzyk Straszny
                                1. Namiotowisko
                                1. Niedźwiedziowisko
                                    1. Wodospad Bohaterów
                            1. Skałkowa Myśl
                            1. Trójkąt Chaosu
                            1. VirtuFortis
                                1. Akademia VR Aegis
                                1. Bastion Przyrzeczny
                                1. Bazarek Lokalny
                                1. Stadion Sportowy
                                1. Wielki Plac Miraży
                            1. Wremłowo
                                1. rachityczny lasek z jaskiniami
                        1. Wielkie Księstwo Aktenir
                            1. Pałac Jasnego Ognia
                    1. Drzewiec
                        1. Powiat Zielony
                            1. Gniazdowo
                        1. Wielkorolnia
                            1. Żerżuch
                                1. Gospodarstwa
                                1. Jezioro
                                1. Pola uprawne
                    1. Powiat Jastrzębski
                        1. Małopies, okolice
                            1. Mokradła
                        1. Małopies
                            1. Hodowla psów
                            1. Motel
                            1. Stadion sportowy
                    1. Przelotyk
                        1. Maczkowiec, obrzeża
                            1. Zbiornik retencyjny
                        1. Maczkowiec
                            1. Gospodarstwa Przydrożne
                            1. Gospodarstwa Przyleśne
                            1. Hodowla viciniusów
                            1. Lasek Czerwony
                            1. Magitrownia Stu Maków
                            1. Szkoła
                        1. Przelotyk Wschodni
                            1. Cieniaszczyt, podziemia
                                1. Leyline
                            1. Cieniaszczyt
                                1. Arena Nadziei Tęczy
                                1. Bazar Wschodu Astorii
                                1. Kliniki Czarnego Światła
                                1. Knajpka Szkarłatny Szept
                                1. Kompleks Nukleon
                                1. Mordownia Czaszka Kralotha
                                1. Mrowisko
                                1. Pałac Szkarłatnego Światła
                                1. Teatr Posępny
                                1. Wewnątrzzbocze Zachodnie
                                    1. Panorama Światła
                            1. Przejściak
                                1. Hotel Pirat
                            1. Roszbór
                                1. Stary Tor Wyścigowy
                        1. Przelotyk Zachodni Dziki
                            1. Lancatim, okolice
                            1. Lancatim
                            1. Sanktuarium Kazitan
                                1. Dystrykt Szafir
                                    1. Komnata lecznicza
                                    1. Komnata mieszkalna
                        1. Wojnowiec
                            1. Klamża
                                1. Cmentarzysko Servarów
                                1. Knajpa Wygrana
                                1. Stacja Nadawcza Eter
                                1. Tor Wyścigowy
                    1. Szczeliniec
                        1. Powiat Jastrzębski
                            1. Jastrząbiec, okolice
                                1. Blokhaus Widmo
                                1. Containment Chambers
                                1. Klinika Iglica
                                    1. Kompleks Itaran
                                1. TechBunkier Sarrat
                            1. Jastrząbiec
                                1. Hotel Stacja Kosmiczna
                                1. Ratusz
                            1. Kalbark, Nierzeczywistość
                                1. Utopia
                            1. Kalbark
                                1. Autoklub Piękna
                                1. Escape Room Lustereczko
                                1. Mini Barbakan
                                1. Rzeźnia
                            1. Praszalek, okolice
                                1. Fabryka schronień Defensor
                                1. Lasek Janor
                                1. Park rozrywki Janor
                            1. Praszalek
                                1. Komenda policji
                        1. Powiat Pustogorski
                            1. Czarnopalec
                                1. Kotlina Mikarajły
                                1. Pusta Wieś
                            1. Czemerta, okolice
                                1. Baza Irrydius
                                1. Fortifarma Irrydia
                                1. Studnia Irrydiańska
                            1. Czemerta
                            1. Czerwinór
                                1. Biurowiec Wielotransu
                            1. Czółenko, okolice
                                1. Las Trzęsawny
                            1. Czółenko
                                1. Bunkry
                                1. Generatory Keriltorn
                                1. Opuszczony Silos
                                1. Pola Północne
                                1. Tancbuda
                            1. Kramamcz
                                1. Włóknin
                            1. Majkłapiec
                                1. Farma Krecik
                                1. Kociarnia Zawtrak
                                1. Wegefarma Myriad
                                1. Zakład Paprykarski Majkłapiec
                            1. Podwiert, obrzeża
                                1. Kompleks Badawczy Skelidar
                                1. Kosmoport
                            1. Podwiert, okolice
                                1. Bioskładowisko podziemne
                                1. Fortifarma Lechotka
                                1. Oczyszczalnia Słonecznik
                            1. Podwiert
                                1. Bastion Pustogoru
                                1. Bunkier Rezydenta
                                1. Dolina Biurowa
                                1. Dzielnica Luksusu Rekinów
                                    1. Fortyfikacje Rolanda
                                    1. Obrzeża Biedy
                                        1. Domy Ubóstwa
                                        1. Hotel Milord
                                        1. Stadion Lotników
                                        1. Stajnia Rumaków
                                    1. Sektor Brudu i Nudy
                                        1. Komputerownia
                                        1. Konwerter Magielektryczny
                                        1. Magitrownia Pogardy
                                        1. Skrytki Czereśniaka
                                    1. Serce Luksusu
                                        1. Apartamentowce Elity
                                        1. Arena Amelii
                                        1. Fontanna Królewska
                                        1. Kawiarenka Relaks
                                        1. Lecznica Rannej Rybki
                                1. Iglice Nadziei
                                    1. Posiadłość Arieników
                                1. Klub Arkadia
                                1. Komenda policji
                                1. Komisariat
                                1. Kompleks Korporacyjny
                                    1. Bar Ciężki Młot
                                    1. Chemiczna firma Kurara
                                    1. Dokumentarium Terminuskie
                                    1. Dystrybutor Prądu Ozitek
                                    1. Elektrownia Węglowa Szarpien
                                    1. Zakład Recyklingu Owczarek
                                1. Kopalnia Terposzy
                                1. Las Trzęsawny
                                    1. Jeziorko Mokre
                                    1. Schron TRZ-17
                                1. Magazyny sprzętu ciężkiego
                                1. Magazyny Sprzętu Ciężkiego
                                    1. Klub Napartar
                                1. Odlewnia
                                1. Osiedle Leszczynowe
                                    1. Klub Arkadia
                                    1. Sklep z reliktami Fantasmagoria
                                    1. Szkoła Nowa
                                1. Osiedle Rdzawych Dębów
                                    1. Sklep Eliksir Siekiery
                                1. Osiedle Sosen
                                1. Osiedle Tęczy
                                1. Sensoplex
                                    1. Koloseum
                                    1. Labirynt
                                1. Technopark Senetis
                                1. Tor Wyścigowy Pamięci
                            1. Powiat Przymurski
                                1. Fortifarma Karpovska
                                1. Las Przymurski
                            1. Przywiesław
                                1. Przychodnia
                            1. Pustogor, okolice
                                1. Firma Protogór
                                1. Rezydencja Blakenbauerów
                            1. Pustogor
                                1. Barbakan
                                1. Eksterior
                                    1. Arena Szalonego Króla
                                    1. Dolina Uciech
                                    1. Fort Mikado
                                    1. Miasteczko
                                        1. Knajpa Górska Szalupa
                                    1. Zamek Weteranów
                                1. Gabinet Pięknotki
                                1. Interior
                                    1. Bunkry Barbakanu
                                    1. Dzielnica Mieszkalna
                                    1. Inkubator Ekonomiczny Samszarów
                                    1. Laboratorium Senetis
                                1. Kawiarenka Ciemna Strona
                                1. Knajpa Górska Szalupa
                                1. Kompleks Testowy
                                1. Miasteczko
                                1. Port Eteryczny
                                1. Pustułka
                                1. Rdzeń
                                    1. Barbakan
                                        1. Kazamaty
                                    1. Szpital Terminuski
                                1. Zamek Weteranów
                            1. Samoklęska
                                1. Dom Kornela
                                1. Laboratorium analityczne
                            1. Trzęsawisko Zjawosztup
                            1. Trzykwiat, okolice
                                1. Las Trzęsawny
                                    1. Fortifarmy Trzykwiata
                                    1. Kurhan Metalu
                            1. Trzykwiat
                                1. Centrum
                                    1. Bazar
                                    1. Ratusz
                                    1. Zbrojownia
                                1. Południowy Pierścień
                                    1. Osiedle Południowe
                                    1. Sady i Szklarnie
                                    1. Wieże zewnętrzne
                                1. Północny Pierścień
                                    1. Bastion
                                    1. Dworzec Maglev
                                    1. Osiedle Północne
                                    1. Służby Miejskie
                                    1. Wieże zewnętrzne
                            1. Zaczęstwo, obrzeża
                                1. Przytułek Cicha Gwiazdka
                            1. Zaczęstwo
                                1. Akademia Magii, kampus
                                    1. Akademik
                                    1. Arena Treningowa
                                    1. Artefaktorium
                                    1. Audytorium
                                    1. Budynek Centralny
                                        1. Piwnica
                                            1. Stare AI Core
                                        1. Skrzydło Loris
                                            1. Laboratorium Wysokich Energii
                                            1. Nieskończony Labirynt
                                    1. Domek dyrektora
                                    1. Las Trzęsawny
                                    1. Złomiarium
                                1. Akademia Magii
                                1. Arena Migświatła
                                1. Bazar Różności
                                1. Biurowce
                                    1. Biurowiec Formael
                                    1. Papierówka
                                1. Biurowiec Gorland
                                1. Cyberszkoła
                                1. Dzielnica Bliskomagiczna
                                1. Dzielnica Kwiecista
                                    1. Rezydencja Porzeczników
                                        1. Garaż Groźnych Eksperymentów
                                        1. Podziemne Laboratorium
                                1. Dzielnica Ogrodów
                                    1. Wielki Szpital Magiczny
                                1. Hotel Tellur
                                1. Kasyno Marzeń
                                1. Kawiarenka Leopold
                                1. Klub Poetycki Sucharek
                                1. Kompleks Tiamenat
                                    1. Budynek Wołkowca
                                    1. Centralny Biolab
                                    1. Centrum Dowodzenia
                                1. Kwatera Terminusa
                                1. Las Trzęsawny
                                    1. Medbunkier Sigma
                                1. Mekka Wolności
                                1. Nieużytki Staszka
                                1. Osiedle Ptasie
                                1. Park Centralny
                                    1. Jezioro Gęsie
                                1. Sklep Komputrix
                                1. Sypialnia Szczelińca
                                1. Wschodnie Pole Namiotowe
                                1. Złomowisko
                            1. Żarnia
                                1. Osiedle Połacierz
                                1. Żarlasek
                                1. Żernia
                        1. Powiat Złotodajski
                            1. Złotkordza
                                1. Stadion
                        1. Pustogor
                        1. Trzęsawisko Zjawosztup
                            1. Accadorian
                            1. Głodna Ziemia
                            1. Laboratorium W Drzewie
                            1. Sfera Pyłu
                            1. Toń Pustki
                    1. Trójząb Wielki
                        1. Bramiasto
                        1. Czarnomiód
                            1. Bazar
                        1. Niedźwiedźnik
                            1. Dworek Karmazyn
                            1. Kępa Niedźwiedzia
                            1. Magitrownia Błękitna
                            1. Rzeka Błękitka
                        1. Toporzysko
                            1. Krukniew
                                1. Stary Labirynt
                            1. Wysoczysko
                                1. Burgerownia Wielka Świnia
                                1. Hotel Złotocień
                                1. Jadłodajnia Księżyc
                                1. Szkoła Makina
                    1. Trójząb
                        1. Powiat Jezierzy
                            1. Wypływowo
                                1. Osiedle Domowe
                                    1. Hotel Bez Pyłu
                                1. Osiedle Lotnicze
                                    1. Montownia Awianów
                                1. Osiedle Siłowiec
                                    1. Magitrownia Selenin
                1. Wielkie Jezioro Indygo
                    1. Obszar Sojuszu Letejskiego
                        1. Podwodna Stacja Badawcza Medimmortal
                        1. Rafa Prisma
                        1. Rów Penelope
            1. Brama Kariańska
            1. Brama Trzypływów
            1. Dwupunkt Cztery
                1. Kartaliana
                    1. Ciemnica
            1. Elang, księżyc Astorii
                1. EtAur Zwycięska
                    1. Magazyny wewnętrzne
                    1. Sektor bioinżynierii
                        1. Biolab
                        1. Komory żywieniowe
                        1. Skrzydło medyczne
                    1. Sektor inżynierski
                        1. Panele słoneczne i bateriownia
                        1. Podtrzymywanie życia
                        1. Serwisownia
                        1. Stacja pozyskiwania wody
                    1. Sektor mieszkalny
                        1. Barbakan
                            1. Administracja
                            1. Ochrona
                            1. Rdzeń AI
                        1. Centrum Rozrywki
                        1. Podtrzymywanie życia
                        1. Pomieszczenia mieszkalne
                        1. Przedszkole
                        1. Stołówki
                        1. Szklarnie
                    1. Sektor przeładunkowy
                        1. Atraktory małych pakunków
                        1. Magazyny
                        1. Pieczara Gaulronów
                        1. Punkt celny
                        1. Stacja grazerów
                        1. Stacja przeładunkowa
                        1. Starport
                    1. Ukryte sektory
            1. Iorus
                1. Iorus, pierścień
                    1. Keldan Voss
            1. Kosmiczna Pustka
            1. Krwawa Baza Piracka
            1. Neikatis, orbita
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Lirvint
                    1. Arkologia Nativis, okolice
                        1. Szepczące Wydmy
                    1. Arkologia Nativis
                        1. Poziom 1 - Dolny
                            1. Południe
                                1. Magazyny
                                1. Processing
                                1. Stacje Skorpionów
                            1. Północ - Stara Arkologia
                                1. Blokhaus E
                                1. Blokhaus F
                                    1. Szczurowisko
                                1. Stare Wejście Północne
                                1. Szczurowisko
                            1. Wschód
                                1. Farmy Wschodnie
                            1. Zachód
                                1. Centrala Prometeusa
                                1. Stare TechBunkry
                        1. Poziom 2 - Niższy Środkowy
                            1. Południe
                                1. Engineering
                                1. Power Core
                            1. Północ
                                1. Bazar i sklepy
                            1. Wschód
                                1. Centrum Kultury i Rozrywki
                                    1. Bar Śrubka z Masła
                                    1. Muzeum Kidirona
                                    1. Ogrody Gurdacza
                                    1. Stacja holosymulacji
                            1. Zachód
                                1. Koszary Hełmów
                                1. Sektor Porządkowy
                        1. Poziom 3 - Górny Środkowy
                            1. Południe
                                1. Life Support
                                1. Medical
                            1. Wschód
                                1. Dzielnica Luksusu
                                    1. Ambasadorka Ukojenia
                                    1. Ogrody Wiecznej Zieleni
                                    1. Stacja holosymulacji
                        1. Poziom 4 - Górny
                            1. Wschód
                                1. Centrala dowodzenia
                                1. Obserwatorium Astronomiczne
                                1. Radiowęzeł
                            1. Zachód
                                1. Tereny Sportowe
                        1. Poziom 5 - Szczyt
                            1. Północ
                                1. Port kosmiczny
                    1. Arkologia Sarviel, okolice
                        1. Krater Coruscatis
                    1. CES Mineralis
                    1. CES Purdont, okolice
                        1. Wiertło Ekopoezy Delta
                    1. CES Purdont
                        1. Kafeteria
                        1. Laboratorium Ekopoezy
                        1. Life Support
                        1. System łączności
                    1. Cognitio Nexus
                        1. Poziom (-1) Mieszkalny
                            1. NE
                                1. Administracja
                                1. Medical
                                1. Rebreathing
                            1. NW
                                1. Magazyny wewnętrzne
                                1. Sprzęt ciężki, kopalniany
                                1. System obronny
                            1. S
                                1. Centrum Rozrywki
                                1. Park
                                1. Sektor mieszkalny
                                    1. Przedszkole
                                    1. Stołówki
                            1. Szyb centralny
                        1. Poziom (-2) Industrialny
                            1. NE
                                1. Centrum Dowodzenia
                                1. Podtrzymywanie życia
                                1. Rdzeń AI
                            1. NW
                                1. Fabrykacja
                                1. Inżynieria
                                1. Magazyny
                                1. Reaktor
                                1. Reprocesor Śmieci
                            1. S
                                1. Stacje badawcze
                                1. Szklarnie podziemne
                        1. Poziom (-3) Badań Niebezpiecznych
                            1. Anomaliczne
                            1. Biolab
                            1. Magitech
                            1. Śluza defensywna
                        1. Poziom zero
                            1. Stacja przeładunkowa
                            1. Starport
                            1. System defensywny
                    1. Ogród Zwłok Exerinna
                1. Dystrykt Lennet
                1. Dystrykt Quintal
                    1. Arkologia Aspiria
                    1. Stacja Damnos
                1. Dystrykt Samrajia
                    1. Arkologia Terkelis
                        1. Instytut Alany Castelli
            1. Libracja Lirańska
                1. Anomalia Kolapsu, orbita
                    1. CON Ratio Spei
                        1. Brzuchowisko
                        1. Rdzeń
                            1. Mostek
                            1. Starport
                        1. Torus
                            1. Brzuchowisko
                                1. Bar Proch Strzelniczy
                                1. Bazarek
                    1. Planetoida Kazmirian
                    1. SC Nonarion Nadziei
                        1. Moduł ExpanLuminis
                        1. Moduł Remedianin
                    1. Sortownia Seibert
                    1. Strefa Biur HR
                    1. Strefa Upiorów Orbitera
                        1. Planetoida Kazmirian
                        1. Planetoida Lodowca
                1. Anomalia Kolapsu
                    1. Cmentarzysko Statków
                        1. Krypta Saitaera
                1. Stacja Ratunkowa Allandea
            1. Pas Omszawera
                1. Anomalia Somnium, orbita
                    1. Anomalia Somnium
                    1. Mechosystem Amientor
                        1. Aleksandria
                1. Kolonia Hikirion
                1. Kolonia Samojed
                    1. Stacja Astropociągów
                    1. Zona Czarna
                    1. Zona Mieszkalna
                1. Stacja Omszawer Wielki
            1. Pas Teliriański
                1. Planetoidy Kazimierza
                    1. Domena Ukojenia
                        1. Planetoida Mirnas
                        1. Planetoida Talio
                        1. Stacja Górnicza Arinkaria
                        1. Stacja Ukojenie Barana
                        1. Szamunczak
                            1. Klub Korona
                    1. Morze Ułud
                    1. Planetoida Asimear
                        1. Planetoida właściwa
                            1. Kompleks mieszkalny CK14
                                1. Dom Poprawczy Irys
                        1. Stacja Lazarin
                1. Stacja Telira-Melusit VII
            1. Stacja Valentina
                1. Krwawa Arena
                1. Slave Pens
                1. Speluna Smutny Rajder
            1. Stocznia Kariańska
            1. Transfer Iorus Anomalia
        1. Sektor Kalmantis
            1. Sebirialis, orbita
                1. CON Szernief
                    1. Orbitujące stacje hydroponiczne
                    1. Powłoka Wewnętrzna
                        1. Poziom Minus Dwa
                            1. Więzienie
                        1. Poziom Minus Jeden
                            1. Obszar Mieszkalny Savaran
                        1. Poziom Minus Trzy
                            1. Wielkie Obrady
                    1. Powłoka Zewnętrzna
                        1. Panele Słoneczne
            1. Sebirialis
                1. Krater Ablardius
                    1. Kopalnie Ablardius
                1. Płaskowyż Zaitrus
                    1. Ukryta Baza NavirMed
        1. Sektor Lacarin
        1. Sektor Mevilig
            1. Chmura Piranii
            1. Keratlia
            1. Planetoida Kalarfam
        1. Sektor Nieznany
            1. Elora
        1. Sektor Noviter
            1. Brama
            1. Pas Sowińskiego
                1. Anomalia Dorszant
                1. Czarnoszept
                1. Stacja Dorszant
                    1. Czarne Sektory
                1. Stacja Szotaron
        1. Sektor Paradizo
            1. Brama
        1. Sektor Sanadan
            1. Brama Polarna
        1. Zagubieni w Kosmosie
            1. Crepuscula
                1. Pasmo Zmroku
                    1. Baza Noktiańska Zona Tres
                    1. Podwodna Brama Eteryczna