# Miranda Ceres
## Identyfikator

Id: 9999-miranda-ceres

## Sekcja Opowieści

### Antos, szafa i statek Piscerników

* **uid:** 220503-antos-szafa-i-statek-piscernikow, _numer względny_: 5
* **daty:** 0108-09-23 - 0108-09-25
* **obecni:** Anna Szrakt, Antos Kuramin, Bartek Wudrak, Kara Prazdnik, Miranda Ceres

Streszczenie:

Kara bardzo chciała wygrać konkurs karaoke, więc została z Antosem gdy Królowa odleciała. Gdy przyleciał statek Luxuritias, Kara bardzo próbowała się wyślizgnąć - ale Antos zamknął ją w szafie. Wojciech pamięta - Piscernikowie z Luxuritias są często powiązani z handlarzami niewolników, więc Anna nie pozwoliła nikomu opuszczać Królowej. Tymczasem Anna musiała sprać Bartka by Wojciech i Romka mieli spokój, potem opanować dzieciaki, które stwierdziły, że da się dobrze okraść Luxuritias.

Aktor w Opowieści:

* Dokonanie:
    * koordynowała i dyskretnie przesyłała wszystkie ważne informacje Annie i Zespołowi - np. to, że Luxuritias / Piscernik to może być statek niewolniczy.


### Lepsza kariera dla Romki

* **uid:** 220405-lepsza-kariera-dla-romki, _numer względny_: 4
* **daty:** 0108-09-15 - 0108-09-17
* **obecni:** Anna Szrakt, Antos Kuramin, Damian Szczugor, Helena Banbadan, Miranda Ceres, Romana Kundel, SC Królowa Przygód, Ursyn Uszat, Wojciech Kaznodzieja

Streszczenie:

Blakvelowcy chcą usunąć eks-piratów z Królowej, ale Miranda to uniemożliwiła. Anna zatrzymała Helenę przed ćpaniem i wsadziła Wojciechowi Romkę pod opiekę. Dzięki temu Poważne Myśli Romki o prostytucji się oddaliły. A potem Anna musiała już tylko porozmawiać z Antosem Kuraminem, by się dla niej poopiekował Heleną...

Aktor w Opowieści:

* Dokonanie:
    * nadal działa z cienia - pokazała Annie że Helena się szprycuje i pokazała Romce ULTRA HARD PORN by ta nie szła w prostytucję. Królowa cieni :-).


### Młodociani i pirat na Królowej

* **uid:** 220329-mlodociani-i-pirat-na-krolowej, _numer względny_: 3
* **daty:** 0108-08-29 - 0108-09-09
* **obecni:** Anna Szrakt, Bartek Wudrak, Berdysz Rozdzieracz, Gotard Kicjusz, Helena Banbadan, Kara Prazdnik, Łucjan Torwold, Miranda Ceres, Prokop Umarkon, Romana Kundel, SC Królowa Przygód, Seweryn Grzęźlik, Wojciech Kaznodzieja

Streszczenie:

Królowa Przygód przeszła przez Asimear i zebrała 3 młodocianych przestępców i Annę, by młodzi obejrzeli życie do którego aspirują (zwykle górników w Pasie). Załoga i dzieciaki nie współpracowały najlepiej, aż Anna nacisnęła. Po drodze Królowa napotkała statek piracki w opałach, pomogła zestrzelić Strachy i przejęła statek piracki. Niestety, kapitan (Berdysz) wślizgnął się na Królową. Owszem - stracił statek i większość załogi, ale odszedł wolno. A Królowa dostała nowych członków załogi i pryzowe za jednostkę piracką.

Aktor w Opowieści:

* Dokonanie:
    * nigdy się nie zdradziła, choć to ona steruje Królową Przygód. Uratowała statek przed Berdyszem, potem udając Annę połączyła z nim dzieci. Niewidzialne wsparcie.


### Planetoida bogatsza niż być powinna

* **uid:** 231027-planetoida-bogatsza-niz-byc-powinna, _numer względny_: 2
* **daty:** 0108-06-29 - 0108-07-02
* **obecni:** Bartek Burbundow, Gotard Kicjusz, Grzegorz Fabutownik, Juliusz Cieślawok, Kazimierz Zamglis, Łucjan Torwold, Mikołaj Resztkowiec, Miranda Ceres, Prokop Umarkon, SC Królowa Przygód, Seweryn Grzęźlik

Streszczenie:

Naprawiona Królowa Przygód została skierowana jako kiepski statek górniczy przez Blakvelowców. Niestety, planetoida która miała być wartościowa miała 'gniazdo' nieaktywnych Alterientów i jeden z górników został Zmieniony. Miranda kombinowała, jak uzmysłowić to ludziom niezdolnym do postrzegania zagrożenia i znalazła swoją drogę przez paranoicznego Łucjana. Potem - widząc NieGórnika - rozstrzelała go jedynym sprawnym działkiem i zasługi skromnie oddała załodze. Jeszcze jedna misja, która się udała bez strat załogi...

Aktor w Opowieści:

* Dokonanie:
    * na typowej operacji górniczej ratowała załogę przed kiepskim sprzętem i awariami udając Ceres. Gdy pojawiła się anomalia Alteris to najpierw wylogikowała z czym ma do czynienia, potem użyła paranoi Łucjana by on też do tego doszedł a potem udawała że jest psyche ludzi i gadała do nich bezpośrednio. Na końcu - zestrzeliła NieGórnika i pokasowała odpowiednio logi.


### Wskrzeszenie Królowej Przygód

* **uid:** 220327-wskrzeszenie-krolowej-przygod, _numer względny_: 1
* **daty:** 0108-05-01 - 0108-05-16
* **obecni:** Damian Szczugor, Gotard Kicjusz, Łucjan Torwold, Miranda Ceres, Prokop Umarkon, SC Królowa Przygód, Seweryn Grzęźlik

Streszczenie:

Miranda (kimkolwiek jest) została sprzężona z Królową Przygód; załoga zginęła. Miranda była ostatnio w okolicy Stacji Valentina, ale obudziła się w Pasie Kazimierza. Znaleziona przez trzech górników; "przekonała" ich manipulując systemami, że tylko oni mogą tą jednostką dowodzić i doprowadziła do tego, że naprawią ją Blakvelowcy, by zrobić z niej dalekosiężny średni statek transportowy. Miranda COŚ WIE; jest to jakoś powiązane z Percivalem Diakonem.

Aktor w Opowieści:

* Dokonanie:
    * bardzo zmodyfikowana TAI Ceres; ma jakieś powiązanie z Percivalem Diakonem. Reanimowana w Pasie Kazimierza, wrobiła wszystkich w to, że jej statek (Królowa Przygód) będzie odbudowana przez blakvelowców i trzech górników zostanie członkami jej załogi.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0108-09-25
    1. Primus    : 5, @: 0108-09-25
        1. Sektor Astoriański    : 5, @: 0108-09-25
            1. Pas Teliriański    : 5, @: 0108-09-25
                1. Planetoidy Kazimierza    : 5, @: 0108-09-25
                    1. Domena Ukojenia    : 5, @: 0108-09-25
                        1. Szamunczak    : 2, @: 0108-09-25
                            1. Klub Korona    : 1, @: 0108-09-17
                    1. Planetoida Asimear    : 1, @: 0108-09-09
                        1. Planetoida właściwa    : 1, @: 0108-09-09
                            1. Kompleks mieszkalny CK14    : 1, @: 0108-09-09
                                1. Dom Poprawczy Irys    : 1, @: 0108-09-09

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| SC Królowa Przygód   | 4 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Anna Szrakt          | 3 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki; 220503-antos-szafa-i-statek-piscernikow)) |
| Gotard Kicjusz       | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Łucjan Torwold       | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Prokop Umarkon       | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Seweryn Grzęźlik     | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 231027-planetoida-bogatsza-niz-byc-powinna)) |
| Antos Kuramin        | 2 | ((220405-lepsza-kariera-dla-romki; 220503-antos-szafa-i-statek-piscernikow)) |
| Bartek Wudrak        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220503-antos-szafa-i-statek-piscernikow)) |
| Damian Szczugor      | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220405-lepsza-kariera-dla-romki)) |
| Helena Banbadan      | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Kara Prazdnik        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220503-antos-szafa-i-statek-piscernikow)) |
| Romana Kundel        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Wojciech Kaznodzieja | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Bartek Burbundow     | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Berdysz Rozdzieracz  | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Grzegorz Fabutownik  | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Juliusz Cieślawok    | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Kazimierz Zamglis    | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Mikołaj Resztkowiec  | 1 | ((231027-planetoida-bogatsza-niz-byc-powinna)) |
| Ursyn Uszat          | 1 | ((220405-lepsza-kariera-dla-romki)) |