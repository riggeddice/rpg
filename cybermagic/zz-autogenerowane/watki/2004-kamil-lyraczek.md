# Kamil Lyraczek
## Identyfikator

Id: 2004-kamil-lyraczek

## Sekcja Opowieści

### To nie pułapka na Nereidę...

* **uid:** 220105-to-nie-pulapka-na-nereide, _numer względny_: 14
* **daty:** 0112-05-04 - 0112-05-09
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Lutus Amerin, Maria Naavas, Natalia Aradin, Saitaer

Streszczenie:

Plan - dorwanie tajemniczych koloidowych korwet. Wiemy, że to pułapka, więc zróbmy kontr-pułapkę. Ale okazało się, że to nie pułapka na Nereidę a na Infernię. Standardowe badania wprowadziły w Infernię (Dianę) memetic payload i Diana prawie wpadła pod kontrolę Saitaera - odrzucenie kokonu z Natalią sprawiło, że udało się ciężko uszkodzoną Infernię uratować. Zwalczając ixion Arianna sięgnęła do Esuriit przez Elenę; Infernia by zginęła gdyby nie samobójcza akcja Izabeli i Kamila. Elena wchłonęła tyle energii ile była w stanie, przekraczając swój Wzór. ALE - Infernia przetrwała. Przy okazji, Natalia się wykluła. Ani osoba ani statek kosmiczny - mordercza anomalia kosmiczna.

Aktor w Opowieści:

* Dokonanie:
    * oddał życie, by tylko Infernia i kult mogły przetrwać atak Saitaera. Utrzymał memetycznie wiarę w Ariannę, w integralność Inferni. Wierzył nawet gdy miażdżyły go wrota. Nie miał lekkiej śmierci. KIA.


### Sklejanie Inferni do kupy

* **uid:** 211215-sklejanie-inferni-do-kupy, _numer względny_: 13
* **daty:** 0112-04-27 - 0112-04-29
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Kamil Lyraczek, Kasandra Destrukcja Diakon, Leona Astrienko, Maria Naavas, OO Infernia, Roland Sowiński

Streszczenie:

Ixioński wariant Inferni okazuje się całkiem słodki i pod kontrolą Eustachego. Morale Inferni jest w ruinie. Z pomocą Marii Arianna przesuwa kulturę Inferni w stronę "kultyści + noktianie + miłośnicy psychopatycznej lolitki Diany". Eustachy wpada w pułapkę Leony, która chce go wyleczyć czerwiem Esuriit - ale Eustachy przekonuje ją, że ma Dianę pod kontrolą. Jednak gdzieś w tle znajduje się prawdziwy ixioński potwór na stoczni Neotik.

Aktor w Opowieści:

* Dokonanie:
    * wyjaśnił Ariannie, że z Izą tworzą kult dookoła Arianny (Vigilusa) i Eustachego (Nihilusa). Figurki itp. Jest za głęboko jej oddany. Arianna się zmartwiła...


### Kurczakownia

* **uid:** 211020-kurczakownia, _numer względny_: 12
* **daty:** 0112-03-24 - 0112-03-26
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, OO Infernia, Otto Azgorn, Vigilus Mevilig

Streszczenie:

By uratować jak najwięcej osób w Sektorze Mevilig, Eustachy zaprojektował Kurczakownię - maszynę do oddzielania mechanicznego głów i rdzenia od reszty człowieka. Arianna starła się ze Zbawicielem-Niszczycielem jako aspekt Zbawiciela, wspierana przez Działo Rozpaczy (Eustachy). Udało im się sporo uratować (40% populacji Planetoidy Kalarfam), acz załoga Inferni będzie mieć koszmary senne. Klaudia zebrała dość danych, by zapobiec naturalnej manifestacji Zbawiciela-Niszczyciela w Sektorze Astoriańskim i zrobić kontrakcję jak co.

Aktor w Opowieści:

* Dokonanie:
    * pisze teksty, które biozsyntetyzowana "głowa" kultysty Zbawiciela-Niszczyciela ma mówić. Chodzi o przekonanie innych kultystów, że Arianna JEST Zbawicielem.
* Progresja:
    * TRAUMA. Kurczakowanie - rekurczakowanie. Plus Zbawiciel-Niszczyciel. To oznacza, że ARIANNA jest NAPRAWDĘ aspektem Zbawiciela. Włączył w kult.


### Nieudana infiltracja Inferni

* **uid:** 210616-nieudana-infiltracja-inferni, _numer względny_: 11
* **daty:** 0112-01-27 - 0112-02-01
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Marian Tosen, OO Opresor, Roland Sowiński

Streszczenie:

Marian Tosen poprosił Infernię o to, by sprawdzili co się dzieje w Anomalii Kolapsu - podejrzewa anomalie memetyczne, skoro Orbiter traci tam statki  (m.in. OO Opresor) i pojawiają się jednostki nieoznaczonej frakcji. Tymczasem Roland Sowiński chce na pokład. Arianna wybiła mu to z głowy, ale wykryli agentkę wśród jego guwernantek - Flawię Blakenbauer, która miała się nim opiekować i jest trochę zniewolona przez Sowińskich. By zadośćuczynić po tym co Elena zrobiła i trochę Flawii pomóc, Flawia dołączyła do Inferni. "Oficjalnie" udało jej się Infernię zinfiltrować...

Aktor w Opowieści:

* Dokonanie:
    * skutecznie przekonał Rolanda Sowińskiego (i jego guwernantki), że Anomalia Kolapsu to najstraszniejsze miejsce ever. Nie chcą tam lecieć :D.


### Morderstwo na Inferni

* **uid:** 210526-morderstwo-na-inferni, _numer względny_: 10
* **daty:** 0111-12-31 - 0112-01-06
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Feliks Walrond, Kamil Lyraczek, Karol Reichard, Klaudia Stryk, Leona Astrienko, Marian Tosen, Martyn Hiwasser, OO Tivr, Otto Azgorn, Tal Marczak

Streszczenie:

Noktiański mechanik Inferni, Tal Marczak, chciał zniszczyć OO Tivr (własność noktiańskiej rodziny Tala). Noktianie trzymają się razem - inni mechanicy Inferni doprowadzili do morderstwa Tala na K1 i wzięli amnestyki, by nikt nie wiedział co się stało. Zespół Inferni skutecznie doszedł do tego o co chodzi, odpowiedzialnych za morderstwo noktian przeniesiono na Żelazko a Arianna poważnie opieprzyła swoją noktiańską załogę. Będzie lepiej w przyszłości.

Aktor w Opowieści:

* Dokonanie:
    * mimo swoich wybitnych umiejętności ludzkich, inspiracji i perswazji nawet jemu nie udało się dotrzeć do noktian na Inferni. Wie o nich trochę, ale nie jest blisko i nie zna sekretów.


### Dekralotyzacja Asimear

* **uid:** 210414-dekralotyzacja-asimear, _numer względny_: 9
* **daty:** 0111-11-03 - 0111-11-12
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Llarnagraht, Malictrix d'Pandora, Mariusz Tubalon, Martyn Hiwasser, Martyna Bianistek, SCA Płetwal Błękitny, Tomasz Sowiński

Streszczenie:

Wiedząc, że Jolanta jest pod wpływem kralotha Infernia sfabrykowała kralotyczny alergizator. Arianna zdradziła się Aesimar i powiedziała, że ma zamiar zniszczyć kralotha z ramienia Orbitera. Udało się kralotha zmiażdżyć i zepchnąć do podziemi Asimear, choć ze sporymi stratami ludzkimi; tymczasem Tirakal uciekł na Płetwala. Elena zinfiltrowała Płetwala, ale nie mogła zniszczyć Tirakala nie krzywdząc ludzi. Destrukcję Tirakala Infernia zostawiła więc Malictrix.

Aktor w Opowieści:

* Dokonanie:
    * skutecznie nastraszył Tomasza Sowińskiego, żeby ten współpracował z Arianną. Potem - ustawił kult Arianny, by wzmocnić jej moc przy łączeniu brainwave Jolanty i Eleny (by oszukać Entropik).


### Krypta i Wyjec

* **uid:** 201216-krypta-i-wyjec, _numer względny_: 8
* **daty:** 0111-03-29 - 0111-03-31
* **obecni:** AK Nocna Krypta, AK Wyjec, Anastazja Sowińska, Arianna Verlen, Diana Arłacz, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser

Streszczenie:

Podczas walki Nocnej Krypty i Wyjca Zespół wbił się na Nocną Kryptę, by wyleczyć się z Różowej Plagi. Na miejscu okazało się, że Krypta ma "nowe programowanie" (leczenie lub 'zachowanie'), nowe mechanizmy (MIRV, anomalne działo, anioły) oraz że jest groźniejsza niż kiedykolwiek. Arianna zostawiła na pokładzie Krypty Anastazję, by Krypta naprawiła jej Wzór, po czym oszukała Sowińskich, że niby Krypta porwała Anastazję.

Aktor w Opowieści:

* Dokonanie:
    * z kultystami Arianny udało mu się przyjąć potężne uderzenie krzyku Wyjca; utrzymał Infernię pryzmatycznie pod wpływem chorej anomalii.


### Anastazja - bohaterką?

* **uid:** 201118-anastazja-bohaterka, _numer względny_: 7
* **daty:** 0111-03-22 - 0111-03-25
* **obecni:** Anastazja Sowińska, Arianna Verlen, Diana Arłacz, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, Stefan Jamniczek

Streszczenie:

Anastazja nie została bohaterką. Wpierw Eustachy wplątał się w pojedynek o Dianę z jakimś szlachcicem - skończyło się różową mgiełką miłości i pospieszną ewakuacją Inferni z Kontrolera Pierwszego, potem epicki plan Izy z piratami rozpadł się, bo na Infernię polował jakiś statek koloidowy. Ale przetrwali - i mają zamiar to wszystko rozplątać.

Aktor w Opowieści:

* Dokonanie:
    * potężnym głosem kaznodziei przesunął miłość wobec Eustachego wszystkich na Inferni w stronę Arianny - gdzie owej miłości miejsce.


### Arystokratka w ładowni na świnie

* **uid:** 200909-arystokratka-w-ladowni-na-swinie, _numer względny_: 6
* **daty:** 0111-02-12 - 0111-02-17
* **obecni:** Anastazja Sowińska, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, OO Galaktyczny Tucznik

Streszczenie:

Zespół wyciągnął Martyna z więzienia (legalnie). Wpakowali się na Galaktycznego Tucznika by przenieść świnie na orbitę... ale poleciała z nimi dziennikarka Izabela (o czym wiedzą) i 15letnia Anastazja Sowińska (o czym nie wiedzą). Przy lądowaniu, atak nojrepów na Tucznika spowodował katastrofę - seria Paradoksów Anastazji, uszkodzenia Tucznika itp. Zespół uratował Anastazję (choć kosztem ran Martyna) i wylądował Tucznikiem na Astorii. To jest sukces.

Aktor w Opowieści:

* Dokonanie:
    * miał pomagać Klaudii; niestety, skończył nieprzytomny przez anomalię terroru Anastazji (w czym pomogły środki Martyna).


### Nienawiść do świń

* **uid:** 200826-nienawisc-do-swin, _numer względny_: 5
* **daty:** 0111-02-05 - 0111-02-11
* **obecni:** Aleksandra Termia, Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Sebastian Alarius, Tadeusz Ursus

Streszczenie:

Po pojedynku Kramer opieprzył Ariannę i dał jej dyskretną tajną misję - ma niby w niesławie wozić świnie, naprawdę ma pomóc Orbiterowi rozwiązać spisek w Trzecim Raju. Ale wysłanie Arianny na świnie wywołało bunt na Kontrolerze Pierwszym; Arianna musiała rozpaczliwie deeskalować, nawet budząc Leonę. Udało się bunt opanować, acz wysokim kosztem - pierwszy post-noktiański kapitan nie żyje, załoga Inferni częściowo zesłana będzie do Trzeciego Raju a Arianna miała bardzo żenujące wejście.

Aktor w Opowieści:

* Dokonanie:
    * podburzony przez Klaudię, by chronić Ariannę doprowadził do buntu na Orbiterze. Klaudia skutecznie ukryła ten fakt przed wszystkimi, by nikt nie wiedział że to on zaczął.


### Niemożliwe nieuczciwe ćwiczenia Bladawira

* **uid:** 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira, _numer względny_: 4
* **daty:** 0110-11-26 - 0110-11-30
* **obecni:** Antoni Bladawir, Arianna Verlen, Elena Verlen, Kamil Lyraczek, Klaudia Stryk, Lars Kidironus, Marta Keksik, OO Infernia, OO Paprykowiec, Patryk Samszar, Zaara Mieralit

Streszczenie:

Bladawir przedstawia Inferni Zaarę i informuje o ćwiczeniach, rozgrywając Zaarę przeciw Inferni. Klaudia odkryła że jednostki jakie Infernia ma eskortować mają _Pain Module_. Arianna prosząc przez Krew Elenę doprowadziła do neutralizacji tych modułów. Ćwiczenia okazują się prawie niemożliwe, ale Infernia poświęciła Paprykowiec, 'zniszczyła' jednostki w przewadze i uratowała załogę. Odwróciła ćwiczenia Bladawira przeciw niemu, przy okazji podnosząc swoją chwałę i pokazując że nie będzie grać w cudze gry.

Aktor w Opowieści:

* Dokonanie:
    * charyzmatyczny inspirator, po kiepskiej mowie Bladawira wygłosił własną do załogi. Robi show dla Arianny. Wysłany przez Ariannę na wydobycie informacji od Zaary i jej przekonanie o racjach Arianny wykonał zadanie - dowiedział się czego był w stanie i pokazał jej potencjalnie lepszy świat.


### O psach i kryształach

* **uid:** 200408-o-psach-i-krysztalach, _numer względny_: 3
* **daty:** 0110-09-29 - 0110-10-04
* **obecni:** Arianna Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Sebastian Namczek

Streszczenie:

Infernia - QShip - została wysłana na rutynową misję zapobieżenia zamieszkom na stacji Samojed. Na miejscu okazało się, że jest zainfekowany anomalnymi kryształami krążownik Leotis. Arianna Verlen ciężko uszkodziła stację Samojed i prawie zniszczyła Infernię, ale dała radę zniszczyć Leotis poświęcając więcej niż ktokolwiek mógł chcieć.

Aktor w Opowieści:

* Dokonanie:
    * nie pamiętał o członkach załogi dotkniętych anomalią krystaliczną (co było cenną podpowiedzią dla Arianny i Klaudii). Dzięki temu Klaudia i Martyn poznali prawdę.


### Infernia i Martyn Hiwasser

* **uid:** 200106-infernia-martyn-hiwasser, _numer względny_: 2
* **daty:** 0110-06-28 - 0110-07-03
* **obecni:** Arianna Verlen, Dominik Ogryz, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, Szczepan Olgrod, Wioletta Keiril

Streszczenie:

Arianna Verlen uratowała Martyna Hiwassera przed nędznym losem, ratując go od problemów z komodorem Ogryzem na Kontrolerze Pierwszym. Niestety, podpadła dużej ilości oficerów Orbitera. Ale za to przywróciła Martyna do załogi Inferni.

Aktor w Opowieści:

* Dokonanie:
    * ludzki kaznodzieja Arianny, nadal z nią. Dobry do przesłuchiwania i ludzkich aspektów załogi - gadał, przekonywał i dopytywał by pomóc znaleźć Wiolettę a potem Martyna.


### Nocna Krypta i Bohaterka

* **uid:** 191025-nocna-krypta-i-bohaterka, _numer względny_: 1
* **daty:** 0108-05-06 - 0108-05-08
* **obecni:** AK Nocna Krypta, Arianna Verlen, Kamil Lyraczek, Wojciech Kuszar

Streszczenie:

Arianna Verlen - bohaterka Orbitera. Zdecydowała się uratować osoby złapane przez Nocną Kryptę i wydobyć je, oraz zdobyć całość wiedzy noktiańskiej. Prawie zginęła; dzięki swojej załodze udało jej się wyjść ze stanu arcymaga, nie została opętana przez kapitan Oliwię z Krypty oraz udało im się wszystkim ujść z życiem. Kosztem przekształcenia Nocnej Krypty w niebezpieczny statek anomalny...

Aktor w Opowieści:

* Dokonanie:
    * kaznodzieja ludzki, wierzący w Ariannę jak w boginię. Przez to przesuwa ją do poziomu Arcymaga, ale też ryzykuje życiem załogi oraz samej Arianny. Oficialnie, medyk.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 14, @: 0112-05-09
    1. Primus    : 14, @: 0112-05-09
        1. Sektor Astoriański    : 13, @: 0112-05-09
            1. Astoria, Orbita    : 5, @: 0112-02-01
                1. Kontroler Pierwszy    : 5, @: 0112-02-01
                    1. Hangary Alicantis    : 1, @: 0112-02-01
            1. Astoria, Pierścień Zewnętrzny    : 3, @: 0112-05-09
                1. Poligon Stoczni Neotik    : 1, @: 0112-05-09
                1. Stocznia Neotik    : 2, @: 0112-05-09
                    1. Zewnętrzny dok ixioński    : 1, @: 0112-04-29
            1. Astoria    : 1, @: 0111-02-17
                1. Sojusz Letejski, NW    : 1, @: 0111-02-17
                    1. Ruiniec    : 1, @: 0111-02-17
                        1. Trzeci Raj, okolice    : 1, @: 0111-02-17
                            1. Kopiec Nojrepów    : 1, @: 0111-02-17
            1. Kosmiczna Pustka    : 1, @: 0108-05-08
            1. Pas Omszawera    : 1, @: 0110-10-04
                1. Kolonia Samojed    : 1, @: 0110-10-04
                    1. Stacja Astropociągów    : 1, @: 0110-10-04
                    1. Zona Czarna    : 1, @: 0110-10-04
                    1. Zona Mieszkalna    : 1, @: 0110-10-04
            1. Pas Teliriański    : 1, @: 0111-11-12
                1. Planetoidy Kazimierza    : 1, @: 0111-11-12
                    1. Planetoida Asimear    : 1, @: 0111-11-12
                        1. Stacja Lazarin    : 1, @: 0111-11-12
        1. Sektor Mevilig    : 1, @: 0112-03-26
            1. Planetoida Kalarfam    : 1, @: 0112-03-26

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 14 | ((191025-nocna-krypta-i-bohaterka; 200106-infernia-martyn-hiwasser; 200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211020-kurczakownia; 211215-sklejanie-inferni-do-kupy; 220105-to-nie-pulapka-na-nereide; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Klaudia Stryk        | 12 | ((200106-infernia-martyn-hiwasser; 200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211020-kurczakownia; 220105-to-nie-pulapka-na-nereide; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Eustachy Korkoran    | 11 | ((200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211020-kurczakownia; 211215-sklejanie-inferni-do-kupy; 220105-to-nie-pulapka-na-nereide)) |
| Martyn Hiwasser      | 9 | ((200106-infernia-martyn-hiwasser; 200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 211020-kurczakownia)) |
| Elena Verlen         | 8 | ((200909-arystokratka-w-ladowni-na-swinie; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211020-kurczakownia; 211215-sklejanie-inferni-do-kupy; 220105-to-nie-pulapka-na-nereide; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Leona Astrienko      | 5 | ((200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211215-sklejanie-inferni-do-kupy)) |
| Anastazja Sowińska   | 3 | ((200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec)) |
| Izabela Zarantel     | 3 | ((200909-arystokratka-w-ladowni-na-swinie; 210616-nieudana-infiltracja-inferni; 220105-to-nie-pulapka-na-nereide)) |
| OO Infernia          | 3 | ((211020-kurczakownia; 211215-sklejanie-inferni-do-kupy; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| AK Nocna Krypta      | 2 | ((191025-nocna-krypta-i-bohaterka; 201216-krypta-i-wyjec)) |
| Antoni Kramer        | 2 | ((200826-nienawisc-do-swin; 210526-morderstwo-na-inferni)) |
| Diana Arłacz         | 2 | ((201118-anastazja-bohaterka; 201216-krypta-i-wyjec)) |
| Flawia Blakenbauer   | 2 | ((210616-nieudana-infiltracja-inferni; 211020-kurczakownia)) |
| Maria Naavas         | 2 | ((211215-sklejanie-inferni-do-kupy; 220105-to-nie-pulapka-na-nereide)) |
| Marian Tosen         | 2 | ((210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni)) |
| Otto Azgorn          | 2 | ((210526-morderstwo-na-inferni; 211020-kurczakownia)) |
| Roland Sowiński      | 2 | ((210616-nieudana-infiltracja-inferni; 211215-sklejanie-inferni-do-kupy)) |
| Adam Szarjan         | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| AK Wyjec             | 1 | ((201216-krypta-i-wyjec)) |
| Aleksandra Termia    | 1 | ((200826-nienawisc-do-swin)) |
| Antoni Bladawir      | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Diana d'Infernia     | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Dominik Ogryz        | 1 | ((200106-infernia-martyn-hiwasser)) |
| Feliks Walrond       | 1 | ((210526-morderstwo-na-inferni)) |
| Karol Reichard       | 1 | ((210526-morderstwo-na-inferni)) |
| Kasandra Destrukcja Diakon | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Lars Kidironus       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Llarnagraht          | 1 | ((210414-dekralotyzacja-asimear)) |
| Lutus Amerin         | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Malictrix d'Pandora  | 1 | ((210414-dekralotyzacja-asimear)) |
| Mariusz Tubalon      | 1 | ((210414-dekralotyzacja-asimear)) |
| Marta Keksik         | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Martyna Bianistek    | 1 | ((210414-dekralotyzacja-asimear)) |
| Natalia Aradin       | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| OO Galaktyczny Tucznik | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| OO Opresor           | 1 | ((210616-nieudana-infiltracja-inferni)) |
| OO Paprykowiec       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| OO Tivr              | 1 | ((210526-morderstwo-na-inferni)) |
| Patryk Samszar       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Saitaer              | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| SCA Płetwal Błękitny | 1 | ((210414-dekralotyzacja-asimear)) |
| Sebastian Alarius    | 1 | ((200826-nienawisc-do-swin)) |
| Sebastian Namczek    | 1 | ((200408-o-psach-i-krysztalach)) |
| Stefan Jamniczek     | 1 | ((201118-anastazja-bohaterka)) |
| Szczepan Olgrod      | 1 | ((200106-infernia-martyn-hiwasser)) |
| Tadeusz Ursus        | 1 | ((200826-nienawisc-do-swin)) |
| Tal Marczak          | 1 | ((210526-morderstwo-na-inferni)) |
| Tomasz Sowiński      | 1 | ((210414-dekralotyzacja-asimear)) |
| Vigilus Mevilig      | 1 | ((211020-kurczakownia)) |
| Wioletta Keiril      | 1 | ((200106-infernia-martyn-hiwasser)) |
| Wojciech Kuszar      | 1 | ((191025-nocna-krypta-i-bohaterka)) |
| Zaara Mieralit       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |