# Tymon Grubosz
## Identyfikator

Id: 1909-tymon-grubosz

## Sekcja Opowieści

### Przygoda, randka i porwanie

* **uid:** 201020-przygoda-randka-i-porwanie, _numer względny_: 18
* **daty:** 0110-10-25 - 0110-10-27
* **obecni:** Daniel Sowiński, Gabriel Ursus, Henryk Sowiński, Kacper Bankierz, Laurencjusz Sorbian, Liliana Bankierz, Robert Pakiszon, Robinson Porzecznik, Tomasz Tukan, Triana Porzecznik, Tymon Grubosz, Urszula Miłkowicz

Streszczenie:

Młodzi i niekompetentni Sowińscy łażą po Zaczęstwie i szukają przygód. Gdy ich dyskretny opiekun został aresztowany, szybko ściągnął Roberta i Gabriela do pomocy - by zrobili przygodę. Jednocześnie Triana chciała podwójnej randki i też robiła przygodę z Lilianą. Skończyło się na tym, że wszystkie przygody się pomieszały, Triana została porwana przez siły Zespołu (!?) i walnęła efemeryda na budowanym biurowcu Gorland. Ale - 'stream must go on'.

Aktor w Opowieści:

* Dokonanie:
    * aresztował Sorbiana - dygnitarza i ochroniarza Aurum. Zniszczył prototypowego, niebezpiecznego konstruminusa Triany który wymknął się spod kontroli. Złapał Gabriela i Ulę przy efemerydzie. Ogólnie, żmudny, głupi i pracowity czas.


### Tajna baza Orbitera?

* **uid:** 200510-tajna-baza-orbitera, _numer względny_: 17
* **daty:** 0110-09-07 - 0110-09-11
* **obecni:** Alina Anakonda, Artur Kołczond, Gabriel Ursus, Ignacy Myrczek, Kallista Exolon, Laura Tesinik, Mariusz Trzewń, Natalia Tessalon, Pięknotka Diakon, Sabina Kazitan, Talarand d'Irrydius, Teresa Mieralit, Tymon Grubosz

Streszczenie:

Natalia Tessalon chce przetransportować rannego brata do Aurum, ale manewr zablokowała Laura (terminuska-prawniczka) na prośbę Pięknotki. Natalia i Sabina się ścięły, przez co ucierpiał Myrczek a Sabina trafiła do aresztu. Teresa Mieralit zapewni, że Myrczek nie ma głowy do Sabiny. Pięknotka z Tymonem poszukali kto ciężko zmiażdżył Gabriela; udało się im znaleźć tajną bazę Orbitera w której jest Emulatorka, która się do tego przyznała.

Aktor w Opowieści:

* Dokonanie:
    * tym razem jego groźne oblicze, kataliza i technomancja się przydały - wykrył, że czujniki na autofarmie Kołczonda mają NIE znaleźć bazy w Studni Irrydiańskiej.


### Rozbrojenie bomby w Kalbarku

* **uid:** 200222-rozbrojenie-bomby-w-kalbarku, _numer względny_: 16
* **daty:** 0110-07-27 - 0110-08-01
* **obecni:** Aleksandra Szklarska, Ataienne, Bartłomiej Małczarek, Diana Tevalier, Lucjusz Blakenbauer, Mateusz Kardamacz, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Kalbark okazał się jedną wielką bombą. Liberatis, silnie uzbrojeni. Kardamacz, kontroluje całe miasto. Terminusi, skażeni kralotycznie. I Chevaleresse na którą wszystko spadnie jeśli przyjdzie wsparcie. Pięknotka rozmontowała tą bombę, ściągnęła Ataienne (oficjalnie) i Zespół dał radę rozproszyć Liberatis, wycofać Chevaleresse i odzyskać kontrolę nad Barbakanem. Pięknotka zebrała wsparcie i na polu bitwy został na serio tylko Kardamacz.

Aktor w Opowieści:

* Dokonanie:
    * znalazł zastępstwo na Zaczęstwo i pojedzie pomóc Pięknotce i Ataienne rozwiązać problemy w Kalbarku. Oczywiście, dyskretnie i na urlopie.


### Liliana w świecie dokumentów

* **uid:** 190820-liliana-w-swiecie-dokumentow, _numer względny_: 15
* **daty:** 0110-07-04 - 0110-07-08
* **obecni:** Adela Pieczar, Arnulf Poważny, Liliana Bankierz, Szymon Jaszczurzec, Tadeusz Kruszawiecki, Tymon Grubosz

Streszczenie:

Tiamenat wyprodukował eksperymentalnego, pomocnego mimika. Chcieli przesłać go do Trzeciego Raju gdzie by się przydał, ale ingerencja Liliany sprawiła, że mimik zniknął. A Tymon próbował rozpaczliwie niczego nie zauważyć, lecz dzięki działaniu Zespołu (magów ze Szkoły Magii) musiał rozwiązać problem. Ech, te dzieci. Ale Liliana odzyskała dobre imię.

Aktor w Opowieści:

* Dokonanie:
    * terminus, który BARDZO próbował nie zauważyć noktiańskiej firmy Kajrata pomagającej Trzeciemu Rajowi. Niestety, uczniowie szkoły magów nie dali mu tej szansy.


### Zaginiona soniczka

* **uid:** 191105-zaginiona-soniczka, _numer względny_: 14
* **daty:** 0110-07-02 - 0110-07-03
* **obecni:** Ataienne, Erwin Galilien, Mariola Tralment, Mariusz Trzewń, Mateusz Kardamacz, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Ataienne poprosiła Pięknotkę o znalezienie zaginionej fanki. Pięknotka zaczęła śledztwo i doszła do tego, że Mariola została porwana przez gang przerzucający dziewczyny do Cieniaszczytu. Erwin słyszał takie plotki. Pięknotka zlokalizowała i usunęła komórkę tego gangu; śledztwo pokazało na autoklub fitness jako jedno interesujące miejsce. Najpewniej nie to jest miejscem "zła", ale warto sprawdzić.

Aktor w Opowieści:

* Dokonanie:
    * dewastująca maszyna zniszczenia, który przez Efekt Skażenia wygląda jak słodka Mariolka. Nieszczęśliwy, ale poszedł szturmować.


### Esuriit w sercu Alicji

* **uid:** 190619-esuriit-w-sercu-alicji, _numer względny_: 13
* **daty:** 0110-04-16 - 0110-04-18
* **obecni:** Alicja Kiermacz, Eliza Farnorz, Rafał Muczor, Tymon Grubosz

Streszczenie:

Alicja Kiermacz została wyleczona, ale Esuriit do niej wróciło. Prawie skrzywdziła swoją eks-przyjaciółkę (Elizę) i prawie porwał ją Kajrat. Zaczęła gasnąć i tracić energię. W końcu jednak została przekonana wbrew sobie do pojechania do Pustogoru - tam mogą jej pomóc. Oby.

Aktor w Opowieści:

* Dokonanie:
    * terminus, który zatroszczył się o los Alicji Kiermacz. Wywiózł ją do Pustogoru po upewnieniu się, że w Alicji jakoś zagnieździło się Esuriit.


### Chevaleresse

* **uid:** 190217-chevaleresse, _numer względny_: 12
* **daty:** 0110-02-25 - 0110-02-27
* **obecni:** Alan Bartozol, Diana Tevalier, Karolina Erenit, Marlena Maja Leszczyńska, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Diana, aka Chevaleresse przybyła do Cyberszkoły szukać Alana. Postraszyła Marlenę i skończyła zaatakowana przez Tymona. Alan się zaopiekował swoją członkinią gildii; okazało się, że jego gildia (Elisquid) wpadła w złe ręce. Teraz Alan rozpaczliwie szuka sposobu na to by się odbanować; tymczasowo Chevaleresse zostaje u Alana... KTÓRY STAŁ SIĘ JEJ PRAWNYM OPIEKUNEM. Aha, Alan wie, że to wina Marleny że go zabanowali (a tak naprawdę jest Pięknotki).

Aktor w Opowieści:

* Dokonanie:
    * wezwał Pięknotkę na wszelki wypadek - i była potrzebna. Sklupał Dianę i Karolinę. Nie umiał deeskalować z dziewczynami i wolał się wycofać i oddać jej problem.


### Nie da się odrzucić mocy

* **uid:** 190206-nie-da-sie-odrzucic-mocy, _numer względny_: 11
* **daty:** 0110-02-17 - 0110-02-20
* **obecni:** Arnulf Poważny, Karolina Erenit, Kasjopea Maus, Kornel Garn, Marlena Maja Leszczyńska, Minerwa Metalia, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Karolina źle reaguje na to, że stała się czarodziejką i chciała odrzucić moc. Jednak nie udało jej się, jedynie zdewastowała Cyberszkołę i zagroziła ludziom. Karolina sprowadziła Ixion do Cyberszkoły. Zespół ją powstrzymał i Pięknotka zaczęła reintegrować życie Karoliny, kierując ją do Marleny Mai i do AMZ.

Aktor w Opowieści:

* Dokonanie:
    * wsparcie Pięknotki w przebijaniu się przez Cyberszkołę skażoną ixiońsko, chroni uderzenia padające w Minerwę i Pięknotkę.


### Migświatło psychotroniczek

* **uid:** 190828-migswiatlo-psychotroniczek, _numer względny_: 10
* **daty:** 0110-02-08 - 0110-02-10
* **obecni:** Artur Michasiewicz, Ernest Kajrat, Marek Puszczok, Minerwa Metalia, Pięknotka Diakon, Talia Aegis, Tymon Grubosz

Streszczenie:

Gdy Kajrat jest w więzieniu, nikt nie kontroluje przepływu technologii noktiańskiej. Talia ostrzegła Pięknotkę, że do holo-walki AI dodała subturingowe BIA klasy Szponowiec, z żądania gościa od Grzymościa. Okazało się, że to niegroźny hazard jest - ale są tam dwie dziwne walczące AI: BIA oraz militarna, stworzona przez Minerwę. Pięknotka chciała tylko zabić BIA, ale przypadkowo z Minerwą wysadziły Arenę Migświatła. Tymon zabrał Minerwie jej TAI. Powiedział, że jeszcze nie czas by takie rzeczy robiła.

Aktor w Opowieści:

* Dokonanie:
    * terminus - technomanta i katalista kontrolujący Zaczęstwo. Tym razem po prostu uznał, że Minerwa nie może budować TAI na rynek - za duże ryzyko ixionu.


### Czarodziejka z woli Saitaera

* **uid:** 190202-czarodziejka-z-woli-saitaera, _numer względny_: 9
* **daty:** 0110-02-01 - 0110-02-05
* **obecni:** Adela Kirys, Karolina Erenit, Minerwa Metalia, Pięknotka Diakon, Saitaer, Sławomir Muczarek, Tymon Grubosz, Wojtek Kurczynos

Streszczenie:

Wojtek Kurczynos ZNOWU dobierał się do Karoliny Erenit. Przez machinacje Sławka i Adeli Saitaer dokonał transfuzji mocy magicznej od Wojtka do Karoliny. Pięknotka drastycznie ukrywała wszystkie wpływy wszystkich osób, by ucierpiał tylko winny Wojtek. Nawet współpracowała z Saitaerem, by terroformizować Wojtka. Wszystko, by Karolina zachowała moc magiczną. ALE - doprowadziła do tego, że spojrzeli na Karolinę Mausowie. W ten sposób Karradrael uniemożliwił hold Saitaera na Karolinie.

Aktor w Opowieści:

* Dokonanie:
    * terminus który prawie wpadł w katastrofalne kłopoty, bo ukrył działanie Sławka i Adeli wobec Wojtka. Potem osłaniał Minerwę i Pięknotkę. O dziwo, przeszło.


### Ixioński Transorganik

* **uid:** 190127-ixionski-transorganik, _numer względny_: 8
* **daty:** 0110-01-29 - 0110-01-30
* **obecni:** Erwin Galilien, Karolina Erenit, Kirył Najłalmin, Minerwa Metalia, Napoleon Bankierz, Pięknotka Diakon, Saitaer, Tymon Grubosz, Wiktor Satarail, Wojtek Kurczynos

Streszczenie:

Minerwa próbowała pomóc Karolinie Erenit, ale niestety użyła artefaktu ixiońskiego i przekształciła wrednego Wojtka Kurczynosa w transorganika. Pięknotka z pomocą Epirjona przeteleportowała siebie i Wojtka na Trzęsawisko i tam z pomocą Wiktora Sataraila odwrócili tą transorganizację. Ten konkretny problem udało się dyskretnie rozwiązać.

Aktor w Opowieści:

* Dokonanie:
    * lubi Pięknotkę i nie chce jej robić problemów; puścił płazem ewentualne problemy z Minerwą i Erwinem.


### Nowa Minerwa w nowym świecie

* **uid:** 190120-nowa-minerwa-w-nowym-swiecie, _numer względny_: 7
* **daty:** 0110-01-22 - 0110-01-26
* **obecni:** Erwin Galilien, Karla Mrozik, Kasjopea Maus, Minerwa Metalia, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Minerwa wróciła. Jako czarodziejka, opiekować się ma nią Pięknotka. Minerwa nie do końca radzi sobie w nowej rzeczywistości - nie jest piękną Diakonką, ma odłamki pamięci Saitaera i aby powrócić, poświęciła życie człowieka. Zdecydowała się tymczasowo przenieść do Zaczęstwa (Karla wysłała tam wsparcie). Tymczasem Kornel Garn już planuje by ją przekonać by doń dołączyła.

Aktor w Opowieści:

* Dokonanie:
    * miał nadzieję na wsparcie i dzięki Pięknotce do Zaczęstwa dostał wsparcie oraz Minerwę. Akceptuje pomoc Minerwie; da jej szansę.


### Rozpaczliwe ratowanie BII

* **uid:** 190827-rozpaczliwe-ratowanie-bii, _numer względny_: 6
* **daty:** 0110-01-18 - 0110-01-21
* **obecni:** BIA Tarn, Ernest Kajrat, Marek Puszczok, Mariusz Trzewń, Pięknotka Diakon, Sławomir Niejadek, Talia Aegis, Tymon Grubosz

Streszczenie:

Talia próbowała utrzymać przy życiu BIA 3 generacji. Nie mając surowców, użyła tej BIA do zdobycia rzeczy odżywczych z Tiamenat - zrzucając winę na Wiktora Sataraila. Pięknotka poszła za śladem i gdy dotarła do Talii, Kajrat wziął na siebie winę (za co Pięknotka go aresztowała). Następnie poprosiła Wiktora Sataraila by ten ochronił swoje dobre imię - i zniszczył śmiertelnie niebezpieczną BIA.

Aktor w Opowieści:

* Dokonanie:
    * pomaga Pięknotce rozwiązać problem "Wiktora" (czyli BIA) w Zaczęstwie. Osłania Pięknotkę gdy idą do Talii Aegis.


### Neutralizacja artylerii koszmarów

* **uid:** 181114-neutralizacja-artylerii-koszmarow, _numer względny_: 5
* **daty:** 0109-10-20 - 0109-10-21
* **obecni:** Felicja Melitniek, Pięknotka Diakon, Tymon Grubosz, Wiktor Satarail

Streszczenie:

Wiktor Satarail uderzył z Trzęsawiska. Zdecydował się zrobić Artylerię Koszmarów bazując na strachu ludzi z Mekki Wolności w Zaczęstwie by zeń zbudować artylerię na Trzęstawisku. Pięknotka starła się z nim dwa razy i dała mu odejść spokojnie, by tylko wejść na Trzęsawisko, uwieść Wiktora i zneutralizować Pryzmat artylerii. Wiktor docenił jej plan; pozwolił jej odejść i obiecał, że jeszcze poczeka z eliminacją magów Zaczęstwa.

Aktor w Opowieści:

* Dokonanie:
    * wsparł Pięknotkę w przejęciu kontrolę nad Nieużytkami Staszka atakowanymi przez Pnączoszpona (który tylko ich straszył)


### Wojna o uczciwe półfinały

* **uid:** 181101-wojna-o-uczciwe-polfinaly, _numer względny_: 4
* **daty:** 0109-10-17 - 0109-10-19
* **obecni:** Alan Bartozol, Damian Podpalnik, Karolina Erenit, Marlena Maja Leszczyńska, Pięknotka Diakon, Tadeusz Kruszawiecki, Tymon Grubosz

Streszczenie:

Marlena przybyła do Zaczęstwa sprawdzić, czy Zaczęstwiacy oszukują w SupMis. Okazało się, że to ludzie - nie wiedzą o magii i tak, magia im pomaga. Marlena zaczęła próbować im pomóc i wyplątać ich z magii zanim yyizdis zabanuje tą drużynę (konkurencję Marleny). Spowodowała Efekt Skażenia i musiała ją uratować Pięknotka, która zaczęła się zastanawiać jak wplątała się w tą sprawę. Koniec końców się udało - ale Pięknotka dostała w odpowiedzialność zarządzanie Zaczęstwem jako terminuska...

Aktor w Opowieści:

* Dokonanie:
    * przerażający terminus-katalista o gołębim sercu. Dobry przyjaciel Pięknotki. Miał niefortunnie piękny efekt Skażenia rozrywając Karolinę przy studni.


### Księżniczka Arianna ratuje dzieci w lesie

* **uid:** 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie, _numer względny_: 3
* **daty:** 0085-01-26 - 0085-01-28
* **obecni:** Arnulf Poważny, Kaella Sarimanis, Klaudia Stryk, Mariusz Trzewń, Teresa Mieralit, Tymon Grubosz

Streszczenie:

Korzystając z odbudowy po wojnie, napływowa grupa gangsterów polowała na dzieci. Gdy złapali Kaellę, młodą protomag Noctis, ta stworzyła iluzję Księżniczki Arianny z bajek, aby im pomogła. Ta - manifestacja Alucis i Esuriit - porwała dzieci od porywaczy i je schowała. Tymczasem Teresa - noktianka z AMZ - dostała sygnał od 'Księżniczki' i zaczęła szukać kierowana głosami swoich zmarłych rodziców.

Klaudia i Mariusz doszli do tego co się stało i po tym jak osłonili Teresę poszli do dyrektora Arnulfa. Opracowali plan ratunkowy po odkryciu co się stało. Arnulf z Tymonem skupili się na gangsterach, Klaudia i Mariusz poszli po dzieci. Plan zakładał odwrócenie uwagi Drapieżników poprzez stworzenie ataku potworów.

Niestety, Teresa ma flashbacki z czasów wojny a Alucis pokazało jej rodziców. Po wyłączeniu iluzji stworzonej przez Księżniczkę Ariannę, wszyscy musieli uciekać przed manifestacją Esuriit, która zagrażała wszystkich. Udało się im bezpiecznie dotrzeć do terenu AMZ, gdzie dzieci otrzymały niezbędną pomoc medyczną.

Aktor w Opowieści:

* Dokonanie:
    * Morderczy terminus, ze wsparciem dyrektora i pod osłoną atakujących potworów i braku detekcji zdewastował obóz porywaczy dzieci. Dyskretna operacja dla dyskretnego problemu.
* Progresja:
    * zbudował silniejszy link z Arnulfem Poważnym; wspólnie uczestniczyli w operacji niszczenia porywaczy dzieci.


### Ukryta wychowanka Arnulfa

* **uid:** 211010-ukryta-wychowanka-arnulfa, _numer względny_: 2
* **daty:** 0084-12-11 - 0084-12-12
* **obecni:** Arnulf Poważny, Felicjan Szarak, Klaudia Stryk, Ksenia Kirallen, Sasza Morwowiec, Strażniczka Alair, Talia Aegis, Teresa Mieralit, Tymon Grubosz

Streszczenie:

Dyrektor Arnulf Poważny ma wychowankę - Teresę Mieralit, noktiańską piętnastonastolatkę która mieszka w AMZ. Strażniczka Alair ją wykryła i wpadła w pętlę - komponenty TAI uznały ją za potencjalne zagrożenie, BIA za osobę do ochrony. Strażniczka wycofała drony do osłony AI Core i zaatakowały Teresę. Klaudia zorientowała się w problemie, obudziła hipernetem Arnulfa. Arnulf osłonił Teresę, Klaudia wezwała Talię i w grupie udało im się opanować niesforne TAI (bo Klaudia zanomalizowała konwertery energii Strażniczki, która czerpie energię z Trzęsawiska).

Aktor w Opowieści:

* Dokonanie:
    * terminus; przybył pomóc Strażniczce w nocy do AMZ i pomógł Arnulfowi w obronie przed atakami dron aż Talia i Klaudia nie wyłączyły TAI. Potem skonfrontował się z nim Sasza, ale Tymon to olał.
* Progresja:
    * ma wroga w Saszy Morwowcu (terminus). Uważa go za konspiratora próbującego działać na szkodę Pustogoru. Z jakiegoś powodu.


### Nowa Strażniczka AMZ

* **uid:** 210926-nowa-strazniczka-amz, _numer względny_: 1
* **daty:** 0084-06-14 - 0084-06-26
* **obecni:** Albert Kalandryk, Arnulf Poważny, Klaudia Stryk, Ksenia Kirallen, Mariusz Trzewń, Strażniczka Alair, Talia Aegis, Tymon Grubosz

Streszczenie:

Klaudia - młoda grzeczna administratorka AMZ - przypadkiem odkrywa konspirację dyrektora, terminusa i noktianki by uratować umierającą Eszarę. Po konfrontacji z dyrektorem pomaga im by wszystko działało. Trzewń uniknął amnestyków.

Aktor w Opowieści:

* Dokonanie:
    * 28 lat; "emerytowany" neuroszturmowiec Orbitera. "Gość" terminusów. Współpracuje z Arnulfem i Talią nad zrobieniem Strażniczki Alair. Przekonuje młodziutkiego Trzewnia, żeby nie zgłaszał tego Pustogorowi (chce mu oszczędzić amnestyków).


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 18, @: 0110-10-27
    1. Primus    : 18, @: 0110-10-27
        1. Sektor Astoriański    : 18, @: 0110-10-27
            1. Astoria    : 18, @: 0110-10-27
                1. Sojusz Letejski    : 18, @: 0110-10-27
                    1. Szczeliniec    : 18, @: 0110-10-27
                        1. Powiat Jastrzębski    : 2, @: 0110-08-01
                            1. Kalbark    : 2, @: 0110-08-01
                                1. Autoklub Piękna    : 2, @: 0110-08-01
                                1. Mini Barbakan    : 1, @: 0110-08-01
                        1. Powiat Pustogorski    : 17, @: 0110-10-27
                            1. Czemerta, okolice    : 1, @: 0110-09-11
                                1. Baza Irrydius    : 1, @: 0110-09-11
                                1. Fortifarma Irrydia    : 1, @: 0110-09-11
                                1. Studnia Irrydiańska    : 1, @: 0110-09-11
                            1. Czółenko    : 1, @: 0110-07-03
                                1. Bunkry    : 1, @: 0110-07-03
                            1. Pustogor    : 7, @: 0110-09-11
                                1. Barbakan    : 3, @: 0110-01-26
                                1. Eksterior    : 2, @: 0110-09-11
                                    1. Miasteczko    : 2, @: 0110-09-11
                                        1. Knajpa Górska Szalupa    : 1, @: 0110-09-11
                                1. Gabinet Pięknotki    : 1, @: 0110-01-26
                                1. Kawiarenka Ciemna Strona    : 1, @: 0110-01-26
                                1. Miasteczko    : 1, @: 0110-02-27
                                1. Rdzeń    : 1, @: 0110-09-11
                                    1. Szpital Terminuski    : 1, @: 0110-09-11
                                1. Zamek Weteranów    : 1, @: 0084-06-26
                            1. Zaczęstwo    : 15, @: 0110-10-27
                                1. Akademia Magii, kampus    : 7, @: 0110-07-08
                                    1. Akademik    : 2, @: 0084-12-12
                                    1. Budynek Centralny    : 3, @: 0110-07-08
                                        1. Skrzydło Loris    : 1, @: 0110-07-08
                                    1. Domek dyrektora    : 1, @: 0110-02-20
                                    1. Złomiarium    : 1, @: 0084-06-26
                                1. Arena Migświatła    : 2, @: 0110-02-10
                                1. Bazar Różności    : 1, @: 0110-10-27
                                1. Biurowce    : 1, @: 0110-07-08
                                    1. Papierówka    : 1, @: 0110-07-08
                                1. Biurowiec Gorland    : 1, @: 0110-10-27
                                1. Cyberszkoła    : 6, @: 0110-07-08
                                1. Dzielnica Kwiecista    : 1, @: 0110-10-27
                                    1. Rezydencja Porzeczników    : 1, @: 0110-10-27
                                        1. Podziemne Laboratorium    : 1, @: 0110-10-27
                                1. Kawiarenka Leopold    : 1, @: 0110-10-27
                                1. Klub Poetycki Sucharek    : 1, @: 0110-07-08
                                1. Kompleks Tiamenat    : 1, @: 0110-01-21
                                1. Kwatera Terminusa    : 1, @: 0110-02-20
                                1. Las Trzęsawny    : 1, @: 0085-01-28
                                1. Mekka Wolności    : 2, @: 0110-04-18
                                1. Nieużytki Staszka    : 5, @: 0110-10-27
                                1. Osiedle Ptasie    : 4, @: 0110-07-03
                        1. Trzęsawisko Zjawosztup    : 2, @: 0110-01-30
                            1. Głodna Ziemia    : 1, @: 0110-01-30
                            1. Laboratorium W Drzewie    : 2, @: 0110-01-30

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 12 | ((181101-wojna-o-uczciwe-polfinaly; 181114-neutralizacja-artylerii-koszmarow; 190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy; 190217-chevaleresse; 190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek; 191105-zaginiona-soniczka; 200222-rozbrojenie-bomby-w-kalbarku; 200510-tajna-baza-orbitera)) |
| Arnulf Poważny       | 5 | ((190206-nie-da-sie-odrzucic-mocy; 190820-liliana-w-swiecie-dokumentow; 210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Karolina Erenit      | 5 | ((181101-wojna-o-uczciwe-polfinaly; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy; 190217-chevaleresse)) |
| Mariusz Trzewń       | 5 | ((190827-rozpaczliwe-ratowanie-bii; 191105-zaginiona-soniczka; 200510-tajna-baza-orbitera; 210926-nowa-strazniczka-amz; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Minerwa Metalia      | 5 | ((190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy; 190828-migswiatlo-psychotroniczek)) |
| Talia Aegis          | 4 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek; 210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Erwin Galilien       | 3 | ((190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 191105-zaginiona-soniczka)) |
| Klaudia Stryk        | 3 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Marlena Maja Leszczyńska | 3 | ((181101-wojna-o-uczciwe-polfinaly; 190206-nie-da-sie-odrzucic-mocy; 190217-chevaleresse)) |
| Teresa Mieralit      | 3 | ((200510-tajna-baza-orbitera; 211010-ukryta-wychowanka-arnulfa; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Alan Bartozol        | 2 | ((181101-wojna-o-uczciwe-polfinaly; 190217-chevaleresse)) |
| Ataienne             | 2 | ((191105-zaginiona-soniczka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Diana Tevalier       | 2 | ((190217-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Ernest Kajrat        | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Gabriel Ursus        | 2 | ((200510-tajna-baza-orbitera; 201020-przygoda-randka-i-porwanie)) |
| Kasjopea Maus        | 2 | ((190120-nowa-minerwa-w-nowym-swiecie; 190206-nie-da-sie-odrzucic-mocy)) |
| Ksenia Kirallen      | 2 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Liliana Bankierz     | 2 | ((190820-liliana-w-swiecie-dokumentow; 201020-przygoda-randka-i-porwanie)) |
| Marek Puszczok       | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Mateusz Kardamacz    | 2 | ((191105-zaginiona-soniczka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Saitaer              | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Strażniczka Alair    | 2 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Tadeusz Kruszawiecki | 2 | ((181101-wojna-o-uczciwe-polfinaly; 190820-liliana-w-swiecie-dokumentow)) |
| Wiktor Satarail      | 2 | ((181114-neutralizacja-artylerii-koszmarow; 190127-ixionski-transorganik)) |
| Wojtek Kurczynos     | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Adela Kirys          | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
| Adela Pieczar        | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Albert Kalandryk     | 1 | ((210926-nowa-strazniczka-amz)) |
| Aleksandra Szklarska | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Alicja Kiermacz      | 1 | ((190619-esuriit-w-sercu-alicji)) |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Artur Kołczond       | 1 | ((200510-tajna-baza-orbitera)) |
| Artur Michasiewicz   | 1 | ((190828-migswiatlo-psychotroniczek)) |
| Bartłomiej Małczarek | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| BIA Tarn             | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Damian Podpalnik     | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
| Daniel Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Eliza Farnorz        | 1 | ((190619-esuriit-w-sercu-alicji)) |
| Felicja Melitniek    | 1 | ((181114-neutralizacja-artylerii-koszmarow)) |
| Felicjan Szarak      | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Henryk Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Ignacy Myrczek       | 1 | ((200510-tajna-baza-orbitera)) |
| Kacper Bankierz      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Kaella Sarimanis     | 1 | ((231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Karla Mrozik         | 1 | ((190120-nowa-minerwa-w-nowym-swiecie)) |
| Kirył Najłalmin      | 1 | ((190127-ixionski-transorganik)) |
| Kornel Garn          | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Laura Tesinik        | 1 | ((200510-tajna-baza-orbitera)) |
| Laurencjusz Sorbian  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Lucjusz Blakenbauer  | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Mariola Tralment     | 1 | ((191105-zaginiona-soniczka)) |
| Napoleon Bankierz    | 1 | ((190127-ixionski-transorganik)) |
| Natalia Tessalon     | 1 | ((200510-tajna-baza-orbitera)) |
| Rafał Muczor         | 1 | ((190619-esuriit-w-sercu-alicji)) |
| Robert Pakiszon      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Robinson Porzecznik  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Sabina Kazitan       | 1 | ((200510-tajna-baza-orbitera)) |
| Sasza Morwowiec      | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Sławomir Muczarek    | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
| Sławomir Niejadek    | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Szymon Jaszczurzec   | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Tomasz Tukan         | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Triana Porzecznik    | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Urszula Miłkowicz    | 1 | ((201020-przygoda-randka-i-porwanie)) |