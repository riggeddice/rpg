# Nikola Kirys
## Identyfikator

Id: 9999-nikola-kirys

## Sekcja Opowieści

### Upadek enklawy Floris

* **uid:** 190626-upadek-enklawy-floris, _numer względny_: 5
* **daty:** 0110-05-28 - 0110-05-31
* **obecni:** Ariela Sirmin, Ernest Kajrat, Hubert Kraborów, Jolanta Teresis, Konrad Czukajczek, Marcel Sowiński, Nikola Kirys, Roman Rymtusz, Szymon Maszczor, Wargun Ira

Streszczenie:

W odpowiedzi na atak na Pięknotkę, Pustogor wysłał oddział by dowiedzieć się co się dzieje w Enklawach, mający porwać Enklawę Floris. Floris jednak się rozdzieliło - część osób poszła się sama poddać, inni zdecydowali się dalej chować. Poszli do Wiecznej Maszyny i tam znaleźli tymczasową bazę przez posiadanie jednego Skażonego maga (którego Maszyna akceptuje). Jest tam też mag Kajrata zdolny do fabrykacji - źródło Pajęczaków. Połączyli siły i zamaskowali swoje ślady. Pustogor został z niczym, acz z ~10 osobami z Floris, więc... sukces?

Aktor w Opowieści:

* Dokonanie:
    * nadal konsekwentnie pomaga Enklawom; jej serce widocznie jest po tej stronie a nie po stronie cywilizacji i Pustogoru. Ostrzegła o ataku plus zapewniła ochronę Hubertowi.


### Wojna Kajrata

* **uid:** 190622-wojna-kajrata, _numer względny_: 4
* **daty:** 0110-05-18 - 0110-05-21
* **obecni:** Ernest Kajrat, Liliana Bankierz, Nikola Kirys, Olaf Zuchwały, Pięknotka Diakon, Serafina Ira

Streszczenie:

Kajrat zaeskalował; ściągnął Lilianę i powiedział Pięknotce, że Liliana zostanie jego podwładną jeśli Serafina nie osiągnie sukcesu. Pięknotka poznała historię Serafiny - kiedyś agentka Pustogoru, w wyniku burzy w Pacyfice straciła przyjaciół. Teraz jest cieniem dawnej osoby. Po drodze, w Wolnych Ptakach, Pięknotkę zaatakowała grupa wolnych magów; uruchomił się Cień i Pięknotka zrobiła masakrę. To sprawiło większe problemy i niedogadanie między Enklawami a Pustogorem

Aktor w Opowieści:

* Dokonanie:
    * powiedziała Pięknotce na czym polega historia Serafiny.


### Bardzo nieudane porwania

* **uid:** 190503-bardzo-nieudane-porwania, _numer względny_: 3
* **daty:** 0110-04-18 - 0110-04-20
* **obecni:** Damian Orion, Karla Mrozik, Minerwa Metalia, Mirela Orion, Nikola Kirys, Pięknotka Diakon

Streszczenie:

Kirasjerzy Orbitera dostali zadanie odbicia Minerwy i Nikoli, ale w mieszkaniu Minerwy czekała Pięknotka. Porwała z zaskoczenia Emulatorkę, uwolniła ją Cieniem i gdy Kirasjerzy uderzyli by po cichu odzyskać agentkę, Pięknotka poprosiła o pomoc Epirjon. W wyniku Pięknotce udało się ewakuować z Emulatorką, uwolniła ją Cieniem i wsadziła do Pustogoru. Potem z Karlą wynegocjowała ochronę dla Minerwy i Nikoli i jeszcze z tymi dwoma załatwiła by to było możliwe. Kirasjerzy zostali odepchnięci przez JEDNĄ terminuskę Pustogoru.

Aktor w Opowieści:

* Dokonanie:
    * polowali na nią Kirasjerzy; przez pół roku chroni Pustogor i Wolne Ptaki przed anomaliami i problemami, za co ma ochronę przed Orbiterem.
* Progresja:
    * jak długo współpracuje z Pustogorem chroniąc Wolne Ptaki i nie robiąc nic anty-Pustogorowi, chroniona przed Orbiterem. Uziemiona na tym terenie.


### Pierwszy Emulator Orbitera

* **uid:** 190502-pierwszy-emulator-orbitera, _numer względny_: 2
* **daty:** 0110-04-14 - 0110-04-16
* **obecni:** Alan Bartozol, Bożymir Szczupak, Minerwa Metalia, Nikola Kirys, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

Idąc śladami Elizy Pięknotka natrafiła na Nikolę - o której dawni przyjaciele (Olaf) mówią, że "zginęła", coś jej zrobiono. Pięknotka doszła do tego, że Nikola jest projektem Emulator; dowiedziała się, że Emulatory były budowane m.in. przez Minerwę. Pięknotka doszła do tego, że zintegrowana z Finis Vitae Nikola została częściowo uwolniona przez wpływ Arazille na Finis Vitae. Gdy spotkała się z Nikolą - szok. Cień zareagował. Nikola odzyskała wolność po kontakcie z Cieniem, ale Pięknotka go utrzymała. Cień ma w sobie "emocje" spętanych Emulatorów. Pięknotce udało się wyperswadować Nikoli budzenie autowara lub niszczenie Astorii; zwiadowczyni jednak odjechała w ogromnej konfuzji i nie wiedząc, co teraz robić.

Aktor w Opowieści:

* Dokonanie:
    * Emulatorka, która po dotknięciu Cienia odzyskała wolność. Kiedyś sprzężona z Finis Vitae, Minerwa zrobiła z niej Pierwszą Emulatorkę.
* Progresja:
    * Emulatorka; po dotknięciu Cienia odzyskała wolność od Orbitera Pierwszego (ku potencjalnej zgubie tej organizacji)


### Zrzut w Pacyfice

* **uid:** 190427-zrzut-w-pacyfice, _numer względny_: 1
* **daty:** 0110-04-07 - 0110-04-09
* **obecni:** Erwin Galilien, Minerwa Metalia, Nikola Kirys, Pięknotka Diakon

Streszczenie:

Do Pacyfiki ktoś z Cieniaszczytu zrzucił świetnej klasy ścigacz. Pięknotka i Erwin zinfiltrowali Pacyfikę by dostać namiary na ten pojazd. Okazało się, że pilotem ma być Nikola - też Nurek Szczeliny, świetnej klasy. Eks-Noctis. Pięknotka po raz pierwszy była w Pacyfice. Dodatkowo, dowiedziała się od Minerwy, że Cień niekoniecznie odpala się na żądanie, nie rozumie Pięknotki. Świetnie.

Aktor w Opowieści:

* Dokonanie:
    * była agentka Inwazji Noctis, świetny pilot i Nurek Szczeliny. Porusza się płynnie po Pacyfice i zdobyła swój ścigacz; wykryta przez Erwina i Pięknotkę


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0110-05-31
    1. Primus    : 5, @: 0110-05-31
        1. Sektor Astoriański    : 5, @: 0110-05-31
            1. Astoria    : 5, @: 0110-05-31
                1. Sojusz Letejski, SW    : 5, @: 0110-05-31
                    1. Granica Anomalii    : 5, @: 0110-05-31
                        1. Las Pusty, okolice    : 1, @: 0110-05-31
                        1. Pacyfika, obrzeża    : 2, @: 0110-04-20
                        1. Pacyfika    : 1, @: 0110-04-09
                        1. Wieczna Maszyna, okolice    : 1, @: 0110-05-31
                        1. Wolne Ptaki    : 2, @: 0110-05-21
                            1. Królewska Baza    : 2, @: 0110-05-21
                1. Sojusz Letejski    : 3, @: 0110-05-21
                    1. Szczeliniec    : 3, @: 0110-05-21
                        1. Powiat Pustogorski    : 3, @: 0110-05-21
                            1. Podwiert, obrzeża    : 1, @: 0110-04-20
                                1. Kosmoport    : 1, @: 0110-04-20
                            1. Pustogor    : 2, @: 0110-04-20
                                1. Interior    : 1, @: 0110-04-20
                                    1. Bunkry Barbakanu    : 1, @: 0110-04-20
                                1. Rdzeń    : 2, @: 0110-04-20
                                    1. Szpital Terminuski    : 2, @: 0110-04-20
                            1. Zaczęstwo, obrzeża    : 1, @: 0110-04-20
                            1. Zaczęstwo    : 2, @: 0110-05-21
                                1. Kawiarenka Leopold    : 1, @: 0110-05-21
                                1. Osiedle Ptasie    : 1, @: 0110-04-20

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 4 | ((190427-zrzut-w-pacyfice; 190502-pierwszy-emulator-orbitera; 190503-bardzo-nieudane-porwania; 190622-wojna-kajrata)) |
| Minerwa Metalia      | 3 | ((190427-zrzut-w-pacyfice; 190502-pierwszy-emulator-orbitera; 190503-bardzo-nieudane-porwania)) |
| Ernest Kajrat        | 2 | ((190622-wojna-kajrata; 190626-upadek-enklawy-floris)) |
| Olaf Zuchwały        | 2 | ((190502-pierwszy-emulator-orbitera; 190622-wojna-kajrata)) |
| Alan Bartozol        | 1 | ((190502-pierwszy-emulator-orbitera)) |
| Ariela Sirmin        | 1 | ((190626-upadek-enklawy-floris)) |
| Bożymir Szczupak     | 1 | ((190502-pierwszy-emulator-orbitera)) |
| Damian Orion         | 1 | ((190503-bardzo-nieudane-porwania)) |
| Erwin Galilien       | 1 | ((190427-zrzut-w-pacyfice)) |
| Hubert Kraborów      | 1 | ((190626-upadek-enklawy-floris)) |
| Jolanta Teresis      | 1 | ((190626-upadek-enklawy-floris)) |
| Karla Mrozik         | 1 | ((190503-bardzo-nieudane-porwania)) |
| Konrad Czukajczek    | 1 | ((190626-upadek-enklawy-floris)) |
| Liliana Bankierz     | 1 | ((190622-wojna-kajrata)) |
| Marcel Sowiński      | 1 | ((190626-upadek-enklawy-floris)) |
| Mirela Orion         | 1 | ((190503-bardzo-nieudane-porwania)) |
| Roman Rymtusz        | 1 | ((190626-upadek-enklawy-floris)) |
| Serafina Ira         | 1 | ((190622-wojna-kajrata)) |
| Szymon Maszczor      | 1 | ((190626-upadek-enklawy-floris)) |
| Wargun Ira           | 1 | ((190626-upadek-enklawy-floris)) |