# Martyn Hiwasser
## Identyfikator

Id: 2206-martyn-hiwasser

## Sekcja Opowieści

### Ratujemy porywaczy Eleny

* **uid:** 220610-ratujemy-porywaczy-eleny, _numer względny_: 49
* **daty:** 0112-09-15 - 0112-09-17
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Hestia d'Atropos, Jakub Bulgocz, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Raoul Lavanis

Streszczenie:

Elena jest już zbyt zdestabilizowana by funkcjonować niezależnie, więc wpierw zajęli się nią w Laboratorium Kranix a po zbudowaniu dla niej Containment Chamber przesłali ją do stacji medycznej Atropos do regeneracji. Stamtąd ją i Martyna porwał Syndykat Aureliona (ale Martyn przewidział JAKIŚ problem i ostrzegł Klaudię). Elena by zniszczyła okręt napastników, ale Zespół skutecznie odkrył problemy na Atropos, przesłuchali dowódcę, zneutralizowali miragenta i dotarli do okrętu Aureliona zdobywając jeńców i zatrzymując Elenę przed przebudzeniem i Spustoszeniem statku.

Aktor w Opowieści:

* Dokonanie:
    * opiekował się Eleną na Atropos i zauważył, że coś jest nie tak. Szyfrem ostrzegł Klaudię i przekonał Hestię do usunięcia info jak containować Elenę. Po czym zażywał sporo amnestyków. Porwany przez Syndykat z Eleną, nie umiał im nic powiedzieć - tylko dziesiątki potencjalnych planów (błędnych, bo "zapomniał" że Elena jest uncontainable). Eustachy zdjął mu neuroobrożę. TAK, Martyn zaplanował eksterminację z zimną krwią z Eleną jako bronią.
* Progresja:
    * wrócił na Infernię - pełnosprawny, acz musi towarzyszyć mu 10 akolitów z Eterni z uwagi na niestabilny arkin.


### Rzieza niszczy Infernię

* **uid:** 211027-rzieza-niszczy-infernie, _numer względny_: 48
* **daty:** 0112-03-28 - 0112-04-02
* **obecni:** Arianna Verlen, Elena Verlen, Flawia Blakenbauer, Klaudia Stryk, Martyn Hiwasser, Otto Azgorn, TAI Rzieza d'K1

Streszczenie:

Infernia wraca z Mevilig. Rzieza chce wyczyścić im pamięć o sobie. Arianna się opiera. Rzieza dowiaduje się o Ataienne. Arianna budzi Elenę po sympatii, Elena sprowadza Esuriit do laboratorium dekontaminacyjnego K1. Dużo śmierci. Martyn próbuje to opanować - budzi swoje simulacrum. Arianna i Klaudia opanowują przy pomocy Rziezy sytuację. 37% załogi Inferni nie żyje, Flawia wyssana w kosmos, Elena złamana, Martyn w szpitalu.

Aktor w Opowieści:

* Dokonanie:
    * gdy Klaudia powiedziała mu o problemie ze Skażeniem Esuriit, zaakceptował plan. Ale Esuriit jego też zmogło - obudził simulacrum i zaczął zabijać załogę Inferni (w samoobronie i dla demonstracji). Przynajmniej udało mu się wyssać Esuriit. Zapłacił straszną cenę - szpital, reputacja, te wszystkie śmierci na sumieniu. Znowu.
* Progresja:
    * 5 miesięcy regeneracji w szpitalu na K1. Jego arkin jest żywy, niestabilny i szuka. Ma aktywne simulacrum w formie "fingers". Część osób na Inferni się go boi.


### Kurczakownia

* **uid:** 211020-kurczakownia, _numer względny_: 47
* **daty:** 0112-03-24 - 0112-03-26
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, OO Infernia, Otto Azgorn, Vigilus Mevilig

Streszczenie:

By uratować jak najwięcej osób w Sektorze Mevilig, Eustachy zaprojektował Kurczakownię - maszynę do oddzielania mechanicznego głów i rdzenia od reszty człowieka. Arianna starła się ze Zbawicielem-Niszczycielem jako aspekt Zbawiciela, wspierana przez Działo Rozpaczy (Eustachy). Udało im się sporo uratować (40% populacji Planetoidy Kalarfam), acz załoga Inferni będzie mieć koszmary senne. Klaudia zebrała dość danych, by zapobiec naturalnej manifestacji Zbawiciela-Niszczyciela w Sektorze Astoriańskim i zrobić kontrakcję jak co.

Aktor w Opowieści:

* Dokonanie:
    * wpierw zsyntetyzował "głowę kultysty" by przekonać wszystkich, że Arianna jest Zbawicielem a potem z pomocą Klaudii przeprowadził operację "kurczakowanie - rekurczakowanie", by przetransportować jak najwięcej osób z Sektora Mevilig do Kontrolera Pierwszego. Duży sukces. On nie dostał żadnych ran mentalnych.


### Szara nawałnica

* **uid:** 211013-szara-nawalnica, _numer względny_: 46
* **daty:** 0112-03-18 - 0112-03-23
* **obecni:** Aleksandra Termia, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, Otto Azgorn, Remigiusz Błyszczyk, TAI Rzieza d'K1

Streszczenie:

Zespół przygotował się na wejście do Mevilig. Rzieza odkrył jak działają Piranie i że tamten sektor jest opanowany przez TAI 5+ generacji. Niestety, Infernia musiała uciekać przed Rziezą. Na miejscu Eustachy znalazł skuteczny sposób przemykania między Piraniami, dotarli do planetoidy Kalarfam gdzie znaleźli bazę ludzi. Mechaniczny "smok" został zniszczony przez Infernię, pobrali lokalsów i Arianna przekonała ich do zmiany kultu. Aha, anomalny wybuch torpedy anihilacyjnej Arianna przekierowała na planetę, wypowiadając ostrą wojnę TAI 5+ generacji...

Aktor w Opowieści:

* Dokonanie:
    * zajął się ocaleńcami z sektora Mevilig; wyjaśnił, że ich poważnym problemem jest potężne Skażenie Esuriit. Wymagają ogromnej pomocy na K1.


### Grupa Ekspedycyjna Kellert

* **uid:** 210929-grupa-ekspedycyjna-kellert, _numer względny_: 45
* **daty:** 0112-03-13 - 0112-03-16
* **obecni:** Adam Nerawol, Aleksandra Termia, Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Olena Orion, OO Infernia, OO Omega Septius, Otto Azgorn, TAI Marszałek Grzmotoszpon Trzeci, Vigilus Mevilig

Streszczenie:

Termia wysłała siły do sektora "Noviter", ale to był inny sektor (Mevilig). Straciła bezzałogowce, potem grupę ekspedycyjną Kellert. Kramer wysłał Infernię zakoloidowaną; okazało się, że ten sektor ma "strażnika Esuriit" zrobionego przez ludzi i grupę Piranii, sterowanych przez TAI 4 poziomu. Inferni udało się zebrać dane i wrócić z siedmioma uratowanymi członkami OO Savera, gdzie większość załogi została corruptowana przez Vigilusa...

Aktor w Opowieści:

* Dokonanie:
    * kosmiczny spacer do odstrzelonego fragmentu OO Savera; wyłączył kogo się dało z Dotkniętych przez Vigilusa. Po manifestacji Vigilusa doszedł co to jest - próbował ich ochronić sympatią Esuriit, ale nie udało mu się. Gdyby nie szybka reakcja Arianny i rozkaz ataku do Otto i Emulatorki, byłoby po nim. A tak uratowali 7 osób. Z 36 z załogi.


### Ostatnia akcja bohaterki

* **uid:** 210922-ostatnia-akcja-bohaterki, _numer względny_: 44
* **daty:** 0112-02-23 - 0112-03-09
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Izabela Zarantel, Jolanta Kopiec, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Olgierd Drongon, OO Żelazko, Roland Sowiński

Streszczenie:

Z Martynem skontaktowała się tien Kopiec prosząc o ostatnią misję zanim Esuriit ją pożre. Martyn zorganizował lot Infernią. Ustabilizował Jolantę, by ta dała radę dolecieć słowami i opowieściami, przy okazji tworząc piękną opowieść o Eterni i eulogię dla Jolanty - bohaterki wojennej - w Sekretach Orbitera (przy okazji wyszła jego przeszłość). Infernia zniszczyła Krwawą Bazę Piratów kosztem dewastacji Żelazka. Jolanta zniszczyła krwawego maga. Sektor jest bezpieczniejszy, Eternia szczęśliwa a Arianna ma profity.

Aktor w Opowieści:

* Dokonanie:
    * załatwił dla tien Kopiec Infernię (Klaudia, Arianna). Zatrzymał Ariannę przed próbą ugrania nagrody od Eterni za Jolantę. Utrzymał ją by dotarła do bazy piratów (opowieści, Izabela). Doprowadził do sławienia jej legendy, kosztem swojej reputacji i bezpieczeństwa. Zapewnił emiter anihilacji (Arianna). Zaplanował z tien Kopiec jak dorwać odszczepieńca Eterni. Zabarwił moc Arianny swoją krwią by zmusić wroga do działania, używając eternijskiej magii (kosztem relacji z Leoną). Martyn dla Jolanty Kopiec - jej reputacji i spokoju ducha - oddał praktycznie wszystko co mógł.
* Progresja:
    * stracił relację z Leoną i on nie jest w stanie jej odbudować. Ona może - on nie.
    * rozpoznawany przez Eternian za "swojego człowieka". Lubiany, podziwiany i zwyczajnie kochany w Eterni. "Nasz tien w kosmosie".
    * wydało się, że jest tien honorowym tienem Eterni. Wszyscy jego wrogowie wiedzą gdzie jest. Cała przeszłość go dogoniła.


### Stabilizacja Bramy Eterycznej

* **uid:** 210901-stabilizacja-bramy-eterycznej, _numer względny_: 43
* **daty:** 0112-02-09 - 0112-02-12
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Gilbert Bloch, Janus Krzak, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Medea Sowińska, OO Kanagar, OO Netrahina, OO Trasman, Seweryn Atanair

Streszczenie:

Infernia wróciła z innymi jednostkami naprawić Bramę Kariańską. Eustachy ufortyfikował i uniewidocznił pintki; Arianna pozyskała dane z reanimowanego magią krążownika Orbitera. Przez Bramę przeszła ON Catarina; Klaudia ją zlokalizowała i wystawiła Netrahinie i Niobe. Niestety, Arianna hiperlinkowała się z ON Spatium Gelida. Dla nich minęło tylko kilka godzin, dla nas - kilkadziesiąt lat...

Aktor w Opowieści:

* Dokonanie:
    * zdaniem Eleny, na pewno ma pas cnoty w sprzęcie (fałsz). Pomógł naprawić Elenę z Esuriit i Leonę po walce z Eleną.


### Morderstwo na Inferni

* **uid:** 210526-morderstwo-na-inferni, _numer względny_: 42
* **daty:** 0111-12-31 - 0112-01-06
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Feliks Walrond, Kamil Lyraczek, Karol Reichard, Klaudia Stryk, Leona Astrienko, Marian Tosen, Martyn Hiwasser, OO Tivr, Otto Azgorn, Tal Marczak

Streszczenie:

Noktiański mechanik Inferni, Tal Marczak, chciał zniszczyć OO Tivr (własność noktiańskiej rodziny Tala). Noktianie trzymają się razem - inni mechanicy Inferni doprowadzili do morderstwa Tala na K1 i wzięli amnestyki, by nikt nie wiedział co się stało. Zespół Inferni skutecznie doszedł do tego o co chodzi, odpowiedzialnych za morderstwo noktian przeniesiono na Żelazko a Arianna poważnie opieprzyła swoją noktiańską załogę. Będzie lepiej w przyszłości.

Aktor w Opowieści:

* Dokonanie:
    * kazał uaktualnić apteczkę Inferni o amnestyki; potem zrobił autopsję Tala Marczaka. I wykrył brak obecności amnestyków w noktiańskich apteczkach na Inferni.


### Osiemnaście Oczu

* **uid:** 210519-osiemnascie-oczu, _numer względny_: 41
* **daty:** 0111-12-07 - 0111-12-18
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Marian Tosen, Martyn Hiwasser, OO Alaya, OO Tivr

Streszczenie:

OO Alaya została zainfekowana anomalią memetyczną, infohazardem. Marian Tosen użył Zespołu by zneutralizować infohazard. Zespół przeszedł przez serię iteracji używając amnestyków by w końcu rozwiązać problem Alayi - amnestyki na całą Alayę. Do końca Zespół nie ma pojęcia czym ta anomalia była (bo wiedza ich by zaraziła). Udało się ewakuować większość Alayi.

Aktor w Opowieści:

* Dokonanie:
    * MVP misji. Pracował z amnestykami i inhibitorami zapamiętywania, przygotowując mechanizmy odporności na anomalię memetyczną. Też: poświęcił się z Eustachym (sceny notatkowe), by Arianna i Klaudia zostały "czyste".
* Progresja:
    * wysokie uznanie ze strony Mariana Tosena z grupy antymemetyczej Orbitera.


### Ewakuacja z Serenit

* **uid:** 210512-ewakuacja-z-serenit, _numer względny_: 40
* **daty:** 0111-11-23 - 0111-12-02
* **obecni:** Aida Serenit, AK Serenit, Arianna Verlen, Bogdan Anatael, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OE Falołamacz, OO Infernia, Roland Sowiński

Streszczenie:

Klaudia spowolniła Serenit, po czym Elena weszła zdalnym Entropikiem na pokład Falołamacza. Arianna zmanipulowała ludzi by dało się ich uratować. Samobójcza pintka zniechęciła Falołamacz do dalszej walki. Nie wszystkich udało się uratować (ale kapitana i arystokratę Sowińskich tak). Niestety, Infernia skończyła jako kosmiczny wrak i musiała wysłać SOS...

Aktor w Opowieści:

* Dokonanie:
    * wyjaśnił jak Serenit infekuje statki, po czym ratował "na wędce" osoby które wypadły z Inferni. W końcu mistrz spacerów kosmicznych ;-).


### Infekcja Serenit

* **uid:** 210428-infekcja-serenit, _numer względny_: 39
* **daty:** 0111-11-22 - 0111-11-23
* **obecni:** AK Serenit, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OE Falołamacz, OE Piękna Elena, OO Infernia, Persefona d'Infernia, Rafał Grambucz, Tadeusz Ursus

Streszczenie:

Ciężko uszkodzona Infernia wraca do domu i dostaje SOS od Ursusa - OE Falołamacz "zniknął", coś z anomaliami. Gdy Zespół dotarł do Falołamacza, próbowali dowiedzieć się co się dzieje - faktycznie mamy anomaliczny okręt. Podczas badań Falołamacza Morrigan przejęła Persefonę Inferni; Eustachy musiał poważnie uszkodzić Infernię by nie zniszczyli Falołamacza. Po kosmicznym spacerze Eleny i Klaudii doszli do tego co się stało - Falołamacz staje się Odłamkiem Serenit. I jeszcze da się ludzi ratować. Ale Infernia jest w bardzo złym stanie...

Aktor w Opowieści:

* Dokonanie:
    * rozpoznał zdalnie problemy Grambucza (anomalizacja). Gdy dowiedział się, że walczą przeciwko Serenitowi, przekazał Klaudii informację że mają mało czasu - faktycznie, Serenit tu leci.


### Znudzona załoga Inferni

* **uid:** 210421-znudzona-zaloga-inferni, _numer względny_: 38
* **daty:** 0111-11-16 - 0111-11-19
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Morrigan d'Tirakal, Otto Azgorn, Persefona d'Infernia, Tomasz Sowiński

Streszczenie:

Po raz pierwszy od dawna Infernia jest w stanie rozprzężenia - wracają z daleka po trudnej misji i nic im nie grozi. Jako, że załoga ma straszne tarcia (Leona poluje na "słabe jednostki", komandosi Verlenów chcą zatrzymać Leonę, noktianie vs astorianie...), Arianna autoryzuje Eustachego do zrobienia "Tirakala" jako niebezpieczną sytuację. Eustachy reanimuje Tirakala, ale niestety cholerstwo zanomalizowało - przywrócił Morrigan. By Zespół nie rozwiązał Tirakala za szybko, Eustachy trochę sabotował Infernię co Elena przeczytała jako... umówienie się na randkę. Gdy Klaudia wykryła, że na INFERNI pojawiła się straszna anomalia (Morrigan) próbowali to usunąć - ale Eustachy przecież sabotował Infernię! Udało im się opanować Morrigan, ale kosztem uszkodzeń i obrażeń na Inferni. I scenek kompromitujących Jolantę Sowińską. Infernia jest uszkodzona, ale się trzyma.

Aktor w Opowieści:

* Dokonanie:
    * próbował utrzymać Jolantę nieaktywną jak Infernia się wyłączyła, ale jakkolwiek pokonał i unieszkodliwił Tomasza to Jolanta go rozłożyła magią.


### Dekralotyzacja Asimear

* **uid:** 210414-dekralotyzacja-asimear, _numer względny_: 37
* **daty:** 0111-11-03 - 0111-11-12
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Llarnagraht, Malictrix d'Pandora, Mariusz Tubalon, Martyn Hiwasser, Martyna Bianistek, SCA Płetwal Błękitny, Tomasz Sowiński

Streszczenie:

Wiedząc, że Jolanta jest pod wpływem kralotha Infernia sfabrykowała kralotyczny alergizator. Arianna zdradziła się Aesimar i powiedziała, że ma zamiar zniszczyć kralotha z ramienia Orbitera. Udało się kralotha zmiażdżyć i zepchnąć do podziemi Asimear, choć ze sporymi stratami ludzkimi; tymczasem Tirakal uciekł na Płetwala. Elena zinfiltrowała Płetwala, ale nie mogła zniszczyć Tirakala nie krzywdząc ludzi. Destrukcję Tirakala Infernia zostawiła więc Malictrix.

Aktor w Opowieści:

* Dokonanie:
    * wraz z Klaudią bardzo skutecznie złożył środek do alergizacji kralotha używając próbek z Jolanty Sowińskiej i wsadzonego w nią pasożyta (w roli Governora).


### Arianna podbija Asimear

* **uid:** 210317-arianna-podbija-asimear, _numer względny_: 36
* **daty:** 0111-10-18 - 0111-11-02
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Martyna Bianistek, SCA Płetwal Błękitny, Tomasz Sowiński

Streszczenie:

"Płetwal Błękitny" ma na pieńku z "Goldarionem"; by nie złamać konspiracji, Klaudia potraktowała Płetwala Bazyliszkiem. Tymczasem pojawiła się wiadomość, że Arianna chce zniszczyć Asimear i żąda wydania Sowińskiej. Prawdziwa Arianna złamała konspirę, skontaktowała się z Jolantą Sowińską, po czym ją porwała. Okazało się, że Jolanta jest kontrolowana przez kralotycznego pasożyta...

Aktor w Opowieści:

* Dokonanie:
    * z pomocą Klaudii dał radę naprawić Jolę Sowińską usuwając jej kralotycznego pasożyta i przepinając jej kontrolne TAI na swoje miejsce.


### Infernia jako Goldarion

* **uid:** 210218-infernia-jako-goldarion, _numer względny_: 35
* **daty:** 0111-09-16 - 0111-10-01
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, OO Infernia, Tomasz Sowiński

Streszczenie:

Infernia udaje Goldariona, by przetransportować tajną przesyłkę Sowińskich do Jolanty Sowińskiej na planetoidzie Asimear. Tomasz Sowiński na Inferni okazuje się być dużym dzieckiem, łatwo jest wmanipulowany w otwarcie paczki - jest tam nietypowy antynanitkowy Entropik (którego psuje Elena przez błędną integrację). W tle wszystkiego - próba zdobycia niezależnej od Orbitera floty przez Aurum i jeden znikający advancer Kramera na Asimear...

Aktor w Opowieści:

* Dokonanie:
    * przekształcił używając swoich bioskilli wygląd Eustachego w Aleksandra Leszerta, dowódcę Goldariona. W ogóle, dba, by wszyscy wyglądali jak nie-Infernia.


### Wolna TAI na K1

* **uid:** 210209-wolna-tai-na-k1, _numer względny_: 34
* **daty:** 0111-08-30 - 0111-09-04
* **obecni:** Adalbert Brześniak, Andrea Burgacz, Janusz Parzydeł, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Miki Katasair, TAI Neita Lairossa, TAI Rzieza d'K1

Streszczenie:

Na Kontrolerze Pierwszym jest AI Lord - TAI Rzieza, system odpornościowy K1, który eksterminuje "wolne TAI". Skupił się na eksterminacji virtek. Klaudia i Adalbert odkryli Rziezę i pomogli uciec jednej wolnej TAI - Neicie (ukrytej w implantach). Tej samej Neicie, którą ujawnili przed Rziezą, co dało mu możliwość jej eksterminacji.

Aktor w Opowieści:

* Dokonanie:
    * Klaudia zwróciła się doń w chwili rozpaczy - trzeba natychmiast ewakuować Andreę zanim Rzieza ją zabije. Martyn autoryzował Leonę i przeprowadził evac. Jak zawsze niezawodny.


### Ratunkowa misja Goldariona

* **uid:** 210108-ratunkowa-misja-goldariona, _numer względny_: 33
* **daty:** 0111-08-09 - 0111-08-15
* **obecni:** Adam Permin, Aleksander Leszert, Elena Verlen, Feliks Przędz, Kamil Frederico, Klaudia Stryk, Martyn Hiwasser, Oliwia Pietrova, SCA Goldarion, Semla d'Goldarion, SL Uśmiechnięta

Streszczenie:

By zdobyć własne laboratorium w czarnych strefach Kontrolera, Klaudia weszła we współpracę z firmą ArcheoPrzędz i pomogła z Eleną uratować grupkę piratów, którzy bez advancera próbowali eksplorować wrak "Uśmiechniętej" - wraku Luxuritias. Po drodze przelecieli się najbardziej rozpadającym się i najbrudniejszym statkiem cywilnym - Goldarionem - jaki Klaudia kiedykolwiek widziała. Aha, i okazało się, że pojawiają się problemy na linii drakolici - fareil. Bonus: Martyn jako negocjator i mechanizm społeczny XD.

Aktor w Opowieści:

* Dokonanie:
    * negocjator i klej społeczny Klaudii i Eleny na Goldarionie. Ten, który deeskalował atmosferę i sprawił, że misja się jakoś udała. I stymulanty na Elenę. A potem - mnóstwo leczenia.


### Sympozjum Zniszczenia

* **uid:** 210120-sympozjum-zniszczenia, _numer względny_: 32
* **daty:** 0111-08-01 - 0111-08-05
* **obecni:** Arianna Verlen, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Martyn Hiwasser, OA Bakałarz, Olgierd Drongon

Streszczenie:

Maria Naavas próbuje spiknąć Ariannę i Olgierda i do pomocy używa Eustachego. Później - Eustachy, Diana, Klaudia i Elena są na Sympozjum Zniszczenia OA Bakałarz. Próbują przebić się przez anomalną materię. Diana odkrywa słabość owej materii do Esuriit i się Skaża; Skażenie rozlewa się po Bakałarzu i Zespół dostaje opiernicz za niekompetencję (większość na Elenę). Zespół w gniewie, nienawiści i Esuriit fabrykuje Emiter Plagi Nienawiści - nową broń na pokład Inferni, piekielnie niebezpieczną...

Aktor w Opowieści:

* Dokonanie:
    * poskładał skrzywdzonego przez Dianę, poskładał samą Dianę, deeskalował sytuację i poważnie opieprzył Eustachego za to, że nie zajmuje się Dianą - a to on ją tu ściągnął z Aurum.
* Progresja:
    * reputacja "świetni w niszczeniu i destrukcji, ale idźcie na bok i nie psujcie więcej". Takie trochę dzieci od zniszczenia.


### SOS z haremu

* **uid:** 210106-sos-z-haremu, _numer względny_: 31
* **daty:** 0111-07-26 - 0111-07-27
* **obecni:** Arianna Verlen, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Horacy Aktenir, Julia Aktenir, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Rozalia Teirik

Streszczenie:

Pałacyk rodu Aktenir został infestowany przez kralotha. Julia, zesłana z Orbitera za zerwanie z przeciwnikiem Eustachego, wysłała SOS do Orbitera. Infernia rusza Julii na pomoc. Gdy odkryli że chodzi o kralotha to zamiast wchodzić na kralotyczny teren, zorganizowali plan "arystokratki na orgię" i wyciągneli kralotycznego niewolnika (Horacego Aktenira) na otwartą przestrzeń, po czym Arianna uruchomiła obcą sentisieć a Eustachy zawołał do Julii rozmytej w środku...

Aktor w Opowieści:

* Dokonanie:
    * sporo pomocy medycznej i detoksu załogi Inferni; nie przeszkadzały mu erotyczne klimaty dookoła kralotha, przypominały mu jego burzliwą młodość ;-).


### Porwanie Anastazji z Odkupienia

* **uid:** 210127-porwanie-anastazji-z-odkupienia, _numer względny_: 30
* **daty:** 0111-07-22 - 0111-07-23
* **obecni:** Anastazja Sowińska Dwa, Arianna Verlen, Dariusz Krantak, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OA Odkupienie, OO Infernia, Rufus Niegnat, SP Plugawy Jaszczur

Streszczenie:

Arianna, Eustachy i Klaudia oddali "Anastazję" na OA Odkupienie tylko po to, by ją stamtąd porwać. Nawet załoga Inferni o tym nie miała pojęcia. Okazało się, że Odkupienie jest sentisieciowanym specjalnym statkiem Aurum, gdzie ci co narazili się Sowińskim mają szansę na "odkupienie".

Aktor w Opowieści:

* Dokonanie:
    * dopytał pretorian Anastazji odnośnie Odkupienia i pomógł im zregenerować się trochę po anomalii Klaudii/Diany. Odsunięty od głównej akcji - zbyt "dobry".


### Pułapka z Anastazji

* **uid:** 201230-pulapka-z-anastazji, _numer względny_: 29
* **daty:** 0111-07-19 - 0111-07-20
* **obecni:** AK Nocna Krypta, AK Rodivas, Anastazja Sowińska Dwa, Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Henryk Sowiński, Klaudia Stryk, Martyn Hiwasser, OA Zguba Tytanów, OO Infernia

Streszczenie:

Krypta "oddała Anastazję", zmieniając cywilny statek Rodivas w pułapkę na Ariannę.

Aktor w Opowieści:

* Dokonanie:
    * błędnie uznał "Anastazję Dwa" za prawdziwą Anastazję, jedynie dodając Ariannie kłopotów i zgryzot.


### Mychainee na Netrahinie

* **uid:** 201111-mychainee-na-netrahinie, _numer względny_: 28
* **daty:** 0111-07-05 - 0111-07-08
* **obecni:** Arianna Verlen, Eustachy Korkoran, Halina Szkwalnik, Klaudia Stryk, Martyn Hiwasser, Rufus Komczirp

Streszczenie:

Komczirp pod wpływem anomalicznych grzybów (Mychainee) próbował porwać Ariannę by ta pomogła Netrahinie. Arianna nie dała się porwać, ale z Zespołem wbiła na Netrahinę i zniszczyła anomalię. Niestety, Netrahina została bardzo poważnie uszkodzona, ale nie ma dużych strat w ludziach.

Aktor w Opowieści:

* Dokonanie:
    * przeprowadził operację abordażu Netrahiny 'kosmiczne śmieci'. Niestety, stracił większość kanistrów - więc w MedBay Netrahiny musiał zsyntetyzować nowe (z Klaudią), niszczące Mychainee.


### Pierwsza BIA mag

* **uid:** 210721-pierwsza-bia-mag, _numer względny_: 27
* **daty:** 0111-05-17 - 0111-05-19
* **obecni:** Arianna Verlen, BIA Solitaria d'Zona Tres, Eustachy Korkoran, Janus Krzak, Klaudia Stryk, Martyn Hiwasser, Romana Arnatin

Streszczenie:

Nie wiadomo gdzie jest rogue planet. Nie ma sensownej opcji powrotu. Infernia zaskarbiła sobie współpracę z BIA, ale podczas pracy BIA wykryła magię. Martyn przekonał BIA, że tkanka magiczna to "wszczepy" - BIA pozwoliła na używanie magii i faktycznie, to pomogło w naprawianiu bazy. Martyn i Eustachy rozdzielili Klaudię i BIA, ale Klaudia jest w stazie a BIA uzyskała moce magiczne. Jak wrócić? Ano, jedyną opcją jest w sumie w cieniu Nocnej Krypty...

Aktor w Opowieści:

* Dokonanie:
    * wyjaśnił BIA Solitaria, że tkanka magiczna to "wszczep" i magia to magitech. Wraz z Eustachym rozdzielił Klaudię i BIA. Nie radzi sobie z archaiczną medyczną technologią noktiańską.


### Baza Zona Tres

* **uid:** 210714-baza-zona-tres, _numer względny_: 26
* **daty:** 0111-05-14 - 0111-05-16
* **obecni:** Arianna Verlen, BIA XXX d'Zona Tres, Elena Verlen, Eustachy Korkoran, Janus Krzak, Martyn Hiwasser, Ulisses Kalidon

Streszczenie:

Podczas eksploracji archaicznej noktiańskiej bazy Infernia doszła do tego, że to mityczna Zona Tres - odpowiednik "area 51", z badaniami nad magią. Infernia przez przypadek uruchomiła Bię. Zespół zamaskował Infernię jako Alivię Nocturnę i Arianna przekonała Bię do wstępnej współpracy. Okazało się, że Bia jest chora i nie postrzega wszystkiego prawidłowo, Klaudia jest z Bią sprzężona (oops). By dowiedzieć się więcej, Infernia wyleciała na orbitę i okazało się, że są na rogue planet - planecie która wypadła z orbity i zagubiła się w kosmosie...

Aktor w Opowieści:

* Dokonanie:
    * objął kontrolę nad ambulatorium i chce przejąć szpital, za zgodą Bii. Chce uratować Bię oraz Klaudię.


### Po drugiej stronie Bramy

* **uid:** 210707-po-drugiej-stronie-bramy, _numer względny_: 25
* **daty:** 0111-05-11 - 0111-05-13
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Janus Krzak, Klaudia Stryk, Martyn Hiwasser, Raoul Lavanis, Tal Marczak

Streszczenie:

Infernia próbując przetransportować eksperta Eteru na konferencję przez Bramę uległa katastrofie - przeniosła się GDZIEŚ INDZIEJ, pod wodę i zniknęła Klaudia. Nie uległa katastrofalnym uszkodzeniom, ale po zachęceniu pintką Lewiatana dotarła do archaicznej podwodnej noktiańskiej bazy. Tam znajduje się Klaudia, ale coś jest z nią nie tak. Sprzęgnięta z bazą? Tak czy inaczej, infiltracja bazy dopiero się zacznie. Trzeba znaleźć Klaudię i wrócić do domu.

Aktor w Opowieści:

* Dokonanie:
    * gdy Klaudia zniknęła, skupił się by ją znaleźć a potem uspokoił Janusa, by ten pomógł Ariannie. Potem próbował nawiązać kontakt z "anomaliczną Klaudią".


### Wyścig jako randka?

* **uid:** 201125-wyscig-jako-randka, _numer względny_: 24
* **daty:** 0111-04-08 - 0111-04-13
* **obecni:** Arianna Verlen, Diana Arłacz, Gunnar Brunt, Klaudia Stryk, Maria Naavas, Martyn Hiwasser, Olgierd Drongon

Streszczenie:

Infernia traci części i ekipę remontową, bo Olgierd z Żelazka na niej pasożytuje. W wyniku komedii pomyłek doszło do tego, że Olgierd uznał, że Arianna się w nim podkochuje ale nie wie jak zagadać. Skończyło się pojedynkiem ścigaczy na torze sfabrykowanym przez Klaudię i na anomalicznym Skażeniu Kontrolera Pierwszego. Przez Klaudię, naturalnie.

Aktor w Opowieści:

* Dokonanie:
    * święcie przekonany, że sekretem Arianny jest zauroczenie Olgierdem - i to przekazał Marii. Spiskuje z Marią, by Arianna x Olgierd.


### Krypta i Wyjec

* **uid:** 201216-krypta-i-wyjec, _numer względny_: 23
* **daty:** 0111-03-29 - 0111-03-31
* **obecni:** AK Nocna Krypta, AK Wyjec, Anastazja Sowińska, Arianna Verlen, Diana Arłacz, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser

Streszczenie:

Podczas walki Nocnej Krypty i Wyjca Zespół wbił się na Nocną Kryptę, by wyleczyć się z Różowej Plagi. Na miejscu okazało się, że Krypta ma "nowe programowanie" (leczenie lub 'zachowanie'), nowe mechanizmy (MIRV, anomalne działo, anioły) oraz że jest groźniejsza niż kiedykolwiek. Arianna zostawiła na pokładzie Krypty Anastazję, by Krypta naprawiła jej Wzór, po czym oszukała Sowińskich, że niby Krypta porwała Anastazję.

Aktor w Opowieści:

* Dokonanie:
    * zahibernował większość załogi i z użyciem Nocnej Krypty znalazł antidotum na Różową Plagę. Ogłuszony od tyłu przez Eustachego, by mogli zostawić Anastazję.


### Anastazja - bohaterką?

* **uid:** 201118-anastazja-bohaterka, _numer względny_: 22
* **daty:** 0111-03-22 - 0111-03-25
* **obecni:** Anastazja Sowińska, Arianna Verlen, Diana Arłacz, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, Stefan Jamniczek

Streszczenie:

Anastazja nie została bohaterką. Wpierw Eustachy wplątał się w pojedynek o Dianę z jakimś szlachcicem - skończyło się różową mgiełką miłości i pospieszną ewakuacją Inferni z Kontrolera Pierwszego, potem epicki plan Izy z piratami rozpadł się, bo na Infernię polował jakiś statek koloidowy. Ale przetrwali - i mają zamiar to wszystko rozplątać.

Aktor w Opowieści:

* Dokonanie:
    * współuczestniczył w Paradoksie Miłości Arianny, po czym od razu ogłosił kwarantannę na Inferni i uznał Infernię za Statek Plag.


### Sabotaż świni

* **uid:** 201104-sabotaz-swini, _numer względny_: 21
* **daty:** 0111-03-13 - 0111-03-16
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Diana Arłacz, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, Rafał Armadion, Robert Garwen

Streszczenie:

Sabotażysta - miragent - podjął próbę zniszczenia Arianny gdy ta ładowała świnie na Wesołego Wieprzka używając wybuchowej, anomalnej świni. Zespół przetrwał, choć "Wieprzek" został zniszczony. Arianna pojmała miragenta i przekazała go do Kontrolera Pierwszego, acz kosztem ofiar w ludziach. No i nie było dość świń by je wziąć na orbitę XD.

Aktor w Opowieści:

* Dokonanie:
    * ratował na lewo i prawo ludzi po wybuchu sabotowanej eksplozywnej świni.


### Arystokratka w ładowni na świnie

* **uid:** 200909-arystokratka-w-ladowni-na-swinie, _numer względny_: 20
* **daty:** 0111-02-12 - 0111-02-17
* **obecni:** Anastazja Sowińska, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, OO Galaktyczny Tucznik

Streszczenie:

Zespół wyciągnął Martyna z więzienia (legalnie). Wpakowali się na Galaktycznego Tucznika by przenieść świnie na orbitę... ale poleciała z nimi dziennikarka Izabela (o czym wiedzą) i 15letnia Anastazja Sowińska (o czym nie wiedzą). Przy lądowaniu, atak nojrepów na Tucznika spowodował katastrofę - seria Paradoksów Anastazji, uszkodzenia Tucznika itp. Zespół uratował Anastazję (choć kosztem ran Martyna) i wylądował Tucznikiem na Astorii. To jest sukces.

Aktor w Opowieści:

* Dokonanie:
    * bohater; zrobił spacer kosmiczny pod ostrzałem nojrepów by ze Skażonej ładowni uratować nastolatkę Aurum, mimo strasznych ran od owych nojrepów.
* Progresja:
    * niechętnie widziany na Orbiterze chwilowo
    * został Wielką Miłością Anastazji Sowińskiej. Piętnastolatki.
    * przez następny tydzień w bardzo złym stanie i złej formie.


### Nienawiść do świń

* **uid:** 200826-nienawisc-do-swin, _numer względny_: 19
* **daty:** 0111-02-05 - 0111-02-11
* **obecni:** Aleksandra Termia, Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Sebastian Alarius, Tadeusz Ursus

Streszczenie:

Po pojedynku Kramer opieprzył Ariannę i dał jej dyskretną tajną misję - ma niby w niesławie wozić świnie, naprawdę ma pomóc Orbiterowi rozwiązać spisek w Trzecim Raju. Ale wysłanie Arianny na świnie wywołało bunt na Kontrolerze Pierwszym; Arianna musiała rozpaczliwie deeskalować, nawet budząc Leonę. Udało się bunt opanować, acz wysokim kosztem - pierwszy post-noktiański kapitan nie żyje, załoga Inferni częściowo zesłana będzie do Trzeciego Raju a Arianna miała bardzo żenujące wejście.

Aktor w Opowieści:

* Dokonanie:
    * na tyle dobry lekarz, że mimo że był w kiciu to trzeba było go wyciągnąć by postawił Leonę by nie ucierpiała za mocno. Nie chce zdradzić tożsamości damy, którą chronił - nawet, jeśli dzięki temu by wyszedł.


### Gdy Arianna nie patrzy

* **uid:** 200822-gdy-arianna-nie-patrzy, _numer względny_: 18
* **daty:** 0111-02-02 - 0111-02-04
* **obecni:** Klaudia Stryk, Martyn Hiwasser, Sylwia Milarcz, Tadeusz Ursus

Streszczenie:

Klaudia i Martyn mieli dość tego, że nie wiedzą nic o tematach związanych z Esuriit a ostatnio mają z tym ciągle do czynienia. Podczas dojścia do tego jaką dawkę dostał Tadeusz gdy stworzył Simulacrum przypadkowo odkryli konspirację - lekarz Tadeusza próbował go (długoterminowo) zniszczyć. Po drodze reputacja Arianny ucierpiała a Martyn trafił do aresztu za okrutne użycie magii wobec thugów. Tadeusz jest jednak bardzo wdzięczny Ariannie która o niczym nie wie :-).

Aktor w Opowieści:

* Dokonanie:
    * zainteresowany Esuriit by chronić Infernię, wpakował się w intrygę Eterni. Użył mocy i skręcił trzech napastników bioformicznie. Skończył w brygu.
* Progresja:
    * po odruchowej obronie na Kontrolerze Pierwszym gdzie wypaczył 3 thugów dostał opinię okrutnego biomaga Inferni, godnego Leony.
    * ma psychofankę - Sylwię Milarcz - ze świty Tadeusza Ursusa. Sylwia się go boi i to ją strasznie kręci.


### Sekrety Orbitera - historia prawdziwa

* **uid:** 200819-sekrety-orbitera-historia-prawdziwa, _numer względny_: 17
* **daty:** 0111-01-24 - 0111-02-01
* **obecni:** Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Leszek Kurzmin, Martyn Hiwasser, Sabina Servatel, Tadeusz Ursus

Streszczenie:

Elena wyzwała Leonę na pojedynek. Klaudia i Arianna wkręciły Tadeusza. Tadeusz - arcymag Eterni - używa Simulacrum w walce, więc Zespół zaczął montować plan jak Leona może jednak wygrać z Simulacrum. I wszystko skończyło się tym, że Leona wygrała, Elena jest wolna od Tadeusza i powstała holodrama "Sekrety Orbitera"...

Aktor w Opowieści:

* Dokonanie:
    * stworzył neurotoksynę osłabiającą Tadeusza oraz otworzył z Eustachym i Zodiac Leonę, by ją przebudować i jej pomóc. Udało się. Duży sukces.


### Narkotyczna pacyfikacja tienów na Castigatorze

* **uid:** 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze, _numer względny_: 16
* **daty:** 0110-12-19 - 0110-12-21
* **obecni:** Elena Verlen, Eleonora Perłamila, Igor Arłacz, Julia Myrczek, Kamil Burgacz, Klaudia Stryk, Konstanty Keksik, Kosmicjusz Tanecznik Diakon, Leszek Kurzmin, Łucja Larnecjat, Marta Keksik, Martyn Hiwasser, Patryk Samszar

Streszczenie:

Klaudia została poproszona przez oficer naukową na Castigatorze o zbadanie fenomenu pracowitości spolegliwości. Okazało się, że XO neutralizuje tienów wspierając dostarczanie narkotyków imprezowych-kralotycznych (anipacis). Klaudia zdecydowała się zostawić tam Martyna na stopniowe wyciszanie anipacis. Nie doszła do tego kto DOKŁADNIE jest konwerterem anipacis na Feromon Ambicji, ale zostawiła sprawę wystarczająco bezpieczną.

Aktor w Opowieści:

* Dokonanie:
    * wykorzystując zarówno swoją wiedzę z czasów Cieniaszczytu jak i biomagiczną analizę, zidentyfikował środki – feromon Ambicji na Castigatorze. Skonfliktowany - dla niego to nie jest nic niemoralnego, ale rozumie czemu to problem. Rekomenduje łagodne ‘wyciszenie’ tego eksperymentu.


### Sabotaż Miecza Światła

* **uid:** 200415-sabotaz-miecza-swiatla, _numer względny_: 15
* **daty:** 0110-10-08 - 0110-10-10
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, Mateusz Sowiński

Streszczenie:

Zniszczona Infernia została uratowana przez Miecz Światła, który zaraz potem leciał ratować inny statek przed AK Serenit. W odpowiedzi Arianna doprowadziła do sabotażu Miecza Światła i zniszczenia reputacji komodora Sowińskiego by ratować Miecz Światła przed Serenitem. Jej się upiekło, acz pojawiło się więcej ofiar śmiertelnych...

Aktor w Opowieści:

* Dokonanie:
    * pamiętając straszliwe przeżycia z Serenit w przeszłości wybudził Ariannę z regeneracyjnego snu. Pośrednio odpowiedzialny za sabotaż Miecza Światła.


### O psach i kryształach

* **uid:** 200408-o-psach-i-krysztalach, _numer względny_: 14
* **daty:** 0110-09-29 - 0110-10-04
* **obecni:** Arianna Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Sebastian Namczek

Streszczenie:

Infernia - QShip - została wysłana na rutynową misję zapobieżenia zamieszkom na stacji Samojed. Na miejscu okazało się, że jest zainfekowany anomalnymi kryształami krążownik Leotis. Arianna Verlen ciężko uszkodziła stację Samojed i prawie zniszczyła Infernię, ale dała radę zniszczyć Leotis poświęcając więcej niż ktokolwiek mógł chcieć.

Aktor w Opowieści:

* Dokonanie:
    * utrzymał umierającą Infernię przy życiu. Wykrył hybrydyzację psów hodowanych w asteroidach z ludźmi; też pomógł Klaudii z anomalią krystaliczną.


### Pech komodora Ogryza

* **uid:** 200115-pech-komodora-ogryza, _numer względny_: 13
* **daty:** 0110-07-20 - 0110-07-23
* **obecni:** Dominik Ogryz, Janet Erwon, Klaudia Stryk, Martyn Hiwasser, Sonia Ogryz, Wioletta Keiril

Streszczenie:

Janet Erwon pomagała załodze Inferni znaleźć miejsce dla Wioletty na Kontrolerze Pierwszym. W wyniku intrygi Janet, Martyna i Klaudii - komodor Ogryz ma przechlapane na Kontrolerze Pierwszym, Janet dostała nowy statek (Rozbójnik) i musi z Ogryzem zdobyć stację Dorszant. A Wioletta, jak się okazało, jest dość niekompetentna w grach wojennych Orbitera. No i Ogryz ma przed sobą koniec kariery politycznej i początek kariery militarnej. Wszyscy przegrali..?

Aktor w Opowieści:

* Dokonanie:
    * by chronić Wiolettę, połączył siłę Klaudii i Janet, powodując zamieszki na Orbiterze. Idzie na każdą randkę na jaką może. Skończył przez to w szpitalu.


### Infernia i Martyn Hiwasser

* **uid:** 200106-infernia-martyn-hiwasser, _numer względny_: 12
* **daty:** 0110-06-28 - 0110-07-03
* **obecni:** Arianna Verlen, Dominik Ogryz, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, Szczepan Olgrod, Wioletta Keiril

Streszczenie:

Arianna Verlen uratowała Martyna Hiwassera przed nędznym losem, ratując go od problemów z komodorem Ogryzem na Kontrolerze Pierwszym. Niestety, podpadła dużej ilości oficerów Orbitera. Ale za to przywróciła Martyna do załogi Inferni.

Aktor w Opowieści:

* Dokonanie:
    * doskonałej klasy biomanipulator i potężny mag. Elegancki kobieciarz. Wdał się w małe rozgrywki polityczne w Orbiterze, pomógł Wioli i został pojmany przez Ogryza - i uratowany przez zespół Arianny.


### Skażona symulacja Tabester

* **uid:** 240204-skazona-symulacja-tabester, _numer względny_: 11
* **daty:** 0109-11-01 - 0109-11-04
* **obecni:** Ewaryst Kajman, Klaudia Stryk, Lara Linven, Martyn Hiwasser, OO Serbinius, OOn Tabester, TAI Ashtaria d'Tabester

Streszczenie:

Klaudia, zmagająca się z brakiem pamięci skąd się tu wzięła i co robi, znajduje się w symulacji ataku terrorformów na statek. Skupia się na ratowaniu niewinnych ludzi i 'marines' (ludzi) przed Skażoną TAI. Gdy odkrywa sytuację i nawiązuje kontakt z Martynem, przeciąża TAI i wprowadza ją w konfuzję, wyłączając symulację i ratując ludzi. Ale skąd wzięło się Skażenie..?

Aktor w Opowieści:

* Dokonanie:
    * gdy Klaudia jest w symulacji, monitoruje jej stan i pomaga jej hormonalnie. Gdy ktoś w symulacji jest Złamany (np. Lara), wyciąga tą osobę asap. Część (np. Larę) zdążył uratować. Spowalnia też białkowy komponent Skażonej TAI (umysły śpiących ludzi).


### Załoga Vishaera przeżyje

* **uid:** 240102-zaloga-vishaera-przezyje, _numer względny_: 10
* **daty:** 0109-10-26 - 0109-10-28
* **obecni:** Anastazy Termann, Fabian Korneliusz, Helmut Szczypacz, Klaudia Stryk, Martyn Hiwasser, Wolfgang Sępiarz

Streszczenie:

Vishaer, niezależny statek wpadł w poważne kłopoty i nie miał szansy na ratunek. Kapitan kazał załodze się hibernować po czym wyłączył generator Memoriam i spróbował użyć magii by wszyscy przetrwali. 3 lata później na Vishaer natrafił inny statek, Talikazer, i próbował mu pomoc. 2h potem na sygnał SOS przyleciał Serbinius. Udało się uratować załogę Talikazera oraz część załogi Vishaera, po czym Vishaer zmienił się w anomalię.

Aktor w Opowieści:

* Dokonanie:
    * uratuje wszystkich kogo się da, dowodził operacją wejścia na AK Vishaer, osłonił Klaudię w walce z terrorformem i udało mu się wydobyć stasis-chamber które dało się uratować


### Ziarno Kuratorów na Karnaxianie

* **uid:** 230530-ziarno-kuratorow-na-karnaxianie, _numer względny_: 9
* **daty:** 0109-10-06 - 0109-10-07
* **obecni:** Anastazy Termann, Fabian Korneliusz, Helmut Szczypacz, Klaudia Stryk, Kurator Sarkamair, Leo Mikirnik, Martyn Hiwasser, OO Serbinius, SC Karnaxian

Streszczenie:

Kurator Sarkamair rozstawił Ziarna - mikrofabrykatory atakujące TAI - i złowił salvager Karnaxian. Skonstruował Alexandrię i zaczął rekonstruować silniki by odlecieć do bazy o bliżej nieokreślonej lokalizacji. Serbinius zainteresował się Karnaxianem poza kursem i nie dał się zmylić optymistyczną Semlą i projekcją załogi. Gdy Serbinius odparł atak Kuratora (co było dość trudne), zrobili intruzję by uratować uwięzioną w Aleksandrii załogę. Niestety, nie udało im się - jedyne co mogli zrobić to zniszczyć Karnaxian i ostrzec wszystkich przed nową strategią Kuratorów, a zwłaszcza Sarkamaira.

Aktor w Opowieści:

* Dokonanie:
    * pomógł Leo i Klaudii przekonać Fabiana, że załoga Karnaxiana jest już martwa. Chciał uratować załogę Karnaxiana przed Kuratorem, ale nie kosztem Serbiniusa.


### Helmut i nieoczekiwana awaria Lancera

* **uid:** 230528-helmut-i-nieoczekiwana-awaria-lancera, _numer względny_: 8
* **daty:** 0109-09-23 - 0109-09-26
* **obecni:** Anastazy Termann, Fabian Korneliusz, Helmut Szczypacz, Klaudia Stryk, Martyn Hiwasser, Miranda Termann, Nikodem Dewiremicz, OO Serbinius

Streszczenie:

Helmut próbuje optymalizować substrat fabrykatorów na Serbiniusie, co niepokoi Klaudię. Tymczasem pojawia się seria niewielkich awarii na różnych jednostkach, Serbinius pomaga. Klaudia zauważa wzór, ale dopiero jak członek Serbiniusa ma awarię jetpacka znajduje dowód - to Anomalia Statystyczna, niewykrywalna na Primusie. Współpracując z teoretykiem z Orbitera mapują obszar tej Anomalii i wyciągają wszystkich z kłopotów.

Aktor w Opowieści:

* Dokonanie:
    * jako advancer uratował Uciekiniera Anastazego odcinając odeń jetpack, potem uratował jetpack jak się okazał być dowodem Anomalii Statystycznej. Jako lekarz odkrył, że Anastazy jest Skażony i przekazał to Klaudii. Próbuje moderować młodego Anastazego - ważniejsza jest pomoc ludziom niż piraci i przygody.


### Rozszczepiona Persefona na Itorwienie

* **uid:** 230521-rozszczepiona-persefona-na-itorwienie, _numer względny_: 7
* **daty:** 0109-09-15 - 0109-09-17
* **obecni:** Emilia Ibris, Fabian Korneliusz, Helmut Szczypacz, Iskander Matorin, Karol Brinik, Klaudia Stryk, Martyn Hiwasser, Mojra Karstall, OO Itorwien, OO Serbinius, Tadeusz Arkaladis

Streszczenie:

Itorwien, 'pługośmieciarka' Orbitera miała problem z przemytnikami - ktoś otworzył przemycane narkotyki i cała jednostka była Skażona (lifesupport). Persefona została ograniczona przez psychotronika na pokładzie, więc nie mogła nic zrobić. Ciśnienie i uszkodzenie Persi doprowadziło do jej rozszczepienia. Serbinius uratował Itorwien, bez strat (poza winnym psychotronikiem zabitym przez rozszczepioną Persefonę).

Aktor w Opowieści:

* Dokonanie:
    * pełni rolę advancera, wchodząc na Itorwien. Jako dobry advancer dał radę uniknąć eksplozji pancerza aktywnego zrobionego przez Persi. Doszedł do substratu narkotyków w systemie lifesupport i wprowadził OVERSEER do systemów Persefony.


### Młodziaki na Savarańskim statku handlowym

* **uid:** 220925-mlodziaki-na-savaranskim-statku-handlowym, _numer względny_: 6
* **daty:** 0109-05-24 - 0109-05-25
* **obecni:** Dawid Aximar, Fabian Korneliusz, Klaudia Stryk, Klaudiusz Zanęcik, Martyn Hiwasser, OO Serbinius, Perdius Aximar, SC Hektor 17

Streszczenie:

Młody Orbiterowiec wraz z dziewczyną zatrudnili się na savarańskiej jednostce SC Hektor 17. Ojciec owego Orbiterowca jest mentorem Fabiana Korneliusza, więc Zespół udał się wyciągać tą dwójkę. Klaudia doszła do tego, że to neonoktiańska jednostka i współpracując z kapitanem tej jednostki wyciągnęła młodych oraz ukryła niegroźne naruszenia na 'Hektorze 13' które Fabian by wykorzystał.

Aktor w Opowieści:

* Dokonanie:
    * nadal nie jest wolny od echa Noctis i wojny z Noctis; był obecny, ale Klaudia skutecznie mu nic nie powiedziała.


### Chory piesek na statku Luxuritias

* **uid:** 220716-chory-piesek-na-statku-luxuritias, _numer względny_: 5
* **daty:** 0109-05-05 - 0109-05-07
* **obecni:** Achellor Santorinus, Bożena Kokorobant, Fabian Korneliusz, Henryk Urkon, Klaudia Stryk, Martyn Hiwasser, OO Serbinius, Roberto Santorinus, SL Rajasee Bagh, Teodor Margrabarz

Streszczenie:

Młody panicz dał sygnał alarmowy, że plaga na pokładzie statku Luxuritias opuszczającego Valentinę. Serbinius poleciał zbadać sprawę. Na miejscu okazało się, że chłopak chciał by jego psa zobaczył weterynarz. A JEDNAK NIE. Faktycznie jest plaga - z Neikatis. Zespół wszedł w sojusz z szefem ochrony statku i plaga została powstrzymana a statek odholowany do Atropos na fumigację (Śmiechowica to typ grzyba).

Aktor w Opowieści:

* Dokonanie:
    * przypisany do OO Serbinius; z Klaudią doszedł do tego że Rajasee Bagh jest zakażony Śmiechowicą. Gdy Klaudia i Fabian robili dywersję, jednym zaklęciem zdjął najgroźniejszego z zarażonych. Cichy jak zawsze.


### Złamane serce Martyna

* **uid:** 211016-zlamane-serce-martyna, _numer względny_: 4
* **daty:** 0085-08-31 - 0085-09-06
* **obecni:** Adela Kołczan, Adrian Kozioł, Amadeusz Martel d'Kopiec, Jolanta Kopiec, Kalina Rota d'Kopiec, Martyn Hiwasser, Wanessa Ogarek

Streszczenie:

Martyn i Kalina są małżeństwem. Ambasadorzy Eterni w Aurum, ocieplają wizerunek. Niestety, morderca zabija Kalinę. Martyn znajduje mordercę i okazuje się, że to Adela, którą kiedyś Paradoks Martyna zniszczył. Martyn robi jej bezbolesny coup de grace i opuszcza zarówno Aurum, Eternię jak i arkin - odchodzi do Orbitera.

Aktor w Opowieści:

* Dokonanie:
    * stracił Kalinę - swoją żonę i serce ORAZ nienarodzone dziecko. Znalazł mordercę - Adelę - po czym się z nią spotkał. Gdy okazało się, że Adela nie kontaktuje i chce być z Martynem, on ją przytulił i zrobił _coup de grace_. Po czym opuścił Aurum, Eternię i arkin.
* Progresja:
    * od tej pory nie ujawnia się, nie mówi o sobie, nie mówi o przeszłości. Jest cieniem. I nie umie już działać w Eterni - za dużo wspomnień. Przestaje być lordem eternijskim - przekazał arkin Jolancie.
    * jego paranoja i te wszystkie mentalne sprawy się u niego nasiliły. Od tej pory żyje na lekach i supresorach.
    * z perspektywy Aurum jest niebezpieczny i niezwykle zimny. Stracił posadę ambasadora. Unikany.


### Pierwsza bitwa Martyna

* **uid:** 210918-pierwsza-bitwa-martyna, _numer względny_: 3
* **daty:** 0080-11-23 - 0080-11-28
* **obecni:** Jolanta Kopiec, Jonasz Staran, Kalina Rota d'Kopiec, Martyn Hiwasser, OO Invictus, Pandemiusz Diakon, Szczepan Kutarcjusz

Streszczenie:

Tien Kopiec zaatakowała dziwną noktiańską jednostkę i została odparta. Wezwała na pomoc OO Invictus. Po otrzymaniu wsparcia mieli się tylko wycofać i zdobyć kilku jeńców, ale okazało się to dużo trudniejsze - Invictus nie miał szans z noktiańskim statkiem i musiał uciekać. Kopiec + Kutarcjusz + Hiwasser dali radę sterroryzować załogę noktiańskiej jednostki. Tamci się poddali. Ogromne straty po wszystkich stronach. Ale - na plus - Martyn dostał uratowaną Kalinę od Jolanty, co zniszczy mu karierę w Orbiterze XD.

Aktor w Opowieści:

* Dokonanie:
    * 25 lat. Medyk na OO Invictus. Zbudował magicznie detekcję gdzie są wrogowie na wrogiej jednostce ("czy ktoś żyje pod gruzami"), wzmocnił tien Kopiec przez arkiny i oddał jej swoją energię by sformowała simulacrum. Ale uratował Kalinę, ma szacunek Jolanty i wyłączył delikwenta w ciężkim serwarze magią anestezjologiczną. Ma Kalinę od Jolanty w prezencie XD.
* Progresja:
    * w oczach tien Jolanty Kopiec z Eterni jest proaktywny, ratuje, działa. Dorósł. Ryzykowny, ale ma dobre pomysły i pamięta o innych. Lubi go.
    * przez moment był w sieci krwawych rubinów Eterni. ROZUMIE jak to działa. Rozumie Eternię. Już nie myśli o nich jako o "złe wampiry".
    * MEDAL! Za zdobycie noktiańskiego okrętu, odwagę i poświęcenie w kryzysowej sytuacji. Medal symbolizuje, że bez niego inaczej by się potoczyło.
    * od Jolanty Kopiec dostał Kalinę w prezencie. Tzn, Kalina jest wolna, ale jest "z nim" i "jego". Co strasznie utrudnia mu życie na jednostce wojskowej Orbitera.


### Medyk przeciwko Cieniaszczytowi

* **uid:** 210912-medyk-przeciwko-cieniaszczytowi, _numer względny_: 2
* **daty:** 0079-09-13 - 0079-09-16
* **obecni:** Adela Kołczan, Adrian Kozioł, Franciszek Tocz, Irena Czakram, Kasia Pieprz, Martyn Hiwasser, Paweł Pieprz

Streszczenie:

Sprawa zaginionej siostry kumpla doprowadziła do odkrycia, że BlackTech robi eksperymenty na porywanych ludziach. BlackTech jest tak wysoko osadzone, że nikt nie chciał przeciw nim stanąć. Martyn Hiwasser, którego ruchy ratujące siostrę przyjaciela były neutralizowane zaatakował ich "frontalnie" i ujawnił co zrobili. Za to jest nagroda za jego głowę i terminuska-sojuszniczka-na-szybko wysłała go do sił specjalnych Orbitera. By go ratować.

Aktor w Opowieści:

* Dokonanie:
    * 24 lata. Chcąc uratować siostrę przyjaciela, zniszczył najgroźniejszy gang w Cieniaszczycie zmieniając strategie co pięć sekund i skazując się na śmierć. Uratowany przez Orbiter. Musiał odejść, ale odszedł z HUKIEM.Pierwszy raz kogoś zabił (Franciszek Tocz), doprowadził do śmierci siostry przyjaciela (Kasia Pieprz) i skrzywdził swoją ochroniarkę, która się w nim śmiertelnie zakochała przez magię (Adela Kołczan).
* Progresja:
    * nieskończona wręcz nienawiść wobec cieniaszczyckiego gangu BlackTech i działań mających krzywdzić niewinnych ludzi - skazując ich na eksperymenty celem nadawania magii.
    * odrzucony przez gangi i grupy cieniaszczyckie, ale też uwielbiany przez te grupy które utraciły w Cieniaszczycie bliskich do BlackTech.
    * wróg bardzo wysoko w Cieniaszczycie, w BlackTech (i pochodnych), Paweł (były przyjaciel), Adrian (były były Kasi i były Adeli) i wśród wielu osób w Cieniaszczycie. Jest... źle. SERIO ciężko. Nagroda za głowę.
    * Opinia mavericka. Śmiertelnie niebezpiecznego, inteligentnego i szybko działającego maga, który jednak na poziomie strategicznym nie umie zarządzać ryzykiem w żaden sposób.


### Broszka dla eternianki

* **uid:** 210904-broszka-dla-eternianki, _numer względny_: 1
* **daty:** 0075-09-10 - 0075-09-21
* **obecni:** Dagmara Kamyk, Jolanta Kopiec, Kalina Rota d'Kopiec, Martyn Hiwasser, Ola Klamka

Streszczenie:

Kaprys eternijskiej arystokratki, która chciała pozyskać broszkę i zażądała tego od swojej wygnanej służki splótł losy owej służki z Martynem Hiwasserem. Ten od razu zwąchał okazję do założenia HANDLU BIŻUTERIĄ EGZOTYCZNĄ! Plan był niezły - ale ktoś podrobił część biżuterii (krwawe rubiny), dzięki czemu sygnał poddaństwa wobec tien Kopiec stał się ozdobą. Gdy Martyn i Kalina się z tego wyplątywali, nie osiągnęli ogromnego majątku - ale przynajmniej nikomu nic się nie stało.

Aktor w Opowieści:

* Dokonanie:
    * 20 lat. Chcąc pomóc dwóm dziewczynom zrobił potężny popyt na biżuterię eternijską co sprawiło, że część ludzi zaczęła replikować symbole poddaństwa Jolanty Kopiec. Pomógł trochę, ale przekroczył swoje umiejętności - nie został krezusem eternijskiej mody. Ale Kalina dostała broszkę i wszystko dobrze się skończyło.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 49, @: 0112-09-17
    1. Primus    : 49, @: 0112-09-17
        1. Sektor Astoriański    : 46, @: 0112-09-17
            1. Astoria, Orbita    : 16, @: 0112-04-02
                1. Kontroler Pierwszy    : 15, @: 0112-04-02
                    1. Arena Kalaternijska    : 1, @: 0111-02-01
                    1. Laboratoria Dekontaminacyjne    : 1, @: 0112-04-02
                    1. Sektor 22    : 1, @: 0111-07-27
                        1. Dom Uciech Wszelakich    : 1, @: 0111-07-27
                        1. Kasyno Stanisława    : 1, @: 0111-07-27
                    1. Sektor 43    : 1, @: 0111-04-13
                        1. Tor wyścigowy ścigaczy    : 1, @: 0111-04-13
                    1. Sektor 57    : 1, @: 0111-08-15
                    1. Sektor Cywilny    : 1, @: 0111-08-15
            1. Astoria, Pierścień Zewnętrzny    : 4, @: 0112-09-17
                1. Laboratorium Kranix    : 1, @: 0112-09-17
                1. Stacja Medyczna Atropos    : 1, @: 0112-09-17
            1. Astoria    : 6, @: 0111-07-27
                1. Sojusz Letejski, NW    : 2, @: 0111-03-16
                    1. Ruiniec    : 2, @: 0111-03-16
                        1. Trzeci Raj, okolice    : 1, @: 0111-02-17
                            1. Kopiec Nojrepów    : 1, @: 0111-02-17
                        1. Trzeci Raj    : 1, @: 0111-03-16
                1. Sojusz Letejski    : 4, @: 0111-07-27
                    1. Aurum    : 2, @: 0111-07-27
                        1. Pięciokąt Ichtis    : 1, @: 0085-09-06
                            1. Powiat Dzikiej Sieci    : 1, @: 0085-09-06
                            1. Powiat Pentalis    : 1, @: 0085-09-06
                                1. Pałac dla gości    : 1, @: 0085-09-06
                        1. Wielkie Księstwo Aktenir    : 1, @: 0111-07-27
                            1. Pałac Jasnego Ognia    : 1, @: 0111-07-27
                    1. Przelotyk    : 2, @: 0079-09-16
                        1. Przelotyk Wschodni    : 2, @: 0079-09-16
                            1. Cieniaszczyt, podziemia    : 1, @: 0079-09-16
                                1. Leyline    : 1, @: 0079-09-16
                            1. Cieniaszczyt    : 2, @: 0079-09-16
                                1. Kliniki Czarnego Światła    : 1, @: 0079-09-16
                                1. Knajpka Szkarłatny Szept    : 1, @: 0079-09-16
                                1. Mrowisko    : 1, @: 0079-09-16
                                1. Teatr Posępny    : 1, @: 0075-09-21
                        1. Wojnowiec    : 1, @: 0075-09-21
                            1. Klamża    : 1, @: 0075-09-21
                                1. Knajpa Wygrana    : 1, @: 0075-09-21
                                1. Tor Wyścigowy    : 1, @: 0075-09-21
            1. Brama Kariańska    : 4, @: 0112-03-23
            1. Krwawa Baza Piracka    : 1, @: 0112-03-09
            1. Libracja Lirańska    : 1, @: 0111-07-08
            1. Pas Omszawera    : 1, @: 0110-10-04
                1. Kolonia Samojed    : 1, @: 0110-10-04
                    1. Stacja Astropociągów    : 1, @: 0110-10-04
                    1. Zona Czarna    : 1, @: 0110-10-04
                    1. Zona Mieszkalna    : 1, @: 0110-10-04
            1. Pas Teliriański    : 3, @: 0111-11-12
                1. Planetoidy Kazimierza    : 3, @: 0111-11-12
                    1. Planetoida Asimear    : 3, @: 0111-11-12
                        1. Stacja Lazarin    : 2, @: 0111-11-12
            1. Stocznia Kariańska    : 1, @: 0112-03-23
        1. Sektor Mevilig    : 3, @: 0112-03-26
            1. Chmura Piranii    : 2, @: 0112-03-23
            1. Keratlia    : 1, @: 0112-03-23
            1. Planetoida Kalarfam    : 2, @: 0112-03-26
        1. Sektor Noviter    : 1, @: 0112-03-16
        1. Zagubieni w Kosmosie    : 3, @: 0111-05-19
            1. Crepuscula    : 3, @: 0111-05-19
                1. Pasmo Zmroku    : 3, @: 0111-05-19
                    1. Baza Noktiańska Zona Tres    : 3, @: 0111-05-19
                    1. Podwodna Brama Eteryczna    : 1, @: 0111-05-13

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 44 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200819-sekrety-orbitera-historia-prawdziwa; 200822-gdy-arianna-nie-patrzy; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210108-ratunkowa-misja-goldariona; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210209-wolna-tai-na-k1; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 220610-ratujemy-porywaczy-eleny; 220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 240102-zaloga-vishaera-przezyje; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze; 240204-skazona-symulacja-tabester)) |
| Arianna Verlen       | 33 | ((200106-infernia-martyn-hiwasser; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 220610-ratujemy-porywaczy-eleny)) |
| Eustachy Korkoran    | 29 | ((200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 220610-ratujemy-porywaczy-eleny)) |
| Elena Verlen         | 25 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210108-ratunkowa-misja-goldariona; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 220610-ratujemy-porywaczy-eleny; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Leona Astrienko      | 14 | ((200408-o-psach-i-krysztalach; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210106-sos-z-haremu; 210209-wolna-tai-na-k1; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 220610-ratujemy-porywaczy-eleny)) |
| Kamil Lyraczek       | 9 | ((200106-infernia-martyn-hiwasser; 200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 211020-kurczakownia)) |
| Diana Arłacz         | 7 | ((201104-sabotaz-swini; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| OO Infernia          | 7 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Fabian Korneliusz    | 6 | ((220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 240102-zaloga-vishaera-przezyje)) |
| OO Serbinius         | 6 | ((220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 240204-skazona-symulacja-tabester)) |
| Otto Azgorn          | 6 | ((210421-znudzona-zaloga-inferni; 210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Antoni Kramer        | 5 | ((200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert)) |
| Anastazja Sowińska   | 4 | ((200909-arystokratka-w-ladowni-na-swinie; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec)) |
| Helmut Szczypacz     | 4 | ((230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 240102-zaloga-vishaera-przezyje)) |
| Janus Krzak          | 4 | ((210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej)) |
| Jolanta Kopiec       | 4 | ((210904-broszka-dla-eternianki; 210918-pierwsza-bitwa-martyna; 210922-ostatnia-akcja-bohaterki; 211016-zlamane-serce-martyna)) |
| Tadeusz Ursus        | 4 | ((200819-sekrety-orbitera-historia-prawdziwa; 200822-gdy-arianna-nie-patrzy; 200826-nienawisc-do-swin; 210428-infekcja-serenit)) |
| Tomasz Sowiński      | 4 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni)) |
| Aleksandra Termia    | 3 | ((200826-nienawisc-do-swin; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica)) |
| Anastazy Termann     | 3 | ((230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 240102-zaloga-vishaera-przezyje)) |
| Flawia Blakenbauer   | 3 | ((210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Izabela Zarantel     | 3 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 210922-ostatnia-akcja-bohaterki)) |
| Jolanta Sowińska     | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Kalina Rota d'Kopiec | 3 | ((210904-broszka-dla-eternianki; 210918-pierwsza-bitwa-martyna; 211016-zlamane-serce-martyna)) |
| Olgierd Drongon      | 3 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210922-ostatnia-akcja-bohaterki)) |
| TAI Rzieza d'K1      | 3 | ((210209-wolna-tai-na-k1; 211013-szara-nawalnica; 211027-rzieza-niszczy-infernie)) |
| Adela Kołczan        | 2 | ((210912-medyk-przeciwko-cieniaszczytowi; 211016-zlamane-serce-martyna)) |
| Adrian Kozioł        | 2 | ((210912-medyk-przeciwko-cieniaszczytowi; 211016-zlamane-serce-martyna)) |
| AK Nocna Krypta      | 2 | ((201216-krypta-i-wyjec; 201230-pulapka-z-anastazji)) |
| AK Serenit           | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Anastazja Sowińska Dwa | 2 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia)) |
| Damian Orion         | 2 | ((200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Dominik Ogryz        | 2 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza)) |
| Leszek Kurzmin       | 2 | ((200819-sekrety-orbitera-historia-prawdziwa; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Maria Naavas         | 2 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia)) |
| Marian Tosen         | 2 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni)) |
| Martyna Bianistek    | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| OE Falołamacz        | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| OO Tivr              | 2 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni)) |
| Persefona d'Infernia | 2 | ((210421-znudzona-zaloga-inferni; 210428-infekcja-serenit)) |
| Raoul Lavanis        | 2 | ((210707-po-drugiej-stronie-bramy; 220610-ratujemy-porywaczy-eleny)) |
| Roland Sowiński      | 2 | ((210512-ewakuacja-z-serenit; 210922-ostatnia-akcja-bohaterki)) |
| SCA Płetwal Błękitny | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Tal Marczak          | 2 | ((210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy)) |
| Vigilus Mevilig      | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Wioletta Keiril      | 2 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza)) |
| Achellor Santorinus  | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Adalbert Brześniak   | 1 | ((210209-wolna-tai-na-k1)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Adam Permin          | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Aida Serenit         | 1 | ((210512-ewakuacja-z-serenit)) |
| AK Rodivas           | 1 | ((201230-pulapka-z-anastazji)) |
| AK Wyjec             | 1 | ((201216-krypta-i-wyjec)) |
| Aleksander Leszert   | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Amadeusz Martel d'Kopiec | 1 | ((211016-zlamane-serce-martyna)) |
| Andrea Burgacz       | 1 | ((210209-wolna-tai-na-k1)) |
| Ataienne             | 1 | ((201104-sabotaz-swini)) |
| BIA Solitaria d'Zona Tres | 1 | ((210721-pierwsza-bia-mag)) |
| BIA XXX d'Zona Tres  | 1 | ((210714-baza-zona-tres)) |
| Bogdan Anatael       | 1 | ((210512-ewakuacja-z-serenit)) |
| Bożena Kokorobant    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Dagmara Kamyk        | 1 | ((210904-broszka-dla-eternianki)) |
| Dariusz Krantak      | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Dawid Aximar         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Eleonora Perłamila   | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Emilia Ibris         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Ewaryst Kajman       | 1 | ((240204-skazona-symulacja-tabester)) |
| Feliks Przędz        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Feliks Walrond       | 1 | ((210526-morderstwo-na-inferni)) |
| Franciszek Tocz      | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Gilbert Bloch        | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Gunnar Brunt         | 1 | ((201125-wyscig-jako-randka)) |
| Halina Szkwalnik     | 1 | ((201111-mychainee-na-netrahinie)) |
| Henryk Sowiński      | 1 | ((201230-pulapka-z-anastazji)) |
| Henryk Urkon         | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Hestia d'Atropos     | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Horacy Aktenir       | 1 | ((210106-sos-z-haremu)) |
| Igor Arłacz          | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Irena Czakram        | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Iskander Matorin     | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Jakub Bulgocz        | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Janet Erwon          | 1 | ((200115-pech-komodora-ogryza)) |
| Janusz Parzydeł      | 1 | ((210209-wolna-tai-na-k1)) |
| Jonasz Staran        | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Julia Aktenir        | 1 | ((210106-sos-z-haremu)) |
| Julia Myrczek        | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Kamil Burgacz        | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Kamil Frederico      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Karol Brinik         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Karol Reichard       | 1 | ((210526-morderstwo-na-inferni)) |
| Kasia Pieprz         | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Klaudiusz Zanęcik    | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Konstanty Keksik     | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Kosmicjusz Tanecznik Diakon | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Kurator Sarkamair    | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Lara Linven          | 1 | ((240204-skazona-symulacja-tabester)) |
| Leo Mikirnik         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Llarnagraht          | 1 | ((210414-dekralotyzacja-asimear)) |
| Łucja Larnecjat      | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Malictrix d'Pandora  | 1 | ((210414-dekralotyzacja-asimear)) |
| Mariusz Tubalon      | 1 | ((210414-dekralotyzacja-asimear)) |
| Marta Keksik         | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Mateusz Sowiński     | 1 | ((200415-sabotaz-miecza-swiatla)) |
| Medea Sowińska       | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Miki Katasair        | 1 | ((210209-wolna-tai-na-k1)) |
| Miranda Termann      | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Mojra Karstall       | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Morrigan d'Tirakal   | 1 | ((210421-znudzona-zaloga-inferni)) |
| Nikodem Dewiremicz   | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| OA Bakałarz          | 1 | ((210120-sympozjum-zniszczenia)) |
| OA Odkupienie        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OA Zguba Tytanów     | 1 | ((201230-pulapka-z-anastazji)) |
| OE Piękna Elena      | 1 | ((210428-infekcja-serenit)) |
| Ola Klamka           | 1 | ((210904-broszka-dla-eternianki)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Oliwia Pietrova      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| OO Alaya             | 1 | ((210519-osiemnascie-oczu)) |
| OO Galaktyczny Tucznik | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| OO Invictus          | 1 | ((210918-pierwsza-bitwa-martyna)) |
| OO Itorwien          | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Netrahina         | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Żelazko           | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| OOn Tabester         | 1 | ((240204-skazona-symulacja-tabester)) |
| Pandemiusz Diakon    | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Patryk Samszar       | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Paweł Pieprz         | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Perdius Aximar       | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Rafał Armadion       | 1 | ((201104-sabotaz-swini)) |
| Rafał Grambucz       | 1 | ((210428-infekcja-serenit)) |
| Remigiusz Błyszczyk  | 1 | ((211013-szara-nawalnica)) |
| Robert Garwen        | 1 | ((201104-sabotaz-swini)) |
| Roberto Santorinus   | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Romana Arnatin       | 1 | ((210721-pierwsza-bia-mag)) |
| Rozalia Teirik       | 1 | ((210106-sos-z-haremu)) |
| Rufus Komczirp       | 1 | ((201111-mychainee-na-netrahinie)) |
| Rufus Niegnat        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| SC Hektor 17         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| SC Karnaxian         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| SCA Goldarion        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Sebastian Alarius    | 1 | ((200826-nienawisc-do-swin)) |
| Sebastian Namczek    | 1 | ((200408-o-psach-i-krysztalach)) |
| Semla d'Goldarion    | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| SL Rajasee Bagh      | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| SL Uśmiechnięta      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Sonia Ogryz          | 1 | ((200115-pech-komodora-ogryza)) |
| SP Plugawy Jaszczur  | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Stefan Jamniczek     | 1 | ((201118-anastazja-bohaterka)) |
| Sylwia Milarcz       | 1 | ((200822-gdy-arianna-nie-patrzy)) |
| Szczepan Kutarcjusz  | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Szczepan Olgrod      | 1 | ((200106-infernia-martyn-hiwasser)) |
| Tadeusz Arkaladis    | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| TAI Ashtaria d'Tabester | 1 | ((240204-skazona-symulacja-tabester)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| TAI Neita Lairossa   | 1 | ((210209-wolna-tai-na-k1)) |
| Teodor Margrabarz    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Ulisses Kalidon      | 1 | ((210714-baza-zona-tres)) |
| Wanessa Ogarek       | 1 | ((211016-zlamane-serce-martyna)) |
| Wolfgang Sępiarz     | 1 | ((240102-zaloga-vishaera-przezyje)) |