# Medea Sowińska
## Identyfikator

Id: 9999-medea-sowińska

## Sekcja Opowieści

### Stabilizacja Bramy Eterycznej

* **uid:** 210901-stabilizacja-bramy-eterycznej, _numer względny_: 5
* **daty:** 0112-02-09 - 0112-02-12
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Gilbert Bloch, Janus Krzak, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Medea Sowińska, OO Kanagar, OO Netrahina, OO Trasman, Seweryn Atanair

Streszczenie:

Infernia wróciła z innymi jednostkami naprawić Bramę Kariańską. Eustachy ufortyfikował i uniewidocznił pintki; Arianna pozyskała dane z reanimowanego magią krążownika Orbitera. Przez Bramę przeszła ON Catarina; Klaudia ją zlokalizowała i wystawiła Netrahinie i Niobe. Niestety, Arianna hiperlinkowała się z ON Spatium Gelida. Dla nich minęło tylko kilka godzin, dla nas - kilkadziesiąt lat...

Aktor w Opowieści:

* Dokonanie:
    * na szybko naprawiła systemy Inferni i dostarczyła sporo sprzętu i wiedzy. Po raz pierwszy BARDZO współpracuje by ratować Bramę Kariańską.


### Uszkodzona Brama Eteryczna

* **uid:** 210825-uszkodzona-brama-eteryczna, _numer względny_: 4
* **daty:** 0112-02-04 - 0112-02-07
* **obecni:** AK Nocna Krypta, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Gilbert Bloch, Klaudia Stryk, Medea Sowińska, ON Spatium Gelida, OO Infernia, OO Mfumo

Streszczenie:

Infernia została przechwycona przez Medeę lecąc na Anomalię Kolapsu i przekierowana na uszkodzoną Bramę Eteryczną (gdzie, jak się okazuje, są pasożyty i flota noktiańska która nie doleciała). Na miejscu Infernia uniknęła efemeryd i channelując Kryptę uratowała wszystkich efemerydami. Gdy Krypta pojawiła się na serio, Infernia zwiała paląc silniki. Statki noktiańskie w Bramie mają tether na Infernię.

Aktor w Opowieści:

* Dokonanie:
    * nie mając nikogo kto może pomóc w naprawieniu Eterycznej Bramy zrekrutowała Infernię (bo ekranowanie). Musiała wyznać prawdę o pasożytach Bramy i przyczynach tego, że Infernię "porwało" przez Bramę całkiem niedawno.


### Porwanie cywila z Kokitii

* **uid:** 200429-porwanie-cywila-z-kokitii, _numer względny_: 3
* **daty:** 0111-10-01 - 0111-10-06
* **obecni:** Alara Ehmes, Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Medea Sowińska

Streszczenie:

Infernia jakoś nie może zostać naprawiona a skarga Mateusza Sowińskiego nie pomaga. Po śledztwie Zespołu okazuje się, że Infernia jest kanibalizowana przez Niobe, do jakiejś misji sił specjalnych. Arianna z ekipą wkręciła się na tą misję, przekonując Medeę Sowińską, że bez niej to się nie uda. Misja polegała na ataku na Kokitię pomiędzy Bramami i ekstrakcja z Kokitii cywila, udając piratów. Dzięki załodze Inferni udało się to lepiej niż ktokolwiek mógłby się spodziewać i niż Medea zaplanowała sama.

Aktor w Opowieści:

* Dokonanie:
    * zintegrowana z Niobe agentka sił specjalnych Orbitera (frakcja "Gorący Lód"). Zaakceptowała współpracę z Arianną i zdecydowała się ją wzmocnić.


### Elwira, koszmar Nox Ignis

* **uid:** 230102-elwira-koszmar-nox-ignis, _numer względny_: 2
* **daty:** 0082-08-02 - 0082-08-05
* **obecni:** Aida Liminis, AK Nox Ignis, Aletia Nix, Brunon Szwagacz, Dominik Łarnisz, Franz Szczypiornik, Medea Sowińska, OO Loricatus, Riaon Diralik, Talia Derwisz, Tatiana Ozariat, Tristan Ozariat

Streszczenie:

Orbiter jest uważany za winnego zniknięcia ładnych noktianek a nastroje na Ratio Spei są fatalne. Okazuje się, że dwie ze znikniętych noktianek są 'specjalne' - jedna jest chyba protomagiem, druga jest aktywatorką serpentisów. Okazuje się, że chyba Orbiter faktycznie porywa lokalne noktianki, co się Franzowi bardzo nie podoba. Talia infiltruje Tristana i okazuje się, że ów jest wrakiem człowieka, pożeranym przez echo Nox Ignis. Przy okazji, Talia chce znaleźć miejsce dla Riaona (noktiańskiego nastolatka) i złapała link z noktiańskim oficerem porządkowym.

Aktor w Opowieści:

* Dokonanie:
    * przeanalizowała listę znikniętych dziewczyn. Odkryła dwie nietypowe noktianki.


### Dowody na istnienie Nox Ignis

* **uid:** 221230-dowody-na-istnienie-nox-ignis, _numer względny_: 1
* **daty:** 0082-07-28 - 0082-08-01
* **obecni:** AK Nox Ignis, Aletia Nix, Dominik Łarnisz, Franz Szczypiornik, Katrina Komczirp, Medea Sowińska, OO Loricatus, Persefona d'Loricatus, Sarian Xadaar, Talia Derwisz, Tatiana Ozariat, Tristan Ozariat, Wawrzyn Rewemis

Streszczenie:

Stacja CON Ratio Spei jest wypełniona resztkami złamanych noktian i aż ocieka pesymizmem i brakiem nadziei. Gdzieś na stacji Katrina Komczirp odnalazła sekret anomalnej superbroni Noctis, Nox Ignis i wpadła w kłopoty. Załoga Loricatus ma zadanie wyciągnąć Katrinę i poznać sekret Nox Ignis. Talia i Dominik zinfiltrowali Ratio Spei i się zorientowali w sytuacji. Jako łowcy nagród mają pierwszy plan - poszukać Katriny u Tristana Ozariata.

Aktor w Opowieści:

* Dokonanie:
    * 18-letnia czarodziejka, ekspert komunikacji Loricatus, zimna i formalna z nastawieniem do dominacji i bezduszności. Zabiła co najmniej 1 osobę strzelając jej w tył głowy. Zintegrowała swoją sentisieć z resztką sentisieci Talii dając im perfekcyjną komunikację.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0112-02-12
    1. Nierzeczywistość    : 1, @: 0111-10-06
    1. Primus    : 5, @: 0112-02-12
        1. Sektor Astoriański    : 5, @: 0112-02-12
            1. Astoria, Orbita    : 1, @: 0111-10-06
                1. Kontroler Pierwszy    : 1, @: 0111-10-06
            1. Brama Kariańska    : 3, @: 0112-02-12
            1. Brama Trzypływów    : 1, @: 0111-10-06
            1. Libracja Lirańska    : 2, @: 0082-08-05
                1. Anomalia Kolapsu, orbita    : 2, @: 0082-08-05
                    1. CON Ratio Spei    : 2, @: 0082-08-05
                        1. Brzuchowisko    : 1, @: 0082-08-01
                        1. Rdzeń    : 2, @: 0082-08-05
                            1. Mostek    : 1, @: 0082-08-05
                            1. Starport    : 1, @: 0082-08-05
                        1. Torus    : 1, @: 0082-08-05
                            1. Brzuchowisko    : 1, @: 0082-08-05
                                1. Bar Proch Strzelniczy    : 1, @: 0082-08-05
                                1. Bazarek    : 1, @: 0082-08-05

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 3 | ((200429-porwanie-cywila-z-kokitii; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Eustachy Korkoran    | 3 | ((200429-porwanie-cywila-z-kokitii; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Klaudia Stryk        | 3 | ((200429-porwanie-cywila-z-kokitii; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| AK Nox Ignis         | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Aletia Nix           | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Dominik Łarnisz      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Elena Verlen         | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Flawia Blakenbauer   | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Franz Szczypiornik   | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Gilbert Bloch        | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Leona Astrienko      | 2 | ((200429-porwanie-cywila-z-kokitii; 210901-stabilizacja-bramy-eterycznej)) |
| OO Loricatus         | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Talia Derwisz        | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Tatiana Ozariat      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Tristan Ozariat      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Aida Liminis         | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| AK Nocna Krypta      | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Alara Ehmes          | 1 | ((200429-porwanie-cywila-z-kokitii)) |
| Antoni Kramer        | 1 | ((200429-porwanie-cywila-z-kokitii)) |
| Brunon Szwagacz      | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Diana d'Infernia     | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Janus Krzak          | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Katrina Komczirp     | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Martyn Hiwasser      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Infernia          | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Netrahina         | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Persefona d'Loricatus | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Riaon Diralik        | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Sarian Xadaar        | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Wawrzyn Rewemis      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |