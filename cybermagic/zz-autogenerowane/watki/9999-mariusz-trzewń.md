# Mariusz Trzewń
## Identyfikator

Id: 9999-mariusz-trzewń

## Sekcja Opowieści

### Zgubiony holokryształ w lesie

* **uid:** 210817-zgubiony-holokrysztal-w-lesie, _numer względny_: 19
* **daty:** 0111-06-22 - 0111-06-24
* **obecni:** Alan Bartozol, Cyryl Perikas, Kacper Bankierz, Laura Tesinik, Liliana Bankierz, Mariusz Trzewń, Marysia Sowińska, Rupert Mysiokornik, Stella Armadion, Tomasz Tukan

Streszczenie:

Jeden z przerzucanych przez mafię przedmiotów do Aurum to ko-matryca Kuratorów, jako broń. Ale Cyryl - kurier - był ofiarą terminuskiego polowania więc ją odrzucił w Lesie Trzęsawnym gdzie znalazła ją Stella przekonana, że to noktiańska technologia. Marysia wypchnęła Kacpra Bankierza z mafii (i tego terenu) a Tukan znalazł i usunął ko-matrycę m.in. używając Laury.

Aktor w Opowieści:

* Dokonanie:
    * pomógł Tukanowi dowiedzieć się czym jest ten dziwny kryształ - i wykrył ko-matrycę Kuratorów.


### Porwanie Daniela Terienaka

* **uid:** 210720-porwanie-daniela-terienaka, _numer względny_: 18
* **daty:** 0111-06-14 - 0111-06-16
* **obecni:** Daniel Terienak, Karolina Terienak, Mariusz Trzewń, Marysia Sowińska, Rafał Torszecki, Tomasz Tukan, Urszula Miłkowicz

Streszczenie:

Urszula Miłkowicz zwabiła w pułapkę i porwała Daniela Terienaka - z nadzieją, że odkryje Rekina który współpracuje z mafią. Siostra Daniela, Karolina, pobiła Torszeckiego i tak spotkała się z Marysią. Wspólnie odkryły że za wszystkim stoi Ula i zmusiły ją do oddania Daniela bez przeskanowania pamięci (by się upewnić, Karolina wysadziła techbunker). Marysia dowiedziała się, że Ula jest uciekinierką z Aurum i eks-protomagiem.

Aktor w Opowieści:

* Dokonanie:
    * wspiera Ulę Miłkowicz. Jest jej "terminusem-opiekunem", acz nieformalnym. Przy okazji, zna kody do różnych mechanicznych potworności powojennych w Lesie Trzęsawnym.


### Haracz w parku rozrywki

* **uid:** 200913-haracz-w-parku-rozrywki, _numer względny_: 17
* **daty:** 0110-10-09 - 0110-10-10
* **obecni:** Adam Janor, Alan Bartozol, Diana Tevalier, Franciszek Owadowiec, Karol Senonik, Krystyna Senonik, Maja Janor, Mariusz Trzewń, Melinda Teilert, Pięknotka Diakon, Rafał Kamaron

Streszczenie:

Łatwa misja w okolicach parku rozrywki Janor przekształciła się w dziwną operację z pasożytem magicznym dotykającym pobierającego haracz Grzymościowca i dziwnie zachowującą się komendant policji. Pięknotka i Alan rozwalili siły Grzymościa, ale nie są pewni, czy to nie była tylko dywersja... (a tymczasem Melinda i Chevaleresse świetnie się bawiły w parku rozrywki).

Aktor w Opowieści:

* Dokonanie:
    * znalazł dla Pięknotki łatwą misję, która okazała się być dość ostra; nie dał rady odnaleźć ludzi od Grzymościa, ale wykrył Pasożyta w magu.


### Dom dla Melindy

* **uid:** 200524-dom-dla-melindy, _numer względny_: 16
* **daty:** 0110-09-13 - 0110-09-16
* **obecni:** Diana Lemurczak, Diana Tevalier, Franek Bulterier, Gerwazy Lemurczak, Laura Tesinik, Mariusz Trzewń, Melinda Teilert, Pięknotka Diakon

Streszczenie:

Rodzina chce Melindę z powrotem, Melinda nie chce wracać. Pięknotka szuka Melindy i znalazła ją w Arkadii. Gdy poznała sytuację, we współpracy z Dianą wsadziła Melindę Alanowi przy użyciu Chevaleresse. Wszyscy są zadowoleni, choć Melinda musi opuścić Podwiert - ale wreszcie jest bezpieczna.

Aktor w Opowieści:

* Dokonanie:
    * nigdy nie był w Podwiercie bo to "miasto gangów" a on nie umie walczyć. Pięknotka wzięła go na kabaret w Arkadii. Zainicjował szukanie Melindy.


### Tajna baza Orbitera?

* **uid:** 200510-tajna-baza-orbitera, _numer względny_: 15
* **daty:** 0110-09-07 - 0110-09-11
* **obecni:** Alina Anakonda, Artur Kołczond, Gabriel Ursus, Ignacy Myrczek, Kallista Exolon, Laura Tesinik, Mariusz Trzewń, Natalia Tessalon, Pięknotka Diakon, Sabina Kazitan, Talarand d'Irrydius, Teresa Mieralit, Tymon Grubosz

Streszczenie:

Natalia Tessalon chce przetransportować rannego brata do Aurum, ale manewr zablokowała Laura (terminuska-prawniczka) na prośbę Pięknotki. Natalia i Sabina się ścięły, przez co ucierpiał Myrczek a Sabina trafiła do aresztu. Teresa Mieralit zapewni, że Myrczek nie ma głowy do Sabiny. Pięknotka z Tymonem poszukali kto ciężko zmiażdżył Gabriela; udało się im znaleźć tajną bazę Orbitera w której jest Emulatorka, która się do tego przyznała.

Aktor w Opowieści:

* Dokonanie:
    * przegrzebywał się przez dokumenty Gabriela, gdzie był ślad dla Pięknotki. Gabriel zdecydowanie ich przecenił - nic nie znalazł. Ale pomógł nakierować na awiana.


### Arystokraci na Trzęsawisku

* **uid:** 200414-arystokraci-na-trzesawisku, _numer względny_: 14
* **daty:** 0110-08-04 - 0110-08-05
* **obecni:** Franciszek Leszczowik, Ignacy Myrczek, Mariusz Trzewń, Pięknotka Diakon, Sabina Kazitan, Strażniczka Alair, Talia Aegis

Streszczenie:

Franciszek z Aurum dostosował Sabinę do swoich upodobań jako seksbombę. Sabina zdecydowała się go zniszczyć - doprowadziła go do Wysysacza Esuriit i wskazała, że na Trzęsawisku może zdobyć coś co mu się przyda. Franciszek stracił część świty na Trzęsawisku; gdyby nie Pięknotka, zginąłby tam. A tak tylko skończył z traumą a Sabinie się upiekło - roboty społeczne na 2 tygodnie.

Aktor w Opowieści:

* Dokonanie:
    * odkrył, że seksbomba-arystokratka to Sabina Kazitan używając systemów analitycznych Pustogoru.


### Krucjata Chevaleresse

* **uid:** 200202-krucjata-chevaleresse, _numer względny_: 13
* **daty:** 0110-07-24 - 0110-07-27
* **obecni:** Ataienne, Damian Orion, Diana Tevalier, Keraina d'Orion, Malictrix d'Itaran, Mariusz Trzewń, Mateusz Kardamacz, Minerwa Metalia, Pięknotka Diakon, Talia Aegis, Tomasz Tukan

Streszczenie:

Chevaleresse wykradła broń Alana i uzbroiła Liberitias, gdzie działa też Ataienne przeciw Kardamaczowi, Grzymościowi i krzywdzicielowi Alana. Pięknotka doszła do serca problemu i materii, acz potrzebowała wsparcia Orbitera - dokładniej, Kerainy. Dzięki Kerainie wprowadziła Malictrix do Kompleksu Itaran. Teraz to tylko kwestia czasu aż Grzymość zostanie zniszczony.

Aktor w Opowieści:

* Dokonanie:
    * nerd taktyki i gryzipiórek; doszedł do tego, że nagrania wskazują, że napastnikiem bazy Grzymościa jest TAI poziomu ponad 3. Pięknotce jest zimno.


### Ukradziony Entropik

* **uid:** 191201-ukradziony-entropik, _numer względny_: 12
* **daty:** 0110-07-13 - 0110-07-15
* **obecni:** Hestia d'Itaran, Keraina d'Orion, Mariusz Trzewń, Minerwa Metalia, Persefona d'Jastrząbiec, Pięknotka Diakon, Roland Grzymość

Streszczenie:

Orbiter szukał informacji o zaginionym dawno Entropiku; Ataienne go wykryła gdy przejęła Jastrząbiec. Pięknotka wraz z miragentem znalazła owego Entropika - jest w ukrytym kompleksie medyczno-badawczym Grzymościa. By nie eskalować i nie ryzykować ludzkich żyć, doprowadziła do ewakuacji Entropika. Dzięki temu Orbiter musiał poradzić sobie sam. A Pustogor stanął przed tym, że Grzymość ma potężnych sojuszników i potężną bazę w Jastrząbcu.

Aktor w Opowieści:

* Dokonanie:
    * pomógł Pięknotce zlokalizować kompleks Itaran używając matematyki, triangulacji i znajomości z odpowiednimi magami. Niestety, przez to był wyciek.


### Korupcja z arystokratki

* **uid:** 191112-korupcja-z-arystokratki, _numer względny_: 11
* **daty:** 0110-07-04 - 0110-07-05
* **obecni:** Ataienne, Ignacy Myrczek, Mariusz Trzewń, Minerwa Metalia, Persefona d'Jastrząbiec, Pięknotka Diakon, Sabina Kazitan, Tadeusz Sklerzec

Streszczenie:

Po wydarzeniach z Grzymościem oczy Pięknotki padły na Jastrząbiec - tam dzieją się dziwne rzeczy z narkotykami. Okazało się, że to Ignacy Myrczek próbuje rozpaczliwie udowodnić arystokratce z Aurum, że nie zrobił jej krzywdy świadomie a pod wpływem narkotyków. Pięknotka wpadła (z pomocą Ataienne) i zrobiła porządek - ukarała burmistrza, Myrczka, arystokratkę i w sumie wszystkich. Ma smak popiołu w ustach, tu nie było nikogo sensownego...

Aktor w Opowieści:

* Dokonanie:
    * rozpaczliwie próbował być przydatny i odkupić się u Karli, więc znalazł Pięknotce Jastrząbiec. Dzięki Pięknotce, Karla zdjęła go z 'listy-osób-do-opieprzania'.


### Zaginiona soniczka

* **uid:** 191105-zaginiona-soniczka, _numer względny_: 10
* **daty:** 0110-07-02 - 0110-07-03
* **obecni:** Ataienne, Erwin Galilien, Mariola Tralment, Mariusz Trzewń, Mateusz Kardamacz, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Ataienne poprosiła Pięknotkę o znalezienie zaginionej fanki. Pięknotka zaczęła śledztwo i doszła do tego, że Mariola została porwana przez gang przerzucający dziewczyny do Cieniaszczytu. Erwin słyszał takie plotki. Pięknotka zlokalizowała i usunęła komórkę tego gangu; śledztwo pokazało na autoklub fitness jako jedno interesujące miejsce. Najpewniej nie to jest miejscem "zła", ale warto sprawdzić.

Aktor w Opowieści:

* Dokonanie:
    * na prośbę Pięknotki sprawdził silnym sygnałem obecność Marioli, przez co rozwalił Karli jedno tajne śledztwo. ALE - uratowali pięć dziewczyn. Więc w sumie Karla go nie zabije.


### Kontrpolowanie Pięknotki - pułapka

* **uid:** 191103-kontrpolowanie-pieknotki-pulapka, _numer względny_: 9
* **daty:** 0110-06-24 - 0110-07-01
* **obecni:** Damian Orion, Józef Małmałaz, Ksenia Kirallen, Lilia Ursus, Marek Puszczok, Mariusz Trzewń, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon, Rafał Roszczeniok

Streszczenie:

Pięknotka poluje na noktiańskiego zabójcę. W ciągu tygodnia udało jej się złożyć do kupy pułapkę, rozpocząć linię przerzutową artefaktów do Aurum, połączyć Orbiter, Lilię, Minerwę oraz Pustogor i doprowadzić do tego, że docelowo Małmałaz ma zostać złapany i doprowadzony do Pustogoru w zemście za krzywdę Alana. To był bardzo trudny tydzień, politycznie.

Aktor w Opowieści:

* Dokonanie:
    * analityk Pięknotki; powiedział jej, że Ksenia ma lokalizację - gdzieś w Aurum. Pomógł Pięknotce wyłączyć problemy z Ksenią.


### Polowanie na Pięknotkę

* **uid:** 190901-polowanie-na-pieknotke, _numer względny_: 8
* **daty:** 0110-06-21 - 0110-06-24
* **obecni:** Alan Bartozol, Diana Tevalier, Józef Małmałaz, Lucjusz Blakenbauer, Mariusz Trzewń, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Noktiański zabójca chciał pozyskać Cienia; pokonał Pięknotkę, ale Alan go zestrzelił. Potem zabójca złapał Chevaleresse i korzystając z niej jako zakładniczki wysłał Alana do szpitala. Pięknotka doszła do tego, jak się nazywa z pomocą Minerwy i Cienia.

Aktor w Opowieści:

* Dokonanie:
    * organizował kampanię do tępienia noktian w enklawach; przekonany przez Pięknotkę że to zły pomysł


### Wypadek w Kramamczu

* **uid:** 190906-wypadek-w-kramamczu, _numer względny_: 7
* **daty:** 0110-06-18 - 0110-06-19
* **obecni:** Dariusz Kuromin, Kasjopea Maus, Ksenia Kirallen, Mariusz Trzewń, Pięknotka Diakon

Streszczenie:

Wrabiają Wiktora Sataraila w atak na niewielką firmę, Włóknin z Kramamczy! Pięknotka i Ksenia wysłane by to naprawić! Pięknotka przyzwała z powrotem wszystkie królowe które pouciekały a Ksenia odkryła sabotaż wewnętrzny. Dariusz próbuje wyciągnąć ubezpieczenie. Pięknotka wezwała Kasjopeę (dziennikarkę) do pomocy i przekonała Ksenię, żeby ta odpuściła Dariuszowi tylko znalazła osoby winne tej sytuacji i prowokacji.

Aktor w Opowieści:

* Dokonanie:
    * analityk Barbakanu Pustogoru; powiedział Pięknotce o sytuacji finansowej Włóknina. Innymi słowy - mają przyczynę.


### Rozpaczliwe ratowanie BII

* **uid:** 190827-rozpaczliwe-ratowanie-bii, _numer względny_: 6
* **daty:** 0110-01-18 - 0110-01-21
* **obecni:** BIA Tarn, Ernest Kajrat, Marek Puszczok, Mariusz Trzewń, Pięknotka Diakon, Sławomir Niejadek, Talia Aegis, Tymon Grubosz

Streszczenie:

Talia próbowała utrzymać przy życiu BIA 3 generacji. Nie mając surowców, użyła tej BIA do zdobycia rzeczy odżywczych z Tiamenat - zrzucając winę na Wiktora Sataraila. Pięknotka poszła za śladem i gdy dotarła do Talii, Kajrat wziął na siebie winę (za co Pięknotka go aresztowała). Następnie poprosiła Wiktora Sataraila by ten ochronił swoje dobre imię - i zniszczył śmiertelnie niebezpieczną BIA.

Aktor w Opowieści:

* Dokonanie:
    * taktyk, wsparcie, hacker z Barbakanu; często współpracuje z Pięknotką. Zidentyfikował dla Pięknotki, że pnączoszpony używały noktiańskiej taktyki.


### Księżniczka Arianna ratuje dzieci w lesie

* **uid:** 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie, _numer względny_: 5
* **daty:** 0085-01-26 - 0085-01-28
* **obecni:** Arnulf Poważny, Kaella Sarimanis, Klaudia Stryk, Mariusz Trzewń, Teresa Mieralit, Tymon Grubosz

Streszczenie:

Korzystając z odbudowy po wojnie, napływowa grupa gangsterów polowała na dzieci. Gdy złapali Kaellę, młodą protomag Noctis, ta stworzyła iluzję Księżniczki Arianny z bajek, aby im pomogła. Ta - manifestacja Alucis i Esuriit - porwała dzieci od porywaczy i je schowała. Tymczasem Teresa - noktianka z AMZ - dostała sygnał od 'Księżniczki' i zaczęła szukać kierowana głosami swoich zmarłych rodziców.

Klaudia i Mariusz doszli do tego co się stało i po tym jak osłonili Teresę poszli do dyrektora Arnulfa. Opracowali plan ratunkowy po odkryciu co się stało. Arnulf z Tymonem skupili się na gangsterach, Klaudia i Mariusz poszli po dzieci. Plan zakładał odwrócenie uwagi Drapieżników poprzez stworzenie ataku potworów.

Niestety, Teresa ma flashbacki z czasów wojny a Alucis pokazało jej rodziców. Po wyłączeniu iluzji stworzonej przez Księżniczkę Ariannę, wszyscy musieli uciekać przed manifestacją Esuriit, która zagrażała wszystkich. Udało się im bezpiecznie dotrzeć do terenu AMZ, gdzie dzieci otrzymały niezbędną pomoc medyczną.

Aktor w Opowieści:

* Dokonanie:
    * Stworzył drona stealth i testując go znalazł ludzi polujących na Teresę. Odpędził ich. Badając dronę porywaczy, odkrył, gdzie są i przejął kontrolę nad ich flotą. Podczas akcji ratunkowej, gdy Alucis zmieniło się w Esuriit, magicznie zregenerował zniszczonego APC i ewakuowali się zanim manifestacja ich dorwie.


### Czarodziejka, która jednak może się zabić

* **uid:** 211019-czarodziejka-ktora-jednak-moze-sie-zabic, _numer względny_: 4
* **daty:** 0084-12-20 - 0084-12-24
* **obecni:** Arnulf Poważny, Klaudia Stryk, Ksenia Kirallen, Mariusz Trzewń, Maryla Koternik, Talia Aegis, Teresa Mieralit

Streszczenie:

Nastroje antynoktiańskie się nasilają. Pojawiły się seksboty do bicia, w kształcie noktian i noktianek (co strasznie zestresowało Teresę). Klaudia zaproponowała Arnulfowi powiększenie kadry o Weteranów i przejmując to prawie się zarżnęła (uratował ją Trzewń). Maryla szuka informacji o Teresie - szuka ukrytego noktianina na AMZ, ale Trzewń wykluczył Teresę dzięki dokumentom sformowanym przez Arnulfa. Klaudia i Ksenia pracują nad petycją, by stworzyć Dom Weteranów Noctis.

Aktor w Opowieści:

* Dokonanie:
    * wziął na siebie część roboty Klaudii, po czym zrobił sieć pomocników - w ten sposób odpowiedzialność nie leży na samej Klaudii i wszyscy są w stanie działać. Szukał ukrytego noktianina w systemach i wykluczył Teresę jako noktiankę (co jest całkiem zabawne). Chce usunąć noktian z AMZ.


### Szukaj serpentisa w lesie

* **uid:** 211009-szukaj-serpentisa-w-lesie, _numer względny_: 3
* **daty:** 0084-11-13 - 0084-11-14
* **obecni:** Agostino Karwen, Dariusz Skórnik, Edelmira Neralis, Felicjan Szarak, Grzegorz Czerw, Klaudia Stryk, Ksenia Kirallen, Mariusz Trzewń, Strażniczka Alair, Udom Rapnak

Streszczenie:

Prosta operacja zdekomponowania medbunkra w Lesie Trzęsawnym (2 żołnierze, 4 uczniowie AMZ) skończyła się horrorem - serpentis noktiański porwał Trzewnia i 2 żołnierzy. Klaudia uruchomiła martwy medbunkier i przemyciła dronę do Zaczęstwa a Ksenia była dywersją (co skończyło się dla niej traumą). Mimo sytuacji, Ksenia przekonała serpentisów że jak się nie oddadzą w ręce astorian, jeden z nich umrze. I przekonała ich dzięki wiadomościom Klaudii świadczącym, że Talia współpracuje a nie jest w niewoli Astorii.

Aktor w Opowieści:

* Dokonanie:
    * 19 lat; przechwycony przez serpentisa Agostino w Lesie Trzęsawnym, ale dał radę przekazać zapętloną wiadomość Klaudii magią przez zagłuszający pojazd. Technomanta <3.


### Nowa Strażniczka AMZ

* **uid:** 210926-nowa-strazniczka-amz, _numer względny_: 2
* **daty:** 0084-06-14 - 0084-06-26
* **obecni:** Albert Kalandryk, Arnulf Poważny, Klaudia Stryk, Ksenia Kirallen, Mariusz Trzewń, Strażniczka Alair, Talia Aegis, Tymon Grubosz

Streszczenie:

Klaudia - młoda grzeczna administratorka AMZ - przypadkiem odkrywa konspirację dyrektora, terminusa i noktianki by uratować umierającą Eszarę. Po konfrontacji z dyrektorem pomaga im by wszystko działało. Trzewń uniknął amnestyków.

Aktor w Opowieści:

* Dokonanie:
    * 19 lat; nerd militarny. Rozpaczliwie trzyma się terminusów Pustogoru, bo to jedyne co jest STAŁE po wojnie. Rozchwiany, doskonale się skrada - podsłuchał TYMONA GRUBOSZA (ze wszystkich ludzi). Chce nadać temat terminusom, ale Tymon + Klaudia + Eszara przekonali go, by dał Eszarze szansę.
* Progresja:
    * 19-latkiem będąc, był przekonany o wyższości podejścia Pustogoru nad wolnym strzelcem Tymonem Gruboszem. Nie lubi Tymona.
    * za 10 lat i później będzie cichym sojusznikiem AMZ i Eszary. Ona będzie z nim koordynować nie mówiąc nikomu.


### Czółenkowe Esuriit w AMZ

* **uid:** 211122-czolenkowe-esuriit-w-amz, _numer względny_: 1
* **daty:** 0083-10-13 - 0083-10-22
* **obecni:** Dariusz Drewniak, Ernest Termann, Felicjan Szarak, Klaudia Stryk, Ksenia Kirallen, Mariusz Trzewń, Wiktor Szurmak

Streszczenie:

Wpierw Trzewń wziął Klaudię na randkę (ona tego nie wiedziała) do AI Core AMZ. Złapał ich Szurmak i ukarał. Potem były próby puryfikacji Czółenka przez magów Eterni i terminusów. Skończyło się katastrofą - Agent Esuriit dotarł nawet do AMZ, co doprowadziło do pokazania talentu organizacyjnego Klaudii i śmierci dyrektora Termanna który bronił uczniów. Skrzydło Loris AMZ zostało odcięte i uznane za zakazane. Przez ilość i typy energii przez MIESIĄC nie było zajęć - leczenie, regeneracja, puryfikacja.

Aktor w Opowieści:

* Dokonanie:
    * 18 lat; chciał poderwać Klaudię na Hestię, wkradli się do budynku AMZ i skończył z pasami na gołym tyłku od Szurmaka. Potem gdy zaatakował Agent Esuriit chciał postawić Hestię by mogła pomóc, ale jedynie doprowadził do zniszczenia Hestii i śmierci dyrektora. Nie może sobie tego wybaczyć i wyleczył się z bycia bohaterem.
* Progresja:
    * wyleczył się z heroizmu od teraz do końca życia. Ciężka trauma - przez niego zginął dyrektor, w jego obronie. I Hestia, w którą wierzył do samego końca. Zrozumiał, że nie wszystko da się wygrać.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 19, @: 0111-06-24
    1. Primus    : 19, @: 0111-06-24
        1. Sektor Astoriański    : 19, @: 0111-06-24
            1. Astoria    : 19, @: 0111-06-24
                1. Sojusz Letejski    : 19, @: 0111-06-24
                    1. Szczeliniec    : 19, @: 0111-06-24
                        1. Powiat Jastrzębski    : 5, @: 0110-10-10
                            1. Jastrząbiec, okolice    : 2, @: 0110-07-27
                                1. Containment Chambers    : 1, @: 0110-07-15
                                1. Klinika Iglica    : 2, @: 0110-07-27
                                    1. Kompleks Itaran    : 2, @: 0110-07-27
                                1. TechBunkier Sarrat    : 1, @: 0110-07-27
                            1. Jastrząbiec    : 1, @: 0110-07-05
                                1. Hotel Stacja Kosmiczna    : 1, @: 0110-07-05
                                1. Ratusz    : 1, @: 0110-07-05
                            1. Kalbark    : 2, @: 0110-07-27
                                1. Autoklub Piękna    : 1, @: 0110-07-03
                                1. Escape Room Lustereczko    : 1, @: 0110-07-27
                            1. Praszalek, okolice    : 1, @: 0110-10-10
                                1. Fabryka schronień Defensor    : 1, @: 0110-10-10
                                1. Park rozrywki Janor    : 1, @: 0110-10-10
                            1. Praszalek    : 1, @: 0110-10-10
                                1. Komenda policji    : 1, @: 0110-10-10
                        1. Powiat Pustogorski    : 15, @: 0111-06-24
                            1. Czemerta, okolice    : 1, @: 0110-09-11
                                1. Baza Irrydius    : 1, @: 0110-09-11
                                1. Fortifarma Irrydia    : 1, @: 0110-09-11
                                1. Studnia Irrydiańska    : 1, @: 0110-09-11
                            1. Czółenko    : 3, @: 0111-06-24
                                1. Bunkry    : 2, @: 0110-07-03
                            1. Kramamcz    : 1, @: 0110-06-19
                                1. Włóknin    : 1, @: 0110-06-19
                            1. Podwiert    : 3, @: 0111-06-24
                                1. Dzielnica Luksusu Rekinów    : 2, @: 0111-06-24
                                    1. Obrzeża Biedy    : 1, @: 0111-06-16
                                        1. Stadion Lotników    : 1, @: 0111-06-16
                                    1. Sektor Brudu i Nudy    : 1, @: 0111-06-16
                                        1. Skrytki Czereśniaka    : 1, @: 0111-06-16
                                1. Klub Arkadia    : 1, @: 0110-09-16
                                1. Las Trzęsawny    : 2, @: 0111-06-24
                            1. Pustogor    : 4, @: 0110-09-11
                                1. Eksterior    : 2, @: 0110-09-11
                                    1. Miasteczko    : 2, @: 0110-09-11
                                        1. Knajpa Górska Szalupa    : 1, @: 0110-09-11
                                1. Rdzeń    : 2, @: 0110-09-11
                                    1. Szpital Terminuski    : 2, @: 0110-09-11
                                1. Zamek Weteranów    : 1, @: 0084-06-26
                            1. Zaczęstwo    : 11, @: 0111-06-16
                                1. Akademia Magii, kampus    : 5, @: 0110-08-05
                                    1. Akademik    : 3, @: 0084-12-24
                                    1. Arena Treningowa    : 1, @: 0083-10-22
                                    1. Budynek Centralny    : 1, @: 0083-10-22
                                        1. Piwnica    : 1, @: 0083-10-22
                                            1. Stare AI Core    : 1, @: 0083-10-22
                                        1. Skrzydło Loris    : 1, @: 0083-10-22
                                            1. Laboratorium Wysokich Energii    : 1, @: 0083-10-22
                                            1. Nieskończony Labirynt    : 1, @: 0083-10-22
                                    1. Domek dyrektora    : 1, @: 0084-12-24
                                    1. Złomiarium    : 1, @: 0084-06-26
                                1. Arena Migświatła    : 1, @: 0084-06-26
                                1. Kompleks Tiamenat    : 1, @: 0110-01-21
                                1. Las Trzęsawny    : 2, @: 0085-01-28
                                    1. Medbunkier Sigma    : 1, @: 0084-11-14
                                1. Nieużytki Staszka    : 3, @: 0110-07-03
                                1. Osiedle Ptasie    : 3, @: 0111-06-16
                        1. Pustogor    : 1, @: 0110-07-01
                        1. Trzęsawisko Zjawosztup    : 1, @: 0110-08-05

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 12 | ((190827-rozpaczliwe-ratowanie-bii; 190901-polowanie-na-pieknotke; 190906-wypadek-w-kramamczu; 191103-kontrpolowanie-pieknotki-pulapka; 191105-zaginiona-soniczka; 191112-korupcja-z-arystokratki; 191201-ukradziony-entropik; 200202-krucjata-chevaleresse; 200414-arystokraci-na-trzesawisku; 200510-tajna-baza-orbitera; 200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Ksenia Kirallen      | 6 | ((190906-wypadek-w-kramamczu; 191103-kontrpolowanie-pieknotki-pulapka; 210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 211122-czolenkowe-esuriit-w-amz)) |
| Klaudia Stryk        | 5 | ((210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 211122-czolenkowe-esuriit-w-amz; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Minerwa Metalia      | 5 | ((190901-polowanie-na-pieknotke; 191103-kontrpolowanie-pieknotki-pulapka; 191112-korupcja-z-arystokratki; 191201-ukradziony-entropik; 200202-krucjata-chevaleresse)) |
| Talia Aegis          | 5 | ((190827-rozpaczliwe-ratowanie-bii; 200202-krucjata-chevaleresse; 200414-arystokraci-na-trzesawisku; 210926-nowa-strazniczka-amz; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Tymon Grubosz        | 5 | ((190827-rozpaczliwe-ratowanie-bii; 191105-zaginiona-soniczka; 200510-tajna-baza-orbitera; 210926-nowa-strazniczka-amz; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Diana Tevalier       | 4 | ((190901-polowanie-na-pieknotke; 200202-krucjata-chevaleresse; 200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Alan Bartozol        | 3 | ((190901-polowanie-na-pieknotke; 200913-haracz-w-parku-rozrywki; 210817-zgubiony-holokrysztal-w-lesie)) |
| Arnulf Poważny       | 3 | ((210926-nowa-strazniczka-amz; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Ataienne             | 3 | ((191105-zaginiona-soniczka; 191112-korupcja-z-arystokratki; 200202-krucjata-chevaleresse)) |
| Ignacy Myrczek       | 3 | ((191112-korupcja-z-arystokratki; 200414-arystokraci-na-trzesawisku; 200510-tajna-baza-orbitera)) |
| Laura Tesinik        | 3 | ((200510-tajna-baza-orbitera; 200524-dom-dla-melindy; 210817-zgubiony-holokrysztal-w-lesie)) |
| Sabina Kazitan       | 3 | ((191112-korupcja-z-arystokratki; 200414-arystokraci-na-trzesawisku; 200510-tajna-baza-orbitera)) |
| Strażniczka Alair    | 3 | ((200414-arystokraci-na-trzesawisku; 210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie)) |
| Teresa Mieralit      | 3 | ((200510-tajna-baza-orbitera; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Tomasz Tukan         | 3 | ((200202-krucjata-chevaleresse; 210720-porwanie-daniela-terienaka; 210817-zgubiony-holokrysztal-w-lesie)) |
| Damian Orion         | 2 | ((191103-kontrpolowanie-pieknotki-pulapka; 200202-krucjata-chevaleresse)) |
| Felicjan Szarak      | 2 | ((211009-szukaj-serpentisa-w-lesie; 211122-czolenkowe-esuriit-w-amz)) |
| Józef Małmałaz       | 2 | ((190901-polowanie-na-pieknotke; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Keraina d'Orion      | 2 | ((191201-ukradziony-entropik; 200202-krucjata-chevaleresse)) |
| Marek Puszczok       | 2 | ((190827-rozpaczliwe-ratowanie-bii; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Marysia Sowińska     | 2 | ((210720-porwanie-daniela-terienaka; 210817-zgubiony-holokrysztal-w-lesie)) |
| Mateusz Kardamacz    | 2 | ((191105-zaginiona-soniczka; 200202-krucjata-chevaleresse)) |
| Melinda Teilert      | 2 | ((200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Persefona d'Jastrząbiec | 2 | ((191112-korupcja-z-arystokratki; 191201-ukradziony-entropik)) |
| Adam Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Agostino Karwen      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Albert Kalandryk     | 1 | ((210926-nowa-strazniczka-amz)) |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Artur Kołczond       | 1 | ((200510-tajna-baza-orbitera)) |
| BIA Tarn             | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Cyryl Perikas        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Daniel Terienak      | 1 | ((210720-porwanie-daniela-terienaka)) |
| Dariusz Drewniak     | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Dariusz Kuromin      | 1 | ((190906-wypadek-w-kramamczu)) |
| Dariusz Skórnik      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Diana Lemurczak      | 1 | ((200524-dom-dla-melindy)) |
| Edelmira Neralis     | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Ernest Kajrat        | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Ernest Termann       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Erwin Galilien       | 1 | ((191105-zaginiona-soniczka)) |
| Franciszek Leszczowik | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Franciszek Owadowiec | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Franek Bulterier     | 1 | ((200524-dom-dla-melindy)) |
| Gabriel Ursus        | 1 | ((200510-tajna-baza-orbitera)) |
| Gerwazy Lemurczak    | 1 | ((200524-dom-dla-melindy)) |
| Grzegorz Czerw       | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Hestia d'Itaran      | 1 | ((191201-ukradziony-entropik)) |
| Kacper Bankierz      | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Kaella Sarimanis     | 1 | ((231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Karol Senonik        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Karolina Terienak    | 1 | ((210720-porwanie-daniela-terienaka)) |
| Kasjopea Maus        | 1 | ((190906-wypadek-w-kramamczu)) |
| Krystyna Senonik     | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Lilia Ursus          | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Liliana Bankierz     | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Lucjusz Blakenbauer  | 1 | ((190901-polowanie-na-pieknotke)) |
| Maja Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Malictrix d'Itaran   | 1 | ((200202-krucjata-chevaleresse)) |
| Mariola Tralment     | 1 | ((191105-zaginiona-soniczka)) |
| Maryla Koternik      | 1 | ((211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Natalia Tessalon     | 1 | ((200510-tajna-baza-orbitera)) |
| Olaf Zuchwały        | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Rafał Kamaron        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Rafał Roszczeniok    | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Rafał Torszecki      | 1 | ((210720-porwanie-daniela-terienaka)) |
| Roland Grzymość      | 1 | ((191201-ukradziony-entropik)) |
| Rupert Mysiokornik   | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Sławomir Niejadek    | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Stella Armadion      | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Tadeusz Sklerzec     | 1 | ((191112-korupcja-z-arystokratki)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Udom Rapnak          | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Urszula Miłkowicz    | 1 | ((210720-porwanie-daniela-terienaka)) |
| Wiktor Szurmak       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |