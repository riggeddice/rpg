# Rafał Kidiron
## Identyfikator

Id: 9999-rafał-kidiron

## Sekcja Opowieści

### Infiltrator ucieka a Arkologia płonie

* **uid:** 230621-infiltrator-ucieka-a-arkologia-plonie, _numer względny_: 10
* **daty:** 0093-03-25 - 0093-03-26
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, BIA Prometeus, Dalmjer Servart, Eustachy Korkoran, Kalia Awiter, Laurencjusz Kidiron, Małgorzata Maratelus, OO Infernia, Rafał Kidiron, Ralf Tapszecz

Streszczenie:

Ardilla utrzymuje Kidirona przy życiu, negocjując dla niego bezpieczne miejsce w Szczurowisku i stawiając swoją i Ralfową reputację na szali. Potem - rozpaczliwie ratuje Prometeusa, acz z ciężkimi stratami. Eustachy w pogoni za niezwykle groźnym Infiltratorem musiał dać mu odejść; oddał Skorpiona i czterech załogantów, ale uratował Kalię Awiter. Tymczasem o Arkologię Nativis toczy się wojna dusz - Bartłomiej Korkoran kontra Laurencjusz Kidiron. A w tle eksperymenty Kidirona (jak np. farighanowie jako Hełmy) wyrywają się spod kontroli i zdecydowanie nie pomagają.

Aktor w Opowieści:

* Dokonanie:
    * nieprzytomny; Ardilla wynegocjowała dlań w Szczurowisku bezpieczne miejsce.


### Atak na Kidirona

* **uid:** 230614-atak-na-kidirona, _numer względny_: 9
* **daty:** 0093-03-22 - 0093-03-24
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Dalmjer Servart, Eustachy Korkoran, Kalia Awiter, Lycoris Kidiron, OO Infernia, Rafał Kidiron, Ralf Tapszecz, SAN Szare Ostrze, Stanisław Uczantor

Streszczenie:

Gdy Wujek się obudził, powiedział że z Infernią mogą być ostre problemy jeśli spada poniżej poziomu Memoriam. Zmartwił się hipotezą Ardilli że Ralf może być magiem Nihilusa, ale Ardilla walczyła o dalsze spotykanie się z Ralfem. Zamach na Kidirona. Większość jego sztabu umiera, ale sam Kidiron przeżywa, acz ciężko ranny; ratuje go Ardilla i pozyskuje info o jego mrocznych planach. Kalia bierze na siebie morale i propagandę, ale przez to wpada na celownik Infiltratora który ją porywa. Eustachy próbuje odbić Kalię z rąk Infiltratora w koloidzie...

Aktor w Opowieści:

* Dokonanie:
    * miał szczęście - zajmował się swoimi planami poza spotkaniem głównym i nie było go gdy Infiltrator próbował go zabić. Nie on a miragent zginął (wraz z całym sztabem R.K.). Bardzo ciężko ranny w swoim pomniejszym schronieniu. Uratowany przez... Ardillę. Jest TWARDY DOWÓD, że bez niego arkologia nie przetrwa.
* Progresja:
    * miał szczęście i nie zginął w zamachu. Został jednak bardzo ciężko ranny. Co najmniej 2 tygodnie ciężkiego szpitalnego leczenia.


### Zdrada rozrywająca arkologię

* **uid:** 230329-zdrada-rozrywajaca-arkologie, _numer względny_: 8
* **daty:** 0093-03-14 - 0093-03-16
* **obecni:** Amelia Sarkaldir, Ardilla Korkoran, BIA Prometeus, Celina Lertys, Eustachy Korkoran, Ewelina Paroknis, Feliks Kidiron, Franciszek Pietraszczyk, Iwona Paroknis, Jonasz Paroknis, Kalia Awiter, Katarzyna Falernik, Małgorzata Maratelus, OO Infernia, Rafał Kidiron, Ralf Tapszecz, Szczepan Falernik, Wojciech Grzebawron

Streszczenie:

Nastolatka miała pamiętnik czytany przez macochę. Ojciec to ignorował. Nastolatka napisała w pamiętniku, że ojciec zdradza macochę z ekspertką hydrologii. Zrobił się shitstorm który doprowadził do poważnych problemów - m.in. macocha (dyrektor skrzydła medycznego) zaczęła robić błędy co wykryła Celina. Kidiron podejrzewa spisek - ktoś próbuje skłócić ważne rodziny, a także shackować Prometeusa (BIA arkologii). Rozmowa z ich eks-piratami kończy się tym, że to nie oni - za tym stoi wyspecjalizowana siła z innej arkologii lub noktianie. 

Ślady prowadzą do pamiętnika nastolatki, który shackowany przez ekspertkę Inferni (i upubliczniony przez problemy na linii Infernia - Prometeus) pokazują, że najpewniej nastolatka za wszystkim stoi sama. Uratowana przez tajemniczego maga Interis przed samobójstwem nastolatka przyznała na czym polegał problem i ujawnia, że pomaga jej Feliks Kidiron, najmłodszy z rodu i jej jedyny przyjaciel. Czyli to nie spisek. Ale jak to teraz naprawić?

Aktor w Opowieści:

* Dokonanie:
    * dowiedział się, że są problemy z maszynami medycznymi obsługującymi Bartłomieja Korkorana. OCZYWIŚCIE że dał uprawnienia Inferni by zajmowali się wujkiem. Podejrzewa spisek, że ktoś chce skłócając rodziny zniszczyć arkologię. Wszystkie te tematy - dysputy rodzinne, _petty shit_ są zdecydowanie poniżej jego godności i zainteresowania. Typowe "I've got morons on my team..."


### Bardzo nieudane porwanie Inferni

* **uid:** 230315-bardzo-nieudane-porwanie-inferni, _numer względny_: 7
* **daty:** 0093-03-06 - 0093-03-09
* **obecni:** Ardilla Korkoran, Eustachy Korkoran, Hubert Grzebawron, Mariusz Dobrowąs, Nadia Sekernik, OO Infernia, Rafał Kidiron, Ralf Tapszecz, SAN Szare Ostrze, Wojciech Grzebawron

Streszczenie:

Mimo braku wujka, Infernia działa sprawnie. Wujek jest pod opieką medyczną, ale Celina nie ufając Kidironowi, chce być z nim cały czas. Infernia otrzymuje wezwanie od CES Mineralis, gdyż stacja została zaatakowana przez Trianai. Eustachy wysyła Czarne Hełmy i komandosów z Inferni, którzy wspólnie działają, ale Trianai jest za dużo. Eustachy wabi więc Trianai za pomocą energii magicznej, tracąc przytomność po połączeniu z koordynatorem Trianai (magiem), zabijając go. Stacja CES ucierpiała w Paradoksie Eustachego, powodując wiele ofiar. Ardilla koordynuje operację ratunkową, ale gdy okazuje się, że część załogi z CES atakuje mostek próbując ukraść statek, Ardilla wyłącza Infernię i z Ralfem wieją do kanałów Inferni.

Eustachy budzi się w niewoli, przekonuje oprychów, że jedynie on potrafi sterować Infernią (ucierpiał przy tym nieco). Ardilla i Ralf - na prośbę Eustachego - sabotują generatory Memoriam, pozwalając Inferni uwolnić się spod kontroli wrogów. Wywiązuje się strzelanina, w wyniku której napastnicy zostają pokonani - Eustachy kontroluje sytuację. Eustachy decyduje, że ich karą będzie służba na Inferni, ale jeden z nich odchodzi ostrzegać przed atakiem na Infernię. To wszystkim pasuje.

Ekipa wraca na statek, gdzie Kidiron proponuje przesłuchanie pojmanych piratów, obiecując immunitet w zamian za współpracę. Eustachy zgadza się, ale tylko z ochotnikami i Ardillą jako gwarantem bezpieczeństwa. Piraci zgadzają się współpracować mimo użycia neuroobroży przy przesłuchaniu przez Kidirona. Gdy Zespół próbuje montować nowe generatory Memoriam - nie da się. Nie mogą być zamontowane, gdyż się palą. Infernia wykrywa próbę montażu nowych, ale nie zauważa starych.

Aktor w Opowieści:

* Dokonanie:
    * zaproponował przesłuchanie pojmanych piratów, obiecując im immunitet w zamian za współpracę, co pozwoliło mu zdobyć cenne informacje. Pokazał Ardilli, że piraci zgodzą się na przesłuchania neuroobrożą i podkreślał "kapitana Eustachego Korkorana".
* Progresja:
    * pozyskał informacje o piratach na Neikatis dzięki śmiałej akcji Eustachego gdy próbowano Infernię porwać.


### Terrorystka w Ambasadorce

* **uid:** 230215-terrorystka-w-ambasadorce, _numer względny_: 6
* **daty:** 0093-02-22 - 0093-02-23
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Kalia Awiter, Magda Misteria Sarbanik, Rafał Kidiron, Ralf Tapszecz, Tobiasz Lobrak, Tymon Korkoran

Streszczenie:

Kalia i Ardilla weszły do Ambasadorki by pomóc jeśli są w stanie, odcięły ich wiły i Ardilla ich poddaje. Misteria - napastniczka - okazuje się być czarodziejką roślin. Ona chce odzyskać wiły i zarzucić Kidironowi zniszczenie arkologii oraz porwanie wił. Jej celem jest ujawnienie prawdy o Kidironie, ukaranie Tobiasza (patrona Ambasadorki który robi złe rzeczy) i zabezpieczenie wił. Wujek chce zastąpić Ardillę, ale ona odmawia. Zespół (poza Ardillą) planuje zinfiltrować Ambasadorkę i usunąć maga (Misterię). 

Eustachy wpada na pomysł kontrolowanego obniżenia poziomu tlenu wewnątrz. Ardilla przekonuje Misterię do wymiany rannych na wujka. Ralf stanowi kanał komunikacyjny. Misteria tworzy Czarne Serce, kosztem wił i energii. Planują przekonać ludzi o winie Kidironów, wydostać się z Ambasadorki i stworzyć reportaż z Kalią. Ralf mówi o laserze na dachu, którym może zniszczyć czarodziejkę. Tobiasz wzbudza w sercu Misterii terror. Eustachy i Kidiron oczekują na rozwój sytuacji. W wyniku negocjacji, Kidiron zgadza się współpracować pod warunkiem, że Misteria nie będzie działać przeciwko niemu czy arkologii.

Kalia tworzy reportaż o Kidironie i arkologii Lirvint. Kidiron obiecuje bezpieczne wyjście dla Misterii i wił, pod warunkiem zapewnienia nieagresji. Ardilla próbuje przekonać Misterię do ewakuacji, ale ona nie chce opuścić miejsca bez rozwiązania sprawy z Tobiaszem. Czarne Serce wpływa na Misterię, więc wujek ją obezwładnia wstrzykując jej jakieś świństwo, sam zostaje poraniony przez wiły. Kidiron dotrzymał słowa odnośnie ewakuacji, acz postawił na Misterii tracker.

Aktor w Opowieści:

* Dokonanie:
    * wierzył, że to fundamentalnie nie ma znaczenia, że wyszły na jaw jego mroczne ruchy w arkologii Lirvint. I faktycznie, nie miało w samej arkologii, ale pojawił się ruch oporu. Został oskarżany przez Misterię o uszkodzenie arkologii Lirvint i porwanie wił. Ostatecznie zgodził się na współpracę, pod warunkiem, że Misteria nie będzie działać przeciwko niemu i arkologii. Umieścił tracker w Misterii, aby monitorować jej ruchy. Zimny jak zawsze, korzystał z okazji by mocniej pokazać Eustachemu że takie działania są potrzebne dla tej arkologii.
* Progresja:
    * ujawnione zostało, że robił złe rzeczy w arkologii Lirvint. Pojawiła się silna opozycja wewnętrzna i ruch oporu. Im lepiej w arkologii i im szczęśliwsi są ludzie, im mniej się boją (a Kidiron próbuje by tak było) tym większy opór przeciw niemu.
    * jedyna osoba na której mu naprawdę zależy i którą naprawdę lubi - Kalia Awiter - została ujawniona jako jego "słaby punkt".


### Pierwsza randka Eustachego

* **uid:** 230208-pierwsza-randka-eustachego, _numer względny_: 5
* **daty:** 0093-02-14 - 0093-02-21
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Franciszek Pietraszczyk, Kalia Awiter, Marcin Pietraszczyk, OO Infernia, Rafał Kidiron, Ralf Tapszecz

Streszczenie:

Wujek opieprza Eustachego za krzywdę niewinnych noktian, ale Kidiron się za Eustachym wstawia. Ardilla socjalizuje Ralfa i ratuje młodego Marcinka który się zaklinował w rurze Szczurowiska; okazuje się, że wszyscy kojarzą Infernię jako symbol nadziei. Potem Eustachy idzie (z przymusu) na randkę z Kalią; okazuje się, że Kalia zmanipulowała by Eustachy wygrał bo chce dać mu jeden piękny dzień. Wszystkie dowody podsłuchała i złapała Ardilla. Gdy Eustachy poczuł chore emanacje magiczne z Ambasadorki, porzucił Kalię i pobiegł do Inferni.

Aktor w Opowieści:

* Dokonanie:
    * broni Eustachego i jego decyzji przed wujkiem - jego zdaniem Eustachy zrobił co należy. Gdy Eustachy powiedział, że nie chce iść na randkę z Kalią, Kidiron mu powiedział - robimy co robimy dla arkologii. Nie to, co chcemy robić.


### Wyłączone generatory Memoriam Inferni

* **uid:** 230201-wylaczone-generatory-memoriam-inferni, _numer względny_: 4
* **daty:** 0093-02-10 - 0093-02-12
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Ernest Puszczowiec, Eustachy Korkoran, Jan Lertys, Kalia Awiter, Kalista Luminis, Kratos Coruscatis, OO Infernia, Rafał Kidiron, Ralf Tapszecz, Tymon Korkoran

Streszczenie:

Eustachy przebudzony przez Infernię po burzy piaskowej poszedł za jej podszeptami i wyłączył generatory Memoriam. Infernia go Dotknęła - jest to anomalia Interis uwięziona w mechanicznej skorupie. Tylko Eustachy może Infernią sterować. Więc gdy pojawiła się misja pomocy bazie noktiańskiej Coruscatis, Eustachy musiał lecieć. Kidiron chciał aresztowania Kallisty Luminis a Wujek chciał pomocy noktianom. Okazało się, że na miejscu sformował się amalgamoid trianai i Infernia została ostrzelana; acz odpowiedziała ogniem. Zespół pomógł i ewakuował noktian i zdestabilizował reaktor fuzyjny by wysadzić wszystko. Pod wpływem Ardilli i Celiny Eustachy nie oddał noktian Kidironowi a wysadził ich w bezpiecznym miejscu. Kolejny dowód, że arkologia Nativis wykorzystuje żywność jako broń, co jest niezgodne z wszelkimi konwencjami.

Aktor w Opowieści:

* Dokonanie:
    * gdy przełożeni arkologii Sarviel się do niego zgłosili z prośbą o pomoc w pozbyciu się noktian z Coruscatis, doprowadził do skażenia eksportowanej żywności by pomóc Trianai w zniszczeniu Coruscatis. Na prośbę Sarviel poprosił Korkoranów o aresztowanie Kallisty Luminis. Zrobi KAŻDĄ potworność, by wzmocnić Neikatis.


### Infernia taksówką dla Lycoris

* **uid:** 220720-infernia-taksowka-dla-lycoris, _numer względny_: 3
* **daty:** 0093-01-20 - 0093-01-22
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Czesław Żuczek, Daria Raizis, Eustachy Korkoran, Jan Lertys, Kamil Wraczok, Lycoris Kidiron, OO Infernia, Rafał Kidiron, Wiktor Turkalis

Streszczenie:

Plaga Trianai uderzyła w CES Purdont; Infernia p.d. Bartłomieja Korkorana ruszyła jako taksówka dla Lycoris ale młodzi trafili do Purdont a Infernia szuka Lycoris i Wiktora. Gdy młodzi doszli do Plagi i przebili się do łączności to poinformowali Nativis i wujka o ryzyku. Dostali skonfliktowane polecenia, ale mają przetrwać. Jednak dla części z nich to nieakceptowalne, bo nie zostawią zarażonej koleżanki na pożarcie Pladze Trianai mimo znajdującego się gdzieś w cieniu Inteligentnego Koordynatora Trianai...

Aktor w Opowieści:

* Dokonanie:
    * wysłał Lycoris do CES Purdont; zniknęła niedaleko Wiertła Ekopoezy Delta. W panice wezwał Infernię ale nic im nie powiedział. Pomógł Zespołowi przesyłając kody z arkologii by wstrzymywać Plagę.


### Pasożytnicze osy w Nativis

* **uid:** 220712-pasozytnicze-osy-w-nativis, _numer względny_: 2
* **daty:** 0092-10-29 - 0092-11-07
* **obecni:** Damian Marlinczak, Jarlow Gurdacz, Lycoris Kidiron, Rafał Kidiron, Serentina d'Remora

Streszczenie:

W arkologii Nativis pojawiły się dziwne osy / drzewa i zainfekowały bioinżynier arkologii z rodu rządzącego, Lycoris. Z kopii biologicznej Lycoris złożono miragenta trzeciej generacji; ów miragent z pomocą swej Remory doszedł do tego, że celem os jest rozprzestrzenienie Pasożyta dalej, do innych arkologii. Miragent zablokował eksport i Nativis przeszła w tryb bezpiecznego kontrolowanego czyszczenia. I mają próbki Pasożyta jako potencjalną broń biologiczną.

Aktor w Opowieści:

* Dokonanie:
    * bezwzględny szef ochrony Nativis należący do kasty rządzącej i kuzyn Lycoris; dobrze zarządza Nativis słuchając rad i zostawiając ekspertom działania. Opanował Pasożyta z pomocą miragenta Lycoris i pozyskał próbki Pasożyta jako broń biologiczną.


### To, co zostało po burzy

* **uid:** 230104-to-co-zostalo-po-burzy, _numer względny_: 1
* **daty:** 0092-10-26 - 0092-10-28
* **obecni:** Aniela Myszawcowa, Anna Seiren, Antoni Grzypf, Cyprian Kugrak, Eustachy Korkoran, JAN Seiren, JAN Uśmiech Kamili, Kalia Awiter, Michał Uszwon, Rafał Kidiron, Rufus Seiren, Zofia d'Seiren

Streszczenie:

Neikatis miewa burze piaskowe które z uwagi na strukturę piasku powodują efekty magiczne i po których pojawiają się krótkotrwałe anomalie. Salvagerzy zbierają te anomalie i drenują je, by energię dostarczyć arkologii. Eustachy został przydzielony do salvagera "Seiren", by uczyć się jak wygląda życie w arkologii. Niestety, współwłaścicielka Seiren została porwana przez dowódcę innego salvagera i odjechali w burzę piaskową by ratować JEGO żonę. Seiren jedzie w kierunku na sygnał drugiego salvagera przez burzę, w której odbijają się dziwne głosy i sygnały komunikacyjne. Napotykają ów "Uśmiech Kamili", ale część osób jest martwa i nie ma śladów życia...

Aktor w Opowieści:

* Dokonanie:
    * zgodził się, by Eustachy wziął skorpiona (Seiren) i pojechał ratować Annę.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 10, @: 0093-03-26
    1. Primus    : 10, @: 0093-03-26
        1. Sektor Astoriański    : 10, @: 0093-03-26
            1. Neikatis    : 10, @: 0093-03-26
                1. Dystrykt Glairen    : 10, @: 0093-03-26
                    1. Arkologia Lirvint    : 1, @: 0093-02-23
                    1. Arkologia Nativis, okolice    : 1, @: 0092-10-28
                        1. Szepczące Wydmy    : 1, @: 0092-10-28
                    1. Arkologia Nativis    : 9, @: 0093-03-26
                        1. Poziom 1 - Dolny    : 5, @: 0093-03-26
                            1. Północ - Stara Arkologia    : 5, @: 0093-03-26
                                1. Blokhaus F    : 3, @: 0093-03-26
                                    1. Szczurowisko    : 2, @: 0093-03-26
                                1. Stare Wejście Północne    : 1, @: 0093-03-26
                                1. Szczurowisko    : 1, @: 0093-02-21
                            1. Wschód    : 1, @: 0092-11-07
                                1. Farmy Wschodnie    : 1, @: 0092-11-07
                            1. Zachód    : 2, @: 0093-03-26
                                1. Centrala Prometeusa    : 1, @: 0093-03-26
                                1. Stare TechBunkry    : 1, @: 0092-11-07
                        1. Poziom 2 - Niższy Środkowy    : 1, @: 0092-10-28
                            1. Wschód    : 1, @: 0092-10-28
                                1. Centrum Kultury i Rozrywki    : 1, @: 0092-10-28
                                    1. Bar Śrubka z Masła    : 1, @: 0092-10-28
                        1. Poziom 3 - Górny Środkowy    : 4, @: 0093-03-24
                            1. Wschód    : 4, @: 0093-03-24
                                1. Dzielnica Luksusu    : 4, @: 0093-03-24
                                    1. Ambasadorka Ukojenia    : 2, @: 0093-02-23
                                    1. Ogrody Wiecznej Zieleni    : 3, @: 0093-03-24
                                    1. Stacja holosymulacji    : 1, @: 0093-02-21
                    1. Arkologia Sarviel, okolice    : 1, @: 0093-02-12
                        1. Krater Coruscatis    : 1, @: 0093-02-12
                    1. CES Mineralis    : 1, @: 0093-03-09
                    1. CES Purdont, okolice    : 1, @: 0093-01-22
                        1. Wiertło Ekopoezy Delta    : 1, @: 0093-01-22
                    1. CES Purdont    : 1, @: 0093-01-22
                        1. Kafeteria    : 1, @: 0093-01-22
                        1. Laboratorium Ekopoezy    : 1, @: 0093-01-22
                        1. Life Support    : 1, @: 0093-01-22
                        1. System łączności    : 1, @: 0093-01-22

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eustachy Korkoran    | 9 | ((220720-infernia-taksowka-dla-lycoris; 230104-to-co-zostalo-po-burzy; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Ardilla Korkoran     | 8 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Kalia Awiter         | 7 | ((230104-to-co-zostalo-po-burzy; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| OO Infernia          | 7 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Ralf Tapszecz        | 7 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Bartłomiej Korkoran  | 6 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Celina Lertys        | 3 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Lycoris Kidiron      | 3 | ((220712-pasozytnicze-osy-w-nativis; 220720-infernia-taksowka-dla-lycoris; 230614-atak-na-kidirona)) |
| BIA Prometeus        | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Dalmjer Servart      | 2 | ((230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Franciszek Pietraszczyk | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| Jan Lertys           | 2 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni)) |
| Małgorzata Maratelus | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| SAN Szare Ostrze     | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230614-atak-na-kidirona)) |
| Tymon Korkoran       | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce)) |
| Wojciech Grzebawron  | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Aniela Myszawcowa    | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Anna Seiren          | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Antoni Grzypf        | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Cyprian Kugrak       | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Czesław Żuczek       | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Damian Marlinczak    | 1 | ((220712-pasozytnicze-osy-w-nativis)) |
| Daria Raizis         | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Hubert Grzebawron    | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| JAN Seiren           | 1 | ((230104-to-co-zostalo-po-burzy)) |
| JAN Uśmiech Kamili   | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Jarlow Gurdacz       | 1 | ((220712-pasozytnicze-osy-w-nativis)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kamil Wraczok        | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Laurencjusz Kidiron  | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Marcin Pietraszczyk  | 1 | ((230208-pierwsza-randka-eustachego)) |
| Mariusz Dobrowąs     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Michał Uszwon        | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Nadia Sekernik       | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Rufus Seiren         | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Serentina d'Remora   | 1 | ((220712-pasozytnicze-osy-w-nativis)) |
| Stanisław Uczantor   | 1 | ((230614-atak-na-kidirona)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Tobiasz Lobrak       | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Wiktor Turkalis      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Zofia d'Seiren       | 1 | ((230104-to-co-zostalo-po-burzy)) |