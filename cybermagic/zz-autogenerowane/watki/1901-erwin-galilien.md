# Erwin Galilien
## Identyfikator

Id: 1901-erwin-galilien

## Sekcja Opowieści

### Rekin z Aurum i fortifarma

* **uid:** 200509-rekin-z-aurum-i-fortifarma, _numer względny_: 22
* **daty:** 0110-08-30 - 0110-09-04
* **obecni:** Artur Kołczond, Artur Michasiewicz, Erwin Galilien, Gabriel Ursus, Natalia Tessalon, Pięknotka Diakon, Sabina Kazitan, Tadeusz Tessalon

Streszczenie:

Sabina Kazitan ostrzegła Pięknotkę, że Natalia Tessalon z Aurum zażądała od niej niebezpiecznego rytuału do hodowania potwora. Pięknotka doszła do tego z sygnatury energii magicznej, że problem jest w okolicy Studni Irrydiańskiej; tam jest też fortifarma w której stary były-terminus tępi Latające Rekiny. I faktycznie - arystokraci z Aurum stworzyli potwora (krwawego) a Pięknotka Cieniem go zniszczyła. Niestety, brat Natalii został ciężko porażony i nigdy nie będzie już taki jak kiedyś. Aha, kwatermistrz opieprza Pięknotkę.

Aktor w Opowieści:

* Dokonanie:
    * najdoskonalszy katalista; pomógł Gabrielowi znaleźć, że Krwawy Potwór budowany jest w okolicy fortifarmy Irrydii.


### Infiltrator poluje na TAI Minerwy

* **uid:** 200425-inflitrator-poluje-na-tai-minerwy, _numer względny_: 21
* **daty:** 0110-08-24 - 0110-08-26
* **obecni:** Erwin Galilien, Gabriel Ursus, Laura Tesinik, Minerwa Metalia, Pięknotka Diakon, Strażniczka Alair, Tomasz Tukan

Streszczenie:

Problemy na Trzęsawisku wpływają na Cyberszkołę w Zaczęstwie, co uszkadza możliwości TAI "Strażniczki", czyli Eszary d'AlephAiren. Tymczasem jakaś tajemnicza siła uszkadza cywilne TAI, które usprawniała Minerwa. Pięknotka odpiera Tukana, który chce udowodnić winę Minerwy i odkrywa, że za tym wszystkim stoi ktoś z Aurum, ktoś kto posiada profesjalnego pajęczego Infiltratora Iniekcyjnego, zaprojektowanego do uszkadzania AI. Dzięki temu Pięknotka wyciągnęła Minerwę z aresztu.

Aktor w Opowieści:

* Dokonanie:
    * mistrz delikatnej katalizy, z Gabrielem odkrył, że za problemami TAI nie stoi Minerwa a Infiltrator Iniekcyjny Aurum


### Kwiaty w służbie puryfikacji

* **uid:** 190817-kwiaty-w-sluzbie-puryfikacji, _numer względny_: 20
* **daty:** 0110-07-04 - 0110-07-09
* **obecni:** Ataienne, Erwin Galilien, Kornel Garn, Nataniel Marszalnik, Pięknotka Diakon, Teresa Marszalnik

Streszczenie:



Aktor w Opowieści:

* Dokonanie:
    * 


### Zaginiona soniczka

* **uid:** 191105-zaginiona-soniczka, _numer względny_: 19
* **daty:** 0110-07-02 - 0110-07-03
* **obecni:** Ataienne, Erwin Galilien, Mariola Tralment, Mariusz Trzewń, Mateusz Kardamacz, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Ataienne poprosiła Pięknotkę o znalezienie zaginionej fanki. Pięknotka zaczęła śledztwo i doszła do tego, że Mariola została porwana przez gang przerzucający dziewczyny do Cieniaszczytu. Erwin słyszał takie plotki. Pięknotka zlokalizowała i usunęła komórkę tego gangu; śledztwo pokazało na autoklub fitness jako jedno interesujące miejsce. Najpewniej nie to jest miejscem "zła", ale warto sprawdzić.

Aktor w Opowieści:

* Dokonanie:
    * najlepszy katalista. Wykrył po starej aurze co było nie tak z Mariolą oraz pomógł Pięknotce rozwikłać zagadkę. Gdy potrzebny jest arcykatalista, Erwin jest na miejscu.


### Mimik śni o Esuriit

* **uid:** 190527-mimik-sni-o-esuriit, _numer względny_: 18
* **daty:** 0110-05-07 - 0110-05-09
* **obecni:** Adela Kirys, Erwin Galilien, Mirela Satarail, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Mirela uciekła Wiktorowi Satarailowi z Trzęsawiska, więc ów poprosił o pomoc Pięknotkę. Mirela wyczuła Esuriit w Czółenku i chciała nakarmić głód; Pięknotka jednak dała radę ją znaleźć i nie dopuścić do Pożarcia Mireli przez Esuriit; zamiast tego ściągnęła oddział terminusów którzy sami rozwiązali problem. No i wycofała Mirelę z powrotem do Wiktora.

Aktor w Opowieści:

* Dokonanie:
    * najlepszy katalista w okolicy, wpadł pomóc Pięknotce znaleźć Mirelę a znalazł faktyczne Esuriit.


### Sabotaż szeptów Elizy

* **uid:** 190429-sabotaz-szeptow-elizy, _numer względny_: 17
* **daty:** 0110-04-10 - 0110-04-13
* **obecni:** Alan Bartozol, Aleksander Rugczuk, Diana Tevalier, Eliza Ira, Erwin Galilien, Karla Mrozik, Lucjusz Blakenbauer, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

Chevaleresse wpakowała się w "kult Elizy" grając z ludźmi online; powiedziała o tym Pięknotce. Pięknotka odkryła, że Eliza rozprzestrzenia swoje wpływy przez kryształy w formie biżuterii; wraz z Alanem, Erwinem i (nadal ranną) Minerwą zaprojektowali mechanizm uszkadzający sieć krystaliczną Elizy. Udało im się kupić jakiś miesiąc czasu, aż Orbiter i Pustogor znajdą lepsze rozwiązanie. Sama Chevaleresse ma się nie spotykać z takimi tam. Ale może z nimi grać online.

Aktor w Opowieści:

* Dokonanie:
    * katalista delikatnych energii oraz konstruktor pułapki energii; wraz z Alanem, Minerwą i Pięknotką stworzyli rezonator kryształów Elizy, niszczący jej plany.


### Zrzut w Pacyfice

* **uid:** 190427-zrzut-w-pacyfice, _numer względny_: 16
* **daty:** 0110-04-07 - 0110-04-09
* **obecni:** Erwin Galilien, Minerwa Metalia, Nikola Kirys, Pięknotka Diakon

Streszczenie:

Do Pacyfiki ktoś z Cieniaszczytu zrzucił świetnej klasy ścigacz. Pięknotka i Erwin zinfiltrowali Pacyfikę by dostać namiary na ten pojazd. Okazało się, że pilotem ma być Nikola - też Nurek Szczeliny, świetnej klasy. Eks-Noctis. Pięknotka po raz pierwszy była w Pacyfice. Dodatkowo, dowiedziała się od Minerwy, że Cień niekoniecznie odpala się na żądanie, nie rozumie Pięknotki. Świetnie.

Aktor w Opowieści:

* Dokonanie:
    * niechętnie wziął Pięknotkę do Pacyfiki; płynnie po Pacyfice nawigował i skutecznie doprowadził Pięknotkę do tajemniczego ścigacza i Nikoli


### Budowa ixiońskiego mimika

* **uid:** 190424-budowa-ixionskiego-mimika, _numer względny_: 15
* **daty:** 0110-04-03 - 0110-04-05
* **obecni:** Aleksander Rugczuk, Erwin Galilien, Karla Mrozik, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

W świetle narastających napięć w Pustogorze i czarnych artefaktów na wolności, Pięknotka wykorzystuje (za aprobatą Barbakanu) mimika który zdominował Marcela. Minerwa go psychotronicznie osłabia z perspektywy morderstw i infekuje go energią ixiońską. Pięknotka straciła go przy Dzielnicy Uciechy, ale i tak zrobił swoje (choć straty materialne są większe). Zainfekowany przez mimika wykrzyczał, że Eliza Ira wróciła oraz Saitaer się przebudził. Niepokojące dla Pięknotki.

Aktor w Opowieści:

* Dokonanie:
    * poczciwy i kochany; powiedział Pięknotce o próbach przemytu z okolicy Wolnych Ptaków. Nie miał pojęcia, że Pięknotka wyczyści Pustogor ixiońską pięścią.


### Pustogorski Konflikt

* **uid:** 190422-pustogorski-konflikt, _numer względny_: 14
* **daty:** 0110-04-02 - 0110-04-03
* **obecni:** Erwin Galilien, Karla Mrozik, Marcel Nieciesz, Olaf Zuchwały, Pięknotka Diakon, Wojmił Siwywilk

Streszczenie:

Napięcia na linii Barbakan - Miasteczkowcy w Pustogorze są silne; działania Wiktora jedynie przyspieszyły konflikt. Na rynek dostało się sporo niebezpiecznych artefaktów i Pięknotka musiała pomóc grupie Miasteczkowców. Zdesperowani, złapali terminusa i zamknęli go w piwnicy bo podczas jednego z rajdów na Miasteczkowców magowie z Fortu Mikado porwali dwie dziewczyny. Okazało się, że za tym stoi zaawansowany mimik symbiotyczny; Pięknotka z pomocą Cienia pokonała Skażonego terminusa i uwolniła czarodziejki.

Aktor w Opowieści:

* Dokonanie:
    * najwybitniejszy ekspert od power suitów; ostrzegł Pięknotkę że power suit Marcela nie jest normalnym power suitem, bo zachowuje się nietypowo.


### Eksperymentalny power suit

* **uid:** 190402-eksperymentalny-power-suit, _numer względny_: 13
* **daty:** 0110-03-15 - 0110-03-17
* **obecni:** Ataienne, Erwin Galilien, Kaja Selerek, Minerwa Metalia, Pięknotka Diakon, Szymon Oporcznik

Streszczenie:

Orbiter chciał przekazać Pięknotce eksperymentalny Power Suit którego nikt nie potrafi kontrolować. Pięknotka spróbowała się z nim zintegrować; ona, Minerwa, Erwin i jeszcze dwóch magów Orbitera skończyło w szpitalu. Pięknotka się uśmiechnęła. Chce ten power suit. Opanuje go - mimo, że jest Skażony energią ixiońską i ma uszkodzoną psychotronikę pełną nienawiści. Życie Pięknotki uratowała Ataienne, ale skończyła ciężko ranna, z uszkodzoną matrycą.

Aktor w Opowieści:

* Dokonanie:
    * wraz z Minerwą doszli do tego jak działa Cień i jak funkcjonuje. Nie dość dobrze opanował ten power suit; skończył w szpitalu gdy Cień wyrwał się spod kontroli.


### Minerwa i Kwiaty Nadziei

* **uid:** 190210-minerwa-i-kwiaty-nadziei, _numer względny_: 12
* **daty:** 0110-02-21 - 0110-02-23
* **obecni:** Atena Sowińska, Erwin Galilien, Kasjopea Maus, Kornel Garn, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Kornel Garn próbował przekonać Minerwę do dołączenia do niego, by mu pomogła - budując jej społeczeństwo które ją akceptuje i pokazując jej Kwiaty Nadziei z Ixionu. Minerwa w 100% wpadała w to, więc Pięknotka zmontowała front przeciw Kornelowi - Kasjopea, Erwin, terminusi Pustogorscy. Skończyło się ucieczką Kornela, ale Minerwa będzie chciała mu pomóc i dzielić się z nim wiedzą. A Pięknotkę bardzo martwi to, jak Kornel radzi sobie z ixiońskimi anomaliami i swoim kultem.

Aktor w Opowieści:

* Dokonanie:
    * świetne wsparcie w przekonywaniu Minerwy. Dodatkowo, dzięki niemu dało się wyśledzić ruchy Kornela i jego dziwne kwiaty Nadziei.


### Ixioński Transorganik

* **uid:** 190127-ixionski-transorganik, _numer względny_: 11
* **daty:** 0110-01-29 - 0110-01-30
* **obecni:** Erwin Galilien, Karolina Erenit, Kirył Najłalmin, Minerwa Metalia, Napoleon Bankierz, Pięknotka Diakon, Saitaer, Tymon Grubosz, Wiktor Satarail, Wojtek Kurczynos

Streszczenie:

Minerwa próbowała pomóc Karolinie Erenit, ale niestety użyła artefaktu ixiońskiego i przekształciła wrednego Wojtka Kurczynosa w transorganika. Pięknotka z pomocą Epirjona przeteleportowała siebie i Wojtka na Trzęsawisko i tam z pomocą Wiktora Sataraila odwrócili tą transorganizację. Ten konkretny problem udało się dyskretnie rozwiązać.

Aktor w Opowieści:

* Dokonanie:
    * katalista Pięknotki i dywersja by odwrócić uwagę ixiońskiego transorganika.


### Nowa Minerwa w nowym świecie

* **uid:** 190120-nowa-minerwa-w-nowym-swiecie, _numer względny_: 10
* **daty:** 0110-01-22 - 0110-01-26
* **obecni:** Erwin Galilien, Karla Mrozik, Kasjopea Maus, Minerwa Metalia, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Minerwa wróciła. Jako czarodziejka, opiekować się ma nią Pięknotka. Minerwa nie do końca radzi sobie w nowej rzeczywistości - nie jest piękną Diakonką, ma odłamki pamięci Saitaera i aby powrócić, poświęciła życie człowieka. Zdecydowała się tymczasowo przenieść do Zaczęstwa (Karla wysłała tam wsparcie). Tymczasem Kornel Garn już planuje by ją przekonać by doń dołączyła.

Aktor w Opowieści:

* Dokonanie:
    * nie kocha Minerwy. Kocha Pięknotkę. Chciał, by to było wystarczająco jasne. Nie do końca Minerwie ufa, ale jest skłonny jej zaufać.


### Morderczyni-jednej-plotki

* **uid:** 190101-morderczyni-jednej-plotki, _numer względny_: 9
* **daty:** 0109-12-13 - 0109-12-17
* **obecni:** Alan Bartozol, Aleksander Iczak, Erwin Galilien, Karol Szurnak, Olaf Zuchwały, Pięknotka Diakon, Teresa Mieralit

Streszczenie:

Na temat Pięknotki i jej salonu zaczęto rozpuszczać nieprzyjemne plotki. Pięknotka zlokalizowała jedno ze źródeł i je pokazowo zniszczyła, zmuszając maga do przepraszania i płaczu na kolanach. Dodatkowo, Czerwone Myszy oraz Dare Shiver zaczęli interesować się Pięknotką i jej salonem. A sama Pięknotka przecięła "największą nemesis" Adeli Kirys.

Aktor w Opowieści:

* Dokonanie:
    * znalazł dla Pięknotki informacje o Karolu Szurnaku, który rozpuszczał plotki.


### Adieu, Cieniaszczycie

* **uid:** 181227-adieu-cieniaszczycie, _numer względny_: 8
* **daty:** 0109-11-28 - 0109-12-01
* **obecni:** Amadeusz Sowiński, Atena Sowińska, Brygida Maczkowik, Erwin Galilien, Lilia Ursus, Mirela Niecień, Moktar Gradon, Pietro Dwarczan, Pięknotka Diakon, Waleria Cyklon, Zbigniew Burzycki

Streszczenie:

Pięknotka zdobyła nowe ciało dla Minerwy (acz do jego uruchomienia człowiek musi być poświęcony!), przekonała Amadeusza do założenia świątyni Arazille do blokowania Finis Vitae oraz wepchnęła Lilię jako asystentkę Atenie. Do tego rozkochała solidnie Erwina i zamknęła wszystkie wątki, by móc wracać do Pustogoru. Nie dała rady rozdzielić Julii i Łysych Psów, niestety.

Aktor w Opowieści:

* Dokonanie:
    * wpierw dał się porwać Walerii, potem nie rozumiał Cieniaszczytu i na końcu nie zrobił nic produktywnego - ale jest szczęśliwy z Pięknotką.
* Progresja:
    * uleczony od wpływów Saitaera, zakochany w Pięknotce (choć nadal ma serce do Minerwy).


### Czyszczenie toksycznych związków

* **uid:** 181225-czyszczenie-toksycznych-zwiazkow, _numer względny_: 7
* **daty:** 0109-11-13 - 0109-11-17
* **obecni:** Atena Sowińska, Bogdan Szerl, Julia Morwisz, Moktar Gradon, Pięknotka Diakon, Romuald Czurukin

Streszczenie:

Pięknotka ściągnęła Erwina na detoks od Saitaera, acz dała mu informację o Kreacjuszu Diakonie. Saitaer planuje zbudować sprawne ciało dla Minerwy. Atena jest zregenerowana w dość anielskiej formie i dostaje informacje o Orbiterze od Julii. Pięknotka próbuje naprawić pary Romuald x Julia oraz Wioletta x Pietro. Moktar uwalnia Pięknotkę od wpływu Bogdana, masakrując swojego agenta w brutalny sposób.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * poszedł na rekonstrukcję (usunięcie Saitaera). Zakochał się w Pięknotce - nie jest już tylko przyjaciółką.


### Decyzja Minerwy

* **uid:** 181024-decyzja-minerwy, _numer względny_: 6
* **daty:** 0109-10-01 - 0109-10-04
* **obecni:** Adam Szarjan, Adela Kirys, Lilia Ursus, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Atena i Erwin są w ruinie. Tymczasem servar Erwina wyrwał się na wolność. Okazało się, że Minerwa - dawna miłość Erwina - powróciła. Adam próbował ją zneutralizować a Pięknotka - uratować. Skończyło się na tym, że Minerwa, Nutka i terrorform współistnieją w servarze Erwina, Adama boli głowa a Pięknotka cieszy się, że tymczasowo to nie jej problem. Mistrzyni psychotroniki wróciła.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * utracił Nutkę, ale zyskał Nutkę + Minerwę + terrorforma jako swój servar. A był tylko nieprzytomny...


### Powrót Minerwy z terrorforma

* **uid:** 181021-powrot-minerwy-z-terrorforma, _numer względny_: 5
* **daty:** 0109-09-24 - 0109-09-29
* **obecni:** Adam Szarjan, Atena Sowińska, Erwin Galilien, Lilia Ursus, Pięknotka Diakon

Streszczenie:

Adam Szarjan, dawny kolega Erwina pojawił się by uratować go przed problemami z Nutką. Okazało się, że Nutka jest śmiertelnym zagrożeniem pod wpływem energii Nojrepów. Wraz z Ateną i Pięknotką udało im się zneutralizować problem, ale w miejsce Nutki pojawiła się Minerwa - prawdziwa osoba, na bazie której powstała Nutka. Atena i Erwin w bardzo ciężkim stanie trafili do szpitala.

Aktor w Opowieści:

* Dokonanie:
    * sam się ograniczał, by Nutce nie stała się krzywda. Bardzo ciężko ranny osłaniając Pięknotkę w walce z terrorformem. Stracił Nutkę, odzyskał Minerwę.
* Progresja:
    * jego servar (Nutka) jest zastąpiony przez Minerwę.
    * ma połączenie mentalne pomiędzy sobą a Minerwą.
    * bardzo ciężko ranny; co najmniej tydzień w szpitalu + rehabilitacja.


### Lilia na Trzęsawisku

* **uid:** 181003-lilia-na-trzesawisku, _numer względny_: 4
* **daty:** 0109-09-20 - 0109-09-22
* **obecni:** Erwin Galilien, Lilia Ursus, Pięknotka Diakon

Streszczenie:

Erwin ucieka przed Lilią, bo nie chce wziąć jej do Szczeliny. Pięknotka nie była w stanie pogodzić Erwina i Lilii - wzięła ich więc na Trzęsawisko Zjawosztup polować na Anomalie. Niestety, servar Lili został uszkodzony i wszystkie zyski zjadła naprawa. Przy okazji, wyszła sprawa historii Erwina Galiliena oraz Nutki. I Lilii, która lekko zabujała się w Erwinie.

Aktor w Opowieści:

* Dokonanie:
    * skutecznie ucieka przed Lilią, zdradził Pięknotce kilka swoich sekretów z przeszłości i karnie współpracował na Trzęsawisku.
* Progresja:
    * ma ogromne długi, które od kupiła Lilia Ursus. Efektywnie, nie jest panem własnego losu.
    * zamodelował i stworzył Nutkę na wzór swojej dawnej partnerki która zginęła w Szczelinie. Nutka jest o niego zazdrosna.


### Dwa tygodnie szkoły

* **uid:** 180929-dwa-tygodnie-szkoly, _numer względny_: 3
* **daty:** 0109-09-17 - 0109-09-19
* **obecni:** Adela Kirys, Arnulf Poważny, Atena Sowińska, Erwin Galilien, Felicja Melitniek, Ignacy Myrczek, Miedwied Zajcew, Napoleon Bankierz, Pięknotka Diakon, Roland Grzymość

Streszczenie:

Felicja uczy się w szkole magów, ale nie ma przyjaciół i przez to zachowuje się chaotycznie. Erwin Galilien wpakował się w kłopoty i przegrał power suit "Nutkę" w Kasynie; ma przynieść niebezpieczne grzyby. Uczniowie ze szkoły magów też chodzą do tego Kasyna. Połączone siły Zespołu i Ateny wyplątały Erwina z kłopotów i utrudniły Kasynu zwabianie młodych uczniów ze szkoły magów.

Aktor w Opowieści:

* Dokonanie:
    * podróżując po Szczelinie wpierw wpadł w kłopoty, coś mu najpewniej podali, stracił Nutkę (postawił w kasynie), odzyskał Nutkę i zdobył grzyby. Po tym - OPR.
* Progresja:
    * przeciorany za wyciągnięcie niebezpiecznych grzybów ze Szczeliny Pustogorskiej. Upiekło mu się.


### Protomag z Trzęsawisk

* **uid:** 180817-protomag-z-trzesawisk, _numer względny_: 2
* **daty:** 0109-09-07 - 0109-09-09
* **obecni:** Alan Bartozol, Atena Sowińska, Erwin Galilien, Felicja Melitniek, Lucjusz Blakenbauer, Miedwied Zajcew, Pięknotka Diakon

Streszczenie:

Protomag skrzywdził kierowcę z lokalnej mafii Zajcewów. Miedwied i Pięknotka poszli szukać Felicji - protomaga na której eksperymentowali - na Trzęsawisku Zjawosztup. Tam napotkali na terminusów Czerwonych Myszy którzy też chcieli Felicję. Współpraca Zespołu z Ateną Sowińską doprowadziła do przekazania Felicji im i Miedwied został jej opiekunem. Niestety, Trzęsawisko w wyniku pojawienia się Felicji się rozpaliło - jest gorące i będzie emitować Skażeńce...

Aktor w Opowieści:

* Dokonanie:
    * główne źródło informacji Pięknotki w okolicy, na absolutnie każdy istotny temat


### Komary i kosmetyki

* **uid:** 180815-komary-i-kosmetyki, _numer względny_: 1
* **daty:** 0109-09-02 - 0109-09-05
* **obecni:** Adela Kirys, Erwin Galilien, Pięknotka Diakon

Streszczenie:

Adela Kirys wpadła w kłopoty. Stworzyła komary napromieniowujące ludzi na terenie Czystym. Sęk w tym, że poszlaki wskazywały na Pięknotkę. Pięknotka ostrzeżona przez Galiliena odkryła problem - po czym poprosiła Erwina o Skażenie kremu tworzącego te komary (włam + podrzut). Sukces - Adela musiała wycofać całą partię a reputacja Pięknotki nie ucierpiała. A i Nurek Szczeliny się przydał.

Aktor w Opowieści:

* Dokonanie:
    * próbuje chronić reputację Pięknotki, ostrzega ją przed potencjalnym problemem i finalnie włamuje się do Adeli zatruwając jej krem tworzący komary.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 20, @: 0110-09-04
    1. Primus    : 20, @: 0110-09-04
        1. Sektor Astoriański    : 20, @: 0110-09-04
            1. Astoria    : 20, @: 0110-09-04
                1. Sojusz Letejski, NW    : 1, @: 0109-12-01
                    1. Ruiniec    : 1, @: 0109-12-01
                        1. Colubrinus Psiarnia    : 1, @: 0109-12-01
                        1. Świątynia Bez Dna    : 1, @: 0109-12-01
                1. Sojusz Letejski, SW    : 1, @: 0110-04-09
                    1. Granica Anomalii    : 1, @: 0110-04-09
                        1. Pacyfika    : 1, @: 0110-04-09
                1. Sojusz Letejski    : 19, @: 0110-09-04
                    1. Przelotyk    : 1, @: 0109-12-01
                        1. Przelotyk Wschodni    : 1, @: 0109-12-01
                            1. Cieniaszczyt    : 1, @: 0109-12-01
                                1. Knajpka Szkarłatny Szept    : 1, @: 0109-12-01
                                1. Kompleks Nukleon    : 1, @: 0109-12-01
                                1. Mrowisko    : 1, @: 0109-12-01
                    1. Szczeliniec    : 18, @: 0110-09-04
                        1. Powiat Jastrzębski    : 1, @: 0110-07-03
                            1. Kalbark    : 1, @: 0110-07-03
                                1. Autoklub Piękna    : 1, @: 0110-07-03
                        1. Powiat Pustogorski    : 17, @: 0110-09-04
                            1. Czemerta, okolice    : 1, @: 0110-09-04
                                1. Fortifarma Irrydia    : 1, @: 0110-09-04
                                1. Studnia Irrydiańska    : 1, @: 0110-09-04
                            1. Czółenko    : 2, @: 0110-07-03
                                1. Bunkry    : 2, @: 0110-07-03
                                1. Tancbuda    : 1, @: 0110-05-09
                            1. Podwiert, obrzeża    : 1, @: 0110-03-17
                                1. Kompleks Badawczy Skelidar    : 1, @: 0110-03-17
                            1. Przywiesław    : 1, @: 0109-09-05
                                1. Przychodnia    : 1, @: 0109-09-05
                            1. Pustogor    : 11, @: 0110-09-04
                                1. Barbakan    : 1, @: 0110-01-26
                                1. Eksterior    : 4, @: 0110-07-03
                                    1. Dolina Uciech    : 1, @: 0110-04-05
                                    1. Fort Mikado    : 1, @: 0110-04-05
                                    1. Miasteczko    : 4, @: 0110-07-03
                                        1. Knajpa Górska Szalupa    : 1, @: 0110-04-03
                                1. Gabinet Pięknotki    : 5, @: 0110-09-04
                                1. Interior    : 3, @: 0110-04-13
                                    1. Bunkry Barbakanu    : 2, @: 0110-04-13
                                    1. Dzielnica Mieszkalna    : 1, @: 0110-04-03
                                    1. Laboratorium Senetis    : 2, @: 0110-04-13
                                1. Kawiarenka Ciemna Strona    : 1, @: 0110-01-26
                                1. Knajpa Górska Szalupa    : 2, @: 0110-09-04
                                1. Kompleks Testowy    : 1, @: 0109-09-29
                                1. Miasteczko    : 1, @: 0109-09-22
                                1. Pustułka    : 1, @: 0109-09-29
                                1. Rdzeń    : 4, @: 0110-04-13
                                    1. Barbakan    : 2, @: 0110-04-05
                                    1. Szpital Terminuski    : 2, @: 0110-04-13
                            1. Zaczęstwo    : 6, @: 0110-08-26
                                1. Akademia Magii, kampus    : 2, @: 0110-08-26
                                    1. Budynek Centralny    : 1, @: 0110-08-26
                                1. Cyberszkoła    : 2, @: 0110-08-26
                                1. Hotel Tellur    : 1, @: 0110-02-23
                                1. Kasyno Marzeń    : 1, @: 0109-09-19
                                1. Nieużytki Staszka    : 4, @: 0110-08-26
                                1. Osiedle Ptasie    : 2, @: 0110-07-03
                                1. Sypialnia Szczelińca    : 1, @: 0110-08-26
                                1. Wschodnie Pole Namiotowe    : 1, @: 0110-02-23
                        1. Powiat Złotodajski    : 1, @: 0110-07-09
                            1. Złotkordza    : 1, @: 0110-07-09
                                1. Stadion    : 1, @: 0110-07-09
                        1. Trzęsawisko Zjawosztup    : 5, @: 0110-05-09
                            1. Głodna Ziemia    : 2, @: 0110-01-30
                            1. Laboratorium W Drzewie    : 1, @: 0110-01-30

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 20 | ((180815-komary-i-kosmetyki; 180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly; 181003-lilia-na-trzesawisku; 181021-powrot-minerwy-z-terrorforma; 181227-adieu-cieniaszczycie; 190101-morderczyni-jednej-plotki; 190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 190210-minerwa-i-kwiaty-nadziei; 190402-eksperymentalny-power-suit; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190427-zrzut-w-pacyfice; 190429-sabotaz-szeptow-elizy; 190527-mimik-sni-o-esuriit; 190817-kwiaty-w-sluzbie-puryfikacji; 191105-zaginiona-soniczka; 200425-inflitrator-poluje-na-tai-minerwy; 200509-rekin-z-aurum-i-fortifarma)) |
| Minerwa Metalia      | 8 | ((190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 190210-minerwa-i-kwiaty-nadziei; 190402-eksperymentalny-power-suit; 190424-budowa-ixionskiego-mimika; 190427-zrzut-w-pacyfice; 190429-sabotaz-szeptow-elizy; 200425-inflitrator-poluje-na-tai-minerwy)) |
| Atena Sowińska       | 5 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly; 181021-powrot-minerwy-z-terrorforma; 181227-adieu-cieniaszczycie; 190210-minerwa-i-kwiaty-nadziei)) |
| Karla Mrozik         | 4 | ((190120-nowa-minerwa-w-nowym-swiecie; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Olaf Zuchwały        | 4 | ((190101-morderczyni-jednej-plotki; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Adela Kirys          | 3 | ((180815-komary-i-kosmetyki; 180929-dwa-tygodnie-szkoly; 190527-mimik-sni-o-esuriit)) |
| Alan Bartozol        | 3 | ((180817-protomag-z-trzesawisk; 190101-morderczyni-jednej-plotki; 190429-sabotaz-szeptow-elizy)) |
| Ataienne             | 3 | ((190402-eksperymentalny-power-suit; 190817-kwiaty-w-sluzbie-puryfikacji; 191105-zaginiona-soniczka)) |
| Lilia Ursus          | 3 | ((181003-lilia-na-trzesawisku; 181021-powrot-minerwy-z-terrorforma; 181227-adieu-cieniaszczycie)) |
| Tymon Grubosz        | 3 | ((190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 191105-zaginiona-soniczka)) |
| Aleksander Rugczuk   | 2 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Felicja Melitniek    | 2 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly)) |
| Gabriel Ursus        | 2 | ((200425-inflitrator-poluje-na-tai-minerwy; 200509-rekin-z-aurum-i-fortifarma)) |
| Kasjopea Maus        | 2 | ((190120-nowa-minerwa-w-nowym-swiecie; 190210-minerwa-i-kwiaty-nadziei)) |
| Kornel Garn          | 2 | ((190210-minerwa-i-kwiaty-nadziei; 190817-kwiaty-w-sluzbie-puryfikacji)) |
| Lucjusz Blakenbauer  | 2 | ((180817-protomag-z-trzesawisk; 190429-sabotaz-szeptow-elizy)) |
| Miedwied Zajcew      | 2 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly)) |
| Napoleon Bankierz    | 2 | ((180929-dwa-tygodnie-szkoly; 190127-ixionski-transorganik)) |
| Wiktor Satarail      | 2 | ((190127-ixionski-transorganik; 190527-mimik-sni-o-esuriit)) |
| Adam Szarjan         | 1 | ((181021-powrot-minerwy-z-terrorforma)) |
| Aleksander Iczak     | 1 | ((190101-morderczyni-jednej-plotki)) |
| Amadeusz Sowiński    | 1 | ((181227-adieu-cieniaszczycie)) |
| Arnulf Poważny       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Artur Kołczond       | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Artur Michasiewicz   | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Brygida Maczkowik    | 1 | ((181227-adieu-cieniaszczycie)) |
| Diana Tevalier       | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Eliza Ira            | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Ignacy Myrczek       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Kaja Selerek         | 1 | ((190402-eksperymentalny-power-suit)) |
| Karol Szurnak        | 1 | ((190101-morderczyni-jednej-plotki)) |
| Karolina Erenit      | 1 | ((190127-ixionski-transorganik)) |
| Kirył Najłalmin      | 1 | ((190127-ixionski-transorganik)) |
| Laura Tesinik        | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Marcel Nieciesz      | 1 | ((190422-pustogorski-konflikt)) |
| Mariola Tralment     | 1 | ((191105-zaginiona-soniczka)) |
| Mariusz Trzewń       | 1 | ((191105-zaginiona-soniczka)) |
| Mateusz Kardamacz    | 1 | ((191105-zaginiona-soniczka)) |
| Mirela Niecień       | 1 | ((181227-adieu-cieniaszczycie)) |
| Mirela Satarail      | 1 | ((190527-mimik-sni-o-esuriit)) |
| Moktar Gradon        | 1 | ((181227-adieu-cieniaszczycie)) |
| Natalia Tessalon     | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Nataniel Marszalnik  | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Nikola Kirys         | 1 | ((190427-zrzut-w-pacyfice)) |
| Pietro Dwarczan      | 1 | ((181227-adieu-cieniaszczycie)) |
| Roland Grzymość      | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Sabina Kazitan       | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Saitaer              | 1 | ((190127-ixionski-transorganik)) |
| Strażniczka Alair    | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Szymon Oporcznik     | 1 | ((190402-eksperymentalny-power-suit)) |
| Tadeusz Tessalon     | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Teresa Marszalnik    | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Teresa Mieralit      | 1 | ((190101-morderczyni-jednej-plotki)) |
| Tomasz Tukan         | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Waleria Cyklon       | 1 | ((181227-adieu-cieniaszczycie)) |
| Wojmił Siwywilk      | 1 | ((190422-pustogorski-konflikt)) |
| Wojtek Kurczynos     | 1 | ((190127-ixionski-transorganik)) |
| Zbigniew Burzycki    | 1 | ((181227-adieu-cieniaszczycie)) |