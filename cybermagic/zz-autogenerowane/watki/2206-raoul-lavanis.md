# Raoul Lavanis
## Identyfikator

Id: 2206-raoul-lavanis

## Sekcja Opowieści

### Lewiatan za Pandorę

* **uid:** 220622-lewiatan-za-pandore, _numer względny_: 10
* **daty:** 0112-09-30 - 0112-10-03
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Ola Szerszeń, OO Infernia, OO Pandora, OO Straszliwy Pająk, OO Tivr, Raoul Lavanis, Rzeźnik Parszywiec Diakon

Streszczenie:

PRZESZŁOŚĆ: Ola Szerszeń opiekuje się Eleną w Regeneratorze; okazuje się być biosyntem (zakazane eksperymenty Termii). Rzeźnik Diakon to łagodny i jowialny kapitan Straszliwego Pająka, acz znienawidzony za przeszłość zdrajcy Orbitera oraz Noctis. TERAŹNIEJSZOŚĆ: Lewiatan zostaje zestrzelony torpedą anihilacyjną, dzięki Raoulowi ratujemy wielu ludzi na orbicie Lewiatana. Malictrix d'Pandora zostaje zniszczona przez Lewiatana, ale Klaudia ma jego mikrokopię dzięki Ariannie.

Aktor w Opowieści:

* Dokonanie:
    * wszedł na pokład jednostek tworzących Lewiatana i uratował tyle osób ile się dało; świetny advancer. Spacer kosmiczny, uruchamianie kapsuł ratunkowych itp.


### Ratujemy porywaczy Eleny

* **uid:** 220610-ratujemy-porywaczy-eleny, _numer względny_: 9
* **daty:** 0112-09-15 - 0112-09-17
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Hestia d'Atropos, Jakub Bulgocz, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Raoul Lavanis

Streszczenie:

Elena jest już zbyt zdestabilizowana by funkcjonować niezależnie, więc wpierw zajęli się nią w Laboratorium Kranix a po zbudowaniu dla niej Containment Chamber przesłali ją do stacji medycznej Atropos do regeneracji. Stamtąd ją i Martyna porwał Syndykat Aureliona (ale Martyn przewidział JAKIŚ problem i ostrzegł Klaudię). Elena by zniszczyła okręt napastników, ale Zespół skutecznie odkrył problemy na Atropos, przesłuchali dowódcę, zneutralizowali miragenta i dotarli do okrętu Aureliona zdobywając jeńców i zatrzymując Elenę przed przebudzeniem i Spustoszeniem statku.

Aktor w Opowieści:

* Dokonanie:
    * bardzo zauważliwy; zauważył, że "Elena" (miragent) jest źle podpięty do Containment Chamber. A potem w kosmosie zrobił insercję i wszedł na statek Aureliona z Leoną i Eustachym.


### EtAur i przynęta na Kryptę

* **uid:** 220330-etaur-i-przyneta-na-krypte, _numer względny_: 8
* **daty:** 0112-07-02 - 0112-07-05
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Lamia Akacja, Leszek Czarban, Maria Naavas, Raoul Lavanis

Streszczenie:

Okazało się, że "potwór" to były faeril z Neikatis zarażony bronią biologiczną. Próbował doprowadzić do katastrofy by Orbiter lub Eternia zabrali się za leczenie tego cholerstwa, pomagał mu Antoni, p.o. TAI na EtAur. Maria doszła do biostruktury, Klaudia przejęła systemy bazy, Eustachy przechwycił gościa, Arianna zdobyła bazę politycznie. A Klaudia zrobiła raport, by Orbiter nie atakował więcej neutralnych i potencjalnie sojuszniczych sił nie-Orbiterowych w kosmosie. Jako, że Lamii nic się nie stało, Melania jest szczęśliwa.

Aktor w Opowieści:

* Dokonanie:
    * chroni Marię na EtAur; odparł z Eustachym "potwora", po czym ucierpiał jak Eustachy wypalał fagi (phage) Marii.


### Polityka rujnuje Pallidę Voss

* **uid:** 220216-polityka-rujnuje-pallide-voss, _numer względny_: 7
* **daty:** 0112-06-01 - 0112-06-03
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Mateus Sarpon, Raoul Lavanis, SP Pallida Voss, Szczepan Kaltaben

Streszczenie:

Arianna we współpracy z Mateusem planują uratować zakładników pallidańskich od radykałów keldańskich. Ale by to osiągnąć Eustachy i Klaudia infiltrują wpierw Pallidę Voss, bo okazuje się, że Annikę wrabiają jej własni podwładni. Klaudia zdobyła wszystkie potrzebne dowody, Eustachy chciał ratować ludzi... i Paradoks (po sabotowaniu generatorów Memoriam) spowodował straszne straty w ludziach i przesunął Pallidę Voss w Anomalię Kosmiczną. Oops. Ale dzięki drakolitom (keldanitom) udało się sporo ludzi uratować.

Aktor w Opowieści:

* Dokonanie:
    * infiltruje z Eustachym Pallidę Voss; wpadł w kłopoty bo nie ufali Annice (karta była prawidłowa), ale sam się uwolnił.


### Po drugiej stronie Bramy

* **uid:** 210707-po-drugiej-stronie-bramy, _numer względny_: 6
* **daty:** 0111-05-11 - 0111-05-13
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Janus Krzak, Klaudia Stryk, Martyn Hiwasser, Raoul Lavanis, Tal Marczak

Streszczenie:

Infernia próbując przetransportować eksperta Eteru na konferencję przez Bramę uległa katastrofie - przeniosła się GDZIEŚ INDZIEJ, pod wodę i zniknęła Klaudia. Nie uległa katastrofalnym uszkodzeniom, ale po zachęceniu pintką Lewiatana dotarła do archaicznej podwodnej noktiańskiej bazy. Tam znajduje się Klaudia, ale coś jest z nią nie tak. Sprzęgnięta z bazą? Tak czy inaczej, infiltracja bazy dopiero się zacznie. Trzeba znaleźć Klaudię i wrócić do domu.

Aktor w Opowieści:

* Dokonanie:
    * ludzki noktiański advancer Inferni; wraz z Eleną rozpracował starą bazę noktiańską. W grupie roboczej tłumaczącej sigile tej dziwnej archaicznej bazy.


### Anomalna Mavidiz?

* **uid:** 240131-anomalna-mavidiz, _numer względny_: 5
* **daty:** 0111-01-03 - 0111-01-08
* **obecni:** Antoni Bladawir, Arianna Verlen, Borys Kragin, Eustachy Korkoran, Grigor Tarnow, Igor Stratos, Karl Murnoff, Klaudia Stryk, Markus Wąż, ONS Mavidiz, OO Tivr, Raoul Lavanis, Rita Stratos, Tara Ogniczek

Streszczenie:

Bladawir skierował Ariannę na SCA Mavidiz używając OO Tivr. Tam - o czym nikt nie wie - jest Nihilosekt. Madiviz jest anomalnym statkiem - co gorsza coś jest nie tak z załogą wpadającą w obsesje i pętle; pojawiają się halucynacje i z ludźmi coś jest nie tak. Gdy Zespół zostaje zaatakowany, z trudem sabotują jednostkę i robią ostatni przyczółek przy silnikach. Eustachemu udaje się oddzielić 'zarażony' fragment jednostki i Mavidiz - co prawda ciężko zniszczona - przetrwała.

Aktor w Opowieści:

* Dokonanie:
    * połączył Tivr i Mavidiz siatką jako advancer; gdy Eustachy wypadł w kosmos walcząc z nihilosektem, przechwycił Eustachego na pokład Tivra, ratując mu życie.


### Mikiptur, zemsta Woltaren

* **uid:** 240124-mikiptur-zemsta-woltaren, _numer względny_: 4
* **daty:** 0110-12-29 - 0111-01-01
* **obecni:** Antoni Bladawir, Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Lars Kidironus, OA Mikiptur, OO Isratazir, Raoul Lavanis, Władawiec Diakon

Streszczenie:

Syndykat zdecydował się zaatakować Bladawira i wysłał Dewastatora przy użyciu Mikiptur jako pułapki. Skutecznie zniszczył Isratazir. Arianna śpi, więc Eustachy (nie spiesząc się) przyleciał ratować Isratazir. Infernia rozmontowała miny i wyciągnęła kogo się dało, mimo, że Mikiptur uszkodził Sarkalin. Klaudia i Arianna zrobiły co mogły, by ograniczyć polityczny fallout. Arianna ujawniła obecność Syndykatu, ratując co się da z reputacji Bladawira i jednocząc Orbiter.

Aktor w Opowieści:

* Dokonanie:
    * odzyskał kapsułę z danymi z ataku na Isratazir, co było kluczowe dla morale załogi i gromadzenia informacji na temat wroga.


### Bladawir kontra przemyt tienów

* **uid:** 231220-bladawir-kontra-przemyt-tienow, _numer względny_: 3
* **daty:** 0110-12-06 - 0110-12-11
* **obecni:** Antoni Bladawir, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Ewa Razalis, Kazimierz Darbik, Klaudia Stryk, Marcinozaur Verlen, OO Infernia, OO Karsztarin, Raoul Lavanis, Szymon Orzesznik, Zaara Mieralit

Streszczenie:

Komodor Bladawir postanowił usunąć przemyt tienów z Karsztarina (najpewniej Program Kosmiczny Aurum), każąc swoim jednostkom robić wyczerpujące patrole i przeszukiwanie jednostek pod kątem owego przemytu. Tempo jest zabójcze nawet dla Arianny i Inferni - zwłaszcza, że Arianna nie wie po co to wszystko robi. Śledztwo Klaudii pokazuje, że Bladawir najpewniej umieścił potwora na Karsztarinie by zmusić przemytników do rozpaczliwego ruchu. Arianna nie może nikogo ostrzec, ale używa połączenia Krwi z Eleną - i w ten sposób dowiaduje się o roli Orzesznika. Klaudia dyskretnie przekazuje informacje Ewie Razalis. Operacja Bladawira się nie tylko nie udaje, ale trafił na dywanik i jego plan się rozsypał. Bladawir powiedział Ariannie, że ona ALBO będzie z nim współpracować albo on jej nie chce. Nadal nie wie, że to Arianna jest architektem jego porażki.

Aktor w Opowieści:

* Dokonanie:
    * jako advancer robił operację poszukiwania przemytu na kadłubie innej jednostki. Mimo że szukał 'specjalnego', to znalazł 'zwykły' przez co Arianna ma kłopot. Plus, nawet on nie miał jak tego robić.


### Śpiew NieLalki na Castigatorze

* **uid:** 231025-spiew-nielalki-na-castigatorze, _numer względny_: 2
* **daty:** 0110-10-24 - 0110-10-26
* **obecni:** Anna Tessalon, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Feliks Walrond, Igor Arłacz, Klaudia Stryk, Konstanty Keksik, Leona Astrienko, Leszek Kurzmin, Marta Keksik, OO Castigator, OO Infernia, Patryk Samszar, Raoul Lavanis, TAI Eszara d'Castigator

Streszczenie:

Na pokład Castigatora tieni przemycili NieLalkę Altarient i zaczęły mieszać się Znaczenia oraz Rzeczywistość. Zespół doprowadził do kontrolowanego opanowania i wygaszania Castigatora, po czym po zlokalizowaniu Anomalii Altarient Raoul został wysłany by ją zaizolować. Infernia ją zestrzeliła. A Arianna wykorzystała tą okazję by młodzi tieni wzięli się w garść i nie robili głupot. Szczęśliwie, Admiralicja nic nie wie, acz było nieciekawie przez moment.

Aktor w Opowieści:

* Dokonanie:
    * nieskończenie opanowany advancer, wierzy, że da się wszystko zrozumieć ale przede wszystkim WYKONUJE ROZKAZY DO LITERY nawet jak ich nie rozumie. Wszedł w głąb anomalii Altarient i mimo że stracił pojęcie pudełka, idąc precyzyjnie zgodnie z poleceniami rozwiązał problem. Niezwykle precyzyjny, cierpliwy i stoicki. MVP sesji - ani Martyn ani Elena nie daliby rady.


### Ekstaflos na Tezifeng

* **uid:** 231011-ekstaflos-na-tezifeng, _numer względny_: 1
* **daty:** 0110-10-20 - 0110-10-22
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudiusz Terienak, Lars Kidironus, Leona Astrienko, Leszek Kurzmin, Natalia Gwozdnik, OO Castigator, OO Infernia, OO Tezifeng, Raoul Lavanis

Streszczenie:

Jeszcze na Castigatorze, Arianna i Kurzmin z przyjemnością odbudowali znajomość i wspominki. Gdy Elena chcąc ratować Leonę 'zabiła' jej anomalię, Leona wyzwała ją na pojedynek. Elena przegrała, choć było blisko; będzie musiała być służką Leony przez pewien czas, co Castigator odnotował z radością. Kurzmin poprosił Ariannę o znalezienie i pomoc dwóm tienom w rutynowym patrolu (najpewniej przekroczyli paliwo). Niestety, Tezifeng, korweta Castigatora podczas operacji celnych natrafiła na coś kralotycznego. Klaudiusz Terienak został zmieniony w ekstaflos a Natalia Gwozdnik została porwana. Pułapka na Infernię została wykryta przez Ariannę (bo Klaudiusz zachowywał się nie tak jak powinien). Szybka operacja wejścia - wydobycia jednego jeńca (marionetki kralotycznej) i zbadanie sytuacji sprawiła, że Eustachy działkami unieszkodliwił Tezifeng i Arianna wezwała wsparcie - kraloth to coś bardzo wysokiej klasy.

Aktor w Opowieści:

* Dokonanie:
    * desygnowany przez Ariannę do bycia dywersją na pintce. Zadanie spełnił bez słowa i bez błędu. Niewidoczny i skuteczny.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 10, @: 0112-10-03
    1. Primus    : 10, @: 0112-10-03
        1. Sektor Astoriański    : 10, @: 0112-10-03
            1. Astoria, Orbita    : 2, @: 0110-10-26
            1. Astoria, Pierścień Zewnętrzny    : 4, @: 0112-09-17
                1. Laboratorium Kranix    : 1, @: 0112-09-17
                1. Stacja Medyczna Atropos    : 1, @: 0112-09-17
            1. Brama Kariańska    : 1, @: 0111-05-13
            1. Elang, księżyc Astorii    : 1, @: 0112-07-05
                1. EtAur Zwycięska    : 1, @: 0112-07-05
                    1. Magazyny wewnętrzne    : 1, @: 0112-07-05
                    1. Sektor bioinżynierii    : 1, @: 0112-07-05
                        1. Biolab    : 1, @: 0112-07-05
                        1. Komory żywieniowe    : 1, @: 0112-07-05
                        1. Skrzydło medyczne    : 1, @: 0112-07-05
                    1. Sektor inżynierski    : 1, @: 0112-07-05
                        1. Panele słoneczne i bateriownia    : 1, @: 0112-07-05
                        1. Podtrzymywanie życia    : 1, @: 0112-07-05
                        1. Serwisownia    : 1, @: 0112-07-05
                        1. Stacja pozyskiwania wody    : 1, @: 0112-07-05
                    1. Sektor mieszkalny    : 1, @: 0112-07-05
                        1. Barbakan    : 1, @: 0112-07-05
                            1. Administracja    : 1, @: 0112-07-05
                            1. Ochrona    : 1, @: 0112-07-05
                            1. Rdzeń AI    : 1, @: 0112-07-05
                        1. Centrum Rozrywki    : 1, @: 0112-07-05
                        1. Podtrzymywanie życia    : 1, @: 0112-07-05
                        1. Pomieszczenia mieszkalne    : 1, @: 0112-07-05
                        1. Przedszkole    : 1, @: 0112-07-05
                        1. Stołówki    : 1, @: 0112-07-05
                        1. Szklarnie    : 1, @: 0112-07-05
                    1. Sektor przeładunkowy    : 1, @: 0112-07-05
                        1. Atraktory małych pakunków    : 1, @: 0112-07-05
                        1. Magazyny    : 1, @: 0112-07-05
                        1. Pieczara Gaulronów    : 1, @: 0112-07-05
                        1. Punkt celny    : 1, @: 0112-07-05
                        1. Stacja grazerów    : 1, @: 0112-07-05
                        1. Stacja przeładunkowa    : 1, @: 0112-07-05
                        1. Starport    : 1, @: 0112-07-05
                    1. Ukryte sektory    : 1, @: 0112-07-05
            1. Iorus    : 1, @: 0112-06-03
                1. Iorus, pierścień    : 1, @: 0112-06-03
                    1. Keldan Voss    : 1, @: 0112-06-03
            1. Libracja Lirańska    : 1, @: 0112-10-03
                1. Anomalia Kolapsu, orbita    : 1, @: 0112-10-03
        1. Zagubieni w Kosmosie    : 1, @: 0111-05-13
            1. Crepuscula    : 1, @: 0111-05-13
                1. Pasmo Zmroku    : 1, @: 0111-05-13
                    1. Baza Noktiańska Zona Tres    : 1, @: 0111-05-13
                    1. Podwodna Brama Eteryczna    : 1, @: 0111-05-13

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 10 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220622-lewiatan-za-pandore; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231220-bladawir-kontra-przemyt-tienow; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Eustachy Korkoran    | 10 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220622-lewiatan-za-pandore; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231220-bladawir-kontra-przemyt-tienow; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Klaudia Stryk        | 9 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220622-lewiatan-za-pandore; 231025-spiew-nielalki-na-castigatorze; 231220-bladawir-kontra-przemyt-tienow; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Elena Verlen         | 6 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220610-ratujemy-porywaczy-eleny; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231220-bladawir-kontra-przemyt-tienow)) |
| OO Infernia          | 4 | ((220622-lewiatan-za-pandore; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231220-bladawir-kontra-przemyt-tienow)) |
| Antoni Bladawir      | 3 | ((231220-bladawir-kontra-przemyt-tienow; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Leona Astrienko      | 3 | ((220610-ratujemy-porywaczy-eleny; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Lars Kidironus       | 2 | ((231011-ekstaflos-na-tezifeng; 240124-mikiptur-zemsta-woltaren)) |
| Leszek Kurzmin       | 2 | ((231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Maria Naavas         | 2 | ((220330-etaur-i-przyneta-na-krypte; 220622-lewiatan-za-pandore)) |
| Martyn Hiwasser      | 2 | ((210707-po-drugiej-stronie-bramy; 220610-ratujemy-porywaczy-eleny)) |
| OO Castigator        | 2 | ((231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| OO Tivr              | 2 | ((220622-lewiatan-za-pandore; 240131-anomalna-mavidiz)) |
| Anna Tessalon        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Annika Pradis        | 1 | ((220216-polityka-rujnuje-pallide-voss)) |
| Borys Kragin         | 1 | ((240131-anomalna-mavidiz)) |
| Ewa Razalis          | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Feliks Walrond       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Grigor Tarnow        | 1 | ((240131-anomalna-mavidiz)) |
| Hestia d'Atropos     | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Igor Arłacz          | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Igor Stratos         | 1 | ((240131-anomalna-mavidiz)) |
| Jakub Bulgocz        | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Janus Krzak          | 1 | ((210707-po-drugiej-stronie-bramy)) |
| Karl Murnoff         | 1 | ((240131-anomalna-mavidiz)) |
| Kazimierz Darbik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Klaudiusz Terienak   | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Konstanty Keksik     | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Lamia Akacja         | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Leszek Czarban       | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Marcinozaur Verlen   | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Markus Wąż           | 1 | ((240131-anomalna-mavidiz)) |
| Marta Keksik         | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Mateus Sarpon        | 1 | ((220216-polityka-rujnuje-pallide-voss)) |
| Natalia Gwozdnik     | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OA Mikiptur          | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Ola Szerszeń         | 1 | ((220622-lewiatan-za-pandore)) |
| ONS Mavidiz          | 1 | ((240131-anomalna-mavidiz)) |
| OO Isratazir         | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| OO Karsztarin        | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| OO Pandora           | 1 | ((220622-lewiatan-za-pandore)) |
| OO Straszliwy Pająk  | 1 | ((220622-lewiatan-za-pandore)) |
| OO Tezifeng          | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Patryk Samszar       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Rita Stratos         | 1 | ((240131-anomalna-mavidiz)) |
| Rzeźnik Parszywiec Diakon | 1 | ((220622-lewiatan-za-pandore)) |
| SP Pallida Voss      | 1 | ((220216-polityka-rujnuje-pallide-voss)) |
| Szczepan Kaltaben    | 1 | ((220216-polityka-rujnuje-pallide-voss)) |
| Szymon Orzesznik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| TAI Eszara d'Castigator | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Tal Marczak          | 1 | ((210707-po-drugiej-stronie-bramy)) |
| Tara Ogniczek        | 1 | ((240131-anomalna-mavidiz)) |
| Władawiec Diakon     | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Zaara Mieralit       | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |