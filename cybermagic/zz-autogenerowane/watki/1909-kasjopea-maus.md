# Kasjopea Maus
## Identyfikator

Id: 1909-kasjopea-maus

## Sekcja Opowieści

### Śmierć Aleksandrii

* **uid:** 191126-smierc-aleksandrii, _numer względny_: 8
* **daty:** 0110-10-10 - 0110-10-13
* **obecni:** Amelia Mirzant, Kamil Lemurczak, Kasjopea Maus, Ola d'Amelia, Sebastian Kuralsz, Teresa Marszalnik

Streszczenie:

Psychotronika Aleksandrii (której ta nie powinna mieć) w Kruczańcu (gdzie nie powinno jej być) się rozpada. Pojawia się proto-osobowość. Kamil Lemurczak zachowuje się jak dobry mag. W desperacji, Amelia ściąga Zespół i razem negocjują i określają jak dalej żyć. W wyniku Aleksandria jeszcze bardziej się destabilizuje, acz zaczyna ufać Amelii (co nie powinno się stać). W końcu zespół zaczyna współpracować z Kasjopeą i dzięki Karradraelowi dochodzi do destrukcji Aleksandrii i sformowania nowej TAI, na którą poluje spora część szlachty Aurum.

Aktor w Opowieści:

* Dokonanie:
    * szukała brudów na Lemurczaka w Kruczańcu. Znalazła Aleksandrię którą z pomocą Karradraela rozmontowała (tzn. on rozmontował).
* Progresja:
    * uszkodzona przez wpływ Aleksandrii i Karradraela. Potężne osłony w jej umyśle są mniej potężne.


### Rexpapier i Włóknin

* **uid:** 190912-rexpapier-i-wloknin, _numer względny_: 7
* **daty:** 0110-06-21 - 0110-06-25
* **obecni:** Anna Warlank, Dariusz Kuromin, Eleonora Rdeść, Jarek Gułanczak, Kasjopea Maus, Ksenia Kirallen, Mateusz Urszank

Streszczenie:

Anna z Rexpapier próbuje zniszczyć Włóknin za grzechy ojców. To jest praprzyczyna którą wykryli detektywi Kasjopei, idąc po nitce do kłębka. Jednocześnie szef Rexpapier, Jarek, który był bezpośrednim egzekutorem jest człowiekiem dobrym - jego sekretarka weźmie na siebie winę by tylko jemu nic się nie stało. Odpowiedź Kasjopei? Reality show, które Annę i Dariusza zbliży by zniszczyć tą upiorną wendettę.

Aktor w Opowieści:

* Dokonanie:
    * ściągnęła detektywów by uratować Włóknin; poprosiła ją Pięknotka a jej ulubiona sukienka jest stąd. Będzie reżyserować reality show by pogodzić Annę i Dariusza.


### Wypadek w Kramamczu

* **uid:** 190906-wypadek-w-kramamczu, _numer względny_: 6
* **daty:** 0110-06-18 - 0110-06-19
* **obecni:** Dariusz Kuromin, Kasjopea Maus, Ksenia Kirallen, Mariusz Trzewń, Pięknotka Diakon

Streszczenie:

Wrabiają Wiktora Sataraila w atak na niewielką firmę, Włóknin z Kramamczy! Pięknotka i Ksenia wysłane by to naprawić! Pięknotka przyzwała z powrotem wszystkie królowe które pouciekały a Ksenia odkryła sabotaż wewnętrzny. Dariusz próbuje wyciągnąć ubezpieczenie. Pięknotka wezwała Kasjopeę (dziennikarkę) do pomocy i przekonała Ksenię, żeby ta odpuściła Dariuszowi tylko znalazła osoby winne tej sytuacji i prowokacji.

Aktor w Opowieści:

* Dokonanie:
    * wezwana jako rozpaczliwe wsparcie przez Pięknotkę; zaraz elegancko zabrała się do znalezienia taniego ale pewnego zlecenia dla Włóknina - dla weteranów terminuskich


### Osopokalipsa Wiktora

* **uid:** 190419-osopokalipsa-wiktora, _numer względny_: 5
* **daty:** 0110-03-29 - 0110-03-31
* **obecni:** Alan Bartozol, Kasjopea Maus, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Wiktor zdecydował się zemścić w imieniu wił. Zaprojektował osy które robiły krzywdę magom i ludziom - wpierw użył ich jako dywersję, potem zaraził jedzenie w Sensoplex. Pięknotka dekontaminując okolicę odkryła plan; przekonała go, że można bezkrwawo usunąć stąd Sensus. I to zrobiła - z pomocą Kasjopei by ta zrobiła reportaż o "Osopokalipsie", po czym sfabrykowała dowody z pomocą Wiktora. W ten sposób Sensus opuścił teren i nikt szczególnie nie ucierpiał.

Aktor w Opowieści:

* Dokonanie:
    * dziennikarka, chciała sfilmować apokalipsę Sensoplex; osłoniła Pięknotkę biorąc na siebie źródło pierwotne i wyszła na mega bohaterkę w Sensopleksie.


### Minerwa i Kwiaty Nadziei

* **uid:** 190210-minerwa-i-kwiaty-nadziei, _numer względny_: 4
* **daty:** 0110-02-21 - 0110-02-23
* **obecni:** Atena Sowińska, Erwin Galilien, Kasjopea Maus, Kornel Garn, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Kornel Garn próbował przekonać Minerwę do dołączenia do niego, by mu pomogła - budując jej społeczeństwo które ją akceptuje i pokazując jej Kwiaty Nadziei z Ixionu. Minerwa w 100% wpadała w to, więc Pięknotka zmontowała front przeciw Kornelowi - Kasjopea, Erwin, terminusi Pustogorscy. Skończyło się ucieczką Kornela, ale Minerwa będzie chciała mu pomóc i dzielić się z nim wiedzą. A Pięknotkę bardzo martwi to, jak Kornel radzi sobie z ixiońskimi anomaliami i swoim kultem.

Aktor w Opowieści:

* Dokonanie:
    * dziennikarka, która zrobiła na prośbę Pięknotki niewiarygodne świństwo Kornelowi - sprawiła, że jest persona non grata. Będzie mogła negocjować z Minerwą odnośnie Kryształu Pamięci.


### Nie da się odrzucić mocy

* **uid:** 190206-nie-da-sie-odrzucic-mocy, _numer względny_: 3
* **daty:** 0110-02-17 - 0110-02-20
* **obecni:** Arnulf Poważny, Karolina Erenit, Kasjopea Maus, Kornel Garn, Marlena Maja Leszczyńska, Minerwa Metalia, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Karolina źle reaguje na to, że stała się czarodziejką i chciała odrzucić moc. Jednak nie udało jej się, jedynie zdewastowała Cyberszkołę i zagroziła ludziom. Karolina sprowadziła Ixion do Cyberszkoły. Zespół ją powstrzymał i Pięknotka zaczęła reintegrować życie Karoliny, kierując ją do Marleny Mai i do AMZ.

Aktor w Opowieści:

* Dokonanie:
    * wyczuła coś piekielnie interesującego w obszarze Cyberszkoły Zaczęstwa, ale Minerwa przekierowała jej uwagę na siebie.


### Nowa Minerwa w nowym świecie

* **uid:** 190120-nowa-minerwa-w-nowym-swiecie, _numer względny_: 2
* **daty:** 0110-01-22 - 0110-01-26
* **obecni:** Erwin Galilien, Karla Mrozik, Kasjopea Maus, Minerwa Metalia, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Minerwa wróciła. Jako czarodziejka, opiekować się ma nią Pięknotka. Minerwa nie do końca radzi sobie w nowej rzeczywistości - nie jest piękną Diakonką, ma odłamki pamięci Saitaera i aby powrócić, poświęciła życie człowieka. Zdecydowała się tymczasowo przenieść do Zaczęstwa (Karla wysłała tam wsparcie). Tymczasem Kornel Garn już planuje by ją przekonać by doń dołączyła.

Aktor w Opowieści:

* Dokonanie:
    * psycholka która uważa, że Minerwa jest cudowna - żyje na krawędzi i ma chory umysł. Chce zrobić holokostki z wizji Minerwy. Pięknotka staje jej na drodze.


### Wypalenie Saitaera z Trzęsawiska

* **uid:** 190116-wypalenie-saitaera-z-trzesawiska, _numer względny_: 1
* **daty:** 0110-01-07 - 0110-01-09
* **obecni:** Alan Bartozol, ASD Centurion, Hieronim Maus, Karradrael, Kasjopea Maus, Pięknotka Diakon, Saitaer, Wiktor Satarail

Streszczenie:

Pięknotka dostała zadanie eskortowania kapłana Karradraela do ołtarza Saitaera. ASD Centurion oraz Alan odwracali uwagę Trzęsawiska. Plan się udał, choć w wyniku Pięknotka została znowu przekształcona przez Saitaera. Za to, nie ma już wpływu Saitaera w okolicy - więcej, udało się zdjąć też wpływ z Wiktora.

Aktor w Opowieści:

* Dokonanie:
    * poszła jako dziennikarka uczuć na Trzęsawisko. Motywowała Hieronima. Niespecjalnie przeszkadzała Pięknotce, acz skończyła jako pocisk balistyczny niosący Karradraela.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 8, @: 0110-10-13
    1. Primus    : 8, @: 0110-10-13
        1. Sektor Astoriański    : 8, @: 0110-10-13
            1. Astoria    : 8, @: 0110-10-13
                1. Sojusz Letejski    : 8, @: 0110-10-13
                    1. Aurum    : 1, @: 0110-10-13
                        1. Powiat Lemurski    : 1, @: 0110-10-13
                            1. Kruczaniec    : 1, @: 0110-10-13
                                1. Zamek Pisarza    : 1, @: 0110-10-13
                    1. Szczeliniec    : 7, @: 0110-06-25
                        1. Powiat Pustogorski    : 6, @: 0110-06-25
                            1. Kramamcz    : 2, @: 0110-06-25
                                1. Włóknin    : 1, @: 0110-06-19
                            1. Podwiert    : 2, @: 0110-06-25
                                1. Sensoplex    : 1, @: 0110-03-31
                            1. Pustogor    : 1, @: 0110-01-26
                                1. Barbakan    : 1, @: 0110-01-26
                                1. Gabinet Pięknotki    : 1, @: 0110-01-26
                                1. Kawiarenka Ciemna Strona    : 1, @: 0110-01-26
                            1. Zaczęstwo    : 2, @: 0110-02-23
                                1. Akademia Magii, kampus    : 1, @: 0110-02-20
                                    1. Domek dyrektora    : 1, @: 0110-02-20
                                1. Cyberszkoła    : 1, @: 0110-02-20
                                1. Hotel Tellur    : 1, @: 0110-02-23
                                1. Kwatera Terminusa    : 1, @: 0110-02-20
                                1. Nieużytki Staszka    : 1, @: 0110-02-23
                                1. Wschodnie Pole Namiotowe    : 1, @: 0110-02-23
                        1. Trzęsawisko Zjawosztup    : 2, @: 0110-03-31
                            1. Głodna Ziemia    : 1, @: 0110-01-09

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 6 | ((190116-wypalenie-saitaera-z-trzesawiska; 190120-nowa-minerwa-w-nowym-swiecie; 190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei; 190419-osopokalipsa-wiktora; 190906-wypadek-w-kramamczu)) |
| Minerwa Metalia      | 3 | ((190120-nowa-minerwa-w-nowym-swiecie; 190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei)) |
| Alan Bartozol        | 2 | ((190116-wypalenie-saitaera-z-trzesawiska; 190419-osopokalipsa-wiktora)) |
| Dariusz Kuromin      | 2 | ((190906-wypadek-w-kramamczu; 190912-rexpapier-i-wloknin)) |
| Erwin Galilien       | 2 | ((190120-nowa-minerwa-w-nowym-swiecie; 190210-minerwa-i-kwiaty-nadziei)) |
| Kornel Garn          | 2 | ((190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei)) |
| Ksenia Kirallen      | 2 | ((190906-wypadek-w-kramamczu; 190912-rexpapier-i-wloknin)) |
| Tymon Grubosz        | 2 | ((190120-nowa-minerwa-w-nowym-swiecie; 190206-nie-da-sie-odrzucic-mocy)) |
| Wiktor Satarail      | 2 | ((190116-wypalenie-saitaera-z-trzesawiska; 190419-osopokalipsa-wiktora)) |
| Amelia Mirzant       | 1 | ((191126-smierc-aleksandrii)) |
| Anna Warlank         | 1 | ((190912-rexpapier-i-wloknin)) |
| Arnulf Poważny       | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| ASD Centurion        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Atena Sowińska       | 1 | ((190210-minerwa-i-kwiaty-nadziei)) |
| Eleonora Rdeść       | 1 | ((190912-rexpapier-i-wloknin)) |
| Hieronim Maus        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Jarek Gułanczak      | 1 | ((190912-rexpapier-i-wloknin)) |
| Kamil Lemurczak      | 1 | ((191126-smierc-aleksandrii)) |
| Karla Mrozik         | 1 | ((190120-nowa-minerwa-w-nowym-swiecie)) |
| Karolina Erenit      | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Karradrael           | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Mariusz Trzewń       | 1 | ((190906-wypadek-w-kramamczu)) |
| Marlena Maja Leszczyńska | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Mateusz Urszank      | 1 | ((190912-rexpapier-i-wloknin)) |
| Ola d'Amelia         | 1 | ((191126-smierc-aleksandrii)) |
| Saitaer              | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Sebastian Kuralsz    | 1 | ((191126-smierc-aleksandrii)) |
| Teresa Marszalnik    | 1 | ((191126-smierc-aleksandrii)) |