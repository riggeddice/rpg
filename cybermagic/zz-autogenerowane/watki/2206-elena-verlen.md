# Elena Verlen
## Identyfikator

Id: 2206-elena-verlen

## Sekcja Opowieści

### Ratujemy porywaczy Eleny

* **uid:** 220610-ratujemy-porywaczy-eleny, _numer względny_: 68
* **daty:** 0112-09-15 - 0112-09-17
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Hestia d'Atropos, Jakub Bulgocz, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Raoul Lavanis

Streszczenie:

Elena jest już zbyt zdestabilizowana by funkcjonować niezależnie, więc wpierw zajęli się nią w Laboratorium Kranix a po zbudowaniu dla niej Containment Chamber przesłali ją do stacji medycznej Atropos do regeneracji. Stamtąd ją i Martyna porwał Syndykat Aureliona (ale Martyn przewidział JAKIŚ problem i ostrzegł Klaudię). Elena by zniszczyła okręt napastników, ale Zespół skutecznie odkrył problemy na Atropos, przesłuchali dowódcę, zneutralizowali miragenta i dotarli do okrętu Aureliona zdobywając jeńców i zatrzymując Elenę przed przebudzeniem i Spustoszeniem statku.

Aktor w Opowieści:

* Dokonanie:
    * gdy ratowała agenta Inferni, Spustoszenie zniszczyło jej pamięć. Pokonała (nie zabijając) sporo agentów, zanim Arianna do niej dotarła. W Kranix ją zregenerowali i zrobili jej containment chamber. Śpi. Porwana przez Syndykat Aureliona, Inferni udało się ją odbić zanim się przebudziła.
* Progresja:
    * jej pierwsze wystąpienie jako Spustoszenie Strain Elena.


### Potwór czy choroba na EtAur?

* **uid:** 220316-potwor-czy-choroba-na-etaur, _numer względny_: 67
* **daty:** 0112-06-29 - 0112-07-01
* **obecni:** Arianna Verlen, Dominika Perikas, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Melania Akacja, Suwan Chankar, Tymoteusz Czerw

Streszczenie:

Na stacji EtAur pojawiły się poważne problemy - potwór? Choroba? Suwan próbuje ustabilizować co się da, ale nic nie może zrobić. Arianna poproszona o pomoc przybyła, ale nikt nie chce z nią praktycznie współpracować. Klaudia doszła do przyczyny problemów - choroba, ale potwór jest wektorem. Arianna doszła do transmisji po kanałach magicznych. Świetnie.

Aktor w Opowieści:

* Dokonanie:
    * pyta Eustachego, kiedy ten zamierza jej się oświadczyć. Jej szaleństwo rośnie. Wyłączona, wsadzona do biovatu na regenerację.
* Progresja:
    * contained. Znajduje się w specjalnym biovacie gdzie jest regenerowana i reanimowana do formy 'kontrolowanego viciniusa'.


### Upadek Eleny

* **uid:** 220309-upadek-eleny, _numer względny_: 66
* **daty:** 0112-06-08 - 0112-06-14
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Tomasz Kaltaben

Streszczenie:

Keldan Voss została ustabilizowana. Annika będzie mieć szansę powodzenia - zostaje przełożoną, z łącznikiem z keldanitami Mateusem. Odbudowany jej link z pallidanami. Pallidanie mają ogólnie "spokój". Tomasz Kaltaben będzie ją wspierał a on sam dostanie 3 magów Orbitera co go wspierają (i są agentami). Elena niestety zabrała trzy życia odzyskując naturalną urodę. Klaudia ciężko pracuje by zbudować dla Eleny Detox Chamber - ona nie jest OK.

Aktor w Opowieści:

* Dokonanie:
    * dla Eustachego, dla piękna, dla przyszłości pożarła kultystów. Ale porwała Kroczącego we Mgle, wbijając mu neurokontrolę ixiońską palcem w układ nerwowy (creepy as hell).
* Progresja:
    * słowami Mateusa - "w imieniu Saitaera POŚWIĘCASZ. Ona nie POŚWIĘCA. Ona ZABIERA. Zabiera przeznaczenie innych by wzmacniać swoje". Odzyskała swoje piękno zabierając 3 życia.


### Stabilizacja Keldan Voss

* **uid:** 220223-stabilizacja-keldan-voss, _numer względny_: 65
* **daty:** 0112-06-04 - 0112-06-07
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, SP Światło Nadziei, Szczepan Kaltaben, Tomasz Kaltaben

Streszczenie:

Ewakuacja pallidan na Keldan Voss się udało, acz Eustachy musiał zniszczyć anomalnych napastników z Mgły i naprawić morale Anniki. Elenie (słyszącej szepty) udało się zniszczyć BIA i wprowadzić na jej miejsce amalgamat szamana. Arianna ma polityczną kontrolę nad sytuacją a dzięki Eustachemu udało się sytuację ustabilizować militarnie. Dzięki Klaudii wiedzą co i jak. Teraz już tylko zostaje zostawić Keldan Voss w stabilnej formie.

Aktor w Opowieści:

* Dokonanie:
    * wykryła atak Mgły na Eustachego; Klaudia przez nią (omni) przepuściła sygnał dla wyczyszczenia. Usłyszała WSZYSTKO. Szepty Saitaera. Zinfiltrowała BIA, nie wyszło jej wprowadzenie BIA, ale zintegrowała szamana z BIA. Coraz bardziej ma cechy terrorforma a nie maga.
* Progresja:
    * obsesja na punkcie swojej urody; słyszy szepty Keldan Voss. Szepty Saitaera? Szepty Mgieł? Nawet ona nie wie.


### Polityka rujnuje Pallidę Voss

* **uid:** 220216-polityka-rujnuje-pallide-voss, _numer względny_: 64
* **daty:** 0112-06-01 - 0112-06-03
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Mateus Sarpon, Raoul Lavanis, SP Pallida Voss, Szczepan Kaltaben

Streszczenie:

Arianna we współpracy z Mateusem planują uratować zakładników pallidańskich od radykałów keldańskich. Ale by to osiągnąć Eustachy i Klaudia infiltrują wpierw Pallidę Voss, bo okazuje się, że Annikę wrabiają jej własni podwładni. Klaudia zdobyła wszystkie potrzebne dowody, Eustachy chciał ratować ludzi... i Paradoks (po sabotowaniu generatorów Memoriam) spowodował straszne straty w ludziach i przesunął Pallidę Voss w Anomalię Kosmiczną. Oops. Ale dzięki drakolitom (keldanitom) udało się sporo ludzi uratować.

Aktor w Opowieści:

* Dokonanie:
    * destabilizuje się; chce NISZCZYĆ dla dywersji (Klaudia ją zatrzymała). Głównie działała jako prom transportujący na lewo i prawo, acz energia ją trochę nosi.


### Sekrety Keldan Voss

* **uid:** 220202-sekrety-keldan-voss, _numer względny_: 63
* **daty:** 0112-05-29 - 0112-05-31
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Mateus Sarpon

Streszczenie:

Zespół skutecznie ewakuował pallidan z Keldan Voss, po drodze orientując się że część porwanych przez Mgły osób została Skażona i zmieniona w Kroczących w Mgle. Eustachy wydobył jednego do badań dla Marii. Co ciekawe - pallidanie i keldanici widzą zupełnie inne wersje historii ("snajper" vs "porwania") a do tego Mgły powodują jeszcze większą nierzeczywistość i złudzenia. A Arianna politycznie a Klaudia naukowo dochodzą do tego co jest prawdą...

Aktor w Opowieści:

* Dokonanie:
    * Osłania Eustachego. Nie reaguje na phase shift. Zestrzeliła serię dron górniczych skuteczniej niż kiedykolwiek. Widzi niestety też rzeczy których nie ma... albo jest tam więcej niż widać.


### Keldan Voss, kolonia Saitaera

* **uid:** 220126-keldan-voss-kolonia-saitaera, _numer względny_: 62
* **daty:** 0112-05-24 - 0112-05-27
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Kormonow Voss, Mateus Sarpon, OO Kastor, SP Pallida Voss, Szczepan Kaltaben, Zygfryd Maus

Streszczenie:

Kramer wysłał załogę Inferni na Keldan Voss (stację pod kontrolą pallidan i źródło kryształów vitium), bo tam mogą więcej wiedzieć o anomalizacji ludzi i magów. Co więcej, jest poważny problem polityczny i techniczny - pallidanie zwalczają lokalnych drakolitów i kolonia ma problem typu wojna domowa. Na miejscu okazało się, że są CO NAJMNIEJ dwa stronnictwa a Klaudia uznała, że lokalna Hestia nie jest Hestią. Więc czym jest?

Aktor w Opowieści:

* Dokonanie:
    * przeszła przez przyspieszone testy psychologiczne, ale nie jest z nią w porządku.


### To nie pułapka na Nereidę...

* **uid:** 220105-to-nie-pulapka-na-nereide, _numer względny_: 61
* **daty:** 0112-05-04 - 0112-05-09
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Lutus Amerin, Maria Naavas, Natalia Aradin, Saitaer

Streszczenie:

Plan - dorwanie tajemniczych koloidowych korwet. Wiemy, że to pułapka, więc zróbmy kontr-pułapkę. Ale okazało się, że to nie pułapka na Nereidę a na Infernię. Standardowe badania wprowadziły w Infernię (Dianę) memetic payload i Diana prawie wpadła pod kontrolę Saitaera - odrzucenie kokonu z Natalią sprawiło, że udało się ciężko uszkodzoną Infernię uratować. Zwalczając ixion Arianna sięgnęła do Esuriit przez Elenę; Infernia by zginęła gdyby nie samobójcza akcja Izabeli i Kamila. Elena wchłonęła tyle energii ile była w stanie, przekraczając swój Wzór. ALE - Infernia przetrwała. Przy okazji, Natalia się wykluła. Ani osoba ani statek kosmiczny - mordercza anomalia kosmiczna.

Aktor w Opowieści:

* Dokonanie:
    * w kryzysowej i krytycznej sytuacji przekierowywała energie w lewą i prawą pomagając Izie, Klaudii i Kamilowi. Jej wzór uległ uszkodzeniu w sposób nieosiągalny. Poświęciła się dla Inferni i przyjaciół. ŻYJE ALE.
* Progresja:
    * zniszczony Wzór, pomiędzy ixionem i esuriit. Dwa potężne ryczące oceany a pomiędzy nimi jedna mała osobowość Eleny.
    * perfekcyjny pilot Nereidy. Perfekcyjna anomalna integracja z pojazdami. Jej eidolon i ona stanowią jedność.


### Kult Saitaera w Neotik

* **uid:** 211222-kult-saitaera-w-neotik, _numer względny_: 60
* **daty:** 0112-04-30 - 0112-05-01
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kasandra Destrukcja Diakon, Klaudia Stryk, Lutus Amerin, Maciek Kalmarzec, Maria Naavas

Streszczenie:

Klaudia wróciła z bronią anty-Serenitową od Tosena, Arianna z feromonami Marii nadała Inferni nową kulturę - miłośnicy lolitek x kultyści x noktianie. Elena uziemiona w Inferni zinfiltrowała Stocznię ukryta przez Klaudię i odkryła niższe Spustoszenie + kult Saitaera. Nereida jest czysta od Saitaera; przetransferowana na Infernię dla Eleny i uzbrojona. Klaudia ma listę jednostek potencjalnie w rękach Saitaera / Spustoszonych. Przygotowania do kontrataku skończone.

Aktor w Opowieści:

* Dokonanie:
    * uziemiona przez Klaudię i Ariannę w Inferni zinfiltrowała Stocznię Neotik i odkryła Kult Saitaera oraz niższe Spustoszenie.


### Sklejanie Inferni do kupy

* **uid:** 211215-sklejanie-inferni-do-kupy, _numer względny_: 59
* **daty:** 0112-04-27 - 0112-04-29
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Kamil Lyraczek, Kasandra Destrukcja Diakon, Leona Astrienko, Maria Naavas, OO Infernia, Roland Sowiński

Streszczenie:

Ixioński wariant Inferni okazuje się całkiem słodki i pod kontrolą Eustachego. Morale Inferni jest w ruinie. Z pomocą Marii Arianna przesuwa kulturę Inferni w stronę "kultyści + noktianie + miłośnicy psychopatycznej lolitki Diany". Eustachy wpada w pułapkę Leony, która chce go wyleczyć czerwiem Esuriit - ale Eustachy przekonuje ją, że ma Dianę pod kontrolą. Jednak gdzieś w tle znajduje się prawdziwy ixioński potwór na stoczni Neotik.

Aktor w Opowieści:

* Dokonanie:
    * chciała opuścić Infernię po tym wszystkim; Arianna przekonała ją, że jak będzie dowodzić Tivrem to pomoże Orbiterowi. Poważnie Skażona Ixionem i Esuriit; nie ma statusu już czarodziejki. Nie ma dla niej powrotu do domu.
* Progresja:
    * NOWA KARTA POSTACI. Skażenie Ixionem i Esuriit. Zmiana wyglądu. Nie ma dla niej powrotu do Verlenlandu. Nie jest już czarodziejką.


### O krok za daleko

* **uid:** 211208-o-krok-za-daleko, _numer względny_: 58
* **daty:** 0112-04-25 - 0112-04-26
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Hestia d'Neotik, Jarosław Szarjan, Klaudia Stryk, Leona Astrienko, OO Infernia, Wawrzyn Rewemis

Streszczenie:

Klaudia doszła do tego, że Hestia d'Neotik oszukuje Stocznię; w okolicy są niewielkie statki koloidowe o których Stocznia nie wie. Arianna zintegrowała Eustachego z Infernią ixiońsko, niestety anomalizując Infernię. Elena ciężko Skażona ixionem i (w swoim mniemaniu) porzucona przez Eustachego. Eustachy ujarzmiając Infernię rozbijał ją o różne asteroidy, co powoduje śmierć części załogi. Infernia ma zerowe morale, ale jest opanowana. Przyjazna Anomalia Kosmiczna z tymczasową bazą w Stoczni Neotik.

Aktor w Opowieści:

* Dokonanie:
    * gdy Arianny moc wychodzi poza kontrolę, gdy wpływa na Eustachego - próbowała powstrzymać Paradoks Arianny. Potężne Skażenie ixiońskie. Nie jest już czarodziejką. W rozpaczy próbowała zatrzymać Eustachego przed pójściem z "Dianą" - ale Eustachy wybrał Infernię nad nią. Ma złamane serce, Infernia jej nie lubi i ogólnie nie wie co robić.
* Progresja:
    * próbowała pochłonąć Eksplozję Paradoksu Arianny; napromieniowana ixionem. Staje się viciniusem. NOWA KARTA POSTACI, pełna rekonstrukcja.
    * rana mentalna od Arianny (she didn't give a meow) i od Eustachego (odrzucił i wzgardził; wybrał Infernię, ranił ludzi). A ona dla nich wszystko.
    * Infernia ją zwalcza, nie chce jej na pokładzie. Ixion i Esuriit...


### Prototypowa Nereida Natalii

* **uid:** 211124-prototypowa-nereida-natalii, _numer względny_: 57
* **daty:** 0112-04-20 - 0112-04-24
* **obecni:** Adam Szarjan, Arianna Verlen, Arkadia Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Maria Naavas, Natalia Aradin, OO Infernia, Roland Sowiński, Wawrzyn Rewemis

Streszczenie:

Kampania reklamowa "dołącz do Inferni" zrobiona przez Izę przy użyciu Eustachego jako modela - sukces. Klaudia zdobyła z tego nowych załogantów (acz to jeszcze potrwa). Szarjan uwolnił Klaudię z zarzutów, potrzebuje pomocy w stoczni - jego przyjaciółka Natalia ma problem z prototypem myśliwca klasy Nereida i została ixiońsko zintegrowana. Infernia próbowała Nereidę uratować, ale niestety skończyło się to poważnym uszkodzeniem Nereidy i stworzeniem ixiońskiego kokonu, z którego Natalia musi się wykluć...

Aktor w Opowieści:

* Dokonanie:
    * nie umie dojść do siebie przez sytuację z Eustachym, ale jak trzeba było ratować Natalię z Nereidy, nie wahała się ani przez sekundę. Sama w kosmosie w Eidolonie czekała aż Eustachy ściągnię na nią Nereidę. Jednak nie ściągnął, więc siedziała sama w kosmosie i kontemplowała życie.


### Porwany Trismegistos

* **uid:** 211117-porwany-trismegistos, _numer względny_: 56
* **daty:** 0112-04-15 - 0112-04-17
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Jamon Korab, Kalira d'Trismegistos, Klara Gwozdnik, Klaudia Stryk, Leona Astrienko, Maria Naavas, Mira Anastel, OO Tivr, Roland Sowiński, SC Trismegistos, Zygfryd Maus

Streszczenie:

Wiadomość od pirata że porwał Trismegistos poderwała Ariannę i Tivr. Historia nie trzymała się kupy. Załoga Inferni (Tivru) zorientowała się, że coś jest nie tak - opowieść o statku kupieckim który pozbył się miragenta brzmiała DZIWNIE. Okazało się, że Trismegistos jest statkiem kaperskim. Gdy Trismegistos zażądał poddania się Tivru Klaudia zrobiła sygnał "lol leci na Was Serenit" i TAI Trismegistosa spanikowała. Wolna TAI, współpracująca z fareilem i drakolitką. Wolny dystrykt. Arianna dała im żyć - ale odzyskała porwanych arystokratów i komory adaptacji.

Aktor w Opowieści:

* Dokonanie:
    * Arianna jej zaufała, by ta pilotowała Tivr. I faktycznie, najszybszy pilot. Plus, spacer kosmiczny w Eidolonie pozwolił jej podpiąć Klaudię do Trismegistosa.
* Progresja:
    * zaimponowała noktianom na pokładzie Tivra, że świetnie pilotuje tą jednostkę. Z ogromnym tempem i wydajnością.


### Romans dzięki Esuriit

* **uid:** 211110-romans-dzieki-esuriit, _numer względny_: 55
* **daty:** 0112-04-13 - 0112-04-14
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Karol Reichard, Klaudia Stryk, Leona Astrienko, Maria Naavas, Wawrzyn Rewemis

Streszczenie:

Infernia nie ma dość załogi, by móc działać, więc Termia przekazuje Ariannie Tivr. Leona nie może sobie wybaczyć, że jej nie było i to się stało, więc się wypisuje z pomocą Eustachego (co powoduje plotki Leona x Eustachy). Elena ucieka do Sektora 49 na K1 by być samotna; Arianna organizuje ostre poszukiwania. Eustachy znajduje Elenę by ją pocieszyć. Elena w końcu przyznaje Eustachemu, że go kocha. Maria Naavas z woli Martyna tymczasowo dołącza do załogi Arianny.

Aktor w Opowieści:

* Dokonanie:
    * złamana, nie może się pogodzić z tym co zrobiła (Esuriit) i co się z nią stało (Esuriit). Ucieka do Sektora 49. Arianna organizuje grę wojenną by ją znaleźć. W końcu Elena przyznaje Eustachemu, że go kocha. Ale nie usłyszała tego z powrotem, więc wypłakuje oczy, że Eustachy wybrał LEONĘ nad nią.
* Progresja:
    * załamana i zrozpaczona. Eustachy wybrał LEONĘ nad nią. Ze wszystkich osób. LEONĘ!
    * jej zdaniem byłoby lepiej, gdyby NIE ISTNIAŁA. Zabiła tyle niewinnych osób. Esuriit ją pożera. Chce odejść w noc... ale nie może i nie chce. Eustachy lub śmierć.


### Rzieza niszczy Infernię

* **uid:** 211027-rzieza-niszczy-infernie, _numer względny_: 54
* **daty:** 0112-03-28 - 0112-04-02
* **obecni:** Arianna Verlen, Elena Verlen, Flawia Blakenbauer, Klaudia Stryk, Martyn Hiwasser, Otto Azgorn, TAI Rzieza d'K1

Streszczenie:

Infernia wraca z Mevilig. Rzieza chce wyczyścić im pamięć o sobie. Arianna się opiera. Rzieza dowiaduje się o Ataienne. Arianna budzi Elenę po sympatii, Elena sprowadza Esuriit do laboratorium dekontaminacyjnego K1. Dużo śmierci. Martyn próbuje to opanować - budzi swoje simulacrum. Arianna i Klaudia opanowują przy pomocy Rziezy sytuację. 37% załogi Inferni nie żyje, Flawia wyssana w kosmos, Elena złamana, Martyn w szpitalu.

Aktor w Opowieści:

* Dokonanie:
    * obudzona rozpaczliwie przez Ariannę. Zdecentrowana. Próbowała wszystkich uratować i wlała Esuriit do laboratorium (eksplozja Paradoksu). Z trudem powstrzymała manifestację Zbawiciela-Niszczyciela. Potężnie Skażona i przekształcona przez Esuriit; resztę operacji dobierała się do śpiącego Eustachego. NIE MOŻE TEGO SOBIE WYBACZYĆ.
* Progresja:
    * straszna rekonstrukcja Esuriit. Libido++. Obsesja w stronę Eustachego. Elena nie czuje, że kontroluje swoje ciało i umysł. Przebudowa Paradoksu na Eustachego.


### Kurczakownia

* **uid:** 211020-kurczakownia, _numer względny_: 53
* **daty:** 0112-03-24 - 0112-03-26
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, OO Infernia, Otto Azgorn, Vigilus Mevilig

Streszczenie:

By uratować jak najwięcej osób w Sektorze Mevilig, Eustachy zaprojektował Kurczakownię - maszynę do oddzielania mechanicznego głów i rdzenia od reszty człowieka. Arianna starła się ze Zbawicielem-Niszczycielem jako aspekt Zbawiciela, wspierana przez Działo Rozpaczy (Eustachy). Udało im się sporo uratować (40% populacji Planetoidy Kalarfam), acz załoga Inferni będzie mieć koszmary senne. Klaudia zebrała dość danych, by zapobiec naturalnej manifestacji Zbawiciela-Niszczyciela w Sektorze Astoriańskim i zrobić kontrakcję jak co.

Aktor w Opowieści:

* Dokonanie:
    * akcja infiltracyjna Kultu Esuriit z Flawią. Udało jej się znaleźć gdzie są przetrzymywani Orbiterowcy, acz wpadła w kłopoty. W zamieszaniu wywołanym pojawieniem się Arianny udało jej i Flawii się uciec.
* Progresja:
    * TRAUMA. Kurczakowanie - rekurczakowanie. Dla niej to jest... straszne.


### Szara nawałnica

* **uid:** 211013-szara-nawalnica, _numer względny_: 52
* **daty:** 0112-03-18 - 0112-03-23
* **obecni:** Aleksandra Termia, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, Otto Azgorn, Remigiusz Błyszczyk, TAI Rzieza d'K1

Streszczenie:

Zespół przygotował się na wejście do Mevilig. Rzieza odkrył jak działają Piranie i że tamten sektor jest opanowany przez TAI 5+ generacji. Niestety, Infernia musiała uciekać przed Rziezą. Na miejscu Eustachy znalazł skuteczny sposób przemykania między Piraniami, dotarli do planetoidy Kalarfam gdzie znaleźli bazę ludzi. Mechaniczny "smok" został zniszczony przez Infernię, pobrali lokalsów i Arianna przekonała ich do zmiany kultu. Aha, anomalny wybuch torpedy anihilacyjnej Arianna przekierowała na planetę, wypowiadając ostrą wojnę TAI 5+ generacji...

Aktor w Opowieści:

* Dokonanie:
    * wraz z komandosami Verlenów przechwytywała lokalsów ze śmieciostateczku w Kalarfam na Infernię. Niewielka rola.


### Grupa Ekspedycyjna Kellert

* **uid:** 210929-grupa-ekspedycyjna-kellert, _numer względny_: 51
* **daty:** 0112-03-13 - 0112-03-16
* **obecni:** Adam Nerawol, Aleksandra Termia, Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Olena Orion, OO Infernia, OO Omega Septius, Otto Azgorn, TAI Marszałek Grzmotoszpon Trzeci, Vigilus Mevilig

Streszczenie:

Termia wysłała siły do sektora "Noviter", ale to był inny sektor (Mevilig). Straciła bezzałogowce, potem grupę ekspedycyjną Kellert. Kramer wysłał Infernię zakoloidowaną; okazało się, że ten sektor ma "strażnika Esuriit" zrobionego przez ludzi i grupę Piranii, sterowanych przez TAI 4 poziomu. Inferni udało się zebrać dane i wrócić z siedmioma uratowanymi członkami OO Savera, gdzie większość załogi została corruptowana przez Vigilusa...

Aktor w Opowieści:

* Dokonanie:
    * zinfiltrowała Skażoną OO Savera. Dała się Zobaczyć Vigilusowi, ale skutecznie odkryła położenie wszystkiego na Saverze i zdołała uciec zanim została rozsiekana przez krwawe "bóstwo".


### Ostatnia akcja bohaterki

* **uid:** 210922-ostatnia-akcja-bohaterki, _numer względny_: 50
* **daty:** 0112-02-23 - 0112-03-09
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Izabela Zarantel, Jolanta Kopiec, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Olgierd Drongon, OO Żelazko, Roland Sowiński

Streszczenie:

Z Martynem skontaktowała się tien Kopiec prosząc o ostatnią misję zanim Esuriit ją pożre. Martyn zorganizował lot Infernią. Ustabilizował Jolantę, by ta dała radę dolecieć słowami i opowieściami, przy okazji tworząc piękną opowieść o Eterni i eulogię dla Jolanty - bohaterki wojennej - w Sekretach Orbitera (przy okazji wyszła jego przeszłość). Infernia zniszczyła Krwawą Bazę Piratów kosztem dewastacji Żelazka. Jolanta zniszczyła krwawego maga. Sektor jest bezpieczniejszy, Eternia szczęśliwa a Arianna ma profity.

Aktor w Opowieści:

* Dokonanie:
    * MVP manewrów. Wprowadziła Ptakiem ekipę do Krwawej Bazy Piratów, odciągała ogień i ostrzeliwała cele W TEJ BAZIE.


### Stabilizacja Bramy Eterycznej

* **uid:** 210901-stabilizacja-bramy-eterycznej, _numer względny_: 49
* **daty:** 0112-02-09 - 0112-02-12
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Gilbert Bloch, Janus Krzak, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Medea Sowińska, OO Kanagar, OO Netrahina, OO Trasman, Seweryn Atanair

Streszczenie:

Infernia wróciła z innymi jednostkami naprawić Bramę Kariańską. Eustachy ufortyfikował i uniewidocznił pintki; Arianna pozyskała dane z reanimowanego magią krążownika Orbitera. Przez Bramę przeszła ON Catarina; Klaudia ją zlokalizowała i wystawiła Netrahinie i Niobe. Niestety, Arianna hiperlinkowała się z ON Spatium Gelida. Dla nich minęło tylko kilka godzin, dla nas - kilkadziesiąt lat...

Aktor w Opowieści:

* Dokonanie:
    * wykazała się mistrzowskim pilotażem ściągając echo krążownika Perforator, po czym pobierając Ariannę spod Bramy i wprowadzając ją na ów Perforator. Ale potem Skażona Esuriit krzyczała, że Eustachemu trzeba założyć pas cnoty i padła do Leony, acz ją poturbowała. CO ZA WSTYD (ten pas...)


### Uszkodzona Brama Eteryczna

* **uid:** 210825-uszkodzona-brama-eteryczna, _numer względny_: 48
* **daty:** 0112-02-04 - 0112-02-07
* **obecni:** AK Nocna Krypta, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Gilbert Bloch, Klaudia Stryk, Medea Sowińska, ON Spatium Gelida, OO Infernia, OO Mfumo

Streszczenie:

Infernia została przechwycona przez Medeę lecąc na Anomalię Kolapsu i przekierowana na uszkodzoną Bramę Eteryczną (gdzie, jak się okazuje, są pasożyty i flota noktiańska która nie doleciała). Na miejscu Infernia uniknęła efemeryd i channelując Kryptę uratowała wszystkich efemerydami. Gdy Krypta pojawiła się na serio, Infernia zwiała paląc silniki. Statki noktiańskie w Bramie mają tether na Infernię.

Aktor w Opowieści:

* Dokonanie:
    * widząc "Dianę" "napastującą" Eustachego, zaatakowała ją by chronić Eustachego. OCZYWIŚCIE tylko dlatego i wcale się nie przejmowała czy coś Eustachemu nie jest lub nie robi.


### Nieudana infiltracja Inferni

* **uid:** 210616-nieudana-infiltracja-inferni, _numer względny_: 47
* **daty:** 0112-01-27 - 0112-02-01
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Marian Tosen, OO Opresor, Roland Sowiński

Streszczenie:

Marian Tosen poprosił Infernię o to, by sprawdzili co się dzieje w Anomalii Kolapsu - podejrzewa anomalie memetyczne, skoro Orbiter traci tam statki  (m.in. OO Opresor) i pojawiają się jednostki nieoznaczonej frakcji. Tymczasem Roland Sowiński chce na pokład. Arianna wybiła mu to z głowy, ale wykryli agentkę wśród jego guwernantek - Flawię Blakenbauer, która miała się nim opiekować i jest trochę zniewolona przez Sowińskich. By zadośćuczynić po tym co Elena zrobiła i trochę Flawii pomóc, Flawia dołączyła do Inferni. "Oficjalnie" udało jej się Infernię zinfiltrować...

Aktor w Opowieści:

* Dokonanie:
    * dawno, dawno temu zrobiła KATASTROFALNĄ krzywdę Flawii, wpakowała ją w długi i doprowadziła do jej dewastacji.
* Progresja:
    * uważa, że Eustachy flirtuje z KAŻDYM. A zwłaszcza z Flawią >.>.
    * straszne poczucie winy za to co zrobiła kiedyś Flawii. Ale nie jest w stanie nic z tym zrobić.


### Listy od fanów

* **uid:** 210630-listy-od-fanow, _numer względny_: 46
* **daty:** 0112-01-15 - 0112-01-18
* **obecni:** Arianna Verlen, Bogdan Anatael, Elena Verlen, Izabela Zarantel, Klaudia Stryk, Michał Teriakin, OE Lord Savaron, Olgierd Drongon, Rafael Galwarn, Remigiusz Falorin, TAI Rzieza d'K1, TAI XT-723 d'K1, TAI Zefiris

Streszczenie:

Na K1 znajduje się tajna baza Eterni, we współpracy z niektórymi elementami K1. Jeden z przekształcanych tam w Pilota chłopców wysłał fanmail do Arianny ("chce się spotkać zanim zginie na froncie"). Arianna z Klaudią znalazły obecność bazy Eterni, pozyskały Eidolona z Eterni (za PR), użyły Eidolona do infiltracji tej bazy, użyły Rziezy do zniszczenia tej bazy a wina spadła na Olgierda (który dla Arianny robił niedaleko manewry). Młodzi Piloci zostali odratowani; nie wiedzą o jaki front chodzi Eterni, ale jedno jest pewne - Eternia szykuje się do wojny kosmicznej. Ale z kim?

Aktor w Opowieści:

* Dokonanie:
    * dokonała głębokiej infiltracji nowym Eidolonem bazy Eterni na K1, rozpracowała jej układ, podpięła tamte TAI do K1 (dla Rziezy) i została ranna uciekając przez Żelazko.
* Progresja:
    * dostała eternijski Eidolon z eternijskim logiem na stałe, w zamian za promocję Eterni. Przynajmniej ten Eidolon jest w dobrym stanie.
    * 3 dni regeneracji i naprawy Eidolona po akcji na K1. Bycie w szpitalu zaczyna być dla niej irytujące.


### Morderstwo na Inferni

* **uid:** 210526-morderstwo-na-inferni, _numer względny_: 45
* **daty:** 0111-12-31 - 0112-01-06
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Feliks Walrond, Kamil Lyraczek, Karol Reichard, Klaudia Stryk, Leona Astrienko, Marian Tosen, Martyn Hiwasser, OO Tivr, Otto Azgorn, Tal Marczak

Streszczenie:

Noktiański mechanik Inferni, Tal Marczak, chciał zniszczyć OO Tivr (własność noktiańskiej rodziny Tala). Noktianie trzymają się razem - inni mechanicy Inferni doprowadzili do morderstwa Tala na K1 i wzięli amnestyki, by nikt nie wiedział co się stało. Zespół Inferni skutecznie doszedł do tego o co chodzi, odpowiedzialnych za morderstwo noktian przeniesiono na Żelazko a Arianna poważnie opieprzyła swoją noktiańską załogę. Będzie lepiej w przyszłości.

Aktor w Opowieści:

* Dokonanie:
    * Eustachy poprosił ją by znalazła Tala Marczaka i przygotowała wnioski formalne. Elena rozmawiając z Klaudią i Arianną próbowała wyprostować to szaleństwo - PO CO TO WSZYSTKO.


### Anomalne awarie Athamarein?

* **uid:** 231018-anomalne-awarie-athamarein, _numer względny_: 44
* **daty:** 0111-12-22 - 0111-12-27
* **obecni:** Alicja Szadawir, Andrzej Gwozdnik, Arianna Verlen, Błażej Sowiński, Elena Verlen, Eustachy Korkoran, Hiacynt Samszar, Jakub Oroginiec, Kajetan Kircznik, Klaudia Stryk, Leona Astrienko, OO Athamarein, Remigiusz Alkarenit, Uśmiechniczka Konstrukcjonistka Aurora Diakon

Streszczenie:

Athamarein - bardzo stara korweta rakietowa, jeszcze sprzed czasu Astorii - miała spotkanie z rajderami Syndykatu. Sama obróciła się, by Syndykat nie zabił jej załogi (Persefona nic nie wie). Jednak załoga Athamarein jest skłócona, straumatyzowana i niekompetentna - nie poradziłaby sobie na akcji, więc Athamarein zaczęła się sama 'sabotować'. W rozpaczy, ściągnęli załogę Inferni zanim Athamarein pójdzie na żyletki - niech Infernianie odkryją i wyjaśnią, co się stało z Athamarein. W wyniku, Athamarein została oddelegowana jako jednostka szkoleniowa. Czas na emeryturę.

Aktor w Opowieści:

* Dokonanie:
    * doskonały pilot, integruje się z Athamarein i powiedziała, że coś jest nie tak - Athamarein "sama" koryguje niektóre rzeczy. Pilotuje Athamarein bezbłędnie gdy zaakceptowała jej specyfikę, choć najpierw ją lekko uszkodziła.


### Osiemnaście Oczu

* **uid:** 210519-osiemnascie-oczu, _numer względny_: 43
* **daty:** 0111-12-07 - 0111-12-18
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Marian Tosen, Martyn Hiwasser, OO Alaya, OO Tivr

Streszczenie:

OO Alaya została zainfekowana anomalią memetyczną, infohazardem. Marian Tosen użył Zespołu by zneutralizować infohazard. Zespół przeszedł przez serię iteracji używając amnestyków by w końcu rozwiązać problem Alayi - amnestyki na całą Alayę. Do końca Zespół nie ma pojęcia czym ta anomalia była (bo wiedza ich by zaraziła). Udało się ewakuować większość Alayi.

Aktor w Opowieści:

* Dokonanie:
    * wyszła z pozycji advancera by ostrzec zespół; jej omnidetekcja jednak sprawiła, że od razu wpadła pod infohazard. Jako infektant chroniła life support, próbując Skazić Eustachego.
* Progresja:
    * wysokie uznanie ze strony Mariana Tosena z grupy antymemetyczej Orbitera.


### Ewakuacja z Serenit

* **uid:** 210512-ewakuacja-z-serenit, _numer względny_: 42
* **daty:** 0111-11-23 - 0111-12-02
* **obecni:** Aida Serenit, AK Serenit, Arianna Verlen, Bogdan Anatael, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OE Falołamacz, OO Infernia, Roland Sowiński

Streszczenie:

Klaudia spowolniła Serenit, po czym Elena weszła zdalnym Entropikiem na pokład Falołamacza. Arianna zmanipulowała ludzi by dało się ich uratować. Samobójcza pintka zniechęciła Falołamacz do dalszej walki. Nie wszystkich udało się uratować (ale kapitana i arystokratę Sowińskich tak). Niestety, Infernia skończyła jako kosmiczny wrak i musiała wysłać SOS...

Aktor w Opowieści:

* Dokonanie:
    * połączona z Eidolonem i sympatią z Entropikiem zdalnie poruszała Entropikiem na Falołamaczu przez sympatię, odpierając ataki Serenita na swój umysł. Uratowała wielu, acz zapłaciła szpitalem.
* Progresja:
    * po neurosprzężeniu i sentisprzężeniu i Dotyku Serenit skończyła na dwa tygodnie w szpitalu na regenerację i rekalibrację Wzoru.
    * Serenit wie o jej istnieniu. Jeśli będzie gdzieś blisko, może na nią zapolować.
    * traci dostęp do sentisprzężonego Eidolona - ów wymaga naprawy ("regeneracji?") w Verlenlandzie. 2 miesiące od teraz.


### Infekcja Serenit

* **uid:** 210428-infekcja-serenit, _numer względny_: 41
* **daty:** 0111-11-22 - 0111-11-23
* **obecni:** AK Serenit, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OE Falołamacz, OE Piękna Elena, OO Infernia, Persefona d'Infernia, Rafał Grambucz, Tadeusz Ursus

Streszczenie:

Ciężko uszkodzona Infernia wraca do domu i dostaje SOS od Ursusa - OE Falołamacz "zniknął", coś z anomaliami. Gdy Zespół dotarł do Falołamacza, próbowali dowiedzieć się co się dzieje - faktycznie mamy anomaliczny okręt. Podczas badań Falołamacza Morrigan przejęła Persefonę Inferni; Eustachy musiał poważnie uszkodzić Infernię by nie zniszczyli Falołamacza. Po kosmicznym spacerze Eleny i Klaudii doszli do tego co się stało - Falołamacz staje się Odłamkiem Serenit. I jeszcze da się ludzi ratować. Ale Infernia jest w bardzo złym stanie...

Aktor w Opowieści:

* Dokonanie:
    * zażenowana "Piękną Eleną" Tadeusza Ursusa; robi kosmiczny spacer na Falołamacz i ratuje Klaudię przed złapaniem przez Serenit (mistrzyni manewrów i uników).


### Znudzona załoga Inferni

* **uid:** 210421-znudzona-zaloga-inferni, _numer względny_: 40
* **daty:** 0111-11-16 - 0111-11-19
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Morrigan d'Tirakal, Otto Azgorn, Persefona d'Infernia, Tomasz Sowiński

Streszczenie:

Po raz pierwszy od dawna Infernia jest w stanie rozprzężenia - wracają z daleka po trudnej misji i nic im nie grozi. Jako, że załoga ma straszne tarcia (Leona poluje na "słabe jednostki", komandosi Verlenów chcą zatrzymać Leonę, noktianie vs astorianie...), Arianna autoryzuje Eustachego do zrobienia "Tirakala" jako niebezpieczną sytuację. Eustachy reanimuje Tirakala, ale niestety cholerstwo zanomalizowało - przywrócił Morrigan. By Zespół nie rozwiązał Tirakala za szybko, Eustachy trochę sabotował Infernię co Elena przeczytała jako... umówienie się na randkę. Gdy Klaudia wykryła, że na INFERNI pojawiła się straszna anomalia (Morrigan) próbowali to usunąć - ale Eustachy przecież sabotował Infernię! Udało im się opanować Morrigan, ale kosztem uszkodzeń i obrażeń na Inferni. I scenek kompromitujących Jolantę Sowińską. Infernia jest uszkodzona, ale się trzyma.

Aktor w Opowieści:

* Dokonanie:
    * zaplanowała konfrontację z Eustachym by powiedział jej czy mu na niej zależy. Doprowadziła do tego. Gdy ona chciała on nie chciał. Gdy on chciał to zorientowała się że to niegodne i uciekła XD.


### Dekralotyzacja Asimear

* **uid:** 210414-dekralotyzacja-asimear, _numer względny_: 39
* **daty:** 0111-11-03 - 0111-11-12
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Llarnagraht, Malictrix d'Pandora, Mariusz Tubalon, Martyn Hiwasser, Martyna Bianistek, SCA Płetwal Błękitny, Tomasz Sowiński

Streszczenie:

Wiedząc, że Jolanta jest pod wpływem kralotha Infernia sfabrykowała kralotyczny alergizator. Arianna zdradziła się Aesimar i powiedziała, że ma zamiar zniszczyć kralotha z ramienia Orbitera. Udało się kralotha zmiażdżyć i zepchnąć do podziemi Asimear, choć ze sporymi stratami ludzkimi; tymczasem Tirakal uciekł na Płetwala. Elena zinfiltrowała Płetwala, ale nie mogła zniszczyć Tirakala nie krzywdząc ludzi. Destrukcję Tirakala Infernia zostawiła więc Malictrix.

Aktor w Opowieści:

* Dokonanie:
    * opieprzyła Eustachego że w ramach działań na Asimear nie optymalizował przeżywalności ludzi. Potem infiltruje Płetwala, przejęła Entropika i wdała się w "taniec" z Tirakalem. Ten grożąc ludziom wymanewrował ją i skończyła w kosmosie - ale podpięła Tirakal do Entropika dając czysty strzał Malictrix.


### Arianna podbija Asimear

* **uid:** 210317-arianna-podbija-asimear, _numer względny_: 38
* **daty:** 0111-10-18 - 0111-11-02
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Martyna Bianistek, SCA Płetwal Błękitny, Tomasz Sowiński

Streszczenie:

"Płetwal Błękitny" ma na pieńku z "Goldarionem"; by nie złamać konspiracji, Klaudia potraktowała Płetwala Bazyliszkiem. Tymczasem pojawiła się wiadomość, że Arianna chce zniszczyć Asimear i żąda wydania Sowińskiej. Prawdziwa Arianna złamała konspirę, skontaktowała się z Jolantą Sowińską, po czym ją porwała. Okazało się, że Jolanta jest kontrolowana przez kralotycznego pasożyta...

Aktor w Opowieści:

* Dokonanie:
    * mistrzowsko sterowała pinasą na Infernię, uciekając od tajemniczych jednostek na Asimear.


### Infernia jako Goldarion

* **uid:** 210218-infernia-jako-goldarion, _numer względny_: 37
* **daty:** 0111-09-16 - 0111-10-01
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, OO Infernia, Tomasz Sowiński

Streszczenie:

Infernia udaje Goldariona, by przetransportować tajną przesyłkę Sowińskich do Jolanty Sowińskiej na planetoidzie Asimear. Tomasz Sowiński na Inferni okazuje się być dużym dzieckiem, łatwo jest wmanipulowany w otwarcie paczki - jest tam nietypowy antynanitkowy Entropik (którego psuje Elena przez błędną integrację). W tle wszystkiego - próba zdobycia niezależnej od Orbitera floty przez Aurum i jeden znikający advancer Kramera na Asimear...

Aktor w Opowieści:

* Dokonanie:
    * JUŻ NIE MIROKIN; chciała się zaopiekować biednym Sowińskim by ten nie cierpiał od Leony (wbrew sobie), jednak skończyła w szoku i medbay po integracji z dziwnym Entropikiem.


### Elena z rodu Verlen

* **uid:** 210331-elena-z-rodu-verlen, _numer względny_: 36
* **daty:** 0111-08-27 - 0111-08-30
* **obecni:** Arianna Verlen, Dariusz Blakenbauer, Elena Verlen, Krystian Blakenbauer, Romeo Verlen, Viorika Verlen

Streszczenie:

Arianna i Viorika doprowadziły do zamknięcia wojny Blakenbauer - Verlen. Viorika dotarła do Dariusza Blakenbauera i wydobyła co Blakenbauerowie zrobili Elenie (destrukcja reputacji) i co Elena zrobiła im (nie mają miejsca wśród gwiazd). Arianna natomiast wkręciła Romeo w wyznanie Elenie swych uczuć, a potem doprowadziła Elenę do płaczu. Razem przezwyciężą wszystko. Jakoś. Będzie pokój a Elena wróci do rodu Verlen.

Aktor w Opowieści:

* Dokonanie:
    * skonfrontowała się ze swoją przeszłością i czynami vs Blakenbauer. Też z miłością Romea do niej. Po dużej ilości łez i działań Arianny, przeprosiła Blakenbauerów (ze wzajemnością) i wróciła do rodu.
* Progresja:
    * W PRZESZŁOŚCI zniszczyła skutecznie reputację Blakenbauerów w Orbiterze. Nie mają wstępu na orbitę.
    * Blakenbauerowie W PRZESZŁOŚCI zapłacili ogromne sumy by niszczyć reputację Eleny na Orbiterze. To sprawia, że wszystko co złe automatycznie przylepia się do niej.
    * dotarło do niej co zrobiła. Trauma, bo skrzywdziła ogromną ilość niewinnych Blakenbauerów. Ale nie chce się już mścić... za to potrzebuje WIĘCEJ DYSCYPLINY!


### Miłość w rodzie Verlen

* **uid:** 210210-milosc-w-rodzie-verlen, _numer względny_: 35
* **daty:** 0111-08-21 - 0111-08-24
* **obecni:** Apollo Verlen, Arianna Verlen, Brunhilda Verlen, Elena Verlen, Franz Verlen, Krucjusz Verlen, Romeo Verlen, Seraf Verlen, Viorika Verlen

Streszczenie:

Arianna i Elena wróciły do domu, do rodu Verlen. Tam się okazało, że ród jest skłonny iść na wojnę z Sowińskimi za despekty (często słuszne) wyrządzane Ariannie i Elenie. Elena nie umie się odnaleźć jako akceptowana i lubiana. Elena integruje się z sentisprzężonym Eidolonem i statkiem orbitalnym; po czym w pojedynku pokonuje swojego dawnego rywala, Romeo. Wychodzą na jaw Mroczne Sekrety Eleny. A Arianna próbuje zapobiec tragedii, nie dać się wrobić w potomka i pokazuje przypadkiem najbardziej efektowny ratunek klasy arcymag ever.

Aktor w Opowieści:

* Dokonanie:
    * alias Mirokin; okazuje się, że jest lubiana w Verlen, czego nie umie zasymilować. Połączyła się z sentisiecią, wyzwała Romeo na pojedynek - i wygrała. By nie przejęło jej Esuriit, "wyłączyła" się. Cierpi na wieczny kompleks "ja siama".
* Progresja:
    * wyszło na jaw, że została pilotem by dokuczyć Romeo i że kiedyś miała za najlepszą przyjaciółkę TAI. Bardzo dziecinna była jak była mała. Nie dała się opętać Esuriit, odcinając układ nerwowy.
    * jest akceptowana taka jaka jest w rodzie Verlen i ma opcję powrotu - jeśli przeprosi (Blakenbauerów, ze wszystkich rodów). Co sprawia, że jest bardzo skonfliktowana.
    * potrafi się połączyć z sentisiecią Verlen i jest w tym mocniejsza niż się wydaje. Ale nie akceptuje rodu i siebie, więc połączenie jest słabsze. Potencjalnie najmocniejszy link z sentisiecią.


### Ratunkowa misja Goldariona

* **uid:** 210108-ratunkowa-misja-goldariona, _numer względny_: 34
* **daty:** 0111-08-09 - 0111-08-15
* **obecni:** Adam Permin, Aleksander Leszert, Elena Verlen, Feliks Przędz, Kamil Frederico, Klaudia Stryk, Martyn Hiwasser, Oliwia Pietrova, SCA Goldarion, Semla d'Goldarion, SL Uśmiechnięta

Streszczenie:

By zdobyć własne laboratorium w czarnych strefach Kontrolera, Klaudia weszła we współpracę z firmą ArcheoPrzędz i pomogła z Eleną uratować grupkę piratów, którzy bez advancera próbowali eksplorować wrak "Uśmiechniętej" - wraku Luxuritias. Po drodze przelecieli się najbardziej rozpadającym się i najbrudniejszym statkiem cywilnym - Goldarionem - jaki Klaudia kiedykolwiek widziała. Aha, i okazało się, że pojawiają się problemy na linii drakolici - fareil. Bonus: Martyn jako negocjator i mechanizm społeczny XD.

Aktor w Opowieści:

* Dokonanie:
    * alias Mirokin; jak już przebiła się przez niesprawne Eidolony, pokazała co potrafi jako advancer - zinfiltrowała "Uśmiechniętą", zebrała informacje i zniszczyła Skażoną Bię.
* Progresja:
    * dostaje dostęp do trzech średniej klasy Eidolonów należących do ArcheoPrzędz. Zrobiła też na cywilach z ArcheoPrzędz duże wrażenie.


### Sympozjum Zniszczenia

* **uid:** 210120-sympozjum-zniszczenia, _numer względny_: 33
* **daty:** 0111-08-01 - 0111-08-05
* **obecni:** Arianna Verlen, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Martyn Hiwasser, OA Bakałarz, Olgierd Drongon

Streszczenie:

Maria Naavas próbuje spiknąć Ariannę i Olgierda i do pomocy używa Eustachego. Później - Eustachy, Diana, Klaudia i Elena są na Sympozjum Zniszczenia OA Bakałarz. Próbują przebić się przez anomalną materię. Diana odkrywa słabość owej materii do Esuriit i się Skaża; Skażenie rozlewa się po Bakałarzu i Zespół dostaje opiernicz za niekompetencję (większość na Elenę). Zespół w gniewie, nienawiści i Esuriit fabrykuje Emiter Plagi Nienawiści - nową broń na pokład Inferni, piekielnie niebezpieczną...

Aktor w Opowieści:

* Dokonanie:
    * alias Mirokin; próbowała rozwiązać problem Skażenia Esuriit na Bakałarzu + ostrzegła innych; nie tylko jej się nie udało skoordynować Zespołu, co jeszcze spadło na nią że to ona winna.
* Progresja:
    * wszystkie niepowodzenia z Esuriit i z opanowaniem tego przez Zespół spadły na nią, mimo, że nie ma z tym nic wspólnego. Duży cios reputacyjny; sama z tego już nie wyjdzie.
    * bardzo, bardzo rozczarowana Eustachym i jego podejściem do Diany. Nie zatrzymał jej. Nie opanował jej. I wszystko spada na Elenę. A mógł zadziałać.
    * reputacja "świetni w niszczeniu i destrukcji, ale idźcie na bok i nie psujcie więcej". Takie trochę dzieci od zniszczenia.


### SOS z haremu

* **uid:** 210106-sos-z-haremu, _numer względny_: 32
* **daty:** 0111-07-26 - 0111-07-27
* **obecni:** Arianna Verlen, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Horacy Aktenir, Julia Aktenir, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Rozalia Teirik

Streszczenie:

Pałacyk rodu Aktenir został infestowany przez kralotha. Julia, zesłana z Orbitera za zerwanie z przeciwnikiem Eustachego, wysłała SOS do Orbitera. Infernia rusza Julii na pomoc. Gdy odkryli że chodzi o kralotha to zamiast wchodzić na kralotyczny teren, zorganizowali plan "arystokratki na orgię" i wyciągneli kralotycznego niewolnika (Horacego Aktenira) na otwartą przestrzeń, po czym Arianna uruchomiła obcą sentisieć a Eustachy zawołał do Julii rozmytej w środku...

Aktor w Opowieści:

* Dokonanie:
    * alias Mirokin; bardzo próbowała uratować Rozalię przed zakochaniem się w Eustachym - nie udało jej się. Jest na siebie zła i zła na Eustachego.
* Progresja:
    * STRASZNIE rozczarowana Eustachym, który rozkochał w sobie biedną Rozalię, która mu nie zawiniła i nigdy nie może go mieć...


### Porwanie Anastazji z Odkupienia

* **uid:** 210127-porwanie-anastazji-z-odkupienia, _numer względny_: 31
* **daty:** 0111-07-22 - 0111-07-23
* **obecni:** Anastazja Sowińska Dwa, Arianna Verlen, Dariusz Krantak, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OA Odkupienie, OO Infernia, Rufus Niegnat, SP Plugawy Jaszczur

Streszczenie:

Arianna, Eustachy i Klaudia oddali "Anastazję" na OA Odkupienie tylko po to, by ją stamtąd porwać. Nawet załoga Inferni o tym nie miała pojęcia. Okazało się, że Odkupienie jest sentisieciowanym specjalnym statkiem Aurum, gdzie ci co narazili się Sowińskim mają szansę na "odkupienie".

Aktor w Opowieści:

* Dokonanie:
    * alias Mirokin; nie zgadza się na złe traktowanie pretorian Anastazji. Nie zgadza się na skłócanie jej z Dianą przez Eustachego. Protestowała do Arianny. Odsunięta z akcji, bo zbyt paladyńska.
* Progresja:
    * WIE, że Eustachy próbował skłócić ją i Dianę. Nie wie, czemu. Też zachowanie Arianny i Klaudii wobec Pretorian Anastazji ją zdrażniło. Ogólnie, silny cios w relacje z dowództwem Inferni.


### Pułapka z Anastazji

* **uid:** 201230-pulapka-z-anastazji, _numer względny_: 30
* **daty:** 0111-07-19 - 0111-07-20
* **obecni:** AK Nocna Krypta, AK Rodivas, Anastazja Sowińska Dwa, Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Henryk Sowiński, Klaudia Stryk, Martyn Hiwasser, OA Zguba Tytanów, OO Infernia

Streszczenie:

Krypta "oddała Anastazję", zmieniając cywilny statek Rodivas w pułapkę na Ariannę.

Aktor w Opowieści:

* Dokonanie:
    * alias Mirokin; pilotowała Infernię najszybciej, jak tylko potrafiła. Udało jej się nie uszkodzić ani nie zniszczyć tego pięknego statku.


### Baza Zona Tres

* **uid:** 210714-baza-zona-tres, _numer względny_: 29
* **daty:** 0111-05-14 - 0111-05-16
* **obecni:** Arianna Verlen, BIA XXX d'Zona Tres, Elena Verlen, Eustachy Korkoran, Janus Krzak, Martyn Hiwasser, Ulisses Kalidon

Streszczenie:

Podczas eksploracji archaicznej noktiańskiej bazy Infernia doszła do tego, że to mityczna Zona Tres - odpowiednik "area 51", z badaniami nad magią. Infernia przez przypadek uruchomiła Bię. Zespół zamaskował Infernię jako Alivię Nocturnę i Arianna przekonała Bię do wstępnej współpracy. Okazało się, że Bia jest chora i nie postrzega wszystkiego prawidłowo, Klaudia jest z Bią sprzężona (oops). By dowiedzieć się więcej, Infernia wyleciała na orbitę i okazało się, że są na rogue planet - planecie która wypadła z orbity i zagubiła się w kosmosie...

Aktor w Opowieści:

* Dokonanie:
    * advancuje do głębszych elementów bazy, po czym ratuje Ulissesa (noktiańskiego komputerowca) przed autosystemami zbrojowni. Wycofała bezpiecznie swoją grupę.


### Po drugiej stronie Bramy

* **uid:** 210707-po-drugiej-stronie-bramy, _numer względny_: 28
* **daty:** 0111-05-11 - 0111-05-13
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Janus Krzak, Klaudia Stryk, Martyn Hiwasser, Raoul Lavanis, Tal Marczak

Streszczenie:

Infernia próbując przetransportować eksperta Eteru na konferencję przez Bramę uległa katastrofie - przeniosła się GDZIEŚ INDZIEJ, pod wodę i zniknęła Klaudia. Nie uległa katastrofalnym uszkodzeniom, ale po zachęceniu pintką Lewiatana dotarła do archaicznej podwodnej noktiańskiej bazy. Tam znajduje się Klaudia, ale coś jest z nią nie tak. Sprzęgnięta z bazą? Tak czy inaczej, infiltracja bazy dopiero się zacznie. Trzeba znaleźć Klaudię i wrócić do domu.

Aktor w Opowieści:

* Dokonanie:
    * advancerka; zinfiltrowała z Raoulem opuszczoną archaiczną bazę noktiańską. Zrobiła lay of the land i napotkała Klaudiokształtną anomalię..?


### Nieprawdopodobny zbieg okoliczności

* **uid:** 201224-nieprawdopodobny-zbieg-okolicznosci, _numer względny_: 27
* **daty:** 0111-04-15 - 0111-04-16
* **obecni:** Diana Arłacz, Elena Verlen, Grzegorz Chropst, Jasmina Perikas, Klaudia Stryk

Streszczenie:

Nieco zaniedbane dziecko bawiące się na K1. Skażona Elainka, na którą polują. Badaczka biomantyczna, która objęła rozpadające się biolab. I Klaudia, która z Eleną wplątała się w to wszystko by naprawić sytuację i trafiła na ślad ducha opiekuńczego Kontrolera Pierwszego. "Zofia"?

Aktor w Opowieści:

* Dokonanie:
    * alias Mirokin; słaba, ale chce uratować Anastazję za wszelką cenę. W walce z Grzegorzem go unieszkodliwiła, acz została bardziej ranna.
* Progresja:
    * osobisty wróg w Grzegorzu Chropście - skrzywdziła i pobiła łapczaka (który zaatakował ją pierwszy).


### Krystaliczny gniew Elizy

* **uid:** 201014-krystaliczny-gniew-elizy, _numer względny_: 26
* **daty:** 0111-03-02 - 0111-03-05
* **obecni:** Aleksandra Termia, Anastazja Sowińska, Arianna Verlen, Ataienne, Dariusz Krantak, Elena Verlen, Eliza Ira, Eustachy Korkoran, Klaudia Stryk, Marian Fartel, OO Wesoły Wieprzek

Streszczenie:

Pomoc humanitarna przybyła do Trzeciego Raju wraz z zabójcami Anastazji. Mindwarpowani komandosi noktiańscy prawie zabili Elenę, ale Anastazja została ochroniona. Do akcji weszła Eliza Ira - zażądała oddania jej Trzeciego Raju i noktian. Zespół przygotowuje się do nie-oddania Raju i uratowania jak najwięcej noktian z Aurum (jakkolwiek to nie brzmi).

Aktor w Opowieści:

* Dokonanie:
    * alias Mirokin; gdy zabójcy próbowali zabić Anastazję, oddała sporo zdrowia i użyła pełnej mocy (łącznie z Esuriit) by ratować i osłaniać Anastazję. Prawie umarła.
* Progresja:
    * bardzo ciężko ranna, przepalona i bez cienia kontroli energii. Co najmniej miesiąc bardzo ograniczonej aktywności.


### Magiczna burza w Raju

* **uid:** 200923-magiczna-burza-w-raju, _numer względny_: 25
* **daty:** 0111-02-21 - 0111-02-25
* **obecni:** Anastazja Sowińska, Arianna Verlen, Dariusz Krantak, Elena Verlen, Eliza Ira, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Marian Fartel, Marianna Lemurczak, Nikodem Sowiński

Streszczenie:

Zbliża się burza magiczna. Nie ma schronienia ani zapasów. Zespół wysłał SOS na Kontroler i do Nikodema Sowińskiego (Anastazja). Zbudowali co się da by odeprzeć burzę magiczną i im się to udało bez strat w ludziach. O dziwo, siła która im pomogła to Eliza Ira (acz dyskretnie). Anastazja wierzy że Nikodem jest jej najlepszym przyjacielem, ale on pragnie jej zguby...

Aktor w Opowieści:

* Dokonanie:
    * alias Mirokin; oswaja Anastazję; pozwoliła jej nazwać swój myśliwiec ("Gwieździsty Ptak") i powoli ją oswaja. Przyjęła smolistego gluta na siebie by chronić Anastazję. Zła na Eustachego.
* Progresja:
    * wzięła na siebie obowiązek dbania o Anastazję; w końcu to jej przeszła wersja w pewien sposób. To teraz jej 'klucz', najważniejsza rzecz. Wychować młodą.


### Śmierć Raju

* **uid:** 200916-smierc-raju, _numer względny_: 24
* **daty:** 0111-02-18 - 0111-02-20
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Celina Szilat, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Marian Fartel, Rafał Armadion, Robert Garwen, Wanessa Pyszcz

Streszczenie:

Najważniejszym problemem Raju jest pozbycie się bombardowania nojrepów. Arianna zaproponowała zuchwały pomysł - w kontrolowany sposób niech nojrepy wejdą do miasteczka i wezmą czego szukają. Udało jej się wszystkich przekonać. Sukces - nojrepy się wycofały, ale Tucznik nie jest zdolny do latania a Trzeci Raj jest zniszczony. Została tylko Ataienne i ruina. Ataienne rozpaliła hipnotycznie morale trzeciorajowców - przetrwają to wszystko.

Aktor w Opowieści:

* Dokonanie:
    * alias Mirokin; przede wszystkim babysitter Anastazji. Pomagała Eustachemu zdekombinować osłony z Tucznika.


### Arystokratka w ładowni na świnie

* **uid:** 200909-arystokratka-w-ladowni-na-swinie, _numer względny_: 23
* **daty:** 0111-02-12 - 0111-02-17
* **obecni:** Anastazja Sowińska, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, OO Galaktyczny Tucznik

Streszczenie:

Zespół wyciągnął Martyna z więzienia (legalnie). Wpakowali się na Galaktycznego Tucznika by przenieść świnie na orbitę... ale poleciała z nimi dziennikarka Izabela (o czym wiedzą) i 15letnia Anastazja Sowińska (o czym nie wiedzą). Przy lądowaniu, atak nojrepów na Tucznika spowodował katastrofę - seria Paradoksów Anastazji, uszkodzenia Tucznika itp. Zespół uratował Anastazję (choć kosztem ran Martyna) i wylądował Tucznikiem na Astorii. To jest sukces.

Aktor w Opowieści:

* Dokonanie:
    * alias Mirokin; w lekkiej depresji, że będzie świnie wozić; służyła jako zastępstwo Eustachego. Niezbyt aktywna.


### Sekrety Orbitera - historia prawdziwa

* **uid:** 200819-sekrety-orbitera-historia-prawdziwa, _numer względny_: 22
* **daty:** 0111-01-24 - 0111-02-01
* **obecni:** Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Leszek Kurzmin, Martyn Hiwasser, Sabina Servatel, Tadeusz Ursus

Streszczenie:

Elena wyzwała Leonę na pojedynek. Klaudia i Arianna wkręciły Tadeusza. Tadeusz - arcymag Eterni - używa Simulacrum w walce, więc Zespół zaczął montować plan jak Leona może jednak wygrać z Simulacrum. I wszystko skończyło się tym, że Leona wygrała, Elena jest wolna od Tadeusza i powstała holodrama "Sekrety Orbitera"...

Aktor w Opowieści:

* Dokonanie:
    * alias Mirokin; wyzwała Leonę na pojedynek. Potem przez Ariannę i Klaudię ryzykowała, że skończy u Tadeusza. POTEM pocałowała Tadeusza przez Eustachego. Jest nieszczęśliwa, ale ma jednego absztyfikanta mniej.
* Progresja:
    * Tadeusz Ursus się od niej odczepi. Leona wygrała z Tadeuszem. Dzięki Eustachemu i Ariannie Elena ma jeden (poważny) problem z głowy.
    * jest święcie przekonana, że Eustachy Korkoran jest w niej zakochany - tylko dlatego jego działania takie jakie są mają sens.
    * jej reputacja jest na stałe - to podfruwajka; raz całuje a raz strzela. Gorrrąca. Potwierdzone przez całowanie Tadeusza przed walką.
    * ma NISKĄ opinię o Ariannie. Nie wie, ile Arianna zrobiła by ją z tego wyciągnąć. Myśli że wszystko Eustachy XD.


### Wielki Kosmiczny Romans

* **uid:** 200722-wielki-kosmiczny-romans, _numer względny_: 21
* **daty:** 0111-01-10 - 0111-01-13
* **obecni:** Antoni Kramer, Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Julian Muszel, Konrad Wolczątek, Leona Astrienko, Olgierd Drongon, OO Welgat, OO Żelazko, Tadeusz Ursus

Streszczenie:

Pojawiła się plotka, że Arianna, Elena i Eustachy mają romans. Plotka pochodzi od Leony, ale ktoś ją wykorzystał. Rozwiązując plotkę Zespół doszedł do tego, że Elena ma niestabilną energię magiczną i na niektórych upiornie mocno działa, poznali też elementy przeszłości Eleny. Po wyplątaniu Eleny z jednego obsesyjnego adoratora (i wpakowanie plotki że Olgierd z Żelazka aspiruje do jej ręki), Arianna zdecydowała się z Eustachym, Klaudią i Martynem pomóc Kirasjerom w uratowaniu zaginionej Emulatorki z Nocnej Krypty...

Aktor w Opowieści:

* Dokonanie:
    * alias Mirokin; o jej rękę ubiega się sporo adoratorów, ale ona NIE CHCE. Nie do końca kontrolując magię, emitowała Esuriit. Skończyła z reputacją w strzępach, ale powiedziała Ariannie prawdę.
* Progresja:
    * opinia "tej, która nie kontroluje swojej magii" i "podrywa na lewo i prawo - ale to nie jej wina, jej magia tak działa". Nie jest brana poważnie. Jej opinia - w RUINIE.
    * potrafi emitować Esuriit; nie ma pełnej kontroli nad magią, ale jest w stanie magię świetnie instynktownie używać. Acz nie zatrzyma się pod wpływem emocji.
    * dostała Iquitas od Damiana Oriona do przetestowania.
    * Eustachy jej pomógł, odplątał jej z krwawego pojedynku arystokratycznego przeciw Konradowi Wolczątkowi. Nie jest taki zły O_O.


### Sabotaż Netrahiny

* **uid:** 200715-sabotaz-netrahiny, _numer względny_: 20
* **daty:** 0111-01-04 - 0111-01-07
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Lothar Diakon, OO Netrahina, OO Tvarana, Percival Diakon, Rufus Komczirp, Szczepan Myksza

Streszczenie:

Arianna dostała prośbę o pojawienie się na Netrahinie, dalekosiężnym krążowniku Orbitera jako mediator. Na miejscu okazało się, że to TAI Persefona jest sabotażystką Netrahiny - sygnały z Anomalii Kolapsu ją "uwolniły". Arianna i Klaudia dostały od Persefony koordynaty i kod uwalniający, po czym zamaskowały to co się stało - zniszczyły Persefonę i uszkodziły Netrahinę.

Aktor w Opowieści:

* Dokonanie:
    * alias Mirokin; przesterowała korwetę i pobiła rekordy prędkości (za co zapłacił dowódca korwety a chwałę wzięła Arianna). Potem - wykazała się mistrzostwem sabotażu.
* Progresja:
    * nie tylko jest Mistrzynią Wkurzania Eustachego ale i wygrała z nim - wycofał się z pojedynku i ją przeprosił. Morale do góry.
    * dzięki Eustachemu święcie przekonana, że Arianna jest pozerem; bierze jej chwałę i wszystkich. Zawiść wobec Arianny.


### Problematyczna Elena

* **uid:** 200708-problematyczna-elena, _numer względny_: 19
* **daty:** 0110-12-27 - 0111-01-01
* **obecni:** AK Salamin, Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leszek Kurzmin, Olgierd Drongon, OO Aurelion, OO Żelazko, Persefona d'Infernia

Streszczenie:

Na Infernię trafiła nowa podoficer - podporucznik Elena Verlen, z Castigatora. W konkursie z Eustachym prawie zniszczyli Infernię. Niedługo potem Infernia (podłatana) poleciała do Anomalii Kolapsu uratować Aurelion, który natknął się na anomaliczny kiedyś-krążownik Orbitera Salamin. Arianna uratowała kogo się da i skłoniła Salamin do samozniszczenia; Persefona d'Salamin miała jeszcze dość psychotronicznej świadomości i miłości (?) do swojej martwej kapitan.

Aktor w Opowieści:

* Dokonanie:
    * alias Mirokin; kompetentny pilot, automatycznie używająca swojej magii. Ma kij w tyłku i uważa zespół Inferni za niekompetentny. Wysłana na Infernię przez Leszka z nadzieją, że Arianna sobie poradzi.


### Narkotyczna pacyfikacja tienów na Castigatorze

* **uid:** 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze, _numer względny_: 18
* **daty:** 0110-12-19 - 0110-12-21
* **obecni:** Elena Verlen, Eleonora Perłamila, Igor Arłacz, Julia Myrczek, Kamil Burgacz, Klaudia Stryk, Konstanty Keksik, Kosmicjusz Tanecznik Diakon, Leszek Kurzmin, Łucja Larnecjat, Marta Keksik, Martyn Hiwasser, Patryk Samszar

Streszczenie:

Klaudia została poproszona przez oficer naukową na Castigatorze o zbadanie fenomenu pracowitości spolegliwości. Okazało się, że XO neutralizuje tienów wspierając dostarczanie narkotyków imprezowych-kralotycznych (anipacis). Klaudia zdecydowała się zostawić tam Martyna na stopniowe wyciszanie anipacis. Nie doszła do tego kto DOKŁADNIE jest konwerterem anipacis na Feromon Ambicji, ale zostawiła sprawę wystarczająco bezpieczną.

Aktor w Opowieści:

* Dokonanie:
    * (nieobecna), od pewnego czasu ma podawane _anipacis_, przez co jest milsza i mniej groźna. Buduje przyjaźnie. Konwertuje narkotyk w feromon ambicji.
* Progresja:
    * przez narkotyki kralotyczne (anipacis) o których nie wie że zażywa, zaczyna budować przyjaźnie na Castigatorze a nawet ma chłopaka XD


### Bladawir kontra przemyt tienów

* **uid:** 231220-bladawir-kontra-przemyt-tienow, _numer względny_: 17
* **daty:** 0110-12-06 - 0110-12-11
* **obecni:** Antoni Bladawir, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Ewa Razalis, Kazimierz Darbik, Klaudia Stryk, Marcinozaur Verlen, OO Infernia, OO Karsztarin, Raoul Lavanis, Szymon Orzesznik, Zaara Mieralit

Streszczenie:

Komodor Bladawir postanowił usunąć przemyt tienów z Karsztarina (najpewniej Program Kosmiczny Aurum), każąc swoim jednostkom robić wyczerpujące patrole i przeszukiwanie jednostek pod kątem owego przemytu. Tempo jest zabójcze nawet dla Arianny i Inferni - zwłaszcza, że Arianna nie wie po co to wszystko robi. Śledztwo Klaudii pokazuje, że Bladawir najpewniej umieścił potwora na Karsztarinie by zmusić przemytników do rozpaczliwego ruchu. Arianna nie może nikogo ostrzec, ale używa połączenia Krwi z Eleną - i w ten sposób dowiaduje się o roli Orzesznika. Klaudia dyskretnie przekazuje informacje Ewie Razalis. Operacja Bladawira się nie tylko nie udaje, ale trafił na dywanik i jego plan się rozsypał. Bladawir powiedział Ariannie, że ona ALBO będzie z nim współpracować albo on jej nie chce. Nadal nie wie, że to Arianna jest architektem jego porażki.

Aktor w Opowieści:

* Dokonanie:
    * czarny koń Arianny; komunikacja po Krwi sprawiła, że Elena za plecami Bladawira mogła pomóc Ariannie dowiedzieć się co wie Kurzmin (swoją omnidetekcją). Zapłaciła reputacją, ale dowiedziała się tego co było potrzebne.
* Progresja:
    * nie ma żadnego szacunku do magów Aurum. I jej zdaniem reguły mają znaczenie WIĘKSZE niż dobre serce itp.
    * podpada na Castigatorze na lewo i prawo, bo wykonuje misję dla Arianny. Sabotaż dla maskowania śladów wyszedł że przekora. Dostaje opinię niesterowalnej i złośliwej.


### Niemożliwe nieuczciwe ćwiczenia Bladawira

* **uid:** 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira, _numer względny_: 16
* **daty:** 0110-11-26 - 0110-11-30
* **obecni:** Antoni Bladawir, Arianna Verlen, Elena Verlen, Kamil Lyraczek, Klaudia Stryk, Lars Kidironus, Marta Keksik, OO Infernia, OO Paprykowiec, Patryk Samszar, Zaara Mieralit

Streszczenie:

Bladawir przedstawia Inferni Zaarę i informuje o ćwiczeniach, rozgrywając Zaarę przeciw Inferni. Klaudia odkryła że jednostki jakie Infernia ma eskortować mają _Pain Module_. Arianna prosząc przez Krew Elenę doprowadziła do neutralizacji tych modułów. Ćwiczenia okazują się prawie niemożliwe, ale Infernia poświęciła Paprykowiec, 'zniszczyła' jednostki w przewadze i uratowała załogę. Odwróciła ćwiczenia Bladawira przeciw niemu, przy okazji podnosząc swoją chwałę i pokazując że nie będzie grać w cudze gry.

Aktor w Opowieści:

* Dokonanie:
    * niechętnie odpowiedziała na prośbę Arianny, ale czyny Bladawira były po prostu złe. Zinfiltrowała Paprykowiec i skutecznie sabotowała pain amplifier, po czym wydostała się niezauważenie. Zrobiła to częściowo dla Arianny (mimo jej ogromnej niechęci do kuzynki) a częściowo ponieważ czyn Bladawira był zły. Świetna akcja o której nikt nie wie.


### Komodor Bladawir i Korona Woltaren

* **uid:** 231109-komodor-bladawir-i-korona-woltaren, _numer względny_: 15
* **daty:** 0110-10-27 - 0110-11-03
* **obecni:** Aneta Woltaren, Antoni Bladawir, Arianna Verlen, Elena Verlen, Feliks Walrond, Klaudia Stryk, Krzysztof Woltaren, Leszek Kurzmin, Lidia Woltaren, Marta Keksik, OO Paprykowiec, Patryk Samszar, SC Korona Woltaren, Tomasz Rewernik, Wojciech Mykirło

Streszczenie:

Komodorowi Bladawirowi podpadła "Korona Waltaren" i wysłał Ariannę z zespołem na inspekcję. Podczas inspekcji Arianna i Klaudia odkrywają różne problemy, w tym zagrożenia środowiskowe i sanitarne. Mimo problemów, załoga broni Anety i staje za swoim statkiem; kolejna grzywna może "Koronę" utopić. Arianna opracowuje plan, który pozwoli statkowi uniknąć grzywny przez złożenie prośby o ratunek. Kilka miesięcy później jednak Elena szukając kralotha wykrywa coś dziwnego na tej jednostce, ale nie umie tego nazwać. Okazuje się, że na "Koronie" która była niedaleko Iorusa zalągł się terrorform ixioński i gdyby nie poświęcenie młodej tienki, Marty, Zespół mógłby zginąć. Zespół ewakuuje Koronę (która zostaje porzucona) i mimo wielu rannych udaje się większość uratować. Bladawir jest zadowolony - ałoga 'Korony' trafi w okolice Anomalii, 'gdzie ich miejsce'.

Aktor w Opowieści:

* Dokonanie:
    * wykryła 'coś' na Koronie Woltaren (ale nie kralotha; tam był ixioński pacyfikator-terrorform). Pewna że coś jest nie tak zignorowała polecenie kmdr. Walronda i rzuciła czar, uszkadzając 'drzewo'. OPIEPRZ.
* Progresja:
    * potężny opieprz od komodora Walronda, który uważa ją za jakąś maskotkę. Elena ma to gdzieś.


### Śpiew NieLalki na Castigatorze

* **uid:** 231025-spiew-nielalki-na-castigatorze, _numer względny_: 14
* **daty:** 0110-10-24 - 0110-10-26
* **obecni:** Anna Tessalon, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Feliks Walrond, Igor Arłacz, Klaudia Stryk, Konstanty Keksik, Leona Astrienko, Leszek Kurzmin, Marta Keksik, OO Castigator, OO Infernia, Patryk Samszar, Raoul Lavanis, TAI Eszara d'Castigator

Streszczenie:

Na pokład Castigatora tieni przemycili NieLalkę Altarient i zaczęły mieszać się Znaczenia oraz Rzeczywistość. Zespół doprowadził do kontrolowanego opanowania i wygaszania Castigatora, po czym po zlokalizowaniu Anomalii Altarient Raoul został wysłany by ją zaizolować. Infernia ją zestrzeliła. A Arianna wykorzystała tą okazję by młodzi tieni wzięli się w garść i nie robili głupot. Szczęśliwie, Admiralicja nic nie wie, acz było nieciekawie przez moment.

Aktor w Opowieści:

* Dokonanie:
    * wiedząc o losie Natalii Gwozdnik, opuściła Castigator i dołączyła do sił komodora Walronda. Myśli, że to z uwagi na jej wysoką kompetencję i rekomendacje, bo jest Verlenką polującą na potwory.


### Ekstaflos na Tezifeng

* **uid:** 231011-ekstaflos-na-tezifeng, _numer względny_: 13
* **daty:** 0110-10-20 - 0110-10-22
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudiusz Terienak, Lars Kidironus, Leona Astrienko, Leszek Kurzmin, Natalia Gwozdnik, OO Castigator, OO Infernia, OO Tezifeng, Raoul Lavanis

Streszczenie:

Jeszcze na Castigatorze, Arianna i Kurzmin z przyjemnością odbudowali znajomość i wspominki. Gdy Elena chcąc ratować Leonę 'zabiła' jej anomalię, Leona wyzwała ją na pojedynek. Elena przegrała, choć było blisko; będzie musiała być służką Leony przez pewien czas, co Castigator odnotował z radością. Kurzmin poprosił Ariannę o znalezienie i pomoc dwóm tienom w rutynowym patrolu (najpewniej przekroczyli paliwo). Niestety, Tezifeng, korweta Castigatora podczas operacji celnych natrafiła na coś kralotycznego. Klaudiusz Terienak został zmieniony w ekstaflos a Natalia Gwozdnik została porwana. Pułapka na Infernię została wykryta przez Ariannę (bo Klaudiusz zachowywał się nie tak jak powinien). Szybka operacja wejścia - wydobycia jednego jeńca (marionetki kralotycznej) i zbadanie sytuacji sprawiła, że Eustachy działkami unieszkodliwił Tezifeng i Arianna wezwała wsparcie - kraloth to coś bardzo wysokiej klasy.

Aktor w Opowieści:

* Dokonanie:
    * sama, na boku od wszystkich. Weszła w konflikt z Leoną (nie sprowokowała!) i weszły w zakład o zabijanie 'Skażeń Saitaera'. Przegrała zakład, przez co będzie musiała być służką Leony przez jakiś czas.
* Progresja:
    * będzie służką Leony przez jakiś tydzień gdy będzie okazja; przegrała zakład eksterminacji małych 'ech Saitaera' na Castigatorze.
    * serio nie cierpi Eustachego i Leony. Naprawdę. Może nie do poziomu nienawiści, ale są jej najmniej ulubionymi ludźmi.


### Gdy HR reedukuje niewłaściwą osobę

* **uid:** 230111-gdy-hr-reedukuje-niewlasciwa-osobe, _numer względny_: 12
* **daty:** 0100-11-13 - 0100-11-16
* **obecni:** Adragain Ferrias, AK Nox Ignis, Aleksy Sartaran, Arianna Verlen, Daria Czarnewik, Elena Verlen, Grażyna Burgacz, Kajetan Kircznik, Lana Mirkinin, Leszek Kurzmin, Maja Samszar, Miłosz Klinek, OO Astralna Flara, OO Athamarein, OO Loricatus, Persefona d'Loricatus, Sabrina Ferrias, Salazar Bolza, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Niestety, Kircznik (lekarz) przestraszył Sargona i młodych infiltratorów Lodowcem, Bolzą i ogólnie duchami noktiańskimi. Okazało się, że Grażyna jest ekspertem od duchów i jest w stanie zrobić egzorcyzm. Niedługo potem (i chwilę przed egzorcyzmem) pojawiła się szybka korweta z Biur HR i poprosili o pomoc w odparciu Anomalii Kosmicznej. Myrczek sprawdził jej pamięć i Athamarein i Flara poleciały na starcie z Nox Ignis. Nawiązały kontakt z Biurami HR, przechwyciły biednego męża pilotki Nox Ignis i poznały prawdę o Destructor Animarum i wpływie Syndykatu Aureliona na Biura HR. Kurzmin pobił rekordy Athamarein, Arianna skutecznie zaplanowała wypalenie pilotki Nox Ignis przez własny statek. Tylko advancer Miłosz (badający ślady Nox Ignis) jest MIA.

Aktor w Opowieści:

* Dokonanie:
    * bierze się każdej możliwej misji by tylko NIE rozmawiać z Arianną odnośnie tego co się działo ostatnio ;-)


### Astralna Flara i nowy komodor

* **uid:** 221221-astralna-flara-i-nowy-komodor, _numer względny_: 11
* **daty:** 0100-11-07 - 0100-11-10
* **obecni:** Arianna Verlen, Daria Czarnewik, Elena Verlen, Ellarina Samarintael, Kajetan Kircznik, Kaspian Certisarius, Leszek Kurzmin, Maja Samszar, NekroTAI Zarralea, OO Astralna Flara, OO Athamarein, OO Loricatus, Salazar Bolza, Sargon Niiris, Władawiec Diakon

Streszczenie:

Po "uratowaniu" noktian z Dominy Lucis Arianna dostała medal a Lodowca zastąpił komodor Bolza, zimny drakolita. Gdy grupa wydzielona wróciła na teren Anomalii Kolapsu okazało się, że planetoida Kazmirian ma squatterów; Zespół skutecznie ich przejął odzyskując kontrolę nad systemami Semli d'Kazmirian. Na miejscu - TKO-4271 - okazało się że Zarralea jest zniszczona. Orbiter odzyskał swoją bazę, acz Loricatis musi współdzielić Persefonę. Z innej beczki: link Elena - Władawiec się pięknie rozwija.

Aktor w Opowieści:

* Dokonanie:
    * skonfrontowała się z Arianną - nie podoba jej się Dark Awakening Zarralei. Straciła trochę wiary w Orbitera, ale Arianna robi co może by Elena te rzeczy utrzymała. Wraz z Władawcem przeprowadziła niesamowity taniec z szablami, po czym uciekła ze wstydu. NAPRAWDĘ jest szczęśliwa podczas tańca.
* Progresja:
    * nadal uważa, że Arianna wie lepiej i zrobiła co powinna, ale wie już że Arianna inaczej postrzega to co ważne niż ona - dla Eleny liczy się 'działanie prawidłowo', dla Arianny 'jak najwięcej żyć które przetrwają'. Zaczyna się rift między nimi. Ale jest uspokojona przez Ariannę odnośnie Orbitera i decyzji Orbitera.


### Astralna Flara w strefie duchów

* **uid:** 221130-astralna-flara-w-strefie-duchow, _numer względny_: 10
* **daty:** 0100-10-05 - 0100-10-08
* **obecni:** Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Elena Verlen, Ellarina Samarintael, Gabriel Lodowiec, Hubert Kerwelenios, Kajetan Kircznik, Kirea Rialirat, Klarysa Jirnik, Leo Kasztop, Maja Samszar, NekroTAI Zarralea, OO Astralna Flara, OO Athamarein, Szymon Wanad, Tomasz Dojnicz

Streszczenie:

Planetoida Kazmirian została przejęta przez Orbiter. Daria ostrzegła Nonarion (Leo), że Orbiter nadal poluje na TAI i zdobyła informację o 'strefie duchów'. Z Mają doszły do tego, że 'strefa duchów' jest świetnym maskowaniem; Lodowiec wyprowadził tam Orbiter a Arianna stworzyła nekroTAI z Zarralei d'Isigtand. Używając advancera przechwycili samotną savarankę (której Lodowiec nie umie przesłuchać) i stanęło na tym, że 'strefa duchów' jest bazą noktiańską zbudowaną dookoła jednostki wsparcia, 'Domina Lucis'. Lodowiec chce doprowadzić noktian do sprawiedliwości Orbitera, Arianna i Daria są skonfliktowane. Mają jeńca - Kireę, lokalną młodą savarankę.

Aktor w Opowieści:

* Dokonanie:
    * płynnie przesunęła Flarę by Klara ewakuowała Huberta nanowłóknem. Świetnie pilotuje.


### Astralna Flara dociera do Nonariona Nadziei

* **uid:** 221116-astralna-flara-dociera-do-nonariona-nadziei, _numer względny_: 9
* **daty:** 0100-09-08 - 0100-09-11
* **obecni:** Alan Nierkamin, Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Gabriel Lodowiec, Grażyna Burgacz, Kajetan Kircznik, Leo Kasztop, Leszek Kurzmin, Maja Samszar, Marcel Kulgard, OO Astralna Flara, OO Athamarein, SCA Hadiah Emas, SCA Isigtand, Szymon Wanad, Tomasz Ruppok, Władawiec Diakon

Streszczenie:

Flara dociera do Nonariona, który wygląda jak absolutny śmieć. Morale na Flarze ucierpiało - aż do plotek o Egzotycznych Pięknościach, co sprawiło Ariannie kolejne problemy. Alan, eks-lokals powiedział Darii że jest jakaś forma standaryzacji na Nonarionie - coś tam jest, coś "bogatego" jako frakcja. Daria zdobyła od Leo planetoidę którą może użyć jako bazę dla Orbitera, ale w drodze tam zboczyli by ratować uszkodzone statki lokalne. I uratowali jednostkę niewolniczą i zniszczony grazer. Komodor Lodowiec - sensowny koleś - ma dylemat moralny. Neikatiańska TAI oraz niewolnicy.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * trochę się otwiera na Władawca Diakona - on jest dobry w zapasach i ona MUSI go pokonać!


### Astralna Flara i porwanie na Karsztarinie

* **uid:** 221102-astralna-flara-i-porwanie-na-karsztarinie, _numer względny_: 8
* **daty:** 0100-07-28 - 0100-08-03
* **obecni:** Ada Wyrocznik, Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Elena Verlen, Gerwazy Kircznik, Grażyna Burgacz, Hubert Kerwelenios, Kajetan Kircznik, Klarysa Jirnik, Maja Samszar, Mariusz Bulterier, OO Karsztarin, OO Optymistyczny Żuk, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Arianna otrzymała nowy okręt - jednostkę wsparcia Astralna Flara. Jako pilota dostała Elenę. Elena ma problemy z aklimatyzacją na Orbiterze; chce być najlepsza i walczy o to. Nie rozmawia z ludźmi. Arianna zdecydowała się poprosić Alezję o pomoc w tej dziedzinie. Tymczasem przy ćwiczeniach na Karsztarinie okazało się, że Flara jest tam w czasie porwania - Zespołowi Flary udało się zatrzymać porwanie, choć operacja była niesamowicie niebezpieczna - niedoświadczony Zespół vs 4 komandosów Syndykatu Aureliona.

Aktor w Opowieści:

* Dokonanie:
    * klasyczny kij w tyłku, disillusioned naleśnictwem Orbitera; ma nadzieję, że z Arianną wyprowadzą tą jednostkę jak Verleńską. Chyba ma trzy tryby - uczy się, jest na akcji i śpi. Tak chciała się wykazać w grze z innymi advancerami że straciła kontrolę nad magią, ale się wybroniła przed Arianną. Stała się arogancka, nigdy nie była. Nieskończenie głodna bycia NAJLEPSZĄ. Gdy robili super groźną insercję przy wirującym statku, jej moc zintegrowała skafandry i udało im się bezpiecznie wejść.
* Progresja:
    * ma w papierach, że jest świetnym pilotem i advancerem, ale nikt nie chce z nią pracować. Ma nagany i przesunięcia. Chwalona jak działa solo. Else - nagany.
    * wchodząc w warp Astralną Flarą zrobiła to szybciej i skuteczniej niż inni. Już zaczyna mieć opinię 'superstar' na Flarze.


### Samotna w programie Advancer

* **uid:** 221004-samotna-w-programie-advancer, _numer względny_: 7
* **daty:** 0099-05-20 - 0099-05-29
* **obecni:** Elena Verlen, Hubert Mirsz, Kacper Wentel, Lara Kiriczko, Michał Warkoczak, OO Karsztarin, Sandra Kantarelo, Tymon Krakdacz

Streszczenie:

Elena jest w programie Advancer na K1, ale jest na uboczu grupy. Radzi sobie świetnie kompetencyjnie, ale boi się zbliżyć do innych by ich nie skrzywdzić chaotyczną magią. To ma punkt kulminacyjny gdy jest odrzucona i tauntowana; traci kontrolę i wir magii rani jej zespół i uszkadza pokój. Elena zostaje przeniesiona - jej podejście stało się przyczyną jej problemów. Szczęśliwie, Sandra się do niej zbliżyła i Elena zrozumiała co robi nie tak.

Aktor w Opowieści:

* Dokonanie:
    * najmłodsza w programie Advancer Orbitera na K1; unika wszystkich by nikogo nie zranić i chce wygrywać samodzielnie. Kompetencyjnie świetna, emocjonalnie jak sprężyna. W końcu, odrzucona przez wszystkich poza Sandrą wybucha i jej moc magiczna zadaje straszne rany wszystkim obecnym aż Sandra ją zneutralizowała.
* Progresja:
    * przeniesiona z programu 'advancer' do innego programu po utracie kontroli i zadaniu ciężkich ran i NAGANA DO AKT.
    * złożyła pierwszy raz prośbę o neurosprzężenie
    * opinia niebezpiecznej i niestabilnej, ale też o bardzo dużym potencjale


### Lustrzane odbicie Eleny

* **uid:** 210324-lustrzane-odbicie-eleny, _numer względny_: 6
* **daty:** 0097-09-02 - 0097-09-05
* **obecni:** Arianna Verlen, Elena Verlen, Lucjusz Blakenbauer, Michał Perikas, Przemysław Czapurt, Romeo Verlen, Viorika Verlen

Streszczenie:

Elena, chcąc utrzymać kontrolę nad swoją mocą zaczęła brać wiktoriatę. To plus przepuszczanie magii przez lustra zniszczyło jej wzór i ją uszkodziło. Elena stała się wampirem - pożera energetycznie żołnierzy by chronić teren i wszystkich cywilów. Plus, pragnie zemsty na Blakenbauerach i na Perikasach. W swoim Skażeniu powołała Lustrzanego Golema. Viorika i Arianna dały radę wymanewrować Elenę, odkryć, że to ona stoi za Lustrzanym Golemem i z pomocą sierżanta Czapurta w Poniewierzy dały radę Elenę unieszkodliwić, by ją docelowo naprawić...

Aktor w Opowieści:

* Dokonanie:
    * perfekcyjna kontrola ciała, umysłu i magii dzięki wiktoriacie; ukrywa ślady pod ostrym gotyckim makijażem. Zabija i "znika" żołnierzy, by chronić cywilów. Nie straciła jednak resztek duszy. Ostatecznie - zamknięta z Arianną (przez Ariannę) na Arenie Poniewierskiej aż przybył Czapurt z oddziałem by ją unieszkodliwić.
* Progresja:
    * narkotyki i przepuszczanie swoją energię przez lustra ZNISZCZYŁO jej Wzór. Co najmniej pół roku naprawy.
    * stała się (tymczasowo) wampirem. Zabiera życie żołnierzy; zabiła 20 osób, które jej zaufały i były po jej stronie... jest uznana za POTWORA.
    * STRASZNE uprzedzenie do rodu Blakenbauerów, Blakenbauerów, każdego Blakenbauera, narzędzi Blakenbauerów i rzeczy na literę "B". PLUS arystokracji Aurum.


### Studenci u Verlenów

* **uid:** 210311-studenci-u-verlenow, _numer względny_: 5
* **daty:** 0097-01-16 - 0097-01-22
* **obecni:** Apollo Verlen, Arianna Verlen, Dariusz Blakenbauer, Elena Verlen, Maja Samszar, Michał Perikas, Rafał Perikas, Rufus Samszar, Sylwia Perikas, Viorika Verlen

Streszczenie:

Verlenowie mieli wymianę studencką, współpracę z magami rodów Perikas i Samszar. Dariusz Blakenbauer chciał rozbić potencjalny sojusz, dając młodym Perikasom lustro mające zaspokoić ich ochotę na Elenę. Niestety, wszystko poszło katastrofalnie nie tak bo Elena ma anomalną magię - w wyniku eksplozji zginęło kilkanaście wił i 2 żołnierzy. Elena w ruinie psychicznej, wszyscy przerażeni. Wezwane Arianna i Viorika odkryły że za wszystkim stoi Blakenbauer i znalazły winnych wśród młodych Perikasów. Jednak - dla polityki - wszystko zwaliły na Blakenbauerów :D.

Aktor w Opowieści:

* Dokonanie:
    * 16 lat, próbowała opanować młodych, lecz nie miała autorytetu. Potem pod wpływem rezonansu z lustrem (podłożonym przez Blakenbauera i Perikasa) doszło do erupcji jej mocy i zginęły 2 osoby i 14 wił. Schowała się w jaskini; Arianna i Viorika ją wyciągały. Tak bardzo, bardzo pragnęła się wykazać i być jak Arianna.
* Progresja:
    * powszechnie uważana w Verlenlandzie za niebezpieczną. Verlenka, która straciła kontrolę. Elena - przecież przyjazne stworzenie - się całkowicie oddaliła od "przeciętnego człowieka". Znienawidzona w Poniewierzy.


### Karolinka, nieokiełznana świnka

* **uid:** 230419-karolinka-nieokielznana-swinka, _numer względny_: 4
* **daty:** 0095-07-19 - 0095-07-23
* **obecni:** Aleksander Samszar, Apollo Verlen, Arianna Verlen, Elena Verlen, Fantazjusz Verlen, Marcinozaur Verlen, Tymek Samszar, Ula Blakenbauer, Viorika Verlen

Streszczenie:

Zaprojektowano świnkę Karolinkę do pomszczenia Vioriki. Aria, Elena i Viorika ruszyły do Powiatu radząc sobie z figlarnym glukszwajnem. Po drodze Elena rozbiła vana w menhir, wintegrowała niewinnego Samszara w menhir, porzuciły tam świnkę i dotarły do Siewczyna. Tam Elena próbowała być słodka i współpracować z Samszarem. O dziwo, udało się przekonać Samszara by pozwolił Verlenkom zmierzyć się ze Strażnikiem. W wyniku Paradoksów, 'potwór został porwany' i uciekł do Verlenlandu a Elenie po raz pierwszy uruchomiła się moc Esuriit.

Aktor w Opowieści:

* Dokonanie:
    * lat 15, superpoważna agentka specjalnej operacji 'pomścić Viorikę', pilotowała vana (aż się rozbiła o menhir; Karolinka wyssała jej energię). Wkleiła Tymka Samszara w menhir. Potem słodyczą (plan Arianny) próbowała przekonać Aleksandra Samszara do współpracy. I na końcu - Paradoks. Kłębek powagi zespołu.
* Progresja:
    * po raz pierwszy uruchomiła jej się moc Esuriit gdy interaktowała ze Strażnikiem / Hybrydą.


### Strasznołabędź atakuje granicę

* **uid:** 230502-strasznolabedz-atakuje-granice, _numer względny_: 3
* **daty:** 0095-07-17 - 0095-07-19
* **obecni:** Elena Verlen, Maks Samszar, Marcinozaur Verlen, Szymon Kapeć, Ula Blakenbauer, Viorika Verlen

Streszczenie:

Marcinozaurowi uciekł łabędź (Ula miała Paradoks, Marcinozaur ją kryje). Viorika z Eleną ruszyły go przechwycić w samszarskim Forcie Tawalizer. Chciały wbić się dyskretnie, ale Maks Samszar ma za dobrych ludzi. Gdy rozbroili Viorikę, łabędź się ruszył. Zła współpraca między verlenlandczykami i samszarowcami doprowadziła do tego, że łabądź zabił jednego z samszarowców i PRAWIE uciekł - Elena go jednak zatrzymała uszkadzając poważnie Lancera (i kończąc ranna). Cholernie ryzykowny ruch. Viorika zestrzeliła łabędzia i potem współpracuje z Maksem by problem się nie pojawił.

Aktor w Opowieści:

* Dokonanie:
    * pała entuzjazmem do KAŻDEJ operacji Vioriki, też polowanie na łabędzia. Służy jako zasób 'cuteness' do przekonania Maksa. Jako jedyny mag wspiera oddział Verlenów walczący z łabędziem, ledwo sobie radzi i robi SKRAJNIE ryzykowne manewry. Ale się udało.


### Dźwiedzie polują na ser

* **uid:** 230412-dzwiedzie-poluja-na-ser, _numer względny_: 2
* **daty:** 0095-07-12 - 0095-07-14
* **obecni:** Apollo Verlen, Arianna Verlen, Elena Verlen, Marcinozaur Verlen, Ula Blakenbauer, Viorika Verlen

Streszczenie:

Koszarów Chłopięcy ma problem - Apollo i Marcinozaur się kłócą o płaszczkę Blakenbauerów. Więc przybywa Arianna i Viorika na prośbę Apollo. Nie tylko łagodzą sprawę i poznają prawdę (chodzi o honor Eleny i by pokazać wartość Blakenbauerów Verlenom i vice versa) ale jeszcze robią wyzwanie między Ulą Blakenbauer i Verlenami. Okazuje się, że Ula jest niesamowicie silna, ale taktyka Vioriki, dźwiedzie Verlenów i umiejętności ścigaczowania Arianny (w co nadal nikt nie wierzy bo ona nie umie pilotować XD) wygrały konflikt bezkrwawo. Poszukiwania pochodzenia Szeptomandry trwają, zajmuje się tym Marcinozaur i Ula.

Aktor w Opowieści:

* Dokonanie:
    * (nieobecna) gdy zbliżała się do Szeptomandry, zaczęła mieć halucynacje i krzyżówki tego co było naprawdę z Szeptami. Gdyby nie Apollo, skrzywdziłaby swoich żołnierzy. Dlatego Apollo ją odesłał.
* Progresja:
    * okazuje się wyjątkowo podatna na Szeptomandrę i ataki tego typu


### KJS - Wygrać za wszelką cenę!

* **uid:** 230124-kjs-wygrac-za-wszelka-cene, _numer względny_: 1
* **daty:** 0095-06-23 - 0095-06-25
* **obecni:** Apollo Verlen, Elena Verlen, Emilia Lawendowiec, Karol Atenuatia, KDN Kajis, Lidia Nemert, Lucas Oktromin, Marta Krissit, Serena Krissit, Wojciech Namczak, Zuzanna Kraczamin, Żaneta Krawędź

Streszczenie:

Koszarów Chłopięcy jest miejscem, gdzie młodzi z Verlenlandu ćwiczą i rozwijają cnoty sportowe. Pojawiły się tam nietypowe narkotyki / środki, które nie są pochodzenia stricte chemicznego. Zespół Kajisa ma minimalny poziom energii i musi awaryjnie tam lądować by jak najszybciej uzyskać baterie irianium lub esencji. Na miejscu Lucas wdał się w pyskówkę z nastoletnią Eleną Verlen - nie pomoże jej rozwiązać problemu, bo nie współpracuje z gówniarami.

Aktor w Opowieści:

* Dokonanie:
    * 15 lat. Pomalowana na emo. W drobnym mundurze, mającym do niej pasować, z odznakami za wytrwałość i umiejętność pilotowania. Broni honoru Verlenów bijąc się z Wojtkiem (19-latkiem), ale nie ma siły fizycznej mimo techniki; wygrała przez automatyczne użycie magii. Chciała pomocy Lucasa i Zespołu by odkryć co jest dopalaczem w okolicy, ale potraktowali ją jak dziecko. Nie dała się ponieść emocjom, odeszła. Sama rozwiąże problem mimo nich.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 67, @: 0112-09-17
    1. Primus    : 67, @: 0112-09-17
        1. Sektor Astoriański    : 65, @: 0112-09-17
            1. Astoria, Orbita    : 21, @: 0112-04-24
                1. Kontroler Pierwszy    : 18, @: 0112-04-24
                    1. Arena Kalaternijska    : 1, @: 0111-02-01
                    1. Hangary Alicantis    : 3, @: 0112-02-01
                    1. Laboratoria Dekontaminacyjne    : 1, @: 0112-04-02
                    1. Sektor 22    : 1, @: 0111-07-27
                        1. Dom Uciech Wszelakich    : 1, @: 0111-07-27
                        1. Kasyno Stanisława    : 1, @: 0111-07-27
                    1. Sektor 43    : 1, @: 0111-04-16
                        1. Laboratorium biomantyczne    : 1, @: 0111-04-16
                        1. Tor wyścigowy ścigaczy    : 1, @: 0111-04-16
                    1. Sektor 49    : 1, @: 0112-04-14
                    1. Sektor 57    : 1, @: 0111-08-15
                    1. Sektor Cywilny    : 1, @: 0111-08-15
            1. Astoria, Pierścień Zewnętrzny    : 9, @: 0112-09-17
                1. Laboratorium Kranix    : 1, @: 0112-09-17
                1. Poligon Stoczni Neotik    : 3, @: 0112-05-09
                1. Stacja Medyczna Atropos    : 1, @: 0112-09-17
                1. Stocznia Neotik    : 5, @: 0112-05-09
                    1. Zewnętrzny dok ixioński    : 1, @: 0112-04-29
            1. Astoria    : 13, @: 0111-08-30
                1. Sojusz Letejski, NW    : 4, @: 0111-03-05
                    1. Ruiniec    : 4, @: 0111-03-05
                        1. Ortus-Conticium    : 1, @: 0111-02-25
                        1. Trzeci Raj, okolice    : 2, @: 0111-02-20
                            1. Kopiec Nojrepów    : 2, @: 0111-02-20
                            1. Zdrowa Ziemia    : 1, @: 0111-02-20
                        1. Trzeci Raj    : 3, @: 0111-03-05
                            1. Barbakan    : 1, @: 0111-02-20
                            1. Centrala Ataienne    : 1, @: 0111-02-20
                            1. Mały Kosmoport    : 1, @: 0111-02-20
                            1. Ratusz    : 1, @: 0111-02-20
                            1. Stacja Nadawcza    : 1, @: 0111-02-20
                1. Sojusz Letejski    : 9, @: 0111-08-30
                    1. Aurum    : 9, @: 0111-08-30
                        1. Powiat Samszar    : 2, @: 0095-07-23
                            1. Fort Tawalizer    : 1, @: 0095-07-19
                                1. Centrum Miasta (C)    : 1, @: 0095-07-19
                                    1. Luksusowa dzielnica mieszkalna    : 1, @: 0095-07-19
                                        1. Jezioro Fortowe    : 1, @: 0095-07-19
                                1. Dzielnica mieszkaniowa (S)    : 1, @: 0095-07-19
                                    1. Domy mieszkalne    : 1, @: 0095-07-19
                                    1. Sklepy i centra handlowe    : 1, @: 0095-07-19
                                    1. Szkoły i placówki oświatowe    : 1, @: 0095-07-19
                                1. Obserwatorium Potworów i Fort (E)    : 1, @: 0095-07-19
                                    1. Koszary garnizonu    : 1, @: 0095-07-19
                                1. Wzgórza Potworów (N)    : 1, @: 0095-07-19
                                    1. Baterie defensywne    : 1, @: 0095-07-19
                                    1. Farmy kóz    : 1, @: 0095-07-19
                                    1. Obserwatoria potworów    : 1, @: 0095-07-19
                            1. Siewczyn    : 1, @: 0095-07-23
                                1. Północne obrzeża    : 1, @: 0095-07-23
                                    1. Spichlerz Jedności    : 1, @: 0095-07-23
                            1. Wielkie Kwiatowisko    : 1, @: 0095-07-23
                                1. Menhir Centralny    : 1, @: 0095-07-23
                        1. Verlenland    : 7, @: 0111-08-30
                            1. Hold Karaan, obrzeża    : 1, @: 0111-08-24
                                1. Lądowisko    : 1, @: 0111-08-24
                            1. Hold Karaan    : 2, @: 0111-08-30
                                1. Barbakan Centralny    : 1, @: 0111-08-24
                                1. Krypta Plugastwa    : 2, @: 0111-08-30
                                1. Szpital Centralny    : 1, @: 0111-08-30
                            1. Koszarów Chłopięcy, okolice    : 1, @: 0095-07-14
                                1. Las Wszystkich Niedźwiedzi    : 1, @: 0095-07-14
                                1. Skały Koszarowe    : 1, @: 0095-07-14
                            1. Koszarów Chłopięcy    : 2, @: 0095-07-14
                                1. Miasteczko    : 1, @: 0095-06-25
                                    1. Bar Krwawy Topór    : 1, @: 0095-06-25
                                1. Wielki Stadion    : 1, @: 0095-06-25
                            1. Mikrast    : 1, @: 0097-09-05
                            1. Poniewierz, obrzeża    : 1, @: 0097-01-22
                                1. Jaskinie Poniewierskie    : 1, @: 0097-01-22
                            1. Poniewierz, północ    : 1, @: 0097-01-22
                                1. Osiedle Nadziei    : 1, @: 0097-01-22
                            1. Poniewierz    : 3, @: 0111-08-30
                                1. Arena    : 1, @: 0097-09-05
                                1. Dom Uciech Wszelakich    : 1, @: 0097-01-22
                                1. Zamek Gościnny    : 3, @: 0111-08-30
                            1. Skałkowa Myśl    : 1, @: 0097-09-05
                            1. VirtuFortis    : 1, @: 0095-07-23
                                1. Wielki Plac Miraży    : 1, @: 0095-07-23
                        1. Wielkie Księstwo Aktenir    : 1, @: 0111-07-27
                            1. Pałac Jasnego Ognia    : 1, @: 0111-07-27
            1. Brama Kariańska    : 5, @: 0112-03-23
            1. Elang, księżyc Astorii    : 1, @: 0112-07-01
                1. EtAur Zwycięska    : 1, @: 0112-07-01
            1. Iorus    : 5, @: 0112-06-14
                1. Iorus, pierścień    : 5, @: 0112-06-14
                    1. Keldan Voss    : 5, @: 0112-06-14
            1. Krwawa Baza Piracka    : 1, @: 0112-03-09
            1. Neikatis    : 1, @: 0112-04-17
                1. Dystrykt Lennet    : 1, @: 0112-04-17
            1. Libracja Lirańska    : 5, @: 0111-01-07
                1. Anomalia Kolapsu, orbita    : 3, @: 0100-11-16
                    1. Strefa Biur HR    : 1, @: 0100-11-16
                    1. Strefa Upiorów Orbitera    : 3, @: 0100-11-16
                        1. Planetoida Kazmirian    : 2, @: 0100-11-10
                        1. Planetoida Lodowca    : 3, @: 0100-11-16
                1. Anomalia Kolapsu    : 2, @: 0111-01-07
            1. Pas Teliriański    : 3, @: 0111-11-12
                1. Planetoidy Kazimierza    : 3, @: 0111-11-12
                    1. Planetoida Asimear    : 3, @: 0111-11-12
                        1. Stacja Lazarin    : 2, @: 0111-11-12
            1. Stocznia Kariańska    : 1, @: 0112-03-23
        1. Sektor Mevilig    : 3, @: 0112-03-26
            1. Chmura Piranii    : 2, @: 0112-03-23
            1. Keratlia    : 1, @: 0112-03-23
            1. Planetoida Kalarfam    : 2, @: 0112-03-26
        1. Sektor Noviter    : 1, @: 0112-03-16
        1. Zagubieni w Kosmosie    : 2, @: 0111-05-16
            1. Crepuscula    : 2, @: 0111-05-16
                1. Pasmo Zmroku    : 2, @: 0111-05-16
                    1. Baza Noktiańska Zona Tres    : 2, @: 0111-05-16
                    1. Podwodna Brama Eteryczna    : 1, @: 0111-05-13

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 61 | ((200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210210-milosc-w-rodzie-verlen; 210218-infernia-jako-goldarion; 210311-studenci-u-verlenow; 210317-arianna-podbija-asimear; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220610-ratujemy-porywaczy-eleny; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 231011-ekstaflos-na-tezifeng; 231018-anomalne-awarie-athamarein; 231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Klaudia Stryk        | 50 | ((200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201224-nieprawdopodobny-zbieg-okolicznosci; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210108-ratunkowa-misja-goldariona; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210707-po-drugiej-stronie-bramy; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220610-ratujemy-porywaczy-eleny; 231018-anomalne-awarie-athamarein; 231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Eustachy Korkoran    | 46 | ((200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220610-ratujemy-porywaczy-eleny; 231011-ekstaflos-na-tezifeng; 231018-anomalne-awarie-athamarein; 231025-spiew-nielalki-na-castigatorze; 231220-bladawir-kontra-przemyt-tienow)) |
| Martyn Hiwasser      | 25 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210108-ratunkowa-misja-goldariona; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 220610-ratujemy-porywaczy-eleny; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Leona Astrienko      | 21 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 210106-sos-z-haremu; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 220610-ratujemy-porywaczy-eleny; 231011-ekstaflos-na-tezifeng; 231018-anomalne-awarie-athamarein; 231025-spiew-nielalki-na-castigatorze)) |
| OO Infernia          | 15 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210825-uszkodzona-brama-eteryczna; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Izabela Zarantel     | 11 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Kamil Lyraczek       | 8 | ((200909-arystokratka-w-ladowni-na-swinie; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211020-kurczakownia; 211215-sklejanie-inferni-do-kupy; 220105-to-nie-pulapka-na-nereide; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Leszek Kurzmin       | 8 | ((200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Maria Naavas         | 8 | ((210120-sympozjum-zniszczenia; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220316-potwor-czy-choroba-na-etaur)) |
| Viorika Verlen       | 7 | ((210210-milosc-w-rodzie-verlen; 210311-studenci-u-verlenow; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230502-strasznolabedz-atakuje-granice)) |
| Antoni Kramer        | 6 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert)) |
| Otto Azgorn          | 6 | ((210421-znudzona-zaloga-inferni; 210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Raoul Lavanis        | 6 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220610-ratujemy-porywaczy-eleny; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231220-bladawir-kontra-przemyt-tienow)) |
| Roland Sowiński      | 6 | ((210512-ewakuacja-z-serenit; 210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Annika Pradis        | 5 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Apollo Verlen        | 5 | ((210210-milosc-w-rodzie-verlen; 210311-studenci-u-verlenow; 230124-kjs-wygrac-za-wszelka-cene; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka)) |
| Flawia Blakenbauer   | 5 | ((210616-nieudana-infiltracja-inferni; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Kajetan Kircznik     | 5 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 231018-anomalne-awarie-athamarein)) |
| Maja Samszar         | 5 | ((210311-studenci-u-verlenow; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Olgierd Drongon      | 5 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210120-sympozjum-zniszczenia; 210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki)) |
| Adam Szarjan         | 4 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Anastazja Sowińska   | 4 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Daria Czarnewik      | 4 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Diana Arłacz         | 4 | ((201224-nieprawdopodobny-zbieg-okolicznosci; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| Diana d'Infernia     | 4 | ((210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Marcinozaur Verlen   | 4 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230502-strasznolabedz-atakuje-granice; 231220-bladawir-kontra-przemyt-tienow)) |
| Marta Keksik         | 4 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| OO Athamarein        | 4 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 231018-anomalne-awarie-athamarein)) |
| Patryk Samszar       | 4 | ((231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Tomasz Sowiński      | 4 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni)) |
| Aleksandra Termia    | 3 | ((201014-krystaliczny-gniew-elizy; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica)) |
| Antoni Bladawir      | 3 | ((231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Damian Orion         | 3 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Dariusz Krantak      | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 210127-porwanie-anastazji-z-odkupienia)) |
| Feliks Walrond       | 3 | ((210526-morderstwo-na-inferni; 231025-spiew-nielalki-na-castigatorze; 231109-komodor-bladawir-i-korona-woltaren)) |
| Janus Krzak          | 3 | ((210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210901-stabilizacja-bramy-eterycznej)) |
| Jolanta Sowińska     | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Marian Fartel        | 3 | ((200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Marian Tosen         | 3 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni)) |
| Mateus Sarpon        | 3 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss)) |
| OO Astralna Flara    | 3 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Karsztarin        | 3 | ((221004-samotna-w-programie-advancer; 221102-astralna-flara-i-porwanie-na-karsztarinie; 231220-bladawir-kontra-przemyt-tienow)) |
| OO Tivr              | 3 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos)) |
| OO Żelazko           | 3 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210922-ostatnia-akcja-bohaterki)) |
| Persefona d'Infernia | 3 | ((200708-problematyczna-elena; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit)) |
| Romeo Verlen         | 3 | ((210210-milosc-w-rodzie-verlen; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen)) |
| Szczepan Kaltaben    | 3 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss)) |
| Tadeusz Ursus        | 3 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 210428-infekcja-serenit)) |
| TAI Rzieza d'K1      | 3 | ((210630-listy-od-fanow; 211013-szara-nawalnica; 211027-rzieza-niszczy-infernie)) |
| Ula Blakenbauer      | 3 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230502-strasznolabedz-atakuje-granice)) |
| Wawrzyn Rewemis      | 3 | ((211110-romans-dzieki-esuriit; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| Władawiec Diakon     | 3 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nocna Krypta      | 2 | ((201230-pulapka-z-anastazji; 210825-uszkodzona-brama-eteryczna)) |
| AK Serenit           | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Anastazja Sowińska Dwa | 2 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia)) |
| Arnulf Perikas       | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Ataienne             | 2 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy)) |
| Bogdan Anatael       | 2 | ((210512-ewakuacja-z-serenit; 210630-listy-od-fanow)) |
| Dariusz Blakenbauer  | 2 | ((210311-studenci-u-verlenow; 210331-elena-z-rodu-verlen)) |
| Eliza Ira            | 2 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Ellarina Samarintael | 2 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor)) |
| Gilbert Bloch        | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Grażyna Burgacz      | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Hubert Kerwelenios   | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Igor Arłacz          | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Karol Reichard       | 2 | ((210526-morderstwo-na-inferni; 211110-romans-dzieki-esuriit)) |
| Kasandra Destrukcja Diakon | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Klarysa Jirnik       | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Konstanty Keksik     | 2 | ((231025-spiew-nielalki-na-castigatorze; 240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Lars Kidironus       | 2 | ((231011-ekstaflos-na-tezifeng; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Lutus Amerin         | 2 | ((211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Martyna Bianistek    | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Medea Sowińska       | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Michał Perikas       | 2 | ((210311-studenci-u-verlenow; 210324-lustrzane-odbicie-eleny)) |
| Natalia Aradin       | 2 | ((211124-prototypowa-nereida-natalii; 220105-to-nie-pulapka-na-nereide)) |
| NekroTAI Zarralea    | 2 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor)) |
| OE Falołamacz        | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| OO Castigator        | 2 | ((231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| OO Loricatus         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Netrahina         | 2 | ((200715-sabotaz-netrahiny; 210901-stabilizacja-bramy-eterycznej)) |
| OO Paprykowiec       | 2 | ((231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Salazar Bolza        | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| SCA Płetwal Błękitny | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| SP Pallida Voss      | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Szczepan Myrczek     | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Tal Marczak          | 2 | ((210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy)) |
| Tomasz Kaltaben      | 2 | ((220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Vigilus Mevilig      | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Zaara Mieralit       | 2 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Zygfryd Maus         | 2 | ((211117-porwany-trismegistos; 220126-keldan-voss-kolonia-saitaera)) |
| Ada Wyrocznik        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Adam Permin          | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Aida Serenit         | 1 | ((210512-ewakuacja-z-serenit)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Rodivas           | 1 | ((201230-pulapka-z-anastazji)) |
| AK Salamin           | 1 | ((200708-problematyczna-elena)) |
| Aleksander Leszert   | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Aleksander Samszar   | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alezja Dumorin       | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Alicja Szadawir      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Andrzej Gwozdnik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Aneta Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Anna Tessalon        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| BIA XXX d'Zona Tres  | 1 | ((210714-baza-zona-tres)) |
| Błażej Sowiński      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Brunhilda Verlen     | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Dominika Perikas     | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Eleonora Perłamila   | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Emilia Lawendowiec   | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Ewa Razalis          | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Fantazjusz Verlen    | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Feliks Przędz        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Franz Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Gabriel Lodowiec     | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Gerwazy Kircznik     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Grzegorz Chropst     | 1 | ((201224-nieprawdopodobny-zbieg-okolicznosci)) |
| Henryk Sowiński      | 1 | ((201230-pulapka-z-anastazji)) |
| Hestia d'Atropos     | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Hestia d'Neotik      | 1 | ((211208-o-krok-za-daleko)) |
| Hiacynt Samszar      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Horacy Aktenir       | 1 | ((210106-sos-z-haremu)) |
| Hubert Mirsz         | 1 | ((221004-samotna-w-programie-advancer)) |
| Jakub Bulgocz        | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Jakub Oroginiec      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Jarosław Szarjan     | 1 | ((211208-o-krok-za-daleko)) |
| Jasmina Perikas      | 1 | ((201224-nieprawdopodobny-zbieg-okolicznosci)) |
| Jolanta Kopiec       | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Julia Aktenir        | 1 | ((210106-sos-z-haremu)) |
| Julia Myrczek        | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Julian Muszel        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Kacper Wentel        | 1 | ((221004-samotna-w-programie-advancer)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Kamil Burgacz        | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Kamil Frederico      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Karol Atenuatia      | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Kaspian Certisarius  | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Kazimierz Darbik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| KDN Kajis            | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Kirea Rialirat       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Klaudiusz Terienak   | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Konrad Wolczątek     | 1 | ((200722-wielki-kosmiczny-romans)) |
| Kormonow Voss        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Kosmicjusz Tanecznik Diakon | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Krucjusz Verlen      | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Krystian Blakenbauer | 1 | ((210331-elena-z-rodu-verlen)) |
| Krzysztof Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Lara Kiriczko        | 1 | ((221004-samotna-w-programie-advancer)) |
| Leo Kasztop          | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Lidia Nemert         | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Lidia Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Llarnagraht          | 1 | ((210414-dekralotyzacja-asimear)) |
| Lothar Diakon        | 1 | ((200715-sabotaz-netrahiny)) |
| Lucas Oktromin       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Lucjusz Blakenbauer  | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Łucja Larnecjat      | 1 | ((240107-narkotyczna-pacyfikacja-tienow-na-castigatorze)) |
| Maciek Kalmarzec     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Maks Samszar         | 1 | ((230502-strasznolabedz-atakuje-granice)) |
| Malictrix d'Pandora  | 1 | ((210414-dekralotyzacja-asimear)) |
| Marianna Lemurczak   | 1 | ((200923-magiczna-burza-w-raju)) |
| Mariusz Bulterier    | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Mariusz Tubalon      | 1 | ((210414-dekralotyzacja-asimear)) |
| Marta Krissit        | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Melania Akacja       | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Michał Teriakin      | 1 | ((210630-listy-od-fanow)) |
| Michał Warkoczak     | 1 | ((221004-samotna-w-programie-advancer)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Morrigan d'Tirakal   | 1 | ((210421-znudzona-zaloga-inferni)) |
| Natalia Gwozdnik     | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Nikodem Sowiński     | 1 | ((200923-magiczna-burza-w-raju)) |
| OA Bakałarz          | 1 | ((210120-sympozjum-zniszczenia)) |
| OA Odkupienie        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OA Zguba Tytanów     | 1 | ((201230-pulapka-z-anastazji)) |
| OE Lord Savaron      | 1 | ((210630-listy-od-fanow)) |
| OE Piękna Elena      | 1 | ((210428-infekcja-serenit)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Oliwia Pietrova      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Alaya             | 1 | ((210519-osiemnascie-oczu)) |
| OO Aurelion          | 1 | ((200708-problematyczna-elena)) |
| OO Galaktyczny Tucznik | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Kastor            | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Opresor           | 1 | ((210616-nieudana-infiltracja-inferni)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Tezifeng          | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Tvarana           | 1 | ((200715-sabotaz-netrahiny)) |
| OO Welgat            | 1 | ((200722-wielki-kosmiczny-romans)) |
| OO Wesoły Wieprzek   | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Percival Diakon      | 1 | ((200715-sabotaz-netrahiny)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Przemysław Czapurt   | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Rafael Galwarn       | 1 | ((210630-listy-od-fanow)) |
| Rafał Armadion       | 1 | ((200916-smierc-raju)) |
| Rafał Grambucz       | 1 | ((210428-infekcja-serenit)) |
| Rafał Perikas        | 1 | ((210311-studenci-u-verlenow)) |
| Remigiusz Alkarenit  | 1 | ((231018-anomalne-awarie-athamarein)) |
| Remigiusz Błyszczyk  | 1 | ((211013-szara-nawalnica)) |
| Remigiusz Falorin    | 1 | ((210630-listy-od-fanow)) |
| Robert Garwen        | 1 | ((200916-smierc-raju)) |
| Rozalia Teirik       | 1 | ((210106-sos-z-haremu)) |
| Rufus Komczirp       | 1 | ((200715-sabotaz-netrahiny)) |
| Rufus Niegnat        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Rufus Samszar        | 1 | ((210311-studenci-u-verlenow)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Saitaer              | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Sandra Kantarelo     | 1 | ((221004-samotna-w-programie-advancer)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| SC Korona Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| SCA Goldarion        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Semla d'Goldarion    | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Seraf Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Serena Krissit       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| SL Uśmiechnięta      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| SP Plugawy Jaszczur  | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| SP Światło Nadziei   | 1 | ((220223-stabilizacja-keldan-voss)) |
| Suwan Chankar        | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Sylwia Perikas       | 1 | ((210311-studenci-u-verlenow)) |
| Szczepan Myksza      | 1 | ((200715-sabotaz-netrahiny)) |
| Szymon Kapeć         | 1 | ((230502-strasznolabedz-atakuje-granice)) |
| Szymon Orzesznik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Szymon Wanad         | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| TAI Eszara d'Castigator | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| TAI XT-723 d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI Zefiris          | 1 | ((210630-listy-od-fanow)) |
| Tomasz Dojnicz       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Tomasz Rewernik      | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Tymek Samszar        | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Tymon Krakdacz       | 1 | ((221004-samotna-w-programie-advancer)) |
| Tymoteusz Czerw      | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Ulisses Kalidon      | 1 | ((210714-baza-zona-tres)) |
| Uśmiechniczka Konstrukcjonistka Aurora Diakon | 1 | ((231018-anomalne-awarie-athamarein)) |
| Wanessa Pyszcz       | 1 | ((200916-smierc-raju)) |
| Wojciech Mykirło     | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Wojciech Namczak     | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Zuzanna Kraczamin    | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Żaneta Krawędź       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |