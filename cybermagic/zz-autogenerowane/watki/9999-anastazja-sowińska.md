# Anastazja Sowińska
## Identyfikator

Id: 9999-anastazja-sowińska

## Sekcja Opowieści

### Krypta i Wyjec

* **uid:** 201216-krypta-i-wyjec, _numer względny_: 9
* **daty:** 0111-03-29 - 0111-03-31
* **obecni:** AK Nocna Krypta, AK Wyjec, Anastazja Sowińska, Arianna Verlen, Diana Arłacz, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser

Streszczenie:

Podczas walki Nocnej Krypty i Wyjca Zespół wbił się na Nocną Kryptę, by wyleczyć się z Różowej Plagi. Na miejscu okazało się, że Krypta ma "nowe programowanie" (leczenie lub 'zachowanie'), nowe mechanizmy (MIRV, anomalne działo, anioły) oraz że jest groźniejsza niż kiedykolwiek. Arianna zostawiła na pokładzie Krypty Anastazję, by Krypta naprawiła jej Wzór, po czym oszukała Sowińskich, że niby Krypta porwała Anastazję.

Aktor w Opowieści:

* Dokonanie:
    * podatna na moc mentalną Krypty, została na pokładzie celem Krypty naprawy Wzoru. Tchórzliwa, ale ufa Ariannie i Zespołowi.
* Progresja:
    * została na Nocnej Krypcie do naprawy Wzoru; nie wie nawet o tym, co się z nią dzieje.


### Pocałunek Aspirii

* **uid:** 201210-pocalunek-aspirii, _numer względny_: 8
* **daty:** 0111-03-26 - 0111-03-29
* **obecni:** AK Nocna Krypta, AK Wyjec, Anastazja Sowińska, Arianna Verlen, Donald Parziarz, Eustachy Korkoran, Juliusz Sowiński, Katra Igneus, Klaudia Stryk, OA Zguba Tytanów, OO Infernia

Streszczenie:

Juliusz Sowiński zażądał oddania Anastazji, ale ona nie chce wracać - jej reputacja i stan są w ruinie. Anastazja chce być jak Elena, ku zgryzocie Juliusza. Tymczasem Arianna zastawiła pułapkę na koloidowy statek - "Pocałunek Aspirii" - i go zestrzeliła. Po czym wezwała Nocną Kryptę by zdjąć z nich klątwę miłości i naprawić co się da z "Pocałunku" i AK Wyjec.

Aktor w Opowieści:

* Dokonanie:
    * załamana tym, że po ostatnim zrobiła pośmiewisko swojemu rodowi. Zakochana w Eustachym. Chce wstąpić do wojska i postawiła się Juliuszowi.


### Anastazja - bohaterką?

* **uid:** 201118-anastazja-bohaterka, _numer względny_: 7
* **daty:** 0111-03-22 - 0111-03-25
* **obecni:** Anastazja Sowińska, Arianna Verlen, Diana Arłacz, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, Stefan Jamniczek

Streszczenie:

Anastazja nie została bohaterką. Wpierw Eustachy wplątał się w pojedynek o Dianę z jakimś szlachcicem - skończyło się różową mgiełką miłości i pospieszną ewakuacją Inferni z Kontrolera Pierwszego, potem epicki plan Izy z piratami rozpadł się, bo na Infernię polował jakiś statek koloidowy. Ale przetrwali - i mają zamiar to wszystko rozplątać.

Aktor w Opowieści:

* Dokonanie:
    * miała tylko patronować pojedynkowi Eustachy - Jamniczek, ale się zakochała w Eustachym i skompromitowała publicznie na K1.


### Sabotaż świni

* **uid:** 201104-sabotaz-swini, _numer względny_: 6
* **daty:** 0111-03-13 - 0111-03-16
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Diana Arłacz, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, Rafał Armadion, Robert Garwen

Streszczenie:

Sabotażysta - miragent - podjął próbę zniszczenia Arianny gdy ta ładowała świnie na Wesołego Wieprzka używając wybuchowej, anomalnej świni. Zespół przetrwał, choć "Wieprzek" został zniszczony. Arianna pojmała miragenta i przekazała go do Kontrolera Pierwszego, acz kosztem ofiar w ludziach. No i nie było dość świń by je wziąć na orbitę XD.

Aktor w Opowieści:

* Dokonanie:
    * aresztowała miragenta... przez co zginął astorianin pilnujący aresztanta. Trzeci Raj nie jest pod jej pozytywnym wrażeniem.
* Progresja:
    * opinia w Trzecim Raju: plan Arianny był dobry, ale Anastazja źle go wykonała. Anastazja ma w Raju złą opinię jako niekompetentna arystokratka Aurum.


### Noktianie rodu Arłacz

* **uid:** 201021-noktianie-rodu-arlacz, _numer względny_: 5
* **daty:** 0111-03-07 - 0111-03-10
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Diana Arłacz, Eliza Ira, Eustachy Korkoran, Izabela Zarantel, Jolanta Arłacz, Juliusz Sowiński, Klaudia Stryk, Klaus Rumak, Maria Gołąb, OO Szalony Rumak, OO Wesoły Wieprzek, Robert Arłacz, Wanessa Pyszcz

Streszczenie:

By uzyskać współpracę Elizy Iry, Arianna zdecydowała się "porwać" noktian rodu Arłacz wykorzystywanych do produkcji czystej krwi dla magów. Z uwagi na działania Marii Gołąb plan został odrzucony i wykryty, ale znalezienie haków na Arłaczy rozwiązało ten problem - Arianna i Arłacze współpracują. Nie doszło do porozumienia z Elizą Irą, ale Eliza się wycofała z Trzeciego Raju, ogłaszając się protektorką tego terenu. Sytuacja w miarę ustabilizowana.

Aktor w Opowieści:

* Dokonanie:
    * Arianna wytrąciła ją z pasywności; wreszcie zaczęła działać jak Sowińska. Wpierw kazała "Szalonemu Rumakowi" się dostosować, potem pięknie zażądała Diany. Będzie z niej wartościowa arystokratka Aurum. Musiała się "pogodzić" z Nikodemem przez Marię Gołąb, czego NIE CIERPI.
* Progresja:
    * publiczne (na wizji) potwierdził, że Nikodem i ona współpracowali by znaleźć zdrajcę. Nie może nic już zrobić Nikodemowi przez próbę ratowania noktian.
    * dostała silny cios reputacyjny - stoi po stronie Elizy Iry przeciwko lojalistom Aurum. Kolaboruje z Elizą! Ale - bonus wśród sympatyków noktian.


### Krystaliczny gniew Elizy

* **uid:** 201014-krystaliczny-gniew-elizy, _numer względny_: 4
* **daty:** 0111-03-02 - 0111-03-05
* **obecni:** Aleksandra Termia, Anastazja Sowińska, Arianna Verlen, Ataienne, Dariusz Krantak, Elena Verlen, Eliza Ira, Eustachy Korkoran, Klaudia Stryk, Marian Fartel, OO Wesoły Wieprzek

Streszczenie:

Pomoc humanitarna przybyła do Trzeciego Raju wraz z zabójcami Anastazji. Mindwarpowani komandosi noktiańscy prawie zabili Elenę, ale Anastazja została ochroniona. Do akcji weszła Eliza Ira - zażądała oddania jej Trzeciego Raju i noktian. Zespół przygotowuje się do nie-oddania Raju i uratowania jak najwięcej noktian z Aurum (jakkolwiek to nie brzmi).

Aktor w Opowieści:

* Dokonanie:
    * Elena poświęciła sporo zdrowia, by ją uratować przed zabójcami jej przyjaciela, ochroniarza. Sama jest ranna. Jej świat się rozsypał.
* Progresja:
    * jej świat się rozsypał. Tylko dziadkowi na niej zależy (chyba), Elena chroniąc ją prawie umarła i po raz pierwszy ever została RANNA. ZABÓJCY!
    * już nie ma traumy "nie może zostać sama". Wyleczyła się. Zamknęła się w sobie.


### Magiczna burza w Raju

* **uid:** 200923-magiczna-burza-w-raju, _numer względny_: 3
* **daty:** 0111-02-21 - 0111-02-25
* **obecni:** Anastazja Sowińska, Arianna Verlen, Dariusz Krantak, Elena Verlen, Eliza Ira, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Marian Fartel, Marianna Lemurczak, Nikodem Sowiński

Streszczenie:

Zbliża się burza magiczna. Nie ma schronienia ani zapasów. Zespół wysłał SOS na Kontroler i do Nikodema Sowińskiego (Anastazja). Zbudowali co się da by odeprzeć burzę magiczną i im się to udało bez strat w ludziach. O dziwo, siła która im pomogła to Eliza Ira (acz dyskretnie). Anastazja wierzy że Nikodem jest jej najlepszym przyjacielem, ale on pragnie jej zguby...

Aktor w Opowieści:

* Dokonanie:
    * zaczyna ufać Elenie, która traktuje ją twardo ale wie, przez co Anastazja przechodzi. Nawet powoli zaczyna pomagać. Wysłała SOS dla Raju do Nikodema z prośbą o wsparcie.
* Progresja:
    * zaakceptowała Elenę jako opiekunkę i strażniczkę. Przekonana, że Arianna chce ją wykorzystać, ale ELENIE na niej zależy. Taka "twarda sympatia".


### Śmierć Raju

* **uid:** 200916-smierc-raju, _numer względny_: 2
* **daty:** 0111-02-18 - 0111-02-20
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Celina Szilat, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Marian Fartel, Rafał Armadion, Robert Garwen, Wanessa Pyszcz

Streszczenie:

Najważniejszym problemem Raju jest pozbycie się bombardowania nojrepów. Arianna zaproponowała zuchwały pomysł - w kontrolowany sposób niech nojrepy wejdą do miasteczka i wezmą czego szukają. Udało jej się wszystkich przekonać. Sukces - nojrepy się wycofały, ale Tucznik nie jest zdolny do latania a Trzeci Raj jest zniszczony. Została tylko Ataienne i ruina. Ataienne rozpaliła hipnotycznie morale trzeciorajowców - przetrwają to wszystko.

Aktor w Opowieści:

* Dokonanie:
    * w bardzo trudnej sytuacji - jej bohaterowie nie są tacy jak myślała, nie ma ubrań i stroju i nie kontroluje energii magicznych. Oddana w opiekę Elenie.


### Arystokratka w ładowni na świnie

* **uid:** 200909-arystokratka-w-ladowni-na-swinie, _numer względny_: 1
* **daty:** 0111-02-12 - 0111-02-17
* **obecni:** Anastazja Sowińska, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, OO Galaktyczny Tucznik

Streszczenie:

Zespół wyciągnął Martyna z więzienia (legalnie). Wpakowali się na Galaktycznego Tucznika by przenieść świnie na orbitę... ale poleciała z nimi dziennikarka Izabela (o czym wiedzą) i 15letnia Anastazja Sowińska (o czym nie wiedzą). Przy lądowaniu, atak nojrepów na Tucznika spowodował katastrofę - seria Paradoksów Anastazji, uszkodzenia Tucznika itp. Zespół uratował Anastazję (choć kosztem ran Martyna) i wylądował Tucznikiem na Astorii. To jest sukces.

Aktor w Opowieści:

* Dokonanie:
    * 15-letnia wysoka szlachcianka Aurum o przypadłości nienaturalnie silnych Paradoksów; schowała się w ładowni Tucznika i jej Paradoksy stworzyły tam krainę terroru. Uratowana przez załogę Tucznika (Inferni) z niekończącego się koszmaru.
* Progresja:
    * Paradoksalnie splątana z Arianną Verlen; nie może się od niej oddalić > 1 km. Zdaniem wielu, Arianna splątała się specjalnie z Anastazją.
    * śmiertelnie i na całe życie i w ogóle zakochana w Martynie Hiwasserze, jej RYCERZU.
    * boi się zostać sama, bo Paradoksalny Koszmar wróci...


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 9, @: 0111-03-31
    1. Primus    : 9, @: 0111-03-31
        1. Sektor Astoriański    : 9, @: 0111-03-31
            1. Astoria, Orbita    : 2, @: 0111-02-20
                1. Kontroler Pierwszy    : 2, @: 0111-02-20
            1. Astoria    : 6, @: 0111-03-16
                1. Sojusz Letejski, NW    : 6, @: 0111-03-16
                    1. Ruiniec    : 6, @: 0111-03-16
                        1. Ortus-Conticium    : 1, @: 0111-02-25
                        1. Pustynia Kryształowa    : 1, @: 0111-03-10
                        1. Trzeci Raj, okolice    : 2, @: 0111-02-20
                            1. Kopiec Nojrepów    : 2, @: 0111-02-20
                            1. Zdrowa Ziemia    : 1, @: 0111-02-20
                        1. Trzeci Raj    : 5, @: 0111-03-16
                            1. Barbakan    : 1, @: 0111-02-20
                            1. Centrala Ataienne    : 1, @: 0111-02-20
                            1. Mały Kosmoport    : 1, @: 0111-02-20
                            1. Ratusz    : 1, @: 0111-02-20
                            1. Stacja Nadawcza    : 1, @: 0111-02-20
                1. Sojusz Letejski    : 1, @: 0111-03-10
                    1. Aurum    : 1, @: 0111-03-10
                        1. Powiat Niskowzgórza    : 1, @: 0111-03-10
                            1. Domena Arłacz    : 1, @: 0111-03-10
                                1. Posiadłość Arłacz    : 1, @: 0111-03-10
                                    1. Baterie Puryfikacji Krwi    : 1, @: 0111-03-10
                                    1. Małe lądowisko    : 1, @: 0111-03-10
            1. Neikatis    : 1, @: 0111-03-29
                1. Dystrykt Quintal    : 1, @: 0111-03-29
                    1. Arkologia Aspiria    : 1, @: 0111-03-29

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 9 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Eustachy Korkoran    | 9 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Klaudia Stryk        | 9 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Ataienne             | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Diana Arłacz         | 4 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec)) |
| Elena Verlen         | 4 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Izabela Zarantel     | 4 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz)) |
| Martyn Hiwasser      | 4 | ((200909-arystokratka-w-ladowni-na-swinie; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec)) |
| Eliza Ira            | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Kamil Lyraczek       | 3 | ((200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec)) |
| Marian Fartel        | 3 | ((200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| AK Nocna Krypta      | 2 | ((201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| AK Wyjec             | 2 | ((201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Dariusz Krantak      | 2 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Juliusz Sowiński     | 2 | ((201021-noktianie-rodu-arlacz; 201210-pocalunek-aspirii)) |
| OO Wesoły Wieprzek   | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Rafał Armadion       | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Robert Garwen        | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Wanessa Pyszcz       | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Aleksandra Termia    | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Donald Parziarz      | 1 | ((201210-pocalunek-aspirii)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Katra Igneus         | 1 | ((201210-pocalunek-aspirii)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Marianna Lemurczak   | 1 | ((200923-magiczna-burza-w-raju)) |
| Nikodem Sowiński     | 1 | ((200923-magiczna-burza-w-raju)) |
| OA Zguba Tytanów     | 1 | ((201210-pocalunek-aspirii)) |
| OO Galaktyczny Tucznik | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| OO Infernia          | 1 | ((201210-pocalunek-aspirii)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Stefan Jamniczek     | 1 | ((201118-anastazja-bohaterka)) |