# Keira Amarco d'Namertel
## Identyfikator

Id: 9999-keira-amarco-d'namertel

## Sekcja Opowieści

### Lea, strażniczka lasu

* **uid:** 240305-lea-strazniczka-lasu, _numer względny_: 5
* **daty:** 0111-10-21 - 0111-10-25
* **obecni:** Arkadia Verlen, Bogdan Gwiazdocisz, Ernest Namertel, Karolina Terienak, Keira Amarco d'Namertel, Lea Samszar, Malena Barandis, Marek Samszar, Mariusz Kupieczka, Marysia Sowińska, Michał Klabacz, Triana Porzecznik

Streszczenie:

Marysia zatrzymała Leę przed naruszaniem kulturowego tabu eternijskiego (stroju) i przy okazji dowiedziała się, że Lea próbuje pomóc innym, ale nikt nie może o tym wiedzieć. Tymczasem jej EfemeHorror poranił grupę Rekinów i AMZ współpracujących ze sobą w celu pomocy staremu 'archeologowi' szukającemu prawdy o ofiarach wojny.

Aktor w Opowieści:

* Dokonanie:
    * bardzo niezadowolona z uwagi na to, że Lea łamie zasady strojów eternijskich (tabu). Marysia ją uspokaja, zajmując się tą sprawą.


### Płaszcz ochronny Mimozy

* **uid:** 220222-plaszcz-ochronny-mimozy, _numer względny_: 4
* **daty:** 0111-09-21 - 0111-09-25
* **obecni:** Ernest Namertel, Karolina Terienak, Keira Amarco d'Namertel, Lorena Gwozdnik, Marysia Sowińska, Mimoza Elegancja Diakon

Streszczenie:

Mimoza weszła do akcji - wzięła Lorenę do azylu i ochroniła część kultystek przed Ernestem. Konflikt Ernest - Mimoza się rozpalił, Marysia + Karo poszły się z Mimozą spotkać by zdeeskalować problem. Lorena podsłuchiwała, więc Mimoza straciła twarz. Cóż. Mimoza wynegocjowała przekazanie kultystek Sensacjuszowi, Marysia wynegocjowała by Lorena zrobiła ten akt. ALE W DOMU MIMOZY. Zaczyna się ciekawie...

Aktor w Opowieści:

* Dokonanie:
    * jednostka szturmowa Ernesta, odbiła się od apartamentu Mimozy. Nie umie jej zinfiltrować.


### Marysiowa Hestia Rekinów

* **uid:** 220111-marysiowa-hestia-rekinow, _numer względny_: 3
* **daty:** 0111-09-16 - 0111-09-19
* **obecni:** Diana Tevalier, Ernest Namertel, Hestia d'Rekiny, Jeremi Sowiński, Karolina Terienak, Keira Amarco d'Namertel, Liliana Bankierz, Marysia Sowińska

Streszczenie:

Konflikt Liliana - Marysia się zaostrza. Marysia skupia się na wzmocnieniu Hestii i uzyskaniu kontroli nad Rekinami - przejęła Hestię jako sojuszniczkę. Chevaleresse poprosiła Karolinę o uratowanie Melissy i Karo faktycznie pomogła - Ernest wysłał Keirę i odbili Melissę. To sprawiło, że Mimoza Diakon poczuła się zagrożona i weszła do akcji. Jakby tego było mało, Sowińscy wysyłają Jeremiego by opanował kuzynkę.

Aktor w Opowieści:

* Dokonanie:
    * podczas "ćwiczeń" zaplanowanych przez Ernesta by wyciągnąć "przypadkiem" Mimozę wpadła, załatwiła kilku magów, pokonała feromony kralotyczne i wyciągnęła Melissę.
* Progresja:
    * Arkadia Verlen wraca zrobić z nią porządek. Ernest zbyt się szarogęsi.


### Akt, o którym Marysia nie wie

* **uid:** 211228-akt-o-ktorym-marysia-nie-wie, _numer względny_: 2
* **daty:** 0111-09-09 - 0111-09-12
* **obecni:** Daniel Terienak, Ernest Namertel, Karolina Terienak, Keira Amarco d'Namertel, Liliana Bankierz, Lorena Gwozdnik, Marysia Sowińska, Napoleon Bankierz, Rafał Torszecki

Streszczenie:

Torszecki ostrzegł Marysię, że jej akt krąży w Zaczęstwie. Ona to olała. Ale jak Ernest zaczął na niego polować to Marysia się zainteresowała. Zdecydowała się aktem odbić Ernesta Amelii. Zaflirtowała z Ernestem, obiecała mu akt specjalnie dla niego i że odda mu oryginał tego. Karo pokonała Napoleona, znalazła link do aktu który ostatecznie zdobył Torszecki. Daniel dla Karo zbił Torszeckiego i odzyskał akt. Marysia zaflirtowała z Ernestem ;-). Torszecki, który chciał dobrze i chronił Marysię skończył w szpitalu.

Aktor w Opowieści:

* Dokonanie:
    * zabójczyni wysłana w roli infiltratorki, by pozyskać akt Marysi. Jest dobra, ale nie tak dobra jak Torszecki, który był szybszy. No i nie zna terenu który infiltruje.


### Wysadzony żywy ścigacz

* **uid:** 210928-wysadzony-zywy-scigacz, _numer względny_: 1
* **daty:** 0111-07-26 - 0111-07-27
* **obecni:** Azalia Sernat d'Namertel, Ernest Namertel, Franek Bulterier, Karolina Terienak, Keira Amarco d'Namertel, Marysia Sowińska, Rafał Torszecki, Żorż d'Namertel

Streszczenie:

Ernest z Eterni okazał się być zupełnie inny niż się wydaje. Lekko naiwny, z paladyńskim zacięciem, chce czynić dobro i integrować Rekiny dookoła Marysi. Chce dokończyć dobre dzieło Amelii. Marysia znalazła z nim linię porozumienia a Ernest x Karolina są jak dwa łyse konie. Marysia przekonała go, by sam nic nie robił - nie rozumie terenu. Niestety, ktoś wysadził żywy ścigacz i zabił ducha z jego arkinu. Ernesta zatrzymała Marysia. Karolinie nie udało się ducha uratować. Dziewczyny zdecydowały, że znajdą mordercę...

Aktor w Opowieści:

* Dokonanie:
    * bardzo lekko ubrana (eufemizm) zabójczyni Ernesta. Mało mówi. Brunetka. W jakiś sposób ma dostęp do kinezy (przemieszczanie się, akceleracja) i do ostrza eterycznego. Ale zabójca powinien być człowiekiem a nie magiem.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0111-10-25
    1. Primus    : 5, @: 0111-10-25
        1. Sektor Astoriański    : 5, @: 0111-10-25
            1. Astoria    : 5, @: 0111-10-25
                1. Sojusz Letejski    : 5, @: 0111-10-25
                    1. Szczeliniec    : 5, @: 0111-10-25
                        1. Powiat Pustogorski    : 5, @: 0111-10-25
                            1. Podwiert    : 5, @: 0111-10-25
                                1. Dzielnica Luksusu Rekinów    : 5, @: 0111-10-25
                                    1. Fortyfikacje Rolanda    : 2, @: 0111-09-25
                                    1. Obrzeża Biedy    : 2, @: 0111-09-25
                                        1. Domy Ubóstwa    : 2, @: 0111-09-25
                                        1. Hotel Milord    : 2, @: 0111-09-25
                                        1. Stadion Lotników    : 2, @: 0111-09-25
                                        1. Stajnia Rumaków    : 2, @: 0111-09-25
                                    1. Sektor Brudu i Nudy    : 2, @: 0111-09-25
                                        1. Komputerownia    : 2, @: 0111-09-25
                                        1. Konwerter Magielektryczny    : 2, @: 0111-09-25
                                        1. Magitrownia Pogardy    : 2, @: 0111-09-25
                                        1. Skrytki Czereśniaka    : 2, @: 0111-09-25
                                    1. Serce Luksusu    : 4, @: 0111-09-25
                                        1. Apartamentowce Elity    : 4, @: 0111-09-25
                                        1. Arena Amelii    : 2, @: 0111-09-25
                                        1. Fontanna Królewska    : 2, @: 0111-09-25
                                        1. Kawiarenka Relaks    : 2, @: 0111-09-25
                                        1. Lecznica Rannej Rybki    : 3, @: 0111-09-25
                                1. Las Trzęsawny    : 2, @: 0111-10-25
                            1. Zaczęstwo    : 3, @: 0111-09-25
                                1. Akademia Magii, kampus    : 3, @: 0111-09-25
                                    1. Akademik    : 3, @: 0111-09-25
                                    1. Arena Treningowa    : 3, @: 0111-09-25
                                    1. Las Trzęsawny    : 3, @: 0111-09-25

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ernest Namertel      | 5 | ((210928-wysadzony-zywy-scigacz; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 240305-lea-strazniczka-lasu)) |
| Karolina Terienak    | 5 | ((210928-wysadzony-zywy-scigacz; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 240305-lea-strazniczka-lasu)) |
| Marysia Sowińska     | 5 | ((210928-wysadzony-zywy-scigacz; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 240305-lea-strazniczka-lasu)) |
| Liliana Bankierz     | 2 | ((211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow)) |
| Lorena Gwozdnik      | 2 | ((211228-akt-o-ktorym-marysia-nie-wie; 220222-plaszcz-ochronny-mimozy)) |
| Rafał Torszecki      | 2 | ((210928-wysadzony-zywy-scigacz; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Arkadia Verlen       | 1 | ((240305-lea-strazniczka-lasu)) |
| Azalia Sernat d'Namertel | 1 | ((210928-wysadzony-zywy-scigacz)) |
| Bogdan Gwiazdocisz   | 1 | ((240305-lea-strazniczka-lasu)) |
| Daniel Terienak      | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Diana Tevalier       | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Franek Bulterier     | 1 | ((210928-wysadzony-zywy-scigacz)) |
| Hestia d'Rekiny      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Jeremi Sowiński      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Lea Samszar          | 1 | ((240305-lea-strazniczka-lasu)) |
| Malena Barandis      | 1 | ((240305-lea-strazniczka-lasu)) |
| Marek Samszar        | 1 | ((240305-lea-strazniczka-lasu)) |
| Mariusz Kupieczka    | 1 | ((240305-lea-strazniczka-lasu)) |
| Michał Klabacz       | 1 | ((240305-lea-strazniczka-lasu)) |
| Mimoza Elegancja Diakon | 1 | ((220222-plaszcz-ochronny-mimozy)) |
| Napoleon Bankierz    | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Triana Porzecznik    | 1 | ((240305-lea-strazniczka-lasu)) |
| Żorż d'Namertel      | 1 | ((210928-wysadzony-zywy-scigacz)) |