# Amanda Kajrat
## Identyfikator

Id: 9999-amanda-kajrat

## Sekcja Opowieści

### Wygrany kontrakt

* **uid:** 200311-wygrany-kontrakt, _numer względny_: 5
* **daty:** 0110-07-16 - 0110-07-19
* **obecni:** Amanda Kajrat, Dagmara Doberman, Ernest Kajrat, Liliana Bankierz, Lucjusz Blakenbauer, Minerwa Metalia, Ziemowit Zięba

Streszczenie:

Ernest Kajrat jest w stanie śmiertelnym; Lucjusz i Minerwa trzymają go przy życiu. Tymczasem firma dostarczająca jedzenie "Zygmunt Zając" wygrała przetarg (na który nie startowała) odnośnie jedzenia dla zixionizowanej Amandy Kajrat w rękach Grzymościa. Współpraca Blakenbauera i "Zająca" doprowadza do uwolnienia Amandy i kupienia szansy Ernestowi...

Aktor w Opowieści:

* Dokonanie:
    * Skażona energiami ixiońskimi przez Saitaera, wpadła w ręce Grzymościa, acz poniszczyła mu mafiozów solidnie


### Niespodziewany wpływ Aidy

* **uid:** 190804-niespodziewany-wplyw-aidy, _numer względny_: 4
* **daty:** 0110-06-26 - 0110-06-28
* **obecni:** Amanda Kajrat, Gabriel Ursus, Julia Morwisz, Ossidia Saitis, Pięknotka Diakon, Saitaer

Streszczenie:

Po złapaniu Aidy przez Hralwagha z Cieniaszczytu okazało się, że Aida wpłynęła na samego Hralwagha. Potwór splugawił swoich kontrolerów z Cieniaszczytu. Pięknotka współpracując z Ossidią i Saitaerem pokonała Hralwagha zanim komukolwiek stała się krzywda. Dodatkowo - Ossidia porwała Amandę Kajrat dla Saitaera; Ossidia dalej poluje na Ernesta Kajrata.

Aktor w Opowieści:

* Dokonanie:
    * dostarczyła Pięknotce dowodów odnośnie narkotyku Grzymościa by zredukować gorącą wojnę. Niestety, wpadła w ręce Ossidii i Saitaera - ma być przynętą na Kajrata.


### Bardzo niebezpieczne składowisko

* **uid:** 190726-bardzo-niebezpieczne-skladowisko, _numer względny_: 3
* **daty:** 0110-06-22 - 0110-06-24
* **obecni:** Amanda Kajrat, Cezary Alentik, Ernest Kajrat, Gabriel Ursus, Mirela Orion, Pięknotka Diakon, Roland Grzymość

Streszczenie:

Na Składowisku Odpadów Niebezpiecznych przy Podwiercie siły Wolnego Uśmiechu zaczęły składać jakieś narkotyki. Nie podoba się to Błękitnemu Niebu. Pięknotka w środku tego wszystkiego - udało jej się poprosić Orbiter o zajęcie się linią dostawczą z Cieniaszczytu, Kajrata by chwilowo zwolnił, Pustogor by chronili właściciela a Grzymościa, by zszedł do podziemia. Polityczny sukces - ale czemu Grzymość idzie w jakieś narkotyki? I czemu Kajrat dąży do wojny z Grzymościem o te narkotyki?

Aktor w Opowieści:

* Dokonanie:
    * skłonna do skrzywdzenia grupy terminusów-rekrutów by rozpalić wojnę Pustogor - Wolny Uśmiech. Nie chce wojny. Nie walczyła z Pięknotką, trochę podpowiedziała. Świetnie się chowa w stealth suit.


### Kult, choroba Esuriit

* **uid:** 190714-kult-choroba-esuriit, _numer względny_: 2
* **daty:** 0110-06-03 - 0110-06-05
* **obecni:** Amanda Kajrat, Ernest Kajrat, Pięknotka Diakon, Tomasz Tukan

Streszczenie:

Tukan poluje dalej na temat somnibela i znalazł kult wywołany przez magię. Pięknotka zdobyła mu uprawnienia a on spieprzył - dał się zainfekować Esuriit i ruszył na wojnę z Kajratem. Pięknotka uratowała mu tyłek za co dostała wsparcie Amandy Kajrat. Z jej pomocą zniszczyła kult w Podwiercie, ale dużo wskazuje że w Czółenku (gdzie indziej...) jest ulubiony ciąg dalszy.

Aktor w Opowieści:

* Dokonanie:
    * traktowana przez Kajrata jako córka, chce uratować "ojca". Świetnie wyczuwa Esuriit; wskazała Pięknotce Czółenko jako źródło problemów.


### Glizda, która leczy

* **uid:** 211120-glizda-ktora-leczy, _numer względny_: 1
* **daty:** 0108-04-03 - 0108-04-14
* **obecni:** Amanda Kajrat, Amelia Sowińska, Ernest Kajrat, Feliks Keksik, Justynian Diakon, Kacper Bankierz, Karol Pustak, Oliwia Lemurczak, Roland Sowiński, Sabina Kazitan, Sensacjusz Diakon, Stella Sowińska

Streszczenie:

Sensacjusz Diakon został wysłany na prowincję pod przykrywką Lekarza Rekinów, by dojść ze Stellą do tego czemu Amelia i Roland Sowińscy zachowują się nietypowo. Na miejscu okazało się, że Roland jest w stanie ostrej wojny z mafią (która nie traktuje tego poważnie; Kajrat się bawi z Rekinami) a Amelia po prostu nie w pełni sobie radzi i próbuje opanować sytuację. Roland jest Skażony Esuriit; Sensacjusz przetransportował go glizdą do Aurum i sam, uwięziony w bojowej formie, deeskalował konflikt z mafią. A Amelia jest tymczasowym dowódcą Rekinów...

Aktor w Opowieści:

* Dokonanie:
    * przekradła się koło czterech Lancerów sterowanych przez Elainkę i zestrzeliła Rolanda Sowińskiego jak przemawiał na podwyższeniu, zmieniając go w klauna. Zwykłe ćwiczenia.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0110-07-19
    1. Primus    : 5, @: 0110-07-19
        1. Sektor Astoriański    : 5, @: 0110-07-19
            1. Astoria    : 5, @: 0110-07-19
                1. Sojusz Letejski    : 5, @: 0110-07-19
                    1. Szczeliniec    : 5, @: 0110-07-19
                        1. Powiat Pustogorski    : 5, @: 0110-07-19
                            1. Czółenko    : 1, @: 0110-06-05
                            1. Podwiert, okolice    : 2, @: 0110-06-28
                                1. Bioskładowisko podziemne    : 2, @: 0110-06-28
                            1. Podwiert    : 3, @: 0110-06-28
                                1. Dzielnica Luksusu Rekinów    : 1, @: 0108-04-14
                                    1. Serce Luksusu    : 1, @: 0108-04-14
                                        1. Lecznica Rannej Rybki    : 1, @: 0108-04-14
                                1. Las Trzęsawny    : 1, @: 0108-04-14
                                1. Magazyny sprzętu ciężkiego    : 1, @: 0110-06-28
                                1. Osiedle Leszczynowe    : 1, @: 0110-06-05
                                    1. Szkoła Nowa    : 1, @: 0110-06-05
                            1. Pustogor, okolice    : 1, @: 0110-07-19
                                1. Rezydencja Blakenbauerów    : 1, @: 0110-07-19
                            1. Zaczęstwo    : 1, @: 0110-07-19
                                1. Nieużytki Staszka    : 1, @: 0110-07-19

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ernest Kajrat        | 4 | ((190714-kult-choroba-esuriit; 190726-bardzo-niebezpieczne-skladowisko; 200311-wygrany-kontrakt; 211120-glizda-ktora-leczy)) |
| Pięknotka Diakon     | 3 | ((190714-kult-choroba-esuriit; 190726-bardzo-niebezpieczne-skladowisko; 190804-niespodziewany-wplyw-aidy)) |
| Gabriel Ursus        | 2 | ((190726-bardzo-niebezpieczne-skladowisko; 190804-niespodziewany-wplyw-aidy)) |
| Amelia Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Cezary Alentik       | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Dagmara Doberman     | 1 | ((200311-wygrany-kontrakt)) |
| Feliks Keksik        | 1 | ((211120-glizda-ktora-leczy)) |
| Julia Morwisz        | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Justynian Diakon     | 1 | ((211120-glizda-ktora-leczy)) |
| Kacper Bankierz      | 1 | ((211120-glizda-ktora-leczy)) |
| Karol Pustak         | 1 | ((211120-glizda-ktora-leczy)) |
| Liliana Bankierz     | 1 | ((200311-wygrany-kontrakt)) |
| Lucjusz Blakenbauer  | 1 | ((200311-wygrany-kontrakt)) |
| Minerwa Metalia      | 1 | ((200311-wygrany-kontrakt)) |
| Mirela Orion         | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Oliwia Lemurczak     | 1 | ((211120-glizda-ktora-leczy)) |
| Ossidia Saitis       | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Roland Grzymość      | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Roland Sowiński      | 1 | ((211120-glizda-ktora-leczy)) |
| Sabina Kazitan       | 1 | ((211120-glizda-ktora-leczy)) |
| Saitaer              | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Sensacjusz Diakon    | 1 | ((211120-glizda-ktora-leczy)) |
| Stella Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Tomasz Tukan         | 1 | ((190714-kult-choroba-esuriit)) |
| Ziemowit Zięba       | 1 | ((200311-wygrany-kontrakt)) |