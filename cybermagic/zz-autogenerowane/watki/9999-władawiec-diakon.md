# Władawiec Diakon
## Identyfikator

Id: 9999-władawiec-diakon

## Sekcja Opowieści

### Gdy HR reedukuje niewłaściwą osobę

* **uid:** 230111-gdy-hr-reedukuje-niewlasciwa-osobe, _numer względny_: 8
* **daty:** 0100-09-18 - 0100-09-21
* **obecni:** Adragain Ferrias, AK Nox Ignis, Aleksy Sartaran, Arianna Verlen, Daria Czarnewik, Elena Verlen, Grażyna Burgacz, Kajetan Kircznik, Lana Mirkinin, Leszek Kurzmin, Maja Samszar, Miłosz Klinek, OO Astralna Flara, OO Athamarein, OO Loricatus, Persefona d'Loricatus, Sabrina Ferrias, Salazar Bolza, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Niestety, Kircznik (lekarz) przestraszył Sargona i młodych infiltratorów Lodowcem, Bolzą i ogólnie duchami noktiańskimi. Okazało się, że Grażyna jest ekspertem od duchów i jest w stanie zrobić egzorcyzm. Niedługo potem (i chwilę przed egzorcyzmem) pojawiła się szybka korweta z Biur HR i poprosili o pomoc w odparciu Anomalii Kosmicznej. Myrczek sprawdził jej pamięć i Athamarein i Flara poleciały na starcie z Nox Ignis. Nawiązały kontakt z Biurami HR, przechwyciły biednego męża pilotki Nox Ignis i poznały prawdę o Destructor Animarum i wpływie Syndykatu Aureliona na Biura HR. Kurzmin pobił rekordy Athamarein, Arianna skutecznie zaplanowała wypalenie pilotki Nox Ignis przez własny statek. Tylko advancer Miłosz (badający ślady Nox Ignis) jest MIA.

Aktor w Opowieści:

* Dokonanie:
    * osoba uspokajająca Lanę, by dało się jej przeczytać pamięć i poznać prawdę odnośnie dziwnej anomalicznej jednostki (Nox Ignis)


### Astralna Flara i nowy komodor

* **uid:** 221221-astralna-flara-i-nowy-komodor, _numer względny_: 7
* **daty:** 0100-09-12 - 0100-09-15
* **obecni:** Arianna Verlen, Daria Czarnewik, Elena Verlen, Ellarina Samarintael, Kajetan Kircznik, Kaspian Certisarius, Leszek Kurzmin, Maja Samszar, NekroTAI Zarralea, OO Astralna Flara, OO Athamarein, OO Loricatus, Salazar Bolza, Sargon Niiris, Władawiec Diakon

Streszczenie:

Po "uratowaniu" noktian z Dominy Lucis Arianna dostała medal a Lodowca zastąpił komodor Bolza, zimny drakolita. Gdy grupa wydzielona wróciła na teren Anomalii Kolapsu okazało się, że planetoida Kazmirian ma squatterów; Zespół skutecznie ich przejął odzyskując kontrolę nad systemami Semli d'Kazmirian. Na miejscu - TKO-4271 - okazało się że Zarralea jest zniszczona. Orbiter odzyskał swoją bazę, acz Loricatis musi współdzielić Persefonę. Z innej beczki: link Elena - Władawiec się pięknie rozwija.

Aktor w Opowieści:

* Dokonanie:
    * przekonał Elenę do tańca z szablami i zrobienia epickiego widowiska. Został może ranny, ale pokazał Elenie jak można to zrobić, wyciągnął ją troszkę ze skorupki i pokazał jej PIĘKNO tańca. Zbliża ją do siebie i siebie do niej.


### Egzotyczna Piękność na Astralnej Flarze

* **uid:** 221123-egzotyczna-pieknosc-na-astralnej-flarze, _numer względny_: 6
* **daty:** 0100-07-18 - 0100-07-21
* **obecni:** Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Ellarina Samarintael, Erwin Pies, Frank Mgrot, Gabriel Lodowiec, Gerwazy Kircznik, Hind Szug Traf, Kajetan Kircznik, Leszek Kurzmin, Marcel Kulgard, OO Astralna Flara, OO Athamarein, SCA Hadiah Emas, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Flara naprawia Hadiah Emas, a po drodze komodor Lodowiec rozplątuje wszystkie wątki - niewolnik z Orbitera? (okazał się dezerterem), autonomiczna TAI Mirtaela d'Hadiah Emas? (zostaje Ograniczona przez psychotronika Athamarein) itp. Arianna zarządza Flarą, wprowadzając Ellarinę jako maskotkę od morale i stabilizując wszystko by jakoś zespół dopasował się do realiów Nonariona. To nie jest ani Aurum ani Orbiter, to coś zupełnie innego.

Aktor w Opowieści:

* Dokonanie:
    * oglądał przesłuchanie Ellariny. Docenił jej umiejętności manipulacyjne i jej wpływ na morale. Uczy się, jak do niej podejść by ją kontrolować dla Arianny - może być jedyną osobą zdolną do przewidzenia jej ruchów i kontroli Ellariny na Flarze.


### Astralna Flara dociera do Nonariona Nadziei

* **uid:** 221116-astralna-flara-dociera-do-nonariona-nadziei, _numer względny_: 5
* **daty:** 0100-07-14 - 0100-07-17
* **obecni:** Alan Nierkamin, Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Gabriel Lodowiec, Grażyna Burgacz, Kajetan Kircznik, Leo Kasztop, Leszek Kurzmin, Maja Samszar, Marcel Kulgard, OO Astralna Flara, OO Athamarein, SCA Hadiah Emas, SCA Isigtand, Szymon Wanad, Tomasz Ruppok, Władawiec Diakon

Streszczenie:

Flara dociera do Nonariona, który wygląda jak absolutny śmieć. Morale na Flarze ucierpiało - aż do plotek o Egzotycznych Pięknościach, co sprawiło Ariannie kolejne problemy. Alan, eks-lokals powiedział Darii że jest jakaś forma standaryzacji na Nonarionie - coś tam jest, coś "bogatego" jako frakcja. Daria zdobyła od Leo planetoidę którą może użyć jako bazę dla Orbitera, ale w drodze tam zboczyli by ratować uszkodzone statki lokalne. I uratowali jednostkę niewolniczą i zniszczony grazer. Komodor Lodowiec - sensowny koleś - ma dylemat moralny. Neikatiańska TAI oraz niewolnicy.

Aktor w Opowieści:

* Dokonanie:
    * zachowuje absolutny optymizm i rozpoczął ćwiczenia zapasów z Eleną i z marines. Skutecznie skanuje asteroidy; pełni rolę eksperta od radarów i detektorów.


### Astralna Flara i porwanie na Karsztarinie

* **uid:** 221102-astralna-flara-i-porwanie-na-karsztarinie, _numer względny_: 4
* **daty:** 0100-06-02 - 0100-06-08
* **obecni:** Ada Wyrocznik, Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Elena Verlen, Gerwazy Kircznik, Grażyna Burgacz, Hubert Kerwelenios, Kajetan Kircznik, Klarysa Jirnik, Maja Samszar, Mariusz Bulterier, OO Karsztarin, OO Optymistyczny Żuk, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Arianna otrzymała nowy okręt - jednostkę wsparcia Astralna Flara. Jako pilota dostała Elenę. Elena ma problemy z aklimatyzacją na Orbiterze; chce być najlepsza i walczy o to. Nie rozmawia z ludźmi. Arianna zdecydowała się poprosić Alezję o pomoc w tej dziedzinie. Tymczasem przy ćwiczeniach na Karsztarinie okazało się, że Flara jest tam w czasie porwania - Zespołowi Flary udało się zatrzymać porwanie, choć operacja była niesamowicie niebezpieczna - niedoświadczony Zespół vs 4 komandosów Syndykatu Aureliona.

Aktor w Opowieści:

* Dokonanie:
    * chce się wykazać przed Arianną i być najlepszym oficerem ever. Acz podrywanie lasek na propsie, choć bez ekstra środków. CEL: Elena.
* Progresja:
    * będzie flirtował z Eleną, will make her his. Ale bez użycia specjalnych technik, samym urokiem i umiejętnościami.


### Kapitan Verlen i niezapowiedziana inspekcja

* **uid:** 221012-kapitan-verlen-i-niezapowiedziana-inspekcja, _numer względny_: 3
* **daty:** 0100-03-22 - 0100-03-26
* **obecni:** Adam Chrząszczewicz, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Hubert Kerwelenios, Leona Astrienko, Maja Samszar, Mariusz Bulterier, OO Królowa Kosmicznej Chwały, Szczepan Myrczek, Tomasz Dojnicz, Władawiec Diakon

Streszczenie:

Podczas skanu kadłuba Królowej advancerzy z Darią wykryli scrambler TAI, destabilizator silników itp. Arianna stoczyła bitwę ćwiczebną z marine, ale pojawiła się inspekcja (która nie była zapowiedziana bo Maja nie przekazała informacji). Daria reanimowała TAI, wszystko częściowo poszło nie tak. Ale o dziwo audytor chciał pomóc Ariannie, która stanęła między Orbiterem (wsparcie) i Aurum (my sami sobie poradzimy). Arianna upewniła się, że Władawiec nie stał za operacją - najpewniej Maja. Ale Królowa jest w lepszym stanie niż kiedykolwiek, tylko, że Arianna je konserwy (by nie wpaść pod feromony itp)

Aktor w Opowieści:

* Dokonanie:
    * wpuścił na Infernię inspektora Adama, ale chronił Ariannę (nie wiedział o inspekcji). Powiedział jej, że nie złamał porozumienia. Postara się, by wszystko co stało się na jednostce zostało na jednostce.


### Kapitan Verlen i pojedynek z marine

* **uid:** 220928-kapitan-verlen-i-pojedynek-z-marine, _numer względny_: 2
* **daty:** 0100-03-19 - 0100-03-21
* **obecni:** Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Grażyna Burgacz, Leona Astrienko, Leszek Kurzmin, Maja Samszar, OO Królowa Kosmicznej Chwały, Stefan Torkil, Szymon Wanad, Tomasz Ruppok, Władawiec Diakon

Streszczenie:

Królowa miała się przesunąć, ale prawie się rozbiła; ta jednostka jest rozkradziona. Arianna zatrzymała biczowanie załogantów i doszła do tego, że załoga jest niemrawa i skonfliktowana bo część ludzi jest z Aurum a część nie. Perfect storm of suck. By zatrzymać Wanada przed "wygraniem cnoty koleżanki inżyniera" stoczyła z nim pojedynek i wygrała. Leona ostrzegła, by Arianna nic nie jadła i nie piła. Czyli coś jest w jedzeniu. To tłumaczy czemu Królowa jest tak zdegenerowaną jednostką. Aha, Arianna ma wsparcie kapitana Leszka Kurzmina, dawnego rywala Alezji.

Aktor w Opowieści:

* Dokonanie:
    * okazuje się, że na pokładzie są cztery osoby które go nie kochają - Daria, Arianna (nowe), Leona (scaaary) i Grażyna (nie spełnia klasycznych kanonów). He did stuff to water/food...


### Kapitan Verlen i Królowa Kosmicznej Chwały

* **uid:** 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly, _numer względny_: 1
* **daty:** 0100-03-11 - 0100-03-17
* **obecni:** Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Grażyna Burgacz, Klarysa Jirnik, Klaudiusz Terienak, Leona Astrienko, Maja Samszar, OO Królowa Kosmicznej Chwały, Szymon Wanad, Władawiec Diakon

Streszczenie:

Królowa Kosmicznej Chwały to najbardziej dysfunkcyjna jednostka pod kontrolą Orbitera - służy do łamania karier obiecujących oficerów i sabotażu programu kosmicznego Aurum (oba z woli sił specjalnych Aurum). Trafiła na królową Arianna i zaczęła robić porządek. Poprzednią panią kapitan zdecydowała się odratować, współpracując z Darią z inżynierii rozmontować wyścigi psów i stwierdziła, że doprowadzi do tego, by Królowa zaczęła skutecznie działać podczas ćwiczeń. Aha, tu poznała się z Leoną Astrienko ;-).

Aktor w Opowieści:

* Dokonanie:
    * pierwszy oficer Królowej; zdominował i scorruptował Alezję i 'wszystkie spódniczki jego'. Zdecydował się nie sabotować Arianny jeśli ona mu nie przeszkadza. Nie chce oddać holdu na Alezji - jego zabawka.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 8, @: 0100-09-21
    1. Primus    : 8, @: 0100-09-21
        1. Sektor Astoriański    : 8, @: 0100-09-21
            1. Libracja Lirańska    : 4, @: 0100-09-21
                1. Anomalia Kolapsu, orbita    : 4, @: 0100-09-21
                    1. Planetoida Kazmirian    : 1, @: 0100-07-17
                    1. SC Nonarion Nadziei    : 1, @: 0100-07-17
                        1. Moduł ExpanLuminis    : 1, @: 0100-07-17
                    1. Strefa Biur HR    : 1, @: 0100-09-21
                    1. Strefa Upiorów Orbitera    : 2, @: 0100-09-21
                        1. Planetoida Kazmirian    : 1, @: 0100-09-15
                        1. Planetoida Lodowca    : 2, @: 0100-09-21

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 8 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Daria Czarnewik      | 8 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Maja Samszar         | 7 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Arnulf Perikas       | 6 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Grażyna Burgacz      | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kajetan Kircznik     | 5 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leszek Kurzmin       | 5 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alezja Dumorin       | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| OO Astralna Flara    | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Szczepan Myrczek     | 4 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Elena Verlen         | 3 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Erwin Pies           | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Leona Astrienko      | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| OO Królowa Kosmicznej Chwały | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Szymon Wanad         | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Ellarina Samarintael | 2 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor)) |
| Gabriel Lodowiec     | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gerwazy Kircznik     | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hubert Kerwelenios   | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Klarysa Jirnik       | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Marcel Kulgard       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Mariusz Bulterier    | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Loricatus         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Salazar Bolza        | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| SCA Hadiah Emas      | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Tomasz Ruppok        | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Ada Wyrocznik        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Adam Chrząszczewicz  | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Kaspian Certisarius  | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Klaudiusz Terienak   | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leo Kasztop          | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| NekroTAI Zarralea    | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| OO Karsztarin        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Stefan Torkil        | 1 | ((220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Tomasz Dojnicz       | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |