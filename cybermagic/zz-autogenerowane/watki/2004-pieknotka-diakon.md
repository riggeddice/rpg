# Pięknotka Diakon
## Identyfikator

Id: 2004-pieknotka-diakon

## Sekcja Opowieści

### Kraloth w parku Janor

* **uid:** 201025-kraloth-w-parku-janor, _numer względny_: 90
* **daty:** 0110-10-29 - 0110-10-31
* **obecni:** Hestia d'Janor, Lucjusz Blakenbauer, Maciej Oczorniak, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Dużo ciężkiej pracy Lucjusza i Minerwy, ale Pięknotka wróciła do "normy", acz potrzebuje regularnych transfuzji. Pięknotka oddała Minerwie Erwina; sama myśli jak zamieszkać z Lucjuszem i nie udawać jego narzeczonej. Chciała skończyć sprawę w parku rozrywki; z Minerwą odkryły że za wszystkim stoi cholerny kraloth. Porwały terminusa przejętego przez kralotha by on wezwał SOS z Pustogoru.

Aktor w Opowieści:

* Dokonanie:
    * próbuje się odnaleźć w świecie, w którym jest nie-sobą. Znajduje z Minerwą kralotha w parku rozrywki i porywa zniewolonego terminusa używając nowej siły.
* Progresja:
    * koniecznie potrzebuje transfuzji puryfikacyjnej krwi raz na tydzień, pełna transfuzja. Pod opieką Lucjusza Blakenbauera.


### Narodziny paladynki Saitaera

* **uid:** 201011-narodziny-paladynki-saitaera, _numer względny_: 89
* **daty:** 0110-10-12 - 0110-10-13
* **obecni:** Agaton Ociegor, Aleksander Muniakiewicz, Gabriel Ursus, Minerwa Metalia, Pięknotka Diakon, Sabina Kazitan, Saitaer

Streszczenie:

Pięknotka wyprawiła Sabinę Kazitan do Aurum po akcji Gabriela, ale wymuszając na obu stronach by zaprzestały wojny ze sobą. Jako, że zaczął się atak na kompleks Grzymościa, Alan i Gabriel zostali tam przekierowani - a Pięknotka poszła z Minerwą do parku rozrywki Janor by zakończyć tą sprawę. Niestety, grzymościowiec Ociegor złapał Pięknotkę chcąc ją "wyleczyć" z kralotycznej siły. Pięknotka wezwała Cienia i ów prawie zabił Ociegora. Walcząc z Cieniem, Pięknotka umarła - i Saitaer ją przywrócił jako swoją paladynkę.

Aktor w Opowieści:

* Dokonanie:
    * skończyła waśń między rodami Kazitan i Ursus; potem zrobiła nagą dywersję w parku rozrywki i gdy złapał ją Agaton by "wyleczyć", spanikowała. Gdy Cień prawie zabił Agatona, Pięknotka stanęła przeciw Cieniowi twarzą w twarz i zginęła. Saitaer ją zixionizował i przywrócił jako swoją paladynkę, ku wielkiej żałości Pięknotki.
* Progresja:
    * PIVOTAL POINT; zginęła w walce z Cieniem i została odrodzona przez Saitaera jako jego paladynka. Cień został zniszczony, zintegrowany z jej ciałem (poza Esuriit). Pięknotka jest klasyfikowana jako mag ixioński, zupełnie jak Minerwa. Wyjątkowo wrażliwa na Esuriit, ale z nanorozkładem materii i świetną biosyntezą.


### Haracz w parku rozrywki

* **uid:** 200913-haracz-w-parku-rozrywki, _numer względny_: 88
* **daty:** 0110-10-09 - 0110-10-10
* **obecni:** Adam Janor, Alan Bartozol, Diana Tevalier, Franciszek Owadowiec, Karol Senonik, Krystyna Senonik, Maja Janor, Mariusz Trzewń, Melinda Teilert, Pięknotka Diakon, Rafał Kamaron

Streszczenie:

Łatwa misja w okolicach parku rozrywki Janor przekształciła się w dziwną operację z pasożytem magicznym dotykającym pobierającego haracz Grzymościowca i dziwnie zachowującą się komendant policji. Pięknotka i Alan rozwalili siły Grzymościa, ale nie są pewni, czy to nie była tylko dywersja... (a tymczasem Melinda i Chevaleresse świetnie się bawiły w parku rozrywki).

Aktor w Opowieści:

* Dokonanie:
    * zestrzeliwuje servary szybciej niż Alan Fulmenem, gada z ludźmi i zbiera informacje dla Alana, też znalazła dronę która ich śledziła.


### Adaptacja Azalii

* **uid:** 200623-adaptacja-azalii, _numer względny_: 87
* **daty:** 0110-09-26 - 0110-10-04
* **obecni:** Azalia d'Alkaris, Karla Mrozik, Konrad Wączak, Minerwa Metalia, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Orbiter, frakcja NeoMil, wystawili na Trzęsawisko bazę nanitkową której celem jest uczenie odludzkiej TAI klasy Azalia. Pięknotka i Minerwa poszły jako wsparcie z Pustogoru. Okazało się, że Wiktor pozwala TAI Azalii na wiarę, że adaptuje się do Trzęsawiska by przejąć nad nią kontrolę. Pięknotka wynegocjowała bezpieczną ewakuację - ale Wiktor powiedział, że zniszczy Castigator. Pięknotka nie wie jak, ale OK. Nie wygra tego. Grunt, że wszyscy są bezpieczni.

Aktor w Opowieści:

* Dokonanie:
    * przeprowadziła ekspedycję Orbitera przez Trzęsawisko z pomocą Minerwy; wcześniej przenegocjowała z Wiktorem żeby im bardzo nie przeszkadzał.


### Bardzo straszna mysz

* **uid:** 200616-bardzo-straszna-mysz, _numer względny_: 86
* **daty:** 0110-09-18 - 0110-09-21
* **obecni:** Diana Lauris, Franek Bulterier, Gabriel Ursus, Henryk Wkrąż, Ksenia Kirallen, Laura Tesinik, Matylda Sęk

Streszczenie:

Kilka Rekinów chciało się podlizać Gerwazemu Lemurczakowi i postraszyć Dianę i Melindę. Diana w odpowiedzi sprawiła, że ich eksperymentalna 'mysz Esuriit' uciekła i zaczęła stanowić prawdziwe zagrożenie. W REAKCJI NA TO Laura i Gabriel rozbroili sytuację, wykryli obecność Diany i zatarli wszelkie ślady. Diana jednak nie wybacza i nie zapomina a Henryk też chce iść w temat dalej...

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * Dostała solidny opiernicz od Ksenii za to, że puszcza Gabriela na akcje z potworami w terenie Esuriit. Poszło do papierów.


### Dom dla Melindy

* **uid:** 200524-dom-dla-melindy, _numer względny_: 85
* **daty:** 0110-09-13 - 0110-09-16
* **obecni:** Diana Lemurczak, Diana Tevalier, Franek Bulterier, Gerwazy Lemurczak, Laura Tesinik, Mariusz Trzewń, Melinda Teilert, Pięknotka Diakon

Streszczenie:

Rodzina chce Melindę z powrotem, Melinda nie chce wracać. Pięknotka szuka Melindy i znalazła ją w Arkadii. Gdy poznała sytuację, we współpracy z Dianą wsadziła Melindę Alanowi przy użyciu Chevaleresse. Wszyscy są zadowoleni, choć Melinda musi opuścić Podwiert - ale wreszcie jest bezpieczna.

Aktor w Opowieści:

* Dokonanie:
    * zaczęła od szukania zaginionej Melindy, skończyła na współpracowaniu z Melindą by ta nie wracała do domu; wsadziła Melindę Alanowi dla poprawienia humoru.


### Tajna baza Orbitera?

* **uid:** 200510-tajna-baza-orbitera, _numer względny_: 84
* **daty:** 0110-09-07 - 0110-09-11
* **obecni:** Alina Anakonda, Artur Kołczond, Gabriel Ursus, Ignacy Myrczek, Kallista Exolon, Laura Tesinik, Mariusz Trzewń, Natalia Tessalon, Pięknotka Diakon, Sabina Kazitan, Talarand d'Irrydius, Teresa Mieralit, Tymon Grubosz

Streszczenie:

Natalia Tessalon chce przetransportować rannego brata do Aurum, ale manewr zablokowała Laura (terminuska-prawniczka) na prośbę Pięknotki. Natalia i Sabina się ścięły, przez co ucierpiał Myrczek a Sabina trafiła do aresztu. Teresa Mieralit zapewni, że Myrczek nie ma głowy do Sabiny. Pięknotka z Tymonem poszukali kto ciężko zmiażdżył Gabriela; udało się im znaleźć tajną bazę Orbitera w której jest Emulatorka, która się do tego przyznała.

Aktor w Opowieści:

* Dokonanie:
    * katalizator: wyłączyła relację Myrczek - Kazitan, odnalazła tajną bazę Irrydius przy użyciu Tymona oraz porozmawiała z tamtejszą Emulatorką.


### Rekin z Aurum i fortifarma

* **uid:** 200509-rekin-z-aurum-i-fortifarma, _numer względny_: 83
* **daty:** 0110-08-30 - 0110-09-04
* **obecni:** Artur Kołczond, Artur Michasiewicz, Erwin Galilien, Gabriel Ursus, Natalia Tessalon, Pięknotka Diakon, Sabina Kazitan, Tadeusz Tessalon

Streszczenie:

Sabina Kazitan ostrzegła Pięknotkę, że Natalia Tessalon z Aurum zażądała od niej niebezpiecznego rytuału do hodowania potwora. Pięknotka doszła do tego z sygnatury energii magicznej, że problem jest w okolicy Studni Irrydiańskiej; tam jest też fortifarma w której stary były-terminus tępi Latające Rekiny. I faktycznie - arystokraci z Aurum stworzyli potwora (krwawego) a Pięknotka Cieniem go zniszczyła. Niestety, brat Natalii został ciężko porażony i nigdy nie będzie już taki jak kiedyś. Aha, kwatermistrz opieprza Pięknotkę.

Aktor w Opowieści:

* Dokonanie:
    * 
* Progresja:
    * podpadła Arturowi Michasiewiczowi, kwatermistrzowi Aurum. Nie szanuje sprzętu i go uszkadza a nie musi.


### Po co atakują Minerwę?

* **uid:** 200502-po-co-atakuja-minerwe, _numer względny_: 82
* **daty:** 0110-08-26 - 0110-08-28
* **obecni:** Feliks Mirtan, Halina Sermniek, Kreacjusz Diakon, Minerwa Metalia, Nikodem Larwent, Pięknotka Diakon

Streszczenie:

Okazuje się, że za atakiem na Minerwę stali Technożercy - a dokładniej, ich liderka - Halina. Pięknotka ostro weszła do Haliny, dewastując jej mieszkanie. Dowiedziała się, że Halina chciała dać swoim sponsorom z Aurum coś czego żądali - i przypadkowo (nie wiedząc o tym) skalibrowała Infiltratora na Minerwę. Pięknotka złamała Halinę, zmusiła ją do opuszczenia Technożerców. Nie ma mowy, że czarodziejka będzie wspierać taką organizację ludzi.

Aktor w Opowieści:

* Dokonanie:
    * pełne zastraszenie Haliny; weszła w ciemności PLUS po aktywacji Cienia dokonała demolki. Zastraszanie było super-efektywne.
* Progresja:
    * opinia wyjątkowo okrutnej terminuski wśród cywili; wchodzi twardo w pacyfistkę Halinę. Ale jest niesamowicie skuteczna - to się liczy.


### Infiltrator poluje na TAI Minerwy

* **uid:** 200425-inflitrator-poluje-na-tai-minerwy, _numer względny_: 81
* **daty:** 0110-08-24 - 0110-08-26
* **obecni:** Erwin Galilien, Gabriel Ursus, Laura Tesinik, Minerwa Metalia, Pięknotka Diakon, Strażniczka Alair, Tomasz Tukan

Streszczenie:

Problemy na Trzęsawisku wpływają na Cyberszkołę w Zaczęstwie, co uszkadza możliwości TAI "Strażniczki", czyli Eszary d'AlephAiren. Tymczasem jakaś tajemnicza siła uszkadza cywilne TAI, które usprawniała Minerwa. Pięknotka odpiera Tukana, który chce udowodnić winę Minerwy i odkrywa, że za tym wszystkim stoi ktoś z Aurum, ktoś kto posiada profesjalnego pajęczego Infiltratora Iniekcyjnego, zaprojektowanego do uszkadzania AI. Dzięki temu Pięknotka wyciągnęła Minerwę z aresztu.

Aktor w Opowieści:

* Dokonanie:
    * chcąc pomóc Minerwie przed fałszywym oskarżeniem o uszkadzanie TAI odkryła i zniszczyła Infiltratora Iniekcyjnego. Spadła z awiana by spłaszczyć elektro-DJa.


### Wojna Trzęsawiska

* **uid:** 200418-wojna-trzesawiska, _numer względny_: 80
* **daty:** 0110-08-16 - 0110-08-21
* **obecni:** Gabriel Ursus, Ignacy Myrczek, Karla Mrozik, Pięknotka Diakon, Sabina Kazitan, Wiktor Satarail

Streszczenie:

Trzęsawisko odpowiedziało na atak tworząc nową bioformę na bazie Sabiny Kazitan; zaczęło przyciągać ludzi do siebie. Z uwagi na wysokie niebezpieczeństwo Karla chciała zbombardować Trzęsawisko z orbity, jak kiedyś. Pięknotka zrobiła kanał negocjacyjny Pustogor - Wiktor Satarail; Wiktor pomoże Pięknotce uspokoić Trzęsawisko, ale nie będzie bombardowania z orbity. Plus, Pustogor pomoże mu sformować nową bioformę...

Aktor w Opowieści:

* Dokonanie:
    * wpierw broniła Czemerty przed bioformami Trzęsawiska (przez kłąb feromonów ściągających) a potem przekradła się po Trzęsawisku by negocjować z Wiktorem Satarailem.


### Nawoływanie Trzęsawiska

* **uid:** 200417-nawolywanie-trzesawiska, _numer względny_: 79
* **daty:** 0110-08-12 - 0110-08-14
* **obecni:** Gabriel Ursus, Ignacy Myrczek, Napoleon Bankierz, Olaf Zuchwały, Pięknotka Diakon, Sabina Kazitan, Strażniczka Alair

Streszczenie:

Trzęsawisko Zjawosztup zostało Skrzywdzone przez ostatnie wydarzenia. Zaczęło wabić Sabinę i Ignacego by ich pożreć. Szczęśliwie, Pięknotka zdążyła dotrzeć do Sabiny (która wabiona szukała Ignacego) i uratowała ich przed Trzęsawiskiem. Dodatkowo Pięknotka poznała sekret Sabiny i zdobyła nad nią absolutną kontrolę.

Aktor w Opowieści:

* Dokonanie:
    * śledząc dziwne zachowanie Sabiny odkryła jej sekret i ją złamała. Ale potem spróbowała jej pomóc i odpięła ją od Trzęsawiska używając Szpitala Terminuskiego. Przekonała Gabriela by zachował sekret Sabiny dla siebie.
* Progresja:
    * ma pełną kontrolę nad losem Sabiny Kazitan; Sabina jej NIE nienawidzi, ale się jej boi.


### Arystokraci na Trzęsawisku

* **uid:** 200414-arystokraci-na-trzesawisku, _numer względny_: 78
* **daty:** 0110-08-04 - 0110-08-05
* **obecni:** Franciszek Leszczowik, Ignacy Myrczek, Mariusz Trzewń, Pięknotka Diakon, Sabina Kazitan, Strażniczka Alair, Talia Aegis

Streszczenie:

Franciszek z Aurum dostosował Sabinę do swoich upodobań jako seksbombę. Sabina zdecydowała się go zniszczyć - doprowadziła go do Wysysacza Esuriit i wskazała, że na Trzęsawisku może zdobyć coś co mu się przyda. Franciszek stracił część świty na Trzęsawisku; gdyby nie Pięknotka, zginąłby tam. A tak tylko skończył z traumą a Sabinie się upiekło - roboty społeczne na 2 tygodnie.

Aktor w Opowieści:

* Dokonanie:
    * sprowadziła kontrolowanego potwora Trzęsawiska na głupią ekspedycję arystokratów a potem by ich ratować użyła chemikaliów by straumatyzować Franciszka.


### Rozbrojenie bomby w Kalbarku

* **uid:** 200222-rozbrojenie-bomby-w-kalbarku, _numer względny_: 77
* **daty:** 0110-07-27 - 0110-08-01
* **obecni:** Aleksandra Szklarska, Ataienne, Bartłomiej Małczarek, Diana Tevalier, Lucjusz Blakenbauer, Mateusz Kardamacz, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Kalbark okazał się jedną wielką bombą. Liberatis, silnie uzbrojeni. Kardamacz, kontroluje całe miasto. Terminusi, skażeni kralotycznie. I Chevaleresse na którą wszystko spadnie jeśli przyjdzie wsparcie. Pięknotka rozmontowała tą bombę, ściągnęła Ataienne (oficjalnie) i Zespół dał radę rozproszyć Liberatis, wycofać Chevaleresse i odzyskać kontrolę nad Barbakanem. Pięknotka zebrała wsparcie i na polu bitwy został na serio tylko Kardamacz.

Aktor w Opowieści:

* Dokonanie:
    * pokonała dwóch terminusów i bioformę bez Cienia. Potem zdobyła wsparcie Ataienne, wydobyła Chevaleresse i zapewniła siły w Kalbarku na potem.


### Krucjata Chevaleresse

* **uid:** 200202-krucjata-chevaleresse, _numer względny_: 76
* **daty:** 0110-07-24 - 0110-07-27
* **obecni:** Ataienne, Damian Orion, Diana Tevalier, Keraina d'Orion, Malictrix d'Itaran, Mariusz Trzewń, Mateusz Kardamacz, Minerwa Metalia, Pięknotka Diakon, Talia Aegis, Tomasz Tukan

Streszczenie:

Chevaleresse wykradła broń Alana i uzbroiła Liberitias, gdzie działa też Ataienne przeciw Kardamaczowi, Grzymościowi i krzywdzicielowi Alana. Pięknotka doszła do serca problemu i materii, acz potrzebowała wsparcia Orbitera - dokładniej, Kerainy. Dzięki Kerainie wprowadziła Malictrix do Kompleksu Itaran. Teraz to tylko kwestia czasu aż Grzymość zostanie zniszczony.

Aktor w Opowieści:

* Dokonanie:
    * rozplątuje upiorną sieć zarzuconą przez Ataienne i Chevaleresse przeciwko Grzymościowi, Kardamaczowi oraz krzywdzicielowi Alana. Zmartwiona niestabilnością Ataienne.


### Ukradziony Entropik

* **uid:** 191201-ukradziony-entropik, _numer względny_: 75
* **daty:** 0110-07-13 - 0110-07-15
* **obecni:** Hestia d'Itaran, Keraina d'Orion, Mariusz Trzewń, Minerwa Metalia, Persefona d'Jastrząbiec, Pięknotka Diakon, Roland Grzymość

Streszczenie:

Orbiter szukał informacji o zaginionym dawno Entropiku; Ataienne go wykryła gdy przejęła Jastrząbiec. Pięknotka wraz z miragentem znalazła owego Entropika - jest w ukrytym kompleksie medyczno-badawczym Grzymościa. By nie eskalować i nie ryzykować ludzkich żyć, doprowadziła do ewakuacji Entropika. Dzięki temu Orbiter musiał poradzić sobie sam. A Pustogor stanął przed tym, że Grzymość ma potężnych sojuszników i potężną bazę w Jastrząbcu.

Aktor w Opowieści:

* Dokonanie:
    * stanęła przed niemożliwą sytuacją - Orbiter czy Karla. Tępić Grzymościa czy zdrowie ludzi. A wszystko dlatego, bo zniknął jeden Entropik.


### Kwiaty w służbie puryfikacji

* **uid:** 190817-kwiaty-w-sluzbie-puryfikacji, _numer względny_: 74
* **daty:** 0110-07-04 - 0110-07-09
* **obecni:** Ataienne, Erwin Galilien, Kornel Garn, Nataniel Marszalnik, Pięknotka Diakon, Teresa Marszalnik

Streszczenie:



Aktor w Opowieści:

* Dokonanie:
    * udaje Adelę Kirys;


### Korupcja z arystokratki

* **uid:** 191112-korupcja-z-arystokratki, _numer względny_: 73
* **daty:** 0110-07-04 - 0110-07-05
* **obecni:** Ataienne, Ignacy Myrczek, Mariusz Trzewń, Minerwa Metalia, Persefona d'Jastrząbiec, Pięknotka Diakon, Sabina Kazitan, Tadeusz Sklerzec

Streszczenie:

Po wydarzeniach z Grzymościem oczy Pięknotki padły na Jastrząbiec - tam dzieją się dziwne rzeczy z narkotykami. Okazało się, że to Ignacy Myrczek próbuje rozpaczliwie udowodnić arystokratce z Aurum, że nie zrobił jej krzywdy świadomie a pod wpływem narkotyków. Pięknotka wpadła (z pomocą Ataienne) i zrobiła porządek - ukarała burmistrza, Myrczka, arystokratkę i w sumie wszystkich. Ma smak popiołu w ustach, tu nie było nikogo sensownego...

Aktor w Opowieści:

* Dokonanie:
    * rozdysponowała wpierdol - arystokratce, uczniakowi, burmistrzowi, nawet TAI. Ściągnęła Ataienne by wiedzieć komu dać wpierdol.


### Zaginiona soniczka

* **uid:** 191105-zaginiona-soniczka, _numer względny_: 72
* **daty:** 0110-07-02 - 0110-07-03
* **obecni:** Ataienne, Erwin Galilien, Mariola Tralment, Mariusz Trzewń, Mateusz Kardamacz, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Ataienne poprosiła Pięknotkę o znalezienie zaginionej fanki. Pięknotka zaczęła śledztwo i doszła do tego, że Mariola została porwana przez gang przerzucający dziewczyny do Cieniaszczytu. Erwin słyszał takie plotki. Pięknotka zlokalizowała i usunęła komórkę tego gangu; śledztwo pokazało na autoklub fitness jako jedno interesujące miejsce. Najpewniej nie to jest miejscem "zła", ale warto sprawdzić.

Aktor w Opowieści:

* Dokonanie:
    * posłuchała prośby Ataienne, nie wierząc o co chodzi - i rozwiązała ring transportujący dziewczyny do Cieniaszczytu. Dzięki temu mają ślad na oficera Grzymościa.


### Niespodziewany wpływ Aidy

* **uid:** 190804-niespodziewany-wplyw-aidy, _numer względny_: 71
* **daty:** 0110-06-30 - 0110-07-02
* **obecni:** Amanda Kajrat, Gabriel Ursus, Julia Morwisz, Ossidia Saitis, Pięknotka Diakon, Saitaer

Streszczenie:

Po złapaniu Aidy przez Hralwagha z Cieniaszczytu okazało się, że Aida wpłynęła na samego Hralwagha. Potwór splugawił swoich kontrolerów z Cieniaszczytu. Pięknotka współpracując z Ossidią i Saitaerem pokonała Hralwagha zanim komukolwiek stała się krzywda. Dodatkowo - Ossidia porwała Amandę Kajrat dla Saitaera; Ossidia dalej poluje na Ernesta Kajrata.

Aktor w Opowieści:

* Dokonanie:
    * pokonała splugawionych przez Hralwagha magów, potem we współpracy z Saitaerem i Ossidią samego Hralwagha. Oddała Saitaerowi Amandę Kajrat.


### Kontrpolowanie Pięknotki - pułapka

* **uid:** 191103-kontrpolowanie-pieknotki-pulapka, _numer względny_: 70
* **daty:** 0110-06-24 - 0110-07-01
* **obecni:** Damian Orion, Józef Małmałaz, Ksenia Kirallen, Lilia Ursus, Marek Puszczok, Mariusz Trzewń, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon, Rafał Roszczeniok

Streszczenie:

Pięknotka poluje na noktiańskiego zabójcę. W ciągu tygodnia udało jej się złożyć do kupy pułapkę, rozpocząć linię przerzutową artefaktów do Aurum, połączyć Orbiter, Lilię, Minerwę oraz Pustogor i doprowadzić do tego, że docelowo Małmałaz ma zostać złapany i doprowadzony do Pustogoru w zemście za krzywdę Alana. To był bardzo trudny tydzień, politycznie.

Aktor w Opowieści:

* Dokonanie:
    * .


### Bardzo niebezpieczne składowisko

* **uid:** 190726-bardzo-niebezpieczne-skladowisko, _numer względny_: 69
* **daty:** 0110-06-26 - 0110-06-28
* **obecni:** Amanda Kajrat, Cezary Alentik, Ernest Kajrat, Gabriel Ursus, Mirela Orion, Pięknotka Diakon, Roland Grzymość

Streszczenie:

Na Składowisku Odpadów Niebezpiecznych przy Podwiercie siły Wolnego Uśmiechu zaczęły składać jakieś narkotyki. Nie podoba się to Nocnemu Niebu. Pięknotka w środku tego wszystkiego - udało jej się poprosić Orbiter o zajęcie się linią dostawczą z Cieniaszczytu, Kajrata by chwilowo zwolnił, Pustogor by chronili właściciela a Grzymościa, by zszedł do podziemia. Polityczny sukces - ale czemu Grzymość idzie w jakieś narkotyki? I czemu Kajrat dąży do wojny z Grzymościem o te narkotyki?

Aktor w Opowieści:

* Dokonanie:
    * politycznie rozmontowała potencjalną wojnę Wolnego Uśmiechu i Nocnego Nieba o dziwne narkotyki. Ściągnęła do tego Orbiter i deeskalowała sprawę.


### Polowanie na Pięknotkę

* **uid:** 190901-polowanie-na-pieknotke, _numer względny_: 68
* **daty:** 0110-06-21 - 0110-06-24
* **obecni:** Alan Bartozol, Diana Tevalier, Józef Małmałaz, Lucjusz Blakenbauer, Mariusz Trzewń, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Noktiański zabójca chciał pozyskać Cienia; pokonał Pięknotkę, ale Alan go zestrzelił. Potem zabójca złapał Chevaleresse i korzystając z niej jako zakładniczki wysłał Alana do szpitala. Pięknotka doszła do tego, jak się nazywa z pomocą Minerwy i Cienia.

Aktor w Opowieści:

* Dokonanie:
    * przegrała walkę z Józefem; uratował ją Alan. Ale - udało jej się dowiedzieć kim był napastnik, zatrzymać głupi atak na enklawy a potem uratowała Alana i Chevaleresse.


### Zagubiony efemerydyta

* **uid:** 190917-zagubiony-efemerydyta, _numer względny_: 67
* **daty:** 0110-06-21 - 0110-06-22
* **obecni:** Gabriel Ursus, Jan Uszczar, Marek Puszczok, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

Młody efemerydyta uciekł z Eterni. Był przyciskany by dołączyć do mafii więc czasem w panice rzucał niewłaściwe zaklęcia w złych momentach. Pięknotka go znalazła i zajęła się nim - oddała go Orbiterowi, bo tam się może przydać. Mafia musi się obejść smakiem.

Aktor w Opowieści:

* Dokonanie:
    * nie ma głowy do zabawy z nieletnimi efemerydytami z Eterni. Po znalezieniu efemerydyty, oddała go Orbiterowi.


### Wypadek w Kramamczu

* **uid:** 190906-wypadek-w-kramamczu, _numer względny_: 66
* **daty:** 0110-06-18 - 0110-06-19
* **obecni:** Dariusz Kuromin, Kasjopea Maus, Ksenia Kirallen, Mariusz Trzewń, Pięknotka Diakon

Streszczenie:

Wrabiają Wiktora Sataraila w atak na niewielką firmę, Włóknin z Kramamczy! Pięknotka i Ksenia wysłane by to naprawić! Pięknotka przyzwała z powrotem wszystkie królowe które pouciekały a Ksenia odkryła sabotaż wewnętrzny. Dariusz próbuje wyciągnąć ubezpieczenie. Pięknotka wezwała Kasjopeę (dziennikarkę) do pomocy i przekonała Ksenię, żeby ta odpuściła Dariuszowi tylko znalazła osoby winne tej sytuacji i prowokacji.

Aktor w Opowieści:

* Dokonanie:
    * zamiast pójść w zniszczenie Włóknina, skupiła się na uratowaniu tej niewielkiej firmy - poprosiła Kasjopeę i zneutralizowała Ksenię.


### Odzyskana agentka Orbitera

* **uid:** 190724-odzyskana-agentka-orbitera, _numer względny_: 65
* **daty:** 0110-06-12 - 0110-06-15
* **obecni:** Adam Szarjan, Aida Serenit, Julia Morwisz, Mirela Niecień, Mirela Orion, Moktar Gradon, Pięknotka Diakon

Streszczenie:

Aidę porwała frakcja Cieniaszczytu zainspirowana przez Julię - by ratować Emulatorkę. Ale Aida, jak się okazało, jest nędznym cywilem i to takim uratowanym z kosmosu. Pięknotka wygrała z championem kralotha przez zanęcenie Moktara - a zwabiła go dając mu walkę z Mirelą. Ogólnie, wszyscy są zadowoleni..?

Aktor w Opowieści:

* Dokonanie:
    * rozwiązała problem polityczny; zinfiltrowała Cieniaszczyt, wkręciła Mirelę do walki z Moktarem i vice versa oraz ostatecznie odbiła Aidę... cudzymi siłami.


### Kirasjerka najgorszym detektywem

* **uid:** 190721-kirasjerka-najgorszym-detektywem, _numer względny_: 64
* **daty:** 0110-06-10 - 0110-06-11
* **obecni:** Aida Serenit, Mirela Orion, Olga Myszeczka, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Przyjaciółka Kirasjerki Orbitera, Aida, zniknęła. Kirasjerka poszła jej szukać i znalazła ślady Pięknotki. Oczywiście, Kirasjerka zaatakowała by zdobyć informacje a Pięknotka nie mogła się poddać (osłaniana przez Amandę). Skończyło się na dewastacji 3 servarów, ciężko rannej Kirasjerce i stropionej Pięknotce. Gdy Pięknotka poszła do Olgi znaleźć leczenie dla Mireli (Kirasjerki), tam dowiedziała się od Wiktora Sataraila, że on podłożył ślady by pokazać jej obecność Cieniaszczytu - a dokładniej kralotycznej bioformy. Największe możliwe nieporozumienie.

Aktor w Opowieści:

* Dokonanie:
    * pokonała w walce Mirelę (Kirasjerkę). Od Wiktora dowiedziała się o co chodzi z kralotami cieniaszczyckimi w okolicy. Oraz od Orbitera wysępiła stracony power suit.


### Kult, choroba Esuriit

* **uid:** 190714-kult-choroba-esuriit, _numer względny_: 63
* **daty:** 0110-06-07 - 0110-06-09
* **obecni:** Amanda Kajrat, Ernest Kajrat, Pięknotka Diakon, Tomasz Tukan

Streszczenie:

Tukan poluje dalej na temat somnibela i znalazł kult wywołany przez magię. Pięknotka zdobyła mu uprawnienia a on spieprzył - dał się zainfekować Esuriit i ruszył na wojnę z Kajratem. Pięknotka uratowała mu tyłek za co dostała wsparcie Amandy Kajrat. Z jej pomocą zniszczyła kult w Podwiercie, ale dużo wskazuje że w Czółenku (gdzie indziej...) jest ulubiony ciąg dalszy.

Aktor w Opowieści:

* Dokonanie:
    * uratowała Tukana przed Kajratem i współpracując z Amandą Kajrat doszła do tego, że kult Esuriit jest w okolicach Czółenka.


### Somnibel uciekł Arienikom

* **uid:** 190709-somnibel-uciekl-arienikom, _numer względny_: 62
* **daty:** 0110-06-05 - 0110-06-07
* **obecni:** Ernest Kajrat, Jan Revlen, Ksawery Wojnicki, Pięknotka Diakon, Staś Arienik, Tomasz Tukan, Urszula Arienik

Streszczenie:

Bogatemu rodowi Arieników powiązanemu z Luxuritias uciekł somnibel. Pięknotka znalazła go u Kajrata - który dla odmiany nie porwał kociego viciniusa; dostał go od podwładnego. Kajrat oddał somnibela Pięknotce po wykonaniu pewnego testu a napięcie między Arienikami a "plebsem" z Podwiertu się podniosło.

Aktor w Opowieści:

* Dokonanie:
    * znalazła somnibela, który uciekł do Kajrata i zdecydowała się na nie deeskalowanie konfliktu między nieprzyjemną arystokratką a "plebsem" dookoła.


### Kto wrobił Alana

* **uid:** 190830-kto-wrobil-alana, _numer względny_: 61
* **daty:** 0110-06-03 - 0110-06-05
* **obecni:** Alan Bartozol, Diana Tevalier, Karolina Erenit, Pięknotka Diakon, Talia Aegis, Wojciech Zermann

Streszczenie:

Ktoś wrabia Alana. Okazało się, że to nie Alan jest celem - to Chevaleresse. Podpadła kilku magom z Aurum w vircie i stwierdzili że ją nastraszą - ale najpierw muszą odsunąć Alana na bok. Pięknotka z Tymonem zastawili pułapkę współpracując z Chevaleresse jako przynętą. Udało się - magowie Aurum wpadli. Przy okazji - Karolina pomagała magom Aurum bo uznała, że to pomoże terminusom i Chevaleresse najbardziej. Karolina bowiem nienawidzi arystokracji magów.

Aktor w Opowieści:

* Dokonanie:
    * pogodziła Chevaleresse i Talię, osłoniła Alana przed skargą oraz zastawiła skuteczną pułapkę na Wojciecha Zermanna.


### Noc Kajrata

* **uid:** 190623-noc-kajrata, _numer względny_: 60
* **daty:** 0110-05-22 - 0110-05-25
* **obecni:** Ernest Kajrat, Liliana Bankierz, Ossidia Saitis, Pięknotka Diakon, Serafina Ira

Streszczenie:

Kajrat poszedł za ciosem swojego planu. Przekształcił Serafinę mocą Esuriit; nadał jej Aspekt Banshee, który jednak Serafina jest w stanie jakoś opanować (nie zmieniając jej charakteru ani podejścia). Pięknotka odkryła sekret Kajrata - jest częściowo istotą Esuriit i jest jednym z dowódców Inwazji Noctis. Uwolniła Lilianę spod wpływu Kajrata i udało jej się uratować Ossidię przed śmiercią z rąk upiora Esuriit.

Aktor w Opowieści:

* Dokonanie:
    * uratowała Ossidię przed zniszczeniem z Kajratowej ręki Esuriit, po czym odkryła sekret Kajrata. Wyciągnęła Lilianę ze szponów Kajrata, ale nie dała rady powstrzymać Serafiny.


### Wojna Kajrata

* **uid:** 190622-wojna-kajrata, _numer względny_: 59
* **daty:** 0110-05-18 - 0110-05-21
* **obecni:** Ernest Kajrat, Liliana Bankierz, Nikola Kirys, Olaf Zuchwały, Pięknotka Diakon, Serafina Ira

Streszczenie:

Kajrat zaeskalował; ściągnął Lilianę i powiedział Pięknotce, że Liliana zostanie jego podwładną jeśli Serafina nie osiągnie sukcesu. Pięknotka poznała historię Serafiny - kiedyś agentka Pustogoru, w wyniku burzy w Pacyfice straciła przyjaciół. Teraz jest cieniem dawnej osoby. Po drodze, w Wolnych Ptakach, Pięknotkę zaatakowała grupa wolnych magów; uruchomił się Cień i Pięknotka zrobiła masakrę. To sprawiło większe problemy i niedogadanie między Enklawami a Pustogorem

Aktor w Opowieści:

* Dokonanie:
    * utraciła kontrolę nad Cieniem i zmasakrowała grupę napastników; dodatkowo poznała przeszłość Serafiny i chroniła jak mogła Lilianę przed Kajratem.
* Progresja:
    * opinia bezwzględnej i okrutnej morderczyni wśród Wolnych Ptaków; niechętnie widziana poza murami Centrum Astorii.


### Anomalna Serafina

* **uid:** 190616-anomalna-serafina, _numer względny_: 58
* **daty:** 0110-05-12 - 0110-05-15
* **obecni:** Antoni Żuwaczka, Ernest Kajrat, Krystian Namałłek, Pięknotka Diakon, Ronald Grzymość, Serafina Ira, Tomasz Tukan

Streszczenie:

W Podwiercie pojawiła się Serafina, piosenkarka zbierająca i asymilująca anomalie. Wyraźnie pomaga jej Kajrat, który ją dodatkowo chroni. Pięknotka dostała zadanie odzyskać te niestabilne anomalie - ale Serafina jest lubiana przez kilku cieniaszczyckich bonzów; nie można odebrać jej anomalii siłą. Dodatkowo, Serafina ma w sobie stary gniew na Pustogor za coś, co się zdarzyło w Pacyfice. Wszystko zbliża się w kierunku na wojnę lub konflikt zbrojny.

Aktor w Opowieści:

* Dokonanie:
    * zatrzymuje wojnę Grzymościem,


### Mimik śni o Esuriit

* **uid:** 190527-mimik-sni-o-esuriit, _numer względny_: 57
* **daty:** 0110-05-07 - 0110-05-09
* **obecni:** Adela Kirys, Erwin Galilien, Mirela Satarail, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Mirela uciekła Wiktorowi Satarailowi z Trzęsawiska, więc ów poprosił o pomoc Pięknotkę. Mirela wyczuła Esuriit w Czółenku i chciała nakarmić głód; Pięknotka jednak dała radę ją znaleźć i nie dopuścić do Pożarcia Mireli przez Esuriit; zamiast tego ściągnęła oddział terminusów którzy sami rozwiązali problem. No i wycofała Mirelę z powrotem do Wiktora.

Aktor w Opowieści:

* Dokonanie:
    * pozyskała krew Mireli i kosmetyki Adeli by znaleźć i wkupić się w łaski Mireli, po czym nie dopuściła do spotkania Mireli z Esuriit i wezwała grupę terminusów do rozwiązania problemu.


### Uciekający seksbot

* **uid:** 190519-uciekajacy-seksbot, _numer względny_: 56
* **daty:** 0110-04-25 - 0110-04-26
* **obecni:** Arnulf Poważny, Eliza Ira, Ernest Kajrat, Liliana Bankierz, Ossidia Saitis, Pięknotka Diakon, Saitaer, Teresa Mieralit, Tomasz Tukan

Streszczenie:

Ernest z mafii przybył do szkoły magów w Zaczęstwie szukając swojego seksbota. Pięknotka deeskalowała sytuację i poszła z neuronautą Tomaszem szukać; okazało się, że seksbot jest bardzo zmodyfikowany i bardzo świadomy - do tego stopnia, że seksbotowi zaczęła pomagać Eliza Ira. Pięknotka i Tomasz rozmontowali problem, ale Pięknotka nie miała serca oddać seksbota sadystycznemu Ernestowi. W rozpaczy, poprosiła o pomoc Saitaera, który odpowiedział...

Aktor w Opowieści:

* Dokonanie:
    * od negocjacji z Ernestem, przez walkę z Elizą aż do prośby wobec Saitaera o uratowanie seksbota. Bardzo trudny okres.
* Progresja:
    * o krok bliżej do Saitaera. Wezwała go, a on odpowiedział.


### Szczur który chroni

* **uid:** 190505-szczur-ktory-chroni, _numer względny_: 55
* **daty:** 0110-04-22 - 0110-04-24
* **obecni:** Adela Kirys, Alan Bartozol, Ernest Kajrat, Kasjan Czerwoczłek, Krystian Namałłek, Lucjusz Blakenbauer, Oliwia Namałłek, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Zaczęło się od Skażenia proszkiem kralotycznym Nieużytków Staszka. Potem Pięknotka współpracując z mafią doszła do tego, że Adela i terminus stoją za tą sprawą. Potem Pięknotka uratowała Adelę i terminusa wprowadzając szturmowych terminusów Pustogoru na mafię. Gdy "Cień wyrwał się spod kontroli", Alan ją strzelił z działa strumieniowego. I tak skończyło się rumakowanie na tydzień - ale wszyscy są bezpieczni.

Aktor w Opowieści:

* Dokonanie:
    * uratowała Adelę mimo, że kosztowało ją to: trafienie z działa Koenig, tydzień w szpitalu, negocjacje z Satarailem i ogólnie mnóstwo bólu głowy i wysiłku.
* Progresja:
    * Cień oraz ona dostają reputację morderczych potworów w mafii. I wśród terminusów. Jak jest w Cieniu - uciekaj.


### Bardzo nieudane porwania

* **uid:** 190503-bardzo-nieudane-porwania, _numer względny_: 54
* **daty:** 0110-04-18 - 0110-04-20
* **obecni:** Damian Orion, Karla Mrozik, Minerwa Metalia, Mirela Orion, Nikola Kirys, Pięknotka Diakon

Streszczenie:

Kirasjerzy Orbitera dostali zadanie odbicia Minerwy i Nikoli, ale w mieszkaniu Minerwy czekała Pięknotka. Porwała z zaskoczenia Emulatorkę, uwolniła ją Cieniem i gdy Kirasjerzy uderzyli by po cichu odzyskać agentkę, Pięknotka poprosiła o pomoc Epirjon. W wyniku Pięknotce udało się ewakuować z Emulatorką, uwolniła ją Cieniem i wsadziła do Pustogoru. Potem z Karlą wynegocjowała ochronę dla Minerwy i Nikoli i jeszcze z tymi dwoma załatwiła by to było możliwe. Kirasjerzy zostali odepchnięci przez JEDNĄ terminuskę Pustogoru.

Aktor w Opowieści:

* Dokonanie:
    * wpierw porwała Kirasjerom Emulatorkę, potem ją uwolniła Cieniem, potem wycofała do Pustogoru i jeszcze politycznie zabezpieczyła Minerwę i Nikolę przed nimi. Lol.
* Progresja:
    * zdobyła 'rapport' u Cienia za uratowanie Emulatorki Kirasjerów (Mireli)
    * zdobyła szacunek Kirasjerów Orbitera za to, że samodzielnie dała radę wyślizgnąć się z ich operacji


### Pierwszy Emulator Orbitera

* **uid:** 190502-pierwszy-emulator-orbitera, _numer względny_: 53
* **daty:** 0110-04-14 - 0110-04-16
* **obecni:** Alan Bartozol, Bożymir Szczupak, Minerwa Metalia, Nikola Kirys, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

Idąc śladami Elizy Pięknotka natrafiła na Nikolę - o której dawni przyjaciele (Olaf) mówią, że "zginęła", coś jej zrobiono. Pięknotka doszła do tego, że Nikola jest projektem Emulator; dowiedziała się, że Emulatory były budowane m.in. przez Minerwę. Pięknotka doszła do tego, że zintegrowana z Finis Vitae Nikola została częściowo uwolniona przez wpływ Arazille na Finis Vitae. Gdy spotkała się z Nikolą - szok. Cień zareagował. Nikola odzyskała wolność po kontakcie z Cieniem, ale Pięknotka go utrzymała. Cień ma w sobie "emocje" spętanych Emulatorów. Pięknotce udało się wyperswadować Nikoli budzenie autowara lub niszczenie Astorii; zwiadowczyni jednak odjechała w ogromnej konfuzji i nie wiedząc, co teraz robić.

Aktor w Opowieści:

* Dokonanie:
    * jak nigdy, spotkała się ze stonewallem i nikt nie chciał jej nic powiedzieć. Na szczęście, wiedza Barbakanu oraz Cień pomogły jej w uratowaniu Nikoli oraz wszystkich przed Nikolą.
* Progresja:
    * skutecznie narzuciła swoją wolę Cieniowi w chwili, w której instynkt jej i Cienia były maksymalnie sprzeczne


### Sabotaż szeptów Elizy

* **uid:** 190429-sabotaz-szeptow-elizy, _numer względny_: 52
* **daty:** 0110-04-10 - 0110-04-13
* **obecni:** Alan Bartozol, Aleksander Rugczuk, Diana Tevalier, Eliza Ira, Erwin Galilien, Karla Mrozik, Lucjusz Blakenbauer, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

Chevaleresse wpakowała się w "kult Elizy" grając z ludźmi online; powiedziała o tym Pięknotce. Pięknotka odkryła, że Eliza rozprzestrzenia swoje wpływy przez kryształy w formie biżuterii; wraz z Alanem, Erwinem i (nadal ranną) Minerwą zaprojektowali mechanizm uszkadzający sieć krystaliczną Elizy. Udało im się kupić jakiś miesiąc czasu, aż Orbiter i Pustogor znajdą lepsze rozwiązanie. Sama Chevaleresse ma się nie spotykać z takimi tam. Ale może z nimi grać online.

Aktor w Opowieści:

* Dokonanie:
    * po śladach Chevaleresse dotarła do Elizy, sformowała zespół mający rozwalić jej plan i go wykonała. Czyżby manager?


### Zrzut w Pacyfice

* **uid:** 190427-zrzut-w-pacyfice, _numer względny_: 51
* **daty:** 0110-04-07 - 0110-04-09
* **obecni:** Erwin Galilien, Minerwa Metalia, Nikola Kirys, Pięknotka Diakon

Streszczenie:

Do Pacyfiki ktoś z Cieniaszczytu zrzucił świetnej klasy ścigacz. Pięknotka i Erwin zinfiltrowali Pacyfikę by dostać namiary na ten pojazd. Okazało się, że pilotem ma być Nikola - też Nurek Szczeliny, świetnej klasy. Eks-Noctis. Pięknotka po raz pierwszy była w Pacyfice. Dodatkowo, dowiedziała się od Minerwy, że Cień niekoniecznie odpala się na żądanie, nie rozumie Pięknotki. Świetnie.

Aktor w Opowieści:

* Dokonanie:
    * przygotowała akcję dojścia do zrzuconego ścigacza; zobaczyła dlaczego Pacyfika jest tak strasznym miejscem i doceniła Erwina ponownie


### Budowa ixiońskiego mimika

* **uid:** 190424-budowa-ixionskiego-mimika, _numer względny_: 50
* **daty:** 0110-04-03 - 0110-04-05
* **obecni:** Aleksander Rugczuk, Erwin Galilien, Karla Mrozik, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

W świetle narastających napięć w Pustogorze i czarnych artefaktów na wolności, Pięknotka wykorzystuje (za aprobatą Barbakanu) mimika który zdominował Marcela. Minerwa go psychotronicznie osłabia z perspektywy morderstw i infekuje go energią ixiońską. Pięknotka straciła go przy Dzielnicy Uciechy, ale i tak zrobił swoje (choć straty materialne są większe). Zainfekowany przez mimika wykrzyczał, że Eliza Ira wróciła oraz Saitaer się przebudził. Niepokojące dla Pięknotki.

Aktor w Opowieści:

* Dokonanie:
    * stworzyła zimny plan jak zaprowadzić pokój w Pustogorze i go przeprowadziła za aprobatą Karli. Skazić mimika energią ixiońską i wzbudzić gniew...
* Progresja:
    * dyskretny medal BlackOps od Karli, w służbie Pustogoru


### Pustogorski Konflikt

* **uid:** 190422-pustogorski-konflikt, _numer względny_: 49
* **daty:** 0110-04-02 - 0110-04-03
* **obecni:** Erwin Galilien, Karla Mrozik, Marcel Nieciesz, Olaf Zuchwały, Pięknotka Diakon, Wojmił Siwywilk

Streszczenie:

Napięcia na linii Barbakan - Miasteczkowcy w Pustogorze są silne; działania Wiktora jedynie przyspieszyły konflikt. Na rynek dostało się sporo niebezpiecznych artefaktów i Pięknotka musiała pomóc grupie Miasteczkowców. Zdesperowani, złapali terminusa i zamknęli go w piwnicy bo podczas jednego z rajdów na Miasteczkowców magowie z Fortu Mikado porwali dwie dziewczyny. Okazało się, że za tym stoi zaawansowany mimik symbiotyczny; Pięknotka z pomocą Cienia pokonała Skażonego terminusa i uwolniła czarodziejki.

Aktor w Opowieści:

* Dokonanie:
    * zaczęła jako negocjatorka z Miasteczkowcami a skończyła walcząc w Cieniu przeciwko wzmocnionemu mimikowi symbiotycznemu Marcela.


### Osopokalipsa Wiktora

* **uid:** 190419-osopokalipsa-wiktora, _numer względny_: 48
* **daty:** 0110-03-29 - 0110-03-31
* **obecni:** Alan Bartozol, Kasjopea Maus, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Wiktor zdecydował się zemścić w imieniu wił. Zaprojektował osy które robiły krzywdę magom i ludziom - wpierw użył ich jako dywersję, potem zaraził jedzenie w Sensoplex. Pięknotka dekontaminując okolicę odkryła plan; przekonała go, że można bezkrwawo usunąć stąd Sensus. I to zrobiła - z pomocą Kasjopei by ta zrobiła reportaż o "Osopokalipsie", po czym sfabrykowała dowody z pomocą Wiktora. W ten sposób Sensus opuścił teren i nikt szczególnie nie ucierpiał.

Aktor w Opowieści:

* Dokonanie:
    * chroniła ludzi, Wiktora oraz Sensus - negocjacjami i akcją bezpośrednią. Minimalizowała szkody; nie dało się niczego tu wygrać.


### Bardzo kosztowne łzy

* **uid:** 190406-bardzo-kosztowne-lzy, _numer względny_: 47
* **daty:** 0110-03-21 - 0110-03-26
* **obecni:** Adrian Wężoskór, Ida Tiara, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Wiktor Satarail pomógł wiłom z których łez produkowany był Imperialny Proszek przez Sensus. Pięknotka wynegocjowała jego bezpieczne przejście a on próbował zmienić matrycę czarodziejki dowodzącej kompleksem Sensus - by z jej łez osiągnąć lepszy Proszek. Pięknotka uratowała tą czarodziejkę.

Aktor w Opowieści:

* Dokonanie:
    * uratowała Sensus przed Wiktorem Satarailem, jednorazowo. Odpaliła Cień by odepchnąć Wiktora. Jeden dzień w szpitalu.


### Eksperymentalny power suit

* **uid:** 190402-eksperymentalny-power-suit, _numer względny_: 46
* **daty:** 0110-03-15 - 0110-03-17
* **obecni:** Ataienne, Erwin Galilien, Kaja Selerek, Minerwa Metalia, Pięknotka Diakon, Szymon Oporcznik

Streszczenie:

Orbiter chciał przekazać Pięknotce eksperymentalny Power Suit którego nikt nie potrafi kontrolować. Pięknotka spróbowała się z nim zintegrować; ona, Minerwa, Erwin i jeszcze dwóch magów Orbitera skończyło w szpitalu. Pięknotka się uśmiechnęła. Chce ten power suit. Opanuje go - mimo, że jest Skażony energią ixiońską i ma uszkodzoną psychotronikę pełną nienawiści. Życie Pięknotki uratowała Ataienne, ale skończyła ciężko ranna, z uszkodzoną matrycą.

Aktor w Opowieści:

* Dokonanie:
    * przejęła Cień od Orbitera i spróbowała go opanować. Niestety, psychotronika Cienia ją wciągnęła; Ataienne uratowała jej życie ale i tak skończyła w szpitalu.
* Progresja:
    * ma "Cień". Bardzo potężny symbiotyczny ixioński nienawistny techorganiczny power suit którego nie może kontrolować...


### Wygaśnięcie starego autosenta

* **uid:** 190213-wygasniecie-starego-autosenta, _numer względny_: 45
* **daty:** 0110-03-08 - 0110-03-09
* **obecni:** Antoni Kotomin, Atena Sowińska, Baltazar Rączniak, Dariusz Bankierz, Jadwiga Pszarnik, Pięknotka Diakon, Rafał Bobowiec

Streszczenie:

Stary autosent się jakoś zregenerował by uratować Jadwigę przed śmiercią. Zagroził Skałopływowi, szukając medyka dla Jadwigi. Zespołowi udało się postawić Jadwigę na nogi i wyprowadzić autosenta poza Skałopływ, po czym przeprowadzono echo Teresy Tevalier przez procedurę dezaktywacji autosenta. Docelowo, autosent trafił do Cieniaszczytu.

Aktor w Opowieści:

* Dokonanie:
    * przełożona operacji neutralizacji autosenta. Ku swojemu wielkiemu zdziwieniu, lokalne siły były kompetentne. Przekazała autosenta Cieniaszczytowi.


### Kurz po Ataienne

* **uid:** 190331-kurz-po-ataienne, _numer względny_: 44
* **daty:** 0110-03-07 - 0110-03-09
* **obecni:** Alan Bartozol, Ataienne, Diana Tevalier, Pięknotka Diakon, Romeo Węglas, Szymon Oporcznik

Streszczenie:

Alan poprosił Pięknotkę o interwencję w sprawie tego, że uziemił Chevaleresse; Pięknotka opanowała chcącą opuścić Alana Chevaleresse. Potem Pięknotka nawiązała użyteczny kontakt w Szymonie z Orbitera, znalazła osoby odpowiedzialne za próbę zabicia Ataienne i porozmawiała z samą Ataienne; uznała, że ta konkretna AI jest zdecydowanie dziwna.

Aktor w Opowieści:

* Dokonanie:
    * uspokoiła sytuację pomiędzy Chevaleresse i Alanem, dodatkowo doszła do porozumienia z Szymonem z Orbitera i dotarła do zleceniodawców eksterminacji Ataienne.


### Herbata, grzyby i mimik

* **uid:** 190208-herbata-grzyby-i-mimik, _numer względny_: 43
* **daty:** 0110-03-04 - 0110-03-06
* **obecni:** Almeda Literna, Antoni Kotomin, Baltazar Rączniak, Dariusz Bankierz, Jadwiga Pszarnik, Pięknotka Diakon, Stach Sosnowiecki

Streszczenie:

Na Skażonym terenie, w Skałopływie, doszło do waśni pomiędzy bizneswoman od grzybów a biznesmanem-mafioso od herbatek. Pięknotka i Antoni dotarli na miejsce, zagasili pożar, przetrwali atak zdominowanego konstruminusa i odnaleźli PRAWDZIWEGO sprawcę waśni - przejętą przez mimika symbiotycznego policjantkę Almedę. Skutecznie ją pokonali, ale w wyniku tego wszystkiego Jadwiga (bizneswoman od grzybów) musi opuścić Skałopływ, przy ogromnej żałości dla siebie i miłośników grzybów z Pustogoru.

Aktor w Opowieści:

* Dokonanie:
    * synchronizowała się z grzybami i gasiła pożar bombą chemiczną; pokonała z Antonim mimika symbiotycznego.


### Polowanie na Ataienne

* **uid:** 190330-polowanie-na-ataienne, _numer względny_: 42
* **daty:** 0110-03-03 - 0110-03-04
* **obecni:** Alan Bartozol, Ataienne, Atena Sowińska, Diana Tevalier, Lucjusz Blakenbauer, Pięknotka Diakon

Streszczenie:

Ataienne chciała zrobić koncert; Orbiter inkarnował ją w Zaczęstwie. Podczas koncertu Ataienne została zaatakowana i w jej obronie stanęła Chevaleresse, która aportowała broń Alana... Pięknotka wpierw zatrzymała tą wojnę, potem uratowała Ataienne od technovora a na końcu wydobyła siebie i Ataienne z rąk najemników którzy chcieli zniszczyć TAI. Trudny dzień; wszystko wskazuje na to, że za polowaniem na Ataienne stoi Eliza Ira?

Aktor w Opowieści:

* Dokonanie:
    * wpadła zatrzymać wojnę Chevaleresse - uczniowie szkoły magów, uratowała Ataienne przed technovorem a potem jeszcze przed grupą najemników. Straciła power suit.


### Chevaleresse

* **uid:** 190217-chevaleresse, _numer względny_: 41
* **daty:** 0110-02-25 - 0110-02-27
* **obecni:** Alan Bartozol, Diana Tevalier, Karolina Erenit, Marlena Maja Leszczyńska, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Diana, aka Chevaleresse przybyła do Cyberszkoły szukać Alana. Postraszyła Marlenę i skończyła zaatakowana przez Tymona. Alan się zaopiekował swoją członkinią gildii; okazało się, że jego gildia (Elisquid) wpadła w złe ręce. Teraz Alan rozpaczliwie szuka sposobu na to by się odbanować; tymczasowo Chevaleresse zostaje u Alana... KTÓRY STAŁ SIĘ JEJ PRAWNYM OPIEKUNEM. Aha, Alan wie, że to wina Marleny że go zabanowali (a tak naprawdę jest Pięknotki).

Aktor w Opowieści:

* Dokonanie:
    * rozpaczliwie deeskaluje wszystkie problemy między Alanem i Tymonem, między Marleną/Karoliną i Dianą. Potem nieźle się bawi kosztem Alana gdy do niego wprowadza się Chevaleresse.


### Minerwa i Kwiaty Nadziei

* **uid:** 190210-minerwa-i-kwiaty-nadziei, _numer względny_: 40
* **daty:** 0110-02-21 - 0110-02-23
* **obecni:** Atena Sowińska, Erwin Galilien, Kasjopea Maus, Kornel Garn, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Kornel Garn próbował przekonać Minerwę do dołączenia do niego, by mu pomogła - budując jej społeczeństwo które ją akceptuje i pokazując jej Kwiaty Nadziei z Ixionu. Minerwa w 100% wpadała w to, więc Pięknotka zmontowała front przeciw Kornelowi - Kasjopea, Erwin, terminusi Pustogorscy. Skończyło się ucieczką Kornela, ale Minerwa będzie chciała mu pomóc i dzielić się z nim wiedzą. A Pięknotkę bardzo martwi to, jak Kornel radzi sobie z ixiońskimi anomaliami i swoim kultem.

Aktor w Opowieści:

* Dokonanie:
    * walczyła o duszę Minerwy z Kornelem. Udało jej się zmontować front polityczny przeciw niemu i go wywalić z okolic Pustogoru - oraz zatrzymać Minerwę dla siebie.


### Nie da się odrzucić mocy

* **uid:** 190206-nie-da-sie-odrzucic-mocy, _numer względny_: 39
* **daty:** 0110-02-17 - 0110-02-20
* **obecni:** Arnulf Poważny, Karolina Erenit, Kasjopea Maus, Kornel Garn, Marlena Maja Leszczyńska, Minerwa Metalia, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Karolina źle reaguje na to, że stała się czarodziejką i chciała odrzucić moc. Jednak nie udało jej się, jedynie zdewastowała Cyberszkołę i zagroziła ludziom. Karolina sprowadziła Ixion do Cyberszkoły. Zespół ją powstrzymał i Pięknotka zaczęła reintegrować życie Karoliny, kierując ją do Marleny Mai i do AMZ.

Aktor w Opowieści:

* Dokonanie:
    * przebija się przez Cyberszkołę uszkadzając Ixion (w czym Minerwę) i unieszkodliwiając Karolinę Erenit. Potem ją reintegruje ze społeczeństwem - już jako maga.


### Migświatło psychotroniczek

* **uid:** 190828-migswiatlo-psychotroniczek, _numer względny_: 38
* **daty:** 0110-02-08 - 0110-02-10
* **obecni:** Artur Michasiewicz, Ernest Kajrat, Marek Puszczok, Minerwa Metalia, Pięknotka Diakon, Talia Aegis, Tymon Grubosz

Streszczenie:

Gdy Kajrat jest w więzieniu, nikt nie kontroluje przepływu technologii noktiańskiej. Talia ostrzegła Pięknotkę, że do holo-walki AI dodała subturingowe BIA klasy Szponowiec, z żądania gościa od Grzymościa. Okazało się, że to niegroźny hazard jest - ale są tam dwie dziwne walczące AI: BIA oraz militarna, stworzona przez Minerwę. Pięknotka chciała tylko zabić BIA, ale przypadkowo z Minerwą wysadziły Arenę Migświatła. Tymon zabrał Minerwie jej TAI. Powiedział, że jeszcze nie czas by takie rzeczy robiła.

Aktor w Opowieści:

* Dokonanie:
    * wezwana przez Talię do rozwiązania problemu z BIA. Znalazła winną (Minerwę) i drugiego winnego (Puszczoka). Z Minerwą wysadziła Migświatło; nie jest z tego szczególnie dumna.


### Czarodziejka z woli Saitaera

* **uid:** 190202-czarodziejka-z-woli-saitaera, _numer względny_: 37
* **daty:** 0110-02-01 - 0110-02-05
* **obecni:** Adela Kirys, Karolina Erenit, Minerwa Metalia, Pięknotka Diakon, Saitaer, Sławomir Muczarek, Tymon Grubosz, Wojtek Kurczynos

Streszczenie:

Wojtek Kurczynos ZNOWU dobierał się do Karoliny Erenit. Przez machinacje Sławka i Adeli Saitaer dokonał transfuzji mocy magicznej od Wojtka do Karoliny. Pięknotka drastycznie ukrywała wszystkie wpływy wszystkich osób, by ucierpiał tylko winny Wojtek. Nawet współpracowała z Saitaerem, by terroformizować Wojtka. Wszystko, by Karolina zachowała moc magiczną. ALE - doprowadziła do tego, że spojrzeli na Karolinę Mausowie. W ten sposób Karradrael uniemożliwił hold Saitaera na Karolinie.

Aktor w Opowieści:

* Dokonanie:
    * ukrywała wpływ Saitaera na Wojtka chroniąc: Adelę, Sławka, Karolinę i Minerwę. Wyintrygowała zmianę Wojtka w terrorforma by "stracił magię".
* Progresja:
    * potrafi Skażać energią Saitaera. Niestety dla niej, umie przesyłać ixiońską energię.


### Ixioński Transorganik

* **uid:** 190127-ixionski-transorganik, _numer względny_: 36
* **daty:** 0110-01-29 - 0110-01-30
* **obecni:** Erwin Galilien, Karolina Erenit, Kirył Najłalmin, Minerwa Metalia, Napoleon Bankierz, Pięknotka Diakon, Saitaer, Tymon Grubosz, Wiktor Satarail, Wojtek Kurczynos

Streszczenie:

Minerwa próbowała pomóc Karolinie Erenit, ale niestety użyła artefaktu ixiońskiego i przekształciła wrednego Wojtka Kurczynosa w transorganika. Pięknotka z pomocą Epirjona przeteleportowała siebie i Wojtka na Trzęsawisko i tam z pomocą Wiktora Sataraila odwrócili tą transorganizację. Ten konkretny problem udało się dyskretnie rozwiązać.

Aktor w Opowieści:

* Dokonanie:
    * wezwana przez Saitaera do odwrócenia ixiońskiej transorganizacji, stoczyła ciężką walkę z terrorformem (Wojtkiem). Z pomocą Wiktora, odwróciła to.


### Nowa Minerwa w nowym świecie

* **uid:** 190120-nowa-minerwa-w-nowym-swiecie, _numer względny_: 35
* **daty:** 0110-01-22 - 0110-01-26
* **obecni:** Erwin Galilien, Karla Mrozik, Kasjopea Maus, Minerwa Metalia, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Minerwa wróciła. Jako czarodziejka, opiekować się ma nią Pięknotka. Minerwa nie do końca radzi sobie w nowej rzeczywistości - nie jest piękną Diakonką, ma odłamki pamięci Saitaera i aby powrócić, poświęciła życie człowieka. Zdecydowała się tymczasowo przenieść do Zaczęstwa (Karla wysłała tam wsparcie). Tymczasem Kornel Garn już planuje by ją przekonać by doń dołączyła.

Aktor w Opowieści:

* Dokonanie:
    * rozpaczliwie próbuje poprawić samopoczucie Minerwy, żonglować polityką i Karlą oraz nie stracić do Minerwy Erwina.


### Rozpaczliwe ratowanie BII

* **uid:** 190827-rozpaczliwe-ratowanie-bii, _numer względny_: 34
* **daty:** 0110-01-18 - 0110-01-21
* **obecni:** BIA Tarn, Ernest Kajrat, Marek Puszczok, Mariusz Trzewń, Pięknotka Diakon, Sławomir Niejadek, Talia Aegis, Tymon Grubosz

Streszczenie:

Talia próbowała utrzymać przy życiu BIA 3 generacji. Nie mając surowców, użyła tej BIA do zdobycia rzeczy odżywczych z Tiamenat - zrzucając winę na Wiktora Sataraila. Pięknotka poszła za śladem i gdy dotarła do Talii, Kajrat wziął na siebie winę (za co Pięknotka go aresztowała). Następnie poprosiła Wiktora Sataraila by ten ochronił swoje dobre imię - i zniszczył śmiertelnie niebezpieczną BIA.

Aktor w Opowieści:

* Dokonanie:
    * nie uwierzyła w winę Wiktora Sataraila; zlokalizowała, że to wina BIA i przesłuchała Talię. Potem złapała Kajrata (serio) i załatwiła z Wiktorem Satarailem, że BIA będzie przezeń zniszczona.


### Skorpipedy królewskiego xirathira

* **uid:** 190119-skorpipedy-krolewskiego-xirathira, _numer względny_: 33
* **daty:** 0110-01-14 - 0110-01-15
* **obecni:** Adela Kirys, Olga Myszeczka, Pięknotka Diakon, Sławomir Muczarek, Waldemar Mózg, Wiktor Satarail

Streszczenie:

Czerwone Myszy poprosiły Pięknotkę o pomoc - pojawiły się na ich terenie niebezpieczne skorpipedy i działania Adeli jedynie pogarszają sprawę. Okazało się, że to działania Wiktora Sataraila, który chce pomóc Oldze Myszeczce w utrzymaniu terenu i odparciu harrasserów z Myszy. Pięknotka odkręciła sprawę i trochę poprawiła relację z Wiktorem. Acz jest sceptyczna wobec samych Myszy...

Aktor w Opowieści:

* Dokonanie:
    * zdecydowała się uratować Myszy przed nimi samymi - odkryła, że Olga przyjaźni się z Wiktorem i że Wiktor zdecydował się skorpipedami zrobić problemy harrassującym Olgę Myszom.
* Progresja:
    * poprawiła swoje relacje z Wiktorem Satarailem - nie jakoś bardzo, ale wrócili do neutralnego chłodnego.


### Wypalenie Saitaera z Trzęsawiska

* **uid:** 190116-wypalenie-saitaera-z-trzesawiska, _numer względny_: 32
* **daty:** 0110-01-07 - 0110-01-09
* **obecni:** Alan Bartozol, ASD Centurion, Hieronim Maus, Karradrael, Kasjopea Maus, Pięknotka Diakon, Saitaer, Wiktor Satarail

Streszczenie:

Pięknotka dostała zadanie eskortowania kapłana Karradraela do ołtarza Saitaera. ASD Centurion oraz Alan odwracali uwagę Trzęsawiska. Plan się udał, choć w wyniku Pięknotka została znowu przekształcona przez Saitaera. Za to, nie ma już wpływu Saitaera w okolicy - więcej, udało się zdjąć też wpływ z Wiktora.

Aktor w Opowieści:

* Dokonanie:
    * eskortowała (niechętnie) cywili na Trzęsawisko by zniszczyć wpływ Saitaera. Udało jej się - dodatkowo rozproszyła wpływ Saitaera na Wiktorze. Uległa transformacji Saitaera (znowu).
* Progresja:
    * wśród załogi ASD Centurion ma reputację szalonej, zarówno w negatywnym spojrzeniu jak i z pewnym podziwem. Co więcej, przetrwała.
    * jej Wzór jest łatwy do zmodyfikowania przez Saitaera. Za dużo razy to już robił i zbyt dobrze ją już zna.


### Chrońmy Karolinę przed uczniami

* **uid:** 190113-chronmy-karoline-przed-uczniami, _numer względny_: 31
* **daty:** 0110-01-05 - 0110-01-06
* **obecni:** Adela Kirys, Arnulf Poważny, Karolina Erenit, Liliana Bankierz, Napoleon Bankierz, Pięknotka Diakon, Teresa Mieralit

Streszczenie:

Karla chciała pozbyć się Pięknotki jeszcze raz. Wysłała na papierkową robotę do Szkoły Magów w Zaczęstwie. Tam okazało się, że kandydat na terminusa (Napoleon) próbował uratować człowieka, Karolinę Erenit przed innymi uczniami szkoły magów. Niestety, eliksir który zamówił u Adeli miał efekty uboczne. Pięknotka gładko to rozwiązała i nikt nie miał problemów. Poza tym, że Karolina nadal nie jest chroniona.

Aktor w Opowieści:

* Dokonanie:
    * poszła do Szkoły Magów zająć się papierkową robotą, skończyła rozwiązując małą Plagę sprowadzoną przypadkowo przez Adelę - przy okazji poznała kilku uczniów.
* Progresja:
    * Arnulf Poważny jej ufa i będzie z nią współpracował zanim wybierze innego terminusa.


### Eksperymenty na wiłach?

* **uid:** 190112-eksperymenty-na-wilach, _numer względny_: 30
* **daty:** 0109-12-28 - 0109-12-31
* **obecni:** Alan Bartozol, Kornel Garn, Pięknotka Diakon, Saitaer, Sławomir Muczarek

Streszczenie:

Alan powiedział Pięknotce, że Myszy polują na wiły dla kogoś. Okazało się, że były wojskowy (Kornel) eksperymentuje na wiłach by stworzyć potężną pryzmatyczną broń defensywną w formie kwiatów. Niestety, to dzieje się na terenie na którym był Saitaer i Władca Rekonstrukcji powrócił. Nadal słaby, ale zintegrował się z Pięknotką i zbudował ołtarz na Trzęsawisku. Kornel uciekł na swój teren a cała akcja skończyła się rozpadem Myszy.

Aktor w Opowieści:

* Dokonanie:
    * poszła tropem przerzucanych w dziwne miejsce wił a skończyła na modlitwie do Saitaera, by tylko rekonstruowana wiła nie zabiła wszystkich...


### A może pustogorska mafia?

* **uid:** 190106-a-moze-pustogorska-mafia, _numer względny_: 29
* **daty:** 0109-12-24 - 0109-12-26
* **obecni:** Adela Kirys, Amadeusz Sowiński, Kasjan Czerwoczłek, Pięknotka Diakon, Roland Grzymość, Tadeusz Rupczak, Waldemar Mózg

Streszczenie:

Adela poprosiła Pięknotkę o uratowanie młodego podkochującego się w niej Kasjana od wydalenia z Pustogoru - to on zrobił atak na Pięknotkę. Niestety, nie dało się tego zrobić - ale Pięknotka oddała go Pietro w Cieniaszczycie. Przy okazji, Pięknotka nie ma surowców na bagna, zapewniła utrzymanie relacji z Czerwonymi Myszami i pozyskanie sponsorów.

Aktor w Opowieści:

* Dokonanie:
    * lawirowała pomiędzy stronnictwami politycznymi - Adela, Kasjan, terminusi, sponsorzy. Bardzo dużo załatwiania i negocjacji. Ale udało jej się odepchnąć Adelę i Kasjana od mafii pustogorskiej.
* Progresja:
    * nie ma większości swoich bagiennych zasobów po ostatniej akcji.


### Turyści na Trzęsawisku

* **uid:** 190105-turysci-na-trzesawisku, _numer względny_: 28
* **daty:** 0109-12-22 - 0109-12-24
* **obecni:** Alan Bartozol, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Grupa turystów spoza Pustogoru odwiedziała Trzęsawisko wraz z trzema łowcami nagród. Pięknotka i Alan poszli ich ratować. W samą porę - sygnał SOS pokazał, że jest już późno. Udało się wszystkich uratować, za co... Pięknotce ktoś zdemolował gabinet. Pięknotka i Alan doszli do pewnego porozumienia w sprawie Adeli - znają jej imię. Acz chyba Alan się o nią martwi.

Aktor w Opowieści:

* Dokonanie:
    * uratowała grupę turystów z Alanem, pokazała swoje zaprzyjaźnione wiły i skończyła przelatując Alana. O tym nie rozmawiają i nikomu nie mówią.
* Progresja:
    * ma grupę zaprzyjaźnionych wił na Trzęsawisku Zjawosztup.


### Stalker i Czerwone Myszy

* **uid:** 190102-stalker-i-czerwone-myszy, _numer względny_: 27
* **daty:** 0109-12-18 - 0109-12-21
* **obecni:** Jan Kramczuk, Pięknotka Diakon, Teresa Mieralit, Waldemar Mózg

Streszczenie:

Myszy napuściły Kramczuka, by ten wlazł nocą do Pięknotki - udało mu się, nie jest zbyt chroniona. Pięknotce spodobał się ów reporter-infiltrator i by się z nim bliżej poznać, przeszła się z nim na Zjawosztup. Tam, Toń pokazała Pięknotce Saitaera i sprzęgła go z nią ponownie. Dodatkowo, Pięknotka pozbyła się wszelkiego terenu a Myszy się podzieliły - część z nich wspiera Adelę i chce, by Adela przerosła Pięknotkę a część wspiera Pięknotkę z nadzieją, że dołączy do Myszy.

Aktor w Opowieści:

* Dokonanie:
    * wyplątała się ze wszelkich wymagań terenowych. Potem zaprosiła stalkera (Jana) na Trzęsawisko i spojrzała w Toń, odzyskując kontakt z Saitaerem.
* Progresja:
    * pozbyła się jakiegokolwiek terenu - za żaden nie odpowiada. Od tej pory jest terminusem lotnym, wykonującym działania dla Pustogoru.
    * znowu poczuła głos Saitaera w jej głowie; ale nie jest to złe. Tym razem nic jej z jego strony nie grozi.


### Morderczyni-jednej-plotki

* **uid:** 190101-morderczyni-jednej-plotki, _numer względny_: 26
* **daty:** 0109-12-13 - 0109-12-17
* **obecni:** Alan Bartozol, Aleksander Iczak, Erwin Galilien, Karol Szurnak, Olaf Zuchwały, Pięknotka Diakon, Teresa Mieralit

Streszczenie:

Na temat Pięknotki i jej salonu zaczęto rozpuszczać nieprzyjemne plotki. Pięknotka zlokalizowała jedno ze źródeł i je pokazowo zniszczyła, zmuszając maga do przepraszania i płaczu na kolanach. Dodatkowo, Czerwone Myszy oraz Dare Shiver zaczęli interesować się Pięknotką i jej salonem. A sama Pięknotka przecięła "największą nemesis" Adeli Kirys.

Aktor w Opowieści:

* Dokonanie:
    * gdy uderzono w reputację jej salonu i w jej pracowników, zrobiła kontratak, pokazówkę, i zniszczyła wszelkie plotki na swój temat. Strachem.
* Progresja:
    * w Pustogorze ma reputację osoby której nie wolno stanąć na drodze, bo go zniszczy. Zrobi krzywdę na kilku poziomach.


### Uwięzienie Saitaera

* **uid:** 181230-uwiezienie-saitaera, _numer względny_: 25
* **daty:** 0109-12-07 - 0109-12-09
* **obecni:** Karla Mrozik, Kreacjusz Diakon, Lucjusz Blakenbauer, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Pięknotka wróciła do Pustogoru, będąc po drodze zaatakowana przez latającego pnączoszpona. Odkryła, że Saitaer poważnie zmienił już Minerwę. Zmartwiła się tym, jak bardzo ta Minerwa różni się od jej przyjaciółki. Ta sprawa jest ponad siły Pięknotki - poszła do Karli, przełożonej Pustogoru. Tam dowiedziała się, że Saitaer to broń i że być może to on stoi za problemami Trzęsawiska Zjawosztup. Pustogor pojmał Saitaera i przejął wskrzeszoną Minerwę do leczenia.

Aktor w Opowieści:

* Dokonanie:
    * podjęła bardzo trudną decyzję - sprawa Saitaera przekracza jej możliwości i kompetencje, patrząc na biedną Minerwę. Oddała temat Karli i Pustogorowi.


### Adieu, Cieniaszczycie

* **uid:** 181227-adieu-cieniaszczycie, _numer względny_: 24
* **daty:** 0109-11-28 - 0109-12-01
* **obecni:** Amadeusz Sowiński, Atena Sowińska, Brygida Maczkowik, Erwin Galilien, Lilia Ursus, Mirela Niecień, Moktar Gradon, Pietro Dwarczan, Pięknotka Diakon, Waleria Cyklon, Zbigniew Burzycki

Streszczenie:

Pięknotka zdobyła nowe ciało dla Minerwy (acz do jego uruchomienia człowiek musi być poświęcony!), przekonała Amadeusza do założenia świątyni Arazille do blokowania Finis Vitae oraz wepchnęła Lilię jako asystentkę Atenie. Do tego rozkochała solidnie Erwina i zamknęła wszystkie wątki, by móc wracać do Pustogoru. Nie dała rady rozdzielić Julii i Łysych Psów, niestety.

Aktor w Opowieści:

* Dokonanie:
    * wyprostowała wszystkie tematy w Cieniaszczycie, uwolniła się od długu Arazille, zdobyła ciało dla Minerwy i poderwała skutecznie Erwina.
* Progresja:
    * uwolniła się od długu u Arazille. Wszystkie tematy w Cieniaszczycie załatwione - wraca do Pustogoru.


### Finis Vitae

* **uid:** 181226-finis-vitae, _numer względny_: 23
* **daty:** 0109-11-18 - 0109-11-27
* **obecni:** Amadeusz Sowiński, Arazille, Atena Sowińska, Moktar Gradon, Pięknotka Diakon, Waleria Cyklon

Streszczenie:

Moktar zregenerował Pięknotkę i powiedział jej, że chce od niej uratowania swojego Skażonego oficera. Pięknotka dowiedziała się od Amadeusza o autowarze Finis Vitae pod Cieniaszczytem; Saitaer nie może nic wiedzieć. Pięknotka doprowadziła do tego, że Lilia wróciła do Cieniaszczytu (courtesy of Moktar), uratowała spod ziemi tego oficera, oraz zobaczyła miejsce, które będzie chyba do końca życia widziała w koszmarach. Gdyby nie Atena, zginęłaby na tej akcji.

Aktor w Opowieści:

* Dokonanie:
    * uratowała Dariusza z macek autowara. Przeszła przez piekło robiąc straszne rzeczy. Celuje w uratowanie Lilii. Sama się nie poznaje.
* Progresja:
    * wzięła dług u Arazille - pomoże jej założyć świątynię blokującą działania autowara Finis Vitae
    * utraciła ochronę Arazille - czas minął. Jest znowu "normalną Pięknotką".
    * jest traktowana jako pomniejszy sojusznik Moktara oraz Łysych Psów przez Moktara i Łyse Psy. One of them, though not completely.


### Czyszczenie toksycznych związków

* **uid:** 181225-czyszczenie-toksycznych-zwiazkow, _numer względny_: 22
* **daty:** 0109-11-13 - 0109-11-17
* **obecni:** Atena Sowińska, Bogdan Szerl, Julia Morwisz, Moktar Gradon, Pięknotka Diakon, Romuald Czurukin

Streszczenie:

Pięknotka ściągnęła Erwina na detoks od Saitaera, acz dała mu informację o Kreacjuszu Diakonie. Saitaer planuje zbudować sprawne ciało dla Minerwy. Atena jest zregenerowana w dość anielskiej formie i dostaje informacje o Orbiterze od Julii. Pięknotka próbuje naprawić pary Romuald x Julia oraz Wioletta x Pietro. Moktar uwalnia Pięknotkę od wpływu Bogdana, masakrując swojego agenta w brutalny sposób.

Aktor w Opowieści:

* Dokonanie:
    * spiknęła ze sobą dwie pary, zapewniła Atenie źródło informacji, ściągnęła Erwina do detoksu od Saitaera i została pokonana przez Moktara gdy broniła Bogdana.
* Progresja:
    * jest "wolna" od wpływu Bogdana. Ona kontroluje tą relację ze swojej strony, choć ma słabość - ale nie jest kontrolowana.
    * Erwin Galilien się w niej zakochał. Nie jest tylko zwykłą przyjaciółką - zostali kochankami.
    * Moktar czuje do niej szacunek. Niechętny szacunek, jako wystarczająco godnego przeciwnika.
    * chce jak najszybciej spreparować ciało dla Minerwy stąd, z Cieniaszczytu. By jak najbardziej ułatwić Minerwie przejście i bycie sobą.


### Tajemniczy ołtarz Moktara

* **uid:** 181218-tajemniczy-oltarz-moktara, _numer względny_: 21
* **daty:** 0109-11-11 - 0109-11-13
* **obecni:** Atena Sowińska, Bogdan Szerl, Moktar Gradon, Pięknotka Diakon, Waleria Cyklon

Streszczenie:

Pięknotka śniła o lustrach; w poszukiwaniu informacji co się dzieje dowiedziała się, że do wyczyszczenia Saitaera wykorzystana jest energia Arazille. O dziwo, Moktar kontroluje energię Arazille a przynajmniej używa jej jako broni. Pięknotka wróciła w ręce Bogdana i przekonała Atenę, by ta poddała się rekonstrukcji - by uwolniła się od Saitaera.

Aktor w Opowieści:

* Dokonanie:
    * dowiedziała się o relacjach między Moktarem, Saitaerem i Arazille (przynajmniej częściowo). Wpadła we współzależność z Bogdanem. Jest szczęśliwa.
* Progresja:
    * potrzebuje Bogdana. Po prostu. To nie jest miłość, to jest uzależnienie. Spirala.
    * przez następne dwa tygodnie jest całkowicie niemożliwe by cokolwiek zdominowało Pięknotkę przez energię Arazille.


### Wolna od terrorforma

* **uid:** 181216-wolna-od-terrorforma, _numer względny_: 20
* **daty:** 0109-11-05 - 0109-11-11
* **obecni:** Atena Sowińska, Bogdan Szerl, Julia Morwisz, Mirela Niecień, Pięknotka Diakon

Streszczenie:

Pięknotka chciała uwolnić się od terrorforma - udało jej się, gdyż oddała się we władzę straszliwego mindwarpa Łysych Psów, Bogdana Szerla. Szerl wraz z kralothami zrekonstruowali Wzór Pięknotki - teraz jest odporna na wpływy terrorforma, ale jednocześnie jest bardzo zmieniona. Sensual, dangerous, chemical. Przy okazji Julia (Emulator) oddała serce Pięknotce a Atena zatrzymała swój przydział na Epirjon.

Aktor w Opowieści:

* Dokonanie:
    * całkowicie przebudowana przez kralothy oraz Bogdana. Obudziły się w niej diakońskie instynkty i stała się jeszcze groźniejsza i piękniejsza. Praktycznie - nowy początek.
* Progresja:
    * pełna przebudowa karty postaci. Główne aspekty: chemiczna, seksowna broń uwodząca. Nadal pozostaje kosmetyczką i terminuską - jest po prostu groźniejsza.
    * straszna słabość do Bogdana Szerla. Trudno jej się mu oprzeć w dowolnej formie.
    * bardzo jej zależy, by jakoś się uodpornić na działania Bogdana Szerla.
    * chce spiknąć ze sobą Pietra i Wiolettę. Skoro ma odejść z terenu, niech przynajmniej zostanie po niej coś fajnego.


### Kajdan na Moktara

* **uid:** 181209-kajdan-na-moktara, _numer względny_: 19
* **daty:** 0109-11-02 - 0109-11-03
* **obecni:** Pietro Dwarczan, Pięknotka Diakon, Waleria Cyklon

Streszczenie:

Pięnotka próbowała osłonić się przed potencjalnym atakiem Moktara i poszła szukać jakiejś jego słabości - i spotkała się w pojedynku snajperów z Walerią Cyklon. Wygrała dzięki lepszemu sprzętowi (power suit), acz skończyła w złym stanie. Wycofała się i Walerię przed atakiem dziwnych nojrepów i wybudziła Walerię; dostała swój dowód, przeskanowała pamięć Walerii i uratowała bazę Moktara przed zniszczeniem przez te dziwne nojrepy. Ogólnie, sukces.

Aktor w Opowieści:

* Dokonanie:
    * pokonała w pojedynku snajperskim Walerię, uzyskała dowód mogący pogrążyć Moktara oraz wyszła na zero - nie ma groźnych wrogów. Pełen sukces.
* Progresja:
    * Moktar i ona nie są już w stanie otwartej wojny. Pięknotka ma hak na Moktara - może zniszczyć Łyse Psy i osłabić jego plany.
    * ma dostęp do ech pamięci Walerii z różnych momentów, czasów i punktów przeszłości.


### Swaty w cieniu potwora

* **uid:** 181125-swaty-w-cieniu-potwora, _numer względny_: 18
* **daty:** 0109-10-29 - 0109-11-01
* **obecni:** Lilia Ursus, Moktar Gradon, Pietro Dwarczan, Pięknotka Diakon, Romuald Czurukin, Waleria Cyklon, Wioletta Kalazar

Streszczenie:

Pięknotka bardzo chciała przespać się z Pietrem; niestety, spotkała Lilię na bazarze i dowiedziała się o dziwnych nojrepach z symbolem Orbitera. Lilia wyraźnie czuje się niedoceniana i pogardzana. Cóż, na szczęście doprowadziła Pięknotkę do Wioletty. Gdy Pietro wyznał (załamanej) Pięknotce że ma miętę do Wioletty, Pięknotka zdecydowała się ich skojarzyć. Lilia jako detektyw miała pomóc. Lilia wpadła jednak w szpony Moktara, który zaczął ją łamać. Pięknotka i Pietro uratowali Lilię, acz Pietro skończył w szpitalu. Pięknotka, ku swemu zdziwieniu, dała radę kogoś poderwać - Wiolettę. To nie były złe trzy dni...

Aktor w Opowieści:

* Dokonanie:
    * dowiedziała się o dziwnych nojrepach, dała radę stawić czoło potwornemu Moktarowi oraz pomogła Wioletcie wygrać taniec z szablami. Potem - poderwała Wiolettę.
* Progresja:
    * zaprzyjaźniła się blisko (łącznie z łóżkiem) z Wiolettą.
    * chce zrozumieć czym jest Moktar Gradon i znaleźć środki, którymi może go unieszkodliwić by nigdy nie miał jej bezradnej w swoich szponach.


### Romuald i Julia

* **uid:** 181118-romuald-i-julia, _numer względny_: 17
* **daty:** 0109-10-27 - 0109-10-29
* **obecni:** Adam Szarjan, Julia Morwisz, Pietro Dwarczan, Pięknotka Diakon, Romuald Czurukin

Streszczenie:

Pięknotka pomogła Romualdowi uratować jego ukochaną Julię. Po drodze był też groźny robot bojowy. Docelowo okazało się, że "Julia" jest Emulatorem - agentem Orbitera Pierwszego której celem było zaprzyjaźnić się z Ateną i Pięknotką by je obserwować. Pięknotka zneutralizowała Emulatora Romualdem i jego miłością plus Pryzmat, acz dwa razy skończyła w szpitalu podczas jednej sesji (rekord).

Aktor w Opowieści:

* Dokonanie:
    * chciała zakosztować rozkoszy w Cieniaszczycie, skończyła w szpitalu dwa razy i pokonała morderczo niebezpieczną Emulator. Taki los.
* Progresja:
    * w Cieniaszczycie Pięknotka ma reputację osoby, która lubi być ciężko poturbowana (kraloth/Zwierz, 2 razy w szpitalu...)
    * chce wzmocnić swój power suit lokalną technologią. Emulator jest w końcu technomantką i inżynierem...


### Odklątwianie Ateny

* **uid:** 181112-odklatwianie-ateny, _numer względny_: 16
* **daty:** 0109-10-23 - 0109-10-26
* **obecni:** Amadeusz Sowiński, Atena Sowińska, Cezary Zwierz, Lucjusz Blakenbauer, Minerwa Diakon, Pięknotka Diakon, Saitaer

Streszczenie:

Atenie się nie poprawia a jej echo atakuje Pięknotkę. By zrozumieć co się dzieje, Pięknotka rozmawia z "terrorformem" (Saitaerem) i dowiaduje się o krwawym klątwożycie. Następnie używając wpływów Adama Szarjana wyrywa Atenę spod opieki lekarza i transportuje ją do Cieniaszczytu, by tam można było jej pomóc. By jej pomóc i odkryć napastnika potrzeba jest moc kralotyczna - Atena się nie zgadza, lecz Pięknotka ją przekonała. Atena ulega rekonstrukcji kralotycznej a Amadeusz ma mniej więcej namiar na twórcę klątwożyta.

Aktor w Opowieści:

* Dokonanie:
    * skłonna uratować Atenę za wszelką cenę - podpadając lekarzom, współpracując z kralothami, nawet z terrorformem. Przechytrzyła wszystkich, wygrała zdrowie Ateny.


### Kotlina Duchów

* **uid:** 181104-kotlina-duchow, _numer względny_: 15
* **daty:** 0109-10-22 - 0109-10-24
* **obecni:** Kirył Najłalmin, Marlena Maja Leszczyńska, Olga Myszeczka, Pięknotka Diakon, Wawrzyn Towarzowski

Streszczenie:

Pięknotka ma dość Zaczęstwa - serio ten teren jej nie pasuje. Po pokonaniu energoforma Marlena zaproponowała jej zmianę regionu na Czarnopalec. Kirył z Epirjona jej pomagał znaleźć tą lokalizację; okazało się, że tamtejszy terminus jest Skażony i służy efemerydzie. Pięknotka z Olgą bohatersko wstrzymały efemerydę aż pojawiło się wsparcie z Pustogoru. Kirył został bohaterem a Pięknotka dostała gwarancję, że tamten teren jest jej (a przynajmniej że Zaczęstwo nie jest jej).

Aktor w Opowieści:

* Dokonanie:
    * chcąc się wyplątać z Zaczęstwa wpadła w świeżo formującą się Efemerydę Kotliny niezauważalną dla Epirjonu. Kupiła z Olgą czas by terminusi uratowali sytuację.
* Progresja:
    * nie odpowiada już za Zaczęstwo. Za to odpowiada za Czarnopalec, Podwiert oraz Żarnię (i okolice)
    * trzy dni w szpitalu - detoks i odkażanie po sprawach z Efemerydą Kotliny Mikarajły


### Neutralizacja artylerii koszmarów

* **uid:** 181114-neutralizacja-artylerii-koszmarow, _numer względny_: 14
* **daty:** 0109-10-20 - 0109-10-21
* **obecni:** Felicja Melitniek, Pięknotka Diakon, Tymon Grubosz, Wiktor Satarail

Streszczenie:

Wiktor Satarail uderzył z Trzęsawiska. Zdecydował się zrobić Artylerię Koszmarów bazując na strachu ludzi z Mekki Wolności w Zaczęstwie by zeń zbudować artylerię na Trzęstawisku. Pięknotka starła się z nim dwa razy i dała mu odejść spokojnie, by tylko wejść na Trzęsawisko, uwieść Wiktora i zneutralizować Pryzmat artylerii. Wiktor docenił jej plan; pozwolił jej odejść i obiecał, że jeszcze poczeka z eliminacją magów Zaczęstwa.

Aktor w Opowieści:

* Dokonanie:
    * Wiktor Satarail miał wszystkie możliwe przewagi, więc "dała mu wygrać" - a potem go uwiodła i zneutralizowała koszmarną moc jego artylerii słodyczą.
* Progresja:
    * Wiktor Satarail ma do niej lekką słabość.
    * uważana przez wielu terminusów za zdecydowanie niepoważną - dała odejść Satarailowi a potem strasznie ryzykowała chcąc rozmontować Artylerię Koszmarów.


### Wojna o uczciwe półfinały

* **uid:** 181101-wojna-o-uczciwe-polfinaly, _numer względny_: 13
* **daty:** 0109-10-17 - 0109-10-19
* **obecni:** Alan Bartozol, Damian Podpalnik, Karolina Erenit, Marlena Maja Leszczyńska, Pięknotka Diakon, Tadeusz Kruszawiecki, Tymon Grubosz

Streszczenie:

Marlena przybyła do Zaczęstwa sprawdzić, czy Zaczęstwiacy oszukują w SupMis. Okazało się, że to ludzie - nie wiedzą o magii i tak, magia im pomaga. Marlena zaczęła próbować im pomóc i wyplątać ich z magii zanim yyizdis zabanuje tą drużynę (konkurencję Marleny). Spowodowała Efekt Skażenia i musiała ją uratować Pięknotka, która zaczęła się zastanawiać jak wplątała się w tą sprawę. Koniec końców się udało - ale Pięknotka dostała w odpowiedzialność zarządzanie Zaczęstwem jako terminuska...

Aktor w Opowieści:

* Dokonanie:
    * knuje, obrabia tyłek Alanowi i trochę wbrew sobie pomaga Marlenie w tych dziwnych grach online, choć zupełnie jej nie rozumie
* Progresja:
    * dostała odpowiedzialność za teren Zaczęstwa, co jej wyjątkowo nie pasuje. Tymon jej będzie pomagał.
    * chce zrzucić z siebie Zaczęstwo i dostać jakiś sensowny teren. JAKIKOLWIEK. Ale nie Zaczęstwo - nie jest technomantką ani katalistką.


### Jeden dokument nie w porę

* **uid:** 181205-jeden-dokument-nie-w-pore, _numer względny_: 12
* **daty:** 0109-10-12 - 0109-10-13
* **obecni:** Anita Wirkot, Pięknotka Diakon, Szymon Grej

Streszczenie:

Pewna czarodziejka chciała oszczędzić kar za gapiostwo z niedostarczeniem dokumentów do księgowości. Kilka Paradoksów później i problem trafił na Pięknotkę - golem z papierów z księgowości i lekko creepy mag nieudacznik jako sojusznik...

Aktor w Opowieści:

* Dokonanie:
    * terminuska która coraz mniej kocha Zaczęstwo; tym razem golem z papierów z księgowości i czarodziej któremu w życiu nic nie wyszło


### Terminuska czy kosmetyczka?

* **uid:** 181027-terminuska-czy-kosmetyczka, _numer względny_: 11
* **daty:** 0109-10-07 - 0109-10-11
* **obecni:** Adela Kirys, Arnulf Poważny, Brygida Maczkowik, Ignacy Myrczek, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Jednoczesny atak na reputację Ateny (przez cybergrzyby) oraz wielki konkurs kosmetyczny w Pustogorze wpłynęły stosunkowo źle na sytuację Pięknotki. Nie rozdwoiła się, ale obie rzeczy udało jej się rozwiązać - acz jest wycieńczona. Odkryła konspirację wymierzoną przeciwko Atenie - ale nie wie kto i gdzie próbuje Atenę usunąć.

Aktor w Opowieści:

* Dokonanie:
    * trudny wybór - terminuska czy kosmetyczka? Pomóc Atenie czy wygrać z Adelą? Uniosła się kompetencjami i wygrała oba, pokazując poziom mistrzowski. Wielki sukces.
* Progresja:
    * pierwsze miejsce w konkursie kosmetyczek w Pustogorze. Najlepsza i najbardziej ceniona. Ciężko na to zapracowała.
    * chce dowiedzieć się, kto do cholery próbuje się pozbyć Ateny Sowińskiej


### Decyzja Minerwy

* **uid:** 181024-decyzja-minerwy, _numer względny_: 10
* **daty:** 0109-10-01 - 0109-10-04
* **obecni:** Adam Szarjan, Adela Kirys, Lilia Ursus, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Atena i Erwin są w ruinie. Tymczasem servar Erwina wyrwał się na wolność. Okazało się, że Minerwa - dawna miłość Erwina - powróciła. Adam próbował ją zneutralizować a Pięknotka - uratować. Skończyło się na tym, że Minerwa, Nutka i terrorform współistnieją w servarze Erwina, Adama boli głowa a Pięknotka cieszy się, że tymczasowo to nie jej problem. Mistrzyni psychotroniki wróciła.

Aktor w Opowieści:

* Dokonanie:
    * oswoiła Minerwę oraz przekonała Adama, by dał jej szansę. Doprowadziła do tego, że Minerwa zdecydowała się zostać.
* Progresja:
    * stosunki Pięknotki i Minerwy się ociepliły


### Powrót Minerwy z terrorforma

* **uid:** 181021-powrot-minerwy-z-terrorforma, _numer względny_: 9
* **daty:** 0109-09-24 - 0109-09-29
* **obecni:** Adam Szarjan, Atena Sowińska, Erwin Galilien, Lilia Ursus, Pięknotka Diakon

Streszczenie:

Adam Szarjan, dawny kolega Erwina pojawił się by uratować go przed problemami z Nutką. Okazało się, że Nutka jest śmiertelnym zagrożeniem pod wpływem energii Nojrepów. Wraz z Ateną i Pięknotką udało im się zneutralizować problem, ale w miejsce Nutki pojawiła się Minerwa - prawdziwa osoba, na bazie której powstała Nutka. Atena i Erwin w bardzo ciężkim stanie trafili do szpitala.

Aktor w Opowieści:

* Dokonanie:
    * przekonywała, knuła, robiła administrację, wciągnęła Atenę... i na końcu jako jedyna jest tylko lekko ranna.
* Progresja:
    * reputacja wyjątkowo niszczycielskiej. Wypożyczyła salę i 2 osoby skończyły krytycznie w szpitalu a as awianów stracił servara.


### Lilia na Trzęsawisku

* **uid:** 181003-lilia-na-trzesawisku, _numer względny_: 8
* **daty:** 0109-09-20 - 0109-09-22
* **obecni:** Erwin Galilien, Lilia Ursus, Pięknotka Diakon

Streszczenie:

Erwin ucieka przed Lilią, bo nie chce wziąć jej do Szczeliny. Pięknotka nie była w stanie pogodzić Erwina i Lilii - wzięła ich więc na Trzęsawisko Zjawosztup polować na Anomalie. Niestety, servar Lili został uszkodzony i wszystkie zyski zjadła naprawa. Przy okazji, wyszła sprawa historii Erwina Galiliena oraz Nutki. I Lilii, która lekko zabujała się w Erwinie.

Aktor w Opowieści:

* Dokonanie:
    * rozpaczliwie łagodzi starcie między Nutką i Lilią; też, główny przewodnik po Trzęsawisku.
* Progresja:
    * ma dostęp do nasion Lis'ha'ora.


### Dwa tygodnie szkoły

* **uid:** 180929-dwa-tygodnie-szkoly, _numer względny_: 7
* **daty:** 0109-09-17 - 0109-09-19
* **obecni:** Adela Kirys, Arnulf Poważny, Atena Sowińska, Erwin Galilien, Felicja Melitniek, Ignacy Myrczek, Miedwied Zajcew, Napoleon Bankierz, Pięknotka Diakon, Roland Grzymość

Streszczenie:

Felicja uczy się w szkole magów, ale nie ma przyjaciół i przez to zachowuje się chaotycznie. Erwin Galilien wpakował się w kłopoty i przegrał power suit "Nutkę" w Kasynie; ma przynieść niebezpieczne grzyby. Uczniowie ze szkoły magów też chodzą do tego Kasyna. Połączone siły Zespołu i Ateny wyplątały Erwina z kłopotów i utrudniły Kasynu zwabianie młodych uczniów ze szkoły magów.

Aktor w Opowieści:

* Dokonanie:
    * współpracowała z Ateną i Erwinem nad odzyskaniem Nutki i nad unieczynnieniem kasyna.


### Inkarnata nienawiści

* **uid:** 180912-inkarnata-nienawisci, _numer względny_: 6
* **daty:** 0109-09-14 - 0109-09-15
* **obecni:** Atena Sowińska, Brygida Maczkowik, Pięknotka Diakon, Staś Kruszawiecki

Streszczenie:

Pięknotka dowiedziała się, że Atena Sowińska wylądowała na zesłaniu z Epirjona. Pojechała jej pomóc (kontrolować przerażająca komendant Epirjona). Na miejscu rozwiązały problem z viciniusem przybyłym ze Zjawosztup oraz się okazało, że Atena tak koszmarną postacią nie jest.

Aktor w Opowieści:

* Dokonanie:
    * przede wszystkim, kontrolowała Atenę i dowiedziała się mnóstwo zaskakujących rzeczy na jej temat. Trochę się zaprzyjaźniła z Ateną. Trochę.


### Nikt nie śpi w swoim łóżku

* **uid:** 180906-nikt-nie-spi-w-swoim-lozku, _numer względny_: 5
* **daty:** 0109-09-09 - 0109-09-11
* **obecni:** Atena Sowińska, Brygida Maczkowik, Grażyna Sirwąg, Mieszko Weiner, Pięknotka Diakon, Szczepan Mensic, Tadeusz Kruszawiecki

Streszczenie:

W Zaczęstwie pojawił się dziwny duch - kiedyś policjant Wąsacz, teraz karze ludzi za wykroczenia śmiercią. Pięknotka i Mieszko skupili się na jego usunięciu. Okazało się, że dyrektor Cyberszkoły jest creepy, sypia z Brygidą i przez to prawie zginął. Zespół pojmał ducha i zaklął go w pluszaka, przedtem zatrzymując działania klubu okultystycznego. Dodatkowo, Atena wyleciała ze stacji Epirjon. Aha, nikt tu nie śpi w swoim łóżku.

Aktor w Opowieści:

* Dokonanie:
    * wyciągnęła prawdę z Grażyny i Szczepana, uspokoiła Brygidę a potem ducha Wąsacza i przekonała ducha, by dał się zapuszkować Mieszkowi.


### Protomag z Trzęsawisk

* **uid:** 180817-protomag-z-trzesawisk, _numer względny_: 4
* **daty:** 0109-09-07 - 0109-09-09
* **obecni:** Alan Bartozol, Atena Sowińska, Erwin Galilien, Felicja Melitniek, Lucjusz Blakenbauer, Miedwied Zajcew, Pięknotka Diakon

Streszczenie:

Protomag skrzywdził kierowcę z lokalnej mafii Zajcewów. Miedwied i Pięknotka poszli szukać Felicji - protomaga na której eksperymentowali - na Trzęsawisku Zjawosztup. Tam napotkali na terminusów Czerwonych Myszy którzy też chcieli Felicję. Współpraca Zespołu z Ateną Sowińską doprowadziła do przekazania Felicji im i Miedwied został jej opiekunem. Niestety, Trzęsawisko w wyniku pojawienia się Felicji się rozpaliło - jest gorące i będzie emitować Skażeńce...

Aktor w Opowieści:

* Dokonanie:
    * neutralizowała Czerwone Myszy dając czas Miedwiedowi, potem użyła kruczków prawnych i Ateny by ich całkowicie wyrolować.
* Progresja:
    * zrobiła sobie wroga w Czerwonych Myszach (dokładnie: Alanie Bartozolu), bo wymanewrowała Alana ze zdobycia Felicji.


### Programista mimo woli

* **uid:** 180821-programista-mimo-woli, _numer względny_: 3
* **daty:** 0109-09-06 - 0109-09-07
* **obecni:** Atena Sowińska, Bronisława Strzelczyk, Pięknotka Diakon, Staś Kruszawiecki, Tadeusz Kruszawiecki

Streszczenie:

Na terenie Zaczęstwa doszło do Emisji i pojawiła się efemeryda. Próbując ją usunąć, magowie stworzyli Cyberszkołę w Zaczęstwie podpiętą energetycznie do Trzęsawiska Zjawosztup. Efemeryda była podpięta do dziecka - małego Stasia. By ją usunąć, dyrektor Cyberszkoły i ojciec Stasia musiał zrezygnować ze zmuszenia syna do programowania. Niech rysuje. Viva magia mentalna ;-).

Aktor w Opowieści:

* Dokonanie:
    * wraz z Bronką rozwiązały problem efemerydy; mentalnie zmusiła dyrektora szkoły by ten pozwolił dziecku rozwijać rysowanie (jak Staś chciał)


### Komary i kosmetyki

* **uid:** 180815-komary-i-kosmetyki, _numer względny_: 2
* **daty:** 0109-09-02 - 0109-09-05
* **obecni:** Adela Kirys, Erwin Galilien, Pięknotka Diakon

Streszczenie:

Adela Kirys wpadła w kłopoty. Stworzyła komary napromieniowujące ludzi na terenie Czystym. Sęk w tym, że poszlaki wskazywały na Pięknotkę. Pięknotka ostrzeżona przez Galiliena odkryła problem - po czym poprosiła Erwina o Skażenie kremu tworzącego te komary (włam + podrzut). Sukces - Adela musiała wycofać całą partię a reputacja Pięknotki nie ucierpiała. A i Nurek Szczeliny się przydał.

Aktor w Opowieści:

* Dokonanie:
    * próbowała dojść do tego skąd pojawiły się magiczne komary i znalazła Adelę. Adela nie chciała po dobroci - więc poprosiła, by Erwin rozwiązał dla niej problem.
* Progresja:
    * jej reputacja nie ucierpiała i jej przeszłość - tworzenie problematycznych komarów - poszła w niepamięć w Pustogorze


### Trufle z kosmosu

* **uid:** 180814-trufle-z-kosmosu, _numer względny_: 1
* **daty:** 0109-08-30 - 0109-09-01
* **obecni:** Arkadiusz Mocarny, Bronisława Strzelczyk, Jakub Wirus, Kirył Najłalmin, Pięknotka Diakon, Roman Kłębek

Streszczenie:

Na stacji Epirjon doszło do błędu - magiczne trufle uderzyły w Żarnię. Reporter Jakub uratował Romana od kwiatów sztucznych przed samowysadzeniem, po czym zauważył heatmapę problemu i ściągnął terminuski. Terminuski znalazły maga w stealth suicie, pojmały go i poznały prawdę. W wyniku tego, Bronia oprowadziła Olgowe glukszwajny po okolicy by usunąć problem z truflami raz na zawsze.

Aktor w Opowieści:

* Dokonanie:
    * wyciągnęła Kiryła na pożądanie z jego ufortyfikowanego pokoju prosto na jego własną minę.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 89, @: 0110-10-31
    1. Primus    : 89, @: 0110-10-31
        1. Sektor Astoriański    : 89, @: 0110-10-31
            1. Astoria, Orbita    : 1, @: 0109-09-01
                1. Stacja Orbitalna Epirjon    : 1, @: 0109-09-01
            1. Astoria    : 89, @: 0110-10-31
                1. Sojusz Letejski, NW    : 7, @: 0109-12-01
                    1. Ruiniec    : 7, @: 0109-12-01
                        1. Colubrinus Meditech    : 3, @: 0109-11-17
                        1. Colubrinus Psiarnia    : 2, @: 0109-12-01
                        1. Diamentowa Forteca    : 1, @: 0109-11-01
                        1. Skalny Labirynt    : 2, @: 0109-11-27
                        1. Studnia Bez Dna    : 1, @: 0109-11-27
                        1. Świątynia Bez Dna    : 1, @: 0109-12-01
                1. Sojusz Letejski, SW    : 6, @: 0110-05-21
                    1. Granica Anomalii    : 6, @: 0110-05-21
                        1. Pacyfika, obrzeża    : 2, @: 0110-04-20
                        1. Pacyfika    : 1, @: 0110-04-09
                        1. Skałopływ    : 2, @: 0110-03-09
                            1. Herbast    : 1, @: 0110-03-06
                            1. Hotel Pustogorski    : 1, @: 0110-03-06
                            1. Jezioro Macek    : 1, @: 0110-03-06
                            1. Martwy Step    : 1, @: 0110-03-06
                            1. Osiedle Bezpieczne    : 1, @: 0110-03-06
                            1. Podfarma    : 1, @: 0110-03-06
                            1. Strażnica    : 1, @: 0110-03-06
                        1. Wolne Ptaki    : 2, @: 0110-05-21
                            1. Królewska Baza    : 2, @: 0110-05-21
                1. Sojusz Letejski    : 86, @: 0110-10-31
                    1. Przelotyk    : 10, @: 0110-06-15
                        1. Przelotyk Wschodni    : 10, @: 0110-06-15
                            1. Cieniaszczyt    : 10, @: 0110-06-15
                                1. Arena Nadziei Tęczy    : 1, @: 0109-11-01
                                1. Bazar Wschodu Astorii    : 1, @: 0109-11-01
                                1. Knajpka Szkarłatny Szept    : 5, @: 0110-06-15
                                1. Kompleks Nukleon    : 8, @: 0110-06-15
                                1. Mordownia Czaszka Kralotha    : 1, @: 0109-11-01
                                1. Mrowisko    : 5, @: 0109-12-01
                                1. Pałac Szkarłatnego Światła    : 1, @: 0109-10-26
                                1. Wewnątrzzbocze Zachodnie    : 1, @: 0109-10-29
                                    1. Panorama Światła    : 1, @: 0109-10-29
                            1. Przejściak    : 1, @: 0110-06-15
                                1. Hotel Pirat    : 1, @: 0110-06-15
                    1. Szczeliniec    : 77, @: 0110-10-31
                        1. Powiat Jastrzębski    : 8, @: 0110-10-31
                            1. Jastrząbiec, okolice    : 2, @: 0110-07-27
                                1. Containment Chambers    : 1, @: 0110-07-15
                                1. Klinika Iglica    : 2, @: 0110-07-27
                                    1. Kompleks Itaran    : 2, @: 0110-07-27
                                1. TechBunkier Sarrat    : 1, @: 0110-07-27
                            1. Jastrząbiec    : 1, @: 0110-07-05
                                1. Hotel Stacja Kosmiczna    : 1, @: 0110-07-05
                                1. Ratusz    : 1, @: 0110-07-05
                            1. Kalbark    : 3, @: 0110-08-01
                                1. Autoklub Piękna    : 2, @: 0110-08-01
                                1. Escape Room Lustereczko    : 1, @: 0110-07-27
                                1. Mini Barbakan    : 1, @: 0110-08-01
                            1. Praszalek, okolice    : 3, @: 0110-10-31
                                1. Fabryka schronień Defensor    : 1, @: 0110-10-10
                                1. Lasek Janor    : 2, @: 0110-10-31
                                1. Park rozrywki Janor    : 3, @: 0110-10-31
                            1. Praszalek    : 1, @: 0110-10-10
                                1. Komenda policji    : 1, @: 0110-10-10
                        1. Powiat Pustogorski    : 68, @: 0110-10-31
                            1. Czarnopalec    : 3, @: 0110-06-11
                                1. Kotlina Mikarajły    : 1, @: 0109-10-24
                                1. Pusta Wieś    : 3, @: 0110-06-11
                            1. Czemerta, okolice    : 2, @: 0110-09-11
                                1. Baza Irrydius    : 1, @: 0110-09-11
                                1. Fortifarma Irrydia    : 2, @: 0110-09-11
                                1. Studnia Irrydiańska    : 2, @: 0110-09-11
                            1. Czemerta    : 1, @: 0110-08-21
                            1. Czółenko    : 5, @: 0110-07-03
                                1. Bunkry    : 4, @: 0110-07-03
                                1. Tancbuda    : 1, @: 0110-05-09
                            1. Kramamcz    : 1, @: 0110-06-19
                                1. Włóknin    : 1, @: 0110-06-19
                            1. Podwiert, obrzeża    : 3, @: 0110-04-20
                                1. Kompleks Badawczy Skelidar    : 1, @: 0110-03-17
                                1. Kosmoport    : 2, @: 0110-04-20
                            1. Podwiert, okolice    : 3, @: 0110-07-02
                                1. Bioskładowisko podziemne    : 3, @: 0110-07-02
                            1. Podwiert    : 14, @: 0110-09-16
                                1. Bastion Pustogoru    : 1, @: 0110-04-24
                                1. Bunkier Rezydenta    : 1, @: 0110-01-15
                                1. Dolina Biurowa    : 1, @: 0110-05-15
                                1. Iglice Nadziei    : 1, @: 0110-06-07
                                    1. Posiadłość Arieników    : 1, @: 0110-06-07
                                1. Klub Arkadia    : 1, @: 0110-09-16
                                1. Komenda policji    : 1, @: 0109-10-24
                                1. Kopalnia Terposzy    : 1, @: 0109-12-09
                                1. Magazyny sprzętu ciężkiego    : 3, @: 0110-07-02
                                1. Odlewnia    : 1, @: 0110-04-24
                                1. Osiedle Leszczynowe    : 3, @: 0110-06-09
                                    1. Szkoła Nowa    : 2, @: 0110-06-09
                                1. Osiedle Tęczy    : 1, @: 0110-08-28
                                1. Sensoplex    : 4, @: 0110-08-28
                            1. Przywiesław    : 1, @: 0109-09-05
                                1. Przychodnia    : 1, @: 0109-09-05
                            1. Pustogor, okolice    : 1, @: 0110-10-31
                                1. Rezydencja Blakenbauerów    : 1, @: 0110-10-31
                            1. Pustogor    : 30, @: 0110-09-11
                                1. Barbakan    : 7, @: 0110-01-26
                                1. Eksterior    : 10, @: 0110-09-11
                                    1. Arena Szalonego Króla    : 1, @: 0109-10-11
                                    1. Dolina Uciech    : 2, @: 0110-06-22
                                    1. Fort Mikado    : 2, @: 0110-06-22
                                    1. Miasteczko    : 9, @: 0110-09-11
                                        1. Knajpa Górska Szalupa    : 4, @: 0110-09-11
                                    1. Zamek Weteranów    : 1, @: 0110-08-14
                                1. Gabinet Pięknotki    : 8, @: 0110-09-04
                                1. Interior    : 6, @: 0110-04-20
                                    1. Bunkry Barbakanu    : 3, @: 0110-04-20
                                    1. Dzielnica Mieszkalna    : 1, @: 0110-04-03
                                    1. Inkubator Ekonomiczny Samszarów    : 1, @: 0109-10-04
                                    1. Laboratorium Senetis    : 3, @: 0110-04-13
                                1. Kawiarenka Ciemna Strona    : 1, @: 0110-01-26
                                1. Knajpa Górska Szalupa    : 3, @: 0110-09-04
                                1. Kompleks Testowy    : 1, @: 0109-09-29
                                1. Miasteczko    : 3, @: 0110-03-09
                                1. Pustułka    : 1, @: 0109-09-29
                                1. Rdzeń    : 11, @: 0110-09-11
                                    1. Barbakan    : 3, @: 0110-04-05
                                    1. Szpital Terminuski    : 9, @: 0110-09-11
                            1. Trzęsawisko Zjawosztup    : 2, @: 0109-10-11
                            1. Zaczęstwo, obrzeża    : 1, @: 0110-04-20
                            1. Zaczęstwo    : 31, @: 0110-08-26
                                1. Akademia Magii, kampus    : 8, @: 0110-08-26
                                    1. Budynek Centralny    : 5, @: 0110-08-26
                                        1. Skrzydło Loris    : 1, @: 0110-04-26
                                    1. Domek dyrektora    : 1, @: 0110-02-20
                                1. Arena Migświatła    : 1, @: 0110-02-10
                                1. Biurowce    : 1, @: 0109-10-13
                                    1. Biurowiec Formael    : 1, @: 0109-10-13
                                1. Cyberszkoła    : 11, @: 0110-08-26
                                1. Hotel Tellur    : 1, @: 0110-02-23
                                1. Kasyno Marzeń    : 1, @: 0109-09-19
                                1. Kawiarenka Leopold    : 1, @: 0110-05-21
                                1. Klub Poetycki Sucharek    : 1, @: 0109-09-11
                                1. Kompleks Tiamenat    : 1, @: 0110-01-21
                                1. Kwatera Terminusa    : 1, @: 0110-02-20
                                1. Mekka Wolności    : 1, @: 0109-10-21
                                1. Nieużytki Staszka    : 14, @: 0110-08-26
                                1. Osiedle Ptasie    : 6, @: 0110-07-03
                                1. Sypialnia Szczelińca    : 1, @: 0110-08-26
                                1. Wschodnie Pole Namiotowe    : 1, @: 0110-02-23
                                1. Złomowisko    : 1, @: 0110-03-04
                            1. Żarnia    : 2, @: 0110-01-15
                                1. Żernia    : 2, @: 0110-01-15
                        1. Powiat Złotodajski    : 1, @: 0110-07-09
                            1. Złotkordza    : 1, @: 0110-07-09
                                1. Stadion    : 1, @: 0110-07-09
                        1. Pustogor    : 1, @: 0110-07-01
                        1. Trzęsawisko Zjawosztup    : 17, @: 0110-10-04
                            1. Accadorian    : 1, @: 0109-12-24
                            1. Głodna Ziemia    : 3, @: 0110-01-30
                            1. Laboratorium W Drzewie    : 3, @: 0110-08-21
                            1. Sfera Pyłu    : 1, @: 0109-12-24
                            1. Toń Pustki    : 2, @: 0109-12-24

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Minerwa Metalia      | 25 | ((181024-decyzja-minerwy; 181027-terminuska-czy-kosmetyczka; 181230-uwiezienie-saitaera; 190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei; 190402-eksperymentalny-power-suit; 190424-budowa-ixionskiego-mimika; 190427-zrzut-w-pacyfice; 190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 190503-bardzo-nieudane-porwania; 190828-migswiatlo-psychotroniczek; 190901-polowanie-na-pieknotke; 191103-kontrpolowanie-pieknotki-pulapka; 191112-korupcja-z-arystokratki; 191201-ukradziony-entropik; 200202-krucjata-chevaleresse; 200425-inflitrator-poluje-na-tai-minerwy; 200502-po-co-atakuja-minerwe; 200623-adaptacja-azalii; 201011-narodziny-paladynki-saitaera; 201025-kraloth-w-parku-janor)) |
| Erwin Galilien       | 20 | ((180815-komary-i-kosmetyki; 180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly; 181003-lilia-na-trzesawisku; 181021-powrot-minerwy-z-terrorforma; 181227-adieu-cieniaszczycie; 190101-morderczyni-jednej-plotki; 190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 190210-minerwa-i-kwiaty-nadziei; 190402-eksperymentalny-power-suit; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190427-zrzut-w-pacyfice; 190429-sabotaz-szeptow-elizy; 190527-mimik-sni-o-esuriit; 190817-kwiaty-w-sluzbie-puryfikacji; 191105-zaginiona-soniczka; 200425-inflitrator-poluje-na-tai-minerwy; 200509-rekin-z-aurum-i-fortifarma)) |
| Alan Bartozol        | 16 | ((180817-protomag-z-trzesawisk; 181101-wojna-o-uczciwe-polfinaly; 190101-morderczyni-jednej-plotki; 190105-turysci-na-trzesawisku; 190112-eksperymenty-na-wilach; 190116-wypalenie-saitaera-z-trzesawiska; 190217-chevaleresse; 190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 190419-osopokalipsa-wiktora; 190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 190505-szczur-ktory-chroni; 190830-kto-wrobil-alana; 190901-polowanie-na-pieknotke; 200913-haracz-w-parku-rozrywki)) |
| Atena Sowińska       | 15 | ((180817-protomag-z-trzesawisk; 180821-programista-mimo-woli; 180906-nikt-nie-spi-w-swoim-lozku; 180912-inkarnata-nienawisci; 180929-dwa-tygodnie-szkoly; 181021-powrot-minerwy-z-terrorforma; 181112-odklatwianie-ateny; 181216-wolna-od-terrorforma; 181218-tajemniczy-oltarz-moktara; 181225-czyszczenie-toksycznych-zwiazkow; 181226-finis-vitae; 181227-adieu-cieniaszczycie; 190210-minerwa-i-kwiaty-nadziei; 190213-wygasniecie-starego-autosenta; 190330-polowanie-na-ataienne)) |
| Mariusz Trzewń       | 12 | ((190827-rozpaczliwe-ratowanie-bii; 190901-polowanie-na-pieknotke; 190906-wypadek-w-kramamczu; 191103-kontrpolowanie-pieknotki-pulapka; 191105-zaginiona-soniczka; 191112-korupcja-z-arystokratki; 191201-ukradziony-entropik; 200202-krucjata-chevaleresse; 200414-arystokraci-na-trzesawisku; 200510-tajna-baza-orbitera; 200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Tymon Grubosz        | 12 | ((181101-wojna-o-uczciwe-polfinaly; 181114-neutralizacja-artylerii-koszmarow; 190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy; 190217-chevaleresse; 190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek; 191105-zaginiona-soniczka; 200222-rozbrojenie-bomby-w-kalbarku; 200510-tajna-baza-orbitera)) |
| Wiktor Satarail      | 12 | ((181114-neutralizacja-artylerii-koszmarow; 190105-turysci-na-trzesawisku; 190116-wypalenie-saitaera-z-trzesawiska; 190119-skorpipedy-krolewskiego-xirathira; 190127-ixionski-transorganik; 190406-bardzo-kosztowne-lzy; 190419-osopokalipsa-wiktora; 190505-szczur-ktory-chroni; 190527-mimik-sni-o-esuriit; 190721-kirasjerka-najgorszym-detektywem; 200418-wojna-trzesawiska; 200623-adaptacja-azalii)) |
| Adela Kirys          | 10 | ((180815-komary-i-kosmetyki; 180929-dwa-tygodnie-szkoly; 181024-decyzja-minerwy; 181027-terminuska-czy-kosmetyczka; 190106-a-moze-pustogorska-mafia; 190113-chronmy-karoline-przed-uczniami; 190119-skorpipedy-krolewskiego-xirathira; 190202-czarodziejka-z-woli-saitaera; 190505-szczur-ktory-chroni; 190527-mimik-sni-o-esuriit)) |
| Diana Tevalier       | 10 | ((190217-chevaleresse; 190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 190429-sabotaz-szeptow-elizy; 190830-kto-wrobil-alana; 190901-polowanie-na-pieknotke; 200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku; 200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Ernest Kajrat        | 10 | ((190505-szczur-ktory-chroni; 190519-uciekajacy-seksbot; 190616-anomalna-serafina; 190622-wojna-kajrata; 190623-noc-kajrata; 190709-somnibel-uciekl-arienikom; 190714-kult-choroba-esuriit; 190726-bardzo-niebezpieczne-skladowisko; 190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Gabriel Ursus        | 9 | ((190726-bardzo-niebezpieczne-skladowisko; 190804-niespodziewany-wplyw-aidy; 190917-zagubiony-efemerydyta; 200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200425-inflitrator-poluje-na-tai-minerwy; 200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera; 201011-narodziny-paladynki-saitaera)) |
| Lucjusz Blakenbauer  | 9 | ((180817-protomag-z-trzesawisk; 181112-odklatwianie-ateny; 181230-uwiezienie-saitaera; 190330-polowanie-na-ataienne; 190429-sabotaz-szeptow-elizy; 190505-szczur-ktory-chroni; 190901-polowanie-na-pieknotke; 200222-rozbrojenie-bomby-w-kalbarku; 201025-kraloth-w-parku-janor)) |
| Olaf Zuchwały        | 9 | ((190101-morderczyni-jednej-plotki; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 190622-wojna-kajrata; 190917-zagubiony-efemerydyta; 191103-kontrpolowanie-pieknotki-pulapka; 200417-nawolywanie-trzesawiska)) |
| Ataienne             | 8 | ((190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 190402-eksperymentalny-power-suit; 190817-kwiaty-w-sluzbie-puryfikacji; 191105-zaginiona-soniczka; 191112-korupcja-z-arystokratki; 200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Karla Mrozik         | 8 | ((181230-uwiezienie-saitaera; 190120-nowa-minerwa-w-nowym-swiecie; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy; 190503-bardzo-nieudane-porwania; 200418-wojna-trzesawiska; 200623-adaptacja-azalii)) |
| Saitaer              | 8 | ((181112-odklatwianie-ateny; 190112-eksperymenty-na-wilach; 190116-wypalenie-saitaera-z-trzesawiska; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190519-uciekajacy-seksbot; 190804-niespodziewany-wplyw-aidy; 201011-narodziny-paladynki-saitaera)) |
| Ignacy Myrczek       | 7 | ((180929-dwa-tygodnie-szkoly; 181027-terminuska-czy-kosmetyczka; 191112-korupcja-z-arystokratki; 200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200510-tajna-baza-orbitera)) |
| Karolina Erenit      | 7 | ((181101-wojna-o-uczciwe-polfinaly; 190113-chronmy-karoline-przed-uczniami; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy; 190217-chevaleresse; 190830-kto-wrobil-alana)) |
| Sabina Kazitan       | 7 | ((191112-korupcja-z-arystokratki; 200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera; 201011-narodziny-paladynki-saitaera)) |
| Kasjopea Maus        | 6 | ((190116-wypalenie-saitaera-z-trzesawiska; 190120-nowa-minerwa-w-nowym-swiecie; 190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei; 190419-osopokalipsa-wiktora; 190906-wypadek-w-kramamczu)) |
| Lilia Ursus          | 6 | ((181003-lilia-na-trzesawisku; 181021-powrot-minerwy-z-terrorforma; 181024-decyzja-minerwy; 181125-swaty-w-cieniu-potwora; 181227-adieu-cieniaszczycie; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Moktar Gradon        | 6 | ((181125-swaty-w-cieniu-potwora; 181218-tajemniczy-oltarz-moktara; 181225-czyszczenie-toksycznych-zwiazkow; 181226-finis-vitae; 181227-adieu-cieniaszczycie; 190724-odzyskana-agentka-orbitera)) |
| Tomasz Tukan         | 6 | ((190519-uciekajacy-seksbot; 190616-anomalna-serafina; 190709-somnibel-uciekl-arienikom; 190714-kult-choroba-esuriit; 200202-krucjata-chevaleresse; 200425-inflitrator-poluje-na-tai-minerwy)) |
| Arnulf Poważny       | 5 | ((180929-dwa-tygodnie-szkoly; 181027-terminuska-czy-kosmetyczka; 190113-chronmy-karoline-przed-uczniami; 190206-nie-da-sie-odrzucic-mocy; 190519-uciekajacy-seksbot)) |
| Julia Morwisz        | 5 | ((181118-romuald-i-julia; 181216-wolna-od-terrorforma; 181225-czyszczenie-toksycznych-zwiazkow; 190724-odzyskana-agentka-orbitera; 190804-niespodziewany-wplyw-aidy)) |
| Talia Aegis          | 5 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek; 190830-kto-wrobil-alana; 200202-krucjata-chevaleresse; 200414-arystokraci-na-trzesawisku)) |
| Teresa Mieralit      | 5 | ((190101-morderczyni-jednej-plotki; 190102-stalker-i-czerwone-myszy; 190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 200510-tajna-baza-orbitera)) |
| Waleria Cyklon       | 5 | ((181125-swaty-w-cieniu-potwora; 181209-kajdan-na-moktara; 181218-tajemniczy-oltarz-moktara; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Adam Szarjan         | 4 | ((181021-powrot-minerwy-z-terrorforma; 181024-decyzja-minerwy; 181118-romuald-i-julia; 190724-odzyskana-agentka-orbitera)) |
| Amadeusz Sowiński    | 4 | ((181112-odklatwianie-ateny; 181226-finis-vitae; 181227-adieu-cieniaszczycie; 190106-a-moze-pustogorska-mafia)) |
| Brygida Maczkowik    | 4 | ((180906-nikt-nie-spi-w-swoim-lozku; 180912-inkarnata-nienawisci; 181027-terminuska-czy-kosmetyczka; 181227-adieu-cieniaszczycie)) |
| Kornel Garn          | 4 | ((190112-eksperymenty-na-wilach; 190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei; 190817-kwiaty-w-sluzbie-puryfikacji)) |
| Liliana Bankierz     | 4 | ((190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 190622-wojna-kajrata; 190623-noc-kajrata)) |
| Marek Puszczok       | 4 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek; 190917-zagubiony-efemerydyta; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Marlena Maja Leszczyńska | 4 | ((181101-wojna-o-uczciwe-polfinaly; 181104-kotlina-duchow; 190206-nie-da-sie-odrzucic-mocy; 190217-chevaleresse)) |
| Mirela Orion         | 4 | ((190503-bardzo-nieudane-porwania; 190721-kirasjerka-najgorszym-detektywem; 190724-odzyskana-agentka-orbitera; 190726-bardzo-niebezpieczne-skladowisko)) |
| Napoleon Bankierz    | 4 | ((180929-dwa-tygodnie-szkoly; 190113-chronmy-karoline-przed-uczniami; 190127-ixionski-transorganik; 200417-nawolywanie-trzesawiska)) |
| Nikola Kirys         | 4 | ((190427-zrzut-w-pacyfice; 190502-pierwszy-emulator-orbitera; 190503-bardzo-nieudane-porwania; 190622-wojna-kajrata)) |
| Pietro Dwarczan      | 4 | ((181118-romuald-i-julia; 181125-swaty-w-cieniu-potwora; 181209-kajdan-na-moktara; 181227-adieu-cieniaszczycie)) |
| Roland Grzymość      | 4 | ((180929-dwa-tygodnie-szkoly; 190106-a-moze-pustogorska-mafia; 190726-bardzo-niebezpieczne-skladowisko; 191201-ukradziony-entropik)) |
| Amanda Kajrat        | 3 | ((190714-kult-choroba-esuriit; 190726-bardzo-niebezpieczne-skladowisko; 190804-niespodziewany-wplyw-aidy)) |
| Bogdan Szerl         | 3 | ((181216-wolna-od-terrorforma; 181218-tajemniczy-oltarz-moktara; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Damian Orion         | 3 | ((190503-bardzo-nieudane-porwania; 191103-kontrpolowanie-pieknotki-pulapka; 200202-krucjata-chevaleresse)) |
| Felicja Melitniek    | 3 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly; 181114-neutralizacja-artylerii-koszmarow)) |
| Kirył Najłalmin      | 3 | ((180814-trufle-z-kosmosu; 181104-kotlina-duchow; 190127-ixionski-transorganik)) |
| Laura Tesinik        | 3 | ((200425-inflitrator-poluje-na-tai-minerwy; 200510-tajna-baza-orbitera; 200524-dom-dla-melindy)) |
| Mateusz Kardamacz    | 3 | ((191105-zaginiona-soniczka; 200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Mirela Niecień       | 3 | ((181216-wolna-od-terrorforma; 181227-adieu-cieniaszczycie; 190724-odzyskana-agentka-orbitera)) |
| Olga Myszeczka       | 3 | ((181104-kotlina-duchow; 190119-skorpipedy-krolewskiego-xirathira; 190721-kirasjerka-najgorszym-detektywem)) |
| Ossidia Saitis       | 3 | ((190519-uciekajacy-seksbot; 190623-noc-kajrata; 190804-niespodziewany-wplyw-aidy)) |
| Romuald Czurukin     | 3 | ((181118-romuald-i-julia; 181125-swaty-w-cieniu-potwora; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Serafina Ira         | 3 | ((190616-anomalna-serafina; 190622-wojna-kajrata; 190623-noc-kajrata)) |
| Sławomir Muczarek    | 3 | ((190112-eksperymenty-na-wilach; 190119-skorpipedy-krolewskiego-xirathira; 190202-czarodziejka-z-woli-saitaera)) |
| Strażniczka Alair    | 3 | ((200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska; 200425-inflitrator-poluje-na-tai-minerwy)) |
| Tadeusz Kruszawiecki | 3 | ((180821-programista-mimo-woli; 180906-nikt-nie-spi-w-swoim-lozku; 181101-wojna-o-uczciwe-polfinaly)) |
| Waldemar Mózg        | 3 | ((190102-stalker-i-czerwone-myszy; 190106-a-moze-pustogorska-mafia; 190119-skorpipedy-krolewskiego-xirathira)) |
| Aida Serenit         | 2 | ((190721-kirasjerka-najgorszym-detektywem; 190724-odzyskana-agentka-orbitera)) |
| Aleksander Rugczuk   | 2 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Antoni Kotomin       | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Artur Kołczond       | 2 | ((200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera)) |
| Artur Michasiewicz   | 2 | ((190828-migswiatlo-psychotroniczek; 200509-rekin-z-aurum-i-fortifarma)) |
| Baltazar Rączniak    | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Bronisława Strzelczyk | 2 | ((180814-trufle-z-kosmosu; 180821-programista-mimo-woli)) |
| Dariusz Bankierz     | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Eliza Ira            | 2 | ((190429-sabotaz-szeptow-elizy; 190519-uciekajacy-seksbot)) |
| Jadwiga Pszarnik     | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Józef Małmałaz       | 2 | ((190901-polowanie-na-pieknotke; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Kasjan Czerwoczłek   | 2 | ((190106-a-moze-pustogorska-mafia; 190505-szczur-ktory-chroni)) |
| Keraina d'Orion      | 2 | ((191201-ukradziony-entropik; 200202-krucjata-chevaleresse)) |
| Kreacjusz Diakon     | 2 | ((181230-uwiezienie-saitaera; 200502-po-co-atakuja-minerwe)) |
| Krystian Namałłek    | 2 | ((190505-szczur-ktory-chroni; 190616-anomalna-serafina)) |
| Ksenia Kirallen      | 2 | ((190906-wypadek-w-kramamczu; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Melinda Teilert      | 2 | ((200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Miedwied Zajcew      | 2 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly)) |
| Natalia Tessalon     | 2 | ((200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera)) |
| Persefona d'Jastrząbiec | 2 | ((191112-korupcja-z-arystokratki; 191201-ukradziony-entropik)) |
| Staś Kruszawiecki    | 2 | ((180821-programista-mimo-woli; 180912-inkarnata-nienawisci)) |
| Szymon Oporcznik     | 2 | ((190331-kurz-po-ataienne; 190402-eksperymentalny-power-suit)) |
| Wojtek Kurczynos     | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Adam Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Adrian Wężoskór      | 1 | ((190406-bardzo-kosztowne-lzy)) |
| Agaton Ociegor       | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Aleksander Iczak     | 1 | ((190101-morderczyni-jednej-plotki)) |
| Aleksander Muniakiewicz | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Aleksandra Szklarska | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Almeda Literna       | 1 | ((190208-herbata-grzyby-i-mimik)) |
| Anita Wirkot         | 1 | ((181205-jeden-dokument-nie-w-pore)) |
| Antoni Żuwaczka      | 1 | ((190616-anomalna-serafina)) |
| Arazille             | 1 | ((181226-finis-vitae)) |
| Arkadiusz Mocarny    | 1 | ((180814-trufle-z-kosmosu)) |
| ASD Centurion        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Azalia d'Alkaris     | 1 | ((200623-adaptacja-azalii)) |
| Bartłomiej Małczarek | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| BIA Tarn             | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Bożymir Szczupak     | 1 | ((190502-pierwszy-emulator-orbitera)) |
| Cezary Alentik       | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Cezary Zwierz        | 1 | ((181112-odklatwianie-ateny)) |
| Damian Podpalnik     | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
| Dariusz Kuromin      | 1 | ((190906-wypadek-w-kramamczu)) |
| Diana Lemurczak      | 1 | ((200524-dom-dla-melindy)) |
| Feliks Mirtan        | 1 | ((200502-po-co-atakuja-minerwe)) |
| Franciszek Leszczowik | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Franciszek Owadowiec | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Franek Bulterier     | 1 | ((200524-dom-dla-melindy)) |
| Gerwazy Lemurczak    | 1 | ((200524-dom-dla-melindy)) |
| Grażyna Sirwąg       | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Halina Sermniek      | 1 | ((200502-po-co-atakuja-minerwe)) |
| Hestia d'Itaran      | 1 | ((191201-ukradziony-entropik)) |
| Hestia d'Janor       | 1 | ((201025-kraloth-w-parku-janor)) |
| Hieronim Maus        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Ida Tiara            | 1 | ((190406-bardzo-kosztowne-lzy)) |
| Jakub Wirus          | 1 | ((180814-trufle-z-kosmosu)) |
| Jan Kramczuk         | 1 | ((190102-stalker-i-czerwone-myszy)) |
| Jan Revlen           | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Jan Uszczar          | 1 | ((190917-zagubiony-efemerydyta)) |
| Kaja Selerek         | 1 | ((190402-eksperymentalny-power-suit)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Karol Senonik        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Karol Szurnak        | 1 | ((190101-morderczyni-jednej-plotki)) |
| Karradrael           | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Konrad Wączak        | 1 | ((200623-adaptacja-azalii)) |
| Krystyna Senonik     | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Ksawery Wojnicki     | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Maciej Oczorniak     | 1 | ((201025-kraloth-w-parku-janor)) |
| Maja Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Malictrix d'Itaran   | 1 | ((200202-krucjata-chevaleresse)) |
| Marcel Nieciesz      | 1 | ((190422-pustogorski-konflikt)) |
| Mariola Tralment     | 1 | ((191105-zaginiona-soniczka)) |
| Mieszko Weiner       | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Minerwa Diakon       | 1 | ((181112-odklatwianie-ateny)) |
| Mirela Satarail      | 1 | ((190527-mimik-sni-o-esuriit)) |
| Nataniel Marszalnik  | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Nikodem Larwent      | 1 | ((200502-po-co-atakuja-minerwe)) |
| Oliwia Namałłek      | 1 | ((190505-szczur-ktory-chroni)) |
| Rafał Bobowiec       | 1 | ((190213-wygasniecie-starego-autosenta)) |
| Rafał Kamaron        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Rafał Roszczeniok    | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Roman Kłębek         | 1 | ((180814-trufle-z-kosmosu)) |
| Romeo Węglas         | 1 | ((190331-kurz-po-ataienne)) |
| Ronald Grzymość      | 1 | ((190616-anomalna-serafina)) |
| Sławomir Niejadek    | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Stach Sosnowiecki    | 1 | ((190208-herbata-grzyby-i-mimik)) |
| Staś Arienik         | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Szczepan Mensic      | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Szymon Grej          | 1 | ((181205-jeden-dokument-nie-w-pore)) |
| Tadeusz Rupczak      | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Tadeusz Sklerzec     | 1 | ((191112-korupcja-z-arystokratki)) |
| Tadeusz Tessalon     | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Teresa Marszalnik    | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Urszula Arienik      | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Wawrzyn Towarzowski  | 1 | ((181104-kotlina-duchow)) |
| Wioletta Kalazar     | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Wojciech Zermann     | 1 | ((190830-kto-wrobil-alana)) |
| Wojmił Siwywilk      | 1 | ((190422-pustogorski-konflikt)) |
| Zbigniew Burzycki    | 1 | ((181227-adieu-cieniaszczycie)) |