# Liliana Bankierz
## Identyfikator

Id: 1901-liliana-bankierz

## Sekcja Opowieści

### Pierwszy tajemniczy wielbiciel Liliany

* **uid:** 230212-pierwszy-tajemniczy-wielbiciel-liliany, _numer względny_: 16
* **daty:** 0111-10-22 - 0111-10-24
* **obecni:** Julia Kardolin, Laura Tesinik, Liliana Bankierz, Mariusz Kupieczka, Triana Porzecznik

Streszczenie:

Liliana, Triana, Julia pracują nad formą integracji scutiera i lancera by Liliana mogła pokonać (lub choć wytrzymać) Arkadię Verlen. Triana dostała alarm ze swojej zaginiętej drony o nieprzytomnych ludziach na Nieużytkach. Okazuje się, że portret Liliany wyssał tych ludzi. Okazuje się, że student AMZ, Mariusz, zrobił portret by Lilianę poderwać, ale spartaczył; porter wysysa. Potem portret został ukradziony. Liliana dalej podejrzewa spiski. Gdy Liliana i Mariusz się konfrontują, okazuje się że były 3 portrety. Liliana zachowuje się nikczemnie wobec Mariusza, więc się szybko odkochał. Ona nie skojarzyła, że komuś się może podobać.

Aktor w Opowieści:

* Dokonanie:
    * AMZ. Jej portret zrobiony przez Mariusza wysysa ludzi. Podejrzewa że to Eternia, Rekiny lub Zygmunt Zając. Koreluje dane statystyczne by dojść do tego kto za tym stoi, potem atakuje Mariusza słownie. Nie wierzy, że może się komukolwiek podobać - nie jest już Diakonką (ale tego nie mówi).


### Supersupertajny plan Loreny

* **uid:** 220730-supersupertajny-plan-loreny, _numer względny_: 15
* **daty:** 0111-09-30 - 0111-10-04
* **obecni:** Arkadia Verlen, Daniel Terienak, Karolina Terienak, Liliana Bankierz, Marsen Gwozdnik, Władysław Owczarek, Żorż d'Namertel

Streszczenie:

Marsen Gwozdnik wszedł na teren by pomóc Lorenie (która nie wie) uważając Lorenę za najlepszego taktyka na świecie (którym nie jest). Wyedytował część kontraktów okolicznych firm z Eternią, by spowodować niechęć firm do Eterni (a Ernest próbuje pomóc by Mimoza nie była jedyną która pomaga). Karo zastawiła na Marsena pułapkę i wzięła jako wsparcie Arkadię. Marsen natomiast wziął Lancera i Lilianę. Skończyło się na rannej Arkadii, sprzedanym Lancerze, rannej Lilianie i tym, że Karo przekonała Marsenę że jest... agentką Loreny?

Aktor w Opowieści:

* Dokonanie:
    * chciała uderzyć w Marysię i Ernesta (za Lorenę) i pomóc Marsenowi, acz nie wiedziała co Marsen robi i co planuje dla lokalnego biznesu (bo by nie pomogła). Zailuzjowała Marsenowego lancera i sama poszła jako Marek Samszar. Zaatakowana przez Arkadię nożem z zaskoczenia, skończyła w gliździe.
* Progresja:
    * solidnie ranna nożem Arkadii; dobre 2 tygodnie w gliździe Sensacjusza.


### Marysiowa Hestia Rekinów

* **uid:** 220111-marysiowa-hestia-rekinow, _numer względny_: 14
* **daty:** 0111-09-16 - 0111-09-19
* **obecni:** Diana Tevalier, Ernest Namertel, Hestia d'Rekiny, Jeremi Sowiński, Karolina Terienak, Keira Amarco d'Namertel, Liliana Bankierz, Marysia Sowińska

Streszczenie:

Konflikt Liliana - Marysia się zaostrza. Marysia skupia się na wzmocnieniu Hestii i uzyskaniu kontroli nad Rekinami - przejęła Hestię jako sojuszniczkę. Chevaleresse poprosiła Karolinę o uratowanie Melissy i Karo faktycznie pomogła - Ernest wysłał Keirę i odbili Melissę. To sprawiło, że Mimoza Diakon poczuła się zagrożona i weszła do akcji. Jakby tego było mało, Sowińscy wysyłają Jeremiego by opanował kuzynkę.

Aktor w Opowieści:

* Dokonanie:
    * twórca strasznych ulotek "Marysia Sowińska wróg wszelkiej wolności i podliz". Zrobiła artykuł w "Plotkach Royalsów" Pustaka.


### Akt, o którym Marysia nie wie

* **uid:** 211228-akt-o-ktorym-marysia-nie-wie, _numer względny_: 13
* **daty:** 0111-09-09 - 0111-09-12
* **obecni:** Daniel Terienak, Ernest Namertel, Karolina Terienak, Keira Amarco d'Namertel, Liliana Bankierz, Lorena Gwozdnik, Marysia Sowińska, Napoleon Bankierz, Rafał Torszecki

Streszczenie:

Torszecki ostrzegł Marysię, że jej akt krąży w Zaczęstwie. Ona to olała. Ale jak Ernest zaczął na niego polować to Marysia się zainteresowała. Zdecydowała się aktem odbić Ernesta Amelii. Zaflirtowała z Ernestem, obiecała mu akt specjalnie dla niego i że odda mu oryginał tego. Karo pokonała Napoleona, znalazła link do aktu który ostatecznie zdobył Torszecki. Daniel dla Karo zbił Torszeckiego i odzyskał akt. Marysia zaflirtowała z Ernestem ;-). Torszecki, który chciał dobrze i chronił Marysię skończył w szpitalu.

Aktor w Opowieści:

* Dokonanie:
    * należy do miłośników piękna. Ma akt od Marysi Sowińskiej i ma bliski kontakt z Loreną Gwozdnik. Nie wydała Marysi Loreny jako artystki, ale przekazała Lorenie zdjęcia z trackerem. Nie daruje Marysi.
* Progresja:
    * Marysia Sowińska wyciągnęła od niej informację pośrednio (trackerem) kto jest autorem aktu Marysi (Lorena). Przez to zaskarbiła sobie jej wściekłość i zemstę.
    * przyjaciółka Loreny Gwozdnik. Połączyło je upodobanie do piękna.


### Zgubiony holokryształ w lesie

* **uid:** 210817-zgubiony-holokrysztal-w-lesie, _numer względny_: 12
* **daty:** 0111-06-22 - 0111-06-24
* **obecni:** Alan Bartozol, Cyryl Perikas, Kacper Bankierz, Laura Tesinik, Liliana Bankierz, Mariusz Trzewń, Marysia Sowińska, Rupert Mysiokornik, Stella Armadion, Tomasz Tukan

Streszczenie:

Jeden z przerzucanych przez mafię przedmiotów do Aurum to ko-matryca Kuratorów, jako broń. Ale Cyryl - kurier - był ofiarą terminuskiego polowania więc ją odrzucił w Lesie Trzęsawnym gdzie znalazła ją Stella przekonana, że to noktiańska technologia. Marysia wypchnęła Kacpra Bankierza z mafii (i tego terenu) a Tukan znalazł i usunął ko-matrycę m.in. używając Laury.

Aktor w Opowieści:

* Dokonanie:
    * była na wycieczce AMZ dowodzonej przez Alana Bartozola; zauważyła że Stella coś tam znalazła i przekazała to Laurze gdy ta spytała.
* Progresja:
    * przyjaciółka Laury Tesinik "od zawsze".


### Verlenka na grzybkach

* **uid:** 210622-verlenka-na-grzybkach, _numer względny_: 11
* **daty:** 0111-05-30 - 0111-05-31
* **obecni:** Arkadia Verlen, Ignacy Myrczek, Julia Kardolin, Liliana Bankierz, Marek Samszar, Marysia Sowińska, Rafał Torszecki, Różewicz Diakon, Triana Porzecznik, Urszula Miłkowicz

Streszczenie:

Zespół AMZ składał projekt zaliczeniowy, gdy zaatakowała Arkadia Verlen na grzybkach ze swojego Lancera, uszkadzając projekt (robota). Julia wezwała Ulę (terminuskę) na pomoc, Ula unieszkodliwiła Marka (który podmienił Arkadię). Jako, że Julia i Triana były zajęte ratowaniem projektu, Marysia rzuciła się na znalezienie "kto atakuje Julię". Dotarła do niewinności Marka, dotarła do Arkadii i z pomocą Uli unieszkodliwiła Arkadię. Jedyne straty - ranna Ula i ranny Torszecki. Ale kto podał Arkadii te grzybki i jak?

Aktor w Opowieści:

* Dokonanie:
    * podejrzewa, że uszkodzenie robota kultywacyjnego to plan firmy Zygmunt Zając. Ale odda się pod dowodzenie Julii by dostarczyć robota na czas.


### Skradziony kot Olgi

* **uid:** 210615-skradziony-kot-olgi, _numer względny_: 10
* **daty:** 0111-05-13 - 0111-05-15
* **obecni:** Arkadia Verlen, Julia Kardolin, Liliana Bankierz, Marek Samszar, Marysia Sowińska, Olga Myszeczka, Paweł Szprotka, Wiktor Satarail

Streszczenie:

Marysia i Julia mają imprezę karaoke w Lesie Trzęsawnym, ale Marek nie dotarł - walczy na magię z Lilianą. Julia puściła drony Triany, które wpadły w berserk. By Tymon się nie zainteresował tym co się dzieje w lesie, Marysia i Julia chciały rozwiązać problem - okazało się, że Marek ukradł somnibela Oldze Myszeczce i dał go Arkadii Verlen w prezencie. Marysia i Julia przekonały Arkadię do oddania kota, oddały go Oldze, po czym Arkadia zerwała z Markiem i go ciężko pobiła. Acz Paweł Szprotka (który próbował odzyskać kota od Marka i którego broniła Liliana) boi się o życie, bo niektóre Rekiny się na niego uwzięły za karę...

Aktor w Opowieści:

* Dokonanie:
    * nie akceptuje tego, że na Pawle znęca się Samszar. Weszła z nim w starcie magiczne w Lesie Trzęsawnym. Przygotowuje walkę i zemstę przeciwko Samszarowi. Zneutralizowana przez okrucieństwo Arkadii na Samszara.


### Przygoda, randka i porwanie

* **uid:** 201020-przygoda-randka-i-porwanie, _numer względny_: 9
* **daty:** 0110-10-25 - 0110-10-27
* **obecni:** Daniel Sowiński, Gabriel Ursus, Henryk Sowiński, Kacper Bankierz, Laurencjusz Sorbian, Liliana Bankierz, Robert Pakiszon, Robinson Porzecznik, Tomasz Tukan, Triana Porzecznik, Tymon Grubosz, Urszula Miłkowicz

Streszczenie:

Młodzi i niekompetentni Sowińscy łażą po Zaczęstwie i szukają przygód. Gdy ich dyskretny opiekun został aresztowany, szybko ściągnął Roberta i Gabriela do pomocy - by zrobili przygodę. Jednocześnie Triana chciała podwójnej randki i też robiła przygodę z Lilianą. Skończyło się na tym, że wszystkie przygody się pomieszały, Triana została porwana przez siły Zespołu (!?) i walnęła efemeryda na budowanym biurowcu Gorland. Ale - 'stream must go on'.

Aktor w Opowieści:

* Dokonanie:
    * odegrała scenę "och nie, Upiór mnie opętał", po czym przebrana za gotkę leciała na pomoc porwanej Trianie! Pokazała dużą siłę jako kinetka. Zdyscyplinowana - gdy terminuska kazała, Liliana się wycofała.


### Pojedynek: Akademia - Rekiny

* **uid:** 201013-pojedynek-akademia-rekiny, _numer względny_: 8
* **daty:** 0110-10-14 - 0110-10-22
* **obecni:** Aleksander Bemucik, Ignacy Myrczek, Julia Kardolin, Justynian Diakon, Kacper Bankierz, Liliana Bankierz, Napoleon Bankierz, Remor 340D, Robert Pakiszon, Stella Armadion, Teresa Mieralit

Streszczenie:

Kolejna ustawka między Rekinami a uczniami AMZ mogłaby się skończyć bardzo źle, więc grupa uczniów spróbowała wprowadzić sposób rozwiązywania konfliktu przez turnieje między Rekinami i AMZ. Nie tylko im się to udało - użycie starego ścigacza wojskowego Remor 340D i echo emocji wojen noktiańskich sprawiło, że Rekiny się unormowały i zainwestowano w budowę Toru Wyścigowego Pamięci w Podwiercie.

Aktor w Opowieści:

* Dokonanie:
    * prowodyrka i podżegaczka bitew między Rekinami i studentami AMZ. Spróbowała zapalić studentów do ostrzejszej wojny, ale Robert i Julia zmienili to w zawody.


### Test z etyki

* **uid:** 200326-test-z-etyki, _numer względny_: 7
* **daty:** 0110-07-29 - 0110-07-31
* **obecni:** Aniela Kark, Berenika Wrążowiec, Ignacy Myrczek, Liliana Bankierz, Napoleon Bankierz, Teresa Mieralit

Streszczenie:

Liliana eskalowała swoją krucjatę przeciw "Zygmuntowi Zającowi", włączając do działania Ignacego Myrczka. Teresa Mieralit, nauczycielka m.in. etyki, zrobiła z tego przypadku egzamin dla dwóch uczennic kończących już swoją naukę w Akademii Magii. Skończyło się na stworzeniu anomalnego impa robiącego zdjęć śpiącej Lilianie i jeszcze większym podgrzaniu atmosfery. Ale - konserwy ZZ faktycznie posiadają dziwne substraty.

Aktor w Opowieści:

* Dokonanie:
    * AMZ; przekonana o tym że "Zygmunt Zając" ją prześladuje i wysyła na nią agentów. Zdekomponowała ich konserwy i wyhodowała z nich straszne rzeczy przy pomocy Myrczka. Imp od zjęć ją prześladuje.


### Wygrany kontrakt

* **uid:** 200311-wygrany-kontrakt, _numer względny_: 6
* **daty:** 0110-07-20 - 0110-07-23
* **obecni:** Amanda Kajrat, Dagmara Doberman, Ernest Kajrat, Liliana Bankierz, Lucjusz Blakenbauer, Minerwa Metalia, Ziemowit Zięba

Streszczenie:

Ernest Kajrat jest w stanie śmiertelnym; Lucjusz i Minerwa trzymają go przy życiu. Tymczasem firma dostarczająca jedzenie "Zygmunt Zając" wygrała przetarg (na który nie startowała) odnośnie jedzenia dla zixionizowanej Amandy Kajrat w rękach Grzymościa. Współpraca Blakenbauera i "Zająca" doprowadza do uwolnienia Amandy i kupienia szansy Ernestowi...

Aktor w Opowieści:

* Dokonanie:
    * chciała udowodnić, że jedzenie firmy "Zygmunt Zając" jest niejadalne (mimo że trafia na stołówkę); włamała się i zebrała dowody. Upokorzona na wizji, stała się wrogiem "Zająca" na całe życie.


### Liliana w świecie dokumentów

* **uid:** 190820-liliana-w-swiecie-dokumentow, _numer względny_: 5
* **daty:** 0110-07-04 - 0110-07-08
* **obecni:** Adela Pieczar, Arnulf Poważny, Liliana Bankierz, Szymon Jaszczurzec, Tadeusz Kruszawiecki, Tymon Grubosz

Streszczenie:

Tiamenat wyprodukował eksperymentalnego, pomocnego mimika. Chcieli przesłać go do Trzeciego Raju gdzie by się przydał, ale ingerencja Liliany sprawiła, że mimik zniknął. A Tymon próbował rozpaczliwie niczego nie zauważyć, lecz dzięki działaniu Zespołu (magów ze Szkoły Magii) musiał rozwiązać problem. Ech, te dzieci. Ale Liliana odzyskała dobre imię.

Aktor w Opowieści:

* Dokonanie:
    * nadmiar bohaterstwa i żądzy przygód. Zaczęła od zrobienia artefaktu, potem wpakowała się w linię spedycyjną Noctis - Trzeci Raj i skończyła jako przynęta na mimika. Tydzień w szpitalu.


### Noc Kajrata

* **uid:** 190623-noc-kajrata, _numer względny_: 4
* **daty:** 0110-05-22 - 0110-05-25
* **obecni:** Ernest Kajrat, Liliana Bankierz, Ossidia Saitis, Pięknotka Diakon, Serafina Ira

Streszczenie:

Kajrat poszedł za ciosem swojego planu. Przekształcił Serafinę mocą Esuriit; nadał jej Aspekt Banshee, który jednak Serafina jest w stanie jakoś opanować (nie zmieniając jej charakteru ani podejścia). Pięknotka odkryła sekret Kajrata - jest częściowo istotą Esuriit i jest jednym z dowódców Inwazji Noctis. Uwolniła Lilianę spod wpływu Kajrata i udało jej się uratować Ossidię przed śmiercią z rąk upiora Esuriit.

Aktor w Opowieści:

* Dokonanie:
    * trafiła w strefę wpływów Kajrata; zobaczyła że ma do czynienia z potworem. Pięknotka uratowała ją przed losem służki Kajrata.


### Wojna Kajrata

* **uid:** 190622-wojna-kajrata, _numer względny_: 3
* **daty:** 0110-05-18 - 0110-05-21
* **obecni:** Ernest Kajrat, Liliana Bankierz, Nikola Kirys, Olaf Zuchwały, Pięknotka Diakon, Serafina Ira

Streszczenie:

Kajrat zaeskalował; ściągnął Lilianę i powiedział Pięknotce, że Liliana zostanie jego podwładną jeśli Serafina nie osiągnie sukcesu. Pięknotka poznała historię Serafiny - kiedyś agentka Pustogoru, w wyniku burzy w Pacyfice straciła przyjaciół. Teraz jest cieniem dawnej osoby. Po drodze, w Wolnych Ptakach, Pięknotkę zaatakowała grupa wolnych magów; uruchomił się Cień i Pięknotka zrobiła masakrę. To sprawiło większe problemy i niedogadanie między Enklawami a Pustogorem

Aktor w Opowieści:

* Dokonanie:
    * za to co zrobiła z seksbotem Kajrata ów ją przechwycił; czuje dziwną energię od Kajrata ale nie umie jej nazwać czy określić.
* Progresja:
    * straumatyzowana przez Kajrata, który może zabrać jej wszystko jeśli do niego nie dołączy.


### Uciekający seksbot

* **uid:** 190519-uciekajacy-seksbot, _numer względny_: 2
* **daty:** 0110-04-25 - 0110-04-26
* **obecni:** Arnulf Poważny, Eliza Ira, Ernest Kajrat, Liliana Bankierz, Ossidia Saitis, Pięknotka Diakon, Saitaer, Teresa Mieralit, Tomasz Tukan

Streszczenie:

Ernest z mafii przybył do szkoły magów w Zaczęstwie szukając swojego seksbota. Pięknotka deeskalowała sytuację i poszła z neuronautą Tomaszem szukać; okazało się, że seksbot jest bardzo zmodyfikowany i bardzo świadomy - do tego stopnia, że seksbotowi zaczęła pomagać Eliza Ira. Pięknotka i Tomasz rozmontowali problem, ale Pięknotka nie miała serca oddać seksbota sadystycznemu Ernestowi. W rozpaczy, poprosiła o pomoc Saitaera, który odpowiedział...

Aktor w Opowieści:

* Dokonanie:
    * żądała od Pięknotki, by ona pomogła biednemu seksbotowi. Nie udało jej się zmusić Pięknotki do niczego. Najpewniej uczestniczyła w wykradzeniu seksbota Ernestowi.


### Chrońmy Karolinę przed uczniami

* **uid:** 190113-chronmy-karoline-przed-uczniami, _numer względny_: 1
* **daty:** 0110-01-05 - 0110-01-06
* **obecni:** Adela Kirys, Arnulf Poważny, Karolina Erenit, Liliana Bankierz, Napoleon Bankierz, Pięknotka Diakon, Teresa Mieralit

Streszczenie:

Karla chciała pozbyć się Pięknotki jeszcze raz. Wysłała na papierkową robotę do Szkoły Magów w Zaczęstwie. Tam okazało się, że kandydat na terminusa (Napoleon) próbował uratować człowieka, Karolinę Erenit przed innymi uczniami szkoły magów. Niestety, eliksir który zamówił u Adeli miał efekty uboczne. Pięknotka gładko to rozwiązała i nikt nie miał problemów. Poza tym, że Karolina nadal nie jest chroniona.

Aktor w Opowieści:

* Dokonanie:
    * chciała pomóc Karolinie tak jak Napoleon, ale wezwała terminusa. Też się zaraziła i zdemolowała pokój. Dostała niesłuszną reputację. Kiedyś Diakonka, ale ciało odrzuciła tą krew.
* Progresja:
    * dostała całkowicie nieuzasadnioną opinię osoby zdolnej do walki z terminusem w power suicie bez niczego (przez erupcję energii w Pięknotkę).


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 16, @: 0111-10-24
    1. Primus    : 16, @: 0111-10-24
        1. Sektor Astoriański    : 16, @: 0111-10-24
            1. Astoria    : 16, @: 0111-10-24
                1. Sojusz Letejski, SW    : 1, @: 0110-05-21
                    1. Granica Anomalii    : 1, @: 0110-05-21
                        1. Wolne Ptaki    : 1, @: 0110-05-21
                            1. Królewska Baza    : 1, @: 0110-05-21
                1. Sojusz Letejski    : 16, @: 0111-10-24
                    1. Szczeliniec    : 16, @: 0111-10-24
                        1. Powiat Pustogorski    : 16, @: 0111-10-24
                            1. Czarnopalec    : 1, @: 0111-05-15
                                1. Pusta Wieś    : 1, @: 0111-05-15
                            1. Czółenko    : 2, @: 0111-06-24
                                1. Bunkry    : 1, @: 0110-05-25
                            1. Podwiert    : 7, @: 0111-10-04
                                1. Dzielnica Luksusu Rekinów    : 5, @: 0111-09-19
                                    1. Fortyfikacje Rolanda    : 1, @: 0111-09-19
                                    1. Obrzeża Biedy    : 3, @: 0111-09-19
                                        1. Domy Ubóstwa    : 2, @: 0111-09-19
                                        1. Hotel Milord    : 2, @: 0111-09-19
                                        1. Stadion Lotników    : 1, @: 0111-09-19
                                        1. Stajnia Rumaków    : 1, @: 0111-09-19
                                    1. Sektor Brudu i Nudy    : 1, @: 0111-09-19
                                        1. Komputerownia    : 1, @: 0111-09-19
                                        1. Konwerter Magielektryczny    : 1, @: 0111-09-19
                                        1. Magitrownia Pogardy    : 1, @: 0111-09-19
                                        1. Skrytki Czereśniaka    : 1, @: 0111-09-19
                                    1. Serce Luksusu    : 2, @: 0111-09-19
                                        1. Apartamentowce Elity    : 2, @: 0111-09-19
                                        1. Arena Amelii    : 1, @: 0111-09-19
                                        1. Fontanna Królewska    : 1, @: 0111-09-19
                                        1. Kawiarenka Relaks    : 1, @: 0111-09-19
                                        1. Lecznica Rannej Rybki    : 2, @: 0111-09-19
                                1. Kompleks Korporacyjny    : 1, @: 0111-10-04
                                    1. Zakład Recyklingu Owczarek    : 1, @: 0111-10-04
                                1. Las Trzęsawny    : 3, @: 0111-06-24
                                    1. Schron TRZ-17    : 1, @: 0111-05-15
                                1. Sensoplex    : 1, @: 0110-10-22
                                1. Tor Wyścigowy Pamięci    : 1, @: 0110-10-22
                            1. Pustogor, okolice    : 1, @: 0110-07-23
                                1. Rezydencja Blakenbauerów    : 1, @: 0110-07-23
                            1. Pustogor    : 1, @: 0111-05-31
                                1. Rdzeń    : 1, @: 0111-05-31
                                    1. Barbakan    : 1, @: 0111-05-31
                                        1. Kazamaty    : 1, @: 0111-05-31
                            1. Zaczęstwo    : 13, @: 0111-10-24
                                1. Akademia Magii, kampus    : 8, @: 0111-10-24
                                    1. Akademik    : 2, @: 0111-09-19
                                    1. Arena Treningowa    : 3, @: 0111-09-19
                                    1. Artefaktorium    : 2, @: 0111-10-24
                                    1. Audytorium    : 1, @: 0110-10-22
                                    1. Budynek Centralny    : 4, @: 0110-07-31
                                        1. Skrzydło Loris    : 3, @: 0110-07-31
                                    1. Las Trzęsawny    : 2, @: 0111-09-19
                                1. Bazar Różności    : 1, @: 0110-10-27
                                1. Biurowce    : 1, @: 0110-07-08
                                    1. Papierówka    : 1, @: 0110-07-08
                                1. Biurowiec Gorland    : 1, @: 0110-10-27
                                1. Cyberszkoła    : 1, @: 0110-07-08
                                1. Dzielnica Kwiecista    : 2, @: 0111-10-24
                                    1. Rezydencja Porzeczników    : 2, @: 0111-10-24
                                        1. Garaż Groźnych Eksperymentów    : 1, @: 0111-10-24
                                        1. Podziemne Laboratorium    : 1, @: 0110-10-27
                                1. Dzielnica Ogrodów    : 1, @: 0111-10-24
                                    1. Wielki Szpital Magiczny    : 1, @: 0111-10-24
                                1. Kawiarenka Leopold    : 2, @: 0110-10-27
                                1. Klub Poetycki Sucharek    : 1, @: 0110-07-08
                                1. Las Trzęsawny    : 1, @: 0111-05-31
                                1. Nieużytki Staszka    : 6, @: 0111-10-24

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 5 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 210817-zgubiony-holokrysztal-w-lesie; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow)) |
| Ernest Kajrat        | 4 | ((190519-uciekajacy-seksbot; 190622-wojna-kajrata; 190623-noc-kajrata; 200311-wygrany-kontrakt)) |
| Julia Kardolin       | 4 | ((201013-pojedynek-akademia-rekiny; 210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Napoleon Bankierz    | 4 | ((190113-chronmy-karoline-przed-uczniami; 200326-test-z-etyki; 201013-pojedynek-akademia-rekiny; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Pięknotka Diakon     | 4 | ((190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 190622-wojna-kajrata; 190623-noc-kajrata)) |
| Teresa Mieralit      | 4 | ((190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 200326-test-z-etyki; 201013-pojedynek-akademia-rekiny)) |
| Arkadia Verlen       | 3 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 220730-supersupertajny-plan-loreny)) |
| Arnulf Poważny       | 3 | ((190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 190820-liliana-w-swiecie-dokumentow)) |
| Ignacy Myrczek       | 3 | ((200326-test-z-etyki; 201013-pojedynek-akademia-rekiny; 210622-verlenka-na-grzybkach)) |
| Kacper Bankierz      | 3 | ((201013-pojedynek-akademia-rekiny; 201020-przygoda-randka-i-porwanie; 210817-zgubiony-holokrysztal-w-lesie)) |
| Karolina Terienak    | 3 | ((211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220730-supersupertajny-plan-loreny)) |
| Tomasz Tukan         | 3 | ((190519-uciekajacy-seksbot; 201020-przygoda-randka-i-porwanie; 210817-zgubiony-holokrysztal-w-lesie)) |
| Triana Porzecznik    | 3 | ((201020-przygoda-randka-i-porwanie; 210622-verlenka-na-grzybkach; 230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Daniel Terienak      | 2 | ((211228-akt-o-ktorym-marysia-nie-wie; 220730-supersupertajny-plan-loreny)) |
| Ernest Namertel      | 2 | ((211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow)) |
| Keira Amarco d'Namertel | 2 | ((211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow)) |
| Laura Tesinik        | 2 | ((210817-zgubiony-holokrysztal-w-lesie; 230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Marek Samszar        | 2 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach)) |
| Ossidia Saitis       | 2 | ((190519-uciekajacy-seksbot; 190623-noc-kajrata)) |
| Rafał Torszecki      | 2 | ((210622-verlenka-na-grzybkach; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Robert Pakiszon      | 2 | ((201013-pojedynek-akademia-rekiny; 201020-przygoda-randka-i-porwanie)) |
| Serafina Ira         | 2 | ((190622-wojna-kajrata; 190623-noc-kajrata)) |
| Stella Armadion      | 2 | ((201013-pojedynek-akademia-rekiny; 210817-zgubiony-holokrysztal-w-lesie)) |
| Tymon Grubosz        | 2 | ((190820-liliana-w-swiecie-dokumentow; 201020-przygoda-randka-i-porwanie)) |
| Urszula Miłkowicz    | 2 | ((201020-przygoda-randka-i-porwanie; 210622-verlenka-na-grzybkach)) |
| Adela Kirys          | 1 | ((190113-chronmy-karoline-przed-uczniami)) |
| Adela Pieczar        | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Alan Bartozol        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Aleksander Bemucik   | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Amanda Kajrat        | 1 | ((200311-wygrany-kontrakt)) |
| Aniela Kark          | 1 | ((200326-test-z-etyki)) |
| Berenika Wrążowiec   | 1 | ((200326-test-z-etyki)) |
| Cyryl Perikas        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Dagmara Doberman     | 1 | ((200311-wygrany-kontrakt)) |
| Daniel Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Diana Tevalier       | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Eliza Ira            | 1 | ((190519-uciekajacy-seksbot)) |
| Gabriel Ursus        | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Henryk Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Hestia d'Rekiny      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Jeremi Sowiński      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Justynian Diakon     | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Karolina Erenit      | 1 | ((190113-chronmy-karoline-przed-uczniami)) |
| Laurencjusz Sorbian  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Lorena Gwozdnik      | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Lucjusz Blakenbauer  | 1 | ((200311-wygrany-kontrakt)) |
| Mariusz Kupieczka    | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Mariusz Trzewń       | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Marsen Gwozdnik      | 1 | ((220730-supersupertajny-plan-loreny)) |
| Minerwa Metalia      | 1 | ((200311-wygrany-kontrakt)) |
| Nikola Kirys         | 1 | ((190622-wojna-kajrata)) |
| Olaf Zuchwały        | 1 | ((190622-wojna-kajrata)) |
| Olga Myszeczka       | 1 | ((210615-skradziony-kot-olgi)) |
| Paweł Szprotka       | 1 | ((210615-skradziony-kot-olgi)) |
| Remor 340D           | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Robinson Porzecznik  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Różewicz Diakon      | 1 | ((210622-verlenka-na-grzybkach)) |
| Rupert Mysiokornik   | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Saitaer              | 1 | ((190519-uciekajacy-seksbot)) |
| Szymon Jaszczurzec   | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Tadeusz Kruszawiecki | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Wiktor Satarail      | 1 | ((210615-skradziony-kot-olgi)) |
| Władysław Owczarek   | 1 | ((220730-supersupertajny-plan-loreny)) |
| Ziemowit Zięba       | 1 | ((200311-wygrany-kontrakt)) |
| Żorż d'Namertel      | 1 | ((220730-supersupertajny-plan-loreny)) |