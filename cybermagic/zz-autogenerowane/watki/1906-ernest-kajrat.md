# Ernest Kajrat
## Identyfikator

Id: 1906-ernest-kajrat

## Sekcja Opowieści

### Wygrany kontrakt

* **uid:** 200311-wygrany-kontrakt, _numer względny_: 19
* **daty:** 0110-07-20 - 0110-07-23
* **obecni:** Amanda Kajrat, Dagmara Doberman, Ernest Kajrat, Liliana Bankierz, Lucjusz Blakenbauer, Minerwa Metalia, Ziemowit Zięba

Streszczenie:

Ernest Kajrat jest w stanie śmiertelnym; Lucjusz i Minerwa trzymają go przy życiu. Tymczasem firma dostarczająca jedzenie "Zygmunt Zając" wygrała przetarg (na który nie startowała) odnośnie jedzenia dla zixionizowanej Amandy Kajrat w rękach Grzymościa. Współpraca Blakenbauera i "Zająca" doprowadza do uwolnienia Amandy i kupienia szansy Ernestowi...

Aktor w Opowieści:

* Dokonanie:
    * śmiertelnie ranny, utrzymywany przez Lucjusza przy życiu. Najpewniej gdzieś w Rezydencji Blakenbauerów


### Bardzo niebezpieczne składowisko

* **uid:** 190726-bardzo-niebezpieczne-skladowisko, _numer względny_: 18
* **daty:** 0110-06-26 - 0110-06-28
* **obecni:** Amanda Kajrat, Cezary Alentik, Ernest Kajrat, Gabriel Ursus, Mirela Orion, Pięknotka Diakon, Roland Grzymość

Streszczenie:

Na Składowisku Odpadów Niebezpiecznych przy Podwiercie siły Wolnego Uśmiechu zaczęły składać jakieś narkotyki. Nie podoba się to Nocnemu Niebu. Pięknotka w środku tego wszystkiego - udało jej się poprosić Orbiter o zajęcie się linią dostawczą z Cieniaszczytu, Kajrata by chwilowo zwolnił, Pustogor by chronili właściciela a Grzymościa, by zszedł do podziemia. Polityczny sukces - ale czemu Grzymość idzie w jakieś narkotyki? I czemu Kajrat dąży do wojny z Grzymościem o te narkotyki?

Aktor w Opowieści:

* Dokonanie:
    * zdecydował się zaufać Pustogorowi w formie Pięknotki by nie eskalować wojny z Wolnym Uśmiechem. A przynajmniej teraz.


### Kult, choroba Esuriit

* **uid:** 190714-kult-choroba-esuriit, _numer względny_: 17
* **daty:** 0110-06-07 - 0110-06-09
* **obecni:** Amanda Kajrat, Ernest Kajrat, Pięknotka Diakon, Tomasz Tukan

Streszczenie:

Tukan poluje dalej na temat somnibela i znalazł kult wywołany przez magię. Pięknotka zdobyła mu uprawnienia a on spieprzył - dał się zainfekować Esuriit i ruszył na wojnę z Kajratem. Pięknotka uratowała mu tyłek za co dostała wsparcie Amandy Kajrat. Z jej pomocą zniszczyła kult w Podwiercie, ale dużo wskazuje że w Czółenku (gdzie indziej...) jest ulubiony ciąg dalszy.

Aktor w Opowieści:

* Dokonanie:
    * przekazał Pięknotce jako wsparcie swoją "córkę". Nie udało mu się zbadać Tukana, ale zbadał dotkniętego przez Esuriit kultystę.


### Somnibel uciekł Arienikom

* **uid:** 190709-somnibel-uciekl-arienikom, _numer względny_: 16
* **daty:** 0110-06-05 - 0110-06-07
* **obecni:** Ernest Kajrat, Jan Revlen, Ksawery Wojnicki, Pięknotka Diakon, Staś Arienik, Tomasz Tukan, Urszula Arienik

Streszczenie:

Bogatemu rodowi Arieników powiązanemu z Luxuritias uciekł somnibel. Pięknotka znalazła go u Kajrata - który dla odmiany nie porwał kociego viciniusa; dostał go od podwładnego. Kajrat oddał somnibela Pięknotce po wykonaniu pewnego testu a napięcie między Arienikami a "plebsem" z Podwiertu się podniosło.

Aktor w Opowieści:

* Dokonanie:
    * całkowicie zaskoczony tym, że ma somnibela; porwał dla niego go Ksawery. Chciał sprawdzić czy Serafina rozerwie link somnibel - ofiara; okazuje się, że tak.


### Upadek enklawy Floris

* **uid:** 190626-upadek-enklawy-floris, _numer względny_: 15
* **daty:** 0110-05-28 - 0110-05-31
* **obecni:** Ariela Sirmin, Ernest Kajrat, Hubert Kraborów, Jolanta Teresis, Konrad Czukajczek, Marcel Sowiński, Nikola Kirys, Roman Rymtusz, Szymon Maszczor, Wargun Ira

Streszczenie:

W odpowiedzi na atak na Pięknotkę, Pustogor wysłał oddział by dowiedzieć się co się dzieje w Enklawach, mający porwać Enklawę Floris. Floris jednak się rozdzieliło - część osób poszła się sama poddać, inni zdecydowali się dalej chować. Poszli do Wiecznej Maszyny i tam znaleźli tymczasową bazę przez posiadanie jednego Skażonego maga (którego Maszyna akceptuje). Jest tam też mag Kajrata zdolny do fabrykacji - źródło Pajęczaków. Połączyli siły i zamaskowali swoje ślady. Pustogor został z niczym, acz z ~10 osobami z Floris, więc... sukces?

Aktor w Opowieści:

* Dokonanie:
    * wzmacnia Enklawy pod szyldem Nocnego Nieba; zapewnił sobie wsparcie Floris z fabrykatorem Wiecznej Maszyny.


### Noc Kajrata

* **uid:** 190623-noc-kajrata, _numer względny_: 14
* **daty:** 0110-05-22 - 0110-05-25
* **obecni:** Ernest Kajrat, Liliana Bankierz, Ossidia Saitis, Pięknotka Diakon, Serafina Ira

Streszczenie:

Kajrat poszedł za ciosem swojego planu. Przekształcił Serafinę mocą Esuriit; nadał jej Aspekt Banshee, który jednak Serafina jest w stanie jakoś opanować (nie zmieniając jej charakteru ani podejścia). Pięknotka odkryła sekret Kajrata - jest częściowo istotą Esuriit i jest jednym z dowódców Inwazji Noctis. Uwolniła Lilianę spod wpływu Kajrata i udało jej się uratować Ossidię przed śmiercią z rąk upiora Esuriit.

Aktor w Opowieści:

* Dokonanie:
    * Pięknotka odkryła jego sekret powiązany z Esuriit. Doprowadził do transformacji Serafiny Iry w istotę będącą częściowo Banshee - broń przeciwko Ataienne. Skończył wyczerpany ale zwycięski.


### Wojna Kajrata

* **uid:** 190622-wojna-kajrata, _numer względny_: 13
* **daty:** 0110-05-18 - 0110-05-21
* **obecni:** Ernest Kajrat, Liliana Bankierz, Nikola Kirys, Olaf Zuchwały, Pięknotka Diakon, Serafina Ira

Streszczenie:

Kajrat zaeskalował; ściągnął Lilianę i powiedział Pięknotce, że Liliana zostanie jego podwładną jeśli Serafina nie osiągnie sukcesu. Pięknotka poznała historię Serafiny - kiedyś agentka Pustogoru, w wyniku burzy w Pacyfice straciła przyjaciół. Teraz jest cieniem dawnej osoby. Po drodze, w Wolnych Ptakach, Pięknotkę zaatakowała grupa wolnych magów; uruchomił się Cień i Pięknotka zrobiła masakrę. To sprawiło większe problemy i niedogadanie między Enklawami a Pustogorem

Aktor w Opowieści:

* Dokonanie:
    * pokazał bardziej okrutną naturę szantażysty i dominatora przerażonej Liliany; dał jednak Pięknotce chwilę czasu na rozwiązanie sprawy Serafiny i Liliany.


### Anomalna Serafina

* **uid:** 190616-anomalna-serafina, _numer względny_: 12
* **daty:** 0110-05-12 - 0110-05-15
* **obecni:** Antoni Żuwaczka, Ernest Kajrat, Krystian Namałłek, Pięknotka Diakon, Ronald Grzymość, Serafina Ira, Tomasz Tukan

Streszczenie:

W Podwiercie pojawiła się Serafina, piosenkarka zbierająca i asymilująca anomalie. Wyraźnie pomaga jej Kajrat, który ją dodatkowo chroni. Pięknotka dostała zadanie odzyskać te niestabilne anomalie - ale Serafina jest lubiana przez kilku cieniaszczyckich bonzów; nie można odebrać jej anomalii siłą. Dodatkowo, Serafina ma w sobie stary gniew na Pustogor za coś, co się zdarzyło w Pacyfice. Wszystko zbliża się w kierunku na wojnę lub konflikt zbrojny.

Aktor w Opowieści:

* Dokonanie:
    * mastermind stojący za "niech Pustogor i Cieniaszczyt staną do walki przeciw sobie". Trochę pomaga Pięknotce, bardzo Serafinie. Grzymość go w końcu zatrzymał, ale zaczął ziarno konfliktu.
* Progresja:
    * Grzymość mu zdecydowanie mniej ufa. "Mad Dog Kajrat", nie zaufana prawa ręka - czemu chciał wojny?
    * Zły na Pięknotkę - wprowadziła Grzymościa do walki z Kajratem zamiast stanąć naprzeciw niego osobiście.


### Uciekający seksbot

* **uid:** 190519-uciekajacy-seksbot, _numer względny_: 11
* **daty:** 0110-04-25 - 0110-04-26
* **obecni:** Arnulf Poważny, Eliza Ira, Ernest Kajrat, Liliana Bankierz, Ossidia Saitis, Pięknotka Diakon, Saitaer, Teresa Mieralit, Tomasz Tukan

Streszczenie:

Ernest z mafii przybył do szkoły magów w Zaczęstwie szukając swojego seksbota. Pięknotka deeskalowała sytuację i poszła z neuronautą Tomaszem szukać; okazało się, że seksbot jest bardzo zmodyfikowany i bardzo świadomy - do tego stopnia, że seksbotowi zaczęła pomagać Eliza Ira. Pięknotka i Tomasz rozmontowali problem, ale Pięknotka nie miała serca oddać seksbota sadystycznemu Ernestowi. W rozpaczy, poprosiła o pomoc Saitaera, który odpowiedział...

Aktor w Opowieści:

* Dokonanie:
    * dżentelmen i oficer mafii, który - jak się okazuje - ma bardzo mroczne zapędy. Zarówno sadystyczne jak i wobec Ateny. I nic a nic nie ukrywa swojej natury. Nic dziwnego, że się go boją.


### Szczur który chroni

* **uid:** 190505-szczur-ktory-chroni, _numer względny_: 10
* **daty:** 0110-04-22 - 0110-04-24
* **obecni:** Adela Kirys, Alan Bartozol, Ernest Kajrat, Kasjan Czerwoczłek, Krystian Namałłek, Lucjusz Blakenbauer, Oliwia Namałłek, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Zaczęło się od Skażenia proszkiem kralotycznym Nieużytków Staszka. Potem Pięknotka współpracując z mafią doszła do tego, że Adela i terminus stoją za tą sprawą. Potem Pięknotka uratowała Adelę i terminusa wprowadzając szturmowych terminusów Pustogoru na mafię. Gdy "Cień wyrwał się spod kontroli", Alan ją strzelił z działa strumieniowego. I tak skończyło się rumakowanie na tydzień - ale wszyscy są bezpieczni.

Aktor w Opowieści:

* Dokonanie:
    * prawa ręka mafii. Oficer Grzymościa. Niebezpieczny w walce, z dużym poczuciem humoru. Polował na Adelę i Krystiana; skończył trafiony przez Alana z Koeniga.


### Migświatło psychotroniczek

* **uid:** 190828-migswiatlo-psychotroniczek, _numer względny_: 9
* **daty:** 0110-02-08 - 0110-02-10
* **obecni:** Artur Michasiewicz, Ernest Kajrat, Marek Puszczok, Minerwa Metalia, Pięknotka Diakon, Talia Aegis, Tymon Grubosz

Streszczenie:

Gdy Kajrat jest w więzieniu, nikt nie kontroluje przepływu technologii noktiańskiej. Talia ostrzegła Pięknotkę, że do holo-walki AI dodała subturingowe BIA klasy Szponowiec, z żądania gościa od Grzymościa. Okazało się, że to niegroźny hazard jest - ale są tam dwie dziwne walczące AI: BIA oraz militarna, stworzona przez Minerwę. Pięknotka chciała tylko zabić BIA, ale przypadkowo z Minerwą wysadziły Arenę Migświatła. Tymon zabrał Minerwie jej TAI. Powiedział, że jeszcze nie czas by takie rzeczy robiła.

Aktor w Opowieści:

* Dokonanie:
    * nadal w więzieniu; okazuje się, że pełni kluczową rolę neutralizatora przepływu artefaktów noktiańskich. Dzięki niemu nie ma dziwnych anomalii. Teraz go nie ma.


### Rozpaczliwe ratowanie BII

* **uid:** 190827-rozpaczliwe-ratowanie-bii, _numer względny_: 8
* **daty:** 0110-01-18 - 0110-01-21
* **obecni:** BIA Tarn, Ernest Kajrat, Marek Puszczok, Mariusz Trzewń, Pięknotka Diakon, Sławomir Niejadek, Talia Aegis, Tymon Grubosz

Streszczenie:

Talia próbowała utrzymać przy życiu BIA 3 generacji. Nie mając surowców, użyła tej BIA do zdobycia rzeczy odżywczych z Tiamenat - zrzucając winę na Wiktora Sataraila. Pięknotka poszła za śladem i gdy dotarła do Talii, Kajrat wziął na siebie winę (za co Pięknotka go aresztowała). Następnie poprosiła Wiktora Sataraila by ten ochronił swoje dobre imię - i zniszczył śmiertelnie niebezpieczną BIA.

Aktor w Opowieści:

* Dokonanie:
    * całkowicie nieświadomy sprawy wpakował się w kiepskiej klasy intrygę Talii. Wziął winę na siebie, dał się złapać i poszedł dla niej do więzienia na pewien czas; uzyskał jej wsparcie za to.
* Progresja:
    * poszedł do więzienia by chronić Talię Aegis przed jej działaniami. Nie przeszkadza mu to w zupełności.


### Glizda, która leczy

* **uid:** 211120-glizda-ktora-leczy, _numer względny_: 7
* **daty:** 0108-04-07 - 0108-04-18
* **obecni:** Amanda Kajrat, Amelia Sowińska, Ernest Kajrat, Feliks Keksik, Justynian Diakon, Kacper Bankierz, Karol Pustak, Oliwia Lemurczak, Roland Sowiński, Sabina Kazitan, Sensacjusz Diakon, Stella Sowińska

Streszczenie:

Sensacjusz Diakon został wysłany na prowincję pod przykrywką Lekarza Rekinów, by dojść ze Stellą do tego czemu Amelia i Roland Sowińscy zachowują się nietypowo. Na miejscu okazało się, że Roland jest w stanie ostrej wojny z mafią (która nie traktuje tego poważnie; Kajrat się bawi z Rekinami) a Amelia po prostu nie w pełni sobie radzi i próbuje opanować sytuację. Roland jest Skażony Esuriit; Sensacjusz przetransportował go glizdą do Aurum i sam, uwięziony w bojowej formie, deeskalował konflikt z mafią. A Amelia jest tymczasowym dowódcą Rekinów...

Aktor w Opowieści:

* Dokonanie:
    * ojej, Rekinki zdecydowały się na pomoc lokalną. To słodkie i fajne. Ale wchodzą w szkodę Grzymościowi? Ok - czas na darmowy trening. Maksymalizacja upokorzenia.


### Ratuj młodzież dla Kajrata

* **uid:** 230627-ratuj-mlodziez-dla-kajrata, _numer względny_: 6
* **daty:** 0095-08-20 - 0095-08-25
* **obecni:** AJA Szybka Strzała, Amanda Kajrat, Elena Samszar, Ernest Kajrat, Herbert Samszar, Impresja Ignicja Incydencja Diakon, Karolinus Samszar, Mitria Ira, Talia Aegis, Wacław Samszar, Wargun Ira

Streszczenie:

Herbert powiedział o lokacjach baz i że to prace na duchach. Strzała wie o tym że ma uszkodzoną psychotronikę - skontaktowała Zespół z Kajratem (S. potrzebuje pomocy Talii). Kajrat da dostęp do Talii, jeśli Zespół pomoże uratować noktiańskich młodych magów z niewoli Samszarów. Po znalezieniu bazy Karolinus wywabił na Itrię (którą dołączył do drużyny XD) Wacława (strażnika i ekstraktora) a Elena z Amandą Kajrat wydobyły nieszczęsnych noktian. Strzała odjechała do Kajrata. Kajrat zostawił Zespołowi Amandę do pomocy.

Aktor w Opowieści:

* Dokonanie:
    * skorzystał z desperacji Strzały odnośnie psychotroniki i zapewnił wsparcie Talii w zamian za uratowanie dwóch noktiańskich magów z niewoli Samszarów. Wysłał młodym Samszarom Amandę.


### Operacja: mag dla Symlotosu

* **uid:** 230906-operacja-mag-dla-symlotosu, _numer względny_: 5
* **daty:** 0081-06-26 - 0081-06-28
* **obecni:** Amanda Kajrat, Caelia Calaris, Dragan Halatis, Ernest Kajrat, Leon Varkas, Lestral Kirmanik, Xavera Sirtas

Streszczenie:

Kajrat odkrył, że to Wolny Uśmiech stoi za porwaniem Furii. Wysyła Xaverę i Amandę do Zaczęstwa, by pozyskały maga i zrobiły kanał przerzutowy. Furiom w Quispisie udało się dotrzeć do Wielkiego Muru Pustogorskiego i go przekroczyć, acz skończyły bez servarów. Mają jednak po drugiej stronie ukryte zapasy oraz kontakt do agenta po stronie Szczelińca...

Aktor w Opowieści:

* Dokonanie:
    * poznał los Furii i nie zamierza tego tak zostawić. Chce odzyskać od Grzymościa Furie; wysłał Amandę i Xaverę, by one porwały maga i zbudowały kanał przerzutowy.


### Zwiad w Iliminar Caos

* **uid:** 230806-zwiad-w-iliminar-caos, _numer względny_: 4
* **daty:** 0081-06-22 - 0081-06-24
* **obecni:** Alaric Rakkeir, Amanda Kajrat, Caelia Calaris, Dragan Halatis, Edmund Garzin, Ernest Kajrat, Isaura Velaska, Leon Varkas, Lestral Kirmanik

Streszczenie:

Kajrat przedstawił nową misję - ratujemy noktian i podpinamy się pod Sojusz Letejski. Wysłał Amandę i Quispis, by wysłali sygnał używając dawno rozbitego _supply ship_ Iliminar Caos. Grupa wydzielona Quispis wykryła, że tam są salvagerzy - udało im się zastraszyć ich (udając że są Czarnymi Czaszkami) i zmusić do współpracy. Mają też jednego niewolnika salvagerów jako intel dla Kajrata.

Aktor w Opowieści:

* Dokonanie:
    * major logistyki oddziałów szturmowych Noctis; przejął kontrolę nad siłami Noctis na Astorii (co wyszło z dyskusji z Garzinem). Symbol lepszego jutra dla noktian. Kazał zabezpieczyć intel i zasoby z rozbitej jednostki Iliminar Caos. Dla niego Furie - infiltratorki - są kluczem do integracji i wbicia się w struktury Astorii.


### Skażone schronienie w Fortecy

* **uid:** 230730-skazone-schronienie-w-fortecy, _numer względny_: 3
* **daty:** 0081-06-18 - 0081-06-21
* **obecni:** Amanda Kajrat, Ayna Marialin, Ernest Kajrat, Isaura Velaska, Leira Euridis, Raab Navan, Xavera Sirtas

Streszczenie:

Po rozbiciu awiana Furie nie mają już zasobów ani możliwości. Mimo, że Czarne Czaszki na nie polowały, uciekły do Fortecy Symlotosu (nie wiedząc co to jest). Tam dostały pomoc i spotkały Leirę (kolejną Furię) która zaakceptowała Symlotos. Furie dzielnie broniły się przed Skażeniem, skupiając się na pomocy Symlotosowi i zwalczaniem Czaszek. Niestety, operacja zwabiania Czaszek w pułapkę skończyła się ciężką raną Xavery. Ale Kajrat wspierany przez kolejną Furię, Isaurę, pozyskał Amandę i Xaverę, po czym otworzył negocjacje z Symlotosem by odzyskać Leirę i Aynę za maga.

Aktor w Opowieści:

* Dokonanie:
    * oficer logistyczny jednostki wsparcia; ma niewielką bazę w obszarze Enklaw. Wyciągnął Amandę i Xaverę i negocjuje z Symlotosem oddanie Ayna i Leiry za maga oraz pomoc z Czaszkami. Pragmatyk jak cholera.


### Furia poluje na Furie

* **uid:** 230729-furia-poluje-na-furie, _numer względny_: 2
* **daty:** 0081-06-17 - 0081-06-18
* **obecni:** Amanda Kajrat, Ayna Marialin, Ernest Kajrat, Ralena Karimin, Xavera Sirtas

Streszczenie:

Servary przestają działać. Amanda pozyskała stash od Kajrata z gniazda latających jaszczurów, acz kosztem swojego servara. Jedyny sprawny - Xavery - został zmodowany jako jednostka transportowa. Niestety, podpalenie gniazda jaszczurów sprawiło, że Ralena - kontrolowana przez KOGOŚ Furia - zaatakowała siłą 8 servarów i 2 awianów. Zespół dał radę zdjąć 1 awiana a drugiego porwać i jakkolwiek wszystkie Furie są zatrute, Xavera ranna, Ayna bardzo ciężko ranna, ale mają kilkanaście kilometrów przewagi nad atakującymi. Furie dzielnie się bronią, ale zaczynają przegrywać...

Aktor w Opowieści:

* Dokonanie:
    * ostrzegł wszystkie Furie o magii mentalnej i porozkładał _stashe_ pomagające Furiom które przetrwały. Monitoruje teren by przechwycić Furie.


### Crashlanding Furii Mataris

* **uid:** 230723-crashlanding-furii-mataris, _numer względny_: 1
* **daty:** 0081-06-15 - 0081-06-17
* **obecni:** Amanda Kajrat, Ayna Marialin, Ernest Kajrat, Lucia Veidril, Xavera Sirtas

Streszczenie:

Noktiańska jednostka przewożąca Furie Mataris została zestrzelona i część Furii spadło w okolice Pustogoru. Astorianie polują na Furie, Kajrat je ostrzega i formuje swoje Nocne Niebo. Amanda odzyskała kontakt z dwoma innymi Furiami i przechodzą przez tereny na południu Pustogoru, próbując dostać się do Pacyfiki. Tam czeka na nie Kajrat. A po drodze - rozbity dziwny noktiański statek, polujący na Furie itp.

Aktor w Opowieści:

* Dokonanie:
    * próbuje uratować jak najwięcej Furii Mataris swoim pomniejszym oddziałem szturmowym który uruchomił protokół Nocnego Nieba. Ostrzegł Furie przed potencjalnymi ludzkimi drapieżnikami i działa jak może.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 19, @: 0110-07-23
    1. Primus    : 19, @: 0110-07-23
        1. Sektor Astoriański    : 19, @: 0110-07-23
            1. Astoria    : 19, @: 0110-07-23
                1. Sojusz Letejski, SW    : 7, @: 0110-05-31
                    1. Granica Anomalii    : 7, @: 0110-05-31
                        1. Forteca Symlotosu    : 1, @: 0081-06-21
                        1. Las Pusty, okolice    : 2, @: 0110-05-31
                        1. Ruina Iliminar Caos    : 1, @: 0081-06-24
                        1. Ruina miasteczka Kalterweiser    : 2, @: 0081-06-28
                        1. Ruiny Kaliritosa    : 1, @: 0081-06-17
                        1. Wieczna Maszyna, okolice    : 1, @: 0110-05-31
                        1. Wolne Ptaki    : 1, @: 0110-05-21
                            1. Królewska Baza    : 1, @: 0110-05-21
                    1. Wielki Mur Pustogorski    : 1, @: 0081-06-28
                1. Sojusz Letejski    : 13, @: 0110-07-23
                    1. Aurum    : 1, @: 0095-08-25
                        1. Powiat Samszar    : 1, @: 0095-08-25
                            1. Gwiazdoczy, okolice    : 1, @: 0095-08-25
                                1. Technopark Jutra    : 1, @: 0095-08-25
                                    1. Inkubatory małego biznesu    : 1, @: 0095-08-25
                            1. Gwiazdoczy    : 1, @: 0095-08-25
                        1. Verlenland    : 1, @: 0095-08-25
                            1. Duchowiec Śmieszny, okolice    : 1, @: 0095-08-25
                            1. Duchowiec Śmieszny    : 1, @: 0095-08-25
                    1. Szczeliniec    : 12, @: 0110-07-23
                        1. Powiat Pustogorski    : 12, @: 0110-07-23
                            1. Czółenko    : 2, @: 0110-06-09
                                1. Bunkry    : 1, @: 0110-05-25
                            1. Podwiert, okolice    : 1, @: 0110-06-28
                                1. Bioskładowisko podziemne    : 1, @: 0110-06-28
                            1. Podwiert    : 5, @: 0110-06-09
                                1. Bastion Pustogoru    : 1, @: 0110-04-24
                                1. Dolina Biurowa    : 1, @: 0110-05-15
                                1. Dzielnica Luksusu Rekinów    : 1, @: 0108-04-18
                                    1. Serce Luksusu    : 1, @: 0108-04-18
                                        1. Lecznica Rannej Rybki    : 1, @: 0108-04-18
                                1. Iglice Nadziei    : 1, @: 0110-06-07
                                    1. Posiadłość Arieników    : 1, @: 0110-06-07
                                1. Las Trzęsawny    : 1, @: 0108-04-18
                                1. Odlewnia    : 1, @: 0110-04-24
                                1. Osiedle Leszczynowe    : 2, @: 0110-06-09
                                    1. Szkoła Nowa    : 2, @: 0110-06-09
                                1. Sensoplex    : 1, @: 0110-04-24
                            1. Pustogor, okolice    : 1, @: 0110-07-23
                                1. Rezydencja Blakenbauerów    : 1, @: 0110-07-23
                            1. Zaczęstwo    : 6, @: 0110-07-23
                                1. Akademia Magii, kampus    : 1, @: 0110-04-26
                                    1. Budynek Centralny    : 1, @: 0110-04-26
                                        1. Skrzydło Loris    : 1, @: 0110-04-26
                                1. Arena Migświatła    : 1, @: 0110-02-10
                                1. Kawiarenka Leopold    : 1, @: 0110-05-21
                                1. Kompleks Tiamenat    : 1, @: 0110-01-21
                                1. Nieużytki Staszka    : 2, @: 0110-07-23
                        1. Trzęsawisko Zjawosztup    : 1, @: 0110-04-24

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 10 | ((190714-kult-choroba-esuriit; 190726-bardzo-niebezpieczne-skladowisko; 200311-wygrany-kontrakt; 211120-glizda-ktora-leczy; 230627-ratuj-mlodziez-dla-kajrata; 230723-crashlanding-furii-mataris; 230729-furia-poluje-na-furie; 230730-skazone-schronienie-w-fortecy; 230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Pięknotka Diakon     | 10 | ((190505-szczur-ktory-chroni; 190519-uciekajacy-seksbot; 190616-anomalna-serafina; 190622-wojna-kajrata; 190623-noc-kajrata; 190709-somnibel-uciekl-arienikom; 190714-kult-choroba-esuriit; 190726-bardzo-niebezpieczne-skladowisko; 190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Liliana Bankierz     | 4 | ((190519-uciekajacy-seksbot; 190622-wojna-kajrata; 190623-noc-kajrata; 200311-wygrany-kontrakt)) |
| Tomasz Tukan         | 4 | ((190519-uciekajacy-seksbot; 190616-anomalna-serafina; 190709-somnibel-uciekl-arienikom; 190714-kult-choroba-esuriit)) |
| Xavera Sirtas        | 4 | ((230723-crashlanding-furii-mataris; 230729-furia-poluje-na-furie; 230730-skazone-schronienie-w-fortecy; 230906-operacja-mag-dla-symlotosu)) |
| Ayna Marialin        | 3 | ((230723-crashlanding-furii-mataris; 230729-furia-poluje-na-furie; 230730-skazone-schronienie-w-fortecy)) |
| Serafina Ira         | 3 | ((190616-anomalna-serafina; 190622-wojna-kajrata; 190623-noc-kajrata)) |
| Talia Aegis          | 3 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek; 230627-ratuj-mlodziez-dla-kajrata)) |
| Caelia Calaris       | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Dragan Halatis       | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Isaura Velaska       | 2 | ((230730-skazone-schronienie-w-fortecy; 230806-zwiad-w-iliminar-caos)) |
| Krystian Namałłek    | 2 | ((190505-szczur-ktory-chroni; 190616-anomalna-serafina)) |
| Leon Varkas          | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Lestral Kirmanik     | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Lucjusz Blakenbauer  | 2 | ((190505-szczur-ktory-chroni; 200311-wygrany-kontrakt)) |
| Marek Puszczok       | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Minerwa Metalia      | 2 | ((190828-migswiatlo-psychotroniczek; 200311-wygrany-kontrakt)) |
| Nikola Kirys         | 2 | ((190622-wojna-kajrata; 190626-upadek-enklawy-floris)) |
| Ossidia Saitis       | 2 | ((190519-uciekajacy-seksbot; 190623-noc-kajrata)) |
| Tymon Grubosz        | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Wargun Ira           | 2 | ((190626-upadek-enklawy-floris; 230627-ratuj-mlodziez-dla-kajrata)) |
| Adela Kirys          | 1 | ((190505-szczur-ktory-chroni)) |
| AJA Szybka Strzała   | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Alan Bartozol        | 1 | ((190505-szczur-ktory-chroni)) |
| Alaric Rakkeir       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Amelia Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Antoni Żuwaczka      | 1 | ((190616-anomalna-serafina)) |
| Ariela Sirmin        | 1 | ((190626-upadek-enklawy-floris)) |
| Arnulf Poważny       | 1 | ((190519-uciekajacy-seksbot)) |
| Artur Michasiewicz   | 1 | ((190828-migswiatlo-psychotroniczek)) |
| BIA Tarn             | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Cezary Alentik       | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Dagmara Doberman     | 1 | ((200311-wygrany-kontrakt)) |
| Edmund Garzin        | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Elena Samszar        | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Eliza Ira            | 1 | ((190519-uciekajacy-seksbot)) |
| Feliks Keksik        | 1 | ((211120-glizda-ktora-leczy)) |
| Gabriel Ursus        | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Herbert Samszar      | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Hubert Kraborów      | 1 | ((190626-upadek-enklawy-floris)) |
| Impresja Ignicja Incydencja Diakon | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Jan Revlen           | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Jolanta Teresis      | 1 | ((190626-upadek-enklawy-floris)) |
| Justynian Diakon     | 1 | ((211120-glizda-ktora-leczy)) |
| Kacper Bankierz      | 1 | ((211120-glizda-ktora-leczy)) |
| Karol Pustak         | 1 | ((211120-glizda-ktora-leczy)) |
| Karolinus Samszar    | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Kasjan Czerwoczłek   | 1 | ((190505-szczur-ktory-chroni)) |
| Konrad Czukajczek    | 1 | ((190626-upadek-enklawy-floris)) |
| Ksawery Wojnicki     | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Leira Euridis        | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Lucia Veidril        | 1 | ((230723-crashlanding-furii-mataris)) |
| Marcel Sowiński      | 1 | ((190626-upadek-enklawy-floris)) |
| Mariusz Trzewń       | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Mirela Orion         | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Mitria Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Olaf Zuchwały        | 1 | ((190622-wojna-kajrata)) |
| Oliwia Lemurczak     | 1 | ((211120-glizda-ktora-leczy)) |
| Oliwia Namałłek      | 1 | ((190505-szczur-ktory-chroni)) |
| Raab Navan           | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Ralena Karimin       | 1 | ((230729-furia-poluje-na-furie)) |
| Roland Grzymość      | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Roland Sowiński      | 1 | ((211120-glizda-ktora-leczy)) |
| Roman Rymtusz        | 1 | ((190626-upadek-enklawy-floris)) |
| Ronald Grzymość      | 1 | ((190616-anomalna-serafina)) |
| Sabina Kazitan       | 1 | ((211120-glizda-ktora-leczy)) |
| Saitaer              | 1 | ((190519-uciekajacy-seksbot)) |
| Sensacjusz Diakon    | 1 | ((211120-glizda-ktora-leczy)) |
| Sławomir Niejadek    | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Staś Arienik         | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Stella Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Szymon Maszczor      | 1 | ((190626-upadek-enklawy-floris)) |
| Teresa Mieralit      | 1 | ((190519-uciekajacy-seksbot)) |
| Urszula Arienik      | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Wacław Samszar       | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Wiktor Satarail      | 1 | ((190505-szczur-ktory-chroni)) |
| Ziemowit Zięba       | 1 | ((200311-wygrany-kontrakt)) |