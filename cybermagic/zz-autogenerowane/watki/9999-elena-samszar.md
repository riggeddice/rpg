# Elena Samszar
## Identyfikator

Id: 9999-elena-samszar

## Sekcja Opowieści

### Piękna Diakonka i rytuał nirwany kóz

* **uid:** 230606-piekna-diakonka-i-rytual-nirwany-koz, _numer względny_: 7
* **daty:** 0095-08-15 - 0095-08-18
* **obecni:** AJA Szybka Strzała, Dźwiedź Łagodne Słowo, Elena Samszar, Herbert Samszar, Impresja Ignicja Incydencja Diakon, Karolinus Samszar, Maks Samszar

Streszczenie:

Itria Diakon ma kolejny projekt - nirwana kóz u Samszarów. Jej obecność spowodowała chaos w Forcie Tawalizer. Karolinus i Elena S. dotarli tam by dowiedzieć się o tajnych podziemnych bazach Samszarów od Herberta, ale musieli wpierw rozwiązać problem z Itrią. W końcu doprowadzili do rytuału kóz (dzięki Elenie i jej researchowi) i trochę naprawili sytuację. Czas spotkać się z Herbertem poza terenem sentisieci i poznać sekrety badań nad duchami.

Aktor w Opowieści:

* Dokonanie:
    * znalazła rytuał nirwany kóz szperając po bibliotekach, ale nie poszukała bardzo głęboko by nie musieć się przyznawać kolegom z biblioteki. Potem nauczyła Maksa tego rytuału, ale nie zadbała o dokładność - bo to i tak będzie tylko raz czy dwa razy a nie będzie się upokarzać. Nie chce patrzeć na kolesia przebranego za kozę.
* Progresja:
    * wysłała do Vioriki informację o tym, że Romeo spieprzył operację. Romeo powiedział Viorice, że Elena S. sobie z nim nie radziła. Wniosek Verlenów: Elena jest 'słaba' i 'irytująca'.


### Romeo, dyskretny instalator Supreme Missionforce

* **uid:** 230523-romeo-dyskretny-instalator-supreme-missionforce, _numer względny_: 6
* **daty:** 0095-08-09 - 0095-08-11
* **obecni:** AJA Szybka Strzała, Albert Samszar, Elena Samszar, Karolinus Samszar, Maja Samszar, Nataniel Samszar, Romeo Verlen

Streszczenie:

Strzała ma za zadanie wychować i nauczyć współpracy Karolinusa i Eleny. Karolinus jest wezwany do interwencji, gdy Maja przesyła sygnał SOS. Maja twierdzi, że niechcący mogła zabić kolegę. Ekipa dociera na miejsce, gdzie odkrywają ciężarówkę z zaawansowanym sprzętem. Maja jest w stanie panicznym, a jej magia wymyka się spod kontroli - próbuje przebić dziwne pole siłowe otaczające dziurę w piwnicy biurowca. Karolinus i Elena zmuszają Maję do opuszczenia miejsca, obiecując uratować Romeo. W międzyczasie, Strzała zabiera Maję do Ogrodów Medytacyjnych by nie miała problemów z ojcem. Elena zyskuje na czasie przed ojcem Mai (Albertem), dopóki Maja nie jest bezpiecznie umieszczona w Ogrodach. Potem wydobywają Romeo z podziemi zauważając, że ktoś tam ma tajną dziwną bazę do eksperymentów na duchach.

Aktor w Opowieści:

* Dokonanie:
    * przeszła przez sentisieć i wykryła obecność monterów w ciężarówce i zmiany w sentisieci (acz zaalarmowała wszystkie strony łącznie z Albertem). Bablała Albertowi kupując czas Strzale na ewakuację Mai, nawet kosztem swojej reputacji. Na końcu połączyła się z dziwnym duchem, co ją wyłączyło z akcji na moment.
* Progresja:
    * zdaniem Alberta Samszara, gdy pije to nie da się z nią dogadać i jest niezwykle irytująca. Ogólnie - zwykle niegodna uwagi.


### Karolinka - raciczki zemsty Verlenów

* **uid:** 230516-karolinka-raciczki-zemsty-verlenow, _numer względny_: 5
* **daty:** 0095-07-29 - 0095-07-31
* **obecni:** Aleksander Samszar, Amara Zegarzec, Elena Samszar, Franciszek Chartowiec, Karolinus Samszar, Ludmiła Zegarzec

Streszczenie:

Karolinka, świnka podłożona przez Verlenów na Wielkie Kwiatowisko zaczęła polować na lokalne duchy a przedsiębiorcza Ludmiła z pobliskiego hotelu zorganizowała dziennikarza Paktu i okazję do zarobienia. Karolinus i Elena przebili się przez problematycznego dziennikarza Paktu, zwabili świnkę do pojazdu i uśpili oraz podrzucili ją (rękami żołnierzy) do Verlenlandu. A Strzała jest naprawiana.

Aktor w Opowieści:

* Dokonanie:
    * chciała uniknąć straty twarzy, ale bardziej chciała chronić duchy przed glukszwajnem. Unikała prasy, ale rzuciła w świnkę jabłkiem. Zniszczyła drony dziennikarza "przypadkiem".
* Progresja:
    * na wideo Paktu gdy zwalczali glukszwajna jako "opiekunka duchów i obrończyni ich przed świnią". Popularność wśród Paktu rośnie.


### Samszarowie, Lemurczak i fortel Strzały

* **uid:** 230509-samszarowie-lemurczak-i-fortel-strzaly, _numer względny_: 4
* **daty:** 0095-07-24 - 0095-07-26
* **obecni:** AJA Szybka Strzała, Elena Samszar, Irek Kraczownik, Jonatan Lemurczak, Karolinus Samszar, Roland Samszar

Streszczenie:

Strzała w ruinie, ale dała radę dotrzeć do w miarę bezpiecznego miejsca pod grzmotoptakami. Gdy dwójka nastolatków na które poluje ich zmieniona przez Lemurczaka matka się pojawili blisko, Elena ją unieruchomiła a Karolinus przekształcił w normalną formę. Acz Paradoksem wysłał sygnaturę Verlenopodobną. Gdy Karolinus i Elena się kłócą czy pomóc czy czekać, Strzała pojedynczą droną wymanewrowała Stegozaur-class support hovertank i przestraszyła Lemurczaka hintując, że Verlenowie polujący na ptaki są w pobliżu. Elena zmanipulowała Irka, więc E+K wyszli na osoby pozytywne które chcą dobrze, acz nie zawsze mają idealne plany (bo są młodzi). A Roland zajmie się Sanktuarium Kazitan.

Aktor w Opowieści:

* Dokonanie:
    * duża wrogość do egzorcysty Irka; nie chce ocieplać stosunków. Unieruchomiła magią przekształconą przez Lemurczaka matkę nastolatków. Zmanipulowała Irka, by ten powiedział że Elena i Karolinus są po właściwej stronie i on nie był porwany tylko ich potrzebował. Dzięki temu Samszarowie wyszli na bohaterów (acz jeszcze nieudolnych bo młodych) a nie na potwory z Aurum XD.


### Egzorcysta z Sanktuarium

* **uid:** 230411-egzorcysta-z-sanktuarium, _numer względny_: 3
* **daty:** 0095-07-21 - 0095-07-23
* **obecni:** AJA Szybka Strzała, Arnold Kazitan, Elena Samszar, Irek Kraczownik, Karolinus Samszar, Tadeusz Dzwańczak

Streszczenie:

Egzorcysta Irek poszukiwany przez Samszarów jest w Sanktuarium Kazitan w Przelotyku Zachodnim Dzikim. Na miejscu Sanktuarium ulega Emisji - katastroficzne elementalne działania. Zespół ratuje dzieciaka od Emisji (najpierw mu zagrażając XD), ale gdy dociera do egzorcysty - ostatniego maga który próbuje pomóc Sanktuarium, to go porywają. Przy próbie porwania Strzała zostaje ciężko uszkodzona i nie daje rady wrócić do Powiatu Samszar - crashlanduje w bezpiecznej części Przelotyka.

Aktor w Opowieści:

* Dokonanie:
    * nie jest zainteresowana pomaganiem dziecku, ale nie chce niszczyć Sanktuarium. Jednak sprawiedliwość i "swoi ludzie" muszą być uratowani. Skonfliktowana, pozwala Karolinusowi porwać Irka. Bardziej pasywna rola, nie wie co robić w zastałej sytuacji.


### Wszystkie duchy Siewczyna

* **uid:** 230404-wszystkie-duchy-siewczyna, _numer względny_: 2
* **daty:** 0095-07-18 - 0095-07-20
* **obecni:** AJA Szybka Strzała, Elena Samszar, Irek Kraczownik, Karolinus Samszar, Maksymilian Sforzeczok

Streszczenie:

Karolinus, wraz z kuzynką Eleną Samszar zostali wysłani do Siewczyna gdzie podobno są problemy (by uniknąć dalszego antagonizowania Verlenów). Na miejscu okazało się, że niekompetentny egzorcysta Irek sprowadził tu Hybrydę noktiańskiej TAI i ducha oraz ten byt próbował zemścić się za śmierć swoich ludzi. Elena Paradoksem zniszczyła Ducha Opiekuńczego Spichlerza a Karolinus Paradoksem stworzył strażniczego ducha w kształcie Vioriki w bikini. A w tle - spory między ludźmi i duchami (podsycane przez Hybrydę) oraz między podejściem 'ekonomia vs harmonia'.

Aktor w Opowieści:

* Dokonanie:
    * przesłuchiwała jako tienka ludzi pracujących dla lokalnego dyrektora nie lubiącego magii; magicznie połączyła się ze Strażnikiem Spichlerza i Paradoksem dała Hybrydzie owego Strażnika zniszczyć. Ale przekonała Hybrydę, że egzorcysta jest winny i kupiła czas Karolinusowi i Strzale.
* Progresja:
    * zniszczyła lokalnego Ducha Strażniczego Spichlerza, który istniał 80 lat. W Siewczynie jej tego nie zapomną...


### Żywy artefakt w Gwiazdoczach

* **uid:** 230418-zywy-artefakt-w-gwiazdoczach, _numer względny_: 1
* **daty:** 0094-10-04 - 0094-10-06
* **obecni:** Adelaida Samszar, Antonina Blakenbauer, Elena Samszar, Joachim Pulkmocz, Neidria Lazvarin, Robinson Porzecznik, Sara Mazirin

Streszczenie:

Elena Samszar została poproszona przez bibliotekarza o pomoc - jego kuzyn zakochał się (przez inną Samszarkę) w poezji wierszy Verlenów o niedźwiedziach. Elena poszła pomóc, ale okazało się, że to coś dziwnego. Zebrała wsparcie - Robinson i Antonina i doszli do tego, że to "żywy tatuaż Esuriit" na człowieku. Niuchacze doprowadziły ich do posiadaczki tatuażu i Elena, używając sentisieci, złapała zarówno unsealowanego ducha który podpowiadał jak używać Esuriit jak i nieszczęśniczkę z tatuażem. A w tle - Paradoks z niedźwiedziogorami śpiewającymi kiepską poezję zwalczanymi przez sentisieć.

Aktor w Opowieści:

* Dokonanie:
    * gdy Joachim się do niej zwrócił, że Samszarka przeklęła jego kuzyna, zajęła się sprawą. Zebrała ekipę - turysta Porzecznik, koleżanka Blakenbauer - i doszła do tego, że to nie był czar a 'żywy artefakt', tatuaż Esuriit. Po zlokalizowaniu ofiary, zamknęła ją sentisiecią w komnacie i złapała też ducha doradzającego w sprawie Esuriit zanim sekrety jak duch został unsealowany zanikną.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 7, @: 0095-08-18
    1. Primus    : 7, @: 0095-08-18
        1. Sektor Astoriański    : 7, @: 0095-08-18
            1. Astoria    : 7, @: 0095-08-18
                1. Sojusz Letejski    : 7, @: 0095-08-18
                    1. Aurum    : 5, @: 0095-08-18
                        1. Powiat Samszar    : 5, @: 0095-08-18
                            1. Fort Tawalizer    : 1, @: 0095-08-18
                                1. Obserwatorium Potworów i Fort (E)    : 1, @: 0095-08-18
                                    1. Areszt    : 1, @: 0095-08-18
                                    1. Koszary garnizonu    : 1, @: 0095-08-18
                                1. Wzgórza Potworów (N)    : 1, @: 0095-08-18
                                    1. Farmy kóz    : 1, @: 0095-08-18
                            1. Gwiazdoczy    : 1, @: 0094-10-06
                                1. Centrum (Centrum)    : 1, @: 0094-10-06
                                    1. Puby i restauracje    : 1, @: 0094-10-06
                                    1. Sklepy i kawiarnie    : 1, @: 0094-10-06
                                1. Dzielnica Akademicka (NW, W)    : 1, @: 0094-10-06
                                    1. Czytelnie naukowe    : 1, @: 0094-10-06
                                    1. Kampus uczelniany    : 1, @: 0094-10-06
                                    1. Muzeum Historii    : 1, @: 0094-10-06
                                    1. Uniwersytet (NW)    : 1, @: 0094-10-06
                                    1. Wielka Biblioteka (W)    : 1, @: 0094-10-06
                                1. Dzielnica studencka (N)    : 1, @: 0094-10-06
                                    1. Strefa Sportowa    : 1, @: 0094-10-06
                                1. Dzielnica Technologii (S)    : 1, @: 0094-10-06
                                    1. Centralna stacja pociągów    : 1, @: 0094-10-06
                                    1. Centrum Eszary    : 1, @: 0094-10-06
                                    1. Park Technologiczny    : 1, @: 0094-10-06
                                    1. Warsztaty i inkubatory    : 1, @: 0094-10-06
                                1. Szczelina Światów (E)    : 1, @: 0094-10-06
                                    1. Archiwa duchów    : 1, @: 0094-10-06
                                    1. Instytut Sztuki Wspomaganej    : 1, @: 0094-10-06
                                    1. Kalejdoskop Astralny    : 1, @: 0094-10-06
                                    1. Magitrownie puryfikacyjne    : 1, @: 0094-10-06
                                    1. Plac rytuałów    : 1, @: 0094-10-06
                                    1. Wielki Cenotaf Skupiający    : 1, @: 0094-10-06
                            1. Karmazynowy Świt, okolice    : 1, @: 0095-08-11
                                1. Centrum Danych Symulacji Zarządzania    : 1, @: 0095-08-11
                                    1. Techbunkier Arvitas    : 1, @: 0095-08-11
                                        1. Kontrola bezpieczeństwa (1)    : 1, @: 0095-08-11
                                        1. Kwatery mieszkalne (1)    : 1, @: 0095-08-11
                            1. Karmazynowy Świt    : 1, @: 0095-08-11
                            1. Siewczyn    : 1, @: 0095-07-20
                                1. Astralne Ogrody (ES)    : 1, @: 0095-07-20
                                    1. Centralne Biura Rolnicze    : 1, @: 0095-07-20
                                    1. Centrum R&D dla zrównoważonego rolnictwa    : 1, @: 0095-07-20
                                    1. Dom Szamana    : 1, @: 0095-07-20
                                    1. Fabryka neutralizatorów astralnych    : 1, @: 0095-07-20
                                    1. Gaj Duchów    : 1, @: 0095-07-20
                                    1. Rezydencje Harmonii    : 1, @: 0095-07-20
                                1. Centrum Jedności Mieszka (Center)    : 1, @: 0095-07-20
                                    1. Bazar rękodzieła    : 1, @: 0095-07-20
                                    1. Drzewo Jedności    : 1, @: 0095-07-20
                                    1. Most jedności    : 1, @: 0095-07-20
                                    1. Park miejski    : 1, @: 0095-07-20
                                    1. Ratusz miejski    : 1, @: 0095-07-20
                                1. Północne obrzeża    : 1, @: 0095-07-20
                                    1. Spichlerz Jedności    : 1, @: 0095-07-20
                                1. Wzgórza Industrialnej Harmonii (NW)    : 1, @: 0095-07-20
                                    1. Fabryka siewników i sadzarek    : 1, @: 0095-07-20
                                    1. Fabryka traktorów    : 1, @: 0095-07-20
                                    1. Przestrzeń mieszkalna    : 1, @: 0095-07-20
                                    1. Sklepy i centra handlowe    : 1, @: 0095-07-20
                            1. Wielkie Kwiatowisko    : 1, @: 0095-07-31
                                1. Hotel Odpoczynek Pszczół    : 1, @: 0095-07-31
                                1. Menhir Centralny    : 1, @: 0095-07-31
                    1. Przelotyk    : 2, @: 0095-07-26
                        1. Przelotyk Zachodni Dziki    : 2, @: 0095-07-26
                            1. Lancatim, okolice    : 1, @: 0095-07-26
                            1. Lancatim    : 1, @: 0095-07-26
                            1. Sanktuarium Kazitan    : 1, @: 0095-07-23
                                1. Dystrykt Szafir    : 1, @: 0095-07-23
                                    1. Komnata lecznicza    : 1, @: 0095-07-23
                                    1. Komnata mieszkalna    : 1, @: 0095-07-23

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolinus Samszar    | 6 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230516-karolinka-raciczki-zemsty-verlenow; 230523-romeo-dyskretny-instalator-supreme-missionforce; 230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| AJA Szybka Strzała   | 5 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230523-romeo-dyskretny-instalator-supreme-missionforce; 230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Irek Kraczownik      | 3 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Adelaida Samszar     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Albert Samszar       | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Aleksander Samszar   | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Amara Zegarzec       | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Antonina Blakenbauer | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Arnold Kazitan       | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Dźwiedź Łagodne Słowo | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Franciszek Chartowiec | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Herbert Samszar      | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Impresja Ignicja Incydencja Diakon | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Joachim Pulkmocz     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Jonatan Lemurczak    | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Ludmiła Zegarzec     | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Maja Samszar         | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Maks Samszar         | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Maksymilian Sforzeczok | 1 | ((230404-wszystkie-duchy-siewczyna)) |
| Nataniel Samszar     | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Neidria Lazvarin     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Robinson Porzecznik  | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Roland Samszar       | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Romeo Verlen         | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Sara Mazirin         | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Tadeusz Dzwańczak    | 1 | ((230411-egzorcysta-z-sanktuarium)) |