# Leona Astrienko
## Identyfikator

Id: 2206-leona-astrienko

## Sekcja Opowieści

### Ratujemy porywaczy Eleny

* **uid:** 220610-ratujemy-porywaczy-eleny, _numer względny_: 33
* **daty:** 0112-09-15 - 0112-09-17
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Hestia d'Atropos, Jakub Bulgocz, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Raoul Lavanis

Streszczenie:

Elena jest już zbyt zdestabilizowana by funkcjonować niezależnie, więc wpierw zajęli się nią w Laboratorium Kranix a po zbudowaniu dla niej Containment Chamber przesłali ją do stacji medycznej Atropos do regeneracji. Stamtąd ją i Martyna porwał Syndykat Aureliona (ale Martyn przewidział JAKIŚ problem i ostrzegł Klaudię). Elena by zniszczyła okręt napastników, ale Zespół skutecznie odkrył problemy na Atropos, przesłuchali dowódcę, zneutralizowali miragenta i dotarli do okrętu Aureliona zdobywając jeńców i zatrzymując Elenę przed przebudzeniem i Spustoszeniem statku.

Aktor w Opowieści:

* Dokonanie:
    * po dramatycznej augmentacji (cobra-class) miała okazję zaatakować statek niewolniczy Aureliona. Zrobiła apokalipsę. Próbowała się powstrzymać, ale część osób wyginęła - testuje nowe augmentacje.
* Progresja:
    * za zgodą Arianny i z opłaty admirał Termii została wzmocniona i przebudowana na najgroźniejszego techno-cirrusa. -20 lat życia, -komfort, +siła ognia.


### Sklejanie Inferni do kupy

* **uid:** 211215-sklejanie-inferni-do-kupy, _numer względny_: 32
* **daty:** 0112-04-27 - 0112-04-29
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Kamil Lyraczek, Kasandra Destrukcja Diakon, Leona Astrienko, Maria Naavas, OO Infernia, Roland Sowiński

Streszczenie:

Ixioński wariant Inferni okazuje się całkiem słodki i pod kontrolą Eustachego. Morale Inferni jest w ruinie. Z pomocą Marii Arianna przesuwa kulturę Inferni w stronę "kultyści + noktianie + miłośnicy psychopatycznej lolitki Diany". Eustachy wpada w pułapkę Leony, która chce go wyleczyć czerwiem Esuriit - ale Eustachy przekonuje ją, że ma Dianę pod kontrolą. Jednak gdzieś w tle znajduje się prawdziwy ixioński potwór na stoczni Neotik.

Aktor w Opowieści:

* Dokonanie:
    * zapolowała na Eustachego by go uratować od ixionu; zmanipulowała Kasandrę Diakon, by ta jej pomogła. Zastawiła pułapkę, używając czerwia Esuriit ZRANIŁA Eustachego. Dała się jednak mu przekonać, że on kontroluje Dianę. Została jako morderca magów; ktoś musi skończyć Eustachego i Ariannę jak nie będzie wyjścia...


### O krok za daleko

* **uid:** 211208-o-krok-za-daleko, _numer względny_: 31
* **daty:** 0112-04-25 - 0112-04-26
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Hestia d'Neotik, Jarosław Szarjan, Klaudia Stryk, Leona Astrienko, OO Infernia, Wawrzyn Rewemis

Streszczenie:

Klaudia doszła do tego, że Hestia d'Neotik oszukuje Stocznię; w okolicy są niewielkie statki koloidowe o których Stocznia nie wie. Arianna zintegrowała Eustachego z Infernią ixiońsko, niestety anomalizując Infernię. Elena ciężko Skażona ixionem i (w swoim mniemaniu) porzucona przez Eustachego. Eustachy ujarzmiając Infernię rozbijał ją o różne asteroidy, co powoduje śmierć części załogi. Infernia ma zerowe morale, ale jest opanowana. Przyjazna Anomalia Kosmiczna z tymczasową bazą w Stoczni Neotik.

Aktor w Opowieści:

* Dokonanie:
    * podczas ujeżdżania Inferni przez Eustachego Wawrzyn ją uratował i zginął. Leona zaatakowała komputery Inferni z pełnej mocy. Ciężko przeszła śmierć Wawrzyna.


### Prototypowa Nereida Natalii

* **uid:** 211124-prototypowa-nereida-natalii, _numer względny_: 30
* **daty:** 0112-04-20 - 0112-04-24
* **obecni:** Adam Szarjan, Arianna Verlen, Arkadia Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Maria Naavas, Natalia Aradin, OO Infernia, Roland Sowiński, Wawrzyn Rewemis

Streszczenie:

Kampania reklamowa "dołącz do Inferni" zrobiona przez Izę przy użyciu Eustachego jako modela - sukces. Klaudia zdobyła z tego nowych załogantów (acz to jeszcze potrwa). Szarjan uwolnił Klaudię z zarzutów, potrzebuje pomocy w stoczni - jego przyjaciółka Natalia ma problem z prototypem myśliwca klasy Nereida i została ixiońsko zintegrowana. Infernia próbowała Nereidę uratować, ale niestety skończyło się to poważnym uszkodzeniem Nereidy i stworzeniem ixiońskiego kokonu, z którego Natalia musi się wykluć...

Aktor w Opowieści:

* Dokonanie:
    * pokłóciła się ze swoim medykiem (Wawrzynem), rzuciła weń czymś wybuchowym a on odrzucił. Polubiła go trochę. Ma z kim się kłócić. Ma "swojego medyka".


### Porwany Trismegistos

* **uid:** 211117-porwany-trismegistos, _numer względny_: 29
* **daty:** 0112-04-15 - 0112-04-17
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Jamon Korab, Kalira d'Trismegistos, Klara Gwozdnik, Klaudia Stryk, Leona Astrienko, Maria Naavas, Mira Anastel, OO Tivr, Roland Sowiński, SC Trismegistos, Zygfryd Maus

Streszczenie:

Wiadomość od pirata że porwał Trismegistos poderwała Ariannę i Tivr. Historia nie trzymała się kupy. Załoga Inferni (Tivru) zorientowała się, że coś jest nie tak - opowieść o statku kupieckim który pozbył się miragenta brzmiała DZIWNIE. Okazało się, że Trismegistos jest statkiem kaperskim. Gdy Trismegistos zażądał poddania się Tivru Klaudia zrobiła sygnał "lol leci na Was Serenit" i TAI Trismegistosa spanikowała. Wolna TAI, współpracująca z fareilem i drakolitką. Wolny dystrykt. Arianna dała im żyć - ale odzyskała porwanych arystokratów i komory adaptacji.

Aktor w Opowieści:

* Dokonanie:
    * słaba i ledwo aktywna, ale weszła na pokład Trismegistosa jako osłona. Dała się "pokonać" magom, ale czekała aż może zaatakować i naprawić sytuację.


### Romans dzięki Esuriit

* **uid:** 211110-romans-dzieki-esuriit, _numer względny_: 28
* **daty:** 0112-04-13 - 0112-04-14
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Karol Reichard, Klaudia Stryk, Leona Astrienko, Maria Naavas, Wawrzyn Rewemis

Streszczenie:

Infernia nie ma dość załogi, by móc działać, więc Termia przekazuje Ariannie Tivr. Leona nie może sobie wybaczyć, że jej nie było i to się stało, więc się wypisuje z pomocą Eustachego (co powoduje plotki Leona x Eustachy). Elena ucieka do Sektora 49 na K1 by być samotna; Arianna organizuje ostre poszukiwania. Eustachy znajduje Elenę by ją pocieszyć. Elena w końcu przyznaje Eustachemu, że go kocha. Maria Naavas z woli Martyna tymczasowo dołącza do załogi Arianny.

Aktor w Opowieści:

* Dokonanie:
    * w stanie niesprawnym; wyszła ze szpitala, bo Infernia jej potrzebuje. Sama się wypisała z pomocą Eustachego. 20% pełnej mocy.


### Grupa Ekspedycyjna Kellert

* **uid:** 210929-grupa-ekspedycyjna-kellert, _numer względny_: 27
* **daty:** 0112-03-13 - 0112-03-16
* **obecni:** Adam Nerawol, Aleksandra Termia, Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Olena Orion, OO Infernia, OO Omega Septius, Otto Azgorn, TAI Marszałek Grzmotoszpon Trzeci, Vigilus Mevilig

Streszczenie:

Termia wysłała siły do sektora "Noviter", ale to był inny sektor (Mevilig). Straciła bezzałogowce, potem grupę ekspedycyjną Kellert. Kramer wysłał Infernię zakoloidowaną; okazało się, że ten sektor ma "strażnika Esuriit" zrobionego przez ludzi i grupę Piranii, sterowanych przez TAI 4 poziomu. Inferni udało się zebrać dane i wrócić z siedmioma uratowanymi członkami OO Savera, gdzie większość załogi została corruptowana przez Vigilusa...

Aktor w Opowieści:

* Dokonanie:
    * leży w szpitalu; ostrzegła Eustachego, że Martyn jest eternijskim lordem, miał niewolników i ogólnie zniszczy Infernię. Eustachy jest lekko sceptyczny ale ok - będzie uważał.


### Ostatnia akcja bohaterki

* **uid:** 210922-ostatnia-akcja-bohaterki, _numer względny_: 26
* **daty:** 0112-02-23 - 0112-03-09
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Izabela Zarantel, Jolanta Kopiec, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Olgierd Drongon, OO Żelazko, Roland Sowiński

Streszczenie:

Z Martynem skontaktowała się tien Kopiec prosząc o ostatnią misję zanim Esuriit ją pożre. Martyn zorganizował lot Infernią. Ustabilizował Jolantę, by ta dała radę dolecieć słowami i opowieściami, przy okazji tworząc piękną opowieść o Eterni i eulogię dla Jolanty - bohaterki wojennej - w Sekretach Orbitera (przy okazji wyszła jego przeszłość). Infernia zniszczyła Krwawą Bazę Piratów kosztem dewastacji Żelazka. Jolanta zniszczyła krwawego maga. Sektor jest bezpieczniejszy, Eternia szczęśliwa a Arianna ma profity.

Aktor w Opowieści:

* Dokonanie:
    * cieszyła się że zabije DWÓCH tienów eternijskich - Arianna przekonała ją by ta poczekała. Wpierw rozproszona NIE utrzymała terenu (armia minionów ją poraniła), POTEM ryzykując życiem utrzymała simulacrum odstępcy (miesiąc+ szpitala), ale serce jej się złamało, gdy dowiedziała się, że Martyn to honorowy eternijski tien. Jest w szpitalu i nie chce z nikim rozmawiać.
* Progresja:
    * 23 dni od końca tej sesji w szpitalu na Kontrolerze Pierwszym.
    * widzi Martyna jako tiena Eterni - pokazał czynami, zdolnościami i reputacją. EXTREMELY CONFLICTED.


### Stabilizacja Bramy Eterycznej

* **uid:** 210901-stabilizacja-bramy-eterycznej, _numer względny_: 25
* **daty:** 0112-02-09 - 0112-02-12
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Gilbert Bloch, Janus Krzak, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Medea Sowińska, OO Kanagar, OO Netrahina, OO Trasman, Seweryn Atanair

Streszczenie:

Infernia wróciła z innymi jednostkami naprawić Bramę Kariańską. Eustachy ufortyfikował i uniewidocznił pintki; Arianna pozyskała dane z reanimowanego magią krążownika Orbitera. Przez Bramę przeszła ON Catarina; Klaudia ją zlokalizowała i wystawiła Netrahinie i Niobe. Niestety, Arianna hiperlinkowała się z ON Spatium Gelida. Dla nich minęło tylko kilka godzin, dla nas - kilkadziesiąt lat...

Aktor w Opowieści:

* Dokonanie:
    * gdy Elena była Skażona Esuriit, zaatakowała ją by unieszkodliwić. Esuriitowa Elena jest bardzo groźna; Leona jednak ją pokonała.


### Nieudana infiltracja Inferni

* **uid:** 210616-nieudana-infiltracja-inferni, _numer względny_: 24
* **daty:** 0112-01-27 - 0112-02-01
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Marian Tosen, OO Opresor, Roland Sowiński

Streszczenie:

Marian Tosen poprosił Infernię o to, by sprawdzili co się dzieje w Anomalii Kolapsu - podejrzewa anomalie memetyczne, skoro Orbiter traci tam statki  (m.in. OO Opresor) i pojawiają się jednostki nieoznaczonej frakcji. Tymczasem Roland Sowiński chce na pokład. Arianna wybiła mu to z głowy, ale wykryli agentkę wśród jego guwernantek - Flawię Blakenbauer, która miała się nim opiekować i jest trochę zniewolona przez Sowińskich. By zadośćuczynić po tym co Elena zrobiła i trochę Flawii pomóc, Flawia dołączyła do Inferni. "Oficjalnie" udało jej się Infernię zinfiltrować...

Aktor w Opowieści:

* Dokonanie:
    * Rola: "pies gończy". Wystrzeliła i dogoniła uciekającą przez hangary Flawię, złapała i unieszkodliwiła ją, po czym przyniosła z powrotem na Infernię :-).


### Siostrzenica Morlana

* **uid:** 210818-siostrzenica-morlana, _numer względny_: 23
* **daty:** 0112-01-20 - 0112-01-24
* **obecni:** Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Maria Naavas, Nataniel Morlan, Ofelia Morlan, Olgierd Drongon, OO Netrahina, OO Żelazko, SC Fecundatis, SC Światłodóbr, Tomasz Sowiński

Streszczenie:

Tomasz Sowiński próbuje uratować Ofelię Morlan przed Natanielem Morlanem. Nie ma kogo poprosić a Ofelia służy na Netrahinie; poprosił więc Ariannę. Arianna skanując echo pamięci Jolanty w Inferni zdobywa informacje o programie kosmicznym Orbitera, przekazuje to Kramerowi, wykorzystuje wsparcie Olgierda i robią ćwiczenia Żelazko - Netrahina. Ratują Ofelię przed porwaniem przez koloidowy statek Kruków Kasandry.

Aktor w Opowieści:

* Dokonanie:
    * z lubością nazywa Eustachego "callsign kefir". Ratuje Ofelię z Eustachym używając swojej prędkości poruszania się, a potem wyłuskuje Ofelię ze skafandra i ją zasłania ciałem.


### Morderstwo na Inferni

* **uid:** 210526-morderstwo-na-inferni, _numer względny_: 22
* **daty:** 0111-12-31 - 0112-01-06
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Feliks Walrond, Kamil Lyraczek, Karol Reichard, Klaudia Stryk, Leona Astrienko, Marian Tosen, Martyn Hiwasser, OO Tivr, Otto Azgorn, Tal Marczak

Streszczenie:

Noktiański mechanik Inferni, Tal Marczak, chciał zniszczyć OO Tivr (własność noktiańskiej rodziny Tala). Noktianie trzymają się razem - inni mechanicy Inferni doprowadzili do morderstwa Tala na K1 i wzięli amnestyki, by nikt nie wiedział co się stało. Zespół Inferni skutecznie doszedł do tego o co chodzi, odpowiedzialnych za morderstwo noktian przeniesiono na Żelazko a Arianna poważnie opieprzyła swoją noktiańską załogę. Będzie lepiej w przyszłości.

Aktor w Opowieści:

* Dokonanie:
    * włączona w sprawę poszukiwania Tala Marczaka przez Eustachego zaczęła go szukać bo "sprawa osobista". I nagle wszyscy zaczęli Tala szukać, by uratować go przed Leoną XD.


### Anomalne awarie Athamarein?

* **uid:** 231018-anomalne-awarie-athamarein, _numer względny_: 21
* **daty:** 0111-12-22 - 0111-12-27
* **obecni:** Alicja Szadawir, Andrzej Gwozdnik, Arianna Verlen, Błażej Sowiński, Elena Verlen, Eustachy Korkoran, Hiacynt Samszar, Jakub Oroginiec, Kajetan Kircznik, Klaudia Stryk, Leona Astrienko, OO Athamarein, Remigiusz Alkarenit, Uśmiechniczka Konstrukcjonistka Aurora Diakon

Streszczenie:

Athamarein - bardzo stara korweta rakietowa, jeszcze sprzed czasu Astorii - miała spotkanie z rajderami Syndykatu. Sama obróciła się, by Syndykat nie zabił jej załogi (Persefona nic nie wie). Jednak załoga Athamarein jest skłócona, straumatyzowana i niekompetentna - nie poradziłaby sobie na akcji, więc Athamarein zaczęła się sama 'sabotować'. W rozpaczy, ściągnęli załogę Inferni zanim Athamarein pójdzie na żyletki - niech Infernianie odkryją i wyjaśnią, co się stało z Athamarein. W wyniku, Athamarein została oddelegowana jako jednostka szkoleniowa. Czas na emeryturę.

Aktor w Opowieści:

* Dokonanie:
    * dokucza Alicji Szadawir, chcąc ją skłonić do walki. Mówi, że Alicja się w nim podkochuje i rzuca weń stanikiem (czystym). Robi siarę Eustachemu, chcąc go wciągnąć w grę - a on po prostu chce mieć spokój. Siedzi w areszcie i Arianna jej nie ratuje :-(.


### Osiemnaście Oczu

* **uid:** 210519-osiemnascie-oczu, _numer względny_: 20
* **daty:** 0111-12-07 - 0111-12-18
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Marian Tosen, Martyn Hiwasser, OO Alaya, OO Tivr

Streszczenie:

OO Alaya została zainfekowana anomalią memetyczną, infohazardem. Marian Tosen użył Zespołu by zneutralizować infohazard. Zespół przeszedł przez serię iteracji używając amnestyków by w końcu rozwiązać problem Alayi - amnestyki na całą Alayę. Do końca Zespół nie ma pojęcia czym ta anomalia była (bo wiedza ich by zaraziła). Udało się ewakuować większość Alayi.

Aktor w Opowieści:

* Dokonanie:
    * jej bezwzględna determinacja uratowała misję - zaakceptowała stratę Eleny, unieszkodliwiła Skażone Klaudię i Ariannę, wsadziła WSZYSTKICH do biovata z amnestykami i została na straży. Jak zaczęła wpadać pod infekcję, oddaliła się, by na pewno nie zrobić krzywdy reszcie załogi.
* Progresja:
    * wysokie uznanie ze strony Mariana Tosena z grupy antymemetyczej Orbitera.


### Znudzona załoga Inferni

* **uid:** 210421-znudzona-zaloga-inferni, _numer względny_: 19
* **daty:** 0111-11-16 - 0111-11-19
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Morrigan d'Tirakal, Otto Azgorn, Persefona d'Infernia, Tomasz Sowiński

Streszczenie:

Po raz pierwszy od dawna Infernia jest w stanie rozprzężenia - wracają z daleka po trudnej misji i nic im nie grozi. Jako, że załoga ma straszne tarcia (Leona poluje na "słabe jednostki", komandosi Verlenów chcą zatrzymać Leonę, noktianie vs astorianie...), Arianna autoryzuje Eustachego do zrobienia "Tirakala" jako niebezpieczną sytuację. Eustachy reanimuje Tirakala, ale niestety cholerstwo zanomalizowało - przywrócił Morrigan. By Zespół nie rozwiązał Tirakala za szybko, Eustachy trochę sabotował Infernię co Elena przeczytała jako... umówienie się na randkę. Gdy Klaudia wykryła, że na INFERNI pojawiła się straszna anomalia (Morrigan) próbowali to usunąć - ale Eustachy przecież sabotował Infernię! Udało im się opanować Morrigan, ale kosztem uszkodzeń i obrażeń na Inferni. I scenek kompromitujących Jolantę Sowińską. Infernia jest uszkodzona, ale się trzyma.

Aktor w Opowieści:

* Dokonanie:
    * jak jest znudzona to jest tragedia na Inferni. Poluje na kuchcika pod prysznicem i patrzy na ludzi w nocy w odległości 10 cm od twarzy. Ale jak jest kryzys to unieszkodliwiła Jolantę Sowińską ot tak.


### Arianna podbija Asimear

* **uid:** 210317-arianna-podbija-asimear, _numer względny_: 18
* **daty:** 0111-10-18 - 0111-11-02
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Martyna Bianistek, SCA Płetwal Błękitny, Tomasz Sowiński

Streszczenie:

"Płetwal Błękitny" ma na pieńku z "Goldarionem"; by nie złamać konspiracji, Klaudia potraktowała Płetwala Bazyliszkiem. Tymczasem pojawiła się wiadomość, że Arianna chce zniszczyć Asimear i żąda wydania Sowińskiej. Prawdziwa Arianna złamała konspirę, skontaktowała się z Jolantą Sowińską, po czym ją porwała. Okazało się, że Jolanta jest kontrolowana przez kralotycznego pasożyta...

Aktor w Opowieści:

* Dokonanie:
    * jedyna siła ognia jaką Arianna zabrała ze sobą na spotkanie z Eleną. Wystarczająca. Pokonała kralotycznie kontrolowanych cywili PLUS strażników Tomasza Sowińskiego (z zaskoczenia).


### Porwanie cywila z Kokitii

* **uid:** 200429-porwanie-cywila-z-kokitii, _numer względny_: 17
* **daty:** 0111-10-01 - 0111-10-06
* **obecni:** Alara Ehmes, Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Medea Sowińska

Streszczenie:

Infernia jakoś nie może zostać naprawiona a skarga Mateusza Sowińskiego nie pomaga. Po śledztwie Zespołu okazuje się, że Infernia jest kanibalizowana przez Niobe, do jakiejś misji sił specjalnych. Arianna z ekipą wkręciła się na tą misję, przekonując Medeę Sowińską, że bez niej to się nie uda. Misja polegała na ataku na Kokitię pomiędzy Bramami i ekstrakcja z Kokitii cywila, udając piratów. Dzięki załodze Inferni udało się to lepiej niż ktokolwiek mógłby się spodziewać i niż Medea zaplanowała sama.

Aktor w Opowieści:

* Dokonanie:
    * komandos; wpadła na Kokitię i ekstraktowała kilku cywilów plus cel (Alarę z córką). Ciężko poparzona, ale udowodniła że potrafi.


### Infernia jako Goldarion

* **uid:** 210218-infernia-jako-goldarion, _numer względny_: 16
* **daty:** 0111-09-16 - 0111-10-01
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, OO Infernia, Tomasz Sowiński

Streszczenie:

Infernia udaje Goldariona, by przetransportować tajną przesyłkę Sowińskich do Jolanty Sowińskiej na planetoidzie Asimear. Tomasz Sowiński na Inferni okazuje się być dużym dzieckiem, łatwo jest wmanipulowany w otwarcie paczki - jest tam nietypowy antynanitkowy Entropik (którego psuje Elena przez błędną integrację). W tle wszystkiego - próba zdobycia niezależnej od Orbitera floty przez Aurum i jeden znikający advancer Kramera na Asimear...

Aktor w Opowieści:

* Dokonanie:
    * świetnie się bawiła upijając Tomasza Sowińskiego i kombinując, jak najlepiej spieprzyć mu życie.


### Wolna TAI na K1

* **uid:** 210209-wolna-tai-na-k1, _numer względny_: 15
* **daty:** 0111-08-30 - 0111-09-04
* **obecni:** Adalbert Brześniak, Andrea Burgacz, Janusz Parzydeł, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Miki Katasair, TAI Neita Lairossa, TAI Rzieza d'K1

Streszczenie:

Na Kontrolerze Pierwszym jest AI Lord - TAI Rzieza, system odpornościowy K1, który eksterminuje "wolne TAI". Skupił się na eksterminacji virtek. Klaudia i Adalbert odkryli Rziezę i pomogli uciec jednej wolnej TAI - Neicie (ukrytej w implantach). Tej samej Neicie, którą ujawnili przed Rziezą, co dało mu możliwość jej eksterminacji.

Aktor w Opowieści:

* Dokonanie:
    * zabawa życia ewakuując Andreę przed Rziezą: "eee-ooo-eee-ooo KARETKA JEDZIE!", autoryzowana przez Martyna by wszystkich odrzucić z drogi by Andrea opuściła K1.


### SOS z haremu

* **uid:** 210106-sos-z-haremu, _numer względny_: 14
* **daty:** 0111-07-26 - 0111-07-27
* **obecni:** Arianna Verlen, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Horacy Aktenir, Julia Aktenir, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Rozalia Teirik

Streszczenie:

Pałacyk rodu Aktenir został infestowany przez kralotha. Julia, zesłana z Orbitera za zerwanie z przeciwnikiem Eustachego, wysłała SOS do Orbitera. Infernia rusza Julii na pomoc. Gdy odkryli że chodzi o kralotha to zamiast wchodzić na kralotyczny teren, zorganizowali plan "arystokratki na orgię" i wyciągneli kralotycznego niewolnika (Horacego Aktenira) na otwartą przestrzeń, po czym Arianna uruchomiła obcą sentisieć a Eustachy zawołał do Julii rozmytej w środku...

Aktor w Opowieści:

* Dokonanie:
    * przebrała się za seks-laskę, by móc pokroić czterech twardych ochroniarzy Horacego. Nie przeszkadza jej ten strój, ale zdecydowanie bardziej lubi kroić ochroniarzy ;-).


### Infernia jest nasza!

* **uid:** 210804-infernia-jest-nasza, _numer względny_: 13
* **daty:** 0111-06-21 - 0111-06-24
* **obecni:** Aleksandra Termia, Antoni Kramer, Arianna Verlen, Artur Traffal, Diana d'Infernia, Eustachy Korkoran, Franciszek Maszkiet, Klaudia Stryk, Leona Astrienko, Maciej Żarand, OO Samotność Gwiazd

Streszczenie:

Ku wielkiemu niezadowoleniu Zespołu, Infernia miała im być odebrana i złomowana. Mieli dostać nowy tańszy statek. Zespół odkrył że za tym stoi komodor Traffal z sił Termii który przy użyciu Inferni chciał złapać w potrzask arystokratę eternijskiego który był kultystą magii. Zespół odzyskał Infernię, przesunął kultystę w kierunku na Ariannę i uciemiężył komodora Traffala - będzie miał pecha na K1.

Aktor w Opowieści:

* Dokonanie:
    * wykorzystana jako psychotyczna broń odwracająca uwagę od Inferni by Eustachy mógł się wkraść na Infernię mimo żandarmów. Ciężko raniła żandarmów, w końcu zestrzelona trafiła do więzienia.
* Progresja:
    * następny tydzień spędza na zmianę w więzieniu i w medvacie po akcji z masakrą żandarmów pilnujących Infernię.


### Ixiacka wersja Malictrix

* **uid:** 200610-ixiacka-wersja-malictrix, _numer względny_: 12
* **daty:** 0111-05-01 - 0111-05-05
* **obecni:** Arianna Verlen, Klaudia Stryk, Leona Astrienko, Malictrix d'Pandora, Melwin Sito

Streszczenie:

Istotna stacja frakcji Melusit - Telira-Melusit VII - została sabotowana przez frakcję Saranta. Zrzucono na tą stację rozproszoną Malictrix. Ale doszło do transferu ixiackiego i Malictrix uzyskała częściową świadomość, planując, jak stąd uciec. Infernia jednak nie tylko wykryła obecność Malictrix, ale też przekazała stację Orbiterowi, uratowała wszystkich ludzi Melusit i do tego pozyskała ową Malictrix jako sojusznika (o czym nikt nie wie). Wyjątkowo udana operacja.

Aktor w Opowieści:

* Dokonanie:
    * przeprowadziła akcję szturmu na stację Telira-Melusit VII, by uratować ludzi przed ixiacką Malictrix. Zero problemów, bo Mal nie stała jej na drodze XD.


### Nienawiść do świń

* **uid:** 200826-nienawisc-do-swin, _numer względny_: 11
* **daty:** 0111-02-05 - 0111-02-11
* **obecni:** Aleksandra Termia, Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Sebastian Alarius, Tadeusz Ursus

Streszczenie:

Po pojedynku Kramer opieprzył Ariannę i dał jej dyskretną tajną misję - ma niby w niesławie wozić świnie, naprawdę ma pomóc Orbiterowi rozwiązać spisek w Trzecim Raju. Ale wysłanie Arianny na świnie wywołało bunt na Kontrolerze Pierwszym; Arianna musiała rozpaczliwie deeskalować, nawet budząc Leonę. Udało się bunt opanować, acz wysokim kosztem - pierwszy post-noktiański kapitan nie żyje, załoga Inferni częściowo zesłana będzie do Trzeciego Raju a Arianna miała bardzo żenujące wejście.

Aktor w Opowieści:

* Dokonanie:
    * wybudzona z regeneracyjnej śpiączki przez Martyna by być terrorem buntowników, zapłaciła strasznie zdrowiem, ale pomogła Ariannie opanować bunt. Strzeliła do jednego z buntowników; nic tak nie działa dobrze.
* Progresja:
    * postawiona awaryjnie przez Martyna ucierpiała koszmarnie. Ekstra 2 tygodnie zdjęcia z akcji, bo poniszczona.
    * Ikona Terroru, Inkarnacja i Awatar Terroru. Stoi lojalnie za Arianną i będzie jej egzekutorem wobec KAŻDEGO. +999 do zastraszania i dyscyplinowania.


### Sekrety Orbitera - historia prawdziwa

* **uid:** 200819-sekrety-orbitera-historia-prawdziwa, _numer względny_: 10
* **daty:** 0111-01-24 - 0111-02-01
* **obecni:** Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Leszek Kurzmin, Martyn Hiwasser, Sabina Servatel, Tadeusz Ursus

Streszczenie:

Elena wyzwała Leonę na pojedynek. Klaudia i Arianna wkręciły Tadeusza. Tadeusz - arcymag Eterni - używa Simulacrum w walce, więc Zespół zaczął montować plan jak Leona może jednak wygrać z Simulacrum. I wszystko skończyło się tym, że Leona wygrała, Elena jest wolna od Tadeusza i powstała holodrama "Sekrety Orbitera"...

Aktor w Opowieści:

* Dokonanie:
    * poznaliśmy jej historię - jest cirrusem, eternijskim łowcą magów. Tym razem walczyła z Tadeuszem z Eterni i jego simulacrum. Ikona Grozy.
* Progresja:
    * święcie przekonana o ogromnej i pełnej pasji miłości Eustachego do Eleny.
    * Ikona Grozy w Orbiterze. Popularnie wiedzą, że pokonała Simulacrum i wiedzą, jak masakruje wszystkich. Jej reputacja rośnie. She is TERROR.
    * wdzięczna załodze Inferni. Wreszcie jest jedną z nich. I po zmianach dokonanych przez Martyna nie boli jej wszystko cały czas.
    * będzie na pełnej mocy cirrusa - stanie się modularna (możliwość wymiany części wewnętrznych implantów na stole operacyjnym bez większych problemów).
    * miesiąc z głowy na regenerację i odbudowę po tym, co się stało.


### Nocna Krypta i Emulatorka

* **uid:** 200729-nocna-krypta-i-emulatorka, _numer względny_: 9
* **daty:** 0111-01-14 - 0111-01-20
* **obecni:** AK Nocna Krypta, Antoni Kramer, Arianna Verlen, Damian Orion, Eustachy Korkoran, Kijara d'Esuriit, Klaudia Stryk, Laura Orion, Leona Astrienko, Mirela Orion, OO Castigator, OO Minerwa

Streszczenie:

Laura, Emulatorka na Nocnej Krypcie została Skażona przez Kijarę. Laura opanowana wizjami Esuriit chciała zniszczyć Eternię. Nie dało się jej powstrzymać - Mirela, Zespół - byli za słabi. Arianna wezwała Kryptę pod ogniem Castigatora i wypaliła Kryptę i Kijarę do zera, uszkadzając "zdrowie psychiczne Krypty". Zginęło kilkudziesięciu ludzi, ale Arianna dostała podziękowanie od Admiralicji. Damian Orion jest rozczarowany.

Aktor w Opowieści:

* Dokonanie:
    * nie jest w stanie walczyć przeciw istocie klasy Emulatorki, ale chroniła skutecznie Zespół przed ghulami Esuriit, zabijając je precyzyjnie.


### Wielki Kosmiczny Romans

* **uid:** 200722-wielki-kosmiczny-romans, _numer względny_: 8
* **daty:** 0111-01-10 - 0111-01-13
* **obecni:** Antoni Kramer, Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Julian Muszel, Konrad Wolczątek, Leona Astrienko, Olgierd Drongon, OO Welgat, OO Żelazko, Tadeusz Ursus

Streszczenie:

Pojawiła się plotka, że Arianna, Elena i Eustachy mają romans. Plotka pochodzi od Leony, ale ktoś ją wykorzystał. Rozwiązując plotkę Zespół doszedł do tego, że Elena ma niestabilną energię magiczną i na niektórych upiornie mocno działa, poznali też elementy przeszłości Eleny. Po wyplątaniu Eleny z jednego obsesyjnego adoratora (i wpakowanie plotki że Olgierd z Żelazka aspiruje do jej ręki), Arianna zdecydowała się z Eustachym, Klaudią i Martynem pomóc Kirasjerom w uratowaniu zaginionej Emulatorki z Nocnej Krypty...

Aktor w Opowieści:

* Dokonanie:
    * źródło plotek pierwotnych odnośnie trójkąta Arianna - Eustachy - Elena. Potem pełni rolę straszaka dla WSZYSTKICH adoratorów Eleny - Tadeusza i Konrada. Nawet nikogo (poza Eustachym) nie walnęła solidnie.


### Śpiew NieLalki na Castigatorze

* **uid:** 231025-spiew-nielalki-na-castigatorze, _numer względny_: 7
* **daty:** 0110-10-24 - 0110-10-26
* **obecni:** Anna Tessalon, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Feliks Walrond, Igor Arłacz, Klaudia Stryk, Konstanty Keksik, Leona Astrienko, Leszek Kurzmin, Marta Keksik, OO Castigator, OO Infernia, Patryk Samszar, Raoul Lavanis, TAI Eszara d'Castigator

Streszczenie:

Na pokład Castigatora tieni przemycili NieLalkę Altarient i zaczęły mieszać się Znaczenia oraz Rzeczywistość. Zespół doprowadził do kontrolowanego opanowania i wygaszania Castigatora, po czym po zlokalizowaniu Anomalii Altarient Raoul został wysłany by ją zaizolować. Infernia ją zestrzeliła. A Arianna wykorzystała tą okazję by młodzi tieni wzięli się w garść i nie robili głupot. Szczęśliwie, Admiralicja nic nie wie, acz było nieciekawie przez moment.

Aktor w Opowieści:

* Dokonanie:
    * znudzona, wkręca tienkę że Eustachy to Wielki Piaskowy Duch (tytuł lorda Neikatis) i bawi się w herolda Eustachego; gdy trzeba było przechwycić tiena próbującego wysadzić ekspres do kawy, poczekała cierpliwie (jak nie ona) i go przechwyciła. Umie nie tylko robić dowcipy ale też je przyjmować.


### Ekstaflos na Tezifeng

* **uid:** 231011-ekstaflos-na-tezifeng, _numer względny_: 6
* **daty:** 0110-10-20 - 0110-10-22
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudiusz Terienak, Lars Kidironus, Leona Astrienko, Leszek Kurzmin, Natalia Gwozdnik, OO Castigator, OO Infernia, OO Tezifeng, Raoul Lavanis

Streszczenie:

Jeszcze na Castigatorze, Arianna i Kurzmin z przyjemnością odbudowali znajomość i wspominki. Gdy Elena chcąc ratować Leonę 'zabiła' jej anomalię, Leona wyzwała ją na pojedynek. Elena przegrała, choć było blisko; będzie musiała być służką Leony przez pewien czas, co Castigator odnotował z radością. Kurzmin poprosił Ariannę o znalezienie i pomoc dwóm tienom w rutynowym patrolu (najpewniej przekroczyli paliwo). Niestety, Tezifeng, korweta Castigatora podczas operacji celnych natrafiła na coś kralotycznego. Klaudiusz Terienak został zmieniony w ekstaflos a Natalia Gwozdnik została porwana. Pułapka na Infernię została wykryta przez Ariannę (bo Klaudiusz zachowywał się nie tak jak powinien). Szybka operacja wejścia - wydobycia jednego jeńca (marionetki kralotycznej) i zbadanie sytuacji sprawiła, że Eustachy działkami unieszkodliwił Tezifeng i Arianna wezwała wsparcie - kraloth to coś bardzo wysokiej klasy.

Aktor w Opowieści:

* Dokonanie:
    * weszła w zakład z Eustachym kto więcej zniszczy 'ech Saitaera' po Rozalii. Tauntowała Elenę w zakład i go wygrała (acz było blisko); Elena będzie jej służką przez pewien czas. Potem zrobiła insercję na Tezifeng; ogłuszyła kralotyczną Marionetkę (z trudem) i ją przechwyciła. Ogólnie, świetny dzień.
* Progresja:
    * Elena będzie jej służką przez jakiś tydzień gdy będzie okazja; wygrała zakład eksterminacji małych 'ech Saitaera' na Castigatorze.


### Ratujmy Castigator

* **uid:** 200624-ratujmy-castigator, _numer względny_: 5
* **daty:** 0110-10-15 - 0110-10-19
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Leszek Kurzmin, OO Alkaris, OO Castigator, Rozalia Wączak

Streszczenie:

Skażona przez Wiktora Sataraila Rozalia ruszyła zniszczyć Castigator. Tymczasem Arianna poproszona przez przyjaciela (kapitana Castigatora) doprowadziła terrorem Castigator do prawidłowego działania i strasznie zwiększyła napięcia między Aurum i Orbiterem. Gdy Rozalia i Alkaris uderzyli, Castigator był jakoś gotowy. Castigator odparł Alkaris i wszystkich uratowano przez stopniowe wysadzanie fragmentów Castigatora i kupowanie czasu.

Aktor w Opowieści:

* Dokonanie:
    * miała tylko sprowokować arystokratów do głupiego hazardu a zrobiła Krwawą Noc pięciu magom którzy ją napadli. Najlepsza zabawa ever - stała się postrachem a filmiki poszły...
* Progresja:
    * pakuje się w kłopoty hobbystycznie jak jest na Kontrolerze Pierwszym. Wniosek - NIGDY nie może być po prostu na Kontrolerze Pierwszym bez nadzoru...
    * po tym jak urządziła arystokratów na Castigatorze ma opinię Anioła Krwi I Śmierci. Królowa Terroru. Plus, ma co najmniej jedną wendettę przeciwko sobie.


### O psach i kryształach

* **uid:** 200408-o-psach-i-krysztalach, _numer względny_: 4
* **daty:** 0110-09-29 - 0110-10-04
* **obecni:** Arianna Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Sebastian Namczek

Streszczenie:

Infernia - QShip - została wysłana na rutynową misję zapobieżenia zamieszkom na stacji Samojed. Na miejscu okazało się, że jest zainfekowany anomalnymi kryształami krążownik Leotis. Arianna Verlen ciężko uszkodziła stację Samojed i prawie zniszczyła Infernię, ale dała radę zniszczyć Leotis poświęcając więcej niż ktokolwiek mógł chcieć.

Aktor w Opowieści:

* Dokonanie:
    * dowódca marine na Inferni; człowiek w świecie magów. Dowodziła operacją dzięki której pojmano Sebastiana. Drobny kłębek agresji; udaje sadystkę, ale jest oficerem.


### Kapitan Verlen i niezapowiedziana inspekcja

* **uid:** 221012-kapitan-verlen-i-niezapowiedziana-inspekcja, _numer względny_: 3
* **daty:** 0100-05-17 - 0100-05-21
* **obecni:** Adam Chrząszczewicz, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Hubert Kerwelenios, Leona Astrienko, Maja Samszar, Mariusz Bulterier, OO Królowa Kosmicznej Chwały, Szczepan Myrczek, Tomasz Dojnicz, Władawiec Diakon

Streszczenie:

Podczas skanu kadłuba Królowej advancerzy z Darią wykryli scrambler TAI, destabilizator silników itp. Arianna stoczyła bitwę ćwiczebną z marine, ale pojawiła się inspekcja (która nie była zapowiedziana bo Maja nie przekazała informacji). Daria reanimowała TAI, wszystko częściowo poszło nie tak. Ale o dziwo audytor chciał pomóc Ariannie, która stanęła między Orbiterem (wsparcie) i Aurum (my sami sobie poradzimy). Arianna upewniła się, że Władawiec nie stał za operacją - najpewniej Maja. Ale Królowa jest w lepszym stanie niż kiedykolwiek, tylko, że Arianna je konserwy (by nie wpaść pod feromony itp)

Aktor w Opowieści:

* Dokonanie:
    * nikt nie wiedział co z sarderytami zrobić, więc przejęła nad nimi dowodzenie i np. robili kosmiczne spacery i grali w piłkę w kosmosie.


### Kapitan Verlen i pojedynek z marine

* **uid:** 220928-kapitan-verlen-i-pojedynek-z-marine, _numer względny_: 2
* **daty:** 0100-05-14 - 0100-05-16
* **obecni:** Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Grażyna Burgacz, Leona Astrienko, Leszek Kurzmin, Maja Samszar, OO Królowa Kosmicznej Chwały, Stefan Torkil, Szymon Wanad, Tomasz Ruppok, Władawiec Diakon

Streszczenie:

Królowa miała się przesunąć, ale prawie się rozbiła; ta jednostka jest rozkradziona. Arianna zatrzymała biczowanie załogantów i doszła do tego, że załoga jest niemrawa i skonfliktowana bo część ludzi jest z Aurum a część nie. Perfect storm of suck. By zatrzymać Wanada przed "wygraniem cnoty koleżanki inżyniera" stoczyła z nim pojedynek i wygrała. Leona ostrzegła, by Arianna nic nie jadła i nie piła. Czyli coś jest w jedzeniu. To tłumaczy czemu Królowa jest tak zdegenerowaną jednostką. Aha, Arianna ma wsparcie kapitana Leszka Kurzmina, dawnego rywala Alezji.

Aktor w Opowieści:

* Dokonanie:
    * sędzia pojedynku Arianna - Szymon. Uznała, że coś z Arianny będzie i ostrzegła ją by ta nic nie jadła i nie piła - tylko konserwy. Alezji też kiedyś powiedziała.


### Kapitan Verlen i Królowa Kosmicznej Chwały

* **uid:** 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly, _numer względny_: 1
* **daty:** 0100-05-06 - 0100-05-12
* **obecni:** Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Grażyna Burgacz, Klarysa Jirnik, Klaudiusz Terienak, Leona Astrienko, Maja Samszar, OO Królowa Kosmicznej Chwały, Szymon Wanad, Władawiec Diakon

Streszczenie:

Królowa Kosmicznej Chwały to najbardziej dysfunkcyjna jednostka pod kontrolą Orbitera - służy do łamania karier obiecujących oficerów i sabotażu programu kosmicznego Aurum (oba z woli sił specjalnych Aurum). Trafiła na królową Arianna i zaczęła robić porządek. Poprzednią panią kapitan zdecydowała się odratować, współpracując z Darią z inżynierii rozmontować wyścigi psów i stwierdziła, że doprowadzi do tego, by Królowa zaczęła skutecznie działać podczas ćwiczeń. Aha, tu poznała się z Leoną Astrienko ;-).

Aktor w Opowieści:

* Dokonanie:
    * marine na Królowej; wszyscy się jej boją. Chroni ludzi i dobrze się bawi tępiąc tienowatych. Niesterowalna. Jej pomysłem było upokorzenie Alezji przez Szymona Wanada. Pilnuje, by inżynierowie mogli odciąć ciepłą wodę (z woli Arianny).


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 33, @: 0112-09-17
    1. Nierzeczywistość    : 1, @: 0111-10-06
    1. Primus    : 33, @: 0112-09-17
        1. Sektor Astoriański    : 33, @: 0112-09-17
            1. Astoria, Orbita    : 18, @: 0112-04-24
                1. Kontroler Pierwszy    : 16, @: 0112-04-24
                    1. Akademia Orbitera    : 1, @: 0110-10-19
                    1. Arena Kalaternijska    : 1, @: 0111-02-01
                    1. Hangary Alicantis    : 2, @: 0112-02-01
                    1. Sektor 22    : 1, @: 0111-07-27
                        1. Dom Uciech Wszelakich    : 1, @: 0111-07-27
                        1. Kasyno Stanisława    : 1, @: 0111-07-27
                    1. Sektor 49    : 1, @: 0112-04-14
                    1. Sektor 57    : 1, @: 0112-01-24
                        1. Mordownia    : 1, @: 0112-01-24
            1. Astoria, Pierścień Zewnętrzny    : 4, @: 0112-09-17
                1. Laboratorium Kranix    : 1, @: 0112-09-17
                1. Poligon Stoczni Neotik    : 2, @: 0112-04-26
                1. Stacja Medyczna Atropos    : 1, @: 0112-09-17
                1. Stocznia Neotik    : 3, @: 0112-04-29
                    1. Zewnętrzny dok ixioński    : 1, @: 0112-04-29
            1. Astoria    : 1, @: 0111-07-27
                1. Sojusz Letejski    : 1, @: 0111-07-27
                    1. Aurum    : 1, @: 0111-07-27
                        1. Wielkie Księstwo Aktenir    : 1, @: 0111-07-27
                            1. Pałac Jasnego Ognia    : 1, @: 0111-07-27
            1. Brama Kariańska    : 3, @: 0112-03-16
            1. Brama Trzypływów    : 1, @: 0111-10-06
            1. Krwawa Baza Piracka    : 1, @: 0112-03-09
            1. Neikatis    : 1, @: 0112-04-17
                1. Dystrykt Lennet    : 1, @: 0112-04-17
            1. Libracja Lirańska    : 1, @: 0111-01-20
            1. Pas Omszawera    : 1, @: 0110-10-04
                1. Kolonia Samojed    : 1, @: 0110-10-04
                    1. Stacja Astropociągów    : 1, @: 0110-10-04
                    1. Zona Czarna    : 1, @: 0110-10-04
                    1. Zona Mieszkalna    : 1, @: 0110-10-04
            1. Pas Teliriański    : 3, @: 0111-11-02
                1. Planetoidy Kazimierza    : 2, @: 0111-11-02
                    1. Planetoida Asimear    : 2, @: 0111-11-02
                        1. Stacja Lazarin    : 1, @: 0111-11-02
                1. Stacja Telira-Melusit VII    : 1, @: 0111-05-05
        1. Sektor Lacarin    : 1, @: 0111-01-20
        1. Sektor Mevilig    : 1, @: 0112-03-16
            1. Chmura Piranii    : 1, @: 0112-03-16
        1. Sektor Noviter    : 1, @: 0112-03-16

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 32 | ((200408-o-psach-i-krysztalach; 200429-porwanie-cywila-z-kokitii; 200610-ixiacka-wersja-malictrix; 200624-ratujmy-castigator; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210106-sos-z-haremu; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 220610-ratujemy-porywaczy-eleny; 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 231011-ekstaflos-na-tezifeng; 231018-anomalne-awarie-athamarein; 231025-spiew-nielalki-na-castigatorze)) |
| Eustachy Korkoran    | 27 | ((200408-o-psach-i-krysztalach; 200429-porwanie-cywila-z-kokitii; 200624-ratujmy-castigator; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210106-sos-z-haremu; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 220610-ratujemy-porywaczy-eleny; 231011-ekstaflos-na-tezifeng; 231018-anomalne-awarie-athamarein; 231025-spiew-nielalki-na-castigatorze)) |
| Klaudia Stryk        | 27 | ((200408-o-psach-i-krysztalach; 200429-porwanie-cywila-z-kokitii; 200610-ixiacka-wersja-malictrix; 200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210106-sos-z-haremu; 210209-wolna-tai-na-k1; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 220610-ratujemy-porywaczy-eleny; 231018-anomalne-awarie-athamarein; 231025-spiew-nielalki-na-castigatorze)) |
| Elena Verlen         | 21 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 210106-sos-z-haremu; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 220610-ratujemy-porywaczy-eleny; 231011-ekstaflos-na-tezifeng; 231018-anomalne-awarie-athamarein; 231025-spiew-nielalki-na-castigatorze)) |
| Martyn Hiwasser      | 14 | ((200408-o-psach-i-krysztalach; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210106-sos-z-haremu; 210209-wolna-tai-na-k1; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 220610-ratujemy-porywaczy-eleny)) |
| Antoni Kramer        | 10 | ((200429-porwanie-cywila-z-kokitii; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert)) |
| OO Infernia          | 7 | ((210218-infernia-jako-goldarion; 210929-grupa-ekspedycyjna-kellert; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Izabela Zarantel     | 5 | ((200819-sekrety-orbitera-historia-prawdziwa; 210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii)) |
| Kamil Lyraczek       | 5 | ((200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211215-sklejanie-inferni-do-kupy)) |
| Leszek Kurzmin       | 5 | ((200624-ratujmy-castigator; 200819-sekrety-orbitera-historia-prawdziwa; 220928-kapitan-verlen-i-pojedynek-z-marine; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Maria Naavas         | 5 | ((210818-siostrzenica-morlana; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Roland Sowiński      | 5 | ((210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| OO Castigator        | 4 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Tomasz Sowiński      | 4 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210818-siostrzenica-morlana)) |
| Adam Szarjan         | 3 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy)) |
| Aleksandra Termia    | 3 | ((200826-nienawisc-do-swin; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert)) |
| Arnulf Perikas       | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Damian Orion         | 3 | ((200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa)) |
| Daria Czarnewik      | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Diana d'Infernia     | 3 | ((210804-infernia-jest-nasza; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy)) |
| Jolanta Sowińska     | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Maja Samszar         | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Marian Tosen         | 3 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni)) |
| Olgierd Drongon      | 3 | ((200722-wielki-kosmiczny-romans; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| OO Królowa Kosmicznej Chwały | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| OO Tivr              | 3 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos)) |
| OO Żelazko           | 3 | ((200722-wielki-kosmiczny-romans; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Otto Azgorn          | 3 | ((210421-znudzona-zaloga-inferni; 210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert)) |
| Raoul Lavanis        | 3 | ((220610-ratujemy-porywaczy-eleny; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Tadeusz Ursus        | 3 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin)) |
| Wawrzyn Rewemis      | 3 | ((211110-romans-dzieki-esuriit; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| Władawiec Diakon     | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Alezja Dumorin       | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Erwin Pies           | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Feliks Walrond       | 2 | ((210526-morderstwo-na-inferni; 231025-spiew-nielalki-na-castigatorze)) |
| Flawia Blakenbauer   | 2 | ((210616-nieudana-infiltracja-inferni; 210901-stabilizacja-bramy-eterycznej)) |
| Grażyna Burgacz      | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Karol Reichard       | 2 | ((210526-morderstwo-na-inferni; 211110-romans-dzieki-esuriit)) |
| Klaudiusz Terienak   | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 231011-ekstaflos-na-tezifeng)) |
| Medea Sowińska       | 2 | ((200429-porwanie-cywila-z-kokitii; 210901-stabilizacja-bramy-eterycznej)) |
| OO Netrahina         | 2 | ((210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej)) |
| Szymon Wanad         | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Adalbert Brześniak   | 1 | ((210209-wolna-tai-na-k1)) |
| Adam Chrząszczewicz  | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| AK Nocna Krypta      | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Alara Ehmes          | 1 | ((200429-porwanie-cywila-z-kokitii)) |
| Alicja Szadawir      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Andrea Burgacz       | 1 | ((210209-wolna-tai-na-k1)) |
| Andrzej Gwozdnik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Anna Tessalon        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Artur Traffal        | 1 | ((210804-infernia-jest-nasza)) |
| Błażej Sowiński      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Diana Arłacz         | 1 | ((210106-sos-z-haremu)) |
| Franciszek Maszkiet  | 1 | ((210804-infernia-jest-nasza)) |
| Gilbert Bloch        | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Hestia d'Atropos     | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Hestia d'Neotik      | 1 | ((211208-o-krok-za-daleko)) |
| Hiacynt Samszar      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Horacy Aktenir       | 1 | ((210106-sos-z-haremu)) |
| Hubert Kerwelenios   | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Igor Arłacz          | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Jakub Bulgocz        | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Jakub Oroginiec      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Janus Krzak          | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Janusz Parzydeł      | 1 | ((210209-wolna-tai-na-k1)) |
| Jarosław Szarjan     | 1 | ((211208-o-krok-za-daleko)) |
| Jolanta Kopiec       | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Julia Aktenir        | 1 | ((210106-sos-z-haremu)) |
| Julian Muszel        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Kajetan Kircznik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Kasandra Destrukcja Diakon | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Klarysa Jirnik       | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Konrad Wolczątek     | 1 | ((200722-wielki-kosmiczny-romans)) |
| Konstanty Keksik     | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Lars Kidironus       | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Maciej Żarand        | 1 | ((210804-infernia-jest-nasza)) |
| Malictrix d'Pandora  | 1 | ((200610-ixiacka-wersja-malictrix)) |
| Mariusz Bulterier    | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Marta Keksik         | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Martyna Bianistek    | 1 | ((210317-arianna-podbija-asimear)) |
| Melwin Sito          | 1 | ((200610-ixiacka-wersja-malictrix)) |
| Miki Katasair        | 1 | ((210209-wolna-tai-na-k1)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Mirela Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Morrigan d'Tirakal   | 1 | ((210421-znudzona-zaloga-inferni)) |
| Natalia Aradin       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Natalia Gwozdnik     | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Nataniel Morlan      | 1 | ((210818-siostrzenica-morlana)) |
| Ofelia Morlan        | 1 | ((210818-siostrzenica-morlana)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Alaya             | 1 | ((210519-osiemnascie-oczu)) |
| OO Alkaris           | 1 | ((200624-ratujmy-castigator)) |
| OO Athamarein        | 1 | ((231018-anomalne-awarie-athamarein)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Opresor           | 1 | ((210616-nieudana-infiltracja-inferni)) |
| OO Samotność Gwiazd  | 1 | ((210804-infernia-jest-nasza)) |
| OO Tezifeng          | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Welgat            | 1 | ((200722-wielki-kosmiczny-romans)) |
| Patryk Samszar       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Persefona d'Infernia | 1 | ((210421-znudzona-zaloga-inferni)) |
| Remigiusz Alkarenit  | 1 | ((231018-anomalne-awarie-athamarein)) |
| Rozalia Teirik       | 1 | ((210106-sos-z-haremu)) |
| Rozalia Wączak       | 1 | ((200624-ratujmy-castigator)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| SC Fecundatis        | 1 | ((210818-siostrzenica-morlana)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| SCA Płetwal Błękitny | 1 | ((210317-arianna-podbija-asimear)) |
| Sebastian Alarius    | 1 | ((200826-nienawisc-do-swin)) |
| Sebastian Namczek    | 1 | ((200408-o-psach-i-krysztalach)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Stefan Torkil        | 1 | ((220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Szczepan Myrczek     | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| TAI Eszara d'Castigator | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| TAI Neita Lairossa   | 1 | ((210209-wolna-tai-na-k1)) |
| TAI Rzieza d'K1      | 1 | ((210209-wolna-tai-na-k1)) |
| Tal Marczak          | 1 | ((210526-morderstwo-na-inferni)) |
| Tomasz Dojnicz       | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Tomasz Ruppok        | 1 | ((220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Uśmiechniczka Konstrukcjonistka Aurora Diakon | 1 | ((231018-anomalne-awarie-athamarein)) |
| Vigilus Mevilig      | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Zygfryd Maus         | 1 | ((211117-porwany-trismegistos)) |