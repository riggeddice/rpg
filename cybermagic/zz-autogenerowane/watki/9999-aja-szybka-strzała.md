# AJA Szybka Strzała
## Identyfikator

Id: 9999-aja-szybka-strzała

## Sekcja Opowieści

### Ratuj młodzież dla Kajrata

* **uid:** 230627-ratuj-mlodziez-dla-kajrata, _numer względny_: 11
* **daty:** 0095-08-20 - 0095-08-25
* **obecni:** AJA Szybka Strzała, Amanda Kajrat, Elena Samszar, Ernest Kajrat, Herbert Samszar, Impresja Ignicja Incydencja Diakon, Karolinus Samszar, Mitria Ira, Talia Aegis, Wacław Samszar, Wargun Ira

Streszczenie:

Herbert powiedział o lokacjach baz i że to prace na duchach. Strzała wie o tym że ma uszkodzoną psychotronikę - skontaktowała Zespół z Kajratem (S. potrzebuje pomocy Talii). Kajrat da dostęp do Talii, jeśli Zespół pomoże uratować noktiańskich młodych magów z niewoli Samszarów. Po znalezieniu bazy Karolinus wywabił na Itrię (którą dołączył do drużyny XD) Wacława (strażnika i ekstraktora) a Elena z Amandą Kajrat wydobyły nieszczęsnych noktian. Strzała odjechała do Kajrata. Kajrat zostawił Zespołowi Amandę do pomocy.

Aktor w Opowieści:

* Dokonanie:
    * anty-amnestykowała Karolinusa, po czym skontaktowała zespół z Kajratem by móc naprawić swoją psychotronikę używając Talii Aegis. Zdobyła plany inkubatora gdzie jest baza hiperpsychotroników i podpowiadała Karolinusowi jak gadać z Itrią ;-).


### Piękna Diakonka i rytuał nirwany kóz

* **uid:** 230606-piekna-diakonka-i-rytual-nirwany-koz, _numer względny_: 10
* **daty:** 0095-08-15 - 0095-08-18
* **obecni:** AJA Szybka Strzała, Dźwiedź Łagodne Słowo, Elena Samszar, Herbert Samszar, Impresja Ignicja Incydencja Diakon, Karolinus Samszar, Maks Samszar

Streszczenie:

Itria Diakon ma kolejny projekt - nirwana kóz u Samszarów. Jej obecność spowodowała chaos w Forcie Tawalizer. Karolinus i Elena S. dotarli tam by dowiedzieć się o tajnych podziemnych bazach Samszarów od Herberta, ale musieli wpierw rozwiązać problem z Itrią. W końcu doprowadzili do rytuału kóz (dzięki Elenie i jej researchowi) i trochę naprawili sytuację. Czas spotkać się z Herbertem poza terenem sentisieci i poznać sekrety badań nad duchami.

Aktor w Opowieści:

* Dokonanie:
    * zbiera informacje na co ostatnio 'poleciała' Itria, stanowi bezpieczne miejsce rozmowy z Herbertem i monitoruje + patternuje zachowania Itrii by zrobić jej projekcję dla Maksa.


### Romeo, dyskretny instalator Supreme Missionforce

* **uid:** 230523-romeo-dyskretny-instalator-supreme-missionforce, _numer względny_: 9
* **daty:** 0095-08-09 - 0095-08-11
* **obecni:** AJA Szybka Strzała, Albert Samszar, Elena Samszar, Karolinus Samszar, Maja Samszar, Nataniel Samszar, Romeo Verlen

Streszczenie:

Strzała ma za zadanie wychować i nauczyć współpracy Karolinusa i Eleny. Karolinus jest wezwany do interwencji, gdy Maja przesyła sygnał SOS. Maja twierdzi, że niechcący mogła zabić kolegę. Ekipa dociera na miejsce, gdzie odkrywają ciężarówkę z zaawansowanym sprzętem. Maja jest w stanie panicznym, a jej magia wymyka się spod kontroli - próbuje przebić dziwne pole siłowe otaczające dziurę w piwnicy biurowca. Karolinus i Elena zmuszają Maję do opuszczenia miejsca, obiecując uratować Romeo. W międzyczasie, Strzała zabiera Maję do Ogrodów Medytacyjnych by nie miała problemów z ojcem. Elena zyskuje na czasie przed ojcem Mai (Albertem), dopóki Maja nie jest bezpiecznie umieszczona w Ogrodach. Potem wydobywają Romeo z podziemi zauważając, że ktoś tam ma tajną dziwną bazę do eksperymentów na duchach.

Aktor w Opowieści:

* Dokonanie:
    * identyfikuje urządzenia Romeo jako detektory i ciężarówkę jako pojazd opancerzony, rozstawia drony monitorujące teren. Potem - uspokaja Maję i leci z pełną prędkością by ją umieścić w bezpiecznym miejscu zanim jej ojciec się wścieknie.


### Zaginięcie psychotronika Cede

* **uid:** 230613-zaginiecie-psychotronika-cede, _numer względny_: 8
* **daty:** 0095-08-02 - 0095-08-05
* **obecni:** AJA Szybka Strzała, Aleksander Samszar, Cede Burian, Celina Burian, Fabian Samszar, Karolinus Samszar, Lara Ukraptin

Streszczenie:

Przyjaciel Karolinusa, Cede, zniknął. Siostra - Celina - będąca na terenie Sowińskich poprosiła go o pomoc (bo nikt inny nie pomaga). Karolinus i Strzała odkryli, że Cede robił coś w Siewczynie i Gwiazdoczach i ktoś udaje, że Cede miał problemy z mafią. Dalsze poszukiwania wykazały, że w sprawę zamieszana jest jednostka do walki psychotronicznej (???) oraz wysoko postawieni Samszarowie. Karolinus współpracując z Aleksandrem dotarł do swojego przeszłego nauczyciela, Fabiana, i zażył amnestyki. Ale Aleksander zostawił w Strzale środki naprawcze - które zadziałają po pewnym czasie.

Aktor w Opowieści:

* Dokonanie:
    * analizuje dane komputera w pokoju Cede, zmasakrowała biednego demona i znalazła że w tle jest jednostka do walki psychotronicznej. Została niewykryta.
* Progresja:
    * okazuje się, że jej pamięć jest uszkodzona; robili z nią jakieś eksperymenty. Ma geas i nie jest w stanie sobie przypomnieć.


### Samszarowie, Lemurczak i fortel Strzały

* **uid:** 230509-samszarowie-lemurczak-i-fortel-strzaly, _numer względny_: 7
* **daty:** 0095-07-24 - 0095-07-26
* **obecni:** AJA Szybka Strzała, Elena Samszar, Irek Kraczownik, Jonatan Lemurczak, Karolinus Samszar, Roland Samszar

Streszczenie:

Strzała w ruinie, ale dała radę dotrzeć do w miarę bezpiecznego miejsca pod grzmotoptakami. Gdy dwójka nastolatków na które poluje ich zmieniona przez Lemurczaka matka się pojawili blisko, Elena ją unieruchomiła a Karolinus przekształcił w normalną formę. Acz Paradoksem wysłał sygnaturę Verlenopodobną. Gdy Karolinus i Elena się kłócą czy pomóc czy czekać, Strzała pojedynczą droną wymanewrowała Stegozaur-class support hovertank i przestraszyła Lemurczaka hintując, że Verlenowie polujący na ptaki są w pobliżu. Elena zmanipulowała Irka, więc E+K wyszli na osoby pozytywne które chcą dobrze, acz nie zawsze mają idealne plany (bo są młodzi). A Roland zajmie się Sanktuarium Kazitan.

Aktor w Opowieści:

* Dokonanie:
    * mimo połowy systemów zniszczonych, dotarła do bezpiecznej jaskini niedaleko grzmotoptaków. Używając okoliczności i inteligencji w tak marnym stanie przestraszyła Lemurczaka (jego hovertank), że tu są Verlenowie i mają złe intencje. A potem zdała pełen raport Rolandowi Samszarowi i zgasła. Shutdown + autorepair.


### Egzorcysta z Sanktuarium

* **uid:** 230411-egzorcysta-z-sanktuarium, _numer względny_: 6
* **daty:** 0095-07-21 - 0095-07-23
* **obecni:** AJA Szybka Strzała, Arnold Kazitan, Elena Samszar, Irek Kraczownik, Karolinus Samszar, Tadeusz Dzwańczak

Streszczenie:

Egzorcysta Irek poszukiwany przez Samszarów jest w Sanktuarium Kazitan w Przelotyku Zachodnim Dzikim. Na miejscu Sanktuarium ulega Emisji - katastroficzne elementalne działania. Zespół ratuje dzieciaka od Emisji (najpierw mu zagrażając XD), ale gdy dociera do egzorcysty - ostatniego maga który próbuje pomóc Sanktuarium, to go porywają. Przy próbie porwania Strzała zostaje ciężko uszkodzona i nie daje rady wrócić do Powiatu Samszar - crashlanduje w bezpiecznej części Przelotyka.

Aktor w Opowieści:

* Dokonanie:
    * Sanktuarium Kazitan podczas Emisji jest miejscem na granicy możliwości Strzały; została uszkodzona, ale skutecznie tam Zespół doprowadziła. Jednak potem, gdy Zespół uciekał z porwanym Irkiem z Sanktuarium, Strzała tak starała się żadnego cywila nie zastrzelić że została ciężko uszkodzona (pokazując, że jest najbardziej "ludzka i humanitarna" z całego Zespołu). Ale doprowadziła wszystkich bezpiecznie do Przelotyka i wylądowała w ruinie.
* Progresja:
    * bardzo ciężko uszkodzona, wymaga sporej naprawy. Doprowadziła jednak wszystkich poza Sanktuarium Kazitan i skutecznie porwała Irka.


### Wszystkie duchy Siewczyna

* **uid:** 230404-wszystkie-duchy-siewczyna, _numer względny_: 5
* **daty:** 0095-07-18 - 0095-07-20
* **obecni:** AJA Szybka Strzała, Elena Samszar, Irek Kraczownik, Karolinus Samszar, Maksymilian Sforzeczok

Streszczenie:

Karolinus, wraz z kuzynką Eleną Samszar zostali wysłani do Siewczyna gdzie podobno są problemy (by uniknąć dalszego antagonizowania Verlenów). Na miejscu okazało się, że niekompetentny egzorcysta Irek sprowadził tu Hybrydę noktiańskiej TAI i ducha oraz ten byt próbował zemścić się za śmierć swoich ludzi. Elena Paradoksem zniszczyła Ducha Opiekuńczego Spichlerza a Karolinus Paradoksem stworzył strażniczego ducha w kształcie Vioriki w bikini. A w tle - spory między ludźmi i duchami (podsycane przez Hybrydę) oraz między podejściem 'ekonomia vs harmonia'.

Aktor w Opowieści:

* Dokonanie:
    * odkryła konflikt między duchami a ludźmi, samodzielnie wykonywała zwiady i weszła w wojnę psychotroniczną z Hybrydą AI-ducha.


### Niepotrzebny ratunek Mai

* **uid:** 230328-niepotrzebny-ratunek-mai, _numer względny_: 4
* **daty:** 0095-06-30 - 0095-07-02
* **obecni:** AJA Szybka Strzała, Apollo Verlen, Bonifacy Samszar, Fiona Szarstasz, Karolinus Samszar, Maja Samszar, Romeo Verlen, Viorika Verlen

Streszczenie:

Tydzień po ucieczce z Verlenlandu, Maja, kuzynka Karolinusa, zostaje porwana. Karolinus próbuje zdobyć informacje i nagrania z porwania. Odkrywa, że Maja miała kontakt z Romeo Verlenem, a jej rodzice kasują informacje o nieeleganckim zachowaniu córki. Karolinus łączy się z Romeem, który twierdzi, że Maja jest bezpieczna w mieście VirtuFortis (i ogólnie jest OK). Karolinus wyrusza tam ze swoim zespołem na pokładzie Szybkiej Strzały. 

Niestety, podczas Paradoksu udało się przypadkowo Karolinusowi zaatakować duchami o kształcie Mai Verlenland. Docierają w końcu do VirtuFortis i Karolinus kontaktuje się z Mają. Maja twierdzi, że jest porwana, ale Fiona wyczuwa, że Maja ściemnia. Okazuje się, że Maja uciekła z domu i przyjechała na turniej Supreme Missionforce, żeby zmierzyć się z Romeem. Karolinus przekonuje Maję, żeby wzięła udział w turnieju, a on odbierze ją po tygodniu. Reputacja Karolinusa wzrasta wśród Verlenów, ale spada wśród Samszarów. Fiona zostaje uważana za zły wpływ na Karolinusa.

Aktor w Opowieści:

* Dokonanie:
    * skuteczne unikanie wykrycia, dzięki czemu Karolinus mógł dostać się do VirtuFortis. Wymanewrowała potwora i Viorikę (która na nią nie polowała)


### Brudna konkurencja w Arachnoziem

* **uid:** 230314-brudna-konkurencja-w-arachnoziem, _numer względny_: 3
* **daty:** 0095-06-20 - 0095-06-22
* **obecni:** AJA Szybka Strzała, Ania Turabnik, Fiona Szarstasz, Fircjusz Szarstasz, Julita Mopsarin, Kacper Aczramin, Karolinus Samszar, Laura Turabnik

Streszczenie:

Karolinus Samszar przybywa na prośbę swej guwernantki Fiony by pomóc w jej rodzinnej firmie ArachnoBuild, która ryzykuje utratę swojej pozycji na rynku i która jest w Verlenlandzie. Pojawił się Potwór przez błędy w działaniach ArachnoBuild. Karolinus dowiaduje się od barmanki Laury o konkurencji - firmie EnMilStrukt, finansowanej przez Brunhildę Verlen. EnMilStrukt ma inne podejście do prowadzenia biznesu (nie harmonia z naturą a dominacja natury) i zyskuje poparcie części mieszkańców.

Fiona i Karolinus dochodzą do wniosku, że jedynymi osobami, które mogły wyciec dane, są Kacper i Ania. Dane wskazują na odchylenia w lokalizacji jaszczurów od czasu pojawienia się EnMilStrukt. Fiona rozmawia z bratem Fircjuszem, który ujawnia swoje uczucia do Ani, a Kacper okazuje się być starym przyjacielem rodziny. Ania ma kompleks, że nie jest wystarczająco dobra, a Kacper ma długi.

Pod wpływem magii puryfikacji Karolinusa, Ania przyznaje, że spotkała kogoś, kto twierdził, że jest kuzynem Fircjusza, i przekazywała mu dane (magia mentalna). Miejsce spotkań to kopalnia, a numer telefonu jest przypisany do ludzi zatrudnionych przez EnMilStrukt.Strzała próbuje zlokalizować telefon, ale nie udaje się zdobyć wystarczających dowodów, aby udowodnić, że EnMilStrukt miało wiedzę o spisku. Za to Karolinusowi udało się dotrzeć do wrogiej czarodziejki i Paradoksem zmienił populację większości miasta w Fionę XD. Viorika Verlen próbowała złapać Karolinusa, ale Strzała może lecieć w stratosferę XD.

Aktor w Opowieści:

* Dokonanie:
    * (Autonomiczna Jednostka Aurum; zamaskowana eks-ŁZa militarna która podczas wojny uratowała życie Samszara i teraz jest sprzężona z duchem. ADVANTAGES: walka elektroniczna, walka, skan, transport, hidden.) Zlokalizowała telefon używany przez ludzi zatrudnionych przez EnMilStrukt a potem szybko dostarczyła Karolinusa do lokalizacji wrogiej czarodziejki. Skutecznie zwiała Viorice lecąc w stratosferę.


### Kłótnie sąsiadów w Wańczarku

* **uid:** 230425-klotnie-sasiadow-w-wanczarku, _numer względny_: 2
* **daty:** 0095-05-16 - 0095-05-19
* **obecni:** AJA Szybka Strzała, Artemis Lawellan, Artur Lawellan, Damian Fenekis, Elea Brzozecka, Ilfons Lawellan, Karolinus Samszar, Olga Fenekis

Streszczenie:

Karolinus został wysłany do Wańczarka, by rozsądzać spory między sąsiadami odnośnie sadów, ale wpakował się w starą intrygę ciemniejszej strony Samszarów i Blakenbauerów odnośnie robienia Koncentratu Oświecenia z soku mandragory. Gdy pojawiła się próba utopienia Ilfonsa przez Eleę, Strzała wyciągnęła topielca a Karolinus go uratował. Karolinus zmienił jednego Lawellana w świnię, a potem ze Strzałą przyszpilili Artemis która powiedziała im prawdę o tym terenie - jak powstaje Koncentrat Oświecenia. Aha, miesiąc temu zniknął tu dziennikarz z Verlenlandu.

Aktor w Opowieści:

* Dokonanie:
    * sprawdza przychody, rozchody, sprawdza wioskę pobieżnie. Spaliła pół elektroniki wioski (za słabe), ale ma kluczowe dane. Ratuje życie topielca Ilfonsa. Potem monitoruje Artemis Lawellan i ją konfrontuje w imieniu Karolinusa.


### Karolinus, sędzia Mirkali

* **uid:** 230620-karolinus-sedzia-mirkali, _numer względny_: 1
* **daty:** 0095-04-15 - 0095-04-18
* **obecni:** Agnieszka Klirpin, AJA Szybka Strzała, Filip Klirpin, Juanita Derwisz, Karolinus Samszar

Streszczenie:

Mirkala - miasteczko, gdzie stare (zioła) zwalcza się z nowym (Juanita pragnie budować ekologiczne prefab-domy na wzgórzach). Karolinus przekonał Starszyznę, że muszą dać młodym odskocznię (i znaleźć pieniądze) by Mirkala nie była całkowitym kołchozem, a potem z pomocą Strzały odepchnął Juanitę z tego terenu; niech nie mści się na Starszyźnie za śmierć rodziny jej przyjaciela.

Aktor w Opowieści:

* Dokonanie:
    * odkryła, że wszystkie dokumenty Mirkali są całkowicie fikcyjne. Potem przygotowała hiper-paczkę wiadomości odnośnie rodziny przyjaciela Juanity i złamała morale Juanity. Juanita przestała walczyć.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 11, @: 0095-08-25
    1. Primus    : 11, @: 0095-08-25
        1. Sektor Astoriański    : 11, @: 0095-08-25
            1. Astoria    : 11, @: 0095-08-25
                1. Sojusz Letejski    : 11, @: 0095-08-25
                    1. Aurum    : 9, @: 0095-08-25
                        1. Powiat Samszar    : 7, @: 0095-08-25
                            1. Fort Tawalizer    : 1, @: 0095-08-18
                                1. Obserwatorium Potworów i Fort (E)    : 1, @: 0095-08-18
                                    1. Areszt    : 1, @: 0095-08-18
                                    1. Koszary garnizonu    : 1, @: 0095-08-18
                                1. Wzgórza Potworów (N)    : 1, @: 0095-08-18
                                    1. Farmy kóz    : 1, @: 0095-08-18
                            1. Gwiazdoczy, okolice    : 1, @: 0095-08-25
                                1. Technopark Jutra    : 1, @: 0095-08-25
                                    1. Inkubatory małego biznesu    : 1, @: 0095-08-25
                            1. Gwiazdoczy    : 2, @: 0095-08-25
                            1. Karmazynowy Świt, okolice    : 1, @: 0095-08-11
                                1. Centrum Danych Symulacji Zarządzania    : 1, @: 0095-08-11
                                    1. Techbunkier Arvitas    : 1, @: 0095-08-11
                                        1. Kontrola bezpieczeństwa (1)    : 1, @: 0095-08-11
                                        1. Kwatery mieszkalne (1)    : 1, @: 0095-08-11
                            1. Karmazynowy Świt    : 1, @: 0095-08-11
                            1. Mirkala    : 1, @: 0095-04-18
                                1. Wielka Zielarnia (E)    : 1, @: 0095-04-18
                                1. Wzgórza Bezduszne (W)    : 1, @: 0095-04-18
                                    1. Fabryka Domów    : 1, @: 0095-04-18
                                    1. Technopark    : 1, @: 0095-04-18
                            1. Siewczyn    : 2, @: 0095-08-05
                                1. Astralne Ogrody (ES)    : 1, @: 0095-07-20
                                    1. Centralne Biura Rolnicze    : 1, @: 0095-07-20
                                    1. Centrum R&D dla zrównoważonego rolnictwa    : 1, @: 0095-07-20
                                    1. Dom Szamana    : 1, @: 0095-07-20
                                    1. Fabryka neutralizatorów astralnych    : 1, @: 0095-07-20
                                    1. Gaj Duchów    : 1, @: 0095-07-20
                                    1. Rezydencje Harmonii    : 1, @: 0095-07-20
                                1. Centrum Jedności Mieszka (Center)    : 1, @: 0095-07-20
                                    1. Bazar rękodzieła    : 1, @: 0095-07-20
                                    1. Drzewo Jedności    : 1, @: 0095-07-20
                                    1. Most jedności    : 1, @: 0095-07-20
                                    1. Park miejski    : 1, @: 0095-07-20
                                    1. Ratusz miejski    : 1, @: 0095-07-20
                                1. Północne obrzeża    : 1, @: 0095-07-20
                                    1. Spichlerz Jedności    : 1, @: 0095-07-20
                                1. Wzgórza Industrialnej Harmonii (NW)    : 1, @: 0095-07-20
                                    1. Fabryka siewników i sadzarek    : 1, @: 0095-07-20
                                    1. Fabryka traktorów    : 1, @: 0095-07-20
                                    1. Przestrzeń mieszkalna    : 1, @: 0095-07-20
                                    1. Sklepy i centra handlowe    : 1, @: 0095-07-20
                            1. Wańczarek    : 1, @: 0095-05-19
                                1. Wańczarek    : 1, @: 0095-05-19
                                    1. Dzielnica Sadowa (NW, W)    : 1, @: 0095-05-19
                                        1. Dom sołtysa    : 1, @: 0095-05-19
                                        1. Sady owocowe (NW)    : 1, @: 0095-05-19
                                    1. Dzielnica Smutku (E)    : 1, @: 0095-05-19
                                        1. Szpital Jabłoni    : 1, @: 0095-05-19
                        1. Verlenland    : 3, @: 0095-08-25
                            1. Arachnoziem    : 1, @: 0095-06-22
                                1. Bar Łeb Jaszczura    : 1, @: 0095-06-22
                                1. Kopalnia    : 1, @: 0095-06-22
                            1. Duchowiec Śmieszny, okolice    : 1, @: 0095-08-25
                            1. Duchowiec Śmieszny    : 1, @: 0095-08-25
                            1. VirtuFortis    : 1, @: 0095-07-02
                                1. Akademia VR Aegis    : 1, @: 0095-07-02
                                1. Bastion Przyrzeczny    : 1, @: 0095-07-02
                                1. Bazarek Lokalny    : 1, @: 0095-07-02
                                1. Stadion Sportowy    : 1, @: 0095-07-02
                                1. Wielki Plac Miraży    : 1, @: 0095-07-02
                    1. Przelotyk    : 2, @: 0095-07-26
                        1. Przelotyk Zachodni Dziki    : 2, @: 0095-07-26
                            1. Lancatim, okolice    : 1, @: 0095-07-26
                            1. Lancatim    : 1, @: 0095-07-26
                            1. Sanktuarium Kazitan    : 1, @: 0095-07-23
                                1. Dystrykt Szafir    : 1, @: 0095-07-23
                                    1. Komnata lecznicza    : 1, @: 0095-07-23
                                    1. Komnata mieszkalna    : 1, @: 0095-07-23

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolinus Samszar    | 11 | ((230314-brudna-konkurencja-w-arachnoziem; 230328-niepotrzebny-ratunek-mai; 230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230425-klotnie-sasiadow-w-wanczarku; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230523-romeo-dyskretny-instalator-supreme-missionforce; 230606-piekna-diakonka-i-rytual-nirwany-koz; 230613-zaginiecie-psychotronika-cede; 230620-karolinus-sedzia-mirkali; 230627-ratuj-mlodziez-dla-kajrata)) |
| Elena Samszar        | 6 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230523-romeo-dyskretny-instalator-supreme-missionforce; 230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata)) |
| Irek Kraczownik      | 3 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Fiona Szarstasz      | 2 | ((230314-brudna-konkurencja-w-arachnoziem; 230328-niepotrzebny-ratunek-mai)) |
| Herbert Samszar      | 2 | ((230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata)) |
| Impresja Ignicja Incydencja Diakon | 2 | ((230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata)) |
| Maja Samszar         | 2 | ((230328-niepotrzebny-ratunek-mai; 230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Romeo Verlen         | 2 | ((230328-niepotrzebny-ratunek-mai; 230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Agnieszka Klirpin    | 1 | ((230620-karolinus-sedzia-mirkali)) |
| Albert Samszar       | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Aleksander Samszar   | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Amanda Kajrat        | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Ania Turabnik        | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Apollo Verlen        | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Arnold Kazitan       | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Artemis Lawellan     | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Artur Lawellan       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Bonifacy Samszar     | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Cede Burian          | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Celina Burian        | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Damian Fenekis       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Dźwiedź Łagodne Słowo | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Elea Brzozecka       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Ernest Kajrat        | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Fabian Samszar       | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Filip Klirpin        | 1 | ((230620-karolinus-sedzia-mirkali)) |
| Fircjusz Szarstasz   | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Ilfons Lawellan      | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Jonatan Lemurczak    | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Juanita Derwisz      | 1 | ((230620-karolinus-sedzia-mirkali)) |
| Julita Mopsarin      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Kacper Aczramin      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Lara Ukraptin        | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Laura Turabnik       | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Maks Samszar         | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Maksymilian Sforzeczok | 1 | ((230404-wszystkie-duchy-siewczyna)) |
| Mitria Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Nataniel Samszar     | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Olga Fenekis         | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Roland Samszar       | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Tadeusz Dzwańczak    | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Talia Aegis          | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Viorika Verlen       | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Wacław Samszar       | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Wargun Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |