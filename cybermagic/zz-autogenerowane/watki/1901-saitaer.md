# Saitaer
## Identyfikator

Id: 1901-saitaer

## Sekcja Opowieści

### To nie pułapka na Nereidę...

* **uid:** 220105-to-nie-pulapka-na-nereide, _numer względny_: 14
* **daty:** 0112-05-04 - 0112-05-09
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Lutus Amerin, Maria Naavas, Natalia Aradin, Saitaer

Streszczenie:

Plan - dorwanie tajemniczych koloidowych korwet. Wiemy, że to pułapka, więc zróbmy kontr-pułapkę. Ale okazało się, że to nie pułapka na Nereidę a na Infernię. Standardowe badania wprowadziły w Infernię (Dianę) memetic payload i Diana prawie wpadła pod kontrolę Saitaera - odrzucenie kokonu z Natalią sprawiło, że udało się ciężko uszkodzoną Infernię uratować. Zwalczając ixion Arianna sięgnęła do Esuriit przez Elenę; Infernia by zginęła gdyby nie samobójcza akcja Izabeli i Kamila. Elena wchłonęła tyle energii ile była w stanie, przekraczając swój Wzór. ALE - Infernia przetrwała. Przy okazji, Natalia się wykluła. Ani osoba ani statek kosmiczny - mordercza anomalia kosmiczna.

Aktor w Opowieści:

* Dokonanie:
    * zaplanował przechwycenie Inferni, Eleny, lub Arianny. Co najmniej Natalii. Co prawda prawie zniszczył te wszystkie jednostki, ale nie udało mu się uzyskać ani jednego agenta. Nawet Natalia jest wolną istotą.


### Narodziny paladynki Saitaera

* **uid:** 201011-narodziny-paladynki-saitaera, _numer względny_: 13
* **daty:** 0110-10-12 - 0110-10-13
* **obecni:** Agaton Ociegor, Aleksander Muniakiewicz, Gabriel Ursus, Minerwa Metalia, Pięknotka Diakon, Sabina Kazitan, Saitaer

Streszczenie:

Pięknotka wyprawiła Sabinę Kazitan do Aurum po akcji Gabriela, ale wymuszając na obu stronach by zaprzestały wojny ze sobą. Jako, że zaczął się atak na kompleks Grzymościa, Alan i Gabriel zostali tam przekierowani - a Pięknotka poszła z Minerwą do parku rozrywki Janor by zakończyć tą sprawę. Niestety, grzymościowiec Ociegor złapał Pięknotkę chcąc ją "wyleczyć" z kralotycznej siły. Pięknotka wezwała Cienia i ów prawie zabił Ociegora. Walcząc z Cieniem, Pięknotka umarła - i Saitaer ją przywrócił jako swoją paladynkę.

Aktor w Opowieści:

* Dokonanie:
    * gdy Pięknotka umierała w walce z Cieniem, powiedział jej, że jej może pomóc. Pięknotka opierała się mu do końca, ale ją wskrzesił jako paladynkę - acz nie ma nad nią pełni kontroli.


### Niespodziewany wpływ Aidy

* **uid:** 190804-niespodziewany-wplyw-aidy, _numer względny_: 12
* **daty:** 0110-06-30 - 0110-07-02
* **obecni:** Amanda Kajrat, Gabriel Ursus, Julia Morwisz, Ossidia Saitis, Pięknotka Diakon, Saitaer

Streszczenie:

Po złapaniu Aidy przez Hralwagha z Cieniaszczytu okazało się, że Aida wpłynęła na samego Hralwagha. Potwór splugawił swoich kontrolerów z Cieniaszczytu. Pięknotka współpracując z Ossidią i Saitaerem pokonała Hralwagha zanim komukolwiek stała się krzywda. Dodatkowo - Ossidia porwała Amandę Kajrat dla Saitaera; Ossidia dalej poluje na Ernesta Kajrata.

Aktor w Opowieści:

* Dokonanie:
    * wezwany przez Pięknotkę, zniszczył Hralwagha dla niej; pozyskał sporo kralotycznego adaptogenu na swoje eksperymenty.


### Uciekający seksbot

* **uid:** 190519-uciekajacy-seksbot, _numer względny_: 11
* **daty:** 0110-04-25 - 0110-04-26
* **obecni:** Arnulf Poważny, Eliza Ira, Ernest Kajrat, Liliana Bankierz, Ossidia Saitis, Pięknotka Diakon, Saitaer, Teresa Mieralit, Tomasz Tukan

Streszczenie:

Ernest z mafii przybył do szkoły magów w Zaczęstwie szukając swojego seksbota. Pięknotka deeskalowała sytuację i poszła z neuronautą Tomaszem szukać; okazało się, że seksbot jest bardzo zmodyfikowany i bardzo świadomy - do tego stopnia, że seksbotowi zaczęła pomagać Eliza Ira. Pięknotka i Tomasz rozmontowali problem, ale Pięknotka nie miała serca oddać seksbota sadystycznemu Ernestowi. W rozpaczy, poprosiła o pomoc Saitaera, który odpowiedział...

Aktor w Opowieści:

* Dokonanie:
    * wezwany przez Pięknotkę, odpowiedział, że pomoże seksbotowi i da mu albo wolność albo dobrą śmierć. O krok bliżej do ewolucji Pięknotki.


### Czarodziejka z woli Saitaera

* **uid:** 190202-czarodziejka-z-woli-saitaera, _numer względny_: 10
* **daty:** 0110-02-01 - 0110-02-05
* **obecni:** Adela Kirys, Karolina Erenit, Minerwa Metalia, Pięknotka Diakon, Saitaer, Sławomir Muczarek, Tymon Grubosz, Wojtek Kurczynos

Streszczenie:

Wojtek Kurczynos ZNOWU dobierał się do Karoliny Erenit. Przez machinacje Sławka i Adeli Saitaer dokonał transfuzji mocy magicznej od Wojtka do Karoliny. Pięknotka drastycznie ukrywała wszystkie wpływy wszystkich osób, by ucierpiał tylko winny Wojtek. Nawet współpracowała z Saitaerem, by terroformizować Wojtka. Wszystko, by Karolina zachowała moc magiczną. ALE - doprowadziła do tego, że spojrzeli na Karolinę Mausowie. W ten sposób Karradrael uniemożliwił hold Saitaera na Karolinie.

Aktor w Opowieści:

* Dokonanie:
    * robił eksperyment w transfuzji energii maga do człowieka. Aktywnie pomagał Pięknotce w maskowaniu tego eksperymentu. Niestety, nie dostał tego co chciał.
* Progresja:
    * dowiedział się sporo odnośnie wykorzystania energii ixiońskiej do transfuzji energii magicznej od maga do człowieka.
    * wszystkie plany w uzyskaniu nowej agentki (Karoliny Erenit) się nie powiodły. Trudno, nie dostał czego chciał.


### Ixioński Transorganik

* **uid:** 190127-ixionski-transorganik, _numer względny_: 9
* **daty:** 0110-01-29 - 0110-01-30
* **obecni:** Erwin Galilien, Karolina Erenit, Kirył Najłalmin, Minerwa Metalia, Napoleon Bankierz, Pięknotka Diakon, Saitaer, Tymon Grubosz, Wiktor Satarail, Wojtek Kurczynos

Streszczenie:

Minerwa próbowała pomóc Karolinie Erenit, ale niestety użyła artefaktu ixiońskiego i przekształciła wrednego Wojtka Kurczynosa w transorganika. Pięknotka z pomocą Epirjona przeteleportowała siebie i Wojtka na Trzęsawisko i tam z pomocą Wiktora Sataraila odwrócili tą transorganizację. Ten konkretny problem udało się dyskretnie rozwiązać.

Aktor w Opowieści:

* Dokonanie:
    * zgłosił się do Pięknotki - zmusił ją do naprawy ixiońskiego transorganika który był Wojtkiem. Czemu? Bo to maladaptacja.
* Progresja:
    * planuje wykorzystać ixiońską energię rezydualną na Wojtku Kurczynosie.


### Wypalenie Saitaera z Trzęsawiska

* **uid:** 190116-wypalenie-saitaera-z-trzesawiska, _numer względny_: 8
* **daty:** 0110-01-07 - 0110-01-09
* **obecni:** Alan Bartozol, ASD Centurion, Hieronim Maus, Karradrael, Kasjopea Maus, Pięknotka Diakon, Saitaer, Wiktor Satarail

Streszczenie:

Pięknotka dostała zadanie eskortowania kapłana Karradraela do ołtarza Saitaera. ASD Centurion oraz Alan odwracali uwagę Trzęsawiska. Plan się udał, choć w wyniku Pięknotka została znowu przekształcona przez Saitaera. Za to, nie ma już wpływu Saitaera w okolicy - więcej, udało się zdjąć też wpływ z Wiktora.

Aktor w Opowieści:

* Dokonanie:
    * po raz kolejny odparty; tym razem wypalony z Wiktora Sataraila i z Trzęsawiska Zjawosztup. Nie podjął rękawicy Karradraela by rozpocząć wojnę totalną.
* Progresja:
    * utracił _hold_ na Trzęsawisku Zjawosztup i w Wiktorze Satarailu. Uzyskał możliwość łatwej transformacji Pięknotki.


### Eksperymenty na wiłach?

* **uid:** 190112-eksperymenty-na-wilach, _numer względny_: 7
* **daty:** 0109-12-28 - 0109-12-31
* **obecni:** Alan Bartozol, Kornel Garn, Pięknotka Diakon, Saitaer, Sławomir Muczarek

Streszczenie:

Alan powiedział Pięknotce, że Myszy polują na wiły dla kogoś. Okazało się, że były wojskowy (Kornel) eksperymentuje na wiłach by stworzyć potężną pryzmatyczną broń defensywną w formie kwiatów. Niestety, to dzieje się na terenie na którym był Saitaer i Władca Rekonstrukcji powrócił. Nadal słaby, ale zintegrował się z Pięknotką i zbudował ołtarz na Trzęsawisku. Kornel uciekł na swój teren a cała akcja skończyła się rozpadem Myszy.

Aktor w Opowieści:

* Dokonanie:
    * obudzony przez krew umierającej wiły i modlitwę Pięknotki, zbudował ołtarz na Trzęsawisku Zjawosztup poświęcając odbudowaną wiłę.
* Progresja:
    * na Trzęsawisku Zjawosztup znajduje się jego ołtarz, sam Saitaer jest przebudzony oraz Kornel jest nim zafascynowany.


### Uwięzienie Saitaera

* **uid:** 181230-uwiezienie-saitaera, _numer względny_: 6
* **daty:** 0109-12-07 - 0109-12-09
* **obecni:** Karla Mrozik, Kreacjusz Diakon, Lucjusz Blakenbauer, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Pięknotka wróciła do Pustogoru, będąc po drodze zaatakowana przez latającego pnączoszpona. Odkryła, że Saitaer poważnie zmienił już Minerwę. Zmartwiła się tym, jak bardzo ta Minerwa różni się od jej przyjaciółki. Ta sprawa jest ponad siły Pięknotki - poszła do Karli, przełożonej Pustogoru. Tam dowiedziała się, że Saitaer to broń i że być może to on stoi za problemami Trzęsawiska Zjawosztup. Pustogor pojmał Saitaera i przejął wskrzeszoną Minerwę do leczenia.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * containowany przez Pustogor. Pytanie na jak długo.


### Adieu, Cieniaszczycie

* **uid:** 181227-adieu-cieniaszczycie, _numer względny_: 5
* **daty:** 0109-11-28 - 0109-12-01
* **obecni:** Amadeusz Sowiński, Atena Sowińska, Brygida Maczkowik, Erwin Galilien, Lilia Ursus, Mirela Niecień, Moktar Gradon, Pietro Dwarczan, Pięknotka Diakon, Waleria Cyklon, Zbigniew Burzycki

Streszczenie:

Pięknotka zdobyła nowe ciało dla Minerwy (acz do jego uruchomienia człowiek musi być poświęcony!), przekonała Amadeusza do założenia świątyni Arazille do blokowania Finis Vitae oraz wepchnęła Lilię jako asystentkę Atenie. Do tego rozkochała solidnie Erwina i zamknęła wszystkie wątki, by móc wracać do Pustogoru. Nie dała rady rozdzielić Julii i Łysych Psów, niestety.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * zapewnił odpowiednie spowolnienie Pięknotki przy jej powrocie do Pustogoru. Ma dość czasu - Minerwę i infekcję rzeczywistości.


### Finis Vitae

* **uid:** 181226-finis-vitae, _numer względny_: 4
* **daty:** 0109-11-18 - 0109-11-27
* **obecni:** Amadeusz Sowiński, Arazille, Atena Sowińska, Moktar Gradon, Pięknotka Diakon, Waleria Cyklon

Streszczenie:

Moktar zregenerował Pięknotkę i powiedział jej, że chce od niej uratowania swojego Skażonego oficera. Pięknotka dowiedziała się od Amadeusza o autowarze Finis Vitae pod Cieniaszczytem; Saitaer nie może nic wiedzieć. Pięknotka doprowadziła do tego, że Lilia wróciła do Cieniaszczytu (courtesy of Moktar), uratowała spod ziemi tego oficera, oraz zobaczyła miejsce, które będzie chyba do końca życia widziała w koszmarach. Gdyby nie Atena, zginęłaby na tej akcji.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * ma 10 dni spokojnej pracy nad swoimi mrocznymi planami. Tylko on, Minerwa oraz Kreacjusz.


### Czyszczenie toksycznych związków

* **uid:** 181225-czyszczenie-toksycznych-zwiazkow, _numer względny_: 3
* **daty:** 0109-11-13 - 0109-11-17
* **obecni:** Atena Sowińska, Bogdan Szerl, Julia Morwisz, Moktar Gradon, Pięknotka Diakon, Romuald Czurukin

Streszczenie:

Pięknotka ściągnęła Erwina na detoks od Saitaera, acz dała mu informację o Kreacjuszu Diakonie. Saitaer planuje zbudować sprawne ciało dla Minerwy. Atena jest zregenerowana w dość anielskiej formie i dostaje informacje o Orbiterze od Julii. Pięknotka próbuje naprawić pary Romuald x Julia oraz Wioletta x Pietro. Moktar uwalnia Pięknotkę od wpływu Bogdana, masakrując swojego agenta w brutalny sposób.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * ma warm lead do Kreacjusza Diakona, osoby zajmującej się farmami klonów w okolicach Pustogoru. Wykona swój plan.
    * chce przejąć kontrolę nad pomniejszymi salami do klonowania.


### Odklątwianie Ateny

* **uid:** 181112-odklatwianie-ateny, _numer względny_: 2
* **daty:** 0109-10-23 - 0109-10-26
* **obecni:** Amadeusz Sowiński, Atena Sowińska, Cezary Zwierz, Lucjusz Blakenbauer, Minerwa Diakon, Pięknotka Diakon, Saitaer

Streszczenie:

Atenie się nie poprawia a jej echo atakuje Pięknotkę. By zrozumieć co się dzieje, Pięknotka rozmawia z "terrorformem" (Saitaerem) i dowiaduje się o krwawym klątwożycie. Następnie używając wpływów Adama Szarjana wyrywa Atenę spod opieki lekarza i transportuje ją do Cieniaszczytu, by tam można było jej pomóc. By jej pomóc i odkryć napastnika potrzeba jest moc kralotyczna - Atena się nie zgadza, lecz Pięknotka ją przekonała. Atena ulega rekonstrukcji kralotycznej a Amadeusz ma mniej więcej namiar na twórcę klątwożyta.

Aktor w Opowieści:

* Dokonanie:
    * zamieszkujący ciało terrorforma corruptor oraz władca magii krwi. Okrutny. Chce wskrzesić swój świat i Naznaczył Atenę, Pięknotkę i Erwina. Klasyfikacja: demon? Bóstwo?


### Skażenie Grazoniusza

* **uid:** 190123-skazenie-grazoniusza, _numer względny_: 1
* **daty:** 0079-03-30 - 0079-04-01
* **obecni:** ASD Grazoniusz, Ataienne

Streszczenie:

ASD Grazoniusz, statek wydobywczo-górniczy, natknął się na uśpionego Saitaera w asteroidach. Aspekty Saitaera szybko i bardzo dyskretnie pozbyły się Mausów oraz przejęły kontrolę nad statkiem tak, że Astoria nie miała o niczym pojęcia. Co gorsza, Saitaer dyskretnie schował część swoich agentów i rozprzestrzenia się w taki sposób, że nikt nie ma o niczym pojęcia.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * otrzymał statek flagowy - ASD Grazoniusz. Dodatkowo, rozsiał ukrytych agentów, którzy mają zwiększyć jego moc i jego wiarę.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 9, @: 0112-05-09
    1. Primus    : 9, @: 0112-05-09
        1. Sektor Astoriański    : 9, @: 0112-05-09
            1. Astoria, Pierścień Zewnętrzny    : 1, @: 0112-05-09
                1. Poligon Stoczni Neotik    : 1, @: 0112-05-09
                1. Stocznia Neotik    : 1, @: 0112-05-09
            1. Astoria    : 8, @: 0110-10-13
                1. Sojusz Letejski    : 8, @: 0110-10-13
                    1. Przelotyk    : 1, @: 0109-10-26
                        1. Przelotyk Wschodni    : 1, @: 0109-10-26
                            1. Cieniaszczyt    : 1, @: 0109-10-26
                                1. Kompleks Nukleon    : 1, @: 0109-10-26
                                1. Pałac Szkarłatnego Światła    : 1, @: 0109-10-26
                    1. Szczeliniec    : 8, @: 0110-10-13
                        1. Powiat Jastrzębski    : 1, @: 0110-10-13
                            1. Praszalek, okolice    : 1, @: 0110-10-13
                                1. Lasek Janor    : 1, @: 0110-10-13
                                1. Park rozrywki Janor    : 1, @: 0110-10-13
                        1. Powiat Pustogorski    : 6, @: 0110-07-02
                            1. Podwiert, okolice    : 1, @: 0110-07-02
                                1. Bioskładowisko podziemne    : 1, @: 0110-07-02
                            1. Podwiert    : 2, @: 0110-07-02
                                1. Magazyny sprzętu ciężkiego    : 2, @: 0110-07-02
                            1. Pustogor    : 2, @: 0109-12-31
                                1. Barbakan    : 1, @: 0109-12-31
                                1. Eksterior    : 1, @: 0109-10-26
                                    1. Miasteczko    : 1, @: 0109-10-26
                                1. Rdzeń    : 1, @: 0109-10-26
                                    1. Szpital Terminuski    : 1, @: 0109-10-26
                            1. Zaczęstwo    : 4, @: 0110-04-26
                                1. Akademia Magii, kampus    : 2, @: 0110-04-26
                                    1. Budynek Centralny    : 2, @: 0110-04-26
                                        1. Skrzydło Loris    : 1, @: 0110-04-26
                                1. Cyberszkoła    : 2, @: 0110-02-05
                                1. Nieużytki Staszka    : 1, @: 0109-12-31
                                1. Osiedle Ptasie    : 2, @: 0110-02-05
                        1. Trzęsawisko Zjawosztup    : 3, @: 0110-01-30
                            1. Głodna Ziemia    : 2, @: 0110-01-30
                            1. Laboratorium W Drzewie    : 1, @: 0110-01-30

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 8 | ((181112-odklatwianie-ateny; 190112-eksperymenty-na-wilach; 190116-wypalenie-saitaera-z-trzesawiska; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190519-uciekajacy-seksbot; 190804-niespodziewany-wplyw-aidy; 201011-narodziny-paladynki-saitaera)) |
| Minerwa Metalia      | 3 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 201011-narodziny-paladynki-saitaera)) |
| Alan Bartozol        | 2 | ((190112-eksperymenty-na-wilach; 190116-wypalenie-saitaera-z-trzesawiska)) |
| Gabriel Ursus        | 2 | ((190804-niespodziewany-wplyw-aidy; 201011-narodziny-paladynki-saitaera)) |
| Karolina Erenit      | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Ossidia Saitis       | 2 | ((190519-uciekajacy-seksbot; 190804-niespodziewany-wplyw-aidy)) |
| Sławomir Muczarek    | 2 | ((190112-eksperymenty-na-wilach; 190202-czarodziejka-z-woli-saitaera)) |
| Tymon Grubosz        | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Wiktor Satarail      | 2 | ((190116-wypalenie-saitaera-z-trzesawiska; 190127-ixionski-transorganik)) |
| Wojtek Kurczynos     | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Adela Kirys          | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
| Agaton Ociegor       | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Aleksander Muniakiewicz | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Amadeusz Sowiński    | 1 | ((181112-odklatwianie-ateny)) |
| Amanda Kajrat        | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Arianna Verlen       | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Arnulf Poważny       | 1 | ((190519-uciekajacy-seksbot)) |
| ASD Centurion        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Atena Sowińska       | 1 | ((181112-odklatwianie-ateny)) |
| Cezary Zwierz        | 1 | ((181112-odklatwianie-ateny)) |
| Elena Verlen         | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Eliza Ira            | 1 | ((190519-uciekajacy-seksbot)) |
| Ernest Kajrat        | 1 | ((190519-uciekajacy-seksbot)) |
| Erwin Galilien       | 1 | ((190127-ixionski-transorganik)) |
| Eustachy Korkoran    | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Hieronim Maus        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Izabela Zarantel     | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Julia Morwisz        | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Kamil Lyraczek       | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Karradrael           | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Kasjopea Maus        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Kirył Najłalmin      | 1 | ((190127-ixionski-transorganik)) |
| Klaudia Stryk        | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Kornel Garn          | 1 | ((190112-eksperymenty-na-wilach)) |
| Liliana Bankierz     | 1 | ((190519-uciekajacy-seksbot)) |
| Lucjusz Blakenbauer  | 1 | ((181112-odklatwianie-ateny)) |
| Lutus Amerin         | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Maria Naavas         | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Minerwa Diakon       | 1 | ((181112-odklatwianie-ateny)) |
| Napoleon Bankierz    | 1 | ((190127-ixionski-transorganik)) |
| Natalia Aradin       | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Sabina Kazitan       | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Teresa Mieralit      | 1 | ((190519-uciekajacy-seksbot)) |
| Tomasz Tukan         | 1 | ((190519-uciekajacy-seksbot)) |