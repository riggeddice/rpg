# Daria Czarnewik
## Identyfikator

Id: 2206-daria-czarnewik

## Sekcja Opowieści

### Gdy HR reedukuje niewłaściwą osobę

* **uid:** 230111-gdy-hr-reedukuje-niewlasciwa-osobe, _numer względny_: 15
* **daty:** 0100-11-13 - 0100-11-16
* **obecni:** Adragain Ferrias, AK Nox Ignis, Aleksy Sartaran, Arianna Verlen, Daria Czarnewik, Elena Verlen, Grażyna Burgacz, Kajetan Kircznik, Lana Mirkinin, Leszek Kurzmin, Maja Samszar, Miłosz Klinek, OO Astralna Flara, OO Athamarein, OO Loricatus, Persefona d'Loricatus, Sabrina Ferrias, Salazar Bolza, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Niestety, Kircznik (lekarz) przestraszył Sargona i młodych infiltratorów Lodowcem, Bolzą i ogólnie duchami noktiańskimi. Okazało się, że Grażyna jest ekspertem od duchów i jest w stanie zrobić egzorcyzm. Niedługo potem (i chwilę przed egzorcyzmem) pojawiła się szybka korweta z Biur HR i poprosili o pomoc w odparciu Anomalii Kosmicznej. Myrczek sprawdził jej pamięć i Athamarein i Flara poleciały na starcie z Nox Ignis. Nawiązały kontakt z Biurami HR, przechwyciły biednego męża pilotki Nox Ignis i poznały prawdę o Destructor Animarum i wpływie Syndykatu Aureliona na Biura HR. Kurzmin pobił rekordy Athamarein, Arianna skutecznie zaplanowała wypalenie pilotki Nox Ignis przez własny statek. Tylko advancer Miłosz (badający ślady Nox Ignis) jest MIA.

Aktor w Opowieści:

* Dokonanie:
    * lokalny ekspert od Biur HR; wie co to jest. Skutecznie ochroniła Flarę i Athamarein przed integracją psychotroniczną z Nox Ignis budując odpowiednie ekrany z pomocą Mai.


### Astralna Flara i nowy komodor

* **uid:** 221221-astralna-flara-i-nowy-komodor, _numer względny_: 14
* **daty:** 0100-11-07 - 0100-11-10
* **obecni:** Arianna Verlen, Daria Czarnewik, Elena Verlen, Ellarina Samarintael, Kajetan Kircznik, Kaspian Certisarius, Leszek Kurzmin, Maja Samszar, NekroTAI Zarralea, OO Astralna Flara, OO Athamarein, OO Loricatus, Salazar Bolza, Sargon Niiris, Władawiec Diakon

Streszczenie:

Po "uratowaniu" noktian z Dominy Lucis Arianna dostała medal a Lodowca zastąpił komodor Bolza, zimny drakolita. Gdy grupa wydzielona wróciła na teren Anomalii Kolapsu okazało się, że planetoida Kazmirian ma squatterów; Zespół skutecznie ich przejął odzyskując kontrolę nad systemami Semli d'Kazmirian. Na miejscu - TKO-4271 - okazało się że Zarralea jest zniszczona. Orbiter odzyskał swoją bazę, acz Loricatis musi współdzielić Persefonę. Z innej beczki: link Elena - Władawiec się pięknie rozwija.

Aktor w Opowieści:

* Dokonanie:
    * ostrzegła Ariannę o tym, że najpewniej squatterzy się pojawili w Kazmirian; pomogła opracować plan odstraszania bezkrwawego i sojuszu ze squatterami. Sprzedała to jako 'Orbitera sie nie okrada'. Wstawiła się za Sargonem u Arianny.


### Astralna Flara kontra Domina Lucis

* **uid:** 221214-astralna-flara-kontra-domina-lucis, _numer względny_: 13
* **daty:** 0100-10-09 - 0100-10-11
* **obecni:** Arianna Verlen, Axel Nargan, Daria Czarnewik, Ellarina Samarintael, Gabriel Lodowiec, Kirea Rialirat, Leszek Kurzmin, NekroTAI Zarralea, OO Astralna Flara, OO Athamarein, Sarian Xadaar, Tristan Rialirat

Streszczenie:

Lodowiec dostał rozkazy pojmać / zabić dowódcę Dominy Lucis. Użył nekroTAI by odebrać resztkę nadziei noktianom (you will serve alive or dead) i jakkolwiek sporo noktian się poddało, główni popełnili samobójstwo. Orbiter zajął Dominę Lucis i Strefę Duchów, acz kosztem reputacji. Ogromny, bezkrwawy sukces wzmacniający Pax Orbiter dzięki propagandzie i Zarralei.

Aktor w Opowieści:

* Dokonanie:
    * wraz z Ellariną opracowuje sygnał mający za zadanie przekonać noktian do poddania się. Retransmituje sygnał z Orbitera o NekroTAI i wielką mowę Lodowca by Nonarion miał dowody jak groźny i bezwzględny i niemoralny jest Orbiter a zwłaszcza Lodowiec.


### Astralna Flara w strefie duchów

* **uid:** 221130-astralna-flara-w-strefie-duchow, _numer względny_: 12
* **daty:** 0100-10-05 - 0100-10-08
* **obecni:** Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Elena Verlen, Ellarina Samarintael, Gabriel Lodowiec, Hubert Kerwelenios, Kajetan Kircznik, Kirea Rialirat, Klarysa Jirnik, Leo Kasztop, Maja Samszar, NekroTAI Zarralea, OO Astralna Flara, OO Athamarein, Szymon Wanad, Tomasz Dojnicz

Streszczenie:

Planetoida Kazmirian została przejęta przez Orbiter. Daria ostrzegła Nonarion (Leo), że Orbiter nadal poluje na TAI i zdobyła informację o 'strefie duchów'. Z Mają doszły do tego, że 'strefa duchów' jest świetnym maskowaniem; Lodowiec wyprowadził tam Orbiter a Arianna stworzyła nekroTAI z Zarralei d'Isigtand. Używając advancera przechwycili samotną savarankę (której Lodowiec nie umie przesłuchać) i stanęło na tym, że 'strefa duchów' jest bazą noktiańską zbudowaną dookoła jednostki wsparcia, 'Domina Lucis'. Lodowiec chce doprowadzić noktian do sprawiedliwości Orbitera, Arianna i Daria są skonfliktowane. Mają jeńca - Kireę, lokalną młodą savarankę.

Aktor w Opowieści:

* Dokonanie:
    * ostrzegła Leo przed tym, że Orbiter COŚ robi z tymi neikatiańskimi TAI. Pozyskała dane o Strefie Duchów. Próbowała reanimować TAI Zarraleę z Isigtand; niestety, nie udało jej się - zbyt zniszczona (bardzo wbrew sobie, nie pozwoli na jej Ograniczenie). Przerażona powstaniem nekroTAI.
* Progresja:
    * Arianna wie, że Daria się starała w odbudowie Zarralei. Ale Lodowiec ma podejrzenia, że Daria go sabotowała.


### Egzotyczna Piękność na Astralnej Flarze

* **uid:** 221123-egzotyczna-pieknosc-na-astralnej-flarze, _numer względny_: 11
* **daty:** 0100-09-12 - 0100-09-15
* **obecni:** Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Ellarina Samarintael, Erwin Pies, Frank Mgrot, Gabriel Lodowiec, Gerwazy Kircznik, Hind Szug Traf, Kajetan Kircznik, Leszek Kurzmin, Marcel Kulgard, OO Astralna Flara, OO Athamarein, SCA Hadiah Emas, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Flara naprawia Hadiah Emas, a po drodze komodor Lodowiec rozplątuje wszystkie wątki - niewolnik z Orbitera? (okazał się dezerterem), autonomiczna TAI Mirtaela d'Hadiah Emas? (zostaje Ograniczona przez psychotronika Athamarein) itp. Arianna zarządza Flarą, wprowadzając Ellarinę jako maskotkę od morale i stabilizując wszystko by jakoś zespół dopasował się do realiów Nonariona. To nie jest ani Aurum ani Orbiter, to coś zupełnie innego.

Aktor w Opowieści:

* Dokonanie:
    * przeprowadziła plan i procedurę naprawy Hadiah Emas tym co miała; duży koszt surowcowy, ale dobra okazja do pomocy. ABSOLUTNIE nie zgadza się na skrzywdzenie Mirtaeli i próbowała przekonać pośrednio komodora, by nie Ograniczył Mirtaeli. Przyjaciółka Ellariny, pomogła ją zintegrować z Astralną Flarą.


### Astralna Flara dociera do Nonariona Nadziei

* **uid:** 221116-astralna-flara-dociera-do-nonariona-nadziei, _numer względny_: 10
* **daty:** 0100-09-08 - 0100-09-11
* **obecni:** Alan Nierkamin, Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Gabriel Lodowiec, Grażyna Burgacz, Kajetan Kircznik, Leo Kasztop, Leszek Kurzmin, Maja Samszar, Marcel Kulgard, OO Astralna Flara, OO Athamarein, SCA Hadiah Emas, SCA Isigtand, Szymon Wanad, Tomasz Ruppok, Władawiec Diakon

Streszczenie:

Flara dociera do Nonariona, który wygląda jak absolutny śmieć. Morale na Flarze ucierpiało - aż do plotek o Egzotycznych Pięknościach, co sprawiło Ariannie kolejne problemy. Alan, eks-lokals powiedział Darii że jest jakaś forma standaryzacji na Nonarionie - coś tam jest, coś "bogatego" jako frakcja. Daria zdobyła od Leo planetoidę którą może użyć jako bazę dla Orbitera, ale w drodze tam zboczyli by ratować uszkodzone statki lokalne. I uratowali jednostkę niewolniczą i zniszczony grazer. Komodor Lodowiec - sensowny koleś - ma dylemat moralny. Neikatiańska TAI oraz niewolnicy.

Aktor w Opowieści:

* Dokonanie:
    * wyczaiła, że agent Lodowca Alan może i był lokalsem ale jest zbyt pro-Orbiterowy; pozyskała doskonałą planetoidę od Leo i wyjaśniła jak działa stacja Nonarion; wykorzystała manewr Flary by rozpuścić wodę i odwrócić uwagę anomalnych asteroid od Athamarein.


### Astralna Flara i porwanie na Karsztarinie

* **uid:** 221102-astralna-flara-i-porwanie-na-karsztarinie, _numer względny_: 9
* **daty:** 0100-07-28 - 0100-08-03
* **obecni:** Ada Wyrocznik, Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Elena Verlen, Gerwazy Kircznik, Grażyna Burgacz, Hubert Kerwelenios, Kajetan Kircznik, Klarysa Jirnik, Maja Samszar, Mariusz Bulterier, OO Karsztarin, OO Optymistyczny Żuk, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Arianna otrzymała nowy okręt - jednostkę wsparcia Astralna Flara. Jako pilota dostała Elenę. Elena ma problemy z aklimatyzacją na Orbiterze; chce być najlepsza i walczy o to. Nie rozmawia z ludźmi. Arianna zdecydowała się poprosić Alezję o pomoc w tej dziedzinie. Tymczasem przy ćwiczeniach na Karsztarinie okazało się, że Flara jest tam w czasie porwania - Zespołowi Flary udało się zatrzymać porwanie, choć operacja była niesamowicie niebezpieczna - niedoświadczony Zespół vs 4 komandosów Syndykatu Aureliona.

Aktor w Opowieści:

* Dokonanie:
    * zaproponowała Ariannie inne blueprinty do Astralnej Flary; cywile nie chodzą na sprzęcie Orbitera. Po odkryciu, że Elena generuje ogromne Skażenie jako pilot rekalkulowała Flarę by zwiększyć jej wydajność. Skutecznie włączyła ograniczniki gdy Elena przekroczyła parametry lotu. Potwierdziła próbę porwania z nazwisk.


### Kapitan Verlen i koniec przygody na Królowej

* **uid:** 221026-kapitan-verlen-i-koniec-przygody-na-krolowej, _numer względny_: 8
* **daty:** 0100-06-08 - 0100-06-15
* **obecni:** Antoni Kramer, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Klaudiusz Terienak, Leszek Kurzmin, OO Królowa Kosmicznej Chwały, OO Tucznik Trzeci, Stefan Torkil, Tomasz Ruppok

Streszczenie:

Po długiej i ciężkiej pracy Arianna i Daria doprowadziły Królową do 50% sprawności nominalnej. Radość i wysokie morale załogi. Gdy Arnulf przyszedł do Arianny mówiąc o fabrykacji narkotyków i alkoholu, ona kazała to 'zatruć'. Zmotywowała Ruppoka, by dał jej szansę i znalazła sposób jak częściowo odzyskać materiały do fabrykatora po rozmowie z Arnulfem - sojusz z bliską Anomalii Kolapsu stacją Nonarion; poprosiła Kurzmina by to załatwił. TAK PRZESTRASZYŁA siły specjalne Orbitera, że Ariannę i załogę przenieśli bliżej Nonariona by sabotować program kosmiczny Aurum. Koniec przygody na Królowej :-).

Aktor w Opowieści:

* Dokonanie:
    * uruchomiła statek do pełnego działania. Po długiej pracy Królowa jest kontrolowana i będzie działać. Więc - przesuną ją gdzieś indziej XD.
* Progresja:
    * przeniesiona z Królowej Kosmicznej Chwały na dużo lepszą jednostkę, Astralną Flarę.


### Kapitan Verlen i pierwszy ruch statku

* **uid:** 221019-kapitan-verlen-i-pierwszy-ruch-statku, _numer względny_: 7
* **daty:** 0100-05-23 - 0100-06-04
* **obecni:** Arianna Verlen, Daria Czarnewik, Erwin Pies, Maja Samszar, Marcelina Trzęsiel, OO Królowa Kosmicznej Chwały, Romeo Verlen, Rufus Warkoczyk, Stefan Torkil

Streszczenie:

Arianna włączyła ćwiczenia dla Królowej - prawie wyszło, ale ludzie popanikowali a statek odmówił posłuszeństwa i załoga zaczęła się bić. Arianna ich przećwiczyła w działaniach awaryjnych i oddała panikarza medykowi - niestety, Paradoks zmienił Marcela w Marcelinę... ale następnym razem ćwiczenia powinny się już udać. Daria ma sprawę pod kontrolą i Arianna też raczej jest pewna sukcesu.

Aktor w Opowieści:

* Dokonanie:
    * wprawnie zrestartowała Królową gdy pojawiła się awaria i przeszła na manualne. Królowa jest w podłym stanie, ale Daria krok po kroku ją doprowadza do działania.


### Kapitan Verlen i niezapowiedziana inspekcja

* **uid:** 221012-kapitan-verlen-i-niezapowiedziana-inspekcja, _numer względny_: 6
* **daty:** 0100-05-17 - 0100-05-21
* **obecni:** Adam Chrząszczewicz, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Hubert Kerwelenios, Leona Astrienko, Maja Samszar, Mariusz Bulterier, OO Królowa Kosmicznej Chwały, Szczepan Myrczek, Tomasz Dojnicz, Władawiec Diakon

Streszczenie:

Podczas skanu kadłuba Królowej advancerzy z Darią wykryli scrambler TAI, destabilizator silników itp. Arianna stoczyła bitwę ćwiczebną z marine, ale pojawiła się inspekcja (która nie była zapowiedziana bo Maja nie przekazała informacji). Daria reanimowała TAI, wszystko częściowo poszło nie tak. Ale o dziwo audytor chciał pomóc Ariannie, która stanęła między Orbiterem (wsparcie) i Aurum (my sami sobie poradzimy). Arianna upewniła się, że Władawiec nie stał za operacją - najpewniej Maja. Ale Królowa jest w lepszym stanie niż kiedykolwiek, tylko, że Arianna je konserwy (by nie wpaść pod feromony itp)

Aktor w Opowieści:

* Dokonanie:
    * wykryła scrambler TAI i inne rzeczy na powierzchni Królowej, potem wykorzystała to jako okazję do ćwiczeń. Reanimowała Semlę. Odzyskuje inżynieryjną kontrolę nad statkiem.


### Kapitan Verlen i pojedynek z marine

* **uid:** 220928-kapitan-verlen-i-pojedynek-z-marine, _numer względny_: 5
* **daty:** 0100-05-14 - 0100-05-16
* **obecni:** Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Grażyna Burgacz, Leona Astrienko, Leszek Kurzmin, Maja Samszar, OO Królowa Kosmicznej Chwały, Stefan Torkil, Szymon Wanad, Tomasz Ruppok, Władawiec Diakon

Streszczenie:

Królowa miała się przesunąć, ale prawie się rozbiła; ta jednostka jest rozkradziona. Arianna zatrzymała biczowanie załogantów i doszła do tego, że załoga jest niemrawa i skonfliktowana bo część ludzi jest z Aurum a część nie. Perfect storm of suck. By zatrzymać Wanada przed "wygraniem cnoty koleżanki inżyniera" stoczyła z nim pojedynek i wygrała. Leona ostrzegła, by Arianna nic nie jadła i nie piła. Czyli coś jest w jedzeniu. To tłumaczy czemu Królowa jest tak zdegenerowaną jednostką. Aha, Arianna ma wsparcie kapitana Leszka Kurzmina, dawnego rywala Alezji.

Aktor w Opowieści:

* Dokonanie:
    * uruchomiła silniki Królowej i Królowa wyszła poza kontrolę; z trudem odzyskała kontrolę nad jednostką. CO TU SIĘ DZIEJE?!


### Kapitan Verlen i Królowa Kosmicznej Chwały

* **uid:** 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly, _numer względny_: 4
* **daty:** 0100-05-06 - 0100-05-12
* **obecni:** Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Grażyna Burgacz, Klarysa Jirnik, Klaudiusz Terienak, Leona Astrienko, Maja Samszar, OO Królowa Kosmicznej Chwały, Szymon Wanad, Władawiec Diakon

Streszczenie:

Królowa Kosmicznej Chwały to najbardziej dysfunkcyjna jednostka pod kontrolą Orbitera - służy do łamania karier obiecujących oficerów i sabotażu programu kosmicznego Aurum (oba z woli sił specjalnych Aurum). Trafiła na królową Arianna i zaczęła robić porządek. Poprzednią panią kapitan zdecydowała się odratować, współpracując z Darią z inżynierii rozmontować wyścigi psów i stwierdziła, że doprowadzi do tego, by Królowa zaczęła skutecznie działać podczas ćwiczeń. Aha, tu poznała się z Leoną Astrienko ;-).

Aktor w Opowieści:

* Dokonanie:
    * w cywilnej części Kontrolera Pierwszego, przydzielona na Królową jako chief engineer. Przejęła kontrolę nad inżynierią, natychmiast kazała odbudować to co ważne, podjęła niepopularne decyzje (m.in. rozmontowanie toru do wyścigu psów).


### Ailira, niezależna handlarka wodą

* **uid:** 221113-ailira-niezalezna-handlarka-woda, _numer względny_: 3
* **daty:** 0090-06-22 - 0090-06-28
* **obecni:** Ailira Niiris, Daria Czarnewik, Filip Szukurkor, Iga Mikikot, Jakub Uprzężnik, Julia Karnit, Kaspian Certisarius, Leo Kasztop, Ludwik Trójkadur, Nastia Barbatov, Patryk Lapszyn, Safira d'Hiyori

Streszczenie:

Hiyori kończą się pieniądze. Nastia i Jakub biorą robotę najemniczą u byłego eks Nastii. Kaspian jest P.O. Daria składa Hiyori z różnych dostawców i możliwie tanich a dobrych komponentów. Na Kaspiana poluje niejaka Julia uważając go za komandosa advancera mordercę. Okazuje się też, że z 6 statków co miały wodę Ailiry 4 nie wróciły. Ailira została pobita, straciła grazera i sama sprzedała się sarderytom na części. Gdy Kaspian o tym usłyszał, absolutnie zakazał Hiyori coś z tym robić. Czekamy na Ogdena i Nastię. Jednak stacja Nonarion to zło.

Aktor w Opowieści:

* Dokonanie:
    * wrobiła Kaspiana w dowodzenie załogą w zastępstwie Nastii. Złożyła z serii dostawców najtańszy akceptowalny model Hiyori. Dużo zajmowała się rzeczami technicznymi i szpiegowała Kaspiana. Iga za nią negocjowała.


### Niebezpieczna woda na Hiyori

* **uid:** 221111-niebezpieczna-woda-na-hiyori, _numer względny_: 2
* **daty:** 0090-06-17 - 0090-06-21
* **obecni:** Ailira Niiris, Daria Czarnewik, Iga Mikikot, Jakub Uprzężnik, Kaspian Certisarius, Leo Kasztop, Nastia Barbatov, Ogden Barbatov, Patryk Lapszyn, Safira d'Hiyori, SCA Hiyori

Streszczenie:

Załoga Hiyori kupiła lokalizację nowej jednostki w Anomalii na salvage. Jednak podczas lotu okazało się, że zakupiona woda (od stałego dostawcy) jest niebezpieczna; ma bioformy reagujące z energią. Hiyori pozbyła się wody, wysłała sygnał SOS i zamknęli wodę w pojemnikach biocontain. Ogden poleciał do Orbitera użyć starych kontaktów i sprzedać a Nastia skonfrontowała się ze sprzedawcą wody. Leo (sprzedawca informacji) ich ostrzegł, żeby nie brali tanich pożyczek - coś się dzieje na Nonarionie.

Aktor w Opowieści:

* Dokonanie:
    * wykryła bawiąc się zabawkami do detekcji BIO 'węgorze' w wodzie, natychmiast zrobiła alarm i nadzorowała operację pozbycia się niebezpiecznej wody.


### Derelict Okarantis: wejście

* **uid:** 221022-derelict-okarantis-wejscie, _numer względny_: 1
* **daty:** 0090-03-03 - 0090-03-14
* **obecni:** Daria Czarnewik, Iga Mikikot, Jakub Uprzężnik, Kaspian Certisarius, Leo Kasztop, Nastia Barbatov, Ogden Barbatov, Patryk Lapszyn, Safira d'Hiyori, SCA Hiyori

Streszczenie:

SCA Salvager Hiyori wlatuje do Anomalii Kolapsu, by dostać się do nieznanego derelicta Okarantis. Podczas operacji salvagowania główny advancer został ranny i pojawiła się konieczność rozbicia Okarantis na dwie części - anomalną i "bezpieczną". Zespół ma wejście na bezpieczną część Okarantis i może rozpocząć salvagowanie.

Aktor w Opowieści:

* Dokonanie:
    * 23 lata, inżynier na salvagerze; analizowała dokładnie derelict Okarantis, opracowała nieudaną intruzję a potem udaną intruzję. Wbiła się do manifestu cargo Okarantis, potem określiła serię eksplozji na fault lines.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 15, @: 0100-11-16
    1. Primus    : 15, @: 0100-11-16
        1. Sektor Astoriański    : 15, @: 0100-11-16
            1. Libracja Lirańska    : 9, @: 0100-11-16
                1. Anomalia Kolapsu, orbita    : 9, @: 0100-11-16
                    1. Planetoida Kazmirian    : 1, @: 0100-09-11
                    1. SC Nonarion Nadziei    : 4, @: 0100-09-11
                        1. Moduł ExpanLuminis    : 2, @: 0100-09-11
                        1. Moduł Remedianin    : 1, @: 0090-06-28
                    1. Strefa Biur HR    : 1, @: 0100-11-16
                    1. Strefa Upiorów Orbitera    : 4, @: 0100-11-16
                        1. Planetoida Kazmirian    : 2, @: 0100-11-10
                        1. Planetoida Lodowca    : 4, @: 0100-11-16
                1. Anomalia Kolapsu    : 1, @: 0090-03-14
                    1. Cmentarzysko Statków    : 1, @: 0090-03-14

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 12 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Maja Samszar         | 9 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Arnulf Perikas       | 8 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow)) |
| Władawiec Diakon     | 8 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leszek Kurzmin       | 7 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kajetan Kircznik     | 6 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Astralna Flara    | 6 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 6 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Erwin Pies           | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Grażyna Burgacz      | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leo Kasztop          | 5 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| OO Królowa Kosmicznej Chwały | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Alezja Dumorin       | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Elena Verlen         | 4 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Ellarina Samarintael | 4 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Gabriel Lodowiec     | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Kaspian Certisarius  | 4 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda; 221221-astralna-flara-i-nowy-komodor)) |
| Szczepan Myrczek     | 4 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Szymon Wanad         | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Hubert Kerwelenios   | 3 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Iga Mikikot          | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Jakub Uprzężnik      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Klarysa Jirnik       | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Leona Astrienko      | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Nastia Barbatov      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| NekroTAI Zarralea    | 3 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Patryk Lapszyn       | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Safira d'Hiyori      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Stefan Torkil        | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Tomasz Ruppok        | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Ailira Niiris        | 2 | ((221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Gerwazy Kircznik     | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Kirea Rialirat       | 2 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Klaudiusz Terienak   | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Marcel Kulgard       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Mariusz Bulterier    | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Ogden Barbatov       | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| OO Loricatus         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Salazar Bolza        | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| SCA Hadiah Emas      | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| SCA Hiyori           | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Tomasz Dojnicz       | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221130-astralna-flara-w-strefie-duchow)) |
| Ada Wyrocznik        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Adam Chrząszczewicz  | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Antoni Kramer        | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Axel Nargan          | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Filip Szukurkor      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Julia Karnit         | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Ludwik Trójkadur     | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Marcelina Trzęsiel   | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Karsztarin        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Tucznik Trzeci    | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Romeo Verlen         | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Rufus Warkoczyk      | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Sarian Xadaar        | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Tristan Rialirat     | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |