# Klasa Hacker
## Identyfikator

Id: 9999-klasa-hacker

## Sekcja Opowieści

### Relikwia z androida?!

* **uid:** 240214-relikwia-z-androida, _numer względny_: 6
* **daty:** 0107-05-16 - 0107-05-18
* **obecni:** Aerina Cavalis, Alina Mekran, Elwira Barknis, Kalista Surilik, Klasa Dyplomata, Klasa Hacker, Klasa Inżynier, Klasa Oficer Naukowy, Klasa Sabotażysta, Mawir Hong, Tadeusz Mekran, Vanessa d'Cavalis

Streszczenie:

Zniknęły szczątki Vanessy - bohaterskiego androida, wydane trzem biologom. Niewyjaśnione polecenie wydania szczątków Vanessy i niespodziewane pojawienie się fanatyków walczących w jej imieniu na podziemnych arenach, zainfekowanych niemożliwymi do zrobienia na tej stacji hipnonarkotykami, wskazało na problem z anomaliami magicznymi. Okazało się, że przyczyną było morderstwo Aliny Mekran przez Elwirę Barknis, której plagiat i agresywne metody w pracy wyszły na jaw. Alina, próbująca wrócić do życia jako "resurrector" za pomocą roślin i hydroponiki, potrzebowała energii, krwi i ciała. Agencja była w stanie zamaskować sytuację, zniszczyć zainfekowany orbitujący dookoła Szernief hydrolab H4 i zabrali Vanessę jako influencerkę turystyczną poza Stację.

Aktor w Opowieści:

* Dokonanie:
    * obrona przed psychotronicznym atakiem, zabezpieczając systemy stacji przed przejęciem przez Anomalię BIA. Też: znalezienie pacjenta zero Szernief i danych z dokumentacji, co tu się naprawdę działo i jak daleki zasięg ma Elwira.


### Dla swych marzeń, warto!

* **uid:** 240117-dla-swych-marzen-warto, _numer względny_: 5
* **daty:** 0106-11-04 - 0106-11-06
* **obecni:** Aerina Cavalis, Artur Tavit, Estril Cavalis, Felina Amatanir, Kalista Surilik, Klasa Hacker, Klasa Oficer Naukowy, Klasa Sabotażysta, Larkus Talvinir, Maia Sakiran, Mawir Hong, Vanessa d'Cavalis

Streszczenie:

Na Stacji pojawił się pierścień Esuriit, który sprawia, że ludzie potrafią poświęcić energię do spełnienia obsesyjnych marzeń kosztem życia tych ludzi. Za pierścieniem przybył Szmaciarz, próbujący dodać pierścień do swojej kolekcji artefaktów. Zespołowi udało się zatrzymać Skażenie i uratować członka rady - stracili jednak Vanessę (biosyntkę) oraz Kalista została Aktywowana. Za to zespół zintegrował biosynty z resztą stacji.

Aktor w Opowieści:

* Dokonanie:
    * poza standardową integracją strumieni danych, przekonała Kalistę do współpracy i containowała niebezpieczny Pierścień


### Sen chroniący kochanków

* **uid:** 231122-sen-chroniacy-kochankow, _numer względny_: 4
* **daty:** 0105-09-02 - 0105-09-05
* **obecni:** Damian Orczakin, Dorion Fughar, Felina Amatanir, Jola-09 Szernief, Kalista Surilik, Klasa Dyplomata, Klasa Hacker, Klasa Oficer Naukowy, Klaudiusz Widar, Mawir Hong, Rovis Skarun, Szymon Alifajrin

Streszczenie:

Na stacji CON Szernief konflikt między savaranami a drakolitami, podsycany przez inwestorów i stymulanty, prowadzi do zagrożenia pryzmatycznego. Dochodzi do manifestacji Esuriit i Alucis. Punktem kluczowym okazała się miłość Rovisa i Joli-09, prowadząca do rozprzestrzeniającego się Snu Ukojenia zwalczającego i zwalczanego przez Nienawiść Esuriit. Lux Umbrarum interweniuje, tworząc antidotum i przesiedlając kochanków. Kryzys został rozwiązany i stacja uspokojona tymczasowo, ale Kalista jest na tropie 'dziwnych rzeczy' a zaufanie populacji stacji do Inwestorów i Rady spada.

Aktor w Opowieści:

* Dokonanie:
    * Mery; zebrała informacje o przyczynie problemów (inwestorzy), wbiła się w dane Kalisty odkrywając jej hipotezy i potrafiła prześledzić ruchy Rovisa jak i nagrania z walki gdy savaranie odbijali Jolę-09.


### Polowanie na biosynty na Szernief

* **uid:** 231213-polowanie-na-biosynty-na-szernief, _numer względny_: 3
* **daty:** 0105-07-26 - 0105-07-31
* **obecni:** Artur Tavit, Delgado Vitriol, Felina Amatanir, Kalista Surilik, Klasa Biurokrata, Klasa Dyplomata, Klasa Hacker, Klasa Oficer Naukowy, Klasa Sabotażysta, Malik Darien, Sebastian-194, Vanessa d'Cavalis

Streszczenie:

Kalista wezwała Agencję bo biosynty są niszczone (ludzie zabijają je pierwsi) i dzieje się 'coś dziwnego' a 'człowiek Agencji' jest niekompetentny - okazało się, że jest Skażony Unumens (ma pasożyta). Agencja zrzuciła 'legalność' biosyntów na arystokratę Artura, zapewnia sobie współpracę z biosyntami i kieruje je do zniszczenia Parasekta Unumens. By odwrócić uwagę od swoich działań - zdradza Kalistę i Mawira (i wszystkie problemy zrzuca na nich). Udało się jednak większość uratować, mimo śmierci kilku osób w tak trudnej sytuacji.

Aktor w Opowieści:

* Dokonanie:
    * zestawienie połączenia zdalnego z Malikiem, odkrycie danych z kamer: to savaranie atakowali biosynty pierwsi, śledzenie WSZYSTKICH ludzi by z szokiem dojść do tego kto jest zarażony, budowa fałszywych dowodów (że Agencja jest niewinna).


### Protest przed kultem na Szernief

* **uid:** 231202-protest-przed-kultem-na-szernief, _numer względny_: 2
* **daty:** 0103-10-15 - 0103-10-19
* **obecni:** Aerina Cavalis, Frederico Zyklas, Kalista Surilik, Klasa Biurokrata, Klasa Hacker, OLU Luminarius, Vanessa d'Cavalis

Streszczenie:

Seria tajemniczych morderstw arystokratów na Szernief. Pierwszym sprawcą był Frederico, inżynier, Agencja odkrywa, że morderstwa wydają się być spowodowane przez anomalię Esuriit, która wpływa na zachowania ofiar. Kluczem do prawdy było przesłuchanie Frederico (kiedyś kultysty) - jego żona i nienarodzone dziecko zginęły podczas protestu arystokratów. Okazało się, że jego żona próbuje inkarnować się w Aerinie. Rozwiązaniem było rozlokowanie arystokratów poza stację i wyleczenie Aeriny, zabierając ją do Agencji.

Aktor w Opowieści:

* Dokonanie:
    * włamała się do danych Kalisty i znalazła z jej pomocą dane o arystokratach, zidentyfikowała anomalne zachowania Frederico i przygotowała możliwość wartościowej ewakuacji arystokratów (by rozproszyć anomalię przez redukcję masy krytycznej energii).


### Złomowanie legendarnej Anitikaplan

* **uid:** 240111-zlomowanie-legendarnej-anitikaplan, _numer względny_: 1
* **daty:** 0084-04-02 - 0084-04-06
* **obecni:** Klasa Biurokrata, Klasa Hacker, Klasa Oficer Naukowy, Klasa Sabotażysta, SN Anitikaplan, SN Varilen, Twaróg Worl, Wiktor Worl

Streszczenie:

Anitikaplan – legendarna jednostka od bajek z Tygryskiem została zniszczona podczas Wojny Deoriańskiej przez grupę fanatyków. Gdy zostały kupione prawa do zezłomowania Anitikaplan i gdy zaczęły dziać się anomalne rzeczy na pokładzie, Luminarius przybył, by usunąć anomalię. Anitikaplan próbowała przywrócić przeszłość do teraźniejszości; Sempitus i Alteris wspólnie kontynuowały zarówno Wojnę Deoriańską jak i ‘podaruj wszystkim dzieciom radość’. Agencji – mimo dużych strat w załodze – udało się wyczyścić Anitikaplan i zachować sekret magii przed populacją, zgodnie z misją.

Aktor w Opowieści:

* Dokonanie:
    * skutecznie połączył się z kapsułą ratunkową (i pośrednio z umysłem Pereza i Anitikaplan); potem zagregował dane ze wszystkich serwopancerzy i danych statku tworząc superstrumień.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 6, @: 0107-05-18
    1. Primus    : 6, @: 0107-05-18
        1. Sektor Kalmantis    : 6, @: 0107-05-18
            1. Sebirialis, orbita    : 5, @: 0107-05-18
                1. CON Szernief    : 5, @: 0107-05-18
                    1. Orbitujące stacje hydroponiczne    : 1, @: 0107-05-18
                    1. Powłoka Wewnętrzna    : 2, @: 0105-09-05
                        1. Poziom Minus Dwa    : 2, @: 0105-09-05
                            1. Więzienie    : 2, @: 0105-09-05
                        1. Poziom Minus Jeden    : 2, @: 0105-09-05
                            1. Obszar Mieszkalny Savaran    : 2, @: 0105-09-05
                        1. Poziom Minus Trzy    : 2, @: 0105-09-05
                            1. Wielkie Obrady    : 2, @: 0105-09-05
                    1. Powłoka Zewnętrzna    : 1, @: 0105-09-05
                        1. Panele Słoneczne    : 1, @: 0105-09-05

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kalista Surilik      | 5 | ((231122-sen-chroniacy-kochankow; 231202-protest-przed-kultem-na-szernief; 231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Oficer Naukowy | 5 | ((231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 240111-zlomowanie-legendarnej-anitikaplan; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Sabotażysta    | 4 | ((231213-polowanie-na-biosynty-na-szernief; 240111-zlomowanie-legendarnej-anitikaplan; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Vanessa d'Cavalis    | 4 | ((231202-protest-przed-kultem-na-szernief; 231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Aerina Cavalis       | 3 | ((231202-protest-przed-kultem-na-szernief; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Felina Amatanir      | 3 | ((231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Klasa Biurokrata     | 3 | ((231202-protest-przed-kultem-na-szernief; 231213-polowanie-na-biosynty-na-szernief; 240111-zlomowanie-legendarnej-anitikaplan)) |
| Klasa Dyplomata      | 3 | ((231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 240214-relikwia-z-androida)) |
| Mawir Hong           | 3 | ((231122-sen-chroniacy-kochankow; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Artur Tavit          | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Alina Mekran         | 1 | ((240214-relikwia-z-androida)) |
| Damian Orczakin      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Delgado Vitriol      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Dorion Fughar        | 1 | ((231122-sen-chroniacy-kochankow)) |
| Elwira Barknis       | 1 | ((240214-relikwia-z-androida)) |
| Estril Cavalis       | 1 | ((240117-dla-swych-marzen-warto)) |
| Frederico Zyklas     | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Jola-09 Szernief     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Klasa Inżynier       | 1 | ((240214-relikwia-z-androida)) |
| Klaudiusz Widar      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Larkus Talvinir      | 1 | ((240117-dla-swych-marzen-warto)) |
| Maia Sakiran         | 1 | ((240117-dla-swych-marzen-warto)) |
| Malik Darien         | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| OLU Luminarius       | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Rovis Skarun         | 1 | ((231122-sen-chroniacy-kochankow)) |
| Sebastian-194        | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| SN Anitikaplan       | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| SN Varilen           | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Szymon Alifajrin     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Tadeusz Mekran       | 1 | ((240214-relikwia-z-androida)) |
| Twaróg Worl          | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Wiktor Worl          | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |