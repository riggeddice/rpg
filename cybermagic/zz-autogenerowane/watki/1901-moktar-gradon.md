# Moktar Gradon
## Identyfikator

Id: 1901-moktar-gradon

## Sekcja Opowieści

### Odzyskana agentka Orbitera

* **uid:** 190724-odzyskana-agentka-orbitera, _numer względny_: 7
* **daty:** 0110-06-12 - 0110-06-15
* **obecni:** Adam Szarjan, Aida Serenit, Julia Morwisz, Mirela Niecień, Mirela Orion, Moktar Gradon, Pięknotka Diakon

Streszczenie:

Aidę porwała frakcja Cieniaszczytu zainspirowana przez Julię - by ratować Emulatorkę. Ale Aida, jak się okazało, jest nędznym cywilem i to takim uratowanym z kosmosu. Pięknotka wygrała z championem kralotha przez zanęcenie Moktara - a zwabiła go dając mu walkę z Mirelą. Ogólnie, wszyscy są zadowoleni..?

Aktor w Opowieści:

* Dokonanie:
    * champion Pięknotki do walki z kralotycznym championem przekupiony walką z Kirasjerką. Pokonał wszystko z czym walczył i się doskonale bawił.


### Adieu, Cieniaszczycie

* **uid:** 181227-adieu-cieniaszczycie, _numer względny_: 6
* **daty:** 0109-11-28 - 0109-12-01
* **obecni:** Amadeusz Sowiński, Atena Sowińska, Brygida Maczkowik, Erwin Galilien, Lilia Ursus, Mirela Niecień, Moktar Gradon, Pietro Dwarczan, Pięknotka Diakon, Waleria Cyklon, Zbigniew Burzycki

Streszczenie:

Pięknotka zdobyła nowe ciało dla Minerwy (acz do jego uruchomienia człowiek musi być poświęcony!), przekonała Amadeusza do założenia świątyni Arazille do blokowania Finis Vitae oraz wepchnęła Lilię jako asystentkę Atenie. Do tego rozkochała solidnie Erwina i zamknęła wszystkie wątki, by móc wracać do Pustogoru. Nie dała rady rozdzielić Julii i Łysych Psów, niestety.

Aktor w Opowieści:

* Dokonanie:
    * przekonał (?!) Pięknotkę, że jeśli walka ma się toczyć z Saitaerem, Pięknotka CHCE by Psy miały Julię na pełnej mocy.


### Finis Vitae

* **uid:** 181226-finis-vitae, _numer względny_: 5
* **daty:** 0109-11-18 - 0109-11-27
* **obecni:** Amadeusz Sowiński, Arazille, Atena Sowińska, Moktar Gradon, Pięknotka Diakon, Waleria Cyklon

Streszczenie:

Moktar zregenerował Pięknotkę i powiedział jej, że chce od niej uratowania swojego Skażonego oficera. Pięknotka dowiedziała się od Amadeusza o autowarze Finis Vitae pod Cieniaszczytem; Saitaer nie może nic wiedzieć. Pięknotka doprowadziła do tego, że Lilia wróciła do Cieniaszczytu (courtesy of Moktar), uratowała spod ziemi tego oficera, oraz zobaczyła miejsce, które będzie chyba do końca życia widziała w koszmarach. Gdyby nie Atena, zginęłaby na tej akcji.

Aktor w Opowieści:

* Dokonanie:
    * po zregenerowaniu Pięknotki dał jej zadanie uratowania Dariusza; co więcej, oddał jej życie Lilii i wyjaśnił problem podziemi.


### Czyszczenie toksycznych związków

* **uid:** 181225-czyszczenie-toksycznych-zwiazkow, _numer względny_: 4
* **daty:** 0109-11-13 - 0109-11-17
* **obecni:** Atena Sowińska, Bogdan Szerl, Julia Morwisz, Moktar Gradon, Pięknotka Diakon, Romuald Czurukin

Streszczenie:

Pięknotka ściągnęła Erwina na detoks od Saitaera, acz dała mu informację o Kreacjuszu Diakonie. Saitaer planuje zbudować sprawne ciało dla Minerwy. Atena jest zregenerowana w dość anielskiej formie i dostaje informacje o Orbiterze od Julii. Pięknotka próbuje naprawić pary Romuald x Julia oraz Wioletta x Pietro. Moktar uwalnia Pięknotkę od wpływu Bogdana, masakrując swojego agenta w brutalny sposób.

Aktor w Opowieści:

* Dokonanie:
    * wszedł do Colubrinus Meditech sprawdzić o co chodzi z Julią i uratował Pięknotkę od Bogdana. Przez MOMENT stracił kontrolę. Channelował Arazille i Saitaera.
* Progresja:
    * czuje do Pięknotki niechętny szacunek - wystarczająco godny przeciwnik.


### Tajemniczy ołtarz Moktara

* **uid:** 181218-tajemniczy-oltarz-moktara, _numer względny_: 3
* **daty:** 0109-11-11 - 0109-11-13
* **obecni:** Atena Sowińska, Bogdan Szerl, Moktar Gradon, Pięknotka Diakon, Waleria Cyklon

Streszczenie:

Pięknotka śniła o lustrach; w poszukiwaniu informacji co się dzieje dowiedziała się, że do wyczyszczenia Saitaera wykorzystana jest energia Arazille. O dziwo, Moktar kontroluje energię Arazille a przynajmniej używa jej jako broni. Pięknotka wróciła w ręce Bogdana i przekonała Atenę, by ta poddała się rekonstrukcji - by uwolniła się od Saitaera.

Aktor w Opowieści:

* Dokonanie:
    * upewnił się, że Pięknotki nie dotyczą wpływy Arazille. Wzmacnia Arazille, ale nie chce by ona miała gdzieś tu swój hold.


### Kajdan na Moktara

* **uid:** 181209-kajdan-na-moktara, _numer względny_: 2
* **daty:** 0109-11-02 - 0109-11-03
* **obecni:** Pietro Dwarczan, Pięknotka Diakon, Waleria Cyklon

Streszczenie:

Pięnotka próbowała osłonić się przed potencjalnym atakiem Moktara i poszła szukać jakiejś jego słabości - i spotkała się w pojedynku snajperów z Walerią Cyklon. Wygrała dzięki lepszemu sprzętowi (power suit), acz skończyła w złym stanie. Wycofała się i Walerię przed atakiem dziwnych nojrepów i wybudziła Walerię; dostała swój dowód, przeskanowała pamięć Walerii i uratowała bazę Moktara przed zniszczeniem przez te dziwne nojrepy. Ogólnie, sukces.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * ma ogromną wiedzę o dziwnych nojrepach i całej tej sprawie z Orbiterem Pierwszym
    * wie, że Pięknotka ma dowód mogący poważnie uszkodzić jak nie zniszczyć Łyse Psy. Musi działać przeciw niej ostrożniej.
    * Podzielił się z Pięknotką strefami wpływów; jak długo nie wchodzą sobie w drogę, Pięknotka jest bezpieczna.


### Swaty w cieniu potwora

* **uid:** 181125-swaty-w-cieniu-potwora, _numer względny_: 1
* **daty:** 0109-10-29 - 0109-11-01
* **obecni:** Lilia Ursus, Moktar Gradon, Pietro Dwarczan, Pięknotka Diakon, Romuald Czurukin, Waleria Cyklon, Wioletta Kalazar

Streszczenie:

Pięknotka bardzo chciała przespać się z Pietrem; niestety, spotkała Lilię na bazarze i dowiedziała się o dziwnych nojrepach z symbolem Orbitera. Lilia wyraźnie czuje się niedoceniana i pogardzana. Cóż, na szczęście doprowadziła Pięknotkę do Wioletty. Gdy Pietro wyznał (załamanej) Pięknotce że ma miętę do Wioletty, Pięknotka zdecydowała się ich skojarzyć. Lilia jako detektyw miała pomóc. Lilia wpadła jednak w szpony Moktara, który zaczął ją łamać. Pięknotka i Pietro uratowali Lilię, acz Pietro skończył w szpitalu. Pięknotka, ku swemu zdziwieniu, dała radę kogoś poderwać - Wiolettę. To nie były złe trzy dni...

Aktor w Opowieści:

* Dokonanie:
    * potwór. Złapał sobie Lilię, by ją złamać. Stoczył walkę z Pięknotką, w wyniku której Pietro wyniósł Lilię. Nie osiągnął sukcesu - ale obiecał Pięknotce, że się spotkają.
* Progresja:
    * epicka ballada Romualda sprawiła, że jest postrzegany jako jeszcze straszniejszy potwór. Z czym Moktar dobrze się czuje.
    * planuje zapolować na Pięknotkę Diakon (tak przy okazji) - chce jej pokazać KTO naprawdę tu rządzi.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 6, @: 0110-06-15
    1. Primus    : 6, @: 0110-06-15
        1. Sektor Astoriański    : 6, @: 0110-06-15
            1. Astoria    : 6, @: 0110-06-15
                1. Sojusz Letejski, NW    : 5, @: 0109-12-01
                    1. Ruiniec    : 5, @: 0109-12-01
                        1. Colubrinus Meditech    : 2, @: 0109-11-17
                        1. Colubrinus Psiarnia    : 2, @: 0109-12-01
                        1. Diamentowa Forteca    : 1, @: 0109-11-01
                        1. Skalny Labirynt    : 1, @: 0109-11-27
                        1. Studnia Bez Dna    : 1, @: 0109-11-27
                        1. Świątynia Bez Dna    : 1, @: 0109-12-01
                1. Sojusz Letejski    : 6, @: 0110-06-15
                    1. Przelotyk    : 6, @: 0110-06-15
                        1. Przelotyk Wschodni    : 6, @: 0110-06-15
                            1. Cieniaszczyt    : 6, @: 0110-06-15
                                1. Arena Nadziei Tęczy    : 1, @: 0109-11-01
                                1. Bazar Wschodu Astorii    : 1, @: 0109-11-01
                                1. Knajpka Szkarłatny Szept    : 3, @: 0110-06-15
                                1. Kompleks Nukleon    : 5, @: 0110-06-15
                                1. Mordownia Czaszka Kralotha    : 1, @: 0109-11-01
                                1. Mrowisko    : 4, @: 0109-12-01
                            1. Przejściak    : 1, @: 0110-06-15
                                1. Hotel Pirat    : 1, @: 0110-06-15

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 6 | ((181125-swaty-w-cieniu-potwora; 181218-tajemniczy-oltarz-moktara; 181225-czyszczenie-toksycznych-zwiazkow; 181226-finis-vitae; 181227-adieu-cieniaszczycie; 190724-odzyskana-agentka-orbitera)) |
| Atena Sowińska       | 4 | ((181218-tajemniczy-oltarz-moktara; 181225-czyszczenie-toksycznych-zwiazkow; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Waleria Cyklon       | 4 | ((181125-swaty-w-cieniu-potwora; 181218-tajemniczy-oltarz-moktara; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Amadeusz Sowiński    | 2 | ((181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Bogdan Szerl         | 2 | ((181218-tajemniczy-oltarz-moktara; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Julia Morwisz        | 2 | ((181225-czyszczenie-toksycznych-zwiazkow; 190724-odzyskana-agentka-orbitera)) |
| Lilia Ursus          | 2 | ((181125-swaty-w-cieniu-potwora; 181227-adieu-cieniaszczycie)) |
| Mirela Niecień       | 2 | ((181227-adieu-cieniaszczycie; 190724-odzyskana-agentka-orbitera)) |
| Pietro Dwarczan      | 2 | ((181125-swaty-w-cieniu-potwora; 181227-adieu-cieniaszczycie)) |
| Romuald Czurukin     | 2 | ((181125-swaty-w-cieniu-potwora; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Adam Szarjan         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Aida Serenit         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Arazille             | 1 | ((181226-finis-vitae)) |
| Brygida Maczkowik    | 1 | ((181227-adieu-cieniaszczycie)) |
| Erwin Galilien       | 1 | ((181227-adieu-cieniaszczycie)) |
| Mirela Orion         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Wioletta Kalazar     | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Zbigniew Burzycki    | 1 | ((181227-adieu-cieniaszczycie)) |