# Lorena Gwozdnik
## Identyfikator

Id: 2109-lorena-gwozdnik

## Sekcja Opowieści

### Supersupertajny plan Loreny

* **uid:** 220730-supersupertajny-plan-loreny, _numer względny_: 9
* **daty:** 0111-09-30 - 0111-10-04
* **obecni:** Arkadia Verlen, Daniel Terienak, Karolina Terienak, Liliana Bankierz, Marsen Gwozdnik, Władysław Owczarek, Żorż d'Namertel

Streszczenie:

Marsen Gwozdnik wszedł na teren by pomóc Lorenie (która nie wie) uważając Lorenę za najlepszego taktyka na świecie (którym nie jest). Wyedytował część kontraktów okolicznych firm z Eternią, by spowodować niechęć firm do Eterni (a Ernest próbuje pomóc by Mimoza nie była jedyną która pomaga). Karo zastawiła na Marsena pułapkę i wzięła jako wsparcie Arkadię. Marsen natomiast wziął Lancera i Lilianę. Skończyło się na rannej Arkadii, sprzedanym Lancerze, rannej Lilianie i tym, że Karo przekonała Marsenę że jest... agentką Loreny?

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * Marsen wierzy, że ona jest  najlepszym strategiem i że Ernest x Marysia walczą przeciw niej. Nie wie jak się z tego wyplątać.


### Płaszcz ochronny Mimozy

* **uid:** 220222-plaszcz-ochronny-mimozy, _numer względny_: 8
* **daty:** 0111-09-21 - 0111-09-25
* **obecni:** Ernest Namertel, Karolina Terienak, Keira Amarco d'Namertel, Lorena Gwozdnik, Marysia Sowińska, Mimoza Elegancja Diakon

Streszczenie:

Mimoza weszła do akcji - wzięła Lorenę do azylu i ochroniła część kultystek przed Ernestem. Konflikt Ernest - Mimoza się rozpalił, Marysia + Karo poszły się z Mimozą spotkać by zdeeskalować problem. Lorena podsłuchiwała, więc Mimoza straciła twarz. Cóż. Mimoza wynegocjowała przekazanie kultystek Sensacjuszowi, Marysia wynegocjowała by Lorena zrobiła ten akt. ALE W DOMU MIMOZY. Zaczyna się ciekawie...

Aktor w Opowieści:

* Dokonanie:
    * podsłuchiwała rozmowę Mimozy - Marysi (Mimoza o tym nie wie, więc sprawiła, że Mimoza straciła twarz). Robi co może by wyprzeć się że to ona narysowała akt.
* Progresja:
    * STARCIE z Ernestem Namertelem. Ona uważa go za siepacza z Eterni. Żąda wydania dwóch kultystek Sensacjuszowi.
    * okazuje się, że jest oficerem działań dywersyjnych zjednoczonych sił Aurum (co bardzo obniża opinię o tych siłach wszystkich co znają Lorenę)


### Akt, o którym Marysia nie wie

* **uid:** 211228-akt-o-ktorym-marysia-nie-wie, _numer względny_: 7
* **daty:** 0111-09-09 - 0111-09-12
* **obecni:** Daniel Terienak, Ernest Namertel, Karolina Terienak, Keira Amarco d'Namertel, Liliana Bankierz, Lorena Gwozdnik, Marysia Sowińska, Napoleon Bankierz, Rafał Torszecki

Streszczenie:

Torszecki ostrzegł Marysię, że jej akt krąży w Zaczęstwie. Ona to olała. Ale jak Ernest zaczął na niego polować to Marysia się zainteresowała. Zdecydowała się aktem odbić Ernesta Amelii. Zaflirtowała z Ernestem, obiecała mu akt specjalnie dla niego i że odda mu oryginał tego. Karo pokonała Napoleona, znalazła link do aktu który ostatecznie zdobył Torszecki. Daniel dla Karo zbił Torszeckiego i odzyskał akt. Marysia zaflirtowała z Ernestem ;-). Torszecki, który chciał dobrze i chronił Marysię skończył w szpitalu.

Aktor w Opowieści:

* Dokonanie:
    * stworzyła piękny akt Marysi Sowińskiej mającej wszystko na głowie, który trafił do Napoleona Bankierza. Dowiedziała się, że Marysia o niej wie, więc oddała się pod ochronę Ernesta Namertela.
* Progresja:
    * pod ochroną Ernesta Namertela by Marysia Sowińska nie zrobiła jej krzywdy (nie, żeby chciała).
    * przyjaciółka Liliany Bankierz. Połączyło je upodobanie do piękna.


### Gdy zabraknie prądu Rekinom

* **uid:** 211207-gdy-zabraknie-pradu-rekinom, _numer względny_: 6
* **daty:** 0111-08-30 - 0111-08-31
* **obecni:** Arkadia Verlen, Arnold Kłaczek, Daniel Terienak, Henryk Wkrąż, Hestia d'Rekiny, Karolina Terienak, Lorena Gwozdnik, Marysia Sowińska, Natalia Tessalon, Sensacjusz Diakon, Urszula Arienik

Streszczenie:

Ponieważ Amelia nie przekazała Marysi informacji o tym, że ta powinna przejąć obowiązki zarządcze to Marysia jako administratorka nie dostawała informacji. Przez to odcięło im prąd. Marysia przywróciła prąd podpisując (standardową) umowę z dystrybutorem podwierckim a Karolina uratowała ludzką część Dzielnicy Rekinów od pobicia przez wkurzonych magów. Próba obniżenia rachunków przez współpracę z mafią została przez dziewczyny zażegnana używając Arkadii.

Aktor w Opowieści:

* Dokonanie:
    * w gliździe medycznej, bo Marysia potrzebowała prądu z generatora Sensacjusza.
* Progresja:
    * święcie przekonana, że Marysia stoi po stronie Karoliny i nią gardzi / jej nienawidzi. Musi unikać Marysi.


### Satarail pomaga Marysi

* **uid:** 211102-satarail-pomaga-marysi, _numer względny_: 5
* **daty:** 0111-08-09 - 0111-08-10
* **obecni:** Ernest Namertel, Karolina Terienak, Lorena Gwozdnik, Marek Samszar, Marysia Sowińska, Rafał Torszecki, Sensacjusz Diakon, Wiktor Satarail

Streszczenie:

Satarail uderzył - zainfekował Owadem który wypełzł z Torszeckiego Samszara (który kiedyś ukradł somnibela Olgi). Zainfekowane Rekiny zaczęły dewastować dzielnicę. Karo skutecznie rozwala owady (i Lorenę), po czym Sensacjusza w glistę medyczną. Marysia samodzielnie leci jako owad wyłączyć Samszara. Torszecki jest rozgrzeszony - "nie był sobą". Ale polowanie na Torszeckiego czas zacząć.

Aktor w Opowieści:

* Dokonanie:
    * zainfekowana przez Owada Sataraila zaatakowała ścigacz Karoliny wyrzutnią rakiet nie mając pełnej władzy mentalnej nad sobą. Karolina połamała jej nogi ścigaczem.
* Progresja:
    * ciężko połamana; Karolina wbiła się jej w nogi swoim ścigaczem gdy Lorena pod wpływem Owada ją zaatakowała wyrzutnią rakiet
    * jej relacja z Karoliną Terienak przekroczyła point of no return. Lorena nienawidzi Karoliny (loathing, nie hate). Chce być jak najdalej od niej.


### Serafina staje za Wydrami

* **uid:** 210831-serafina-staje-za-wydrami, _numer względny_: 4
* **daty:** 0111-07-07 - 0111-07-10
* **obecni:** Daniel Terienak, Halina Sermniek, Karolina Terienak, Laura Tesinik, Lorena Gwozdnik, Marysia Sowińska, Rafał Torszecki, Serafina Ira

Streszczenie:

Karolina MUSI poznać prawdę co się stało z jej bratem. Odkryła, że istnieje grupa Wydry polująca na Rekiny, ludzi skrzywdzonych przez Rekiny w przeszłości. I pomaga im Serafina Ira. Z Marysią porozmawiały z Serafiną i przekonały ją, by ta działała wolniej - i drugą linią napuściły na Serafinę terminusów z Pustogoru.

Aktor w Opowieści:

* Dokonanie:
    * kręci się koło Daniela (dziewczyna?). Duża antypatia do Karoliny; dość zadziorna. Nie pierwszy raz włamuje się do ludzi - wie jak to robić i ma przygotowane odpowiednie czarne, ekranowane stroje.
* Progresja:
    * zerwała z Danielem Terienakiem. Ma psychiczną siostrę.
    * personalnie wściekła na Karolinę Terienak. Ma z nią kosę za wszystko (obrażanie, traktowanie, zerwanie jej związku itp).


### Porywaczka miragentów

* **uid:** 210518-porywaczka-miragentow, _numer względny_: 3
* **daty:** 0111-05-07 - 0111-05-08
* **obecni:** Diana Lemurczak, Julia Kardolin, Lorena Gwozdnik, Marysia Sowińska, Triana Porzecznik

Streszczenie:

Lorena chciała zaimponować Gerwazemu i wysłała na Dianę miragenta. Diana przejęła miragenta, ale ten w rozpaczy ostrzelał Marysię Sowińską. Zespół znalazł Lorenę, zestrzelił ją (berserkerskimi dronami Triany) i poznał prawdę o miragencie, po czym udał się do klubu Arkadia w Podwiercie i odebrał Dianie miragenta. Nie powiedzieli nikomu innemu, że wiedzą o jej Eidolonie.

Aktor w Opowieści:

* Dokonanie:
    * chciała się popisać przed Gerwazym i wysłała miragenta przeciw Dianie (i go straciła). Próbując go odzyskać, wpakowała się w strzelaninę z Marysią Sowińską. Skończyła ranna i z mandatem.


### Potencjalnie eksterytorialny seksbot

* **uid:** 210406-potencjalnie-eksterytorialny-seksbot, _numer względny_: 2
* **daty:** 0111-04-27 - 0111-04-28
* **obecni:** Barnaba Burgacz, Feliks Keksik, Franek Bulterier, Henryk Wkrąż, Julia Kardolin, Lorena Gwozdnik, Marysia Sowińska, Rupert Mysiokornik, Sabina Kazitan, Triana Porzecznik

Streszczenie:

Sabina Kazitan zostawiła za sobą plan mający skonfliktować Rekiny na poziomie Podwiertu, co miałoby kaskadować na relacje rodów w Aurum. Plan zawierał Skażonego seksbota, którego Rekiny miały same zrobić. Podszepnęła też odpowiednie słówka odpowiednim osobom. Jednak gdy podatne Rekiny chciały iść za tym planem, gdy szabrowały ze złomowiska głowę seksbota to zatrzymał je Zespół. Skończyło się na tym, że seksbota nie będzie - Marysia im absolutnie zakazała cokolwiek robić, nawet, jeśli nie znała planu Sabiny.

Aktor w Opowieści:

* Dokonanie:
    * 3 mc temu Marysia kazała jej zająć się Rupertem Mysiokornikiem, by ten nie szalał na imprezie. Zajęła się nim, sama się upiła, tańczyła na stole a potem skończyła w łóżku z Mysiokornikiem... wstydzi się.


### Dezinhibitor dla Sabiny

* **uid:** 201006-dezinhibitor-dla-sabiny, _numer względny_: 1
* **daty:** 0110-10-07 - 0110-10-09
* **obecni:** Aranea Diakon, Gabriel Ursus, Ignacy Myrczek, Justynian Diakon, Karolina Erenit, Laura Tesinik, Lorena Gwozdnik, Napoleon Bankierz, Rafał Kumczek, Sabina Kazitan

Streszczenie:

Zanim Sabina wróci do Aurum, Rekiny Paladyni chcieli ją przyskrzynić - podać jej dezinhibitor w towarzystwie Myrczka i to nagrać, z nadzieją, że się ją zamknie. Gabriel i Laura wykryli intrygę i zatrzymali ją, nie dopuszczając do dezinhibicji Sabiny. O dziwo, tien Kazitan chciała Myrczka CHRONIĆ.

Aktor w Opowieści:

* Dokonanie:
    * Rekin; dramatycznie pragnie się wykazać przed Justynianem lub kimkolwiek. Wzięła na siebie rolę syntezatora dezinhibitora i udawała seksbota (ubranego) przed Myrczkiem.
* Progresja:
    * po wstrzyknięciu sobie dezinhibitora chciała PRZEJĄĆ SENTISIEĆ. Ale po spotkaniu z Gabrielem dezinhibitor przekierował na niego jej oddanie. Pirotess do Ashrama.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 8, @: 0111-09-25
    1. Primus    : 8, @: 0111-09-25
        1. Sektor Astoriański    : 8, @: 0111-09-25
            1. Astoria    : 8, @: 0111-09-25
                1. Sojusz Letejski    : 8, @: 0111-09-25
                    1. Szczeliniec    : 8, @: 0111-09-25
                        1. Powiat Jastrzębski    : 1, @: 0110-10-09
                            1. Jastrząbiec, okolice    : 1, @: 0110-10-09
                                1. Blokhaus Widmo    : 1, @: 0110-10-09
                        1. Powiat Pustogorski    : 8, @: 0111-09-25
                            1. Czółenko    : 1, @: 0111-08-31
                                1. Generatory Keriltorn    : 1, @: 0111-08-31
                            1. Podwiert    : 8, @: 0111-09-25
                                1. Dzielnica Luksusu Rekinów    : 7, @: 0111-09-25
                                    1. Fortyfikacje Rolanda    : 1, @: 0111-09-25
                                    1. Obrzeża Biedy    : 2, @: 0111-09-25
                                        1. Domy Ubóstwa    : 1, @: 0111-09-25
                                        1. Hotel Milord    : 1, @: 0111-09-25
                                        1. Stadion Lotników    : 2, @: 0111-09-25
                                        1. Stajnia Rumaków    : 1, @: 0111-09-25
                                    1. Sektor Brudu i Nudy    : 2, @: 0111-09-25
                                        1. Komputerownia    : 2, @: 0111-09-25
                                        1. Konwerter Magielektryczny    : 1, @: 0111-09-25
                                        1. Magitrownia Pogardy    : 1, @: 0111-09-25
                                        1. Skrytki Czereśniaka    : 1, @: 0111-09-25
                                    1. Serce Luksusu    : 5, @: 0111-09-25
                                        1. Apartamentowce Elity    : 3, @: 0111-09-25
                                        1. Arena Amelii    : 2, @: 0111-09-25
                                        1. Fontanna Królewska    : 1, @: 0111-09-25
                                        1. Kawiarenka Relaks    : 1, @: 0111-09-25
                                        1. Lecznica Rannej Rybki    : 3, @: 0111-09-25
                                1. Kompleks Korporacyjny    : 1, @: 0111-08-31
                                    1. Dystrybutor Prądu Ozitek    : 1, @: 0111-08-31
                                    1. Elektrownia Węglowa Szarpien    : 1, @: 0111-08-31
                                1. Las Trzęsawny    : 3, @: 0111-07-10
                                    1. Jeziorko Mokre    : 1, @: 0111-07-10
                                1. Osiedle Leszczynowe    : 1, @: 0111-05-08
                                    1. Klub Arkadia    : 1, @: 0111-05-08
                                1. Osiedle Tęczy    : 1, @: 0111-07-10
                            1. Pustogor    : 1, @: 0111-08-10
                                1. Rdzeń    : 1, @: 0111-08-10
                                    1. Szpital Terminuski    : 1, @: 0111-08-10
                            1. Zaczęstwo    : 5, @: 0111-09-25
                                1. Akademia Magii, kampus    : 3, @: 0111-09-25
                                    1. Akademik    : 2, @: 0111-09-25
                                    1. Arena Treningowa    : 3, @: 0111-09-25
                                    1. Las Trzęsawny    : 2, @: 0111-09-25
                                1. Dzielnica Kwiecista    : 2, @: 0111-05-08
                                    1. Rezydencja Porzeczników    : 2, @: 0111-05-08
                                1. Złomowisko    : 1, @: 0111-04-28

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 7 | ((210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow; 210831-serafina-staje-za-wydrami; 211102-satarail-pomaga-marysi; 211207-gdy-zabraknie-pradu-rekinom; 211228-akt-o-ktorym-marysia-nie-wie; 220222-plaszcz-ochronny-mimozy)) |
| Karolina Terienak    | 5 | ((210831-serafina-staje-za-wydrami; 211102-satarail-pomaga-marysi; 211207-gdy-zabraknie-pradu-rekinom; 211228-akt-o-ktorym-marysia-nie-wie; 220222-plaszcz-ochronny-mimozy)) |
| Daniel Terienak      | 3 | ((210831-serafina-staje-za-wydrami; 211207-gdy-zabraknie-pradu-rekinom; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Ernest Namertel      | 3 | ((211102-satarail-pomaga-marysi; 211228-akt-o-ktorym-marysia-nie-wie; 220222-plaszcz-ochronny-mimozy)) |
| Rafał Torszecki      | 3 | ((210831-serafina-staje-za-wydrami; 211102-satarail-pomaga-marysi; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Henryk Wkrąż         | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 211207-gdy-zabraknie-pradu-rekinom)) |
| Julia Kardolin       | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow)) |
| Keira Amarco d'Namertel | 2 | ((211228-akt-o-ktorym-marysia-nie-wie; 220222-plaszcz-ochronny-mimozy)) |
| Laura Tesinik        | 2 | ((201006-dezinhibitor-dla-sabiny; 210831-serafina-staje-za-wydrami)) |
| Napoleon Bankierz    | 2 | ((201006-dezinhibitor-dla-sabiny; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Sabina Kazitan       | 2 | ((201006-dezinhibitor-dla-sabiny; 210406-potencjalnie-eksterytorialny-seksbot)) |
| Sensacjusz Diakon    | 2 | ((211102-satarail-pomaga-marysi; 211207-gdy-zabraknie-pradu-rekinom)) |
| Triana Porzecznik    | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow)) |
| Aranea Diakon        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Arkadia Verlen       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Arnold Kłaczek       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Barnaba Burgacz      | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Diana Lemurczak      | 1 | ((210518-porywaczka-miragentow)) |
| Feliks Keksik        | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Franek Bulterier     | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Gabriel Ursus        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Halina Sermniek      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Hestia d'Rekiny      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Ignacy Myrczek       | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Justynian Diakon     | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Karolina Erenit      | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Liliana Bankierz     | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Marek Samszar        | 1 | ((211102-satarail-pomaga-marysi)) |
| Mimoza Elegancja Diakon | 1 | ((220222-plaszcz-ochronny-mimozy)) |
| Natalia Tessalon     | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Rafał Kumczek        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Rupert Mysiokornik   | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Serafina Ira         | 1 | ((210831-serafina-staje-za-wydrami)) |
| Urszula Arienik      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Wiktor Satarail      | 1 | ((211102-satarail-pomaga-marysi)) |