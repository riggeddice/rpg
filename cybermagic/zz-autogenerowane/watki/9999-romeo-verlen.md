# Romeo Verlen
## Identyfikator

Id: 9999-romeo-verlen

## Sekcja Opowieści

### Elena z rodu Verlen

* **uid:** 210331-elena-z-rodu-verlen, _numer względny_: 7
* **daty:** 0111-08-27 - 0111-08-30
* **obecni:** Arianna Verlen, Dariusz Blakenbauer, Elena Verlen, Krystian Blakenbauer, Romeo Verlen, Viorika Verlen

Streszczenie:

Arianna i Viorika doprowadziły do zamknięcia wojny Blakenbauer - Verlen. Viorika dotarła do Dariusza Blakenbauera i wydobyła co Blakenbauerowie zrobili Elenie (destrukcja reputacji) i co Elena zrobiła im (nie mają miejsca wśród gwiazd). Arianna natomiast wkręciła Romeo w wyznanie Elenie swych uczuć, a potem doprowadziła Elenę do płaczu. Razem przezwyciężą wszystko. Jakoś. Będzie pokój a Elena wróci do rodu Verlen.

Aktor w Opowieści:

* Dokonanie:
    * zmanipulowany przez Ariannę, wyznał Elenie miłość i raz na zawsze zamknął z nią tą chorą rywalizację. Teraz Elena nim trochę gardzi i się lituje, nie nienawidzi...


### Miłość w rodzie Verlen

* **uid:** 210210-milosc-w-rodzie-verlen, _numer względny_: 6
* **daty:** 0111-08-21 - 0111-08-24
* **obecni:** Apollo Verlen, Arianna Verlen, Brunhilda Verlen, Elena Verlen, Franz Verlen, Krucjusz Verlen, Romeo Verlen, Seraf Verlen, Viorika Verlen

Streszczenie:

Arianna i Elena wróciły do domu, do rodu Verlen. Tam się okazało, że ród jest skłonny iść na wojnę z Sowińskimi za despekty (często słuszne) wyrządzane Ariannie i Elenie. Elena nie umie się odnaleźć jako akceptowana i lubiana. Elena integruje się z sentisprzężonym Eidolonem i statkiem orbitalnym; po czym w pojedynku pokonuje swojego dawnego rywala, Romeo. Wychodzą na jaw Mroczne Sekrety Eleny. A Arianna próbuje zapobiec tragedii, nie dać się wrobić w potomka i pokazuje przypadkiem najbardziej efektowny ratunek klasy arcymag ever.

Aktor w Opowieści:

* Dokonanie:
    * kochał się kiedyś w Elenie, teraz jej wieczny rywal. Przegrał z nią pojedynek na pilotaż. Waleczny i odważny, oddany rodowi. BW.


### Kapitan Verlen i pierwszy ruch statku

* **uid:** 221019-kapitan-verlen-i-pierwszy-ruch-statku, _numer względny_: 5
* **daty:** 0100-05-23 - 0100-06-04
* **obecni:** Arianna Verlen, Daria Czarnewik, Erwin Pies, Maja Samszar, Marcelina Trzęsiel, OO Królowa Kosmicznej Chwały, Romeo Verlen, Rufus Warkoczyk, Stefan Torkil

Streszczenie:

Arianna włączyła ćwiczenia dla Królowej - prawie wyszło, ale ludzie popanikowali a statek odmówił posłuszeństwa i załoga zaczęła się bić. Arianna ich przećwiczyła w działaniach awaryjnych i oddała panikarza medykowi - niestety, Paradoks zmienił Marcela w Marcelinę... ale następnym razem ćwiczenia powinny się już udać. Daria ma sprawę pod kontrolą i Arianna też raczej jest pewna sukcesu.

Aktor w Opowieści:

* Dokonanie:
    * KIEDYŚ zniszczył jedzenie Mai i poderwał jej kuzynkę. A potem walczył nie fair. Więc Maja i kilka Samszarów ma na niego krechę i chcą go pojechać. Przez niego Arianna ma teraz problemy z Mają Samszar.


### Lustrzane odbicie Eleny

* **uid:** 210324-lustrzane-odbicie-eleny, _numer względny_: 4
* **daty:** 0097-09-02 - 0097-09-05
* **obecni:** Arianna Verlen, Elena Verlen, Lucjusz Blakenbauer, Michał Perikas, Przemysław Czapurt, Romeo Verlen, Viorika Verlen

Streszczenie:

Elena, chcąc utrzymać kontrolę nad swoją mocą zaczęła brać wiktoriatę. To plus przepuszczanie magii przez lustra zniszczyło jej wzór i ją uszkodziło. Elena stała się wampirem - pożera energetycznie żołnierzy by chronić teren i wszystkich cywilów. Plus, pragnie zemsty na Blakenbauerach i na Perikasach. W swoim Skażeniu powołała Lustrzanego Golema. Viorika i Arianna dały radę wymanewrować Elenę, odkryć, że to ona stoi za Lustrzanym Golemem i z pomocą sierżanta Czapurta w Poniewierzy dały radę Elenę unieszkodliwić, by ją docelowo naprawić...

Aktor w Opowieści:

* Dokonanie:
    * przez Czapurta jest w jednym oddziale z Eleną. Dokucza konsekwentnie Elenie oraz pod koniec autoryzował unieruchomienie Eleny (on, Viorika i Arianna).


### Piękna Diakonka i rytuał nirwany kóz

* **uid:** 230606-piekna-diakonka-i-rytual-nirwany-koz, _numer względny_: 3
* **daty:** 0095-08-15 - 0095-08-18
* **obecni:** AJA Szybka Strzała, Dźwiedź Łagodne Słowo, Elena Samszar, Herbert Samszar, Impresja Ignicja Incydencja Diakon, Karolinus Samszar, Maks Samszar

Streszczenie:

Itria Diakon ma kolejny projekt - nirwana kóz u Samszarów. Jej obecność spowodowała chaos w Forcie Tawalizer. Karolinus i Elena S. dotarli tam by dowiedzieć się o tajnych podziemnych bazach Samszarów od Herberta, ale musieli wpierw rozwiązać problem z Itrią. W końcu doprowadzili do rytuału kóz (dzięki Elenie i jej researchowi) i trochę naprawili sytuację. Czas spotkać się z Herbertem poza terenem sentisieci i poznać sekrety badań nad duchami.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * dostał opieprz od Vioriki za to, że Elena S. wysłała do niej "omg zawiódł operację". Nie dlatego, że zawiódł operację. Dlatego, że R. pokazał tak dużą słabość że się E.S. odważyła napisać.


### Romeo, dyskretny instalator Supreme Missionforce

* **uid:** 230523-romeo-dyskretny-instalator-supreme-missionforce, _numer względny_: 2
* **daty:** 0095-08-09 - 0095-08-11
* **obecni:** AJA Szybka Strzała, Albert Samszar, Elena Samszar, Karolinus Samszar, Maja Samszar, Nataniel Samszar, Romeo Verlen

Streszczenie:

Strzała ma za zadanie wychować i nauczyć współpracy Karolinusa i Eleny. Karolinus jest wezwany do interwencji, gdy Maja przesyła sygnał SOS. Maja twierdzi, że niechcący mogła zabić kolegę. Ekipa dociera na miejsce, gdzie odkrywają ciężarówkę z zaawansowanym sprzętem. Maja jest w stanie panicznym, a jej magia wymyka się spod kontroli - próbuje przebić dziwne pole siłowe otaczające dziurę w piwnicy biurowca. Karolinus i Elena zmuszają Maję do opuszczenia miejsca, obiecując uratować Romeo. W międzyczasie, Strzała zabiera Maję do Ogrodów Medytacyjnych by nie miała problemów z ojcem. Elena zyskuje na czasie przed ojcem Mai (Albertem), dopóki Maja nie jest bezpiecznie umieszczona w Ogrodach. Potem wydobywają Romeo z podziemi zauważając, że ktoś tam ma tajną dziwną bazę do eksperymentów na duchach.

Aktor w Opowieści:

* Dokonanie:
    * waleczny acz pełen pasji (BWR: individual, order, emotion). Nie lubi Mai Samszar, ale chciał podjechać jej zamontować Supreme Missionforce Chambers by ta mogła ćwiczyć i być lepsza. By "nie było wstydu jak przeciw niej walczy". Pożyczył ciężką ciężarówkę, monterów i podjechał do Karmazynowego Świtu (z 3 monterami i bardziej doświadczonymi ludźmi). A potem z Mają znalazł Dziwną Ukrytą Bazę i się uwięził XD. Odpalił techno i wyładowania by nie znalazły go potwory i schował się w kwaterach mieszkalnych. Jak Samszarowie wyszli, pomógł im z jednym potworem i w "kanapce" został ewakuowany przez Samszarów.


### Niepotrzebny ratunek Mai

* **uid:** 230328-niepotrzebny-ratunek-mai, _numer względny_: 1
* **daty:** 0095-06-30 - 0095-07-02
* **obecni:** AJA Szybka Strzała, Apollo Verlen, Bonifacy Samszar, Fiona Szarstasz, Karolinus Samszar, Maja Samszar, Romeo Verlen, Viorika Verlen

Streszczenie:

Tydzień po ucieczce z Verlenlandu, Maja, kuzynka Karolinusa, zostaje porwana. Karolinus próbuje zdobyć informacje i nagrania z porwania. Odkrywa, że Maja miała kontakt z Romeo Verlenem, a jej rodzice kasują informacje o nieeleganckim zachowaniu córki. Karolinus łączy się z Romeem, który twierdzi, że Maja jest bezpieczna w mieście VirtuFortis (i ogólnie jest OK). Karolinus wyrusza tam ze swoim zespołem na pokładzie Szybkiej Strzały. 

Niestety, podczas Paradoksu udało się przypadkowo Karolinusowi zaatakować duchami o kształcie Mai Verlenland. Docierają w końcu do VirtuFortis i Karolinus kontaktuje się z Mają. Maja twierdzi, że jest porwana, ale Fiona wyczuwa, że Maja ściemnia. Okazuje się, że Maja uciekła z domu i przyjechała na turniej Supreme Missionforce, żeby zmierzyć się z Romeem. Karolinus przekonuje Maję, żeby wzięła udział w turnieju, a on odbierze ją po tygodniu. Reputacja Karolinusa wzrasta wśród Verlenów, ale spada wśród Samszarów. Fiona zostaje uważana za zły wpływ na Karolinusa.

Aktor w Opowieści:

* Dokonanie:
    * przekonał Viorikę do utrzymania Mai na czas Supreme Missionforce, przekonał Apolla do idiotycznego okupu by kupić czas i wygadał się niefortunnie Karolinusowi że temat z Mają to nie jest proste porwanie.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 6, @: 0111-08-30
    1. Primus    : 6, @: 0111-08-30
        1. Sektor Astoriański    : 6, @: 0111-08-30
            1. Astoria    : 5, @: 0111-08-30
                1. Sojusz Letejski    : 5, @: 0111-08-30
                    1. Aurum    : 5, @: 0111-08-30
                        1. Powiat Samszar    : 1, @: 0095-08-11
                            1. Karmazynowy Świt, okolice    : 1, @: 0095-08-11
                                1. Centrum Danych Symulacji Zarządzania    : 1, @: 0095-08-11
                                    1. Techbunkier Arvitas    : 1, @: 0095-08-11
                                        1. Kontrola bezpieczeństwa (1)    : 1, @: 0095-08-11
                                        1. Kwatery mieszkalne (1)    : 1, @: 0095-08-11
                            1. Karmazynowy Świt    : 1, @: 0095-08-11
                        1. Verlenland    : 4, @: 0111-08-30
                            1. Hold Karaan, obrzeża    : 1, @: 0111-08-24
                                1. Lądowisko    : 1, @: 0111-08-24
                            1. Hold Karaan    : 2, @: 0111-08-30
                                1. Barbakan Centralny    : 1, @: 0111-08-24
                                1. Krypta Plugastwa    : 2, @: 0111-08-30
                                1. Szpital Centralny    : 1, @: 0111-08-30
                            1. Mikrast    : 1, @: 0097-09-05
                            1. Poniewierz    : 2, @: 0111-08-30
                                1. Arena    : 1, @: 0097-09-05
                                1. Zamek Gościnny    : 2, @: 0111-08-30
                            1. Skałkowa Myśl    : 1, @: 0097-09-05
                            1. VirtuFortis    : 1, @: 0095-07-02
                                1. Akademia VR Aegis    : 1, @: 0095-07-02
                                1. Bastion Przyrzeczny    : 1, @: 0095-07-02
                                1. Bazarek Lokalny    : 1, @: 0095-07-02
                                1. Stadion Sportowy    : 1, @: 0095-07-02
                                1. Wielki Plac Miraży    : 1, @: 0095-07-02

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 4 | ((210210-milosc-w-rodzie-verlen; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen; 221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Viorika Verlen       | 4 | ((210210-milosc-w-rodzie-verlen; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen; 230328-niepotrzebny-ratunek-mai)) |
| Elena Verlen         | 3 | ((210210-milosc-w-rodzie-verlen; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen)) |
| Maja Samszar         | 3 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku; 230328-niepotrzebny-ratunek-mai; 230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| AJA Szybka Strzała   | 2 | ((230328-niepotrzebny-ratunek-mai; 230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Apollo Verlen        | 2 | ((210210-milosc-w-rodzie-verlen; 230328-niepotrzebny-ratunek-mai)) |
| Karolinus Samszar    | 2 | ((230328-niepotrzebny-ratunek-mai; 230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Albert Samszar       | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Bonifacy Samszar     | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Brunhilda Verlen     | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Daria Czarnewik      | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Dariusz Blakenbauer  | 1 | ((210331-elena-z-rodu-verlen)) |
| Elena Samszar        | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Erwin Pies           | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Fiona Szarstasz      | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Franz Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Krucjusz Verlen      | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Krystian Blakenbauer | 1 | ((210331-elena-z-rodu-verlen)) |
| Lucjusz Blakenbauer  | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Marcelina Trzęsiel   | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Michał Perikas       | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Nataniel Samszar     | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| OO Królowa Kosmicznej Chwały | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Przemysław Czapurt   | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Rufus Warkoczyk      | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Seraf Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Stefan Torkil        | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |