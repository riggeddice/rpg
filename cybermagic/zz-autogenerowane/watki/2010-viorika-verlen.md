# Viorika Verlen
## Identyfikator

Id: 2010-viorika-verlen

## Sekcja Opowieści

### Elena z rodu Verlen

* **uid:** 210331-elena-z-rodu-verlen, _numer względny_: 14
* **daty:** 0111-08-27 - 0111-08-30
* **obecni:** Arianna Verlen, Dariusz Blakenbauer, Elena Verlen, Krystian Blakenbauer, Romeo Verlen, Viorika Verlen

Streszczenie:

Arianna i Viorika doprowadziły do zamknięcia wojny Blakenbauer - Verlen. Viorika dotarła do Dariusza Blakenbauera i wydobyła co Blakenbauerowie zrobili Elenie (destrukcja reputacji) i co Elena zrobiła im (nie mają miejsca wśród gwiazd). Arianna natomiast wkręciła Romeo w wyznanie Elenie swych uczuć, a potem doprowadziła Elenę do płaczu. Razem przezwyciężą wszystko. Jakoś. Będzie pokój a Elena wróci do rodu Verlen.

Aktor w Opowieści:

* Dokonanie:
    * dogadała się z Dariuszem Blakenbauerem i doprowadziła do tego, że on nie chce wojny z Eleną i sam lobbuje za pokojem w swoim rodzie.


### Miłość w rodzie Verlen

* **uid:** 210210-milosc-w-rodzie-verlen, _numer względny_: 13
* **daty:** 0111-08-21 - 0111-08-24
* **obecni:** Apollo Verlen, Arianna Verlen, Brunhilda Verlen, Elena Verlen, Franz Verlen, Krucjusz Verlen, Romeo Verlen, Seraf Verlen, Viorika Verlen

Streszczenie:

Arianna i Elena wróciły do domu, do rodu Verlen. Tam się okazało, że ród jest skłonny iść na wojnę z Sowińskimi za despekty (często słuszne) wyrządzane Ariannie i Elenie. Elena nie umie się odnaleźć jako akceptowana i lubiana. Elena integruje się z sentisprzężonym Eidolonem i statkiem orbitalnym; po czym w pojedynku pokonuje swojego dawnego rywala, Romeo. Wychodzą na jaw Mroczne Sekrety Eleny. A Arianna próbuje zapobiec tragedii, nie dać się wrobić w potomka i pokazuje przypadkiem najbardziej efektowny ratunek klasy arcymag ever.

Aktor w Opowieści:

* Dokonanie:
    * starsza od Arianny i stopniem i wiekiem, przyjaciółka Arianny i Eleny. Mistrzowsko steruje sentisiecią i stabilizuje wszystko, by Arianna i Elena nie ucierpiały. Nieco złośliwa ;-).


### Lustrzane odbicie Eleny

* **uid:** 210324-lustrzane-odbicie-eleny, _numer względny_: 12
* **daty:** 0097-09-02 - 0097-09-05
* **obecni:** Arianna Verlen, Elena Verlen, Lucjusz Blakenbauer, Michał Perikas, Przemysław Czapurt, Romeo Verlen, Viorika Verlen

Streszczenie:

Elena, chcąc utrzymać kontrolę nad swoją mocą zaczęła brać wiktoriatę. To plus przepuszczanie magii przez lustra zniszczyło jej wzór i ją uszkodziło. Elena stała się wampirem - pożera energetycznie żołnierzy by chronić teren i wszystkich cywilów. Plus, pragnie zemsty na Blakenbauerach i na Perikasach. W swoim Skażeniu powołała Lustrzanego Golema. Viorika i Arianna dały radę wymanewrować Elenę, odkryć, że to ona stoi za Lustrzanym Golemem i z pomocą sierżanta Czapurta w Poniewierzy dały radę Elenę unieszkodliwić, by ją docelowo naprawić...

Aktor w Opowieści:

* Dokonanie:
    * jej ukrytym chłopakiem jest Lucjusz Blakenbauer. Uratowała Michała Perikasa używając jetpacka, zbadała sentisieć pod kątem Skalniaczków i ich wpływu oraz odkryła sekretne Miejsce Kaźni Eleny.


### Studenci u Verlenów

* **uid:** 210311-studenci-u-verlenow, _numer względny_: 11
* **daty:** 0097-01-16 - 0097-01-22
* **obecni:** Apollo Verlen, Arianna Verlen, Dariusz Blakenbauer, Elena Verlen, Maja Samszar, Michał Perikas, Rafał Perikas, Rufus Samszar, Sylwia Perikas, Viorika Verlen

Streszczenie:

Verlenowie mieli wymianę studencką, współpracę z magami rodów Perikas i Samszar. Dariusz Blakenbauer chciał rozbić potencjalny sojusz, dając młodym Perikasom lustro mające zaspokoić ich ochotę na Elenę. Niestety, wszystko poszło katastrofalnie nie tak bo Elena ma anomalną magię - w wyniku eksplozji zginęło kilkanaście wił i 2 żołnierzy. Elena w ruinie psychicznej, wszyscy przerażeni. Wezwane Arianna i Viorika odkryły że za wszystkim stoi Blakenbauer i znalazły winnych wśród młodych Perikasów. Jednak - dla polityki - wszystko zwaliły na Blakenbauerów :D.

Aktor w Opowieści:

* Dokonanie:
    * 21 lat, podczas walki z echem Eleny sprzęgła je z lustrem, po czym odnalazła to lustro używając Eleny. Uspokajała i zajmowała się Eleną.


### Ekologia jaszczurów w Arachnoziemie

* **uid:** 230524-ekologia-jaszczurow-w-arachnoziemie, _numer względny_: 10
* **daty:** 0095-08-06 - 0095-08-09
* **obecni:** Arianna Verlen, Fabian Rzelicki, Marcel Biekakis, Mściząb Verlen, Ula Blakenbauer, Viorika Verlen

Streszczenie:

W Arachnoziemie są dwa stwory - śluzowiec i mimik. A+V wprowadziły mechanizmy ochrony ludzi i oddzielania zainfekowanych ludzi od potworów i wezwały Ulę Blakenbauer na pomoc. Ta przybyła, zrobiła biolab i stworzyła płaszczki mające uratować kogo się da; jaszczury tunelowe ściągnie od Blakenbauerów. Sytuacja jest opanowana - gdy Ula zajęła się usuwaniem śluzowców to Verlenki usunęły krystalicznego mimika. Ogólnie - Arachnoziem ustabilizowany. A całość zasług za karę dostał Mściząb.

Aktor w Opowieści:

* Dokonanie:
    * przepytała żołnierzy i Kłów odnośnie sytuacji, sprawdziła dokumenty żołnierzy by izolować śluzińce, zaopiekowała się pacyfistyczną Ulą i po włączeniu Mścizęba w akcję usuwania krystalicznego mimika opracowała jak go usunąć i go rozstrzelała.


### Mściząb, zabójca arachnoziemskich jaszczurów

* **uid:** 230426-mscizab-zabojca-arachnoziemskich-jaszczurow, _numer względny_: 9
* **daty:** 0095-08-03 - 0095-08-05
* **obecni:** Arianna Verlen, Atraksjusz Verlen, Emmanuelle Gęsiawiec, Mściząb Verlen, Viorika Verlen

Streszczenie:

Opiekując się Eleną, Arianna jest wyczulona na sygnały Esuriit przekazywane przez sentisieć, nawet 'wyciszone', więc A+V ruszyły do Arachnoziem. Tam się okazuje że Mściząb Verlen zrobił blokadę informacyjną bo chce pokonać potwora który tam jest bez pomocy; tyle, że zginęło już kilku ludzi m.in. flarą Esuriit 17-latek. Za wszystkim stoi zakochana w Mścizębie Emmanuelle, która jest w jego oddziale i udaje kogoś innego - ona doprowadziła do rozognienia Tunelowych Jaszczurów. Ale jak Jaszczurów zabrakło, na powierzchnię wyszło Coś Innego, potworny łowca polujący na ludzi...

Aktor w Opowieści:

* Dokonanie:
    * gdy Mściząb (17) ją zaczepił, poszła z nim na ostro i zdewastowała w walce. Opierniczyła Mścizęba i próbowała pokazać mu zniszczenia jakie powoduje. "Zły glina" gadając z Emmanuelle.


### Karolinka - raciczki zemsty Verlenów

* **uid:** 230516-karolinka-raciczki-zemsty-verlenow, _numer względny_: 8
* **daty:** 0095-07-29 - 0095-07-31
* **obecni:** Aleksander Samszar, Amara Zegarzec, Elena Samszar, Franciszek Chartowiec, Karolinus Samszar, Ludmiła Zegarzec

Streszczenie:

Karolinka, świnka podłożona przez Verlenów na Wielkie Kwiatowisko zaczęła polować na lokalne duchy a przedsiębiorcza Ludmiła z pobliskiego hotelu zorganizowała dziennikarza Paktu i okazję do zarobienia. Karolinus i Elena przebili się przez problematycznego dziennikarza Paktu, zwabili świnkę do pojazdu i uśpili oraz podrzucili ją (rękami żołnierzy) do Verlenlandu. A Strzała jest naprawiana.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * zdaniem Verlenów, odpowiednio pomszczona za Karolinusa. Nic nie musi robić w sprawie tego co on robił do tej pory.


### Karolinka, nieokiełznana świnka

* **uid:** 230419-karolinka-nieokielznana-swinka, _numer względny_: 7
* **daty:** 0095-07-19 - 0095-07-23
* **obecni:** Aleksander Samszar, Apollo Verlen, Arianna Verlen, Elena Verlen, Fantazjusz Verlen, Marcinozaur Verlen, Tymek Samszar, Ula Blakenbauer, Viorika Verlen

Streszczenie:

Zaprojektowano świnkę Karolinkę do pomszczenia Vioriki. Aria, Elena i Viorika ruszyły do Powiatu radząc sobie z figlarnym glukszwajnem. Po drodze Elena rozbiła vana w menhir, wintegrowała niewinnego Samszara w menhir, porzuciły tam świnkę i dotarły do Siewczyna. Tam Elena próbowała być słodka i współpracować z Samszarem. O dziwo, udało się przekonać Samszara by pozwolił Verlenkom zmierzyć się ze Strażnikiem. W wyniku Paradoksów, 'potwór został porwany' i uciekł do Verlenlandu a Elenie po raz pierwszy uruchomiła się moc Esuriit.

Aktor w Opowieści:

* Dokonanie:
    * musztrowała Karolinkę by zająć czas; przekonała Zespół by współpracować z Samszarami. Bardzo nie chce być na operacji 'mścimy Viorikę', ale Marcinozaur... Taktyk i siła ognia zespołu.


### Strasznołabędź atakuje granicę

* **uid:** 230502-strasznolabedz-atakuje-granice, _numer względny_: 6
* **daty:** 0095-07-17 - 0095-07-19
* **obecni:** Elena Verlen, Maks Samszar, Marcinozaur Verlen, Szymon Kapeć, Ula Blakenbauer, Viorika Verlen

Streszczenie:

Marcinozaurowi uciekł łabędź (Ula miała Paradoks, Marcinozaur ją kryje). Viorika z Eleną ruszyły go przechwycić w samszarskim Forcie Tawalizer. Chciały wbić się dyskretnie, ale Maks Samszar ma za dobrych ludzi. Gdy rozbroili Viorikę, łabędź się ruszył. Zła współpraca między verlenlandczykami i samszarowcami doprowadziła do tego, że łabądź zabił jednego z samszarowców i PRAWIE uciekł - Elena go jednak zatrzymała uszkadzając poważnie Lancera (i kończąc ranna). Cholernie ryzykowny ruch. Viorika zestrzeliła łabędzia i potem współpracuje z Maksem by problem się nie pojawił.

Aktor w Opowieści:

* Dokonanie:
    * wiedząc, że Marcinozaurowi uciekł łabędź, zdecydowała się mu pomóc i wziąć Elenę (ćwiczenia). Nie udało jej się tego zrobić dyskretnie. Używa 'cuteness' Eleny jako zasobu do przekonania Maksa. Gdy musiała oddać sprzęt, zaufała swoim ludziom (planom) i Elenie. Współpracuje z Maksem po wszystkim, daje mu dźwiedzia konsultanta bezpieczeństwa.


### Dźwiedzie polują na ser

* **uid:** 230412-dzwiedzie-poluja-na-ser, _numer względny_: 5
* **daty:** 0095-07-12 - 0095-07-14
* **obecni:** Apollo Verlen, Arianna Verlen, Elena Verlen, Marcinozaur Verlen, Ula Blakenbauer, Viorika Verlen

Streszczenie:

Koszarów Chłopięcy ma problem - Apollo i Marcinozaur się kłócą o płaszczkę Blakenbauerów. Więc przybywa Arianna i Viorika na prośbę Apollo. Nie tylko łagodzą sprawę i poznają prawdę (chodzi o honor Eleny i by pokazać wartość Blakenbauerów Verlenom i vice versa) ale jeszcze robią wyzwanie między Ulą Blakenbauer i Verlenami. Okazuje się, że Ula jest niesamowicie silna, ale taktyka Vioriki, dźwiedzie Verlenów i umiejętności ścigaczowania Arianny (w co nadal nikt nie wierzy bo ona nie umie pilotować XD) wygrały konflikt bezkrwawo. Poszukiwania pochodzenia Szeptomandry trwają, zajmuje się tym Marcinozaur i Ula.

Aktor w Opowieści:

* Dokonanie:
    * wydobyła od Apollo, co się działo z Eleną. Potem skłoniła Ulę do wyzwania Verlenów a nie robienia losowych płaszczek. Opracowała plan z dźwiedziami, by całkowicie uniknąć walki. Grand Strategist, zostawiła działanie Ariannie.


### Niepotrzebny ratunek Mai

* **uid:** 230328-niepotrzebny-ratunek-mai, _numer względny_: 4
* **daty:** 0095-06-30 - 0095-07-02
* **obecni:** AJA Szybka Strzała, Apollo Verlen, Bonifacy Samszar, Fiona Szarstasz, Karolinus Samszar, Maja Samszar, Romeo Verlen, Viorika Verlen

Streszczenie:

Tydzień po ucieczce z Verlenlandu, Maja, kuzynka Karolinusa, zostaje porwana. Karolinus próbuje zdobyć informacje i nagrania z porwania. Odkrywa, że Maja miała kontakt z Romeo Verlenem, a jej rodzice kasują informacje o nieeleganckim zachowaniu córki. Karolinus łączy się z Romeem, który twierdzi, że Maja jest bezpieczna w mieście VirtuFortis (i ogólnie jest OK). Karolinus wyrusza tam ze swoim zespołem na pokładzie Szybkiej Strzały. 

Niestety, podczas Paradoksu udało się przypadkowo Karolinusowi zaatakować duchami o kształcie Mai Verlenland. Docierają w końcu do VirtuFortis i Karolinus kontaktuje się z Mają. Maja twierdzi, że jest porwana, ale Fiona wyczuwa, że Maja ściemnia. Okazuje się, że Maja uciekła z domu i przyjechała na turniej Supreme Missionforce, żeby zmierzyć się z Romeem. Karolinus przekonuje Maję, żeby wzięła udział w turnieju, a on odbierze ją po tygodniu. Reputacja Karolinusa wzrasta wśród Verlenów, ale spada wśród Samszarów. Fiona zostaje uważana za zły wpływ na Karolinusa.

Aktor w Opowieści:

* Dokonanie:
    * uratowała Maję Samszar po jej spotkaniu z potworem oraz gdy Karolinus rozpoczął "inwazję efemerycznych Maj" wymanewrowała je na potwora i obroniła VirtuFortis.


### Wiktoriata

* **uid:** 210306-wiktoriata, _numer względny_: 3
* **daty:** 0093-12-19 - 0093-12-28
* **obecni:** Apollo Verlen, Lucjusz Blakenbauer, Przemysław Czapurt, Viorika Verlen, Wiktor Blakenbauer

Streszczenie:

Po przegranym pojedynku na oddziały z Apollem, Viorika trafia do papierologii w Poniewierzy. Tam dowiaduje się, że Blakenbauerowie chyba porywają ludzi. Współpracując z Lucjuszem Blakenbauerem, odkrywa prawdę - to psychoza wywołana przez narkotyki bojowe sprzedawane Verlenom przez Blakenbauerów. Przypadkowo z Lucjuszem rozbija siatkę szpiegowską Blakenbauerów na terenie Verlenów. Plus, zalicza początek romansu z Lucjuszem.

Aktor w Opowieści:

* Dokonanie:
    * nieświadomie rozwaliła plan sił specjalnych Blakenbauerów oraz wraz z Lucjuszem (którego ściągnęła) uratowała przed psychotycznymi żołnierzami Verlenów grupę cywili Blakenbauerów i Verlenów.
* Progresja:
    * zneutralizowała tajną siatkę szpiegowsko-infiltracyjną Blakenbauerów w formie wił, wprowadzając własne, czyste, z innego źródła. Dzięki, Lucjusz :D. Za to dostała pochwałę z kontrwywiadu.
    * po zniszczeniu planu z wiłami uznana przez Blakenbauerów za "mistrzynię szpiegów". She is the spymistress, kontrwywiad. Udaje niewinną i słodką, ale w rzeczywistości jej umysł stoi za dużą ilością kontr-planów.
    * jakieś pół roku po tym wydarzeniu została parą z Lucjuszem Blakenbauerem.


### Umierająca farma biovatów

* **uid:** 210302-umierajaca-farma-biovatow, _numer względny_: 2
* **daty:** 0093-08-17 - 0093-08-19
* **obecni:** Frezja Amanit, Lucjusz Blakenbauer, Selena Walecznik, Viorika Verlen

Streszczenie:

Lucjusz Blakenbauer nie chciał dopuścić do śmierci jednej biovatowej farmy. Potrzebował vitae. Zaatakował dyskretnie ród Verlen, pobierając vitae z żołnierzy w Wremłowie. Viorika była akurat na wakacjach, zobaczyła anomalię w sentisieci i odparła ruchy Lucjusza; używając kombinacji sentisieci i taktyki zmusiła go do kapitulacji. Może zabrać to co zdobył, ale musi przekazać zapas języcznika i jej wisi coś małego.

Aktor w Opowieści:

* Dokonanie:
    * Sentisiecią znalazła obecność Lucjusza Blakenbauera i jego podłych planów we Wremłowie a taktyką i umiejętnościami dowódczymi złapała wszystkie jego płaszczki - nawet te, które były przeznaczone do prześlizgnięcia się.


### Sentiobsesja

* **uid:** 210224-sentiobsesja, _numer względny_: 1
* **daty:** 0092-10-01 - 0092-10-04
* **obecni:** Apollo Verlen, Arianna Verlen, Lucjusz Blakenbauer, Milena Blakenbauer, Mścigrom Verlen, Przemysław Czapurt, Viorika Verlen

Streszczenie:

Apollo Verlen, sam w Holdzie Bastion był jedynym sentitienem. Gdy uderzyły siły Mileny Blakenbauer, wściekł się i wpadł w sentiobsesję - chce atakować Blakenbauerów. Na szczęście na miejsce przybyły Arianna i Viorika, które rozwiązały sprawę, odkryły sekret Apolla (3 kochanki) i ściągnęły go pułapką, po czym... pokonały i przywróciły. Aha, potem odzyskały brakujący oddział kosztem śpiewu i tańca Apolla przed Mileną Blakenbauer.

Aktor w Opowieści:

* Dokonanie:
    * 17 lat, zwana "Wirek". Umysł taktyczny i bardzo mocno powiązany z sentisiecią Verlen. Przejęła kontrolę nad sentisiecią i bez chwili wahania uderzyła nią w Apollo by wyrwać go z sentisieci. Mniej społeczna niż Arianna, bardziej intelektualna.
* Progresja:
    * chwała jej planom; już w tak młodym wieku uznawana za świetnego stratega znajdującego słabe punkty u przeciwnika.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 13, @: 0111-08-30
    1. Primus    : 13, @: 0111-08-30
        1. Sektor Astoriański    : 13, @: 0111-08-30
            1. Astoria    : 13, @: 0111-08-30
                1. Sojusz Letejski    : 13, @: 0111-08-30
                    1. Aurum    : 13, @: 0111-08-30
                        1. Powiat Samszar    : 2, @: 0095-07-23
                            1. Fort Tawalizer    : 1, @: 0095-07-19
                                1. Centrum Miasta (C)    : 1, @: 0095-07-19
                                    1. Luksusowa dzielnica mieszkalna    : 1, @: 0095-07-19
                                        1. Jezioro Fortowe    : 1, @: 0095-07-19
                                1. Dzielnica mieszkaniowa (S)    : 1, @: 0095-07-19
                                    1. Domy mieszkalne    : 1, @: 0095-07-19
                                    1. Sklepy i centra handlowe    : 1, @: 0095-07-19
                                    1. Szkoły i placówki oświatowe    : 1, @: 0095-07-19
                                1. Obserwatorium Potworów i Fort (E)    : 1, @: 0095-07-19
                                    1. Koszary garnizonu    : 1, @: 0095-07-19
                                1. Wzgórza Potworów (N)    : 1, @: 0095-07-19
                                    1. Baterie defensywne    : 1, @: 0095-07-19
                                    1. Farmy kóz    : 1, @: 0095-07-19
                                    1. Obserwatoria potworów    : 1, @: 0095-07-19
                            1. Siewczyn    : 1, @: 0095-07-23
                                1. Północne obrzeża    : 1, @: 0095-07-23
                                    1. Spichlerz Jedności    : 1, @: 0095-07-23
                            1. Wielkie Kwiatowisko    : 1, @: 0095-07-23
                                1. Menhir Centralny    : 1, @: 0095-07-23
                        1. Sentipustkowie Pierwotne    : 1, @: 0092-10-04
                        1. Świat Dżungli    : 1, @: 0092-10-04
                            1. Ostropnącz    : 1, @: 0092-10-04
                        1. Verlenland    : 12, @: 0111-08-30
                            1. Arachnoziem    : 2, @: 0095-08-09
                                1. Bar Łeb Jaszczura    : 1, @: 0095-08-05
                                1. Garnizon    : 1, @: 0095-08-09
                                1. Kopalnia    : 2, @: 0095-08-09
                                1. Stacja Pociągu Maglev    : 1, @: 0095-08-09
                            1. Hold Bastion    : 1, @: 0092-10-04
                                1. Barbakan    : 1, @: 0092-10-04
                                1. Karcer    : 1, @: 0092-10-04
                                1. Karczma Szczur    : 1, @: 0092-10-04
                            1. Hold Karaan, obrzeża    : 1, @: 0111-08-24
                                1. Lądowisko    : 1, @: 0111-08-24
                            1. Hold Karaan    : 2, @: 0111-08-30
                                1. Barbakan Centralny    : 1, @: 0111-08-24
                                1. Krypta Plugastwa    : 2, @: 0111-08-30
                                1. Szpital Centralny    : 1, @: 0111-08-30
                            1. Koszarów Chłopięcy, okolice    : 1, @: 0095-07-14
                                1. Las Wszystkich Niedźwiedzi    : 1, @: 0095-07-14
                                1. Skały Koszarowe    : 1, @: 0095-07-14
                            1. Koszarów Chłopięcy    : 1, @: 0095-07-14
                            1. Mikrast    : 1, @: 0097-09-05
                            1. Poniewierz, obrzeża    : 1, @: 0097-01-22
                                1. Jaskinie Poniewierskie    : 1, @: 0097-01-22
                            1. Poniewierz, północ    : 1, @: 0097-01-22
                                1. Osiedle Nadziei    : 1, @: 0097-01-22
                            1. Poniewierz    : 4, @: 0111-08-30
                                1. Archiwum Poniewierskie    : 1, @: 0093-12-28
                                1. Arena    : 1, @: 0097-09-05
                                1. Dom Uciech Wszelakich    : 2, @: 0097-01-22
                                1. Garnizon    : 1, @: 0093-12-28
                                1. Magazyny Anomalii    : 1, @: 0093-12-28
                                1. Zamek Gościnny    : 3, @: 0111-08-30
                            1. Skałkowa Myśl    : 1, @: 0097-09-05
                            1. Trójkąt Chaosu    : 1, @: 0093-12-28
                            1. VirtuFortis    : 2, @: 0095-07-23
                                1. Akademia VR Aegis    : 1, @: 0095-07-02
                                1. Bastion Przyrzeczny    : 1, @: 0095-07-02
                                1. Bazarek Lokalny    : 1, @: 0095-07-02
                                1. Stadion Sportowy    : 1, @: 0095-07-02
                                1. Wielki Plac Miraży    : 2, @: 0095-07-23
                            1. Wremłowo    : 1, @: 0093-08-19
                                1. rachityczny lasek z jaskiniami    : 1, @: 0093-08-19

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 9 | ((210210-milosc-w-rodzie-verlen; 210224-sentiobsesja; 210311-studenci-u-verlenow; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230426-mscizab-zabojca-arachnoziemskich-jaszczurow; 230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Apollo Verlen        | 7 | ((210210-milosc-w-rodzie-verlen; 210224-sentiobsesja; 210306-wiktoriata; 210311-studenci-u-verlenow; 230328-niepotrzebny-ratunek-mai; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka)) |
| Elena Verlen         | 7 | ((210210-milosc-w-rodzie-verlen; 210311-studenci-u-verlenow; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230502-strasznolabedz-atakuje-granice)) |
| Lucjusz Blakenbauer  | 4 | ((210224-sentiobsesja; 210302-umierajaca-farma-biovatow; 210306-wiktoriata; 210324-lustrzane-odbicie-eleny)) |
| Romeo Verlen         | 4 | ((210210-milosc-w-rodzie-verlen; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen; 230328-niepotrzebny-ratunek-mai)) |
| Ula Blakenbauer      | 4 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230502-strasznolabedz-atakuje-granice; 230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Marcinozaur Verlen   | 3 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230502-strasznolabedz-atakuje-granice)) |
| Przemysław Czapurt   | 3 | ((210224-sentiobsesja; 210306-wiktoriata; 210324-lustrzane-odbicie-eleny)) |
| Dariusz Blakenbauer  | 2 | ((210311-studenci-u-verlenow; 210331-elena-z-rodu-verlen)) |
| Maja Samszar         | 2 | ((210311-studenci-u-verlenow; 230328-niepotrzebny-ratunek-mai)) |
| Michał Perikas       | 2 | ((210311-studenci-u-verlenow; 210324-lustrzane-odbicie-eleny)) |
| Mściząb Verlen       | 2 | ((230426-mscizab-zabojca-arachnoziemskich-jaszczurow; 230524-ekologia-jaszczurow-w-arachnoziemie)) |
| AJA Szybka Strzała   | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Aleksander Samszar   | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Atraksjusz Verlen    | 1 | ((230426-mscizab-zabojca-arachnoziemskich-jaszczurow)) |
| Bonifacy Samszar     | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Brunhilda Verlen     | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Emmanuelle Gęsiawiec | 1 | ((230426-mscizab-zabojca-arachnoziemskich-jaszczurow)) |
| Fabian Rzelicki      | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Fantazjusz Verlen    | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Fiona Szarstasz      | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Franz Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Frezja Amanit        | 1 | ((210302-umierajaca-farma-biovatow)) |
| Karolinus Samszar    | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Krucjusz Verlen      | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Krystian Blakenbauer | 1 | ((210331-elena-z-rodu-verlen)) |
| Maks Samszar         | 1 | ((230502-strasznolabedz-atakuje-granice)) |
| Marcel Biekakis      | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Milena Blakenbauer   | 1 | ((210224-sentiobsesja)) |
| Mścigrom Verlen      | 1 | ((210224-sentiobsesja)) |
| Rafał Perikas        | 1 | ((210311-studenci-u-verlenow)) |
| Rufus Samszar        | 1 | ((210311-studenci-u-verlenow)) |
| Selena Walecznik     | 1 | ((210302-umierajaca-farma-biovatow)) |
| Seraf Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Sylwia Perikas       | 1 | ((210311-studenci-u-verlenow)) |
| Szymon Kapeć         | 1 | ((230502-strasznolabedz-atakuje-granice)) |
| Tymek Samszar        | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Wiktor Blakenbauer   | 1 | ((210306-wiktoriata)) |