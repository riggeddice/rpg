# Celina Lertys
## Identyfikator

Id: 9999-celina-lertys

## Sekcja Opowieści

### Wojna o Arkologię Nativis - konsolidacja sił

* **uid:** 230628-wojna-o-arkologie-nativis-konsolidacja-sil, _numer względny_: 7
* **daty:** 0093-03-27 - 0093-03-28
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Eustachy Korkoran, Izabella Saviripatel, Karol Lertys, Laurencjusz Kidiron, Marcel Draglin, Ralf Tapszecz, Tobiasz Lobrak, Tymon Korkoran

Streszczenie:

W Nativis panuje wojna domowa - Laurencjusz i Tymon próbują przejąć władzę jako 'nowi lepsi Kidiron + Korkoran'. Eustachy i Ardilla konsolidują lojalistów arkologii - Lertysów, Draglina, Szczury - tworząc potężną siłę pro-Rafał Kidiron. Gdy Laurencjusz i Tymon wspomagani przez Lobraka próbują Eustachego przekonać do współpracy, Eustachy odrzuca. Oddziały Inferni z jakiegoś powodu krzywdzą cywili (co dziwi Eustachego i martwi Ardillę) i zbrodnie są dobrze nagłośnione, spadając na ręce Eustachego. Farighanowie są odparci. Szczury są złamane. I wiemy, że L+T współpracowali z Infiltratorem...

Aktor w Opowieści:

* Dokonanie:
    * czuje się zdradzona przez Ardillę i nie przyjmuje jej argumentów - Rafał Kidiron MUSI zostać odsunięty od władzy. Dziadek przekonał ją, że muszą pomóc Ardilli, ale Celina rozerwała przyjaźń z Ardillą.
* Progresja:
    * wspiera Bartłomieja Korkorana i tylko jego z rodziny Korkoranów. Wszyscy inni są jej zdaniem lojalistami Kidirona.


### Zdrada rozrywająca arkologię

* **uid:** 230329-zdrada-rozrywajaca-arkologie, _numer względny_: 6
* **daty:** 0093-03-14 - 0093-03-16
* **obecni:** Amelia Sarkaldir, Ardilla Korkoran, BIA Prometeus, Celina Lertys, Eustachy Korkoran, Ewelina Paroknis, Feliks Kidiron, Franciszek Pietraszczyk, Iwona Paroknis, Jonasz Paroknis, Kalia Awiter, Katarzyna Falernik, Małgorzata Maratelus, OO Infernia, Rafał Kidiron, Ralf Tapszecz, Szczepan Falernik, Wojciech Grzebawron

Streszczenie:

Nastolatka miała pamiętnik czytany przez macochę. Ojciec to ignorował. Nastolatka napisała w pamiętniku, że ojciec zdradza macochę z ekspertką hydrologii. Zrobił się shitstorm który doprowadził do poważnych problemów - m.in. macocha (dyrektor skrzydła medycznego) zaczęła robić błędy co wykryła Celina. Kidiron podejrzewa spisek - ktoś próbuje skłócić ważne rodziny, a także shackować Prometeusa (BIA arkologii). Rozmowa z ich eks-piratami kończy się tym, że to nie oni - za tym stoi wyspecjalizowana siła z innej arkologii lub noktianie. 

Ślady prowadzą do pamiętnika nastolatki, który shackowany przez ekspertkę Inferni (i upubliczniony przez problemy na linii Infernia - Prometeus) pokazują, że najpewniej nastolatka za wszystkim stoi sama. Uratowana przez tajemniczego maga Interis przed samobójstwem nastolatka przyznała na czym polegał problem i ujawnia, że pomaga jej Feliks Kidiron, najmłodszy z rodu i jej jedyny przyjaciel. Czyli to nie spisek. Ale jak to teraz naprawić?

Aktor w Opowieści:

* Dokonanie:
    * ostrzegła Ardillę, że mechanizmy medyczne pilnujące wujka mają błędne odczyty. Pilnuje, by NIKT nie dostał się do wujka i nie zrobił mu krzywdy.


### Wyłączone generatory Memoriam Inferni

* **uid:** 230201-wylaczone-generatory-memoriam-inferni, _numer względny_: 5
* **daty:** 0093-02-10 - 0093-02-12
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Ernest Puszczowiec, Eustachy Korkoran, Jan Lertys, Kalia Awiter, Kalista Luminis, Kratos Coruscatis, OO Infernia, Rafał Kidiron, Ralf Tapszecz, Tymon Korkoran

Streszczenie:

Eustachy przebudzony przez Infernię po burzy piaskowej poszedł za jej podszeptami i wyłączył generatory Memoriam. Infernia go Dotknęła - jest to anomalia Interis uwięziona w mechanicznej skorupie. Tylko Eustachy może Infernią sterować. Więc gdy pojawiła się misja pomocy bazie noktiańskiej Coruscatis, Eustachy musiał lecieć. Kidiron chciał aresztowania Kallisty Luminis a Wujek chciał pomocy noktianom. Okazało się, że na miejscu sformował się amalgamoid trianai i Infernia została ostrzelana; acz odpowiedziała ogniem. Zespół pomógł i ewakuował noktian i zdestabilizował reaktor fuzyjny by wysadzić wszystko. Pod wpływem Ardilli i Celiny Eustachy nie oddał noktian Kidironowi a wysadził ich w bezpiecznym miejscu. Kolejny dowód, że arkologia Nativis wykorzystuje żywność jako broń, co jest niezgodne z wszelkimi konwencjami.

Aktor w Opowieści:

* Dokonanie:
    * aktorstwem spowolniła amalgamoid trianai przed atakiem na Infernię; potem ostro postawiła się planom Kidirona (Tymon && Eustachy) i skłoniła Eustachego by jednak pomógł noktianom. Pomogła noktianom i ostrzegła Kallistę, że chcą ją złapać. Zbiera dane o infekcji żywności przez trianai.
* Progresja:
    * uznana za przyjaciółkę noktian z Coruscatis na Neikatis. Coruscatis jej tego nie zapomną.
    * straciła jakiekolwiek uczucia wobec Eustachego. Eustachy okazał się być innym człowiekiem niż wierzyła.


### Osy w CES Purdont

* **uid:** 220817-osy-w-ces-purdont, _numer względny_: 4
* **daty:** 0093-01-23 - 0093-01-24
* **obecni:** Ardilla Korkoran, Celina Lertys, Eustachy Korkoran, Jan Lertys, Joachim Puriur, Kamil Wraczok, Kordian Olgator, VN Karglondel

Streszczenie:

Eustachy przejął dowodzenie nad obroną CES i sealował część przejść. Postawili serię pułapek i defensyw - i wtedy poza Trianai pojawili się komandosi z zewnątrz, z łazika (crawlera) Karglondel. Czegoś szukają pod bazą. Magia Eustachego naprowadziła ich prosto na rdzeń Trianai i Ardilla wykorzystała komandosów jako dywersję dla Trianai gdy ratowała Darię przed ostateczną transformacją w istotę Trianai (antidotum złożyła Celina). Udało im się przetrwać do przybycia Inferni i uratowali wszystkich kto był ważny. Komandosi nie przeżyli ;-).

Aktor w Opowieści:

* Dokonanie:
    * z kiepskiego systemu medycznego zsyntetyzowała antidotum mające uratować Darię przed nędznym losem.


### Infernia taksówką dla Lycoris

* **uid:** 220720-infernia-taksowka-dla-lycoris, _numer względny_: 3
* **daty:** 0093-01-20 - 0093-01-22
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Czesław Żuczek, Daria Raizis, Eustachy Korkoran, Jan Lertys, Kamil Wraczok, Lycoris Kidiron, OO Infernia, Rafał Kidiron, Wiktor Turkalis

Streszczenie:

Plaga Trianai uderzyła w CES Purdont; Infernia p.d. Bartłomieja Korkorana ruszyła jako taksówka dla Lycoris ale młodzi trafili do Purdont a Infernia szuka Lycoris i Wiktora. Gdy młodzi doszli do Plagi i przebili się do łączności to poinformowali Nativis i wujka o ryzyku. Dostali skonfliktowane polecenia, ale mają przetrwać. Jednak dla części z nich to nieakceptowalne, bo nie zostawią zarażonej koleżanki na pożarcie Pladze Trianai mimo znajdującego się gdzieś w cieniu Inteligentnego Koordynatora Trianai...

Aktor w Opowieści:

* Dokonanie:
    * złotoskóra o błękitnych elektrycznych włosach podkochująca się w Eustachym (science / bio officer + medical) Inferni. Dość poważna, nosi przy sobie kosmetyki do podrywania Eustachego, augmentowana na widzenie rzeczy ukrytych (np. zmian skóry Kamila). Mimo młodego wieku (19) dość kompetentna i umie się opanować.


### Czarne Hełmy i Robaki

* **uid:** 220831-czarne-helmy-i-robaki, _numer względny_: 2
* **daty:** 0092-08-15 - 0092-08-27
* **obecni:** Ardilla Korkoran, Celina Lertys, Eustachy Korkoran, Jan Lertys, Stanisław Uczantor, Tymon Korkoran, Wojciech Czerpń

Streszczenie:

Tymon Korkoran chce pomóc Arkologii Nativis by zniszczyć kult śmierci Robaków. Zespół doszedł do tego że to nie kult śmierci; to grupa radykalnych ludzi uważających że coś jest nie tak z żywnością i robiących badania żywności by do tego dojść. Nevertheless, Eustachy zrobił straszny sabotaż (ranni itp) by ludzie znienawidzili Robaki, Ardilla jedną osobę wyciągnęła z Robaków by nic jej się nie stało i ogólnie Robaki zostały zmiażdżone. Tymon poszedł do góry w hierarchii Kidironów, acz Dziadek i Wujek są przeciw tak ostrym działaniom.

Aktor w Opowieści:

* Dokonanie:
    * doszła do tego, że Robaki robią analizy żywności i dlatego Kidironowie ich tępią..? Jako "ta społeczna" poznała kilka nazwisk i doszła, że to nie kult śmierci.


### Polowanie na szczury w Nativis

* **uid:** 220723-polowanie-na-szczury-w-nativis, _numer względny_: 1
* **daty:** 0087-05-03 - 0087-05-12
* **obecni:** Celina Lertys, Emilia d'Erozja, Jan Lertys, Karol Lertys, Laurencjusz Kidiron, OA Erozja Ego, Tymon Korkoran

Streszczenie:

Młodzi Lertysi - Celina i Janek - próbowali sobie dorobić polując na szczury w Nativis. Skonfliktowali się z Laurencjuszem Kidironem (i go sponiewierali skunksem), ale przez to musieli użyć wiedzy o kanałach serwisowych Nativis. To sprawiło że znaleźli ślady advancera polującego na ich dziadka. I to sprawiło, że poznali prawdę o swoim dziedzictwie - ich dziadek nie jest ich dziadkiem a oni pochodzą z martwej arkologii Aspiria.

Aktor w Opowieści:

* Dokonanie:
    * przekonała Tymona do wzięcia szczurzego zlecenia jako słup, zastawiła pułapkę na Laurencjusza, przeszukała rzeczy niebezpiecznego advancera i wyciągnęła z dziadka info o przeszłości. Aha, uratowała wandali przed dziadkiem strzelającym przez ścianę. A, i jest aspirianką.
* Progresja:
    * ogólna reputacja 'dziwnej' w Nativis; nikt nie chce z nią zadzierać. Wie, że pochodzi z Aspirii.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 7, @: 0093-03-28
    1. Primus    : 7, @: 0093-03-28
        1. Sektor Astoriański    : 7, @: 0093-03-28
            1. Neikatis    : 7, @: 0093-03-28
                1. Dystrykt Glairen    : 7, @: 0093-03-28
                    1. Arkologia Nativis    : 6, @: 0093-03-28
                        1. Poziom 1 - Dolny    : 4, @: 0093-03-28
                            1. Południe    : 1, @: 0093-03-28
                                1. Magazyny    : 1, @: 0093-03-28
                                1. Processing    : 1, @: 0093-03-28
                                1. Stacje Skorpionów    : 1, @: 0093-03-28
                            1. Północ - Stara Arkologia    : 3, @: 0093-03-28
                                1. Blokhaus E    : 2, @: 0093-03-28
                                1. Blokhaus F    : 2, @: 0093-03-28
                                    1. Szczurowisko    : 2, @: 0093-03-28
                                1. Stare Wejście Północne    : 1, @: 0093-03-28
                            1. Wschód    : 1, @: 0093-03-28
                                1. Farmy Wschodnie    : 1, @: 0093-03-28
                            1. Zachód    : 2, @: 0093-03-28
                                1. Centrala Prometeusa    : 1, @: 0093-03-28
                                1. Stare TechBunkry    : 2, @: 0093-03-28
                        1. Poziom 2 - Niższy Środkowy    : 2, @: 0093-03-28
                            1. Południe    : 1, @: 0093-03-28
                                1. Engineering    : 1, @: 0093-03-28
                                1. Power Core    : 1, @: 0093-03-28
                            1. Północ    : 1, @: 0093-03-28
                                1. Bazar i sklepy    : 1, @: 0093-03-28
                            1. Wschód    : 2, @: 0093-03-28
                                1. Centrum Kultury i Rozrywki    : 2, @: 0093-03-28
                                    1. Bar Śrubka z Masła    : 2, @: 0093-03-28
                                    1. Muzeum Kidirona    : 1, @: 0093-03-28
                                    1. Ogrody Gurdacza    : 1, @: 0093-03-28
                                    1. Stacja holosymulacji    : 1, @: 0093-03-28
                            1. Zachód    : 1, @: 0093-03-28
                                1. Koszary Hełmów    : 1, @: 0093-03-28
                                1. Sektor Porządkowy    : 1, @: 0093-03-28
                        1. Poziom 3 - Górny Środkowy    : 2, @: 0093-03-28
                            1. Południe    : 1, @: 0093-03-28
                                1. Life Support    : 1, @: 0093-03-28
                                1. Medical    : 1, @: 0093-03-28
                            1. Wschód    : 2, @: 0093-03-28
                                1. Dzielnica Luksusu    : 2, @: 0093-03-28
                                    1. Ambasadorka Ukojenia    : 1, @: 0093-03-28
                                    1. Ogrody Wiecznej Zieleni    : 2, @: 0093-03-28
                                    1. Stacja holosymulacji    : 1, @: 0093-03-28
                        1. Poziom 4 - Górny    : 1, @: 0093-03-28
                            1. Wschód    : 1, @: 0093-03-28
                                1. Centrala dowodzenia    : 1, @: 0093-03-28
                                1. Obserwatorium Astronomiczne    : 1, @: 0093-03-28
                                1. Radiowęzeł    : 1, @: 0093-03-28
                            1. Zachód    : 1, @: 0093-03-28
                                1. Tereny Sportowe    : 1, @: 0093-03-28
                        1. Poziom 5 - Szczyt    : 1, @: 0093-03-28
                            1. Północ    : 1, @: 0093-03-28
                                1. Port kosmiczny    : 1, @: 0093-03-28
                    1. Arkologia Sarviel, okolice    : 1, @: 0093-02-12
                        1. Krater Coruscatis    : 1, @: 0093-02-12
                    1. CES Purdont, okolice    : 1, @: 0093-01-22
                        1. Wiertło Ekopoezy Delta    : 1, @: 0093-01-22
                    1. CES Purdont    : 2, @: 0093-01-24
                        1. Kafeteria    : 1, @: 0093-01-22
                        1. Laboratorium Ekopoezy    : 1, @: 0093-01-22
                        1. Life Support    : 1, @: 0093-01-22
                        1. System łączności    : 1, @: 0093-01-22

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 6 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Eustachy Korkoran    | 6 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Jan Lertys           | 5 | ((220720-infernia-taksowka-dla-lycoris; 220723-polowanie-na-szczury-w-nativis; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni)) |
| Tymon Korkoran       | 4 | ((220723-polowanie-na-szczury-w-nativis; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Bartłomiej Korkoran  | 3 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| OO Infernia          | 3 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Rafał Kidiron        | 3 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Ralf Tapszecz        | 3 | ((230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Kalia Awiter         | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Kamil Wraczok        | 2 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont)) |
| Karol Lertys         | 2 | ((220723-polowanie-na-szczury-w-nativis; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Laurencjusz Kidiron  | 2 | ((220723-polowanie-na-szczury-w-nativis; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| BIA Prometeus        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Czesław Żuczek       | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Daria Raizis         | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Emilia d'Erozja      | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Franciszek Pietraszczyk | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Izabella Saviripatel | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Joachim Puriur       | 1 | ((220817-osy-w-ces-purdont)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kordian Olgator      | 1 | ((220817-osy-w-ces-purdont)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Lycoris Kidiron      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Małgorzata Maratelus | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Marcel Draglin       | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| OA Erozja Ego        | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Stanisław Uczantor   | 1 | ((220831-czarne-helmy-i-robaki)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Tobiasz Lobrak       | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| VN Karglondel        | 1 | ((220817-osy-w-ces-purdont)) |
| Wiktor Turkalis      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Wojciech Czerpń      | 1 | ((220831-czarne-helmy-i-robaki)) |
| Wojciech Grzebawron  | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |