# Ernest Namertel
## Identyfikator

Id: 9999-ernest-namertel

## Sekcja Opowieści

### Lea, strażniczka lasu

* **uid:** 240305-lea-strazniczka-lasu, _numer względny_: 10
* **daty:** 0111-10-21 - 0111-10-25
* **obecni:** Arkadia Verlen, Bogdan Gwiazdocisz, Ernest Namertel, Karolina Terienak, Keira Amarco d'Namertel, Lea Samszar, Malena Barandis, Marek Samszar, Mariusz Kupieczka, Marysia Sowińska, Michał Klabacz, Triana Porzecznik

Streszczenie:

Marysia zatrzymała Leę przed naruszaniem kulturowego tabu eternijskiego (stroju) i przy okazji dowiedziała się, że Lea próbuje pomóc innym, ale nikt nie może o tym wiedzieć. Tymczasem jej EfemeHorror poranił grupę Rekinów i AMZ współpracujących ze sobą w celu pomocy staremu 'archeologowi' szukającemu prawdy o ofiarach wojny.

Aktor w Opowieści:

* Dokonanie:
    * z przyjemnością spędza czas z Marysią w łóżku; wyjaśnił Marysi problem tabu strojów eternijskich które łamie Lea. Potem powiedział jej, że Lea używa rytuału Krwi...


### Gdy prawnik przyjdzie po Rekiny

* **uid:** 220802-gdy-prawnik-przyjdzie-po-rekiny, _numer względny_: 9
* **daty:** 0111-10-09 - 0111-10-11
* **obecni:** Ernest Namertel, Hipolit Umadek, Marysia Sowińska

Streszczenie:

Marysia w końcu założyła hold na Erneście i się doń wprowadziła. Ernest zerwał z Amelią i plotki dotarły do niej czemu. Tymczasem plany Marsena sprawiają, że Ernest zaczyna być coraz bardziej zły na Mimozę (co Marysia neutralizuje) a Hipolit, prawnik z AMZ, chce by Marysia kontrolowała Rekiny skoro już Enklawa jest eksterytorialna. Albo coś się będzie musiało zmienić.

Aktor w Opowieści:

* Dokonanie:
    * podejrzewa Mimozę o wszystko co najgorsze - uważa, to ONA stoi za Marsenem Gwozdnikiem i Loreną. Pod wpływem Marysi oddał jej temat i zerwał zaręczyny z Amelią.
* Progresja:
    * spodobała mu się Marysia Sowińska BARDZIEJ niż Amelia, póki tu jest. To znaczy, że Marysia go odbija
    * będzie zdawał się na Marysię w sprawach politycznych (+1Vg w takich sytuacjach)
    * zerwał zaręczyny z Amelią, bo Marysia też jest w jego życiu i Amelia nie jest już tą jedyną
    * zacietrzewiony na Mimozę, Marsena i Lorenę. Oni stoją na drodze do lepszego świata i konspirują przeciw niemu.


### Płaszcz ochronny Mimozy

* **uid:** 220222-plaszcz-ochronny-mimozy, _numer względny_: 8
* **daty:** 0111-09-21 - 0111-09-25
* **obecni:** Ernest Namertel, Karolina Terienak, Keira Amarco d'Namertel, Lorena Gwozdnik, Marysia Sowińska, Mimoza Elegancja Diakon

Streszczenie:

Mimoza weszła do akcji - wzięła Lorenę do azylu i ochroniła część kultystek przed Ernestem. Konflikt Ernest - Mimoza się rozpalił, Marysia + Karo poszły się z Mimozą spotkać by zdeeskalować problem. Lorena podsłuchiwała, więc Mimoza straciła twarz. Cóż. Mimoza wynegocjowała przekazanie kultystek Sensacjuszowi, Marysia wynegocjowała by Lorena zrobiła ten akt. ALE W DOMU MIMOZY. Zaczyna się ciekawie...

Aktor w Opowieści:

* Dokonanie:
    * chce ochronić i wyczyścić wszystkich z kralotyzacji; przejmuje kontrolę nad terenem Rekinów. Mimoza staje mu na drodze. Ścierają się w nim impulsy: Esuriit - dobro.
* Progresja:
    * uznał za PERSONALNĄ obrazę i PERSONALNY afront to, że Mimoza zwinęła mu Lorenę. I Ernest nie może dostać swojego aktu. STARCIE z Mimozą Diakon.


### Marysiowa Hestia Rekinów

* **uid:** 220111-marysiowa-hestia-rekinow, _numer względny_: 7
* **daty:** 0111-09-16 - 0111-09-19
* **obecni:** Diana Tevalier, Ernest Namertel, Hestia d'Rekiny, Jeremi Sowiński, Karolina Terienak, Keira Amarco d'Namertel, Liliana Bankierz, Marysia Sowińska

Streszczenie:

Konflikt Liliana - Marysia się zaostrza. Marysia skupia się na wzmocnieniu Hestii i uzyskaniu kontroli nad Rekinami - przejęła Hestię jako sojuszniczkę. Chevaleresse poprosiła Karolinę o uratowanie Melissy i Karo faktycznie pomogła - Ernest wysłał Keirę i odbili Melissę. To sprawiło, że Mimoza Diakon poczuła się zagrożona i weszła do akcji. Jakby tego było mało, Sowińscy wysyłają Jeremiego by opanował kuzynkę.

Aktor w Opowieści:

* Dokonanie:
    * zaplanował, przekonany przez Karo, odbicie Melissy z potencjalnego kultu jako ćwiczenia. Wysłał jedną Keirę.


### Akt, o którym Marysia nie wie

* **uid:** 211228-akt-o-ktorym-marysia-nie-wie, _numer względny_: 6
* **daty:** 0111-09-09 - 0111-09-12
* **obecni:** Daniel Terienak, Ernest Namertel, Karolina Terienak, Keira Amarco d'Namertel, Liliana Bankierz, Lorena Gwozdnik, Marysia Sowińska, Napoleon Bankierz, Rafał Torszecki

Streszczenie:

Torszecki ostrzegł Marysię, że jej akt krąży w Zaczęstwie. Ona to olała. Ale jak Ernest zaczął na niego polować to Marysia się zainteresowała. Zdecydowała się aktem odbić Ernesta Amelii. Zaflirtowała z Ernestem, obiecała mu akt specjalnie dla niego i że odda mu oryginał tego. Karo pokonała Napoleona, znalazła link do aktu który ostatecznie zdobył Torszecki. Daniel dla Karo zbił Torszeckiego i odzyskał akt. Marysia zaflirtowała z Ernestem ;-). Torszecki, który chciał dobrze i chronił Marysię skończył w szpitalu.

Aktor w Opowieści:

* Dokonanie:
    * chciał zdobyć akt Marysi (wysłał Keirę) i może poprosić o akt Amelii artystkę (Lorenę). Ale po rozmowie z Marysią zaczął na nią trochę inaczej patrzeć; został cząstkowo uwodzony. Zainteresował się nią.
* Progresja:
    * zaczął patrzeć na Marysię Sowińską inaczej. Może nie tylko na zimną damę, ale też na interesującą partnerkę w łóżku..? Ma jej akt i mu się bardzo podoba.


### Waśń o ryby w Majkłapcu

* **uid:** 211127-waśń-o-ryby-w-majklapcu, _numer względny_: 5
* **daty:** 0111-08-19 - 0111-08-24
* **obecni:** Arkadia Verlen, Daniel Terienak, Genowefa Krecik, Iwan Zawtrak, Karolina Terienak, Ksenia Kirallen, Paweł Szprotka, Rafał Torszecki, Stella Amakirin

Streszczenie:

Daniel chciał rozwiązać problem w Majkłapcu, gdzie koty poraniły ryby. Okazało się, że to wina mafii, której właściciel kociarni nie chce płacić okupu. Karolina zebrała drużynę, uratowali zatrute wściekłością koty i zaatakowali siedzibę małego oddziałka mafii. Po zdobyciu dowodów (i ucieczce) przekazali temat Ksenii.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * jego nazwiskiem i Eternią podobno straszy się lokalny biznes w Majkłapcu. Robota Karoliny i Daniela.


### Odbudowa według Justyniana

* **uid:** 211123-odbudowa-wedlug-justyniana, _numer względny_: 4
* **daty:** 0111-08-11 - 0111-08-20
* **obecni:** Azalia Sernat d'Namertel, Daniel Terienak, Ernest Namertel, Ignacy Myrczek, Justynian Diakon, Karol Pustak, Karolina Terienak, Marysia Sowińska, Rafał Torszecki

Streszczenie:

Marysia się tymczasowo wprowadziła do Ernesta, by go trochę uspokoić i przekonać do Rekinów. Tymczasem Justynian Diakon przejął kontrolę operacyjną i zaczął odbudowywać Dzielnicę Rekinów po ruinie jaką zrobił tam Wiktor Satarail. Marysia skontaktowała się z Justynianem i przekazała mu plan Azalii d'Namertel jako swój, by odzyskać kontrolę i pozycję. Justynian zaakceptował jej plan i powiedział jej, że Torszeckiego biją. Do tego Marysię odwiedził Pustak jako herold Myrczka - prosi, by Marysia skontaktowała go z Sabiną Kazitan, miłością Myrczka...

Aktor w Opowieści:

* Dokonanie:
    * Marysia przekonała go by dać szansę Rekinom i się jednak jakoś integrował. Na jego prośbę Azalia opracowała rozwiązanie jak odbudować Dzielnicę.
* Progresja:
    * daje szansę Rekinom. Marysia z pomocą Karoliny dały radę go przekonać, że Rekiny nie są tak złe - i Amelia by tego chciała.


### Satarail pomaga Marysi

* **uid:** 211102-satarail-pomaga-marysi, _numer względny_: 3
* **daty:** 0111-08-09 - 0111-08-10
* **obecni:** Ernest Namertel, Karolina Terienak, Lorena Gwozdnik, Marek Samszar, Marysia Sowińska, Rafał Torszecki, Sensacjusz Diakon, Wiktor Satarail

Streszczenie:

Satarail uderzył - zainfekował Owadem który wypełzł z Torszeckiego Samszara (który kiedyś ukradł somnibela Olgi). Zainfekowane Rekiny zaczęły dewastować dzielnicę. Karo skutecznie rozwala owady (i Lorenę), po czym Sensacjusza w glistę medyczną. Marysia samodzielnie leci jako owad wyłączyć Samszara. Torszecki jest rozgrzeszony - "nie był sobą". Ale polowanie na Torszeckiego czas zacząć.

Aktor w Opowieści:

* Dokonanie:
    * nie chciał pomagać w ratowaniu Rekinów, ale zrobił to bo Amelia by to zrobiła. Nadal - nawet jego oddział nie dał rady przebić się przez zarażone Rekiny.


### Wysadzony żywy ścigacz

* **uid:** 210928-wysadzony-zywy-scigacz, _numer względny_: 2
* **daty:** 0111-07-26 - 0111-07-27
* **obecni:** Azalia Sernat d'Namertel, Ernest Namertel, Franek Bulterier, Karolina Terienak, Keira Amarco d'Namertel, Marysia Sowińska, Rafał Torszecki, Żorż d'Namertel

Streszczenie:

Ernest z Eterni okazał się być zupełnie inny niż się wydaje. Lekko naiwny, z paladyńskim zacięciem, chce czynić dobro i integrować Rekiny dookoła Marysi. Chce dokończyć dobre dzieło Amelii. Marysia znalazła z nim linię porozumienia a Ernest x Karolina są jak dwa łyse konie. Marysia przekonała go, by sam nic nie robił - nie rozumie terenu. Niestety, ktoś wysadził żywy ścigacz i zabił ducha z jego arkinu. Ernesta zatrzymała Marysia. Karolinie nie udało się ducha uratować. Dziewczyny zdecydowały, że znajdą mordercę...

Aktor w Opowieści:

* Dokonanie:
    * naiwny wobec ludzi i polityki, ale świetny w walce i taktyce. Ma gdzieś protokoły, chce pomagać. Wierzy, że Marysia i Amelia są "dobre" i zaprzyjaźnił się z Karoliną. Bardzo przyjacielsko traktuje swój arkin. Gdy jego ścigacz (żywy) został wysadzony, prawie uruchomił simulacrum. Marysia przekonała go, by oddał jej śledztwo. Rozpacza w ciszy.
* Progresja:
    * Marysia i Karolina są jedynymi Rekinami jakim ufa i lubi. Inni są niebezpieczni lub tolerowalni.
    * następne 7 dni regeneracji po Skażeniu Esuriit; nie udało mu się uratować Daina.


### Przybycie Rekina z Eterni

* **uid:** 210921-przybycie-rekina-z-eterni, _numer względny_: 1
* **daty:** 0111-07-20 - 0111-07-25
* **obecni:** Amelia Sowińska, Arkadia Verlen, Ernest Namertel, Jolanta Sowińska, Karol Pustak, Karolina Terienak, Lucjan Sowiński, Marysia Sowińska, Nataniel Morlan, Rafał Torszecki, Tomasz Tukan

Streszczenie:

Do Rekinów dołącza tien z Eterni. Marysia dowiaduje się, że to kwestia intryg jej kuzynki, Amelii, próbującej deeskalować problemy na linii Morlan - Jolanta. Dowiedziała się od Amelii, że to nie Amelia stała za morderstwami w Podwiercie, kogoś chroni. Oraz... że Amelia jest zakochana (Amelia nie wie że Marysia wie). Ale na Marysię spadła ochrona Ernesta. Więc... usunęła wszystkie zagrożenia o których pomyślała (Arkadia) i zaczęła działania mające poprawić reputację Eternianina, by Rekiny go przyjęły.

Aktor w Opowieści:

* Dokonanie:
    * rekin z Eterni, kiedyś mentorowany przez Morlana więc niezły w walce; zakochany ze wzajemnością w Amelii Sowińskich (ich związek jest sekretem).


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 9, @: 0111-10-25
    1. Primus    : 9, @: 0111-10-25
        1. Sektor Astoriański    : 9, @: 0111-10-25
            1. Astoria    : 9, @: 0111-10-25
                1. Sojusz Letejski    : 9, @: 0111-10-25
                    1. Szczeliniec    : 9, @: 0111-10-25
                        1. Powiat Pustogorski    : 9, @: 0111-10-25
                            1. Podwiert    : 9, @: 0111-10-25
                                1. Dzielnica Luksusu Rekinów    : 9, @: 0111-10-25
                                    1. Fortyfikacje Rolanda    : 3, @: 0111-09-25
                                    1. Obrzeża Biedy    : 3, @: 0111-09-25
                                        1. Domy Ubóstwa    : 3, @: 0111-09-25
                                        1. Hotel Milord    : 3, @: 0111-09-25
                                        1. Stadion Lotników    : 3, @: 0111-09-25
                                        1. Stajnia Rumaków    : 3, @: 0111-09-25
                                    1. Sektor Brudu i Nudy    : 3, @: 0111-09-25
                                        1. Komputerownia    : 3, @: 0111-09-25
                                        1. Konwerter Magielektryczny    : 3, @: 0111-09-25
                                        1. Magitrownia Pogardy    : 3, @: 0111-09-25
                                        1. Skrytki Czereśniaka    : 3, @: 0111-09-25
                                    1. Serce Luksusu    : 8, @: 0111-10-11
                                        1. Apartamentowce Elity    : 7, @: 0111-10-11
                                        1. Arena Amelii    : 3, @: 0111-09-25
                                        1. Fontanna Królewska    : 3, @: 0111-09-25
                                        1. Kawiarenka Relaks    : 3, @: 0111-09-25
                                        1. Lecznica Rannej Rybki    : 5, @: 0111-09-25
                                1. Las Trzęsawny    : 2, @: 0111-10-25
                            1. Pustogor    : 1, @: 0111-08-10
                                1. Rdzeń    : 1, @: 0111-08-10
                                    1. Szpital Terminuski    : 1, @: 0111-08-10
                            1. Zaczęstwo    : 3, @: 0111-09-25
                                1. Akademia Magii, kampus    : 3, @: 0111-09-25
                                    1. Akademik    : 3, @: 0111-09-25
                                    1. Arena Treningowa    : 3, @: 0111-09-25
                                    1. Las Trzęsawny    : 3, @: 0111-09-25

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 9 | ((210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 220802-gdy-prawnik-przyjdzie-po-rekiny; 240305-lea-strazniczka-lasu)) |
| Karolina Terienak    | 8 | ((210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 240305-lea-strazniczka-lasu)) |
| Keira Amarco d'Namertel | 5 | ((210928-wysadzony-zywy-scigacz; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 240305-lea-strazniczka-lasu)) |
| Rafał Torszecki      | 5 | ((210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Lorena Gwozdnik      | 3 | ((211102-satarail-pomaga-marysi; 211228-akt-o-ktorym-marysia-nie-wie; 220222-plaszcz-ochronny-mimozy)) |
| Arkadia Verlen       | 2 | ((210921-przybycie-rekina-z-eterni; 240305-lea-strazniczka-lasu)) |
| Azalia Sernat d'Namertel | 2 | ((210928-wysadzony-zywy-scigacz; 211123-odbudowa-wedlug-justyniana)) |
| Daniel Terienak      | 2 | ((211123-odbudowa-wedlug-justyniana; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Karol Pustak         | 2 | ((210921-przybycie-rekina-z-eterni; 211123-odbudowa-wedlug-justyniana)) |
| Liliana Bankierz     | 2 | ((211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow)) |
| Marek Samszar        | 2 | ((211102-satarail-pomaga-marysi; 240305-lea-strazniczka-lasu)) |
| Amelia Sowińska      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Bogdan Gwiazdocisz   | 1 | ((240305-lea-strazniczka-lasu)) |
| Diana Tevalier       | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Franek Bulterier     | 1 | ((210928-wysadzony-zywy-scigacz)) |
| Hestia d'Rekiny      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Hipolit Umadek       | 1 | ((220802-gdy-prawnik-przyjdzie-po-rekiny)) |
| Ignacy Myrczek       | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Jeremi Sowiński      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Jolanta Sowińska     | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Justynian Diakon     | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Lea Samszar          | 1 | ((240305-lea-strazniczka-lasu)) |
| Lucjan Sowiński      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Malena Barandis      | 1 | ((240305-lea-strazniczka-lasu)) |
| Mariusz Kupieczka    | 1 | ((240305-lea-strazniczka-lasu)) |
| Michał Klabacz       | 1 | ((240305-lea-strazniczka-lasu)) |
| Mimoza Elegancja Diakon | 1 | ((220222-plaszcz-ochronny-mimozy)) |
| Napoleon Bankierz    | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Nataniel Morlan      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Sensacjusz Diakon    | 1 | ((211102-satarail-pomaga-marysi)) |
| Tomasz Tukan         | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Triana Porzecznik    | 1 | ((240305-lea-strazniczka-lasu)) |
| Wiktor Satarail      | 1 | ((211102-satarail-pomaga-marysi)) |
| Żorż d'Namertel      | 1 | ((210928-wysadzony-zywy-scigacz)) |