# Sabina Kazitan
## Identyfikator

Id: 2004-sabina-kazitan

## Sekcja Opowieści

### Admirał Gruby Wieprz

* **uid:** 211114-admiral-gruby-wieprz, _numer względny_: 13
* **daty:** 0112-02-15 - 0112-02-20
* **obecni:** Adalbert Brześniak, Izabela Zarantel, Klaudia Stryk, Rafael Galwarn, Roman Panracz, Rzieza d'K1, Sabina Kazitan

Streszczenie:

Klaudia na K1 dostała dziwny SOS. Okazało się, że pochodzi z K1 - z multivirtu. Virtsystem gry Gwiezdny Podbój uzyskał życie. Klaudia zmontowała akcję ratunkową i z Adalbertem (współpracujący z Sabiną Kazitan), Izą i Romanem zaczęli przenosić TAI do ewakuacji. Niestety, Klaudia przypadkiem Skaziła Połączony Rdzeń na K1 (śledztwo i poważne zarzuty) a Iza chcąc dać Wieprzowi głos trafiła na celownik Rziezy, który ją ośmieszył a Wieprza pokazał jako żywą TAI - psychopatę...

Aktor w Opowieści:

* Dokonanie:
    * współpracuje z Adalbertem Brześniakiem; daje teren i energię by móc chronić TAI. M.in. tam znajduje się Neita? Adalbert wysłał jej trochę wojska / najemników. Jej motywy są niezrozumiałe i nieznane - ruch polityczny? Ale frakcja pro-wolne TAI to akceptuje.
* Progresja:
    * zbudowała niepewność w Aurum; czy współpracuje z Orbiterem? Jest agentką? Można z nią walczyć / ją zdominować czy to niebezpieczne?


### Potencjalnie eksterytorialny seksbot

* **uid:** 210406-potencjalnie-eksterytorialny-seksbot, _numer względny_: 12
* **daty:** 0111-04-27 - 0111-04-28
* **obecni:** Barnaba Burgacz, Feliks Keksik, Franek Bulterier, Henryk Wkrąż, Julia Kardolin, Lorena Gwozdnik, Marysia Sowińska, Rupert Mysiokornik, Sabina Kazitan, Triana Porzecznik

Streszczenie:

Sabina Kazitan zostawiła za sobą plan mający skonfliktować Rekiny na poziomie Podwiertu, co miałoby kaskadować na relacje rodów w Aurum. Plan zawierał Skażonego seksbota, którego Rekiny miały same zrobić. Podszepnęła też odpowiednie słówka odpowiednim osobom. Jednak gdy podatne Rekiny chciały iść za tym planem, gdy szabrowały ze złomowiska głowę seksbota to zatrzymał je Zespół. Skończyło się na tym, że seksbota nie będzie - Marysia im absolutnie zakazała cokolwiek robić, nawet, jeśli nie znała planu Sabiny.

Aktor w Opowieści:

* Dokonanie:
    * wielka nieobecna sesji, która jednak zaplanowała rytuał zrobienie seksbota waśni typu 'Eris' wykorzystując eksterytorialność dzielnicy Rekinów w Podwiercie i swoją mroczną magię. Jako prezent ;-). Cel: spowodowanie wojen między rodami w Aurum przez to, jak młodzi magowie się zachowują w Podwiercie. Rok temu - miała starcie z Marysią Sowińską.


### Narodziny paladynki Saitaera

* **uid:** 201011-narodziny-paladynki-saitaera, _numer względny_: 11
* **daty:** 0110-10-12 - 0110-10-13
* **obecni:** Agaton Ociegor, Aleksander Muniakiewicz, Gabriel Ursus, Minerwa Metalia, Pięknotka Diakon, Sabina Kazitan, Saitaer

Streszczenie:

Pięknotka wyprawiła Sabinę Kazitan do Aurum po akcji Gabriela, ale wymuszając na obu stronach by zaprzestały wojny ze sobą. Jako, że zaczął się atak na kompleks Grzymościa, Alan i Gabriel zostali tam przekierowani - a Pięknotka poszła z Minerwą do parku rozrywki Janor by zakończyć tą sprawę. Niestety, grzymościowiec Ociegor złapał Pięknotkę chcąc ją "wyleczyć" z kralotycznej siły. Pięknotka wezwała Cienia i ów prawie zabił Ociegora. Walcząc z Cieniem, Pięknotka umarła - i Saitaer ją przywrócił jako swoją paladynkę.

Aktor w Opowieści:

* Dokonanie:
    * przekonała Pięknotkę, by ta pozwoliła jej odejść. Zarzuciła wszelkie vendetty i zostawiła Pustogor za sobą. Czas wrócić do domu, do Aurum.
* Progresja:
    * wraca do Aurum. Jest wolna od Pustogoru.
    * zrzeka się vendetty i jakichkolwiek ofensywnych ruchów wobec Gabriela, Lilii itp. Plus, musi informować Pięknotkę o istotnych rzeczach z Aurum dotyczących Pustogoru.


### Dezinhibitor dla Sabiny

* **uid:** 201006-dezinhibitor-dla-sabiny, _numer względny_: 10
* **daty:** 0110-10-07 - 0110-10-09
* **obecni:** Aranea Diakon, Gabriel Ursus, Ignacy Myrczek, Justynian Diakon, Karolina Erenit, Laura Tesinik, Lorena Gwozdnik, Napoleon Bankierz, Rafał Kumczek, Sabina Kazitan

Streszczenie:

Zanim Sabina wróci do Aurum, Rekiny Paladyni chcieli ją przyskrzynić - podać jej dezinhibitor w towarzystwie Myrczka i to nagrać, z nadzieją, że się ją zamknie. Gabriel i Laura wykryli intrygę i zatrzymali ją, nie dopuszczając do dezinhibicji Sabiny. O dziwo, tien Kazitan chciała Myrczka CHRONIĆ.

Aktor w Opowieści:

* Dokonanie:
    * miała 3 dni do powrotu do Aurum. Ale wpakowana w intrygę przez Rekiny złamała parol by uratować Myrczka przed nim samym. Wparowała, opieprzyła Myrczka i z nim "zerwała". Wierzy, że za wszystkim stoi Gabriel i to była intryga co miała ją zatrzymać w Pustogorze.
* Progresja:
    * nienawiść wobec Gabriela Ursusa. On chce ją zniszczyć, uderzył też w Myrczka jako przynętę (jej zdaniem). Typowy arystokrata w skórze owcy; groźny i inteligentny.


### Jeden problem, dwie rodziny

* **uid:** 191113-jeden-problem-dwie-rodziny, _numer względny_: 9
* **daty:** 0110-09-30 - 0110-10-05
* **obecni:** Amelia Mirzant, Kamil Lemurczak, Karol Kszatniak, Klara Baszcz, Leszek Baszcz, Paweł Kukułnik, Sabina Kazitan, Teresa Marszalnik

Streszczenie:

By zasilić Aleksandrię, czasami trzeba było kogoś zdigitalizować. Zabrano ciężko chorego syna lekarza. Gdy żona lekarza udała się do prasy, wsadzili ją do szpitala psychiatrycznego i narkotykami zniszczyli jej mózg. Lekarz, wspierany przez Sabinę (która chlapnęła pijana) stał się potworem Krwi i Esuriit. Zespół - Amelia i arystokraci - zniszczyli potwora, ale korzystając z okazji sprzęgli Kamila Lemurczaka z Aleksandrią.

Aktor w Opowieści:

* Dokonanie:
    * degeneratka, która wpierw wypaplała o krwawym rytuale człowiekowi a potem wróciła by pomóc. Panicznie się boi Kamila Lemurczaka i go nienawidzi do poziomu, że wybiera śmierć. Ma ogromną wiedzę o rytuałach, magii krwi i rzeczach bardzo ezoterycznych i niebezpiecznych.
* Progresja:
    * przeżyła. Jej zrozumienie Krwawych Rytuałów i Esuriit okazało się naprawdę duże i dość subtelne, co jest niepokojące.


### Tajna baza Orbitera?

* **uid:** 200510-tajna-baza-orbitera, _numer względny_: 8
* **daty:** 0110-09-07 - 0110-09-11
* **obecni:** Alina Anakonda, Artur Kołczond, Gabriel Ursus, Ignacy Myrczek, Kallista Exolon, Laura Tesinik, Mariusz Trzewń, Natalia Tessalon, Pięknotka Diakon, Sabina Kazitan, Talarand d'Irrydius, Teresa Mieralit, Tymon Grubosz

Streszczenie:

Natalia Tessalon chce przetransportować rannego brata do Aurum, ale manewr zablokowała Laura (terminuska-prawniczka) na prośbę Pięknotki. Natalia i Sabina się ścięły, przez co ucierpiał Myrczek a Sabina trafiła do aresztu. Teresa Mieralit zapewni, że Myrczek nie ma głowy do Sabiny. Pięknotka z Tymonem poszukali kto ciężko zmiażdżył Gabriela; udało się im znaleźć tajną bazę Orbitera w której jest Emulatorka, która się do tego przyznała.

Aktor w Opowieści:

* Dokonanie:
    * skonfliktowana arystokratka Aurum. Skonfrontowana z Natalią przyjmowała na chłodno aż Myrczek się wtrącił. By go chronić przed Natalią, zraniła go. Aresztowana.


### Rekin z Aurum i fortifarma

* **uid:** 200509-rekin-z-aurum-i-fortifarma, _numer względny_: 7
* **daty:** 0110-08-30 - 0110-09-04
* **obecni:** Artur Kołczond, Artur Michasiewicz, Erwin Galilien, Gabriel Ursus, Natalia Tessalon, Pięknotka Diakon, Sabina Kazitan, Tadeusz Tessalon

Streszczenie:

Sabina Kazitan ostrzegła Pięknotkę, że Natalia Tessalon z Aurum zażądała od niej niebezpiecznego rytuału do hodowania potwora. Pięknotka doszła do tego z sygnatury energii magicznej, że problem jest w okolicy Studni Irrydiańskiej; tam jest też fortifarma w której stary były-terminus tępi Latające Rekiny. I faktycznie - arystokraci z Aurum stworzyli potwora (krwawego) a Pięknotka Cieniem go zniszczyła. Niestety, brat Natalii został ciężko porażony i nigdy nie będzie już taki jak kiedyś. Aha, kwatermistrz opieprza Pięknotkę.

Aktor w Opowieści:

* Dokonanie:
    * z przyjemnością przekazała niebezpieczny rytuał Tessalonom i ostrzegła o tym Pięknotkę. Z przyjemnością patrzyła na zniszczenie Tadeusza Tessalona i ból Natalii.


### Wojna Trzęsawiska

* **uid:** 200418-wojna-trzesawiska, _numer względny_: 6
* **daty:** 0110-08-16 - 0110-08-21
* **obecni:** Gabriel Ursus, Ignacy Myrczek, Karla Mrozik, Pięknotka Diakon, Sabina Kazitan, Wiktor Satarail

Streszczenie:

Trzęsawisko odpowiedziało na atak tworząc nową bioformę na bazie Sabiny Kazitan; zaczęło przyciągać ludzi do siebie. Z uwagi na wysokie niebezpieczeństwo Karla chciała zbombardować Trzęsawisko z orbity, jak kiedyś. Pięknotka zrobiła kanał negocjacyjny Pustogor - Wiktor Satarail; Wiktor pomoże Pięknotce uspokoić Trzęsawisko, ale nie będzie bombardowania z orbity. Plus, Pustogor pomoże mu sformować nową bioformę...

Aktor w Opowieści:

* Dokonanie:
    * leży w szpitalu z Ignacym Myrczkiem; próbuje nie dać się zwariować od choroby i od opowieści o grzybach.
* Progresja:
    * stała się dużo bardziej rozpoznawalna w Szczelińcu przez anomalie Trzęsawiska. BARDZO nielubiana przez to.


### Nawoływanie Trzęsawiska

* **uid:** 200417-nawolywanie-trzesawiska, _numer względny_: 5
* **daty:** 0110-08-12 - 0110-08-14
* **obecni:** Gabriel Ursus, Ignacy Myrczek, Napoleon Bankierz, Olaf Zuchwały, Pięknotka Diakon, Sabina Kazitan, Strażniczka Alair

Streszczenie:

Trzęsawisko Zjawosztup zostało Skrzywdzone przez ostatnie wydarzenia. Zaczęło wabić Sabinę i Ignacego by ich pożreć. Szczęśliwie, Pięknotka zdążyła dotrzeć do Sabiny (która wabiona szukała Ignacego) i uratowała ich przed Trzęsawiskiem. Dodatkowo Pięknotka poznała sekret Sabiny i zdobyła nad nią absolutną kontrolę.

Aktor w Opowieści:

* Dokonanie:
    * ma koszmary przez Trzęsawisko, które ją nawołują. Próbuje sama rozwiązać problem do momentu zdradzenia jej sekretu przed Pięknotką. Oddała Pięknotce nad sobą kontrolę.
* Progresja:
    * kiedyś skrzywdziła Lilię Ursus, siostrę Gabriela. Ma nienawiść Gabriela i Lilii do siebie.
    * płynnie kontroluje czarną magię i magię krwi. Potrafi nawet ukryć ich emanacje, wchłaniając to w siebie.
    * jej sekret (czarna magia) został wykryty przez Gabriela Ursusa i Pięknotkę Diakon.
    * jest kompatybilna z Trzęsawiskiem Zjawosztup; Trzęsawisko pragnie jej obecności i ją przyzywa.


### Arystokraci na Trzęsawisku

* **uid:** 200414-arystokraci-na-trzesawisku, _numer względny_: 4
* **daty:** 0110-08-04 - 0110-08-05
* **obecni:** Franciszek Leszczowik, Ignacy Myrczek, Mariusz Trzewń, Pięknotka Diakon, Sabina Kazitan, Strażniczka Alair, Talia Aegis

Streszczenie:

Franciszek z Aurum dostosował Sabinę do swoich upodobań jako seksbombę. Sabina zdecydowała się go zniszczyć - doprowadziła go do Wysysacza Esuriit i wskazała, że na Trzęsawisku może zdobyć coś co mu się przyda. Franciszek stracił część świty na Trzęsawisku; gdyby nie Pięknotka, zginąłby tam. A tak tylko skończył z traumą a Sabinie się upiekło - roboty społeczne na 2 tygodnie.

Aktor w Opowieści:

* Dokonanie:
    * zmieniona w seksbombę dla przyjemności Franciszka, zemściła się - wprowadziła Franciszka na Trzęsawisko i z lubością patrzyła na jego dewastację. Zeznała wszystko.


### Korupcja z arystokratki

* **uid:** 191112-korupcja-z-arystokratki, _numer względny_: 3
* **daty:** 0110-07-04 - 0110-07-05
* **obecni:** Ataienne, Ignacy Myrczek, Mariusz Trzewń, Minerwa Metalia, Persefona d'Jastrząbiec, Pięknotka Diakon, Sabina Kazitan, Tadeusz Sklerzec

Streszczenie:

Po wydarzeniach z Grzymościem oczy Pięknotki padły na Jastrząbiec - tam dzieją się dziwne rzeczy z narkotykami. Okazało się, że to Ignacy Myrczek próbuje rozpaczliwie udowodnić arystokratce z Aurum, że nie zrobił jej krzywdy świadomie a pod wpływem narkotyków. Pięknotka wpadła (z pomocą Ataienne) i zrobiła porządek - ukarała burmistrza, Myrczka, arystokratkę i w sumie wszystkich. Ma smak popiołu w ustach, tu nie było nikogo sensownego...

Aktor w Opowieści:

* Dokonanie:
    * arystokratka. Dla przyjemności znęcała się nad Myrczkiem, sadystycznie. Pięknotka wywaliła ją z terenu i zniszczyła jej robota bojowego klasy Serratus.


### Perfekcyjna córka

* **uid:** 200219-perfekcyjna-corka, _numer względny_: 2
* **daty:** 0110-05-16 - 0110-05-19
* **obecni:** Aleksandra Szklarska, Ataienne, Lena Kardamacz, Leszek Szklarski, Mateusz Kardamacz, Paweł Oszmorn, Sabina Kazitan

Streszczenie:

72 dni przed wejściem Pięknotki do Kalbarka, sformowana niedawno grupa Liberatis próbująca rozwiązać problem straszliwej kontroli memetycznej na Astorii wpadła w kłopoty. Jedna z członkiń Liberatis, Lena, była bioformą stworzoną przez Mateusza - i Mateusz chciał ją z powrotem. W odpowiedzi na to, że Mateusz porywa anarchistów Liberatis ściągnęli do Kalbarku Ataienne. Współpracując z nią, Leszek (lider Liberatis) wymanewrował Mateusza i wraz z Leną i wsparciem Sabiny pokonali Mateusza, uwalniając przy okazji innych anarchistów. Aha, powstało też stado wolnych samochodów...

Aktor w Opowieści:

* Dokonanie:
    * wysłana przez Lemurczaka by zobaczyć co Kardamacz ma wartościowego, dyskretnie wzmocniła Liberatis i rozwaliła plany Kardamacza. Bo Lemurczak. Mistrzyni potężnych energii.


### Glizda, która leczy

* **uid:** 211120-glizda-ktora-leczy, _numer względny_: 1
* **daty:** 0108-04-07 - 0108-04-18
* **obecni:** Amanda Kajrat, Amelia Sowińska, Ernest Kajrat, Feliks Keksik, Justynian Diakon, Kacper Bankierz, Karol Pustak, Oliwia Lemurczak, Roland Sowiński, Sabina Kazitan, Sensacjusz Diakon, Stella Sowińska

Streszczenie:

Sensacjusz Diakon został wysłany na prowincję pod przykrywką Lekarza Rekinów, by dojść ze Stellą do tego czemu Amelia i Roland Sowińscy zachowują się nietypowo. Na miejscu okazało się, że Roland jest w stanie ostrej wojny z mafią (która nie traktuje tego poważnie; Kajrat się bawi z Rekinami) a Amelia po prostu nie w pełni sobie radzi i próbuje opanować sytuację. Roland jest Skażony Esuriit; Sensacjusz przetransportował go glizdą do Aurum i sam, uwięziony w bojowej formie, deeskalował konflikt z mafią. A Amelia jest tymczasowym dowódcą Rekinów...

Aktor w Opowieści:

* Dokonanie:
    * przekazana przez Oliwię Rolandowi, skusiła go możliwością pokonania Kajrata i wkręciła go w Esuriit. Zdominowana przez Rolanda, jednak tak naprawdę to ona wygrywa.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 13, @: 0112-02-20
    1. Multivirt    : 1, @: 0112-02-20
    1. Primus    : 13, @: 0112-02-20
        1. Sektor Astoriański    : 13, @: 0112-02-20
            1. Astoria, Orbita    : 1, @: 0112-02-20
                1. Kontroler Pierwszy    : 1, @: 0112-02-20
                    1. Połączony Rdzeń    : 1, @: 0112-02-20
            1. Astoria    : 12, @: 0111-04-28
                1. Sojusz Letejski    : 12, @: 0111-04-28
                    1. Aurum    : 1, @: 0110-10-05
                        1. Powiat Lemurski    : 1, @: 0110-10-05
                            1. Kruczaniec    : 1, @: 0110-10-05
                                1. Fabryka Mebli Larmat    : 1, @: 0110-10-05
                                1. Supermarket Złotko    : 1, @: 0110-10-05
                                1. Upiorny Las    : 1, @: 0110-10-05
                                    1. Mordownia    : 1, @: 0110-10-05
                                1. Zamek Pisarza    : 1, @: 0110-10-05
                    1. Szczeliniec    : 11, @: 0111-04-28
                        1. Powiat Jastrzębski    : 4, @: 0110-10-13
                            1. Jastrząbiec, okolice    : 1, @: 0110-10-09
                                1. Blokhaus Widmo    : 1, @: 0110-10-09
                            1. Jastrząbiec    : 1, @: 0110-07-05
                                1. Hotel Stacja Kosmiczna    : 1, @: 0110-07-05
                                1. Ratusz    : 1, @: 0110-07-05
                            1. Kalbark, Nierzeczywistość    : 1, @: 0110-05-19
                                1. Utopia    : 1, @: 0110-05-19
                            1. Kalbark    : 1, @: 0110-05-19
                                1. Autoklub Piękna    : 1, @: 0110-05-19
                                1. Escape Room Lustereczko    : 1, @: 0110-05-19
                                1. Mini Barbakan    : 1, @: 0110-05-19
                                1. Rzeźnia    : 1, @: 0110-05-19
                            1. Praszalek, okolice    : 1, @: 0110-10-13
                                1. Lasek Janor    : 1, @: 0110-10-13
                                1. Park rozrywki Janor    : 1, @: 0110-10-13
                        1. Powiat Pustogorski    : 8, @: 0111-04-28
                            1. Czemerta, okolice    : 2, @: 0110-09-11
                                1. Baza Irrydius    : 1, @: 0110-09-11
                                1. Fortifarma Irrydia    : 2, @: 0110-09-11
                                1. Studnia Irrydiańska    : 2, @: 0110-09-11
                            1. Czemerta    : 1, @: 0110-08-21
                            1. Podwiert    : 3, @: 0111-04-28
                                1. Dzielnica Luksusu Rekinów    : 2, @: 0111-04-28
                                    1. Obrzeża Biedy    : 1, @: 0111-04-28
                                        1. Stadion Lotników    : 1, @: 0111-04-28
                                    1. Serce Luksusu    : 1, @: 0108-04-18
                                        1. Lecznica Rannej Rybki    : 1, @: 0108-04-18
                                1. Las Trzęsawny    : 2, @: 0111-04-28
                            1. Pustogor    : 3, @: 0110-09-11
                                1. Eksterior    : 2, @: 0110-09-11
                                    1. Miasteczko    : 2, @: 0110-09-11
                                        1. Knajpa Górska Szalupa    : 2, @: 0110-09-11
                                    1. Zamek Weteranów    : 1, @: 0110-08-14
                                1. Gabinet Pięknotki    : 1, @: 0110-09-04
                                1. Knajpa Górska Szalupa    : 1, @: 0110-09-04
                                1. Rdzeń    : 2, @: 0110-09-11
                                    1. Szpital Terminuski    : 2, @: 0110-09-11
                            1. Zaczęstwo    : 4, @: 0111-04-28
                                1. Akademia Magii, kampus    : 2, @: 0110-10-09
                                    1. Arena Treningowa    : 1, @: 0110-10-09
                                1. Dzielnica Kwiecista    : 1, @: 0111-04-28
                                    1. Rezydencja Porzeczników    : 1, @: 0111-04-28
                                1. Nieużytki Staszka    : 1, @: 0110-08-14
                                1. Złomowisko    : 1, @: 0111-04-28
                        1. Trzęsawisko Zjawosztup    : 2, @: 0110-08-21
                            1. Laboratorium W Drzewie    : 1, @: 0110-08-21

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 7 | ((191112-korupcja-z-arystokratki; 200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera; 201011-narodziny-paladynki-saitaera)) |
| Gabriel Ursus        | 6 | ((200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera; 201006-dezinhibitor-dla-sabiny; 201011-narodziny-paladynki-saitaera)) |
| Ignacy Myrczek       | 6 | ((191112-korupcja-z-arystokratki; 200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200510-tajna-baza-orbitera; 201006-dezinhibitor-dla-sabiny)) |
| Mariusz Trzewń       | 3 | ((191112-korupcja-z-arystokratki; 200414-arystokraci-na-trzesawisku; 200510-tajna-baza-orbitera)) |
| Artur Kołczond       | 2 | ((200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera)) |
| Ataienne             | 2 | ((191112-korupcja-z-arystokratki; 200219-perfekcyjna-corka)) |
| Feliks Keksik        | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 211120-glizda-ktora-leczy)) |
| Justynian Diakon     | 2 | ((201006-dezinhibitor-dla-sabiny; 211120-glizda-ktora-leczy)) |
| Laura Tesinik        | 2 | ((200510-tajna-baza-orbitera; 201006-dezinhibitor-dla-sabiny)) |
| Lorena Gwozdnik      | 2 | ((201006-dezinhibitor-dla-sabiny; 210406-potencjalnie-eksterytorialny-seksbot)) |
| Minerwa Metalia      | 2 | ((191112-korupcja-z-arystokratki; 201011-narodziny-paladynki-saitaera)) |
| Napoleon Bankierz    | 2 | ((200417-nawolywanie-trzesawiska; 201006-dezinhibitor-dla-sabiny)) |
| Natalia Tessalon     | 2 | ((200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera)) |
| Strażniczka Alair    | 2 | ((200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska)) |
| Adalbert Brześniak   | 1 | ((211114-admiral-gruby-wieprz)) |
| Agaton Ociegor       | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Aleksander Muniakiewicz | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Aleksandra Szklarska | 1 | ((200219-perfekcyjna-corka)) |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Amanda Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Amelia Mirzant       | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Amelia Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Aranea Diakon        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Artur Michasiewicz   | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Barnaba Burgacz      | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Ernest Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Erwin Galilien       | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Franciszek Leszczowik | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Franek Bulterier     | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Henryk Wkrąż         | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Izabela Zarantel     | 1 | ((211114-admiral-gruby-wieprz)) |
| Julia Kardolin       | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Kacper Bankierz      | 1 | ((211120-glizda-ktora-leczy)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Kamil Lemurczak      | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Karla Mrozik         | 1 | ((200418-wojna-trzesawiska)) |
| Karol Kszatniak      | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Karol Pustak         | 1 | ((211120-glizda-ktora-leczy)) |
| Karolina Erenit      | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Klara Baszcz         | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Klaudia Stryk        | 1 | ((211114-admiral-gruby-wieprz)) |
| Lena Kardamacz       | 1 | ((200219-perfekcyjna-corka)) |
| Leszek Baszcz        | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Leszek Szklarski     | 1 | ((200219-perfekcyjna-corka)) |
| Marysia Sowińska     | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Mateusz Kardamacz    | 1 | ((200219-perfekcyjna-corka)) |
| Olaf Zuchwały        | 1 | ((200417-nawolywanie-trzesawiska)) |
| Oliwia Lemurczak     | 1 | ((211120-glizda-ktora-leczy)) |
| Paweł Kukułnik       | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Paweł Oszmorn        | 1 | ((200219-perfekcyjna-corka)) |
| Persefona d'Jastrząbiec | 1 | ((191112-korupcja-z-arystokratki)) |
| Rafael Galwarn       | 1 | ((211114-admiral-gruby-wieprz)) |
| Rafał Kumczek        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Roland Sowiński      | 1 | ((211120-glizda-ktora-leczy)) |
| Roman Panracz        | 1 | ((211114-admiral-gruby-wieprz)) |
| Rupert Mysiokornik   | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Rzieza d'K1          | 1 | ((211114-admiral-gruby-wieprz)) |
| Saitaer              | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Sensacjusz Diakon    | 1 | ((211120-glizda-ktora-leczy)) |
| Stella Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Tadeusz Sklerzec     | 1 | ((191112-korupcja-z-arystokratki)) |
| Tadeusz Tessalon     | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Talia Aegis          | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Teresa Marszalnik    | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Teresa Mieralit      | 1 | ((200510-tajna-baza-orbitera)) |
| Triana Porzecznik    | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Tymon Grubosz        | 1 | ((200510-tajna-baza-orbitera)) |
| Wiktor Satarail      | 1 | ((200418-wojna-trzesawiska)) |