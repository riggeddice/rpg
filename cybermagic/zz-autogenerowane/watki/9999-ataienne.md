# Ataienne
## Identyfikator

Id: 9999-ataienne

## Sekcja Opowieści

### Arcymag w Raju

* **uid:** 190326-arcymag-w-raju, _numer względny_: 17
* **daty:** 0111-07-18 - 0111-07-19
* **obecni:** Ataienne, Dawid Szardak, Eliza Ira, Fergus Salien, Grzegorz Kamczarnik, Olga Leszcz, OO Castigator

Streszczenie:

Eliza Ira zdecydowała się odzyskać swoich ludzi z czasów wojny przetrzymywanych w Trzecim Raju. Stworzyła Krystaliczną Plagę unieszkodliwiającą wpływ mindwarpującej AI, Ataienne. Jednak Fergus i Olga dali radę przekonać Elizę, że wojna się skończyła i ich wspólnym przeciwnikiem jest teraz Astoria. Arcymagini weszła w ostrożny sojusz z Orbiterem Pierwszym, odzyskując kilkanaście osób chcących z nią współpracować.

Aktor w Opowieści:

* Dokonanie:
    * mindwarpująca TAI o funkcji nadpisywania wspomnień; hipnopiosenkarka holograficzna. Kontroluje Trzeci Raj składający się z jeńców wojennych.


### Sabotaż świni

* **uid:** 201104-sabotaz-swini, _numer względny_: 16
* **daty:** 0111-03-13 - 0111-03-16
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Diana Arłacz, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, Rafał Armadion, Robert Garwen

Streszczenie:

Sabotażysta - miragent - podjął próbę zniszczenia Arianny gdy ta ładowała świnie na Wesołego Wieprzka używając wybuchowej, anomalnej świni. Zespół przetrwał, choć "Wieprzek" został zniszczony. Arianna pojmała miragenta i przekazała go do Kontrolera Pierwszego, acz kosztem ofiar w ludziach. No i nie było dość świń by je wziąć na orbitę XD.

Aktor w Opowieści:

* Dokonanie:
    * zestrzeliła działkami Trzeciego Raju anomalnego Wieprzka, po czym użyła pełni mindwarpa by wyczyścić ofiary anomalnego Wieprzka. I nikt nic nie wie.


### Noktianie rodu Arłacz

* **uid:** 201021-noktianie-rodu-arlacz, _numer względny_: 15
* **daty:** 0111-03-07 - 0111-03-10
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Diana Arłacz, Eliza Ira, Eustachy Korkoran, Izabela Zarantel, Jolanta Arłacz, Juliusz Sowiński, Klaudia Stryk, Klaus Rumak, Maria Gołąb, OO Szalony Rumak, OO Wesoły Wieprzek, Robert Arłacz, Wanessa Pyszcz

Streszczenie:

By uzyskać współpracę Elizy Iry, Arianna zdecydowała się "porwać" noktian rodu Arłacz wykorzystywanych do produkcji czystej krwi dla magów. Z uwagi na działania Marii Gołąb plan został odrzucony i wykryty, ale znalezienie haków na Arłaczy rozwiązało ten problem - Arianna i Arłacze współpracują. Nie doszło do porozumienia z Elizą Irą, ale Eliza się wycofała z Trzeciego Raju, ogłaszając się protektorką tego terenu. Sytuacja w miarę ustabilizowana.

Aktor w Opowieści:

* Dokonanie:
    * bardzo chce się połączyć psychotronicznie z Anastazją. Nie ma okazji - Arianna jej nie pozwala. Za to ma nowych 100 noktian do zmanipulowania i Raj do trzymania.


### Krystaliczny gniew Elizy

* **uid:** 201014-krystaliczny-gniew-elizy, _numer względny_: 14
* **daty:** 0111-03-02 - 0111-03-05
* **obecni:** Aleksandra Termia, Anastazja Sowińska, Arianna Verlen, Ataienne, Dariusz Krantak, Elena Verlen, Eliza Ira, Eustachy Korkoran, Klaudia Stryk, Marian Fartel, OO Wesoły Wieprzek

Streszczenie:

Pomoc humanitarna przybyła do Trzeciego Raju wraz z zabójcami Anastazji. Mindwarpowani komandosi noktiańscy prawie zabili Elenę, ale Anastazja została ochroniona. Do akcji weszła Eliza Ira - zażądała oddania jej Trzeciego Raju i noktian. Zespół przygotowuje się do nie-oddania Raju i uratowania jak najwięcej noktian z Aurum (jakkolwiek to nie brzmi).

Aktor w Opowieści:

* Dokonanie:
    * widząc jak Dariusz potraktował Anastazję Sowińską (jej krewną), użyła swej mocy na życzenie Arianny by go zmusić do przyznania się.


### Magiczna burza w Raju

* **uid:** 200923-magiczna-burza-w-raju, _numer względny_: 13
* **daty:** 0111-02-21 - 0111-02-25
* **obecni:** Anastazja Sowińska, Arianna Verlen, Dariusz Krantak, Elena Verlen, Eliza Ira, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Marian Fartel, Marianna Lemurczak, Nikodem Sowiński

Streszczenie:

Zbliża się burza magiczna. Nie ma schronienia ani zapasów. Zespół wysłał SOS na Kontroler i do Nikodema Sowińskiego (Anastazja). Zbudowali co się da by odeprzeć burzę magiczną i im się to udało bez strat w ludziach. O dziwo, siła która im pomogła to Eliza Ira (acz dyskretnie). Anastazja wierzy że Nikodem jest jej najlepszym przyjacielem, ale on pragnie jej zguby...

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * niechęć wobec Arianny Verlen. Ataienne jest przeciwna bogom i istotom bogom podobnym a Arianna jest arcymagiem używającym eterniańskich metod.


### Śmierć Raju

* **uid:** 200916-smierc-raju, _numer względny_: 12
* **daty:** 0111-02-18 - 0111-02-20
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Celina Szilat, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Marian Fartel, Rafał Armadion, Robert Garwen, Wanessa Pyszcz

Streszczenie:

Najważniejszym problemem Raju jest pozbycie się bombardowania nojrepów. Arianna zaproponowała zuchwały pomysł - w kontrolowany sposób niech nojrepy wejdą do miasteczka i wezmą czego szukają. Udało jej się wszystkich przekonać. Sukces - nojrepy się wycofały, ale Tucznik nie jest zdolny do latania a Trzeci Raj jest zniszczony. Została tylko Ataienne i ruina. Ataienne rozpaliła hipnotycznie morale trzeciorajowców - przetrwają to wszystko.

Aktor w Opowieści:

* Dokonanie:
    * w Trzecim Raju działa w ukryciu, nie pokazując co naprawdę umie i stanowi źródło rozczarowań. Ale jak Raj został zniszczony, użyła pełnej mocy hipnotycznej. Nikt jej nie jest w stanie kontrolować, acz Arianna i Klaudia widzą, co potrafi.


### Rozbrojenie bomby w Kalbarku

* **uid:** 200222-rozbrojenie-bomby-w-kalbarku, _numer względny_: 11
* **daty:** 0110-07-27 - 0110-08-01
* **obecni:** Aleksandra Szklarska, Ataienne, Bartłomiej Małczarek, Diana Tevalier, Lucjusz Blakenbauer, Mateusz Kardamacz, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Kalbark okazał się jedną wielką bombą. Liberatis, silnie uzbrojeni. Kardamacz, kontroluje całe miasto. Terminusi, skażeni kralotycznie. I Chevaleresse na którą wszystko spadnie jeśli przyjdzie wsparcie. Pięknotka rozmontowała tą bombę, ściągnęła Ataienne (oficjalnie) i Zespół dał radę rozproszyć Liberatis, wycofać Chevaleresse i odzyskać kontrolę nad Barbakanem. Pięknotka zebrała wsparcie i na polu bitwy został na serio tylko Kardamacz.

Aktor w Opowieści:

* Dokonanie:
    * autoryzowana przez Pięknotkę do pomocy w Barbakanie, pokazuje bezwzględność wobec Małmałaza i Kardamacza. Rozmontowała Liberatis i odzyskała broń.


### Krucjata Chevaleresse

* **uid:** 200202-krucjata-chevaleresse, _numer względny_: 10
* **daty:** 0110-07-24 - 0110-07-27
* **obecni:** Ataienne, Damian Orion, Diana Tevalier, Keraina d'Orion, Malictrix d'Itaran, Mariusz Trzewń, Mateusz Kardamacz, Minerwa Metalia, Pięknotka Diakon, Talia Aegis, Tomasz Tukan

Streszczenie:

Chevaleresse wykradła broń Alana i uzbroiła Liberitias, gdzie działa też Ataienne przeciw Kardamaczowi, Grzymościowi i krzywdzicielowi Alana. Pięknotka doszła do serca problemu i materii, acz potrzebowała wsparcia Orbitera - dokładniej, Kerainy. Dzięki Kerainie wprowadziła Malictrix do Kompleksu Itaran. Teraz to tylko kwestia czasu aż Grzymość zostanie zniszczony.

Aktor w Opowieści:

* Dokonanie:
    * TAI powyżej 3 stopnia. Działa w tle - taktyk oraz commander, ale niekoniecznie mistrzyni zdobywania informacji. Jedyna TAI 3.5 w okolicy Szczelińca. Sformowała 'Liberitas'


### Kwiaty w służbie puryfikacji

* **uid:** 190817-kwiaty-w-sluzbie-puryfikacji, _numer względny_: 9
* **daty:** 0110-07-04 - 0110-07-09
* **obecni:** Ataienne, Erwin Galilien, Kornel Garn, Nataniel Marszalnik, Pięknotka Diakon, Teresa Marszalnik

Streszczenie:



Aktor w Opowieści:

* Dokonanie:
    * 


### Korupcja z arystokratki

* **uid:** 191112-korupcja-z-arystokratki, _numer względny_: 8
* **daty:** 0110-07-04 - 0110-07-05
* **obecni:** Ataienne, Ignacy Myrczek, Mariusz Trzewń, Minerwa Metalia, Persefona d'Jastrząbiec, Pięknotka Diakon, Sabina Kazitan, Tadeusz Sklerzec

Streszczenie:

Po wydarzeniach z Grzymościem oczy Pięknotki padły na Jastrząbiec - tam dzieją się dziwne rzeczy z narkotykami. Okazało się, że to Ignacy Myrczek próbuje rozpaczliwie udowodnić arystokratce z Aurum, że nie zrobił jej krzywdy świadomie a pod wpływem narkotyków. Pięknotka wpadła (z pomocą Ataienne) i zrobiła porządek - ukarała burmistrza, Myrczka, arystokratkę i w sumie wszystkich. Ma smak popiołu w ustach, tu nie było nikogo sensownego...

Aktor w Opowieści:

* Dokonanie:
    * nieprawdopodobnie potężna z perspektywy opętywania maszyn i przejmowania kontroli. Sojuszniczka Pięknotki, która przełamała wszystkie zabezpieczenia nawet się nie starając...


### Zaginiona soniczka

* **uid:** 191105-zaginiona-soniczka, _numer względny_: 7
* **daty:** 0110-07-02 - 0110-07-03
* **obecni:** Ataienne, Erwin Galilien, Mariola Tralment, Mariusz Trzewń, Mateusz Kardamacz, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Ataienne poprosiła Pięknotkę o znalezienie zaginionej fanki. Pięknotka zaczęła śledztwo i doszła do tego, że Mariola została porwana przez gang przerzucający dziewczyny do Cieniaszczytu. Erwin słyszał takie plotki. Pięknotka zlokalizowała i usunęła komórkę tego gangu; śledztwo pokazało na autoklub fitness jako jedno interesujące miejsce. Najpewniej nie to jest miejscem "zła", ale warto sprawdzić.

Aktor w Opowieści:

* Dokonanie:
    * przestraszyła się o Mariolę (swoją soniczkę która zaginęła) i udała się z prośbą o pomoc do Pięknotki. Przypadkowo, Ataienne wykryła gang przerzutowy dziewczyn do Cieniaszczytu.


### Perfekcyjna córka

* **uid:** 200219-perfekcyjna-corka, _numer względny_: 6
* **daty:** 0110-05-16 - 0110-05-19
* **obecni:** Aleksandra Szklarska, Ataienne, Lena Kardamacz, Leszek Szklarski, Mateusz Kardamacz, Paweł Oszmorn, Sabina Kazitan

Streszczenie:

72 dni przed wejściem Pięknotki do Kalbarka, sformowana niedawno grupa Liberatis próbująca rozwiązać problem straszliwej kontroli memetycznej na Astorii wpadła w kłopoty. Jedna z członkiń Liberatis, Lena, była bioformą stworzoną przez Mateusza - i Mateusz chciał ją z powrotem. W odpowiedzi na to, że Mateusz porywa anarchistów Liberatis ściągnęli do Kalbarku Ataienne. Współpracując z nią, Leszek (lider Liberatis) wymanewrował Mateusza i wraz z Leną i wsparciem Sabiny pokonali Mateusza, uwalniając przy okazji innych anarchistów. Aha, powstało też stado wolnych samochodów...

Aktor w Opowieści:

* Dokonanie:
    * dzięki współpracy Leszka i Sabiny przejęła kontrolę nad Kalbarkiem i się tam zaszczepiła. Zapewnia absolutną widoczność oraz neutralizuje Paradoksy przekierowując emitery.


### Eksperymentalny power suit

* **uid:** 190402-eksperymentalny-power-suit, _numer względny_: 5
* **daty:** 0110-03-15 - 0110-03-17
* **obecni:** Ataienne, Erwin Galilien, Kaja Selerek, Minerwa Metalia, Pięknotka Diakon, Szymon Oporcznik

Streszczenie:

Orbiter chciał przekazać Pięknotce eksperymentalny Power Suit którego nikt nie potrafi kontrolować. Pięknotka spróbowała się z nim zintegrować; ona, Minerwa, Erwin i jeszcze dwóch magów Orbitera skończyło w szpitalu. Pięknotka się uśmiechnęła. Chce ten power suit. Opanuje go - mimo, że jest Skażony energią ixiońską i ma uszkodzoną psychotronikę pełną nienawiści. Życie Pięknotki uratowała Ataienne, ale skończyła ciężko ranna, z uszkodzoną matrycą.

Aktor w Opowieści:

* Dokonanie:
    * wniknęła do Cienia i uratowała Pięknotkę przed nienawistnym instynktem psychotroniki. Skończyła bardzo ciężko ranna, z rozpadającą się matrycą.
* Progresja:
    * ciężko ranna, z uszkodzoną matrycą. Uratowała życie Pięknotce, ale zapłaciła rozpad matrycy; ma trochę czasu regeneracji.


### Kurz po Ataienne

* **uid:** 190331-kurz-po-ataienne, _numer względny_: 4
* **daty:** 0110-03-07 - 0110-03-09
* **obecni:** Alan Bartozol, Ataienne, Diana Tevalier, Pięknotka Diakon, Romeo Węglas, Szymon Oporcznik

Streszczenie:

Alan poprosił Pięknotkę o interwencję w sprawie tego, że uziemił Chevaleresse; Pięknotka opanowała chcącą opuścić Alana Chevaleresse. Potem Pięknotka nawiązała użyteczny kontakt w Szymonie z Orbitera, znalazła osoby odpowiedzialne za próbę zabicia Ataienne i porozmawiała z samą Ataienne; uznała, że ta konkretna AI jest zdecydowanie dziwna.

Aktor w Opowieści:

* Dokonanie:
    * uważa Chevaleresse za przyjaciółkę i dlatego nie udało się Pięknotce jej przekonać do użycia swoich mocy by nieco ustabilizować Chevaleresse. Wdzięczna Pięknotce.


### Polowanie na Ataienne

* **uid:** 190330-polowanie-na-ataienne, _numer względny_: 3
* **daty:** 0110-03-03 - 0110-03-04
* **obecni:** Alan Bartozol, Ataienne, Atena Sowińska, Diana Tevalier, Lucjusz Blakenbauer, Pięknotka Diakon

Streszczenie:

Ataienne chciała zrobić koncert; Orbiter inkarnował ją w Zaczęstwie. Podczas koncertu Ataienne została zaatakowana i w jej obronie stanęła Chevaleresse, która aportowała broń Alana... Pięknotka wpierw zatrzymała tą wojnę, potem uratowała Ataienne od technovora a na końcu wydobyła siebie i Ataienne z rąk najemników którzy chcieli zniszczyć TAI. Trudny dzień; wszystko wskazuje na to, że za polowaniem na Ataienne stoi Eliza Ira?

Aktor w Opowieści:

* Dokonanie:
    * chciała tylko zrobić koncert - a cudem uniknęła śmierci dzięki ratunkowi Chevaleresse a potem Pięknotki. Miała hosta w formie człowieka ku niezadowoleniu Pięknotki.


### Parszywa ekspedycja

* **uid:** 190123-parszywa-ekspedycja, _numer względny_: 2
* **daty:** 0095-04-14 - 0095-04-16
* **obecni:** ASD Grazoniusz, Ataienne, OO Bubuta

Streszczenie:

Oddział skazańców został wysłany na ASD Grazionusz w celu odzyskania AI Persefona. Dostali znane plany statku z adnotacją "Nic z tego nie musi być prawdą". Podczas drogi przez statek natknęli się na wielu ludzi przekształconych przez Saitaera. Kiedy używali magii, wzburzyli energię na tyle, że Saitaer zaczął do nich mówić i kusić. Spotkali również Alicję, małą dziewczynkę przedstawiającą się jako kapitan tego statku. Szybko domyślili się, że to jest teraz AI statku, ale postanowili dojść do głównego pokoju komputerowego. Po drodze.
Ostatecznie udało im się tam dotrzeć i wrócić do śluzy. John był już pod silnym wpływem Saitaera, wpadł w amok i został na ASD Grazionusz.
Andrzej był bardzo blisko i pod silnym wpływem, ale postanowił opuścić statek.

Aktor w Opowieści:

* Dokonanie:
    * kiedyś kapitan Alicja Sowińska, teraz z woli Saitaera TAI Grazioniusza. "Porwana" przez ekspedycję OO Bubuta.
* Progresja:
    * Alicja Sowińska powróciła do Orbitera Pierwszego, ale jako TAI a nie czarodziejka. Nie wiadomo co z nią zrobią. Nie wiadomo co ona może zrobić.


### Skażenie Grazoniusza

* **uid:** 190123-skazenie-grazoniusza, _numer względny_: 1
* **daty:** 0079-03-30 - 0079-04-01
* **obecni:** ASD Grazoniusz, Ataienne

Streszczenie:

ASD Grazoniusz, statek wydobywczo-górniczy, natknął się na uśpionego Saitaera w asteroidach. Aspekty Saitaera szybko i bardzo dyskretnie pozbyły się Mausów oraz przejęły kontrolę nad statkiem tak, że Astoria nie miała o niczym pojęcia. Co gorsza, Saitaer dyskretnie schował część swoich agentów i rozprzestrzenia się w taki sposób, że nikt nie ma o niczym pojęcia.

Aktor w Opowieści:

* Dokonanie:
    * jeszcze Alicja Sowińska. Idealistyczna kapitan statku ASD Grazoniusz. Nieświadoma obecności Saitaera na swoim statku, została zainfekowana i dołączyła do sił Saitaera.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 16, @: 0111-07-19
    1. Primus    : 16, @: 0111-07-19
        1. Sektor Astoriański    : 16, @: 0111-07-19
            1. Astoria, Orbita    : 1, @: 0111-02-20
                1. Kontroler Pierwszy    : 1, @: 0111-02-20
            1. Astoria    : 14, @: 0111-07-19
                1. Sojusz Letejski, NW    : 5, @: 0111-07-19
                    1. Ruiniec    : 5, @: 0111-07-19
                        1. Kryształowa Forteca    : 1, @: 0111-07-19
                            1. Mauzoleum    : 1, @: 0111-07-19
                            1. Rajskie Peryferia    : 1, @: 0111-07-19
                        1. Pustynia Kryształowa    : 1, @: 0111-03-10
                        1. Trzeci Raj, okolice    : 1, @: 0111-02-20
                            1. Kopiec Nojrepów    : 1, @: 0111-02-20
                            1. Zdrowa Ziemia    : 1, @: 0111-02-20
                        1. Trzeci Raj    : 5, @: 0111-07-19
                            1. Barbakan    : 1, @: 0111-02-20
                            1. Centrala Ataienne    : 2, @: 0111-07-19
                            1. Mały Kosmoport    : 1, @: 0111-02-20
                            1. Ratusz    : 1, @: 0111-02-20
                            1. Stacja Nadawcza    : 1, @: 0111-02-20
                1. Sojusz Letejski    : 10, @: 0111-03-10
                    1. Aurum    : 1, @: 0111-03-10
                        1. Powiat Niskowzgórza    : 1, @: 0111-03-10
                            1. Domena Arłacz    : 1, @: 0111-03-10
                                1. Posiadłość Arłacz    : 1, @: 0111-03-10
                                    1. Baterie Puryfikacji Krwi    : 1, @: 0111-03-10
                                    1. Małe lądowisko    : 1, @: 0111-03-10
                    1. Szczeliniec    : 9, @: 0110-08-01
                        1. Powiat Jastrzębski    : 5, @: 0110-08-01
                            1. Jastrząbiec, okolice    : 1, @: 0110-07-27
                                1. Klinika Iglica    : 1, @: 0110-07-27
                                    1. Kompleks Itaran    : 1, @: 0110-07-27
                                1. TechBunkier Sarrat    : 1, @: 0110-07-27
                            1. Jastrząbiec    : 1, @: 0110-07-05
                                1. Hotel Stacja Kosmiczna    : 1, @: 0110-07-05
                                1. Ratusz    : 1, @: 0110-07-05
                            1. Kalbark, Nierzeczywistość    : 1, @: 0110-05-19
                                1. Utopia    : 1, @: 0110-05-19
                            1. Kalbark    : 4, @: 0110-08-01
                                1. Autoklub Piękna    : 3, @: 0110-08-01
                                1. Escape Room Lustereczko    : 2, @: 0110-07-27
                                1. Mini Barbakan    : 2, @: 0110-08-01
                                1. Rzeźnia    : 1, @: 0110-05-19
                        1. Powiat Pustogorski    : 5, @: 0110-07-27
                            1. Czółenko    : 1, @: 0110-07-03
                                1. Bunkry    : 1, @: 0110-07-03
                            1. Podwiert, obrzeża    : 2, @: 0110-03-17
                                1. Kompleks Badawczy Skelidar    : 1, @: 0110-03-17
                                1. Kosmoport    : 1, @: 0110-03-09
                            1. Podwiert    : 1, @: 0110-03-04
                                1. Osiedle Leszczynowe    : 1, @: 0110-03-04
                            1. Pustogor    : 2, @: 0110-07-03
                                1. Eksterior    : 1, @: 0110-07-03
                                    1. Miasteczko    : 1, @: 0110-07-03
                                1. Miasteczko    : 1, @: 0110-03-09
                            1. Zaczęstwo    : 3, @: 0110-07-27
                                1. Nieużytki Staszka    : 2, @: 0110-07-03
                                1. Osiedle Ptasie    : 1, @: 0110-07-03
                                1. Złomowisko    : 1, @: 0110-03-04
                        1. Powiat Złotodajski    : 1, @: 0110-07-09
                            1. Złotkordza    : 1, @: 0110-07-09
                                1. Stadion    : 1, @: 0110-07-09
            1. Libracja Lirańska    : 2, @: 0095-04-16
                1. Anomalia Kolapsu    : 2, @: 0095-04-16
                    1. Cmentarzysko Statków    : 1, @: 0079-04-01
                        1. Krypta Saitaera    : 1, @: 0079-04-01

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 8 | ((190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 190402-eksperymentalny-power-suit; 190817-kwiaty-w-sluzbie-puryfikacji; 191105-zaginiona-soniczka; 191112-korupcja-z-arystokratki; 200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Anastazja Sowińska   | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Arianna Verlen       | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Diana Tevalier       | 4 | ((190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Eustachy Korkoran    | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Klaudia Stryk        | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Mateusz Kardamacz    | 4 | ((191105-zaginiona-soniczka; 200202-krucjata-chevaleresse; 200219-perfekcyjna-corka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Eliza Ira            | 3 | ((190326-arcymag-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Erwin Galilien       | 3 | ((190402-eksperymentalny-power-suit; 190817-kwiaty-w-sluzbie-puryfikacji; 191105-zaginiona-soniczka)) |
| Mariusz Trzewń       | 3 | ((191105-zaginiona-soniczka; 191112-korupcja-z-arystokratki; 200202-krucjata-chevaleresse)) |
| Minerwa Metalia      | 3 | ((190402-eksperymentalny-power-suit; 191112-korupcja-z-arystokratki; 200202-krucjata-chevaleresse)) |
| Alan Bartozol        | 2 | ((190330-polowanie-na-ataienne; 190331-kurz-po-ataienne)) |
| Aleksandra Szklarska | 2 | ((200219-perfekcyjna-corka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| ASD Grazoniusz       | 2 | ((190123-parszywa-ekspedycja; 190123-skazenie-grazoniusza)) |
| Diana Arłacz         | 2 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Elena Verlen         | 2 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy)) |
| Izabela Zarantel     | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Lucjusz Blakenbauer  | 2 | ((190330-polowanie-na-ataienne; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Marian Fartel        | 2 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy)) |
| OO Wesoły Wieprzek   | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Rafał Armadion       | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Robert Garwen        | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Sabina Kazitan       | 2 | ((191112-korupcja-z-arystokratki; 200219-perfekcyjna-corka)) |
| Szymon Oporcznik     | 2 | ((190331-kurz-po-ataienne; 190402-eksperymentalny-power-suit)) |
| Tymon Grubosz        | 2 | ((191105-zaginiona-soniczka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Wanessa Pyszcz       | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Aleksandra Termia    | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Atena Sowińska       | 1 | ((190330-polowanie-na-ataienne)) |
| Bartłomiej Małczarek | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Damian Orion         | 1 | ((200202-krucjata-chevaleresse)) |
| Dariusz Krantak      | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Dawid Szardak        | 1 | ((190326-arcymag-w-raju)) |
| Fergus Salien        | 1 | ((190326-arcymag-w-raju)) |
| Grzegorz Kamczarnik  | 1 | ((190326-arcymag-w-raju)) |
| Ignacy Myrczek       | 1 | ((191112-korupcja-z-arystokratki)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Juliusz Sowiński     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Kaja Selerek         | 1 | ((190402-eksperymentalny-power-suit)) |
| Keraina d'Orion      | 1 | ((200202-krucjata-chevaleresse)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Kornel Garn          | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Lena Kardamacz       | 1 | ((200219-perfekcyjna-corka)) |
| Leszek Szklarski     | 1 | ((200219-perfekcyjna-corka)) |
| Malictrix d'Itaran   | 1 | ((200202-krucjata-chevaleresse)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Mariola Tralment     | 1 | ((191105-zaginiona-soniczka)) |
| Martyn Hiwasser      | 1 | ((201104-sabotaz-swini)) |
| Nataniel Marszalnik  | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Olga Leszcz          | 1 | ((190326-arcymag-w-raju)) |
| OO Bubuta            | 1 | ((190123-parszywa-ekspedycja)) |
| OO Castigator        | 1 | ((190326-arcymag-w-raju)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Paweł Oszmorn        | 1 | ((200219-perfekcyjna-corka)) |
| Persefona d'Jastrząbiec | 1 | ((191112-korupcja-z-arystokratki)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Romeo Węglas         | 1 | ((190331-kurz-po-ataienne)) |
| Tadeusz Sklerzec     | 1 | ((191112-korupcja-z-arystokratki)) |
| Talia Aegis          | 1 | ((200202-krucjata-chevaleresse)) |
| Teresa Marszalnik    | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Tomasz Tukan         | 1 | ((200202-krucjata-chevaleresse)) |