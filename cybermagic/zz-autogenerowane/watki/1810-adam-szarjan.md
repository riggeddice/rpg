# Adam Szarjan
## Identyfikator

Id: 1810-adam-szarjan

## Sekcja Opowieści

### Kult Saitaera w Neotik

* **uid:** 211222-kult-saitaera-w-neotik, _numer względny_: 8
* **daty:** 0112-04-30 - 0112-05-01
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kasandra Destrukcja Diakon, Klaudia Stryk, Lutus Amerin, Maciek Kalmarzec, Maria Naavas

Streszczenie:

Klaudia wróciła z bronią anty-Serenitową od Tosena, Arianna z feromonami Marii nadała Inferni nową kulturę - miłośnicy lolitek x kultyści x noktianie. Elena uziemiona w Inferni zinfiltrowała Stocznię ukryta przez Klaudię i odkryła niższe Spustoszenie + kult Saitaera. Nereida jest czysta od Saitaera; przetransferowana na Infernię dla Eleny i uzbrojona. Klaudia ma listę jednostek potencjalnie w rękach Saitaera / Spustoszonych. Przygotowania do kontrataku skończone.

Aktor w Opowieści:

* Dokonanie:
    * stoi za przekazaniem Nereidy na testy Elenie; w pełni współpracuje z Arianną i oddaje jej się pod komendę nieformalnie. Przekonał Stocznię, że Nereidę przekazaną Elenie należy prawidłowo uzbroić.


### Sklejanie Inferni do kupy

* **uid:** 211215-sklejanie-inferni-do-kupy, _numer względny_: 7
* **daty:** 0112-04-27 - 0112-04-29
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Kamil Lyraczek, Kasandra Destrukcja Diakon, Leona Astrienko, Maria Naavas, OO Infernia, Roland Sowiński

Streszczenie:

Ixioński wariant Inferni okazuje się całkiem słodki i pod kontrolą Eustachego. Morale Inferni jest w ruinie. Z pomocą Marii Arianna przesuwa kulturę Inferni w stronę "kultyści + noktianie + miłośnicy psychopatycznej lolitki Diany". Eustachy wpada w pułapkę Leony, która chce go wyleczyć czerwiem Esuriit - ale Eustachy przekonuje ją, że ma Dianę pod kontrolą. Jednak gdzieś w tle znajduje się prawdziwy ixioński potwór na stoczni Neotik.

Aktor w Opowieści:

* Dokonanie:
    * przekonany przez Ariannę, że ten ixioński potwór na Neotik jest prawdziwy i nikt nie wie o jego istnieniu bo potencjalnie współpracuje z Hestią.


### O krok za daleko

* **uid:** 211208-o-krok-za-daleko, _numer względny_: 6
* **daty:** 0112-04-25 - 0112-04-26
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Hestia d'Neotik, Jarosław Szarjan, Klaudia Stryk, Leona Astrienko, OO Infernia, Wawrzyn Rewemis

Streszczenie:

Klaudia doszła do tego, że Hestia d'Neotik oszukuje Stocznię; w okolicy są niewielkie statki koloidowe o których Stocznia nie wie. Arianna zintegrowała Eustachego z Infernią ixiońsko, niestety anomalizując Infernię. Elena ciężko Skażona ixionem i (w swoim mniemaniu) porzucona przez Eustachego. Eustachy ujarzmiając Infernię rozbijał ją o różne asteroidy, co powoduje śmierć części załogi. Infernia ma zerowe morale, ale jest opanowana. Przyjazna Anomalia Kosmiczna z tymczasową bazą w Stoczni Neotik.

Aktor w Opowieści:

* Dokonanie:
    * przerażony, wstrząśnięty i zszokowany wszystkim co wydarzyło się na Inferni - próba ekstrakcji ixiońskiej, integracji Eustachego... te śmierci itp. Oddelegowany na Infernię przez ojca (Jarka Szarjana).
* Progresja:
    * oddelegowany na Infernię przez Jarosława Szarjana jako wsparcie dla Inferni i obserwator, by Natalia się dobrze wykluła.


### Prototypowa Nereida Natalii

* **uid:** 211124-prototypowa-nereida-natalii, _numer względny_: 5
* **daty:** 0112-04-20 - 0112-04-24
* **obecni:** Adam Szarjan, Arianna Verlen, Arkadia Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Maria Naavas, Natalia Aradin, OO Infernia, Roland Sowiński, Wawrzyn Rewemis

Streszczenie:

Kampania reklamowa "dołącz do Inferni" zrobiona przez Izę przy użyciu Eustachego jako modela - sukces. Klaudia zdobyła z tego nowych załogantów (acz to jeszcze potrwa). Szarjan uwolnił Klaudię z zarzutów, potrzebuje pomocy w stoczni - jego przyjaciółka Natalia ma problem z prototypem myśliwca klasy Nereida i została ixiońsko zintegrowana. Infernia próbowała Nereidę uratować, ale niestety skończyło się to poważnym uszkodzeniem Nereidy i stworzeniem ixiońskiego kokonu, z którego Natalia musi się wykluć...

Aktor w Opowieści:

* Dokonanie:
    * by uratować Natalię wyciągnął swoimi kanałami Klaudię z procesu i przekazał Inferni info o projekcie "Nereida". Niestety, jego marzenia się nie spełniły - Natalia ucierpiała.


### Odzyskana agentka Orbitera

* **uid:** 190724-odzyskana-agentka-orbitera, _numer względny_: 4
* **daty:** 0110-06-12 - 0110-06-15
* **obecni:** Adam Szarjan, Aida Serenit, Julia Morwisz, Mirela Niecień, Mirela Orion, Moktar Gradon, Pięknotka Diakon

Streszczenie:

Aidę porwała frakcja Cieniaszczytu zainspirowana przez Julię - by ratować Emulatorkę. Ale Aida, jak się okazało, jest nędznym cywilem i to takim uratowanym z kosmosu. Pięknotka wygrała z championem kralotha przez zanęcenie Moktara - a zwabiła go dając mu walkę z Mirelą. Ogólnie, wszyscy są zadowoleni..?

Aktor w Opowieści:

* Dokonanie:
    * powiedział Pięknotce, że Aida nie jest przecież jakąś elitką czy nikim ważnym. Uratowana z kapsuły w kosmosie.


### Romuald i Julia

* **uid:** 181118-romuald-i-julia, _numer względny_: 3
* **daty:** 0109-10-27 - 0109-10-29
* **obecni:** Adam Szarjan, Julia Morwisz, Pietro Dwarczan, Pięknotka Diakon, Romuald Czurukin

Streszczenie:

Pięknotka pomogła Romualdowi uratować jego ukochaną Julię. Po drodze był też groźny robot bojowy. Docelowo okazało się, że "Julia" jest Emulatorem - agentem Orbitera Pierwszego której celem było zaprzyjaźnić się z Ateną i Pięknotką by je obserwować. Pięknotka zneutralizowała Emulatora Romualdem i jego miłością plus Pryzmat, acz dwa razy skończyła w szpitalu podczas jednej sesji (rekord).

Aktor w Opowieści:

* Dokonanie:
    * hipernetowe źródło informacji dla Pięknotki odnośnie symboli z Laboratorium Technomantycznego Orbitera Pierwszego.


### Decyzja Minerwy

* **uid:** 181024-decyzja-minerwy, _numer względny_: 2
* **daty:** 0109-10-01 - 0109-10-04
* **obecni:** Adam Szarjan, Adela Kirys, Lilia Ursus, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Atena i Erwin są w ruinie. Tymczasem servar Erwina wyrwał się na wolność. Okazało się, że Minerwa - dawna miłość Erwina - powróciła. Adam próbował ją zneutralizować a Pięknotka - uratować. Skończyło się na tym, że Minerwa, Nutka i terrorform współistnieją w servarze Erwina, Adama boli głowa a Pięknotka cieszy się, że tymczasowo to nie jej problem. Mistrzyni psychotroniki wróciła.

Aktor w Opowieści:

* Dokonanie:
    * dążył do zniszczenia psychotroniki Minerwy; bał się terrorforma i chciał, by Erwin oraz wszyscy dookoła byli bezpieczni.
* Progresja:
    * bardzo nieufny wobec Minerwy; ogólnie, pilnuje, by nic złego się nie stało


### Powrót Minerwy z terrorforma

* **uid:** 181021-powrot-minerwy-z-terrorforma, _numer względny_: 1
* **daty:** 0109-09-24 - 0109-09-29
* **obecni:** Adam Szarjan, Atena Sowińska, Erwin Galilien, Lilia Ursus, Pięknotka Diakon

Streszczenie:

Adam Szarjan, dawny kolega Erwina pojawił się by uratować go przed problemami z Nutką. Okazało się, że Nutka jest śmiertelnym zagrożeniem pod wpływem energii Nojrepów. Wraz z Ateną i Pięknotką udało im się zneutralizować problem, ale w miejsce Nutki pojawiła się Minerwa - prawdziwa osoba, na bazie której powstała Nutka. Atena i Erwin w bardzo ciężkim stanie trafili do szpitala.

Aktor w Opowieści:

* Dokonanie:
    * o dziwo, przybył by pomóc Erwinowi bez ukrytej agendy. Dużo poświęcił, by pomóc "Nutce" stać się Minerwą. Bardzo pozytywna postać.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 8, @: 0112-05-01
    1. Primus    : 8, @: 0112-05-01
        1. Sektor Astoriański    : 8, @: 0112-05-01
            1. Astoria, Orbita    : 1, @: 0112-04-24
                1. Kontroler Pierwszy    : 1, @: 0112-04-24
            1. Astoria, Pierścień Zewnętrzny    : 4, @: 0112-05-01
                1. Poligon Stoczni Neotik    : 2, @: 0112-04-26
                1. Stocznia Neotik    : 4, @: 0112-05-01
                    1. Zewnętrzny dok ixioński    : 1, @: 0112-04-29
            1. Astoria    : 4, @: 0110-06-15
                1. Sojusz Letejski    : 4, @: 0110-06-15
                    1. Przelotyk    : 2, @: 0110-06-15
                        1. Przelotyk Wschodni    : 2, @: 0110-06-15
                            1. Cieniaszczyt    : 2, @: 0110-06-15
                                1. Knajpka Szkarłatny Szept    : 2, @: 0110-06-15
                                1. Kompleks Nukleon    : 2, @: 0110-06-15
                                1. Wewnątrzzbocze Zachodnie    : 1, @: 0109-10-29
                                    1. Panorama Światła    : 1, @: 0109-10-29
                            1. Przejściak    : 1, @: 0110-06-15
                                1. Hotel Pirat    : 1, @: 0110-06-15
                    1. Szczeliniec    : 2, @: 0109-10-04
                        1. Powiat Pustogorski    : 2, @: 0109-10-04
                            1. Pustogor    : 2, @: 0109-10-04
                                1. Eksterior    : 1, @: 0109-10-04
                                    1. Miasteczko    : 1, @: 0109-10-04
                                1. Gabinet Pięknotki    : 1, @: 0109-09-29
                                1. Interior    : 1, @: 0109-10-04
                                    1. Inkubator Ekonomiczny Samszarów    : 1, @: 0109-10-04
                                1. Kompleks Testowy    : 1, @: 0109-09-29
                                1. Pustułka    : 1, @: 0109-09-29
                            1. Trzęsawisko Zjawosztup    : 1, @: 0109-10-04

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 4 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Elena Verlen         | 4 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Eustachy Korkoran    | 4 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Pięknotka Diakon     | 4 | ((181021-powrot-minerwy-z-terrorforma; 181024-decyzja-minerwy; 181118-romuald-i-julia; 190724-odzyskana-agentka-orbitera)) |
| Diana d'Infernia     | 3 | ((211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Klaudia Stryk        | 3 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik)) |
| Leona Astrienko      | 3 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy)) |
| Maria Naavas         | 3 | ((211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| OO Infernia          | 3 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy)) |
| Izabela Zarantel     | 2 | ((211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik)) |
| Julia Morwisz        | 2 | ((181118-romuald-i-julia; 190724-odzyskana-agentka-orbitera)) |
| Kasandra Destrukcja Diakon | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Lilia Ursus          | 2 | ((181021-powrot-minerwy-z-terrorforma; 181024-decyzja-minerwy)) |
| Roland Sowiński      | 2 | ((211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Wawrzyn Rewemis      | 2 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| Adela Kirys          | 1 | ((181024-decyzja-minerwy)) |
| Aida Serenit         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Atena Sowińska       | 1 | ((181021-powrot-minerwy-z-terrorforma)) |
| Erwin Galilien       | 1 | ((181021-powrot-minerwy-z-terrorforma)) |
| Hestia d'Neotik      | 1 | ((211208-o-krok-za-daleko)) |
| Jarosław Szarjan     | 1 | ((211208-o-krok-za-daleko)) |
| Kamil Lyraczek       | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Lutus Amerin         | 1 | ((211222-kult-saitaera-w-neotik)) |
| Maciek Kalmarzec     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Minerwa Metalia      | 1 | ((181024-decyzja-minerwy)) |
| Mirela Niecień       | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Mirela Orion         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Moktar Gradon        | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Natalia Aradin       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Pietro Dwarczan      | 1 | ((181118-romuald-i-julia)) |
| Romuald Czurukin     | 1 | ((181118-romuald-i-julia)) |