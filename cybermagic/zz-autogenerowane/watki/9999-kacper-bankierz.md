# Kacper Bankierz
## Identyfikator

Id: 9999-kacper-bankierz

## Sekcja Opowieści

### Zgubiony holokryształ w lesie

* **uid:** 210817-zgubiony-holokrysztal-w-lesie, _numer względny_: 6
* **daty:** 0111-06-22 - 0111-06-24
* **obecni:** Alan Bartozol, Cyryl Perikas, Kacper Bankierz, Laura Tesinik, Liliana Bankierz, Mariusz Trzewń, Marysia Sowińska, Rupert Mysiokornik, Stella Armadion, Tomasz Tukan

Streszczenie:

Jeden z przerzucanych przez mafię przedmiotów do Aurum to ko-matryca Kuratorów, jako broń. Ale Cyryl - kurier - był ofiarą terminuskiego polowania więc ją odrzucił w Lesie Trzęsawnym gdzie znalazła ją Stella przekonana, że to noktiańska technologia. Marysia wypchnęła Kacpra Bankierza z mafii (i tego terenu) a Tukan znalazł i usunął ko-matrycę m.in. używając Laury.

Aktor w Opowieści:

* Dokonanie:
    * po tym jak jego kurier (Cyryl) zgubił ko-matrycę Kuratorów, jest zmuszony do wycofania się z tego terenu. Nie odda ko-matrycy mafii (zwłaszcza że Marysia ją zdobyła)
* Progresja:
    * ma poważnie przechlapane u mafii Grzymościa (Wolny Uśmiech), bo "zgubił" ko-matrycę i na jej miejsce podłożył fałszywkę (czego NIE zrobił on a Pustogor).


### Rekin wspiera mafię

* **uid:** 210713-rekin-wspiera-mafie, _numer względny_: 5
* **daty:** 0111-06-06 - 0111-06-09
* **obecni:** Julia Kardolin, Kacper Bankierz, Marysia Sowińska, Rafał Torszecki, Tomasz Tukan, Urszula Miłkowicz

Streszczenie:

Gabriel Ursus powiedział Marysi, że jakiś Rekin uczestniczy w przemycie nielegalnych rzeczy w Podwiercie, na terenie eksterytorialnym. I będzie rajd i przechwycą te Rekiny. Marysia dotarła do tego, że są tajne magazyny (chronione przez anomalię) pod kontrolą Kacpra Bankierza. Rozmowa z Kacprem nic nie dała, więc Marysia doprowadziła do uwolnienia Arkadii z więzienia i zrobiła fałszywe ślady - to Kacper stał za jej odurzeniem. Plus, Marysia odbudowała kontakt z Tukanem...

Aktor w Opowieści:

* Dokonanie:
    * współpracuje z mafią Kajrata by być KIMŚ na tym terenie i nie tylko. Nie chciał się dzielić z Marysią i używa kurierów (agentów) by sam nie ucierpieć. Bezpośrednia kolizja z praworządną Arkadią.
* Progresja:
    * wrobiony przez Marysię w to, że niby to on otruł Arkadię. A jest niewinny.


### Grzybopreza

* **uid:** 210323-grzybopreza, _numer względny_: 4
* **daty:** 0111-04-22 - 0111-04-23
* **obecni:** Ignacy Myrczek, Julia Kardolin, Kacper Bankierz, Marysia Sowińska, Napoleon Bankierz, Triana Porzecznik, Urszula Miłkowicz

Streszczenie:

Trianie "uciekł" źle skonfigurowany techno-pies ratujący ludzi. Marysia Sowińska (Rekin) porwała Myrczka na pomysł Julii, zrobili super-trufle i Triana z Julią dotarły do psa i go unieczynniły. Tymczasem Marysia i Myrczek wymanewrowały Ulę (uczennicę terminusa) i sprowokowali ją do wezwania wsparcia przeciw ad-hocowej czystej grzyboprezie Rekinów. Nawet Pakt o tym napisał.

Aktor w Opowieści:

* Dokonanie:
    * gorąco pogratulował Marysi Sowińskiej wpakowanie Napoleona do jeziora. Potem namówiony przez Marysię zrobił grzybową CZYSTĄ imprezę ze swoimi Rekinami - by tylko wpakować w kłopoty terminusów i by się to głośno odbiło.
* Progresja:
    * szacunek wobec Marysi Sowińskiej - nie tylko jest SOWIŃSKA, ale też wyrolowała terminuskę (Ulę) oraz wpakowała Napoleona do jeziora na jego ścigaczu.


### Przygoda, randka i porwanie

* **uid:** 201020-przygoda-randka-i-porwanie, _numer względny_: 3
* **daty:** 0110-10-25 - 0110-10-27
* **obecni:** Daniel Sowiński, Gabriel Ursus, Henryk Sowiński, Kacper Bankierz, Laurencjusz Sorbian, Liliana Bankierz, Robert Pakiszon, Robinson Porzecznik, Tomasz Tukan, Triana Porzecznik, Tymon Grubosz, Urszula Miłkowicz

Streszczenie:

Młodzi i niekompetentni Sowińscy łażą po Zaczęstwie i szukają przygód. Gdy ich dyskretny opiekun został aresztowany, szybko ściągnął Roberta i Gabriela do pomocy - by zrobili przygodę. Jednocześnie Triana chciała podwójnej randki i też robiła przygodę z Lilianą. Skończyło się na tym, że wszystkie przygody się pomieszały, Triana została porwana przez siły Zespołu (!?) i walnęła efemeryda na budowanym biurowcu Gorland. Ale - 'stream must go on'.

Aktor w Opowieści:

* Dokonanie:
    * młodzi Sowińscy doprowadzili do pojedynku, po czym ich opiekunowie oszukali. Kacper ma z nimi kosę. Ale przy efemerydzie - pomógł ją rozmontować. I wyciągnął arystokratów - nikt nie zasłużył na areszt przez Tymona, to nie-Aurumowate.
* Progresja:
    * Henryk i Daniel Sowińscy mają u niego dług honorowy, który ma zamiar wykorzystać w podły dla nich sposób.


### Pojedynek: Akademia - Rekiny

* **uid:** 201013-pojedynek-akademia-rekiny, _numer względny_: 2
* **daty:** 0110-10-14 - 0110-10-22
* **obecni:** Aleksander Bemucik, Ignacy Myrczek, Julia Kardolin, Justynian Diakon, Kacper Bankierz, Liliana Bankierz, Napoleon Bankierz, Remor 340D, Robert Pakiszon, Stella Armadion, Teresa Mieralit

Streszczenie:

Kolejna ustawka między Rekinami a uczniami AMZ mogłaby się skończyć bardzo źle, więc grupa uczniów spróbowała wprowadzić sposób rozwiązywania konfliktu przez turnieje między Rekinami i AMZ. Nie tylko im się to udało - użycie starego ścigacza wojskowego Remor 340D i echo emocji wojen noktiańskich sprawiło, że Rekiny się unormowały i zainwestowano w budowę Toru Wyścigowego Pamięci w Podwiercie.

Aktor w Opowieści:

* Dokonanie:
    * wieczny przeciwnik Liliany - Rekin, który gardzi podejściem Liliany i uważa ją za impostora w rodzie. Zwykle robił przeciw niej ustawki. Ma małą frakcję Rekinów.
* Progresja:
    * do jego ścigacza połączyło się echo 'ustawki' i efemerydy. Ścigacz robi czasem "swoje rzeczy".


### Glizda, która leczy

* **uid:** 211120-glizda-ktora-leczy, _numer względny_: 1
* **daty:** 0108-04-07 - 0108-04-18
* **obecni:** Amanda Kajrat, Amelia Sowińska, Ernest Kajrat, Feliks Keksik, Justynian Diakon, Kacper Bankierz, Karol Pustak, Oliwia Lemurczak, Roland Sowiński, Sabina Kazitan, Sensacjusz Diakon, Stella Sowińska

Streszczenie:

Sensacjusz Diakon został wysłany na prowincję pod przykrywką Lekarza Rekinów, by dojść ze Stellą do tego czemu Amelia i Roland Sowińscy zachowują się nietypowo. Na miejscu okazało się, że Roland jest w stanie ostrej wojny z mafią (która nie traktuje tego poważnie; Kajrat się bawi z Rekinami) a Amelia po prostu nie w pełni sobie radzi i próbuje opanować sytuację. Roland jest Skażony Esuriit; Sensacjusz przetransportował go glizdą do Aurum i sam, uwięziony w bojowej formie, deeskalował konflikt z mafią. A Amelia jest tymczasowym dowódcą Rekinów...

Aktor w Opowieści:

* Dokonanie:
    * w nagraniu polecał innym arystokratom przyjechanie na prowincję, BO TU JEST MEGA FAJNIE! Amelia robi zawody z bicia się żuli i w ogóle!


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 6, @: 0111-06-24
    1. Primus    : 6, @: 0111-06-24
        1. Sektor Astoriański    : 6, @: 0111-06-24
            1. Astoria    : 6, @: 0111-06-24
                1. Sojusz Letejski    : 6, @: 0111-06-24
                    1. Szczeliniec    : 6, @: 0111-06-24
                        1. Powiat Pustogorski    : 6, @: 0111-06-24
                            1. Czółenko    : 1, @: 0111-06-24
                            1. Podwiert    : 4, @: 0111-06-24
                                1. Dzielnica Luksusu Rekinów    : 3, @: 0111-06-24
                                    1. Obrzeża Biedy    : 1, @: 0111-06-09
                                        1. Stadion Lotników    : 1, @: 0111-06-09
                                    1. Sektor Brudu i Nudy    : 1, @: 0111-06-09
                                        1. Skrytki Czereśniaka    : 1, @: 0111-06-09
                                    1. Serce Luksusu    : 2, @: 0111-06-09
                                        1. Apartamentowce Elity    : 1, @: 0111-06-09
                                        1. Lecznica Rannej Rybki    : 1, @: 0108-04-18
                                1. Las Trzęsawny    : 3, @: 0111-06-24
                                1. Sensoplex    : 1, @: 0110-10-22
                                1. Tor Wyścigowy Pamięci    : 1, @: 0110-10-22
                            1. Zaczęstwo    : 3, @: 0111-04-23
                                1. Akademia Magii, kampus    : 2, @: 0111-04-23
                                    1. Akademik    : 1, @: 0111-04-23
                                    1. Artefaktorium    : 1, @: 0110-10-22
                                    1. Audytorium    : 1, @: 0110-10-22
                                1. Bazar Różności    : 1, @: 0110-10-27
                                1. Biurowiec Gorland    : 2, @: 0111-04-23
                                1. Dzielnica Kwiecista    : 2, @: 0111-04-23
                                    1. Rezydencja Porzeczników    : 2, @: 0111-04-23
                                        1. Podziemne Laboratorium    : 2, @: 0111-04-23
                                1. Kawiarenka Leopold    : 1, @: 0110-10-27
                                1. Nieużytki Staszka    : 3, @: 0111-04-23
                                1. Park Centralny    : 1, @: 0111-04-23
                                    1. Jezioro Gęsie    : 1, @: 0111-04-23

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Julia Kardolin       | 3 | ((201013-pojedynek-akademia-rekiny; 210323-grzybopreza; 210713-rekin-wspiera-mafie)) |
| Liliana Bankierz     | 3 | ((201013-pojedynek-akademia-rekiny; 201020-przygoda-randka-i-porwanie; 210817-zgubiony-holokrysztal-w-lesie)) |
| Marysia Sowińska     | 3 | ((210323-grzybopreza; 210713-rekin-wspiera-mafie; 210817-zgubiony-holokrysztal-w-lesie)) |
| Tomasz Tukan         | 3 | ((201020-przygoda-randka-i-porwanie; 210713-rekin-wspiera-mafie; 210817-zgubiony-holokrysztal-w-lesie)) |
| Urszula Miłkowicz    | 3 | ((201020-przygoda-randka-i-porwanie; 210323-grzybopreza; 210713-rekin-wspiera-mafie)) |
| Ignacy Myrczek       | 2 | ((201013-pojedynek-akademia-rekiny; 210323-grzybopreza)) |
| Justynian Diakon     | 2 | ((201013-pojedynek-akademia-rekiny; 211120-glizda-ktora-leczy)) |
| Napoleon Bankierz    | 2 | ((201013-pojedynek-akademia-rekiny; 210323-grzybopreza)) |
| Robert Pakiszon      | 2 | ((201013-pojedynek-akademia-rekiny; 201020-przygoda-randka-i-porwanie)) |
| Stella Armadion      | 2 | ((201013-pojedynek-akademia-rekiny; 210817-zgubiony-holokrysztal-w-lesie)) |
| Triana Porzecznik    | 2 | ((201020-przygoda-randka-i-porwanie; 210323-grzybopreza)) |
| Alan Bartozol        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Aleksander Bemucik   | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Amanda Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Amelia Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Cyryl Perikas        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Daniel Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Ernest Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Feliks Keksik        | 1 | ((211120-glizda-ktora-leczy)) |
| Gabriel Ursus        | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Henryk Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Karol Pustak         | 1 | ((211120-glizda-ktora-leczy)) |
| Laura Tesinik        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Laurencjusz Sorbian  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Mariusz Trzewń       | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Oliwia Lemurczak     | 1 | ((211120-glizda-ktora-leczy)) |
| Rafał Torszecki      | 1 | ((210713-rekin-wspiera-mafie)) |
| Remor 340D           | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Robinson Porzecznik  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Roland Sowiński      | 1 | ((211120-glizda-ktora-leczy)) |
| Rupert Mysiokornik   | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Sabina Kazitan       | 1 | ((211120-glizda-ktora-leczy)) |
| Sensacjusz Diakon    | 1 | ((211120-glizda-ktora-leczy)) |
| Stella Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Teresa Mieralit      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Tymon Grubosz        | 1 | ((201020-przygoda-randka-i-porwanie)) |