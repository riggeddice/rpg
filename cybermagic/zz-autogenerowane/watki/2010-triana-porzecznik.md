# Triana Porzecznik
## Identyfikator

Id: 2010-triana-porzecznik

## Sekcja Opowieści

### Lea, strażniczka lasu

* **uid:** 240305-lea-strazniczka-lasu, _numer względny_: 7
* **daty:** 0111-10-21 - 0111-10-25
* **obecni:** Arkadia Verlen, Bogdan Gwiazdocisz, Ernest Namertel, Karolina Terienak, Keira Amarco d'Namertel, Lea Samszar, Malena Barandis, Marek Samszar, Mariusz Kupieczka, Marysia Sowińska, Michał Klabacz, Triana Porzecznik

Streszczenie:

Marysia zatrzymała Leę przed naruszaniem kulturowego tabu eternijskiego (stroju) i przy okazji dowiedziała się, że Lea próbuje pomóc innym, ale nikt nie może o tym wiedzieć. Tymczasem jej EfemeHorror poranił grupę Rekinów i AMZ współpracujących ze sobą w celu pomocy staremu 'archeologowi' szukającemu prawdy o ofiarach wojny.

Aktor w Opowieści:

* Dokonanie:
    * próbowała pomóc Gwiazdociszowi w sprawie badań z czasów wojny. Ucierpiała przez EfemeHorror.


### Pierwszy tajemniczy wielbiciel Liliany

* **uid:** 230212-pierwszy-tajemniczy-wielbiciel-liliany, _numer względny_: 6
* **daty:** 0111-10-22 - 0111-10-24
* **obecni:** Julia Kardolin, Laura Tesinik, Liliana Bankierz, Mariusz Kupieczka, Triana Porzecznik

Streszczenie:

Liliana, Triana, Julia pracują nad formą integracji scutiera i lancera by Liliana mogła pokonać (lub choć wytrzymać) Arkadię Verlen. Triana dostała alarm ze swojej zaginiętej drony o nieprzytomnych ludziach na Nieużytkach. Okazuje się, że portret Liliany wyssał tych ludzi. Okazuje się, że student AMZ, Mariusz, zrobił portret by Lilianę poderwać, ale spartaczył; porter wysysa. Potem portret został ukradziony. Liliana dalej podejrzewa spiski. Gdy Liliana i Mariusz się konfrontują, okazuje się że były 3 portrety. Liliana zachowuje się nikczemnie wobec Mariusza, więc się szybko odkochał. Ona nie skojarzyła, że komuś się może podobać.

Aktor w Opowieści:

* Dokonanie:
    * AMZ. Chce pomóc Lilianie w zrobieniu servara zdolnego do starcia z Arkadią, ale nie wie jak. Jej zagubiona drona wysłała sygnał SOS bo ludzie są nieprzytomni. Po policzeniu prawdopodobieństw, to jest możliwe i Triana śpi spokojnie.


### Verlenka na grzybkach

* **uid:** 210622-verlenka-na-grzybkach, _numer względny_: 5
* **daty:** 0111-05-30 - 0111-05-31
* **obecni:** Arkadia Verlen, Ignacy Myrczek, Julia Kardolin, Liliana Bankierz, Marek Samszar, Marysia Sowińska, Rafał Torszecki, Różewicz Diakon, Triana Porzecznik, Urszula Miłkowicz

Streszczenie:

Zespół AMZ składał projekt zaliczeniowy, gdy zaatakowała Arkadia Verlen na grzybkach ze swojego Lancera, uszkadzając projekt (robota). Julia wezwała Ulę (terminuskę) na pomoc, Ula unieszkodliwiła Marka (który podmienił Arkadię). Jako, że Julia i Triana były zajęte ratowaniem projektu, Marysia rzuciła się na znalezienie "kto atakuje Julię". Dotarła do niewinności Marka, dotarła do Arkadii i z pomocą Uli unieszkodliwiła Arkadię. Jedyne straty - ranna Ula i ranny Torszecki. Ale kto podał Arkadii te grzybki i jak?

Aktor w Opowieści:

* Dokonanie:
    * pracuje nad robotem kultywacyjnym; zupełnie nie radzi sobie jak jest ostrzeliwana przez Lancera.


### Porywaczka miragentów

* **uid:** 210518-porywaczka-miragentow, _numer względny_: 4
* **daty:** 0111-05-07 - 0111-05-08
* **obecni:** Diana Lemurczak, Julia Kardolin, Lorena Gwozdnik, Marysia Sowińska, Triana Porzecznik

Streszczenie:

Lorena chciała zaimponować Gerwazemu i wysłała na Dianę miragenta. Diana przejęła miragenta, ale ten w rozpaczy ostrzelał Marysię Sowińską. Zespół znalazł Lorenę, zestrzelił ją (berserkerskimi dronami Triany) i poznał prawdę o miragencie, po czym udał się do klubu Arkadia w Podwiercie i odebrał Dianie miragenta. Nie powiedzieli nikomu innemu, że wiedzą o jej Eidolonie.

Aktor w Opowieści:

* Dokonanie:
    * chciała oglądać horror a zrobiła ŁZy nad którymi straciła kontrolę (i które obróciły się przeciw wszystkim). Drony mają autonaprawę oprogramowania i w ogóle.


### Potencjalnie eksterytorialny seksbot

* **uid:** 210406-potencjalnie-eksterytorialny-seksbot, _numer względny_: 3
* **daty:** 0111-04-27 - 0111-04-28
* **obecni:** Barnaba Burgacz, Feliks Keksik, Franek Bulterier, Henryk Wkrąż, Julia Kardolin, Lorena Gwozdnik, Marysia Sowińska, Rupert Mysiokornik, Sabina Kazitan, Triana Porzecznik

Streszczenie:

Sabina Kazitan zostawiła za sobą plan mający skonfliktować Rekiny na poziomie Podwiertu, co miałoby kaskadować na relacje rodów w Aurum. Plan zawierał Skażonego seksbota, którego Rekiny miały same zrobić. Podszepnęła też odpowiednie słówka odpowiednim osobom. Jednak gdy podatne Rekiny chciały iść za tym planem, gdy szabrowały ze złomowiska głowę seksbota to zatrzymał je Zespół. Skończyło się na tym, że seksbota nie będzie - Marysia im absolutnie zakazała cokolwiek robić, nawet, jeśli nie znała planu Sabiny.

Aktor w Opowieści:

* Dokonanie:
    * statystka. 3 miesiące temu zalana, spała i Wkrąż dorysowywał jej kocie uszka. Teraz: gdy potrzebna była interwencja na złomowisku, zakładała i kalibrowała servara za długo i problem się rozwiązał.
* Progresja:
    * ma włączony na stałe monitoring Złomowiska w Zaczęstwie. By nikt nie szabrował... lub by coś nieupoważnionego nie wyszło.


### Grzybopreza

* **uid:** 210323-grzybopreza, _numer względny_: 2
* **daty:** 0111-04-22 - 0111-04-23
* **obecni:** Ignacy Myrczek, Julia Kardolin, Kacper Bankierz, Marysia Sowińska, Napoleon Bankierz, Triana Porzecznik, Urszula Miłkowicz

Streszczenie:

Trianie "uciekł" źle skonfigurowany techno-pies ratujący ludzi. Marysia Sowińska (Rekin) porwała Myrczka na pomysł Julii, zrobili super-trufle i Triana z Julią dotarły do psa i go unieczynniły. Tymczasem Marysia i Myrczek wymanewrowały Ulę (uczennicę terminusa) i sprowokowali ją do wezwania wsparcia przeciw ad-hocowej czystej grzyboprezie Rekinów. Nawet Pakt o tym napisał.

Aktor w Opowieści:

* Dokonanie:
    * odrestaurowała ratunkowego psa wojskowego (K9Epickor), który dał w długą. Pomogła Myrczkowi zrobić super-trufle. Następnie z pomocą Julii wyłączyła K9 i go zwinęła, unikając uwagi Uli (uczennicy terminusa).
* Progresja:
    * pozyskała w miarę sprawnego technopsa militarnego służącego do ratowania ludzi. Nazwała go "K9Epickor". Przy AI pomagała Julia Kardolin.


### Przygoda, randka i porwanie

* **uid:** 201020-przygoda-randka-i-porwanie, _numer względny_: 1
* **daty:** 0110-10-25 - 0110-10-27
* **obecni:** Daniel Sowiński, Gabriel Ursus, Henryk Sowiński, Kacper Bankierz, Laurencjusz Sorbian, Liliana Bankierz, Robert Pakiszon, Robinson Porzecznik, Tomasz Tukan, Triana Porzecznik, Tymon Grubosz, Urszula Miłkowicz

Streszczenie:

Młodzi i niekompetentni Sowińscy łażą po Zaczęstwie i szukają przygód. Gdy ich dyskretny opiekun został aresztowany, szybko ściągnął Roberta i Gabriela do pomocy - by zrobili przygodę. Jednocześnie Triana chciała podwójnej randki i też robiła przygodę z Lilianą. Skończyło się na tym, że wszystkie przygody się pomieszały, Triana została porwana przez siły Zespołu (!?) i walnęła efemeryda na budowanym biurowcu Gorland. Ale - 'stream must go on'.

Aktor w Opowieści:

* Dokonanie:
    * zaprojektowała double date z Lilianą i Sowińskimi; wyłączyła alarmy w Rezydencji i stworzyła opowieść o Upiorze. Porwana przez pomyłkę przez agentów Zespołu.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 7, @: 0111-10-25
    1. Primus    : 7, @: 0111-10-25
        1. Sektor Astoriański    : 7, @: 0111-10-25
            1. Astoria    : 7, @: 0111-10-25
                1. Sojusz Letejski    : 7, @: 0111-10-25
                    1. Szczeliniec    : 7, @: 0111-10-25
                        1. Powiat Pustogorski    : 7, @: 0111-10-25
                            1. Podwiert    : 4, @: 0111-10-25
                                1. Dzielnica Luksusu Rekinów    : 4, @: 0111-10-25
                                    1. Obrzeża Biedy    : 2, @: 0111-05-31
                                        1. Domy Ubóstwa    : 1, @: 0111-05-31
                                        1. Stadion Lotników    : 1, @: 0111-04-28
                                    1. Serce Luksusu    : 1, @: 0111-05-08
                                        1. Apartamentowce Elity    : 1, @: 0111-05-08
                                1. Las Trzęsawny    : 4, @: 0111-10-25
                                1. Osiedle Leszczynowe    : 1, @: 0111-05-08
                                    1. Klub Arkadia    : 1, @: 0111-05-08
                            1. Pustogor    : 1, @: 0111-05-31
                                1. Rdzeń    : 1, @: 0111-05-31
                                    1. Barbakan    : 1, @: 0111-05-31
                                        1. Kazamaty    : 1, @: 0111-05-31
                            1. Zaczęstwo    : 6, @: 0111-10-24
                                1. Akademia Magii, kampus    : 2, @: 0111-10-24
                                    1. Akademik    : 1, @: 0111-04-23
                                    1. Artefaktorium    : 1, @: 0111-10-24
                                1. Bazar Różności    : 1, @: 0110-10-27
                                1. Biurowiec Gorland    : 2, @: 0111-04-23
                                1. Dzielnica Kwiecista    : 5, @: 0111-10-24
                                    1. Rezydencja Porzeczników    : 5, @: 0111-10-24
                                        1. Garaż Groźnych Eksperymentów    : 1, @: 0111-10-24
                                        1. Podziemne Laboratorium    : 2, @: 0111-04-23
                                1. Dzielnica Ogrodów    : 1, @: 0111-10-24
                                    1. Wielki Szpital Magiczny    : 1, @: 0111-10-24
                                1. Kawiarenka Leopold    : 1, @: 0110-10-27
                                1. Las Trzęsawny    : 1, @: 0111-05-31
                                1. Nieużytki Staszka    : 4, @: 0111-10-24
                                1. Park Centralny    : 1, @: 0111-04-23
                                    1. Jezioro Gęsie    : 1, @: 0111-04-23
                                1. Złomowisko    : 1, @: 0111-04-28

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Julia Kardolin       | 5 | ((210323-grzybopreza; 210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow; 210622-verlenka-na-grzybkach; 230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Marysia Sowińska     | 5 | ((210323-grzybopreza; 210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow; 210622-verlenka-na-grzybkach; 240305-lea-strazniczka-lasu)) |
| Liliana Bankierz     | 3 | ((201020-przygoda-randka-i-porwanie; 210622-verlenka-na-grzybkach; 230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Urszula Miłkowicz    | 3 | ((201020-przygoda-randka-i-porwanie; 210323-grzybopreza; 210622-verlenka-na-grzybkach)) |
| Arkadia Verlen       | 2 | ((210622-verlenka-na-grzybkach; 240305-lea-strazniczka-lasu)) |
| Ignacy Myrczek       | 2 | ((210323-grzybopreza; 210622-verlenka-na-grzybkach)) |
| Kacper Bankierz      | 2 | ((201020-przygoda-randka-i-porwanie; 210323-grzybopreza)) |
| Lorena Gwozdnik      | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow)) |
| Marek Samszar        | 2 | ((210622-verlenka-na-grzybkach; 240305-lea-strazniczka-lasu)) |
| Mariusz Kupieczka    | 2 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany; 240305-lea-strazniczka-lasu)) |
| Barnaba Burgacz      | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Bogdan Gwiazdocisz   | 1 | ((240305-lea-strazniczka-lasu)) |
| Daniel Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Diana Lemurczak      | 1 | ((210518-porywaczka-miragentow)) |
| Ernest Namertel      | 1 | ((240305-lea-strazniczka-lasu)) |
| Feliks Keksik        | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Franek Bulterier     | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Gabriel Ursus        | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Henryk Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Henryk Wkrąż         | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Karolina Terienak    | 1 | ((240305-lea-strazniczka-lasu)) |
| Keira Amarco d'Namertel | 1 | ((240305-lea-strazniczka-lasu)) |
| Laura Tesinik        | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Laurencjusz Sorbian  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Lea Samszar          | 1 | ((240305-lea-strazniczka-lasu)) |
| Malena Barandis      | 1 | ((240305-lea-strazniczka-lasu)) |
| Michał Klabacz       | 1 | ((240305-lea-strazniczka-lasu)) |
| Napoleon Bankierz    | 1 | ((210323-grzybopreza)) |
| Rafał Torszecki      | 1 | ((210622-verlenka-na-grzybkach)) |
| Robert Pakiszon      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Robinson Porzecznik  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Różewicz Diakon      | 1 | ((210622-verlenka-na-grzybkach)) |
| Rupert Mysiokornik   | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Sabina Kazitan       | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Tomasz Tukan         | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Tymon Grubosz        | 1 | ((201020-przygoda-randka-i-porwanie)) |