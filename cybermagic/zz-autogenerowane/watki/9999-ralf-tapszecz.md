# Ralf Tapszecz
## Identyfikator

Id: 9999-ralf-tapszecz

## Sekcja Opowieści

### Wojna o Arkologię Nativis - nowa regentka

* **uid:** 230719-wojna-o-arkologie-nativis-nowa-regentka, _numer względny_: 10
* **daty:** 0093-03-28 - 0093-03-29
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Izabella Saviripatel, Kalia Awiter, Marcel Draglin, OO Infernia, Ralf Tapszecz, Tobiasz Lobrak

Streszczenie:

Ardilla ma plan przejęcia kontroli nad Arkologią, w tle Kidiron przez nią kontrolowany. To też neutralizuje główne ataki L&T, że Kidiron taki zły. Z pomocą Kalii (ewakuowanej ze skrzydła medycznego) zbudowała linię propagandową i po odzyskaniu Radiowęzła, Kalia nadała wiadomość pokoju i pojednania. Eustachy zmiażdżył główne siły L&T koło Engineering, Kalia JAKIMŚ CUDEM została regentką przez przypadek a czarodziejka Syndykatu, Izabella została zmiażdżona w imię Nihilusa przez Ralfa chroniącego Ardillę.

Aktor w Opowieści:

* Dokonanie:
    * cień Ardilli, ochronił ją przed atakiem mentalnym przepisującym ją wzywając energię Nihilusa niszcząc umysł Izabelli. Jego ostatnią wartościową kotwicą jest Ardilla.


### Wojna o Arkologię Nativis - konsolidacja sił

* **uid:** 230628-wojna-o-arkologie-nativis-konsolidacja-sil, _numer względny_: 9
* **daty:** 0093-03-27 - 0093-03-28
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Eustachy Korkoran, Izabella Saviripatel, Karol Lertys, Laurencjusz Kidiron, Marcel Draglin, Ralf Tapszecz, Tobiasz Lobrak, Tymon Korkoran

Streszczenie:

W Nativis panuje wojna domowa - Laurencjusz i Tymon próbują przejąć władzę jako 'nowi lepsi Kidiron + Korkoran'. Eustachy i Ardilla konsolidują lojalistów arkologii - Lertysów, Draglina, Szczury - tworząc potężną siłę pro-Rafał Kidiron. Gdy Laurencjusz i Tymon wspomagani przez Lobraka próbują Eustachego przekonać do współpracy, Eustachy odrzuca. Oddziały Inferni z jakiegoś powodu krzywdzą cywili (co dziwi Eustachego i martwi Ardillę) i zbrodnie są dobrze nagłośnione, spadając na ręce Eustachego. Farighanowie są odparci. Szczury są złamane. I wiemy, że L+T współpracowali z Infiltratorem...

Aktor w Opowieści:

* Dokonanie:
    * ranny i niewidoczny; jest cieniem Ardilli.


### Infiltrator ucieka a Arkologia płonie

* **uid:** 230621-infiltrator-ucieka-a-arkologia-plonie, _numer względny_: 8
* **daty:** 0093-03-25 - 0093-03-26
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, BIA Prometeus, Dalmjer Servart, Eustachy Korkoran, Kalia Awiter, Laurencjusz Kidiron, Małgorzata Maratelus, OO Infernia, Rafał Kidiron, Ralf Tapszecz

Streszczenie:

Ardilla utrzymuje Kidirona przy życiu, negocjując dla niego bezpieczne miejsce w Szczurowisku i stawiając swoją i Ralfową reputację na szali. Potem - rozpaczliwie ratuje Prometeusa, acz z ciężkimi stratami. Eustachy w pogoni za niezwykle groźnym Infiltratorem musiał dać mu odejść; oddał Skorpiona i czterech załogantów, ale uratował Kalię Awiter. Tymczasem o Arkologię Nativis toczy się wojna dusz - Bartłomiej Korkoran kontra Laurencjusz Kidiron. A w tle eksperymenty Kidirona (jak np. farighanowie jako Hełmy) wyrywają się spod kontroli i zdecydowanie nie pomagają.

Aktor w Opowieści:

* Dokonanie:
    * wykazał się savarańską doktryną, strzelając do buntowników w Szczurowisku i poświęcając nawet swojego Lancera by chronić Prometeusa. Jest cieniem Ardilli, nie da jej zrobić krzywdy.


### Atak na Kidirona

* **uid:** 230614-atak-na-kidirona, _numer względny_: 7
* **daty:** 0093-03-22 - 0093-03-24
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Dalmjer Servart, Eustachy Korkoran, Kalia Awiter, Lycoris Kidiron, OO Infernia, Rafał Kidiron, Ralf Tapszecz, SAN Szare Ostrze, Stanisław Uczantor

Streszczenie:

Gdy Wujek się obudził, powiedział że z Infernią mogą być ostre problemy jeśli spada poniżej poziomu Memoriam. Zmartwił się hipotezą Ardilli że Ralf może być magiem Nihilusa, ale Ardilla walczyła o dalsze spotykanie się z Ralfem. Zamach na Kidirona. Większość jego sztabu umiera, ale sam Kidiron przeżywa, acz ciężko ranny; ratuje go Ardilla i pozyskuje info o jego mrocznych planach. Kalia bierze na siebie morale i propagandę, ale przez to wpada na celownik Infiltratora który ją porywa. Eustachy próbuje odbić Kalię z rąk Infiltratora w koloidzie...

Aktor w Opowieści:

* Dokonanie:
    * Ardilla podejrzewa go o bycie magiem Nihilusa. On jeszcze nie wie. Gdy doszło do ataku na arkologię, Ralf od razu pojawił się przy Ardilli i chroni ją i tylko ją. Skutecznie zgasił pożar drzew w arkologii, zaczynając od drzew które Ardilla lubi najbardziej.


### Zdrada rozrywająca arkologię

* **uid:** 230329-zdrada-rozrywajaca-arkologie, _numer względny_: 6
* **daty:** 0093-03-14 - 0093-03-16
* **obecni:** Amelia Sarkaldir, Ardilla Korkoran, BIA Prometeus, Celina Lertys, Eustachy Korkoran, Ewelina Paroknis, Feliks Kidiron, Franciszek Pietraszczyk, Iwona Paroknis, Jonasz Paroknis, Kalia Awiter, Katarzyna Falernik, Małgorzata Maratelus, OO Infernia, Rafał Kidiron, Ralf Tapszecz, Szczepan Falernik, Wojciech Grzebawron

Streszczenie:

Nastolatka miała pamiętnik czytany przez macochę. Ojciec to ignorował. Nastolatka napisała w pamiętniku, że ojciec zdradza macochę z ekspertką hydrologii. Zrobił się shitstorm który doprowadził do poważnych problemów - m.in. macocha (dyrektor skrzydła medycznego) zaczęła robić błędy co wykryła Celina. Kidiron podejrzewa spisek - ktoś próbuje skłócić ważne rodziny, a także shackować Prometeusa (BIA arkologii). Rozmowa z ich eks-piratami kończy się tym, że to nie oni - za tym stoi wyspecjalizowana siła z innej arkologii lub noktianie. 

Ślady prowadzą do pamiętnika nastolatki, który shackowany przez ekspertkę Inferni (i upubliczniony przez problemy na linii Infernia - Prometeus) pokazują, że najpewniej nastolatka za wszystkim stoi sama. Uratowana przez tajemniczego maga Interis przed samobójstwem nastolatka przyznała na czym polegał problem i ujawnia, że pomaga jej Feliks Kidiron, najmłodszy z rodu i jej jedyny przyjaciel. Czyli to nie spisek. Ale jak to teraz naprawić?

Aktor w Opowieści:

* Dokonanie:
    * wiedząc, że pamiętnik Eweliny został upubliczniony i wiedząc, że Ardilli na Ewelinie zależy był w pobliżu Eweliny gdy ta próbowała się zabić. Nie miał siły, by przenieść Ewelinę z Ardillą do bezpiecznego miejsca (savaranin jest słaby fizycznie). Uważa, że Ewelina zachowała się "nieefektywnie".


### Bardzo nieudane porwanie Inferni

* **uid:** 230315-bardzo-nieudane-porwanie-inferni, _numer względny_: 5
* **daty:** 0093-03-06 - 0093-03-09
* **obecni:** Ardilla Korkoran, Eustachy Korkoran, Hubert Grzebawron, Mariusz Dobrowąs, Nadia Sekernik, OO Infernia, Rafał Kidiron, Ralf Tapszecz, SAN Szare Ostrze, Wojciech Grzebawron

Streszczenie:

Mimo braku wujka, Infernia działa sprawnie. Wujek jest pod opieką medyczną, ale Celina nie ufając Kidironowi, chce być z nim cały czas. Infernia otrzymuje wezwanie od CES Mineralis, gdyż stacja została zaatakowana przez Trianai. Eustachy wysyła Czarne Hełmy i komandosów z Inferni, którzy wspólnie działają, ale Trianai jest za dużo. Eustachy wabi więc Trianai za pomocą energii magicznej, tracąc przytomność po połączeniu z koordynatorem Trianai (magiem), zabijając go. Stacja CES ucierpiała w Paradoksie Eustachego, powodując wiele ofiar. Ardilla koordynuje operację ratunkową, ale gdy okazuje się, że część załogi z CES atakuje mostek próbując ukraść statek, Ardilla wyłącza Infernię i z Ralfem wieją do kanałów Inferni.

Eustachy budzi się w niewoli, przekonuje oprychów, że jedynie on potrafi sterować Infernią (ucierpiał przy tym nieco). Ardilla i Ralf - na prośbę Eustachego - sabotują generatory Memoriam, pozwalając Inferni uwolnić się spod kontroli wrogów. Wywiązuje się strzelanina, w wyniku której napastnicy zostają pokonani - Eustachy kontroluje sytuację. Eustachy decyduje, że ich karą będzie służba na Inferni, ale jeden z nich odchodzi ostrzegać przed atakiem na Infernię. To wszystkim pasuje.

Ekipa wraca na statek, gdzie Kidiron proponuje przesłuchanie pojmanych piratów, obiecując immunitet w zamian za współpracę. Eustachy zgadza się, ale tylko z ochotnikami i Ardillą jako gwarantem bezpieczeństwa. Piraci zgadzają się współpracować mimo użycia neuroobroży przy przesłuchaniu przez Kidirona. Gdy Zespół próbuje montować nowe generatory Memoriam - nie da się. Nie mogą być zamontowane, gdyż się palą. Infernia wykrywa próbę montażu nowych, ale nie zauważa starych.

Aktor w Opowieści:

* Dokonanie:
    * uczestniczył w akcji ratunkowej (ale przede wszystkim pilnował Ardillę), gdy mostek był zaatakowany to szybkim atakiem zranił dwóch napastników po oaczach. Potem współpraca z Ardillą w sabotowaniu generatorów Memoriam i dywersji.


### Terrorystka w Ambasadorce

* **uid:** 230215-terrorystka-w-ambasadorce, _numer względny_: 4
* **daty:** 0093-02-22 - 0093-02-23
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Kalia Awiter, Magda Misteria Sarbanik, Rafał Kidiron, Ralf Tapszecz, Tobiasz Lobrak, Tymon Korkoran

Streszczenie:

Kalia i Ardilla weszły do Ambasadorki by pomóc jeśli są w stanie, odcięły ich wiły i Ardilla ich poddaje. Misteria - napastniczka - okazuje się być czarodziejką roślin. Ona chce odzyskać wiły i zarzucić Kidironowi zniszczenie arkologii oraz porwanie wił. Jej celem jest ujawnienie prawdy o Kidironie, ukaranie Tobiasza (patrona Ambasadorki który robi złe rzeczy) i zabezpieczenie wił. Wujek chce zastąpić Ardillę, ale ona odmawia. Zespół (poza Ardillą) planuje zinfiltrować Ambasadorkę i usunąć maga (Misterię). 

Eustachy wpada na pomysł kontrolowanego obniżenia poziomu tlenu wewnątrz. Ardilla przekonuje Misterię do wymiany rannych na wujka. Ralf stanowi kanał komunikacyjny. Misteria tworzy Czarne Serce, kosztem wił i energii. Planują przekonać ludzi o winie Kidironów, wydostać się z Ambasadorki i stworzyć reportaż z Kalią. Ralf mówi o laserze na dachu, którym może zniszczyć czarodziejkę. Tobiasz wzbudza w sercu Misterii terror. Eustachy i Kidiron oczekują na rozwój sytuacji. W wyniku negocjacji, Kidiron zgadza się współpracować pod warunkiem, że Misteria nie będzie działać przeciwko niemu czy arkologii.

Kalia tworzy reportaż o Kidironie i arkologii Lirvint. Kidiron obiecuje bezpieczne wyjście dla Misterii i wił, pod warunkiem zapewnienia nieagresji. Ardilla próbuje przekonać Misterię do ewakuacji, ale ona nie chce opuścić miejsca bez rozwiązania sprawy z Tobiaszem. Czarne Serce wpływa na Misterię, więc wujek ją obezwładnia wstrzykując jej jakieś świństwo, sam zostaje poraniony przez wiły. Kidiron dotrzymał słowa odnośnie ewakuacji, acz postawił na Misterii tracker.

Aktor w Opowieści:

* Dokonanie:
    * w jakiś sposób dał radę nawiązać połączenie z Ardillą gdy ona była w Ambasadorce pełznąc korytarzami. Nawiązał link Eustachy - Ardilla i zadbał o bezpieczeństwo wszystkich.


### Pierwsza randka Eustachego

* **uid:** 230208-pierwsza-randka-eustachego, _numer względny_: 3
* **daty:** 0093-02-14 - 0093-02-21
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Franciszek Pietraszczyk, Kalia Awiter, Marcin Pietraszczyk, OO Infernia, Rafał Kidiron, Ralf Tapszecz

Streszczenie:

Wujek opieprza Eustachego za krzywdę niewinnych noktian, ale Kidiron się za Eustachym wstawia. Ardilla socjalizuje Ralfa i ratuje młodego Marcinka który się zaklinował w rurze Szczurowiska; okazuje się, że wszyscy kojarzą Infernię jako symbol nadziei. Potem Eustachy idzie (z przymusu) na randkę z Kalią; okazuje się, że Kalia zmanipulowała by Eustachy wygrał bo chce dać mu jeden piękny dzień. Wszystkie dowody podsłuchała i złapała Ardilla. Gdy Eustachy poczuł chore emanacje magiczne z Ambasadorki, porzucił Kalię i pobiegł do Inferni.

Aktor w Opowieści:

* Dokonanie:
    * powoli zbliża się do Ardilli. Wyraźnie ma problemy z konfliktem kultury savarańskiej (musisz być użyteczny) a tym co _chce_. Przyznał się Ardilli że widział oczy Nihilusa i to go zmieniło. Wyraźnie chce pomóc innym, ale nie rozumie dlaczego warto pomóc komuś kto nie ma już potencjału - ale CHCE pomóc. Bardzo skonfliktowany. WIE, że mag (Eustachy) nie może być sam. Ma problem z pozycjonowaniem Bartłomieja Korkorana jako bohatera lub głupca marnującego zasoby.


### Wyłączone generatory Memoriam Inferni

* **uid:** 230201-wylaczone-generatory-memoriam-inferni, _numer względny_: 2
* **daty:** 0093-02-10 - 0093-02-12
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Ernest Puszczowiec, Eustachy Korkoran, Jan Lertys, Kalia Awiter, Kalista Luminis, Kratos Coruscatis, OO Infernia, Rafał Kidiron, Ralf Tapszecz, Tymon Korkoran

Streszczenie:

Eustachy przebudzony przez Infernię po burzy piaskowej poszedł za jej podszeptami i wyłączył generatory Memoriam. Infernia go Dotknęła - jest to anomalia Interis uwięziona w mechanicznej skorupie. Tylko Eustachy może Infernią sterować. Więc gdy pojawiła się misja pomocy bazie noktiańskiej Coruscatis, Eustachy musiał lecieć. Kidiron chciał aresztowania Kallisty Luminis a Wujek chciał pomocy noktianom. Okazało się, że na miejscu sformował się amalgamoid trianai i Infernia została ostrzelana; acz odpowiedziała ogniem. Zespół pomógł i ewakuował noktian i zdestabilizował reaktor fuzyjny by wysadzić wszystko. Pod wpływem Ardilli i Celiny Eustachy nie oddał noktian Kidironowi a wysadził ich w bezpiecznym miejscu. Kolejny dowód, że arkologia Nativis wykorzystuje żywność jako broń, co jest niezgodne z wszelkimi konwencjami.

Aktor w Opowieści:

* Dokonanie:
    * nowy dodatek do Inferni, mag (domena: DOMINUJĄCA ROZPACZ) i kultysta Interis. Noktianin, savaranin. (ENCAO:  -00-+ |Bezbarwny, przezroczysty;;Buja w obłokach| VALS: Conformity, Humility >> Benevolence| DRIVE: Piękno zniszczenia). Świetny w bezszelestności i infiltracji, nigdy się nie skarży, polubił się z Ardillą. Wujek dodał go do Inferni by mu pomóc.


### Savaranie przed obliczem Nihilusa

* **uid:** 230720-savaranie-przed-obliczem-nihilusa, _numer względny_: 1
* **daty:** 0092-07-01 - 0092-07-02
* **obecni:** Agnieszka Serkis, Arnold Tapszecz, Igor Seklamant, Jola Seklamant, JRN Kantala Ravis, Katrina Kirten, Kiran Ravis, Lena Morazik, Marcelin Viirdus, Rafał Kurrodis, Ralena Annitas, Ralf Tapszecz

Streszczenie:

Kantala Ravis uratowała rozbitków z innej jednostki, którzy przynieśli na statek Anomalię Nihilusa. Lataravia przyleciała ratować ratujących. Pacyfikatory - apatyczne, ludzie pod wpływem Nihilusa wpadają w rozpacz ("niech się skończy!") lub w wyparcie ("wszystko w porządku!"). Lataravia odkryła, że rozbitkowie przynieśli anomalię i są najpewniej kotwicą, więc wyrzucili rozbitków w kosmos. Udało im się stracić tylko jednego członka załogi.

Aktor w Opowieści:

* Dokonanie:
    * na imię ma ARNOLD. Jedyny ocalały ze swojego statku gdzie była Anomalia Nihilusa, uratowany przez 'Kantalę Ravis'; stworzył magią manifestację swego ojca mocą Nihilusa (ojciec RALF jest jego bohaterem). 'Ojciec' - manifestacja Nihilusa - zatruwała Kantalę Ravis, ale ten jego aspekt w któego wierzył 'Ralf' próbowała go zabić (i przez to chronić jego i Kantalę Ravis). Polubił się z Kiranem z Kantali; Igor wpakował Ralfa i Arnolda do wahadłowca, który potem został wysadzony. KIA, ale Nihilus go przywrócił na Neikatis, z mniejszą dozą pamięci. Był ogniście lojalny rodzinie.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 10, @: 0093-03-29
    1. Primus    : 10, @: 0093-03-29
        1. Sektor Astoriański    : 10, @: 0093-03-29
            1. Neikatis, orbita    : 1, @: 0092-07-02
            1. Neikatis    : 9, @: 0093-03-29
                1. Dystrykt Glairen    : 9, @: 0093-03-29
                    1. Arkologia Lirvint    : 1, @: 0093-02-23
                    1. Arkologia Nativis    : 8, @: 0093-03-29
                        1. Poziom 1 - Dolny    : 5, @: 0093-03-28
                            1. Południe    : 1, @: 0093-03-28
                                1. Magazyny    : 1, @: 0093-03-28
                                1. Processing    : 1, @: 0093-03-28
                                1. Stacje Skorpionów    : 1, @: 0093-03-28
                            1. Północ - Stara Arkologia    : 5, @: 0093-03-28
                                1. Blokhaus E    : 1, @: 0093-03-28
                                1. Blokhaus F    : 4, @: 0093-03-28
                                    1. Szczurowisko    : 3, @: 0093-03-28
                                1. Stare Wejście Północne    : 2, @: 0093-03-28
                                1. Szczurowisko    : 1, @: 0093-02-21
                            1. Wschód    : 1, @: 0093-03-28
                                1. Farmy Wschodnie    : 1, @: 0093-03-28
                            1. Zachód    : 2, @: 0093-03-28
                                1. Centrala Prometeusa    : 2, @: 0093-03-28
                                1. Stare TechBunkry    : 1, @: 0093-03-28
                        1. Poziom 2 - Niższy Środkowy    : 2, @: 0093-03-29
                            1. Południe    : 2, @: 0093-03-29
                                1. Engineering    : 2, @: 0093-03-29
                                1. Power Core    : 1, @: 0093-03-28
                            1. Północ    : 1, @: 0093-03-28
                                1. Bazar i sklepy    : 1, @: 0093-03-28
                            1. Wschód    : 1, @: 0093-03-28
                                1. Centrum Kultury i Rozrywki    : 1, @: 0093-03-28
                                    1. Bar Śrubka z Masła    : 1, @: 0093-03-28
                                    1. Muzeum Kidirona    : 1, @: 0093-03-28
                                    1. Ogrody Gurdacza    : 1, @: 0093-03-28
                                    1. Stacja holosymulacji    : 1, @: 0093-03-28
                            1. Zachód    : 1, @: 0093-03-28
                                1. Koszary Hełmów    : 1, @: 0093-03-28
                                1. Sektor Porządkowy    : 1, @: 0093-03-28
                        1. Poziom 3 - Górny Środkowy    : 6, @: 0093-03-29
                            1. Południe    : 2, @: 0093-03-29
                                1. Life Support    : 1, @: 0093-03-28
                                1. Medical    : 2, @: 0093-03-29
                            1. Wschód    : 5, @: 0093-03-28
                                1. Dzielnica Luksusu    : 5, @: 0093-03-28
                                    1. Ambasadorka Ukojenia    : 3, @: 0093-03-28
                                    1. Ogrody Wiecznej Zieleni    : 4, @: 0093-03-28
                                    1. Stacja holosymulacji    : 2, @: 0093-03-28
                        1. Poziom 4 - Górny    : 2, @: 0093-03-29
                            1. Wschód    : 2, @: 0093-03-29
                                1. Centrala dowodzenia    : 1, @: 0093-03-28
                                1. Obserwatorium Astronomiczne    : 1, @: 0093-03-28
                                1. Radiowęzeł    : 2, @: 0093-03-29
                            1. Zachód    : 1, @: 0093-03-28
                                1. Tereny Sportowe    : 1, @: 0093-03-28
                        1. Poziom 5 - Szczyt    : 1, @: 0093-03-28
                            1. Północ    : 1, @: 0093-03-28
                                1. Port kosmiczny    : 1, @: 0093-03-28
                    1. Arkologia Sarviel, okolice    : 1, @: 0093-02-12
                        1. Krater Coruscatis    : 1, @: 0093-02-12
                    1. CES Mineralis    : 1, @: 0093-03-09

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 9 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Eustachy Korkoran    | 9 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Bartłomiej Korkoran  | 7 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Kalia Awiter         | 7 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| OO Infernia          | 7 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Rafał Kidiron        | 7 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Celina Lertys        | 3 | ((230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Tobiasz Lobrak       | 3 | ((230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Tymon Korkoran       | 3 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| BIA Prometeus        | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Dalmjer Servart      | 2 | ((230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Franciszek Pietraszczyk | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| Izabella Saviripatel | 2 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Laurencjusz Kidiron  | 2 | ((230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Małgorzata Maratelus | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Marcel Draglin       | 2 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| SAN Szare Ostrze     | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230614-atak-na-kidirona)) |
| Wojciech Grzebawron  | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Agnieszka Serkis     | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Arnold Tapszecz      | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Hubert Grzebawron    | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Igor Seklamant       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Jan Lertys           | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Jola Seklamant       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| JRN Kantala Ravis    | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Karol Lertys         | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Katrina Kirten       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Kiran Ravis          | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Lena Morazik         | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Lycoris Kidiron      | 1 | ((230614-atak-na-kidirona)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Marcelin Viirdus     | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Marcin Pietraszczyk  | 1 | ((230208-pierwsza-randka-eustachego)) |
| Mariusz Dobrowąs     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Nadia Sekernik       | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Rafał Kurrodis       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Ralena Annitas       | 1 | ((230720-savaranie-przed-obliczem-nihilusa)) |
| Stanisław Uczantor   | 1 | ((230614-atak-na-kidirona)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |