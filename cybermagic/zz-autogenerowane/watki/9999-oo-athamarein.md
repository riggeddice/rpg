# OO Athamarein
## Identyfikator

Id: 9999-oo-athamarein

## Sekcja Opowieści

### Anomalne awarie Athamarein?

* **uid:** 231018-anomalne-awarie-athamarein, _numer względny_: 7
* **daty:** 0111-12-22 - 0111-12-27
* **obecni:** Alicja Szadawir, Andrzej Gwozdnik, Arianna Verlen, Błażej Sowiński, Elena Verlen, Eustachy Korkoran, Hiacynt Samszar, Jakub Oroginiec, Kajetan Kircznik, Klaudia Stryk, Leona Astrienko, OO Athamarein, Remigiusz Alkarenit, Uśmiechniczka Konstrukcjonistka Aurora Diakon

Streszczenie:

Athamarein - bardzo stara korweta rakietowa, jeszcze sprzed czasu Astorii - miała spotkanie z rajderami Syndykatu. Sama obróciła się, by Syndykat nie zabił jej załogi (Persefona nic nie wie). Jednak załoga Athamarein jest skłócona, straumatyzowana i niekompetentna - nie poradziłaby sobie na akcji, więc Athamarein zaczęła się sama 'sabotować'. W rozpaczy, ściągnęli załogę Inferni zanim Athamarein pójdzie na żyletki - niech Infernianie odkryją i wyjaśnią, co się stało z Athamarein. W wyniku, Athamarein została oddelegowana jako jednostka szkoleniowa. Czas na emeryturę.

Aktor w Opowieści:

* Dokonanie:
    * bardzo stara już korweta, która Odbiła się w Eterze. Traci nadspodziewanie mało załogi. SAMA zrobiła manewr unikający działa Syndykatu. W chwili, w której załoga Athamarein nie jest zdolna do pełnienia misji, Athamarein zaczęła sama się sabotować, uniemożliwiając im iść na akcję gdzie mogliby zginąć. Pozytywna jednostka o podstawowych cechach Anomalii Kosmicznej, ale nastawionej pro-ludzko.
* Progresja:
    * przeznaczona od teraz do misji szkoleniowych. Jej ochrona załogi, jej odporność i niskokosztowość są idealne w tej jakże przecież ważnej roli. Czas na emeryturę.


### Gdy HR reedukuje niewłaściwą osobę

* **uid:** 230111-gdy-hr-reedukuje-niewlasciwa-osobe, _numer względny_: 6
* **daty:** 0100-11-13 - 0100-11-16
* **obecni:** Adragain Ferrias, AK Nox Ignis, Aleksy Sartaran, Arianna Verlen, Daria Czarnewik, Elena Verlen, Grażyna Burgacz, Kajetan Kircznik, Lana Mirkinin, Leszek Kurzmin, Maja Samszar, Miłosz Klinek, OO Astralna Flara, OO Athamarein, OO Loricatus, Persefona d'Loricatus, Sabrina Ferrias, Salazar Bolza, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Niestety, Kircznik (lekarz) przestraszył Sargona i młodych infiltratorów Lodowcem, Bolzą i ogólnie duchami noktiańskimi. Okazało się, że Grażyna jest ekspertem od duchów i jest w stanie zrobić egzorcyzm. Niedługo potem (i chwilę przed egzorcyzmem) pojawiła się szybka korweta z Biur HR i poprosili o pomoc w odparciu Anomalii Kosmicznej. Myrczek sprawdził jej pamięć i Athamarein i Flara poleciały na starcie z Nox Ignis. Nawiązały kontakt z Biurami HR, przechwyciły biednego męża pilotki Nox Ignis i poznały prawdę o Destructor Animarum i wpływie Syndykatu Aureliona na Biura HR. Kurzmin pobił rekordy Athamarein, Arianna skutecznie zaplanowała wypalenie pilotki Nox Ignis przez własny statek. Tylko advancer Miłosz (badający ślady Nox Ignis) jest MIA.

Aktor w Opowieści:

* Dokonanie:
    * przekroczyła moc silników (150%), ale złapała Adragaina (brata pilotki Nox Ignis) i wróciła manewrować przeciw Nox Ignis. Uległa lekkim uszkodzeniom silników (max. 70% pełnej mocy)
* Progresja:
    * uszkodzona; pełna moc silników to 70%. Przesterowana.


### Astralna Flara i nowy komodor

* **uid:** 221221-astralna-flara-i-nowy-komodor, _numer względny_: 5
* **daty:** 0100-11-07 - 0100-11-10
* **obecni:** Arianna Verlen, Daria Czarnewik, Elena Verlen, Ellarina Samarintael, Kajetan Kircznik, Kaspian Certisarius, Leszek Kurzmin, Maja Samszar, NekroTAI Zarralea, OO Astralna Flara, OO Athamarein, OO Loricatus, Salazar Bolza, Sargon Niiris, Władawiec Diakon

Streszczenie:

Po "uratowaniu" noktian z Dominy Lucis Arianna dostała medal a Lodowca zastąpił komodor Bolza, zimny drakolita. Gdy grupa wydzielona wróciła na teren Anomalii Kolapsu okazało się, że planetoida Kazmirian ma squatterów; Zespół skutecznie ich przejął odzyskując kontrolę nad systemami Semli d'Kazmirian. Na miejscu - TKO-4271 - okazało się że Zarralea jest zniszczona. Orbiter odzyskał swoją bazę, acz Loricatis musi współdzielić Persefonę. Z innej beczki: link Elena - Władawiec się pięknie rozwija.

Aktor w Opowieści:

* Dokonanie:
    * retransmiter dla Astralnej Flary by Maja mogła odzyskać kontrolę nad Kazmirian i unieszkodliwić squatterów.


### Astralna Flara kontra Domina Lucis

* **uid:** 221214-astralna-flara-kontra-domina-lucis, _numer względny_: 4
* **daty:** 0100-10-09 - 0100-10-11
* **obecni:** Arianna Verlen, Axel Nargan, Daria Czarnewik, Ellarina Samarintael, Gabriel Lodowiec, Kirea Rialirat, Leszek Kurzmin, NekroTAI Zarralea, OO Astralna Flara, OO Athamarein, Sarian Xadaar, Tristan Rialirat

Streszczenie:

Lodowiec dostał rozkazy pojmać / zabić dowódcę Dominy Lucis. Użył nekroTAI by odebrać resztkę nadziei noktianom (you will serve alive or dead) i jakkolwiek sporo noktian się poddało, główni popełnili samobójstwo. Orbiter zajął Dominę Lucis i Strefę Duchów, acz kosztem reputacji. Ogromny, bezkrwawy sukces wzmacniający Pax Orbiter dzięki propagandzie i Zarralei.

Aktor w Opowieści:

* Dokonanie:
    * ostrożnie skanuje okolicę Strefy Duchów i wykrywa miny i inne noktiańskie niespodzianki; przygotuje odpowiednie kąty rażenia, by nikt nie dał rady się wycofać z Dominy Lucis.


### Astralna Flara w strefie duchów

* **uid:** 221130-astralna-flara-w-strefie-duchow, _numer względny_: 3
* **daty:** 0100-10-05 - 0100-10-08
* **obecni:** Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Elena Verlen, Ellarina Samarintael, Gabriel Lodowiec, Hubert Kerwelenios, Kajetan Kircznik, Kirea Rialirat, Klarysa Jirnik, Leo Kasztop, Maja Samszar, NekroTAI Zarralea, OO Astralna Flara, OO Athamarein, Szymon Wanad, Tomasz Dojnicz

Streszczenie:

Planetoida Kazmirian została przejęta przez Orbiter. Daria ostrzegła Nonarion (Leo), że Orbiter nadal poluje na TAI i zdobyła informację o 'strefie duchów'. Z Mają doszły do tego, że 'strefa duchów' jest świetnym maskowaniem; Lodowiec wyprowadził tam Orbiter a Arianna stworzyła nekroTAI z Zarralei d'Isigtand. Używając advancera przechwycili samotną savarankę (której Lodowiec nie umie przesłuchać) i stanęło na tym, że 'strefa duchów' jest bazą noktiańską zbudowaną dookoła jednostki wsparcia, 'Domina Lucis'. Lodowiec chce doprowadzić noktian do sprawiedliwości Orbitera, Arianna i Daria są skonfliktowane. Mają jeńca - Kireę, lokalną młodą savarankę.

Aktor w Opowieści:

* Dokonanie:
    * udało się użyć Persi i Zarralei do zrozumienia co się dzieje w Strefie Duchów.


### Egzotyczna Piękność na Astralnej Flarze

* **uid:** 221123-egzotyczna-pieknosc-na-astralnej-flarze, _numer względny_: 2
* **daty:** 0100-09-12 - 0100-09-15
* **obecni:** Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Ellarina Samarintael, Erwin Pies, Frank Mgrot, Gabriel Lodowiec, Gerwazy Kircznik, Hind Szug Traf, Kajetan Kircznik, Leszek Kurzmin, Marcel Kulgard, OO Astralna Flara, OO Athamarein, SCA Hadiah Emas, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Flara naprawia Hadiah Emas, a po drodze komodor Lodowiec rozplątuje wszystkie wątki - niewolnik z Orbitera? (okazał się dezerterem), autonomiczna TAI Mirtaela d'Hadiah Emas? (zostaje Ograniczona przez psychotronika Athamarein) itp. Arianna zarządza Flarą, wprowadzając Ellarinę jako maskotkę od morale i stabilizując wszystko by jakoś zespół dopasował się do realiów Nonariona. To nie jest ani Aurum ani Orbiter, to coś zupełnie innego.

Aktor w Opowieści:

* Dokonanie:
    * ma na pokładzie nadspodziewanie dobrego psychotronika. Na pokład trafił do aresztu Frank Mgrot ("marine z Orbitera / dezerter") zanim Hadiah Emas odleciała dalej.


### Astralna Flara dociera do Nonariona Nadziei

* **uid:** 221116-astralna-flara-dociera-do-nonariona-nadziei, _numer względny_: 1
* **daty:** 0100-09-08 - 0100-09-11
* **obecni:** Alan Nierkamin, Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Gabriel Lodowiec, Grażyna Burgacz, Kajetan Kircznik, Leo Kasztop, Leszek Kurzmin, Maja Samszar, Marcel Kulgard, OO Astralna Flara, OO Athamarein, SCA Hadiah Emas, SCA Isigtand, Szymon Wanad, Tomasz Ruppok, Władawiec Diakon

Streszczenie:

Flara dociera do Nonariona, który wygląda jak absolutny śmieć. Morale na Flarze ucierpiało - aż do plotek o Egzotycznych Pięknościach, co sprawiło Ariannie kolejne problemy. Alan, eks-lokals powiedział Darii że jest jakaś forma standaryzacji na Nonarionie - coś tam jest, coś "bogatego" jako frakcja. Daria zdobyła od Leo planetoidę którą może użyć jako bazę dla Orbitera, ale w drodze tam zboczyli by ratować uszkodzone statki lokalne. I uratowali jednostkę niewolniczą i zniszczony grazer. Komodor Lodowiec - sensowny koleś - ma dylemat moralny. Neikatiańska TAI oraz niewolnicy.

Aktor w Opowieści:

* Dokonanie:
    * ciężka korweta rakietowa Orbitera p.d. Leszka Kurzmina i statek flagowy Gabriela Lodowca. Walczy dużo ponad swą wagę, acz wymaga jednostki wsparcia (zasięg, amunicja). Skutecznie unika ataku asteroid anomalnych, ale wpada w pułapkę i Flara go uratowała.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 7, @: 0111-12-27
    1. Primus    : 7, @: 0111-12-27
        1. Sektor Astoriański    : 7, @: 0111-12-27
            1. Astoria, Orbita    : 1, @: 0111-12-27
                1. Kontroler Pierwszy    : 1, @: 0111-12-27
            1. Libracja Lirańska    : 6, @: 0100-11-16
                1. Anomalia Kolapsu, orbita    : 6, @: 0100-11-16
                    1. Planetoida Kazmirian    : 1, @: 0100-09-11
                    1. SC Nonarion Nadziei    : 1, @: 0100-09-11
                        1. Moduł ExpanLuminis    : 1, @: 0100-09-11
                    1. Strefa Biur HR    : 1, @: 0100-11-16
                    1. Strefa Upiorów Orbitera    : 4, @: 0100-11-16
                        1. Planetoida Kazmirian    : 2, @: 0100-11-10
                        1. Planetoida Lodowca    : 4, @: 0100-11-16

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 7 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 231018-anomalne-awarie-athamarein)) |
| Daria Czarnewik      | 6 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kajetan Kircznik     | 6 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 231018-anomalne-awarie-athamarein)) |
| OO Astralna Flara    | 6 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leszek Kurzmin       | 5 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Elena Verlen         | 4 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 231018-anomalne-awarie-athamarein)) |
| Ellarina Samarintael | 4 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Gabriel Lodowiec     | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Maja Samszar         | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Władawiec Diakon     | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Arnulf Perikas       | 3 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow)) |
| NekroTAI Zarralea    | 3 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Grażyna Burgacz      | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kirea Rialirat       | 2 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Leo Kasztop          | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Marcel Kulgard       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| OO Loricatus         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Salazar Bolza        | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| SCA Hadiah Emas      | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Szczepan Myrczek     | 2 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Szymon Wanad         | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alezja Dumorin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Alicja Szadawir      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Andrzej Gwozdnik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Axel Nargan          | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Błażej Sowiński      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Erwin Pies           | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Eustachy Korkoran    | 1 | ((231018-anomalne-awarie-athamarein)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gerwazy Kircznik     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hiacynt Samszar      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hubert Kerwelenios   | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Jakub Oroginiec      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Kaspian Certisarius  | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Klarysa Jirnik       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Klaudia Stryk        | 1 | ((231018-anomalne-awarie-athamarein)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leona Astrienko      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Remigiusz Alkarenit  | 1 | ((231018-anomalne-awarie-athamarein)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Sarian Xadaar        | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Tomasz Dojnicz       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Tomasz Ruppok        | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Tristan Rialirat     | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Uśmiechniczka Konstrukcjonistka Aurora Diakon | 1 | ((231018-anomalne-awarie-athamarein)) |