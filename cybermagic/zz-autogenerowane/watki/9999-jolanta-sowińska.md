# Jolanta Sowińska
## Identyfikator

Id: 9999-jolanta-sowińska

## Sekcja Opowieści

### Siostrzenica Morlana

* **uid:** 210818-siostrzenica-morlana, _numer względny_: 9
* **daty:** 0112-01-20 - 0112-01-24
* **obecni:** Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Maria Naavas, Nataniel Morlan, Ofelia Morlan, Olgierd Drongon, OO Netrahina, OO Żelazko, SC Fecundatis, SC Światłodóbr, Tomasz Sowiński

Streszczenie:

Tomasz Sowiński próbuje uratować Ofelię Morlan przed Natanielem Morlanem. Nie ma kogo poprosić a Ofelia służy na Netrahinie; poprosił więc Ariannę. Arianna skanując echo pamięci Jolanty w Inferni zdobywa informacje o programie kosmicznym Orbitera, przekazuje to Kramerowi, wykorzystuje wsparcie Olgierda i robią ćwiczenia Żelazko - Netrahina. Ratują Ofelię przed porwaniem przez koloidowy statek Kruków Kasandry.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * jej plany / wiedza odnośnie programu kosmicznego Aurum zostały przekazane Kramerowi a jej reputacja w Aurum poważnie uszkodzona, że "dała cynk".
    * wisi Ariannie Verlen spory dług wdzięczności za ratunek Ofelii. Tomasz zawarł dług, ale Jolanta będzie honorować.


### Znudzona załoga Inferni

* **uid:** 210421-znudzona-zaloga-inferni, _numer względny_: 8
* **daty:** 0111-11-16 - 0111-11-19
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Morrigan d'Tirakal, Otto Azgorn, Persefona d'Infernia, Tomasz Sowiński

Streszczenie:

Po raz pierwszy od dawna Infernia jest w stanie rozprzężenia - wracają z daleka po trudnej misji i nic im nie grozi. Jako, że załoga ma straszne tarcia (Leona poluje na "słabe jednostki", komandosi Verlenów chcą zatrzymać Leonę, noktianie vs astorianie...), Arianna autoryzuje Eustachego do zrobienia "Tirakala" jako niebezpieczną sytuację. Eustachy reanimuje Tirakala, ale niestety cholerstwo zanomalizowało - przywrócił Morrigan. By Zespół nie rozwiązał Tirakala za szybko, Eustachy trochę sabotował Infernię co Elena przeczytała jako... umówienie się na randkę. Gdy Klaudia wykryła, że na INFERNI pojawiła się straszna anomalia (Morrigan) próbowali to usunąć - ale Eustachy przecież sabotował Infernię! Udało im się opanować Morrigan, ale kosztem uszkodzeń i obrażeń na Inferni. I scenek kompromitujących Jolantę Sowińską. Infernia jest uszkodzona, ale się trzyma.

Aktor w Opowieści:

* Dokonanie:
    * pod wpływem środków i niestabilna unieszkodliwiła Martyna magią, po czym zapolowała na nią Leona i ją szybko zdjęła.


### Arianna podbija Asimear

* **uid:** 210317-arianna-podbija-asimear, _numer względny_: 7
* **daty:** 0111-10-18 - 0111-11-02
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Martyna Bianistek, SCA Płetwal Błękitny, Tomasz Sowiński

Streszczenie:

"Płetwal Błękitny" ma na pieńku z "Goldarionem"; by nie złamać konspiracji, Klaudia potraktowała Płetwala Bazyliszkiem. Tymczasem pojawiła się wiadomość, że Arianna chce zniszczyć Asimear i żąda wydania Sowińskiej. Prawdziwa Arianna złamała konspirę, skontaktowała się z Jolantą Sowińską, po czym ją porwała. Okazało się, że Jolanta jest kontrolowana przez kralotycznego pasożyta...

Aktor w Opowieści:

* Dokonanie:
    * spotkała się kiedyś z kralothem na Asimear, została przezeń zdominowana. Porwana przez Ariannę, naprawiona przez Martyna - ale kralotycznie uzależniona.
* Progresja:
    * usunięto jej pasożyta kralotycznego i odbudowano połączenie z TAI, ale jest skrajnie uzależniona kralotycznie.


### Infernia jako Goldarion

* **uid:** 210218-infernia-jako-goldarion, _numer względny_: 6
* **daty:** 0111-09-16 - 0111-10-01
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, OO Infernia, Tomasz Sowiński

Streszczenie:

Infernia udaje Goldariona, by przetransportować tajną przesyłkę Sowińskich do Jolanty Sowińskiej na planetoidzie Asimear. Tomasz Sowiński na Inferni okazuje się być dużym dzieckiem, łatwo jest wmanipulowany w otwarcie paczki - jest tam nietypowy antynanitkowy Entropik (którego psuje Elena przez błędną integrację). W tle wszystkiego - próba zdobycia niezależnej od Orbitera floty przez Aurum i jeden znikający advancer Kramera na Asimear...

Aktor w Opowieści:

* Dokonanie:
    * nie pojawiła się jeszcze; kociła w przeszłości Tomasza Sowińskiego. Do niej przesłano zaawansowany antynanitkowy Entropik. Sama używa Tirakal/Morrigan.


### Torszecki pokazał kręgosłup

* **uid:** 211012-torszecki-pokazal-kregoslup, _numer względny_: 5
* **daty:** 0111-07-29 - 0111-07-30
* **obecni:** Amelia Sowińska, Jolanta Sowińska, Karolina Terienak, Lucjan Sowiński, Marysia Sowińska, Rafał Torszecki, Sensacjusz Diakon

Streszczenie:

Marysia dostała nową "prośbę" ze strony Dworu Sowińskich - pozyskać próbkę krwi "córki Morlana" z Pustogoru. Tymczasem Karolina poszła do Torszeckiego (do lecznicy) by ten wreszcie miał kręgosłup. Wymusiła na nim pomoc w znalezieniu mordercy ścigacza. Torszecki nie chcąc krzywdy Karoliny się jej przyznał - to on. Chciał zrobić Ernest vs Mafia by chronić Marysię; skrzyżował w głowie, że Ernest x Amelia to Esuriit, więc to samo spotka biedną Marysię... Karo i Marysia mają teraz problem - jak to rozplątać?

Aktor w Opowieści:

* Dokonanie:
    * o dziwo, zaakceptowała plan Amelii Sowińskiej by pokazać Morlanowi krew "córki Morlana". Ten plan po prostu ma sens.


### Przybycie Rekina z Eterni

* **uid:** 210921-przybycie-rekina-z-eterni, _numer względny_: 4
* **daty:** 0111-07-20 - 0111-07-25
* **obecni:** Amelia Sowińska, Arkadia Verlen, Ernest Namertel, Jolanta Sowińska, Karol Pustak, Karolina Terienak, Lucjan Sowiński, Marysia Sowińska, Nataniel Morlan, Rafał Torszecki, Tomasz Tukan

Streszczenie:

Do Rekinów dołącza tien z Eterni. Marysia dowiaduje się, że to kwestia intryg jej kuzynki, Amelii, próbującej deeskalować problemy na linii Morlan - Jolanta. Dowiedziała się od Amelii, że to nie Amelia stała za morderstwami w Podwiercie, kogoś chroni. Oraz... że Amelia jest zakochana (Amelia nie wie że Marysia wie). Ale na Marysię spadła ochrona Ernesta. Więc... usunęła wszystkie zagrożenia o których pomyślała (Arkadia) i zaczęła działania mające poprawić reputację Eternianina, by Rekiny go przyjęły.

Aktor w Opowieści:

* Dokonanie:
    * jej ruchy mające uwolnić osoby spod kontroli Nataniela Morlana są zauważone przez Morlana i stanowią coraz więcej problemów dla rodu Sowińskich. Skonfliktowana politycznie z Amelią Sowińską.


### Fecundatis w Domenie Barana

* **uid:** 210820-fecundatis-w-domenie-barana, _numer względny_: 3
* **daty:** 0109-03-06 - 0109-03-09
* **obecni:** Antonella Temaris, Antos Kuramin, Bruno Baran, Cień Brighton, Damian Szczugor, Deneb Ira, Elsa Kułak, Flawia Blakenbauer, Jolanta Sowińska, Kara Szamun, Leon Kantor, Rick Varias, Tamara Mardius

Streszczenie:

Fecundatis dotarł do Domeny Ukojenia w Pasie Kazimierza. Na miejscu - Blakvelowcy zmiażdżyli Mardiusowców; Strachy mają kolejną ofensywę i wszyscy się boją. Po przeanalizowaniu sytuacji Fecundatis zdecydował się na znalezienie i przejęcie miragenta, ale przeszkodziła im inwazja DUŻEJ ilości groźnych Strachów. Zespół skutecznie odparł Strachy (przy pewnych stratach) i wyniósł Antosa do roli lokalnego bohatera i jednoczyciela. Mieli jednak wsparcie Kuratorów... którzy m.in. Tamarę Mardius wpakowali już do Aleksandrii...

Aktor w Opowieści:

* Dokonanie:
    * autorka pomysłu użycia Antonelli i jej wiedzy o Esuriit jako baterii do przekształcenia skorpioidów; drugim torem zasilała Fecundatis swoją Elainką. Doszło do rozszczepienia ona - TAI.
* Progresja:
    * rozszczepienie Jolanta - Elainka; Jolanta jest niezborna i ma problemy z dostępem do swojej magii / niektórych funkcji.


### Szmuglowanie Antonelli

* **uid:** 210813-szmuglowanie-antonelli, _numer względny_: 2
* **daty:** 0109-02-11 - 0109-02-23
* **obecni:** Antonella Temaris, Bruno Baran, Cień Brighton, Flawia Blakenbauer, Jolanta Sowińska, Lucjusz Blakenbauer, SC Fecundatis, Tomasz Sowiński

Streszczenie:

Antonella Temaris stała się problemem politycznym na linii Sowińscy - Nataniel Morlan. By nie została oddana, Tomasz, Jolanta i Flawia weszli we współpracę z Cieniem Brightonem, przemytnikiem. Przemycili Flawię na orbitę (Brighton skłonił Jolantę, by ta poleciała z nimi!), po czym zgubili ewentualny pościg na Valentinie. A drugą linią Flawia przekonała Lucjusza, by ten przygotował szpital terminuski w Pustogorze na zmianę Wzoru Antonelli, by ją naprawić...

Aktor w Opowieści:

* Dokonanie:
    * świeżo hipersprzężona z TAI Elainka; wie o paśmie Kazimierza (bo słała kiedyś listy miłosne do Bruno Barana). Poleciała z Brightonem i Flawią do Pasa Kazimierza by ratować Antonellę; nabawiła się strachu przed skorpioidami XD.
* Progresja:
    * boi się skorpioidów... po podróży Fecundatis z Antonellą nic dziwnego że ma pomniejszą fobię.
    * Brighton wyjaśnił jej, że ucieczka przed strachem to ucieczka. Nie warto oddać TAI kontroli nad sobą. Uwrażliwiona. Nie odda kontroli. PLUS - zainteresowana programem kosmicznym by inne tak nie cierpiały jak Flawia.


### Porwanie na Gwiezdnym Motylu

* **uid:** 210810-porwanie-na-gwiezdnym-motylu, _numer względny_: 1
* **daty:** 0108-12-25 - 0108-12-30
* **obecni:** Antonella Temaris, Flawia Blakenbauer, Franek Kuparał, Jolanta Sowińska, Lena Fenatil, Nataniel Morlan, Renata Szarżun, SLX Gwiezdny Motyl, Tomasz Sowiński

Streszczenie:

Flawia Blakenbauer miała nadzieję, że Tomasz Sowiński pomoże jej z kłopotami, ale ów zniknął na Gwiezdnym Motylu. Flawia, Szmuglerka i Strażniczka skutecznie znalazły konspirację noktian którzy próbowali przehandlować Tomasza za innych noktian, oraz miragenta próbującego Tomasza usunąć. Do tego zaplątały się dookoła Ducha - eternijskiej szlachcianki uciekającej przed potężnym łowcą magów uważającym że jest jej ojcem. All in all, Tomasz odzyskany, łowca magów uśpiony a Duch uciekł.

Aktor w Opowieści:

* Dokonanie:
    * twinnowana czarodziejka. Zupełnie nie wie jak rozmawiać z innymi. Powiedziała Flawii wszystko co mogła o Tomaszu i dała namiar na jego porywaczy i aktualną lokalizację.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 8, @: 0111-11-19
    1. Primus    : 8, @: 0111-11-19
        1. Sektor Astoriański    : 8, @: 0111-11-19
            1. Astoria, Orbita    : 2, @: 0111-11-02
                1. Kontroler Pierwszy    : 2, @: 0111-11-02
            1. Astoria    : 3, @: 0111-07-30
                1. Sojusz Letejski    : 3, @: 0111-07-30
                    1. Aurum    : 1, @: 0109-02-23
                        1. Imperium Sowińskich    : 1, @: 0109-02-23
                            1. Krystalitium    : 1, @: 0109-02-23
                                1. Klub Eksplozja    : 1, @: 0109-02-23
                                1. Pałac Świateł    : 1, @: 0109-02-23
                    1. Szczeliniec    : 3, @: 0111-07-30
                        1. Powiat Pustogorski    : 3, @: 0111-07-30
                            1. Podwiert    : 2, @: 0111-07-30
                                1. Dzielnica Luksusu Rekinów    : 2, @: 0111-07-30
                                    1. Serce Luksusu    : 2, @: 0111-07-30
                                        1. Apartamentowce Elity    : 2, @: 0111-07-30
                                        1. Lecznica Rannej Rybki    : 1, @: 0111-07-30
                            1. Pustogor    : 1, @: 0109-02-23
                                1. Rdzeń    : 1, @: 0109-02-23
                                    1. Szpital Terminuski    : 1, @: 0109-02-23
            1. Pas Teliriański    : 3, @: 0111-11-02
                1. Planetoidy Kazimierza    : 3, @: 0111-11-02
                    1. Domena Ukojenia    : 1, @: 0109-03-09
                        1. Planetoida Mirnas    : 1, @: 0109-03-09
                        1. Planetoida Talio    : 1, @: 0109-03-09
                        1. Stacja Ukojenie Barana    : 1, @: 0109-03-09
                        1. Szamunczak    : 1, @: 0109-03-09
                            1. Klub Korona    : 1, @: 0109-03-09
                    1. Planetoida Asimear    : 2, @: 0111-11-02
                        1. Stacja Lazarin    : 1, @: 0111-11-02
            1. Stacja Valentina    : 1, @: 0109-02-23

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Tomasz Sowiński      | 5 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli)) |
| Antonella Temaris    | 3 | ((210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Arianna Verlen       | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Elena Verlen         | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Eustachy Korkoran    | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Flawia Blakenbauer   | 3 | ((210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Klaudia Stryk        | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Leona Astrienko      | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Martyn Hiwasser      | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Amelia Sowińska      | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Bruno Baran          | 2 | ((210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Cień Brighton        | 2 | ((210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Karolina Terienak    | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Lucjan Sowiński      | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Marysia Sowińska     | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Nataniel Morlan      | 2 | ((210810-porwanie-na-gwiezdnym-motylu; 210921-przybycie-rekina-z-eterni)) |
| Rafał Torszecki      | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Antoni Kramer        | 1 | ((210218-infernia-jako-goldarion)) |
| Antos Kuramin        | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Arkadia Verlen       | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Damian Szczugor      | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Deneb Ira            | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Elsa Kułak           | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Ernest Namertel      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Franek Kuparał       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Kara Szamun          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Karol Pustak         | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Lena Fenatil         | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Leon Kantor          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Lucjusz Blakenbauer  | 1 | ((210813-szmuglowanie-antonelli)) |
| Martyna Bianistek    | 1 | ((210317-arianna-podbija-asimear)) |
| Morrigan d'Tirakal   | 1 | ((210421-znudzona-zaloga-inferni)) |
| OO Infernia          | 1 | ((210218-infernia-jako-goldarion)) |
| Otto Azgorn          | 1 | ((210421-znudzona-zaloga-inferni)) |
| Persefona d'Infernia | 1 | ((210421-znudzona-zaloga-inferni)) |
| Renata Szarżun       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Rick Varias          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| SC Fecundatis        | 1 | ((210813-szmuglowanie-antonelli)) |
| SCA Płetwal Błękitny | 1 | ((210317-arianna-podbija-asimear)) |
| Sensacjusz Diakon    | 1 | ((211012-torszecki-pokazal-kregoslup)) |
| SLX Gwiezdny Motyl   | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Tamara Mardius       | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Tomasz Tukan         | 1 | ((210921-przybycie-rekina-z-eterni)) |