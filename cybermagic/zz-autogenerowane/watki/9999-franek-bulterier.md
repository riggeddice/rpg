# Franek Bulterier
## Identyfikator

Id: 9999-franek-bulterier

## Sekcja Opowieści

### Ten nawiedzany i ta ukryta

* **uid:** 230325-ten-nawiedzany-i-ta-ukryta, _numer względny_: 5
* **daty:** 0111-10-16 - 0111-10-17
* **obecni:** Aleksandra Burgacz, Daniel Terienak, Franek Bulterier, Karolina Terienak, Lea Samszar, Michał Klabacz, Nadia Uprewien, Oliwier Czepek, Rupert Mysiokornik

Streszczenie:

Ola Burgacz, poślednia tienka zacnego rodu zorganizowała wyścig mający drażnić lokalsów Podwiertu, który skończył się: zwycięstwem Daniela, rozbiciem Mysiokornika (ktoś zatruł jego paliwo) i całusem Oli do Bulteriera. Tymczasem do Karo przyszedł centuś Oliwier prosić o pomoc dla pracownika, Michała, który jest "przeklęty". Karo i Michał weszli mu na chatę, znaleźli ślady magii ale co ważne nie na samym Michale. By pozbyć się wścibskiej sąsiadki, podłożyli jej szczura.

Okazuje się, że Ola dokuczała Michałowi i ów zrobił artefakt który podobno szedł z planów Triany (nonsens) i który nie miał prawa działać - a działa. Ola chciała mu potem pomóc, ale nie umiała. Za wszystkim stoi Lea Samszar, która lubiła Michała ale dla utrzymania swojej reputacji przed bardziej podłymi Rekinami poświęciła Michała w rytuale. Gdy nie wiedzący o tym Daniel i Karo dotarli do Lei i przywieźli jej nieprzytomnego Michała po ataku efemerycznego horroru, Lea podziękowała i powiedziała że mu pomoże. Jak inne REKINY poprosiły ją o pomoc, miała pretekst by mu pomóc.

A przy okazji, przypadkiem, Karo i Daniel sprowadzili na Podwiert plagę magicznych szczurów za które obwiniona jest AMZ...

Aktor w Opowieści:

* Dokonanie:
    * Rekin latający na wielkim, głośnym, ciężkim ścigaczu (bardziej czołg). Zgodził się na wyścig z Mysiokornikiem, bo czemu nie? Wulgarny, powiedział Oli Burgacz wprost, że wszyscy 'chcą umoczyć'. Przez to, że Mysiokornik się rozbił a Daniel się zmył to Bulterier wygrał wyścig i Ola go pocałowała XD. Bulterier się nieźle bawił.


### Wysadzony żywy ścigacz

* **uid:** 210928-wysadzony-zywy-scigacz, _numer względny_: 4
* **daty:** 0111-07-26 - 0111-07-27
* **obecni:** Azalia Sernat d'Namertel, Ernest Namertel, Franek Bulterier, Karolina Terienak, Keira Amarco d'Namertel, Marysia Sowińska, Rafał Torszecki, Żorż d'Namertel

Streszczenie:

Ernest z Eterni okazał się być zupełnie inny niż się wydaje. Lekko naiwny, z paladyńskim zacięciem, chce czynić dobro i integrować Rekiny dookoła Marysi. Chce dokończyć dobre dzieło Amelii. Marysia znalazła z nim linię porozumienia a Ernest x Karolina są jak dwa łyse konie. Marysia przekonała go, by sam nic nie robił - nie rozumie terenu. Niestety, ktoś wysadził żywy ścigacz i zabił ducha z jego arkinu. Ernesta zatrzymała Marysia. Karolinie nie udało się ducha uratować. Dziewczyny zdecydowały, że znajdą mordercę...

Aktor w Opowieści:

* Dokonanie:
    * chciał się bić z Ernestem. Wie, kto wysadził ścigacz Ernesta (nie wiedział, że ów żyje). Przed śmiercią do simulacrum uratowała go Karolina, wchodząc z nim w strzelankę ścigaczami - rozbił się w lesie.


### Potencjalnie eksterytorialny seksbot

* **uid:** 210406-potencjalnie-eksterytorialny-seksbot, _numer względny_: 3
* **daty:** 0111-04-27 - 0111-04-28
* **obecni:** Barnaba Burgacz, Feliks Keksik, Franek Bulterier, Henryk Wkrąż, Julia Kardolin, Lorena Gwozdnik, Marysia Sowińska, Rupert Mysiokornik, Sabina Kazitan, Triana Porzecznik

Streszczenie:

Sabina Kazitan zostawiła za sobą plan mający skonfliktować Rekiny na poziomie Podwiertu, co miałoby kaskadować na relacje rodów w Aurum. Plan zawierał Skażonego seksbota, którego Rekiny miały same zrobić. Podszepnęła też odpowiednie słówka odpowiednim osobom. Jednak gdy podatne Rekiny chciały iść za tym planem, gdy szabrowały ze złomowiska głowę seksbota to zatrzymał je Zespół. Skończyło się na tym, że seksbota nie będzie - Marysia im absolutnie zakazała cokolwiek robić, nawet, jeśli nie znała planu Sabiny.

Aktor w Opowieści:

* Dokonanie:
    * wezwany przez Marysię by zdewastować szabrowników. Zeskoczył ze ścigacza i zdewastował szabrowników. Dokładnie to lubi i w ten sposób :-).


### Bardzo straszna mysz

* **uid:** 200616-bardzo-straszna-mysz, _numer względny_: 2
* **daty:** 0110-09-18 - 0110-09-21
* **obecni:** Diana Lauris, Franek Bulterier, Gabriel Ursus, Henryk Wkrąż, Ksenia Kirallen, Laura Tesinik, Matylda Sęk

Streszczenie:

Kilka Rekinów chciało się podlizać Gerwazemu Lemurczakowi i postraszyć Dianę i Melindę. Diana w odpowiedzi sprawiła, że ich eksperymentalna 'mysz Esuriit' uciekła i zaczęła stanowić prawdziwe zagrożenie. W REAKCJI NA TO Laura i Gabriel rozbroili sytuację, wykryli obecność Diany i zatarli wszelkie ślady. Diana jednak nie wybacza i nie zapomina a Henryk też chce iść w temat dalej...

Aktor w Opowieści:

* Dokonanie:
    * Rekin który chciał pomóc Dianie i nie pozwolić, by inne osoby z Aurum wpadły w kłopoty. Więc poszedł po linii arystokratycznej do Gabriela Ursusa.


### Dom dla Melindy

* **uid:** 200524-dom-dla-melindy, _numer względny_: 1
* **daty:** 0110-09-13 - 0110-09-16
* **obecni:** Diana Lemurczak, Diana Tevalier, Franek Bulterier, Gerwazy Lemurczak, Laura Tesinik, Mariusz Trzewń, Melinda Teilert, Pięknotka Diakon

Streszczenie:

Rodzina chce Melindę z powrotem, Melinda nie chce wracać. Pięknotka szuka Melindy i znalazła ją w Arkadii. Gdy poznała sytuację, we współpracy z Dianą wsadziła Melindę Alanowi przy użyciu Chevaleresse. Wszyscy są zadowoleni, choć Melinda musi opuścić Podwiert - ale wreszcie jest bezpieczna.

Aktor w Opowieści:

* Dokonanie:
    * jeden z gangu Rekinów z Podwiertu; szczególnie zaprzyjaźiony z Dianą. Gość chodzi w łańcuchach (ozdoba) i lubi się bić, hobbystycznie.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0111-10-17
    1. Primus    : 5, @: 0111-10-17
        1. Sektor Astoriański    : 5, @: 0111-10-17
            1. Astoria    : 5, @: 0111-10-17
                1. Sojusz Letejski    : 5, @: 0111-10-17
                    1. Szczeliniec    : 5, @: 0111-10-17
                        1. Powiat Pustogorski    : 5, @: 0111-10-17
                            1. Czółenko    : 1, @: 0110-09-21
                                1. Bunkry    : 1, @: 0110-09-21
                                1. Pola Północne    : 1, @: 0110-09-21
                            1. Podwiert    : 5, @: 0111-10-17
                                1. Dzielnica Luksusu Rekinów    : 2, @: 0111-07-27
                                    1. Obrzeża Biedy    : 1, @: 0111-04-28
                                        1. Stadion Lotników    : 1, @: 0111-04-28
                                    1. Serce Luksusu    : 1, @: 0111-07-27
                                        1. Apartamentowce Elity    : 1, @: 0111-07-27
                                1. Klub Arkadia    : 1, @: 0110-09-16
                                1. Kompleks Korporacyjny    : 1, @: 0111-10-17
                                    1. Bar Ciężki Młot    : 1, @: 0111-10-17
                                1. Las Trzęsawny    : 2, @: 0111-07-27
                                1. Osiedle Leszczynowe    : 1, @: 0110-09-21
                                    1. Sklep z reliktami Fantasmagoria    : 1, @: 0110-09-21
                                1. Osiedle Rdzawych Dębów    : 1, @: 0111-10-17
                                    1. Sklep Eliksir Siekiery    : 1, @: 0111-10-17
                            1. Zaczęstwo    : 1, @: 0111-04-28
                                1. Dzielnica Kwiecista    : 1, @: 0111-04-28
                                    1. Rezydencja Porzeczników    : 1, @: 0111-04-28
                                1. Złomowisko    : 1, @: 0111-04-28

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Henryk Wkrąż         | 2 | ((200616-bardzo-straszna-mysz; 210406-potencjalnie-eksterytorialny-seksbot)) |
| Karolina Terienak    | 2 | ((210928-wysadzony-zywy-scigacz; 230325-ten-nawiedzany-i-ta-ukryta)) |
| Laura Tesinik        | 2 | ((200524-dom-dla-melindy; 200616-bardzo-straszna-mysz)) |
| Marysia Sowińska     | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 210928-wysadzony-zywy-scigacz)) |
| Rupert Mysiokornik   | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 230325-ten-nawiedzany-i-ta-ukryta)) |
| Aleksandra Burgacz   | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Azalia Sernat d'Namertel | 1 | ((210928-wysadzony-zywy-scigacz)) |
| Barnaba Burgacz      | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Daniel Terienak      | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Diana Lauris         | 1 | ((200616-bardzo-straszna-mysz)) |
| Diana Lemurczak      | 1 | ((200524-dom-dla-melindy)) |
| Diana Tevalier       | 1 | ((200524-dom-dla-melindy)) |
| Ernest Namertel      | 1 | ((210928-wysadzony-zywy-scigacz)) |
| Feliks Keksik        | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Gabriel Ursus        | 1 | ((200616-bardzo-straszna-mysz)) |
| Gerwazy Lemurczak    | 1 | ((200524-dom-dla-melindy)) |
| Julia Kardolin       | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Keira Amarco d'Namertel | 1 | ((210928-wysadzony-zywy-scigacz)) |
| Ksenia Kirallen      | 1 | ((200616-bardzo-straszna-mysz)) |
| Lea Samszar          | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Lorena Gwozdnik      | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Mariusz Trzewń       | 1 | ((200524-dom-dla-melindy)) |
| Matylda Sęk          | 1 | ((200616-bardzo-straszna-mysz)) |
| Melinda Teilert      | 1 | ((200524-dom-dla-melindy)) |
| Michał Klabacz       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Nadia Uprewien       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Oliwier Czepek       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Pięknotka Diakon     | 1 | ((200524-dom-dla-melindy)) |
| Rafał Torszecki      | 1 | ((210928-wysadzony-zywy-scigacz)) |
| Sabina Kazitan       | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Triana Porzecznik    | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Żorż d'Namertel      | 1 | ((210928-wysadzony-zywy-scigacz)) |