# Amanda Kajrat
## Identyfikator

Id: 2306-amanda-kajrat

## Sekcja Opowieści

### Wygrany kontrakt

* **uid:** 200311-wygrany-kontrakt, _numer względny_: 17
* **daty:** 0110-07-20 - 0110-07-23
* **obecni:** Amanda Kajrat, Dagmara Doberman, Ernest Kajrat, Liliana Bankierz, Lucjusz Blakenbauer, Minerwa Metalia, Ziemowit Zięba

Streszczenie:

Ernest Kajrat jest w stanie śmiertelnym; Lucjusz i Minerwa trzymają go przy życiu. Tymczasem firma dostarczająca jedzenie "Zygmunt Zając" wygrała przetarg (na który nie startowała) odnośnie jedzenia dla zixionizowanej Amandy Kajrat w rękach Grzymościa. Współpraca Blakenbauera i "Zająca" doprowadza do uwolnienia Amandy i kupienia szansy Ernestowi...

Aktor w Opowieści:

* Dokonanie:
    * Skażona energiami ixiońskimi przez Saitaera, wpadła w ręce Grzymościa, acz poniszczyła mu mafiozów solidnie


### Niespodziewany wpływ Aidy

* **uid:** 190804-niespodziewany-wplyw-aidy, _numer względny_: 16
* **daty:** 0110-06-30 - 0110-07-02
* **obecni:** Amanda Kajrat, Gabriel Ursus, Julia Morwisz, Ossidia Saitis, Pięknotka Diakon, Saitaer

Streszczenie:

Po złapaniu Aidy przez Hralwagha z Cieniaszczytu okazało się, że Aida wpłynęła na samego Hralwagha. Potwór splugawił swoich kontrolerów z Cieniaszczytu. Pięknotka współpracując z Ossidią i Saitaerem pokonała Hralwagha zanim komukolwiek stała się krzywda. Dodatkowo - Ossidia porwała Amandę Kajrat dla Saitaera; Ossidia dalej poluje na Ernesta Kajrata.

Aktor w Opowieści:

* Dokonanie:
    * dostarczyła Pięknotce dowodów odnośnie narkotyku Grzymościa by zredukować gorącą wojnę. Niestety, wpadła w ręce Ossidii i Saitaera - ma być przynętą na Kajrata.


### Bardzo niebezpieczne składowisko

* **uid:** 190726-bardzo-niebezpieczne-skladowisko, _numer względny_: 15
* **daty:** 0110-06-26 - 0110-06-28
* **obecni:** Amanda Kajrat, Cezary Alentik, Ernest Kajrat, Gabriel Ursus, Mirela Orion, Pięknotka Diakon, Roland Grzymość

Streszczenie:

Na Składowisku Odpadów Niebezpiecznych przy Podwiercie siły Wolnego Uśmiechu zaczęły składać jakieś narkotyki. Nie podoba się to Nocnemu Niebu. Pięknotka w środku tego wszystkiego - udało jej się poprosić Orbiter o zajęcie się linią dostawczą z Cieniaszczytu, Kajrata by chwilowo zwolnił, Pustogor by chronili właściciela a Grzymościa, by zszedł do podziemia. Polityczny sukces - ale czemu Grzymość idzie w jakieś narkotyki? I czemu Kajrat dąży do wojny z Grzymościem o te narkotyki?

Aktor w Opowieści:

* Dokonanie:
    * skłonna do skrzywdzenia grupy terminusów-rekrutów by rozpalić wojnę Pustogor - Wolny Uśmiech. Nie chce wojny. Nie walczyła z Pięknotką, trochę podpowiedziała. Świetnie się chowa w stealth suit.


### Kult, choroba Esuriit

* **uid:** 190714-kult-choroba-esuriit, _numer względny_: 14
* **daty:** 0110-06-07 - 0110-06-09
* **obecni:** Amanda Kajrat, Ernest Kajrat, Pięknotka Diakon, Tomasz Tukan

Streszczenie:

Tukan poluje dalej na temat somnibela i znalazł kult wywołany przez magię. Pięknotka zdobyła mu uprawnienia a on spieprzył - dał się zainfekować Esuriit i ruszył na wojnę z Kajratem. Pięknotka uratowała mu tyłek za co dostała wsparcie Amandy Kajrat. Z jej pomocą zniszczyła kult w Podwiercie, ale dużo wskazuje że w Czółenku (gdzie indziej...) jest ulubiony ciąg dalszy.

Aktor w Opowieści:

* Dokonanie:
    * traktowana przez Kajrata jako córka, chce uratować "ojca". Świetnie wyczuwa Esuriit; wskazała Pięknotce Czółenko jako źródło problemów.


### Glizda, która leczy

* **uid:** 211120-glizda-ktora-leczy, _numer względny_: 13
* **daty:** 0108-04-07 - 0108-04-18
* **obecni:** Amanda Kajrat, Amelia Sowińska, Ernest Kajrat, Feliks Keksik, Justynian Diakon, Kacper Bankierz, Karol Pustak, Oliwia Lemurczak, Roland Sowiński, Sabina Kazitan, Sensacjusz Diakon, Stella Sowińska

Streszczenie:

Sensacjusz Diakon został wysłany na prowincję pod przykrywką Lekarza Rekinów, by dojść ze Stellą do tego czemu Amelia i Roland Sowińscy zachowują się nietypowo. Na miejscu okazało się, że Roland jest w stanie ostrej wojny z mafią (która nie traktuje tego poważnie; Kajrat się bawi z Rekinami) a Amelia po prostu nie w pełni sobie radzi i próbuje opanować sytuację. Roland jest Skażony Esuriit; Sensacjusz przetransportował go glizdą do Aurum i sam, uwięziony w bojowej formie, deeskalował konflikt z mafią. A Amelia jest tymczasowym dowódcą Rekinów...

Aktor w Opowieści:

* Dokonanie:
    * przekradła się koło czterech Lancerów sterowanych przez Elainkę i zestrzeliła Rolanda Sowińskiego jak przemawiał na podwyższeniu, zmieniając go w klauna. Zwykłe ćwiczenia.


### Nauczmy młodego tiena jak żyć

* **uid:** 230808-nauczmy-mlodego-tiena-jak-zyc, _numer względny_: 12
* **daty:** 0095-09-14 - 0095-09-23
* **obecni:** Amanda Kajrat, Armin Samszar, Celina Maskiewnik, Elena Samszar, Florian Samszar, Karolinus Samszar, Kleopatra Trusiek, Rufus Bilgemener, Tadeusz Maskiewnik, Wiktor Blakenbauer

Streszczenie:

Florian chciał dać szansę Elenie i Karolinusowi i zrobił operację w której E+K mają wychować młodego Armina z pomocą rodu. Jednak E+K zadziałali w sposób pokazujący że są chętni do lekceważącego krzywdzenia ludzi i jakkolwiek osiągnęli cel z Arminem, oni sami "zawiedli". Florian dostał wpierdol. Amanda i Elena nawiązały pozytywne kontakty z Wiktorem Blakenbauerem.

Aktor w Opowieści:

* Dokonanie:
    * pozyskała od Bilgemenera ładną dziewczynę do pułapkowania Armina oraz dzięki Elenie nawiązała silny link z Wiktorem Blakenbauerem. Zastraszyła barmana pokazując zmumifikowane kobiece ucho (które nosi dla takich chwil). Ciężko zraniła Celinę pułapką (przypadkowo, nie doceniła słabości servara) ale potem poszła ją uratować przed makaronowym potworem jak żaden z magów nie uznał tego za istotne. Chroni "swoich".
* Progresja:
    * ma opinię, że współpracuje z najmroczniejszymi z mrocznych Samszarów. Bilgemener się jej boi. Złoty Cień nie stanie jej na drodze. Jednocześnie w oczach Złotego Cienia, broni swoich ludzi nawet jeśli Samszarowie ich poświęcają.
    * dzięki Elenie Samszar ma kontakt z Wiktorem Blakenbauerem, który uważa ją za bardzo potężną i wpływową agentką mafii.


### Amanda konsoliduje Złoty Cień

* **uid:** 230715-amanda-konsoliduje-zloty-cien, _numer względny_: 11
* **daty:** 0095-09-09 - 0095-09-12
* **obecni:** Amanda Kajrat, Bella Samszar, Emil Samszar, Kasimir Esilin, Rufus Bilgemener, Tadeusz Samszar, Wacław Samszar

Streszczenie:

Amanda dostaje wsparcie od Kajrata w formie Kasimira i żołnierzy; wykorzystuje to do obrony Bilgemenera i poszerzanie jego sił. Pozornie uległa Wacławowi Samszarowi, dając mu poczucie bezpieczeństwa i wciągając go w bycie słupem Amandy w Złotym Cieniu. Przekonali razem Tadeusza Samszara, by Lojalistów dodać do Złotego Cienia - a Secesjoniści i element Lojalistów to tymczasowo problem Belli. Klinika Oteriiel pozostaje nierozwiązana i 'neutralna', acz pracuje nad nią Bella.

Aktor w Opowieści:

* Dokonanie:
    * oddała smyczkę Wacławowi Samszarowi, przez co nieformalnie dowodzi Złotym Cieniem. Poszerzyła wpływy Cienia i dodała do Cienia grupę Przemytników, frontem są Wacław i Karolinus plus przejęła kontrolę nad większością Cienia z tła.
* Progresja:
    * dostała wsparcie od Kajrata. Kasimir Esilin oraz 7 noktiańskich żołnierzy. Lojalni, cisi, robią operacje militarne. Mają też akceptowalny sprzęt.
    * opinia pokornej służki Wacława Samszara u samego Wacława i innych osób widzących sytuację. Bilgemener widzi więcej, ale wie gdzie jest power level.


### Wojna w Złotym Cieniu

* **uid:** 230708-wojna-w-zlotym-cieniu, _numer względny_: 10
* **daty:** 0095-09-04 - 0095-09-08
* **obecni:** Amanda Kajrat, Nadia Obiris, Rufus Bilgemener

Streszczenie:

Amanda Kajrat zdobywa Złoty Cień dla Nocnego Nieba. Znalazła niewielką subfrakcję Bilgemenera i pokazała mu, że Samszarowie szukają często kozłów ofiarnych. Amanda wezwała wsparcie z Nieba i destabilizuje Lojalistów przeciw Secesjonistów by kupić czas, przygotowując pułapki na mniejsze grupki by ich przechwycić. A wszystko to pod patronatem Samszarów (którzy niekoniecznie o tym wiedzą).

Aktor w Opowieści:

* Dokonanie:
    * inteligentnie zinfiltrowała bazę Złotego Cienia Bilgemenera, po czym obiecała mu, że osłoni go przed gniewem Samszarów. Ściągnęła wsparcie od Kajrata by na bazie Bilgemenera odbudować Złoty Cień w nowej formie. By kupić czas, rozpaliła konflikt Secesjoniści - Lojaliści przez zabicie Secesjonisty udając Lojalistkę.
* Progresja:
    * dalej wykrywana jako powiązana z Karolinusem i Eleną, dalej ukryta w sentisieci i pasywnie niewidoczna


### Maja chciała być dorosła

* **uid:** 230704-maja-chciala-byc-dorosla, _numer względny_: 9
* **daty:** 0095-08-29 - 0095-09-01
* **obecni:** Albert Samszar, Amanda Kajrat, Jonatan Lemurczak, Karolinus Samszar, Maja Samszar, Nadia Obiris, Vanessa Lemurczak, Wirgot Samszar

Streszczenie:

Karolinus namówiony przez Kajrata chce zrzucić swoje winy na współpracujący z handlarzami ludźmi Złoty Cień. By ich zniszczyć, manipuluje młodą Maję i wystawia ją jako ofiarę dla Lemurczaków zaproszonych przez Wirgota. Maja zostaje strasznie skrzywdzona, ale Złoty Cień jest złamany, Karolinus ma sojusz z Albertem, Lemurczakowie zostali odepchnięci a Wirgot ma w Albercie śmiertelnego wroga.

Aktor w Opowieści:

* Dokonanie:
    * przyhaczyła Maję na imprezie, dała jej tabletkę i przekazała agentom Wirgota by dali ją Lemurczakowi. Potem dostała się na dach i ustrzeliła Vanessę Lemurczak gdy ta torturowała Maję. Nie dbała o stan Mai - to tienka. Wszystko dla Kajrata - przechwycić dlań grupę przestępczą Złoty Cień.


### Ratuj młodzież dla Kajrata

* **uid:** 230627-ratuj-mlodziez-dla-kajrata, _numer względny_: 8
* **daty:** 0095-08-20 - 0095-08-25
* **obecni:** AJA Szybka Strzała, Amanda Kajrat, Elena Samszar, Ernest Kajrat, Herbert Samszar, Impresja Ignicja Incydencja Diakon, Karolinus Samszar, Mitria Ira, Talia Aegis, Wacław Samszar, Wargun Ira

Streszczenie:

Herbert powiedział o lokacjach baz i że to prace na duchach. Strzała wie o tym że ma uszkodzoną psychotronikę - skontaktowała Zespół z Kajratem (S. potrzebuje pomocy Talii). Kajrat da dostęp do Talii, jeśli Zespół pomoże uratować noktiańskich młodych magów z niewoli Samszarów. Po znalezieniu bazy Karolinus wywabił na Itrię (którą dołączył do drużyny XD) Wacława (strażnika i ekstraktora) a Elena z Amandą Kajrat wydobyły nieszczęsnych noktian. Strzała odjechała do Kajrata. Kajrat zostawił Zespołowi Amandę do pomocy.

Aktor w Opowieści:

* Dokonanie:
    * 26-letnia noktiańska komandoska; ma pomóc i chronić młodych Samszarów z woli Ernesta Kajrata. Zabiła dwóch strażników bazy hiperpsychotroników, przeprowadziła Elenę bezpiecznie do uwięzionych magów.


### Legenda o noktiańskiem mafii

* **uid:** 230920-legenda-o-noktianskiej-mafii, _numer względny_: 7
* **daty:** 0081-07-10 - 0081-07-13
* **obecni:** Amanda Kajrat, Bogdan Gwiazdocisz, Dmitri Karpov, Irelia Kairanolis, Patryk Majwuron, Petra Karpov, Xavera Sirtas

Streszczenie:

Furie z Karpovami opracowywały infiltrację i jak się zachowywać jako lokalsi. Jednak pewnej nocy pojawili się Grzymościowcy by torturować Karpovów (bez sensu). Furie jako Czerwone Myszy spróbowały porwać ścigacze - nie wyszło. Skończyło się na walce (masakrze), gdzie jako NOWA mafia, noktiańska, odparły Grzymościowców (i dowiedziały się, że jest tu inna Furia, Irelia, mindwarpowana). Po dotarciu do Zaczęstwa przez Podwiert Furie wpakowały się na gang porywający młode kobiety (który ich nie porwał) i schowały się w Przytułku. Nie wiedzą o tym, ale gang na nie dalej poluje, nie wiedząc kim są...

Aktor w Opowieści:

* Dokonanie:
    * gdy nie udało się cicho ukraść ścigacza, zostawiła granat i ustrzeliła przeciwników (acz nie Irelię). Złamała morale Grzymościowców. Dotarła do Zaczęstwa.


### Operacja: spotkać się z Dmitrim

* **uid:** 230913-operacja-spotkac-sie-z-dmitrim, _numer względny_: 6
* **daty:** 0081-06-28 - 0081-06-30
* **obecni:** Amanda Kajrat, Arnulf Poważny, Dmitri Karpov, Petra Karpov, Xavera Sirtas

Streszczenie:

Furie po przejściu przez Mur nawiązały kontakt z Dmitrim (a to był Arnulf). Zostawiły sygnał, ale też zastawiły pułapkę - i skutecznie ominęły 5 servarów, acz się obie pochorowały i zapłaciły zdrowiem. Wyślizgnąwszy się z pułapki, napotkały Dmitriego i dotarły do jego fortifarmy, gdzie poznały też jego żonę, Petrę.

Aktor w Opowieści:

* Dokonanie:
    * połączyła się z 'Dmitrim', ale nie dała mu swojej pozycji; gdy wyskoczył na nią polujący technolampart, zestrzeliła go snajperką. Poraniona i w bąblach, ale uniknęła Arnulfa i dotarła z Xaverą do Dmitriego.


### Operacja: mag dla Symlotosu

* **uid:** 230906-operacja-mag-dla-symlotosu, _numer względny_: 5
* **daty:** 0081-06-26 - 0081-06-28
* **obecni:** Amanda Kajrat, Caelia Calaris, Dragan Halatis, Ernest Kajrat, Leon Varkas, Lestral Kirmanik, Xavera Sirtas

Streszczenie:

Kajrat odkrył, że to Wolny Uśmiech stoi za porwaniem Furii. Wysyła Xaverę i Amandę do Zaczęstwa, by pozyskały maga i zrobiły kanał przerzutowy. Furiom w Quispisie udało się dotrzeć do Wielkiego Muru Pustogorskiego i go przekroczyć, acz skończyły bez servarów. Mają jednak po drugiej stronie ukryte zapasy oraz kontakt do agenta po stronie Szczelińca...

Aktor w Opowieści:

* Dokonanie:
    * wysłana przez Kajrata do Zaczęstwa; zero poczucia humoru na Quispisie. Ale robi z grzmotodrzew dywersję dla patrolu i poświęca inteligentnie Culicyd by przejść przez Mur Pustogorski z Xaverą.


### Zwiad w Iliminar Caos

* **uid:** 230806-zwiad-w-iliminar-caos, _numer względny_: 4
* **daty:** 0081-06-22 - 0081-06-24
* **obecni:** Alaric Rakkeir, Amanda Kajrat, Caelia Calaris, Dragan Halatis, Edmund Garzin, Ernest Kajrat, Isaura Velaska, Leon Varkas, Lestral Kirmanik

Streszczenie:

Kajrat przedstawił nową misję - ratujemy noktian i podpinamy się pod Sojusz Letejski. Wysłał Amandę i Quispis, by wysłali sygnał używając dawno rozbitego _supply ship_ Iliminar Caos. Grupa wydzielona Quispis wykryła, że tam są salvagerzy - udało im się zastraszyć ich (udając że są Czarnymi Czaszkami) i zmusić do współpracy. Mają też jednego niewolnika salvagerów jako intel dla Kajrata.

Aktor w Opowieści:

* Dokonanie:
    * zero poczucia humoru, co sprawiło pewną konfuzję w grupie wydzielonej Quispis; jakoś się zintegrowała z grupą noktiańską. Akceptuje erotyczne żarty na swój temat, akceptuje brak profesjonalizmu przez stosunkowo niskie morale, zaproponowała plan przebrania się za Czaszki by zastraszyć salvagerów i porwała kilku wartowników salvagerów z pomocą Leona.


### Skażone schronienie w Fortecy

* **uid:** 230730-skazone-schronienie-w-fortecy, _numer względny_: 3
* **daty:** 0081-06-18 - 0081-06-21
* **obecni:** Amanda Kajrat, Ayna Marialin, Ernest Kajrat, Isaura Velaska, Leira Euridis, Raab Navan, Xavera Sirtas

Streszczenie:

Po rozbiciu awiana Furie nie mają już zasobów ani możliwości. Mimo, że Czarne Czaszki na nie polowały, uciekły do Fortecy Symlotosu (nie wiedząc co to jest). Tam dostały pomoc i spotkały Leirę (kolejną Furię) która zaakceptowała Symlotos. Furie dzielnie broniły się przed Skażeniem, skupiając się na pomocy Symlotosowi i zwalczaniem Czaszek. Niestety, operacja zwabiania Czaszek w pułapkę skończyła się ciężką raną Xavery. Ale Kajrat wspierany przez kolejną Furię, Isaurę, pozyskał Amandę i Xaverę, po czym otworzył negocjacje z Symlotosem by odzyskać Leirę i Aynę za maga.

Aktor w Opowieści:

* Dokonanie:
    * przeprowadziła Furie przez piekło do Symlotosu, tam ustrzegła je przed Skażeniem a potem opracowała plan pozyskania zasobów i dała się uratować Kajratowi. Nigdy nie straciła nadziei i wymanewrowała wszystkich przeciwników - myśleli, że Amanda się złamie, podda czy straci nadzieję, ale Amanda szła do przodu i kombinowała. Niezłomna NAWET jak na Furię.
* Progresja:
    * lekko ranna (2 dni regeneracji), przechwycona przez siły Kajrata i inkorporowana do oddziału Isaury.


### Furia poluje na Furie

* **uid:** 230729-furia-poluje-na-furie, _numer względny_: 2
* **daty:** 0081-06-17 - 0081-06-18
* **obecni:** Amanda Kajrat, Ayna Marialin, Ernest Kajrat, Ralena Karimin, Xavera Sirtas

Streszczenie:

Servary przestają działać. Amanda pozyskała stash od Kajrata z gniazda latających jaszczurów, acz kosztem swojego servara. Jedyny sprawny - Xavery - został zmodowany jako jednostka transportowa. Niestety, podpalenie gniazda jaszczurów sprawiło, że Ralena - kontrolowana przez KOGOŚ Furia - zaatakowała siłą 8 servarów i 2 awianów. Zespół dał radę zdjąć 1 awiana a drugiego porwać i jakkolwiek wszystkie Furie są zatrute, Xavera ranna, Ayna bardzo ciężko ranna, ale mają kilkanaście kilometrów przewagi nad atakującymi. Furie dzielnie się bronią, ale zaczynają przegrywać...

Aktor w Opowieści:

* Dokonanie:
    * weszła servarem w gniazdo jaszczuroskrzydeł by zdobyć _stash_ od Kajrata; opracowała sposób jak uciec od sił Wolnego Uśmiechu i go przeprowadziła. Snajperką zestrzeliła wrogiego awiana i ewakuowała się z Xaverą i Ayną do nowego miejsca. W najlepszej formie z Trzech Furii.
* Progresja:
    * lekko ranna i zatruta przez straszne korzenie w okolicach Enklaw; ma personal armour i snajperkę ppanc


### Crashlanding Furii Mataris

* **uid:** 230723-crashlanding-furii-mataris, _numer względny_: 1
* **daty:** 0081-06-15 - 0081-06-17
* **obecni:** Amanda Kajrat, Ayna Marialin, Ernest Kajrat, Lucia Veidril, Xavera Sirtas

Streszczenie:

Noktiańska jednostka przewożąca Furie Mataris została zestrzelona i część Furii spadło w okolice Pustogoru. Astorianie polują na Furie, Kajrat je ostrzega i formuje swoje Nocne Niebo. Amanda odzyskała kontakt z dwoma innymi Furiami i przechodzą przez tereny na południu Pustogoru, próbując dostać się do Pacyfiki. Tam czeka na nie Kajrat. A po drodze - rozbity dziwny noktiański statek, polujący na Furie itp.

Aktor w Opowieści:

* Dokonanie:
    * liniowa Furia Mataris która unika wykrycia przez astorian po rozbiciu się. Schowała się przed awianami, autodestructowała kapsułę, zostawiła Lucię jako dywersję nie mogąc jej pomóc i ostrożnie prze w kierunku na Pacyfikę - tylko tam jest jakakolwiek nadzieja w aktualnej beznadziejnej sytuacji.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 17, @: 0110-07-23
    1. Primus    : 17, @: 0110-07-23
        1. Sektor Astoriański    : 17, @: 0110-07-23
            1. Astoria    : 17, @: 0110-07-23
                1. Sojusz Letejski, SW    : 5, @: 0081-06-28
                    1. Granica Anomalii    : 5, @: 0081-06-28
                        1. Forteca Symlotosu    : 1, @: 0081-06-21
                        1. Las Pusty, okolice    : 1, @: 0081-06-17
                        1. Ruina Iliminar Caos    : 1, @: 0081-06-24
                        1. Ruina miasteczka Kalterweiser    : 2, @: 0081-06-28
                        1. Ruiny Kaliritosa    : 1, @: 0081-06-17
                    1. Wielki Mur Pustogorski    : 1, @: 0081-06-28
                1. Sojusz Letejski    : 12, @: 0110-07-23
                    1. Aurum    : 5, @: 0095-09-23
                        1. Powiat Blakenbauer    : 1, @: 0095-09-23
                            1. Gęstwina Duchów    : 1, @: 0095-09-23
                        1. Powiat Samszar    : 5, @: 0095-09-23
                            1. Gwiazdoczy, okolice    : 1, @: 0095-08-25
                                1. Technopark Jutra    : 1, @: 0095-08-25
                                    1. Inkubatory małego biznesu    : 1, @: 0095-08-25
                            1. Gwiazdoczy    : 1, @: 0095-08-25
                            1. Karmazynowy Świt, okolice    : 2, @: 0095-09-12
                                1. Stadion Mistrzów    : 1, @: 0095-09-08
                                1. Strefa ekonomiczna    : 2, @: 0095-09-12
                                    1. Biurowce    : 2, @: 0095-09-12
                            1. Karmazynowy Świt    : 2, @: 0095-09-08
                                1. Centralny Park Harmonii    : 2, @: 0095-09-08
                                    1. Gwiezdna Rozkosz    : 2, @: 0095-09-08
                                    1. Pałac Harmonii    : 1, @: 0095-09-01
                            1. Triticatus, północ    : 1, @: 0095-09-23
                                1. Stare magazyny    : 1, @: 0095-09-23
                            1. Triticatus    : 1, @: 0095-09-23
                                1. Makaroniarnia (NW)    : 1, @: 0095-09-23
                                    1. Pola Pszenicy    : 1, @: 0095-09-23
                        1. Verlenland    : 1, @: 0095-08-25
                            1. Duchowiec Śmieszny, okolice    : 1, @: 0095-08-25
                            1. Duchowiec Śmieszny    : 1, @: 0095-08-25
                    1. Szczeliniec    : 7, @: 0110-07-23
                        1. Powiat Pustogorski    : 7, @: 0110-07-23
                            1. Czółenko    : 1, @: 0110-06-09
                            1. Podwiert, okolice    : 2, @: 0110-07-02
                                1. Bioskładowisko podziemne    : 2, @: 0110-07-02
                            1. Podwiert    : 3, @: 0110-07-02
                                1. Dzielnica Luksusu Rekinów    : 1, @: 0108-04-18
                                    1. Serce Luksusu    : 1, @: 0108-04-18
                                        1. Lecznica Rannej Rybki    : 1, @: 0108-04-18
                                1. Las Trzęsawny    : 1, @: 0108-04-18
                                1. Magazyny sprzętu ciężkiego    : 1, @: 0110-07-02
                                1. Osiedle Leszczynowe    : 1, @: 0110-06-09
                                    1. Szkoła Nowa    : 1, @: 0110-06-09
                            1. Powiat Przymurski    : 2, @: 0081-07-13
                                1. Fortifarma Karpovska    : 2, @: 0081-07-13
                                1. Las Przymurski    : 1, @: 0081-06-30
                            1. Pustogor, okolice    : 1, @: 0110-07-23
                                1. Rezydencja Blakenbauerów    : 1, @: 0110-07-23
                            1. Zaczęstwo, obrzeża    : 1, @: 0081-07-13
                                1. Przytułek Cicha Gwiazdka    : 1, @: 0081-07-13
                            1. Zaczęstwo    : 1, @: 0110-07-23
                                1. Nieużytki Staszka    : 1, @: 0110-07-23

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ernest Kajrat        | 10 | ((190714-kult-choroba-esuriit; 190726-bardzo-niebezpieczne-skladowisko; 200311-wygrany-kontrakt; 211120-glizda-ktora-leczy; 230627-ratuj-mlodziez-dla-kajrata; 230723-crashlanding-furii-mataris; 230729-furia-poluje-na-furie; 230730-skazone-schronienie-w-fortecy; 230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Xavera Sirtas        | 6 | ((230723-crashlanding-furii-mataris; 230729-furia-poluje-na-furie; 230730-skazone-schronienie-w-fortecy; 230906-operacja-mag-dla-symlotosu; 230913-operacja-spotkac-sie-z-dmitrim; 230920-legenda-o-noktianskiej-mafii)) |
| Ayna Marialin        | 3 | ((230723-crashlanding-furii-mataris; 230729-furia-poluje-na-furie; 230730-skazone-schronienie-w-fortecy)) |
| Karolinus Samszar    | 3 | ((230627-ratuj-mlodziez-dla-kajrata; 230704-maja-chciala-byc-dorosla; 230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Pięknotka Diakon     | 3 | ((190714-kult-choroba-esuriit; 190726-bardzo-niebezpieczne-skladowisko; 190804-niespodziewany-wplyw-aidy)) |
| Rufus Bilgemener     | 3 | ((230708-wojna-w-zlotym-cieniu; 230715-amanda-konsoliduje-zloty-cien; 230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Caelia Calaris       | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Dmitri Karpov        | 2 | ((230913-operacja-spotkac-sie-z-dmitrim; 230920-legenda-o-noktianskiej-mafii)) |
| Dragan Halatis       | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Elena Samszar        | 2 | ((230627-ratuj-mlodziez-dla-kajrata; 230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Gabriel Ursus        | 2 | ((190726-bardzo-niebezpieczne-skladowisko; 190804-niespodziewany-wplyw-aidy)) |
| Isaura Velaska       | 2 | ((230730-skazone-schronienie-w-fortecy; 230806-zwiad-w-iliminar-caos)) |
| Leon Varkas          | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Lestral Kirmanik     | 2 | ((230806-zwiad-w-iliminar-caos; 230906-operacja-mag-dla-symlotosu)) |
| Nadia Obiris         | 2 | ((230704-maja-chciala-byc-dorosla; 230708-wojna-w-zlotym-cieniu)) |
| Petra Karpov         | 2 | ((230913-operacja-spotkac-sie-z-dmitrim; 230920-legenda-o-noktianskiej-mafii)) |
| Wacław Samszar       | 2 | ((230627-ratuj-mlodziez-dla-kajrata; 230715-amanda-konsoliduje-zloty-cien)) |
| AJA Szybka Strzała   | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Alaric Rakkeir       | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Albert Samszar       | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Amelia Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Armin Samszar        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Arnulf Poważny       | 1 | ((230913-operacja-spotkac-sie-z-dmitrim)) |
| Bella Samszar        | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Bogdan Gwiazdocisz   | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Celina Maskiewnik    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Cezary Alentik       | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Dagmara Doberman     | 1 | ((200311-wygrany-kontrakt)) |
| Edmund Garzin        | 1 | ((230806-zwiad-w-iliminar-caos)) |
| Emil Samszar         | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Feliks Keksik        | 1 | ((211120-glizda-ktora-leczy)) |
| Florian Samszar      | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Herbert Samszar      | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Impresja Ignicja Incydencja Diakon | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Irelia Kairanolis    | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Jonatan Lemurczak    | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Julia Morwisz        | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Justynian Diakon     | 1 | ((211120-glizda-ktora-leczy)) |
| Kacper Bankierz      | 1 | ((211120-glizda-ktora-leczy)) |
| Karol Pustak         | 1 | ((211120-glizda-ktora-leczy)) |
| Kasimir Esilin       | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Kleopatra Trusiek    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Leira Euridis        | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Liliana Bankierz     | 1 | ((200311-wygrany-kontrakt)) |
| Lucia Veidril        | 1 | ((230723-crashlanding-furii-mataris)) |
| Lucjusz Blakenbauer  | 1 | ((200311-wygrany-kontrakt)) |
| Maja Samszar         | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Minerwa Metalia      | 1 | ((200311-wygrany-kontrakt)) |
| Mirela Orion         | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Mitria Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Oliwia Lemurczak     | 1 | ((211120-glizda-ktora-leczy)) |
| Ossidia Saitis       | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Patryk Majwuron      | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Raab Navan           | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Ralena Karimin       | 1 | ((230729-furia-poluje-na-furie)) |
| Roland Grzymość      | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Roland Sowiński      | 1 | ((211120-glizda-ktora-leczy)) |
| Sabina Kazitan       | 1 | ((211120-glizda-ktora-leczy)) |
| Saitaer              | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Sensacjusz Diakon    | 1 | ((211120-glizda-ktora-leczy)) |
| Stella Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Tadeusz Maskiewnik   | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Tadeusz Samszar      | 1 | ((230715-amanda-konsoliduje-zloty-cien)) |
| Talia Aegis          | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Tomasz Tukan         | 1 | ((190714-kult-choroba-esuriit)) |
| Vanessa Lemurczak    | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Wargun Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Wiktor Blakenbauer   | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Wirgot Samszar       | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Ziemowit Zięba       | 1 | ((200311-wygrany-kontrakt)) |