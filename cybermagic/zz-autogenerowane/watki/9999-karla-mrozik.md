# Karla Mrozik
## Identyfikator

Id: 9999-karla-mrozik

## Sekcja Opowieści

### Adaptacja Azalii

* **uid:** 200623-adaptacja-azalii, _numer względny_: 8
* **daty:** 0110-09-26 - 0110-10-04
* **obecni:** Azalia d'Alkaris, Karla Mrozik, Konrad Wączak, Minerwa Metalia, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Orbiter, frakcja NeoMil, wystawili na Trzęsawisko bazę nanitkową której celem jest uczenie odludzkiej TAI klasy Azalia. Pięknotka i Minerwa poszły jako wsparcie z Pustogoru. Okazało się, że Wiktor pozwala TAI Azalii na wiarę, że adaptuje się do Trzęsawiska by przejąć nad nią kontrolę. Pięknotka wynegocjowała bezpieczną ewakuację - ale Wiktor powiedział, że zniszczy Castigator. Pięknotka nie wie jak, ale OK. Nie wygra tego. Grunt, że wszyscy są bezpieczni.

Aktor w Opowieści:

* Dokonanie:
    * z ciężkim sercem, wysłała Pięknotkę i Minerwę na ratowanie Orbitera - nie chciała przed Trzęsawiskiem ujawniać Minerwy, ale nie ma wyjścia.


### Wojna Trzęsawiska

* **uid:** 200418-wojna-trzesawiska, _numer względny_: 7
* **daty:** 0110-08-16 - 0110-08-21
* **obecni:** Gabriel Ursus, Ignacy Myrczek, Karla Mrozik, Pięknotka Diakon, Sabina Kazitan, Wiktor Satarail

Streszczenie:

Trzęsawisko odpowiedziało na atak tworząc nową bioformę na bazie Sabiny Kazitan; zaczęło przyciągać ludzi do siebie. Z uwagi na wysokie niebezpieczeństwo Karla chciała zbombardować Trzęsawisko z orbity, jak kiedyś. Pięknotka zrobiła kanał negocjacyjny Pustogor - Wiktor Satarail; Wiktor pomoże Pięknotce uspokoić Trzęsawisko, ale nie będzie bombardowania z orbity. Plus, Pustogor pomoże mu sformować nową bioformę...

Aktor w Opowieści:

* Dokonanie:
    * w niefortunnym położeniu - chce chronić ludzi (bombardowanie z orbity) i chce chronić teren (nie dotykać Trzęsawiska). Decyduje o negocjacji z Satarailem.


### Bardzo nieudane porwania

* **uid:** 190503-bardzo-nieudane-porwania, _numer względny_: 6
* **daty:** 0110-04-18 - 0110-04-20
* **obecni:** Damian Orion, Karla Mrozik, Minerwa Metalia, Mirela Orion, Nikola Kirys, Pięknotka Diakon

Streszczenie:

Kirasjerzy Orbitera dostali zadanie odbicia Minerwy i Nikoli, ale w mieszkaniu Minerwy czekała Pięknotka. Porwała z zaskoczenia Emulatorkę, uwolniła ją Cieniem i gdy Kirasjerzy uderzyli by po cichu odzyskać agentkę, Pięknotka poprosiła o pomoc Epirjon. W wyniku Pięknotce udało się ewakuować z Emulatorką, uwolniła ją Cieniem i wsadziła do Pustogoru. Potem z Karlą wynegocjowała ochronę dla Minerwy i Nikoli i jeszcze z tymi dwoma załatwiła by to było możliwe. Kirasjerzy zostali odepchnięci przez JEDNĄ terminuskę Pustogoru.

Aktor w Opowieści:

* Dokonanie:
    * amused commander; Pięknotka dała jej możliwość wbicia bolesnej szpili elitarnym Kirasjerom. Chroni Minerwę i Nikolę przed szponami Orbitera.


### Sabotaż szeptów Elizy

* **uid:** 190429-sabotaz-szeptow-elizy, _numer względny_: 5
* **daty:** 0110-04-10 - 0110-04-13
* **obecni:** Alan Bartozol, Aleksander Rugczuk, Diana Tevalier, Eliza Ira, Erwin Galilien, Karla Mrozik, Lucjusz Blakenbauer, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

Chevaleresse wpakowała się w "kult Elizy" grając z ludźmi online; powiedziała o tym Pięknotce. Pięknotka odkryła, że Eliza rozprzestrzenia swoje wpływy przez kryształy w formie biżuterii; wraz z Alanem, Erwinem i (nadal ranną) Minerwą zaprojektowali mechanizm uszkadzający sieć krystaliczną Elizy. Udało im się kupić jakiś miesiąc czasu, aż Orbiter i Pustogor znajdą lepsze rozwiązanie. Sama Chevaleresse ma się nie spotykać z takimi tam. Ale może z nimi grać online.

Aktor w Opowieści:

* Dokonanie:
    * trzeba zaryzykować zdrowie Minerwy? Ok. Trzeba być opieprzoną przez Lucjusza? Ok. Idzie za celami Barbakanu i o nic innego nie dba.


### Budowa ixiońskiego mimika

* **uid:** 190424-budowa-ixionskiego-mimika, _numer względny_: 4
* **daty:** 0110-04-03 - 0110-04-05
* **obecni:** Aleksander Rugczuk, Erwin Galilien, Karla Mrozik, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

W świetle narastających napięć w Pustogorze i czarnych artefaktów na wolności, Pięknotka wykorzystuje (za aprobatą Barbakanu) mimika który zdominował Marcela. Minerwa go psychotronicznie osłabia z perspektywy morderstw i infekuje go energią ixiońską. Pięknotka straciła go przy Dzielnicy Uciechy, ale i tak zrobił swoje (choć straty materialne są większe). Zainfekowany przez mimika wykrzyczał, że Eliza Ira wróciła oraz Saitaer się przebudził. Niepokojące dla Pięknotki.

Aktor w Opowieści:

* Dokonanie:
    * bez westchnięcia smutku autoryzowała plan Pięknotki wrobienia niewinnego Miasteczkowca i zestrzelenie go działami Wagner tylko po to, by Pustogor odzyskał spokój.


### Pustogorski Konflikt

* **uid:** 190422-pustogorski-konflikt, _numer względny_: 3
* **daty:** 0110-04-02 - 0110-04-03
* **obecni:** Erwin Galilien, Karla Mrozik, Marcel Nieciesz, Olaf Zuchwały, Pięknotka Diakon, Wojmił Siwywilk

Streszczenie:

Napięcia na linii Barbakan - Miasteczkowcy w Pustogorze są silne; działania Wiktora jedynie przyspieszyły konflikt. Na rynek dostało się sporo niebezpiecznych artefaktów i Pięknotka musiała pomóc grupie Miasteczkowców. Zdesperowani, złapali terminusa i zamknęli go w piwnicy bo podczas jednego z rajdów na Miasteczkowców magowie z Fortu Mikado porwali dwie dziewczyny. Okazało się, że za tym stoi zaawansowany mimik symbiotyczny; Pięknotka z pomocą Cienia pokonała Skażonego terminusa i uwolniła czarodziejki.

Aktor w Opowieści:

* Dokonanie:
    * raz na jakiś czas organizuje w Pustogorze kryzys, by co bardziej zawadiackich wolnościowców z Miasteczka wygnać do Wolnych Ptaków. Pustogor przede wszystkim.


### Nowa Minerwa w nowym świecie

* **uid:** 190120-nowa-minerwa-w-nowym-swiecie, _numer względny_: 2
* **daty:** 0110-01-22 - 0110-01-26
* **obecni:** Erwin Galilien, Karla Mrozik, Kasjopea Maus, Minerwa Metalia, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Minerwa wróciła. Jako czarodziejka, opiekować się ma nią Pięknotka. Minerwa nie do końca radzi sobie w nowej rzeczywistości - nie jest piękną Diakonką, ma odłamki pamięci Saitaera i aby powrócić, poświęciła życie człowieka. Zdecydowała się tymczasowo przenieść do Zaczęstwa (Karla wysłała tam wsparcie). Tymczasem Kornel Garn już planuje by ją przekonać by doń dołączyła.

Aktor w Opowieści:

* Dokonanie:
    * przekazała Minerwę pod opiekę Pięknotce i zaakceptowała, że Minerwa osiądzie raczej w Zaczęstwie. Niech Tymon ma wsparcie w Zaczęstwie (terminusi).


### Uwięzienie Saitaera

* **uid:** 181230-uwiezienie-saitaera, _numer względny_: 1
* **daty:** 0109-12-07 - 0109-12-09
* **obecni:** Karla Mrozik, Kreacjusz Diakon, Lucjusz Blakenbauer, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Pięknotka wróciła do Pustogoru, będąc po drodze zaatakowana przez latającego pnączoszpona. Odkryła, że Saitaer poważnie zmienił już Minerwę. Zmartwiła się tym, jak bardzo ta Minerwa różni się od jej przyjaciółki. Ta sprawa jest ponad siły Pięknotki - poszła do Karli, przełożonej Pustogoru. Tam dowiedziała się, że Saitaer to broń i że być może to on stoi za problemami Trzęsawiska Zjawosztup. Pustogor pojmał Saitaera i przejął wskrzeszoną Minerwę do leczenia.

Aktor w Opowieści:

* Dokonanie:
    * głównodowodząca Pustogoru; informacja od Pięknotki o Saitaerze była ostatnim ogniwem brakującym Karli do zrozumienia sprawy i wydania rozkazu przejęcia.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 8, @: 0110-10-04
    1. Primus    : 8, @: 0110-10-04
        1. Sektor Astoriański    : 8, @: 0110-10-04
            1. Astoria    : 8, @: 0110-10-04
                1. Sojusz Letejski, SW    : 1, @: 0110-04-20
                    1. Granica Anomalii    : 1, @: 0110-04-20
                        1. Pacyfika, obrzeża    : 1, @: 0110-04-20
                1. Sojusz Letejski    : 8, @: 0110-10-04
                    1. Szczeliniec    : 8, @: 0110-10-04
                        1. Powiat Pustogorski    : 7, @: 0110-08-21
                            1. Czemerta    : 1, @: 0110-08-21
                            1. Podwiert, obrzeża    : 1, @: 0110-04-20
                                1. Kosmoport    : 1, @: 0110-04-20
                            1. Podwiert    : 1, @: 0109-12-09
                                1. Kopalnia Terposzy    : 1, @: 0109-12-09
                                1. Magazyny sprzętu ciężkiego    : 1, @: 0109-12-09
                            1. Pustogor    : 6, @: 0110-04-20
                                1. Barbakan    : 1, @: 0110-01-26
                                1. Eksterior    : 3, @: 0110-04-13
                                    1. Dolina Uciech    : 1, @: 0110-04-05
                                    1. Fort Mikado    : 1, @: 0110-04-05
                                    1. Miasteczko    : 3, @: 0110-04-13
                                        1. Knajpa Górska Szalupa    : 1, @: 0110-04-03
                                1. Gabinet Pięknotki    : 1, @: 0110-01-26
                                1. Interior    : 4, @: 0110-04-20
                                    1. Bunkry Barbakanu    : 3, @: 0110-04-20
                                    1. Dzielnica Mieszkalna    : 1, @: 0110-04-03
                                    1. Laboratorium Senetis    : 2, @: 0110-04-13
                                1. Kawiarenka Ciemna Strona    : 1, @: 0110-01-26
                                1. Rdzeń    : 5, @: 0110-04-20
                                    1. Barbakan    : 3, @: 0110-04-05
                                    1. Szpital Terminuski    : 3, @: 0110-04-20
                            1. Zaczęstwo, obrzeża    : 1, @: 0110-04-20
                            1. Zaczęstwo    : 2, @: 0110-04-20
                                1. Nieużytki Staszka    : 1, @: 0110-04-05
                                1. Osiedle Ptasie    : 1, @: 0110-04-20
                        1. Trzęsawisko Zjawosztup    : 2, @: 0110-10-04
                            1. Laboratorium W Drzewie    : 1, @: 0110-08-21

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 8 | ((181230-uwiezienie-saitaera; 190120-nowa-minerwa-w-nowym-swiecie; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy; 190503-bardzo-nieudane-porwania; 200418-wojna-trzesawiska; 200623-adaptacja-azalii)) |
| Minerwa Metalia      | 6 | ((181230-uwiezienie-saitaera; 190120-nowa-minerwa-w-nowym-swiecie; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy; 190503-bardzo-nieudane-porwania; 200623-adaptacja-azalii)) |
| Erwin Galilien       | 4 | ((190120-nowa-minerwa-w-nowym-swiecie; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Olaf Zuchwały        | 3 | ((190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Aleksander Rugczuk   | 2 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Lucjusz Blakenbauer  | 2 | ((181230-uwiezienie-saitaera; 190429-sabotaz-szeptow-elizy)) |
| Wiktor Satarail      | 2 | ((200418-wojna-trzesawiska; 200623-adaptacja-azalii)) |
| Alan Bartozol        | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Azalia d'Alkaris     | 1 | ((200623-adaptacja-azalii)) |
| Damian Orion         | 1 | ((190503-bardzo-nieudane-porwania)) |
| Diana Tevalier       | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Eliza Ira            | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Gabriel Ursus        | 1 | ((200418-wojna-trzesawiska)) |
| Ignacy Myrczek       | 1 | ((200418-wojna-trzesawiska)) |
| Kasjopea Maus        | 1 | ((190120-nowa-minerwa-w-nowym-swiecie)) |
| Konrad Wączak        | 1 | ((200623-adaptacja-azalii)) |
| Kreacjusz Diakon     | 1 | ((181230-uwiezienie-saitaera)) |
| Marcel Nieciesz      | 1 | ((190422-pustogorski-konflikt)) |
| Mirela Orion         | 1 | ((190503-bardzo-nieudane-porwania)) |
| Nikola Kirys         | 1 | ((190503-bardzo-nieudane-porwania)) |
| Sabina Kazitan       | 1 | ((200418-wojna-trzesawiska)) |
| Tymon Grubosz        | 1 | ((190120-nowa-minerwa-w-nowym-swiecie)) |
| Wojmił Siwywilk      | 1 | ((190422-pustogorski-konflikt)) |