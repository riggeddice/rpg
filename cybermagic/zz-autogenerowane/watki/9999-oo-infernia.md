# OO Infernia
## Identyfikator

Id: 9999-oo-infernia

## Sekcja Opowieści

### Lewiatan za Pandorę

* **uid:** 220622-lewiatan-za-pandore, _numer względny_: 49
* **daty:** 0112-09-30 - 0112-10-03
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Ola Szerszeń, OO Infernia, OO Pandora, OO Straszliwy Pająk, OO Tivr, Raoul Lavanis, Rzeźnik Parszywiec Diakon

Streszczenie:

PRZESZŁOŚĆ: Ola Szerszeń opiekuje się Eleną w Regeneratorze; okazuje się być biosyntem (zakazane eksperymenty Termii). Rzeźnik Diakon to łagodny i jowialny kapitan Straszliwego Pająka, acz znienawidzony za przeszłość zdrajcy Orbitera oraz Noctis. TERAŹNIEJSZOŚĆ: Lewiatan zostaje zestrzelony torpedą anihilacyjną, dzięki Raoulowi ratujemy wielu ludzi na orbicie Lewiatana. Malictrix d'Pandora zostaje zniszczona przez Lewiatana, ale Klaudia ma jego mikrokopię dzięki Ariannie.

Aktor w Opowieści:

* Dokonanie:
    * wystrzeliła torpedę anihilacyjną w Lewiatana. Skutecznie. Eustachy jest dowódcą Inferni do końca świata.


### Lewiatan przy Seibert

* **uid:** 220615-lewiatan-przy-seibert, _numer względny_: 48
* **daty:** 0112-09-27 - 0112-09-29
* **obecni:** Arianna Verlen, Eszara d'Seibert, Eustachy Korkoran, Jonasz Parys, Klaudia Stryk, Ola Szerszeń

Streszczenie:

W okolicach Anomalii Kolapsu obudził się Lewiatan. Ruszył na sortownię Seibert, pożerając kilka jednostek. Grupa Infernia, która miała ćwiczenia (i pokonała koloidową korwetę Zająca 3) dała radę zlokalizować Lewiatana, zabezpieczyć się przed aspektem bazyliszka, zdobyć trochę jego energii do badań i przygotować się do neutralizacji. Jakoś.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * lekko uszkodzona, trafiona przez Lewiatana podczas lotu koszącego.


### Kult Saitaera w Neotik

* **uid:** 211222-kult-saitaera-w-neotik, _numer względny_: 47
* **daty:** 0112-04-30 - 0112-05-01
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kasandra Destrukcja Diakon, Klaudia Stryk, Lutus Amerin, Maciek Kalmarzec, Maria Naavas

Streszczenie:

Klaudia wróciła z bronią anty-Serenitową od Tosena, Arianna z feromonami Marii nadała Inferni nową kulturę - miłośnicy lolitek x kultyści x noktianie. Elena uziemiona w Inferni zinfiltrowała Stocznię ukryta przez Klaudię i odkryła niższe Spustoszenie + kult Saitaera. Nereida jest czysta od Saitaera; przetransferowana na Infernię dla Eleny i uzbrojona. Klaudia ma listę jednostek potencjalnie w rękach Saitaera / Spustoszonych. Przygotowania do kontrataku skończone.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * Nereida przekazana do Inferni jako jednostka eksperymentalna którą ma sterować Elena. Tymczasowo.
    * unikalna kultura: noctis x kult Vigilus - Nihilus x miłośnicy Diany - lolitki z dziwnymi żartami. Stworzona przez syntezę mowy Arianny, zdolności Izy i feromonów Marii.


### Sklejanie Inferni do kupy

* **uid:** 211215-sklejanie-inferni-do-kupy, _numer względny_: 46
* **daty:** 0112-04-27 - 0112-04-29
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Kamil Lyraczek, Kasandra Destrukcja Diakon, Leona Astrienko, Maria Naavas, OO Infernia, Roland Sowiński

Streszczenie:

Ixioński wariant Inferni okazuje się całkiem słodki i pod kontrolą Eustachego. Morale Inferni jest w ruinie. Z pomocą Marii Arianna przesuwa kulturę Inferni w stronę "kultyści + noktianie + miłośnicy psychopatycznej lolitki Diany". Eustachy wpada w pułapkę Leony, która chce go wyleczyć czerwiem Esuriit - ale Eustachy przekonuje ją, że ma Dianę pod kontrolą. Jednak gdzieś w tle znajduje się prawdziwy ixioński potwór na stoczni Neotik.

Aktor w Opowieści:

* Dokonanie:
    * żywy, regenerujący się ixioński statek. Znajduje się w zewnętrznym, ixion-friendly doku dobudowanym i niestycznym z Neotik.
* Progresja:
    * Eustachy udowodnił, że jak był torturowany przez Leonę to jednak Infernia nie zrobiła nic głupiego. Więc nie jest tak niebezpieczna dla załogi jak się wydawało.


### O krok za daleko

* **uid:** 211208-o-krok-za-daleko, _numer względny_: 45
* **daty:** 0112-04-25 - 0112-04-26
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Hestia d'Neotik, Jarosław Szarjan, Klaudia Stryk, Leona Astrienko, OO Infernia, Wawrzyn Rewemis

Streszczenie:

Klaudia doszła do tego, że Hestia d'Neotik oszukuje Stocznię; w okolicy są niewielkie statki koloidowe o których Stocznia nie wie. Arianna zintegrowała Eustachego z Infernią ixiońsko, niestety anomalizując Infernię. Elena ciężko Skażona ixionem i (w swoim mniemaniu) porzucona przez Eustachego. Eustachy ujarzmiając Infernię rozbijał ją o różne asteroidy, co powoduje śmierć części załogi. Infernia ma zerowe morale, ale jest opanowana. Przyjazna Anomalia Kosmiczna z tymczasową bazą w Stoczni Neotik.

Aktor w Opowieści:

* Dokonanie:
    * anomalizacja ixiońska. Infernia ożyła, jest współpracującą z Eustachym Anomalią Kosmiczną; rolę TAI przejęła efemeryczna Diana. Próbując sił z Eustachym została zdemolowana i wróciła ciężko uszkodzona do portu.
* Progresja:
    * uległa trwałej Anomalizacji; klasyfikacja jako 'AK Infernia', typ: anomalia ixiońska. Ale jest przyjazną anomalią; z Dianą jako TAI, integrującą Persefonę, Morrigan, Dianę itp. Wchłonęła część załogi (19% straconych, czyli 46 osób), przechwytując ich wiedzę, sekrety itp. Dzięki działaniom Klaudii i ixionowi ma zdolności do syntezy sprzętu i małych jednostek ze swojej materii. Jednostka silnie polimorficzna - traktujmy jak super-nanitkową jednostkę przez ixion, acz ultrawrażliwa na Esuriit.
    * Infernia / Diana słucha się Eustachego i jest w nim zakochana. Jest wobec niego pokorna.
    * dwa tygodnie regeneracji po wyniszczeniach związanych z byciem ujarzmianą przez Eustachego.


### Prototypowa Nereida Natalii

* **uid:** 211124-prototypowa-nereida-natalii, _numer względny_: 44
* **daty:** 0112-04-20 - 0112-04-24
* **obecni:** Adam Szarjan, Arianna Verlen, Arkadia Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Maria Naavas, Natalia Aradin, OO Infernia, Roland Sowiński, Wawrzyn Rewemis

Streszczenie:

Kampania reklamowa "dołącz do Inferni" zrobiona przez Izę przy użyciu Eustachego jako modela - sukces. Klaudia zdobyła z tego nowych załogantów (acz to jeszcze potrwa). Szarjan uwolnił Klaudię z zarzutów, potrzebuje pomocy w stoczni - jego przyjaciółka Natalia ma problem z prototypem myśliwca klasy Nereida i została ixiońsko zintegrowana. Infernia próbowała Nereidę uratować, ale niestety skończyło się to poważnym uszkodzeniem Nereidy i stworzeniem ixiońskiego kokonu, z którego Natalia musi się wykluć...

Aktor w Opowieści:

* Dokonanie:
    * uruchomiona po raz pierwszy od dawna, z nowymi (tymczasowymi) członkami załogi; uczestniczyła w operacji zestrzelenia prototypu Nereidy. Zintegrowała się z Eustachym by dorównać prędkości Nereidy. Dzięki działaniom Klaudii i kampanii reklamowej dookoła Eustachego ma załogę.
* Progresja:
    * użyte przez Eustachego sprzężenie sprawiło, że Infernia poprzesuwała część dział robiąc strukturalne uszkodzenia jednostki.
    * dzięki działaniom Klaudii i kampanii reklamowej dookoła Eustachego ma załogę. Nie jest to NAJLEPSZA załoga, ale good enough. Za miesiąc mamy sprawną "nową" załogę Inferni.


### Rzieza niszczy Infernię

* **uid:** 211027-rzieza-niszczy-infernie, _numer względny_: 43
* **daty:** 0112-03-28 - 0112-04-02
* **obecni:** Arianna Verlen, Elena Verlen, Flawia Blakenbauer, Klaudia Stryk, Martyn Hiwasser, Otto Azgorn, TAI Rzieza d'K1

Streszczenie:

Infernia wraca z Mevilig. Rzieza chce wyczyścić im pamięć o sobie. Arianna się opiera. Rzieza dowiaduje się o Ataienne. Arianna budzi Elenę po sympatii, Elena sprowadza Esuriit do laboratorium dekontaminacyjnego K1. Dużo śmierci. Martyn próbuje to opanować - budzi swoje simulacrum. Arianna i Klaudia opanowują przy pomocy Rziezy sytuację. 37% załogi Inferni nie żyje, Flawia wyssana w kosmos, Elena złamana, Martyn w szpitalu.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * w wyniku dewastacji załogi przez simulacrum Martyna i emisję Esuriit Eleny, Infernia straciła 37% załogi. I Flawię. A załoga Inferni nie może mieć wymazanej pamięci.


### Kurczakownia

* **uid:** 211020-kurczakownia, _numer względny_: 42
* **daty:** 0112-03-24 - 0112-03-26
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, OO Infernia, Otto Azgorn, Vigilus Mevilig

Streszczenie:

By uratować jak najwięcej osób w Sektorze Mevilig, Eustachy zaprojektował Kurczakownię - maszynę do oddzielania mechanicznego głów i rdzenia od reszty człowieka. Arianna starła się ze Zbawicielem-Niszczycielem jako aspekt Zbawiciela, wspierana przez Działo Rozpaczy (Eustachy). Udało im się sporo uratować (40% populacji Planetoidy Kalarfam), acz załoga Inferni będzie mieć koszmary senne. Klaudia zebrała dość danych, by zapobiec naturalnej manifestacji Zbawiciela-Niszczyciela w Sektorze Astoriańskim i zrobić kontrakcję jak co.

Aktor w Opowieści:

* Dokonanie:
    * uratowano [1500, 2000] eks-kultystów Esuriit. Uratowano 29 Orbiterowców z Sektora Mevilig. Serio, świetna robota - choć koszmary senne załogi.
* Progresja:
    * traci Działo Rozpaczy, ale za to owo działo się przydało. Infernia jest uszkodzona; nic bardzo poważnego, ale 2 tygodnie naprawy są konieczne.
    * zdecydowana większość załogi jest silnie straumatyzowana przez kurczakowanie - rekurczakowanie. Wiedzą, że to jest potrzebne, ale... to ZŁE.


### Grupa Ekspedycyjna Kellert

* **uid:** 210929-grupa-ekspedycyjna-kellert, _numer względny_: 41
* **daty:** 0112-03-13 - 0112-03-16
* **obecni:** Adam Nerawol, Aleksandra Termia, Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Olena Orion, OO Infernia, OO Omega Septius, Otto Azgorn, TAI Marszałek Grzmotoszpon Trzeci, Vigilus Mevilig

Streszczenie:

Termia wysłała siły do sektora "Noviter", ale to był inny sektor (Mevilig). Straciła bezzałogowce, potem grupę ekspedycyjną Kellert. Kramer wysłał Infernię zakoloidowaną; okazało się, że ten sektor ma "strażnika Esuriit" zrobionego przez ludzi i grupę Piranii, sterowanych przez TAI 4 poziomu. Inferni udało się zebrać dane i wrócić z siedmioma uratowanymi członkami OO Savera, gdzie większość załogi została corruptowana przez Vigilusa...

Aktor w Opowieści:

* Dokonanie:
    * zakoloidowana, z dwoma torpedami anihilacyjnymi, wysłana przez Bramę do nieznanego sektora by uratować Grupę Ekspedycyjną Kellert. Sukces - Infernia + siedem osób wróciło żywe.


### Ostatnia akcja bohaterki

* **uid:** 210922-ostatnia-akcja-bohaterki, _numer względny_: 40
* **daty:** 0112-02-23 - 0112-03-09
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Izabela Zarantel, Jolanta Kopiec, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Olgierd Drongon, OO Żelazko, Roland Sowiński

Streszczenie:

Z Martynem skontaktowała się tien Kopiec prosząc o ostatnią misję zanim Esuriit ją pożre. Martyn zorganizował lot Infernią. Ustabilizował Jolantę, by ta dała radę dolecieć słowami i opowieściami, przy okazji tworząc piękną opowieść o Eterni i eulogię dla Jolanty - bohaterki wojennej - w Sekretach Orbitera (przy okazji wyszła jego przeszłość). Infernia zniszczyła Krwawą Bazę Piratów kosztem dewastacji Żelazka. Jolanta zniszczyła krwawego maga. Sektor jest bezpieczniejszy, Eternia szczęśliwa a Arianna ma profity.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * tydzień z głowy na pomniejsze naprawy. Całość ognia wzięło na siebie Żelazko.
    * jedyna jednostka Orbitera, która cieszy się zaufaniem i sympatią ze strony większości Eterni.


### Stabilizacja Bramy Eterycznej

* **uid:** 210901-stabilizacja-bramy-eterycznej, _numer względny_: 39
* **daty:** 0112-02-09 - 0112-02-12
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Gilbert Bloch, Janus Krzak, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Medea Sowińska, OO Kanagar, OO Netrahina, OO Trasman, Seweryn Atanair

Streszczenie:

Infernia wróciła z innymi jednostkami naprawić Bramę Kariańską. Eustachy ufortyfikował i uniewidocznił pintki; Arianna pozyskała dane z reanimowanego magią krążownika Orbitera. Przez Bramę przeszła ON Catarina; Klaudia ją zlokalizowała i wystawiła Netrahinie i Niobe. Niestety, Arianna hiperlinkowała się z ON Spatium Gelida. Dla nich minęło tylko kilka godzin, dla nas - kilkadziesiąt lat...

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * dorobiła się bazy danych Sił Specjalnych odnośnie jednostek Orbitera.


### Uszkodzona Brama Eteryczna

* **uid:** 210825-uszkodzona-brama-eteryczna, _numer względny_: 38
* **daty:** 0112-02-04 - 0112-02-07
* **obecni:** AK Nocna Krypta, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Gilbert Bloch, Klaudia Stryk, Medea Sowińska, ON Spatium Gelida, OO Infernia, OO Mfumo

Streszczenie:

Infernia została przechwycona przez Medeę lecąc na Anomalię Kolapsu i przekierowana na uszkodzoną Bramę Eteryczną (gdzie, jak się okazuje, są pasożyty i flota noktiańska która nie doleciała). Na miejscu Infernia uniknęła efemeryd i channelując Kryptę uratowała wszystkich efemerydami. Gdy Krypta pojawiła się na serio, Infernia zwiała paląc silniki. Statki noktiańskie w Bramie mają tether na Infernię.

Aktor w Opowieści:

* Dokonanie:
    * świetnie ekranowana przed anomalną energią, uciekła Krypcie na przesterowanych silnikach przez Eustachego.
* Progresja:
    * perfekcyjnie zamaskowana przed bytami taumicznymi, acz to kosztuje strasznie dużo reaktora jak maskowanie jest włączone. Straciła bycie Q-Ship póki to ma.
    * silniki mają 20% mocy (uszkodzenie przez Eustachego). Manewrowność Tucznika... i nie ma szans na drydock...


### Nieudana infiltracja Inferni

* **uid:** 210616-nieudana-infiltracja-inferni, _numer względny_: 37
* **daty:** 0112-01-27 - 0112-02-01
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Marian Tosen, OO Opresor, Roland Sowiński

Streszczenie:

Marian Tosen poprosił Infernię o to, by sprawdzili co się dzieje w Anomalii Kolapsu - podejrzewa anomalie memetyczne, skoro Orbiter traci tam statki  (m.in. OO Opresor) i pojawiają się jednostki nieoznaczonej frakcji. Tymczasem Roland Sowiński chce na pokład. Arianna wybiła mu to z głowy, ale wykryli agentkę wśród jego guwernantek - Flawię Blakenbauer, która miała się nim opiekować i jest trochę zniewolona przez Sowińskich. By zadośćuczynić po tym co Elena zrobiła i trochę Flawii pomóc, Flawia dołączyła do Inferni. "Oficjalnie" udało jej się Infernię zinfiltrować...

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * wzbogaciła się o 10 'Kirasjerów' (roboty bojowe Tosena), drony i point defense.


### Sekrety Kariatydy

* **uid:** 210609-sekrety-kariatydy, _numer względny_: 36
* **daty:** 0112-01-10 - 0112-01-13
* **obecni:** Arianna Verlen, Eustachy Korkoran, Marian Tosen, OO Kariatyda, Rafael Galwarn, Roland Sowiński

Streszczenie:

Arianna chce Tivr do swojej floty. Do tego celu zdecydowała się poznać sekrety Kariatydy - czemu Walrond tak unika tego statku i nazwy? Niestety, Arianna i Eustachy rozognili opowieść o "Orbiterze, który porzucił swoich w Anomalii Kolapsu" a Arianna dowiedziała się dyskretnie, że OO Kariatyda to specjalny statek - nie można powiedzieć, że był zniszczony. Ten statek został porwany przez TAI i uciekł do Anomalii Kolapsu. Arianna, promienna mistrzyni PR, zmieniła to w "KONKURS. Wyślij KARIATYDA i wygrasz przejażdżkę luksusowym statkiem". Aha, Roland Sowiński (uratowany przez Ariannę z Odłamka Serenita) chce się z nią ożenić.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * opancerzona "chityną" i z zewnętrzną Persefoną. Eustachy uczynił cuda tym, co miał dzięki Aurum.


### Morderstwo na Inferni

* **uid:** 210526-morderstwo-na-inferni, _numer względny_: 35
* **daty:** 0111-12-31 - 0112-01-06
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Feliks Walrond, Kamil Lyraczek, Karol Reichard, Klaudia Stryk, Leona Astrienko, Marian Tosen, Martyn Hiwasser, OO Tivr, Otto Azgorn, Tal Marczak

Streszczenie:

Noktiański mechanik Inferni, Tal Marczak, chciał zniszczyć OO Tivr (własność noktiańskiej rodziny Tala). Noktianie trzymają się razem - inni mechanicy Inferni doprowadzili do morderstwa Tala na K1 i wzięli amnestyki, by nikt nie wiedział co się stało. Zespół Inferni skutecznie doszedł do tego o co chodzi, odpowiedzialnych za morderstwo noktian przeniesiono na Żelazko a Arianna poważnie opieprzyła swoją noktiańską załogę. Będzie lepiej w przyszłości.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * w apteczkach Inferni wszyscy mają pomniejsze amnestyki, uaktualnione z rozkazu Martyna Hiwassera (po "Osiemnastu Oczach").
    * załoga jest nieufna wobec K1 i Orbitera. Zwiera szyki przeciwko "obcym". Tylko Arianna i "nasi" z Inferni. A noktianie trzymają się tylko razem.


### Ewakuacja z Serenit

* **uid:** 210512-ewakuacja-z-serenit, _numer względny_: 34
* **daty:** 0111-11-23 - 0111-12-02
* **obecni:** Aida Serenit, AK Serenit, Arianna Verlen, Bogdan Anatael, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OE Falołamacz, OO Infernia, Roland Sowiński

Streszczenie:

Klaudia spowolniła Serenit, po czym Elena weszła zdalnym Entropikiem na pokład Falołamacza. Arianna zmanipulowała ludzi by dało się ich uratować. Samobójcza pintka zniechęciła Falołamacz do dalszej walki. Nie wszystkich udało się uratować (ale kapitana i arystokratę Sowińskich tak). Niestety, Infernia skończyła jako kosmiczny wrak i musiała wysłać SOS...

Aktor w Opowieści:

* Dokonanie:
    * straciła drugą pintkę. Uszkodzenie strukturalne. Jeszcze dała radę skosić Falołamacz lekkimi działkami, ale po odleceniu Falołamacza się wyłączyła...
* Progresja:
    * kosmiczny wrak. JESZCZE ledwo lata, ale jest już strukturalnie uszkodzona. Nie doleci sama na Kontroler Pierwszy, ale uciekła Serenitowi...


### Infekcja Serenit

* **uid:** 210428-infekcja-serenit, _numer względny_: 33
* **daty:** 0111-11-22 - 0111-11-23
* **obecni:** AK Serenit, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OE Falołamacz, OE Piękna Elena, OO Infernia, Persefona d'Infernia, Rafał Grambucz, Tadeusz Ursus

Streszczenie:

Ciężko uszkodzona Infernia wraca do domu i dostaje SOS od Ursusa - OE Falołamacz "zniknął", coś z anomaliami. Gdy Zespół dotarł do Falołamacza, próbowali dowiedzieć się co się dzieje - faktycznie mamy anomaliczny okręt. Podczas badań Falołamacza Morrigan przejęła Persefonę Inferni; Eustachy musiał poważnie uszkodzić Infernię by nie zniszczyli Falołamacza. Po kosmicznym spacerze Eleny i Klaudii doszli do tego co się stało - Falołamacz staje się Odłamkiem Serenit. I jeszcze da się ludzi ratować. Ale Infernia jest w bardzo złym stanie...

Aktor w Opowieści:

* Dokonanie:
    * bardzo ciężko uszkodzona; poszły silniki, life support i sporo innych komponentów. Morrigan przejęła Persefonę. Ale dalej skutecznie służy Orbiterowi i wykonuje misję.
* Progresja:
    * przeciążone silniki. Ledwo sprawne, przegrzane bronie. Martwy lifesupport. Sensory poodcinane. Systemy niekrytyczne nie działają. Statek ledwo trzyma się kupy.


### Znudzona załoga Inferni

* **uid:** 210421-znudzona-zaloga-inferni, _numer względny_: 32
* **daty:** 0111-11-16 - 0111-11-19
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Morrigan d'Tirakal, Otto Azgorn, Persefona d'Infernia, Tomasz Sowiński

Streszczenie:

Po raz pierwszy od dawna Infernia jest w stanie rozprzężenia - wracają z daleka po trudnej misji i nic im nie grozi. Jako, że załoga ma straszne tarcia (Leona poluje na "słabe jednostki", komandosi Verlenów chcą zatrzymać Leonę, noktianie vs astorianie...), Arianna autoryzuje Eustachego do zrobienia "Tirakala" jako niebezpieczną sytuację. Eustachy reanimuje Tirakala, ale niestety cholerstwo zanomalizowało - przywrócił Morrigan. By Zespół nie rozwiązał Tirakala za szybko, Eustachy trochę sabotował Infernię co Elena przeczytała jako... umówienie się na randkę. Gdy Klaudia wykryła, że na INFERNI pojawiła się straszna anomalia (Morrigan) próbowali to usunąć - ale Eustachy przecież sabotował Infernię! Udało im się opanować Morrigan, ale kosztem uszkodzeń i obrażeń na Inferni. I scenek kompromitujących Jolantę Sowińską. Infernia jest uszkodzona, ale się trzyma.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * uszkodzona w wielu miejscach (fabrykacja Tirakala, walka z Morrigan) z lekko rannymi. Na pewno nie działa system rozrywkowy i uszkodzona virtsfera. Jej TAI jest Skażone, zintegrowane Morrigan i Samuraj Miłości.


### Arianna podbija Asimear

* **uid:** 210317-arianna-podbija-asimear, _numer względny_: 31
* **daty:** 0111-10-18 - 0111-11-02
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Martyna Bianistek, SCA Płetwal Błękitny, Tomasz Sowiński

Streszczenie:

"Płetwal Błękitny" ma na pieńku z "Goldarionem"; by nie złamać konspiracji, Klaudia potraktowała Płetwala Bazyliszkiem. Tymczasem pojawiła się wiadomość, że Arianna chce zniszczyć Asimear i żąda wydania Sowińskiej. Prawdziwa Arianna złamała konspirę, skontaktowała się z Jolantą Sowińską, po czym ją porwała. Okazało się, że Jolanta jest kontrolowana przez kralotycznego pasożyta...

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * straciła 1 ze swoich dwóch pinas; Elena nie dała rady wycofać się dość szybko przed tajemniczymi jednostkami.


### Infernia jako Goldarion

* **uid:** 210218-infernia-jako-goldarion, _numer względny_: 30
* **daty:** 0111-09-16 - 0111-10-01
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, OO Infernia, Tomasz Sowiński

Streszczenie:

Infernia udaje Goldariona, by przetransportować tajną przesyłkę Sowińskich do Jolanty Sowińskiej na planetoidzie Asimear. Tomasz Sowiński na Inferni okazuje się być dużym dzieckiem, łatwo jest wmanipulowany w otwarcie paczki - jest tam nietypowy antynanitkowy Entropik (którego psuje Elena przez błędną integrację). W tle wszystkiego - próba zdobycia niezależnej od Orbitera floty przez Aurum i jeden znikający advancer Kramera na Asimear...

Aktor w Opowieści:

* Dokonanie:
    * oficjalnie w okolicach Valentiny i ma refit. Faktycznie - udaje Goldariona i leci w kierunku planetoidy Asimear.


### Sympozjum Zniszczenia

* **uid:** 210120-sympozjum-zniszczenia, _numer względny_: 29
* **daty:** 0111-08-01 - 0111-08-05
* **obecni:** Arianna Verlen, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Martyn Hiwasser, OA Bakałarz, Olgierd Drongon

Streszczenie:

Maria Naavas próbuje spiknąć Ariannę i Olgierda i do pomocy używa Eustachego. Później - Eustachy, Diana, Klaudia i Elena są na Sympozjum Zniszczenia OA Bakałarz. Próbują przebić się przez anomalną materię. Diana odkrywa słabość owej materii do Esuriit i się Skaża; Skażenie rozlewa się po Bakałarzu i Zespół dostaje opiernicz za niekompetencję (większość na Elenę). Zespół w gniewie, nienawiści i Esuriit fabrykuje Emiter Plagi Nienawiści - nową broń na pokład Inferni, piekielnie niebezpieczną...

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * dostaje na pokład Emiter Plagi Nienawiści, działo sfabrykowane przez Zespół, które jest przeklęte i rozprzestrzenia nienawiść wśród osób w aurze - plus, skutecznie niszczy to co trafi.


### Porwanie Anastazji z Odkupienia

* **uid:** 210127-porwanie-anastazji-z-odkupienia, _numer względny_: 28
* **daty:** 0111-07-22 - 0111-07-23
* **obecni:** Anastazja Sowińska Dwa, Arianna Verlen, Dariusz Krantak, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OA Odkupienie, OO Infernia, Rufus Niegnat, SP Plugawy Jaszczur

Streszczenie:

Arianna, Eustachy i Klaudia oddali "Anastazję" na OA Odkupienie tylko po to, by ją stamtąd porwać. Nawet załoga Inferni o tym nie miała pojęcia. Okazało się, że Odkupienie jest sentisieciowanym specjalnym statkiem Aurum, gdzie ci co narazili się Sowińskim mają szansę na "odkupienie".

Aktor w Opowieści:

* Dokonanie:
    * pod dowodzeniem Klaudii wykonała operację "odbijmy Anastazję od Sowińskich, z Odkupienia". Ma (słuszną) reputację psującej się.
* Progresja:
    * ma reputację "psującej się", dzięki czemu nikt nie podejrzewa że pokonała OA Odkupienie.


### Pułapka z Anastazji

* **uid:** 201230-pulapka-z-anastazji, _numer względny_: 27
* **daty:** 0111-07-19 - 0111-07-20
* **obecni:** AK Nocna Krypta, AK Rodivas, Anastazja Sowińska Dwa, Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Henryk Sowiński, Klaudia Stryk, Martyn Hiwasser, OA Zguba Tytanów, OO Infernia

Streszczenie:

Krypta "oddała Anastazję", zmieniając cywilny statek Rodivas w pułapkę na Ariannę.

Aktor w Opowieści:

* Dokonanie:
    * pobiła rekord prędkości z Eleną u steru, Arianną jako siła magiczna i Eustachym przesterowującym systemy - dzięki temu przegoniła Zgubę Tytanów.


### Infernia jest nasza!

* **uid:** 210804-infernia-jest-nasza, _numer względny_: 26
* **daty:** 0111-06-21 - 0111-06-24
* **obecni:** Aleksandra Termia, Antoni Kramer, Arianna Verlen, Artur Traffal, Diana d'Infernia, Eustachy Korkoran, Franciszek Maszkiet, Klaudia Stryk, Leona Astrienko, Maciej Żarand, OO Samotność Gwiazd

Streszczenie:

Ku wielkiemu niezadowoleniu Zespołu, Infernia miała im być odebrana i złomowana. Mieli dostać nowy tańszy statek. Zespół odkrył że za tym stoi komodor Traffal z sił Termii który przy użyciu Inferni chciał złapać w potrzask arystokratę eternijskiego który był kultystą magii. Zespół odzyskał Infernię, przesunął kultystę w kierunku na Ariannę i uciemiężył komodora Traffala - będzie miał pecha na K1.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * opinia bardzo drogiego statku, na poziomie krążownika a nie fregaty dzięki komodorowi Traffalowi.
    * opinia - tylko Arianna Verlen może kontrolować Infernię. Ten statek jest po prostu zbyt "chory" i anomalny.


### W cieniu Nocnej Krypty

* **uid:** 210728-w-cieniu-nocnej-krypty, _numer względny_: 25
* **daty:** 0111-05-20 - 0111-06-06
* **obecni:** AK Nocna Krypta, Arianna Verlen, Atrius Kurunen, Eustachy Korkoran, Finis Vitae, Gerard Adanor, Helena Adanor, Janus Krzak, Oliwia Karelan, Romana Arnatin, Romana Arnatin, Ulisses Kalidon

Streszczenie:

Jak wrócić z tajnej noktiańskiej bazy oderwanej od świata i Bram? Oczywiście, Nocną Kryptą. Arianna wezwała Kryptę i Infernia schowała się w jej cieniu przenosząc się między rzeczywistościami. Po drodze udało się Inferni doprowadzić do zniszczenia niewielkiej niegroźnej floty używając Krypty, trafiła do przeszłości Krypty i widziała Finis Vitae - ale wróciła przez Anomalię Kolapsu do domu. Bo Krypta jest połączona z Anomalią Kolapsu. Aha, część noktian z Inferni została w Zonie Tres.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * przeszły wysoki poziom anomalizacji Inferni pozwalał na ukształtowanie jej na nowo. Wyższe osiągi. Większe możliwości. Co można zrobić / wykręcić ze statkami. Plus, to jest unikat. Dlatego Eustachy się doń podczepił.
    * opętana "aparycją" / "efemerydą" Diany Arłacz w najbardziej psychotycznej i ukochanej przez Eustachego wersji.


### Baza Zona Tres

* **uid:** 210714-baza-zona-tres, _numer względny_: 24
* **daty:** 0111-05-14 - 0111-05-16
* **obecni:** Arianna Verlen, BIA XXX d'Zona Tres, Elena Verlen, Eustachy Korkoran, Janus Krzak, Martyn Hiwasser, Ulisses Kalidon

Streszczenie:

Podczas eksploracji archaicznej noktiańskiej bazy Infernia doszła do tego, że to mityczna Zona Tres - odpowiednik "area 51", z badaniami nad magią. Infernia przez przypadek uruchomiła Bię. Zespół zamaskował Infernię jako Alivię Nocturnę i Arianna przekonała Bię do wstępnej współpracy. Okazało się, że Bia jest chora i nie postrzega wszystkiego prawidłowo, Klaudia jest z Bią sprzężona (oops). By dowiedzieć się więcej, Infernia wyleciała na orbitę i okazało się, że są na rogue planet - planecie która wypadła z orbity i zagubiła się w kosmosie...

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * udaje Alivię Nocturnę (pre-Nocną Kryptę) przed Bią bazy noktiańskiej "Zona Tres".


### Po drugiej stronie Bramy

* **uid:** 210707-po-drugiej-stronie-bramy, _numer względny_: 23
* **daty:** 0111-05-11 - 0111-05-13
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Janus Krzak, Klaudia Stryk, Martyn Hiwasser, Raoul Lavanis, Tal Marczak

Streszczenie:

Infernia próbując przetransportować eksperta Eteru na konferencję przez Bramę uległa katastrofie - przeniosła się GDZIEŚ INDZIEJ, pod wodę i zniknęła Klaudia. Nie uległa katastrofalnym uszkodzeniom, ale po zachęceniu pintką Lewiatana dotarła do archaicznej podwodnej noktiańskiej bazy. Tam znajduje się Klaudia, ale coś jest z nią nie tak. Sprzęgnięta z bazą? Tak czy inaczej, infiltracja bazy dopiero się zacznie. Trzeba znaleźć Klaudię i wrócić do domu.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * pozbyła się pierwszej pinasy by zrobić z niego "randkę dla Lewiatana".
    * dzięki szybkim działaniom Eustachego, tylko trochę uszkodzona. Ma słabsze sensory. Trzyma się tylko dzięki polu.


### Ixiacka wersja Malictrix

* **uid:** 200610-ixiacka-wersja-malictrix, _numer względny_: 22
* **daty:** 0111-05-01 - 0111-05-05
* **obecni:** Arianna Verlen, Klaudia Stryk, Leona Astrienko, Malictrix d'Pandora, Melwin Sito

Streszczenie:

Istotna stacja frakcji Melusit - Telira-Melusit VII - została sabotowana przez frakcję Saranta. Zrzucono na tą stację rozproszoną Malictrix. Ale doszło do transferu ixiackiego i Malictrix uzyskała częściową świadomość, planując, jak stąd uciec. Infernia jednak nie tylko wykryła obecność Malictrix, ale też przekazała stację Orbiterowi, uratowała wszystkich ludzi Melusit i do tego pozyskała ową Malictrix jako sojusznika (o czym nikt nie wie). Wyjątkowo udana operacja.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * otrzymuje specjalistyczne skanery psychotroniczne krótkiego zasięgu, pozwalające jednak Inferni na detekcję rozproszonych bytów psychotronicznych.


### Pocałunek Aspirii

* **uid:** 201210-pocalunek-aspirii, _numer względny_: 21
* **daty:** 0111-03-26 - 0111-03-29
* **obecni:** AK Nocna Krypta, AK Wyjec, Anastazja Sowińska, Arianna Verlen, Donald Parziarz, Eustachy Korkoran, Juliusz Sowiński, Katra Igneus, Klaudia Stryk, OA Zguba Tytanów, OO Infernia

Streszczenie:

Juliusz Sowiński zażądał oddania Anastazji, ale ona nie chce wracać - jej reputacja i stan są w ruinie. Anastazja chce być jak Elena, ku zgryzocie Juliusza. Tymczasem Arianna zastawiła pułapkę na koloidowy statek - "Pocałunek Aspirii" - i go zestrzeliła. Po czym wezwała Nocną Kryptę by zdjąć z nich klątwę miłości i naprawić co się da z "Pocałunku" i AK Wyjec.

Aktor w Opowieści:

* Dokonanie:
    * QShip; udawała OO Blask Aurum. Lekko uszkodzona, ale pułapkując zniszczyła Pocałunek Aspirii.
* Progresja:
    * lekko uszkodzona; ma migoczące pole grawitacyjne i lekko słabsze stabilizatory. Ogólnie, operacyjna.


### Anastazja - bohaterką?

* **uid:** 201118-anastazja-bohaterka, _numer względny_: 20
* **daty:** 0111-03-22 - 0111-03-25
* **obecni:** Anastazja Sowińska, Arianna Verlen, Diana Arłacz, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, Stefan Jamniczek

Streszczenie:

Anastazja nie została bohaterką. Wpierw Eustachy wplątał się w pojedynek o Dianę z jakimś szlachcicem - skończyło się różową mgiełką miłości i pospieszną ewakuacją Inferni z Kontrolera Pierwszego, potem epicki plan Izy z piratami rozpadł się, bo na Infernię polował jakiś statek koloidowy. Ale przetrwali - i mają zamiar to wszystko rozplątać.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * dorobiła się Upiora Kwiatu Wiśni, awatara miłości działającego w najbardziej nieodpowiednim momencie. Isuzu of Love.
    * okazało się, że jej Persefona jest niereplikowalna; to jedna z "tych" podobno wadliwych modeli.


### Noktianie rodu Arłacz

* **uid:** 201021-noktianie-rodu-arlacz, _numer względny_: 19
* **daty:** 0111-03-07 - 0111-03-10
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Diana Arłacz, Eliza Ira, Eustachy Korkoran, Izabela Zarantel, Jolanta Arłacz, Juliusz Sowiński, Klaudia Stryk, Klaus Rumak, Maria Gołąb, OO Szalony Rumak, OO Wesoły Wieprzek, Robert Arłacz, Wanessa Pyszcz

Streszczenie:

By uzyskać współpracę Elizy Iry, Arianna zdecydowała się "porwać" noktian rodu Arłacz wykorzystywanych do produkcji czystej krwi dla magów. Z uwagi na działania Marii Gołąb plan został odrzucony i wykryty, ale znalezienie haków na Arłaczy rozwiązało ten problem - Arianna i Arłacze współpracują. Nie doszło do porozumienia z Elizą Irą, ale Eliza się wycofała z Trzeciego Raju, ogłaszając się protektorką tego terenu. Sytuacja w miarę ustabilizowana.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * Diana Arłacz dołącza do załogi Inferni, ściągnięta przez Eustachego.
    * cios reputacyjny - Infernia kolaboruje z Elizą Irą, co pokazują Sekrety Orbitera. I bonus wśród sympatyków noktian.


### Arystokratka w ładowni na świnie

* **uid:** 200909-arystokratka-w-ladowni-na-swinie, _numer względny_: 18
* **daty:** 0111-02-12 - 0111-02-17
* **obecni:** Anastazja Sowińska, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, OO Galaktyczny Tucznik

Streszczenie:

Zespół wyciągnął Martyna z więzienia (legalnie). Wpakowali się na Galaktycznego Tucznika by przenieść świnie na orbitę... ale poleciała z nimi dziennikarka Izabela (o czym wiedzą) i 15letnia Anastazja Sowińska (o czym nie wiedzą). Przy lądowaniu, atak nojrepów na Tucznika spowodował katastrofę - seria Paradoksów Anastazji, uszkodzenia Tucznika itp. Zespół uratował Anastazję (choć kosztem ran Martyna) i wylądował Tucznikiem na Astorii. To jest sukces.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * Izabela Zarantel dołącza do załogi.


### Sekrety Orbitera - historia prawdziwa

* **uid:** 200819-sekrety-orbitera-historia-prawdziwa, _numer względny_: 17
* **daty:** 0111-01-24 - 0111-02-01
* **obecni:** Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Leszek Kurzmin, Martyn Hiwasser, Sabina Servatel, Tadeusz Ursus

Streszczenie:

Elena wyzwała Leonę na pojedynek. Klaudia i Arianna wkręciły Tadeusza. Tadeusz - arcymag Eterni - używa Simulacrum w walce, więc Zespół zaczął montować plan jak Leona może jednak wygrać z Simulacrum. I wszystko skończyło się tym, że Leona wygrała, Elena jest wolna od Tadeusza i powstała holodrama "Sekrety Orbitera"...

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * wzmocnione czujniki; zdecydowanie podniesiony zasięg oraz trafność.


### Mikiptur, zemsta Woltaren

* **uid:** 240124-mikiptur-zemsta-woltaren, _numer względny_: 16
* **daty:** 0110-12-29 - 0111-01-01
* **obecni:** Antoni Bladawir, Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Lars Kidironus, OA Mikiptur, OO Isratazir, Raoul Lavanis, Władawiec Diakon

Streszczenie:

Syndykat zdecydował się zaatakować Bladawira i wysłał Dewastatora przy użyciu Mikiptur jako pułapki. Skutecznie zniszczył Isratazir. Arianna śpi, więc Eustachy (nie spiesząc się) przyleciał ratować Isratazir. Infernia rozmontowała miny i wyciągnęła kogo się dało, mimo, że Mikiptur uszkodził Sarkalin. Klaudia i Arianna zrobiły co mogły, by ograniczyć polityczny fallout. Arianna ujawniła obecność Syndykatu, ratując co się da z reputacji Bladawira i jednocząc Orbiter.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * przebudzenie jej anomalii umożliwiła jej pożarcie TAI Malictrix i neutralizację psychotronicznych pułapek na Isratazir.


### Bladawir kontra przemyt tienów

* **uid:** 231220-bladawir-kontra-przemyt-tienow, _numer względny_: 15
* **daty:** 0110-12-06 - 0110-12-11
* **obecni:** Antoni Bladawir, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Ewa Razalis, Kazimierz Darbik, Klaudia Stryk, Marcinozaur Verlen, OO Infernia, OO Karsztarin, Raoul Lavanis, Szymon Orzesznik, Zaara Mieralit

Streszczenie:

Komodor Bladawir postanowił usunąć przemyt tienów z Karsztarina (najpewniej Program Kosmiczny Aurum), każąc swoim jednostkom robić wyczerpujące patrole i przeszukiwanie jednostek pod kątem owego przemytu. Tempo jest zabójcze nawet dla Arianny i Inferni - zwłaszcza, że Arianna nie wie po co to wszystko robi. Śledztwo Klaudii pokazuje, że Bladawir najpewniej umieścił potwora na Karsztarinie by zmusić przemytników do rozpaczliwego ruchu. Arianna nie może nikogo ostrzec, ale używa połączenia Krwi z Eleną - i w ten sposób dowiaduje się o roli Orzesznika. Klaudia dyskretnie przekazuje informacje Ewie Razalis. Operacja Bladawira się nie tylko nie udaje, ale trafił na dywanik i jego plan się rozsypał. Bladawir powiedział Ariannie, że ona ALBO będzie z nim współpracować albo on jej nie chce. Nadal nie wie, że to Arianna jest architektem jego porażki.

Aktor w Opowieści:

* Dokonanie:
    * pod kontrolą Bladawira, z zewnętrznymi silnikami dzięki Eustachemu, prowadzi operacje celne i szukające przemytu tienów (i innych rzeczy). Sama ma przemyt na pokładzie przez co Arianna ma OPR.


### Niemożliwe nieuczciwe ćwiczenia Bladawira

* **uid:** 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira, _numer względny_: 14
* **daty:** 0110-11-26 - 0110-11-30
* **obecni:** Antoni Bladawir, Arianna Verlen, Elena Verlen, Kamil Lyraczek, Klaudia Stryk, Lars Kidironus, Marta Keksik, OO Infernia, OO Paprykowiec, Patryk Samszar, Zaara Mieralit

Streszczenie:

Bladawir przedstawia Inferni Zaarę i informuje o ćwiczeniach, rozgrywając Zaarę przeciw Inferni. Klaudia odkryła że jednostki jakie Infernia ma eskortować mają _Pain Module_. Arianna prosząc przez Krew Elenę doprowadziła do neutralizacji tych modułów. Ćwiczenia okazują się prawie niemożliwe, ale Infernia poświęciła Paprykowiec, 'zniszczyła' jednostki w przewadze i uratowała załogę. Odwróciła ćwiczenia Bladawira przeciw niemu, przy okazji podnosząc swoją chwałę i pokazując że nie będzie grać w cudze gry.

Aktor w Opowieści:

* Dokonanie:
    * obecność Zaary na ćwiczeniach i typ ćwiczeń (i nieobecność Eustachego) sprawiły, że Infernia miała prawdziwie skomplikowaną operację i na poziomie morale i zdolności. Klaudia jako oficer taktyczny dała radę, toteż Kamil jako oficer morale. Infernia przeszła przez pole minowe, 'straciła pancerz', ale 'zestrzeliła' dwie jednostki w absolutnej przewadze.


### Ćwiczenia komodora Bladawira

* **uid:** 231115-cwiczenia-komodora-bladawira, _numer względny_: 13
* **daty:** 0110-11-08 - 0110-11-15
* **obecni:** Antoni Bladawir, Arianna Verlen, Dorota Radraszew, Ewa Razalis, Izabela Zarantel, Klaudia Stryk, Leszek Kurzmin, OO Karsztarin, OO Paprykowiec

Streszczenie:

Komodorzy Bladawir i Razalis mają historię i przy wspólnych ćwiczeniach zrobili wszystko by to drugie nie wygrało. Bladawir ściągnął Ariannę jako dywersję, ale okazało się że to był plan wewnątrz planu. Gdy Klaudia doszła do tego, że atak na Karsztarin jest niemożliwy, Arianna wkradła się w łaski Bladawira i poznała plan - porwać Ewę Razalis jej lojalnymi ludźmi. Arianna poszła za planem (prosząc Kurzmina by ostrzegł Razalis); Bladawirowi Arianna się spodobała do tego stopnia, że dodał ją do swoich sił. Też dlatego, bo Kurzmin stanął przeciw niemu a Bladawir nie wybacza.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * trafia pod kontrolę komodora Bladawira


### Śpiew NieLalki na Castigatorze

* **uid:** 231025-spiew-nielalki-na-castigatorze, _numer względny_: 12
* **daty:** 0110-10-24 - 0110-10-26
* **obecni:** Anna Tessalon, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Feliks Walrond, Igor Arłacz, Klaudia Stryk, Konstanty Keksik, Leona Astrienko, Leszek Kurzmin, Marta Keksik, OO Castigator, OO Infernia, Patryk Samszar, Raoul Lavanis, TAI Eszara d'Castigator

Streszczenie:

Na pokład Castigatora tieni przemycili NieLalkę Altarient i zaczęły mieszać się Znaczenia oraz Rzeczywistość. Zespół doprowadził do kontrolowanego opanowania i wygaszania Castigatora, po czym po zlokalizowaniu Anomalii Altarient Raoul został wysłany by ją zaizolować. Infernia ją zestrzeliła. A Arianna wykorzystała tą okazję by młodzi tieni wzięli się w garść i nie robili głupot. Szczęśliwie, Admiralicja nic nie wie, acz było nieciekawie przez moment.

Aktor w Opowieści:

* Dokonanie:
    * overcharguje generatory Memoriam, podlatuje do Castigatora i chroni mostek używając swoich Memoriam. Jej przeładowanie generatorami Memoriam jedynie bardzo pomogło. Plus, zapewnia prawdę swoimi sensorami.


### Ekstaflos na Tezifeng

* **uid:** 231011-ekstaflos-na-tezifeng, _numer względny_: 11
* **daty:** 0110-10-20 - 0110-10-22
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudiusz Terienak, Lars Kidironus, Leona Astrienko, Leszek Kurzmin, Natalia Gwozdnik, OO Castigator, OO Infernia, OO Tezifeng, Raoul Lavanis

Streszczenie:

Jeszcze na Castigatorze, Arianna i Kurzmin z przyjemnością odbudowali znajomość i wspominki. Gdy Elena chcąc ratować Leonę 'zabiła' jej anomalię, Leona wyzwała ją na pojedynek. Elena przegrała, choć było blisko; będzie musiała być służką Leony przez pewien czas, co Castigator odnotował z radością. Kurzmin poprosił Ariannę o znalezienie i pomoc dwóm tienom w rutynowym patrolu (najpewniej przekroczyli paliwo). Niestety, Tezifeng, korweta Castigatora podczas operacji celnych natrafiła na coś kralotycznego. Klaudiusz Terienak został zmieniony w ekstaflos a Natalia Gwozdnik została porwana. Pułapka na Infernię została wykryta przez Ariannę (bo Klaudiusz zachowywał się nie tak jak powinien). Szybka operacja wejścia - wydobycia jednego jeńca (marionetki kralotycznej) i zbadanie sytuacji sprawiła, że Eustachy działkami unieszkodliwił Tezifeng i Arianna wezwała wsparcie - kraloth to coś bardzo wysokiej klasy.

Aktor w Opowieści:

* Dokonanie:
    * szybka i skuteczna; przechwyciła Tezifeng i z woli Eustachego ją ostrzelała. Zero uszkodzeń, zero nieefektywności. Idealna misja dla Inferni.


### Korkoran płaci cenę za Nativis

* **uid:** 230726-korkoran-placi-cene-za-nativis, _numer względny_: 10
* **daty:** 0093-03-29 - 0093-03-30
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Izabella Saviripatel, Marcel Draglin, OO Infernia, Tobiasz Lobrak, Tymon Korkoran

Streszczenie:

Draglin zapędził Czarnymi Hełmami Tymona Korkorana do Muzeum Kidirona. Używając sił swoich i Inferni (pod rozkazami Eustachego) uratował piratów pod mentalnym wpływem. Negocjacje Lobrak - Eustachy pokazały Eustachemu, że nie są w stanie pokonać Syndykatu. Ardilla nie pozwala mu dołączyć Arkologii do Syndykatu - to nie to co powinno być. W chwili, w której Eustachy skonsolidował pełnię sił i mocy Tymon eksploduje wysadzając siebie i Wujka. Eustachy zostaje bez swojego sumienia, sam z Ardillą, Draglinem, Kalią i uszkodzoną Arkologią. Naprzeciw Syndykatu i Lobrakowi, który chce sojuszu.

Aktor w Opowieści:

* Dokonanie:
    * nadal kusi Eustachego; tym razem pokazuje mu Izabellę na kolanach przed nim, zrekonstruowaną mocą Nihilusa w posłuszną agentkę Eustachego


### Wojna o Arkologię Nativis - nowa regentka

* **uid:** 230719-wojna-o-arkologie-nativis-nowa-regentka, _numer względny_: 9
* **daty:** 0093-03-28 - 0093-03-29
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Izabella Saviripatel, Kalia Awiter, Marcel Draglin, OO Infernia, Ralf Tapszecz, Tobiasz Lobrak

Streszczenie:

Ardilla ma plan przejęcia kontroli nad Arkologią, w tle Kidiron przez nią kontrolowany. To też neutralizuje główne ataki L&T, że Kidiron taki zły. Z pomocą Kalii (ewakuowanej ze skrzydła medycznego) zbudowała linię propagandową i po odzyskaniu Radiowęzła, Kalia nadała wiadomość pokoju i pojednania. Eustachy zmiażdżył główne siły L&T koło Engineering, Kalia JAKIMŚ CUDEM została regentką przez przypadek a czarodziejka Syndykatu, Izabella została zmiażdżona w imię Nihilusa przez Ralfa chroniącego Ardillę.

Aktor w Opowieści:

* Dokonanie:
    * najbezpieczniejsze miejsce w okolicach Arkologii Nativis. Będące anomalią Nihilusa kuszącą Eustachego do zdjęcia kolejnych generatorów memoriam.


### Infiltrator ucieka a Arkologia płonie

* **uid:** 230621-infiltrator-ucieka-a-arkologia-plonie, _numer względny_: 8
* **daty:** 0093-03-25 - 0093-03-26
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, BIA Prometeus, Dalmjer Servart, Eustachy Korkoran, Kalia Awiter, Laurencjusz Kidiron, Małgorzata Maratelus, OO Infernia, Rafał Kidiron, Ralf Tapszecz

Streszczenie:

Ardilla utrzymuje Kidirona przy życiu, negocjując dla niego bezpieczne miejsce w Szczurowisku i stawiając swoją i Ralfową reputację na szali. Potem - rozpaczliwie ratuje Prometeusa, acz z ciężkimi stratami. Eustachy w pogoni za niezwykle groźnym Infiltratorem musiał dać mu odejść; oddał Skorpiona i czterech załogantów, ale uratował Kalię Awiter. Tymczasem o Arkologię Nativis toczy się wojna dusz - Bartłomiej Korkoran kontra Laurencjusz Kidiron. A w tle eksperymenty Kidirona (jak np. farighanowie jako Hełmy) wyrywają się spod kontroli i zdecydowanie nie pomagają.

Aktor w Opowieści:

* Dokonanie:
    * proponuje Eustachemu serię planów które mogą zabić Kalię. Infernia nie dba o Kalię. Chce Eustachego od niej odepchnąć.


### Atak na Kidirona

* **uid:** 230614-atak-na-kidirona, _numer względny_: 7
* **daty:** 0093-03-22 - 0093-03-24
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Dalmjer Servart, Eustachy Korkoran, Kalia Awiter, Lycoris Kidiron, OO Infernia, Rafał Kidiron, Ralf Tapszecz, SAN Szare Ostrze, Stanisław Uczantor

Streszczenie:

Gdy Wujek się obudził, powiedział że z Infernią mogą być ostre problemy jeśli spada poniżej poziomu Memoriam. Zmartwił się hipotezą Ardilli że Ralf może być magiem Nihilusa, ale Ardilla walczyła o dalsze spotykanie się z Ralfem. Zamach na Kidirona. Większość jego sztabu umiera, ale sam Kidiron przeżywa, acz ciężko ranny; ratuje go Ardilla i pozyskuje info o jego mrocznych planach. Kalia bierze na siebie morale i propagandę, ale przez to wpada na celownik Infiltratora który ją porywa. Eustachy próbuje odbić Kalię z rąk Infiltratora w koloidzie...

Aktor w Opowieści:

* Dokonanie:
    * conduit dla magii Eustachego; wzmocniona przez entropię. Okazuje się, że ma już tylko 40% aktywnych generatorów memoriam i że PRZEKSZTAŁCIŁA kiedyś jednego maga, którego Bartłomiej Korkoran musiał zabić.


### Zdrada rozrywająca arkologię

* **uid:** 230329-zdrada-rozrywajaca-arkologie, _numer względny_: 6
* **daty:** 0093-03-14 - 0093-03-16
* **obecni:** Amelia Sarkaldir, Ardilla Korkoran, BIA Prometeus, Celina Lertys, Eustachy Korkoran, Ewelina Paroknis, Feliks Kidiron, Franciszek Pietraszczyk, Iwona Paroknis, Jonasz Paroknis, Kalia Awiter, Katarzyna Falernik, Małgorzata Maratelus, OO Infernia, Rafał Kidiron, Ralf Tapszecz, Szczepan Falernik, Wojciech Grzebawron

Streszczenie:

Nastolatka miała pamiętnik czytany przez macochę. Ojciec to ignorował. Nastolatka napisała w pamiętniku, że ojciec zdradza macochę z ekspertką hydrologii. Zrobił się shitstorm który doprowadził do poważnych problemów - m.in. macocha (dyrektor skrzydła medycznego) zaczęła robić błędy co wykryła Celina. Kidiron podejrzewa spisek - ktoś próbuje skłócić ważne rodziny, a także shackować Prometeusa (BIA arkologii). Rozmowa z ich eks-piratami kończy się tym, że to nie oni - za tym stoi wyspecjalizowana siła z innej arkologii lub noktianie. 

Ślady prowadzą do pamiętnika nastolatki, który shackowany przez ekspertkę Inferni (i upubliczniony przez problemy na linii Infernia - Prometeus) pokazują, że najpewniej nastolatka za wszystkim stoi sama. Uratowana przez tajemniczego maga Interis przed samobójstwem nastolatka przyznała na czym polegał problem i ujawnia, że pomaga jej Feliks Kidiron, najmłodszy z rodu i jej jedyny przyjaciel. Czyli to nie spisek. Ale jak to teraz naprawić?

Aktor w Opowieści:

* Dokonanie:
    * szybko i sprawnie przechwyciła Skorpiona należącego do Szczepana zanim ten zrobił coś głupiego. Wyciągnęła Skorpiona z dołu "na hol", z lekkimi tylko uszkodzeniami owego Skorpiona.


### Bardzo nieudane porwanie Inferni

* **uid:** 230315-bardzo-nieudane-porwanie-inferni, _numer względny_: 5
* **daty:** 0093-03-06 - 0093-03-09
* **obecni:** Ardilla Korkoran, Eustachy Korkoran, Hubert Grzebawron, Mariusz Dobrowąs, Nadia Sekernik, OO Infernia, Rafał Kidiron, Ralf Tapszecz, SAN Szare Ostrze, Wojciech Grzebawron

Streszczenie:

Mimo braku wujka, Infernia działa sprawnie. Wujek jest pod opieką medyczną, ale Celina nie ufając Kidironowi, chce być z nim cały czas. Infernia otrzymuje wezwanie od CES Mineralis, gdyż stacja została zaatakowana przez Trianai. Eustachy wysyła Czarne Hełmy i komandosów z Inferni, którzy wspólnie działają, ale Trianai jest za dużo. Eustachy wabi więc Trianai za pomocą energii magicznej, tracąc przytomność po połączeniu z koordynatorem Trianai (magiem), zabijając go. Stacja CES ucierpiała w Paradoksie Eustachego, powodując wiele ofiar. Ardilla koordynuje operację ratunkową, ale gdy okazuje się, że część załogi z CES atakuje mostek próbując ukraść statek, Ardilla wyłącza Infernię i z Ralfem wieją do kanałów Inferni.

Eustachy budzi się w niewoli, przekonuje oprychów, że jedynie on potrafi sterować Infernią (ucierpiał przy tym nieco). Ardilla i Ralf - na prośbę Eustachego - sabotują generatory Memoriam, pozwalając Inferni uwolnić się spod kontroli wrogów. Wywiązuje się strzelanina, w wyniku której napastnicy zostają pokonani - Eustachy kontroluje sytuację. Eustachy decyduje, że ich karą będzie służba na Inferni, ale jeden z nich odchodzi ostrzegać przed atakiem na Infernię. To wszystkim pasuje.

Ekipa wraca na statek, gdzie Kidiron proponuje przesłuchanie pojmanych piratów, obiecując immunitet w zamian za współpracę. Eustachy zgadza się, ale tylko z ochotnikami i Ardillą jako gwarantem bezpieczeństwa. Piraci zgadzają się współpracować mimo użycia neuroobroży przy przesłuchaniu przez Kidirona. Gdy Zespół próbuje montować nowe generatory Memoriam - nie da się. Nie mogą być zamontowane, gdyż się palą. Infernia wykrywa próbę montażu nowych, ale nie zauważa starych.

Aktor w Opowieści:

* Dokonanie:
    * gdy Ardilla uszkodziła kolejne generatory Memoriam, pożarła część napastników na swoim pokładzie. Do Inferni dołączyła część piratów z woli Eustachego. Infernia będzie o nich dbać...
* Progresja:
    * opinia demonicznego, super niebezpiecznego statku u piratów, nomadów i nieArkologicznych sił Neikatis dzięki Hubertowi.


### Pierwsza randka Eustachego

* **uid:** 230208-pierwsza-randka-eustachego, _numer względny_: 4
* **daty:** 0093-02-14 - 0093-02-21
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Franciszek Pietraszczyk, Kalia Awiter, Marcin Pietraszczyk, OO Infernia, Rafał Kidiron, Ralf Tapszecz

Streszczenie:

Wujek opieprza Eustachego za krzywdę niewinnych noktian, ale Kidiron się za Eustachym wstawia. Ardilla socjalizuje Ralfa i ratuje młodego Marcinka który się zaklinował w rurze Szczurowiska; okazuje się, że wszyscy kojarzą Infernię jako symbol nadziei. Potem Eustachy idzie (z przymusu) na randkę z Kalią; okazuje się, że Kalia zmanipulowała by Eustachy wygrał bo chce dać mu jeden piękny dzień. Wszystkie dowody podsłuchała i złapała Ardilla. Gdy Eustachy poczuł chore emanacje magiczne z Ambasadorki, porzucił Kalię i pobiegł do Inferni.

Aktor w Opowieści:

* Dokonanie:
    * okazuje się, że za rządów Bartłomieja Korkorana stała się symbolem nadziei dla Nativis. Ludzie rozpoznają ją jako pozytywną jednostkę która przyniesie dobro.


### Wyłączone generatory Memoriam Inferni

* **uid:** 230201-wylaczone-generatory-memoriam-inferni, _numer względny_: 3
* **daty:** 0093-02-10 - 0093-02-12
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Ernest Puszczowiec, Eustachy Korkoran, Jan Lertys, Kalia Awiter, Kalista Luminis, Kratos Coruscatis, OO Infernia, Rafał Kidiron, Ralf Tapszecz, Tymon Korkoran

Streszczenie:

Eustachy przebudzony przez Infernię po burzy piaskowej poszedł za jej podszeptami i wyłączył generatory Memoriam. Infernia go Dotknęła - jest to anomalia Interis uwięziona w mechanicznej skorupie. Tylko Eustachy może Infernią sterować. Więc gdy pojawiła się misja pomocy bazie noktiańskiej Coruscatis, Eustachy musiał lecieć. Kidiron chciał aresztowania Kallisty Luminis a Wujek chciał pomocy noktianom. Okazało się, że na miejscu sformował się amalgamoid trianai i Infernia została ostrzelana; acz odpowiedziała ogniem. Zespół pomógł i ewakuował noktian i zdestabilizował reaktor fuzyjny by wysadzić wszystko. Pod wpływem Ardilli i Celiny Eustachy nie oddał noktian Kidironowi a wysadził ich w bezpiecznym miejscu. Kolejny dowód, że arkologia Nativis wykorzystuje żywność jako broń, co jest niezgodne z wszelkimi konwencjami.

Aktor w Opowieści:

* Dokonanie:
    * w rzeczywistości pancerz okrywający straszliwą anomalię Interis, która jest uśpiona generatorami Memoriam. Eustachy się z nią sprzęgł; Infernia weszła w jego głowę. Nie dba o nic, pragnie siać zniszczenie. Wreszcie wolna i tylko Eustachy może nią sterować (ostatnie ogniwo do uwolnienia tego co kryje Infernia). Dominuje nawet Persefonę.
* Progresja:
    * wreszcie WOLNA od generatorów Memoriam, przebudzona i świadoma! Jedyne, co ją ogranicza to żywy Eustachy, ale ta anomalia Interis może poczekać.
    * jedynie Eustachy Korkoran jest w stanie ją pilotować.


### Infernia taksówką dla Lycoris

* **uid:** 220720-infernia-taksowka-dla-lycoris, _numer względny_: 2
* **daty:** 0093-01-20 - 0093-01-22
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Czesław Żuczek, Daria Raizis, Eustachy Korkoran, Jan Lertys, Kamil Wraczok, Lycoris Kidiron, OO Infernia, Rafał Kidiron, Wiktor Turkalis

Streszczenie:

Plaga Trianai uderzyła w CES Purdont; Infernia p.d. Bartłomieja Korkorana ruszyła jako taksówka dla Lycoris ale młodzi trafili do Purdont a Infernia szuka Lycoris i Wiktora. Gdy młodzi doszli do Plagi i przebili się do łączności to poinformowali Nativis i wujka o ryzyku. Dostali skonfliktowane polecenia, ale mają przetrwać. Jednak dla części z nich to nieakceptowalne, bo nie zostawią zarażonej koleżanki na pożarcie Pladze Trianai mimo znajdującego się gdzieś w cieniu Inteligentnego Koordynatora Trianai...

Aktor w Opowieści:

* Dokonanie:
    * główny okręt obronny Arkologii Nativis; zdolny do działania w burzach piaskowych i praktycznie dowolnych plugawych warunkach. Pod dowodzeniem Bartłomieja Korkorana. Tylko Bartłomiej zna sekrety Inferni - i Emilia d'Erozja.


### Ona chce dziecko Eustachego

* **uid:** 221006-ona-chce-dziecko-eustachego, _numer względny_: 1
* **daty:** 0092-09-20 - 0092-09-24
* **obecni:** Ardilla Korkoran, Ava Kieras, Emban Dolamor, Eustachy Korkoran, Lerten Kieras, Maks Selert, Michał Kervendal, OO Infernia, Staszek Zakraton, VN Exerinn

Streszczenie:

Rift - wujek vs Kidironowie w sprawie Robaków i Dziewczynki. Eustachy rozdarty, acz staje za wujkiem. Z woli Kidironów Eustachy wziął Infernię i poleciał do crawlera o nazwie Exerinn by odzyskać Robaki które uciekły. Tam Eustachy poznał jak żyją Sarderyci z Exerinna i jak radzą sobie z trudnościami (m.in. Farighanami - nekroborgami Neikatis). Po tym jak Infernia pomogła Exerinnowi i Eustachy oddał geny Avie, Robaki wróciły z Exerinna na Infernię i Eustachy ukrył ich życie przed Kidironami - fałszywka, że stali się Farighanami.

Aktor w Opowieści:

* Dokonanie:
    * zintegrowała się z Eustachym po raz pierwszy; obcy umysł Inferni Dotknął Eustachego i uznał go godnym. Zestrzeliła dla Eustachego kilka Farighanów.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 27, @: 0112-10-03
    1. Primus    : 27, @: 0112-10-03
        1. Sektor Astoriański    : 26, @: 0112-10-03
            1. Astoria, Orbita    : 5, @: 0112-04-24
                1. Kontroler Pierwszy    : 2, @: 0112-04-24
            1. Astoria, Pierścień Zewnętrzny    : 5, @: 0112-04-29
                1. Poligon Stoczni Neotik    : 2, @: 0112-04-26
                1. Stocznia Neotik    : 3, @: 0112-04-29
                    1. Zewnętrzny dok ixioński    : 1, @: 0112-04-29
            1. Brama Kariańska    : 2, @: 0112-03-16
            1. Neikatis    : 11, @: 0111-03-29
                1. Dystrykt Glairen    : 10, @: 0093-03-30
                    1. Arkologia Nativis    : 9, @: 0093-03-30
                        1. Poziom 1 - Dolny    : 4, @: 0093-03-26
                            1. Północ - Stara Arkologia    : 4, @: 0093-03-26
                                1. Blokhaus F    : 3, @: 0093-03-26
                                    1. Szczurowisko    : 2, @: 0093-03-26
                                1. Stare Wejście Północne    : 1, @: 0093-03-26
                                1. Szczurowisko    : 1, @: 0093-02-21
                            1. Zachód    : 1, @: 0093-03-26
                                1. Centrala Prometeusa    : 1, @: 0093-03-26
                        1. Poziom 2 - Niższy Środkowy    : 2, @: 0093-03-30
                            1. Południe    : 1, @: 0093-03-29
                                1. Engineering    : 1, @: 0093-03-29
                            1. Wschód    : 1, @: 0093-03-30
                                1. Centrum Kultury i Rozrywki    : 1, @: 0093-03-30
                                    1. Muzeum Kidirona    : 1, @: 0093-03-30
                        1. Poziom 3 - Górny Środkowy    : 4, @: 0093-03-29
                            1. Południe    : 1, @: 0093-03-29
                                1. Medical    : 1, @: 0093-03-29
                            1. Wschód    : 3, @: 0093-03-24
                                1. Dzielnica Luksusu    : 3, @: 0093-03-24
                                    1. Ambasadorka Ukojenia    : 1, @: 0093-02-21
                                    1. Ogrody Wiecznej Zieleni    : 3, @: 0093-03-24
                                    1. Stacja holosymulacji    : 1, @: 0093-02-21
                        1. Poziom 4 - Górny    : 1, @: 0093-03-29
                            1. Wschód    : 1, @: 0093-03-29
                                1. Radiowęzeł    : 1, @: 0093-03-29
                    1. Arkologia Sarviel, okolice    : 1, @: 0093-02-12
                        1. Krater Coruscatis    : 1, @: 0093-02-12
                    1. CES Mineralis    : 1, @: 0093-03-09
                    1. CES Purdont, okolice    : 1, @: 0093-01-22
                        1. Wiertło Ekopoezy Delta    : 1, @: 0093-01-22
                    1. CES Purdont    : 1, @: 0093-01-22
                        1. Kafeteria    : 1, @: 0093-01-22
                        1. Laboratorium Ekopoezy    : 1, @: 0093-01-22
                        1. Life Support    : 1, @: 0093-01-22
                        1. System łączności    : 1, @: 0093-01-22
                    1. Ogród Zwłok Exerinna    : 1, @: 0092-09-24
                1. Dystrykt Quintal    : 1, @: 0111-03-29
                    1. Arkologia Aspiria    : 1, @: 0111-03-29
            1. Libracja Lirańska    : 1, @: 0112-10-03
                1. Anomalia Kolapsu, orbita    : 1, @: 0112-10-03
            1. Pas Teliriański    : 1, @: 0111-10-01
                1. Planetoidy Kazimierza    : 1, @: 0111-10-01
                    1. Planetoida Asimear    : 1, @: 0111-10-01
        1. Sektor Mevilig    : 2, @: 0112-03-26
            1. Chmura Piranii    : 1, @: 0112-03-16
            1. Planetoida Kalarfam    : 1, @: 0112-03-26
        1. Sektor Noviter    : 1, @: 0112-03-16

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eustachy Korkoran    | 26 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210825-uszkodzona-brama-eteryczna; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 220622-lewiatan-za-pandore; 220720-infernia-taksowka-dla-lycoris; 221006-ona-chce-dziecko-eustachego; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231220-bladawir-kontra-przemyt-tienow)) |
| Arianna Verlen       | 17 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210825-uszkodzona-brama-eteryczna; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 220622-lewiatan-za-pandore; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Elena Verlen         | 15 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210825-uszkodzona-brama-eteryczna; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Klaudia Stryk        | 15 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210825-uszkodzona-brama-eteryczna; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 220622-lewiatan-za-pandore; 231025-spiew-nielalki-na-castigatorze; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Ardilla Korkoran     | 10 | ((220720-infernia-taksowka-dla-lycoris; 221006-ona-chce-dziecko-eustachego; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Bartłomiej Korkoran  | 7 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Leona Astrienko      | 7 | ((210218-infernia-jako-goldarion; 210929-grupa-ekspedycyjna-kellert; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Martyn Hiwasser      | 7 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Rafał Kidiron        | 7 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Ralf Tapszecz        | 7 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Kalia Awiter         | 6 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Raoul Lavanis        | 4 | ((220622-lewiatan-za-pandore; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze; 231220-bladawir-kontra-przemyt-tienow)) |
| Adam Szarjan         | 3 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy)) |
| AK Nocna Krypta      | 3 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji; 210825-uszkodzona-brama-eteryczna)) |
| Celina Lertys        | 3 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Diana d'Infernia     | 3 | ((210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy)) |
| Kamil Lyraczek       | 3 | ((211020-kurczakownia; 211215-sklejanie-inferni-do-kupy; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Maria Naavas         | 3 | ((211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy; 220622-lewiatan-za-pandore)) |
| Roland Sowiński      | 3 | ((210512-ewakuacja-z-serenit; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| AK Serenit           | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Anastazja Sowińska Dwa | 2 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia)) |
| Antoni Bladawir      | 2 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Antoni Kramer        | 2 | ((210218-infernia-jako-goldarion; 210929-grupa-ekspedycyjna-kellert)) |
| BIA Prometeus        | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Dalmjer Servart      | 2 | ((230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Flawia Blakenbauer   | 2 | ((210825-uszkodzona-brama-eteryczna; 211020-kurczakownia)) |
| Franciszek Pietraszczyk | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| Izabella Saviripatel | 2 | ((230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Jan Lertys           | 2 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni)) |
| Lars Kidironus       | 2 | ((231011-ekstaflos-na-tezifeng; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Leszek Kurzmin       | 2 | ((231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Lycoris Kidiron      | 2 | ((220720-infernia-taksowka-dla-lycoris; 230614-atak-na-kidirona)) |
| Małgorzata Maratelus | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Marcel Draglin       | 2 | ((230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Marta Keksik         | 2 | ((231025-spiew-nielalki-na-castigatorze; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| OA Zguba Tytanów     | 2 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji)) |
| OE Falołamacz        | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| OO Castigator        | 2 | ((231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Otto Azgorn          | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Patryk Samszar       | 2 | ((231025-spiew-nielalki-na-castigatorze; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| SAN Szare Ostrze     | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230614-atak-na-kidirona)) |
| Tobiasz Lobrak       | 2 | ((230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Tymon Korkoran       | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230726-korkoran-placi-cene-za-nativis)) |
| Vigilus Mevilig      | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Wawrzyn Rewemis      | 2 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| Wojciech Grzebawron  | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Zaara Mieralit       | 2 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Aida Serenit         | 1 | ((210512-ewakuacja-z-serenit)) |
| AK Rodivas           | 1 | ((201230-pulapka-z-anastazji)) |
| AK Wyjec             | 1 | ((201210-pocalunek-aspirii)) |
| Aleksandra Termia    | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Anastazja Sowińska   | 1 | ((201210-pocalunek-aspirii)) |
| Anna Tessalon        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Ava Kieras           | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Bogdan Anatael       | 1 | ((210512-ewakuacja-z-serenit)) |
| Czesław Żuczek       | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Damian Orion         | 1 | ((201230-pulapka-z-anastazji)) |
| Daria Raizis         | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Dariusz Krantak      | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Diana Arłacz         | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Donald Parziarz      | 1 | ((201210-pocalunek-aspirii)) |
| Emban Dolamor        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Ewa Razalis          | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Walrond       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Gilbert Bloch        | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Henryk Sowiński      | 1 | ((201230-pulapka-z-anastazji)) |
| Hestia d'Neotik      | 1 | ((211208-o-krok-za-daleko)) |
| Hubert Grzebawron    | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Igor Arłacz          | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Izabela Zarantel     | 1 | ((211124-prototypowa-nereida-natalii)) |
| Jarosław Szarjan     | 1 | ((211208-o-krok-za-daleko)) |
| Jolanta Sowińska     | 1 | ((210218-infernia-jako-goldarion)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Juliusz Sowiński     | 1 | ((201210-pocalunek-aspirii)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kamil Wraczok        | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Kasandra Destrukcja Diakon | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Katra Igneus         | 1 | ((201210-pocalunek-aspirii)) |
| Kazimierz Darbik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Klaudiusz Terienak   | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Konstanty Keksik     | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Laurencjusz Kidiron  | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Lerten Kieras        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Maks Selert          | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Marcin Pietraszczyk  | 1 | ((230208-pierwsza-randka-eustachego)) |
| Marcinozaur Verlen   | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Mariusz Dobrowąs     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Medea Sowińska       | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Michał Kervendal     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Nadia Sekernik       | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Natalia Aradin       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Natalia Gwozdnik     | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OA Odkupienie        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OE Piękna Elena      | 1 | ((210428-infekcja-serenit)) |
| Ola Szerszeń         | 1 | ((220622-lewiatan-za-pandore)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Karsztarin        | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Pandora           | 1 | ((220622-lewiatan-za-pandore)) |
| OO Paprykowiec       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| OO Straszliwy Pająk  | 1 | ((220622-lewiatan-za-pandore)) |
| OO Tezifeng          | 1 | ((231011-ekstaflos-na-tezifeng)) |
| OO Tivr              | 1 | ((220622-lewiatan-za-pandore)) |
| Persefona d'Infernia | 1 | ((210428-infekcja-serenit)) |
| Rafał Grambucz       | 1 | ((210428-infekcja-serenit)) |
| Rufus Niegnat        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Rzeźnik Parszywiec Diakon | 1 | ((220622-lewiatan-za-pandore)) |
| SP Plugawy Jaszczur  | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Stanisław Uczantor   | 1 | ((230614-atak-na-kidirona)) |
| Staszek Zakraton     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Szymon Orzesznik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Tadeusz Ursus        | 1 | ((210428-infekcja-serenit)) |
| TAI Eszara d'Castigator | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Tomasz Sowiński      | 1 | ((210218-infernia-jako-goldarion)) |
| VN Exerinn           | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Wiktor Turkalis      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |