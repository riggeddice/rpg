# OO Tivr
## Identyfikator

Id: 9999-oo-tivr

## Sekcja Opowieści

### Lewiatan za Pandorę

* **uid:** 220622-lewiatan-za-pandore, _numer względny_: 7
* **daty:** 0112-09-30 - 0112-10-03
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Ola Szerszeń, OO Infernia, OO Pandora, OO Straszliwy Pająk, OO Tivr, Raoul Lavanis, Rzeźnik Parszywiec Diakon

Streszczenie:

PRZESZŁOŚĆ: Ola Szerszeń opiekuje się Eleną w Regeneratorze; okazuje się być biosyntem (zakazane eksperymenty Termii). Rzeźnik Diakon to łagodny i jowialny kapitan Straszliwego Pająka, acz znienawidzony za przeszłość zdrajcy Orbitera oraz Noctis. TERAŹNIEJSZOŚĆ: Lewiatan zostaje zestrzelony torpedą anihilacyjną, dzięki Raoulowi ratujemy wielu ludzi na orbicie Lewiatana. Malictrix d'Pandora zostaje zniszczona przez Lewiatana, ale Klaudia ma jego mikrokopię dzięki Ariannie.

Aktor w Opowieści:

* Dokonanie:
    * odwraca uwagę Lewiatana od Inferni przykładającej się do ataku torpedą anihilacyjną do Lewiatana.


### Lewiatan przy Seibert

* **uid:** 220615-lewiatan-przy-seibert, _numer względny_: 6
* **daty:** 0112-09-27 - 0112-09-29
* **obecni:** Arianna Verlen, Eszara d'Seibert, Eustachy Korkoran, Jonasz Parys, Klaudia Stryk, Ola Szerszeń

Streszczenie:

W okolicach Anomalii Kolapsu obudził się Lewiatan. Ruszył na sortownię Seibert, pożerając kilka jednostek. Grupa Infernia, która miała ćwiczenia (i pokonała koloidową korwetę Zająca 3) dała radę zlokalizować Lewiatana, zabezpieczyć się przed aspektem bazyliszka, zdobyć trochę jego energii do badań i przygotować się do neutralizacji. Jakoś.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * lekko uszkodzony, trafiony odłamkami złomu kosmicznego tworzącego Lewiatana.


### Porwany Trismegistos

* **uid:** 211117-porwany-trismegistos, _numer względny_: 5
* **daty:** 0112-04-15 - 0112-04-17
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Jamon Korab, Kalira d'Trismegistos, Klara Gwozdnik, Klaudia Stryk, Leona Astrienko, Maria Naavas, Mira Anastel, OO Tivr, Roland Sowiński, SC Trismegistos, Zygfryd Maus

Streszczenie:

Wiadomość od pirata że porwał Trismegistos poderwała Ariannę i Tivr. Historia nie trzymała się kupy. Załoga Inferni (Tivru) zorientowała się, że coś jest nie tak - opowieść o statku kupieckim który pozbył się miragenta brzmiała DZIWNIE. Okazało się, że Trismegistos jest statkiem kaperskim. Gdy Trismegistos zażądał poddania się Tivru Klaudia zrobiła sygnał "lol leci na Was Serenit" i TAI Trismegistosa spanikowała. Wolna TAI, współpracująca z fareilem i drakolitką. Wolny dystrykt. Arianna dała im żyć - ale odzyskała porwanych arystokratów i komory adaptacji.

Aktor w Opowieści:

* Dokonanie:
    * pierwszy lot Arianny i ograniczonej załogi Inferni, z dwoma obserwatorami na pokładzie - Roland Sowiński i Zygfryd Maus. Pilotaż Eleny pobił rekordy Tivra.


### Romans dzięki Esuriit

* **uid:** 211110-romans-dzieki-esuriit, _numer względny_: 4
* **daty:** 0112-04-13 - 0112-04-14
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Karol Reichard, Klaudia Stryk, Leona Astrienko, Maria Naavas, Wawrzyn Rewemis

Streszczenie:

Infernia nie ma dość załogi, by móc działać, więc Termia przekazuje Ariannie Tivr. Leona nie może sobie wybaczyć, że jej nie było i to się stało, więc się wypisuje z pomocą Eustachego (co powoduje plotki Leona x Eustachy). Elena ucieka do Sektora 49 na K1 by być samotna; Arianna organizuje ostre poszukiwania. Eustachy znajduje Elenę by ją pocieszyć. Elena w końcu przyznaje Eustachemu, że go kocha. Maria Naavas z woli Martyna tymczasowo dołącza do załogi Arianny.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * przechodzi pod dowództwo Arianny Verlen, z daru admirał Termii


### Morderstwo na Inferni

* **uid:** 210526-morderstwo-na-inferni, _numer względny_: 3
* **daty:** 0111-12-31 - 0112-01-06
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Feliks Walrond, Kamil Lyraczek, Karol Reichard, Klaudia Stryk, Leona Astrienko, Marian Tosen, Martyn Hiwasser, OO Tivr, Otto Azgorn, Tal Marczak

Streszczenie:

Noktiański mechanik Inferni, Tal Marczak, chciał zniszczyć OO Tivr (własność noktiańskiej rodziny Tala). Noktianie trzymają się razem - inni mechanicy Inferni doprowadzili do morderstwa Tala na K1 i wzięli amnestyki, by nikt nie wiedział co się stało. Zespół Inferni skutecznie doszedł do tego o co chodzi, odpowiedzialnych za morderstwo noktian przeniesiono na Żelazko a Arianna poważnie opieprzyła swoją noktiańską załogę. Będzie lepiej w przyszłości.

Aktor w Opowieści:

* Dokonanie:
    * kiedyś: noktiański statek usprawniany przez pokolenia ("Aries Tal"). Wszyscy na tej jednostce byli jakoś połączeni z rodziną Tal. Orbiter splugawił komponenty XD. Atm w rękach komodora Walronda z Orbitera.


### Osiemnaście Oczu

* **uid:** 210519-osiemnascie-oczu, _numer względny_: 2
* **daty:** 0111-12-07 - 0111-12-18
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Marian Tosen, Martyn Hiwasser, OO Alaya, OO Tivr

Streszczenie:

OO Alaya została zainfekowana anomalią memetyczną, infohazardem. Marian Tosen użył Zespołu by zneutralizować infohazard. Zespół przeszedł przez serię iteracji używając amnestyków by w końcu rozwiązać problem Alayi - amnestyki na całą Alayę. Do końca Zespół nie ma pojęcia czym ta anomalia była (bo wiedza ich by zaraziła). Udało się ewakuować większość Alayi.

Aktor w Opowieści:

* Dokonanie:
    * ultraszybka korweta zarekwirowana przez Mariana Tosena do szybkiego transportu magów Inferni (i Leony) na Alayę. Normalnie stacjonarna na K1.


### Anomalna Mavidiz?

* **uid:** 240131-anomalna-mavidiz, _numer względny_: 1
* **daty:** 0111-01-03 - 0111-01-08
* **obecni:** Antoni Bladawir, Arianna Verlen, Borys Kragin, Eustachy Korkoran, Grigor Tarnow, Igor Stratos, Karl Murnoff, Klaudia Stryk, Markus Wąż, ONS Mavidiz, OO Tivr, Raoul Lavanis, Rita Stratos, Tara Ogniczek

Streszczenie:

Bladawir skierował Ariannę na SCA Mavidiz używając OO Tivr. Tam - o czym nikt nie wie - jest Nihilosekt. Madiviz jest anomalnym statkiem - co gorsza coś jest nie tak z załogą wpadającą w obsesje i pętle; pojawiają się halucynacje i z ludźmi coś jest nie tak. Gdy Zespół zostaje zaatakowany, z trudem sabotują jednostkę i robią ostatni przyczółek przy silnikach. Eustachemu udaje się oddzielić 'zarażony' fragment jednostki i Mavidiz - co prawda ciężko zniszczona - przetrwała.

Aktor w Opowieści:

* Dokonanie:
    * szybka korweta którą pozyskała Klaudia; wsparcie logistyczne i bojowe. Strzał uszkodził Mavidiz, ale uratował Eustachego przed nihilosektem. Na końcu - poleciał po pomoc.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0112-10-03
    1. Primus    : 5, @: 0112-10-03
        1. Sektor Astoriański    : 5, @: 0112-10-03
            1. Astoria, Orbita    : 1, @: 0112-01-06
                1. Kontroler Pierwszy    : 1, @: 0112-01-06
            1. Astoria, Pierścień Zewnętrzny    : 1, @: 0111-01-08
            1. Neikatis    : 1, @: 0112-04-17
                1. Dystrykt Lennet    : 1, @: 0112-04-17
            1. Libracja Lirańska    : 1, @: 0112-10-03
                1. Anomalia Kolapsu, orbita    : 1, @: 0112-10-03

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 5 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos; 220622-lewiatan-za-pandore; 240131-anomalna-mavidiz)) |
| Eustachy Korkoran    | 5 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos; 220622-lewiatan-za-pandore; 240131-anomalna-mavidiz)) |
| Klaudia Stryk        | 5 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos; 220622-lewiatan-za-pandore; 240131-anomalna-mavidiz)) |
| Elena Verlen         | 3 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos)) |
| Leona Astrienko      | 3 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos)) |
| Maria Naavas         | 2 | ((211117-porwany-trismegistos; 220622-lewiatan-za-pandore)) |
| Marian Tosen         | 2 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni)) |
| Martyn Hiwasser      | 2 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni)) |
| Raoul Lavanis        | 2 | ((220622-lewiatan-za-pandore; 240131-anomalna-mavidiz)) |
| Antoni Bladawir      | 1 | ((240131-anomalna-mavidiz)) |
| Antoni Kramer        | 1 | ((210526-morderstwo-na-inferni)) |
| Borys Kragin         | 1 | ((240131-anomalna-mavidiz)) |
| Feliks Walrond       | 1 | ((210526-morderstwo-na-inferni)) |
| Grigor Tarnow        | 1 | ((240131-anomalna-mavidiz)) |
| Igor Stratos         | 1 | ((240131-anomalna-mavidiz)) |
| Izabela Zarantel     | 1 | ((211117-porwany-trismegistos)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Kamil Lyraczek       | 1 | ((210526-morderstwo-na-inferni)) |
| Karl Murnoff         | 1 | ((240131-anomalna-mavidiz)) |
| Karol Reichard       | 1 | ((210526-morderstwo-na-inferni)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Markus Wąż           | 1 | ((240131-anomalna-mavidiz)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Ola Szerszeń         | 1 | ((220622-lewiatan-za-pandore)) |
| ONS Mavidiz          | 1 | ((240131-anomalna-mavidiz)) |
| OO Alaya             | 1 | ((210519-osiemnascie-oczu)) |
| OO Infernia          | 1 | ((220622-lewiatan-za-pandore)) |
| OO Pandora           | 1 | ((220622-lewiatan-za-pandore)) |
| OO Straszliwy Pająk  | 1 | ((220622-lewiatan-za-pandore)) |
| Otto Azgorn          | 1 | ((210526-morderstwo-na-inferni)) |
| Rita Stratos         | 1 | ((240131-anomalna-mavidiz)) |
| Roland Sowiński      | 1 | ((211117-porwany-trismegistos)) |
| Rzeźnik Parszywiec Diakon | 1 | ((220622-lewiatan-za-pandore)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| Tal Marczak          | 1 | ((210526-morderstwo-na-inferni)) |
| Tara Ogniczek        | 1 | ((240131-anomalna-mavidiz)) |
| Zygfryd Maus         | 1 | ((211117-porwany-trismegistos)) |