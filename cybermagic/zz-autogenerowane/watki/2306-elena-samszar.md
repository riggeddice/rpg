# Elena Samszar
## Identyfikator

Id: 2306-elena-samszar

## Sekcja Opowieści

### Rozbierany poker do zwalczania Interis

* **uid:** 231024-rozbierany-poker-do-zwalczania-interis, _numer względny_: 12
* **daty:** 0095-10-08 - 0095-10-10
* **obecni:** Adelajda Kalmiris, Cyprian Mirisztalnik, Elena Samszar, Estella Gwozdnik, Jaromir Gaburon, Jarosław Mirelski, Maurycy Derwisz, Mikołaj Larnecjat, Paweł Lawarczak, Tomasz Afagrel

Streszczenie:

Tienki powoli aklimatyzują się z bazą (m.in. zbierając ziemniaki). Okazuje się, że rozpad i gnicie ziemniaków są powiązane z energią Nihilusa; gdzieś tu jest potężne źródło energii Interis. Elena nie tylko powiedziała o tym kapitanowi Larnecjatowi ale dodatkowo ściągnęła świeże jedzenie od Samszarów, co zdecydowanie podniesie morale. Tienki spotkały się ze swoim zespołem i jakkolwiek oni oszukiwali tienki w karty, ale Elena oszukiwała bardziej (magią) - udało im się zachować ubrania a nawet zaskarbić sobie pewien szacunek.

Aktor w Opowieści:

* Dokonanie:
    * bardzo kiepsko obiera ziemniaki (aż się zacięła); wykryła magicznie źródło Interis. Potem oszukiwała żołnierzy w karty, ale wprowadziła do gry Adelajdę, żeby nie byli zbyt wściekli. Pozyskała jedzenie dla Orbitera od Samszarów.
* Progresja:
    * pochwała od kapitana Larnecjata, skierowana do rodu Samszar odnośnie samej Eleny
    * zaakceptowana przez żołnierzy; traktowana jako 'swoja tien porucznik'. Ale - ma zakaz obierania ziemniaków i się z niej tu podśmiewują


### Druga tienka przybywa na pomoc

* **uid:** 230905-druga-tienka-przybywa-na-pomoc, _numer względny_: 11
* **daty:** 0095-10-05 - 0095-10-07
* **obecni:** Adelajda Kalmiris, Cyprian Mirisztalnik, Elena Samszar, Mikołaj Larnecjat, Zenobia Samszar

Streszczenie:

Rodzice Eleny chronili ją przed 'wygnaniem' do wspierania Orbitera, ale nie udało im się. Orbiter potrzebuje Samszara, Elena się nadaje i dostała od mamy list polecający - bo na Flarze jest ktoś kto kojarzy Zenobię Samszar. Elena jedzie w Góry Hallarmeng. To jest "morze sargassowe". Elena dostanie dobry sprzęt od doświadczonych Samszarów, Chevaliera i Cypriana z Adelajdą. A na miejscu Elena zrobiła dobre wrażenie na komendancie Mikołaju Larnecjacie. Elena się ziemniaków nie boi. Czas zacząć nowe życie.

Aktor w Opowieści:

* Dokonanie:
    * przekonała wszystkich, by jednak nie dawali jej nadmiernych niepotrzebnych rzeczy i pozytywnie podeszła do tematu 'wygnania w góry'. Ma wsparcie matki i list polecający.
* Progresja:
    * wsparcie Adelajdy, Cypriana oraz własny Chevalier. I sprzęt do przetrwania w paskudnych warunkach górskich.


### Nauczmy młodego tiena jak żyć

* **uid:** 230808-nauczmy-mlodego-tiena-jak-zyc, _numer względny_: 10
* **daty:** 0095-09-14 - 0095-09-23
* **obecni:** Amanda Kajrat, Armin Samszar, Celina Maskiewnik, Elena Samszar, Florian Samszar, Karolinus Samszar, Kleopatra Trusiek, Rufus Bilgemener, Tadeusz Maskiewnik, Wiktor Blakenbauer

Streszczenie:

Florian chciał dać szansę Elenie i Karolinusowi i zrobił operację w której E+K mają wychować młodego Armina z pomocą rodu. Jednak E+K zadziałali w sposób pokazujący że są chętni do lekceważącego krzywdzenia ludzi i jakkolwiek osiągnęli cel z Arminem, oni sami "zawiedli". Florian dostał wpierdol. Amanda i Elena nawiązały pozytywne kontakty z Wiktorem Blakenbauerem.

Aktor w Opowieści:

* Dokonanie:
    * mercenary (co mam z tego mieć) a nie pomoc rodowi; opracowała (z Amandą) plan jak przerazić młodego Armina oraz sama uznała, że nie będzie współpracować do tego celu z innymi Samszarami. Zrobiła Makaronowego Potwora, zbudowała sojusz z Wiktorem Blakenbauerem (nadając mu Amandę Kajrat jako mafię za nagrodę) i nie dała Karolinusowi zwinąć do łóżka Kleopatry. Gdy pojawił się Karolinus, zaprzestała aktywnych działań.
* Progresja:
    * zbudowała silny sojusz z Wiktorem Blakenbauerem; Wiktor jest zainteresowany byciem wsparciem dla Eleny, ona jest znacząca i nie tylko w kontekście mafii
    * Samszarowie stwierdzili, że nie dba o ród. Wykonuje polecenia, ale nie ma tam 'serca'. Zdaniem Samszarów nie zależy jej na nikim poza niej samej.


### Zablokowana sentisieć w krainie makaronu

* **uid:** 230711-zablokowana-sentisiec-w-krainie-makaronu, _numer względny_: 9
* **daty:** 0095-09-05 - 0095-09-08
* **obecni:** Elena Samszar, Ignatus Blakenbauer, Irek Kraczownik, Karolinus Samszar, Petra Samszar, Wacław Samszar, Wiktor Blakenbauer

Streszczenie:

Rośnie napięcie między Hiperpsychotronikami i Eleną, bo Elena znalazła połączenie ich badań z Eternią. Karolinus nadał Wacława Amandzie... łącznie z Itrią. Tymczasem w Triticatusie (mieście makaronu) tienka mająca rozwiązać problem, Petra, weszła w głupi zakład z Wiktorem Blakenbauerem. Gdy Zespół tam trafił, Elena pokłóciła się ostro z Petrą, zestrzeliła jej drony i Paradoksem zablokowała sentisieć. Petra, przekonana, że Elena jest psychopatką przeprosiła Wiktora. A tymczasem Ożywiony Makaron stanowił kłopoty. W końcu Karolinus odblokował sentisieć, Wiktor zniszczył płaszczkę i wszystko JAKOŚ wróciło do normy, acz Petra została wrogiem Zespołu.

Aktor w Opowieści:

* Dokonanie:
    * potraktowana przez Hiperpsychotroników amnestykami i nie wie co jej zrobili. Mistrzyni ciętej riposty konfliktująca się z Petrą; używając znajomego Ignatusa Blakenbauera doszła do linku eternijskiego Hiperpsychotroników. W Triticatusie skonfliktowała się z Petrą, rozwaliła jej drony, Paradoksem zablokowała sentisieć, uratowała nieszczęśnika opętanego przez makaron i ogólnia była niebezpieczna i konfliktowa. Ale skuteczna.
* Progresja:
    * Petra Samszar jest przekonana, że ona jest psychopatką i nie dba o nikogo. Chce to ujawnić i udowodnić.
    * po operacji chronienia duchów udowodniła w okolicy, że dba o duchy, ale o ludzi zupełnie nie.


### Ratuj młodzież dla Kajrata

* **uid:** 230627-ratuj-mlodziez-dla-kajrata, _numer względny_: 8
* **daty:** 0095-08-20 - 0095-08-25
* **obecni:** AJA Szybka Strzała, Amanda Kajrat, Elena Samszar, Ernest Kajrat, Herbert Samszar, Impresja Ignicja Incydencja Diakon, Karolinus Samszar, Mitria Ira, Talia Aegis, Wacław Samszar, Wargun Ira

Streszczenie:

Herbert powiedział o lokacjach baz i że to prace na duchach. Strzała wie o tym że ma uszkodzoną psychotronikę - skontaktowała Zespół z Kajratem (S. potrzebuje pomocy Talii). Kajrat da dostęp do Talii, jeśli Zespół pomoże uratować noktiańskich młodych magów z niewoli Samszarów. Po znalezieniu bazy Karolinus wywabił na Itrię (którą dołączył do drużyny XD) Wacława (strażnika i ekstraktora) a Elena z Amandą Kajrat wydobyły nieszczęsnych noktian. Strzała odjechała do Kajrata. Kajrat zostawił Zespołowi Amandę do pomocy.

Aktor w Opowieści:

* Dokonanie:
    * ostro negocjuje z Kajratem, zapewniając m.in. wsparcie Amandy. Potem ukrywa Amandę sentisiecią, wchodzi do bazy hiperpsychotroników i wyciąga uwięzionych noktiańskich magów kombinacją magii i sentisieci.


### Piękna Diakonka i rytuał nirwany kóz

* **uid:** 230606-piekna-diakonka-i-rytual-nirwany-koz, _numer względny_: 7
* **daty:** 0095-08-15 - 0095-08-18
* **obecni:** AJA Szybka Strzała, Dźwiedź Łagodne Słowo, Elena Samszar, Herbert Samszar, Impresja Ignicja Incydencja Diakon, Karolinus Samszar, Maks Samszar

Streszczenie:

Itria Diakon ma kolejny projekt - nirwana kóz u Samszarów. Jej obecność spowodowała chaos w Forcie Tawalizer. Karolinus i Elena S. dotarli tam by dowiedzieć się o tajnych podziemnych bazach Samszarów od Herberta, ale musieli wpierw rozwiązać problem z Itrią. W końcu doprowadzili do rytuału kóz (dzięki Elenie i jej researchowi) i trochę naprawili sytuację. Czas spotkać się z Herbertem poza terenem sentisieci i poznać sekrety badań nad duchami.

Aktor w Opowieści:

* Dokonanie:
    * znalazła rytuał nirwany kóz szperając po bibliotekach, ale nie poszukała bardzo głęboko by nie musieć się przyznawać kolegom z biblioteki. Potem nauczyła Maksa tego rytuału, ale nie zadbała o dokładność - bo to i tak będzie tylko raz czy dwa razy a nie będzie się upokarzać. Nie chce patrzeć na kolesia przebranego za kozę.
* Progresja:
    * wysłała do Vioriki informację o tym, że Romeo spieprzył operację. Romeo powiedział Viorice, że Elena S. sobie z nim nie radziła. Wniosek Verlenów: Elena jest 'słaba' i 'irytująca'.


### Romeo, dyskretny instalator Supreme Missionforce

* **uid:** 230523-romeo-dyskretny-instalator-supreme-missionforce, _numer względny_: 6
* **daty:** 0095-08-09 - 0095-08-11
* **obecni:** AJA Szybka Strzała, Albert Samszar, Elena Samszar, Karolinus Samszar, Maja Samszar, Nataniel Samszar, Romeo Verlen

Streszczenie:

Strzała ma za zadanie wychować i nauczyć współpracy Karolinusa i Eleny. Karolinus jest wezwany do interwencji, gdy Maja przesyła sygnał SOS. Maja twierdzi, że niechcący mogła zabić kolegę. Ekipa dociera na miejsce, gdzie odkrywają ciężarówkę z zaawansowanym sprzętem. Maja jest w stanie panicznym, a jej magia wymyka się spod kontroli - próbuje przebić dziwne pole siłowe otaczające dziurę w piwnicy biurowca. Karolinus i Elena zmuszają Maję do opuszczenia miejsca, obiecując uratować Romeo. W międzyczasie, Strzała zabiera Maję do Ogrodów Medytacyjnych by nie miała problemów z ojcem. Elena zyskuje na czasie przed ojcem Mai (Albertem), dopóki Maja nie jest bezpiecznie umieszczona w Ogrodach. Potem wydobywają Romeo z podziemi zauważając, że ktoś tam ma tajną dziwną bazę do eksperymentów na duchach.

Aktor w Opowieści:

* Dokonanie:
    * przeszła przez sentisieć i wykryła obecność monterów w ciężarówce i zmiany w sentisieci (acz zaalarmowała wszystkie strony łącznie z Albertem). Bablała Albertowi kupując czas Strzale na ewakuację Mai, nawet kosztem swojej reputacji. Na końcu połączyła się z dziwnym duchem, co ją wyłączyło z akcji na moment.
* Progresja:
    * zdaniem Alberta Samszara, gdy pije to nie da się z nią dogadać i jest niezwykle irytująca. Ogólnie - zwykle niegodna uwagi.


### Karolinka - raciczki zemsty Verlenów

* **uid:** 230516-karolinka-raciczki-zemsty-verlenow, _numer względny_: 5
* **daty:** 0095-07-29 - 0095-07-31
* **obecni:** Aleksander Samszar, Amara Zegarzec, Elena Samszar, Franciszek Chartowiec, Karolinus Samszar, Ludmiła Zegarzec

Streszczenie:

Karolinka, świnka podłożona przez Verlenów na Wielkie Kwiatowisko zaczęła polować na lokalne duchy a przedsiębiorcza Ludmiła z pobliskiego hotelu zorganizowała dziennikarza Paktu i okazję do zarobienia. Karolinus i Elena przebili się przez problematycznego dziennikarza Paktu, zwabili świnkę do pojazdu i uśpili oraz podrzucili ją (rękami żołnierzy) do Verlenlandu. A Strzała jest naprawiana.

Aktor w Opowieści:

* Dokonanie:
    * chciała uniknąć straty twarzy, ale bardziej chciała chronić duchy przed glukszwajnem. Unikała prasy, ale rzuciła w świnkę jabłkiem. Zniszczyła drony dziennikarza "przypadkiem".
* Progresja:
    * na wideo Paktu gdy zwalczali glukszwajna jako "opiekunka duchów i obrończyni ich przed świnią". Popularność wśród Paktu rośnie.


### Samszarowie, Lemurczak i fortel Strzały

* **uid:** 230509-samszarowie-lemurczak-i-fortel-strzaly, _numer względny_: 4
* **daty:** 0095-07-24 - 0095-07-26
* **obecni:** AJA Szybka Strzała, Elena Samszar, Irek Kraczownik, Jonatan Lemurczak, Karolinus Samszar, Roland Samszar

Streszczenie:

Strzała w ruinie, ale dała radę dotrzeć do w miarę bezpiecznego miejsca pod grzmotoptakami. Gdy dwójka nastolatków na które poluje ich zmieniona przez Lemurczaka matka się pojawili blisko, Elena ją unieruchomiła a Karolinus przekształcił w normalną formę. Acz Paradoksem wysłał sygnaturę Verlenopodobną. Gdy Karolinus i Elena się kłócą czy pomóc czy czekać, Strzała pojedynczą droną wymanewrowała Stegozaur-class support hovertank i przestraszyła Lemurczaka hintując, że Verlenowie polujący na ptaki są w pobliżu. Elena zmanipulowała Irka, więc E+K wyszli na osoby pozytywne które chcą dobrze, acz nie zawsze mają idealne plany (bo są młodzi). A Roland zajmie się Sanktuarium Kazitan.

Aktor w Opowieści:

* Dokonanie:
    * duża wrogość do egzorcysty Irka; nie chce ocieplać stosunków. Unieruchomiła magią przekształconą przez Lemurczaka matkę nastolatków. Zmanipulowała Irka, by ten powiedział że Elena i Karolinus są po właściwej stronie i on nie był porwany tylko ich potrzebował. Dzięki temu Samszarowie wyszli na bohaterów (acz jeszcze nieudolnych bo młodych) a nie na potwory z Aurum XD.


### Egzorcysta z Sanktuarium

* **uid:** 230411-egzorcysta-z-sanktuarium, _numer względny_: 3
* **daty:** 0095-07-21 - 0095-07-23
* **obecni:** AJA Szybka Strzała, Arnold Kazitan, Elena Samszar, Irek Kraczownik, Karolinus Samszar, Tadeusz Dzwańczak

Streszczenie:

Egzorcysta Irek poszukiwany przez Samszarów jest w Sanktuarium Kazitan w Przelotyku Zachodnim Dzikim. Na miejscu Sanktuarium ulega Emisji - katastroficzne elementalne działania. Zespół ratuje dzieciaka od Emisji (najpierw mu zagrażając XD), ale gdy dociera do egzorcysty - ostatniego maga który próbuje pomóc Sanktuarium, to go porywają. Przy próbie porwania Strzała zostaje ciężko uszkodzona i nie daje rady wrócić do Powiatu Samszar - crashlanduje w bezpiecznej części Przelotyka.

Aktor w Opowieści:

* Dokonanie:
    * nie jest zainteresowana pomaganiem dziecku, ale nie chce niszczyć Sanktuarium. Jednak sprawiedliwość i "swoi ludzie" muszą być uratowani. Skonfliktowana, pozwala Karolinusowi porwać Irka. Bardziej pasywna rola, nie wie co robić w zastałej sytuacji.


### Wszystkie duchy Siewczyna

* **uid:** 230404-wszystkie-duchy-siewczyna, _numer względny_: 2
* **daty:** 0095-07-18 - 0095-07-20
* **obecni:** AJA Szybka Strzała, Elena Samszar, Irek Kraczownik, Karolinus Samszar, Maksymilian Sforzeczok

Streszczenie:

Karolinus, wraz z kuzynką Eleną Samszar zostali wysłani do Siewczyna gdzie podobno są problemy (by uniknąć dalszego antagonizowania Verlenów). Na miejscu okazało się, że niekompetentny egzorcysta Irek sprowadził tu Hybrydę noktiańskiej TAI i ducha oraz ten byt próbował zemścić się za śmierć swoich ludzi. Elena Paradoksem zniszczyła Ducha Opiekuńczego Spichlerza a Karolinus Paradoksem stworzył strażniczego ducha w kształcie Vioriki w bikini. A w tle - spory między ludźmi i duchami (podsycane przez Hybrydę) oraz między podejściem 'ekonomia vs harmonia'.

Aktor w Opowieści:

* Dokonanie:
    * przesłuchiwała jako tienka ludzi pracujących dla lokalnego dyrektora nie lubiącego magii; magicznie połączyła się ze Strażnikiem Spichlerza i Paradoksem dała Hybrydzie owego Strażnika zniszczyć. Ale przekonała Hybrydę, że egzorcysta jest winny i kupiła czas Karolinusowi i Strzale.
* Progresja:
    * zniszczyła lokalnego Ducha Strażniczego Spichlerza, który istniał 80 lat. W Siewczynie jej tego nie zapomną...


### Żywy artefakt w Gwiazdoczach

* **uid:** 230418-zywy-artefakt-w-gwiazdoczach, _numer względny_: 1
* **daty:** 0094-10-04 - 0094-10-06
* **obecni:** Adelaida Samszar, Antonina Blakenbauer, Elena Samszar, Joachim Pulkmocz, Neidria Lazvarin, Robinson Porzecznik, Sara Mazirin

Streszczenie:

Elena Samszar została poproszona przez bibliotekarza o pomoc - jego kuzyn zakochał się (przez inną Samszarkę) w poezji wierszy Verlenów o niedźwiedziach. Elena poszła pomóc, ale okazało się, że to coś dziwnego. Zebrała wsparcie - Robinson i Antonina i doszli do tego, że to "żywy tatuaż Esuriit" na człowieku. Niuchacze doprowadziły ich do posiadaczki tatuażu i Elena, używając sentisieci, złapała zarówno unsealowanego ducha który podpowiadał jak używać Esuriit jak i nieszczęśniczkę z tatuażem. A w tle - Paradoks z niedźwiedziogorami śpiewającymi kiepską poezję zwalczanymi przez sentisieć.

Aktor w Opowieści:

* Dokonanie:
    * gdy Joachim się do niej zwrócił, że Samszarka przeklęła jego kuzyna, zajęła się sprawą. Zebrała ekipę - turysta Porzecznik, koleżanka Blakenbauer - i doszła do tego, że to nie był czar a 'żywy artefakt', tatuaż Esuriit. Po zlokalizowaniu ofiary, zamknęła ją sentisiecią w komnacie i złapała też ducha doradzającego w sprawie Esuriit zanim sekrety jak duch został unsealowany zanikną.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 12, @: 0095-10-10
    1. Primus    : 12, @: 0095-10-10
        1. Sektor Astoriański    : 12, @: 0095-10-10
            1. Astoria    : 12, @: 0095-10-10
                1. Sojusz Letejski, W    : 2, @: 0095-10-10
                    1. Góry Hallarmeng    : 2, @: 0095-10-10
                        1. Przyprzelotyk    : 2, @: 0095-10-10
                            1. Korony Cmentarne    : 2, @: 0095-10-10
                                1. Dolina Krosadasza    : 2, @: 0095-10-10
                                1. Ruina Lohalian    : 2, @: 0095-10-10
                1. Sojusz Letejski    : 10, @: 0095-09-23
                    1. Aurum    : 8, @: 0095-09-23
                        1. Powiat Blakenbauer    : 1, @: 0095-09-23
                            1. Gęstwina Duchów    : 1, @: 0095-09-23
                        1. Powiat Samszar    : 8, @: 0095-09-23
                            1. Fort Tawalizer    : 1, @: 0095-08-18
                                1. Obserwatorium Potworów i Fort (E)    : 1, @: 0095-08-18
                                    1. Areszt    : 1, @: 0095-08-18
                                    1. Koszary garnizonu    : 1, @: 0095-08-18
                                1. Wzgórza Potworów (N)    : 1, @: 0095-08-18
                                    1. Farmy kóz    : 1, @: 0095-08-18
                            1. Gwiazdoczy, okolice    : 1, @: 0095-08-25
                                1. Technopark Jutra    : 1, @: 0095-08-25
                                    1. Inkubatory małego biznesu    : 1, @: 0095-08-25
                            1. Gwiazdoczy    : 2, @: 0095-08-25
                                1. Centrum (Centrum)    : 1, @: 0094-10-06
                                    1. Puby i restauracje    : 1, @: 0094-10-06
                                    1. Sklepy i kawiarnie    : 1, @: 0094-10-06
                                1. Dzielnica Akademicka (NW, W)    : 1, @: 0094-10-06
                                    1. Czytelnie naukowe    : 1, @: 0094-10-06
                                    1. Kampus uczelniany    : 1, @: 0094-10-06
                                    1. Muzeum Historii    : 1, @: 0094-10-06
                                    1. Uniwersytet (NW)    : 1, @: 0094-10-06
                                    1. Wielka Biblioteka (W)    : 1, @: 0094-10-06
                                1. Dzielnica studencka (N)    : 1, @: 0094-10-06
                                    1. Strefa Sportowa    : 1, @: 0094-10-06
                                1. Dzielnica Technologii (S)    : 1, @: 0094-10-06
                                    1. Centralna stacja pociągów    : 1, @: 0094-10-06
                                    1. Centrum Eszary    : 1, @: 0094-10-06
                                    1. Park Technologiczny    : 1, @: 0094-10-06
                                    1. Warsztaty i inkubatory    : 1, @: 0094-10-06
                                1. Szczelina Światów (E)    : 1, @: 0094-10-06
                                    1. Archiwa duchów    : 1, @: 0094-10-06
                                    1. Instytut Sztuki Wspomaganej    : 1, @: 0094-10-06
                                    1. Kalejdoskop Astralny    : 1, @: 0094-10-06
                                    1. Magitrownie puryfikacyjne    : 1, @: 0094-10-06
                                    1. Plac rytuałów    : 1, @: 0094-10-06
                                    1. Wielki Cenotaf Skupiający    : 1, @: 0094-10-06
                            1. Karmazynowy Świt, okolice    : 1, @: 0095-08-11
                                1. Centrum Danych Symulacji Zarządzania    : 1, @: 0095-08-11
                                    1. Techbunkier Arvitas    : 1, @: 0095-08-11
                                        1. Kontrola bezpieczeństwa (1)    : 1, @: 0095-08-11
                                        1. Kwatery mieszkalne (1)    : 1, @: 0095-08-11
                            1. Karmazynowy Świt    : 1, @: 0095-08-11
                            1. Siewczyn    : 1, @: 0095-07-20
                                1. Astralne Ogrody (ES)    : 1, @: 0095-07-20
                                    1. Centralne Biura Rolnicze    : 1, @: 0095-07-20
                                    1. Centrum R&D dla zrównoważonego rolnictwa    : 1, @: 0095-07-20
                                    1. Dom Szamana    : 1, @: 0095-07-20
                                    1. Fabryka neutralizatorów astralnych    : 1, @: 0095-07-20
                                    1. Gaj Duchów    : 1, @: 0095-07-20
                                    1. Rezydencje Harmonii    : 1, @: 0095-07-20
                                1. Centrum Jedności Mieszka (Center)    : 1, @: 0095-07-20
                                    1. Bazar rękodzieła    : 1, @: 0095-07-20
                                    1. Drzewo Jedności    : 1, @: 0095-07-20
                                    1. Most jedności    : 1, @: 0095-07-20
                                    1. Park miejski    : 1, @: 0095-07-20
                                    1. Ratusz miejski    : 1, @: 0095-07-20
                                1. Północne obrzeża    : 1, @: 0095-07-20
                                    1. Spichlerz Jedności    : 1, @: 0095-07-20
                                1. Wzgórza Industrialnej Harmonii (NW)    : 1, @: 0095-07-20
                                    1. Fabryka siewników i sadzarek    : 1, @: 0095-07-20
                                    1. Fabryka traktorów    : 1, @: 0095-07-20
                                    1. Przestrzeń mieszkalna    : 1, @: 0095-07-20
                                    1. Sklepy i centra handlowe    : 1, @: 0095-07-20
                            1. Triticatus, północ    : 2, @: 0095-09-23
                                1. Dopływ Strumienia Pszenicznego    : 1, @: 0095-09-08
                                1. Stare magazyny    : 1, @: 0095-09-23
                            1. Triticatus    : 2, @: 0095-09-23
                                1. Makaroniarnia (NW)    : 2, @: 0095-09-23
                                    1. Formatornia    : 1, @: 0095-09-08
                                    1. Pola Pszenicy    : 2, @: 0095-09-23
                                    1. Semolinatorium    : 1, @: 0095-09-08
                                    1. Strumień Pszeniczny    : 1, @: 0095-09-08
                                    1. Systemy Irygacyjne    : 1, @: 0095-09-08
                                    1. Wielka Suszarnia    : 1, @: 0095-09-08
                                    1. Wielki Młyn    : 1, @: 0095-09-08
                                1. Pszenicznik (SE)    : 1, @: 0095-09-08
                                    1. Dworzec Maglev    : 1, @: 0095-09-08
                                    1. Magazyny    : 1, @: 0095-09-08
                                    1. Obszar administracyjny    : 1, @: 0095-09-08
                                    1. Rezydencje mieszkalne    : 1, @: 0095-09-08
                            1. Wielkie Kwiatowisko    : 1, @: 0095-07-31
                                1. Hotel Odpoczynek Pszczół    : 1, @: 0095-07-31
                                1. Menhir Centralny    : 1, @: 0095-07-31
                        1. Verlenland    : 1, @: 0095-08-25
                            1. Duchowiec Śmieszny, okolice    : 1, @: 0095-08-25
                            1. Duchowiec Śmieszny    : 1, @: 0095-08-25
                    1. Przelotyk    : 2, @: 0095-07-26
                        1. Przelotyk Zachodni Dziki    : 2, @: 0095-07-26
                            1. Lancatim, okolice    : 1, @: 0095-07-26
                            1. Lancatim    : 1, @: 0095-07-26
                            1. Sanktuarium Kazitan    : 1, @: 0095-07-23
                                1. Dystrykt Szafir    : 1, @: 0095-07-23
                                    1. Komnata lecznicza    : 1, @: 0095-07-23
                                    1. Komnata mieszkalna    : 1, @: 0095-07-23

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolinus Samszar    | 9 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230516-karolinka-raciczki-zemsty-verlenow; 230523-romeo-dyskretny-instalator-supreme-missionforce; 230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata; 230711-zablokowana-sentisiec-w-krainie-makaronu; 230808-nauczmy-mlodego-tiena-jak-zyc)) |
| AJA Szybka Strzała   | 6 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230523-romeo-dyskretny-instalator-supreme-missionforce; 230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata)) |
| Irek Kraczownik      | 4 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Adelajda Kalmiris    | 2 | ((230905-druga-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Amanda Kajrat        | 2 | ((230627-ratuj-mlodziez-dla-kajrata; 230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Cyprian Mirisztalnik | 2 | ((230905-druga-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Herbert Samszar      | 2 | ((230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata)) |
| Impresja Ignicja Incydencja Diakon | 2 | ((230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata)) |
| Mikołaj Larnecjat    | 2 | ((230905-druga-tienka-przybywa-na-pomoc; 231024-rozbierany-poker-do-zwalczania-interis)) |
| Wacław Samszar       | 2 | ((230627-ratuj-mlodziez-dla-kajrata; 230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Wiktor Blakenbauer   | 2 | ((230711-zablokowana-sentisiec-w-krainie-makaronu; 230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Adelaida Samszar     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Albert Samszar       | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Aleksander Samszar   | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Amara Zegarzec       | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Antonina Blakenbauer | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Armin Samszar        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Arnold Kazitan       | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Celina Maskiewnik    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Dźwiedź Łagodne Słowo | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Ernest Kajrat        | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Estella Gwozdnik     | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Florian Samszar      | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Franciszek Chartowiec | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Ignatus Blakenbauer  | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Jaromir Gaburon      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Jarosław Mirelski    | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Joachim Pulkmocz     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Jonatan Lemurczak    | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Kleopatra Trusiek    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Ludmiła Zegarzec     | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Maja Samszar         | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Maks Samszar         | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Maksymilian Sforzeczok | 1 | ((230404-wszystkie-duchy-siewczyna)) |
| Maurycy Derwisz      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Mitria Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Nataniel Samszar     | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Neidria Lazvarin     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Paweł Lawarczak      | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Petra Samszar        | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Robinson Porzecznik  | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Roland Samszar       | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Romeo Verlen         | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Rufus Bilgemener     | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Sara Mazirin         | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Tadeusz Dzwańczak    | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Tadeusz Maskiewnik   | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Talia Aegis          | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Tomasz Afagrel       | 1 | ((231024-rozbierany-poker-do-zwalczania-interis)) |
| Wargun Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Zenobia Samszar      | 1 | ((230905-druga-tienka-przybywa-na-pomoc)) |