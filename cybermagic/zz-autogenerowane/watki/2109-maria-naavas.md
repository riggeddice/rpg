# Maria Naavas
## Identyfikator

Id: 2109-maria-naavas

## Sekcja Opowieści

### Lewiatan za Pandorę

* **uid:** 220622-lewiatan-za-pandore, _numer względny_: 13
* **daty:** 0112-09-30 - 0112-10-03
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Ola Szerszeń, OO Infernia, OO Pandora, OO Straszliwy Pająk, OO Tivr, Raoul Lavanis, Rzeźnik Parszywiec Diakon

Streszczenie:

PRZESZŁOŚĆ: Ola Szerszeń opiekuje się Eleną w Regeneratorze; okazuje się być biosyntem (zakazane eksperymenty Termii). Rzeźnik Diakon to łagodny i jowialny kapitan Straszliwego Pająka, acz znienawidzony za przeszłość zdrajcy Orbitera oraz Noctis. TERAŹNIEJSZOŚĆ: Lewiatan zostaje zestrzelony torpedą anihilacyjną, dzięki Raoulowi ratujemy wielu ludzi na orbicie Lewiatana. Malictrix d'Pandora zostaje zniszczona przez Lewiatana, ale Klaudia ma jego mikrokopię dzięki Ariannie.

Aktor w Opowieści:

* Dokonanie:
    * poproszona przez Klaudię dodała info o Oli Szerszeń do swojej listy szczurzenia po adm. Termii. Doszła do tego - niestety, poszła za ostro i skutecznie zniszczyła kilka serc i uderzyła w plany i reputację Termii. Maria ma problem, ale Termia też.


### EtAur - dziwnie ważny węzeł

* **uid:** 220706-etaur-dziwnie-wazny-wezel, _numer względny_: 12
* **daty:** 0112-09-19 - 0112-09-21
* **obecni:** Aleksandra Termia, Eustachy Korkoran, Klaudia Stryk, Maria Naavas

Streszczenie:

Maria zainteresowała się planem adm. Termii wskazującym na to, że ta zdradza Orbiter z Syndykatem Aureliona. Eustachy (do którego przyszła) zapytał Termię wprost i doszedł do tego, że Termia szuka koloidowej bazy Aureliona na księżycu Elang swymi bezdusznymi metodami. Eustachy i Klaudia wskazali Termii, że baza EtAur jest potencjalnie punktem kontaktowym Syndykatu z Astorią i tu jest ryzyko. Eustachy doprowadził do kontaktu Termia - EtAur, ale też gra na współpracę Inferni i EtAur.

Aktor w Opowieści:

* Dokonanie:
    * podejrzewa adm. Termię o grę przeciwko Orbiterowi i za Syndykatem Aureliona; przyszła z tym do Eustachego i chyba zaczęła sojusz Termia - EtAur?


### EtAur i przynęta na Kryptę

* **uid:** 220330-etaur-i-przyneta-na-krypte, _numer względny_: 11
* **daty:** 0112-07-02 - 0112-07-05
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Lamia Akacja, Leszek Czarban, Maria Naavas, Raoul Lavanis

Streszczenie:

Okazało się, że "potwór" to były faeril z Neikatis zarażony bronią biologiczną. Próbował doprowadzić do katastrofy by Orbiter lub Eternia zabrali się za leczenie tego cholerstwa, pomagał mu Antoni, p.o. TAI na EtAur. Maria doszła do biostruktury, Klaudia przejęła systemy bazy, Eustachy przechwycił gościa, Arianna zdobyła bazę politycznie. A Klaudia zrobiła raport, by Orbiter nie atakował więcej neutralnych i potencjalnie sojuszniczych sił nie-Orbiterowych w kosmosie. Jako, że Lamii nic się nie stało, Melania jest szczęśliwa.

Aktor w Opowieści:

* Dokonanie:
    * niebezpieczne zaklęcie na uciekającego potwora, ale doszła do tego, że jest tu PLAGA - dziwna Irytka - i broń biologiczna w jednym.


### Potwór czy choroba na EtAur?

* **uid:** 220316-potwor-czy-choroba-na-etaur, _numer względny_: 10
* **daty:** 0112-06-29 - 0112-07-01
* **obecni:** Arianna Verlen, Dominika Perikas, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Melania Akacja, Suwan Chankar, Tymoteusz Czerw

Streszczenie:

Na stacji EtAur pojawiły się poważne problemy - potwór? Choroba? Suwan próbuje ustabilizować co się da, ale nic nie może zrobić. Arianna poproszona o pomoc przybyła, ale nikt nie chce z nią praktycznie współpracować. Klaudia doszła do przyczyny problemów - choroba, ale potwór jest wektorem. Arianna doszła do transmisji po kanałach magicznych. Świetnie.

Aktor w Opowieści:

* Dokonanie:
    * zawsze ma "najlepsze" pomysły. Jako, że w EtAur nikt nie lubi Zespołu, zaproponowała rozkochać wszystkich feromonami Eustachego. WTF. Oczywiście - zablokowano ten plan.


### To nie pułapka na Nereidę...

* **uid:** 220105-to-nie-pulapka-na-nereide, _numer względny_: 9
* **daty:** 0112-05-04 - 0112-05-09
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Lutus Amerin, Maria Naavas, Natalia Aradin, Saitaer

Streszczenie:

Plan - dorwanie tajemniczych koloidowych korwet. Wiemy, że to pułapka, więc zróbmy kontr-pułapkę. Ale okazało się, że to nie pułapka na Nereidę a na Infernię. Standardowe badania wprowadziły w Infernię (Dianę) memetic payload i Diana prawie wpadła pod kontrolę Saitaera - odrzucenie kokonu z Natalią sprawiło, że udało się ciężko uszkodzoną Infernię uratować. Zwalczając ixion Arianna sięgnęła do Esuriit przez Elenę; Infernia by zginęła gdyby nie samobójcza akcja Izabeli i Kamila. Elena wchłonęła tyle energii ile była w stanie, przekraczając swój Wzór. ALE - Infernia przetrwała. Przy okazji, Natalia się wykluła. Ani osoba ani statek kosmiczny - mordercza anomalia kosmiczna.

Aktor w Opowieści:

* Dokonanie:
    * z Klaudią złożyła feromony by spowolnić destabilizację Eleny; ixiońska energia wpłynęła do kokonu Natalii. Cichy MVP operacji - dzięki jej działaniom medycznym i stabilizacyjnym (magiczne i nie) udało się uratować większość załogi przed strasznym losem.
* Progresja:
    * z uwagi na swoje bezwględne, spokojne triagowanie i przekierowywanie energii została uznana za potwora przez członków Inferni. Maria spokojnie to akceptuje.


### Kult Saitaera w Neotik

* **uid:** 211222-kult-saitaera-w-neotik, _numer względny_: 8
* **daty:** 0112-04-30 - 0112-05-01
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kasandra Destrukcja Diakon, Klaudia Stryk, Lutus Amerin, Maciek Kalmarzec, Maria Naavas

Streszczenie:

Klaudia wróciła z bronią anty-Serenitową od Tosena, Arianna z feromonami Marii nadała Inferni nową kulturę - miłośnicy lolitek x kultyści x noktianie. Elena uziemiona w Inferni zinfiltrowała Stocznię ukryta przez Klaudię i odkryła niższe Spustoszenie + kult Saitaera. Nereida jest czysta od Saitaera; przetransferowana na Infernię dla Eleny i uzbrojona. Klaudia ma listę jednostek potencjalnie w rękach Saitaera / Spustoszonych. Przygotowania do kontrataku skończone.

Aktor w Opowieści:

* Dokonanie:
    * jej feromony pomogły w mindwarpowaniu przez Ariannę i Izę załogi Inferni i zbudowaniu jej nowej, unikalnej kultury (kult x pro-lolitka x noktianie).


### Sklejanie Inferni do kupy

* **uid:** 211215-sklejanie-inferni-do-kupy, _numer względny_: 7
* **daty:** 0112-04-27 - 0112-04-29
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Kamil Lyraczek, Kasandra Destrukcja Diakon, Leona Astrienko, Maria Naavas, OO Infernia, Roland Sowiński

Streszczenie:

Ixioński wariant Inferni okazuje się całkiem słodki i pod kontrolą Eustachego. Morale Inferni jest w ruinie. Z pomocą Marii Arianna przesuwa kulturę Inferni w stronę "kultyści + noktianie + miłośnicy psychopatycznej lolitki Diany". Eustachy wpada w pułapkę Leony, która chce go wyleczyć czerwiem Esuriit - ale Eustachy przekonuje ją, że ma Dianę pod kontrolą. Jednak gdzieś w tle znajduje się prawdziwy ixioński potwór na stoczni Neotik.

Aktor w Opowieści:

* Dokonanie:
    * zaproponowała Ariannie feromony by pomóc załodze przetrwać na Inferni (uspokajające feromony "dla kotów"). Ogromny sukces - adaptacja udana, załoga może działać na Inferni, choć kultura wyszła nieco dziwnie.


### Prototypowa Nereida Natalii

* **uid:** 211124-prototypowa-nereida-natalii, _numer względny_: 6
* **daty:** 0112-04-20 - 0112-04-24
* **obecni:** Adam Szarjan, Arianna Verlen, Arkadia Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Maria Naavas, Natalia Aradin, OO Infernia, Roland Sowiński, Wawrzyn Rewemis

Streszczenie:

Kampania reklamowa "dołącz do Inferni" zrobiona przez Izę przy użyciu Eustachego jako modela - sukces. Klaudia zdobyła z tego nowych załogantów (acz to jeszcze potrwa). Szarjan uwolnił Klaudię z zarzutów, potrzebuje pomocy w stoczni - jego przyjaciółka Natalia ma problem z prototypem myśliwca klasy Nereida i została ixiońsko zintegrowana. Infernia próbowała Nereidę uratować, ale niestety skończyło się to poważnym uszkodzeniem Nereidy i stworzeniem ixiońskiego kokonu, z którego Natalia musi się wykluć...

Aktor w Opowieści:

* Dokonanie:
    * upiękniła Eustachego i po tym jak Eustachy / Infernia pokrzywdził "nową" załogę Inferni zajęła się ratowaniem żyć. Dlatego nie mogła pomóc Klaudii w ratowaniu Natalii...


### Porwany Trismegistos

* **uid:** 211117-porwany-trismegistos, _numer względny_: 5
* **daty:** 0112-04-15 - 0112-04-17
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Jamon Korab, Kalira d'Trismegistos, Klara Gwozdnik, Klaudia Stryk, Leona Astrienko, Maria Naavas, Mira Anastel, OO Tivr, Roland Sowiński, SC Trismegistos, Zygfryd Maus

Streszczenie:

Wiadomość od pirata że porwał Trismegistos poderwała Ariannę i Tivr. Historia nie trzymała się kupy. Załoga Inferni (Tivru) zorientowała się, że coś jest nie tak - opowieść o statku kupieckim który pozbył się miragenta brzmiała DZIWNIE. Okazało się, że Trismegistos jest statkiem kaperskim. Gdy Trismegistos zażądał poddania się Tivru Klaudia zrobiła sygnał "lol leci na Was Serenit" i TAI Trismegistosa spanikowała. Wolna TAI, współpracująca z fareilem i drakolitką. Wolny dystrykt. Arianna dała im żyć - ale odzyskała porwanych arystokratów i komory adaptacji.

Aktor w Opowieści:

* Dokonanie:
    * zbadała martwego miragenta i wykryła, że to miragent. Potem zajęła się wszystkimi przejętymi arystokratami Aurum - ci do stazy, ci na potem.


### Romans dzięki Esuriit

* **uid:** 211110-romans-dzieki-esuriit, _numer względny_: 4
* **daty:** 0112-04-13 - 0112-04-14
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Karol Reichard, Klaudia Stryk, Leona Astrienko, Maria Naavas, Wawrzyn Rewemis

Streszczenie:

Infernia nie ma dość załogi, by móc działać, więc Termia przekazuje Ariannie Tivr. Leona nie może sobie wybaczyć, że jej nie było i to się stało, więc się wypisuje z pomocą Eustachego (co powoduje plotki Leona x Eustachy). Elena ucieka do Sektora 49 na K1 by być samotna; Arianna organizuje ostre poszukiwania. Eustachy znajduje Elenę by ją pocieszyć. Elena w końcu przyznaje Eustachemu, że go kocha. Maria Naavas z woli Martyna tymczasowo dołącza do załogi Arianny.

Aktor w Opowieści:

* Dokonanie:
    * tymczasowo dołączyła do Inferni z woli Martyna (który jest nieaktywny). Podała Eustachemu leki antyradiacyjne po jego wybryku. Zajmie się Eustachy x Elena ;-).


### Siostrzenica Morlana

* **uid:** 210818-siostrzenica-morlana, _numer względny_: 3
* **daty:** 0112-01-20 - 0112-01-24
* **obecni:** Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Maria Naavas, Nataniel Morlan, Ofelia Morlan, Olgierd Drongon, OO Netrahina, OO Żelazko, SC Fecundatis, SC Światłodóbr, Tomasz Sowiński

Streszczenie:

Tomasz Sowiński próbuje uratować Ofelię Morlan przed Natanielem Morlanem. Nie ma kogo poprosić a Ofelia służy na Netrahinie; poprosił więc Ariannę. Arianna skanując echo pamięci Jolanty w Inferni zdobywa informacje o programie kosmicznym Orbitera, przekazuje to Kramerowi, wykorzystuje wsparcie Olgierda i robią ćwiczenia Żelazko - Netrahina. Ratują Ofelię przed porwaniem przez koloidowy statek Kruków Kasandry.

Aktor w Opowieści:

* Dokonanie:
    * przypisała ("przypadkowo") Ariannę i Tomasza do małego pokoiku na Żelazku, by wywołać zazdrość Olgierda. Z powodzeniem :-). Olgierd overridował decyzję i Arianna była sama.


### Sympozjum Zniszczenia

* **uid:** 210120-sympozjum-zniszczenia, _numer względny_: 2
* **daty:** 0111-08-01 - 0111-08-05
* **obecni:** Arianna Verlen, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Martyn Hiwasser, OA Bakałarz, Olgierd Drongon

Streszczenie:

Maria Naavas próbuje spiknąć Ariannę i Olgierda i do pomocy używa Eustachego. Później - Eustachy, Diana, Klaudia i Elena są na Sympozjum Zniszczenia OA Bakałarz. Próbują przebić się przez anomalną materię. Diana odkrywa słabość owej materii do Esuriit i się Skaża; Skażenie rozlewa się po Bakałarzu i Zespół dostaje opiernicz za niekompetencję (większość na Elenę). Zespół w gniewie, nienawiści i Esuriit fabrykuje Emiter Plagi Nienawiści - nową broń na pokład Inferni, piekielnie niebezpieczną...

Aktor w Opowieści:

* Dokonanie:
    * Abzan (BWG). Poświęci Eustachego by Olgierd i Arianna byli na randce. Wpakowała ich do jednej kapsuły, uszkodziła life support by było zimno i wysłała na 3 dni ;-).


### Wyścig jako randka?

* **uid:** 201125-wyscig-jako-randka, _numer względny_: 1
* **daty:** 0111-04-08 - 0111-04-13
* **obecni:** Arianna Verlen, Diana Arłacz, Gunnar Brunt, Klaudia Stryk, Maria Naavas, Martyn Hiwasser, Olgierd Drongon

Streszczenie:

Infernia traci części i ekipę remontową, bo Olgierd z Żelazka na niej pasożytuje. W wyniku komedii pomyłek doszło do tego, że Olgierd uznał, że Arianna się w nim podkochuje ale nie wie jak zagadać. Skończyło się pojedynkiem ścigaczy na torze sfabrykowanym przez Klaudię i na anomalicznym Skażeniu Kontrolera Pierwszego. Przez Klaudię, naturalnie.

Aktor w Opowieści:

* Dokonanie:
    * była miłość Martyna Hiwassera, zdradziła mu kilka sekretów o Żelazku i jego załodze... plus, poznała od niego "sekrety Arianny x Olgierda".


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 13, @: 0112-10-03
    1. Primus    : 13, @: 0112-10-03
        1. Sektor Astoriański    : 13, @: 0112-10-03
            1. Astoria, Orbita    : 4, @: 0112-04-24
                1. Kontroler Pierwszy    : 4, @: 0112-04-24
                    1. Sektor 43    : 1, @: 0111-04-13
                        1. Tor wyścigowy ścigaczy    : 1, @: 0111-04-13
                    1. Sektor 49    : 1, @: 0112-04-14
                    1. Sektor 57    : 1, @: 0112-01-24
                        1. Mordownia    : 1, @: 0112-01-24
            1. Astoria, Pierścień Zewnętrzny    : 4, @: 0112-05-09
                1. Poligon Stoczni Neotik    : 2, @: 0112-05-09
                1. Stocznia Neotik    : 4, @: 0112-05-09
                    1. Zewnętrzny dok ixioński    : 1, @: 0112-04-29
            1. Elang, księżyc Astorii    : 2, @: 0112-07-05
                1. EtAur Zwycięska    : 2, @: 0112-07-05
                    1. Magazyny wewnętrzne    : 1, @: 0112-07-05
                    1. Sektor bioinżynierii    : 1, @: 0112-07-05
                        1. Biolab    : 1, @: 0112-07-05
                        1. Komory żywieniowe    : 1, @: 0112-07-05
                        1. Skrzydło medyczne    : 1, @: 0112-07-05
                    1. Sektor inżynierski    : 1, @: 0112-07-05
                        1. Panele słoneczne i bateriownia    : 1, @: 0112-07-05
                        1. Podtrzymywanie życia    : 1, @: 0112-07-05
                        1. Serwisownia    : 1, @: 0112-07-05
                        1. Stacja pozyskiwania wody    : 1, @: 0112-07-05
                    1. Sektor mieszkalny    : 1, @: 0112-07-05
                        1. Barbakan    : 1, @: 0112-07-05
                            1. Administracja    : 1, @: 0112-07-05
                            1. Ochrona    : 1, @: 0112-07-05
                            1. Rdzeń AI    : 1, @: 0112-07-05
                        1. Centrum Rozrywki    : 1, @: 0112-07-05
                        1. Podtrzymywanie życia    : 1, @: 0112-07-05
                        1. Pomieszczenia mieszkalne    : 1, @: 0112-07-05
                        1. Przedszkole    : 1, @: 0112-07-05
                        1. Stołówki    : 1, @: 0112-07-05
                        1. Szklarnie    : 1, @: 0112-07-05
                    1. Sektor przeładunkowy    : 1, @: 0112-07-05
                        1. Atraktory małych pakunków    : 1, @: 0112-07-05
                        1. Magazyny    : 1, @: 0112-07-05
                        1. Pieczara Gaulronów    : 1, @: 0112-07-05
                        1. Punkt celny    : 1, @: 0112-07-05
                        1. Stacja grazerów    : 1, @: 0112-07-05
                        1. Stacja przeładunkowa    : 1, @: 0112-07-05
                        1. Starport    : 1, @: 0112-07-05
                    1. Ukryte sektory    : 1, @: 0112-07-05
            1. Neikatis    : 1, @: 0112-04-17
                1. Dystrykt Lennet    : 1, @: 0112-04-17
            1. Libracja Lirańska    : 1, @: 0112-10-03
                1. Anomalia Kolapsu, orbita    : 1, @: 0112-10-03

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 12 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220622-lewiatan-za-pandore)) |
| Eustachy Korkoran    | 12 | ((210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220622-lewiatan-za-pandore; 220706-etaur-dziwnie-wazny-wezel)) |
| Klaudia Stryk        | 12 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220622-lewiatan-za-pandore; 220706-etaur-dziwnie-wazny-wezel)) |
| Elena Verlen         | 8 | ((210120-sympozjum-zniszczenia; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220316-potwor-czy-choroba-na-etaur)) |
| Leona Astrienko      | 5 | ((210818-siostrzenica-morlana; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Izabela Zarantel     | 4 | ((211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Adam Szarjan         | 3 | ((211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Olgierd Drongon      | 3 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana)) |
| OO Infernia          | 3 | ((211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy; 220622-lewiatan-za-pandore)) |
| Roland Sowiński      | 3 | ((211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Diana Arłacz         | 2 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia)) |
| Diana d'Infernia     | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Kamil Lyraczek       | 2 | ((211215-sklejanie-inferni-do-kupy; 220105-to-nie-pulapka-na-nereide)) |
| Kasandra Destrukcja Diakon | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Lutus Amerin         | 2 | ((211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Martyn Hiwasser      | 2 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia)) |
| Natalia Aradin       | 2 | ((211124-prototypowa-nereida-natalii; 220105-to-nie-pulapka-na-nereide)) |
| OO Tivr              | 2 | ((211117-porwany-trismegistos; 220622-lewiatan-za-pandore)) |
| Raoul Lavanis        | 2 | ((220330-etaur-i-przyneta-na-krypte; 220622-lewiatan-za-pandore)) |
| Wawrzyn Rewemis      | 2 | ((211110-romans-dzieki-esuriit; 211124-prototypowa-nereida-natalii)) |
| Aleksandra Termia    | 1 | ((220706-etaur-dziwnie-wazny-wezel)) |
| Antoni Kramer        | 1 | ((210818-siostrzenica-morlana)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Dominika Perikas     | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Gunnar Brunt         | 1 | ((201125-wyscig-jako-randka)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Karol Reichard       | 1 | ((211110-romans-dzieki-esuriit)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Lamia Akacja         | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Leszek Czarban       | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Maciek Kalmarzec     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Melania Akacja       | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Nataniel Morlan      | 1 | ((210818-siostrzenica-morlana)) |
| OA Bakałarz          | 1 | ((210120-sympozjum-zniszczenia)) |
| Ofelia Morlan        | 1 | ((210818-siostrzenica-morlana)) |
| Ola Szerszeń         | 1 | ((220622-lewiatan-za-pandore)) |
| OO Netrahina         | 1 | ((210818-siostrzenica-morlana)) |
| OO Pandora           | 1 | ((220622-lewiatan-za-pandore)) |
| OO Straszliwy Pająk  | 1 | ((220622-lewiatan-za-pandore)) |
| OO Żelazko           | 1 | ((210818-siostrzenica-morlana)) |
| Rzeźnik Parszywiec Diakon | 1 | ((220622-lewiatan-za-pandore)) |
| Saitaer              | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| SC Fecundatis        | 1 | ((210818-siostrzenica-morlana)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| Suwan Chankar        | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Tomasz Sowiński      | 1 | ((210818-siostrzenica-morlana)) |
| Tymoteusz Czerw      | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Zygfryd Maus         | 1 | ((211117-porwany-trismegistos)) |