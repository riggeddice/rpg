# Diana Tevalier
## Identyfikator

Id: 1905-diana-tevalier

## Sekcja Opowieści

### Stary kot a Trzęsawisko

* **uid:** 220814-stary-kot-a-trzesawisko, _numer względny_: 13
* **daty:** 0111-10-12 - 0111-10-13
* **obecni:** Alan Bartozol, Ariela Lechot, Diana Tevalier, Iwan Zawtrak, Karolina Terienak, Kot Rozbójnik, Marian Lechot, Urszula Miłkowicz, Viirai d'Lechotka

Streszczenie:

Właściciele fortifarmy Lechotka kiedyś dali radę wyrwać ziemię Trzęsawisku. Trzęsawisko nie zapomniało. Teraz jak są starzy i sami, Trzęsawisko porwało Arielę. Terminusi nie pomogli Marianowi, więc poszedł by ją uratować i został porwany. Ich anty-anomalny kot Rozbójnik nie był w stanie im pomóc; rannego kota znalazła w rowie Karolina. Zawiozła do Majkłapca, ale kot im uciekł. Karo współpracując z Ulą opanowały Rozbójnika, dogadały się z fortifarmą i z pomocą Alana zniszczyły ludzi porwanych przez Trzęsawisko. A Rozbójnik i fortifarma wpadły do zaskoczonej Uli.

Aktor w Opowieści:

* Dokonanie:
    * skrajnie chroni Alana, ale jak usłyszała, że problem to ludzie na Trzęsawisku, dała Karo się z Alanem skomunikować.


### Marysiowa Hestia Rekinów

* **uid:** 220111-marysiowa-hestia-rekinow, _numer względny_: 12
* **daty:** 0111-09-16 - 0111-09-19
* **obecni:** Diana Tevalier, Ernest Namertel, Hestia d'Rekiny, Jeremi Sowiński, Karolina Terienak, Keira Amarco d'Namertel, Liliana Bankierz, Marysia Sowińska

Streszczenie:

Konflikt Liliana - Marysia się zaostrza. Marysia skupia się na wzmocnieniu Hestii i uzyskaniu kontroli nad Rekinami - przejęła Hestię jako sojuszniczkę. Chevaleresse poprosiła Karolinę o uratowanie Melissy i Karo faktycznie pomogła - Ernest wysłał Keirę i odbili Melissę. To sprawiło, że Mimoza Diakon poczuła się zagrożona i weszła do akcji. Jakby tego było mało, Sowińscy wysyłają Jeremiego by opanował kuzynkę.

Aktor w Opowieści:

* Dokonanie:
    * nie ustąpiła w sprawie Melissy; doprowadziła do tego że Karolina przechwyciła Melissę i rozbiła Kult Ośmiornicy. Rękoma ETERNI.


### Chevaleresse infiltruje Rekiny

* **uid:** 211221-chevaleresse-infiltruje-rekiny, _numer względny_: 11
* **daty:** 0111-09-06 - 0111-09-07
* **obecni:** Alan Bartozol, Barnaba Burgacz, Diana Tevalier, Hestia d'Rekiny, Justynian Diakon, Karolina Terienak, Marysia Sowińska, Melissa Durszenko, Rupert Mysiokornik, Santino Mysiokornik, Staś Arienik, Żorż d'Namertel

Streszczenie:

Hestia zaproponowała Marysi naprawienie Rekin Defense Grid - i od razu wykryli osoby spoza Rekinów i co najmniej jedną infiltratorkę. To Chevaleresse, która szuka Stasia Arienika by Alan nie musiał iść na Trzęsawisko. Karo i Marysia złapały Chevaleresse, w trójkę się dogadały. Karo przechwyciła Stasia (któremu pomagał Babu) i oddała go Alanowi zrzutem ze ścigacza. Justynian skierował oczy na Kult Ośmiornicy który podobno jest w Dzielnicy Rekinów.

Aktor w Opowieści:

* Dokonanie:
    * 17 lat (ale mówi 18, wygląda 15). Zinfiltrowała Rekiny (po raz kolejny), by znaleźć Stasia Arienika. Jeśli tu go nie ma, Alan będzie szukał bliżej Trzęsawiska, a na to ona się nie godzi. Powiedziała prawdę Karo i Marysi. Używa zrobionych przez siebie ciasteczek by być słodsza... lub podać truciznę XD.


### Haracz w parku rozrywki

* **uid:** 200913-haracz-w-parku-rozrywki, _numer względny_: 10
* **daty:** 0110-10-09 - 0110-10-10
* **obecni:** Adam Janor, Alan Bartozol, Diana Tevalier, Franciszek Owadowiec, Karol Senonik, Krystyna Senonik, Maja Janor, Mariusz Trzewń, Melinda Teilert, Pięknotka Diakon, Rafał Kamaron

Streszczenie:

Łatwa misja w okolicach parku rozrywki Janor przekształciła się w dziwną operację z pasożytem magicznym dotykającym pobierającego haracz Grzymościowca i dziwnie zachowującą się komendant policji. Pięknotka i Alan rozwalili siły Grzymościa, ale nie są pewni, czy to nie była tylko dywersja... (a tymczasem Melinda i Chevaleresse świetnie się bawiły w parku rozrywki).

Aktor w Opowieści:

* Dokonanie:
    * świetnie się bawiła w parku rozrywki Janus; tam Dotknęła Kultu Ośmiornicy. Pomogła Alanowi w dyskretnym połączeniu z TAI Hestia d'Janus.
* Progresja:
    * zainteresowana Kultem Ośmiornicy; poczuła jego potęgę. Ona chce rząd dusz i wspierać innych; ludzie Kultu to mają, czemu ona nie może?


### Dom dla Melindy

* **uid:** 200524-dom-dla-melindy, _numer względny_: 9
* **daty:** 0110-09-13 - 0110-09-16
* **obecni:** Diana Lemurczak, Diana Tevalier, Franek Bulterier, Gerwazy Lemurczak, Laura Tesinik, Mariusz Trzewń, Melinda Teilert, Pięknotka Diakon

Streszczenie:

Rodzina chce Melindę z powrotem, Melinda nie chce wracać. Pięknotka szuka Melindy i znalazła ją w Arkadii. Gdy poznała sytuację, we współpracy z Dianą wsadziła Melindę Alanowi przy użyciu Chevaleresse. Wszyscy są zadowoleni, choć Melinda musi opuścić Podwiert - ale wreszcie jest bezpieczna.

Aktor w Opowieści:

* Dokonanie:
    * Pięknotka ją przekonała, że jest taka Melinda która potrzebuje opieki i ma przechlapane. Sama Chevaleresse przekonała Alana że tak ma być. Alan nie podejrzewa Pięknotki.


### Rozbrojenie bomby w Kalbarku

* **uid:** 200222-rozbrojenie-bomby-w-kalbarku, _numer względny_: 8
* **daty:** 0110-07-27 - 0110-08-01
* **obecni:** Aleksandra Szklarska, Ataienne, Bartłomiej Małczarek, Diana Tevalier, Lucjusz Blakenbauer, Mateusz Kardamacz, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Kalbark okazał się jedną wielką bombą. Liberatis, silnie uzbrojeni. Kardamacz, kontroluje całe miasto. Terminusi, skażeni kralotycznie. I Chevaleresse na którą wszystko spadnie jeśli przyjdzie wsparcie. Pięknotka rozmontowała tą bombę, ściągnęła Ataienne (oficjalnie) i Zespół dał radę rozproszyć Liberatis, wycofać Chevaleresse i odzyskać kontrolę nad Barbakanem. Pięknotka zebrała wsparcie i na polu bitwy został na serio tylko Kardamacz.

Aktor w Opowieści:

* Dokonanie:
    * przyczyna wojny Liberatis - Kardamacz, zmanipulowana kralotycznie przez Kardamacza i doń dołączyła. Skończyła przez Pięknotkę w szpitalu.


### Krucjata Chevaleresse

* **uid:** 200202-krucjata-chevaleresse, _numer względny_: 7
* **daty:** 0110-07-24 - 0110-07-27
* **obecni:** Ataienne, Damian Orion, Diana Tevalier, Keraina d'Orion, Malictrix d'Itaran, Mariusz Trzewń, Mateusz Kardamacz, Minerwa Metalia, Pięknotka Diakon, Talia Aegis, Tomasz Tukan

Streszczenie:

Chevaleresse wykradła broń Alana i uzbroiła Liberitias, gdzie działa też Ataienne przeciw Kardamaczowi, Grzymościowi i krzywdzicielowi Alana. Pięknotka doszła do serca problemu i materii, acz potrzebowała wsparcia Orbitera - dokładniej, Kerainy. Dzięki Kerainie wprowadziła Malictrix do Kompleksu Itaran. Teraz to tylko kwestia czasu aż Grzymość zostanie zniszczony.

Aktor w Opowieści:

* Dokonanie:
    * zrozpaczona sytuacją z Alanem, zrobiła coś niewłaściwego - uzbroiła jego bronią Liberitias oraz weszła w sojusz z Ataienne. Najpewniej to nie koniec problemów.


### Polowanie na Pięknotkę

* **uid:** 190901-polowanie-na-pieknotke, _numer względny_: 6
* **daty:** 0110-06-21 - 0110-06-24
* **obecni:** Alan Bartozol, Diana Tevalier, Józef Małmałaz, Lucjusz Blakenbauer, Mariusz Trzewń, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Noktiański zabójca chciał pozyskać Cienia; pokonał Pięknotkę, ale Alan go zestrzelił. Potem zabójca złapał Chevaleresse i korzystając z niej jako zakładniczki wysłał Alana do szpitala. Pięknotka doszła do tego, jak się nazywa z pomocą Minerwy i Cienia.

Aktor w Opowieści:

* Dokonanie:
    * porwana przez Józefa, wykorzystana by Alan się poddał. Nie straciła przytomności umysłu - weszła w virt i zaalarmowała Pięknotkę o problemie.


### Kto wrobił Alana

* **uid:** 190830-kto-wrobil-alana, _numer względny_: 5
* **daty:** 0110-06-03 - 0110-06-05
* **obecni:** Alan Bartozol, Diana Tevalier, Karolina Erenit, Pięknotka Diakon, Talia Aegis, Wojciech Zermann

Streszczenie:

Ktoś wrabia Alana. Okazało się, że to nie Alan jest celem - to Chevaleresse. Podpadła kilku magom z Aurum w vircie i stwierdzili że ją nastraszą - ale najpierw muszą odsunąć Alana na bok. Pięknotka z Tymonem zastawili pułapkę współpracując z Chevaleresse jako przynętą. Udało się - magowie Aurum wpadli. Przy okazji - Karolina pomagała magom Aurum bo uznała, że to pomoże terminusom i Chevaleresse najbardziej. Karolina bowiem nienawidzi arystokracji magów.

Aktor w Opowieści:

* Dokonanie:
    * chciała się pochwalić artefaktem który posłużył do wrobienia Alana. Bardzo przeprosiła Talię i posłużyła jako przynętę przeciwko magom Aurum dla Pięknotki.


### Sabotaż szeptów Elizy

* **uid:** 190429-sabotaz-szeptow-elizy, _numer względny_: 4
* **daty:** 0110-04-10 - 0110-04-13
* **obecni:** Alan Bartozol, Aleksander Rugczuk, Diana Tevalier, Eliza Ira, Erwin Galilien, Karla Mrozik, Lucjusz Blakenbauer, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

Chevaleresse wpakowała się w "kult Elizy" grając z ludźmi online; powiedziała o tym Pięknotce. Pięknotka odkryła, że Eliza rozprzestrzenia swoje wpływy przez kryształy w formie biżuterii; wraz z Alanem, Erwinem i (nadal ranną) Minerwą zaprojektowali mechanizm uszkadzający sieć krystaliczną Elizy. Udało im się kupić jakiś miesiąc czasu, aż Orbiter i Pustogor znajdą lepsze rozwiązanie. Sama Chevaleresse ma się nie spotykać z takimi tam. Ale może z nimi grać online.

Aktor w Opowieści:

* Dokonanie:
    * maleństwo broniące wojskowych, straciła nerwy i uderzyła kolegę za co dostała lekki wpierdol; powiedziała Pięknotce o Elizie i ogólnie, jak na nią, wyjątkowo dorosła.


### Kurz po Ataienne

* **uid:** 190331-kurz-po-ataienne, _numer względny_: 3
* **daty:** 0110-03-07 - 0110-03-09
* **obecni:** Alan Bartozol, Ataienne, Diana Tevalier, Pięknotka Diakon, Romeo Węglas, Szymon Oporcznik

Streszczenie:

Alan poprosił Pięknotkę o interwencję w sprawie tego, że uziemił Chevaleresse; Pięknotka opanowała chcącą opuścić Alana Chevaleresse. Potem Pięknotka nawiązała użyteczny kontakt w Szymonie z Orbitera, znalazła osoby odpowiedzialne za próbę zabicia Ataienne i porozmawiała z samą Ataienne; uznała, że ta konkretna AI jest zdecydowanie dziwna.

Aktor w Opowieści:

* Dokonanie:
    * straciła zaufanie do Alana (bo ją uziemił). Rozważała opuszczenie tego terenu, ale Pięknotka ją przekonała do pozostania.


### Polowanie na Ataienne

* **uid:** 190330-polowanie-na-ataienne, _numer względny_: 2
* **daty:** 0110-03-03 - 0110-03-04
* **obecni:** Alan Bartozol, Ataienne, Atena Sowińska, Diana Tevalier, Lucjusz Blakenbauer, Pięknotka Diakon

Streszczenie:

Ataienne chciała zrobić koncert; Orbiter inkarnował ją w Zaczęstwie. Podczas koncertu Ataienne została zaatakowana i w jej obronie stanęła Chevaleresse, która aportowała broń Alana... Pięknotka wpierw zatrzymała tą wojnę, potem uratowała Ataienne od technovora a na końcu wydobyła siebie i Ataienne z rąk najemników którzy chcieli zniszczyć TAI. Trudny dzień; wszystko wskazuje na to, że za polowaniem na Ataienne stoi Eliza Ira?

Aktor w Opowieści:

* Dokonanie:
    * dzielnie walczyła z magami z Zaczęstwa broniąc Ataienne (sprowadzonym sprzętem Alana), za co ów ją uziemił (szlaban). Nie dała się, uciekła do multivirtu.


### Chevaleresse

* **uid:** 190217-chevaleresse, _numer względny_: 1
* **daty:** 0110-02-25 - 0110-02-27
* **obecni:** Alan Bartozol, Diana Tevalier, Karolina Erenit, Marlena Maja Leszczyńska, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Diana, aka Chevaleresse przybyła do Cyberszkoły szukać Alana. Postraszyła Marlenę i skończyła zaatakowana przez Tymona. Alan się zaopiekował swoją członkinią gildii; okazało się, że jego gildia (Elisquid) wpadła w złe ręce. Teraz Alan rozpaczliwie szuka sposobu na to by się odbanować; tymczasowo Chevaleresse zostaje u Alana... KTÓRY STAŁ SIĘ JEJ PRAWNYM OPIEKUNEM. Aha, Alan wie, że to wina Marleny że go zabanowali (a tak naprawdę jest Pięknotki).

Aktor w Opowieści:

* Dokonanie:
    * w grze "Chevaleresse", support, 16-latka a wygląda na 13. Przybyła na Astorię z Anathi by odnaleźć Alana, zaatakowała Marlenę, wpadła na Tymona i w końcu udało jej się odnaleźć Alana.
* Progresja:
    * Alan stał się jej prawnym opiekunem.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 13, @: 0111-10-13
    1. Primus    : 13, @: 0111-10-13
        1. Sektor Astoriański    : 13, @: 0111-10-13
            1. Astoria    : 13, @: 0111-10-13
                1. Sojusz Letejski    : 13, @: 0111-10-13
                    1. Szczeliniec    : 13, @: 0111-10-13
                        1. Powiat Jastrzębski    : 3, @: 0110-10-10
                            1. Jastrząbiec, okolice    : 1, @: 0110-07-27
                                1. Klinika Iglica    : 1, @: 0110-07-27
                                    1. Kompleks Itaran    : 1, @: 0110-07-27
                                1. TechBunkier Sarrat    : 1, @: 0110-07-27
                            1. Kalbark    : 2, @: 0110-08-01
                                1. Autoklub Piękna    : 1, @: 0110-08-01
                                1. Escape Room Lustereczko    : 1, @: 0110-07-27
                                1. Mini Barbakan    : 1, @: 0110-08-01
                            1. Praszalek, okolice    : 1, @: 0110-10-10
                                1. Fabryka schronień Defensor    : 1, @: 0110-10-10
                                1. Park rozrywki Janor    : 1, @: 0110-10-10
                            1. Praszalek    : 1, @: 0110-10-10
                                1. Komenda policji    : 1, @: 0110-10-10
                        1. Powiat Pustogorski    : 11, @: 0111-10-13
                            1. Majkłapiec    : 1, @: 0111-10-13
                                1. Kociarnia Zawtrak    : 1, @: 0111-10-13
                            1. Podwiert, obrzeża    : 1, @: 0110-03-09
                                1. Kosmoport    : 1, @: 0110-03-09
                            1. Podwiert, okolice    : 1, @: 0111-10-13
                                1. Fortifarma Lechotka    : 1, @: 0111-10-13
                            1. Podwiert    : 5, @: 0111-10-13
                                1. Dzielnica Luksusu Rekinów    : 2, @: 0111-09-19
                                    1. Fortyfikacje Rolanda    : 2, @: 0111-09-19
                                    1. Obrzeża Biedy    : 1, @: 0111-09-19
                                        1. Domy Ubóstwa    : 1, @: 0111-09-19
                                        1. Hotel Milord    : 1, @: 0111-09-19
                                        1. Stadion Lotników    : 1, @: 0111-09-19
                                        1. Stajnia Rumaków    : 1, @: 0111-09-19
                                    1. Sektor Brudu i Nudy    : 1, @: 0111-09-19
                                        1. Komputerownia    : 1, @: 0111-09-19
                                        1. Konwerter Magielektryczny    : 1, @: 0111-09-19
                                        1. Magitrownia Pogardy    : 1, @: 0111-09-19
                                        1. Skrytki Czereśniaka    : 1, @: 0111-09-19
                                    1. Serce Luksusu    : 2, @: 0111-09-19
                                        1. Apartamentowce Elity    : 1, @: 0111-09-19
                                        1. Arena Amelii    : 2, @: 0111-09-19
                                        1. Fontanna Królewska    : 1, @: 0111-09-19
                                        1. Kawiarenka Relaks    : 1, @: 0111-09-19
                                        1. Lecznica Rannej Rybki    : 1, @: 0111-09-19
                                1. Klub Arkadia    : 1, @: 0110-09-16
                                1. Kompleks Korporacyjny    : 1, @: 0111-10-13
                                    1. Dokumentarium Terminuskie    : 1, @: 0111-10-13
                                1. Osiedle Leszczynowe    : 1, @: 0110-03-04
                            1. Pustogor    : 4, @: 0110-06-24
                                1. Eksterior    : 1, @: 0110-04-13
                                    1. Miasteczko    : 1, @: 0110-04-13
                                1. Interior    : 1, @: 0110-04-13
                                    1. Bunkry Barbakanu    : 1, @: 0110-04-13
                                    1. Laboratorium Senetis    : 1, @: 0110-04-13
                                1. Miasteczko    : 2, @: 0110-03-09
                                1. Rdzeń    : 2, @: 0110-06-24
                                    1. Szpital Terminuski    : 2, @: 0110-06-24
                            1. Trzęsawisko Zjawosztup    : 1, @: 0111-10-13
                            1. Zaczęstwo    : 6, @: 0111-09-19
                                1. Akademia Magii, kampus    : 1, @: 0111-09-19
                                    1. Akademik    : 1, @: 0111-09-19
                                    1. Arena Treningowa    : 1, @: 0111-09-19
                                    1. Las Trzęsawny    : 1, @: 0111-09-19
                                1. Cyberszkoła    : 1, @: 0110-02-27
                                1. Nieużytki Staszka    : 2, @: 0110-06-24
                                1. Osiedle Ptasie    : 1, @: 0110-06-24
                                1. Złomowisko    : 1, @: 0110-03-04

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 10 | ((190217-chevaleresse; 190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 190429-sabotaz-szeptow-elizy; 190830-kto-wrobil-alana; 190901-polowanie-na-pieknotke; 200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku; 200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Alan Bartozol        | 9 | ((190217-chevaleresse; 190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 190429-sabotaz-szeptow-elizy; 190830-kto-wrobil-alana; 190901-polowanie-na-pieknotke; 200913-haracz-w-parku-rozrywki; 211221-chevaleresse-infiltruje-rekiny; 220814-stary-kot-a-trzesawisko)) |
| Ataienne             | 4 | ((190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Lucjusz Blakenbauer  | 4 | ((190330-polowanie-na-ataienne; 190429-sabotaz-szeptow-elizy; 190901-polowanie-na-pieknotke; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Mariusz Trzewń       | 4 | ((190901-polowanie-na-pieknotke; 200202-krucjata-chevaleresse; 200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Karolina Terienak    | 3 | ((211221-chevaleresse-infiltruje-rekiny; 220111-marysiowa-hestia-rekinow; 220814-stary-kot-a-trzesawisko)) |
| Minerwa Metalia      | 3 | ((190429-sabotaz-szeptow-elizy; 190901-polowanie-na-pieknotke; 200202-krucjata-chevaleresse)) |
| Hestia d'Rekiny      | 2 | ((211221-chevaleresse-infiltruje-rekiny; 220111-marysiowa-hestia-rekinow)) |
| Karolina Erenit      | 2 | ((190217-chevaleresse; 190830-kto-wrobil-alana)) |
| Marysia Sowińska     | 2 | ((211221-chevaleresse-infiltruje-rekiny; 220111-marysiowa-hestia-rekinow)) |
| Mateusz Kardamacz    | 2 | ((200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Melinda Teilert      | 2 | ((200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Talia Aegis          | 2 | ((190830-kto-wrobil-alana; 200202-krucjata-chevaleresse)) |
| Tymon Grubosz        | 2 | ((190217-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Adam Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Aleksander Rugczuk   | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Aleksandra Szklarska | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Ariela Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Atena Sowińska       | 1 | ((190330-polowanie-na-ataienne)) |
| Barnaba Burgacz      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Bartłomiej Małczarek | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Damian Orion         | 1 | ((200202-krucjata-chevaleresse)) |
| Diana Lemurczak      | 1 | ((200524-dom-dla-melindy)) |
| Eliza Ira            | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Ernest Namertel      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Erwin Galilien       | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Franciszek Owadowiec | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Franek Bulterier     | 1 | ((200524-dom-dla-melindy)) |
| Gerwazy Lemurczak    | 1 | ((200524-dom-dla-melindy)) |
| Iwan Zawtrak         | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Jeremi Sowiński      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Józef Małmałaz       | 1 | ((190901-polowanie-na-pieknotke)) |
| Justynian Diakon     | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Karla Mrozik         | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Karol Senonik        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Keira Amarco d'Namertel | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Keraina d'Orion      | 1 | ((200202-krucjata-chevaleresse)) |
| Kot Rozbójnik        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Krystyna Senonik     | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Laura Tesinik        | 1 | ((200524-dom-dla-melindy)) |
| Liliana Bankierz     | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Maja Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Malictrix d'Itaran   | 1 | ((200202-krucjata-chevaleresse)) |
| Marian Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Marlena Maja Leszczyńska | 1 | ((190217-chevaleresse)) |
| Melissa Durszenko    | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Olaf Zuchwały        | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Rafał Kamaron        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Romeo Węglas         | 1 | ((190331-kurz-po-ataienne)) |
| Rupert Mysiokornik   | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Santino Mysiokornik  | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Staś Arienik         | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Szymon Oporcznik     | 1 | ((190331-kurz-po-ataienne)) |
| Tomasz Tukan         | 1 | ((200202-krucjata-chevaleresse)) |
| Urszula Miłkowicz    | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Viirai d'Lechotka    | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Wojciech Zermann     | 1 | ((190830-kto-wrobil-alana)) |
| Żorż d'Namertel      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |