# Rafał Torszecki
## Identyfikator

Id: 2109-rafal-torszecki

## Sekcja Opowieści

### Akt, o którym Marysia nie wie

* **uid:** 211228-akt-o-ktorym-marysia-nie-wie, _numer względny_: 13
* **daty:** 0111-09-09 - 0111-09-12
* **obecni:** Daniel Terienak, Ernest Namertel, Karolina Terienak, Keira Amarco d'Namertel, Liliana Bankierz, Lorena Gwozdnik, Marysia Sowińska, Napoleon Bankierz, Rafał Torszecki

Streszczenie:

Torszecki ostrzegł Marysię, że jej akt krąży w Zaczęstwie. Ona to olała. Ale jak Ernest zaczął na niego polować to Marysia się zainteresowała. Zdecydowała się aktem odbić Ernesta Amelii. Zaflirtowała z Ernestem, obiecała mu akt specjalnie dla niego i że odda mu oryginał tego. Karo pokonała Napoleona, znalazła link do aktu który ostatecznie zdobył Torszecki. Daniel dla Karo zbił Torszeckiego i odzyskał akt. Marysia zaflirtowała z Ernestem ;-). Torszecki, który chciał dobrze i chronił Marysię skończył w szpitalu.

Aktor w Opowieści:

* Dokonanie:
    * ostrzegł Marysię przed tym że jej akt istnieje. Jak ta zignorowała, sam go zdobył od Liliany Bankierz. Zanim zdążył go zniszczyć, Daniel Terienak go pobił i mu zabrał ów akt.
* Progresja:
    * tydzień w szpitalu, pobity przez Daniela. Bo próbował ochronić Marysię przed jej aktem "na wolności".


### Waśń o ryby w Majkłapcu

* **uid:** 211127-waśń-o-ryby-w-majklapcu, _numer względny_: 12
* **daty:** 0111-08-19 - 0111-08-24
* **obecni:** Arkadia Verlen, Daniel Terienak, Genowefa Krecik, Iwan Zawtrak, Karolina Terienak, Ksenia Kirallen, Paweł Szprotka, Rafał Torszecki, Stella Amakirin

Streszczenie:

Daniel chciał rozwiązać problem w Majkłapcu, gdzie koty poraniły ryby. Okazało się, że to wina mafii, której właściciel kociarni nie chce płacić okupu. Karolina zebrała drużynę, uratowali zatrute wściekłością koty i zaatakowali siedzibę małego oddziałka mafii. Po zdobyciu dowodów (i ucieczce) przekazali temat Ksenii.

Aktor w Opowieści:

* Dokonanie:
    * nie znalazł dla Karo info o mafii i tylko podpadł mafii, ale zaproponował transponder co Karo przekształciła w wabik na koty. Dostarczył kompromitujące twarde narkotyki Danielowi, by ten mógł podłożyć je mafii. Pomógł Karo w manipulowaniu mafii.
* Progresja:
    * próbował znaleźć info o mafii i ich działaniach w kierunku lokalnych biznesów i mafia się zirytowała. Niech się nie interesuje. Ma problem z mafią.


### Odbudowa według Justyniana

* **uid:** 211123-odbudowa-wedlug-justyniana, _numer względny_: 11
* **daty:** 0111-08-11 - 0111-08-20
* **obecni:** Azalia Sernat d'Namertel, Daniel Terienak, Ernest Namertel, Ignacy Myrczek, Justynian Diakon, Karol Pustak, Karolina Terienak, Marysia Sowińska, Rafał Torszecki

Streszczenie:

Marysia się tymczasowo wprowadziła do Ernesta, by go trochę uspokoić i przekonać do Rekinów. Tymczasem Justynian Diakon przejął kontrolę operacyjną i zaczął odbudowywać Dzielnicę Rekinów po ruinie jaką zrobił tam Wiktor Satarail. Marysia skontaktowała się z Justynianem i przekazała mu plan Azalii d'Namertel jako swój, by odzyskać kontrolę i pozycję. Justynian zaakceptował jej plan i powiedział jej, że Torszeckiego biją. Do tego Marysię odwiedził Pustak jako herold Myrczka - prosi, by Marysia skontaktowała go z Sabiną Kazitan, miłością Myrczka...

Aktor w Opowieści:

* Dokonanie:
    * stawał w obronie czci Marysi, za co pobili go raz czy dwa. Justynian powiedział Marysi o jego nędznym losie.


### Satarail pomaga Marysi

* **uid:** 211102-satarail-pomaga-marysi, _numer względny_: 10
* **daty:** 0111-08-09 - 0111-08-10
* **obecni:** Ernest Namertel, Karolina Terienak, Lorena Gwozdnik, Marek Samszar, Marysia Sowińska, Rafał Torszecki, Sensacjusz Diakon, Wiktor Satarail

Streszczenie:

Satarail uderzył - zainfekował Owadem który wypełzł z Torszeckiego Samszara (który kiedyś ukradł somnibela Olgi). Zainfekowane Rekiny zaczęły dewastować dzielnicę. Karo skutecznie rozwala owady (i Lorenę), po czym Sensacjusza w glistę medyczną. Marysia samodzielnie leci jako owad wyłączyć Samszara. Torszecki jest rozgrzeszony - "nie był sobą". Ale polowanie na Torszeckiego czas zacząć.

Aktor w Opowieści:

* Dokonanie:
    * Owad Sataraila sprawił, że faktycznie nikt nie wini go za to co się stało. Ale nie jest ulubionym magiem nikogo ;-).
* Progresja:
    * eternianie WIEDZĄ, że był zainfekowany, ale NIE DBAJĄ o to. Sezon bicia Torszeckiego otwarty. Rekinom też to pasuje, bo Torszecki (plus ciekawe co Marysia).


### Koszt ratowania Torszeckiego

* **uid:** 211026-koszt-ratowania-torszeckiego, _numer względny_: 9
* **daty:** 0111-08-01 - 0111-08-05
* **obecni:** Ignacy Myrczek, Ksenia Kirallen, Marysia Sowińska, Olga Myszeczka, Paweł Szprotka, Rafał Torszecki, Sensacjusz Diakon, Teresa Mieralit, Wiktor Satarail

Streszczenie:

By ratować Torszeckiego, Marysia wchodzi w sojusz z Wiktorem Satarailem. On dał jej podskórnego robaka którego dostanie Torszecki, by uzasadnić dziwne zachowanie. Wiktor "dostanie swoją zapłatę" od "kogoś winnego". Marysia przekonała też Sensacjusza, że ona x Torszecki. Zaczęły się też pojawiać takie plotki...

Aktor w Opowieści:

* Dokonanie:
    * nie wiedział jak z tego z Ernestem wyjść, ale Marysia przyszła do niego z planem. Zaakceptował ten plan - wziął od niej robaka podskórnego. Musi go w końcu kochać.
* Progresja:
    * pojawiają się plotki na temat jego romansu z Marysią Sowińską. Na razie nie podsyca.
    * jest przekonany, że Marysia Sowińska się w nim podkochuje. Mówi mu groźne dla siebie sekrety i zaryzykowała nawet Sataraila by go ratować.


### Torszecki pokazał kręgosłup

* **uid:** 211012-torszecki-pokazal-kregoslup, _numer względny_: 8
* **daty:** 0111-07-29 - 0111-07-30
* **obecni:** Amelia Sowińska, Jolanta Sowińska, Karolina Terienak, Lucjan Sowiński, Marysia Sowińska, Rafał Torszecki, Sensacjusz Diakon

Streszczenie:

Marysia dostała nową "prośbę" ze strony Dworu Sowińskich - pozyskać próbkę krwi "córki Morlana" z Pustogoru. Tymczasem Karolina poszła do Torszeckiego (do lecznicy) by ten wreszcie miał kręgosłup. Wymusiła na nim pomoc w znalezieniu mordercy ścigacza. Torszecki nie chcąc krzywdy Karoliny się jej przyznał - to on. Chciał zrobić Ernest vs Mafia by chronić Marysię; skrzyżował w głowie, że Ernest x Amelia to Esuriit, więc to samo spotka biedną Marysię... Karo i Marysia mają teraz problem - jak to rozplątać?

Aktor w Opowieści:

* Dokonanie:
    * leżąc w szpitalu przyznał się Karolinie że to on zniszczył ścigacz. Nie chce, by Karolina ucierpiała z ręki Marysi tak jak on. Zrobił to by chronić Marysię przed miłością wywołaną przez Esuriit z ręki Ernesta. Powiązał (błędnie) fakty Ernest x Amelia za "Marysię czeka ten sam los".
* Progresja:
    * naprawdę szczerze LUBI Karolinę Terienak; by ją osłonić przed krzywdą przyznał jej się, że to on zniszczył ścigacz. Ona nie odwzajemnia przyjaźni.
    * nie ufa Marysi Sowińskiej. Jest jej ultralojalny, ale nie ufa jej. Planuje za jej plecami; dostał info z Rodu Sowińskich o Erneście i skrzyżował fakty Amelia x Ernest i dlatego chciał Marysię wesprzeć i ją chronić... i poszła katastrofa.


### Wysadzony żywy ścigacz

* **uid:** 210928-wysadzony-zywy-scigacz, _numer względny_: 7
* **daty:** 0111-07-26 - 0111-07-27
* **obecni:** Azalia Sernat d'Namertel, Ernest Namertel, Franek Bulterier, Karolina Terienak, Keira Amarco d'Namertel, Marysia Sowińska, Rafał Torszecki, Żorż d'Namertel

Streszczenie:

Ernest z Eterni okazał się być zupełnie inny niż się wydaje. Lekko naiwny, z paladyńskim zacięciem, chce czynić dobro i integrować Rekiny dookoła Marysi. Chce dokończyć dobre dzieło Amelii. Marysia znalazła z nim linię porozumienia a Ernest x Karolina są jak dwa łyse konie. Marysia przekonała go, by sam nic nie robił - nie rozumie terenu. Niestety, ktoś wysadził żywy ścigacz i zabił ducha z jego arkinu. Ernesta zatrzymała Marysia. Karolinie nie udało się ducha uratować. Dziewczyny zdecydowały, że znajdą mordercę...

Aktor w Opowieści:

* Dokonanie:
    * wpierw robił jako herold Marysi wzbudzając radość Ernesta, potem pomagał Marysi wydobyć od Ernesta wiedzę (jako błazen nie ze swojej winy). Doszedł do relacji Ernest - Amelia. Gdy Ernest stracił panowanie, zasłonił Marysię i dostał poważne rany.
* Progresja:
    * szczera nienawiść do Ernesta - wyśmiewał go, nie szanuje Marysi i ma większą świtę niż ona. Plus, ciężko go skrzywdził poniżej 5 sekund.
    * 5 dni regeneracji u Różewicza Diakona. Ernest zadaje straszne obrażenia.
    * zorientował się, że jest coś między Amelią Sowińską i Ernestem Namertelem. Wie też, że Ernest patrzy na Amelię jak na paladynkę. I wie, że Amelia to podła sucz.


### Przybycie Rekina z Eterni

* **uid:** 210921-przybycie-rekina-z-eterni, _numer względny_: 6
* **daty:** 0111-07-20 - 0111-07-25
* **obecni:** Amelia Sowińska, Arkadia Verlen, Ernest Namertel, Jolanta Sowińska, Karol Pustak, Karolina Terienak, Lucjan Sowiński, Marysia Sowińska, Nataniel Morlan, Rafał Torszecki, Tomasz Tukan

Streszczenie:

Do Rekinów dołącza tien z Eterni. Marysia dowiaduje się, że to kwestia intryg jej kuzynki, Amelii, próbującej deeskalować problemy na linii Morlan - Jolanta. Dowiedziała się od Amelii, że to nie Amelia stała za morderstwami w Podwiercie, kogoś chroni. Oraz... że Amelia jest zakochana (Amelia nie wie że Marysia wie). Ale na Marysię spadła ochrona Ernesta. Więc... usunęła wszystkie zagrożenia o których pomyślała (Arkadia) i zaczęła działania mające poprawić reputację Eternianina, by Rekiny go przyjęły.

Aktor w Opowieści:

* Dokonanie:
    * nieprawdopodobnie zazdrosny o świtę Ernesta Namertela; to Marysia Sowińska powinna mieć największą świtę wśród Rekinów. Przeprowadził PLANY by pozycja Marysi wróciła do najwyższego poziomu / punktu.


### Serafina staje za Wydrami

* **uid:** 210831-serafina-staje-za-wydrami, _numer względny_: 5
* **daty:** 0111-07-07 - 0111-07-10
* **obecni:** Daniel Terienak, Halina Sermniek, Karolina Terienak, Laura Tesinik, Lorena Gwozdnik, Marysia Sowińska, Rafał Torszecki, Serafina Ira

Streszczenie:

Karolina MUSI poznać prawdę co się stało z jej bratem. Odkryła, że istnieje grupa Wydry polująca na Rekiny, ludzi skrzywdzonych przez Rekiny w przeszłości. I pomaga im Serafina Ira. Z Marysią porozmawiały z Serafiną i przekonały ją, by ta działała wolniej - i drugą linią napuściły na Serafinę terminusów z Pustogoru.

Aktor w Opowieści:

* Dokonanie:
    * znalazł dla Marysi kontakt i namiar na Serafinę Irę. Oddał się Serafinie na zakładnika, by Marysia mogła z Serafiną porozmawiać.


### Porwanie Daniela Terienaka

* **uid:** 210720-porwanie-daniela-terienaka, _numer względny_: 4
* **daty:** 0111-06-14 - 0111-06-16
* **obecni:** Daniel Terienak, Karolina Terienak, Mariusz Trzewń, Marysia Sowińska, Rafał Torszecki, Tomasz Tukan, Urszula Miłkowicz

Streszczenie:

Urszula Miłkowicz zwabiła w pułapkę i porwała Daniela Terienaka - z nadzieją, że odkryje Rekina który współpracuje z mafią. Siostra Daniela, Karolina, pobiła Torszeckiego i tak spotkała się z Marysią. Wspólnie odkryły że za wszystkim stoi Ula i zmusiły ją do oddania Daniela bez przeskanowania pamięci (by się upewnić, Karolina wysadziła techbunker). Marysia dowiedziała się, że Ula jest uciekinierką z Aurum i eks-protomagiem.

Aktor w Opowieści:

* Dokonanie:
    * po tym jak ślady destrukcji magazynu Kacpra wskazywały na niego został skopany przez Karolinę Terienak. Wyjął na nią różdżkę kontra jej ścigacz i wywołał KOLEJNĄ anomalię...
* Progresja:
    * sterroryzowany przez Karolinę Terienak; jest tchórzem. Zrobi to co Karolina mu każe. Nie z miłości a ze strachu.


### Rekin wspiera mafię

* **uid:** 210713-rekin-wspiera-mafie, _numer względny_: 3
* **daty:** 0111-06-06 - 0111-06-09
* **obecni:** Julia Kardolin, Kacper Bankierz, Marysia Sowińska, Rafał Torszecki, Tomasz Tukan, Urszula Miłkowicz

Streszczenie:

Gabriel Ursus powiedział Marysi, że jakiś Rekin uczestniczy w przemycie nielegalnych rzeczy w Podwiercie, na terenie eksterytorialnym. I będzie rajd i przechwycą te Rekiny. Marysia dotarła do tego, że są tajne magazyny (chronione przez anomalię) pod kontrolą Kacpra Bankierza. Rozmowa z Kacprem nic nie dała, więc Marysia doprowadziła do uwolnienia Arkadii z więzienia i zrobiła fałszywe ślady - to Kacper stał za jej odurzeniem. Plus, Marysia odbudowała kontakt z Tukanem...

Aktor w Opowieści:

* Dokonanie:
    * wpierw działał jako detektyw (wykrył skrytkę Kacpra Bankierza x mafii) a potem jako szpieg (podłożył dowody świadczące że to Kacper otruł Arkadię). Lojalny Marysi.
* Progresja:
    * Kacper Bankierz uznał, że Torszecki to jego osobisty wróg za poszczucie go Arkadią.


### Verlenka na grzybkach

* **uid:** 210622-verlenka-na-grzybkach, _numer względny_: 2
* **daty:** 0111-05-30 - 0111-05-31
* **obecni:** Arkadia Verlen, Ignacy Myrczek, Julia Kardolin, Liliana Bankierz, Marek Samszar, Marysia Sowińska, Rafał Torszecki, Różewicz Diakon, Triana Porzecznik, Urszula Miłkowicz

Streszczenie:

Zespół AMZ składał projekt zaliczeniowy, gdy zaatakowała Arkadia Verlen na grzybkach ze swojego Lancera, uszkadzając projekt (robota). Julia wezwała Ulę (terminuskę) na pomoc, Ula unieszkodliwiła Marka (który podmienił Arkadię). Jako, że Julia i Triana były zajęte ratowaniem projektu, Marysia rzuciła się na znalezienie "kto atakuje Julię". Dotarła do niewinności Marka, dotarła do Arkadii i z pomocą Uli unieszkodliwiła Arkadię. Jedyne straty - ranna Ula i ranny Torszecki. Ale kto podał Arkadii te grzybki i jak?

Aktor w Opowieści:

* Dokonanie:
    * Rekin. Cel życiowy - być największym przydupasem Marysi Sowińskiej na świecie. Dość przydatny w pozyskiwaniu informacji, acz nie ma zupełnie inicjatywy. Wykonuje rozkazy, nie myśli proaktywnie. Skończył jako przynęta na Arkadię.


### Trzygłowiec kontra Melinda

* **uid:** 200513-trzyglowec-kontra-melinda, _numer względny_: 1
* **daty:** 0109-11-08 - 0109-11-17
* **obecni:** Adam Cześń, Diana Lemurczak, Feliks Keksik, Jan Łowicz, Katja Nowik, Kinga Kruk, Mariusz Grabarz, Melinda Teilert, Natasza Aniel, Rafał Torszecki

Streszczenie:

Mariusz Grabarz robi program mający podnieść swojego klienta (Rafała Torszeckiego) w oczach Aurum; przez to wkręca Trzygłowca, gang uciekinierów z Aurum w małą wojnę domową. Ma wszystkie pozwolenia. Gdy Melinda próbowała jednemu z Trzygłowców pomóc, wpadło to na Fantasmagorię. W wyniku Zespół poszedł na współpracę z Grabarzem, skompromitowali Podwiert ale zyskali pewne korzyści finansowe. Niestety (?), przy okazji Jan stracił Melindę jako stalkerkę.

Aktor w Opowieści:

* Dokonanie:
    * arystokrata Aurum, zapłacił Grabarzowi by podnieść swoją pozycję i reputację w Aurum.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 13, @: 0111-09-12
    1. Primus    : 13, @: 0111-09-12
        1. Sektor Astoriański    : 13, @: 0111-09-12
            1. Astoria    : 13, @: 0111-09-12
                1. Sojusz Letejski    : 13, @: 0111-09-12
                    1. Szczeliniec    : 13, @: 0111-09-12
                        1. Powiat Pustogorski    : 13, @: 0111-09-12
                            1. Czarnopalec    : 1, @: 0111-08-05
                                1. Pusta Wieś    : 1, @: 0111-08-05
                            1. Majkłapiec    : 1, @: 0111-08-24
                                1. Farma Krecik    : 1, @: 0111-08-24
                                1. Kociarnia Zawtrak    : 1, @: 0111-08-24
                                1. Wegefarma Myriad    : 1, @: 0111-08-24
                                1. Zakład Paprykarski Majkłapiec    : 1, @: 0111-08-24
                            1. Podwiert    : 12, @: 0111-09-12
                                1. Dzielnica Luksusu Rekinów    : 11, @: 0111-09-12
                                    1. Fortyfikacje Rolanda    : 1, @: 0111-08-20
                                    1. Obrzeża Biedy    : 4, @: 0111-08-20
                                        1. Domy Ubóstwa    : 2, @: 0111-08-20
                                        1. Hotel Milord    : 1, @: 0111-08-20
                                        1. Stadion Lotników    : 3, @: 0111-08-20
                                        1. Stajnia Rumaków    : 1, @: 0111-08-20
                                    1. Sektor Brudu i Nudy    : 3, @: 0111-08-20
                                        1. Komputerownia    : 1, @: 0111-08-20
                                        1. Konwerter Magielektryczny    : 1, @: 0111-08-20
                                        1. Magitrownia Pogardy    : 1, @: 0111-08-20
                                        1. Skrytki Czereśniaka    : 3, @: 0111-08-20
                                    1. Serce Luksusu    : 8, @: 0111-09-12
                                        1. Apartamentowce Elity    : 6, @: 0111-09-12
                                        1. Arena Amelii    : 1, @: 0111-08-20
                                        1. Fontanna Królewska    : 1, @: 0111-08-20
                                        1. Kawiarenka Relaks    : 1, @: 0111-08-20
                                        1. Lecznica Rannej Rybki    : 5, @: 0111-09-12
                                1. Las Trzęsawny    : 5, @: 0111-07-27
                                    1. Jeziorko Mokre    : 1, @: 0111-07-10
                                1. Osiedle Leszczynowe    : 1, @: 0109-11-17
                                    1. Klub Arkadia    : 1, @: 0109-11-17
                                    1. Sklep z reliktami Fantasmagoria    : 1, @: 0109-11-17
                                1. Osiedle Tęczy    : 1, @: 0111-07-10
                            1. Pustogor    : 2, @: 0111-08-10
                                1. Rdzeń    : 2, @: 0111-08-10
                                    1. Barbakan    : 1, @: 0111-05-31
                                        1. Kazamaty    : 1, @: 0111-05-31
                                    1. Szpital Terminuski    : 1, @: 0111-08-10
                            1. Zaczęstwo    : 4, @: 0111-09-12
                                1. Akademia Magii, kampus    : 2, @: 0111-09-12
                                    1. Akademik    : 2, @: 0111-09-12
                                    1. Arena Treningowa    : 1, @: 0111-09-12
                                    1. Las Trzęsawny    : 1, @: 0111-09-12
                                1. Las Trzęsawny    : 1, @: 0111-05-31
                                1. Nieużytki Staszka    : 1, @: 0111-05-31
                                1. Osiedle Ptasie    : 1, @: 0111-06-16

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 11 | ((210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka; 210831-serafina-staje-za-wydrami; 210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211012-torszecki-pokazal-kregoslup; 211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Karolina Terienak    | 9 | ((210720-porwanie-daniela-terienaka; 210831-serafina-staje-za-wydrami; 210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211012-torszecki-pokazal-kregoslup; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211127-waśń-o-ryby-w-majklapcu; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Daniel Terienak      | 5 | ((210720-porwanie-daniela-terienaka; 210831-serafina-staje-za-wydrami; 211123-odbudowa-wedlug-justyniana; 211127-waśń-o-ryby-w-majklapcu; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Ernest Namertel      | 5 | ((210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Arkadia Verlen       | 3 | ((210622-verlenka-na-grzybkach; 210921-przybycie-rekina-z-eterni; 211127-waśń-o-ryby-w-majklapcu)) |
| Ignacy Myrczek       | 3 | ((210622-verlenka-na-grzybkach; 211026-koszt-ratowania-torszeckiego; 211123-odbudowa-wedlug-justyniana)) |
| Lorena Gwozdnik      | 3 | ((210831-serafina-staje-za-wydrami; 211102-satarail-pomaga-marysi; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Sensacjusz Diakon    | 3 | ((211012-torszecki-pokazal-kregoslup; 211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi)) |
| Tomasz Tukan         | 3 | ((210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka; 210921-przybycie-rekina-z-eterni)) |
| Urszula Miłkowicz    | 3 | ((210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka)) |
| Amelia Sowińska      | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Azalia Sernat d'Namertel | 2 | ((210928-wysadzony-zywy-scigacz; 211123-odbudowa-wedlug-justyniana)) |
| Jolanta Sowińska     | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Julia Kardolin       | 2 | ((210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie)) |
| Karol Pustak         | 2 | ((210921-przybycie-rekina-z-eterni; 211123-odbudowa-wedlug-justyniana)) |
| Keira Amarco d'Namertel | 2 | ((210928-wysadzony-zywy-scigacz; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Ksenia Kirallen      | 2 | ((211026-koszt-ratowania-torszeckiego; 211127-waśń-o-ryby-w-majklapcu)) |
| Liliana Bankierz     | 2 | ((210622-verlenka-na-grzybkach; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Lucjan Sowiński      | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Marek Samszar        | 2 | ((210622-verlenka-na-grzybkach; 211102-satarail-pomaga-marysi)) |
| Paweł Szprotka       | 2 | ((211026-koszt-ratowania-torszeckiego; 211127-waśń-o-ryby-w-majklapcu)) |
| Wiktor Satarail      | 2 | ((211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi)) |
| Adam Cześń           | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Diana Lemurczak      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Feliks Keksik        | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Franek Bulterier     | 1 | ((210928-wysadzony-zywy-scigacz)) |
| Genowefa Krecik      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Halina Sermniek      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Iwan Zawtrak         | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Jan Łowicz           | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Justynian Diakon     | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Kacper Bankierz      | 1 | ((210713-rekin-wspiera-mafie)) |
| Katja Nowik          | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Kinga Kruk           | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Laura Tesinik        | 1 | ((210831-serafina-staje-za-wydrami)) |
| Mariusz Grabarz      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Mariusz Trzewń       | 1 | ((210720-porwanie-daniela-terienaka)) |
| Melinda Teilert      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Napoleon Bankierz    | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Nataniel Morlan      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Natasza Aniel        | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Olga Myszeczka       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Różewicz Diakon      | 1 | ((210622-verlenka-na-grzybkach)) |
| Serafina Ira         | 1 | ((210831-serafina-staje-za-wydrami)) |
| Stella Amakirin      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Teresa Mieralit      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Triana Porzecznik    | 1 | ((210622-verlenka-na-grzybkach)) |
| Żorż d'Namertel      | 1 | ((210928-wysadzony-zywy-scigacz)) |