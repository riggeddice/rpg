# Klasa Dyplomata
## Identyfikator

Id: 9999-klasa-dyplomata

## Sekcja Opowieści

### Relikwia z androida?!

* **uid:** 240214-relikwia-z-androida, _numer względny_: 6
* **daty:** 0107-05-16 - 0107-05-18
* **obecni:** Aerina Cavalis, Alina Mekran, Elwira Barknis, Kalista Surilik, Klasa Dyplomata, Klasa Hacker, Klasa Inżynier, Klasa Oficer Naukowy, Klasa Sabotażysta, Mawir Hong, Tadeusz Mekran, Vanessa d'Cavalis

Streszczenie:

Zniknęły szczątki Vanessy - bohaterskiego androida, wydane trzem biologom. Niewyjaśnione polecenie wydania szczątków Vanessy i niespodziewane pojawienie się fanatyków walczących w jej imieniu na podziemnych arenach, zainfekowanych niemożliwymi do zrobienia na tej stacji hipnonarkotykami, wskazało na problem z anomaliami magicznymi. Okazało się, że przyczyną było morderstwo Aliny Mekran przez Elwirę Barknis, której plagiat i agresywne metody w pracy wyszły na jaw. Alina, próbująca wrócić do życia jako "resurrector" za pomocą roślin i hydroponiki, potrzebowała energii, krwi i ciała. Agencja była w stanie zamaskować sytuację, zniszczyć zainfekowany orbitujący dookoła Szernief hydrolab H4 i zabrali Vanessę jako influencerkę turystyczną poza Stację.

Aktor w Opowieści:

* Dokonanie:
    * kampania informacyjna, która umożliwiła pomyślną ewakuację Vanessy, minimalizując szkody i zapewniając jej nową rolę jako ikony turystycznej. Też: negocjacje z 'shardem Aliny' wewnątrz TAI Szernief.


### Przeznaczeniem Szernief nie jest wojna domowa

* **uid:** 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa, _numer względny_: 5
* **daty:** 0105-12-11 - 0105-12-13
* **obecni:** Aerina Cavalis, Dorion Fughar, Felina Amatanir, Gabriel Septenas, Kaldor Czuk, Kalista Surilik, Klasa Dyplomata, Klasa Inżynier, Klasa Sabotażysta, Mawir Hong, Sia-03 Szernief

Streszczenie:

Na Stacji doszło do odprawienia savarańskiego rytuału Krwi dobrobytu w formie energii Fidetis. Problem w tym, że savaranie mają jedną wizję sukcesu a Szernief ma N grup. Więc każda grupa jest wzmacniana w _inny_ sposób, co powoduje ciekawe problemy (np. sabotaże by działali razem, lub savaranie atakujący Mawirowców by ci mogli być silniejsi). Agencja odkrywa obecność Łowcy (polującego na Radę) i go neutralizuje. Niestety, Tkaczka integruje się z Sią-03 tworząc nową frakcję.

Aktor w Opowieści:

* Dokonanie:
    * wyłapała różnice ideowe między drakolitami i doszła do radykalizacji. Podniosła pozycję Teraquid i dodała Teraquid zaufanie do Agencji. Nowa populacja i to przyjazna celowi.


### Sen chroniący kochanków

* **uid:** 231122-sen-chroniacy-kochankow, _numer względny_: 4
* **daty:** 0105-09-02 - 0105-09-05
* **obecni:** Damian Orczakin, Dorion Fughar, Felina Amatanir, Jola-09 Szernief, Kalista Surilik, Klasa Dyplomata, Klasa Hacker, Klasa Oficer Naukowy, Klaudiusz Widar, Mawir Hong, Rovis Skarun, Szymon Alifajrin

Streszczenie:

Na stacji CON Szernief konflikt między savaranami a drakolitami, podsycany przez inwestorów i stymulanty, prowadzi do zagrożenia pryzmatycznego. Dochodzi do manifestacji Esuriit i Alucis. Punktem kluczowym okazała się miłość Rovisa i Joli-09, prowadząca do rozprzestrzeniającego się Snu Ukojenia zwalczającego i zwalczanego przez Nienawiść Esuriit. Lux Umbrarum interweniuje, tworząc antidotum i przesiedlając kochanków. Kryzys został rozwiązany i stacja uspokojona tymczasowo, ale Kalista jest na tropie 'dziwnych rzeczy' a zaufanie populacji stacji do Inwestorów i Rady spada.

Aktor w Opowieści:

* Dokonanie:
    * Ola; wydobyła od Feliny że ta wie gdzie jest Rovis, ułagodziła napięcia na stacji i doprowadziła do miękkiej ewakuacji Rovisa i Joli ze stacji za recykler. Skutecznie zarządziła naprawą sytuacji.


### Polowanie na biosynty na Szernief

* **uid:** 231213-polowanie-na-biosynty-na-szernief, _numer względny_: 3
* **daty:** 0105-07-26 - 0105-07-31
* **obecni:** Artur Tavit, Delgado Vitriol, Felina Amatanir, Kalista Surilik, Klasa Biurokrata, Klasa Dyplomata, Klasa Hacker, Klasa Oficer Naukowy, Klasa Sabotażysta, Malik Darien, Sebastian-194, Vanessa d'Cavalis

Streszczenie:

Kalista wezwała Agencję bo biosynty są niszczone (ludzie zabijają je pierwsi) i dzieje się 'coś dziwnego' a 'człowiek Agencji' jest niekompetentny - okazało się, że jest Skażony Unumens (ma pasożyta). Agencja zrzuciła 'legalność' biosyntów na arystokratę Artura, zapewnia sobie współpracę z biosyntami i kieruje je do zniszczenia Parasekta Unumens. By odwrócić uwagę od swoich działań - zdradza Kalistę i Mawira (i wszystkie problemy zrzuca na nich). Udało się jednak większość uratować, mimo śmierci kilku osób w tak trudnej sytuacji.

Aktor w Opowieści:

* Dokonanie:
    * przekonanie Kalisty do współpracy, przekonanie szerszej populacji że szczepienie to jedyne wyjście (presja na Parasektowców), zrzucenie win z Agencji na Mawirowców i Kalistę.


### Tajemnicze Tunele Sebirialis

* **uid:** 231119-tajemnicze-tunele-sebirialis, _numer względny_: 2
* **daty:** 0104-11-20 - 0104-11-24
* **obecni:** Aniela Kafantelas, Eryk Kawanicz, Felina Amatanir, Ignatius Sozyliw, Janvir Krassus, Kalista Surilik, Klasa Dyplomata, Klasa Sabotażysta, Larkus Talvinir, Leon Hurmniow, Marta Sarilit, OLU Luminarius

Streszczenie:

W tunelach kopalni pod CON Szernief znikają ludzie. Agencja dochodzi do tego, że winny jest protomag Dotknięty Alteris, który uciekł z tajnej (dla populacji Szernief) placówki NavirMed gdzie prowadzi się badania na ludziach. Agencja doszła do łańcucha logicznego i odkryła prawdę; by to rozwiązać, ewakuowali kopalnię i placówkę NavirMed na 3 miesiące by protomag umarł z głodu.

Aktor w Opowieści:

* Dokonanie:
    * Sigwald Asidar z Lux Umbrarum; rozmawiając z dokerami dowiaduje się o blackships, z Radą o problemach, łapie powiązania kto-z-kim-kiedy i ogólnie robi świetną robotę śledczą. Praktycznie fraternizując się ze wszystkimi doszedł do tego że nikt nic nie wie, ale zapewnił ich współpracę.


### Echo z odmętów przeszłości

* **uid:** 240117-echo-z-odmetow-przeszlosci, _numer względny_: 1
* **daty:** 0096-01-24 - 0096-01-25
* **obecni:** Anna Ivanova, Elżbieta Sanchez, Gerald Barowiecki, Jędrzej Sanchez, Klasa Dyplomata, Klasa Inżynier, Klasa Oficer Ochrony, Robert Tisso

Streszczenie:

Eksperymentalna łódź podwodna Kelpie działająca w okolicach Trójzębu odkrywa podwodną stację badawczą sprzed pęknięcia, na której znajduje 'żywych' ludzi. Badając sprawę odkrywa anomalię, która ożywia zmarłych w obrębie bazy oraz utrzymuje urządzenia techniczne w działaniu, nawet jeśli nie powinny. Ostatecznie Kelpie przez oficerów nawiązuje współpracę ze stacją.

Aktor w Opowieści:

* Dokonanie:
    * przekonuje Karolinę, że na Kelpie znajduje się laboratorium, którego mogłaby użyć, przekonuje kapitana Barowieckiego do swojego pomysłu na pomoc stacji i wcielenie jej do organizacji, przekonuje Elżbietę do współpracy oraz Annę, by pracowała dla nich, dając jej nowy sens życia.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 6, @: 0107-05-18
    1. Primus    : 6, @: 0107-05-18
        1. Sektor Astoriański    : 1, @: 0096-01-25
            1. Astoria    : 1, @: 0096-01-25
                1. Wielkie Jezioro Indygo    : 1, @: 0096-01-25
                    1. Obszar Sojuszu Letejskiego    : 1, @: 0096-01-25
                        1. Podwodna Stacja Badawcza Medimmortal    : 1, @: 0096-01-25
                        1. Rafa Prisma    : 1, @: 0096-01-25
                        1. Rów Penelope    : 1, @: 0096-01-25
        1. Sektor Kalmantis    : 5, @: 0107-05-18
            1. Sebirialis, orbita    : 5, @: 0107-05-18
                1. CON Szernief    : 5, @: 0107-05-18
                    1. Orbitujące stacje hydroponiczne    : 1, @: 0107-05-18
                    1. Powłoka Wewnętrzna    : 2, @: 0105-09-05
                        1. Poziom Minus Dwa    : 2, @: 0105-09-05
                            1. Więzienie    : 2, @: 0105-09-05
                        1. Poziom Minus Jeden    : 2, @: 0105-09-05
                            1. Obszar Mieszkalny Savaran    : 2, @: 0105-09-05
                        1. Poziom Minus Trzy    : 2, @: 0105-09-05
                            1. Wielkie Obrady    : 2, @: 0105-09-05
                    1. Powłoka Zewnętrzna    : 1, @: 0105-09-05
                        1. Panele Słoneczne    : 1, @: 0105-09-05
            1. Sebirialis    : 1, @: 0104-11-24
                1. Krater Ablardius    : 1, @: 0104-11-24
                    1. Kopalnie Ablardius    : 1, @: 0104-11-24
                1. Płaskowyż Zaitrus    : 1, @: 0104-11-24
                    1. Ukryta Baza NavirMed    : 1, @: 0104-11-24

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kalista Surilik      | 5 | ((231119-tajemnicze-tunele-sebirialis; 231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240214-relikwia-z-androida)) |
| Felina Amatanir      | 4 | ((231119-tajemnicze-tunele-sebirialis; 231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klasa Sabotażysta    | 4 | ((231119-tajemnicze-tunele-sebirialis; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240214-relikwia-z-androida)) |
| Klasa Hacker         | 3 | ((231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 240214-relikwia-z-androida)) |
| Klasa Inżynier       | 3 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240117-echo-z-odmetow-przeszlosci; 240214-relikwia-z-androida)) |
| Klasa Oficer Naukowy | 3 | ((231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 240214-relikwia-z-androida)) |
| Mawir Hong           | 3 | ((231122-sen-chroniacy-kochankow; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240214-relikwia-z-androida)) |
| Aerina Cavalis       | 2 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240214-relikwia-z-androida)) |
| Dorion Fughar        | 2 | ((231122-sen-chroniacy-kochankow; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Vanessa d'Cavalis    | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240214-relikwia-z-androida)) |
| Alina Mekran         | 1 | ((240214-relikwia-z-androida)) |
| Aniela Kafantelas    | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Anna Ivanova         | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Artur Tavit          | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Damian Orczakin      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Delgado Vitriol      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Elwira Barknis       | 1 | ((240214-relikwia-z-androida)) |
| Elżbieta Sanchez     | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Eryk Kawanicz        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Gabriel Septenas     | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Gerald Barowiecki    | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Ignatius Sozyliw     | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Janvir Krassus       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Jędrzej Sanchez      | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Jola-09 Szernief     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Kaldor Czuk          | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klasa Biurokrata     | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Klasa Oficer Ochrony | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Klaudiusz Widar      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Larkus Talvinir      | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Leon Hurmniow        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Malik Darien         | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Marta Sarilit        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| OLU Luminarius       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Robert Tisso         | 1 | ((240117-echo-z-odmetow-przeszlosci)) |
| Rovis Skarun         | 1 | ((231122-sen-chroniacy-kochankow)) |
| Sebastian-194        | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Sia-03 Szernief      | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Szymon Alifajrin     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Tadeusz Mekran       | 1 | ((240214-relikwia-z-androida)) |