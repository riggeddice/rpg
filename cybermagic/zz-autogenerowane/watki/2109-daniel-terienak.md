# Daniel Terienak
## Identyfikator

Id: 2109-daniel-terienak

## Sekcja Opowieści

### Ten nawiedzany i ta ukryta

* **uid:** 230325-ten-nawiedzany-i-ta-ukryta, _numer względny_: 13
* **daty:** 0111-10-16 - 0111-10-17
* **obecni:** Aleksandra Burgacz, Daniel Terienak, Franek Bulterier, Karolina Terienak, Lea Samszar, Michał Klabacz, Nadia Uprewien, Oliwier Czepek, Rupert Mysiokornik

Streszczenie:

Ola Burgacz, poślednia tienka zacnego rodu zorganizowała wyścig mający drażnić lokalsów Podwiertu, który skończył się: zwycięstwem Daniela, rozbiciem Mysiokornika (ktoś zatruł jego paliwo) i całusem Oli do Bulteriera. Tymczasem do Karo przyszedł centuś Oliwier prosić o pomoc dla pracownika, Michała, który jest "przeklęty". Karo i Michał weszli mu na chatę, znaleźli ślady magii ale co ważne nie na samym Michale. By pozbyć się wścibskiej sąsiadki, podłożyli jej szczura.

Okazuje się, że Ola dokuczała Michałowi i ów zrobił artefakt który podobno szedł z planów Triany (nonsens) i który nie miał prawa działać - a działa. Ola chciała mu potem pomóc, ale nie umiała. Za wszystkim stoi Lea Samszar, która lubiła Michała ale dla utrzymania swojej reputacji przed bardziej podłymi Rekinami poświęciła Michała w rytuale. Gdy nie wiedzący o tym Daniel i Karo dotarli do Lei i przywieźli jej nieprzytomnego Michała po ataku efemerycznego horroru, Lea podziękowała i powiedziała że mu pomoże. Jak inne REKINY poprosiły ją o pomoc, miała pretekst by mu pomóc.

A przy okazji, przypadkiem, Karo i Daniel sprowadzili na Podwiert plagę magicznych szczurów za które obwiniona jest AMZ...

Aktor w Opowieści:

* Dokonanie:
    * Rekin; master tracker, dobry w pułapkach i mistrz walki wręcz prowadzący videobloga; tu przydała się jego kataliza detekcji. Wygrał wyścig  O dziwo, został dyplomatą mediując między Leą i Karo jak te się gryzły o protokół XD. Źródło Plagi Szczurów w Podwiercie, za które obwinione będzie AMZ.


### Tank as a love letter

* **uid:** 220819-tank-as-a-love-letter, _numer względny_: 12
* **daty:** 0111-10-07 - 0111-10-08
* **obecni:** Ariel Kubunczak, Arkadiusz Terienak, AU Flara Astorii, Daniel Terienak, Henryk Wkrąż, Iwona Perikas, Izabela Selentik, Michał Kabarniec, Mimoza Diakon, Talia Mikrit

Streszczenie:

The truck infected with Esuriit led to Mimosa's site. Henryk (Shark) was supposed to be a support, but he told he took over - because of Iwona Perikas, his GF. The team unveiled that Iwona was the one to throw in the Esuriit-infected stuff and she was trying to use Henryk to get to Daniel whom she loves. The Team turned it around and cooperated with Henryk to lure Daniel and Iwona. But... Daniel was not the problematic one - when he heard of 'Astorian Flare' (esuriit-hovertank) he ordered other Sharks to stop everything. There are some mafia aspects here too... fortunately, Mimosa's agents were undetected.

Aktor w Opowieści:

* Dokonanie:
    * Rekin; Shark; Henryk summoned him to confront him about Iwona Perikas. He was unaware of AU Flara Astorii being found and stopped them from doing stupid stuff and unearthing the Esuriit anomaly. He beat Henryk up and broke all contact with Iwona.
* Progresja:
    * broke all contact with Iwona Perikas; she might love him, but she is despicable - hurting Henryk Wkrąż and reanimating Flara Astorii...


### Supersupertajny plan Loreny

* **uid:** 220730-supersupertajny-plan-loreny, _numer względny_: 11
* **daty:** 0111-09-30 - 0111-10-04
* **obecni:** Arkadia Verlen, Daniel Terienak, Karolina Terienak, Liliana Bankierz, Marsen Gwozdnik, Władysław Owczarek, Żorż d'Namertel

Streszczenie:

Marsen Gwozdnik wszedł na teren by pomóc Lorenie (która nie wie) uważając Lorenę za najlepszego taktyka na świecie (którym nie jest). Wyedytował część kontraktów okolicznych firm z Eternią, by spowodować niechęć firm do Eterni (a Ernest próbuje pomóc by Mimoza nie była jedyną która pomaga). Karo zastawiła na Marsena pułapkę i wzięła jako wsparcie Arkadię. Marsen natomiast wziął Lancera i Lilianę. Skończyło się na rannej Arkadii, sprzedanym Lancerze, rannej Lilianie i tym, że Karo przekonała Marsenę że jest... agentką Loreny?

Aktor w Opowieści:

* Dokonanie:
    * pomógł siostrze zastawić pułapkę na osoby atakujące mały biznes podwiercki (bo siostra); pokonał kilku ludzi w walce wręcz, demoralizując ich.


### Akt, o którym Marysia nie wie

* **uid:** 211228-akt-o-ktorym-marysia-nie-wie, _numer względny_: 10
* **daty:** 0111-09-09 - 0111-09-12
* **obecni:** Daniel Terienak, Ernest Namertel, Karolina Terienak, Keira Amarco d'Namertel, Liliana Bankierz, Lorena Gwozdnik, Marysia Sowińska, Napoleon Bankierz, Rafał Torszecki

Streszczenie:

Torszecki ostrzegł Marysię, że jej akt krąży w Zaczęstwie. Ona to olała. Ale jak Ernest zaczął na niego polować to Marysia się zainteresowała. Zdecydowała się aktem odbić Ernesta Amelii. Zaflirtowała z Ernestem, obiecała mu akt specjalnie dla niego i że odda mu oryginał tego. Karo pokonała Napoleona, znalazła link do aktu który ostatecznie zdobył Torszecki. Daniel dla Karo zbił Torszeckiego i odzyskał akt. Marysia zaflirtowała z Ernestem ;-). Torszecki, który chciał dobrze i chronił Marysię skończył w szpitalu.

Aktor w Opowieści:

* Dokonanie:
    * wezwany awaryjnie przez siostrę do wyśledzenia Torszeckiego i jego kryjówek by zdobyć akt Marysi Sowińskiej zanim on go zniszczy. Znalazł Torszeckiego, ale by go zatrzymać musiał go ciężko zbić.


### Gdy zabraknie prądu Rekinom

* **uid:** 211207-gdy-zabraknie-pradu-rekinom, _numer względny_: 9
* **daty:** 0111-08-30 - 0111-08-31
* **obecni:** Arkadia Verlen, Arnold Kłaczek, Daniel Terienak, Henryk Wkrąż, Hestia d'Rekiny, Karolina Terienak, Lorena Gwozdnik, Marysia Sowińska, Natalia Tessalon, Sensacjusz Diakon, Urszula Arienik

Streszczenie:

Ponieważ Amelia nie przekazała Marysi informacji o tym, że ta powinna przejąć obowiązki zarządcze to Marysia jako administratorka nie dostawała informacji. Przez to odcięło im prąd. Marysia przywróciła prąd podpisując (standardową) umowę z dystrybutorem podwierckim a Karolina uratowała ludzką część Dzielnicy Rekinów od pobicia przez wkurzonych magów. Próba obniżenia rachunków przez współpracę z mafią została przez dziewczyny zażegnana używając Arkadii.

Aktor w Opowieści:

* Dokonanie:
    * miał się bić z Bulterierem na arenie, ale padł prąd. Potem pomagał Karo ciągnąć kable i jak Karo była narażona na walkę z Rekinami, powiedział, że siostra to świętość. Walka się nie odbyła.


### Waśń o ryby w Majkłapcu

* **uid:** 211127-waśń-o-ryby-w-majklapcu, _numer względny_: 8
* **daty:** 0111-08-19 - 0111-08-24
* **obecni:** Arkadia Verlen, Daniel Terienak, Genowefa Krecik, Iwan Zawtrak, Karolina Terienak, Ksenia Kirallen, Paweł Szprotka, Rafał Torszecki, Stella Amakirin

Streszczenie:

Daniel chciał rozwiązać problem w Majkłapcu, gdzie koty poraniły ryby. Okazało się, że to wina mafii, której właściciel kociarni nie chce płacić okupu. Karolina zebrała drużynę, uratowali zatrute wściekłością koty i zaatakowali siedzibę małego oddziałka mafii. Po zdobyciu dowodów (i ucieczce) przekazali temat Ksenii.

Aktor w Opowieści:

* Dokonanie:
    * chce pomóc domowej sentisieci, więc zainteresował się pomocą morderców ryb w Majkłapcu (by się zbliżyć do czarodziejki z AMZ). Złożył czar z Pawłem Szprotką - artefakt szukający kotów. Doszedł do tego, że Iwan Zawtrak nie mówi całej prawdy; wszedł na twardo i jakkolwiek dostał cios, to prawda wyszła na jaw. Włamał się skutecznie do mafii i podłożył im narkotyki.
* Progresja:
    * w Majkłapcu jest lubiany przez Iwana Zawtraka z Kociarni i ma jego zaufanie a Genowefa z Farmy Krecik nim gardzi.


### Odbudowa według Justyniana

* **uid:** 211123-odbudowa-wedlug-justyniana, _numer względny_: 7
* **daty:** 0111-08-11 - 0111-08-20
* **obecni:** Azalia Sernat d'Namertel, Daniel Terienak, Ernest Namertel, Ignacy Myrczek, Justynian Diakon, Karol Pustak, Karolina Terienak, Marysia Sowińska, Rafał Torszecki

Streszczenie:

Marysia się tymczasowo wprowadziła do Ernesta, by go trochę uspokoić i przekonać do Rekinów. Tymczasem Justynian Diakon przejął kontrolę operacyjną i zaczął odbudowywać Dzielnicę Rekinów po ruinie jaką zrobił tam Wiktor Satarail. Marysia skontaktowała się z Justynianem i przekazała mu plan Azalii d'Namertel jako swój, by odzyskać kontrolę i pozycję. Justynian zaakceptował jej plan i powiedział jej, że Torszeckiego biją. Do tego Marysię odwiedził Pustak jako herold Myrczka - prosi, by Marysia skontaktowała go z Sabiną Kazitan, miłością Myrczka...

Aktor w Opowieści:

* Dokonanie:
    * cieszy się, bo Justynian zmontował Arenę Amelii i będą walki ludzi (gladiatorów). Z przyjemnością mówi siostrze co się dzieje i pomaga w odbudowie Justynianowi. Nie chroni przed Karoliną Loreny - jeśli postawiła się jego siostrze to słusznie dostała.


### Serafina staje za Wydrami

* **uid:** 210831-serafina-staje-za-wydrami, _numer względny_: 6
* **daty:** 0111-07-07 - 0111-07-10
* **obecni:** Daniel Terienak, Halina Sermniek, Karolina Terienak, Laura Tesinik, Lorena Gwozdnik, Marysia Sowińska, Rafał Torszecki, Serafina Ira

Streszczenie:

Karolina MUSI poznać prawdę co się stało z jej bratem. Odkryła, że istnieje grupa Wydry polująca na Rekiny, ludzi skrzywdzonych przez Rekiny w przeszłości. I pomaga im Serafina Ira. Z Marysią porozmawiały z Serafiną i przekonały ją, by ta działała wolniej - i drugą linią napuściły na Serafinę terminusów z Pustogoru.

Aktor w Opowieści:

* Dokonanie:
    * ma podejrzanie dużo "przyjaciółek" w okolicy. Kiepska nawijka. Prowadzi prywatnego wideobloga. Dzięki temu udało się namierzyć Serafinę Irę - po jej muzyce. Acz siorka płakała jak bloga oglądała...


### Mandragora nienawidzi Rekinów

* **uid:** 210824-mandragora-nienawidzi-rekinow, _numer względny_: 5
* **daty:** 0111-07-03 - 0111-07-05
* **obecni:** Amelia Sowińska, Daniel Terienak, Ekaterina Zajcew, Karolina Terienak, Laura Tesinik, Marysia Sowińska, Sensacjusz Diakon, Tomasz Tukan

Streszczenie:

Mandragora nienawidzi Rekinów i jej ofiarą został Damian Terienak. Idąc za jego śladem Marysia i Karolina znalazły mandragorę i pnączoszpona oraz z pomocą dwóch uczennic terminusa rozwaliły problem. Dowiedziały się, że mandragora konkretnie celuje w Amelię Sowińską, która opuściła ten teren dawno temu (i która była dość okrutna).

Aktor w Opowieści:

* Dokonanie:
    * wpadł na mandragorę która nienawidzi Rekinów. Udało mu się przesunąć swoje Skażenie z "rekiny" na "mafia", by chronić siostrę.
* Progresja:
    * nieaktywny przez następne 3 dni (regeneracja)


### Porwanie Daniela Terienaka

* **uid:** 210720-porwanie-daniela-terienaka, _numer względny_: 4
* **daty:** 0111-06-14 - 0111-06-16
* **obecni:** Daniel Terienak, Karolina Terienak, Mariusz Trzewń, Marysia Sowińska, Rafał Torszecki, Tomasz Tukan, Urszula Miłkowicz

Streszczenie:

Urszula Miłkowicz zwabiła w pułapkę i porwała Daniela Terienaka - z nadzieją, że odkryje Rekina który współpracuje z mafią. Siostra Daniela, Karolina, pobiła Torszeckiego i tak spotkała się z Marysią. Wspólnie odkryły że za wszystkim stoi Ula i zmusiły ją do oddania Daniela bez przeskanowania pamięci (by się upewnić, Karolina wysadziła techbunker). Marysia dowiedziała się, że Ula jest uciekinierką z Aurum i eks-protomagiem.

Aktor w Opowieści:

* Dokonanie:
    * chciał sobie dorobić u Kacpra Bankierza i poszedł na współpracę z mafią. Ula Miłkowicz go wymanewrowała, porwała i zaczęła skan. Uratowany przez siostrę (Karolinę).


### Karolina w Ciężkim Młocie

* **uid:** 220101-karolina-w-ciezkim-mlocie, _numer względny_: 3
* **daty:** 0111-04-25 - 0111-05-05
* **obecni:** Daniel Terienak, Henryk Murkot, Karolina Terienak, Mimoza Elegancja Diakon, Władysław Owczarek

Streszczenie:

Daniel chciał wygrać zakład z Mysiokornikiem o Mimozę Diakon i alkohol w barze Ciężki Młot. Karo chciała mu pomóc, więc poszła do tego baru rozeznać sprawę. Nie jest akceptowana przez ludzi pracy i próbuje ową akceptację uzyskać. Daniel chce jej pomóc więc robi artefaktyczną zaklątwioną lodówkę; Karo ratuje zakład recyklingowy i jest zaakceptowana w Młocie. Potem opieprza Daniela i Daniel rezygnuje z zakładu z Mysiokornikiem, nieszczęśliwy.

Aktor w Opowieści:

* Dokonanie:
    * chciał wygrać zakład z Mysiokornikiem, więc Karo próbowała wbić się do knajpy. Gdy nie miała akceptacji, sprowokował anomaliczny problem, dzięki czemu Karo pomogła i została zaakceptowana. Po wściekłej Karolinie zrezygnował z zakładu i ma ogólnie ponury nastrój.


### Dziewczyna i pies

* **uid:** 201215-dziewczyna-i-pies, _numer względny_: 2
* **daty:** 0110-11-15 - 0110-11-17
* **obecni:** Andrzej Kuncerzyk, Daniel Terienak, Franciszek Zygmunt, Grzegorz Terienak, Izydor Grumczewicz, Karolina Terienak, Patrycja Radniak, Paulina Mordoch, Tadeusz Łaśnic

Streszczenie:

Patrycja, coraz bardziej opętana przez Lucka/Esuriit pragnie odzyskać rodzinę. Znalazła "rodzinę" w Małopsie, gdzie "przygarnęła" Paulinę i Andrzeja, za cichym zezwoleniem tajemniczych władców Małopsa (którzy chcieli ją naprawić). Tymczasem trzy Rekiny rodu Terieniak poleciały do Małopsa odzyskać swojego kolegę, Izydora, i skończyło się na tym, że wyciągnęły "rodziców" Patrycji z jej mgły mentalnej i zmusiły Patrycję do ucieczki na mokradła.

Aktor w Opowieści:

* Dokonanie:
    * nie waha się użyć tortur by osiągnąć cel; wyrwał Paulinę i Andrzeja z mgły mentalnej wygenerowanej przez Paulinę. Zero sympatii i współczucia wobec ludzi.


### Impreza w Małopsie

* **uid:** 201201-impreza-w-malopsie, _numer względny_: 1
* **daty:** 0110-11-04 - 0110-11-06
* **obecni:** Andrzej Kuncerzyk, Barnaba Burgacz, Cezary Urmaszcz, Daniel Terienak, Franciszek Zygmunt, Izydor Grumczewicz, Karolina Terienak, Paulina Mordoch, Tadeusz Łaśnic

Streszczenie:

W Małopsie egzorcysta-amator zainfekowany demonem próbował zrobić doom-metalowy pesymistyczny koncert. To jest nielegalne (Pryzmat: tyrania optymizmu). Rekiny, które się pojawiły by to rozwiązać przeszły przez temat używając pięści - usunęły demona, zmieniły go w broń i oddały to Grzymościowcom (za to, że Wolny Uśmiech tu posprząta po nich). Ale skąd tu demon?

Aktor w Opowieści:

* Dokonanie:
    * Rekin. Dziąsłowiec i biedaartefaktor, który nie lubi jak ktoś lekceważy kobiety. Idzie prosto do celu pięścią i agresją. Stworzył "z demona" artefakt, który oddał Grzymościowcom za to, że oni posprzątają po Rekinach.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 13, @: 0111-10-17
    1. Primus    : 13, @: 0111-10-17
        1. Sektor Astoriański    : 13, @: 0111-10-17
            1. Astoria    : 13, @: 0111-10-17
                1. Sojusz Letejski    : 13, @: 0111-10-17
                    1. Powiat Jastrzębski    : 2, @: 0110-11-17
                        1. Małopies, okolice    : 1, @: 0110-11-17
                            1. Mokradła    : 1, @: 0110-11-17
                        1. Małopies    : 2, @: 0110-11-17
                            1. Hodowla psów    : 1, @: 0110-11-06
                            1. Motel    : 1, @: 0110-11-06
                            1. Stadion sportowy    : 1, @: 0110-11-06
                    1. Szczeliniec    : 11, @: 0111-10-17
                        1. Powiat Pustogorski    : 11, @: 0111-10-17
                            1. Czółenko, okolice    : 1, @: 0111-10-08
                                1. Las Trzęsawny    : 1, @: 0111-10-08
                            1. Czółenko    : 1, @: 0111-08-31
                                1. Generatory Keriltorn    : 1, @: 0111-08-31
                            1. Majkłapiec    : 1, @: 0111-08-24
                                1. Farma Krecik    : 1, @: 0111-08-24
                                1. Kociarnia Zawtrak    : 1, @: 0111-08-24
                                1. Wegefarma Myriad    : 1, @: 0111-08-24
                                1. Zakład Paprykarski Majkłapiec    : 1, @: 0111-08-24
                            1. Podwiert, okolice    : 1, @: 0111-10-08
                                1. Oczyszczalnia Słonecznik    : 1, @: 0111-10-08
                            1. Podwiert    : 9, @: 0111-10-17
                                1. Dzielnica Luksusu Rekinów    : 6, @: 0111-09-12
                                    1. Fortyfikacje Rolanda    : 1, @: 0111-08-20
                                    1. Obrzeża Biedy    : 2, @: 0111-08-20
                                        1. Domy Ubóstwa    : 1, @: 0111-08-20
                                        1. Hotel Milord    : 1, @: 0111-08-20
                                        1. Stadion Lotników    : 2, @: 0111-08-20
                                        1. Stajnia Rumaków    : 1, @: 0111-08-20
                                    1. Sektor Brudu i Nudy    : 3, @: 0111-08-31
                                        1. Komputerownia    : 2, @: 0111-08-31
                                        1. Konwerter Magielektryczny    : 1, @: 0111-08-20
                                        1. Magitrownia Pogardy    : 1, @: 0111-08-20
                                        1. Skrytki Czereśniaka    : 2, @: 0111-08-20
                                    1. Serce Luksusu    : 4, @: 0111-09-12
                                        1. Apartamentowce Elity    : 2, @: 0111-09-12
                                        1. Arena Amelii    : 2, @: 0111-08-31
                                        1. Fontanna Królewska    : 1, @: 0111-08-20
                                        1. Kawiarenka Relaks    : 1, @: 0111-08-20
                                        1. Lecznica Rannej Rybki    : 3, @: 0111-09-12
                                1. Kompleks Korporacyjny    : 4, @: 0111-10-17
                                    1. Bar Ciężki Młot    : 2, @: 0111-10-17
                                    1. Dystrybutor Prądu Ozitek    : 1, @: 0111-08-31
                                    1. Elektrownia Węglowa Szarpien    : 1, @: 0111-08-31
                                    1. Zakład Recyklingu Owczarek    : 2, @: 0111-10-04
                                1. Las Trzęsawny    : 3, @: 0111-07-10
                                    1. Jeziorko Mokre    : 2, @: 0111-07-10
                                1. Osiedle Rdzawych Dębów    : 1, @: 0111-10-17
                                    1. Sklep Eliksir Siekiery    : 1, @: 0111-10-17
                                1. Osiedle Tęczy    : 1, @: 0111-07-10
                            1. Zaczęstwo    : 2, @: 0111-09-12
                                1. Akademia Magii, kampus    : 1, @: 0111-09-12
                                    1. Akademik    : 1, @: 0111-09-12
                                    1. Arena Treningowa    : 1, @: 0111-09-12
                                    1. Las Trzęsawny    : 1, @: 0111-09-12
                                1. Osiedle Ptasie    : 1, @: 0111-06-16

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolina Terienak    | 12 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies; 210720-porwanie-daniela-terienaka; 210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami; 211123-odbudowa-wedlug-justyniana; 211127-waśń-o-ryby-w-majklapcu; 211207-gdy-zabraknie-pradu-rekinom; 211228-akt-o-ktorym-marysia-nie-wie; 220101-karolina-w-ciezkim-mlocie; 220730-supersupertajny-plan-loreny; 230325-ten-nawiedzany-i-ta-ukryta)) |
| Marysia Sowińska     | 6 | ((210720-porwanie-daniela-terienaka; 210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami; 211123-odbudowa-wedlug-justyniana; 211207-gdy-zabraknie-pradu-rekinom; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Rafał Torszecki      | 5 | ((210720-porwanie-daniela-terienaka; 210831-serafina-staje-za-wydrami; 211123-odbudowa-wedlug-justyniana; 211127-waśń-o-ryby-w-majklapcu; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Arkadia Verlen       | 3 | ((211127-waśń-o-ryby-w-majklapcu; 211207-gdy-zabraknie-pradu-rekinom; 220730-supersupertajny-plan-loreny)) |
| Lorena Gwozdnik      | 3 | ((210831-serafina-staje-za-wydrami; 211207-gdy-zabraknie-pradu-rekinom; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Andrzej Kuncerzyk    | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Ernest Namertel      | 2 | ((211123-odbudowa-wedlug-justyniana; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Franciszek Zygmunt   | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Henryk Wkrąż         | 2 | ((211207-gdy-zabraknie-pradu-rekinom; 220819-tank-as-a-love-letter)) |
| Izydor Grumczewicz   | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Laura Tesinik        | 2 | ((210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami)) |
| Liliana Bankierz     | 2 | ((211228-akt-o-ktorym-marysia-nie-wie; 220730-supersupertajny-plan-loreny)) |
| Paulina Mordoch      | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Sensacjusz Diakon    | 2 | ((210824-mandragora-nienawidzi-rekinow; 211207-gdy-zabraknie-pradu-rekinom)) |
| Tadeusz Łaśnic       | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Tomasz Tukan         | 2 | ((210720-porwanie-daniela-terienaka; 210824-mandragora-nienawidzi-rekinow)) |
| Władysław Owczarek   | 2 | ((220101-karolina-w-ciezkim-mlocie; 220730-supersupertajny-plan-loreny)) |
| Aleksandra Burgacz   | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Amelia Sowińska      | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Ariel Kubunczak      | 1 | ((220819-tank-as-a-love-letter)) |
| Arkadiusz Terienak   | 1 | ((220819-tank-as-a-love-letter)) |
| Arnold Kłaczek       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| AU Flara Astorii     | 1 | ((220819-tank-as-a-love-letter)) |
| Azalia Sernat d'Namertel | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Barnaba Burgacz      | 1 | ((201201-impreza-w-malopsie)) |
| Cezary Urmaszcz      | 1 | ((201201-impreza-w-malopsie)) |
| Ekaterina Zajcew     | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Franek Bulterier     | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Genowefa Krecik      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Grzegorz Terienak    | 1 | ((201215-dziewczyna-i-pies)) |
| Halina Sermniek      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Henryk Murkot        | 1 | ((220101-karolina-w-ciezkim-mlocie)) |
| Hestia d'Rekiny      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Ignacy Myrczek       | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Iwan Zawtrak         | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Iwona Perikas        | 1 | ((220819-tank-as-a-love-letter)) |
| Izabela Selentik     | 1 | ((220819-tank-as-a-love-letter)) |
| Justynian Diakon     | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Karol Pustak         | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Keira Amarco d'Namertel | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Ksenia Kirallen      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Lea Samszar          | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Mariusz Trzewń       | 1 | ((210720-porwanie-daniela-terienaka)) |
| Marsen Gwozdnik      | 1 | ((220730-supersupertajny-plan-loreny)) |
| Michał Kabarniec     | 1 | ((220819-tank-as-a-love-letter)) |
| Michał Klabacz       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Mimoza Diakon        | 1 | ((220819-tank-as-a-love-letter)) |
| Mimoza Elegancja Diakon | 1 | ((220101-karolina-w-ciezkim-mlocie)) |
| Nadia Uprewien       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Napoleon Bankierz    | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Natalia Tessalon     | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Oliwier Czepek       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Patrycja Radniak     | 1 | ((201215-dziewczyna-i-pies)) |
| Paweł Szprotka       | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Rupert Mysiokornik   | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Serafina Ira         | 1 | ((210831-serafina-staje-za-wydrami)) |
| Stella Amakirin      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Talia Mikrit         | 1 | ((220819-tank-as-a-love-letter)) |
| Urszula Arienik      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Urszula Miłkowicz    | 1 | ((210720-porwanie-daniela-terienaka)) |
| Żorż d'Namertel      | 1 | ((220730-supersupertajny-plan-loreny)) |