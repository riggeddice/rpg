# Kalista Surilik
## Identyfikator

Id: 9999-kalista-surilik

## Sekcja Opowieści

### Relikwia z androida?!

* **uid:** 240214-relikwia-z-androida, _numer względny_: 8
* **daty:** 0107-05-16 - 0107-05-18
* **obecni:** Aerina Cavalis, Alina Mekran, Elwira Barknis, Kalista Surilik, Klasa Dyplomata, Klasa Hacker, Klasa Inżynier, Klasa Oficer Naukowy, Klasa Sabotażysta, Mawir Hong, Tadeusz Mekran, Vanessa d'Cavalis

Streszczenie:

Zniknęły szczątki Vanessy - bohaterskiego androida, wydane trzem biologom. Niewyjaśnione polecenie wydania szczątków Vanessy i niespodziewane pojawienie się fanatyków walczących w jej imieniu na podziemnych arenach, zainfekowanych niemożliwymi do zrobienia na tej stacji hipnonarkotykami, wskazało na problem z anomaliami magicznymi. Okazało się, że przyczyną było morderstwo Aliny Mekran przez Elwirę Barknis, której plagiat i agresywne metody w pracy wyszły na jaw. Alina, próbująca wrócić do życia jako "resurrector" za pomocą roślin i hydroponiki, potrzebowała energii, krwi i ciała. Agencja była w stanie zamaskować sytuację, zniszczyć zainfekowany orbitujący dookoła Szernief hydrolab H4 i zabrali Vanessę jako influencerkę turystyczną poza Stację.

Aktor w Opowieści:

* Dokonanie:
    * walcząca o prawdę za wszelką cenę i zamknięta przez Aerinę w izolatce, odkryła prawdę o Elwirze i podzieliła się nią z Agencją. Zdradzona przez Aerinę i Agencję, pójdzie własną drogą.
* Progresja:
    * zdradzona przez Agencję (ponownie) oraz przez Aerinę, zbliża się do Mawira. Prawda jest najważniejsza.


### Dla swych marzeń, warto!

* **uid:** 240117-dla-swych-marzen-warto, _numer względny_: 7
* **daty:** 0106-11-04 - 0106-11-06
* **obecni:** Aerina Cavalis, Artur Tavit, Estril Cavalis, Felina Amatanir, Kalista Surilik, Klasa Hacker, Klasa Oficer Naukowy, Klasa Sabotażysta, Larkus Talvinir, Maia Sakiran, Mawir Hong, Vanessa d'Cavalis

Streszczenie:

Na Stacji pojawił się pierścień Esuriit, który sprawia, że ludzie potrafią poświęcić energię do spełnienia obsesyjnych marzeń kosztem życia tych ludzi. Za pierścieniem przybył Szmaciarz, próbujący dodać pierścień do swojej kolekcji artefaktów. Zespołowi udało się zatrzymać Skażenie i uratować członka rady - stracili jednak Vanessę (biosyntkę) oraz Kalista została Aktywowana. Za to zespół zintegrował biosynty z resztą stacji.

Aktor w Opowieści:

* Dokonanie:
    * doszła do tego, że za wszystkim stoi tajemniczy Pierścień; współpracuje z Agencją, bo to mniejsze zło. Straciła palec do Jonatana i nosiła Pierścień, co ją Aktywowało.
* Progresja:
    * trwale straciła palec, ale pierścień Esuriit Aktywował jej moce protomaga. Od teraz musi nosić rękawiczkę - jej palec NIGDY nie zadziała.


### Pan Skarpetek i Odratowany Ogród

* **uid:** 231221-pan-skarpetek-i-odratowany-ogrod, _numer względny_: 6
* **daty:** 0106-04-25 - 0106-04-27
* **obecni:** Felina Amatanir, Ignatius Sozyliw, Kalista Surilik, Klasa Inżynier, Klasa Oficer Naukowy, Klasa Sabotażysta, Mawir Hong

Streszczenie:

Rada wezwała Agencję, bo dzieciaki potrafią rozkazywać dorosłym używając savarańskiej postaci z bajek, 'Pana Skarpetka'. Agencja odkrywa, że dzieciaki próbują zrobić Rajski Ogród - miejsce z roślinami i radością. Gdy Agencja orientuje się, że to jest niemożliwe technicznie, odkrywają wpływ energii Alteris. Po przebijaniu się przez Alteris udało im się uratować dzieci, ale pomieszczenie jest Zniekształcone i trzeba było Kaliście coś powiedzieć o magii... za to, Agencja ma poprawioną reputację, choć ma opinię tych co promują współpracę ludów na stacji.

Aktor w Opowieści:

* Dokonanie:
    * dowiaduje się o rzeczach magicznych i Oficer Naukowy wyjaśnia jej Pryzmat. Rozumie czemu jest tak a nie inaczej. Dalej nie ufa Agencji, ale ufa JEDNEJ OSOBIE.


### Przeznaczeniem Szernief nie jest wojna domowa

* **uid:** 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa, _numer względny_: 5
* **daty:** 0105-12-11 - 0105-12-13
* **obecni:** Aerina Cavalis, Dorion Fughar, Felina Amatanir, Gabriel Septenas, Kaldor Czuk, Kalista Surilik, Klasa Dyplomata, Klasa Inżynier, Klasa Sabotażysta, Mawir Hong, Sia-03 Szernief

Streszczenie:

Na Stacji doszło do odprawienia savarańskiego rytuału Krwi dobrobytu w formie energii Fidetis. Problem w tym, że savaranie mają jedną wizję sukcesu a Szernief ma N grup. Więc każda grupa jest wzmacniana w _inny_ sposób, co powoduje ciekawe problemy (np. sabotaże by działali razem, lub savaranie atakujący Mawirowców by ci mogli być silniejsi). Agencja odkrywa obecność Łowcy (polującego na Radę) i go neutralizuje. Niestety, Tkaczka integruje się z Sią-03 tworząc nową frakcję.

Aktor w Opowieści:

* Dokonanie:
    * z ogromną niechęcią przyjęła pojawienie się Agencji na terenie Szernief znowu. Unika ich. Zebrała dane o tym że coś się tu dzieje i przekazała je Felinie. Ogólnie, offscreenowa.
* Progresja:
    * dzięki działaniom Mawira, jej pozycja jest bardzo podniesiona.


### Sen chroniący kochanków

* **uid:** 231122-sen-chroniacy-kochankow, _numer względny_: 4
* **daty:** 0105-09-02 - 0105-09-05
* **obecni:** Damian Orczakin, Dorion Fughar, Felina Amatanir, Jola-09 Szernief, Kalista Surilik, Klasa Dyplomata, Klasa Hacker, Klasa Oficer Naukowy, Klaudiusz Widar, Mawir Hong, Rovis Skarun, Szymon Alifajrin

Streszczenie:

Na stacji CON Szernief konflikt między savaranami a drakolitami, podsycany przez inwestorów i stymulanty, prowadzi do zagrożenia pryzmatycznego. Dochodzi do manifestacji Esuriit i Alucis. Punktem kluczowym okazała się miłość Rovisa i Joli-09, prowadząca do rozprzestrzeniającego się Snu Ukojenia zwalczającego i zwalczanego przez Nienawiść Esuriit. Lux Umbrarum interweniuje, tworząc antidotum i przesiedlając kochanków. Kryzys został rozwiązany i stacja uspokojona tymczasowo, ale Kalista jest na tropie 'dziwnych rzeczy' a zaufanie populacji stacji do Inwestorów i Rady spada.

Aktor w Opowieści:

* Dokonanie:
    * dziennikarka skupiona na prawdzie i uczciwości, podejrzewa Inwestorów o dodanie czegoś do wody i skutecznie ujawniła niektóre ruchy Agencji. Świetnie zbiera informacje, ma je też spoza stacji.


### Polowanie na biosynty na Szernief

* **uid:** 231213-polowanie-na-biosynty-na-szernief, _numer względny_: 3
* **daty:** 0105-07-26 - 0105-07-31
* **obecni:** Artur Tavit, Delgado Vitriol, Felina Amatanir, Kalista Surilik, Klasa Biurokrata, Klasa Dyplomata, Klasa Hacker, Klasa Oficer Naukowy, Klasa Sabotażysta, Malik Darien, Sebastian-194, Vanessa d'Cavalis

Streszczenie:

Kalista wezwała Agencję bo biosynty są niszczone (ludzie zabijają je pierwsi) i dzieje się 'coś dziwnego' a 'człowiek Agencji' jest niekompetentny - okazało się, że jest Skażony Unumens (ma pasożyta). Agencja zrzuciła 'legalność' biosyntów na arystokratę Artura, zapewnia sobie współpracę z biosyntami i kieruje je do zniszczenia Parasekta Unumens. By odwrócić uwagę od swoich działań - zdradza Kalistę i Mawira (i wszystkie problemy zrzuca na nich). Udało się jednak większość uratować, mimo śmierci kilku osób w tak trudnej sytuacji.

Aktor w Opowieści:

* Dokonanie:
    * wezwała Agencję bo coś jest bardzo nie tak; gdy już przyskrzyniła fakty (ale bez magii) Agencja poprosiła ją o współpracę przeciw chorobie pasożytniczej. Kalista na to poszła, ale były efekty uboczne i Agencja zrzuciła na nią problemy. Ucierpiała, przesunęła się bliżej Mawira Honga i dalej od Feliny.
* Progresja:
    * przez działania Agencji dostała silny cios reputacyjny i przesunęła się bliżej Mawira Honga (który też dostał za niewinność). Ogromna nieufność wobec Agencji.


### Tajemnicze Tunele Sebirialis

* **uid:** 231119-tajemnicze-tunele-sebirialis, _numer względny_: 2
* **daty:** 0104-11-20 - 0104-11-24
* **obecni:** Aniela Kafantelas, Eryk Kawanicz, Felina Amatanir, Ignatius Sozyliw, Janvir Krassus, Kalista Surilik, Klasa Dyplomata, Klasa Sabotażysta, Larkus Talvinir, Leon Hurmniow, Marta Sarilit, OLU Luminarius

Streszczenie:

W tunelach kopalni pod CON Szernief znikają ludzie. Agencja dochodzi do tego, że winny jest protomag Dotknięty Alteris, który uciekł z tajnej (dla populacji Szernief) placówki NavirMed gdzie prowadzi się badania na ludziach. Agencja doszła do łańcucha logicznego i odkryła prawdę; by to rozwiązać, ewakuowali kopalnię i placówkę NavirMed na 3 miesiące by protomag umarł z głodu.

Aktor w Opowieści:

* Dokonanie:
    * dziennikarka badająca znikanie górników na Sebiralisie; zeszła z potencjalnymi inwestorami do kopalni robić badania i została porwana przez Eryka i Alteris. W jakiś sposób przetrwała, uratowana przez Felinę. Jej zniknięcie było dużym utrapieniem dla Rady.


### Protest przed kultem na Szernief

* **uid:** 231202-protest-przed-kultem-na-szernief, _numer względny_: 1
* **daty:** 0103-10-15 - 0103-10-19
* **obecni:** Aerina Cavalis, Frederico Zyklas, Kalista Surilik, Klasa Biurokrata, Klasa Hacker, OLU Luminarius, Vanessa d'Cavalis

Streszczenie:

Seria tajemniczych morderstw arystokratów na Szernief. Pierwszym sprawcą był Frederico, inżynier, Agencja odkrywa, że morderstwa wydają się być spowodowane przez anomalię Esuriit, która wpływa na zachowania ofiar. Kluczem do prawdy było przesłuchanie Frederico (kiedyś kultysty) - jego żona i nienarodzone dziecko zginęły podczas protestu arystokratów. Okazało się, że jego żona próbuje inkarnować się w Aerinie. Rozwiązaniem było rozlokowanie arystokratów poza stację i wyleczenie Aeriny, zabierając ją do Agencji.

Aktor w Opowieści:

* Dokonanie:
    * świetnie złapała Agencję za tematy anomalne; przekazała informacje Zespołowi o tym co wie (historia, Frederico, działania) i za to dostała wsparcie Luminariusa do prowadzenia swoich badań i śledztwa. Tak czy inaczej, jej chęć pomocy jest większa niż jej nieufność. Acz nie jest traktowana poważnie przez Agencję.
* Progresja:
    * miała dostęp do Luminariusa, jego publicznych banków danych i mocy obliczeniowej co mogła doskonale wykorzystać. I to zrobiła.
    * nie ufa Agencji. Wie o linkach Agencji z dziwnymi rzeczami jakie się tu dzieją. Wie o ciężkiej operacji Agencji i że są dziwni.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 8, @: 0107-05-18
    1. Primus    : 8, @: 0107-05-18
        1. Sektor Kalmantis    : 8, @: 0107-05-18
            1. Sebirialis, orbita    : 8, @: 0107-05-18
                1. CON Szernief    : 8, @: 0107-05-18
                    1. Orbitujące stacje hydroponiczne    : 1, @: 0107-05-18
                    1. Powłoka Wewnętrzna    : 2, @: 0105-09-05
                        1. Poziom Minus Dwa    : 2, @: 0105-09-05
                            1. Więzienie    : 2, @: 0105-09-05
                        1. Poziom Minus Jeden    : 2, @: 0105-09-05
                            1. Obszar Mieszkalny Savaran    : 2, @: 0105-09-05
                        1. Poziom Minus Trzy    : 2, @: 0105-09-05
                            1. Wielkie Obrady    : 2, @: 0105-09-05
                    1. Powłoka Zewnętrzna    : 1, @: 0105-09-05
                        1. Panele Słoneczne    : 1, @: 0105-09-05
            1. Sebirialis    : 1, @: 0104-11-24
                1. Krater Ablardius    : 1, @: 0104-11-24
                    1. Kopalnie Ablardius    : 1, @: 0104-11-24
                1. Płaskowyż Zaitrus    : 1, @: 0104-11-24
                    1. Ukryta Baza NavirMed    : 1, @: 0104-11-24

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Felina Amatanir      | 6 | ((231119-tajemnicze-tunele-sebirialis; 231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto)) |
| Klasa Sabotażysta    | 6 | ((231119-tajemnicze-tunele-sebirialis; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Dyplomata      | 5 | ((231119-tajemnicze-tunele-sebirialis; 231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240214-relikwia-z-androida)) |
| Klasa Hacker         | 5 | ((231122-sen-chroniacy-kochankow; 231202-protest-przed-kultem-na-szernief; 231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Oficer Naukowy | 5 | ((231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Mawir Hong           | 5 | ((231122-sen-chroniacy-kochankow; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Aerina Cavalis       | 4 | ((231202-protest-przed-kultem-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Vanessa d'Cavalis    | 4 | ((231202-protest-przed-kultem-na-szernief; 231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Inżynier       | 3 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240214-relikwia-z-androida)) |
| Artur Tavit          | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Dorion Fughar        | 2 | ((231122-sen-chroniacy-kochankow; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Ignatius Sozyliw     | 2 | ((231119-tajemnicze-tunele-sebirialis; 231221-pan-skarpetek-i-odratowany-ogrod)) |
| Klasa Biurokrata     | 2 | ((231202-protest-przed-kultem-na-szernief; 231213-polowanie-na-biosynty-na-szernief)) |
| Larkus Talvinir      | 2 | ((231119-tajemnicze-tunele-sebirialis; 240117-dla-swych-marzen-warto)) |
| OLU Luminarius       | 2 | ((231119-tajemnicze-tunele-sebirialis; 231202-protest-przed-kultem-na-szernief)) |
| Alina Mekran         | 1 | ((240214-relikwia-z-androida)) |
| Aniela Kafantelas    | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Damian Orczakin      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Delgado Vitriol      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Elwira Barknis       | 1 | ((240214-relikwia-z-androida)) |
| Eryk Kawanicz        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Estril Cavalis       | 1 | ((240117-dla-swych-marzen-warto)) |
| Frederico Zyklas     | 1 | ((231202-protest-przed-kultem-na-szernief)) |
| Gabriel Septenas     | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Janvir Krassus       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Jola-09 Szernief     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Kaldor Czuk          | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klaudiusz Widar      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Leon Hurmniow        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Maia Sakiran         | 1 | ((240117-dla-swych-marzen-warto)) |
| Malik Darien         | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Marta Sarilit        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Rovis Skarun         | 1 | ((231122-sen-chroniacy-kochankow)) |
| Sebastian-194        | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Sia-03 Szernief      | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Szymon Alifajrin     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Tadeusz Mekran       | 1 | ((240214-relikwia-z-androida)) |