# Wiktor Satarail
## Identyfikator

Id: 1807-wiktor-satarail

## Sekcja Opowieści

### Satarail pomaga Marysi

* **uid:** 211102-satarail-pomaga-marysi, _numer względny_: 19
* **daty:** 0111-08-09 - 0111-08-10
* **obecni:** Ernest Namertel, Karolina Terienak, Lorena Gwozdnik, Marek Samszar, Marysia Sowińska, Rafał Torszecki, Sensacjusz Diakon, Wiktor Satarail

Streszczenie:

Satarail uderzył - zainfekował Owadem który wypełzł z Torszeckiego Samszara (który kiedyś ukradł somnibela Olgi). Zainfekowane Rekiny zaczęły dewastować dzielnicę. Karo skutecznie rozwala owady (i Lorenę), po czym Sensacjusza w glistę medyczną. Marysia samodzielnie leci jako owad wyłączyć Samszara. Torszecki jest rozgrzeszony - "nie był sobą". Ale polowanie na Torszeckiego czas zacząć.

Aktor w Opowieści:

* Dokonanie:
    * pomścił somnibela Olgi. Stworzył Owada który przez Torszeckiego wszedł w Marka Samszara i zadał mu straszne cierpienie, uszkadzając Dzielnicę Rekinów.


### Koszt ratowania Torszeckiego

* **uid:** 211026-koszt-ratowania-torszeckiego, _numer względny_: 18
* **daty:** 0111-08-01 - 0111-08-05
* **obecni:** Ignacy Myrczek, Ksenia Kirallen, Marysia Sowińska, Olga Myszeczka, Paweł Szprotka, Rafał Torszecki, Sensacjusz Diakon, Teresa Mieralit, Wiktor Satarail

Streszczenie:

By ratować Torszeckiego, Marysia wchodzi w sojusz z Wiktorem Satarailem. On dał jej podskórnego robaka którego dostanie Torszecki, by uzasadnić dziwne zachowanie. Wiktor "dostanie swoją zapłatę" od "kogoś winnego". Marysia przekonała też Sensacjusza, że ona x Torszecki. Zaczęły się też pojawiać takie plotki...

Aktor w Opowieści:

* Dokonanie:
    * po otrzymaniu od Marysi opery dał Marysi robaka do ratowania Torszeckiego. "Winny zapłaci". Uznał Marysię za zdesperowaną lub arogancką.


### Skradziony kot Olgi

* **uid:** 210615-skradziony-kot-olgi, _numer względny_: 17
* **daty:** 0111-05-13 - 0111-05-15
* **obecni:** Arkadia Verlen, Julia Kardolin, Liliana Bankierz, Marek Samszar, Marysia Sowińska, Olga Myszeczka, Paweł Szprotka, Wiktor Satarail

Streszczenie:

Marysia i Julia mają imprezę karaoke w Lesie Trzęsawnym, ale Marek nie dotarł - walczy na magię z Lilianą. Julia puściła drony Triany, które wpadły w berserk. By Tymon się nie zainteresował tym co się dzieje w lesie, Marysia i Julia chciały rozwiązać problem - okazało się, że Marek ukradł somnibela Oldze Myszeczce i dał go Arkadii Verlen w prezencie. Marysia i Julia przekonały Arkadię do oddania kota, oddały go Oldze, po czym Arkadia zerwała z Markiem i go ciężko pobiła. Acz Paweł Szprotka (który próbował odzyskać kota od Marka i którego broniła Liliana) boi się o życie, bo niektóre Rekiny się na niego uwzięły za karę...

Aktor w Opowieści:

* Dokonanie:
    * znajdował się na zapleczu domku Olgi Myszeczki. Gdyby Olga nie odzyskała swojego somnibela, Wiktor by go dla niej odzyskał.


### Adaptacja Azalii

* **uid:** 200623-adaptacja-azalii, _numer względny_: 16
* **daty:** 0110-09-26 - 0110-10-04
* **obecni:** Azalia d'Alkaris, Karla Mrozik, Konrad Wączak, Minerwa Metalia, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Orbiter, frakcja NeoMil, wystawili na Trzęsawisko bazę nanitkową której celem jest uczenie odludzkiej TAI klasy Azalia. Pięknotka i Minerwa poszły jako wsparcie z Pustogoru. Okazało się, że Wiktor pozwala TAI Azalii na wiarę, że adaptuje się do Trzęsawiska by przejąć nad nią kontrolę. Pięknotka wynegocjowała bezpieczną ewakuację - ale Wiktor powiedział, że zniszczy Castigator. Pięknotka nie wie jak, ale OK. Nie wygra tego. Grunt, że wszyscy są bezpieczni.

Aktor w Opowieści:

* Dokonanie:
    * Orbiter próbował wejść na jego teren, na jego Trzęsawisko by wyszkolić Azalię d'Alkaris. On użył ixiońskiej energii do Skażenia oryginalnej Azalii na Alkarisie.


### Wojna Trzęsawiska

* **uid:** 200418-wojna-trzesawiska, _numer względny_: 15
* **daty:** 0110-08-16 - 0110-08-21
* **obecni:** Gabriel Ursus, Ignacy Myrczek, Karla Mrozik, Pięknotka Diakon, Sabina Kazitan, Wiktor Satarail

Streszczenie:

Trzęsawisko odpowiedziało na atak tworząc nową bioformę na bazie Sabiny Kazitan; zaczęło przyciągać ludzi do siebie. Z uwagi na wysokie niebezpieczeństwo Karla chciała zbombardować Trzęsawisko z orbity, jak kiedyś. Pięknotka zrobiła kanał negocjacyjny Pustogor - Wiktor Satarail; Wiktor pomoże Pięknotce uspokoić Trzęsawisko, ale nie będzie bombardowania z orbity. Plus, Pustogor pomoże mu sformować nową bioformę...

Aktor w Opowieści:

* Dokonanie:
    * zapowiedział wojnę jeśli będzie bombardowanie Trzęsawiska z orbity, pomoże uspokoić Trzęsawisko jeśli Pustogor pomoże mu ze zbudowaniem bioformy na bazie Sabiny.


### Kirasjerka najgorszym detektywem

* **uid:** 190721-kirasjerka-najgorszym-detektywem, _numer względny_: 14
* **daty:** 0110-06-10 - 0110-06-11
* **obecni:** Aida Serenit, Mirela Orion, Olga Myszeczka, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Przyjaciółka Kirasjerki Orbitera, Aida, zniknęła. Kirasjerka poszła jej szukać i znalazła ślady Pięknotki. Oczywiście, Kirasjerka zaatakowała by zdobyć informacje a Pięknotka nie mogła się poddać (osłaniana przez Amandę). Skończyło się na dewastacji 3 servarów, ciężko rannej Kirasjerce i stropionej Pięknotce. Gdy Pięknotka poszła do Olgi znaleźć leczenie dla Mireli (Kirasjerki), tam dowiedziała się od Wiktora Sataraila, że on podłożył ślady by pokazać jej obecność Cieniaszczytu - a dokładniej kralotycznej bioformy. Największe możliwe nieporozumienie.

Aktor w Opowieści:

* Dokonanie:
    * znalazł bioformę kralotyczną więc podłożył ślady na Pięknotkę by do niego przyszła. Niestety, ślady znalazła Mirela. Wiktor zapoznał się z Emulatorami Orbitera.


### Mimik śni o Esuriit

* **uid:** 190527-mimik-sni-o-esuriit, _numer względny_: 13
* **daty:** 0110-05-07 - 0110-05-09
* **obecni:** Adela Kirys, Erwin Galilien, Mirela Satarail, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Mirela uciekła Wiktorowi Satarailowi z Trzęsawiska, więc ów poprosił o pomoc Pięknotkę. Mirela wyczuła Esuriit w Czółenku i chciała nakarmić głód; Pięknotka jednak dała radę ją znaleźć i nie dopuścić do Pożarcia Mireli przez Esuriit; zamiast tego ściągnęła oddział terminusów którzy sami rozwiązali problem. No i wycofała Mirelę z powrotem do Wiktora.

Aktor w Opowieści:

* Dokonanie:
    * zmartwiony ucieczką Mireli; poprosił Pięknotkę (alternatywą jest jego osobiste działanie). Dał Pięknotkce trochę krwi Mireli by ta zrobiła detektor.


### Szczur który chroni

* **uid:** 190505-szczur-ktory-chroni, _numer względny_: 12
* **daty:** 0110-04-22 - 0110-04-24
* **obecni:** Adela Kirys, Alan Bartozol, Ernest Kajrat, Kasjan Czerwoczłek, Krystian Namałłek, Lucjusz Blakenbauer, Oliwia Namałłek, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Zaczęło się od Skażenia proszkiem kralotycznym Nieużytków Staszka. Potem Pięknotka współpracując z mafią doszła do tego, że Adela i terminus stoją za tą sprawą. Potem Pięknotka uratowała Adelę i terminusa wprowadzając szturmowych terminusów Pustogoru na mafię. Gdy "Cień wyrwał się spod kontroli", Alan ją strzelił z działa strumieniowego. I tak skończyło się rumakowanie na tydzień - ale wszyscy są bezpieczni.

Aktor w Opowieści:

* Dokonanie:
    * pomógł Pięknotce przeprowadzając Krystiana przez Trzęsawisko i biorąc do siebie Oliwię i Adelę na jakiś czas. Adelę nauczy, Oliwię wyleczy.


### Córka Mimika

* **uid:** 190524-corka-mimika, _numer względny_: 11
* **daty:** 0110-04-20 - 0110-04-23
* **obecni:** Antoni Żuwaczka, Dariusz Puszczak, Mirela Satarail, Wiktor Satarail

Streszczenie:

Córka Mimika okazała się być stosunkowo niegroźną kobietą amplifikatorem. Znalazła się w Sensoplex, ale została wydobyta stamtąd przez Zespół próbujący nie dopuścić do uciemiężenia agentów Senetis przez Szakale. Córka Mimika trafiła pod opiekę Wiktora Sataraila, Podwiert stał się grobem dla Szakali (rozwiązało ich) oraz Sensoplex jest okryty sławą miejsca tajemniczych skarbów.

Aktor w Opowieści:

* Dokonanie:
    * pomógł Córce Mimika i dostał ją pod swoją opiekę; dodatkowo wzbudził grozę w Podwiercie (bo co tym razem knuje)


### Osopokalipsa Wiktora

* **uid:** 190419-osopokalipsa-wiktora, _numer względny_: 10
* **daty:** 0110-03-29 - 0110-03-31
* **obecni:** Alan Bartozol, Kasjopea Maus, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Wiktor zdecydował się zemścić w imieniu wił. Zaprojektował osy które robiły krzywdę magom i ludziom - wpierw użył ich jako dywersję, potem zaraził jedzenie w Sensoplex. Pięknotka dekontaminując okolicę odkryła plan; przekonała go, że można bezkrwawo usunąć stąd Sensus. I to zrobiła - z pomocą Kasjopei by ta zrobiła reportaż o "Osopokalipsie", po czym sfabrykowała dowody z pomocą Wiktora. W ten sposób Sensus opuścił teren i nikt szczególnie nie ucierpiał.

Aktor w Opowieści:

* Dokonanie:
    * pokazał pełnię mocy tworząc osy by zniszczyć Sensoplex. Pięknotka przekonała go, że można ich wrobić i usunąć politycznie. Osiągnął cel.


### Bardzo kosztowne łzy

* **uid:** 190406-bardzo-kosztowne-lzy, _numer względny_: 9
* **daty:** 0110-03-21 - 0110-03-26
* **obecni:** Adrian Wężoskór, Ida Tiara, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Wiktor Satarail pomógł wiłom z których łez produkowany był Imperialny Proszek przez Sensus. Pięknotka wynegocjowała jego bezpieczne przejście a on próbował zmienić matrycę czarodziejki dowodzącej kompleksem Sensus - by z jej łez osiągnąć lepszy Proszek. Pięknotka uratowała tą czarodziejkę.

Aktor w Opowieści:

* Dokonanie:
    * uratował kilkanaście nieszczęsnych wił z fabryk Sensus. Ma krwawą wendettę z Sensus. Starł się z Pięknotką, potem z Cieniem. Liże rany na bagnie. Nauczył się groźnych technik od Saitaera.
* Progresja:
    * wieczna wojna przeciwko Sensus; zarówno on wobec nich jak i oni wobec niego.


### Ixioński Transorganik

* **uid:** 190127-ixionski-transorganik, _numer względny_: 8
* **daty:** 0110-01-29 - 0110-01-30
* **obecni:** Erwin Galilien, Karolina Erenit, Kirył Najłalmin, Minerwa Metalia, Napoleon Bankierz, Pięknotka Diakon, Saitaer, Tymon Grubosz, Wiktor Satarail, Wojtek Kurczynos

Streszczenie:

Minerwa próbowała pomóc Karolinie Erenit, ale niestety użyła artefaktu ixiońskiego i przekształciła wrednego Wojtka Kurczynosa w transorganika. Pięknotka z pomocą Epirjona przeteleportowała siebie i Wojtka na Trzęsawisko i tam z pomocą Wiktora Sataraila odwrócili tą transorganizację. Ten konkretny problem udało się dyskretnie rozwiązać.

Aktor w Opowieści:

* Dokonanie:
    * pomógł Pięknotce na Trzęsawisku by nie stała się nadmierna krzywda Wojtkowi; odwrócił jego transorganizację.
* Progresja:
    * opracowuje sposób odwrócenia ixiońskiej transorganizacji; częściowo już mu się to udało.


### Ostatnia misja Tarna

* **uid:** 190928-ostatnia-misja-tarna, _numer względny_: 7
* **daty:** 0110-01-27 - 0110-01-28
* **obecni:** BIA Tarn, Eustachy Mrownik, Hestia d'Tiamenat, Pedro Ronfak, Talia Aegis, Wiktor Satarail

Streszczenie:

Wiktor Satarail poproszony przez Pięknotkę skupił się na zniszczeniu Tarna. Zaatakował Tiamenat subtelnie, skażeniem biologicznym i hackerem uszkodził percepcję TAI Hestii. Tarn się obudził i odparł atak Wiktora; w wyniku ten go Zatruł. Ale Tarn wynegocjował życie - bo nikomu nie zrobił krzywdy. Wiktor się na to zgodził, jeśli Tarn opuści ten teren raz na zawsze - poleci w kosmos.

Aktor w Opowieści:

* Dokonanie:
    * na prośbę Pięknotki uderzył w Tiamenat by skażeniem biologicznym zniszczyć Tarna. Gdy Tarn przejął kompleks, Wiktor go Zatruł - ale darował życie.
* Progresja:
    * zdobywa sporo wiedzy noktiańskiej dzięki współpracy z BIA Tarn.


### Skorpipedy królewskiego xirathira

* **uid:** 190119-skorpipedy-krolewskiego-xirathira, _numer względny_: 6
* **daty:** 0110-01-14 - 0110-01-15
* **obecni:** Adela Kirys, Olga Myszeczka, Pięknotka Diakon, Sławomir Muczarek, Waldemar Mózg, Wiktor Satarail

Streszczenie:

Czerwone Myszy poprosiły Pięknotkę o pomoc - pojawiły się na ich terenie niebezpieczne skorpipedy i działania Adeli jedynie pogarszają sprawę. Okazało się, że to działania Wiktora Sataraila, który chce pomóc Oldze Myszeczce w utrzymaniu terenu i odparciu harrasserów z Myszy. Pięknotka odkręciła sprawę i trochę poprawiła relację z Wiktorem. Acz jest sceptyczna wobec samych Myszy...

Aktor w Opowieści:

* Dokonanie:
    * zdecydował się pomóc Oldze (chyba z nią flirtuje). Stworzył dziwne skorpipedy i odwiedza Olgę regularnie w jej Pustej Wsi. Chce jej pomóc.
* Progresja:
    * chce pomóc Oldze w jej świecie, z jej viciniusami i jej problemami. Niech Olga ma spokojne, fajne życie.


### Wypalenie Saitaera z Trzęsawiska

* **uid:** 190116-wypalenie-saitaera-z-trzesawiska, _numer względny_: 5
* **daty:** 0110-01-07 - 0110-01-09
* **obecni:** Alan Bartozol, ASD Centurion, Hieronim Maus, Karradrael, Kasjopea Maus, Pięknotka Diakon, Saitaer, Wiktor Satarail

Streszczenie:

Pięknotka dostała zadanie eskortowania kapłana Karradraela do ołtarza Saitaera. ASD Centurion oraz Alan odwracali uwagę Trzęsawiska. Plan się udał, choć w wyniku Pięknotka została znowu przekształcona przez Saitaera. Za to, nie ma już wpływu Saitaera w okolicy - więcej, udało się zdjąć też wpływ z Wiktora.

Aktor w Opowieści:

* Dokonanie:
    * próbował rozpaczliwie zatrzymać atak magów na Trzęsawisko - bez skutku. Potem Pięknotka rzuciła weń Kasjopeą, rozpraszając wpływ Saitaera. Jest wściekły jak osa.
* Progresja:
    * oczyszczony z wpływów Saitaera, ale jednocześnie wściekły na Pustogor za rany zadane Trzęsawisku, jak i na Pięknotkę za zabranie mu potencjalnych jeńców.


### Turyści na Trzęsawisku

* **uid:** 190105-turysci-na-trzesawisku, _numer względny_: 4
* **daty:** 0109-12-22 - 0109-12-24
* **obecni:** Alan Bartozol, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Grupa turystów spoza Pustogoru odwiedziała Trzęsawisko wraz z trzema łowcami nagród. Pięknotka i Alan poszli ich ratować. W samą porę - sygnał SOS pokazał, że jest już późno. Udało się wszystkich uratować, za co... Pięknotce ktoś zdemolował gabinet. Pięknotka i Alan doszli do pewnego porozumienia w sprawie Adeli - znają jej imię. Acz chyba Alan się o nią martwi.

Aktor w Opowieści:

* Dokonanie:
    * nie jest Skażonym potworem i chyba nie jest sterowany przez Saitaera. Pomógł Pięknotce - a mógł zrobić cokolwiek. Chroni Trzęsawisko i powstrzymywał jego działanie.


### Uwięzienie Saitaera

* **uid:** 181230-uwiezienie-saitaera, _numer względny_: 3
* **daty:** 0109-12-07 - 0109-12-09
* **obecni:** Karla Mrozik, Kreacjusz Diakon, Lucjusz Blakenbauer, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Pięknotka wróciła do Pustogoru, będąc po drodze zaatakowana przez latającego pnączoszpona. Odkryła, że Saitaer poważnie zmienił już Minerwę. Zmartwiła się tym, jak bardzo ta Minerwa różni się od jej przyjaciółki. Ta sprawa jest ponad siły Pięknotki - poszła do Karli, przełożonej Pustogoru. Tam dowiedziała się, że Saitaer to broń i że być może to on stoi za problemami Trzęsawiska Zjawosztup. Pustogor pojmał Saitaera i przejął wskrzeszoną Minerwę do leczenia.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * potencjalnie Skażony przez Saitaera. A przynajmniej, tak uważa Karla Mrozik.


### Neutralizacja artylerii koszmarów

* **uid:** 181114-neutralizacja-artylerii-koszmarow, _numer względny_: 2
* **daty:** 0109-10-20 - 0109-10-21
* **obecni:** Felicja Melitniek, Pięknotka Diakon, Tymon Grubosz, Wiktor Satarail

Streszczenie:

Wiktor Satarail uderzył z Trzęsawiska. Zdecydował się zrobić Artylerię Koszmarów bazując na strachu ludzi z Mekki Wolności w Zaczęstwie by zeń zbudować artylerię na Trzęstawisku. Pięknotka starła się z nim dwa razy i dała mu odejść spokojnie, by tylko wejść na Trzęsawisko, uwieść Wiktora i zneutralizować Pryzmat artylerii. Wiktor docenił jej plan; pozwolił jej odejść i obiecał, że jeszcze poczeka z eliminacją magów Zaczęstwa.

Aktor w Opowieści:

* Dokonanie:
    * zirytowany tym, że magowie ingerują w Trzęsawisko Zjawosztup zdecydował się zrobić artylerię koszmarów. Artyleria została zneutralizowana przez słodką Pięknotkę w bardzo sprytny sposób.
* Progresja:
    * ma lekką słabość do Pięknotki. Soft spot. Pięknotka nie jest kill on sight.


### Terminuska czy kosmetyczka?

* **uid:** 181027-terminuska-czy-kosmetyczka, _numer względny_: 1
* **daty:** 0109-10-07 - 0109-10-11
* **obecni:** Adela Kirys, Arnulf Poważny, Brygida Maczkowik, Ignacy Myrczek, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Jednoczesny atak na reputację Ateny (przez cybergrzyby) oraz wielki konkurs kosmetyczny w Pustogorze wpłynęły stosunkowo źle na sytuację Pięknotki. Nie rozdwoiła się, ale obie rzeczy udało jej się rozwiązać - acz jest wycieńczona. Odkryła konspirację wymierzoną przeciwko Atenie - ale nie wie kto i gdzie próbuje Atenę usunąć.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * zainteresuje się Brygidą Maczkowik - może uda się ją dostosować do rodu Satarail?


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 17, @: 0111-08-10
    1. Primus    : 17, @: 0111-08-10
        1. Sektor Astoriański    : 17, @: 0111-08-10
            1. Astoria    : 17, @: 0111-08-10
                1. Sojusz Letejski    : 17, @: 0111-08-10
                    1. Szczeliniec    : 17, @: 0111-08-10
                        1. Powiat Pustogorski    : 15, @: 0111-08-10
                            1. Czarnopalec    : 4, @: 0111-08-05
                                1. Pusta Wieś    : 4, @: 0111-08-05
                            1. Czemerta    : 1, @: 0110-08-21
                            1. Czółenko    : 2, @: 0110-06-11
                                1. Bunkry    : 2, @: 0110-06-11
                                1. Tancbuda    : 1, @: 0110-05-09
                            1. Podwiert, okolice    : 1, @: 0110-06-11
                                1. Bioskładowisko podziemne    : 1, @: 0110-06-11
                            1. Podwiert    : 8, @: 0111-08-10
                                1. Bastion Pustogoru    : 1, @: 0110-04-24
                                1. Bunkier Rezydenta    : 1, @: 0110-01-15
                                1. Dolina Biurowa    : 1, @: 0110-04-23
                                1. Dzielnica Luksusu Rekinów    : 3, @: 0111-08-10
                                    1. Obrzeża Biedy    : 1, @: 0111-05-15
                                        1. Hotel Milord    : 1, @: 0111-05-15
                                    1. Serce Luksusu    : 2, @: 0111-08-10
                                        1. Lecznica Rannej Rybki    : 2, @: 0111-08-10
                                1. Las Trzęsawny    : 1, @: 0111-05-15
                                    1. Schron TRZ-17    : 1, @: 0111-05-15
                                1. Odlewnia    : 1, @: 0110-04-24
                                1. Osiedle Sosen    : 1, @: 0110-04-23
                                1. Sensoplex    : 4, @: 0110-04-24
                            1. Pustogor    : 3, @: 0111-08-10
                                1. Barbakan    : 1, @: 0109-10-21
                                1. Gabinet Pięknotki    : 1, @: 0109-12-24
                                1. Rdzeń    : 1, @: 0111-08-10
                                    1. Szpital Terminuski    : 1, @: 0111-08-10
                            1. Zaczęstwo    : 6, @: 0111-08-05
                                1. Akademia Magii, kampus    : 1, @: 0111-08-05
                                    1. Akademik    : 1, @: 0111-08-05
                                1. Cyberszkoła    : 1, @: 0110-01-30
                                1. Kompleks Tiamenat    : 1, @: 0110-01-28
                                    1. Budynek Wołkowca    : 1, @: 0110-01-28
                                    1. Centralny Biolab    : 1, @: 0110-01-28
                                    1. Centrum Dowodzenia    : 1, @: 0110-01-28
                                1. Mekka Wolności    : 1, @: 0109-10-21
                                1. Nieużytki Staszka    : 3, @: 0111-05-15
                                1. Osiedle Ptasie    : 1, @: 0110-01-30
                            1. Żarnia    : 1, @: 0110-01-15
                                1. Żernia    : 1, @: 0110-01-15
                        1. Trzęsawisko Zjawosztup    : 10, @: 0110-10-04
                            1. Accadorian    : 1, @: 0109-12-24
                            1. Głodna Ziemia    : 2, @: 0110-01-30
                            1. Laboratorium W Drzewie    : 3, @: 0110-08-21
                            1. Sfera Pyłu    : 1, @: 0109-12-24
                            1. Toń Pustki    : 1, @: 0109-12-24

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 12 | ((181114-neutralizacja-artylerii-koszmarow; 190105-turysci-na-trzesawisku; 190116-wypalenie-saitaera-z-trzesawiska; 190119-skorpipedy-krolewskiego-xirathira; 190127-ixionski-transorganik; 190406-bardzo-kosztowne-lzy; 190419-osopokalipsa-wiktora; 190505-szczur-ktory-chroni; 190527-mimik-sni-o-esuriit; 190721-kirasjerka-najgorszym-detektywem; 200418-wojna-trzesawiska; 200623-adaptacja-azalii)) |
| Alan Bartozol        | 4 | ((190105-turysci-na-trzesawisku; 190116-wypalenie-saitaera-z-trzesawiska; 190419-osopokalipsa-wiktora; 190505-szczur-ktory-chroni)) |
| Olga Myszeczka       | 4 | ((190119-skorpipedy-krolewskiego-xirathira; 190721-kirasjerka-najgorszym-detektywem; 210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego)) |
| Adela Kirys          | 3 | ((190119-skorpipedy-krolewskiego-xirathira; 190505-szczur-ktory-chroni; 190527-mimik-sni-o-esuriit)) |
| Marysia Sowińska     | 3 | ((210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi)) |
| Erwin Galilien       | 2 | ((190127-ixionski-transorganik; 190527-mimik-sni-o-esuriit)) |
| Ignacy Myrczek       | 2 | ((200418-wojna-trzesawiska; 211026-koszt-ratowania-torszeckiego)) |
| Karla Mrozik         | 2 | ((200418-wojna-trzesawiska; 200623-adaptacja-azalii)) |
| Kasjopea Maus        | 2 | ((190116-wypalenie-saitaera-z-trzesawiska; 190419-osopokalipsa-wiktora)) |
| Marek Samszar        | 2 | ((210615-skradziony-kot-olgi; 211102-satarail-pomaga-marysi)) |
| Minerwa Metalia      | 2 | ((190127-ixionski-transorganik; 200623-adaptacja-azalii)) |
| Mirela Satarail      | 2 | ((190524-corka-mimika; 190527-mimik-sni-o-esuriit)) |
| Paweł Szprotka       | 2 | ((210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego)) |
| Rafał Torszecki      | 2 | ((211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi)) |
| Saitaer              | 2 | ((190116-wypalenie-saitaera-z-trzesawiska; 190127-ixionski-transorganik)) |
| Sensacjusz Diakon    | 2 | ((211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi)) |
| Tymon Grubosz        | 2 | ((181114-neutralizacja-artylerii-koszmarow; 190127-ixionski-transorganik)) |
| Adrian Wężoskór      | 1 | ((190406-bardzo-kosztowne-lzy)) |
| Aida Serenit         | 1 | ((190721-kirasjerka-najgorszym-detektywem)) |
| Antoni Żuwaczka      | 1 | ((190524-corka-mimika)) |
| Arkadia Verlen       | 1 | ((210615-skradziony-kot-olgi)) |
| ASD Centurion        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Azalia d'Alkaris     | 1 | ((200623-adaptacja-azalii)) |
| BIA Tarn             | 1 | ((190928-ostatnia-misja-tarna)) |
| Dariusz Puszczak     | 1 | ((190524-corka-mimika)) |
| Ernest Kajrat        | 1 | ((190505-szczur-ktory-chroni)) |
| Ernest Namertel      | 1 | ((211102-satarail-pomaga-marysi)) |
| Eustachy Mrownik     | 1 | ((190928-ostatnia-misja-tarna)) |
| Felicja Melitniek    | 1 | ((181114-neutralizacja-artylerii-koszmarow)) |
| Gabriel Ursus        | 1 | ((200418-wojna-trzesawiska)) |
| Hestia d'Tiamenat    | 1 | ((190928-ostatnia-misja-tarna)) |
| Hieronim Maus        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Ida Tiara            | 1 | ((190406-bardzo-kosztowne-lzy)) |
| Julia Kardolin       | 1 | ((210615-skradziony-kot-olgi)) |
| Karolina Erenit      | 1 | ((190127-ixionski-transorganik)) |
| Karolina Terienak    | 1 | ((211102-satarail-pomaga-marysi)) |
| Karradrael           | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Kasjan Czerwoczłek   | 1 | ((190505-szczur-ktory-chroni)) |
| Kirył Najłalmin      | 1 | ((190127-ixionski-transorganik)) |
| Konrad Wączak        | 1 | ((200623-adaptacja-azalii)) |
| Krystian Namałłek    | 1 | ((190505-szczur-ktory-chroni)) |
| Ksenia Kirallen      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Liliana Bankierz     | 1 | ((210615-skradziony-kot-olgi)) |
| Lorena Gwozdnik      | 1 | ((211102-satarail-pomaga-marysi)) |
| Lucjusz Blakenbauer  | 1 | ((190505-szczur-ktory-chroni)) |
| Mirela Orion         | 1 | ((190721-kirasjerka-najgorszym-detektywem)) |
| Napoleon Bankierz    | 1 | ((190127-ixionski-transorganik)) |
| Oliwia Namałłek      | 1 | ((190505-szczur-ktory-chroni)) |
| Pedro Ronfak         | 1 | ((190928-ostatnia-misja-tarna)) |
| Sabina Kazitan       | 1 | ((200418-wojna-trzesawiska)) |
| Sławomir Muczarek    | 1 | ((190119-skorpipedy-krolewskiego-xirathira)) |
| Talia Aegis          | 1 | ((190928-ostatnia-misja-tarna)) |
| Teresa Mieralit      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Waldemar Mózg        | 1 | ((190119-skorpipedy-krolewskiego-xirathira)) |
| Wojtek Kurczynos     | 1 | ((190127-ixionski-transorganik)) |