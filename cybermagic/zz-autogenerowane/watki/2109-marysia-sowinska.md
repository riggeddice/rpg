# Marysia Sowińska
## Identyfikator

Id: 2109-marysia-sowinska

## Sekcja Opowieści

### Lea, strażniczka lasu

* **uid:** 240305-lea-strazniczka-lasu, _numer względny_: 24
* **daty:** 0111-10-21 - 0111-10-25
* **obecni:** Arkadia Verlen, Bogdan Gwiazdocisz, Ernest Namertel, Karolina Terienak, Keira Amarco d'Namertel, Lea Samszar, Malena Barandis, Marek Samszar, Mariusz Kupieczka, Marysia Sowińska, Michał Klabacz, Triana Porzecznik

Streszczenie:

Marysia zatrzymała Leę przed naruszaniem kulturowego tabu eternijskiego (stroju) i przy okazji dowiedziała się, że Lea próbuje pomóc innym, ale nikt nie może o tym wiedzieć. Tymczasem jej EfemeHorror poranił grupę Rekinów i AMZ współpracujących ze sobą w celu pomocy staremu 'archeologowi' szukającemu prawdy o ofiarach wojny.

Aktor w Opowieści:

* Dokonanie:
    * po wyjściu z łóżka Ernesta pomogła rozwiązać problem Lei, która łamie eternijskie tabu. Doszła do tego, że Lea próbuje pomóc Rekinom i ludziom, ale robi to tak by nie pokazać że pomaga. Doszła do tego, że Lea stoi za EfemeHorrorem z lasu i kazała Lei przestać to cholerstwo wzmacniać.


### Jak wsadzić Ulę Alanowi?

* **uid:** 220816-jak-wsadzic-ule-alanowi, _numer względny_: 23
* **daty:** 0111-10-12 - 0111-10-13
* **obecni:** Karolina Terienak, Marysia Sowińska, Mimoza Diakon, Tomasz Tukan, Urszula Miłkowicz

Streszczenie:

Części do Hestii nadal nie dotarły do Marysi - okazuje się, że Mimoza ją sabotuje. Więc jest ryzyko, że części do Hestii dojdą i Jeremi Sowiński będzie miał dostęp do pełni mocy Hestii a Marysia nie XD. Dodatkowo, Karo poprosiła Marysię by ta wsadziła Ulę Alanowi, ale nie chcą zabijać kota. Ale JAK wsadzić Alanowi Ulę (z kotem) jako uczennicę, skoro vistermin jest morderczy, zbliża się audyt Marysi a w Tukanie obudził się terminus (i nie chce ryzykować Uli)? I do tego Alan nie może się dowiedzieć? Do tego okazuje się że części do Hestii konkurują z normalnymi częściami potrzebnymi na tym terenie i DLATEGO Mimoza - paladynka cholerna - blokuje Marysię...

Aktor w Opowieści:

* Dokonanie:
    * skomplikowany problem - Mimoza blokuje części do Hestii bo ratuje region, Karo prosi o wsadzenie Uli Alanowi ale audyt i vistermin... Marysia zbiera dane i się nieco miota.


### Gdy prawnik przyjdzie po Rekiny

* **uid:** 220802-gdy-prawnik-przyjdzie-po-rekiny, _numer względny_: 22
* **daty:** 0111-10-09 - 0111-10-11
* **obecni:** Ernest Namertel, Hipolit Umadek, Marysia Sowińska

Streszczenie:

Marysia w końcu założyła hold na Erneście i się doń wprowadziła. Ernest zerwał z Amelią i plotki dotarły do niej czemu. Tymczasem plany Marsena sprawiają, że Ernest zaczyna być coraz bardziej zły na Mimozę (co Marysia neutralizuje) a Hipolit, prawnik z AMZ, chce by Marysia kontrolowała Rekiny skoro już Enklawa jest eksterytorialna. Albo coś się będzie musiało zmienić.

Aktor w Opowieści:

* Dokonanie:
    * wprowadziła się do Ernesta, splątała go ze sobą i przekierowała na siebie jego uczucia. Jednocześnie przekonała go, żeby nie atakował Mimozy tylko dał jej rozwiązać sprawę.
* Progresja:
    * zamieszkała w Apartamencie Ernesta, "by mieć nań większy wpływ". Rekiny uważają że to dobry pomysł.


### Płaszcz ochronny Mimozy

* **uid:** 220222-plaszcz-ochronny-mimozy, _numer względny_: 21
* **daty:** 0111-09-21 - 0111-09-25
* **obecni:** Ernest Namertel, Karolina Terienak, Keira Amarco d'Namertel, Lorena Gwozdnik, Marysia Sowińska, Mimoza Elegancja Diakon

Streszczenie:

Mimoza weszła do akcji - wzięła Lorenę do azylu i ochroniła część kultystek przed Ernestem. Konflikt Ernest - Mimoza się rozpalił, Marysia + Karo poszły się z Mimozą spotkać by zdeeskalować problem. Lorena podsłuchiwała, więc Mimoza straciła twarz. Cóż. Mimoza wynegocjowała przekazanie kultystek Sensacjuszowi, Marysia wynegocjowała by Lorena zrobiła ten akt. ALE W DOMU MIMOZY. Zaczyna się ciekawie...

Aktor w Opowieści:

* Dokonanie:
    * by doprowadzić do stabilizacji Ernesta, chce zapewnić by Lorena narysowała ten obiecany akt XD. Deeskaluje konflikt Mimoza - Ernest (tymczasowo), ale stabilizuje relację swoją z Mimozą.


### Marysiowa Hestia Rekinów

* **uid:** 220111-marysiowa-hestia-rekinow, _numer względny_: 20
* **daty:** 0111-09-16 - 0111-09-19
* **obecni:** Diana Tevalier, Ernest Namertel, Hestia d'Rekiny, Jeremi Sowiński, Karolina Terienak, Keira Amarco d'Namertel, Liliana Bankierz, Marysia Sowińska

Streszczenie:

Konflikt Liliana - Marysia się zaostrza. Marysia skupia się na wzmocnieniu Hestii i uzyskaniu kontroli nad Rekinami - przejęła Hestię jako sojuszniczkę. Chevaleresse poprosiła Karolinę o uratowanie Melissy i Karo faktycznie pomogła - Ernest wysłał Keirę i odbili Melissę. To sprawiło, że Mimoza Diakon poczuła się zagrożona i weszła do akcji. Jakby tego było mało, Sowińscy wysyłają Jeremiego by opanował kuzynkę.

Aktor w Opowieści:

* Dokonanie:
    * zamawia wzmocnienia dla Hestii, znajduje dziewczynę dla Myrczka i doprowadza do silnej korupcji Hestii - Hestia jest jej przyjaciółką i sojuszniczką teraz.
* Progresja:
    * Rekiny widzą, że Marysia stoi za "państwem policyjnym" wśród Rekinów.


### Akt, o którym Marysia nie wie

* **uid:** 211228-akt-o-ktorym-marysia-nie-wie, _numer względny_: 19
* **daty:** 0111-09-09 - 0111-09-12
* **obecni:** Daniel Terienak, Ernest Namertel, Karolina Terienak, Keira Amarco d'Namertel, Liliana Bankierz, Lorena Gwozdnik, Marysia Sowińska, Napoleon Bankierz, Rafał Torszecki

Streszczenie:

Torszecki ostrzegł Marysię, że jej akt krąży w Zaczęstwie. Ona to olała. Ale jak Ernest zaczął na niego polować to Marysia się zainteresowała. Zdecydowała się aktem odbić Ernesta Amelii. Zaflirtowała z Ernestem, obiecała mu akt specjalnie dla niego i że odda mu oryginał tego. Karo pokonała Napoleona, znalazła link do aktu który ostatecznie zdobył Torszecki. Daniel dla Karo zbił Torszeckiego i odzyskał akt. Marysia zaflirtowała z Ernestem ;-). Torszecki, który chciał dobrze i chronił Marysię skończył w szpitalu.

Aktor w Opowieści:

* Dokonanie:
    * gdy dowiedziała się, że jej akt krąży po Zaczęstwie to wykorzystała go do sprawienia, by Ernest nie chciał spać po nocy ;-). Uwodzi Ernesta, by odbić go Amelii. Dla niej Daniel zbił Torszeckiego który ją chronił.
* Progresja:
    * wyciągnęła od Liliany Bankierz informację pośrednio (trackerem) kto jest autorem aktu Marysi (Lorena). Przez to zaskarbiła sobie jej wściekłość i zemstę.


### Chevaleresse infiltruje Rekiny

* **uid:** 211221-chevaleresse-infiltruje-rekiny, _numer względny_: 18
* **daty:** 0111-09-06 - 0111-09-07
* **obecni:** Alan Bartozol, Barnaba Burgacz, Diana Tevalier, Hestia d'Rekiny, Justynian Diakon, Karolina Terienak, Marysia Sowińska, Melissa Durszenko, Rupert Mysiokornik, Santino Mysiokornik, Staś Arienik, Żorż d'Namertel

Streszczenie:

Hestia zaproponowała Marysi naprawienie Rekin Defense Grid - i od razu wykryli osoby spoza Rekinów i co najmniej jedną infiltratorkę. To Chevaleresse, która szuka Stasia Arienika by Alan nie musiał iść na Trzęsawisko. Karo i Marysia złapały Chevaleresse, w trójkę się dogadały. Karo przechwyciła Stasia (któremu pomagał Babu) i oddała go Alanowi zrzutem ze ścigacza. Justynian skierował oczy na Kult Ośmiornicy który podobno jest w Dzielnicy Rekinów.

Aktor w Opowieści:

* Dokonanie:
    * obserwuje Chevaleresse przez Hestię i z nią dyskutuje. Przekonała Justyniana, że trzeba zmiażdżyć Kult Ośmiornicy i odzyskać Stasia Arienika.


### Gdy zabraknie prądu Rekinom

* **uid:** 211207-gdy-zabraknie-pradu-rekinom, _numer względny_: 17
* **daty:** 0111-08-30 - 0111-08-31
* **obecni:** Arkadia Verlen, Arnold Kłaczek, Daniel Terienak, Henryk Wkrąż, Hestia d'Rekiny, Karolina Terienak, Lorena Gwozdnik, Marysia Sowińska, Natalia Tessalon, Sensacjusz Diakon, Urszula Arienik

Streszczenie:

Ponieważ Amelia nie przekazała Marysi informacji o tym, że ta powinna przejąć obowiązki zarządcze to Marysia jako administratorka nie dostawała informacji. Przez to odcięło im prąd. Marysia przywróciła prąd podpisując (standardową) umowę z dystrybutorem podwierckim a Karolina uratowała ludzką część Dzielnicy Rekinów od pobicia przez wkurzonych magów. Próba obniżenia rachunków przez współpracę z mafią została przez dziewczyny zażegnana używając Arkadii.

Aktor w Opowieści:

* Dokonanie:
    * nie wiedziała, że musi być Oficjalnym Przedstawicielem Aurum (dzięki, Amelia), więc nim nie była i odcięli prąd. Szybko załatwiła prąd z innego źródła i deeskalowała ryzyko, że ktoś na nią spojrzy; zrzuciła ogień na Arieników.
* Progresja:
    * została Oficjalnym Przedstawicielem Aurum wśród Rekinów. Współpracuje z nią Hestia d'Rekiny.


### Odbudowa według Justyniana

* **uid:** 211123-odbudowa-wedlug-justyniana, _numer względny_: 16
* **daty:** 0111-08-11 - 0111-08-20
* **obecni:** Azalia Sernat d'Namertel, Daniel Terienak, Ernest Namertel, Ignacy Myrczek, Justynian Diakon, Karol Pustak, Karolina Terienak, Marysia Sowińska, Rafał Torszecki

Streszczenie:

Marysia się tymczasowo wprowadziła do Ernesta, by go trochę uspokoić i przekonać do Rekinów. Tymczasem Justynian Diakon przejął kontrolę operacyjną i zaczął odbudowywać Dzielnicę Rekinów po ruinie jaką zrobił tam Wiktor Satarail. Marysia skontaktowała się z Justynianem i przekazała mu plan Azalii d'Namertel jako swój, by odzyskać kontrolę i pozycję. Justynian zaakceptował jej plan i powiedział jej, że Torszeckiego biją. Do tego Marysię odwiedził Pustak jako herold Myrczka - prosi, by Marysia skontaktowała go z Sabiną Kazitan, miłością Myrczka...

Aktor w Opowieści:

* Dokonanie:
    * tymczasowo przeprowadziła się do Ernesta; przekonała go do dania szansy Rekinom. Dowiedziawszy się o działaniach Justyniana pokazała mu, że ma lepszy plan od niego (plan Azalii). Będzie walczyć o utrzymanie władzy.
* Progresja:
    * podobno jest "zabawką" Ernesta bo on jest taki dobry w łóżku.
    * stworzyła tak dobry plan odbudowy Dzielnicy Rekinów, że Rekiny uznają jej wartość (plan wymyśliła Azalia ale Marysia go przywłaszczyła).


### Satarail pomaga Marysi

* **uid:** 211102-satarail-pomaga-marysi, _numer względny_: 15
* **daty:** 0111-08-09 - 0111-08-10
* **obecni:** Ernest Namertel, Karolina Terienak, Lorena Gwozdnik, Marek Samszar, Marysia Sowińska, Rafał Torszecki, Sensacjusz Diakon, Wiktor Satarail

Streszczenie:

Satarail uderzył - zainfekował Owadem który wypełzł z Torszeckiego Samszara (który kiedyś ukradł somnibela Olgi). Zainfekowane Rekiny zaczęły dewastować dzielnicę. Karo skutecznie rozwala owady (i Lorenę), po czym Sensacjusza w glistę medyczną. Marysia samodzielnie leci jako owad wyłączyć Samszara. Torszecki jest rozgrzeszony - "nie był sobą". Ale polowanie na Torszeckiego czas zacząć.

Aktor w Opowieści:

* Dokonanie:
    * wyczaiła, że z Sensacjuszem jest coś nie tak (infekcja Owadem), ściągnęła Karolinę i z Karo wrzuciła go do glisty (sama wpadając). Sama poleciała jako owad pokonać zOwadzonego Samszara. Ona chce naprawić to SAMA.
* Progresja:
    * zdjęcie w śluzie od glisty trafiło do prasy. Są plotki, że wszystko dla Torszeckiego. Rekiny kibicują cicho ich zakazanej miłości (czyli Torszeckiemu).


### Koszt ratowania Torszeckiego

* **uid:** 211026-koszt-ratowania-torszeckiego, _numer względny_: 14
* **daty:** 0111-08-01 - 0111-08-05
* **obecni:** Ignacy Myrczek, Ksenia Kirallen, Marysia Sowińska, Olga Myszeczka, Paweł Szprotka, Rafał Torszecki, Sensacjusz Diakon, Teresa Mieralit, Wiktor Satarail

Streszczenie:

By ratować Torszeckiego, Marysia wchodzi w sojusz z Wiktorem Satarailem. On dał jej podskórnego robaka którego dostanie Torszecki, by uzasadnić dziwne zachowanie. Wiktor "dostanie swoją zapłatę" od "kogoś winnego". Marysia przekonała też Sensacjusza, że ona x Torszecki. Zaczęły się też pojawiać takie plotki...

Aktor w Opowieści:

* Dokonanie:
    * wzięła Pawła Szprotkę pod ochronę przed Eternią, po czym wynegocjowała z Satarailem, że on da jej Robaka do zainfekowania Torszeckiego (by go chronić). Wszystko zaczyna wyglądać jakby podkochiwała się w Torszeckim.
* Progresja:
    * pojawiają się plotki na temat jej romansu z Torszeckim. Na razie ignoruje.
    * Sensacjusz Diakon uważa ją za zagubioną młodą kobietę, ale zdolną do miłości. Nie to co Amelia. Czyli Sensacjusz jest potencjalnym sojusznikiem.


### Torszecki pokazał kręgosłup

* **uid:** 211012-torszecki-pokazal-kregoslup, _numer względny_: 13
* **daty:** 0111-07-29 - 0111-07-30
* **obecni:** Amelia Sowińska, Jolanta Sowińska, Karolina Terienak, Lucjan Sowiński, Marysia Sowińska, Rafał Torszecki, Sensacjusz Diakon

Streszczenie:

Marysia dostała nową "prośbę" ze strony Dworu Sowińskich - pozyskać próbkę krwi "córki Morlana" z Pustogoru. Tymczasem Karolina poszła do Torszeckiego (do lecznicy) by ten wreszcie miał kręgosłup. Wymusiła na nim pomoc w znalezieniu mordercy ścigacza. Torszecki nie chcąc krzywdy Karoliny się jej przyznał - to on. Chciał zrobić Ernest vs Mafia by chronić Marysię; skrzyżował w głowie, że Ernest x Amelia to Esuriit, więc to samo spotka biedną Marysię... Karo i Marysia mają teraz problem - jak to rozplątać?

Aktor w Opowieści:

* Dokonanie:
    * dostała nowe zadanie od Dworu Sowińskich - zdobyć krew "córki Morlana" ze szpitala Pustogorskiego. Dotarło do niej, że jej traktowanie Torszeckiego częściowo spowodowało tą katastrofę ze zniszczeniem ścigacza Ernesta.


### Wysadzony żywy ścigacz

* **uid:** 210928-wysadzony-zywy-scigacz, _numer względny_: 12
* **daty:** 0111-07-26 - 0111-07-27
* **obecni:** Azalia Sernat d'Namertel, Ernest Namertel, Franek Bulterier, Karolina Terienak, Keira Amarco d'Namertel, Marysia Sowińska, Rafał Torszecki, Żorż d'Namertel

Streszczenie:

Ernest z Eterni okazał się być zupełnie inny niż się wydaje. Lekko naiwny, z paladyńskim zacięciem, chce czynić dobro i integrować Rekiny dookoła Marysi. Chce dokończyć dobre dzieło Amelii. Marysia znalazła z nim linię porozumienia a Ernest x Karolina są jak dwa łyse konie. Marysia przekonała go, by sam nic nie robił - nie rozumie terenu. Niestety, ktoś wysadził żywy ścigacz i zabił ducha z jego arkinu. Ernesta zatrzymała Marysia. Karolinie nie udało się ducha uratować. Dziewczyny zdecydowały, że znajdą mordercę...

Aktor w Opowieści:

* Dokonanie:
    * przekonała do siebie Ernesta i używając Torszeckiego wyciągnęła z Ernesta jego sekrety i relację z Amelią. Przekonała go też, żeby nie robił nic sam. Gdy Ernest stracił nad sobą kontrolę, zatrzymała manifestację simulacrum i przekonała (ponownie) by oddał jej śledztwo. Dobija ją porównanie z Amelią - "że obie są paladynkami". Nonsens. Amelia nie jest ;p.
* Progresja:
    * ma przyjaźń i zaufanie Ernesta Namertela.


### Przybycie Rekina z Eterni

* **uid:** 210921-przybycie-rekina-z-eterni, _numer względny_: 11
* **daty:** 0111-07-20 - 0111-07-25
* **obecni:** Amelia Sowińska, Arkadia Verlen, Ernest Namertel, Jolanta Sowińska, Karol Pustak, Karolina Terienak, Lucjan Sowiński, Marysia Sowińska, Nataniel Morlan, Rafał Torszecki, Tomasz Tukan

Streszczenie:

Do Rekinów dołącza tien z Eterni. Marysia dowiaduje się, że to kwestia intryg jej kuzynki, Amelii, próbującej deeskalować problemy na linii Morlan - Jolanta. Dowiedziała się od Amelii, że to nie Amelia stała za morderstwami w Podwiercie, kogoś chroni. Oraz... że Amelia jest zakochana (Amelia nie wie że Marysia wie). Ale na Marysię spadła ochrona Ernesta. Więc... usunęła wszystkie zagrożenia o których pomyślała (Arkadia) i zaczęła działania mające poprawić reputację Eternianina, by Rekiny go przyjęły.

Aktor w Opowieści:

* Dokonanie:
    * odkryła sekrety Amelii Sowińskiej - zarówno ten o zakazanej miłości jak i to, że to nie Amelia zabiła ludzi. Zaczęła pracę nad akceptacją Ernesta wśród Rekinów i manipulacjami pousuwała większość oczywistych zagrożeń. Okazuje się, że ma kuzyna Lucjana który ją lubi.
* Progresja:
    * osobiście odpowiedzialna by Ernestowi Namertelowi nic się nie stało (manipulacja Amelii). Jej słaby punkt.
    * ma świetny materiał do szantażu / mezaliansu - Amelia Sowińska kocha się w Erneście Namertelu z Eterni, a przecież Amelia ma dostęp do rzeczy których Eternia nie powinna nigdy wiedzieć.


### Serafina staje za Wydrami

* **uid:** 210831-serafina-staje-za-wydrami, _numer względny_: 10
* **daty:** 0111-07-07 - 0111-07-10
* **obecni:** Daniel Terienak, Halina Sermniek, Karolina Terienak, Laura Tesinik, Lorena Gwozdnik, Marysia Sowińska, Rafał Torszecki, Serafina Ira

Streszczenie:

Karolina MUSI poznać prawdę co się stało z jej bratem. Odkryła, że istnieje grupa Wydry polująca na Rekiny, ludzi skrzywdzonych przez Rekiny w przeszłości. I pomaga im Serafina Ira. Z Marysią porozmawiały z Serafiną i przekonały ją, by ta działała wolniej - i drugą linią napuściły na Serafinę terminusów z Pustogoru.

Aktor w Opowieści:

* Dokonanie:
    * używa systemów Sowińskich by odkryć informację o Serafinie irze, przy użyciu Torszeckiego się z nią spotkała i przekonała do spowolnienia wspierania Wydr przeciw Rekinom.


### Mandragora nienawidzi Rekinów

* **uid:** 210824-mandragora-nienawidzi-rekinow, _numer względny_: 9
* **daty:** 0111-07-03 - 0111-07-05
* **obecni:** Amelia Sowińska, Daniel Terienak, Ekaterina Zajcew, Karolina Terienak, Laura Tesinik, Marysia Sowińska, Sensacjusz Diakon, Tomasz Tukan

Streszczenie:

Mandragora nienawidzi Rekinów i jej ofiarą został Damian Terienak. Idąc za jego śladem Marysia i Karolina znalazły mandragorę i pnączoszpona oraz z pomocą dwóch uczennic terminusa rozwaliły problem. Dowiedziały się, że mandragora konkretnie celuje w Amelię Sowińską, która opuściła ten teren dawno temu (i która była dość okrutna).

Aktor w Opowieści:

* Dokonanie:
    * wpierw przeskanowała umysł nieprzytomnego Daniela by dowiedzieć się co go "zeżarło", potem broniąc Ekaterinę przed mandragorą postawiła jej potężną tarczę mentalną. Chroni Rekiny przed mandragorą z powinności i z ciekawości.
* Progresja:
    * w oczach Rekinów Marysia wygnała Kacpra by przejąć mafię.
    * Amelia jej na tyle nie lubiła, że wywaliła ją na prowincję by była Rekinem.
    * Sensacjusz Diakon jest święcie przekonany, że plotki o mafii x Marysi to 100% prawda. I uważa, że Marysia jest jak Amelia.


### Zgubiony holokryształ w lesie

* **uid:** 210817-zgubiony-holokrysztal-w-lesie, _numer względny_: 8
* **daty:** 0111-06-22 - 0111-06-24
* **obecni:** Alan Bartozol, Cyryl Perikas, Kacper Bankierz, Laura Tesinik, Liliana Bankierz, Mariusz Trzewń, Marysia Sowińska, Rupert Mysiokornik, Stella Armadion, Tomasz Tukan

Streszczenie:

Jeden z przerzucanych przez mafię przedmiotów do Aurum to ko-matryca Kuratorów, jako broń. Ale Cyryl - kurier - był ofiarą terminuskiego polowania więc ją odrzucił w Lesie Trzęsawnym gdzie znalazła ją Stella przekonana, że to noktiańska technologia. Marysia wypchnęła Kacpra Bankierza z mafii (i tego terenu) a Tukan znalazł i usunął ko-matrycę m.in. używając Laury.

Aktor w Opowieści:

* Dokonanie:
    * dowiedziawszy się o ko-matrycy Kuratorów wypchnęła Kacpra Bankierza z mafii (i tego terenu). Potem przekazała Tukanowi wolną rękę - niech usunie ko-matrycę i uderzy w mafię.


### Porwanie Daniela Terienaka

* **uid:** 210720-porwanie-daniela-terienaka, _numer względny_: 7
* **daty:** 0111-06-14 - 0111-06-16
* **obecni:** Daniel Terienak, Karolina Terienak, Mariusz Trzewń, Marysia Sowińska, Rafał Torszecki, Tomasz Tukan, Urszula Miłkowicz

Streszczenie:

Urszula Miłkowicz zwabiła w pułapkę i porwała Daniela Terienaka - z nadzieją, że odkryje Rekina który współpracuje z mafią. Siostra Daniela, Karolina, pobiła Torszeckiego i tak spotkała się z Marysią. Wspólnie odkryły że za wszystkim stoi Ula i zmusiły ją do oddania Daniela bez przeskanowania pamięci (by się upewnić, Karolina wysadziła techbunker). Marysia dowiedziała się, że Ula jest uciekinierką z Aurum i eks-protomagiem.

Aktor w Opowieści:

* Dokonanie:
    * uratowała Torszeckiego przed Karoliną. Wykryła, że brata Karoliny (Daniela) porwała Ula. Zmusiła Ulę do współpracy politycznym manewrem i zaintrygowana terminuską doszła do tego kim Ula jest i skąd ta nienawiść.


### Rekin wspiera mafię

* **uid:** 210713-rekin-wspiera-mafie, _numer względny_: 6
* **daty:** 0111-06-06 - 0111-06-09
* **obecni:** Julia Kardolin, Kacper Bankierz, Marysia Sowińska, Rafał Torszecki, Tomasz Tukan, Urszula Miłkowicz

Streszczenie:

Gabriel Ursus powiedział Marysi, że jakiś Rekin uczestniczy w przemycie nielegalnych rzeczy w Podwiercie, na terenie eksterytorialnym. I będzie rajd i przechwycą te Rekiny. Marysia dotarła do tego, że są tajne magazyny (chronione przez anomalię) pod kontrolą Kacpra Bankierza. Rozmowa z Kacprem nic nie dała, więc Marysia doprowadziła do uwolnienia Arkadii z więzienia i zrobiła fałszywe ślady - to Kacper stał za jej odurzeniem. Plus, Marysia odbudowała kontakt z Tukanem...

Aktor w Opowieści:

* Dokonanie:
    * władczyni marionetek; Torszeckim znalazła Kacpra Bankierza, gdy ów nie chciał przestać współpracować z mafią to używając Uli dotarła do Tukana, uwolniła nim Arkadię i poszczuła nią Kacpra.


### Verlenka na grzybkach

* **uid:** 210622-verlenka-na-grzybkach, _numer względny_: 5
* **daty:** 0111-05-30 - 0111-05-31
* **obecni:** Arkadia Verlen, Ignacy Myrczek, Julia Kardolin, Liliana Bankierz, Marek Samszar, Marysia Sowińska, Rafał Torszecki, Różewicz Diakon, Triana Porzecznik, Urszula Miłkowicz

Streszczenie:

Zespół AMZ składał projekt zaliczeniowy, gdy zaatakowała Arkadia Verlen na grzybkach ze swojego Lancera, uszkadzając projekt (robota). Julia wezwała Ulę (terminuskę) na pomoc, Ula unieszkodliwiła Marka (który podmienił Arkadię). Jako, że Julia i Triana były zajęte ratowaniem projektu, Marysia rzuciła się na znalezienie "kto atakuje Julię". Dotarła do niewinności Marka, dotarła do Arkadii i z pomocą Uli unieszkodliwiła Arkadię. Jedyne straty - ranna Ula i ranny Torszecki. Ale kto podał Arkadii te grzybki i jak?

Aktor w Opowieści:

* Dokonanie:
    * przeprowadziła dochodzenie - kto chciał zestrzelić robota kultywacyjnego Julii? Znalazła Arkadię na grzybkach i współpracując z Ulą ją unieszkodliwiła podstępem i fortelem.
* Progresja:
    * jawnie kolaboruje z terminusami wykorzystując ich jako broń przeciwko Arkadii. "Wrogowie Sowińskich kończą w pierdlu". Jest bezwzględną Sowińską.


### Skradziony kot Olgi

* **uid:** 210615-skradziony-kot-olgi, _numer względny_: 4
* **daty:** 0111-05-13 - 0111-05-15
* **obecni:** Arkadia Verlen, Julia Kardolin, Liliana Bankierz, Marek Samszar, Marysia Sowińska, Olga Myszeczka, Paweł Szprotka, Wiktor Satarail

Streszczenie:

Marysia i Julia mają imprezę karaoke w Lesie Trzęsawnym, ale Marek nie dotarł - walczy na magię z Lilianą. Julia puściła drony Triany, które wpadły w berserk. By Tymon się nie zainteresował tym co się dzieje w lesie, Marysia i Julia chciały rozwiązać problem - okazało się, że Marek ukradł somnibela Oldze Myszeczce i dał go Arkadii Verlen w prezencie. Marysia i Julia przekonały Arkadię do oddania kota, oddały go Oldze, po czym Arkadia zerwała z Markiem i go ciężko pobiła. Acz Paweł Szprotka (który próbował odzyskać kota od Marka i którego broniła Liliana) boi się o życie, bo niektóre Rekiny się na niego uwzięły za karę...

Aktor w Opowieści:

* Dokonanie:
    * ratuje swoje karaoke deeskalując sytuację w Lesie Trzęsawnym oraz odkrywa, że Marek ukradł somnibela Oldze. Przekonała Arkadię do oddania somnibela.


### Porywaczka miragentów

* **uid:** 210518-porywaczka-miragentow, _numer względny_: 3
* **daty:** 0111-05-07 - 0111-05-08
* **obecni:** Diana Lemurczak, Julia Kardolin, Lorena Gwozdnik, Marysia Sowińska, Triana Porzecznik

Streszczenie:

Lorena chciała zaimponować Gerwazemu i wysłała na Dianę miragenta. Diana przejęła miragenta, ale ten w rozpaczy ostrzelał Marysię Sowińską. Zespół znalazł Lorenę, zestrzelił ją (berserkerskimi dronami Triany) i poznał prawdę o miragencie, po czym udał się do klubu Arkadia w Podwiercie i odebrał Dianie miragenta. Nie powiedzieli nikomu innemu, że wiedzą o jej Eidolonie.

Aktor w Opowieści:

* Dokonanie:
    * ostrzelana przez miragenta zrobiła świetne uniki, przeraziła Lorenę i zmusiła ją do wyznania prawdy o miragencie i przekonała Dianę do oddania jej miragenta. Ogólnie, dobry dzień ;-).
* Progresja:
    * ma dostęp do superkomputera Sowińskich w swoich apartamentach w Podwiercie. A tam większość plotek z Aurum ;-).


### Potencjalnie eksterytorialny seksbot

* **uid:** 210406-potencjalnie-eksterytorialny-seksbot, _numer względny_: 2
* **daty:** 0111-04-27 - 0111-04-28
* **obecni:** Barnaba Burgacz, Feliks Keksik, Franek Bulterier, Henryk Wkrąż, Julia Kardolin, Lorena Gwozdnik, Marysia Sowińska, Rupert Mysiokornik, Sabina Kazitan, Triana Porzecznik

Streszczenie:

Sabina Kazitan zostawiła za sobą plan mający skonfliktować Rekiny na poziomie Podwiertu, co miałoby kaskadować na relacje rodów w Aurum. Plan zawierał Skażonego seksbota, którego Rekiny miały same zrobić. Podszepnęła też odpowiednie słówka odpowiednim osobom. Jednak gdy podatne Rekiny chciały iść za tym planem, gdy szabrowały ze złomowiska głowę seksbota to zatrzymał je Zespół. Skończyło się na tym, że seksbota nie będzie - Marysia im absolutnie zakazała cokolwiek robić, nawet, jeśli nie znała planu Sabiny.

Aktor w Opowieści:

* Dokonanie:
    * rok temu uratowała Pożeracza przed torturami ze strony Sabiny a 3 miesiące temu zatrzymała agresywnie napalonego Mysiokornika każąc Lorenie go upić. Teraz: zakazała używać rytuałów na seksboty.


### Grzybopreza

* **uid:** 210323-grzybopreza, _numer względny_: 1
* **daty:** 0111-04-22 - 0111-04-23
* **obecni:** Ignacy Myrczek, Julia Kardolin, Kacper Bankierz, Marysia Sowińska, Napoleon Bankierz, Triana Porzecznik, Urszula Miłkowicz

Streszczenie:

Trianie "uciekł" źle skonfigurowany techno-pies ratujący ludzi. Marysia Sowińska (Rekin) porwała Myrczka na pomysł Julii, zrobili super-trufle i Triana z Julią dotarły do psa i go unieczynniły. Tymczasem Marysia i Myrczek wymanewrowały Ulę (uczennicę terminusa) i sprowokowali ją do wezwania wsparcia przeciw ad-hocowej czystej grzyboprezie Rekinów. Nawet Pakt o tym napisał.

Aktor w Opowieści:

* Dokonanie:
    * porwała Myrczka z okna akademika, wymanewrowała Napoleona na ścigaczu (wrzucając go do jeziora), po czym zorganizowała grzyboprezę i ściągnęła na nią Kacpra Bankierza - na złość terminusce.
* Progresja:
    * Urszula Miłkowicz, uczennica terminusa ma z nią kosę. Ale Kacper Bankierz ma u niej duży szacunek za pojechanie Napoleona a potem wpakowanie terminusów w "pułapkę" czystą grzyboprezą Myrczka.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 24, @: 0111-10-25
    1. Primus    : 24, @: 0111-10-25
        1. Sektor Astoriański    : 24, @: 0111-10-25
            1. Astoria    : 24, @: 0111-10-25
                1. Sojusz Letejski    : 24, @: 0111-10-25
                    1. Szczeliniec    : 24, @: 0111-10-25
                        1. Powiat Pustogorski    : 24, @: 0111-10-25
                            1. Czarnopalec    : 2, @: 0111-08-05
                                1. Pusta Wieś    : 2, @: 0111-08-05
                            1. Czółenko    : 2, @: 0111-08-31
                                1. Generatory Keriltorn    : 1, @: 0111-08-31
                            1. Podwiert    : 23, @: 0111-10-25
                                1. Dzielnica Luksusu Rekinów    : 23, @: 0111-10-25
                                    1. Fortyfikacje Rolanda    : 4, @: 0111-09-25
                                    1. Obrzeża Biedy    : 8, @: 0111-09-25
                                        1. Domy Ubóstwa    : 4, @: 0111-09-25
                                        1. Hotel Milord    : 4, @: 0111-09-25
                                        1. Stadion Lotników    : 6, @: 0111-09-25
                                        1. Stajnia Rumaków    : 3, @: 0111-09-25
                                    1. Sektor Brudu i Nudy    : 6, @: 0111-09-25
                                        1. Komputerownia    : 4, @: 0111-09-25
                                        1. Konwerter Magielektryczny    : 3, @: 0111-09-25
                                        1. Magitrownia Pogardy    : 3, @: 0111-09-25
                                        1. Skrytki Czereśniaka    : 5, @: 0111-09-25
                                    1. Serce Luksusu    : 15, @: 0111-10-11
                                        1. Apartamentowce Elity    : 10, @: 0111-10-11
                                        1. Arena Amelii    : 5, @: 0111-09-25
                                        1. Fontanna Królewska    : 3, @: 0111-09-25
                                        1. Kawiarenka Relaks    : 3, @: 0111-09-25
                                        1. Lecznica Rannej Rybki    : 8, @: 0111-09-25
                                1. Kompleks Korporacyjny    : 1, @: 0111-08-31
                                    1. Dystrybutor Prądu Ozitek    : 1, @: 0111-08-31
                                    1. Elektrownia Węglowa Szarpien    : 1, @: 0111-08-31
                                1. Las Trzęsawny    : 11, @: 0111-10-25
                                    1. Jeziorko Mokre    : 2, @: 0111-07-10
                                    1. Schron TRZ-17    : 1, @: 0111-05-15
                                1. Osiedle Leszczynowe    : 1, @: 0111-05-08
                                    1. Klub Arkadia    : 1, @: 0111-05-08
                                1. Osiedle Tęczy    : 1, @: 0111-07-10
                            1. Pustogor    : 2, @: 0111-08-10
                                1. Rdzeń    : 2, @: 0111-08-10
                                    1. Barbakan    : 1, @: 0111-05-31
                                        1. Kazamaty    : 1, @: 0111-05-31
                                    1. Szpital Terminuski    : 1, @: 0111-08-10
                            1. Zaczęstwo    : 10, @: 0111-09-25
                                1. Akademia Magii, kampus    : 5, @: 0111-09-25
                                    1. Akademik    : 5, @: 0111-09-25
                                    1. Arena Treningowa    : 3, @: 0111-09-25
                                    1. Las Trzęsawny    : 3, @: 0111-09-25
                                1. Biurowiec Gorland    : 1, @: 0111-04-23
                                1. Dzielnica Kwiecista    : 3, @: 0111-05-08
                                    1. Rezydencja Porzeczników    : 3, @: 0111-05-08
                                        1. Podziemne Laboratorium    : 1, @: 0111-04-23
                                1. Las Trzęsawny    : 1, @: 0111-05-31
                                1. Nieużytki Staszka    : 3, @: 0111-05-31
                                1. Osiedle Ptasie    : 1, @: 0111-06-16
                                1. Park Centralny    : 1, @: 0111-04-23
                                    1. Jezioro Gęsie    : 1, @: 0111-04-23
                                1. Złomowisko    : 1, @: 0111-04-28

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolina Terienak    | 15 | ((210720-porwanie-daniela-terienaka; 210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami; 210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211012-torszecki-pokazal-kregoslup; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211207-gdy-zabraknie-pradu-rekinom; 211221-chevaleresse-infiltruje-rekiny; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 220816-jak-wsadzic-ule-alanowi; 240305-lea-strazniczka-lasu)) |
| Rafał Torszecki      | 11 | ((210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka; 210831-serafina-staje-za-wydrami; 210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211012-torszecki-pokazal-kregoslup; 211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Ernest Namertel      | 9 | ((210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 220802-gdy-prawnik-przyjdzie-po-rekiny; 240305-lea-strazniczka-lasu)) |
| Lorena Gwozdnik      | 7 | ((210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow; 210831-serafina-staje-za-wydrami; 211102-satarail-pomaga-marysi; 211207-gdy-zabraknie-pradu-rekinom; 211228-akt-o-ktorym-marysia-nie-wie; 220222-plaszcz-ochronny-mimozy)) |
| Daniel Terienak      | 6 | ((210720-porwanie-daniela-terienaka; 210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami; 211123-odbudowa-wedlug-justyniana; 211207-gdy-zabraknie-pradu-rekinom; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Julia Kardolin       | 6 | ((210323-grzybopreza; 210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow; 210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie)) |
| Tomasz Tukan         | 6 | ((210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka; 210817-zgubiony-holokrysztal-w-lesie; 210824-mandragora-nienawidzi-rekinow; 210921-przybycie-rekina-z-eterni; 220816-jak-wsadzic-ule-alanowi)) |
| Arkadia Verlen       | 5 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 210921-przybycie-rekina-z-eterni; 211207-gdy-zabraknie-pradu-rekinom; 240305-lea-strazniczka-lasu)) |
| Keira Amarco d'Namertel | 5 | ((210928-wysadzony-zywy-scigacz; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 240305-lea-strazniczka-lasu)) |
| Liliana Bankierz     | 5 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 210817-zgubiony-holokrysztal-w-lesie; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow)) |
| Sensacjusz Diakon    | 5 | ((210824-mandragora-nienawidzi-rekinow; 211012-torszecki-pokazal-kregoslup; 211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi; 211207-gdy-zabraknie-pradu-rekinom)) |
| Triana Porzecznik    | 5 | ((210323-grzybopreza; 210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow; 210622-verlenka-na-grzybkach; 240305-lea-strazniczka-lasu)) |
| Urszula Miłkowicz    | 5 | ((210323-grzybopreza; 210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka; 220816-jak-wsadzic-ule-alanowi)) |
| Ignacy Myrczek       | 4 | ((210323-grzybopreza; 210622-verlenka-na-grzybkach; 211026-koszt-ratowania-torszeckiego; 211123-odbudowa-wedlug-justyniana)) |
| Marek Samszar        | 4 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 211102-satarail-pomaga-marysi; 240305-lea-strazniczka-lasu)) |
| Amelia Sowińska      | 3 | ((210824-mandragora-nienawidzi-rekinow; 210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Hestia d'Rekiny      | 3 | ((211207-gdy-zabraknie-pradu-rekinom; 211221-chevaleresse-infiltruje-rekiny; 220111-marysiowa-hestia-rekinow)) |
| Kacper Bankierz      | 3 | ((210323-grzybopreza; 210713-rekin-wspiera-mafie; 210817-zgubiony-holokrysztal-w-lesie)) |
| Laura Tesinik        | 3 | ((210817-zgubiony-holokrysztal-w-lesie; 210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami)) |
| Rupert Mysiokornik   | 3 | ((210406-potencjalnie-eksterytorialny-seksbot; 210817-zgubiony-holokrysztal-w-lesie; 211221-chevaleresse-infiltruje-rekiny)) |
| Wiktor Satarail      | 3 | ((210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi)) |
| Alan Bartozol        | 2 | ((210817-zgubiony-holokrysztal-w-lesie; 211221-chevaleresse-infiltruje-rekiny)) |
| Azalia Sernat d'Namertel | 2 | ((210928-wysadzony-zywy-scigacz; 211123-odbudowa-wedlug-justyniana)) |
| Barnaba Burgacz      | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 211221-chevaleresse-infiltruje-rekiny)) |
| Diana Tevalier       | 2 | ((211221-chevaleresse-infiltruje-rekiny; 220111-marysiowa-hestia-rekinow)) |
| Franek Bulterier     | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 210928-wysadzony-zywy-scigacz)) |
| Henryk Wkrąż         | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 211207-gdy-zabraknie-pradu-rekinom)) |
| Jolanta Sowińska     | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Justynian Diakon     | 2 | ((211123-odbudowa-wedlug-justyniana; 211221-chevaleresse-infiltruje-rekiny)) |
| Karol Pustak         | 2 | ((210921-przybycie-rekina-z-eterni; 211123-odbudowa-wedlug-justyniana)) |
| Lucjan Sowiński      | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Mariusz Trzewń       | 2 | ((210720-porwanie-daniela-terienaka; 210817-zgubiony-holokrysztal-w-lesie)) |
| Napoleon Bankierz    | 2 | ((210323-grzybopreza; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Olga Myszeczka       | 2 | ((210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego)) |
| Paweł Szprotka       | 2 | ((210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego)) |
| Żorż d'Namertel      | 2 | ((210928-wysadzony-zywy-scigacz; 211221-chevaleresse-infiltruje-rekiny)) |
| Arnold Kłaczek       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Bogdan Gwiazdocisz   | 1 | ((240305-lea-strazniczka-lasu)) |
| Cyryl Perikas        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Diana Lemurczak      | 1 | ((210518-porywaczka-miragentow)) |
| Ekaterina Zajcew     | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Feliks Keksik        | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Halina Sermniek      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Hipolit Umadek       | 1 | ((220802-gdy-prawnik-przyjdzie-po-rekiny)) |
| Jeremi Sowiński      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Ksenia Kirallen      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Lea Samszar          | 1 | ((240305-lea-strazniczka-lasu)) |
| Malena Barandis      | 1 | ((240305-lea-strazniczka-lasu)) |
| Mariusz Kupieczka    | 1 | ((240305-lea-strazniczka-lasu)) |
| Melissa Durszenko    | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Michał Klabacz       | 1 | ((240305-lea-strazniczka-lasu)) |
| Mimoza Diakon        | 1 | ((220816-jak-wsadzic-ule-alanowi)) |
| Mimoza Elegancja Diakon | 1 | ((220222-plaszcz-ochronny-mimozy)) |
| Natalia Tessalon     | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Nataniel Morlan      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Różewicz Diakon      | 1 | ((210622-verlenka-na-grzybkach)) |
| Sabina Kazitan       | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Santino Mysiokornik  | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Serafina Ira         | 1 | ((210831-serafina-staje-za-wydrami)) |
| Staś Arienik         | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Stella Armadion      | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Teresa Mieralit      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Urszula Arienik      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |