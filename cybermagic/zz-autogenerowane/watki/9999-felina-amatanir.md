# Felina Amatanir
## Identyfikator

Id: 9999-felina-amatanir

## Sekcja Opowieści

### Dla swych marzeń, warto!

* **uid:** 240117-dla-swych-marzen-warto, _numer względny_: 6
* **daty:** 0106-11-04 - 0106-11-06
* **obecni:** Aerina Cavalis, Artur Tavit, Estril Cavalis, Felina Amatanir, Kalista Surilik, Klasa Hacker, Klasa Oficer Naukowy, Klasa Sabotażysta, Larkus Talvinir, Maia Sakiran, Mawir Hong, Vanessa d'Cavalis

Streszczenie:

Na Stacji pojawił się pierścień Esuriit, który sprawia, że ludzie potrafią poświęcić energię do spełnienia obsesyjnych marzeń kosztem życia tych ludzi. Za pierścieniem przybył Szmaciarz, próbujący dodać pierścień do swojej kolekcji artefaktów. Zespołowi udało się zatrzymać Skażenie i uratować członka rady - stracili jednak Vanessę (biosyntkę) oraz Kalista została Aktywowana. Za to zespół zintegrował biosynty z resztą stacji.

Aktor w Opowieści:

* Dokonanie:
    * wezwała Agencję rozpoznając Alteris w nagraniach o Szmaciarzu. Silnie przekonywała Estrila do współpracy z Agencją. Sprawdziła i dostarczała dossier ludzi na stacji. Kompetentna i skuteczna, acz w roli wsparcia.


### Pan Skarpetek i Odratowany Ogród

* **uid:** 231221-pan-skarpetek-i-odratowany-ogrod, _numer względny_: 5
* **daty:** 0106-04-25 - 0106-04-27
* **obecni:** Felina Amatanir, Ignatius Sozyliw, Kalista Surilik, Klasa Inżynier, Klasa Oficer Naukowy, Klasa Sabotażysta, Mawir Hong

Streszczenie:

Rada wezwała Agencję, bo dzieciaki potrafią rozkazywać dorosłym używając savarańskiej postaci z bajek, 'Pana Skarpetka'. Agencja odkrywa, że dzieciaki próbują zrobić Rajski Ogród - miejsce z roślinami i radością. Gdy Agencja orientuje się, że to jest niemożliwe technicznie, odkrywają wpływ energii Alteris. Po przebijaniu się przez Alteris udało im się uratować dzieci, ale pomieszczenie jest Zniekształcone i trzeba było Kaliście coś powiedzieć o magii... za to, Agencja ma poprawioną reputację, choć ma opinię tych co promują współpracę ludów na stacji.

Aktor w Opowieści:

* Dokonanie:
    * odcięta od własnych systemów przez swoich ludzi dotkniętych Alteris; briefowała Zespół i skierowała ich na Ignatiusa i innych.


### Przeznaczeniem Szernief nie jest wojna domowa

* **uid:** 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa, _numer względny_: 4
* **daty:** 0105-12-11 - 0105-12-13
* **obecni:** Aerina Cavalis, Dorion Fughar, Felina Amatanir, Gabriel Septenas, Kaldor Czuk, Kalista Surilik, Klasa Dyplomata, Klasa Inżynier, Klasa Sabotażysta, Mawir Hong, Sia-03 Szernief

Streszczenie:

Na Stacji doszło do odprawienia savarańskiego rytuału Krwi dobrobytu w formie energii Fidetis. Problem w tym, że savaranie mają jedną wizję sukcesu a Szernief ma N grup. Więc każda grupa jest wzmacniana w _inny_ sposób, co powoduje ciekawe problemy (np. sabotaże by działali razem, lub savaranie atakujący Mawirowców by ci mogli być silniejsi). Agencja odkrywa obecność Łowcy (polującego na Radę) i go neutralizuje. Niestety, Tkaczka integruje się z Sią-03 tworząc nową frakcję.

Aktor w Opowieści:

* Dokonanie:
    * nadal zablokowana przez Radę, ale zebrała dane zdjęciowe od Kalisty i pokazała je Agencji - tak wygląda ofiara potwora (The Hunter). I że Kalista dostała je od Mawira.


### Sen chroniący kochanków

* **uid:** 231122-sen-chroniacy-kochankow, _numer względny_: 3
* **daty:** 0105-09-02 - 0105-09-05
* **obecni:** Damian Orczakin, Dorion Fughar, Felina Amatanir, Jola-09 Szernief, Kalista Surilik, Klasa Dyplomata, Klasa Hacker, Klasa Oficer Naukowy, Klaudiusz Widar, Mawir Hong, Rovis Skarun, Szymon Alifajrin

Streszczenie:

Na stacji CON Szernief konflikt między savaranami a drakolitami, podsycany przez inwestorów i stymulanty, prowadzi do zagrożenia pryzmatycznego. Dochodzi do manifestacji Esuriit i Alucis. Punktem kluczowym okazała się miłość Rovisa i Joli-09, prowadząca do rozprzestrzeniającego się Snu Ukojenia zwalczającego i zwalczanego przez Nienawiść Esuriit. Lux Umbrarum interweniuje, tworząc antidotum i przesiedlając kochanków. Kryzys został rozwiązany i stacja uspokojona tymczasowo, ale Kalista jest na tropie 'dziwnych rzeczy' a zaufanie populacji stacji do Inwestorów i Rady spada.

Aktor w Opowieści:

* Dokonanie:
    * wie gdzie schował się Rovis i próbuje opanować i utrzymać CON Szernief do kupy, ale ma problemy przez energie magiczne. Współpracuje z Agencją. Mimo problemów, dalej ma zaufanie populacji stacji.


### Polowanie na biosynty na Szernief

* **uid:** 231213-polowanie-na-biosynty-na-szernief, _numer względny_: 2
* **daty:** 0105-07-26 - 0105-07-31
* **obecni:** Artur Tavit, Delgado Vitriol, Felina Amatanir, Kalista Surilik, Klasa Biurokrata, Klasa Dyplomata, Klasa Hacker, Klasa Oficer Naukowy, Klasa Sabotażysta, Malik Darien, Sebastian-194, Vanessa d'Cavalis

Streszczenie:

Kalista wezwała Agencję bo biosynty są niszczone (ludzie zabijają je pierwsi) i dzieje się 'coś dziwnego' a 'człowiek Agencji' jest niekompetentny - okazało się, że jest Skażony Unumens (ma pasożyta). Agencja zrzuciła 'legalność' biosyntów na arystokratę Artura, zapewnia sobie współpracę z biosyntami i kieruje je do zniszczenia Parasekta Unumens. By odwrócić uwagę od swoich działań - zdradza Kalistę i Mawira (i wszystkie problemy zrzuca na nich). Udało się jednak większość uratować, mimo śmierci kilku osób w tak trudnej sytuacji.

Aktor w Opowieści:

* Dokonanie:
    * próbuje pomóc Agencji, ale jest zablokowana przez Radę; część jej podwładnych (o czym nie wie) jest zainfekowana Parasektem. Nadal dała polecenie pomocy w roznoszeniu 'krewetkowych sensorów' po stacji i podpowiada Agencji tam gdzie jest w stanie.


### Tajemnicze Tunele Sebirialis

* **uid:** 231119-tajemnicze-tunele-sebirialis, _numer względny_: 1
* **daty:** 0104-11-20 - 0104-11-24
* **obecni:** Aniela Kafantelas, Eryk Kawanicz, Felina Amatanir, Ignatius Sozyliw, Janvir Krassus, Kalista Surilik, Klasa Dyplomata, Klasa Sabotażysta, Larkus Talvinir, Leon Hurmniow, Marta Sarilit, OLU Luminarius

Streszczenie:

W tunelach kopalni pod CON Szernief znikają ludzie. Agencja dochodzi do tego, że winny jest protomag Dotknięty Alteris, który uciekł z tajnej (dla populacji Szernief) placówki NavirMed gdzie prowadzi się badania na ludziach. Agencja doszła do łańcucha logicznego i odkryła prawdę; by to rozwiązać, ewakuowali kopalnię i placówkę NavirMed na 3 miesiące by protomag umarł z głodu.

Aktor w Opowieści:

* Dokonanie:
    * nieustraszona; jak tylko doszła do tego że nie może zrobić Dobra będąc w radzie i zablokowana przez kontrakt, szybko opuściła kontrakt. Poszła do tuneli Sebiralis szukać Kalisty i inwestorów. Spotkawszy Alteris, udało jej się ewakuować z Kalistą. Doszła do współpracy z NavirMed i jej się to nie podoba. Nie chce współpracować z Agencją, ale nie ma wyjścia.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 6, @: 0106-11-06
    1. Primus    : 6, @: 0106-11-06
        1. Sektor Kalmantis    : 6, @: 0106-11-06
            1. Sebirialis, orbita    : 6, @: 0106-11-06
                1. CON Szernief    : 6, @: 0106-11-06
                    1. Powłoka Wewnętrzna    : 2, @: 0105-09-05
                        1. Poziom Minus Dwa    : 2, @: 0105-09-05
                            1. Więzienie    : 2, @: 0105-09-05
                        1. Poziom Minus Jeden    : 2, @: 0105-09-05
                            1. Obszar Mieszkalny Savaran    : 2, @: 0105-09-05
                        1. Poziom Minus Trzy    : 2, @: 0105-09-05
                            1. Wielkie Obrady    : 2, @: 0105-09-05
                    1. Powłoka Zewnętrzna    : 1, @: 0105-09-05
                        1. Panele Słoneczne    : 1, @: 0105-09-05
            1. Sebirialis    : 1, @: 0104-11-24
                1. Krater Ablardius    : 1, @: 0104-11-24
                    1. Kopalnie Ablardius    : 1, @: 0104-11-24
                1. Płaskowyż Zaitrus    : 1, @: 0104-11-24
                    1. Ukryta Baza NavirMed    : 1, @: 0104-11-24

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kalista Surilik      | 6 | ((231119-tajemnicze-tunele-sebirialis; 231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto)) |
| Klasa Sabotażysta    | 5 | ((231119-tajemnicze-tunele-sebirialis; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto)) |
| Klasa Dyplomata      | 4 | ((231119-tajemnicze-tunele-sebirialis; 231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klasa Oficer Naukowy | 4 | ((231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto)) |
| Mawir Hong           | 4 | ((231122-sen-chroniacy-kochankow; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto)) |
| Klasa Hacker         | 3 | ((231122-sen-chroniacy-kochankow; 231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Aerina Cavalis       | 2 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240117-dla-swych-marzen-warto)) |
| Artur Tavit          | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Dorion Fughar        | 2 | ((231122-sen-chroniacy-kochankow; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Ignatius Sozyliw     | 2 | ((231119-tajemnicze-tunele-sebirialis; 231221-pan-skarpetek-i-odratowany-ogrod)) |
| Klasa Inżynier       | 2 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod)) |
| Larkus Talvinir      | 2 | ((231119-tajemnicze-tunele-sebirialis; 240117-dla-swych-marzen-warto)) |
| Vanessa d'Cavalis    | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Aniela Kafantelas    | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Damian Orczakin      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Delgado Vitriol      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Eryk Kawanicz        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Estril Cavalis       | 1 | ((240117-dla-swych-marzen-warto)) |
| Gabriel Septenas     | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Janvir Krassus       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Jola-09 Szernief     | 1 | ((231122-sen-chroniacy-kochankow)) |
| Kaldor Czuk          | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Klasa Biurokrata     | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Klaudiusz Widar      | 1 | ((231122-sen-chroniacy-kochankow)) |
| Leon Hurmniow        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Maia Sakiran         | 1 | ((240117-dla-swych-marzen-warto)) |
| Malik Darien         | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Marta Sarilit        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| OLU Luminarius       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Rovis Skarun         | 1 | ((231122-sen-chroniacy-kochankow)) |
| Sebastian-194        | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Sia-03 Szernief      | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Szymon Alifajrin     | 1 | ((231122-sen-chroniacy-kochankow)) |